{{good article}}
[[File:Algonquin Round Table.gif|thumb|250px|right|Members and associates of the Algonquin Round Table: (l-r) [[Art Samuels]], [[Charles MacArthur]], [[Harpo Marx]], [[Dorothy Parker]], and [[Alexander Woollcott]]]]
The '''Algonquin Round Table''' was a celebrated group of [[New York City]] writers, critics, actors, and wits. Gathering initially as part of a [[practical joke]], members of "The Vicious Circle", as they dubbed themselves, met for lunch each day at the [[Algonquin Hotel]] from 1919 until roughly 1929. At these luncheons they engaged in wisecracks, wordplay, and witticisms that, through the newspaper columns of Round Table members, were disseminated across the country.

Daily association with each other, both at the luncheons and outside of them, inspired members of the Circle to collaborate creatively. The entire group worked together successfully only once, however, to create a revue called ''[[Algonquin Round Table#No Sirree.21|No Sirree!]]'' which helped launch a Hollywood career for Round Tabler [[Robert Benchley]].

In its ten years of association, the Round Table and a number of its members acquired national reputations, both for their contributions to literature and for their sparkling wit. Although some of their contemporaries, and later in life even some of its members, disparaged the group, its reputation has endured long after its dissolution.

==Origin==
The group that would become the Round Table began meeting in June 1919 as the result of a practical joke carried out by theatrical press agent [[John Peter Toohey]]. Toohey, annoyed at ''[[New York Times]]'' drama critic [[Alexander Woollcott]] for refusing to plug one of Toohey's clients ([[Eugene O'Neill]]) in his column, organized a luncheon supposedly to welcome Woollcott back from [[World War I]], where he had been a correspondent for ''[[Stars and Stripes (newspaper)|Stars and Stripes]]''. Instead Toohey used the occasion to poke fun at Woollcott on a number of fronts. Woollcott's enjoyment of the joke and the success of the event prompted Toohey to suggest that the group in attendance meet at the Algonquin each day for lunch.<ref>{{cite book
| last =Herrmann
| first =Dorothy
| authorlink =
| title =With Malice Toward All: The Quips, Lives and Loves of Some Celebrated 20th-Century American Wits
| publisher =G. P. Putnam's Sons
| year =1982
| location = New York
| pages =17–18
| url =
| doi =
| id =
| isbn = 0-399-12710-0}}</ref>

The group first gathered in the Algonquin's Pergola Room (later called the [[Oak Room (Algonquin Hotel)|Oak Room]]) at a long rectangular table. As they increased in number, Algonquin manager [[Frank Case]] moved them to the Rose Room and a round table.<ref>Hermann, pp. 19–20</ref> Initially the group called itself "The Board" and the luncheons "Board meetings". After being assigned a waiter named Luigi, the group re-christened itself "Luigi Board". Finally they became "The Vicious Circle" although "The Round Table" gained wide currency after cartoonist [[Edmund Duffy]] of the ''[[Brooklyn Eagle]]'' [[caricature]]d the group sitting at a round table and wearing armor.<ref>Herrmann, p. 20</ref>

==Membership==
<!-- Deleted image removed: [[File:Algrt.jpg|thumb|right|The Algonquin Round Table in caricature by [[Al Hirschfeld]]. Seated at the table, clockwise from left: [[Dorothy Parker]], [[Robert Benchley]], [[Alexander Woollcott]], [[Heywood Broun]], [[Marc Connelly]], [[Franklin P. Adams]], [[Edna Ferber]], [[George S. Kaufman]], [[Robert E. Sherwood|Robert Sherwood]]. In back from left to right: frequent Algonquin guests [[Lynn Fontanne]] and [[Alfred Lunt]], ''[[Vanity Fair (magazine)|Vanity Fair]]'' editor [[Frank Crowninshield]] and [[Frank Case]].|350px]] -->

Charter members of the Round Table included:

* [[Franklin Pierce Adams]], columnist
* [[Robert Benchley]], humorist and actor
* [[Heywood Broun]], columnist and sportswriter (married to [[Ruth Hale (feminist)|Ruth Hale]])
* [[Marc Connelly]], playwright
* [[Ruth Hale (feminist)|Ruth Hale]], freelance writer who worked for women's rights
* [[George S. Kaufman]], playwright and director
* [[Dorothy Parker]], critic, poet, short-story writer, and screenwriter
* [[Brock Pemberton]], [[Broadway theatre|Broadway]] producer<ref>{{cite book 
  | last =Fitzpatrick
  | first =Kevin
  | authorlink =
  |author2=Nat Benchley 
   | title =The Lost Algonquin Round Table: Humor, Fiction, Journalism, Criticism and Poetry from America's Most Famous Literary Circle
  | publisher =Donald Books
  | year =2009
  | location =New York, NY
  | pages =259
  | url =
  | doi =
  | id =  
  | isbn =978-1-4401-5152-1 | type =hardcover }}</ref>
* [[Harold Ross]], ''[[The New Yorker]]'' editor
* [[Robert E. Sherwood]], author and playwright
* [[John Peter Toohey]], [[Broadway theatre|Broadway]] publicist
<!--Harpo Marx was NOT a charter member of the Round Table. He was an associate. Please stop adding him as a charter member. Thank you.-->
* [[Alexander Woollcott]], critic and journalist<ref>{{cite book
  | last =Meade
  | first =Marion
  | authorlink =
  | title =Dorothy Parker: What Fresh Hell is This?
  | publisher =Penguin Books
  | year =1987
  | location =New York, NY
  | pages =75
  | url =
  | doi =
  | id =  
  | isbn =0-14-011616-8 | type = paperback }}</ref>

Membership was not official or fixed for so many others who moved in and out of the Circle. Some of these included:
<!--DO NOT ADD NAMES TO THIS LIST WITHOUT ATTRIBUTION TO A RELIABLE SOURCE -->
* [[Tallulah Bankhead]], actress
* [[Noël Coward]], playwright<ref>Payn, G. ''My Life with Noel Coward''. Applause Books (2000), p. 159. ISBN 1557831904.</ref>
* [[Blyth Daly]], actress
* [[Edna Ferber]], author and playwright
* [[Eva Le Gallienne]], actress
* [[Margalo Gillmore]], actress
* [[Jane Grant]], journalist and feminist (married to Ross)
* [[Beatrice Kaufman]], editor and playwright (married to [[George S. Kaufman]])
* [[Margaret Leech]], writer and historian
* [[Neysa McMein]], magazine illustrator
* [[Harpo Marx]], comedian and film star
* [[Alice Duer Miller]], writer
* [[Donald Ogden Stewart]], playwright and screenwriter
* [[Frank Sullivan (writer)|Frank Sullivan]], journalist and humorist
* [[Deems Taylor]], composer
* [[Estelle Winwood]], actress and comedian
* [[Peggy Wood]], actress<ref>{{cite book
| last =Altman
| first =Billy
| title =Laughter's Gentle Soul: The Life of Robert Benchley
| publisher =W. W. Norton & Company
| year =1997
| location = New York
| pages =167–8
| isbn = 0-393-03833-5}}</ref>

==Activities==
In addition to the daily luncheons, members of the Round Table worked and associated with each other almost constantly. The group was devoted to games, including [[cribbage]] and [[poker]]. The group had its own poker club, the Thanatopsis Literary and Inside Straight Club, which met at the hotel on Saturday nights. Regulars at the game included Kaufman, Adams, Broun, Ross and Woollcott, with non-Round Tablers [[Herbert Bayard Swope]], silk merchant [[Paul Hyde Bonner]], baking heir [[Raoul Fleischmann]], actor [[Harpo Marx]], and writer [[Ring Lardner]] sometimes sitting in.<ref>Meade, pp. 76–7</ref> The group also played charades (which they called simply "The Game") and the "I can give you a sentence" game, which spawned Dorothy Parker's memorable sentence using the word ''horticulture'': "You can lead a horticulture but you can't make her think."<ref>Herrmann, p. 23</ref>

Members often visited [[Neshobe Island]], a private island co-owned by several "Algonks"—but governed by Aleck Woollcott as a "benevolent tyrant", as his biographer [[Samuel Hopkins Adams]] charitably put it<ref>[[Adams, Samuel Hopkins]]. ''A. Woollcott: His Life and His World''. Reynold & Hitchcock, 1945. p. 186</ref>—located on several acres in the middle of [[Lake Bomoseen]] in [[Vermont]].<ref>{{cite web
  | last =Bailey
  | first =Craig C.
  | authorlink =
  | title =Famous Vermonters, Part II
  | work =
  | publisher =Business Digest
  | date =August 1998
  | url =http://www.vermontguides.com/1998/profil74.htm
  | doi =
  | accessdate = 2007-09-19 | archiveurl= https://web.archive.org/web/20070925171009/http://www.vermontguides.com/1998/profil74.htm| archivedate= 25 September 2007 <!--DASHBot-->| deadurl= no}}</ref> There they would engage in their usual array of games including [[Wink murder]], which they called simply "Murder", plus [[croquet]].

A number of Round Tablers were inveterate practical jokers, constantly pulling pranks on one another. As time went on the jokes became ever more elaborate. Harold Ross and Jane Grant once spent weeks playing a particularly memorable joke on Woollcott involving a prized portrait of himself. They had several copies made, each slightly more askew than the last, and would periodically secretly swap them out and then later comment to Woollcott "What on earth is wrong with your portrait?" until Woollcott was beside himself. Eventually they returned the original portrait.<ref>Hermann, p. 28</ref>

==''No Sirree!''==
Given the literary and theatrical activities of the Round Table members, it was perhaps inevitable that they would write and stage their own revue. ''[[No Sirree!]]'', staged for one night only in April 1922, was a take-off of a then-popular European touring revue called ''[[La Chauve-Souris]]'', directed by [[Nikita Balieff]].<ref>{{cite book
| last =Kunkel
| first =Thomas
| authorlink = 
| title =Genius in Disguise: Harold Ross of The New Yorker
| publisher =Carroll & Graf Publishers (paperback)
| year =1995
| location = New York
| page =81
| url = 
| doi = 
| id =  
| isbn = 0-7867-0323-7}}</ref>

''No Sirree!'' had its genesis at the studio of [[Neysa McMein]], which served as something of a [[Salon (gathering)|salon]] for Round Tablers away from the Algonquin. Acts included: "Opening Chorus" featuring Woollcott, Toohey, Kaufman, Connelly, Adams and Benchley with violinist [[Jascha Heifetz]] providing offstage, off-key accompaniment; "He Who Gets Flapped", a musical number featuring the song "The Everlastin' Ingenue Blues" written by Dorothy Parker and performed by Robert Sherwood accompanied by "chorus girls" including [[Tallulah Bankhead]], [[Helen Hayes]], [[Ruth Gillmore]], [[Lenore Ulric]] and [[Mary Brandon (singer)|Mary Brandon]]; "Zowie, or the Curse of an [[Zoe Akins|Akins]] Heart"; "The Greasy Hag, an [[Eugene O'Neill|O'Neill]] Play in One Act" with Kaufman, Connelly and Woollcott; and "Mr. Whim Passes By—An [[A. A. Milne]] Play."<ref>Altman, p. 203</ref>

The only item of note to emerge from ''No Sirree!'' was Robert Benchley's contribution, ''[[The Treasurer's Report]]''. Benchley's disjointed parody so delighted those in attendance that [[Irving Berlin]] hired Benchley in 1923 to deliver the ''Report'' as part of Berlin's ''Music Box Revue'' for $500 a week.<ref>Altman, p. 208–9</ref> In 1928, ''Report'' was later made into a [[short film|short]] [[sound film]] in the [[Movietone sound system|Fox Movietone]] sound-on-film system by [[Fox Film Corporation]].<ref>{{cite web
  | last =Internet Movie Database
  | first =
  | authorlink =
  | title =The Treaurer's Report
  | work =
  | publisher =
  | url =http://imdb.com/title/tt0019491/
  | format =
  | doi =
  | accessdate = 2007-09-03 }}</ref> The film kicked off a second career for Benchley in Hollywood.<ref>{{cite web
  | last =Internet Movie Database
  | first =
  | authorlink =
  | title =Robert Benchley
  | work =
  | publisher =
  | url =http://imdb.com/name/nm0070361/
  | format =
  | doi =
  | accessdate = 2007-09-03 }}</ref>

With the success of ''No Sirree!'' the Round Tablers hoped to duplicate it with an "official" Vicious Circle production open to the public with material performed by professional actors. Kaufman and Connelly funded the revue, named ''The Forty-niners''.<ref>Altman, p. 207</ref> The revue opened in November 1922 and was a failure, running for just 15 performances.<ref>Meade, pp. 104–5</ref>

==Decline of the Round Table==
As members of the Round Table moved into ventures outside New York City, inevitably the group drifted apart. By the early 1930s the Vicious Circle was broken. Edna Ferber said she realized it when she arrived at the Rose Room for lunch one day in 1932 and found the group's table occupied by a family from Kansas. Frank Case was asked what happened to the group. He shrugged and replied, "What became of the [[Croton Distributing Reservoir|reservoir at Fifth Avenue and Forty-Second Street]]? These things do not last forever."<ref>Meade, p. 320</ref> Some members of the group remained friends after its dissolution. Parker and Benchley in particular remained close up until his death in 1945, although her political leanings did strain their relationship.<ref>Altman 314</ref> Others, as the group itself would come to understand when it gathered following Woollcott's death in 1943, simply realized that they had nothing to say to one another.<ref>{{cite book
| last =Case
| first =Frank
| authorlink = 
| title =Tales of a Wayward Inn
| publisher =Garden City Publishing Co
| year =1938
| location = New York
| pages =60
| url = 
| doi = 
| id =  
| isbn = }}</ref>

==Public response and legacy==
Because a number of the members of the Round Table had regular newspaper columns, the activities and quips of various Round Table members were reported in the national press. This brought Round Tablers widely into the public consciousness as renowned wits.

Not all of their contemporaries were fans of the group. Their critics accused them of ''[[logrolling]]'', or exchanging favorable plugs of one another's works, and of rehearsing their witticisms in advance.<ref>Hermann, p. 29</ref> [[James Thurber]] was a detractor of the group, accusing them of being too consumed by their elaborate practical jokes. [[H. L. Mencken]], who was much admired by many in the Circle, was also a critic, commenting to fellow writer [[Anita Loos]] that "their ideals were those of a [[vaudeville]] actor, one who is extremely 'in the know' and inordinately trashy".<ref>Hermann, p. 30</ref>

The group showed up in the 1923 best-seller ''[[Black Oxen]]'' by [[Gertrude Atherton]]. She sarcastically described a group she called "the Sophisticates":

{{quote box|They met at the sign of the Indian Chief where the cleverest of them&mdash;and those who were so excitedly sure of their cleverness that for the moment they convinced others as well as themselves&mdash;foregathered daily. There was a great deal of scintillating talk in this group of the significant books and tendencies of the day....They appraised, debated, rejected, finally placed the seal of their august approval upon a favored few.<ref>Altman, p. 170</ref>}}

[[Groucho Marx]], brother of Round Table associate Harpo, was never comfortable amidst the viciousness of the Vicious Circle. "The price of admission is a serpent's tongue and a half-concealed [[stiletto]]."<ref>{{cite web
  | last =Randall
  | first =David
  | authorlink =
  | title =Dorothy, the Algonquin hotel and how the legend just kept growing
  | work =
  | publisher =The Independent (London)
  | date =2002-06-30
  | url =http://findarticles.com/p/articles/mi_qn4158/is_20020630/ai_n12628142
  | format =
  | doi =
  | accessdate = 2007-09-16 }} {{Dead link|date=September 2010|bot=H3llBot}}</ref> Even some members of the Round Table disparaged it later in life. Dorothy Parker in particular criticized the group.

{{quote box|These were no giants. Think who was writing in those days&mdash;[[Ring Lardner|Lardner]], [[F. Scott Fitzgerald|Fitzgerald]], [[William Faulkner|Faulkner]] and [[Ernest Hemingway|Hemingway]]. Those were the real giants. The Round Table was just a lot of people telling jokes and telling each other how good they were. Just a bunch of loudmouths showing off, saving their gags for days, waiting for a chance to spring them....There was no truth in anything they said. It was the terrible day of the wisecrack, so there didn't have to be any truth...<ref>Herrmann p. 85</ref>}}

Despite Parker's bleak assessment and while it is true that some members of the Round Table are perhaps now "[[famous for being famous]]" instead of for their literary output, Round Table members and associates contributed to the literary landscape, including [[Pulitzer Prize]]-winning work by Circle members Kaufman, Connelly and Sherwood (who won four) and by associate Ferber and the legacy of Ross's ''New Yorker''. Others made lasting contributions to the realms of stage and screen — [[Tallulah Bankhead]] and [[Eva Le Gallienne]] became Broadway greats and the films of Harpo and Benchley remain popular; and Parker has remained renowned for her short stories and literary reviews.

The Algonquin Round Table, as well as the number of other literary and theatrical greats who lodged there, helped earn the Algonquin Hotel its status as a New York City Historic Landmark. The hotel was so designated in 1987.<ref>{{Cite news
  | last =Anderson
  | first =Susan Heller
  | title =City Makes It Official: Algonquin is Landmark
  | newspaper =[[The New York Times]]
  | pages =
  | date =20 September 1987
  | url = https://query.nytimes.com/gst/fullpage.html?res=9B0DE6DE1E38F933A1575AC0A961948260
}}</ref> In 1996 the hotel was designated a national literary landmark by the [[Friends of Libraries USA]] based on the contributions of "The Round Table Wits". The organization's bronze plaque is attached to the front of the hotel.<ref>{{cite web
  | last =Friends of Libraries USA
  | first =
  | authorlink =
  | title =1996 dedications
  | work =
  | publisher =
  | url =http://www.folusa.org/outreach/landmarks-year/1996.php
   | format =
  | doi =
  | accessdate = 2007-09-13 |archiveurl = https://web.archive.org/web/20080506140823/http://www.folusa.org/outreach/landmarks-year/1996.php |archivedate = 2008-05-06}}</ref>
Although the Rose Room was removed from the Algonquin in a 1998 remodel, the hotel paid tribute to the group by commissioning and hanging the painting [http://ascencios.com/work/algonquin/ ''A Vicious Circle''] by [[Natalie Ascencios]], depicting the Round Table and also created a replica of the original table.<ref>{{cite web
  | last =Iovine
  | first =Julie V.
  | authorlink =
  | title =Algonquin, at Wits' End, Retrofits
  | work =
  | publisher =New York Times
  | date =1998-05-28
  | url =https://query.nytimes.com/gst/fullpage.html?res=9C00E1DC1238F93BA15756C0A96E958260&sec=travel&spon=&pagewanted=print
| format =
  | doi =
  | accessdate = 2007-09-13 }}</ref> The hotel occasionally stages an original musical production, ''The Talk of the Town'', in the Oak Room. Its latest production started September 11, 2007 and ran through the end of the year.<ref>{{cite web
  | last =Algonquin Hotel
  | first =
  | authorlink =
  | title =Algonquin Round Table Musical "The Talk of the Town" Returns to Original 1919 Oak Room Setting
  | work =
  | publisher =
  | url =http://www.algonquinhotel.com/press_roundtablemusical.html
  | doi =
  | accessdate =  2007-09-03 |archiveurl = https://web.archive.org/web/20070629132114/http://www.algonquinhotel.com/press_roundtablemusical.html |archivedate = June 29, 2007}}</ref>

A film about the members, ''[[The Ten-Year Lunch]]'' (1987), won the [[Academy Awards|Academy Award]] for Best Documentary Feature.<ref>{{cite news
  | last = Thomas
  | first = Bob
  | title = 'Last Emperor' Leads Awards
  | newspaper = The Gainesville Sun
  | pages = 1A, 14A
  | publisher = Associated Press
  | date = 1988-04-12
  | url = https://news.google.com/newspapers?id=PJ0RAAAAIBAJ&sjid=FuoDAAAAIBAJ&pg=5577,3293895&dq=ten-year+lunch+academy+award&hl=en
  | archive-url = https://archive.is/20130124144546/http://news.google.com/newspapers?id=PJ0RAAAAIBAJ&sjid=FuoDAAAAIBAJ&pg=5577,3293895&dq=ten-year+lunch+academy+award&hl=en
  | dead-url = yes
  | archive-date = 2013-01-24
  | accessdate = 2009-10-07}} </ref>
The dramatic film ''[[Mrs. Parker and the Vicious Circle]]'' (1994) recounted the Round Table from the perspective of Dorothy Parker.<ref>{{cite web
  | last =Internet Movie Database
  | first =
  | authorlink =
  | title =Mrs. Parker and the Vicious Circle
  | work =
  | publisher =
  | url =http://www.imdb.com/title/tt0110588/
  | format =
  | doi =
  | accessdate =  2007-09-03}}</ref>

Portions of the 1981 film ''[[Rich and Famous (1981 film)|Rich and Famous]]'' were set in the Algonquin and one of the film's characters, Liz Hamilton (played by [[Jacqueline Bisset]]) refers to the Round Table during the film.

In 1991, the Algonquin Round Table was mentioned in the Seinfeld season 2 episode [[The Phone Message]].<ref>{{Cite web|url=http://www.seinfeldscripts.com/ThePhoneMessage.htm|title=The Phone Message|website=SeinfeldScripts|access-date=2016-08-23}}</ref>

In 1993, the Algonquin Round Table was featured in ''[[The Young Indiana Jones Chronicles|The Young Indiana Jones and the Scandal of 1920]]'' where the titular character meets the group and attends at least two lunches. ''Wonderful Nonsense - The Algonquin Round Table'' is a documentary produced for the DVD release of that film in 2008.<ref>[http://www.starwars.com/community/news/films/news20080415.html Young Indy on DVD: A Tour of Volume 3: Disc 8] on StarWars.com ([https://web.archive.org/web/20080418140917/http://www.starwars.com:80/community/news/films/news20080415.html backup link] on Archive.org)</ref>

In 1997,  the television show *Friends* referenced the Algonquin Round Table in a line delivered by Matthew Perry's character as,  "The Algonquin's Kids' Table", during Season 4, Episode 3 titled "The One With the 'Cuffs".

In 2009, Robert Benchley's grandson, [[Nat Benchley]], and co-editor [[Kevin C. Fitzpatrick]] published ''The Lost Algonquin Round Table'', a collection of the early writings of the group.

In the 2015 Hollywood movie [[The Martian (film)|The Martian]], the NASA JPL engineer Tim Grimes refers to the Algonquin Round Table when describing the crude communication JPL had established with stranded astronaut Mark Whatney: "Thirty-two minute round trip communications time, all he can do is ask yes/no questions, and all we can do is point the camera. This won't exactly be an Algonquin Round Table of snappy repartee."

{{Portal|1920s}}

==References==
{{reflist|2}}

==External links==
* [http://www.algonquinhotel.com/round-table Algonquin Round Table] at the [[Algonquin Hotel]]
* {{Official website|http://www.algonquinroundtable.org/}}
* [http://www.pbs.org/wnet/americanmasters/database/algonquin_round_table.html Algonquin Round Table] at PBS's American Masters
* {{cite web |url= http://archives.nypl.org/the/22110 |title= Aviva Slesin collection of research and production materials for the ten-year lunch: the wit and legend of the Algonquin Round Table (1920s&mdash;1988) |work= Billy Rose Theatre Division |publisher= [[New York Public Library for the Performing Arts]] }}

[[Category:American literary movements]]
[[Category:Culture of Manhattan]]
[[Category:American humorists]]
[[Category:Literary circles]]