<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Lil Breezy
 | image=File:Harper Aircraft Lil Breezy-B photo 1 cropped.JPG
 | caption=Harper Lil Breezy- B single seater
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]] and [[Light-sport aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Harper Aircraft]]
 | designer=Jack Harper
 | first flight=
 | introduced=
 | retired=
 | status= Production completed (2012)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]21,000 (Two seat version, 2011)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Harper Lil Breezy''' is an [[United States|American]] [[ultralight aircraft|ultralight]] and [[light-sport aircraft]] that was designed by Jack Harper and produced by [[Harper Aircraft]] of [[Jacksonville, Florida]]. While the company was in business the aircraft was supplied as a kit for [[Homebuilt aircraft|amateur construction]] or as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 58. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
[[File:Harper Aircraft L'il Breezy B photo 3.jpg|thumb|Harper Lil Breezy-B single seater at [[Sun 'n Fun]] 2004]]
The Lil Breezy was inspired by the similar [[RLU-1 Breezy]] and designed to comply with the United States [[FAR Part 103 Ultralight Vehicles]] rules in its single place version and also the US light-sport aircraft rules. It features a [[strut-braced]] [[high-wing]], a single-seat or optionally a two-seats-in-[[tandem]] open cockpit without a windshield, fixed [[conventional landing gear]] and a single engine in [[pusher configuration]].<ref name="WDLA11" />

The early Lil Breezy-A is made from welded steel tubing with its flying surfaces covered in [[Aircraft dope|doped]] [[aircraft fabric]]. The later "B" model is of [[aluminium]] construction, with flying surfaces covered with [[Dacron]] sailcloth. Standard engines available included the {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]] [[two-stroke]] powerplant.<ref name="WDLA11" />
<!-- ==Operational history== -->

==Variants==
;Lil Breezy-A
:Initial model, available as a kit, with a steel tube [[fuselage]].<ref name="WDLA11" />
;Lil Breezy-B
:Second model, available as a kit or later as a complete aircraft only, with aluminium construction.<ref name="WDLA11" />
<!-- ==Aircraft on display== -->

==Specifications (Lil Breezy-B) ==
[[File:Harper Aircraft L'il Breezy B photo 4.jpg|thumb|Harper Lil Breezy-B single seater at [[Sun 'n Fun]] 2004]]
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=200
|empty weight lb=
|empty weight note=
|gross weight kg=307
|gross weight lb=
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 503]]
|eng1 type=twin cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=50<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=105
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=80
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=45
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[Blue Yonder EZ Flyer]]
*[[Mathews Petit Breezy]]

==References==
{{reflist}}

==External links==
{{Commons category}}
*[http://wayback.archive.org/web/*/http://www.harperaircraft.com/ Harper Aircraft website archives]

[[Category:United States ultralight aircraft 2000–2009]]
[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Single-engine aircraft]]