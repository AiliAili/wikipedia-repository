{{Use dmy dates|date=January 2017}}
{{Use British English|date=January 2017}}
{{refimprove|date=October 2008}}
[[File:FRANCIS YEATS-BROWN as a hungarian mechanic.jpg|right|thumb|Yeats-Brown as a "Hungarian Mechanic" in 1919.]]
'''Major Francis Charles Claypon Yeats-Brown''', [[Distinguished Flying Cross (United Kingdom)|DFC]] (15 August 1886 – 19 December 1944) was an officer in the British Indian army and the author of the celebrated memoir ''[[The Lives of a Bengal Lancer (book)|The Lives of a Bengal Lancer]]'', for which he was awarded the 1930 [[James Tait Black Memorial Prize]].<ref>[http://awardsandwinners.com/category/james-tait-black-memorial-prize/1930/ "1930 James Tait Black Memorial Prize"]. Awards & Winners. Retrieved 13 September 2014.</ref>

==Life and career==

Yeats-Brown was born in [[Genoa]] in 1886, the son of the British consul [[Montague Yeats-Brown]]. He studied at [[Harrow School|Harrow]] and [[Royal Military Academy Sandhurst|Sandhurst]]. When he was 20, he went to [[British Raj|India]] where he was attached to the [[King's Royal Rifle Corps]] at [[Bareilly]] in present-day [[Uttar Pradesh]]. He was then transferred to the [[cavalry]] and sent to the perennially turbulent [[North-West Frontier Province (1901–1955)|North West Frontier]] region. His time there engendered in him a sympathy for the [[Muslim]] point of view, and in later years he would support the creation of an independent [[Pakistan]].

During the [[World War I|First World War]], Yeats-Brown saw action in [[France]] and in [[Mesopotamia]], where he was a member of the [[Royal Flying Corps]]. His acts of bravery gained him the DFC. In 1915, his plane was damaged on landing on a sabotage mission outside Baghdad, and he spent the following two years as a [[prisoner of war]]. This provided the material for his first book ''Caught by the Turks'' (1919).

Following a temporary commission in the [[Royal Air Force]] he returned to the Indian Army in August 1919.<ref name="Gazette31825" /> He retired from the army in 1924,<ref name="Gazette32983" /> and joined the staff of the ''[[The Spectator|Spectator]]'' magazine as assistant editor. He quit the post in 1928. ''Bengal Lancer'', his most famous book, was published in 1930.  The book is a memoir of Yeats-Brown's time in India from 1905 to 1914, with an emphasis on [[cantonment]] life at and around Bareilly. An immediate hit with readers and critics, the book won the [[James Tait Black Award]] that year, and was turned into a successful [[The Lives of a Bengal Lancer (film)|1935 film of the same name]], starring [[Gary Cooper]]. In 1936, he published ''Lancer at Large'' where he showed an affinity for the principles of [[yoga]].

During the 1930s, Yeats-Brown also became involved in right-wing politics. He was a member of the [[January Club]] and the [[Right Club]],<ref>[[Anna Wolkoff]]</ref> and wrote newspaper articles in praise of [[Francisco Franco]] and [[Hitler]], asserting that Hitler had solved Germany's unemployment problem. He also wrote articles for ''[[New Pioneer]]'', a far-right journal controlled by [[Gerard Wallop, 9th Earl of Portsmouth|Viscount Lymington]] and closely linked to the [[British People's Party (1939)|British People's Party]].<ref>Martin Pugh, ''Hurrah for the Blackshirts! Fascists and Fascism in Britain Between the Wars'', Pimlico, 2006, p. 279</ref>

When the [[World War II|Second World War]] broke out in 1939, Yeats-Brown took up a commission again. During 1943–44, he toured the camps of India and the battlefields of [[Burma]], gathering material for a book entitled ''Martial India''. He died in [[England]] in December 1944.

==Selected works==
* ''[[Caught by the Turks]]'' (1919).
* ''[[The Lives of a Bengal Lancer (book)|The Lives of a Bengal Lancer]]'' (1930)
* ''Golden Horn'' (1932)
* ''Dogs of War'' (1934)
* ''Lancer at Large'' (1936)
* ''Yoga Explained'' (1937)
* ''[[European Jungle]]'' (1939)
* ''Indian Pageant'' (1942)
* [https://books.google.com/books/about/Martial_India.html?id=Xa4DAAAAMAAJ&redir_esc=y ''Martial India''] (1945), [[Eyre and Spottiswoode]]

==Honours and awards==
*10 October 1919 - Flying Officer Francis Charles Claydon Yeats-Brown of the Royal Air Force is awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] in recognition of distinguished services rendered during the war.<ref name="dfc" />

==References==
{{Reflist|refs=
<ref name="dfc">{{LondonGazette |issue=31592 |date=12 October 1919|startpage=12527 |supp=x |accessdate=27 August 2010}}</ref>
<ref name="Gazette31825">{{LondonGazette |issue=31825 |date=16 March 1920|startpage=3319 |supp= |accessdate=27 August 2010}}</ref>
<ref name="Gazette32983">{{LondonGazette |issue=32983 |date=17 October 1924 |startpage=7510 |supp= |accessdate=27 August 2010}}</ref>
}}

==External links==
* {{Gutenberg author | id=Yeats-Brown,+Francis | name=Francis Yeats-Brown}}
* {{FadedPage|id=Yeats-Brown, Francis Charles Claypon|name=Francis Yeats-Brown|author=yes}}
* {{Internet Archive author |sname=Francis Yeats-Brown}}

{{Authority control}}

{{DEFAULTSORT:Yeats-Brown, Francis}}
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:British writers]]
[[Category:People educated at Harrow School]]
[[Category:Graduates of the Royal Military College, Sandhurst]]
[[Category:1886 births]]
[[Category:1944 deaths]]
[[Category:British Indian Army officers]]
[[Category:King's Royal Rifle Corps officers]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Flying Corps officers]]
[[Category:World War I prisoners of war held by the Ottoman Empire]]
[[Category:British World War I prisoners of war]]
[[Category:Memoirs of imprisonment]]<!--Until "Caught by the Turks" has its own page-->
[[Category:Royal Air Force officers]]
[[Category:James Tait Black Memorial Prize recipients]]
[[Category:British fascists]]