<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Limousine
 | image=Merçay Limousine.jpg
 | caption=The Limousine at the 1919 Salon
}}{{Infobox Aircraft Type
 | type=Two seat tourer
 | national origin=[[France]]
 | manufacturer=SAECA Edmund de Marçay
 | designer=
 | first flight=1920
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''de Marçay Limousine''' was a two seat [[France|French]] touring [[biplane]] introduced at the 1919 Paris Aero Salon. A smaller but otherwise very similar single-seater was also there.

==Design and development==

As well as the very small and low-powered [[de Marçay Passe-Partout|Passe-Partout]], the de Marçay stand at the 1919 Paris Aero Salon displayed two touring aircraft, both powered by {{convert|60|hp|kW|abbr=on|order=flip}} [[Le Rhône 9Z]] nine-cylinder [[rotary engine]]s.  One was a single-seater and the other, the rather larger Limousine, seated two.<ref name=Flight2/><ref name=Laero2/>  Some recent sources refer to the latter as the '''de Marçay T-2'''.<ref name=AvFr/> At the time of the show, neither had flown.<ref name=Laero1/>

Both aircraft were [[biplane#Bay|single bay biplanes]] with wings of rectangular plan mounted with strong [[stagger (aeronautics)|stagger]].  Both upper and lower wings were one-piece, two [[spar (aeronautics)|spar]] structures. Normally such wings were braced together on each side by a pair of [[interplane strut]]s, one between each of the two corresponding upper and lower spars and stayed by incidence wires, but the de Marçays had instead rigid interplane braces of [[parallelogram]] form.  The upper wing  was supported over the [[fuselage]] with two pairs of N-form [[cabane strut]]s and the lower one passed under the fuselage and was joined to it by three short [[strut]]s, two to the forward spar linked to the engine mounting and the other, centrally, to the rear.<ref name=Flight2/> The wingspan and wing area of the Limousine were about 17% greater than that of the single seater.<ref name=Flight1/>

They shared a semi-ellipsoidal aluminium engine [[aircraft fairing#Engine cowling|cowling]], split into a [[spinner (aeronautics)|spinner]] from which the [[propeller (aeronautics)|propeller]] protruded and with a large opening for cooling air, and a fixed rear part that reached back to the forward cabane. Behind the engine mounting the fuselage was a circular section, tapered [[monocoque]], with the [[cockpit]] of the single-seater under a cut-out in the [[trailing edge]] of the upper wing. The absence of internal structure in the fuselage made it straightforward to extend the Limousine's fuselage by 23% to include a second cockpit.  At the Salon its two seats were enclosed within a rather blunt [[aircraft canopy|canopy]] or coupé with a flat windscreen and two windows on each side. This was readily detachable and it is not known if the Limousine was flown with it in place.<ref name=Flight2/><ref name=Laero2/><ref name=Flight1/>

Their empennages were conventional, with [[plywood]] covered horizontal tails on top of the fuselages.  The [[fin]]s were small and  semi-circular and the [[aircraft fabric covering|fabric covered]] [[rudder]]s had scalloped, rounded edges. They had fixed, tailskid [[landing gear|undercarriages]] with mainwheels on a single axle rubber-sprung from a transverse member mounted on V-struts. The forward member of the V joined the forward fuselage just behind the metal cowling and the rear one went to the lower wing forward attachment points.<ref name=Flight2/>

There are a few reports on the post-Salon activities of the single-seat de Marçay tourer. In late March 1922 it took off from a football pitch near [[le Bourget]] and reached {{convert|200|km/h|mph|abbr=on}}.<ref name=Lailes1/> In late June that year it was scheduled to be flown by Guérin at an international meeting in [[Brussels]] organised by the Belgian Aeroclub.<ref name=Lailes2/>

==Variants==
;Limousine: Two seat, as described.
;Single seater: Smaller with span {{convert|16|ft|6|in|m|abbr=on|order=flip}}, length {{convert|14|ft|10|in|m|abbr=on|order=flip}}, empty weight {{convert|300|lb|kg|abbr=on|order=flip}}, maximum speed {{convert|112|mph||km/h|abbr=on|order=flip}}.<ref name=Flight1/>

==Specifications (Limousine) ==
{{Aircraft specs
|ref=Flight (1 January 1920, pp.16-7)<ref name=Flight1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=one pilot
|capacity=one passenger
|length ft=18
|length in=3
|length note=
|span ft=19
|span in=7
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=1.95
|height note=<ref name=Laero2/>
|wing area sqft=167
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight lb=390
|empty weight note=
|gross weight lb=835
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Le Rhone 9Z]]
|eng1 type=9-cylinder [[rotary engine|rotary]]
|eng1 hp=60
|eng1 note=<ref name=Laero2/>
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=87
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
}}

==References==
{{reflist|2|refs=

<ref name=Flight1>{{cite journal |date=1 January 1920 |title=The Paris Aero Show at a Glance|journal= [[Flight International|Flight]]|volume=XII |issue=1 |pages=16–17  |url=http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200017.html}}</ref>

<ref name=Flight2>{{cite journal |date=8 January 1920 |title=The Paris Aero Show 1919|journal= [[Flight International|Flight]]|volume=XII |issue=2 |pages=45–6  |url= http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200047.html}}</ref>

<ref name=Laero1>{{cite journal |date=1–15 February 1920 |title=Les Avions actuels et le 6<sup>e</sup> Salon Aéronautique|journal=L'Aérophile|volume=28 |issue=3-4 |pages=34|url=http://gallica.bnf.fr/ark:/12148/bpt6k6551870h/f3.image }}</ref>

<ref name=Laero2>{{cite journal |date=1–15 February 1920 |title=Les Avions actuels et le 6<sup>e</sup> Salon Aéronautique|journal=L'Aérophile|volume=28 |issue=3-4 |pages=44–5|url=http://gallica.bnf.fr/ark:/12148/bpt6k6551870h/f24 }}</ref>

<ref name=Lailes1>{{cite journal |date=30 March 1922 |title=On essaie ... L'avion tourisme de Marçay|journal=L'Ailes|volume=20 |issue=41 |pages=2|url=http://gallica.bnf.fr/ark:/12148/bpt6k6555987c/f2}}</ref>

<ref name=Lailes2>{{cite journal |date=8 June 1922 |title=Le meeting de Bruxelles Marçay|journal=L'Ailes|volume=20 |issue=51 |pages=3|url=http://gallica.bnf.fr/ark:/12148/bpt6k6555997r/f3}}</ref>

<ref name=AvFr>{{cite web |url=http://www.aviafrance.com/de-marcay-t-2-aviation-france-7955.htm|title=de Marçay T-2 |author=Bruno Parmentier |accessdate=8 August 2015}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

{{DEFAULTSORT:De Marcay Limousine}}
[[Category:French sport aircraft 1920–1929]]
[[Category:De Marcay aircraft|Limousine]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]