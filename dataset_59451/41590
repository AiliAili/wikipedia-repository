{{Infobox military conflict
|conflict=Battle of Gedo
|partof=[[Somali Civil War]], [[War in Somalia (2009-present)]]
|date=April 27, 2011-
|image=
|place=[[Gedo]], [[Somalia]]
|caption=
|result= Ongoing
*[[Federal Government of Somalia]] controls 6 districts , Al-Shabaab controls 1
|combatant1={{flagicon image|ShababAdmin.png}} [[Harakat al-Shabaab Mujahideen]]
|combatant2=
{{Flagicon|Somalia}} [[Federal Government of Somalia]] 
[[File:Flag of Ahlu Sunnah Waljamaca.png|22px]] [[Ahlu Sunna Waljama'a]]<br />
{{flag|Ethiopia}}<br />
{{flagicon|Kenya}} [[Kenya Defence Forces]]
|commander1={{flagicon image|ShababAdmin.png}} [[Fuad Mohamed Qalaf|Fuad Shangole]]
|commander2=
[[File:Flag of Ahlu Sunnah Waljamaca.png|22px]]  [[Sheikh Hassan Sheikh Ahmed]] {{KIA}}<br>
[[File:Flag of Ahlu Sunnah Waljamaca.png|22px]] [[Sheikh Mohammed Hussein Al Qadi]]<br>
[[File:Flag of Ahlu Sunnah Waljamaca.png|22px]] [[Sheikh Is-haq Mursal]]<br>
{{Flagicon|Somalia}} [[Diyad Abdi Kalil]]
|strength1=
|strength2=
|casualties1= 168+ killed <br>4 military vehicles captured
|casualties2= Total: 81+ KIA
*{{Flagicon|Somalia}} 46+ KIA
*[[File:Flag of Ahlu Sunnah Waljamaca.png|22px]] 35+ KIA<br>3 military vehicles destroyed<br> 2 military vehicles damaged<br> 6 military vehicles captured*
|casualties3= At least 27 civilians killed<br>At least 380 killed in total
|notes=*Based largely on Al-Shabaab claims which were not confirmed
}}
{{Campaignbox War in Somalia (2009–)}}
The '''Battle of Gedo''' is an ongoing conflict of the [[War in Somalia (2009–present)|2009–present phase]] of the [[Somali Civil War]]. Centered in the region of [[Gedo]], it pits the [[Federal Government of Somalia|Somali government]] and its allies against the [[al-Qaeda]]-aligned militant group [[Al-Shabaab (militant group)|Al-Shabaab]].

==Timeline==
=== April 27, 2011 ===
The fighting started after Somali forces ambushed [[Al-Shabaab (militant group)|Al-Shabaab]] fighters in [[Tulo-Barwaqo]], near Garbaharey. The spokesman for the [[Ahlu Sunna Waljama'a]] paramilitary claimed they killed around 20 fighters from the opposing side.<ref>{{cite web|url=http://www.shabelle.net/article.php?id=5946 |accessdate=April 29, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110914233714/http://shabelle.net/article.php?id=5946 |archivedate=September 14, 2011 }}</ref> On the road between [[Elberde]] and [[Luuq]] a roadside mine killed 15 people. The landmine detonated underneath a minibus, killing nine passengers immediately. Six passengers subsequently died from blood loss.
In Tulo-Barwaqo, Ahlu Sunna Waljama’a fighters clashed with Al-Shabaab militants an Ahlu Sunna spokesman, reported that Ahlu Sunna fighters captured “[[assault rifles]], [[pistols]], [[explosive devices]] and … two military vehicles” and had killed about twenty Al-Shabaab militants.
In Luuq, Al-Shabaab attacked Transitional Federal Government troops using hit-and-run tactics, killing four soldiers.<ref>{{cite web|url=http://www.criticalthreats.org/gulf-aden-security-review/gulf-aden-security-review-april-27-2011 |title=Gulf of Aden Security Review - April 27, 2011 |publisher=Critical Threats |date= |accessdate=2013-04-30}}</ref>
In Elwak, Al-Shabaab ambushed government soldiers, killing ten. One military vehicle belonging to Somali military forces was burnt, and two others were damaged by gunshots in the skirmish.<ref>{{cite web|url=http://www.presstv.ir/detail/176955.html |title=No Operation |publisher=Presstv.ir |date= |accessdate=2013-04-30 |deadurl=yes |archiveurl=https://web.archive.org/web/20121009120547/http://www.presstv.ir/detail/176955.html |archivedate=October 9, 2012 }}</ref>

=== April 28, 2011 ===
Some 36 Al-Shabaab fighters were killed after fighting with Somali army in the village of Tulo Barwaqo just outside Garbaharey town.<ref>[http://www.shabelle.net/article.php?id=5987 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}
Al-Shabaab launched an ambush attack on Somali forces in Gedo region. Al-Shabaab are said to have ambushed government forces flanked by military vehicles at the village of [[Kured]] just outside [[Luuq District]]. At least 5 persons, all the combatants, were killed and dozens more injured during the confrontation.<ref>[http://www.shabelle.net/article.php?id=6006 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}
27 gunmen from pro-government Ahlu Sunna Waljama'a group were killed in the skirmishes this day and 8 transitional government troops.<ref>{{cite web|url=http://www.presstv.ir/detail/177152.html |title=No Operation |publisher=Presstv.ir |date= |accessdate=2013-04-30 |deadurl=yes |archiveurl=https://web.archive.org/web/20121009120517/http://www.presstv.ir/detail/177152.html |archivedate=October 9, 2012 }}</ref>

=== April 29, 2011 ===
In Luuq, Al-Shabaab fighters ambushed Somali army forces, killing at least 10 people, mostly from the combatants, and injuring dozens more.<ref>[http://www.shabelle.net/article.php?id=6023 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}} A total of 24 Somali forces lost their lives and two military vehicles were destroyed that day.<ref>{{cite web|url=http://www.presstv.ir/detail/177459.html |title=No Operation |publisher=Presstv.ir |date= |accessdate=2013-04-30 |deadurl=yes |archiveurl=https://web.archive.org/web/20121009120558/http://www.presstv.ir/detail/177459.html |archivedate=October 9, 2012 }}</ref>

=== April 30, 2011 ===
A clash erupted in the village of Burgadud near Garbaharey when Somali forces launched premeditated attacks on Al-Shabaab fighters, at last 6 government soldiers were wounded.<ref>[http://www.shabelle.net/article.php?id=6061 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}
Somali troops and ASWJ forces took over the cities of Tulo Barwaqo and Garbaharey when Al-Shabaab fighters reportedly abandoned the towns.<ref>{{cite web|url=http://shabelle.net/article.php?id=6139 |accessdate=May 2, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110723071709/http://shabelle.net/article.php?id=6139 |archivedate=July 23, 2011 }}</ref> At the same time Al-Shabaab claim victory in these fighting. At least 10 Al-Shabaab fighters have been killed during the confrontations and two military vehicles from Ahlu Sunna Waljama'a were reported seized.<ref>[http://www.shabelle.net/article.php?id=6094 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}

=== May 1, 2011 ===
Heavy fighting was still continuing in Gedo. In Af-barwaqo and Buurgudud villages, at least 10 persons were killed and 14 others wounded. The spokesman of Al-Shabaab, [[Sheikh Abdiaziz abu Mus’ab]], said his fighters seized four pick-up cars from government forces including one battle wagon.<ref>{{cite web|url=http://www.raxanreeb.com/?p=94412 |accessdate=May 2, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110725072058/http://www.raxanreeb.com/?p=94412 |archivedate=July 25, 2011 }}</ref>

=== May 2, 2011 ===
Early in the morning, Al-Shabaab forces launched an attack on [[Garbaharey]]; they tried to seize the city but failed to. At least 75 Al-Shabaab fighters were killed by Somali troops and ASWJ forces. According to the Somali government, Al-Shabaab high-level commander Fuaad Shongole was killed in these clashes. Only [[Baardheere]] and Buur Dhuubo are still in Al-Shabaab's hands.<ref>{{cite web|author=Muqdisho |url=http://www.bar-kulan.com/2011/05/02/war-degdeg-ah-fuaad-shongale-oo-la-dilay/ |title=Maxamed Kaliil: Fu’aad Shongole waa dilnay, maydkiisana waan heynaa |publisher=Bar-kulan |date=2013-01-18 |accessdate=2013-04-30}}</ref>

=== May 3, 2011 ===
Somali troops ambushed Al-Shabaab militias and pushed the insurgents out of a village named Meykaareeb 19 kilometers west from [[Garbaharey]]. Government troops killed 15 Al-Shabaab insurgents during these operations and seized two pick-up cars from Al-Shabaab forces.<ref>{{cite web|author=Muqdisho |url=http://www.bar-kulan.com/2011/05/03/dagaal-ka-dhacay-meel-ka-baxsan-garbahaarrey |title=Dagaal ka dhacay meel ka baxsan Garbahaarrey |publisher=Bar-kulan |date=2013-01-18 |accessdate=2013-04-30}}</ref> On afternoon Al-Shabaab launched an ambush attack against Somali military killing 8 ASWJ soldiers and at least 10 Al-Shabaab fighters were also killed.<ref>[http://www.shabelle.net/article.php?id=6186 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}
In Garbaharey, three persons from one family were killed after a mortar shelling destroyed their home.<ref>{{cite web|url=http://www.raxanreeb.com/?p=94650 |accessdate=May 3, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110725072140/http://www.raxanreeb.com/?p=94650 |archivedate=July 25, 2011 }}</ref>

=== May 4, 2011 ===
In Garbaharey, at least 70 people, mainly the combatants, were killed in heavy fighting between Al-Shabaab forces and Somali soldiers.<ref>[http://www.shabelle.net/article.php?id=6221 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}

=== May 5, 2011 ===
Sheikh Abdi-aziz Abu Mus’ab, the spokesman of Al Ahabaab, claimed that the chairman on ASWJ in Gedo, Sheikh Hassan Sheikh Ahmed (Qoryoley), died from his wounds sustained from an ambush attack in Garbaharey. Sheikh Isaq Hussein, an Ahlu Sunna officer, confirmed the death.<ref>{{cite web|url=http://www.shabelle.net/article.php?id=6253 |accessdate=May 5, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20131005063951/http://shabelle.net/article.php/?id=6253 |archivedate=October 5, 2013 }}</ref>

=== May 6, 2011 ===
Nine people (six children and three adults) were killed by a landmine explosion in the Gedo region in an unspecified city.<ref>{{cite web|url=http://www.presstv.ir/detail/178630.html |title=PressTV - Landmine kills 9 people in Somalia |publisher=Presstv.ir |date=2011-05-07 |accessdate=2013-04-30}}</ref>

=== May 7, 2011 ===
Somali forces left the Gusar village following a problem on the payment of their salaries. Al-Shabaab forces took over the village after that.<ref>{{cite web|url=http://www.raxanreeb.com/?p=95256 |accessdate=May 8, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110725072252/http://www.raxanreeb.com/?p=95256 |archivedate=July 25, 2011 }}</ref>

=== May 10, 2011 ===
Witnesses in [[Beled Haawo]], [[Garbaharey]], [[Ceelwaaq]] and [[Luuq]] saw Somali troops accompanied by ASWJ forces leaving in the early morning and heading to [[Al-Shabaab (militant group)|Al-Shabaab]] strongholds. Later that day Somali forces seized four villages close to [[Baardheere]], Al-Shabaab's biggest and last stronghold in [[Gedo]]. Al-Shabaab did not put up a fight and withdrew once they saw that they were outnumbered by [[Military of Somalia|Somali Armed Forces]].<ref>http://kismaayo24.com/?p=10991</ref>

=== May 13, 2011 ===
[[Ahlu Sunna Waljama'a]] forces ambushed Al-Shabaab militias in a village near [[Garbaharey]] named Kalabeyr, Al-Shabaab fled the village after ASWJ forces killed 3 Al-Shabaab soldiers and seized some weapons from Al-Shabaab. [[Ahlu Sunna Waljama'a]] Maxamed Xuseen Al-Qaaddi told radio Shabelle that ASWJ forces are controlling Kalabeyr after Al-Shabaab lost the battle.<ref>{{cite web|url=http://shabelle.net/article.php?id=6498 |accessdate=May 13, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110521021139/http://shabelle.net/article.php?id=6498 |archivedate=May 21, 2011 }}</ref>

=== May 14, 2011 ===
In the village of Banmudalo near Luuq clashed beginning between Somali interim government troops and Al-Shabaab fighters. At least two people have been killed.<ref>[http://www.shabelle.net/article.php?id=6544 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}

=== May 15, 2011 ===
Al-Shabaab forces has pounded volleys of mortars to the town of Garbaharey, No deaths or injuries were reported. Some residents started to flee from their home against of back of new more fighting could again renew from the area.<ref>[http://www.shabelle.net/article.php?id=6596 ]{{dead link|date=April 2013}}</ref>{{dead link|date=February 2013}}

=== June 13, 2011 ===
Almost 1000 Somali troops finished training in [[Doolow]], these troops will be deployed in [[Gedo]] resuming the offensive.

===October 11, 2012===
Columns of armoured vehicles and military pick-up trucks carrying fresh Ethiopian troops have reportedly reached Somalia’s border town of [[Luq]] located in [[Gedo]] province. [[Somali National Army]] (SNA) commanders in the area say the Ethiopian troops set up military bases on the suburbs of the town, where they are planning major offensive against the remaining towns and locations controlled by [[al Qaeda]]-linked militants of [[Al-Shabaab (militant group)|Al-Shabaab]].<ref>{{cite web|url=http://somaliamediamonitoring.org/october-11-2012-daily-monitoring-report/ |title=October 11, 2012 &#124; Daily Monitoring Report. - AMISOM Daily Media Monitoring |publisher=Somaliamediamonitoring.org |date= |accessdate=2013-04-30}}</ref>

=== October 15, 2012 ===
Al-Shabaab fighters have been reported to making their last preparation to defend [[Bardera]] district of [[Gedo]] region in southern Somalia from the allied forces led by the Somali Military.<ref>{{cite web|url=http://somaliamediamonitoring.org/october-15-2012-daily-monitoring-report/ |title=October 15, 2012 &#124; Daily Monitoring Report. - AMISOM Daily Media Monitoring |publisher=Somaliamediamonitoring.org |date= |accessdate=2013-04-30}}</ref>

=== October 29, 2012 ===
The government has declared [[Luq]] town, [[Gedo]] region, and its environ safe and secure and urged aid agencies to open their offices in the area. Area district commissioner [[Abdullahi Kuredow]] said security in the district has been bolstered and now reliable. He said Somali forces backed by Ethiopian troops and their allied Ahlu Sunna militias are now in full control of the entire district.<ref>{{cite web|url=http://somaliamediamonitoring.org/october-29-2012-morning-headlines/ |title=October 29, 2012 &#124; Morning Headlines. - AMISOM Daily Media Monitoring |publisher=Somaliamediamonitoring.org |date=2012-10-29 |accessdate=2013-04-30}}</ref>

=== November 10, 2012 ===
At least two people have been killed in a fierce battle between [[Somali National Army]] (SNA) and Al-Shabaab militants in [[Gedo]] province, the latest in surge of attacks in the southwestern region. The violence reportedly erupted after heavily armed Al-Shabaab fighters launched a surprise assault on Somali forces at a checkpoint located on the outskirts of [[Garbaharey]], a town largely controlled by Somali government.<ref>{{cite web|author=maalik_eng |url=http://shabelle.net/two-killed-in-a-heavy-fighting-in-gedosouthwestern-somalia/ |title=Shabelle Media Network – Two killed in a heavy fighting in Gedo,Southwestern Somalia |publisher=Shabelle.net |date=2012-11-10 |accessdate=2013-04-30}}</ref>

=== January 2, 2013 ===
Reports from [[Bardhere]] town of [[Gedo]] region indicate the withdrawal of [[Al-Shabaab (militant group)|Al-Shabaab]] leadership from the town
Local residents told Bar-kulan that the town is in a state of panic as the Al-Shabaab fighters prepare for an assault from government and allied forces. Among the fleeing leaders are foreigners, the residents told Bar-kulan. Bardhere is remaining stronghold of Al-Shabaab militants.
The town is approached by government troops and [[Kenya Defence Forces]].<ref>{{cite web|url=http://somaliamediamonitoring.org/january-2-2013-daily-monitoring-report/ |title=January 2, 2013 &#124; Daily Monitoring Report. - AMISOM Daily Media Monitoring |publisher=Somaliamediamonitoring.org |date= |accessdate=2013-04-30}}</ref>

==References==
{{Reflist|2}}

{{War in Somalia (2009–)}}

{{coord missing|Somalia}}

{{DEFAULTSORT:Gedo }}
[[Category:2010s conflicts]]
[[Category:Battles of the War in Somalia (2009–present)|Gedo]]
[[Category:2010s in Somalia]]