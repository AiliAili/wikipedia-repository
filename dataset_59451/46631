{{Orphan|date=July 2014}}
{{Expand German|Thomas Borer|date=July 2014}}

'''Thomas Borer''' (born 1957) is a former diplomat of Switzerland. He had a role in resolving the problems created by [[Banking in Switzerland#Law and regulation|Swiss banking secrecy laws]] in protecting the [[Nazi plunder|Nazi seizure of Jewish assets]] during the [[Holocaust]], and in the use of Swiss banks as a medium for [[Tax evasion in Switzerland|tax evasion]].

== Biography ==

Thomas Borer was born in 1957 and studied law at the [[University of Basel]] where he obtained his doctorate [[summa cum laude]] in 1985.{{citation needed|date=July 2014}} His thesis focused  on the principle of legality and foreign affairs.

== Career ==

In 1987, Borer joined the Swiss Department of Foreign Affairs (EDA) as a diplomat. He was posted to [[Bern]] (Department for International Law, 1987 and 1989–93), Lagos (1987/88) and Geneva (1988/89). In 1993, he was appointed Embassy Secretary for Legal and Political Affairs at the Swiss Embassy in [[Washington, D.C.|Washington]], USA.<ref name="osce">{{cite web|url=http://www.osce.org/eea/42179|title=Strengthening the OSCE's Role in the Realm of Environment and Security (OSCE Follow-up Seminar to the 7th and 8th Economic Forum)|date=2001-07-04}}</ref><ref>{{cite web|url= https://www.nytimes.com/1993/11/11/nyregion/us-and-switzerland-to-share-seized-22-million.html|title= U.S. and Switzerland to Share Seized $22 Million|website=The New York Times|date=1993-11-11}}</ref> On 21 December 1994, he was appointed Deputy General Secretary of the EDA by the [[Swiss Federal Council]]. In this role, he led the personnel, technology, logistics, finance and administrative law departments. In addition, he was responsible for the reorganisation of the EDA and the Swiss Representative Network Abroad.<ref name="osce" />

On 25 October 1996 the Swiss Federal Council appointed him Ambassador and Head of the “Switzerland – [[World War II|Second World War]]” Task Force, which examined the role of Switzerland as a financial centre during the Nazi era. The Task Force contributed substantially to settle a lawsuit  filed by the [[World Jewish Congress]] against Swiss banks in 1995. As a consequence on August 12, 1998, the Swiss banks [[UBS]] and [[Credit Suisse]] reached an agreement with the [[World Jewish Congress]] and agreed to pay $1.25 billion to [[Victims of the Holocaust|victims of the holocaust]] or their heirs.  Borer gained international recognition for his commitment in this process.<ref>{{cite web|url=http://www.swissinfo.ch/eng/swiss_news/Looking_back_at_the_Holocaust_assets_controversy.html?cid=36756580|title=Looking back at the Holocaust assets controversy|date=2013-09-03}}</ref><ref>{{cite web|url=http://news.bbc.co.uk/2/hi/35938.stm|title=Swiss confirmed as main Nazi bankers|website=BBC News World|date=1997-12-01}}</ref><ref>{{cite web|url=http://www.wiesenthal.com/site/pp.asp?c=lsKWLbPJLnF&b=4441321 |title=Swiss Role in WWII |website=Simon Wiesenthal Center|date=1998-06-17}}</ref><ref>{{cite web|url=http://articles.baltimoresun.com/1997-12-03/news/1997337005_1_holocaust-swiss-borer |title=Swiss deny owing Jews compensation for Nazi loot Jewish leader seeks billions of dollars for Holocaust victims |website=The Baltimore Sun|date=1997-12-03}}</ref><ref>{{cite web|url=http://www.jewishpress.com/indepth/columns/louis-bene-beres/switzerland-and-the-jews-a-realistic-assessment/2013/06/19/ |title=Switzerland and the Jews: A Realistic Assessment |website=The Jewish Press|date=2013-06-19}}</ref>

On 31 March 1999, the Swiss Federal Council named Borer as Swiss Ambassador to the [[Federal Republic of Germany]]. At the end of April 2002, he left public service and returned to Switzerland.<ref>{{cite web|url=http://www.time.com/time/magazine/article/0,9171,344591,00.html |title=Boring, He's Not |website=Time Magazine |date=2002-08-27}}</ref> Shortly after he became strategic consultant for Russian oligarch [[Viktor Vekselberg]],<ref>{{cite web|url=http://www.spiegel.de/international/business/we-are-not-the-bad-guys-switzerland-to-loosen-bank-secrecy-a-611033.html |title='We Are not the Bad Guys': Switzerland To Loosen Bank Secrecy |website=Spiegel online International |date=2009-03-03}}</ref> and in 2005 a member of the Board of Directors of [[Renova Group]], Viktor Vekselberg’s company.<ref>{{cite web|url=http://www.businessmir.ch/?p=98&language=en |title=Viktor Vekselberg |website=Business Mir |date=May 2007}}</ref>

In the German and international news media Borer is a popular speaker and commentator on economic issues and on Swiss-German relations, particularly on Switzerland as a financial center and on the banking secrecy.<ref>{{cite web|url=http://www.spiegel.de/wirtschaft/soziales/schweizer-steuerabkommen-borer-fordert-ende-des-bankgeheimnisses-a-868544.html |title=Steuerabkommen: Ex-Botschafter Borer fordert Ende des Bankgeheimnisses |website=Spiegel online |date=2012-11-22}}</ref><ref>{{cite web|url=http://www.telegraph.co.uk/finance/newsbysector/banksandfinance/2787389/Swiss-vow-to-keep-banking-secrecy-laws.html |title=Swiss vow to keep banking secrecy laws |website=The Telegraph |date=2008-04-02}}</ref><ref>{{cite web|url=http://www.srf.ch/player/video?id=d9876408-0cad-4d15-bfc7-20ad9b7d68fc |title=WEF 2012: Interview with Thomas Borer |website=SRF Swiss Radio and Television |date=2012-01-26}}</ref><ref>{{cite web|url=http://www.bild.de/politik/ausland/schweiz/was-sie-uns-voraus-hat-34697662.bild.html |title=Was die Schweiz uns voraus hat |website=Bild online |date=2014-02-16}}</ref>

== Publications ==

* ''Das Legalitätsprinzip und die auswärtigen Angelegenheiten.'' Helbing und Lichtenhahn. Basel/Frankfurt am Main. 1986.
*"Schweizerische Neutralität auf dem Prüfstand – Schweizerische Aussenpolitik zwischen Kontinuität und Wandel". Bern. 1992.
*"Switzerland and the European Economic Union". Washington. 1993.
*"Die bewaffnete Neutralität der Schweiz". Thun. 1996.
*"Struktur und Arbeitsweise des EDA im Wandel". Revue d’Allemagne. 1996.
*"The role of Switzerland as financial center during World War II, The United States House of Representatives Committee on Banking and Financial Services". Washington. 1996.
*"Switzerland – Second World War, London Conference on Nazi Gold". London. 1997.
*"Informationsführung in einer Krisenlage". Aargau. 1998.
*"Holocaust Era Assets, Looted Art, the Swiss perspective", Washington Conference on Holocaust Era Assets. J.D. Bindenagel. Washington. 1999.
* ''Die Auseinandersetzung Schweiz – Zweiter Weltkrieg: Ein neuer Typ politischer Risiken für Unternehmen und die Lehren für die Zukunft?'' (at the Symposium on Management of political Risks at Financial Institutions, Universität of St. Gallen). Maerki Baumann. Zürich. 2001.
*"Symposium zum Management politischer Risiken in Finanzinstitutionen". Universität St. Gallen. 2001.
*"Das Erscheinungsbild der Schweiz im Ausland und die „öffentliche Diplomatie"". Coutts Zürich. 2002.
*"Die Uhren gehen anders – Gedanken zu Kommunikation und Zusammenarbeit zwischen Wirtschaft und Politik, 19. Deutscher Logistik-Kongress". Berlin. 2002.
*"Grenzüberschreitendes Finanzdienstleistungsgeschäft: Schweizer Finanzdienstleistungen auf dem deutschen und europäischen Markt". Friends of Funds. Zürich. 2003.
*"Public Affairs". Econ Verlag. München. 2003.
*"Why we Swiss are ‘staying out’". Financial Times. 2004.
*"Russland ist anders, als man es zu kennen glaubt. Ein Plädoyer für den fairen Umgang". Weltwoche. 9 July 2008 
*"Ein Land mit dem Gesicht nach Westen: Warum Russland für Schweizer Investoren spannend bleibt". Handelszeitung. 17 June 2009. 
*"Die Konkordanz fördert Durchschnittliche". Neue Zürcher Zeitung. 12 Januar 2012.

== Further reading ==

* Oliver Zihlmann, Philippe Pfister: ''Der Fall Borer. Fakten und Hintergründe eines Medienskandals.'' Werd Verlag. Zürich. 2003.

== References ==

{{reflist}}

<!--- Categories --->

{{Authority control}}
{{DEFAULTSORT:Borer, Thomas}}
[[Category:1957 births]]
[[Category:Living people]]
[[Category:University of Basel alumni]]
[[Category:Swiss diplomats]]