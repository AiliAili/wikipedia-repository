{{for|the ethical subject|Environmental ethics}}
{{Infobox journal
| title = Environmental Ethics
| cover = [[File:enviroethics-cover.gif]]
| editor = Eugene C. Hargrove
| discipline = [[Environmental ethics]]
| formernames = 
| abbreviation = Environ. Ethics
| publisher = Environmental Philosophy
| country = United States
| frequency = Quarterly
| history = 1979-present
| openaccess = 
| license = 
| impact = 0.283
| impact-year = 2015
| website = http://www.pdcnet.org/enviroethics
| link1 = http://www.pdcnet.org/enviroethics/toc
| link1-name = Journal contents with abstracts
| JSTOR = 
| OCLC = 4372676
| LCCN = 79-644567
| CODEN = ENETDD
| ISSN = 0163-4275
| eISSN = 2153-7895
}}
'''''Environmental Ethics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering the study of philosophical aspects of environmental problems. It was established in 1979. The [[editor-in-chief]] is Eugene Hargrove and it is published by the [[Center for Environmental Philosophy]] ([[University of North Texas]]). All issues are available online from the [[Philosophy Documentation Center]].

== Abstracting and indexing ==
''Environmental Ethics'' is abstracted and indexed in:
{{Div col|2}}
* [[Abstracts in Environmental Management]]
* [[Academic Search Premier]]
* [[AgrIndex]]
* [[ATLA Religion Database]]
* [[Bibliography of Agriculture]]
* [[BIOSIS Previews]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[Current Philosophy]]
* [[Dow Jones Insight]]
* [[Ecology Abstracts]]
* [[Energy Abstracts]]
* [[Environment Abstracts]]
* [[Environment Index]]
* [[Environmental Engineering Abstracts]]
* [[Environmental Periodicals Bibliography]]
* [[Environmental Science and Pollution Management]]
* [[Expanded Academic ASAP]]
* [[Factiva]]
* [[Humanities International Index]]
* [[International Bibliography of Book Reviews of Scholarly Literature]]
* [[International Bibliography of Periodical Literature]]
* [[International Philosophical Bibliography]]
* [[MEDLINE]]
* [[Periodica Islamica]]
* [[Philosopher's Index]]
* [[Philosophy Research Index]]
* [[PhilPapers]]
* [[Political Science Abstracts]]
* [[Pollution Abstracts]]
* [[ProQuest]] 5000
* [[Public Affairs Index]]
* [[Referativny Zhurnal]]
* [[Religious and Theological Abstracts]]
* [[Safety Science Abstracts]]
* [[Scopus]]
* [[SocINDEX]]
* [[Social Science Citation Index]]
* [[Sociological Abstracts]]
* [[Wildlife Review]]
* [[Wilson OmniFILE]]
* [[The Zoological Record]]
{{Div col end}}

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.283, ranking it 46th out of 51 journals in the category "Ethics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Ethics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[Environmental philosophy]]
* [[List of ethics journals]]
* [[List of philosophy journals]]
* [[List of environmental journals]]

==References==
{{Reflist}}

== External links ==
* {{Official|http://www.pdcnet.org/enviroethics}}

[[Category:English-language journals]]
[[Category:Environmental ethics]]
[[Category:Publications established in 1979]]
[[Category:Quarterly journals]]
[[Category:Ethics journals]]
[[Category:Environmental studies journals]]
[[Category:Environmental humanities journals]]