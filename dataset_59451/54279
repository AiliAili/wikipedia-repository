{{for|the British Conservative Member of Parliament|Joan Hall (UK politician)}}
{{Use dmy dates|date=August 2016}}
{{Use Australian English|date=August 2016}}
'''Joan Lynette Hall''' ([[née]] '''Bullock''') (born 22 December 1946) is a former member of the [[South Australian House of Assembly]], serving in the [[electoral district of Coles]] from 1993 to 2002 and the renamed [[electoral district of Morialta]] from 2002 to 2006.

The wife of former [[Premier of South Australia|Premier]], [[Liberal Movement (Australia)|Liberal Movement]] leader, and [[Australian Senator]] [[Steele Hall]], she met Hall while working as his [[parliamentary secretary]] during the 1960s and 1970s. Later, she was a staffer to Premier [[Dean Brown]] before entering parliament as the member for the [[Adelaide Hills]] seat of Coles at the [[South Australian state election, 1993|1993 election]].

A moderate like her husband, Hall felt chagrin that Brown did not promote her to the ministry after the Liberals' landslide 1993 victory.<ref name=Crikey>Kingston, Charles Cameron. [http://web.archive.org/web/20041029025315/http://www.crikey.com.au/politics/2001/10/21-olsen.html The unluckiest politician in Australia]. [[Crikey]], 2001-10-21.</ref>  When Industry Minister [[John Olsen]], leader of the conservative wing of the state Liberal Party, decided to challenge Brown's leadership, Hall threw her support to him, giving Olsen the numbers to successfully challenge Brown for the Premiership.  Under Olsen, she was Minister for Youth and Employment from December 1997, then Minister for Tourism from October 1998. She won party pre-selections in 2002 and 2006 despite claims of "interference in a preselection by Federal Members" by both herself and her husband.<ref>{{cite web
  | title =Morialta
  | work =
  | publisher =The Poll Bludger
  | date =
  | url =http://www.pollbludger.com/sa2006/morialta.htm
  | accessdate = 2007-10-06 }}</ref>

Whilst serving as Tourism Minister, Hall was involved in bringing the [[Tour Down Under]], the [[Clipsal 500]], The Le Mans Race of 1000 years, the [[National Wine Centre]] and the 2007 Police and Fire Games to [[South Australia]]. In October 2001, she resigned from the ministry due to a conflict of interest in her handling of the [[Hindmarsh Stadium|Hindmarsh Soccer Stadium]] redevelopment.<ref>{{cite news 
  | title =SA soccer stadium scandal claims two ministers
  | work =[[The 7.30 Report]]
  | publisher =[[ABC Television|ABC TV]]
  | date = 4 October 2001
  | url =http://www.abc.net.au/7.30/content/2001/s382728.htm
  | accessdate = 2007-10-06 }}</ref>

On a margin of 4 percent from the previous election, Hall suffered a 12 percent swing in Morialta at the [[South Australian state election, 2006|2006 election]], which saw her lose the seat to [[Australian Labor Party|Labor]] candidate [[Lindsay Simmons]]. Moderate candidate [[John Gardner (Australian politician)|John Gardner]] reclaimed the seat for the Liberals at the [[South Australian state election, 2010|2010 election]] with an 11 percent swing.

==Notes==
{{Reflist}}

==External links==
*[http://www.parliament.sa.gov.au/Internet/DesktopModules/memberdrill.aspx?pid=545 Parliamentary Profile]
*[http://www.pollbludger.com/sa2006/morialta.htm Poll Bludger - Morialta electorate]

{{DEFAULTSORT:Hall, Joan}}
[[Category:Liberal Party of Australia members of the Parliament of South Australia]]
[[Category:1946 births]]
[[Category:Living people]]
[[Category:Members of the South Australian House of Assembly]]
[[Category:Australian women in politics]]
[[Category:21st-century Australian politicians]]
[[Category:21st-century women politicians]]