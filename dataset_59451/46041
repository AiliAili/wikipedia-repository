{{italic title}}
{{automatic taxobox
|name = ''Adina''
|image = Adina rubella - Villa Taranto (Verbania) - DSC03796.JPG
|image_caption = ''[[Adina rubella]]''
|display parents = 2
|taxon = Adina
|authority = [[Richard Anthony Salisbury|Salisb.]]
|type_species = ''[[Adina pilulifera]]''
|type_species_authority = ([[Jean-Baptiste Lamarck|Lam.]]) [[Adrien René Franchet|Franch.]] ex [[Emmanuel Drake del Castillo|Drake]]
|}}
{{Commons category|Adina}}
'''''Adina''''' is a [[genus]] of 7 species of [[flowering plant]]s in the [[Rubiaceae]] [[Family (biology)|family]]. They are [[shrub]]s or small [[tree]]s, native to [[East Asia]] and [[Southeast Asia]].<ref name="Mabberley2008">{{cite book|author=Mabberley DJ|year=2008|title=Mabberley's Plant Book|edition=3|publisher=Cambridge University Press|ISBN=978-0-521-82071-4}}</ref>

== Description ==
''Adina'' is a genus of shrubs and small trees. The terminal vegetative buds are inconspicuous and loosely surrounded by the stipules. The stipules are bifid for at least 2/3 of their length. The corolla lobes are nearly valvate in bud, being subimbricate at the apex. The anthers are basifixed and introrse. The ovary has two locules, with up to four ovules per locule.<ref name="ridsdale1978">{{cite journal|author=Ridsdale CE|year=1978|title=A revision of the tribe Naucleeae s.s. (Rubiaceae)|journal=Blumea|volume=24|issue=2|pages=307–366}}</ref>

==Taxonomy==
''Adina'' was [[Botanical name|named]] by [[Richard Salisbury]] in 1807 in his book, ''[[The Paradisus Londinensis]]''.<ref name="salisbury1807">{{cite book|author=Salisbury RA|year=1807|title=The Paradisus Londinensis: Containing plants cultivated in the vicinity of the metropolis|volume=1}}</ref> The [[Genus#Generic name|genus name]] is [[Etymology|derived]] from the [[Ancient Greek]] word ''adinos'', meaning "clustered, crowded". It refers to the tightly clustered heads of flowers.<ref name="Quattrocchi2000">{{cite book|author=Quattrocchi U|year=2000|title=CRC World Dictionary of Plant Names|volume=1|publisher=CRC Press|location=Boca Raton, New York, Washington DC, London|ISBN=978-0-8493-2675-2}}</ref> The [[biological type]] for ''Adina'' consists of the [[Biological specimen|specimen]]s that Salisbury called ''Adina globiflora''.<ref name="ingadina">''Adina'' In: Index Nominum Genericorum. In: [[Regnum Vegetabile]].</ref> These are now included in the species ''[[Adina pilulifera]]''.<ref name="ridsdale1978"/> [[Molecular phylogenetic]] [[Research|studies]] have shown that ''Adina'' is [[paraphyletic]] over ''[[Adinauclea]]'', a [[monospecific]] genus [[Range (biology)|from]] [[Sulawesi]] and the [[Moluccas]].<ref name="Manns2010">{{cite journal|author=Manns U, Bremer B|year=2010|title=Towards a better understanding of intertribal relationships and stable tribal delimitations within Cinchonoideae s.s. (Rubiaceae)|journal=Molecular Phylogenetics and Evolution|volume=56|pages=21–39|url=http://www.bergianska.se/pub/publikationer/Manns/Manns_Bremer_2010.pdf | doi = 10.1016/j.ympev.2010.04.002 | pmid= 20382247}}</ref>

== Species ==
Species currently accepted as of April 2014:<ref>[http://apps.kew.org/wcsp/qsearch.do Kew World Checklist of Selected Plant Families]</ref>

*''[[Adina cordifolia]]'' <small>(Roxb.) Brandis</small> - from [[India]] to [[Yunnan]] to Peninsular [[Malaysia]]
* ''[[Adina dissimilis]]'' <small>Craib</small> -  [[Thailand]] and Peninsular [[Malaysia]]
*''[[Adina fagifolia]]'' <small>(Teijsm. & Binn. ex Havil.) Valeton ex Merr</small> - [[Sulawesi]] and [[Maluku (province)|Maluku]]
* ''[[Adina pilulifera]]'' <small>([[Jean-Baptiste Lamarck|Lam.]]) [[Adrien René Franchet|Franch.]] ex [[Emmanuel Drake del Castillo|Drake]]</small> - [[Japan]], [[China]], [[Vietnam]]
* ''[[Adina pubicostata]]'' <small>[[Elmer Drew Merrill|Merr.]]</small> - [[Hunan]], [[Guangxi]], [[Vietnam]]
* ''[[Adina rubella]]'' <small>Hance</small> - [[China]], [[Korea]]
*''[[Adina trichotoma]]'' <small>(Zoll. & Moritzi) Benth. & Hook.f. ex B.D.Jacks.</small> - from [[Assam]] to southern [[China]] south to [[Java]] and [[New Guinea]]

== References ==
{{Reflist}}

== External links ==
*[http://apps.kew.org/wcsp/rubiaceae/ World Checklist of Rubiaceae]
* [http://www.ipni.org/ipni/advPlantNameSearch.do;jsessionid=D5C5ED8944E5B6EA7C8E154F9A59516A?find_family=&find_genus=Adina+&find_species=&find_infrafamily=&find_infragenus=&find_infraspecies=&find_authorAbbrev=&find_includePublicationAuthors=on&find_includePublicationAuthors=off&find_includeBasionymAuthors=on&find_includeBasionymAuthors=off&find_publicationTitle=&find_isAPNIRecord=on&find_isAPNIRecord=false&find_isGCIRecord=on&find_isGCIRecord=false&find_isIKRecord=on&find_isIKRecord=false&find_rankToReturn=all&output_format=normal&find_sortByFamily=on&find_sortByFamily=off&query_type=by_query&back_page=plantsearch ''Adina''] At: [http://www.ipni.org/ipni/plantnamesearchpage.do Plant Names] At: [http://www.ipni.org/ IPNI]
* [https://books.google.com/books?id=KJUUAAAAYAAJ&lpg=PT156&ots=txo816qJt2&dq=Paradisus%20Londinensis%20v.1&pg=PT481#v=onepage&q=Paradisus%20Londinensis%20v.1&f=false ''Adina''] In: [https://books.google.com/books?id=KJUUAAAAYAAJ&lpg=PT156&ots=txo816qJt2&dq=Paradisus%20Londinensis%20v.1&pg=PT9#v=onepage&q=Paradisus%20Londinensis%20v.1&f=false ''The Paradisus Londinensis''] At: [https://books.google.com/books?searchword=Adina ''Adina''] At:[http://www.botany.si.edu/ing/ Index Nominum Genericorum] At: [http://www.botany.si.edu/index.htm?references References] At: [http://www.botany.si.edu/index.htm NMNH Department of Botany] At: [http://www.mnh.si.edu/rc/ Research and Collections] At: [http://www.mnh.si.edu/ Smithsonian National Museum of Natural History]
* [http://www.ars-grin.gov/cgi-bin/npgs/html/genus.pl?210 ''Adina''] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/gnlist.pl?975 List of Genera] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/family.pl?975 Rubiaceae] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/famlist.pl List of families] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/taxfam.pl?language=en Families and Genera in GRIN] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/queries.pl?language=en Queries] At: [http://www.ars-grin.gov/cgi-bin/npgs/html/index.pl GRIN taxonomy for plants]

[[Category:Rubiaceae genera]]
[[Category:Naucleeae]]
[[Category:Flora of Asia]]