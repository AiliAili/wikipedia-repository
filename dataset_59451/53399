{{Use dmy dates|date=September 2012}}
{{Use Australian English|date=September 2012}}
{{Infobox cricketer
|          name = Keith Bradshaw
|               image = 
|             country = Australia
|            fullname = Keith Bradshaw
|          birth_date = {{Birth date and age|1963|10|2|df=yes}}
| birth_place = [[Hobart]], [[Tasmania]], [[Australia]]
|            nickname = 
|            heightft = 
|          heightinch = 
|             heightm = 
|             batting = Right-hand batsman
|             bowling = Right-arm medium pace
|                role = 
|              family = 
|               club1 = [[Tasmanian Tigers|Tasmania]]
|               year1 = 1984&ndash;1988
|         clubnumber1 = 
|             columns = 2
|             column1 = [[First-class cricket|First-class]]
|            matches1 = 25
|               runs1 = 1,083
|            bat avg1 = 25.78
|           100s/50s1 = 2/4
|          top score1 = 121
|         deliveries1 = 738
|            wickets1 = 9
|           bowl avg1 = 48.77
|            fivefor1 = 0
|             tenfor1 = 0
|       best bowling1 = 3/81
|  catches/stumpings1 = 12/&ndash;
|             column2 = [[List A cricket|List A]]
|            matches2 = 9
|               runs2 = 180
|            bat avg2 = 20.00
|           100s/50s2 = 0/0
|          top score2 = 43
|         deliveries2 = 150
|            wickets2 = 4
|           bowl avg2 = 33.50
|            fivefor2 = 0
|             tenfor2 = 0
|       best bowling2 = 1/15
|  catches/stumpings2 = 7/&ndash;
|                date = 19 August
|                year = 2010
|              source = http://www.cricketarchive.com/Archive/Players/2/2410/2410.html Cricket Archive
}}
'''Keith Bradshaw''' (born [[Hobart, Tasmania]] 2 October 1963) is an [[Australian]] [[cricketer]], [[accountant]] and [[Administration (business)|administrator]].

He was a right-handed batsman who played in 25 first-class and nine List A limited-over matches for [[Tasmania]] between 1984–85 and 1987-88 without fulfilling his early promise. In his fifth match, he scored 121 against [[Queensland]]. He represented [[Sussex CCC|Sussex]] in 1986 at Second XI and Under-25 level, without breaking into the 1st XI. He lost his place in the Tasmanian team during the 1986-87 season and retired after making a final appearance during the following season. He deputised for the State Captain [[David Boon]], during his absence, while on international duty.

He returned to the [[University of Tasmania]] to complete his studies in 1988. Upon qualifying as an accountant, he forged a successful business career with [[Price Waterhouse]] and [[Deloitte]] rising to be a [[Partner (business rank)|Partner]].  

In 2001, after a brief hiatus from competitive cricket, he made a final comeback with the [[Derwent Cricket Club]] in the [[Cricket Tasmania|Southern Tasmanian Cricket League]], which lasted until the 2005-06 season.

He was appointed as the Secretary & Chief Executive by the [[Marylebone Cricket Club]] on 30 January 2006, succeeding the retiring [[Roger Knight]] [[OBE]] in October 2006.

Whilst Chief Executive of the MCC Bradshaw had a place on the administrative board of the [[England and Wales Cricket Board]] and it has been reported that he had some say in the removal from office of England Head Coach [[Duncan Fletcher]] [[OBE]] (now [[India]] Head Coach) in April 2007. [http://news.bbc.co.uk/sport2/hi/cricket/england/6608545.stm]. 

The MCC accepted his resignation as Secretary in August 2011. Bradshaw was awarded Honorary Life Membership.

In November 2011 it was announced that [[South Australian Cricket Association]] (SACA) had appointed Bradshaw as its next [[CEO]].<ref>[http://www.abc.net.au/news/2011-11-18/saca-ceo-keith-bradshaw-john-harnden/3679096?section=sa www.abc.net.au]</ref>

==See also==
* [[List of Tasmanian representative cricketers]]

== References ==
{{reflist}}

==External links==
* [http://www.cricinfo.com Cricinfo]
* [http://www.cricketarchive.com Cricket Archive]
* [http://www.lords.org Marylebone Cricket Club]

{{DEFAULTSORT:Bradshaw, Keith}}
[[Category:1963 births]]
[[Category:Living people]]
[[Category:Australian accountants]]
[[Category:Australian cricketers]]
[[Category:Australian cricket administrators]]
[[Category:Secretaries of the Marylebone Cricket Club]]
[[Category:Sportspeople from Hobart]]
[[Category:Tasmania cricketers]]
[[Category:Cricketers from Tasmania]]