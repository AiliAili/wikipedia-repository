{{good article}}
{{Infobox Simpsons episode
|episode_name=Bart Carny
|episode_no=190
|prod_code=5F08
|airdate=January 11, 1998
|show runner=[[Mike Scully]]
|writer=[[John Swartzwelder]]
|director=[[Mark Kirkland]]
|blackboard=
|couch_gag=As the family goes to sit down, the couch gets pulled back. Nelson appears from behind the couch, saying "Ha-Ha".<ref name=bbc>{{cite web |url=http://www.bbc.co.uk/cult/simpsons/episodeguide/season9/page12.shtml |title=Bart Carny |accessdate=2007-11-05 |author1=Martyn, Warren |author2=Wood, Adrian |year=2000 |publisher=BBC}}</ref>
|guest_star=[[Jim Varney]] as Cooder
|image=
|image_caption=
|commentary=[[Matt Groening]]<BR>Mike Scully<br />[[George Meyer]]<BR>Mark Kirkland
|season=9
|}}

"'''Bart Carny'''" is the twelfth episode of ''[[The Simpsons]]''<nowiki>'</nowiki> [[The Simpsons (season 9)|ninth season]]. It originally aired on the [[Fox Broadcasting Company|Fox network]] in the United States on January 11, 1998.<ref name="officialsite">{{cite web |url=http://www.thesimpsons.com/#/recaps/season-9_episode-12 |title=Bart Carny |accessdate=2011-09-24 |publisher=The Simpsons.com}}</ref> [[Homer Simpson|Homer]] and [[Bart Simpson|Bart]] start working at a carnival and befriend a father and son duo named Cooder and Spud. It was written by [[John Swartzwelder]], directed by [[Mark Kirkland]] and guest stars [[Jim Varney]] as Cooder the [[carny]].<ref name=bbc/> The episode contains several cultural references and received a generally mixed critical reception.

==Plot==
When [[Marge Simpson|Marge]] unsuccessfully tries to get the kids to clean up the backyard, [[Homer Simpson|Homer]] runs into the house to exclaim to the family that the [[carnival]] is in town. After trying some rides, [[Bart Simpson|Bart]] gets himself into trouble by crashing a display of [[Adolf Hitler|Hitler]]'s [[limousine]] into a tree. To repay the loss, Bart and Homer become carnies.

They meet up with carnies Cooder and his son, Spud. Cooder asks Homer to run his fixed game, but Homer fails to bribe [[Chief Wiggum]], and Cooder's game is shut down. Feeling guilty, Homer invites Cooder and Spud to stay at the Simpson residence, much to Marge's dismay.

To express their gratitude, the Cooders give the Simpsons tickets on a [[glass-bottom boat]] ride. When the Simpsons return, they find that the locks have been changed, the windows are all boarded up, and the Simpsons' name is crossed off the mailbox and replaced by "The Cooders". The family is forced to take up residence in Bart's [[treehouse]].

Homer proposes to Cooder, that if he can throw a [[hula hoop]] onto the [[chimney]], they get their house back. If he misses, he will sign the deed over to Cooder. Cooder agrees and steps onto the lawn to watch Homer's attempt. Homer stretches and warms up, as if about to throw, but instead he and his family suddenly rush into the house, leaving Cooder and Spud dumbfounded.<ref name="Book">{{cite book
|last=Gimple |first=Scott M. |title=[[The Simpsons Forever!: A Complete Guide to Our Favorite Family ...Continued]] |publisher=[[HarperCollins]] |date=December 1, 1999 |isbn=978-0-06-098763-3}}</ref>

==Production==
[[File:Big E fair.jpg|right|thumb|[[The Big E]] Fair was the inspiration for this episode.]]
The carnival in this episode is based on [[The Eastern States Exposition]] (currently known as The Big E) fair.<ref name=BartCarnyDVD>{{cite video |people=Scully, Mike |date=2006 |title=The Simpsons The Complete Ninth Season DVD commentary for the episode "Bart Carny" |medium=DVD |publisher=20th Century Fox}}</ref> As a child, Mike Scully went to the fair, and had hoped one day to be a carny.<ref name="BartCarnyDVD"/> This is the only episode that Mark Kirkland did not tell his parents to watch.<ref name=BartCarnyDVD2>{{cite video |people=Kirkland, Mark |date=2006 |title=The Simpsons The Complete Ninth Season DVD commentary for the episode "Bart Carny" |medium=DVD |publisher=20th Century Fox}}</ref> This is due to Bart's line "Out of my way, I'm Hitler". Kirkland's stepfather was a lieutenant in World War II and was injured while in combat. Cooder was modeled after [[David Mirkin]], the showrunner of [[The Simpsons (season 5)|seasons five]] and [[The Simpsons (season 6)|six]] and co-writer and the executive producer of two episodes in the [[The Simpsons (season 9)|ninth season]].<ref name=BartCarnyDVD3>{{cite video |date=2006 |title=The Simpsons The Complete Ninth Season DVD commentary for the episode "Bart Carny" |medium=DVD |publisher=20th Century Fox}}</ref> Spud's head shape is modeled after Bart's head. The "[[Fisheye lens|fisheye effect]]", when Cooder is looking through the peep hole was drawn by hand, not optically by assistant director [[Matthew Nastuk]]. Matt Groening said they had several endings worked out, including one where Homer made the [[hula hoop]] over the chimney.<ref name=BartCarnyDVD/>

==Cultural references==
When Homer and Bart talk through their teeth while holding the chickens, it is a reference to [[Bob Hope]] and [[Bing Crosby]] films.<ref name="BartCarnyDVD3"/> Some of the prizes for the ring toss game are a [[Def Leppard]] mirror,<ref name="BartCarnyDVD3"/> a [[Rubik's Cube]], and a [[Magic 8-Ball]].<ref name="book">{{cite book |last=Groening |first=Matt |authorlink=Matt Groening |editor1-first=Ray |editor1-last=Richmond |editor1-link=Ray Richmond |editor2-first=Antonia |editor2-last=Coffman |title=[[The Simpsons episode guides#The Simpsons: A Complete Guide to Our Favorite Family|The Simpsons: A Complete Guide to Our Favorite Family]]  |edition=1st |year=1997 |location=New York |publisher=[[HarperPerennial]] |lccn=98141857 |ol=433519M |oclc=37796735 |isbn=978-0-06-095252-5 |page=25 |ref={{harvid|Richmond & Coffman|1997}}}}.</ref> The song being played, at the end when Homer fixes his "ass groove" is "[[Groove Me]]" by [[King Floyd]].<ref name="BartCarnyDVD3"/>

==Reception==
In its original broadcast, "Bart Carny" finished 13th in ratings for the week of January 5–11, 1998, with a [[Nielsen ratings|Nielsen rating]] of 11.9, equivalent to approximately 11.7 million viewing households, making it the highest rated episode of Season 9. It was tied with ''[[King of the Hill]]'' as the second highest-rated show on the Fox network that week, following ''[[The X-Files]]''.<ref>{{cite news |title=NBC reclaims Nielsen ratings title |work=Sun-Sentinel |author=Associated Press |page=4E |date=January 15, 1998}}</ref>

The authors of the book ''I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide'', Warren Martyn and Adrian Wood, called it "one of the most dismally unfunny episodes ever, lifted only by the brief appearance of a talking camel and Homer's clever way of getting Cooder and Spud out of his home. Whereas most of the series' politically incorrect moments are funny and well-observed, this episode seems to be saying that fairground folk and travelers really are deeply unpleasant criminals who are both irredeemable and unworthy of help. Nasty-taste-in-the-mouth time."<ref name=bbc/> Despite the negative review from ''I Can't Believe It's a Bigger and Better Updated Unofficial Simpsons Guide'', Isaac Mitchell-Frey of the ''[[Herald Sun]]'' described the episode as "brilliant", and highlighted it along with episodes "[[The Trouble with Trillions]]" and "[[The Joy of Sect]]" and it has been described by the other ''Simpsons'' writers in the DVD audio commentary as "criminally underrated".<ref>{{cite news |last=Mitchell-Frey |first=Isaac |title=Comedy - The Simpsons, Series 9 |work=[[Herald Sun]] |page=E12 |date=February 11, 2007 }}</ref>

==References==
{{Reflist}}

==External links==
{{wikiquote|The_Simpsons#Bart_Carny_.5B9.12.5D|"Bart Carny"}}
{{portal|The Simpsons}}
*[http://www.thesimpsons.com/#/recaps/season-9_episode-13 "Bart Carny"] at The Simpsons.com
*{{snpp capsule|5F08}}
*[http://www.tv.com/shows/the-simpsons/bart-carny-1475/ "Bart Carny"] at TV.com

{{The Simpsons episodes|9}}

[[Category:The Simpsons (season 9) episodes]]
[[Category:1998 American television episodes]]
[[Category:Screenplays by John Swartzwelder]]