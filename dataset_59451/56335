{{Infobox journal
| title        = Communications in Mathematical Physics
| cover        = [[File:CMP cover.jpg]]
| editor       = [[Horng-Tzer Yau]]
| discipline   = [[Physics|Mathematical Physics]]
| language     = [[English language|English]]
| abbreviation = Commun. Math. Phys.
| publisher    = [[Springer Science+Business Media|Springer]]
| country      = [[Germany]]
| frequency    = 24/year
| history      = 1965–present
| openaccess   =
<!--
| impact       = 2.067 (2009)
| impact       = 2.000 (2010)
| impact       = 1.941 (2011)
-->
| impact       = 2.375 (2015)
| website      = http://www.springer.com/journal/220/
| link1        = http://www.springerlink.com/content/1432-0916
| link1-name   = Online access
| link2        =
| link2-name   = 
| RSS          = http://www.springerlink.com/content/1434-6028?sortorder=asc&export=rss
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 0010-3616
| eISSN        = 1432-0916
}}

'''''Communications in Mathematical Physics''''' is a [[peer-review]]ed [[academic journal]] published by [[Springer (publisher)|Springer]]. The journal publishes papers in all fields of [[mathematical physics]], but focuses particularly in [[Analysis (mathematics)|analysis]] related to [[condensed matter physics]], [[statistical mechanics]] and [[quantum field theory]], and in [[operator algebra]]s, [[quantum information]] and [[theory of relativity|relativity]]. The current [[editor-in-chief]] is [[Horng-Tzer Yau]].

Articles from 1965 to 1997 are available in electronic form  free of charge, via [[Project Euclid]], a non-profit organization initiated by [[Cornell University Library]].<ref name="Euclid">http://projecteuclid.org/DPubS?service=UI&version=1.0&verb=Display&handle=euclid.cmp Volumes available via Euclid</ref> This portion of the journal is provided through the Electronic Mathematical Archiving Network Initiative (EMANI)<ref>http://www.emani.org/ EMANI</ref> to support the long-term electronic preservation of mathematical publications.

== History ==

[[Rudolf Haag]] conceived this journal with [[Res Jost]], and Haag became the Founding Chief Editor. The first issue of ''Communications in Mathematical Physics'' appeared in 1965. Haag guided the journal for the next eight years. Then [[Klaus Hepp]] succeeded him for three years, followed by [[James Glimm]], for another three years. [[Arthur Jaffe]] began as chief editor in 1979 and served for 21 years. [[Michael Aizenman]] became the fifth chief editor in the year 2000 and served in this role until 2012.

== References ==
{{reflist}}

== External links ==
* [http://www.arthurjaffe.com/Assets/documents/CMPFounding.htm On 40 years of CMP]

[[Category:Mathematics journals]]
[[Category:Physics journals]]
[[Category:Publications established in 1965]]
[[Category:Springer Science+Business Media academic journals]]