{{Infobox album
|  Name        = Of Human Feelings
|  Type        = studio
|  Border      = yes
|  Cover       = Ornette Coleman - Of Human Feelings.jpg
|  Alt = 
|  Artist      = [[Ornette Coleman]]
|  Released    = {{Start date|1982|mf=yes}}
|  Recorded    = April 25, 1979
|  Studio      = [[CBS Studio Building|CBS Studios]] in New York City
|  Genre       = [[Jazz-funk]], [[free jazz]], [[funk]], [[jazz fusion]]
|  Length      = {{Duration|m=36|s=21}}
|  Label       = [[Antilles Records|Antilles]]
|  Producer    = Ornette Coleman
|  Last album  = ''[[Body Meta]]''<br />(1978)
|  This album  = '''''Of Human Feelings''''' <br /> (1982)
|  Next album  = ''[[Opening the Caravan of Dreams]]''<br />(1985)
}}

'''''Of Human Feelings''''' is a 1982 [[studio album]] by American [[jazz]] saxophonist, composer, and bandleader [[Ornette Coleman]]. It was recorded on April 25, 1979, at [[CBS Studio Building|CBS Studios]] in New York City with his Prime Time band, which featured guitarists Charlie Ellerbee and [[Bern Nix]], bassist [[Jamaaladeen Tacuma]], and drummers Calvin Weston and Coleman's son [[Denardo Coleman|Denardo]]. It followed the saxophonist's failed attempt to record a [[direct to disc recording|direct-to-disc]] session earlier in March 1979 and was the first jazz album to be [[digital recording|recorded digitally]] in the United States.

''Of Human Feelings'' explores [[jazz-funk]] music and continues Coleman's [[harmolodic]] approach to improvisation with Prime Time, whom he introduced on his 1975 album ''[[Dancing in Your Head]]''. He drew on [[rhythm and blues]] influences from early in his career for ''Of Human Feelings'', which had shorter and more distinct compositions than ''Dancing in Your Head''. Coleman also applied [[free jazz]] principles from his music during the 1960s to elements of [[funk]].

Following a change in management, Coleman signed with [[Island Records]], and ''Of Human Feelings'' was released in 1982 by its subsidiary label [[Antilles Records]]. Critics generally praised the album's expressive music and harmolodic approach, but it made little commercial impact and went [[out of print]]. Coleman enlisted his son Denardo as manager after a dispute with his former managers over the album's [[royalties]], a change that inspired him to perform publicly again during the 1980s.

== Background ==
[[File:Ornette at The Forum 1982.jpg|left|thumb|upright|Coleman in 1982]]

By the end of the 1960s, Ornette Coleman had become one of the most influential musicians in [[jazz]] after pioneering its most controversial subgenre, [[free jazz]], which jazz critics and musicians initially derided for its deviation from conventional structures of [[harmony]] and [[tonality]].<ref>{{harvnb|Palmer|1982}}; {{harvnb|Rinzler|2008|pp=62–63}}</ref> In the mid-1970s, he stopped recording free jazz, recruited electric instrumentalists, and pursued a new creative theory he called [[harmolodics]].{{sfn|Cohen|2012|p=97}} According to Coleman's theory, all the musicians are able to play individual melodies in any [[key (music)|key]], and still sound coherent as a group. He taught his young sidemen this new improvisational and ensemble approach, based on their individual tendencies, and prevented them from being influenced by conventional styles.{{sfn|Palmer|1986}} Coleman likened this group ethic to a spirit of "[[collective consciousness]]" that stresses "human feelings" and "biological rhythms", and said that he wanted the music, rather than himself, to be successful.{{sfn|Nelson|Bittan|Takiff|Carrier|1982|p=52}} He also started to incorporate elements from other styles into his music, including [[rock music|rock]] influences such as the electric guitar and non-Western rhythms played by Moroccan and Nigerian musicians.{{sfn|Palmer|1982}}

''Of Human Feelings'' was a continuation of the harmolodics approach Coleman had applied with Prime Time, an electric quartet introduced on his 1975 album ''[[Dancing in Your Head]]''. The group comprised guitarists Charlie Ellerbee and [[Bern Nix]], bassist [[Jamaaladeen Tacuma]], and drummers [[Ronald Shannon Jackson]] and [[Denardo Coleman]], Ornette Coleman's son.<ref>{{harvnb|Palmer|1986}}; {{harvnb|Litweiler|1992|p=170}}</ref> Tacuma was still in high school when Coleman enlisted him, and first recorded with Prime Time in 1975 for the album ''[[Body Meta]]'', which was released in 1978.<ref>{{harvnb|Nicholson|1998|p=313}}; {{harvnb|Larkin|1998|p=5280}}</ref> Tacuma had played in an ensemble for jazz organist [[Charles Earland]], but Earland dismissed him as he felt audiences gave excessive attention to his playing. Coleman found Tacuma's playing ideal for harmolodics and encouraged him not to change.{{sfn|Mandel|2007|p=161}} Although Coleman's theory initially challenged his knowledge and perception of music, Tacuma came to like the unconventional role each band member was given as a soloist and melodist: "When we read Ornette's music we have his [[note (music)|notes]], but we listen for his [[phrase (music)|phrases]] and phrase the way he wants to. I can take the same melody, then, and phrase it like I want to, and those notes will determine the phrasing, the rhythm, the harmony&nbsp;– all of that."{{sfn|Mandel|2007|p=162}}

== Recording and production ==
[[File:Jamaaladeen Tacuma.jpg|right|thumb|Prime Time bassist [[Jamaaladeen Tacuma]], photographed in 2007]]

In March 1979, Coleman went to [[RCA Records]]' New York studio to produce an album with Prime Time by [[Direct to disc recording|direct-to-disc recording]]. They had mechanical problems with the studio equipment and the recording was rejected. The failed session was a project under Phrase Text, Coleman's [[Music publisher (popular music)|music publishing company]]. He wanted to set up his own record company with the same name, and chose his old friend Kunle Mwanga as his manager.{{sfn|Litweiler|1992|p=170}} In April, Mwanga arranged another session at [[CBS Studio Building|CBS Studios]] in New York City, and Coleman recorded ''Of Human Feelings'' there on April 25; the session was originally titled ''Fashion Faces''.<ref>{{harvnb|Litweiler|1992|p=170}}; {{harvnb|Anon.|1982a}}.</ref> Jackson did not record with the band and Calvin Weston was hired in his place to play simultaneously with Denardo Coleman.{{sfn|Litweiler|1992|p=170}} They recorded all the album's songs on the first take without any equipment problems.<ref>{{harvnb|Litweiler|1992|p=170}}; {{harvnb|Wilson|1999|p=207}}.</ref> 

''Of Human Feelings'' was recorded with a Sony [[PCM adaptor|PCM-1600]] two-track [[digital recording|digital recorder]], a rare item at the time.{{sfn|Butterworth|2013}} According to journalist Howard Mandel, the [[passage (music)|passages]] played by the band sounded neither very soft or loud on the album, because it had been [[audio mixing (recorded music)|mixed]] with a middle-[[frequency]] range and [[dynamic range compression|compressed dynamics]].{{sfn|Mandel|2007|p=162}} Because of the equipment used, Coleman did not embellish the album with added effects and avoided [[overdubbing]], [[Multitrack recording|multi-tracking]], and [[remix]]ing.{{sfn|Butterworth|2013}} According to him, ''Of Human Feelings'' was the first jazz album to be digitally recorded in the United States.{{sfn|Wilson|1999|p=207}}

== Composition ==
{{quote box|quote="People have started asking me if I'm really a rhythm-'n'-blues player, and I always say, why, sure. To me rhythm is the oxygen that sits under the notes and moves them along and blues is the colouring of those notes, how they're interpreted in an emotional way."|source=—[[Ornette Coleman]], 1981{{sfn|Harrison|Fox|Thacker|Nicholson|2000|p=573}}|width=17%|align=left|style=padding:8px;|border=2px}}

According to ''[[The New Grove Dictionary of Music and Musicians|The Concise Oxford Dictionary of Music]]'' (2004), ''Of Human Feelings'' features [[jazz-funk]], a type of music that originated around 1970 and was characterized by intricate rhythmic patterns, a recurrent bass line, and [[Latin American music|Latin rhythmic]] elements.{{sfn|Kennedy|Bourne|2004|p=152}} Lloyd Sachs of the ''[[Chicago Sun-Times]]'' wrote that, although Coleman was not viewed as a [[jazz fusion]] artist, the album can be described as such because of its combination of free jazz and [[funk]].{{sfn|Sachs|1997|p=10}} [[Glenn Kenny]] disagreed and felt its boisterous style had more in common with the [[no wave]] genre and the artists of New York City's [[downtown music]] scene such as [[John Zorn]].{{sfn|Kenny|2015}} Jazz writer Stuart Nicholson viewed it as the culmination of Coleman's musical principles that dated back to his free jazz music in 1960, but reappropriated with a funk-oriented [[backbeat]].{{sfn|Harrison|Fox|Thacker|Nicholson|2000|p=574}} According to [[music journalism|jazz critic]] Barry McRae, "it was as if Coleman was translating the concept of the famous double quartet" from his 1961 album ''[[Free Jazz (album)|Free Jazz]]'' to what was required to perform jazz-funk.{{sfn|McRae|Middleton|1988|p=67}}

[[File:Ellerbie Coleman Nix.jpg|thumb|right|Coleman (middle), accompanied by guitarists Charlie Ellerbee (left) and [[Bern Nix]] (right)]]
Coleman incorporated traditional [[Musical form|structures]] and rhythms, and other elements from the [[rhythm and blues]] music he had played early his career.{{sfn|Giddins|1985|p=241}} According to Mandel, the album's simple, brisk music was more comparable to a coherent R&B band than jazz fusion.{{sfn|Mandel|2007|pp=162–163}} Although Coleman still performed the melodies on a song, he employed two guitarists for [[contrast (music)|contrast]] to make each pair of guitarist and drummer responsible for either the rhythm or melody.{{sfn|McRae|Middleton|1988|p=67}} Ellerbee provided [[accent (music)|accented]] [[linear counterpoint]] and Nix played variations of the song's melody, while Denardo Coleman and Weston played both [[polyrhythm]]s and backbeats.<ref>{{harvnb|Giddins|1982|p=4}}; {{harvnb|Palmer|1982}}</ref> On songs such as "Jump Street" and "Love Words", Ellerbee incorporated [[distortion (music)|distortion]] into his guitar playing, which gave the songs a thicker [[Texture (music)|texture]].{{sfn|Palmer|1982}} Tacuma and Ornette Coleman's instrumental [[call and response (music)|responses]] were played as the foreground to the less prominent guitars.{{sfn|Litweiler|1992|p=170}} McRae remarked that Coleman and Prime Time exchanged "directional hints" throughout the songs, as one player changed key and the others [[Modulation (music)|modulated]] accordingly.{{sfn|McRae|Middleton|1988|p=67}} The band made no attempt to harmonize their radically different parts while playing.{{sfn|Mandel|2007|p=162}}

{{Listen|pos = left
 |filename     = Ornette Coleman - Sleep Talk.ogg
 |title        = "Sleep Talk"
 |description  = A 21-second sample of "Sleep Talk"
}}
''Of Human Feelings'' features shorter and more distinct compositions than ''Dancing in Your Head''.{{sfn|Palmer|1986}} "Sleep Talk", "Air Ship", and "Times Square" were originally performed by Coleman during his concerts in 1978 under the names "Dream Talking", "Meta", and "Writing in the Streets", respectively. "What Is the Name of That Song?" was titled as a sly reference to two of his older compositions, "Love Eyes" and "Forgotten Songs" (also known as "Holiday for Heroes"), whose [[theme (music)|themes]] were played concurrently and transfigured by Prime Time.{{sfn|Wilson|1999|p=207}} The theme from "Forgotten Songs", originally from Coleman's 1972 album ''[[Skies of America]]'', was used as a [[refrain]].{{sfn|Giddins|1982|p=4}} "Jump Street" is a [[blues]] piece, "Air Ship" comprises a six-[[bar (music)|bar]] riff, and the [[atonality|atonal]] "Times Square" has futuristic dance themes.<ref>{{harvnb|Giddins|1982|p=4}}; {{harvnb|Harrison|Fox|Thacker|Nicholson|2000|p=574}}</ref> "Love Words" heavily uses [[Polymodal chromaticism|polymodality]], a central feature of harmolodics, and juxtaposes Coleman's extended solo against a dense, rhythmically complex backdrop. Nicholson observed [[Music of West Africa|West African]] rhythms and collective improvisation rooted in [[Dixieland|New Orleans jazz]] on "Love Words", and suggested that "Sleep Talk" was derived from the opening [[bassoon]] solo in [[Igor Stravinsky]]'s 1913 orchestral work ''[[The Rite of Spring]]''.{{sfn|Harrison|Fox|Thacker|Nicholson|2000|p=574}}

== Release and promotion ==
A few weeks after ''Of Human Feelings'' was recorded, Mwanga went to Japan to negotiate a deal with [[Kenwood Corporation|Trio Records]] to have the album released on Phrase Text. Trio, who had previously released a compilation of Coleman's 1966 to 1971 live performances in Paris, prepared to [[record press|press]] the album once Mwanga provided the label with the [[matrix numbers|record stamper]]. Coleman was also set to perform his song "Skies of America" with the [[NHK Symphony Orchestra]], but cancelled both deals upon Mwanga's return from Japan. Mwanga immediately quit after less than four months as Coleman's manager.{{sfn|Litweiler|1992|p=170}} In 1981, Coleman hired Stan and [[Sid Bernstein]] as his managers, who sold the album's recording tapes to [[Island Records]].<ref>{{harvnb|Davis|1986|p=143}}; {{harvnb|Nicholson|1990|p=109}}</ref> He signed with the record label that year, and ''Of Human Feelings'' was released in 1982 on Island's subsidiary jazz label [[Antilles Records]].<ref>{{harvnb|Davis|1986|p=143}}; {{harvnb|Davis|1986|p=143}}</ref> ''[[Billboard (magazine)|Billboard]]'' magazine published a front-page story at the time about its distinction as both the first digital album recorded in New York City and the first digital jazz album recorded by an American label.{{sfn|Litweiler|1992|pp=152, 170}}

According to jazz writer [[Francis Davis]], "a modest commercial breakthrough seemed imminent" for Coleman, who appeared to be regaining his celebrity.{{sfn|Davis|1986|pp=142–3}} German musicologist Peter Niklas Wilson said the album may have been the most tuneful and commercial-sounding of his career at that point.{{sfn|Wilson|1999|p=206}} The album's clean mix and relatively short tracks were interpreted as an attempt for [[airplay|radio airplay]] by Mandel, who described its production as "the surface consistency that would put it in the pop sphere".{{sfn|Mandel|2007|p=162}} ''Of Human Feelings'' had no success on the American [[hit parade|pop charts]], only charting on the [[Top Jazz Albums]], where it spent 26 weeks and peaked at number 15.<ref>{{harvnb|Anon.|n.d.}}; {{harvnb|Anon.|1982b|p=33}}</ref> Because the record offered a middle ground between funk and jazz, McRae argued that it consequently appealed to neither demographic of listeners.{{sfn|McRae|Middleton|1988|p=68}} ''[[Sound & Vision (magazine)|Sound & Vision]]'' critic Brent Butterworth speculated that it was overlooked because it had electric instruments, rock and funk drumming, and did not conform to what he felt was the hokey image of jazz that many of the genre's fans preferred.{{sfn|Butterworth|2013}} The album later went [[out of print]].{{sfn|Cooper|Smay|2004|p=238}}

[[File:Prime Time 1985.jpg|thumb|left|Coleman and Prime Time at the [[Caravan of Dreams]] in 1985]]
Coleman received $25,000 for the [[publishing rights]] to ''Of Human Feelings'' but said his managers sold it for less than the recording costs and that he did not receive any of its [[royalties]]. According to Stan Bernstein, Coleman had financial expectations that were "unrealistic in this business unless you're Michael Jackson". Antilles label executive Ron Goldstein felt the $25,000 Coleman received was neither a great nor a fair amount for someone in jazz.{{sfn|Davis|1986|pp=143–4}} After he had gone over budget to record a follow-up album, Island did not release it nor pick up their [[option (finance)|option]] on him, and in 1983, he left the Bernstein Agency.{{sfn|Davis|1986|p=144}} He chose Denardo Coleman to manage his career while overcoming his reticence of public performance, which had been rooted in his distrust of doing business with a predominantly White music industry.{{sfn|Nicholson|1990|p=109}} According to Nicholson, "the man once accused of standing on the throat of jazz was welcomed back to the touring circuits with both curiosity and affection" during the 1980s.{{sfn|Nicholson|1990|p=109}} Coleman did not record another album for six years and instead performed internationally with Prime Time.{{sfn|McRae|Middleton|1988|p=68}}
{{clear}}

== Critical reception ==
{{Album ratings
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|4|5}}{{sfn|Yanow|n.d.}}
| rev2 = ''[[Rolling Stone]]''
| rev2Score = {{Rating|4|5}}{{sfn|Morrison|1982|p=43}}
| rev3 = ''[[The Rolling Stone Jazz Record Guide|Rolling Stone Jazz Record Guide]]''
| rev3Score = {{Rating|4|5}}{{sfn|Swenson|1985|p=46}}
| rev4 = ''[[Spin Alternative Record Guide]]''
| rev4Score = 10/10{{sfn|Weisbard|Marks|1995}}
| rev5 = ''[[The Village Voice]]''
| rev5Score = A+{{sfn|Christgau|1982}}
}}
''Of Human Feelings'' received considerable acclaim from contemporary critics.{{sfn|Tinder|1982|p=19}} In a review for ''[[Esquire (magazine)|Esquire]]'', [[Gary Giddins]] hailed it as another landmark album from Coleman and his most accomplished work of harmolodics, partly because of compositions which he found clearly expressed and occasionally timeless. In his opinion, the discordant keys radically transmuted conventional [[polyphony]] and would be the most challenging part for listeners, who he said should concentrate on Coleman's playing and "let the maelstrom resolve itself around his center". Giddins also highlighted the melody of "Sleep Talk", deeming it among the best of the saxophonist's career.{{sfn|Giddins|1982|p=4}} Kofi Natambu from the ''[[Metro Times|Detroit Metro Times]]'' wrote that Coleman's synergetic approach displayed expressive immediacy rather than superficial technical flair while calling the record "a multi-tonal mosaic of great power, humor, color, wit, sensuality, compassion and tenderness". He found the songs inspirational, danceable, and encompassing developments in [[African-American music]] over the previous century.{{sfn|Natambu|1982|p=39}} [[Robert Christgau]] found the music heartfelt and sophisticated in its exchange of rhythms and simple pieces of melody, writing in ''[[The Village Voice]]'', "the way the players break into ripples of song only to ebb back into the tideway is participatory democracy at its most practical and utopian."{{sfn|Christgau|1982}}

[[Purist]] critics in jazz complained about the music's incorporation of danceable beats and electric guitar.{{sfn|Palmer|1982}} In ''[[Stereo Review]]'', [[Chris Albertson]] deemed the combination of saxophone and bizarre funk occasionally captivating but ultimately unfocused.{{sfn|Albertson|1982|p=83}} Dan Sullivan of the ''[[Los Angeles Times]]'' believed the album's supporters in "hip rock circles" had overlooked flaws, arguing that Tacuma and Coleman's playing sounded like a unique "beacon of clarity" amid an incessant background.{{sfn|Sullivan|1982|p=K80}} [[Leonard Feather]] wrote in the ''[[The Blade (Toledo)|Toledo Blade]]'' deemed the music stylistically ambiguous, potentially controversial, and difficult to assess but interesting enough to warrant a listen.{{sfn|Feather|1982|p=3}} At the end of 1982, ''Billboard'' editor Peter Keepnews named ''Of Human Feelings'' the year's best album, calling it a prime example of fusing free jazz with modern funk.{{sfn|Keepnews|1983|p=68}} In year-end lists for ''[[The Phoenix (newspaper)|The Boston Phoenix]]'', James Hunter and Howard Hampton ranked the album number one and number four, respectively.{{sfn|Anon.|1983a|p=12}} It was voted 13th best in the [[Pazz&nbsp;& Jop]], an annual poll of American critics nationwide, published in ''The Village Voice''.{{sfn|Anon.|1983b}} Christgau, the poll's supervisor, ranked it number one in an accompanying list, and in 1990 he named it the second-best album of the 1980s.<ref>{{harvnb|Christgau|1983}}; {{harvnb|Christgau|1990}}</ref>

In a 1986 article for ''[[The New York Times]]'' on Coleman's work with Prime Time, [[Robert Palmer (writer)|Robert Palmer]] said ''Of Human Feelings'' was still innovative and radical by the standards of other music in 1982, three years after it was recorded.{{sfn|Palmer|1986}} Because writers and musicians had heard its [[white label|test pressing]] in 1979, the album's mix of jazz improvisation and gritty, [[punk rock|punk]] and funk-derived energy sounded "prophetic" when it was released, Palmer explained. "The album is clearly the progenitor of much that has sounded radically new in the ongoing fusion of punk rock, black dance rhythms, and free jazz."{{sfn|Palmer|1982}} [[AllMusic]] critic [[Scott Yanow]] said although Coleman's compositions never achieved popularity, they succeeded within the context of an album that showcased his distinctive saxophone style, which was high-brow yet catchy.{{sfn|Yanow|n.d.}} Joshua Klein from ''[[The A.V. Club]]'' recommended ''Of Human Feelings'' as the best album for new listeners of Coleman's harmolodics-based music, while ''[[Chicago Tribune]]'' rock critic [[Greg Kot]] included it in his guide for novice jazz listeners; he named it one of the few albums that helped him both become a better listener of rock music and learn how to enjoy jazz.<ref>{{harvnb|Klein|2002}}; {{harvnb|Kot|1998|p=1}}.</ref> In 2008, ''[[New York (magazine)|New York]]'' magazine's Martin Johnson included it in his list of canonical albums from what he felt had been New York's sceneless yet vital jazz of the previous 40 years; ''Of Human Feelings'' exuded what he described as a spirit of sophistication with elements of funk, Latin, and [[Music of Africa|African music]], all of which were encapsulated by music that retained a jazz identity.{{sfn|Johnson|2008}}
{{clear}}

== Track listing ==
All compositions by Ornette Coleman.{{sfn|Anon.|1982a}}

{| style="width:50%;"
|-
|  style="width:50%; vertical-align:top;"|
{{Track listing
| headline = Side one
| title1 = Sleep Talk
| length1 = 3:34
| title2 = Jump Street
| length2 = 4:24
| title3 = Him and Her
| length3 = 4:24
| title4 = Air Ship
| length4 = 6:11
}}
{{Track listing
| headline = Side two
| title1 = What Is the Name of That Song?
| length1 = 3:58
| title2 = Job Mob
| length2 = 4:57
| title3 = Love Words
| length3 = 2:54
| title4 = Times Square
| length4 = 6:03
}}
|}

== Personnel ==
Credits are adapted from the album's liner notes.{{sfn|Anon.|1982a}}

=== Musicians ===
*[[Denardo Coleman]]&nbsp;– [[Drum kit|drums]]
*[[Ornette Coleman]]&nbsp;– [[alto saxophone]], [[Record producer|production]]
*Charlie Ellerbee&nbsp;– [[guitar]]
*[[Bern Nix]]&nbsp;– guitar
*[[Jamaaladeen Tacuma]]&nbsp;– [[bass guitar]]
*Calvin Weston&nbsp;– drums

=== Additional personnel ===
*Susan Bernstein&nbsp;– cover painting
*[[Peter Corriston]]&nbsp;– cover design
*[[Joe Gastwirt]]&nbsp;– [[Audio mastering|mastering]]
*[[Ron Saint Germain]]&nbsp;– [[Audio engineering|engineering]]
*Ron Goldstein&nbsp;– executive direction
*Harold Jarowsky&nbsp;– second engineering
*Steven Mark Needham&nbsp;– photography
*Ken Robertson&nbsp;– [[Tape recorder|tape operation]]

== See also ==
{{Portal|jazz}}
* [[Harmolodic funk]]
* [[Loft jazz]]
* [[Punk jazz]]

== References ==
{{reflist|15em}}

== Bibliography ==
{{refbegin|30em}}
* {{cite journal|ref=harv|last=Albertson|first=Chris|authorlink=Chris Albertson|date=August 1982|journal=[[Stereo Review]]|location=New York|volume=47|issue=8|title=Popular Music}}
* {{cite AV media notes|ref={{SfnRef|Anon.|1982a}}|author=Anon.|year=1982|title=Of Human Feelings|others=[[Ornette Coleman]]|publisher=[[Antilles Records]]|id=AN-2001|type=LP liner notes}}
* {{cite journal|ref={{SfnRef|Anon.|1982b}}|author=Anon.|year=1982|url=https://books.google.com/books?id=8SQEAAAAMBAJ&pg=PT54#v=onepage&q&f=false|accessdate=April 11, 2013|title=Jazz LPs|journal=[[Billboard (magazine)|Billboard]]|location=New York|issue=August 28}}
* {{cite news|ref={{SfnRef|Anon.|1983b}}|author=Anon.|year=1983|url=http://www.robertchristgau.com/xg/pnj/pjres82.php|title=The 1982 Pazz & Jop Critics Poll|newspaper=[[The Village Voice]]|location=New York|issue=February 22|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6Hutoi4SB|archivedate=July 6, 2013|deadurl=no}}
* {{cite news|ref={{SfnRef|Anon.|1983a}}|author=Anon.|year=1983|url=https://news.google.com/newspapers?id=4H0hAAAAIBAJ&sjid=rooFAAAAIBAJ&pg=5566,303352|accessdate=April 11, 2013|title=Boston Phoenix Critics' Top 10 Albums for 1982|issue=January 4|newspaper=[[The Phoenix (newspaper)|The Boston Phoenix]]}}
* {{cite web|ref=harv|author=Anon.|date=n.d.|url=http://www.allmusic.com/album/of-human-feelings-mw0000902383/awards|title=Of Human Feelings – Ornette Colement : Awards|publisher=[[AllMusic]]|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6HutbENvF|archivedate=July 6, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Butterworth|first=Brent|issue=May 26|year=2013|url=http://www.soundandvisionmag.com/blog/2013/05/26/brent-butterworth-jump-street-ornette-coleman|title=Brent Butterworth on 'Jump Street' by Ornette Coleman|journal=[[Sound & Vision (magazine)|Sound & Vision]]|location=New York|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6J8DogPQq|archivedate=August 25, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Christgau|first=Robert|authorlink=Robert Christgau|issue=June 1|year=1982|url=http://www.robertchristgau.com/xg/cg/cgv6-82.php|title=Christgau's Consumer Guide|newspaper=The Village Voice|location=New York|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6HutgfkEW|archivedate=July 6, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Christgau|first=Robert|issue=February 22|year=1983|url=http://www.robertchristgau.com/xg/pnj/deans82.php|title=Pazz & Jop 1982: Dean's List|newspaper=The Village Voice|location=New York|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6Hutuoexu|archivedate=July 6, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Christgau|first=Robert|year=1990|title=Christgau's Record Guide: The '80s|publisher=[[Pantheon Books]]|isbn=067973015X}}
* {{cite news|ref=harv|last=Christgau|first=Robert|issue=January 2|year=1990|url=http://www.robertchristgau.com/xg/list/decade80.php|title=Decade Personal Best: '80s|newspaper=The Village Voice|location=New York|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6Hutx5EI4|archivedate=July 6, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Cohen|first=Thomas F.|title=Playing to the Camera: Musicians and Musical Performance in Documentary Cinema|year=2012|publisher=[[Columbia University Press]]|isbn=978-1-906660-22-2}}
* {{cite book|ref=harv|editor1-last=Cooper|editor1-first=Kim|editor2-last=Smay|editor2-first=David|year=2004|title=Lost in the Grooves: Scram's Capricious Guide to the Music You Missed|publisher=[[Routledge]]|isbn=978-0-415-96998-7}}
* {{cite book|ref=harv|last=Davis|first=Francis|authorlink=Francis Davis|year=1986|title=In the Moment: Jazz in the 1980s|publisher=[[Oxford University Press]]|isbn=978-0-19-504090-6}}
* {{cite news|ref=harv|last=Feather|first=Leonard|authorlink=Leonard Feather|newspaper=[[The Blade (Toledo)|Toledo Blade]]|issue=June 5|year=1982|title=Options Widen for Jazz Buffs|url=https://news.google.com/newspapers?id=gT9PAAAAIBAJ&sjid=pwIEAAAAIBAJ&pg=6803,6247300|accessdate=April 11, 2013}}
* {{cite journal|ref=harv|last=Giddins|first=Gary|authorlink=Garry Giddins|journal=[[Esquire (magazine)|Esquire]]|date=July 1982|volume=98|issue=1|title=High Notes: The Five Best Recent Releases|location=New York}}
* {{cite book|ref=harv|last=Giddins|first=Gary|title=Rhythm-a-Ning: Jazz Tradition and Innovation in the '80s|publisher=Oxford University Press|year=1985|isbn=978-0-19-503558-2}}
* {{cite book|ref=harv|last1=Harrison|first1=Max|last2=Fox|first2=Charles|authorlink2=Charles Fox (jazz critic)|last3=Thacker|first3=Eric|last4=Nicholson|first4=Stuart|year=2000|title=The Essential Jazz Records: Modernism to Postmodernism|volume=2|series=The Essential Jazz Records|publisher=Mansell|isbn=978-0-7201-1822-3}}
* {{cite journal|ref=harv|last=Johnson|first=Martin|year=2008|url=http://nymag.com/anniversary/40th/culture/45768/|title=40th Anniversary: The New York Jazz Canon|journal=[[New York (magazine)|New York]]|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6Huu3kGbN|archivedate=July 6, 2013|deadurl=no}}
* {{cite journal|ref=harv|last=Keepnews|first=Peter|year=1983|issue=January 8|url=https://books.google.com/books?id=PSQEAAAAMBAJ&pg=PT69#v=onepage&q&f=false|accessdate=April 11, 2013|title=Critics' Choice|journal=Billboard|location=New York}}
* {{cite book|ref=harv|editor1-last=Kennedy|editor1-first=Michael|editor1-link=Michael Kennedy (music critic)|editor2-last=Bourne|editor2-first=Joyce|year=2004|title=[[The New Grove Dictionary of Music and Musicians|The Concise Oxford Dictionary of Music]]|publisher=Oxford University Press|isbn=978-0-19-860884-4|edition=4th}}
* {{cite web|ref=harv|last=Kenny|first=Glenn|authorlink=Glenn Kenny|date=June 11, 2015|url=http://www.vulture.com/2015/06/ornette-colemans-uncompromising-genius.html|title=Ornette Coleman's Uncompromising Genius|publisher=[[Vulture.com|Vulture]]|accessdate=July 15, 2015|archiveurl=http://www.webcitation.org/6a2ljeJh6|archivedate=July 15, 2015|deadurl=no}}
* {{cite news|ref=harv|last=Klein|first=Joshua|issue=March 29|year=2002|url=http://www.avclub.com/articles/ornette-coleman-the-complete-science-fiction-sessi,21761/|title=Ornette Coleman: The Complete Science Fiction Sessions|newspaper=[[The A.V. Club]]|location=Chicago|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6Huu1qDfn|archivedate=July 6, 2013|deadurl=no}}
* {{cite news|ref=harv|last=Kot|first=Greg|authorlink=Greg Kot|issue=April 26|year=1998|url=http://articles.chicagotribune.com/1998-04-26/news/9804260381_1_jazz-showcase-sonny-sharrock-chicago-jazz-festival|title=A Rock Critic's Education In Jazz|newspaper=[[Chicago Tribune]]|accessdate=April 11, 2013|at=Arts & Entertainment section|archiveurl=http://www.webcitation.org/6HuteP3d6|archivedate=July 6, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Larkin|first=Colin|authorlink=Colin Larkin (writer)|volume=7|title=[[The Encyclopedia of Popular Music]]|year=1998|publisher=[[Muze|Muze UK]]|isbn=978-1-56159-237-1|edition=3rd}}
* {{cite book|ref=harv|last=Litweiler|first=John|year=1992|title=Ornette Coleman: A Harmolodic Life|publisher=[[William Morrow and Company|W. Morrow]]|isbn=978-0-688-07212-4}}
* {{cite book|ref=harv|last=Mandel|first=Howard|title=Miles, Ornette, Cecil: Jazz Beyond Jazz|year=2007|publisher=Routledge|isbn=978-0-203-93564-4}}
* {{cite book|ref=harv|last1=McRae|first1=Barry|last2=Middleton|first2=Tony|year=1988|title=Ornette Coleman|series=Jazz Masters Series|publisher=Apollo|isbn=978-0-948820-08-3}}
* {{cite journal|ref=harv|last=Morrison|first=Buzz|issue=372|date=June 24, 1982|journal=[[Rolling Stone]]|location=New York|title=Of Human Feelings}}
* {{cite news|ref=harv|last=Natambu|first=Kofi|issue=June|year=1982|newspaper=[[Metro Times|Detroit Metro Times]]|title=The Blues in 4-D: Of Human Feelings by Ornette Colement and the Prime Time Band}}
* {{cite news|ref=harv|last1=Nelson|first1=Nels|last2=Bittan|first2=Dave|last3=Takiff|first3=Jonathan|last4=Carrier|first4=Jerry|newspaper=[[Philadelphia Daily News]]|issue=June 4|year=1982|title=Kool Jazz Players}}
* {{cite book|ref=harv|last=Nicholson|first=Stuart|year=1990|title=Jazz: The 1980s Resurgence|publisher=[[Da Capo Press]]|isbn=978-0-306-80612-4}}
* {{cite book|ref=harv|last=Nicholson|first=Stuart|year=1998|title=Jazz Rock: A History|publisher=[[Canongate Books]]|isbn=978-0-86241-817-5}}
* {{cite news|ref=harv|last=Palmer|first=Robert|authorlink=Robert Palmer (writer)|year=1982|issue=March 14|url=https://www.nytimes.com/1982/03/14/arts/ornette-coleman-s-prophetic-jazz.html|title=Ornette Coleman's Prophetic Jazz|newspaper=[[The New York Times]]|accessdate=December 21, 2014|archiveurl=http://www.webcitation.org/6Uzq1KZKR|archivedate=December 21, 2014|deadurl=no}}
* {{cite news|ref=harv|last=Palmer|first=Robert|issue=July 16|year=1986|url=https://www.nytimes.com/1986/07/16/arts/the-pop-life-ornette-coleman-s-music-develops-in-prime-time.html|title=The Pop Life – Ornette Coleman's Music Develops in Prime Time|newspaper=The New York Times|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6HutVmZ0p|archivedate=July 6, 2013|deadurl=no}}
* {{cite book|ref=harv|last=Rinzler|first=Paul|year=2008|title=The Contradictions of Jazz|publisher=[[Scarecrow Press]]|isbn=0810862158}}
* {{cite news|ref=harv|last=Sachs|first=Lloyd|issue=July 27|year=1997|newspaper=Chicago Sun-Times|title=10 jazz-rock standouts|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=CSTB&p_theme=cstb&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB42319515DAE66&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|accessdate=April 11, 2013}} {{subscription required}}
* {{cite news|ref=harv|last=Sullivan|first=Dan|issue=July 25|year=1982|title=Album Briefs|newspaper=[[Los Angeles Times]]|url=http://pqasb.pqarchiver.com/latimes/access/667316032.html?dids=667316032:667316032&FMT=ABS&FMTS=ABS:AI&type=historic&date=Jul+25%2C+1982&author=&pub=Los+Angeles+Times&desc=ALBUM+BRIEFS&pqatl=google|accessdate=April 11, 2013}} {{subscription required}}
* {{cite book|ref=harv|editor-last=Swenson|editor-first=John|year=1985|title=[[The Rolling Stone Jazz Record Guide]]|publisher=[[Random House]]|location=New York|isbn=039472643X}}
* {{cite journal|ref=harv|last=Tinder|first=Cliff|date=April 1982|journal=[[Down Beat]]|volume=49|location=Chicago|title=Jamaaladeen Tacuma: Electric Bass in the Harmolodic Pocket|issue=4}}
* {{cite book|ref=harv|last1=Weisbard|first1=Eric|last2=Marks|first2=Craig|year=1995|title=[[Spin Alternative Record Guide]]|publisher=[[Vintage Books]]|location=New York|isbn=0-679-75574-8|chapter=Ornette Coleman}}
* {{cite book|ref=harv|last=Wilson|first=Peter Niklas|year=1999|title=Ornette Coleman: His Life and Music|publisher=Berkeley Hills Books|isbn=978-1-893163-04-1}}
* {{cite web|ref=harv|last=Yanow|first=Scott|authorlink=Scott Yanow|date=n.d.|url=http://www.allmusic.com/album/of-human-feelings-mw0000902383|title=Of Human Feelings – Ornette Coleman|publisher=AllMusic|accessdate=April 11, 2013|archiveurl=http://www.webcitation.org/6HutzEdoO|archivedate=July 6, 2013|deadurl=no}}
{{refend}}

==External links==
*{{Discogs master|155641}}
{{Ornette Coleman}}
{{featured article}}
{{Use mdy dates|date=April 2015}}

[[Category:1982 albums]]
[[Category:Antilles Records albums]]
[[Category:Ornette Coleman albums]]
[[Category:Jazz-funk albums]]
[[Category:Funk albums by American artists]]
[[Category:Jazz fusion albums]]
[[Category:Instrumental albums]]
[[Category:Albums recorded at CBS 30th Street Studio]]