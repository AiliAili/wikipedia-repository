<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin 
  |name = Gulfstream III
  |image =C-20_Gulfstream.png
  |caption = C-20 Gulfstream III operated by the [[United States Navy]]
}}{{Infobox Aircraft Type
  |type = [[Business jet]]
  |manufacturer = [[Gulfstream Aerospace]]
  |designer = 
  |first flight = 2 December 1979<ref name="Janes 82 p383-4">Taylor 1982, pp. 383–384.</ref>
  |introduced = 1980
  |retired = 
  |status = 
  |primary user = [[United States]]
  |more users = [[Gabon]] <br/>[[India]] <br/>[[Italy]]
  |produced =1979–1986 <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 202
  |unit cost = US$37M
  |developed from = [[Grumman Gulfstream II]]
  |variants with their own articles = [[Gulfstream IV|Gulfstream IV/G400/G450]]}}
|}

The '''Gulfstream III''', a [[business jet]] produced by [[Gulfstream Aerospace]], is an improved variant of the [[Grumman Gulfstream II]].

==Design and development==
The Gulfstream III was built at Savannah, Georgia in the United States and was designed as an improved variant of the [[Grumman Gulfstream II]].  Design studies were performed by [[Grumman Aerospace Corporation]] in collaboration with [[Gulfstream American]] Corporation.  Design of the Gulfstream III started with an effort to synthesize a completely new wing employing NASA supercritical airfoil sections and winglets. Optimization studies considering weight, drag, fuel volume, cost, and performance indicated that a substantial portion of the new wing benefit could be secured with modifications to the existing wing. As a result, the new wing concept was canceled and work began on design modifications that would retain the Gulfstream II wing box structure and trailing edge surfaces.<ref>Boppe, Charles W., "Computational Aerodynamic Design: X-29, the Gulfstream Series and a Tactical Fighter", SAE paper 851789, 1985 Wright Brothers Award Paper, presented at the Aerospace Technology Conference & Exposition, Long Beach California, October 1985.</ref> 

Compared to the [[Gulfstream II|G-1159 Gulfstream II]], the wing has {{convert|6|ft|m|1|abbr=on}} more span and {{convert|5|ft|m|1|abbr=on}} winglets added, the leading edge is longer and its contour is modified. The fuselage is {{convert|2|ft|m|1|abbr=on}} longer aft of the main door, the radome is extended and there is a new curved windshield. Maximum takeoff weight is increased to {{convert|68200|lb|kg|0|abbr=on}} or {{convert|69700|lb|kg|0|abbr=on}} and there are various changes to the autopilot, flight instruments, and engine instruments. The aircraft received its [[type certificate]] from the American [[Federal Aviation Administration]] on 22 September 1980.<ref name= typeCert>{{cite web |url= http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/2d504046f9657ec386257f610074fcfb/$FILE/A12EA_Rev46.pdf |title= Type Certificate date Sheet NO. A12EA, revision 46 |publisher= FAA |date= February 22, 2016}}</ref> A total of 202 Gulfstream IIIs were built, with the last example built in 1986.<ref name="Janes up 94 p313"/>

In 2013, the FAA modified 14 CFR part 91 rules to prohibit the operation of jets weighing 75,000 pounds or less that are not stage 3 noise compliant after December 31, 2015.  The Gulfstream III is listed explicitly in Federal Register [http://www.gpo.gov/fdsys/granule/FR-2013-07-02/2013-15843/content-detail.html 78 FR 39576].  Any Gulfstream III's that have not been modified by installing Stage 3 noise compliant engines or have not had "hushkits" installed for non-compliant engines will not be permitted to fly in the contiguous 48 states after December 31, 2015.  ''14 CFR §91.883 Special flight authorizations for jet airplanes weighing 75,000 pounds or less'' - lists special flight authorizations that may be granted for operation after December 31, 2015.

==Variants==

===Civil variants===
[[File:HZ-NR2-GulfstreamIII-304.jpg|thumb|Gulfstream III in 1981]]
*'''Model G-1159A Gulfstream III''' - Two or three-crew executive, corporate transport aircraft, powered by two [[Rolls-Royce Spey]] [[turbofan]] engines.

===Military variants===
*'''C-20A''' - [[United States Air Force]] variant configured for 14 passengers and five crew; phased out of USAF service in 2002, one example transferred to [[NASA]] for use at the [[Neil A. Armstrong Flight Research Center]] at [[Edwards Air Force Base]] as a test aircraft.<ref name="dod">''Model Designation of Military Aerospace Vehicles'', DoD 4120.15L, 2004-05-12</ref><ref>http://www.af.mil/information/factsheets/factsheet.asp?id=87</ref><ref>http://www.dfrc.nasa.gov/gallery/Photo/C-20A/HTML/EC02-0221-6.html</ref>
*'''C-20B''' - [[United States Air Force]] and [[United States Coast Guard]] variant with upgraded electronics, used for Operational Support Airlift (OSA) and Special Assignment Airlift Missions (SAAM); the single Coast Guard C-20B was used by the [[Commandant of the Coast Guard]] and other senior USCG officials as well as the [[United States Secretary of Homeland Security|Secretary of Homeland Security]].<ref name="dod"/>
*'''C-20C''' - [[United States Air Force]] C-20B with upgraded and "hardened" secure communications, often utilized as backup aircraft accompanying the [[Boeing VC-25|VC-25A]] aircraft when it is operating as [[Air Force One]]<ref name="dod"/>
*'''C-20D''' - [[United States Navy]] Operational Support Airlift (OSA) aircraft with modified communications equipment for use by the Navy, normally in support of high-ranking naval officials<ref name="dod"/>
*'''C-20E''' - Stretched fuselage/redesigned wing variant for use by the [[United States Army]] as an Operational Support Airlift (OSA) aircraft<ref name="dod"/><ref>The United States Military Aviation Directory, AIRTime Publishing, Norwalk, CT, c2000, ISBN 978-1-880588-29-1</ref>
*'''Gulfstream III SRA-1''' - Special reconnaissance and surveillance version for export.
*'''Gulfstream III SMA-3''' - Export model for Denmark, fitted with a [[Texas Instruments]] APS-127 search radar. Three maritime reconnaissance and patrol, fisheries protection, search and rescue, and VIP transport aircraft were built for the [[Royal Danish Air Force]] in 1983. No longer in service.

'''NOTE:'''  [[United States Army]] '''C-20F''' and '''C-20J''', [[United States Navy]]/[[United States Marine Corps]] '''C-20G''', and [[United States Air Force]] '''C-20H''' aircraft are all [[Gulfstream IV]] variants

==Special mission variants==
[[File:Gulfstream cargo door-phoenix air.jpg|thumb|Detail of the custom right cargo door of the Royal Danish Air Force SMA-3's and similar to those found on US military C-20 variants]]
A NASA Gulfstream C-20B (83-0502 cn 389) has been fitted with a centerline pylon to allow it to carry the UAVSAR pod.<ref>[http://www.nasa.gov/centers/dryden/aircraft/G-III_UAVSAR/index.html G-III UAVSAR] Retrieved 31 July 2011.</ref>

A NASA Gulfstream III (N992NA cn 309) has also been fitted with a centerline pylon to allow it to carry the AIRMOSS pod, a modification of the UAVSAR pod.<ref>[http://www.nasa.gov/centers/dryden/Features/uavsar_installed_for_airmoss_study.html#.UyI2YPldWSo UAVSAR Installed on JSC G-III for AirMOSS Study] Retrieved 13 March 2014.</ref>

The [[Phoenix Air]] Group operates two former [[Royal Danish Air Force]] SMA-3 aircraft (N173PA cn 313, N163PA cn 249) and a Gulfstream III (N186PA cn 317).<ref>[http://www.bizcorpjets.net/index.php?option=com_kunena&func=view&catid=10&id=148&Itemid=84 Full Details of Active Gulfstream IIIs] Retrieved 31 July 2011.</ref>  One aircraft provides airborne maritime range surveillance for the [[Missile Defense Agency]] (MDA) and other Department of Defense range facilities using a high definition [[Texas Instruments]] APS-127 Surface Search Radar system.<ref>[http://www.phoenixair.com/milops_range.html 'Military Ops Range Clearing'] Retrieved 31 July 2011.</ref> All three are configured with a large cargo door. In 2008 Phoenix Air developed an Airborne Biomedical Containment System with the [[Center for Disease Control|CDC]]. In 2014, the system was deployed during the [[Ebola virus epidemic in Liberia]] to fly 12 ebola missions to the United States.<ref>{{cite journal|magazine=AOPA Pilot|date=January 2015|page=T-14|title=Mike Ott The Inside Story of a Ebola Evacuation Mission|author=Thomas A Horne}}</ref>

N30LX (cn 438) has been modified by the addition of a ventral canoe and sensor turret as the "Dragon Star" Airborne Multi-Intelligence Laboratory for use by [[Lockheed Martin]].<ref>;[http://www.arabianaerospace.aero/article.php?section=defence&article=enter-the-dragon Enter The Dragon] Retrieved 31 July 2011.</ref> This has been leased by Italy since 2012.<ref>{{cite news |url=http://www.defensenews.com/story/defense/air-space/isr/2015/07/11/italy-sigint-aircraft-renew-north-africa/29937199/ |title=Italy Renews Lease on SIGINT Aircraft |last1=Kington |first1=Tom |date=11 July 2015 |website=www.defensenews.com |publisher=TEGNA |accessdate=13 July 2015}}</ref>

[[Calspan]] operates N710CF (cn 448), which has been modified as an airborne test bed.  Modifications include a centerline pylon <ref>[http://www.calspan.com/services/flight-testing/ Flight testing] N710CF with Centerline Pylon Capable of Carrying External Stores] Retrieved 22 August 2014.</ref> and a dorsal satcom radome <ref>[http://www.calspan.com/services/flight-testing/sensors-airborne-services-test-beds/ sensors airborne services-test-beds/] N710CF with Common Systems Radome on upper fuselage for SATCOM antenna testing] Retrieved 22 August 2014.</ref>

Two Gulfstream IIIs, K2961 (cn 494) and K2962 (cn 495), equipped with long-range oblique photography cameras mounted in the fuselage, were delivered to the [[Indian Air Force]].<ref>[http://www.airliners.net/photo/1217403/L/ Picture of the Gulfstream Aerospace G-1159A Gulfstream III aircraft] Retrieved 31 July 2011.</ref><ref>[http://www.airliners.net/photo/1736432/L/ Picture of the Gulfstream Aerospace G-1159A Gulfstream III aircraft] Retrieved 31 July 2011.</ref>

==Operators==

===Military and government operators===
[[File:US Navy 090219-N-9552I-030 Naval Aircrewmen 1st Class Troy Rudisill and David Williams conduct pre-flight checks in the cockpit of a Gulfstream C-20A-G III.jpg|thumb|Cockpit of a C-20A]]
<!-- We need to identify which one of these operate the G-III -->

Military and government operators of the Gulfstream III and C-20 include:
; {{DZA}}
; {{CMR}}
*[[Cameroon Air Force]] (phased out)
; {{CIV}}
; {{DEN}}
*[[Royal Danish Air Force]]<ref name="Schrøder">Schrøder, Hans (1991). "Royal Danish Airforce". Ed. Kay S. Nielsen. Tøjhusmuseet, 1991, p. 1–64. ISBN 87-89022-24-6.</ref>
; {{GAB}}
; {{GHA}}
*[[Ghana Air Force]]
; {{ITA}}
*[[Italian Air Force]]  operated two Gulfstream III from 1985 until 2003.<ref>{{cite web|url=http://www.aeroflight.co.uk/waf/italy/af/ital-af2-all-time.htm|title=Italian Air Force Aircraft Types|work=aeroflight.co.uk|accessdate=17 December 2015}}</ref>
; {{IND}}
*[[Indian Air Force]]
;{{IRL}}
*[[Irish Air Corps]] - leased aircraft
; {{MEX}}
*[[Mexican Air Force]] - (former operator)
; {{MAR}}
*[[Royal Moroccan Air Force]]
; {{OMA}}
; {{KSA}}
; {{TOG}}
; {{UGA}}
* [[Ugandan Air Force]]
; {{USA}}
* [[United States Air Force]]
* [[United States Navy]]
* [[United States Army]]
* [[United States Coast Guard]]
* [[National Aeronautics and Space Administration]]
; {{VEN}}
* [[Venezuelan Air Force]]
; {{ZIM}}
* [[Air Force of Zimbabwe]]

==Accidents and incidents==
*August 3, 1996 - Flew into mountain during final approach to [[Vagar Airport]] on Faroe Islands. The Gulfstream III (F-330) from RDAF - Royal Danish Air Force was destroyed killing all nine people on board, including the Danish Chief of Defence [[Jørgen Garde]].
*March 29, 2001 - While trying to land at [[Aspen-Pitkin County Airport]], an Avjet Gulfstream III [[2001 Avjet Aspen crash|crashed]] into a hill, killing all 18 people on board.

==Specifications (Gulfstream III)==
[[File:Gulfstream 3.gif|right|350px]]
{{Aircraft specifications
<!--    If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]]-->
|plane or copter?=plane
|jet or prop?=jet
|ref=Jane's Civil and Military Aircraft Upgrades 1994–95<ref name="Janes up 94 p313">Michell 1994, p. 313.</ref>
<!--
    Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with an asterisk; "*".
-->
<!--
        General characteristics
-->
|crew=two or three
|capacity=19 passengers (standard seating)
|length main=83 ft 1 in
|length alt=25.32 m
|span main=77 ft 10 in
|span alt=23.72 m
|height main=24 ft 4½ in
|height alt=7.43 m
|area main=934.6 sq ft
|area alt=86.83 m²
|aspect ratio=6.0:1
|airfoil=
|empty weight main=38,000 lb
|empty weight alt=17,236 kg
|loaded weight main=
|loaded weight alt= 
|useful load main= 
|useful load alt=
|max takeoff weight main=69,700 lb
|max takeoff weight alt=31,615 kg
|more general=
|engine (jet)=[[Rolls-Royce Spey RB.163 Mk 511-8]]
|type of jet=[[Turbofan]]
|number of jets=2
|thrust main=11,400 lbf
|thrust alt=50.7 kN
|thrust original=

<!--
        Performance
-->
   
|max speed main=576 mph
|max speed alt=501 knots, 928 km/h
|max speed more=(max cruise)
|cruise speed main=508 mph 
|cruise speed alt=442 knots, 818 km/h
|cruise speed more=(long range cruise)
|never exceed speed main=
|never exceed speed alt=
|stall speed main= 121 mph
|stall speed alt=105 knots, 194 km/h
|range main= 4,200 mi
|range alt=3,650 [[nautical mile|nmi]], 6,760 km
|range more=(eight passengers, IFR reserves)
|ceiling main=45,000 ft
|ceiling alt=13,716 m
|climb rate main=3,800 ft/min
|climb rate alt=19.3 m/s
|loading main=
|loading alt=
|thrust/weight=<!-- a unitless ratio -->
|power/mass main=
|power/mass alt=
|more performance=
|armament=
|avionics=

}}

==See also==
{{Aircontent
|related=
* [[Grumman Gulfstream II]]
* [[Gulfstream G400/G450|Gulfstream IV/G400/G450]]
|similar aircraft=
|sequence=
|lists=
* [[List of active United States military aircraft]]

<!-- |see also= -->

}}

== References ==
;Notes
{{Reflist|30em}}
;Bibliography
* Michell, Simon. ''Jane's Civil and Military Aircraft Upgrades 1994–95''. Coulsdon, UK:Jane's Information Group, 1994. ISBN 0-7106-1208-7.
*[[John W. R. Taylor|Taylor, John W. R.]] (editor). ''Jane's All The World's Aircraft 1982–83''. London:Jane's Yearbooks, 1982. ISBN 0-7106-0748-2.

==External links==
* {{commons category-inline|Gulfstream III}}

{{US transport aircraft}}
{{Gulfstream}}

[[Category:Gulfstream aircraft]]
[[Category:Twinjets]]
[[Category:United States business aircraft 1980–1989]]
[[Category:T-tail aircraft]]