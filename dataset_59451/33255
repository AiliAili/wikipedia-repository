{{About|the Castaic-Tejon Route in California|the Ridge Route in ancient Israel|Way of the Patriarchs}}
{{Use mdy dates|date=June 2013}}
{{Infobox road
|name = Ridge Route
|direction_a = South
|direction_b = North
|header_type = historic
|state = CA
|map_notes=Ridge Route highlighted in red
|length_mi=44
|length_round=0
|length_ref=<ref name="map" />
|established = 1915
|decommissioned = 1970
|terminus_a={{jct|state=CA|US 1926|99|SR|126}} in [[Castaic Junction, California|Castaic Junction]]
|junction={{jct|state=CA|SR|138}} in [[Gorman, California|Gorman]]
|terminus_b={{jct|state=CA|US 1926|99}} in [[Grapevine, California|Grapevine]]
|allocation={{jct|state=CA|US 1926|99}} after 1926
|embedded={{Infobox NRHP | embed=yes
 | name =Ridge Route, Old
 | nrhp_type =
 | nearest_city= [[Castaic, California]]
 | locmapin=Los Angeles
 | area =
 | built =1915
 | architect= W. Lewis Clark and J.B. Woodson
 | added = September 25, 1997
 | governing_body = [[United States Forest Service]]
 | refnum=97001113<ref name="nris">{{NRISref|2006a|mdy}}</ref>
 }}
}}

[[File:View of the Ridge Route (CHS-46239).jpg|thumb|upright=1.4|View of the route, 1920]]

The '''Ridge Route''', officially the '''Castaic-Tejon Route''', was a two-lane highway between [[Los Angeles County, California|Los Angeles]] and [[Kern County, California|Kern counties]], California. Opened in 1915 and paved with 15-ft [[concrete]] between 1917 and 1921, the road was the first [[paved highway]] directly linking the [[Los Angeles Basin]] with the [[San Joaquin Valley]] over the [[Tejon Pass]] and the rugged [[Sierra Pelona Mountains]] ridge south of [[Gorman, California|Gorman]]. Much of the old road runs through the [[Angeles National Forest]], and passes by many historical landmarks, including the National Forest Inn, Reservoir Summit, Kelly's Half Way Inn, Tumble Inn, and Sandberg's Summit Hotel. North of the forest, the Ridge Route passed through Deadman's Curve before ending at [[Grapevine, California|Grapevine]].

Most of the road was bypassed in 1933–34 by the three-lane '''Ridge Route Alternate''', then [[U.S. Route 99 in California|U.S. Route&nbsp;99]] (US&nbsp;99), to handle increased traffic and remove many curves. The four-lane US&nbsp;99 was completed in 1953 and replaced by a [[freeway]], [[Interstate 5 in California|Interstate&nbsp;5]] (I-5) around 1968. The portion of the road within the Angeles National Forest was added to the [[National Register of Historic Places]] in 1997, following the efforts of Harrison Scott. Much of the road has been closed by the [[U.S. Forest Service]]; other remnants of the road are used by local traffic.

==Route description==
The Ridge Route was officially the Castaic-Tejon Route.<ref>{{cite web |author= [[California State Legislature]] |url= http://www.leginfo.ca.gov/pub/01-02/bill/asm/ab_0051-0100/acr_98_bill_20011002_chaptered.html |title= Assembly Concurrent Resolution No. 98: Relative to the 1915 Ridge Route Highway Historical Monument |date= October 2, 2001 |publisher= Legislative Counsel of California}}</ref> The official limits of the Ridge Route, as built in the 1910s, were [[State Route 126 (California)|SR 126]] at [[Castaic Junction, California|Castaic Junction]] and the bottom of the grade at [[Grapevine, California|Grapevine]].<ref name="[[#Scott|Scott]], pp.&nbsp;74–75">[[#Scott|Scott]], pp.&nbsp;74–75</ref> Until 1930-31 the road from [[San Fernando, California|San Fernando]] to Castaic Junction ran through the [[Newhall Tunnel]] at [[San Fernando Pass]] and along San Fernando Road, Magic Mountain Parkway (both part of SR&nbsp;126 until the early 2000s)<ref>{{cite web |author= California State Legislature |url= http://www.leginfo.ca.gov/pub/01-02/bill/asm/ab_0601-0650/ab_635_bill_20011012_chaptered.html |title= Assembly Bill 635 |date= October 12, 2001 |publisher= Legislative Counsel of California}}</ref><ref>{{cite web |author= California State Legislature |url= http://www.leginfo.ca.gov/pub/03-04/bill/asm/ab_1701-1750/ab_1717_bill_20030925_chaptered.html |title= Assembly Bill 1717 |date= September 25, 2003 |publisher= Legislative Counsel of California}}</ref> and Feedmill Road to a former bridge over the [[Santa Clara River (California)|Santa Clara River]].<ref>[[#Scott|Scott]], pp.&nbsp;89–91</ref> A 1930 bypass of the tunnel and [[Newhall, California|Newhall]] through [[Weldon Canyon]] is now part of The Old Road.<ref name=s60-61>[[#Scott|Scott]], pp.&nbsp;60–61</ref>

From Castaic Junction north to [[Castaic, California|Castaic]] the Ridge Route has been largely buried by the Ridge Route Alternate and Interstate 5.<ref>[[#Scott|Scott]], pp.&nbsp;93–94</ref> At Castaic the Ridge Route Alternate turned northwest from the old road at {{coord|34.4898|-118.617|display=inline}}, now the intersection of Castaic Road and Neely Street. The first piece of Ridge Route Road out of Castaic has been realigned as recently as the late 1990s when the North Lake housing development was built.<ref>[[#Scott|Scott]], pp.&nbsp;100 and 289</ref> The road begins to climb after passing North Lake; a portion was straightened in 1924 and now is next to the southbound lanes of I-5.<ref>[[#Scott|Scott]], p.&nbsp;102</ref> In this area, known as the Five-Mile Grade, the four-lane Ridge Route Alternate became the northbound lanes of I-5, while the added four-lane alignment, built to the east (next to the old Ridge Route), had lower grades and became the southbound (downhill) lanes to cut down on [[runaway truck]]s. Two bridges were built to allow traffic to cross to the left side.<ref>[[#Scott|Scott]], p.&nbsp;283</ref> Near the north end of this area, the Ridge Route curves away from the newer bypass.<ref name="tgla" /> The road enters the [[Angeles National Forest]] about {{convert|1|mi|km|spell=in}} south of [[Templin Highway]], with the Forest Service road designation 8N04.<ref name=nmap>{{cite map|publisher=[[United States Geological Survey]] |url=http://viewer.nationalmap.gov/viewer/ |title=National Map Viewer |accessdate=March 17, 2007 |deadurl=yes |archiveurl=http://www.webcitation.org/66gupqQDM |archivedate=April 5, 2012 }}</ref>

Establishments in the forest included the National Forest Inn, Kelly's Half Way Inn, Tumble Inn, and Sandberg's Summit Hotel.

<div id="National Forest Inn"></div>

[[File:Ridge Route National Forest Inn steps.jpg|thumb|left|Remains of the steps to the National Forest Inn]]

The National Forest Inn was on the west side of the road. A popular place along the route, composed of white [[Clapboard (architecture)|clapboard]] buildings, it was described in a 1932 [[highway beautification]] pamphlet as "the sort of filling station that gets into a national forest and is no addition thereto". On October&nbsp;14, 1932,<ref>{{cite news |title= Flames Spread Ruin and Menace Highway Travel: Ridge Route Fire Spreads Blaze Which Destroys Forest Inn and Dozen Cabins Leaps to Highway and Perils Travelers |url= http://search.proquest.com/hnplatimes/docview/163044885/358DCA404A154766PQ/1?accountid=14512 |accessdate= January 31, 2014 |newspaper= Los Angeles Times |date= October 15, 1934 |page= A1 |subscription= yes |via= ProQuest}}</ref>  a fire began in the garage, and took over a day to put out. When the Ridge Route Alternate bypassed the site to the west, the inn was not rebuilt, and all that remains are [[concrete]] steps.<ref>[[#Scott|Scott]], pp.&nbsp;114–119</ref>

<div id="Swede's Cut"></div>About {{convert|2|mi|km|spell=in}} north of the National Forest Inn is Serpentine Drive, where the road curves around the sides of hills as it climbs out of a low point in the route (about {{convert|3200|ft|m}} above [[sea level]]).<ref>{{Cite map|publisher= United States Geological Survey |cartography= USGS |url= http://msrmaps.com/image.aspx?T=2&S=12&Z=11&X=428&Y=4792&W=3 |title= Liebre Mountain Quadrangle |year= 1988 |series= 7.5-minute}}</ref> North of the curves, the road passes through Swede's Cut, also called Big Cut, Culebra Excavation, or Castaic Cut. The cut was the largest on the route, with a depth of {{convert|110|ft|m}}.<ref>[[#Scott|Scott]], pp.&nbsp;71, 119–120</ref>

<div id="Reservoir Summit"></div>Reservoir Summit, also called Reservoir Hill, is {{convert|3883|ft|m}} above sea level. The Reservoir Summit Café was a popular [[high-class]] restaurant on the east side of the road, closed in the late 1920s; the [[Foundation (architecture)|foundation]] remains. The summit was named after a now-dry [[reservoir]], one of three probably built for the concrete used in paving the road.<ref>[[#Scott|Scott]], pp.&nbsp;43–45, 121–126</ref> <div id="Half Way Inn"></div>Kelly's Half Way Inn was roughly halfway between Los Angeles and Bakersfield. Located on a small knoll with a single tree on the east side of the road, all that remains is remnants of the foundation.<ref>[[#Scott|Scott]], pp.&nbsp;126–130</ref> <div id="Tumble Inn"></div>The Tumble Inn, later Mountain View Lodge, was on the west side of the road, and closed when the Ridge Route Alternate opened in 1933. Steps, including the top step with "TUMBLE INN" in the concrete, and a [[retaining wall]] remain.<ref>[[#Scott|Scott]], pp.&nbsp;135–138</ref>

<div id="Sandberg"></div>The Sandberg's Summit Hotel, later Sandberg's Lodge, was located just north of Liebre Summit, the highest point ({{convert|4233|ft|m}}) on the road, at {{convert|4170|ft|m}} above sea level. The hotel was built in 1914, and thus served travelers from the opening of the road in 1915. Built of [[timber|log]]s, it was a high-class hotel. The place, which had become a [[ceramic]]s factory, burned down on April&nbsp;29, 1961, from a fire started by the new owner—who was converting it into a "camp-type operation" for [[underprivileged children]]—burning trash in the fireplace. The lease from the [[U.S. Forest Service]] was canceled in 1963, and only portions of the foundation and a rock wall remain. The name "Sandberg" is still used by the [[National Weather Service]] for an automated weather station a short distance to the north at Pine Canyon Road.<ref>[[#Scott|Scott]], pp.&nbsp;144–160</ref> Pine Canyon Road ([[County Route N2 (California)|CR N2]]) marks the end of the forest and the beginning of county maintenance, and CR N2 uses the old Ridge Route alignment to reach [[State Route 138 (California)|SR 138]] near [[Quail Lake]].<ref name=nmap/>

[[File:Ridge Route Swede's Cut.jpg|thumb|Looking north through Swede's Cut]]

The Ridge Route crosses the [[West Branch California Aqueduct]] with SR 138, splitting to the northwest on Gorman Post Road. It rejoins the path of I-5 at [[Gorman, California|Gorman]], and, from Gorman to the end at Grapevine, most of the old road has been covered over by the Ridge Route Alternate or I-5. The path of the Ridge Route is now named Peace Valley Road and Lebec Road, passing over [[Tejon Pass]] and past [[Lebec, California|Lebec]] and [[Fort Tejon]]. Past Fort Tejon, the route descends through [[Grapevine Canyon (Kern County, California)|Grapevine Canyon]] to Grapevine (named for [[grape]]vines in the area<ref>[[#Scott|Scott]], pp.&nbsp;iii-iv</ref>). The best-known curve on the road, Deadman's Curve or Death Curve, is visible from Digier Road on the west side of I-5. The next part of the old road that still exists is near the bottom of the grade, where a number of curves brought the road down to Grapevine. The original plan was to build the road nearer to the center of the canyon, but a March&nbsp;1914 flood destroyed the work, and the grading was redone higher up. Deadman's Curve and the Grapevine loops were both bypassed by the Ridge Route Alternate, which was built directly over most of the old road in this area. At Grapevine, the land suddenly flattens out, and the road north of Grapevine was the longest straight section of road in the state—{{convert|17|mi|km}}—in 1926. Most of this road lies under I-5 and [[State Route 99 (California)|SR 99]], but the southernmost piece in Grapevine was bypassed by the Alternate, and is now in the [[median (road)|median]] of I-5.<ref>[[#Scott|Scott]], pp.&nbsp;162–224</ref>

==History==

===Before the Ridge Route===
Before the Ridge Route, passage between Los Angeles and the San Joaquin Valley was more indirect. For example, [[El Camino Real (California)|El Camino Real]] (Spanish, "the King's road"), the first major road in California,<!--laid out when?--> connected Los Angeles and the various [[Spanish missions in California|missions]], [[presidio]]s and settlements with [[San Francisco]] and [[Sonoma County, California|Sonoma]]. The San Joaquin Valley route split from the El Camino Real at present-day [[Universal City, California|Universal City]], the western leg running along the [[Pacific Coast]] and coastal valleys and the eastern [[El Camino Viejo]], (Spanish, "the old road") laid out via the San Joaquin Valley to San Antonio, now [[East Oakland]].<ref name="[[#Scott|Scott]], pp.&nbsp;1–2">[[#Scott|Scott]], pp.&nbsp;1–2</ref>

An alternate route between Los Angeles and Bakersfield followed the southern approach to the Ridge Route to [[Saugus, California|Saugus]], but took a longer route between Saugus and Gorman, heading northeast through the [[San Francisquito Canyon]] to the [[Antelope Valley]] and west to Gorman. The rest of the route, from Gorman toward Bakersfield via the Tejon Pass, followed the same path as the Ridge Route.<ref name="[[#Scott|Scott]], pp.&nbsp;1–2"/>

The [[Butterfield Overland Mail|Butterfield Overland Stage]], the first overland mail service to California, went from [[Tipton, Missouri|Tipton]], [[Missouri]] and [[Memphis, TN|Memphis]] to San Francisco via Los Angeles. From October&nbsp;1858, when the first stage passed through Tejon Pass, until April&nbsp;1861, the route was identical to El Camino Viejo, running via San Francisquito Canyon. In order to keep the stages from running directly northwest from [[San Bernardino, California|San Bernardino]] and bypassing Los Angeles, the [[Los Angeles County Board of Supervisors]] spent $8000 in 1858 (about ${{Formatprice|{{Inflation|US-NGDPPC|8000|1858|r=3}}}} in {{inflation-year|US-NGDPPC}}){{Inflation-fn|US-NGDPPC}} to deepen the cut—later [[Beale's Cut]]—at San Fernando Pass, south of Saugus and Newhall (east of Sierra Highway, west of the Antelope Valley Freeway or State Route 14). The path followed by the stages was changed to pass along the Pacific Coast from Los Angeles in April 1861.<ref>[[#Scott|Scott]], pp.&nbsp;15–19, 24–25</ref>

Similarly, the residents of the City of Los Angeles approved the expenditure of a good deal of money towards the building of the [[Southern Pacific Railroad]], which had originally planned to bypass the city. The line to San Francisco, including the {{convert|6966|ft|m|adj=on}} [[San Fernando Tunnel]] through San Fernando Pass, was completed on September&nbsp;5, 1876.<ref>[[#Scott|Scott]], pp.&nbsp;63–64</ref> The railroad's route between Saugus and Bakersfield was even longer than that of El Camino Viejo and the Butterfield Overland Stage, heading east through [[Soledad Canyon]] before turning north via [[Palmdale, California|Palmdale]] to [[Mojave, California|Mojave]] and northwest over [[Tehachapi Pass]].<ref>{{cite map|publisher= [[Rand McNally]] |url= http://fermi.jhuapl.edu/states/1895/ca_1895.jpg |title= New 11 x 14 Map of California |year=1895}}</ref>

In the 1910s, several power companies built lines through the area. The [[General Pipe Line Company]] completed an [[oil pipeline]] connecting the San Joaquin Valley's [[Midway-Sunset Oil Field]] with the port at [[San Pedro, California|San Pedro]] in 1913. Its alignment followed the Ridge Route north of Gorman and south of [[Reservoir Summit, California|Reservoir Summit]], a longer distance than any of the earlier transportation routes, but from Gorman south to the present location of [[Pyramid Lake (Southern California)|Pyramid Lake]], where it turned east to Reservoir Summit, it roughly followed the later Ridge Route Alternate.<!--via Peace Valley or Hungry Valley?--><ref>[[#Scott|Scott]], pp.&nbsp;31–32</ref> That same year, the [[Midway Gas Company]] opened a [[natural gas line]], and the [[Pacific Light and Power Company]] opened a [[electric power transmission|power line]], both staying fairly close to the entire Ridge Route.<ref>[[#Scott|Scott]], pp.&nbsp;37–38, 41–42</ref>

The two general routes followed by the Butterfield Overland Stage and the Southern Pacific Railroad—known respectively as the Tejon Pass Route<!--San Francisquito or Bouquet Canyon--> and the Tehachapi or Midway Route<!--Bouquet or Mint Canyon--> - were the main [[automobile]] routes between Los Angeles and the San Joaquin Valley around the start of the 20th century. The [[State Bureau of Highways (California)|State Bureau of Highways]] recommended in 1895 that a more direct route be built to shorten this distance. A [[bond issue]] was approved in 1909 and 1910 to build a [[state highway]] system, including the Ridge Route. The new [[California Highway Commission]] was unable to raise funds in the [[Eastern United States|East]], but Los Angeles again contributed funds to ensure that construction would go forward.<ref>[[#Scott|Scott]], pp.&nbsp;47–51</ref>

These routes all shared the roadway south of Saugus, which was generally flat but included the crossing of San Fernando Pass. As discussed earlier, this was deepened at the county's expense in 1858. The [[California State Legislature]] authorized a [[Toll road|turnpike]] in 1861, and it was completed in 1863, then owned by [[Edward Fitzgerald Beale]]. The cut, which came to be known as [[Beale's Cut]], was deepened by Beale, and lowered once more by the county in 1904.<ref>[[#Scott|Scott]], pp.&nbsp;23–27</ref> To improve the crossing, the county bypassed the cut with the narrow {{convert|435|ft|m|adj=on}} [[Newhall Tunnel]], for railroad traffic only, which opened in October&nbsp;1910.<ref name="[[#Scott|Scott]], p.&nbsp;57">[[#Scott|Scott]], p.&nbsp;57</ref>

===Construction===
The California Highway Commission considered several easterly routes between Saugus and Gorman: [[Soledad Canyon]] (used by the Southern Pacific) had frequent [[washout]]s; [[Mint Canyon]] (used by the Midway Route) was too long and cost too much; [[Bouquet Canyon]] (used by both automobile routes) had bad drainage; and [[San Francisquito Canyon]] (used by the Tejon Pass Route) was steep and narrow. Another possible route, through [[Piru Canyon]] to the west, was rejected because of a proposal for a dam. This route was later chosen for the ''Ridge Route Alternate'', but had to be abandoned in the 1970s due to the construction of Pyramid Lake.<ref name=s64-65>[[#Scott|Scott]], pp.&nbsp;64–65</ref>

[[File:Ridge Route construction tamping crew 1915.jpg|thumb|left|Leveling concrete pavement on the original Ridge Route,1915]]
[[File:Ridge Route construction reinforcement 1919.jpg|thumb|left|Reinforcing concrete, 1919]]

Thus, the route chosen was a direct line between Saugus and Gorman, over the top of the ridge for many miles. Due to cost and drainage concerns, and a desire to reduce altering the terrain, the road was built with 697&nbsp;curves, high in the mountains.<ref name="s64-65" /><ref name=scholar /> Construction on the Ridge Route, officially considered to run from Castaic Junction (west of Saugus on the road to [[Ventura, California|Ventura]]) to the bottom of the grade at [[Grapevine, California|Grapevine]],<ref name="[[#Scott|Scott]], pp.&nbsp;74–75"/> began in 1914.<ref>[[#Scott|Scott]], p.&nbsp;67</ref> The highway was one of the earliest projects completed by the California Highway Commission.<ref name="chpw" /> To construct the road, horse-drawn scrapers were used.<ref name="bend" />

The new road, designed with an ideal [[grade (land)|grade]] of 6&nbsp;percent (but with several 7&nbsp;percent grades, including at Grapevine,<ref>[[#Scott|Scott]], p.&nbsp;85</ref>) cut the distance by {{convert|24|mi|km}} over the Tejon Pass Route or {{convert|58|mi|km}} over the Midway Route. A [[speed limit]] of {{convert|15|mph|km/h}} was enforced between Castaic and Quail Lake, making the trip from Los Angeles to Bakersfield take about 12&nbsp;hours. On the {{convert|48|mi|km|adj=on}} Ridge Route, between Castaic Junction and Grapevine, the curves added up to a total of about 109.5 complete circles, with a minimum [[radius of curvature (mathematics)|radius]] of {{convert|70|ft|m}}.<ref name=s285>[[#Scott|Scott]], p.&nbsp;285</ref> The [[unpaved]] road, which had cost $450,000 (about ${{Formatprice|{{Inflation|US-NGDPPC|450000|1915|r=3}}}} in {{inflation-year|US-NGDPPC}}){{Inflation-fn|US-NGDPPC}}, opened in October 1915.<ref>[[#Scott|Scott]], pp.&nbsp;69–75</ref>

In addition to being part of the state highway system (as a portion of the unsigned [[Sacramento, California|Sacramento]]-Los Angeles [[Legislative Route 4 (California pre-1964)|Legislative Route 4]]),<ref>[[#Scott|Scott]], p.&nbsp;76</ref> the road was also part of the [[National Park to Park Highway]], a privately designated [[auto trail]], and became part of [[U.S. Route 99 (California)|US 99]] in 1926.<ref>{{cite map |publisher= Rand McNally |title= Junior Road Map of California and Nevada |year= 1926}}</ref> The continuation south via Saugus and the Newhall Tunnel towards Los Angeles, also part of Route 4, was added to the state highway system in 1917.<ref name="[[#Scott|Scott]], p.&nbsp;57"/>

According to the ''[[San Francisco Chronicle]]'', the new route was "one of the most remarkable engineering feats accomplished by the State Highway Commission. It is Southern California's [[masterpiece|Magnus Opus]] [''[[sic]]''] in mountain highway construction."<ref>[[#Scott|Scott]], p.&nbsp;77</ref> It was also credited with stopping efforts to split California into two states, by linking its two halves over the rugged terrain separating them.<ref>[[#Scott|Scott]], p.&nbsp;iv</ref>

Work on [[pavement (material)|paving]] the Ridge Route with {{convert|20|ft|m|adj=on}} wide<ref name=scholar/> and {{convert|4|in|mm|adj=on}} thick [[reinforced concrete]]<ref>{{cite news|work= Los Angeles Times |title= Ridge Route Now Open; Paved from End to End |date=November 16, 1919|page=VI1}}</ref> began in 1917, but was delayed until 1919 by the U.S. entry into [[World War I]].<ref name="Scott">[[#Scott|Scott]], p.&nbsp;284</ref> During work on the road, traffic was [[Detour (road)|detour]]ed via Mint and Bouquet Canyons.<ref>{{cite news | title=Ridge Route, Why it is Closed, and When it will Open | work=Los Angeles Times | date=March 30, 1919 | author=Staff | page=VI3}}</ref> The road was reopened on November 15, 1919, completely paved except north of Lebec, including the grade at Grapevine, which was only [[oiled (road)|oiled]]. After a [[bond issue]] was passed in July&nbsp;1919, this portion was paved from September&nbsp;1919 to May&nbsp;1921; the dangerous detour included a 20&nbsp;percent grade. This paving added about $1 million to the cost, for a total price of about $1.5&nbsp;million (about ${{Formatprice|{{Inflation|US-NGDPPC|1500000|1921|r=3}}}} in {{inflation-year|US-NGDPPC}}){{Inflation-fn|US-NGDPPC}}, which was not paid off until 1965.<ref name="Scott" /> [[Asphalt]] was added on top of the concrete for {{convert|20|mi|km}} south from Swede's Cut in 1922,<!--is there a reason the work ended there?--> and in 1924 a number of dangerous [[blind curve]]s were widened and straightened.<ref>[[#Scott|Scott]], pp.&nbsp;79–86</ref>

===Bypasses and the fate of the bypassed road===
[[File:Lebec 1943.jpg|thumb|left|The Ridge Route Bypass in 1943, looking north towards [[Grapevine, California|Grapevine]]]]

In 1929, the state decided to build a new [[bypass route|bypass]] of the central portion through Piru Canyon, and to upgrade the northern portion over Tejon Pass and down the hill at Grapevine. This Ridge Route Alternate, or Ridge Alternate Highway,<ref>{{cite news |work= [[Oakland Tribune]] |title= Valley Road Good, Oakland to L.A. |date= April 14, 1935|page=2B}}</ref> shortened the route by {{convert|9.6|mi|km}} and increased the minimum [[radius of curvature (mathematics)|curve radius]] from {{convert|70|to|1000|ft|m}}.<ref name="s285" /> This was done over concerns regarding the safety of the road, which had a high number of traffic accidents, as well as the highway's increased use. The old highway was to be relinquished to Los Angeles County, and incorporated into the county road system.<ref name="chpw">{{cite journal| title=New Location on The Ridge Route Unsurpassed for Mountain Alignment | author=California Division of Highways | journal=California Highways and Public Works | date=January 1930 | volume=8 | issue=1 | pages=9–10}}</ref> Construction began in 1930, and the road south of Tejon Pass was officially opened on October&nbsp;29, 1933; replacement of the Grapevine grade was completed by 1936. The road, which included a center [[suicide lane|"suicide" passing lane]], cost $3.5&nbsp;million (about ${{Formatprice|{{Inflation|US-NGDPPC|3500000|1936|r=3}}}} in {{inflation-year|US-NGDPPC}}){{Inflation-fn|US-NGDPPC}}, paid for by the state [[gas tax]].<ref name="s285" /> It was estimated that it would pay itself off in 2.5&nbsp;years with the savings in time and gas. A new bypass of the [[Newhall Tunnel]], through [[Weldon Canyon]], had been opened on May&nbsp;28, 1930, thus giving drivers a better route all the way from Los Angeles to Bakersfield.<ref name="s60-61" /> The roadway was widened to an [[Limited-access road|expressway]] by 1952 at a cost of $13.5&nbsp;million (about ${{Formatprice|{{Inflation|US-NGDPPC|13500000|1952|r=3}}}} in {{inflation-year|US-NGDPPC}}),{{Inflation-fn|US-NGDPPC}}<ref name="s285" /> providing two lanes in each direction and a center [[median (road)|median]], but retaining [[at-grade intersection]]s.<ref>[[#Scott|Scott]], pp.&nbsp;265–267</ref> <!-- The 1955 song ''[[Hot Rod Lincoln]]'' celebrates the long steep and already fairly straight section up Grapevine Canyon.-->

Even this four-lane Ridge Route Alternate was not adequate for the volume of traffic using the road, and construction of the eight-lane [[freeway]] I-5 with a minimum curve radius of {{convert|3000|ft|m}} began in June 1963. Large parts of the Ridge Route Alternate were incorporated into the new roadway, but the area through the present [[Pyramid Lake (Southern California)|Pyramid Lake]] was completely bypassed. I-5 over the mountains between Castaic Junction and Grapevine was completed on August 24, 1970, at a cost of $103&nbsp;million (about ${{Formatprice|{{Inflation|US-NGDPPC|103000000|1970|r=3}}}} in {{inflation-year|US-NGDPPC}}).{{Inflation-fn|US-NGDPPC}}<ref>[[#Scott|Scott]], pp.&nbsp;283–285</ref>

[[File:Dead-Man's Curve in Lebec, California, 2010.jpg|thumb|right|A section of the 1915 Ridge Route in Lebec, abandoned when US&nbsp;99 (later upgraded to I-5) was constructed over the [[Tejon Pass]] in order to make the travel straighter and safer]]
Meanwhile, portions of the old road continued to exist in as a [[county road]]. The longest preserved segment was the part completely bypassed by the Ridge Route Alternate, between [[Castaic, California|Castaic]] and [[State Route 138 (California)|SR 138]] near [[Sandberg, California|Sandberg]], including the portion through the [[Angeles National Forest]]. The road for about {{convert|2|mi|km|spell=in}} south from SR 138 is part of CR N2.<ref name="tgla">{{cite map|title=Los Angeles County Street Guide|year=2009|publisher=Thomas Brothers}}</ref>

Harrison Scott, a retired engineer, was traveling north on I-5 with his son in 1991, when his son speculated about the difficulty of driving over the first road through the rugged terrain. Having some extra time, and recalling a drive on the old road in 1955, they left the freeway and traveled along most of the abandoned section. Near SR&nbsp;138, Scott asked a county road crew which was resurfacing the road if it was still maintained by the county, and the crew replied that they didn't know who maintained it through the desert.<ref name=scholar/><ref name=s285-289/> It turned out that the county officially owned it but did not maintain it; Scott has since convinced the county and forest to exchange ownership with the [[Santa Anita Canyon Road]], then a county road.<ref name=interview>{{cite news |publisher= Ridge Route Preservation Organization |title= Conversation with Harrison Scott |url= http://www.ridgeroute.com/scott_interview.html |date= Fall 2006 |work= The Ridge Route Sentinel}}</ref>

[[File:Ridge Route concrete curb.jpg|thumb|right|Stretch of original pavement<!--concrete or asphalt?--> and [[curb (road)|curb]] in 2007]]
For the next six years, Scott worked on getting the road added to the [[National Register of Historic Places]],<ref name=scholar>{{cite news |first= Bob |last= Pool |work= [[Los Angeles Times]] |title= Ridge Route's Scholar |url= http://www.ridgeroute.com/story1.htm |date= October 13, 1997|pages=B1, B3}}</ref><ref name=s285-289>[[#Scott|Scott]], pp.&nbsp;285-289</ref> and finally succeeded when, on September 25, 1997, the [[National Park Service]] added the {{convert|17.6|mi|km}} within the forest to the National Register.<ref name="nris" /> He subsequently wrote a book, ''Ridge Route: The Road That United California'', from the research he had done for the National Register application, that was published in 2002.<ref name="bend">{{cite news| title=Around the Bend, Old Road May Find New Life | work=Los Angeles Times | date=August 14, 2003 | author=Pool, Bob | page=B2}}</ref>

The route was repaired in 2005 following heavy rainfall, but the road was not reopened {{as of|2013|alt=as of early 2013}}, due to objections from Angeles National Forest officials.<ref>{{cite news |url= http://articles.latimes.com/2013/jan/13/local/la-me-ridge-route-20130113 |title= Ridge Route Repair Group Runs up Against Forest Service |work= Los Angeles Times |date= January 13, 2013 |last= Pool |first= Bob |page= A21}}</ref>

==Major intersections==
{{CAinttop|length_ref=<ref name="map" />|former=yes|hatnote=off|dest_ref=<ref name="map">{{cite map|url=http://www.cosmos-monitor.com/ca/map1956/fresno-to-ventura.html|title=California Official Highway Map|year=1956|publisher=California Division of Highways}}</ref>}}
{{CAint
|county=Los Angeles
|cspan=3
|location=Castaic Junction
|lspan=2
|mile=0
|road={{Jct|state=CA|US 1926|99|dir1=south}}
|notes=Continuation south
}}
{{CAint
|road={{Jct|state=CA|SR|126}}
|mile=0
|notes=
}}
{{CAint
|location=Gorman
|mile=30
|road={{Jct|state=CA|SR|138}}
|notes=
}}
{{CAint
|county=Kern
|cspan=2
|location=none
|mile=34
|road=[[Frazier Park, California|Frazier Park]]
|notes=
}}
{{CAint
|location=Grapevine
|mile=44
|road={{Jct|state=CA|US 1926|99|dir1=north}}
|notes=Continuation north
}}
{{Jctbtm}}

==See also==
*{{portal-inline|California Roads}}
* [[1857 Fort Tejon earthquake]]
* [[Mountain Communities of the Tejon Pass]]{{Clear}}

==References==
{{Reflist|30em}}

==Works cited==
{{refbegin}}
* {{cite book |ref=Scott |last= Scott |first= Harrison Irving |title= Ridge Route: The Road That United California |year= 2003 |publisher= Harrison Irving Scott |location=Torrance, California |isbn= 0-615-12000-8}}
{{refend}}

==External links==
{{commons category}}
{{Attached KML|display=title, inline}}
* [http://www.ridgeroute.com Ridge Route Preservation Organization]
* [http://www.gbcnet.com/ushighways/US99/US99f.html Historic US 99 Guide: The Ridge Route] (includes detailed history of the Ridge Route Alternate and Interstate 5)
* [http://www.socalregion.com/highways/ridge_route/ Virtual Tour of the Ridge Route] (includes detailed history of the Ridge Route Alternate and Interstate 5)
* {{cite web | author = |url= http://www.scvhistory.com/scvhistory/ridge.htm | title= Santa Clarita Valley History in Pictures: Ridge Route & US 99| publisher= Santa Clarita Valley Historical Society <!--| accessdate=July 4, 2004-->}} (mostly old postcards)

{{featured article}}

[[Category:California highways]]
[[Category:Roads in Los Angeles County, California]]
[[Category:History of Los Angeles County, California]]
[[Category:National Register of Historic Places in Los Angeles County, California]]
[[Category:Roads on the National Register of Historic Places in California]]
[[Category:San Emigdio Mountains]]
[[Category:Sierra Pelona Mountains]]
[[Category:U.S. Route 99]]
[[Category:Former state highways in California]]