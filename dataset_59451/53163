{{Infobox journal
| title = Zeitschrift für Kristallographie &ndash; New Crystal Structures
| cover = File:Cover of Z. Kristallogr. - New Cryst. Struct. (2016) 231(1).jpg
| editor = Hubert Huppertz
| discipline = [[Chemistry]], [[Crystallography]]
| formernames = 
| abbreviation = Z. Kristallogr. - New Cryst. Struct.
| publisher = [[Walter de Gruyter]]
| country =
| frequency = Quarterly
| history = 1987–present<br />Started at volume 212
| openaccess =
| license =
| impact = 0.122
| impact-year = 2015
| website = https://www.degruyter.com/view/j/ncrs
| link1 = 
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR = 
| OCLC = 37393535
| LCCN = sn97023098
| CODEN = ZKNSFT
| ISSN = 1433-7266
| eISSN = 2197-4578
}}
'''''Zeitschrift für Kristallographie &ndash; New Crystal Structures''''' is a quarterly [[peer-reviewed]] [[scientific journal]] published in English. Its first issue was published in December 1987 and bore the subtitle "International journal for structural, physical, and chemical aspects of crystalline materials."<ref name = 1stIssueCover>{{cite journal|journal = Z. Kristallogr. - New Cryst. Struct.|page = I|volume = 212|issue = 1|doi = 10.1524/ncrs.1997.212.1.masthead|title = Zeitschrift für Kristallographie &ndash; New Crystal Structures}}</ref>  Created as a spin-off of ''[[Zeitschrift für Kristallographie]]'' for reporting novel and refined [[crystal structure]]s,<ref name = Homepage /> it began at volume 212<ref name =1stIssueCover /> in order to remain aligned with the numbering of the parent journal.  [[Paul Heinrich von Groth|Paul von Groth]], Professor of Mineralogy at the [[University of Strasbourg]], established ''Zeitschrift für Krystallographie und Mineralogie'' in 1877;<ref>{{cite book |url=https://books.google.com.au/books?id=NC6_4yip49MC&pg=PT127 |chapter=1912: The Discovery of X-Ray Diffraction and the Birth of X-Ray Analysis |title=Early Days of X-ray Crystallography |first=André |last=Authier |publisher=[[Oxford University Press]] |year=2013 |isbn=9780191635021 |series=International Union of Crystallography Texts on Crystallography}}</ref> after several name changes, the journal adopted its present name, ''[[Zeitschrift für Kristallographie - Crystalline Materials|Zeitschrift für Kristallographie &ndash; Crystalline Materials]]'', in 2010.<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101653866 |accessdate=December 13, 2016 |title=Zeitschrift für Kristallographie. Crystalline materials |date=September 2014 |website=[[National Center for Biotechnology Information]] Database |publisher=[[United States National Library of Medicine]]}}</ref>

The inaugural [[editor-in-chief|editors-in-chief]] were [[Hans Georg von Schnering]]<ref name = 1stEIC>{{cite journal|title = Zeitschrift für Kristallographie &ndash; New Crystal Structures|volume = 212|issue = 1|pages  = XVI–XVIII|doi = 10.1524/ncrs.1997.212.1.xvi|year = 1997|journal = Z. Kristallogr. - New Cryst. Struct.}}</ref><ref name = 1stEds>{{cite journal|title = Editors|volume = 212|issue = 1|page  = XV|doi = 10.1524/ncrs.1997.212.1.xv|year = 1997|journal = Z. Kristallogr. - New Cryst. Struct.}}</ref> ([[:de:Hans Georg von Schnering|German Wikipedia profile]]) of the [[Max Planck Institute for Solid State Research]] in Stuttgart<ref>{{cite web|url = http://www.chemistryviews.org/details/ezine/764029/Hans_Georg_von_Schnering_1931__2010.html|title = Hans Georg von Schnering (1931 &ndash; 2010)|author = ChemViews|date = July 30, 2010|accessdate = December 14, 2016|website = [[ChemistryViews]]|publisher = [[ChemPubSoc Europe]]}}</ref> and [[Heinz Hermann Schulz]]<ref name = 1stEIC /><ref name = 1stEds /> of the [[Ludwig-Maximilians-Universität München]].<ref>{{cite web|title = Professor Dr. Heinz Hermann Schulz|url = https://www.mvhs.de/lernen-und-gedaechtnis/forum/dozentinnen-und-dozenten/professor-dr-heinz-hermann-schulz/|language = German|accessdate = December 14, 2016|publisher = [[:de:Münchner Volkshochschule]]}}</ref>  In 2016, the editor-in-chief was [[Hubert Huppertz]] ([[Universität Innsbruck]]).<ref name = Homepage />

The journal is indexed in various databases and, according to the ''[[Journal Citation Reports]]'', had a 2015 [[impact factor]] of 0.122.<ref name = Homepage /><ref name = WoS>{{cite book |year=2016 |chapter=Zeitschrift für Kristallographie &ndash; New Crystal Structures |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in:
*[[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=December 13, 2016}}</ref>
*[[Current Contents]]/Physical, Chemical and Earth Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=December 13, 2016}}</ref>
*[[EBSCO Information Services|EBSCO databases]]<ref name = Homepage>{{cite web|url = https://www.degruyter.com/view/j/ncrs|title = Zeitschrift für Kristallographie &ndash; New Crystal Structures|accessdate = December 13, 2016|publisher = [[Walter de Gruyter]]|year = 2016}}</ref>
*[[Inspec]]<ref name=Inspec>{{cite web |url=http://www.theiet.org/resources/inspec/support/docs/loj.cfm?type=pdf |title=Inspec list of journals |format=[[PDF]] |publisher=[[Institution of Engineering and Technology (professional society)|Institution of Engineering and Technology]] |work=Inspec |accessdate=December 13, 2016}}</ref>
*[[Science Citation Index Expanded]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=[[Scopus]] |accessdate=December 13, 2016}}</ref>

==References==
{{Reflist|30em}}

==External links==
*{{Official website|www.degruyter.com/view/j/ncrs}}

{{DEFAULTSORT:Zeitschrift für Kristallographie - New Crystal Structures}}
[[Category:Chemistry journals]]
[[Category:Crystallography]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1987]]
[[Category:English-language journals]]
[[Category:Walter de Gruyter academic journals]]