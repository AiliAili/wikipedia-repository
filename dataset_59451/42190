<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=Akaflieg München Mü17 Merle
|image=Akaflieg Mue 17.JPG
|caption=The Mü17 in flight
}}{{Infobox Aircraft Type
|type=Olympic [[Glider aircraft]]
|national origin=[[Germany]]
|manufacturer=[[Akaflieg|Akaflieg München]]
|designer=
|first flight=1938
|introduced=
|retired=
|status=
|primary user=
|number built=2 prototypes + ca 60 production units<ref name="Mü17">http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=83&Itemid=116</ref>}}|}
The '''Akaflieg München Mü17 Merle''' is a single-place [[glider aircraft]] that was designed and built in [[Germany]] from 1938.<ref name="Mü17"/>

==Development==
The Mü17 Merle (a.k.a. ‘Aerostat’ or ‘Kleiderbügel’) is the oldest Akaflieg design still flying with the Fliegergruppe today. In 1938 the ISTUS conference issued a specification for an Olympic games Olympic glider to be used at the 1940 Olympics gliding competition. Akaflieg München designed and offered the Mü17 Merle to the committee at [[Rome]] in February 1939, but it was rejected in favour of the [[DFS Meise]].<ref name="Mü17 Simons">Simons, Martin. ''Sailplanes 1920-1945”. 2nd revised edition. EQIP Werbung und Verlag G.m.b.H.. Königswinter. 2006. ISBN 3-9806773-4-6</ref>

Two prototypes of the Mü17 Merle were built with emphasis on simple construction, straightforward handling and easy rigging and de-rigging, using automatic connections for the aileron and [[Air brake (aircraft)|airbrake]] controls. The plywood- and fabric-covered wooden wings are trapezoidal in plan view with swept leading edges and straight trailing edges, ailerons over the outer half of the trailing edges and [[Spoiler (aeronautics)|spoilers]] mid-chord at approx half span. Adhering to the 'Schüle München' the fuselage is a welded steel-tube space-frame with fabric covering and wooden tail surfaces, also covered in plywood and fabric. Despite failing in the Olympic selection the Mü17 Merle was put into production with around 60 units built before and after [[World War II]], many with variations such as retractable landing gear.<ref name="Mü17"/>

A Mü17 Merle 'Alpha Sierra' (D-1740), built in 1961, is still in use today at the Akaflieg München gliding club, where it is used as an intermediate single-seater for early single-seat and cross-country flying. The low weight and sedate performance suiting the aircraft to inexperienced pilots.<ref name="Mü17"/>

A 19m (62.3 feet) span wing was conceived for use with the Mü17 fuselage, as the '''Akaflieg München Mü19''', gradually losing all direct connection with the Mü17 as the design evolved into a motor-glider. Construction of the prototype was not completed due to the war situation.

==Variants==
:'''Akaflieg München Mü17''' – The original proposal, a single-seat 15m (49.2 feet) span Olympic glider contender. Two examples were built.
:'''Akaflieg München Mü19''' – A 19m span version of the Mü17, not completed due to the war situation.

==Specifications (Mü17 Merle)==
{{aerospecs
|ref=<ref name="Mü17"/>
|met or eng?=met
|crew=1
|length m=
|length ft=
|length in=
|span m=15
|span ft=49
|span in=3
|height m= 
|height ft= 
|height in= 
|wing area sqm=13.3
|wing area sqft=143
|aspect ratio=16.9
|wing profile=Mü Scheibe
|empty weight kg=157.7
|empty weight lb=348
|gross weight kg=300
|gross weight lb=661
|eng1 number=
|eng2 number= 
|max speed kmh= 
|max speed mph= 
|stall speed kmh=
|stall speed mph=
|g limits=
|glide ratio=26 @ 64km/h (40 mph)
|sink rate m/s=0.63 @ 54km/h (34 mph)
|sink rate ft/min=124}}

==References==
{{reflist}}
*[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=83&Itemid=116]
*Simons, Martin. ''Sailplanes 1920-1945”. 2nd revised edition. EQIP Werbung und Verlag G.m.b.H.. Königswinter. 2006. ISBN 3-9806773-4-6
*[http://www.akaflieg.vo.tum.de/index.php?option=com_content&task=view&id=144&Itemid=151]

==External links==
*[http://www.sailplanedirectory.com/munchen.htm]

{{Akaflieg München aircraft}}

{{DEFAULTSORT:Akaflieg Munchen Mu17 Merle}}
[[Category:German sailplanes 1930–1939]]
[[Category:Akaflieg München aircraft|Mu17]]