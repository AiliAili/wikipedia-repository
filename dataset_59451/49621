{{Infobox short story <!--See [[Wikipedia:WikiProject Novels]]-->
| name              = A Drama in the Air
| image             = [[File:'A Drama in the Air' by Émile Bayard 4.jpg|250px]]
| title_orig        = Un drame dans les airs
| translator        = Anne T. Wilbur
| author            = [[Jules Verne]]
| country           = [[France]]
| language          = [[French language|French]]
| genre             = 
| published_in      = "Museé des Familles"
| publication_type  =
| publisher         = 
| media_type        = 
| pub_date          = August 1851
| english_pub_date  = May 1852
}}

"'''A Drama in the Air'''" ({{lang-fr|"'Un drame dans les airs'"}}) is an adventure [[short story]] by [[Jules Verne]]. The story was first published in August 1851 under the title "Science for families. A Voyage in a Balloon" ("La science en famille. Un voyage en ballon") in ''[[Musée des familles]]'' with five illustrations by [[Alexandre de Bar]].<ref name=biblio>{{cite web|last=Dehs|first=Volker|title=The Complete Jules Verne Bibliography: II. Short Stories|url=http://jv.gilead.org.il/biblio/stories.html|work=Jules Verne Collection|publisher=Zvi Har’El|accessdate=11 February 2013|author2=Jean-Michel Margot, Zvi Har’El}}</ref> In 1874, with six illustrations by [[Émile Bayard|Émile-Antoine Bayard]], it was included in ''[[Doctor Ox]]'', the only collection of Jules Verne’s short stories published during Verne’s lifetime. An English translation by Anne T. Wilbur, published in May 1852 in ''[[Sartain's Magazine|Sartain’s Union Magazine of Literature]]'', marked the first time a work by Jules Verne was translated into the English language.

==Plot outline==
Just as the narrator starts the ascent of his [[Balloon (aircraft)|balloon]], a stranger jumps into its car. The unexpected passenger's only intent is to take the balloon as high as it will go, even at the cost of his and pilot's life. The intruder takes advantage of the long journey to recount the history of incidents related to the epic of [[lighter than air|lighter-than-air]] travel.

This short story foreshadows Verne's first novel, ''[[Five Weeks in a Balloon]]''.

==English publication==
The story has appeared in English translation in the following forms.<ref name=Evans>{{cite journal|last=Evans|first=Arthur B.|title=A Bibliography of Jules Verne’s English Translations|journal=Science Fiction Studies|date=March 2005|volume=XXXII|series=1|issue=95|pages=105–141|url=http://jv.gilead.org.il/evans/VerneTrans%28biblio%29.html|accessdate=11 February 2013}}</ref>

As "A Voyage in a Balloon" (translated by Anne T. Wilbur):
*1852 – ''Sartain’s Union Magazine of Literature''

As "A Drama in Mid-Air" (translated by Abby L. Alger):
*1874 – ''From the Clouds to the Mountains'', Boston: Gill

As "A Drama in the Air" (translated by George M. Towle):
*1874 – ''Dr. Ox and Other Stories'', Boston: Osgood
*1876 – ''A Winter Amid the Ice, and Other Stories'', London: Sampson Low
*1911 – ''Works of Jules Verne'', Vol.1, New York: Vincent Parke,  ed. Charles F. Horne
*1964 – ''Dr. Ox, and Other Stories'', London: Arco/Westport, CT: Associated Booksellers: Fitzroy Edition, ed. I. O. Evans
*1999 – ''The Eternal Adam, and other Stories'', London: Phoenix, ed. Peter Costello

==References==
{{reflist}}

==External links==
{{Commons category|A Drama in the Air}}
{{wikisource}}
* [http://jv.gilead.org.il/rpaul/Un%20drame%20dans%20les%20airs/ Illustrations] by Émile-Antoine Bayard
* [http://jv.gilead.org.il/pg/11589-h/ Un drame dans les airs] available at [http://jv.gilead.org.il/ Jules Verne Collection] {{fr icon}}

{{Jules Verne}}

{{DEFAULTSORT:Drama in the Air, A}}
[[Category:1851 short stories]]
[[Category:Short stories by Jules Verne]]
[[Category:Works originally published in Musée des familles]]

[[cs:Doktor Ox#Drama ve vzduchu]]