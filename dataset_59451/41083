'''VIDA: Women in Literary Arts''' is an organization based in the United States that "seeks to explore critical and cultural perceptions of writing by women through meaningful conversation and the exchange of ideas among existing and emerging literary communities." <ref>{{cite web|title=Mission and History|url=http://www.vidaweb.org/about-vida/mission|publisher=Vida--Women in Literary Arts|accessdate=16 May 2013}}</ref>

== History ==

VIDA: Women in Literary Arts was co-founded in 2009 by poets [[Erin Belieu]] and [[Cate Marvin]]. Belieu and Marvin, because they are both poets and teachers, had been acquainted with one another for some time. However, due to there being a small amount of opportunities for women in the literary community, they perceived themselves as being in competition with one another. That perception changed when Marvin sent Belieu an email detailing how isolated she felt as a female writer, and questioning why there was no feminist conversation happening in literary circles. Belieu then forwarded the email to hundreds of writers, and it received a huge response. Based on that response, Belieu and Marvin decided to co-found VIDA: Women in Literary Arts.<ref>{{cite web|last=Belieu|first=Erin|title=A Vision to Lift Women's Voices|url=http://www.mariashriver.com/blog/2013/04/A-vision-to-lift-womens-voices-erin-belieu|publisher=Maria Shriver|accessdate=30 April 2013}}</ref><ref name="lambdaliterary">{{cite web|last=Levine|first=Julie|title=Erin Belieu: The VIDA Count and Women in Publishing|url=http://www.lambdaliterary.org/features/05/11/erin-belieu-the-vida-count-and-women-in-publishing/|publisher=Lambda Literary|accessdate=23 May 2013}}</ref>

=== The Count ===

VIDA: Women in Literary Arts is best known for The Count. The Count is a yearly inventory of how many women and men are published in, or have their books reviewed by, notable literary magazines. The Count was first performed, and the results published, in 2010. It showed that significantly lower numbers of women than men had been published or had their books reviewed by notable literary magazines.<ref name="vidaweb">{{cite web|title=The Count|url=http://www.vidaweb.org/the-count-2010|publisher=Vida|accessdate=16 May 2013}}</ref>

The 2010 version of The Count started a large conversation about inequity in the literary world. The comments section below the original publication of The Count shows the widely varied opinions about the relevance of, and reason for, the disparity in the numbers. Some people were not surprised, some were surprised and angered, some were inspired to act, some questioned the validity of the methods used to collect the numbers, and some questioned whether relevant information had been excluded from the numbers.<ref name="vidaweb" />

The conversation surrounding The Count propelled VIDA forward, and it continues to grow in membership and notoriety. Since 2010, VIDA has published a yearly version of The Count. The reactions of the magazines included in The Count are as varied as the comments section below The Count on VIDA's website. Some magazines have ignored the numbers, others have challenged the validity of the numbers, and some have actively tried to address the issue and change it.<ref>{{cite web|title=The 2011 Count Articles|url=http://www.vidaweb.org/resources|publisher=Vida|accessdate=30 May 2013}}</ref>

== Reactions ==

Responses to "The Count" have been widely varied, and VIDA's pie charts have been reproduced in many periodicals and journals.  
The conversation spurred by VIDA's Count has been explained in the ''[[Mother Jones (magazine)|Mother Jones]]'' article "Where are the Women Writers." [http://www.motherjones.com/media/2012/04/women-writers-vida-asme] 
Some magazines, such as ''[[The Coffin Factory]],'' have openly and bluntly criticized VIDA for The Count, claiming that the questions asked by VIDA, and that the methods used to come to the conclusion that there is gender disparity in the publishing world are flawed.<ref>{{cite web|last=Isaacman|first=Laura|title=Why the VIDA Count is Bullshit|url=http://thecoffinfactory.com/the-vida-count-is-bullshit/|publisher=The Coffin Factory|accessdate=17 May 2013}}</ref> The most common criticism of VIDA's methods is that the numbers don't include information on how many submissions are made to each magazine by men and women respectively.<ref name="lambdaliterary" /> The assertion is that if there is a disparity in the number of submissions by men versus women, then the presence of a disparity between published work by men versus women makes sense and is not evidence of gender bias. VIDA contributor and poet Danielle Pafunda responded to this concern in her article "Why the Submissions Numbers Don't Count." Here, she details seven reasons why submissions numbers are ultimately irrelevant.<ref>{{cite web|last=Pafunda|first=Danielle|title=Why the Submissions Numbers Don't Count|url=http://www.vidaweb.org/why-the-submissions-numbers-dont-count|publisher=Vida|accessdate=17 May 2013}}</ref>

Other magazines, such as ''[[Granta]],'' ''[[Tin House]],'' and ''[[Boston Review]]'' have responded to The Count by making a conscious effort to remedy the gender disparity within their journals. Not only do the editors of these publications say that they are making these efforts, but the numbers of The Count show this to be the case.<ref>{{cite web|last=Temple|first=Emily|title='It Isn't Rocket Science': 'Tin House' and 'Granta' Editors on How to Run a Publication That Isn't Sexist|url=http://flavorwire.com/376951/it-isnt-rocket-science-tin-house-and-granta-editors-on-how-to-run-a-publication-that-isnt-sexist|publisher=Flavorwire|accessdate=17 May 2013}}</ref>

The Count has also inspired some activism by individuals, such as Lorraine Berry who, in response to ''[[Harper's]]'' highly disparate numbers for three years running, published her letter "Why I’m Canceling My Subscription: An Open Letter to ‘Harper’s’ from a Loyal Reader." In this letter, Berry details her history as a ''[[Harper's]]'' subscriber, including how much she has enjoyed reading it and how much she has been inspired by it. She then goes on to say how disheartened she was to see that ''[[Harper's]]'' has failed to do anything to correct the gender disparity in their publication, and that she will be cancelling her subscription as a result.<ref>{{cite web|last=Berry|first=Lorraine|title=Why I'm Canceling My Subscription: An Open Letter to 'Harper's' from a Loyal Reader|url=http://flavorwire.com/377631/why-im-canceling-my-subscription-an-open-letter-to-harpers-from-a-loyal-reader|publisher=Flavorwire}}</ref>

== Her Kind ==

In May 2012 VIDA: Women in Literary Arts launched their online forum and blog "Her Kind," taking its name from an [[Anne Sexton]] poem. Her Kind "serves as a forum to create lively conversation about issues that are often dismissed or overlooked by the mainstream media."<ref name="herkind">{{cite web|title=Her Kind: Mission and History|url=http://herkind.org/mission-history|publisher=Vida|accessdate=23 May 2013}}</ref><ref>{{cite web|last=Sexton|first=Anne|title=Her Kind|url=http://www.poets.org/viewmedia.php/prmMID/15297|publisher=Poets.org|accessdate=23 May 2013}}</ref>

HerKind retired in 2014 and archived its articles and essays to VIDAweb.org, which continues to publish up-to-date content.

== Similar Organizations ==

[[Canadian Women in Literary Arts]] (CWILA) was founded by Gillian Jerome, a Canadian poet and essayist. "CWILA (Canadian Women in the Literary Arts) is an inclusive national literary organization for people who share feminist values and see the importance of strong and active female perspectives and presences within the Canadian literary landscape." <ref>{{cite web|title=About Us|url=http://cwila.com/wordpress/about-us/|publisher=Canadian Women in Literary Arts|accessdate=23 May 2013}}</ref>

Following VIDA's lead, CWILA began publishing their own version of The Count in 2012. CWILA focuses on Canadian literary magazines.<ref>{{cite web|title=2011 CWILA Numbers|url=http://cwila.com/wordpress/?p=253|publisher=Canadian Women in Literary Arts|accessdate=23 May 2013}}</ref>

<ref>http://www.motherjones.com/media/2012/04/women-writers-vida-asme</ref>

== References ==
{{Reflist}}

[[Category:Feminist literature]]
[[Category:Feminist organizations]]