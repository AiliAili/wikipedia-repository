{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. -->
| name = Nicholas. A. Basbanes
| image = Nicholas A. Basbanes in China.jpg
| image_size =
| alt =
| caption = Basbanes in China conducting research for his book, ''On Paper: The Everything of Its Two-Thousand-Year History.''
| pseudonym =
| birth_name =
| birth_date = {{birth date and age|1943|05|25}}
| birth_place = [[Lowell, Massachusetts]]
| death_date = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place =
| resting_place =
| occupation = Author, journalist and lecturer
| language = English
| nationality = American
| ethnicity =
| citizenship =
| education =
| alma_mater = [[Bates College]] (BA), [[Pennsylvania State University]] (MA)
| period =
| genre = Nonfiction, journalism
| subject = Books and book culture
| movement =
| notableworks =
| spouse = Constance Valentzas Basbanes
| partner =
| children = Barbara Basbanes Richter
 Nicole Basbanes Claire
| relatives = Georgia Koumoutseas Basbanes, mother
 John G. Basbanes, father
| influences =
| influenced =
| awards =
| signature =
| signature_alt =
| website = <!-- www.nicholasbasbanes.com -->
| portaldisp =
}}

'''Nicholas Andrew Basbanes''' (born May 25, 1943, in [[Lowell, Massachusetts]]) is an American author who writes and lectures widely about books and book culture. His subjects have included the “eternal passion for books” (''A Gentle Madness'');<ref name="MichaelDirda">Michael Dirda, “Genuine Book Cases,” ''Washington Post'', July 30, 1995.</ref> the history and future of libraries (''Patience & Fortitude'');<ref>Merle Rubin, “Can you have too many books? Musings on Bibliophiles From Classical Alexandria to the Internet,” ''Christian Science Monitor'', December 27, 2001.[http://www.csmonitor.com/2001/1227/p17s1-bogn.html]</ref> the “willful destruction of books” and the “determined effort to rescue them” (''A Splendor of Letters'');<ref>André Bernard, “Fear of Book Assasination [sic] Haunts Bibliophile’s Musings,” ''The New York Observer'', December 15, 2003. [http://observer.com/2003/12/fear-of-book-assasination-haunts-bibliophiles-musings/]</ref> “the power of the printed word to stir the world” (''Every Book Its Reader'')<ref>Brigitte Weeks, “The Manifold Beauties of Books,” ''Washington Post'', January 5, 2006. [http://www.washingtonpost.com/wp-dyn/content/article/2006/01/04/AR2006010401710.html]</ref> and the invention of paper and its effect on civilization (''On Paper: The Everything of Its Two-Thousand-Year History'').<ref name="ncsu.edu">Martin A. Hubbe,"On Paper - A Celebration of Two Millennia of the Work and Craft of Papermakers," ''BioResources,'' 8(4), 4791-4792, November 2013.[http://www.ncsu.edu/bioresources/BioRes_08/BioRes_08_4_4791_Hubbe_Editorial_OnPaper_Basbanes_4414.pdf]</ref>

== Early life and education ==

Nicholas Basbanes is the son of two first-generation Greek-Americans. He graduated from [[Lowell High School (Massachusetts)|Lowell High School]] in 1961, and earned a bachelor’s degree in English from [[Bates College]] in [[Lewiston, Maine]], in 1965. Following a year of graduate study at [[Pennsylvania State University]], he did research for his master’s thesis in [[Washington, D.C.]], then entered U. S. Navy [[Officer Candidate School]] in [[Newport, Rhode Island]]. He attended the [[Defense Information School]] in the spring of 1968 and received his master’s degree in journalism in 1969 while serving aboard the aircraft carrier {{USS|Oriskany|CV-34}} during the first of two combat deployments he made to [[Yankee Station]] in the [[Gulf of Tonkin]], off the coast of Vietnam.<ref name="articles.sun-sentinel.com">Chauncey Mabe, “The Book On Books: Nicholas Basbanes Brings a Journalist's Training and Sensibility to Writing About, well, Writing, and Books,” ''South Florida Sun Sentinel'' (Fort Lauderdale, FL), March 14, 2004.
[http://articles.sun-sentinel.com/2004-03-14/entertainment/0403101234_1_stephen-blumberg-nicholas-basbanes-books]</ref>

== Early career ==

Discharged from active duty in 1971, Basbanes went to work as a general assignment reporter for ''The Evening Gazette'' in [[Worcester, Massachusetts]], specializing in investigative journalism. In 1978, he was appointed books editor of a sister publication, the Worcester ''Sunday Telegram'', a full-time position that included writing a weekly column for which he would interview more than a thousand authors over the next twenty-one years.

When Basbanes left the newspaper (by then known as the ''[[Telegram & Gazette]]'') in 1991 to complete his first book, he continued writing the column and distributed it through Literary Features Syndicate, an agency that he formed that placed it in more than thirty publications nationwide. Two selections of his literary journalism were collected in ''Editions & Impressions'' (2007) and ''About the Author'' (2010).<ref name="articles.sun-sentinel.com"/>

== Writing ==

Basbanes' first book, ''A Gentle Madness: Bibliophiles, Bibliomanes, and the Eternal Passion for Books'', was published in 1995. It has since appeared in eight hardcover editions and more than twenty paperback printings, surprising figures for a work of nonfiction whose topic was dismissed as too arcane for a general readership by many New York editors who had passed on the opportunity to publish it.<ref>William A. Davis, “Bible for Bibliophiles: Basbanes' ‘A Gentle Madness’ Confounds the Naysayers, ” ''Boston Globe'', June 26, 1996, reprinted ''Bates Magazine'', Spring 1997. [http://abacus.bates.edu/pubs/mag/97-Spring/bibliophile.html]  and John Baker, “A Mania for Books,” ''Publishers Weekly'', vol. 252, issue 45, November 11, 2005. [http://www.publishersweekly.com/pw/print/20051114/20772-a-mania-for-books.html]</ref>

Its topic is book collecting, but its focus is human nature – what Basbanes calls the “gentle madness” of [[bibliomania]]. Of the many people profiled in ''A Gentle Madness'', none has created more interest than [[Stephen Blumberg]], arguably the most accomplished book thief of the twentieth century, and to this day a subject of fascination for the bizarre methods he used to steal volumes from more than three hundred libraries in North America.<ref name="MichaelDirda" />

''A Gentle Madness'' was named a ''New York Times'' notable book of the year,<ref>"Notable Books of the year 1995,"''New York Times,''December 3, 1995.</ref> and was a finalist for the [[National Book Critics Circle Award]] in nonfiction for1995.<ref>[http://bookcritics.org/awards/past_awards/#1995%20Awards NBCC Finalists]</ref> In 2010, the ''Wall Street Journal'' named it one of the most influential works about book collecting published in the twentieth century.<ref>[https://online.wsj.com/article/SB10001424052748703843804575533971996423764.html ''Wall Street Journal'', October 9, 2010]</ref> In 2012, an updated paperback edition and a new electronic version of the book were published.<ref>{{cite web|url=http://store.finebooksmagazine.com/a-gentle-madness---a-new-edition.aspx |title=A Gentle Madness - A New Edition! |publisher=Store.finebooksmagazine.com |date= |accessdate=2013-04-15}}</ref>

By 2003, with the publication of ''A Splendor of Letters'', Basbanes was already acknowledged as a leading authority on books and book culture. One reviewer commented, “No other writer has traced the history of the book so thoroughly or so engagingly,”<ref>Andre Bernard,"Fear of Book Assasination [sic] Haunts Bibliophile’s Musings," ''The New York Observer,'' December 15, 2013. [http://observer.com/2003/12/fear-of-book-assasination-haunts-bibliophiles-musings/]</ref> and Yale University Press chose him to write its 2008 centennial history, ''A World of Letters'', which chronicled the inside stories of its classic books from conception to production.<ref>{{cite web|url=http://yalepress.yale.edu/yupbooks/centennial/worldofletters.asp |title=Yale Press Centennial: A World of Letters by Nicolas A. Basbanes |publisher=Yalepress.yale.edu |date= |accessdate=2013-04-15}}</ref>

Basbanes' ninth book, ''On Paper: The Everything of Its Two-Thousand-Year History'',<ref>Barbara Hoffert, “Barbara’s Picks, October 2013, Pt. 3: Basbanes, Boyle, Cahill, Drabble, Goleman, Holmes, Lepore, MacGregor, Venter, & Winterson,” ''Library Journal'', April 15, 2013. [http://reviews.libraryjournal.com/2013/04/prepub/picks/barbaras-picks-oct-2013-pt-3-basbanes-boyle-cahill-drabble-goleman-holmes-lepore-macgregor-venter-winterson/]</ref> is not only a consideration of paper as a principal medium for the transmission of text over the past ten centuries, but also a wider examination of the ubiquitous material itself.<ref name="ncsu.edu"/> The eight-year project, which was released in October 2013, was supported in part by the award of a [[National Endowment for the Humanities]] Research Fellowship in 2008.<ref>[http://www.nhalliance.org/bm~doc/fy08nehmassachusetts.pdf NEH 2008 Grant Obligations Massachusetts]</ref> It was named a notable book by the American Library Association;<ref>[http://www.ala.org/news/press-releases/2014/01/rusas-reveals-notable-books-list-winners-outstanding-fiction-nonfiction-and ''ALA News'', "2014 Notable Books List," January 26,2014]</ref> one of the best books of the year by ''Kirkus Reviews'',<ref>[https://www.kirkusreviews.com/issue/best-of-2013/section/nonfiction/ ''Kirkus'', "Best Books of 2013," Best Non-Fiction Books of 2013]</ref>''Mother Jones''<ref>''Mother Jones'',"MoJo Staff Picks: The Best Books of 2013," Culture, December 17, 2013.[http://www.motherjones.com/media/2013/12/mother-jones-staff-picks-best-books-2013],</ref> and ''Bloomberg'';<ref>Stephen L. Carter,"Best Books of 2013: Slavery and Bibliophilia," ''Bloomberg'' The Ticker, December 3, 2013.[https://www.bloomberg.com/news/2013-12-03/best-books-of-2013-slavery-and-bibliophilia.html]</ref> a "favourite" book of the year by the ''National Post'' (Canada)<ref>''National Post'',"Open Book: Philip Marchand’s favourite books of 2013," Arts, Afterword, December 27, 2013.[http://arts.nationalpost.com/2013/12/27/open-book-philip-marchands-favourite-books-of-2013/]</ref> and was a finalist for the 2014 [[Andrew Carnegie Medal for Excellence in Nonfiction]].<ref>ALA,org, Andrew Carnegie Medals for Excellence in Fiction and Nonfiction, Awards Finalists, April 7, 2014.[http://www.ala.org/awardsgrants/carnegieadult/short-lists]</ref>

In addition to his books, Basbanes writes for numerous newspapers, magazines, and journals.  He writes the “Gently Mad” column for ''Fine Books & Collections'' magazine, and lectures widely on book-related subjects.

In July 2015, Basbanes received one of the inaugural grants from the Public Scholar program, a major new initiative from the National Endowment for the Humanities, for his work-in-progress, ''Cross of Snow: The Love Story and Lasting Legacy of American Poet Henry Wadsworth Longfellow (1807-1882)''.<ref>[[Ron Charles]], "Uncle Sam Wants YOU to Read 'Popular' Scholarly Books," ''Washington Post'', The Style Blog, July 28, 2015.[http://www.washingtonpost.com/blogs/style-blog/wp/2015/07/28/can-the-neh-make-scholarship-popular/]</ref> The Public Scholar program is designed to promote the publication of scholarly nonfiction books for general audiences.

The [[Texas_A%26M_University_Libraries#Cushing_Memorial_Library_and_Archives|Cushing Memorial Library and Archives]] of [[Texas A&M University]] acquired Basbanes' papers as the Nicholas A. Basbanes Collection in December 2015. The collection includes archives of Basbanes’ professional career as an author and literary journalist, as well as a significant portion of his personal library. Highlights of the collection include research materials related to the writing of his nine books and approximately eight hundred books inscribed to him over the course of his career.<ref>{{cite press release |url=https://library.tamu.edu/about/news-and-events/2015/12/Basbanes-Collection.html |title=Basbanes Collection Added to Cushing Library |language= |publisher=[[Texas_A%26M_University_Libraries#Cushing_Memorial_Library_and_Archives|Cushing Memorial Library and Archives, Texas A&M University]] |date=2015-12-07 |accessdate=2016-01-05 }}</ref>

== Bibliography ==

*''On Paper: The Everything of Its Two-Thousand-Year History'', New York: Alfred A. Knopf, 2013 (ISBN 9780307266422)
*''About the Author: Inside the Creative Process'', Durham, NC: Fine Books Press, 2010 (ISBN 9780979949135)
*''A World of Letters: Yale University Press, 1908-2008'', New Haven: Yale University Press, 2008 (ISBN 9780300115987)
*''Editions & Impressions: Twenty Years on the Book Beat'', Durham, N.C.: Fine Books Press, 2007 (ISBN 9780979949104)
*''Every Book its Reader: The Power of the Printed Word to Stir the World'', New York: HarperCollins, 2005 (ISBN 9780060593247)
*''A Splendor of Letters: The Permanence of Books in an Impermanent World'', New York: HarperCollins, 2003 (ISBN 9780060082871)
*''Among the Gently Mad: Perspectives and Strategies for the Book-Hunter in the 21st Century'', New York: Henry Holt & Co., 2002 (ISBN 9780805051599)
*''Patience & Fortitude: A Roving Chronicle of Book People, Book Places, and Book Culture'', New York: HarperCollins, 2001 (ISBN 9780060196950)
*''A Gentle Madness: Bibliophiles, Bibliomanes, and the Eternal Passion for Books'', New York: Henry Holt & Co., 1995. (ISBN 9780805061765); Durham, NC: Fine Books Press, 2012 (updated print edition, and first electronic edition) (ISBN 9780979949166)

== Selected journalism and op-ed essays ==
*[https://www.nytimes.com/1991/04/14/books/bibliophilia-still-no-cure-in-sight.html?pagewanted=all&src=pm “Bibliophilia: Still No Cure in Sight”], ''New York Times'', April 14, 1991.
*[http://articles.philly.com/1995-02-22/entertainment/25705891_1_harlem-renaissance-literary-prize-dorothy-west "A Voice Revived Dorothy West Was Part Of The Harlem Renaissance In The '20s And '30s. Now, At Age 87, She Has A New Novel - Her First In Nearly 50 Years"],''Philadelphia Inquirer'', February 22, 1995.
*[http://articles.latimes.com/2004/jan/12/opinion/oe-basbanes12 “Fragile Guardians of Culture”], ''Los Angeles Times'', January 12, 2004. 
*[http://www.boston.com/news/globe/editorial_opinion/oped/articles/2004/02/29/a_bitter_end_for_hub_gem/ “A Bitter End for Hub Gem”], ''Boston Globe'', Feb. 29. 2004.
*[http://articles.latimes.com/2004/may/06/opinion/oe-basbanes6 “Honorable Death for a Rusty Warrior”], ''Los Angeles Times'', May 6, 2004.
*[http://articles.latimes.com/2006/apr/24/opinion/oe-basbanes24 “Bibliophiles Inside the Wire”], ''Los Angeles Times'', April 24, 2006.
*[http://www.csmonitor.com/2006/0508/p09s01-coop.html “Iraq: The Cradle of the Written Word”], ''Christian Science Monitor'', May 8, 2006.
*[http://www.csmonitor.com/2006/0526/p09s01-coop.html “When We Said Goodbye to the USS Oriskany”], ''Christian Science Monitor'', May 26, 2006.
*[http://www.smithsonianmag.com/arts-culture/famous_once_again.html “Famous Once Again”], ''Smithsonian'', February 2007.
*[https://www.bates.edu/magazine/back-issues/y2007/spring07/postcards-from-bates/your-page/ “The Bard Out Loud”], ''Bates Magazine'', June 2007, reprinted from ''The Book That Changed My Life: 71 Remarkable Writers Celebrate the Books That Matter Most to Them'' (Gotham, 2006).
*[http://www.nicholasbasbanes.com/travelogue/china.phtml “The Paper Trail: Hand Papermaking in China”], ''Fine Books & Collections'', March/April 2008.
*[http://www.latimes.com/opinion/commentary/la-oe-basbanes-paperless-society-myth-20131208,0,7402105.story#axzz2mnqwQdCG "A Paperless Society? Not So Fast."] ''Los Angeles Times'', December 8, 2013. 
*[http://www.neh.gov/humanities/2014/januaryfebruary/feature/paper-trail "Paper Trail"] ''Humanities'', January/February 2014, Volume 35, Number 1.
*[http://www.neh.gov/humanities/2014/novemberdecember/feature/summer-camp-book-nerds-why-i-keep-returning-rare-book-schoo "Summer Camp for Book Nerds"] ''Humanities'', November/December 2014, Volume 35, Number 6.
*[http://www.finebooksmagazine.com/issue/1301/stuart-rose-1.phtml "High Spots in Human Progress"] ''Fine Books & Collections'', December 2014.
*[http://www.neh.gov/humanities/2015/septemberoctober/feature/romantic-notion-one-scholars-lifetime-devotion-the-letters- "A Romantic Notion: One Scholar’s Lifetime of Devotion to the Letters of Robert Browning and Elizabeth Barrett Browning"] ''Humanities'', September/October 2015 | Volume 36, Number 5.
*[https://www.finebooksmagazine.com/issue/1403/dante-1.phtml "A Dante Devotee"] ''Fine Books & Collections'', Summer 2016. 

== C-SPAN appearances ==
*[http://www.booknotes.org/Watch/67215-1/Nicholas+Basbanes.aspx Booknotes, Interview with Brian Lamb], October 15, 1995 (59 minutes)
*[http://www.c-span.org/video/?166606-1/abel-berland-rare-book-library-sale Nicholas Basbanes speaks about the Abel Berland library and is interviewed about 18 items on auction at the Abel Berland Rare Book Library Sale at Christie's in New York City] October 8, 2001 (1 hour, 53 minutes) 
*At the Miami Book Fair, [http://www.c-spanvideo.org/program/173891-3 Literary Lives, panelist], November 23, 2002 (65 minutes) 
*[http://www.c-spanvideo.org/program/190531-1 After Words, Interview with David Kipen], January 4, 2006 (58 minutes)
*[http://www.c-spanvideo.org/program/283398-1 Nicholas Basbanes Tour of His Home Library], October 21, 2008 (57 minutes)
*[http://www.c-span.org/video/?316782-1/history-paper Nicholas Basbanes talks about his book, ''On Paper: The Everything of Its Two-Thousand-Year History'' at the National Archives in Washington, DC], December 13, 2013 (61 minutes)

== NPR appearances ==

*''Diane Rehm Show'', November 8, 2001 ([http://thedianerehmshow.org/shows/2001-11-08/nicholas-basbanes-patience-and-fortitude-harper-collins ''Patience & Fortitude'']) 
*''Diane Rehm Show'', February 9, 2004 ([http://thedianerehmshow.org/shows/2004-02-09 ''A Splendor of Letters''])
*''TTBOOK'', "Lost & Found Books with Steve Paulson," July 11, 2004 ([http://www.ttbook.org/book/nicholas-basbanes-book-destruction Nicholas Basbanes on Book Destruction ''A Splendor of Letters''])
*''Diane Rehm Show'', January 31, 2006 ([http://thedianerehmshow.org/shows/2006-01-31/nicholas-basbanes-every-book-its-reader-harper-collins ''Every Book Its Reader''])
*''Airtalk with Larry Mantle'', April 30, 2008 ([http://www.scpr.org/programs/airtalk/2008/04/30/3908/a-literary-conversation-with-nicholas-basbanes/ ''Editions and Impressions'']) 
*''Diane Rehm Show'', August 24, 2011 Readers' Review, guest panelist: [http://thedianerehmshow.org/shows/2011-08-24/readers-review-guernsey-literary-and-potato-peel-pie-society-mary-ann-shaffer-and-a ''The Guernsey Literary and Potato Peel Pie Society''], by Mary Ann Shaffer and Annie Barrows
*''The Point with Mindy Todd'', July 2012 ([http://www.wgbh.org/programs/The-Point-298/episodes/Nicholas-Basbanes-42613 on the new edition of ''A Gentle Madness'']) 
*''Diane Rehm Show'', October 17, 2013 ([http://thedianerehmshow.org/shows/2013-10-17/nicholas-basbanes-paper ''On Paper: The Everything of Its Two-Thousand-Year History''])
*''Radio Boston with Meghna Chakrabarti and Anthony Brooks'', October 25, 2013 ([http://radioboston.wbur.org/2013/10/25/on-paper ''On Paper''])
*''RadioWest with Doug Fabrizio'', November 3, 2013 ([http://radiowest.kuer.org/post/paper ''On Paper'']) 
*''Leonard Lopate Show'', November 14, 2013 ([http://www.wnyc.org/people/nicholas-basbanes/ ''On Paper: The Everything of Its Two-Thousand-Year History'']) 
*''Word of Mouth with Virginia Prescott'', December 17, 2013 ([http://nhpr.org/post/paper-dead-long-live-paper "Paper is Dead! Long Live Paper!])
*''Morning Edition'', May 26, 2015 ([http://www.npr.org/2015/05/26/408794149/dont-write-off-paper-just-yet "Don't Write Off Paper Just Yet"])

== Lectures ==

*January 31, 2006, Library of Congress, "The Power of the Printed Word,"[http://www.read.gov/webcasts/basbanes.html]
*November 14, 2013, the Strand bookstore in New York City, "Nicholas Basbanes on the Strange and Fascinating History of People and Paper," {{YouTube|F42tcvvV_pA}}

== References ==
{{reflist|30em}}

== External links ==
*[http://www.nicholasbasbanes.com Nicholas Basbanes Website]
*[http://www.worldcat.org/identities/lccn-n94077608/ Works by or about Nicholas Basbanes in libraries (WorldCat Identities)]
*[http://yalepress.yale.edu/yupbooks/pdf/BasbanesComplete.pdf ''A World of Letters: Yale University Press, 1908—2008'']  
*[http://www.bostonathenaeum.org/node/675 Boston Athenæum Author Profile]
*[http://mulibraries.missouri.edu/about/giving/W12_LibraryConnex_FINAL.pdf "Leading Authority on Books to Speak at Library Society Dinner," ''Library Connections,''University of Missouri, Winter 2012. ]
*[http://www.americanwritersmuseum.org/person/nick-basbanes/ American Writers Museum Advisory Council]
*[http://www.abebooks.com/docs/RareBooks/Avid-Collector/Jun06/basbanes.shtml “Nicholas Basbanes: A Love Affair with Books”], Abebooks.com, Rare Book Room
*Pradeep Sebastian, [http://www.thehindu.com/opinion/columns/pradeep_sebastian/endpaper-beyond-book-collection/article3584059.ece  “Endpaper: Beyond Book Collection”], ''The Hindu'', July 1, 2012
*Pradeep Sebastian,[http://www.thehindu.com/books/books-columns/scroll-down-memory-lane/article5075409.ece "Endpaper: Scroll Down Memory Lane"], ''The Hindu'', August 31, 2013
*[http://www.librarything.com/author/basbanesnicholas Library Thing Profile]
*William A. Davis, [http://abacus.bates.edu/pubs/mag/97-Spring/bibliophile.html “Bible for Bibliophiles: Basbanes' ‘A Gentle Madness’ Confounds the Naysayers”]. ''Boston Globe'', June 26, 1996, reprinted ''Bates Magazine'', Spring 1997.
*John Baker, “A Mania for Books”, ''Publishers Weekly'', vol. 252, issue 45, November 11, 2005.[http://www.publishersweekly.com/pw/print/20051114/20772-a-mania-for-books.html]
*William F.Meehan III,"First Impression: An Interview with Author and Bibliophile Nicholas A. Basbanes", ''Indiana Libraries'', volume 25, number 3, 2006.[https://journals.iupui.edu/index.php/IndianaLibraries/article/viewFile/16360/pdf_140]
*Michael M. Jones,"Reamed Out: PW Talks with Nicholas A. Basbanes," ''Publishers Weekly'',  August 23, 2013.[http://www.publishersweekly.com/pw/by-topic/authors/interviews/article/58827-reamed-out-pw-talks-with-nicholas-a-basbanes.html]
*Bob Minzesheimer,"Five Great Books about Libraries", ''USA Today'', May 8, 2013.[http://www.usatoday.com/story/life/books/2013/05/08/favorite-books-about-libraries/2143247/]
*"On Paper: The Everything of Its Two-Thousand -Year History,"''Publishers Weekly'', August 5, 2013.[http://www.publishersweekly.com/9780307266422] 
*"On Paper: The Everything of Its Two-Thousand-Year History,"''Kirkus Reviews'',Volume LXXXI, No 19, September 1, 2013, Posted Online August 18, 2013.[https://www.kirkusreviews.com/book-reviews/nicholas-a-basbanes/on-paper/]
*Helen Gallagher, "On Paper: The Everything of Its Two-Thousand-Year History," ''New York Journal of Books'', October 15, 2013.[http://www.nyjournalofbooks.com/review/paper-everything-its-two-thousand-year-history]
*Nicholas A. Basbanes,"10 Most Bizarre Uses of Paper in History," ''Huffington Post'', HuffPost Books, The Blog, October 21, 2013.[http://www.huffingtonpost.com/nicholas-a-basbanes/post_5940_b_4136654.html]
*Ron Charles, “Nicholas Basbanes on the Enduring Importance of Paper,” ''The Washington Post'', The Style Blog, October 28, 2013. [http://www.washingtonpost.com/blogs/style-blog/wp/2013/10/28/nicholas-basbanes-on-the-enduring-importance-of-paper/]
*"On Paper," ''The New Yorker'', Briefly Noted, p.&nbsp;83, November 11, 2013.[http://www.newyorker.com/arts/reviews/brieflynoted/2013/11/11/131111crbn_brieflynoted]
*Peter Lewis, "On Paper," ''Barnes and Noble Review'', Reviews & Essays, Cultural History, November 15, 2013. [http://bnreview.barnesandnoble.com/t5/Reviews-Essays/On-Paper/ba-p/11747] 
*Philip Marchand, "Open Book: On Paper, by Nicholas A. Basbanes," ''National Post'', November 22, 2013. [http://arts.nationalpost.com/2013/11/22/open-book-on-paper-by-nicholas-a-basbanes/]
*David Walton, "Book Review: ‘On Paper: The Everything of Its Two-Thousand-Year History,’ by Nicholas Basbanes," ''The Dallas Morning News'', GuideLIVE Books, November 30, 2013. [http://www.dallasnews.com/entertainment/books/20131130-book-review-on-paper-the-everything-of-its-two-thousand-year-history-by-nicholas-basbanes.ece]
* Christine Rosen, "Sheet by Sheet 'On Paper: The Everything of Its Two-Thousand-Year History,'" ''The Wilson Quarterly'', Book Reviews, Autumn 2013. [http://www.wilsonquarterly.com/book-reviews/sheet-sheet]
*William F. Meehan III, Book Review, "On Paper: The Everything of Its Two-Thousand-Year History," ''Information & Culture: A Journal of History'', University of Texas Press, University of Texas at Austin, January 21, 2014. [http://www.infoculturejournal.org/book_reviews/basbanes_meehan_2014]
*Donna Seaman, ''Booklist Online'', Booklist Review, “On Paper: The Everything of Its Two-Thousand-Year History,” February 5, 2014. [http://www.booklistonline.com/On-Paper-The-Everything-of-Its-Two-Thousand-Year-History-Nicholas-A-Basbanes/pid=6648248]
*Glenn C. Altschuler, ''Tulsa World'', "Book Review: History of paper a ripping read," February 9, 2014. [http://www.tulsaworld.com/scene/books/book-review-history-of-paper-a-ripping-read/article_b5b9176c-66fd-5c05-b00a-f74eb10818fe.html]
*Carlin Romano, ''The Chronicle of Higher Education'', The Chronicle Review, “Whither Paper? Enough with simplistic predictions of its demise,” February 10, 2014. [http://chronicle.com/article/Whither-Paper-/144535/?key=S2lzJlJuZSdKMXlkYj4QMTwDa3FrOE90NXpGbC4hbltXEA==]
*Lois Carr, ''The Wichita Eagle'', "Nicholas Basbanes Celebrates the Ingenuity Behind One of Our Most Common Products," March 2, 2014. [http://www.kansas.com/2014/03/02/3319728/nicholas-basbanes-celebrates-the.html]
*''Booklist Online'', Notable Books 2014, March 15, 2014. [http://booklistonline.com/ProductInfo.aspx?pid=6657261&AspxAutoDetectCookieSupport=1]
*Leah Price, ''The Times Literary Supplement'', "Hold or Fold," March 19, 2014.[http://www.the-tls.co.uk/tls/public/article1389124.ece] 
*Lewis Fried,''The Key Reporter'', [[Phi Beta Kappa]]'s Publication for News and Alumni Relations, "Life of the Mind: 'On Paper'," April 1, 2014. [http://www.keyreporter.org/BookReviews/LifeOfTheMind/Details/944.html] 
*James McGrath Morris, ''Santa Fe New Mexican,'' Pasatiempo, The Sante Fe New Mexican’s Weekly Magazine of Arts, Entertainment & Culture, "Book Review: 'On Paper: The Everything of Its Two-Thousand-Year History' by Nicholas A. Basbanes," April 11, 2014. [http://www.santafenewmexican.com/pasatiempo/books/book_reviews/book-review-on-paper-the-everything-of-its-two-thousand/article_01f9471d-3bc2-5259-9609-fc68725caa7f.html]
*''Imprints and Impressions: Milestones in Human Progress, ''Exhibition of the Rarest Books in the World at the University of Dayton, October 2, 2014.[https://www.youtube.com/watch?v=ledVUlTzstk&feature=youtu.be]
*Stephanie McFeeters,''The Boston Globe'', Names, "Local Writers Will Share National Endowment for the Humanities Grant," July 29, 2015.[https://www.bostonglobe.com/lifestyle/names/2015/07/28/local-writers-will-share-national-endowment-for-humanities-grant/tWvEyQUmaV8OYfCnDQEoZN/story.html] 
*Michael Schaub, ''Los Angeles Times'', Jacket Copy, "Academic Nonfiction for the Masses? NEH Awards $1.7 million in Public Scholar grants," July 29, 2015.[http://www.latimes.com/books/jacketcopy/la-et-jc-neh-academic-nonfiction-20150728-story.html]
*Michael S. Rosenwald, ''Washington Post'', Local, "Take note: The Paper Industry is Planning a Big Comeback," July 29, 2015.[http://www.washingtonpost.com/local/paper-or-pixels-after-years-of-decline-the-paper-industry-tries-to-win-back-consumers-hearts/2015/07/29/c942d6f0-3213-11e5-97ae-30a30cca95d7_story.html]  
*Texas A&M University, University Libraries, News and Events, "Basbanes Collection Added to Cushing Library," December 7, 2015. [http://library.tamu.edu/about/news-and-events/2015/12/Basbanes-Collection.html]
*"Nicholas A. Basbanes,"Quotes," goodreads.com.[https://www.goodreads.com/author/quotes/7739.Nicholas_A_Basbanes?auto_login_attempted=true]
*[https://books.google.com/books?id=ZntfgJwFYdsC&printsec=frontcover&dq=basbanes+quotable+book+lover&hl=en&sa=X&ei=L1dYUfiDCJG14AOSp4DICQ&ved=0CDoQ6AEwAA Foreword to ''The Quotable Book Lover''], edited by Ben Jacobs and Helena Hjalmarsson (New York: The Lyons Press, 1999). The quotations in Chapter 10, “Collecting Books: A Special Section by Nicholas A. Basbanes, ” pp.&nbsp;209–228, were compiled from ''A Gentle Madness''.
*[https://books.google.com/books?id=30zZgRMSwLsC&pg=PT7&dq=basbanes+library+illustrated+foreword&hl=en&sa=X&ei=K1tYUeaHB4XD4AOE94CgCA&ved=0CDEQ6AEwAA Foreword to ''The Library: An Illustrated History''], by Stuart A. P. Murray (New York: Skyhorse Publishing Co., and Chicago: American Library Association, 2009)
*[https://books.google.com/books?id=qoTht-trmWQC&pg=PT6&dq=basbanes+modern+book+collecting+introduction+wilson&hl=en&sa=X&ei=AV9YUc_JA8np0gGGyoGgCg&ved=0CD8Q6AEwAA Introduction to Robert A. Wilson, ''Modern Book Collecting''], a new edition (New York: Skyhorse Publishing Co., 2010)
*Nicholas A. Basbanes,"The ''Evening Star'' and the Bobby Baker Story: A Case Study,"  Thesis (M.A.),Pennsylvania State University.[http://www.worldcat.org/title/evening-star-and-the-bobby-baker-story-a-case-study/oclc/82178715]
*Nicholas A. Basbanes,"Heritage 70 [cruisebook] USS Oriskany CVA 34"[http://www.worldcat.org/title/heritage-70-cruisebook-uss-oriskany-cva-34/oclc/000160983] 
*Nicholas A. Basbanes, ''Fine Books and Collections,''[http://www.finebooksmagazine.com/search.phtml?q=Nicholas+Basbanes&start=0&rows=10]

<!-- - Categories - -->

{{Authority control}}

{{DEFAULTSORT:Basbanes, Nicholas A.}}
<!-- [[Category:Articles created via the Article Wizard]]-->
[[Category:1943 births]]
[[Category:Living people]]
[[Category:American non-fiction writers]]
[[Category:Writers from Worcester, Massachusetts]]
[[Category:Writers from Lowell, Massachusetts]]
[[Category:Bates College alumni]]
[[Category:Pennsylvania State University alumni]]
[[Category:American people of Greek descent]]
[[Category:American male journalists]]
[[Category:Lecturers]]
[[Category:United States Navy officers]]
[[Category:American military personnel of the Vietnam War]]
[[Category:Book collecting]]