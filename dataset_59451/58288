{{Infobox journal
| title = Journal of Media Economics
| cover =
| editor = Nodir Adilov, Hugh Martin
| discipline = [[Media economics]]
| former_names =
| abbreviation = J. Media Econ.
| publisher = [[Routledge]]
| country =
| frequency = Quarterly
| history = 1988-present
| openaccess =
| license =
| impact = 0.240
| impact-year = 2012
| website = http://www.tandfonline.com/action/journalInformation?journalCode=hmec20
| link1 = http://www.tandfonline.com/toc/hmec20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/hmec20
| link2-name = Online archive
| JSTOR =
| OCLC = 18128413
| LCCN = sf96090646
| CODEN =
| ISSN = 0899-7764
| eISSN = 1532-7736
}}
The '''''Journal of Media Economics''''' is a [[peer-reviewed]] [[academic journal]] covering all aspects of [[media economics]] published by [[Routledge]]. Since September 2011 its [[editors-in-chief]] have been Nodir Adilov ([[Indiana University – Purdue University Indianapolis]]) and Hugh Martin ([[Ohio University]]). The journal was established in 1988 with [[Robert G. Picard]] as founding editor. Alan B. Albarran became its second editor. He was succeeded by Stephen Lacy, [[Steven S. Wildman]] ([[Michigan State University]]), Ben Compaine ([[Fordham University]]), and Brendan Cunningham ([[U.S. Naval Academy]]).{{cn|date=March 2014}} According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.240.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of Media Economics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

The biannual ''Journal of Media Economics'' Award of Honor recognizes scholars whose work has played important roles in developing the field of media economics studies and its literature.

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.tandfonline.com/action/journalInformation?journalCode=hmec20}}


[[Category:Media studies journals]]
[[Category:Publications established in 1988]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Works about information economics]]
[[Category:Economics journals]]


{{econ-journal-stub}}