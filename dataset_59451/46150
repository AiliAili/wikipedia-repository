{{Infobox venue
|name              = Amazingrace Coffeehouse
|nickname          = Grace
|image             = 
|image_caption     = 
|address           = Scott Hall, 601 University Place (1971-72)<br>Shanley Hall, Colfax Street (1972-74)<br>845 Chicago Avenue (1974-78) 
|location          = [[Evanston, Illinois]]
|coordinates       = 
|type              = Performance and recording venue; counterculture gathering place
|genre             = Folk, jazz, country, bluegrass, newgrass, roots, blues, rock, theater, poetry
|opened            = {{Start date|1970}}
|closed            = {{end date|1980}}
|owner             = Amazingrace Family
|seating_type      = Floor and chairs
|seating_capacity  = 100-400
|website           = {{URL|http://amazingrace.com}}
}}

'''Amazingrace Coffeehouse''' (later known as '''Amazingrace''') was a notable and influential counterculture music and performance venue in [[Evanston, Illinois]], during the 1970s. Run by a collective called the Amazingrace Family, it was best known for its welcoming atmosphere, eclectic menu, excellent sound system, and respectful audiences. Amazingrace was the top music club in the [[Chicago Reader]] poll 1973-1975, plus Number 3 in the 1975 wrap-up of "Who's Who in Chicago's Alternative Culture".<ref>Chicago Reader, September 26, 1975</ref> Performers from a wide variety of genres (including blues, bluegrass, folk, funk, rock, jazz, comedy, spoken word, and theater) played at Amazingrace from its beginning on the campus of [[Northwestern University]] until its final incarnation at The Main on Chicago Avenue in Evanston.

==History==

===Beginnings at Scott Hall===
Amazingrace had its beginnings in the [[Moratorium to End the War in Vietnam]] movement, and was closely associated with the cultural and political ferment of the 1960s and 1970s. It started as a spontaneous food service to student protesters on the campus of Northwestern University. The idea for a permanent student restaurant originated when university officials closed a cafeteria that served the independents on campus.<ref>Sluis, William. "Restaurant Thrives on Health Food", The Chicago Tribune, March 5, 1972. p. 22</ref> Students formed the Scott Hall Grill Committee, which then successfully requested permission from the Associated Student Government to run a food service and entertainment space in the basement of 601 University Place.<ref>{{cite web|url=http://digital.library.northwestern.edu/architecture/building.php?bid=21|title=Building View, Northwestern Architecture, Northwestern University Library|work=northwestern.edu|accessdate=29 July 2015}}</ref> At the same time these students formed a communal living situation in an empty University housing apartment on Sherman Avenue.

The coffeehouse in the basement was a hit, often selling out of lunch and packing the room for evening music. Student volunteers made daily runs to Chicago’s [[South Water Market]] for fresh produce. Patrons sat on the floor at cable spool tables to eat granola, chicken soup and soy loaf. Anyone with a good recipe that could serve 400 cheap was invited to take a turn at cooking.<ref>Rosen, Joel "Amazingrace in Evanston", The Chicago Reader, November 19, 1971, p. 7</ref>

Performers such as [[Bill Quateman]] and [[Fred Anderson (musician)|Fred Anderson]] came in to play, and patrons passed the hat to pay them. The as-yet unnamed venue also presented outdoor concerts, sponsored art shows, and hosted an alternative [[Free School]], whose offerings included "[[Street Medicine]]", "[[Alternative Structures]]: From Plastic Bag to [[Geodesic Dome]]", "[[Meher Baba]]", and "Computer Programming for Freaks".

The venue received its name when folk duo Norman Schwartz and Carla Reiter started ending their set with an ''a cappella'' rendition of the well-known song [[Amazing Grace]]. The strong audience response led Schwartz and other performers to close subsequent sets the same way, and soon the whole audience was singing what had now become the club's nightly finale.<ref>Tesser, Neil. "Amazingrace". Northwestern Magazine, Fall 2011.</ref>  The song christened both the coffeehouse and the collective.

===Shanley Hall===
In the Fall of 1972 the University re-appropriated the Scott Hall basement for office space. Amazingrace moved to the larger Shanley Hall,<ref> "Shanley Hall," Northwestern Magazine, Fall 2012</ref> which doubled the coffeehouse’s seating capacity to 200. Shanley Hall, a Quonset hut, was built after [[World War II|WWII]] to make classrooms for increased students due to the [[G.I. Bill]]. Amazingrace began to book touring national acts such as [[Phil Ochs]], [[David Bromberg]], [[Mimi Fariña]], [[Jamie Brockett]], [[John Hartford]], [[Norman Blake (American musician)|Norman Blake]], [[Ry Cooder]], and [[Vassar Clements]]. These touring acts were then complemented by local opening acts and headliners such as [[Corky Siegel]], [[Luther Allison]], [[Claudia Schmidt]], [[Tom Dundee]], [[Bonnie Koloc]] and [[Redwood Landing]].

In addition to Shanley Hall, Amazingrace also had access to larger campus venues such as the 1200+-seat [[Cahn Auditorium]] and the 8000-seat McGaw Hall (now [[Welsh-Ryan Arena]]). Here it presented shows by [[Taj Mahal]], [[Leo Kottke]], [[John Fahey (musician)|John Fahey]], [[John Prine]], [[Commander Cody & His Lost Planet Airmen]], [[John McLaughlin (musician)|John McLaughlin]] and [[The Mahavishnu Orchestra]], [[Steve Goodman]], and [[The Grateful Dead]].

The Grateful Dead concert at McGaw is well remembered for two reasons. First, the band took an unusually long set break of close to an hour. The second notable element was the startling visual of numerous silk parachutes strung across the vast ceiling. The silks were part of a plan that the band and Amazingrace created to "improve" the acoustics and visuals of the hall, which is built like an airplane hangar. While the famous Dead [[Wall of Sound]] did not formally debut until March 1974 at the [[Cow Palace]] in San Francisco, many of the WOS elements were already being used at this concert, including McIntosh 2300 and 3500 amplifiers, noise-canceling vocal mics, and plentiful [[JBL]] drivers. Despite complaints about the delays, the consensus of reviewers was that the sound and set list were amazing.<ref>Grateful Dead Live at McGaw Memorial Hall on 1 November 1973, reviews. From the Deadlists Project at archive.org.</ref>

Despite these larger productions, Amazingrace maintained its coffeehouse tradition of broad cultural programming and community participation. It developed its own ticketing system, designed and printed its own posters, and cooked lunch and dinner for its patrons and performers. Amazingrace hosted poetry readings, anti-war rallies, films, and photography shows. It also continued to invite guest cooks into the kitchen. The kitchen was also the informal [[green room]] for the musicians, with the exception of the preeminent American folk musician [[Odetta]]. For Odetta, the women’s bathroom was commandeered as a true dressing room, and the men’s toilet became unisex for the duration of each of her engagements.

"The ambience was very special there," recalled [[Bob Gibson (musician)|Bob Gibson]], who recorded live at Amazingrace. "We knew it would be really high energy performing."<ref>Bob Gibson and Carol Bender. “I Come For To Sing,” Pelican Publishing, Louisiana, 2001, p. 114</ref>

Success brought Amazingrace increased visibility, and with that increased visibility came two significant problems. The first problem was Amazingrace’s status as a university organization. By 1974 most member of the Amazingrace collective were no longer students. They had either graduated or dropped out due to the demands of running the club. The University was uncomfortable with their continuing use of its grounds and facilities. In addition, Amazingrace's loose policies regarding "bring your own" beer and marijuana led University President Robert Strotz to worry about "a situation that would lead to the place being busted".<ref>Pridmore, Jay. “Northwestern University: Celebrating 150 Years.” Northwestern University Press, 2000, p. 223</ref> Then, on 6 November 1973, the City of Evanston added additional pressure when it warned the University that by allowing Amazingrace to carry on, Northwestern was in violation of a zoning regulation that prohibited commercial business operations on University-owned land.<ref>Tesser, Neil. "The Corporate Hippies Fight Back", The Chicago Reader, December 7, 1973, p. 7</ref>

Amazingrace's second big problem during this time was also with the City, but this issue involved the group's new communal living situation at Colfax Street in Evanston. A dozen people resided at Colfax Street, in addition to the frequent touring musicians, poets, and [[yippies]] who could always find a meal and a bed in the basement. This style of occupancy put the house in violation of the Evanston [[exclusionary zoning]] ordinance that disallowed more than three unrelated people living together. In response, Amazingrace partnered with the Evanston chapter of the [[American Civil Liberties Union]] (ACLU) to challenge the ordinance. However, both parties agreed to drop the effort in April 1974, when the [[Supreme Court of the United States]] upheld the constitutionality of this type of ordinance in the similar case of [[Village of Belle Terre v. Boraas]].

===The Main and Amazingrace West===
In late 1974, after years of disagreement and conflict with both Northwestern and the City of Evanston, Amazingrace severed ties with Northwestern, moved out of Colfax Street, and also fragmented as a group. Several members went off to new ventures outside of the music business. Five members left Evanston and headed west to [[Eugene, Oregon]], where they lived and worked for the next two years as Amazingrace "West," producing and promoting shows by [[Mimi Fariña]], [[Bryan Bowers]], [[John Prine]], [[Sam Leopold]], and Turkey Run. They also worked with Eugene's [[W.O.W. Hall]], supplying sound equipment and bookings assistance.

Six members stayed in Evanston. They took over a small 4-flat apartment building on Crain Street in Evanston, thereby staying within the city’s occupancy regulations. They partnered with Evanston architect [[Edward Noonan (architect)|Ed Noonan]] to create the next incarnation of Amazingrace at The Main, Noonan’s new mixed-use development at 845 Chicago Avenue in south Evanston.<ref>{{cite web|url=http://findingaids.library.northwestern.edu/catalog/inu-ead-nua-archon-707|title=Records of Amazingrace Coffeehouse, 1971-2011|work=Northwestern University|accessdate=29 July 2015}}</ref>

Amazingrace was now located much closer to Chicago, and was right across the street from a [[Chicago Transit Authority]] "El" train station. The new venue was 3297 square feet. It had neither kitchen facilities for food service nor a liquor license, but its good location, reasonable prices ($2.50-$3.50 admission for one of the two nightly sets), "non-nightclub" atmosphere, and "all-ages welcome" policy grew the audience. With 16-foot high acoustical ceilings and a wrap-around balcony, Amazingrace at The Main could accommodate almost 400 patrons, most of whom sat on the carpeted floor. The sound system featured [[Electro-Voice]] Sentry III studio monitor speakers, [[Dynaco]] Stereo 400 amplifiers, an [[Allen & Heath]] soundboard, and Amazingrace’s own house-brand Earworks 24-band graphic equalizer.<ref>Billboard Magazine. “Northwestern U Club Will Have Disking Studios,”  21 December 1974, p. 4</ref> A [[Colortran]] lightboard and theatrical-quality lighting instruments complemented the sound system and heightened the performance experience. Reviewers noted that performers got "a warm reception...enhanced by the open, relaxed atmosphere of the two-tiered club and the clean, open amplification."<ref> Billboard,  8 August 1976, p. 40</ref>

Live and delayed concert broadcasts on radio stations continued to be a staple, as they had at Shanley Hall. [[The Midnight Special (radio)|The Midnight Special]], a popular evening program on Chicago's fine arts station [[WFMT-FM]], frequently featured Amazingrace shows, as did rock station [[WXRT-FM]] and university station [[WNUR-FM]].

During this period the booking roster expanded to include more jazz. Established masters [[Sonny Rollins]], [[Charles Mingus]], and [[McCoy Tyner]] played repeat engagements. Amazingrace also became the go-to venue for the next generation of upcoming jazz artists, including pianist [[Keith Jarrett]], the [[Paul Winter Consort]], [[Oregon]], vibraphonist [[Gary Burton]] and guitarist [[Pat Metheny]].

In addition to jazz, the total range of presentations also grew. Amazingrace hosted national acts from the folk, country, blues, funk, soul, rock, swing, and bluegrass traditions. [[Randy Newman]], [[The Persuasions]], [[Jimmy Buffett]], [[New Grass Revival]], [[Tom Rush]], [[Emmy Lou Harris]], [[Jerry Jeff Walker]], [[Fairport Convention]] with [[Sandy Denny]], [[Doc Watson]] and [[Merle Watson]], [[Mary Travers]], [[John Hiatt]], and [[Sun Ra]] played there. The audiences came to trust the venue, and it was not unusual for a patron to show up, buy a ticket, and only then ask who was playing.

Relationships with performers were personal. For example, when Steve Goodman needed money to purchase a house, he called Amazingrace and asked if they could quickly book him in. The four resulting sold-out shows became known as the "Let's Buy Steve a House" gig. Shortly afterwards, when the washing machine at Crain Street broke down, Goodman had his road manager Steve Cohen buy the collective a new one. "That," says Cohen, "was the Steve Goodman Memorial Washing Machine."<ref>Eals, Clay. "Steve Goodman: Facing the Music." ECW Press, Ontario, Canada, 2006, p. 442</ref>

Continuing the coffeehouse tradition established at Scott and Shanley Halls, Amazingrace at The Main also presented film, lecture, theater, comedy and poetry. [[Steve Martin]] played his first Chicago-area comedy shows at Amazingrace. [[Henny Youngman]] and Proctor & Bergman of [[Firesign Theater]] played to sold-out shows. [[Charles Bukowski]], [[Anne Waldman]], and [[United States Poet Laureate|US Poet Laureate]] [[Mark Strand]] appeared as part of the [[No Mountains Poetry Project]] readings and broadsides series. [[Allen Ginsberg]] read in a benefit performance for [[Rangjung Rigpe Dorje]], the 16th [[Gyalwa Karmapa]].

In 1976 the [[Piven Theatre Workshop]] chose Amazingrace as the rehearsal and performance space for its unique combination of story-telling and [[improvisational theater]].<ref>Joyce Piven and Susan Applebaum. “In the Studio with Joyce Piven.” Bloomsbury Publishing, 2012, p. xviii</ref> This allowed Amazingrace to host the first performances of those who would become the next great generation of Chicago actors. Performers at that time included [[Jeremy Piven]], [[John Cusack]], [[Aidan Quinn]], [[Lili Taylor]], and [[Joan Cusack]].

Once again, however, Amazingrace's popularity, artistic success, and commitment to low prices for the community came at a cost. Finances were a challenge. The collective members needed day jobs to help cover their own living expenses. Lack of liquor and food sale income made it hard to have sufficient cashflow to pay rent, electricity, advertising, and performers. Additional pressure came from the ever-increasing common maintenance charges and heating bills that [[Heil, Heil, Smart & Golee]] (The Main’s management firm)  imposed. In 1978 Amazingrace was late on a rent payment, but was able to negotiate a settlement with Heil, Heil, Smart & Golee. Despite meeting the terms of the settlement, the management firm subsequently ordered Amazingrace to leave The Main. Citing high costs and unresponsive management, several other stores left during the same time.,<ref>Marshall, Steve. “The Main: Owners seeking new retailers,” Crain’s Chicago Business, 24 July 1978, p. 12</ref><ref>Beard, Dave. "Amazingrace to quit The Main: management problems cited."The Daily Northwestern, May 10, 1978, p. 1</ref>

Amazingrace at The Main put on its last show on July 31, 1978, in a raucous 4-day weekend featuring Jim Post, Corky Siegel, [[Tom Dundee]], Steve Goodman, with fans, musicians, and former collective members attending from all over the country.<ref>Billboard Magazine. “Curtain Falls on Funky Chi Amazingrace.” August 12, 1978</ref><ref>Wald, Elliot."Goodnight Gracie: Amazingrace closes its doors."Chicago Sun-Times, August 2, 1978, p. 79</ref> The final song, sung from the stage with audience participation, was “Amazing Grace.”<ref>Guide to the Records of Amazingrace Coffeehouse, Northwestern University Library</ref>

The Evanston members continued on as Amazingrace and produced several shows in other local venues such as the Varsity Theater and [[Pick-Staiger]] Concert Hall through the early 1980s.

==Legacy==
Although the last venue owned by Amazingrace closed in 1978, its legacy and influence survive. [[FitzGerald’s Nightclub]],<ref>Wilkinson, Cathryn, "A River Runs Through It," interview with Bill FitzGerald in OakPark.com, 26 June 2007</ref> The Morse Theater Project,<ref>Reich, Howard. "Bringing Jazz to Rogers Park," Chicago Tribune, 24 February 2008</ref>and [[Evanston S.P.A.C.E.]]<ref>interview with SPACE director Stuart Rosenberg in the Chicago Tribune, 7 March 2010</ref> all cite Amazingrace as their model and inspiration.

Amazingrace Reunions are held semi-regularly for family, friends, performers, and fans. The first was in 1988 at Shanley Hall in Evanston. The 2004 reunion was at [[Centre East]] in [[Skokie, Illinois]]. In 2011 there was a full-week, multi-site 40th anniversary celebration at [[Northwestern University Library]],<ref>{{cite web|url=http://www.northwestern.edu/newscenter/stories/2011/10/amazing-grace-summary.html|title=Still 'Amazing' After All These Years: Northwestern University News|work=Northwestern University|accessdate=29 July 2015}}</ref> the [[Old Town School of Folk Music]] in Chicago, and [[Evanston S.P.A.C.E.]]

==References==
{{Reflist}}

==Further reading==
*[http://www.library.northwestern.edu/news-events/exhibits/past-exhibits-chronological-index/past-exhibits-2011 Amazingrace Collective: A Countercultural Legacy]. This exhibit of Amazingrace objects, books, clippings, memorabilia and poster collection was curated by Allen Streicker and hosted by the Northwestern University Library. It featured many rare and previously unseen photographs documenting the backstage, performance, and collective family life and times. 
*Billboard Magazine. “Curtain Falls on Funky Chi Amazingrace.” August 12, 1978
*Eals, Clay. “Steve Goodman: Facing the Music.” ECW Press, Ontario, Canada, 2006
*[http://findingaids.library.northwestern.edu/catalog/inu-ead-nua-archon-707 Guide to the Records of Amazingrace Coffeehouse], Northwestern University Library
*[http://digital.library.northwestern.edu/architecture/building.php?bid=21 History of Scott Hall]
*Piven, Joyce and Applebaum, Susan. “In the Studio with Joyce Piven.” Bloomsbury Publishing, London, 2012 
*Pridmore, Jay. “Northwestern University: Celebrating 150 Years.” Northwestern University Press, 2000.
*Sullivan, James D.  “On the Walls and in the Streets: American Poetry Broadsides from the 1960s.” University of Illinois Press
*Tesser, Neil. “Amazingrace,”  [http://www.northwestern.edu/magazine/fall2011/feature/amazingrace.html Northwestern Magazine, Fall 2011]

==External links==
* [http://www.amazingrace.com Amazingrace]
* [https://docs.google.com/spreadsheets/d/1jdo4ecZXxPpf3JSIGaBdwVudT_UtMUFnpxIS-zeJHOw/pub?output=html Who Played at Amazingrace?]
* [https://www.facebook.com/groups/46034139569/ The Facebook fanpage Remembering Amazingrace Coffeehouse]  
* [http://www.concertvault.com/search.html?t=amazingrace&tb=0&sort=performer Tapes of over 400 Amazingrace shows are available at the Concert Vault]

{{coords|42|03|00|N|87|40|45|W|display=title}}

[[Category:Articles created via the Article Wizard]]
[[Category:Coffeehouses]]
[[Category:Demolished music venues in the United States]]
[[Category:Former music venues in the United States]]
[[Category:1970 establishments in Illinois]]
[[Category:1980 disestablishments in Illinois]]