{{Other uses|1919 (disambiguation)}}
{{good article}}
{{Infobox television episode
| title = 19:19
| image =
| caption = 
| series = [[Millennium (TV series)|Millennium]]
| season = 2
| episode = 7
| airdate =November 7, 1997
| production = 5C06
| writer = [[Glen Morgan]]<br/>[[James Wong (producer)|James Wong]]
| director = [[Thomas J. Wright]]
| guests =
*[[Kristen Cloke]] as Lara Means
*[[Christian Hoff]] as Matthew Prine
*Steven Rankin as Sheriff John Cayce
*[[Colleen Rennison]] as Jessica Cayce
| prev = [[The Curse of Frank Black]]
| next = [[The Hand of St. Sebastian]]
| episode_list = [[Millennium (season 2)|List of season 2 episodes]]<br>[[List of Millennium episodes|List of ''Millennium'' episodes]]
}}

"''''19:19'''" is the seventh episode of the [[Millennium (season 2)|second season]] of the [[Television in the United States|American]] [[Crime (genre)|crime]]-[[Thriller (genre)|thriller]] television series ''[[Millennium (TV series)|Millennium]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] on November 7, 1997. The episode was written by [[Glen Morgan]] and [[James Wong (producer)|James Wong]], and directed by [[Thomas J. Wright]]. "19:19" featured guest appearances by [[Kristen Cloke]] and [[Christian Hoff]].

[[Millennium Group]] offender profiler [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) investigates the abduction of a bus full of schoolchildren, requiring the help of fellow Group members Peter Watts ([[Terry O'Quinn]]) and Lara Means (Cloke) as he tracks a man preparing for a third world war.

"19:19" featured several minor guest stars who would later return to the series, as well as the second appearance by recurring actor Cloke. The episode was viewed by approximately 5.98 million households in its initial broadcast, and received a mixed response from television critics.

==Plot==

In [[Broken Bow, Oklahoma]], Matthew Prine ([[Christian Hoff]]) intently watches several televisions simultaneously, scrawling his reactions across every inch of his floor. As he finishes writing, he experiences a vision of the future—nuclear war and its barren aftermath.

Later, [[Millennium Group]] offender profilers [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) and Peter Watts ([[Terry O'Quinn]]) investigate the disappearance of a bus full of schoolchildren. They believe the driver was also a victim, and not responsible; they meet with the local sheriff, John Cayce (Steven Rankin), who has dredged the bus from a lake. It is empty, but inside, Black experiences the same vision as Prine. He also finds paint transfer on the exterior, indicating the perpetrator was driving a white van. A false positive results as police apprehend [[Storm chasing|storm chasers]] in a different van, who warn that a violent tornado is approaching.

Prine is behind the kidnapping, and forces the children and driver into an underground bunker. He and his accomplice count the hostages, realizing that there is one less child than they had anticipated. Black and Cayce realize this too, and race to the home of the child who had not taken the bus that morning. They arrive in time to apprehend Prine as he attempted to snatch the child, and take him into custody.

Black believes Prine is not driven by malice; he and Watts use the resources of the Millennium Group to find his home, discovering the dense writings across his floors. They learn that Prine believes a third world war is imminent, and took the children as he believes one of them is destined to bring peace during this time, wishing to protect this child when he learns which one it is destined to be. Black seeks aid from another Group member, Lara Means ([[Kristen Cloke]]), who is able to observe Prine's behavioural tells for clues when interviewing him. This, coupled with analysis of soil from his clothing, points to the children being held in an aluminium quarry. The investigators rush to the quarry, where Prine's accomplice engages them in a firefight. However, the advancing tornado forces them to take cover; it kills Prine and lifts the roof from the entombing bunker. It subsides as quickly as it arrived, and the children emerge safely from the wreckage; Black senses that Cayce's daughter may be the prophesied peacemaker. It turns out that the tornado destroyed the school where the children would have been if they weren't abducted, and that Prine had saved their lives.

==Production==

"19:19" was written by frequent collaborators [[Glen Morgan]] and [[James Wong (producer)|James Wong]]. The duo would pen a total of fifteen episodes throughout the series' run.<ref name="season1book">{{cite AV media notes |title=Millennium: The Complete First Season |titlelink=Millennium (season 1) |year=1996–97 |others=[[David Nutter]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref><ref name="season2book">{{cite AV media notes |title=Millennium: The Complete Second Season |titlelink=Millennium (season 2) |year=1997–98 |others=[[Thomas J. Wright]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> The pair had also taken the roles of co-executive producers for the season.<ref name="fall">{{cite web |url=http://www.highbeam.com/doc/1G1-56443413.html |title=Fall Watch; 'Millennium' takes new turn |publisher=''[[The Boston Herald]]'' |first=Harvey |last=Soloman |date=September 18, 1997 |accessdate=May 19, 2012}} {{subscription required}}</ref> "19:19" was directed by [[Thomas J. Wright]], who helmed a total of twenty-six episodes across all three seasons.<ref name="season1book"/><ref name="season2book"/><ref name="season3book">{{cite AV media notes |title=Millennium: The Complete Third Season |titlelink=Millennium (season 3) |year=1998–1999 |others=[[Thomas J. Wright]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> Wright would also go on to direct "[[Millennium (The X-Files)|Millennium]]", the series' [[fictional crossover|crossover]] episode with its [[sister show]] ''[[The X-Files]]''.<ref name="millenniumxfiles">{{cite episode| episodelink=Millennium (The X-Files) | title=Millennium| series=[[The X-Files]] |credits =[[Thomas J. Wright]] (director); [[Vince Gilligan]] & [[Frank Spotnitz]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 7 | number = 4 |airdate= November 28, 1999}}</ref>

The episode features the second appearance of Kristen Cloke as Millennium Group member Lara Means. Cloke had first portrayed the character in "[[Monster (Millennium)|Monster]]",<ref>{{cite episode| title=Monster |episodelink=Monster (Millennium) | series=Millennium | serieslink=Millennium (TV series) |credits =[[Perry Lang]] (director); [[Glen Morgan]] & [[James Wong (producer)|James Wong]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 2 | number = 4 |airdate=October 17, 1997}}</ref> and would last appear in the second season finale "[[The Time Is Now (Millennium)|The Time Is Now]]".<ref>{{cite episode| episodelink=The Time Is Now (Millennium) | title=The Time Is Now | series=Millennium | serieslink=Millennium (TV series) |credits =[[Thomas J. Wright]] (director); [[Glen Morgan]] & [[James Wong (producer)|James Wong]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 2 | number = 23 |airdate= May 15, 1998}}</ref> Several of the episode's minor guest stars would appear again later in the series. Bill Marchant, who portrayed Prine's accomplice, reappeared in an unrelated role in the third season episode "[[Collateral Damage (Millennium)|Collateral Damage]]",<ref>{{cite episode |title=Collateral Damage |episodelink=Collateral Damage (Millennium) |series=Millennium |serieslink=Millennium (TV series) |number=11 |season=3 |credits=[[Michael R. Perry]] (writer); [[Thomas J. Wright]] (director) |airdate=January 22, 1999 | network = [[Fox Broadcasting Company|Fox]]}}</ref> while Kurt Evans, who played a sheriff's deputy, resurfaced in "[[Darwin's Eye]]", also in the third season.<ref>{{cite episode |title=Darwin's Eye |episodelink=Darwin's Eye |series=Millennium |serieslink=Millennium (TV series) |number=17 |season=3 |credits=[[Patrick Harbinson]] (writer); [[Kenneth Fink|Ken Fink]] (director) |airdate=April 16, 1999 | network = [[Fox Broadcasting Company|Fox]]}}</ref>

==Broadcast and reception==

{{quote box
|width=30em
|bgcolor=#c6dbf7
|quote="I ... wish that one of these episodes would eventually make every idea in its head come together, instead of seeming like a random collection of interesting facts and cool knowledge, like one of those old ''Reader’s Digest'' "fun facts" compendiums, assembled into TV episode form".
|source=–''The A.V. Club''{{'s}} Todd VanDerWerff<ref name="avc"/>}}

"19:19" was first broadcast on the [[Fox Broadcasting Company|Fox network]] on November 7, 1997.{{sfn|Shearman|Pearson|2009|p=149}} The episode earned a [[Nielsen rating]] of 6.1 during its original broadcast, meaning that {{nowrap|6.1 percent}} of households in the United States viewed the episode. This represented approximately {{nowrap|5.98 million}} households, and left the episode the eighty-fourth most-viewed broadcast that week.<ref name="ratings">{{cite web |url=http://www.highbeam.com/doc/1G1-67750766.html |title='Angel' Helps CBS Tie in Ratings Race |work=[[Rocky Mountain News]] |date=November 13, 1997 |first=Dave |last=Bauder |accessdate=October 8, 2012}} {{Subscription required}}</ref>{{#tag:ref|Each ratings point represented 980,000 households during the 1997–98 television season.<ref name="ratings"/>|group="nb"}}

''[[The A.V. Club]]''{{'s}} Todd VanDerWerff rated the episode a "B", finding its final act to be powerful and entertaining. However, he believed that the episode suffered from a poor opening act, and was critical of what he saw as a poor performance by guest star Steven Rankin.<ref name="avc">{{cite web |url=http://www.avclub.com/articles/emily1919,55169/ |title="Emily"/"19:19" {{!}} The X-Files/Millennium {{!}} TV Club |work=[[The A.V. Club]] |first=Todd |last=VanDerWerff |date=April 30, 2011 |accessdate=October 8, 2012}}</ref> Bill Gibron, writing for [[DVD Talk]], rated the episode 3 out of 5, feeling that the episode's [[police procedural]] elements did not mesh well with its theological themes. Gibron felt that the plot's resolution was overly contrived, but praised the chemistry between the lead actors, highlighting Henriksen and Cloke in particular.<ref name="dvdtalk">{{cite web |url=http://www.dvdtalk.com/reviews/13926/millennium-season-2/?___rd=1 |title=Millennium: Season 2: DVD Talk Review of the DVD Video |first=Bill |last=Gibron |publisher=[[DVD Talk]] |date=January 3, 2005 |accessdate=October 8, 2012}}</ref> [[Robert Shearman]] and [[Lars Pearson]], in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', rated "19:19" three-and-a-half stars out of five. Shearman felt the episode was a successful blend of the investigative plot structure used in the first season with the themes of religious eschatology introduced in the second season; however, he believed that the plot "ran out of steam" by the final act, sacrificing engaging drama for accurate but dry psychology.{{sfn|Shearman|Pearson|2009|p=156}}

==Notes==

<references group="nb"/>

==Footnotes==
{{reflist|colwidth=33em}}

===References===

*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=097594469X |ref=harv}}

==External links==
* {{IMDb episode|0648214}}
* {{Tv.com episode|25893}}

{{Millennium episodes|2}}

[[Category:Millennium (TV series) episodes]]
[[Category:1997 television episodes]]