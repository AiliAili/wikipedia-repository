{{Redirect|POCO||Poco (disambiguation)}}

In [[software engineering]], a '''plain old CLR object''' ('''POCO''') is a simple object created in the [[Common Language Runtime]] (CLR) of the [[.NET Framework]] which is unencumbered by inheritance or attributes. This is often used in opposition to the complex or specialised objects that [[object-relational mapping]] frameworks often require<ref>See, for example, this MSDN article: [http://msdn.microsoft.com/en-us/library/cc681329.aspx Data Contracts - POCO Support]</ref>. In essence, a POCO does not have any dependency on an external framework and generally does not have any attached behaviour.

==Etymology==
''Plain Old CLR Object'' is a play on the term ''[[plain old Java object]]'' from the [[Java Platform, Enterprise Edition|Java EE]] programming world, which was coined by Martin Fowler in 2000<ref>See anecdote here: http://www.martinfowler.com/bliki/POJO.html</ref>. POCO is often expanded to ''plain old [[C Sharp (programming language)|C#]] object'', though POCOs can be created with any language targeting the CLR. An alternative acronym sometimes used is ''plain old .NET object''.<ref>See, for example, a reference to PONO in this whitepaper: [http://www.springframework.net/doc-latest/reference/pdf/spring-net-reference.pdf Spring.net Reference Documentation]</ref>

==Benefits==
Some benefits of POCOs are:
* allows a simple storage mechanism for data, and simplifies serialisation and passing data through layers;
* goes hand-in-hand with [[dependency injection]] and the repository pattern;
* minimised complexity and dependencies on other layers (higher layers only care about the POCOs, POCOs don't care about anything) which facilitates loose coupling;
* increases testability through simplification.

==See also==
* [[Plain old data structure]]
* [[Plain old Java object]]
* [[Data transfer object]]

==References==
<references/>

{{.NET Framework}}

{{DEFAULTSORT:Plain Old Clr Object}}
[[Category:.NET Framework]]

{{Compu-lang-stub}}