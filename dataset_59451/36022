{{Use dmy dates|date=August 2015}}
{{good article}}
{{Infobox rugby biography
| name = Doolan Downing
| image = Sergeant Albert Downing.jpg
| image_size = 
| alt = 
| caption = 
| birth_name = Albert Joseph Downing
| birth_date = {{birth date|1886|07|12|df=y}}
| birth_place = [[Port Ahuriri]], [[Napier, New Zealand]]
| death_date = {{death date and age|1915|08|08|1886|07|12|df=y}}
| death_place = [[Chunuk Bair]], [[Gallipoli]], [[Ottoman Empire|Turkey]]
| height = {{convert|1.83|m|ftin}}
| weight = {{convert|89|kg|stlb}}
| school = [[Napier Boys' High School|Napier Boys' High]]
| university = 
| relatives = 
| occupation = 
| ru_position = [[Rugby union positions#Locks|Lock]]
| allblackid = 238
| allblackno = 176
| ru_amateuryears = 
| ru_amateurclubs = 
| ru_clubyears = {{ubl|1909–1911|1913–1914|1915}}
| ru_proclubs = {{ubl|{{nowrap|Napier Marist}}|{{nowrap|[[Marist Brothers Old Boys RFC|Auckland Marist]]}}|{{nowrap|Trentham Military Forces}}}}
| ru_clubcaps = 
| ru_clubpoints = 
| ru_provinceyears = {{ubl|1909–1910|1911–1912|1913–1914}}
| ru_province = {{ubl|[[Hawke's Bay Rugby Union|Hawke's Bay]]|North Island Country|[[Auckland rugby union team|Auckland]]}}
| ru_provincecaps = 
| ru_provincepoints = 
| ru_nationalyears = 1913–1914
| ru_nationalteam = {{nowrap|{{ru|New Zealand}}}}
| ru_nationalcaps = 26
| ru_nationalpoints = (21)
| module = 
----{{infobox military person|embed=yes
| allegiance         = {{UK}}
| branch             = {{Army|New Zealand}}
| serviceyears       = 1911–1915
| rank               = Sergeant
| unit               = {{nowrap|Fifth Reinforcements (Wellington Battalion)}}
| commands           = 
| battles            = [[Battle of Chunuk Bair]], [[Gallipoli Campaign]]
| placeofburial    =
| placeofburial_coordinates = 
| mawards            =  <!-- for military awards - appears as "Awards" if |awards= is not set -->
| memorials = 
}}
}}

'''Albert "Doolan" Joseph Downing''' (12 July 1886 – 8 August 1915) was a New Zealand international rugby union player, capped 26 times at [[Rugby union positions#Locks|lock]] between 1913 and 1914.  He was born in [[Napier, New Zealand|Napier]], and began his playing career for Napier Marist in 1909, from which he was selected for [[Hawke's Bay Rugby Union|Hawke's Bay]] and for the North Island. He moved at the end of 1912 to [[Auckland]] and there joined [[Marist Brothers Old Boys RFC|Auckland Marist]], where he was the club's first [[All Black]], playing his debut match against a touring Australian team in 1913. He was selected for the highly successful tour of North America in 1913, playing in 14 of the 16 matches and scoring 6 tries.

While the All Blacks were on a tour of Australia in 1914, the British Empire declared war on Germany and the team collectively decided to enlist. Three of them were killed, Downing the first of the All Blacks in World War I. After enlisting in early 1915, he took part in the [[Battle of Chunuk Bair]], part of the [[Gallipoli Campaign]], and was killed on 8 August. [[Henry Dewar (rugby)|Henry Dewar]], a team mate from the USA tour, died the following day at [[Anzac Cove]].

==Early life==
Albert Downing was born on 12 July 1886 in [[Port Ahuriri]], [[Napier, New Zealand]], the eldest son of Mr and Mrs John Downing.{{sfn|Sewell|1919|p=46}}{{sfn|McCrery|2014|p=136}}<ref name="napierobmaristrugby.co.nz">{{cite web|url=http://www.napierobmaristrugby.co.nz/news-events/index.htm?articleId=141|title=Napier Old Boys Marist :: News & Events|publisher=napierobmaristrugby.co.nz}}</ref> He attended [[Napier Boys' High School]] until 1904. He was a farmer before joining Barry Brothers, carriers and coal merchants, choosing to work as a carter outdoors, rather than join the clerical staff.<ref>{{cite news|title=Today's List|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=PBH19150911.2.20.2&srpos=7&e=--1839---1920--50--1----0Doolan+Downing--|accessdate=11 July 2015|work=Poverty Bay Herald|issue=13786|date=11 September 1915|page=4}}</ref> Later, he worked as a storeman for the Ellison and Duncan Company.<ref name="aucklandmarist.co.nz">{{cite web|url=http://www.aucklandmarist.co.nz/large-gallery-article/albert-joseph-doolan-downing-our-anzac-all-black/135289/346037/|title=Albert Joseph "Doolan" DOWNING – our ANZAC All Black|publisher=aucklandmarist.co.nz}}</ref>

==Rugby career==
Downing’s rugby career, playing at [[Rugby union positions#Locks|lock]], started with the Napier Old Boys Rugby Club and then Napier Marist Rugby Club;<ref name="napierobmaristrugby.co.nz"/> he represented [[Hawke's Bay Rugby Union|Hawke's Bay]] from 1909 to 1912. In 1911, he was selected for a North Island Country team, playing all games of a five match tour.<ref name="aucklandmarist.co.nz"/> The tour was part of a scheme by the New Zealand Rugby Union to discover talent, which brought Downing to the selectors' attention.{{sfn|Sewell|1919|p=47}}<ref name=Sun150911/> The North Island team played four games, against Auckland, Taranaki Union, Wanganui and Wellington, between 29 July and 9 August, and played a final match against the South Island on 12 August.<ref>{{cite news|title=Rugby|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=AS19110726.2.74.1&srpos=111|accessdate=14 July 2015|work=Auckland Star|date=26 July 1911|page=7}}</ref> Reports from the tour show Downing was very involved in the games, scoring a try against Wanganui for a 13–0 victory,<ref>{{cite news|title=Demonstration against a player|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=NZH19110807.2.142.3&srpos=10|accessdate=14 July 2015|work=The New Zealand Herald|date=7 August 1911|page=9}}</ref> and nearly scoring in a close game against Auckland, which resulted in a 8–8 draw.<ref>{{cite news|title=Auckland v Country Unions|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=NZH19110731.2.7.1&srpos=113|accessdate=14 July 2015|work=The New Zealand Herald|date=31 July 1911|page=4}}</ref> The North versus South match, which the North won 18–6,<ref>{{cite news|title=North Island v South|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=ME19110814.2.10&srpos=101|accessdate=14 July 2015|work=Mataura Ensign|date=14 August 1911|page=3}}</ref> was described in the press as lacking in quality: it was an "uninteresting match",<ref name=Awkward>{{cite news|title=The Awkward Squad in Inter-Island Game|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=DOM19110814.2.13&srpos=109|accessdate=14 July 2015|work=Dominion|date=14 August 1911|page=4}}</ref> and "play was crude and poor and lacked vigour".<ref>{{cite news|title=Country Players Meet|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19110814.2.30&srpos=124|accessdate=14 July 2015|work=Evening Post|date=14 August 1911|page=4}}</ref> Downing stood out: "The only member of either team who could be said to have played up to inter-island form was Downing – a fine forward in the North Island team."<ref name=Awkward />

The following year, Downing's name was put forward by Hawke's Bay to play for the North Island in the annual Inter-Island match and he made selection.<ref>{{cite news|title=The Inter-Island Match|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=CHP19120626.2.28.3&srpos=23|accessdate=14 July 2015|work=The Press|date=26 June 1912|page=7}}</ref><ref>{{cite news|title=North Island Team|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19120703.2.8&srpos=47|accessdate=14 July 2015|work=Evening Post|date=3 July 1912|page=2}}</ref> The North beat the South 12–8 in a closely contested, entertaining game in front of a full capacity crowd. The Northern forwards in general played a good game; it was suggested that Downing's passing back from the lineout to the halfback was something that other forwards might consider imitating.<ref>{{cite news|title=North v South|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&d=HAST19120715.2.9&cl=search&srpos=68|accessdate=14 July 2015|work=Hastings Standard|date=15 July 1912|page=2}}</ref> A tour with the North Island Country team followed, in which Downing played all four games.<ref name="napierobmaristrugby.co.nz"/> The last, against South Island Country in Wellington on 4 August, had to be stopped at half time due to the condition of the match ground, and the North won 14–3.<ref>{{cite news|title=North and South Country Teams at Play|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=WC19120805.2.5.5&srpos=94|accessdate=14 July 2015|work=Wanganui Chronicle|date=5 August 1912|page=3}}</ref> His Hawke's Bay and North Island Country team-mate Norman McKenzie described him as "an outstanding line-out forward with a wonderful pair of hands".<ref name="napierobmaristrugby.co.nz"/>

===Auckland and the All Blacks===
[[File:1913 All Blacks team that toured California.jpg|right|thumb|300px|The New Zealand team before their departure to North America in 1913. Downing is standing in the middle of the back row.|alt=A team of men wearing blazers and hats posing for a formal photo.]]
Downing relocated to [[Auckland]] at the end of 1912 and was recruited by [[Marist Brothers Old Boys RFC|Auckland Marists]] on the strength of his playing and the links with the Napier Marists.<ref name="napierobmaristrugby.co.nz"/> He was the club's first [[All Black]], joined shortly after by Jim "Buster" Barrett.<ref name="aucklandmarist.co.nz"/> Downing's debut match was against Australia in Wellington on 6 September, which the All Blacks won easily 30–6, bettering Australia "in every respect".<ref>{{cite news|title=New Zealand v Australia|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=ODT19130908.2.4&srpos=320|accessdate=14 July 2015|work=Otago Daily Times|date=8 September 1913|page=2}}</ref> 

He was subsequently selected for the [[1913 New Zealand rugby union tour of North America|tour of North America]] the same year. On 10 September, the eve of departure, Wellington took on the All Blacks in a "thrilling" game which saw the visitors nearly defeated. With the wind behind them in the first half, the All Blacks gained a 13-point lead; but in the second half, Wellington came back strongly; and with a drop-goal in the final three minutes, closed the lead to just one point, 19–18. There was strong back play on both sides; amongst the forwards, one player from each side received special mention in the press: Downing for the All Blacks, Miller for Wellington.<ref>{{cite news|title=The Blacks Meet Wellington|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=DOM19130911.2.95.1&srpos=330|accessdate=14 July 2015|work=The Dominion|date=11 September 1913|page=8}}</ref><ref>{{cite news|title=Almost Defeated All Blacks Narrow Escape|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19130911.2.34&srpos=71|accessdate=14 July 2015|work=Evening Post|date=11 September 1913|page=4}}</ref>

It is possible that Downing might not have been selected for the tour had he not moved to Auckland.<ref>{{cite news|title=Rugby Representatives|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=NZTR19130927.2.33&srpos=342|accessdate=14 July 2015|work=NZ Truth|issue=431|date=27 September 1913|page=6}}</ref> As it was, he played in 14 of the 16 matches, and contributed 6 tries for 18 points towards a total tally of 610 points.<ref>{{cite news|title=The All Blacks Californian Tour – A Successful Finish|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19131220.2.23&srpos=402|accessdate=14 July 2015|work=Evening Post|date=20 December 1913|page=4}}</ref> There was little interest in the American press about the tour. A single short paragraph in the ''[[New York Tribune]]'' reports on the 51–3 defeat of the All America team on 15 November. In New Zealand, meanwhile, detailed match reports were coming in, and many of these were full of praise for Downing.<ref>{{cite news|title=All Blacks – The Californian Tour – Incidents on the Journey – Details of the First Matches|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19131106.2.7&srpos=359|accessdate=14 July 2015|work=Evening Post|date=6 November 1913|page=2}}</ref><ref>{{cite news|title=With the All Blacks|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=CHP19131108.2.107&srpos=361|accessdate=14 July 2015|work=The Press|date=8 November 1913|page=14}}</ref><ref>{{cite news|title=Touring 'All Blacks'|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=NZH19131111.2.8&srpos=366|accessdate=14 July 2015|work=The New Zealand Herald|date=11 November 1913|page=4}}</ref> Of all the players in the USA match, four would die in the Great War: [[Frank Jacob Gard]], the USA captain (died 29 September 1918); and three All Blacks: [[Henry Dewar (rugby)|Henry Dewar]], [[George Sellars]] and Downing himself.<ref>{{cite web|url=http://therugbyhistorysociety.co.uk/mitchellmm.html|title=Mowatt Mitchell From The Rugby History Society|publisher=therugbyhistorysociety.co.uk}}</ref><ref name="napierobmaristrugby.co.nz"/> 

In 1914, Downing was again selected for the Inter-Island match in Wellington on 9 June,<ref>{{cite news|title=The Inter-Island Match|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=CHP19140609.2.73.5&srpos=452|accessdate=14 July 2015|work=The Press|date=9 June 1914|page=8}}</ref> which the South won 8–0.<ref>{{cite news|title=South Island Wins|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=TS19140620.2.97.1&srpos=486|accessdate=14 July 2015|work=The Star|issue=11109|date=20 June 1914|page=9}}</ref> Later in the year, four of Downing's Marist team-mates were with him in the All Black side selected to tour Australia: Barrett, five eighths Jock McKenzie, who had transferred from Wellington, and fullback Jack O’Brien, a founding member of the club.<ref>{{cite web|url=http://clubhubssl.com/rss/CMS1257/history.php|title=Marist Brothers Old Boys Rugby Club|author=ClubHub|publisher=clubhubssl.com}}</ref> The All Blacks played Wellington again on the day before leaving for Australia, this time losing 19–14, Downing contributing a try.<ref>{{cite news|title=All Blacks Defeated|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=DOM19140702.2.87&srpos=533|accessdate=14 July 2015|work=The Dominion|date=2 July 1914|page=8}}</ref> Downing played in 10 of the 11 matches, including the 3 tests,<ref name="aucklandmarist.co.nz"/> and was praised for his line-out ability.<ref name="napierobmaristrugby.co.nz"/> In the first test on 18 July, he was, according to ''The Star'', "easily the best forward in the team",<ref>{{cite news|title=Rugby Game|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=TS19140808.2.18&srpos=638|accessdate=14 July 2015|work=The Star|issue=11151|date=8 August 1914|page=4}}</ref> and after the second test, ''The Southland Times'' opined that "it is quite possible that before he leaves the lengthy Aucklander may prove himself to be included in the star category of New Zealand forwards."<ref>{{cite news|title=Football|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=ST19140808.2.48&srpos=639|accessdate=14 July 2015|work=Southland Times|issue=17724|date=8 August 1914|page=8}}</ref>

In an obituary, Downing was described as "big, strong, fast, brainy, clever with hands and feet, dashing, and resourceful." He was best known for his work in the line-out and in the loose, equally good in attack as in defence. He played hard but clean.<ref name=Sun150911>{{cite news|title=For King and Empire: Dardanelles Casualties: Sergeant A. J. Downing|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=SUNCH19150911.2.50&srpos=2|accessdate=11 July 2015|work=The Sun|issue=496|date=11 September 1915|page=8}}</ref> Such was his devotion to rugby that Downing had a tattoo on his left forearm of the [[Ranfurly Shield]].<ref>{{cite web|url=http://www.allblacks.com/News/25606/lest-we-forget-ww1-and-rugby-in-new-zealand|title=News|work=All Blacks}}</ref>{{sfn|Kidd|2012}}

===International appearances===
{| class="wikitable sortable"
|-
! Opposition !! Score !! Result !! Date !! Venue
!scope="col" class="unsortable" | {{abbr|Ref(s)|Reference(s)}}
|-
| {{ru|Australia}}||align="center"| 30–5 || Win ||align=right| 6 September 1913 || [[Wellington, New Zealand]] ||align="center"| <ref>{{cite web|url=http://en.espn.co.uk/statsguru/rugby/match/19180.html|title=Rugby Union – ESPN Scrum – New Zealand v Australia at Wellington|work=ESPN scrum}}</ref>
|-
| {{ru|USA}}||align="center"| 3–51 || Win ||align=right| 15 November 1913 || [[Berkeley, California]] ||align="center"|<ref>{{cite web|url=http://en.espn.co.uk/statsguru/rugby/match/19183.html|title=Rugby Union – ESPN Scrum – United States of America v New Zealand at Berkeley|work=ESPN scrum}}</ref> 
|-
| {{ru|Australia}} ||align="center"| 0–5 || Win ||align=right| 18 July 1914 || Sydney ||align="center"|<ref>{{cite web|url=http://en.espn.co.uk/statsguru/rugby/match/19193.html|title=Rugby Union – ESPN Scrum – Australia v New Zealand at Sydney|work=ESPN scrum}}</ref>
|-
| {{ru|Australia}} ||align="center"| 0–17 || Win ||align=right| 1 August 1914 || [[Brisbane]], Australia ||align="center"|<ref>{{cite web|url=http://en.espn.co.uk/statsguru/rugby/match/19194.html|title=Rugby Union – ESPN Scrum – Australia v New Zealand at Brisbane|work=ESPN scrum}}</ref>
|-
| {{ru|Australia}}||align="center"| 7–22 || Win ||align=right| 15 August 1914 || Sydney ||align="center"|<ref>{{cite web|url=http://en.espn.co.uk/statsguru/rugby/match/19195.html|title=Rugby Union – ESPN Scrum – Australia v New Zealand at Sydney|work=ESPN scrum}}</ref> 
|}

==Military career==
During the All Black tour of Australia, in the game against Metropolitan Union in Sydney on 5 August 1914, the news was posted on the scoreboard that the British Empire – and therefore New Zealand and Australia – had declared war.<ref>{{cite web|url=http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=11306911|title=100 Kiwi Stories: Thirteen All Blacks who never returned from war|work=The New Zealand Herald}}</ref> On the ship home, the players collectively decided to volunteer for military service. Three of them were killed, including Downing at [[Gallipoli]], Bobby Black at the Somme and Jim McNeece at Messines.<ref name=nzr>[http://www.nzrugby.co.nz/ww100/rugby-and-the-first-world-war/all-blacks-in-1914 All Blacks in 1914], NZRugby. Retrieved 7 July 2015</ref>

Downing enlisted with the Fifth Reinforcements (Wellington Battalion) on 2 February 1915.{{sfn|Sewell|1919|p=46}} While doing basic training, he also played two games of rugby for the Trentham Military Forces Team, against Wellington on 1 May and Auckland on 5 June.{{sfn|McCrery|2014|p=136}}<ref name=Dropkick/><ref name=H&N>{{cite news|title=Sport and the War|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=HNS19150925.2.26.6&srpos=4&e=--1839---1920--50--1----0Doolan+Downing--|accessdate=11 July 2015|work=Hawera & Normanby Star|date=25 September 1915|page=6}}</ref> In the first of these, Downing was reckoned to be the standout forward of the Trentham team, and "played splendidly".<ref name=Dropkick>{{cite news|author1="Dropkick"|title=Trentham "Reps" against Wellington|url=http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&cl=search&d=EP19150508.2.162&srpos=3&e=--1839---1920--50--1----0Doolan+Downing--|accessdate=11 July 2015|work=The Evening Post|issue=108|date=8 May 1915}}</ref>

On 13 June, he departed bound for Suez in Egypt, arriving 24 July.{{sfn|McCrery|2014|p=136}} His unit took part, beginning on 6 August, in the [[Battle of Chunuk Bair]], in support of the [[landing at Suvla Bay]], which was intended to break the deadlock in the [[Gallipoli Campaign]]. The initial assault was successful and early on the morning of 8 August, Downing was with A Company occupying the Turkish trench on the crest of Chunuck Bair. The Turks counter-attacked at dawn, forcing back the British battalions and the Wellingtons. The crest was lost and the battle continued for 12 hours on the seaward slopes. By nightfall, Downing, who had earlier distinguished himself in a bayonet charge, was killed, reportedly "blown to pieces".<ref name="aucklandmarist.co.nz"/> Downing was the first of 13 All Blacks killed in the war,<ref>{{cite web|url=http://www.stuff.co.nz/manawatu-standard/sport/10353242/Boots-and-bullets-fallen-All-Blacks|title=Boots and bullets – fallen All Blacks|work=Stuff}}</ref> just a day before Henry Dewar, the second All Black to fall,<ref name="napierobmaristrugby.co.nz"/> was killed in action with the [[Wellington Mounted Rifles Regiment|Wellington Mounted Rifles]] at [[Anzac Cove]].<ref>
[http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=11306911 100 Kiwi Stories: Thirteen All Blacks who never returned from war], Andrew Stone, ''The New Zealand Herald''. Retrieved 7 July 2015</ref>

Sergeant Doolan Downing is commemorated on panel 17 of the New Zealand Memorial to the Missing on Chunuk Bair, along with his commanding-officer, Lt Col [[William George Malone]], who died aged 56, and more than 300 other men of his battalion.{{sfn|McGibbon|2014}}

==References==
{{Reflist|30em}}

===Bibliography===
*{{cite book|last1=Kidd|first1=Heather|title=Piri – Straight Up: Cups, Downs & Keeping Calm|date=2012|publisher=Hachette UK|isbn=1869712897|ref=harv}}
*{{cite book|last1=McCrery|first1=Nigel|title=Into Touch: Rugby Internationals Killed in the Great War|date=2014|publisher=Pen and Sword|isbn=1473833213|ref=harv}}
*{{cite book|last1=McGibbon|first1=Ian|title=Gallipoli: A Guide to New Zealand Battlefields and Memorials|date=2014|publisher=Penguin UK|isbn=1743486898|ref=harv}}
*{{cite book|last1=Sewell|first1=Edward Humphrey Dalrymple|authorlink=E. H. D. Sewell|title=The Rugby Football Internationals Roll of Honour|date=1919|publisher=T. C. & E. C. Jack|location=London, Edinburgh|ref=harv}}

[[Category:1886 births]]
[[Category:1915 deaths]]
[[Category:New Zealand international rugby union players]]
[[Category:New Zealand military personnel killed in World War I]]
[[Category:Deaths in Turkey]]