{{Infobox journal
| title = American Journal of Sports Medicine
| cover = [[File:American Journal of Sports Medicine.tif]]
| editor = Bruce Reider, MD
| discipline = [[Health Sciences]]
| former_names = 
| abbreviation = Am. J. Sports Med.
| publisher = [[SAGE Publications]]
| country = United Kingdom
| frequency = Monthly
| history = 1976–present
| openaccess = 
| license = 
| impact = 4.362
| impact-year = 2014
| website = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201672
| link1 = http://ajs.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ajs.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 2314681
| LCCN = 76646332
| CODEN = 
| ISSN = 0363-5465
| eISSN = 1552-3365
}}

'''''American Journal of Sports Medicine''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Sport Sciences]]. The journal's [[Editor-in-Chief|editor]] is Bruce Reider, MD. It has been in publication since 1972 and is currently published by [[SAGE Publications]] in association with American Orthopedic Society for Sports Medicine.

== Scope ==
''American Journal of Sports Medicine'' is a source of information for sports medicine specialists with the aim of improving the identification, prevention, treatment and rehabilitation of sports injuries. The journal acts as a forum for orthopaedic sports medicine research and education, allowing clinical practitioners the ability to make decisions based on scientific information.

== Abstracting and indexing ==
''The American journal of Sports Medicine'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 4.362, ranking it 1 out of 72 journals in the category ‘Orthopedics’.<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Orthopedics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=|work=Web of Science |postscript=.}}</ref> and 6 out of 81 journals in the category ‘Sport Sciences’.<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Sport Sciences |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=|work=Web of Science |postscript=.}}</ref> The journal's 5-Year Impact Factor is currently listed at 5.084.

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://ajs.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Sports medicine journals]]