{|{{Infobox Aircraft Begin
  |name = FBA-2
  |image = File:FoundFBA-2C1BushHawkC-GYWF.jpg
  |caption = Found Aircraft FBA-2C1 BushHawk
}}{{Infobox Aircraft Type
  |type = Cabin monoplane
  |national origin=[[Canada]]
  |manufacturer = [[Found Aircraft]]
  |designer = [[S.R. Found]] 
  |first flight = [[1960 in aviation|1960]] 
  |introduced = 
  |retired = 
  |status = Production completed
  |primary user = 
  |more users = 
  |produced = 
  |number built = 68
  |unit cost = 
  |developed from = [[Found FBA-1]]
  |variants with their own articles = [[Found Centennial]]
}}
|}

The '''Found FBA-2''' is a 1960s [[Canada|Canadian]] four/five-seat cabin [[monoplane]] that was produced by [[Found Aircraft]].

==Design and development==
The '''Found FBA-2''' is an all-metal development of the company's first design, the [[Found FBA-1]]. The prototype first flew on 11 August 1960. It is a high-wing monoplane with a fixed tricycle undercarriage. The production version was to be the '''Found FBA-2B''' but the aircraft was produced with a conventional tail-wheel landing gear as the '''Found FBA-2C'''. The first production FBA-2C first flew on 9 May 1962. It is powered by an Avco Lycoming O-540-A1D engine and had a slightly longer cabin and enlarged cabin doors than the prototype. Originally, float or ski landing gear was available through third parties, and later became a factory option. Production ended in 1965 to concentrate on building the newer and larger [[Found Centennial|Centennial 100]]. Thirty-four had been built.<ref name="FoundHistory">{{cite web|url = http://www.foundair.com/index.php?module=pagemaster&PAGE_user_op=view_page&PAGE_id=6|title = Found Aircraft - A Brief History|accessdate = 2008-02-20|last = Found Aircraft Development|authorlink = |year = n.d.}}</ref>

In 1996 the design was acquired by Found Aircraft Development who developed an improved model the  FBA-2C2 Bush Hawk-XP. This model was certified by [[Transport Canada]] in March, 1999 and by the [[Federal Aviation Administration]] in March, 2000. This version was manufactured between 2000-2007, after which it was replaced by a new version of the same basic airframe designated the Expedition E350 and the Expedition E350XC.<ref name="FoundHistory"/>

===Expedition E350===
[[File:Expedition-E350.jpg|right|thumb|Expedition E350]]
The E350 is an evolutionary development of the basic FBA-2 aimed at the personal use market. The Expedition E350 was [[Federal Aviation Administration|FAA]] [[type certificate|type certified]] in December 2008.  The aircraft can be equipped with four or five seats and has a full fuel payload in excess of 900 pounds. It has a range of {{convert|700|nmi|km|0|abbr=on}} at a cruise speed of {{convert|156|kn|km/h|0|abbr=on}} and is powered by a [[Lycoming IO-580]] powerplant producing  {{convert|315|hp|kW|0|abbr=on}}. The E350 has been designed with rugged landing gear for operating from unprepared surfaces and has STOL performance.<ref name="Expedition">{{cite web|url = http://www.expeditionaircraft.com|title = Expedition Aircraft|accessdate = 2009-01-01|last = Expedition Aircraft|authorlink = |year = n.d.}}</ref><ref name="AvWeb31Dec08">{{cite web|url = http://www.avweb.com/avwebflash/news/CanadasExpeditionE350GetsFAAOK_199492-1.html|title = Canada's Expedition E350 Gets FAA OK |accessdate = 2008-12-31|last =Grady|first = Mary|authorlink = |date=December 2008}}</ref>

[[Pacific Aerospace]] acquired the E-350 program in early 2016 from Found Aircraft. In September 2016, the E-350 Expedition tooling was shipped to its [[Hamilton, New Zealand]] plant. Pacific Aerospace plans to relaunch production of the five-seat type in the first half of 2017. It is also planned to produce the E-350 in its joint venture plant in [[China]] with [[Beijing General Aviation Company]]. The joint venture will be known as [[Beijing Pan-Pacific Aerospace Technology]].<ref>{{cite web|url = https://www.flightglobal.com/news/articles/pacific-aerospaces-chinese-venture-ready-for-21-oct-430476/|title=Pacific Aerospace's Chinese venture ready for 21 October inauguration|last=Sarsfield|first=Kate|work=[[Flight Global]]|accessdate = 2016-10-15}}</ref><ref>{{cite web|url = http://www.avweb.com/avwebflash/news/Found-Expedition-To-Resume-Production-228030-1.html?omhide=true|title = Found Expedition To Resume Production|accessdate = 14 November 2016|last = Niles|first = Russ|work=AVweb|date = 13 November 2016}}</ref>

==Variants==
;FBA-2
:Prototype, one built.
;FBA-2C
:Initial production variant, 26 built.
;FBA-2C1 Bush Hawk 300
:Improved variant with a 300hp Lycoming IO-540L, one built.
;FBA-2C1 Bush Hawk XP
:Production variant of the Bush Hawk 300, 31 built.
;FBA-2C2 Bush Hawk 300XP
:Minior changes, 6 built.
;FBA-2C3 Expedition E350
:Tricycle landing gear first flown in 2006, 3 built.
;FBA-2C4 Expedition E350XC
:Tail-wheel, one built.
;FBA-2D
:Proposed variant powered by a 290hp Lycoming engine, not built.

==Specifications (FBA-2C)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref= Jane's All The World's Aircraft 1965-66 <ref name="Janes 65 p23-4">Taylor 1965, pp. 23–24.</ref>
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|crew=1
|length main= 26 ft 5 in
|length alt= 8.05 m
|span main= 36 ft 0 in
|span alt= 10.97 m
|height main= 8 ft 4 in
|height alt= 2.54 m
|area main= 180.0 ft²
|area alt= 16.72 m²
|empty weight main= 1,550 lb
|empty weight alt= 703 kg
|loaded weight main= 
|loaded weight alt= 
|max takeoff weight main= 2,950 lb 
|max takeoff weight alt= 1,338 kg
|engine (prop)=[[Lycoming O-540|Avco Lycoming O-540-A1D]]
|type of prop= flat-six piston
|number of props=1
|power main= 250 hp
|power alt= 187 kw
|max speed main= 147 mph
|max speed alt=128 knots, 237 km/h
|max speed more=at sea level
|cruise speed main=129 mph
|cruise speed alt=112 knots, 208 km/h
|cruise speed more=(economy cruise at 5,000 ft (1,525 m))
|range main= 610 miles
|range alt=530 [[nautical mile|nmi]], 980 km
|ceiling main= 16,000 ft
|ceiling alt= 4875 m
|climb rate main= 1,100 ft/min
|climb rate alt=5.6 m/s
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament= 
}}<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=
|lists=
|see also=
}}

==References==
{{Reflist}}
{{commons category|Found FBA-2}}
* ''The [[Illustrated Encyclopedia of Aircraft]] (Part Work 1982-1985)''. London: Orbis Publishing.
*{{cite book|last=Taylor|first=John W. R.|authorlink=John W. R. Taylor |coauthors= |title= Jane's All The World's Aircraft 1965-66 |year=1965 |publisher=Samson Low, Marston |location= London|isbn= |page= |pages= |url= |accessdate=}}

<!-- ==External links== -->
{{Found Aircraft}}

[[Category:Canadian civil utility aircraft 1960–1969]]