{{Infobox cyclist
| name          = Torkil Veyhe
| image         = Road Bicycle Racers in Statoil Kring Føroyar 2010.jpg
| caption       = Torkil Veyhe (number two from left) at the 2010 [[Tour of Faroe Islands|Kring Føroyar]]
| fullname      = Torkil Veyhe
| nickname      =
| birth_date    = {{birth date and age|1990|1|9|df=y}}
| birth_place   = 
| height        = {{convert|186|cm|ftin|abbr=on}}
| weight        = {{convert|80|kg|lb|abbr=on}}
| currentteam   = [[Team ColoQuick–Cult]]
| discipline    = Road race
| role          = Rider
| ridertype     = 
| amateuryears1 = 2007–2012
| amateurteam1  = Tórshavnar Súkklufelag
| amateuryears2 = 2012–2015
| amateurteam2  = Odder Cykel Klub
| amateuryears3 = 2015–2016
| amateurteam3  = Team WeBike-CK Aarhus
| amateuryears4 = 
| amateurteam4  = 
| proyears1     = 2017
| proteam1      = [[Team ColoQuick–Cult]]
| majorwins     = 
| medaltemplates = 
{{MedalCountry| {{FRO}} }}
{{MedalCompetition|[[Island Games]]}}
{{MedalGold | [[2015 Island Games|2015 Jersey]] | Road race}}
{{MedalSilver| [[2013 Island Games|2013 Bermuda]] | Time trial}}
{{MedalSilver| 2013 Bermuda | Road race}}
{{MedalSilver | [[2009 Island Games|2009 Åland Islands]] | Team}}
{{MedalBronze | 2015 Jersey | Road race}}
{{MedalBronze | 2015 Jersey | Team}}
| updated       = 4 November 2016
}}

'''Torkil Veyhe''' (born 9 January 1990) is a [[Faroe Islands|Faroese]] [[road bicycle racer]] riding for the Danish [[UCI Continental]] team [[Team ColoQuick–Cult]].<ref name="coloquick">{{cite web |url=http://feltet.dk/nyheder/torkil_veyhe_til_coloquick-cult/ |title=Torkil Veyhe til coloQuick-CULT |last1=Berg |first1=Christian |date=4 November 2016 |website=Feltet.dk |publisher= |access-date=4 November 2016 |language=Danish}}</ref> He has also been riding for the national cycling team of the [[Faroe Islands]] at the [[Island Games]] and in 2016 he rode for the Danish national team at the [[Danmark Rundt]]. Earlier he was cycling for the [[Danish Cycling Federation|DCU]] team [[Team WeBike—CK Aarhus]], the Danish club Odder Cykel Klub and the Faroese club Tórshavnar Súkklufelag. In 2016 he won his first A-race in Denmark and his first UCI points. He has won gold, silver and bronze at the [[Island Games]] for the Faroe Islands.

== Background and education ==
Torkil Veyhe comes from the [[Faroe Islands]], from the capital [[Tórshavn]]. He started as a cyclist there and soon became a multiple Faroese Champion<ref name="FM2015">{{cite web|url=http://tsf.fo/news-fm+2015+torkil+gjordist+trifalt+fm+vinnari.htm|title=FM 2015: Torkil gjørdist stóri vinnarin|year=2015|publisher=Tórshavnar Súkklufelag|language=Faroese|accessdate=30 June 2016}}</ref> as well as multiple winner of [[Tour of Faroe Islands]] (Kring Føroyar) road race.<ref name="volvo2014">{{cite web|url=http://www.jn.fo/volvo+kring+foroyar+torkil+veyhe+stori+sigursharrin.html|title=VOLVO Kring Føroyar: Torkil Veyhe stóri sigursharrin|last=Nielsen|first=Jóanis|date=28 July 2014|publisher=jn.fo|language=Faroese|accessdate=30 June 2016}}</ref><ref name="KringForoyar2015">{{cite web|url=http://www.tsf.fo/news-volvo+kring+foroyar+2015+enn+ein+sigur+til+torkil+veyhe.htm|title=Volvo Kring Føroyar 2015 - enn ein sigur til Torkil Veyhe|year=2015|publisher=Tórshavnar Súkklufelag|language=Faroese|accessdate=30 June 2016}}</ref><ref name="Veyhe_vann_2015">{{cite web|url=http://www.in.fo/news-detail/news/torkil-veyhe-vann-volvo-kring-foeroyar/|title=Torkil Veyhe vann Volvo Kring Føroyar|last=Rana|first=Hallur av|date=27 July 2016|publisher=in.fo|language=Faroese|accessdate=30 June 2016}}</ref> After finishing the [[Higher Commercial Examination Programme]] (HHX) in Tórshavn in 2010, Veyhe moved to Aarhus in Denmark to study for Engineer and to compete in Danish cycling competitions. First he took one year of preparation for the education he wanted to study, and the following year he started the education. He took the Bachelor of Engineering (B.E.) in Structural Engineering from the [[Aarhus University]]  School of Engineering in 2014.<ref name="Linkedin">{{cite web|url=https://dk.linkedin.com/in/torkilveyhe|title=Profile on Linkedin|publisher=Linkedin|accessdate=21 June 2016}}</ref> The same year he continued to study for the Master of Science in Engineering degree which he finished in 2016.<ref name="succeshold">{{cite web|url=http://stiften.dk/sport/aarhusiansk-succeshold-koerer-saa-hurtigt-at-pengene-ikke-kan-foelge-med|title=Århusiansk succeshold kører så hurtigt, at pengene ikke kan følge med|last=Graahede|first=Kim Robin|date=26 June 2016|publisher=Århus Stiftstidende|language=Danish|accessdate=30 June 2016}}</ref> Veyhe defended his MS final thesis on the day after competing at the Danish Championships time trial, where he was number 9, and two days after the exam he competed at the Danish Championships road race, hitting an early break out together with three other cyclists [[Chris Anker Sørensen]], [[Michael Reihs]], and [[Mark Sehested Pedersen]].<ref name="flotDM">{{cite web|url=http://elite.ckaarhus.dk/flot-dm-lukkede-foraarssaesonen/|title=Flot DM lukkede forårssæsonen|date=27 June 2016|publisher=elite.ckaarhus.dk|language=Danish|accessdate=30 June 2016}}</ref> After the Danish Championships he was elected for the Danish national team.<ref name="udtaget2016">{{cite web|url=http://www.postnorddanmarkrundt.dk/postnord-danmark-holdet-er-udtaget-kan-koere-med-om-sejre-og-i-det-samlede-klassementet/|title=PostNord Danmark-holdet er udtaget: Kan køre med om sejre og i det samlede klassement|date=29 June 2016|publisher=Postnord Danmark Rundt|language=Danish|accessdate=30 June 2016}}</ref>

== Career as a cyclist==

He has won competitions like [[Cycling at the Island Games|Island Games]] in 2015 and [[Tour of Faroe Islands|Kring Føroyar]] in 2009, 2010, 2012, 2014,<ref>[http://www.in.fo/index.php?id=42&tx_news_pi1%5Bnews%5D=14926&tx_news_pi1%5Bcontroller%5D=News&tx_news_pi1%5Baction%5D=detail&cHash=7e7ad3cb12a58bbc7a44bf85fb5a542f in.fo - Torkil Veyhe fyrstur á mál]</ref> and 2015. He is Faroese champion in time trial<ref>[http://www.tsf.fo/news-urslit+fm+byarkoyring+2012.htm tsf.fo - Úrslit: FM 2012]</ref> and in road race 2009.<ref>[http://vev.fo/20090912+torkil+weyhe+er+foeroyameistari+i+landavegssukkling.html vev.fo]</ref> Torkil Veyhe moved to live in Denmark to study and at the same time he continued to cycle on a high level. The first years in [[Denmark]], he competed in lower class, but soon he won a 124-km race in [[Hjørring]] for license cyclists in the B class.<ref>[http://cykelmagasinet.dk/resultater_fra_hjoerring cykelmagasinet.dk]</ref> After competing in five races in Denmark in 2013 he was promoted to the A class.<ref>[http://cykelmagasinet.dk/faeroesk-mester-og-steensen-forlaenger cykelmagasinet.dk - Færøsk mester og Steensen forlænger]</ref> In June 2015 he took bronze in the Nishiki Løbet 2015 (Nishiki Race 2015) in [[Randers]].<ref>[http://ny.cyklingdanmark.dk/kontakt/presse/details/artikel/resultater-nishiki-loebet-2015-randers-ck.html ny.cyklingdanmark.dk - Resultater: Nishiki Løbet 2015, Randers CK]</ref>

On 15 October 2015 Veyhe got a two year agreement with Tryggingarfelagið Føroyar (a Faroese insurance company), which will support him financially until October 2017.<ref name="Stuðulsavtala">{{cite web|url=http://portal.fo/torkil+veyhe+fingid+studulsavtalu.html|title=Torkil Veyhe fingið stuðulsavtalu|last=Patursson|first=Steinar|date=15 October 2015|publisher=Portal.fo|language=Faroese|accessdate=16 October 2015}}</ref>

After joining Team We-Bike CK Aarhus in November 2015 he sat two goals for 2016, he would win an A-race and he would win his first UCI points.<ref name="cykelmagasinet2015">{{cite web|url=http://cykelmagasinet.dk/faeroesk-mester-haaber-paa-sejr|title=Færøsk mester håber på sejr|last=Mogensen|first=Steffen|date=22 November 2015|publisher=Cykelmagasinet|language=Danish|accessdate=21 June 2016}}</ref> In April and May 2016 both goals were achieved, he won his first 3 UCI points when finishing as number eight at the [[Himmerland Rundt]] UCI category 1.2 on 29 April 2016 and two weeks later he won the A-race JE.DK in [[Herning]].<ref name="je.dk2016">{{cite web|url=http://www.feltet.dk/nyheder/resultater_fra_jedk_loebet_herning/|title=Resultater fra JE.dk Løbet Herning|last=Askholt|first=Anders|date=15 May 2016|publisher=Feltet.dk|language=Danish|accessdate=21 June 2016}}</ref> After the Danish Championships in June 2016 he was elected to ride for the Danish national team at the [[2016 Danmark Rundt|Danmark Rundt]],<ref name="Torkil-in.fo-2016">{{cite web|url=http://www.in.fo/itrottur/sport-detail/torkil-er-uttikin-til-landslidid/|title=Torkil er úttikin til landsliðið|last=Rana|first=Danjal av|date=29 June 2016|publisher=in.fo|language=Faroese|accessdate=30 June 2016}}</ref> along with team mate Niklas Pedersen.<ref name="udstillingsvindue">{{cite web|url=http://cykelmagasinet.dk/webike-ck-aarhus-et-godt-udstillingsvindue|title=Webike CK-Aarhus: Et godt udstillingsvindue|last=Pedersen|first=Asger Brix|date=29 June 2016|publisher=cykelmagasinet.dk|language=Danish|accessdate=30 June 2016}}</ref> At the third stage he won the combativity award.<ref name="skysovs">{{cite web|url=http://www.dr.dk/sporten/cykling/postnord-danmark-rundt/video-dagens-faering-i-skysovs-rigtig-fedt-jeg-kan-goere-det|title=VIDEO Dagens færing i skysovs: Rigtig fedt, at jeg kan gøre det her|last=Akbal|first=Ejder|date=29 July 2016|publisher=Danmarks Radio|language=Danish|accessdate=31 July 2016}}</ref>

=== Results in 2016 ===

=== PostNord Danmark Rundt ===
* Number 98.
**Combativity award at the third stage at the [[2016 Danmark Rundt]]<ref name="skysovs"/>

==== National Championships Denmark ====
* Number 9 in the ITT on 23 June 2016, distance: 43 km<ref name="torkil-2016">{{cite web|url=http://www.in.fo/itrottur/sport-detail/torkil-enn-eitt-gott-urslit/|title=Torkil enn eitt gott úrslit|last=Albinus|first=Pætur|date=23 June 2016|publisher=in.fo|language=Faroese|accessdate=24 June 2016}}</ref><ref name="bornholmsk">{{cite web|url=http://feltet.dk/nyheder/bornholmsk_overraskelse_snoed_de_store_favoritter/|title=Bornholmsk overraskelse snyder de store favoritter|last=Penning|first=Lasse Wededge|date=23 June 2016|publisher=Feltet.dk|language=Danish|accessdate=24 June 2016}}</ref>
* Number 51 in the road race on 26 June, distance: 201 km. Hit an early breakout.<ref name="flotDM"/>

==== UCI category 1.2 ====
* Number 8 in [[Himmerland Rundt]], 29 April 2016, distance: 188,3 km<ref name="himmerland2016">{{cite web|url=http://www.cyclingfever.com/stage.html?etappe_idd=MzQxNjQ=|title=Himmerland Rundt 2016 (1.2)|publisher=Cycling Fever|accessdate=21 June 2016}}</ref>
* Number 9 in [[GP Horsens]] Posten, 12 June 2016<ref>[http://www.tv2oj.dk/nyheder/12-06-2016/1930/amatorhold-i-verdensklasse?autoplay=1#player?clip#player tv2oj.dk - AMATØRHOLD I VERDENSKLASSE]</ref>
** Winner of the  mountain classification in GP Horsens, 12 June 2016.<ref>[http://www.feltet.dk/nyheder/kamp_slog_til_i_horsens/#mlYQEmzITKeuu1GZ.97 Felstet.dk - Kamp slog til i Horsens]</ref>
* Number 31 in [[Fyen Rundt]]. 11 June 2016, distance: 215 km
* Number 77 in Rent Liv Løbet Skive, 1 May 2016, distance: 177,7 km

==== DCU-A races in Denmark ====
* Winner of JE.DK Løbet Herning, one of three stages of the Marcello Bergamo Cup.<ref name="firstcycling">{{cite web|url=http://firstcycling.com/rider.php?r=22295|title=Torkil Veyhe|publisher=First Cycling|accessdate=21 June 2016}}</ref>
* Winner of the Danish road Marcello Bergamo Cup, also known as Pinsecuppen, on 15 May 2016.<ref name="Marcello_Bergamo_Cup">{{cite web|url=http://www.feltet.dk/nyheder/slutstillinger_i_marcello_bergamo_cuppen/|title=Se slutstillingerne i Marcello Bergamo Cuppen|last=Jesper|first=Johannesen|date=18 May 2016|publisher=Feltet.dk|language=Danish|accessdate=30 June 2016}}</ref>
* Number 2 in Sønderborg-Post Cup<ref name="sonderborg2016">{{cite web|url=http://portal.fo/torkil+aftur+a+sigurspallinum.html|title=Torkil aftur á sigurspallinum|last=Samuelsen|first=Ingi|date=30 May 2016|publisher=Portal.fo|language=Faroese|accessdate=21 June 2016}}</ref>
* Number 2 in Designa løbet in Silkeborg<ref name="Silkeborg2016">{{cite web|url=http://www.feltet.dk/nyheder/resultater_fra_designa_loebet_silkeborg/|title=Resultater fra Designa Løbet Silkeborg|last=Askholt|first=Anders|date=15 May 2016|publisher=Feltet.dk|language=Danish|accessdate=21 June 2016}}</ref>
* Number 2 in Post Cup on [[Bornholm]], 5 June 2016.<ref name="bornholm2016">{{cite web|url=http://www.feltet.dk/nyheder/overraskende_andenplads_jeg_er_stolt/#fbBFez8LCYdXXq8X.97|title=Overraskende andenplads: Jeg er stolt|last=Lillelund|first=Benjamin|date=5 June 2016|publisher=Feltet.dk|language=Danish|accessdate=21 June 2016}}</ref><ref name="tv2bornholm2016">{{cite web|url=http://www.tv2bornholm.dk/%5Cdefault.aspx?newsID=106506|title=Post Cup: Troels Vinther vinder anden afdeling|last=Husted|first=William|date=5 June 2016|publisher=TV2 Bornholm|language=Danish|accessdate=21 June 2016}}</ref>
* Number 4 in Hjørring-Sparekassen Vendsyssel<ref name="Vendsyssel2016">{{cite web|url=http://www.feltet.dk/nyheder/resultater_fra_licens-loeb_i_hjoerring/|title=Resultater fra Sparekassen Vendsyssel Løbet|last=Sørensen|first=Tobias Munck|date=16 April 2016|publisher=Feltet.dk|language=Danish|accessdate=21 June 2016}}</ref>
* Number 5 in Grenaa CC on 8 May 2016<ref name="Grenaa2016">{{cite web|url=http://www.feltet.dk/nyheder/resultater_fra_grenaa_cc/|title=Resultater fra Grenaa CC|last=Penning|first=Lasse Wedege|date=9 May 2016|publisher=Feltet.dk|language=Faroese|accessdate=21 June 2016}}</ref>
* Number 8 in Kolding BC on 19 June 2016<ref name="KoldingBC2016">{{cite web|url=http://www.feltet.dk/nyheder/resultater_fra_kolding_bc_-_guldhammer_sejrede/|title=Resultater fra Kolding BC - Guldhammer sejrede|last=Johannesen|first=Jesper|date=20 June 2016|publisher=Feltet.dk|language=Danish|accessdate=21 June 2016}}</ref>
* Number 16 in HAC-løbet (HAC-race)<ref>[http://www.feltet.dk/nyheder/resultater_fra_hac_loebet_2016/#YmKJ3gQSXgBrtdWX.97 feltet.dk - Resultater fra HAC løbet 2016], 10. april 2016</ref>
* Number 23 in Aalborg Cykle-Ring
* Number 51 in [[GP Herning]], distance: 186.0 km

=== Island Games 2015 ===
Torkil Veyhe won the first Faroese gold medal at the [[2015 Island Games]], when he took gold in the men's individual time trial on 28 June 2015.<ref name="Fyrsta gullið">[http://kvf.fo/greinar/2015/06/28/torkil-vann-fyrsta-oyggjaleikagullid Kvf.fo], Torkil vann fyrsta oyggjaleikagullið.</ref> He also won bronze in the men's road race at the 2015 Island Games.<ref name="KVF2015">[http://kvf.fo/greinar/2015/06/30/torkil-veyhe-bronsu-i-landavegssukkling#.VZ6UPPlViko kvf.fo], Torkil Veyhe bronsu í landavegssúkkling.</ref>
*Gold in the Men's Individual Time Trial, 35 km in the time 47 min 30 sec<ref>[http://jersey2015results.com/competitor.aspx?RegID=32950 jersey2015results.com - Competitor Details, Torkil Veyhe]</ref><ref>[http://www.jersey2015.com/news/cycling-wrap jersey2015.com - Cycling Wrap]</ref>
*Bronze in the Men's Road Race, 112 km with the time 2:51.20,076<ref>[http://www.bbc.com/sport/0/cycling/33327131 www.bbc.com - Island Games 2015: Jersey's Kim Ashton wins road race gold]</ref><ref name="jn2015">[http://jn.fo/torkil+veyhe+vann+bronsu+a+112+km.html jn.fo], Torkil Veyhe vann bronsu á 112 km.</ref>
*Bronze in the Men's Time Trial Team Award.<ref>{{cite web|url=http://jersey2015results.com/Sports/CYCLING/default.aspx?SportID=5&EventID=97|title=Cycling - Men's Time Trial Team Award|date=28 June 2015|publisher=Natwest Island Games|accessdate=14 July 2015}}</ref><ref>[http://www.feltet.dk/nyheder/odder-rytter_vinder_guld_ved_island_games/ Feltet.dk - Odder-rytter vinder guld ved Island Games], written by Lasse Wedege Penning on 6 July 2015.</ref>

=== 2013 Island Games ===
Torkil Veyhe competed at the [[Cycling at the 2013 Island Games|2013 Island Games]] in [[Bermuda]] where he took two silver medals.
*Silver in the Men's Individual Time Trial<ref>[http://www.natwestislandgames2013results.com/Competitor.aspx?RegID=27510 natwestislandgames2013results.com - Competitor Details - Torkil Veyhe]</ref>
*Silver in the Men's Road Race

=== 2009 Island Games ===
*Silver in Men's Team Road Race, together with Eli Christiansen, Gunnar Dahl-Olsen, Bogi Kristiansen, Sigmund Olsson.<ref>[http://www.alandresults2009.com/Competitor.aspx?RegID=8264 alandresults2009.com - Competitor Details]</ref>

=== Winner of Tour of Faroe Islands ===
Torkil Veyhe has won the main Faroese cycling event, the annual [[Tour of Faroe Islands]] (Kring Føroyar) five times: 2009, 2010, 2012, 2014 and 2015. Kring Føroyar is for sponsor reasons called Volvo Kring Føroyar since 2014, before that it was called Effo Kring Føroyar and Statoil Kring Føroyar respectively.<ref name="Sudurras2014">{{cite web|url=http://sudurras.fo/?p=37070|title=Volvo Kring Føroyar 2014|date=17 July 2014|publisher=Sudurras.fo|language=Faroese|accessdate=9 July 2015}}</ref><ref>{{cite web|url=http://www.in.fo/news-detail/news/sukklukapping-eitur-nu-volvo-kring-foeroyar/|title=www.in.fo|work=in.fo|accessdate=14 July 2015}}</ref>

=== Island Games 2007 ===
Torkil Veyhe competed at several events at the [[2007 Island Games]] in [[Rhodes]]. His best result was silver in the Men's Team Road Race. He was number 8 in the Men's Individual Time Trial.<ref>[http://rhodesresults2007.com/Competitor.aspx?RegID=8264 rhodesresults2007.com]</ref>
*Silver in the Men's Team Road Race

== References ==
{{Reflist|2}}

== External links ==
*[http://www.feltet.dk/rdb/ryttere/torkil_veyhe/#zKYz1DmK0ckfdelH.97 Torkil Veyhe on feltet.dk]

{{DEFAULTSORT:Veyhe, Torkil}}

[[Category:1990 births]]
[[Category:Living people]]
[[Category:Faroese male cyclists]]
[[Category:People from Tórshavn]]