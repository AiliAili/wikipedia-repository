{{Orphan|date=April 2015}}
{{Infobox plant disease
| color            = 
| name             = Bur oak blight
| image            = 
| caption          = 
| common_names     = BOB
| causal_agents    = ''Tubakia iowensis''
| hosts            = [[bur oak]]s  (''Quercus macrocarpa'')
| vectors          = 
| EPPO_code        = 
| second_EPPO_code = 
| distribution     = 
| treatment        = 
}}
{{Taxobox
| name = 
| regnum = [[Fungus|Fungi]]
| divisio = [[Ascomycota]]
| subdivisio = [[Pezizomycotina]]
| classis = [[Sordariomycetes]]
| subclassis = [[Sordariomycetidae]]
| ordo = [[Diaporthales]]
| familia = unknown
| genus = [[Tubakia]]
| species = '''''T. iowensis'''''
| binomial = ''Tubakia iowensis''
| binomial_authority = (T.C. Harr & D. McNew)
}}
'''Bur oak blight''' (BOB) is a [[fungus|fungal]] [[disease]] that is relatively new to the plant pathogen landscape. BOB started to appear in Midwestern states in the 1990s. The first few diagnoses pointed to a common fungus, ''Tubakia dryina'', as the culprit. However upon further research BOB was said to be caused by a new unnamed species of ''Tubakia'', later named ‘’Tubakia iowensis T.C. Harr. & D. McNew sp. nov. ‘’  BOB has severe symptoms and is a serious new problem amidst the plant pathology world.

==Hosts and symptoms==
The host for this disease is the [[bur oak]], ''Quercus macrocarpa''.  Research shows that ''Q. macrocarpa var. olivaeformis'' tends to be the most susceptible to the pathogen, but the more common and widespread ''Q. macrocarpa var. macrocarpa'' has also been affected by BOB.  ''Q. macrocarpa var. olivaeformis'' has a range centered in the state of Iowa and is characterized by acorns that are olive shaped and smaller than the acorns of other susceptible varieties.<ref>Deitschmann GH. 1965. Bur oak (Quercus macrocarpa Michx.). In: Fowells HA, comp. 1965. Silvics of forest trees of the United States. USDA, Handbook 271. Washington DC. p 563–568.</ref>  The range of BOB is centered in the state of Iowa, however, it has also recently been confirmed in southern Minnesota.

When affected by the disease, the oak develops vein necrosis confined to the leaf midvein and major lateral veins, eventually causing leaf death. Symptoms typically start in the lower crown of the tree and eventually spread to the entire tree.<ref>Engelbrecht, C., and A. Flickinger. "What’s happening to Iowa’s bur oaks." Iowa State University Extension, Hort & Home Pest News IC-497 (21) 22 (2007): 8-22.</ref>  If this disease persists for many years, there can be lasting effects. One example is the possible loss of food reserves. This can leave the tree susceptible to greater insect predation, secondary infections from other pathogens, and death. A distinguishing characteristic of ''T. iowensis'' is the formation of black crustose asexual fruiting bodies at the base of the petioles that are retained until the following year. These distinctive fruiting bodies are pycnothyria (conidiomata) with radiating setae-like hyphae that form along the necrotic veins.

==Disease cycle==
For the purposes of this description of BOB the disease cycle will start in the springtime. In the spring, black crustose conidiomata (pychnothyria) that have served as the overwintering mechanism of the disease release conidia. These spores are dispersed in a number of ways, most commonly in the presence of moisture, be it heavy rain or morning dew. Conidia are also easily wind dispersed. The spores land on healthy leaves and the process of infection begins. Symptoms don’t start to appear on the leaves until late in the summer months, July and August. As autumn approaches BOB forms pychnothyria along leaf veins and on the base of the leaf petioles that remain on the tree until spring. The petiole forms an abscission layer but pustules under the epidermis form possibly causing leaf retention.  This prepares the disease for winter and the cycle repeats. It should be noted that secondary infection could occur during abnormally wet periods during the summer giving this pathogen’s polycyclic potential. A polycyclic pathogen is one that is able to go through several infection cycles in one season.<ref>Sinclair WA, Lyon HH. 2005. Diseases of trees and shrubs. 2nd ed. Ithaca, New York: Cornell Univ. Press.</ref>

==Environment==
According to the National Oceanic and Atmospheric Administration, or NOAA, over the previous two decades the state of Iowa has received more early season rainfall than in years past.<ref>Harrington, T. C., et al. (2012). "Bur oak blight, a new disease on Quercus macrocarpa caused by Tubakia iowensis sp nov." Mycologia 104(1): 79-92.</ref>   This increase in moisture may be what is leading to a more favorable environment for BOB to flourish in. It is unknown if the observed disease is caused by a new pathogen or if the conditions are simply more favorable for the existing pathogen to thrive.  Several changes have been documented in the Iowa annual cycles including warmer nighttime temperatures, more humidity, more spring and summer rainfall, and a shift from the normal late season precipitation.<ref>Takle ES. 2011. Climate changes in Iowa. In: Iowa Climate Change Impacts Committee, eds. 2011, Climate change impacts on Iowa, 2010. Des Moines, Iowa: Iowa Press. p 8–13.</ref>  In the latter part of April and into May rains help BOB during its most critical period by slashing conidia from the crustose pychnothyria on the upper surface of the leaf. The amount of rainfall during this critical time of year has been higher than the normal averages since 1895.<ref>Harrington, T. C., et al. (2012). "Bur oak blight, a new disease on Quercus macrocarpa caused by Tubakia iowensis sp nov." Mycologia 104(1): 79-92.</ref>  They also report that there have been no droughts in Iowa from 1989 -2011.  This increase in moisture is thought to be responsible for the buildup of primary inoculum.  Repeated defoliations due to high concentrations of BOB conidia will eventually lead to a depletion of the bur oak’s food reserves and ultimately cause death.<ref>Harrington, T. C., et al. (2012). "Bur oak blight, a new disease on Quercus macrocarpa caused by Tubakia iowensis sp nov." Mycologia 104(1): 79-92.</ref>

==Management==
When left untreated, BOB will often kill the infected tree. Management is thus an important implication when discussing this disease. Due to the fact that BOB overwinters on leaf petioles that stay on the tree, removing fallen infected leaves will do little in terms of disease control. One proven method of attacking the pathogen is the introduction of a fungicide.  Macro injections of the fungicide Alamo™ (Propiconazole (K)) have been shown to be effective on a two-year rotation.  Dosage should be carefully monitored, as there are some phytotoxic effects.

==Importance==
The impact of BOB reaches many aspects of the regions in which it is found. First is the impact on the wildlife that live around the oaks. Bur oaks, along with other oak species, are prolific acorn producers during mast years. Many birds and mammals use the acorns of the oak as a source of nutrition. Declining bur oak populations could also wreak havoc on wildlife populations that are dependent on acorns for food. These organisms would either have to find another food source, move to a new area, or may perish due to a lack of resources.<ref>Harrington, T. C., et al. (2012). "Bur oak blight, a new disease on Quercus macrocarpa caused by Tubakia iowensis sp nov." Mycologia 104(1): 79-92.</ref>

The loss of these oaks can also cause costly problems in city management. Dead Bur Oak trees pose a problem for the upkeep of urban areas. At $1000/ tree a tree for removal, costs can add up fast. In the state of Iowa, removal can reach over $700,000 trees a year.<ref>5 Miles, P.D. Forest Inventory EVALIDator web-application version 4.01 beta. St.Paul, MN:U.S. Department of Agriculture, Forest
Service, Northern Research Station. December 13, 2012 http://fiatools.fs.fed.us/Evalidator{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }} 4/tmattribute.jsp</ref> With the cost of replacing the trees and other needs, the total cost can reach over $964,548,000. This is obviously a large sum that could hurt the economies of the areas affected.<ref>file:///G:/My%20Documents/My%20Pictures/ETBOB.pdf</ref>

==References==
<references/>

==External links==
* [http://shop.arborjet.com/ProductDetails.asp?ProductCode=0008 Fungicide Info]
* www.iowadnr.gov/portals/idnr/uploads/forestry/forest health/bur oak blight impact.pdf Iowa DNR Bulletin
* [http://www.myminnesotawoods.umn.edu/2010/09/bur-oak-blight-bob-in-minnesota/ Minnesota Pest Bulletin]
* [http://na.fs.fed.us/pubs/palerts/bur_oak_blight/bob_print.pdf  Forest Service Pest Alert]
* [http://www.ipm.iastate.edu/ipm/hortnews/2011/2-9/buroakblight.html Iowa State Article]

[[Category:Parasitic fungi]]
<!-- [[Category:Sordariomycetes]] moved to species Latin name redirect -->
[[Category:Fungal tree pathogens and diseases]]