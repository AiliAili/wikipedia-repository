{{primary sources|date=June 2008}}
{{Infobox publisher
| name         = S. Karger AG
| image        = 
| caption      =
| parent       = 
| status       = 
| traded_as    =
| predecessor  =
| founded      = 1890
| founder      = Samuel Karger
| successor    = 
| country      = [[Switzerland]]
| headquarters = [[Basel]]
| distribution = 
| keypeople    = 
| publications = [[Academic journal]]s, [[book]]s
| topics       = science and medicine
| genre        = 
| imprints     = 
| revenue      = 
| owner        =
| numemployees =
| url          = {{URL|http://www.karger.com}}
}}

'''Karger Publishers''' (also: '''Karger Medical and Scientific Publishers''', '''S. Karger AG''') is an [[academic publishing|academic publisher]] of [[scientific journal|scientific]] and [[medical journal]]s and books. The current CEO is Gabriella Karger.

== History ==
The company was founded in 1890 in [[Berlin]] by [[Samuel Karger]], who remained at the helm of the company until his death in 1935. His son, Heinz Karger led the company until his death in 1959, and the Heinz's son (and Samuel's grandson) Thomas Karger took over the leadership of the company; he was followed as the company leader by his eldest son, Steven Karger and, most recently, by his youngest daughter, Gabriella Karger, who leads the publishing house now.<ref>http://www.karger.com/Company/History</ref> Its first [[medical journal]], ''[[Dermatology (journal)|Dermatologische Zeitschrift]]'' (later: ''Dermatologica'', now: ''Dermatology'') was established in 1893. The company published works from well-known scientists such as [[Sigmund Freud]]. Because of political pressure from the [[Nazism|Nazi]] regime, the company was relocated to [[Basel]], Switzerland, in 1937 and lost all German authors and editors. This led to a more international focus and most journal titles were changed from German to Latin and articles were now published in either [[German language|German]], [[English language|English]], [[French language|French]], or [[Italian language|Italian]].<ref name=history>{{cite web |url=http://content.karger.com/company/history.asp |title=From Turning Medical Progress into Print to Connecting the World of Biomedical Science |format= |work= |publisher=Karger Publishers |accessdate=2010-08-12}}</ref> The company currently publishes over 80 journals.<ref>{{cite web |url=http://content.karger.com/ProdukteDB/produkte.asp?Aktion=JournalIndex&ContentOnly=false |title=Karger Publishers Journal Index |format= |work=karger.com |accessdate=2010-08-12}}</ref>

== See also ==
* [[:Category:Karger academic journals]]

== Further reading ==
* {{cite web |url=http://mag.digitalpc.co.uk/fvx/karger/1890-1990/ |title=Karger: Turning Medical Progress into Print |author=Harold M. Schmeck |format= |work=KargerActiveBooks |accessdate=2010-08-12}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.karger.com/}}

[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1890]]
[[Category:Publishing companies of Switzerland]]
[[Category:1890 establishments in Germany]]
[[Category:Companies based in Basel]]


{{publishing-stub}}