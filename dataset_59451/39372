{{Good Article}}
{{Use dmy dates|date=December 2013}}
{{Use British English|date=December 2013}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Carnarvon.jpg|300px]]
|Ship caption=''Carnarvon'' at anchor
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=HMS ''Carnarvon''
|Ship ordered=
|Ship namesake=[[Caernarfonshire]]
|Ship builder=[[William Beardmore & Company]], [[Dalmuir]]
|Ship laid down=1 October 1902
|Ship launched=7 October 1903
|Ship christened=
|Ship completed=29 May 1905
|Ship commissioned=
|Ship recommissioned=
|Ship decommissioned=
|Ship in service=
|Ship fate=Sold for [[ship breaking|scrap]], 8 November 1921
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass-|Devonshire|cruiser (1903)|0}} [[armoured cruiser]]
|Ship displacement={{convert|10850|LT|t}} (normal)
|Ship length={{convert|473|ft|6|in|m|1|abbr=on}} ([[Length overall|o/a]])
|Ship beam={{convert|68|ft|6|in|m|1|abbr=on}}
|Ship draught={{convert|24|ft|m|1|abbr=on}}
|Ship power=*{{convert|21000|ihp|lk=in|abbr=on}}
*17 [[Niclausse boiler]]s; 6 cylindrical [[boiler]]s
|Ship propulsion=*2 × Shafts
*2 × [[Humphrys, Tennant and Dykes|Humphrys, Tennant and Co.]] 4-cylinder [[triple-expansion steam engine]]s
|Ship speed={{convert|22|kn|lk=in}}
|Ship range=
|Ship complement=610
|Ship armament=*4 × single [[BL 7.5 inch Mk I naval gun|BL 7.5-inch Mk I guns]]
*6 × single [[BL 6 inch Mk VII naval gun|BL 6-inch Mk VII guns]]
*2 × single [[QF 12-pounder 8-cwt Mk I naval gun|12-pounder 8 cwt guns]]<ref group=Note>"Cwt" is the abbreviation for [[hundredweight]], 12 cwt referring to the weight of the gun.</ref>
*18 × single [[QF 3-pounder Hotchkiss|QF 3-pounder]] [[Hotchkiss gun]]s
*2 × single [[British 18 inch torpedo|18-inch]] [[torpedo tube]]s 
|Ship armour=*[[Belt armor|Belt]]: {{convert|2|-|6|in|mm|abbr=on|0}}
*[[Deck (ship)|Deck]]s: {{convert|.75|-|2|in|mm|abbr=on}}
*[[Barbette]]s: {{convert|6|in|mm|abbr=on|0}}
*[[Turret]]s: {{convert|5|in|mm|abbr=on}}
*[[Conning tower]]: {{convert|12|in|mm|abbr=on|0}}
*[[Bulkhead (partition)|Bulkhead]]s: {{convert|5|in|mm|abbr=on|0}} 
}}
|}
'''HMS ''Carnarvon''''' was one of six {{sclass-|Devonshire|cruiser (1903)|0}} [[armoured cruiser]]s built for the [[Royal Navy]] in the first decade of the 20th century. She was assigned to the [[3rd Cruiser Squadron]] of the [[Mediterranean Fleet]] upon completion in 1905 and was transferred to the [[2nd Cruiser Squadron (United Kingdom)|2nd Cruiser Squadron]] of the [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]] in 1907. She was assigned to the [[Reserve fleet|reserve]] Third Fleet in 1909 and became [[flagship]] of the [[5th Cruiser Squadron (United Kingdom)|5th Cruiser Squadron]] of the reserve Second Fleet in 1912.

When World War I began in August 1914, she was assigned to the [[Cape Verde]] Station to search for German [[commerce raider]]s while protecting British shipping. ''Carnarvon'' was transferred to the South Atlantic two months later and assigned to the squadron that destroyed the [[German East Asia Squadron]] at the [[Battle of the Falklands]]. She was assigned to the [[North America and West Indies Station]] in 1915 and continued to patrol against German raiders and escort convoys to the end of the war. In 1919, she became a [[training ship]] and was then sold for [[ship breaking|scrap]] in 1921.

==Design and description==
''Carnarvon'' was designed to [[Displacement (ship)|displace]] {{convert|10850|LT|t}}. The ship had an [[length overall|overall length]] of {{convert|473|ft|6|in|m|1}}, a [[beam (nautical)|beam]] of {{convert|68|ft|6|in|m|1}} and a deep [[draft (ship)|draught]] of {{convert|24|ft|m|1}}. She was powered by two 4-cylinder [[triple-expansion steam engine]]s, each driving one shaft, which produced a total of {{convert|21000|ihp|lk=in}} and gave a maximum speed of {{convert|22|kn|lk=in}}. The engines were powered by seventeen [[Niclausse boiler|Niclausse]] and six cylindrical [[boiler]]s.<ref name=ck8>Chesneau & Kolesnik, p. 71</ref> She carried a maximum of {{convert|1033|LT|t}} of coal and her complement consisted of 610 officers and enlisted men.<ref name=f6>Friedman 2012, p. 336</ref>

Her main armament consisted of four [[List of British ordnance terms#BL|breech-loading (BL)]] [[BL 7.5 inch Mk I naval gun|7.5-inch Mk I guns]] mounted in four single-[[gun turret]]s, one each fore and aft of the superstructure and one [[wing turret|on each side]].<ref name=f56>Friedman 2012, p. 256</ref> The guns fired their {{convert|200|lb|adj=on}} shells to a range of about {{convert|13800|yd}}.<ref>Friedman 2011, pp. 75–76</ref> Her secondary armament of six [[BL 6-inch Mk VII naval gun|BL 6-inch Mk VII guns]] was arranged in [[casemate]]s amidships. Four of these were mounted on the main deck and were only usable in calm weather.<ref>Friedman 2012, pp. 256, 260–61</ref> They had a maximum range of approximately {{convert|12200|yd}} with their {{convert|100|lb|adj=on}} shells.<ref>Friedman 2011, pp. 80–81</ref> ''Carnarvon'' also carried eighteen [[quick-firing gun|quick-firing (QF)]] [[QF 3-pounder Hotchkiss|3-pounder]] [[Hotchkiss gun]]s and two submerged [[British 18 inch torpedo|18-inch]] [[torpedo tube]]s.<ref name=ck8/> Her two [[QF 12-pounder 8-cwt Mk I naval gun|12-pounder 8 cwt guns]] could be dismounted for service ashore.<ref name=f56/>

At some point in the war, the main deck six-inch guns of the ''Devonshire''-class ships were moved to the upper deck and given [[gun shield]]s. Their casemates were plated over to improve [[seakeeping]] and the four 3-pounder guns displaced by the transfer were landed.<ref>Friedman 2012, p. 280</ref>

The ship's [[waterline]] [[Belt armor|armour belt]] ranged from {{convert|2|to|6|in|0|spell=in}} in thickness and was closed off by {{convert|5|in|adj=on|0|spell=in}} transverse [[bulkhead (partition)|bulkhead]]s. The armour of the gun turrets was also five inches thick whilst that of their barbettes was six inches thick. The protective [[deck (ship)|deck]] armour ranged in thickness from {{convert|.75|-|2|in|mm|0}} and the [[conning tower]] was protected by {{convert|12|in|0|spell=in}} of armour.<ref name=ck8/>

==Construction and service==
[[File:HMS H5 through H10 with HMS Carnarvon.gif|300px|thumb|right|The [[drydock]]ed ''Carnarvon'' (at upper left) with the British [[submarines]] {{HMS|H5||2}}, {{HMS|H6||2}}, {{HMS|H7||2}}, {{HMS|H8||2}}, {{HMS|H9||2}}, and {{HMS|H10||2}}  in 1915]]

''Carnarvon'', named to commemorate the [[Caernarfonshire|Welsh county]],<ref name=s1>Silverstone, p. 220</ref> was [[Keel|laid down]] by [[William Beardmore & Company]] at their [[Dalmuir]] shipyard on 1 October 1902 and [[Ship naming and launching|launched]] on 7 October 1903. She was completed on 29 May 1905<ref name=ck8/> and was initially assigned to the 3rd Cruiser Squadron of the Mediterranean Fleet. She was transferred to the 2nd Cruiser Squadron of the Atlantic Fleet in June 1907 and was then assigned to the reserve Third Fleet at [[HMNB Devonport|Devonport]] in April 1909. The ship was transferred to the Second Fleet at Devonport in March 1912 and subsequently became the flagship of the 5th Cruiser Squadron until the start of World War I.<ref name=gg3>Gardiner & Gray, p. 13</ref> She participated in the fleet manoeuvres in July–August 1913 as well as those in July 1914. On 31 July, a few days before war was declared on Germany, she encountered the German [[light cruiser]] {{SMS|Strassburg||2}} in the [[English Channel]] returning home and the two ships saluted each other.<ref name=tr>Transcript</ref>

When the war began on 5 August, ''Carnarvon'', now the flagship of [[Rear Admiral]] [[Archibald Stoddart]], was in [[Gibraltar]]. She was quickly sent to Cape Verde and captured the German merchant ship {{SS|Professor Woermann}} on 23 August 1914. She escorted her prize to [[Freetown]], Sierra Leone for disposal and resumed patrolling. She moved to the Brazilian coast in October<ref name=tr/> and then proceeded to the [[Falkland Islands]] with the squadron commanded by [[Vice-Admiral]] [[Doveton Sturdee]].<ref>Massie, pp. 244, 249</ref>

===Battle of the Falklands===
{{main|Battle of the Falklands}}
Upon arrival at [[Port Stanley]] on 7 December, Sturdee informed his captains that he planned to recoal the entire squadron the following day from the two available [[collier (ship type)|collier]]s and to begin the search for the East Asia Squadron, believed to be running for home around the tip of South America, the day after. Vice-Admiral [[Maximilian von Spee]], commander of the German squadron, had other plans and intended to destroy the radio station at Port Stanley on the morning of 8 December. The appearance of two German ships at 07:30 caught Sturdee's ships by surprise, but the Germans were driven off by {{convert|12|in|adj=on}} shells fired by the [[predreadnought battleship]] {{HMS|Canopus|1897|2}} when they came within range around 09:20. ''Carnarvon'' completed recoaling at 08:00 and the squadron cleared the harbour by 10:30. Sturdee ordered "general chase" at that time, but ''Carnarvon'' could only manage {{convert|18|kn}} and fell behind the other British ships. His two [[battlecruiser]]s were the fastest ships present and inexorably began to close on the German cruisers, opening fire at 12:55 that straddled the light cruiser {{SMS|Leipzig||2}}, the rear ship in the German formation. It was clear to Spee that his ships could not outrun the battlecruisers and that the only hope for any of his ships to survive was to scatter. So he turned his two armoured cruisers around to buy time by engaging the battlecruisers and ordered his three light cruisers to disperse at 13:20. ''Carnarvon'', now {{convert|10|nmi}} behind, had no hope of catching the scattering German ships and continued to trail the battlecruisers.<ref>Massie, pp. 258–65</ref>

''Carnarvon ''finally came within range of the German armoured cruisers and opened fire shortly before {{SMS|Scharnhorst||2}} rolled over and capsized at 16:17. She then engaged {{SMS|Gneisenau||2}} until Sturdee ordered "cease fire" at 17:50. The German captain had started to scuttle his ship 10 minutes earlier when it was clear that the situation was hopeless and his ship sank at 18:00. ''Carnarvon'' rescued 20 survivors from ''Gneisenau'', but only wreckage was visible when she later steamed through the area where ''Scharnhorst'' had sunk.<ref>Massie, pp. 270–73</ref>

After the battle she participated in the hunt for the light cruiser {{SMS|Dresden||2}} that had escaped during the battle and investigated anchorages in Argentina, Chile and the island of [[South Georgia Island|South Georgia]] before proceeding north to Brazil in February. She struck a [[coral reef]] off the [[Abrolhos Archipelago]] on 22 February 1915 and had to be [[Beaching (nautical)|beached]] to avoid sinking. The ship received temporary repairs at [[Rio de Janeiro]] the following month.<ref name=tr/> ''Carnarvon'' received permanent repairs in [[Montreal]], Canada, from May to July after which she escorted several [[British H-class submarine]]s from Halifax to the United Kingdom en route to Devonport. She then returned to Halifax where she was based for the rest of the year.<ref name=tr/> Now assigned to the North America and West Indies Station, she resumed her duties protecting British shipping for the rest of the war.<ref name=gg3/> In 1919, she began serving as a cadet training ship, remaining in that role until she was listed for sale in March 1921.<ref name=gg3/> ''Carnarvon'' was sold for scrap on 8 November 1921 and subsequently broken up in Germany.<ref name=s1/>

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{reflist|2}}

==Bibliography==
* {{cite book|title=Conway's All the World's Fighting Ships 1860–1905|editor1-last=Chesneau|editor1-first=Roger|editor2-last=Kolesnik|editor2-first=Eugene M.|publisher=Conway Maritime Press|location=Greenwich|year=1979|isbn=0-8317-0302-4|last-author-amp=yes}}
*{{cite book|last=Friedman|first=Norman|title=British Cruisers of the Victorian Era|year=2012|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|isbn=978-1-59114-068-9}}
* {{cite book|last=Friedman|first=Norman|title=Naval Weapons of World War One|publisher=Seaforth|location=Barnsley, South Yorkshire, UK|year=2011|isbn=978-1-84832-100-7}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5|last-author-amp=yes}}
* {{cite book|title=[[Castles of Steel: Britain, Germany, and the Winning of the Great War at Sea]] |last=Massie|first=Robert K.|authorlink=Robert K. Massie|publisher=Random House|year=2003|location= New York|isbn=0-679-45671-6}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
*{{cite web|url=http://www.naval-history.net/OWShips-WW1-05-HMS_Carnarvon.htm|title=Transcript: HMS CARNARVON – July 1913 to December 1915, British Waters, Central and South Atlantic, Battle of the Falklands, North Atlantic (Part 1 of 2)|work=Royal Navy Log Books of the World War 1 Era|publisher=Naval-History.net|accessdate=21 May 2014}}

==Further reading==
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations to the Battle of the Falklands|year=n.d.|edition=2nd, reprint of the 1938|series=History of the Great War: Based on Official Documents|volume=I|publisher=Imperial War Museum and Battery Press|location=London and Nashville, Tennessee|isbn=0-89839-256-X}}
*{{cite book|last=Corbett|first=Julian|title=Naval Operations|edition=2nd, reprint of the 1929|series=History of the Great War: Based on Official Documents|volume=II|year=1997|publisher=Imperial War Museum in association with the Battery Press|location=London and Nashville, Tennessee|isbn=1-870423-74-7}}

==External links==
* [http://www.vlib.us/medical/hanks/ HMS ''Carnarvon'' Memoir 1914–1915] – annotated transcript of a diary kept by George H. J. Hanks, a sick bay attendant.

{{Devonshire class cruiser (1903)}}

{{DEFAULTSORT:Carnarvon (1903)}}
[[Category:Devonshire-class cruisers (1903)]]
[[Category:Clyde-built ships]]
[[Category:1903 ships]]
[[Category:World War I cruisers of the United Kingdom]]