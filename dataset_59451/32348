{{featured article}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name= A-4G Skyhawk
 |image= File:A4 lands on melbourne.jpg
 |caption= An A-4G landing on HMAS ''Melbourne'' in 1980
 |alt= Colour photograph of a grey military aircraft on the deck of an aircraft carrier
}}{{Infobox aircraft type
 |type= [[Interceptor aircraft|Fleet air defence]]<br>[[Attack aircraft|Light attack]]
 |national origin= United States
 |manufacturer= [[McDonnell Douglas]]
 |introduced= 1967 <!--Date the aircraft entered or will enter military or revenue service-->
 |retired= 1991 <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
 |primary user= [[Royal Australian Navy]]
 |more users= [[Royal New Zealand Air Force]]
 |produced= 1967, 1970–71
 |number built= 20
 |developed from= [[Douglas A-4 Skyhawk]]
}}
|}

The '''McDonnell Douglas A-4G Skyhawk''' is a variant of the [[Douglas A-4 Skyhawk]] attack aircraft developed for the [[Royal Australian Navy]] (RAN). The model was based on the A-4F variant of the Skyhawk, and was fitted with slightly different avionics as well as the capacity to operate [[AIM-9 Sidewinder]] air-to-air missiles. The RAN received ten A-4Gs in 1967 and another ten in 1971, and operated the type from 1967 to 1984.

In Australian service the A-4Gs formed part of the air group of the aircraft carrier {{HMAS|Melbourne|R21|6}}, and were primarily used to provide air defence for the fleet.  They took part in exercises throughout the Pacific region and also supported the training of RAN warships as well as other elements of the Australian military. The Skyhawks did not see combat, and a planned deployment of some of their pilots to fight in the [[Vietnam War]] was cancelled before it took place. Ten A-4Gs were destroyed as a result of equipment failures and non-combat crashes during the type's service with the Navy, causing the deaths of two pilots.

The RAN had no need for most of its fixed-wing aircraft after ''Melbourne'' was decommissioned in 1982, and the ten remaining A-4Gs were sold to the [[Royal New Zealand Air Force]] (RNZAF) in 1984 where they were initially used for training purposes. Between 1986 and 1991 these aircraft were upgraded and re-designated A-4Ks. Two of the former A-4Gs crashed during 2001, resulting in the death of a pilot. The RNZAF's Skyhawks were retired in 2001. Eight A-4Ks, including six former A-4Gs, were sold to [[Draken International]] in 2012 and are in service supporting United States military training exercises.

==Acquisition==

During the late 1950s the Australian Government and Royal Australian Navy (RAN) considered options to replace the aircraft carrier HMAS ''Melbourne'', and her air group. While ''Melbourne'' had only been commissioned in 1955, the [[de Havilland Sea Venom]] fighters and [[Fairey Gannet]] maritime patrol aircraft operated by the [[Fleet Air Arm (RAN)|Fleet Air Arm]] (FAA) were becoming obsolete. It was believed that ''Melbourne'' was too small to operate more modern aircraft types, and the RAN investigated options to buy a larger carrier. The Government judged that the cost of a new aircraft carrier was too high, especially given the expense of the [[Australian Army]] and [[Royal Australian Air Force|Royal Australian Air Force's]] (RAAF's) procurement programs at that time, and in November 1959 it was announced that the FAA would cease to operate fixed-wing aircraft in 1963.{{sfn|Cooper|2006|p=187}}

As a result of intervention by the Minister for the Navy, Senator [[John Gorton]], the Government eventually agreed to purchase new fixed-wing aircraft. Gorton had served as a fighter pilot in [[World War II]] and had a strong interest in his portfolio. In 1961 Gorton convinced Cabinet to fund a program to reinvigorate the FAA, starting with the purchase of 27 [[Westland Wessex]] anti-submarine helicopters.{{sfn|Cooper|2006|pp=192–193}} At this time it was planned to retain ''Melbourne'' as a [[helicopter carrier]], but in mid-1963 the Government gave the Navy permission to retain the Sea Venoms and Gannets in service until at least 1967.{{sfn|Gillett|1988|p=85}} Minister for Defence Senator [[Shane Paltridge]] rejected a proposal from the Navy to purchase an [[Essex-class aircraft carrier|''Essex''-class aircraft carrier]] from the [[United States Navy]] in June 1964, and the next month ''Melbourne'' undertook flight trials with A-4 Skyhawks and [[Grumman S-2 Tracker]]s during a visit to the major American base at [[Subic Bay]] in the Philippines.{{sfn|Cooper|2006|p=193}} The Skyhawk was a particularly light and compact attack plane, with a wing small enough to not require a folding mechanism.{{sfn|Gunston|1977|p=84}} American Trackers had previously flown off the carrier during exercises in 1957, and the [[Royal Canadian Navy]] had successfully trialled Skyhawks from ''Melbourne''{{'s}} sister ship {{HMCS|Bonaventure|CVL 22|6}}. The trials conducted at Subic Bay went well, and confirmed that ''Melbourne'' would need only minor modifications to safely operate both types of aircraft.{{sfn|Cooper|2006|p=193}}

In late 1964 the RAN sought the Government's approval to upgrade ''Melbourne'' and purchase a force of 18 Skyhawks and 16 Trackers. The Skyhawks were intended to be used to provide air defence for the fleet as well as to attack warships and targets on land. While the Naval Board{{refn | group = Note |The Naval Board was the committee of senior officers responsible for administering the Royal Australian Navy.<ref>{{cite web|title=Land, sea and air forces|url=http://www.naa.gov.au/collection/explore/defence/services.aspx|work=Popular research topics|publisher=National Archives of Australia|accessdate=15 May 2013}}</ref>}} saw maritime strike as being a logical task for the FAA, the RAAF argued that the 24 [[General Dynamics F-111C]] aircraft it had ordered would be more effective in this role. Cabinet agreed to the proposal to modernise the carrier and acquire Trackers in November 1964, but delayed a decision on the Skyhawks at that time.{{sfn|Cooper|2006|p=193}} Following further lobbying and staff work by the Navy, the Government eventually agreed to purchase ten Skyhawks in early 1965 for a cost of £9.2 million.{{sfn|Cooper|2006|p=194}} This order comprised eight single-seat fighter aircraft and a pair of two-seat TA-4 Skyhawk trainers.{{sfn|Wilson|1993|p=151}} These aircraft were the first newly built Skyhawks to be sold to a country other than the United States.{{sfn|Winchester|2005|p=130}}{{refn | group = Note | Argentina became the first export customer for the Skyhawk in 1965, but its 50 aircraft were A-4Bs originally built for the US military.{{sfn|Winchester|2005|pp=65, 119}}}}
[[File:RAN McDonnell Douglas A-4G Skyhawk taxiing during the Fleet Air Arm 21st Anniversary of HMAS Albatross at Nowra October 1969.jpg|thumb|alt=Colour photograph of a white military jet fighter taxiing along a runway|Skyhawk 886 at HMAS ''Albatross'' in 1969]]

The Australian Skyhawks, which were designated the A-4G, were a variant of the A-4F Skyhawk. The A-4F was the final single-seat version of the Skyhawk to be designed specifically for the [[US Navy]], and first flew in 1966; 164 were eventually delivered.{{sfn|Wilson|1993|pp=140–141}} This variant had a more powerful engine than that fitted to earlier Skyhawks, as well as better protection against ground fire and a distinctive "hump" containing avionics, which earned it the nickname "Camel".{{sfn|Wilson|1993|pp=140–141}}{{sfn|Gunston|1977|p=53}} The main differences between the F and G variants were that the latter did not have an avionics hump and was fitted to operate AIM-9B Sidewinder air-to-air missiles.{{sfn|Wilson|1993|pp=140–141}} The A-4Gs also lacked the A-4F's ability to operate guided air-to-ground weapons.{{sfn|Winchester|2005|p=56}} The two-seat TA-4G trainers were fitted with the same avionics and weapons as the single-seat aircraft, but were unable to be operated from ''Melbourne'' as their flight characteristics meant that they could not safely take off from the ship in the event of a "[[Bolter (aviation)|bolter]]" landing.{{sfn|Wilson|1993|p=151}}{{sfn|Winchester|2005|p=56}}

The ten Skyhawks were delivered to the RAN during 1967. The first A-4G test flight took place on 19 July that year, and the initial TA-4G first flew two days later.{{sfn|Winchester|2005|p=56}} On 26 July an A-4G and a TA-4G were handed over to the RAN at a ceremony conducted at McDonnell Douglas' factory at [[Long Beach, California]]. In October that year, ''Melbourne'' sailed to the United States to pick up the Skyhawks and Trackers, with the A-4s being embarked at [[Naval Air Station North Island]] in [[San Diego]]. The carrier transported these aircraft back to [[Jervis Bay]], New South Wales, from where they were unloaded and moved by road to the Navy's air station at [[HMAS Albatross (air station)|HMAS ''Albatross'']] near [[Nowra, New South Wales|Nowra]]. After completing this task ''Melbourne'' proceeded to Sydney to begin a refit that would prepare her to operate the new air group.{{sfn|Wilson|1993|p=151}}

A further ten A-4Gs were purchased in 1969. The aircraft were funded by cancelling plans to expand the Navy's force of [[Oberon-class submarine|''Oberon'' class submarines]] from six to eight boats; this change was justified on the grounds that it would improve the effectiveness of the carrier and expand the FAA's strike capabilities.{{sfn|Cooper|2006|p=194}} As with the first order, this purchase comprised eight A-4Gs and two TA-4Gs. The Skyhawks were former US Navy A-4Fs, and were modified to A-4G standard before being delivered to Australia. These aircraft were collected from San Diego by {{HMAS|Sydney|R17|6}} in July 1971, which delivered them to Jervis Bay the next month.{{sfn|Wilson|1993|p=151}}<ref>{{cite web|title=HMAS Sydney (III)|url=http://www.navy.gov.au/hmas-sydney-iii|publisher=Royal Australian Navy|accessdate=7 April 2013}}</ref>

The Australian Skyhawks retained their US Navy [[serial numbers]], but were also allocated shorter "buzz" numbers painted near their nose. The first batch of A-4Gs were allocated buzz numbers 882 to 889, and the second batch were assigned 870 to 877. The first two TA-4Gs were 880 and 881, and the second pair 878 and 879.{{sfn|Wilson|1993|p=152}}{{sfn|Gillett|1988|p=104}}

==Operational history==

===Royal Australian Navy===
[[File:A-4G VF-085 on USS Kearsarge (CVS-33) 1969.jpg|thumb|A Skyhawk of 805 Squadron operating from {{USS|Kearsarge|CV-33|6}} in 1969]]
RAN Skyhawk operations began in 1968. On 10 January that year [[805 Squadron RAN|805 Squadron]] was recommissioned at HMAS ''Albatross'' to operate the type from ''Melbourne''. Skyhawk flight training started later that month, with six experienced pilots being instructed by a US Navy officer and two other Australian pilots who had previously been posted to the United States to qualify as instructors.{{sfn|Australian Naval Aviation Museum|1998|p=203}} Shortages of spare parts disrupted flying activities for much of the year, and the first course graduated in mid-December 1968 rather than May as had been originally planned; the shortages were largely the result of the US Navy prioritising deliveries to its forces fighting in Vietnam over the needs of the RAN.{{sfn|Grey|1998|p=91}}{{sfn|Australian Naval Aviation Museum|1998|pp=203–204}} The A-4Gs first operated at sea in November 1968, when they landed on board the British carrier {{HMS|Hermes|R12|6}} while she was visiting Australia.{{sfn|Wilson|1993|p=160}}
[[File:Skyhawk launch HMAS Melbourne.jpg|thumb|left|alt=Colour photograph of a white military aircraft having just taken off from the flight deck of an aircraft carrier at sea|A Skyhawk being launched from ''Melbourne'' in 1976]]

The Australian Government considered deploying pilots from 805 Squadron to fight in Vietnam during 1967 and 1968. On 12 May 1967, the Naval Board recommended to the Government that six Skyhawk pilots and their ground crew be offered to the US Navy during May the next year after these personnel had completed their training on the new aircraft. This decision was motivated by a desire to keep the pilots gainfully occupied while ''Melbourne'' completed her refit, and would not involve deploying the A-4Gs as they lacked the weapons systems and avionics needed to counter North Vietnamese air defences.{{sfn|Grey|1998|pp=88–89}} As part of the initial consideration of this option, the [[Minister for Defence (Australia)|Minister for Air]] suggested that the Australian Skyhawks could be sent to [[Ubon Royal Thai Air Force Base|Ubon Air Force Base]] in Thailand to relieve the [[CAC Sabre]]-equipped [[No. 79 Squadron RAAF]] stationed there, but such a deployment was found to be impractical.{{sfn|Grey|1998|p=89}} The Government's Defence Committee{{refn | group = Note |The Defence Committee comprises the secretaries of the [[Department of Defence (Australia)|Department of Defence]] and other government departments with involvement in defence matters, as well as the head of the military, and provides advice to the Minister for Defence.<ref>{{cite web|title=How the Commonwealth has managed the defence of Australia|url=http://www.naa.gov.au/collection/explore/defence/administration.aspx|work=Popular research topics|publisher=National Archives of Australia|accessdate=15 May 2013}}</ref>}} subsequently recommended that FAA fighter pilots be attached to a [[United States Marine Corps]] (USMC) unit stationed at [[Chu Lai Air Base]] and conduct combat missions over South Vietnam. While the USMC Skyhawk units frequently attacked targets in North Vietnam, the FAA aircrew were to be prohibited from joining these operations as the Government had previously decided to restrict Australia's war effort to South Vietnam. This plan was announced by Prime Minister [[Harold Holt]] in October 1967 as part of a package of Australian reinforcements to the war effort in Vietnam.{{sfn|Grey|1998|pp=89–90}} In the event, the deployment did not proceed as the delays to 805 Squadron's initial Skyhawk conversion course meant that the pilots were not ready until after ''Melbourne'' re-entered service. The Navy assessed that it would not be possible to both man a fighter squadron on the carrier and post pilots to Vietnam, and delaying the reactivation of ''Melbourne''{{'}}s air group could lead to criticism of the Skyhawk acquisition. In February 1969 the Minister for Defence directed that no further consideration was to be given to sending naval fighter pilots to Vietnam.{{sfn|Grey|1998|pp=91–92}}

[[724 Squadron RAN|724 Squadron]], the RAN's jet aircraft [[operational conversion unit]], was also equipped with Skyhawks during 1968. By December that year the squadron was operating a mixed fleet of TA-4G Skyhawks, [[de Havilland Vampire|de Havilland Sea Vampire]]s and Sea Venoms from ''Albatross''.<ref name=724_Sqn_History>{{cite web|title=724 Squadron History|url=http://www.navy.gov.au/history/squadron-histories/724-squadron-history|publisher=Royal Australian Navy|accessdate=1 April 2013}}</ref> The Skyhawk conversion courses conducted by this unit lasted for six months, and involved a total of about 110 hours of flying in TA-4Gs and A-4Gs. During this period pilots became familiar with flying the type and practised combat missions as well as [[aerial refueling|air-to-air refuelling]] from other Skyhawks using buddy fuel tanks. They were also required to complete about 100 simulated carrier landings at ''Albatross'' before landing a Skyhawk on board ''Melbourne''.{{sfn|Wilson|1993|pp=161–162}}

After ''Melbourne''{{'}}s refit was completed, the ship put to sea with Skyhawks in 1969. At this time her air group typically comprised four Skyhawks from 805 Squadron, six S-2 Trackers operated by [[816 Squadron RAN|816 Squadron]] and eight of [[817 Squadron RAN|817 Squadron's]] Wessex anti-submarine helicopters. The mix of aircraft carried varied from time to time.{{sfn|Wilson|1993|p=162}} During the late 1960s and 1970s ''Melbourne'' made regular deployments throughout the Pacific region to exercise with the [[Royal Navy]] and US Navy.{{sfn|Wilson|1993|pp=162–165}} In May 1972, 805 Squadron's strength was increased to eight Skyhawks, and it temporarily operated with ten of the type during Exercise Kangaroo in May 1974.{{sfn|Australian Naval Aviation Museum|1998|pp=219, 221}} A-4Gs were embarked on board the carrier when she visited the United Kingdom in 1977 to participate in the [[Fleet review (Commonwealth realms)#Elizabeth II|Jubilee Fleet Review]], and one of the Skyhawks also took part in that year's [[Royal International Air Tattoo]].{{sfn|Winchester|2005|p=56}} ''Melbourne'' underwent major refits between December 1970 and August 1971, mid-1975 to June 1976 and from July 1978 to February 1979. During the periods the carrier was under refit her air group operated from ''Albatross''.{{sfn|Wilson|1993|pp=162–165}} ''Melbourne'' was the smallest carrier to regularly operate Skyhawks.{{sfn|Winchester|2005|p=130}}
[[File:Melbourne-at-anchor-honiara.jpg|thumb|alt=Colour photograph of a grey-painted aircraft carrier at sea. Several white aircraft are parked on its flight deck.|''Melbourne'' in 1980 with four A-4Gs on her flight deck]]

The A-4Gs operated in several roles while embarked on board ''Melbourne'', and also took part in training exercises from air bases around Australia. While their main role was to provide air defence for the carrier and other warships using Sidewinder missiles and cannon, the Skyhawks lacked the manoeuvrability needed to be effective fighters, and had relatively poor performance at high altitude.{{sfn|Wilson|1993|p=167}}{{sfn|Winchester|2005|p=130}} The A-4Gs carried "dumb" bombs and rockets in the ground attack and maritime strike roles as they were unable to operate guided weapons. The aircraft could also be fitted with a D-704 buddy pod aerial refuelling system, which allowed them to refuel from one another in flight, as well as 150 or 300 gallon capacity [[drop tank]]s to extend their range. At this time the A-4Gs were the only Australian military aircraft capable of in-flight refuelling.{{sfn|Winchester|2005|p=131}} The Skyhawks' long range and ability to carry a large weapons load meant that they proved more successful in air-to-ground roles than as air defence fighters.{{sfn|Wilson|1993|p=167}} While 724 Squadron did not deploy on board the carrier, it took part in training exercises with other RAN units as well as elements of the Australian Army.{{sfn|Eather|1995|p=134}} A particularly important task for the squadron was supporting the work-up of RAN warships by acting as targets; in this role Skyhawks were often deployed to [[RAAF Williams|RAAF Base Laverton]] in Victoria and [[RAAF Base Pearce]] in Western Australia.{{sfn|Wilson|1993|pp=167–170}}{{sfn|Gillett|1988|p=104}} 724 Squadron also formed a Skyhawk-equipped [[aerobatics]] team called the Checkmates.{{sfn|Eather|1995|p=134}}

From the mid-1970s the RAN investigated options to replace ''Melbourne'' and her air group. During 1977 the Navy sought tenders for an aircraft carrier capable of operating [[Harrier Jump Jet]]s and helicopters. In February 1982 the Australian Government reached an agreement to purchase {{HMS|Invincible|R05|6}} from Britain the following year. As a result of this deal, ''Melbourne'' was decommissioned on 30 June 1982, followed by 805 and 816 Squadrons on 2 July. All of the remaining ten Skyhawks were assigned to 724 Squadron, which continued to operate from ''Albatross''.{{sfn|Wilson|1993|p=171}} The sale of ''Invincible'' to Australia was cancelled in mid-1982 as a result of the [[Falklands War]], and in March 1983 the new [[Hawke Government]] decided to not replace ''Melbourne''. In May that year the Government also announced that the disbandment of the FAA's fixed-wing force would be brought forward as the RAN no longer needed such aircraft.{{sfn|Jones|2006|pp=227–228}} Six of the A-4Gs were taken out of service on 30 June 1983, and the other four were retained for target towing, radar and weapon calibration duties, and other fleet support tasks. While some consideration was given to using the Skyhawks in the close air support role, it was decided to retire the type. The last Australian A-4G flights took place on 30 June 1984.{{sfn|Wilson|1993|p=172}}

===Royal New Zealand Air Force===

The [[Royal New Zealand Air Force|Royal New Zealand Air Force (RNZAF)]] had purchased a fleet of 14 A-4K Skyhawks in 1970. These aircraft were a variant of the A-4E, and were used mainly in the close air support role. By the early 1980s they were becoming outdated, and lacked the air-to-air and advanced air-to-ground capabilities identified as requirements for the RNZAF in the 1983 ''Defence Review''.{{sfn|Greener|2009|pp=89–90}}{{sfn|McClure|2012|pp=200–201}} Two of the A-4Ks had been destroyed in flying accidents and the remaining twelve aircraft fleet was not considered a viable force.{{sfn|McClure|2012|p=227}}

During the early 1980s the RNZAF considered options to replace or upgrade its A-4Ks. A team of senior RNZAF personnel visited France, the United Kingdom and the United States to assess new aircraft, and the New Zealand Government released a tender in May 1982 seeking proposals to upgrade the Skyhawks.{{sfn|McClure|2012|p=227}} After the decision was made to disband the RAN's fighter force, the Australian Government offered the ten surviving A-4Gs and spare parts for these aircraft to New Zealand for $NZ40 million.{{sfn|McClure|2012|p=227}} The [[Cabinet of New Zealand]] gave the RNZAF authority to begin formal discussions of this deal on 5 September 1983. A team of Air Force personnel inspected the A-4Gs at ''Albatross'' in November that year and judged that they were in generally good condition.{{sfn|Simms|Lee-Frampton|2011|p=124}} The commander of the RNZAF, Air Vice-Marshal [[David Crooks (RNZAF officer)|David Crooks]], favoured purchasing the A-4Gs and then upgrading the entire A-4 fleet over the alternative of buying new aircraft, as this option was the cheapest way to improve the Air Force's capabilities.{{sfn|McClure|2012|p=227}} He recommended to Prime Minister [[Rob Muldoon]] that the deal be taken up, and a decision to acquire the aircraft was made in May 1984.{{sfn|McClure|2012|pp=227–228}} As part of the purchase agreement, the New Zealand Government committed to return one of the Skyhawks to Australia for display after the type was retired from service.<ref name="Skyhawk_returns">{{cite news|title=Skyhawk returns from NZ|url=http://digital.realviewtechnologies.com/default.aspx?xml=defencenews_navy.xml&iid=62063|accessdate=31 March 2013|newspaper=Navy|date=26 April 2012|page=5}}</ref>

The eight A-4Gs and two TA-4Gs were ferried from Nowra to [[RNZAF Base Ohakea]] in three groups from July 1984. While they received minor modifications (at a total cost of $NZ2.7 million) and new serial numbers before entering service with the RNZAF, the aircraft retained the A-4G designation at this time.{{sfn|McClure|2012|p=227}}{{sfn|Wilson|1993|p=174}} All of the A-4Gs were initially assigned to [[No. 2 Squadron RNZAF]], which was reformed on 11 December 1984 as an operational conversion and training unit.{{sfn|McClure|2012|p=228}}{{sfn|Harrison|2001|p=48}} The spare parts sold with the A-4Gs greatly increased the RNZAF's stocks, and were calculated to be worth about $NZ30 million at commercial rates.{{sfn|Simms|Lee-Frampton|2011|pp=126–127}}

In order to address the shortcomings of the RNZAF's Skyhawk fleet, the New Zealand Government approved the [[Project Kahu]] program in May 1985.{{sfn|Simms|Lee-Frampton|2011|p=128}} This project had a total cost of NZ$140 million, and primarily involved upgrading the A-4G and A-4Ks' avionics to almost the standard of those fitted to the modern [[General Dynamics F-16 Fighting Falcon|General Dynamics F-16C Fighting Falcon]].{{sfn|Greener|2009|pp=90–91}} The Skyhawks also received new wings drawn from the stock of spare parts purchased from the RAN in 1984.{{sfn|Wilson|1993|p=176}} Work began on the first aircraft in 1986, and the program was completed in 1991.{{sfn|Winchester|2005|p=138}} After receiving the Kahu upgrade package the A-4Gs were re-designated A-4Ks.{{sfn|Wilson|1993|p=174}}

From 1990 until 2001, No. 2 Squadron was stationed at HMAS ''Albatross'', where it operated four A-4Ks and two TA-4Ks and trained alongside the [[Australian Defence Force]]; several former Australian aircraft were among those serving with No. 2 Squadron during this period.{{sfn|Wilson|1993|p=176}}{{sfn|McClure|2012|pp=241–242}} The RNZAF continued to operate Skyhawks until 13 December 2001, when both of its fighter squadrons were disbanded.{{sfn|Greener|2009|p=104}} In 2012 eight of the A-4Ks were sold to [[Draken International]] to provide training support for the US military; six of these aircraft were former A-4Gs.<ref>{{cite news|last=Cowlishaw|first=Shane|title=New life for RNZAF jets|url=http://www.stuff.co.nz/dominion-post/news/8526152/New-life-for-RNZAF-jets|accessdate=5 May 2013|newspaper=The Dominion Post|date=9 April 2013}}</ref>{{sfn|Simms|Lee-Frampton|2011|p=361}}<ref>{{cite web|title=Draken International|url=http://registry.faa.gov/aircraftinquiry/Name_Results.aspx?Nametxt=DRAKEN+INTERNATIONAL&sort_option=1&PageNo=1|work=FAA Registry|publisher=Federal Aviation Administration|accessdate=13 July 2013}}</ref> The remaining nine A-4Ks were donated to museums.<ref>{{cite news|last=Davison|first=Isaac|title=Skyhawks finally heading overseas|url=http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=10825898|accessdate=5 May 2013|newspaper=The New Zealand Herald|date=10 August 2012}}</ref>

==Aircraft on display==
[[File:McDonnell Douglas A-4G Skyhawk 880 at the Fleet Air Arm Museum in February 2015.jpg|thumb|alt=Colour photograph of a military fighter jet aircraft painted in blue and grey camouflage in a museum other aircraft and engines are visible.|A-4G 880 on display at the Fleet Air Arm Museum in 2015]]
In April 2011, the New Zealand Government returned TA-4K N13-154911 (880 in RAN service and NZ6255 in RNZAF service) to Australia. The Skyhawk had served with 724 Squadron from 1967 until 1984, and was flown back to Australia in April 2012 within one of the RAAF's [[Boeing C-17 Globemaster III in Australian service|Boeing C-17 Globemaster III]] transports.<ref>{{cite news|title=Nine Skyhawks heading to museums|url=http://www.nzherald.co.nz/nz/news/article.cfm?c_id=1&objectid=10717541|accessdate=29 March 2013|newspaper=The New Zealand Herald|date=6 April 2011}}</ref><ref name=Skyhawk_returns_home>{{cite news|title=Skyhawk returns home 18 years later|url=http://australianaviation.com.au/2012/04/skyhawk-returns-home-18-years-later/|accessdate=29 March 2013|newspaper=Australian Aviation|date=24 April 2012}}</ref><ref>{{cite web|title=Old friend helps Navy remember its fixed-wing history|url=http://news.navy.gov.au/en/Sep2013/Events/429/Old-friend-helps-Navy-remember-its-fixed-wing-history.htm|publisher=Royal Australian Navy|date=20 September 2013|accessdate=11 August 2014}}</ref> This aircraft is currently displayed at the RAN's [[Fleet Air Arm Museum (Australia)|Fleet Air Arm Museum]]. Since November 1999 a former American A-4B (142871) was first displayed as A-4G 885 (154906) and from 2007 as A-4G 882 (154903) at this museum.{{sfn|Winchester|2005|p=132}}<ref>{{cite web|title=Skyhawks on Display|url=http://a4skyhawk.org/2c/museum.htm|publisher=A4 Skyhawk Association|accessdate=11 August 2014}}
</ref><ref>{{cite web|title=142871 Fleet Air Arm Museum Calum Gibson 640|url=http://a4skyhawk.org/content/142871-fleet-air-arm-museum-calum-gibson-640|publisher=A4 Skyhawk Association|accessdate=11 August 2014}}</ref><ref>{{cite web|title=McDonnell Douglas A4G Skyhawk |url=http://www.navy.gov.au/aircraft/mcdonnell-douglas-a4g-skyhawk|publisher=Royal Australian Navy|accessdate=11 August 2014}}</ref>

==Accidents and incidents==

Ten of the twenty A-4Gs were lost during the type's service with the RAN. This gave the Skyhawk a poor safety record, but the high loss rate was at least partially attributable to the intrinsic dangers involved in operating from an aircraft carrier.{{sfn|Winchester|2005|p=131}}{{sfn|Wilson|1993|p=170}} Two pilots were killed in these accidents.{{sfn|Wilson|1993|p=170}}

[[File:McDonnell Douglas A-4G Skyhawk, Australia - Navy AN2129470.jpg|thumb|left|alt=Colour photograph of a military jet aircraft parked at an airfield|A-4G 870 on display at an airshow in 1977. This aircraft was destroyed as the result of an engine failure in 1979.]]
Four Skyhawks were destroyed in accidents during the mid-1970s. The first A-4G to be lost was 873, which crashed into the sea off [[Williamtown, New South Wales]], on 5 June 1973 due to an engine failure; its pilot ejected and was rescued by a RAAF helicopter. On 8 November that year ''Melbourne'' suffered a catapult failure while launching 889 near Singapore, and the aircraft landed in the sea in front of the carrier. While ''Melbourne'' sailed directly over the downed Skyhawk, its pilot managed to free himself and was picked up by a helicopter. The next loss took place on 16 May 1974 when TA-4G 879 crashed into the sea while conducting a practice attack on ''Melbourne'' off the [[South Coast (New South Wales)|south coast]] of New South Wales, killing the pilot. On 16 May the next year A-4Gs 870 and 872 collided while flying near Jervis Bay; 872 crashed, resulting in the death of its pilot, but 870 managed to return to ''Albatross''.{{sfn|Wilson|1993|p=170}}

The other six A-4G losses occurred during 1979 and 1980. On 23 January 1979, 870 suffered an in-flight turbine failure and crashed near [[Braidwood, New South Wales]]; its pilot was rescued by a RAAF helicopter. On 23 May that year 888 ran off the flight deck of ''Melbourne'' after an [[Arresting gear|arrester wire]] snapped during a landing near Jervis Bay. Its pilot, a US Navy exchange officer, survived. On 24 September 1979, 886 rolled over the side of the carrier while being moved along the flight deck in heavy seas; an FAA ground crewman who was sitting in the cockpit at the time managed to escape after the aircraft hit the water and was rescued by the destroyer {{HMAS|Hobart|D 39|6}}. The next loss occurred on 28 April 1980 when the engine of TA-4G 878 failed while it was preparing to land at HMAS ''Albatross''. The pilot of this aircraft ejected safely and was rescued by a RAN helicopter. On 2 October that year the A-4G 875 crashed into the sea near Sumatra, when its engine failed while being launched from ''Melbourne''; its pilot ejected and was rescued. The final loss occurred on 21 October 1980, when 885 crashed into the sea as a result of a catapult failure; the pilot was picked up by a Wessex.{{sfn|Wilson|1993|p=170}}

Two of the former A-4Gs were also lost during their service with the RNZAF. On 16 February 2001, NZ6211 (882 in Australian service) crashed near Nowra while practising an aerobatic manoeuvre with another Skyhawk; its pilot, who was also the commanding officer of No. 2 Squadron, was killed instantly. The last former A-4G to be lost was TA-4K NZ6256 (the former 881), which crashed into the Indian Ocean off [[Perth, Western Australia]] on 20 March 2001. Its pilot ejected and survived.{{sfn|Harrison|2001|p=58}}<ref>{{cite news|title=Pilot survives Skyhawk crash|url=http://tvnz.co.nz/national-news|accessdate=30 March 2013|newspaper=OneNews|date=20 March 2001}}</ref>

==Operators==
*{{navy|Australia}}
**[[724 Squadron RAN|724 Squadron]]
**[[805 Squadron RAN|805 Squadron]]
*{{air force|New Zealand}}
**[[No. 2 Squadron RNZAF|No. 2 Squadron]]

==Specifications (A-4G Skyhawk)==
[[File:A-4E 3sd NAN8-71.jpg|right|200px]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
| plane or copter? = <!-- options: plane/copter -->
| jet or prop? = <!-- options: jet/prop/both/neither -->
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank.

-->
| ref = Wilson 1993{{sfn|Wilson|1993|p=141}}
<!--
        General characteristics
-->
| crew         = 1
| length main  = 12.27 metres
| length alt   = 40.3 ft
| span main    = 8.38 metres
| span alt     = 27.5 ft
| height main  = 4.62 metres
| height alt   = 15.2 ft
| area main    = 24.15 m²
| area alt     = 260 ft²
| airfoil      = 
| aspect ratio = 
| empty weight main  = 4,581 kilograms
| empty weight alt = 10,100 lb
| max takeoff weight main = {{convert|11113|kg|lb}} (carrier-based operations)
| max takeoff weight alt  = {{convert|12438|kg|lb}} (land-based operations)
| more general            = 
<!--
        Powerplant
-->
|engine (jet)=[[Pratt & Whitney J52]]-P8A
|type of jet=[[turbojet]]
|number of jets=1
|thrust main= 9,300 lbf

<!--
        Performance
-->
| max speed main    = {{convert|1086|km/h|mi/h}} (at sea level)
| cruise speed main = {{convert|788|km/h|mi/h}}
| combat radius main= 644 kilometres
| combat radius alt = 400 mi
| ferry range main  = with external fuel tanks, 2440 kilometres
| ferry range alt =  1,520 mi
| ceiling main      = 14,600 metres
| ceiling alt       = 47,900 ft
| climb rate main   = 1,713 m/minute
| climb rate alt = 5,620 ft/minute

<!--
        Armament
-->
| guns     = 2 x [[20 mm caliber|20 mm (0.79 in)]] [[Colt Mk 12 cannon]], 100 rounds/gun
| bombs    = 
| rockets  = 
| missiles =

| hardpoints = Five
| hardpoint capacity = {{convert|3629|kg|lb}}
| hardpoint rockets  = Up to five [[Zuni (rocket)|127 mm Mk 32 Zuni rockets]] or 2.75 inch FFAR rocket pods
| hardpoint missiles = 2 x [[AIM-9 Sidewinder]]
| hardpoint bombs    = Three clusters of three 250 or 500 lb bombs, five single 250 or 500 lb bombs or three single 1,000 lb bombs
| hardpoint other    =

| avionics =
*AN/APG-53A radar
*ASN-1 Dead Reckoning Navigation Computer
*Bendix Automatic Flight Control System
*[[Identification friend or foe]] system
*[[Tactical air navigation system|Tactical air navigation]] equipment
*Ultra high frequency (UHF) radio
*UHF direction finder
}}

==Notes==
{{reflist|group=Note}}

==References==
{{commons category|A-4G Skyhawk}}
;Citations
{{reflist|25em}}

;Works consulted
*{{cite book |author=Australian Naval Aviation Museum|title=Flying Stations: a story of Australian naval aviation |year=1998 |publisher=Allen & Unwin |location=St Leonards, NSW |isbn=1-86448-846-8 |ref=harv}}
*{{cite book |last=Cooper|first=Alastair|editor=Stevens, David |title=The Royal Australian Navy|chapter=1955–1972: The Era of Forward Defence|series=The Australian Centenary History of Defence|year=2006|origyear=2001|edition=paperback |publisher=Oxford University Press |location=South Melbourne|isbn=0-19-554116-2|ref=harv}}
* {{cite book|last=Eather|first=Steve|title=Flying Squadrons of the Australian Defence Force|publisher=Aerospace Publications|location=Weston Creek, Australian Capital Territory|year=1995|isbn=1-875671-15-3|ref=harv}}
*{{cite book|last=Gillett|first=Ross|title=Wings Across the Sea|year=1988|publisher=Aerospace Publications|location=Sydney|isbn=0-9587978-0-3|ref=harv}}
*{{cite book|last=Greener |first=Peter |title=Timing is Everything: The Politics and Processes of New Zealand Defence Acquisition Decision Making |publisher=ANU E Press |series=Canberra Papers on Strategy and Defence |volume=No. 173 |location=Canberra, ACT |year=2009 |isbn=978-1-921536-65-6 |oclc= |url=http://epress.anu.edu.au/timing_citation.html |accessdate=29 March 2013|ref=harv}}
*{{cite book |last=Grey |first=Jeffrey |title=Up Top: the Royal Australian Navy and Southeast Asian conflicts, 1955–1972 |year=1998 |series=The Official History of Australia's Involvement in Southeast Asian Conflicts 1948–1975 |publisher=Allen & Unwin |location=St. Leonards, NSW |isbn=1-86448-290-7|ref=harv}}
*{{cite book |last=Gunston |first=Bill |authorlink=Bill Gunston|title=Modern Military Aircraft |year=1977 |publisher=Salamander Books |location=London |isbn=0-86101-010-8|ref=harv}}
*{{cite book|last=Harrison|first=Paul|title=Kiwi Air Combat Forces: A Portrait of the Air Combat Forces of the Royal New Zealand Air Force|year=2001|publisher=New Zealand Defence Force|location=Wellington|oclc=58406875|ref=harv}}
*{{cite book |last=Jones|first=Peter|editor=Stevens, David |title=The Royal Australian Navy|chapter=1972–1983 : Towards Self-Reliance|series=The Australian Centenary History of Defence|year=2006|origyear=2001|edition=paperback |publisher=Oxford University Press |location=South Melbourne|isbn=0-19-554116-2|ref=harv}}
*{{cite book|last=McClure|first=Margaret|title=Fighting Spirit: 75 Years of the RNZAF|year=2012|publisher=Random House|location=Auckland|isbn=978-1-86979-610-5|ref=harv}}
*{{cite book|last=Simms|first=Don|last2=Lee-Frampton|first2=Nick|title=Skyhawks: The History of the RNZAF Skyhawk|year=2011|publisher=WilsonScott|location=Christchurch|isbn=9781877427367|ref=harv}}
*{{cite book|last=Wilson|first=Stewart|title=Phantom, Hornet, and Skyhawk in Australian Service|year=1993|publisher=Aerospace Publications|location=Weston Creek, Australian Capital Territory|isbn=1-875671-03-X|ref=harv}}
*{{cite book|last=Winchester|first=Jim|title=A-4 Skyhawk: 'Heinemann's Hot Rod'|year=2005|publisher=Pen & Sword Aviation|location=Barnsley, Yorkshire |isbn=1-84415-085-2|ref=harv}}

{{McDonnell Douglas military aircraft}}

[[Category:McDonnell Douglas aircraft|A-04G Skyhawk]]
[[Category:Single-engined jet aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Carrier-based aircraft]]
[[Category:Douglas A-4 Skyhawk|A-004G]]
[[Category:Australian naval aviation]]