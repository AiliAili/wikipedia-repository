{{Use dmy dates|date=January 2013}}
'''Secular Organizations for Sobriety''' ('''SOS'''), also known as '''Save Our Selves''',<ref name="SOS_INTERNATIONAL">{{cite web | title = SOS International | url=http://www.sossobriety.org/ | accessdate = 9 February 2013 }}</ref> is a [[non-profit]] network of autonomous [[addiction recovery groups]]. The program stresses the need to place the highest priority on sobriety and uses mutual support to assist members in achieving this goal. The Suggested Guidelines for Sobriety emphasize rational decision-making and are not religious or spiritual in nature. SOS represents an alternative to the spiritually based addiction recovery programs such as [[Alcoholics Anonymous]] (AA). SOS members may also attend AA meetings, but SOS does not view [[spirituality]] or surrender to a [[Higher Power]] as being necessary to maintain abstinence.<ref name="CHIAUZZI">{{cite book |last=Chiauzzi |first=Emil J. |title=Time Effective Treatment with CE Test: A Manual for Substance Abuse Professionals |chapter=Chapter 2: Principles of Time-Effective Substance Abuse Treatment and Program Development |pages=9–27 |publisher=Hazelden |isbn=1-59285-046-4 |year=2003}}</ref><ref name="CONNORS">{{cite journal |last=Connors |first=Gerard J. |author2=Dermen, Kurt H.  |title=Characteristics of Participants in Secular Organizations for Sobriety (SOS) |journal=The American Journal of Drug and Alcohol Abuse |volume=22 |issue=2 |year=1996 |pages=281–295 |doi=10.3109/00952999609001659 |pmid=8727060}}</ref>

==History==
James Christopher's [[alcoholism]] began when he was a teenager. He had originally sought help in Alcoholics Anonymous (AA), but was uncomfortable with the emphasis on [[God]] and [[religion]] and he began looking for direction in the writings of [[secular humanists]]. Christopher wrote about his frustrations with AA and his own developing program for recovery. In 1985, ''[[Free Inquiry]]'' published an article "Sobriety Without Superstition" written by Christopher. He received hundreds of letters in response and decided to organize [[secular]], self-help, alcoholism recovery group meetings.<ref name="CONNORS"/> The first such meeting was held in November 1986 in [[North Hollywood, California]],<ref name="CONNORS"/> and meetings continue to this day at the [[Center for Inquiry]] in [[Los Angeles]] and at other locations.{{Citation needed|date=July 2011}} Christopher has been continuously sober since 1978.<ref name="CONNORS"/>

== Processes ==
SOS recognizes genetic and environmental factors contributing to addiction, but allows each member to decide whether or not alcoholism is a disease. SOS holds the view that alcoholics can recover (addictive behaviors can be arrested), but that ultimately it is never cured; relapse is always possible. SOS does not endorse sponsor/sponsee relationships.<ref name="SOS_CFIWEST">{{cite web | title = Secular Organizations for Sobriety Website: What is SOS? | url=http://www.cfiwest.org/sos/intro.htm | accessdate = 1 July 2011 }}</ref>

The SOS program is based on the Suggested Guidelines for Sobriety, that emphasize the "sobriety priority." In order to change, members must make abstinence their top priority: not drinking despite changing conditions in their lives. SOS suggests members follow a daily, three part, Cycle of Sobriety: acknowledgment of their addiction, acceptance of their addictions and prioritization of maintaining sobriety. Members are also encouraged to develop strategies or aphorisms that strengthen their resolve to maintain sobriety.<ref name="SOS_CFIWEST" /> on what SOS teaches

===Suggested Guidelines for Sobriety===
These guidelines are suggested by SOS for maintaining sobriety.<ref name="HORVATH2011">{{cite book |last1=Horvath |first=Arthur T. |editor1-last=Ruiz |editor1-first=Pedro |editor2-last=Strain |editor2-first=Eric C |editor3-last=Lowinson |editor3-first=Joyce H |title=Lowinson and Ruiz's substance abuse |year=2011 |location=Philadelphia |publisher=Wolters Kluwer Health/Lippincott Williams & Wilkins |oclc=665130135 |isbn=1-60547-277-8 |edition=5th |chapter=38: Alternative Support Groups |pages=535–536}}</ref>
*To break the cycle of denial and achieve sobriety, we first acknowledge that we are alcoholics or addicts.
*We reaffirm this daily and accept without reservation the fact that as clean and sober individuals, we can not and do not drink or use, no matter what.
*Since drinking or using is not an option for us, we take whatever steps are necessary to continue our Sobriety Priority lifelong.
*A quality of life, "the good life," can be achieved. However, life is also filled with uncertainties. Therefore, we do not drink or use regardless of feelings, circumstances, or conflicts.
*We share in confidence with each other our thoughts and feelings as sober, clean individuals.
*Sobriety is our Priority, and we are each responsible for our lives and sobriety.

===Meetings===
While each SOS meeting is autonomous, SOS does provide a meeting format. The opening reading for meeting conveners summarizes their program. Following the reading of the opening, typically there are announcements, acknowledgment of members sobriety anniversaries and a reading of the Suggested Guidelines. The suggested opening reads as follows.<ref name="CONNORS"/>

{{Quotation |Welcome to SOS. My name is [leader states his or her first name here] and I have been asked to lead tonight's meeting. Secular Organizations for Sobriety (or Save Our Selves) is dedicated to providing a path to sobriety, an alternative to those paths depending upon supernatural or religious beliefs. We respect diversity, welcome healthy skepticism, and encourage rational thinking as well as the expression of feelings. We each take responsibility for our individual sobriety on a daily basis. This is a sobriety meeting. Our focus is on the priority of abstaining from alcohol and other mind-altering drugs. We respect the anonymity of each person in this room. This is a self-help, nonprofessional group. At this meeting, we share our experiences, understandings, thoughts, and feelings.||}}

==Demographics==
A preliminary survey of SOS members was conducted in 1996. The survey results showed SOS attracted members with secular attitudes; 70% had no current religious affiliation and 70% were self-described atheists or agnostics, while another 22% described themselves as spiritual but non-churchgoers. SOS members were predominantly [[White people|white]] (99%) employed (nearly half were employed full-time) males (1/4 of the respondents were female) over the age of 40. Abstinence was the reported goal of 86% of SOS members; 70% of respondents reported that they were currently abstinent (for an average of 6.3 years), another 16% were "mostly abstinent."  The majority of SOS members planned long-term affiliation with SOS. Nearly every member surveyed had been exposed to AA, the average rate of AA attendance amongst SOS members in the previous month was 4.5 meetings.<ref name="CONNORS"/> A 2007 survey of addiction recovery groups found that [[religiosity]] [[correlation|correlated negatively]] with participation in SOS.<ref name="ATKINS2007">{{cite journal | last = Atkins | first =  Randolph G. |author2=Hawdon, James E.  | title = Religiosity and participation in mutual-aid support groups for addiction |date=October 2007 | journal = Journal of Substance Abuse Treatment | volume = 33 | issue = 3 | pages = 321–331 | url = http://www.journalofsubstanceabusetreatment.com/article/S0740-5472(07)00187-0/abstract | doi = 10.1016/j.jsat.2007.07.001 | pmid = 17889302 | issn = 1873-6483 | oclc = 9965265 | pmc = 2095128}}</ref>

== See also ==
{{Col-begin}}
{{Col-2}}
*[[Addiction recovery groups]]
*[[Alcoholism]]
*[[Cognitive behavior therapy]]
*[[Drug addiction]]
{{Col-2}}
*[[LifeRing Secular Recovery]]
*[[Rational Recovery]]
*[[SMART Recovery]]
*[[Women For Sobriety]]
*[[Moderation Management|Moderation Management (MM)]]
* [[Pagans in Recovery|Pagans in Recovery (PIR)]]
* [[Association of Recovering Motorcyclists|Association of Recovering Motorcyclists (ARM)]]
* [[Celebrate Recovery|Celebrate Recovery (CR)]]
{{Col-end}}

==References==
<references/>

== Literature ==
* {{cite book |last=Christopher |first=James |title=How to stay sober: recovery without religion |publisher=[[Prometheus Books]] |year=1988 |location=Buffalo, NY |isbn=0-87975-438-9 |oclc=17385031}}
* {{cite book |last=Christopher |first=James |title=SOS sobriety: the proven alternative to 12-step programs |publisher=[[Prometheus Books]] |location=[[Buffalo, NY]] |year=1992 |isbn=0-87975-726-4 |oclc=25411883}}
* {{cite book |last=Christopher |first=James |title=Unhooked: staying sober and drug-free |location=[[Buffalo, NY]] |publisher=[[Prometheus Books]] |year=1989 |isbn=0-87975-542-3 |oclc=20171952}}

== Further reading ==
*{{cite book |last1=Uebel |first1=Michael |first2=Clayton |last2=Shorkey |year=2008 |chapter=Secular Organization for Sobriety |title=Encyclopedia of Substance Abuse Prevention, Treatment, and Recovery |editor1-first=Gary L. |editor1-last=Fisher |editor2-first=Nancy A. |editor2-last=Roget |publisher=Sage |location=Thousand Oaks |isbn=1-4129-5084-8 |oclc=185031308}}
*White, William L (2003) [http://www.uis.edu/illaps/wp-content/uploads/sites/173/2015/07/Management_of_the_High_Risk_DUI_Offender2003.pdf Management of the High-Risk DUI Offender], Institute for Legal, Administrative and Policy Studies, University of Illinois at Springfield


==External links==
* [http://www.sossobriety.org/ SOS International]
* [http://www.cfiwest.org/sos/index.htm SOS Resources on The Center for Inquiry site]
* {{worldcat id|id=lccn-n90-621933}}

{{DEFAULTSORT:Secular Organizations For Sobriety}}
[[Category:Addiction and substance abuse organizations]]
[[Category:Alcohol abuse]]
[[Category:Drug rehabilitation]]
[[Category:Non-profit organizations based in California]]
[[Category:Organizations established in 1985]]
[[Category:Personal development]]
[[Category:Positive mental attitude]]
[[Category:Psychosocial rehabilitation]]
[[Category:Self care]]
[[Category:Support groups]]