{{Use dmy dates|date=February 2011}}
{{Infobox album | <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Odyssey Number Five
| Type        = studio
| Artist      = [[Powderfinger]]
| Cover       = Odyssey - 5.jpg
| Released    = 4 September 2000<br />20 March 2001 (U.S. release)
| Recorded    = 1999–2000
| Genre       = [[Alternative rock]]
| Length      = 45:26
| Label       = [[Universal Music]]
| Producer    = [[Nick DiDia]]
| Last album  = ''[[The Triple M Acoustic Sessions]]''<br />(1998)
| This album  = ''Odyssey Number Five''<br />(2000)
| Next album  = ''[[Vulture Street (album)|Vulture Street]]''<br />(2003)
| Misc = 
  {{Singles
  | Name           = Odyssey Number Five
  | Type           = studio
  | single 1       = [[My Kind of Scene]]
  | single 1 date  = June 2000 <!-- On promotional single, check PCentral for confirmation -->
  | single 2       = [[My Happiness (song)|My Happiness]]
  | single 2 date  = 14 August 2000
  | single 3       = [[Like a Dog (song)|Like a Dog]]
  | single 3 date  = 15 January 2001
  | single 4       = [[The Metre/Waiting for the Sun|The Metre<span style="color:black;">"<br />"</span>Waiting for the Sun]]
  | single 4 date  = 27 August 2001
  }}
}}
{{Album ratings
| rev1 = [[Allmusic]]
| rev1Score = {{Rating|1.5|5}} <ref name="amg" />
| rev2 = [[Robert Christgau]]
| rev2Score = {{rating-Christgau|dud}} <ref name="RobertChristgauReview">{{cite web|url=http://www.robertchristgau.com/get_artist.php?id=4719&name=Powderfinger |title=Robert Christgau: CG: Powerderfinger|author=Christgau, Robert|authorlink=Robert Christgau|accessdate=31 January 2008}}</ref>
| rev3 = [[College Music Journal|CMJ]]
| rev3Score = (favourable) <ref name="CollegeMusicJournalReview">{{cite news|title=Powderfinger: Odyssey Number Five|work=[[College Music Journal|CMJ]]|date=26 March 2001|page=12}}</ref>
| rev4 = [[Entertainment Weekly]]
| rev4Score = B+<ref name="ew" />
| rev5 = [[Los Angeles Times]]
| rev5Score = {{Rating|2.5|5}}<ref name="LosAngelesTimesReview">{{cite news|url=http://pqasb.pqarchiver.com/latimes/access/69789134.html?dids=69789134:69789134&FMT=ABS&FMTS=ABS:FT&type=current&date=Mar+18%2C+2001&author=STEVE+APPLEFORD&pub=Los+Angeles+Times&desc=Pop+Music%3B+Record+Rack%3B+In+Brief%3B+**+1%2F2+Powderfinger%2C+%22Odyssey+Number+Five%2C%22+Republic%2FUniversal.&pqatl=google|title=Powderfinger: Odyssey Number Five|author=Appleford, Steve|date=18 March 2001|accessdate=20 November 2009|work= Los Angeles Times}}</ref>
| rev6 = [[New Straits Times]]
| rev6Score = (favourable)<ref name="NewStraitsTimesReview">{{cite web|url=http://www.highbeam.com/doc/1P1-82602342.html|title=Aussies rock with promise|author=Leo, Christie|date=17 June 2001|accessdate=20 November 2009}}</ref>
| rev7 = [[New York Post]]
| rev7Score = (highly favourable)<ref name="NewYorkPostReview">{{cite news|url=http://pqasb.pqarchiver.com/nypost/access/69864560.html?dids=69864560:69864560&FMT=ABS&FMTS=ABS:FT&type=current&date=Mar+20%2C+2001&author=Dan+Aquilante&pub=New+York+Post&desc=NO+SUGAR+COATING+FOR+KILLING+HEIDI%27S+AUSSIE-POP&pqatl=google|title=No Sugar Coating For Killing Heidi's Aussie-Pop|author=Aquilante, Dan|date=20 March 2001|accessdate=20 November 2009 | work=New York Post}}</ref>
| rev8 = [[The New Zealand Herald]]
| rev8Score = {{rating|4|5}}<ref name="TheNewZealandHeraldReview">{{cite news|url=http://www.nzherald.co.nz/music/news/article.cfm?c_id=264&objectid=165336|title=Powderfinger: Odyssey Number Five|author=Baillie, Russell|accessdate=20 November 2009|work=The New Zealand Herald|date=16 December 2000}}</ref>
| rev9 = [[PopMatters]]
| rev9Score = (favourable)<ref name="popmatters" />
| rev10 = [[Q (magazine)|Q]]
| rev10Score = {{rating|3|5}}<ref name="QMagazineReview">{{cite news|title=Powderfinger: Odyssey Number Five|work=[[Q (magazine)|Q]]|date=June 2001|page=113}}</ref>
}}

'''''Odyssey Number Five''''' is the fourth [[studio album]] by the Australian rock band [[Powderfinger]], produced by [[Nick DiDia]] and released on 4 September 2000 by [[Universal Music]]. It won the 2001 [[ARIA Music Awards of 2001|ARIA Music Award]] for Highest Selling Album, Best Group and Best Rock Album. The album was the band's shortest yet, focusing on social, political, and emotional issues that had appeared in prior works, especially ''[[Internationalist (album)|Internationalist]]''.

The album produced four [[single (music)|singles]]. The most successful, "[[My Happiness (song)|My Happiness]]", reached #4 on the [[ARIA Singles Chart]], won the [[ARIA Music Awards of 2001|2001 ARIA Music Award]] for "Single of the Year", and topped [[Triple J Hottest 100, 2000|Triple J's Hottest 100]] in 2000. The album also featured "[[These Days (Powderfinger song)|These Days]]", which topped [[Triple J Hottest 100, 2000|Triple J's Hottest 100]] in 1999. The album was also ranked at number 1 in [[Triple J Hottest 100 Australian Albums of All Time, 2011|Triple J's Hottest 100 Australian Albums of All Time]] poll in 2011.

Many critics lauded the album as Powderfinger's best work; one stated that the album was "the Finger's Crowning Glory", however, others were critical of the "imitation" contained in the album. Overall, the album won five ARIA Music Awards in 2001 and was certified [[Music recording sales certification|platinum]] seven times, and earned an eighth in 2004. ''Odyssey Number Five'' was Powderfinger's first album to chart in the United States, and the band toured extensively around North America to promote its release.

== Background, recording, and production ==
In a 1997 interview, Powderfinger [[bassist]] [[John Collins (Australian musician)|John Collins]] hinted that the group's next album would be similar to their previous album, ''[[Internationalist (album)|Internationalist]]'',<ref>{{cite news|title=Keeping An Even Keel|publisher=Time Off Publications|author=Nicholson, Geoff|date=17 November 1999}}</ref> while [[frontman]] [[Bernard Fanning]] said in September 2000 that the lyrics on the album, like those on "[[The Metre/Waiting for the Sun|Waiting for the Sun]]", were his "most personal and direct yet".<ref name=PCentral39 /> Fanning said his lyrics were based on the "obstacles in the way of being in a relationship, especially in our work situation".<ref name=PCentral39 />

Powderfinger worked with [[record producer|producer]] [[Nick DiDia]] on ''Odyssey Number Five'', as they had done on ''Internationalist'', finishing the album in August 2000 after six weeks of recording.<ref name=Jackets /> The band spent this time ensuring higher quality songs than on ''Internationalist'', which had featured out-of-tune guitars on "[[Passenger (Powderfinger song)|Passenger]]".<ref name=Jackets>{{cite news|title=Trusty Old Jackets|publisher=''Massive''|author=Yates, Rod|date=September 2000}}</ref><!-- re. the first FAC, in the source they admit to out of sync guitars -->

''Odyssey Number Five'' was Powderfinger's shortest album when recorded, running for approximately 45 minutes. The focus of the album was on restraint, with more simplistic lyrics than previously, and with a plain and simple message intended.<ref name=PCentral39 /> Fanning said of his songwriting ethic: "You try and make it something that’s got some substance, but also, you can never do that at the cost of it having relevance to what you’re singing."<ref name=PCentral39 /> Powderfinger manager [[Paul Piticco]] commented that "their ethos is to be pushing the limits of their songwriting ability".<ref name=PCentral39>{{cite news|title=This Sporting Life|publisher=''Juice''|author=Wooldrige, Simon|date=September 2000}}</ref>

Like Powderfinger's previous album ''Internationalist'', ''Odyssey Number Five'' commented on [[sociology|social]] and [[politics|political]] issues heavily, with the primary point of focus being [[Indigenous Australians#Indigenous Australians|Aboriginal affairs]].<ref name=PCentral39 /> The lyrics of "[[Like a Dog (song)|Like a Dog]]" attacked former [[Prime Minister of Australia]] [[John Howard]]'s [[Liberal Party of Australia|Liberal]] government for its treatment of [[Indigenous Australians]], and for breaking the "relaxed and comfortable" promise he made in the [[Australian federal election, 1996]].<ref name=Jackets /><ref>{{cite news|url=http://www.theaustralian.news.com.au/story/0,25197,22950641-16382,00.html |title=Time to be pragmatic|publisher=[[News.com.au]]|work=[[The Australian]]|date=20 December 2007|accessdate=6 January 2008|quote=In 1996, John Howard won by promising not a conservative revolution but to govern "for all of us" and to make Australians feel "relaxed and comfortable".}}</ref> Lead singer [[Bernard Fanning]] related this to the band's other ethical stances—refusing to appear on ''[[Hey Hey it's Saturday]]'', for its anti-[[homosexual|gay]] commentary, or not allowing Powderfinger songs to be used in [[jingle]]s, amongst others—stating, "We're not here to set an example. We just want to be happy with ourselves and not end up with a guilty conscience."<ref name="fiveeasypieces">{{cite news|title=Five Easy Pieces|publisher=''[[Sain (magazine)|Sain]]''|author=Eliszer, Christie|date=September 2000}}</ref> Fanning said that despite "Like a Dog" being about a political issue, it was not a political song, rather just Powderfinger "voicing our opinions".<ref name=Eskymag /> The band worked with boxer [[Anthony Mundine]] on the song's music video, whom Fanning praised as "the perfect lead, in terms of what the song is about and the fact that he’s prepared to speak up for what he believes in."<ref name=Eskymag>{{cite news|title=Powderfinger 2001 - The Odyssey Continues|publisher=''Esky''|author=Munro, Kelsey|date=February 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl28.htm --></ref>

As well as providing social commentary, ''Odyssey Number Five'' also discussed love, a recurring motif in Fanning's songwriting. Fanning noted that one of the causes of this was his passion for [[soul (music)|soul]] and [[gospel music]], stating that he "listen[s] to a lot of soul music that's unashamedly about love and how good it makes you feel".<ref name=SainUnlimited /> Lead guitarist [[Ian Haug]] agreed, and also noted that the band as a whole were fully committed to Fanning's lyrics, stating "It's really important for us to agree with what Bernard is singing."<ref name=SainUnlimited>{{cite news|title=Odyssey Number Five Is Born|publisher=''Sain Unlimited''|author=Dennison, Pennie|date=September 2000}}</ref>

''Odyssey Number Five'' marked Powderfinger's first successful attempt to enter the United States market. Fanning told ''[[Billboard.com|Billboard]]'' in a 2001 interview that the band were not taking anything for granted, however, stating, "In America, we haven't really done any work yet to deserve any major popularity",<ref name=BBinterview /> with the "vibes" on previous albums failing to reach the American mainstream.<ref name=RDW>{{cite news|title=Powderfinger Finds Their Bit Of Happiness|publisher=''[[Real Detroit Weekly]]''|author=McCarthy, Shannon|date=24 May 2001}}</ref> Powderfinger toured extensively around the country, performing in 22 cities. As a result of these efforts, "[[My Happiness (song)|My Happiness]]" was briefly placed on rotation on [[KROQ-FM]] and several other [[radio station]]s. The song ultimately peaked at #23 on the [[Modern Rock Tracks]] chart.<ref name=BBinterview>{{cite web|url=http://www.billboard.com/articles/news/79441/powderfinger-exports-its-aussie-appeal-stateside |title=Powderfinger Exports Its Aussie Appeal Stateside |publisher=[[Billboard.com]]|author=Jill Pesselnick|date=13 June 2001|accessdate=23 December 2007}}</ref><ref name=BBcharts>{{cite web |url={{BillboardURLbyName|artist=powderfinger|chart=all}} |title=Artist Chart History&nbsp;— Powderfinger|publisher=''[[Billboard.com|Billboard]]''|accessdate=23 December 2007|archiveurl = https://web.archive.org/web/20080220155414/{{BillboardURLbyName|artist=powderfinger|chart=all}} |archivedate = 20 February 2008|deadurl=yes}}</ref> This success was assisted by the band appearing on the ''[[Late Show with David Letterman]]'', and by supporting [[Coldplay]] on tour.<ref name=PttP /> Guitarist [[Darren Middleton]] summarised their work in the United States by stating "This year has been a bit of a blur."<ref name=PttP>{{cite news|publisher=''RM Rave''|title=Powder to the People|author=Sawford, Gavin|date=12 July 2001}}<!-- http://www.ozmusic-central.com.au/powderfinger/text/articl41.htm --></ref>

== Album and single releases ==
{{Listen|filename=Powderfinger-My Happiness-23s.ogg|title="My Happiness"|description=A 23-second sample from "My Happiness", which went on to become Powderfinger's most successful single, and helped break into the American market.<ref name=BBcharts /> [[Allmusic]]'s Dean Carlson commented that in the song, Powderfinger "drowns a hip-hop-inspired guitar riff better than most bands of their stature".<ref name="amg">{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}} |title=Odyssey Number Five > Review|author=Carlson, Dean|publisher=[[Allmusic]]|accessdate=31 January 2008}}</ref>}}
{{Listen|filename=Powderfinger-LikeADog-13sec.ogg|title="Like a Dog"|description=A 13-second sample from "Like a Dog", which aggressively attacks the Howard government, asking "why the hell won’t John Howard say sorry to the Aboriginal people!"<ref name=LikeADog /> The song's riff was written by Haug, and was influenced by his love of [[Iggy Pop]].<ref name=Fingerprints>Powderfinger (Fanning, Middleton, Haug, Collins, Coghill) et al. (2005). ''[[Fingerprints: The Best of Powderfinger, 1994 - 2000]]'' companion booklet.</ref>}}

''Odyssey Number Five'' was released on 4 September 2000, on the [[Grudge Records|Grudge]]/[[Universal Music Australia|Universal]] record labels. The album was released in the United Kingdom on [[Polydor]], with 15 minutes of video and an additional track ''Nature Boy'', at a later date. A sampler version was released in the United States in 2001, containing five tracks.<ref>{{cite web|url=http://www.powderfinger.com/site.php#releases |title=Discography |publisher=[[Powderfinger]] |accessdate=9 March 2008|archiveurl = https://web.archive.org/web/20080317060254/http://www.powderfinger.com/site.php#releases |archivedate = 17 March 2008|deadurl=yes}}</ref><ref>{{cite web|url=http://www.amazon.com/dp/B00004Z3BE |title=Odyssey Number Five: Powderfinger|publisher=[[Amazon.com]]|accessdate=9 March 2008}}</ref>

Four [[single (music)|singles]] were released from the album. "[[My Kind of Scene]]" was the first; released as a [[promotional single]] in June 2000. The track was written for the 2000 film ''[[Mission: Impossible II]]'', and appeared on its [[Mission: Impossible II soundtrack|soundtrack]]. Collins and Middleton recalled that the song was written and produced with photos of [[Tom Cruise]] and [[Nicole Kidman]] on the wall of the band's studio "as inspiration". They noted that the band made three songs in response to the ''Mission: Impossible II'' request, and that "My Kind of Scene" was chosen over "Up & Down & Back Again" and "Whatever Makes You Happy".<ref name=Jackets />

The second single from the album was "[[My Happiness (song)|My Happiness]]", released on 14 August 2000 in Australia.<ref>{{cite web|url=http://www.amazon.com/dp/B00004W9QG |title=My Happiness: Powderfinger|publisher=[[Amazon.com]]|accessdate=8 March 2008}}</ref>  "My Happiness" entered the [[ARIA Charts|ARIA Singles Chart]] at #4, and spent 24 weeks on the chart, making it Powderfinger's highest charting single in Australia.<ref name=AUSchartshome>{{cite web|url=http://www.australian-charts.com/showinterpret.asp?interpret=Powderfinger |title=Discography&nbsp;— Powderfinger|publisher=australian-charts.com|accessdate=26 December 2007}}</ref> It peaked at #7 on the [[RIANZ|New Zealand singles chart]], and spent 23 weeks in the top 50. Furthermore, "My Happiness" was Powderfinger's first single to chart in the USA, reaching #23 on the  [[Modern Rock Tracks]] chart.<ref name=BBcharts />

"[[Like a Dog (song)|Like a Dog]]" was released as the third single on 15 January 2001. The song was heavy in political sentiment, akin to "[[The Day You Come]]" on ''Internationalist''.<ref name=Eskymag /> The [[riff]] for the song was written by [[Ian Haug]], and the song's music video featured Australian Aboriginal [[boxing|boxer]] [[Anthony Mundine]], and was based on the 1980 [[Martin Scorsese]] film ''[[Raging Bull]]''. [[Drummer]] [[Jon Coghill]] said the song revolved around the question of "why the hell won’t John Howard say sorry to the Aboriginal people!"<ref name=LikeADog>{{cite news|title=Powderfinger&nbsp;— Jon Coghill|author=Deegan, Ray|publisher=''Drum Scene''|date=Nov 2001 – Jan 2002}}</ref> "Like a Dog" spent one week on the ARIA Singles Chart, at #40.<ref name=AUSchartshome />

Two songs from the album, "[[The Metre/Waiting for the Sun|The Metre]]" and "[[The Metre/Waiting for the Sun|Waiting for the Sun]]", were released as a double [[A-side]] to form the final single. The single was released on 21 August 2001, and included a cover of [[Iron Maiden]]'s "[[The Number of the Beast (song)|Number of the Beast]]".<ref>{{cite web|url=http://www.amazon.com/dp/B00005NQQ9 |title=Metre: Powderfinger|publisher=[[Amazon.com]]|accessdate=8 March 2008}}</ref> "Waiting for the Sun" was written by Fanning as a devotional, [[gospel music|gospel]] style song. He said of the song; "It’s about being in a relationship and being really heavily happy with it."<ref name=PCentral39 /> "The Metre" spent one week on the ARIA Singles Chart, at #31.<ref name=AUSchartshome />

== Reception ==
''Odyssey Number Five'' mostly gained positive reviews, and was more successful than its predecessor, ''Internationalist''. ''[[Entertainment Weekly]]'' reviewer Marc Weingarten gave the album a B+. He stated that album entered new "terrain" in guitar rock, complimenting the "scratching and clawing guitars", drawing comparisons to [[Travis (band)|Travis]] ("prim") and [[Oasis (band)|Oasis]] ("mock-grandiose").<ref name="ew">{{cite news|url=http://www.ew.com/ew/article/0,,280590,00.html |title=Odyssey Number Five|publisher=''[[Entertainment Weekly]]''|author=Weingarten, Marc|date=30 March 2001|accessdate=6 January 2008}}</ref>

[[Allmusic]] reviewer Dean Carlson disliked the album, giving it a rating of one and a half stars. He described it as "little more than a slightly off-base perspective into the world of mid-90s American grunge", and described it as highly similar to [[Neil Young]].<ref name="amg-review" /> Carlson's only praise was for songs "Odyssey #5" and "[[My Happiness (song)|My Happiness]]", stating that "[t]oo often, Powderfinger is too earnest, a bit too careful in their career". Carlson noted that despite his critique, the album achieved some success in the American market.<ref name="amg-review">{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}}|title=Odyssey Number Five > Review|publisher=[[Allmusic]]|author=Carlson, Dean|accessdate=23 December 2007}}</ref>

Devon Powers of ''[[PopMatters]]'' complimented Fanning's vocals, and said the focus of the album was "meaty, rolling ballads".<ref name="popmatters" /> Powers said that many of the songs on the album were "the kind of songs you put on repeat for hours, or days".<ref name="popmatters" /> The main critique was for the "faster numbers", stating that "[[Like a Dog (song)|Like a Dog]]" "sounds mostly a little bored".<ref name="popmatters" /> The review concluded by noting that the best songs on ''Odyssey'' were those not available as "fleeting radio singles and background music".<ref name="popmatters">{{cite web|url=http://www.popmatters.com/music/reviews/p/powderfinger-odyssey.shtml|title=Powderfinger: Odyssey #5|author=Powers, Devon|publisher=''[[PopMatters]]''|accessdate=22 December 2007}}</ref>

''Odyssey Number Five'' won the [[ARIA Music Awards of 2001|2001]] [[ARIA Awards]] for "Album of the Year", "Highest Selling Album", "Best Rock Album", "Best Cover Art", and "Best Group". "My Happiness" won the award for "Single of the Year", while "Like a Dog" was nominated for "Highest Selling Single" and "Best Video". At the [[ARIA Music Awards of 2002|2002 ARIA Awards]], "The Metre" was nominated for "Best Group".<ref name=AAwards>{{cite web|url=http://www.ariaawards.com.au/history-by-artist.php?letter=P&artist=Powderfinger|title=History: Winners by Artist: Powderfinger|publisher=Australian Recording Industry Association|accessdate=18 September 2007|archiveurl=https://web.archive.org/web/20070926235606/http://ariaawards.com.au/history-by-artist.php?letter=P&amp;artist=Powderfinger <!--Added by H3llBot-->|archivedate=26 September 2007}}</ref> The album was named "Album of the Year" by ''[[Rolling Stone Australia]]'' readers, with "My Happiness" taking out "Song of the Year" and Powderfinger receiving "Band of the Year".<ref name=RSBOTY>{{cite news |title=Powderfinger&nbsp;— Band Of The Year|publisher=''[[Rolling Stone Australia]]''|author=Apter, Jeff|date=April 2001}}</ref>

== Charts and certifications ==
{| class="wikitable"
!Chart
!Peak
!Certification
|-
|[[ARIA Charts|ARIA Albums Chart]]
|#1<ref name=Auscharts>{{cite web|url=http://www.australian-charts.com/showitem.asp?interpret=Powderfinger&titel=Odyssey+Number+Five&cat=a |title=Powderfinger&nbsp;— Odyssey Number Five|publisher=australian-charts.com |accessdate=23 December 2007}}</ref><ref>{{cite web|url=http://www.bbc.co.uk/dna/h2g2/A701551 |title=Powderfinger&nbsp;— the Band|publisher=[[h2g2]]|accessdate=9 March 2008}}</ref>
|<center>8× [[Music recording sales certification#List of international sales certification thresholds|Platinum]]<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-accreditations-albums-2004.htm |title=ARIA Charts&nbsp;— Accreditations - 2004 Albums|publisher=[[Australian Recording Industry Association|ARIA]]|accessdate=23 December 2007}}</ref>
|-
|[[Recording Industry Association of New Zealand|RIANZ Albums Chart]]
|#15<ref>{{cite web|url=http://charts.org.nz/showitem.asp?interpret=Powderfinger&titel=Odyssey+Number+Five&cat=a |title=Powderfinger&nbsp;— Odyssey Number Five|publisher=charts.org.nz|work=[[Recording Industry Association of New Zealand|RIANZ]]|accessdate=23 December 2007}}</ref>
|–
|-
|[[Top Heatseekers]]
|#35<ref>{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}} |title=Odyssey Number Five > Charts & Awards > Billboard Albums|publisher=[[Allmusic]]|accessdate=23 December 2007}}</ref>
|–
|}

== Awards and nominations ==
{{main article|List of Powderfinger awards}}

=== ARIA Awards ===
{| class="wikitable"
!align="center"|Year
!align="center"|Nominated work
!align="center"|Award
!align="center"|Result<ref name=AAwards />
|-
|rowspan=8| [[ARIA Music Awards of 2001|2001]]
|rowspan=5| ''Odyssey Number Five''
| Album of the Year
| style="background: #ddffdd"| Won
|-
| Highest Selling Album
| style="background: #ddffdd"| Won
|-
| Best Rock Album
| style="background: #ddffdd"| Won
|-
| Best Cover Art
| style="background: #ddffdd"| Won
|-
| Best Group
| style="background: #ddffdd"| Won
|-
| "[[My Happiness (song)|My Happiness]]"
| Single of the Year
| style="background: #ddffdd"| Won
|-
|rowspan=2| "[[Like a Dog (song)|Like a Dog]]"
| Highest Selling Single
| style="background: #ffdddd"| Nominated
|-
| Best Video
| style="background: #ffdddd"| Nominated
|-
| [[ARIA Music Awards of 2002|2002]]
| "[[The Metre/Waiting for the Sun|The Metre]]"
| Best Group
| style="background: #ffdddd"| Nominated
|}

=== Other accolades ===
{| class="wikitable"
|-
! Year
! Provider
! Nominated work
! Accolade
! Result
|-
|rowspan=2| [[Triple J Hottest 100, 2000|2000]]
|rowspan=2| [[Triple J]]
| "[[My Happiness (song)|My Happiness]]"
| [[Triple J Hottest 100|Hottest 100 Inductee]]
| No.&nbsp;1<ref name=JJJ100>{{cite web | title=Hottest 100 2000 | url=http://www.abc.net.au/triplej/hottest100/history/2000.htm | work=[[Triple J]] | accessdate=11 June 2007}}</ref>
|-
| "[[My Kind of Scene]]"
| Hottest 100 Inductee
| No.&nbsp;3<ref name=JJJ100 />
|-
|2001
|[[Australasian Performing Right Association|APRA]]
| "[[My Happiness (song)|My Happiness]]"
| Song of the Year
|style="background-color: #ddffdd"| Won<ref>{{cite web|url=http://www.apra.com.au/awards/music/winners2001.asp|title=APRA Music Awards 2001|work=Winners|publisher=Australasian Performing Right Association|accessdate=24 December 2007|archiveurl = https://web.archive.org/web/20071221055752/http://www.apra.com.au/awards/music/winners2001.asp |archivedate = 21 December 2007|deadurl=yes}}</ref>
|-
|2010
|[[John O'Donnell (music journalist)|John O'Donnell]], [[Toby Creswell]], [[Craig Mathieson]]
| ''Odyssey Number Five''
| ''[[100 Best Australian Albums]]''
| No.&nbsp;43<ref name="ODonnell">{{Cite book | title = [[100 Best Australian Albums]] | last1 = O'Donnell | first1 = John | authorlink1 = John O'Donnell (music journalist) | last2 = Creswell | first2=Toby | authorlink2 = Toby Creswell | last3 = Mathieson | first3 = Craig | authorlink3 = Craig Mathieson | publisher = Hardie Grant Books | date = October 2010 | location = [[Prahran, Victoria|Prahran, Vic]] | isbn = 978-1-74066-955-9 }}</ref>
|-
|2011
|[[Triple J]]
| ''Odyssey Number Five''
| ''[[Triple J Hottest 100 Australian Albums of All Time, 2011|Hottest 100 Australian Albums]]''
| No.&nbsp;1<ref name=JJJ100Australian>{{Cite web | title=Triple J Hottest 100 Australian Albums of All Time, 2011 | url=http://www.abc.net.au/triplej/hottest100/archive/archive.htm?year=2011&alltime=1 | work=[[Triple J]] | accessdate=26 June 2013}}</ref>
|}

== Track listing ==
All songs written by Powderfinger:<ref name=Auscharts />

# "[[The Metre/Waiting for the Sun|Waiting for the Sun]]"&nbsp;– 3:54
# "[[My Happiness (song)|My Happiness]]"&nbsp;– 4:36
# "[[The Metre/Waiting for the Sun|The Metre]]"&nbsp;– 4:33
# "[[Like a Dog (song)|Like a Dog]]"&nbsp;– 4:20
# "Odyssey #5"&nbsp;– 1:44
# "Up & Down & Back Again"&nbsp;– 4:24
# "[[My Kind of Scene]]"&nbsp;– 4:37
# "[[These Days (Powderfinger song)|These Days]]"&nbsp;– 4:58
# "We Should Be Together Now"&nbsp;– 3:42
# "Thrilloilogy"&nbsp;– 6:10
# "Whatever Makes You Happy"&nbsp;– 2:28
# "Nature Boy" (UK release)&nbsp;– 3:37

== Personnel ==
{{col-start}}
{{col-break}}
<big>'''Powderfinger'''</big>
* [[Bernard Fanning]]&nbsp;– [[guitar]]s and [[vocal]]s<ref name=AMGcreidts>{{cite web|url={{Allmusic|class=album|id=r524182|pure_url=yes}} |title=Odyssey Number Five > Credits|publisher=[[Allmusic]]|accessdate=23 December 2007}}</ref>
* [[Darren Middleton]]&nbsp;– guitars and [[Backing vocalist|backing vocals]]
* [[Ian Haug]]&nbsp;– guitars
* [[John Collins (Australian musician)|John Collins]]&nbsp;– [[bass guitar]]s
* [[Jon Coghill]]&nbsp;– [[Drum kit|drums]] and [[Percussion instrument|percussion]]

<big>'''Production'''</big>
* Nick DiDia&nbsp;– [[Record producer|production]], [[Audio engineer|engineering]] and [[audio mixing (recorded music)|mixing]]<ref name=AMGcreidts />
* Matt Voigt&nbsp;– assistant engineering
* Anton Hagop&nbsp;– assistant engineer
* Stewart Whitmore&nbsp;– digital editing
* Stephen Marcussen&nbsp;– [[Audio mastering|mastering]]
* Anton Hagop&nbsp;– assistant production
* Kevin Wilkins&nbsp;– [[art direction]] and photography
{{col-break}}

<big>'''Additional musicians'''</big>
* Nick DiDia&nbsp;– percussion<ref name=AMGcreidts />
* Alex Pertout&nbsp;– percussion
* Matt Murphy&nbsp;– [[Hammond organ|Hammond]] [[Organ (music)|organ]], [[Wurlitzer]] and prophet [[synthesiser]]
* Daniel Denholm&nbsp;– [[String instrument|string]] [[arrangement]]s
* Jun Yi Ma&nbsp;– [[violin]]
* Naomi Radom&nbsp;– violin
* Fiona Ziegler&nbsp;– violin
* Jacob Plooij&nbsp;– violin
* Mandy Murphy&nbsp;– [[viola]]
* Felicity Wyithe&nbsp;– viola
* Peter Morrison&nbsp;– [[cello]]
* Maxime Bibeau&nbsp;– [[contrabass guitar]]
* Damien Bennett&nbsp;– backing vocals
*; Tiddas
* [[Tiddas (band)|Lou Bennett]]&nbsp;– backing vocals
* [[Tiddas (band)|Sally Dastey]]&nbsp;– backing vocals
* [[Tiddas (band)|Amy Saunders]]&nbsp;– backing vocals
{{col-end}}

== See also ==
{{portal|Music of Australia}}
* [[Powderfinger discography]]
{{Clear}}

== References ==
{{Reflist|30em}}

{{s-start}}
{{succession box
 | before = ''33⅓'' by [[John Farnham]]
 | title = Australian [[ARIA Charts|ARIA Albums Chart]] [[Number-one albums of 2000 (Australia)|number-one album]]
 | years = 11–24 September 2000<br />30 October - 5 November 2000
 | after = ''[[The Games of the XXVII Olympiad 2000: Music from the Opening Ceremony]]''<br /> by Various artists
}}
{{end}}

{{Powderfinger}}
{{ARIA Award for Album of the Year 2000s}}

{{featured article}}

[[Category:2000 albums]]
[[Category:ARIA Award-winning albums]]
[[Category:Powderfinger albums]]
[[Category:Universal Music Australia albums]]