{|{{Infobox aircraft begin
 |name            = XBQ-2
 |image           = Fleetwings XBQ-2A.jpg
 |size            = 300px
 |alt             = 
 |caption         = 
 |long caption    = 
}}{{Infobox aircraft type
 |type            = [[Flying bomb]]
 |national origin = [[United States]]
 |manufacturer    = 
 |builder         = [[Fleetwings]]
 |designer        = 
 |design group    = 
 |first flight    = 1943
 |introduced      = 
 |introdction     = 
 |retired         = 
 |status          = 
 |primary user    = [[United States Army Air Forces]]
 |more users      = 
 |produced        = 
 |number built    = 1
 |program cost    = 
 |unit cost       = 
 |developed from  = 
 |variants with their own articles = [[Fleetwings BQ-1]]
 |developed into  = 
}}
|}
The '''Fleetwings BQ-2''' was an early expendable [[unmanned aerial vehicle]] — referred to at the time as an "[[flying bomb|assault drone]]" — developed by [[Fleetwings]] during the [[Second World War]] for use by the [[United States Army Air Forces]]. Only a single example of the type was built; the aircraft was deemed too expensive for service and was cancelled after a brief flight testing career.

==Development==
Development of the BQ-2 began on July 10, 1942, under a program for the development of "aerial torpedoes" - unmanned [[flying bomb]]s - that had been instigated in March of that year. Fleetwings was contracted to build a single XBQ-2 assault drone,<ref name="AU">Werrell 1985, p.30.</ref> powered by two [[Lycoming XO-435]] [[opposed piston engine]]s, and fitted with a fixed landing gear in [[tricycle landing gear|tricycle configuration]];<ref name="DS"/> the landing  gear was jettisonable for better aerodynamics.<ref name="AU"/>

The BQ-2 was [[optionally piloted vehicle|optionally piloted]]; a single-seat cockpit was installed for ferry and training flights; a fairing would replace the cockpit canopy on operational missions.<ref name="DS">Parsch 2005</ref> The BQ-2 was intended to carry a {{convert|2000|lb}} [[warhead]] over a range of {{convert|1717|mi}} at {{convert|225|mph}}; the aircraft would be destroyed in the act of striking the target.<ref name="AU"/> A single [[Fleetwings BQ-1|BQ-1]] was to be constructed as well under the same contract.<ref name="AU"/>

==Flight testing==
The XO-435 engines were dropped from the design of the XBQ-2 before completion, being replaced by two [[Lycoming R-680]] [[radial engine]]s, with the aircraft being redesignated XBQ-2A.<ref>Andrade 1979, p.60.</ref>

Following trials of the [[television]]-based [[command guidance]] system using a [[Fleetwings PQ-12|PQ-12]] [[target drone]], the XBQ-2A flew in mid 1943; following flight trials, the design was determined to be too expensive for operational use, and the program was cancelled in December of that year.<ref name="DS"/>

==Specifications (XBQ-2A)==
[[File:Fleetwings XBQ-2A front.jpg|thumb|right|The XBQ-2A.]]
{{Aircraft specs
|ref=<ref name="DS"/>
|prime units?=imp

|crew=1 (optional)
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=48
|span in=7
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=7700
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=

|eng1 number=2
|eng1 name=[[Lycoming R-680]]-13
|eng1 type=radial piston engines
|eng1 kw=
|eng1 hp=280
|eng1 shp=
|eng1 note=
|power original=

|prop blade number=
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

|perfhide=y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=

|more performance=

|armament=* {{convert|2000|lb}} warhead
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
|see also=
|related=
* [[Fleetwings BQ-1]]
|similar aircraft=
* [[Fairchild BQ-3]]
* [[Interstate TDR]]
|lists=
|similar engines=
}}

==References==
{{commons category|Fleetwings}}
;Notes
{{reflist}}

;Bibliography
{{refbegin}}
*{{cite book|last=Andrade|first=John|title=U.S. Military Aircraft Designations and Serials since 1909|year=1979|publisher=Midland Counties Publications|location=Leicester, UK|isbn=0-904597-22-9}}
*{{cite web |url=http://www.designation-systems.net/dusrm/app1/bq-1.html |title= Fleetwings BQ-1/2  |first=Andreas |last=Parsch |year=2005 |work=Directory of U.S. Military Rockets and Missiles, Appendix 1: Early Missiles and Drones |publisher=designation-systems.net |accessdate=2013-01-22}}
*{{cite book|last=Werrell|first=Kenneth P.|title=The Evolution of the Cruise Missile|year=1985|publisher=Air University Press|location=Maxwell AFB, Alabama|isbn=978-1478363057}}
{{refend}}

{{Fleetwings aircraft}}
{{USAAF drones}}

[[Category:Fleetwings aircraft|BQ-2]]
[[Category:United States bomber aircraft 1940–1949]]
[[Category:Unmanned aerial vehicles of the United States]]
[[Category:World War II guided missiles of the United States]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:High-wing aircraft]]