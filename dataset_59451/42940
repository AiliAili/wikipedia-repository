{{Infobox aircraft
|name           = Hanriot 1909 monoplane 
|image          = File:1909 Salon Hanriot.png
|caption        = At the 1909 Paris Salon
|type           = sports aircraft
|national origin = [[France]]
|manufacturer   = [[Hanriot]]
|designer       = 
|first flight   = 1909
|introduced     = <!--Date the aircraft entered or will enter military or revenue service-->
|retired        = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status         = <!--In most cases, redundant; use sparingly -->
|primary user   = <!-- List only one user; for military aircraft, this is a nation or a service arm. Please DON'T add those tiny flags, as they limit horizontal space. -->
|more users     = <!-- Limited to THREE (3) 'more users' here (4 total users).  Separate users with <br/>. -->
|produced       = <!--Years in production (eg. 1970-1999) if still in active use but no longer built -->
|number built   =2 
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from = <!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
}}

The '''Hanriot 1909 monoplane''' was an early [[France|French]] aircraft constructed by [[Rene Hanriot]], a successful automobile racer.

==Design==
[[File:Hanriot 1909 engine.jpg|thumbnail|Engine installation, 50 hp Buchet]]
The Hanriot 1909 [[monoplane]] had an uncovered  rectangular-section wire-braced wooden [[fuselage]] with deeply [[camber (aerodynamics)|camber]]ed parallel-[[chord (aircraft)|chord]] wings. The main undercarriage consisted of a pair of skids which carried a pair of independently sprung wheels mounted on a steel cross tube, the skids being carried on two pairs of struts which converged inwards, the aft pair being continued above the fuselage to form an inverted V [[cabane strut|cabane]] to which the wing bracing and [[wing-warping|warping]] wires were attached. The front struts terminated at the engine bearers, which were midway between the upper and lower [[longerons]]. Tail surfaces consisted of a [[tailplane]] and [[elevator (aircraft)|elevator]] mounted on top of the fuselage and a fixed fin mounted under the fuselage with the attached rudder underneath the horizontal tail surfaces.<ref>Opdycke 1999, pp. 148-9.</ref> The aircraft was controlled with a pair of handwheels on either side of the cockpit operating [[wing warping]] and [[elevator (aircraft)|elevator]], and foot-pedals operating the [[rudder]].

Two examples were shown at the Paris Aero Salon in October 1909 in an unfinished condition. One was powered by a (37&nbsp;kW (50&nbsp;hp)  [[Buchet]] engine and the other by a 25&nbsp;kW (35&nbsp;hp) Hanriot engine.<ref name= Flight1/>  One was successfully flown at Rheims in December 1909, first by [[Eugene Ruchonnet]] and afterwards by Rene Hanriot followed by his son [[Marcel Hanriot|Marcel]], then aged 15.<ref>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200818.html Hanriot Flyers at Rheims   ][[Flight International|''Flight'']], 18 December 1909.</ref>
[[File:Hanriot 1909.jpg|thumb|right|Marcel Hanriot in front of the 1909 monoplane]]
The two aircraft displayed at the 1909 Paris exhibition were the only examples manufactured.  ''Flight'' refers to them as the Hanriot I (Hanriot engine) and Hanriot II (Buchet engine).<ref name=Flight1/>  By the time that they were first flown  Hanriot and Ruchonnet had already started work on a series of closely related monoplane designs, which were exhibited at the ''Salon d'Automobiles, d'Aeronautique, du Cycles et des Sports'',  which opened in [[Brussels]] on 16 January 1910.<ref>Opdycke 1999, p.149.</ref>  
[[File:Salon de locomotion aerienne 1909 Grand Palais Paris.jpg|thumbnail|The Paris Aero Salon, 1909: the two Hanriot monoplanes at bottom left]]

==Specifications==
{{Aircraft specs|ref=<ref name=Flight1>[http://www.flightglobal.com/pdfarchive/view/1909/1909%20-%200721.html Table of French Flyers][[Flight International|''Flight'']], 13 November 1909.</ref>

|prime units?=met

<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=9.4
|length ft=
|length in=
|span m=9.16
|span ft=
|span in=
|height m=
|height ft=
|height in=
|wing area sqm=24
|wing area sqft=
|empty weight kg=400
|empty weight lb=
|gross weight kg=
|gross weight lb=
|fuel capacity=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Buchet]] 
|eng1 type=6-cylinder inline [[piston engine]], water cooled
|eng1 kw=<!-- prop engines -->
|eng1 hp=50
|eng1 shp=<!-- prop engines -->

|prop blade number=
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->

<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|cruise speed kts=
|range km=
|range miles=
|range nmi=
|ceiling m=
|ceiling ft=
|climb rate ms=
|climb rate ftmin=
|more performance=
}}

==Notes==
{{reflist}}

==References==
*Opdycke, Leonard, ''French Aeroplanes before the Great War''. Atglen, PA: Schiffer, 1999 ISBN 0-7643-0752-5

{{Hanriot aircraft}}

[[Category:French experimental aircraft 1900–1909]]
[[Category:Hanriot aircraft|1909 monoplane]]
[[Category:Articles using Infobox aircraft]]