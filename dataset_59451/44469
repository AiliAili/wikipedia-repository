{{other uses}}
{{Infobox book|
| name         = The Black Tulip
| title_orig   = La Tulipe Noire
| image        =
| author       = [[Alexandre Dumas, père|Alexandre Dumas]]
| cover_artist =
| country      = France
| language     = Translated from French
| genre        = [[Historical]], [[Romance novel|Romantic]]
| publisher    = Baudry, Paris 1850
| release_date = French 1850
| media_type   =
| pages        = 234 p. (Penguin Classics Edition)
| isbn         = 978-0-14-044892-4
| isbn_note    = (Penguin Classics Edition)
| dewey= 843/.7 22
| congress= PQ2229.T8 E5 2003
| oclc= 51528417
}}
'''''The Black Tulip''''' is a [[historical novel]] written by [[Alexandre Dumas, père]].

==Plot==
The story begins with a historical event — the 1672 [[lynching]] of the Dutch [[Grand Pensionary]] (roughly equivalent to a modern [[Prime Minister]]) [[Johan de Witt]] and his brother [[Cornelis de Witt|Cornelis]], by a wild mob of their own countrymen — considered by many as one of the most painful episodes in Dutch history, described by Dumas with a dramatic intensity.

The main plot line, involving fictional characters, takes place in the following eighteen months; only gradually does the reader understand its connection with the killing of the de Witt brothers.

The city of [[Haarlem]], Netherlands, has set a [[prize]] of {{currency|100000|NLG}} to the person who can grow a black [[tulip]], sparking competition between the country's best gardeners to win the money, honour and fame. Only the city's oldest citizens remember the [[Tulip Mania]] thirty years prior, and the citizens throw themselves into the competition. The young and [[bourgeois]] Cornelius van Baerle has almost succeeded but is suddenly thrown into the [[Loevestein]] [[prison]].  There he meets the prison guard's beautiful daughter Rosa, who will be his comfort and help, and eventually become his rescuer.

The novel was originally published in three volumes in 1850 as ''La Tulipe Noire'' by Baudry (Paris).

==Characters==
'''William, Prince of Orange''', afterward [[William III of England|William III]]. [[King of England]].

'''[[Louis XIV]]''', [[King of France]].

'''Cornelius de Witt''', inspector of dikes at [[the Hague]].

'''[[Johan de Witt]]''', his brother, Grand Pensionary of Holland.

'''Colonel van Deeken''', aide-de-camp to William of Orange.

'''Dr. Cornelius van Baerle''', a [[tulip]]-fancier, godson of Cornelius de Witt.

'''Mynheer Isaac Boxtel''', his rival.

'''Marquis de Louvois'''.

'''Count Tilly''', [[Captain (land)|Captain]] of the Cavalry of [[the Hague]].

'''Mynheer Bowelt''', [[wikt:deputy|deputy]].

'''Mynheer d'Asperen''', deputy.

'''The Recorder of the States'''.

'''Master van Spenser''', a [[magistrate]] at [[Dordrecht|Dort]].

'''Tyckalaer''', a surgeon at [[the Hague]].

'''Gerard Dow'''.

'''Mynheer van Systens''', Burgomaster of [[Haarlem]] and President of its [[Horticultural Society]].

'''Craeke''', a confidential servant of John de Witt.

'''Gryphus''', a jailer, Rosa's father.

'''Rosa''', his daughter, in love with Cornelius van Baerle.

==Memorable Quotes==
"Sometimes one's sufferings have been so great that one need never say, 'I am too happy." Engraved on the cell wall of Hugo Grotius at Loevestein and subsequently above the door of Cornelius Van Baerle.

"Let them eat cake" Marie Antoinette

"yes" said the axe man "you can take the body".

== Adaptations ==
The first screen adaptation appears to have been a silent 1921 Dutch-UK co-production directed by [[Maurits Binger]] and [[Frank Richardson (director)|Frank Richardson]].  [[Alex Bryce]] directed a well-regarded UK adaptation of the novel in 1937, with [[Patrick Waddington]] as Cornelus Van Baerle.  A five-part [[BBC]] [[miniseries]] debuted in August 1956 with [[Douglas Wilmer]] in the lead role.  A second British miniseries appeared in September 1970.  In 1988, Australia's [[Burbank Films Australia|Burbank]] production company created a 50-minute children's animated film from a bowdlerised version of the story.

In 1963, a movie called ''[[La Tulipe noire]]'' with actor [[Alain Delon]] was produced in France, but was not based on the novel.  There was also a short [[Finland|Finnish]] documentary made by [[Pacho Lane]] in 1988 called ''The Black Tulip'', but it told the story of [[Soviet Union|Soviet]] soldiers fighting in [[Afghanistan]].

A musical adaptation was written in 2004 by Kit Goldstein, and premiered at [[Union College]] in February 2005.
<!-- Looking on the same page in German: http://de.wikipedia.org/wiki/Die_schwarze_Tulpe , you will found the confirmation: "1963 wurde ein französischer Film gleichen Namens mit Alain Delon veröffentlicht, dessen Titel vom Roman inspiriert wurde. Der Inhalt weicht jedoch wesentlich von der Romanvorlage ab[1]."  In 1963 a French film with Alain Delon was released with a title apparently inspired by the novel. The story differs apparently from the book edition and has nothing to do with the original novel by Dumas, according to French sources.
Looking on the page in French about the film "La Tulipe noire", we could find the following information: http://fr.wikipedia.org/wiki/La_Tulipe_noire_%28film%29 "Contrairement à ce que pourrait laisser penser le titre, ce film n'a rien à voir avec le roman d'Alexandre Dumas."  Contrary to what could the title let think, this film has nothing to do with the novel of Alexander Dumas. -->

==See also==
{{portal|Novels}}
*[[Assassinations in fiction#Novels|Assassinations in fiction]]
*[[Tulip mania]]

== External links ==
*[https://books.google.com/books?id=CPMLAAAAIAAJ&printsec=frontcover&dq=the+black+tulip&hl=en&ei=-5VxTPWPK8L_lgeWpZSwDw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CDEQ6AEwAA#v=onepage&q&f=false ''The Black Tulip''] full text at [[Google Books]]
* {{gutenberg|no=965|name=The Black Tulip}}
* [http://www.elook.org/literature/dumas/the-black-tulip/ eLook Literature: ''The Black Tulip''] - HTML version broken down chapter by chapter.
* [http://www.kitgoldstein.com/BlackTulip.php Webpage] for ''The Black Tulip'' musical at kitgoldstein.com.
* {{librivox book | title=The Black Tulip | author=Alexandre DUMAS}}

{{Alexandre Dumas, père}}

{{Authority control}}

{{DEFAULTSORT:Black Tulip, The}}
[[Category:1850 novels]]
[[Category:Novels by Alexandre Dumas]]
[[Category:Historical novels]]
[[Category:1670s in fiction]]
[[Category:Novels set in the Netherlands]]
[[Category:Novels set in the Dutch Republic]]  
[[Category:Novels set in the Dutch Golden Age]]
[[Category:French novels adapted into plays]]
[[Category:Cultural depictions of William III of England]]
[[Category:French novels adapted into films]]