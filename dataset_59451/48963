{{Maya civilization}}
'''Ka'Kabish''' is an archaeological site in the [[Orange Walk District]] of [[Belize]], Central America, located near the Maya sites of [[Lamanai]], El Pozito, and [[Blue Creek, Orange Walk|Blue Creek]]. It was once a moderate sized city, built as part of the [[Maya civilization]],<ref name=cause/> and has been determined to have been largely autonomous throughout its history. The modern communities of [[Indian Church, Belize|Indian Church]] and San Filipe are in close proximity to Ka’Kabish, and the [[Mennonites in Belize|Mennonite]] community of [[Blue Creek, Orange Walk|Blue Creek]] is slightly further afield. A road connecting [[Indian Church, Belize|Indian Church]] to San Filipe separates the site into two areas, the North Complex and the South Complex.<ref name=":0">Haines, Helen R. 2014. “Six Years and Counting!:Overview of the 2013 Field Season”. In ''The 2012 Archaeological Report of the Ka’Kabish Archaeological Research Project (KARP).  Final Report of the 2012 field season submitted to The Institute of Archaeology, NICH, Belmopan, Belize'', edited by Helen R. Haines, pp. 7-11. Ka’Kabish Archaeological Research Project, Trent University, Peterborough.</ref><ref name=":1">Tremain, Cara G. and Haines, Helen R. 2013. “Overview of the 2012 Research at Ka’Kabish”. In Ka’Kabish Archaeological Research Project (KARP) Interim Report on the 2012 Field Season, edited by Cara G. Tremain and Helen R. Haines, pp. 7-17. Ka’Kabish Archaeological Research Project, Trent University, Peterborough.</ref>

[[Alfredo Barrera Vasquez]]’s ''Diccionario Maya'' defines the name as; "KA’KAB-IS-AX 10: (place name); ka’kab: village, the seat of population, high land and strong; is: Ipomoea batatas, Lam: sweet + ax: wart; name of the archaeological ruins located near Numk’ini (Nun k’ini, Campeche)."<ref>Barrera Vasquez, A. 1995. ''Diccionario Maya'', 3rd edition. Mexico, DF: Editorial Porrua, S. A.</ref>

The current name of the site is believed to be relatively modern, but its origin has resisted attempts to be traced.<ref name=cause/>

==History==
Ka'Kabish is believed to have been initially occupied during the [[Mesoamerican chronology|Maya Late Pre-Classic Period]] (ca. 400 BCE- 200 CE) with one temple securely dated to this time and a second tentatively dated to this period.<ref name=cause>Haines, Helen R. 2008. "Causeway Terminus, Minor Centre, Elite Refuge, or Ritual Capital? Ka’Kabish, A New Puzzle on the Ancient Maya Landscape of North-Central Belize". In ''Research Reports in Belizean Archaeology'' Volume 5. Institute of Archaeology, NICH, Belize</ref> Material recovered from the tops of some of the buildings suggest that the city was in use at least until end of the [[Mesoamerican chronology#Classic Era|Classic Period]] (900 CE), while evidence from the residential zone surrounding the city indicates a thriving occupation as late as the end of the Early Post-Classic Period (1200 CE).<ref name=cause/>

==Site description==
The site has only recently become the focus of intensive investigation. A mid-1990s study of the site core revealed a total of 27 monumental structures arranged around two plazas, a subsequent study increased the number of structures to 55.<ref name=fp/>

Within several of these structures the looted remains of tombs belonging to high status, possibly royal individuals, were discovered. One of these tombs was found to have possessed [[Glyph|painted glyphs]]. The style is part of a tradition of painted tombs first noted at Rio Azul in Northern Guatemala.<ref>Adams, Richard E.W. 1999 ''Rio Azul: An Ancient Maya City''. University of Oklahoma Press, Norman.</ref>

==Archaeological Investigations==
Ka’Kabish was first visited by [[David M. Pendergast]] of the [[Royal Ontario Museum]] while he was working at the nearby site of [[Lamanai]], although the lack of a reliable road made work at the site unfeasible at that time. What was noted at the time was the evidence of wholesale illicit excavations, in which virtually every structure had been looted.<ref>Pendergast, David M. 1991 "And the Loot Goes On: Winning Some Battles, But Not the War." ''Journal of Field Archaeology'', vol. 18:89-95.</ref>

The site was identified initially for potential study in 1990 by members of the [[Maya Research Program]]. Due to potentially dangerous conditions in the area, a team did not return to Ka’Kabish until 1995, when formative mapping and surveying of the site was conducted.<ref name=":0" /><ref>Guderjan, Thomas H. 1996. "Kakabish: Mapping of a Maya Center". In ''Archaeological Research at Blue Creek, Belize. Progress Report of the Fourth (1995) Field Season'', edited by T. H. Guderjan, W. D. Driver and H. R. Haines, pp. 117-120. Maya Research Program, St. Mary's University, San Antonio.</ref>

Dr. Helen R. Haines (TUARC, [[Trent University]]) began establishing the groundwork for the Ka’Kabish Archaeological Research Project (KARP) in 2005, with the permission of Belize’s Institute of Archeology, a branch of the National Institute of Culture and History (NICH).<ref>{{Cite web|title = National Institute of Culture and History|url = http://nichbelize.org|website = nichbelize.org|accessdate = 2015-11-01}}</ref> KARP’s first field season was 2007, with a focus on clearing vegetation from, and remapping, the site’s South Complex. The North Complex of the site was remapped in 2009.<ref name=":0" />

Since its inception, the Ka'Kabish Archaeological Research Project has expended efforts on mapping the site to gain an understanding of the extent of the site and the types of buildings present.<ref name=fp>{{cite web|url=http://www.kakabish.org/field-project.html |title=Ka'Kabish Archaeological Research Project - Field Project |publisher=Kakabish.org |date= |accessdate=2010-10-09}}</ref> Knowledge of the architectural arrangements provides significant clues as to the importance of the site and the role it might have played in the larger Maya political landscape.<ref name=fp/> Under a grant from the [[Social Sciences and Humanities Research Council|Social Sciences and Humanities Research Council of Canada]], first excavations at Ka’Kabish began in 2010 and have continued in the intervening years, with excavation seasons in 2011, 2013, and 2015.<ref name=":0" />

As of 2013, survey and excavations have located 90 structures, located in 8 groups.<ref name=":0" /> Architectural features include two major temples, a ball court with circular ball court marker, and several large platforms (or range buildings) that likely served as royal or high status elite residences and/or administrative structures. Research has also been conducted on the numerous [[chultun]]s located at Ka’Kabish.

==Fundraising and Purchase==

Previously, the site sustained damage during the construction of the [[Indian Church, Belize|Indian Church]] to San Filipe Road, and two buildings were removed and their limestone material used for road fill for the road.<ref name=":1" /> Looting has also been a problem, and several structures have been destabilized through looter’s trenches.<ref>Tremain, Cara G. (2011) ''Investigations in Looters’ Trenches at Ka’Kabish, Northern Belize: An Analysis of Ancient Maya Architecture and Construction Practices'' (M.A. thesis.) Trent University.</ref>

Until 2015, the land on which the site is located was administered by the San Filipe Land Committee, and was owned by three different landowners.<ref name=":1" /> Though the owners were supportive of archaeological research, Ka’Kabish was in danger due to encroaching agricultural expansion. This prompted an attempt to purchase the land and, under the auspices of NICH, establish Ka’Kabish as a National Park. A fundraising campaign raised $20,030 CND of a needed $70,000 CND, providing enough money to make a down payment on the site in July 2015. The funds for the campaign were managed by [[Trent University]], and the land was successfully removed from the Agricultural Land Registry.<ref>{{Cite web|title = CLICK HERE to support Help Save an Ancient Maya Site & Rainforest Refuge|url = https://www.indiegogo.com/projects/help-save-an-ancient-maya-site-rainforest-refuge#/|website = Indiegogo|accessdate = 2015-11-01|language = en}}</ref>

==Fieldschool==

In 2013, field school credit for college archaeological students began being offered via [[Trent University]]. Students participating in the field school stay in the village of [[Indian Church, Belize|Indian Church]] during their time with the project.<ref>{{Cite web|title = Field School Information|url = http://www.kakabish.org/field-school-information.html|website = Ka'Kabish Archaeological Research Project|accessdate = 2015-11-01}}</ref>

== Sources ==
<references/>
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see [[WP:REFB]] for instructions on how to add citations. --->
*

==External links==
* [http://www.kakabish.org Ka'Kabish Archaeological Research Project]
* [https://twitter.com/kakabish1 Ka’Kabish Project Twitter Account]
* [https://www.indiegogo.com/projects/help-save-an-ancient-maya-site-rainforest-refuge#/ Ka’Kabish Indiegogo Campaign]
* [http://www.beyondtouring.com/Lamanai/lamanai_history.htm Beyondtouring.com]

{{Maya sites}}
{{coord missing|Belize}}

[[Category:Maya sites in Belize]]
[[Category:Former populated places in Belize]]
[[Category:Orange Walk District]]