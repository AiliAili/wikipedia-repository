{{Infobox artist
| bgcolour      = #6495ED
| name          = John Scarpati
| image         = John Scarpati.jpg
| imagesize     = 
| caption       = John Scarpatji
| birth_name     = 
| birth_date     = June 29, 1960
| birth_place    = Great Bend, Kansas
| death_date     = 
| death_place    = 
| nationality   = [[United States|American]]
| field         = [[Photographer]]
| training      = 
| movement      = 
| works         = [[Fishbone (EP)|Fishbone]] <br />[[Look What the Cat Dragged In]] <br />[[Hooked (Great White album)|Hooked]] <br />[[Cherry Pie (album)|Cherry Pie]]  <br />[[One Day It Will Please Us to Remember Even This]] <br /> [[Tight Rope (album)|Tight Rope]]<br />[[Folklore and Superstition]]
| patrons       = 
| influenced by = 
| influenced    = 
| awards        = 
| website       = [http://www.scarpati.com/ scarpati.com]
}}

'''John Scarpati''' (born June 29, 1960) is a professional photographer whose photography has appeared on hundreds of album and CD covers for bands and individual musicians.<ref name=ALLMUSIC>{{cite web |url= http://www.allmusic.com/artist/john-scarpati-mn0001782364 |title=Allmusic Guide Listing for John Scarpati |publisher= Allmuisc (Rovi) |accessdate=15 January 2013}}</ref><ref name=DISCOG>{{cite web |url= http://www.discogs.com/artist/John+Scarpati |title=Discog Listing for John Scarpati |publisher= Discog|accessdate=15 January 2013}}</ref>  He is also the owner of Scarpati Studio, a photography studio that does photography and layouts for advertising campaigns, some of which have won national and regional awards. Scarpati has produced two books based on his photography: Cramp, Slash, & Burn: When Punk and Glam Were Twins <ref>{{cite web |url=http://www.goodreads.com/book/show/16309964-cramp-slash-burn |title=Goodreads Catalog Reference for Cramp Slash & Burn |publisher= Goodeads Librarians |accessdate=15 January 2013}}</ref> and Eyes Wide Open.<ref>{{cite web |url=http://www.goodreads.com/book/show/17228354-eyes-wide-open |title=Goodreads Catalog Reference for Eyes Wide Open |publisher= Goodeads Librarians |accessdate=15 January 2013}}</ref> The first major solo art exhibit of Scarpat’s work was in1991 at Midem - Palais des Festivals in Cannes, France.<ref>{{cite web |url=http://www.metal-hammer.de/das-archiv/article195043/john-scarpati.html |title= Metal Hammer Article on Scarpati's Midem exhibit |first1= Metal Mike |date= 1 April 1991 |work= John Scarpati Hooked on Covers |publisher= Metal magazine |accessdate=15 January 2013}}</ref> The exhibit was a dye transfer print series. Scarpati’s work has also appeared in publications such as the [[New York Times]] and [[Rolling Stone Magazine]].<ref>{{cite web |url= https://www.nytimes.com/2010/10/08/us/08bcfilms.html |title= Showcases for Locally Made Documentaries |last1=HARMANCI | first1= REYHAN |date= 17 October 2010 |work= New York Times Article on Fishbone |publisher= The New York Times Company |accessdate=15 January 2013}}</ref>

Because of his extensive photography work with so many bands in Hollywood the 1980s, as well as the production of his book Cramp, Slash, & Burn: When Punk and Glam Were Twins, Alarm Magazine has referred to Scarpati as The Anthropologist of the Sunset Strip.<ref name="alarm-magazine">{{cite web |url=http://alarm-magazine.com/tag/fishbone/ |title= Cramp, Slash & Burn: Punk and glam through the lens of rock photographer John Scarpati |last1=Eddy | first1= Lincoln |date= 19 Oct 2012 |work= Alarm Magazine article on Cramp Slash & Burn |publisher= Alarm Magazine |accessdate=15 January 2013}}</ref>

==Career==

=== Music Industry ===

The majority of Scarpati’s photography work has been done in the music industry, having worked on hundreds of album covers and photo shoots across different genres. He has worked with many famous musical artists, such as [[Rush (band)|Rush]], [[Def Leppard]], [[Warrant (American band)|Warrant]] and [[Kenny Rogers]].

He has also done work for the following employers: [[Michael Jackson]] // [[Sherman Halsey]] // Planet Pictures // [[Delicious Vinyl]] // [[Dwight Yoakam]] // [[Hugh Syme]] // Fresh Design

Because of his extensive work with the band [[Fishbone]], Scarpati’s photos were featured in the documentary [[Everyday Sunshine]]. In addition, one of Scarpati’s photos of the band was sent to the Smithsonian’s [[National Museum of African American History and Culture]], which also has a collection of artifacts from the band.<ref>{{cite web |url= http://newsdesk.si.edu/releases/smithsonian-presents-screening-documentary-everyday-sunshine-story-fishbone |title =Smithsonian Presents Screening of Documentary Everyday Sunshine: The Story of Fishbone|date= 10 Nov 2011 |website=Newsdesk Newsroom of the Smithsonian|publisher=Smithsonian Institution |accessdate=12 August 2014}}</ref>

=== Advertising ===
In his work through his company Scarpati Studio and other advertising firms, Scarpati has worked as a photographer or art director on a number of advertising campaigns for corporate clients. His artistic approach has won a number of awards, including one national ADDY award <ref name = ADDY>{{cite web |url= http://www.aaf.org/default.asp?id=782 |title = List of ADDY Award Winners  |work= 2007 ADDY Awards |publisher= American Advertising Federation |accessdate=16 January 2013}}</ref> and two regional ADDY awards.<ref>{{cite web |url= http://www.aafnashville.com/news/2012/02/24/nashville-honors-47th-addy-winners.343473 |title = List of Regional 2011 ADDY Award Winners  |work= 2011 ADDY Awards |publisher= American Advertising Federation Nashville |accessdate=16 January 2013}}</ref>

A partial list of Scarpati’s Corporate clients include Bridgestone, Firestone, One Systems, Honda, AKG Microphones, Hunter Fans, Tequila Rose, Vantage Bowling, Fuzion, Paramount Studios, and English Eccentrics. In addition, his cover for the [[New York Dolls]] album [[One Day It Will Please Us To Remember Even This]] was used in an iPod Nano advertising campaign.<ref name = ipod>{{cite web |url= http://www.maclife.com/article/gallery/gallery_visual_history_ipod#slide-15 |title =  Gallery: A Visual History of the iPod |last1=Subramony | first1= Ambika |date= 16 May 2011 |work= MacLife Article on the History of iPod Advertising |publisher= MacLife |accessdate=16 January 2013}}</ref>

==Books==

Scarpati has produced two books based on his photography work.  This first, called Eyes Wide Open, is a collection of unusual and artistic images from his work. The Second is called Cramp, Slash, & Burn: When Punk and Glam Were Twins, and focuses on Scarpati's work in the Hollywood music scene in the 1980s.

=== Eyes Wide Open ===
The photography artwork in Eyes Wide Open began as an art show exhibit in alternative venues, such as clubs, bars, and hotels.<ref>{{cite web |url= http://altpick.com/news/4679 |title = Altpick Article |date= 14 Jul 2010 |work= Altpick Article on Eyes Wide Open |publisher= Altpick |accessdate=15 January 2013}}</ref>  While there may appear to be no obvious cohesive theme to the book, the concept is a collection of fine art prints on a wide array of [[substrate (printing)|Substrates]]. The collection contains a number of artistic examples of the photographer’s work and showcases his creativity.<ref>{{cite web |url=http://www.1stwebdesigner.com/inspiration/flash-portfolios-inspiration/ |title = Webdesigner Article |last1=Zennand | first1= Tina |date= 12 Sep 2010 |work= Webdesigner Article on Eyes Wide Open |publisher= Webdesigner |accessdate=15 January 2013}}</ref>

=== Cramp, Slash, & Burn: When Punk and Glam Were Twins ===
Cramp, Slash, & Burn: When Punk and Glam Were Twins is a collection of Scarpati’s photos from the Punk and Glam music industry in Hollywood during the 1980s. The photographs have been cleaned and digitized and the original film was wet drum scanned and digitally remastered to produce high quality images. The book is produced in a 12x12 format to mimic the size of traditional album covers. In addition, life size images of memorabilia from the period, including tick stubs, trinkets, and an authentic can of Aquanet from the time period. The text inside the book is written by band members and others who were a part of the scene at the time. The artwork from the book premiered at La Luz de Jesus Galley in Los Angeles<ref>{{cite web |url= http://www.laluzdejesus.com/shows/2012/Scarpati/Scarpati2012.htm |title = La Luz de Jesus Press release |date= 22 June 2012 |work= Press Release for Cramp Slash & Bur  Show |publisher= La Luz de Jesus Gallery |accessdate=15 January 2013}}</ref> and continues to be on display at various venues around the country.

The book won a Mohawk award <ref name="feltandwire">{{cite web |url=http://www.feltandwire.com/2012/09/14/mohawk-show-winner-cramp-slash-burn/ |title= Mohawk Show Winner Cramp, Slash & Burn | last1=Biederbeck | first1= Tom |date= 14 Sep 2012 |work= Felt & Wire Magazine article on Cramp Slash & Burn |publisher= Felt & Wire Magazine |accessdate=16 January 2013}}</ref> and earned Scarpati the title “Anthropologist of the Sunset strip from Alarm Magazine, which said, “As a photographer, John Scarpati is well known for his work on album covers, each a dynamic and living piece of art. During the 1980s in Los Angeles, he was the music photographer, an anthropologist of the Sunset Strip, his lens documenting the fashion, sounds, and faces in the era of outrageous musical fashion.” <ref name="alarm-magazine" /> The Indie Reader, Culture Catch, and Felt and Wire also praised the accuracy of the book in documenting the era and scene.<ref name="feltandwire" /><ref>{{cite web |url=http://culturecatch.com/dusty/mick-rock-john-scarpati|title= Rock and Roll Ain't Noise Pollution | last1=Wright | first1= Dusty |date= 24 Jul 2012 |work= Culture Catch article on Cramp Slash & Burn |publisher= Culture Catch |accessdate=16 January 2013}}</ref>

==Album cover art==
Chronological list of John Scarpati's album cover art.
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%;"
|- bgcolor="#B0C4DE" align="center"
! Year
! Artist
! Album
! Role
|-
| 1984
| [[Steve Perry (musician)|Steve Perry]]
| [[Street Talk]]
| Photography
|-
| 1985
| [[Fishbone]]
| [[Fishbone (EP)|Fishbone]]
| Photography
|-
| 1985
| [[Tex and the Horseheads|Tex & The Horseheads]]
| Life’s So Cool
| Photography
|-
| 1985
| Various
| [[Enigma Variations (album)|The Enigma Variations]]
| Photography
|-
| 1985
| [[Stryper]]
| [[Soldiers Under Command]]
| Photography
|-
| 1985
| [[Stryper]]
| [[Reason for the Season|Reason For The Season]]
| Photography
|-
| 1986
| [[T.S.O.L]]
| [[Revenge (T.S.O.L. album)|Revenge]]
| Photography
|-
| 1986
| [[Fishbone]]
| [[In Your Face (Fishbone album)|In Your Face]]
| Photography
|-
| 1986
| [[Poison (American band)|Poison]]
| [[Look What the Cat Dragged In]]
| Photography
|-
| 1986
| Eyes
| Eyes
| Photography
|-
| 1986
| [[The Unforgiven (band)|The Unforgiven]]
| The Unforgiven
| Photography
|-
| 1987
| [[Redd Kross]]
| [[Neurotica]]
| Photography
|-
| 1987
| [[Dramarama]]
| [[Box Office Bomb (album)|Box Office Bomb]]
| Photography
|-
| 1988
| [[L.A. Guns]]
| [[L.A. Guns (album)|L.A. Guns]]
| Photography
|-
| 1988
| [[Kix (band)|Kix]]
| [[Blow My Fuse]]
| Photography
|-
| 1988
| [[Social Distortion]]
| Prison Bound
| Album Design
|-
| 1988
| [[Oingo Boingo]]
| [[Boingo Alive]]
| Photography
|-
| 1988
| Various Artists
| [[A Town South of Bakersfield]] Vol 1
| Photography
|-
| 1989
| [[Warrant (American band)|Warrant]]
| [[Dirty Rotten Filthy Stinking Rich]]
| Photography
|-
| 1989
| [[Kingdom Come (band)]]
| [[In Your Face (Kingdom Come album)|In Your Face]]
| Photography
|-
| 1989
| [[Michael Monroe]]
| [[Not Fakin' It]]
| Photography
|-
| 1989
| [[Dramarama]]
| [[Stuck in Wonderamaland]]
| Photography
|-
| 1989
| [[Rush (band)|Rush]]
| [[Presto (album)|Presto]]
| Photography
|-
| 1989
| [[Babylon A.D. (band)|Babylon A. D.]]
| [[Babylon A. D. (album)|Babylon A. D.]]
| Photography
|-
| 1989
| [[Bonfire (band)]]
| [[Point Blank (Bonfire album)|Point Blank]]
| Photography
|-
| 1989
| [[Fates Warning]]
| [[Perfect Symmetry (Fates Warning album)|Perfect Symmetry]]
| Photography
|-
| 1990
| [[Magnum (band)|Magnum]]
| [[Goodnight L.A.]] 
| Photography
|-
| 1990
| [[Rush (band)|Rush]]
| [[Chronicles (Rush album)|Chronicles]]
| Photography
|-
| 1990
| [[Warrant (American band)|Warrant]]
| [[Cherry Pie (album)|Cherry Pie]]
| Photography
|-
| 1990
| [[Hurricane (band)|Hurricane]]
| [[Slave to the Thrill]]
| Photography
|-
| 1990
| [[The Northern Pikes]]
| [[Snow in June]]
| Photography
|-
| 1990
| [[Alias (band)|Alias]]
| [[Alias (album)|Alias]]
| Photography
|-
| 1990
| [[Blowfly (musician)|Blowfly]]
| Twisted World of Blowfly
| Photography
|-
| 1990
| [[Electric Angels]]
| Electric Angels
| Photography
|-
| 1990
| [[Social Distortion]]
| [[Social Distortion (album)|Social Distortion]]
| Photography
|-
| 1991
| [[Rush (band)|Rush]]
| [[Roll the Bones]]
| Photography
|-
| 1991
| [[Great White]]
| [[Hooked (Great White album)|Hooked]]
| Photography
|-
| 1991
| [[Fates Warning]]
| [[Parallels (album)|Parallels]]
| Photography
|-
| 1991
| [[Kix (band)|Kix]]
| [[Hot Wire (Kix album)|Hot Wire]]
| Photography
|-
| 1991
| [[Bad English]]
| [[Backlash (Bad English album)|Backlash]]
| Photography
|-
| 1991
| The Storm
| The Storm
| Photography
|-
| 1991
| [[Armored Saint]]
| [[Symbol of Salvation]]
| Photography
|-
| 1991
| [[Aldo Nova]]
| [[Blood on the Bricks (Aldo Nova album)|Blood on the Bricks]]
| Photography
|-
| 1991
| [[Me Phi Me]]
| One
| Photography
|-
| 1991
| [[Dramarama]]
| [[Vinyl (Dramarama album)|Vinyl]]
| Photography
|-
| 1991
| [[The Scream (band)|The Scream]]
| [[Let It Scream]]
| Photography
|-
| 1992
| [[Warrant (American band)|Warrant]]
| [[Dog Eat Dog (Warrant album)|Dog Eat Dog]]
| Photography
|-
| 1992
| [[Steve Perry (musician)|Steve Perry]]
| [[Greatest Hits + Five Unreleased]]
| Photography
|-
| 1992
| [[21 Guns (band)|21 Guns]]
| [[Salute (21 Guns album)|Salute]]
| Photography
|-
| 1993
| [[Dramarama]]
| [[Hi-Fi Sci-Fi]]
| Photography
|-
| 1995
| [[Bill Miller (musician)|Bill Miller]]
| Raven in the Snow
| Photography
|-
| 1995
| [[Marshall Chapman]]
| It's About Time
| Photography
|-
| 1995
| [[Philip Claypool]]
| A Circus Leaving Town
| Photography
|-
| 1996
| [[Diamond Rio]]
| [[IV (Diamond Rio album)|IV]]
| Photography
|-
| 1996
| [[Dramarama]]
| [[The Best of Dramarama: 18 Big Ones]]
| Photography
|-
| 1996
| [[Warrant (American band)|Warrant]]
| [[The Best of Warrant]]
| Photography
|-
| 1996
| [[Al Anderson (NRBQ)|Al Anderson]]
| Pay Before You Pump
| Photography
|-
| 1996
| [[Jason and the Scorchers]]
| Clear Impetuous Morning
| Photography
|-
| 1997
| [[Eddie Rabbitt]]
| [[Beatin' the Odds (Eddie Rabbitt album)|Beatin’ the Odds]]
| Photography
|-
| 1997
| [[Scorpions (band)|Scorpions]]
| Deadly Sting: The Mercury Years
| Photography
|-
| 1997
| Rick Altizer
| Blue Plate Special
| Photography
|-
| 1998
| [[The Thompson Brothers Band]]
| Blame It on the Dog
| Photography
|-
| 1998
| [[Pam Tillis]]
| [[Every Time (album)|Every Time]]
| Photography
|-
| 1998
| [[Eddie Rabbitt]]
| [[Songs from Rabbittland]]
| Photography
|-
| 1998
| [[The Insyderz]]
| [[Fight of My Life]]
| Photography
|-
| 1999
| [[Brooks & Dunn]]
| [[Tight Rope (album)|Tight Rope]]
| Photography
|-
| 1999
| [[Jetboy (band)|Jetboy]]
| Lost and Found
| Photography
|-
| 2000
| [[The Kentucky Headhunters]]
| [[Songs from the Grass String Ranch|Songs From the Grass String Ranch]]
| Photography
|-
| 2000
| [[Ray Price (musician)|Ray Price]]
| Prisoner of Love
| Photography
|-
| 2000
| [[The Sky Kings]]
| From Out of the Blue
| Photography
|-
| 2001
| [[Rush (band)|Rush]]
| Chronicles/ Moving Pictures
| Photography
|-
| 2001
| [[Danni Leigh]]
| Divide and Conquer
| Photography
|-
| 2002
| [[Oingo Boingo]]
| 20th Century Masters - The Millennium Collection
| Photography
|-
| 2003
| [[Social Distortion]]
| Greatest Hits
| Photography
|-
| 2003
| [[Buddy Jewell]]
| [[Buddy Jewell (album)|Buddy Jewell]]
| Photography
|-
| 2003
| [[Jamie Lee Thurston]]
| I Just Wanna Do My Thing
| Photography
|-
| 2003
| [[Janis Ian]]
| Working Without a Net
| Photography
|-
| 2004
| [[Brad Cotter]]	
| [[Patient Man]]
| Photography
|-
| 2004
| [[Restless Heart]]
| [[Still Restless]]
| Photography
|-
| 2004
| [[Lane Turner]]
| Wanting More
| Photography
|-
| 2005
| [[Jo Dee Messina]]
| [[Delicious Surprise]]
| Photography
|-
| 2005
| [[Hank Williams, Jr.]]
| [[That's How They Do It in Dixie: The Essential Collection|That's How They Do It in Dixie]]
| Photography
|-
| 2006
| [[New York Dolls]]
| [[One Day It Will Please Us to Remember Even This]]
| Photography
|-
| 2006
| Mechanical Birds
| The Possibility of Flight
| Photography
|-
| 2007
| [[Social Distortion]]
| Greatest Hits
| Photography
|-
| 2007
| Hydrogyn
| Deadly Passions
| Photography
|-
| 2007
| Randy Kohrs
| Old Photograph
| Photography
|-
| 2007
| [[Clay Walker]]
| [[Fall (album)|Fall]]
| Photography
|-
| 2008
| [[De Novo Dahl]]
| Move Every Muscle, Make Every Sound
| Photography
|-
| 2008
| [[Black Stone Cherry]]
| [[Folklore and Superstition]]
| Photography
|-
| 2009
| [[Steadlür]]
| [[Steadlür (album)|Steadlür]]
| Photography
|-
| 2009
| Doyle and Debbie
| Performing all their top hits to the very best of their ability
| Photography
|-
| 2009
| Korby Lenker
| Lovers and Fools
| Photography
|-
| 2010
| Randy Kohrs
| Quicksand
| Photography
|-
| 2012
| Craig Morrison
| Craig Morrison
| Photography
|-
| 2012
| [[Deborah Allen]]
| Hear Me Now
| Photography
|-
| 2012
| Fishbone
| Every Day Sunshine
| Photography
|-
| 2012
| [[Redd Kross]]
| Researching The Blues
| Photo editing
|-
| 2012
| Korby Lenker
| Korby Lenker
| Photography
|-
|}

==Awards and Notable Achievements==

=== Gold & Platinum Records ===
[[Steve Perry (musician)|Steve Perry]] - [[Street Talk]]<ref name=ALLMUSIC /><ref name=DISCOG /><br />
[[Poison (American band)|Poison]] - [[Look What the Cat Dragged In]]<ref name=ALLMUSIC /><ref name=DISCOG /><br />
[[L.A. Guns]] – [[L.A. Guns (album)|L.A. Guns]]<ref name=ALLMUSIC /><ref name=DISCOG /><br />
[[Great White]] - [[Hooked (Great White album)|Hooked]]<ref name=ALLMUSIC /><br />
[[Warrant (American band)|Warrant]] – [[Dirty Rotten Filthy Stinking Rich]]<ref name=ALLMUSIC /><ref name=DISCOG /><br />
[[Warrant (American band)|Warrant]] - [[Cherry Pie (album)|Cherry Pie]]<ref name=ALLMUSIC /><br />
[[Rush (band)|Rush]] – [[Roll The Bones]]<ref name=ALLMUSIC /><br />
[[Kix (band)|Kix]] – Blow My Fuse<ref name=ALLMUSIC /><br />
[[Rush (band)|Rush]] – [[Presto (album)|Presto]]<ref name=ALLMUSIC /><ref name=DISCOG /><br />
[[Brooks & Dunn]] – [[Tight Rope (album)|Tight Rope]]<ref name=ALLMUSIC /><br />
[[Buddy Jewell]] – [[Buddy Jewell (album)|Buddy Jewell]]<ref name=ALLMUSIC />

=== Notable Achievements and Recognition ===
1984 - Scarpati’s 1st album cover to go Platinum – [[Steve Perry (musician)|Steve Perry]] / [[Street Talk]]: Columbia<ref name=ALLMUSIC /><ref name=DISCOG /><br />
1989 - Shot 1st music video – [[Tone Lōc]] / [[Wild Thing (Tone Lōc song)|Wild Thing]] for Delicious Vinyl /camera operator DP<br />
1990 - [[Juno Award]] – Best album cover – Rush / Presto - Atlantic<ref name="junoawards">{{cite web |url=http://junoawards.ca/awards/artist-summary/?artist_name=rush&submit=Search |title = Rush Juno Awards  |year= 2012 |work= Juno Awards Database Listings |publisher= The Canadian Academy of Recording Arts and Sciences |accessdate=18 January 2013}}</ref><br />
1991 - Worst album cover – [[Rolling Stone]] Magazine – Warrant [[Cherry Pie (album)|Cherry Pie]] – Columbia<br />
1992 - [[Juno Award]] – Best album cover – [[Rush (band)|Rush]] / [[Roll the Bones]] – Atlantic<ref name="junoawards" /><br />
2005 - [[Honda]] Ad Campaign featured in Luerzer's Archive<br />
2006 - [[iTunes]] podcast – interview with Scarpati by light source<br />
2007 - [[Addy Awards|Silver Addy]] “national” Design CD/DVD packaging – Mechanical Birds<br />
2008 - [[Addy Awards|Silver Addy]] – photography  print Campaign “local” – [[De Novo Dahl]] - CD packaging<br />
2008 - [[Apple Inc.|Apple]] [[iPod]] Nano “TV & Print” New York Dolls cover featured in advertising campaign<ref name = ipod /><br />
2009 - [[Shutterbug (magazine)|Shutterbug]] Magazine – feature article (March) Pro’s Choice<br />
2010 – Featured article PDN magazine – Scarpati Rocks the House – Legends of Photography Issue<br />
2011 - 2 full page spread  - [[Sports Illustrated]] swimsuit edition – [[Bridgestone]] Tires Campaign

==References==
{{reflist}}

== External links ==
* [http://www.scarpati.com/ Scarpati Studios Website]
* [http://www.crampslashandburn.com/ Cramp Slash & Burn Website]
* [https://www.facebook.com/crampslashandburn/ Cramp Slash & Burn on Facebook]
* [http://scarpati.com/exhibition/ Eyes Wide Open]
* [http://www.blurb.com/user/scarpati/ Scarpati on Blurb]
* [http://www.etsy.com/people/scarpatistudio/ Scarpati on Etsy]
* [http://www.linkedin.com/in/scarpatistudio// Scarpati's LinkedIn Profile]

{{DEFAULTSORT:Scarpati, John}}
[[Category:American photographers]]
[[Category:Living people]]
[[Category:1960 births]]