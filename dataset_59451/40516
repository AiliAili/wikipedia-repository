{{Use dmy dates|date=April 2017}}
{{Use British English|date=April 2017}}
{|{{Infobox ship begin}}
{{Infobox ship image
| Ship image= 
| Ship caption= 
}}
{{Infobox ship career
| Hide header= 
| Ship country=
| Ship flag= [[File:Naval Ensign of the United Kingdom.svg|60px|Royal Navy Ensign]]
| Ship name=HMS ''Wren''
| Ship ordered=April 1918
| Ship builder= [[Yarrow Shipbuilders|Yarrow Shipbuilders Limited]]
| Ship laid down=June 1918
| Ship launched=11 November 1919
| Ship acquired= 
| Ship commissioned=23 January 1923
| Ship decommissioned= 
| Ship in service= 
| Ship out of service= 
| Ship struck= 
| Ship reinstated= 
| Ship fate= Sunk 27 July 1940 by air attack
| Ship honours=*Atlantic 1939-400
*Norway 1940
| Ship badge= On a Field Blue, a Wren on a branch, all Gold.
| Ship motto=*Ex parvalis magna
*"From Small Things (come) Great"
| Ship identification=[[Pennant number]]s D76 and I76
| Ship notes= 
}}
{{Infobox ship characteristics
| Hide header= 
| Header caption= 
| Ship class=[[V and W class destroyer|Admiralty modified W class]] [[destroyer]]
| Ship displacement=1,140&nbsp;tons standard, 1,550&nbsp;tons full
| Ship length= 312&nbsp;ft [[Length overall|o/a]], 300&nbsp;ft [[Length between perpendiculars|p/p]]
| Ship beam= {{convert|29.5|ft|m}}
| Ship draught= {{convert|9|ft|m}}, {{convert|11.25|ft|m}} under full load
| Ship propulsion=*3 × [[Yarrow boiler|Yarrow type]] Water-tube [[boilers]]
*Brown-Curtis geared [[steam turbines]] driving 2 shafts producing 27,000&nbsp;shp
| Ship speed= 34&nbsp;[[knot (unit)|kn]]
| Ship range=*320-370&nbsp;tons oil
*3,500&nbsp;[[nautical mile|nmi]] at 15&nbsp;kn
*900&nbsp;nmi at 32&nbsp;kn
| Ship complement= 127
| Ship sensors= 
| Ship EW= 
|Ship armament=*4 × [[BL 4.7 inch /45 naval gun|BL 4.7 in (120-mm) Mk.I guns]], mount P Mk.I
*2 × [[QF 2 pounder naval gun|QF 2 pdr Mk.II "pom-pom" (40 mm L/39)]]
*6 × [[British 21 inch torpedo|21-inch Torpedo Tubes]]
| Ship armour= 
| Ship aircraft= 
| Ship aircraft facilities=
| Ship notes= 
}}
{{Infobox ship characteristics
| Hide header=
| Header caption= War Modifications
| Conversion Date=
| Ship complement= 134
| Ship sensors=
| Ship EW= 
|Ship armament=*3 × BL 4.7 in (120mm) Mk.I L/45 guns
*1 × [[QF 12 pounder 12 cwt naval gun]]
*2 × QF 2 pdr Mk.II "pom-pom" (40 mm L/39)
*3 × 21-inch Torpedo Tubes (one triple mount)
*2 × depth charge racks
| Ship armour= 
| Ship aircraft= 
| Ship aircraft facilities=
| Ship notes= 
}}
{{Infobox service record
|is_ship=yes
|label= 
|partof=*4th Destroyer Flotilla – 1923
*16th Destroyer Flotilla – Sep 1939
|codes=
|commanders=
|operations=[[World War II]] 1939 to 1945
|victories=None
|awards=
}}
|}
'''HMS ''Wren'' (D88/I88)''' was an [[V and W class destroyer|Admiralty modified W class]] [[destroyer]] built for the [[Royal Navy]].  She was ordered in April 1918 from [[Yarrow Shipbuilders|Yarrow Shipbuilders Limited]] under the 13th Order for Destroyers of the Emergency War Program of 1918-19. She was the third Royal Navy ship to carry the name, which was introduced in 1653.<ref name="www.naval-history.net">{{cite web|title=Service Histories of Royal Navy Warships in World War 2|url=http://www.naval-history.net/xGM-Chrono-10DD-09VW-Wren.htm}}</ref>

== Construction ==
HMS ''Wren''{{'}}s keel was laid in June 1918 at the Yarrow Shipbuilders Limited, [[Scotstoun]].  The [[Armistice with Germany|signing of the armistice with Germany]] led to the cancellation of 35 destroyers out of ''Wren'''s class of 56, but she survived this and the cancellation of a further seven vessels in September 1919.  ''Wren'' was launched on 11 November 1919 after which she was towed to the dockyard at [[Pembroke Dock]] for completion.<ref name="www.naval-history.net"/><ref name="www.pbenyon1.plus.com">{{cite web|title=Jane's Fighting Ships © for 1919|url=http://www.pbenyon1.plus.com/Janes_1919/Destroyers/Admiralty-V_Post_War.html}}</ref>

== Inter War Years ==
After a delay, she was commissioned into the [[Royal Navy]] on 23 January 1923 with [[pennant number]] D88.  After commissioning she was assigned to the [[4th Destroyer Flotilla]] of the [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]].  She served mainly in home waters and in 1938 was assigned as rescue ship for the [[Home Fleet]] carriers.<ref name="www.naval-history.net"/>

== Second World War ==
In September 1939 ''Wren'' was transferred to the [[16th Destroyer Flotilla]] at Portsmouth for convoy defence and anti-submarine patrols in the English Channel and [[Southwest Approaches]]. In November she was transferred to [[Nore Command]] for convoy defence in the [[North Sea]].<ref name="www.naval-history.net"/> ''Wren'' was re-assigned to the [[18th Destroyer Flotilla]], [[Western Approaches Command]] in January 1940 and undertook convoy defence once more in the Southwest Approaches.  Following the [[German invasion of Norway]] in April 1940 she transferred to [[Scapa Floe]] to carry out convoy escort duties to Norway. She provided gunfire support to the British landings at [[Bjerkvik]] but returned to convoy duties and did not take part in the evacuation of the British expeditionary force. At the end of May her pennant number was changed to I88 for visual signalling purposes.<ref name="www.naval-history.net"/> On 25 June she returned to the 16th Destroyer Flotilla based at Harwich for convoy escort and patrol duties in Nore Command.<ref name="www.naval-history.net"/>

==Loss==
On 27 July 1940 ''Wren'', alongside [[HMS Montrose (D01)|''Montrose'']], was providing anti-aircraft protection for minesweeping operations off [[Aldeburgh]], Suffolk.  She came under heavy and sustained dive bombing attack by 15 [[Junkers Ju-87]] aircraft and was damaged by several near misses which holed her below the waterline.  Collapsed [[Bulkhead (partition)|bulkheads]] caused heavy flooding which led her to sink quickly, killing 37 of her crew.  ''Wren'''s survivors were rescued by the minesweepers.<ref name="www.naval-history.net"/>

== Notes ==
{{Reflist}}

==Bibliography==
*{{cite book|last=Campbell|first=John|title=Naval Weapons of World War II|year=1985|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-459-4}}
*{{cite book|title=Conway's All the World's Fighting Ships 1922–1946|editor1-last=Chesneau|editor1-first=Roger|publisher=Conway Maritime Press|location=Greenwich, UK|year=1980|isbn=0-85177-146-7}}
*{{Colledge}}
* {{cite book |first1=Maurice |last1=Cocker |first2=Ian |last2=Allan |title=Destroyers of the Royal Navy, 1893-1981 |isbn=0-7110-1075-7}}
* {{cite book|last=Friedman|first=Norman|title=British Destroyers From Earliest Days to the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2009|isbn=978-1-59114-081-8}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5|lastauthoramp=y}}
* {{cite book|last=Lenton|first=H. T.|authorlink=Henry Trevor Lenton|title=British & Empire Warships of the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1998|isbn=1-55750-048-7}}
*{{cite book|last=March|first=Edgar J.|title=British Destroyers: A History of Development, 1892-1953; Drawn by Admiralty Permission From Official Records & Returns, Ships' Covers & Building Plans|year=1966|publisher=Seeley Service|location=London |OCLC=164893555}}
* {{cite book |last=Preston |first=Antony |title='V & W' Class Destroyers 1917-1945 |publisher=Macdonald |location=London |year=1971 |oclc=464542895}}
* {{cite book |last=Raven |first=Alan |last2=Roberts|first2=John  |title='V' and 'W' Class Destroyers |publisher=Arms & Armour |location=London |year=1979 |series=Man o'War |volume=2 |isbn=0-85368-233-X|lastauthoramp=y }}
* {{cite book|last=Rohwer|first=Jürgen|title=Chronology of the War at Sea 1939-1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2005|edition=Third Revised|isbn=1-59114-119-2}}
* {{cite book |last=Whinney |first=Bob |title=The U-boat Peril: A Fight for Survival |publisher=Cassell |year=2000 |isbn=0-304-35132-6}}
* {{cite book|last=Whitley|first=M. J.|title=Destroyers of World War 2|publisher=Naval Institute Press|date=1988|isbn=0-87021-326-1|location=Annapolis, Maryland}}
* {{cite book|last=Winser|first=John de D.|title=B.E.F. Ships Before, At and After Dunkirk|publisher=World Ship Society|location=Gravesend, Kent|year=1999|isbn=0-905617-91-6}}

== External links ==
* Service History of HMS ''Wren'' compiled by the late LtCdr Geoffry B. Mason, RN (Rtd) and can be found at [http://www.naval-history.net/index.htm Naval History Web Site]
* [http://uboat.net:8080/allies/warships/ship/4253.html U-Boat.net]
* A list of her wartime Commanding Officers can be found at [http://uboat.net:8080/allies/warships/ship/4253.html U-Boat.net]

{{V and W class destroyer}}
{{July 1940 shipwrecks}}

{{DEFAULTSORT:Wren}}
[[Category:V and W-class destroyers of the Royal Navy]]
[[Category:1919 ships]]
[[Category:World War II destroyers of the United Kingdom]]
[[Category:Clyde-built ships]]
[[Category:Maritime incidents in July 1940]]