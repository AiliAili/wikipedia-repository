[[Image:Cocaine Anonymous logo.JPG|150px|thumb|right|Cocaine Anonymous Logo]] '''Cocaine Anonymous''' ('''CA''') is a [[twelve-step program]] for people who seek recovery from drug [[Substance dependence|addiction]]. CA is patterned very closely after [[Alcoholics Anonymous]], although the two groups are unaffiliated.  While many CA members have been addicted to cocaine, crack, speed or similar substances, identifying specifically as a [[cocaine]] addict is not required.<ref name="CAMIND2007">{{cite web|author=Cocaine Anonymous |title=And All Other Mind-Altering Substances |accessdate=2007-11-15 |date=2007-11-13 |url=http://www.ca.org/literature/allothermas.htm |deadurl=yes |archiveurl=https://web.archive.org/web/20090304202508/http://www.ca.org:80/literature/allothermas.htm |archivedate=2009-03-04 |df= }}</ref>

CA uses the book ''Alcoholics Anonymous''<ref name="BIGBOOK">{{cite book | last = Alcoholics Anonymous | title = Alcoholics Anonymous | publisher = Alcoholics Anonymous World Services | date = 1976-06-01 | isbn = 0-916856-59-3 | oclc = 32014950}}</ref> as its basic text.  Complementing this are the CA Storybook, ''Hope, Faith and Courage: Stories from the Fellowship of Cocaine Anonymous''.<ref name="CASTORYBOOK">{{cite book |author=Cocaine Anonymous |title=Hope, Faith and Courage: Stories from the Fellowship of Cocaine Anonymous |publisher=Cocaine Anonymous World Services |isbn=0-9638193-1-3 |date= January 1993 |oclc=32014453 |location=[[Los Angeles, California]]}}</ref> and the [[Alcoholics Anonymous|AA]] book ''Twelve Steps and Twelve Traditions''<ref name="12AND12">{{cite book | last = Alcoholics Anonymous | title = Twelve Steps and Twelve Traditions | publisher = [[Hazelden]] | date = 2002-02-10 | isbn = 0-916856-01-1 | oclc = 13572433}}</ref>

CA was formed in [[Los Angeles]] in 1982 by a long-standing AA member. He worked in the film industry and saw a number of people who had difficulty finding help from anyone knowledgeable about the special difficulties presented by cocaine addiction. '''Co-Anon''' (formerly '''CocAnon''') is a program for families of cocaine users, analogous to [[Al-Anon]] for the friends and family of alcoholics.<ref name="COHEN1985">{{cite book |last=Cohen |first=Sidney |title=The Substance Abuse Problems |year=1985 |publisher=[[Haworth Press]]  |location=[[New York, New York]] |isbn=0-86656-368-7 |oclc=6666765}}</ref>

== See also ==
* [[Addiction recovery groups]]
* [[List of twelve-step groups]]

== References ==
{{reflist}}

== Further reading ==
*{{cite journal |author1=Crits-Christoph, P. |author2=Gibbons, M. B. C. |author3=Barber, J. P. |author4=Gallop, R. |author5=Beck, A. T. |author6=Mercer, D. |date=2003-11-01 |title=Mediators of outcome of psychosocial treatments for cocaine dependence |journal=Journal of Consulting and Clinical Psychology |volume=71 |issue=5 |pages=918–925 |doi=10.1037/0022-006X.71.5.918 |pmid=14516240|display-authors=etal}}
*{{cite journal |author1=Maude-Griffin, P. M. |author2=Hohenstein, J. M. |author3=Humfleet, G. L. |author4=Reilly, P. M. |author5=Tusel, D. J. |author6=& Hall, S. M. |date=1997-11-01 |title=Superior efficacy of cognitive-behavioral therapy for urban crack cocaine abusers: Main and matching effects |journal=Journal of Consulting and Clinical Psychology |volume=66 |issue=5 |pages=832–837 |doi=10.1037/0022-006X.66.5.832 |pmid=9803702}}
*{{cite journal |author1=Weiss, R. D. |author2=Griffin, M. L. |author3=Gallop, R. J. |author4=Najavits, L. M. |author5=Frank, A. |author6=Crits-Christoph, P. |date=2005-02-01 |title=The effect of 12-step self-help group attendance and participation on drug use outcomes among cocaine-dependent patients |journal=Drug and Alcohol Dependence |issue=2 |pages=177–184 |doi=10.1016/j.drugalcdep.2004.08.012 |volume=77 |pmid=15664719|display-authors=etal}}

== External links ==
*[http://www.ca.org/ CA World Services website]
*[http://www.ca-online.org/ Online meeting website]
*[http://www.cauk.org.uk/ Official UK website]
*[http://www.co-anon.org/ Co-Anon] for friends and family of addicts
*{{worldcat id|id=lccn-n95-3188}}

[[Category:Cocaine]]
[[Category:Organizations established in 1982]]
[[Category:Twelve-step programs]]
[[Category:Non-profit organizations based in Los Angeles]]
[[Category:Addiction and substance abuse organizations]]