{|{{Infobox aircraft begin
 |name=BG
 |image=Great Lakes BG-1 VB-3 NAN3-88.jpg
 |caption=Great Lakes BG-1 of bombing squadron VB-3B
}}{{Infobox aircraft type
 |type=[[Dive-bomber]]
 |manufacturer=[[Great Lakes Aircraft Company]]
 |designer=
 |first flight=1933
 |introduced=1934
 |retired=1941
 |status=
 |primary user=[[United States Navy]]
 |more users=[[United States Marine Corps]]
 |produced=
 |number built=61
 |variants with their own articles=
}}
|}

The '''Great Lakes BG''' was an [[United States of America|American]] [[aircraft carrier|carrier]]-based [[dive bomber]] of the 1930s. Designed and built by the [[Great Lakes Aircraft Company]] of [[Cleveland, Ohio]], 61 were used by the [[United States Navy]] and [[United States Marine Corps]] from 1934 to 1940.

==Development and design==
The Great Lakes Aircraft Company, who had previously built 18 TG-1 and 32 TG-2 variants of the [[Martin T4M]],<ref name="Swan navyp312">Swanborough and Bowers 1976, p.312.</ref> received an order from the [[United States Navy|U.S. Navy]] for a prototype two seat [[dive bomber]] capable of carrying a 1,000&nbsp;lb (454&nbsp;kg) bomb in 1933.<ref name="Swan navyp193">Swanborough and Bowers 1976, p.193.</ref> (This compared with contemporary Scout Bombers such as the [[Vought SBU]] and the [[SBC Helldiver|Curtiss SBC Helldiver]], also capable of dive bombing, which had  bombloads of 500&nbsp;lb (227&nbsp;kg)).

The resulting design was a single engined [[biplane]] with single bay, unequal span tapered wings and a fixed tailwheel undercarriage. The aircraft was powered by a {{convert|750|hp|abbr=on}} [[Pratt & Whitney]] [[Pratt & Whitney R-1535|Twin Wasp Junior]] [[radial engine]].<ref name="Donald World p467">Donald 1997, p.467.</ref>

The prototype '''XBG-1''' was completed in mid-1933 and evaluated against the competing [[Consolidated XB2Y|Consolidated XB2Y-1]], proving superior. As a result, in November 1933, orders were placed for production of the aircraft as the '''BG-1''', which was fitted with a canopy over the [[cockpit]]s for the two crew, in place of the open cockpits of the prototype. A total of 61 of these aircraft were built, including the prototype.<ref name="Donald World p467"/><ref name="Grossnickp461">Grossnick 1995, p.461.</ref>

==Operational history==
[[File:Great Lakes-bg1 - in flight.jpg|thumb|BG-1s of VB-7]]

The BG-1 entered service in October 1934, equipping VB-3B (later re-designated VB-4) aboard the carriers {{USS|Ranger|CV-4|2}} and {{USS|Lexington|CV-2|2}}.<ref name="Grossnickp53">Grossnick 1995, p.51.</ref> It was also operated by the [[United States Marine Corps|Marine Corps]], equipping two squadrons from 1935.<ref name="Swan navyp.193-194">Swanborough and Bowers 1976, p.193-194.</ref>

The BG-1 continued in front line use with the Navy until 1938, and with the Marines Corps until 1940.<ref Name="Swan Navyp194"/> It was used for utility duties at shore bases until June 1941.<ref name="Grossnickp461"/>

==Variants==
[[File:Great Lakes XB2G-1 in flight 1936.jpg|thumb|The XB2G-1 with a retractable landing gear, 1936.]]
;XBG-1
:Prototype. Open cockpit and powered by a [[Pratt & Whitney R-1535]]-64 radial engine, one built.<ref name="Orbis p1999">Orbis 1985, p.1999</ref>
;BG-1
:Production version with enclosed cockpit and powered by a [[Pratt & Whitney R-1535]]-82 radial engine, 60 built.<ref name="Orbis p1999" />
;XB2G-1
:Developed version with retractable undercarriage and an enclosed bomb bay. One prototype only.<ref name="Donald  World p468">Donald 1997, p.468.</ref>

==Operators==
;{{USA}}
*[[United States Marine Corps]]
*[[United States Navy]]

==Specifications (BG-1)==
[[File:Great Lakes BG-1 VB-7.jpg|thumb|BG-1, showing the single bay wings.]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=United States Navy Aircraft since 1914 <ref Name="Swan Navyp194">Swanborough and Bowers 1976, p.194</ref>
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=Two
|capacity=
|length main= 28 ft 9 in
|length alt= 8.77 m
|span main= 36 ft 0 in
|span alt= 10.98 m
|height main= 11 ft 0 in
|height alt= 3.35 m
|area main= 384 ft²
|area alt= 35.7 m²
|airfoil=
|empty weight main= 3,903 lb
|empty weight alt= 1,774 kg
|loaded weight main= 6,347 lb
|loaded weight alt= 2,885 kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|more general=
|engine (prop)=[[Pratt & Whitney R-1535]]-82 Twin Wasp Junior
|type of prop=14-cylinder, two row air cooled [[radial engine]]
|number of props=1
|power main= 750 hp
|power alt= 560 kW
|power original=
|max speed main= 188 mph
|max speed alt= 163 kn, 303 km/h
|cruise speed main=
|cruise speed alt=
|never exceed speed main=
|never exceed speed alt=
|stall speed main=
|stall speed alt=
|range main= 549 mi
|range alt= 477 nmi, 884 km
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 20,100 ft
|ceiling alt= 6,100 m
|climb rate main= <!--ft/min -->
|climb rate alt= <!--m/s -->
|loading main= 16.5 lb/ft²
|loading alt= 80.8 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.12 hp/lb
|power/mass alt= 190 W/kg
|more performance=*'''Climb to 5,000 ft (1,520 m):''' 5.5 min.
|guns= 1 × fixed forward firing 0.30 in (7.62 mm [[M1919 Browning machine gun]] and 1 × flexibly mounted rear gun
|bombs= 1 × 1,000 lb (454 kg) bomb under fuselage
|rockets=
|missiles=
|hardpoints=
|hardpoint capacity=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=*[[Vought SBU]]<br />
*[[SBC Helldiver|Curtiss SBC]]
*[[Aichi D1A]]
|lists=*[[List of military aircraft of the United States (naval)]]
}}

==References==
===Notes===
{{Reflist}}

===Bibliography===
{{refbegin}}
* Donald, David (editor). ''The Encyclopedia of World Aircraft''. Aerospace Publishing. 1997. ISBN 1-85605-375-X.
* Grossnik, Roy A. ''Dictionary of Americal Naval Aviation Squadrons: Volume 1 The History of VA, VAH, VAK, VAL, VAP and VFA Squadrons''. Washington DC: Naval Historical Centre, 1995. ISBN 0-945274-29-7.
* Swanborough, Gordon and Bowers, Peter M. ''United States Navy Aircraft since 1911''. London:Putnam, Second edition 1976. ISBN 0-370-10054-9.
* The ''[[Illustrated Encyclopedia of Aircraft]]'' (Part Work 1982-1985), 1985, Orbis Publishing.
{{refend}}

==External links==
{{commons category|Great Lakes BG}}
*[http://www.aerofiles.com/_grlakes.html Great Lakes] ''Aerofiles''.

{{USN bomber aircraft}}
{{Great Lakes aircraft}}

[[Category:Great Lakes aircraft|B01G]]
[[Category:United States bomber aircraft 1930–1939|Great Lakes BG1]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Carrier-based aircraft]]