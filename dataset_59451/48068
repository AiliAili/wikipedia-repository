{{Infobox person
| name = Nathan Gardels
| image= Nathan Gardels, Journalist.jpg
| birthname =
| birth_date = {{birth date and age|1952|12|22}}
| birth_place = [[United States]]
| education = ''B.A. in Theory and Comparative Politics; M.A. in Architecture and Urban Planning from [[UCLA]]
| occupation = [[Journalist]], [[Author]]
}}

'''Nathan Gardels''' (born December 22, 1952) is a senior adviser to the [[Berggruen Institute]] and is editor-in-chief of The WorldPost.<ref name="huffingtonpost.com">[http://www.huffingtonpost.com/2014/01/22/worldpost-launch_n_4644993.html WorldPost Launch Event Draws Global Leaders at Davos.] The Huffington Post. Retrieved 1-24-2014.</ref> He has also been editor-in-chief of New Perspectives Quarterly (NPQ) since its founding in 1985 and of the Global Viewpoint Network and Nobel Laureates Plus (services of the Los Angeles Times Syndicate/Tribune Media), which reaches 35 million readers in 15 languages, since 1989.<ref>[http://www.huffingtonpost.com/nathan-gardels/ Nathan Gardels author page at ''The Huffington Post.'']'' ''Retrieved 8-1-2013.</ref>

== Journalism career ==

Nathan Gardels has been editor of New Perspectives Quarterly since it began publishing in 1985. He has served as editor of Global Viewpoint, Global Economic Viewpoint and Nobel Laureates Plus since 1989. In 2014, Gardels became the editor-in-chief of The WorldPost, a digital publication stemming from a partnership between the Huffington Post and the [[Berggruen Institute]].<ref>http://www.independent.co.uk/news/media/online/huffington-post-founder-arianna-huffington-to-launch-world-post-news-website-9049893.html</ref>

Gardels has written for the Wall Street Journal, Los Angeles Times, New York Times, Washington Post, Harper’s, U.S. News & World Report and New York Review of Books. He has also written for foreign publications, including Corriere della Sera, El Pais, Le Figaro, Yomiuri Shimbun, O’Estado de Sao Paulo, the Guardian, Die Welt and many others.<ref>http://www.international.ucla.edu/burkle/article/130592</ref>

== Institutional Affiliations ==

From 1983 to 1985, Gardels was executive director of the Institute for National Strategy where he conducted policy research at the USA-Canada Institute in Moscow, the People's Institute of Foreign Affairs in Beijing, the [[Swedish Institute]] in Stockholm, and the [[Friedrich Ebert Stiftung]] in Bonn. Prior to this, he spent four years as adviser to Governor [[Jerry Brown]] of California on economic affairs, with an emphasis on public investment, trade issues, the Pacific Basin and Mexico.<ref>http://tribunecontentagency.com/premium-content/opinion/international/global-viewpoint-network/</ref>

Since 1986, Gardels has been a Media Fellow of the World Economic Forum (Davos). He has lectured at the [[Islamic Educational, Scientific and Cultural Organization]] (ISESCO) in Rabat, Morocco, and the Chinese Academy of Social Sciences in Beijing, China. Gardels was also a founding member at the New Delhi meeting of Intellectuels du Monde.<ref>[http://www.milkeninstitute.org/events/gcprogram.taf?function=bio&EventID=GC05&SPID=1776 Speaker's Biography.] Milken Institute. Retrieved 8-1-2013.</ref>
Gardels has been a long-standing member of the Council on Foreign Relations.<ref>[http://www.cfr.org/about/membership/roster.html?letter=G Council on Foreign Relations membership list]. Retrieved 8-1-2013.</ref> He is a senior fellow at the [[UCLA School of Public Affairs]] and a senior adviser at the [[Berggruen Institute]].<ref>[http://berggruen.org/people/nathan-gardels] Berggruen Institute. Retrieved 8-1-2013.</ref> Since January 2014, he has served as editor-in-chief of The WorldPost.<ref name="huffingtonpost.com"/>

== Books ==

Gardels is the author of several books,<ref>[http://www.goodreads.com/author/show/1504103.Nathan_Gardels Nathan Gardel's Books.] Good Reads. Retrieved 8-1-2013.</ref> including:
* ''At Century’s End'' (Alti/McGraw Hill, 1996)
* ''The Changing Global Order: World Leaders Reflect'' (Wiley-Blackwell, 1997)
* ''American Idol After Iraq: Competing for Hearts and Minds in the Global Media Age'', with Mike Medavoy (Wiley-Blackwell, 2009) and
* ''Intelligent Governance for the 21st Century: A Middle Way Between West and East'' with [[Nicolas Berggruen]]. A [[Financial Times]] best book of 2012<ref>[http://www.ft.com/intl/cms/s/2/88bdb3c0-37cf-11e2-a97e-00144feabdc0.html#axzz2alQzlsQp Best books of 2012]. ''The Financial Times. ''Retrieved 12-3-2012.</ref>

== Personal ==

Gardels holds degrees in Theory and Comparative Politics and in Architecture and Urban Planning from UCLA. He lives in Los Angeles with his wife, Lilly, and two sons, Carlos and Alexander.

== References ==

<!--- See [[Wikipedia:Footnotes]] on how to create references using<ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==

{{DEFAULTSORT:Gardels, Nathan}}
[[Category:Articles created via the Article Wizard]]
[[Category:1952 births]]
[[Category:Living people]]