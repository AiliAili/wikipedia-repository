
{{infobox Organization
|name              =  ASBPA | American Shore and Beach Preservation Association
|image        =0113asbpaLogo sig.jpg 
|image_border = 
|size         = 
|caption      = 
|map          = 
|msize        = 
|mcaption     = 
|abbreviation    = ASBPA
|motto             = Advocating for healthy coastlines
|formation    = 1926
|extinction   =
|type         =
|status         = 
|purpose      = 
|headquarters = 
|location     =
|region_served =
|membership   = 
|language     = 
|leader_title = President
|leader_name = Anthony P. Pratt
|main_organ = 
|parent_organization =
|affiliations = 
|num_staff    = 
|num_volunteers =
|budget       = 
|website      = http://www.asbpa.org
|remarks =
}}
The '''American Shore and Beach Preservation Association''' ('''ASBPA''') is a private, nonprofit organization formed in 1926. It was founded to address [[coastal erosion]] and the loss of sand on America's [[beaches]].<ref>"Scientific and Technical Societies of the United States," National Academy of Science - National Research Council, 1961, pg. 94, https://books.google.com/books?id=hy4rAAAAYAAJ&pg=PA94&lpg</ref> Today, ASBPA is an association of beach and coastal practitioners, including beach towns and managers, coastal engineers, coastal geologists, dredging and ecological restoration companies, coastal academics and students. ASBPA advances coastal science and [[coastal engineering]] through its peer-reviewed journal, ''Shore & Beach'', and an annual technical conference. It also hosts an annual coastal summit in Washington, DC to advocate for coastal policies.

== History ==
ASBPA was founded in 1926, and was initially instrumental in persuading the Congress to enact legislation authorizing federal funding of erosion studies and project works for shore protection. In 1930, ASBPA helped form the [[Beach Erosion Board]].<ref>[http://cdm16021.contentdm.oclc.org/utils/getfile/collection/p16021coll4/id/37/filename/38.pdf  "The History of the Beach Erosion Board,U.S. Army, Corps of Engineers, 1930-63," by Mary-Louise Quinn, Miscellaneous Report No. 77-9, August 1977]</ref><ref>Moore, J.W., and D.P. Moore, "The Corps of Engineers and Beach Erosion Control 1930-1982," ''Shore & Beach'', 51(January 1983), 13-17</ref>  ASBPA was politically engaged on coastal erosion and beach nourishment for most of the 20th century<ref>Pilkey, O. and A. Cooper. ''The Last Beach''. Duke University Press, 2014</ref> and has had number of prominent coastal advocates and members of the [[United States Army Corps of Engineers|Army Corps of Engineers]] on its board, including [[Morrough Parker O'Brien]], who served as President from 1972-1983.<ref>Wiegel, R.L. and T. Saville "History of Coastal Engineering in the USA". In: Kraus, N (ed.) ''History and Heritage of Coastal Engineering'' American Society of Civil Engineers, 1996.</ref>

After a long partnership, ASBPA merged with the American Coastal Coalition (ACC) in 2003.<ref>http://asbpa.org/about_us/about_us_ACC_ASBPA_history.htm</ref> In 2015, ASBPA formed memorandum of understanding with the Coastal Zone Foundation (CZF) to collaborate on a certification program for coastal practitioners.

== Advocacy ==
ASBPA (and the ACC, which it merged with in 2003) has been the leading advocate for the [[United States Army Corps of Engineers]] shore protection program<ref>Dean, Cornelia ''Against the Tide: The Battle for America's Beaches,'' Columbia University Press, 1999</ref> which builds and provides federal funding for beach and dunes systems as coastal flood protection for vulnerable communities. The focus on sand and sediment has led the organization to advocate for the beneficial use of dredged material<ref>{{Cite web|url=hhttps://www.scientificamerican.com/article/congress-wants-to-stop-coastal-erosion-with-mud/|title=Congress Wants to Stop Coastal Erosion--with Mud|date=2017-02-14|website=Scientific American|access-date=2017-03-16}}</ref> and natural coastal infrastructure. In February 2017, ASBPA President Anthony P. Pratt was invited to testify before [[United States Senate Committee on Environment and Public Works]] on the value of beaches, dunes, wetlands and other natural coastal infrastructure to the nation.<ref>https://www.epw.senate.gov/public/index.cfm/2017/2/oversight-modernizing-our-nation-s-infrastructure</ref>

== Publications ==

''Shore & Beach'' is a peer-reviewed journal, published by ASBPA four times per year. It includes scientific articles and general interest features. Typically, one issue per year is a special issue devoted to single subject. ''Shore & Beach'' has been published since 1933.<ref>"Summary of ''Shore & Beach'' issues and articles," ''Shore & Beach'', 83(4), 3</ref>

"Coastal Voice" is a member newsletter distributed by ASBPA eleven times per year. It includes coastal policy updates, science and technology updates, information on conferences and workshops, and changes to the board and staff of ASBPA.<ref>http://asbpa.org/publications/pubs_S_and_B.htm</ref>

== Best Restored Beach ==

ASBPA presents annual awards for '''Best Restored Beach''' to communities that have recently undergone nourishment or full restoration. Recent winners have included:

2016 winners:
* Babe’s Beach, Galveston, TX<ref>{{Cite web|url=http://www.chron.com/neighborhood/bayarea/article/New-Galveston-beach-named-after-Open-Beaches-Act-7943441.php|title=Restored Galveston beach named for A.R. 'Babe' Schwartz|website=Houston Chronicle|access-date=2016-06-21}}</ref>
* Rosewood Beach, Highland Park, IL<ref>{{Cite web|url=http://abc7chicago.com/1352405/|title=Highland Park's Rosewood Beach among top restored beaches|date=2016-05-23|website=ABC7 Chicago|language=en-US|access-date=2016-06-21}}</ref>
* Seabrook Island, SC<ref>{{Cite web|url=http://www.postandcourier.com/20160618/160619378/seabrook-island-named-among-top-restored-beaches#.V2fr1lzHF6M.twitter|title=Seabrook Island named among top restored beaches|date=2016-06-18|website=Post and Courier|access-date=2016-06-21}}</ref>
* Topsail Beach, NC<ref>{{Cite web|url=http://www.wilmingtonbiz.com/more_news/2016/05/23/legislative_savvy_good_beach_management_win_designation_for_topsail_beach/14820|title=Legislative savvy, good beach management win designation for Topsail Beach  {{!}} WilmingtonBiz|website=WilmingtonBiz|access-date=2016-06-21}}</ref>
* Redondo Beach, CA.<ref>{{Cite web|url=http://asbpa.org/news/newsroom_16BN0523_2016_best_restored_beaches.htm|title=ASBPA: Celebrate America’s beaches:|website=asbpa.org|access-date=2016-06-21}}</ref>
2015 winners:
*Galveston Island, TX<ref>Ferguson, J.W. "Building tomorrow's beach: As one sand project is completed, another is set to begin", ''Galveston County Daily News'' May 22, 2015 http://www.galvnews.com/news/free/article_cb265e8c-0041-11e5-8659-97dd83cec1e5.html</ref>
*Santa Monica, CA<ref>"City of Santa Monica Beach Project Named One of America's Best Restored Beaches for 2015," ''Santa Monica Mirror'' May 27, 2015 http://www.smmirror.com/articles/News/City-Of-Santa-Monica-Beach-Project-Named-One-Of-Americas-Best-Restored-Beaches-For-2015/43409</ref><ref>"Nonprofit Names Santa Monica Best Restored Beach," ''Santa Monica Lookout'' May 27, 2015 http://www.surfsantamonica.com/ssm_site/the_lookout/news/News-2015/May-2015/05_27_2015_Nonprofit_Names_Santa_Monica_Best_Restored_Beach.html</ref>
*Western Destin Beach, FL<ref>"Western Destin Beach Restoration Project is a Winner," ''ETB Travel News'', May 25, 2015 http://america.etbtravelnews.com/250811/western-destin-beach-restoration-project-is-a-winner/</ref>
*Folly Beach, SC<ref>"FOCUS: Folly Beach is one of nation's best restored beaches," ''Charleston Currents'' May 25, 2015 http://charlestoncurrents.com/2015/05/focus-folly-beach-is-one-of-nations-best-restored-beaches/</ref>
*South Hutchinson Island/St Lucie, FL<ref>Gill, E. "Hutchinson Island renourishment project named one of America's best," ''Vero Beach Press Jounal'', June 4, 2015 http://www.tcpalm.com/yournews/st-lucie-county/hutchinson-island-renourishment-project-named-one-of-americas-best-ep-1121613073-340648501.html</ref>

== Chapters ==
*California Shore & Beach Preservation Association - [http://www.csbpa.org www.csbpa.org]
*Central-East Coast chapter of ASBPA
*Hawaii Shore & Beach Preservation Association - [http://www.hawaiishoreandbeach.org www.hawaiishoreandbeach.org]
*Great Lakes Shore & Beach Preservation Association - [http://www.glsbpa.org www.glsbpa.org]
*Northeast Shore & Beach Preservation Association - [http://www.nsbpa.org www.nsbpa.org]
*Student Chapter of ASBPA at Stevens Institute of Technology - [http://gradlife.stevens.edu/org/stevensasbpa http://gradlife.stevens.edu/org/stevensasbpa]
*Texas chapter of ASBPA - [http://www.texasasbpa.org www.texasasbpa.org]

==External links==
*[http://www.asbpa.org ASBPA Web Site]

==References==
{{reflist}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Environmental organizations based in the United States]]
[[Category:Coastal engineering]]