{{For|Natalie Merchant song|Ophelia (album)}}
{{EngvarB|date=March 2014}}
{{Use dmy dates|date=March 2014}}
{{Infobox single
| Name           = Break Your Heart
| Cover          = Break your heart.jpg
| Border         = yes
| Artist         = [[Taio Cruz]] <!-- THE REMIX FEATURES LUDACRIS, NOT THE ORIGINAL VERSION -->
| Album          = [[Rokstarr]]
| Released       = {{ubl|{{start date|2009|09|13|df=yes}} (UK)|{{start date|df=yes|2010|02|02}} (US)}}
| Format         = {{hlist|[[CD single|CD]]|[[Music download|digital download]]}}
| Recorded       = 2009
| Genre          = {{hlist|[[Electropop]]|[[Electro (music)|Electro]]-[[Contemporary R&B|R&B]]|[[dance-pop]]}}
| Length         = {{ubl|3:23 (original version)|3:05 (remix with Ludacris)}}
| Label          = {{hlist|[[Island Records|Island]]|[[Mercury Records|Mercury]]}}
| Writer         = {{hlist|[[Taio Cruz]]|[[Fraser T Smith]]|[[Ludacris|Chris Bridges]]}}
| Producer       = {{hlist|[[Taio Cruz]]|Fraser T Smith}}
| Misc           = {{Extra chronology
| Artist         = [[Taio Cruz]] singles
| Type           = single
| Last single    = "[[Take Me Back (Tinchy Stryder song)|Take Me Back]]"<br />(2009)
| This single    = "'''Break Your Heart'''"<br />(2009)
| Next single    = "[[No Other One (song)|No Other One]]"<br />(2009)
}}}}
"'''Break Your Heart'''" is a song by English singer and songwriter [[Taio Cruz]]. The song serves as the [[lead single]] from his second studio album, ''[[Rokstarr]]'' (2009). It was written by Cruz and [[Fraser T Smith]] and produced by Smith. It was first released in the United Kingdom on 20 September 2009, followed by a release in the United States and other markets on 2 February 2010. The official remix version features American rapper [[Ludacris]]; that version was the single released in North American countries. The song, originally penned for [[Cheryl Cole]], is an uptempo [[Contemporary R&B|R&B]] song with [[synthpop]] and [[dance-pop]] elements, accompanied by Cruz's [[Auto-Tune]]d vocals. The song is lyrically a warning to someone about being a [[wikt:heartbreaker|heartbreaker]].

The song received mixed to positive reviews, critics commending its infectious sound, but noting that it was generic. The song peaked at number one in Canada, Switzerland, the United Kingdom and the United States, and also within the top ten of many other countries. The accompanying music video features several scenes of Cruz on escapades with different women, including a [[speedboat]] compared to classic [[Sean Combs|Diddy]] and a club scene with Ludacris in the American version.

==Background==
"Break Your Heart" was one of two songs penned by Cruz for [[Cheryl Cole]] for her debut solo album, ''[[3 Words]]''.<ref name="COLE">{{cite web|url=http://www.mtv.co.uk/artists/taio-cruz/news/157386-taio-cruz-talks-cheryl-and-sugababes|title=Taio Cruz Talks Cheryl And Sugababes|date=30 September 2009|work=''[[MTV News|MTV News UK]]''|publisher=MTV Networks|accessdate=12 May 2010}}</ref> After Cruz did not hear back from Cole's label about the song, he reworked the song for a male and made it the first song off his second album, ''Rokstarr''.<ref name="COLE"/> Cruz told ''[[MTV News#UK|MTV News UK]]'' that after he released the song, Cole's people would have liked the song for her after all.<ref name="COLE"/> The latter track by Cruz, "Stand Up" made it onto the final track list on Cole's album.<ref name="COLE"/>

However, according to the song's other co-writer [[Fraser T Smith]] in an interview with ''[[HitQuarters]]'', "Break Your Heart" was rejected by Cole's [[Polydor Records|Polydor]] label boss Ferdy Unger-Hamilton felt it was too similar to "[[Heartbreaker (will.i.am song)|Heartbreaker]]" by [[will.i.am]], the executive producer of ''3 Words''.<ref name="hitquarters.com">{{cite web |url=http://www.hitquarters.com/index.php3?page=intrview/opar/intrview_Fraser_T_Smith_Interview.html |title=Interview With Fraser T Smith |publisher=[[HitQuarters]]|date=22 March 2010 |accessdate=29 March 2010}}</ref> Smith also told This Must Be Pop: "Taio felt the lyric was a bit cocky for him to carry off – I told him he sounded great on it."<ref>{{cite web|url=http://www.thismustbepop.com/2010/10/thank-you-for-music-fraser-t-smith.html |title=Thank You for the Music: Fraser T Smith |publisher=This Must Be Pop |date=9 October 2010 |accessdate=28 August 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20111003072158/http://www.thismustbepop.com/2010/10/thank-you-for-music-fraser-t-smith.html |archivedate=3 October 2011 |df=dmy }}</ref>

Speaking of the song's lyrical background, Cruz told Pete Lewis of ''[[Blues & Soul]]'': <blockquote>"Its about breaking a girl's heart, but in a way that's kinda not on purpose. It's more that I'm just a single guy, trying to be single and trying to remain single. And sometimes, when you are in that place, you get girls who wanna be a part of what you're about – but, because you're not really ready for a relationship, those girls can end up being heartbroken. So what I'm basically saying is 'I might just break your heart. But I'm only gonna break your heart if you come through this way right now'."<ref>{{cite web|url=http://www.bluesandsoul.com/feature/456/taio_cruz_rokstarr_in_the_making|title=ROKSTARR IN THE MAKING -TAIO CRUZ INTERVIEW BY PETE LEWIS|work=Blues and Soul|accessdate=10 May 2010}}</ref></blockquote> Cruz also has called his song "catchy" with a "good melody" and "fun topic", stating that "both girls and guys can get into this character".<ref name="EW">{{cite web|url=http://music-mix.ew.com/2010/03/03/taio-cruz-interview/|title=Taio Cruz talks 'Break Your Heart', his new album and getting 'Dirty' with Ke$ha|first=Adam|last=Markovitz|date=3 March 2010|accessdate=12 May 2010|work=Entertainment Weekly}}</ref> In an interview with ''[[Entertainment Weekly]]'', Cruz elaborated and said the song was partially based on a personal situation, and rather an "exaggeration of an experience".<ref name="EW"/>

The success of the song in the United Kingdom attracted the attention of [[David Massey (music executive)|David Massey]] and Daniel Werner from Mercury/Island Def Jam who were excited and aggressive about releasing "Break Your Heart" in the United States on their label.<ref name="hitquarters"/> According to Cruz's manager Jamie Binns, the relationship with Monte Lipman at Universal Republic had "gone a bit quiet" by this point and as Taio wanted to be with the label that was most enthusiastic about his music, a move from Universal Republic to Mercury/Island Def Jam was engineered.<ref name="hitquarters"/> Massey and Werner's belief in the single's potential within the United States and relentless promotional push they gave it helped the song reach the [[Billboard Hot 100|''Billboard'' Hot 100]] top spot.<ref name="hitquarters"/>

===Ludacris remix===
In addition to re-working his album for an American release, Cruz tapped American rapper [[Ludacris]] on a remix for the American version of "Break Your Heart". According to Cruz's manager Jamie Binns, Mercury Records president David Massey had suggested that to introduce Cruz to the American market, the single should feature an American rapper with chart credibility.<ref name="hitquarters">{{cite web |url=http://www.hitquarters.com/index.php3?page=intrview/opar/intrview_JBinns.html |title=Interview With Jamie Binns |publisher=[[HitQuarters]]|date=18 April 2011 |accessdate=2 May 2011}}</ref> Massey and Mercury A&R manager Daniel Werner engineered an introduction with Ludacris' manager Jeff Dixon, who then played the song to Ludacris, who loved the track and within a week his contribution was complete.<ref name="hitquarters"/>

On collaborating with Ludacris, Cruz said, "With Ludacris, pretty much every track he's ever featured on sounds amazing. I gave him a quick call and asked him if he could get on the record, and he recorded it and sent it over. As I expected, there was nothing I needed to change. It sounded perfect. He put my name in there, which is great – so people know to pronounce it now properly, hopefully."<ref>{{cite web|url=http://www.mtv.com/news/articles/1634365/20100319/_kesha_.jhtml|title=Taio Cruz Talks Collaborating With Ludacris, Ke$ha|date=19 March 2010|accessdate=10 May 2010|work=[[MTV News]]|publisher=MTV Networks|first=Akshay|last=Bhansali}}</ref> The version featuring Ludacris was originally released digitally as the [[A-side and B-side|b-side]] to "[[No Other One (song)|No Other One]]" in November 2009, before in the United States in February 2010.<ref name="USDL"/><ref name="UKRL">{{cite web|url=https://itunes.apple.com/gb/album/no-other-one-ep/id340278665|title=No Other One – Single by Taio Cruz|publisher=[[iTunes|UK iTunes]]|accessdate=12 May 2010}}</ref>

==Composition==
{{listen|filename=Breakyourheart.ogg|title="Break Your Heart" (Remix)
|description=A 31-second sample of the remix version of "Break Your Heart" featuring Cruz's vocals and Ludacris' rap-intro.
|pos= right
|format=[[Ogg]]}}
"Break Your Heart" is an [[electropop]] song featuring a "surging [[dance-pop]]" sound, accompanied with Cruz's [[Auto-Tune]]d [[Contemporary R&B|R&B]] vocals.<ref name="GUARDIAN">{{cite web|url=https://www.theguardian.com/music/2009/oct/15/taio-cruz-cd-review|title=Taio Cruz: Rokstarr|date=15 September 2010|accessdate=12 May 2010|work=The Guardian |location=UK|first=Caroline|last=Sullivan}}</ref><ref name="BBC"/><ref name="BILLBOARDREVIEW">{{cite web|url=http://www.billboard.com/articles/review/1069659/taio-cruz-featuring-ludacris-break-your-heart|title=Taio Cruz featuring Ludacris, "Break Your Heart"|first=Michael|last=Menachem|date=5 March 2010|accessdate=12 May 2010|work=''[[Billboard (magazine)|Billboard]]''|publisher=MTV Networks.}}</ref><ref name="BUZZWORTHY"/> It is written in the [[key (music)|key]] of [[E-flat major|E{{music|flat}} major]] described as a "medium dance groove", and Cruz's vocals span from B{{music|flat}}<sub>4</sub> to B{{music|flat}}<sub>5</sub>.<ref name="sheet">{{cite web|title=Taio Cruz Break Your Heart – Digital Sheet Music|work=Musicnotes.com|publisher=[[Alfred Music Publishing|Alfred Publishing]]|accessdate=19 June 2010}}</ref> According to Jason Draper of ''[[Yahoo!|Yahoo! Music UK]]'', the song is a mix of [[Europop|European]] and American [[urban contemporary|urban music]].<ref>{{cite web|url=http://uk.launch.yahoo.com/091027/33/221xz.html|title=Taio Cruz – Rokstarr|date=27 October 2009|accessdate=14 June 2010|work=[[Yahoo!|Yahoo! Music UK]]|first=Jason|last=Draper}}</ref> It includes several tempo changes, which have been compared to that of [[Jay Sean]]'s "[[Down (Jay Sean song)|Down]]".<ref name="BILLBOARDREVIEW"/> The song is filled with boasting lyrics about being a heartbreaker rather than being heartbroken.<ref name="BBC"/>

==Critical reception==
Although ''[[BBC|BBC Music]]'' called the song a "cheese-fest", the reviewer said "in a weird way it's kind of beautiful", investing in the "[[Ibiza]]-inspired R&B trend".<ref name="BBC">{{cite web|url=http://www.bbc.co.uk/blogs/chartblog/2009/09/taio_cruz_break_your_heart.shtml|title=Taio Cruz – 'Break Your Heart'|date=12 September 2009|accessdate=12 May 2010|work=''[[BBC|BBC Music]]''}}</ref> The review also compared the song to [[Dizzee Rascal]] and Cruz's "layered vocals" and "slick production" to [[OneRepublic]] and [[Timbaland]]'s "[[Apologize (OneRepublic song)|Apologize]]".<ref name="BBC"/>
Michael Menachem of ''[[Billboard (magazine)|Billboard]]'' said that the "stateside version" turns up the heat with Ludacris' feature as "Cruz's breezy vocals on this electro-pop number have all the warmth of smooth R&B, while producer Fraser T Smith sets up the right ratio of catchy vocals and tempo changes to make a hit".<ref name="BILLBOARDREVIEW"/> Ash Dosanjh of ''[[NME|New Musical Express]]'' said that Cruz's downfall was when he acts the player, as on "Break Your Heart" and "[[Dirty Picture]]".<ref>{{cite web|url=http://www.nme.com/reviews/taio-cruz/10933|title=Album review: Taio Cruz – 'Rokstarr'|date=9 October 2009|accessdate=12 May 2010|work=''[[NME]]'|first=Ash|last=Dosanjh}}</ref> Caroline Sullivan of ''[[guardian.co.uk|The Guardian]]'' said Cruz was "proficient but generic" in the song, but his "autotuned vocals could have been anyone's, and this facelessness is a problem Cruz rarely surmounts."<ref name="GUARDIAN"/> Chris Ryan of ''[[MTV News|MTV Buzzworthy]]'' said that the song "perfectly embodies Cruz's infectious, dancefloor-friendly sound and sleek, immaculate production", and compares it to [[Jason Derülo]] and [[Akon]].<ref name="BUZZWORTHY">{{cite web|url=http://buzzworthy.mtv.com/2010/03/12/the-buzz-on-taio-cruz/|title=The Buzz on Taio Cruz|first=Chris|last=Ryan|date=12 March 2010|accessdate=12 May 2010|work=''[[MTV News|MTV Buzzworthy]]''|publisher=MTV Networks}}</ref>

==Chart performance==
"Break Your Heart" debuted at number one in the United Kingdom becoming Cruz's first song to reach the pole position in that territory.<ref name="THEBRITISHISLES">{{cite web|url=http://www.officialcharts.com/artist/_/taio%20cruz/|title=Taio Cruz|publisher=[[Official Charts Company]]|accessdate=19 June 2010}}</ref> The song remained at the top of the chart for three weeks.<ref name="THEBRITISHISLES"/> The song also peaked at number one in Switzerland, and in the top ten in several other countries across Europe.<ref>{{cite web|url=http://www.swisscharts.com/showitem.asp?interpret=Taio+Cruz+feat.+Ludacris&titel=Break+Your+Heart&cat=s |title=Taio Cruz feat. Ludacris – Break Your Heart (song) |publisher=Hung Medien |accessdate=19 June 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20111025092611/http://swisscharts.com/showitem.asp?interpret=Taio+Cruz+feat%2E+Ludacris&titel=Break+Your+Heart&cat=s |archivedate=25 October 2011 |df=dmy }}</ref>

The song also charted at number two in Australia, while peaking at number one in the United States.<ref name="USCHART">{{cite web|url=http://www.billboard.com/articles/news/959077/taio-cruz-cruises-to-record-no-1-jump-on-hot-100|title=Taio Cruz Cruises To Record No. 1 Jump on Hot 100|first=Silvio|last=Pietroluongo|date=10 March 2010|accessdate=30 May 2010|work=''[[Billboard (magazine)|Billboard]]''|publisher=Nielsen Business Media, Inc.}}</ref> With the jump from 53 to one on the [[Billboard Hot 100|''Billboard'' Hot 100]], Cruz also set the record for largest jump to the top of the chart by a debut act.<ref name="USCHART"/> The record was previously held by American singer [[Kelly Clarkson]], who jumped 52 to one with her ''[[American Idol]]'' coronation song, "[[A Moment Like This]]", and who also holds the record for the biggest non-debut jump on the chart, moving from 97 to one with "[[My Life Would Suck Without You]]".<ref name="USCHART"/> It originally moved 31,000 digital copies in the United States for a partial sales week prompting its original Hot 100 entry, and shifted 273,000 downloads in its first full week of availability, claiming the top debut on [[Hot Digital Songs]].<ref name="USCHART"/> It also marked Ludacris' fifth number one song.

The song eventually dominated American airplay also, peaking at number one on the [[Mainstream Top 40]] chart.<ref name="POP"/> Cruz became the twelfth male artist to have his first solo single peak at the top of the chart, and was the third since October 2008, following [[Jay Sean]] and [[Jason Derulo]].<ref name="POP">{{cite web|url=http://www.billboard.com/articles/columns/chart-beat/958203/pop-songs-where-the-boys-are|title=Pop Songs: Where The Boys Are|first=Gary|last=Trust|date=18 May 2010|accessdate=30 May 2010|work=''[[Billboard (magazine)|Billboard]]''|publisher=Nielsen Business Media, Inc.}}</ref> By August 2012, "Break Your Heart" had sold 3,712,000 digital copies in the United States.<ref name="yahoo2aug2012">{{cite news |url=http://music.yahoo.com/blogs/chart-watch/week-ending-aug-5-2012-songs-phillip-phillips-022224858.html |title=Week Ending Aug. 5, 2012. Songs: Phillip Phillips Is "Home" |author=Paul Grein |work=Chart Watch |publisher=Yahoo Music |date=8 August 2012 |accessdate=9 August 2012 }}</ref>

==Music video==
[[File:Breakyourheartvideo.jpg|thumb|200px|left|Cruz and his girlfriend on a speedboat in the music video.]]
The original music video was filmed in [[Sotogrande]],<ref>{{cite web|url=http://www.clipland.com/Summary/701013918/|title=Taio Cruz – Break Your Heart (version 1)|work=Clipland|accessdate=20 July 2015}}</ref> Spain in July 2009, with new parts filmed in Miami, United States in February 2010 to include [[Ludacris]],<ref>{{cite web|url=http://www.in4merz.com/releasestaio/2513-important-taio-cruz-dates.html|title=Important Taio Cruz Dates|work=In4merz Magazine|accessdate=29 May 2010}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> resulting in two versions of the video.<ref>{{cite web|url=http://www.clipland.com/Summary/701013919/|title=Taio Cruz feat. Ludacris – Break Your Heart (version 2)|work=Clipland|accessdate=14 June 2010}}</ref>

The video begins with Cruz and his girlfriend, played by [[Uzbekistan]]-born supermodel Nadya Nepomnyashaya, sitting together in a [[sports car]] near a [[pier]]. She tells him "You know I'm just gonna hurt you." Cruz responds by saying "You know I'm only gonna break your heart, right?" She says "You want a bet?" and he says "Bring It On." They then exit the car, walk down the pier and enter a nearby [[speedboat]]. Scenes are then shown in several different venues, including a club, a boat, a beachfront party, the speedboat and a hotel room. Throughout the video, Cruz is on escapades with different women. The video ends with Cruz's girlfriend laughing at his attempts to break her heart and then the two get off the boat they are on and go back onto the speedboat. For the version with Ludacris, there are scenes with him and Cruz in a white concrete backdrop with a flashing light, as well as the two of them together with a large group of people in another club.

When asked if an [[Old school hip hop|old school]] [[Sean Combs|Diddy]] was an influence on the video, Cruz said "Probably on some kind of subconscious level. I just love supermodels, I love [[sunshine]], and I love sports cars. And this time we also added a speedboat. So you got the four S's in there."<ref name="EW"/> In an interview with ''[[Rap-Up]]'', when talking about the video portraying him as a heartbreaker, Cruz said "No, not really. I suppose maybe 20%. It was more of just playing a character and having fun, just going out there and making a real cool, fun, cocky video. Not everyone has seen the whole plot of the video, but it's actually myself and my girlfriend both going out with the intention of breaking everyone's hearts."<ref>{{cite web|url=http://www.rap-up.com/2010/05/26/10-questions-for-taio-cruz/comment-page-1/#comment-339186|title=10 Questions for Taio Cruz|accessdate=29 May 2010|work=[[Rap-Up]]|first=Jordan|last=Upmalis}}</ref>
{{Clear}}

==Covers and other uses==
[["Weird Al" Yankovic]] included the chorus in his polka medley "[[Polka Face]]" from his 2011 album ''[[Alpocalypse]]''.<ref>{{cite web|url= http://www.covermesongs.com/2010/07/weird-al-unveils-his-new-polkaface.html |title= Weird Al Unveils His New Polka (face) |publisher= Cover Me |date= 23 July 2010 |accessdate= 5 June 2013}}</ref> Violinist [[Lindsey Stirling]] used the song to perform to in ''[[America's Got Talent]]''.

==Track listing==
* '''German CD single'''<ref>{{cite web|url=http://www.amazon.co.uk/dp/B003H695J6 |title=Break Your Heart (Feat. Ludacris) (2-Tracks): Taio Cruz: Amazon.co.uk: Music |publisher=Amazon.co.uk |accessdate=28 August 2011}}</ref>
# "Break Your Heart" (featuring Ludacris) – 3:05
# "Break Your Heart" (The Wideboys Remix Radio Edit) – 3:46

* '''UK CD single'''<ref>{{cite web|url=http://hmv.com/hmvweb/displayProductDetails.do?ctx=280;0;-1;-1;-1&sku=214562|title=music: Break Your Heart (2009)|accessdate=30 May 2010|publisher=[[HMV Group|HMV]]}}</ref>
#"Break Your Heart" – 3:23
#"Break Your Heart" (Vito Benito FF Radio Remix) – 3:22
#"Break Your Heart" (Paul Thomas Remix) – 7:41
#"Break Your Heart" (Cassette Club Remix) – 7:22

* '''Digital download'''<ref>https://itunes.apple.com/gb/album/break-your-heart-ep/id329493929</ref>
#"Break Your Heart" – 3:23
#"Break Your Heart" (Paul Thomas Remix) – 7:41

* '''Digital download – EP'''<ref>https://itunes.apple.com/gb/album/break-your-heart-ep/id330173074</ref>
#"Break Your Heart" (Vito Benito FF Radio Remix) – 3:22
#"Break Your Heart" (Cassette Club Remix) – 7:22
#"Break Your Heart" (Agent X Remix) – 4:27

==Credits and personnel==
* Songwriting – Taio Cruz [[Fraser T Smith]], [[Ludacris|Chris Bridges]]
* Producing – Taio Cruz, Fraser T Smith
* Engineering – Beatriz Artola
* [[Mixing (process engineering)|Mixing]] – Fraser T Smith
* [[Audio mastering|Mastering]] – Dick Beetham

Credits adapted from the album liner notes.<ref>{{cite AV media notes|others=Taio Cruz|title=Break Your Heart|year=2009|type=Liner notes|publisher=[[Island Records]]}}</ref>

==Charts and certifications==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable sortable"
|-
! Chart (2009–10)
! Peak<br />position
|-
{{singlechart|Australia|2|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=18 March 2010.}}
|-
{{singlechart|Austria|10|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=14 October 2010.}}
|-
{{singlechart|Flanders|3|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=24 April 2010.}}
|-
{{singlechart|Wallonia|2|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=24 April 2010.}}
|-
|Brazilian Singles Chart ([[Associação Brasileira dos Produtores de Discos|ABPD]])<ref name="Brazil">{{cite web |url = http://asdfg-menezes.org/bookBR.pdf |title = Brazil |work = ABPD |date = October 6, 2001 |accessdate = April 1, 2014 }}</ref>
| style="text-align:center;"|17
|-
{{singlechart|Billboardcanadianhot100|1|artist=Taio Cruz|artistid=796650|accessdate=13 March 2010.}}
|-
{{singlechart|Czech Republic|3|year=2010|week=10|accessdate=13 March 2010.}}
|-
{{singlechart|Denmark|36|artist=Taio Cruz|song=Taio Cruz feat. Ludacris|accessdate=13 March 2010.}}
|-
{{singlechart|Billboardeuropeanhot100|2|artist=Taio Cruz|artistid=796650|accessdate=13 March 2010.}}
|-
{{singlechart|Finland|4|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=13 March 2010.}}
|-
{{singlechart|French|2|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=24 April 2010.}}
|-
{{singlechart|Germany2|5|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|date=20100521|accessdate=21 May 2010.}}
|-
{{singlechart|Hungary|2|year=2010|week=39|}}
|-
{{singlechart|Ireland|2|year=2009|week=38|accessdate=13 March 2010.}}
|-
{{singlechart|Dutch40|8|year=2010|week=22}}
|-
{{singlechart|New Zealand|17|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=13 March 2010.}}
|-
{{singlechart|Norway|7|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=13 March 2010.}}
|-
{{singlechart|Slovakia|3|year=2009|week=45|accessdate=13 March 2010.}}
|-
{{singlechart|Spain|19|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=14 October 2010.}}
|-
{{singlechart|Sweden|6|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=13 March 2010.}}
|-
{{singlechart|Switzerland|1|artist=Taio Cruz feat. Ludacris|song=Break Your Heart|accessdate=13 March 2010.}}
|-
{{singlechart|UKchartstats|1|artist=Taio Cruz|song=Break Your Heart|songid=34535|accessdate=13 March 2010.}}
|-
{{singlechart|UKrandb|1|date=10 October 2009|artist=Taio Cruz|song=Break Your Heart|accessdate=24 April 2010.}}
|-
{{singlechart|Billboardhot100|1|artist=Taio Cruz|artistid=277535|accessdate=13 March 2010}}
|-
{{singlechart|Billboardadultcontemporary|24|artist=Taio Cruz|artistid=277535|accessdate=24 April 2010}}
|-
{{singlechart|Billboardadultpopsongs|18|artist=Taio Cruz|artistid=277535|accessdate=24 April 2010}}
|-
{{singlechart|Billboarddanceclubplay|5|artist=Taio Cruz|artistid=277535|accessdate=13 March 2010}}
|-
{{singlechart|Billboardpopsongs|1|artist=Taio Cruz|artistid=277535|accessdate=24 April 2010}}
|-
{{singlechart|Billboardrhythmic|3|artist=Taio Cruz|artistid=277535|accessdate=24 April 2010}}
|}
{| class="wikitable sortable"
|-
! Chart (2015)
! Peak<br />position
|-
{{singlechart|Poland|87|chartid=1818|year=2015|accessdate=20 December 2015}}
|}
{{col-2}}

=== Certifications ===
{{Certification Table Top}}
{{Certification Table Entry|region=Australia|type=single|award=Platinum|number=2|certyear=2010|artist=Taio Cruz|title=Break Your Heart|autocat=true}}
{{Certification Table Entry|region=Austria|type=single|award=Gold|relyear=2010|artist=Taio Cruz|title=Break Your Heart|autocat=true}}
{{Certification Table Entry|region=Belgium|title=Break Your Heart|artist=Taio Cruz|type=single|award=Gold|accessdate=24 February 2012|certyear=2010|relyear=2010|autocat=yes}}
{{Certification Table Entry|region=Canada|type=single|award=Platinum|number=3|certyear=2012|artist=Taio Cruz|title=Break Your Heart|autocat=true|digital=true}}
{{Certification Table Entry|region=Germany|type=single|award=Gold|relyear=2010|artist=Taio Cruz|title=Break Your Heart|autocat=true}}
{{Certification Table Entry|region=New Zealand|type=single|award=Gold|relyear=2010|artist=Taio Cruz|title=Break Your Heart|autocat=true|certref=<ref>{{cite web|url=http://www.radioscope.net.nz/index.php?option=com_content&task=view&id=77&Itemid=63 |archive-url=https://web.archive.org/web/20081014104613/http://www.radioscope.net.nz/index.php?option=com_content&task=view&id=77&Itemid=63 |dead-url=yes |archive-date=14 October 2008 |title=Latest Gold / Platinum Singles – RadioScope New Zealand |publisher=Radioscope.net.nz |accessdate=28 August 2011 |df=dmy }}</ref>}}
{{Certification Table Entry|region=Sweden|certyear=2010|relyear=2011|artist=Taio Cruz|title=Break Your Heart|award=Gold|type=single|autocat=yes|certref=<ref>{{cite web | url=http://www.sverigetopplistan.se/netdata/ghl002.mbr/artdata?dfom=20110722&sart=337984 | title=Taio Cruz - Break Your Heart| publisher=[[Swedish Recording Industry Association|Grammofon Leverantörernas Förening]] | accessdate=1 December 2011 | language=Swedish}}</ref>}}
{{Certification Table Entry|region=Switzerland|type=single|award=Platinum|number=2|certyear=2012|artist=Taio Cruz feat. Ludacris|title=Break Your Heart|autocat=true|relyear=2010}}
{{Certification Table Entry|region=United Kingdom|type=single|award=Gold|relyear=2011|certyear=2013|artist=Taio Cruz|title=Break Your Heart|autocat=true}}
{{Certification Table Entry|region=United States|type=single|award=Platinum|number=
3|relyear=2010|artist=Taio Cruz|title=Break Your Heart|autocat=true|salesamount=3,712,000|salesref=<ref name="yahoo2aug2012" />}}
{{Certification Table Bottom|format=3col|nounspecified=true}}

=== Year-end charts ===
{| class="wikitable sortable"
|-
!scope="col"| Chart (2010)
!scope="col"| Position
|-
|Australia ([[ARIA Charts|ARIA]])<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-end-of-year-charts-top-100-singles-2010.htm |title=ARIA Charts – End of Year Charts – Top 100 Singles |publisher=Australian Recording Industry Association |accessdate=7 February 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20101205172929/http://aria.com.au/pages/aria-charts-end-of-year-charts-top-100-singles-2010.htm |archivedate=5 December 2010 |df=dmy }}</ref>
| style="text-align:center;"|31
|-
|Austria ([[Ö3 Austria Top 40]])<ref>{{cite web|url=http://austriancharts.at/2010_single.asp |title=Jahreshitparade 2010 |language=German |work=[[Hitradio Ö3]] |publisher=Hung Medien |accessdate=7 February 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110227055450/http://austriancharts.at:80/2010_single.asp |archivedate=27 February 2011 |df=dmy }}</ref>
| style="text-align:center;"|32
|-
|Belgium ([[Ultratop 50]] Flanders)<ref>{{cite web|url=http://www.ultratop.be/nl/annual.asp?year=2010|title=Jaaroverzichten 2010 (Flanders)  |publisher=Ultratop. Hung Medien|language=Dutch |accessdate=7 February 2011}}</ref>
| style="text-align:center;"|30
|-
|Belgium ([[Ultratop 50]] Wallonia)<ref>{{cite web |title=Rapports annuels 2010 – Singles |url=http://www.ultratop.be/fr/annual.asp?year=2010 |publisher=Ultratop |language=French |accessdate=7 February 2011}}</ref>
| style="text-align:center;"|18
|-
| Canada ([[Canadian Hot 100]])<ref>{{cite web|url=http://www.billboard.com/charts/year-end/2010/canadian-hot-100|date=9 December 2009|accessdate=7 February 2011|title=2010 Year-End Canadian Hot 100 Songs |work=[[Billboard (magazine)|Billboard]]|publisher=Nielsen Business Media, Inc}}</ref>
| style="text-align:center;"|5
|-
|Germany ([[Media Control AG]])<ref>{{cite web|url=http://www.viva.tv/charts/viva-single-jahrescharts-2010-2010-212/?start=20 |title=2010 Year-End German Charts |publisher=Media Control Charts. Viva.tv |accessdate=7 February 2011 |language=German |deadurl=yes |archiveurl=https://web.archive.org/web/20110110033940/http://www.viva.tv/charts/viva-single-jahrescharts-2010-2010-212/?start=20 |archivedate=10 January 2011 |df=dmy }}</ref>
| style="text-align:center;"|23
|-
|Hungary ([[Association of Hungarian Record Companies|Rádiós Top 40]])<ref>{{cite web |title=MAHASZ Rádiós TOP 100 2010|url=http://www.mahasz.hu/?menu=slagerlistak&menu2=eves_osszesitett_listak&id=radios&ev=2010|work=Mahasz |language=hungarian |accessdate=10 March 2011}}</ref>
| style="text-align:center;"|25
|-
|Netherlands ([[Dutch Top 40]])<ref>{{cite web|url=http://dutchcharts.nl/yearchart.asp?cat=s |title=2010 Dutch Year-end chart |publisher=MegaCharts. Hung Medien |language=Dutch |accessdate=7 February 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20101122072405/http://dutchcharts.nl/yearchart.asp?cat=s |archivedate=22 November 2010 |df=dmy }}</ref>
| style="text-align:center;"|84
|-
|Romania ([[Romanian Top 100]])<ref>{{cite web |title=Topul celor mai difuzate piese în România în 2010 |url=http://www.romanialibera.ro/arte/muzica/exclusiv-topul-celor-mai-difuzate-piese-in-romania-in-2010-211604.html |publisher=''[[România Liberă]] |language=Romanian |accessdate=7 February 2011}}</ref>
| style="text-align:center;"|72
|-
| Switzerland ([[Swiss Music Charts|Schweizer Hitparade]])<ref>{{cite web|url=http://swisscharts.com/year.asp?key=2010 |title=2010 Year End Swiss Singles Chart |year=2011 |accessdate=7 February 2011 |publisher=[[Swiss Music Charts]] |deadurl=yes |archiveurl=https://web.archive.org/web/20110712202416/http://swisscharts.com:80/year.asp?key=2010 |archivedate=12 July 2011 |df=dmy }}</ref>
| style="text-align:center;"|9
|-
|US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref>{{cite web|url=http://www.billboard.com/charts/year-end/2010/hot-100-songs|accessdate=7 February 2011|title=2010 Year-End Hot 100 Songs |work=[[Billboard (magazine)|Billboard]]|publisher=Nielsen Business Media, Inc}}</ref>
| style="text-align:center;"|10
|-
|US [[Mainstream Top 40 (Pop Songs)|Pop Songs]] ([[Billboard (magazine)|''Billboard]])<ref>{{cite web|url=http://www.billboard.com/charts?tag=nav#/charts-year-end/hot-pop-songs?|accessdate=7 February 2011|title=2010 Year-End Pop Songs |work=[[Billboard (magazine)|Billboard]]|publisher=Nielsen Business Media, Inc}}</ref>
| style="text-align:center;"|7
|}
{{col-end}}

==Release history==
{| class="wikitable"
|-
! Region
! Date
! Format
! Label
|-
| rowspan="2"|United Kingdom
| 13 September 2009<ref name="BYHEP">{{cite web|url=https://itunes.apple.com/gb/album/break-your-heart-ep/id329493929|title=Break Your Heart – EP by Taio Cruz|accessdate=30 May 2010|publisher=[[ITunes Store|UK iTunes]]}}</ref>
| [[Music download|Digital download]]
| rowspan="2"|[[Island Records|Island]]
|-
| 14 September 2009<ref>{{cite web|url=http://www.amazon.co.uk/dp/B002KL0IZ4|title=Break Your Heart [Single]|accessdate=30 May 2010|publisher=[[Amazon.com|amazon.co.uk]]}}</ref>
| [[CD single]]
|-
| rowspan="3"|United States
| 2 February 2010<ref>{{cite web|url=http://www.allaccess.com/top40-rhythmic/future-releases|title=Top 40/R Future Releases|accessdate=30 May 2010|publisher=Allaccess}}</ref>
| [[Contemporary hit radio|Mainstream]] and [[rhythmic contemporary|rhythmic]] [[airplay]]
| rowspan="3"|[[Mercury Records|Mercury]]
|-
| 25 February 2010<ref name="USDL">{{cite web|url=https://itunes.apple.com/us/album/break-your-heart-feat-ludacris/id358275356 |title=Break Your Heart (feat. Ludacris) – Single by Taio Cruz |publisher=[[iTunes Store|iTunes Music Store]] |accessdate=30 May 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100804045810/http://itunes.apple.com:80/us/album/break-your-heart-feat-ludacris/id358275356? |archivedate=4 August 2010 |df=dmy }}</ref>
| Digital download
|-
|11 May 2010<ref>{{cite web|url=http://gfa.radioandrecords.com/publishGFA/GFANextPage.asp?sDate=05/11/2010&Format=5|title= R&R Future Releases: Urban|accessdate=30 May 2010|publisher=Radio & Records}}</ref>
|[[Urban contemporary|Urban]] airplay
|}

==See also==
*[[List of UK Singles Chart number ones of the 2000s#2009|List of number-one singles from the 2000s (UK)]]
*[[List of UK R&B Chart number-one singles of 2009]]
*[[List of Billboard Hot 100 number-one singles of the 2010s#2010|List of Billboard Hot 100 number-one singles of the 2010s]]
*[[List of Mainstream Top 40 number-one hits of 2010 (U.S.)]]
*[[List of number-one dance airplay hits of 2010 (U.S.)]]
*[[List of Hot 100 number-one singles of 2010 (Canada)]]
*[[List of number-one hits of 2010 (Switzerland)]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Taio Cruz}}
* Music video [http://www.clipland.com/Summary/701013918/ ''Break Your Heart'' (version 1)] at [http://www.clipland.com/ Clipland]
* Music video [http://www.clipland.com/Summary/701013919/ ''Break Your Heart'' (version 2 with Ludacris)] at [http://www.clipland.com/ Clipland]
{{Good article}}
* {{MetroLyrics song|taio-cruz|break-your-heart}}<!-- Licensed lyrics provider -->

{{Taio Cruz singles}}
{{Ludacris singles}}

[[Category:2009 singles]]
[[Category:2010 singles]]
[[Category:Billboard Hot 100 number-one singles]]
[[Category:Billboard Dance/Mix Show Airplay number-one singles]]
[[Category:Canadian Hot 100 number-one singles]]
[[Category:Ludacris songs]]
[[Category:Song recordings produced by Fraser T Smith]]
[[Category:Songs written by Fraser T Smith]]
[[Category:Songs written by Taio Cruz]]
[[Category:Taio Cruz songs]]
[[Category:UK R&B Singles Chart number-one singles]]
[[Category:UK Singles Chart number-one singles]]
[[Category:2009 songs]]
[[Category:Songs about betrayal]]