<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=C4
 | image=C4 in flight.jpg
 | caption=The C4 in flight
}}{{Infobox Aircraft Type
 | type=[[Light aircraft]]
 | national origin=[[Germany]]
 | manufacturer=[[Flight Design]]
 | designer=
 | first flight=9 April 2015<ref name="avweb.com">{{cite web|url=http://www.avweb.com/avwebflash/news/First-Flight-For-Flight-Design-C4-223849-1.html|title=First Flight For Flight Design C4|work=avweb.com|accessdate=14 April 2015}}</ref>
 | introduced=2011
 | retired=
 | status=Under development
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= 220,000 [[Euro]]s/[[US$]]250,000 (goal)<!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Flight Design CTSW]]
 | variants with their own articles=
}}
|}
The '''Flight Design C4''' is a [[Germany|German]] four seat, [[high-wing]], single engine [[light aircraft]] under development by [[Flight Design]] of [[Leinfelden-Echterdingen]].<ref name="AvWeb15Apr11">{{Cite news|url =http://www.avweb.com/avwebflash/news/flight_design_c4_four_seat_204493-1.html|title = New Four-Seater From Flight Design|accessdate = 18 April 2011|last = Pew|first = Glenn|authorlink = |date=April 2011| work = AvWeb}}</ref><ref name="AvWeb25Jul11">{{Cite news|url =http://www.avweb.com/news/airventure/EAAAirVenture2011_FlightDesignFourPlaceMockupOshkoshDebut_205016-1.html|title = Flight Design Four-Place Mockup Oshkosh Debut|accessdate = 25 July 2011|last = Grady|first = Mary|authorlink = |date=July 2011| work = AvWeb}}</ref><ref name="CompanyFactSheet">{{cite web|url = http://flightdesign.com/files/Media/C4%20fact%20sheet.pdf|title = C4 Fact Sheet|accessdate = 25 July 2011|last = [[Flight Design]]|authorlink = |date=April 2011}}</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 164. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

In February 2011 the company announced that it was developing a four-seat design. The C4 was introduced at the [[AERO Friedrichshafen]] 2011 show. The company finalized the design features through an on-line survey to determine the features and performance that potential customers were looking for in a new aircraft. A full-sized exterior mock-up was also displayed at [[AirVenture]] 2011.<ref name="AvWeb15Apr11" /><ref name="AvWeb25Jul11" /><ref name="WDLA11" /><ref name="Avweb10Feb11">{{Cite news|url = http://www.avweb.com/avwebflash/news/flight_design_c4_survey_204105-1.html|title = Flight Design's Four Seater|accessdate = 11 February 2011|last = Pew|first = Glenn|authorlink = |date=February 2011| work = AvWeb}}</ref>

The aircraft first flew on 9 April 2015.<ref name="avweb.com"/>

==Development==
The company applied to start [[type certificate|certification]] for the C4 under [[European Aviation Safety Agency]] rules in early 2011 and intends to gain US [[Federal Aviation Administration]] FAA certification through EASA certification reciprocity and was initially forecasting FAA approval in early 2013, although this was later delayed. First deliveries were initially forecast for 2013, at a price of 220,000 Euros or [[US$]]250,000. Company CEO Matthias Betsch indicated that the key to the aircraft's success will be the price of US$250,000, saying: "We'll do everything to make that number. That's the magic number."<ref name="AvWeb15Apr11" /><ref name="AvWeb25Jul11" />

In writing about the aircraft's price goal, AVweb's Paul Bertorelli wrote: "here comes Flight Designs with a certified, four-place cruiser it proposes to sell for around $250,000. Why does it think it can do this with Diamond's DA40—a comparable proven and competent airplane—sells for around $350,000? One reason is that it builds airplanes in the Ukraine, where labor rates are lower. But another may be that it hasn't certified a four-place airplane in the current market and is doing what most airplane companies do: underestimating the cost of bringing a new airplane into production. I wish them the best, but I'd rather see a realistic price that builds in the most important thing any new airplane should have: Good value for the customer and profitability for the company building the airplane. It's never in the customer's interest to have the company losing money on every sale."<ref name="AVweb">{{Cite news|url = http://www.avweb.com/blogs/insider/AVWebInsider_OSHNotebook_205053-1.html|title = OSH Notebook|accessdate = 27 July 2011|last = Bertorelli|first = Paul|authorlink = |date=July 2011| work = AVweb}}</ref>

At [[AirVenture]] 2011 the company indicated that they had sold 40 delivery positions.<ref name="AVweb02Aug11">{{Cite news|url = http://www.avweb.com/avwebflash/news/VendorsReportRobustSalesAtAirVenture_205142-1.html|title = Vendors Report Robust Sales At AirVenture|accessdate = 4 August 2011|last = Grady|first = Mary|authorlink = |date=August 2011| work = AVweb}}</ref> By [[Sun 'n Fun]] in March 2012 development was continuing, with a focus on selecting [[avionics]]. The company remained committed to the US$250,000 price goal and had about 65 orders for the C4.<ref name="Grady27Mar12">{{cite news|url = http://www.avweb.com/news/snf/SunNFun2012_FlightDesignMovingForwardWithNewProjects_206392-1.html|title = Flight Design Moving Forward With New Projects|accessdate = 28 March 2012|last = Grady|first = Mary|date = 27 March 2012| work = AVWev}}</ref> At the 2012 Aero Expo in Germany, Flight Design displayed a fuselage mock-up of the C4.<ref>{{cite web|last=Horne |first=Thomas A. |url=http://www.aopa.org/aircraft/articles/2012/120420flight-design-ctls-earns-easa-certification.html |title=Flight Design's CTLS earns EASA certification |publisher=Aopa.org |date=2012-04-20 |accessdate=2012-04-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20120423135301/http://www.aopa.org:80/aircraft/articles/2012/120420flight-design-ctls-earns-easa-certification.html |archivedate=2012-04-23 |df= }}</ref>

At [[Sun 'n Fun]] 2013 the company indicated that they were delaying development of the aircraft to take advantage of the [[Federal Aviation Administration]]'s review of the [[Type certificate|FAR 23 type certification]] standards, which should simplify and reduce the cost of certifying the C4. If the regulatory changes take too long then the company indicated that it may certify the aircraft in the [[primary aircraft category]] instead, even though it would preclude some commercial uses, such as [[air charter]]. At this time the first flight of a proof of concept prototype was forecast for the summer of 2014 with certification by the end of 2015.<ref name="Grady09Apr13">{{cite news|url = http://www.avweb.com/news/snf/SunNFun2013_FlightDesignPushesBackC4Program_208459-1.html|title = Flight Design Pushes Back C4 Program |accessdate = 11 April 2013|last = Grady|first = Mary|date = 9 April 2013| work = AVweb}}</ref>

At the end of March 2014 first flight was scheduled for June 2014 with certification forecast to be completed by the end of 2015. The target price of US$250,000 was still current as well.<ref name="Grady31Mar14">{{cite news|url = http://www.avweb.com/avwebflash/news/Flight-Design-Four-Place-Advances221712-1.html|title = Flight Design Four-Place Advances|accessdate = 31 March 2014|last = Grady|first = Mary|date = 31 March 2014| work = AVweb}}</ref> Further delays, however, pushed the first flight back to a new forecast date of November 2014.<ref name="Berqvist13Nov14">{{cite news|url = http://www.flyingmag.com/aircraft/pistons/flight-designs-c4-nears-first-flight|title = Flight Design’s C4 Nears First Flight|accessdate = 2 Jan 2015|last = Berqvist|first = Pia|date = 13 Nov 2014|work=Flying Magazine}}</ref> The C4 was actually first flown on 9 April 2015. The initial flight data showed that it met its design performance goals and had benign approach to [[Stall (fluid mechanics)|stall]] characteristics.<ref name="avweb.com"/><ref>[https://www.youtube.com/watch?feature=player_embedded&v=9vEvABOjF6o Video of first flight, and interview]</ref>

In April 2015 the company announced that production of the type would be in the [[United States]].<ref name="Niles22Apr15">{{cite news|url = http://www.avweb.com/avwebflash/news/Flight-Design-C4-To-Be-Assembled-In-US-223949-1.html|title = Flight Design C4 To Be Assembled In U.S.|accessdate = 23 April 2015|last = Niles|first = Russ|date = 22 April 2015| work = AVweb}}</ref>

==Design==
The aircraft is an all-composite design developed from the two-seat [[Flight Design CT]] family. The structure will be a carbon, [[Aramide]] and glass sandwich structure. The wing will be a one-piece cantilever design. Cabin access will be through two doors, hinged at the front, plus a baggage compartment door. Seating is individual front seats, with a rear folding bench seat. Design engines will be the [[Continental IO-360|Continental IO-360-AF]] of {{convert|180|hp|kW|0|abbr=on}} and maybe the [[Thielert Centurion 2.0]] turbocharged diesel of {{convert|155|hp|kW|0|abbr=on}}. The [[propeller]] will be a composite [[constant speed propeller|constant speed]] type. The C4's design empty weight is {{convert|600|kg|lb|0|abbr=on}}, with a gross weight of {{convert|1200|kg|lb|0|abbr=on}}. A full-aircraft [[Ballistic Recovery Systems]] parachute system will be standard equipment.<ref name="AvWeb15Apr11" /><ref name="AvWeb25Jul11" /><ref name="CompanyFactSheet" /> In 2014 the [[List of Garmin products#Aviation|Garmin G3X]] was selected as the aircraft's [[avionics]] suite.<ref name=thurb>Thurber, Matt. "[http://www.ainonline.com/aviation-news/aviation-international-news/2014-09-01/airventure-report-2014 AirVenture Report: 2014]" ''AINonline'', 1 September 2014. Accessed: 4 September 2014.</ref>

Maximum cruise speed will be {{convert|296|km/h|kn|0|abbr=on}} with the Lycoming engine and {{convert|269|km/h|kn|0|abbr=on}} with the Thielert powerplant, although the latter will provide {{convert|3151|km|nmi|0|abbr=on}} range.<ref name="AvWeb15Apr11" /><ref name="CompanyFactSheet" />

Optional equipment will be available that will allow the aircraft to be used in the [[Aero-tow|glider]] or [[banner towing]] role or on [[Seaplane|floats]]. An [[ice protection system]] is under consideration by the company for future incorporation into the design.<ref name="CompanyFactSheet" />
<!-- ==Operational history== -->
<!-- ==Variants== -->

==Specifications (C4) ==
{{Aircraft specs
|ref=AvWeb and Company fact Sheet<ref name="AvWeb15Apr11" /><ref name="CompanyFactSheet" /><ref name=thurb/>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=7.876
|length ft=
|length in=
|length note=
|span m=9.930
|span ft=
|span in=
|span note=
|height m=2.611
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=1320
|empty weight note=
|gross weight kg=
|gross weight lb=2640
|gross weight note=
|fuel capacity={{convert|70|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental IO-360|Continental IO-360-AF]]
|eng1 type=[[avgas]] and alternative fuels piston [[aircraft engine]]
|eng1 hp=180

|prop blade number=3<!-- propeller aircraft -->
|prop name=composite [[constant speed propeller]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=296
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=93
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=360
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=3150
|range miles=
|range nmi=1200 NM (2200 km)@ 65% power; 8.4 gal/hr (32 l/hr) 145 kt
|range note=
|endurance=13:35 maximum
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=5.0
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=*[[Garmin G1000]]
*Dynon SkyView suite
*[[autopilot]]
*[[Traffic collision avoidance system]] and [[terrain awareness and warning system]]
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Diamond DA40]]
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

==External links==
{{commons category|Flight Design C4}}
*[https://web.archive.org/web/20110711202350/http://www.flightdesign.com:80/index.php?page=product&p=60 Official website]
*[https://www.youtube.com/watch?v=B3O8ZKjVXSY AVweb video report on the C4 cabin mock-up and engine at Aero Friedrichshafen 2012]
{{Flight Design aircraft}}
[[Category:Flight Design aircraft|C4]]
[[Category:Proposed aircraft of Germany]]