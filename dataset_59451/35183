{{Infobox hurricane season
| Basin=SWI
| Year=1998
| Track=1997-1998 South-West Indian Ocean cyclone season summary.jpg
| First storm formed=January 2, 1998 <small>(Unofficially)</small><br>January 16, 1998 <small>(Officially)</small>
| Last storm dissipated=April 22, 1998 <small>(Officially)</small><br>July 23, 1998 <small>(Unofficially)</small>
| Strongest storm name=Anacelle
| Strongest storm pressure=950
| Strongest storm winds=75
| Average wind speed=10
| Total disturbances=16
| Total depressions=11
| Total storms=5 official, 5 unofficial
| Total hurricanes=1<!--number of Tropical Cyclones >63 knots-->
| Total intense=<!--not used in this basin-->
| Fatalities=88&ndash;144
| Damages=
| five seasons=[[1995–96 South-West Indian Ocean cyclone season|1995–96]], [[1996–97 South-West Indian Ocean cyclone season|1996–97]], '''1997–98''', [[1998–99 South-West Indian Ocean cyclone season|1998–99]], [[1999–2000 South-West Indian Ocean cyclone season|1999–00]]
|Australian season=1997–98 Australian region cyclone season
|South Pacific season=1997–98 South Pacific cyclone season
}}
The '''1997–98 South-West Indian Ocean cyclone season''' was fairly quiet and had the latest start in 30&nbsp;years. The first tropical disturbance originated on January&nbsp;16, although the first named storm, Anacelle, was not upgraded until February&nbsp;8, a record late start. The last storm to dissipate was an unusually late tropical depression in late July. Many of the storms suffered from the effects of [[wind shear]], which contributed to there being only one [[tropical cyclone scales#South-Western Indian Ocean|tropical cyclone]] &ndash; equivalent to a [[Saffir-Simpson Hurricane Scale|minimal hurricane]]. The season also occurred during a powerful [[El Niño]].

Tropical Depression A1, the first of the season, moved throughout most of [[Mozambique]] in January, causing landslides and flooding. One landslide affected [[Milange District]], where many houses were swept into a river. Landslides killed between 87 and 143&nbsp;people in the country. In February, Cyclone Anacelle buffeted several islands with gusty winds after becoming the strongest storm of the season, reaching [[maximum sustained wind]]s of 140&nbsp;km/h (85&nbsp;mph). Although Anacelle was the first named storm of the season, another tropical depression preceded it that crossed Madagascar several times. The depression eventually became Tropical Storm Beltane, and lasted 17&nbsp;days. Beltane caused flooding across Madagascar due to heavy rainfall, which killed one person and left locally heavy crop damage. There were several other disturbances in February, including [[Cyclone Katrina-Victor-Cindy|Cindy]] which dissipated 50&nbsp;days after it originated, as well as a disturbance that brought heavy rainfall to [[Réunion]] and [[Mauritius]]. The rest of the season was fairly quiet, mostly with short-lived tropical disturbances or storms.

==Season summary==
<center>
<timeline>
ImageSize = width:790 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/12/1997 till:01/06/1998
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/12/1997

Colors     =
 id:canvas value:gray(0.88)
 id:GP  value:red
 id:DT  value:rgb(0.38,0.73,1)  legend:Tropical_Disturbance_&lt;_50_km/h_(_&lt;_31_mph)
 id:TD  value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_51&ndash;62_km/h_(32&ndash;38_mph)
 id:MS  value:rgb(0,0.98,0.96)  legend:Moderate_tropical_Storm_=_63&ndash;88_km/h_(39&ndash;54_mph)
 id:SS  value:rgb(0.80,1,1)     legend:Severe_Tropical_Storm_=_89&ndash;117_km/h_(55&ndash;73_mph)
 id:TC  value:rgb(1,1,0.80)     legend:Tropical_Cyclone_=_118&ndash;165_km/h_(74&ndash;102_mph)
 id:IT  value:rgb(1,0.76,0.25)  legend:Intense_=_166&ndash;212_km/h_(103&ndash;132_mph)
 id:VI  value:rgb(1,0.38,0.38)  legend:Very_Intense_&gt;_212_km/h_(_&gt;_132_mph)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=
  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:02/01/1998 till:03/01/1998 color:TD text:"Selwyn"
  from:16/01/1998 till:23/01/1998 color:TD text:"A1"
  from:03/02/1998 till:20/02/1998 color:MS text:"Beltane"
  from:06/02/1998 till:13/02/1998 color:TC text:"Anacelle"
  from:16/02/1998 till:19/02/1998 color:TD text:"Victor-Cindy"
  from:16/02/1998 till:19/02/1998 color:TD text:"D1"
  barset:break
  from:24/02/1998 till:24/02/1998 color:DT text:"D2"
  from:28/02/1998 till:01/03/1998 color:DT text:"D3"
  from:04/03/1998 till:10/03/1998 color:MS text:"Donaline"
  from:09/03/1998 till:18/03/1998 color:SS text:"Elsie"
  from:15/03/1998 till:20/03/1998 color:TD text:"Fiona"
  from:07/04/1998 till:14/04/1998 color:MS text:"Gemma"
  barset:break
  from:22/04/1998 till:22/04/1998 color:TD text:"34S"
  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/12/1997 till:01/01/1998 text:December
  from:01/01/1998 till:01/02/1998 text:January
  from:01/02/1998 till:01/03/1998 text:February
  from:01/03/1998 till:01/04/1998 text:March
  from:01/04/1998 till:01/05/1998 text:April
  from:01/05/1998 till:01/06/1998 text:May
</timeline>
</center>

During the year, the [[Météo-France]] office on [[Réunion]] (MFR) issued warnings for tropical systems in the region as the [[Regional Specialised Meteorological Centre]].<ref name="report"/> In the year, MFR tracked tropical cyclones south of the equator from the coast of Africa to 90°&nbsp;[[Longitude|E]].<ref>{{cite report|author=Philippe Caroff|date=June 2011|title=Operational procedures of TC satellite analysis at RSMC La Reunion|publisher=World Meteorological Organization|accessdate=2013-05-05|url=http://www.wmo.int/pages/prog/www/tcp/documents/RSMCLaReunionforIWSATC.pdf|format=PDF|display-authors=etal}}</ref> The [[Joint Typhoon Warning Center]] also issued warnings in an unofficial capacity.<ref name="atcr">{{cite report|author=[[Joint Typhoon Warning Center]]|publisher=United States Navy|accessdate=2014-05-05|title=Annual Tropical Cyclone Report|year=1999|chapter=South Pacific And South Indian Ocean Tropical Cyclones|url=http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1998atcr.pdf|format=PDF}}</ref>

The season had the latest start in 30&nbsp;years, with the first depression forming in January.<ref name="report"/> The first storm, Anacelle, was not named until February&nbsp;8, which retains the record for the latest date of the first named storm.<ref>{{cite report|author=Philippe Caroff|chapter=Subject B3) When was the earliest tropical cyclone named ? The latest ?|title=Frequently Asked Questions|publisher=Météo-France|accessdate=2014-05-05|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/faq/FAQ_Ang_B.html}}</ref> For the early portion of the season, there were unusually quiet conditions across much of the basin, along with [[high pressure area|higher than normal pressure]]. The [[intertropical convergence zone]] (ITCZ) associated with the [[monsoon]] only became active in February, allowing [[tropical cyclogenesis]] to occur more frequently. There were six tropical storms during the season, of which only one attained tropical cyclone status; these are below the averages of 9 and 4, respectively. No storms attained intense tropical cyclone status. The season's low activity contrasted that of [[1996–97 South-West Indian Ocean cyclone season|the previous season]], which was much more active. There were 18&nbsp;days in which a storm was active, the lowest since [[1982–83 South-West Indian Ocean cyclone season|1982–83]]. An ongoing [[El Niño]] was evident during the season.<ref name="report">{{cite report|author=Guy Le Goff|year=1997|publisher=[[Météo-France]]|title=1997-1998 Cyclone Season in the South-West Indian Ocean|accessdate=2014-05-03|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/archives/publications/saisons_cycloniques/index19971998.html}}</ref>

==Systems==
{{Clear}}

===Tropical Depression A1===
{{Infobox Hurricane Small
|Basin=SWI
|Image=A1 Jan 17 1998 1155Z.png
|Formed=January 16
|Dissipated=January 23
|10-min winds=32
|1-min winds=35
|Pressure=995
}}
The first system of the season originated out of a circulation that persisted in the northern [[Mozambique Channel]] on January&nbsp;15. Convection developed around the center near [[Grande Comore]], meriting its classification Tropical Disturbance 1. Moving southwestward, the system organized into a tropical depression on January&nbsp;17, developing a curved band of convection. Further intensification was halted as the system moved ashore [[Mozambique]] near [[Angoche]]. The depression turned to the south over land, remaining over inland Mozambique for several days.<ref name="report"/> On January&nbsp;18, the JTWC classified the system as Tropical Cyclone 13S, estimating winds of 65&nbsp;km/h (40&nbsp;mph),<ref name="atcr"/> despite the storm being 55&nbsp;km (35&nbsp;mi) inland.<ref name="jgp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary January 1998|year=1998|accessdate=2014-05-05|url=http://www.australiasevereweather.com/cyclones/1998/summ9801.htm}}</ref> The agency quickly downgraded the storm to tropical depression status, but briefly re-upgraded it on January&nbsp;19 as the system crossed over the extreme western Mozambique Channel. The agency again downgraded it after the storm moved ashore. By contrast, the MFR assessed that the system remained a tropical depression and placed the circulation farther inland.<ref name="1bt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 A19798:HSK1398 (1998016S12043)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-05|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998016S12043}}</ref> On January&nbsp;20, the depression turned to the southeast over open waters, influenced by a [[trough (meteorology)|trough]] to the south. Despite warmer waters, the system was unable to re-intensify much due to the presence of wind shear,<ref name="report"/> although the JTWC again upgraded the system to tropical storm status for a third and final time.<ref name="1bt"/> The depression approached tropical storm intensify after developing increased convection over the center, but it weakened again on January&nbsp;22. On the next day, the system dissipated just off the southern coast of Madagascar.<ref name="report"/>

In its formative stages, the depression dropped beneficial rainfall in the [[Comoros]], reaching {{convert|163|mm|in|abbr=on}} at [[Prince Said Ibrahim International Airport]]. While the depression was over land, the plume of warm air from the open waters sustained heavy convection over the circulation, which dropped heavy rainfall across eastern Mozambique.<ref name="report"/> The rains caused landslides and flooding in the country, which disrupted transport in three provinces, damaging several bridges.<ref>{{cite report|work=United Nations Department of Humanitarian Affairs|date=1998-01-26|title=Mozambique Landslides Report No. 1|publisher=[[ReliefWeb]]|accessdate=2014-05-05|url=http://reliefweb.int/report/mozambique/mozambique-landslides-report-no-1}}</ref><ref>{{cite report|work=Pan African News Agency|date=1998-01-18|title=Heavy Rains Cut Niassa's Roads In Mozambique|publisher=ReliefWeb|accessdate=2014-05-05|url=http://reliefweb.int/report/mozambique/heavy-rains-cut-niassas-roads-mozambique}}</ref> The most significant landslide occurred in [[Milange District]] at nighttime, which swept houses into a river; about 2,500&nbsp;people were left homeless in the village. There were 73&nbsp;confirmed fatalities, with another 70&nbsp;people missing and presumed killed.<ref>{{cite news|agency=Reuters|date=1998-01-29|title=Landslide death toll might hit 143|publisher=ReliefWeb|accessdate=2014-05-05|url=http://reliefweb.int/report/mozambique/landslide-death-toll-might-hit-143}}</ref> However; the International Disaster Database (EM-DAT) later placed the total number of casualties at 87.{{EM-DAT}} Rainfall also extended into [[Malawi]], where villages were flooded and crops were damaged.<ref name="jgp"/> While the system was accelerating to the southeast away from Mozambique, it produced gale force winds on [[Europa Island]].<ref name="report"/>
{{clear}}

===Moderate Tropical Storm Beltane===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Beltane Feb 16 1998 1124Z.png
|Track=Beltane 1998 track.png
|Formed=February 3
|Dissipated=February 20
|10-min winds=35
|1-min winds=40
|Pressure=992
}}
A northerly flow produced a [[low pressure area]] on February&nbsp;1 in the central Mozambique Channel. Influenced by the monsoon trough, the system developed a distinct circulation on February&nbsp;3 near [[Juan de Nova Island]], becoming a tropical disturbance and bringing gusts of 50&nbsp;km/h (30&nbsp;mph) to the island. The convection organized around the circulation while moving eastward. Conditions were favorable for further strengthening, although the system made landfall in western Madagascar between [[Maintirano]] and [[Morondava]] on February&nbsp;5. After progressing slightly inland, the disturbance looped and turned to the south. The circulation became difficult to locate, but surface observations helped track the circulation southward through the country. Late on February&nbsp;8, the system reached the open waters south of Madagascar and quickly redeveloped convection southeast of the center, displaced by wind shear, and it was reclassified as a [[subtropical cyclone|subtropical depression]].<ref name="report"/> The JTWC briefly classified it as Tropical Cyclone 21S on February&nbsp;9 with winds of 65&nbsp;km/h (40&nbsp;mph).<ref name="atcr"/>

A building ridge to the south turned the system northeastward on February&nbsp;10 and later to the northwest, bringing it back over southwestern Madagascar. On February&nbsp;11, the circulation again reentered the Mozambique Channel, and subsequently the thunderstorms rebuilt over the poorly defined center. A trough behind the ridge allowed the system to turn to the southwest and later southeast. An increase in convection on February&nbsp;15 organized into a curved band, and MFR upgraded the system to Tropical Storm Beltane on the next day off the west coast of Madagascar.<ref name="report"/> The JTWC also classified the system as Tropical Cyclone 23S on February&nbsp;16,<ref name="atcr"/> possibly due to the extended duration between issuing advisories.<ref name="mgp"/> Strong wind shear stripped the convection from the center as Beltane approached southwestern Madagascar on February&nbsp;17. Another building ridge turned the weakened depression to the northwest across the Mozambique Channel, finally dissipating on February&nbsp;20 near the mouth of the [[Zambezi]]. The remnants later moved across Mozambique accompanied by locally heavy rainfall.<ref name="report"/>

Due to its trajectories across Madagascar, Beltane brought heavy rainfall to the country.<ref name="report"/> The persistent precipitation damaged crops, up to 100% in some areas, and forced thousands to evacuate their houses. Floodwaters covered the village of [[Vohipeno]], killing one person.<ref>{{Cite report|title=Madagascar: Post-Flood Food Security and Cholera Prevention|date=1998-04-03|publisher=International Federation of Red Cross and Red Crescent Societies|accessdate=2014-05-06|url=http://www.ifrc.org/docs/appeals/98/1398.PDF|format=PDF}}</ref> Several roads and bridges were also washed away.<ref>{{Cite report|title=Madagascar: Post-Flood Food Security and Cholera Prevention|date=1999-03-15|publisher=International Federation of Red Cross and Red Crescent Societies|accessdate=2014-05-06|url=https://www.ifrc.org/docs/appeals/98/139801.pdf|format=PDF}}</ref>
{{clear}}

===Tropical Cyclone Anacelle===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Anacelle Feb 11 1998 1038Z.png
|Track=Anacelle 1998 track.png
|Formed=February 6
|Dissipated=February 13
|10-min winds=75
|1-min winds=115
|Pressure=950
}}
On February&nbsp;5, the ITCZ spawned an area of convection about {{Convert|1000|km|mi|abbr=on}} southwest of [[Diego Garcia]]. The system slowly organized, aided by warm waters and weakening wind shear. On February&nbsp;6, it developed into a tropical disturbance, and became Tropical Storm Anacelle two days later.<ref name="report"/> Also on February&nbsp;8, the JTWC initiated advisories on the storm as Tropical Cyclone 20S.<ref name="atcr"/> The storm initially moved westward due to a [[ridge (meteorology)|ridge]] to the north, although the motion shifted to the southwest on February&nbsp;9 due to a trough and the influence of the system that would become Tropical Storm Beltane. Anacelle developed an [[eye (cyclone)|eye]] feature on February&nbsp;10, indicating that it attained tropical cyclone status, or winds of at least 120&nbsp;km/h (75&nbsp;mph). Around that time, Anacelle passed just west of [[St. Brandon]].<ref name="report"/> On February&nbsp;11, the cyclone passed about 100&nbsp;km (60&nbsp;mi) east of [[Mauritius]]. Shortly thereafter, Anacelle attained peak winds while presenting a {{convert|30|km|mi|abbr=on}} eye.<ref name="report"/> It reached peak winds of 140&nbsp;km/h (85&nbsp;mph), according to MFR, while the JTWC estimated peak winds of 215&nbsp;km/h (130&nbsp;mph).<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 Anacelle (1998036S13066)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-05|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998036S13066}}</ref> An approaching trough weakened the cyclone and steered it southeastward, causing the eye to disappear. On February&nbsp;13, Anacelle became [[extratropical cyclone|extratropical]], although the remnants continued southeastward, passing near [[Île Amsterdam]] on the next day and re-intensifying on February&nbsp;15 in the southern Indian Ocean.<ref name="report"/>

While passing near St. Brandon, Anacelle produced peak winds of {{Convert|101|km/h|mph|abbr=on}}, with gusts to {{convert|151|km/h|mph|abbr=on}}. Later, the storm produced gusty winds of less than 120&nbsp;km/h (75&nbsp;mph) on Mauritius,<ref name="report"/> along with {{convert|125|mm|in|abbr=on}} of rainfall at [[Port Louis, Mauritius|Port Louis]].<ref name="mgp">{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary March 1998|year=1998|accessdate=2014-05-05|url=http://www.australiasevereweather.com/cyclones/1998/summ9803.htm}}</ref> The extratropical remnants also brought gale force winds to Île Amsterdam.<ref name="report"/>
{{clear}}

===Moderate Tropical Storm Donaline===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Donaline Mar 7 1998 0934Z.png
|Track=Donaline 1998 track.png
|Formed=March 4
|Dissipated=March 10
|10-min winds=40
|1-min winds=55
|Pressure=988
}}
A large area of low pressure between the [[Chagos Archipelago]] and the [[Mascarene Islands]] spawned a small tropical disturbance on March&nbsp;4. Moving southeastward, the system slowly developed as wind shear in the region slowly decreased. Despite only being a tropical depression, it was named Donaline on March&nbsp;5.<ref name="report"/> On the next day, the JTWC classified it as Tropical Cyclone 26S.<ref name="atcr"/> Increased convection organized into a [[central dense overcast]], and Donaline intensified into a minimal tropical storm,<ref name="report"/> reaching peak winds of 75&nbsp;km/h (55&nbsp;mph) according to the MFR. In contrast, the JTWC estimated peak winds of 100&nbsp;km/h (65&nbsp;mph).<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 Donaline (1998064S13061)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-06|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998064S13061}}</ref> The wind shear returned, causing weakening and dislocating the circulation from the convection. On March&nbsp;10, Donaline became extratropical and was absorbed by a cold front two days later.<ref name="report"/>
{{clear}}

===Severe Tropical Storm Elsie===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Elsie tir.gif
|Track=Elsie 1998 track.png
|Formed=March 9 (entered the basin)
|Dissipated=March 18
|10-min winds=55
|1-min winds=90
|Pressure=975
}}
On March&nbsp;7, a low pressure area persisted west of the [[Cocos Islands]] in the Australian basin. It drifted westward, entering the south-west Indian Ocean on March&nbsp;9 as a tropical disturbance. It remained weak, with little convection over the center. [[Outflow (meteorology)|Outflow]] gradually increased, although satellite imagery was limited in the region to only one image per day. Late on March&nbsp;12, the satellite imagery indicated a well-defined tropical storm with curved convection, and the MFR immediately upgraded it to Severe Tropical Storm Elsie,<ref name="report"/> estimating peak winds of 100&nbsp;km/h (65&nbsp;mph). By contrast, the JTWC estimated winds of 165&nbsp;km/h (105&nbsp;mph),<ref name="ebt">{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 Elsie (1998064S09093)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-06|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998064S09093}}</ref> having classified it as Tropical Cyclone 27S that day.<ref name="atcr"/> By that time, the storm was moving steadily to the southwest due to a trough in the region related to the remnants of Donaline. Increased wind shear caused steady weakening, removing the circulation from the convection on February&nbsp;14. On the next day, Elsie weakened to tropical depression status as it curved southward. A building ridge to the south turned the system to the east, gradually looping back to the northwest. Elsie eventually dissipated on March&nbsp;20.<ref name="report"/>
{{clear}}

===Tropical Depression Fiona===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Fiona Mar 18 1998 1055Z.png
|Track=Fiona 1998 track.png
|Formed=March 15
|Dissipated=March 20
|10-min winds=30
|1-min winds=35
|Pressure=995
}}
While Elsie was weakening and turning to the south, another system was forming near St. Brandon. Convection associated with the monsoon trough persisted on March&nbsp;13, becoming a tropical disturbance two days later. The ridge steered the system to the southwest toward Rodigues, and conditions were expected to allow for intensification. As a result, the Mauritius Meteorological Service named the disturbance as Fiona on March&nbsp;16. On the next day, Fiona intensified into a tropical depression,<ref name="report"/> reaching peak winds of only 55&nbsp;km/h (35&nbsp;mph).<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 Fiona (1998073S11065)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998073S11065}}</ref> Also on March&nbsp;17, the JTWC initiated advisories on Tropical Cyclone 28S.<ref name="atcr"/> Around this time, Fiona passed about {{convert|200|km|mi|abbr=on}} southeast of St. Brandon. After peaking, the convection decreased due to wind shear, causing the winds to fluctuate. On March&nbsp;20, the circulation became exposed from the thunderstorms and approached 80&nbsp;km (45&nbsp;mi) east of Mauritius,<ref name="report"/> producing wind gusts of 70&nbsp;km/h (44&nbsp;mph).<ref name="mgp"/> The next day, Fiona dissipated into an approaching cold front.<ref name="report"/> 
{{clear}}

===Moderate Tropical Storm Gemma===
{{Infobox Hurricane Small
|Basin=SWI
|Image=Gemma Apr 10 1998 1002Z.png
|Track=Gemma 1998 track.png
|Formed=April 7
|Dissipated=April 14
|10-min winds=45
|1-min winds=70
|Pressure=985
}}
After an extended period of inactivity, the ITCZ produced two areas of convection &ndash; one was located about {{Convert|550|km|mi|abbr=on}} south-southwest of Diego Garcia, and the other was located 900&nbsp;km (560&nbsp;mi) east-southeast of that system. Both were classified as tropical disturbances on April&nbsp;7 and subsequently [[Fujiwhara effect|interacted]] with each other.<ref name="report"/> The eastern system, classified as Tropical Cyclone 33S,<ref name="atcr"/> quickly dissipated due to strong wind shear and was absorbed into the western system. The disturbance continued to organize and developed a central dense overcast over the center, becoming Tropical Storm Gemma on April&nbsp;8. A ridge and a trough steered the storm to the southeast and later to the east.<ref name="report"/> On April&nbsp;9, Gemma attained peak winds of 85&nbsp;km/h (50&nbsp;mph), according to the MFR, while the JTWC estimated 130&nbsp;km/h (80&nbsp;mph) winds.<ref>{{cite report|author=Kenneth R. Knapp|author2=Michael C. Kruk|author3=David H. Levinson|author4=Howard J. Diamond|author5=Charles J. Neumann|year=2010|work=The International Best Track Archive for Climate Stewardship (IBTrACS): Unifying tropical cyclone best track data|title=1998 Gemma (1998096S13071)|publisher=Bulletin of the American Meteorological Society|accessdate=2014-05-07|url=http://atms.unca.edu/ibtracs/ibtracs_v03r04/browse-ibtracs/index.php?name=v03r04-1998096S13071}}</ref> As with most other storms in the year, increased wind shear caused the storm to weaken. The weaker system isolated it from the upper-level steering, causing the circulation to loop southwestward. On April&nbsp;16, Gemma dissipated far to the east of Rodrigues.<ref name="report"/>
{{clear}}

===Other systems===
In addition to the named systems, there were nine tropical depressions or disturbances tracked by the MFR,<ref name="report"/> and several by other agencies.

On January&nbsp;2, the tropical depression that was once [[1997–98 Australian region cyclone season#Severe Tropical Cyclone Selwyn|Cyclone Selwyn]] crossed 90°&nbsp;E from the Australian region, but dissipated the next day.<ref>{{cite report|title=Tropical Cyclone Selwyn|publisher=Bureau of Meteorology|url=http://www.bom.gov.au/cyclone/history/pdf/selwyn.pdf|accessdate=2014-05-05|format=PDF}}</ref>

[[File:Victor-Cindy 1998 track.png|thumb|left|Cyclone Victor-Cindy's path, with the points indicating the storm's position and intensity at six-hour intervals, and being colored using the [[Saffir–Simpson hurricane wind scale]].]]
On February&nbsp;8, a tropical low formed just north of [[Western Australia]] from the remnants of Cyclone Katrina, which earlier formed off the east coast of Australia. The low moved generally westward due to a strong ridge to the south. Given the name [[Cyclone Katrina-Victor-Cindy|Victor]], the storm intensified to a peak of 120&nbsp;km/h (75&nbsp;mph) before weakening steadily due to increased wind shear. On February&nbsp;14, the storm weakened to tropical depression status.<ref name="vbom">{{cite report|title=Tropical Cyclone Victor|publisher=Bureau of Meteorology|accessdate=2014-05-06|url=http://www.bom.gov.au/cyclone/history/pdf/victor.pdf|format=PDF}}</ref> Victor crossed into the south-west Indian Ocean on February&nbsp;16 with a well-defined circulation but little convection. Despite being downgraded to a tropical disturbance, the system was named Cindy by the Mauritius Meteorological Service on February&nbsp;16. The system continued gradually weakening while turning more to the southwest, dissipating on February&nbsp;19.<ref name="report"/> This marked a 50&#8209;day period in which the same system was active.<ref name="vbom"/>

After Cyclone Anacelle became extratropical, an area of convection developed about {{convert|700|km|mi|abbr=on}} northeast of Rodrigues on February&nbsp;14. The circulation moved southwestward, organizing into Tropical Disturbance D1 on February&nbsp;16. Later that day, it was upgraded to tropical depression status after the convection organized into a central dense overcast,<ref name="report"/> and on the same day the JTWC classified it as Tropical Cyclone 24S.<ref name="atcr"/> Increased wind shear weakened the depression as a trough turned it more to the southeast. On February&nbsp;19, the trough absorbed the system.<ref name="report"/>

After the disturbance dissipated, a large low pressure area persisted east of Madagascar with several associated circulations. On February&nbsp;24, Tropical Disturbance D2 passed about 160&nbsp;km (100&nbsp;mi) west of Réunion, and continued to the southeast, passing south of Mauritius. Wind shear stripped the convection from the center and caused it to dissipate. Over a nine-day period, the system dropped nearly {{convert|2|m|ft|abbr=on}} of rainfall in portions of Réunion, including nearly {{Convert|700|mm|in|abbr=on}} at [[Salazie]] on February&nbsp;24; at that station, {{convert|255|mm|in|abbr=on}} of precipitation fell in just three hours. Gusts reached {{Convert|100|km/h|mph|abbr=on}} in some locations.<ref name="report"/> The storm caused flooding and landslides on the island as well as power outages. Rainfall also reached {{convert|240|mm|in|Abbr=on}} on Mauritius. Residents were generally caught off guard by the storm due to the lack of warnings.<ref name="mgp"/> Tropical Disturbance D3 also developed before March.<ref name="report"/>

Although Tropical Storm Gemma was the final named storm, there were four subsequent tropical disturbances. The first formed toward the end of April after Gemma dissipated in the same general region.<ref name="report"/> Named Tropical Cyclone 34S by the JTWC, it moved westward throughout its duration but failed to intensify due to wind shear. On April&nbsp;22, the system dissipated,<ref>{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary April 1998|year=1998|accessdate=2014-05-08|url=http://www.australiasevereweather.com/cyclones/1998/summ9804.htm}}</ref> never having developed beyond tropical disturbance status. The last disturbance of the year formed on July&nbsp;20 about 1480&nbsp;km (920&nbsp;mi) east of Diego Garcia. The system moved generally southwestward, dissipating on July&nbsp;23 due to wind shear. At the time, the tropical cyclone year for the basin lasted from August&nbsp;1 to July&nbsp;31 of the following year, although the JTWC considers the start of the tropical cyclone year to begin on July&nbsp;1. As a result, the MFR considered the system Tropical Disturbance H4 while the JTWC classified it as Tropical Cyclone 01S.<ref>{{cite web|publisher=Gary Padgett|title=Monthly Global Tropical Cyclone Summary July 1998|year=1998|accessdate=2014-05-08|url=http://www.australiasevereweather.com/cyclones/1999/summ9807.htm}}</ref>

==Storm names==
A tropical disturbance is named when it reaches moderate tropical storm strength. If a tropical disturbance reaches moderate tropical storm status west of [[55th meridian east|55°E]], then the Sub-regional Tropical Cyclone Advisory Centre in [[Madagascar]] assigns the appropriate name to the storm. If a tropical disturbance reaches moderate tropical storm status between [[55th meridian east|55°E]] and [[90th meridian east|90°E]], then the Sub-regional Tropical Cyclone Advisory Centre in [[Mauritius]] assigns the appropriate name to the storm. A new annual list is used every year so no names are retired.<ref>{{cite report|author=Guy Le Goff|year=1997|publisher=[[Météo-France]]|title=1996-1997 Cyclone Season in the South-West Indian Ocean|page=78|accessdate=2014-05-08|url=http://www.meteo.fr/temps/domtom/La_Reunion/webcmrs9.0/anglais/archives/publications/saisons_cycloniques/index19961997.html}}</ref> 
{| width="90%"
|
*Anacelle
*Beltane
*Cindy
*Donaline
*Elsie
*Fiona
*Gemma
*{{tcname unused|Hillary}}
|
*{{tcname unused|Inese}}
*{{tcname unused|Judith}}
*{{tcname unused|Kimmy}}
*{{tcname unused|Lynn}}
*{{tcname unused|Monique}}
*{{tcname unused|Nicole}}
*{{tcname unused|Olivette}}
|
*{{tcname unused|Prisca}}
*{{tcname unused|Renette}}
*{{tcname unused|Sarah}}
*{{tcname unused|Tania}}
*{{tcname unused|Valencia}}
*{{tcname unused|Wanicky}}
*{{tcname unused|Yandah}}
|}

==See also==
{{Portal|Tropical cyclones}}
*[[List of Southern Hemisphere tropical cyclone seasons]]
*Atlantic hurricane seasons: [[1997 Atlantic hurricane season|1997]], [[1998 Atlantic hurricane season|1998]]
*Pacific hurricane seasons: [[1997 Pacific hurricane season|1997]], [[1998 Pacific hurricane season|1998]]
*Pacific typhoon seasons: [[1997 Pacific typhoon season|1997]], [[1998 Pacific typhoon season|1998]]
*North Indian Ocean cyclone seasons: [[1997 North Indian Ocean cyclone season|1997]], [[1998 North Indian Ocean cyclone season|1998]]

==References==
{{Reflist|30em}}

==External links==
* [http://www.usno.navy.mil/JTWC Joint Typhoon Warning Center (JTWC)]
* [http://www.meteo.fr/temps/domtom/La_Reunion/ Météo France (RSMC La Réunion)]
* [http://www.wmo.int/index-en.html World Meteorological Organization]
* [http://ftp.wmo.int/pages/prog/www/TCP_vO/Reports/RA1TCC14-Report.doc RA I Tropical Cyclone Committee Final Report]
* [http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1998atcr/pdf.html Joint Typhoon Warning Center 1998 ATCR]
* [http://www.meteo.fr/temps/domtom/La_Reunion/base_cyclone/Saison/Saison_1998_1997.html 1997-98 Best Track Data from Météo France]
* [http://australiasevereweather.com/cyclones/tropical_cyclones_1998_summaries_and_track_data.htm September 1997 to June 1998 Tropical Cyclone Summaries and Operational Track Data]

{{1997–98 South-West Indian Ocean cyclone season buttons}}
{{TC Decades|Year=1990|basin=South-West Indian Ocean|type=cyclone|shem=yes}}
{{good article}}

{{DEFAULTSORT:1997-98 South-West Indian Ocean Cyclone Season}}
[[Category:1997–98 Southern Hemisphere tropical cyclone season|*]]
[[Category:1997–98 South-West Indian Ocean cyclone season| ]]
[[Category:South-West Indian Ocean cyclone seasons]]