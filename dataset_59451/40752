{{Infobox Organization
|name         = NOVA-MBA Association
|image        = NOVA-MBA_Association_Logo_(small)_(2012).jpg
|image_border =
|size         = 
|caption      =
|map          =
|msize        =
|mcaption     =
|motto        =
|formation    = 2001
|extinction   =
|type         = [[Alumni association]], [[Research institute]], [[think tank]]
|headquarters = [[New York City]], [[United States]]
|location     = Global
|membership   = ~1,500
|language     = Italian, English
|leader_title = 
|leader_name  = 
|key_people   = 
|num_staff    = 25 officers
|budget       = $88,946<ref name="bare_url_a">{{cite web|title=NOVA-MBA Organization Overview|url=http://nccsdataweb.urban.org/orgs/profile/134184218?popup=1|publisher=NCCS - National Center For Charitable Statistics|accessdate=1 May 2012}}</ref>  (2010)
|website      = [http://www.nova-mba.org/ nova-mba.org]
}}

'''NOVA-MBA Association''' is a nonprofit organization founded in 2001 and incorporated in 2002. The organization's mission is to "Facilitate the participation of Italian Talents to top US [[MBA]] programs, contribute to the success of Italy and Italian Companies by promoting Italian MBA students and alumni recruiting, and create a strong, lasting and highly valuable network of successful executives, entrepreneurs and professionals among Italian MBA students and alumni".

==Membership==

===Member schools===

NOVA membership has historically been restricted to Italian [[MBA]] graduates of the seventeen US institutions included in the list below.  As of 2013, London Business School has been included as a member.

{| class="wikitable" style="text-align: center;"
|-
! Business School
! University
|-
| [[Booth School of Business]]
| [[University of Chicago]]
|-
| [[Columbia Business School]]
| [[Columbia University]]
|-
| [[Daniels College of Business]]
| [[University of Denver]]
|-
| [[Fuqua School of Business]]
| [[Duke University]]
|-
| [[Haas School of Business]]
| [[University of California, Berkeley]]
|-
| [[Harvard Business School]]
| [[Harvard University]]
|-
| [[Kelley School of Business]]
| [[Indiana University]]
|-
| [[Kellogg School of Management]]
| [[Northwestern University]]
|-
| [[McCombs School of Business]]
| [[University of Texas at Austin]]
|-
| [[MIT Sloan School of Management]]
| [[Massachusetts Institute of Technology]]
|-
| [[New York University Stern School of Business]]
| [[New York University]]
|-
| [[Samuel Curtis Johnson Graduate School of Management]]
| [[Cornell University]]
|-
| [[Stanford Graduate School of Business]]
| [[Stanford University]]
|-
| [[Tepper School of Business]]
| [[Carnegie Mellon University]]
|-
| [[Tuck School of Business]]
| [[Dartmouth College]]
|-
| [[UCLA Anderson School of Management]]
| [[University of California, Los Angeles]]
|-
| [[Wharton School of the University of Pennsylvania]]
| [[University of Pennsylvania]]
|-
| [[London Business School]]
| [[University of London]]
|}

However, graduates of other programs figuring in the [[list of United States graduate business school rankings]], and most notably the [[Yale School of Management]], the [[Ross School of Business]], and the [[Darden Graduate School of Business Administration]] are generally accepted as well.

Note that all six [[Ivy League]] business schools are included (Wharton, Tuck, HBS, CBS, Cornell, Yale). [[Brown University]] and [[Princeton University]] do not have business schools (although Brown offers a joint Executive MBA program with [[Spain]]'s [[Instituto de Empresa]] Business School).<ref>[http://www.iebrown.com/executive-mba.php IE - Brown joint EMBA]</ref>

As of 2012, the association is in the process of evaluating extending membership to graduates of Top European MBAs as well (such as [[INSEAD]], [[International Institute for Management Development|IMD]] and others).<ref name=presletter>[http://www.nova-mba.org/article.html?aid=257&nl=81 "NOVA President's letter"], New York, 22 April 2012. Retrieved on 28 April 2012.</ref>

===Student clubs in US MBA Programs===

As of 2012, an Italian student club formally affiliated with NOVA exists only at Columbia Business School.<ref>[http://www4.gsb.columbia.edu/cbs-directory/student-groups "Student Groups at Columbia Business School"]. Retrieved on 6 May 2012.</ref>

In other MBA programs, the number of Italian students is typically less than 3-4 per year, which usually implies that Italian clubs do not formally exist, and students are typically part of broader European groups, such as, for example, Harvard Business School's European Club.<ref>[http://www.hbs.edu/mba/studentlife/clubs/european.html "HBS European Club". Retrieved on 6 May 2012.]</ref>

===Size of membership base===

It is estimated that a total of ~2,000 Italians have obtained MBA degrees from top US universities from 1950 to 2012, or roughly 30 students per year. The number of graduating Italian students in top MBA programs varies significantly on a year by year basis and peaked at 86 for the class of 2010.<ref name=presletter />

As of 2012, NOVA had a total of ~1,500 registered members in its website database. Given the general preference of Italian students for Northeast US business school, [[New York City]]- and [[Boston]]-based schools (Columbia, NYU, Harvard, MIT) have the largest number of alumni in the association.

===Notable members===

Notable NOVA members include ENI CEO [[Paolo Scaroni]], [[Telecom Italia]] and [[Assicurazioni Generali]] chairman [[Gabriele Galateri di Genola]], [[Vodafone Group]] CEO [[Vittorio Colao]], and former [[Italian Minister of Economic Development]] [[Corrado Passera]].

==Yearly conference==

Arguably, the most notable event hosted by the Association is its yearly conference. Since 1999, the Association has hosted a conference each year in the US. In 2011 two conferences were hosted - one in Italy for the 10th anniversary of the Association and the regular conference in the US.

The NOVA conference typically attracts between 100-300 individuals among current students, MBA alumni, corporate recruiters, and guests. The format is generally a 2-day weekend event, with a gala dinner on the Saturday evening. The conference is held in Autumn, typically in October or November.

===Location and theme===

* 2012: Boston: "WAKE UP ITALY!": "Strategies, opportunities and challenges to re-launch the growth of Italy" <ref>[http://www.novaconference.org/ NOVA Conference Website]</ref>
* 10th Year Anniversary Event [2011]: "Milan, Italy / Una cultura della crescita e delle regole per salvare il Paese dalla catastrofe"<ref>{{cite web|title=NOVA 10th Anniversary (Decennale)|url=http://www.unleashingideas.org/events/view/17638|publisher=Kauffman Foundation - Global Entrepreneurship Week Events|accessdate=1 May 2012}}</ref>
* 2011: New York, Columbia Business School / NYU, "Italian citizenship, European passport, Global competition"<ref>{{cite web|title=Student Club Events - XII NOVA Conference|url=http://www.stern.nyu.edu/experience-stern/news-events/nova-2011|publisher=New York University - Stern School of Business|accessdate=1 May 2012}}</ref><ref name="rolandberger">{{cite web|title=12th Nova Conference, November 12, 2011|url=http://www.rolandberger.it/news/Nova_Conference/2011-11-16-Nova_Conference_en.html|publisher=Roland Berger Strategy Consultants|accessdate=1 May 2012}}</ref>
* 2010: New York, Columbia Business School / NYU, "Italy 2020: Inspiring Our Future"<ref>{{cite web|title=XI Nova Conference - Italy 2020: Inspiring Our Future|url=http://cbs.campusgroups.com/novaconf/home/|publisher=Columbia Business School - Campus Groups|accessdate=1 May 2012}}</ref><ref>{{cite web|title=NOVA Conference: Italy 2020 Inspiring Our Future|url=http://www.i-italy.org/16223/nova-conference-italy-2020-inspiring-our-future|publisher=i-Italy|accessdate=1 May 2012}}</ref>
* 2009: Boston, MIT / Harvard Business School: "Relentless InNOVAtion - International perspectives at the crossroads of business, technology, and public policy"<ref>{{cite web|title=X NOVA Conference|url=http://novaconference.mit.edu/|publisher=Massachusetts Institute of Technology|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Gli studenti MBA italiani a Boston|url=http://www.voceditalia.it/articolo.asp?id=42333|publisher=La Voce - Quotidiano Indipendente|accessdate=3 May 2012}}</ref>  (in collaboration with the European Student Club Conference at HBS on 'Financing Innovation: VC/PE in Europe')<ref>{{cite web|title=Financing Innovation Conference|url=http://novaconference.mit.edu/images/stories/About_Us/10th_Innovation_Conference_Flyer_Harvard.pdf|publisher=European Student Club of HBS|accessdate=4 May 2012}}</ref>
* 2008: New York, Columbia Business School / NYU : "Accelerating the Development of Italian Productivity: The Key Engines of Growth"
* 2007: New York, Columbia Business School / NYU: "Positive aspects of polarization: the Italian advantage"<ref>{{cite web|title=NOVA, talenti per le imprese|url=http://www.oggi7.info/2007/11/19/480-nova-talenti-le-imprese|publisher=Oggi 7 - Magazine Domenicale di America Oggi|accessdate=3 May 2012}}</ref>
* 2006: Stanford<ref>{{cite web|title=News - Conference Asks: "What Drives Innovation?"|url=http://www.gsb.stanford.edu/news/headlines/cgbe_innovationconf.shtml|publisher=Stanford Graduate School of Business|accessdate=1 May 2012}}</ref>
* 2005: Boston, Harvard Business School / MIT
* 2004: New York, Columbia Business School / NYU
* 2003: Chicago/Kellogg/Wharton
* 2002: Boston, Harvard Business School / MIT
* 2001: New York, Columbia Business School / NYU

===Notable speakers===

* 2012: [[Ferruccio Ferragamo]], [[Francesco Venturini]], [[Bruno Spagnolini]], [[Fabio Sattin]], [[Luca Zanotti]], [[Alessandro Piol]]
* 10th Year Anniversary Event [2011]: [[Roger Abravanel]], [[Enrico Letta]], [[Francesco Micheli]], [[M. Pescarmona]], [[A. Merli]], [[M. Valla]], et al.
* 2011: [[Gerardo Braggiotti]], [[Gianfelice Rocca]], [[Andrea Morante]], [[Luigi Simonelli]], [[Elio Leoni-Sceti]]
* 2010: [[Domenico Siniscalco]], [[Francesco Starace]], [[Stefano Pessina]], [[Victor Massiah]], [[Lorenzo Pelliccioli]]
* 2009: [[Massimo D’Amore]], [[Gianfilippo Cuneo]], [[Nicola Pianon]], [[Charles Kane (business executive)|Charles Kane]], [[Richard Boly]], [[Richard Spillenkothen]], [[Beppe Severgnini]]
* 2008: [[Gabriele Galateri di Genola]], [[Paolo Pellegrini]], [[Roger Abravanel]],  [[Fabio Cane’]], [[Alessandro Pansa]], [[Stefano Aversa]], [[Carlo Scognamiglio Pasini]]
* 2007: [[Matteo Arpe]], [[Carlo Buora]], [[Roberto Crapelli]], [[Fernando Napolitano]], [[Paolo Timoni]]
* 2006: [[Eric Schmidt]], [[Diego Piacentini]], [[Marco Milani]], [[Paul Romer]], [[Guerrino De Luca]], [[Roberto Crea]]
* 2005: [[Fulvio Conti]], [[Alberto Alesina]], [[Mario Baldassarri]], [[Francesco Giavazzi]]
* 2004: [[Mario Draghi]], [[Lucio Stanca]], [[Aswath Damodaran]], [[Francesco Gianni]]
* 2003: [[Maurizio Sella]], [[Ferdinando Beccalli]], [[Paolo Timoni]], [[Luigi Zingales]], [[Diego Piacentini]]
* 2002: [[Franco Modigliani]], [[Mario Baldassarri]], [[Gerardo Braggiotti]]
* 2001: [[Franco Modigliani]], [[Paolo Scaroni]], [[Gianfilippo Cuneo]], [[Daniele Bodini]], [[Joseph Stiglitz]]

==Other activities and initiatives==

===Press Relations===

NOVA officers and members regularly engage with the national and international press. Articles quoting NOVA officers and members have appeared in [[The Wall Street Journal]],<ref>[https://online.wsj.com/article/SB10001424052748704081604576144013418685144.html#articleTabs%3Darticle "Italy Takes Steps to Address Flight of Talented Youth"],''[[The Wall Street Journal]]'', Rome, 11 March 2011. Retrieved on 28 April 2012.</ref> [[Time Magazine]],<ref>[http://www.time.com/time/magazine/article/0,9171,2024136-2,00.html "Arrivederci, Italia: Why Young Italians Are Leaving"],''[[Time (magazine)]]'', Rome, 18 October 2010. Retrieved on 28 April 2012.</ref> [[Radio 24 (Italy)]],<ref>[http://www.radio24.ilsole24ore.com/player/player.php?filename=120331-giovani-talenti.mp3 "Giovani talenti"],''[[Radio 24 (Italy)]]'', Milan, 31 March 2012. Retrieved on 28 April 2012.</ref><ref>[http://www.radio24.ilsole24ore.com/popup/player.php?filename=101218-giovani-talenti.mp3 "Giovani talenti"],''[[Radio 24 (Italy)]]'', Milan, 18 December 2010. Retrieved on 28 April 2012.</ref> [[YouDem Tv]].<ref>[http://www.youdem.tv/doc/225429/laltra-italia-linea-mondo-del-20012011.htm "YouDem.TV: L<nowiki>'</nowiki>altra Italia. Linea Mondo"],''[[YouDem Tv]]'', Milan, 20 January 2011. Retrieved on 28 April 2012.</ref>

NOVA historically had a partnership with leading Italian newspaper [[Corriere della Sera]], with regular articles appearing until 2002.<ref name="corriere">{{cite web|title=Nova , lettere dalle Business School|url=http://www.corriere.it/speciali/american_business/business.shtml|publisher=Corriere Della Sera|accessdate=29 April 2012}}</ref>

===Policy Affairs===

NOVA officers publicly engage with Italian members of parliament on matters relevant to members of the association. Occasionally, this dialog happens through the press.<ref name="corriere" /><ref>[http://www.radio24.ilsole24ore.com/blog/nava/?p=204 "Il DDL Controesodo copre anche gli studenti Mba?"], Milan, 14 June 2010. Retrieved on 28 April 2012.</ref>

NOVA has been among the non-governmental associations contributing to the improvement and ongoing dialogue<ref>{{cite web|title=Retaining Talent in Italy. IESE Alumni Gather in Milan for Continuous Education Session|url=http://www.iese.edu/Aplicaciones/News/view.asp?lang=en&id=2884|publisher=IESE Business School - Top Stories|accessdate=29 April 2012}}</ref><ref>{{cite web|title=Italiani all'estero, "Controesodo - Viva l'Italia": a New York|url=http://www.italiachiamaitalia.net/news/121/ARTICLE/24722/2011-01-17.html|publisher=Italia Chiama Italia|accessdate=29 April 2012}}</ref>  relative to the [[:it:Associazione TrecentoSessanta#Controesodo – Talenti in Movimento|"Controesodo" bipartizan law]], passed by the Italian Parliament in 2010 to incentivize the return of talented Italians to the country.

As of 2012, four officers were part of the Policy Affairs group.<ref name=OffDir />

===Think Tank===

As of 2012, the Association is seeking funding to create a Think Tank centered on the study of managerial and entrepreneurial professions in Italy.<ref name=presletter />

===Other Activities===

Besides the yearly conferences, NOVA-MBA also:
* Supports formal and informal alumni gatherings, mainly in the USA,<ref>{{cite web|title=104th Consecutive Monthly Aperitivo Italian Professionals and Entrepreneurs|url=http://www.alumnibocconi.com/article.html?aid=248|publisher=Alumni Bocconi University|accessdate=1 May 2012}}</ref> Italy<ref>{{cite web|title=MBA's Cup: 19 business schools put wind in their sails|url=http://www.press.unibocconi.eu/stampa.php?ida=1180&idr=10|publisher=Universita' Bocconi - Press Office|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Enel Green Power e American Express vincono Premio "Scacchi e Strategie Aziendali"|url=http://www.comunicati-stampa.net/com/enel-green-power-e-american-express-vincono-premio-scacchi-e-strategie-aziendali.html|publisher=Associazione Scacchi e Strategie Aziendali|accessdate=1 May 2012}}</ref>   and the UK.
* Engages in initiatives to facilitate scholarships and access to credit for MBA candidates.<ref name="bare_url">{{cite web|title=Press Release: Scholarship NOVA - The Boston Consulting Group Scholar|url=http://www4.gsb.columbia.edu/null/download?&exclusive=filemgr.download&file_id=72311|publisher=Columbia Graduate School of Business|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Yale School of Management - List of Outside Scholarships|url=http://mba.yale.edu/MBA/admissions/financial_aid/outside_schol.shtml|publisher=Yale School of Management|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Post-Graduate Associations|url=http://www.fondostudentiitaliani.it/nova.htm|publisher=The Italian Student Loan Fund|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Outside Scholarships|url=https://internationalaffairs.uchicago.edu/students/admitted/resource_links_country.shtml|publisher=University Of Chicago - Office of International Affairs|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Outside Scholarships|url=http://business.nd.edu/MBA/Admissions_and_Financial_Aid/Tuition_and_Financial_Aid/Additional_Resources/|publisher=University of Notre Dame|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Other Scholarships - "The NOVA Scholarship"|url=http://www.gsb.stanford.edu/finaid/documents/outside_scholarships0910.pdf|publisher=Stanford Graduate School of Business|accessdate=1 May 2012}}</ref><ref>{{cite web|title=Outside Funding|url=http://www.anderson.ucla.edu/x34564.xml|publisher=UCLA Anderson School of Business|accessdate=1 May 2012}}</ref>

==Corporate governance==

NOVA is a [[501(c)(3)]] not-for-profit corporation, incorporated in [[New York State]] in 2002. NOVA is further recognized as a [[public charity]] by the [[IRS]].<ref name="bare_url_a" />

The association has five members currently serving a two-year term on its board and two honorary chairmen.<ref name=OffDir>[http://www.nova-mba.org/officers.html "List of NOVA officers and directors"]. Retrieved on 28 April 2012.</ref>

NOVA officers are nominated by the board for a two-year period, following an online poll among the entire membership base. The number and roles of officers varies from time to time; there are currently 25 officers volunteering for the Association.<ref name=OffDir />

==References==
{{reflist}}

==External links==
*[http://www.nova-mba.org/ NOVA-MBA Association website]
*[http://www.controesodo.it Controesodo Website]
*[http://www.associazione360.it TrecentoSessanta Association]
*[http://www.radio24.ilsole24ore.com/blog/nava/ Radio 24: Giovani Talenti]
*[https://www.unicredit.it/it/chisiamo/per-le-persone/azioni-concrete/controesodo.html Unicredit Controesodo Website]
*[http://www.linkedin.com/company/nova-mba NOVA-MBA Official Linkedin Group (1000+ members)]
*[http://www.llm.it/ ALMA - Italian LLM Association]
*[http://www.forbes.com/lists/2011/95/best-business-schools-11_rank.html Forbes - MBA / Business Schools Rankings]
*[http://tools.businessweek.com/BSchool_Comparator/ Business Week - MBA / Business Schools Rankings]
*[http://rankings.ft.com/businessschoolrankings/global-mba-rankings-2012 Financial Times - MBA / Business Schools Rankings]
*[http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-business-schools/mba-rankings US News - MBA / Business Schools Rankings]

[[Category:Italian American]]
[[Category:Political and economic think tanks in the United States]]
[[Category:Non-profit organizations based in New York City]]