{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
'''Albert Bensimon''' (born 1948) is an [[Egyptian-Australian]] businessman from [[Adelaide]], [[South Australia]]. He is most well known as the owner of [[Shiels Jewellers]]. He frequently appeared in television advertisements and became famous for his catchphrase "No Hoo-Haa".

Bensimon was born in [[Cairo, Egypt]]. His secondary education took place at the [[Christian Brothers College, Rose Bay|Christian Brothers College]] in [[Rose Bay, New South Wales]] followed by a stint at the [[London School of Economics]]. Bensimon is of [[Jewish]] descent.

In 1994, he helped to establish the [[Helpmann Academy]], named in honour of famous South Australian Sir [[Robert Helpmann]]. The academy is designed to promote the arts in South Australia. He was the first chairman and is still a member of its board.

At the [[South Australian state election, 2002|2002 South Australian state election]], Bensimon ran as a "No Hoo-Haa Party" candidate in the [[South Australian House of Assembly|House of Assembly]] seat of [[Electoral district of Adelaide|Adelaide]], receiving a primary vote of 2.4 percent (492 votes). His "how-to-vote" ticket indicated a first preference for the [[Liberal Party of Australia]]. Bensimon is also a donor to the Liberal Party.<ref>[http://www.crikey.com.au/2002/02/03/labor-still-a-chance-to-take-the-final-state/ Labor still a chance to take the final state: Crikey 3 February 2002]</ref>

In 2006, Bensimon was rejected twice for membership of the gentlemen's club, [[The Adelaide Club]], which he claimed was due to [[anti-Semitism]]. This was denied by the Club president, who stated that it was "offensive" to suggest the club was racist and that it has "a diverse membership". In 2008, when another Egyptian-born Jewish businessman was accepted for membership, Bensimon claimed some credit for the move, saying "I broke the back of a small but influential element within the Adelaide Club."<ref>http://www.adelaidenow.com.au/news/south-australia/adelaide-club-move-ends-jewish-tension/story-e6frea83-1111116926408</ref><ref>{{cite web |url=http://www.abc.net.au/rn/talks/natint/stories/s1583416.htm  |work=National Interest |date=2006-03-05 |title=The Roundup |accessdate=2006-06-29 |publisher=[[Australian Broadcasting Corporation]]}}</ref>

==References==
<references/>

==External links==
*[http://pandora.nla.gov.au/pan/23425/20020201-0000/www.nohoohaaparty.com/index.html No Hoo-Haa Party 2002 election campaign website: Pandora Internet Archive]
*[http://shiels.com.au/ Shiels Jewellers website]
*[https://www.youtube.com/watch?v=q4xUa9QMiJQ Bill Clinton Talks About Albert and the No Hoo Haa Party]

{{DEFAULTSORT:Bensimon, Albert}}
[[Category:Living people]]
[[Category:People from Adelaide]]
[[Category:Australian Jews]]
[[Category:1948 births]]
[[Category:Australian jewellers]]
[[Category:Australian businesspeople]]
[[Category:Australian people of Egyptian-Jewish descent]]
[[Category:Egyptian emigrants to Australia]]
[[Category:Egyptian Jews]]
[[Category:People from Cairo]]
[[Category:Date of birth missing (living people)]]