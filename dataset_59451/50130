{{Infobox ethnic group
| group      = Afro-Abkhazians
|image    = [[Image:Afro-Abkhazians.jpg|300px]] 
|caption  = Photo of Afro-Abkhazian family from [[Caucasus]].
| population = 
| popplace =  formerly [[Adzyubzha]], currently [[Russia]] and other parts of [[Abkhazia]].
| languages  = [[Russian language|Russian]], [[Abkhazian language|Abkhazian]], [[Turkish language|Turkish]]
| religions  = 
| footnotes  = 
}}

'''Afro-Abkhazians''', or '''Abkhazians of African descent''', were a small group of people of [[Black people|Black]] [[Africa]]n descent in [[Abkhazia]],<ref>{{Abkhazia-note}}</ref> who used to live mainly in the settlement [[Adzyubzha]] at the mouth of the [[Kodori River]] and the surrounding villages ([[Chlou]], Pokvesh, Agdarra, and Merkulov) on the eastern coast of the [[Black Sea]] in [[Eastern Europe]].<ref>Zinaida Richter, ''Adzyubisa'' (1930)</ref><ref>[http://www.kavkaznews.net/rus/topics/biorus23.htm Абхазские негры ]{{ru icon}}  {{webarchive |url=https://web.archive.org/web/20091231030247/http://www.kavkaznews.net/rus/topics/biorus23.htm |date=December 31, 2009 }}</ref> 
[[Image:An African man in Karabakh by George Kennan.jpg|200px|thumb|right|An Afro-Abkhazian. Photo by [[George Kennan (explorer)|George Kennan]], 1870.]]

These peoples are not a small group of people who originated from the Ottoman slave trade, but to the contrary are direct ancestor to some of the first inhabitants of the U.S.S.R.. The Colchians, direct descendants of the ancient Egyptian race. Herodotus, the father of European history, "There can be no doubt the Colchians are an Egyptian race. Before I heard any mention of it from others I remarked it myself...My own conjectures were founded, first on the fact that they are black skinned and have woolly hair, which certainly amounts to but little, since other nations are so too; but further and more especially, on the circumstances that the Colchians, the Egyptians, and the Ethiopians, are the only nations who have practiced circumcision from the earliest times...I will add a further proof to the identity of the Egyptians and the Colchians. These two nations weave linen in exactly the same way, and this is a way entirely unknown to the rest of the world; they also in their whole mode of life and in their language resemble one another. {The History of Herodotus, Book II, pp. 114-15}
{Introduction to African Civilization Chapter 2, p. 92 John G. Jackson

==Origin==
===Hypotheses===
The ethnic origin of the Abkhazians of African descent—and how Africans arrived in Abkhazia—is still a matter of dispute among experts. Historians agree that the settlement of Africans in a number of villages in the village of [[Adzyubzha]] in Abkhazia (then part of the [[Ottoman Empire]]) is likely to have happened in the 17th century. According to one version, a few hundred slaves were bought and brought by [[Shervashidze]] princes (Chachba) to work on the [[citrus]] plantations.<ref name=afro>[http://www.istrodina.com/rodina_articul.php3?id=1036&n=14 Негры в Кодорском ущелье]{{ru icon}}  {{webarchive |url=https://web.archive.org/web/20090313071243/http://www.istrodina.com/rodina_articul.php3?id=1036&n=14 |date=March 13, 2009 }}</ref> This case was a unique, and apparently not entirely successful, case of mass import of Africans to the [[Black Sea]] coast.

In 1927, the [[Russians|Russian]] writer [[Maxim Gorky]], together with the Abkhaz writer [[Samson Chanba]], visited the village of Adzyubzha and met elderly Africans there. Based on his visit and a comparison of his observations with the published data, he felt that the Ethiopian version of the origin of the Abkhazians of African descent is true.<ref>[http://old.russ.ru/ist_sovr/20021015.html Загадка национальных корней Пушкина]{{ru icon}}  {{webarchive |url=https://web.archive.org/web/20110524204009/http://old.russ.ru/ist_sovr/20021015.html |date=May 24, 2011 }}</ref>

===Legends===
There are a number of folk legends that might be based partly on true events. According to one of them, which is mentioned in the memorandum of [[Ivan Isakov]] to [[Nikita Khrushchev]], an Ottoman ship wrecked near the Abkhazian coast during a storm, with slaves who were brought up for sale, and the current Abkhazians of African descent are the descendants of survivors from the ship, who founded the colony in Abkhazia.<ref name="afro"/> This legend, however, does not explain how such a ship could have entered the waters of the [[Black Sea]], which is so far from major shipping lanes of the [[slave trade]] of that time.

Another legend tells about the dealings of [[Narts]] with certain "black-faced people" from the [[Horn of Africa]]. The legendary Narts are said to have come back to the Caucasus from a long African campaign with hundreds of African escorts, who remained in Abkhazia.<ref>[http://kolhida.ru/index.php3?path=_etnography/book/nart&source=nart_blak_fase Нарты у чернолицых людей]{{ru icon}}  {{webarchive |url=https://web.archive.org/web/20090313203857/http://kolhida.ru/index.php3?path=_etnography/book/nart&source=nart_blak_fase |date=March 13, 2009 }}</ref>

In a third legend, the appearance of Afro-Abkhazians is involved with [[Peter the Great]]: he imported many black Africans to Russia, and it is said that those who were unable to acclimate to the northern capital of Russia, [[Saint Petersburg]], were then generously given to the Abkhazian princes.<ref>[http://anomalia.kulichki.ru/text9/395.htm Зана, самка снежного человека-2]{{ru icon}}  {{webarchive |url=https://web.archive.org/web/20090313000931/http://anomalia.kulichki.ru/text9/395.htm |date=March 13, 2009 }}</ref> According to the history candidate Igor Burtsev, there could have been a few dozen such "gifts of Peter" to Abkhazian princes. {{Dead link|date=May 2016}}<ref>[http://www.kp.ru/daily/23754.4/56156/ Кавказская пленница] {{ru icon}}</ref>

==History and present==
[[Herodotus]], writing about his visit to the land of the [[Colchis|Colchians]] (who lived to the south of early Abkhazians) in the [[5th century BC]], mentioned wooly-haired Africans, speculating that these black Colchians were soldiers from the army of the [[Egypt]]ian conqueror [[Sesostris]] that he left in the area on his return from what is now [[Southern Russia|South Russia]].<ref name=English>{{cite journal|last=English|first=Patrick T.|title=Cushites, Colchians, and Khazars|journal=Journal of Near Eastern Studies|volume=18|issue=1|pages=49-53 |year=1959|jstor=543940}}</ref> 2200 years later, journalist [[John Gunther]] also wrote about a small community of Africans in Abkhazia.<ref name=English/> The said memorandum of Ivan Isakov to Khrushchev on the Abkhazians of African descent says, among other things, that the governor of the Caucasus [[Illarion Ivanovich Vorontsov-Dashkov|Illarion Vorontsov-Dashkov]], imitating Peter the Great, had his personal convoy of Afro-Adzyubzhi, who accompanied him in [[Chokha]]. Prince Alexander of Oldenburg, founder of [[Gagra]], kept in his yard a few representatives from each of the peoples of the Black Sea coast of the [[Caucasus]], including the local blacks.<ref>[http://www.mk.ru/editions/daily/article/2003/05/22/135962-chitayte-v-zavtrashnem-mk.html Корреспондент “МК” нашел у абхазов негритянские корни] {{ru icon}}</ref>

It is known that by the 19th century, Afro-Abkhazians spoke only [[Abkhazian language|Abkhazian]] and identified with [[Abkhazia]]. Their total number is estimated by different observers in the range of "several families" to "several villages".<ref>[http://vlasti.net/news/45641 В Абхазии живут негры, считающие себя абхазцами] {{ru icon}}</ref> They are not religiously homogeneous, either. Apparently in Abkhazia there are or have been in the recent past black Christians, black Muslims, and black Jews.{{citation needed|date=September 2016}}

Afro-Abkhazians engaged in growing citrus, grapes, and corn, working in the coal mines of Tkvarchreli and enterprises of [[Sukhumi]], working in knitting factories, etc.{{citation needed|date=September 2016}} Like Abkhaz people, the Abkhazians of African descent today also speak in [[Russian language|Russian]]. Many left Kodor to settle in other parts of Georgia and in neighbouring Russia, as well as other nearby countries.

==In popular culture==
*Afro-Abkhazians and their relationships with indigenous Abkhaz were featured in prose by [[Fazil Iskander]].<ref>{{cite book|chapter=Sandro of Chegem|first=Donald|last=Rayfield|title=Reference Guide to Russian Literature|editor-first=Neil|editor-last=Cornwell|year=1998|location=Chicago, IL|publisher=Fitzroy Dearborn Publishers|pages=398-399|isbn=978-1884964107|chapter-url=https://books.google.co.uk/books?id=uXxEAgAAQBAJ&lpg=PA398&dq=%22sandro%20of%20chegem%22%20african&pg=PA398#v=onepage&q&f=false}}</ref>

==See also==
*[[Afro-Russians]]
*[[Black people]]
*[[African diaspora]]
*[[Abkhaz people]]
*[[African admixture in Europe]]

== References ==
{{reflist|2}}

{{African diaspora}}

[[Category:African diaspora in Europe]]
[[Category:Ethnic groups in Abkhazia]]
[[Category:History of Abkhazia]]
[[Category:Sub-ethnic groups]]