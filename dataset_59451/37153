{{Good article}}
{{Taxobox
| status = LC | status_system = IUCN3.1
| status_ref = <ref name=IUCN>{{IUCN| id = 196451| taxon = ''Toxotes jaculatrix''| assessor = Hoese, D.| assessment_year = 2012| version = 2012.1| accessdate = 25 October 2012}}</ref>
| image              = Toxotes jaculatrix.jpg
| regnum             = [[Animal]]ia
| phylum             = [[Chordate|Chordata]]
| classis            = [[Actinopterygii]]
| ordo               = [[Perciform]]es
| familia            = [[Toxotidae]]
| genus              = ''[[Archerfish|Toxotes]]''
| species            = '''''T. jaculatrix'''''
| binomial           = ''Toxotes jaculatrix''
| binomial_authority = ([[Peter Simon Pallas|Pallas]], 1767)
| synonyms =
*''Labrus jaculatrix''
*''Sciaena jaculatrix''
*''Toxotes jaculator''
| synonyms_ref = <ref name="ITIS">{{ITIS | id = 169495 | taxon = ''Toxotes jaculatrix'' | accessdate = 18 February 2010 }}</ref>
}}
The '''banded archerfish''' (''Toxotes jaculatrix'') is a [[brackish water]] [[perciform]] fish of the archerfish genus ''[[Archerfish|Toxotes]]''. It is silvery in colour and has a [[dorsal fin]] towards the posterior end. It has distinctive, semi-triangular markings along its sides.<ref name="brill"/> It is best known for its ability to spit a jet of water to "shoot down" prey.<ref name="FishBase"/><ref name="AuMus">
{{cite web
|url=http://australianmuseum.net.au/Banded-Archerfish-Toxotes-jaculatrix/
|title=Banded Archerfish, ''Toxotes jaculatrix'' (Pallas, 1767)
|last=McGrouther|first=Mark 
|date=15 May 2009
|publisher=Australian Museum
|accessdate=11 February 2011}}</ref> Larger specimens may be able to hit prey {{convert|2|to|3|m}} away.<ref name="AuMus"/> The banded archerfish may reach the displaced prey within 50 [[millisecond]]s of its hitting the water.<ref name="mwalker">{{cite book|last=Walker|first=Matt|title=Fish That Fake Orgasms: And Other Zoological Curiosities|publisher=Macmillan|date=2007|edition=illustrated|pages=105|isbn=0-312-37116-0}}</ref>

The name (binomial as well as common) refers to [[Sagittarius (astrology)|Sagittarius]] the archer, because of the unusual method banded archerfish use to capture prey. Banded archerfish are found in [[Indo-Pacific]] and [[Oceania]]n waters, generally in [[river mouth]]s and [[mangrove]] [[estuary|estuaries]]. They move between fresh, salt, and brackish water over the course of their lifetime, though not to breed. Because of their markings and silvery colour, banded archerfish are sometimes kept as [[aquarium fish]], though they are difficult to care for and not recommended for most home aquaria.<ref name="FishBase"/>

==Taxonomy and etymology==
[[File:Toxotes jaculatrix.png|thumb|right|Banded archerfish, illustrated in [[Pieter Bleeker|Bleeker]]'s 1878 ''Atlas Ichthyologique'']]
''Toxotes jaculatrix'' were originally described by [[Peter Simon Pallas]] in [[1767 in science|1767]]. Since then, several [[synonym (taxonomy)|synonym]]s (such as ''Labrus jaculatrix'' and ''Sciaena jaculatrix'') and misspellings (''Toxotes jaculator'') have come into use.<ref name="ITIS"/><ref name="FishBase"/>

''[[wikt:Toxotes|Toxotes]]'' is Greek for "bowman" or "archer", and specifically refers to [[Sagittarius (astrology)|Sagittarius]].<ref>{{cite book|last=Trzaskoma|first=Stephen|author2=R. Scott Smith |author3=Stephen Brunet |title=Anthology of classical myth: primary sources in translation|publisher= Hackett Publishing|date=2004|pages=106|isbn=978-0-87220-721-9}}</ref> The species name ''jaculatrix'' is related to the English [[:wikt:jaculate|jaculate]] and means "thrower" or "caster" (of a dart or arrow). Both the common name and binomial name refer to the banded archerfish's habit of catching prey by shooting "arrows" of water through its mouth.<ref>{{cite book|last=Jordan|first=David Starr|title=A guide to the study of fishes|publisher=H. Holt and Company|date=1905|volume=2|pages=400}}</ref>

==Description==
Banded archerfish have four dorsal spines, 11 to 13 dorsal [[soft ray]]s, three anal spines (of which the third is longest)<ref name="brill"/> and 15 to 17 anal soft rays.<ref name="FishBase">{{FishBase species| genus = Toxotes | species = jaculatrix | year = 2010 | month = January}}</ref> The first spine is always the shortest; the rays become shorter toward the posterior end.<ref name="brill"/> There are about 23 scales between the first dorsal spine and the posterior nostrils.<ref name="brill">{{cite book|last=Weber|first=Max Carl Wilhelm|author2=Lieven Ferdinand de Beaufort|title=The Fishes of the Indo-australian Archipelago VII|publisher=E. J. Brill Ltd. |location=Leiden|date=1936|series=The Fishes of the Indo-australian Archipelago |volume= Volume 7|pages=200–201}}</ref> Certain areas of the body are tinged green. The back of the fish is olive-green or brown.<ref name="brill"/> The [[dorsal fin]] is yellowish-green and located towards the posterior end, and its base is shorter than that of the anal fin.<ref name="brill"/> The [[caudal fin]] is "dirty green" and about the same height until the point of attachment, where it becomes shallower.<ref name="brill"/> The anal fin is silver.<ref name="IWE"/>

The body of the banded archerfish is oblong in shape and raised on the posterior side. The body is generally silver-white in colour, though varying colourations, such as yellow, have been observed.<ref name="brill"/> Four to six broad black bars may be present on the dorsal side.<ref name="AuMus"/><ref name="IWE"/> The first bar is found anterior to the [[operculum (fish)|operculum]], the bony plate covering the gills, and the second is found behind the operculum. The third bar is found below the origin of the dorsal fin, the fourth bar below the soft dorsal, and the fifth (if any) on the area between the anal fin and caudal fin ([[caudal peduncle]]).<ref name="brill"/> These bars become shorter as the fish ages. The [[lateral line]] curves upwards at the area between the fourth and ninth [[lateral scale]]s.<ref name="brill"/> Banded archerfish can reach a maximum length of {{convert|30|cm}}; however, average length is about {{convert|20|cm}}.<ref name="FishBase"/>

Banded archerfish have large eyes, which, unlike many other fishes, are positioned for [[binocular vision]].<ref name="IWE"/> The head is slightly shorter than the body, with a distinctively pointed snout. Juveniles may be yellow-green to brown on the dorsal side and silvery on the ventral side. The juveniles' flanks are grey-green.<ref name="IWE"/> Some banded archerfish have irregular yellow patches between their bands.<ref name="mongabay"/>

The possibility of [[sexual dimorphism]] in banded archerfish has not been investigated.<ref name="aquaatlas">{{cite book|last=Baensch|first=Hans A.|author2=Rudiger Riehl |author3=Hans A. Smith |author4=Eberhard Schulze |author5=Bob Behme |title=Aquarium Atlas|publisher=Steven Simpson Books|date=1997|edition=6th|volume=1|pages=812|isbn=1-890087-12-2}}</ref>

===Comparison to other archerfish===

The banded archerfish and its relative the largescale archerfish (''[[Toxotes chatareus]]'') are sometimes grouped and sold together under the label "archerfish".<ref name="nmonks">{{cite web|url=http://homepage.mac.com/nmonks/Projects/FAQ/3c.html|title=Archerfish, family Toxotidae|last=Monks|first=Neale|author2=Bruce Hansen|work=Brackish Water Aquarium FAQ |accessdate=1 March 2010}}</ref> However, the banded archerfish has four dorsal spines whereas largescale archerfish has five. The banded archerfish usually has four to five wedge-shaped bands, but largescale archerfish has six or seven spots and shorter bands in a regular, alternating pattern.<ref name="nmonks"/> Unlike the silvery banded archerfish, the largescale is sooty in colour. The banded archerfish may also be confused with the smallscale archerfish, ''[[Toxotes microlepis]]''. These are more difficult to distinguish, but the most striking difference is in the last two bands. While both species have four or five wedge-shaped bands, those of the banded archerfish extend to the dorsal fin, whereas those of the smallscale archerfish do not; there are two spots on the dorsal fin separate from the main bar.<ref name="nmonks"/>

==Behaviour==

===Diet and feeding===
Banded archerfish are [[omnivore|omnivorous]]. In the daytime, they come to the surface to feed on floating matter. Their diet comprises plant matter and insects, which they are able to "shoot down".<ref name="FishBase"/> Banded archerfish are also able to capture prey by jumping out of the water and seizing it from low overhanging branches. Young archerfish form small [[school (fish)|school]]s while learning aim, increasing the chance that at least one shot will hit the target. Their diet also comprises underwater prey, including [[crustacean]]s and small fishes.<ref name="mba"/>

===Shooting===
[[File:PSM V44 D612 The toxotes throwing water at insects.jpg|thumbnail|The banded archerfish shooting water at insects.]]
Banded archerfish have mouths adapted to spit jets of water over distance, usually to knock prey into the water. The banded archerfish shoots the jet of water by raising its tongue against the roof of its mouth, forming a tube. The opercula then close quickly, pressurizing water along the tube.<ref name="AuMus"/> Most archerfish are able to spit at a range of {{convert|150|cm}},<ref name="FishBase"/> though some larger specimens may be capable of ranges of up to {{convert|2|to|3|m}}.<ref name="AuMus"/> When a prey is shot down, the banded archerfish begins to move towards the place where it will land within 100 [[millisecond]]s and can reach it within 50 ms of its hitting the water.<ref name="mwalker"/>

A study found that banded archerfish could be trained to hit moving targets at an accuracy rate of greater than 50%.<ref name="curbio">{{cite journal|last=Schuster|first=Stefan|author2=Saskia Wo |author3=Markus Griebsch |author4=Ina Klostermeier |date=21 February 2006|title=Animal Cognition: How Archer Fish Learn to Down Rapidly Moving Targets|journal=Current Biology|volume=16|publisher=Elsevier Ltd.|issue=16|pages=378–383|url=http://www.nbb.cornell.edu/neurobio/department/reprints/Schuster%20et%20al.%202006.pdf|oclc=45113007|format=pdf|issn=0960-9822|doi=10.1016/j.cub.2005.12.037|accessdate=19 February 2010|pmid=16488871}}</ref> According to this study, the ability to hit moving targets is a complex learned behaviour, and can be learnt from other members of the school. The study concluded that fish could shoot more accurately after observing other members of the school shooting.<ref name="curbio"/>

The banded archerfish is able to hit targets with a high degree of accuracy, despite [[refraction]] of light at the water-air interface. It was believed that they are able to achieve this level of accuracy by positioning their bodies to shoot from directly under the intended target.<ref name="IWE">{{cite encyclopedia  | year =  1994 | title =  Archerfish | encyclopedia = International Wildlife Encyclopedia | publisher =  Marshall Cavendish | location =   | id =  |authorlink=Maurice Burton | last=Burton | first=Maurice}}</ref> However, later studies have found that banded archerfish are able to achieve great accuracy even at angles, suggesting that they are somehow able to compensate for refraction.<ref name="curbio"/><ref name="jeb">{{cite journal|last=Rossel|first=Samuel|author2=Julia Corlija|author3=Stefan Schuster|last-author-amp=yes|date=2002|title=Predicting three-dimensional target motion: how archer fish determine where to catch their dislodged prey|journal=The Journal of Experimental Biology|publisher=The Company of Biologists Limited |location=Cambridge|volume=205|issue=Pt 21|pages=3321–3326|issn=1477-9145|oclc=1754580|url=http://jeb.biologists.org/cgi/content/full/205/21/3321|accessdate=19 February 2010|pmid=12324541}}</ref> This may also suggest that banded archerfish are capable of three-dimensional tasks.<ref name="jeb"/>

===Breeding===
The breeding habits of the banded archerfish are not well known. Banded archerfish first begin to breed when they are about {{convert|10|cm}} long.<ref name="IWE"/> The banded archerfish reproduces by [[spawn (biology)|spawning]]. There are reports that banded archerfish go to saltwater reefs to spawn, but these have not been confirmed.<ref name="mae"/> Archerfish lay 20,000 to 150,000 eggs at a time.<ref name="mba">{{cite web|url=http://www.montereybayaquarium.org/animals/AnimalDetails.aspx?id=781868|title=Archerfish|publisher=Monterey Bay Aquarium |accessdate=1 March 2010}}</ref> Banded archerfish rarely breed in captivity.<ref name="mongabay"/>

==Distribution and habitat==

The banded archerfish inhabits the [[Indo-Pacific]] and waters off northern Australia, and less frequently those on the southern coast of Australia.<ref name="aquaatlas"/>  It can be found from [[India]] eastwards to the [[Philippine Islands]], southwards to Australia, as well as in waters off the [[Solomon Islands]] and the [[List of islands of Indonesia|Indonesian Archipelago]].<ref name="FishBase"/> It has been observed as far east as the New Hebrides (now known as [[Vanuatu]]).<ref>{{cite encyclopedia|last=Scott|first=Thomas|title=Concise encyclopedia biology|publisher=Walter de Gruyter|date=1996|isbn=3-11-010661-2}}</ref>
The banded archerfish occurs mainly in areas of brackish water. [[Mangrove]] [[estuary|estuaries]] are its primary habitat, though it occasionally moves upstream into freshwater rivers. It is associated with reefs and has been reported to occur near overhanging vegetation.<ref name="FishBase"/> While they may [[fish migration|move between fresh and salt water]] during their life cycle, they [[amphidromous fish|do not do so to breed]].<ref>{{cite book|last=Nagelkerken|first=Ivan|title=Ecological Connectivity Among Tropical Coastal Ecosystems|publisher=Springer Science+Business Media|date=2009|pages=333|isbn=90-481-2405-0}}</ref>

==Relationship with humans==
Banded archerfish are fairly common in the Indo-Pacific and are not currently endangered.<ref name=IUCN/> They have a minor commercial role in fisheries and may be sold fresh in markets or collected for the aquarium trade.<ref name="FishBase"/> Banded archerfish are threatened by the [[habitat destruction|destruction]] of their mangrove swamp habitat<ref name="IJNES">{{cite journal|last=Ghaffar |first=Mazlan |author2=Simon Kumar Das |author3=Zaidi Che Cob |last-author-amp=yes|date=2008|title=Comparison of Scale and Otolith Age Estimates for two Archer Fishes (''Toxotes jaculatrix'', Pallas, 1767 and ''Toxotes chatareus'', Hamilton, 1822) from Malaysian Estuaries|journal=International Journal of Natural and Engineering Sciences|issue=3|volume=2|pages=129–134|issn=1307-1149|url=http://www.nobel.gen.tr/Makaleler/IJNES-Issue%203-59-2011.pdf|format=PDF|accessdate=1 March 2010}}</ref> and by pollution.<ref name="mba"/>

===In aquaria===
Species in the genus ''[[Archerfish|Toxotes]]'', including the banded archerfish, are kept as [[aquarium fish]].<ref name="FishBase"/> In aquaria, the banded archerfish can grow up to {{convert|25|cm}} long.<ref name="mongabay">{{cite web|url=http://fish.mongabay.com/species/Toxotes_jaculatrix.html|title=Archerfish|last=Butler|first=Rhett A.|date=1995|work=Mongabay.com|accessdate=28 February 2010}}</ref> They swim at the top level of the aquarium. Banded archerfish can be kept in small groups of three to five;<ref name="mae"/> fish of the same size get along but fish that are larger may be aggressive towards those that are smaller,<ref name="aquaatlas"/> and even try to eat them.<ref name="mae"/> They may live from five to eight years in captivity, and occasionally nine or ten. Banded archerfish need warm water, usually between {{convert|25|and|30|C|F}}.<ref name="aquaatlas"/> The aquarium should be large with middling amounts of plant growth and plenty of space for swimming. It should be at least {{convert|20|to|30|cm}} deep.<ref name="aquaatlas"/>

Banded archerfish are generally not recommended for average home [[aquarium|aquaria]] despite their attractive appearance because they are difficult to care for and require special conditions.<ref name="mae"/> Banded archerfish prefer to shoot and capture live food rather than be fed [[flake food]], and are therefore difficult to feed.<ref name="aquaatlas"/> They need brackish water as well as a tall canopy. In the wild they are able to jump out of the water to capture prey on overhanging branches; a tall canopy is required to prevent their jumping out of the aquarium.<ref name="mae">{{cite web|url=http://en.microcosmaquariumexplorer.com/wiki/Banded_Archerfish|title=Banded Archerfish|work=Microcosm Aquarium Explorer|accessdate=19 February 2010}}</ref> Banded archerfish should be kept in an aquarium with a volume of at least {{convert|45|to|55|gal}}, though a greater volume is preferred.<ref name="mongabay"/> Because of such difficulties in caring for banded archerfish, the species has not yet been successful in captivity.<ref name="aquaatlas"/>

==See also==
{{Portal|Aquarium Fish}}
*[[Big scale archerfish]] (''Toxotes oligolepis''), also known as the Western archerfish
*[[List of brackish aquarium fish species]]

==References==
{{Reflist}}
{{Commons category|Toxotes jaculatrix}}

[[Category:Archerfish]]
[[Category:Animals described in 1767]]