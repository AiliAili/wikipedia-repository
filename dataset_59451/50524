{{Infobox journal
| title = Annals of Saudi Medicine
| cover =
| editor = Nasser Al-Sanea
| discipline = [[Medicine]]
| abbreviation = Ann. Saudi Med.
| formernames = King Faisal Specialist Hospital Medical Journal
| publisher = [[King Faisal Specialist Hospital]]
| country = Saudi Arabia
| frequency = Bimonthly
| history = 1981–present
| openaccess = Yes
| license =
| impact = 0.486
| impact-year = 2014
| website = http://www.annsaudimed.net
| link2 = http://www.annsaudimed.net/index.php/past
| link2-name = Online archive
| JSTOR = 
| OCLC = 12388567
| LCCN =
| CODEN = ANSMEJ
| ISSN = 0256-4947
| eISSN = 0975-4466
}}
The '''''Annals of Saudi Medicine''''' is a bimonthly [[peer-reviewed]] [[medical journal]] published by the [[King Faisal Specialist Hospital]] and Research Centre ([[Riyadh, Saudi Arabia]]). It was established in 1981 as the ''King Faisal Specialist Hospital Medical Journal'' and obtained its current name in 1985. Publication frequency increased from quarterly to bimonthly in 1988. The [[editor-in-chief]] is Nasser Al-Sanea (King Faisal Specialist Hospital and Research Centre).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-07-23}}</ref>
* [[CAB Direct (database)|CAB Abstracts]]<ref name= CABAB>{{cite web |url=http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title=Serials cited |work=[[CAB Abstracts]] |publisher=[[CABI (organisation)|CABI]] |accessdate=2015-07-23}}</ref>
* [[CASSI]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-07-23 }}</ref>
* [[CINAHL]]<ref name=CINAHL>{{cite web |url=http://www.ebscohost.com/titleLists/ccf-coverage.htm |title=CINAHL Complete Database Coverage List |publisher=[[EBSCO Information Services]] |work=[[CINAHL]] |accessdate=2015-07-23}}</ref>
* [[Current Contents]]/Clinical Medicine<ref name=ISI/>
* [[EBSCO Information Services|EBSCO databases]]
* [[Embase]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2015-07-23}}</ref>
* [[Excerpta Medica]]/[[EMBASE]]<ref name=Embase>{{cite web |url=http://www.elsevier.com/solutions/embase/coverage |title=Embase Coverage |publisher=[[Elsevier]] |work=Embase |accessdate=2015-07-23}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8507355 |title=Annals of Saudi Medicine |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-07-23}}</ref>
* [[ProQuest]]
* [[Science Citation Index Expanded]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-07-23}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2014 [[impact factor]] of 0.486.<ref name=WoS>{{cite book |year=2015 |chapter=Annals of Saudi Medicine |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |work=Web of Science}}</ref>The journal has been in the Thomson-Reuters  database since 1997.<ref>{{cite web|title=Annals of Saudi medicine (ANN SAUDI MED)|url=https://www.researchgate.net/journal/0256-4947_Annals_of_Saudi_medicine|publisher=Researchgate|accessdate=14 February 2016}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.annsaudimed.net/}}

[[Category:Open access journals]]
[[Category:General medical journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1981]]