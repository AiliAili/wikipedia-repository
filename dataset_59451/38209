{{good article}}
{{Infobox University Boat Race
| name= 35th Boat Race
| winner =Oxford
| margin = 10 lengths
| winning_time= 22 minutes 15 seconds
| date= {{Start date|1878|4|13|df=y}}
| umpire =[[Joseph William Chitty]]<br>(Oxford)
| prevseason= [[The Boat Race 1877|1877]]
| nextseason= [[The Boat Race 1879|1879]]
| overall =16&ndash;18
}}
The '''35th Boat Race''' took place on 13 April 1878.  [[The Boat Race]] is an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  In total, ten former [[Blue (university sport)|Blues]] took part in the contest.  The race was umpired by former rower [[Joseph William Chitty]] and Oxford won by a margin of 10 lengths in a time of 22 minutes 15 seconds.  The victory took the overall record to 18&ndash;16 in Oxford's favour.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 12 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 12 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities;  it is followed throughout the United Kingdom and as of 2014, broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=9 July 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 12 July 2014}}</ref>  Neither crew went into the race as reigning champions &ndash; the [[The Boat Race 1877|previous year's race]] had been declared a "dead heat".  However Oxford held the overall lead, with 17 victories to Cambridge's 16.<ref>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 21 September 2014}}</ref>

Cambridge's coach was James Brooks Close (who rowed for the Light Blues in the [[The Boat Race 1872|1872]], [[The Boat Race 1873|1873]] and [[The Boat Race 1874|1874 races]]).<ref>Burnell, p. 104</ref>  Oxford were coached by [[William Grenfell, 1st Baron Desborough|William Grenfell]] (who had rowed for the Dark Blues the previous year and was rowing at number four in 1878), A. J. Mulholland (who rowed in 1877) and Edmund Warre (who represented the Oxford in the [[The Boat Race 1857|1857]] and the [[The Boat Race 1858|1858 races]]).<ref>Burnell, pp. 101&ndash;102</ref><ref>Burnell, pp. 110&ndash;111</ref>

The race was umpired by [[Joseph William Chitty]] who had rowed for Oxford twice in 1849 (in the [[The Boat Race 1849 (March)|March]] and [[The Boat Race 1849 (December)|December races]]) and the [[The Boat Race 1852|1852 race]], while the starter was Edward Searle and the finishing judge was E. H. Fairrie.<ref>Burnell, pp. 49, 97</ref>

==Crews==
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 3.675&nbsp;[[Pound (mass)|lb]] (77.7&nbsp;kg), {{convert|6.875|lb|kg|1}} more than their opponents.<ref name=burn61/>  Cambridge saw four former [[Blue (university sport)|Blues]] return, including the [[Coxswain (rowing)|cox]] George Latham Davies who was taking part in his fourth consecutive Boat Race.  Oxford's crew included six individuals with experience of the race, with boat club president [[Tom Cottingham Edwards-Moss]] making his fourth appearance in the event.<ref name=burn61/>  Drinkwater suggested that the Oxford crew was the "best ... up to that date".<ref name=drink73>Drinkwater, p. 73</ref>
{{multiple image
| align = right
| direction = horizontal
| width = 180
| image1 = CharlesGurdon.jpg
| image2 = William Henry Grenfell 1921.jpg
| footer = [[Charles Gurdon]] ''(left)'', [[Cambridge University Boat Club]] president at the time, rowed at number six for Cambridge, while [[William Grenfell, 1st Baron Desborough|William Grenfell]] ''(right)'' occupied the four seat for Oxford.
}}
{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[William Augustine Ellison|W. A. Ellison]] || [[University College, Oxford|University]] || 10 st 13 lb || Ll. R. Jones || [[Jesus College, Cambridge|Jesus]] || 10 st 9 lb
|-
| 2 ||  D. J. Cowles || [[St John's College, Oxford|St John's]] || 11 st 5 lb || J. A. Watson-Taylor || [[Magdalene College, Cambridge|Magdalene]] || 11 st 9.75 lb
|-
| 3 || H. B. Southwell || [[Pembroke College, Oxford|Pembroke]] || 12 st 8.5 lb || T. W. Barker || [[First and Third Trinity Boat Club|1st Trinity]] || 12 st 6 lb
|-
| 4 || [[William Grenfell, 1st Baron Desborough|W. H. Grenfell]] || [[Balliol College, Oxford|Balliol]] || 12 st 10.5 lb || R. J. Spurrell || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 13.5 lb
|-
| 5 ||H. Pelham || [[Magdalen College, Oxford|Magdalen]] || 12 st 11 lb || L. G. Pike || [[Gonville and Caius College, Cambridge|Gonville & Caius]] || 12 st 8.5 lb
|-
| 6 || G. F. Burgess || [[Keble College, Oxford|Keble]] || 13 st 3.5 lb || [[Charles Gurdon|C. Gurdon]] (P) || [[Jesus College, Cambridge|Jesus]]  || 12 st 10.5 lb
|-
| 7 || [[Tom Cottingham Edwards-Moss|T. C. Edwards-Moss]] (P) || [[Brasenose College, Oxford|Brasenose]] || 12 st 3 lb || T. E. Hockin || [[Jesus College, Cambridge|Jesus]]  || 12 st 4.5 lb
|-
| [[Stroke (rowing)|Stroke]] ||   H. P. Marriott || [[Brasenose College, Oxford|Brasenose]] || 12 st 2.5 lb || E. H. Prest || [[Jesus College, Cambridge|Jesus]]  || 10 st 12.75 lb
|-
| [[Coxswain (rowing)|Cox]] || F. M. Beaumont || [[New College, Oxford|New College]] || 7 st 0.5 lb || G. L. Davis || [[Clare College, Cambridge|Clare]]  || 7 st 5 lb
|-
!colspan="7"|Source:<ref name=burn61>Burnell, p. 61</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]], along which the race is conducted]]
Oxford won the [[coin flipping|toss]] and elected to start from the Surrey station, handing the Middlesex side of the river to Cambridge.  The race commenced at 10.15am. [[Stroke rate|Out-rating]] Oxford by four strokes per minute, Cambridge took an early lead and were half a length ahead after the first minute.  They held the lead round the bend which favoured them but as the river curved to favour Oxford, the Dark Blues quickly overtook, even with the slower stroke rate.  They were at least half-a-length clear at the Crab Tree pub which they had extended to four lengths by [[Hammersmith Bridge]].<ref name=drink73/>  Oxford won by 40 seconds (approximately 10 lengths) in a time of 22 minutes and 15 seconds, their second victory in nine years, which took the overall record to 18&ndash;16 in their favour.<ref name=results>{{Cite web | url = http://theboatrace.org/men/results| publisher = The Boat Race Company Limited| title = Boat Race – Results| accessdate = 21 September 2014}}</ref><ref name=burn61>Burnell, p. 61</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1878}}

[[Category:The Boat Race]]
[[Category:1878 in English sport]]
[[Category:April 1878 sports events]]