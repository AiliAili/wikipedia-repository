{{EngvarB|date=May 2013}}
{{Use dmy dates|date=May 2013}}
{{Infobox military person
|name= Josiah Thomas Mberikwazvo Tungamirai
|birth_date=8 October 1948
|death_date= 25 August 2005
|birth_place= [[Gutu]], [[Southern Rhodesia]]
|death_place= South Africa
|image= Josiah Tungamirai.jpg
|caption=
|nickname=Muzamani
|allegiance={{flagicon|Zimbabwe}} [[Zimbabwe]]
|serviceyears=
|rank= [[Air Chief Marshal]]
|branch= [[Air Force of Zimbabwe]]
|commands= [[Air Force of Zimbabwe]]
|unit=
|battles=
|awards=
|laterwork= Zimbabwean Government Minister
}}
[[Air Chief Marshal]] '''Josiah Tungamirai''' (8 October 1948<ref name="Whitehouse">[http://georgewbush-whitehouse.archives.gov/news/releases/2003/03/text/20030307-11.html Executive Order: Blocking Property Of Persons Undermining Democratic Processes Or Institutions In Zimbabwe], ''The [[White House]]''. Retrieved on 1 April 2007.</ref> – 25 August 2005), born '''Thomas Mberikwazvo''',<ref name="gozimbabwe">[http://www.gozimbabwe.com/mdc_tungamirai.html MDC MPs break boycott for Tungamirai burial], ''GoZimbabwe.com''. Retrieved on 1 April 2007.</ref> was a [[Zimbabwe]]an military officer and politician. He was commander of the Air Force and later served as Minister of State for Indigenization and Empowerment in President [[Robert Mugabe]]'s government before his death in 2005.

==Early years and education==
Tungamirai was born into a peasant family as Thomas Mberikwazvo on 8 October 1948 in [[Gutu]], [[Masvingo Province]] in what was then [[Southern Rhodesia]]. He received his primary education at the Mutero Mission in Gutu from 1957 to 1964. His secondary education was completed at the Chikwingwizha Seminary. Tungamirai went up to [[Harare Polytechnic|Salisbury Polytechnic]] and he completed his studies in Physics and Mathematics in 1970.<ref name="Mathaba">[http://www.mathaba.net/news/?x=334284 Zim robbed of great strategist], ''Mathaba News Network''. Retrieved on 1 April 2007.</ref>

==Military career==
Tungamirai served in [[Robert Mugabe]]'s [[Zimbabwe African National Liberation Army]] (ZANLA) during the [[Rhodesian Bush War]]. He remained loyal to Mugabe during the Nhari and Vashandi inter-factional strife within ZANLA, and was among the ZANLA officials who escaped Zambia, following the Zambian government's arrest of senior [[ZANU]] officials after the assassination of [[Herbert Chitepo]]. Towards the end of the war, he served as ZANLA Political Commissar.<ref>Martin, D and Johnson, P. 1981. [http://www.amazon.com/Struggle-Zimbabwe-David-Martin/dp/0571110665   The Struggle for Zimbabwe]. Faber & Faber.</ref>

In 1979 Tungamirai was part of the [[ZANU–PF|Patriotic Front]] delegation which was a party to the [[Lancaster House Agreement]].  When Zimbabwe became independent in 1980, Tungamirai became a [[major general]] in the newly formed [[Zimbabwe National Army]] and was appointed to the Zimbabwe Joint High Command. He was involved in the work to integrate the formerly belligerent [[Zimbabwe African National Liberation Army]] and [[Zimbabwe People's Revolutionary Army]] as well as the [[Rhodesian Army]].

In 1982 Tungamirai was transferred to the [[Air Force of Zimbabwe]] to fill the post of Chief of Staff. At that time he was redesignated an [[air vice-marshal]]. Tungamirai qualified as a pilot in October 1984.<ref name="Mathaba"/>

Tungamirai went on to reach the rank of [[air chief marshal]] and serve as the commander of the [[Air Force of Zimbabwe]].<ref name="AFZ">[http://www.mod.gov.zw/airforce/airforce.htm Air Force of Zimbabwe], ''Zimbabwe Ministry of Defence''. Retrieved on 1 April 2007.</ref>

In 1992 Tungamirai was replaced by [[Perence Shiri]] as Air Force commander.<ref name="AFZ"/>

==Political career==
In early 2004, Tungamirai was elected to parliament from [[Gutu North]] in a by-election. He was soon afterwards appointed to the Cabinet as Minister of State for Indigenisation and Empowerment on 9 February 2004.<ref>[http://www.zimbabwesituation.com/feb10a_2004.html#link6 "Mugabe rewards loyalists in new Cabinet"], ''New Zimbabwe'' (zimbabwesituation.com), 9 February 2004.</ref>

==Death==
Following what anonymous members of Tungamirai's family said were problems with rejection of a kidney transplant carried out several years previously,<ref name="newzim1">[http://www.newzimbabwe.com/pages/zvobgo15.13055.html Tungamirai dies in South Africa], ''New Zimbabwe''. Retrieved on 1 April 2007.</ref> Tungamirai was flown to a South African hospital for emergency treatment. He died there on 25 August 2005.<ref name="newzim2">[http://www.newzimbabwe.com/pages/zvobgo20.14616.html Tungamirai was poisoned, wife claims], ''New Zimbabwe''. Retrieved on 1 April 2007.</ref> After Josiah Tungamirai's death, his widow Pamela Tungamirai claimed that he had been poisoned.<ref name="newzim2"/>

==References==
{{Reflist}}

{{s-start}}
{{s-mil}}
|-
{{s-bef|before=[[Azim Daudpota|A Daudpota]]}}
{{s-ttl|title=Commander of the [[Air Force of Zimbabwe]]|years=1986–1992}}
{{s-aft|after=[[Perence Shiri|P Shiri]]}}
|-
{{s-off}}
{{s-bef|before=New ministry}}
{{s-ttl|title=Minister of State for Indigenisation and Empowerment|years=2004–2005}}
{{s-aft|after=[[Samuel Mumbengegwi|S C Mumbengegwi]]}}
{{s-end}}

{{Zimbabwe government ministers}}

{{DEFAULTSORT:Tungamirai, Josiah}}
[[Category:1948 births]]
[[Category:2005 deaths]]
[[Category:People from Masvingo Province]]
[[Category:Air Force of Zimbabwe air marshals]]
[[Category:Zimbabwe African National Liberation Army personnel]]
[[Category:Zimbabwean politicians]]
[[Category:Government ministers of Zimbabwe]]