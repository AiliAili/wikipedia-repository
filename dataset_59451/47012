{{Infobox hospital
| Name        = Children's Health
| Org/Group   = <!-- optional -->
| Image       = [[File:Children's Health logo.jpg|200px|center]]

| Caption     = 
| map_type    = 
| latitude    = <!-- used only for adding a map, with map_type -->
| longitude   = <!-- used only for adding a map, with map_type -->
| Logo        = <!-- optional -->
| Logo Size   = <!-- optional -->
| Location    = 
| Region      = 
| State       = Texas
| Country     = US
| Coordinates = 
| HealthCare  = <!-- UK:NHS. AU/CA: Medicare. ELSE freetext, eg Private -->
| Funding     = Private, Non-profit
| Type        = 
| Speciality  = <!-- if devoted to a speciality (ie not a broad spectrum of specialities) and Type=Specialist/Teaching -->
| Standards   = <!-- optional if no national standards -->
| Emergency   = <!-- Pediatric Level I -->
| Affiliation = [[University of Texas Southwestern Medical Center]]
| Patron      = <!-- 'None' or the individual who acts as the hospital patron -->
| Network     = <!-- optional -->
| Beds        = 562
| Founded     =     
| Website     = http://www.childrens.com
| Wiki-Links  = <!-- optional -->
}}
'''Children's Health''' is a pediatric health care system in North Texas anchored by two hospitals, [[Children's Medical Center Dallas]] and [[Children's Medical Center Plano]], as well as seven specialty centers and 19 pediatric clinics located throughout the region.<ref>{{cite web|url=https://finance.yahoo.com/news/children-health-petsmart-partner-fund-160000863.html|title=Children’s Health and PetSmart Partner to Fund Canine Therapy Program|date=21 January 2015|work=Yahoo Finance}}</ref> A private, not-for-profit organization, Children’s Health provides pediatric health, wellness and acute care services for children from birth to age 18, including specialty care, primary care, home health, a pediatric research institute, and community outreach services.<ref>{{cite web|url=http://www.bizjournals.com/dallas/blog/2014/09/children-s-health-system-rebranding-more-than.html|title=Children’s Health System rebranding more than cosmetic, CEO says|date=22 September 2014|work=Dallas Business Journal}}</ref>

According to Beckers Hospital Review, Children's Health is the fifth largest pediatric healthcare provider in the nation.<ref>{{cite web|url=http://www.beckershospitalreview.com/150-great-places-to-work-in-healthcare-2015/childrens-health-system-texas-15.html|title=Children's Health System of Texas - 150 great places to work in healthcare 2015|author=Staff|work=beckershospitalreview.com}}</ref> Children's Health is also a pediatric kidney, liver, heart, bowel, and bone marrow transplant center, and includes a designated Level 1 trauma center.<ref>{{cite web|url=http://www.bmtinfonet.org/node/6618|title=Children's Medical Center Dallas|date=22 January 2015|work=Blood and Marrow Transplant Information Network}}</ref> <ref>{{cite web|url=http://www.dshs.state.tx.us/emstraumasystems/etrahosp.shtm|title=Texas Trauma Facilities|date=23 July 2015|work=Texas Department of State Health Facilities}}</ref>

Until 2014, Children's Health was known by the name of its main hospital, Children's Medical Center Dallas. In September 2014, it rebranded as Children's Health, legally known as Children's Health System of Texas.<ref>{{cite web|url=http://www.dallasnews.com/business/headlines/20140921-childrens-medical-center-rebrands-itself-childrens-health-system-of-texas.ece|title=Children’s Medical Center rebrands itself Children’s Health System of Texas|work=dallasnews.com}}</ref>

==History==
Children's Health traces its origins to the summer of 1913, when a group of nurses organized an open-air clinic, called the Dallas Baby Camp, on the lawn of the old Parkland Hospital in Dallas. In 1930, the Dallas Baby Camp grew into the Bradford Hospital for Babies, which merged with Children's Hospital of Texas and Richmond Freeman Memorial Clinic in 1948 to form what is now known as Children's Medical Center Dallas. Children's Medical Center affiliated with [[University of Texas Southwestern Medical Center]] in 1964. In 2014, Children's Medical Center expanded into Children's Health.<ref>{{cite web|url=http://www.dallasnews.com/news/metro/20130406-childrens-medical-center-dallas-born-100-years-ago-as-camp-for-sick-babies.ece|title=Children's Medical Center Dallas born 100 years ago as camp for sick babies|work=Dallas Morning News}}</ref>

==Facilities==
===Children's Medical Center Dallas===
[[Children's Medical Center Dallas]], the original hospital of the Children's Health system, is an academic medical center campus anchored by a 490-bed hospital<ref>{{cite web|url=http://www.beckershospitalreview.com/hospital-profile/10-things-to-know-about-childrens-medical-center-of-dallas.html|title=10 things to know about Children's Medical Center of Dallas|work=Becker's Hospital Review}}</ref> It features 16 operating rooms and provides care in more than 60 specialties.<ref>{{cite web|url=http://doctor.webmd.com/hospital/childrens-health-dallas-tx-bb5ca6ae-5e80-48e2-b379-caa71b5d9eb9-overview|title=Childrens Health|work=webmd.com}}</ref>

===Children's Medical Center Plano===
Built in 2008, [[Children's Medical Center Plano]] is the second hospital included in the Children's Health system.<ref>{{cite web|url=http://www.modernhealthcare.com/article/20090907/NEWS/309079985|title=Honorable Mention/Built: Children's Medical Center at Legacy, Plano, Texas|work=Modern Healthcare}}</ref> It is an academic medical center campus located adjacent to the Children's Health Specialty Center Plano. The hospital currently has 72 beds and four operating rooms. It offers 24-7 emergency services, and laboratory, pharmacy, and imaging services. <ref>{{cite web|url=http://www.utswmedicine.org/hospitals-clinics/childrens/childrens-legacy/|title=Children's Health Children's Medical Center Plano|work=UT Southwestern Medical Center}}</ref>

===Children's Health Pediatric Group===
Children's Health includes 19 pediatric primary care offices located throughout North Texas, specializing in health care for newborns, infants, and children through age 18.<ref>{{cite web|url=http://healthcare.dmagazine.com/2015/05/13/what-five-of-dfws-most-recognizable-health-systems-hope-to-accomplish-in-joint-aco/|title=What Five Of DFW's Most Recognizable Health Systems Hope To Accomplish In Joint ACO|work=D Healthcare Daily}}</ref> These locations include Bachman Lake, Medical District, Mill City, Carrollton, Celina, East Plano, Garland, Lake Highlands, McKinney, West Plano, Cedar Hill, Cockrell Hill, Grapevine, Irving, Lancaster Kiest, Oak Cliff, Pleasant Grove, St. Philip's, and Prosper.<ref>{{cite web|url=https://www.childrens.com/location-landing/locations-and-directions/pediatric-group|title=Children's Health℠ Pediatric Group|work=childrens.com}}</ref>

===Children's Health Specialty Centers===
Seven Children's Health specialty centers located throughout North Texas offer subspecialty care, outpatient surgery, imaging, physical medicine, and rehabilitation.<ref>{{cite web|url=http://www.news-medical.net/news/20110616/Childrens-Medical-Center-to-open-new-2421M-Specialty-Care-Center-in-Southlake.aspx|title=Children's Medical Center to open new $21M Specialty Care Center in Southlake|date=16 June 2011|work=News-Medical.net}}</ref> These centers are located in Southlake, at Texas Health Presbyterian Hospital in Dallas, in Plano, at the Bass Building, in Dallas across the street from Children's Medical Center Dallas, in Mesquite, and in Rockwall.<ref>{{cite web|url=https://www.childrens.com/location-landing/locations-and-directions/specialty-centers|title=Children’s Health℠ Specialty Centers|work=childrens.com}}</ref>

===Children's Medical Center Research Institute at UT Southwestern===
Children's Health is affiliated with [[University of Texas Southwestern Medical Center]], one of the country's leading medical education and biomedical research institutions.<ref>{{cite web|url=http://www.utsouthwestern.edu/about-us/facts.html|title=About Us: Facts - UT Southwestern Medical Center, Dallas, TX|work=UT Southwestern Medical Center}}</ref> Many UT Southwestern professors practice medicine at the nearby [[Children's Medical Center Dallas]]. The Children's Medical Center Research Institute, located in Dallas on the UT Southwestern campus, was established in 2011 with the purpose of performing biomedical research to better understand the biological basis of disease. Teams of Children's Health physicians and UT Southwestern scientists pursue advances in regenerative medicine, cancer biology, and metabolism.<ref>{{cite web|url=http://www.utswmedicine.org/hospitals-clinics/childrens/|title=Children’s Health℠ Children’s Medical Center|work=utswmedicine.org}}</ref>

===Children's Health Andrews Institute for Orthopaedics & Sports Medicine===
In February 2015, Children's Health announced its partnership with orthopedic surgeon [[James Andrews (physician)|James Andrews]]. The new facility, to be located in Plano, will focus on orthopedic and sports medicine care, pediatric injury prevention, rehabilitation and therapy, and will also house an athletic performance center.<ref>{{cite web|url=http://starlocalmedia.com/planocourier/news/children-s-medical-center-plano-to-open-orthopedic-sports-medicine/article_8ba3924e-d71e-11e4-8f16-136da8730dad.html|title=Children's Medical Center Plano to open orthopedic sports medicine facility|work=Plano Star Courier}}</ref> The new facility is expected to be complete in late 2016.<ref>{{cite web|url=http://www.dallasnews.com/news/community-news/plano/headlines/20150319-childrens-health-partners-with-andrews-institute-for-new-youth-orthopedic-sports-medicine-clinic-in-plano.ece|title=Children’s Health partners with Andrews Institute for new youth orthopedic, sports medicine clinic in Plano|work=dallasnews.com}}</ref><ref>{{cite web|url=http://www.andrewssportsmedicine.com/|title=Andrews Sports Medicine And Orthopaedic Center (Birmingham, Alabama - Jefferson County)|work=andrewssportsmedicine.com}}</ref>

==Community involvement==
===Health and Wellness Alliance for Children===
The Health and Wellness Alliance for Children, created by Children's Health, is a coalition of more than 60 hospitals, social service organizations, faith-based organizations, school districts, government entities, and families focused on improving the health and well-being of children in the Dallas and Collin counties. The alliance's first areas of focus are asthma, the top reason for emergency department admissions at Children's Health, and weight management.<ref>{{cite web|url=http://www.healthandwellnessalliance.com/who-we-are/|title=Who We Are|work=healthandwellnessalliance.com}}</ref>

===Local school programs===
Through the TeleHealth program, Children's Health partners with local school nurses to provide access to physicians without forcing parents to leave work to take their child to the doctor.<ref>{{cite web|url=http://www.kxii.com/home/headlines/Denison-ISD-implements-new-TeleHealth-System-281879941.html|title=Denison ISD implements new TeleHealth System|author=Ashley Park|work=kxii.com}}</ref> Children's Health also delivers routine check-ups and vaccinations to children at events throughout the region.

===Rees-Jones Center for Foster Care Excellence===
Children's Medical Center Dallas houses a foster care program, called the Rees-Jones Center for Foster Care Excellence, that provides support for children living with relatives or foster parents, or in a group home.<ref>{{cite web|url=http://dfw.cbslocal.com/2013/12/02/childrens-medical-center-to-open-healthcare-program-for-foster-children/|title=Children's Medical Center to open healthcare program for foster children|work=CBS}}</ref> The clinic focuses on support for foster parents and children, care coordination, and child welfare and health to encourage the children's recovery from neglect and abuse. <ref>{{cite web|url=https://www.aap.org/en-us/advocacy-and-policy/aap-health-initiatives/healthy-foster-care-america/Pages/Models/ChildrensMedicalCenterFosterCareClinicsDallas.aspx|title=Children's Medical Center Foster Care Clinics Dallas|work=American Academy of Pediatrics}}</ref> <ref>{{cite web|url=http://keranews.org/post/remaking-foster-care-after-number-crises-redesign-takes-root-fort-worth|title=Remaking Foster Care: After A Number Of Crises, Redesign Takes Root In Fort Worth|author=Doualy Xaykaothao|work=keranews.org}}</ref>

===Dallas Children's Health Holiday Parade===
The Dallas Children's Health Holiday Parade began in 1987 with a partnership between [[Adolphus Hotel]] and Children's Medical Center, who were both celebrating 75th year anniversaries. The parade kicks off the holiday season for the city with marching bands, famous characters, holiday floats, and balloons.<ref>{{cite web|url=http://events.kvil.cbslocal.com/dallas_tx/events/dallas-childrens-health-holiday-parade-/E0-001-077386786-9|title=Dallas Children's Health holiday parade|work=CBS}}</ref> The parade draws a crowd of more than 400,000.<ref name="dallaschildrensparade.com">{{cite web|url=http://dallaschildrensparade.com/about-us/history/|title=History|work=Dallas Children's Health℠ Holiday Parade presented by Pizza Hut}}</ref>

==GENECIS program==
In May 2015, Children's Health announced the opening of the GENECIS clinic. GENECIS (GENder Education and Care, Interdisciplinary Support) provides counseling and support for children and teens with gender dysphoria through a combination of therapy, evaluations, and puberty-blocking medications. The clinic was started by Ximena Lopez, a pediatric endocrinologist at [[Children's Medical Center Dallas]] who has been treating patients with gender dysphoria for several years. It is the only pediatric transgender clinic in the Southwest.<ref>{{cite web|url=http://keranews.org/post/inside-dallas-clinic-transgender-kids|title=Inside A Dallas Clinic For Transgender Kids|author=Stephen Becker|work=keranews.org}}</ref>

== References ==

{{Reflist}}

== External links ==
*[http://www.childrens.com Children's Health website]

[[Category:Children's hospitals in the United States]]