<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Wibault 3
 |image=Wib 3 0.png
 |caption=
}}{{Infobox Aircraft Type
 |type=Single seat [[fighter aircraft]]
 |national origin=[[France]]
 |manufacturer=Pierre Levasseur<ref name=Flight/>
 |designer=Michel Wibaut
 |first flight=Q1, 1923<ref name=G&S/>
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users= <!--Limited to three in total; separate using <br /> -->
 |produced= <!--years in production-->
 |number built=
 |program cost= <!--Total program cost-->
 |unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 |developed from= 
 |variants with their own articles=
}}
|}

The '''Wibault 3''' or '''Wibault Wib 3 C.1''' was a French [[parasol wing]] prototype [[fighter aircraft]] from the 1920s, designed for high altitude operations. Its development was abandoned after repeated materials failure in its [[supercharger]].

==Design and development==
The Wib 3, or Wib 3 C.1 (the C for ''Chasseur'' or fighter, 1 indicating single seat) was Wibault's response to a call from the ''Service Technique de l'Aéronautique'' (S.T.Aé, Technical Department of Aeronautics) for a high altitude fighter. This was required to have a top speed of {{convert|240|km/h|mph|abbr=on|0}} at {{convert|7000|m|ft|-2}} and a [[service ceiling]] of {{convert|8500|m|ft|-2}}; to achieve this performance at altitude, the specification called for a [[Turbocharger|turbocharged]] engine.<ref name=G&S/>

It was an all-metal aircraft in the contemporary sense, with a structure of [[duralumin]] but largely [[aircraft fabric covering|fabric covering]]. A parasol wing, with a cut-out in the [[trailing edge]] over the open [[cockpit]], ensured the pilot a good all round view. The wing was straight edged with constant [[chord (aircraft)|chord]] and was fitted with long span [[ailerons]]. It was braced to the lower fuselage on each side with a pair of parallel, [[aircraft fairing|faired]] [[strut]]s to about half span.<ref name=Flight/> The wing section to half span was moderately thick but thinned outboard, giving an overall maximum [[lift to drag ratio]] of almost 20.<ref name=Flight/><ref name=G&S/>

The Wib 3 was powered by a {{convert|300|hp|kW|lk=on|0|abbr=on|disp=flip}} [[Hispano-Suiza 8F]]b upright water-cooled [[V8 engine|V-8]] engine<ref name=G&S/> with a [[Lamblin]] cylindrical [[radiator (engine cooling)|radiator]] on each side of the fully enclosed [[aircraft fairing#Types|cowling]]. A {{ill|Auguste Rateau|fr|lt=Rateau}} supercharger maintained power up to {{convert|15000|ft|m|-2|disp=flip}}.<ref name=Flight/> The [[fuselage]] was aluminium skinned from its nose to the cockpit; aft, it was fabric covered.<ref name=Flight/> Its wire braced, almost triangular [[tailplane]] carried split [[elevator (aircraft)|elevator]]s, the inner ends cropped to allow movement of the broad [[rudder]]. The Wib 3 had a fixed [[conventional undercarriage]], with mainwheels on a rigid axle supported by a pair of V-[[strut]]s mounted at the roots of the interplane struts. The axle was enclosed within an aerofoil shaped fairing which added {{convert|1.50|m2|sqft|abbr=on|2}} to the wing area.<ref name=G&S/>

On its first flight early in 1923, the Rateau turbo-supercharger was not fitted, its development having been interrupted by repeated failures caused by the difficulty of producing suitable high temperature resistant alloys. As a result, it was cancelled by the (S.T.Aé). The Wib 3 continued its flight testing until the autumn of 1923, after which the high altitude specification was withdrawn.<ref name=G&S/>

[[File:Wib 3 1.png|thumb|Wibault 3 C.1]]

==Specifications==
[[File:Wibault 3 C.1 fighter 3v.png|thumb|Wibault 3 C.1 three view]]
{{Aircraft specs
|ref=Green & Swanborough pp.595-6<ref name=G&S/>
|prime units?=met
<!--        General characteristics -->
|genhide=
|crew=One
|length m=8.20
|length note=
|span m=11.72
|span note=
|height m=3.04
|height note=
|wing area sqm=25.00
|wing area note=
|airfoil=
|empty weight kg=994
|empty weight note=
|gross weight kg=1420
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|500|lb|kg|abbr=on|0|disp=flip}}<ref name=Flight/>
|more general=
<!--        Powerplant -->
|eng1 number=1
|eng1 name=[[Hispano-Suiza 8F]]b
|eng1 type=water-cooled [[V8 engine|V-8]], [[supercharged]]
|eng1 hp=300
|eng1 note=
|power original=
|more power=
|prop blade number=2
|prop name=
|prop dia m=
|prop dia ft=
|prop dia in=
|prop dia note=
<!--        Performance -->
|perfhide=
|max speed kmh=241
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=530
|range nmi=
|range note=at {{convert|16550|ft|m|abbr=on|0|disp=flip}}<ref name=Flight/>
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=3.0 hr
|ceiling m=7000
|ceiling note=service
|g limits=
|roll rate=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading lb/sqft=10.8
|wing loading note=<ref name=Flight/>
|power/mass= 160 W/kg (0.097 hp/lb) from Flight,<ref name=Flight/> who give a power loading of 10.3 lb/hp
|more performance=
<!--        Armament -->
|guns= 2×{{convert|7.7|mm|in|abbr=on|3}} [[synchronization gear|synchronised]] [[Vickers machine gun]]s, firing through propeller arc
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{commons category|Wibault aircraft}}
{{reflist|refs=

<ref name=G&S>{{cite book |last=Green |first=William |first2= Gordon|last2= Swanborough |title=The Complete Book of Fighters |year=1994|publisher=Salamander Books|location=Godalming, UK|isbn=1-85833-777-1|page=175 (D.8), 595–6}}</ref>
<ref name=Flight>{{cite magazine|date=31 May 1923 |title=A New French All-metal Aeroplane|magazine=[[Flight International|Flight]]|volume=XV|issue=22|pages=289–90|url=http://www.flightglobal.com/pdfarchive/view/1923/1923%20-%200289.html}}</ref>
}}

==Further reading==
* {{cite journal|title=L'avion de Chasse Monoplan Wibault 3C1|journal=L'Aérophile|date=15 May 1923|pages=138–141|url=http://gallica.bnf.fr/ark:/12148/bpt6k6555017m/f144|accessdate=30 August 2014|language=French}}
<!-- ==External links== -->

{{Wibault aircraft}}

[[Category:Single-engine aircraft]]
[[Category:Parasol-wing aircraft]]
[[Category:French fighter aircraft 1920–1929|Wibault 03]]
[[Category:Wibault aircraft|Wib 3]]