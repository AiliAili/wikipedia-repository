{{stack begin}}
{{Taxobox
| image = Amanita ravenelii 57104.jpg
| image_width = 234px
| image_caption =
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Amanitaceae]]
| genus = ''[[Amanita]]''
| species = '''''A. ravenelii'''''
| binomial = ''Amanita ravenelii''
| binomial_authority = ([[Miles Joseph Berkeley|Berk.]] & [[Moses Ashley Curtis|M.A. Curtis]]) [[Pier Andrea Saccardo|Sacc.]]
| synonyms_ref = <ref name="urlMycoBank: Amanita ravenelii"/>
| synonyms = *''Agaricus ravenelii'' <small>Berk. & M.A. Curtis</small>
}}
{{mycomorphbox
| name = ''Amanita ravenelii''
| whichGills = free
| capShape = umbonate
| hymeniumType = gills
| stipeCharacter = ring and volva
| ecologicalType = mycorrhizal
| sporePrintColor = white
| howEdible2= inedible
}}
{{stack end}}

'''''Amanita ravenelii''''', commonly known as the '''pinecone Lepidella''', is a species of [[fungus]] in the family [[Amanitaceae]]. The [[Basidiocarps|fruit bodies]] are medium to large, with caps up to {{convert|17|cm|in|1|abbr=on}} wide, and a stem up to {{convert|25|cm|in|1|abbr=on}} long and {{convert|3|cm|in|1|abbr=on}} thick. The warts on the whitish cap surface are large—up to {{convert|6|mm|in|abbr=on}} wide and {{convert|4|mm|abbr=on}} high. The stem has a large bulb at its base, covered with whitish to brownish scales, that may root several centimeters into the soil. The [[annulus (mycology)|ring]] on the stem is thick and cotton- or felt-like. It is widely distributed in [[mixed forest|mixed]] and [[deciduous forest]]s of the southeastern United States, where it grows solitarily or in groups on the ground in late summer and autumn. The mushrooms, which have an odor resembling [[bleaching powder]], are not recommended for consumption.

==Taxonomy==
The species was first described scientifically by [[Miles Joseph Berkeley]] and [[Moses Ashley Curtis]] in 1859 as ''Agaricus ravenelii''.<ref name=Berkeley1859/> [[Pier Andrea Saccardo]] transferred it to the genus ''[[Amanita]]'' in 1887.<ref name=Saccardo1887/> It is in the subsection ''Solitariae'', [[section (biology)|section]] ''Lepidella'' in the genus ''[[Amanita]]''.<ref name=Singer1986/> Other North American species in the section ''Lepidella'' include ''[[Amanita abrupta|A.&nbsp;abrupta]]'', ''[[Amanita atkinsoniana|A.&nbsp;atkinsoniana]]'', ''[[Amanita chlorinosma|A.&nbsp;chlorinosma]]'', ''[[Amanita cokeri|A.&nbsp;cokeri]]'', ''[[Amanita daucipes|A.&nbsp;daucipes]]'', ''[[Amanita mutabilis|A.&nbsp;mutabilis]]'', ''[[Amanita onusta|A.&nbsp;onusta]]'', ''[[Amanita pelioma|A.&nbsp;pelioma]]'', ''[[Amanita polypyramis|A.&nbsp;polypyramis]]'', and ''[[Amanita rhopalopus|A.&nbsp;rhopalopus]]''.<ref name=Bhatt2004/>

The [[specific name (botany)|specific epithet]] ''ravenelli'' honors the American mycologist [[Henry William Ravenel]].<ref name=Bessette2007/> The fungus is [[common name|commonly]] known as the "pinecone Lepidella".<ref name=McKnight1987/>

==Description==
{{multiple image
| align = left
| direction = vertical
| width = 180
| image1 = Amanita ravenelii 57103.jpg
| caption1 = The cap surface is covered with conical to truncate-conical warts.
| image2 = Amanita ravenelii 92433.jpg
| caption2 = The gills here are covered with remnants of the partial veil.
}}
The [[pileus (mycology)|cap]] is {{convert|8|–|17|cm|in|1|abbr=on}} wide, initially hemispherical to almost round, later becoming convex to flattened. It is fleshy, white to yellowish-white, usually dry, but occasionally slightly sticky with age. The [[universal veil]] remains as a pale yellow to brownish-orange layer that breaks up into crowded, rather coarse, conical to truncate-conical warts. The warts are up to {{convert|6|mm|in|abbr=on}} wide and {{convert|4|mm|abbr=on}} high, becoming more scale-like towards the cap margin with age. The margin is non-striate (without any grooves), and appendiculate (with [[partial veil]] remnants hanging along the cap margin). The [[lamella (mycology)|gills]] are free from attachment to the stem, crowded together, moderately broad, and yellowish-white to pale yellow. Interspersed among the gills are short gills (lamellulae) that do not extend completely to the stem; they are somewhat truncated (abruptly terminated) to attenuated (tapering gradually). The [[stipe (mycology)|stem]] is {{convert|10|–|25|cm|in|1|abbr=on}} long and {{convert|1|–|3|cm|in|1|abbr=on}} wide, and decreases slightly in thickness near the apex. It is solid (i.e., not hollow), white to pale yellow, and covered with tufts of soft woolly hairs or fibrils. It has a large basal bulb, swollen in the middle, which roots in the ground up to {{convert|5.5|cm|in|abbr=on}}. The partial veil is yellowish-white to pale yellow, forming an [[annulus (mycology)|ring]] which is thick, woolly, delicate, and soon falls away. The universal veil remains at the stem base as thick scales, curved downward, often forming irregular rings. The [[trama (mycology)|flesh]] is firm, and white to pale yellow. The mushroom tissue has an odor of chlorinated lime ([[bleaching powder]]),<ref name=Bhatt2004/> or "old tennis shoes".<ref name=Miller2006/> The [[edible mushroom|edibility]] of ''Amanita ravenelii'' mushrooms have been described variously as unknown,<ref name=Bessette2007/> "not recommended",<ref name=McKnight1987/> or poisonous.<ref name=Miller2006/>

===Microscopic characteristics===
The [[spore]]s are [[wikt:ellipsoid|ellipsoid]], occasionally ovoid or [[wikt:obovoid|obovoid]], thin-walled, [[hyaline]], [[amyloid (mycology)|amyloid]], and measure 8–11 by 5.5–7.5&nbsp;µm. The [[spore print|spore deposit]] is white. The [[basidia]] (spore-bearing cells) are 40–65 by 7–11.5&nbsp;µm, four-spored, with clamps at their bases. [[wikt:cheilocystidium|Cheilocystidia]] ([[cystidia]] on the gill edge) are occasionally seen as small, club-shaped cells measuring 15–35 by 10–15&nbsp;µm, on thin-walled hyphae that are 3–7&nbsp;µm in diameter. The [[pileipellis|cap cuticle]], which is not clearly differentiated from the cap tissue, consists of thin-walled, interwoven [[hypha]]e 2.5–9&nbsp;µm in diameter. The tissue of the universal veil on the cap consists of more or less parallel and erect rows of roughly spherical, and ellipsoid to broadly ellipsoid cells, up to 78 by 65&nbsp;µm and spindle- to club-shaped cells up to 125 by 30&nbsp;µm. These latter cells are terminal or in short, terminal chains, and are borne on moderately abundant, thin-walled, branched, interwoven, sometimes nearly [[wikt:coralloid|coralloid]] hyphae, 3–9.5&nbsp;µm diameter with a few scattered oleiferous (oil-containing) hyphae, 5–12.5&nbsp;µm diameter. The distribution of hyphae at the stem base is similar to that on the cap, but with more filamentous hyphae. [[Clamp connection]]s are present.<ref name=Bhatt2004/>

===Similar species===
[[image:Amanita ravenelii 92431.jpg|thumb|160px|right|The basal bulb is characteristic of ''A.&nbsp;ravenelii''.]]
The fruit bodies of ''A.&nbsp;ravenelii'' are distinguished from ''[[Amanita chlorinosma|A.&nbsp;chlorinosma]]'' by the presence of pale yellow to brownish orange, large, conical to truncate-conical warts on the cap surface and a large basal bulb. The mushroom ''[[Amanita polypyramis|A.&nbsp;polypyramis]]'' is pure white, and lacks the pale yellow to brownish-orange large conical warts typical of ''A.&nbsp;ravenelii''.<ref name=Bhatt2004/> The North American species ''[[Amanita armillariiformis|A.&nbsp;armillariiformis]]'' has a similar [[wikt:areolate|areolate]] cap surface, but unlike ''A.&nbsp;ravenelii'', does not have a distinct basal bulb, and it is found in semi-arid areas associated with [[aspen]] and old growth [[Douglas fir]].<ref name=Miller1990/> Also from North America, ''[[Amanita mutabilis|A.&nbsp;mutabilis]]'' has pink tones on the cap and stem, and will turn pink when its flesh is cut; it smells of [[anise]].<ref name=Miller2006/>

==Habitat and distribution==
''Amanita ravenelii'' is a [[mycorrhiza]]l fungus, meaning it forms [[Mutualism (biology)|mutualistic]] associations with shrubs and trees.<ref>Jenkins, 1986, pp. 5–6.</ref> Mushrooms grow on the ground solitarily, scattered, or in groups in [[mixed forest|mixed]] coniferous and deciduous forests.<ref name=Bessette2007/> Although the specific tree associations preferred by ''A.&nbsp;ravenelii'' are unknown, in general, ''Amanita'' from section ''Lepidella'' tend to associate with diploxylon pine (that is, pines in [[Pinus classification|subgenus ''Pinus'']]), [[oak]], and [[hickory]].<ref name=Bhatt2004/>

''Amanita ravenelii'' is widely distributed in the southeastern United States, where their occurrence is "occasional to frequent" in the late summer and autumn months of August to November;<ref name=Bessette2007/> mushrooms been collected from the US states of [[Maryland]], [[North Carolina]], [[South Carolina]], [[Indiana]], [[Tennessee]], and [[Virginia]].<ref>Jenkins, 1986, p. 100.</ref> It has also been reported growing in northern [[Baja California]], Mexico.<ref name=Ayala1988/>

==See also==
{{portal|Fungi}}
* [[List of Amanita species|List of ''Amanita'' species]]

==References==
{{Reflist|colwidth=30em|refs=

<ref name=Ayala1988>{{cite journal |vauthors=Ayala N, Manjarrez I, Guzman G, Thiers HS |year=1988 |title=Fungi from the Baja California Peninsula Mexico III. The known species of the genus ''Amanita'' |journal=Revista Mexicana de Micologia |volume=4 |pages=69–74 |language=Spanish}}</ref>

<ref name=Berkeley1859>{{cite journal |vauthors=Berkeley MJ, Curtis MA |year=1859 |title=Centuries of North American fungi |journal=Annals and Magazine of Natural History |volume=4 |series=III |pages=284–296}}</ref>

<ref name=Bessette2007>{{cite book |title=Mushrooms of the Southeastern United States |vauthors=Bessette AE, Roody WC, Bessette AR |year=2007 |publisher=Syracuse University Press |location=Syracuse, New York |isbn=978-0-8156-3112-5 |page=112 |url=https://books.google.com/books?id=IB1Gv3jZMmAC&lpg=PA112&dq=Amanita%20ravenelii&pg=PA112#v=onepage&q=Amanita%20ravenelii&f=false}}</ref>

<ref name=Bhatt2004>{{cite book |vauthors=Bhatt RP, Miller OK Jr | editor-last=Cripps CL |title=Fungi in Forest Ecosystems: Systematics, Diversity, and Ecology |publisher=New York Botanical Garden Press |year=2004 |pages=33–59 |chapter=''Amanita'' subgenus ''Lepidella'' and related taxa in the southeastern United States |isbn=978-0-89327-459-7}}</ref>

<ref name=McKnight1987>{{cite book |vauthors=McKnight VB, McKnight KH |title=A Field Guide to Mushrooms: North America |publisher=Houghton Mifflin |location=Boston |year=1987 |page=208 |isbn=0-395-91090-0|url=https://books.google.com/books?id=kSdA3V7Z9WcC&lpg=PA208-IA56&dq=Amanita%20ravenelii&pg=PA208-IA56#v=onepage&q=Amanita%20ravenelii&f=false}}</ref>

<ref name=Miller1990>{{cite journal |vauthors=Miller Jr OK, Trueblood E, Jenkins DT |year=1990 |title=Three new species of ''Amanita'' from southwestern Idaho and southeastern Oregon |journal=Mycologia |volume=82 |issue=1 |pages=120–128 |jstor=3759971 |doi=10.2307/3759971}}</ref>

<ref name=Miller2006>{{cite book |vauthors=Miller HR, Miller OK |title=North American Mushrooms: A Field Guide to Edible and Inedible Fungi |publisher=Falcon Guide |location=Guilford, CN |year=2006 |page=45 |isbn=0-7627-3109-5 |url=https://books.google.com/books?id=zjvXkLpqsEgC&lpg=PA45&dq=Amanita%20ravenelii&pg=PA45#v=onepage&q=Amanita%20ravenelii&f=false}}</ref>

<ref name=Saccardo1887>{{cite journal |author=Saccardo PA |year=1887 |title=Sylloge Hymenomycetum, Vol. I. Agaricineae |journal=Sylloge Fungorum |volume=5 |page=15 |language=Latin}}</ref>

<ref name=Singer1986>{{cite book |author=Singer R. |title=The Agaricales in Modern Taxonomy |edition=4th |publisher=Koeltz Scientific Books |location=Koenigstein |year=1986 |page=452 |isbn=3-87429-254-1}}</ref>

<ref name="urlMycoBank: Amanita ravenelii">{{cite web |url=http://www.mycobank.org/MycoTaxo.aspx?Link=T&Rec=28771 |title=''Amanita ravenelii'' (Berk. & Broome) Sacc. 1887 |publisher=International Mycological Association |work=MycoBank |accessdate=2010-09-13}}</ref>

}}

===Cited books===
*{{cite book |author=Jenkins DB |title=''Amanita'' of North America |publisher=Mad River Press |location=Eureka, California |year=1986 |isbn=0-916422-55-0}}
{{good article}}

[[Category:Amanita|ravenelii]]
[[Category:Fungi described in 1859]]
[[Category:Fungi of North America]]
[[Category:Inedible fungi]]
[[Category:Taxa named by Miles Joseph Berkeley]]