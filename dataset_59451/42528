<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=C.800
 | image=Caudron_C.800_(MAE).JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=Two-seat basic [[Training aircraft|training]] [[glider (sailplane)|glider]]
 | national origin=[[France]] 
 | manufacturer=[[SNCAN]]
 | designer=Raymond Jarlaud
 | first flight=April 1942
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= 
 | number built=315
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Caudron C.800''', at first also known as the '''Epervier''' ({{lang-en|Sparrowhawk}}) is a [[France|French]] two seat [[Training aircraft|training]] [[glider (sailplane)|glider]], designed and first flown during [[World War II]] and put into large scale post-war production.  It was the dominant basic training glider with French clubs until the 1960s and several still fly.

==Design and development==

Design of the Caudron C.800 began soon after the Franco-German [[Second Armistice at Compiègne|Armistice]] of June 1940, proceeding in parallel with that of the [[Castel C.25S]].  Both aircraft were intended to increase the number of machines available for recreational gliding in the southern, unoccupied region of France. Its wood framed, [[Aircraft fabric covering|fabric]] covered [[monoplane#Types|high wings]] are braced from below with short and quite broad [[chord (aircraft)|chord]] [[aircraft fairing|faired]] [[strut]]s, one on each side, from the lower [[fuselage]] to the constant chord wing centre section.  Outboard the wing panels taper roughly [[elliptical]]ly, with obliquely hinged [[ailerons]] filling their whole [[trailing edge]]s.<ref name=Hardy>{{cite book |title=Gliders & Sailplanes of the World|last= Hardy |first= Michael |edition= |year=1982|publisher=Ian Allen Ltd|location= London|isbn=0 7110 1152 4|page=25}}</ref><ref name="Shenstone">{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>

The fuselage is a [[plywood]] covered wooden [[monocoque]]<ref name="Shenstone" /> with an oval cross section; the is wing mounted at the highest point immediately behind the [[cockpit]], which places instructor and pupil in [[tandem#Side-by-side|side-by-side]] seats ahead of the [[leading edge]], equipped with dual control and covered by a short, upward opening, rear hinged, multi-piece [[canopy (aircraft)|canopy]].<ref name=Hardy/> There is another pair of opening fuselage transparencies immediately below the canopy.  Behind the wing the fuselage tapers, initially quickly, to the tail where the narrow chord, round tipped [[tailplane]] is mounted, with some [[dihedral (aircraft)|dihedral]], on top of it.  The broader, split [[elevator (aircraft)|elevators]] are ahead of a straight edged, blunt tipped narrow [[fin]] and wide [[rudder]]. Like the wings, the [[empennage]] is wood framed and fabric covered.  The [[Landing gear#Gliders|monowheel undercarriage]] is assisted by a sprung, wooden skid reaching forwards from the wheel to the nose, and by a tail skid.<ref name=Hardy/>

Two prototype C.800s were flown during [[World War II]], the first in April 1942.<ref name=VGC>{{cite journal |last= |first= |authorlink= |date=Summer 1982 |title= French glider production|journal=Vintage Glider Club News|volume=44 |issue= |pages=11–22 |id= |url=http://www.lakesgc.co.uk/mainwebpages/VGC%20News%201973-2003/No_44_Summer_1982.pdf|quote=production figures as stated in a special edition of AVIASPORT for French glider types, published by Pierre Bonneau in 1960|accessdate= }}</ref><ref name=j2mcl>{{cite web|url=http://www.j2mcl-planeurs.net/dbj2mcl/planeurs-machines/planeur-fiche_0int.php/?code=370|title=Caudron C.800 - j2mcl Planeurs|accessdate=2012-11-10}}</ref>  A single-seat version, the '''C.810''' was also flown in 1942 but these two prototypes were destroyed by bombing.  An improved single-seat variant, the '''C.811''' was flown after 1945 but not developed; it was seen by the French Air Ministry as too similar to existing types such as the [[Schneider Grunau Baby|Grunau]].<ref name=VGC/>

In 1951 an improved version of the C.800 named '''C.801''' was designed by Raymond Jarlaud.  This had a reinforced structure, an enlarged rudder and balanced ailerons.  Cockpit visibility was improved by simplifying the frames with more curved glazing and ground handling was made easier by moving the monowheel forward.<ref name=VGC/><ref name=j2mcl2>{{cite web|url=http://www.j2mcl-planeurs.net/dbj2mcl/planeurs-machines/planeur-fiche_0int.php?code=371|title=Caudron C.801 - j2mcl Planeurs|accessdate=2012-11-10}}</ref>

[[File:Interior view air museum Angers-Marcé-4.jpg|thumb|right]]

==Operational history==

After the liberation of France in 1944, the French government ordered 450 as part of an effort to revive French aviation, though this was later reduced to 248.<ref name=j2mcl/> Production of 300 began in 1945 at the [[Aire-sur-Adour]] factory of the Fouga company, by then part of [[SCAN]]. Most went to civil gliding clubs becoming, along with the [[Castel C.25S]], the national standard two-seat trainer type until their replacement by the [[Wassmer WA 30 Bijave]] in the early 1960s. It remained an important club stalwart for twenty years after its introduction.<ref name=VGC/> Some were operated by the [[French Air Force]] and [[Aéronavale]].<ref name=j2mcl/>

Ten C.801s were built at Aire-sur-Adour<ref name=VGC/> but were withdrawn from use in 1957 on safety grounds.<ref name=j2mcl2/>

In 2010 six C.800s remained on the [[France|French]] civil aircraft register and one on the [[Netherlands|Dutch]].<ref name=EuReg>{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}</ref>

==Variants==
;C.800: Original 1940s production; 302 built.
;C.800 Motorized version: Little is known of this one-off modification.
;C.801: Improved 1950s version; 10 built.
;C.810: Single-seat variant, flown 1942.  The two prototypes were destroyed by bombing.
;C.811: Improved C.310 flown post-war but not developed.
<!-- ==Units using this aircraft/Operators (choose)== -->

==Aircraft on display==
''Data from'' Aviation Museums and Collections of Mainland Europe.<ref name=Ogden>{{cite book |title=Aviation Museums and Collections of Mainland Europe |last=Ogden |first=Bob |edition= |year=2009|publisher= Air Britain (Historians) Ltd|location= |isbn=978 0 85130 418 2}}</ref>
C.800s are on public display at
* The Army Museum, [[Brussel]]
;* Musée de l'Agriculture et de la Locomotion, [[Uzès]]
;* Musée Maurice Dufresnes, [[Azay-le-Rideau]]
;* Musée Régional de l'Air, [[Angers]]
;* Musée de l'Aviation de Mas Palegry, [[Perpignan]]
;* Musée de l'Air et de l'Espace, [[le Bourget]]
;* Ailes Anciennes Toulouse, [[Blagnac]]

== Specifications (C.800) ==
{{Aircraft specs
|ref=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name="Shenstone" />
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Two
|length m=8.35
|length note=
|span m=16.0
|span note=
|width note=
|height m=2.36
|height note=
|wing area sqm=22.0
|wing area note=
|aspect ratio=11.6
|airfoil= root Göttingen 654, tip Göttingen 676
|empty weight kg=240
|empty weight note=
|gross weight kg=420
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|never exceed speed kmh=170
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|85|km/h|mph kn|abbr=on|1}}
*'''Aerotow speed:''' {{convert|90|km/h|mph kn|abbr=on|1}}
|g limits=
|sink rate ms=0.93
|sink rate note=at {{convert|68|km/h|mph kn|abbr=on|1}}
|lift to drag=~21 at {{convert|78|km/h|mph kn|abbr=on|1}}
|wing loading kg/m2=19.1
|wing loading note=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
;Notes
{{reflist}}
;Bibliography
*{{cite book |title=Gliders & Sailplanes of the World|last= Hardy |first= Michael |edition= |year=1982|publisher=Ian Allen Ltd|location= London|isbn=0 7110 1152 4|page=25}}
*{{cite book |title=Aviation Museums and Collections of Mainland Europe |last=Ogden |first=Bob |edition= |year=2009|publisher= Air Britain (Historians) Ltd|location= |isbn=978 0 85130 418 2}}
*{{cite book |title= The World's Sailplanes|last= OSTIV|volume=1|edition=|year=1958|publisher=OSTIV & Schweizer Aero-Revue|location=|isbn=|pages=31, 34}}
*{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0}}
*{{cite journal |last= |first= |authorlink= |date=Summer 1982 |title= French glider production|journal=Vintage Glider Club News|volume=44 |issue= |pages=11–22 |id= |url=http://www.lakesgc.co.uk/mainwebpages/VGC%20News%201973-2003/No_44_Summer_1982.pdf|quote=production figures as stated in a special edition of AVIASPORT for French glider types, published by Pierre Bonneau in 1960|accessdate= }}
*{{cite web|url=http://www.j2mcl-planeurs.net/dbj2mcl/planeurs-machines/planeur-fiche_0int.php/?code=370|title=Caudron C.800 - j2mcl Planeurs|accessdate=2012-11-10}}
*{{cite web|url=http://www.j2mcl-planeurs.net/dbj2mcl/planeurs-machines/planeur-fiche_0int.php?code=371|title=Caudron C.801 - j2mcl Planeurs|accessdate=2012-11-10}}

<!-- ==Further reading== -->

==External links==

{{commons category|Caudron C.800}}
*[http://www.ae.illinois.edu/m-selig/ads/afplots/goe654.gif Göttingen 654 airfoil]
*[http://www.ae.illinois.edu/m-selig/ads/afplots/goe676.gif Göttingen 676 airfoil]
*[http://www.lakesgc.co.uk/mainwebpages/VGC%20News%201973-2003/No_44_Summer_1982.pdf]
<!-- Navboxes go here -->

{{Caudron aircraft}}

[[Category:French sailplanes 1940–1949]]
[[Category:Caudron aircraft]]