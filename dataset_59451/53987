{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
[[File:Adelaide Christmas Pageant 2004 Snow White Float.jpg|right|250px|thumb|Snow White float in the 2004 Pageant.]]

The '''Adelaide Christmas Pageant''' is a [[Santa Claus parade|parade]] held annually in the [[South Australia]]n capital of [[Adelaide]]. Recognised as a [[National Trust of Australia|heritage icon]],<ref>{{cite web|title=BankSA Heritage Items List |publisher=National Trust of South Australia |date=2006-10-20 |url=http://www.nationaltrustsa.org.au/heritage_icons_2006.htm |accessdate=2006-10-20 |archiveurl=https://web.archive.org/web/20060825040435/http://www.nationaltrustsa.org.au/heritage_icons_2006.htm |archivedate=25 August 2006 |deadurl=no |df=dmy }}</ref> the pageant is a [[Government of South Australia|state institution]] and is sponsored by four local [[credit unions]]. Since 1996 it has been known as the Credit Union Christmas Pageant.

Established in 1933, the event is staged on the second Saturday of November every year, usually from 9.30am. It comprises a [[procession]] of 85 sets and 1,700 volunteers, including some 63 floats, 15 bands, 164 clowns, dancing groups, and walking performers, all culminating in the arrival of [[Father Christmas]].<ref>http://events.weekendnotes.com/the-credit-union-christmas-pageant/</ref>

The pageant takes place in the [[Adelaide city centre]], along a 3.35 kilometre route which commences on [[King William Street, Adelaide|King William Street]] at [[South Terrace, Adelaide|South Terrace]], and concludes on [[North Terrace, Adelaide|North Terrace]], wherefrom Father Christmas proceeds to the [[Santa's Grotto|Magic Cave]], originally in the [[John Martin & Co.|John Martin's]] building, and now in the rebuilt [[David Jones Limited|David Jones]] building on the same site. (The route sometimes changes slightly due to building or road works.<ref>[http://www.cupageant.com.au/194.aspx Route Map] {{webarchive |url=https://web.archive.org/web/20071101080059/http://www.cupageant.com.au/194.aspx |date=1 November 2007 }}, Official Site (cupageant.com.au)</ref>)

==History==
Adelaide's Christmas Pageant was founded by [[Edward Hayward|Sir Edward Hayward]], owner of the Adelaide department store [[John Martin & Co.|John Martin's]], who was inspired by the [[Toronto Santa Claus Parade]] and [[Macy's Thanksgiving Day Parade]].<ref>{{cite news|first=David |last=Sly |title=Spirit of Johnnies lives on |url=http://www.adelaidereview.com.au/archives/2004_10_29/history_story1.shtml |publisher=[[The Adelaide Review]] |date=2004-10-29 |accessdate=2006-10-16 |deadurl=yes |archiveurl=https://web.archive.org/web/20081101224420/http://www.adelaidereview.com.au:80/archives/2004_10_29/history_story1.shtml |archivedate=1 November 2008 |df=dmy }}</ref> He opened the first 'Children's Christmas Parade' on 18 November 1933 at the height of the [[Great Depression]]. It was a success; running for around 40 minutes with just 8 floats and 3 bands, it attracted 200,000 spectators and from then the tradition of the John Martin's Christmas Pageant or as it was affectionately known the  'Johnnie's Christmas Pageant' was born.<ref name="History">{{cite web|title=Adelaide's First Pageant |publisher=Credit Union Christmas Pageant |date=2006-10-20 |url=http://www.cupageant.com.au/history_first_pageant.php |accessdate=2006-10-20 |deadurl=yes |archiveurl=https://web.archive.org/web/20070929081418/http://www.cupageant.com.au/history_first_pageant.php |archivedate=29 September 2007 }}</ref> Father Christmas was introduced in 1934 and tradition of the pageant finishing at the Magic Cave (itself created in 1905) was established.<ref name="Tradition">{{cite web|title=Credit Union Pageant Tradition |publisher=Credit Union Christmas Pageant |date=2006-10-20 |url=http://www.cupageant.com.au/history_cu_pageant_tradition.php |accessdate=2006-10-20 |archiveurl=https://web.archive.org/web/20070306222018/http://www.cupageant.com.au/history_cu_pageant_tradition.php |archivedate=6 March 2007 |deadurl=no |df=dmy }}</ref>

During the [[World War II|war years]] of 1941-1944, the pageant was in abeyance. It was restored in 1945. By 1969, the event had grown significantly, with attendances reaching 500,000 and television broadcasting commencing.<ref name="History"/> In 1979, the largest induction of new floats took place, with 16 joining the pageant.<ref name="Tradition"/>

In 1985, John Martin's was acquired by [[David Jones Limited]], who continued the pageant under the John Martin's name. However, with the collapse of the [[Adelaide Steamship Company|Adelaide Steamship Group]] (of which David Jones was a significant member), and the public float of the David Jones retailing arm, in the mid-1990s the [[Government of South Australia|South Australian Government]] acquired the event. It sought sponsorship from the South Australian business community, and in 1996 sold the naming rights to the six South Australian Credit Unions: [[Australian Central Credit Union|Australian Central]], Savings & Loans,<ref>[http://www.savingsloans.com.au/Search/Search.aspx?q=pageant&p=40&x=46&y=6 Christmas Pageant]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, Savings & Loans Credit Union</ref> Community CPS,<ref>[http://www.communitycps.com.au/aspx/community_marketing.aspx#pageant Credit Union Christmas Pageant], Community CPS Credit Union</ref> PowerState,<ref>[http://www.powerstate.com.au/DesktopDefault.aspx?tabid=189 Christmas Pageant competition]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, PowerState Credit Union</ref> Satisfac<ref>[http://www.satisfac.com.au/About-Christmas%20Pageant.asp About the Christmas Pageant], Satisfac Credit Union</ref> and the Police Credit Union.<ref name="History"/> Today, the pageant is managed by Events South Australia,<ref>[http://www.cupageant.com.au/157.aspx The people behind the Credit Union Christmas Pageant] {{webarchive |url=https://web.archive.org/web/20090911235836/http://www.cupageant.com.au/157.aspx |date=11 September 2009 }}, cupageant.com.au</ref> a division of the [[South Australian Tourism Commission]]. As a result of mergers, the current naming right sponsors are [[People's Choice Credit Union]], [[Beyond Bank Australia]], [[Credit Union SA]] and Police Credit Union.<ref>{{cite web|title=Pageant Partners |url=http://www.cupageant.com.au/pageant-partners.htm |publisher=Credit Union Christmas Pageant |deadurl=yes |archiveurl=https://web.archive.org/web/20130811183527/http://cupageant.com.au/pageant-partners.htm |archivedate=11 August 2013 |df=dmy }}</ref> One Johnnie's tradition that the credit unions have been delighted to continue is that of the Pageant Queen. In 2009 a Pageant King and Princes were introduced to the Pageant and with the Pageant Queen and Princesses make up the Pageant Royal Family. The Royal Family tour the state visiting schools, libraries and children's groups as well as the Women's and Children's Hospital on Pageant Day to share the Pageant magic.

In 2008 there was a [[Guinness World Records|Guinness world record]] attempt for the longest and largest [[Mexican wave]], but it failed.

In 2010 the spectators broke the record for the largest group of carol singers singing [http://www.christmasloanss.co.uk/ Christmas] carols at the same time. They set a record of over 9,100 carol singers, breaking the previous record of 7,541 set in the USA.<ref>[http://www.adelaidenow.com.au/credit-union-christmas-pageant-breaks-into-world-record-books/story-e6frea6u-1225953036735 Credit Union Christmas Pageant breaks into world-record books], www.adelaidenow.com.au</ref>

==Broadcast==
The Pageant is broadcast officially by [[NWS-9]], the local affiliate of the [[Nine Network]]. For many years the broadcast was carried separately by both [[SAS-7]] and [[ABS-2]].

Commentators and Presenters in recent years have included:

*2005: [[Rob Kelvin]] and [[Georgina McGuinness]], with [[Mark Bickley]] and Lisa McAskill.
*2006: [[Rob Kelvin]] and [[Georgina McGuinness]], with [[Mark Bickley]] and Lisa McAskill.
*2007: [[Georgina McGuinness]] and [[Kym Dillon]], with [[Mark Bickley]] and Lisa McAskill.
*2008: [[Kelly Nestor (journalist)|Kelly Nestor]] and [[Brenton Ragless]], with [[Georgina McGuinness]], [[Mark Bickley]] and [[Kate Collins (journalist)|Kate Collins]].
*2009: [[Brenton Ragless]] and [[Kate Collins (journalist)|Kate Collins]], with [[Kelly Nestor (journalist)|Kelly Nestor]] and Jason 'Snowy' Carter.
*2015:   Brenton Ragless and Kate Collins with Virginia Langeberg (Virginia's last time). This was the first time the pageant was broadcast interstate to Perth, Melbourne and Sydney on GEM{{cn|date=November 2015}}

==References==
<references />

==External links==
{{commonscat|Adelaide Christmas Pageant}}
* {{Official website|http://www.cupageant.com.au/}}

{{Adelaide Festivals}}

[[Category:Recurring events established in 1933]]
[[Category:Festivals in Adelaide|Christmas Pageant]]
[[Category:Christmas and holiday season parades]]
[[Category:Parades in Australia]]
[[Category:Festivals established in 1933]]