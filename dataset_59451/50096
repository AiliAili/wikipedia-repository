{{Infobox character
| name        = Varys
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Conleth Hill]]<br>(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Varys-Conleth Hill.jpg
| caption     = [[Conleth Hill]] as Varys
| first       = '''Novel''': <br>''[[A Game of Thrones]]'' (1996) <br>'''Television''': <br>"[[Lord Snow]]" (2011)
| last        =
| occupation  =
| title       = Master of Whisperers
| alias       = The Spider<br>The Eunuch<br>Lord Varys<br>Rugen<br>Varys of Lys
| gender      = Male
| family      = 
| spouse      = 
| children    = 
| relatives   = 
| nationality = Lysene
}}

'''Varys''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 1996's ''[[A Game of Thrones]]'', Varys is from the fictional city of Lys. He subsequently appeared in Martin's ''[[A Clash of Kings]]'' (1998), ''[[A Storm of Swords]]'' (2000) and ''[[A Dance with Dragons]]'' (2011).

Varys is portrayed by [[Conleth Hill]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/varys/bio/varys.html |title=''Game of Thrones'' Cast and Crew: Varys played by Conleth Hill |publisher=[[HBO]] | accessdate=December 29, 2015}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html |title=From HBO |publisher= |deadurl=yes |archiveurl=https://web.archive.org/web/20160307150640/http://grrm.livejournal.com/164794.html |archivedate=2016-03-07 |df= }}</ref>

==Character description and background==
Varys, also called the Spider, is a [[eunuch]]  and [[courtier]] who serves as Master of Whisperers, the spymaster for the king of the Seven Kingdoms at the royal court in King's Landing.{{sfn|''A Game of Thrones''|loc=Appendix: House Baratheon, pp.676–677}} As Master of Whisperers he is on the [[privy council]]. He is feared by nobles and common people alike. He knows all of the secret passages in the royal castle and his spies are found everywhere.<ref>{{cite web|url=http://www.tor.com/2012/01/13/a-read-of-ice-and-fire-a-clash-of-kings-part-2/|title=A Read of Ice and Fire: A Clash of Kings, Part 2|work=Tor.com}}</ref> He is a skilled manipulator and commands a network of informants across two continents. He often puts on the public persona of being nothing more than a bald, pudgy man well suited to the pleasantries of court life; humble, obsequious, fawning, and a little effeminate. This is simply a facade that Varys has developed, which often leads those who do not know him well to underestimate him as a cheerful and vapid flatterer. In reality he is a cunning and ruthless manipulator of court politics, on par with Master of Coin [[Petyr Baelish|Petyr "Littlefinger" Baelish]], with whom he frequently spars. Unlike Baelish, Varys insists that his goals are to achieve what he honestly feels is best for the realm.

Varys was born as a [[slave]] in the Free City of Lys, and joined a travelling acting troupe. While the troupe was performing in Myr, a sorcerer bought a young Varys from the troupe's leader. The sorcerer drugged Varys before removing his genitals and burning them in a brazier in a blood magic ritual, afterwards casting him into the streets. Varys turned to begging, [[prostitution]], and ultimately [[theft]] to survive, but soon became known in Myr and fled to Pentos. There he befriended a poor [[mercenary|sellsword]], [[Illyrio Mopatis]], teaming up to steal valuables from other thieves and returning them to their owners for a fee. Varys eventually realised that there was more to gain from stealing secrets instead of valuables, and trained his spies to copy information from the wealthy and powerful. Varys and Illyrio became rich, and Varys' reputation reached the ears of the King of Westeros, [[Aerys II Targaryen]], who appointed Varys as his Master of Whisperers. Jaime claims that Aerys saw traitors everywhere, and Varys was quick to point out any he missed. Apparently when [[Rhaegar Targaryen|Rhaegar]] intended to use a Tourney at Harrenhal to call a Great Council to deal with his father's instability, possibly dethroning him, Varys warned Aerys, meaning they attended the tourney. Sometime prior to the Sack of King's Landing, Varys had [[Rhaegar Targaryen]]'s infant son Aegon swapped with a lowborn baby and smuggled to Essos to be raised in hiding by Rhaegar's friend Jon Connington, who was exiled by the Mad King for failing to defeat Robert. Varys remains Master of Whisperers after [[Robert Baratheon]] seizes the Iron Throne, but secretly remains loyal to House Targaryen.

==Overview==
Varys is not a [[Narration#Third-person|point of view]] character in the novels, so his actions are witnessed and interpreted through the eyes of other people, such as [[Ned Stark]] and [[Tyrion Lannister]]. Varys is mostly a background character in the novels.<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=HBO}}</ref>

==Storylines==

===In the books===
====''A Game of Thrones''====
Soon after [[Eddard Stark]]'s arrival in King's Landing, Varys warns him that Robert's wife [[Cersei Lannister]] intends to kill Robert and offers his help to Eddard in investigating the Lannisters. He later meets in secret with Illyrio to discuss stalling a war between Houses Stark and Lannister until the right moment; this is overheard by [[Arya Stark]], but she is unable to identify the men. When Robert is ultimately killed, Varys remains spymaster for his heir [[Joffrey Baratheon|Joffrey]], and suggests that Ser [[Barristan Selmy]] be blamed for his death. Selmy is removed from the Kingsguard and ultimately defects to [[Daenerys Targaryen]], which may have been Varys' plan all along. Varys visits Eddard in captivity and convinces him to plead guilty to treason and join the Night's Watch in order to save the life of his daughter [[Sansa Stark|Sansa]], also averting war between the Starks and Lannisters. Eddard go along with the plan, but Joffrey has him executed regardless. Varys also arranges for Robert's bastard [[Gendry]] to join the Night's Watch, to avoid being killed by Cersei.

====''A Clash of Kings''====
Varys is the first to learn that [[Tyrion Lannister]] has brought his mistress Shae with him to King's Landing, and tells Tyrion of a route that he can use to visit her. He forms an uneasy alliance with Tyrion to share the information gathered by his spy network.

====''A Storm of Swords''====
Varys continues to facilitate Tyrion and Shae's trysts, but testifies against Tyrion when he is tried for Joffrey's murder. After Tyrion is sentenced to death, [[Jaime Lannister]] forces him to help Tyrion escape. As he and Tyrion escape through the secret passages of the Red Keep, Tyrion decides to seek out his father [[Tywin Lannister|Tywin]]'s chambers. Varys protests, but gives Tyrion the exact directions to his room. It is likely Varys took advantage of the situation to have Tywin killed, weakening a chief rival of House Targaryen.

====''A Feast for Crows''====
After helping Tyrion escape, Varys disappears. A Red Keep turnkey, Rugen, disappears at the same time, and a Tyrell coin is found in his chambers. This cements Cersei's distrust of House Tyrell, though she is unaware Rugen is merely a disguise of Varys.

====''A Dance with Dragons''====
Varys sneaks into the Red Keep to kill Grand Maester Pycelle. He also lures Lord Regent Kevan Lannister into Pycelle's chambers, and mortally wounds him. As Kevan dies Varys explains that his death was necessary to destabilise the Seven Kingdoms in preparation for Aegon Targaryen's invasion, before ordering his little birds to finish Kevan off.

===In the show===
[[File:Conleth Hill by Gage Skidmore.jpg|thumb|right|upright|[[Conleth Hill]] plays the role of Varys in the [[Game of Thrones|television series]].]]
[[File:Three dragons.png|thumb|150px|alt=A coat of arms showing a red three-headed dragon on a black field over a scroll reading "Fire and Blood."|Coat of arms of House Targaryen]]

Varys' storyline remains, for the most part, identical between the first two seasons of the show and the books.
====Season 3====
After Littlefinger's confidante Ros is severely beaten by Joffrey's guards and he fails to intervene, Varys takes her into his service as a spy. Littlefinger finds out and has her killed, taunting Varys that he could not protect her. Varys affirms his course as a means to prevent chaos befalling the realm, claiming that Littlefinger "would see the Seven Kingdoms burn, if he could be king over the ashes."
====Season 4====
Varys informs Tyrion that Cersei has discovered his relationship with Shae. Though he claims he will not lie for him, he implores Tyrion to send Shae away for her own safety. Varys later testifies against Tyrion at his trial for murdering Joffrey, but when Tyrion is convicted, Jaime enlists Varys' help in smuggling Tyrion out of King's Landing to Essos. As Varys prepares to return to the Red Keep, he hears the tolling bells, making him realise that Tyrion's escape has been discovered (along with his murder of Tywin), and he joins Tyrion on the voyage to Essos
====Season 5====
Varys and Tyrion arrive in Pentos, where he reveals his allegiance to House Targaryen and convinces Tyrion to travel with him to Meereen and aid Daenerys Targaryen in retaking the Iron Throne. While in Volantis, Tyrion is kidnapped by Daenerys' former advisor Jorah Mormont. Varys journeys on to Meereen, where he arrives to discover Tyrion in control of the city in Daenerys' absence. He offers Tyrion the use of his spy network to maintain order in the city.
====Season 6====
Varys discovers that the Meereenese insurgency the Sons of the Harpy are funded by the masters of Yunkai and Astapor and the slavers of Volantis, and brokers a meeting between Tyrion and representatives of those cities. The slavers agree a truce with Tyrion and Meereen begins to prosper. Varys departs for the Seven Kingdoms, telling Tyrion that he will seek out allies for Daenerys. His ultimate destination is Dorne, where Ellaria Sand has killed Doran Martell out of anger at his inaction against the Lannisters and seized power. There he meets with Ellaria and Olenna Tyrell, who also seeks vengeance against Cersei as her machinations have caused the death of all other Tyrells, and forms an alliance. Varys returns to Meereen with ships from Dorne and the Reach, and sets sail for Westeros with Daenerys and her army.

==TV adaptation==
Varys is played by Northern Irish actor [[Conleth Hill]] in the television adaption of the series of books.<ref>{{cite web|url=http://www.vulture.com/2015/02/conleth-hill-varys-game-of-thrones.html|title='Game of Thrones': Conleth Hill on Varys|work=Vulture}}</ref> Author George R. R. Martin wrote on the casting of Hill as Varys, saying, "Hill, like Varys, is quite a chameleon, an actor who truly disappears inside the characters he portrays, more than capable not only of bringing the slimy, simpering eunuch to life."<ref>{{Cite web|url=http://grrm.livejournal.com/163777.html |title=Along Came a Spider |work=Not a Blog |first=George R. R. |last=Martin |accessdate=October 10, 2016 |deadurl=yes |archiveurl=https://web.archive.org/web/20150505130200/http://grrm.livejournal.com/163777.html |archivedate=May 5, 2015 |df= }}</ref>

==References==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Varys}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional characters introduced in 1996]]
[[Category:Fictional secret agents and spies]]
[[Category:Fictional nobility]]
[[Category:Fictional victims of child abuse]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional asexuals]]