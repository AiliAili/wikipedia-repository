{{good article}}
{{Infobox officeholder
| name        = Peter Aquino Aduja<ref name="SB25MAR07"/>
| image       = File:Peter Aduja.jpeg
| alt         = 
| caption     = Family photo
| birth_name  = 
| birth_date  = {{birth date|1920|10|19|df=y}}<ref name="SB25MAR07"/>
| birth_place = Salindig, [[Vigan]], [[Ilocos Sur]], [[Luzon]], [[Insular Government of the Philippine Islands|Philippine Islands]]<ref name="SB22FEB07">{{cite news |last=Borreca |first=Richard |date=22 February 2007 |title=Lawmaker first U.S. Filipino to hold office |url=http://archives.starbulletin.com/2007/02/22/news/story05.html |newspaper=Star Bulletin |location=Honolulu |accessdate=8 October 2014 }}</ref>
| death_date  = {{death date and age|2007|2|19|1920|10|19|df=y}}<ref name="SB25MAR07">{{cite news |author=<!--Staff writer(s); no by-line.--> |date=25 March 2007 |title=Services set for Thursday for pioneering politician |url=http://archives.starbulletin.com/2007/03/25/news/story14.html |newspaper=Star Bulletin |accessdate=11 October 2014 }}</ref>
| death_place = [[Las Vegas Valley|Las Vegas]], [[Nevada]]<ref name="GMA30MAR07"/>
| resting_place = [[Hawaii State Veterans Cemetery]]<ref name="Midweek">{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Funeral Set Thursday For Former Rep. Peter Aduja |url=http://archives.midweek.com/content/zones/windward_news_article/funeral_set_thursday_for_former_rep_peter_aduja/ |newspaper=MidWeek |date=28 March 2007 |accessdate=12 October 2014 }}</ref>
| nationality = American
| other_names = 
| known_for   = 
| occupation  = Soldier, teacher, judge, politician

| office             = [[Hawaii House of Representatives|Territorial House of Representatives member]]
| term_start         = 1954
| term_end           = 1956<ref name="Boylan1991"/>

| office2            =  [[Hawaii State District Courts|District Court Judge]]
| term_start2        =  1960
| term_end2          =  1962<ref name="GMA30MAR07"/>

| office3            =  State House of Representatives member, 23rd District<ref>{{cite book |last=Clements |first=John |year=1972 |title=Taylor's Encyclopedia of Government Officials, Federal and State |url=https://books.google.com/books?ei=1Ok6VPCEIMWIiwK5kIGQAw&id=ehGHAAAAMAAJ&dq=Taylor%27s+Encyclopedia+of+Government+Officials%2C+Federal+and+State+%22Peter+Aduja%22&focus=searchwithinvolume&q=Peter+Aduja  |publisher=Political Research, Incorporated |page=76 }}</ref>
| term_start3        =  1966
| term_end3          =  1974<ref name="GMA30MAR07"/>

| party       = [[Hawaii Republican Party|Republican]]<ref name="GMA30MAR07">{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Hawai'i honors Fil-Am legislator |url=http://www.gmanetwork.com/news/story/36369/ulatfilipino/balitangpinoy/hawai-i-honors-fil-am-legislator |newspaper=GMA News |date=30 March 2007 |accessdate=8 October 2014 }}</ref><ref>{{cite book|author=Gary G. Aguiar|title=Party Mobilization, Class and Ethnicity: The Case of Hawaii, 1930 to 1964|url=https://books.google.com/books?id=nfB2Il8uv6YC&pg=PA297|date=August 1997|publisher=Universal-Publishers|isbn=978-0-9658564-3-0|page=297}}</ref>
<!--Military service-->
| nickname           = 
| allegiance         = 
| branch             = [[United States Army]]<ref name="GMA30MAR07"/>
| serviceyears       = 1944-1946<ref name="GMA30MAR07"/>
| rank               = [[First lieutenant#U.S. Army, U.S. Marine Corps and U.S. Air Force|1st Lieutenant]]<ref name="GMA30MAR07"/>
| unit               = [[1st Filipino Infantry Regiment]]<ref name="GMA30MAR07"/><ref>{{cite journal |last=Revilla |first=Linda A. |year=1996 |title="Pineapples", "Hawayanos," and "Loyal Americans": Local Boys in the Firist Filipino Infantry Regiment US Army|url=http://www.sociology.hawaii.edu/research/social_process_in_hawaii/volume_37.pdf |journal=Social Process in Hawaii |publisher=Department of Sociology, University of Hawaii at Manoa |volume=37 |editor1-first=Kiyoshi |editor1-last=Ikeda |editor2-first=Michael G. |editor2-last=Weinstein |editor3-last=Okamura |editor3-first=Jonathan Y. |pages=57–73 |issn=0737-6871 |archiveurl=http://www.efilarchives.org/pdf/social%20process%20vol%2037/sp37_revilla_localboys.pdf |archivedate=2006 |accessdate=12 October 2014}}<br/>{{cite news |last=Viotti |first=Vicki |date=29 September 2002 |title=Tale of Filipino bravery about to be told |url=http://the.honoluluadvertiser.com/article/2002/Sep/29/ln/ln21a.html |newspaper=Honolulu Advertiser |accessdate=12 October 2014 }}</ref>
| commands           = 
| battles            = 
}}

'''Peter Aquino Aduja''' (19 October 1920 – 19 February 2007) was the first [[Filipino American]] elected to public office in the United States. He was elected as a representative in the [[Hawaii Legislature]] in 1954.<ref name="SB22FEB07"/><ref>{{cite book|author=Jon Sterngass|title=Filipino Americans|url=https://books.google.com/books?id=-gjvUvVY-ngC&pg=PA128|date=1 January 2009|publisher=Infobase Publishing|isbn=978-1-4381-0711-0|page=128}}</ref>

Born in the Philippines, Aduja emigrated to Hawaii in his youth, and then served in the [[United States Army]] during [[World War II]]. After World War II, he worked as a teacher, before becoming one of the first [[Filipinos in Hawaii|Filipino lawyers in Hawaii]]. After two years in elected office, he worked for the Hawaiian Department of Attorney General, and served two years as a judge, until being elected to the [[Hawaii House of Representatives]]. Aduja died in [[Las Vegas, Nevada|Las Vegas]] in 2007.

==Early life==
Aduja was born in [[Ilocos Sur]] in the [[Philippines]] and emigrated with his family at the age of eight to [[Hilo, Hawaii]].<ref name="HAObit2007">{{cite news |title=Peter Aduja, distinguished local Filipino |author=Rod Ohira |url=http://the.honoluluadvertiser.com/article/2007/Feb/22/ln/FP702220334.html |newspaper=Honolulu Advertiser |date=22 February 2007 |accessdate=4 September 2011}}</ref> He was raised in nearby [[Hakalau, Hawaii]], while his father worked on a [[sugarcane]] [[Sugar plantations in Hawaii|plantation]] as a [[sakada]].<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Peter Aduja |page=B2 |url=http://www.ajdigitaledition.com/pdfs/PDF/2009_LA/2009_05_27/2009_LA_05_27_B%202.pdf |newspaper=Asian Journal |location=Los Angeles |date=27 May 2009 |access-date=30 January 2015 }}</ref> He attended [[Hilo High School|Hilo High]], where he was the [[student body president]]<ref name="Boylan1991"/> and an [[Eagle Scout (Boy Scouts of America)|Eagle Scout]],<ref name="GMA">{{cite news |title=First Filipino lawmaker in US is dead at 87 |url=http://www.gmanetwork.com/news/story/31840/ulatfilipino/balitangpinoy/first-filipino-lawmaker-in-us-is-dead-at-87 |newspaper=GMA News |date=23 February 2007 |accessdate=26 October 2014}}</ref> graduating with the class of 1941 as [[salutatorian]].<ref name="Boylan1991"/> After high school, he went on to the [[University of Hawaii]] to [[Academic major|major]] in government and history;<ref name="HAObit2007" /> while attending university Aduja worked as a [[Timekeeper#Timekeeper job|timekeeper]] at [[Pearl Harbor, Hawaii|Pearl Harbor]].<ref name="Boylan1991"/> In 1944, he joined the [[United States Army]], and along with 50 other individuals volunteered for the [[1st Filipino Infantry Regiment (United States)|1st Filipino Infantry Regiment]].<ref name="HAObit2007" />

Following [[World War II]] he married Melodie "Lesing" Cabalona (died 2002). He taught on the island of Hawaii, at Naalehu Intermediate School, before attending [[Boston University]], where in 1951 he earned a law degree.<ref name="Boylan1991"/><ref name="GMA" /><ref>{{cite web |url=http://politicalgraveyard.com/bio/adamske-aedanus.html#430.89.24 |title=Adams-medina to Aedanus |author=Lawrence Kestenbaum |date=December 2013 |website=politicalgraveyard.com |publisher=Lawrence Kestenbaum |accessdate=28 October 2014 }}<br/>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Obituaries |url=http://the.honoluluadvertiser.com/article/2002/Jun/01/ln/ln48aobituaries.html |newspaper=Honolulu Advertiser |date=1 June 2002 |accessdate=28 October 2014 }}</ref> In 1953, along with [[Benjamin Menor|Ben Menor]] (later a justice on [[Supreme Court of Hawaii|Hawaii's Supreme Court]]), Aduja took the bar examination, and both became Hawaii's first Filipino lawyers.<ref name="Boylan1991"/><ref>{{cite book|author=Benjamin B. Domingo|title=Hawaii's eminent Filipinos|url=https://books.google.com/books?id=N390AAAAMAAJ|year=1983|publisher=Foreign Service Institute|isbn=978-971-11-5004-4|page=16}}</ref>

==Political career==
In 1954, Aduja was elected to the [[Territory of Hawaii|Territorial]] [[Hawaii House of Representatives|House of Representatives]], becoming the first Filipino to be elected to public office in Hawaii and the United States.<ref name="HAObit2007" /><ref>{{cite book|author1=Valerie Ooka Pang|author2=Li-Rong Lilly Cheng|title=Struggling To Be Heard: The Unmet Needs of Asian Pacific American Children|url=https://books.google.com/books?id=QQn0GNFehA8C&pg=PA172|date=3 September 1998|publisher=SUNY Press|isbn=978-0-7914-3840-4|page=172}}</ref> He represented one of three seats of the island of [[Hawaii (island)|Hawaii]].<ref name="Boylan1991"/> In 1956, he spoke on behalf of the Republican Party at the [[ILWU]] territorial convention in [[Hilo, Hawaii|Hilo]].<ref>{{cite journal |author=<!--Staff writer(s); no by-line.--> |title=GOP Speakers at ILWU Functions Run from Roy Vitousek to Peter Aduja |url=http://www.hawaii.edu/uhwo/clear/HonoluluRecord/articles/v10n10/GOP%20Speakers%20at%20ILWU%20Functions%20Run%20from%20Roy%20Vitousek%20to%20Peter%20Aduja.html |volume=10 |issue=10 |journal=Honolulu Record |date=3 October 1957 |page=6 |accessdate=11 October 2014 }}</ref> After a single term in office, he went on to work for the [[Attorney General of Hawaii|Department of Attorney General]].<ref name="HAObit2007" /> In 1959, he ran for State Senate from Oahu, a year when fellow Republican [[William F. Quinn]] was elected as the state's first governor, and lost.<ref name="Boylan1991"/>  From 1960 to 1962, he was a district court judge,<ref name="GMA30MAR07"/> resigning from the bench in June 1962.<ref>{{cite book|author=Hawaii. Supreme Court|title=Annual Report of the Chief Justice|url=https://books.google.com/books?id=dUcsAQAAMAAJ|year=1960|page=53}}</ref> After two years out of the public sector, in 1966, he was elected again to public office, this time as a member of the Hawaii House of Representatives.<ref name="HAObit2007" /> His district included [[Kailua, Honolulu County, Hawaii|Kailua]], the [[North Shore (Oahu)|North Shore]], and [[Kahuku, Hawaii|Kahuku]].<ref name="Boylan1991"/> While a member of the Hawaii House of Representatives, he was a delegate at the [[Hawaii State Constitution]]'s 1968 constitutional convention.<ref>{{cite news |author=<!--Staff writer(s); no by-line.--> |title=Hawai'i honors Fil-Am legislator |url=http://www.gmanetwork.com/news/story/36369/ulatfilipino/balitangpinoy/hawai-i-honors-fil-am-legislator |newspaper=GMA News |date=30 March 2007 |accessdate=12 October 2014 }}</ref> Aduja departed the Hawaii House of Representatives in 1974,<ref name="HAObit2007" /> and returned to the public sector in his final position as a member of the City of [[Honolulu]]'s [[Kaneohe, Hawaii|Kaneohe]] [[Neighborhood Boards of Honolulu|Neighborhood Board]], which he was on from 1986 until 1994.<ref name="Midweek"/>

==Later years==
In 1991, Professor Dan Boylan wrote that Aduja was one of three important Filipino politicians in Hawaii during the beginning era of Filipino politics in Hawaii.<ref name="Boylan1991">{{cite journal |last=Boylan |first=Dan |year=1991 |title=Crosscurrents: Filipinos in Hawaii's Politics |url=https://scholarspace.manoa.hawaii.edu/bitstream/handle/10125/22978/Vol_33.pdf?sequence=1 |journal=Social Process in Hawaii |publisher=Department of Sociology, University of Hawaii at Manoa |volume=33 |editor1-last=Okamura |editor1-first=Jonathan Y. |editor2-last=Agbayani |editor2-first=Amefil R. |editor3-last=Kerkvliet |editor3-first=Melinda Tria  |pages=39–55 |issn=0737-6871 |archiveurl=http://www.efilarchives.org/pdf/social%20process%20vol%2033/boylan_politics.pdf |archivedate=2006 |accessdate=11 October 2014}}</ref> Along with [[Alfred Laureta]] and Ben Menor, and a few others minor individuals, they were the few Filipinos in elected office or in significant public office in Hawaii in the mid-20th century.<ref name="Boylan1991"/> On 19 February 2007, he died while on vacation in Las Vegas.<ref name="GMA30MAR07"/> Governor [[Linda Lingle]] declared 29 March 2007 to be Peter A. Aduja Day.<ref>{{cite web |url=http://archive.lingle.hawaii.gov/govgallery/news/proclamations/2007/mar/3.29%20Peter%20Aduja.pdf |title=Proclamation |last1=Lingle |first1=Linda |authorlink1=Linda Lingle |date=19 March 2007 |website=Governor Linda Lingle |publisher=State of Hawaii |accessdate=11 October 2014}}</ref> Aduja was survived by two children and two grandchildren;<ref name="HAObit2007"/> one of whom is [[Melodie Aduja]], a former [[Hawaii Senate|Hawaii state senator]].<ref>{{cite news |title=No Lingle coattails: Republicans lose four House seats |author=Ben DiPietro |url=http://www.bizjournals.com/pacific/stories/2002/11/04/daily45.html |newspaper=Pacific Business News |date=6 November 2002 |accessdate=4 August 2011}}</ref>

==References==
{{reflist|colwidth=30em}}

==External links==
*{{cite web |url=http://phillipinodedication.blogspot.com/2010/05/brief-look-at-life-of-peter-aduja.html |title=A Brief Look at the Life of Peter Aduja |author=<!--Staff writer(s); no by-line.--> |date=6 May 2010 |website=Filipino Immigration to the Hawaiian Islands |publisher=blogspot }}
*{{cite web |url=https://www.leg.state.nv.us/Session/74th2007/Bills/SCR/SCR28_EN.pdf |title=Senate Concurrent Resolution No. 28 |author=<!--Staff writer(s); no by-line.--> |year=2007 |website=74th (2007) Session |publisher=Nevada Legislative Counsel Bureau}}

{{DEFAULTSORT:Aduja, Peter}}
[[Category:1920 births]]
[[Category:2007 deaths]]
[[Category:Members of the Hawaii House of Representatives]]
[[Category:Members of the Hawaii Territorial Legislature]]
[[Category:20th-century American politicians]]
[[Category:Hawaii state court judges]]
[[Category:Hawaii Republicans]]
[[Category:American politicians of Filipino descent]]
[[Category:American military personnel of World War II]]
[[Category:American military personnel of Filipino descent]]
[[Category:Filipino emigrants to the United States]]
[[Category:Eagle Scouts]]
[[Category:University of Hawaii alumni]]
[[Category:Boston University School of Law alumni]]
[[Category:People from Vigan]]
[[Category:People from Hilo, Hawaii]]
[[Category:People from Hawaii County, Hawaii]]
[[Category:20th-century American judges]]