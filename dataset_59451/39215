{{good article}}
{{Infobox album
|Name=Cambodian Rocks
|Type=Compilation
|Cover=Cambodian Rocks album cover.jpg
|Alt=Cambodian Rocks album cover. The image is a charcoal rubbing taken from Angkor Wat.
|Released=1996
|Genre=[[Psychedelic rock]], [[garage rock]], [[Psychedelic pop]], [[romvong]]
|Label=Parallel World
|Length={{duration|m=69|s=34}}
|Compiler=Paul Wheeler
|Language=[[Khmer language|Khmer]]
|Recorded=1960s-1970s
}}
'''''Cambodian Rocks''''' is a compilation of 22 uncredited, untitled Cambodian [[psychedelic rock|psychedelic]] and [[garage rock]] songs from the late 1960s and early 1970s. When the tracks were recorded, musicians in the thriving music scene were combining Western rock and pop genres with their own styles and techniques. When the [[Khmer Rouge]] came to power in 1975, artists were among those viewed as a threat to the regime's [[agrarian socialist]] vision, and several of the performers on the album are believed to have been among those killed during the ensuing [[Cambodian Genocide]] of 1975-1979. Little information about them or their creative output has survived.

The compilation was assembled from cassette tapes purchased by an American tourist in 1994 and released on the Parallel World label in 1996. The album has been lauded for its music as well as its historical and cultural significance, though the label has been criticized for reissuing it years later without working to identify those involved. Through collaboration on the Internet, the songs have all been identified. In 2015 a documentary film about Cambodian music was released, inspired by ''Cambodian Rocks''.

==Historical context==
{{main article|History of Cambodia|Khmer Rouge rule of Cambodia|Cambodian genocide}}
In the years before the [[Khmer Rouge]] came to power in 1975, Cambodia had a flourishing music scene. Particularly in [[Phnom Penh]], artists were combining traditional and native styles with those from the West.<ref name=allmusic/>

The Khmer Rouge regime, led by [[Pol Pot]], wanted to return the nation of Cambodia to an idyllic notion of the past by implementing a radical form of [[agrarian socialism]] while simultaneously shunning outside aid and influence.<ref name=alvarez/> In order to build and protect their [[utopia]]n goals, the regime perceived enmity in anyone tied to the previous Cambodian governments, ethnic and religious minorities, intellectuals, and members of certain professions.<ref name=frey/> Artists posed a threat due to their own influence on culture, incompatibility with an agrarian lifestyle, or exhibiting foreign influence. Between 1975 and 1979, about 2 million people (25% of the country's population) were killed during the ensuing [[Cambodian Genocide]].<ref name=frey/> Several of the artists on ''Cambodian Rocks'' are thought to have been among those killed, and information about them destroyed along with much of their creative output.<ref name=wfmublog/><ref name=publicculture/>

==Production==
{{listen|type=music|filename=Yol Aularong - Yuvajon Kouge Jet - Cambodian Rocks (sample).ogg|title=Yol Aularong - "Yuvajon Kouge Jet"|description=Sample of Yol Aularong's "Yuvajon Kouge Jet" (track 13)|filename2=Sinn Sisamouth - Srolanh Srey Touch - Cambodian Rocks (sample).ogg|title2=Sinn Sisamouth - "Srolanh Srey Touch"|description2=Sample of Sinn Sisamouth's "Srolanh Srey Touch" (track 05)}}

While a tourist in Cambodia in 1994, American Paul Wheeler became interested in music he had been hearing around [[Siem Reap]]. He purchased "about six tapes" of music from a market vendor and assembled a [[mixtape]] of his favorite tracks.<ref name=boulware/><ref name=psychfunk/> His friend at the Parallel World label in New York City, upon hearing the mixtape, agreed to release 1,000 copies on vinyl. When they sold out, the label issued the much more widely known CD version, containing 22 songs instead of the original 13.<ref name=wfmublog/><ref name=publicculture/><ref name=boulware/>

Several reviewers have likened the album to a [[bootleg recording|bootleg]].<ref name=allmusic/><ref name=publicculture/><ref name=aquariumdrunkard/> Neither Wheeler nor Parallel World provided any information about the artists or names of the tracks.<ref name=wfmublog/> When re-issued in 2003, reviewer Mack Hagood criticized Parallel World for reifying its bootleg status by retaining the anonymous track list rather than working to look for surviving artists or their families, who would likely benefit greatly from royalties.<ref name=fareastaudio/>{{refn|group=note|Cambodia had only just passed its first copyright law in 2003, allowing families to claim the artists' intellectual property for the first time. In 2014, Sinn Sisamouth's family provided proof of composition and was awarded ownership of copyright over 180 songs. The event was commemorated with a celebration and tribute concert.<ref name=sotheavy/><ref name=seangly/>}} With information more widely available on the Internet and the opportunity to collaborate, all tracks have since been identified.<ref name=wfmublog/>

The cover art since the first release has been a [[charcoal]] [[stone rubbing|rubbing]] taken from one of the [[bas relief]]s in the [[Angkor Wat]] temple.<ref name=fareastaudio/>

==Musical style==
[[File:Pan Ron.jpg|thumb|upright|Singer [[Pan Ron]] c. 1968]]

The recordings reflect the influence of Western rock and pop music in general and that of the United States in particular due to its heavy involvement in [[Southeast Asia]] during the [[Vietnam War]].<ref name=publicculture/> The compilation is varied, combining a range of popular Western genres like [[garage rock|garage]], [[psychedelic rock|psychedelic]], and [[surf rock]], with Khmer vocal techniques, instrumental innovations, and the popular [[romvong]] "circle dance music" trend.<ref name=allmusic/><ref name=wfmublog/><ref name=publicculture/><ref name=losercity/><ref name=rollingstone/><ref name=adventures/> Several reviews describe the uncanny quality of the songs using words like "mystery" and "familiarity", to help account for the album's wide appeal at a time of its release when such compilations were less common.<ref name=allmusic/><ref name=publicculture/><ref name=nytimes/> [[Yol Aularong]]'s "Yuvajon Kouge Jet", for example, has been noted as sounding like a "fuzzed-out, reverb-soaked"<ref name=publicculture/> "go-go organ and fuzz-guitar"<ref name=boulware/> [[cover song|cover]] of [[Them (band)|Them]]'s "[[Gloria (Them song)|Gloria]]".<ref name=allmusic/> Reviewers also noted similarities between [[Sinn Sisamouth]]'s "Srolanh Srey Touch" and [[Santana (band)|Santana]]'s cover of [[Fleetwood Mac]]'s "[[Black Magic Woman]]".<ref name=adventures/> Other tracks have been likened to [[Booker T. & the M.G.'s]] and [[The Animals]].<ref name=publicculture/><ref name=boulware/>

Several of the artists on ''Cambodian Rocks'' had been quite successful before the Khmer Rouge, while others achieved fame posthumously.<ref name=wfmublog/><ref name=publicculture/><ref name=losercity/>

Sinn Sisamouth (1932-1976) was a prolific [[singer-songwriter]] with a [[crooning]] voice that has been likened to that of [[Nat King Cole]].<ref name=smh/> He got his start singing on the radio while in medical school in his early 20s, and was admitted to the Royal Treasury's classical ensemble, with whom he performed at state functions.<ref name=chambersletson/> By the late-1950s and early-1960s he was one of the most popular musicians in the country.<ref name=chambersletson/> As a solo artist in the 1960s he performed with a rock band rather than traditional wind instrument backing, experimenting with combining Khmer music with Western sounds.<ref name=newsday/> Sometimes called the "King of Khmer music", the Cambodian [[Elvis Presley|Elvis]]", or the "golden voice", his lasting cultural impact is difficult to overstate.<ref name=sotheavy/><ref name=seangly/><ref name=allthingsconsidered/><ref name=ocweekly/> He was killed by execution squad during the genocide in 1976, but his voice remains one of the country's most recognizable.<ref name=smh/><ref name=sassoon/><ref name=jackson/><ref name=vice/>

The "King of Khmer music" is credited with discovering "the queen of Cambodian rock 'n' roll", [[Ros Sereysothea]], who had been singing at weddings.<ref name=smh/><ref name=chambersletson/> Coming from a poor rural background, she moved to [[Phnom Penh]] and found success singing on national radio.<ref name=newsday/> She was known for her take on traditional folk songs but increasingly incorporated Western genres and instrumentation, even issuing cover versions of a number of Western hits.<ref name=chambersletson/><ref name=vice/> Under the Khmer Rouge, she was forced to perform for political leaders and to marry a party official, though accounts of her fate differ and no specifics have been confirmed.<ref name=smh/><ref name=vice/>

The other artists did not achieve the same degree of success as Sisamouth or Sereysothea, and even less information about them survives. The ''[[Sydney Morning Herald]]'' called [[Pan Ron]] "the most risque of Cambodian singers of the 1960s to mid-1970s [who] hopped genres from traditional Khmer music to covers or localised takes on western rock, twist, cha cha, mambo, jazz and folk."<ref name=smh/> According to an unconfirmed account, she died sometime after the 1978 Vietnamese invasion of Cambodia, probably killed by the Khmer Rouge.<ref name=smh/> [[Yol Aularong]] was one of the subjects of the 2015 documentary ''[[Don't Think I've Forgotten]]''. In a review of the film, ''[[The New York Times]]'' described him as "a charismastic proto-punk who mocked conformist society."<ref name=nytimes/>

==Reception and legacy==
The album has received praise for its musical content as well as historical and cultural significance. ''[[Rolling Stone]]'' called it "a marvel of cultural appropriation"<ref name=rollingstone/> and said it exhibits "all manner of virtuosity".<ref name=rollingstone955/> ''Far East Audio'' ascribed it the label "instant classic",<ref name=fareastaudio/> while ''Allmusic'' said the album is "an incredible historical document of late-'60s to early-'70s Cambodian rock".<ref name=allmusic/> Nick Hanover called it an "unpredictably playful" mix with "each track&nbsp;... a continuous surprise, a fusion of elements that should be contradictory but somehow strike a balance of West and East&nbsp;– organ hooks swerving between fierce guitar riffs that antagonize vocals that frequently sound closer to ghostly siren wails than traditional pop melodies".<ref name=losercity/> ''[[The New York Times]]'' said the album and the circumstances of its release "established a lasting aura of mystery around the music".<ref name=nytimes/>

''Cambodian Rocks'' was the first release of its kind, followed by a number of similar compilations like ''Cambodian Cassette Archives'' and other unrelated titles by the same or similar names, inspired by the original or exploiting its weak standing with regard to intellectual property.<ref name=psychfunk/><ref name=aquariumdrunkard/><ref name=fareastaudio/><ref name=nytimes/> It also gave rise, in part, to a broader trend of Western interest in obscure psychedelic and progressive rock from the rest of the world.<ref name=nytimes/>

The California band [[Dengue Fever (band)|Dengue Fever]], known for its Khmer rock performances, has recorded a number of covers of tracks from ''Cambodian Rocks''.<ref name=wfmublog/><ref name=nytimes/><ref name=singout/> The album inspired founding member [[Ethan Holtzman]] to travel to Cambodia, forming the band upon his return to California.<ref name=publicculture/> The band's singer is Cambodian emigrant [[Chhom Nimol]], who lived in a refugee camp in Thailand during the Khmer Rouge.<ref name=audrey/>

Filmmaker [[John Pirozzi]] received a copy of the album while in Cambodia filming ''[[City of Ghosts]]'' and began researching the stories of the artists. Eventually, he tracked down material to create ''[[Don't Think I've Forgotten]]'', a 2015 documentary about pre-Khmer Rouge Cambodian music which takes its name from a Sinn Sisamouth song.<ref name=nytimes/><ref name=columbusalive/><ref name=bostonglobe/><ref name=osullivan/>
{{clear}}

==Track listing==
[[File:Sinn Sisamouth.jpg|thumb|upright|Singer/songwriter [[Sinn Sisamouth]]]]
''Cambodian Rocks'' includes 22 songs. Although the album provides no track information, fans and researchers have identified the artists and song names. The table below includes both the original [[romanization of Khmer|romanized Khmer]] titles and English translations.<ref name=wfmublog/><ref name=fareastaudio/>

{|class="wikitable"
|-
!Number!! Duration !! Artist(s) !! Original title (romanized Khmer) !! English translation
|-
|1||4:31||[[Yol Aularong]]||"Jeas Cyclo"||"Riding a Cyclo"
|-
|2||3:50||[[Ros Sereysothea]]||"Chnam oun Dop-Pram Muy"||"I'm 16"
|-
|3||2:14||[[Ros Sereysothea]]||"Tngai Neas Kyom Yam Sra"||"Today I Drink Wine"
|-
|4||2:12||[[Yol Aularong]] and Liev Tuk||"Sou Slarp Kroam Kombut Srey"||"Rather Die Under the Woman's Sword"
|-
|5||2:57||[[Sinn Sisamouth]]||"Srolanh Srey Touch"||"I Love Petite Girls"
|-
|6||2:34||[[Pan Ron]]||"Rom Jongvak Twist"||"Dance Twist"
|-
|7||3:14||[[Pan Ron]]||"Knyom Mun Sok Jet Te"||"I'm Unsatisfied"
|-
|8||3:23||Liev Tuk||"Rom Sue Sue"||"Dance Soul Soul"
|-
|9||3:23||[[Ros Sereysothea]]||"Jam 10 Kai Thiet"||"Wait 10 More Months"
|-
|10||3:36||[[Ros Sereysothea]]||"Jah Bong Ju Aim"||"Old Sour & Sweet"
|-
|11||1:54||[[Sinn Sisamouth]], [[Ros Sereysothea]], [[Pan Ron]], and Dara Jamchan <small>(composed by Voy Ho)</small>||"Maok Pi Naok"||"Where Are You From?"
|-
|12||4:00||[[Sinn Sisamouth]]||"Phneit Oun Mean Evey"||"What Are Your Eyes Made Of?"
|-
|13||3:27||[[Yol Aularong]]||"Yuvajon Kouge Jet"||"Broken Hearted Man"
|-
|14||3:43||Meas Samon <small>(composed by Mai Bun)</small>||"Jol Dondeung Kone Key"||"Going to Get Engaged"
|-
|15||3:19||[[Ros Sereysothea]]||"Kerh Snae Kyoum Thai"||"Have You Seen My Boyfriend?"
|-
|16||3:22||[[Ros Sereysothea]]||"Chnang Jas Bai Chgn-ainj"||"Old Pot, Tasty Rice"
|-
|17||3:37||[[Ros Sereysothea]] and Dara Jamchan <small>(composed by Voy Ho)</small>||"Kone Oksok Nas Pa"||"We're Very Bored, Dad!"
|-
|18||3:19||[[Ros Sereysothea]]||"Kom Kung Twer Evey"||"Don't Be Mad"
|-
|19||3:11||[[Ros Sereysothea]]||"Penh Jet Thai Bong Mouy"||"I Like Only You"
|-
|20||2:20||[[Pan Ron]] and In Yeng||"Sralanh Srey Chnas"||"I Love Mean Girls"
|-
|21||2:07||[[Sinn Sisamouth]] and Meas Samon <small>(composed by Voy Ho)</small>||"Komlos Teng Bey"||"Three Gentlemen"
|-
|22||3:21||[[Ros Sereysothea]]||"Retrey Yung Joup Knea"||"The Night We Met"
|}

==See also==
{{portal|Cambodia|Music}}
*[[Dance in Cambodia]]
*[[Music of Cambodia]]
*[[Traditional Cambodian musical instruments]]

==Notes==
{{reflist|group=note}}

==References==
{{reflist|3|refs=
<ref name=allmusic>{{cite web|work=Allmusic|title=Various Artists - Cambodian Rocks|last=Samuelson|first=Sam|url=http://www.allmusic.com/album/cambodian-rocks-mw0000010619}}</ref>
<ref name=nytimes>{{cite web|work=New York Times|url=https://www.nytimes.com/2015/04/12/movies/dont-think-ive-forgotten-a-documentary-revives-cambodias-silenced-sounds.html|title=‘Don’t Think I’ve Forgotten,’ a Documentary, Revives Cambodia’s Silenced Sounds|date=9 April 2015|last=Sisario|first=Ben}}</ref>
<ref name=wfmublog>{{cite web|url=http://blog.wfmu.org/freeform/2007/12/cambodian-rocks.html|title=Cambodian Rocks (MP3s)|date=9 December 2007|work=WFMU blog}}</ref>
<ref name=aquariumdrunkard>{{cite web|work=Aquarium Drunkard|url=http://www.aquariumdrunkard.com/2009/01/08/cambodia-rocks-sounds-from-the-60s-70s/|title=Cambodia Rocks : Sounds From The '60s & '70s|date=9 January 2009|last=Gage|first=Justin}}</ref>
<ref name=columbusalive>{{cite web|work=Columbus Alive|url=http://www.columbusalive.com/content/stories/2015/05/28/film-preview-director-john-pirozzi-traces-the-history-of-early-cambodian-rock-n-roll-in-dont-think-ive-forgotten.html|title=Film preview: Director John Pirozzi traces the history of early Cambodian rock ’n’ roll in "Don’t Think I’ve Forgotten"|date=28 May 2015|last=Downing|first=Andy}}</ref>
<ref name=bostonglobe>{{cite web|url=https://www.bostonglobe.com/arts/music/2015/04/23/new-film-explores-cambodia-rock-scene/iqI1bT1rFXh4a0cMHslJaK/story.html|work=Boston Globe|last=Reed|first=James|date=23 April 2015|title=‘Don’t Think I’ve Forgotten’ explores Cambodia’s rock scene}}</ref>
<ref name=audrey>{{cite web|title=Voices Carry: Chhom Nimol|url=http://audreymagazine.com/voices-carry-chhom-nimol/|website=Audrey Magazine|publisher=Audrey Magazine|last=Tseng|first=Ada|date=11 March 2014}}</ref>
<ref name=singout>{{cite web|via=Highbeam|url=http://www.highbeam.com/doc/1G1-238176777.html|title=Various: Dengue Fever Presents: Electric Cambodia.(Sound recording review)|last=Pryor|first=Tom|work=Sing Out!|date=22 June 2010}}</ref>
<ref name=psychfunk>{{cite web|last=Phillips|first=Jeremy|url=http://psychfunk.com/crate/cambodian-rocks-vol-1-various-artists/|work=PsychFunk|title=Cambodian Rocks}}</ref>
<ref name=publicculture>{{cite journal|url=http://www.music.ucsb.edu/sites/secure.lsit.ucsb.edu.musi.d7/files/sitefiles/people/novak/Novak2011SFPublicCulture.pdf|work=Public Culture|title=The Sublime Frequencies of New Old Media|last=Novak|first=David|volume=23|number=3|date=Fall 2011|doi=10.1215/08992363-1336435}}</ref>
<ref name=frey>{{cite book|last=Frey|first=Rebecca Joyce|year=2009|title=Genocide and International Justice|publisher=Infobase Publishing|isbn=0816073104}}</ref>
<ref name=alvarez>{{cite book|last=Alvarez|first=Alex|date=2001|title=Governments, Citizens, and Genocide: A Comparative and Interdisciplinary Approach|publisher=Indiana University Press|isbn=978-0253338495}}</ref>
<ref name=fareastaudio>{{cite web|url=http://www.fareastaudio.com/archives/04/04/cambodian_rocks_by_various.php|archive-url=https://web.archive.org/web/20090107050551/http://www.fareastaudio.com/archives/04/04/cambodian_rocks_by_various.php|archive-date=7 January 2009|work=Far East Audio Review|date=26 April 2004|title=Various - Cambodian Rocks - Parallel World - Cambodia|last=Hagood|first=Mack}}</ref>
<ref name=boulware>{{cite web|url=http://www.jackboulware.com/writing/journalism/dengue-fever-and-cambodian-rocks|work=American Way|via=Jack Boulware|date=8 April 2009|title=Dengue Fever and Cambodian Rocks}}</ref>
<ref name=losercity>{{cite web|last=Hanover|first=Matt|work=Loser City|url=http://loser-city.com/features/cambodian-rocks|title=Today We Drink Wine: Looking Back at the Tragic History of Cambodian Pop|date=19 June 2015|accessdate=9 January 2016}}</ref>
<ref name=rollingstone>{{cite journal|work=Rolling Stone|date=18 January 2001|page=61|number=860|title=Albums of the year from under the radar and off the map|last=Fricke|first=David|via=ProQuest}}</ref>
<ref name=rollingstone955>{{cite journal|work=Rolling Stone|date=19 August 2004|page=71|number=955|title=Hot Scene|last=Bearman|first=Joshuah|via=ProQuest}}</ref>
<ref name=adventures>{{cite web|work=Adventures In Sound|title=First They Killed the Musicians: Cambodian Rocks (Parallel World CD-6)|last=Nutt|first=Kevin|url=http://wlt4.home.mindspring.com/adventures/reviews/cambodian.htm|accessdate=5 March 2016}}</ref>
<ref name=smh>{{cite web|work=Sydney Morning Herald|last=Dow|first=Steve|title=Golden era of Cambodian music given its second airing|url=http://www.smh.com.au/entertainment/art-and-design/golden-era-of-cambodian-music-given-its-second-airing-20130912-2tnbf.html|date=13 September 2013}}</ref>
<ref name=chambersletson>{{cite journal|title="No, I Can’t Forget": Performance and Memoryin Dengue Fever’s Cambodian America|last=Chambers-Letson|first=Joshua|work=Journal of Popular Music Studies|volume=23|issue=3|pages=259–287}}</ref>
<ref name=sassoon>{{cite news|work=The Phnom Penh Post|title=Concert highlights legacy of Sinn Sisamouth|date=14 November 2015|last=Sassoon|first=Alessandro Larazzi}}</ref>
<ref name=sotheavy>{{cite web|work=Khmer Times|url=http://www.khmertimeskh.com/news/7138/sinn-sisamouth-song-copyrights-awarded-to-family/|title=Sinn Sisamouth Song Copyrights Awarded to Family|date=18 December 2014|last=Sotheavy|first=Nou}}</ref>
<ref name=seangly>{{cite web|work=The Phnom Penh Post|title=Sisamouth's songs protected|last=Seangly|first=Phak|url=http://www.phnompenhpost.com/national/sisamouth%E2%80%99s-songs-protected|date=30 April 2014}}</ref>
<ref name=allthingsconsidered>{{cite web|work=All Things Considered|publisher=NPR|title='Don't Think I've Forgotten': The Lost Story Of The Cambodian's Vibrant Music Scene|date=22 April 2015|url=https://www.highbeam.com/doc/1P1-234115397.html|via=HighBeam}}</ref>
<ref name=osullivan>{{cite web|work=Washington Post|via=HighBeam|date=29 May 2015|last=O'Sullivan|first=Michael|title=The Music Died, as a Country Lost So Much More|url=https://www.highbeam.com/doc/1P2-38344360.html}}</ref>
<ref name=ocweekly>{{cite web|work=OC Weekly|title=Resurrecting the Memory of Sinn Sisamouth, the Cambodian Elvis|date=29 August 2014|last=Bennett|first=Sarah|url=http://www.ocweekly.com/music/resurrecting-the-memory-of-sinn-sisamouth-the-cambodian-elvis-6595781}}</ref>
<ref name=jackson>{{cite web|work=Phnom Penh Post|last=Jackson|first=Will|title=Elvis of the Kingdom gets new star role|url=http://www.phnompenhpost.com/7days/elvis-kingdom-gets-new-star-role|date=9 May 2014}}</ref>
<ref name=newsday>{{cite web|work=Newsday|date=5 May 2015|last=Guzman|first=Rafer|title='Don't Think I've Forgotten' director John Pirozzi talks Cambodian rock and roll film|url=http://www.newsday.com/entertainment/movies/don-t-think-i-ve-forgotten-director-john-pirozzi-talks-cambodian-rock-and-roll-film-1.10381008}}</ref>
<ref name=vice>{{cite web|work=Vice|last=Woolfson|first=Daniel|date=19 September 2014|title=Cambodian Surf Rockers Were Awesome, but the Khmer Rouge Killed Them|url=http://www.vice.com/read/the-tragic-bloody-history-of-cambodian-surf-rock-930}}</ref>
}}

==External links==
*{{discogs release|3313592}}
*{{MusicBrainz release|mbid=c3c2fb19-b1ac-4c83-9133-ee83073f6ad0|name=Cambodian Rocks}}

[[Category:1996 compilation albums]]
[[Category:Cambodian music]]
[[Category:Garage rock compilation albums]]
[[Category:Psychedelic music compilation albums]]
[[Category:Rock compilation albums]]
[[Category:World music compilation albums]]
[[Category:Bootleg recordings]]
[[Category:Anonymous works]]
[[Category:Outsider music albums]]