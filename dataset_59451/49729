{{About|the novel}}
{{EngvarB|date=September 2013}}
{{Infobox book
| name          = Lorna Doone
| image         = Lorna Doone - cover - Project Gutenberg eText 17460.jpg
| image_size    = 
| alt           = Cover of an illustrated 1893 edition of ''Lorna Doone''
| caption       = 1893 edition
| author        = [[R. D. Blackmore]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| genre         = Fiction
| published     = 1869
| publisher     = Sampson Low, Son, & Marston
| media_type    = Print
| pages         =
| isbn          =
}}

'''''Lorna Doone: A Romance of Exmoor''''' is a novel by English author [[Richard Doddridge Blackmore]], published in 1869. It is a romance based on a group of historical characters and set in the late 17th century in [[Devon]] and [[Somerset]], particularly around the [[East Lyn Valley]] area of [[Exmoor]]. In 2003, the novel was listed on the BBC's survey [[The Big Read]].<ref>[http://www.bbc.co.uk/arts/bigread/top200_2.shtml "BBC – The Big Read"]. BBC. April 2003, Retrieved 1 December 2012</ref>

==Introduction==
Blackmore experienced difficulty in finding a publisher, and the novel was first published anonymously in 1869, in a limited three-volume edition of just 500 copies, of which only 300 sold. The following year it was republished in an inexpensive one-volume edition and became a huge critical and financial success. It has never been out of print.

It received acclaim from Blackmore's contemporary, [[Margaret Oliphant]], and as well from later Victorian writers including [[Robert Louis Stevenson]], [[Gerard Manley Hopkins]], and [[Thomas Hardy]]. A favourite among female readers,<ref>''Lorna Doone'' intr. Sally Shuttleworth.(The World's Classics). London: Oxford U. P., 1989</ref> it is also popular among males, and was chosen by male students at Yale in 1906 as their favourite novel.<ref>Dunn, W. H. (1956) ''R. D. Blackmore: The Author of Lorna Doone''. London: Robert Hale; p. 142</ref>

By his own account, Blackmore relied on a "phonologic" style for his characters' speech, emphasising their accents and word formation.<ref>Buckler, William E. (1956) "Blackmore's Novels before ''Lorna Doone''" in: ''Nineteenth-Century Fiction'', vol. 10 (1956), p. 183</ref> He expended great effort, in all of his novels, on his characters' dialogues and dialects, striving to recount realistically not only the ways, but also the tones and accents, in which thoughts and utterances were formed by the various sorts of people who lived in the [[Exmoor|Exmoor district]] in the 17th century.

Blackmore incorporated real events and places into the novel. [[Great Frost of 1683–84|The Great Winter]] described in chapters 41–45 was a real event.<ref>[http://www.exmoor-nationalpark.gov.uk/print/index/learning_about/general_information/geography/climate/snow/blizzards.htm Exmoor National Park]</ref>
He himself attended [[Blundell's School]] in Tiverton which serves as the setting for the opening chapters. One of the inspirations behind the plot is said to be the shooting of a young woman at a church in [[Chagford]], Devon, in the 17th century. Unlike the heroine of the novel, she did not survive, but is commemorated in the church. Apparently, Blackmore invented the name "Lorna", possibly drawing on a Scottish source.<ref>[http://www.yourdictionary.com/lorna Webster's New World College Dictionary]</ref>

According to the preface, the work is a romance and not a historical novel, because the author neither "dares, nor desires, to claim for it the dignity or cumber it with the difficulty of an historical novel." As such, it combines elements of traditional romance, of [[Sir Walter Scott]]'s historical novel tradition, of the pastoral tradition, of traditional Victorian values, and of the contemporary [[sensation novel]] trend. The basis for Blackmore's historical understanding is [[Thomas Babington Macaulay, 1st Baron Macaulay|Macaulay]]'s ''History of England'' and its analysis of the [[Monmouth rebellion]]. Along with the historical aspects are folk traditions, such as the many legends based around both the Doones and Tom Faggus. The composer [[Giacomo Puccini|Puccini]] once considered using the story as the plot for an opera, but abandoned the idea.

==Plot summary==
[[Image:Badgworthy Water Malmsmead.jpg|thumb|left|300px|Badgworthy water, Malmsmead]]
[[File:Lorna Doone - Jan Ridd learns to fire a gun - Project Gutenberg eText 17460.jpg|thumb|Jan Ridd learns to fire his father's gun – from an 1893 illustrated edition]]
The book is set in the 17th century in the [[Badgworthy Water]] region of [[Exmoor]] in Devon and [[Somerset]], England. John (in [[West Country dialect]], pronounced "Jan") Ridd is the son of a respectable farmer who was murdered in cold blood by one of the notorious Doone clan, a once noble family, now outlaws, in the isolated Doone Valley. Battling his desire for revenge, John also grows into a respectable farmer and takes good care of his mother and two sisters. He falls hopelessly in love with Lorna, a girl he meets by accident, who turns out to be not only (apparently) the granddaughter of Sir Ensor Doone (lord of the Doones), but destined to marry (against her will) the impetuous, menacing, and now jealous heir of the Doone Valley, Carver Doone. Carver will let nothing get in the way of his marriage to Lorna, which he plans to force upon her once Sir Ensor dies and he comes into his inheritance.

Sir Ensor dies, and Carver becomes lord of the Doones. John Ridd helps Lorna escape to his family's farm, Plover's Barrows. Since Lorna is a member of the hated Doone clan, feelings are mixed toward her in the Ridd household, but she is nonetheless defended against the enraged Carver's retaliatory attack on the farm. A member of the Ridd household notices Lorna's necklace, a jewel that she was told by Sir Ensor belonged to her mother. During a visit from the Counsellor, Carver's father and the wisest of the Doone family, the necklace is stolen from Plover's Barrows. Shortly after its disappearance, a family friend discovers Lorna's origins, learning that the necklace belonged to a Lady Dugal, who was robbed and murdered by a band of outlaws. Only her daughter survived the attack. It becomes apparent that Lorna, being evidently the long-lost girl in question, is in fact heiress to one of the largest fortunes in the country, and not a Doone after all (although the Doones are remotely related, being descended from a collateral branch of the Dugal family). She is required by law, but against her will, to return to London to become a ward in Chancery. Despite John and Lorna's love for one another, their marriage is out of the question.

[[King Charles II of England|King Charles II]] dies, and the [[James Scott, 1st Duke of Monmouth|Duke of Monmouth]] (the late king's illegitimate son) challenges Charles's brother [[James II of England|James]] for the throne. The Doones, abandoning their plan to marry Lorna to Carver and claim her wealth, side with Monmouth in the hope of reclaiming their ancestral lands. However, Monmouth is defeated at the [[Battle of Sedgemoor]], and his associates are sought for [[treason]]. John Ridd is captured during the revolution. Innocent of all charges, he is taken to London by an old friend to clear his name. There, he is reunited with Lorna (now Lorna Dugal), whose love for him has not diminished. When he thwarts an attack on Lorna's great-uncle and legal guardian Earl Brandir, John is granted a pardon, a title, and a coat of arms by the king and returns a free man to Exmoor.

In the meantime, the surrounding communities have grown tired of the Doones and their depredations. Knowing the Doones better than any other man, John leads the attack on their land. All the Doone men are killed, except the Counsellor (from whom John retrieves the stolen necklace) and his son Carver, who escapes, vowing revenge. When Earl Brandir dies and [[Judge Jeffreys]], as [[Lord Chancellor]], becomes Lorna's guardian, she is granted her freedom to return to Exmoor and marry John. During their wedding, Carver bursts into the church at Oare. He shoots Lorna and flees. Distraught and filled with blinding rage, John pursues and confronts him. A struggle ensues in which Carver is left sinking in a [[bog|mire]]. Exhausted and bloodied from the fight, John can only pant as he watches Carver slip away. He returns to discover that Lorna is not dead, and after a period of anxious uncertainty, she survives to live happily ever after.

==Chronological key==
The narrator, John Ridd, says he was born on 29 November 1661; in Chapter 24, he mentions [[Anne, Queen of Great Britain|Queen Anne]] as the current monarch, so the time of narration is 1702–1714 making him 40–52 years old. Although he celebrates New Year's Day on 1 January, at that time in England the year in terms of A.D. "begins" [[New Year#Historical Dates for the new year|Annunciation Style]] on 25 March, so 14 February 1676 would still be 1675 according to the old reckoning. Most of the dates below are given explicitly in the book.

{| border="5" cellspacing="10" style="line-height: 1em;"
|-
| width="50%" |
{|
|-
|align="center"|''Chapters 1–10''
|-
|Elements of Education
|-
|An Important Item (29 Nov 73, 12th birthday)
|-
|The War-path of the Doones
|-
|A Rash Visit
|-
|An Illegal Settlement
|-
|Necessary Practice (Dec 73)
|-
|Hard it is to Climb (29 Nov 75, 14 Feb 76)
|-
|A Boy and a Girl
|-
|There is no Place like Home
|-
|A Brave Rescue and a Rough Ride (Nov 76)
|-
|align="center"|''Chapters 11–20''
|-
|Tom Deserves his Supper (Nov 76)
|-
|A Man Justly Popular (Nov 76, Feb 77, Dec 82)
|-
|Master Huckaback Comes In (31 Dec 82)
|-
|A Motion which Ends in a Mull (1 Jan 83)
|-
|Quo Warranto? (Jan 83)
|-
|Lorna Growing Formidable (14 Feb 83)
|-
|John is Bewitched
|-
|Witchery Leads to Witchcraft (Mar)
|-
|Another Dangerous Interview
|-
|Lorna Begins her Story
|-
|align="center"|''Chapters 21–30''
|-
|Lorna Ends her Story
|-
|A Long Spring Month (Mar, Apr)
|-
|A Royal Invitation
|-
|A Safe Pass for King's Messenger
|-
|A Great Man Attends to Business
|-
|John is Drained and Cast Aside
|-
|Home Again at Last (Aug 83?)
|-
|John has Hope of Lorna
|-
|Reaping Leads to Revelling
|-
|Annie Gets the Best of it
|-
|align="center"|''Chapters 31–40''
|-
|John Fry's Errand
|-
|The Feeding of the Pigs
|-
|An Early Morning Call (Oct 83)
|-
|Two Negatives Make an Affirmative
|-
|Ruth is not like Lorna
|-
|John Returns to Business (Nov)
|-
|A Very Desperate Venture
|-
|A Good Turn for Jeremy
|-
|A Troubled State and a Foolish Joke
|-
|Two Fools Together
|}
| width="50%" |
{|
|-
|align="center"|''Chapters 41–50''
|-
|Cold Comfort
|-
|The Great Winter (Dec 83)
|-
|Not Too Soon
|-
|Brought Home at Last
|-
|Change Long Needed (15 Dec 83 – 7 Mar 84)
|-
|Squire Faggus Makes some Lucky Hits
|-
|Jeremy in Danger
|-
|Every Man Must Defend Himself
|-
|Maiden Sentinels are Best
|-
|A Merry Meeting a Sad One
|-
|align="center"|''Chapters 51–60''
|-
|A Visit from the Counsellor
|-
|The Way to Make the Cream Rise
|-
|Jeremy Finds out Something
|-
|Mutual Discomfiture
|-
|Getting into Chancery
|-
|John Becomes too Popular
|-
|Lorna knows her Nurse
|-
|Master Huckaback's Secret
|-
|Lorna Gone Away
|-
|Annie Luckier than John (autumn 84)
|-
|align="center"|''Chapters 61–70''
|-
|Therefore he Seeks Comfort (autumn-winter 84)
|-
|The King Must not be Prayed for (8 Feb 13 Jun, Jul 85)
|-
|John is Worsted by the Women (Jul 85)
|-
|Slaughter in the Marshes ([[Battle of Sedgemoor|Sedgemoor]], 6 Jul 85)
|-
|Falling Among Lambs
|-
|Suitable Devotion
|-
|Lorna Still is Lorna
|-
|John is John no Longer
|-
|Not to be Put up with
|-
|Compelled to Volunteer
|-
|align="center"|''Chapters 71–75''
|-
|A Long Account settled
|-
|The Counsellor, and the Carver
|-
|How to Get Out of Chancery
|-
|Blood Upon the Altar [in some editions, At the Altar] (Whittuesday 86)
|-
|Give Away the Grandeur [in some editions, Given Back]
|-
|&nbsp;
|-
|&nbsp;
|-
|&nbsp;
|-
|&nbsp;
|-
|&nbsp;
|}
|}

==Other versions and cultural references==
*The novel has inspired at least ten movies and broadcast mini-series; see [[:Category:Films based on Lorna Doone]]
*Lorna Doone is also a [[Lorna Doone (cookie)|shortbread cookie]] made by [[Nabisco]].<ref>[http://ask.yahoo.com/20020425.html Lorna Doone Cookie]</ref>
*Lorna Kennedy was named after Lorna Doone.
*''Lorna Doone'' was said to be the favourite book of Australian bushranger and outlaw [[Ned Kelly]], who may have thought of the idea of his armour by reading of the outlaw Doones "with iron plates on breast and head."<ref>Jones, Ian ''Ned Kelly: a Short Life'', p. 212</ref>
*The phrase "Lorna Doone" is used in [[Cockney rhyming slang]] for spoon.<ref>[http://www.probertencyclopaedia.com/browse/ZL.HTM Slang (L)]</ref>
*Lorna Doone is a character portrayed by [[Christine McIntyre]] in [[The Three Stooges]] shorts ''[[The Hot Scots]]'' and ''[[Scotched in Scotland]]''. She schemed with Angus and MacPherson to steal the treasures of the Earl of Glenheather Castle, trick the Stooges and frighten them away. The constables later arrested her. 
*The book inspired the song "Pangs of Lorna" by [[Kraus (musician)|Kraus]].<ref>{{cite journal|last=Dass|first=Kiran|title=A Journey Through the First Dimension with Kraus by Kraus review|journal=New Zealand Listener|date=28 January 2012|issue=3742|url=http://www.listener.co.nz/culture/music/a-journey-through-the-first-dimension-with-kraus-by-kraus-review/|accessdate=29 June 2013}}</ref>
*There is reference to R.D. Blackmore's Lorna Doone in John Galsworthy's play Justice (1910)
*The musical "Loving Lorna Doone", with book and lyrics by Aaron Rocklyn and Music by Jaymi Wilson and Anthony Knutson, is based on the book
*The name Lorna Doone is mentioned in Carole Bayer Sager's 1977 international hit, "You're Moving Out Today".
*The character of Lorna from the animated mini-series [[Over the Garden Wall (miniseries)]] was inspired by Lorna Doone
*Lorna Doone II was the name of a yawl type of sailboat on episode 7 (Risk to Death) of Baywatch Hawaii.
*British Professional wrestler Jack Baltus was billed as Carver Doone.
*In the ''[[Dudley Do-Right]]'' cartoon, "Elevenworth Prison", Snidely Whiplash refers to Dudley Do Right as "Lorna Doone" and Do Right calls himself "Lorna Doone" also.

==References==
{{reflist|30em}}

==Further reading==
*Blackmore, R. D. (1908) ''Lorna Doone: a romance of Exmoor''; Doone-land edition; with introduction and notes by H. Snowden Ward and illustrations by Mrs. Catharine Weed Ward. lii, 553 pp., plates. London: Sampson Low, Marston and Company (includes "Slain by the Doones", pp.&nbsp;529–53)
*Delderfield, Eric (1965?) ''The Exmoor Country: [a] brief guide & gazetteer; 6th ed. Exmouth: The Raleigh Press
*Elliott-Cannon, A. (1969) ''The Lorna Doone Story''. Minehead: The Cider Press

==External links==
{{Wikisource}}
{{Commons category|Lorna Doone}}
{{Gutenberg|no=840|name=Lorna Doone: A Romance of Exmoor}}
{{Gutenberg|no=17460|name=Lorna Doone: A Romance of Exmoor}} – a lavishly illustrated edition (Burrows Brothers Company, 1889)
*[http://www.imdb.com/find?ref_=nv_sr_fn&q=lorna+Doone&s=all IMDb Listing of movies based on Lorna Doone]
*[http://www.litfix.com/blackmore/lorna/index.html HTML online text of ''Lorna Doone'']
*[http://www.silversirens.co.uk/ml/lornadoone.php ''Lorna Doone'' at Silver Sirens]
* {{librivox book | title=Lorna Doone | author=Richard Doddridge BLACKMORE}}

{{R. D. Blackmore}}

{{Use dmy dates|date=September 2013}}

[[Category:1869 novels]]
[[Category:Novels by Richard Doddridge Blackmore]]
[[Category:Exmoor]]
[[Category:Novels adapted into comics]]
[[Category:British novels adapted into films]]
[[Category:Novels adapted into television programs]]
[[Category:19th-century British novels]]
[[Category:Novels set in Devon]]
[[Category:Novels set in Somerset]]
[[Category:Novels set in Early Modern England]]