{{distinguish|Andrew Albers}}
{{good article}}
{{Infobox ice hockey player
| image = Andrew Alberts Canucks.jpg
| image_size = 230px
| caption = Alberts with the Canucks in October 2013
| position = [[Defenceman|Defense]]
| shoots = Left
| height_ft = 6
| height_in = 5
| weight_lb = 218
| ntl_team = United States
| birth_date = {{Birth date and age|1981|6|30|mf=y}}
| birth_place = [[Minneapolis]], [[Minnesota|MN]], [[United States|USA]]
| career_start = 2005
| career_end = 2014
| draft = 179th overall
| draft_year = 2001
| draft_team = [[Boston Bruins]]
| played_for = [[Boston Bruins]]<br>[[Philadelphia Flyers]]<br>[[Carolina Hurricanes]]<br>[[Vancouver Canucks]]
}}
'''Andrew James Alberts''' (born June 30, 1981) is an [[United States|American]] professional [[ice hockey]] [[Defenceman|defenseman]] who is currently an [[Unrestricted Free Agent]], having last played for the [[Vancouver Canucks]] of the [[National Hockey League]] (NHL). He additionally played in the NHL with the [[Boston Bruins]], [[Philadelphia Flyers]] and [[Carolina Hurricanes]]. A [[Stay-at-home defenceman|stay-at-home defenseman]], he was known for playing a physical style of game.

After a two-year [[junior ice hockey|junior]] career in the [[United States Hockey League]] (USHL), he was selected by the Bruins 179th overall in the [[2001 NHL Entry Draft]]. Following the draft, he joined the [[college hockey|college ranks]] with the [[Boston College Eagles men's ice hockey|Boston College Eagles]] of the [[Hockey East]] conference. In four seasons with the Eagles, Alberts was named Hockey East's Best Defensive Defenseman and was a two-time [[NCAA]] All-American. Joining the Bruins in [[2005–06 NHL season|2005]], he played three seasons with the club before being traded to the Flyers. He played with the Flyers for one year, then with the Hurricanes in 2009. At the trade deadline the following year, he was dealt to the Canucks. Internationally, Alberts has competed for the [[United States men's national ice hockey team|American national team]] at the [[2006 Men's World Ice Hockey Championships|2006]] and [[2007 Men's World Ice Hockey Championships|2007 World Championships]].

==Playing career==

===Amateur===
Alberts played high school hockey for [[Benilde-St. Margaret's]] from 1997 to 1999. In his graduating year, he earned All-Conference honors while leading the Red Knights to a State Class A title.<ref name=bostoncollege/> He played the next two seasons at the [[junior ice hockey|junior level]] in the [[United States Hockey League]] (USHL) with the [[Waterloo Blackhawks]].  Recording 4 points over 49 games in his rookie campaign, he was named the team's Most Improved Player.<ref name=bostoncollege/> In 2000–01, he served as an [[alternate captain (ice hockey)|alternate captain]] while raising his points total to 14 over 54 games.<ref name=bostoncollege/> That summer, Alberts was drafted by the [[Boston Bruins]] in the sixth round (179th overall) of the [[2001 NHL Entry Draft]].<ref name=nhl>{{cite web|title=Andrew Alberts player profile|url=http://www.nhl.com/ice/player.htm?id=8469626|accessdate=2011-03-14|publisher=National Hockey League}}</ref>

Following the draft, Alberts began playing [[college hockey]] for [[Boston College Eagles men's ice hockey|Boston College Eagles]] of the [[Hockey East]] conference in 2001. His first college goal came in the first round of the 2002 [[Beanpot (ice hockey)|Beanpot]],{{#tag:ref|The Beanpot is an annual tournament between the four major colleges in the Boston area. It does not count toward conference standings or standings.|group=notes}} a [[short handed]] marker against the [[Boston University Terriers men's ice hockey|Boston University Terriers]].<ref name=bostoncollege/> He scored 12 points his freshman year before improving to 22 points in 2002–03. In his third college year, he recorded 16 points was a co-recipient of Hockey East's Best Defensive Defenseman award with [[Prestin Ryan]] of the [[Maine Black Bears men's ice hockey|Maine Black Bears]].<ref>{{cite web|title=Hockey East Awards |url=http://www.hockeyeastonline.com/men/hea/awards.php |accessdate=2011-01-18 |publisher=[[Hockey East]] |deadurl=yes |archiveurl=https://web.archive.org/web/20120413200503/http://www.hockeyeastonline.com:80/men/hea/awards.php |archivedate=2012-04-13 |df= }}</ref> He was also named to the Hockey East Second All-Star and NCAA East First All-American Teams.<ref name=tsn/>

Alberts did not miss a game during his college career until suffering two knee injuries during his senior year in 2004–05.<ref name=bostoncollege/><ref>{{Cite news|title=BC avenges earlier loss, gets quality win|url=http://sports.espn.go.com/ncaa/news/story?id=1991652|date=2005-02-15|accessdate=2011-01-18|publisher=[[ESPN]]}}</ref> Limited to 30 games, he again recorded 16 points and was named to the Hockey East First All-Star Team. In the playoffs, he ended what was the longest semifinal game in Hockey East history with a double-overtime goal against the Maine Black Bears.<ref name=semifinal/>{{#tag:ref|The game ended at 89 minutes and 9 seconds, surpassing the previous record-setting match between [[Boston College Eagles men's ice hockey|Boston College]] and [[Boston University Terriers men's ice hockey|Boston University]], which lasted 85 minutes.<ref name=semifinal>{{cite news|title=Alberts, BC gain final in second OT|url=http://www.boston.com/sports/colleges/mens_hockey/articles/2005/03/19/schneider_bc_stop_maine_in_double_ot/|accessdate=2011-01-18|date=2005-03-19|publisher=''[[The Boston Globe]]''|author=Nancy Marrapese-Burrell}}</ref>|group=notes}} Alberts earned Hockey East All-Tournament honors, as the Eagles defeated the [[New Hampshire Wildcats men's ice hockey|New Hampshire Wildcats]] 3–1 in the final to win the [[Lamoriello Trophy]] as conference champions.<ref>{{cite news|title=Boyle and Schneider Take Weekly Accolades for Tournament Performances|url=http://www.cstv.com/sports/m-hockey/stories/032205aaa.html|accessdate=2011-01-18|date=2005-03-21|publisher=CBS College Sports}}</ref> He also earned his second consecutive NCAA East All-American recognition.<ref name=tsn/>

[[Image:Andrew Alberts 2008-04-02.jpg|thumb|left|240px|Alberts with the Bruins in April 2008]]

===Professional===
Following his senior year with the Eagles, Alberts signed an amateur tryout contract with the Boston Bruins' [[American Hockey League]] (AHL) affiliate, the [[Providence Bruins]], on April 1, 2005.<ref>{{cite news|title=Providence Bruins sign Andrew Alberts|url=http://www.oursportscentral.com/services/releases/?id=3141262|accessdate=2011-01-18|date=2005-04-01|publisher=OurSports Central}}</ref> He appeared in the final eight games of the [[2004–05 AHL season|2004–05 AHL regular season]] before helping the club to the Conference Finals of the playoffs. He scored his first professional goal in Game 5 of the Conference Finals against goaltender [[Antero Niittymaki]] in a 6–4 win against the [[Philadelphia Phantoms]].<ref>{{cite news|title=Boyes, Bruins force Game 6|url=http://www.oursportscentral.com/services/releases/?id=3168404|date=2005-05-27|accessdate=2011-01-18|publisher=OurSports Central}}</ref>

After signing a one-year NHL contract with Boston in August 2005,<ref name=tsn>{{cite web|title=Andrew Alberts|url=http://tsn.ca/nhl/teams/players/bio/?id=2507|accessdate=2011-01-17|publisher=[[The Sports Network]]}}</ref> he appeared in his first NHL training camp in September 2005. Making the Bruins' roster for the [[2005–06 NHL season|2005–06 season]], he made his NHL debut on October 5, 2005 in a game versus the [[Montreal Canadiens]].<ref name=canucksbio>{{cite web|title=Andrew Alberts: Bio|url=http://canucks.nhl.com/club/player.htm?id=8469626&view=bio|accessdate=2011-01-18|publisher=[[Vancouver Canucks]]}}</ref> A month later, he notched his first NHL point, an assist, in a game against the [[Buffalo Sabres]] on November 19.<ref name=canucksbio/> In December 2005, Alberts received a brief 10-day assignment to Providence, notching an assist over 6 games during that span.<ref name=tsn/> Returning to the Boston lineup, he scored his first NHL goal late in the campaign during a game against the [[Buffalo Sabres]] on March 12, 2006.<ref name=canucksbio/> Playing in 73 games, Alberts scored a goal and six assists.<ref name=nhl/> During his rookie season, Bruins head coach [[Mike Sullivan (ice hockey b. 1968)|Mike Sullivan]] experimented with playing Alberts at the [[forward (ice hockey)|forward]] position for roughly a month.<ref>{{cite news|title=Bill Sweatt living the lifelong dream |url=http://www.vancouversun.com/sports/Bill+Sweatt+living+lifelong+dream/5833505/story.html |accessdate=2011-12-11 |date=2011-12-08 |work=[[The Vancouver Sun]] |author=Ziemer, Brad }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

After re-signing for another year in July 2006, he recorded 10 assists over 76 games the [[2006–07 NHL season|following season]]. He earned his first multi-year contract in the off-season, re-signing with the Bruins.<ref>{{cite news|title=Andrew Alberts extended by Bruins|url=http://www.cbc.ca/sports/hockey/story/2007/06/12/nhl-bruins-alberts.html|accessdate=2011-01-17|date=2007-06-12|publisher=[[Canadian Broadcasting Corporation]]}}</ref> During the [[2007–08 NHL season|2007–08 season]], he was limited to 35 games due to a head injury suffered on November 26, 2007, during a game against the [[Philadelphia Flyers]]. Going down to his knees to block a puck moving into the defensive zone, opposing forward [[Scott Hartnell]] bodychecked him, using his elbow to hit Alberts' head against the boards.<ref name=hartnell/> Alberts left the game injured, while Hartnell received a five-minute major penalty and a game misconduct; he was later suspended an additional two games by the league.<ref name=hartnell>{{cite news|title=Bruins' Alberts mum on Hartnell suspension|url=http://www.boston.com/sports/hockey/bruins/articles/2007/11/28/hartnell_suspended_two_games/|accessdate=2011-01-17|date=2007-11-28|publisher=''[[Boston Globe]]''|author=Kevin Paul Dumont}}</ref> Alberts recovered in time to make his NHL playoff debut in April 2008, as the Bruins were eliminated in the first round by the Montreal Canadiens.<ref>{{cite news | url = http://www.usatoday.com/sports/hockey/2008-04-13-115267208_x.htm| title = Kovalev wins it in overtime for Montreal| publisher = ''[[USA Today]]'' | date = 2008-04-13 | accessdate = 2011-03-14}}</ref><ref>{{cite web|url=http://www.hockey-reference.com/playoffs/NHL_2008.html|title=2008 NHL Playoffs Summary|accessdate=2011-03-14|publisher=Hockey Reference.com}}</ref>

[[Image:Andrew Alberts Flyers.jpg|thumb|right|Alberts with the Flyers in March 2009]]
After being a healthy scratch for the Bruins' first two games in the [[2008–09 NHL season|2008–09 season]], Alberts was traded to Philadelphia for [[Ned Lukacevic]] and a conditional [[2009 NHL Entry Draft|2009 draft pick]] on October 13, 2008.<ref>{{cite news|url=http://flyers.nhl.com/team/app?articleid=386366&page=NewsPage&service=page |title=News: Flyers Acquire Andrew Alberts |publisher=[[Philadelphia Flyers]] |date=2008-10-13 |accessdate=2009-04-24 |deadurl=yes |archiveurl=https://web.archive.org/web/20081017051841/http://flyers.nhl.com/team/app/?service=page&page=NewsPage&articleid=386366 |archivedate=October 17, 2008 }}</ref> The emergence of younger defenseman [[Matt Hunwick]] was partly responsible for his expandability.<ref>{{cite news|title=Flyers trade for Andrew Alberts|url=http://www.cbc.ca/sports/hockey/story/2008/10/13/andrew-alberts.html|accessdate=2011-01-17|date=2008-10-13|publisher=[[Canadian Broadcasting Corporation]]}}</ref> Alberts became an integral part of the Flyers' defensive corps, leading the team in hits (157) and ranking third in blocked shots (133).<ref name=contract09>{{cite news|title=Hurricanes agree to terms with Alberts|url=http://www.nhl.com/ice/news.htm?id=442786|accessdate=2011-01-17|date=2009-07-15|publisher=[[National Hockey League]]}}</ref> His 12 assists and 13 points were career-highs.<ref name=nhl/>

Becoming an [[unrestricted free agent]] in the off-season, Alberts signed a two-year deal with the [[Carolina Hurricanes]] on July 16, 2009. The contract paid him $800,000 the first year and $1.3 million the second.<ref name=contract09/> After appearing in 62 games for Carolina, he was dealt at the [[2009–10 NHL season|2009–10]] trade deadline on March 3, 2010, to the [[Vancouver Canucks]] in exchange for a third round pick in the [[2010 NHL Entry Draft|2010 draft]] ([[Austin Levi]]). Between Carolina and Vancouver, Alberts finished the regular season with 3 goals and 12 points over 76 games. His defensive play struggled in his initial stint with Vancouver, often being made a healthy scratch.<ref name=remaining/> He continued to earn criticism from Vancouver fans and media in the playoffs, particularly for his lack of speed and for taking costly penalties.<ref name=remaining>{{cite news|title=Remaining Canucks blueliners Bieksa, O'Brien and...yes...Alberts elevate their game|url=http://www.faceoff.com/hockey/teams/vancouver-canucks/Remaining+Canuck+blueliners+Bieksa+Brien+Alberts+elevate+their+game/3010774/story.html|accessdate=2011-01-17|date=2010-05-11|publisher=''[[The Vancouver Sun]]''|author=Iain MacIntyre}} {{Dead link|date=April 2012|bot=H3llBot}}</ref><ref>{{Cite news|title=Penalties turn blueliner Andrew Alberts into Mr. Unpopularity |url=http://www.vancouversun.com/sports/Penalties+turn+blue+liner+Andrew+Alberts+into+Unpopularity/2917649/story.html |accessdate=2011-01-17 |date=2010-04-17 |publisher=''[[The Vancouver Sun]]'' |author=Brad Zeimer |deadurl=yes |archiveurl=https://web.archive.org/web/20100523104606/http://www.vancouversun.com/sports/Penalties+turn+blue+liner+Andrew+Alberts+into+Unpopularity/2917649/story.html |archivedate=May 23, 2010 }}</ref>

Alberts came back to the Canucks with improved play during the 2010 pre-season and beat out [[Shane O'Brien (ice hockey)|Shane O'Brien]] for the team's final spot on defense (O'Brien was subsequently traded prior to the start of the season).<ref>{{cite news|title=Salary cap crunch will force Vancouver to make withdrawal from defence corps |url=http://www.vancouversun.com/story_print.html?id=3607469&sponsor= |accessdate=2011-03-22 |date=2010-10-01 |work=[[The Vancouver Sun]] |location=Vancouver |publisher=Postmedia Network |author=Brad Zeimer }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{cite news|title=Predators trade two; get Canucks' O'Brien|url=http://www.sportsnet.ca/hockey/2010/10/05/predators_trade_two_get/|accessdate=2011-03-22|date=2010-10-05|publisher=[[Rogers Sportsnet]]}}</ref> Nearly a month into the season, Alberts suffered a minor knee injury during a game against the [[Colorado Avalanche]],<ref>{{cite news|title=Canucks infirmary: Andrew Alberts' knee could add to injury woes |url=http://www.vancouversun.com/sports/hockey/vancouver-canucks/Canucks+infirmary+Andrew+Alberts+knee+could+injury+woes/3736039/story.html |accessdate=2011-03-22 |date=2010-10-27 |work=[[The Vancouver Sun]] |publisher=Postmedia Network |location=Vancouver |author=Iain MacIntyre |deadurl=yes |archiveurl=https://web.archive.org/web/20110110091523/http://www.vancouversun.com/sports/hockey/vancouver-canucks/Canucks+infirmary+Andrew+Alberts+knee+could+injury+woes/3736039/story.html |archivedate=January 10, 2011 }}</ref> but did not miss any games.<ref name=tsn/> Later in the season, he was sucker-punched in the face by [[enforcer (ice hockey)|enforcer]] [[Jody Shelley]] during a game against the Flyers in December 2010. The two were being restrained by referees during a scrum when Shelley struck him. As a result, he received a two-game suspension from the league and forfeited  $26,829.27 in salary. Though Alberts left the game, he was not injured on the play.<ref name=shelley>{{cite news|title=Alberts OK after sucker-punch in Flyers game|url=http://www.canada.com/story_print.html?id=4039145&sponsor=hp-storytoolbox|accessdate=2011-03-22|date=2010-12-30|work=[[The Province]]|author=Jim Jamieson|publisher=Postmedia Network|location=Vancouver}} {{Dead link|date=April 2012|bot=H3llBot}}</ref> The following month, Alberts suffered a right shoulder injury during a game against the [[Minnesota Wild]] on January 16, 2011.<ref>{{cite news|title=Reeling Wild rebound on Anton Khubodin's first career shutout|url=http://sports.espn.go.com/nhl/recap?gameId=310116030|accessdate=2011-03-22|publisher=[[ESPN]]|date=2011-01-16}}</ref> Shortly after recovering and returning to the lineup, he suffered another injury, breaking his wrist while blocking a shot during a game against the [[St. Louis Blues]] on February 14, 2011.<ref>{{cite news|title=Canucks lose another d-man as Alberts breaks wrist|url=http://www.nhl.com/ice/news.htm?id=552794|accessdate=2011-03-22|date=2011-02-15|publisher=[[National Hockey League]]}}</ref> He missed the remainder of the regular season,<ref name=tsn/> finishing with a goal and seven assists, while leading Canucks defensemen with 113 hits, over 42 games. Recovering in time for the playoffs, Alberts appeared in nine post-season games (he did not register any points) during the Canucks' run to the [[2011 Stanley Cup Finals]], where they were defeated in seven games by the Boston Bruins. During the off-season, Alberts was re-signed by the Canucks to a two-year, $2.45 million deal on June 29, 2011 (two days prior to his pending unrestricted free agency).<ref>{{cite news|title=Maxim Lapierre, Andrew Alberts re-sign|url=http://sports.espn.go.com/nhl/news/story?id=6717507|accessdate=2011-06-29|date=2011-06-29|publisher=[[ESPN]]|agency=Associated Press}}</ref>

During the 2013 off-season, the Canucks signed Alberts to a one-year extension.<ref>{{cite news|title=Canucks re-sign Chris Tanev|url=http://espn.go.com/nhl/story/_/id/9592546/vancouver-canucks-re-sign-chris-tanev-andrew-alberts|accessdate=2014-08-29|date=2013-08-22|publisher=[[ESPN]]|agency=Associated Press}}</ref> Alberts suffered a [[concussion]] during a December 29, 2013 game against the [[Calgary Flames]] due to a high hit delivered by [[Enforcer (ice hockey)|enforcer]] [[Brian McGrattan]] that effectively ended Alberts' professional hockey career.<ref>{{cite news|title=Canucks' Andrew Alberts hoping for 'one day without headaches'|url=http://www.vancouversun.com/sports/Canucks+Andrew+Alberts+hoping+without+headaches/9898138/story.html|accessdate=2014-08-29|date=2014-06-02|work=[[The Vancouver Sun]]|publisher=Postmedia Network|location=Vancouver|author=Iain MacIntyre}}</ref>

==International play==
Alberts first played internationally for the [[United States men's national ice hockey team|American national team]] at the [[2006 IIHF World Championship]] in [[Riga, Latvia]]. He scored his first international goal in the preliminary round, a 3–0 shutout against [[Denmark men's national ice hockey team|Denmark]].<ref>{{cite web|title=Game Summary|url=http://www.iihf.com/Hydra/Tournaments_06/output/WS/ihm046d10_74_1_0.pdf|accessdate=2011-01-17|date=2006-05-07|publisher=[[International Ice Hockey Federation]]}}</ref> Later in the tournament, the United States were shut out by [[Sweden men's national ice hockey team|Sweden]] in the quarterfinal and finished in seventh place.<ref>{{cite web|title=Tournament Progress|url=http://www.iihf.com/Hydra/Tournaments_06/output/WS/ihm046z1117_76_1_0.pdf|accessdate=2011-01-18|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref><ref>{{cite web|title=Final Rankings|url=http://www.iihf.com/Hydra/Tournaments_06/output/WS/ihm046z017_final_ranking_1_0.pdf|accessdate=2011-01-18|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> Alberts returned for the [[2007 IIHF World Championship]] in [[Moscow, Russia]]. He notched one assist as the United States finished in fifth place.<ref>{{cite web|title=Final Rankings|url=http://www.iihf.com/Hydra/Tournaments_07/output/WS/hydra.iihf.com/113/IHM113Z017_Final_Ranking_1_0.pdf|accessdate=2011-01-18|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref> They were eliminated in the quarterfinal against [[Finland men's national ice hockey team|Finland]], a 5–4 shootout loss.<ref>{{cite web|title=Tournament Progress|url=http://www.iihf.com/Hydra/Tournaments_07/output/WS/hydra.iihf.com/113/IHM113Z017_76B_2_0.pdf|accessdate=2011-01-18|publisher=[[International Ice Hockey Federation]]|format=PDF}}</ref>

==Personal life==
Alberts was born in [[Minneapolis, Minnesota]], to Mary and Dale Alberts.<ref name=bostoncollege>{{cite web|title=Andrew Alberts |url=http://bceagles.cstv.com/sports/m-hockey/mtt/alberts_andrew00.html |accessdate=2011-01-18 |publisher=[[Boston College]] |archivedate=2011-03-23 |archiveurl=http://www.webcitation.org/5xOC6ooRm?url=http%3A%2F%2Fbceagles.cstv.com%2Fsports%2Fm-hockey%2Fmtt%2Falberts_andrew00.html |deadurl=no |df= }}</ref> The third of four children, he has two older sisters and one younger brother.<ref name=bostoncollege/> Alberts attended [[Eden Prairie High School]] for his first two years of secondary before graduating from [[Benilde-St. Margaret's]] in June 1999.<ref name=bostoncollege/> While enrolled in [[Boston College]], Alberts earned a communications degree.<ref name=canucksbio/> Prior to his senior year, he was awarded the Morrissey Brothers Memorial Hockey Scholarship.<ref name=bostoncollege/>

==Career statistics==

===Regular season and playoffs===
<!-- AS PER WIKIPEDIA STANDARD PLEASE DO NOT ADD STATS TILL END OF SEASON -->
{| border="0" cellpadding="1" cellspacing="0" style="text-align:center; width:60em" 
|- bgcolor="#e0e0e0" 
! colspan="3" bgcolor="#ffffff" |
! rowspan="99" bgcolor="#ffffff" |
! colspan="5" | [[Regular season]] 
! rowspan="99" bgcolor="#ffffff" |
! colspan="5" | [[Playoffs]] 
|- bgcolor="#e0e0e0" 
! [[Season (sports)|Season]] 
! Team 
! League 
! GP 
! [[Goal (ice hockey)|G]] 
! [[Assist (ice hockey)|A]] 
! [[Point (ice hockey)|Pts]] 
! [[Penalty (ice hockey)|PIM]] 
! GP 
! G 
! A 
! Pts 
! PIM 
|- ALIGN="center"
| [[1999–00 USHL season|1999–00]]
| [[Waterloo Blackhawks]]
| [[United States Hockey League|USHL]]
| 49
| 2
| 2
| 4
| 55
| 4
| 0
| 0
| 0
| 12
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2000–01 USHL season|2000–01]]
| Waterloo Blackhawks
| USHL
| 54
| 4
| 10
| 14
| 128
| —
| —
| —
| —
| —
|- ALIGN="center"
| [[2001–02 NCAA Division I men's ice hockey season|2001–02]]
| [[Boston College Eagles]]
| [[Hockey East|HE]]
| 38
| 2
| 10
| 12
| 52
| —
| —
| —
| —
| —
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2002–03 NCAA Division I men's ice hockey season|2002–03]]
| Boston College Eagles
| HE
| 39
| 6
| 16
| 22
| 60
| —
| —
| —
| —
| —
|- ALIGN="center"
| [[2003–04 NCAA Division I men's ice hockey season|2003–04]]
| Boston College Eagles
| HE
| 42
| 4
| 12
| 16
| 64
| —
| —
| —
| —
| —
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2004–05 NCAA Division I men's ice hockey season|2004–05]]
| Boston College Eagles
| HE
| 30
| 4
| 12
| 16
| 67
| —
| —
| —
| —
| —
|- ALIGN="center"
| [[2004–05 AHL season|2004–05]]
| [[Providence Bruins]]
| [[American Hockey League|AHL]]
| 8
| 0
| 0
| 0
| 16
| 16
| 1
| 4
| 5
| 40
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2005–06 AHL season|2005–06]]
| Providence Bruins
| AHL
| 6
| 0
| 1
| 1
| 7
| —
| —
| —
| —
| —
|- ALIGN="center"
| [[2005–06 NHL season|2005–06]]
| [[Boston Bruins]]
| [[National Hockey League|NHL]]
| 73
| 1
| 6
| 7
| 68
| —
| —
| —
| —
| —
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2006–07 NHL season|2006–07]]
| Boston Bruins
| NHL
| 76
| 0
| 10
| 10
| 124
| —
| —
| —
| —
| —
|- ALIGN="center"
| [[2007–08 NHL season|2007–08]]
| Boston Bruins
| NHL
| 35
| 0
| 2
| 2
| 39
| 2
| 0
| 0
| 0
| 0
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2008–09 NHL season|2008–09]]
| [[Philadelphia Flyers]]
| NHL
| 79
| 1
| 12
| 13
| 61
| 6
| 0
| 1
| 1
| 10
|- ALIGN="center"
| [[2009–10 NHL season|2009–10]]
| [[Carolina Hurricanes]]
| NHL
| 62
| 2
| 8
| 10
| 74
| —
| —
| —
| —
| —
|- ALIGN="center" bgcolor="#f0f0f0"
| 2009–10
| [[Vancouver Canucks]]
| NHL
| 14
| 1
| 1
| 2
| 13
| 10
| 0
| 1
| 1
| 27
|-
| [[2010–11 NHL season|2010–11]]
| Vancouver Canucks
| NHL
| 42
| 1
| 6
| 7
| 41
| 9
| 0
| 0
| 0
| 6
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2011–12 NHL season|2011–12]]
| Vancouver Canucks
| NHL
| 44
| 2
| 1
| 3
| 40
| —
| —
| —
| —
| —
|- ALIGN="center" 
| [[2012–13 NHL season|2012–13]]
| Vancouver Canucks
| NHL
| 24
| 0
| 1
| 1
| 32
| 4
| 0
| 0
| 0
| 2
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2013–14 NHL season|2013–14]]
| Vancouver Canucks
| NHL
| 10
| 0
| 0
| 0
| 0
| —
| —
| —
| —
| —
|- bgcolor="#e0e0e0"
! colspan="3" | NHL totals
! 459
! 8
! 47
! 55
! 492
! 31
! 0
! 2
! 2
! 45
|}

===International===
{| border="0" cellpadding="1" cellspacing="0" ID="Table3" style="text-align:center; width:60em"
|- ALIGN="center" bgcolor="#e0e0e0"
! Year
! Team
! Event
! Result
! rowspan="99" bgcolor="#ffffff" | 
! GP
! G
! A
! Pts
! PIM
|- ALIGN="center" 
| [[2006 Men's World Ice Hockey Championships|2006]]
| [[American national men's hockey team|United States]]
| [[IIHF World Championships|WC]]
| 7th
| 7
| 1
| 0
| 1
| 14
|- ALIGN="center" bgcolor="#f0f0f0"
| [[2007 Men's World Ice Hockey Championships|2007]]
| United States
| WC
| 5th
| 7
| 0
| 1
| 1
| 14
|- ALIGN="center" bgcolor="#e0e0e0"
! colspan=4 | Senior totals
! 14
! 1
! 1
! 2
! 28
|}

==Awards and honors==
{| class="wikitable" border="1"
|-
! Award
! Year
!
|-
| All-[[Hockey East]] [[List of All-Hockey East Teams#Second Team|Second Team]]
| [[2003–04 NCAA Division I men's ice hockey season|2003–04]]
| 
|-
| Hockey East's Best Defensive Defenseman <br/><small>(co-recipient with [[Prestin Ryan]])</small> || 2004
| 
|-
| [[American Hockey Coaches Association|AHCA]] [[List of Division I AHCA All-American Teams|East First-Team All-American]]
| [[2003–04 NCAA Division I men's ice hockey season|2003–04]]
| 
|-
| All-[[Hockey East]] [[List of All-Hockey East Teams#First Team|First Team]]
| [[2004–05 NCAA Division I men's ice hockey season|2004–05]]
| 
|-
| [[American Hockey Coaches Association|AHCA]] [[List of Division I AHCA All-American Teams|East First-Team All-American]]
| [[2004–05 NCAA Division I men's ice hockey season|2004–05]]
| 
|-
| [[Hockey East]] [[List of Hockey East All-Tournament Teams|All-Tournament Team]]
| [[2005 Hockey East Men's Ice Hockey Tournament|2005]]
| <ref name= award>{{cite news|title=2013-14 Hockey East Media Guide|url=http://issuu.com/hockeyeast/docs/1314heamguide|publisher=Hockey East|accessdate=2014-05-19}}</ref>
|-
| [[Lamoriello Trophy]] <br/> <small>(Hockey East champions; with [[Boston College Eagles men's ice hockey|Boston College Eagles]])</small> || 2005
|-
| [[Hockey East]] [[List of Hockey East All-Tournament Teams|All-Tournament Team]]
| [[2005 Hockey East Men's Ice Hockey Tournament|2005]]
| <ref name="award"/>
|-
|}

==Notes==
{{Reflist|group=notes}}

==References==
{{Reflist|2}}

==External links==
{{Commons category|Andrew Alberts}}
{{Ice Hockey Stats |nhl=8469626 |elite= |espn= |euro= |hr=a/alberan01.html |hockeydb=56749 |legends=20207 |tsn= }}

{{s-start}}
{{s-ach}}
{{succession box|before=[[Cliff Loya]]|title=[[List of Hockey East Best Defensive Defenseman|Hockey East Best Defensive Defenseman]]<br/><small>(shared with [[Prestin Ryan]])</small>|years=[[2003–04 NCAA Division I men's ice hockey season|2003–04]]|after=[[Tim Judy]]}}
{{s-end}}

{{DEFAULTSORT:Alberts, Andrew}}
[[Category:1981 births]]
[[Category:American ice hockey defensemen]]
[[Category:Boston Bruins draft picks]]
[[Category:Boston Bruins players]]
[[Category:Boston College Eagles men's ice hockey players]]
[[Category:Carolina Hurricanes players]]
[[Category:Ice hockey people from Minnesota]]
[[Category:Living people]]
[[Category:People from Eden Prairie, Minnesota]]
[[Category:Philadelphia Flyers players]]
[[Category:Providence Bruins players]]
[[Category:Vancouver Canucks players]]
[[Category:Waterloo Black Hawks players]]
[[Category:Sportspeople from Minneapolis]]