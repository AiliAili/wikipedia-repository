{{Orphan|date=April 2014}}

{{Infobox comics creator
| name = Brian McCarty
| image =
| imagesize =
| caption =
| birth_name  =
| birth_date  = 1974
| birth_place   = [[Memphis, Tennessee]]
| living = yes
| death_date  =
| death_place =
| nationality = [[United States|American]]
| area = Photographer
| cartoonist = 
| write =
| ink =
| letter =
| color =
| edit =
| alias = 
| notable works =
| awards =
| website = http://www.brianmccarty.com
| sortkey = McCarty,Brian
| subcat = American
}}

'''Brian McCarty''' (1974) is a [[contemporary artist]] and [[photographer]] known for his work with toys.<ref>{{cite web|title=Toys: A Photographers Muse|url=http://abcnews.go.com/Video/playerIndex?id=2619294|publisher=ABC News Video|accessdate=27 March 2013|date=31 October 2006}}</ref> McCarty's approach is based upon integrating toy characters into real-life situations through the use of forced perspective in carefully crafted scenes.<ref>{{cite web|title=Brian McCarty Plays God|url=http://www.xlr8r.com/tv/101|publisher=XLR8R TV|accessdate=27 March 2013|date=17 March 2009}}</ref><ref>{{cite journal|last=Farace|first=Joe|title=mccartyphotoworks.com|journal=Shutterbug|date=April 2008|page=22}}</ref> Preferring to work in-camera and without compositing, McCarty creates his photographs by sometimes traveling to exotic locations,<ref>{{cite journal|last=Hogg|first=Victoria|title=Toy Story|journal=Bizarre Magazine|date=July 2010|pages=83–85|location=UK}}</ref> including active war zones.<ref>{{cite web|last=Namdar|first=Asieh|title=Art Project Combines War, Therapy -- and Toys|url=http://www.cnn.com/2011/11/03/world/war-toys-mccarty/index.html|publisher=CNN.com|accessdate=27 March 2013|date=3 November 2011}}</ref> Although grounded in reality, because of his use of wit and whimsy, McCarty's work is often associated with the [[Designer toys|Art-Toy]], [[Lowbrow (art movement)|Lowbrow]], and [[Pop Surrealism|Pop Surrealist]] movements.<ref>{{cite book|title=Beyond Ultraman: Seven Artists Explore the Vinyl Frontier|year=2007|publisher=Baby Tattoo Books|pages=46–51|editor=PMCA and LATDA|type = Exhibition Catalog}}</ref><ref>{{cite web|last=Platt|first=Olivia|title=The Secret Life of Toys: Dramatic Photos by Brian McCarty|url=http://www.telegraph.co.uk/culture/culturepicturegalleries/6131878/The-secret-life-of-toys-dramatic-toy-photos-by-Brian-McCarty.html|work=Interview|publisher=Telegraph.co.uk|accessdate=27 March 2013|date=3 September 2009}}</ref>

== Career ==

=== Education ===
McCarty attended [[Parsons School of Design]] (1992–1996) and earned a [[Bachelor of Fine Arts|BFA]] in [[photography]].<ref>{{cite journal|last=Host|first=Vivian|title=Vis-Ed: Brian McCarty|journal=XLR8R|date=November 2006|pages=124–128}}</ref> While at Parsons in 1993, he began work on a photographic series titled ''The Dollhouse''. For the series McCarty constructed a toy hyperreality and used the simulacra of family to explore modern living.<ref>{{cite journal|last=Seifert|first=Daniel|title=Brian McCarty|journal=Hi Fructose|date=Spring 2005|pages=6–12}}</ref> ''The Dollhouse'' received notable praise – earning a spot in the traveling exhibition ''Making It Real'', curated by [[Vik Muniz]].<ref>{{cite journal|last=Zimmer|first=William|title=Life With a Twist: Fiction-Based Photography…|journal=The New York Times|date=16 March 1997|page=12}}</ref><ref>{{cite book|last=Muniz|first=Vik|title=Making it Real|year=1997|publisher=Independent Curators International|location=New York|pages=56–57}}</ref>

=== Fabrica ===
Upon graduation from Parsons in 1996, McCarty accepted a grant to the [[Benetton Group|Benetton]] supported [[Fabrica research centre]] in Treviso, Italy.<ref>{{cite journal|last=Croci|first=Roberto|title=Crazy Toys Addiction|journal=L’Uomo Vogue|date=February 2007|page=66|language=Italian}}</ref> While there he collaborated with an international group of young artists on a number of
advertising campaigns and fine art exhibitions, including ''Habitus, Abito, Abitare, Progetto Arte'' at the [[Centro per l'arte contemporanea Luigi Pecci|Luigi Pecci Centre for
Contemporary Art]] and ''KON©EPT'', the first major photographic exhibition in [[Zagreb]] after the [[Croatian war of independence|Croatian War of Independence]].<ref>{{cite book|title=KON©EPT|year=1996|publisher=Hrvatski Fotosavez|location=Zagreb|page=137|editor=Croatian Photographic Society|type = Exhibition Catalog}}</ref>

=== Mattel ===
McCarty worked as an in-house photographer for [[Mattel|Mattel Toys]] (1999-2002), primarily on the [[Hot wheels|Hot Wheels]] brand of toys.<ref>{{cite journal|last=Gaita|first=Paul|title=Serious Fun: Photographer Brian McCarty Taps His Inner Child to Explore Grown-up Ideas|journal=NOHO>LA|date=October 2003|page=6}}</ref>

=== Art-Toys ===
In 2003 McCarty founded the photo studio McCarty PhotoWorks in [[West Hollywood]], [[California]].<ref>{{cite journal|last=Jimenez|first=Zoe|title=Brian McCarty|journal=While You Were Sleeping|date=November–December 2004|pages=108–109}}</ref> Through the studio, McCarty created collaborative work with a number of well-known artists from the [[Designer toys|Art-Toy]] and [[Pop Surrealism|Pop Surrealist]] movements,<ref>{{cite web|last=Pescovitz|first=David|title=Brian McCarty's art toy photography|url=http://boingboing.net/2005/06/25/brian-mccartys-art-t.html|publisher=Boing Boing|date=25 June 2005|accessdate=27 March 2013}}</ref> including [[Mark Ryden]],<ref>{{cite web|last=Pescovitz|first=David|title=Mark Ryden's first toy, photographed by Brian McCarty|url=http://boingboing.net/2009/03/30/mark-rydens-first-to.html|publisher=Boing Boing|date=30 March 2009|accessdate=27 March 2013}}</ref> [[Gary Baseman]], [[Tim Biskup]], [[Joe Ledbetter]], Amanda Visell, Michelle Valigura, [[Greg Simkins|Greg "Craola" Simkins]], [[FriendsWithYou]], [[Andrew Bell (artist)|Andrew Bell]], [[Jeremy Fish]], [[Luke chueh|Luke Chueh]], Mario “MARS-1” Martinez, Scott Tolleson, [[Simone Legno]], and Yoskay Yamamoto.<ref>{{cite journal|last=Okes|first=Thomas|title=Brian McCarty’s Playful Therapy|journal=One Small Seed|year=2011|issue=21|pages=43–48|location=South Africa}}</ref> A collection of McCarty's Art-Toy collaborations was published as a monograph in 2010 by Baby Tattoo Books in Los Angeles.<ref>{{cite journal|last=Hogg|first=Victoria|title=Toy Story|journal=Bizarre Magazine|date=July 2010|pages=83–85|location=UK}}</ref>

=== War-Toys ===
McCarty began the WAR-TOYS [[Photo-essay]] and [[documentary film]] in 2010.<ref>{{cite web|last=Namdar|first=Asieh|title=Art Project Combines War, Therapy -- and Toys|url=http://www.cnn.com/2011/11/03/world/war-toys-mccarty/index.html|publisher=CNN.com|accessdate=27 March 2013|date=3 November 2011}}</ref> The project uses principles of [[play therapy]] and [[art therapy]] to explore children's firsthand accounts of war.<ref>{{cite web|last=Platt|first=Olivia|title=WAR-TOYS: Fake Plastic Soldiers|url=http://www.dontpaniconline.com/magazine/radar/war-toys|work=Interview|publisher=Don’t Panic|accessdate=27 March 2013|date=1 August 2011}}</ref> McCarty completed his first trip to the Middle East in 2011,<ref>{{cite web|title=War-Toys: Conflict through the eyes of children|url=http://cnnphotos.blogs.cnn.com/2011/11/03/war-toys-conflict-through-the-eyes-of-children|publisher=CNN Photo Blog|accessdate=27 March 2013|date=3 November 2011}}</ref> collaborating with children from the [[American Colony, Jerusalem#The Spaffords|Spafford Children's Center]] in East Jerusalem and the [[Dheisheh|Dheisheh Refugee Camp]] outside of Bethlehem. He returned to the region in late 2012 and collaborated with Palestinian children in the [[Gaza Strip]] during the escalation leading up to [[Operation Pillar of Defense]], then with Israeli children in the nearby town of [[Sderot]] beginning on the morning of the ceasefire.<ref>{{cite web|last=McCarty|first=Brian|title=WAR-TOYS Blog|url=http://www.blog.wartoysproject.com|accessdate=27 March 2013}}</ref> McCarty has announced his intention to expand the project into additional areas on conflict around the world, including [[Sudan]], [[Afghanistan]], and [[Colombia]].<ref>{{cite web|last=McCarty|first=Brian|title=War, Art Therapy, and Toys|url=http://tedxtalks.ted.com/video/TEDxSantaMonica-Brian-McCarty-W|publisher=TEDx|accessdate=27 March 2013}}</ref>

==Selected Exhibitions==
* "War Games," V&A Museum of Childhood, London, England (2013)
* “War Games,” Volksmuseum Schleswig, Schleswig, Germany (2012)<ref>{{cite web|title=War Games|url=http://www.schloss-gottorf.de/volkskunde-museum-schleswig/ausstellungen/wargames|publisher=Volkskunde Museum Schleswig|accessdate=28 March 2013}}</ref> 
* "WAR-TOYS,” Richard F. Brush Art Gallery, St. Lawrence University, Canton, NY (2012)<ref>{{cite web|url=http://www.stlawu.edu/gallery/exhibitions/f/12wartoys.php|title=WAR-TOYS: Photographs by Brian McCarty|publisher=St. Lawrence University|accessdate=28 March 2013}}</ref> 
* “Freedom to Create,” The Company’s Gardens, Cape Town, South Africa (2011)<ref>{{cite web|title=Brian McCarty|url=http://www.freedomtocreate.com/brian-mccarty|publisher=Freedom to Create|accessdate=28 March 2013|location=South Africa}}</ref>
* “Art-Toys,” La Luz de Jesus Gallery, Los Angeles, California (2010)
* “Sweet Streets,” Nucleus Gallery, Alhambra, California (2010)
* “Under the Influence: He Man and the Masters of the Universe,” Gallery 1988, Los Angeles, California (2010)
* “MANIFEST HOPE: DC,” Irvine Contemporary Gallery, Washington, D.C. (2009)
* “Three Apples,” Royal/T, Culver City, California (2009)
* “Hi Fructose Group Art Show,” CoproNason Gallery, Santa Monica, California (2008)
* “Beyond Ultraman,” Pasadena Museum of California Art, Pasadena, California (2007)
* “i am 8-bit,” Gallery 1988, Los Angeles, California (2007)
* “Toys: New Designs From the Art Toy Revolution,” The Showroom NYC (2006)
* “Curious Creatures,” Punch Gallery, San Francisco, California (2004)
* “Making it Real,” Portland Museum of Art, Portland, Maine (1998)
* “Making it Real,” Bayly Art Museum, University of Virginia, Charlottesville, VA (1998)
* “Making it Real,” Bakalar Gallery, Massachusetts College of Art, Boston, MA (1998)
* “Making it Real,” Emerson Gallery, Hamilton College, Clinton, New York (1998)
* “Making it Real,” Aldrich Contemporary Art Museum, Ridgefield, Connecticut (1997)
* “Making it Real,” The Reykjavik Municipal Art Museum, Reykjavik, Iceland (1997)
* “Making it Real,” the Luigi Pecci Centre for Contemporary Art in Prato, Italy (1996)

== Bibliography ==
* ''War-Toys: Israel, West Bank, Gaza Strip'', McCarty, Brian. Los Angeles. McCarty PhotoWorks, 2013 (ISBN 978-0615836584)
* ''Art-Toys: Photographs by Brian McCarty'', McCarty, Brian. Los Angeles. Baby Tattoo Books, 2010 (ISBN 978-0979330766)
* ''Hi-fructose Collected Edition'', Owens, Annie. San Francisco. Last Gasp, 2009  (ISBN 978-0867197136)
* ''Beyond Ultraman'', Kwong, Maria, and Pasadena Museum of California Art, ed. Los Angeles. Baby Tattoo Books, 2007 (ISBN 978-0979330728) 
* ''Dot Dot Dash'', Klanten, Robert, and Hubner, Matthias, ed. Berlin. Die Gestalten Verlag, 2006 (ISBN 978-3899551617)
* ''Toys: New Designs From the Art Toy Revolution'', STRANGEco, ed. New York. MTV/Universe Publishing, 2006 (ISBN 978-0789313904)
* ''Vinyl Will Kill'', IdN and Jeremyville, ed. Hong Kong. IdN, 2004(ISBN 978-9889706500)

== References ==
{{Reflist}}

== External links ==
*[http://www.brianmccarty.com Official website of Brian McCarty]
*[http://www.wartoysproject.com WAR-TOYS project website]
*[http://www.cnn.com/2011/11/03/world/war-toys-mccarty/index.html CNN.com feature on WAR-TOYS]

{{DEFAULTSORT:McCarty, Brian}}
[[Category:American photographers]]
[[Category:1974 births]]
[[Category:Living people]]