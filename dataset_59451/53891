{{Refimprove|date=January 2017}}
'''Pre-echo''', sometimes called a '''forward echo''', (not to be confused with [[reverse echo]]) is a [[digital audio]] [[compression artifact]] where a sound is heard before it occurs (hence the name). It is most noticeable in impulsive sounds from [[percussion instrument]]s such as [[castanet]]s or [[cymbal]]s.

It occurs in transform-based [[Audio compression (data)|audio compression]] algorithms – typically based on the [[modified discrete cosine transform]] (MDCT) – such as [[MP3]], [[Advanced Audio Coding|MPEG-4 AAC]], and [[Vorbis]], and is due to quantization noise being spread over the entire transform-window of the codec.

== Cause ==
The psychoacoustic component of the effect is that one hears only the echo preceding the transient, not the one following – because this latter is drowned out by the transient. Formally, forward [[temporal masking]] is much stronger than backwards temporal masking, hence one hears a pre-echo, but no post-echo.

==Mitigation==
In an effort to avoid pre-echo artifacts, many sound processing systems use filters where all of the response occurs after the main impulse, rather than [[linear phase]] filters. Such filters necessarily introduce [[phase (waves)|phase]] distortion and temporal smearing, but this additional distortion is less audible because of strong forward masking.

{{Listen|filename=
Genesis-Duke-LPpreecho.ogg|title=LP pre echo|description=The empty space before the start of the music on this LP has been amplified +15dB to reveal the pre-echo.|format=[[Ogg]]}}

Avoiding pre-echo is a substantial design difficulty in transform domain [[lossy]] audio [[codec]]s such as [[MP3]], [[Advanced Audio Coding|MPEG-4 AAC]], and [[Vorbis]]. It is also one of the problems encountered in [[digital room correction]] [[algorithm]]s and frequency domain filters in general ([[Noise reduction|denoising]] by spectral subtraction, [[Equalization (audio)|equalization]], and others). One way of reducing "breathing" for filters and compression techniques using piecewise Fourier-based transforms is picking a smaller transform window (short blocks in MP3), thus increasing the temporal resolution of the algorithm at the cost of reducing its frequency resolution.

==As a sound effect==
A pre-echo effect may be added intentionally as a type of sound effect. A common use for this is to make a person's voice sound [[ghost]]ly.<ref>[https://vimeo.com/21233069 MAKING GHOSTLY SOUND EFFECTS - Backwards Echo - Linden Hudson]</ref>

==See also==
*[[Compression artifact]]

==References==
{{Reflist}}

==External links==
*[http://wiki.hydrogenaud.io/index.php?title=Pre_echo Pre-echo] at [http://wiki.hydrogenaud.io/ Hydrogenaudio Knowledgebase]
*[http://18.7.29.232/handle/1721.1/36465 Pre-echo detection & reduction, Massachusetts Institute of Technology, 1994]

[[Category:Acoustics]]