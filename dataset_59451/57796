{{Infobox organization
|name = Società italiana di economia demografia e statistica
|image = Sieds.gif
|size = 70x42px
|abbreviation = SIEDS
|type = [[Scientific society]]
|founded_date = 1939
|purpose = Furthering economic, demographic, and statistical studies
|headquarters = Rome (Italy)
|leader_title = President
|leader_name = Giovanni Maria Giorgi
}}
The '''Italian society of economics demography and statistics''' (SIEDS, {{lang-it|Società italiana di economia demografia e statistica}}) is a [[learned society]] aiming to further [[economics|economic]], [[demographics|demographic]], and [[statistics|statistical]] studies and to establish active co-operation among professionals of the mentioned subjects in the field of [[social sciences]] and human behaviour. The society pursues this aim by: 
*organising seminars, congresses, or scientific meetings for analysing and discussing problems concerning its activity;
*implementing scientific surveys, enquires and researches and promoting, training activities (courses, seminars, etc.);
*publishing the ''[[Rivista italiana di economia demografia e statistica]]'', ''SIEDS News'', a series of studies and monographs on specific items concerning the scientific interest of the society, as well as the proceedings of its congresses and seminars.

== History ==
SIEDS was founded on June 29, 1939, at the initiative of [[Livio Livi]] and others. The first seat of the society was located in Florence. Initially named "Italian Society of Demography and Statistics", it originated in the Advisory Committee for the Population Study ({{lang-it|Comitato di consulenza per gli studi della popolazione}}), also founded by Livi. The first publications of the society were only the scientific meeting proceedings but, starting from January 1947, the society published its own scientific journal,the ''Italian Review of Demography and Statistics''. On 18 April 1950 the society decided to expand its fields of interest also to economics and obtained its current name. In 1950 the society's journal was renamed accordingly to ''[[Rivista italiana di economia demografia e statistica]]''. During the first meeting of the society, held at Istat (Rome, 28–29 May 1939), Livi underlined that at that time there were already 16 statistical societies in Europe as well as in North America, Argentina, Brazil, India, China, and Japan. Therefore, Italian academics thought that the time was arrived also for their own wide national society. During the scientific meeting held on December 28, 1940 the society already counted 122 ordinary members.

== External links ==
* {{Official website|http://www.sieds.it/}}

[[Category:Economics organizations]]
[[Category:Statistical societies]]
[[Category:Demographics of Italy]]
[[Category:Organizations established in 1939]]
[[Category:1939 establishments in Italy]]
[[Category:Organisations based in Rome]]