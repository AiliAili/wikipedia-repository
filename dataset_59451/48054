{{Use dmy dates|date=March 2014}}
{{Use British English|date=March 2014}}
{{Infobox person
| name        = T. Lindsay Galloway
| image       = 
| alt         = 
| caption     = 
| other_names = Galloway, Thomas Lindsay
| occupation  = British engineer
| birth_date  = {{start-date|1854}}
| death_date  = {{Death-date and age|22 September 1921|1854|df=yes}}
| birth_place = Paisley, Renfrewshire, Scotland
| death_place = Campbeltown, Kintyre, Scotland
}}

'''Thomas Lindsay Galloway''' MA, FRSE. FGS, AMInst, MInstME (1854 – 22 September 1921) was the youngest son of William Galloway (1799–1854) shawl manufacturer and coal master of Paisley, Scotland and Margaret Lindsay (1818–1902). He was a civil and mining engineer and coal master of Argyll Colliery, [[Campbeltown]], [[Kintyre]], and like his brothers, [[Sir William Galloway]] and [[Robert Lindsay Galloway]], he was also the author of several papers, lectures, designs and books.

==Life==

Thomas was born in [[Paisley, Renfrewshire]]. He was educated at [[Glasgow University]] where he studied under [[Lord Kelvin]] and was selected by him to travel to the sea off of [[Brazil]] to carry out the '[[piano wire]] method' of [[deep sea]] soundings.<ref name="geolsoc">{{cite web|url=http://www.geolsoc.org.uk/gsl/society/history/page6010.html|archive-url=https://web.archive.org/web/20110826060415/http://www.geolsoc.org.uk:80/gsl/society/history/page6010.html |archive-date=2011-08-26 |dead-url=yes|title=Geological Society - Obituaries - G|accessdate=26 September 2014}}</ref><ref name="google">{{cite web|url=https://news.google.com/newspapers?nid=1955&dat=19241130&id=BI0hAAAAIBAJ&sjid=4JcFAAAAIBAJ&pg=2571,6006569|title=Reading Eagle - Google News Archive Search|publisher=news.google.com|accessdate=26 September 2014}}</ref> The measuring equipment used by him is kept in the 'Kelvin Room' of the [[Royal Society of Edinburgh]].

On his return from Brazil he concentrated on his studies of mining becoming a Mining Manager of East Scotland on 10 July 1876,<ref name="dmm">{{cite web|url=http://www.dmm.org.uk/certs/names_ga.htm|title=Durham Mining Museum - Management Certificates &#91;ga*&#93;|author=Durham Mining Museum|publisher=dmm.org.uk|accessdate=26 September 2014}}</ref> a member of the North of England Institute of Mining Engineers 2 Sept 1876,<ref name="dmm-gallery">{{cite web|url=http://www.dmm-gallery.org.uk/transime/n30-m.htm|archive-url=https://web.archive.org/web/20080828001859/http://www.dmm-gallery.org.uk/transime/n30-m.htm |archive-date=2008-08-28 |dead-url=yes|title=Durham Mining Museum - Mining Engineers|accessdate=26 September 2014}}</ref> aged 31, at the [[Argyll Colliery]], [[Campbeltown]], [[Kintyre]].

He was also elected a Fellow of the Kintyre Archaeological Society in 1876, an interest he later shared with his wife,<ref name="google2">{{cite book|title=Proceedings of the Society of Antiquaries of Scotland|author=Society of Antiquaries of Scotland|date=2002|issue=v. 132|publisher=Society|issn=0081-1564|url=https://books.google.co.uk/books?id=ZnRnAAAAMAAJ}}</ref><ref name="google3">{{cite book|title=The Scottish Historical Review|author=Company of Scottish History|date=1921|issue=v. 18|publisher=Edinburgh University Press for the Scottish Historical Review Trust|issn=0036-9241|url=https://books.google.co.uk/books?id=mlsJAAAAIAAJ}}</ref> and a founder member and elected Vice Chairman at the inaugural meeting of the Kintyre Scientific Association on 24 October 1890.

While in Kintyre he became deeply involved, being the chief engineer for the [[Campbeltown and Machrihanish Light Railway]] which served his [[Argyll]] Colliery.<ref name="google4">{{cite book|title=Locomotive, Railway Carriage and Wagon Review|date=1906|issue=v. 12-13|publisher=Locomotive Publishing Company|url=https://books.google.co.uk/books?id=l-bNAAAAMAAJ}}</ref><ref name="machrihanish">{{cite web|url=http://www.machrihanish.net/index.php?option=com_content&view=article&id=24&Itemid=63|archive-url=https://web.archive.org/web/20110719002933/http://www.machrihanish.net/index.php?option=com_content&view=article&id=24&Itemid=63 |archive-date=2011-07-19 |dead-url=yes|title=Machrihanish Online &#124; Railway|accessdate=26 September 2014}}</ref>

He died 22 September 1921 at Kilchrist, [[Campbeltown]], Kintyre. His obituary was placed in the Proceedings of the [[Royal Society of Edinburgh]],<ref name="google5">{{cite book|title=Proceedings of the Royal Society of Edinburgh|date=1922|issue=v. 41-42|issn=0080-4541|url=https://books.google.co.uk/books?id=uKtWAAAAIAAJ}}</ref> [[The Geological Society]] of 1922<ref name="geolsoc" />

== Literary Career and Inventions ==

In 1878 T. Lindsay Galloway wrote 'On the Present Condition of Mining in Some of the Principal Coal Producing Districts of the Continent'

The same year he presented a paper, together with C.Z. Bunning, entitled 'A Description of an Instrument for Levelling Underground'<ref name="mininginstitute">{{cite web|url=http://www.mininginstitute.org.uk/library/journals/NEIMME%20vol%2027.html|title=Home|publisher=mininginstitute.org.uk|accessdate=26 September 2014}}</ref> to the North of England Institute of Mining & Mechanical Engineers (NEIMME).

His mining surveys of 1881 are kept by the NEIMME<ref name="mininginstitute2">{{cite web|url=http://www.mininginstitute.org.uk/library/categories/TransScot.html|title=Home|publisher=mininginstitute.org.uk|accessdate=26 September 2014}}</ref>

In 1902 he read a paper entitled 'The Campbeltown Colliery and Light Railway' to the Glasgow Association of Students of the Institution of Civil Engineers,<ref name="google6">{{cite book|title=The Campbeltown Colliery and Light Railway: A Paper Read Before the Glasgow Association of Students of the Institution of Civil Engineers, Session 1901-1902|author=Galloway, T.L.|date=1902|publisher=Aird & Coghill|url=https://books.google.co.uk/books?id=WFJqNQAACAAJ}}</ref><ref name="google7">{{cite book|title=The Glasgow University Calendar|author=University of Glasgow|date=1902|url=https://books.google.co.uk/books?id=NL84AAAAMAAJ}}</ref> and his notes are referred to by the 'Transactions of the Institute of Mining Engineers' Vol. 45<ref name="google8">{{cite book|title=Transactions of the Institution of Mining Engineers|author=Institution of Mining Engineers (Great Britain)|date=1913|issue=v. 45|publisher=The Institution|issn=0371-9634|url=https://books.google.co.uk/books?id=scIlAQAAIAAJ}}</ref>

In 1903 he read before the Glasgow University Engineering Society papers describing various methods of supplying power to mines, and indicating why different methods have found suitable spheres of application.<ref name="google9">{{cite book|title=Factory and Industrial Management|author1=Dunlap, J.R.|author2=Carmody, J.M.|date=1903|issue=v. 24|publisher=McGraw-Hill Publishing Company, Incorporated|issn=0096-0578|url=https://books.google.co.uk/books?id=zrAPAQAAIAAJ}}</ref> Then in 1905 he instructed the Institute of Mining Engineers that he had recently had erected an electric plant at Campbeltown colliery.<ref name="google10">{{cite book|title=Transactions of the Institution of Mining Engineers|author=Institution of Mining Engineers (Great Britain)|date=1905|issue=v. 27|publisher=The Institution|issn=0371-9634|url=https://books.google.co.uk/books?id=Zw7OAAAAMAAJ}}</ref>

His Papers on 'Experiments with two electrically driven pumps' were discussed by the Institute of Mining Engineers, Kilmarnock 8 August 1908<ref name="google11">{{cite book|title=Transactions of the Institution of Mining Engineers|author=Institution of Mining Engineers (Great Britain)|date=1909|issue=v. 36|publisher=The Institution|issn=0371-9634|url=https://books.google.co.uk/books?id=fwjOAAAAMAAJ}}</ref>

In 1918 T. Lindsay Galloway read a paper to the Institute of Mining Engineers entitled 'A method of determining the [[magnetic meridian]] as a basis for mining surveys'. He had also invented a portable mining [[magnetometer]], to assist in surveys. This is again described in Vol. 23 of 'The Mining Magazine' of 1920,<ref name="google12">{{cite book|title=The Mining Magazine|author=Rickard, T.A.|date=1920|issue=v. 23|publisher=Mining Journal|issn=0308-6631|url=https://books.google.co.uk/books?id=vtJLAAAAYAAJ}}</ref> the 'Engineering and Mining Journal' Vol. 106 of 1918,<ref name="google13">{{cite book|title=Engineering and Mining Journal|author=American Institute of Mining Engineers|date=1918|issue=v. 106|publisher=McGraw-Hill Publishing Company|issn=0361-3941|url=https://books.google.co.uk/books?id=d4ogAQAAMAAJ}}</ref> and 'Nature, International Journal of Science' Vol. 101 1918<ref name="google14">{{cite book|title=Nature|author=Lockyer, N.|date=1918|issue=v. 101|publisher=Macmillan Journals Limited|issn=0028-0836|url=https://books.google.co.uk/books?id=FEBoxJZeoeMC}}</ref>

In a paper of 1919 he drew attention to the advantage of using a [[theodolite]] and this was repeated in 'Transactions of the Institution of Mining Engineers' Vol. 60 of 1920<ref name="google15">{{cite book|title=Transactions of the Institution of Mining Engineers|author=Institution of Mining Engineers (Great Britain)|date=1921|issue=v. 60|publisher=The Institution|issn=0371-9634|url=https://books.google.co.uk/books?id=XLYlAQAAIAAJ}}</ref>

He wrote several papers which were referred to in 'The Mining Engineer' Vols. 60-61 of 1921<ref name="google16">{{cite book|title=The Mining Engineer|date=1921|issue=v. 60-61|url=https://books.google.co.uk/books?id=u9ARAAAAIAAJ}}</ref>

== Family ==

Thomas Lindsay Galloway married Margaret Maria Christina MacNab, daughter of Duncan MacNab and Margaret McCullock of Kintyre, and widow of Cyril Leslie Johnson, in Paddington, London 1902. She had a son by her previous marriage.

== Bibliography ==

* Galloway T. Lindsay. 'The Campbeltown Colliery and Light Railway' Aird & Coghill, Glasgow. 1902 [https://books.google.co.uk/books?id=WFJqNQAACAAJ&dq=campbeltown+colliery+and+light+railway&hl=en&ei=iYTGToTTIciv8AOJxehz&sa=X&oi=book_result&ct=result&resnum=1&ved=0CDYQ6AEwAA]

== Honours ==
*[[Fellow of the Royal Society of Edinburgh]]<ref>{{cite book|title=Proceedings of the Royal Society of Edinburgh &#124; Obituary|page=193|url=https://books.google.com/books?id=uKtWAAAAIAAJ|year=1922}}</ref>
*[[Fellow of the Geological Society]] 1876

== References ==
{{Reflist}}

== External links ==
* [http://www.kintyremag.co.uk/1999/26.page2.html]
*{{cite web|url=https://news.google.com/newspapers?nid=1955&dat=19241130&id=BI0hAAAAIBAJ&sjid=4JcFAAAAIBAJ&pg=2571,6006569|title=Reading Eagle - Google News Archive Search|publisher=news.google.com|accessdate=26 September 2014}}
*{{cite web|url=http://highfields-arc.co.uk/biogs/wthomson.htm|title=Sir William Thomson, Lord Kelvin.|publisher=highfields-arc.co.uk|accessdate=26 September 2014}}

{{DEFAULTSORT:Galloway, T. Lindsay}}
[[Category:Articles created via the Article Wizard]]
[[Category:1854 births]]
[[Category:1921 deaths]]
[[Category:British mining engineers]]