{{Orphan|date=February 2013}}

'''Yolanda Young''' (born October 31, 1968) <ref>Library Reference https://openlibrary.org/subjects/person:yolanda_young_(1968-)</ref> is
an American journalist, author, and lecturer.<ref>OBABL Media Company Bio {{cite web|url=http://www.obabl.com/about/yolanda-young-esq/ |title=Archived copy |accessdate=September 30, 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120310080659/http://www.obabl.com/about/yolanda-young-esq/ |archivedate=March 10, 2012 }}</ref> Her work as a writer focuses on law, politics, and culture. Young has addressed audiences at Harvard Law School <ref>Harvard BLSA Spring Conference 2011 http://www.harvardblsa.com/conference/tag/spring-conference/</ref> and the University of Arizona.<ref>University of Arizona http://uanews.org/story/nationally-syndicated-columnist-author-yolanda-young-speak-ua-africana-studies-lecture-series</ref> She was the keynote speaker for the 2011 National Black Pre-Law Conference.<ref>National Pre-Law Conference 2011 http://www.prlog.org/11452509-author-lecturer-and-blogger-yolanda-young-to-keynote-at-2011-national-black-pre-law-conference.html</ref>

She is on the board of the PEN/Faulkner Foundation.<ref>PEN/Faulkner Board of Directors http://www.penfaulkner.org/about/board-of-directors/</ref>

==Writing career==

Young's first book, On Our Way To Beautiful (Random House 2002) received favorable reviews from publications like [[The Chicago Tribune]].<ref>Memoir is a tale of triumph August 28, 2002|By Carmen Marti. Special to the Tribune. http://articles.chicagotribune.com/2002-08-28/features/0208280025_1_father-shot-human-spirit-villard-books</ref> Young has also been included in the following anthologies: This I Believe II: More Personal Philosophies of Remarkable Men and Women;<ref>This I Believe http://www.powells.com/biblio/17-9780805090895-2</ref> Shaking the Tree: A Collection of New Fiction;<ref>Shaking the Tree http://www.powells.com/biblio/17-9780393050677-0</ref> and Memoir by Black Women; and The Black Body.<ref>The Black Body http://www.powells.com/biblio/62-9781583228890-0</ref>

Young’s upcoming book, “Thurgood's Legacy: Black Lawyers Reflect on Law School, the Legal Profession, and Life,”<ref>Carolina Academic Press http://www.cap-press.com/books/isbn/9781611631562/Thurgoods-Legacy</ref> is based on a series of articles written for On Being A Black Lawyer.<ref>The Story of a Georgetown Law Class: The Lessons.{{cite web|url=http://www.onbeingablacklawyer.com/?cat%3D570 |title=Archived copy |accessdate=September 30, 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20120430130105/http://www.onbeingablacklawyer.com/?cat=570 |archivedate=April 30, 2012 }}</ref>

==Journalism career==

Young contributes columns to [[USA TODAY]]. She also provides commentary for [[NPR]], [[The Washington Post]] and other publications. Noteworthy pieces include "Bakke ruling exposes generational divide,"<ref>Bakke ruling exposes generational divide, USA Today, March 31, 2011. http://usatoday30.usatoday.com/printedition/news/20080215/opcom14.art.htm</ref> Stay Away From Your Man, Rihanna!<ref>Yolanda Young. Stay Away From Your Man, Rihanna! NPR, September 3, 2009.http://www.npr.org/templates/story/story.php?storyId=112466368</ref> Mapp  v. Ohio Turns 50: A Look at Warren Court’s Rights of
Defendants,<ref>Yolanda Young, Mapp v. Ohio Turns 50: A Look at Warren Court’s Rights of Defendants, Politic 365, June 15, 2011.http://politic365.com/2011/06/15/mapp-v-ohio-turns-50-a-look-at-warren-court’s-rights-of-defendants/</ref> and It’s Shreveport’s Season.<ref>Yolanda Young, It's Shreveport's Season, The Washington Post, Sunday, August 26, 2007 http://www.washingtonpost.com/wp-dyn/content/article/2007/08/24/AR2007082401210.html</ref>

==On Being A Black Lawyer==

Young founded On Being A Black Lawyer (OBABL) in 2008 as a news and resource center for African American attorneys.<ref>OBABL Media Company www.obabl.com</ref> OBABL has been recognized by the American Bar Association.<ref>‘On Being a Black Lawyer’  Blogger Reaches Out to
Minorities Nov 25, 2008 http://www.abajournal.com/news/article/on_being_a_black_lawyer_blogger_reaches_out_to_minorities/</ref> In addition to its legal news blog, OBABL releases the following publications annually: The Power 100, which recognizes the most powerful black attorneys in the nation;<ref>The Power 100, which recognizes the most powerful black attorneys in the nation {{cite web|url=http://www.obabl.com/power_100/index.html |title=Archived copy |accessdate=September 30, 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121215051129/http://www.obabl.com/power_100/index.html |archivedate=December 15, 2012 }}</ref> The Salute to the Congressional Black Caucus & the Nation’s Top Lawyers Black Lawyers With Influence;<ref>The Salute to the Congressional Black Caucus & the Nation’s Top Lawyers Black
http://www.onbeingablacklawyer.com/obabl/obabl2/index.html</ref> and The Black Student’s Guide to Law Schools, which includes rankings of the top law schools for black students.

==Controversy==

In 2008 Young wrote an expose in [[The Huffington Post]] “Law Firm Segregation Reminiscent of Jim Crow.”<ref>Yolanda Young, Law Firm
Segregation Reminiscent of Jim Crow, March 17, 2008 http://www.huffingtonpost.com/yolanda-young/law-firm-segregation-remi_b_91881.html</ref>

“[Law firm] Staff attorneys are non-partner track lawyers who handle the menial legal tasks--generating binders and attaching "relevant" or
"not relevant" codes to thousands of emails, spreadsheets, and any other documents associated with a particular case—that associates
shun. While paralegals have their own offices, as many as ten staff attorneys share windowless file rooms. Segregated from other lawyers in the firm, we go uninvited to attorney-only firm functions and are not provided jury duty or maternity leave. The base pay and bonus structure is half that of a 25 year old first year associate's."

Young would go on to file a lawsuit against the firm alleging discrimination and retaliation; however, a federal judge dismissed the
case on summary judgement.<ref>Judge Dismisses Former Covington Staff Attorney's Discrimination Suit, Legal Times MARCH 07, 2012.
http://legaltimes.typepad.com/blt/2012/03/judge-dismisses-former-covington-staff-attorneys-discrimination-suit.html</ref>

==Personal life==

Young was raised in [[Shreveport, Louisiana]]. In her memoir, On Our Way To Beautiful, Young recounts how she and her family overcame many
tragedies. According to [[Washingtonian Magazine]], “The stories tell of a sometimes-troubled family--Young's father shot and wounded her mother,
and an uncle is in prison--but a mostly loving one, thanks to the values imparted by Young's grandmother and great-grandmother.”<ref name="washingtonian">http://www.washingtonian.com/articles/print/2002/07/01/how-i-got-here.php</ref>

Young attended [[Howard University]] majoring in Accounting with a minor in Political Science. She is also a graduate of the [[Georgetown University Law Center]].<ref name="washingtonian" />

== References ==
{{Reflist}}
*
*
*

{{DEFAULTSORT:Young, Yolanda}}
[[Category:1968 births]]
[[Category:Living people]]
[[Category:American women journalists]]
[[Category:American women writers]]
[[Category:Journalists from Louisiana]]
[[Category:21st-century American writers]]
[[Category:21st-century women writers]]