{{Infobox concert tour
| image           = Butterflyworldtour.jpg
| image_caption   = Promotional photo from tour booklet
| image_size      = 
| border          = yes
| artist          = [[Mariah Carey]]
| album           = ''[[Butterfly (Mariah Carey album)|Butterfly]]''
| start_date      = {{Start date|1998|01|11}}
| end_date        = {{End date|1998|02|21}}
| number_of_legs  = 3
| number_of_shows = {{ubl|5 in Asia|6 in Australia|1 in North America|12 Total}}
| gross           = 
| last_tour       = [[Daydream World Tour]]<br />(1996)
| this_tour       = '''Butterfly World Tour'''<br />(1998)
| next_tour       = [[Rainbow World Tour]]<br />(2000)
}}

The '''Butterfly World Tour''' was the third [[concert tour]] by [[United States|American]] [[singer-songwriter]] [[Mariah Carey]]. The tour promoted Carey's album at the time, ''[[Butterfly (Mariah Carey album)|Butterfly]]'' (1997), and included songs from several of her previous albums. The tour visited [[Asia]], [[Australia]] and the [[United States]], with rehearsals taking place in December 1997. Starting on January 11, 1998 the tour spanned five shows in Asia, six in Australia, and one in Hawaii, US. Throughout the tour, Carey varied hairstyles and outfits, as well as song selections. The Butterfly World Tour was very successful; the four dates at Japan's largest stadium, [[Tokyo Dome]], sold out in under one hour, equaling over 200,000 tickets, breaking the previous record she held at the stadium for show sell-outs.<ref>{{Cite web|url=http://www.timeout.jp/en/tokyo/feature/4829/Tokyo-trivia-40-facts-to-wow-your-mind |publisher=[[Time Out (magazine)|Time Out]] |title=Tokyo trivia: 40 facts to wow your mind |archiveurl=http://www.webcitation.org/6CMblPD0V?url=http://www.timeout.jp/en/tokyo/feature/4829/Tokyo-trivia-40-facts-to-wow-your-mind |archivedate=22 November 2012 |deadurl=yes |df= }}</ref>

The tour was recorded in VHS format, and was titled ''[[Around the World (video)|Around the World]]''. The video featured live performances of Carey at different worldwide venues including New York, Japan, Hawaii and Brisbane. Other scenes are included in the video such as a conversation between Carey and [[Brenda K. Starr]] prior to her performance of "[[I Still Believe (Brenda K. Starr song)|I Still Believe]]". Prior to the performances in Australia, a scene of Carey swimming with dolphins is shown. Additionally, [[Olivia Newton-John]] makes a cameo appearance during their joint performance of Newton-John's song, "[[Hopelessly Devoted to You]]". The video was commercially successful, being certified [[RIAA certification|platinum]] in the United States by the [[Recording Industry Association of America]] (RIAA) and [[List of music recording certifications|gold]] in Brazil by the [[Associação Brasileira dos Produtores de Discos]] (ABPD).

== Background ==
Since her debut in 1990, Carey had not journeyed on a large or extensive tour. In fact, she had not embarked on a tour until her third studio effort, ''[[Music Box (Mariah Carey album)|Music Box]]'' (1993), when she performed six arena shows in the United States during the [[Music Box Tour]].<ref name="nick">{{harvnb|Nickson|1998|pp=114}}</ref> The opening night of the tour received scathing reviews, mostly aimed at Carey's deemed "obvious" stage-fright and failure to make a connection with the crowd.<ref name="nick2">{{harvnb|Nickson|1998|pp=125}}</ref> Succeeding nights were more favorably reviewed, with critics raving about Carey's vocals. [[Jon Pareles]] of ''[[The New York Times]]'' wrote regarding Carey's live vocals, "Beyond any doubt, Ms. Carey's voice is no studio concoction. Her range extends from a rich, husky alto to dog-whistle high notes; she can linger over sensual turns, growl with playful confidence, syncopate like a scat singer."<ref name=nytreview>{{Cite web|url=https://www.nytimes.com/1993/12/13/arts/review-pop-venturing-outside-the-studio-mariah-carey-proves-her-mettle.html|title=Review/Pop; Venturing Outside the Studio, Mariah Carey Proves Her Mettle|last=Pareles|first=Jon|work=[[The New York Times]]|publisher=[[The New York Times Company]]|date=1993-12-13|accessdate=2010-08-18}}</ref> However, after the strong media attention, Carey did not visit the US on her succeeding [[Daydream World Tour]] in 1996, visiting only Europe and Asia.<ref name="nick3">{{harvnb|Nickson|1998|pp=152}}</ref> The tour in contrast, received critical acclaim from critics and fans alike, as well as breaking ticket sale records.<ref name="nick3" /> Carey's three shows at Japan's largest stadium, [[Tokyo Dome]], sold out in under three hours, equaling in over 150,000 tickets, setting the record of fastest show sellouts in Japan's history.<ref name="nick4">{{harvnb|Nickson|1998|pp=153}}</ref> On the Butterfly World Tour, Carey broke the record, selling 200,000 tickets in under one hour.<ref name="nick4" /> During 1997, after the commercially and critically successful release of ''[[Butterfly (Mariah Carey album)|Butterfly]]'', Carey had not planned to tour once again, due to the long travel times and strain on her voice.<ref name="shapiro">{{harvnb|Shapiro|2001|pp=110}}</ref> However, due to overwhelming demand by fans, Carey agreed to perform in Asia once again, only extending the tour to Taiwan and Australia, as well as one last show in the United States.<ref name="shapiro" /> Rehearsals for the show began shortly after Christmas 1997, extending for a period of two weeks.<ref name="nick5">{{harvnb|Nickson|1998|pp=170}}</ref>

== Concert synopsis ==
The show began with Carey standing on a small elevated centerpiece on stage, surrounded by several long draped curtains. Carey featured three background vocalists throughout the tour, [[Trey Lorenz]], Melodie Daniels and [[Kelly Price]]. As the introduction began with "[[Emotions (Mariah Carey song)|Emotions]]", each of the curtains were slowly draped, revealing Carey atop the platform, dressed in a beige mini-dress and matching sheer blouse and stiletto heels. As she began performing "Emotions", the platform was lowered so Carey could access the other sections of the stage throughout the song's performance.  After an intimate performance with dimmed lights for "[[The Roof (Back in Time)]]", Carey was joined on-stage by a Peruvian guitar player, who played the Latin-inspired guitar melodies during her performance of "[[My All]]". Afterwards, Carey sang "Close My Eyes", the only sitting performance of the show. During the song, several male [[backup dancer]]s performed slow and ample dance routines behind Carey on a higher level of the stage.<ref name="Argenson">{{harvnb|Argenson|2010|pp=29–33}}</ref>

For the second part of the show, Carey had the second costume change of the evening, donning a long sequined black gown and semi-teased hair. For the performance of "[[Dreamlover (song)|Dreamlover]]", Carey was joined by three female back-up dancers, who mimicked her light dance routines during the song. The next song on the set-list was "[[Hero (Mariah Carey song)|Hero]]", which featured Carey alone on-stage, without any vocal back-up. After the song's performance, Carey was joined on-stage by Lorenz, who performed "I'll Be There" alongside her. Next came "[[Make It Happen (Mariah Carey song)|Make It Happen]]", a song which accompanied yet another wardrobe change for Carey. She donned a short mini-skirt, alongside a sleeveless white blouse and loose golden curls. On-stage, Carey was joined by a full church choir, all dressed in long black garments. After the song's recital, Carey performed "[[One Sweet Day]]", alongside a previously recorded video of [[Boyz II Men]] during their live performance of the song with Carey at [[Madison Square Garden]] in 1995.<ref name="Argenson" />

After completing the song, Carey changed to a pair of leg-hugging blue jeans and a tank top. Her next performance was the "[[Fantasy (Mariah Carey song)|Fantasy]]" remix, featuring [[Ol' Dirty Bastard]] on a large projection screen behind the stage, as Carey performed light chair dance-routines alongside several male dancers. The performance featured the most intricate choreography Carey performed on the tour. After a low-key performance of "Babydoll", or "Whenever You Call" in other countries, Carey was once again joined by several male dancers, as she sang "[[Honey (Mariah Carey song)|Honey]]", while re-enacting the music video during a small skit. Carey once again changed to a beige ensemble similar to her first outfit before performing her debut single, "[[Vision of Love]]". The final song on the tour was "[[Butterfly (Mariah Carey song)|Butterfly]]", which featured large stills of butterflies and flowers projected onto the large screen behind Carey. She donned a long brown sequined gown for the performance, being joined on stage once again by her trio of back-up singers. During the shows in Japan, Carey performed her holiday classic "[[All I Want for Christmas Is You]]", alongside various male and female dancers on stage who performed light dance routines alongside Carey. During the song, Carey donned a [[Santa suit]] and matching hat, while being carried on a large stage prop by the dancers.<ref name="Argenson" />

== Critical reception ==
Virtually the entire tour was an instant sell-out; the four shows at Japan's largest stadium, [[Tokyo Dome]], broke Carey's previous ticket-sales record, selling out all 200,000 tickets in under one hour.<ref name="nick5" /> Additionally, the entire Australian leg sold out within hours, leading Carey to extend the tour one more date in the United States.<ref name="nick5" /> The show at Hawaii's 50,000 capacity [[Aloha Stadium]] sold out as well, making her one of the few acts in the stadiums history to sell out the entire venue.<ref>{{cite book|url=https://books.google.com/?id=JSx8hi4VaiEC&pg=PT355&dq=mariah+carey+butterfly+tour#v=onepage&q=mariah%20carey%20butterfly%20tour&f=false|title=Events|publisher=Travel Hawaii for Smartphones and Mobile Devices – Illustrated Travel Guide|accessdate=2011-02-28|isbn=978-1-60501-043-4|date=2007-01-01}}</ref> Aside from its commercial success, fans and critics raved about the show's visuals, as well as Carey's vocal delivery.<ref name="shapiro" />

== Broadcasts and recordings ==
During the tour, several bits and performances were filmed and later edited into a VHS and DVD entitled ''[[Around the World (video)|Around the World]]''.<ref name="shapiro" /> The VHS featured performances from Tokyo Dome, Aloha Stadium as well as few other skits and scenes that were later compiled into the video.<ref name="shapiro" /> The film first begins with performances in Hawaii, where the song's recitals are cut into halves, excluding the second verses and bridge to shorten the bulk length of the video. Afterwards, Carey's performance of "My All" is shown in inter-cut scenes from Japan and Taipei. After the conclusion of the song, scenes of Carey conversing with [[Brenda K. Starr]] are shown, which eventually lead to a tribute to her at a small and intimate New York club, where Carey performs "[[I Still Believe (Brenda K. Starr song)#Mariah Carey version|I Still Believe]]". Soon after, Carey's performance in Japan with Lorenz for "I'll Be There" is shown, leading to scenes of Carey swimming with dolphins in Australia. the next title on the video is Carey's live rendition of "[[Hopelessly Devoted To You]]", where she is joined by [[Olivia Newton-John]] on stage in [[Melbourne]]. A scene of a fans gathering outside of a New York City studio is shown, following a performance of "Honey," and "Hero" at Aloha Stadium. The VHS was a commercial success, being certified [[RIAA certification|platinum]] by the [[Recording Industry Association of America]] (RIAA), denoting shipments of over 100,000 units.<ref>{{cite web|url=http://riaa.com/goldandplatinumdata.php?table=SEARCH|title=Searchable Database|publisher=[[Recording Industry Association of America]]|accessdate=2011-02-28}}</ref> The video was also certified [[List of music recording certifications|gold]] in Brazil by the [[Associação Brasileira dos Produtores de Discos]] (ABPD).<ref>{{cite web|url=http://www.abpd.org.br/certificados_interna.asp?sArtista=Mariah%20Carey|title=Certificados|language=Portuguese|publisher=[[Associação Brasileira dos Produtores de Discos]]|accessdate=2011-02-28}}</ref>

== Set list ==

# "[[Daydream (Mariah Carey album)|Looking In]]/[[Butterfly (Mariah Carey song)|Butterfly (Intro)]]"
# "[[Emotions (Mariah Carey song)|Emotions]]"
# "[[The Roof (Back in Time)|The Roof]]"
# "[[My All]]"
# "[[Close My Eyes (Mariah Carey song)|Close My Eyes]]"
# "Daydream Interlude (Fantasy Sweet Dub Mix)"
#"[[Dreamlover (song)|Dreamlover]]"
# "[[Hero (Mariah Carey song)|Hero]]"
# "[[I'll Be There (The Jackson 5 song)#Mariah Carey version|I'll Be There]]" (with [[Trey Lorenz]])
# "Make You Happy ([[Trey Lorenz]])
# "[[Make It Happen (Mariah Carey song)|Make It Happen]]"
# "[[One Sweet Day]]" {{small|(featuring pre-recorded [[Boyz II Men]])}}
# "[[Ain't Nobody]] (Band Interlude)
# "[[Fantasy (Mariah Carey song)|Fantasy (Bad Boy Remix)]]"
# "Whenever You Call"<sup>1</sup>
# "[[Breakdown (Mariah Carey song)|Breakdown]]<sup>1</sup>
# "[[Honey (Mariah Carey song)|Honey]]" {{small|(featuring pre-recorded [[Mase]])}}"
# "[[Vision of Love]]"
# "[[Butterfly (Mariah Carey song)|Butterfly]]
# "[[Without You (Badfinger song)#Mariah Carey version|Without You]]<sup>1</sup>
# "Butterfly (Outro)"
# "[[All I Want for Christmas is You]]<sup>1</sup>
<sup>1</sup> Performed on select dates

== Shows ==
{| class="wikitable" style="text-align:center;"
|+ List of concerts, showing date, city, country, venue, tickets sold, amount of available tickets and gross revenue
|-

! scope="col" style="width:12em;"| Date
! scope="col" style="width:10em;"| City
! scope="col" style="width:10em;"| Country
! scope="col" style="width:16em;"| Venue
! scope="col" style="width:12em;"| Attendance
! scope="col" style="width:10em;"| Revenue
|-

! colspan=6| Asia<ref name="shapiro" />
|-
|January 11, 1998
|rowspan=4|[[Tokyo]]
|rowspan=4|[[Japan]]
|rowspan=4|[[Tokyo Dome]]
|rowspan=5 {{n/a}}
|rowspan=5 {{n/a}}
|-
|January 14, 1998
|-
|January 17, 1998
|-
|January 20, 1998
|-
|January 24, 1998
|[[Taipei]]
|[[Taiwan]]
|[[Taipei Municipal Stadium]]
|-
! colspan=6| Australia<ref name="shapiro" />
|-
|January 31, 1998
|[[Brisbane]]
|rowspan=6|[[Australia]]
|[[Brisbane Entertainment Centre]]
|rowspan=6 {{n/a}}
|rowspan=6 {{n/a}}
|-
|February 2, 1998
|rowspan=2|[[Sydney]]
|rowspan=2|[[Sydney Entertainment Centre]]
|-
|February 6, 1998
|-
|February 10, 1998
|[[Perth, Western Australia|Perth]]
|[[Burswood Entertainment Complex]]
|-
|February 13, 1998
|rowspan=2|[[Melbourne]]
|rowspan=2|[[Rod Laver Arena]]
|-
|February 16, 1998
|-
! colspan=6| North America<ref>{{cite journal|date=March 21, 1998|title=Billboard Boxscore&nbsp;— Concert Grosses|journal=Billboard|volume=110|issue=162|pages=20|publisher=Nielsen Business Media, Inc.|issn=0006-2510|url=https://books.google.com/books?id=2Q4EAAAAMBAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false|accessdate=November 1, 2014}}</ref>
|-
|February 21, 1998
|[[Honolulu]]
|[[United States]]
|[[Aloha Stadium]]
|30,415 / 30,415
|$1,744,210
|-
|}

== Personnel ==
*[[Walter Afanasieff]] – [[Music director|director]], [[Musical keyboard|keyboard]]
*[[Dan Shea (record producer)|Dan Shea]] – keyboard
*Vernon Black – [[guitar]]
*[[Randy Jackson]] – [[Bass (sound)|bass]]
*Gigi Conway – [[Drum kit|drums]]
*Peter Michael – [[Percussion instrument|percussion]]
*Gary Cirimelli – [[Music sequencer|music sequencing]]
*[[Trey Lorenz]] – [[singing|vocals]], [[Backing vocalist|background vocals]]
*[[Kelly Price]] – background vocals
*Cheree Price – background vocals
*Melonie Daniels – background vocals
*Cindy Mizelle – background vocals

Source:<ref name="nick6">{{harvnb|Nickson|1998|pp=154}}</ref>

== References ==
{{Reflist|colwidth=30em}}

== Works cited ==
{{refbegin}}
*{{Cite book| last = Argenson
| first      = Jim
| title      = Mariah Carey Concert Tours
| year       = 2010
| publisher  = [[St. Martin's Press]]
| isbn       = 1-155-56204-6
| ref        = harv
| postscript = <!-- Bot inserted parameter. Either remove it; or change its value to "." for the cite to end in a ".", as necessary. -->
}}
*{{Cite book| last = Nickson
| first      = Chris
| title      = Mariah Carey revisited: her story
| year       = 1998
| publisher  = [[St. Martin's Press]]
| isbn       = 978-0-312-19512-0
| ref        = harv
| postscript = <!-- Bot inserted parameter. Either remove it; or change its value to "." for the cite to end in a ".", as necessary. -->
}}
*{{Cite book| last = Shapiro
| first      = Marc
| title      = Mariah Carey: The Unauthorized Biography
| publisher  = [[ECW Press]]
| year       = 2001
| isbn       = 978-1-55022-444-3
| ref        = harv
| postscript = <!-- Bot inserted parameter. Either remove it; or change its value to "." for the cite to end in a ".", as necessary. -->
}}
{{refend}}

{{Mariah Carey}}

{{Good article}}

[[Category:Mariah Carey concert tours]]
[[Category:1998 concert tours]]