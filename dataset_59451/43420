{|{{Infobox Aircraft Begin
 | name=Polikarpov SPB (D)
 | image=PolikarpovSPB(D).jpg
 | caption=A SPB (D) in a wind tunnel
}}{{Infobox Aircraft Type
 | type=[[Dive bomber]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Polikarpov]]
 | designer=
 | first flight=Spring 1940
 | introduced=
 | retired=
 | status=Cancelled
 | primary user=
 | number built=6
 | developed from= [[Polikarpov VIT-2]]
 | variants with their own articles=
}}
|}
The '''Polikarpov SPB (D)''' (''Skorostnoy Pikiruyushchy Bombardirovshchik (Dalnost)''—High Speed Dive Bomber (Distance)) was a [[Soviet Union|Soviet]] twin-engined [[dive bomber]] designed before [[World War II]]. A single prototype and five pre-production aircraft were built, but two crashed and the program was cancelled in favor of the [[Petlyakov Pe-2]].

==Development==

The SPB (D) closely resembled the [[Polikarpov VIT-2]], which had been recommended for production as a dive bomber, but the former actually was an entirely new design. It was smaller than the VIT-2 and had a [[monocoque]] fuselage. The main gears of the [[conventional undercarriage]] retracted aft into the rear of the engine nacelles and the tail wheel retracted into the rear fuselage. Two {{convert|783|kW|0|abbr=on}} liquid-cooled [[Klimov M-105]] [[V12 engine]]s were slung underneath the wings.<ref>Gunston, p. 306</ref> It retained its predecessor's prominent canopy and nose glazing, but reduced the armament to a single {{convert|7.62|mm|abbr=on}} [[ShKAS machine gun]] for the bombardier/navigator while the rear gunner had a retractable {{convert|12.7|mm|abbr=on}} [[Berezin UB]] dorsal gun and a ventral ShKAS to protect the aircraft's underside. The bomb bay could carry up to {{convert|800|kg|abbr=on}} internally and an additional {{convert|700|kg|abbr=on}} of bombs could be carried underneath the wings.<ref>Gordon, pp. 282–83</ref>

In addition to the SPB (D) prototype, five pre-production machines were ordered even before the prototype made its first flight. This flight, piloted by Boris Kudrin,<ref name=SH>Shavrov, chapter on Polikarpov's multi-role aircraft</ref> occurred safely on 18 February 1940, but on 27 April 1940 the first prototype crashed for unknown causes, killing test pilot Pavel Golovin.<ref name=SH/>

On June 2 test pilot Mikhail Lipkin barely survived when, landing with engines out, his SPB (D) clipped a parked [[Tupolev SB]]. On June 30 the second SPB (D) disintegrated in flight. Lipkin and flight engineer Bulychov, instructed to test [[wing flutter]] at extreme 600&nbsp;km/h diving speed, were killed in the crash; the aircraft actually disintegrated in horizontal flight. Investigators initially blamed the accident on Polikarpov's deputy Zhemchuzhin, who allegedly failed to fit the balance weights into the leading edges of the [[aileron]]s, causing wild flutter.<ref name=SH/> Later they also blamed Lipkin, already dead, for the alleged reckless increase of speed.<ref name=SH/> [[TsAGI]] engineers and airfield staff voiced suspicion that other factors could have been involved, but these were not examined at all.

The third prototype, piloted by Kudrin, lost horizontal [[trim tab]] in flight; the pilot managed to land the plane but refused to fly on SPB (D) prototypes anymore.<ref name=SH/><ref name=Gordon283/> On 29 July 1940 the project was cancelled; tests required for proper crash examination were not completed. The government preferred to build twin-engined dive bombers on a simplified [[Petlyakov VI-100]] platform,<ref name=SH/> – the conversion, named [[Petlyakov Pe-2]], took over the roles originally intended for the SPB (D).<ref name=Gordon283>Gordon, p. 283</ref>

==Specifications ==
{{Aircraft specs
|ref=Gunston, ''The Osprey Encyclopaedia of Russian Aircraft 1875–1995''
|prime units?=met
<!--
        General characteristics
-->
|crew=5
|capacity=
|length m=11.2
|length ft=
|length in=
|length note=
|span m=17
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=42.93
|wing area sqft=
|wing area note=
|airfoil=
|empty weight kg=4480
|empty weight lb=
|empty weight note=
|gross weight kg=6850
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Klimov M-105]]
|eng1 type=[[V12 engine|V-12]] [[Inline engine (aviation)|inline engine]]s
|eng1 kw=783
|eng1 hp=<!-- prop engines -->
|eng1 note=
|prop blade number=3
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|max speed kmh=520
|max speed mph=
|max speed kts=
|max speed note=at {{convert|4500|m|0}}
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=2200
|range miles=
|range nmi=
|range note=
|ceiling m=9000
|ceiling ft=
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|guns=<br />
* 1x 12.7&nbsp;mm [[Berezin UB]] machine gun
* 2x 7.62&nbsp;mm [[ShKAS machine gun]]s
|bombs=up to {{convert|1500|kg|0}} (800 internal, 700 external)
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
* [[Petlyakov Pe-2]]
|lists=<!-- related lists -->
}}

==References==

===Notes===
{{Reflist|2}}

===Bibliography===
{{refbegin}}
* Gordon, Yefim. ''Soviet Airpower in World War 2''. Hinckley, England: Midland Publishing, 2008 ISBN 978-1-85780-304-4
* [[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopaedia of Russian Aircraft 1875–1995''. London, Osprey, 1995 ISBN 1-85532-405-9
* [[Vadim Shavrov]] (2002, in Russian). Istoria konstrukcii samolyotov v SSSR, 1938-1950 (История конструкций самолетов в СССР 1938-1950), vol. 2. ISBN 5-217-03103-4
{{refend}}

==External links==
{{commons category|Polikarpov}}
* [http://www.ctrl-c.liu.se/MISC/RAM/spb-high.html SPB, D, D-3]

{{Polikarpov aircraft}}

{{DEFAULTSORT:Polikarpov Spb (D)}}
[[Category:Polikarpov aircraft|SPB (D)]]
[[Category:Soviet attack aircraft 1940–1949]]
[[Category:Abandoned military aircraft projects of the Soviet Union]]