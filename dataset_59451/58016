{{Infobox journal
| title = Journal of Composite Materials
| cover = [[File:Journal of Composite Materials.jpg]]
| editor = H.Thomas Hahn
| discipline = [[Materials science]]
| former_names = 
| abbreviation = J. Compos. Mater.
| publisher = [[Sage Publications]]
| country = 
| frequency = 28/year
| history = 1967-present
| openaccess = 
| license = 
| impact = 1.257 
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201581/title
| link1 = http://jcm.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jcm.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 6008630
| LCCN = 68007102
| CODEN = JCOMBI
| ISSN = 0021-9983 
| eISSN = 1530-793X
}}
The '''''Journal of Composite Materials''''' is a [[peer-reviewed]] [[scientific journal]] that covers the field of [[materials science]]. Its [[editor-in-chief]] is H.Thomas Hahn ([[UCLA]]). It was established in 1967 and is published by [[Sage Publications]] in association with the [[American Society for Composites]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.257, ranking it 9th out of 23 journals in the category "Materials Science, Composites".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Materials Science, Composites |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |work=[[Web of Science]] |postscript=.}}</ref> >

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201581/title}}
* [http://www.asc-composites.org/ American Society for Composites]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Materials science journals]]
[[Category:Publications established in 1967]]