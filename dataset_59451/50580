{{Infobox journal
| title = Archives Italiennes de Biologie
| cover = [[File:Arch ital biol 2014.jpg|200px]]
| former_name = <!-- or |former_names= -->
| abbreviation = Arch. Ital. Biol.
| discipline = [[Neuroscience]]
| editor = Pietro Pietrini, Brunello Ghelarducci
| publisher = [[University of Pisa|Pisa University Press]]
| country = Italy
| history = 1882-present
| frequency = Quarterly
| openaccess = Yes
| license = 
| impact = 1.422
| impact-year = 2013
| ISSN = 0003-9829
| eISSN =
| CODEN = AIBLAS
| JSTOR =
| LCCN = sn78006850 
| OCLC = 231043091
| website = http://www.architalbiol.org/
| link1 = http://www.architalbiol.org/aib/issue/current
| link1-name = Online access
| link2 = http://www.architalbiol.org/aib/issue/archive
| link2-name = Online archive
}}
The '''''Archives Italiennes de Biologie: A Journal of Neuroscience''''' is a  quarterly [[peer-reviewed]] [[open access]] [[scientific journal]]. The journal was established in 1882; publication was suspended between 1936 and 1957. It originally covered all aspects of [[biology]], especially [[physiology]], but now focusses on [[neuroscience]].<ref>{{cite web |url=http://architalbiol.org/aib/about/editorialPolicies#focusAndScope |title=Editorial Policies |work=Archives Italiennes de Biologie |publisher=Pisa University Press |accessdate=2015-03-18}}</ref>

==Editors-in-chief==
The following persons have been editors-in-chief of the journal:<ref>{{cite journal |title=Front matter |journal=Archives Italiennes de Biologie |date=2014 |volume=152 |issue=1 |page=i-ii |url=http://www.architalbiol.org/aib/article/view/1521/pdf}}</ref>
{{columns-list|colwidth=30em|
* C. Emery and A. Mosso (1882-1886)
* A. Mosso (1887-1904)
* A. Mosso and V. Aducco (1905-1910)
* V. Aducco (1910-1936)
* G. Moruzzi (1957-1980)
* O. Pompeiano (1981-2002)
* O. Pompeiano and P. Pietrini (2003-2007)
* P. Pietrini and B. Ghelarducci (2008-present)
}}

==Abstracting and indexing==
The journal is abstracted and indexed in the [[Science Citation Index]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-03-18}}</ref> [[Current Contents]]/Life Sciences,<ref name=ISI/> [[BIOSIS Previews]],<ref name=ISI/> [[Chemical Abstracts Service]],<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |archive-url=http://wayback.archive-it.org/all/20100211181038/http://cassi.cas.org/search.jsp |dead-url=yes |archive-date=2010-02-11 |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-03-18 }}</ref> and [[Index Medicus]]/[[MEDLINE]]/[[PubMed]].<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/0372441 |title=Archives Italiennes de Biologie |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-03-18}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.422.<ref name=WoS>{{cite book |year=2014 |chapter=Archives Italiennes de Biologie |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist|30em}}

==External links==
* {{Official website}}

[[Category:Neuroscience journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1882]]
[[Category:University of Pisa]]