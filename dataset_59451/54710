{{italic title}}
{{Unreferenced|date=January 2009}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Princess and Curdie
| image         = 
| caption       = 
| author        = [[George MacDonald]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| genre         = [[Children's literature|Children's]] [[Fantasy novel]]
| publisher     = Strahan & Co
| release_date  = 1883
| media_type    = Print
| pages         =
| wikisource    = The Princess and Curdie
| preceded_by   = [[The Princess and the Goblin]]}}
'''''The Princess and Curdie''''' is a children's classic [[fantasy novel]] by [[George MacDonald]] from late 1883.

The book is the sequel to ''[[The Princess and the Goblin]]''. The adventure continues with Princess Irene and Curdie a year or two older. They must overthrow a set of corrupt ministers who are poisoning Irene's father, the king. Irene's grandmother also reappears and gives Curdie a strange gift. A monster called Lina aids his quest.

==Synopsis==
Two years have passed since the last book, and Princess Irene and her father go to Gwyntystorm, while Curdie (a miner boy who is the friend of the Princess) stays at home with his mother and father.

As the years go by, Curdie begins to hunt for pleasure. He also slowly begins to doubt Irene's story of her great-great grandmother. One day, he shoots down a white pigeon. Curdie then remembers Irene's tale of her grandmother's pigeons, assumes the one he has shot down was one of them, and becomes aware of his folly. A light appears at the roof of the castle, and Curdie follows it. There, Curdie meets the old Princess, who appears small and withered, contrary to Irene's descriptions.

The old Princess gently tells Curdie of his wrong thinking, and he confesses. Because he now believes, the pigeon heals. He is then told to keep his bow and arrows but use them for good instead of bad things. The old Princess then tells Curdie he must go on a special quest. Before she sends him, she burns his hands in her special fire of roses. His cleansed hands now possess the ability to be able to feel the hands of his fellow men and detect what kind of person (or beast) they are on the inside. She also gives Curdie's father a special emerald to keep while Curdie is away on the quest. If Curdie is in danger, the emerald will change colour, to alert his father to go after him.

Curdie is given a monstrous yet friendly beast, Lina, as his only travelling companion. Lina saves him from many perils as they travel to Gwyntystorm. Once they reach their destination, Curdie's task becomes clear: he finds himself at the King's palace, where the King lies weak and ill in his bedchamber with his daughter Irene his only nurse.

Having sneaked in to spy on what is going on and eavesdrop on the palace servants, Curdie realises that the King's "doctor" is actually slowly poisoning him. The palace servants and courtiers have all become morally corrupt and enemies of the king. No one can be trusted and both the Princess and the King are in mortal danger, so Curdie realises why the Old Princess has sent him: he must save the king (Irene's father) from a plot to poison him and steal his kingdom by forcibly marrying his daughter Princess Irene to an evil pretender. With the aid of the old Princess, who has been disguised as a housemaid, the king, his daughter, and the kingdom are saved. Curdie and Princess Irene are later married and rule the kingdom after the king dies. However, they have no children, and after they both die, the kingdom deteriorates until one day it collapses and has never been spoken of again.

==External links==
* {{librivox book | title=The Princess and Curdie | author=George MACDONALD}}

{{Commons category}}
{{wikisourcehas}}

{{Portal |Children's literature}}

{{DEFAULTSORT:Princess and Curdie}}
[[Category:1883 novels]]
[[Category:Children's fantasy novels]]
[[Category:Novels by George MacDonald]]
[[Category:Scottish children's literature]]
[[Category:1880s fantasy novels]]
[[Category:Victorian novels]]
[[Category:Sequel novels]]
[[Category:British fantasy novels]]
[[Category:Fictional princesses]]
[[Category:Fictional kings]]