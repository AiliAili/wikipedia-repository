{{Use Australian English|date=March 2013}}
{{Use dmy dates|date=March 2013}}
{{Infobox concert tour
| concert_tour_name = Armageddon Tour
| image             = Armageddon Tour 2012 027.JPG
| image_size        = 220px
| landscape         = 
| alt               =
| image_caption     = Sebastian, [[Carmen Smith]] and [[Gary Pinto]] performing on the ''Armaggedon Tour''
| album             = ''[[Armageddon (Guy Sebastian album)|Armageddon]]''
| artist            = [[Guy Sebastian]]
| start_date        = {{Start date|df=yes|2012|06|01}}
| end_date          = {{End date|df=yes|2012|07|01}}
| gross             =
| number_of_legs    = 1
| number_of_shows   = 20 in Australia<br />20 Total
| last_tour         = '''Like It Like That Tour (USA) '''<br />(2010)
| this_tour         = '''Armageddon Tour'''<br />(2012)
| next_tour         = '''Get Along Tour'''<br />(2013)
}}

'''Armageddon Tour''' was the 9th [[concert tour]] by Australian recording artist [[Guy Sebastian]]. The tour supported his upcoming sixth studio album ''[[Armageddon (Guy Sebastian album)|Armageddon]]''. The tour visited metropolitan and regional areas in Australia.<ref>[http://www.webcitation.org/65E7aubfR?url=http%3A%2F%2Fauspop.blogspot.com.au%2F2012%2F02%2Ftouring-guy-sebastian.html TOURING : Guy Sebastian].auspOp. 5 February 2012. Archived from [http://auspop.blogspot.com.au/2012/02/touring-guy-sebastian.html the original] on 5 February 2012.</ref> In 2012, Sebastian announced that the title of his new album was to be [[Armageddon (Guy Sebastian album)|Armageddon]]. Ahead of the album's release, Sebastian wen on a national tour.  The tour took in 20 dates across the country in both metropolitan and regional centres, kicking off in [[Cessnock, New South Wales|Cessnock]] on 1 June 2012 before finishing up on the [[Sunshine Coast, Queensland|Sunshine Coast]] on 1 July 2012. Sebastian toured in places he has not toured before.

== Background ==

Sebastian toured nationally during June and July 2012. The 20 date tour had concerts in all capital cities and also in regional areas of New South Wales, Victoria and Queensland. Originally scheduled to promote the album after its release, the tour became a showcase for the new songs.<ref>[http://www.webcitation.org/6BQAIN5Gf?url=http%3A%2F%2Fwww.liveguide.com.au%2FTours%2F716363%2FGuy_Sebastian_Armageddon_Tour_2012 Guy Sebastian 'Armageddon' Tour 2012].liveguide.com.au. Archived from [http://www.liveguide.com.au/Tours/716363/Guy_Sebastian_Armageddon_Tour_2012 the original] on 14 October 2012.</ref> An extra concert at the [[Palais Theatre]], Victoria was added to film footage for the deluxe edition of the album.<ref>[http://www.webcitation.org/6BQ8Ahghe?url=http%3A%2F%2Feventful.com%2Fevents%2Fguy-sebastian-armageddon-dvd-shoot-%2FE0-001-048989485-4 Guy Sebastian Armageddon DVD Shoot].eventful.com. Archived from [http://eventful.com/events/guy-sebastian-armageddon-dvd-shoot-/E0-001-048989485-4 the original] on 14 October 2012.</ref><ref>[http://www.webcitation.org/6BIYSS32r?url=http%3A%2F%2Fwww.sanity.com.au%2Fproducts%2F2215660%2FArmageddon_Deluxe_Edition_ONLINE_COMP Armageddon: Deluxe Edition ]. Sanity Music. Archived from [http://www.sanity.com.au/products/2215660/Armageddon_Deluxe_Edition_ONLINE_COMP the original] on 9 October 2012.</ref> Sebastian performed songs from previous albums as well as new songs. He felt it was the best tour he had done, saying "I pretty much put every cent back into the tour and really stepped up the production and the lighting, it's a lot bigger and better. I'm playing some 3000-seat venues and a bunch of them are sold out, so I want to make sure I'm not compromising. I didn't want to fall short, and it has come up great, better than I thought." <ref name="webcitation">[http://www.webcitation.org/6BJMYpY50?url=http%3A%2F%2Fwww.themercury.com.au%2Farticle%2F2012%2F06%2F14%2F337005_music.html A different kind of Guy] The Mercury.  Archived from [http://www.themercury.com.au/article/2012/06/14/337005_music.html the original] on 9 October 2012</ref>

Sebastian invited Ben Burgess, a musician friend from the US, to tour with him. He performed Fiasco's rap on "Battle Scars" and was also the support act. Due to Carmen Smith's disqualification from ''The Voice'' she was able to participate on the tour. She performed backing vocals along with [[Gary Pinto]], and also the featured parts for "[[Art of Love (Guy Sebastian song)|Art of Love]]" and "[[Who's That Girl (Guy Sebastian song)|Who's That Girl]]".<ref name="webcitation" /> Smith also performed "Armageddon" with Sebastian, as at the time he had been thinking of having a featured artist on the track.<ref>[https://www.youtube.com/watch?v=x4LvAJcKrY0&list=UU5dinx1aVKJk8SNsLKTap_g&index=54&feature=plcp Armageddon Tour -"Armageddon" feat. Carmen Smith].YouTube. Retrieved 15 October 2012.</ref> There was speculation in the media that [[Nine Network|Channel Nine's]] ''The Voice'' had issues with Sebastian, when ''The Voice'' contestants including his own brother Chris were barred from attending one of the concerts. Sebastian is a judge and mentor on ''[[The X Factor (Australia)|The X Factor]]'' which is aired on the [[Seven Network]]. Channel Nine blamed it on scheduling and security problems, while the show's producers declined to comment.<ref>J.Mo and Elle(10 June 2012)[http://www.webcitation.org/6BQDrSFRn?url=http%3A%2F%2Fwww.dailytelegraph.com.au%2Fentertainment%2Finsider%2Fguy-sebastians-brother-s-show%2Fstory-e6frewt9-1226390033280 The Voice producers bar contestant Chris Sebastian from watching big bro Guy perform in concert].''The Daily Telegraph''. Archived from [http://www.dailytelegraph.com.au/entertainment/insider/guy-sebastians-brother-s-show/story-e6frewt9-1226390033280 the original] on 14 October 2012.</ref>

== Tour dates ==
{| class="wikitable" style="text-align:center;"
! width="150"| Date
! width="150"| City
! width="150"| Country
! width="200"| Venue
|-
|1 June 2012
|[[Cessnock, New South Wales|Cessnock]]
|rowspan="20"|Australia
|Performing Arts Centre
|-
|2 June 2012
|[[Sydney]]
|[[Hordern Pavilion]]
|-
|5 June 2012
|[[Tamworth, New South Wales|Tamworth]]
|Capitol Theatre
|-
|6 June 2012
|[[Dubbo]]
|Dubbo Regional Theatre & Convention Centre
|-
|8 June 2012
|[[Canberra]]
|[[Canberra Theatre]]
|-
|9 June 2012
|[[Orange, New South Wales|Orange]]
|Orange Civic Theatre
|-
|10 June 2012
|[[Wollongong]]
|Illawarra Performing Arts Centre
|-
|13 June 2012
|[[Ballarat]]
|Her Majesty's Theatre
|-
|14 June 2012
|[[Wangaratta]]
|Performing Arts Centre
|-
|16 June 2012
|[[St Kilda, Victoria|St Kilda]]
|[[Palais Theatre]]
|-
|17 June 2012
|[[Geelong]]
|[[Geelong Performing Arts Centre]]
|-
|19 June 2012
|[[Perth]]
|[[His Majesty's Theatre, Perth|His Majesty's Theatre]]
|-
|20 June 2012
|[[Mandurah]]
|Mandurah Performing Arts Centre
|-
|22 June 2012
|[[Torrensville, South Australia|Torrensville]]
|[[Thebarton Theatre]]
|-
|23 June 2012
|[[Hobart]]
|[[Derwent Entertainment Centre]]
|-
|27 June 2012
|[[Taree, New South Wales|Taree]]
|Manning Entertainment Centre
|-
|28 June 2012
|[[Port Macquarie]]
|Glasshouse Theatre
|-
|29 June 2012
|[[Brisbane]]
|[[Queensland Performing Arts Centre|QPAC Concert Hall]]
|-
|30 June 2012
|[[Gold Coast, Queensland|Gold Coast]]
|[[Jupiters Hotel and Casino|Jupiters Casino]]
|-
|1 July 2012
|[[Sunshine Coast, Queensland|Sunshine Coast]]
|Nambour Civic Centre
|}

== References ==
{{reflist}}

[[Category:2012 concert tours]]