'''Bankruptcy prediction''' is the art of predicting [[bankruptcy]] and various measures of [[financial distress]] of public firms.  It is a vast area of finance and accounting research.  The importance of the area is due in part to the relevance for creditors and investors in evaluating the likelihood that a firm may go bankrupt.

The quantity of research is also a function of the availability of data: for public firms which went bankrupt or did not, numerous accounting ratios that might indicate danger can be calculated, and numerous other potential explanatory variables are also available.  Consequently, the area is well-suited for testing of increasingly sophisticated, data-intensive forecasting approaches.

==History==
The history of bankruptcy prediction includes application of numerous statistical tools which gradually became available, and involves deepening appreciation of various pitfalls in early analyses.  Interestingly, research is still published that suffers pitfalls that have been understood for many years.

Bankruptcy prediction has been a subject of formal analysis since at least 1932, when FitzPatrick published a study of 20 pairs of firms, one failed and one surviving, matched by date, size and industry, in ''The Certified Public Accountant''.  He did not perform statistical analysis as is now common, but he thoughtfully interpreted the ratios and trends in the ratios.  His interpretation was effectively a complex, multiple variable analysis.

In 1967, [[William Beaver]] applied [[t-tests]] to evaluate the importance of individual accounting ratios within a similar pair-matched sample.

In 1968, in the first formal multiple variable analysis, [[Edward I. Altman]] applied [[multiple discriminant analysis]] within a pair-matched sample.  One of the most prominent early models of bankruptcy prediction is the [[Z-Score Financial Analysis Tool]], which is still applied today.

In 1980, [[James Ohlson]] applied [[logit regression]] in a much larger sample that did not involve pair-matching.

==Modern methods==
{{unreferenced section|date=February 2014}}
Survival methods are now applied.

Option valuation approaches involving stock price variability have been developed. Under structural models,<ref>{{Cite web|url=https://www.fields.utoronto.ca/programs/scientific/09-10/finance/courses/hurdnotes2.pdf|title=Structural Modes of Credit Risk|last=|first=|date=|website=|publisher=|access-date=}}</ref> a default event is deemed to occur for a firm when its assets reach a sufficiently low level compared to its liabilities.

Neural network models and other sophisticated models have been tested on bankruptcy prediction.

Modern methods applied by business information companies surpass the annual accounts content and also consider current events like age, judgements, bad press, payment incidents and payment experiences from creditors.

==Comparison of differing approaches ==

The latest research within the field of Bankruptcy and Insolvency Prediction compares various differing approaches, modelling techniques, and individual models to ascertain whether any one technique is superior to its counterparts.

Jackson and Wood (2013) provides an excellent discussion of the literature to date, including an empirical evaluation of 15 popular models from the existing literature. These models range from the univariate models of Beaver through the multidimensional models of Altman and Ohlson, and continuing to more recent techniques which include option valuation approaches. They find that models based on market data  - such as an option valuation approach - outperform those earlier models which rely heavily on accounting numbers.<ref>{{cite journal | url = http://www.sciencedirect.com/science/article/pii/S0890838913000498 | doi=10.1016/j.bar.2013.06.009 | volume=45 | title=The performance of insolvency prediction and credit risk models in the UK: A comparative study | journal=The British Accounting Review | pages=183–202}}</ref>

Zhang, Wang, and Ji (2013) proposed a novel rule-based system to solve bankruptcy prediction problem. The whole procedure consists of the following four stages: first, sequential forward selection was used to extract the most important features; second, a rule-based model was chosen to fit the given dataset since it can present physical meaning; third, a genetic [[ant colony algorithm]] (GACA) was introduced; the
fitness scaling strategy and the chaotic operator were incorporated with GACA, forming a new algorithm—fitness-scaling chaotic GACA (FSCGACA), which was used to seek the optimal parameters of the rule-based model; and finally, the stratified K-fold cross-validation technique was used to enhance the generalization of the model.<ref>{{cite journal|last=Zhang|first=Yudong|author2=Shuihua Wang |author3=Genlin Ji |title=A Rule-Based Model for Bankruptcy Prediction Based on an Improved Genetic Ant Colony Algorithm|journal=Mathematical Problems in Engineering|date=2013|volume=2013|doi=10.1155/2013/753251|url=http://downloads.hindawi.com/journals/mpe/2013/753251.pdf|page=753251}}</ref>

==References==
{{More footnotes|date=February 2014}}
{{reflist}}
*[[FitzPatrick 1932]]
*Beaver 1966. Financial ratios predictors of failure.  ''[[Journal of Accounting Research]], 4 (Supplement), p.&nbsp;71-111.
*Beaver 1968
*{{cite journal | last1 = Altman | first1 = Edward I | year = 1968 | title = Financial ratios, discriminant analysis and the prediction of corporate bankruptcy | url = | journal = [[Journal of Finance]] | volume = 23 | issue = 4| pages = 589–609 }}
*Ohlson, James. 1980.
*{{cite journal | last1 = Balcaen | first1 = Sofie | last2 = Ooghe | first2 = Hubert | year = 2006 | title = 35 years of studies on business failure: an overview of the classic statistical methodologies and their related problems | url = | journal = [[British Accounting Review]] | volume = 38 | issue = | pages = 63–93 }}
*Zmijewski, Mark E. 1984. "Methodological issues related to the estimation of financial distress prediction models". ''[[Journal of Accounting Research]]'' 22 (Supplement), p.&nbsp;59-86.
*{{cite journal | last1 = Jackson | first1 = Richard | last2 = Wood | first2 = Anthony | year = 2013 | title = The Performance of Insolvency Prediction and Credit Risk Models in the UK: A Comparative Study | url = | journal = The British Accounting Review | volume = 45 | issue = 3| pages = 183–202 }}

See also "Corporate Bankruptcy: Assessment, Analysis and Prediction of Financial Distress, Insolvency, and Failure" by Konstantin A. Danilov, available at [http://www.ssrn.com/abstract=2467580]

==External links==

[[Category:Bankruptcy]]
[[Category:Credit scoring]]