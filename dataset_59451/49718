{{Other uses|Le Rêve (disambiguation)}}

{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Le Rêve
| title_orig    = 
| translator    = 
| image         = <!--prefer 1st edition-->
| caption = 
| author        = [[Émile Zola]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Les Rougon-Macquart]]
| genre         = [[Novel]]
| publisher     = Charpentier (book form)
| release_date  = 1888 (book form)
| english_release_date =
| media_type    = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| preceded_by   = [[La Terre]]
| followed_by   = [[La Bête Humaine]]
}}

'''''Le rêve''''' (''The Dream'') is the sixteenth novel in the [[Les Rougon-Macquart|Rougon-Macquart]] series by [[Émile Zola]]. It is about an orphan girl who falls in love with a nobleman, and is set in the years 1860&ndash;69.

The novel was published by Charpentier in October 1888 and translated into English by Eliza E. Chase as ''The Dream'' in 1893 (reprinted in 2005). Other recent translations are by [[Michael Glencross]] (Peter Owen 2005) and Andrew Brown ([[Hesperus Press]] 2005).

==Plot summary==
''Le rêve'' is a simple tale of the orphan Angélique Marie (b. 1851), adopted by a couple of embroiderers, the Huberts, whose marriage is blighted by a childlessness which they attribute to a curse uttered by Mme Hubert's mother on her deathbed. Angélique is enthralled by the tales of the [[saint]]s and [[martyr]]s — particularly [[Saint Agnes]] and [[Saint George]] — as told in the ''[[Golden Legend]]'' of [[Jacobus de Voragine]]. Her dream is to be saved by a handsome prince and to live happily ever after, in the same way the virgin martyrs have their faiths tested on earth before being rescued and married to [[Jesus]] in heaven.

Her dream is realized when she falls in love with Félicien d'Hautecœur, the last in an old family of knights, heroes, and nobles in the service of [[Christ]] and of France. His father, the present Monseigneur, objects to their marrying for reasons of his own. (Before entering the Church he had married for love a woman much younger than himself; when she died giving birth to Félicien, he sent the child away and took holy orders.) Angélique falls ill and pines away. Won over by her virtue and innocence, the Monseigneur finally relents and the lovers are married; but Angélique dies on the steps of the cathedral as she kisses her husband for the first time. Her death, however, is a happy one: her innocence has freed the Huberts and the Monseigneur from their curses.

==Relation to the other Rougon-Macquart novels==
Zola's plan for the Rougon-Macquart novels was to show how [[heredity]] and environment worked on members of one family over the course of the [[Second French Empire]]. All of the descendants of Adelaïde Fouque (Tante Dide), Angélique's great-grandmother, demonstrate what today would be called [[obsessive-compulsive disorder|obsessive-compulsive]] behaviors. Angélique is obsessed with the lives of the saints and with her dream of a princely marriage.

Furthermore, Angélique has a temper and experiences serious mood swings, becoming as passionate as any one of her relatives. Zola strongly implies that, without the upbringing by her adoptive parents and the influence of the cathedral and ''The Golden Legend'', Angélique could easily have been fallen prey to her passions and ended up as a [[prostitution|prostitute]] (like her [[cousin]] [[Nana (novel)|Nana]]).

In ''[[Le Docteur Pascal|Le docteur Pascal]]'', Zola describes Angélique as being a blend of the characteristics of her parents to such a degree that no trace of them shows up in the child. Angélique's mother is Sidonie Rougon, who plays a significant (though brief) role in ''[[La Curée|La curée]]'' and appears briefly in ''[[L'Œuvre|L'œuvre]]''. (Angélique's father is unknown.) Sidonie is unfeeling and nearly inhuman, a cold, dry woman incapable of love. She is a professional procuress, involved in every shady calling, a seller of "anything and everything."

In ''Le docteur Pascal'' (set in 1872), it is revealed that Sidonie has become the austere financial manager of a [[home for unwed mothers]].

==Adaptations==
The novel was dramatized as an [[opera]] in four acts composed by [[Alfred Bruneau]], produced June 18, 1891, at the [[Opéra-Comique]] to a libretto by [[Louis Gallet]].

It was also adapted as two French films, both called ''Le Rêve'' and both directed by [[Jacques de Baroncelli]]: one in 1921 (a silent film) and one in 1931.

==Sources==
*Brown, F. (1995). ''Zola: A life.'' New York: Farrar, Straus & Giroux.
*Zola, E. ''Le doctor Pascal'', translated as ''Doctor Pascal'' by E.A. Vizetelly (1893).
*Zola, E. ''Le rêve'', translated as ''The Dream'' by Andrew Brown ([[Hesperus Press]] 2005).

== External links ==
{{Gutenberg|no=17533|name=Le Rêve}} (French)
{{Gutenberg|no=9499|name=The Dream}} (English)
* [http://gallica.bnf.fr/zola/RecepAdap/Opera1.htm Site about the opera ''Le rêve'' by Bruneau] (in French)
*{{IMDb title|id=0198990|title=Le Rêve}} (1921) ([[silent film|silent]]) (French)
*{{IMDb title|id=0196062|title=Le Rêve}} (1931) (French)
* {{fr}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/emile-zola-le-reve.html/ Le Rêve, audio version] [[Image:Speaker Icon.svg|20px]]

{{Les Rougon-Macquart}}

{{Authority control}}

{{DEFAULTSORT:Reve, Le}}
[[Category:1888 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:Novels about orphans]]
[[Category:1860s in fiction]]
[[Category:French novels adapted into films]]