<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Universal American Flea Ship
 | image=Americanfleaship.JPG
 | caption=Universal American Flea Ship
}}{{Infobox Aircraft Type
 | type=Homebuilt aircraft
 | national origin=United States of America
 | manufacturer=Universal Aircraft<ref>{{cite journal|magazine=Popular Mechanics|date=August 1940}}</ref>
 | designer=[[Henri Mignet]], [[Lillian Holden]].
 | first flight=
 | introduced=1939
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=[[Mignet Pou-du-Ciel]]
 | variants with their own articles=
}}
|}

The '''American Flea Ship''' ('''Flea Triplane''') is a homebuilt triplane design of the early 1930s.<ref>{{cite web|title=American Flea Ship|url=http://www.ww2aircraft.net/forum/personal-gallery/sharing-some-interesting-pics-tam-musem-22942-2.html|accessdate=7 Jan 2011}}</ref> It is one of the first examples of a female designed and built aircraft. One example is displayed at the [[Wings of a Dream Museum]].<ref>{{cite news|title=A Brazilian dream becomes reality|url=|accessdate=6 Jan 2011}}</ref>

==Development==
The American Flea Ship is a homebuilt triplane variant of the French-designed [[Mignet Pou-du-Ciel|Mignet Flea]] licensed by American Mignet Aircraft, and later Universal Aircraft company of [[Ft Worth, Texas]]. It is also known as the Flea Triplane. The aircraft was given away by Universal as a marketing effort when a Universal motor was purchased to power it. Later, the fuselage sold for $695. The kit version of the aircraft was designed by [[Lillian Holden]].<ref>{{cite web|title=American Flea|url=http://aerofiles.com/_al.html|accessdate=7 Jan 2011}}</ref> [[Ace Aircraft Manufacturing Company]] maintains the rights to the American Flea Ship and [[Heath Parasol]].<ref>{{cite book|title=Janes All the Worlds Aircraft|author=Frederick Thomas Jane}}</ref>

==Design==
The Triplane aircraft does not have ailerons, and uses variable incidence wings for roll control.<ref>{{cite book|title=The Aeroplane, Volume 92}}</ref>
<!-- ==Operational history== -->

==Variants==
The aircraft has been referenced under many names including; 
*American Flea Ship
*Mignet HM-20
*HM-20 Flying Flea
*Flea Triplane
*TC-1 Flea (1954)<ref>{{cite book|title=The fighting triplanes|author=Evan Hadingham}}</ref>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications American Flea Ship ==
{{Aircraft specs
|ref=Aerofiles<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=1
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=20
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=460
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Universal
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=40<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=Later models used a 65hp Continental
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=100
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}
{{commons category|Universal American Flea Ship}}

==References==
{{refbegin}}
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:Homebuilt aircraft]]
[[Category:Triplanes]]