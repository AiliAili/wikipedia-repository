{{Use dmy dates|date=March 2015}}
{{Infobox journal
| title = Dilbilim Araştırmaları
| cover =[[File:Dilbilim Arastirmalari 1990.jpg|175px]]
| editor = Deniz Zeyrek
| discipline = [[Linguistics]]
| language = English, [[Turkish language|Turkish]]
| abbreviation = Dilbilim Araşt.
| publisher = [[Boğaziçi University|Boğaziçi University Press]]
| country = [[Turkey]]
| frequency = Biannually
| history = 1990-present
| openaccess =
| impact =
| impact-year =
| website = http://dad.boun.edu.tr/index.php/DAD
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 29482153
| LCCN = 92968471
| ISSN = 1300-8552
| eISSN =
}}
'''''Dilbilim Araştırmaları''''' (''DAD''; English: ''Journal of Linguistic Research'') is a [[Peer review|peer-reviewed]] [[academic journal]] published by [[Boğaziçi University|Boğaziçi University Press]]. The journal covers research on all aspects of [[linguistics]] concerning [[Turkish language]].

==History and profile==
''Dilbilim Araştırmaları'' was established in 1990 by linguists in [[Turkey]].<ref name=dad>{{cite web |title=Ana Sayfa |url=http://dilbilimarastirmalari.com/ |work=Dilbilim Araştırmaları |accessdate=29 October 2013 |language=Turkish}}</ref> The journal has been published by different publishing houses, including Hitit Publishing House and Bizim Büro Publishers.<ref>{{cite web|title=Dilbilim Araştırmaları|url=http://www.worldcat.org/title/dilbilim-arastrmalar/oclc/608605927|work=World Cat|accessdate=29 October 2013}}</ref> The current publisher is Boğaziçi University Press.<ref name=dad/>

Until 2008 one volume was published each year.<ref name=net>{{cite web|title=Dilbilim Araştırmaları 2006|url=http://www.netkitap.com/kitap-dilbilim-arastirmalari-2006-kolektif-bogazici-universitesi-yayinevi.htm|work=Net Kitap|accessdate=3 November 2013|language=Turkish}}</ref> It has been published biannually, specifically in May and in September, since 2009.<ref name=dad/><ref name=asos>{{cite web|title=Dilbilim Araştırmaları|url=http://asosindex.com/journal-view?id=153|work=Asos Index|accessdate=29 October 2013}}</ref> Both English and [[Turkish language|Turkish]] articles are published.<ref name=max>{{cite web|title=Dilbilim Araştırmaları|url=http://www.arastirmax.com/dergi/dilbilim-ara%C5%9Ft%C4%B1rmalar%C4%B1|work=Arastirmax|accessdate=29 October 2013}}</ref>

The founding [[editors-in-chief]] were Gül Durmuşoğlu, Kamile İmer, Ahmet Kocaman, and [[A. Sumru Özsoy]]. Durmuşoğlu served in the post from 1990 to 1993, İmer, Kocaman, and Özsoy from 1990 to 2011. The current editor of the journal has been Deniz Zeyrek, professor of linguistics at [[Middle East Technical University]], since 2011.<ref>{{cite web|title=Yayın Kurulu |url=http://dilbilimarastirmalari.com/sayfalar/61 |work=Dilbilim Araştırmaları |accessdate=29 October 2013 |language=Turkish |deadurl=yes |archiveurl=https://web.archive.org/web/20130919024415/http://dilbilimarastirmalari.com/sayfalar/61 |archivedate=19 September 2013 |df=dmy }}</ref><ref>{{cite web|title=Deniz Zeyrek|url=http://ii.metu.edu.tr/staff/dezeyrek|work=Middle East Technical University|accessdate=17 November 2013}}</ref>  

The [[University of California]] digitized some issues of the journal.<ref>{{cite web|title=Bibliographic information|url=https://books.google.com/books/about/Dilbilim_ara%C5%9Ft%C4%B1rmalar%C4%B1.html?id=vDsFAQAAIAAJ&redir_esc=y|work=Google Books|accessdate=3 November 2013}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in TÜBİTAK-ULAKBİM SBVT, [[Linguistic Bibliography]],<ref>{{cite web|title=Periodicals|url=http://bibliographies.brillonline.com/pages/lb/periodicals|work=BrillOnline|accessdate=29 October 2013}}</ref> [[MLA International Bibliography]]<ref>{{cite web|title=Indexing info |url=http://dilbilimarastirmalari.com/sayfalar/57 |work=Dilbilim Araştırmaları |accessdate=29 October 2013 |language=Turkish |deadurl=yes |archiveurl=https://web.archive.org/web/20130919011523/http://dilbilimarastirmalari.com/sayfalar/57 |archivedate=19 September 2013 |df=dmy }}</ref> and [[Ulrich's Periodicals Directory]].<ref name=asos/>

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://dad.boun.edu.tr/index.php/DAD}}

{{DEFAULTSORT:Dilbilim Arastirmalari}}
[[Category:1990 establishments in Turkey]]
[[Category:Academic journals published by university presses]]
[[Category:Annual journals]]
[[Category:Biannual journals]]
[[Category:Linguistics journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1990]]