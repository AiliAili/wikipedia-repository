{{Infobox Journal
| title = New Phytologist
| cover = [[File:NewPhytologistDec4.gif]]
| abbreviation = New Phytol
| discipline = [[Botany|Plant Science]]
| language = [[English language|English]]
| website = http://onlinelibrary.wiley.com/journal/10.1111/%28ISSN%291469-8137
| publisher = [[Wiley-Blackwell (publisher)|Wiley-Blackwell]]
| country = [[United Kingdom|UK]]
| editor = Alistair M. Hetherington
| history = 1902-present 
| frequency = 16/year
| impact = 7.672
| impact-year = 2014
| JSTOR = 0028646x
| ISSN = 0028-646X
| eISSN = 1469-8137
| OCLC = 1759937
| CODEN = NEPHAV
| LCCN = 07000035 
}}
'''''New Phytologist''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published on behalf of the New Phytologist Trust by [[Wiley-Blackwell (publisher)|Wiley-Blackwell]]. It was founded in 1902 by botanist [[Arthur Tansley]], who served as editor until 1931.<ref>{{Cite journal|last1=Godwin|first1=Harry|authorlink1=Harry Godwin|title=Arthur George Tansley. 1871–1955|journal=[[Biographical Memoirs of Fellows of the Royal Society]]|volume=3|pages=227–226|year=1957|jstor=769363}}</ref>

== Topics covered == 
''New Phytologist'' covers all aspects of [[Botany|plant science]], with topics ranging from intracellular processes through to global environmental change, including:

* [[Physiology]] and [[Developmental biology|development]]: intra/inter-cellular signalling, long-distance signalling, physiology, development, [[eco-devo]] - [[phenotypic plasticity]], transport, [[biochemistry]].
* [[Natural environment|Environment]]: [[global change]] and Earth system functioning, environmental stress, ecophysiology, plant–soil interactions, [[heavy metals]].
* [[Interaction]]: multitrophic systems, [[mycorrhiza]]s and [[pathogen]]s, [[Fungi|fungal]] [[genomics]], nitrogen-fixing [[Symbiosis|symbioses]].
* [[Evolution]]: [[molecular evolution]], [[population genetics]], mating systems, [[phylogenetics]], [[speciation]], plant-enemy coevolution.

== Article categories ==
The journal publishes articles in the following categories:

* Original research articles
* Research reviews
* Commentaries
* Letters
* Meeting reports
* Tansley reviews
* Tansley insights

== References ==
{{Reflist}}

== External links ==
* [http://www.newphytologist.com Journal Homepage]
* [http://www.newphytologist.org New Phytologist Trust]

[[Category:Publications established in 1902]]
[[Category:Botany journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Academic journals associated with non-profit organizations]]


{{botany-journal-stub}}