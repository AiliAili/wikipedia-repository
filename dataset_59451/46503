{{Infobox Weapon
|name= Beretta 70 Series
|image=[[Image:Beretta 70 7.65.jpg|300px]]
|caption= The Beretta 70 in [[.32 ACP]] ([[7.65mm Browning]])
|origin=[[Italy]]
|type=[[Semi-automatic pistol]]
<!-- Type selection -->
|is_ranged=yes
<!-- Service history -->
|service=
|used_by=
|wars=
<!-- Production history -->
|designer=
|design_date=
|manufacturer=[[Beretta]]
|production_date=1958-1985
|number=
|variants=
<!-- General specifications -->
|weight=
* {{convert|660|g|oz}} ''(70)''
* {{convert|480|g|oz}} ''(71)''
* {{convert|535|g|oz}} ''(72)''
* {{convert|550|g|oz}} ''(73)''
* {{convert|570|g|oz}} ''(74, 75)''
* {{convert|930|g|oz}} ''(76)''
* {{convert|990|g|oz}} ''(100)''
|length=
* {{convert|165|mm|in}} ''(70, 71)''
* {{convert|225|mm|in}} ''(72)''
* {{convert|235|mm|in}} ''(73, 74, 75)''
* {{convert|220|mm|in}} ''(70T)''
|part_length=
* {{convert|90|mm|in}} ''(70, 71, 72)''
* {{convert|150|mm|in}} ''(70T, 72, 73, 74, 75)''
* {{convert|130|mm|in}} ''(76)''
|height=
* {{convert|123|mm|in}} ''(70, 71)''
* {{convert|136|mm|in}} ''(72, 73, 74, 75)''

<!-- Ranged weapon specifications -->
|cartridge=[[.32 ACP]], [[.380 ACP]], or [[.22 LR]]
|caliber=
|action= [[Single Action]]
|rate=
|velocity=
|range=
|max_range=
|feed= 7, 8, 9, or 10 round detachable [[Magazine (firearm)|magazine]]
|sights=Fixed or Adjustable
}}

The '''Beretta 70''' was a [[semi-automatic pistol|semi-automatic]] [[pistol]] series designed and produced by [[Beretta]] of [[Italy]],<ref name="imfdb">{{cite web|url=http://www.imfdb.org/wiki/Beretta_Model_70 |title=Beretta Model 70 - Internet Movie Firearms Database - Guns in Movies, TV and Video Games |publisher=Imfdb.org |date= |accessdate=2014-05-31}}</ref> which was based on the earlier [[.32 ACP|7.65mm]] [[Beretta M1935]] pistol.<ref name="berettaweb">{{cite web|url=http://www.berettaweb.com/mod_70/mod_70_P1.htm |title=Beretta Web 70 Series |publisher=Berettaweb.com |date= |accessdate=2014-05-31}}</ref>  Some pistols in this series were also marketed as the Falcon, New Puma, New Sable, Jaguar, and Cougar<ref name=gundigest /><ref name=jbwood /> (not to be confused with the later [[Beretta 8000]], which was also marketed as Cougar). The gun is notable for its appearances in film, and is also the first compact Beretta pistol to feature several improvements commonly found in Beretta pistols for the rest of the century.<ref name="berettaweb" />

== Improvements over previous Beretta pistols ==
The 70 Series improved upon the [[Beretta M1934|M1934]]/[[Beretta M1935|M1935]] (and the later .22 LR offering of the same as the [[Beretta 948|Model 948]]) in several ways.  The swivel-lever safety of the [[Beretta M1935|M1934/35]] was too difficult to operate one handed, so the 70 Series started out with the same cross-bolt safety as the [[Beretta M1951|M1951]],<ref name=jbwood /> but this safety was also difficult to operate with a single hand, and could also accidentally be switched on or off depending on the user's grip.  After only a few years, the cross bolt safety was replaced with the thumb-activated, frame-mounted lever safety found on most Beretta pistols in the latter half of the 20th century, and indeed most [[Single-action#Single-action|single-action]] or [[DA/SA|traditional double-action]] [[semi-automatic pistol|semi-automatic]] pistols in production today, until the relocation of the safety to the slide with the [[Beretta 92|Model 92S]] in 1983.<ref name=jbwood />

As the replacement for the M1934/5 in Beretta's compact/medium pistol line, the Model 70 integrated the M1951's takedown lever, guide rod system, slide stop, and magazine release button in the lower part of the grip.  The M1934/5 would hold open on empty due to the [[Box magazine|magazine follower]] blocking the slide, but removing the magazine would cause the slide to be released unless its safety was subsequently engaged (which would lock the gun open until switched back to Fire mode); this process was slow and cumbersome, which is why the slide stop improvement was implemented in the M1951 and later models of service pistols.<ref name="Ayoob2011">{{cite book|author=Massad Ayoob|authorlink=Massad Ayoob|title=Gun Digest Book of Beretta Pistols: Function Accuracy Performance|url=https://books.google.com/books?id=AD5Kg35n9PkC&pg=PA43|date=28 February 2011|publisher=Gun Digest Books|location=Iola, Wisconsin|isbn=1-4402-2424-2|pages=43–44}}</ref>  The slide stop changed partway through the production run of the pistol from the same "pedal" style release as the M1951 to a round button at the end of the lever.  A softer, longer curve that tapered up to the front of the gun was also added to make the trigger guard more sleek.  Near the end of its production, the same thumb shelf of the 76W's left grip panel was added to the 70S.<ref name=jbwood />

The magazines retain the spur at the front of the floor plate, common to both the M1934/5 and M1951, as is common for many pistols with a European-style magazine release (a hinged latch in the bottom of the grip against the magazine's floor plate), and also some compact pistols as a means of extending the grip surface while keeping the profile of the pistol small.

== Models ==

=== 71 ===
The Model 71 is the same as the 70, except it was only available in lightweight alloy (not steel), which reduced the weight of the pistol by about 200 grams, and was chambered in .22 LR.  The 71 (and 72) was marketed as the Jaguar.<ref name=gundigest />

=== 72 ===
The Model 72 was the same as Models 71 and 75, except it was supplied with two barrels: one short barrel for self-defense, and one long barrel for target shooting.<ref name="Ayoob2011"/> This model was also marketed as Jaguar.<ref name=gundigest />

=== 73, 74, 75 ===
Models 73, 74, and 75 were target models with a longer barrel than the others.  Model 74 featured adjustable sights mounted on the barrel.<ref name=jbwood />  Models 73 and 74 are taller, full-sized frames, whereas the 75 retains the compact frame of the other models.<ref name="Ayoob2011"/>

=== 76 ===
The Model 76 is much more distinctive, with a barrel shroud integrated with the frame, similar to the later Beretta 87 Target, with a rail for mounting optics.  Produced from 1971 to 1985, it features the same full-size frame as models 73 and 74.  Its sights are adjustable, but the rear sight is mounted to the barrel shroud instead of the back of the barrel as on the 74, giving it a longer sight radius and greater accuracy.  The Model 76 was available with either plastic or wooden grips, and respectively suffixed as 76P or 76W.<ref name="Ayoob2011"/>

=== 100, 101, 102 ===
The 100 series pistols were identical to the 70 series, but were briefly marketed in the United States as the such in the late 1960s.  The Model 100 was a .32 ACP with the longer, 150mm barrel and the grip frame of the .22 caliber model; apart from its weight of nearly a kilogram, the pistol's remaining attributes are identical to the Model 74.  Model 101 was identical to the Model 71, and Model 102 was identical to Model 76.  This numbering scheme was dropped in the mid 1970s, when the original Model 70 designation was resumed.<ref name=jbwood />

{|class="wikitable" style="text-align:center;"
|+ Models of the 70 Series
!|Model
!|Caliber
!| Barrel Length
!| Magazine Capacity
!| Rear Sight
!| Front Sight
!| Weight
|-
| rowspan="2" | 70
| rowspan="2" | .32 ACP
| rowspan="2" | {{convert|90|mm|in}}
| rowspan="2" | 8
| rowspan="2" | Fixed
| rowspan="2" | Fixed
| {{convert|660|g|oz}}
|-
| {{convert|520|g|oz}}
|-
| 70 S
| .380 ACP
| {{convert|90|mm|in}}
| 7
| Fixed
| Fixed
| {{convert|675|g|oz}}
|-
| 70 S
| .22 Long Rifle
| {{convert|90|mm|in}}
| 8
| Adjustable
| Fixed
| {{convert|635|g|oz}}
|-
| 71
| .22 Long Rifle
| {{convert|90|mm|in}}
| 8
| Fixed
| Fixed
| {{convert|480|g|oz}}
|-
| rowspan="2" | 72
| rowspan="2" | .22 Long Rifle
| {{convert|150|mm|in}}
| rowspan="2" | 8
| rowspan="2" | Fixed
| rowspan="2" | Fixed
| rowspan="2" | {{convert|535|g|oz}}
|-
| {{convert|90|mm|in}}
|-
| 73
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Fixed
| Fixed
| {{convert|550|g|oz}}
|-
| 74
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Adjustable
| Fixed
| {{convert|570|g|oz}}
|-
| 75
| .22 Long Rifle
| {{convert|150|mm|in}}
| 8
| Fixed
| Fixed
| {{convert|525|g|oz}}
|-
| 76
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Adjustable<ref name=76note>Sights affixed to detachable counterweight</ref>
| Replaceable<ref name=76note />
| {{convert|930|g|oz}}
|-
| 76S
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Adjustable<ref name=76note />
| Replaceable<ref name=76note />
| ≈{{convert|800|g|oz}}
|-
| 100<ref name=jbwood />
| .32 ACP
| {{convert|150|mm|in}}
| 9
| Adjustable
| Fixed
| {{convert|990|g|oz}}
|-
| 101
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Adjustable
| Fixed
| -
|-
| 102
| .22 Long Rifle
| {{convert|150|mm|in}}
| 10
| Adjustable<ref name=76note />
| Replaceable<ref name=76note />
| -
|}

== Versions ==
No special designation was made for it, but the Model 70 was available with a threaded barrel and detachable [[suppressor]].

=== Base versions (no letter suffix) ===
The Model 70 series, without suffix, was manufactured from 1958-1968.  At some point in this period, the frame-mounted thumb safety was adopted, replacing the older cross-bolt safety of the [[Beretta M1951|Model 951]].<ref name=gundigest>{{cite book|last=Ramage|first=Ken|title=The Gun Digest Buyers' Guide to Guns|year=2008|publisher=Krause Publications|isbn=1440224331|url=https://books.google.com/books?id=wAFGGOZRi2kC&pg=PT560&lpg=PT560&dq=beretta+70+1985#v=onepage&q=beretta%2070%201985&f=false}}</ref>

=== S version ===
The S version was introduced in 1977,<ref name=gundigest /> included a new magazine safety,<ref name=gundigest /><ref name=jbwood>{{cite book|last=Wood|first=JB|title=Beretta Automatic Pistols: The Collector's And Shooter's Comprehensive Guide|year=1985|publisher=Stackpole Books|location=United States|isbn=0811704254|pages=122–139|chapter=10 - Model 70 Series Pistols}}</ref> and marked the adoption of caliber .380 for the Model 70, to replace .32 ACP.  The .22 caliber guns had adjustable rear sights, but the .380 did not.  Production ceased in 1985.<ref name=gundigest />

=== T version ===
The T version was produced from 1969 to 1975, and featured a 9-round magazine (+1 from base models) and lengthened 6" (152mm) target barrel.<ref name=gundigest />

== Users ==
* {{ISR}} - Sky Marshals, [[Mossad]],<ref name="Ayoob2011"/> [[Sayeret Matkal]]<ref>{{cite web|last=Jacobellis |first=Nick |url=http://www.tactical-life.com/magazines/tactical-weapons/israeli-mossad-22-lrs/ |title=Israeli Mossad .22 LRS: The Reliable Pistols of the Mossad |publisher=Tactical-life.com |date=2009-05-04 |accessdate=2014-05-31}}</ref>
* {{ITA}} - Italian police

== Gallery ==
<gallery>
File:Beretta_mod_76.jpg|Model 76 with wooden grips
File:Beretta 70.jpg|Model 70 with newer thumb-operated manual safety, flat grips, and early model magazine release button
File:Beretta 70S.jpg|Model 70S with thumb-rest grip and late model magazine release button
</gallery>

== External links ==
* http://www.exordinanza.net/schede/Beretta70.htm (Italian)

== References ==
{{reflist}}

{{commons category|Beretta 70}}
{{Beretta firearms}}
{{.38 Caliber}}

[[Category:.22 LR pistols]]
[[Category:.32 ACP firearms]]
[[Category:.380 ACP firearms]]
[[Category:Beretta pistols|70 Series]]