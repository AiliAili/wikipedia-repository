{{good article}}
{{italic title}}
{{Infobox book
| name             = ''The Art of Cookery, Made Plain and Easy''
| image            = File:Art of Cookery frontispiece.jpg
| image_size       = 300px
| caption          = Frontispiece and title page in an early posthumous edition, published by L. Wangford, c. 1777
| author           = "By a LADY"<br>([[Hannah Glasse]])
| illustrator      = 
| country          = England
| subject          = English cooking
| genre            = [[cookbook]]
| publisher        = Hannah Glasse
| pub_date         = 1747
| pages            = 384
| wikisource       = <!--under development-->
}}

'''''The Art of Cookery made Plain and Easy''''' is a [[cookbook]] by [[Hannah Glasse]] (1708–1770) first published in 1747. It was a best seller for a century after its first publication, dominating the English-speaking market and making Glasse one of the most famous cookbook authors of her time. The book ran through at least 40 editions, many of them pirated. It was published in Dublin from 1748, and in America from 1805.

Glasse emphasised in her note "To the Reader" that she used plain language so that servants would be able to understand it.

The 1751 edition was the first book to mention [[trifle]] with [[gelatin dessert|jelly]] as an ingredient; the 1758 edition gave the first mention of "[[hamburger|Hamburgh sausages]]" and [[piccalilli]], while the 1774 edition of the book included one of the first recipes in English for an Indian-style [[curry]]. Glasse expressed criticism of French influence, but included dishes with French names and French influence in the book. Other recipes use imported ingredients such as [[cocoa bean|cocoa]], [[cinnamon]], [[nutmeg]], [[pistachios]] and even [[musk]].

The book was popular in the [[Thirteen Colonies]] of America, and its appeal survived the [[American War of Independence]], copies being owned by [[Benjamin Franklin]], [[Thomas Jefferson]] and [[George Washington]].

==Book==
''The Art of Cookery'' was the dominant reference for home cooks in much of the English-speaking world in the second half of the 18th century and the early 19th century, and it is still used as a reference for food research and historical reconstruction. The book was updated significantly both during her life and after her death.

[[File:Glasse Art of Cookery 1758 Signature.jpg|thumb|[[Hannah Glasse]]'s signature at the top of the first chapter of her book, 6th Edition, 1758, in an attempt to reduce [[plagiarism]]]]
Early editions were not illustrated. Some posthumous editions include a decorative frontispiece, with the caption

{{quote|''The'' '''FAIR'''<!--original emphasis-->, ''who's Wise and oft consults our'' '''BOOK''',<br>''And thence directions gives her Prudent Cook'',<br>''With'' '''CHOICEST<!--typo amended--> VIANDS''', ''has her Table Crown'd,''<br>''And'' '''Health''', ''with'' '''Frugal Ellegance''' ''is found''.}}

Some of the recipes were plagiarised, to the extent of being reproduced verbatim from earlier books by other writers.<ref>{{cite news |url=http://www.independent.co.uk/life-style/food-and-drink/features/hannah-glasse-the-original-domestic-goddess-405277.html |author=Rose Prince |date=24 June 2006 |publisher=''[[The Independent]]'' |title=Hannah Glasse: The original domestic goddess}}</ref> To guard against [[plagiarism]], the title page of for example the sixth edition (1758) carries at its foot the warning "This BOOK is published with his MAJESTY's Royal Licence; and whoever prints it, or any Part of it, will be prosecuted". In addition, the first page of the main text is signed in ink by the author.

The first edition of the book was published by Glasse herself, funded by subscription, and sold (to non-subscribers) at Mrs. Ashburn's China Shop.<ref>{{cite web|last1=Boyle |first1=Laura |title=Hannah Glasse |url=http://www.janeausten.co.uk/hannah-glasse/ |publisher=Jane Austen Centre |accessdate=24 March 2015 |date=13 October 2011}}</ref>

===Contents===
{{columns-list|colwidth=30em|
* Chapter 1: Of Roasting, Boiling, &c.
* Chapter 2: Made Dishes.
* Chapter 3: Read this Chapter, and you will find how expensive a French Cook's Sauce is.
* Chapter 4: To make a Number of pretty little Dishes fit for a Supper, or Side-Dish, and little Corner-Dishes for a Great Table; and the rest you have in the Chapter for Lent.
* Chapter 5: Of Dressing Fish.
* Chapter 6: Of Soops and Broths.
* Chapter 7: Of Puddings.
* Chapter 8: Of Pies.
* Chapter 9: For a Fast-Dinner, a Number of good Dishes, which you may make use of for a Table at any other Time.
* Chapter 10: Directions for the Sick.
* Chapter 11: For Captains of Ships.
* Chapter 12: Of Hogs Puddings, Sausages, &c.
* Chapter 13: To pot and make Hams, &c.
* Chapter 14: Of Pickling.
* Chapter 15: Of making Cakes, &c.
* Chapter 16: Of Cheesecakes, Creams, Jellies, Whipt Syllabubs, &c.
* Chapter 17: Of Made Wines, Brewing, French Bread, Muffins, &c.
* Chapter 18: Jarring Cherries, and Preserves, &c.
* Chapter 19: To make Anchovies, Vermicella, Catchup, Vinegar; and to keep Artichokes, French Beans, &c.
* Chapter 20: Of Distilling.
* Chapter 21: How to market, and the Seasons of the Year for Butchers Meat, Poultry, Fish, Herbs, Roots, &c and Fruit.
* Chapter 22: [Against pests]
* Additions
* Contents of the Appendix.
}}

===Approach===

{{quote|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''To make a trifle.''{{efn|This recipe first appeared in the 1751 edition, making Glasse the first author to record the use of jelly in trifle.<ref name=Phipps>{{cite web|last1=Phipps|first1=Catherine|title=No such thing as a mere trifle|url=https://www.theguardian.com/lifeandstyle/wordofmouth/poll/2009/dec/21/perfect-trifle-jelly|publisher=[[The Guardian]]|accessdate=20 December 2015|date=21 December 2009}}</ref>}}
COVER the bottom of your dish or bowl with Naples biscuits broke in pieces, [[macaroon|mackeroons]] broke in halves, and [[ratafia]] cakes. Just wet them all through with [[sack (wine)|sack]], then make a good boiled [[custard]] not too thick, and when cold pour it over it, then put a [[syllabub]] over that. You may garnish it with ratafia cakes, currant [[gelatin dessert|jelly]], and flowers.<ref>Glasse, 1758. Page 285</ref>}}

The book has a brief table of contents on the title page, followed by a note "To the Reader", and then a full list of contents, by chapter, naming every recipe. There is a full alphabetical index at the back.

Glasse explains in her note "To the Reader" that she has written simply, "for my Intention is to instruct the lower Sort", giving the example of larding a chicken: she does not call for "large Lardoons, they would not know what I meant: But when I say they must lard with little Pieces of Bacon, they know what I mean." And she comments that "the great Cooks have such a high way of expressing themselves, that the ''poor Girls''<!--her emphasis--> are at a Loss to know what they mean."<ref>Glasse, 1758. Page i</ref>

As well as simplicity, to suit her readers in the kitchen, Glasse stresses her aim of economy: "some Things [are] so extravagant, that it would be almost a Shame to make Use of them, when a Dish can be made full as good, or better, without them."<ref>Glasse, 1758. Page ii</ref>

Chapters sometimes begin with a short introduction giving general advice on the topic at hand, such as cooking meat; the recipes occupy the rest of the text. The recipes give no indication of cooking time or oven temperature.<ref name=NourishedKitchen>{{cite web |title=Cookery Made Plain & Easy – An 18th Century Supper |url=http://nourishedkitchen.com/cookery-made-plain-and-easy/ |publisher=Nourished Kitchen |accessdate=24 March 2015 |date=27 September 2009}}</ref> There are no separate lists of ingredients: where necessary, the recipes specify quantities directly in the instructions. Many recipes do not mention quantities at all, simply instructing the cook what to do, thus:

{{quote|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;''Sauce for Larks.''
LARKS, roast them, and for Sauce have Crumbs of Bread; done thus: Take a Sauce-pan or Stew-pan and some Butter; when melted, have a good Piece of Crumb of Bread, and rub it in a clean Cloth to Crumbs, then throw it into your Pan; keep stirring them about till they are Brown, then throw them into a Sieve to drain, and lay them round your Larks.<ref>Glasse, 1758. Pages 5–6</ref>}}

==Foreign ingredients and recipes==
[[File:Truffe noire du Périgord.jpg|thumb|Glasse used costly [[truffle]]s in some recipes, despite her dislike of [[French cuisine]].]] 
Glasse set out her views of French cuisine in the book's introduction: "I have indeed given some of my Dishes ''French''<!--her emphasis--> Names to distinguish them, because they are known by those names; And where there is great Variety of Dishes, and a large Table to cover, so there must be Variety of Names for them; and it matters not whether they be called by a ''French, Dutch, or English'' Name, so they are good, and done with as little Expence as the Dish will allow of."<ref>Glasse, 1758. Page v</ref> An example of such a recipe is "To à la Daube Pigeons";<ref>Glasse, 1758. Page 85</ref> a [[daube]] is a rich French meat stew from [[Provence]], traditionally made with beef.<ref>{{cite web |title=Daube de boeuf provençale, la recette |url=http://www.saveurscroisees.com/tag/recettes-provencales/#.VRKBVOHtjgw |publisher=Saveurs Croisees |accessdate=25 March 2015}}</ref> Her "A Goose à la Mode" is served in a sauce flavoured with red wine, home-made "Catchup", veal [[sweetbread]], [[truffle]]s, [[morel]]s, and (more ordinary) mushrooms.<ref name="Glasse, 1758. Page 271">Glasse, 1758. Page 271</ref> She occasionally uses French ingredients; "To make a rich Cake" includes "half a Pint of right ''French''{{efn|Her emphasis.}} Brandy", as well as the same amount of "Sack" (Spanish [[sherry]]).<ref name="Glasse, 1758. Page 271"/>

Ingredients from faraway countries were becoming available. The recipe for "Elder-Shoots, in Imitation of Bamboo" makes use of a homely ingredient to substitute for a foreign one that English travellers had encountered in the Far East. The same recipe also calls for a variety of imported spices to flavour the pickle: "an Ounce of white or red Pepper, an Ounce of Ginger sliced, a little Mace, and a few Corns of Jamaica Pepper."<ref>Glasse, 1758. Page 270</ref>

There are two recipes for making [[chocolate]], calling for costly imported ingredients like [[musk]] (an [[aromatic]] obtained from [[musk deer]]) and [[ambergris]] (a waxy substance from [[sperm whale]]s), [[vanilla]] and [[cardamon]]:<ref name=Glasse357>Glasse, 1758. Page 357</ref>

{{quote|Take six pounds of Cocoa-nuts, One Pound of Anniseeds, four Ounces of long Pepper, one of Cinnamon, a Quarter of a Pound of Almonds, one Pound of Pistachios, as much Achiote{{efn|[[Achiote]] is the plant that yields the natural pigment [[annatto]], still used to colour food.}} as will make it the colour of Brick; three grains of Musk, and as much Ambergrease, six Pounds of Loaf-sugar, one Ounce of Nutmegs, dry and beat them, and fearce them through a fine Sieve...<ref name=Glasse357/>}}

==Reception==

===England===
''The Art of Cookery'' was a best seller for a century after its first publication, making Glasse one of the most famous cookbook authors of her time.<ref name=BL/> The book was "by far the most popular cookbook in eighteenth-century Britain".<ref name=StavelyFitzgerald2011>{{cite book |last1=Stavely |first1=Keith W. F. |last2=Fitzgerald |first2=Kathleen |title=Northern Hospitality: Cooking by the Book in New England |url=https://books.google.com/books?id=pRbJUQMM0ssC&pg=PA8 |date=1 January 2011 |publisher=Univ of Massachusetts Press |isbn=1-55849-861-3 |page=8}}</ref>

It was rumoured for decades that despite the byline it was the work of a man, [[Samuel Johnson]] being quoted by [[James Boswell]] as observing at the publisher [[Charles Dilly]]'s house that "Women can spin very well; but they cannot make a good book of cookery."<ref name=BL>{{cite web|title=The Art of Cookery |url=http://www.bl.uk/learning/langlit/booksforcooks/1700s/arttitlehome/titlepage.html |publisher=British Library |accessdate=24 March 2015}}</ref>

''The Foreign Quarterly Review'' of 1844 commented that "there are many good receipts in the work, and it is written in a plain style." The review applauds Glasse's goal of plain language, but observes "This book has one great fault; it is disfigured by a strong anti-Galican [anti-French] prejudice."<ref name=FQR>{{cite book|title=The Foreign Quarterly Review|url=https://books.google.com/books?id=tpNEAQAAMAAJ&pg=PA202|year=1844|publisher=Treuttel and Würtz, Treuttel, Jun, and Richter|page=202}}</ref>

===Thirteen Colonies===
The book sold extremely well in the [[Thirteen Colonies|Colonies of North America]]. This popularity survived the [[American War of Independence]]. A [[New York (state)|New York]] memoir of the 1840s declared that "We had emancipated ourselves from the sceptre of [[George III of England|King George]], but that of Hannah Glasse was extended without challenge over our fire-sides and dinner-tables, with a sway far more imperative and absolute".<ref name=StavelyFitzgerald2011/> The first American edition of ''The Art of Cookery'' (1805) included two recipes for "Indian pudding" as well as "Several New Receipts adapted to the American Mode of Cooking", such as "Pumpkin Pie", "Cranberry Tarts" and "Maple Sugar". [[Benjamin Franklin]] is said to have had some of the recipes translated into French for his cook while he was the American ambassador in Paris.<ref name=HessHess2000>{{cite book |last1=Hess |first1=John L. |last2=Hess |first2=Karen|title=The Taste of America |url=https://books.google.com/books?id=JMVSUEjTCWgC&pg=PA85 |year=2000 |publisher=University of Illinois Press|isbn=978-0-252-06875-1 |page=85}}</ref><ref>{{cite book |last1=Chinard |first1=Gilbert |title=Benjamin Franklin on the Art of Eating |date=1958 |publisher=American Philosophical Society |page=(cited by Hess and Hess, 2000)}}</ref> Both [[George Washington]] and [[Thomas Jefferson]] owned copies of the book.<ref name="Rountree2003">{{cite book|last=Rountree |first=Susan Hight |title=From a Colonial Garden: Ideas, Decorations, Recipes |url=https://books.google.com/books?id=BgUUYWMBf2EC&pg=RA1-PR2 |year=2003 |publisher=Colonial Williamsburg |isbn=978-0-87935-212-7 |page=1}}</ref>

Food critic John Hess and food historian Karen Hess have commented that the "quality and richness" of the dishes "should surprise those who believe that Americans of those days ate only Spartan frontier food", giving as examples the glass of Malaga wine, seven eggs and half a pound of butter in the pumpkin pie. They argue that while the elaborate bills of fare given for each month of the year in American editions were conspicuously wasteful, they were less so than the "interminable" menus "stuffed down" in the Victorian era, as guests were not expected to eat everything, but to choose which dishes they wanted, and "the cooking was demonstrably better in the eighteenth century."<ref name=HessHess2000/>

The book contains a recipe "To make Hamburgh Sausages"; it calls for beef, suet, pepper, cloves, nutmeg, "a great Quantity of Garlick cut small", white wine vinegar, salt, a glass of red wine and a glass of rum; once mixed, this is to be stuffed "very tight" into "the largest Gut you can find", smoked for up to ten days, and then air-dried; it would keep for a year, and was "very good boiled in Peas Porridge, and roasted with toasted bread under it, or in an Amlet".<ref>Glass, 1758. Page 370.</ref> The cookery writer Linda Stradley in an article on [[hamburger]]s suggests that the recipe was brought to England by German immigrants; its appearance in the first American edition may be the first time "Hamburgh" is associated with chopped meat in America.<ref>{{cite web|last1=Stradley |first1=Linda |title=Hamburgers - History and Legends of Hamburgers |url=http://whatscookingamerica.net/History/HamburgerHistory.htm |website=What's Cooking America|accessdate=25 March 2015 |date=2004}}</ref>

===Modern opinions===
[[Rose Prince]], writing in ''[[The Independent]]'', describes Glasse as "the first domestic goddess, the queen of the dinner party and the most important cookery writer to know about." She notes that [[Clarissa Dickson-Wright]] "makes a good case" for giving Glasse this much credit, that Glasse had found a gap in the market, and had the distinctions of simplicity, an "appetising repertoire", and a lightness of touch. Prince quotes the food writer [[Bee Wilson]]: "She's authoritative but she is also intimate, treating you as an equal", and concludes "A perfect book, then; one that deserved the acclaim it received."<ref>{{cite web|last1=Prince|first1=Rose|title=Hannah Glasse: The original domestic goddess|url=http://www.independent.co.uk/life-style/food-and-drink/features/hannah-glasse-the-original-domestic-goddess-405277.html|publisher=The Independent|accessdate=24 March 2015|date=24 June 2006}}</ref> Jane Shilling, writing in ''Mail Online'', agrees, noting that "Glasse writes in the same sort of chatty, intimate style that makes Delia and Nigella's books so comforting for the nervous cook: Glasse concludes one chapter 'You must do just as you like it'."<ref>{{cite web |last1=Shilling |first1=Jane|title=A book on cookery? It'll never catch on |url=http://www.dailymail.co.uk/home/books/article-2536606/A-book-cookery-Itll-never-catch-COOKING-PEOPLE-BY-SOPHIA-WAUGH.html#ixzz3VKHXJQLD |publisher=Daily Mail |accessdate=24 March 2015 |date=9 January 2014}}</ref>

Cookery writer Laura Kelley notes that the 1774 edition was one of the first books in English to include a recipe for [[curry]]: "To make a currey the Indian way." The recipe calls for two small chickens to be fried in butter; for ground [[turmeric]], [[ginger]] and [[black pepper|pepper]] to be added and the dish to be stewed; and for cream and lemon juice to be added just before serving. Kelley comments that "The dish is very good, but not quite a modern curry. As you can see from the title of my interpreted recipe, the modern dish most like it is an eastern (Kolkata) butter chicken. However, the Hannah Glasse curry recipe lacks a full complement of spices and the varying amounts of tomato sauce now so often used in the dish."<ref>{{cite web |last1=Kelley |first1=Laura |title=Indian Curry Through Foreign Eyes #1: Hannah Glasse |url=http://www.silkroadgourmet.com/hannah-glasse-curry/ |publisher=Silk Road Gourmet |accessdate=24 March 2015 |date=14 April 2013}}</ref>

Cookery writer Sophia Waugh said that Glasse's food was what [[Jane Austen]] and her contemporaries would have eaten. Glasse is one of the five female writers discussed in Waugh's 2013 book ''Cooking People: The Writers Who Taught the English How to Eat''.<ref>{{cite web |title=Centuries of home cooking inspiration from female writers to be brought to life at Hampshire's Sophia Waugh book event |url=http://www.hampshire-life.co.uk/home/centuries_of_home_cooking_inspiration_from_female_writers_to_be_brought_to_life_at_hampshire_s_sophia_waugh_book_event_1_3283848 |publisher=Hampshire Life |accessdate=24 March 2015 |date=4 February 2014}}</ref>

==Legacy==

[[Ian Mayes]], writing in ''[[The Guardian]]'', quotes ''[[Brewer's Dictionary of Phrase and Fable]]'' as stating "''First catch your hare''. This direction is generally attributed to Hannah Glasse, habit-maker to the Prince of Wales, and author of The Art of Cookery made Plain and Easy (1747). Her actual directions are, 'Take your Hare when it is cas'd, and make a pudding...' To 'case' means to take off the skin" [not "to catch"]; Mayes notes further that both the ''[[Oxford English Dictionary]]'' and ''[[The Dictionary of National Biography]]'' discuss the attribution.<ref>{{cite web |last1=Mayes |first1=Ian |title=Splitting Hares |url=https://www.theguardian.com/books/2000/jun/03/books.guardianreview5 |work=The Guardian |accessdate=24 March 2015 |date=3 June 2000}}</ref>

{{as of|2015|alt=As at 2015}}, Scott Herritt's "South End" restaurant in [[South Kensington]], London serves some recipes from the book.<ref>{{cite web |last1=Kummer |first1=Corby |title=Restaurant Review: Kitchen in the South End |url=http://www.bostonmagazine.com/2012/10/restaurant-review-kitchen-south-end/ |work=Boston Magazine |accessdate=24 March 2015 |date=November 2012}}</ref> The "Nourished Kitchen" website describes the effort required to translate Glasse's 18th-century recipes into modern cooking techniques.<ref name=NourishedKitchen/>

==Editions==
The book ran through many editions, including:<ref>{{cite web|title=Hannah Glasse: Art of Cookery|url=https://www.worldcat.org/search?q=glasse+art+of+cookery&fq=&se=yr&sd=asc&dblist=638&start=21&qt=next_page|publisher=WorldCat|accessdate=22 March 2015}}</ref>

{{columns-list|colwidth=30em|
* First edition, London: Printed for the author, 1747.
* London: Printed for the author, 1748.
* [[Dublin]]: E. & J. Exshaw, 1748.
* London: Printed for the author, 1751.
* London: Printed for the author, 1755.
* Sixth edition, London: Printed for the author, sold by A. Millar, & T. Trye, 1758.
* London: A. Millar, J. and R. Tonson, W. Strahan, P. Davey and B. Law, 1760.
* London: A. Millar, J. and R. Tonson, W. Strahan, T. Caslon, B. Law, and A. Hamilton, 1763.
* Dublin: E. & J. Exshaw, 1762.
* Dublin: E. & J. Exshaw, 1764.
* London: A. Millar, J. and R. Tonson, W. Strahan, T. Caslon, T. Durham, and W. Nicoll, 1765.
* London: W. Strahan and 30 others, 1770.
* London: J. Cooke, 1772.
* Dublin: J. Exshaw, 1773.
* [[Edinburgh]]: Alexander Donaldson, 1774.
* London: [https://archive.org/details/artcookerymadep02glasgoog W. Strahan and others, 1774].
* London: L. Wangford, c. 1775.
* London: W. Strahan and others, 1778.
* London: W. Strahan and 25 others, 1784.
* London: J. Rivington and others, 1788.
* Dublin: W. Gilbert, 1791.
* London: T. Longman and others, 1796.
* Dublin: W. Gilbert, 1796.
* Dublin: W. Gilbert, 1799.
* London: J. Johnson and 23 others, 1803.
* [[Alexandria, Virginia]]: Cottom and Stewart, 1805.
* Alexandria, Virginia: Cottom and Stewart, 1812.
* London: H. Quelch, 1828.
* London: Orlando Hodgson, 1836.
* London: J.S. Pratt, 1843.
* [[Aberdeen]]: G. Clark & Son, 1846.
* ''The art of cookery, made plain and easy to the understanding of every housekeeper, cook, and servant.'' With John Farley. Philadelphia: Franklin Court, 1978.
* ''"First catch your hare--" : the art of cookery made plain and easy''. With Jennifer Stead, Priscilla Bain. London: Prospect, 1995.
::--- Totnes: Prospect, 2004.
* Alexandria, Virginia: Applewood Books, 1997.
* Farmington Hill, [[Michigan]]: Thomson Gale, 2005.
* [[Mineola, New York]]: Dover Publications, 2015.
}}

==See also==
*[[Cajsa Warg]]
*[[François Pierre La Varenne]]
*[[early modern European cuisine]]

==Notes==
{{notelist}}

==References==
{{reflist|28em}}

==External links==
{{Commons category|Art of Cookery Made Plain and Easy}}
*[http://www.celtnet.org.uk/recipes/glasse-contents.php 1774 edition] (as HTML)
*[https://archive.org/details/artcookerymadep02glasgoog In various formats at the Internet Archive]

{{English cuisine}}

{{DEFAULTSORT:Art of Cookery made Plain and Easy}}
[[Category:English cuisine]]
[[Category:English non-fiction books]]
[[Category:Early Modern cookbooks]]
[[Category:1747 books]]