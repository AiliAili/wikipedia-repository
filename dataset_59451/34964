{{Infobox storm
| name = Tinker Air Force Base tornadoes
| image = Tinker AFB tornado damage 2.jpg
| alt = 
| caption = Damage to [[United States Air Force]] [[bomber]]s from the March 20, 1948, tornado
| formed = March 20, 1948, and March 25, 1948 
| active =
| dissipated =
| lowest pressure =  
| lowest temperature =
| tornadoes = 
| fujitascale = F3
| tornado duration = 
| highest winds = 
| gusts =
| maximum snow = 
| power outages =
| casualties =  Several injuries
| damages = $16 million <ref name="BAMS">{{cite journal| last = Grice| first = G. K.| author2 = Trapp, R. J.| author3 = Corfidi, S. F.| author4 = Davies-Jones, R.| author5 = Buonanno, C. C.| author6 = Craven, J. P.| author7 = Droegemeier, K. K.| author8 = Duchon, C.| author9 = Houghton, J. V.| author10 = Prentice, R. A.| author11 = Romine, G.| author12 = Schlachter, K.| author13 = Wagner, K. K.| date=July 1999| title = The Golden Anniversary Celebration of the First Tornado Forecast| journal = [[Bulletin of the American Meteorological Society]]| volume = 80| issue = 7| pages = 1341–1348| publisher = [[American Meteorological Society]]| location = Boston| doi  = 10.1175/1520-0477(1999)080<1341:TGACOT>2.0.CO;2| url = http://www.nssl.noaa.gov/goldenanniversary/symposium/bamsarticle.pdf| format = PDF| accessdate =July 12, 2009|bibcode = 1999BAMS...80.1341G | archiveurl= https://web.archive.org/web/20090530000341/http://www.nssl.noaa.gov/goldenanniversary/symposium/bamsarticle.pdf| archivedate= May 30, 2009 <!--DASHBot-->| deadurl= no}}</ref> (1948) (${{Inflation|US|16|1948}} million {{CURRENTYEAR}})
| affected = [[Oklahoma City]], Oklahoma
| location = 
| current advisories =
| enhanced =
| notes = 
}}
The '''1948 Tinker Air Force Base tornadoes''' were two [[tornado]]es which struck [[Tinker Air Force Base]] in [[Oklahoma City]], Oklahoma, on March 20 and 25, 1948. Both are estimated to have been equivalent to F3 in intensity on the modern [[Fujita scale]] of tornado intensity,<ref name=Grazulis1>{{harvnb|Grazulis|1993|p=935}}</ref> which was not devised until 1971.<ref name=Grazulis2>{{harvnb|Grazulis|2001|p=131}}</ref> The March 20 tornado was the costliest tornado in Oklahoma history at the time.<ref>{{cite web |first = Michael L. |last = Branick |title = Tornadoes in the Oklahoma City, Oklahoma Area Since 1890 |url = http://www.srh.noaa.gov/oun/tornadodata/okc/ |publisher = [[National Weather Service]] |location = [[Norman, Oklahoma]] |date = December 22, 2008 |accessdate  =July 12, 2009|archiveurl = https://web.archive.org/web/20090630042543/http://www.srh.noaa.gov/oun/tornadodata/okc/ |archivedate = June 30, 2009|deadurl=yes}}</ref> On March 25, [[meteorologist]]s at the base noticed the extreme similarity between the weather conditions of that day and March 20, and later in the day issued a "tornado [[Weather forecasting|forecast]]", which was verified when a tornado struck the base that evening. This was the first official tornado forecast, as well as the first successful tornado forecast, in recorded history.<ref name="BAMS"/>

==March 20 tornado==
Weather forecasting was still crude and prone to large errors in the era before [[weather satellite]]s and computer modeling. Thunderstorms were not even in the forecast for the evening of March 20. However, around 9:30&nbsp;pm storms were reported about {{convert|20|mi|km}} to the southwest, and at 9:52 a tornado was sighted near [[Will Rogers Airport]] {{convert|7|mi|km}} away, along with a {{convert|92|mph|km/h|adj=on}} wind gust, moving northeast towards the base.<ref name="Miller">{{cite web |first = Col. Robert C. |last = Miller |title = Description of Historical Events Relating to Tornado Forecasting in the Late 1940s and Early 1950s |url = http://www.nssl.noaa.gov/GoldenAnniversary/Historic.html |work = The Unfriendly Sky |publisher = [[National Severe Storms Laboratory]] |accessdate =July 12, 2009}}</ref>

At 10:00, the tornado reached the southwest corner of the base. Illuminated by nearly constant lightning, the tornado was highly visible as it bisected the base, tossing around planes which were parked in the open. The [[control tower]] reported a {{convert|78|mph|km/h|adj=on}} wind gust before the windows shattered, injuring several personnel with flying glass. The tornado dissipated at the northeast corner of the base.<ref name="Miller"/>

The tornado missed most structures on the base, but the damage to expensive military aircraft was substantial. The total damage cost came to around $10 million, or ${{Inflation|US|10|1948}} million in {{CURRENTYEAR}} United States dollars.<ref name="BAMS"/> This was the most damaging tornado in [[Oklahoma]] up to that date.<ref name="SEP">{{cite news | title = Flash-Tornado Warning! | first = Pat | last = McDermott | url = http://www.nssl.noaa.gov/goldenanniversary/tornadow.html | publisher = [[The Saturday Evening Post]] | date = July 28, 1951 | pages = 17–19, 53–57 | accessdate =July 12, 2009 }}</ref>

==Investigation and tornado forecast==
In the aftermath of the first tornado, an official inquiry was conducted into the failure to predict the destructive tornado. Air Force investigators came to the conclusion that "due to the nature of the storm it was not forecastable given the present state of the art." They also made recommendations that the meteorological community determine a tornado warning system for the public, as well as a protocol for protecting life and property at military bases.<ref name="Miller"/>

Both of these investigations began almost immediately. In the days following the tornado, Tinker's meteorologists Major Ernest J. Fawbush and Captain [[Robert C. Miller]] investigated [[surface weather analysis|surface]] and [[upper-atmospheric models|upper-air]] weather data from this and past tornado outbreaks, hoping to be able to identify conditions which were favorable for tornadoes. By March 24, they had compiled several possible tornado indicators, and decided it would be difficult, but possible, to identify large [[tornado watch|tornado threat areas]] in the future.<ref name="Miller"/>

On the morning of March 25, base meteorologists noticed that weather charts for the day were strikingly similar to those before the March 20 tornado. Forecasts issued by the [[National Weather Service|Weather Bureau]] indicated that almost the same conditions would be present in the evening of March 25 as were present on March 20. In the morning, they issued a forecast for "heavy thunderstorms" effective for 5–6&nbsp;pm that evening. This would allow the base's commander to alert base personnel that they may institute their brand-new tornado precautions.<ref name="Miller"/>

As the day wore on, conditions appeared more and more favorable for thunderstorms, and more and more similar to the events of March 20. [[Weather radar]] images showed a severe [[squall line]] had formed to the west, and [[weather station]]s to the west reported [[cumulonimbus cloud]]s and thunderstorms. In an afternoon meeting, under some pressure from their commanding officer, base meteorologists composed and issued the first official tornado forecast. Although they were aware of the small chance of success, they felt they had no choice, since the conditions were so similar to March 20.<ref name="Miller"/> Equipment which could be was moved to [[Bomb shelter|bomb-proof shelters]], and base personnel were moved to safer areas.<ref name="SEP"/>

==March 25th tornado==
[[File:Tinker AFB tornado damage 3.jpg|thumb|250px|Damage to airplanes and cars from the March 25, 1948, tornado]]
Although storms were relatively benign up to the point where they reached Tinker, a [[supercell]] formed just west of the base, and at around 6 pm a tornado touched down on the base for the second time in six days.<ref name="The Tornado">{{cite book| last                  = Grazulis| first                 = Thomas P.| authorlink            = Thomas P. Grazulis| title                 = The Tornado: Nature's Ultimate Windstorm| url                   = https://books.google.com/books?id=3zEYILW2MJIC&pg=PA85&lpg=PA85#v=onepage&q&f=false| accessdate            = October 4, 2012| year                  = 2001| publisher             = University of Oklahoma Press| location              = Norman, OK| isbn                  = 0-8061-3538-7| pages                 = 85&ndash;88}}</ref> This second tornado caused $6 million in damage, or ${{Inflation|US|6|1948}} million in  {{CURRENTYEAR}} dollars. However, due to precautions enacted because of the tornado forecast, no injuries were reported, and damage totals could have been much higher.<ref name="Miller"/><ref name="SEP"/>

==Legacy==
The tornado prediction proved to be successful, even if its precision was mostly due to chance. Before this point, the [[Weather Bureau]] had a policy against issuing tornado warnings, mainly due to fear of panic by the public, and subsequent complacency if forecasts turned out to be false alarms.<ref name="SEP"/>

Due to lives and costs saved, Fawbush and Miller continued their tornado forecasts, which verified at quite a high rate over the next three years.<ref name="SEP"/> At first, they kept their forecasts secret. In the spring and summer of 1949, they issued eighteen forecasts for tornadoes within a {{convert|100|sqmi|sqkm|adj=on}} ≈area, and all eighteen proved successful.<ref name="SEP"/> In the subsequent years, while not explicitly using the word "tornado", the Weather Bureau used the pair's forecasts to predict "severe local storms".<ref name="SEP"/>

The [[Synoptic scale meteorology|synoptic]] pattern which occurred on March 25 later became known as the "Miller type-B" pattern and is recognized as one of the most potent severe weather setups.<ref>{{cite journal | last1 = Maddox | first1 = Robert A. | last2 = Crisp | first2 = Charlie A.  |date=August 1999 | title = The Tinker AFB Tornadoes of March 1948 | journal = [[Weather and Forecasting]] | volume = 14 | issue = 4 | pages = 492–499 | publisher = [[American Meteorological Society]] | format = PDF | doi = 10.1175/1520-0434(1999)014<0492:TTATOM>2.0.CO;2 | accessdate = July 3, 2012 | url = http://journals.ametsoc.org/doi/pdf/10.1175/1520-0434%281999%29014%3C0492%3ATTATOM%3E2.0.CO%3B2|bibcode = 1999WtFor..14..492M }}</ref>

==See also==
{{commons|Tinker Air Force Base tornado}}
*[[Weather forecasting]]
*[[1999 Bridge Creek–Moore tornado]], another tornado that affected the airbase
{{clear}}

==References==
{{reflist|colwidth=30em}}
{{clear}}

===Bibliography===
*{{citation|last=Grazulis|first=Thomas|year=1993|title=Significant Tornadoes 1680-1991: A Chronology and Analysis of Events|publisher=Environmental Films|location=[[St. Johnsbury, Vermont]]|isbn=1-879362-03-1}}
*{{citation|last=Grazulis|first=Thomas|year=2001|title=The Tornado: Nature’s Ultimate Windstorm|publisher=University of Oklahoma Press|location=[[Norman, Oklahoma]]|isbn=978-0-8061-3538-0}}

{{Good article}}

{{DEFAULTSORT:1948 Tinker Air Force Base Tornadoes}}
[[Category:Tornadoes of 1948]]
[[Category:Tornadoes in Oklahoma]]
[[Category:1948 in Oklahoma|Tinker Air Force Base Tornadoes, 1948]]
[[Category:March 1948 events]]