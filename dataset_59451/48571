{{Infobox pro wrestling championship
|championshipname=NWL/HoPWF Hardcore Championship
|currentholder=Jimmy Jessup 
|won=March 29, 2008
|promotion=[[National Wrestling League]]
|created=October 11, 2003
|mostreigns=Chuckie Manson {{small|(5)}}
|firstchamp=Chuckie Manson
|longestreign=[[Axl Rotten]] {{small|(98 days)}}
|shortestreign=Todd Hill {{small|(14 days)}}
|pastnames=
}}
The '''NWL/HoPWF Hardcore Championship''' is the secondary [[professional wrestling]] [[Championship (professional wrestling)|title]] in the [[National Wrestling League]] [[professional wrestling promotion|promotion]]. It was first won by Chuckie Manson in a three-way match against [[Morgus the Maniac]], Blood and Shorty Smallz in [[Martinsburg, West Virginia]] on October 11, 2003. The title is defended primarily in the [[Mid-Atlantic United States|Mid-Atlantic]] and [[East Coast of the United States|East Coast]], most often in [[Hagerstown, Maryland]], but also in [[Pennsylvania]] and [[West Virginia]]. There are 16 recognized known champions with a total of 23 title reigns.<ref name="Solie">{{cite web |url=http://www.solie.org/titlehistories/hcthopw.html |title=NWL/HoPWF Hardcore Title History |author=Tsakiries, Phil |year=2004 |work=Solie's Title Histories |publisher=Solie.org |accessdate=2010-05-04}}</ref><ref name="TitleHistory">{{cite web |url=http://www.nwlwrestling.com/champs/hardcore.html |title=National Wrestling League Hardcore Champion History |accessdate=2010-05-04 |work=[[National Wrestling League]] |archiveurl=https://web.archive.org/web/20080626201043/http://www.nwlwrestling.com/champs/hardcore.html |archivedate=2008-06-26}}</ref>{{-}}

==Title history==
{| class="wikitable" width=100%
!style="background: #e3e3e3;" width=20%|Wrestler:
!style="background: #e3e3e3;" width=0% |Times:
!style="background: #e3e3e3;" width=16%|Date:
!style="background: #e3e3e3;" width=21%|Location:
!style="background: #e3e3e3;" width=43%|Notes:
|-
|Chuckie Manson
|1
|October 11, 2003
|[[Martinsburg, West Virginia]]
|{{small|Defeated [[Morgus the Maniac]], Blood and Shorty Smallz in a 3-way match to become the first champion.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated.''
|-
|Blood 
|1
|October 18, 2003 
|[[Gettysburg, Pennsylvania]]
|<ref name="Solie"/><ref name="TitleHistory"/>
|-
|style="background: #dddddd;" colspan=5|''The title is vacated when Blood is stripped of the title for showing up late to a title defense in [[Chambersburg, Pennsylvania]] on December 13, 2003. A triple threat match in held between Chuckie Manson, Jake Davis and Scott Vaughn to decide the new champion.''<ref name="Solie"/><ref name="TitleHistory"/>
|-
|Chuckie Manson 
|2
|December 13, 2003
|[[Chambersburg, Pennsylvania]]
|{{small|Defeated Jake Davis and Scott Vaughn in a triple threat "Pinfalls Count Anywhere" match to win the vacant title.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|John Rambo 
|1
|January 10, 2004
|Martinsburg, West Virginia 
|{{small|Defeated Morgus the Maniac, Blood and Pirate of the Caribbean in a 3-way match.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|Jake "The Machine" Davis 
|1
|February 14, 2004
|[[Blue Ridge Summit, Pennsylvania]]
|<ref name="Solie"/><ref name="TitleHistory"/>
|-
|OGB 
|1
|April 17, 2004
|[[Newville, Pennsylvania]]
|{{small|Won title in a Shootfight match.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated after OGB suffers an injury during a [[NWL Cruiserweight Championship|Cruiserweight]] title defense on May 14, 2002.''<ref name="Solie"/><ref name="TitleHistory"/>
|-
|Blood 
|2
|May 15, 2004
|Martinsburg, West Virginia 
|{{small|Defeated John Rambo and Ian Decay in a three way dance "Fans Bring Weapons" match to win the vacant title.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|Shorty Smallz 
|1
|July 17, 2004
|Martinsburg, West Virginia 
|{{small|Won title in Steel chain match.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|Chuckie Manson 
|3
|October 16, 2004
|Martinsburg, West Virginia 
|{{small|Won title in a "Fans Bring Weapons" match.<ref name="Solie"/><ref name="TitleHistory"/>}}
|-
|The Phantom 
|1
|December 4, 2004
|Newville, Pennsylvania
|{{small|Won title in a Lumberjack Leather Strap Ladder & Chairs match.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on December 18, 2004, when The Phantom is unable to appear for a scheduled title defense. A singles match between Chuckie Manson and Blood is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|Chuckie Manson 
|4
|December 18, 2004
|Martinsburg, West Virginia
|{{small|Defeated Blood to win the vacant title.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on January 29, 2005, when Chuckie Manson is unable to appear for a scheduled title defense. A 3-way match between Axl Rotten, Shorty Smalls and Blood is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|[[Axl Rotten]]
|1
|January 29, 2005
|Newville, Pennsylvania
|{{small|Defeated Shorty Smalls and Blood in a 3-way dance "Pinfalls Count Anywhere" match to win the vacant title.<ref name="TitleHistory"/>}}  
|-
|Chuckie Manson 
|5
|May 7, 2005
|Martinsburg, West Virginia
|{{small|Defeated Axl Rotten and Switchblade in a Triple Threat match.<ref name="TitleHistory"/>}} 
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on December 17, 2005, when Chuckie Manson retires. A 6-man "Barbedwire Christmas Tree" match is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|Shorty Smalls 
|2
|December 17, 2005
|Martinsburg, West Virginia
|{{small|Pinned Switchblade in a match with Marc Mandrake, Hal Litzer and Blood to win the vacant title.<ref name="TitleHistory"/>}} 
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on February 25, 2006, when Shorty Smalls is unable to compete due to injury. A singles match between Dave Donovan and Erik Polaris is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|Dave Donovan 
|1
|February 25, 2006
|Martinsburg, West Virginia
|{{small|Pinned Erik Polaris in a Barbedwire match to win the vacant title. Match held at the [[Potomac State College]].<ref name="TitleHistory"/>}}
|-
|Erik Polaris 
|1
|June 10, 2006
|Martinsburg, West Virginia
|{{small|Won title in a "4 Corners of Pain" title match.<ref name="TitleHistory"/>}} 
|-
|Dave Donovan 
|2
|September 30, 2006
|[[Keyser, West Virginia]]
|{{small|Defeated Shorty Smalls in a Barbedwire match to win the vacant title.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on November 11, 2006, when Dave Donovan is unable to appear for a scheduled title defense. A singles match between Ian Decay and Sik Rik Matrix is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|Ian Decay 
|1
|November 11, 2006
|Newville, Pennsylvania
|{{small|Pinned Sik Rik Matrix to win the vacant title.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on March 2, 2007.''<ref name="TitleHistory"/>
|-
|OGB
|1
|March 17, 2007
|Chambersburg, Pennsylvania
|{{small|Won Bunkhouse battle royal to win the vacant title.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on May 1, 2007.''<ref name="TitleHistory"/>
|-
|Todd Hill 
|1
|May 5, 2007
|Newville, Pennsylvania
|{{small|Won Bunkhouse battle royal to win the vacant title.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on May 19, 2007, when Todd Hill is unable to appear for a scheduled title defense. A Four Corners match is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|Chris Cline   
|1
|May 19, 2007
|Martinsburg, West Virginia
|{{small|Chris Cline defeated [[Kevin Featherstone]], OGB and John Rambo in a "Four Corners of Pain" elimination match.<ref name="TitleHistory"/>}}
|-
|Brent Rage 
|1
|June 23, 2007 
|Martinsburg, West Virginia
|{{small|Won in a "Slaughterhouse-Five" match.<ref name="TitleHistory"/>}}
|-
|style="background: #dddddd;" colspan=5|''The title is vacated on May 19, 2007, when Brent Rage is unable to appear for a scheduled title defense. A Four Corners match is held to determine the new champion.''<ref name="TitleHistory"/>
|-
|"Jackpot" Jimmy Jessup   
|1
|March 29, 2008
|Martinsburg, West Virginia
|{{small|Defeated Goldthumb, Psycrotes and Blood in a "Barbwire & Weapons" 4-Way Dance to win the vacant title.<ref name="TitleHistory"/>}} 
|}

==See also==
{{Portal|Professional Wrestling}}

==References==
{{Reflist}}

==External links==

[[Category:Hardcore wrestling championships]]