{{Infobox company
| name = Kgalagadi Breweries (Pty) Limited
| logo = [[File:KBL_Corporate_Logo.jpg.png|200px]]
| logo_caption =
| image =
| image_caption =
| trading_name = '''KBL'''
| native_name = 
| native_name_lang = 
| romanized =
| former type =
| type =[[Private company]]
| traded_as = 
| industry = [[Brewing]], Beverage
| genre = 
| fate =
| predecessor = Prinz Brau Botswana<ref>{{cite web | url=http://minetravel.co.bw/?p=1213 | title=Kgalagadi Breweries celebrates 40 years of brewing excellence | date=15 May 2013 | accessdate=28 January 2014}}</ref>
| successor =
| foundation = 1970
| founder =
| defunct = 
| location_city = [[Gaborone]]
| location_country = [[Botswana]]
| locations = 1 Brewery,<br/> 4 Sorghum breweries,<br/> 1 Soft-drink plant.
| area_served =
| key_people = Batlang Mmualefe - [[Chairman]] <br/> Johan de Kok - [[CEO]]
| products = 
| production =
| services =
| revenue = [[Botswana Pula|BWP]] 1,74 billion<ref>{{cite web | url=http://www.sundaystandard.info/article.php?NewsID=17088 | title=Sechaba Brewery defies odds as revenues go up 11.6 percent |publisher=Sunday Standard|date=10 June 2013 | accessdate=27 January 2014}}</ref>
| operating_income =
| net_income =
| aum = 
| assets =
| equity =
| owner = [[Sechaba Brewery Holdings|SBHL]] - 60% <br/> [[SABMiller]] - 40%
| num_employees =
| parent =
| divisions =
| subsid =
| homepage = [http://www.sabmiller.com/about-us/where-we-operate/#q=BW&id=9033f971-2c9d-6131-b545-ff00002059a6&type=Country KBL on SABMiller]
| footnotes = 
| intl = 
| bodystyle =
}}

{{Coord|24|37|52|S|25|55|08.6|E|display=title}}

'''Kgalagadi Breweries (Pty) Limited''' are brewers based in Gaborone, Botswana. They produce lager beers, traditional beers, bottled water and soft drinks under license.<ref>{{cite web|url=http://www.bse.co.bw/listed_companies/listed.php?company=SECHABA |title=Sechaba Brewery Holdings Limited |publisher=BSE |date= |accessdate=2014-03-06}}</ref> The brewery started out as Prinz Brau, with two brands, Prinz Brau and Prinz Deluxe.

==Corporate timeline==
* 1960's Botswana Breweries Limited is started in Francistown to brew Chibuku traditional beer.
* 1970s - [[Sechaba Brewery Holdings]] Limited (SBHL) was set up to hold investments in the beverage sector.
* 1977 - SAB acquires management control of KBL (40% equity stake). KBL begins bottling Coca-Cola products.
* 1989 - SBHL is listed on the Botswana Stock Exchange.
* 1995 - New soft drink bottling line built in Gaborone.
* 2009 - KBL launches St Louis Export.
* 2013 - Botswana Breweries Ltd, was amalgamated with KBL.<ref>{{cite web|url=http://www.sabmiller.com/index.asp?pageid=1153 |title=About us - Where we operate - Africa - Botswana - Overview |publisher=SABMiller |date= |accessdate=2014-03-06}}</ref>

==Botswana Breweries Limited==
Botswana Breweries has four plants in Lobatse, Gaborone, Palapye and Francistown.

The products include
* traditional opaque beer [[Chibuku Shake Shake|Chibuku]] - made from sorghum and maize.
* Phafana (opaque beer)
* non-alcoholic beverage Keone Mooka [[Mageu]].

Product packs include a straight-sided screw-top 1 litre carton. A 750ml pack and a draught.<ref>{{cite web|author= |url=http://www.globalvillagedirectory.info/Botswana/Gaborone/KBL-and-BBL.aspx |title=KBL and BBL, Kgalagadi Breweries, KBL, Botswana Breweries Limited, BBL, Brewery, SABMiller |publisher=Globalvillagedirectory.info |date= |accessdate=2014-03-06}}</ref>

==Ownership==
The shares of Kgalagadi Breweries Limited are privately held. The shareholding in the company's stock as at December 31, 2012 was as depicted in the table below:<ref>{{Cite web|url = http://www.sabmiller.com/docs/default-source/investor-documents/reports/2012/financial-reports/annual-report-sbh-2012.pdf?sfvrsn=4|title = Sechaba Brewery Holdings Limited 2012 Annual Report|date = December 31, 2012|accessdate = October 15, 2014|website = SAB Miller|publisher = Sechaba Brewery Holdings Limited|last = |first = }}</ref>

{| style="font-size:100%;"
|-
| width="100%" align="center" | '''Kgalagadi Breweries Limited Stock Ownership'''
|- valign="top" 
|
{| class="wikitable sortable" style="margin-left:auto;margin-right:auto"
! style="width:2em;" |Rank !!Name of Owner!!Percentage Ownership
|-
|1|| [[Sechaba Brewery Holdings]] || 60 
|-
|2|| [[SABMiller|SABMiller Botswana BV]]  || 40
|-
| ||'''Total'''||'''100.00'''
|}
|}
* By virtue of its shareholding in Kgalagadi Breweries Limited and Sechaba Brewery Holdings of 40% and 16.8% respectively, SABMiller's total direct and indirect shareholding in Kgalagadi Breweries Limited and Botswana Breweries Ltd is 50.08%. This makes both companies subsidiaries of SABMiller and associates of Sechaba Brewery Holdings.
==Alcohol levy==
In 2008, [[Ian Khama]] proposed an alcohol levy of 70% to reduce alcoholism in Botswana, there was resistance and the levy was imposed at 30%.  and by November 2009 this had the desired effect causing delines of 35% in lager sales volume and 14% in traditional beer sale volumes.<ref>{{cite web | url=http://www.reuters.com/article/2009/11/18/botswana-brewery-idUSLI10881720091118 | title=Botswana booze tax hits Sechaba Brewery's sales | date=18 Nov 2009 <!-- 8:31am EST -->  | accessdate=27 January 2014 | author=Cropley, Ed}}</ref>
In response to the negative statutory environment the brewery cut costs and maximised efficiencies. Non-alcoholic brands were developed to compensate for the fall in traditional brands. In six month report (September 2013) the levy was at 45% and the decrease in alcohol brands had dropped a further 8%.<ref>{{cite web|url=http://www.botswanaguardian.co.bw/business/730-sechaba-h1-profits-high.html|title=Sechaba H1 profits high|date=29 November 2013|publisher=Botswana Guardian|accessdate=27 January 2014}}</ref> New brands developed include Source bottled water and Keone Mooka Mageu.

==Brands==
* St Louis (1989) 3.5% ABV
* St Louis Export 4.7% ABV
* Castle Lager 5% ABV
* Castle Lite 4% ABV
* Ohlsson’s Lager
* Lion Lager 5% ABV
* Carling Black Label 5.5% ABV

; Traditional Beers (Opaque beers)
* [[Chibuku Shake Shake|Chibuku]]
* Phafana

; Non-alcoholic brands
* Source Water - accredited by the Botswana Bureau of Standards (BOBS)
* Bonaqua Still Water
* Keone Mooka Mageu - a traditional [[Mageu|fermented porridge]]

; Soft Drinks
* Coca-cola
* Coke Zero
* Fanta Orange
* Fanta Grape
* Fanta Pineapple
* Sprite
* Iron Brew
* Ginger Stoney
* Pine Nut
* Minute Maid
* Cream-Soda
* Burn
* Powerade

==Social responsibility programs==
*'''Kickstart''' is an entrepreneur development programme for 18- to 30-year-olds providing mentorship and training to over 107 young entrepreneurs, 80% of participants are still trading.
*'''Project Tshelang''' (Stay alive) launched in 2001 aims to prevent new infections and provide employees and their families with awareness and counselling programs. There are currently 74 trained and active peer educators.

==References==
{{Reflist}}

{{SABMiller}}

[[Category:Beer in Africa]]
[[Category:SABMiller]]
[[Category:Food and drink companies of Botswana]]
[[Category:Companies listed on the Botswana Stock Exchange]]
[[Category:Sorghum]]