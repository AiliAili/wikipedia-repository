{{Taxobox
| status = LC
| status_system = IUCN3.1
| status_ref = <ref name=IUCN>{{IUCN|id=22712236 |title=''Hirundo fuligula'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| image = Rock Martin (Hirundo fuligula) (32682255041).jpg
| image_width = 250px
| image_caption = At the [[Karoo National Park]], [[Western Cape]], South Africa.
| image_alt = A square-tailed brown swallow
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[bird|Aves]]
| ordo = [[passerine|Passeriformes]]
| familia = [[swallow|Hirundinidae]]
| genus = ''[[Ptyonoprogne]]''
| species = '''''P. fuligula'''''
| binomial = ''Ptyonoprogne fuligula''
| binomial_authority = ([[Martin Lichtenstein|Lichtenstein]], 1842)<ref name= Lichtenstein>{{cite book | last =Lichtenstein | first =Martin | title = Verzeichniss einer Sammlung von Säugethieren und Vögeln aus dem Kaffernlande, nebst einer Käffersammlung [Directory of a collection of mammals and birds from the Kaffir country]| year = 1842 |language = German | publisher = Berlin: Royal Academy of Sciences| isbn = |page = 18 }}</ref>
| range_map = Ptyonoprognefuligula3.png
| range_map_width = 250px
| range_map_alt = Map showing the breeding areas in Africa
| range_map_caption =
<div style="text-align:left;"><big>{{Legend2|#008108|Approximate range|border=1px solid #aaa}}</big></div>
| synonyms = ''Hirundo fuligula'' <small>[[Martin Lichtenstein|Lichtenstein]], 1842</small>
}}
The '''rock martin''' (''Ptyonoprogne fuligula'') is a small [[passerine]] [[bird]] in the [[swallow]] family that is resident in central and southern Africa. It breeds mainly in the mountains, but also at lower altitudes, especially in rocky areas and around towns, and, unlike most swallows, it is often found far from water. It is 12–15&nbsp;cm (4.7–5.9&nbsp;in) long, with mainly brown [[plumage]], paler-toned on the upper breast and underwing [[Covert (feather)|coverts]], and with white "windows" on the spread tail in flight. The sexes are similar in appearance, but juveniles have pale fringes to the upperparts and [[flight feather]]s. The former northern [[subspecies]] are smaller, paler, and whiter-throated than southern African forms, and are now usually split as a separate species, the [[pale crag martin]]. The rock martin hunts along cliff faces for flying insects using a slow flight with much gliding. Its call is a soft twitter.

This martin builds a deep bowl nest on a sheltered horizontal surface, or a neat quarter-sphere against a vertical rock face or wall. The nest is constructed with mud pellets and lined with grass or feathers, and may be built on natural sites under cliff overhangs or on man-made structures such as buildings, dam walls, culverts and bridges. It is often reused for subsequent broods or in later years. This species is a solitary breeder, and is not gregarious, but small groups may breed close together in suitable locations. The two or three eggs of a typical [[Clutch (eggs)|clutch]] are white with brown and grey blotches, and are incubated by both adults for 16–19 days prior to hatching. Both parents then feed the chicks. [[Fledge|Fledging]] takes another 22–24 days, but the young birds will return to the nest to roost for a few days after the first flight.

This small martin is caught in flight by several fast, agile [[falcon]] species, such as [[Hobby (bird)|hobbies]], and it sometimes carries parasites, but it faces no major threats. Because of its range of nearly 10&nbsp;million&nbsp;km<sup>2</sup> (4&nbsp;million&nbsp;sq&nbsp;mi) and large, apparently stable, population, it is not seen as vulnerable and is assessed as [[least concern]] on the [[IUCN Red List]].

==Taxonomy==

The rock martin was formally described in 1842 as ''Hirundo fuligula'' by German physician, explorer and zoologist [[Martin Lichtenstein]]<ref name= Lichtenstein/> and was moved to the new genus ''Ptyonoprogne'' by German ornithologist [[Heinrich Gustav Reichenbach]] in 1850.<ref name = reichenbach>Reichenbach (1850) plate LXXXVII figure 6.</ref> Its nearest relatives are the three other members of the genus, the [[pale crag martin]], ''P. obsoleta'' of north Africa, the [[dusky crag martin]] ''P. concolor'' of southern Asia and the [[Eurasian crag martin]] ''P. rupestris''.<ref name = turner158>Turner (1989) pp. 158–164.</ref> The genus name is derived from the [[Ancient Greek]] ''ptuon'' (φτυον), "a fan", referring to the shape of the opened tail, and [[Procne]] (Πρόκνη), a mythological girl who was turned into a swallow.<ref name = bto>{{cite web|title= Crag Martin ''Ptyonoprogne rupestris'' [Scopoli, 1769] |work= Bird facts |url= http://blx1.bto.org/birdfacts/results/bob9910.htm |publisher= [[British Trust for Ornithology]] |accessdate= 28 March 2010}}</ref> The [[specific name (zoology)|specific name]] ''fuligula'' means "sooty-throated", from Latin ''fuligo'' "soot" and ''gula'' "throat".<ref name=chambers>Brookes (2003) pp. 596, 660.</ref>

The three ''Ptyonoprogne'' species are members of the swallow family of birds, and are placed in the Hirundininae subfamily, which comprises all swallows and martins except the very distinctive [[river martin]]s. [[DNA sequence]] studies suggest that there are three major groupings within the Hirundininae, broadly correlating with the type of nest built.<ref name= sheldon>{{cite journal | last=Sheldon | first=Frederick H |authorlink =Fred Sheldon (ornithologist)|author2=Whittingham, Linda A |author3=Moyle, Robert G |author4=Slikas, Beth |author5= Winkler, David W  | year= 2005 | title=Phylogeny of swallows (Aves: Hirundinidae) estimated from nuclear and mitochondrial DNA | journal = [[Molecular Phylogenetics and Evolution|Molecular phylogenetics and evolution]] | volume=35 | issue =1 | pages= 254–270 | doi=10.1016/j.ympev.2004.11.008 | pmid=15737595}}</ref> The groups are the "core martins" including burrowing species like the [[sand martin]], the "nest-adopters", which are birds like the [[tree swallow]] that utilise natural cavities, and the "mud nest builders". The ''[[Ptyonoprogne]]'' species construct open mud nests and therefore belong to the last group. ''[[Hirundo]]'' species also build open nests, ''[[Delichon]]'' house martins have a closed nest, and the ''[[Cecropis]]'' and ''[[Petrochelidon]]'' swallows have [[retort|retort-like]] closed nests with an entrance tunnel.<ref name= sheldon2>{{cite journal | doi=10.1073/pnas.90.12.5705 | last=Winkler | first=David W |author2=Sheldon, Frederick H  | year=1993 | url= http://www.pdfdownload.org/pdf2html/view_online.php?url=http%3A%2F%2Fwww.pnas.org%2Fcontent%2F90%2F12%2F5705.full.pdf | title= Evolution of nest construction in swallows (Hirundinidae): A molecular phylogenetic perspective | journal= Proceedings of the National Academy of Sciences USA| volume=90 | pages=5705–5707 | format=PDF | pmid=8516319 | issue=12 | pmc=46790}}</ref>

The genus ''Ptyonoprogne'' is closely related to the larger swallow genus ''Hirundo'', but a DNA analysis showed that a coherent enlarged ''Hirundo'' genus should contain ''all'' the mud-builder genera. Although the nests of the ''Ptyonoprogne'' crag martins resembles those of typical ''Hirundo'' species like the [[barn swallow]], the DNA research suggested that if the ''Delichon'' house martins are considered to be a separate genus, as is normally the case, ''Cecropis'', ''Petrochelidon'' and ''Ptyonoprogne'' should also be split off.<ref name= sheldon/>

===Subspecies===
There are several subspecies differing in plumage shade or size, although the differences are [[cline (biology)|clinal]], and races interbreed where their ranges meet. The small, pale former subspecies (''obsoleta, peroplasta, perpallida, presaharica, spatzi, arabica'' and ''buchanani'') found in the mountains of [[North Africa]], the [[Arabia]]n peninsula and southwest Asia are now normally split as a separate species, the pale crag martin,<ref name = turner160/> following German [[ornithology|ornithologist]] [[Jean Cabanis]], who first formally described these birds,<ref name= cabanis>{{cite book | last = Cabanis| first = Jean | title = Museum Heineanum Verzeichniss der ornithologischen Sammlung des Oberamtmann Ferdinand Heine auf Gut St. Burchard vor Halberstatdt. Mit kritischen Anmerkungen und Beschriebung der neuen Arten, systematisch bearbeitet von Dr. Jean Cabanis, erstem Kustos der Königlichen zoologischen Sammlung zu Berlin [The directory of the ornithological collection of bailiff Ferdinand Heine at the Museum Heineanum in Gut St. Burchard, Halberstatdt. With critical remarks and descriptions of new species by Dr. Jean Cabanis, first curator of the Royal Zoological collection in Berlin.] | year = 1850|language = German| publisher = Halberstadt: R. Frantz| volume = 1|page = 50}}</ref> but the changes in size and colour are continuous, and the forms often intergrade where they meet, so the evidence for separate species is not strong.<ref name = turner160/> The southern forms of the rock martin can weigh more than twice as much as the smallest northern subspecies of pale crag martin. The average weight for ''P.&nbsp;f.&nbsp;fusciventris'' is 22.4&nbsp;g (0.79&nbsp;oz) against 10&nbsp;g (0.35&nbsp;oz) for ''P.&nbsp;o.&nbsp;obsoleta''.<ref name=" Dunning">Dunning (1993) p.327.</ref> The robust, large-billed southernmost forms (''P. f. fuligula'', ''P. f. pretoriae'', and ''P. f. anderssoni'') are sufficiently different from dark, fine-billed ''P. f. fusciventris'' that the latter could also be regarded as a potentially different species.<ref name= irwin/> However, Rhodesian ornithologist Michael Irwin collected specimens from southern Zimbabwe (then Rhodesia) which were dark above like ''P. f. fusciventris'' and rich reddish below like ''P. f. fuligula''. This led him to suggest that the two groups had previously been isolated, but were probably hybridising following secondary contact.<ref name= irwin>{{cite journal| last= Irwin | first= Michael P S | year= 1977| title= Variation, geographical arcs and gene-flow within the populations of the rock martin ''Hirundo (Ptyonoprogne) fuligula'' in eastern, southern and south-western Africa | journal= Honeyguide | volume= 91| issue = | pages= 10–19 | url = | doi = }}</ref>

{| style="width:98%;" class="wikitable"
! style="text-align:center; background:#d3d3a4;" colspan="4"|Subspecies{{efn|The table is based on Turner (1989).<ref name = turner160>Turner & Rose (1989) pp. 160–163.</ref> Parentheses indicate that the scientific name has changed from that originally given.}}
|-
!width=16% | Subspecies
!width=20% | Authority
!width=28% | Range
!width=36% | Comments
|-
|''P. f. fuligula''
|(Lichtenstein, 1842)
|[[Eastern Cape]].
|The [[nominate subspecies]].
|-
|''P. f. pusilla''
|([[Otto von Zedlitz|Zedlitz]], 1908)
|Mali to western Sudan and most of Ethiopia.
|Paler plumage and smaller than the nominate subspecies.
|-
|''P. f. fusciventris''
|([[Jack Vincent|Vincent]], 1933)
|Southern Sudan and Ethiopia south to northern Mozambique.
|Smaller than ''P. f. pusilla'' with dark plumage.
|-
|''P. f. bansoensis''
|([[David Armitage Bannerman|Bannerman]], 1923)
|West and central Africa.
|Small, and very dark plumage.
|-
|''P. f. anderssoni''
|([[Richard Bowdler Sharpe|Sharpe]] & [[Claude Wilmot Wyatt|Wyatt]], 1887)
|Southwestern Cape north to southern Angola.
|Size similar to nominate, but paler plumage.
|-
|''P. f. pretoriae''
|[[Austin Roberts|Roberts]], 1922
|Eastern South Africa.
|Plumage as nominate, but larger.
|-
|}

==Description==
[[File:2008 02 27 Rock Martin.jpg|thumbnail|''H. fuligata pretoriae'' in flight]]
The rock martin of the nominate subspecies ''P. f. fuligula'' is 12–15&nbsp;cm (4.7–5.9&nbsp;in)<ref name =SASOL/> long with earth-brown upperparts and a short square tail that has small white patches near the tips of all but the central and outermost pairs of feathers. It has a cinnamon chin, throat, upper breast and underwing [[Covert (feather)|coverts]], with the rest of the underparts being a similar brown to the upperparts. The eyes are brown, the small bill is mainly black, and the legs are brownish-pink. The sexes are similar in appearance, but juveniles have pale edges to the upperparts and flight feathers. The other subspecies differ from the nominate form as detailed above.<ref name = turner160/><!-- This ref applies to all the preceding -->

The rock martin's flight is slow, with rapid wing beats interspersed with flat-winged glides, and it is more acrobatic than the larger Eurasian crag martin. It is a quiet bird; the song is a muffled twitter, and other calls include a ''trrt'' resembling the call of the [[common house martin]], a nasal ''vick'',<ref name = collins>Mullarney ''et al.'' (1999) p. 240.</ref> and a high pitched ''twee'' contact call.<ref name = turner160/>

The rock martin is much drabber than most African swallows, and confusion is unlikely except with other crag martins or with sand martins of the genus ''[[Riparia]]''.<ref name = turner160/> The pale crag martin is smaller, paler and greyer than its southern relative.<ref name = bwp/> Although only slightly larger than the sand martin and [[brown-throated sand martin]], the rock martin is more robust, has white tail spots, and lacks a breast band.<ref name = bwp/> It is paler on the throat, breast and underwings than the all-dark form of the brown-throated sand martin.<ref name= SASOL>Sinclair ''et al.'' (2002) p. 298.</ref>

==Distribution and habitat==

The rock martin breeds in suitable habitat in Africa north to Nigeria, Chad and Ethiopia. It is largely resident apart from local movements or a descent to lower altitudes after breeding. This species has been recorded as a [[vagrancy (biology)|vagrant]] in Gabon, and its status in Congo is uncertain.<ref name=IUCN/><ref name = bwp>Snow & Perrins (1998) pp. 1058–1059.</ref><ref name= birdlife>{{cite web | title = Species factsheet ''Hirundo fuligula '' | publisher = [[BirdLife International]] | url = http://www.birdlife.org/datazone/species/index.html?action=SpcHTMDetails.asp&sid=7114&m=1 }} Retrieved 21 November 2012.</ref>

The natural breeding habitat is hilly or mountainous country with cliffs, gorges and caves up to 3,700&nbsp;m (12,000&nbsp;ft) above sea level,<ref name = baker2>Baker (1926) pp. [https://archive.org/stream/BakerFbiBirds3/BakerFBI3#page/n261/mode/1up/ 238–239]</ref> but this martin also breeds in lowlands, especially if rocks or buildings are available, and may be found far from water. It readily uses man-made structures as a substitute for natural precipices.<ref name = turner160/>

==Behaviour==

===Breeding===

Rock martin pairs often nest alone, although where suitable sites are available small loose [[Bird colony|colonies]] may form with up to 40 pairs. These martins aggressively defend their nesting territory against [[Conspecificity|conspecifics]] and other species. Breeding dates vary geographically and with local weather conditions. Two broods are common, and three have been raised in a season. Breeding mainly August to September. The nest, built by both adults over several weeks, is made from several hundred mud pellets and lined with soft dry grass or sometimes feathers. It may be a half-cup when constructed under an overhang on a vertical wall or cliff, or bowl-shaped like that of the barn swallow when placed on a sheltered ledge. The nest may be built on a rock cliff face, in a crevice or on a man-made structure, and is re-used for the second brood and in subsequent years.<ref name = turner160/><ref name = baker2/><!-- Turner ref applies to all preceding -->

The clutch is usually two or three buff-white eggs blotched with sepia or grey-brown particularly at the wide end. The average egg size in South Africa was 20.8&nbsp;x&nbsp;14.1&nbsp;mm (0.82&nbsp;x&nbsp;0.56&nbsp;in) with a weight of 2.17&nbsp;g (0.077&nbsp;oz). Both adults incubate the eggs for 16–19 days prior to hatching and feed the chicks about ten times an hour until they [[fledge]] and for several days after they can fly. The fledging time can vary from 22–24&nbsp;days to 25–30&nbsp;days, though the latter estimates probably take into account fledged young returning to the nest for food.<ref name = turner160/><!-- Ref applies to all preceding -->

===Feeding===

The rock martin feeds mainly on insects caught in flight, although it will occasionally feed on the ground. When breeding, birds often fly back and forth along a rock face catching insects in their bills and feed close to the nesting territory. At other times, they may hunt low over open ground. The insects caught depend on what is locally available, but may include [[mosquito]]es and other [[fly|flies]], [[Hymenoptera]], [[ant]]s and [[beetle]]s. This martin often feeds alone, but sizeable groups may gather at [[wildfire|grass fire]]s to feast on the fleeing insects, and outside the breeding season flocks of up to 300 may form where food is abundant.<!-- Ref applies to all preceding --><ref name = turner160/> Cliff faces generate [[standing wave]]s in the airflow which concentrate insects near vertical areas. Crag martins exploit the area close to the cliff when they hunt, relying on their high manoeuvrability and ability to perform tight turns.<ref name= fantur>{{cite journal | last= Fantur | first= von Roman | year= 1997| title= Die Jagdstrategie der Felsenschwalbe (''Hirundo rupestris'') [The hunting strategy of the crag martin] | journal= Carinthia | volume= 187| issue = 107 | pages= 229–252| url = http://www.biologiezentrum.at/pdf_frei_remote/CAR_187_107_0229-0252.pdf | language = German, English | doi = }}</ref>

A study of nine bird species including four hirundines showed that the more young there are in a nest, the more frequent are the parents' feeding visits, but the visits do not increase in proportion to the number of young. On average a solitary nestling therefore gets more food than a member of a pair or of a trio. Since the nestling period is not prolonged in proportion to the drop in feeding rate, an individual fledgling from a larger brood is likely to weigh less when it leaves the nest. However, a subspecies of the rock martin (''P. f. fusciventris'') was an anomaly in respect of both feeding rate and nestling time. There was no difference in parental feeding rate for members of a pair and members of a trio, but the nestling period averaged 1.5&nbsp;days longer for trios than pairs.<ref name= Moreau >{{cite journal | last= Moreau | first= R E | year= 1947| title= Relations between number in brood, feeding-rate and nestling period in nine species of birds in Tanganyika Territory | journal = Journal of Animal Ecology | volume= 16| issue = 2| pages= 205–209| doi =10.2307/1495 | jstor=1495}}</ref>

==Predators and parasites==
Some [[falcon]]s have the speed and agility to catch swallows and martins in flight, and rock martins may be hunted by species such as the [[peregrine falcon]],<ref name= peregrine>Simmons, Robert E; Jenkins, Andrew R; Brown Christopher J "[http://www.fitzpatrick.uct.ac.za/pdf/Peregrine_Falcon_Pop_Procs_2008_99-108.pdf A review of the population status and threats to Peregrine Falcons throughout Namibia]" in Sielicki & Mizera (2008) pp. 99–108</ref> [[Taita falcon]],<ref name= Dowsett >{{cite journal | last= Dowsett | first= R J | title= Breeding and other observations on the Taita Falcon ''Falco fasciinucha'' | journal= Ibis | volume= 125| issue = 3 | pages= 362–366| url = | doi = 10.1111/j.1474-919X.1983.tb03122.x | year= 2008 }}</ref> [[African hobby]] and wintering [[Eurasian hobby]].<ref name = Barlow165>Barlow ''et al.'' (1997) p. 165.</ref> Rock martins often share their nesting sites with [[little swift]]s,<ref name= Chantler>Chantler & Driessens (2000) p. 241</ref> which sometimes forcibly take over the martin's nests.<ref name= Carr >{{cite journal| last= Carr | first= B A | year= 1984| title= Nest eviction of rock martins by little swifts | journal= Ostrich | volume= 55| issue = 4 | pages= 223–224 | url = | doi = 10.1080/00306525.1984.9634491}}</ref> In 1975, one of the first findings of the tick ''Argas (A.) africolumbae'' was in a nest of ''Ptyonoprogne f. fusciventris'' in [[Kenya]],<ref name= Hoogstraal >{{cite journal | last= Hoogstraal | first= Harry |author2=Kaiser, Makram N |author3=Walker, Jane B |author4=Ledger, John A |author5=Converse, James D |author6= Rice, Robin G A  |date=June 1975| title= Observations on the subgenus Argas (Ixodoidea: Argasidae: ''Argas'') 10. ''A. (A.) africolumbae'', n. sp., a Pretoria virus-infected parasite of birds in southern and eastern Africa | journal= Journal of Medical Entomology | volume= 12| issue = 2| pages= 194–210. | url = http://www.ingentaconnect.com/content/esa/jme/1975/00000012/00000002/art00006 | doi = }}</ref> at that time the martin was described under its [[synonym (taxonomy)|synonym]] ''Ptyonoprogne fuligula rufigula'' ([[Gustav Fischer|Fischer]] & [[Anton Reichenow|Reichenow]]).<ref name= Bergier>{{cite journal | last= Bergier | first= Patrick | year= 2007| title= L'Hirondelle isabelline ''Ptyonoprogne fuligula'' au Maroc [The Rock Martin in Morocco]| journal= Go-South Bulletin | volume= 4| issue = |language = French | pages= 6–25 | url = | doi = }}</ref>

==Status==
The rock martin has a very large range of 9.5&nbsp;million&nbsp;km<sup>2</sup> (3.7&nbsp;million&nbsp;sq&nbsp;mi). The total population is unknown, but the bird is described as generally common, although scarce in Botswana and Namibia. The population is thought to be stable, mainly due to the absence of evidence of any declines or substantial threats. Its large range and presumably high numbers mean that the rock martin is not considered to be threatened, and it is classed as [[least concern]] on the [[IUCN Red List]].<ref name=IUCN/>

==Notes==
{{notelist}}

==References==
{{reflist|colwidth=30em}}

==Cited texts==
* {{cite book|pages= |url= | title=Fauna of British India. Birds | volume=3 |edition=2nd |author=Baker, E C S | year=1926 | publisher= Taylor and Francis| location = London }}
* {{cite book |title=''A field guide to birds of The Gambia and Senegal'' |last= Barlow |first=Clive |author2= Wacher, Tim|author3=Disley, Tony |publisher=Pica Press|location = Robertsbridge | year=1997 |isbn= 1-873403-32-1 }}
* {{cite book|last=Brookes|first=Ian (editor-in-chief)|title=The Chambers Dictionary|year = 2003 | publisher = Chambers Harrap| location =Edinburgh| isbn =0-550-10013-X |edition = ninth}}
* {{cite book| last = Chantler | first = Phil |author2=Driessens, Gerald  | title = Swifts | year = 2000| publisher = Pica | location = Robertsbridge | isbn = 1-873403-83-6}}
* {{cite book | last = Dunning | first = John Barnard | title = CRC handbook of avian body masses | year = 1993 | publisher = CRC Press, Inc.| location = Boca Raton | isbn = 0-8493-4258-9}}
* {{cite book |last = Mullarney |first = Killian |author2=Svensson, Lars|author3=Zetterstrom, Dan|author4=Grant, Peter |title = Collins Bird Guide |year = 1999 |location = London |publisher = HarperCollins |isbn = 0-00-219728-6}}
* {{cite book | last =Reichenbach | first = Heinrich Gustav | title = Avium systema naturale | language=German| year=1850|publisher=F. Hofmeister | location = Dresden and Leipzig: }}
* {{cite book | last = Sielicki | first = Janusz |editor=Mizera, Tadeusz| title = Peregrine Falcon Populations – status and perspectives in the 21st Century | year = 2008 | publisher = Turul and University of Life Sciences | location = Warsaw and Poznań | isbn =978-83-920969-6-2}}
*{{cite book|last=Sinclair|first=Ian|author2=Hockey, Phil |author3=Tarboton, Warwick |title=SASOL Birds of Southern Africa|year=2002| publisher = Struik | location = Cape Town | isbn=1-86872-721-1 }}
* {{cite book |last = Snow |first = David |editor= Perrins, Christopher M |title = The Birds of the Western Palearctic concise edition (2 volumes) | publisher = Oxford University Press |year = 1998 |location = Oxford |isbn = 0-19-854099-X }}
* {{cite book | last = Turner | first = Angela K |author2=Rose, Chris  | title = A Handbook to the Swallows and Martins of the World | year = 1989 | publisher = Christopher Helm | location = London | isbn = 0-7470-3202-5 }}

==External links==
{{commons category|Ptyonoprogne fuligula}}
{{wikispecies|Ptyonoprogne obsoleta}}
* Rock Martin - [http://sabap2.adu.org.za/docs/sabap1/529.pdf Species text in The Atlas of Southern African Birds].

{{Hirundinidae}}

{{featured article}}
{{taxonbar}}

{{DEFAULTSORT:martin, rock}}
[[Category:Ptyonoprogne|rock martin]]
[[Category:Birds of Sub-Saharan Africa]]
[[Category:Birds described in 1842|rock martin]]