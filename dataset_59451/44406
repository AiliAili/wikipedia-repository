{{redirects here|New Castle Airport}}
{{about||the military use of this facility|New Castle Air National Guard Base}}
{{Infobox airport
| name         = Wilmington Airport
| image        = File:New Castle Airport Logo.jpg
| caption      =
| image2       = New Castle Airport - Delaware.jpg 
| caption2      = 2006 [[USGS]] aerial photo 
| IATA         = ILG
| ICAO         = KILG
| FAA          = ILG
| WMO = 72418
| type         = Public
| owner        = [[New Castle County, Delaware]]
| operator     = [[Delaware River and Bay Authority]]
| city-served  = [[Wilmington, Delaware]]
| location     = <!--if different than above-->
| elevation-f  = 80
| website      = http://flywilmilg.com
| coordinates  = {{coord|39|40|43|N|075|36|24|W|region:US-DE|display=inline,title}}
| pushpin_map            = USA Delaware#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Delaware/United States
| pushpin_label          = '''ILG'''
| pushpin_label_position = top
| r1-number    = 9/27
| r1-length-f  = 7,275
| r1-surface   = Asphalt
| r2-number    = 1/19
| r2-length-f  = 7,012
| r2-surface   = Asphalt
| r3-number    = 14/32
| r3-length-f  = 4,602
| r3-surface   = Asphalt
| stat-year    = 2013
| stat1-header = Passenger enplanements
| stat1-data   = 52,456
| stat2-header = Aircraft operations
| stat2-data   = 53,255
| stat3-header = Based aircraft
| stat3-data   = 223
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=ILG|use=PU|own=PU|site=02999.*A}}. Federal Aviation Administration. Effective November 15, 2012.</ref>
}}

'''Wilmington Airport''' {{airport codes|ILG}} (also known as '''Wilmington/Philadelphia Regional Airport''', '''New Castle County Airport''', or '''New Castle Airport''') is an [[airport]] located in [[unincorporated area|unincorporated]] [[New Castle County, Delaware]] near [[Wilmington, Delaware|Wilmington]], [[Delaware]].<ref name="FAA" /> Owned by the [[Delaware River and Bay Authority]], it is five miles (8 km) south of Wilmington and about 30 miles (50 km) from [[Philadelphia, Pennsylvania|Philadelphia]].<ref name="FAA" /> It is included in the [[Federal Aviation Administration]] (FAA) [[National Plan of Integrated Airport Systems]] for 2017–2021, in which it is [[FAA airport categories|categorized]] as a non-hub primary commercial service facility.<ref name="NPIAS Airports">{{cite web|title=List of NPIAS Airports|url=https://www.faa.gov/airports/planning_capacity/npias/reports/media/NPIAS-Report-2017-2021-Appendix-A.pdf|website=FAA.gov|publisher=Federal Aviation Administration|accessdate=27 November 2016|format=PDF|date=21 October 2016}}</ref>

[[Federal Aviation Administration]] records say the airport had 642 passenger boardings (enplanements) in [[calendar year]] 2011 and 1,064 passenger boardings in 2012.<ref>
 {{cite web
 | url = http://www.faa.gov/airports/planning_capacity/passenger_allcargo_stats/passenger/
 | title = Passenger Boarding (Enplanement) and All-Cargo Data for U.S. Airports
 | publisher = Federal Aviation Administration 
 | accessdate = November 19, 2013
 }}
</ref> Thanks to the inauguration of service by [[Frontier Airlines]], 2013 enplanements increased to 52,456,<ref name="nj.com">
 {{cite news
 | url = http://www.nj.com/salem/index.ssf/2013/04/frontier_airlines_to_begin_com.html
 | title = Frontier Airlines to begin commercial service from Delaware's New Castle Airport
 | work = South Jersey Times | date = April 9, 2013
 }}</ref> though Frontier ended its Delaware service in 2015.

== History ==
The airport opened before World War II, named the '''Wilmington Airport''' and the '''Greater Wilmington Airport'''. The facility was taken over by the [[United States Army Air Forces]] during the war. Under USAAF control, the airport became '''New Castle Army Air Base'''. Its mission was to facilitate the movement of aircraft to the British and other Allies. Members of the historic Women Air Service Pilots (WASP) served as test and ferry pilots and towed targets for student gunners. There is a statue today at the airport that honors the women of the WASP that served their country in the time of need.<ref>{{Air Force Historical Research Agency}}</ref>

After the war ended control of the airport was returned to civil authorities. A joint-use agreement was made between the [[United States Air Force]] and [[New Castle County, Delaware|New Castle County]] authorities for a portion of the airport being retained for an [[Air National Guard]] Base. [[Trans World Airlines]] ([[TWA]]) operated a large overhaul base for its overseas planes at the airport until 1957 when the airline moved it to the [[Kansas City Overhaul Base]] which became the basis for today's [[Kansas City International Airport]].<ref>
 {{cite news
 | url = https://news.google.com/newspapers?nid=2294&dat=19531213&id=M84mAAAAIBAJ&sjid=jAIGAAAAIBAJ&pg=987,5232914
 | title = Delawareans Helped To Pioneer Flying; 50th Anniversary to Be Observed Here
 | work = Wilmington Sunday Star | date = December 13, 1953
 }}
</ref>

Delaware's first airline flights were operated by TWA and [[American Airlines]] at Wilmington in late 1947.

By 1967, [[Eastern Airlines]] was operating [[Douglas DC-9]] jet service into the airport with nonstops to New York [[Newark Airport]], Philadelphia and Washington D.C. [[National Airport]] as well as direct flights to Atlanta and Charlotte.<ref>http://www.timetableimages.com, June 13, 1967 Eastern Airlines system timetable</ref>

=== 1990s to present ===
[[File:Airbus A319-111, Frontier Airlines AN1394169.jpg|thumbnail|[[Frontier Airlines]] was the last airline to serve the airport]]
In the late 1990s the county leased the debt-stricken airport to the bi-state [[Delaware River and Bay Authority]] (DRBA), operators of the [[Delaware Memorial Bridge]], on a thirty-year lease with the provision that the DRBA may seek up to two additional thirty-year leases. Since taking over operations, the DRBA made the airport profitable, upgraded many aging buildings, and built numerous new buildings and facilities on the property.

From 1991 through 1998, again from 2000 to 2006, from April 2008 until June 30, 2013, and since April 2015, Delaware has been the only U.S. state without any scheduled airline flights.

[[United Airlines]] served Wilmington until 1991. [[US Airways Express|USAir Express]] carrier [[Crown Airways]] provided scheduled service to [[Mid-Ohio Valley Regional Airport|Parkersburg, West Virginia]], from 1992 to 1993.{{citation needed|date=August 2013}}

[[Shuttle America]] offered scheduled flights out of Wilmington from the airline's founding in November 1998 until February 2000. They flew to [[Bradley International Airport|Hartford]], [[Buffalo Niagara International Airport|Buffalo]], and [[Norfolk International Airport|Norfolk]] with 50-seat [[de Havilland Canada DHC-8 Dash 8]]-300 turboprops. Shuttle America would eventually discontinue its independent operations and become a commuter affiliate of [[United Express]] and [[Delta Connection]].

On June 29, 2006, a [[Delta Air Lines]] regional airline affiliate began flights from [[Atlanta, Georgia|Atlanta's]] [[Hartsfield-Jackson Atlanta International Airport]] to New Castle Airport, the first airline service in six years. [[Delta Connection]] carrier [[Atlantic Southeast Airlines]] flew 50-seat [[Canadair CRJ]] regional jets on two daily roundtrip flights. Delta Air Lines ended the Wilmington flights on September 6, 2007, leaving Delaware without any airline service.

On March 8, 2008, [[Skybus Airlines]] began [[Airbus A319]] jet flights from [[Columbus, Ohio]] and [[Greensboro, North Carolina]] to Wilmington. Skybus ceased all operations effective April 4, 2008,<ref name="MSNBC-1">{{cite news | url = http://www.msnbc.msn.com/id/23962964/ | title = Low-cost carrier Skybus calls it quits | work = MSNBC | date = April 4, 2008 | accessdate = April 5, 2008}}</ref> once again leaving New Castle Airport without any airline service. As of August 4, 2010, [[Avis Rent a Car System, LLC]], [[Budget Rent A Car System, Inc.]], and Cafe Bama were the only tenants in the Main Terminal.

On July 1, 2013, [[Frontier Airlines]] began [[Airbus A320]] jet service at Wilmington, initially with flights to Denver, Chicago-Midway, Houston-Hobby, Orlando, and Tampa.<ref>{{cite news |last1=Mutzabaugh |first1=Ben |title=Frontier Airlines puts Delaware back on USA's flight map |url=http://www.usatoday.com/story/todayinthesky/2013/07/02/frontier-airlines-puts-delaware-back-on-usas-flight-map/2484543/ |accessdate=2014-08-02 |work=[[USA Today]] |date=2013-07-02}}</ref> On June 26, 2013, Frontier announced nonstop jet service to [[Fort Myers, Florida|Fort Myers]] would begin November 16. <ref name="Frontier June 2013">
 {{cite press release
 | url = http://ca.finance.yahoo.com/news/frontier-airlines-adds-nonstop-route-140000465.html
 | title = Frontier Airlines Adds New Nonstop Route Between Wilmington/Philadelphia and Fort Myers, Fla.
 | publisher = Frontier Airlines | date = June 26, 2013
 }}
</ref> In June 2015, it was announced that Frontier Airlines would end service because it was not a profitable operation. Service had actually stopped in April 2015, but at that time, Frontier claimed it was just a seasonal suspension of service.<ref>{{cite news |last1=Goss |first1=Scott |title=Frontier Airlines' pullout leaves Delaware with no flights |url=http://www.usatoday.com/story/todayinthesky/2015/06/29/frontiers-pullout-leaves-delaware-without-airline-service/29462763/ |accessdate=July 1, 2015 |work=[[USA Today]] |date=June 30, 2015}}</ref>

== Facilities==
The airport covers 1,250 [[acre]]s (506 [[hectare|ha]]) at an [[elevation]] of 80 feet (24 m). It has three asphalt [[runway]]s: 9/27 is 7,275 by 150 feet (2,217 x 46 m); 1/19 is 7,012 by 150 feet (2,137 x 46 m); 14/32 is 4,602 by 150 feet (1,403 x 46 m).<ref name="FAA" />

In the year ending October 31, 2011, the airport had 67,328 aircraft operations, average 184 per day: 85% [[general aviation]], 10% military, 5% [[air taxi]], and <1% airline. 219 aircraft were then based at the airport: 48% single-engine, 30% jet, 11% multi-engine, 9% military, and 2% [[helicopter]].<ref name="FAA" />

== See also ==
* [[List of airports in Delaware]]
{{Portalbar|Delaware|Philadelphia<!--Metro Philadelphia-->|Aviation}}

==References==
{{Reflist}}

==External links==
* Official sites: [http://www.flywilmilg.com/ FlyWilmILG.com] and [http://www.newcastleairportilg.com/ NewCastleAirportILG.com]
* {{cite web|url= http://www.deldot.gov/information/community_programs_and_services/airports/pdfs/new_castle_airport.pdf |title=New Castle Airport (ILG) }} at [[Delaware DOT]] website
* [[Fixed-base operator]]s (FBOs): [http://www.hawkerbeechcraft.com/customer_support/hawker_beechcraft_services/wilmington_de/ Hawker Beechcraft Services], [http://www.aerotaxi.com/ Aero-Taxi], [http://www.aerowaysinc.com/ AeroWays], [http://www.dassaultfalcon.com/en/CustomerService/worldwide-presence/Pages/Dassault-Aircraft-Services-Wilmington.aspx Dassault Aircraft Services], [http://www.atlanticaviation.com/Locations/ILG.aspx Atlantic Aviation]
* [http://msrmaps.com/map.aspx?t=1&s=12&lat=39.6792&lon=-75.6076&w=800&h=800&lp=---+None+--- Aerial image as of March 1992] from [[USGS]] ''[[The National Map]]''
* {{FAA-diagram|00458}}
* {{FAA-procedures|ILG}}
* {{US-airport|ILG}}

<!--Navigation box--><br />
{{DRBA facilities}}

[[Category:Airports in Delaware]]
[[Category:Delaware River and Bay Authority facilities]]
[[Category:Buildings and structures in New Castle County, Delaware]]
[[Category:Transportation in New Castle County, Delaware]]