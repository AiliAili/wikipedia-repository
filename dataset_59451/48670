'''Pietro Ichino''' is a professor of [[Labor Law]] at the University of Milan ([[Università degli Studi di Milano|Università Statale di Milano]]). From 1979 to 1983 he was an independent left-wing MP belonging to the ranks of the [[Italian Communist Party]]. In 2008 he was elected Senator for the [[Democratic Party (Italy)]] in the district of Lombardy.<ref name="pietroichino">http://www.pietroichino.it/?page_id=6</ref>

==Biography==
He is brother of the economist [[Andrea Ichino]].<ref>http://ricerca.repubblica.it/repubblica/archivio/repubblica/2009/03/18/neanche-nostri-impiegati-sono-perfetti-assenza.html</ref> He has been married since 1973 to Costanza Rossi, with whom he has had two daughters. After graduating in [[law]], he was a trade union leader, from 1969 to 1972, of the Fiom – Italian General Confederation of Labour (CGIL) [[trade union]]. After his military service, he was Head of Legal Services of the Labor Chamber of Milan from 1973 to 1979. He has said that it was his meeting with [[Don Lorenzo Milani]] that made him decide to study law and to commit himself to the trade union movement.<ref>[http://www.pietroichino.it/?p = 19483'' How I met Don Lorenzo Milani'']</ref>
Since 1970 has been a journalist and since 1997 he has been a columnist for the Italian newspaper'' [[Corriere della Sera]]''. From April 1998 to March 1999, he collaborated with the then Communist Party newspaper “l'Unità”. In 1975 he was admitted to the Milan Bar. It was during the VIII Legislature of the [[Italian Republic]], from 1979 to 1983, that he was elected as an independent left-wing MP belonging to the ranks of the [[Italian Communist Party]]. He was a [[Researcher]] from 1983 to 1986 in the University of Milan ([[Università degli Studi di Milano|Università statale di Milano]]). Subsequently, he became an Associate (1986–1989) and Full (1989–1991) [[Professor]] of Labor Law at the University of Cagliari. Since 1991 he has been Full Professor of Labor Law at University of Milan ([[Università degli Studi di Milano|Università Statale di Milano]]). In 1985 he was appointed coordinator of the "Italian Journal of Labor Law," of which he has been deputy editor since 1991 and editor in chief since 2002. Since 2003 he has been a member of the executive board of the law journal "Civil Justice".<ref name="pietroichino" />
On 2 June 2006 the President of the Republic Carlo Azeglio Ciampi awarded him the title of "commendatore" for the contribution given by his studies and teaching.<ref>http://www.quirinale.it/qrnw/statico/ex-presidenti/Ciampi/dinamico/comunicato.asp?id=27225</ref> He participated in the foundation of the [[Democratic Party (Italy)]] and, as a candidate of this party, he was elected to the Italian Senate in 2008.<ref>http://www.senato.it/leg/16/BGT/Schede_v3/Attsen/00011320.htm</ref>
On December 24, 2012, after having decided not to run in the Democratic Party primaries for the selection of the 2013 General Election candidates, he announced his decision to run for the [[Monti's Agenda for Italy]] electoral coalition in the Lombardy constituency. Among the reasons given for this decision, he cited the underlying ambiguity of the Democratic Party's stance towards the European Union.<ref>[http://www.pietroichino.it/?p=24752 ''La scelta della chiarezza'']</ref>

==Threats ==
Pietro Ichino has been living for some years now under police protection because of threats that some members of the Red Brigades have explicitly addressed to him and due to the fact that two of his colleagues ([[Massimo D'Antona]] and [[Marco Biagi (jurist)|Marco Biagi]]), who worked on the same subjects dealt with by Ichino, have been murdered by the New Red Brigades.  “In Italy, whoever touches the Workers’ Statute dies.” This phrase was uttered by Ichino during his deposition at the trial of the New Red Brigades, in [[Milan]] on January 23, 2009<ref>http://www.corriere.it/cronache/09_gennaio_23/processo_br_ichino_contestato_dai_terroristi_f235b694-e932-11dd-8250-00144f02aabc.shtml.</ref>

==Position on labor law reform ==

Professor Ichino is a supporter of a wholesale reform of employment contracts, which would entail that – as far as all newly hired employees are concerned – short-term and project-based employment contracts would be abolished. Employment agreements without a term would become the normal form of recruitment, which would have a trial period of six months and a system of increasing protection during the course of the employment relationship. He has also proposed a reform of the legislation on dismissals and layoffs. He clarified his ideas on the labor market on the economics website [[lavoce.info]], of which he is one of the editors.<ref>http://www.lavoce.info/ lavocepuntoinfo/autori/pagina10.html</ref>
Professor Ichino supports a shift away from national collective employment agreements to collective bargaining between companies and trade unions.<ref>http://www.pietroichino.it/?p=9908</ref>

== Works==
*''Diritto del lavoro per i lavoratori'', 2 voll., Bari, De Donato, 1975–1977.
*''Diritto alla riservatezza e diritto al segreto nel rapporto di lavoro. La disciplina giuridica della circolazione delle informazioni nell'impresa'', Milano, Giuffrè, 1979.
*''Il collocamento impossibile. Problemi e obiettivi della riforma del mercato del lavoro'', Bari, De Donato, 1982.
*''Il tempo della prestazione nel rapporto di lavoro'', 2 voll., Milano, Giuffrè, 1984–1985.
*''L'orario di lavoro e i riposi'', Milano, Giuffrè, 1987. ISBN 88-14-01035-8.
*''Esercitazioni di diritto del lavoro'', Milano, Giuffrè, 1988. ISBN 88-14-01656-9; 1992. ISBN 88-14-03930-5.
*''Nuovi strumenti di intervento nel mercato del lavoro. La promozione del lavoro degli handicappati e degli anziani. I job clubs. Il job sharing'', a cura di e con [[Andrea Ichino]] e [[Margherita Violi]], Milano, FrancoAngeli, 1988. ISBN 88-204-2868-7.
*''Rilevanza della volontà negoziale ai fini della qualificazione del contratto di lavoro'', Milano, Giuffrè, 1989. ISBN 88-14-01880-4.
*''Subordinazione e autonomia nel diritto del lavoro'', Milano, Giuffrè, 1989. ISBN 88-14-02096-5.
*''Il lavoro subordinato. Definizione e inquadramento'', Milano, Giuffrè, 1992. ISBN 88-14-03116-9.
*''Strategie di comunicazione e Statuto dei lavoratori. I limiti del dialogo tra impresa e dipendenti'', ricerca diretta da, Milano, Giuffrè, 1992. ISBN 88-14-03572-5.
*''Lavoro interinale e servizi per l'impiego. Il nuovo quadro di riferimento'', a cura di, Milano, Giuffrè, 1995. ISBN 88-14-05305-7.
*''Il lavoro e il mercato. Per un diritto del lavoro maggiorenne'', Milano, Mondadori, 1996. ISBN 88-04-42281-5. (Premio Giancarlo Capecchi – [[Intersind]] 1997; Premio Walter Tobagi 1997 per la saggistica)
*''Il diritto del lavoro. In 500 domande e risposte'', Milano, Giuffrè, 1997. ISBN 88-14-06405-9.
*''Trattato di diritto civile e commerciale'', XXVII.2, ''Il contratto di lavoro. 1, Fonti e principi generali, autonomia individuale e collettiva, disciplina del mercato, tipi legali, decentramento produttivo, differenziazione dei trattamenti e inquadramento'', Milano, Giuffrè, 2000. ISBN 88-14-08190-5.
*''Trattato di diritto civile e commerciale'', XLVI, ''Il contratto di lavoro. 2, Soggetti e oggetto del contratto, sicurezza del lavoro, retribuzione, qualità, luogo e tempo della prestazione lavorativa'', Milano, Giuffrè, 2003. ISBN 88-14-10038-1.
*''Trattato di diritto civile e commerciale'', XLVII, ''Il contratto di lavoro. 3, Sospensione del lavoro, sciopero, riservatezza e segreto, potere disciplinare, cessazione del rapporto, conservazione e gestione dei diritti'', Milano, Giuffrè, 2003. ISBN 88-14-10100-0.
*''Lezioni di diritto del lavoro. Un approccio di labour law and economics'', Milano, Giuffrè, 2004. ISBN 88-14-10955-9.
*''A che cosa serve il sindacato? Le follie di un sistema bloccato e la scommessa contro il declino'', Milano, Mondadori, 2005. ISBN 88-04-54970-X.
*''I nullafacenti. Perché e come reagire alla più grave ingiustizia della nostra amministrazione pubblica'', Milano, Mondadori 2006. ISBN 88-04-56592-6; Oscar Mondadori, 2008. ISBN 978-88-04-57468-2. (Premio Tarantelli 2007; Premio dell'Associazione per il Progresso Economico 2007 per la migliore idea economica dell'anno)
*''Il diritto del lavoro nell'Italia repubblicana. Teorie e vicende dei giuslavoristi dalla Liberazione al nuovo secolo'', a cura di, di [[Riccardo Del Punta]], [[Raffaele De Luca Tamajo]] e [[Giuseppe Ferraro]]; con interviste a [[Gino Giugni]], [[Federico Mancini]], [[Luigi Mengoni]], [[Giuseppe Pera]] e [[Renato Scognamiglio]]), Milano, Giuffrè, 2008. ISBN 88-14-13621-1.
*''Inchiesta sul lavoro. Perché non dobbiamo avere paura di una grande riforma'', Milano, Mondadori, 2011. ISBN 978-88-04-61425-8.

==See also==
{{Portal|biography|law |politics}}
*[[Brigate Rosse]]

==Notes==
<references />

==External links  ==
*{{ cite web 
|url= http://www.pietroichino.it/
|title = Pietro Ichino
|publisher  = Official Website
|accessdate = 2009-01-23
}}
*{{cite web
|url= http://www.senato.it/leg/16/BGT/Schede/Attsen/00011320.htm
|title= Scheda di attività di Pietro Ichino – XVI Legislatura
|publisher = Senate of the Republic (Italy)
|accessdate = 2009-01-23
}}
*{{cite web
|url= http://legislature.camera.it/chiosco.asp?cp=1&position=VIII%20Legislatura%20/%20I%20Deputati&content=deputati/legislatureprecedenti/Leg08/framedeputato.asp?Deputato=1d22260
|title= Dati personali e incarichi nella VIII Legislatura
|publisher = Chamber of Deputies (Italy)
|accessdate = 2009-01-23
}}
*{{cite web
|url= http://www.dsl.unimi.it/dslwtemp/ita/modello_scheda_persone.php?id=14
|title= Pietro Ichino 
|publisher =  Dipartimento di Studi del lavoro e del welfare dell'[[Università degli Studi di Milano]]
|accessdate= 2009-01-23
}}
*{{cite web
|url= http://www.radioradicale.it/soggetti/pietro-ichino
|title= Eventi a cui ha partecipato Pietro Ichino
|publisher = [[RadioRadicale.it]]
|accessdate= 2009-01-23
}}
* {{Openpolis|id=333150}}
*{{cite web
|url= http://www.ichinobrugnatelli.it/html-IT/pietro_ichino.html
|title= Pietro Ichino
|publisher = Studio Legale Ichino-Brugnatelli e Associati
|accessdate= 2009-01-23
}}
*{{cite web
|url= http://www.lavoce.info/la-redazione/pietro-ichino/
|title= Pietro Ichino
|publisher = [[lavoce.info]]
|accessdate= 2009-01-23
}}

{{Authority control}}
{{DEFAULTSORT:Ichino, Pietro}}
[[Category:Italian politicians]]
[[Category:Civic Choice politicians]]
[[Category:21st-century Italian politicians]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:University of Cagliari faculty]]
[[Category:University of Milan faculty]]