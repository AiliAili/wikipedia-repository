{{italic title}}
'''''Coningsby, or The New Generation''''', is an English [[political fiction|political novel]] by [[Benjamin Disraeli]] published in 1844. It is rumored to be based on [[Nathan Mayer Rothschild]]. According to his biographer, Robert Blake, the character of Sidonia is a cross between Lionel de Rothschild and Disraeli himself.<ref>Robert Blake, ''Disraeli'' (London, 1966), p. 202.</ref>

==Background==

The book is set against a background of the real political events of the 1830s in England that followed the enactment of the [[Reform Act 1832|Reform Bill]] of 1832.  In describing these events Disraeli sets out his own beliefs including his opposition to [[Robert Peel]], his dislikes of both the [[British Whig Party]] and the ideals of [[Utilitarianism]], and the need for [[social justice]] in a newly industrialized society.  He portrays the self-serving politician in the character of Rigby (based on [[John Wilson Croker]]) and the malicious party insiders in the characters of Taper and Tadpole.

==Plot==
The novel follows the life and career of Henry Coningsby, the orphan grandson of a wealthy [[marquess]], Lord Monmouth. Lord Monmouth initially disapproved of Coningsby's parents' marriage, but on their death he relents and sends the boy to be educated at [[Eton College]].  At Eton Coningsby meets and befriends Oswald Millbank, the son of a rich [[cotton]] manufacturer who is a bitter enemy of Lord Monmouth.  The two older men represent old and new wealth in society.

As Coningsby grows up he begins to develop his own [[Liberalism|liberal]] political views, and falls in love with Oswald's sister Edith.  When Lord Monmouth discovers these developments he is furious and secretly disinherits his grandson.  On his death, Coningsby is left penniless, and is forced to work for his living.  He decides to study law and become a barrister.  This proof of his character impresses Edith's father (who had previously also been hostile) and he consents to their marriage at last.  By the end of the novel Coningsby is elected to [[Parliament of the United Kingdom|Parliament]] for his new father-in-law's constituency and his fortune is restored.

The character of Coningsby is based on [[George Smythe, 7th Viscount Strangford|George Smythe]].
The themes, and some of the characters, reappear in Disraeli's later novels ''[[Sybil (novel)|Sybil]]'', and ''[[Tancred (novel)|Tancred]]''.
<!--Irrelevant comment: please rework text as required

 You've overlooked the essence of this novel starting with, "Can anything be more absurd. . ." to the punch line, "So you see, my dear Coningsby, that the world is governed by very different personages from what is imagined by those who are not behind the scenes. -->

==Characters==
*Philip Augustus, Marquess of Monmouth (Lord Cardiff)
*Henry Coningsby, Esq.
*Sir Charles Buckhurst
*Lord Eskdale
*Duke of Beaumanoir
*Lord Henry Sydney
*Lord Vere
*Lord Fitz-Booby
*Sir Joseph Wallinger
*Lady Wallinger
*Oswald Millbank
*Edith Millbank
*Paul Prince Colonna
*Madame Colonna
*Lucretia Princess Colonna
*[[Theodore Hook|Lucian Gay]]
*Mr. Nicolas Rigby
*Mr. Taper
*Mr. Tadpole
*Mr. Ormsby
*Armand Villebecque
*Marie Estelle Matteau (Stella)
*Flora Villebecque (La Petite)
*Sidonia

==See also==
{{portal|Novels}}
* [[Young England]]
* [[Politics in fiction]]

==Notes==
{{reflist}}

==References==
* ''Oxford Companion to English Literature'', 4th ed., 1967

==External links==
{{wikiquote}}
* {{gutenberg|no=7412|name=Coningsby}}
* {{librivox book | title=Coningsby | author=Benjamin DISRAELI}}

{{Benjamin Disraeli}}

[[Category:1844 novels]]
[[Category:Novels by Benjamin Disraeli]]
[[Category:Political novels]]
[[Category:Novels set in the 1830s]]
[[Category:19th-century British novels]]