{{Infobox journal
| title = European Journal of International Relations
| cover = [[File:European Journal of International Relations.jpg]]
| discipline = [[International relations]]
| abbreviation = Eur. J. Int. Rel.
| editor = [[Beate Jahn]], [[Peter Newell (academic)|Peter Newell]], [[Patricia Owens (academic)|Patricia Owens]]
| publisher = [[Sage Publications]] on behalf of the Standing Group of International Relations of the [[European Consortium for Political Research]]
| country =
| frequency = Quarterly
| history = 1995-present
| impact = 1.972
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal200942/title
| link1 = http://ejt.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ejt.sagepub.com/content
| link2-name = Online archive
| ISSN = 1354-0661
| eISSN = 1460-3713
| OCLC = 474767004
| LCCN = 
}}
The '''''European Journal of International Relations''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] covering [[international relations]]. It is published by [[Sage Publications]] on behalf of the European Standing Group on International Relations of the [[European Consortium for Political Research]]. The [[editors-in-chief]] are [[Beate Jahn]], [[Peter Newell (academic)|Peter Newell]], and [[Patricia Owens (academic)|Patricia Owens]] ([[University of Sussex]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.972, ranking it 7th out of 85 journals in the category "International Relations".<ref name=WoS>{{cite book |year=2015 |chapter=European Journal of International Relations |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.sagepub.com/journals/Journal200942/title}}

[[Category:International relations journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1995]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]