
{{For|the related organization in the United States|Society of Chemical Industry (American Section)}}

{{Infobox Organization
| name         = Society of Chemical Industry
| image        = 
| image_border = 
| size         = 200 px
| caption      = 
| formation    = {{Start date|1881}}
| type         = [[Learned society]]
| headquarters = [[London]]
| location     = United Kingdom
| membership   = 
| language     = English
| leader_title = Hon. President
| leader_name  = Neil Carson<ref name="Carson">{{cite web|title=SCI Honorary President Neil Carson Awarded OBE|url=http://www.soci.org/news/sci/neil-carson-obe|website=Society of Chemical Industry|accessdate=14 Jun 2016}}</ref>
| key_people   =
| num_staff    = 
| budget       = 
| website      = {{URL|www.soci.org}}
}}
The '''Society of Chemical Industry''' ('''SCI''') is a [[learned society]] set up in 1881 "to further the application of [[chemistry]] and related sciences for the public benefit".<ref name="About">{{cite web|title=About SCI|url=http://www.soci.org/About-Us/About-SCI|website=Society of Chemical Industry|accessdate=16 August 2016}}</ref> Its purpose is "Promoting the commercial application of science for the benefit of society" and provides an international forum where science meets business on independent, impartial ground. Since being founded in 1881, the society has expanded and diversified to cover a range of interest areas, such as food and agriculture, pharmaceuticals, biotechnology, environmental science and safety.

==Offices==
The headquarters is in [[Belgrave Square]], [[London]].  In addition there are semi-independent branches in the [[Society of Chemical Industry (American Section)|United States]], [[Chemical Institute of Canada|Canada]], and Australia.

==Aims==
The Society aims to promote links between scientists and industrialists, and does so through technical and business interest groups and international and regional groups, and by running some 50 conferences, seminars and lectures a year.

SCI also aims to inform government decision-making relating to science and industry. A paper urging further action on science education to protect future of UK economic health produced by SCI members in response to the closure of the Chemistry department at the [[University of Sussex]] gained newspaper coverage in the UK.<ref name=Guardian>{{cite web|url=http://education.guardian.co.uk/universitiesincrisis/story/0,,1730055,00.html|title=Concern over chemistry course closure|work=[[Guardian.co.uk|Guardian Unlimited]]|date=2006-03-13|first=Donald|last=MacLeod}}</ref>

==History==
On 21 November 1879, Lancashire chemist John Hargreaves canvassed a meeting of chemists and managers in Widnes, St Helens and Runcorn to consider the formation of a chemical society. Modelled on the successful Tyne Chemical Society already operating in [[Newcastle upon Tyne|Newcastle]], the newly proposed South Lancashire Chemical Society held its first meeting on 29 January 1880 in Liverpool, with the eminent industrial chemist and soda manufacturer [[Ludwig Mond]] presiding.

It was quickly decided that the society should not be limited to just the local region and the title 'the Society of Chemical Industry’ was finally settled upon at a meeting in London on 4 April 1881, as being 'more inclusive'. Held at the offices of the Chemical Society, now the headquarters of the [[Royal Society of Chemistry]], in [[Burlington House]], this meeting was presided over by [[Henry Enfield Roscoe|Henry Roscoe]], appointed first president of SCI,<ref name="Bowden">{{cite book|last1=Bowden|first1=Mary Ellen|last2=Smith|first2=John Kenly|title=American chemical enterprise : a perspective on 100 years of innovation to commemorate the centennial of the Society of Chemical Industry (American Section)|date=1994|publisher=Chemical Heritage Foundation|location=Philadelphia|isbn=9780941901130|url=https://books.google.com/books?id=cSyxIRfC0oYC&pg=PA8}}</ref> and attended by Eustace Carey, [[Ludwig Mond]], FA Abel, [[Lowthian Bell]], [[William Henry Perkin|William H Perkin]], [[Walter Weldon]], [[Edward Rider Cook]], Thomas Tyrer and [[George E. Davis|George E Davis]]; all prominent scientists, industrialists and MPs of the time.

===Membership===
The original membership fee was very steep for the time: The first subscription fee was set at one [[Guinea (British coin)|guinea]], which would be equivalent to nearly £400 today. Four grades of membership were agreed at the time: member, associate, student and honorary, with most appointments made on the basis of a review of their 'eligibility' by the SCI council. Despite the high fee, by the time of the first official meeting of the Society of Chemical Industry in June 1881, it had attracted over 300 members.

===Incorporation===
An Extraordinary General Meeting was held on 27 March 1906, under the direction of president [[Edward Divers]] and secretary C. G. Cresswell, to discuss a motion to apply for incorporation  under a [[royal charter]]. The resolution was formally proposed by Sir (Thomas) Boverton Redwood. After some discussion, the motion was unanimously supported.<ref name="Meeting">{{cite journal|title=Proceedings of the extraordinary general meeting, March 27, 1906|journal=Journal of the Society of Chemical Industry|date=April 30, 1906|volume=25|pages=343-347|url=https://books.google.com/books?id=7xMAAAAAMAAJ&pg=PA345&lpg=PA345|accessdate=17 August 2016}}</ref>
The Society was formally incorporated, by Royal Charter, as of 17 June 1907, and its bylaws were published in the ''Journal of the Society of Chemical Industry''. By that time, it had expanded to include a number of satellite chapters, including Canada, New South Wales, New York and New England as well as locations within Great Britain.<ref name="Charter">{{cite journal|title=Official Notice|journal=Journal of the Society of Chemical Industry|date=July 31, 1908|volume=27|issue=14|pages=721-731|url=https://books.google.com/books?id=OvgwAQAAMAAJ&pg=PA723&lpg=PA723|accessdate=17 August 2016}}</ref>

===Headquarters===
The first headquarters of the newly fledged Society of Chemical Industry was established in 1881 at Palace Chambers, Bridge Street, [[Westminster]], London. After a series of changes of address, the Society finally moved to its fifth and present location at 14/15 – and initially 16 – [[Belgrave Square]] in 1955. Owned by the [[Duke of Westminster]], along with the rest of [[Belgravia]], the building was and still is part of the [[Grosvenor Group|Grosvenor Estate]] and had recently been commandeered by the Ministry of Defence during World War II. Interestingly, the former Nazi commander [[Rudolf Hess]] is believed to have been interrogated in the building after he flew to Britain late in the war.

==Events==
SCI organises over 50 conferences and events per year which are focused around stimulating and informative scientific and special interest subjects. These are primarily organised through SCI member-led technical and regional interest groups.{{Citation needed|date=July 2010}}

SCI runs free Public Evening Lectures,<ref name="Lectures">{{cite web|title=Public Evening Lectures|url=http://www.soci.org/News/SCI-Public-Lectures|website=Society of Chemical Industry|accessdate=16 August 2016}}</ref> as well as several awards programmes designed to raise awareness of the benefits of the practical application of chemistry and related sciences across scientific disciplines and industrial sectors.  The SCI also confers scholarships and travel bursaries to student members, and celebrates accomplished scientists, educators and business people through a number of international awards, medals, and lectureships.

==Technical Interest Groups==
SCI has a number of Technical Interest and Business Interest Groups, which aim to provide opportunities to exchange ideas and gain new perspectives on markets, technologies, strategies and people. These groups over a wide range of topics and regions, with some being more active than others. SCI's Technical Interest Groups comprise:

* BioResources 
* Biotechnology
* The British Carbon Group
* Colloid and Surface Chemistry
* Construction Materials
* Electrochemical Technology
* Environment 
* Fine Chemicals
* Fire and Materials Chemistry
* Food
* Health, Safety and Environment
* Horticulture
* Lipids
* Macro Group UK 
* Materials Chemistry
* Process Engineering
* Science and Enterprise
* Separation Science and Technology
* Young Chemists' Panel

==International Groups==

International Groups comprise:

* [[Society of Chemical Industry (American Section)]] 
* Australia
* [[Chemical Institute of Canada|Canada]]

==Regional Interest Groups==

Regional Interest Groups in the UK comprise:

* All Ireland
* Bristol and South West
* Cambridge and Great Eastern
* Chinese UK
* Liverpool and North West
* London
* Scotland
* Thames and Kennet
* Yorkshire and the Humber
* East Midlands

==Journals==
The society publishes a number of [[peer review|peer-reviewed]] [[scientific journals]] in conjunction with [[John Wiley & Sons]]:

* ''[[Biofuels, Bioproducts and Biorefining]]''
* ''[[Energy Science & Engineering]]''
* ''[[Greenhouse Gases: Science and Technology]]''
* ''[[Journal of Chemical Technology and Biotechnology]]''
* ''[[Journal of the Science of Food and Agriculture]]''
* ''[[Pest Management Science]]''
* ''[[Polymer International]]''

==Chemistry & Industry==
SCI also publishes the monthly [[magazine]] ''Chemistry & Industry''.<ref name="Magazine">{{cite web|title=Chemistry & Industry|url=http://www.soci.org/Chemistry-and-Industry|website=Society of Chemical Industry|accessdate=16 August 2016}}</ref>

==Awards==
The Society gives a number of awards, including the [[Levinstein Memorial Award]] to persons who have made significant contributions in the field of chemical technology.<ref>{{cite web|url=http://www.soci.org/News/Awards-Levinstein-Gordon|title=Levinstein Memorial Award, 2009|publisher=Society of Chemical Industry|accessdate=16 December 2009}}</ref><ref name="Awards">{{cite web|title=Awards List|url=http://www.soci.org/awards/awards-list|website=Society of Chemical Industry|accessdate=16 August 2016}}</ref>

==References==
<references />

==External links==
* {{Official website|http://www.soci.org}}

{{Chemistry societies}}

[[Category:1881 establishments in the United Kingdom]]
[[Category:Chemical engineering organizations]]
[[Category:Chemical industry in the United Kingdom]]
[[Category:Learned societies of the United Kingdom]]
[[Category:Organisations based in the City of Westminster]]
[[Category:Scientific organizations established in 1881]]
[[Category:Scientific organisations based in the United Kingdom]]