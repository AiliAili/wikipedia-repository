{{distinguish|SaudiGulf Airlines}}
{{For|the American airline that operated with a similar name|Gulf Air Transport}}
{{More footnotes|date=July 2012}}
{{Use dmy dates|date=September 2011}}
{{Infobox Airline
|airline = Gulf Air
|image = Gulf Air Logo.svg
|image_size = 200
|IATA = GF
|ICAO = GFA
|callsign = GULF AIR
|founded = 1950 (as [[Gulf Aviation]])
|commenced =
|ceased =
|hubs = [[Bahrain International Airport]]
|secondary_hubs =
|focus_cities =
|frequent_flyer = Falcon Flyer
|lounge = Falcon Gold Lounge
|fleet_size = 28
|destinations = 41
|parent = Government of Bahrain
|headquarters = [[Muharraq]], Bahrain
|key_people =
<div>
* {{nowrap|Maher Salman Jabor Al Musallam, [[CEO]]}}<ref>http://www.arabianbusiness.com/bahrain-s-gulf-air-appoints-new-acting-ceo-482051.html</ref>
* {{nowrap|[[Khalid bin Abdullah Al Khalifa]], Chairman}}<ref name=Gulfair>{{cite web|title=Board of Directors|url=http://www.gulfair.com/English/aboutgulfair/Pages/BoardofDirectors.aspx|accessdate=5 August 2012}}</ref>|website = [http://www.gulfair.com/ gulfair.com]
}}

'''Gulf Air''' ({{lang-ar|طيران الخليج}} ''Ṭayarān al-Khalīj'') is the principal [[flag carrier]] of [[Bahrain]]. Headquartered in [[Muharraq]],<ref>Summers, Mark. "[http://www.gulf-daily-news.com/NewsDetails.aspx?storyid=188654 'It's business as usual' at Gulf Air]." ''[[Gulf Daily News]]''. Wednesday 25 July 2007. Retrieved on 24 September 2009.</ref> adjacent to [[Bahrain International Airport]],<ref>{{cite web| title=Airline Membership| work=IATA| url=http://www.iata.org/membership/Pages/airline_members_list.aspx?All=true}} "Gulf Air Company G.S.C. opposite Bahrain International Airport, Muharraq Manama Bahrain"</ref> the airline operates scheduled services to 41 destinations in 23 countries across Africa, Asia and Europe. Its main base is Bahrain International Airport.<ref name="FI">[[Flight International]] 3 April 2007</ref>

Unlike its larger rivals, [[Emirates (airline)|Emirates Airline]], [[Etihad Airways]] and [[Qatar Airways]], which focus on transit passenger traffic through their respective hubs, Gulf Air has focused on point-to-point passenger traffic.<ref name=":0">{{Cite news|url=http://gulfnews.com/business/aviation/gulf-air-reports-bhd62-7m-in-losses-in-2014-1.1584557|title=Gulf Air reports Bhd62.7m in losses in 2014|last=report|first=Staff|date=2015-09-15|newspaper=GulfNews|access-date=2016-11-29}}</ref> The airline, which has suffered financial losses in recent years, has an ongoing restructuring programme to limit operating losses and return it to profitability.<ref name=":1">{{Cite web|url=https://www.gulfair.com/media-centre/press-releases/gulf-air-reduces-losses-by-88%25-in-the-three-years-since-it-embarked-upon-its-restructuring-strategy|title=gulf air reduces losses by 88% in the three years since it embarked upon its restructuring strategy {{!}} Gulf Air|website=www.gulfair.com|access-date=2016-11-29}}</ref>

==History==

===1949–1973: Gulf Aviation as operating company===
{{Main|Gulf Aviation}}
In the late 1940s, Freddie Bosworth, a British pilot and entrepreneur, began an air taxi service to [[Doha]] and [[Dhahran]] from Bahrain. Bosworth later expanded service and, on 24 March 1950, registered Gulf Aviation Company Limited as a private shareholding company.<ref name=GFhist>{{cite web|title=Gulf Air History|url=http://www.gulfair.com/English/aboutgulfair/Pages/History.aspx|website=Gulf Air|publisher=Gulf Air|accessdate=11 June 2015}}</ref>  This makes its current operating company, Gulf Air, one of the oldest carriers in the Middle East.<ref name="anna.aero">{{cite news| url=http://www.anna.aero/2008/10/03/gulf-air-adds-new-routes-to-china-and-india/| title=Gulf Air adds new routes to China and India; increasing capacity to Europe this winter| date=3 October 2008| publisher=anna.aero}}</ref> The early fleet contained seven [[Avro Anson]]s and three [[De Havilland Express|de Havilland DH.86B]] four-engine [[biplane]]s.

In October 1951, [[British Overseas Airways Corporation]] (BOAC) became a major shareholder in Gulf Aviation, holding a 22% stake through the BOAC subsidiary company BOAC Associated Companies.<ref name=GFhist />

===1970s: Full national ownership===
[[File:VC-10 Gulf Air Heathrow - 1977.jpg|thumb|A Gulf Air [[Vickers VC-10]] landing at [[London Heathrow Airport]] in 1977]]
In 1973 the governments of the Emirate (now Kingdom) of Bahrain, the State of [[Qatar]], the Emirate of [[Abu Dhabi]] and the Sultanate of [[Oman]] agreed to purchase the BOAC Associated Companies holding in Gulf Aviation.<ref name=GFhist /> The Foundation Treaty was signed on 1 January 1974 and gave each government a 25% shareholding in Gulf Aviation, which became a holding company. The operating company was now branded as Gulf Air and became the flag carrier for the four states.<ref name=GFhist />

With leased [[Lockheed L-1011|Lockheed L-1011 Tristar]] and [[Boeing 737]] aircraft joining the fleet, by 1976 Gulf Air had expanded its route network to include [[Amman]], [[Amsterdam]], [[Athens]], [[Baghdad]], [[Mumbai|Bombay]], [[Bangkok]], [[Beirut]], [[Cairo]], [[Colombo]], [[Delhi]], [[Dhaka]], [[Hong Kong]], [[Jeddah]], [[Karachi]], [[Khartoum]], [[Larnaca]], [[Manila]], [[Paris]], [[Ras al-Khaimah]] and [[Sana'a]]. The fleet comprised four [[Vickers VC10]], three [[BAC One-Eleven]]s, two [[Lockheed L-1011 Tristar|Lockheed L-1011 Tristar 200s]] and five [[Boeing 737|Boeing 737–200s]]. In 1978, the airline doubled the Tristar fleet to replace the VC10s. Meanwhile, the airline increased the Boeing 737 fleet to nine and phased out the One-Elevens.<ref name=GFhist />

===1980s–1992: Expansion===
[[File:Gulf Air Boeing 747-200 Gilliand.jpg|thumb|A Gulf Air [[Boeing 747-100]] at [[Charles de Gaulle Airport]] in 1986]]
The 1980s saw an increase in air travel and growth for Gulf Air. In 1981, Gulf Air became an [[IATA]] member, and in the following year became the first international airline to land at [[Riyadh]]. In 1985, ''[[Emirates (airline)|Emirates]]'', the startup national carrier of [[Dubai]], United Arab Emirates, began operating, and would later become a major rival of Gulf Air. In 1988, [[Boeing 767]]s joined the fleet, and the airline launched service to [[Frankfurt]], [[Istanbul]], [[Damascus]], [[Dar es Salaam]], [[Fujairah]] and [[Nairobi]], and resumed service to [[Shiraz]] and [[Baghdad]].<ref name=GFhist />

Gulf Air celebrated its 40th anniversary in 1990. The light-blue and peach [[Balenciaga]]-designed uniform was introduced. Services to [[Singapore]], Sydney and [[Thiruvananthapuram]] were launched, Gulf Air thereby becoming the first Arab airline to fly to Australia. Gulf Air added services to [[Johannesburg]] and Melbourne in 1992, becoming the first Arab airline to fly directly to these cities. In 1993, it opened a flight-simulator centre in Qatar, and introduced service to [[Casablanca]], [[Entebbe]], [[Jakarta]], [[Kilimanjaro]], [[Madras]], Rome, San'a', [[Zanzibar]] and [[Zürich]].<ref name=GFhist />

===1993–2005: New livery and destinations===
In May 1994, Gulf Air received its first [[Airbus A340|Airbus A340-300]]. A no-smoking policy was established in 1998 on flights to [[Singapore]] and Australia, which was later extended through its whole network. In 1999, Gulf Air launched three new routes in northern [[Pakistan]]: [[Islamabad]], [[Lahore]] and [[Peshawar]]. It also took delivery of two out of six [[Airbus A330|Airbus A330-200]] aircraft, and introduced a new Balmain uniform.<ref name=GFhist /> The Gulf Air website opened in January 1997.<ref>{{webarchive |url=https://web.archive.org/web/20000229122653/http://www.gulfairco.com:80/frame12.htm |date=29 February 2000 |title="A Message from Gulf Air's President & Chief Executive." }} Gulf Air. 9 December 2000. Retrieved on 29 May 2011.</ref>

In 2000, the airline celebrated its 50th anniversary. It took delivery of the remaining Airbus A330-200 aircraft in June, and launched service to [[Milan]].<ref name=GFhist />

In May 2002, James Hogan became President and CEO of Gulf Air and instigated a restructuring and turnaround programme in response to a drastic fall in profits and increasing debt{{citation needed|date=June 2015}}. The Gulf Air board unanimously approved the three-year plan at an extraordinary general meeting held on 18 December.{{Citation needed|date=May 2011}} By 1 August 2002 the State of [[Qatar]] announced intentions to withdraw from Gulf Air. The state remained a member state for a six-month period after announcing the intention to withdraw.<ref>{{webarchive |url=https://web.archive.org/web/20030226042321/http://www.gulfairco.com/press/NewsDetails.asp?hID=221 |date=26 February 2003 |title="GULF AIR STATEMENT – OWNER STATES." }} Gulf Air. 1 August 2002. Retrieved on 29 May 2011.</ref>

In 2003, Gulf Air introduced a new [[Landor Associates]]-designed livery and, in June, established [[Gulf Traveller]], a subsidiary, all-economy, full-service airline. It also announced a sponsorship deal for the [[Bahrain Grand Prix]] through 2010, creating the Gulfair Bahrain Grand Prix, of which the first was staged in 2004. The airline also introduced daily flights to [[Athens]] and Sydney via [[Singapore]] on 23 November 2003.<ref name=GFhist />

In 2004, Gulf Air introduced direct flights between [[Dubai]] and London, [[Muscat, Oman|Muscat]] and London, and a daily service between [[Abu Dhabi]] and [[Ras Al Khaimah]]. The airline carried a record 7.5&nbsp;million passengers during that year.<ref name=GFhist /> Gulf Air's sponsorship of the Bahrain Formula 1 Grand Prix continued, with a record race crowd and a global TV audience. The airline announced a return to profit, with the best financial performance since 1997. Despite a BD30&nbsp;million (US$80&nbsp;million) cost to the business through fuel price rises during the year, Gulf Air recorded a profit of BD1.5&nbsp;million (US$4.0&nbsp;million) in the calendar year to December 2004, on revenues up 23.8% to BD476.3&nbsp;million (US$1.26&nbsp;billion) (2003: BD 384.6&nbsp;million / USD1,020.2&nbsp;million). The results meant the airline out-performed the targets set under Project Falcon, the three-year restructuring plan approved by the Board in December 2002.<ref name=GFhist />

The owner states of Gulf Air at that time—the Kingdom of Bahrain, the Emirate of Abu Dhabi, and the [[Sultanate of Oman]]—confirmed their support for further expansion of the airline through a new three-year strategic plan which would include re-equipment of the aircraft fleet and recapitalization of the business through private-sector financing. Gulf Air was also placed on the IOSA registry following its successful completion of the [[International Air Transport Association|IATA]] [[IATA Operational Safety Audit|Operational Safety Audit (IOSA)]].

===2006–2008: Bahrain takes over===
[[File:Airbus A340-312, Gulf Air AN1787280.jpg|thumb|A now retired Gulf Air [[Airbus A340|Airbus A340-300]] in 2007]]
The new summer schedule commencing 28 April 2006 saw the complete withdrawal from Abu Dhabi as a hub, following the decision on 13 September 2005 by the [[Abu Dhabi (emirate)|Emirate of Abu Dhabi]] to withdraw from Gulf Air and establish its own airline, [[Etihad Airways]].<ref name="anna.aero"/> Gulf Air changed its operations to a dual-hub basis between Bahrain and Muscat airports. The airline ran a series of advertisements in local newspapers, thanking Abu Dhabi for its contribution to Gulf Air. As the national carrier for the United Arab Emirates for over 35 years, it has a large customer base located in Abu Dhabi. Gulf Air endeavoured to show continuing support for flights to Abu Dhabi from Bahrain and Muscat, connecting to the rest of the Gulf Air network, via advertisements placed in local newspapers.

James Hogan resigned as President and Chief Executive Officer as of 1 October 2006 (subsequently becoming CEO at rival airline Etihad). Ahmed Al Hammadi was named acting chief executive officer, until Swiss national André Dosé, the former chief executive officer of [[Crossair]] and [[Swiss International Air Lines]], became CEO on 1 April 2007. A few days later, Dosé announced a BD310&nbsp;million (USD825&nbsp;million) restructuring plan. This included originating or terminating all flights in Bahrain; ceasing routes to [[Johannesburg]], Dublin, [[Jakarta]], Singapore, Hong Kong and Sydney; eliminating all Boeing 767s and Airbus A340-300s from the fleet; introducing the [[Airbus A321]] in July 2007 and the [[Airbus A330|Airbus A330-300]] in 2009; and potentially terminating employment based on performance, and without regard for nationality. This led to some employees applying for jobs in other airlines and, in less than a month, Gulf Air lost 500 persons from its workforce, prompting the airline to rule out mass layoffs as part of its recovery plan, except for performance reasons.{{Citation needed|date=June 2015}}

On 5 May 2007, the government of Bahrain claimed full ownership of the airline, as joint-owner Oman withdrew from the airline.<ref>{{Cite news|url=http://gulfnews.com/business/aviation/bahrain-now-sole-owner-of-gulf-air-1.177359|title=Bahrain now sole owner  of Gulf Air|last=Chief|first=Habib Toumi, Bureau|date=2007-05-06|newspaper=GulfNews|access-date=2016-11-29}}</ref> André Dosé resigned on 23 July 2007 and was replaced by Bjorn Naf. On 6 November 2007, Gulf Air started its third daily non-stop flight to [[London Heathrow Airport]] from Bahrain. On the same day, Gulf Air became fully owned by Bahrain.{{Citation needed|date=June 2015}}

The airline inaugurated services to [[Shanghai Pudong International Airport]] on 16 June 2008 (the route was terminated on 25 December 2009). It also placed orders with Boeing (for 24 787s) and Airbus (for 15 A320s and 20 A330s) to upgrade its fleet.{{Citation needed|date=June 2015}} The airline's last commercial [[Boeing 767]] flight was on 29 May 2008. On 3 July 2008, Gulf Air was announced as the official sponsor of London [[association football]] club, [[Queens Park Rangers F.C.|Queens Park Rangers]]. The same year, Gulf Air signed a lease agreement for five aircraft with International Lease Finance Corporation (ILFC) as part of its growth and expansion strategy. The lease was for six years for two [[Airbus A320 family|Airbus A319s]] and three [[Airbus A330|Airbus A330-200s]], due for delivery in March, April and May 2009.{{Citation needed|date=June 2015}}

===Developments since 2009===
[[File:Gulf Air Airbus A320-200 Bahrain Air Show livery KvW.jpg|thumb|A Gulf Air [[Airbus A320-200]] in ''Bahrain Air Show'' livery]]
In March 2009, Gulf Air signed a 42-month lease agreement with [[Jet Airways]] for four [[Boeing 777|Boeing 777-300ER]]s, but the aircraft were returned to Jet Airways starting in September 2009. In May, Gulf Air inaugurated summer seasonal flights to [[Alexandria International Airport (Egypt)|Alexandria]], [[Aleppo International Airport|Aleppo]] and [[Salalah Airport|Salalah]]. On 1 September 2009, Gulf Air resumed flights to [[Baghdad International Airport|Baghdad]].<ref>{{Cite news| title=Gulf Air adds three Iraqi cities | publisher=AMEInfo.com| date=6 August 2009| url=http://www.ameinfo.com/205799.html| accessdate=15 July 2010}}</ref> Services to [[Najaf]] and [[Arbil|Erbil]] began shortly afterward.

Starting June 2009, Gulf Air's Golden Falcon logo was seen on the streets of London, emblazoned on the side of the city's taxi cabs, as part a two-year marketing deal. Fifty Hackney Carriages were to be rolled out in full Gulf Air livery to promote the airline's flights from London Heathrow to Bahrain and beyond.<ref>{{Cite news| last=Sambidge| first=Andy| title=Gulf Air signs two-year London taxis marketing deal| publisher=Arabian Business| date=2 June 2009| url=http://www.arabianbusiness.com/557464-gulf-air-signs-two-year-london-taxis-marketing-deal| accessdate=15 July 2010}}</ref> Later in June, the carrier announced the departure of CEO Bjorn Naf and the appointment of [[Samer Majali]] (who worked previously for [[Royal Jordanian]]) as CEO effective 1 August 2009.

On 1 March 2010, Gulf Air launched its new "Falcon Gold" cabin, a single premium cabin that is aimed at offering higher standards of comfort for the standard premium price. As of August 2011, the new Flat Beds were installed on all aircraft except short-haul aircraft.

On 5 September 2011, Gulf Air appointed [[Dr. Jassim Haji]] as Director of Information Technology,<ref>Gulf Daily News, [http://www.gulf-daily-news.com/NewsDetails.aspx?storyid=312974 "Haji named Gulf Air's IT director"], 5 September 2011</ref> reporting directly to the CEO of the airline.

In 2011, Gulf Air temporarily suspended flights to [[Iran]], [[Iraq]] and [[Lebanon]] during the height of the [[Bahraini uprising (2011–present)|Bahraini uprising]]. The airline originally was to resume service to Iran from November 2012, but cancelled the plan as it was unable to receive approval from the Iranian authorities.<ref>{{Cite news|url=http://www.reuters.com/article/uk-airlines-bahrain-idUSLNE89G01120121017|title=Bahrain's Gulf Air says Iran holds up flight resumption|date=2016-10-17|newspaper=Reuters|access-date=2016-11-29}}</ref> Flights to Iran resumed in March 2014.<ref>{{Cite news|url=http://www.reuters.com/article/us-bahrain-gulfair-iran-idUSBREA190UM20140210|title=Bahrain's Gulf Air to resume flights to Tehran next month|date=2017-02-10|newspaper=Reuters|access-date=2016-11-29}}</ref>

In November 2012, Gulf Air phased out its last Airbus A340-300, after 18 years of service. At the end of November 2012, it was announced that Gulf Air CEO Samer Majali's resignation had been accepted by the Board of Directors. Majali left by the end of 2012, after serving the company for three years.<ref>http://atwonline.com/international-aviation-regulation/news/gulf-air-ceo-samer-majali-resigns-1129</ref> Maher Salman Al Musallam was the acting CEO of Gulf Air until May 2016, when he was officially appointed to the role.

At the Bahrain Airshow in January 2016, Gulf Air ordered 17 [[Airbus A321neo|A321neo]] and 12 [[Airbus A320neo family|A320neo]] aircraft for delivery from June 2018, and cancelled a commitment to acquire six A330-300 aircraft.<ref>{{Cite news|url=https://www.flightglobal.com/news/articles/gulf-air-ups-a320neo-order-but-cancels-new-a330s-421068/|title=Gulf Air ups A320neo order but cancels new A330s|date=2016-01-21|newspaper=Flightglobal.com|access-date=2016-11-29}}</ref> In addition, the airline also announced an restructured order for 16 [[Boeing 787-9]] aircraft. The new order of 16 Boeing 787-9 aircraft replaced an existing order for 16 of the smaller [[Boeing 787 Dreamliner|Boeing 787-8]] aircraft.<ref>{{Cite news|url=http://www.thenational.ae/business/aviation/bahrains-gulf-air-orders-19-airbus-a320-planes|title=Bahrain’s Gulf Air orders 19 Airbus A320 planes {{!}} The National|access-date=2016-11-29}}</ref>

==Corporate affairs==

===Subsidiaries===
'''Gulf Traveller''' was the all-economy full service subsidiary airline of Gulf Air. Its main base was [[Abu Dhabi International Airport]].<ref name="FI"/> It was briefly relocated between Bahrain and [[Muscat, Oman|Muscat]] airports after Abu Dhabi pulled out of the Gulf Air consortium in 2005, and in May 2007 [[Oman]] also pulled out of the group leaving Bahrain as sole owner of Gulf Air. Gulf Traveller has since been disbanded due to these changes.

===Sponsorship===
Gulf Air sponsors events, of which the most prestigious is the [[Bahrain Grand Prix]]. This is usually the first or fourth race of the [[Formula One]] season, and is held in March or April. Gulf Air was also the first ever shirt sponsor of [[Chelsea F.C.]] in 1983 and 1984.<ref>{{cite web|title=Chelsea|url=http://www.historicalkits.co.uk/Chelsea/Chelsea.htm|work=Historical Football Kits|first=Dave|last=Moor|accessdate=21 May 2012}}</ref> More recently, it was shirt sponsor of [[Queens Park Rangers F.C.]] from 2008 to 2011.<ref>{{cite web|title=Queen's Park Rangers|url=http://www.historicalkits.co.uk/Queens_Park_Rangers/Queens_Park_Rangers.htm|work=Historical Football Kits|first=Dave|last=Moor|accessdate=21 May 2012}}</ref> It also sponsors the [[Bahrain International Airshow]]

=== Financial Performance ===
In 2011, due largely to political unrest in the state of Bahrain, Gulf Air lost BHD95 million (USD250 million).<ref name=":2">{{Cite web|url=http://centreforaviation.com/analysis/bahrain-to-continue-to-back-gulf-air-but-carrier-may-emerge-radically-changed-85108|title=Bahrain to continue to back Gulf Air, but carrier may emerge radically changed {{!}} CAPA - Centre for Aviation|website=centreforaviation.com|access-date=2016-11-29}}</ref>

In 2012, the airline's loss grew to BHD196 million (USD520 million).<ref name=":1" /> In response, a decision was taken in 2013 to implement a turnaround plan that involved reducing the airline's fleet, number of staff and number of destinations.<ref name=":2" />

In 2013, the airline recorded a reduced BHD93.3 million (USD247.6 million) loss.<ref name=":0" />

In 2014, Gulf Air incurred a BHD62.7 million (USD166.4 million) loss, a 32.8% improvement from the previous year.<ref name=":0" />

In 2015, Gulf Air sustained a BHD24.1 million (USD63.9 million) loss, a 62% reduction from the previous year, and an overall 88% reduction from 2012.<ref name=":1" />

==Destinations==
{{Main|Gulf Air destinations}}
As of January 2017, Gulf Air flies to 41 international destinations in 23 countries across Africa, Asia and Europe from its hub at [[Bahrain International Airport]].<ref>{{cite web| title=Gulf Air Destinations Map| publisher=Gulf Air| url=http://www.gulfair.com/English/PlanandBook/Pages/InteractiveMap.aspx}}</ref> Gulf Air's own Falcon Gold lounge could be found at the airports of Bahrain, Dubai and London–Heathrow.<ref>{{cite web|title=Lounges - Gulf Air|url=http://www.gulfair.com/ENGLISH/ONTHEGROUND/Pages/Lounges.aspx|publisher=Gulf Air|accessdate=14 July 2012}}</ref>

===Codeshare agreements===
Gulf Air has [[codeshare agreement]]s with the following airlines:<ref name="CAPA Gulf Air profile">{{cite web|url=http://centreforaviation.com/profiles/airlines/gulf-air-gf |title=Profile on Gulf Air |website=CAPA|publisher=Centre for Aviation|access-date=2016-10-31|archive-url=https://web.archive.org/web/20161031215424/http://centreforaviation.com/profiles/airlines/gulf-air-gf |archive-date=2016-10-31|dead-url=no}}</ref>

* [[American Airlines]]
* [[EgyptAir]] 
* [[Philippine Airlines]]
* [[Royal Jordanian]]
* [[Thai Airways]]

==Fleet==

===Current fleet===
[[File:Airbus A320-212, Gulf Air AN1284588.jpg|thumb|Gulf Air [[Airbus A320-200]]]]
[[File:Airbus A330-243 A9C-KC Gulf Air (9094926353).jpg|thumb|Gulf Air [[Airbus A330-200]]]]
The Gulf Air fleet consists of the following aircraft as of November 2016:<ref>[http://www.ch-aviation.com/portal/airline/GF#al_profile_tab_fleet ch-aviation.com - Gulf Air]</ref>

<center>
{|class="wikitable" border="1" cellpadding="3" style="border-collapse:collapse;text-align:center"
|+ '''Gulf Air fleet'''
|-
! rowspan="2" | Aircraft
! rowspan="2" | In service
! rowspan="2" | Orders
! colspan="3" |Passengers
! rowspan="2" | Notes
|-
!J
!Y
! Total
|-
|rowspan=2|[[Airbus A320 family|Airbus A320-200]]
|5
|rowspan=2|&mdash;
|14
|96
|110
|rowspan=2|
|-
|11
|16
|120
|136
|-
|[[Airbus A320neo family|Airbus A320neo]]
|&mdash;
|12
| colspan="3" |TBA
|EIS: 2018<ref name="reuters">[http://www.reuters.com/article/2012/11/12/uk-gulfair-orders-idUSLNE8AB02B20121112 Gulf Air cuts Boeing 787 order, revises Airbus order]</ref>
|-
|[[Airbus A320 family|Airbus A321-200]]
|6
|&mdash;
|8
|161
|169
|
|-
|[[Airbus A320neo family|Airbus A321neo]]
|&mdash;
|17
| colspan="3" |TBA
|EIS: 2020
|-
| rowspan="2" |[[Airbus A330|Airbus A330-200]]
| rowspan="2" |6
| rowspan="2" |&mdash;
|8
|255
|263
| rowspan="2" |
|-
|30
|184
|214
|-
|[[Boeing 787 Dreamliner|Boeing 787–9]]
|&mdash;
|16
| colspan="3" |TBA
|EIS: 2018<ref name="reuters" />
|-
|[[Bombardier CS100]]
|&mdash;
|10
| colspan="3" |TBA
|EIS: 2018<ref name="gulfair_cseries">{{cite web|url=http://bombardier.com/en/corporate/media-centre/press-releases/details?docID=0901260d802c3b07|title=Bombardier Discloses Gulf Air as Airline Customer for 10 CSeries Aircraft and Options for Another Six|author=Bombardier Inc.|date=4 June 2013|accessdate=4 June 2013}}</ref>
|-
| style="text-align:center;" |'''Total'''
| style="text-align:center;" |'''28'''
| style="text-align:center;" |'''55'''
| colspan="4" |
|}
</center>

===Former fleet===
[[File:Lockheed L-1011-385-1-15 TriStar 200, Gulf Air AN2072768.jpg|thumb|A now retired Gulf Air [[Lockheed L-1011 TriStar]] in 1978]]
Over the years, Gulf Air operated the following aircraft types:<ref>[http://airfleets.net/flottecie/Gulf%20Air.htm Gulf Air historic fleet list at airfleets.net. Retrieved 31 March 2011.]</ref> 
<center>
{| class="wikitable sortable" border="1" cellpadding="3" style="border-collapse:collapse"
|+ '''Gulf Air fleet development'''
|-
!Aircraft
!Introduced
!Retired
|-
|[[Airbus A320 family|Airbus A319-100]]
|2008
|2012
|-
|[[Airbus A340|Airbus A340-300]]
|1994
|2012
|-
|[[BAC One-Eleven]]
|1969
|1978
|-
|[[Boeing 707|Boeing 707–320C]]
|1979
|1980
|-
|[[Boeing 737|Boeing 737–200]]
|1977
|1995
|-
|[[Boeing 737 Next Generation|Boeing 737–700]]
|2011
|2012
|-
|[[Boeing 737 Next Generation|Boeing 737–800]]
|2007
|2008
|-
|[[Boeing 747|Boeing 747–100]]
|1984
|1987
|-
|[[Boeing 757|Boeing 757–200F]]
|1993
|1996
|-
|[[Boeing 767|Boeing 767–300ER]]
|1988
|2008
|-
|[[Boeing 777|Boeing 777–300ER]]
|2009
|2010
|-
|[[de Havilland Dove]]
|1951
|1964
|-
|[[de Havilland Heron]]
|1956
|1967
|-
|[[de Havilland Express|de Havilland DH.86B]]
|1950
|1952
|-
|[[Douglas DC-3]]
|1961
|1971
|-
|[[Embraer 170]]
|2010
|2012
|-
|[[Embraer 190]]
|2010
|2013
|-
|[[Fokker F27 Friendship]]
|1967
|1981
|-
|[[Lockheed L-1011 TriStar]]
|1976
|1998
|-
|[[Short Skyvan]]
|1970
|1981
|-
|[[Vickers VC-10]]
|1974
|1978
|-
|}
</center>

==Accidents and Incidents==
* 23 September 1983: [[Gulf Air Flight 771]] was a flight from [[Karachi]], Pakistan to Qatar via Abu Dhabi. While the [[Boeing 737|Boeing 737-200]]<ref>{{cite web| title=Accident Database| publisher=AirDisaster.Com| url=http://www.airdisaster.com/cgi-bin/view_details.cgi?date=09231983&reg=A40-BK&airline=Gulf+Air| accessdate=15 July 2010}}</ref> was on approach to [[Abu Dhabi International Airport]], a bomb exploded in the baggage compartment. The aircraft crashed in the desert near [[Mina Jebel Ali]] between Abu Dhabi and Dubai in the UAE. All seven crew members and 105 passengers died. Most of the fatalities were Pakistani nationals, many returning to jobs in the Gulf after spending the Eid ul-Adha holiday with their families in Pakistan.<ref>The ''Gulf Times'', Qatar, (24 September 1983)</ref> The bomb was apparently planted by the [[Abu Nidal Organization]], to pressure the Gulf States to pay [[protection racket|protection money]] to Nidal so as to avoid attacks on their soil.<ref>{{cite web|title=Abu Nidal - The Sooner the Better|url=http://www.ict.org.il/Articles/tabid/66/Articlsid/688/currentpage/30/Default.aspx|work=International Institute of Counter-Terrorism|accessdate=20 June 2012}}</ref>
*23 August 2000: [[Gulf Air Flight 072]] crashed into the [[Persian Gulf]] on approach to Bahrain International Airport from [[Cairo]]. The [[Airbus A320 family|Airbus A320]], with 143 passengers and crew on board, approached the landing at higher speeds than normal, and carried out an unusual low altitude orbit in an attempt to correct the approach.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=20000823-0&lang=en | title=Airbus A320 A40-EK accident record |publisher=Aviation Safety Network | quote= ..significantly higher than standard aircraft speeds during the descent and the first approach... ...performing an orbit, a non-standard manoeuvre, close to the runway at low altitude".. }}</ref><ref>{{cite web| title=ACCIDENT INVESTIGATION REPORT Gulf Air Flight GF-072 |url=http://www.bahrainairport.com/caa/gf072.html | work=Civil Aviation Authority of Bahrain |archiveurl = https://web.archive.org/web/20040212231659/http://www.bahrainairport.com/caa/gf072.html |archivedate =12 February 2004}}</ref> The orbit was unsuccessful and a go-around was attempted. While carrying out a turning climb the aircraft entered a descent at 15 degrees nose down. The aircrew did not respond to repeated [[GPWS]] warnings<ref>{{cite web|title=ACCIDENT INVESTIGATION REPORT Gulf Air Flight GF-072 |url=http://www.bahrainairport.com/caa/gf072/pdf/conclusions.pdf |quote=4b. The analysis of FDR and CVR recordings indicated that neither the captain nor the first officer perceived, or effectively responded to, the threat of the aircraft's increasing proximity to the ground in spite of repeated hard GPWS warnings... |work=Civil Aviation Authority of Bahrain |deadurl=yes <!--|archiveurl=https://web.archive.org/web/*/http://www.bahrainairport.com/caa/gf072/pdf/conclusions.pdf |archivedate=-->}}</ref> and approximately one minute after starting the go-around the aircraft disappeared from radar screens.<ref>{{cite web | url=http://aviation-safety.net/photos/displayphoto.php?id=20000823-0&vnr=1&kind=G |work=Bureau Enquetes-Accidents |publisher=Aviation Safety Network |title=Airbus A320 A4O-EK accident record – Graphic – A40-EK Flight Path dervied from Lat and Long FDR Parameters}}</ref> All 143 passengers and crew, including 36 children, were killed in the accident.<ref>{{cite news |title=Sheik Hamad bin Isa Khalifa |publisher=CBS News |url=http://www.cbsnews.com/htdocs/plane_crashes/gulfair/pe_03.html |accessdate=20 December 2010}}</ref> The accident investigation concluded that the primary cause of the crash was pilot error (including [[spatial disorientation]]), with a secondary factor being systemic organizational and oversight issues.<ref>{{cite web |url=http://aviation-safety.net/database/record.php?id=20000823-0&lang=en | quote=The investigation showed that no single factor was responsible for the accident to GF-072. The accident was the result of a fatal combination of many contributory factors, both at the individual and systemic levels. | title=Airbus A320 A4O-EK accident record | publisher=Aviation Safety Network}}</ref> Flight 072 was the highest death toll of any accident involving an Airbus A320 at that time. It was subsequently surpassed by [[TAM Airlines Flight 3054]], which crashed on 17 July 2007 with 199 fatalities.
* On 29 August 2011, Gulf Air Flight 270, using an Airbus A320-214, from Bahrain to [[Cochin International Airport|Cochin]] carrying 143 people, skidded off the runway on landing due to pilot error of loss of situational awareness during reduced visibility conditions. The weather was poor with heavy rain and strong winds. The aircraft was badly damaged with nose gear collapsed and seven passengers were injured. Some people were reported to have jumped from an emergency exit when the evacuation slide failed to deploy.<ref>{{cite web|title=Accident to Airbus A320 Aircraft A9C-AG of M/S Gulf Air at Cochin International Airport on 29th August 2011|url=http://dgca.nic.in/accident/reports/AC9-AG.pdf|website=[[Directorate General of Civil Aviation (India)|Directorate General of Civil Aviation]]|accessdate=4 June 2016}}</ref><ref>{{cite web|last1=Hradecky|first1=Simon|title=Accident: Gulf Air A320 at Kochi on Aug 29th 2011, runway excursion|url=http://avherald.com/h?article=4420ff31|website=[[The Aviation Herald]]|accessdate=4 June 2016|date=29 August 2011}}</ref>

==References==

===Citations===
{{Reflist|2}}

===Bibliography===
{{Refbegin}}
* [http://www.gulf-daily-news.com/Story.asp?Article=181744&Sn=BNEW&IssueID=30052 Gulf Daily News]
* [http://www.gulf-daily-news.com/Story.asp?Article=181746&Sn=BNEW&IssueID=30052 Gulf Daily News]
* [http://www.gulf-daily-news.com/Story.asp?Article=183778&Sn=BNEW&IssueID=30073 Gulf Daily News]
* Kaminski-Morrow, David. "[http://www.flightglobal.com/news/articles/gulf-air-must-expand-or-risk-being-smothered-chief-224625/ Gulf Air must expand or risk being smothered: chief executive]." ''[[Flight International]]''. 13 June 2008.
* Kaminski-Morrow, David. "[http://www.flightglobal.com/news/articles/gulf-air-insists-alliances-will-need-middle-eastern-partners-224545/ Gulf Air insists alliances will need Middle Eastern partners]." ''[[Flight International]]''. 10 June 2008.
* [http://www.tradearabia.com/news/newsdetails.asp?Sn=TTN&artid=123438 Trade Arabia]
* Pilling, Mark. "[http://www.flightglobal.com/blogs/airline-business/2008/08/is-gulf-air-relevant-anymore/ Is Gulf Air relevant anymore?]" ''[[Flight International]]''. 12 August 2008.
* Wigglesworth, Robin. "[http://www.ft.com/cms/s/0/d0acdbaa-6805-11dd-8d3b-0000779fd18c.html Gulf Air faces stormy skies]." ''[[Financial Times]]''. 12 August 2008.
{{Refend}}

==External links==
{{Commonscat-inline|Gulf Air}}
* [http://www.gulfair.com/ Official website]

{{Portalbar|Bahrain<!--Represents Bahrain, Qatar, and Oman-->|Abu Dhabi|Oman|Qatar|<!--Former states need to be mentioned, even if they no longer are a part of Gulf Air-->|Aviation}}
{{Airlines of Bahrain}}
{{IATA members|mideast}}
{{Arab Air Carriers Organization}}

[[Category:Airlines of Bahrain]]
[[Category:IATA members]]
[[Category:Arab Air Carriers Organization members]]
[[Category:Airlines established in 1950]]
[[Category:Government-owned airlines]]
[[Category:Gulf Air]]