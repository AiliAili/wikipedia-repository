{{notability|Biographies|date=September 2016}}
{{refimprove|date=September 2016}}
'''Ilona Kickbusch''' (born 27 August 1948 in [[Munich]], Germany) is recognized throughout the world for her contribution to [[health promotion]] and [[global health]].<ref>The Healthcare Forum Journal, vol. 37, no. 3, May/June 1994</ref> She is currently adjunct professor at the [[Graduate Institute of International and Development Studies]], Geneva.<ref>The Graduate Institute Website, Retrieved 2012-2-22: http://graduateinstitute.ch/globalhealth/page3990.html</ref>

==Career Overview==

Ilona Kickbusch graduated from the [[University of Konstanz]], Germany, with a PhD in Political Science (1981). She contributed to the first academic studies in Germany on consumer-centered health care, the self-help and the women's health movement. After joining the [[World Health Organization]] (1981–1998) she was appointed to lead the Global Health Promotion Programme, followed by senior positions at the regional and global level of the organisation. She then joined [[Yale University]] (1998–2004) to head the new Global Health Program at the [[Yale School of Public Health]]. On returning to Europe, she took up senior positions as chairperson of the World Demographic & Ageing Forum, St Gallen, (2005),<ref>World Demographic & Ageing Forum Website, Retrieved 2012-2-22: http://www.wdaforum.org/index.php/en/about-us/board-of-trustees</ref> director of the Global Health Programme<ref>The Graduate Institute Website, Retrieved 2012-2-22: http://graduateinstitute.ch/globalhealth</ref> (2008) at the Graduate Institute of International and Development Studies, Geneva, and chair of Global Health Europe<ref>Global Health Europe Website, Retrieved 2012-2-22: http://www.globalhealtheurope.org/index.php?option=com_content&view=category&layout=blog&id=59&Itemid=97</ref> (2009). In Switzerland, she serves on the executive board of the Careum Foundation <ref>Careum Foundation Website, Retrieved 2012-2-22: http://www.careum-congress.ch/stiftung_en.php</ref> (2009). She teaches regularly at several academic institutions including the [[University of St. Gallen]] (HSG), Switzerland.

A complete summary of career highlights can be found [http://www.ilonakickbusch.com/kickbusch-wAssets/docs/Kickbusch_CV.pdf here]

==Policy and research innovations==

Ilona Kickbusch advises organisations, government agencies and the private sector on policies and strategies to promote health at the national, European and international level. She has published widely and is a member of several advisory boards in both the academic and health policy arenas. She has received many awards while contributing to innovations in health throughout her career. She is a true public health leader with a deep commitment to global responsibility, the empowerment of people and [[Health in All Policies]]<ref>''Implementing Health in All Policies: Adelaide 2010'', Department of Health, Government of South Australia, Edited by Prof. Ilona Kickbusch and Dr. Kevin Buckett: 2010.</ref> (HiAP). 

===Health Promotion - Health Society===

She has had a distinguished career with the World Health Organization, at both the regional and global level. She was responsible for the [[Ottawa Charter for Health Promotion]] in 1986{{cn|date=September 2016}}, a seminal document in public health, as well as for the subsequent global conferences in health promotion. She was also involved in the formulation of the [[Bangkok Charter]] for Health Promotion in a Globalized World in 2005{{cn|date=September 2016}}, and served on the advisory group for the World Conference on Social Determinants of Health 2011. She was the founding editor of the journal Health Promotion International (Oxford University Press) and serves as the chair of the editorial board. She has always been active both in developing implementation strategies (for example, the drafting of the Swiss health promotion and disease prevention law), as well as in strengthening the theoretical basis of [[health promotion]]. Concerning the latter, she has contributed to the debate by developing the concept of the 'health society' and exploring the links between health and modernity and innovative approaches to governance for health at the national and global level. She was recently lead author of white papers on 'The challenge of addiction' and 'The food system: a prism of present and future challenges for health promotion and sustainable development', and is engaged in a policy glossary on 'Learning for well-being'.

===Health in All Policies - Governance for health - Policy innovations for health===

In 2007, she was appointed Adelaide [[Thinker in Residence]]<ref>Adelaide Thinkers in Residence Website, Retrieved 2012-2-22: http://www.thinkers.sa.gov.au/thinkers/kickbusch/who.aspx</ref> for the subject area 'Healthy societies' at the invitation of the South Australian Premier. During this residency, she developed a special 'health lens' approach to Health in All Policies, which has been implemented in South Australia for several years now. She has published and advised widely on HiAP approaches and is considered one of the global leaders in this field. She continues to be involved in a range of hands-on projects regarding Health in All Policies. She has conducted a study for the WHO Regional Office for Europe on 'Governance for health in the 21st century', which is one of the key studies informing the new European policy for health, Health 2020. She is involved in the planning for the 2013 Global Conference on Health Promotion, which will focus on Health in All Policies, and in a range of advisory bodies for the study of the social determinants of health.

===The settings approach - Demography and gender===

She developed the settings approach to health promotion and initiated programmes such as Healthy Cities, health-promoting schools, healthy workplaces, health-promoting hospitals and health in prisons. Many of these networks are now global and have proved to be a highly sustainable approach to public health action. She also initiated the Health Behavior in School-aged Children study, which has become a global gold standard for measuring child and youth health. At the WHO headquarters she initiated the Healthy Ageing programme and continues to be active in this field as chair of the multidisciplinary World Demographic & Ageing Forum, St Gallen. She has always been active in the field of women's health and empowerment, and initiated the first comparative WHO study of women's health in Europe, Women's Health Counts.

===Health Literacy===

She has been a leader in developing the concept of [[health literacy]] and seeking to strengthen it through research and programmes. As a result of her initiative, the EU European Health Literacy Survey was created. It presented its results in 2011. She has been an advocate for citizen participation in health and has been instrumental in creating the new European Network On Patient Empowerment (ENOPE). The South Australian and Swiss health literacy alliances were created and based on her proposals. She is currently engaged in writing a textbook on health literacy in German, as well as a flagship publication for WHO.

===Global Health Governance===

During her time at [[Yale University]] she was head of the global health division, one of the first global health programmes and a school of public health. She contributed significantly to shaping the field of [[global health]], particularly with the analysis of global health governance, and headed a major [[Fulbright Program]] entitled Health in a Borderless World. Today, she is considered one of the leaders in thinking on global health governance and has made significant suggestions with regard to multi-stakeholder involvement at WHO and partnership-based approaches to global health governance, including the proposal for a Committee C at the [[World Health Assembly]]. She provided significant input into EU policy on global health, as well as into the highly innovative Swiss health foreign policy. She also initiated Global Health Europe, a think tank to strengthen Europe's voice in global health.

===Global Health Diplomacy===

She has contributed significantly to the new field of global health diplomacy and has developed a unique approach to executive education in this field, which has gained significant support from the [[Rockefeller Foundation]]. Flagship courses are held regularly in Geneva, and have also been conducted in cooperation with local partners in China, Indonesia, Kenya, the USA and Canada. She has published widely on this subject – including a textbook on global health diplomacy – as well as on the wider issues of global health and foreign policy. She is a member of a number of policy networks in this field including the Global Health Diplomacy Network (GHD-NET). She is the editor (together with Tom Novotny) of the Global Health Diplomacy book series, published by [[World Scientific]].

==Awards and honors==

*CYWHS Oration, Adelaide, Australia
*Honorary doctorate, Nordic School of Public Health, Gothenburg, Sweden
*Leavell Lecture, awarded by the World Federation of Public Health Associations
*Aventis Pasteur International Award, awarded by the Canadian Public Health Association to recognise contributions to international health
*Andrija Stampar Award and medal for lifelong distinguished service to public health, awarded by the Association of Schools of Public Health in the European Region (ASPHER)
*Salomon-Neumann Medal for major contributions to social medicine, awarded by the German Society for Social Medicine
*Special Meritorious Gold Medal, awarded by the Province of Vienna, Austria, for special contributions to the health of the citizens of Vienna
*Candidate of the German government for the position of Regional Director, WHO Europe
*Honorary Fellow, Faculty of Public Health Medicine, UK
*Queen Elizabeth the Queen Mother Lecture, Faculty of Public Health Medicine, UK
*VicHealth Award, awarded by the Victorian Health Promotion Foundation, Melbourne, Australia, for special contributions to health promotion
*Awarded a fellowship by the Friedrich Ebert Stiftung for PhD studies

==Publications==

===Books===

*Rosskam, Ellen and Kickbusch, Ilona (eds.). Negotiating and Navigating Global Health: Case Studies in Global Health Diplomacy. New Jersey: 2011.
*Kickbusch, Ilona and Buckett, Kevin (eds.). Implementing Health in All Policies. Adelaide: 2010.
*Kickbusch, Ilona. Policy Innovation for Health. Springer, 2009.
*Bührlein, B and Kickbusch, I. (eds.). Innovationssystem Gesundheit: Ziele und Nutzen von Gesundheitsinnovationen. Karlsruhe: Fraunhofer Gesellschaft, 2008.
*McQueen, David, Kickbusch, Ilona, Potvin, Louise, Pelikan, Jürgen, Balbo, Laura and Abel, Thomas. On Health and Modernity: Theoretical foundations of health promotion. Springer, 2007.
*Kickbusch, I. Die Gesundheitsgesellschaft. Gamburg: Verlag Gesundheitsförderung, 2006.
*Geene, Raimund, Kickbusch, Ilona and Halkow, Anja (eds.). Prävention und Gesundheitsförderung – eine soziale und politische Aufgabe. Berlin: Gesundheit Berlin, 2005.
*Kickbusch, Ilona, Hartwig, Kari and List, Justin (eds.). Globalization, Women, and Health in the 21st Century. New York: Palgrave Macmillan, 2005.
*Kickbusch, Ilona and Badura, Bernhard (eds.). Health Promotion Research: Towards a New Social Epidemiology. European Series No. 37. Copenhagen: WHO Regional Office for Europe, 1991.
*Kickbusch, Ilona. Good Planets are Hard to Find. WHO Healthy Cities Paper No. 5. Copenhagen: 1989.
*Kickbusch, Ilona, Anderson, Robert, Davies, John K., McQueen, David V. and Turner, Jill (eds.). Health Behaviour Research and Health Promotion. Oxford: Oxford University Press, 1988.
*Kickbusch, Ilona. Die Familialisierung der weiblichen Arbeit: Zur strukturellen Ähnlichkeit zwischen bezahlter und unbezahlter Frauenarbeit. Konstanz: 1987.
*Kickbusch, Ilona and Riedmüller, Barbara (eds.). Die arme Frauen, Frauen im Wohlfahrtsstaat. Sammelband mit internationalen Beiträgen. Introduction (with Riedmüller, B.): 'Theoretische Perspektiven einer Sozialpolitikanalyse' and 'Familie als Beruf – Beruf als Familie: Der segregierte Arbeitsmarkt und die Familialisierung der weiblichen Arbeit' (on female paid and unpaid labour). Frankfurt: Suhrkamp, 1984, pp.&nbsp;7–13 and pp.&nbsp;163–178.
*Kickbusch, Ilona and Hatch, Stephen (eds.). Self-help and Health in Europe: New Approaches in Health Care. Introduction: 'A Reorientation of Health Care', and Conclusion: 'Making a Place for Self-help' (both with Stephen Hatch). Copenhagen: WHO Regional Office for Europe, 1983.
*Kickbusch, Ilona and Trojan, Alf (eds.). Gemeinsam sind wir stärker, Selbsthilfegruppen und Gesundheitssicherung (on self-help groups). Frankfurt: Fischer Alternativ, 1981.

===Selected chapters and articles===

*Global health diplomacy: how foreign policy can influence health' in British Medical Journal, Volume 342: 2011.
*'Global Health Diplomacy and Peace' (with Paulo Buss) in Infectious Disease Clinics of North America, 25(3): 2011, pp.&nbsp;601–610.
*Kickbusch, Ilona, Hein, Wolfgang and Silberschmidt, Gaudenz. 'Addressing global health governance challenges through a new mechanism: the proposal for a Committee C of the World Health Assembly' in The Journal of Law, Medicine & Ethics, Fall: 2010, JLME 38.3.
*'Moving Global Health Governance forward'. Chapter 15 in Buse et al., Making Sense of Global Health Governance: A Policy Perspective. Palgrave MacMillan, 2009.
*'Global Health Diplomacy: the new recognition of health in foreign policy' (with Christian Erk). Chapter 10 in Clapham, Andrew, Robinson, Mary and Hangartner, Salome (eds.), Realizing the right to health. 2009.
*European Perspectives on Global Health: A Policy Glossary (with Graham Lister and David Gleicher). Brussels: European Foundation Centre. Reworked and updated in 2009. Available at www.globalhealtheurope.org
*'In search of the public health paradigm for the 21st century: the political dimensions of public health' in Portuguese Journal of Public Health. 25th Anniversary Supplement Issue: Current public health challenges. Lisbon: 2009, pp.&nbsp;11–19.
*'Health Promotion' (Mittelmark, Kickbusch, Rootman, Scriven and Tones) in Heggenhougen, H.K. and Quah, S.R. (eds.), International Encyclopedia of Public Health, Vol. 3. Oxford: Academic Press, 2008, pp.&nbsp;225–240.
*'Health Governance: The Health Society' in Kickbusch, McQueen et al. (eds.), Health and Modernity: Theoretical Foundations of Health Promotion. Springer, 2007, pp.&nbsp;144–161.
*'Health promotion – Not a tree but a rhizome' in O'Neill, M. et al. (eds.), Health promotion in Canada: Critical perspectives. (Second edition). Toronto: 2007.
*'Global health diplomacy: training across disciplines' (with Thomas E. Novotny, Nico Drager, Gaudenz Silberschmidt and Santiago Alcazar) in Bulletin of the World Health Organization, 85(12): 2007, pp.&nbsp;971–973.
*'Global health diplomacy: the need for new perspectives, strategic approaches and skills in global health' (with Gaudenz Silberschmidt and Paulo Buss) in Bulletin of the World Health Organization, 85(3): 2007, pp.&nbsp;230–232.
*'Health and Wellbeing' in Marinker, Marshall (ed.), Constructive Conversations about Health. Radcliffe, 2006, pp.&nbsp;31–40.
*'Health Literacy: Towards active health citizenship' (with Daniela Maag) in Sprenger, M. (ed.), Public health in Österreich und Europa. Festschrift Horst Noack. Graz: 2006, pp.&nbsp;151–158.
*Navigating Health: The Role of Health Literacy (with Suzanne Wait and Daniela Maag). London: Alliance for Health and the Future, 2006.
*'Perspectives in health promotion and population health' in American Journal of Public Health, March, 93(3): 2003, pp.&nbsp;383–388.
*'Global Health Governance: some new theoretical considerations on the new political space'. Chapter in Lee, K. (ed.), Globalization and Health. London: Palgrave, 2003, pp.&nbsp;192–203.
*'Influence and opportunity: Observations on the US role in global public health' in Health Affairs, 21(6): 2002, pp.&nbsp;131–141.
*'Global influences and global responses: international health at the turn of the 21st century' (with Kent Buse). Chapter in Merson, M.H., Black, R.E. and Mills, A.J. (eds.), Handbook of International Public Health. Aspen Publishers, 2001, pp.&nbsp;701–737.
*'The development of international health policies: accountability intact?' in Social Science & Medicine, 51: 2000, pp.&nbsp;979–989. (Reprinted in Kirton, John (ed.), Global Health, Library of Essays in Global Governance series. Ashgate, 2009.)
*'Partnerships for health in the 21st century' (with Jonathan Quick) in World Statistics Quarterly, 51(1): 1998, pp.&nbsp;68–74.
*'Research for Health: Challenge for the Nineties' (with Kathryn Dean) in Araki, Shunichi (ed.), Behavioral Medicine: An integrated biobehavioral approach to health and illness. Amsterdam: Elsevier, 1992, pp.&nbsp;299–308.
*'Self-care in Health Promotion' in Social Science & Medicine, 29: 1989, pp.&nbsp;125–130.

==External links==
*http://www.kickbusch-health-consult.com
*http://www.careum.ch/team3
*http://graduateinstitute.ch/globalhealth/page3990.html
*http://www.gesundheitsgesellschaft.de/die-gesundheitsgesellschaft/presse/rosenbrock.shtml

==Notes==
{{reflist}}

{{DEFAULTSORT:Kickbusch, Ilona}}
[[Category:Graduate Institute of International and Development Studies faculty]]
[[Category:1948 births]]
[[Category:Living people]]
[[Category:German women scientists]]
[[Category:People in public health]]
[[Category:Political scientists]]
[[Category:Women political scientists]]