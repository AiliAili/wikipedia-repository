{{good article}}
{{Infobox road
|state=CA
|type=SR
|route=209
|length_mi=8
|length_round=0
|length_ref=<ref name=traffic/>
|map_notes=Former SR&nbsp;209 highlighted in red
|established= 1964
|deleted= 2003
|direction_a=South
|terminus_a=[[Cabrillo National Monument]] in [[Point Loma, California|Point Loma]]
|direction_b=North
|terminus_b={{jct|state=CA|I|5|I|8}} in [[San Diego, California|San Diego]]
|previous_route=207
|previous_type=CA
|next_route=210
|next_type=I
|counties=[[San Diego County, California|San Diego]]
}}
'''State Route 209''' (SR&nbsp;209) was a [[state highway]] in the [[U.S. state]] of [[California]], connecting [[Cabrillo National Monument]] with the [[interchange (road)|interchange]] of [[I-5 (CA)|Interstate 5]] (I-5) and [[I-8 (CA)|I-8]] in [[San Diego]], passing through the neighborhoods of [[Point Loma, San Diego|Point Loma]]. The majority of the route was along Rosecrans Street; it also included Cañon Street and Catalina Boulevard leading to the tip of Point Loma.

The Rosecrans Street portion of SR&nbsp;209 corresponded to the original routing of the historic [[La Playa Trail]]. Rosecrans Street was paved in the late 1900s through the community of [[Roseville-Fleetridge, San Diego|Roseville]], and was added to the state highway system in 1933 as Route 12. SR&nbsp;209 was designated in the [[1964 state highway renumbering (California)|1964 state highway renumbering]], and a full interchange with I-5 and I-8 was completed in 1969. The designation was removed from the state highway system in 2003 and responsibility for the road was transferred to the city of San Diego.

==Route description==
[[File:Pointloma2.jpg|right|thumb|Old Point Loma Lighthouse]]
The route began at a turnaround next to the [[Old Point Loma Lighthouse]] in the [[Cabrillo National Monument]], near the southern tip of Point Loma. Heading north along the crest of the Point Loma peninsula, it passed through [[Naval Base Point Loma|Fort Rosecrans Military Reservation]] and [[Fort Rosecrans National Cemetery]] before exiting the former through a gate and entering the [[Wooded Area, San Diego|Wooded Area]] neighborhood of Point Loma as Catalina Boulevard. SR&nbsp;209 passed through this residential neighborhood and provided access to [[Point Loma Nazarene University]]. The SR&nbsp;209 designation then made a right turn onto Cañon Street, curved to the southeast, and went downhill to the bayside location of the old La Playa Trail. At the intersection with Rosecrans Street, the designation made a turn to the northeast at a right angle onto Rosecrans.<ref name="tgsd">{{cite map|title=San Diego County Road Atlas|publisher=Thomas Brothers|year=1998|pages=1268, 1287–1288, 1308}}</ref>

As it continued northeast through the Roseville and [[Loma Portal, San Diego|Loma Portal]] neighborhoods, SR&nbsp;209 intersected both Harbor Boulevard and Nimitz Boulevard before passing along the northwestern edge of the [[Naval Training Center San Diego]] (now closed and redeveloped as [[Liberty Station]])<ref name="tgsd" /> and providing access to the [[Marine Corps Recruit Depot San Diego]] at Barnett Street. SR&nbsp;209 continued from Midway Drive northeast to the intersection of Sports Arena Boulevard and Camino del Rio West, where the highway continued north onto the latter, terminating at the ramps leading into the interchange with [[I-5 (CA)|I-5]] and [[I-8 (CA)|I-8]].<ref name="tgsd" />

In 1996, SR&nbsp;209 had an [[annual average daily traffic]] (AADT) of 2,600&nbsp;vehicles at the southern end in Cabrillo National Monument, and 61,000&nbsp;vehicles at the northern end at the I-5/I-8 junction, the latter of which was the highest AADT for the highway.<ref name=traffic>{{cite web|title=All Traffic Volumes on CSHS |author=Staff |url=http://traffic-counts.dot.ca.gov/1996all/r198220i.htm |publisher=California Department of Transportation |year=2000 |accessdate=August 23, 2013 |archiveurl=http://www.webcitation.org/6JAMpipW1?url=http://traffic-counts.dot.ca.gov/1996all/r198220i.htm |archivedate=August 26, 2013 |deadurl=yes |df= }}</ref>

==History==
[[File:US Boundary Survey 1850.png|thumb|right|United States Boundary Survey of the San Diego area, 1850, showing the [[La Playa Trail]] from La Playa to Old San Diego and the Mission]]
The Rosecrans Street portion of the highway followed the route of the historic [[La Playa Trail]], the oldest European trail on the west coast,<ref name="Hall">{{cite news|url=http://www.utsandiego.com/news/2010/nov/24/marking-time-on-the-la-playa-trail/|title=Marking time on the La Playa Trail; Monument to days gone by has moved|last=Hall|first=Matthew T.|date=November 24, 2010|work=San Diego Union Tribune|accessdate=October 14, 2012}}</ref> which connected the Spanish settlements in [[Old Town San Diego State Historic Park|Old Town]] and [[Mission San Diego de Alcala]] with the ship loading and unloading area at [[La Playa, San Diego|La Playa]].<ref name="chpw" />

In February 1907, a petition to construct a railway line along Rosecrans Street from 38th Street to MacAuley Street was introduced to the San Diego City Council.<ref>{{cite news | title=Both Franchise Petitions Denied | work=The San Diego Union and Daily Bee | date=February 1, 1907 | author=Staff | page=9}}</ref> By July, work was to begin on grading Rosecrans Street through what was known as the city of Roseville in preparation for construction of the Point Loma Electric Railway line.<ref>{{cite news | title=Begins Grading Work For Point Loma Line | work=The San Diego Union and Daily Bee | date=July 7, 1907 | author=Staff | page=12}}</ref> The work was briefly disrupted due to a labor dispute, but by June 1908, {{convert|2|mi|km|spell=in}} of the {{convert|2.5|mi|km}} had been paved.<ref>{{cite news | title=Rosecrans Street Grading Resumed | work=The San Diego Union and Daily Bee | date=June 19, 1908 | author=Staff | page=14}}</ref> By March 1909, railroad tracks were being placed along Rosecrans Street.<ref>{{cite news | title=Tracklayers Making Speedy Process | work=The San Diego Union and Daily Bee | date=March 6, 1909 | author=Staff | page=7}}</ref> The city assumed maintenance of the road on May 1, after it had been paved from Tide Street to Ocean Beach.<ref>{{cite news | title=New Point Loma Boulevard is Turned Over to City | work=The San Diego Union and Daily Bee | date=May 2, 1909 | author=Staff | page=10}}</ref> Private citizens paid for over $100,000 of the costs, and the city was to pay for $12,000, according to an agreement made in 1904.<ref>{{cite news | title=Street Work Pay is Puzzle | work=The San Diego Union and Daily Bee | date=April 23, 1909 | author=Staff | page=8}}</ref> However, by December the bill had not been paid by the city due to a lack of funds, and the construction company filed a claim; on December 29, the city agreed to pay $12,000, using funds from the water department.<ref>{{cite news | title=City Will Settle Boulevard Claim | work=The San Diego Union and Daily Bee | date=December 30, 1909 | author=Staff | page=9}}</ref>

The termination of the Point Loma streetcar line was considered in 1923, but the [[San Diego Electric Railway]] decided to keep it running after residents opposed the idea of using buses instead.<ref>{{cite news | title=Street Car Line Will Remain in Suburban Towns | work=The San Diego Union | date=September 14, 1923 | author=Staff | page=20}}</ref> However, by 1946, bus lines were in place on Rosecrans Street,<ref>{{cite news | title=Luckel Asks Bus Change on Point | work=The San Diego Union | date=December 5, 1946 | page=A4}}</ref> and a petition for increased bus service to Point Loma was given to the city in October 1947.<ref>{{cite news | title=Luckel Submits 500 Names on Bus Petition | work=The San Diego Union | date=October 21, 1947 | author=Staff | page=A6}}</ref>

The new divided highway known as Rosecrans Boulevard from Lytton Street to Canon Street was dedicated on June 5, 1940, and was a part of the state highway system extending to Cabrillo National Monument, which served as the western end of Legislative Route 12. Before this project, it was a two-lane road that had issues such as traffic congestion and flooding.<ref name="chpw">{{cite journal | url=https://archive.org/stream/california193940highwacalirich#page/n607/mode/2up | title=Old La Playa Trail Becomes Modern 4-Lane Divided Highway | author=Scott, Byron | journal=California Highways and Public Works |date=July 1940 | volume=18 | issue=7 | page=10}}</ref> A monument marking La Playa Trail at Avenida de Portugal and Rosecrans Street that had been installed in 1934 was removed by the construction in the 1940s; however, a replacement was installed in 2010.<ref>{{cite news | title=New monument marks part of historic trail | work=San Diego Union-Tribune | date=May 6, 2010 | author=Gonzalez, Blanca | pages=EZ1, EZ3}}</ref> Another of the six monuments was moved out of the median near the Midway Drive intersection to a commercial area that same year.<ref>{{cite news | title=La Playa Trail Marker Moved from Median | work=San Diego Union-Tribune | date=November 27, 2010 | author=Hall, Matthew | page=B2}}</ref> During the 1950s, the route from Lytton Street and Rosecrans Street to Pacific Highway was considered a part of [[US 80 (CA)|US 80]].<ref>{{cite news | title=Discussion Due On Routes For Two Major Freeways | work=The San Diego Union | date=August 19, 1962 | author=Staff | page=A16}}</ref>

SR&nbsp;209 was designated in the [[1964 state highway renumbering (California)|1964 state highway renumbering]].<ref>{{cite CAstat|ch=385|year=1963}}</ref> Plans for an interchange between I-5, I-8, SR 209, and SR 109 date from 1965, although several concerns had to be taken into account, including the preservation of historical [[Old Town (San Diego)|Old Town]] and keeping traffic through the area moving during construction. The goal was to begin construction in 1966, and complete the interchange in 1969.<ref>{{cite news | title=$11 Million Interchange Projected | work=The San Diego Union | date=March 16, 1965 | author=Staff | page=A15}}</ref> There were concerns about a $3 billion shortfall in funding during May 1966, which caused the San Diego Chamber of Commerce Highway Committee to recommend the completion of SR 109 as a project.<ref>{{cite news | title=Fund Loss Feared For State Highways Under Transit Plan | work=The San Diego Union | date=May 11, 1966 | author=Staff | page=A22}}</ref>

Construction had begun on September 22, 1966, and the interchange was to replace the intersection of Pacific Highway and Rosecrans Street. The cost of the interchange was projected to be $10.86 million.<ref>{{cite news | title='Stack' Like Plate Of Spaghetti | work=The San Diego Union | date=March 26, 1967 |last= Brown |first= Joe | page=B1}}</ref> The eight-lane freeway was projected to relieve traffic in the Frontier Street area coming from the [[San Diego Sports Arena]]. Completion of both the interchange and SR 109 was planned for early 1969.<ref name="feb">{{cite news | title=Interstate 5 and 8 Ramps to Close | work=The San Diego Union | date=February 27, 1968 | author=Staff | page=C1}}</ref> The ramp from south I-5 to Camino del Rio opened in February 1968,<ref name="feb" /> with the remainder of the project to be completed in summer 1969.<ref>{{cite news | title=Interstate 5-8 Revisions Due On Monday | work=The San Diego Union | date=August 3, 1968 | author=Staff | page=B1}}</ref> The entire project was completed in September, with the road scheduled to open in October;<ref>{{cite news | title=Dedication Of Ocean Beach Freeway Slated | work=The San Diego Union | date=September 26, 1969 |last= Scarr |first= Lew | page=B1}}</ref> just a few months before, the routing of SR 209 had been determined.<ref>{{cite news | title=New Routes Set To Aid Point Loma Traffic Flow | work=The San Diego Union | date=July 13, 1966 | author=James, Paul | page=A1}}</ref> The routing of SR 109 was officially added to I-8 in 1972.<ref>{{cite CAstat|year=1972 |ch= 1216 |p=}}</ref>

In 1971, there was a state proposal to remove SR 209 from the state highway system, which the City of San Diego objected to.<ref>{{cite news | title=State Asked To Name New Routes | work=The San Diego Union | date=September 2, 1971 | author=Staff | page=B3}}</ref> However, SR 209 was [[deleted state highways in California|deleted]] from the system in 2003,<ref>{{cite CAstat|ch=525|year=2003}}</ref> and had been given to the City of San Diego in 2001.<ref>{{cite web | url=http://docs.sandiego.gov/councilminutes/2009/min20090414rm.pdf | title=Minutes for Regular Council Meeting of April 14, 2009 | publisher=San Diego City Council | date=April 14, 2009 | accessdate=July 10, 2014 | author=Staff | format=PDF}}</ref>

==Major intersections==
{{CAinttop
|county=San Diego
|location=San Diego
|post_ref=<br><ref name=traffic />
|former=yes
}}
{{CAint
|postmile=0.21
|road=[[Cabrillo National Monument]]
|notes=
}}
{{CAint
|postmile=4.68
|road=Rosecrans Street, Canon Street
|notes=
}}
{{CAint
|postmile=5.05
|road=Harbor Drive – [[San Diego International Airport]], [[Harbor Island, San Diego|Harbor Island]]
|notes=
}}
{{CAint
|postmile=5.27
|road=Nimitz Boulevard
|notes=
}}
{{CAint
|postmile=7.29
|road=Rosecrans Street, Pacific Highway, Sports Arena Boulevard – [[Mission Bay Park]]
|notes=
}}
{{CAint
|postmile=R7.76
|road={{jct|state=CA|I|5|dir1=north|name1=[[Interstate 5 in California#Southern California|San Diego Freeway]]|city1=Los Angeles}}
|notes=Interchange; northbound exit and southbound entrance
|type=incomplete
}}
{{CAint
|postmile=R7.76
|road={{jct|state=CA|I|8|dir1=east|city1=El Centro}}
|notes=Interchange; northbound exit and southbound entrance
|type=incomplete
}}
{{Jctbtm|keys=incomplete}}

==See also==
*{{portal-inline|California Roads}}

==References==
{{reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
{{Commons category}}
* [http://www.aaroads.com/california/ca-209.html California @ AARoads.com - State Route 209]
* [http://cahighways.org/209-216.html#209 CAhighways.org - State Route 209]
* [http://www.floodgap.com/roadgap/209/ Floodgap Roadgap – Old CA 209 and the Cabrillo National Monument]

[[Category:Former state highways in California|209]]
[[Category:Roads in San Diego County, California|State Route 209]]
[[Category:Point Loma, San Diego]]
[[Category:Transportation in San Diego]]