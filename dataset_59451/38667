{{EngvarB|date=July 2014}}
{{Infobox mountain
| name = Brean Down
| photo = Brean Down.jpg
| photo_caption = Brean Down from Steep Holm
| elevation_m = 97
| elevation_ref =
| prominence_m = 91
| prominence_ref =
| location = [[Mendip Hills]], England
| range =
| coordinates = {{coord|51.3256|N|3.0290|W|region:GB_type:landmark|display=inline,title}}
| grid_ref_UK = ST284590
| topo = [[Ordnance Survey|OS]] ''Landranger'' 182
| type =
| age =
| first_ascent =
| easiest_route =
}}

'''Brean Down''' is a [[promontory]] off the coast of [[Somerset]], England, standing {{convert|318|ft|m|0}} high and extending {{convert|1.5|mi|km|0}} into the [[Bristol Channel]] at the eastern end of [[Bridgwater Bay]] between [[Weston-super-Mare]] and [[Burnham-on-Sea]].

Made of [[Carboniferous Limestone]], it is a continuation of the [[Mendip Hills]]. Two further continuations are the small islands of [[Steep Holm]] and [[Flat Holm]]. The cliffs on the northern and southern flanks of Brean Down have large quantities of fossils laid down in the marine deposits about 320–350 million years ago. The site has been occupied by humans since the late [[Bronze Age]] and includes the remains of a [[Romano-Celtic Temple]]. At the seaward end is [[Brean Down Fort]] which was built in 1865 and then re-armed in the [[Second World War]].

Brean Down is now owned by the [[National Trust for Places of Historic Interest or Natural Beauty|National Trust]], and is rich in [[wildlife]], history and [[archaeology]]. It is a [[Site of Special Scientific Interest]] due to both the geology and presence of nationally rare plants including the [[Helianthemum apenninum|white rock-rose]]. It has also been scheduled as an [[ancient monument]].<ref>{{cite web|title=Multi-period site on Brean Down|url=http://list.english-heritage.org.uk/resultsingle.aspx?uid=1008211|website=National Heritage List for England|publisher=English Heritage|accessdate=7 June 2014}}</ref>

==Geology==
[[File:Cliffs on the south side of Brean Down (geograph 2570989).jpg|thumb|The cliffs on the south side showing fractures in the limestone.]]
The [[Mendip Hills]], of which Brean Down forms the most westerly part, are the most southerly [[Carboniferous Limestone]] upland in Britain. These rock strata were laid down during the [[Mississippian age|early Carboniferous]] period, about 320–350&nbsp;million years ago.{{sfn|Faulkner|1989|pp=93–106}} Subsequently, much of northwestern Europe underwent [[continental collision]] throughout the late [[Paleozoic]] era, culminating in the final phases of the [[Variscan orogeny]] near the end of the Carboniferous period, 300&nbsp;million years ago. This [[tectonics|tectonic]] activity produced a complex suite of mountain and hill ranges across what is now southern Ireland, south-western England, [[Brittany]], and elsewhere in western Europe.<ref>{{cite web|url=http://www.jncc.gov.uk/default.aspx?page=4175&block=102|title=GCR block&nbsp;– Variscan Structures of South-West England|publisher=Joint Nature Conservation Committee |accessdate=5 March 2010}}</ref> As a result of the Variscan mountain-building, the Mendip area now comprises at least four [[anticline|anticlinal]] [[fold (geology)|fold]] structures, with an east-west trend, each with a core of older [[Devonian]] sandstone and [[Silurian]] volcanic rocks.{{sfn|Kellaway|Welch|1948|pp=7, 10–11, 16 & 34–38}}{{sfn|Haslett|2010|pp=25–28}} West of the main Mendip plateau the Carboniferous Limestone continues in [[Bleadon Hill]] and Brean Down, and on the islands of [[Steep Holm]] and [[Flat Holm]].{{sfn|Coysh|Mason|Waite|1977|p=67}}

Brean Down is just over {{convert|2|km}} long and runs in an east-west direction near the mouth of the [[River Axe (Bristol Channel)|River Axe]] opposite [[Uphill Cliff]]. The flat area on the top of the peninsula may represent a raised bench laid down when the sea level was much higher than it is today, as the highest point of the promontory is approximately {{convert|97|m}} above sea level.<ref>{{cite web|last=Prudden|first=Hugh|title=Somerset Geology&nbsp;– a good rock guide|url=http://people.bath.ac.uk/exxbgs/Somerset_Good_Rock_Guide.pdf|publisher=Bath Geological Society|accessdate=6 February 2014|format=PDF}}</ref> The cliffs on the north and south faces are rich in fossils that include corals, seashells and [[crinoids]], which supports the theory of the marine deposition of the rocks.{{sfn|Haslett|2010|pp=128–132}} 13 separate layers have been identified in the 'sand cliff' on the south side, the lowest five dating to the millennia of the last glaciation.{{sfn|Atthill|1976|pp=52–53}}

==History==
[[File:Brean Down - Brean Down Fort (geograph 2782110).jpg|thumb|left|[[Brean Down Fort]]]]
Human occupation dates back to the [[Beaker culture]] of the late [[Bronze Age]].{{sfn|Goldberg|2009|pp=43–46}} There is also evidence of an [[Iron Age]] [[hill fort]] and prehistoric [[Tumulus|barrows]] and field systems.{{sfn|Adkins|1992|pp=27–29}}{{sfn|Quinn|2008|pp=21–22}} There is evidence of a [[shrine]] dating from pre-Roman times,{{sfn|ApSimon|1958|pp=106–109}}{{sfn|ApSimon|1965|pp=195–258}} which was re-established as a [[Romano-Celtic Temple]] in the mid-4th century. According to at least one source, it is extremely likely this was succeeded by a small late-4th-century Christian oratory.{{sfn|Aston|Burrow|1991}}{{sfn|Atthill|1976|pp=72–73}} Several Roman finds including gold coins of [[Augustus]], [[Nero]], and [[Nero Claudius Drusus|Drusus]], two silver [[Denarius|denarii]] of [[Vespasian]] and a Roman [[carnelian]] ring were found at the site during quarrying.<ref>{{cite web|title=Hillfort, Brean Down|url=http://www.somersetheritage.org.uk/record/10115|work=Somerset Historic Environment Record|publisher=South West Heritage Trust|accessdate=7 July 2011}}</ref>

[[Brean Down Fort]] was built on the headland between 1864 and 1871 on the recommendations of the [[Royal Commission on the Defence of the United Kingdom|1859 Royal Commission]]. It was the most southerly of a chain of defences across the [[Palmerston Forts, Bristol Channel|Bristol Channel]], protecting the access to [[Bristol]] and [[Cardiff]]. Four acres of land at the end of Brean Down were requisitioned in 1862,<ref name="sher">{{cite web |url=http://www.somersetheritage.org.uk/record/10128 |title=Victorian artillery battery, Brean Down |work=Somerset Historic Environment Record |publisher=South West Heritage Trust |accessdate=6 May 2008}}</ref> with construction beginning in 1864 and completed in 1871.

In the 1860s plans were laid for a deep-water harbour on the northern shore of Brean Down.<ref>{{cite news|last=Newton|first=Chris|title=Exploring the Secrets of Brean Down, Western extremeties of the Mendips on the River Axe Estuary|url=http://www.somerset-life.co.uk/out-about/places/exploring_the_secrets_of_brean_down_western_extremeties_of_the_mendips_on_the_river_axe_estuary_1_1632501|accessdate=2 March 2014|newspaper=Somerset Life|date=27 January 2010}}</ref> It was intended that this harbour would replace Bristol as a port on embarkation for transatlantic crossings and the export of minerals and agricultural produce from the Mendip Hills and the rest of Somerset.<ref>{{cite news|title=Brean Down|url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000264/18621006/024/0004|accessdate=7 July 2014|work=Western Daily Press|date=6 October 1862| publisher = [[British Newspaper Archive]]|subscription=yes}}</ref>
The foundation stones of the pier were laid, but the project was later abandoned after a large storm destroyed the foundations.{{sfn|Dunning|2003|p=116}} In 1897, following wireless transmissions from [[Lavernock Point]] in Wales and Flat Holm,<ref>{{cite web |url=http://www.bbc.co.uk/wales/southeast/sites/flatholm/pages/marconi.shtml |title=Marconi: Radio Pioneer |accessdate=12 April 2008 |work=BBC South East Wales }}</ref> [[Guglielmo Marconi]] moved his equipment to Brean Down and set a new distance record of {{convert|14|km}} for wireless transmission over open sea.<ref>{{cite web|title=Marconi at Brean Down (near Bristol)|url=http://g4usb.net/IMD/station-histories/brean-down-gb6md/|publisher=International Marconi Day|accessdate=6 February 2014}}</ref>{{sfn|Garratt|1994|p=86}} In 1912 Brean Down was leased by the [[Royal Society for the Protection of Birds]] as a bird sanctuary, acquiring the shooting rights to stop others shooting on the promontory.<ref>{{cite news|title=Brean Down|url=http://www.britishnewspaperarchive.co.uk/viewer/bl/0000532/19120914/077/0004|accessdate=7 July 2014|work=Gloucester Journal|date=14 September 1912| publisher = [[British Newspaper Archive]]|subscription=yes}}</ref>
[[File:Brean Down arrow. Geograph 1931583 971c5040.jpg|thumb|Concrete arrow]]
On the outbreak of [[World War II]], the fort was rearmed with two 6-inch (15-centimetre) ex-naval guns, and machine gun posts were built on the Down. [[Birnbeck Pier]] was taken over by the [[Admiralty]] in 1941 as an outpost of the [[Department of Miscellaneous Weapons Development]] (DMWD). It was commissioned as HMS Birnbeck, and was used for secret weapons development and storage with testing.{{sfn|van der Bijl|2000|pp=119–123}} The "[[Bouncing bomb]]" was tested at the Brean Down Fort on the opposite side of Weston Bay.<ref name="heritagetrail">{{cite web|title=Birnbeck Pier |work=The Heritage Trail |url=http://www.theheritagetrail.co.uk/piers/birnbeck%20pier.htm |accessdate=18 April 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20100218145015/http://www.theheritagetrail.co.uk:80/piers/birnbeck%20pier.htm |archivedate=18 February 2010 |df=dmy }}</ref><ref>{{cite web |url=http://www.somersetheritage.org.uk/record/12491 |title=Experimental weapon rails, Brean Down |work=Somerset Historic Environment Record |publisher=South West Heritage Trust |accessdate=6 May 2008}}</ref>{{sfn|Evans|2004|pp=5–11}}

In 1954 the former [[Axbridge]] Rural District Council gave {{convert|59,685|ha}} of the down to the [[National Trust for Places of Historic Interest or Natural Beauty|National Trust]] to celebrate the [[Festival of Britain]].<ref name=NTacquisitions>{{cite web|title=Acquisitions Up to December 2011|url=http://www.nationaltrust.org.uk/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadername2=MDT-Type&blobheadername3=Content-Type&blobheadervalue1=inline%3B+filename%3D297%252F965%252Fnt_acquisitions_dec2011-2%252C2.pdf&blobheadervalue2=abinary%3B+charset%3DUTF-8&blobheadervalue3=application%2Fpdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1349117284549&ssbinary=true|publisher=National Trust|accessdate=6 February 2014}}</ref> The Major Aldermen and Burgesses of the Borough of Weston-Super Mare gave {{convert|1,494|ha}} in 1963, and a further {{convert|1,371|ha}} at Brean Down Cove was acquired from M.D. and M Matthews in 2000.<ref name=NTacquisitions/> After restoring the fort, which covers {{convert|1,606|ha}}, [[Sedgemoor]] District Council gave this to the trust as well in 2002.<ref name=NTacquisitions/>

Various proposals have been put forward to construct a [[Severn Barrage]] for tidal electricity production from Brean Down to Lavernock Point in south Wales.<ref>{{cite web|url=http://www.thisisbristol.co.uk/wdp/news/drawing-board/article-249673-detail/article.html|title=Back to the drawing board|last=Brooke|first=Gerry|date=4 August 2008|work=Western Daily Press|publisher=This is Bristol|accessdate=12 December 2009}}</ref><ref name="walesonline hutchinson 2010">{{cite news|last=Hutchinson|first=Clare|title='Real cost' of Severn Barrage £34bn|url=http://www.walesonline.co.uk/news/wales-news/2010/10/19/real-cost-of-severn-barrage-34bn-91466-27498178/|accessdate=18 September 2012|newspaper=[[WalesOnline]]|date=19 October 2010}}</ref> The proposals, which go back over 100 years, have never been successful so far, however [[Peter Hain]] and others are still working on further proposals and trying to persuade the government to fund either the barrage or tidal lagoons.<ref>{{cite news|last=Evans|first=Bethan|title=Barrage bid to be looked at&nbsp;— again|url=http://www.thewestonmercury.co.uk/news/barrage_bid_to_be_looked_at_again_1_1507465|accessdate=12 July 2014|newspaper=[[The Weston & Somerset Mercury]]|date=9 September 2012}}</ref>

==Ecology==
[[Image:Helianthemumapenninum.JPG|thumb|White rock-rose (''[[Helianthemum apenninum]]'') on the south cliff of Brean Down]]
In addition to the geological interest of the site, the range of plants growing on Brean Down has led to it being designated as a [[Site of Special Scientific Interest]].<ref name=sssi>{{cite web |url=http://www.english-nature.org.uk/citation/citation_photo/1003155.pdf |title=Brean Down |accessdate=2 March 2014 |format=PDF |work=English Nature }}</ref> The nationally rare [[Helianthemum apenninum|white rock-rose]] (''Helianthemum appenninum'') is a common species at the site, occurring in abundance on the upper reaches of the grassy south-facing slopes.{{sfn|Twist|2001}} Some of the [[Orobanche|broomrape]]s growing here, which were originally thought to be oxtongue broomrape (''[[Orobanche artemisiae-campestriae]]''), are now no longer believed to be this species, but atypical specimens of ivy broomrape (''[[Orobanche hederae]]'').{{sfn|Green|Green|Crouch|1997}} Other plants on the southern slopes include the [[Koeleria vallesiana|Somerset hair grass]], [[Thymus serpyllum|wild thyme]], [[Hippocrepis comosa|horseshoe vetch]] and [[Lotus (genus)|birds-foot-trefoil]]. The northern side is dominated by [[bracken]], [[bramble]], [[privet]], [[Crataegus monogyna|hawthorn]], [[Primula veris|cowslips]] and [[Erica cinerea|bell heather]].<ref name=sssi/>

The birds seen on Brean Down include [[peregrine falcon]], [[western jackdaw|jackdaw]], [[kestrel]], [[Eurasian collared dove|collared]] and [[stock dove]]s, [[common whitethroat]], [[common linnet]], [[Saxicola|stonechat]], [[dunnock]], [[Eurasian rock pipit|rock pipit]] and&nbsp;– in 2007 – Britain's first and only [[Atlantic yellow-nosed albatross]].<ref>{{cite web|url=http://www.tristandc.com/newsof1stbritishmolly.php |title=1st Sighting of a Molly in Britain&nbsp;– A Mega Event for Twitchers|publisher=Tristan da Cunha |accessdate=6 February 2014}}</ref> There are also several species of [[butterfly]], including [[chalkhill blue]], [[dark green fritillary]], [[meadow brown]], [[Melanargia galathea|marbled white]], [[small heath (butterfly)|small heath]], and [[common blue]].<ref>{{cite web|title=Brean Down Butterflies|url=http://www.breandownfort.co.uk/butterfly/|publisher=Brean Down Fort|accessdate=2 March 2014}}</ref>

==See also==
* [[List of National Trust properties in Somerset]]

==References==
{{reflist|30em}}

==Bibliography==
*{{cite book |last=Adkins |first=Lesley and Roy |authorlink= |author2= |title=A field Guide to Somerset Archeology |year=1992 |publisher=Dovecote press |location=Stanbridge |isbn=0-946159-94-7|ref=harv}}
*{{cite journal|last=ApSimon|first=A.M.|title=The Roman temple on Brean Down, Somerset: An Interim Report on the 1957 Excavations|year=1958|journal=UBSS Proceedings|volume=8|issue=2|pages=106–109|url=http://www.ubss.org.uk/resources/proceedings/vol8/UBSS_Proc_8_2_106-109.pdf|format=PDF|ref=harv}}
*{{cite journal|last=ApSimon|first=A.M.|title=The Roman Temple on Brean Down, Somerset|year=1965|journal=UBSS Proceedings|volume=10|issue=3|pages=195–258|url=http://www.ubss.org.uk/resources/proceedings/vol10/UBSS_Proc_10_3_195-258.pdf|format=PDF|ref=harv}}
*{{cite book | last1 = Aston | first1 = Mick | last2 = Burrow|first2= Ian | title = The Archaeology of Somerset | publisher = Somerset County Council | year = 1991|isbn=978-0-86183-028-2 | location = Taunton|ref=harv}}
*{{cite book|last=Atthill|first=Robin|title=Mendip: A New Study|year=1976|publisher=David & Charles|isbn=978-0-7153-7297-5|ref=harv }}
*{{cite book |last1=Coysh |first1=A.W. |last2= Mason|first2=E.J.|last3= Waite|first3= V. |title=The Mendips |year=1977|publisher=Robert Hale Ltd |location=London |isbn=0-7091-6426-2|ref=harv }}
*{{cite book |last=Dunning |first=Robert |authorlink= |author2= |title=A History of Somerset |year=2003 |publisher=Somerset Books |location=Tiverton |isbn=0-86183-476-3|ref=harv }}
* {{cite book|last=Evans|first=Roger|title=Somerset tales of mystery and murder|publisher=Countryside Books|year=2004|location=Newbury|isbn=978-1-85306-863-8|ref=harv }}
*{{cite journal|last1=Faulkner|first1=T.J.|title=The early Carboniferous (Courceyan) Middle Hope volcanics of Weston-super-Mare: development and demise of an offshore volcanic high|journal=Proceedings of the Geologists' Association|date=1989|volume=100|issue=1|pages=93–106|doi=10.1016/S0016-7878(89)80068-9|url=http://www.sciencedirect.com/science/article/pii/S0016787889800689|ref=harv}}
*{{cite book|last=Garratt|first=G.R.M.|title=The Early History of Radio: From Faraday to Marconi|year=1994|publisher=IET|isbn=978-0-85296-845-1|url=https://books.google.com/books?id=vd90yIE2Ya8C&pg=PA86&lpg=PA86&dq=marconi+lavernock+Brean+Down|ref=harv}}
*{{cite book|last=Goldberg|first=Paul|title=Practical and Theoretical Geoarchaeology|year=2009|publisher=John Wiley & Sons|isbn=978-1-4443-1225-6|url=https://books.google.com/books?id=jyW14Ykil70C&pg=PA43&lpg=PA43&dq=Geology+Brean+Down|ref=harv}}
*{{cite book|last1=Green|first1=Paul R.|last2=Green|first2=Ian P. | last3=Crouch|first3=Geralidine R.|title=The Atlas Flora of Somerset|year=1997|publisher=P.R. Green, I.P. Green, and G.A. Crouch|isbn=978-0-9531324-0-9|ref=harv }}
*{{cite book|last=Haslett|first=Simon K.|title=Somerset Landscapes: Geology and Landforms|year=2010|publisher=Blackbarn Books|location=Usk|isbn=978-1-4564-1631-7|ref=harv}}
*{{cite book |year=1948 |last1= Kellaway |first1= G. A. |last2=Welch |first2=F. B. A. |title=Bristol and Gloucester District |series=British Regional Geology |location=London |publisher=[[HMSO]] for Natural Environment Research Council, Institute of Geological Sciences, Geographical Survey and Museum |edition=Second |isbn=0-11-880064-7|ref=harv}}
*{{cite book|last=Quinn|first=Tom|title=Hidden Britain|year=2008|publisher=New Holland Publishers|isbn=978-1-84773-129-6|url=https://books.google.com/books?id=Sbfms1oJn8MC&pg=PA21&lpg=PA21&dq=Geology+Brean+Down|ref=harv }}
*{{cite book|last1=Twist|first1=Colin|title=Rare Plants in Great Britain&nbsp;– a site guide|publisher=Colin Twist|year=2001|ref=harv }}
* {{cite book | last=van der Bijl | first=Nicholas |year=2000 | title=Brean Down Fort: Its History and the Defences of the Bristol Channel|location= Cossington|publisher=Hawk Editions | isbn=978-0-9529081-7-3|ref=harv}}

==External links==
{{Commons category|Brean Down}}
*[https://www.nationaltrust.org.uk/brean-down Brean Down information at the National Trust]
* [http://www.breandownfort.co.uk Brean Down Visitor Information]

{{SSSIs Somerset biological}}
{{SSSIs Somerset geological}}

{{Use dmy dates|date=July 2014}}

{{good article}}

[[Category:Bristol Channel]]
[[Category:Headlands of Somerset]]
[[Category:Hill forts in Somerset]]
[[Category:Hills of Somerset]]
[[Category:National Trust properties in Somerset]]
[[Category:Roman religious sites in England]]
[[Category:Scheduled Ancient Monuments in Sedgemoor]]
[[Category:Sites of Special Scientific Interest in Somerset]]
[[Category:Sites of Special Scientific Interest notified in 1971]]