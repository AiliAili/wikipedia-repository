{{redirect|King Andrew I|the political cartoon comparing Andrew Jackson to a king|King Andrew the First}}
{{good article}}
{{Infobox royalty|monarch
| name           = Andrew I 
| image          = Andrew I (Chronica Hungarorum).jpg
| caption        = Andrew I on the throne (Thuróczi's Chronicle)
| succession     = [[King of Hungary]]
| reign          = 1046&nbsp;– 1060
| coronation     = 1046, [[Székesfehérvár]]
| cor-type = hungary
| predecessor    = [[Peter, King of Hungary|Peter]]
| successor      = [[Béla I of Hungary|Béla I]]
| spouse         = [[Anastasia of Kiev]]
| issue          = [[Adelaide Arpad|Adelaide, Duchess of Bohemia]]<br/>[[Solomon, King of Hungary]]<br/>[[David of Hungary|David]]<br/>[[George, son of Andrew I of Hungary|George]] (''illegitimate'')
| house = [[Árpád dynasty]]
| house-type=Dynasty
| father         = [[Vazul]]
| mother         = a lady from the Clan Tátony
| birth_date  = c. 1015
| birth_place =
| death_date  = before 6 December {{Death year and age|1060|1015}}
| death_place = [[Zirc]], [[Kingdom of Hungary]]
| burial_place= [[Tihany Abbey]]
| religion = [[Christianity|Christian]]
|}}
'''Andrew I the White''' or '''the Catholic''' ({{lang-hu|I. Fehér or Katolikus András or Endre}}; c. 1015&nbsp;– [[Zirc]], before 6 December 1060) was  [[King of Hungary]] from 1046 to 1060. He descended from a younger branch of the [[Árpád dynasty]]. After spending fifteen years in exile, he ascended the throne during an extensive revolt of the [[pagan]] [[Hungarian people|Hungarians]]. He strengthened the position of [[Christianity]] in the [[Kingdom of Hungary]] and successfully defended its independence against the [[Holy Roman Empire]].

His efforts to ensure the succession of his son, [[Solomon of Hungary|Solomon]], resulted in the open revolt of his brother, [[Béla I of Hungary|Béla]]. Béla dethroned Andrew by force in 1060. Andrew suffered severe injuries during the fighting and died before his brother was crowned king.

==Early life==

===Childhood (c. 1015–1031)===

Medieval sources provide [[Vazul#Family|two contradictory reports]] of the parents of Andrew, and his two brothers, [[Levente]] and [[Béla I of Hungary|Béla]].{{sfn|Györffy|2000|p=378}} For instance, the ''Chronicle of Zagreb'' and ''Saint Gerard's Life''{{sfn|Györffy|2000|p=378}} write that their father was [[Vazul]], a grandson of [[Taksony of Hungary|Taksony]], [[Grand Prince of the Hungarians]] (r. ''c.'' 955–''c.'' 970).{{sfn|Kristó|Makk|1996|pp = 68, Appendices 1-2}} The ''[[Illuminated Chronicle]]'' and other medieval sources write of Vazul's relationship with "some girl" from the Tátony clan who bore his sons, who thus "were not born of a true marriage-bed".<ref>''The Hungarian Illuminated Chronicle:'' (ch. 60.87), p. 113.</ref>{{sfn|Kristó|Makk|1996|pp=77, Appendix 2}} According to a concurrent tradition, which has been preserved by most chronicles, the three princes were the sons of Vazul's brother, [[Ladislas the Bald]].{{sfn|Györffy|2000|p=378}} Modern historians, who reject the latter report, agree that Andrew and his brothers were the sons of Vazul and his concubine from the Tátony clan.{{sfn|Györffy|2000|p=378}}{{sfn|Kristó|Makk|1996|p=68}}{{sfn|Engel|2001|p=29}}{{sfn|Steinhübel|2011|p=23}} According to the historian Gyula Kristó, Andrew was the second among Vazul's three sons. He writes that Andrew was born around 1015.{{sfn|Kristó|Makk|1996|p=68}}

===In exile (1031–1046)===

According to medieval chronicles, Vazul was blinded during the reign of his cousin, King [[Stephen I of Hungary|Stephen I]], the first Christian monarch of Hungary (r. 997–1038).{{sfn|Györffy|2000|p=377}} The king ordered Vazul's mutilation after the death, in 1031, of [[Saint Emeric of Hungary|Emeric]], his only son surviving infancy.{{sfn|Györffy|2000|p=377}}{{sfn|Engel|2001|pp=28-29}} The contemporary ''[[Annales Altahenses|Annals of Altaich]]'' writes that the king himself ordered the mutilation of one of his kinsmen, who had strong claim to the throne, in an attempt to ensure a peaceful succession to his own sister's son, [[Peter Orseolo]].{{sfn|Györffy|2000|pp=377-378}}{{sfn|Engel|2001|p=29}} The same source adds that the king expelled his blinded cousin's three sons from Hungary.{{sfn|Györffy|2000|p=378}} According to the contrasting report of the Hungarian chronicles, King Stephen wanted to save the young princes' lives from their enemies in the royal court and "counselled them with all speed"<ref>''Simon of Kéza: The Deeds of the Hungarians'' (ch. 2.44), p. 107.</ref> to depart from Hungary.{{sfn|Kristó|Makk|1996|p=68}}

[[File:Stephen I at the funeral of his son (below) and blinding of Vazul (above) (Chronicon Pictum 044).jpg|thumb |right |alt=Vazul's blinding |The blinding of [[Vazul]] after the death of [[Saint Emeric of Hungary|Emeric]], the only son of King [[Stephen I of Hungary]]]]

{{Quote|''Having his own son died in his father's life, and having no other sons, Stephen, the king of good memory, who was the maternal uncle of'' [Peter Orseolo]'', adopted and appointed him as heir to his kingdom. For his kinsman's son disagreed with him on this,'' [Stephen] ''had him blinded, even if he was worthier of the kingdom, and sent his little sons into exile.''|''[[Annales Altahenses|Annals of Altaich]]''{{sfn|Kristó|1999|p=240}}}}

Exiled from Hungary, Andrew and his brothers settled in the court of Duke [[Oldřich, Duke of Bohemia|Oldřich of Bohemia]] (r. 1012–1033).{{sfn|Steinhübel|2011|p=23}} Here they came across King [[Mieszko II of Poland]] (r. 1025–1031, 1032–1034){{sfn|Steinhübel|2011|p=23}} who likewise took refuge in [[Bohemia]] after his opponents had expelled him from his kingdom.{{sfn|Manteuffel|1982|p=81}} The Polish monarch regained his crown and returned to Poland in 1032.{{sfn|Manteuffel|1982|p=82}} Andrew, Béla and Levente, whose "condition of life was poor and mean"<ref name="Chronicon_110">''The Hungarian Illuminated Chronicle:'' (ch. 53.78), p. 110.</ref> in Bohemia, followed Mieszko II who received them "kindly and honourably"<ref name="Chronicon_110"/> in Poland.{{sfn|Kristó|Makk|1996|p=68}}{{sfn|Steinhübel|2011|p=23}} After the youngest among them, Béla, married a [[Richeza of Poland, Queen of Hungary|daughter of]] Mieszko II, Andrew and Levente decided to depart from Poland, because they "felt that they would be living in Poland under their brother's shadow",<ref>''Simon of Kéza: The Deeds of the Hungarians'' (ch. 52.), p. 121.</ref> according to [[Simon of Kéza]].{{sfn|Kristó|Makk|1996|p=69}}

Hungarian chronicles have preserved a story full of fabulous or anachronistic details of the two brothers' ensuing wanderings.{{sfn|Kristó|Makk|1996|p=69}} For instance, they narrate that Andrew and Levente were captured by [[Cumans]],{{sfn|Kristó|Makk|1996|p=69}} but the latter only arrived in Europe in the 1050s.{{sfn|Curta|2006|p=306}} Having faced many hardships, Andrew and Levente established themselves in the court of [[Yaroslav the Wise]], [[Grand Prince of Kiev]] (r. 1019–1054) in the late 1030s. The grand prince gave his daughter, [[Anastasia of Kiev|Anastasia]] in marriage to Andrew.{{sfn|Kristó|Makk|1996|p=69}} Kristó writes that Andrew, who had up to that time remained pagan, was baptized on this occasion.{{sfn|Kristó|Makk|1996|p=70}}

{{Quote|''Having received permission from'' [the Polish monarch, Andrew and Levente] ''left their brother'' [Béla] ''behind and made their way to the [[Principality of Volhynia|King of Lodomeria]], who did not receive them. Since they had nowhere to lay their head, they went from there to the'' [Cumans]''. Seeing that they were persons of excellent bearing, the'' [Cumans] ''thought that they had come to spy out the land, and unless a captive Hungarian had recognized them, they should certainly have killed them; but they kept them with them for some time. Then they departed thence to Russia. ''|''[[Illuminated Chronicle|The Hungarian Illuminated Chronicle]]''<ref>''The Hungarian Illuminated Chronicle'' (ch. 55.80), p. 111.</ref>}}

===Return to Hungary (1046)===

[[File:Szent gellért 2.jpg|thumb |right |alt=St Gerard's martyrdom | [[Vata pagan uprising|Pagans slaughtering priests]] and the martyrdom of Bishop [[Gerard of Csanád]] depicted in the ''[[Anjou Legendarium]]'']]

In the meantime, King Peter Orseolo, who had succeeded King Stephen in Hungary in 1038, alienated many lords and prelates from himself, especially when he solemnly recognized the suzerainty of the [[Holy Roman Emperor]], [[Henry III, Holy Roman Emperor|Henry III]] in 1045.{{sfn|Engel|2001|p=29}}{{sfn|Kontler|1999|p=59}} According to the ''Illuminated Chronicle'', the discontented lords, "seeing the sufferings of their people",<ref>''The Hungarian Illuminated Chronicle'' (ch. 55.81), p. 111.</ref> assembled in [[Csanád]] (Cenad, Romania).{{sfn|Kristó|Makk|1996|p=71}} They agreed to send envoys to Andrew and Levente to Kiev in order to persuade them to return to Hungary.{{sfn|Kristó|Makk|1996|p=70}} Fearing "some treacherous ambush",<ref name="Chronicon_111">''The Hungarian Illuminated Chronicle'' (ch. 56.82), p. 111.</ref> the two brothers only set out after the agents they had sent to Hungary confirmed that the Hungarians were ripe for an uprising against the king.{{sfn|Kristó|Makk|1996|p=70}}

By the time the two brothers decided to return, a [[Vata pagan uprising|revolt had broken out]] in Hungary.{{sfn|Engel|2001|p=59}} It was dominated by pagans who captured many clergymen and mercilessly slaughtered them.{{sfn|Engel|2001|p=29}} Andrew and Levente met the rebels at [[Abaújvár]].{{sfn|Kristó|Makk|1996|p=70}} The ''Illuminated Chronicle'' narrates how the pagans urged the dukes "to allow the whole people to live according to the rites of the pagans, to kill the bishops and the clergy, to destroy the churches, to throw off the Christian faith and to worship idols".<ref name="Chronicon_111"/>{{sfn|Kristó|Makk|1996|p=70}} The same source adds that Andrew and Levente gave in to all their demands, "for otherwise they would not fight"<ref name="Chronicon_111"/> for them against King Peter.{{sfn|Kristó|Makk|1996|p=70}}{{sfn|Berend|Laszlovszky|Szakács|2007|p=339}}

The ''Annals of Altaich'' states that Andrew "savagely raged against the flock of the Holy Church".{{sfn|Berend|Laszlovszky|Szakács|2007|p=339}}{{sfn|Kristó|1999|p=256}} Even so, Bishop [[Gerard of Csanád]] and four other prelates were ready to join Andrew, but the pagans captured and slaughtered three of them (including Gerard) at [[Buda]].{{sfn|Kristó|Makk|1996|p=71}}{{sfn|Engel|2001|pp=29-30}} King Peter decided to flee from Hungary and take refugee in [[March of Austria|Austria]].{{sfn|Kristó|Makk|1996|p=71}} However, Andrew's envoys tricked the king to return before he reached the frontier, and they captured and blinded him.{{sfn|Kristó|Makk|1996|p=71}}{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=26}}

==Reign==

===Coronation (1046–1047)===

Most Hungarian lords and the prelates opposed the restoration of paganism.{{sfn|Kontler|1999|p=59}}{{sfn|Engel|2001|p=30}} They preferred the devout Christian Andrew to his pagan brother Levente,{{sfn|Kontler|1999|p=59}} even if, at least according to Kristó and Steinhübel, the latter was the eldest among Vazul's three sons.{{sfn|Kristó|Makk|1996|pp=68, Appendix 2}}{{sfn|Steinhübel|2011|p=25}} The Hungarian chronicles write that Levente, who died in short time, did not oppose his brother's ascension to the throne.{{sfn|Steinhübel|2011|p=25}}{{sfn|Kristó|Makk|1996|p=71}} The three bishops who had survived the pagan uprising crowned Andrew in Székesfehérvár in the last quarter of 1046 or in the spring of 1047.{{sfn|Kristó|Makk|1996|p=71}}{{sfn|Makk|1993|p=71}} Historian Ferenc Makk writes that Andrew was crowned with a crown that the Byzantine Emperor [[Constantine IX Monomachos]] had sent to him.{{sfn|Makk|1993|p=71}} Nine [[vitreous enamel|enamelled]] plaques from this golden crown were unearthed in [[Nyitraivánka]] (Ivanka pri Nitre, Slovakia) in the 19th century.{{sfn|Buckton|1984|p=46}} Andrew soon broke with his pagan supporters, restored Christianity and declared pagan rites illegal.{{sfn|Berend|Laszlovszky|Szakács|2007|p=339}}{{sfn|Engel|2001|p=30}} According to Kosztolnyik, Andrew's [[epithet]]s (the White or the Catholic) are connected to these events.{{sfn|Kosztolnyik|1981|p=74}}

[[File:Coronation of Andrew I (Chronicon Pictum 060).jpg |thumbnail |right |Coronation of Andrew I (''[[Chronicon Pictum|Illuminated Chronicle]]'')]]

{{Quote|''Having now been made secure against all disturbances from enemies, Duke Andreas received the crown of kingship in the [[Székesfehérvár|royal city of Alba]]. No more than three bishops who had escaped that great slaughter of the Christians performed the ceremony of coronation in the [[Anno Domini|year of our Lord]] 1047. He made proclamation to all his people that under pain of death they should lay aside the pagan rites which had formerly been permitted to them, and that they should return to the true faith of Christ and live in all things according to the law which King St Stephen had taught them.''|''[[Illuminated Chronicle|The Hungarian Illuminated Chronicle]]''<ref>''The Hungarian Illuminated Chronicle'' (ch. 60.86), p. 113.</ref>}}

===Wars with the Holy Roman Empire (1047–1053)===

The contemporaneous [[Hermann of Reichenau]] narrates that Andrew "sent frequent envoys with humble entreaties" to Emperor Henry III, proposing "an annual tribute and faithful service"<ref>''Herman of Reichenau, Chronicle'' (year 1047), p. 82.</ref> if the emperor recognized his reign.{{sfn|Kristó|Makk|1996|pp=72-73}} Andrew persuaded his brother, Béla, to return from Poland to Hungary in 1048.{{sfn|Steinhübel|2011|p=26}} He also granted his brother [[ducatus|one third of the kingdom]]{{sfn|Steinhübel|2011|p=26}}{{sfn|Kristó|Makk|1996|p=72}} with the title of duke.{{sfn|Engel|2001|p=30}} Béla's duchy comprised two regions which were centered on [[Nyitra]] (Nitra, [[Slovakia]]) and [[Biharia|Bihar]] (Biharia, [[Romania]]).{{sfn|Steinhübel|2011|p=26}}{{sfn|Engel|2001|p=30}}

Skirmishes on the frontier between Hungary and the Holy Roman Empire first occurred in 1050.{{sfn|Kristó|Makk|1996|p=73}} Emperor Henry invaded Hungary in August{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=26}} 1051, but Andrew and Béla successfully applied [[scorched earth]] tactics against the imperial troops and forced them to withdraw.{{sfn|Kristó|Makk|1996|p=73}}{{sfn|Engel|2001|p=30}} Legend says that the [[Vértes Mountains|Vértes]] Hills near [[Székesfehérvár]] were named after the armours{{spaced ndash}}''vért'' in Hungarian{{spaced ndash}}which were discarded by the retreating German soldiers.{{sfn|Engel|2001|p=30}}

Andrew initiated new peace negotiations with the emperor and promised to pay an annual tribute, but his offers were refused.{{sfn|Kristó|Makk|1996|p=73}} Next summer, the emperor returned to Hungary and laid siege to [[Pressburg]] (Bratislava, Slovakia).{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=26}} Zotmund, "a most skilful swimmer"<ref>''The Hungarian Illuminated Chronicle:'' (ch. 61.89), p. 114.</ref> scuttled the emperor's ships.{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=26}}{{sfn|Kristó|Makk|1996|p=73}} After [[Pope Leo IX]] mediated a peace treaty, the emperor lifted the siege and withdrew from Hungary.{{sfn|Bartl|Čičaj|Kohútova|Letz|2002|p=26}}{{sfn|Kristó|Makk|1996|p=73}} Andrew soon refused to fulfill his promises made under duress,{{sfn|Kristó|Makk|1996|p=73}} and even allied with [[Conrad I, Duke of Bavaria]], a prominent opponent of Emperor Henry III.{{sfn|Robinson|1999|p=22}}

{{Quote|''Because Andreas, the king of the Hungarians was less and less inclined to send envoys and to make promises concerning a peace treaty,'' [the emperor] ''laid siege to the fortress of Pressburg and for a long time attacked it with various machines of war. Since, however, God aided the besieged, who anxiously called on Him, his efforts were always frustrated and he could by no means capture it. Meanwhile the lord Pope Leo had intervened at the request of Andreas to make peace and he called on the emperor to end the siege. Since'' [the pope] ''found'' [the emperor] ''in all respects in agreement with him, while discovering that Andreas on the contrary was less obedient to his advice, he was angry and threatened the latter with excommunincation for mocking the [[Holy See|apostolic see]].''|[[Hermann of Reichenau|Herman of Reichenau]]: ''Chronicle''<ref>''Herman of Reichenau, Chronicle'' (year 1052), pp.&nbsp;92–93.</ref>}}

===Succession crisis and death (1053–1060)===

Andrew's queen, Anastasia, gave birth to a son, named [[Solomon of Hungary|Solomon]] in 1053.{{sfn|Kristó|Makk|1996|p=75}} Andrew attempted to make his son's succession secure, even against his brother, Béla, who had strong claim to succeed Andrew according to the traditional principle of [[seniority]].{{sfn|Engel|2001|pp=30-31}}

[[File:Andrew I on his death bed (Chronicon Pictum 064).jpg|thumb |left |alt=Béla chooses the sword | The scene at [[Tiszavárkony]] depicted in the ''[[Illuminated Chronicle]]'': the paralyzed Andrew forces his brother, [[Béla I of Hungary|Béla]] to choose between the crown and the sword]]

The brothers' relationship did not deteriorate immediately after Solomon's birth.{{sfn|Kristó|Makk|1996|p=80}} In the [[Establishing charter of the abbey of Tihany|deed of the foundation]] of the [[Tihany Abbey]], a [[Benedictine]] monastery established in 1055 by Andrew, Duke Béla was listed among the lords witnessing the act.{{sfn|Kristó|Makk|1996|p=80}} This charter, although primarily written in Latin, contains the earliest extant text{{spaced ndash}}''Feheruuaru rea meneh hodu utu rea'' ("on the military road which leads to Fehérvár"){{spaced ndash}}written in Hungarian.{{sfn|Engel|2001|p=39}} Andrew also established a [[lavra]] for Orthodox hermits in Tihany and an Orthodox monastery near [[Visegrád]].{{sfn|Kristó|Makk|1996|pp=74-75}} The ''Third Book of Law'' of King [[Ladislaus I of Hungary]] (r. 1077–1095) refers to an "estate survey of the judge Sarkas" under "King Andrew and Duke Béla".<ref>''Laws of King Ladislas I'' (Ladislas III:2), p. 1.</ref>{{sfn|Györffy|1994|p=134}} According to [[György Györffy]], the serfs of the royal domains were registered during this survey which took place around 1056.{{sfn|Györffy|1994|p=134}}

Andrew suffered a [[stroke]] which paralyzed him.{{sfn|Kristó|Makk|1996|p=75}} In an attempt to strengthen his son's claim to the throne, he had the child Solomon crowned in the one-year-long period beginning in the autumn of 1057.{{sfn|Kristó|Makk|1996|p=75}} For the same purpose, Andrew also arranged the engagement of his son with [[Judith of Swabia|Judith]]{{spaced ndash}}a daughter of the late Emperor Henry III, and sister of the new German monarch, [[Henry IV, Holy Roman Emperor|Henry IV]] (r. 1056–1105){{spaced ndash}}in September 1058.{{sfn|Robinson|1999|p=23}} Thereafter, according to an episode narrated by most Hungarian chronicles, the king invited Duke Béla to a meeting at [[Tiszavárkony]].{{sfn|Kontler|1999|p=60}} At their meeting, Andrew seemingly offered his brother to freely choose between a crown and a sword, which were the symbols of the kingdom and the ''ducatus'', respectively.{{sfn|Engel|2001|p=31}} Duke Béla, who had previously been informed by his partisans in Andrew's court that he would be murdered on the king's order if he opted for the crown, chose the sword.{{sfn|Engel|2001|p=31}}

However, Béla, who actually had no intention of renouncing his claim to succeed his brother in favor of his nephew, fled to Poland and sought military assistance from Duke [[Boleslaus II of Poland]] (r. 1058–1079).{{sfn|Engel|2001|p=31}}{{sfn|Manteuffel|1982|p=92}} With Duke Boleslaus's support, Béla returned to Hungary at the head of Polish troops.{{sfn|Robinson|1999|p=35}} On the other hand, the Dowager Empress [[Agnes of Poitou|Agnes]]{{spaced ndash}}who governed the Holy Roman Empire in the name of her minor son, Henry IV{{spaced ndash}}sent [[Bavaria]]n, [[Bohemia]]n and [[Duchy of Saxony|Saxon]] troops to assist Andrew.{{sfn|Robinson|1999|p=35}}

The decisive battle was fought in the regions east of the river [[Tisza]].{{sfn|Kristó|Makk|1996|p=75}} Andrew suffered injuries and lost the battle.{{sfn|Kristó|Makk|1996|p=75}}{{sfn|Engel|2001|p=31}} He attempted to flee to the Holy Roman Empire, but his brother's partisans routed his retinue at [[Mosonmagyaróvár|Moson]].{{sfn|Kristó|Makk|1996|p=75}} The ''Annals of Niederaltaich'' narrates that wagons and horses trampled him in the battlefield.{{sfn|Kosztolnyik|1981|p=77}} Deadly wounded in the battlefield, Andrew was seized and taken by his brother's partisans to [[Zirc]]{{sfn|Kristó|Makk|1996|p=75}} where "he was treated with neglect",<ref>''The Hungarian Illuminated Chronicle:'' (ch. 66.93), p. 116.</ref> according to the Illuminated Chronicle.{{sfn|Kosztolnyik|1981|p=77}} Andrew died in the royal manor there before his brother was crowned king on 6 December 1060.{{sfn|Kristó|Makk|1996|pp=75, 81}} Andrew was buried in the crypt of the church of the Tihany Abbey.{{sfn|Berend|Laszlovszky|Szakács|2007|p=348}}

==Family==

[[File:Solomon of Hungary David.jpg|thumb |right |alt=Two boys, one of the wearing a crown |The two sons of Andrew by his wife, [[Anastasia of Kiev]], King [[Solomon of Hungary]] (r. 1063–1074) and Duke [[David of Hungary|David]]]]

Andrew's wife, [[Anastasia of Kiev|Anastasia]], was the daughter of Grand Duke [[Yaroslav the Wise|Yaroslav I the Wise of Kiev]] by his wife, [[Ingegerd Olofsdotter of Sweden|Ingegerd]], who herself was the daughter of King [[Olof Skötkonung|Olof Skötkonung of Sweden]].{{sfn|Wertner|1892|p=117}} Andrew married Anastasia, who was born in about 1020, around 1038.{{sfn|Kristó|Makk|1996|p=69}} Their first child, [[Adelaide of Hungary|Adelaide]] was born around 1040.{{sfn|Kristó|Makk|1996|pp=69-70}} She became the wife of [[Vratislaus II of Bohemia]], who was initially Duke and, from 1085, King of Bohemia.{{sfn|Kristó|Makk|1996|p=Appendix 2}}{{sfn|Wertner|1892|p=123}} Andrew and Anastasia's first son, [[Solomon of Hungary|Solomon]], was born in 1053, their second son, [[David of Hungary|David]], some years later.{{sfn|Kristó|Makk|1996|p=Appendix 2}} Neither Solomon nor David fathered sons; the male line of Andrew's family died out with their death by the end of the 11th&nbsp;century.{{sfn|Kristó|Makk|1996|p=75}}

{{Quote|''King Salomon and David, his brother, never had children, and the seed of King Andreas perished with them. We believe that this was by an act of God; for on his first return with Levente, his brother, to Hungary, Andreas with the purpose of gaining the kingdom permitted the ungodly [[Vatha]] and other most evil men to kill the saintly Gerard and many Christians.''|''[[Illuminated Chronicle|The Hungarian Illuminated Chronicle]]''<ref>''The Hungarian Illuminated Chronicle'' (ch. 71.100), p. 118.</ref>}}

Medieval chronicles write that Andrew had a [[natural son]], named [[George, son of Andrew I of Hungary|George]], "by a [[concubine]]"<ref>''The Hungarian Illuminated Chronicle:'' (ch. 61.88), p. 113.</ref> from the village of [[Pilismarót]].{{sfn|Kristó|Makk|1996|p=86}} Since his name was popular among Orthodox believers, Gyula Kristó says that his mother may have been a [[Kievan Rus'|Russian]] lady-in-waiting of Andrew's queen.{{sfn|Kristó|Makk|1996|p=86}} The theory that the [[Clan Drummond]] in [[Scotland]] was descended from George is not widely accepted by scholars.{{sfn|Wertner|1892|p=136}}

The following family tree presents Andrew's ancestry, his offspring, and some of his relatives mentioned in the article.{{sfn|Kristó|Makk|1996|p=Appendices 1-2}}
{{familytree/start |summary=Andrew I's family}}
{{familytree |border=1| | | |TAK|y|AN1|  TAK=[[Taksony of Hungary|Taksony]]|AN1=a "Cuman" lady*}}
{{familytree | |,|-|-|-|-|^|-|-|-|.|}}
{{familytree |border=1|GÉZ| | | | | | |MIC|y|AN2|  GÉZ=[[Géza, Grand Prince of the Hungarians|Géza]]|MIC=[[Michael of Hungary|Michael]]|AN2=a Bulgarian princess**}}
{{familytree | |!| | | | | | | | | |,|^|-|-|-|.|}}
{{familytree |border=1|STE| | | |AN3|y|VAZ| | |LAS|  STE=[[Stephen I of Hungary]]|AN3=a lady of<br/>the Tátony clan|VAZ=[[Vazul]]|LAS=[[Ladislas the Bald]]|PRE=Premislava***}}
{{familytree | | | | | | |,|-|-|^|-|-|-|-|-|v|-|-|-|.|}}
{{familytree |border=1| |AN4|y|AND|v|~|ANA| |LEV| |BEL| AN4 = concubine from [[Pilismarót]]|AND=Andrew I of Hungary|ANA=[[Anastasia of Kiev]]|LEV=[[Levente]]|BEL=[[Béla I of Hungary]] |boxstyle_AND = background-color:  #d0e5f5}}
{{familytree | | | | |!| | | |!| | | | | | | | | | |!| | | }}
{{familytree |border=1| | | | |!| | | |!| | | | | | | | | |KOH | KOH=[[Kings of Hungary family tree|Kings of Hungary]]<br/>(from 1074)}}
{{familytree | | | | |!| | | |)|-|-|-|-|-|-|-|v|-|-|-|-|-|-|-|.| }}
{{familytree |border=1| | | |GEO| |ADE|~|VRA| |SAL|~|JUD| |DAV| GEO=[[George, son of Andrew I of Hungary|George]] |ADE=[[Adelaide of Hungary|Adelaide]] |VRA=[[Vratislaus II of Bohemia]]|SAL=[[Solomon of Hungary]] |JUD=[[Judith of Swabia]] |DAV=[[David of Hungary|David]]}}
{{familytree/end}}
''*A Khazar, Pecheneg or Volga Bulgarian lady.''<br/>''**Györffy writes that she may have been a member of the [[First Bulgarian Empire|Bulgarian]] [[Cometopuli dynasty]].''

==Gallery==
<Center>
<gallery>
File:Couronne Monomaque - Musée national hongrois.jpg |thumb |alt=Plaques of gold depicting people |The plaques of gold from the crown found at [[Nyitraivánka]] (Ivanka pri Nitre, Slovakia){{mdash}}Andrew I was crowned with this crown, according to historian Ferenc Makk
File:The sinking of the imperial ships at Pressburg (Chronicon Pictum 061).jpg |thumb |alt=A crowned man and two other men on a boat, with two men on a nearby boat |The sinking of the imperial ships at [[Pressburg]] by Zotmund, depicted in the ''[[Illuminated Chronicle]]''
File:Tihanycivertanlegi1.jpg |thumb |alt=A church with a walled courtyard |The [[Benedictine]] [[Tihany Abbey]] founded in 1055 by Andrew
File:Andrew I of Hungary tomb.jpg |thumb |alt=Andrew's tomb |Andrew's tomb in the crypt of the church of the [[Tihany Abbey]]
</gallery>
</Center>

==References==
{{Reflist|2}}

==Sources==

===Primary sources===
{{Refbegin}}
*"Herman of Reichenau, ''Chronicle''" (2008). In Robinson, I. S. Eleventh-Century Germany: The Swabian Chronicles. Manchester University Press. pp.&nbsp;58–98. ISBN 978-0-7190-7734-0.
*''Simon of Kéza: The Deeds of the Hungarians'' (Edited and translated by László Veszprémy and Frank Schaer with a study by Jenő Szűcs) (1999). CEU Press. ISBN 963-9116-31-9.
*''The Hungarian Illuminated Chronicle:'' Chronica de Gestis Hungarorum (Edited by Dezső Dercsényi) (1970). Corvina, Taplinger Publishing. ISBN 0-8008-4015-1.
*"The Laws of King Ladislas I (1077–1095): Book Three". In ''The Laws of the Medieval Kingdom of Hungary, 1000&ndash;1301'' (Translated and Edited by János M. Bak, György Bónis, James Ross Sweeney with an essay on previous editions by Andor Czizmadia, Second revised edition, In collaboration with Leslie S. Domonkos) (1999). Charles Schlacks, Jr. Publishers. pp.&nbsp;15–22. ISBN 1-884445-29-2. {{OCLC|495379882}}. {{OCLC|248424393}}. {{LCCN|89010492}}. {{OL|12153527M}}. (ISBN may be misprinted in the book as 88445-29-2)<!--Not 10 or 13 digits long-->.
{{Refend}}

===Secondary sources===
{{Refbegin}}
*{{cite book |last1=Bartl |first1=Július |last2=Čičaj |first2=Viliam |last3=Kohútova |first3=Mária |last4=Letz |first4=Róbert |last5=Segeš |first5=Vladimír |last6=Škvarna |first6=Dušan |year=2002|title=Slovak History: Chronology & Lexicon |publisher= Bolchazy-Carducci Publishers, Slovenské Pedegogické Nakladatel'stvo |isbn=0-86516-444-4|ref=harv}}
*{{cite book |last1=Berend |first1=Nora |last2=Laszlovszky |first2=József |last3=Szakács |first3=Béla Zsolt |editor-last=Berend |editor-first=Nora | title=Christianization and the Rise of Christian Monarchy: Scandinavia, Central Europe and Rus','' c.''900-1200 |publisher=Cambridge University Press |year=2007 |pages=319–368 |chapter=The kingdom of Hungary |isbn=978-0-521-87616-2|ref=harv}}
*{{cite book |last=Buckton |first=David |year=1984 |title=The Treasury of San Marco, Venice |publisher= Metropolitan Museum of Art |isbn= |ref=harv}}
*{{cite book |last=Curta |first=Florin |year=2006 |title=Southeastern Europe in the Middle Ages, 500-1250 |publisher=Cambridge University Press |isbn=978-0-521-89452-4|ref=harv}}
*{{cite book |last=Engel |first=Pál |year=2001|title=The Realm of St Stephen: A History of Medieval Hungary, 895–1526 |publisher= I.B. Tauris Publishers |isbn=1-86064-061-3|ref=harv}}
*{{cite book |last=Györffy |first=György |year=1994|title=King Saint Stephen of Hungary |publisher= Atlantic Research and Publications |isbn=0-88033-300-6|ref=harv}}
*{{cite book |last=Györffy |first=György |year=2000 |title=István király és műve ''[=King Stephen and his Work]'' |publisher= Balassi Kiadó |isbn= |ref=harv|language=hu}}
*{{cite book |last=Kontler |first=László |year=1999 |title=Millennium in Central Europe: A History of Hungary |publisher=Atlantisz Publishing House |isbn=963-9165-37-9 |ref=harv}}
*{{cite book |last=Kosztolnyik |first=Z. J. |year=1981|title=Five Eleventh Century Hungarian Kings: Their Policies and their Relations with Rome|publisher= Columbia University Press |isbn=0-914710-73-7|ref=harv}}
*{{Cite book |last1=Kristó |first1=Gyula |last2=Makk |first2=Ferenc |year=1996 |title=Az Árpád-ház uralkodói ''[=Rulers of the House of Árpád]''|publisher=I.P.C. Könyvek | isbn=963-7930-97-3|ref=harv|language=hu}}
*{{Cite book |last=Kristó |first=Gyula |year=1999 |title=Az államalapítás korának írott forrásai ''[=Written Sources of the Ages of the Foundation of the State]''|publisher=Szegedi Középkorász Műhely | isbn=963-482-393-9|ref=harv|language=hu}}
*{{Cite book |last=Makk |first=Ferenc |year=1993 |title=Magyar külpolitika (896-1196) ''[Hungarian External Politics (896–1196)]''|publisher=Szegedi Középkorász Műhely |isbn=963-04-2913-6|ref=harv|language=hu}}
*{{cite book |last=Manteuffel |first=Tadeusz |year=1982 |title=The Formation of the Polish State: The Period of Ducal Rule, 963–1194 ''(Translated and with an Introduction by Andrew Gorski)'' |publisher= Wayne State University Press |isbn=0-8143-1682-4|ref=harv}}
*{{Cite book |last=Robinson |first=I. S. |year=1999 |title=Henry IV of Germany, 1056–1106 |publisher=Cambridge University Press | isbn=0-521-54590-0|ref=harv}}
*{{cite book |last=Steinhübel |first=Ján |editor1-last=Teich |editor1-first=Mikuláš |editor2-last=Kováč |editor2-first=Dušan |editor3-last=Brown |editor3-first=Martin D. | title=Slovakia in History |publisher=Cambridge University Press |year=2011 |pages=15–29 |chapter=The Duchy of Nitra |isbn=978-0-521-80253-6|ref=harv}}
*{{cite book |last=Wertner |first=Mór |year=1892 |title=Az Árpádok családi története ''[=Family History of the Árpáds]'' |publisher= Szabó Ferencz N.-eleméri plébános & Pleitz Fer. Pál Könyvnyomdája |isbn= |ref=harv|language=hu}}
{{Refend}}

{{s-start}}
{{s-hou|[[House of Árpád]]| |c. 1015|before 6 December|1060}}
{{s-reg}}
|-
{{succession box|title=[[King of Hungary]]|before=[[Peter Orseolo, King of Hungary|Peter]]|after=[[Béla I of Hungary|Béla I]]|years=1046–1060}}
{{s-end}}
{{Hungarian kings}}
{{Authority control}}
{{commons category|Andrew I of Hungary}}

{{DEFAULTSORT:Andrew 01 Of Hungary}}
[[Category:1010s births]]
[[Category:1060 deaths]]
[[Category:House of Árpád]]
[[Category:Kings of Hungary]]
[[Category:Year of birth unknown]]