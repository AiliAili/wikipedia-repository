{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type     = suburb
| name     = Dulwich
| city     = Adelaide
| state    = sa
| image    =
| caption  =
| lga      = [[City of Burnside]]
| postcode = 5065
| est      = 1854
| pop      = 1,680| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name=2006census>{{Census 2006 AUS | id = SSC41376 | name = Dulwich (State Suburb) | quick = on | accessdate=4 June 2008}}</ref><br />1,596 (2001 census)<ref>{{Census 2001 AUS|id =SSC41361|name=Dulwich (State Suburb)|accessdate=18 July 2008| quick=on}}</ref>
| area     =
| stategov =
| fedgov   =
| near-w   = [[Adelaide city centre|Adelaide]]<br />[[Adelaide Parklands|''(Parklands)'']]
| near-nw  = [[Adelaide city centre|Adelaide]]<br />[[Adelaide Parklands|''(Parklands)'']]
| near-n   = [[Rose Park, South Australia|Rose Park]]
| near-ne  = [[Toorak Gardens, South Australia|Toorak Gardens]]
| near-e   = [[Toorak Gardens, South Australia|Toorak Gardens]]
| near-se  = [[Glenside, South Australia|Glenside]]
| near-s   = [[Glenside, South Australia|Glenside]]
| near-sw  = [[Eastwood, South Australia|Eastwood]]
| dist1    =
| location1=
}}
'''Dulwich''' {{IPAc-en|ˈ|d|ʌ|l|ᵻ|tʃ}} is a [[suburb]] in the [[City of Burnside]], [[Adelaide]], [[South Australia]] with a census area population of 2,663 people.{{Citation needed|date=September 2010}} The suburb is adjacent to Adelaide's [[Adelaide Parklands|east parklands]], and forms part of the western boundary of the City of Burnside. Dulwich is a mix of residential housing and commercial activity&ndash;corporate offices and businesses line [[Fullarton Road, Adelaide|Fullarton]] and [[Greenhill Road, Adelaide|Greenhill]] Roads. The suburb is bordered by [[Rose Park, South Australia|Rose Park]] to the north, [[Toorak Gardens, South Australia|Toorak Gardens]] to the east, [[Glenside, South Australia|Glenside]] to the south and the Adelaide Parklands to the west.

The area, which was settled by Europeans in the 19th century and used as pasture, made a slow transition to a residential suburb which was complete by the mid 20th century. Much of the area's 19th century housing stock has been recognised with heritage protection. Dulwich's close location to the [[Adelaide city centre]], grand old houses and leafy tree-lined streets make it an attractive and sought-after suburb.

==History==
Prior to European settlement, the general area was inhabited by the [[Kaurna people|Kaurna]] tribe of [[Indigenous Australians]].<ref>{{cite web|url=http://www.southaustralia.com/Aboriginal_Exp_Kaurna_Map.pdf |title=Archived copy |accessdate=2010-04-22 |deadurl=yes |archiveurl=https://web.archive.org/web/20110727033148/http://www.southaustralia.com/Aboriginal_Exp_Kaurna_Map.pdf |archivedate=27 July 2011 |df=dmy }}</ref>

Dulwich, named after the [[Dulwich|settlement]] in the [[London]] [[London Borough of Southwark|Borough of Southwark]], has its origins in Section 263 of the [[Adelaide|Adelaide region]] as laid out by South Australia's first chief surveyor, [[William Light|Colonel William Light]]. It was bought by a Captain of the [[Royal Navy]], Daniel Pring. In his initial absence the section was leased to a local cattle dealer, but upon the early death of Pring, his wife inherited the area. After several transactions the land was sold; John Hector, manager of the [[State Bank of South Australia|Savings Bank of South Australia]] bought much of the land with the exception of that located around Victoria Park. It was Hector who christened the land as the 'Village of Dulwich', or in the colloquial terms of the time, 'Hector's Paddock'. Hector oversaw the subdivision of the land and its transfer to many new owners. The Adelaide press at the time, in the form of the [[The Advertiser (Adelaide)|''Advertiser'']] and the ''South Australian Gazetter'', was exceptionally generous in their words relating to the new village: 'The Suburban Village of Dulwich ... beautifully situated on a gentle rise sufficient to command a view of the sea, with the noble amphitheatre of the hills for a background', 'For building sites convenient to the metropolis, Dulwich has no rival'.

[[Image:House in dulwich, 1908.jpg|250px|thumb|left|House in Dulwich, 1908. The style is representative of many houses found in suburban Adelaide, bringing a distinct uniformity to the area.]]
While Dulwich in 1881 was only home to four residences, by 1891, after a period of explosive growth, there were 50. Businesses began to establish themselves in Dulwich during the early part of the 20th century. By the 1930s, Dulwich was home to [[manufacturers]], blacksmiths, [[engineers]] and other groups. This business establishment experienced a surge in the latter part of the 20th century, and offices and businesses now completely line the two major road borders ([[Greenhill Road, Adelaide|Greenhill]] and [[Fullarton Road, Adelaide|Fullarton Road]]s), attracted there by the close proximity to the [[Adelaide city centre]] and the lush surroundings.

With many sons of the suburb fighting in [[World War II]], a [[Returned and Services League of Australia|Returned Services League]] was founded with their return. Eventually the league building became that of the Dulwich Retired Citizens Club, and with its purchase by the [[City of Burnside|Burnside Council]], it has become the Dulwich Community Centre. Between 1955 and 1958, a young [[Tony Blair]] (British [[Prime Minister of the United Kingdom|Prime Minister]] 1997–2007) lived with his family in a house on Ormond Grove while his father Leo was a [[Law]] lecturer at the [[University of Adelaide]].<ref>http://www.adelaide.edu.au/adelaidean/issues/3741/news3770.html</ref>  One of the first [[Kentucky Fried Chicken]] stores in the country was established in Dulwich in the 1980s, however by 2000 it had moved to nearby [[Eastwood, South Australia|Eastwood]].{{Citation needed|date=April 2010}}

==Attractions==
*Dulwich Community Centre
*St. Patrick's Special School
*Dulwich Centre (aka Dulwich Village)

==Residents==
[[Image:Foreignborn dulwich rosepark.svg|right|300px|thumb|Percentages of the fifth of the population born abroad]]
According to the 2001 Census, the population of the Dulwich census area (which includes adjoining [[Rose Park, South Australia|Rose Park]]) is 2,663 people,{{Citation needed|date=September 2010}} with a very slight decrease in population between the 1996 and 2001 censuses. 52.4% of the population is female, 79.4% are Australian born (see chart for a breakdown of foreign-born) and 92.5% of residents are Australian citizens. [[Religion|Religious]] adherence in Dulwich is lower than the Burnside and Adelaide average, standing at 64.3%. The eight strongest religions in decreasing order are; [[Catholic]], [[Anglican]], [[Uniting Church]], [[Orthodox Christianity|Orthodox]], [[Lutheran]], [[Presbyterian]], [[Buddhism]] and [[Hinduism]]. The most common type of dwelling was a Separate House (64.4%) followed by a flat, apartment or other (24.9%) and a semi-detached house (9.6%). Dulwich has a highly educated population with 44.5% holding a diploma or higher degree. This educational attainment is reflected in household income - almost two thirds earn over [[Australian Dollar|A$]]1000 per week.

Similar to other inner-city suburbs, Dulwich has a large proportion of [[students]] who attend nearby universities (9.2%). Overseas students are a sizeable component of this population and represent some of the 7.5% of residents who are not Australian citizens. 60% of households represent families with children; the remaining households are divided among couples without children, lone person households and other non-traditional groupings.{{ref|residents}}

==Transport==
[[Automobile|Cars]] are the dominant means of transport to work in Dulwich; 70% of the population are either a driver or passenger in a vehicle. 10% walked, [[Bicycle|cycled]] or caught [[public transport]]. Public transport usage is notably higher than the [[City of Burnside]] and Adelaide average, owing to the suburb's close inner-city location. A number of bus routes serve the suburb: the 145 travels to [[Glen Osmond, South Australia|Glen Osmond]] through Dulwich, as does the 146 to [[Urrbrae, South Australia|Urrbrae]]; the 580 route travels from [[Mile End, South Australia|Mile End]] to [[Paradise Interchange]] along [[Portrush Road, Adelaide|Portrush Road]]; the 820 [[Adelaide Hills|Hills]] bus travels along [[Greenhill Road, Adelaide|Greenhill Road]] to [[Carey Gully, South Australia|Carey Gully]]. 10% of the population owns no vehicles, 40% owns one, 35% owns two and the remainder own three or more. One early problem with Adelaide's streets built in a grid layout was the tendency for motorists to use inner suburb local roads instead of main roads. 'Rat trails' of cars sneaked through narrow sidestreets, creating sizeable bottlenecks. This was a particular problem for Dulwich because of its location - various traffic control methods were put in place (closing streets, speed bumps, lowered speed limits, roundabouts) to counter these problems. This forced the re-routing of traffic onto local thuroughfares such as [[Fullarton Road, Adelaide|Fullarton]], Greenhill, [[Kensington Road, Adelaide|Kensington]] and Portrush Roads.{{ref|transport}}

==Government==
{| class="wikitable" style="float:right; margin-left:1em"
! colspan="3" | State Elections<small>{{ref|stateelec}}</small>
|-
| {{Australian party style|Liberal}}|&nbsp;
| [[Liberal Party of Australia|Liberal]]
| bgcolor="FFFF99" | 58%
|-
| {{Australian party style|Labor}}|&nbsp;
| [[Australian Labor Party|Labor]]
| bgcolor="FFFF99" | 25%
|-
| {{Australian party style|Democrats}}|&nbsp;
| [[Australian Democrats|Democrats]]
| bgcolor="FFFF99" | 13%
|-
| {{Australian party style|Family First}}|&nbsp;
| [[Family First Party|Family First]]
| bgcolor="FFFF99" | 2%
|-
| bgcolor="#D8D8D8" |&nbsp;
| [[SA First]]
| bgcolor="FFFF99" | 2%
|}
{| class="wikitable" style="float:right; margin-left:1em"
! colspan = 3 | Federal Elections<small>{{ref|fedelec}}</small>
|-
| {{Australian party style|Liberal}}|&nbsp;
| Liberal
| bgcolor="FFFF99" | 60%
|-
| {{Australian party style|Labor}}|&nbsp;
| Labor
| bgcolor="FFFF99" | 30%
|-
| {{Australian party style|Greens}}|&nbsp;
| [[Australian Greens|Greens]]
| bgcolor="FFFF99" | 7%
|-
| {{Australian party style|Democrats}}|&nbsp;
| Democrats
| bgcolor="FFFF99" | 1.5%
|-
| {{Australian party style|Family First}}|&nbsp;
| Family First
| bgcolor="FFFF99" | 1.5%
|}

Dulwich is part of the state [[Electoral district of Bragg (South Australia)|Electoral District of Bragg]], which has been held since 2002 by Liberal [[Member of Parliament|MP]] [[Vickie Chapman]]. In federal politics, the suburb is part of the [[Division of Adelaide]], and has been represented by Labor MP [[Kate Ellis]] since 2004. The results shown are from the closest polling station to Dulwich &mdash; which is located outside of the suburb &mdash; at Rose Park Primary School in nearby [[Rose Park, South Australia|Rose Park]].

Dulwich is a socially [[centrism|moderate]] and [[economic liberalism|economically liberal]] suburb.{{citation needed|date=December 2016}} The centre-right Liberal Party polls very well in the area with around 60% of the vote in the [[Australian federal election, 2007|2007]] and [[Australian federal election, 2010|2010 federal elections]] . The area previously had a relatively strong [[Australian Democrats|Democrat]] vote, until very poor results in the Federal Election of 2004 and the State Election of 2006.{{Citation needed|date=June 2011}} The [[Australian Labor Party|Labor Party]] and the [[Australian Greens|Greens]] have absorbed much of the former Democrat vote.{{Citation needed|date=June 2011}}

At the [[Local government in Australia|council]] level, Dulwich is part of the Rose Park & Toorak Gardens [[Ward (electoral subdivision)|Ward]] of the [[City of Burnside]].<ref>{{cite web | url = https://www.burnside.sa.gov.au/About-Council/Councillors-Administration/Wards | title = City of Burnside - Wards | accessdate = 14 December 2016 }}</ref>

==References==
{{Reflist}}

==Notes==
# {{note|residents}} ''City of Burnside Community Profile for Dulwich/Rose Park'' [http://www.id.com.au/burnside/commprofile/default.asp?id=139&gid=120&pg=1]
# {{note|transport}} ''City of Burnside Community Profile for Dulwich/Rose Park: How do we get to work?'' [http://www.id.com.au/burnside/commprofile/default.asp?id=139&gid=120&pg=17]

{{Coord|-34.936|138.630|format=dms|type:city_region:AU-SA|display=title}}
{{City of Burnside suburbs}}

[[Category:Suburbs of Adelaide]]