{{Infobox weapon
|name= '''Leopard 2E'''
|image= [[File:Leopardo 2E. zaragoza 1.jpg|300px]]
|caption= Spanish Leopard 2E in [[Zaragoza]], June 2008
|origin= [[Germany]]/[[Spain]]
|type= [[Main battle tank]]
<!-- Type selection -->
|is_ranged=
|is_bladed=
|is_explosive=
|is_artillery= 
|is_vehicle= yes
|is_missile=
|is_UK=
<!-- Service history -->
|service= December 2003–present
|used_by= [[Spain]]
|wars=
<!-- Production history -->
|designer= [[Krauss-Maffei Wegmann]]
|design_date=
|manufacturer= [[General Dynamics Santa Bárbara Sistemas]]
|unit_cost=€10.95m including spares etc
|production_date= 2003–08
|number= 219
|variants=
<!-- General specifications -->
|spec_label=
|weight= approx. 63&nbsp;tonnes (69.4&nbsp;[[Short ton|tons]])
|length= 7.7&nbsp;m (25.26&nbsp;ft)
|part_length=
|width= 3.7&nbsp;m (12.14&nbsp;ft)
|height= 3.0&nbsp;m (9.84&nbsp;ft)
|diameter=
|crew= 4
<!-- Vehicle/missile specifications -->
|armour= [[Composite armor]]
|primary_armament= [[Rheinmetall 120 mm gun|Rheinmetall 120 mm L/55]]
|secondary_armament= 2 × 7.62&nbsp;mm [[MG3]]s
|engine= [[MTU Friedrichshafen|MTU]] MB 873 Ka-501 12-cylinder diesel
|engine_power= 1,500&nbsp;PS (1,479&nbsp;hp, 1,103&nbsp;kW) at 2600&nbsp;rpm
|pw_ratio= 23.8&nbsp;PS/t (17.5&nbsp;kW/t)
|transmission= Renk HSWL 354
|payload_capacity=
|suspension= [[Torsion-bar]]
|clearance=
|wingspan=
|propellant=
|fuel_capacity= 1,060&nbsp;[[liter]]s (208.02&nbsp;[[gallon]]s)
|vehicle_range= {{convert|500|km|mi|abbr=on}}
|ceiling=
|altitude=
|boost=
|speed= 72&nbsp;km/h (44.74&nbsp;mi/h)
|guidance=
|steering=
|accuracy=
|launch_platform=
|transport=
}}
{{Spanish tanks}}

The '''Leopard 2E''' or '''Leopard 2A6E''' (''E'' stands for ''España'', [[Spanish language|Spanish]] for Spain) is a variant of the German [[Leopard 2]] [[main battle tank]], tailored to the requirements of the [[Spanish Army|Spanish army]], which acquired it as part of an armament modernization program named ''Programa Coraza'', or Program Breastplate. The acquisition program for the Leopard&nbsp;2E began in 1994, five years after the cancellation of the [[Lince (tank)|Lince]] tank program that culminated in an agreement to transfer 108 [[Leopard 2#Leopard 2A4|Leopard 2A4s]] to the Spanish army in 1998 and started the local production of the Leopard 2E in December 2003. Despite postponement of production owing to the 2003 merger between [[Santa Bárbara Sistemas]] and [[General Dynamics]], and continued fabrication issues between 2006 and 2007, 219 Leopard&nbsp;2Es have been delivered to the Spanish army.

The Leopard&nbsp;2E is a major improvement over the [[M60 Patton]] tank, which it replaced in Spain's mechanized and armored units. Its development represented a total of 2.6&nbsp;million [[man-hour]]s worth of work, 9,600 of them in Germany, at a total cost of 2.4&nbsp;billion [[euros]]. This makes it one of the most expensive Leopard 2s built. Indigenous production amounted to 60% and the vehicles were assembled locally at [[Sevilla]] by Santa Bárbara Sistemas. It has thicker armor on the [[Gun turret|turret]] and [[glacis]] plate than the German Leopard 2A6, and uses a Spanish-designed tank command and control system, similar to the one fitted in German Leopard 2s. The Leopard&nbsp;2E is expected to remain in service until 2025.

== Spanish armor programs 1987–93 ==
By 1987, the [[Spanish Army|Spanish army]] was equipped with 299 French-designed [[AMX-30E]]s, assembled by Santa Bárbara Sistemas,<ref>de Mazarrasa, ''Carro de Combate AMX-30E'', p. 60</ref> and 552 American [[M47 Patton|M47]] and [[M48 Patton]] tanks.<ref>Manrique and Molina, ''La Brunete'', pp. 69–74</ref> The AMX-30Es were put  into service  in 1970,<ref>de Mazarrasa, ''Carro de Combate AMX-30E'', p. 58</ref> while the latter went into service in the mid-1950s.<ref>Zaloga, ''The M47 and M48 Patton Tanks'', pp. 36–37</ref> Although Spain's M47s and M48s were modernized to M47Es and M48Es, bringing them to near equivalence with the [[M60 Patton]] tank,<ref name=Labrunete>Manrique and Molina, ''La Brunete'', p. 73</ref> the Spanish army considered them antiquated.<ref>Gonzáles, ''España negocia la adquisición de hasta 500 carros de combate que serán retirados de Centroeuropa'', El País</ref> In 1984, when deciding to replace its Patton tanks, the Spanish government declared its intention to produce a whole new main battle tank locally,  since known as the [[Lince (tank)|Lince]].<ref>El País, ''Cinco empresas compiten para cofabricar un carro de combate en España''</ref> Five companies expressed interest in bidding, including [[Krauss-Maffei]] in a partnership with Santa Bárbara Sistemas, [[Nexter|GIAT]] with what became the [[Leclerc]], [[General Dynamics]] with the [[M1 Abrams]] and [[Vickers]] with the Valiant.<ref>Yarnóz, ''El Gobierno elegirá el mes próximo el carro de combate de los noventa'', El País</ref> While the M1&nbsp;Abrams and Valiant bids were not accepted,<ref>Yarnóz, ''Serra descarta a EE UU y Reino Unido para cofabricar los nuevos carros'', El País</ref> the bidding continued until 1989 when it was officially canceled.<ref>Yarnóz, ''España eliminará decenas de carros de combate al concluir la negociación sobre desarme en Europa'', El País</ref>

Instead, the Spanish government opted to replace its older Patton<!--huh? replace Pattons with Pattons?--> tanks with [[United States|American]] [[M60 Patton]] tanks retired from Central Europe in accordance with the [[Treaty on Conventional Armed Forces in Europe]].<ref>El País, ''El tanque de los noventa''</ref> Although the Spanish army was originally to receive 532 M60 and M60A1 tanks,<ref>Gonzáles, ''Defensa cifra en 1.500 millones el coste de los 532 tanques estadounidenses que recibirá a partir de 1992'', El País</ref> only 260 M60A3s were ultimately delivered, of which 244 were put into active service in the army.<ref>Military Technology, ''Europe'', p. 192</ref> In the late 1980s the Spanish Ministry of Defense approved a modernization program for 150 of its AMX-30Es and a reconstruction program for the remaining 149 vehicles of this type, restoring them to their original condition.<ref>de Mazarrasa, ''Carro de Combate AMX-30E'', pp. 80–84</ref> However, neither the M60s nor the AMX-30s were a considerable improvement over Spain's fleet of M47 and M48 Patton tanks.<ref>Candil, ''Spain's Armor Force Modernizes'', p. 41</ref>

Since the existing tank fleet did not meet the Spanish army's needs<!--there should be an explanation of why they needed hundreds of advanced tanks-->, Spain opened talks with Germany and Krauss-Maffei over the possibility of future collaboration in regards to Spain's future tank,<ref name=CandilCdC161162>Candil, ''Carros de Combate'', pp. 161–162</ref> and sent a military delegation to Germany in 1994. Although the Germans offered Spain surplus [[Leopard 1]] tanks and Soviet equipment incorporated into the [[German army]] after the [[German reunification|reunification of Germany]], the Spanish government declined these offers and pressed for the Leopard 2.<ref name=CandilCdC161162 />

== Programa Coraza ==
[[Image:Spanish Leopard 2A4 Madrid.JPG|right|thumb|Spanish [[Leopard 2]]A4 in Madrid, October 2006]]
In March 1994, the Spanish Ministry of Defense created ''Programa Coraza 2000'' (Program Breastplate 2000), which focused on the procurement and integration of new armament for the Spanish army's modernization.<ref>Candil, ''Carros de Combate'', p. 162</ref> The program included the Leopard&nbsp;2E and the [[ASCOD AFV|Pizarro infantry combat vehicle]],<ref>Candil, ''Spain's Armor Force Modernizes'', pp. 41–42</ref> as well as the [[Eurocopter Tiger]] attack helicopter.<ref name="Candil20042"/> The program's scope extended to the integration of 108 Leopard&nbsp;2A4s,<ref>Candil, ''Carros de Combate'', p. 163</ref> which were leased to Spain in late 1995.<ref name=Jerchel1998>Jerchel & Schnellbacher, ''Leopard 2 Main Battle Tank 1979–1998'',p. 42</ref> Apart from procurement, ''Programa Coraza'' was meant to prepare the Spanish army logistically for the introduction of new matériel.<ref>Candil, ''Carros de Combate'', pp. 164–165</ref>

=== Leopard 2A4 ===
A [[memorandum of understanding]] was signed on {{nowrap|9 June}} 1995 between the German and Spanish governments, setting the foundations for an acquisition of up to 308 brand-new Leopard&nbsp;2Es. These were to be assembled in Spain by Santa Bárbara Sistemas, with 60–70 percent of the components manufactured by Spanish companies, and production taking place between 1998 and 2003. Furthermore, the German government agreed to lend the Spanish army 108 {{nowrap|Leopard 2A4s}} for training purposes for a period of five years.<ref name=Gonzales2005>González, ''Firmada adquisición de 308 tanques Leopard 2'', El País</ref> These vehicles were delivered between November 1995 and June 1996.<ref name=Jerchel1998 /> In 1998, Spain agreed to procure the ceded {{nowrap|Leopard 2A4s}} and reduce production of the brand-new Leopard&nbsp;2E to 219 vehicles. In 2005 it was declared that the 108 Leopard 2A4s were to cost Spain just €16.9m, to be paid by 2016.<ref>{{cite web | url=http://www.defenseindustrydaily.com/spain-finalizes-buy-of-108-leopard-2a4-tanks-01155/ | publisher=Defense Industry Daily | title= Spain Finalizes Buy of 108 Leopard 2A4 Tanks &#124; date - 8 September 2005}}</ref>

The {{nowrap|Leopard 2A4s}} equipped X and {{nowrap|XI Mechanized}} Infantry Brigade,<ref>Candil, ''Carros de Combate'', p. 167</ref> which at the time formed part of [[Eurocorps]].<ref>Candil, ''Spain's Armor Forces Modernizes'', p. 42</ref> As production of the Leopard&nbsp;2E began and these units received Leopard 2Es, their Leopard&nbsp;2A4s re-equipped the ''Alcántara'' Armored Cavalry Regiment, based in [[Melilla]].<ref>Fuerza Terrestre, ''Reseña histórica del RCAC Alcántara n° 10'', p. 78.</ref>

[[Image:Leopard 2E turret plate and mantlet.jpg|right|thumb|Close-up of the Leopard 2Es turret armor]]
Spain's Leopard&nbsp;2E is based on the [[Leopard 2#Leopard 2A6|Leopard 2A6]],<ref name=Candil2007>Candil, ''The Spanish Leopard 2E'', p. 66</ref> and incorporates the add-on wedge armor of the [[Leopard 2#Leopard 2A5|Leopard 2A5]] on the turret.<ref>Jerchel & Schnellbacher, ''Leopard 2 Main Battle Tank 1979–1998'', pp. 24–36</ref> This armor maximizes the armor depth that a [[kinetic energy penetrator]] must travel through to enter the internal volume of the turret.<ref>Hilmes, ''Aspects of future MBT conception''</ref> Like the Swedish Leopard 2S (Strv 122), the Leopard&nbsp;2E has increased armor thickness on the hull's glacis plate, the turret frontal arc and the turret roof,<ref name=Candil2007 /> bringing the vehicle's weight close to 63&nbsp;[[tonne]]s (69.4&nbsp;[[Short ton|tons]]).<ref>Candil, ''Leopard 2 daneses en Córdoba'', p. 62</ref> The vehicle's protection is  augmented by the added armor that is built into the tank during the manufacturing process, as opposed to being added on after assembly<ref name=Candil2007 /> as is the case for German Leopard&nbsp;2A5s and 2A6s.<ref>Jerchel & Schnellbacher, ''Leopard 2 Main Battle Tank 1979–1998'', p. 35</ref> As a consequence, the Leopard&nbsp;2E is one of the best-protected Leopard&nbsp;2s in service.<ref name=Candil2004 />

The tank is armed with Rheinmetall's [[Rheinmetall 120 mm gun#Rheinmetall L55 120mm|{{convert|120|mm|in|sp=us|adj=on}} L/55 tank-gun]], and is capable of adopting a {{convert|140|mm|in|sp=us|adj=on}} gun.<ref>Candil, ''Carros de Combate'', p. 170</ref> Both the tank commander and gunner have identical second generation thermal viewers, derived from those of the [[BGM-71 TOW|TOW 2B]] Light Launcher System.<ref name=Candil2004>Candil, ''Leopard 2E MBT Delivery Begins'', p. 74</ref> These are integrated into the tank by [[Indra sistemas|Indra]] and [[STN Atlas|Rheinmetall Defense Electronics]].<ref name=Candil2004 /> Indra  provides the tank's command and control system, called the Leopard Information and Command Equipment (LINCE),<ref name=Candil2007 /> based on the Swedish and German ''Integrierte Führungssysteme'' (IFIS).<ref name=Candil2004 /> Other differences between the Spanish Leopard 2E and other Leopard&nbsp;2A6s include an [[auxiliary power unit]], manufactured by SAPA, an air conditioning system and new rubber pads for the vehicle's tracks to increase their lifespan on the irregular Spanish terrain.<ref name=Candil2004 /> About 60% of each Leopard&nbsp;2E was manufactured in Spain, as opposed to  30% for the Swedish Leopard&nbsp;2S, for example.<ref>Candil, ''Carros de Combate'', pp. 168–169</ref>
[[Image:Leopard 2E climbing.jpg|right|thumb|Leopard 2E driven on to a transport truck]]
Although the final contract for the production of Spanish Leopard&nbsp;2Es was signed in 1998, calling for  a production rate of four tanks per month,<ref>Candil, ''Carros de Combate'', p. 168</ref> the first Leopard&nbsp;2Es were not manufactured until late 2003.<ref name=Candil20042>Candil, ''Leopard 2E Delivery Begins'', p. 73</ref> This was largely due to the merging of Santa Bárbara Sistemas with [[General Dynamics]],<ref name=Candil20042 /> and Krauss-Maffei's reservations regarding the sharing of the Leopard&nbsp;2's technology with a rival company<ref>Candil, ''Un entorno industrial plagado de dificultades'', p. 49</ref>  manufacturer of the [[M1 Abrams]].<ref>Green & Stewart, ''M1 Abrams at War'', p. 23</ref> Krauss-Maffei delivered 30 Leopard&nbsp;2Es between 2003 and 2006.<ref>[http://disarmament.un.org/UN_REGISTER.NSF Spain], United Nations Register of Conventional Arms, retrieved 2008-06-20</ref> Production by Santa Bárbara Sistemas was delayed after assembly had begun; between January and November  2007, for example, only three of the 43 Leopard&nbsp;2Es to be delivered to the Spanish army were actually delivered—with 15 more being delivered before the end of the year to make up for the earlier production problems.<ref name=Candil200840 /> By 1&nbsp;July 2006 the Spanish army had received 48 Leopard&nbsp;2Es and nine [[Leopard 2#Engineering and driver training tanks|Büffel armored recovery vehicles]], which was only a quarter of those contracted. Production of the Leopard 2E was planned to end by 2007 but was extended into 2008.<ref name=Candil2008 />

The Leopard&nbsp;2E replaced the Leopard&nbsp;2A4 in Spanish mechanized units, which in turn replaced M60s in cavalry units.<ref>Fuerza Terrestre, ''Carros Leopard 2A4 en Melilla'', p. 66</ref> Both versions of the Leopard 2 are expected to remain in service with the Spanish army until 2025.<ref name=Candil200840>Candil, ''Un entorno industrial plagado de dificultades'', p. 40</ref> In terms of industrial scale, the production and development of the Leopard&nbsp;2E represents a total of 2.6&nbsp;million [[man-hour]]s of work, including 9,600 in Germany.<ref name= "SBS">''[http://www.gdsbs.com/web/frame.asp?page=http://www.gdsbs.com/web/frames.asp Santa Bárbara Sistemas y el Programa Leopardo 2E]'', Santa Bárbara Sistemas, retrieved 2008-06-05 {{webarchive |url=https://web.archive.org/web/20080701200234/http://www.gdsbs.com/web/frame.asp?page=http://www.gdsbs.com/web/frames.asp |date=July 1, 2008 }}</ref> It is one of the most expensive Leopard 2s built;<ref name=Candil2008>Candil, ''Un entorno industrial plagado de dificultades'', p. 45</ref> the original contract was worth €1,910m but the final cost was €2,399m.<ref name=PEAs>{{cite web | url=http://www.revistatenea.es/revistaatenea/revista/PDF/documentos/Documento_1026.pdf | title=Evaluación de los Programas Especiales de Armamento (PEAs) | author =Ministerio de Defensa | location=Madrid | date=September 2011 | publisher=Grupo Atenea| language=Spanish | accessdate=30 September 2012}}</ref>

=== Comparison with other tanks in Spanish service ===
The Spanish army replaced its M60&nbsp;Patton tanks and AMX-30s with the Leopard&nbsp;2 between 1995 and 2008,  a considerable improvement in capability.<ref>Candil, ''Spain's Armor Force Modernizes'', p. 42</ref> Previously, the Spanish army was equipped with M47 and M48&nbsp;Patton tanks, which were upgraded to near M60 equivalency during the late 1970s and during the 1980s.<ref name=Labrunete /> Both the Leopard&nbsp;2A4 and Leopard&nbsp;2E sport a much more powerful gun than the AMX-30 and M60 tanks.<ref>Green & Stewart, ''M1 Abrams at War'', p. 59</ref> The Leopard 2's 1,500&nbsp;horsepower (1,110&nbsp;kW) engine<ref name=Jerchel /> provides greater power than the M60A3's 750&nbsp;horsepower (559.27&nbsp;kW)<ref name=Lathrop /> and the AMX-30EM2's 850&nbsp;horsepower (633.84&nbsp;kW) engines.<ref>de Mazarrasa, ''Carro de Combate AMX-30E'', p. 81</ref> On the other hand, the Leopard&nbsp;2 carries fewer, but larger rounds<ref name=Jerchel /> than the M60A3.<ref name=Lathrop />

{| class="wikitable" border=0 cellspacing=0 cellpadding=3 style="border-top:3px; border-collapse:collapse; text-align:left;" summary="Characteristics of the Leopard 2E"
|- style="vertical-align:bottom; border-bottom:1px solid #999;"
!
! style="text-align:left;" | Leopard 2E
! style="text-align:left;" | Leopard 2A4<ref name=Jerchel>Jerchel and Schnellbacher, ''Leopard 2 Main Battle Tank 1979–1998'', pp. 28–29</ref>
! style="text-align:left;" | M60A3<ref name=Lathrop>Lathrop & McDonald, ''M60 Main Battle Tank 1960–91'', p. 28</ref>
! style="text-align:left;" | AMX-30EM2<ref>de Mazarrasa, ''Carro de Combate AMX-30E'', pp. 80–83, 103–104</ref>
|-
! style="text-align:right;" | Weight
| 63 [[Tonne|t]] (69.45&nbsp;[[Short ton|tons]])
| 55&nbsp;t (60.63&nbsp;tons)
| 55.6&nbsp;t (61.1&nbsp;tons)
| 36&nbsp;t (39.68&nbsp;tons)
|-
! style="text-align:right;" | Gun
| 120&nbsp;mm L/55 smoothbore (4.72&nbsp;[[inch|in]])
| 120&nbsp;mm L/44 smoothbore (4.72&nbsp;in)
| [[Royal Ordnance L7|105&nbsp;mm M68 rifled tank-gun]] (4.13&nbsp;in)
| 105&nbsp;mm rifled (4.13&nbsp;in)
|-
! style="text-align:right;" | Ammunition
| 42 rounds
| 42 rounds
| 63 rounds
| Not available
|-
! style="text-align:right;" | Road&nbsp;range
| {{convert|500|km|mi|abbr=on}}
| {{convert|500|km|mi|abbr=on}}
| {{convert|480|km|mi|abbr=on}}
| {{convert|400|km|mi|abbr=on}}
|-
! style="text-align:right;" | Engine output
| 1,500&nbsp;[[metric horsepower|PS]] (1,479&nbsp;hp, 1,103&nbsp;[[Watt|kW]])
| 1,500&nbsp;PS (1,479&nbsp;hp, 1,103&nbsp;kW)
| 750&nbsp;hp (559.27&nbsp;kW)
| 850&nbsp;hp (633.84&nbsp;kW)
|-
! style="text-align:right;" | Maximum speed
| 68&nbsp;km/h (42.25&nbsp;mph)
| 68&nbsp;km/h (42.25&nbsp;mph)
| 48.28&nbsp;km/h (30&nbsp;mph)
| 65&nbsp;km/h (40&nbsp;mph)
|}

== Notes ==
{{Reflist|30em}}

== References ==
{{Refbegin|30em}}
* {{cite book
 | last = Candil
 | first = Antonio
 | title = Carros de Combate: Evolución, Presente y Futuro
 | publisher = Isdefe
 |date=March 1999
 | location = Madrid, Spain
 | language = Spanish
 | url = http://www.isdefe.es/webisdefe.nsf/web/Carros+de+Combate.+Evoluci%C3%B3n,+presente+y+futuro/$file/CarrosBN.pdf
 |format=PDF| isbn = 84-89338-18-3 }}
* {{cite journal
 | last = Candil
 | first = Antonio
 | title = Leopard 2 daneses en Córdoba
 | journal = Fuerza Terrestre
 | volume = 3
 | issue = 36
 | pages = 7
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 | language = Spanish
 | id = 1575–1090
 }}
* {{cite journal
 | last = Candil
 | first = Antonio
 | title = Leopard 2E MBT Delivery Begins
 | journal = Military Technology
 | pages = 2
 | publisher = Mönch Editorial Group
 | date = 1 March 2004
 }}
* {{cite journal
 | last = Candil
 | first = Antonio J.
 | title = Spain's Armor Force Modernizes
 | journal = [[Armor magazine]]
 | pages = 3
 | publisher = US Army Armor Center
 | location = Fort Knox, Kentucky
 | date = 1 January 2006
 }}
* {{cite journal
 | last = Candil
 | first = Antonio
 | title = The Spanish Leopard 2E: A Magnificent Tool
 | journal = Military Technology
 | pages = 2
 | publisher = Mönch Editorial Group
 | date = 1 February 2007
 }}
* {{cite journal
 | last = Candil
 | first = Antonio
 | title = Un entorno industrial plagado de dificultades: La fabricación del Carro de Combate Leopard 2E en España (I)
 | journal = Fuerza Terrestre
 | volume = 3
 | issue = 49
 | pages = 8
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 | language = Spanish
 | id = 1575–1090
 }}
* {{cite journal
 | title = Carros Leopard 2A4 en Melilla
 | journal = Fuerza Terrestre
 | volume = 3
 | issue = 47
 | pages = 6
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 | language = Spanish
 | id = 1575–1090
 }}
* {{cite news
 | title = Cinco empresas compiten para cofabricar un carro de combate en España
 | work = El País
 | language = Spanish
 | date = 29 October 1984
 | url = http://www.elpais.com/articulo/espana/EMPRESA_NACIONAL_SANTA_BARBARA/empresas/compiten/cofabricar/carro/combate/Espana/elpepiesp/19841029elpepinac_7/Tes
 | accessdate = 2008-06-09 }}
* {{cite book
 | last = de Mazarrasa
 | first = Javier
 | title = Carro de Combate AMX-30E
 | publisher = Aldaba Militaria
 | pages = 104
 | language = Spanish
 | isbn = 84-86629-29-2
 | year = 1990 }}
* {{cite news
 | title = El tanque de los noventa
 | work = El País
 | language = Spanish
 | date = 2 November 1990
 | url = http://www.elpais.com/articulo/espana/ESTADOS_UNIDOS/ESPANA/ESPANA/ESTADOS_UNIDOS/tanque/noventa/elpepiesp/19901102elpepinac_3/Tes
 | accessdate = 2008-06-09 }}
* {{cite journal
 | title = Europe
 | journal = Military Technology
 | pages = 95
 | publisher = Mönch Editorial Group
 | date = 1 January 2006
 }}
* {{cite book
 | last = Green
 | first = Michael
 |author2=Greg Stewart
  | title = M1 Abrams at War
 | publisher = Zenith Press
 | year = 2005
 | location =St. Paul, MN
 | pages = 127
 | isbn = 0-7603-2153-1 }}
* {{cite news
 | last = Gonzáles
 | first = J.C.
 | title = Firmada adquisición de 308 tanques Leopard 2
 | work = El País
 | language = Spanish
 | date = 10 June 1995
 | url = http://www.elpais.com/articulo/espana/GARCIA_VARGAS/_JULIAN/ESPANA/ALEMANIA/ESPANA/REPUBLICA_FEDERAL_ALEMANA/Firmada/adquisicion/308/tanques/Leopard/elpepiesp/19950610elpepinac_3/Tes
 | accessdate = 2008-06-16 }}
* {{cite news
 | last = Gonzáles
 | first = Miguel
 | title = España negocia la adquisición de hasta 500 carros de combate que serán retirados de Centroeuropa
 | work = El País
 | language = Spanish
 | date = 2 November 1990
 | url = http://www.elpais.com/articulo/espana/ESTADOS_UNIDOS/ESPANA/ESPANA/ESTADOS_UNIDOS/Espana/negocia/adquisicion/500/carros/combate/seran/retirados/Centroeuropa/elpepiesp/19901102elpepinac_2/Tes
 | accessdate = 2008-06-07 }}
* {{cite news
 | last = Gonzáles
 | first = Miguel
 | title = Defensa cifra en 1.500 millones el coste de los 532 tanques estadounidenses que recibirá a partir de 1992
 | work = El País
 | language = Spanish
 | date = 31 December 1990
 | url = http://www.elpais.com/articulo/espana/ESTADOS_UNIDOS/ESPANA/ESTADOS_UNIDOS/MINISTERIO_DE_DEFENSA/PODER_EJECUTIVO/_GOBIERNO_PSOE_/1989-1993/Defensa/cifra/1500/millones/coste/532/tanques/estadounidenses/recibira/partir/1992/elpepiesp/19901231elpepinac_3/Tes
 | accessdate = 2008-06-09 }}
* {{cite book
 | last = Jerchel
 | first = Michael
 |author2=Uwe Schnellbacher
  | title = Leopard 2 Main Battle Tank 1979–1998
 | publisher = Osprey Publishing
 | year = 1998
 | location = Oxford, United Kingdom
 | pages = 48
 | isbn = 1-85532-691-4 }}
* {{cite book
 | last = Lathrop
 | first = Richard
 |author2=John McDonald
  | title = M60 Main Battle Tank 1960–91
 | publisher = Osprey Publishing
 | year = 2003
 | location = Oxford, United Kingdom
 | pages = 48
 | isbn = 1-84176-551-1 }}
* {{cite journal
 | title = Leopard 2A4: Lamina Fuerza Terrestre N° 49
 | journal = Fuerza Terrestre
 | volume = 3
 | issue = 49
 | pages = 4
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 | language = Spanish
 | id = 1575–1090
 }}
* {{cite book
 | last = Manrique
 | first = José María
 |author2=Lucas Molina
  | title = La Brunete: 1ª Parte
 | publisher = Quirón Ediciones
 | location = Valladolid, Spain
 | pages = 80
 | language = Spanish
 | isbn = 84-96016-27-7 }}
* {{cite journal
 | title = Reseña histórica del RCAC Alcántara n° 10
 | journal = Fuerza Terrestre
 | volume = 3
 | issue = 48
 | pages = 7
 | publisher = MC Ediciones
 | location = Barcelona, Spain
 | language = Spanish
 }}
* {{cite web
 | title =  Santa Bárbara Sistemas
 | publisher = Santa Bárbara Sistemas
 | url = http://www.gdsbs.com/web/frame.asp?page=http://www.gdsbs.com/web/frames.asp
 | accessdate = 2008-06-05 | archiveurl = https://web.archive.org/web/20080507182429/http://www.gdsbs.com/web/frame.asp?page=http://www.gdsbs.com/web/frames.asp| archivedate = May 7, 2008}}
* {{cite news
 | title = Spain Finalizes Buy of 108 Leopard 2A4 Tanks
 | work = Defense Industry Daily
 | date = 8 September 2005
 | url = http://www.defenseindustrydaily.com/spain-finalizes-buy-of-108-leopard-2a4-tanks-01155/
 | accessdate = 2008-06-16 }}
* {{cite web
 | title = Spain
 | work = United Nations Register of Conventional Arms
 | publisher = United Nations
 | url = http://disarmament.un.org/UN_REGISTER.NSF
 | accessdate = 2008-06-20 }}
* {{cite news
 | last = Yarnóz
 | first = Carlos
 | title = El Gobierno elegirá el mes próximo el carro de combate de los noventa
 | work = El País
 | language = Spanish
 | date = 31 March 1986
 | url = http://www.elpais.com/articulo/espana/ESPAnA/MINISTERIO_DE_DEFENSA/PODER_EJECUTIVO/_GOBIERNO_PSOE_/1982-1986/Gobierno/elegira/mes/proximo/carro/combate/noventa/elpepiesp/19860331elpepinac_13/Tes/
 | accessdate = 2008-06-09 }}
* {{cite news
 | last = Yarnóz
 | first = Carlos
 | title = España eliminará decenas de carros de combate al concluir la negociación sobre desarme en Europa
 | work = El País
 | language = Spanish
 | date = 2 January 1989
 | url = http://www.elpais.com/articulo/espana/MELILLA/CEUTA/MINISTERIO_DE_DEFENSA/PACTO_DE_VARSOVIA/ORGANIZACION_DEL_TRATADO_DEL_ATLANTICO_NORTE_/OTAN/PODER_EJECUTIVO/_GOBIERNO_PSOE_/1986-1989/Espana/eliminara/decenas/carros/combate/concluir/negociacion/desarme/Europa/elpepiesp/19890102elpepinac_1/Tes
 | accessdate = 2008-06-09 }}
* {{cite news
 | last = Yarnóz
 | first = Carlos
 | title = Serra descarta a EE UU y Reino Unido para cofabricar los nuevos carros
 | work = El País
 | language = Spanish
 | date = 30 December 1985
 | url = http://www.elpais.com/articulo/espana/ESPANA/REINO_UNIDO/ESTADOS_UNIDOS/ESPANA/ESTADOS_UNIDOS/REINO_UNIDO/Serra/descarta/EE/UU/Reino/Unido/cofabricar/nuevos/carros/elpepiesp/19851230elpepinac_8/Tes
 | accessdate = 2008-06-09 }}
{{refend}}

{{featured article}}

[[Category:Main battle tanks of Germany]]
[[Category:Main battle tanks of Spain]]
[[Category:Post–Cold War main battle tanks]]
[[Category:Military vehicles 2000–2009]]