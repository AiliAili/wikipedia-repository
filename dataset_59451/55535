{{italic title}}
'''''Anglia''''', subtitled ''Zeitschrift für Englische Philologie'', is a [[Germany|German]] quarterly [[academic journal]] on [[English language|English]] [[linguistics]], published by [[de Gruyter]]. It was established in 1878 by [[Moritz Trautmann]] and [[Richard Paul Wülker]], then based at the [[University of Leipzig]]. Between 1888 and 1892 [[Ewald Flügel]], also at Leipzig, acted as editor in conjunction with [[Gustav Schirmer]]. Although a German publication, it is the longest-established journal for the study of the English language in the world, having been published continuously since 1878.<ref name=web>[http://www.degruyter.de/journals/anglia/detailEn.cfm Journal website] {{webarchive |url=https://web.archive.org/web/20100817042824/http://www.degruyter.de/journals/anglia/detailEn.cfm |date=August 17, 2010 }}</ref> ''Anglia'' publishes articles on the English language and its history, and English literature from the [[Middle Ages]] through to modern times. It also covers [[American literature]] and English-language literature from around the world. Its scope extends to general and theoretical studies of [[comparative literature]] and culture.

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.degruyter.com/view/j/angl}}

{{DEFAULTSORT:Anglia (Journal)}}
[[Category:Linguistics journals]]
[[Category:Publications established in 1878]]
[[Category:Multilingual journals]]
[[Category:Quarterly journals]]
[[Category:Walter de Gruyter academic journals]]


{{ling-journal-stub}}