{{Use British English|date=January 2014}}
{{Use dmy dates|date=January 2014}}
{{Infobox military person
|name              =Arthur Louis Aaron
|image         =File:Arthur Aaron by Ibbeson1.jpg
|image_size    =150
|alt           =
|caption       =[[Graham Ibbeson]]'s statue of Aaron in Leeds
|birth_date    ={{birth date|1922|03|05|df=y}}
|death_date    ={{Death date and age|1943|08|13|1922|03|05|df=y}}
|birth_place   =[[Leeds]], United Kingdom
|death_place   =[[Bône]] Hospital, Algeria
|placeofburial =[[Annaba]], Algeria
|nickname      =
|allegiance    =United Kingdom
|branch        =[[Royal Air Force]]
|serviceyears  =1941–1943
|rank          =[[Flight Sergeant]]
|servicenumber = 1458181
|unit          =[[No. 218 (Gold Coast) Squadron RAF]]
|commands      =
|battles       =[[World War II]]
|battles_label =
|awards        =[[Victoria Cross]]<br/>[[Distinguished Flying Medal]]
|memorials     =
|spouse        = 
|relations     =
|laterwork     =
}}
'''Arthur Louis Aaron''' [[Victoria Cross|VC]], [[Distinguished Flying Medal|DFM]] (5 March 1922 – 13 August 1943) was a [[Royal Air Force]] pilot and an English recipient of the [[Victoria Cross]], the highest award for gallantry in the face of the enemy that can be awarded to British and Commonwealth forces. He had flown 90 operational flying hours and 19 sorties, and had also been awarded posthumously the [[Distinguished Flying Medal]].<ref>[http://www.london-gazette.co.uk/issues/36215/supplements/4620 Supplement to the London Gazette, 19 October 1943]</ref>

==Early life and wartime service==
Aaron was a native of [[Leeds]], Yorkshire, and was educated at [[Roundhay School]] and Leeds School of Architecture.  When the Second World War started in 1939 Aaron joined the [[Air Training Corps]] squadron at Leeds University. The following year he volunteered to train as aircrew in the Royal Air Force.  He trained as a pilot in the United States at No. 1 British Flying Training School (BFTS) at [[Terrell, Texas]]. Aaron completed his pilot training on 15 September 1941 and returned to England to train at an Operation Conversion Unit before he joined [[No. 218 Squadron RAF|No. 218 "Gold Coast" Squadron]], flying [[Short Stirling]] heavy bombers from [[RAF Downham Market]]. 

His first operational sortie was a mining sortie in the Bay of Biscay but was soon flying missions over Germany. On one sortie his Stirling was badly damaged but he completed his bombing run and returned to England, his actions were rewarded with a [[Distinguished Flying Medal]].

==VC action==
Aaron was 21 years old, flying Stirling serial number EF452 on his 20th sortie. Nearing the target, his bomber was struck by machine gun fire. The bomber's Canadian navigator, Cornelius A. Brennan, was killed and other members of the crew were wounded.

The official citation for his VC reads:

{{quote|''Air Ministry, 5th November, 1943.''
The [[George VI|King]] has been graciously pleased to confer the Victoria Cross on the undermentioned airman in recognition of most conspicuous bravery:—

458181 Acting [[Flight Sergeant]] Arthur Louis Aaron, D.F.M., [[Royal Air Force Volunteer Reserve]], No. 218 Squadron (deceased).

On the night of 12 August 1943, Flight Sergeant Aaron was captain and pilot of a Stirling aircraft detailed to attack [[Turin]]. When approaching to attack, the bomber received devastating bursts of fire from an enemy fighter. Three engines were hit, the windscreen shattered, the front and rear [[Gun turret#Aircraft|turrets]] put out of action and the [[Elevator (aircraft)|elevator control]] damaged, causing the aircraft to become unstable and difficult to control. The navigator was killed and other members of the crew were wounded.

A bullet struck Flight Sergeant Aaron in the face, breaking his jaw and tearing away part of his face. He was also wounded in the lung and his right arm was rendered useless. As he fell forward over the control column, the aircraft dived several thousand feet. Control was regained by the flight engineer at 3,000 feet. Unable to speak, Flight Sergeant Aaron urged the bomb aimer by signs to take over the controls. Course was then set southwards in an endeavour to fly the crippled bomber, with one engine out of action, to [[Sicily]] or [[North Africa]].

Flight Sergeant Aaron was assisted to the rear of the aircraft and treated with [[morphia]]. After resting for some time he rallied and, mindful of his responsibility as captain of aircraft, insisted on returning to the pilot's cockpit, where he was lifted into his seat and had his feet placed on the rudder bar. Twice he made determined attempts to take control and hold the aircraft to its course but his weakness was evident and with difficulty he was persuaded to desist. Though in great pain and suffering from exhaustion, he continued to help by writing directions with his left hand.

Five hours after leaving the target the petrol began to run low, but soon afterwards the flare path at [[Rabah Bitat Airport|Bone airfield]] was sighted. Flight Sergeant Aaron summoned his failing strength to direct the bomb aimer in the hazardous task of landing the damaged aircraft in the darkness with undercarriage retracted. Four attempts were made under his direction; at the fifth Flight Sergeant Aaron was so near to collapsing that he had to be restrained by the crew and the landing was completed by the bomb aimer.

Nine hours after landing, Flight Sergeant Aaron died from exhaustion. Had he been content, when grievously wounded, to lie still and conserve his failing strength, he would probably have recovered, but he saw it as his duty to exert himself to the utmost, if necessary with his last breath, to ensure that his aircraft and crew did not fall into enemy hands. In appalling conditions he showed the greatest qualities of courage, determination and leadership and, though wounded and dying, he set an example of devotion to duty which has seldom been equalled and never surpassed.<ref>{{London Gazette |issue=36235 |supp=yes |startpage=4859 |date=5 November 1943}}</ref>}}

The gunfire that hit Flight Sergeant Aaron's aircraft was thought to have been from an enemy night fighter, but may have been [[friendly fire]] from another Stirling.<ref>''The Stirling Bomber'', Bowyer, 1980, page 129</ref>

==Memorials==
He was an 'old boy' of [[Roundhay School]], Leeds (headmaster at the time was B.A.Farrow). There is a plaque in the main hall of the school to his memory incorporating the deed that merited the VC. 

Genealogical research proved many years ago that Aaron's father was a Russian Jewish immigrant even though the family denied it after Aaron was killed, and he boasted of this to members of his air training colleagues in the mess in Texas on many occasions.<ref>"We Will Remember Them" by Henry Morris and Martin Sugarman, published by Valentine Mitchell, 2011</ref> He is commemorated at the AJEX [[Jewish Military Museum]] in Hendon, London, one of three Jewish VC's of the Second World War (the others being [[Thomas William Gould|Tommy Gould]], Royal Navy, and [[John Patrick Kenneally|John Keneally]], Irish Guards). Aaron also belonged at school or University to 319 ATC (Jewish) Squadron in Broughton, Salford, where his photograph still hangs; this fact was researched by Col Martin Newman DL from the HQ Air Cadets archives. Aaron's Victoria Cross is displayed at the [[Leeds City Museum]].

To mark the new [[Millennium]], the Leeds Civic Trust organised a public vote to choose a statue to mark the occasion, and to publicise the city's past heroes and heroines. Candidates included [[Benjamin Latrobe]] and [[Henry Moore|Sir Henry Moore]]. Arthur Aaron won the vote, with [[Don Revie]] beating [[Tetley's (beer)|Joshua Tetley]] and [[Frankie Vaughan]] as runner-up. Located on a [[roundabout]] on the northern edge of the city centre, close to the [[West Yorkshire Playhouse]], the statue of Aaron was unveiled on 24 March 2001 by Malcolm Mitchem, the last survivor of the aircraft. The five-metre [[bronze]] sculpture by [[Graham Ibbeson]] takes the form of Aaron standing next to a tree, up which are climbing three children progressively representing the passage of time between 1950 and 2000, with the last a girl releasing a [[Peace symbol#The dove and olive branch|dove of peace]], all representing the freedom his sacrifice helped ensure.<ref>[http://www.leedscivictrust.org.uk/aaron.htm Leeds Civic Trust]</ref> There was controversy about the siting of the statue, and it was proposed to transfer it to Millennium Square outside Leeds City Museum.<ref>[http://www.yorkshireeveningpost.co.uk/lifestyle/columnists/brave-arthur-deserves-his-pride-of-place-1-2119786 Brave Arthur deserves his pride of place], ''Yorkshire Evening Post'', 1 November 2007</ref> However, as of 2012 the statue remains on the roundabout.<ref>[http://maps.google.co.uk/maps?hl=en&aq=0&sll=53.79846,-1.536064&ie=UTF8&ll=53.798479,-1.536047&spn=0.000324,0.000431&t=h&z=21&vpsrc=6 Google Maps satellite image], retrieved 2012-11-19</ref>

==References==
{{reflist|30em}}

==External links==
*[http://www.victoriacross.org.uk/ggalgeri.htm Burial location of Arthur Aaron] ''Algeria''
*[https://web.archive.org/web/20041212044932/http://www.victoriacross.org.uk:80/ccleeds.htm Location of Arthur Aaron's Victoria Cross] ''Leeds City Museum''
*[http://www.yorkshiredailyphoto.com/2009/04/arthur-aaron-vc-dfm-statue-leeds-city.html Photo of Arthur Aaron's statue in Leeds]

{{DEFAULTSORT:Aaron, Arthur Louis}}
[[Category:1922 births]]
[[Category:1943 deaths]]
[[Category:People from Leeds]]
[[Category:British World War II recipients of the Victoria Cross]]
[[Category:Recipients of the Distinguished Flying Medal]]
[[Category:Royal Air Force airmen]]
[[Category:British military personnel killed in World War II]]
[[Category:Royal Air Force Volunteer Reserve personnel of World War II]]
[[Category:Royal Air Force recipients of the Victoria Cross]]
[[Category:British World War II pilots]]
[[Category:Military personnel killed by friendly fire]]
[[Category:People educated at Roundhay School]]
[[Category:English Jews]]