__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Moni
 | image=Monnett moni.JPG
 | caption=Monnett Moni on display in the National Air and Space Museum
}}{{Infobox Aircraft Type
 | type=Sport aircraft
 | national origin=United States
 | manufacturer=Monnett Experimental Aircraft Inc for [[Homebuilt aircraft|homebuilding]]
 | designer=[[John Monnett]]
 | first flight=July 24, 1981
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=380 kits sold between 1982 and 1986<ref name="NASM fact sheet">{{cite web|url=http://collections.nasm.si.edu/code/emuseum.asp?profile=objects&newstyle=single&quicksearch=A19920066000 |title=Monnett Moni |work=Smithsonian National Air and Space Museum website |publisher=Smithsonian Institution |accessdate=2008-10-08}}</ref>
 | developed from= 
 | variants with their own articles=[[Electric Aircraft Corporation ElectraFlyer-C]]
}}
|}

The '''Monnett Moni''' is a sport aircraft developed in the United States in the early 1980s and marketed for [[homebuilt aircraft|homebuilding]].

Designed by [[John Monnett]], who coined the term "Air Recreation Vehicle" to describe it,<ref name="NASM fact sheet" /> it was a single-seat motorglider with a low, cantilever wing and a V-tail. Construction was of metal throughout, and it was intended to be easy and inexpensive to build and fly.  Like many sailplanes, the main undercarriage was a single monowheel, which in this case was mounted in a streamlined fairing beneath the fuselage and was not retractable, with a steerable tailwheel behind it. Builders were also given the option of constructing their example with fixed tricycle undercarriage.<ref name="Jane's">''Jane's All the World's Aircraft 1984-85'', 756</ref> Power was provided by a small two-cylinder, horizontally opposed, air-cooled engine.

[[File:Moni-SUH.jpg|thumb|Monnett Moni at Udvar Hazy Center]]
Examples of the Moni are on display at the [[Steven F. Udvar-Hazy Center]] of the [[National Air and Space Museum]],<ref>{{cite web|url=http://siris-archives.si.edu/ipac20/ipac.jsp?uri=full=3100001~!228134!0|title=Monnett Experimental Aircraft, Inc. (MONI) Collection, 1981|work=Smithsonian Institution Research Information System|publisher=Smithsonian Institution|accessdate=2008-10-07}}</ref> and the [[EAA AirVenture Museum]].<ref name="EAA">{{cite web|url=http://www.airventuremuseum.org/collection/aircraft/Monnett%20Moni.asp |title=Monnet Moni – N107MX |work=AirVenture Museum website |publisher=EAA |accessdate=2008-10-08}}</ref>
<!-- ==Development== -->
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (with tricycle gear)==
{{aerospecs
|ref=<ref name="Jane's" />
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=One pilot
|capacity=
|length m=4.46
|length ft=14
|length in=8
|span m=8.38
|span ft=27
|span in=6
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=1.07
|height ft=3
|height in=6
|wing area sqm=7.0
|wing area sqft=75
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=118
|empty weight lb=260 
|gross weight kg=227
|gross weight lb=500 
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[IAME KFM 107]]
|eng1 kw=<!-- prop engines -->22
|eng1 hp=<!-- prop engines -->30
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=193
|max speed mph=120 
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->177
|cruise speed mph=110
|range km=515
|range miles=320 
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=3,810
|ceiling ft=12,500
|glide ratio=<!-- sailplanes -->20
|climb rate ms=2.5
|climb rate ftmin=500
|sink rate ms=<!-- sailplanes -->0.85
|sink rate ftmin=<!-- sailplanes -->167
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Monnett Moni}}
* {{cite book |title=Jane's All the World's Aircraft 1984-85 |publisher=Jane's Publishing |location=London }}

{{Monnett aircraft}}

[[Category:Monnett aircraft]]
[[Category:Homebuilt aircraft]]
[[Category:United States sport aircraft 1980–1989]]
[[Category:V-tail aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]