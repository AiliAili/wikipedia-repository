{{Infobox journal
| title = Chemical Communications
| formernames = Chemical Communications (London): Journal of the Chemical Society D Chemical Communications; Journal of the Chemical Society, Chemical Communications
| cover = [[File:Chemical communications cover.jpg]]
| discipline = [[Chemistry]]
| abbreviation = Chem. Commun.
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| impact = 6.567
| impact-year = 2015
| history = 1989-present
| frequency = 100/year
| website = http://pubs.rsc.org/en/journals/journalissues/cc
| ISSN = 1359-7345
| eISSN = 1364-548X
| CODEN = CHCOFS
| OCLC = 869930135
| LCCN = 96660034
}}
'''''Chemical Communications''''', also known as '''''ChemComm''''',<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-01-14 }}{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> is a [[peer-reviewed]] [[scientific journal]] published by the [[Royal Society of Chemistry]].<ref name=About /> It covers all aspects of [[chemistry]]. In January 2012, the journal moved to publishing 100 issues per year.<ref name=About>{{cite web |title=About Chemical Communications |url=http://www.rsc.org/publishing/journals/cc/about.asp |publisher=Royal Society of Chemistry}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts]] <ref name="CASSI"/>
* [[Science Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-01-14}}</ref>
* [[Current Contents]]/Physical, Chemical & Earth Sciences<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=Scopus coverage lists |accessdate=2015-01-14}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9610838 |title=Chemical Communications |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-01-14}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 6.567.<ref name=WoS>{{cite book |year=2016 |chapter=Chemical Communications |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== See also ==
* ''[[New Journal of Chemistry]]''
* ''[[Chemical Society Reviews]]''
* ''[[Chemical Science (journal)|Chemical Science]]''

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.rsc.org/chemcomm}}

{{Royal Society of Chemistry|state=collapsed}}

[[Category:Chemistry journals]]
[[Category:Journals more frequent than weekly]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1965]]