[[Image:Feliksas Vaitkus. Transatlantic flight Lituanica II. 1935.jpg|thumb|F. Vaitkus near ''Lituanica II'' in 1935]]
[[Image:Feliksas Vaitkus and Lithuanica II plane 1935 1 cropped.jpg|thumb|F. Vaitkus near ''Lituanica II'' in 1935]]
[[File:Felikso Vaitkaus sutikimas Kaune 1935 10 02.JPG|thumb|American Lithuanian transatlantic pilot Feliksas Vaitkus (Felix Waitkus - first from the left) arrived in Kaunas. 4th from the left - famous Lithuanian sportsman {{Interlanguage link multi|Vladas Dzindziliauskas|lt}}, Kaunas, October 2, 1935.]]
[[Image:Lithuanian Air Post stamp Mi-405 (1936).jpg|thumb|Lithuanian postage stamp dedicated to ''Lituanica II'' flight (1936)]]

'''Feliksas Vaitkus (Felix Waitkus)''' (1907–1956) was an [[United States|American]] born [[Lithuanian people|Lithuanan]] pilot and the sixth pilot to fly solo across the [[Atlantic]].

== Biography==
His parents came from [[Lithuania]] in 1904, settling in the old "Lithuanian Downtown" in Chicago's [[Bridgeport, Chicago|Bridgeport]] neighborhood where Vaitkus was born three years later. He enlisted in the army in 1928, and after graduating from advanced pilot’s training school, was commissioned a second lieutenant in the [[United States Army Air Corps|Air Corps]]. In 1931, he was placed in the reserves with the rank of first lieutenant and returned to civilian life to work with his father-in-law who operated a flying school in [[Kohler, Wisconsin]].

== Lituanica II==

A few months after the ''[[Lituanica]]'' tragedy, some prominent members of the [[Chicago Lithuanian community]] discussed the possibility of financing another transatlantic flight. This idea was greeted with much enthusiasm, and enough funds were raised during this difficult period, the [[Great Depression]]. A much faster and more modern aircraft (compared to the original ''Lituanica'') was purchased from the Lockheed Aircraft Corp., called the [[Lockheed Vega]], the same model used by [[Wiley Post]] in his round-the-world flight, and by [[Amelia Earhart]], the first woman to fly solo across the Atlantic.

The aircraft was christened ''Lituanica II'' on Sunday, April 22, 1934. When the pilot originally chosen for the flight unexpectedly resigned in the spring, the Lithuanian organizers turned to Feliksas Vaitkus, and he accepted the challenge to fly to Lithuania.

The original flight date was postponed to 1935 because modifications were needed to strengthen the aircraft's structure due to the installation of extra fuel tanks and a more powerful engine. New equipment was added, which [[Lituanica|Darius and Girėnas]] did not have, such as a variable-pitch propeller to improve engine performance and a radio compass. ''Lituanica II'' was test-flown extensively to ensure its success.

== The Flight==

Vaitkus arrived at [[Floyd Bennett Field]] in New York in May 1935. That year, prior to his arrival, no pilots had been able to fly over the Atlantic due to poor weather conditions, and so he had to wait until the chief meteorologist gave him a favorable weather report. It was not until four months later, on the evening of September 20, he was informed that the weather along his route had improved, where conditions were average, with no major changes expected in the next 24 hours. Encouraged with this news, Vaitkus decided to fly.

The next day, September 21, at 6:45 in the morning, ''Lituanica II'' was headed for Europe. Unfortunately, Vaitkus had to fight the worst possible weather conditions, flying through rain, fog, and icing, so that most of his navigation had to be by his new radio compass. More than once he noticed ice forming on the wings and had to descend to a lower altitude until it thawed off. Vaitkus was helped considerably by hourly broadcasts from an Irish radio station. He learned that [[Dublin]] was fogged in, as well as all areas heading east as far as the [[Baltic Sea]]. He knew that he could not make it to [[Kaunas]] due to his low fuel supply, and being exhausted after a 23-hour struggle fighting the elements, he felt it was best to come down somewhere in Ireland.

He spotted an open field in Cloongowla at [[Ballinrobe]], [[County Mayo]] and landed there, with the aircraft suffering extensive damage.<ref>Karolis Vaitkevičius. ''Airijoje - "Lituanicos II" skrydžio pėdsakai'', Lietuvos žinios No. 116 (11 453), 24 May 2006</ref> He was lucky to be alive and uninjured. ''Lituanica II'' was crated for shipment to Lithuania, where it would be restored. By ship and by train he made his way to Kaunas where he was given a hero’s welcome.

Feliksas Vaitkus was the only pilot to fly across the North Atlantic in 1935, and even though he landed in Ireland and not in Kaunas, he was entered in aviation’s history books for being the sixth pilot to fly solo across the Atlantic.

He was recalled to active duty in the Army Air Corps in 1940, serving as the chief test pilot at the Boeing Aircraft Co. in Seattle, testing hundreds of [[B-17 Flying Fortress|B-17]] and [[B-29]] bombers. He was recalled again to serve in the U.S. Air Force during the [[Korean War]].

He died of a heart attack while stationed at [[Wiesbaden]], [[Germany]], on July 25, 1956, at the age of 49. His body was shipped home and interred in his wife’s family plot in the Kohler Cemetery at [[Sheboygan, Wisconsin]]. At the time of his death, he held the rank of lieutenant colonel.

==References==
{{reflist}}

== External links==
{{commons category}}
*[http://lithuanian-american.org/bridges/2000/iss6/baranauskas.html "The Second Transatlantic Flight. Felix Waitkus: Forgotten Hero" by Edward W. Baranauskas]

{{DEFAULTSORT:Vaitkus, Felix}}
[[Category:1907 births]]
[[Category:1956 deaths]]
[[Category:American people of Lithuanian descent]]
[[Category:Aviation history of the United States]]
[[Category:Lithuanian aviators]]
[[Category:United States Army colonels]]