{{this|the George R. R. Martin novel|the video game|Clash of Kings}}
{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = A Clash of Kings
| image         = AClashOfKings.jpg
| caption       = US hardcover edition
| author        = [[George R. R. Martin]]
| audio_read_by = [[Roy Dotrice]]
| cover_artist  = [[Steve Youll]]
| country       = United States
| language      = English
| series        = ''[[A Song of Ice and Fire]]''
| genre         = [[Fantasy]]
| published     = 1998 ([[Voyager Books]]/UK) <br> 1999 ([[Bantam Spectra]]/US) <!-- November 1998 (UK) & March 1999 (US) -->
| isbn          = 0-00-224585-X
| isbn_note     = (UK Hardback) <br> ISBN 0-553-10803-4 (US Hardback)
| pages         = 768 <!-- US Hardback -->
| award         = [[Locus Award for Best Fantasy Novel]] (1999)
| dewey         = 813/.54
| congress      = [http://lccn.loc.gov/98037954 PS3563.A7239 C58 1999]
| oclc          = 59667381
| preceded_by   = [[A Game of Thrones]]
| followed_by   = [[A Storm of Swords]]
}}
'''''A Clash of Kings''''' is the second novel in ''[[A Song of Ice and Fire]]'', an [[epic fantasy]] series by American author [[George R. R. Martin]] expected to consist of seven volumes. It was first published on 16 November 1998 in the United Kingdom, although the first United States edition did not follow until March 1999. Like its predecessor, ''[[A Game of Thrones]]'', it won the [[Locus Award]] (in 1999) for Best Novel and was nominated for the [[Nebula Award]] (also in 1999) for best novel. In May 2005 [[Meisha Merlin]] released a limited edition of the novel, fully illustrated by [[John Howe (illustrator)|John Howe]].

The novel has been adapted for television by [[HBO]] as [[Game of Thrones (season 2)|the second season]] of the TV series ''[[Game of Thrones]]''.

''A Clash of Kings'' is also the name of the first expansion to the [[A Game of Thrones (board game)|''Game of Thrones'' board game]].

== Plot summary ==
''A Clash of Kings'' depicts the Seven Kingdoms of [[Westeros]] in [[civil war]], while the [[Night's Watch]] mounts a reconnaissance to investigate the mysterious people known as [[wildlings]]. Meanwhile, [[Daenerys Targaryen]] continues her plan to reconquer the Seven Kingdoms.

===In the Seven Kingdoms===
With King [[Robert Baratheon]] dead, his son [[Joffrey Baratheon]] and brothers [[Renly Baratheon|Renly]] and [[Stannis Baratheon|Stannis]] all claim the throne of Westeros. [[Robb Stark]] is declared 'King in the [[The North (A Song of Ice and Fire)|North]]' while [[Balon Greyjoy]] declares himself king of the [[Iron Islands]] and attacks the western coast of [[The North (A Song of Ice and Fire)|the North]]. Robb's younger brother [[Bran Stark]] finds new friends in [[Jojen Reed|Jojen]] and [[Meera Reed]].

Stannis Baratheon, as Robert's eldest brother, declares himself King of Westeros, encouraged by [[Melisandre]] of Asshai, a priestess who believes Stannis the [[reincarnation]] of Azor Ahai, a [[Messiah|messianic]] figure of her faith. The war is dubbed the [[War of the Five Kings]]. [[Catelyn Stark]], under orders from her son King Robb Stark, meets Renly and Stannis to discuss alliance against the Lannisters; but despite Stannis offering to make Renly his heir over his daughter Shireen the ensuing parley ends in acrimony and Renly resolves to destroy his brother with his larger army. Renly has married the daughter of the greedy and arrogant Lord Mace Tyrell, who is therefore supporting his attempt to usurp the throne. However, without warning a mysterious shadow comes to life in Renly's own tent and kills him. Shocked, Catelyn and the only other witness, the warrior-maid Brienne of Tarth, flee the scene. Having lost Renly, the storm lords have no choice but to declare for Stannis. Storm's End itself only falls, however, when Stannis's loyal Ser Davos, a former smuggler, sneaks Melisandre below the castle in a small boat. To his horror, Melisandre gives birth to a shadow which enters Storm's End and slays its stubborn castellan Ser Cortnay Penrose. 

[[Tyrion Lannister]] arrives at [[King's Landing (A Song of Ice and Fire)|King's Landing]] as acting Hand of the King, the closest adviser to the monarch Joffrey. Whilst intriguing against Joffrey's mother, the Queen Regent [[Cersei Lannister|Cersei]], Tyrion improves the defenses of the city. Learning of Renly's death, Tyrion resolves on two courses of action. Knowing that the Tyrells will not be happy following Stannis, he decides to make them a better offer. He also resolves to bring the Martells of Dorne into the war on his side. He sends Littlefinger to treat with the Tyrells and sends other messengers to Dorne.The negotiations are solidified by marriage arrangements. Mace Tyrell agrees to wed his daughter Margaery to King Joffrey I, whilst Prince Doran Martell agrees to marry his youngest son Trystane to Joffrey's sister Myrcella. 

To conquer the North and to impress his father Balon, [[Theon Greyjoy]] captures Winterfell, taking Bran and [[Rickon Stark|Rickon]] captive; but Bran and Rickon disappear in the night. Rather than admit his failure, Theon murders two anonymous peasant boys similar in size to Bran and Rickon and mutilates their faces to enhance the resemblance. Believing this ruse, Stark supporters besiege the castle, joined by a force from House Bolton. Theon having conspired with Bolton's bastard son, [[Ramsay Snow]], the Bolton soldiers turn on the besiegers and Theon opens the gates to the victorious Boltons, whereupon they destroy Winterfell, slaughter its inhabitants and take Theon prisoner. Osha, a captured wildling turned castle servant, takes Rickon to safety while Bran, accompanied by Meera, Jojen, and his simpleton manservant Hodor, travels north. Robb Stark wins several victories against the Lannisters, and [[Tywin Lannister]] advances against him, but receiving news that King's Landing is threatened, withdraws.

Arya Stark, posing as a boy named Arry, is taken north by Yoren as a new recruit for the Night's Watch. However, they are attacked by Lannister freeriders led by Ser Amory Lorch and Yoren is killed at a town on the shores of Gods Eye.  The survivors are taken to Harrenhal and put to work as servants in the undermanned castle. For saving his life during the attack, a man named Jaqen H'ghar promises to repay Arya by killing three men of her choice. For her three choices, Arya selects Chiswyck, a soldier in Gregor Clegane's forces, Weese, her servant overseer, and Jaqen H'ghar himself. The first two murders he carries out without question; in lieu of the third choice, Arya leverages H'ghar to help her slay the Lannister soldiers guarding Robett Glover, who had been taken prisoner by the Brave Companions. After Glover is freed, the Brave Companions turn their cloaks and join with the North. H'ghar gives Arya a mysterious iron coin and tells her to find him in Braavos if she should ever desire to learn his secrets. He then adopts a new identity and leaves. Control of Harrenhal is ceded to Roose Bolton, whom Arya serves as cupbearer, but soon escapes.

In the Battle of Blackwater Bay, Stannis Baratheon's army reaches King's Landing and launches assaults by both land and sea. The Baratheon forces vastly outnumber the Lannister defenders but under Tyrion's command, the Lannister forces use "wildfire" (similar to [[Greek fire]]) to ignite the bay, and raise [[Boom (navigational barrier)|a massive chain]] across its mouth to prevent Stannis's fleet from retreating. Stannis's attack fails when Tywin Lannister leads his army (and the remaining forces under Mace Tyrell) to King's Landing. During the defense of the castle, Tyrion is attacked by one of Joffrey's Kingsguard, and rescued by his squire, Podrick Payne, killing the Kingsguard. Stannis's fate is left uncertain.

===On the Wall===
A scouting party from the Night's Watch learn that the wildlings are uniting under 'King-beyond-the-Wall' [[Mance Rayder]]. The Watch then continue to an ancient hill-top fortress known as the Fist of the First Men, whence [[Jeor Mormont]] sends [[Jon Snow (character)|Jon Snow]] and Qhorin Halfhand with others to the Skirling Pass, where they are hunted by wildling warriors. Facing certain defeat, Halfhand commands Snow to infiltrate the wildlings and learn their plans. They are captured by wildlings who demand Jon fight Qhorin to join them. Jon kills Qhorin with the aid of his [[direwolf]], Ghost and Jon learns that Mance Rayder is advancing on the Wall with thirty-thousand wildlings, giants, and [[mammoth]]s.

===In the East===
Daenerys Targaryen travels east, accompanied by the knight [[Jorah Mormont]], her remaining followers, and three newly hatched [[dragon]]s. Scouts find a safe route to the city of [[Qarth]], where her dragons make Daenerys notorious. Xaro Xhoan Daxos, the leader of the Thirteen, a prominent group of traders in Qarth, initially befriends the outsiders; but Daenerys cannot secure aid in claiming the throne of Westeros, because she refuses to give away any of her dragons. As a last resort, Daenerys seeks counsel from the [[warlock]]s of Qarth, who show Daenerys many confusing visions and threaten her life, whereupon one of Daenerys's dragons, Drogon, burns down the warlocks' "House of the Undying".  An attempt to assassinate Daenerys is thwarted by a fat warrior named Strong Belwas and his squire Arstan Whitebeard: agents of Daenerys's ally [[Illyrio Mopatis]], who have come to escort her back to [[Pentos (A Song of Ice and Fire)|Pentos]].

== Characters ==
The tale is told through the eyes of 9 recurring POV characters plus one prologue POV character:
* Prologue: Maester Cressen, maester at Dragonstone.
* [[Tyrion Lannister]], youngest son of Lord [[Tywin Lannister]], a dwarf and a brother to Queen Cersei, and the acting Hand of the King
* Lady [[Catelyn Stark]], of House Tully, widow of Eddard Stark, Lord of Winterfell
* Ser [[Davos Seaworth]], a smuggler turned knight in the service of King Stannis Baratheon, often called the Onion Knight
* [[Sansa Stark]], eldest daughter of Eddard Stark and Catelyn Stark, held captive by Queen Cersei at King's Landing
* [[Arya Stark]], youngest daughter of Eddard Stark and Catelyn Stark, missing and presumed dead
* [[Bran Stark]], second son of Eddard Stark and Catelyn Stark and heir to Winterfell and the King in the North
* [[Jon Snow (character)|Jon Snow]], bastard son of Eddard Stark, and a man of the Night's Watch
* [[Theon Greyjoy]], heir to the Seastone Chair and former ward of Lord Eddard Stark
* Queen [[Daenerys Targaryen]], the Unburnt and Mother of Dragons, of the [[House Targaryen|Targaryen]] dynasty

==Editions==
;Foreign language editions
{{div col|3}}
* Bulgarian: Бард :"Сблъсък на Крале"
* Catalan: Alfaguara :"Xoc de reis" ("Clash of kings")
* Croatian: Algoritam: "Sraz kraljeva"
* Chinese: "列王的纷争", 重庆出版社(2006) ("Conflict of Kings").
* Czech: Talpress: "Střet králů" ("Clash of Kings")
* Danish: Gyldendal :"Kongernes Kamp" ("The Battle of Kings")
* Dutch: One volume, Luithing Fantasy (1999): hardcover : ''De Strijd der Koningen'' ("The Clash of Kings")
* Estonian: Two volumes, hardcover : Varrak (2008, 2009), "Kuningate heitlus I & II" ("A Clash of Kings")
* Finnish: Kirjava: "Kuninkaiden koitos"
* French: Three volumes (Hardcover: Pygmalion (2000); paperback: J'ai Lu (2002)): "La bataille des rois", "L'ombre maléfique", "L'invincible forteresse" ("The Battle of Kings", "The Evil Shadow", "The Invincible Fortress").
* German: Single volume, Fantasy Productions (2004): "Königsfehde" ("King's Feud"). Two volumes, [[Blanvalet]] (2000): "Der Thron der Sieben Königreiche", "Die Saat des goldenen Löwen" ("The Throne of the Seven Kingdoms", "The Seed of the Golden Lion").
* Georgian: Paperback, Arete (2014): "მეფეთა ჯახი" I/II ("Clash of Kings" I/II)
* Greek: Anubis: "Σύγκρουση Βασιλέων" ("Clash of Kings")
* Hebrew: "I/II עימות המלכים" ("Clash of Kings")
* Hungarian: Alexandra Könyvkiadó : "Királyok csatája" ("Battle of Kings")
* Icelandic: UGL: "Konungar kljást" ("Kings Clash")
* Italian: Two volumes, Arnoldo Mondadori Editore (2001, 2002): "Il regno dei lupi", "La regina dei draghi" ("The Kingdom of Wolves", "The Queen of Dragons").
* Japanese: Two volumes, hardcover : Hayakawa (2004), paperback : Hayakawa (2007): "王狼たちの戦旗" ("Banner of the Wolf Kings")
* Korean: Eun Haeng Namu Publishing Co. :"왕들의 전쟁" ("War of Kings")
* Latvian: Whitebook: "Karaļu cīņa" ("War of Kings")
* Lithuanian: Alma Littera "Karalių kova" ("A Battle of Kings")
* Norwegian: Two volumes (2012) 'Bok II Del I: Kongenes kamp' (Book II Part I: The Battle of Kings) and 'Bok II Del II: Dragenes dronning' (Book II Part II: The Queen of Dragons)
* Polish: Zysk i s-ka: "Starcie królów"
* Brazilian Portuguese: Leya: "A Fúria dos Reis" ("Wrath of the Kings")
* European Portuguese: Two Volumes, Saída de Emergência : "A Fúria dos Reis", "O Despertar da Magia"
* Romanian: Nemira: "Încleștarea regilor"
* Russian: Single volume, AST (2004, 2005, 2006): "Битва королей" ("The Battle of Kings"). Two volumes, AST (2000): "Битва королей. Книга 1", "Битва королей. Книга 2" ("The Battle of Kings: Book 1", "The Battle of Kings: Book 2).
* Serbian: Лагуна : "Судар краљева"
* Slovenian: "Spopad kraljev" ("Clash of Kings")
* Spanish: Gigamesh (2003): "Choque de reyes" ("Clash of Kings").
* Swedish: Forum bokförlag :"Kungarnas krig" ("War of the Kings")
* Turkish: Two volumes, Epsilon Yayınevi: "Buz ve Ateşin Şarkısı II: Kralların Çarpışması - Kısım I & Kralların Çarpışması - Kısım II" ("A Clash of Kings")
* Ukrainian: One volume, KM Publishing (2014): "Битва Королів" ("A Clash of Kings")
* Vietnamese: Two volumes: "Trò Chơi Vương Quyền 2A: Hậu Duệ Của Sư Tử Vàng", "Trò Chơi Vương Quyền 2B: Bảy Phụ Quốc". ("Game of Thrones 2A: Descendants of the Golden Lion", "Game of Thrones 2B: Seven Kingdoms") 
{{div col end}}

==Television adaptation==
''A Clash of Kings'' has been adapted for television by HBO as [[Game of Thrones (season 2)|the second season]] of its successful adaptation of ''A Song of Ice and Fire''.<ref name="WWE-1999">{{cite web
| url = http://www.worldswithoutend.com/books_year_index.asp?year=1999
| title = 1999 Award Winners & Nominees
| work = Worlds Without End
| accessdate=2009-07-25
}}</ref> Filming began July 2011, and the first episode of season 2 of ''Game of Thrones'' aired on April 1, 2012.<ref name="Crider">{{cite web|last=Crider|first=Michael|title='Game Of Thrones' Season 2 Starts Filming In July; Producers Talk Cast & Story|url=http://screenrant.com/game-thrones-season-2-details-production-schedule-mcrid-120008/|accessdate=July 15, 2011| archiveurl= https://web.archive.org/web/20110621164921/http://screenrant.com/game-thrones-season-2-details-production-schedule-mcrid-120008/| archivedate= 21 June 2011 <!--DASHBot-->| deadurl= no}}</ref>

==Reception==
As with its predecessor, ''A Clash of Kings'' was positively received by critics. Dorman Shindler of ''[[The Dallas Morning News]]'' described it as "one of the best [works] in this particular subgenre", praising "the richness of this invented world and its cultures ... [that] lends Mr. Martin's novels the feeling of medieval history rather than fiction."<ref>{{cite news|last=Shindler|first=Dorman|title=In Martin's 'Clash of Kings,' the delight is in the details|work=The Dallas Morning News|date=February 21, 1999}}</ref> Writing in ''[[The San Diego Union-Tribune]]'', Jim Hopper called ''A Clash of Kings'' "High Fantasy with a vengeance" and commented: "I'll admit to staying up too late one night last week to finish off this big book, and I hope it's not too terribly long until the next one comes out."<ref>{{cite news|last=Hopper|first=Jim|title=They're wiping out intelligent races -- What? Me worry?|work=The San Diego Union-Tribune|date=March 19, 1999}}</ref> Danielle Pilon wrote in the ''[[Winnipeg Free Press]]'' that the book "shows no signs of the usual 'middle book' aimlessness". Although she found the constantly switching viewpoints "momentarily confusing", she felt that it "draws the reader deep into the labyrinthine political and military intrigues and evokes sympathy for characters on all sides of the conflict."<ref>{{cite news|last=Pilon|first=Danielle|title=Second book in Martin series shines amid dull tomes|work=Winnipeg Free Press|date=March 28, 1999}}</ref> Bradley H. Sinor of the ''[[Tulsa World]]'' praised Martin for "keep[ing] readers balanced on a sword's edge" and managing to do "three important things" with ''A Clash of Kings'': "It grips the reader whether or not they read the earlier book, tells a satisfying story and leaves the reader wanting the next book as soon as possible."<ref>{{cite news|last=Sinor|first=Bradley H.|title=All the king's horses ...|work=Tulsa World|date=April 25, 1999}}</ref> ''[[The Oregonian]]'s'' Steve Perry called the book "easily as good as the first novel" and commented that the ''Song of Ice and Fire'' books were "so complex, fascinating and well-rendered that readers will almost certainly be hooked into the whole series." However, he cautioned that "if it were a movie, it would be rated "R" for sex and violence, so don't pick the book up for your 10-year-old nephew who likes Conan."<ref>{{cite news|last=Perry|first=Steve|title=Adventure drives medieval-style fantasy|work=The Oregonian|date=June 27, 1999}}</ref>

==Awards and nominations==
* [[Locus Award]] – Best Novel (Fantasy) (Won) – (1999)<ref name="WWE-1999"/>
* [[Nebula Award]] – Best Novel (Nominated) – (1999)<ref name="WWE-1999"/>
* Ignotus Award – Best Novel (Foreign) (Won) – (2004)

==References==
{{reflist}}

==External links==
* {{isfdb title|id=8657|title=A Clash of Kings}}
* {{IBList |type=book|id=234|name=A Clash of Kings}}

{{ASOIAF}}
{{George R. R. Martin}}
{{Locus Award Best Fantasy Novel}}

{{DEFAULTSORT:Clash Of Kings}}
[[Category:1998 American novels]]
[[Category:A Song of Ice and Fire books]]
[[Category:Novels by George R. R. Martin]]
[[Category:American fantasy novels]]
[[Category:1990s fantasy novels]]
[[Category:Locus Award-winning works]]
[[Category:Novels adapted into television programs]]