{{Infobox television
|show_name      = The Rookie
|rated          =
|image          = [[Image:TheRookieCTU.jpg|200px]]
|sound          =
|caption        = The Rookie: Day 3
|genre          = [[Drama]]<br />[[Action movie|Action]]
|creator        = [[Kevin Townsend]]
|starring       = [[Jeremy Ray Valdez]]<br>[[Eric Beck]]<br>[[Katrina Law]]<br>[[Jonathan P. Nichols]]<br>[[Palmer Davis]]
|country        = United States
|language       = English
|num_seasons    = 3
|num_episodes   = 12
|list_episodes  =
|runtime        = 3–5 minutes (varies)
|network        = [[Fox Broadcasting Company|Fox]]
|picture_format =
|first_aired    = January 15, 2007
|last_aired     = April 21, 2008
}}

'''''The Rookie''''', formerly known as '''''The Rookie: CTU''''' in its first two seasons, is an exclusive series of [[webisode]]s made in 2007 and 2008, released over the [[internet]], following the life of Jason Blaine, a new [[Counter Terrorist Unit|CTU]] agent. The series is a [[Spin-off (media)|spin-off]] from the TV series [[24 (TV series)|''24'']].

The series is made close to the style of ''24'', in that it uses many of the same devices. For example, at the beginning of each "Day" there is a voiceover of Jason Blaine telling the viewer what time the series takes place between. Also, at the beginning of each episode, there is a "Previously on ''The Rookie''" section, again with a voice over by Jason Blaine.  The splitscreen is incorporated into the episodes often (at least once) to show phone conversations as well as to wrap up each episode. However, it does not take place in real time.

==Episodes==

===Day 1: Coffee Run===
"Coffee Run" is the first season of ''The Rookie''.  It premiered on January 15, 2007, the day after [[24 (season 6)|24: Season 6]] premiered on [[Fox Broadcasting Company|Fox]] in the [[United States]]. It introduced Jason Blaine, a new rookie agent working at CTU Los Angeles, striving to get in some field work. There were three episodes made, each being released weekly.

In the series, Jason was sent to collect coffee for members of CTU whilst a briefing was being held. Whilst at the coffee shop he noticed some robbers entering a bank and decided to intervene. Going in a back entrance, he took the gun of a disarmed security guard and shot several of the robbers. At the same time, a co-worker from CTU called a [[SWAT]] team who entered the bank and dealt with the rest of the robbers. Jason arrived back at CTU with the now-cold coffees, much to his boss, Alton Maxwell's, displeasure.

===Day 2: Mistaken Identity===
"Mistaken Identity", sometimes known as "Get This to ...", is the second season of ''The Rookie''. It premiered on March 26, 2007, the day after 24: Day 6 8:00pm–9:00pm premiered on FOX in the United States. It followed the further tribulations of Jason Blaine. There were three episodes made, each being released weekly.

Jason Blaine, yearning for more field experience, agreed to take a [[Personal digital assistant|PDA]] out in the deserts of Los Angeles to his boss and Deputy Director of CTU Los Angeles Alton Maxwell. Whilst traveling, he received a call intended for his boss, informing him of a terrorist threat against a Russian Ambassador who Maxwell was planning to meet. Jason managed to stop the terrorists before they could attack the Ambassador, and once again to Maxwell's displeasure, Jason arrived late.

===Day 3: Extraction===
"Extraction" is the third season of ''The Rookie''. It premiered on March 17, 2008, just under a year from when the last season premiered. It concluded on April 21, 2008. Six episodes were made, double the amount of the last two seasons. A new episode was released weekly.

"Extraction" followed the further adventures of Jason Blaine, now a more advanced field agent working for the [[Federal Bureau of Investigation|FBI]] after the closure of CTU. His boss, Alton Maxwell was captured, and Jason strived to save him from the evil hands of drug kingpin Esteban Salazar, brother to the villains of [[24 (season 3)|the third season of ''24'']].

==Cast==

===Starring===
* [[Jeremy Ray Valdez]] as ''Jason Blaine'' (Days 1–3)
* [[Eric Beck]] as ''Alton Maxwell'' (Days 1–3)
* [[Palmer Davis]] as ''Angie Lawson'' (Days 1–2)
* [[Katrina Law]] as ''Kate Wyman'' (Day 3)
* [[Jonathan P. Nichols]] as ''Esteban Salazar'' (Day 3)
* [https://pro-labs.imdb.com/name/nm1631897?ref_=tt_nv_mp_profile Igor Korosec] as ''Russian Terrorist'' (Day 2)

==External links==
*{{IMDb title|id=1091921|title=The Rookie: CTU}}

{{24 (TV series)}}

{{DEFAULTSORT:Rookie (Web Series), The}}
[[Category:24 (TV series)|Rookie, The]]
[[Category:Espionage television series]]
[[Category:Television shows set in Los Angeles]]
[[Category:Terrorism in fiction]]
[[Category:Television series by 20th Century Fox Television]]