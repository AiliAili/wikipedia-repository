{{Userspace draft|source=ArticleWizard|date=July 2016}} 

'''Ellen Ida Benham''', B.Sc. (Adelaide), Dip. Ed. (Oxford), (12 March 1871 – 27 April 1917) was a science teacher and education pioneer in South Australia.

==History==
Ellen was born at "Talarno", Kapunda to solicitor W. H. Benham, who arrived in South Australia aboard ''The Gipsy'' in August 1853, and his second wife Amie Benham née Higgins. Her father was an extraordinary man: working as a shearer for three years and driving bullocks before settling down as a lawyer's clerk and studying law.<ref>{{cite news |url=http://nla.gov.au/nla.news-article62399381 |title=A Veteran Lawyer |newspaper=[[The Register (Adelaide)]] |volume=LXXXIV, |issue=22,744 |location=South Australia |date=1 October 1919 |accessdate=24 July 2016 |page=7 |via=National Library of Australia}}</ref>

Ellen was educated privately, then at the [[Advanced School for Girls]] before taking the science course at the [[Adelaide University]], graduating BSc with honours in 1892.<ref name=adb>Helen Jones, 'Benham, Ellen Ida (1871–1917)', Australian Dictionary of Biography, National Centre of Biography, Australian National University, http://adb.anu.edu.au/biography/benham-ellen-ida-5200/text8749, published first in hardcopy 1979, accessed online 24 July 2016.</ref> She taught science at the Kapunda Church of England school for a short time before taking a study tour of England and Europe. She took an assistant position at Dryborough School, then taught at [[Tormore House School]] for a little over twelve years. During this time she travelled to England, where she earned her Diploma of Education at Oxford University. After the death of [[Ralph Tate|Professor Tate]] she taught botany at Adelaide University and was appointed lecturer ''pro tem'' while awaiting the arrival of Professor Osborn<ref>{{cite news |url=http://nla.gov.au/nla.news-article5583140 |title=Personal |newspaper=[[The Advertiser (Adelaide)]] |volume=LIX, |issue=18,269 |location=South Australia |date=3 May 1917 |accessdate=24 July 2016 |page=6 |via=National Library of Australia}}</ref> Thus she was, under similar circumstances to [[Ada Mary Lambert]] at Melbourne University, the first woman appointed to an academic post at the university. 

In December 1912 she purchased Walford School, Fisher Street, [[Malvern, South Australia|Malvern]], from Lydia Adamson, its founder in 1893. She reformed its teaching methods and course content, notably introducing science to the curriculum. In 1917 she sold the school to Mabel Jewell Baker, who then led the school for almost 40 years.

==Recognition==
In 1922 the Ellen Benham Scholarship was established at Walford in recognition of her contribution to the development of the school.<ref>{{cite news |url=http://nla.gov.au/nla.news-article63747206 |title=Walford House School |newspaper=[[The Register (Adelaide)]] |volume=LXXXVII, |issue=25,580 |location=South Australia |date=21 December 1922 |accessdate=24 July 2016 |page=11 |via=National Library of Australia}}</ref> The Benham Wing, which incorporated science classrooms, in what became the Walford Church of England Girls' Grammar School, was also named for her.<ref name=adb/>

She was a co-founder and longtime supporter of Adelaide University's Women's Students' Society, and its president for several years.

== References ==
{{Reflist}}

{{DEFAULTSORT:Benham, Ellen}}
[[Category:1871 births]]
[[Category:1917 deaths]]
[[Category:Australian women scientists]]
[[Category:Australian women academics]]
[[Category:Australian headmistresses]]

[[Category:Articles created via the Article Wizard]]