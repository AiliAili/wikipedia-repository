{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Mosses from an Old Manse
| title_orig    = 
| translator    = 
| image         = MossesFromAnOldManse-titlepage.jpg
| caption = 
| author        = [[Nathaniel Hawthorne]]
| illustrator   = 
| cover_artist  = 
| country       = [[United States]]
| language      = [[English language|English]]
| series        = 
| genre         = [[Short stories]]
| publisher     = [[G. P. Putnam's Sons|Wiley & Putnam]]
| release_date  = 1846
| english_release_date = 
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
'''''Mosses from an Old Manse''''' is a [[short story]] collection by [[Nathaniel Hawthorne]], first published in 1846.

==Background and publication history==
The collection includes several previously-published short stories, and was named in honor of [[The Old Manse]] where Hawthorne and his wife lived for the first three years of their marriage. The first edition was published in 1846.

Hawthorne seems to have been paid $75 for the publication.<ref name=Widmer109>{{cite book|author=Widmer, Edward L. |title=Young America: Flowering of Democracy in New York City|location= New York|publisher= Oxford University Press|date= 1999|page= 109|isbn=0-19-514062-1}}</ref>

==Analysis==
Many of the tales collected in ''Mosses from an Old Manse'' are [[allegory|allegories]] and, typical of Hawthorne, focus on the negative side of human nature. Hawthorne's friend [[Herman Melville]] noted this aspect in his review "[[Hawthorne and His Mosses]]":
{{quote|This black conceit pervades him through and through. You may be witched by his sunlight,—transported by the bright gildings in the skies he builds over you; but there is the blackness of darkness beyond; and even his bright gildings but fringe and play upon the edges of thunder-clouds.<ref>{{cite book|author=Crew, Frederick|title=The Sins of the Fathers: Hawthorne's Psychological Themes|location= Berkeley|publisher= University of California Press|date= 1989|page= 8|isbn= 0-520-06817-3}}</ref>}}

[[William Henry Channing]] noted in his review of the collection, in ''[[The Phalanx|The Harbinger]]'',  its author "had been baptized in the deep waters of ''Tragedy''", and his work was dark with only brief moments of "serene brightness" which was never brighter than "dusky twilight".<ref>{{cite book|author=Delano, Sterling F. |title=Brook Farm: The Dark Side of Utopia|location= Cambridge, Massachusetts|publisher= The Belknap Press of Harvard University Press|date= 2004|pages= 233–234|isbn= 0-674-01160-0}}</ref>

==Critical reception==
After the book's first publication, Hawthorne sent copies to critics including [[Margaret Fuller]], [[Rufus Wilmot Griswold]], [[Edgar Allan Poe]], and [[Henry Theodore Tuckerman]].<ref>{{cite book|author=Miller, Edwin Haviland|title=Salem is my Dwelling Place: A Life of Nathaniel Hawthorne|location= Iowa City|publisher= University of Iowa Press|date= 1991|page= 264|isbn= 0-87745-332-2}}</ref> Poe responded with a lengthy review in which he praised Hawthorne's writing but faulted him for associating with New England journals, [[Ralph Waldo Emerson]], and the [[Transcendentalism|Transcendentalists]]. He wrote, "Let him mend his pen, get a bottle of visible ink, come out from the Old Manse, cut [[Amos Bronson Alcott|Mr. Alcott]], hang (if possible) the editor of '[[The Dial]],' and throw out of the window to the pigs all his odd numbers of the [[North American Review]].<ref>{{cite book|author=Sova, Dawn B. |title=Edgar Allan Poe: A to Z|location= New York|publisher= Checkmark Books|date= 2001|pages= 233|isbn= 978-0-8160-4161-9}}</ref> A young [[Walt Whitman]] wrote that Hawthorne was underpaid, and it was unfair that his book competed with imported European books. He asked, "Shall real American genius shiver with neglect while the public runs after this foreign trash?"<ref name=Widmer109/> Generally, most contemporary critics praised the collection and considered it better than Hawthorne's earlier collection, ''[[Twice-Told Tales]]''.<ref>{{cite book|author=McFarland, Philip|title=Hawthorne in Concord|location= New York|publisher= Grove Press|date= 2005|pages= 132|isbn= 0-8021-1776-7}}</ref>

Regarding the second edition, published in 1854, Hawthorne wrote to publisher [[James Thomas Fields]] that he no longer understood the messages he was sending in these stories. He shared, "I remember that I always had a meaning—or, at least, thought I had",<ref>{{cite book|author=Miller, Edwin Haviland|title=Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne|location= Iowa City|publisher= University of Iowa Press|date= 1991|page= 248|isbn= 0-87745-332-2}}</ref> and noted, "Upon my honor, I am not quite sure that I entirely comprehend my own meaning in some of these blasted allegories... I am a good deal changed since those times; and to tell you the truth, my past self is not very much to my taste, as I see in this book."<ref>{{cite book|author=Crew, Frederick|title=The Sins of the Fathers: Hawthorne's Psychological Themes|location= Berkeley|publisher= University of California Press|date= 1989|pages= 212|isbn= 0-520-06817-3}}</ref>

==Contents==
{{wikisource}}

* "[[The Old Manse]]" (1846)
* "[[The Birth-Mark]]" (1843)
* "A Select Party" (1844)
* "[[Young Goodman Brown]]" (1835)
* "[[Rappaccini's Daughter]]" (1844)
* "Mrs. Bullfrog" (1837)
* "Fire-Worship" (1843)
* "Buds and Bird-Voices" (1843)
* "Monsieur du Miroir" (1837)
* "The Hall of Fantasy" (1843)
* "The Celestial Rail-road "(1843)
* "The Procession of Life" (1843)
* "The New Adam and Eve" (1843)
* "[[Egotism; or, The Bosom-Serpent]]" (1843)
* "The Christmas Banquet" (1844)
* "Drowne's Wooden Image" (1844)
* "The Intelligence Office" (1844)
* "[[Roger Malvin's Burial]]" (1832)
* "[[P.'s Correspondence]]" (1845)
* "Earth's Holocaust" (1844)
* "The Old Apple-Dealer" (1843)
* "[[The Artist of the Beautiful]]" (1844)
* "[[A Virtuoso's Collection]]" (1842)

===Added to second edition in 1854===
* "[[Feathertop]]" (1852)
* "Passages from a Relinquished Work" (1834)
* "Sketches from Memory" (1835)

==References==
{{reflist}}

==Additional reading==
*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=145}}

==External links==
* {{librivox book | title=Mosses from an Old Manse | author=Nathaniel Hawthorne}}
* [http://www.hawthorneinsalem.org/Architecture/HousesinConcord/TheOldManse/Literature.html Literature Related to the Old Manse], ''Hawthorne in Salem''

{{Nathaniel Hawthorne}}

[[Category:1846 short story collections]]
[[Category:Short story collections by Nathaniel Hawthorne]]