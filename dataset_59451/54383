{{Use dmy dates|date=August 2011}}
{{Use Australian English|date=August 2011}}
{{Infobox Australian place
| type                   = protected
| name                   = Marino Conservation Park
| state                  = sa
| iucn_category          = iii
| image                  = Marino CP Lighthouse.jpg
| caption                = Marino Lighthouse
| image2                 = Marino CP cropped panorama.jpg
| caption2               = View of coast and city
| image_alt              = 
| latd                   =  35.05265
| latm                   = 
| lats                   = 
| longd                  = 138.5088 
| longm                  = 
| longs                  = 
| map_scale              = 2000
| relief                 = yes
| map_alt                = 
| nearest_town_or_city   = [[Marino, South Australia|Marino]]
| area                   = {{convert|30|ha|acre|0|abbr=on}}
| area_footnotes         = <ref name=ReserveList>{{cite web|title=Protected Areas Information System - reserve list (as of 16 July 2015)|url= http://www.environment.sa.gov.au/files/sharedassets/public/park_management/protected-areas-30june2015.pdf |publisher=Department of Environment Water and Natural Resources (DEWNR)|accessdate=3 August 2015|pages=}}</ref>
| established            = {{start date|1989|011|02|df=y}}  
| established_footnotes  = <ref name=ReserveList/>
| visitation_num         = 
| visitation_year        = 
| visitation_footnotes   = 
| managing_authorities   = [[Department of Environment, Water and Natural Resources (South Australia)|Department of Environment, Water and Natural Resources]]
| url                    = http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide/Marino_Conservation_Park
|footnotes=IUCN<ref name=CAPAD2014SASum/><br />Coordinates<ref name=CAPAD2014SASum>{{cite web|title= Terrestrial Protected Areas of South Australia (see 'DETAIL' tab)|url=http://www.environment.gov.au/system/files/pages/d00ca066-1c8c-412a-9e16-2a37647454a7/files/capad2014sa.xlsx|work=CAPAD 2014|publisher=Australian Government, Department of the Environment (DoE) |accessdate=13 March 2015|date=2014}}</ref>
}}
'''Marino Conservation Park''' is a 30 ha [[protected area]] located in the [[Adelaide]] suburb of [[Marino, South Australia|Marino]] with views across Adelaide and the coastline.<ref name=ReserveList/><ref name=Marion/>

Once part of the lands of the Aboriginal [[Kaurna people|Kaurna]] people, the Marino Conservation Park and surrounding areas are part of the [[Tjilbruke]] dreamtime.<ref name=Marion>{{cite web|url=http://www.marion.sa.gov.au/web/webmar.nsf/lookup/Marino+Conservation+Park|publisher=Marion City Council|title=Marino Conservation Park|accessdate=22 July 2008 |archiveurl = http://web.archive.org/web/20080720103120/http://www.marion.sa.gov.au/web/webmar.nsf/lookup/Marino+Conservation+Park <!-- Bot retrieved archive --> |archivedate = 20 July 2008}}</ref>

Proclaimed in 1989 as a conservation park, it aims to protect flora such as the ground cover Desert Saw Sedge ''Gahnia lanigera'' and Twiggy Daisy Bush [[Olearia|Olearia ramulosa]].   Native grass species such as Danthonia  species and [[Stipa]] species as well as groundcovers dominate the central and eastern portions of the park.  The Elegant Wattle ''Acacia victoriae'' can also be seen in the park.<ref name=Marion/>

The steep west-facing hillside above the railway line contains a very significant remnant area of coastal heath vegetation, including rare plants such as lemon beauty heads [[Calocephalus|Calocephalus citreus]], shiny ground berry ''Acrotiche patula'' and native apricot [[Pittosporum|Pittosporum phylliraecoides]].<ref name=Marion/>

Past land-use practices severely depleted the habitat available for native wildlife.  Twenty-nine species of bird including owls, falcons, honeyeaters and  rosellas frequent the park.  The [[Eastern brown snake]] and insects also find sanctuary in the remnant vegetation and open space area.<ref name=Marion/>

A 1.5&nbsp;km self-guided botanical trail starts from the car park with two gentle hills to climb.  The path is not suitable for wheelchairs.  There are also designated dog trails.  There are no picnic, toilet or other facilities in the park.  Camping is prohibited.<ref name=Marion/>

The conservation park has an active [[Friends of Parks]], mostly locals, who regularly weed and look after the area.<ref>{{cite web|title=FRIENDS OF MARINO|url=http://www.friendsofparkssa.org.au/members-directory/friends-of-marino-1|publisher=Friends of Parks inc.|accessdate=5 October 2015}}</ref>

The conservation park is accessible via the [[Marino Rocks railway station]], which is nearby.<ref name=Marion/>

The conservation park is classified as an [[International Union for Conservation of Nature|IUCN]] [[IUCN protected area categories#Category III — Natural Monument or Feature|Category III protected area]].<ref name=CAPAD2014SASum/>

==Marino Rocks Lighthouse==
The '''Marino Rocks Lighthouse''', or Marino Lighthouse as it is otherwise known, is situated within the Park. It is a white, square concrete tower with semicircular lantern. This operational lighthouse was established in 1962 and is managed by the [[Australian Maritime Safety Authority]]
[[File:Marino CP south panorama.jpg|thumb|left|View of Hallett Cove and Port Stanvac]]
{{clear left}}

==See also==
* [[List of protected areas in Adelaide]]

==References==
{{reflist}}

==External links==
* [http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide/Marino_Conservation_Park Marino Conservation Park official webpage]
*[http://www.friendsofparkssa.org.au/members-directory/friends-of-marino-1 Friends of Marino webpage on Friends of the Parks website]
*[http://www.protectedplanet.net/marino-conservation-park entry for Marino Conservation Park  on protected planet]
{{Protected areas of South Australia}}

[[Category:Conservation parks of South Australia]]
[[Category:Protected areas in Adelaide]]
[[Category:Protected areas established in 1989]]
[[Category:1989 establishments in Australia]]