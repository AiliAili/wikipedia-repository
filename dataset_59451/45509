{{Use British English|date=June 2015}}
{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Charles George Douglas Napier
| image         =
| caption       =
| birth_date    = 1892
| death_date    = 15 May {{death year and age|1918|1892}}
| birth_place  = [[Shepherd's Bush]], London, England
| death_place  = [[Lamotte-du-Rhône|Lamotte]]
| placeofburial_label =
| placeofburial =  [[Arras Flying Services Memorial]], [[Pas de Calais]], France
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = United Kingdom
| branch        = British Army<br/>Royal Air Force
| serviceyears  = 1914–1918
| rank          = Captain
| unit          = [[Army Cyclist Corps]]<br/>[[No. 20 Squadron RAF|No. 20 Squadron RFC]]<br/>[[No. 48 Squadron RAF]]
| commands      =
| battles       = [[World War I]]
| awards        = [[Military Cross]]<br/>[[Distinguished Conduct Medal]]<br/>[[Médaille militaire|Médaille Militaire]] (France)
| relations     =
| laterwork     =
}}
Captain '''Charles George Douglas Napier''', {{post-nominals|country=GBR|size=100%|MC|DCM|sep=yes}} (1892 – 15 May 1918) was a British [[World War I]] [[flying ace]] credited with nine aerial victories before being killed in action.

==Biography==
Napier was born in Shepherd's Bush, London, England in 1892.<ref name="theaerodrome">{{cite web |url= http://www.theaerodrome.com/aces/england/napier1.php |title=Charles George Douglas Napier |work=The Aerodrome |year=2014 |accessdate=29 September 2014}}</ref> Before the war he was employed in the Fire Department of the Employers' Liability Assurance Corporation Ltd.<ref name="Aviva">{{cite web |url=https://www.aviva.com/about-us/heritage/world-war-one/employees-honoured/n/ |title=Staff of Aviva constituent companies decorated for their actions in the 1914-1918 war |work=[[Aviva plc]] |year=2014 |accessdate=29 September 2014}}</ref>

===Army service===
He began his military service as a [[private (rank)|private]] in the [[Army Cyclist Corps]].<ref>{{cite web |url= http://discovery.nationalarchives.gov.uk/details/r/D4215799 |title=Medal card of Napier, Charles George Douglas Corps: Army Cyclist Corps Regiment |work=The National Archives |year=2014 |accessdate=29 September 2014}}</ref> In August 1915, while serving as a [[corporal]] in the [[47th (1/2nd London) Division|47th Divisional]] Cyclist Company, Napier was awarded the [[Distinguished Conduct Medal]]. His citation read:

{{Quote|For conspicuous gallantry on the 25th and 26th May 1915, at [[Givenchy-lès-la-Bassée|Givenchy]]. After the withdrawal of a bombing party, and having become separated from it, he remained in the trenches with a Serjeant and some men of another Battalion, and greatly assisted this small party by the use of his bombs in retaining possession of a captured trench.<ref>{{London Gazette |date=13 August 1915 |city=e |issue=12841 |startpage=1237 }}</ref>}}

On 24 February 1916 he received the [[Médaille militaire|Médaille Militaire]] from France "in recognition of ... distinguished service during the campaign".<ref>{{London Gazette |date=28 February 1916 |issue=12909 |city=e |startpage=335 |endpage=337 |nolink=yes }}</ref>

===Royal Flying Corps service===
Napier was seconded to the Royal Flying Corps, and commissioned as a temporary second lieutenant on 23 September 1917.<ref>{{London Gazette |date=18 October 1917 |supp=yes |issue=30339 |startpage=10682 }}</ref> He served with No. 20 Squadron in late 1917 before transferring to 48 Squadron in early 1918. He scored his first aerial victory while with them, on 7 February. On 4 April he was promoted to acting-captain.<ref>{{London Gazette |date=19 April 1918 |issue=30640 |startpage=4743 |nolink=yes }}</ref> He would run his total to nine wins,<ref name="ABT">Shores ''et.al.'' (1997), p.288.</ref> rounding off his tally with a triple victory on 9 May; he and his gunner [[Walter Beales]] were also shot down during this action, though without injury.<ref>Guttman & Dempsey (2007), p.16.</ref> Six days later, he was killed in action, shot down along with his gunner of the day. On 12 June, the Germans verified Napier's death. Ten days later, his award of the [[Military Cross]] was gazetted,<ref name="ABT"/> as follows:

{{Quote|On one occasion during a low-flying bombing attack he descended to a height of 100 feet and dropped four bombs amongst a body of enemy troops, causing heavy casualties and scattering the enemy in all directions. Later, whilst on offensive patrol, he observed an enemy two-seater and two scouts. He fired twenty rounds at the two-seater, with the result that it crashed, and then attacked one of the scouts, which turned over completely, and finally went down in a vertical nose dive. In all he has to his credit two enemy machines crashed and four driven down out of control. He has displayed the greatest judgment, determination and daring.<ref>{{London Gazette |date=21 June 1918 |supp=yes |issue=30761 |startpage=7419 |nolink=yes }}</ref>}}

{| class="wikitable"
|+List of aerial victories<ref name="theaerodrome"/>
|-
!No.
!Date/time
!Aircraft
!Foe
!Result
!Location
!Notes
|-
| 1
| {{nowrap|7 February 1918}}<br/>''ca.'' 0600 hours
| {{nowrap|[[Bristol F.2 Fighter|Bristol F.2B Fighter]]}}
| [[LVG]] reconnaissance plane
| Destroyed
| [[Le Catelet]]
| rowspan="6" valign="top" |With gunner/observer [[Joseph Michael John Moore|J. M. J. Moore]] 
|-
| 2
| 8 March 1918<br/>''ca.'' 0600 hours
| Bristol F.2B Fighter
| [[Deutsche Flugzeug-Werke|DFW]] reconnaissance plane
| Set on fire; destroyed
| [[Saint-Quentin, Aisne|Saint-Quentin]]
|-
| 3
| rowspan="2" |16 March 1918<br/>''ca.'' 0600 hours
| rowspan="2" |Bristol F.2B Fighter
| LVG reconnaissance plane
| Driven down out of control
|-
| 4
| [[Albatros D.III]]
| Driven down out of control
|
|-
| 5
| rowspan="2" | 27 March 1918<br/>@ 1120 hours
| rowspan="2" | Bristol F.2B Fighter<br/>(s/n C4886)
| Reconnaissance plane
| Destroyed
| rowspan="2" |Southwest of [[Roye, Somme|Roye]]
|-
| 6
| [[Pfalz D.III]]
| Driven down out of control
|-
| 7
| rowspan="3" | 9 May 1918<br/>@ 1540 hours
| rowspan="3" | Bristol F.2B Fighter<br/>(s/n C4750)
| [[Fokker Dr.I|Fokker Triplane]]
| Driven down out of control
| rowspan="3" |  [[Wiencourt-l'Équipée]]-[[Méricourt-sur-Somme|Mericourt]]
| rowspan="3" valign="top" |With gunner/observer [[Walter Beales]]
|-
| 8 
| Fokker Triplane
| Driven down out of control
|-
| 9
| Fokker Triplane
| Driven down out of control
|-
|}
{{See also|Aerial victory standards of World War I}}

==References==
;Notes
{{reflist|30em}}
* {{cite book |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell |last3=Guest |title=Above the Trenches: A Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=0-948817-19-4 |lastauthoramp=yes}}
* {{cite book |title=Bristol F2 Fighter Aces of World War I |first1=Jon |last1=Guttman |first2=Harry |last2=Dempsey |publisher=Osprey Publishing |year=2007 |isbn=978-1-84603-201-1 |lastauthoramp=yes}}

{{wwi-air}}

{{DEFAULTSORT:Napier, Charles}}
[[Category:1892 births]]
[[Category:1918 deaths]]
[[Category:People from Shepherd's Bush]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Flying Corps officers]]
[[Category:British World War I flying aces]]
[[Category:British military personnel killed in World War I]]
[[Category:Aviators killed by being shot down]]
[[Category:Recipients of the Distinguished Conduct Medal]]
[[Category:Recipients of the Military Cross]]