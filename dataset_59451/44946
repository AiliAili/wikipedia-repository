{{Continuum mechanics}}
A '''seismic metamaterial''', is a [[metamaterial]] that is designed to counteract the adverse effects of [[seismic waves]] on artificial structures, which exist on or near the surface of the earth.<ref name=seismic-cloak/><ref name=seismic-cloak-2/><ref name=control_elastic_waves>{{cite journal| last = Brun| first =M. |author2=S. Guenneau |author3=and A.B. Movchan | title =Achieving control of in-plane elastic waves |journal =Appl. Phys. Lett.| volume =94 |issue =61903 |pages =1–7|date =2009-02-09|arxiv =0812.0912 |doi =10.1063/1.3068491|bibcode = 2009ApPhL..94f1903B }}</ref>  {{As of|2009}} seismic metamaterials were still in the development stage.<ref name=seismic-cloak/><ref name=seismic-cloak-2/>

==The mechanics of seismic waves==
{{see also|Seismic wave|Elastic wave|Hooke's law}}
More than a million [[earthquakes]] are recorded each year, by a worldwide system of earthquake detection stations. The propagation [[velocity]] of the [[seismic waves]] depends on [[density]] and [[elasticity (physics)|elasticity]] of the earth materials. In other words, the speeds of the seismic waves vary as they travel through different materials in the [[earth]]. The two main components of a seismic event are [[Seismic wave#Body waves|body waves]] and [[Seismic wave#Surface waves|surface waves]]. Both of these have different modes of wave propagation.<ref name=seismic_waves>{{cite book| last =Krebs| first =Robert E.| title =The basics of earth science| publisher =Greenwood Publishing Group, Incorporated| year =2003|location =Westport, CT, USA| pages =32–33|url =https://books.google.com/books?id=-4ndyH7u6T0C&pg=PA32|isbn =978-0-313-31930-3}}</ref>

==Dissipating ocean surface waves with seismic metamaterials==
Computations showed that seismic waves traveling toward a [[building]], could be directed around the building, leaving the building unscathed, by using ''seismic metamaterials''. The very long [[wavelength]]s of earthquake waves would be shortened as they interact with the [[metamaterial]]s; the waves would pass around the building so as to arrive in phase as the earthquake wave proceeded, as if the building was not there. The mathematical models produce the regular pattern provided by [[Metamaterial cloaking]]. This method was first understood with [[Metamaterial cloaking|electromagnetic cloaking metamaterials]] - the [[Electromagnetic radiation|electromagnetic]] energy is in [[Effective field theory|effect]] directed around an object, or hole, and protecting buildings from seismic waves employs this same principle.<ref name=seismic-cloak/><ref name=seismic-cloak-2/> 

Giant [[polymer]]-made [[split ring resonator]]s combined with other metamaterials are designed to couple at the seismic [[wavelength]]. [[Concentric]] layers of this material would be stacked, each layer separated by an [[Deformation (engineering)#Elastic deformation|elastic]] medium. The design that worked is ten layers of six different materials, which can be easily deployed in building foundations. As of 2009, the project is still in the design stage.<ref name=seismic-cloak>{{cite news|last =Johnson| first =R. Colin|title =Metamaterial cloak could render buildings 'invisible' to earthquakes|publisher= EETimes.com|date =2009-07-23
|url =http://www.eetimes.com/showArticle.jhtml?articleID=218600378| accessdate =2009-09-09}}</ref><ref name=seismic-cloak-2>{{Cite news| last = Barras| first = Colin| title =Invisibility cloak could hide buildings from quakes| newspaper =New Scientist| date =2009-06-26 
|url =http://www.newscientist.com/article/dn17378-invisibility-cloak-could-hide-buildings-from-quakes.html#| accessdate =2009-10-20}}</ref>

===Electromagnetics cloaking principles for seismic metamaterials===
For seismic metamaterials to protect surface structures, the proposal includes a layered structure of metamaterials, separated by elastic [[plating|plates]] in a [[cylindrical]] configuration. A prior simulation showed that it is possible to create concealment from electromagnetic radiation with concentric, alternating layers of electromagnetic metamaterials. That study was is in contrast to concealment by inclusions in a split ring resonator designed as an [[anisotropic]] metamaterial.<ref name=layered-metamtarial/>

The configuration can be viewed as alternating layers of "[[homogeneity (physics)|homogeneous]] isotropic dielectric material" A. with "homogeneous [[isotropic]] dielectric material" B. Each [[dielectric]] material is much thinner than the radiated wavelength. As a whole, such structure is an anisotropic medium. The layered dielectric materials surround an "infinite conducting cylinder". The layered dielectric materials radiate outward, in a concentric fashion, and the cylinder is encased in the first layer. The other layers alternate and surround the previous layer all the way to the first layer. Electromagnetic wave scattering was calculated and simulated for the layered (metamaterial) structure and the split-ring resonator anistropic metamaterial, to show the effectiveness of the layered metamaterial.<ref name=layered-metamtarial>{{cite journal| last = Huang| first = Ying| last2 = Feng| first2 = Y| last3 = Jiang| first3 = T |title =Electromagnetic cloaking by layered structure of homogeneous isotropic materials |journal =Optics Express| volume =15| issue = 18| pages =11133–11141| date = 2007-08-21| pmid = 19547468 |doi =10.1364/OE.15.011133|bibcode = 2007OExpr..1511133H |arxiv = 0709.0363 }}</ref>

===Acoustic cloaking principles for seismic metamaterials===
The theory and ultimate development for the ''seismic metamaterial'' is based on [[coordinate transformations]] achieved when [[Negative index metamaterials|concealing]] a small cylindrical object with [[electromagnetic wave]]s. This was followed by an analysis of [[acoustics|acoustic]] cloaking, and whether or not coordinate transformations could be applied to artificially fabricated [[Acoustic metamaterials|acoustic material]]s.<ref name=control_elastic_waves/> 

Applying the concepts used to understand [[Electromagnetism|electromagnetic]] materials to material properties in other systems shows them to be closely analogous. [[Wave vector]], [[wave impedance]], and direction of power flow are universal. By understanding how [[permittivity]] and [[Permeability (electromagnetism)|permeability]] control these components of [[wave propagation]], applicable analogies can be used for other material interactions.<ref name=acoustic-transform-media/> 

In most instances, applying coordinate transformation to engineered artificial [[transmission media|elastic media]] is not possible. However, there is at least one special case where there is a direct equivalence between electromagnetics and [[elastodynamics]]. Furthermore, this case appears practically useful. In two dimensions, [[isotropic]] acoustic media and isotropic electromagnetic media are exactly equivalent. Under these conditions, the isotropic characteristic works in [[anisotropic]] media as well.<ref name=acoustic-transform-media/>

It has been demonstrated mathematically that the 2D [[Maxwell equations]] with [[normal incidence]] apply to 2D [[acoustics|acoustic]] equations when replacing the electromagnetic parameters with the following acoustic parameters: [[Pressure#Scalar nature|pressure]], [[Flow velocity|vector fluid velocity]], fluid [[mass density]] and the fluid [[bulk modulus]]. The compressional wave solutions used in the electromagnetic cloaking are transferred to material fluidic solutions where fluid motion is parallel to the wavevector. The computations then show that coordinate transformations can be applied to acoustic media when restricted to normal incidence in two dimensions.<ref name=acoustic-transform-media>{{cite journal| last = Cummer| first = Steven A|author2=David Schurig | title =One path to acoustic cloaking| journal =New Journal of Physics| volume =9| pages = 1–7| date =2007-03-02| doi =10.1088/1367-2630/9/3/045|bibcode = 2007NJPh....9...45C| issue = 3 }}</ref>

Next the electromagnetic cloaking shell is referenced as an exact equivalence for a simulated demonstration of the acoustic cloaking shell. Bulk modulus and mass density determine the spatial dimensions of the cloak, which can bend any incident wave around the center of the shell. In a simulation with perfect conditions, because it is easier to demonstrate the principles involved, there is zero scattering in any direction.<ref name=acoustic-transform-media/>

===The seismic cloak===
However, it can be demonstrated through [[COMSOL Multiphysics|computation and visual simulation]] that the waves are in fact dispersed around the location of the building. The frequency range of this capability is shown to have no limitation regarding the [[Seismic waves|radiated frequency]]. The cloak itself demonstrates no forward or back [[scattering]], hence, the seismic cloak becomes an effective medium.<ref name=control_elastic_waves/>

== Full-scale seismic test ==
In 2012, researchers held an experimental field-test in France, with the aim to highlight analogy with phononic crystal.<ref>{{Cite journal|title = Experiments on Seismic Metamaterials: Molding Surface Waves|url = http://link.aps.org/doi/10.1103/PhysRevLett.112.133901|journal = Physical Review Letters|date = 2014-03-31|pages = 133901|volume = 112|issue = 13|doi = 10.1103/PhysRevLett.112.133901|first = S.|last = Brûlé|first2 = E. H.|last2 = Javelaud|first3 = S.|last3 = Enoch|first4 = S.|last4 = Guenneau|bibcode = 2014PhRvL.112m3901B }}</ref>

==See also==
{{colbegin|3}}
*[[Negative index metamaterials]]
*[[Metamaterial antennas]]
*[[Photonic crystal]]
*[[Superlens]]
*[[Split-ring resonator]]
*[[Terahertz metamaterials]]
*[[Tunable metamaterials]]
*[[Photonic metamaterials]]
{{colend}}

===Material properties===
{{colbegin|3}}
*[[Acoustic dispersion]]
*[[Bulk modulus]]
*[[Constitutive equation]]
*[[Elastic wave]]
*[[Equation of state]]
*[[Linear elasticity]]
*[[Permeability (earth sciences)|Permeability]]
*[[Permittivity]]
*[[Stress (mechanics)]]
*[[Thermodynamic state]]
{{colend}}

==References==
{{reflist|2}}

[[Category:Seismology]]
[[Category:Metamaterials]]
[[Category:Continuum mechanics]]