{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Francis Stanley Symondson
| image         =
| caption       =
| birth_date    = {{Birth date|df=yes|1897|3|27}}
| death_date    = {{Death date and age|df=yes|1975|5|1|1897|3|27}}
| birth_place   = [[Sutton, Surrey]], England
| death_place   =
| placeofburial_label =
| placeofburial =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = United Kingdom
| branch        = British Army<br/>Royal Air Force
| serviceyears  = 1914–1919<br/>1921<br/>1939–1943
| rank          = Captain
| unit          = [[Honourable Artillery Company]]<br/>[[Glamorgan Yeomanry]]<br/>[[No. 29 Squadron RAF|No. 29 Squadron RFC]]<br/>[[No. 66 Squadron RAF]]
| commands      =
| battles       = World War I<br/>{{*}}[[Sinai and Palestine Campaign]]<br/>{{*}}[[Western Front (World War I)|Western Front]]<br/>{{*}}[[Italian Front (World War I)|Italian Front]]<br/>World War II
| awards        = [[Military Cross]]<br/>Silver [[Medal of Military Valor]] (Italy)
| relations     =
| laterwork     =
}}
Captain '''Francis Stanley Symondson''' {{post-nominals|country=GBR|MC}} (27 March 1897 – 1 May 1975) was a British World War I [[flying ace]] credited with [[List of World War I aces credited with 11–14 victories|13 confirmed aerial victories]]. He survived over three years of ground warfare and overcame early setbacks as a fighter pilot on the [[Western Front (World War I)|Western Front]] to become an ace in Italy.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/symondson.php |title=Francis Stanley Symondson |work=The Aerodrome |year=2016 |accessdate=26 February 2016}}</ref>

==Early life==
Symondson was born in [[Sutton, Surrey]], the second of three sons born to Stanley Vernon Symondson, a [[ship broker]], and his wife Jesse Kate (née Uridge). The census of March 1901 found him boarding in [[Margate]], Kent.<ref name="uridge.org">{{cite web |url=http://www.uridge.org/Euridge_Uridge-p/p144.htm |title=Francis Stanley Symondson |first=Teresa Pask |last=Euridge |work=Uridge, Euridge: One-Name Study Narratives |year=2016 |accessdate=26 February 2016}}</ref><ref>{{cite web |url=http://www.uridge.org/Euridge_Uridge-p/p57.htm#i1408 |title=Stanley Vernon Symondson |first=Teresa Pask |last=Euridge |work=Uridge, Euridge: One-Name Study Narratives |year=2016 |accessdate=26 February 2016}}</ref> Symondson first flew before the war in a [[Blériot Aéronautique|Bleriot]] aircraft with [[Frank Goodden]] in June 1914.<ref name="FlightBio">{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200765.html?search=symondson |title=De Havilland "Gipsy Moth" |issue=1178 |volume=XXIII |page=719 |journal=[[Flight International|Flight]] |date=24 July 1931 |accessdate=26 February 2016}}</ref>

==World War I service==
Symondson served three and a half years in the infantry before transferring to the [[Royal Flying Corps]].{{sfnp|Shores|Franks|Guest|1990|p=356}} He first served as a [[Trumpeter (rank)|trumpeter]] in the [[Honourable Artillery Company]], before being commissioned as a second lieutenant in the 25th (County of London) Cyclist Battalion, [[The London Regiment]], on 18 March 1915.<ref>{{London Gazette |date=16 March 1915 |supp=yes |issue=29103 |startpage=2717}}</ref> He later transferred to the [[Glamorgan Yeomanry]] ([[Welch Regiment]]), and was serving in Egypt<ref name="FlightBio"/> when he was seconded to the [[Royal Flying Corps]], in which he was appointed a flying officer on 23 May 1917.<ref>{{London Gazette |date=7 August 1917 |supp=yes |issue=30221 |startpage=8088 |nolink=yes}}</ref> He was promoted to lieutenant on 1 July.<ref>{{London Gazette |date=11 December 1917 |supp=yes |issue=30425 |startpage=13043 |nolink=yes}}</ref>

He was posted to [[No. 29 Squadron RAF|No. 29 Squadron]] in France on 4 September 1917. He crashed three of the squadron's [[Nieuport]]s in the next 16 days, and was sent back to England for further training. He was then posted to Italy to join [[No. 66 Squadron RAF|No. 66 Squadron]] as a [[Sopwith Camel]] pilot. On 7 March 1918, he set fire to a [[kite balloon]] at [[Chiarano]] for his first victory. It was the beginning of a string of a dozen enemy losses, as Symondson destroyed another balloon and ten aircraft by 28 August 1918. On 15 September, he drove down an [[Austrian-Hungarian]] [[Berg D.I]] out of control for his thirteenth win.{{sfnp|Shores|Franks|Guest|1990|pp=356–357}} The following day, his [[Military Cross]] was gazetted. The citation read:
:Lieutenant Francis Stanley Symondson, Yeomanry and Royal Air Force.
::"For conspicuous gallantry and devotion to duty. In two months he destroyed five enemy machines and one enemy kite balloon."<ref>{{London Gazette |date=13 September 1918 |supp=yes |issue=30901 |startpage=11023 |nolink=yes}}</ref>

In November 1918 he was awarded the Silver [[Medal for Military Valour]] by the Italian government.<ref>{{London Gazette |date=1 November 1918 |supp=yes |issue=30989 |startpage=12978 |nolink=yes}}</ref>

Symondson's victories included an observation balloon set on fire, another destroyed, two [[Albatros D.V]] fighters shot down in flames, seven other opposing fighters destroyed, an enemy reconnaissance aircraft destroyed, and another driven down.{{sfnp|Shores|Franks|Guest|1990|p=357}}

===List of aerial victories===
{| class="wikitable" style="font-size:90%;"
|-
|+Combat record<ref name="theaerodrome"/>
|-
!No.
! width="125" |Date/Time
! width="125" |Aircraft/<br/>Serial No.
! width="125" |Opponent
! width="125" |Result
! Location
|-
|1 || 7 March 1918<br />@ 1015 || [[Sopwith Camel]]<br />(B2445) || Balloon || Destroyed in flames || [[Chiarano]]
|-
|2 || 16 March 1918<br />@ 1200 || Sopwith Camel<br />(B2445) || [[Berg D.I]] || Destroyed || Col la Parada
|-
|3 || 30 March 1918<br />@ 1315 || Sopwith Camel<br />(B7353) || [[Albatros D.III]] || Destroyed || Mt. Maletto
|-
|4 || 4 April 1918<br />@ 0915 || Sopwith Camel<br />(B7353) || [[Albatros D.V]] || Destroyed || [[Brenta River]] at [[Cismon del Grappa|Cismon]]
|-
|5 || rowspan="2"|17 April 1918<br />@ 1415–1425 || rowspan="2"|Sopwith Camel<br />(B7353) || Albatros D.III || Destroyed in flames || rowspan="2"|South of Giacomo
|-
|6 || Albatros D.III || Destroyed in flames 
|-
|7 || rowspan="2"| 6 May 1918<br />@ 1040–1042 || rowspan="2"|Sopwith Camel<br />(B7353) || Albatros D.III || Destroyed || rowspan="2"| [[Motta di Livenza|Motta]]
|-
|8 || Albatros D.III || Destroyed 
|-
|9 || 6 June 1918<br />@ 1225 || Sopwith Camel<br />(D1912) || Albatros D.V || Destroyed || Zangetti
|-
|10 || 15 June 1918<br />@ 0810 || Sopwith Camel<br />(D9406) || Albatros D.V || Destroyed || Val d'Assa
|-
|11 || 13 August 1918<br />@ 0935 || Sopwith Camel<br />(D9390) || Balloon || Destroyed || West of [[Conegliano]]
|-
|12 || 28 August 1918<br />@ 1705 || Sopwith Camel<br />(D9390) || Aviatik C || Destroyed || South of [[Feltre]]
|-
|13 || 15 September 1918<br />@ 0830 || Sopwith Camel<br />(E1577) || Berg D.I || Out of control || North-east of Feltre
|-
|}

==Inter-war career==
Symondson was transferred to the RAF unemployed list on 6 June 1919.<ref>{{London Gazette |date=18 July 1919 |issue=31463 |startpage=9139 |nolink=yes}}</ref> He was briefly restored to the active list as a [[flying officer]] for temporary duty between 9 April<ref>{{London Gazette |date=3 May 1921 |issue=32311 |startpage=3544 |nolink=yes}}</ref> and 5 June 1921.<ref>{{London Gazette |date=24 June 1921 |issue=32368 |startpage=5016 |nolink=yes}}</ref> On 26 June 1924 Symondson enlisted in the Territorial Army, losing his right to retain his RAF rank.<ref>{{London Gazette |date=2 September 1924 |issue=32970 |startpage=6580 |nolink=yes}}</ref>

By 1929, he was married to Betty Symondson; she was named to probate a will on 17 June 1929.<ref>{{London Gazette |date=18 October 1929 |issue=33544 |startpage=6656 |nolink=yes}}</ref>

He remained a recreational pilot throughout the 1930s. He was both entrant and pilot of the [[de Havilland Gipsy Moth|Gypsy Moth]] ''G-AARU'' during the [[King's Cup Race]] in June 1930,<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%200659.html?search=symondson |title=Competitors in the King's Cup Air Race, 5 July 1930 |issue=1120 |volume=XXII |page=627 |journal=Flight |date=13 June 1930 |accessdate=26 February 2016 }}</ref> but dropped out of the event en route.<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%200811.html?search=symondson |title=The King's Cup |issue=1124 |volume=XXII |page=771 |journal=Flight |date=11 July 1930 |accessdate=26 February 2016 }}</ref> He competed in the same event the following year, flying the same aircraft, sponsored by the [[Royal Aero Club]].<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200559.html?search=symondson |title=First Entries for the King's Cup |issue=1172 |volume=XXIII |page=521 |journal=Flight |date=12 June 1931 |accessdate=26 February 2016 }}</ref><ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200760.html?search=symondson |title=List of Entries for King's Cup Air Race |issue=1178 |volume=XXIII |page=714 |journal=Flight |date=24 July 1931 |accessdate=26 February 2016}}</ref> He was placed as high as fifth place at one point.<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200804.html?search=symondson |title=The King's Cup Race 1931 |issue=1179 |volume=XXIII |page=750 |journal=Flight |date=31 July 1931 |accessdate=26 February 2016 }}</ref> A month later, in July 1931, at the opening of Plymouth airport, he also flew [[aerobatics]] for His Royal Highness, The Prince of Wales.<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1931/1931%20-%200769.html?search=symondson |title=Another Municipal Venture |issue=1178 |volume=XXIII |page=723 |journal=Flight |date=24 July 1931 |accessdate=26 February 2016}}</ref> Symondson flew in Jubilee Week during May 1935.{{sfnp|Shores|Franks|Guest|1990|p=356}} As late as 1938, he was still flying and stunting a Gypsy Moth at an altitude of only {{Convert|200|ft}}.<ref>{{cite journal |url=http://www.flightglobal.com/pdfarchive/view/1938/1938%20-%202204.html?search=symondson |title=Devon Air Day |issue=1545 |volume=XXXIV |pages=96–97 |journal=Flight |date=4 August 1938 |accessdate=26 February 2016 }}</ref>

==World War II==
On 1 September 1939, two days before [[British and French declaration of war on Germany|Britain's declaration of war on Germany]], Symondson was commissioned into the Administrative and Special Duties Branch of the [[Royal Air Force Volunteer Reserve]],<ref>{{London Gazette |date=20 October 1939 |issue=34713 |startpage=7045 |nolink=yes}}</ref><ref>{{London Gazette |date=5 March 1940 |issue=34805 |startpage=1320 |nolink=yes}}</ref> serving as a [[flight lieutenant]] until finally resigning his commission on 21 June 1943.<ref>{{London Gazette |date=16 July 1943 |supp=yes |issue=36100 |startpage=3284 |nolink=yes}}</ref> He then served in the [[Air Transport Auxiliary]] into late 1945.<ref name="theaerodrome"/>

Francis Stanley Symondson died in [[Bridport]], Dorset, on 1 May 1975.<ref name="uridge.org"/>

==References==
;Notes
{{reflist|30em}}
;Bibliography
* {{cite book |ref=harv |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell F. |last3=Guest |title=Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=978-0-948817-19-9 |lastauthoramp=yes}}

{{DEFAULTSORT:Symondson, Francis}}
[[Category:1897 births]]
[[Category:1975 deaths]]
[[Category:People from Sutton, London]]
[[Category:Honourable Artillery Company soldiers]]
[[Category:London Regiment officers]]
[[Category:Glamorgan Yeomanry officers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:British World War I flying aces]]
[[Category:Recipients of the Military Cross]]
[[Category:Recipients of the Silver Medal of Military Valor]]
[[Category:Royal Air Force Volunteer Reserve personnel of World War II]]