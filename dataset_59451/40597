<!-- Deleted image removed: [[File:Kitchen Theatre Company by Dave Burbank Photography.tiff|thumb|Kitchen Theatre|Kitchen Theatre Company, Photo by Dave Burbank Photography]]  -->The '''Kitchen Theatre Company''' (KTC) is a [[non-profit]] professional theater company in [[Ithaca, New York]] that focuses on making “bold, intimate, and engaging"<ref>{{cite web|title=Ithaca Events.com|url=http://www.ithacaevents.com/venue/30-kitchen-theatre.html|publisher=Community Arts Partnership|accessdate=June 4, 2012}}</ref> theater. The Kitchen was founded in 1991<ref name="kitchentheatre">{{cite web|title=History of the KTC|url=http://www.kitchentheatre.org/history.htm|publisher=Kitchen Theatre Company|accessdate=June 4, 2012}}</ref> and is now in its 24th season. KTC is a member of the [[Theatre Communications Group]] and operates under a Small Professional Theater contract with the [[Actors’ Equity Association]].

==History==
Kitchen Theatre Company was founded in 1991 by Matt Tauber and Tim O’Brien, both [[Ithaca College]] students at the time. Their first production was [[Sam Shepard]]’s [[Buried Child]]. For the company’s first three seasons, it produced in several different venues in Ithaca and then moved to the historic [[Clinton House (Ithaca, New York)|Clinton House]] in downtown Ithaca.<ref name="kitchentheatre" /> Norm Johnson came on as artistic director for the fourth through seventh seasons. In the theater’s eighth season, the company’s current artistic director, [[Rachel Lampert]], stepped in.

In 2008, the company purchased a building at 417 West State Street in downtown Ithaca’s West End.<ref>{{cite web|last=Jones|first=Kenneth|title=Ithaca's Kitchen Theatre Company Buys New Home; 99-Seat House Will Open|url=http://www.playbilledu.com/news/article/123811-Ithacas-Kitchen-Theatre-Company-Buys-New-Home-99-Seat-House-Will-Open|publisher=Playbill.com|accessdate=June 4, 2012}}</ref> In 2010, the renovated building opened for the start of the company’s 20th season with an increased seating capacity of 99 seats, a high-efficiency heating and cooling system, increased stage space, multiple entrances, full accessibility, larger restrooms, and a new lobby with an art gallery space and refreshment counter.<ref>{{cite web|last=Post Editors|title=Walk-In Kitchen|url=http://theithacapost.com/2010/06/27/walk-in-kitchen/|publisher=Ithaca Post|accessdate=June 4, 2012}}</ref> The company received [[Leadership in Energy and Environmental Design|LEED]] certification through the [[United States Green Building Council]].<ref>{{cite web|last=Sharkey|first=John|title=Firsthand View of Green Building Practices|url=http://www.johnson.cornell.edu/Center-for-Sustainable-Global-Enterprise/News-Events/CSGE-Student-Article-Detail/ArticleId/730/Firsthand-View-of-Green-Building-Practices.aspx|publisher=Center for Sustainable Global Enterprise|accessdate=June 4, 2012}}</ref> in 2013.

==Mission==
The Kitchen’s mission statement says that:
“In its intimate performance space. the Kitchen Theatre Company (KTC) creates professional theater that challenges the intellect, excites the imagination, informs and entertains. KTC nurtures a community of diverse artists and brings excellent art to the community and beyond by:
* Developing and producing new plays, exploring established repertory and contributing to the field of American theater;
* Encouraging collaboration and offering a safe haven for experimentation;
* Providing programming that inspires young people, opens the door to newcomers, and speaks to a broad cross-section of our community; and
* Advancing a culture of theatergoing.<ref>{{cite web|title=http://www.kitchentheatre.org/mission.htm|url=http://www.kitchentheatre.org/mission.htm|publisher=Kitchen Theatre Company|accessdate=June 4, 2012}}</ref>”

==Productions==
{| class="wikitable"
|-
! 2014-2015 Season
|-
| ''The House'' by [[Brian Parks]]
|-
| ''[[Lonely Planet (play)|Lonely Planet]]'' by [[Steven Dietz]]
|-
| ''Sunset Baby'' by Dominique Morisseau
|-
| ''Count Me In'' by [[Rachel Lampert]]
|-
| ''A Body of Water'' by [[Lee Blessing]]
|-
| ''Solo Play Festival'' featuring Lorraine Rodriguez-Reyes, Darian Dauchan, Michelle Courtney Berry & Ryan Hope Travis
|-
| ''Swimming in the Shallows'' by [[Adam Bock]]
|-
| ''Thin Walls'' by Alice Eve Cohen
|}

Production highlights include:
* [[Rob Ackerman (playwright)|Rob Ackerman’s]] play ''Call Me Waldo'' premiered at the KTC in January 2012 and was produced later the same year in NYC by the Working Theatre.<ref>{{cite web|last=BWW News Desk|title=Rob Ackerman's CALL ME WALDO Opens 2/22|url=http://broadwayworld.com/article/Rob-Ackermans-CALL-ME-WALDO-Opens-222-20120202|publisher=Broadwayworld.com}}</ref>
* Kitchen Theatre Artistic Director [[Rachel Lampert]] and composer Larry Pressgrove (music director, ''[[title of show]]'') have written five musicals together, all of which premiered at the Kitchen Theatre.<ref>{{cite web|title=Larry Pressgrove|url=http://michaelcassara.net/people/larry-pressgrove|publisher=Michael Cassara Casting}}</ref> Their musical ''The Angle of the Sun'' premiered at the Kitchen and was then presented at the New York Musical Theatre Festival.<ref>{{cite web|last=Hetrick|first=Adam|title=The Angle of the Sun, with Broadway's Watkins, to Play NYMF|url=http://www.playbill.com/news/article/110356-The-Angle-of-the-Sun-with-Broadways-Watkins-to-Play-NYMF|publisher=Playbill.com|accessdate=June 14, 2012}}</ref>
* [[Rachel Lampert]]'s play ''The Soup Comes Last'' premiered at the Kitchen and then moved to the 59e59th street theater.<ref>{{cite web|last=Jones|first=Kenneth|title=West Side Story Staging in China Inspires Solo Show, The Soup Comes Last, Making NYC Premiere Sept. 29|url=http://www.playbill.com/news/article/88573-West-Side-Story-Staging-in-China-Inspires-Solo-Show-The-Soup-Comes-Last-Making-NYC-Premiere-Sept-29|publisher=Playbill.com|accessdate=June 14, 2012}}</ref> Her play ''Precious Nonsense'' premiered at the Kitchen and had its second production at Circle Bar-B Dinner Theater in Santa Barbara, CA.<ref>{{cite web|title=Precious Nonsense|url=http://www.circlebarbtheatre.com/past_shows/10_precious_nonsense/PRECNONv2.pdf|publisher=Circle Bar-B Dinner Theater|accessdate=June 14, 2012}}</ref>
* Four new plays by Brian Dykstra have had their first full productions at the Kitchen Theatre Company: ''Brian Dykstra Selling Out'' (2012)''The Two of You'' (2009), ''A Play on Words'' (2010, then played at 59E59 Street Theatres in the America’s Off Broadway Festival) and ''Strangerhorse'' (2007).<ref>{{cite web|last=Dykstra|first=Brian|title=Playwright|url=http://www.briandykstra.net/playwright.htm|accessdate=June 4, 2012}}</ref>
* ''The Drunken City'' by [[Adam Bock]] was commissioned by the KTC and had its world premiere in 2005.The New York City premiere was in 2008 at [[Playwrights Horizons]].<ref>{{cite web|last=Jones|first=Kenneth|title=Bock's Drunken City Makes NYC Premiere at Playwrights Horizons|url=http://www.playbill.com/news/article/116228-Bocks-Drunken-City-Makes-NYC-Premiere-at-Playwrights-Horizons|publisher=Playbill.com|accessdate=June 4, 2012}}</ref>
* First productions of plays by [[Rachel Axler]] (''Archaeology'', 2009<ref>{{cite web|title=SMUDGE Creative Team & Cast|url=http://www.womensproject.org/Smudge_creativeteam.html|publisher=Women's Project|accessdate=June 4, 2012}}</ref>) and [[Tanya Barfield]] (''Pecan Tan'', 2005<ref>{{cite web|title=Kitchen Theatre Presents Staged Documentary|url=http://www.lansingstar.com/index.php?option=com_content&task=view&id=4775&Itemid=289&bsb_midx=5|publisher=Lansing Star|accessdate=June 4, 2012}}</ref>).

==Achievements==
* 2014 Best Theatre Company (Best of Ithaca 2014).<ref>{{cite web|last=Chaisson|first=Bill|title=Best of Ithaca 2014|url=http://www.ithaca.com/special_sections/best-of-ithaca-readers-vote-in-the-best-of-entertainment/article_f8ae489c-4426-11e4-abaf-63fe6ba539c8.html|publisher=Ithaca.com|accessdate=March 5, 2014}}</ref>
* 2011 Best Theatre Company (Best of Ithaca 2011).<ref>{{cite web|last=Montana|first=Rob|title=Best of Ithaca 2011|url=http://www.ithaca.com/visit_ithaca/best_of_ithaca/article_1d21e1ca-eaac-11e0-b3c4-001cc4c002e0.html|publisher=Ithaca.com|accessdate=June 4, 2012}}</ref>
* 2008 [[Syracuse, New York|Syracuse]] Live Theater Award, Best Production of the Summer Season (''Souvenir'' by Stephen Temperley, directed by Sara Lampert Hoover<ref>{{cite web|last=Staff|title=Tales from the SALT City|url=http://www.syracusenewtimes.com/newyork/article-3086-tales-from-the-salt-city.html|publisher=Syracuse New Times|accessdate=June 4, 2012}}</ref>).
* Since 2007, the Kitchen Theatre Company has received operating support from the [[Shubert Organization]],<ref>{{cite web|title=Grant Programs|url=http://www.shubertfoundation.org/grantprograms/theatre.asp#K|publisher=The Shubert Foundation|accessdate=June 4, 2012}}</ref> a national foundation dedicated to sustaining and advancing the live performing arts in the United States.
* 2006 Best Small Business of the Year Award, [[Tompkins County]] Chamber of Commerce.<ref>{{cite web|last=Wood|first=Wendy|title=Kitchen Awarded Best Small Business Award|url=http://www.lansingstar.com/entertainment-archive/948-kitchen-awarded-best-small-biz|publisher=Lansing Star|accessdate=June 4, 2012}}</ref> The Kitchen was the first not-for-profit recipient of this award.
* 2005 Syracuse Live Theatre Award, Best Actor in a Season (Karl Gregory in ''Fully Committed'' by [[Becky Mode]]<ref>{{cite web|last=Gregory|first=Karl|title=Resume|url=http://www.karlgregory.com/Resume.html|accessdate=June 4, 2012}}</ref>).
* 2005 Syracuse Live Theater Award, Best Actress of the Summer Season ([[Rachel Lampert]] in ''The Soup Comes Last''<ref>{{cite web|last=Lampert|first=Rachel|title=Rachel Lampert|url=http://www.linkedin.com/profile/view?id=12235499&authType=NAME_SEARCH&authToken=nZi-&locale=en_US&srchid=359f575a-ffdb-4303-a129-803046866ef1-0&srchindex=1&srchtotal=9&goback=%2Efps_PBCK_*1_Rachel_Lampert_*1_*1_*1_*1_*2_*1_Y_*1_*1_*1_false_1_R_*1_*51_*1_*51_true_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2_*2&pvs=ps&trk=pp_profile_name_link|publisher=Linkedin.com|accessdate=June 4, 2012}}</ref>).

==References==
{{reflist|2}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

==External links==
* [http://www.kitchentheatre.org/ Official website]
* [http://www.facebook.com/KitchenTheatre Facebook]
* [https://twitter.com/KitchenTheatre Twitter]
* [http://kitchentheatre.wordpress.com/ Blog]

{{Ithaca, New York}}

[[Category:Theatre companies in New York]]