{{good article}}
{{Infobox hurricane season
|Basin=Atl
|Year=1860
|Track=1860 Atlantic hurricane season summary.jpg
|First storm formed=August 8, 1860
|Last storm dissipated=October 24, 1860
|Strongest storm name=[[#Hurricane One|One]]
|Strongest storm winds=115
|Strongest storm pressure=950
|Total depressions=7
|Total storms=7
|Total hurricanes=6
|Fatalities=At least 60
|Damages=1.25
|five seasons=[[1858 Atlantic hurricane season|1858]], [[1859 Atlantic hurricane season|1859]], '''1860''', [[1861 Atlantic hurricane season|1861]], [[1862 Atlantic hurricane season|1862]]}}
During the 1860 [[Atlantic hurricane season]], three severe hurricanes struck [[Louisiana]] and the [[Gulf Coast of the United States]] within a period of seven weeks. The season effectively began on August 8 with the formation of a tropical cyclone in the eastern [[Gulf of Mexico]], and produced seven known tropical storms and hurricanes until the dissipation of the last known system on October 24. Six of the seven storms were strong enough to be considered hurricanes on the modern-day [[Saffir&ndash;Simpson Hurricane Scale]], of which four attained Category 2 status and one attained Category 3 major hurricane strength. The first hurricane was the strongest in both winds and pressure, with peak winds of {{convert|125|mph|km/h}} and a barometric pressure of {{convert|950|mbar|inHg|lk=on}}. Until contemporary reanalysis discovered four previously unknown tropical cyclones that did not affect land, only three hurricanes were known to have existed; all three made landfall in Louisiana, causing severe damage.

The first two hurricanes to strike the Gulf Coast—in August and September, respectively—caused significant inundation of low-lying and coastal communities, inflicting severe damage and killing dozens of people. In some cases, flood waters were more than {{convert|12|ft|m|abbr=on}} deep, and the center of destruction shifted slightly with each storm. Sugar cane crops were destroyed by these two systems as well as the succeeding storm in early October. Property and infrastructure suffered with all three events. With the third storm that made landfall, extreme winds blasted the city of [[New Orleans]] and surrounding areas. All other storms remained away from land with no effects except on shipping.

==Methodology==
Prior to the advent of modern tropical cyclone tracking technology, notably satellite imagery, many hurricanes that did not affect land directly went unnoticed, and storms that did affect land were not recognized until they made landfall. As a result, information on older hurricane seasons was often incomplete. Modern-day efforts have been made and are still ongoing to reconstruct the tracks of known hurricanes and to identify initially undetected storms. In many cases, the only evidence that a hurricane existed was reports from ships in its path. However, judging by the direction of winds experienced by ships, and their location in relation to the storm, it is possible to roughly pinpoint the storm's center of circulation for a given point in time. This is the manner in which four of the seven known storms in the 1860 season were identified by hurricane expert José Fernández Partagás's reanalysis of hurricane seasons between 1851 and 1910. Partagás also extended the known tracks of three other hurricanes previously identified by scholars. The information Partagás and his colleague uncovered was largely adopted by the [[National Oceanic and Atmospheric Administration]]'s [[Atlantic hurricane reanalysis]] in their updates to the [[HURDAT|Atlantic hurricane database]] (HURDAT), with some slight adjustments. HURDAT is the official source for such hurricane data as track and intensity, although due to a sparsity of available records at the time the storms existed, listings on some storms are incomplete.<ref name="HURDAT"/><ref name="meta"/>

Although extrapolated peak [[maximum sustained wind]]s based on whatever reports are available exist for every storm in 1860, estimated minimum central barometric air pressure listings are only present for the three storms that made landfall in the United States.<ref>{{cite web|author=Hurricane Research Division|year=2011|title=Continental United States Tropical Storms: 1851–1930, 1983–2010|publisher=National Oceanic and Atmospheric Administration|accessdate=July 19, 2011|url=http://www.aoml.noaa.gov/hrd/hurdat/uststorms1851-1930&1983-2010-may11.html}}</ref> As the three landfalling storms moved inland, information on their meteorological dissipation was limited. As a result, the intensity of these storms after landfall and until dissipation is based on an inland decay model developed in 1995 to predict the deterioration of inland hurricanes.<ref name="meta"/>

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea = top:10 bottom:80 right:20 left:20
Legend = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period = from:01/08/1860 till:01/11/1860
TimeAxis = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/08/1860

Colors =
 id:canvas value:gray(0.88)
 id:GP value:red
 id:TD value:rgb(0.38,0.73,1) legend:Tropical_Depression_=_&lt;39_mph_(0–62_km/h)
 id:TS value:rgb(0,0.98,0.96) legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
 id:C1 value:rgb(1,1,0.80) legend:Category_1_=_74–95_mph_(119–153_km/h)
 id:C2 value:rgb(1,0.91,0.46) legend:Category_2_=_96–110_mph_(154–177_km/h)
 id:C3 value:rgb(1,0.76,0.25) legend:Category_3_=_111–130_mph_(178–209-km/h)
 id:C4 value:rgb(1,0.56,0.13) legend:Category_4_=_131–155_mph_(210–249_km/h)
 id:C5 value:rgb(1,0.38,0.38) legend:Category_5_=_≥157_mph_(≥252_km/h)

Backgroundcolors = canvas:canvas

BarData =
 barset:Hurricane
 bar:Month

PlotData=
 barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
 from:08/08/1860 till:16/08/1860 color:C3 text:"Hurricane #1"
 from:24/08/1860 till:26/08/1860 color:C2 text:"Hurricane #2"
 from:11/09/1860 till:11/09/1860 color:C1 text:"Hurricane #3"
 from:11/09/1860 till:16/09/1860 color:C2 text:"Hurricane #4"
 from:18/09/1860 till:21/09/1860 color:TS text:"Tropical storm #5"
 from:30/09/1860 till:03/10/1860 color:C2 text:"Hurricane #6"
 from:20/10/1860 till:24/10/1860 color:C2 text:"Hurricane #7"

 bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
 from:01/08/1860 till:01/09/1860 text:August
 from:01/09/1860 till:01/10/1860 text:September
 from:01/10/1860 till:01/11/1860 text:October
TextData =
 pos:(570,30)
 text:"(From the"
 pos:(617,30)
 text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 1 track.png
|Formed=August 8
|Dissipated=August 16
|1-min winds=110
|Pressure=950
}}
The first known storm of the season—which would also become the strongest—is listed in the hurricane database as having formed on August 8 in the eastern [[Gulf of Mexico]], just offshore the west coast of the [[Florida]] peninsula, although it is possible the hurricane was related to a heavy gale encountered by a ship on August 5 during its voyage from Havana, Cuba to New York City. The cyclone drifted southwest for three days, gradually intensifying to attain a strength corresponding to Category 3 major hurricane status on the modern-day Saffir-Simpson Hurricane Scale. Beginning on August 11, the hurricane slowly curved to the north, then gained an easterly component to its movement, and was at its peak intensity with maximum sustained winds of {{convert|130|mph|km/h|abbr=on}} about midday.<ref name="HURDAT"/><ref name="Partagas1"/>

Coming ashore at an [[Angle#Types of angles|"oblique" angle]], the storm crossed the coast at modern [[Burrwood, Louisiana]], and traversed the [[Mississippi River Delta]]. The storm made landfall in [[Mississippi]] between [[Biloxi, Mississippi|Biloxi]] and Pascagoula early on August 12. The hurricane quickly weakened as it continued inland and turned toward the east, likely deteriorating below tropical storm status while over southern Georgia, although because the hurricane database does not utilize tropical depression status for storms before 1871, it is listed as maintaining tropical storm intensity as it crossed the southeastern U.S.; by August 14, it had remerged into the western Atlantic Ocean. Ship reports from the vicinity of the system indicate it regained some strength once over open waters, although it only persisted a couple more days before it disappeared on records.<ref name="HURDAT">{{cite web|author=Hurricane Specialists Unit|publisher=National Hurricane Center|year=2009|accessdate=July 15, 2011|title=Easy to Read HURDAT 1851–2008|url=http://www.aoml.noaa.gov/hrd/hurdat/easyread-2009.html| archiveurl= https://web.archive.org/web/20110628231915/http://www.aoml.noaa.gov/hrd/hurdat/easyread-2009.html| archivedate= 28 June 2011 <!--DASHBot-->| deadurl= no}}</ref><ref name="Partagas1">Partagás, pp. 1–2</ref>

Storm conditions battered New Orleans, Louisiana, on August 11, with winds of up to roughly {{convert|50|mph|km/h|abbr=on}} and heavy rainfall.<ref name="Partagas1"/> The hurricane wrought a great deal of damage throughout southeastern Louisiana. At least 35 to 40 people drowned when a low-lying community at the eastern end of [[Lake Pontchartrain]] was inundated with flood waters up to a depth of {{convert|12|ft|m}}, a result of intense and persistent winds generating a significant [[storm surge]].<ref name="Partagas1"/> It was reported that in the town "there is hardly a house remaining".<ref name="Ludlum">Ludlum, p. 174</ref> A railroad wharf near the lake was largely destroyed, and another settlement called [[Milneburg, New Orleans|Milneburg]] was flooded; residents were rescued by boat. In [[Plaquemines Parish, Louisiana|Plaquemines Parish]], where vast areas of land were left underwater, the hurricane was the most severe since 1812. At least 20 additional people drowned, although it was suspected that since up to 200 fishermen along the [[Mississippi River]] would have had extreme difficulty finding shelter amid the rapidly rising waters, the death toll was likely substantially higher. The storm flattened sugarcane fields across the parish: "the knowing ones will say that two thousand hogshead of sugar less will be made here", according to a post-storm account. Monetary losses from the destruction totaled over $250,000.<ref name="Ludlum"/> Rice and corn crops were also ruined, and 300 head of cattle drowned in the flood on Cat Island.<ref name="DR"/>

At [[La Balize, Louisiana|La Balize]] (now known as [[Pilottown, Louisiana|Pilottown]]), ominous weather conditions on the night of August 9 preceded the onset of gathering clouds and "a violent storm", characterized by building seas and torrents of rain and wind such that water was driven under roof shingles, "leaving not a dry spot to lay our heads".<ref name="Ludlum2">Ludlum, pp. 174–175</ref> Sea water swamped the town overnight on August 10, and it was not until around noon when the storm let up and waters receded with a shift in winds. Property damage was widespread and many trees were blown down. The hurricane beached several watercraft, destroyed wharves, and inflicted losses that "can hardly by estimated", but were initially judged at up to $10,000. No lives were lost in Balize.<ref name="Ludlum2"/> Further down the storm's track, Biloxi, Mississippi, experienced a gale beginning on the morning of August 11 and lasting until early the next morning. Tides ran {{convert|10|ft|m|abbr=on}} above average at Biloxi, and in [[Mobile, Alabama]], residents also found wind-driven rain seeping "through cracks and crevices not known before to exist" in dwellings "always waterproof to ordinary showers".<ref name="Ludlum2"/> Waters rose enough to cause minor inundations, but not nearly at the same severity as those that occurred in other areas. Heavy storm conditions extended as far east as [[Pensacola, Florida]], where {{convert|3.03|in|mm|abbr=on}} of rain fell, accompanied by frequent thunder and lightning.<ref name="Ludlum2"/> The overall death toll from the hurricane was at least 47.<ref name="DR"/>
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 2 track.png
|Formed=August 24
|Dissipated=August 26
|1-min winds=90
|Pressure=
}}
Over a week after the dissipation of the first system, another tropical storm formed to the east of Florida on August 23. The storm tracked generally northeastward, roughly paralleling the [[East Coast of the United States]] as it peaked with sustained winds of {{convert|105|mph|km/h|abbr=on}} on August 25. These winds would have made it a Category 2 hurricane on the Saffir-Simpson Hurricane Scale. Continuing in the same general direction, the hurricane began to weaken shortly after attaining peak intensity, and the last official data point for the storm places it east of [[New England]] on August 26.<ref name="HURDAT"/> The hurricane was undetected until modern-day reanalysis reconstructed its path based on reports from ships in the area.  On August 24, the vessel ''Mary Rusell'' encountered the storm and sustained some damage.  A day later, a heavy southeasterly gale left the vessel ''Rocius'' in a "sinking condition"; her captain and crew were rescued by the crew of the ''Zurich''.<ref name="Partagas23">Partagás, pp. 2–3</ref>
{{clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 3 track.png
|Formed=September 11
|Dissipated=September 11
|1-min winds=70
|Pressure=
}}
The second storm discovered in contemporary reanalysis was identified based on meteorological reports from a single ship, the ''Ocean Spray'', which experienced a northeast gale shifting to northwest on September 11. Given the intensity of the winds, the cyclone is estimated to have been a Category 1 hurricane, although due to a nearly complete lack of information on the system, its track is unknown with the exception of a single set of coordinates well to the southeast of [[Newfoundland (island)|Newfoundland]].<ref name="HURDAT"/><ref name="Partagas3">Partagás, p. 3</ref>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 4 track.png
|Formed=September 11
|Dissipated=September 16
|1-min winds=90
|Pressure=969
}}
A hurricane in the middle of September affected some of the same areas along the northern coast of the Gulf of Mexico as the August hurricane. It was first detected on the evening of September 10 in the southeastern Gulf, although its track may be far off from the actual storms movement due to a lack of available information. Its first data point in the hurricane database lists it as possessing sustained winds of {{convert|100|mph|km/h|abbr=on}}, placing it at Category 2 intensity on the Saffir-Simpson Hurricane Scale.<ref name="HURDAT"/> The storm followed a broad northward curve through the central Gulf of Mexico, making landfall along the Mississippi River Delta of Louisiana on the night of September 14.<ref name="Partagas35">Partagás, pp. 3, 5</ref> It continued toward the north-northeast, dissipating on September 16.<ref name="HURDAT"/>

Once again, Balize, Louisiana was directly in the hurricane's path, and suffered heavily as a consequence. The storm lasted from the afternoon on Friday, September 14 to late Saturday morning. Local reports indicate heavy rain and large hail fell in association with the cyclone, and in Balize, nearly every structure was destroyed; any buildings that withstood the initial winds were swept away by the ensuing inundation. Floodwaters killed ten people at one location alone in Plaquemines Parish. Residents of the hardest-hit communities were forced to wade to safety, in some cases through water shoulder-high. Northerly winds drove water from Lake Pontchartrain that had already been built up by northeasterly winds southward over small fishing and resort towns along the southern bank of the lake. Sheds, wharves, and bathhouses along that section of the lake were destroyed.<ref name="Ludlum176">Ludlum, p. 176</ref> Several people drowned in flood waters that covered lower Plaquemines Parish.<ref name="DR">{{cite web|author=Roth, David|title=Louisiana hurricane history|year=|publisher=Hydrometeorological Prediction Center|page=18|accessdate=July 19, 2011|url=http://www.srh.noaa.gov/images/lch/tropical/lahurricanehistory.pdf}}</ref> The hurricane drove several ships, including the steamer ''Galveston'', ashore.<ref name="Partagas35"/>

An article published in ''[[The Times-Picayune]]'' contrasted this hurricane with the cyclone of August, noting that the two storms were of similar intensity, and although the September system did not last as long in any one location, hydrology played a more significant role: "in August the swamps were nearly dry and the waters from the lake found a natural outlet; whereas, yesterday, the swamp being full, the water rose in the streets of Milneburgh and covered the railroad track for some distance."<ref name="Partagas35"/> In New Orleans proper, heavy rain and gusty winds were reported, but no flooding was observed. The worst damage from the hurricane most likely occurred in Biloxi, where the coastline itself was altered by up to {{convert|20|to|30|ft|m|abbr=on}} and the lighthouse was swept out to sea. A hotel collapsed amid the disaster, killing at least one person, and loose debris covered the town.<ref name="Ludlum176"/> At [[Pascagoula, Mississippi|East Pascagoula, Mississippi]], the sea rose well beyond that of any storm in around 40 years, and nearby a wharf was totally destroyed. Initial estimates placed total damage in the area at $40,000. The storm was just as severe in Mobile, Alabama, and the majority of the $1 million in losses there was from lost cotton stored on flooded wharves.<ref name="Ludlum176"/> At Pascagoula, water reportedly rose {{convert|7|ft|m|abbr=on}} in 20 minutes.<ref name="Partagas35"/>
{{Clear}}

===Tropical Storm Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic tropical storm 5 track.png
|Formed=September 18
|Dissipated=September 21
|1-min winds=60
|Pressure=
}}
The fifth known storm of the year was yet another cyclone first identified in post-season reanalysis. Forming on September 18 well to the north of [[Puerto Rico]], efforts made to piece together wind observations from ships reveal a track that curved toward the northwest, passing about midway between the U.S. East Coast and [[Bermuda]] before heading northeast. Based on available information, the cyclone is estimated to have remained at tropical storm intensity for its entire existence. It is last noted southeast of [[New England]] on September 21.<ref name="HURDAT"/><ref name="Partagas5">Partagás, p. 5</ref>

{{Clear}}

===Hurricane Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 6 track.png
|Formed=September 30
|Dissipated=October 3
|1-min winds=90
|Pressure=969
}}
The third and final hurricane known to have existed prior to reanalysis formed in the south-central [[Gulf of Mexico]].<ref name="HURDAT"/> Ludlum (1963) describes the storm along the Gulf Coast, but a reconstruction of its track further south, including its intensification, was made possible by ship reports. In particular, a schooner along the coast of the [[Yucatán Peninsula]] encountered stormy conditions around September 30 with southeasterly winds and was driven northward through the Gulf. The storm moved toward the north for a few days,<ref name="Partagas6">Partagás, p. 6</ref> reaching its peak with winds of {{convert|105|mph|km/h|abbr=on}} early on October 2.<ref name="HURDAT"/> It likely made landfall on the [[Atchafalaya Basin]] around noon on October 2 before passing west of New Orleans,<ref name="Partagas6"/> weakening to a tropical storm before moving into central Mississippi.<ref name="HURDAT"/> Strong winds at New Orleans lasted more than 24 hours.<ref name="Partagas6"/>

The New Orleans area was hit harder than in the two prior storms to strike the Gulf Coast; the most densely populated areas were now located in the eastern semicircle of the storm, which is one of the most intense quadrants. Residents in Plaquemines Parish, Louisiana reported the storm to be the worst wind event they had ever experienced. Coinciding with the beginning of the annual season of sugar production, the storm destroyed vast fields of sugar cane south and southwest of New Orleans and flattened many sheds used to store farming equipment. Winds throughout the region of south-central Louisiana inflicted "unparallelled destruction", causing major structural damage. It was reported to be nearly impossible to walk through the streets of the city at the height at the storm;<ref name="Ludlum177"/> the winds came with over {{convert|5|in|mm|abbr=on}} of rain. Thirteen people died in the hurricane at New Orleans.<ref name="DR"/> A five-story brick building succumbed to the force of the hurricane and crashed onto the city below; caught in its path were two other buildings. This particular incident killed two people. Many other buildings suffered damaged roofs and broken glass, and telegraph and police wires were brought down.<ref name="Ludlum177">Ludlum, p. 177</ref>

The easterly winds created a storm surge on Lake Pontchartrain and inundated eastern and northern areas of New Orleans. The dynamics of the flood were considered "unprecedented", having come within {{convert|0.75|mi|km|abbr=on}} of the Mississippi River. Flood waters rose until early on October 4, when they slowly began to recede. The Jackson Railroad, on the western and northwestern shore of the lake, was flooded up to a depth of {{convert|5|ft|m|abbr=on}}, and {{convert|11|mi|km|abbr=on}} of track were washed out. Numerous families in residents near the railroad were forced to leave their homes and seek shelter upon the onset of rising waters. Numerous coal boats and steamboats were swamped and sunk in and around [[Baton Rouge, Louisiana|Baton Rouge]], which experienced its first severe hurricane strike in many years. Countless trees were uprooted along the Mississippi, and crop damage continued its prevalence going further north.<ref name="Ludlum178">Ludlum, p. 178</ref> [[Natchez, Louisiana|Natchez]] saw the highest winds in the town since a tornado in 1840, and although the hurricane struck further west than the previous two, gale winds still extended eastward to Pensacola.<ref name="DR"/>
{{Clear}}

===Hurricane Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1860 Atlantic hurricane 7 track.png
|Formed=October 20
|Dissipated=October 24
|1-min winds=90
|Pressure=
}}
The final known storm of the season existed in late October, and followed roughly the same track as Tropical Storm Five, forming in the southwestern Atlantic basin and curving northward between the U.S. and Bermuda. Its track is only known between October 20 and October 24,<ref name="HURDAT"/> and it was also not recognized until subsequent reanalysis. It was determined to have attained the equivalence of Category 2 hurricane intensity based on reports from many ships. Its last data point places it southeast of [[Nova Scotia]] early on October 24.<ref name="meta">{{cite web|author=Hurricane Research Division|year=2011|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration|accessdate=June 4, 2011|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html| archiveurl= https://web.archive.org/web/20110604063810/http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html| archivedate= 4 June 2011 <!--DASHBot-->| deadurl= no}}</ref> The hurricane is thought to have only been a threat to shipping interests, and numerous ships encountered dangerous conditions in association with the cyclone. A few vessels sustained significant damage: the ''Gondar'' experienced winds "with such violence that the top gallant mast was broken off by the cap, the maintopsail was blown to pieces and the ship thrown on her beam ends."<ref name="Partagas7">Partagás, p. 7</ref>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of Atlantic hurricane seasons]]
{{clear}}

==Notes==
{{Reflist|35em}}

== References ==
{{commons category|1860 Atlantic hurricane season}}
* {{cite book|author=[[David M. Ludlum|Ludlum, David McWilliams]]|title=Early American hurricanes, 1492–1870|year=1963|publisher=[[American Meteorological Society]]}}
* {{cite web|author=Partagás, José Fernández|publisher=National Oceanic and Atmospheric Administration|year=2003|title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources: Year 1860|accessdate=July 15, 2011|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1858-1864/1860.pdf}}

{{TC Decades|Year=1860|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1866 Atlantic Hurricane Season}}
[[Category:Atlantic hurricane seasons]]
[[Category:1860–1869 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]