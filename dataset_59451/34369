{{about|World Scouting's Wood Badge program|the Wood Badge program in the United States|Wood Badge (Boy Scouts of America)}}
{{Use mdy dates|date=April 2012}}
{{infobox WorldScouting
| type = Award
| image = Scout woodbadge beads.jpg
| caption = Wood Badge [[bead]]s on top of the 1st Gilwell Scout Group [[neckerchief]].
| name = Wood Badge
| founder = [[Robert Baden-Powell, 1st Baron Baden-Powell|Baden-Powell]]
| country = All
| award-for = Completion of leadership training
| f-date = 1919
| members = > 100,000
}}

'''Wood Badge''' is a [[Scouting]] [[leadership]] programme and the related award for [[Scout Leader|adult leaders]] in the programmes of [[List of World Organization of the Scout Movement members|Scout associations]] throughout the world. Wood Badge [[Course (education)|courses]] aim to make [[Scout Leader|Scouters]] better leaders by teaching advanced leadership skills, and by creating a [[Human bonding|bond]] and [[Personal commitment|commitment]] to the Scout movement. Courses generally have a combined classroom and practical [[Wilderness|outdoors]]-based phase followed by a Wood Badge ''ticket'', also known as the [[project]] phase. By "working the ticket", participants put their newly gained experience into practice to attain ticket goals aiding the Scouting movement. The first Wood Badge training was organized by [[Francis Gidney|Francis "Skipper" Gidney]] and lectured at by [[Robert Baden-Powell, 1st Baron Baden-Powell|Robert Baden-Powell]] and others at [[Gilwell Park]] (United Kingdom) in September 1919. Wood Badge training has since spread across the world with international variations.

On completion of the course, participants are awarded the Wood Badge [[bead]]s to recognize significant achievement in leadership and direct service to young people. The pair of small wooden beads, one on each end of a leather thong (string), is worn around the neck as part of the Scout [[uniform]]. The beads are presented together with a [[taupe]] [[neckerchief]] bearing a [[tartan]] patch of the [[Clan MacLaren|Maclaren clan]], honoring [[William de Bois Maclaren]], who donated the £7000 to purchase Gilwell Park in 1919 plus an additional £3000 for improvements to the house that was on the estate. The neckerchief with the braided leather [[woggle]] (neckerchief slide) denotes the membership of the ''1st Gilwell Scout Group'' or ''Gilwell Troop 1''. Recipients of the Wood Badge are known as ''Wood Badgers'' or ''Gilwellians''.

== Scout leader training course ==

=== History ===
[[File:First Wood Badge training Gilwell Park September 1919.jpg|thumb|300px|right|First Wood Badge training at [[Gilwell Park]]]]
<!-- FAIR USE of 'wb course first.jpg': see image description page at [[:Image:wb course first.jpg]] for rationale -->
Soon after founding the [[Scouting|Scout movement]], [[Robert Baden-Powell, 1st Baron Baden-Powell|Robert Baden-Powell]] saw the need for leader training. Early [[Scout Leader|Scoutmaster]] training camps were held in London and [[Yorkshire]]. Baden-Powell wanted practical training in the [[wilderness|outdoors]] in [[campsite]]s. World War I delayed the development of leader training, so the first formal Wood Badge course was not offered until 1919.<ref name="wbfounding" /><ref name="gillwbhist" /><ref name="origins" /> [[Gilwell Park]], just outside London, was purchased specifically to provide a venue for the course and the Opening Ceremonies were held on July 26, 1919. Francis Gidney, the first Camp Chief at Gilwell Park, conducted the first Wood Badge course there from September 8–19, 1919. It was produced by [[Percy Everett]], the Commissioner of Training, and Baden-Powell himself gave lectures. The course was attended by 18 participants, and other lecturers. After this first course, Wood Badge training continued at Gilwell Park, and it became the home of leadership training in the Scout movement.<ref name="gillwbhist2">{{cite web |url=http://pinetreeweb.com/woodbadg.htm |title=The Wood Badge Homepage |publisher=Pinetree Web |accessdate=2006-08-01}}</ref>

=== Modern curriculum ===
The main [[goal]]s of a Wood Badge course are to:<ref name="tsa_por_trg_limit">{{cite web |url=http://www.scoutbase.org.uk/library/hqdocs/por/2006/3_35.htm#rule_3.34 |title=Rule 3.34: Adult Training Obligations |work=Policy, Organisation and Rules |publisher=The Scout Association |accessdate=2007-01-24}}</ref><ref name="goals">{{cite web |last=Barnard |first=Mike |year=2002 |url=http://www.woodbadge.org/WB21/wb21obj.htm |title=The Objectives of Wood Badge |publisher=Woodbadge.org |accessdate=2007-01-07| archiveurl = https://web.archive.org/web/20070101194129/http://www.woodbadge.org/WB21/wb21obj.htm| archivedate = January 1, 2007}}</ref><ref name="complete">{{cite web |url=http://www.catvog.org/adultsupport/woodbadge.php |title=Training: The Wood Badge |publisher=CATVOG Scout Area (The Scout Association) |accessdate=2007-01-21}}</ref>
* Recognize the contemporary leadership concepts utilized in the corporate world and leading governmental organizations that are relevant to Scouting's values.
* Apply the skills one learns from participating as a member of a successful working team.
* View Scouting globally, as a family of interrelated, values-based programmes that provide age-appropriate activities for youth.
* Revitalize the leader's commitment by sharing in an inspirational experience that helps provide Scouting with the leadership it needs to accomplish its mission.

Generally, a Wood Badge course consists of classroom work, a series of self-study modules, outdoor training, and the Wood Badge "ticket" or "project". Classroom and outdoor training are often combined and taught together, and occur over one or more weeks or weekends. As part of completing this portion of the course, participants must write their tickets.

The exact curriculum varies from country to country, but the training generally includes both theoretical and [[experiential learning]]. All course participants are introduced to the ''1st Gilwell Scout group'' or ''Gilwell Scout Troop 1'' (the latter name is used in the [[Boy Scouts of America]] and some other countries). In the [[Boy Scouts of America]], they are also assigned to one of the traditional Wood Badge "critter" [[patrol]]s. Instructors deliver training designed to strengthen the patrols. One-on-one work with an assigned troop guide helps each participant to reflect on what he has learned, so that he can better prepare an individualized "ticket". This part of the training program gives the adult [[Scout Leader|Scouter]] the opportunity to assume the role of a Scout joining the original "model" troop, to learn firsthand how a troop ideally operates. The locale of all initial training is referred to as ''Gilwell Field'', no matter its geographical location.<ref>{{cite book |title=Wood Badge for the 21st Century – Staff Guide |year=2001 |publisher=Boy Scouts of America}}</ref>

=== Ticket ===
The phrase 'working your ticket' comes from a story attributed in Scouting legend to Baden-Powell: Upon completion of a British soldier's service in India, he had to pay the cost of his ticket home. The most affordable way for a soldier to return was to engineer a progression of assignments that were successively closer to home.

Part of the transformative power of the Wood Badge experience is the effective use of [[metaphor]] and tradition to reach both heart and mind. In most Scout associations, "working your ticket" is the [[culmination]] of Wood Badge training. Participants apply themselves and their new knowledge and skills to the completion of items designed to strengthen the individual's leadership and the home unit's organizational resilience in a project or "ticket". The ticket consists of specific goals that must be accomplished within a specified time, often 18 months due to the large amount of work involved. Effective tickets require much planning and are approved by the Wood Badge course staff before the course phase ends. Upon completion of the ticket, a participant is said to have earned his way back to Gilwell.<ref name="21stticket">{{cite web |last=Barnard |first=Mike |year=2003 |url=http://www.woodbadge.org/WB21/wb21ticket.htm |title=What is a Wood Badge Ticket? |publisher=Woodbadge.org |accessdate=2007-01-07| archiveurl = https://web.archive.org/web/20070101081847/http://www.woodbadge.org/WB21/wb21ticket.htm| archivedate = January 1, 2007}}</ref>

=== On completion ===
After completion of the Wood Badge course, participants are awarded the insignia in a Wood Badge bead ceremony.<ref name="21stceremony">{{cite web |last=Barnard |first=Mike |year=2002 |url=http://www.woodbadge.org/Ceremonies/wbcere.htm |title=Wood Badge Presentation Ceremonies |publisher=Woodbadge.org|accessdate=2007-01-07|archiveurl = https://web.archive.org/web/20070626183933/http://www.woodbadge.org/Ceremonies/wbcere.htm |archivedate = June 26, 2007|deadurl=yes}}</ref> They receive automatic membership in 1st Gilwell Park Scout Group or Gilwell Troop 1. These leaders are henceforth called Gilwellians or Wood Badgers. It is estimated that worldwide over 100,000 Scouters have completed their Wood Badge training.<ref name="recipients">{{cite web |year=2007 |url=http://www.scoutingvermont.org/Training/Advanced/WoodBadge/WBHistory| title=History of Wood Badge|publisher=Green Mountain Council Boy Scouts of America |accessdate=2015-03-10}}</ref> The 1st Gilwell Scout Group meets annually during the first weekend in September at [[Gilwell Park]] for the Gilwell Reunion.<ref name="historyofficial">{{cite book |last=Rogers |first=Peter |title=Gilwell Park: A Brief History and Guided Tour |year=1998 |publisher=[[The Scout Association]] |location=London, England |pages=5–46}}</ref> Gilwell Reunions are also held in other places, often on that same weekend.

== Insignia ==
[[File:Wood badge regalia 1.jpg|thumb|right|Wood Badge neckerchief, beads, and woggle]]
Scout leaders who complete the Wood Badge program are recognized with [[insignia]] consisting of the Wood Badge beads, 1st Gilwell Group neckerchief and woggle.

=== Woggle ===

The Gilwell [[woggle]] is a two-strand version of a [[Turk's head knot]], which has no beginning and no end, and symbolizes the commitment of a Wood Badger to Scouting.<ref name="gillwbhist" /><ref name="origins" /> In some countries, Wood Badge training is divided into more than one part and the Gilwell woggle is given for completion of Wood Badge Part I.

=== Beads ===
[[File:Dinizulu.jpg|thumb|King [[Dinuzulu]], wearing what is perhaps the necklace from which the original Wood Badge beads came]]
The beads were first presented at the initial leadership course in September 1919 at Gilwell Park.

The origins of Wood Badge beads can be traced back to 1888, when Baden-Powell was on a [[military campaign]] in [[Zulu Kingdom|Zululand]] (now part of South Africa). He pursued [[Dinuzulu]], son of [[Cetshwayo]], a [[Zulu people|Zulu]] king, for some time, but never managed to catch up with him. Dinuzulu was said to have had a 12-foot (4&nbsp;m)-long necklace with more than a thousand [[acacia]] beads.<ref name="thousand">{{cite web |year=2006 |url=http://scoutguidehistoricalsociety.com/woodbadge.htm| title=The origins of the Wood Badge |publisher=Johnny Walker's Scouting Milestones |accessdate=2007-01-21}}</ref> Baden-Powell is claimed to have found the necklace when he came to Dinuzulu's deserted mountain stronghold.<ref name="origins">{{cite web |year=2003 |url=https://members.scouts.org.uk/factsheets/FS145001.pdf |format=PDF |title=The Origins of the Wood Badge |publisher=ScoutBase UK|accessdate=2007-01-04}}</ref><ref name="hillcourt">{{cite book|last=Hillcourt|first=William|authorlink=William Hillcourt|year=1964|title=Baden-Powell: The Two Lives of a Hero |publisher=Heinemann |location=London|pages=358}}</ref> Such necklaces were known as ''iziQu'' in Zulu and were presented to brave warrior leaders.<!---the Q is UPPERCASE, see ref---><ref name="iziqu">{{cite web |url=http://africanhistory.about.com/library/glossary/bldef-isiQu.htm| title=iziQu |work=African History |publisher=About.com |accessdate=2007-01-04}}</ref> Other sources suggest that what Baden-Powell actually found were a Zulu girl's marriage dowry beads.

Much later, Baden-Powell sought a distinctive award for the participants in the first Gilwell course. He constructed the first award using two beads from the necklace he had recovered, and threaded them onto a leather thong given by an elderly South African in [[Mafeking]], calling it the ''Wood Badge''.<ref name="wbfounding">{{cite web |last=Block |first=Nelson R. |year=1994 |url=http://www.woodbadge.org/founding.htm |title=The Founding of Wood Badge |publisher=Woodbadge.org |accessdate=2006-07-20|archiveurl = https://web.archive.org/web/20060822100831/http://www.woodbadge.org/founding.htm |archivedate = August 22, 2006|deadurl=yes}}</ref><ref name="gillwbhist">{{cite web |last=Orans |first=Lewis P. |year=2004 |url=http://pinetreeweb.com/woodbadg.htm |title=The Wood Badge Homepage |publisher=Pinetree Web |accessdate=2006-08-01}}</ref><ref name="origins" />

While no official knot exists for tying the two ends of the thong together, the decorative [[diamond knot]] has become the most common. When produced, the thong is joined by a simple [[overhand knot]] and various region specific traditions have arisen around tying the diamond knot, including: having a fellow course member tie it; having a mentor or course leader tie it; and having the recipient tie it after completing some additional activity that shows he or she has mastered the skills taught to him or her during training.<ref name="origins" />

==== Significance of additional beads ====
Additional beads are awarded to Wood Badgers who serve as part of a Wood Badge training team. One additional bead is awarded to each ''Assistant Leader Trainer'' (Wood Badge staff) and two additional beads are awarded to each ''Leader Trainer'' (Wood Badge course directors), for a total of four.<ref name="origins" />

As part of a tradition, five beads may be worn by the "Deputy Camp Chiefs of Gilwell". The Deputy Camp Chiefs are usually the personnel of National Scout Associations in charge of Wood Badge training. The fifth bead symbolizes the Camp Chief's position as an official representative of Gilwell Park, and his or her function in maintaining the global integrity of Wood Badge training.<ref name="origins" /> [[William Hillcourt]] is one person who wore five beads.

The founder of the Scouting movement, Robert Baden-Powell, wore six beads, as did [[Percy Everett|Sir Percy Everett]], then Deputy Chief Scout and the Chief's right hand. Baden-Powell's beads are on display at [[Baden-Powell House]] in London. Everett endowed his six beads to be worn by the Camp Chief of Gilwell as a badge of office. Since that time the wearer of the sixth bead has generally been the director of leader training at Gilwell Park.<ref name="origins" />

{| class="wikitable"
|-
! Number of beads !! Worn by
|-
| 2 || Wood Badge recipient
|-
| 3 || Deputy Gilwell Course Leader
|-
| 4 || Gilwell Course Leader
|-
| 5 || Deputy Camp Chief of Gilwell Park (one per country)
|-
| 6 || Camp Chief of Gilwell Park; Chief Scout of the World
|}

=== 1st Gilwell Scout Group neckerchief ===

The [[neckerchief]] is a universal [[symbol]] of Scouting and its Maclaren [[tartan]] represents Wood Badge's ties to Gilwell Park. The neckerchief, called a "necker" in British and some [[Commonwealth of Nations|Commonwealth]] Scouting associations, is a standard [[triangle|triangular]] scarf made of cotton or wool [[twill]] with a taupe face and red back; a patch of [[Clan MacLaren]] tartan is affixed near the point.<ref name="mclarenclan">{{cite web |year=2004|url=http://www.clanmaclarenna.org/CMSNA/home.nsf/7cf3963177286e1586256c5a00489ae2/1242531143eabbbd86256c90007fff88!OpenDocument
|title=Clan MacLaren and the Scouting Connection|publisher=Clan Maclaren.org |accessdate=2007-01-21}}</ref> The pattern was adopted in honor of a British Scout commissioner who, as a descendant of the Scottish MacLaren clan, donated money for the Gilwell Park property on which the first Wood Badge program was held.<ref name="origins" /><ref name="thousand" /><ref name="neckerchief">{{cite web |url=http://www.scouting.org/Media/FactSheets/02-539.aspx |title=History of Wood Badge|publisher=Scouting.org |accessdate=2007-01-05}}</ref>

Originally, the neckerchief was made entirely of triangular pieces of the tartan, but its expense forced the adoption of the current design. The neckerchief is often worn with the Gilwell [[woggle]].<ref name="gillwbhist" /><ref name="origins" />

=== Axe and Log ===
[[File:Gilwell Park.svg|thumb|100px|The totem of Gilwell Park, the axe and log, has come to represent Wood Badge]]
The axe and log logo was conceived by the first Camp Chief, Francis Gidney, in the early 1920s to distinguish Gilwell Park from the Scout Headquarters. Gidney wanted to associate Gilwell Park with the outdoors and [[Scoutcraft]] rather than the business or administrative Headquarters offices. Scouters present at the original Wood Badge courses regularly saw axe blades masked for safety by being buried in a log. Seeing this, Gidney chose the axe and log as the totem of [[Gilwell Park]].<ref>{{cite web |url=http://www.leaderlore.com/axe.html|title=Origins of the Wood Badge Axe|first1=Nelson|last1=Block|first2=Keith|last2=Larson|accessdate=2008-08-03|date=October–November 1994}}</ref>

=== Other symbols ===
[[File:Jemenittisk sjofar av kuduhorn.jpg|thumb|200px|right|A kudu horn]]
The [[kudu]] horn is another Wood Badge symbol. Baden-Powell first encountered the kudu horn at the [[Shangani Patrol|Battle of Shangani]], where he discovered how the [[Northern Ndebele people|Matabele]] warriors used it to quickly spread a signal of alarm. He used the horn at the [[Brownsea Island Scout camp|first Scout encampment at Brownsea Island]] in 1907. It is used from the early Wood Badge courses to signal the beginning of the course or an activity, and to inspire Scouters to always do better.

The grass fields at the back of the White House at Gilwell Park are known as the Training Ground and The Orchard, and are where Wood Badge training was held from the early years onward. A large [[oak]], known as the ''Gilwell Oak'', separates the two fields. The Gilwell Oak symbol is associated with Wood Badge, although the beads for the Wood Badge have never been made of this oak.<ref name="historyofficial" />

[[Cub Scout|Wolf Cub]] leaders briefly followed a separate training system beginning in 1922, in which they were awarded the ''Akela Badge'' on completion. The badge was a single [[canine tooth|fang]] on a leather thong. Wolf Cub Leader Trainers wore two fangs.<ref name="thousand" /><ref name="fangs">{{cite web |url=http://www.scoutbase.org.uk/library/history/cubs/ |title=The history of Cubbing in the United Kingdom 1916–present |publisher=ScoutBase UK|accessdate=2007-01-04}}</ref> The Akela Badge was discontinued in 1925, and all leaders were awarded the Wood Badge on completion of their training. Very few of the fangs issued as Akela Badges can now be found.<ref name="origins" />

== International training centers and trainers ==

=== Great Britain ===
The first Wood Badge training took place on Gilwell Park. The estate continues to provide the service in 2007, for British Scouters of [[The Scout Association]] and international participants. Original trainers include Baden-Powell and Gilwell Camp Chiefs [[Francis Gidney]], [[J. S. Wilson|John Wilson]] and, until the 1960s, [[John Thurman]].<ref name="milestones">{{cite web|last=Walker|first=Johnny |year=2006 |url=http://scoutguidehistoricalsociety.com/biogs-g-m.htm |title=Gidney, Francis 'Skipper'. 1890–1928 |work=Scouting Personalities |publisher=Johnny Walker's Scouting Milestones |accessdate=2009-06-04}}</ref>

=== Australia ===
Other sites providing Wood Badge training have taken the Gilwell name. The first Australian Wood Badge courses were held in 1920 after the return of two newly minted Deputy Camp Chiefs, [[Charles Hoadley]] and Mr. Russell at the home of Victorian Scouting, [[Gilwell Park (Victoria)|Gilwell Park, Gembrook]]. In 2003, [[Scouts Australia]] established the Scouts Australia Institute of Training, a government-registered National Vocational & Education Training (VET) provider. Under this registration, Scouts Australia awards a "Diploma of Leadership and Management" to those Adult Leaders who complete the Wood Badge training and additional competencies.<ref name="aus">{{cite web |url=http://www.scouts.com.au/main.asp?iStoryID=1944 |title=Wood Badge Training Program |publisher=Scouts Australia |accessdate=2007-01-04}}</ref> The Diploma of Leadership and Management, like all Australian VET qualifications, is recognized throughout Australia by both government and private industry.<ref name="recognition">{{cite web |date=August 2006 |url=http://www.coventryscouts.org.uk/sixth%20issue%20-%20November%202006.pdf |title=Training Bulletin: Woodbadge holders |format=PDF |publisher=Scouts Australia |accessdate=2007-01-12|archiveurl = https://web.archive.org/web/20071030164140/http://www.coventryscouts.org.uk/sixth%20issue%20-%20November%202006.pdf |archivedate = October 30, 2007|deadurl=yes}}</ref> This is an optional extra that Leaders and Rovers may undertake.

=== Austria ===
The first Wood Badge training in Austria was held September 8 to 17, 1922, near Vienna; it was led by Scoutmaster Miegl <ref>{{cite journal |title=Jamboree Symposium of World-wide Scouting |volume=9 |date=January 1923 |page=137}}</ref>

=== Finland ===
Alfons Åkerman gave the first eight Wood Badge courses and was from 1927 to 1935 the first Deputy Camp Chief. In lieu of Gilwell training, the Finnish Scouts have a "Kolmiapila-Gilwell" (Trefoil-Gilwell), combining aspects of both girls' and boys' advanced leadership training.<ref name="fingil">{{cite web|url=http://www.partio.fi/Projektit/English/English/Guiding_and_Scouting/History.iw3|title=History|publisher=Partio Scout|accessdate=2009-06-04}}</ref>

===Canada===
[[Scouts Canada]] holds numerous Woodbadge training courses on an annual basis throughout the country. In this NSO, all Scouters (volunteers) are required to complete a Woodbadge 1 Course, and are encouraged to complete a Woodbadge 2 Course. Upon completion of the Woodbadge 2 course a volunteer is conferred their "beads" and the Gilwell Necker.

=== France ===
The first Wood Badge training in France was held Easter 1923 by Père Sevin in [[Chamarande]].<ref>{{cite web |url=http://www.honneur-au-scoutisme.com/textes/chamarande/chamarande.htm |title=Chamarande |work=Honneur au Scoutisme |accessdate=2009-06-04 |language=French}}</ref>

=== Belgium ===
The first Wood Badge training in Belgium was held in 1934 in [https://www.hopper.be/jeugdverblijf/de-kluis De Kluis, Sint-Joris-Weert].
At the entrance of the Gilwell-trainingground there is a Gilwell-gate. Also in the stained glass of the chapel you can see some Gilwell-symbols. 

=== The Netherlands ===
[[File:1ste Gillwell Leiderscursus.jpg|thumb|Gilwell Leiderscursus, The Netherlands July 9–21, 1923]]
The first Wood Badge training in the Netherlands was held in July 1923 by Scoutmaster [[Jan Schaap]],on [[Gilwell Ada's Hoeve]], [[Ommen]]. At Gilwell Sint Walrick, [[Overasselt]], the Catholic Scouts had their training. Since approximately 2000, the Dutch Wood Badge training takes place on the Scout campsite ''Buitenzorg'', [[Baarn]], or outdoors in Belgium or Germany under the name 'Gilwell Training'.<ref name="neth_1">{{cite web |url=http://gilwell.scouting.nl/static/cursus/varianten.html |title=Cursusvarianten |work=Gilwell een wereldcurcus |publisher=Scouting Nederland |accessdate=2009-06-04 |language=Dutch| archiveurl = https://web.archive.org/web/20080310231026/http://www.gilwell.scouting.nl/static/cursus/varianten.html| archivedate = March 10, 2008}}</ref>

=== Ireland ===
Wood Badge training in Ireland goes back to the 1st [[Larch Hill]] of [[Scouting Ireland (CSI)|the Catholic Boy Scouts of Ireland]], who conducted Wood Badge courses that emphasized the Catholic approach to Scouting. This emphasis is now disappeared since the formation of [[Scouting Ireland]].<ref name="Ireland">{{cite web |url=http://www.scouts.ie/resources/adult-resources/woodbadge-training/ |title=Resources: Adult Resources |publisher=Scouting Ireland |accessdate=2007-01-04 |archiveurl = https://web.archive.org/web/20070207014750/http://www.scouts.ie/resources/adult-resources/woodbadge-training/ <!-- Bot retrieved archive --> |archivedate = 2007-02-07}}</ref>

=== Israel ===
The first Wood Badge training in Israel was held in April 1963 by [[John Thurman]] and took place at the Israeli Scout Ranch, together with 20 participants, [[Jews]], [[Arabs]] and [[Druze]]. Since the first training, every Wood Badge course run by the [[Israel Boy and Girl Scouts Federation]] is a mutual event for all different religions and organizations in Scouting.

=== United States of America ===
{{Main article|Wood Badge (Boy Scouts of America)}}
[[File:FourAmericanWoodBadgers04.jpg|thumb|200px|Four American Wood Badgers with insignia]]
Wood Badge was introduced to America by Baden-Powell and the first course was held in 1936 at the [[Mortimer L. Schiff Scout Reservation]], the [[Boy Scouts of America]] national training center until 1979.<ref name="ubushist">{{cite web |last=Barnard |first=Mike |year=2002 |url=http://www.woodbadge.org/wbhistus.htm |title=History of Wood Badge in the United States |publisher=Woodbadge.org|accessdate=2007-01-07| archiveurl = https://web.archive.org/web/20070101163005/http://www.woodbadge.org/wbhistus.htm| archivedate = January 1, 2007}}</ref> Despite this early first course, Wood Badge was not formally adopted in the United States until 1948 under the guidance of [[William Hillcourt|Bill Hillcourt]] who became national Deputy Camp Chief of the United States.<ref name="schiff">{{cite web |last=Barnard|first=Mike |year=2001 |url=http://www.woodbadge.org/wbgbbWB.htm |title=Green Bar Bill Hillcourt's Impact on Wood Badge |publisher=Woodbadge.org |accessdate=2007-01-30| archiveurl = https://web.archive.org/web/20070101115746/http://www.woodbadge.org/wbgbbWB.htm| archivedate = January 1, 2007}}</ref> Today the national training center of the Boy Scouts of America is the [[Philmont Training Center]], which hosts a few camps each year. Nearly all Wood Badge courses are held throughout the country at local council camps under the auspices of each BSA region.

=== Philippines ===
Wood Badge was introduced in the [[Philippines]] in 1953 with the first course held at Camp Gre-Zar in Novaliches, [[Quezon City]]. Today, Wood Badge courses are held at the Philippine Scouting Center for the Asia-Pacific Region, at the foothills of [[Mount Makiling]], [[Los Baños, Laguna|Los Baños]], [[Laguna (province)|Laguna province]].<ref>{{cite book |title=Diamond Jubilee Yearbook |publisher=Boy Scouts of the Philippines |year=1996 |location=Manila |isbn=9789719176909}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
{{Spoken Wikipedia|Wood Badge.ogg|2008-05-11}}
{{Commons category|Wood badge}}
* [http://www.scouts.com.au/autopage.asp?iMenuID=1206&iShellID=702 Scouts Australia Institute of Training Site]

{{Featured article}}
{{scouting|movement}}

[[Category:Scout leader training]]
[[Category:Scouting spoken word files]]
[[Category:Scouting events]]