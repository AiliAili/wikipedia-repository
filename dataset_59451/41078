{{Infobox professional wrestler
|name        = Dale Veasey
|image       = 
|caption     =
|names       = The Alaskan Hunter<br />Dale Veasey<br />The Hunter<br />'''Lt. James Earl Wright'''
<!-- Please don't change the height or weight. These are the measures as officially stated and they should not be changed. -->
|height      = {{height|ft=5|in=8}}
|weight      = {{convert|235|lb|kg|abbr=on}}
|birth_date  = {{birth date and age|1960|05|20}}
|death_date  =
|birth_place = [[Orlando, Florida]], [[United States]]
|death_place =
|resides     =
|billed      = [[Atlanta, Georgia]]
|trainer     =
|debut       = 1982
|retired     = 1996
|website     =
}}
'''Todd Dale Veasey''' (born May 20, 1960) is a retired American professional wrestler, better known by his ringnames '''Dale Veasey''' and '''Lt. James Earl Wright''', who competed in North American regional promotions including the [[Universal Wrestling Federation (Bill Watts)|Mid-South]] region and the [[National Wrestling Alliance]], particularly the [[Georgia Championship Wrestling|Georgia]] and [[Championship Wrestling from Florida|Florida territories]], as well as brief stints in the [[World Wrestling Entertainment|World Wrestling Federation]] and [[World Championship Wrestling]], most notably as one half of the tag team '''State Patrol''' with [[DeWayne Bruce|Buddy Lee Parker]] during the 1990s.

He would also have a successful career teaming with Bob Brown in international promotions including [[Stampede Wrestling]] and the [[World Wrestling Council]].

==Career==

===Early career===
Dale Veasey made his professional debut in early 1982, beginning his career in Georgia and Florida. Later that year however, he disappeared from the scene and did not reappear until February 1984, in Georgia Championship Wrestling. The on-air reason given for his hiatus was that he had been in a serious automobile accident, breaking his ankle, jaw, right arm, and also suffering two skull fractures. On his return he was heavier than when last seen in 1982, but spent the next few months trimming down. Veasey was still with the promotion when [[Black Saturday (wrestling)|Black Saturday]] occurred, at which point he transition to other territories, losing several singles matches to [[Tim Horner]], [[Shawn Michaels]], [[Terry Taylor]] and [[King Parsons|Iceman Parsons]] while in the Mid-South territory in January 1985 and spending most of the year wrestling in Florida Championship Wrestling facing veterans such as [[Wahoo McDaniel]], [[Rip Rogers]] and [[Hector Guerrero]].

While in [[Southwest Championship Wrestling|Texas All-Star Wrestling]], he also defeated [[Terry Daniels]] in a tournament final to win the vacant TASW Junior Heavyweight Championship in [[San Antonio|San Antonio, Texas]] on September 2, 1985 holding the title for four months before leaving the area.<ref>{{cite web |url=http://www.kayfabememories.com/Regions/tasw/tasw4.htm |title=Regional Territories: Texas All-Star Wrestling, Page #4 |author=Merkich, Chuck |authorlink= |coauthors= |date= June 2002 |work= |publisher=KayfabeMemories.com |pages= |language= |quote= }}</ref>

===The Alaskan Hunters & State Patrol===
The following year, Veasey underwent another physical transformation into [[The Road Warriors|Road Warriors]]-inspired, '''The Hunter'''. He spent most of 1986/1987 wrestling as either The Hunter in singles competition or with tag-team partner Bob Brown as '''The Alaskan Hunters''', going on to win the WWC North American Tag Team Championship with Brown defeating [[Miguel Pérez, Jr.|Miguelito Perez]] & [[Tony Atlas]] in [[San Juan, Puerto Rico]] on June 20, 1987. After defeating WWC World Tag Team Champions [[Mark Youngblood|Mark]] & [[Chris Romero|Chris Youngblood]] on August 26, the two would briefly hold both the World and North American tag team titles before losing the World titles back to the Youngbloods September 20, 1987. After the North American titles were abandoned by the promotion in November, Veasey and Brown would leave the promotion by the end of the year.

Early in 1989, Veasey and Brown spent some time in the World Wrestling Federation and, later, a very short spell in Stampede Wrestling, but by the end of 1989 Veasey was in World Championship Wrestling as 'Lieutenant James Earl Wright' along with 'Sergeant Buddy Lee Parker', his tag team partner in the State Patrol (which first formed while both were in Stampede).

===World Championship Wrestling===
Making their debut in late 1989, the State Patrol substituted for the [[The Headshrinkers|Samoan Swat Team]] and were defeated by the NWA/WCW US Tag Team Champions [[The Steiner Brothers]] at Chicago’s UIC Pavilion on September 30 and also faced [[Ricky Morton]] & [[Tommy Rich]] on December 14, 1990.<ref>{{cite web|url=http://www.angelfire.com/wrestling/cawthon777/wcw90.htm |title=WCW 1990 |accessdate=2007-09-12 |author=Cawthon, Graham |authorlink= |coauthors= |date=2007-05-21 |work= |publisher=Graham Cawthon’s History of the WWE |archiveurl=https://web.archive.org/web/20071219212546/http://www.angelfire.com/wrestling/cawthon777/wcw90.htm |archivedate=2007-12-19 |deadurl=yes |df= }}</ref>

During 1991, Wright and Parker regularly appeared on televised shows including [[WCW Power Hour]] and [[WCW Saturday Night]]. After losing to [[Brian Pillman]] in a singles match on WCW Power Hour on January 26, both he and Parker brawled with Pillman afterwards with Pillman getting the upper hand.<ref>{{cite web |url=http://www.angelfire.com/wrestling/cawthon777/powerhour1-26-91review.htm |title=Review: WCW Power Hour – January 26, 1991 |accessdate=2007-09-12 |author=Peddycord, Matt |authorlink= |coauthors= |date=2007-05-21 |work= |publisher=History of the WWE  |pages= |language= }}</ref> This would lead to a brief feud which resulted in a rematch at [[Clash of the Champions#Clash of the Champions XIV: Dixie Dynamite|Clash of the Champions XIV]] on January 30 with Wright again losing to Pillman.<ref>{{cite web |url=http://www.angelfire.com/wrestling/cawthon777/clash14review.htm |title=Review: WCW Clash of the Champions XIV: Dixie Dynamite – January 30, 1991 |accessdate=2007-09-12 |author=Peddycord, Matt |authorlink= |coauthors= |date=2007-05-26 |work= |publisher=History of the WWE  |pages= |language= |quote= }}</ref>

Although having success against preliminary wrestlers in early 1991,<ref>{{cite web |url=http://www.1wrestling.com/news/newsline.asp?news=14581 |title=1wrestling.com Newsline - WCW Worldwide Classics report |accessdate=2007-09-12 |last=Lee|first=Ralph |authorlink= |coauthors= |date=2003-10-07 |format= |work= |publisher=1wrestling.com |pages= |language= |archiveurl=https://web.archive.org/web/20050119134544/http://www.1wrestling.com/news/newsline.asp?news=14581 |archivedate=January 19, 2005 |quote= }}</ref> Veasey and Parker teamed with Big Cat in a losing effort against the then [[WCW World Six-Man Tag Team Championship|WCW World Six-Man Tag Team Champions]] [[Junkyard Dog]], Ricky Morton & Tommy Rich at [[WrestleWar#1991|WrestleWar ‘91]] on February 24, 1991.<ref>{{cite web |url=http://www.softwolves.pp.se/wrestling/wcw/1991#war91 |title=World Championship Wrestling: WrestleWar 1991 |accessdate=2007-09-12 |author= |authorlink=Karlsson, Peter |coauthors= |date=2005-08-23 |format= |work= |publisher=American Wrestling Trivia |pages= |language= |quote= }}</ref> He would also lose a series of single matches to Junkyard Dog and Dustin Rhodes during the next two months, before losing to [[Big Van Vader]] in a handicap match with Joe Cazana on April 24, 1991.<ref>{{cite web|url=http://www.angelfire.com/wrestling/cawthon777/wcw91.htm |title=WCW 1991 |accessdate=2007-09-12 |author=Cawthon, Graham |authorlink= |coauthors= |date=2007-05-21 |work= |publisher=Graham Cawthon’s History of the WWE |archiveurl=https://web.archive.org/web/20070317053236/http://www.angelfire.com/wrestling/cawthon777/wcw91.htm |archivedate=2007-03-17 |deadurl=yes |df= }}</ref>

The following year, Veasey lost to [[Jim Duggan|"Hacksaw" Jim Duggan]] in a match for the [[WWE United States Championship|WCW United States Heavyweight Championship]] on November 20 and, with Sgt. Buddy Lee Parker, lost to then WCW World Tag Team Champions [[The American Males]] on December 7, 1994.<ref name="Cawthon, Graham">{{cite web|url=http://www.angelfire.com/wrestling/cawthon777/wcw96.htm |title=WCW 1996 |accessdate=2007-09-12 |author=Cawthon, Graham |authorlink= |coauthors= |date=2007-06-29 |work= |publisher=Graham Cawthon’s History of the WWE |archiveurl=https://web.archive.org/web/20070629071312/http://www.angelfire.com/wrestling/cawthon777/wcw96.htm |archivedate=2007-06-29 |deadurl=yes |df= }}</ref>

Appearing on WCW Saturday Night during early 1995, he lost singles matches against [[Sting (wrestler)|Sting]], Brian Pillman and [[Renegade (professional wrestler)|Renegade]]. Although losing to the American Males on August 21, they would later defeat [[Robert Fuller|Col. Robert Lee Parker]]'s Stud Stable ([[Bunkhouse Buck]] & [[Dick Slater|"Dirty" Dick Slater]]) during ''[[WCW Monday Nitro]]'' on September 18. Losing to [[The Nasty Boys]] on October 11 and Sting & [[Ric Flair]] on October 25, he and Parker would later appear in a 60-man Battle Royal where both he and his partner were eliminated by [[Jeff Farmer (wrestler)|Cobra]] at [[WCW World War 3#1995|World War Three '95]] on November 11, 1995. After being eliminated, Veasey/Wright pulled Cobra out of the ring eliminating him from the battle royal as well, however nothing came from this.<ref>{{cite web |url=http://www.softwolves.pp.se/wrestling/wcw/1995#ww3_95 |title=World Championship Wrestling: World War Three 1995 |accessdate=2007-09-12 |last=Karlsson|first=Peter |coauthors= |date=2005-08-23 |work= |publisher=American Wrestling Trivia}}</ref>

The following year, the State Patrol lost a match against [[Harlem Heat]] on WCW Saturday Night in [[Rome, Georgia]] on March 30, 1996.<ref name="Cawthon, Graham" />

The State Patrol also toured Japan several times during the 1990s, and in the late 1990s Veasey retired from active competition although he has since wrestled in the Georgia-based Columbus Championship Wrestling in January 2002.<ref>{{cite web |url=http://www.411mania.com/wrestling/columns/26867 |title=Ask 411 - Booger Red, Marrianna, Albright, Superstar Billy Graham |accessdate=2007-09-12 |author=Litawsky, Craig |authorlink= |coauthors= |date=2002-04-23 |format= |work= |publisher=411mania.com |pages= |language= |quote= }}</ref>

==Championships and accomplishments==
*'''Texas All-Star Wrestling'''
**TASW USA Junior Heavyweight Championship (1 time)<ref name="Titles">{{cite book | author=Royal Duncan & Gary Will | title=Wrestling Title Histories | publisher=Archeus Communications | year=2000|edition=4th | isbn=0-9698161-5-4 }}</ref>
*'''[[World Wrestling Council]]'''
**[[WWC World Tag Team Championship]] ([[WWC World Tag Team Championship#Title history|1 time]]) - with [[Bob Brown (wrestler)|Bob Brown]]<ref>{{cite web |url=http://www.wrestling-titles.com/us/pr/wwc/wwc-t.html |title=W.W.C. World Tag Team Title |year=2003 |publisher=Puroresu Dojo}}</ref>
**[[NWA North American Tag Team Championship (Puerto Rico/WWC version)|WWC North American Tag Team Championship]] ([[WWC North American Tag Team Championship#Title history|1 time]]) - with Bob Brown<ref>{{cite web |url=http://www.wrestling-titles.com/us/pr/wwc/wwc-na-t.html |title=W.W.C. North American Tag Team Title |year=2003|publisher=Puroresu Dojo}}</ref>
**[[WWC Caribbean Tag Team Championship]] ([[WWC Caribbean Tag Team Championship#title history|1 time]]) - with [[Dewayne Bruce|Sgt. Buddy Lee Parker]]<ref>https://www.youtube.com/watch?v=x-_7Q_heSBc</ref>

==References==
{{Reflist}}

==External links==
{{Portal|Professional wrestling}}
*[http://www.cwfarchives.com/Wrestler.php?id=431 Championship Wrestling from Florida: Dale Veasey]
*[http://wrestling.xwiki.com/xwiki/bin/view/Main/JamesEarlWright James Earl Wright] at the [https://web.archive.org/web/20090414025217/http://wrestling.xwiki.com/ WrestleWiki]
*[http://www.onlineworldofwrestling.com/profiles/j/james-earle-wright.html Profile at Online World of Wrestling]
*[http://www.cagematch.de/?id=2&nr=4363&worker=Lt.+James+Earl Cagematch: Lt. James Earl] {{de icon}}

{{DEFAULTSORT:Veasey, Dale}}
[[Category:1960 births]]
[[Category:American male professional wrestlers]]
[[Category:Living people]]
[[Category:Professional wrestlers from Florida]]