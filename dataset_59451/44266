{{Infobox airport
| name                = RAF Binbrook
| ensign              = [[File:Ensign of the Royal Air Force.svg|90px]]
| nativename          = 
| nativename-a        = 
| nativename-r        = 
| image               = XP702-LightningF3-1980.jpg
| image-width         = 250
| caption             = An 11(F) Squadron Lightning from RAF Binbrook landing at RAF Finningley
| IATA                = 
| ICAO                = <s>EGXB</s>
| type                = Military
| owner               = [[Ministry of Defence (United Kingdom)|Ministry of Defence]]
| operator            = [[Royal Air Force]]
| city-served         = 
| location            = [[Brookenby]], [[Lincolnshire]]
| built               = 1939
| used                = June 1940 – 1942<br> 1943 – April 1988
| elevation-f         = 
| elevation-m         = 
| coordinates         = {{Coord|53|26|45|N|000|12|32|W|region:GB_type:airport|display=inline,title}}
| pushpin_map         = Lincolnshire
| pushpin_label       = RAF Binbrook
| website             = 
| pushpin_map_caption = Location in Lincolnshire
| r1-number           = 02/20
| r1-length-m         = 0
| r1-length-f         = 7,500
| r1-surface          = [[Asphalt]]
| r2-number           = 00/00
| r2-length-m         = 0
| r2-length-f         = 0
| r2-surface          = Asphalt
| r3-number           = 00/00
| r3-length-m         = 0
| r3-length-f         = 0
| r3-surface          = Asphalt
| stat-year           = 
| stat1-header        = 
| stat1-data          = 
| stat2-header        = 
| stat2-data          = 
}}
'''Royal Air Force Station Binbrook''' or '''RAF Binbrook''' is a former [[Royal Air Force]] [[Royal Air Force station|station]], now closed, located near [[Binbrook]], [[Lincolnshire]], [[England]]. The old domestic site (married quarters) has been renamed to become the village of Brookenby. RAF Binbrook was primarily used by [[RAF Bomber Command|Bomber Command]]. After the war it was amongst others the home of the Central Fighter Establishment. It also served as base for the last two RAF squadrons to employ the [[English Electric Lightning]] between 1965 and 1988.

==History==
===Bombers===
RAF Binbrook was opened as a Bomber Command station in June 1940 during the [[Second World War]]<ref name=AS2-42>Halpenny 1991, p. 42.</ref> home to [[No. 12 Squadron RAF]] which operated between 3 July 1940 and 25 September 1942 before moving to [[RAF Wickenby]]. The squadron operated the [[Vickers Wellington]] Mk II and III.<ref name="Jefford2001p28">{{Harvnb|Jefford|2001|p=28.}}</ref> Another squadron to use Binbrook before 1942 was [[No. 142 Squadron RAF|142]] which initially operated between 3 July 1940 and 12 August 1940 with the [[Fairey Battle]] and left for a short time before returning on 6 September 1940 and going to [[RAF Grimsby|RAF Waltham]] on 26 November 1941. The squadron used the Battle until November 1940 before switching to the Wellington Mk II.<ref name="Jefford2001p61">{{Harvnb|Jefford|2001|p=61.}}</ref> It closed in 1942 for the installation of three concrete runways, reopening in 1943 as home to [[No. 460 Squadron RAAF|No. 460 Squadron]], [[Royal Australian Air Force]].<ref name=AS2-43>Halpenny 1991, p. 43.</ref> Post-war, Binbrook was home to a number of distinguished [[RAF]] bomber squadrons, notably [[No. 9 Squadron RAF|IX]], [[No. 12 Squadron RAF|12]], [[No. 101 Squadron RAF|101]] and [[No. 617 Squadron RAF|617]], all four of which were there for more than a decade.<ref name=AS2-46-47>Halpenny 1991, pp. 46–47.</ref><ref name=Jefford>Jefford 2001, pp. 30–31, 57, 101–102.</ref> The airfield saw the start of the RAF's transition to jet bombers with the arrival of the first [[English Electric Canberra]]s.<ref name="AS2-46-47"/>

===Fighters===
After the departure of IX and 12 squadrons in 1959, Binbrook housed [[Gloster Javelin]] all-weather fighters belonging to [[No. 64 Squadron RAF|64]] squadron, as well as the Central Fighter Establishment. [[No. 85 Squadron RAF|85]] Squadron also moved to Binbrook with a mixture of Canberras and [[Gloster Meteor]]s in the target facilities role.<ref name="AS2-46-47"/>

===Lightnings===
From 1965, Binbrook was the home to the [[English Electric Lightning]]s of [[No. 5 Squadron RAF|5 Squadron]], joined by the similarly equipped [[No. 11 Squadron RAF|11 Squadron]] in 1972. 5 and 11 were the last two RAF squadrons to employ the Lightning. 5 Squadron re-equipped with the [[Panavia Tornado|Tornado F3]] at [[RAF Coningsby]] early in 1988, leaving 11 Squadron to soldier on at Binbrook for a few more months with the remaining few Lightnings in RAF service. When 11 Squadron disbanded to also re-equip with the [[Tornado F3]] at [[RAF Leeming]], the Lightning was withdrawn from service.

On 8 September 1970, Captain [[William Schaffner]], an American exchange pilot flying BAC Lightnings with [[No. 5 Squadron RAF|5 Squadron]], took off from Binbrook in the plane ''[[United Kingdom military aircraft serials|XS894]]'' at 22:06, armed with two [[Hawker Siddeley Red Top|Red Top]]  [[air-to-air missile]]s. The plane was lost over the North Sea. Three weeks later it was located on the sea bed. Some believe this was an encounter with a [[UFO]].

===Closure===
The station closed as a Main Operating Base in the 1980s, although it continued as a Relief Landing Ground for [[RAF Scampton]] into the early 1990s before eventually closing and all military activity ceasing, it was subsequently sold off for development.<ref name="AS2-46-47"/><ref name=EEBACL >Halpenny 1984, p. ?</ref>

The Control Tower and adjacent Fire Section were demolished in 1995.

In the mid 90s, [[Lincolnshire Police]] and [[Humberside Police]] used the site to teach [[riot control]] techniques to its Police Officers.

As of 2012 a majority of the accommodation blocks have been demolished. The hangars and offices are used as an industrial estate housing many businesses. The flight line is fenced off and used for storage of mainly ex-military equipment awaiting resale. The married quarters are private housing, forming the new village of [[Brookenby]]. There is also a memorial to 460 Squadron (RAAF) consisting of a Memorial Plaque and benches around the former Ident Square.

==Popular culture==
In 1989 RAF Binbrook alongside [[RAF Little Rissington]] served as the [[USAAF]] airbase for filming for the 1990 movie [[Memphis Belle (film)|Memphis Belle]].<ref name=AS2-219>Halpenny 1991, p. 219.</ref>

==References==
===Citations===
{{reflist}}
===Bibliography===
* [[Bruce Barrymore Halpenny|Halpenny, Bruce Barrymore.]] ''Action Stations: Wartime Military Airfields of Lincolnshire and the East Midlands v. 2''. Patrick Stephens Ltd., 1981. ISBN 0-85059-484-7. <br>Later published (With 16 page Update Supplement) as:
** ''Action Stations: Wartime Military Airfields of Lincolnshire and the East Midlands v. 2'' Patrick Stephens Ltd., 1991. ISBN 1-85260-405-0.
*{{wikicite|ref={{harvid|Jefford|2001}}|reference=Jefford, C.G, [[Order of the British Empire|MBE]], BA, RAF (Retd). ''RAF Squadrons, a Comprehensive Record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 2001. ISBN 1-84037-141-2.}}
==Further reading==
* [[Bruce Barrymore Halpenny|Halpenny, Bruce Barrymore.]] ''English Electric/BAC Lightning (Osprey Air Combat Series No.4)''.  Publishing Ltd., 1984. ISBN 0-85045-562-6.
* Scott, Stewart. ''Airfield Focus: Binbrook''. GMS Enterprises, 2000. ISBN 1-870384-80-6.

==External links==
{{Commons category|RAF Binbrook}}
* [http://knol.google.com/k/binbrook-1943-home-of-460-squadron?collectionId=1gelxilfcbq0r.6# Binbrook 1943 Home of 460 Squadron]

{{Royal Air Force}}
{{RAF stations in Lincolnshire}}

[[Category:Royal Air Force stations in Lincolnshire|Binbrook]]
[[Category:Royal Air Force stations of World War II in the United Kingdom|Binbrook]]