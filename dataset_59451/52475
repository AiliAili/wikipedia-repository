{{Infobox Journal
| title = Organic & Biomolecular Chemistry
| cover = [[File:Organic & Biomolecular Chemistry cover.gif|200 px]]
| discipline = [[Organic chemistry]]
| editor = Richard Kelly
| abbreviation = Org. Biomol. Chem.
| website = http://www.rsc.org/Publishing/Journals/OB/index.asp
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| frequency = Biweekly
| history = 2003-present
| impact = 3.559
| impact-year = 2015
| eISSN = 1477-0539
| ISSN = 1477-0520
| OCLC = 884652802
| CODEN = OBCRAK
| LCCN = 2003261024
}}
'''''Organic & Biomolecular Chemistry''''' is a biweekly [[peer-reviewed]] [[scientific journal]] covering all aspects of [[organic chemistry]], including organic aspects of [[chemical biology]], [[medicinal chemistry]], [[natural product chemistry]], [[supramolecular chemistry]], [[macromolecular chemistry]], [[theoretical chemistry]], and [[catalysis]]. It is published by the [[Royal Society of Chemistry]]. Its predecessor journals were ''[[Perkin Transactions]] I'' and ''Perkin Transactions II''. The [[editor-in-chief]] is Richard Kelly.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-01-08}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101154995 |title=Organic & Biomolecular Chemistry |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-01-08}}</ref>
* [[Science Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-01-08}}</ref>
* [[Current Contents]]/Life Sciences<ref name=ISI/>
* Current Contents/Physical, Chemical & Earth Sciences<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2015-01-08}}</ref>
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.562.<ref name=WoS>{{cite book |year=2015 |chapter=Organic & Biomolecular Chemistry |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== See also ==
* ''[[Chemical Communications]]''
* [[List of scientific journals]]
* [[List of scientific journals in chemistry]]

== References ==

<references/>

== External links ==
* {{Official website|http://www.rsc.org/Publishing/Journals/OB/index.asp}}

{{Royal Society of Chemistry|state=collapsed}}
[[Category:Biochemistry journals]]
[[Category:Publications established in 2003]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Biweekly journals]]
[[Category:English-language journals]]