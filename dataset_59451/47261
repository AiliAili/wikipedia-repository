{{Sidebar
| name = Thomas Cowan (broadcaster)
| outertitle = 
| topimage = 
| pretitle = 
| title = 
| image =

| headingstyle = 
| contentstyle =

| heading1 = 
| content1 =

| heading2 = 
| content2 =

| heading3 = 
| content3 =

<!--...up to 35th:-->

| heading35 = 
| content35 =

}}

'''Thomas H. Cowan''' (1884 - November 8, 1969) was a 20th-century radio announcer and is most known for his role in broadcasting for the first time the Baseball World Series over the airwaves. He had been the chief announcer for the countries first city owned and non-commercial radio station in the United States, New York City’s WNYC since its first broadcast in 1925. Throughout the years he had been the announcer “for the myriad of parades, receptions and celebrations from the 1920s through the 1950s, especially early on when athletes and aviators came to town ( New York City) after making or breaking world records.”<ref name="wnyc">{{cite web|title=Radio Pioneer Tommy Cowan Announces a Parade if History,|url=http://www.wnyc.org/blogs/archives/2013/may/10/radio-pioneer-cowan-announces-parade-history/|work=WNYC|accessdate=May 10, 2013}}</ref>    Since his career in radio spanned 40 years until his retirement in 1961 at age 77, he was the oldest active announcer in the radio community at the time.

== Early life Before Radio ==

Thomas H. Cowan,everyone called him Tommy<ref name="Barnouw 1966 83">{{cite book|last=Barnouw|first=Erik|title=A Tower in Babel: A History of Broadcasting in the United States Volume 1 - to 1933|year=1966|publisher=Oxford University Press|location=New York|isbn=0195004744|page=83}}</ref>  was born in Newark, New Jersey in 1884. He attended school until he was 14 years old when he went to work for The World as an office boy. In 1898 he switched jobs and became the receptionist or greeter for Thomas Edison at the Edison Laboratory in West Orange, N.J. To quote Tommy himself he called it “a kind of Grover Whalen job” where he greeted important visitors to the laboratories of Edison and Westinghouse.<ref name="Thomas1969">{{cite news|title=Thomas H. Cowan, First Voice Of City's Radio Station, Is Dead|newspaper=New York Times|date=November 12, 1969}}</ref>

== Introduction to radio and the beginnings of broadcasting station WJZ ==

Station WJZ was licensed on June 1, 1921 and was the first long term, regularly broadcasting New York Area station. The studio and the stations transmitter were located on the roof of Westinghouse Meter Plant in Newark N.J. at the corner of Plane and Orange. Even though the station had been licensed to broadcast in June of that year the station was not ready and did not start to broadcast until October 1921. The station was hastily put together for the upcoming 1921 World Series between the New York Giants and the New York Yankees to cash in on the success that the fight between Dempsey and Carpetier in July of that year, just three months before.<ref>{{cite book|last=Douglas|first=George|title=The Early Days of Radio Broadcasting|year=1987|publisher=McFarland|location=Jefferson, North Carolina and London|isbn=0-89950-285-7|page=25}}</ref>

After Westinghouse had chosen the engineer and station manager of the station, Charles B. Popenoe and his assistant engineer, George Blitzoitis, Westinghouse had to choose an announcer. Tommy was chosen for the position even though, like the other people in the studio, had no experience in radio.<ref>{{cite book|last=Douglas|first=George|title=The Early Days of Radio Broadcasting|year=1987|publisher=McFarland|location=Jefferson, North Carolina and London|isbn=0-89950-285-7|pages=26–27}}</ref> "On a day in 1921 Tommy was told to go up to the roof, to the shack. They were going to try to "make this thing talk".<ref name="Barnouw 1966 83"/>    He basically started from scratch and learned as things came his way. It was alleged that the first words uttered on the air waves by Cowan was “ You are listening to the radio telephone broadcasting station, WJZ, in Newark, N.J.”. He later repeated those same words several times not knowing if anyone was tuning in but later admitted that he doubted that anyone was actually listening.<ref name="Thomas1969" />

== The World Series broadcast for the first time to the masses ==

After the station was set up and running the question came to Tommy as what he should broadcast on the station. He had struggled with this question since the station didn't have a phonograph of their own to play music and the direction of the station and what it broadcast was left to him. It turns out that the station had turned its attention to sports casting by a lucky phone call. The call came from Sandy Hunt who was a sportswriter for the Newark Sunday Call and suggested that the station broadcast the 1921 World Series. After talking it over with the rest of the stations staff they all were warmed and ready to accept the challenge.<ref name="Douglas1987">{{cite book|last=Douglas|first=George|title=The Early Days of Radio Broadcasting|year=1987|publisher=McFarland|location=Jefferson, North Carolina and London|isbn=0-89950-285-7|page=117}}</ref>

	The two reasons that everyone agreed to the idea was that first the game was to be set between two New York baseball teams, the New York Yankees and the New York Giants, two very big rivalries. Second the games were to be played at the polo grounds which were right in the stations area and they were the only station close to the event. The only other station KDKA was 300 miles away in Pittsburgh. So the WJZ station had the perfect set up, almost.<ref name="Douglas1987" />

	Since the technology at the time wasn't advanced enough for the game to be directly broadcast from the Polo Grounds Cowan had to rely on someone being at the Polo grounds to relay a play by play of the game by telephone. He would relay that information though the microphone of the transmitter in radio shack on top of the Westinghouse plant. His voice would be the one doing the talking and not the person on the phone as he would put the plays on the air with his own words to describe the action.
	This daring feat didn't come without its share of bumps along the road.  The first hurtle to go over was the cooperation of the telephone company which saw WJZ and Westinghouse as its rival. The telephone company wouldn't accept the request by the station to provide a direct wire between the New York Stadiums and the WJZ station. So then Sandy and Tommy tried a different approach. They rented a box seat in Yankee Stadium and the Polo Grounds and put in a request again for a simple phone to be installed in the Box. After constant persuasion the phone company finally agreed and complied with their request.<ref name="Douglas1987a">{{cite book|last=Douglas|first=George|title=The Early Days of Radio Broadcasting|year=1987|publisher=McFarland|location=Jefferson, North Carolina and London|isbn=0-89950-285-7|page=118}}</ref>

	Sandy Hunt would go to the game for the Newark Sunday Call as well as for Tommy to relay the game back to him by telephone.  The communication between Sandy and Cowan was in one direction and Cowan could not ask questions of Hunt because the audience would hear it and become confused. He wasn't able to write things down or keep score so by the end of the first game he had forgotten the score and was not able to give the final result. The other trouble was the hours of sitting and a phone set against his ear. His hand became numb and his ear sore, but by the second game of the series he had gotten more comfortable by using a headphone and he became more relaxed with his transmission.<ref name="Douglas1987a" />

== Result of the Broadcast ==

The remote broadcast of the 1921 World Series by Thomas Cowan and the studio of WJZ was a success. Around 4000 pieces of mail flooded the station thanking them for their public service for the many folks that couldn’t make it to the game.<ref name="Douglas1987a" />
	This station and the work of Tommy Cowan started sports casting the United States and changed from sports to being a part of the news to its own entity. Today we take for granted that games such as baseball are on the radio and television. Even though Thomas Cowan and WJZ weren’t the first people to put sports on the air waves they were the ones to make it successful and practical. They were true radio pioneers.

The broadcast did not only effect the world of baseball but the world of radio itself. In the following years radio would be used to cover large events, bringing the news and accomplishments of the world to even larger audiences. "Radio exploded across America, broadcasting the first-ever radio commercial in 1922, the 1925 Cubs-Pirates season opener, the [[Scopes Trial|1925 Scopes monkey trial]], the [[Charles Lindbergh]] cross-Atlantic Paris landing in 1927, the [[The Long Count Fight|1927 Jack Dempsey "long-count fight"]] and much more, all while inventing the sit-com as we know it with the forerunner to "[[Amos 'n' Andy]]" in 1926 and then [[The Goldbergs (broadcast series)|"The Goldbergs"]] in 1929."<ref>{{cite web|last=Ham|first=Eldon|title=Guest commentary: Celebrating 90 years of radio broadcasts|url=http://www.stltoday.com/news/opinion/guest-commentary-celebrating-years-of-radio-broadcasts/article_aeb411bc-3b84-5292-9da4-7e51624a78ba.html|publisher=St.Louis Post-Dispach|accessdate=19 July 2013}}</ref>

== After WJZ ==

Before leaving WJZ in 1925 to join the WNYC Tommy had covered the [[1924 Democratic National Convention|Democratic National Convention of June 1924]] in Madison Square Garden.<ref name="Thomas1969" />  “WNYC’s founder Grover A. Whalen convinced Cowan to be WNYC's Chief Announcer, and his was the first voice heard when we went on the air July 8, 1924.” Says WYNC’s web article about him.<ref name="wnyc" />   Cowan stayed with the station until his retirement in 1961. During his 40-year career he covered just about every major event in New York City, from parades to presidential addresses.<ref name="wnyc" />

== Death ==

Thomas Cowan died at age 85 at his home in West Orange, N.J. on Saturday, November 8, 1969 and was survived by his three sisters.<ref name="Thomas1969" />

== External links ==

* To listen to more than 40 broadcasts with Tommy go to:[http://www.wnyc.org/people/tommy-cowan/]
* 1951 Oral Interview with Tommy by Frank Earnest Hill, (Archived Document):http://www.worldcat.org/title/reminiscences-of-thomas-h-cowan-oral-history-1951/oclc/122565183&referer=brief_results

== References ==

{{reflist}}

{{DEFAULTSORT:Cowan, Thomas}}
[[Category:1884 births]]
[[Category:1969 deaths]]
[[Category:American broadcasters]]