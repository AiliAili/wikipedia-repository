{{Infobox file format
| name          = Extensible Application Markup Language (XAML)
| icon          = 
| logo          = 
| screenshot    = 
| caption       = 
| extension     = <tt>.xaml</tt>
| mime          = application/xaml+xml
| type code     = 
| uniform type  = 
| magic         = 
| owner         = [[Microsoft]]
| released      = {{Start date|2008|06|df=yes}}<ref>{{citation
 | url = http://download.microsoft.com/download/0/A/6/0A6F7755-9AF5-448B-907D-13985ACCF53E/&#91;MS-XAML&#93;.pdf
 | deadurl = yes
 | format = PDF
 | title = &#91;MS-XAML&#93; – v1.0, Xaml Object Mapping Specification 2006
 | publisher = [[Microsoft]]
 | date = June 2006
 | accessdate = 2010-06-24}}</ref>
 | latest release version = v2009
 | latest release date = {{Start date and age|2010|04|16|df=yes}}<ref>{{citation
 | url = http://download.microsoft.com/download/0/A/6/0A6F7755-9AF5-448B-907D-13985ACCF53E/&#91;MS-XAML-2009&#93;.pdf
 | format = PDF |title=&#91;MS-XAML&#93; – v2009, XAML Object Mapping Specification 2009
 | publisher = [[Microsoft]]
 | date = April 2010
 | accessdate = 2010-06-24}}</ref><ref>{{citation
 | url = http://www.microsoft.com/downloads/details.aspx?FamilyID=52a193d1-d14f-4335-aa86-c53193e1885d&displayLang=en
 | title = Extensible Application Markup Language (XAML)
 | publisher = [[Microsoft]]
 | date = 2010-04-16
 | accessdate = 2010-06-24}}</ref>
| genre         = [[User interface markup language]]
| container for = 
| contained by  = 
| extended from = [[XML]]
| extended to   = 
| standard      = 
| free          = 
| url           = 
| used in       = [[Windows Presentation Foundation]]<br>[[Windows Workflow Foundation]]<br>[[Silverlight]]<br>[[Windows Phone|Silverlight for Windows Phone]]<br>[[Windows 8|Windows 8 Metro Apps]]<br>[[Windows Embedded Compact 7|Silverlight for Windows Embedded]]
}}

'''Extensible Application Markup Language''' ('''XAML''', {{IPAc-en||audio=En-us-xaml.ogg|ˈ|z|æ|m|əl}}) is a [[declarative programming|declarative]] [[XML]]-based language developed by [[Microsoft]] that is used for initializing structured values and objects. It is available under Microsoft's [[Open Specification Promise]].<ref>[http://www.sdtimes.com/(X(1)S(kw21wu45u03kzpnafqlanyiy))/content/article.aspx?ArticleID=31886&AspxAutoDetectCookieSupport=1 Microsoft adds XAML to 'Open Specification' list - Software Development Times On The Web<!-- Bot generated title -->]</ref> The acronym originally stood for Extensible Avalon Markup Language - ''Avalon'' being the code-name for [[Windows Presentation Foundation]] (WPF).<ref>[http://www.windows-now.com/blogs/rrelyea/archive/2004/01.aspx Rob Relyea : January 2004 - Posts<!-- Bot generated title -->]</ref>

XAML is used extensively in [[.NET Framework 3.0]] & [[.NET Framework 4.0]] technologies, particularly [[Windows Presentation Foundation|Windows Presentation Foundation (WPF)]], [[Silverlight]], [[Windows Workflow Foundation|Windows Workflow Foundation (WF)]] and [[Windows Runtime XAML Framework]] and Windows Store apps. In WPF, XAML forms a [[user interface markup language]] to define UI elements, data binding, eventing, and other features. In WF, [[workflow]]s can be defined using XAML. XAML can also be used in [[Silverlight]] applications, [[Windows Phone]] apps and [[Windows Store app]]s.

XAML elements map directly to [[Common Language Runtime]] object instances, while XAML attributes map to Common Language Runtime properties and events on those objects. XAML files can be created and edited with visual design tools like [[Microsoft Expression Blend]], [[Microsoft Visual Studio]], and the hostable [[Windows Workflow Foundation]] visual designer. They can also be created and edited with a standard [[text editor]], a code editor like [[XAMLPad]], or a graphical editor like [[Vector Architect]].

Anything that is created or implemented in XAML can be expressed using a more traditional .NET language, such as [[C Sharp (programming language)|C#]] or [[Visual Basic.NET]]. However, a key aspect of the technology is the reduced complexity needed for tools to process XAML, because it is based on XML.<ref>{{cite web
| url = http://msdn2.microsoft.com/en-us/library/ms788723.aspx
| title = XAML Syntax Terminology
| publisher = Microsoft Corporation}}</ref> Consequently, a variety of products are emerging, particularly in the WPF space, which create XAML-based applications. As XAML is simply based on XML, developers and designers are able to share and edit content freely amongst themselves without requiring compilation. XAML also benefits from being a declarative definition of the UI rather than procedural code to generate it.

==Technology==
A XAML file can be compiled into a .BAML file ([[Binary Application Markup Language]]<ref>{{cite web
 | accessdate = 2011-08-18
 | author     = unknown
 | date       = 2009-07-30
 | location   = http://www.dotnetspider.com/
 | publisher  = DOTNET Spider
 | title      = What is BAML?
 | quote      = BAML means Binary Application Markup Language, which is a compiled version of the XAML. When you compile your XAML it creates the BAML file.
 | url        = http://www.dotnetspider.com/forum/216053-What-BAML.aspx}}</ref>), which may be inserted as a resource into a .NET Framework assembly. At run-time, the framework engine extracts the .BAML file from assembly resources, parses it, and creates a corresponding WPF visual tree or workflow.

When used in Windows Presentation Foundation, XAML is used to describe visual [[user interface]]s. WPF allows for the definition of both 2D and 3D objects, rotations, animations, and a variety of other effects and features.

When used in Windows Workflow Foundation contexts, XAML is used to describe potentially long-running declarative logic, such as those created by process modeling tools and rules systems. The [[serialization]] format for workflows was previously called '''XOML''', to differentiate it from UI markup use of XAML, but now they are no longer distinguished. However, the file extension for files containing the workflow markup is still "XOML".<ref>[http://social.msdn.microsoft.com/forums/en-US/windowsworkflowfoundation/thread/590589ef-fdf0-4a1c-86b9-1cc7ad65df31/ MSDN forum post by the WF product manager]</ref><ref>[[RuleML]] and [[BPEL]] are other examples of XML-based declarative logic languages</ref>

===Templates===
XAML uses a specific way to define [[look and feel]] called ''Template''s; differing from [[Cascading Style Sheet]] syntax, it is closer to [[XBL]].<ref>{{cite web
| url = http://weblogs.asp.net/scottgu/pages/silverlight-tutorial-part-7-using-control-templates-to-customize-a-control-s-look-and-feel.aspx
| title = Silverlight Tutorial Part 7: Using Control Templates to Customize a Control's Look and Feel
| last = Guthrie
| first = Scott
| date = 2008-02-22
| accessdate = 2008-03-08}}</ref>

==Example==
This Windows Presentation Foundation example shows the text "Hello, world!" in the top-level XAML container called Canvas.

<source lang=XML>
<Canvas xmlns="http://schemas.microsoft.com/client/2007"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml">
  <TextBlock>Hello, world!</TextBlock>
</Canvas>
</source>

The schema (the xmlns="http://schemas.microsoft.com..." part) may have to be changed to work on your computer.
Using a schema that Microsoft recommends, the example can also be<ref>Microsoft XAML Overview page at
[https://msdn.microsoft.com/en-us/library/ms752059.aspx#xaml_files XAML Overview (Root element and xmlns)]</ref>

<source lang=XML>
<Canvas xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation">
  <TextBlock>Hello, world!</TextBlock>
</Canvas>
</source>

This can be integrated into a [[Web page]] if WPF is installed using [[XBAP]]s (XAML Browser Applications) that are compiled applications running in a sandboxed environment hosted within the browser. Another way is to use the [[Microsoft Silverlight|Silverlight plugin]]. The code cannot be included directly in an [[HTML]] page; rather it must be loaded into the page via [[JavaScript]]. If .NET 3.0 or later is installed, loose XAML files can also be viewed on their own in a compatible [[Web browser]] (including [[Internet Explorer]] and [[Firefox]]) in conjunction with the .NET Framework 3.0, without the need for the Silverlight plugin.<ref>[https://msdn.microsoft.com/en-us/library/aa480223.aspx#wpfandwbas_topic6 Windows Presentation Foundation on the Web: Web Browser Applications - MSDN]</ref> Loose XAML files are markup-only files limited to defining the visual content to be rendered. They are not compiled with an application.

<source lang="xml">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>XAML Example</title>
    <script type="text/javascript" src="MySilverlight.js" />
    <script type="text/javascript" src="Silver.js" />
  </head>
  <body>
    <div id="MySilverlight" >
    </div>
    <script type="text/javascript">
      createMySilverlight();
    </script>
  </body>
</html>
</source>
The ''MySilverlight.js'' file must contain the code that loads the above XAML code (as an XML file) under the ''MySilverlight'' html element.

==Differences between versions of XAML==
There are four Microsoft main implementations of XAML:
* The [[Windows Presentation Foundation]] version, which is used for the [[.NET Framework]] beginning with [[.NET Framework 3.0]]
* The [[Microsoft Silverlight History#Silverlight 3|Silverlight 3]] version
* The [[Microsoft Silverlight History#Silverlight 4|Silverlight 4]] version
* The [[Windows Runtime XAML Framework|Windows 8 XAML/Jupiter]] version

These versions have some differences in the parsing behavior.<ref>{{cite web
| url=https://msdn.microsoft.com/en-us/library/cc917841%28v=vs.95%29.aspx
| title=XAML Processing Differences Between Silverlight Versions and WPF
| publisher=[[Microsoft]]
| quote=''Silverlight includes a XAML parser that is part of the Silverlight core install. Silverlight uses different XAML parsers depending on whether your application targets Silverlight 3 or Silverlight 4. The two parsers exist side-by-side in Silverlight 4 for compatibility. In some cases, the XAML parsing behavior in Silverlight differs from the parsing behavior in Windows Presentation Foundation (WPF). WPF has its own XAML parser.''
| accessdate=2011-10-02}}</ref>

Additionally, the XAML parsing between Silverlight 3 and Silverlight 4 is not 100% [[Backward compatibility|backward compatible]]; XAML files which are accepted by Silverlight 3 may not be accepted or parsed differently in Silverlight 4.<ref>{{cite web
| url=https://msdn.microsoft.com/en-us/library/ff457753%28v=vs.95%29.aspx
| title=https://msdn.microsoft.com/en-us/library/ff457753%28v=vs.95%29.aspx
| publisher=[[Microsoft]]
| accessdate=2011-10-02}}</ref>

==Criticism of XAML GUI usage in Silverlight==
The [[European Committee for Interoperable Systems]] said in 2007 that Microsoft's use of XAML in its Silverlight product aimed to introduce content on the World Wide Web that could only be accessed from the [[Microsoft Windows|Windows]] platform.<ref>{{cite web
| url=http://www.itwire.com/it-industry-news/strategy/8988-microsoft-runs-into-eu-vista-charges
| title=Microsoft runs into EU Vista charges
| publisher=itwire.com
| date=2007-01-28
| accessdate=22 August 2013}}</ref><ref>{{cite web|last=Reimer|first=Jeremy|title=European committee chair accuses Microsoft of hijacking the web|url=http://arstechnica.com/uncategorized/2007/01/8715/|work=Ars Technica|publisher=Condé Nast|accessdate=22 August 2013}}</ref> Using a plugin, XAML is viewable in some non-Microsoft browsers on Windows, [[Linux]], and [[Mac OS X|Mac]]; and Microsoft supported [[Novell]]'s Silverlight viewer for [[GNU]]/[[Linux]] called [[Moonlight (runtime)|Moonlight]].<ref>{{cite web
| url = http://blogs.zdnet.com/microsoft/?p=695
| title = Microsoft officially ‘extends support’ for Novell’s Silverlight Linux port
| publisher = zdnet.com
| last = Foley
| first = Mary Jo
| date = 2007-09-25
| accessdate = 2007-10-13}}</ref> As of January 2010, Moonlight 2 was compatible with Silverlight 2, but development of Moonlight was later discontinued.<ref>
{{cite web
| url = http://tirania.org/blog/archive/2009/Dec-17.html
| title = Releasing Moonlight 2, Roadmap to Moonlight 3 and 4| publisher = Miguel de Icaza| accessdate = 2009-12-17}}</ref>

==See also==
* [[Comparison of user interface markup languages]]
* [[EMML]]
* [[Interface Builder]]
* [[JavaFX]]
* [[Layout manager]]
* [[List of user interface markup languages]]
* [[Open XML Paper Specification]]
* [[XUL]]
* [[ZK Framework]]

==References==
{{Reflist|30em}}

==External links==
* XAML services
* [https://msdn.microsoft.com/en-us/windows/uwp/xaml-platform/xaml-overview XAML overview]
* XAML reference: [https://msdn.microsoft.com/en-us/library/System.Windows.Markup.aspx System.Windows.Markup Namespace] and [https://msdn.microsoft.com/en-us/library/system.xaml.aspx System.Xaml Namespace].

{{.NET Framework}}
{{Widget toolkits}}

{{Graphics file formats}}

[[Category:.NET Framework terminology]]
[[Category:Declarative markup languages]]
[[Category:Declarative programming languages]]
[[Category:Markup languages]]
[[Category:Microsoft application programming interfaces]]
[[Category:Microsoft Windows multimedia technology]]
[[Category:User interface markup languages]]
[[Category:Vector graphics markup languages]]
[[Category:XML-based standards]]