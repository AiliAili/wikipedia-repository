The '''IEEE International Electron Devices Meeting (IEDM)''' is an annual micro- and nanoelectronics conference held each December that serves as a forum for reporting technological breakthroughs in the areas of [[semiconductor]] and related device technologies, design, manufacturing, physics, modeling and circuit-device interaction.<ref>{{cite news | first = Nicolas | last = Mokhoff | title = Start of a beautiful friendship | date = 18 Dec 2006 | publisher = UBM Tech | url = http://www.eetimes.com/electronics-news/4067782/Start-of-a-beautiful-friendship | work = EE Times | accessdate = 2013-04-25}}</ref>

The IEDM is the conference where semiconductor industry pioneer Gordon Moore first updated and explained the prediction he made 10 years earlier which has come to be known as “[[Moore’s Law]].” Moore’s Law states that the complexity of integrated circuits would double approximately every two years.<ref>{{cite web|url=http://www.computerhistory.org/semiconductor/timeline/1965-Moore.html |title=1965: "Moore's Law" Predicts the Future of Integrated Circuits &#124; The Silicon Engine &#124; Computer History Museum |publisher=Computerhistory.org |date= |accessdate=2017-03-11}}</ref><ref>{{cite web|url=http://www.newelectronics.co.uk/electronics-technology/the-economics-of-chip-manufacture-on-advanced-technologies/35562/ |title=The economics of chip manufacture on advanced technologies |publisher=Newelectronics.co.uk |date=2011-07-26 |accessdate=2017-03-11}}</ref>

'''IEDM''' brings together managers, engineers, and scientists from industry, academia, and government around the world to discuss nanometer-scale [[CMOS]] transistor technology, advanced memory, displays, sensors, [[Microelectromechanical systems|MEMS devices]], novel quantum and [[nanoscale]] devices using emerging phenomena, [[optoelectronics]], power, [[energy harvesting]], and ultra-high-speed devices, as well as process technology and device modeling and simulation.  The conference also encompasses discussions and presentations on devices in [[silicon]], [[Compound semiconductor|compound]] and [[Organic semiconductor|organic]] semiconductors, and emerging material systems.<ref>{{cite news | first = Gail | last = Purvis | title = IEDM, where the device is king | date = 15 Nov 2012 | url = http://www.powersystemsdesign.com/iedm-where-the-device-is-king?a=1&c=1151 | work = Power Systems Design | accessdate = 2013-04-25}}</ref>  In addition to technical paper presentations, IEDM includes multiple plenary presentations, panel sessions, tutorials, short courses, and invited talks and an entrepreneurship panel session conducted by experts in the field from around the globe.

The 62nd annual IEEE International Electron Devices Meeting (IEDM) will take place December 3–7, 2016 at the San Francisco Union Square Hilton hotel. This year for the first time there will be a supplier exhibition.

== Sponsor ==

The International Electron Devices Meeting is sponsored by the [http://eds.ieee.org/ Electron Devices Society] of the [http://www.ieee.org/ Institute of Electrical and Electronics Engineers] (IEEE).

== History ==

The First Annual Technical Meeting on Electron Devices (renamed the International Electron Devices Meeting in the mid-1960s) took place on October 24–25, 1955 at the [[Shoreham Hotel]] in Washington D.C. with approximately 700 scientists and engineers in attendance.  At that time, the seven-year-old transistor and the [[Vacuum tube|electron tube]] reigned as the predominant electron-device technology.  Fifty-four papers were presented on the then state-of-the-art in electron device technology, the majority of them from four U.S. companies -- [[Bell Telephone Laboratories]], [[RCA Corporation]], [[Hughes Aircraft Co.]] and [[Sylvania Electric Products]].  The need for an electron devices meeting was driven by two factors: commercial opportunities in the fast-growing new "[[Solid-state (electronics)|solid-state]]" branch of electronics, and the U.S. government's desire for solid-state [[Electronic components|components]] and better [[microwave]] tubes for [[aerospace]] and defense.<ref>{{cite journal | title = A production model K-band backward wave oscillator | journal = IRE Transactions on Electron Devices | date = April 1956 | first = A.W. | last = McEwan | volume = 3 | issue = 2 | page = 108 | doi = 10.1109/T-ED.1956.14115 | url = http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=1472034&url=http%3A%2F%2Fieeexplore.ieee.org%2Fiel5%2F16%2F31547%2F01472034 | accessdate = 2013-04-25}}</ref>

== IEDM 2015 ==
The 2015 International Electron Devices Meeting took place at the Washington Hilton Hotel from December 5–9, 2015. The major topics <ref>{{cite web|author=Paul McLellan |url=http://community.cadence.com/cadence_blogs_8/b/breakfast-bytes/archive/2015/12/11/iedm2015 |title=IEDM: the International Electron Devices Meeting - Breakfast Bytes - Cadence Blogs - Cadence Community |publisher=Community.cadence.com |date=2015-12-11 |accessdate=2017-03-11}}</ref><ref>{{cite web|author=by sdavis |url=http://electroiq.com/chipworks_real_chips_blog/2015/12/02/a-look-ahead-at-iedm-2015/ |title=A Look Ahead at IEDM 2015 &#124; Siliconica |publisher=Electroiq.com |date=2015-12-02 |accessdate=2017-03-11}}</ref> included:
* ultra-small transistors <ref>{{cite web|last=Stevenson |first=Richard |url=http://spectrum.ieee.org/semiconductors/devices/nanowire-transistors-could-let-you-talk-text-and-tweet-longer |title=Nanowire Transistors Could Let You Talk, Text, and Tweet Longer - IEEE Spectrum |publisher=Spectrum.ieee.org |date=2016-01-26 |accessdate=2017-03-11}}</ref>
* advanced memories <ref>{{cite web|author=Tetsuo Nozawa |url=http://techon.nikkeibp.co.jp/atclen/news_en/15mk/122400270/ |title=Samsung: DRAM Can Be Scaled Down to 10nm - Nikkei Technology Online |publisher=Techon.nikkeibp.co.jp |date=2015-12-24 |accessdate=2017-03-11}}</ref>
* low-power devices for mobile & Internet of Things (IoT) applications <ref>{{cite web|author=02:55 PM |url=https://www.semiwiki.com/forum/content/5301-iedm-blogs-%C2%96-part-3-%C2%96-global-foundries-22fdx-briefing.html |title=IEDM Blogs – Part 3 – Global Foundries 22FDX Briefing |publisher=SemiWiki.com |date= |accessdate=2017-03-11}}</ref>  
* alternatives to silicon transistors <ref>{{cite web|author=Ashok Bindra |url=http://electronics360.globalspec.com/article/6286/iedm-divulges-advances-in-wide-bandgap-devices |title=IEDM Divulges Advances in Wide Bandgap Devices &#124; Electronics360 |publisher=Electronics360.globalspec.com |date= |accessdate=2017-03-11}}</ref>
* 3D integrated circuit (IC) technology <ref>{{cite web|last=Turley |first=Jim |url=http://www.eejournal.com/archives/articles/20160201-micron/ |title=How It’s Built: Micron/Intel 3D NAND |publisher=Eejournal.com |date=2016-02-01 |accessdate=2017-03-11}}</ref>
* a broad range of papers addressing some of the fastest-growing specialized areas in micro/nanoelectronics, including silicon photonics,<ref>{{cite web|url=http://www.laserfocusworld.com/articles/2015/11/germanium-tin-laser-for-silicon-photonics-is-cmos-compatible.html|title=Germanium-tin laser for silicon photonics is CMOS compatible|author=|date=|work=laserfocusworld.com|accessdate=11 March 2017}}</ref> physically flexible circuits <ref>{{cite web|url=http://eecatalog.com/chipdesign/2016/02/09/2015-iedm-slide-11-rf-cmos-circuits-on-flexible-application-specific-substrates/ |title=2015 IEDM Slide 11: RF CMOS Circuits on Flexible, Application-Specific Substrates &#124; Chip Design |publisher=Eecatalog.com |date=2016-02-09 |accessdate=2017-03-11}}</ref> and brain-inspired computing <ref>{{cite web|url=http://www.eetimes.com/author.asp?section_id=36&doc_id=1328557 |title=IEDM 2015 NV Memory and Brain Functions |publisher=EE Times |date= |accessdate=2017-03-11}}</ref>

== IEDM 2014 ==
The 2014 International Electron Devices Meeting took place at the Hilton San Francisco Union Square from December 15–17, 2014. The 2014 edition of the IEDM emphasized:
* 14&nbsp;nm FinFET transistor processes <ref>{{cite web|url=http://semimd.com/blog/2015/01/05/solid-doping-for-bulk-finfets/ |title=Solid Doping for Bulk FinFETs &#124; Semiconductor Manufacturing & Design Community |publisher=Semimd.com |date=2015-01-05 |accessdate=2017-03-11}}</ref>
* power electronics <ref>{{cite web|url=http://www.smartgridelectronics.net/2015/01/sic-projected-significant-impact-industrial-markets/|title=Safe and High-Quality Electronic Products -- Smart Grid Electronics|author=|date=|work=smartgridelectronics.net|accessdate=11 March 2017}}</ref>
* bio-sensors and MEMS/NEMS technologies for medical applications <ref>{{cite web|url=http://www.mdtmag.com/blogs/2014/12/innovative-technologies-dna-diagnostics-and-health-monitoring |title=Innovative Technologies for DNA Diagnostics and Health Monitoring |publisher=Mdtmag.com |date= |accessdate=2017-03-11}}</ref>
* new memory devices <ref>{{cite web|last=Morris |first=Kevin |url=http://eejournal.com/blog/tram-and-pcm-at-iedm/ |title=TRAM and PCM at IEDM |publisher=EE Journal |date=2015-02-11 |accessdate=2017-03-11}}</ref>
* display and sensor technologies <ref>{{cite web|url=http://mandetech.com/2015/01/19/cmos-processing-advances-imaging/ |title=CMOS processing advances imaging : Media & Entertainment Technology |publisher=Mandetech.com |date=2015-01-19 |accessdate=2017-03-11}}</ref>
* 3D device architectures <ref>{{cite web|url=http://www.3dincites.com/2015/01/iedm-2014-3d-short-course-highlights-3d-memory-cubes-system-design/ |title=IEDM 2014 3D Shortcourse highlights Importance of 3D Memory Cubes |publisher=3D InCites |date=2015-01-05 |accessdate=2017-03-11}}</ref>

== IEDM 2013 ==

The 2013 International Electron Devices Meeting took place at the [[Washington Hilton|Hilton Washington Hotel]] from December 9–11, 2013 and focused on:

* Non-planar FinFETs on bulk silicon and fully depleted planar silicon-on-insulator (FD-SOI) devices, as the two mainstream advanced technology approach for continued scaling <ref>{{cite web|url=http://www.advancedsubstratenews.com/2013/12/iedm-13-part-2-more-soi-and-advanced-substrate-papers/|title=IEDM ’13 (Part 2): More SOI and Advanced Substrate Papers  - Advanced Substrate News|author=|date=|work=advancedsubstratenews.com|accessdate=11 March 2017}}</ref>
* Non-silicon devices such as tunneling FETs (TFETs), which hold promise as a way to control transistor off-state leakage by getting around the sub-60 mV/decade steep subthreshold slope barrier.<ref>{{cite web|url=http://semiengineering.com/manufacturing-bits-dec-17/|title=Semiconductor Engineering .:.   Manufacturing Bits: Dec. 17|author=|date=|work=semiengineering.com|accessdate=11 March 2017}}</ref>
* 3D integrated circuit for stacking of heterogeneous chips for future system on chip (SOC)
* Various non-volatile memory technologies such as resistive memories (ReRAM or RRAM), which are attracting interest because of their potential to deliver faster write times and greater endurance than flash.<ref>{{cite web|url=http://www.eetimes.com/author.asp?section_id=36&doc_id=1320419|title=Resistive Non-Volatile Memory at IEDM 2013 - EE Times|author=|date=|work=eetimes.com|accessdate=11 March 2017}}</ref>
* Biomedical electronics, which are attracting widespread interest because of the potential for low-cost DNA-sequencing on a chip<ref>{{cite web|url=http://www.mdtmag.com/blogs/2013/12/iedm-conference-gives-opportunity-learn-about-state-art-chips-biology-and-medicine#.UqZHR2eA1LM|title=IEDM Conference Gives Opportunity to Learn About State-of-the-Art Chips for Biology and Medicine|author=|date=9 December 2013|work=mdtmag.com|accessdate=11 March 2017}}</ref>
* Power electronic devices for automotive and smart grid applications

== References ==

{{Reflist}}

== Additional Information ==
* [http://ieee-iedm.org/ IEDM]
* IEDM on [https://www.facebook.com/IEEE.IEDM Facebook]
* IEDM on [https://twitter.com/ieee_iedm Twitter]: @ieee_iedm
* [http://eds.ieee.org/ Electron Device Society] of the IEEE
* [http://www.ieee.org IEEE]
* [http://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=iedm&x=-834&y=-177 IEEE Xplore Digital Library]

== Related Conferences ==
* [http://www.vlsisymposium.org/ Symposia on VLSI Technology and Circuits]
* [http://isscc.org/ International Solid-State Circuits Conference]
* [http://drc.ee.psu.edu/ Device Research Conference]
* [http://www.hotchips.org/ Hot Chips: A Symposium of High Performance Chips]

{{IEEE conferences}}

[[Category:IEEE conferences]]
[[Category:Computer science conferences]]