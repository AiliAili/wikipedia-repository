{{redirect|Slip string|the toy usage|Yo-yo}}
[[Image:Glider Instrument Panel.png|thumb|right|A red yarn yaw string (top center) on the canopy of a [[Schempp-Hirth Janus]]-C [[Glider (sailplane)|glider]] as seen by the pilot in flight. The yaw string and [[slip-skid indicator]] ball show a slight [[slip (aircraft)|slip]] with a positive [[sideslip angle]].]]
The '''yaw string''', also known as a '''slip string''', is a simple device for indicating a [[slip (aerodynamic)|slip]] or [[skid (aerodynamic)|skid]] in an [[aircraft]] in flight. It performs the same function as the [[slip-skid indicator]] ball, but is more sensitive, and does not require the pilot to look down at the instrument panel.<ref>{{cite book
 | last = Reichmann
 | first = Helmut
 | editor = Lert, Peter
 | title = Cross Country Soaring (Streckensegelflug)
 | year = 1988
 | origyear = 1975
 | edition = English
 | publisher = Soaring Society of America
 | id = LCCCN 77-86598
 | page = 77
 }}</ref> Technically, it measures [[sideslip angle]], not [[yaw angle]],<ref>{{cite web
| url = http://www.av8n.com/how/htm/multi.html#@default1498
| title = See How It Flies
| accessdate = 2007-01-21
| author = Denker, John S.
| year = 2002
| quote = The slip string is commonly referred to as a "yaw string", even though it measures the slip angle, not the yaw angle (i.e. heading)....
}}</ref> but this indicates how the aircraft must be yawed to return the sideslip angle to zero.

It is typically constructed from a short piece or [[Tuft (aeronautics)|tuft]] of yarn placed in the free air stream where it is visible to the pilot.<ref>{{cite book
 | title = Glider Flying Handbook
 | year = 2003
 | publisher = U.S. Federal Aviation Administration
 | location = U.S. Government Printing Office, Washington D.C.
 | id = FAA-8083-13_GFH
 | pages = 4–14
 | url=http://www.faa.gov/library/manuals/aircraft/glider_handbook/
 }}</ref> In closed cockpit aircraft, it is usually taped to the aircraft [[Canopy (aircraft)|canopy]]. It may also be mounted on the aircraft's nose, either directly on the skin, or elevated on a mast, in which case it may also be fitted with a small paper cone at the trailing end.<ref>{{cite book
 | last = Selvidge
 | first = Harner, S.D.
 | editor = Licher, Rose Marie
 | others = 
 | title = American Soaring Handbook
 | origyear = 1963
 | edition = 2nd
 | year = 1976
 | publisher = Soaring Society of America 
 | id = LCCCN 59-15668
 | pages = 30–31
 | chapter = Ch. 7 : Equipment I
 }}</ref> They are commonly used on gliders, but may also be found on [[jet aircraft]] (especially [[fighter aircraft|fighters]]), [[ultralight aircraft]], [[light-sport aircraft]], [[autogyro]]s,<ref>{{cite book
 | title = Rotorcraft Flying Handbook
 | year = 2000
 | publisher = U.S. Federal Aviation Administration
 | location = U.S. Government Printing Office, Washington D.C.
 | id = FAA-8083-21
 | pages = 18–4
 | url=http://www.faa.gov/library/manuals/aircraft/media/faa-h-8083-21.pdf
 }}</ref> [[airplanes]] and [[helicopters]]. Its usefulness on airplanes with a [[tractor configuration]] (single [[propeller (aircraft)|propeller]] at the nose) is limited because the propeller creates turbulence and the [[spiral slipstream]] displaces the string to one side.<ref>{{cite web
| url = http://www.challengers101.com/YawString.html
| title = The Yaw String
| accessdate = 2007-01-21
| author = Hurt, George
}}</ref>

The yaw string is considered a primary flight reference instrument on [[Glider (sailplane)|gliders]], which must be flown with near zero sideslip angle to reduce [[Drag (physics)|drag]] as much as possible. It is valued for its high sensitivity, and the fact that it is presented in a [[head-up display]]. Even the most sophisticated modern racing sailplanes are fitted with yaw strings by their pilots, who reference them constantly throughout the flight.

==History==
The yaw string dates from the earliest days of aviation, and actually was the first [[flight instruments|flight instrument]]. The [[Wright Brothers]] used a yaw string on their [[Wright Glider|1902 glider]] tied on their front mounted elevator.<ref>{{cite web
| url = http://www.first-to-fly.com/Adventure/Hangar/gliderrep.htm
| title = 1902 Glider Replica
| accessdate = 2007-01-21
| author = Wright Brothers Aeroplane Company and Museum of Pioneer Aviation
| quote = For all the progress that has been made in a century of aviation, the Ventur 2 still has a "yaw string" attached where the pilot can see it. As does the Wright glider. This was the first flight instrument....
}}</ref> Wilbur Wright is credited with its invention, having applied it concurrently with the movable rudder invented by his brother Orville in October 1902,<ref>{{cite journal 
|quote=<nowiki>[Wilbur Wright]</nowiki> responded, "Yes...we will install a short piece of string out front where we can see it. This string will tell us all we need to know!" (paraphrased) Wilbur had just invented the yaw string — the first aircraft instrument.
|last=Knauff 
|first=Tom 
|date=Feb–Mar 1995 
|title=Inventing the Rudder 
|journal=Free flight 
|volume=1995 
|issue=1 
|page=26 
|id=– 2557 
|url=http://www.wgc.mb.ca/sac/freeflight/95_01.pdf 
|accessdate=2006-01-21|format=PDF 
|issn=0827-2557}}</ref> although others may have used it before. [[Glenn Curtiss]] also used it on his early airplanes.

[[Image:FAA-8083-3A Fig 12-16.PNG|thumb|right|Diagram showing yaw string deflection on a multi-engine airplane flown incorrectly with wings level after an engine failure.]]

==Use on powered aircraft==

Yaw strings are also fitted to the [[Lockheed U-2]] high-altitude [[surveillance aircraft]].<ref>{{cite web
| url = http://www.barryschiff.com/high_flight.htm
| title = High Flight
| accessdate = 2007-01-21
| author = Schiff, Barry
| year = 2006
| quote = A yaw string (like those used on sailplanes) above each canopy silently admonishes those who allow the aircraft to slip or skid when maneuvering.
}}</ref> A flat spin, caused by excessive sideslip even in level flight,  happens much more easily at high altitudes. Some light twin-engine airplane pilots place yaw strings on their aircraft to help maintain control in the event of an engine failure, because the [[slip-skid indicator]] ball is not accurate in this case.<ref>{{cite book
| author = Paul A. Craig
| title = Multiengine flying
| publisher = McGraw-Hill
| location = New York
| year = 2004
| isbn = 0-07-142139-4
| quote = You will see that with the wings level the yaw string does not lay along the centerline, yet the ball is in the center...
}}</ref>
In a multiengine airplane with an inoperative engine,
the centered ball is no longer the indicator of zero
sideslip due to [[asymmetrical thrust]]. The yaw string is the only
flight instrument that will directly tell the pilot the
flight conditions for zero sideslip.<ref>{{cite book
 | title = Airplane Flying Handbook
 | year = 2004
 | publisher = U.S. Federal Aviation Administration
 | location = U.S. Government Printing Office, Washington D.C.
 | id = FAA-8083-3A
 | pages = 12–23
 | url=http://www.faa.gov/library/manuals/aircraft/airplane_handbook/
 }}</ref>

Yaw strings are also used on some (especially smaller) helicopters.

== Side string ==
[[File:Seitenfaden.PNG|thumb|Side string]]
A variation of the yaw string is the side string, used in gliders for a determination of the angle of attack. In this way the best L/D speed, the best thermalling speed and the stall speed can be measured independently of all parameters like air speed, all up weight, acceleration due to turning or stick movements and gusts<ref>[http://www.dg-flugzeugbau.de/index.php?id=seitenfaden-e Use of the side string, DG Flugzeugbau]</ref>

Investigations of the side string and on its use in glider flight were performed from the [[Akaflieg]] [[Cologne]].<ref>[http://www.akaflieg-koeln.de/news/newsdetails/alles-haengt-am-seit-den-enfaden-neue-version/ Use of the side string (German), Akaflieg Köln]</ref>

== See also ==
*[[Glider (sailplane)]]
*[[Gliding]]
*[[Sideslip angle]]
*[[Slip (aerodynamic)]]
*[[Tell-tale]]
*[[Tuft]]
*[[Critical engine]]

==References==
{{reflist}}
{{USGovernment|sourceURL=[http://www.faa.gov/library/manuals/aviation/instrument_flying_handbook/ Instrument Flying Handbook]}}

{{Flight instruments}}

[[Category:Aircraft instruments]]
[[Category:Gliding]]