{{Infobox military conflict
|conflict=Battle of Agounennda
|partof=[[Algerian War]]
|image=
|caption=
|date=23–25 May 1957
|place=Agounennda, [[Algeria]]
|result=French victory
|combatant1={{flagicon|France}}[[France]]
|combatant2={{flagicon|Algeria}}[[National Liberation Front (Algeria)|FLN]]
|commander1=[[Marcel Bigeard]]
|commander2=Si Azzedine
|strength1=700 men<ref name="Windrow35">{{harvnb|Windrow|1997|p=35}}.</ref>
|strength2=300 men<ref name="Horne252">{{harvnb|Horne|1977|pp=251–253}}.</ref>
|casualties1=8 killed,<br>29 wounded<ref name="Windrow35"/>
|casualties2=96 killed,<br> 12 captured,<br>unknown number of wounded<ref name="Windrow35"/>
}}
{{Campaignbox Algerian War}}
The '''Battle of Agounennda''' was an engagement of the [[Algerian War]] fought 23–25 May 1957 between the [[France|French]] [[3rd Marine Infantry Parachute Regiment|3rd Colonial Parachute Regiment]] under [[Lieutenant Colonel]] [[Marcel Bigeard]] and [[National Liberation Front (Algeria)|FLN]]’s Commando 41 (‘Ali Khodja’) under Si Azzedine. Bigeard and his regiment were sent to hunt down the Commando after it had carried out several successful ambushes against French units. They met at Agounennda where the French paratroopers tried to ambush the FLN force, but the FLN discovered the French and instead concentrated their force against an outlying French company.

Bigeard managed to redeploy and surround the FLN force; it withdrew successfully albeit with heavy casualties. However, the French were unable to recover large caches of weapons - the FLN having taken them off the field. The battle altered FLN tactics, reminding them that they were unable to meet the French in open battle. Conversely, it gave the French renewed confidence in a military victory. However, sceptics on both sides saw it as evidence that neither faction would ever gain ascendancy in the other's arena. The FLN avoided military combat with the French, relying on guerilla warfare.<ref name="Horne254">{{harvnb|Horne|1977|pp=253–254}}.</ref>

==Background==
In early May 1957, Commando 41 (‘Ali Khodja’) of the Algerian [[National Liberation Front (Algeria)|National Liberation Front]] (FLN) routed a [[Spahi|Spahi unit]], killing 60 men while only losing 7 men themselves. Roughly two weeks later, on 21 May, the same unit ambushed a detachment of the 5th Algerian [[Tirailleur]] Battalion near [[Médéa]]. A captain and 15 ''[[tirailleurs]]'' were killed in the engagement, while others where persuaded to defect, the FLN only lost 1 killed and 2 wounded.<ref name="Horne252" />

Lieutenant Colonel [[Marcel Bigeard]] and his [[3rd Marine Infantry Parachute Regiment|3rd Colonial Parachute Regiment]] (3e RPC) - recently returned from successful operations in [[Algiers]] - were tasked with hunting down the unit and stopping it.<ref name="Horne252" /> French intelligence suggested that the unit would move west, tasked with escorting commanders of Wilya 4 to a rendezvous with other FLN forces near Médéa. Bigeard decided to ambush Commando 41 on their way and picked Agounenda on the Oued Boulbane, a known FLN route. Bigeard and the 3e RPC was transported by truck from their base at Sidi Ferruch to Hill 895, where they arrived by 01:30 on 23 May. From there Bigeard and his 700 paratroopers made a cold, 4-hour night approach march under strict noise and light discipline. Before dawn the paratroopers was in place and concealed. The Headquarters and mortars were on Hill 1298; the 1st, 2nd and 3rd Companies and the Reconnaissance Squadron on foot were spread over 10&nbsp;km on four crests overlooking the enemy’s probable route. The 4th and Support Companies were in reserve, while helicopters and ground-attack aircraft were on stand-by at Médéa.<ref name="Horne252" />

==Battle==
[[File:Marcel Bigeard img 3546.jpg|thumb|right|250px|Bigeard, a popular leader and veteran of [[Dien Bien Phu]],<ref name="Windrow(2004)237">{{harvnb|Windrow|2004|p=237}}.</ref> led his men personally on the field having returned from the [[Battle of Algiers (1957)|Battle of Algiers]] only days before.<ref name="Horne252" />]]
At 10:30 the most northerly and exposed company, the 3rd “Blue” Company under Captain Llamby, radioed in that they had sighted a large FLN force approaching his position above the north bank of the Oued Boulbane from the east; the company opened fire at 10:45. The forewarned Si Azzedine, leading a column of at least three companies, was attempting to outflank the 3rd Company from the north. Captain Llamby and his 100 men, outnumbered three to one, came under intense pressure.<ref name="Horne252" />

The helicopters in reserve at Médéa were already on their way, Bigeard ordered the Support Company to be lifted onto the high ground north of 3rd Company. The first sticks jumped from the helicopters at 10:55 and the whole Support Company was in action by 11:30. Meanwhile, 1st and 2nd Companies force-marched on foot to support the 3rd Company, helicopters lifted the unengaged Reconnaissance Squadron and 4th Company to a position slightly northeast of the battle between 3rd Company and the FLN force.<ref name="Horne252" />

The FLN force took the low-lying Oued Boulbane, but was dominated from higher ground by the French paratroopers: 3rd, 4th and Support Companies from the north of it and 1st and 2nd Companies plus the Headquarters from the south. In a series of running battles over an area of some 30 square kilometres that lasted 48 hours, Commando 41 and at least two other katibas made several vigorous counterattacks which came to hand-to-hand fighting. Despite support from tactical aircraft, the paratroopers were too thinly stretched to maintain a tight cordon; around 200 FLN fighters managed to slip away through the cracks.<ref name="Horne252" />

==Aftermath==
The FLN left behind 96 dead and 12 prisoners, but managed to withdraw with most of their wounded, while the French paratroopers lost 8 killed and 29 wounded, they also managed to free 5 prisoners. The French were also unable to recover large caches of weapons - the FLN having taken their guns with them. Despite the high FLN casualties, the French only recovered 45 weapons from the battlefield. The French was encouraged by the success air portability on the battlefield. The FLN on the other hand learned that large-scale engagements in the heart of the country had to be avoided at all costs.<ref name="Horne252" />

Other observers draw more pessimistic conclusions for the French, because if an elite unit like the [[3rd Marine Infantry Parachute Regiment|3rd Colonial Parachute Regiment]] with a great commander like [[Marcel Bigeard]] failed to score a total victory on their own terms, was there much hope of winning at all?<ref name="Horne252" />

==Notes==
{{reflist}}

==References==
* {{citation
| last = Horne
| first = Alistair
| author-link = Alistair Horne
| publication-date = 2006
| year = 1977
| title = A Savage War of Peace: Algeria 1954-1962
| publisher = New York Review
| isbn = 978-1-59017-218-6
}};
* {{citation
| last = Windrow
| first = Martin
| author-link = Martin Windrow
| publication-date = 1997
| year = 1997
| title = The Algerian War 1954-62
| publisher = Osprey publishing
| isbn = 1-85532-658-2
}};
*{{citation
| last = Windrow
| first = Martin
| author-link = Martin Windrow
| publication-date = 2004
|  year = 2004
| title = The Last Valley: Dien Bien Phu and the French Defeat in Vietnam
| publisher = Weidenfeld & Nicolson
| isbn = 0-297-84671-X
}};
{{coord missing|Algeria}}

[[Category:Conflicts in 1957|Agounennda]]
[[Category:Battles of the Algerian War|Agounennda]]
[[Category:Battles involving France|Agounennda]]
[[Category:Battles involving Algeria|Agounennda]]
[[Category:1957 in Algeria]]
[[Category:May 1957 events]]