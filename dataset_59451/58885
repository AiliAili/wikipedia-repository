{{Infobox journal
| title = Medieval Archaeology
| cover = [[File:Medieval Archaeology.jpg]]
| discipline = Archaeology
| abbreviation = Mediev. Archaeol.
| editor = Sarah Semple
| publisher = [[Maney Publishing]] on behalf of the [[Society for Medieval Archaeology]]
| country = United Kingdom
| history = 1957-present
| impact = 
| impact-year = 
| frequency = Annual
| openaccess = 
| license =
| website = http://www.medievalarchaeology.co.uk/index.php/publications-2/journal/
| link1 = http://www.maneyonline.com/loi/med
| link1-name = Online access
| ISSN = 0076-6097
| eISSN = 1745-817X
| OCLC = 478949447
| LCCN = 61037482
}}
'''''Medieval Archaeology''''' is an annual [[peer-reviewed]] [[academic journal]] covering the [[archaeology]] of the [[medieval]] period, especially in the United Kingdom and Ireland. It was established in 1957 and is published by [[Maney Publishing]] on behalf of the [[Society for Medieval Archaeology]]. The [[editor-in-chief]] is Sarah Semple ([[University of Durham]]).<ref>{{Cite web|url=http://www.medievalarchaeology.co.uk/index.php/the-sma/sma-council/meet-the-council/editor/|title=Honorary Editor {{!}} The Society for Medieval Archaeology|website=www.medievalarchaeology.co.uk|access-date=2016-07-10}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic Search Complete]]
* [[Arts & Humanities Citation Index]]
* [[British Humanities Index]]
* [[Current Contents]]/Arts & Humanities
* [[MLA International Bibliography]]
* [[Scopus]]
}}

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.medievalarchaeology.co.uk/index.php/publications-2/journal/}}
*[http://archaeologydataservice.ac.uk/archives/view/med_arch/ Free archive of the first fifty volumes]
* [http://www.medievalarchaeology.co.uk/ Society for Medieval Archaeology]

[[Category:Archaeology journals]]
[[Category:Publications established in 1957]]
[[Category:English-language journals]]
[[Category:Annual journals]]


{{archaeology-journal-stub}}