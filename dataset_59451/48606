{{Orphan|date=January 2015}}

'''Sir Edward Hughes''' (4 July 1919 – 16 October 1997) was an eminent Melbourne [[Colorectal surgery|colorectal surgeon]].  He was a professor of surgery at [[Monash University]] and served as president of the [[Royal Australasian College of Surgeons]] and chairman of the Menzies Foundation.  He played a significant role in influencing the Victorian Government to become the first jurisdiction in the world to introduce legislation for the compulsory use of [[seat belts]] in motor vehicles.

==Early life==

Edward Stuart Reginald Hughes was born on 4 July 1919  in [[Bruthen, Victoria]].<ref>Hughes's birth certificate shows his birth date as 1 July 1919 but his family has always insisted that this record is incorrect.</ref>  He was the third of four children of Reginald Hawkins Hughes and Annie Grace Langford.  He was known throughout his life as "Bill", a nickname arising from the fact that he was born during the prime ministership of [[Billy Hughes|William Morris ("Billy") Hughes]].

===Education===

Hughes attended St Paul's Preparatory School in Malvern, Victoria between 1924 and 1926 and then [[Melbourne Church of England Grammar School]] between 1927 and 1937.  At school, he distinguished himself in [[Australian rules football]], tennis and athletics.<ref>See Liber Melburniensis 1915-1995, p. 199</ref>

Hughes studied medicine at the University of Melbourne between 1938 and 1943.  In the course of his undergraduate studies, he was awarded the Dwight's Prize in Anatomy and Physiology (1940), the Walter and Eliza Hall Exhibition in Pathology (1941), the Keith Levi Memorial Scholarship for Medicine (1943) and was placed first in his final year class list in obstetrics and gynaecology (1943).  He represented Melbourne University in Australian rules football and was awarded an Australian Blue for football in 1938.

==Professional experience==

Hughes began work as a junior medical resident officer at the [[Royal Melbourne Hospital]] in 1943.  Between 1946 and 1949 he occupied various positions in Oxford and London whilst studying for his [[Fellowship of the Royal College of Surgeons]] of England (FRCS) and then in 1950 he resumed work in Australia as an assistant surgeon at the Royal Melbourne Hospital and the [[Royal Children's Hospital]] in Melbourne.

Variously described as "brilliant",<ref>Butler, JM, Time Isn't Long Enough, p 93</ref> a "surgeon of international repute"<ref>Beasley, AW, The Mantle of Surgery, p 149</ref>  and "one of the most distinguished Australian surgeons of the second half of the twentieth century",<ref>The Alfred Healthcare Group Heritage Committee, Alfred Hospital: Faces and Places, p 99</ref> Hughes was appointed in 1974 as Professor of Surgery at Monash University, a position he retained until 1984.  Between 1977 and 1979, he was also Associate Dean of the Monash University Faculty of Medicine and, throughout this period, he was a consultant surgeon and a member of the Board of Management at the Alfred Hospital, Melbourne.

A "man of big ideas and concepts" ,<ref>Nicks, R, "Australian Surgeons and Society" (1980) 50 Aust. N.Z. J. Surg. 659, 667</ref> Hughes founded the Ileostomy Association of Victoria in 1957, <ref>Ileostomy Association Victoria, http://www.ileostomyvic.org.au/about.html</ref> the first Australian association to cater for the needs of ostomates, and in 1977 he convened and chaired the inaugural meeting of the Australian Association of Stomal Therapists,<ref>AASTN: Australian Association for Stomal Therapy Nurses, http://www.stomaltherapy.com/ourhistory.php</ref> now the Australian Association of Stomal Therapy Nurses.

Hughes had a longstanding involvement with the Royal Australasian College of Surgeons.  He was elected as a member of the Victorian State Committee in 1966 and appointed to the Court of Examiners in 1968.  He became Censor-in-Chief of the College in 1969 and ultimately served as President between 1975 and 1978.  The Hughes Room at the Royal Australasian College of Surgeons premises in Melbourne is named in his honour.<ref>Beasley, AW, The Mantle of Surgery, p 148</ref>

A significant initiative undertaken by Hughes in the course of his involvement with the Royal Australasian College of Surgeons was to establish the College's Road Trauma Committee.  The advocacy of the Committee is widely acknowledged as directly resulting in the introduction by the Victorian Government of the world's first compulsory seat belt legislation in 1970.<ref>Beasley, AW, The Mantle of Surgery, p 168</ref>   It was said that "his determination to make Victoria the first place in the world for compulsory wearing of seatbelts meant that he saved far more lives through this single initiative than he could ever save with his surgical endeavours" .<ref>Associate Professor John Masterson, The Melbourne Age, 30 October 1998</ref>

In 1979, Hughes was appointed founding chairman of the Sir Robert Menzies National Foundation.  He held the position until 1988 when the Foundation merged with the Sir Robert Menzies Memorial Trust, whereupon he held the position of Deputy Chairman of the merged entity until 1996.<ref>See Menzies Foundation Annual Report 1998, http://www.menziesfoundation.org.au/annualreports/1998/1998hughes.html</ref>
In the course of his career, Hughes authored or co-authored 6 text books and wrote or co-wrote over 300 papers, many of which were published in leading peer-reviewed journals such as the Medical Journal of Australia, the Australian and New Zealand Journal of Surgery, the Lancet and the British Medical Journal.

==Asian involvement==

Hughes had an intense interest in advancing the surgical skills of practitioners in South East Asia. He organised a series of medical teams from the Royal Melbourne Hospital to visit Vietnam in 1964, followed by a team from St Vincent's Hospital, Melbourne in 1965 and teams from Prince Henry's Hospital and the Alfred Hospital in 1966. As chairman of the Royal Melbourne Hospital's Vietnam Committee, he travelled extensively through South East Asia demonstrating operating techniques with "energetic enthusiasm" .<ref>Terry, S, House of Love: Life in a Vietnamese Hospital, p.144</ref>
In India in 1964, and in Vietnam in 1966, he was accompanied on his travels by fellow surgeon Sir Edward ("Weary") Dunlop.<ref>Ebury, S, Weary: the Life of Sir Edward Dunlop, p.584</ref>

==Awards and recognition==

Hughes was made a Commander of the [[Order of the British Empire]] (CBE) in 1971 for "services to medicine and international relations", and he was knighted in 1977 for "distinguished services to medicine in the field of surgery".

Hughes was awarded numerous honorary fellowships of surgical colleges including the American College of Surgeons (1976), the Royal College of Physicians and Surgeons of Canada (1977) and the Royal College of Surgeons of England (1985).

Between 1964 and 1965, Hughes was the recipient of the Sir Arthur Sims Commonwealth Travelling Scholarship, resulting in an extended lecturing commitment in Hong Kong, Malaysia, India and South Africa.

Significant lectures delivered by Hughes during this career included the Moynihan Lecture in London (1965), the Hunterian Oration in London (1972), the Ismail Oration in Kuala Lumpur (1976) and the Digby Memorial Lecture in Hong Kong (1979).

==Military service==

Between 1939 and 1940, Hughes was a member of the AMF [[4th Division (Australia)|4th Division]], 6th Field Ambulance.  He had enlisted at the outbreak of the Second World War but was ordered to return to his studies in 1940 so as to complete his medical training. Between 1950 and 1951, he served as a lieutenant-colonel and senior surgeon at Kure Hospital, Japan, as a member of the British Commonwealth Occupation Forces in the course of the Korean War. In later years, Hughes served as a colonel in the Australian Army Reserve.

==Family==

Hughes married Alison Clare Lelean on 30 December 1944.  They had four children, Jennifer (born 1947), Gordon (1949), Ann (1954) and John (1957).

==Death==

Sir Edward died on 16 October 1998 after a 15-year battle with Parkinson's Disease.

== Sources ==
Books in which Hughes's work is mentioned include the following:
* Beasley, A.W, Portraits at the Royal Australasian College of Surgeons, Royal Australasian College of Surgeons, 1993 pp.&nbsp;48–49
* Beasley, A.W, The Mantle of Surgery, Royal Australasian College of Surgeons, 2002 pp.&nbsp;23, 86, 91, 111, 131, 147-8, 149, 151, 156, 166, 183, 190, 191, 195, 237
* Butler, J. M, Time Isn't Long Enough, The Hawthorne Press, 1971, pp 70, 93
* Ebury,S, Weary: the Life of Sir Edward Dunlop, Viking Australia, 1994, pp 564, 584, 585, 587
* Kingsley Norris, F, No Memory for Pain, William Heinemann, 1970, p 270-271
* Terry, S, House of Love: Life in a Vietnamese Hospital, Lansdowne Press, 1966, p 144
* The Alfred Healthcare Group Heritage Committee, Alfred Hospital: Faces and Places, The Alfred Healthcare Group, 1996, pp 99–101, 114
* Watts, J.C, Surgeon at War, George Allen & Unwin, 1955, p 144
* Wigglesworth, E.C. (ed), The Sir Robert Menzies Memorial Trust and Foundation: the First Fifteen Years 1978-1993, The Menzies Foundation, 1996, Ch 2

== References ==
{{reflist}}

{{DEFAULTSORT:Hughes, Edward}}
[[Category:Australian surgeons]]
[[Category:1919 births]]
[[Category:1998 deaths]]
[[Category:Australian Commanders of the Order of the British Empire]]
[[Category:20th-century Australian medical doctors]] 
[[Category:Medical doctors from Sydney]] 
[[Category:Colorectal surgeons]]