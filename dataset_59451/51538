{{Infobox journal
| title = Indian Journal of Gender Studies
| cover = 
| discipline = [[Women's studies]]
| abbreviation = Indian J. Gend. Stud.
| editor = Malavika Karlekar<br />Leela Kasturi
| publisher = [[Sage Publications]] on behalf of the [[Indian Council of Social Science Research]]
| country = India
| frequency = Triannual
| history = 1994–present
| impact = 0.231
| impact-year = 2015
| website = http://www.sagepub.com/journals/Journal200917/title
| link1 = http://ijg.sagepub.com/current.dtl
| link1-name = Online access
| link2 = http://ijg.sagepub.com/content
| link2-name = Online archive
| ISSN = 0971-5215
| eISSN = 0973-0672
| OCLC = 60628369
| LCCN = 94904841
| CODEN = IJGSF4
}}
The '''''Indian Journal of Gender Studies''''' is a triannual [[peer-reviewed]] [[academic journal]] with a focus on a [[holistic]] understanding of society, particularly [[gender]]. The [[editors-in-chief]] are Malavika Karlekar and Leela Kasturi ([[Indian Council of Social Science Research]]). The journal is published by [[Sage Publications]] on behalf of the [[Indian Council of Social Science Research]].<ref name=About>{{cite web |title=About the Ttitle |url=http://www.sagepub.com/journals/Journal200917/title |publisher=[[Sage Publications]] |work=Indian Journal of Gender Studies |accessdate=20 February 2015}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=Abstract>{{cite web |title=Indian Journal of Gender Studies: Abstracting and indexing |url=http://www.sagepub.com/journals/Journal200917/abstractIndexing |publisher=[[Sage Publications]] |accessdate=20 February 2015}}</ref>
{{columns-list|colwidth=30em|
* [[EBSCOhost]]
* [[EconLit]]
* [[Indian Citation Index]]
* [[International Bibliography of the Social Sciences]]
* [[ProQuest|ProQuest databases]]
* [[Research Papers in Economics]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 0.231, ranking it 33rd out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://ijg.sagepub.com/}}
* [http://www.cwds.ac.in/ijgs.htm Centre for Women's Development Studies, Indian Council of Social Science Research, New Delhi, India]

{{DEFAULTSORT:Indian Journal of Gender Studies }}
[[Category:English-language journals]]
[[Category:Publications established in 1994]]
[[Category:SAGE Publications academic journals]]
[[Category:Triannual journals]]
[[Category:Women's studies journals]]