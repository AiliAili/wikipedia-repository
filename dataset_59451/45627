{{Infobox engineer
|image                =
|image_width          = 
|caption              = 
|name                 = Wladimir Talanczuk
|nationality          =  [[Ukraine|Ukrainian]] - [[Canada|Canadian]]
|birth_date           = 
|birth_place          = [[Ukraine]], [[USSR]]
|death_date           = 
|death_place          = 
|education            =[[Institute for Aviation Specialists]], [[Poland]]
|spouse               =
|parents              =
|children             =
|discipline           =
|institutions         = [[AeroKlub Wrocław]]<br>[[Birdman Enterprises]]
|practice_name        =  
|significant_projects = [[Birdman Chinook|Birdman WT-11 Chinook]]
|significant_design   = 
|significant_advance  =[[Ultralight aircraft]]<br />[[Hang Gliding|Hang gliders]]
|significant_awards   =
}}

[[File:Birdman Chinook 2S C-IBPE 0004.jpeg|thumb|right|[[Birdman Chinook|Birdman Chinook 2S]]]]
[[File:Birdman Chinook 2S C-IBPE 0003.jpeg|thumb|right|[[Birdman Chinook|Birdman Chinook 2S]]]]
 
'''Wladimir Talanczuk''' (also known by his [[anglicized]] name ''Vladimir Talanczuk'') is a [[Ukraine|Ukrainian]]- born [[aeronautical engineer]] known for his [[hang gliding|hang glider]] and [[ultralight aircraft]] designs.<ref name="Birdman1">Jones, Terry: ''About The Designer - Vladimir Talanczuk - Trained in The European Tradition''. Birdman Enterprises, 1984.</ref><ref name="Janes7980">Taylor, John WR, ''Jane's All The World's Aircraft 1979-80'' page 599, Janes Publishing Limited.</ref><ref name="Janes8081">Taylor, John WR, ''Jane's All The World's Aircraft 1980-81'' page 597, Janes Publishing Limited.</ref><ref name="Janes8788">Taylor, John WR, ''Jane's All The World's Aircraft 1987-88'' page 544, Janes Publishing Limited.</ref>

==Career==
Talanczuk graduated from the [[Poland|Polish]] [[Institute for Aviation Specialists]] in 1970 as an aeronautical engineer and immediately embarked upon a career as a designer of hang gliders and light aircraft. He worked with [[Tadensca Dobczynski]], the Polish amateur aircraft builder, assisting in the design and construction of more than thirty Dobczynski designs.<ref name="Birdman1" />

Talanczuk qualified as a light aircraft pilot, logging 1200 hours of flight time. In 1972 he  began to seriously devote his time to hang gliding. While working for the [[AeroKlub Wrocław]] he designed his ''Mars'' series of hang gliders, including the [[Talanczuk WT-6 Mars-S|WT-6 Mars-S]], [[Talanczuk WT-7 Mars-2S|WT-7 Mars-2S]] and [[Talanczuk WT-8 Mars-Agat|WT-8 Mars-Agat]]. Throughout 1979 he was a member of the Polish National Hang Gliding Team and competed in the [[Hang gliding#Competition|World Hang Gliding Championships]] at [[Grenoble]], [[France]], flying a Mars hang glider of his own design. The hang glider he flew at that event was at that time the largest [[wingspan]] hang glider flown.<ref name="Birdman1" /><ref name="Janes7980" /><ref name="Janes8081" />

Talanczuk also served as a consultant to the Polish National Aero Club in the fields of accident investigation and analysis. He designed several light aircraft and completed a single-seat [[gyroplane]] during his time in Poland.<ref name="Birdman1" />

He emigrated to [[Canada]] in 1981 and began working for hang glider and ultralight aircraft manufacturer [[Birdman Enterprises]], of [[Edmonton]], [[Alberta]] shortly after his arrival, filling the position of Chief Engineer and Designer.<ref name="Birdman1" /><ref name="Janes8788" />

Talanczuk's first project at Birdman was the design of a new ultralight aircraft to replace the [[Birdman Atlas]] in production. The company's stated design goals for the aircraft were: good flying characteristics, simplicity of construction and maximization of aesthetics.<ref name="Birdman1" /><ref name="Birdman2">Jones, Terry: ''Birdman WT-11 Chinook - Design Philosophy - A Third-Generation Ultralight''. Birdman Enterprises, 1984.</ref>

Designer Talanczuk stated his own additional project intentions:

{{cquote|An Ultralight is not only a fun machine, it should also be usable for utility purposes -- training, fishing trips, crop-spraying and even for freight carrying. But an Ultralight should be affordable by many people, so it shouldn‘t become expensive. The wings, for example, can't be complicated. They should be easy to build and fix.<ref name="Birdman2" /> }}

Talanczuk chose an [[airfoil]] that was created by Dr Dave Marsden at the [[University of Alberta]], the UA 80/1. The aircraft was his eleventh design and was designated the ''WT-11 Chinook'', although in 1987 the company redesignated it ''1S'' (for 1 seat) to conform to their own nomenclature.<ref name="Janes8788" /><ref name="Birdman2" />

Birdman President Terry Jones assessed the results of the WT-11 design in a press release, saying:<ref name="Birdman2" />

{{cquote|To judge from the final product, Talanczuk has achieved his aims in the Birdman WT 11 CHINOOK. It is a good-looking and compact machine that exhibits gentle but crisp flying characteristics; is strong yet simple in construction and offer a variety of applications from weekend flying to bushplane practicality. With a low initial purchase price and excellent fuel economy, the machine is also inexpensive to own and operate. In all, it's a perfect little aviation package.<ref name="Birdman2" />}}

The WT-11 received both critical acclaim and commercial success. The single seat WT-11 was followed by Talanczuk's two-seat [[Trainer (aircraft)|trainer]] version which Birdman designated as the [[Birdman Chinook|Chinook 2S]] (2 seater). A total of over 700 WT-11s and 2S Chinooks were completed before Birdman Enterprises went out of business in late 1987.<ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page E-9. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref>

Talanczuk's Chinook design was resurrected in 1989, when it was redesigned by [[Aircraft Sales and Parts]] President Brent Holomis as the [[Birdman Chinook|ASAP Chinook 2 Plus]], an [[Ultralight aircraft (Canada)#Advanced ultra-light aeroplane|Advanced Ultralight Aeroplane]] that remains in production today.<ref name="ASAPHist">{{cite web|url = http://www.ultralight.ca/asaphistory.htm|title = ASAP History|accessdate = 2009-08-10|last = [[Aircraft Sales and Parts]]|authorlink = |year = 2002}}</ref><ref name="AULA">{{cite web|url =http://www.tc.gc.ca/civilaviation/general/CCARCS/advancedullist.htm|title = Listing of Models Eligible to be Registered as Advanced Ultra-Light Aeroplanes (AULA) |accessdate = 2009-08-05|last = [[Transport Canada]]|authorlink = |date=May 2009}}</ref><ref name="JohnsonJan08">{{cite web|url = http://www.bydanjohnson.com/index.cfm?b=3&m=5&i=15|title = Lightplane Offerings From Canada|accessdate = 2009-08-12|last = Johnson|first = Dan|authorlink = |date=January 2008}}</ref><ref name="Hunt2">Hunt, Adam: ''Flying the ASAP Chinook Plus 2 with Ottawa’s Capital Air Sports''. [[Canadian Owners and Pilots Association]] ''COPA Flight'', May 2002.</ref>

== Aircraft ==

{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Summary of aircraft designed by Wladimir Talanczuk'''
|- bgcolor="#efefef"
! Model name
! First flight
! Number built
! Type
|-
|align=left| '''[[Talanczuk WT-1|WT-1]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-2|WT-2]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-3|WT-3]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-4|WT-4]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-5|WT-5]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-6 Mars-S|WT-6 Mars-S]]'''
|align=center| 
|align=center| 
|align=left| Single-place [[hang glider]]
|-
|align=left| '''[[Talanczuk WT-7 Mars-2S|WT-7 Mars-2S]]'''
|align=center| 
|align=center| 
|align=left| Single-place [[hang glider]]
|-
|align=left| '''[[Talanczuk WT-8 Mars-Agat|WT-8 Mars-Agat]]'''
|align=center| 
|align=center| 8 built by 1979<ref name="Janes7980" />
|align=left| Single-place [[hang glider]]
|-
|align=left| '''[[Talanczuk WT-9|WT-9]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Talanczuk WT-10|WT-10]]'''
|align=center| 
|align=center| 
|align=left| 
|-
|align=left| '''[[Birdman Chinook|Chinook WT-11]]'''
|align=center| {{avyear|1982}}
|align=center| 700 WT-11 and 2S
|align=left| Single-place [[ultralight aircraft]]
|-
|align=left| '''[[Birdman Chinook|Chinook 2S]]'''
|align=center| {{avyear|1984}}
|align=center|  700 WT-11 and 2S
|align=left| Two-place [[ultralight aircraft]]
|-
|}

==References==
{{reflist}}

==External links==
{{Commons category|Birdman Chinook}}

{{Birdman Enterprises}}

{{DEFAULTSORT:Talanczuk, Wladimir}}
[[Category:Aviation in Canada]]