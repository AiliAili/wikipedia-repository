{{Infobox journal
| title = European Journal of Women's Studies
| cover = [[File:Front cover of european journal of women's studies.jpg]]
| editor = Kathy Davis, Gail Lewis
| discipline = [[Women's studies]]
| formernames =
| abbreviation = Eur. J. Wom. Stud.
| publisher = [[SAGE Publications]]
| country =
| frequency = Quarterly
| history = 1994-present
| openaccess =
| license =
| impact = 1.160
| impact-year = 2015
| website = http://www.uk.sagepub.com/journals/Journal200932
| link1 = http://ejw.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ejw.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 644465838
| LCCN = 94648660
| CODEN = EJWSE5
| ISSN = 1350-5068
| eISSN = 
}}
The '''''European Journal of Women's Studies''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[women's studies]]. It is published quarterly by [[SAGE Publications]]. The journal's [[editors-in-chief]] are Kathy Davis ([[Utrecht University]]) and Gail Lewis ([[Open University]]). It publishes articles, reviews, conference reports, topical and polemical pieces, and overviews on the state of women's studies in various European countries. The journal has published special issues on subjects including women and war, gender and religion, and the politics of identification.

== Abstracting and indexing ==
The ''European Journal of Women's Studies'' is abstracted and indexed in Studies on Women & Gender Abstracts, [[British Humanities Index]], [[International Bibliography of the Social Sciences]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2015 [[impact factor]] is 1.160, ranking it 14th out of 40 journals in the category "Women's Studies".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences  |series=[[Web of Science]] |postscript=.}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://ejw.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1994]]
[[Category:Women's studies journals]]