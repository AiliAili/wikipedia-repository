{{Infobox journal
| title = Proceedings of the Institution of Mechanical Engineers, Part L: Journal of Materials: Design and Applications
| cover = [[File:Proceedings of the IMechE - L - journal cover.jpg]]
| editor = Lucas F.M. da Silva
| discipline = [[Materials engineering]], [[materials science]]
| peer-reviewed = 
| former_names = 
| abbreviation = Proc. Inst. Mech. Eng. L J. Mater. Des. Appl.
| publisher = [[Sage Publications]]
| country = United Kingdom
| frequency = Quarterly
| history = 1999-present
| openaccess = 
| license = 
| impact = 0.746
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal202025
| link1 = http://pil.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pil.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 782918935
| LCCN = sn99038536
| CODEN = PIMAFG
| ISSN = 1464-4207
| eISSN = 2041-3076
}}
The '''''Proceedings of the Institution of Mechanical Engineers, Part L: Journal of Materials: Design and Applications''''' is a [[peer-reviewed]] [[scientific journal]] that covers the usage and design of materials for application in [[engineering]]. The journal was established in 1999 and is published by [[Sage Publications]] on behalf of the [[Institution of Mechanical Engineers]].<ref>[http://www.imeche.org/knowledge/publications Institution of Mechanical Engineers publications page]. Retrieved 9 June 2014.</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.746, ranking it 183rd out of 251 journals in the category "Materials Science, Multidisciplinary".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Materials Science, Multidisciplinary |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal202025}}
{{Institution of Mechanical Engineers}}
[[Category:Engineering journals]]
[[Category:English-language journals]]
[[Category:Institution of Mechanical Engineers academic journals]]
[[Category:Publications established in 1999]]
[[Category:Quarterly journals]]
[[Category:SAGE Publications academic journals]]
{{Engineering-stub}}