<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Passe-Partout
 | image=de Marcay Passe-Partout.png
 | caption=Passe-Partout at the 1919 Paris Salon
}}{{Infobox Aircraft Type
 | type=Light sporting aircraft
 | national origin=[[France]]
 | manufacturer= [[SAECA Edmond de Marçay]] (''Société Anonyme d'Etudes et de Construction Aéronautique Edmond de Marçay'')
 | designer=
 | first flight=Q1, 1920
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''de Marçay Passe-Partout''' (de Marçay Master-key) was a small, low-powered single seat sport and touring aircraft, built in [[France]] just after [[World War I]].

==Design and development==

The Passe-Partout was the smallest and lightest de Marçay aircraft of the three on display at the Paris Aero Salon of 1919. It had a very low power engine, the same {{convert|10|hp|kW|abbr=on}} [[ABC 8 hp]] adapted flat-twin motorcycle engine that powered the [[English Electric Wren]].  Flight magazine doubted its practicality with this engine.<ref name=Flight1/> 

It was a [[biplane#Bay|single bay biplane]] with a single [[interplane strut]] on each side defining a bay braced with a single [[flying wire]] and a single [[flying wire|landing wire]]. Both wings  were two spar structures; there was marked forward [[stagger (aeronautics)|stagger]] but no [[dihedral (aeronautics)|dihedral]] The interplane struts were slender at the top but smoothly widened towards their feet, linking the upper rear spar to both lower wing spars. The narrow upper joint provided a fixed point about which control wires could warp the [[trailing edge]]. Short [[cabane strut]]s from the fuselage supported the centre of the upper wing. <ref name=Flight1/>

The Passe-Partout had a [[monocoque]] fuselage of rounded rectangular cross-section.  Its engine was mounted, with cylinders exposed, in the upper nose. The pilot's open cockpit placed him just aft of the upper trailing edge but over the lower wing because of the stagger.  At the rear a [[plywood]] covered [[tailplane]] was mounted high on the fuselage and fitted with [[aircraft fabric covering|fabric covered]] [[elevator (aeronautics)|elevators]]. Both the [[fin]] and rounded [[rudder]] were also ply covered. Its  fixed [[landing gear]] was of the conventional tailskid type with mainwheels on a single axle rubber rubber sprung from a frame consisting of two V-form struts from the lower fuselage with a single cross-member.<ref name=Flight1/><ref name=Flight0/>

The Passe-Partout had still to fly when it appeared at the Paris Salon in December 1919<ref name=Laero1/> but it had flown by the following May.<ref name=Flight2/>  Marçay continued to advertise it until at least October 1920<ref name=Laero2/> but Flight's doubts about its practicality seem to have been justified for in May 1928, when the de Marçay company ceased to exist, Les Ailes noted that the Passe-Partout had undertaken only a few, inconclusive trials and further, that de Marçay himself saw it more of a curiosity than a practical aircraft.<ref name=LAiles/>

==Specifications ==
{{Aircraft specs
|ref=Flight, 13 May 1920<ref name=Flight2/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=one
|length ft=12
|length in=5.5
|length note=
|span ft=13
|span in=3
|span note=
|height ft=4
|height in=6
|height note=
|wing area sqft=91.5
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight lb=230
|empty weight note=
|gross weight lb=417
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[ABC 8 hp]] 
|eng1 type= flat-twin motorcycle engine
|eng1 hp=10
|eng1 note=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed mph=68
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->

|ceiling ft=3300
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|power/mass=

|more performance=
}}

==References==
{{reflist|2|refs=

<ref name=Flight0>{{cite journal |date=1 January 1920 |title=The Paris Aero Show at a Glance|journal= [[Flight International|Flight]]|volume=XII |issue=1 |pages=16-17  |url=http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200017.html}}</ref>

<ref name=Flight1>{{cite journal |date=15 January 1920 |title=The Paris Aero Show 1919|journal= [[Flight International|Flight]]|volume=XII |issue=3 |pages=63-4  |url= http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200065.html}}</ref>

<ref name=Flight2>{{cite journal |date=13 May 1920 |title= Small Sporting Aeroplanes|journal= [[Flight International|Flight]]|volume=XII |issue=20 |pages=552  |url= http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200522.html}}</ref>

<ref name=Laero1>{{cite journal |date=1–15 February 1920 |title=Les Avions actuels et le 6<sup>e</sup> Salon Aéronautique|journal=L'Aérophile|volume=28 |issue=3-4 |pages=34|url=http://gallica.bnf.fr/ark:/12148/bpt6k6551870h/f3.image }}</ref>

<ref name=Laero2>{{cite journal |date=1–15 October 1920 |title=Marçay advertisement|journal=L'Aérophile|volume=28 |issue=19-20|pages=XVI|url=http://gallica.bnf.fr/ark:/12148/bpt6k6551878t/f30 }}</ref>

<ref name=LAiles>{{cite journal|date=17 May 1928 |title=Allo! Voici|journal=Les Ailes|volume=361 |issue= |pages=3|url=http://gallica.bnf.fr/ark:/12148/bpt6k6568392t/f8 }}</ref>

}}


<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

{{DEFAULTSORT:De Marcay Passe-Partout}}
[[Category:French sport aircraft 1920–1929]]
[[Category:De Marcay aircraft|Passe-Partout]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]