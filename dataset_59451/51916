{{Infobox journal
| cover = [[File:Journal of Interprofessional Care.jpg|150px]]
| editor = Professor Scott Reeves
| discipline = 
| abbreviation = J. Interprof. Care
| publisher = [[Taylor & Francis]]
| country =
| frequency = Bimonthly
| history = 1986-present
| openaccess = 
| license =
| impact = 1.399
| impact-year = 2014
| website = http://informahealthcare.com/journal/jic
| link1 = http://informahealthcare.com/toc/jic/current
| link1-name = Online access
| link2 = http://informahealthcare.com/loi/jic
| link2-name = Online archive
| JSTOR =
| OCLC =
| LCCN =
| CODEN = JINCFT
| ISSN = 1356-1820
| eISSN = 1469-9567
}}
The '''''Journal of Interprofessional Care''''' is a bimonthly [[peer-reviewed]] [[medical journal]] that covers education, practice, and research in [[health care|health and social care]].<ref>{{cite web |url=http://informahealthcare.com/page/Description?journalCode=jic#AimsAndScope |title= Journal of Interprofessional Care - Aims and Scope |accessdate=2010-01-13 |format= |work=}}</ref>

== Aims & Scope ==
The '''''Journal of Interprofessional Care''''' aims to disseminate research and new developments in the field of interprofessional education and practice. We welcome contributions containing an explicit interprofessional focus, and involving a range of settings, professions, and fields. Areas of practice covered include primary, community and hospital care, health education and public health, and beyond health and social care into fields such as criminal justice and primary/elementary education. Papers introducing additional interprofessional views, for example, from a community development or environmental design perspective, are welcome. The Journal is disseminated internationally and encourages submissions from around the world.

The '''''Journal of Interprofessional Care''''' publishes the following types of articles:
* Peer-reviewed original research articles, systematic/analytical reviews and theoretical papers that focus on an element of interprofessional education or practice.
* Peer-reviewed short reports that describe research in progress or completed, or an innovation in the field of interprofessional care.
* In addition, each issue of the Journal contains editorials and book reviews. Suggestions for editorials and book reviews need to be discussed with the Editor-in-Chief and Book Reviews Editors respectively, before submission
The Journal was established in 1986 and is published by [[Taylor & Francis]]. The [[editor-in-chief]] is Professor Scott Reeves ([[Kingston University]] and [[St George's, University of London]]).<ref>{{cite web |url=http://informahealthcare.com/page/jic/EditorialAdvisoryBoard |title=Editorial Board Members |accessdate=2010-01-13 |format= |work=Journal of Interprofessional Care |publisher=Informa Healthcare}}</ref>

== Publication History ==

=== Currently known as ===
* Journal of Interprofessional Care (1992 - current)

=== Formerly known as ===
* Holistic Medicine (1986 - 1991)

== Subjects Covered by this Journal ==
Allied Health; Community Health; Community Social Work; Health Education and Promotion; Health and Social Care; Public Health Policy and Practice; Social Work and Social Policy

== Abstracting & Indexing  ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Applied Social Sciences Index and Abstracts]]
* [[Cambridge Scientific Abstracts]]
* [[CINAHL]]
* [[EMBASE]]/[[Excerpta Medica]]
* [[e-psyche]]
* [[Index Medicus]]/[[MEDLINE]]
* [[PsycINFO]]
* [[PsycLIT]]
* [[Psychological Abstracts]]
* [[Scopus]]
* [[Sociological Abstracts]]
}}

'''2014 Journal Citation Reports'''® Ranks ''Journal of Interprofessional Care'' 70th out of 89 journals in Health Care Sciences & Services (S) and 55th out of 71 journals in the Health Policy & Services (Ss) with a '''2014 Impact Factor''' of 1.399

== Related Websites ==
[http://tandfonline.com/toc/ijic20/current Official Website]

'''Twitter:''' <nowiki>https://twitter.com/jicare</nowiki>

'''Blog:''' <nowiki>http://jinterprofessionalc.blogspot.co.uk/</nowiki>

'''LinkedIn:''' <nowiki>https://www.linkedin.com/groups/Journal-Interprofessional-Care-3697259/about</nowiki>

'''Facebook:''' <nowiki>https://www.facebook.com/groups/254334390083/</nowiki>

== See also ==
* [[Health human resources]]
* [[Interprofessional education]] in health care

== References ==
{{reflist}}

== External links ==
* {{Official website|http://informahealthcare.com/journal/jic}}

[[Category:Healthcare journals]]
[[Category:Publications established in 1986]]
[[Category:Bimonthly journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]