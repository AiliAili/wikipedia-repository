{{Use mdy dates|date=June 2014}}
{{Infobox song
| Name     = Beautiful, Dirty, Rich
| Cover    = Lady Gaga - Beautiful, Dirty, Rich.png
| Alt      = An image of Lady Gaga wearing a red, hooded jacket in front of a black background. Her head is tilted backwards as dollar bills fall around her. At the top of the image, "LADY GAGA" is written in large white text, and below are the words "Beautiful", "Dirty", and "Rich" written in red.
| Caption  = 
| Type     = [[Promotional recording|Promotional single]]
| Artist   = [[Lady Gaga]]
| Album    = [[The Fame]]
| Released = {{start date|2008|9|16}}
| Format   = [[music download|Digital download]]
| Recorded = 150 Studios ([[Parsippany-Troy Hills, New Jersey]])
| Genre    = [[Dance-pop]]
| Length   = 2:53
| Label    = {{hlist|Streamline|[[Kon Live Distribution|Kon Live]]|[[Cherrytree Records|Cherrytree]]|[[Interscope Records|Interscope]]}}
| Writer   = {{hlist|Lady Gaga|[[Rob Fusari]]}}
| Producer = Rob Fusari
| prev     = "[[Eh, Eh (Nothing Else I Can Say)]]"
| prev_no  = 5
| track_no = 6
| next     = "The Fame"
| next_no  = 7
}}

"'''Beautiful, Dirty, Rich'''" is a song by American singer [[Lady Gaga]] from her debut studio album, ''[[The Fame]]'' (2008). It was released as a promotional single from the album on September 16, 2008.<ref name="discography">{{cite web|url=http://www.ladygaga.com/music/beautiful-dirty-rich|title=Lady Gaga : Single : Beautiful, Dirty, Rich|publisher=LadyGaga.com|accessdate= August 16, 2009}}</ref> It is an uptempo dance-pop song that makes heavy use of synthesizers, and is lyrically about Gaga's experiences as a struggling artist in the [[Lower East Side]]. She wrote the song while she was, as she says, "doing a lot of drugs" and "trying to figure things out."

The song was met with critical acclaim, with reviewers complimenting the lyrics and the fun nature of the song. It also achieved minor commercial success, peaking at number 83 on the [[UK Singles Chart]]. Two versions were released of the song's accompanying music video {{ndash}} one was intercut with clips of the [[American Broadcasting Company|ABC]] series ''[[Dirty Sexy Money]]'', serving as a promotion for the show, and the other was the full-length video. Both versions feature Gaga and company dancing around in various locations of a mansion, and [[Money burning|burning money]].

Gaga has performed the song live several times, including on her first headlining [[The Fame Ball Tour]], where she performed the song while wearing a futuristic bustier with silver triangular panels. The performance was praised by critics for Gaga's "strong" vocals and her energy, and also for giving the show a strong beginning.

==Background==
Gaga said the song "was just me trying to figure things out", and she later stated that she "was doing a lot of drugs" when she wrote the song. She claims it is about how "whoever you are or where[ver] you live – you can self-proclaim this inner fame based on your personal style, and your opinions about art and the world, despite being conscious of it", and also about her experiences as a struggling artist working in the [[Lower East Side]].<ref name="bio">{{cite web|url=http://www.ladygaga.com/bio |title=Lady Gaga : Biography|publisher= LadyGaga.com|accessdate= August 10, 2009}}</ref> The line "Daddy, I'm so sorry, I'm so s-s-sorry, yeah", according to Gaga, was inspired by "rich kids" in the area who she would hear calling their parents for money to buy drugs with. She added that "ultimately what I want people to take from it is 'Bang-bang.' No matter who you are and where you come from, you can feel beautiful and dirty rich."<ref name="about.com">{{cite web|url=http://dancemusic.about.com/od/artistshomepages/a/LadyGagaInt_2.htm |title=Lady Gaga Interview {{ndash}} Interview with Lady Gaga|last=Slomowicz|first=Ron|date=June 10, 2008|publisher=[[About.com]]|accessdate=August 10, 2009}}</ref>

==Composition==
{{ listen
| pos         = left
| filename    = Lady Gaga_-_Beautiful Dirty Rich.ogg
| title       = Beautiful, Dirty, Rich
| description = A sample of Gaga's "Beautiful, Dirty, Rich". This section includes the "Daddy, I'm so sorry" pre-chorus.
}}

"Beautiful, Dirty, Rich" is an uptempo dance-pop song which is more synth-heavy compared to the mostly electronic songs on ''The Fame''. According to the sheet music published at Musicnotes.com by [[Sony/ATV Music Publishing]], the song has a moderate electro-dance-pop [[Groove (music)|groove]] and it is composed in the key of [[B minor]] with a [[tempo]] of 120 beats per minute. The song is set in [[common time]], and Gaga's vocal range spans from A<sub>3</sub> to D<sub>5</sub>. It has a basic sequence of Bm–D<sub>5</sub>–A{{music|flat}}–Bm–D<sub>5</sub>–A{{music|flat}} as its [[chord progression]].<ref name="SheetMusic">{{cite web|url=http://www.musicnotes.com/sheetmusic/mtdFPE.asp?ppn=MN0076210|title=Lady Gaga "Beautiful, Dirty, Rich" Sheet Music |publisher=Musicnotes.com|accessdate=June 25, 2014}}</ref>

During recording of the song, Gaga was reluctant to add any dance-oriented beats to the song, and insisted on keeping its original rock version. However, music producer [[Rob Fusari]] convinced her that a [[drum machine]] would not hurt her integrity, telling her that [[Queen (band)|Queen]], who was one of Gaga's musical inspirations, used it in their music. Fusari commented, "I think that's what finally got her to give it a shot, [...] We finished 'Beautiful, Dirty, Rich' that day. It's one of the songs on her debut album."<ref>{{cite news|url=http://www.nydailynews.com/entertainment/music-arts/lady-gaga-stefani-germanotta-new-york-convent-sacred-heart-super-stardom-article-1.194731|title=How Lady Gaga took Stefani Germanotta from New York's Convent of the Sacred Heart to super stardom|date=February 7, 2010|last=O'Keeffe|first=Michael|work=[[Daily News (New York)|Daily News]]|location=New York|accessdate=February 10, 2010}}</ref>

==Reception==
"Beautiful, Dirty, Rich" received general acclaim from critics. Matthew Chisling of [[AllMusic]] used the song and "[[Paparazzi (Lady Gaga song)|Paparazzi]]" as examples of how ''The Fame'''s lyrics "salt and pepper the album with a nasty, club-friendly feeling of fun and feistiness that an excellent, well-produced dance album should have."<ref name="allmusic">{{cite web|url={{Allmusic|class=album|id=r1468150|pure_url=yes}} |title=Allmusic > The Fame|date=October 10, 2008|last=Chisling|first=Matthew|publisher=[[AllMusic]]|accessdate=January 1, 2012}}</ref> Genevieve Koski of ''[[The A.V. Club]]'' called "[[LoveGame]]" and this song "propulsive club anthems" that "chug along on wave after wave of synths and programmed drums, resulting in a dizzying sonic trip that approximates the high point of a chemically enhanced night of club-hopping."<ref name="avclub">{{cite web|url=http://www.avclub.com/review/lady-gaga-emthe-fameem-6744|title= Lady Gaga: The Fame Review|last=Koski|first=Genevieve|date=February 23, 2009|work= [[The A.V. Club]]|accessdate=August 10, 2009}}</ref> Sal Cinquemani of [[Slant Magazine]] said that Gaga "successfully [tosses] out dirties" on the song.<ref name="slant">{{cite web|url=http://www.slantmagazine.com/music/music_review.asp?ID=1558 |title=Lady GaGa: The Fame |last=Cinquemani|first=Sal|date=November 29, 2008|work=[[Slant Magazine]]|accessdate=August 10, 2009}}</ref>

The song debuted on the [[UK Singles Chart]] at number 89 due to strong digital downloads, on the issue dated February 21, 2009. On its second week, it rose to number 83 which is where it peaked, and the next week it dropped to number 87, which was its final appearance on the chart.<ref name="uk"/> On the ''Billboard'' [[Dance/Electronic Digital Songs]] chart of April 3, 2010, the song reached a peak of number 28.<ref name="danceelec">{{cite web|url=http://www.billboard.com/biz/charts/2010-04-03/dance-electronic-digital-songs|title=Dance/Electronic Digital Songs: April 3, 2010|work=Billboard|subscription=yes|accessdate=January 1, 2010}}</ref> According to [[Nielsen Soundscan]], the song has sold 275,000 digital downloads in the United States.<ref>{{cite news|url=http://new.music.yahoo.com/blogs/chart_watch/65419/week-ending-sept-5-2010-rihanna-leads-the-pack/ |title=Week Ending Sept. 5, 2010: Rihanna Leads The Pack |last=Grein |first=Paul |date=September 8, 2010 |accessdate=September 9, 2010 |publisher=[[Yahoo!]] |archiveurl=https://web.archive.org/web/20111013093214/http://new.music.yahoo.com/blogs/chart_watch/65419/week-ending-sept-5-2010-rihanna-leads-the-pack/ |archivedate=October 13, 2011 |deadurl=yes |df=mdy }}</ref>

==Music video==
A music video was filmed for the song and directed by [[Melina Matsoukas]]. There are two versions of the video&nbsp;— one intercut with clips from the [[American Broadcasting Company|ABC]] series ''[[Dirty Sexy Money]]'' and created to promote the show, and another which is the official video.<ref>{{cite web|url=https://www.youtube.com/watch?v=7Nr33m1zXVE |title=Music video of Beautiful, Dirty, Rich|publisher=YouTube|accessdate=June 17, 2009}}</ref> The video is set in a mansion, and starts with Gaga making her way down a hallway with several people who are either walking behind her, holding an umbrella over her, dancing in front of her, or tossing dollar bills around her. Gaga and company are then featured in many repeatedly-changing shots at various locations in the mansion. She is seen lying seductively on a table covered with money, crawling along a black grand piano and striking the keys with her leg several times, dancing around and later making out with a statue, and dancing in an elevator solo. There are also several close-up shots of her [[burning money]] and later stuffing it into her mouth. Over the course of the video, she undergoes numerous costume changes. Singer-songwriter [[Space Cowboy (musician)|Space Cowboy]] is also shown, most notably in scenes where he climbs a wall.

==Live performances==
[[File:Monster Ball BDR.jpg|right|thumb|Gaga performing the song during [[The Monster Ball Tour]]|upright]]

Gaga has performed "Beautiful, Dirty, Rich" live at the [[AOL Sessions]], where she also performed "[[Just Dance (song)|Just Dance]]", an acoustic version of "[[Poker Face (Lady Gaga song)|Poker Face]]", "[[Paparazzi (Lady Gaga song)|Paparazzi]]", and "[[LoveGame]]". She also performed it at the MTV UK Live Sessions, also singing "Just Dance", "LoveGame", and "Poker Face". She most notably performed the song on her first headlining [[The Fame Ball Tour]]. On the tour, it was preceded by the song "Paparazzi", which ended with clicking cameras and a brief medley of "Starstruck" and "LoveGame". The performance of "Beautiful, Dirty, Rich" followed, with Gaga frolicking with her dancers and wearing an "austere" blonde bob wig<ref name="popwatch">{{cite news|url=http://popwatch.ew.com/2009/03/14/lady-gaga-live/|title=Lady GaGa live in L.A.: EW photo blog!|last=Pastorek|first=Whitney|date=March 14, 2009|work=[[Entertainment Weekly]]|accessdate=April 15, 2009}}</ref> and a "sculptural bustier with futuristic triangular silver panels, and exaggerated peplum", which was one of her six costumes over the course of the show.<ref name="dailymail">{{cite news|url=http://www.dailymail.co.uk/tvshowbiz/article-1176858/Madonna-Cyndi-Lauper-style-notes-Lady-Gaga-parades-latest-leotards-concert.html|last=Abraham|first=Tamara|title=Madonna and Cyndi Lauper take style notes as Lady Gaga parades latest leotards in concert|work=[[Daily Mail]]|date=May 4, 2009|accessdate=August 13, 2009}}</ref> The song was the end of the first of four parts of the show, and was followed by a video clip, "The Brain", which featured Gaga as her alter ego, Candy Warhol, brushing her hair.<ref name="chicago">{{cite news|url=http://articles.chicagotribune.com/2009-03-26/entertainment/0903250453_1_lady-gaga-new-yorker-scepter|title=Lady Gaga delights|last=Downing|first=Andy|date=March 26, 2009|work=[[Chicago Tribune]]|accessdate=April 15, 2009}}</ref>

The performance of the song, along with the rest of the first quarter of the show, was praised by Jim Harrington of ''[[San Jose Mercury News]]'' for giving the show a strong beginning, and Gaga's vocal performance was also complimented.<ref name="merconcert">{{cite news|url=http://www.mercurynews.com/ci_11924085?source=rss_viewed|title=Lady Gaga delivers crazy dance-pop show|last=Harrington|first=Jim|date=March 16, 2009|work=[[San Jose Mercury News]]|accessdate=April 30, 2009}}</ref> Mikael Wood of ''[[Rolling Stone]]'' was rather critical of the performance; Wood claimed that it grew tiresome quickly but praised Gaga for her energy.<ref name="rollingstone">{{cite news|url=http://www.rollingstone.com/music/news/lady-gagas-fame-attracts-kanye-west-perez-hilton-to-l-a-show-20090316|title=Lady Gaga's "Fame" Attracts Kanye West, Perez Hilton to L.A. Show|last=Wood|first=Mikael|date=March 16, 2009|work=[[Rolling Stone]]|accessdate=August 13, 2009}}</ref> Whitney Pastorek of ''[[Entertainment Weekly]]'' praised Gaga's "strong" singing voice, adding, "The girl can, and does, sing."<ref name="popwatch"/>

==Credits and personnel==
*[[Lady Gaga]] – [[vocalist|vocals]], [[songwriting]], [[piano]], [[synthesizer]]
*[[Rob Fusari]] – songwriting, [[Record producer|production]]
*Tom Kafafian – [[guitar]]
*Calvin "Sci-Fidelty" Gaines – [[bass guitar]], [[Programming (music)|programming]]
*Dave Murga – [[drums]]
*Robert Orton – [[Audio mixing (recorded music)|audio mixing]]
*Gene Grimaldi – [[audio mastering]] at Oasis Mastering, [[Burbank, California]]
*Recorded at 150 Studios, [[Parsippany-Troy Hills, New Jersey]]

Credits adapted from ''The Fame'' album liner notes.<ref name="credits">{{cite AV media notes |title= The Fame|titlelink= The Fame |others=[[Lady Gaga]] |year=2008 |first=Stefani |last=Germanotta |type=CD liner notes |publisher=[[Interscope Records]]}}</ref>

==Charts==
{| class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (2009–10)
! scope="col"| Peak<br>position
|-
{{singlechart|UKsinglesbyname|83|artist=Lady Gaga|rowheader=true|accessdate=March 3, 2016|refname="uk"}}
|-
! scope="row"| US [[Dance/Electronic Digital Songs]] (''[[Billboard charts|Billboard]]'')<ref name="danceelec"/>
| 28
|}

==Certification==
{{Certification Table Top}}
{{certification Table Entry|region=United States|artist=Lady Gaga|title=Beautiful, Dirty, Rich|award=Gold|type=single|certyear=2008|accessdate=March 3, 2016}}
{{certification Table Bottom|nosales=yes|nounspecified=yes}}

==References==
{{reflist|30em}}

==External links==
* {{MetroLyrics song|lady-gaga|beautiful-dirty-rich}}<!-- Licensed lyrics provider -->

{{Lady Gaga songs}}
{{good article}}

[[Category:2008 songs]]
[[Category:Lady Gaga songs]]
[[Category:Music videos directed by Melina Matsoukas]]
[[Category:Songs written by Lady Gaga]]
[[Category:Songs written by Rob Fusari]]