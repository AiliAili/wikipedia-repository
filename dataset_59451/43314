<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Nighthawk<!--please avoid stating manufacturer in this field; it's stated two lines below -->
  |image = Nieuport Nighthawk.jpg
  |caption = Nieuport Nighthawk
}}{{Infobox Aircraft Type
  |type = [[Fighter plane|Fighter]]
  |manufacturer = [[Nieuport & General Aircraft]]
  |designer = <!--only appropriate for individuals, not for project leaders or a company or corporation-->
  |first flight = 1919<!--if it hasn't happened, leave it out!-->
  |introduced = 1923<!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = 1938 [[Hellenic Air Force]]<!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Air Force]]<!--please list only one, and don't use those tiny flags as they limit horizontal space-->
  |more users = [[Greece]]<!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = [[Nieuport Nightjar]]<br />[[Gloster Sparrowhawk]]<!-- variants OF the topic type -->
}}
|}

The '''Nieuport Nighthawk''' was a [[United Kingdom|British]] fighter aircraft developed by the [[Nieuport & General Aircraft]] company for the [[Royal Air Force]] towards the end of the [[World War I|First World War]]. Although ordered into production before the aircraft first flew, it did not enter large scale service with the RAF owing to unreliable engines. Re-engined aircraft did see service in Greece, serving from 1923 to 1938.

==Design and development==
The Nieuport & General Aircraft Co. Ltd. was formed on 16 November 1916 to produce [[France|French]] [[Nieuport]] aircraft under licence.<ref name= "Bruce p. 248">Bruce August 1963, p. 248.</ref> During 1917, hiring [[Henry Folland]] as chief designer, the company started to design its own aircraft, with the first type, the [[Nieuport B.N.1]] fighter (the designation signifying British Nieuport) flying early in 1918.<ref name= "Bruce p. 248"/>

To produce a fighter to replace the [[Sopwith Snipe]] in service with the RAF, the [[Air Ministry]] produced RAF Specification Type 1 for a single-seat fighter to be powered by the [[ABC Dragonfly]] engine. This was a [[radial engine]] under development which was meant to deliver 340&nbsp;hp (254&nbsp;kW) while weighing only 600&nbsp;lb (272&nbsp;kg), and on the basis of the promised performance, was ordered into production in large numbers.<ref name="brucesnipept2p292">Bruce 1974, p. 292.</ref> The design was also projected as a shipboard fighter, although this was considered a secondary role.<ref name= "Bruce p. 248"/>

To meet this requirement, Folland designed the Nighthawk, a wooden two-bay [[biplane]].<ref name="mason fighter p149">Mason 1992, p. 149.</ref> An initial order for 150 Nighthawks was placed in August 1918, well before prototypes or flight-ready engines were available, with the first prototype, serial number ''F-2909'' flying in April or May 1919.<ref name="mason fighter p150">Mason 1992, p. 150.</ref> By this time, it was clear that the Dragonfly had serious problems, being prone to extreme overheating (which was so severe as to char propeller hubs), high fuel consumption and severe vibration (inadvertently being designed to run at its [[Resonance|resonance frequency]]).<ref name="brucesnipept2p292"/> When the engine could be persuaded to work, the Nighthawk showed excellent performance, but in September 1919, it was finally recognised that the Dragonfly was unsalvagable and the engine programme was cancelled, although by this time 1,147 engines had been delivered.<ref name= "Bruce p. 249">Bruce August 1963, p. 249.</ref>

Seventy Nighthawks were completed by Nieuport and the [[Gloster Aircraft Company|Gloucestershire Aircraft Company]], with a further 54 airframes without engines being completed.<ref name="mason fighter p150"/> Small numbers of Dragonfly-powered Nighthawks were delivered to the [[Royal Aircraft Establishment|Royal Aeronautical Establishment]] (R.A.E.) at [[Farnborough Airfield|Farnborough]], but in that form did not enter operational service.<ref name="mason fighter p150"/>

==Operational history==
Nieuport built a sport aircraft, the '''L.C.1 (Land Commercial) Nighthawk''' with the first civil registered aircraft, K-151 appearing on 21 June 1919 at the first postwar Aerial Derby at [[Hendon]]. An additional Nighthawk prototype (H8553) was fitted with a [[hydrovane]] and was tested in a shipboard configuration at the [[Isle of Grain]] in 1920.<ref>Bruce August 1963, pp. 249–250.</ref> In a vain attempt to work out the problems with the Dragonfly engine, four Nighthawks were also retained by the R.A.E. with experiments carried out in 1920–21. The K-151 was further converted to a two-seater with a new cockpit fitted forward of the pilot's position and was sent to India and Malaya in 1920 for a series of sales-promotion flights. After completing the first "newspaper" flight from Bombay to Poona in February 1920, delivering newspapers, the sales demonstrator was sold to India in September 1920.<ref name= "Bruce p. 251">Bruce August 1963, p. 251.</ref>

A new civil Nighthawk, registered G-EAJY, again modified to a two-seater, had its wingspan reduced by two ft and was flown at the 1920 [[Aerial Derby]] where it placed fourth at an average speed of {{convert|132.67|mph|kph|abbr=on}}. After appearing in the 1921 event, the aircraft was privately sold. A much-modified Nighthawk appeared in 1920, designated the '''Goshawk''' with the aircraft incorporating a more streamlined fuselage, rounded tips on the upper wings and a tightly cowled engine installation. In testing, the Goshawk reached {{convert|166.5|mph|kph|abbr=on}}, a British record at the time. On 12 July 1921 the Goshawk was destroyed when [[Harry Hawker]] fatally crashed while practising for the 1921 Aerial Derby.

===Gloster variants===
Nieuport & General closed down in August 1920, and the rights to the Nighthawk were purchased by the [[Gloster Aircraft Company]], who also hired Folland as chief designer.<ref name="complete fighters p443">Green and Swanborough 1994, p. 443.</ref> Gloster proceeded to produce a number of derivatives of the Nighthawk, using stocks of Nighthawk components acquired by the company from the cancelled production run, calling them the '''Gloster Mars'''.<ref name="mason fighterp152">Mason 1992, p. 152.</ref>

The first of these derivatives was the '''Mars I''' (or '''Bamel''') racing aircraft. Powered by a 450&nbsp;hp (336&nbsp;kW) [[Napier Lion]] II engine, this used a Nighthawk undercarriage, rear fuselage and tail with new, single-bay wings,<ref name="James p69">James 1971, p. 69.</ref> first flying on 20 June 1921.<ref name="jackson civil v2p311">Jackson 1973, p. 311.</ref> It was modified progressively to reduce drag and increase speed, with the wing area at one stage being reduced from the original 205&nbsp;ft² (19.0m²) to 165&nbsp;ft² (15.3 m²), in this form setting a British speed record of 196.4&nbsp;mph (313.3&nbsp;km/h).<ref name="James p70">James 1971, p. 70.</ref><ref>[http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200838.html "Mars I's wonderful performance at Martlesham: 212 m.p.h.!"] ''Flight'', 22 December 1921.</ref>  In 1922, the aircraft made an attempt on the [[Flight airspeed record|world air speed record]]. Although the recorded speed of 212.15&nbsp;mph (342&nbsp;km/h) was faster than the existing record, it did not exceed it by the required margin, so the record was not recognised.<ref name="James p71">James 1971, p. 71.</ref><ref>[http://mysite.wanadoo-members.co.uk/brockworthpc/page6.html "A Brief History of Gloster Aircraft Company."] ''Brockworth Parish Council''. Retrieved 20 February 2008.</ref> In 1923, this aircraft was modified with new wings and a more powerful Lion engine as the '''Gloster I'''.<ref name="jackson civil v2p312">Jackson 1973, p. 312.</ref> The Gloster I was sold to the RAF in December 1923, being fitted with floats and used as a Trainer for the [[High Speed Flight RAF]], being scrapped in 1927.<ref name="James p72-3">James 1971, pp. 72–73.</ref>
[[File:GMars.jpg|thumb|right|The Gloster Mars I]]
The Mars I, after conversion to the Gloster I, was fitted with floats and used as a training [[seaplane]] for the British 1925 and 1927 [[Schneider Trophy]] teams, remaining in use until 1930.<ref name="jackson civil v2p312"/>

The '''[[Gloster Sparrowhawk]]''' (or Mars II, III and IV) was a naval fighter for [[Japan]], powered by the [[Bentley BR2]] rotary engine. The Japanese Sparrowhawks were flown from the Yokosuka Naval Base as well as from platforms built on gun turrets of warships.<ref name= "Bruce p. 299"/>  The [[Nieuport Nightjar|Nightjar]] (known as the Mars X) was a similar carrier fighter for the RAF.

A further factory conversion of a Mars III (civil registered as G-EAYN) led to the [[Gloster Grouse]] I, a [[sesquiplane]], powered by a 185&nbsp;hp Siddeley Lynx, that became the prototype for a small production run of Grouse II for Sweden. The Grouse series was the progenitor of the later [[Gloster Grebe]].<ref name= "Bruce p. 299">Bruce September 1963, p. 299.</ref>

The '''Gloster Nighthawk''', or Mars VI, replaced the Dragonfly with either an [[Armstrong-Siddeley Jaguar]] or a [[Bristol Jupiter]] radial. In 1922, the RAF acquired 29 aircraft converted from Nieuport Nighthawks, powered by both Jaguar and Jupiter engines, while [[Greece]] purchased 25 Jaguar powered fighters.<ref name="mason fighterp152"/>

Three of the RAF's Gloster Nighthawks were sent to [[Iraq]] in 1923 for more extensive evaluation, being tested by [[No. 1 Squadron RAF|No 1]], [[No. 8 Squadron RAF|8]] and [[No. 55 Squadron RAF|55]] Squadrons.<ref name="mason fighter p153">Mason 1992, p. 153.</ref> The 25 Greek aircraft were delivered in 1923, remaining in service until 1938.<ref name="mason fighter p153"/>

The  final Nighthawk variant was the Mars X or '''Nightjar''' naval fighter powered by a 230&nbsp;hp [[Bentley]] B.R. 2. All of the series of 22 Nightjars were converted from available stocks of original Nighthawks.<ref name= "Bruce p. 300">Bruce September 1963, p. 300.</ref> Nightjars were used operationally during the Chanak Crisis in 1922 and were operated from the [[HMS Argus (I49)|H.M.S. ''Argus'']] from 1922–1924.<ref name= "Bruce pp. 300–301">Bruce September 1963, pp. 300-301.</ref>

==Variants==
;Nieuport Nighthawk
:Original production version. Powered by 320 [[ABC Dragonfly]] engine.
; Nieuport L.C.1. 
:Civil version, appearing in both single- and two-seater configurations.
; Nieuport Goshawk. 
: Civil version, one completed as an air racer.
;Gloster Bamel (Mars 1)
:Racing derivative of Nighthawk. Powered by 450 hp [[Napier Lion]] engine. One built
;Gloster 1
:Rebuild of Mars 1 with more powerful engine and smaller wing.
;Gloster Sparrowhawk (Mars II, III and IV)
:{{main|Gloster Sparrowhawk}}
:Naval fighter for Japan. 
; Gloster Grouse (I and II)
: Nighthawk conversion to a sesquiplane, equipped with 185 hp [[Siddeley Lynx]]
;Gloster Nighthawk (Mars VI)
:Rebuild of Nighthawk with [[Armstrong Siddeley Jaguar II]] or [[Bristol Jupiter III]] engine.
;Nieuport Nightjar (Mars X)
:{{main|Nieuport Nightjar}}
:Naval fighter for RAF, equipped with a 230 hp [[Bentley B.R.2]].

==Operators==
;{{GRE}}
*[[Hellenic Air Force]]
;{{JPN}}
*[[Imperial Japanese Navy Air Service]]
;{{SWE}}
*[[Swedish Air Force]]
;{{UK}}
*[[Royal Air Force]]
**[[No. 1 Squadron RAF]]
**[[No. 8 Squadron RAF]]
**[[No. 203 Squadron RAF]]
**[[High Speed Flight RAF]]
*[[Royal Navy]]
**[[801 Naval Air Squadron|No. 401 Flight]]

==Specifications (Nighthawk [Dragonfly Engine]) ==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
|ref=''The British Fighter since 1912'' <ref name="mason fighter p150"/>

<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=one
|capacity=
|length main= 18 ft 6 in
|length alt= 5.64 m
|span main= 28 ft 0 in
|span alt= 8.54 m
|height main= 9 ft 6 in
|height alt= 2.90 m
|area main= 276 ft²
|area alt= 25.6 m²
|airfoil=
|empty weight main= 1,500 lb
|empty weight alt= 682 kg
|loaded weight main= 2,218 lb
|loaded weight alt= 1,008 kg
|useful load main=  
|useful load alt=  
|max takeoff weight main= 2,529 lb 
|max takeoff weight alt=  1,147 kg
|more general=
|engine (prop)=[[ABC Dragonfly]] I
|type of prop=9-cylinder [[radial engine]]
|number of props=1
|power main= 320 hp
|power alt= 239 kW
|power original=
   
|max speed main= 131 kn
|max speed alt= 151 mph, 243 km/h
|max speed more= at sea level
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 310 mi 
|range alt= 499 km
|ceiling main= 24,500 ft
|ceiling alt= 7,470 m
|climb rate main=  
|climb rate alt=  
|loading main= 
|loading alt= 
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 
|power/mass alt= 
|more performance=
*'''Endurance:''' 3 hr
*'''Climb to 10,000 ft (3,050 m):''' 7 min 10 sec
|guns=2 × fixed forward-firing [[.303 British|.303 in]] [[Vickers machine gun]]s.
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=*[[Gloster Nightjar|Nieuport Nightjar]]<br />
*[[Gloster Sparrowhawk]]
*[[Gloster II]]<!-- related developments -->
|similar aircraft=*[[Sopwith Dragon]]<br />
*[[BAT Basilisk]]
*[[Sopwith Snark]]
*[[Sopwith Snapper]]
*[[Armstrong Whitworth Siskin]]
*[[Armstrong Whitworth Ara]]<!-- similar or comparable aircraft -->
|lists=
* [[List of aircraft of the RAF]]
<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==
;Notes
{{Reflist|2}}
;Bibliography
{{refbegin}}
* Bruce, J.M. "Nieuport Nighthawk. (Part 1)." ''Air Pictorial'', Volume 25, Number 8, August 1963.
* Bruce, J.M. "Nieuport Nighthawk. (Part 2)." ''Air Pictorial'', Volume 25, Number 9, September 1963.
* Bruce, J.M. "Sopwith Snipe... the RAF's First Fighter. (Part 2)." ''[[Air International|Air Enthusiast International]]'', Volume 6, Number 6, June 1974. Bromley, UK: Fine Scroll.
* Green, William and Gordon Swanborough. ''The Complete Book of Fighters''. New York: Smithmark, 1994. ISBN 0-8317-3939-8. 
* Jackson, A.J. ''British Civil Aircraft since 1919. Volume 2.'' London: Putnam, Second edition, 1972. ISBN 0-370-10010-7.
* James, Derek N. ''Gloster Aircraft since 1917''. London: Putnam, First edition, 1971. ISBN 0-370-00084-6.
* Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* Mason, T. "The Nighthawk Family." ''Air Pictorial'', Volume 30, No. 10, October 1968.
{{refend}}

==External links==
{{commons category|Nieuport Nighthawk}}
* [http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%201522.html The Nieuport "Nighthawk". ''[[Flight (magazine)|Flight]]'', Volume XI, Issue 48, No. 570, 27 November 1919, pp.&nbsp;1524–1532;  contemporary technical description of the original Dragonfly-engined Nighthawk with photographs and drawings.]

{{Nieuport & General aircraft}}
{{Gloster aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:British fighter aircraft 1920–1929]]
[[Category:Nieuport & General aircraft|Nighthawk]]