{{Infobox institute
| name              = Baylor University Institute for Oral History
| image             = File:Fifth Street view of Carrol Library, Baylor University, Waco, TX.jpg
| image_upright     = 
| alt               = Carroll Library, Home of the BUIOH
| caption           = Carroll Library, Home of the BUIOH
| latin_name        = 
| motto             = 
| founder           = 
| established       = {{startdate|1970}}
| mission           = "To foster a deepening understanding of the past by collecting, preserving, and sharing the historically significant memories of individuals according to the highest ethical and professional standards, to work with scholars across disciplines to design and execute innovative research projects, to equip community groups in their oral history endeavors, and to mentor students in the interdisciplinary field of oral history."<ref name="auto">[http://www.baylor.edu/oralhistory/index.php?id=23343 Baylor University || Institute for Oral History || Our Mission] {{webarchive |url=https://web.archive.org/web/20130222055312/http://www.baylor.edu/oralhistory/index.php?id=23343 |date=February 22, 2013 }} Retrieved February 22, 2013.</ref>
| focus             = 
| president         = 
| ceo               = 
| chairman          = 
| head_label        = 
| head              = 
| faculty           = 
| adjunct_faculty   = 
| staff             = 
| key_people        = 
| budget            = 
| endowment         = 
| debt              = 
| num_members       = 
| subsidiaries      = 
| owner             = 
| non-profit_slogan = 
| former_name       = 
| location          = 
| city              = 
| state             = 
| province          = 
| country           = 
| coor              = 
| address           = 
| website           = 
| dissolved         = 
| footnotes         = 
}}
The '''Baylor University Institute for Oral History''' (BUIOH), located in [[Waco, Texas]], is a freestanding research department within [[Baylor University]]'s Division of Academic Affairs. The BUIOH creates [[oral history]] memoirs by preserving an audio recording and transcript of interviews with individuals who are eyewitnesses to history; it provides both physical and digital access to these materials for those interested in the stories.<ref>{{cite news|last=Hodges|first=Sam|title=Oral history project records elderly blacks on the horror of lynchings|url=http://www.dallasnews.com/news/local-news/20100918-Oral-history-project-records-elderly-blacks-4051.ece|accessdate=17 March 2013|newspaper=Dallas Morning News|date=18 September 2010}}</ref><ref>{{cite news|last=Wallach|first=Dan|title=Professor works on oral history of strikes|accessdate=17 March 2013|newspaper=Beaumont Enterprise|date=March 2, 2002}}</ref> The BUIOH is a sponsoring member of the [[Oral History Association]] (OHA), hosts the Texas Oral History Association (TOHA), participates in H-Oralhist (the oral history online discussion forum) and is active in the [[International Oral History Association]] (IOHA).

[[File:Fifth Street view of Carrol Library, Baylor University, Waco, TX.jpg|thumbnail|right|Carroll Library, Home of the BUIOH]]

==Mission==
The mission of the BUIOH is: 
<blockquote>"To foster a deepening understanding of the past by collecting, preserving, and sharing the historically significant memories of individuals according to the highest ethical and professional standards, to work with scholars across disciplines to design and execute innovative research projects, to equip community groups in their oral history endeavors, and to mentor students in the interdisciplinary field of oral history."<ref name="auto"/></blockquote>

== History ==
First known as the ''Program for Oral History'', the BUIOH was established in 1970 by Baylor University faculty members who recognized a shift in the historical profession, with increasing emphasis on nontraditional history.<ref name=History1>[http://www.baylor.edu/oralhistory/index.php?id=39330 Baylor University || Institute for Oral History || 1970 - 1982] {{webarchive |url=https://web.archive.org/web/20130212150201/http://www.baylor.edu/oralhistory/index.php?id=39330 |date=February 12, 2013 }} Retrieved February 22, 2013</ref> Under the leadership of Thomas L. Charlton, the ''Program'' followed professional and ethical standards of oral history research established by the OHA. Its fundamental purpose was to collect oral history interviews, transcribe and edit them, and create finding aids for their use. The ''Program'' established a close working relationship with [[The Texas Collection]] archives division at Baylor University, which made the interviews available to researchers in accordance with legal agreements governing the use of each interview. In 1982, with the approval of President [[Herbert H. Reynolds]], the ''Program for Oral History'' became the Baylor University Institute for Oral History and subsequently was authorized to broaden its research and professional scope.<ref name=History2>[http://www.baylor.edu/oralhistory/index.php?id=39340 Baylor University || Institute for Oral History || 1982 - 1993] {{webarchive |url=https://web.archive.org/web/20130212150208/http://www.baylor.edu/oralhistory/index.php?id=39340 |date=February 12, 2013 }} Retrieved February 22, 2013.</ref>

In 1992 Dr. Charlton became Baylor's Assistant Vice President for Academic Affairs and by summer 1993, his new administrative responsibilities increased to full-time. When Dr. Charlton resigned as director later that year, the opening coincided with the availability of Rebecca Sharpless, the Institute's former Assistant Director, and she then became the Institute's second director.<ref name=History3>[http://www.baylor.edu/oralhistory/index.php?id=39341 Baylor University || Institute for Oral History || 1993 - 2007] {{webarchive |url=https://web.archive.org/web/20130212150221/http://www.baylor.edu/oralhistory/index.php?id=39341 |date=February 12, 2013 }} Retrieved February 22, 2013.</ref>  The Institute later that year moved from the basement of Tidwell Bible Building to the top floor of Carroll Library, overlooking the Burleson Quadrangle in the heart of the Baylor campus.

During the 1990s, the Institute developed a web site which included information about the Institute, ''Workshop on the Web,'' online oral history tutorial workshop, and a searchable database of abstracts of its oral history collection.<ref name=History3/> The Institute also assisted Baylor Libraries with the process of adding the oral history collection to the BearCat online catalog.

From 1995 through May 1999, the Institute served as headquarters for the Oral History Association, with Rebecca Sharpless serving as Executive Secretary. In the 1990s the Institute hosted three major organizations: the Oral History Association, Texas Oral History Association, and American Studies Association of Texas.<ref name=History3/> In 2003, Rebecca Sharpless began a three-year commitment to OHA culminating in her becoming the association's president in 2005-2006.<ref name=OHA2>[http://www.oralhistory.org/about/past-presidents/ Past Presidents | Oral History Association] Retrieved February 22, 2013</ref>

At the end of July 2006, Rebecca Sharpless left Baylor to pursue a position at [[Texas Christian University]]. While the university conducted a national search for an Institute director, Lois E. Myers served as the Interim Director.<ref name=History3/> In August 2007, Baylor University announced Dr. Stephen Sloan as assistant professor in the Department of History and the new director of the Institute for Oral History.<ref name=History4>[http://www.baylor.edu/oralhistory/index.php?id=55213 Baylor University || Institute for Oral History || 2007 - 2010] {{webarchive |url=https://web.archive.org/web/20130212150231/http://www.baylor.edu/oralhistory/index.php?id=55213 |date=February 12, 2013 }} Retrieved February 22, 2013.</ref>  Dr. Sloan then initiated efforts to guide the Institute fully into the digital age, toward the goal of providing online access to sound files and transcripts gathered since 1970.

Under the direction of Senior Editor Elinor Mazé, the Institute was able to include its collection finding aid within the Baylor University Library Digital Collections.  Over the summer of 2008, Institute staff assessed the condition of its entire collection of analog sound recordings to prepare for ultimate digitization of open-reel and cassette tapes.   During the 2008-2009 academic year, 350 memoir volumes comprising 50,500 typescript pages were digitized through the support of Baylor's Ray I. Riley Digitization Center.<ref name=History4/>  By February 2009 users of the BUIOH collection were able to access transcripts via electronic document delivery. This marked the shift of the Institute to a full-service reference and research center, adding end-user assistance to its collecting, processing, teaching, studying, and publishing activities.

== TOHA ==

In 1982, Dr. Charlton became the founding president of the Texas Oral History Association (TOHA), choosing as its headquarters Baylor University, where it remains today. Rebecca Sharpless served as TOHA's first secretary-treasurer (1982–1987), and Lois Myers has held this position since.<ref name=History2/> Since 1984, TOHA has sponsored program sessions in conjunction with the annual meetings of the [[Texas State Historical Association]] (TSHA). The meetings are held during the first week of March, commemorating Texas Independence Day. During the TOHA session, two or three oral history researchers present scholarly papers on topics related to the history and culture of Texas. TOHA also now holds an annual conference every April which showcases oral history research on Texas topics.<ref name=TOHA1>[http://www.baylor.edu/toha/index.php?id=29336 Baylor University || Texas Oral History Association || Meetings and Conferences] Retrieved February 22, 2013.</ref> In addition, there is also an annual publication, ''Sound Historian'', first published in 1993, that highlights oral history research in multidisciplinary topics. Articles appearing in the journal are chosen for their quality by an editorial board of experienced Texas oral historians.<ref name=TOHA2>[http://www.baylor.edu/toha/index.php?id=29344 Baylor University || Texas Oral History Association || Sound Historian] Retrieved February 22, 2013.</ref>

TOHA bestows four separate awards to deserving oral history practitioners/organizations:<ref name=TOHA3>[http://www.baylor.edu/toha/index.php?id=29342 Baylor University || Texas Oral History Association || Awards] Retrieved February 22, 2013.</ref>

* The Thomas L. Charlton Lifetime Achievement Award
* The Mary Faye Barnes Award for Excellence for Community History Projects
* The W. Stewart Caffey Award for Excellence for Precollegiate Teaching
* The Kenneth E. Hendrickson Jr. Best Article Award

TOHA is an affiliate of the OHA.<ref name=OHA1>[http://www.oralhistory.org/about/regional-organizations/ Regional and International Organizations | Oral History Association] Retrieved February 22, 2013</ref>

== Collection scope ==

=== Project families ===

Over the years BUIOH has amassed a series of major project headings under which most new work is organized. These project families are:

* Arts & Culture
* Baylor University
* Bob Bullock
* Education
* Family Life & Community History
* Historic Preservation
* Religion & Culture
* Special
* Texas Baptist
* Texas Judicial
* Waco & McLennan County

=== Current projects ===

Among many others BUIOH is actively working on two major grant-funded projects:

''For the Greater Good: Philanthropy in Waco'' - Made possible through the generosity of the Cooper Foundation, this project focuses on interviews with philanthropists past and present in Waco's history to better understand the spirit of giving. A web portal will be published in Summer 2013 providing biographies, sound bytes and the details of the charitable impact of project participants.

''Texas Liberators of WWII Concentration Camps'' - Made possible through a grant from the Texas Holocaust and Genocide Commission, this project seeks to tell the stories of Texas World War II veterans who personally participated in the liberation of Nazi concentration camps. Twenty video interviews are planned, with resulting materials disseminated to the Commission for use in online presentations beginning in the Spring of 2013.

''Waco History'' - Through joint collaboration with Baylor's "The Texas Collection," the Institute was able to create a resource for users to explore the legacy of Waco, Texas. Inspired by other projects using the software Curatescape, creators formed a mobile app and website that allows users to observe locations, people, and events within Waco through an interactive map. The project was launched in the spring of 2015. <ref>{{Cite news|url=http://www.baylor.edu/oralhistory/news.php?action=story&story=152673|title=BUIOH Announces Official Launch of “Waco History” Website and Mobile App|date=2015-03-02|newspaper=Institute for Oral History {{!}} Baylor University|access-date=2017-02-13|language=en-US}}</ref>

== Education ==

In 1971, Dr. Charlton received a grant from the [[National Endowment for the Humanities]] to develop undergraduate and graduate curriculum offerings in oral history.<ref name=History1/> The graduate seminar in oral history was first offered in the early 1970s and continues to through the present. Students are allowed to create a topic of investigation and conduct primary interviews while receiving instruction on the history, theory, proper procedure and technology of the profession.

Throughout the decades the directors and staff of the BUIOH have participated in workshops throughout Texas and beyond. Ranging from academic symposia to community Q&A sessions, these efforts help to educate individuals on the best practices in the profession as well as inform others of the work of the BUIOH. Beginning in the mid-1990s the Institute began compiling the materials from these efforts and published ''Workshop on the Web'', an extension of the BUIOH website aimed at providing easy access to tutorials on a variety of oral history related topics.

In July 2009, BUIOH conducted its first online workshop, ''Getting Started with Oral History''. Fifteen oral history newcomers hailing from Connecticut, Pennsylvania, Georgia, Florida, Arizona, and Texas took part in the two-session interactive workshop.<ref name=History4/> Participants are able to listen to the BUIOH staff explain key areas of oral history while viewing multimedia presentations on the topics. Practice project assignments are ascribed and Q&A sessions held after each session. BUIOH has since offered its workshop online twice a year and has serviced a total of 117 participants to date from five countries.

== Fellowships and grants ==

=== Active grants ===

BUIOH offers three active fellowships/grants to help enable local oral history projects:<ref name=Grants>[http://www.baylor.edu/oralhistory/index.php?id=23534 Baylor University || Institute for Oral History || Research Grants] {{webarchive |url=https://web.archive.org/web/20130222055351/http://www.baylor.edu/oralhistory/index.php?id=23534 |date=February 22, 2013 }} Retrieved February 22, 2013</ref>

* '''Faculty Research Fellowship''' - Baylor University faculty scholars interested in creating oral history memoirs in their area of interest may apply - provides a stipend as well as training, equipment, processing, and preservation support for faculty interview projects.
* '''Community Oral History Grant''' - Texas nonprofit organizations may apply - provides financial support plus guidance in developing, conducting, processing, and presenting oral history projects created by the organization.
* '''Charlton Oral History Research Grant''' - Partners the Institute with individual scholars who are conducting interviews of historical significance. The Institute helps to fund the interviews and provides processing, transcribing, and preservation for the project. This grant encourages the scholarship of advanced oral historians whose research could bring new insights to topics that have received little or no oral history application.

=== Past grants ===

In 2000, the Institute offered its first Visiting Research Fellowship which brought scholars to campus for two weeks each year to study materials in Baylor's oral history collection. From 2000 through 2011, the fellowship contributed to the scholarship of eleven outstanding historians. Ph.D. candidates and seasoned scholars alike have benefited from researching the diverse topics in the collection, including interviews on rural life, southern culture and religion, Western swing music, economics and politics, [[Baptists|Baptist]] fundamentalism, the [[civil rights movement]], and public education.

== In the media ==

In 1987, the Institute produced a [[PBS]] radio program entitled "Lincolnville at Moccasin Bend: Black Families on the Texas Frontier," based on interviews conducted in [[Coryell County, Texas]]. In 1989, the Institute invited filmmakers Allen and Cynthia Mondell to participate in Baylor's Distinguished Lecturers Series so that they could discuss their documentary films on the [[JFK assassination]]. In 1990, the Institute sponsored a public lecture on oral history and popular media, presented by documentary filmmaker Allen E. Tullos. With funding from the Cooper Foundation, the Institute presented to the public in 1991 its television production ''Crossroads'', which discussed the interplay between the land and people that formed the history and culture of Waco, Texas. ''Crossroads'' appeared on public television and on the local Waco community cable access station.<ref name=History2/>

In August 2010, BUIOH launched a weekly radio program, ''Living Stories'', based on its oral history collection. Airing on [[KWBU-FM]] 103.3 in Waco four times on Tuesdays, the radio segment topics range from the effects that momentous events such as the [[Great Depression]], [[World War II]], and the [[Korean War]] had on individuals, to memories of school days, holidays, leisure activities, and daily life.<ref name=LS1>[http://www.baylor.edu/livingstories/ Baylor University || Living Stories] Retrieved February 22, 2013</ref> The creator of the series, BUIOH editor Michelle Holland, also built an add-on web presence to the BUIOH site in order to archive the programs and allow users to read about and listen to the complete library of clips.<ref name=LS2>[http://www.baylor.edu/livingstories/index.php?id=75790 Baylor University || Living Stories || • Radio Segments] Retrieved February 22, 2013</ref>

==Publications==

*''Oral History for Texans'' (1981)
<small>An overall treatise on oral history techniques with particular focus on the history of the profession in Texas and resources available in the state. Developed and published in conjunction with the Texas Historical Commission.</small>

*''The Past Meets the Present: Essays on Oral History'' (1987)
<small>Essays by noted oral historians on the craft and art of interviewing in interdisciplinary settings.</small>

*''Memory and History: Essays on Recalling and Interpreting Experience'' (1994)
<small>Essays on the psychology of memory and its impact on oral history.</small>

*''Rock Beneath the Sand: Country Churches in Texas'' (2003)
<small>An exploration of the persistence of the rural church in Central Texas through oral history and photography.</small>

*''Handbook of Oral History'' (2006)

<small>Essays on the theoretical foundations and practical applications of our craft from eighteen prominent oral historians.</small>

*''History of Oral History: Foundations and Methods'' (2007)

<small>Designed for the advanced student and experienced oral historian, these essays present the essentials of oral history, conceptualized with theory, informed by historiography, and stimulated by new field methods.</small>

*''Thinking about Oral History: Theories and Applications'' (2007)
 
<small>Prominent oral historians present theories on memory, communication, narrative, life course, and gender that contribute to the analysis and understanding of oral history and raise issues to consider when preparing to share oral history outcomes through print publications, biography, performance, and audio or film/video documentaries.</small>

==References==
<references/>

==External links==
* [http://www.baylor.edu/oralhistory/ BUIOH Homepage]
* [http://www.baylor.edu/TOHA/ TOHA Homepage]
* [http://www.oralhistory.org OHA Homepage]
* [http://www.cooperfdn.org Cooper Foundation - Waco, TX]
* [http://thgc.texas.gov Texas Holocaust and Genocide Commission]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:Baylor University]]