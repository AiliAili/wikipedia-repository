{{EngvarB|date=October 2013}}
{{Use dmy dates|date=October 2013}}
{{For|the film adaptation of the film|Esther Waters (film)}}
[[File:Esther Waters.jpg|thumb|right|220px|First edition title page.]]
'''''Esther Waters''''' is a novel by [[George Moore (novelist)|George Moore]] first published in 1894.

==Introduction==
Set in England from the early 1870s onward, the novel is about a young, pious woman from a poor [[working class|working-class]] family who, while working as a kitchen maid, is seduced by another employee, becomes pregnant, is deserted by her lover, and against all odds decides to raise her child as a [[single parent|single mother]]. ''Esther Waters'' is one of a group of [[Victorian era|Victorian]] novels that depict the life of a "fallen woman".

Written in a [[Émile Zola|Zola]]-like [[Naturalism (literature)|naturalistic]] style, the novel stands out among Moore's publications as the book whose immediate success, including [[William Ewart Gladstone|Gladstone]]'s approval of the novel in the ''[[Westminster Gazette]]'',<ref>Skilton, David: "Introduction", p. xiv</ref> brought him financial security. Continuously revised by Moore (1899, 1917, 1920, 1931), it is often regarded as his best novel.

''Esther Waters'' is dedicated to [[T. W. Rolleston]].

==Plot summary==
Esther Waters is born to hard-working parents who are [[Plymouth Brethren]] in [[Barnstaple]]. Her father's premature death prompts her mother to move to London and marry again, but Esther's stepfather turns out to be a hard-drinking bully and wife-beater who forces Esther, a natural beauty, to leave school and go out to work instead, thus greatly reducing her chances of ever learning how to read and write, and Esther remains [[literacy|illiterate]] all her life.

Her first job ("situation") outside London is that of a kitchen maid with the Barfields, a [[nouveau riche]] family of [[horse breeding|horse breeders]], [[horse racing|horse racers]] and [[Horseracing in Great Britain#Betting|horse betters]] who live at Woodview near [[Shoreham-by-Sea|Shoreham]]. There she meets William Latch, a [[footman]], and lets herself be seduced by him. Dreaming of a future with Latch, she is dismayed to find that he is having an affair with the Barfields' niece, who is staying at Woodview. After Latch and his lover have eloped together, Esther stays on at Woodview until she cannot hide her pregnancy any longer. Although she has found a kindred soul in Mrs Barfield, who is also a Plymouth Sister and abhors the betting on horses going on all around her, Esther is dismissed ("I couldn't have kept you on, on account of the bad example to the younger servants") and reluctantly goes back to London.

With the little money she has saved, she can stay in a rented room out of her stepfather's sight. Her mother is pregnant with her eighth child and dies giving birth to it at the same time Esther is at [[Queen Charlotte's Hospital]] giving birth to a healthy boy she calls Jackie. Still in confinement, she is visited by her younger sister who asks her for money for her passage to Australia, where her whole family have decided to emigrate. Esther never hears of them again.

Learning that a young mother in her situation can make good money by becoming a [[wet nurse]], Esther leaves her newborn son in the care of a [[baby-farming|baby farmer]] and nurses the weakly child of a wealthy woman ("Rich folk don't suckle their own") who, out of fear of infection, forbids Esther any contact with Jack. When, after two long weeks, she finally sees her son again, realises that he is anything but prospering and even believes that his life might be in danger, she immediately takes him with her, terminates her employment without notice and then sees no other way than to "accept the shelter of the [[workhouse]]" for herself and Jack.

But Esther is lucky, and after only a few months can leave the workhouse again. She chances upon Mrs Lewis, a lonely widow living in [[East Dulwich]] who is both willing and able to raise her boy in her stead, while she herself goes into service again. However, she is not able to really settle down anywhere: either the work is so hard and the hours so long that, fearing for her health, she quits again; or she is dismissed when her employers find out about the existence of her [[illegitimacy|illegitimate son]], concluding that she is a "loose" woman who must not work in a respectable household. Later on, while hiding her son's existence, she is fired when the son of the house, in his youthful fervour, makes passes at her and eventually writes her a [[love letter]] she cannot read.

Another stroke of luck in her otherwise dreary life is her employment as general servant in [[West Kensington, London|West Kensington]] with Miss Rice, a novelist who is very sympathetic to her problems ("Esther could not but perceive the contrast between her own troublous life and the contented privacy of this slender little spinster's"). While working there, she makes the acquaintance of Fred Parsons, a Plymouth Brother and political agitator, who proposes to Esther at about the same time she bumps into William Latch again while on an errand for her mistress. Latch, who has amassed a small fortune betting on horses and as a [[bookmaker]] ("I am worth to-day close on three thousand [[Pound sterling|pounds]]"), is the proprietor of a [[Licensing laws of the United Kingdom|licensed]] [[public house]] in [[Soho]] and has separated from his [[adultery|adulterous]] wife, waiting for his marriage to be divorced. He immediately declares his unceasing love for Esther and urges her to live with him and work behind the bar of his pub. Esther realises that she has arrived at a crossroads and that she must make up her mind between the sheltered, serene and religious life Parsons is offering her—which she is really longing for—and sharing the financially secure but turbulent existence of a successful small-time entrepreneur who, as she soon finds out, operates on both sides of the law. Eventually, for the sake of her son's future, she decides to go to Soho with Latch, and after his divorce has come through the couple get married.

A number of years of relative happiness follow. Jack, now in his teens, can be sent off to school, and Esther even has her own servant. But Latch is a [[gambling|gambler]], and nothing can stop him from risking most of the money he has in the vague hope of gaining even more. Illegal betting is conducted in an upstairs private bar, but more and more also across the counter, until the police clamp down on his activities, his licence is revoked, and he has to pay a heavy [[fine (penalty)|fine]]. This coincides with Latch developing a [[chronic (medicine)|chronic]], sometimes [[Hemoptysis|bloody]], cough, contracting [[pneumonia]], and finally, in his mid-thirties, being diagnosed with [[tuberculosis]] ("consumption"). However, rather than not touching what little money he still has for his wife and son's sake, the dying man puts everything on one horse, loses, and dies a few days later.

With Miss Rice also dead, Esther has no place to turn to and again takes on any menial work she can get hold of. Then she remembers Mrs Barfield, contacts her and, when asked to come to Woodview as her servant, gladly accepts while Jack, now old enough to earn his own living, stays behind in London. When she arrives there, Esther finds the once proud estate in a state of absolute disrepair, with Mrs Barfield the only inhabitant. Mistress and maid develop an increasingly intimate relationship with each other and, for the first time in their lives, can practise their religion unhindered. Looking back on her "life of trouble and strife," Esther, now about 40, says she has been able to fulfil her task—to see her boy "settled in life," and thus does not see any reason whatsoever to want to get married again. In the final scene of the novel, Jack, who has become a soldier, visits the two women at Woodview.

==Stage and film adaptations==
{{Further2|''[[Esther Waters (film)|Esther Waters]]''}}
The reason why Moore chose ''Esther Waters'' rather than one of his lesser known novels (which he might have been able to promote that way) to be adapted for the stage may have been its "Englishness". The subject-matter of ''Esther Waters'' was the most "English" of his novels, and Moore had just returned to England after abandoning his brief interest in the [[Irish_theatre#The_Abbey_and_after|Irish Renaissance theatre movement]]. 1911, then, saw the première, at the [[Apollo Theatre]] in London's [[West End of London|West End]], of ''Esther Waters: a play in five acts'', which Moore had adapted from his own novel. Although it did not receive good reviews, Moore was pleased with the production. In 1913 [[Heinemann (book publisher)|Heinemann]] published the playscript.

There are, however, two more versions of the play. One was the result of an unsuccessful collaboration, in 1922, between Moore and theatre critic [[Barrett H. Clark]]; a third version of the play was written by Clark in the same year, but never performed. The two 1922 versions were first published in 1984.<ref>Davis, W. Eugene (1984) ''The Celebrated Case of Esther Waters: the collaboration of George Moore and Barrett H. Clark on "Esther Waters: a play"''</ref>
''Esther Waters'' was filmed in 1948 by [[Ian Dalrymple]] and [[Peter Proud]] with [[Kathleen Ryan]] (in the title role), [[Dirk Bogarde]] (as William Latch), [[Cyril Cusack]], [[Ivor Barnard]] and [[Fay Compton]]. It was partly filmed at [[Folkington Manor]], East Sussex. Two television dramas ([[miniseries]]) were produced in 1964 and 1977 respectively.<ref>Cf. their [http://www.imdb.com/find?s=all&q=Esther+Waters Internet Movie Database entries].</ref>

==See also==
{{portal|Novels}}
* [[Victorian literature]]
* [[Women in the Victorian era]]
* [[New Woman]]

==Read on==
* [[William Hale White]] (writing as [[Hale White|Mark Rutherford]]): ''[[Clara Hopgood]]'' (controversially educated and independent sisters who shun convention leave their provincial home for London trying to gain social acceptance for Clara's illegitimate child)
* [[Margaret Drabble]]: ''[[The Millstone (novel)|The Millstone]]'' (unmarried young academic becomes pregnant after a [[one-night stand]] in early 1960s London and decides to give birth to her child and raise it herself)
* [[Ödön von Horváth]]: ''[[Geschichten aus dem Wienerwald]]'' (naive young woman must pay bitterly when she breaks off her reluctant engagement with a butcher after falling in love with a fop who, however, has no serious interest in returning her love)
* [[Arthur Schnitzler]]: ''[[Therese (novel)|Therese]]'' (woman toils to provide for her illegitimate son, who in the end turns against her)
* For more works of literature with female protagonists, see the [[Wikipedia:WikiProject Novels/List of literary works with eponymous heroines|List of literary works with eponymous heroines]].

==Footnotes==
{{reflist|colwidth=30em}}

==References==
* Skilton, David (1983) "Introduction", in: George Moore: ''Esther Waters'' (Oxford World Classics) ([[Oxford University Press|OUP]]; vii–xxii
* [[Virginia Woolf|Woolf, Virginia]]: [http://xroads.virginia.edu/~CLASS/workshop97/Gribbin/bornwriter.html "A Born Writer. Review of ''Esther Waters'', by George Moore"], ''[[The Times Literary Supplement]]'' (29 July 1920). Retrieved 17/8/07.
* Youngkin, Molly (2002) [http://www.ohiolink.edu/etd/send-pdf.cgi?osu1037376119 "Men Writing Women: Male Authorship, Narrative Strategies, and Woman's Agency in the Late-Victorian Novel"], PhD dissertation ([[Ohio State University]], 2002). Retrieved 19/8/07.
* Youngkin, Molly (2007) [http://www.ohiostatepress.org/Books/Book%20PDFs/Youngkin%20Feminist.pdf ''Feminist Realism at the Fin de Siècle: the influence of the Late-Victorian woman’s press on the development of the novel''] Columbus OH: [[Ohio State University Press]]. Excerpt retrieved 19/8/07.

==External links==
* {{Gutenberg|no=8157|name=Esther Waters}}
* {{librivox book | title=Esther Waters | author=George MOORE}}

{{italictitle}}

[[Category:1894 novels]]
[[Category:Irish novels]]
[[Category:Novels set in London]]
[[Category:Irish novels adapted into films]]
[[Category:19th-century Irish novels]]