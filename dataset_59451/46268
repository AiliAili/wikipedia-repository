{{Use dmy dates|date=February 2015}}
{{Use British English|date=February 2015}}
{{multiple issues|
{{Orphan|date=August 2014}}
{{Underlinked|date=August 2014}}
}}

The '''Arthur Dunn Cup''' is a football cup competition played between the Old Boys of public schools. It started in 1903 and is named in honour of Arthur Tempest Blakiston Dunn who had proposed such a competition but died very suddenly shortly after. Dunn was a leading amateur player of his day and was in the winning Old Etonians side in the 1882 FA Cup Final. It was Dunn who passed the ball to Anderson who scored the only goal to defeat Blackburn Rovers at the Kennington Oval.<ref name="Arthur Dunn">Daily Telegraph - 31 March 1882</ref>

== Origins of the Cup Competition ==

On 12 October 1901 an article appeared in a newspaper headed "Amateur Clubs and the English Cup". It began "It has been said that amateur football is in the decline, that it is not what it was, and that its ultimate insignificance is, in fact, merely a matter of time".<ref name="Association Football">Fabian, A H; Green, Geoffrey, ''Association Football'', Volume two, p 140-146. The Caxton Publishing Company Ltd., London 1960</ref> This reflected the growing impression that amateur football was doomed and when Arthur Dunn read this, he was so moved that he wrote a brief note to another Old Etonian, Norman Malcolmson, as follows:

{{Quote box
 |quote  = Dear N.,
One might write for ever on this. I think golf is partly responsible, but largely so because one has to play such a lot of hoppers.
There’s nothing for it but an "Old Boy” association, but who has the time to start it?

Yours, ATBD.<ref name="Association Football">Fabian, A H; Green, Geoffrey, ''Association Football'', Volume two, p 140-146. The Caxton Publishing Company Ltd., London 1960</ref>
|Source}}

Arthur did not have time to follow this thought through because he died suddenly on the evening of 19 February 1902.<ref name="Grayson">Grayson, Edward, ''Corinthians and Cricketers and Towards a New Sporting Era''. Yore Publications, Harefield, Middlesex 1955</ref> (Other sources state the date of death as 20 February and this is the date on his memorial at Eton College). However, on 12 March 1902, just three weeks later, an informal meeting of Old Boys interested in amateur football was convened by Norman Malcolmson and held at the Sports Club. At this meeting the following resolutions were passed:

# That, in the opinion of this Meeting, an annual competition, under Association Rules, in the form of a cup contest, confined to the representatives of the chief Public Schools, is desirable;
# That a trophy, to be called "The Arthur Dunn Memorial Cup", or by some such title, be provided for competition, to be held by the winners for one year;
# That the following schools be invited to appoint one representative to act on the General Committee: Bradfield, Brighton, Charterhouse, Eton, Felsted, Forest, Harrow, Lancing, Malvern, Radley, Repton, Rossall, Shrewsbury, Westminster and Winchester, the competition to be for the Old Boys of the above schools<ref name="First Committee Meeting">Minutes of the first meeting of the Arthur Dunn Cup 1902</ref>

Those present also expressed the view that any surplus profits from the competition should be given to charity. This informal meeting was followed, three weeks later, by a formal one, at which the chair was taken by no less a figure that C Wreford-Brown, a pillar of the Corinthians and capped four times for England in full internationals. The motions passed by the informal meeting were unanimously confirmed, and then officers were elected as follows:

President: Lord Kinnaird (Eton); Vice-Presidents: R C Guy (Forest); R C Gosling (Eton); Committee: R T Squire (Westminster), G O Smith (Charterhouse), W J Oakley (Shrewsbury), C Wreford-Brown (Charterhouse), R E Foster (Malvern), W M Cowan (Brighton), J R Mason (Winchester).
Hon Secretary: N Malcolmson.<ref name="Association Football">Fabian, A H; Green, Geoffrey, ''Association Football'', Volume two, p 140-146. The Caxton Publishing Company Ltd., London 1960</ref>

During the next few weeks a special committee drew up the rules of the competition. Meanwhile, the actual trophy was presented by R C Gosling; and thus, within 14 weeks of Dunn’s death, the vision had become a reality. Six months later the Arthur Dunn Challenge Cup competition was well and truly launched, the schools taking part in the first season being:
Bradfield, Brighton, Charterhouse, Eton, Felsted, Forest, Harrow, Lancing, Malvern, Repton, Rossall, Shrewsbury, Westminster and Winchester. Radley College decided not to participate at this stage.<ref name="Arthur Dunn Cup">Roy, David; Bevan, Ian; Hibberd, Stuart; Gilbert, Michael, The Centenary History of the Arthur Dunn Cup. Replay Publishing Limited, Beckenham, Kent 2003</ref>

== The first Winners ==

The first Arthur Dunn Challenge Cup Final was held on 28 March 1903 at [[The Crystal Palace]]. The two teams at that first final were Old Carthusians and Old Salopians. In a keen and close game, the result was a draw (2 – 2) even after 20 minutes extra time and it was decided to replay at Ealing on Wednesday 1 April. Coincidentally this game too ended in a 2 – 2 draw, and so it was then decided that the Cup should be held jointly by these two clubs, six months each.<ref name="Arthur Dunn Cup"/>

== The first 100 Finals ==
The first 25 Finals were dominated by the Old Carthusians who appeared in 11 Finals and won the Cup nine times. The Old Malvernians appeared in nine Finals and won the Cup six times. During these years the Old Carthusians won the Cup every year between 1903 and 1906 and in 1921/2/3. The Old Malvernians also won the Cup three years in succession on 1924/5/6.

In all, 11 different sides reached the Final and the Old Brightonians, who won the Cup in 1913, are the only winners that no longer play in the competition.<ref name="Brightonians">Brighton College Magazine, 1913</ref> In November 1913, Warren Lewis, then a student at Malvern College and the older brother of C. S. Lewis, was in attendance for a portion of the competition.

The Second World War interfered with the second 25 Finals so the 50th Final was not played until 1964. Again, 11 sides reached the Finals and the Old Salopians were the most dominant side reaching 10 Finals and winning seven times, including the 50th Final.<ref name="50th Final">The Field, 16 April 1964</ref> They were followed by the Old Carthusians who reached eight Finals and won on seven occasions.

Fifteen of the 16 Finals played between 1933 and 1955 were won by only three sides - Old Carthusians, Old Salopians and Old Wykehamists. The Wykehamists shared the Cup with the Old Malvernians in 1938 and the Old Salopians shared it with the Old Brentwoods in 1955.<ref name="Brentwoods">The Times - article unpublished due to paper strike 1955</ref>  There were three new winners in this period, the Old Aldenhamians in 1934, the Old Brentwoods and the Old Cholmelians in 1958 and 1959 respectively. 
The third quarter of the 100 Finals were dominated by those arch rivals, the Old Malvernians, who played in 13 Finals and won the Cup on eight occasions, and the Old Reptonians who played in six Finals and won it five times.

Again, 11 sides reached the Finals in this period but there were three new sides who made their mark. Lancing Old Boys played in four Finals and won the Cup in successive years between 1983-5 (the last side to win three years in a row), the Old Chigwellians won in 1980 and the Old Foresters won in 1974. The Old Brentwoods appeared in 11 Finals but only managed to win the Cup on four occasions. The 75th Final in 1989 was between the Old Malvernians and the Old Brentwoods.

In the last 24 years, 14 sides have reached the Final and from a much wider range. Substitutes were first allowed in 1997 when Old Foresters won the Cup.<ref name="Arthur Dunn Cup-DT">Daily Telegraph, 7 April 1997</ref> Yet once again, the Old Carthusians have been the most dominant side, reaching eight Finals, seven of them in the last 13 years when they have won the Cup six times. The Old Salopians have appeared in six Finals winning the Cup on four occasions including the Centenary Final in 2003. This Final was held at Cobham. Unsurprisingly, the finalists were Old Salopians and Old Carthusians who had met in the first Final in 1903. Old Salopians won 2 – 1.<ref name="Arthur Dunn Cup"/>

The Old Etonians at last won the Cup (Arthur Dunn being an Old Etonian) in 2005 and 2010. The Old Harrovians won for the first time on 2007 and the Old Tonbridgians won in 2012 becoming the first of the newer Old Boys teams to win the Cup.

Over this period, the Cup competition has been supported by Arthur Dunn's wife, Helen Dunn, his daughter Mary Shirley and his granddaughter, Jane Sawyer.<ref name="100th Final">Roy, David, Programme for the 100th Final 2014</ref>

== The 100th Final ==

The 100th Final was played on 12 April 2014 at the Imperial College Sports Ground, [[Teddington]]. In a hard-fought Final, Old Carthusians defeated Old Foresters (3 - 0) after extra time.

== References ==

{{reflist}}
[[Category:1903 establishments in England]]
[[Category:Football cup competitions in England]]