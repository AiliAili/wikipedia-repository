{{Refimprove|date=June 2009}}
{{Infobox publisher
| name = Annual Reviews
| image = [[File:Binding Annual Review of Sociology.JPG|250pxpx|The ''Annual Review of Sociology'' series, one of Annual Reviews' 41 series]]
| parent =
| status =
| founded = 1932
| founder =
| successor =
| country = United States
| headquarters = [[Palo Alto, California]]
| distribution =
| keypeople =
| publications = [[Academic journal]]s
| topics = Science and social science
| genre =
| imprints =
| revenue =
| numemployees =
| nasdaq =
| url = {{URL|http://www.annualreviews.org}}
}}

'''Annual Reviews,''' located in [[Palo Alto]] [[California]], Annual Reviews is a [[Nonprofit organization|not for profit]] publisher dedicated to synthesizing and integrating knowledge for the progress of science and the benefit of society. It has a collection of 46 [[Review journal|review series]] in specific disciplines in science and social science.<ref>{{cite web |url=http://www.annualreviews.org/page/about/our-mission-and-our-founder |title=Our Mission & Our Founder |work=Official website |publisher=Annual Reviews |accessdate=}}</ref> Each review series contains 12 to 40 authoritative comprehensive review articles, covering the major journal articles on a specific topic during the preceding few years. The major topics in each subject are covered every few years, and special topics appear as appropriate.
The reviews are widely used in teaching and research, and serve the purposes both of current awareness and introduction to a new subject. Since in [[scientific literature]] it is customary to cite in detail the sources only since the most recent review, these periodicals are among the highest ranking journals in [[Impact Factor]] for their subjects, as shown in the publisher's table.<ref>[http://www.annualreviews.org/catalog/isi-rankings.aspx ISI Rankings - Year reports on Impact Factor]</ref> (This does not imply that they are necessarily the most important journals in the subject; review series always rank highly because of the relatively few articles published each year and the many articles that cite them.) The reviews are written in a compact narrative style, with a minimum of descriptive text for each article covered. Many authors provide lists of summary points and future issues. The length of each review and the number of articles covered vary widely depending on both the topic and the preferences of the author. The articles are written by invitation to the authors, who are accepted authorities on the material covered.

== Availability ==
Each series is available as a bound annual volume, as individual [[electronic article]]s purchased online, or as a subscription to the print, online, or both. They are also available as a database consisting of some or all of the series, with [[site license|site licensing]] available. The back years of the entire collection can be purchased for a one-time [[price]]. Effective January 2008, purchasing a subscription that includes online access entitles you to permanent data rights to that volume regardless of future subscription status. Customers with 2007 online subscriptions will retain access to the volumes that they have previously purchased. As of 2017, Annual Review of Public Health is available without subscription as it is currently now [[Open access journal|open access]].

==History==
The first series to be published was ''Annual Review of Biochemistry'', which began in 1932. Additional series are started every few years, with the topics appropriate to the growth of scientific research. All Annual Review series became available online in 1996. Since 2004 most series have been richly illustrated in color.

== List of titles ==
{{main cat|Annual Reviews academic journals}}
Years in parentheses indicate the first year of publication.
As of April 2013, the publications included the following:

'''A'''
* ''[[Annual Review of Analytical Chemistry]]'' (2008)
* ''Annual Review of Animal Biosciences'' (2013)
* ''[[Annual Review of Anthropology]]'' (1972)
* ''[[Annual Review of Astronomy and Astrophysics]]'' (1963)

'''B'''
* ''[[Annual Review of Biochemistry]]'' (1932)
* ''[[Annual Review of Biomedical Engineering]]'' (1999)
* ''[[Annual Review of Biophysics]]'' (formerly ''Biophysics and Biomolecular Structure'') (1972)

'''C'''
* ''[http://www.annualreviews.org/journal/cancerbio Annual Review of Cancer Biology]'' (2017) 
* ''[[Annual Review of Cell and Developmental Biology]]'' (1985)
* ''[[Annual Review of Chemical and Biomolecular Engineering]]'' (2010)
* ''[[Annual Review of Clinical Psychology]]'' (2005)
* ''Annual Review of Computer Science'' (1986–1990)
* ''[[Annual Review of Condensed Matter Physics]]'' (2010)

'''E'''
* ''[[Annual Review of Earth and Planetary Sciences]]'' (1973)
* ''[[Annual Review of Ecology, Evolution, and Systematics]]'' (formerly ''Ecology and Systematics'') (1970)
* ''[[Annual Review of Economics]]'' (2009)
* ''Annual Review of Entomology'' (1956)
* ''[[Annual Review of Environment and Resources (journal)|Annual Review of Environment and Resources]]'' (formerly ''Energy and the Environment'') (1976)

'''F'''
* ''Annual Review of Financial Economics'' (2009)
* ''[[Annual Review of Fluid Mechanics]]'' (1969)
* ''Annual Review of Food Science and Technology'' (2010)

'''G'''
* ''[[Annual Review of Genetics]]'' (1967)
* ''Annual Review of Genomics and Human Genetics'' (2000)

'''I'''
* ''Annual Review of Immunology'' (1983)

'''L'''
* ''Annual Review of Law and Social Science'' (2005)
* ''Annual Review of Linguistics'' (2015)

'''M'''
* ''[[Annual Review of Marine Science]]'' (2009)
* ''Annual Review of Materials Research'' (1971)
* ''Annual Review of Medicine'' (1950)
* ''Annual Review of Microbiology'' (1947)

'''N'''
* ''Annual Review of Neuroscience'' (1978)
* ''Annual Review of Nuclear and Particle Science'' (1952)
* ''[[Annual Review of Nutrition]]'' (1981)

'''O'''
* ''Annual Review of Organizational Psychology and Organizational Behavior'' (New in 2014)

'''{{vanchor|P}}'''
* ''Annual Review of Pathology: Mechanisms of Disease'' (2006)
* ''Annual Review of Pharmacology and Toxicology'' (1961)
* ''[[Annual Review of Physical Chemistry]]'' (1950)
* ''Annual Review of Physiology'' (1939)
* ''Annual Review of Phytopathology'' (1963)
* ''[[Annual Review of Plant Biology]]'' (formerly ''Plant Physiology and Plant Molecular Biology'') (1950)
* ''[[Annual Review of Political Science]]'' (1998)
* ''Annual Review of Psychology'' (1950)
* ''Annual Review of Public Health'' (1980) As of April 2017, this title is now [[Open access journal|open access]].<ref>{{Cite web|url=https://annualreviewsnews.org/2017/04/06/public-health-oa/|title=The Annual Review of Public Health is now freely available to read, reuse, and share.|last=News|first=A. R.|date=2017-04-06|website=Annual Reviews News|access-date=2017-04-07}}</ref>

'''R'''
* ''[[Annual Review of Resource Economics]]'' (2009)

'''S'''
* ''[[Annual Review of Sociology]]'' (1975)
* ''Annual Review of Statistics'' (New in 2014)

'''V'''
* ''[[Annual Review of Virology]]'' (New in 2014)

== References ==
{{Reflist}}

== External links ==
* [http://www.annualreviews.org/ Annual Reviews home page]

{{DEFAULTSORT:Annual Reviews (Publisher)}}
[[Category:Publishing companies of the United States]]
[[Category:Companies based in Palo Alto, California]]
[[Category:Publishing companies established in 1932]]
[[Category:1932 establishments in California]]
[[Category:Non-profit academic publishers]]