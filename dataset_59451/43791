{|{{Infobox Aircraft Begin
 |name = Tu-14
 |image =Самолёт Ту-14.JPG
 |caption = 
}}{{Infobox Aircraft Type
 |type = [[Torpedo bomber]]
 |manufacturer = [[Tupolev]]
 |designer =
 |first flight = 13 October 1949<ref>Duffy & Kandalov 1996, p.105</ref>
 |introduction =1952 
 |retired = 1959
 |status = 
 |primary user = [[Soviet Naval Aviation]]
 |more users = 
 |produced =
 |number built = About 150
 |unit cost =
 |developed from =
 |variants with their own articles =
}}
|}

The '''Tupolev Tu-14''' ([[NATO reporting name]]: '''Bosun''')<ref>[http://www.designation-systems.net/non-us/soviet.html#_Listings_Bomber Designation-Systems.Net]</ref> ([[USAF]]/[[USDOD|DOD]] reporting name: '''Type 35'''),<ref>[http://www.designation-systems.net/non-us/soviet.html#_DOD_Type Designation-Systems.Net]</ref> was a [[Soviet Union|Soviet]] twin-[[turbojet]] light [[bomber aircraft|bomber]] derived from the Tupolev Tu-73, the failed competitor to the [[Ilyushin Il-28|Ilyushin Il-28 'Beagle']]. It was used as a [[torpedo bomber]] by the mine-torpedo regiments of [[Soviet Naval Aviation]] between 1952–59 and exported to the [[People's Republic of China]].

==Development==
The Tu-14 had its origin in the three-engined Tu-73 design which used a pair of RD-45 turbojets, an unlicensed copy of the [[Rolls-Royce Nene]], under the wings and a single [[Klimov RD-500]], an unlicensed copy of the [[Rolls-Royce Derwent]], in the tail, in an installation much like that of the central engine of a [[Boeing 727]]. The availability of the [[Klimov VK-1]], a more-powerful version of the Nene, allowed the RD-500 to be deleted from the preliminary design, which was given the internal designation of "81". The other major change was the addition of a PSBN navigation [[radar]] which required a fifth crewmember to operate. This was rejected by the [[Soviet Air Forces|VVS]] and [[Tupolev]] reworked the design to eliminate the dorsal and ventral turrets and reduce the crew to only three, the pilot, a bombardier-navigator, and a tail gunner. It retained the two fixed {{convert|23|mm|abbr=on}} [[Nudelman-Rikhter NR-23]] cannon in the fuselage nose, but the design of the fuselage was changed to give the gunner his own separate pressurized compartment and a KDU-81 tail turret armed with another pair of NR-23 guns.<ref>Gordon, pp. 126–27</ref>

Construction of the prototype began in August 1949, using components from the canceled Tu-73S prototypes, and was completed in October. The manufacturer's tests were conducted between 13 October 1949 and 21 January 1950. Its State acceptance trials lasted from 23 January to 27 May 1950 and it was accepted for production, provided that the problems with the KDU-81 turret were resolved and that [[ejection seats]] were provided for the pilot and gunner, a [[Bleed air|hot air]] [[deicing]] was to be fitted and the gun mount in the nose revised. The first five preproduction aircraft did not incorporate these changes as they were built using Tu-73S components, after the factory in [[Irkutsk]] had prematurely begun production of that bomber. One of these was sent to [[Moscow]] where it was evaluated by [[Soviet Naval Aviation]] for use as a torpedo bomber. The sixth aircraft did incorporate all these changes as well as the navigator's ejection seat requested by Naval Aviation, and it was evaluated in May 1951. It was recommended for production as the Tu-14T and entered service in 1952 with Naval Aviation.<ref name=g8/>

About 150 were produced and served with the mine-torpedo regiments of Naval Aviation until 1959. It was given the [[NATO reporting name]] '''Bosun'''. After it was withdrawn from service, several were used for various test programs, including one evaluating ramjet engines.<ref name=g8>Gordon, p. 128</ref> Up to 50 used Tu-14Ts were delivered to the Chinese [[People's Liberation Army Air Force]] although quantities and dates cannot be confirmed.<ref>Gunston, p. 142</ref><ref>{{cite web|url=http://www.globalsecurity.org/military/world/russia/tu-14.htm|title=TU-14 Bosun|accessdate=2009-09-15}}</ref>

The second preproduction Tu-14 was converted into a day or night photographic reconnaissance aircraft with the [[OKB]] designation of "89". The conversion was fairly minor and involved an unpressurized central cabin that housed two automatic pivoting cameras, two fuel tanks and another camera fitted in the [[bomb bay]] and another camera for oblique photography was mounted in the aircraft's tail for the daylight photography role. All cameras and their viewports were electrically heated to prevent misting and icing at altitude. For night photography, the fuel tanks and camera in the bomb bay were removed and a variety of flare bombs were carried to illuminate the targets. In addition, the screen of the PSBN-M navigation radar could be photographed by a special camera and both the pilot and navigator could record their own observations using a voice recorder.<ref name=g9/> However, the VVS had already decided to use the Il-28R reconnaissance version of the standard Il-28 by the time that the "89" first flew on 23 March 1951 and Tupolev decided not to submit it for State acceptance trials.<ref name=g9>Gordon, p. 129</ref>

==Variants==
*'''Tu-14''' – Light bomber version (not accepted for service).
*'''Tu-14R''' – [[Reconnaissance]] version (prototype only). Also known as '''Tu-89'''.
*'''Tu-14T''' – Torpedo bomber version.
*'''Tu-81''' - Initial designation of Tu-14T, powered by two engines.

==Operators==
;{{USSR}}
*[[Soviet Naval Aviation]]
; {{CHN}}
*[[People's Liberation Army Air Force]]

==Specifications (Tu-14)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=jet
|ref=Gunston, ''Tupolev Aircraft since 1922''
|crew=three
|capacity=
|payload main=
|payload alt=
|length main= 21.95 m
|length alt= 72 ft
|span main= 21.69 m
|span alt= 71 ft 2 in
|height main= 5.69 m
|height alt= 18 ft 8 in
|area main= 67.36 m²
|area alt= 725 ft²
|airfoil=
|empty weight main= 14,930 kg
|empty weight alt= 32,914 lb
|loaded weight main= 20,930 kg
|loaded weight alt= 46,046 lb
|useful load main=
|useful load alt=
|max takeoff weight main= 25,350 kg
|max takeoff weight alt= 55,866 lb
|more general=
|engine (jet)=[[Klimov VK-1]]
|type of jet= [[turbojet]]s
|number of jets=2
|thrust main= 26.5 kN
|thrust alt= 5,950 lbf
|thrust original=
|afterburning thrust main=
|afterburning thrust alt=
|max speed main= 848 km/h
|max speed alt= 529 mph
|cruise speed main=
|cruise speed alt=
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range main= 2,930 km
|range alt=1,820 mi<ref name=g8/> 
|ceiling main= 11,200 m
|ceiling alt= 36,745 ft
|climb rate main=
|climb rate alt=
|loading main= 311 kg/m²
|loading alt=64 lb/ft²
|thrust/weight=0.3
|power/mass main=
|power/mass alt=
|more performance=
|armament=
* 2 × 23 mm [[Nudelman-Rikhter NR-23|NR-23]] cannon forward
* 2 × 23 mm [[Nudelman-Rikhter NR-23|NR-23]] cannon in tail [[turret]]
* Up to 3,000 kg (6,610 lb) of bombs, [[Naval mine|mines]] or [[torpedo]]es
|avionics=PSBN-M navigation radar
}}

==See also==
{{commons category|Tupolev Tu-14}}
* [[English Electric Canberra]]
* [[Ilyushin Il-28]]
* [[Tupolev Tu-12]]

==References==
{{reflist|30em}}

==Bibliography==
* {{cite book|last1=Duffy |first1=Paul |last2= Kandalov|first2=A. I. |title=Tupolev: The Man and His Aircraft |publisher=SAE }}
* {{cite book|last=Gordon|first=Yefim|last2=Rigamant|first2=Vladimir |title=OKB Tupolev: A History of the Design Bureau and its Aircraft|publisher=Midland Publishing|location=Hinckley, UK|year=2005|isbn=1-85780-214-4}}
* {{cite book|last=Gunston|first=Bill|authorlink=Bill Gunston|title=Tupolev Aircraft since 1922|publisher=[[Naval Institute Press]]|location=Annapolis, Maryland|year=1995|isbn=1-55750-882-8}}

<!-- == External links == -->

{{Tupolev aircraft}}

[[Category:Tupolev aircraft|Tu-0014]]
[[Category:Soviet bomber aircraft 1940–1949]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]