{{Infobox journal
| title = Melanoma Research
| cover = 
| abbreviation = Melanoma. Res.
| discipline = [[Melanoma]], [[Oncology]]
| editor = 
| publisher = [[Lippincott Williams & Wilkins]]
| country =
| history = 2004-present
| frequency = Monthly
| impact = 3.151
| impact-year = 2014
| website = 
| link1 = 
| link1-name = Online access
| link2 = 
| link2-name = Online archive
| ISSN = 0960-8931
| eISSN = 1473-5636
| LCCN = 
| OCLC = 24024301
| CODEN = 
}}
'''''Melanoma Research''''' (print: {{ISSN|0960-8931}}, online: {{ISSN|1473-5636}}) is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[Lippincott Williams & Wilkins]] since 1991. The journal publishes current findings on [[melanoma]]. Articles include both experimental and clinical research in a broad range of related subjects, including [[genetics]], [[molecular biology]], [[biochemistry]], [[cell biology]], [[photobiology]], [[pathology]], and [[immunology]], and advances in clinical [[oncology]] influencing the prevention, [[diagnosis]], and [[cancer treatment|treatment]] of melanoma.<ref>Lippincott Williams & Wilkins http://www.lww.com/product/?0960-8931</ref> ''Melanoma Research'' is published bimonthly. The current editors in chief are F.J. Lejeune and W.J. Storkus.

==Impact==
According to the 2014 [[Journal Citation Reports]], the journal has an [[impact factor]] of 2.282, ranking it 16th out of 62 in the category "[[Dermatology]]", 140th out of 211 in the category "Oncology", and 65th out of 123 in the category "Medicine, Research & Experimental".

==References==
{{reflist}}

==External links==
* {{Official|http://journals.lww.com/melanomaresearch/pages/default.aspx}}
*[http://www.patientpower.info/health-topic/skin-cancer Patient Power: Melanoma] Audio and video programs that connect, educate, and empower patients to take a proactive role in their healthcare.

[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Oncology journals]]
[[Category:Publications established in 1991]]