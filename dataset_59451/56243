{{Infobox journal
| title = Churchman
| cover = [[File:Churchman cover.jpg|150px]]
| editor = [[Gerald Bray]]
| discipline = [[Theology]]
| formernames = The Churchman 
| abbreviation = 
| publisher = [[Church Society]]
| country =
| history = 1879-present
| frequency = Quarterly
| website = http://www.churchsociety.org/churchman/
| link1 = 
| link1-name= 
| ISSN = 0009-661X
| LCCN = 
| OCLC = 
}}
'''''Churchman''''' is an [[evangelical Anglican]] academic journal published by the [[Church Society]]. It was formerly known as '''''The Churchman''''' and started in 1880 as a monthly periodical before moving to quarterly publication in 1920.<ref>{{cite journal|last=Wolffe|first=John|title=The First Century of ''The Churchman''|journal=Churchman|year=1988| volume=102|issue=3|url=http://www.churchsociety.org/churchman/documents/Cman_102_3_Wolffe.pdf|accessdate=24 October 2011}}</ref> The name change to "Churchman" came in 1977.<ref>{{cite journal|last=[Nixon|first=R. E.]|title=Editorial|journal=Churchman|year=1977| volume=90|issue=1|url=http://churchsociety.org/docs/churchman/091/Cman_091_1_Editorial.pdf|accessdate=23 February 2016}} (subheading: "Churchman"), p. 5.</ref> The [[editor-in-chief]] is [[Gerald Bray]]. 

Early editors included Walter Purton (1880–92), [[William Sinclair (son)|William McDonald Sinclair]] (1892–1901), [[Augustus Robert Buckland]] (1901–02), [[Henry Wace (priest)|Henry Wace]] (1902–05), [[William Griffith Thomas]] (1905–10) and [[Guy Warman]], jointly,  from 1910 to 1914.<ref> {{cite web|url = http://www.churchsociety.org/churchman/documents/Cman_102_3_Wolffe.pdf| title = The First Century of The Churchman|accessdate = 15 November 2013}} </ref>

Contributors to ''Churchman'' have included: [[J. C. Ryle]], [[J. Stafford Wright]], C. Sydney Carter, [[Geoffrey W. Bromiley]], [[Philip Edgecumbe Hughes]], Arthur Pollard, [[J. I. Packer]], Alan Stibbs, [[John Stott]], Roger Beckwith and [[J. A. Motyer]].<ref>[http://www.churchsociety.org/churchman/articles.asp Churchman back articles]</ref>

Among contributors have been Mary Strong, who in her introduction to "Letter of the Scattered Brotherhood" state she submitted and were published in ''The Churchman'' across a span of 14 years letters and writings from anonymous writings of genuine religious experience. These were later published in a collection: "Letters of the Scattered Brotherhood", 1948, New York, Harper & Row and subsq. The copyright continues.

== References ==
{{reflist|"Letters of the Scattered Brotherhood."  Mary strong, 1948, New York Harper & Row, Preface = }}

== External links ==
* {{Official|http://www.churchsociety.org/churchman/}}
* [http://biblicalstudies.org.uk/articles_churchman_01.php BiblicalStudies.org.uk: Index for ''Churchman''], accessed by decade → year → volume → issue → article.
{{humanities-journal-stub}}

[[Category:Protestant studies journals]]
→
[[Category:Publications established in 1879]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]