{{Italic title}}
'''''Higher-Order and Symbol Computation''''' (formerly '''''LISP and Symbolic Computation'''''; print: {{ISSN|1388-3690}}, online: {{ISSN|1573-0557}}) is a [[computer science]] [[academic journal|journal]] published by [[Springer Science+Business Media]]. It focusses on programming concepts and abstractions and [[programming language theory]].

== Editors ==
Former [[Editor-in-chief|editors-in-chief]] of the journal have been:
* [[Richard P. Gabriel]], [[Sun Microsystems, Inc.]], USA (1988 &ndash; 1991)
* [[Guy L. Steele, Jr.|Guy L. Steele Jr.]], Sun Microsystems, Inc., USA (1988 &ndash; 1991)
* [[Robert R. Kessler]], [[University of Utah]], USA (1991 &ndash; 1998)

The current editors-in-chief are  [[Olivier Danvy]] ([[Aarhus University]]) and [[Carolyn Talcott]] ([[SRI International]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in [[InfoTrac|Academic OneFile]], [[ACM Computing Reviews]], [[Association for Computing Machinery|ACM Digital Library]], [[Computer Abstracts International Database]], [[Computer Science Index]], [[Current Abstracts]], [[EBSCO Industries|EBSCO]], [[EI-Compendex]], [[Inspec|INSPEC]], [[io-port.net]], [[PASCAL (database)|PASCAL]], [[Scopus]], [[Serials Solutions|Summon by Serial Solutions]], [[VINITI Database RAS]], and [[Zentralblatt MATH]].

== See also ==
* ''[[Journal of Functional Programming]]''
* ''[[Journal of Functional and Logic Programming]]''
* ''[[Journal of Symbolic Computation]]''

== External links ==
* {{Official|http://www.springer.com/computer/theoretical+computer+science/journal/10990}}
* [http://www.brics.dk/~hosc/ Journal page] at [[Aarhus University]]
* [http://springerlink.metapress.com/content/1573-0557/ Online access]
* [http://www.informatik.uni-trier.de/~ley/db/journals/lisp/index.html DBLP]
* [http://liinwww.ira.uka.de/bibliography/Ai/higherordersymbcomput.html The Collection of Computer Science Bibliographies]


[[Category:Computer science journals]]
[[Category:Springer Science+Business Media academic journals]]