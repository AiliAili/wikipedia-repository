{{Infobox Weapon|is_ranged=yes|
name=Frank Wesson rifle
|image= 
|caption=
|origin={{flag|United States|1859}}
|type=[[Rifle]], [[Carbine]]
|designer=Frank Wesson
|design_date=1859, 1862
<!-- Service history -->
| used_by            = [[United States Army]]<br/>[[Missouri State Militia (pre-Missouri State Guard)|Missouri State Militia]]
| wars               = [[American Civil War]]<br/>[[American Indian Wars|Indian Wars]]
|service=1861–1865
<!-- Production history -->
|variants  = carbine, military, sporter
|number=44,000+
<!-- General specifications -->
|cartridge=.44 long [[rimfire ammunition|rimfire]] (military)<br /> .22 long<br />.32 long<br />.38 long 
|action=[[Break action]]
| barrels            = {{convert|24|in}} carbine<br /> up to {{convert|34|in}}
|rate= 8–10 shots per minute
|range= {{convert|200|yd|abbr=on}}
|max_range= {{convert|500|yd|abbr=on}}
|sights= tang and iron sights
|velocity= {{convert|1000|ft/s|abbr=on}}
|weight= {{convert|6|lb|abbr=on}} carbine, to {{convert|8.5|lb|abbr=on}}
|length= {{convert|43|in}}
|part_length=  
|feed=1 round
}}

'''Frank Wesson rifles''' were a series of single-shot rifles manufactured between 1859 and 1888 in Worcester, Massachusetts.  They were purchased by many state governments during the [[American Civil War]], including Illinois, Indiana, Kansas, Kentucky, Missouri, and Ohio.<ref>Flayderman, N. (2007). Flayderman's guide to antique American firearms-- and their values (9th ed.). Iola, WI: Gun Digest. p. 264</ref><ref name="ROTW">John Walter, Rifles of the World, p. 516, 2006</ref> They were one of the first rifles to use rimfire metallic cartridges.

The rifles were initially made in .22, .32, .38, and .44 rimfire.  Centerfire cartridges were added later, and some rifles were capable of firing rimfire or centerfire cartridges, by altering an adjustment on the hammer.

== History of the rifle ==
By 1859, there were a number of single-shot breech-loading rifles available to the American military and public.  These included the [[Sharps rifle]] (1848), the [[Smith carbine]] (1857), and others.  Those most suitable for military use were loaded through the breech, but required a separate percussion cap to ignite the cartridge.

Copper [[rimfire ammunition|rimfire]] cartridges which contained their own primer were introduced just prior to the American Civil War.  Only a few manufacturers came out with guns which could use this ammunition,these included the [[Henry repeating rifle]] (cartridge introduced in 1860), [[Spencer repeating rifle]], [[Maynard carbine]], Frank Wesson rifles, and Ballard rifles.  The .44 caliber Frank Wesson and Ballard rifles could use the same cartridge as each other, and these cartridges were very close in size to the .44 Henry rimfire.

The Frank Wesson rifle was the first breech-loading rifle designed for these metallic cartridges.<ref name=Artisan>American Artisan and Illustrated Journal of Popular Science, Nov 28, 1866, p. 52,53</ref>

Frank Wesson (1828-1899) and N.S. Harrington were granted patent 25,926, 'Improvement in Breech-Loading Fire-Arms' in 1859,<ref>http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=0025926.PN.&OS=PN/0025926&RS=PN/0025926</ref> and Frank Wesson was granted patent 36,925, 'Improvement in Breech-Loading Fire-Arms'<ref>http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=1&f=G&l=50&s1=0036925.PN.&OS=PN/0036925&RS=PN/0036925</ref> in 1862.

[[File:Frank Wesson Patent 36,925 - drawing1.jpg|thumb|Patent 36,925 drawing of breech opening mechanism, and slotted link for controlling opening of rifle.  Model shown is Frank Wesson two trigger model.]]

The 1862 patent added the use of a slotted link to stop the barrel from pivoting too far, which made the gun much easier and quicker to load.  The carbine with a 24 inch barrel weighed only 6 pounds, low weight being desirable in a weapon to be carried by cavalry.  The 28 and 34 inch barrel models weighing 7 and 8 pounds respectively.<ref>D. Appleton and Company (1865). The American Annual Cyclopedia and Register of Important Events of the Year 1864: Volume 4. New York. p. 639</ref>

By 1866, twenty thousand Frank Wesson rifles had been made, of which 8,000 were military.<ref name=Artisan />

=== Shooting Trials ===
On October 7, 1863, at the Missouri State Fair, in a contest between three infantry companies, shots fired from a Wesson rifle hit a man-sized target 45 times out of 100, at a distance of 300 yards.<ref>H.W.S. Cleveland, Hints to Riflemen, New York, (1864) pp. 151,152</ref>  In a trial at St. Louis, competing against other rifles, a man-sized target was hit 56 times out of 100, the second best rifle scoring 10 of 100.<ref name=Artisan />  At the Massachusetts State trial of breech-loading weapons, at Readville, 20 shots in a row hit a target at 200 yards, and 50 shots were fired in 4 minutes.<ref>Cleveland, p. 152</ref>  A Dr. I.J. Wetherbee, of Boston, shot a string of 12 shots at 110 yards, with telescopic sights, with a 34 inch barreled breech-loading rifle.  His target is shown here.<ref>Cleveland, p. 150</ref>
[[File:Target 110 yds Hints to Riflemen.PNG|thumb|Target shot by I.J. Wetherbee, at 110 yards, with telescopic sights, using a Two-Trigger (tip-up) Frank Wesson rifle.]]

The rifle was also tested at Leavenworth, Kansas, and by General P.F. Robinson, Kentucky.<ref>Parker Gillmore; Gun, Rod and Saddle: Personal Experiences, New York (1869), p. 285</ref>

== Use during the American Civil War ==
Between 3000 and 4000 of these carbines were used by the military during the war.  Over 2000 Wesson rifles were sold to militia in the states of Kentucky and Illinois.<ref>Walter, John.  Rifles of the World, 2006</ref> 44 caliber carbines were purchased by individuals and state governments during the Civil War.  Many of these weapons were sold through a company called Kittredge & Co, of Cincinnati, Ohio, who have their name stamped on the barrel of these weapons.

In July 1862, Brigadier-General J. T. Boyle of Kentucky complained about Gallagher guns, calling them 'worthless'.  He stated that 'They snap often, the cartridge hangs in after firing; difficult to get the exploded cartridges out often with screw-driver; men throw them away and take musket or any other arm.  They are unquestionably worthless.'  He then requested 'Sharps, Wessons, Ballards, or any other kind of carbine.'  He mentioned that Wesson carbines can be had for $25 or less from Cincinnati.<ref>Official Records, Vol. 16, p. 750</ref>

The Wesson carbine was used primarily by the Union armies, as the Confederacy did not manufacture rim fire cartridges.  However, in November 1862, the Confederacy did arrange for the smuggling of 10 Wesson rifles, and 5,000 cartridges to Texas, via Cuba.  These rifles were smuggled by Harris Hoyt, who was brought to trial in January 1865.  The rifles at that time cost $25 each, the cartridges $11 per thousand.<ref>United States. Senate Documents, Otherwise Publ. as Public Documents and Executive ..., (1870-1871) pp. 49, 87</ref>

Wesson carbines were in use at [[battle of Gettysburg]] in 1863,<ref>Edward G. Longacre, The Cavalry at Gettysburg: A Tactical Study of Mounted Operations During the Civil War's Pivotal Campaign, 9 June-14 July 1863, Associated University Presses, (1986) p. 60</ref> and at the [[battle of Westport]] in 1864.<ref name=":0">{{Cite book|title=The Last Hurrah, Sterling Price's Missouri Expedition of 1864|last=Sinisi|first=Kyle|publisher=Rowman & Littlefield|year=2015|isbn=978-0-7425-4535-9|location=Lanham, Maryland|pages=226-227, 253-254|via=}}</ref>

In January 1863, the state of Ohio had 150 Wesson carbines on hand.  At that time, they also had 54,000 Wesson cartridges. The 11th Cavalry regiment was issued 100 Wesson carbines and 400 Spencer repeating rifles.<ref>Ohio, Executive Documents, Annual Reports made to the Governor of the State of Ohio for the year 1863, Part 2 (1864) pp. 603, 607</ref>  The states of Kentucky and Illinois purchased 2000 for their state militias.<ref name="ROTW" />

In July 1863, in a letter to Major S.B. Shaw, St. Louis, Mo, T.F. Robinson, Quartermaster-General of Kentucky reported that portions of two Kentucky regiments had been armed with the "Wesson Cartridge Rifle", were pleased with it, and would not exchange it for any other rifle.<ref>New York Times, July 11, 1863</ref>

Following the [[Lawrence Massacre]] on August 21, 1863, the surviving men formed a rifle company, using Wesson rifles.  These weapons, along with their revolvers were carried at all times.<ref>Edward E. Leslie, Devil Knows How to Ride: The True Story of William Clarke Quantrill and His Confederate Raiders, Random House, Inc, New York (1996)</ref>

Kentucky purchased 1366 Wesson carbines, Indiana purchased 760 for their cavalry, from B. Kittredge & Co., Cincinnati,<ref>William H. H. Terrell, Indiana in the War of the Rebellion, Indiana Adjutant General's Office (1869), p. 177, 436</ref> and the numbers for Ohio, Kansas and Missouri are not known.  Individual soldiers of Kansas and Missouri purchased an unknown number of these rifles. Missouri had over 690 in their regimental armories in 1864.<ref>McAulay, John D, Carbines of the US Cavalry 1861-1905, PIONEER PRESS, Union City, TN, 1997, pp. 119-122</ref>

In 1862, the 7th Kentucky Cavalry was issued 500 Wesson carbines at a price of $25 each.<ref>OFFICIAL RECORDS: Series 1, vol 16, Part 2 (Morgan's First Kentucky Raid, Perryville Campaign) p. 230</ref> In the summer of 1863, individuals from the 11th Regiment, 1st Battalion, Ohio Volunteer Cavalry, purchased 200 Wesson rifles.  Companies I and L were also issued Wesson rifles by the state.<ref>http://11thohiocavalry.com/Uniforms___Equipment.html</ref>  At least 300 individuals from the 5th Missouri State Militia (MSM) Cavalry purchased this weapon during the war, some of which were used during Price's Raid.<ref>Annual Report of the Adjutant General of Missouri for the year ending December 31, 1865, Missouri Office of the Adjutant General, (1866), page 486</ref>  In that raid, the men of the 5th were placed on specific parts of the battlefield in order to take advantage of their more rapid rate of fire.<ref name=":0" />  Troopers from the 3rd MSM also carried Wesson carbines.<ref>Douglas L. Gifford, The Battle of Pilot Knob Staff Ride and Battlefield Tour Guide, Winfield, Missouri, ISBN 1-59196-478-4 (2003) p. 10</ref>  At that time, the cost of this carbine was $40 each.<ref>Charles G. Worman, Gunsmoke and Saddle Leather: Firearms in the Nineteenth-century American West (2005),  University of New Mexico Press, p. 55</ref>  The 48th Illinois Infantry carried an unknown number of Wesson carbines, and ran out of ammunition for these rifles during the [[Battle of Ezra Church]] (part of the Atlanta campaign) on July 28, 1864.  During that battle, some regiments expended 100 bullets per soldier.<ref>James B. Swan, Chicago's Irish Legion: The 90th Illinois Volunteers in the Civil War, Southern Illinois University (2009), p. 154</ref><ref>The War of the Rebellion: A Compilation of the Official Records of the Union and Confederate Armies, Series 1, vol 38, Part 3 (The Atlanta Campaign), p. 344</ref>  At the Battle of Westport, also during Price's Raid, some men of the 19th Kansas State Militia carried this rifle.<ref name=":0" />

=== Advantages of rimfire breech-loading rifles ===
The primary advantages of rimfire breech-loading rifles during the [[American Civil War]] were that the rifle could be loaded easily, that the ammunition was self-contained, and that the ammunition was relatively weather-proof.

Most rifles used during the [[American Civil War]] were muzzle loading rifles.  To load these rifles, powder would be poured into the muzzle, a patch would be placed around the ball or Minie ball, the ball would be forced down the barrel using a ramrod, the ramrod would be replaced, a percussion cap would be pressed onto a nipple on the rifle, and the hammer would be drawn back.

To load these rifles quickly, it was necessary for the soldier to stand.  This made these rifles difficult to use for cavalry, which required that the cavalry typically be dismounted if using muzzle loading rifles.<ref>Lt. Col B. F. Lazear, Official Records of the War of the Rebellion, Serial 83 p. 0361 Chapter LIII.  Price's Missouri Expedition, 1891</ref>  Breech loading rifles, on the other hand, could be loaded while mounted on horseback.

Most rifles, including some that were breech-loading, also used a percussion cap, which required that the soldier carry percussion caps in addition to other ammunition requirements.  These requirements would have been powder, bullets, patches, and percussion caps.  A soldier using a Frank Wesson or [[Henry rifle]] would have only needed to carry cartridges.

For rifles which used separate ball and powder, the ball and powder would be contained in a paper cartridge.  In wet weather, these cartridges were easily damaged.  The copper cartridges used by Henry, Wesson, Ballard and others tolerated moisture better, and were more impervious to wet conditions.

== Use and sales following the American Civil War ==
Frank Wesson rifles continued to be made until 1888.  On November 21, 1877, [[Buffalo Bill]], following one of his Wild West shows, competed with Lincoln C. Daniels, a marksman.  Both competitors were using new Wesson rifles.  Other members in his troupe also used Wesson rifles.  The competition was held in Worcester, Massachusetts, where the Wesson factory was located.<ref>Sandra Sagala, Buffalo Bill on Stage, University of New Mexico Press</ref>

In 1867, the War Department authorized sale of many weapons, including 19,551 weapons at the Leavenworth arsenal in Kansas.  This sale included Wesson carbines, as well as many other revolvers, rifles and carbines of the period (29 different types).<ref>United States, Reports of Committees: 30th Congress, 1st Session - 48th Congress, 2nd Session, Washington Government Printing Office (1872), p. 64</ref>

In 1869, the War Department purchased far fewer weapons of all kinds than it had in the Civil War.  Only three Frank Wesson carbines were purchased during the year, for $20 each. At the same time, it purchased, or had modified 13,098 Sharps carbines (listed as 'Sharps carbines, altered'), at approximately $4 each.<ref>United States, Senate Documents, Otherwise Published as Public Documents and Executive Documents, 14th Congress, 1st Session-48th Congress, 2nd Session and Special Session (1870).  See p. 4 of Document 17</ref>

As late as 1873, Indiana still had 716 Kittredge marked Wesson carbines in their armory.<ref>Report Adjutant General the State of Indiana Year Ending December 31, 1873, Indianapolis (1874) p. 2</ref>  In June 1873, Kentucky had 314 of these carbines,<ref>Kentucky Public Documents, Volume 1 (1873) p. 27</ref> and in August 1875, had 298 of these rifles in good condition, erroneously recorded as 'Smith & Wesson rifles, calibre .44', though they were Wesson carbines.  These rifles were stored in their state arsenal.<ref>[https://books.google.com/books?id=vepAAQAAMAAJ&lpg=PA3&ots=KnwgZxD-2Q&dq=ANNUAL%20REPORT%20OF%20THE%20TREASURER%20OF%20THE%20STATE%20OF%20KENTUCKY%20FROM%20OCTOBER%2011TH%2C%201874&pg=PA3#v=onepage&q&f=false Annual Report of the Treasurer of the State of Kentucky from October 11th, 1874 to October 10th, 1875], p. 18</ref>

Bullets from a Frank Wesson rifle were found at the Hembrillo Battlefield (1880), New Mexico;<ref>Laumbach, Karl W, 'Hembrillo, an Apache battlefield of the Victorio war: the archaeology and history of the Hembrillo battlefield', 2000</ref> and at the Battle of Little Big Horn (1876), Montana.<ref>Wagner, Frederic C. III, 'Participants in the Battle of the Little Big Horn: A Biographical Dictionary of Sioux, Cheyenne and United States Military Personnel', p. 232</ref>

==Rifle models==
On all 'two-trigger' models, the front trigger opened the rifle, the rear trigger fired it.
The following models of rifles were made by Frank Wesson.  Dates are approximate.  Military models had a three position sight, for 100, 250 and 500 yards.

=== Two-trigger models ===

Models:<ref>Flayderman, pp 264-265</ref>
* 1st Type - a slotted link to keep the barrel from swinging too high was on the right hand side of the rifle.  In later years of this model, an extractor was on the left hand side.  The frames were iron or brass. (1859-1864)
* 2nd Type - the slotted link moved to the left hand side of the rifle, the extractor moved to the right side.  Iron frames only. (1863-1876)
* 3rd Type - an adjustable hammer, patented in 1872, was available for rimfire or centerfire. (1872-1888)
* 4th Type (aka Mid Range No. 2) - globe front sight replaced the German silver sight. (1872-1888)
* 5th Type (aka New Model) - Frame was flat on vertical sides to the left and right of the hammer mechanism.(1876-1888)

==Calibers of Ammunition ==
The Wesson rifle was available in .22, 32, .38 and .44 rimfire.  It was also available in 30-30 Wesson and .44 Wesson Extra Long.<ref>Woodard, W. Todd; Cartridges of the World: A Complete and Illustrated Reference for more than 1500 Cartridges, 14th Edition; 2014</ref>

==See also==
*[[Rifles in the American Civil War]]
*[[Henry rifle]], .44 calibre
*[[Maynard carbine]], .50 calibre, requires percussion cap
*[[Sharps rifle]], .56 calibre
*[[Smith carbine]], .50 calibre, requires percussion cap
*[[H & R Firearms]]

==References==
{{reflist}}

*
*
*
*

[[Category:American Civil War rifles]]
[[Category:Early rifles]]
[[Category:Firearm manufacturers of the United States]]
[[Category:Rifles of the United States]]
[[Category:Carbines]]
[[Category:Guns of the American West]]
[[Category:Single-shot rifles]]