{{Infobox officeholder
|name             = Milan Balažic
|image            = 
|honorific-suffix = [[Ambassador]]
|birth_date       = 
|birth_place      = Ljubljana, Slovenia
|nationality      = Slovenian
|residence        =
|alma_mater       = [[University of Ljubljana]] 
}}

'''Milan Balažic''' (born 19 October 1958) is a [[Slovenian]] [[political]] [[theorist]], [[politician]] and [[diplomat]]. 

== Early life ==
Balažic was born in [[Ljubljana]], [[Slovenia]], then part of former [[Yugoslavia]], to a family originating from [[Prekmurje]]. In 1985 he graduated in [[political science]] from the [[University of Ljubljana]], with the thesis The critic of scientific objectivism: monopoly capital and revolutionary subject. In 1994 he obtained his PhD under the supervision of [[Rastko Močnik]], analysing the approaches of [[Frankfurt School]] and [[Lacan]]'s [[psychoanalysis]] to the social and political field.

As a student, he was the editor-in-chief of the student paper Tribuna<ref>[[:sl:Tribuna (časopis)]]</ref>  and a member of the editorial board of the Journal for the Criticism of Science.

==Political career==
In the period of the [[Slovene Spring]] Balažic was a member of the Alliance of Socialist Youth of Slovenia, the autonomous branch of the [[Communist Party]].<ref>[[:sl:Zveza socialistične mladine Slovenije]]</ref>  At the time of the [[JBTZ trial]]<ref>JBTZ trial</ref> he acted as its representative in the [[Igor Bavčar]]'s [[Committee for the Defence of Human Rights]].<ref>Committee for the Defence of Human Rights</ref> He was the vice president of the [[League of Communists of Slovenia]] where he led, together with [[Borut Pahor]]<ref>Borut Pahor</ref> the pro-reformist wing which advocated social-democratisation of the party, immediate implementation of democracy, joining the [[European Union]] and independent Slovenia. Later he became a member of the ruling [[Liberal Democracy of Slovenia]]. In the [[Slovenian parliamentary election, 1992]], he was elected a member of the [[National Assembly of Slovenia]].<ref>[[:sl:Seznam slovenskih poslancev (1990-1992)]]</ref> During the 10-day war in Slovenia he attended the parlimentary sessions wearing the military uniform of the territorial defence forces. In 2016 he established the political movement The New Future. 

== Diplomacy ==
In 1996 Balažic continued his career in diplomacy. At the [[Ministry of Foreign Affairs (Slovenia)]]<ref>[http://www.mzz.gov.si/en/ Ministry of Foreign Affairs | Government of the Republic of Slovenia<!-- Bot generated title -->]</ref> he held the position of an undersecretary of state and acted as the head of the Department for Analysis and Development. He was the founder of the first Slovenian diplomatic academy. In 2008 he took the position of state secretary for strategic affairs and the Ministry's official spokesman.<ref>[http://monde-diplomatique.si/slovenija-in-hrvaska-med-diplomacijo-in-politiko-milan-balazic Slovenija in Hrvaška med diplomacijo in politiko; Milan Balažic - Monde Diplomatique<!-- Bot generated title -->]</ref> Between 2011 and 2014 he served as the Ambassador of the Republic of Slovenia to [[Australia]], [[New Zealand]], [[Indonesia]] and [[Association of Southeast Asian Nations]]. 

==Academic career==
Between 1999 and 2011 Balažic was a professor in a political science and the head of the Department of Political Science<ref>http:// www.politologija.si/1/Katedra/Clanikatedre/MilanBalazic.aspx)</ref>  at the [[University of Ljubljana]]. He also lectured at universities and diplomatic academies in [[Europe]], [[Asia]] and [[Australia]]. His academic work can generally be categorised as politically philosophical and psychoanalytically epistemological. The first part of his writing is predominantly dedicated to the analysis of the era of Slovenian democratisation and independence, whereas the second part addresses the influence of [[psychoanalysis]] on social sciences.

Balažic is also a political commentator and columnist in a daily papers and magazines.<ref>[http://www.finance.si/avtor.php?id=MBA Finance.si<!-- Bot generated title -->]</ref> The main thread is his support for social sustainability, liberal reforms, deregulation and privatisation.

== Personal life ==
Balažic is an active tennis player, mountaineer and guitarist. He was a bass player in the cult Slovenian punk rock band Lublanski psi. He is also the author of two books of poetry. 

Since 2006 Balažic has been in a relationship with the Lacanian psychoanalyst and philosopher Nina Krajnik.  

==Bibliography==
*Gospostvo (Domination), 1995
*Čudežna zgodbica: pesmi (Miraculous Little Story: Poems), 1997
*Ranjena žival: nove pesmi (Wounded Animal: New Poems), 1998 
*Slovenska demokratična revolucija (Slovenian Democratic Revolution), 2004
*Slovenski berlinski zid (Slovenian Berlin Wall), 2006
*Psihoanaliza politike (Psychoanalysis of Politics), 2007
*Politična antifilozofija (Political Antiphilosophy), 2007
*Znanost in realno (Science and the Real), 2008
*Rojstvo slovenske demokracije (Birth of Slovenian Democracy), 2010
*Slovenski plebiscit (Slovenian Plebiscite), 2010
*Samostojna Slovenija (Independent Slovenia), 2011

== References ==
(Some are in the Slovene language.)
{{reflist}}
* (2011) The Social Symptom, Lacanberra Project, Vol. I, Issue 1, Autumn-Winter 2011, Canberra, ACT, p.&nbsp;6-14, http://lacanberraproject.blogspot.com/
* (2012a) The Discourse of the Capitalism, Lacanberra Project, Vol. II, Issue 2, Spring-Summer 2012, Canberra, ACT, p.&nbsp;3-11,http://lacanberraproject.blogspot.com/
* (2012b) Return to the Politics of Psychoanalysis, Lacanberra Project, Vol. III, Issue 3, Autumn-Winter 2012, Canberra, ACT, p.&nbsp;9-16, http://lacanberraproject.blogspot.com/
* (2013) The Hypermodern Discourse of XXI. Century, Lacanberra Project, Vol. III, Issue 4, Spring-Summer 2013, Canberra, ACT, p.&nbsp;3-14,<http://lacanberraproject.blogspot.com/

== External links ==
* http://www.mzz.gov.si/en/ Ministry of Foreign Affairs of RS
* http://www.fdv.uni-lj.si/en/ Faculty of Social Sciences, University of Ljubljana

{{DEFAULTSORT:Balazic, Milan}}
[[Category:Living people]]
[[Category:1958 births]]
[[Category:People from Ljubljana]]
[[Category:Slovenian political theorists]]
[[Category:Slovenian politicians]]
[[Category:Slovenian diplomats]]
[[Category:University of Ljubljana alumni]]
[[Category:Government ministers of Slovenia]]