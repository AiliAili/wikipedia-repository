<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{lowercase|de Havilland Hyena}}
{|{{Infobox Aircraft Begin
 | name=DH.56 Hyena
 | image=De Havilland DH.56 Hyena.jpg
 | caption= The DH.56 Hyena J-7780
}}{{Infobox Aircraft Type
 | type=Army cooperation aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=[[de Havilland]]
 | designer=
 | first flight=17 May 1925
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=2
 | developed from= de Havilland DH.42B Dingo II
 | variants with their own articles=
}}
|}
The '''de Havilland DH.56 Hyena''' was a prototype [[United Kingdom|British]] army cooperation aircraft of the 1920s. A single-engined [[biplane]], the Hyena was designed against an RAF requirement, but was unsuccessful with only two being built, the [[Armstrong Whitworth Atlas]] being preferred.

==Development and design==
The DH.56 Hyena was developed to meet the requirements of [[List of Air Ministry specifications|Air Ministry Specification 30/24]] for an Army Cooperation aircraft to equip Britain's [[Royal Air Force]].<ref name="Mason Bomber p172"/>  It was a development of de Havilland's earlier [[de Havilland Dormouse|DH.42B Dingo]], and like the Dingo, was a single-engined two-bay [[biplane]] carrying a crew of two.  It was armed with a forward-firing [[Vickers machine gun]] and a [[Lewis gun]] operated by the observer.  A hook to pick up messages was fitted beneath the fuselage, while the aircraft was also equipped for photography, artillery spotting, supply dropping and bombing.<ref name="Jackson p213">Jackson 1987, p.213.</ref>

The first Hyena flew on 17 May 1925,<ref name="Mason Bomber p172"/> powered by a 385&nbsp;hp (287&nbsp;kW) [[Armstrong Siddeley Jaguar]] III [[radial engine]]. With this engine it was underpowered, and was quickly re-engined with a 422&nbsp;hp (315&nbsp;kW) Jaguar IV before it was submitted for official testing (which was against the requirements of Specification 20/25, which had superseded 30/24).<ref name="Mason Bomber p172"/>  The two prototype Hyenas were tested against the other competitors for the RAF's orders, the [[Armstrong Whitworth Atlas]], the [[Bristol Bloodhound]]  and the [[Vickers Vespa]], including field evaluation with [[No. 4 Squadron RAF]].<ref name="Jackson p214-5">Jackson 1987, pp. 214–215.</ref> Handling close to the ground was found to be difficult, with a poor view from the cockpit, and the orders went to the Atlas, with the Hyena being abandoned, being used for testing at the [[Royal Aircraft Establishment]], [[Farnborough Airfield|Farnborough]] until 1928.<ref name="Mason Bomber p172"/>

==Specifications (Jaguar IV) ==

<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value e.g. "at low level", "unladen". -->

{{aircraft specifications

|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->

|ref=De Havilland Aircraft since 1909 <ref name="Jackson p215">Jackson 1994, p.215.</ref>

|crew=two
|capacity= <!-- the number of passengers carried in the case of a commercial aircraft-->
|payload main=
|payload alt=
|payload more=
|length main= 29 ft 9 in
|length alt=9.07 m
|span main=43 ft 0 in
|span alt=13.11 m
|height main=10 ft 9 in
|height alt=3.28 m
|area main= 421.25 ft²
|area alt= 39.1 m²
|airfoil=
|empty weight main= 2,399 lb
|empty weight alt= 1,090 kg
|loaded weight main= 4,200 lb
|loaded weight alt= 1,909 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|max takeoff weight more=
|more general=

|engine (prop)=[[Armstrong Siddeley Jaguar]] IV
|type of prop=14-cylinder air-cooled [[radial engine]] <!-- meaning the type of propeller driving engines -->
|number of props=1<!--  ditto number of engines-->
|power main= 422 hp
|power alt=315 kW
|power original=
|power more=

|max speed main= 130 mph
|max speed alt=113 knots, 209 km/h
|max speed more= 
|cruise speed main= 
|cruise speed alt=
|cruise speed more 
|stall speed main= 
|stall speed alt= 
|stall speed more=
|never exceed speed main= 
|never exceed speed alt= 
|range main= 
|range alt=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main= 19,230 ft <ref name="Mason Bomber p172">Mason 1994, p.172.</ref>
|ceiling alt= 5,860 m
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=*'''Climb to 10,000 ft (3,050 m):''' 13 min 24 sec<ref name="Mason Bomber p172"/>

|guns= 1× forward firing [[.303 British|.303 in]] (7.7 mm) [[Vickers machine gun]] and 1 × .303 in (7.7 mm) [[Lewis Gun]] on [[Scarff ring]] in rear cockpit
|bombs= Four light bombs under port wing
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=*[[de Havilland Dormouse|de Havilland Dingo]]<!-- related developments -->
|similar aircraft=*[[Armstrong Whitworth Atlas]]
*[[Bristol Boarhound]]
*[[Vickers Vespa]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*Jackson, A.J. ''De Havilland Aircraft since 1909''. London:Putnam, Third edition 1987. ISBN 0-85177-802-X.
* Mason, Francis K. ''The British Bomber since 1914''. London:Putnam, 1994. ISBN 0-85177-861-5.
{{refend}}

==External links==
{{commons category|De Havilland DH.56 Hyena}}
*[http://www.flightglobal.com/pdfarchive/view/1927/1927%20-%200165.html De Havilland Hyena]
{{de Havilland aircraft}}

[[Category:British bomber aircraft 1920–1929]]
[[Category:De Havilland aircraft|Hyena]]