'''Grenzplankostenrechnung (GPK)''' is a German costing methodology, developed in the late 1940s and 1950s, designed to provide a consistent and accurate application of how managerial costs are calculated and assigned to a product or service. The term Grenzplankostenrechnung, often referred to as GPK, has been translated as either ''Marginal Planned Cost Accounting''<ref name=Friedl>{{cite journal|last1=Friedl|first1=Gunther|author2=Hans-Ulrich Kupper and Burkhard Pedell |title=Relevance Added: Combining ABC with German Cost Accounting|journal=Strategic Finance|issue=June|pages=56–61|date=2005}}</ref> or ''Flexible Analytic Cost Planning and Accounting''.<ref name=Sharman>{{cite journal|last=Sharman|first=Paul A.|title=Bring On German Cost Accounting|journal=Strategic Finance|issue=December|pages=2–9|date=2003}}</ref>
 
The GPK methodology has become the standard for cost accounting in Germany <ref name=Sharman/> as a "result of the modern, strong controlling culture in German corporations".<ref name=Vikas>{{cite journal |last1=Sharman |first1=Paul A. |author2=Kurt Vikas|title=Lessons from German Cost Accounting|journal=Strategic Finance |issue=December |pages=28–35|date=2004}}</ref> German firms that use GPK methodology include Deutsche Telekom, [[Daimler AG]], Porsche AG, Deutsche Bank, and Deutsche Post (German Post Office). These companies have integrated their costing information systems based on ERP (Enterprise Resource Planning) software (e.g., [[SAP AG|SAP]]) and they tend to reside in industries with highly complex processes.<ref name=Kip>{{cite journal|last=Krumwiede|first=Kip R.|title=Rewards And Realities of German Cost Accounting|journal=Strategic Finance|issue=April|pages=27–34|date=2005}}</ref> However, GPK is not exclusive to highly complex organizations; GPK is also applied to less complex businesses.

GPK's objective is to provide meaningful insight and analysis of accounting information that benefits internal users, such as controllers, project managers, plant managers, versus other traditional costing systems that primarily focus on analyzing the firm's profitability from an external reporting perspective complying with financial standards (i.e., [http://www.iasb.org/Home.htm IFRS]/[http://www.fasb.org FASB]), and/or regulatory bodies' demands such as the [http://www.sec.gov Securities and Exchange Commission] (SEC) or the [http://www.irs.gov Internal Revenue Services] (IRS) taxation agency. Thus, the GPK marginal system unites and addresses the needs of both financial and managerial accounting functionality and costing requirements.

[[Resource Consumption Accounting|Resource Consumption Accounting (RCA)]] is based, among others, on key principles of German managerial accounting that are found in GPK.<ref name=clinton>{{cite journal|last1=Clinton|first1=B. D.|author2=Sally Webber|title=RCA at Clopay|journal=Strategic Finance|issue=October|date=2004|pages=21–26}}</ref>

==Background==
The origins of GPK are credited to [[:de:Hans-Georg Plaut|Hans-Georg Plaut]], an automotive engineer and Wolfgang Kilger, an academic, working towards the mutual goal of identifying and delivering a sustained methodology designed to correct and enhance cost accounting information.<ref name=Vikas/> Plaut concentrated on the practical elements of GPK, while Kilger provided the academic discipline and GPK documentation that is still being published in cost accounting textbooks taught in German-speaking universities. The primary textbook on GPK is ''Flexible Plankostenrechnung und Deckungsbeitragsrechnung''.<ref>{{cite book|last=Kilger|first=Wolfgang|others=Updated by Kurt Vikas and Jochen Pampel|title=Flexible Plankostenrechnung und Deckungsbeitragsrechnung|edition=11th|publisher=Gabler GmbH|location=Wiesbaden,Germany|year=2002}}</ref>

In 1946, Plaut founded an independent consulting business in Hannover, Germany which continued to grow employing more than 2,000 consultants.<ref name=Vikas/> Plaut and Kilger focused on creating a cost accounting system that would cater to managers who are responsible for controlling costs, managing profits and providing information that would enable managers to make informed decisions.

== Concepts of GPK ==
GPK is a marginal costing system and is decidedly more comprehensive than most U.S. cost management systems because of the level of organizational planning and control and its emphasis on accurate operational modeling.<ref name=Keys>{{cite journal|last1=Keys|first1=David E.|author2=Anton van der Merwe|title=German vs. United States Cost Management: What insights does German cost management have for U.S. companies?|journal=Management Accounting Quarterly|volume=1|issue=Fall, number 1|date=1999}}</ref>

With GPK's marginal-based approach, internal service and saleable product/service costs should only reflect the direct and indirect costs that can be linked to individual outputs (whether final product or support service) on a causal basis (referred to as the ''principle of causality''). Proportional costs in GPK consists of direct and indirect costs that will vary with the particular output. Proportional costs provide the first contribution margin level that supports short-term decisions and once proportional costs are subtracted from revenue, it reveals whether the product or service is profitable or not. GPK adopters' marginal practices have varied, for example, not all adopters adhere to strict marginal practices such as the pre-allocation of fixed costs based on planned product/service volumes.

Fixed costs, innately do not vary with outputs and usually are not associated with individual outputs' costs. However, in practice, GPK adopters often calculate a standard per-unit-rate for fixed product/service costs and a separate per-unit-rate for proportional product/service costs. The balance of costs not causally assignable to the lowest level product or service can be assigned at yet higher levels within the marginal costing system's multi-level Profit & Loss (P&L) statement. For example, with GPK, fixed costs that relate to a product group or a product line (e.g., R&D, advertising costs) are assigned to the product group or product line reporting/management dimension in the P&L. This marginal costing approach offers managers greater flexibility to view, analyze and monitor costs (e.g., all product and cost-to-serve costs) for their area of responsibility. Thus GPK assigns all costs to the P&L but it does not fully absorb to the lowest level product or service. GPK's multi-dimensional marginal view of the organization supports operational managers with the most relevant information for strategic decision-making purposes about "what products or services to offer" and at "what price to sell them".<ref name=Sharman/>

==Core elements of GPK==
According to German Professors Dr.'s Friedl, Kuepper and Pedell,<ref name=Friedl/> the fundamental structure of GPK consists of four important elements: 
#Cost-type accounting, 
#Cost center accounting, 
#Product[service] cost accounting, and 
#Contribution margin accounting for profitability analysis.

* '''''Cost-type accounting''''' separates costs like labor, materials, and depreciation, followed by each cost account then being broken down into ''fixed'' and ''proportional'' costs along with the assignment of these cost accounts to cost centers.
* '''''Cost center accounting''''' is the most important element in GPK. A cost center can be defined as an area of responsibility that is assigned to a manager who is held accountable for its performance. It is common to have from 200 to over 2,000 cost centers in a typical GPK adopter organization.

GPK distinguishes two types of cost centers: 
*''Primary Cost Centers'' - are cost centers that provide output directly consumed by a saleable product or service is considered to be a ''primary'' cost center. related to the service or manufacturing process. 
*''Secondary Cost Centers'' - are cost centers that incur costs but exist to support the functions of the primary cost centers. Typical secondary cost centers include: information technology (IT) services and; human resources (HR) areas that offer hiring and training functions.
With the GPK marginal costing approach, ''primary cost centers'' outputs consumed by products/services reflect direct causal relationships, as well as causally-linked costs originating from supporting ''secondary cost centers'' that primary cost centers need to function. As such, both of these causally-linked outputs—if proportional in nature—will vary with product/service output volume (albeit the secondaries only indirectly) and are reflected in the appropriate product/service contribution margin in the P&L.
* '''''Product/Service cost accounting''''' also referred to as ''Product Costing'', is where all of the assigned costs that are ''product related'' will be collected in the GPK costing model. In GPK's purest marginal form only proportional costs are assigned to products or services, but as indicated above a compromise is often struck by also assigning product-related fixed costs.
* '''''Profitability management''''' is the final component that completes the marginal costing system by adding in the revenues, cost-to-serve and common fixed costs along with the ''product/service cost accounting'' information discussed above. (Refer to the Exhibit below for a graphic depiction of cost flows in GPK.) The GPK structure allows for a more detailed analysis because of the multi-dimensional contribution margin view. This type of multi-level ''profitability management'' not only supports short-term decision making such as pricing decisions or internal pricing transfers, but it also provides relevant costing information for long-term decisions.<ref name=Friedl/>

==GPK marginal costing diagram==

[[Image:GPK Marginal Costing Structure Flow v4.jpg|GPK Marginal Costing Structure Flow]]

==References==

===Footnotes===
{{reflist}}

===Additional sources===

*{{cite journal|last1=Clinton|first1=B.D.|author2=Sally Webber|title=RCA at Clopay|journal=Strategic Finance|issue=October|pages=21–26|date=2004|issn=1524-833X}}
*{{cite journal|last1=Friedl|first1=Gunther|author2=Hans-Ulrich Kuepper and Burkhard Pedell|title=Relevance Added: Combining ABC with German Cost Accounting|journal=Strategic Finance|issue=June|pages=56–61|date=2005|issn=1524-833X}}
*{{cite journal|last=Gaiser|first=B.|title=German Cost Management Systems Part 1|journal=Journal of Cost Management|issue=September/October|pages=35–41|date=1997|issn=1092-8057}}
*{{cite journal|last=Gaiser|first=B.|title=German Cost Management Systems Part 2|journal=Journal of Cost Management|issue=November/December|pages=41–45|date=1997|issn=1092-8057}}
*{{cite journal|last1=Keys|first1=David E.|author2=Anton van der Merwe|title=German vs. United States Cost Management: What insights does German cost management have for U.S. companies?|journal=Management Accounting Quarterly|volume=1 |issue=Fall, number 1|pages=1–8|date=1999|issn=1092-8057}}
*{{cite book|last=Kilger|first=Wolfgang|others=Updated by Kurt Vikas and Jochen Pampel|title=Flexible Plankostenrechnung und Deckungsbeitragsrechnung|edition=11th|publisher=Gabler GmbH|location=Wiesbaden,Germany|year=2002}}
*{{cite journal|last1=Kilger|first1=Wolfgang|author2=J. Pampel & K. Vikas|title=0 Introduction: Marginal Costing as a Management Accounting Tool|journal=Management Accounting Quarterly|volume=5 |issue=Winter, number 2 |pages=7–28|date=2004|issn=1092-8057}}
*{{cite journal|last1=Krumwiede|first=Kip R.|author2=Augustin Suessmair|title=Comparing U.S. and German Cost Accounting Methods|journal=Management Accounting Quarterly|volume=8|pages=1–9|issue=Spring, number 3 |date=2007|issn=1092-8057}}
*{{cite journal|last=Krumwiede|first=Kip R.|title=Rewards And Realities of German Cost Accounting|journal=Strategic Finance|issue=April|pages=27–34|date=2005|issn=1524-833X}}
*{{cite journal|last=MacArthur|first=J.|title=Cultural Influences on German versus U.S. Management Accounting Practices|journal=Management Accounting Quarterly|volume=7|issue=Winter, number 2 |pages=10–16|date=2006|issn=1092-8057}}
*{{cite journal|last=MacKie|first=B.|title=Merging GPK and ABC on the Road to RCA: A Toronto Children’s Hospital Implementation|journal=Strategic Finance|issue=November|date=2006|issn=1524-833X}}
*{{cite journal|last1=Sharman|first1=Paul A.|author2=Kurt Vikas|title=Lessons from German Cost Accounting|journal=Strategic Finance|issue=December|pages=28–35|date=2004|issn=1524-833X}}
*{{cite journal|last=Sharman|first=Paul A.|title=Bring On German Cost Accounting|journal=Strategic Finance|issue=December|pages=2–9|date=2003|issn=1524-833X}}
*{{cite journal|last=Sharman|first=Paul A.|title=The Case for Management Accounting|journal=Strategic Finance|issue=October|pages=XXX|date=2003|issn=1524-833X}}
*{{cite journal|last=Smith|first=Carl S.|title=Going for GPK: Stihl Moves towards this Costing System in the U.S.|journal=Strategic Finance|issue=April|pages=36–39|date=2005|issn=1524-833X}}
*{{cite journal|last1=Thomson|first1=Jeff|author2=Jim Gurowka|title=ABC, GPK, RCA, TOC: Sorting Out the Clutter|journal=Strategic Finance|issue=August|pages=27–33|date=2005|issn=1524-833X}}
*{{cite journal|last=Van der Merwe|first=Anton|title=Chapter Zero in Perspective|journal=Management Accounting Quarterly|volume=5|issue=Winter, number 2 |pages=1–6|issn=1092-8057|date=2004}}
*{{cite journal|last=Wagenhofer|first=Alfred|title=Management Accounting Research in German-speaking Countries|journal=Journal of Management Accounting Research|volume=18|issue= 1|pages=1–19|date=2006|issn=1049-2127|doi=10.2308/jmar.2006.18.1.1}}

==External links==
*{{cite web|url=http://www.imanet.org|title=Institute of Management Accounting (IMA) - Publisher of ''Management Accounting Quarterly'' and ''Strategic Finance''}}
*{{cite web|url=http://www.rcainstitute.org|title=RCA Institute Official Web Site}}
*{{cite web|url=http://www.plaut.com|title=Plaut - the consulting company still dedicated to the tradition of its founder Hans-Georg Plaut}}

[[Category:Management accounting]]