{{Infobox journal
| title = Clinical Chemistry and Laboratory Medicine
| cover = [[File:Clinical Chemistry and Laboratory Medicine cover.jpg]]
| editor = Mario Plebani
| discipline = [[Clinical chemistry]]
| former_names = Klinische Chemie, European Journal of Clinical Chemistry and Clinical Biochemistry
| abbreviation = Clin. Chem. Lab. Med.
| publisher = [[Walter de Gruyter]]
| country = 
| frequency = Monthly
| history = 1963-present
| openaccess = 
| license = 
| impact = 3.017
| impact-year = 2015
| website = http://www.degruyter.com/view/j/cclm
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 475036853
| LCCN = 
| CODEN = CCLMFW
| ISSN =  1434-6621
| eISSN = 1437-4331
}}
'''''Clinical Chemistry and Laboratory Medicine''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] that is published by [[De Gruyter publishers]].

== History ==
The journal was established in 1963 as ''Clinical chemistry''/''Klinische Chemie''. In 1991 it was renamed to ''European Journal of Clinical Chemistry and Clinical Biochemistry''. In 1998 it obtained its present name.{{sfn|Plebani|2012|p=1-2}}

== Scope ==
The journal covers developments in fundamental and applied research into science related to clinical laboratories. It covers areas such as [[clinical biochemistry]], [[molecular medicine]], [[hematology]], [[immunology]], [[microbiology]], [[virology]], drug measurement, [[genetic epidemiology]], evaluation of diagnostic markers, new reagents and systems, reference materials, and reference values. It also publishes recommendations and news from the International Federation of Clinical Chemistry and Laboratory Medicine and the [[European Federation of Clinical Chemistry and Laboratory Medicine]].{{sfn|Clinical Chemistry and Laboratory Medicine - Ovid}}

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* CAB Abstracts
* [[Global Health]]
* [[Academic Search]]
* [[TOC Premier]]
* [[Elsevier BIOBASE]]
* [[EMBASE]]
* [[Scopus]]
* [[MEDLINE]]/[[PubMed]]
* [[ProQuest|ProQuest databases]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[Current Contents]]/Life Sciences
* [[Science Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.017.{{sfn|Journal Citation Reports}}

== Associated organizations ==
''Clinical Chemistry and Laboratory Medicine'' is the official journal of the European Federation of Clinical Chemistry and Laboratory Medicine (EFLM). It is also the official journal of the [[Association of Clinical Biochemists in Ireland]], the [[Belgian Society of Clinical Chemistry]], the [[German United Society of Clinical Chemistry and Laboratory Medicine]], the [[Greek Society of Clinical Chemistry-Clinical Biochemistry]], the [[Italian Society of Clinical Biochemistry and Clinical Molecular Biology]], the [[Slovenian Association for Clinical Chemistry]], and the [[Spanish Society for Clinical Biochemistry and Molecular Pathology]].{{sfn|EFLM Journal/CCLM}}

== References ==

=== Citations ===
{{reflist|colwidth=30em}}

=== Sources ===
{{refbegin}}
*{{cite web |ref={{harvid|Clinical Chemistry and Laboratory Medicine - Ovid}} |url=http://www.ovid.com/webapp/wcs/stores/servlet/ProductDisplay?storeId=13051&catalogId=13151&langId=-1&partNumber=Prod-1268
 |title=Clinical Chemistry and Laboratory Medicine |publisher=Ovid |accessdate=2012-12-15}}
*{{cite web |ref={{harvid|EFLM Journal/CCLM}} |url=http://efcclm.eu/publications-and-resources/efcc-journal-cclm
 |title=EFLM Journal/CCLM |publisher=European Federation of Clinical Chemistry and Laboratory Medicine |accessdate=2012-12-15}}
*{{cite book |year=2013 |chapter=Clinical Chemistry and Laboratory Medicine |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}
*{{cite journal |ref=harv |url=http://www.degruyter.com/view/j/cclm.2013.51.issue-1/cclm-2012-0333/cclm-2012-0333.xml?format=INT
 |last=Plebani |first=Mario|title=Preface: Happy 50th anniversary!
 |journal=Clinical Chemistry and Laboratory Medicine |volume=51 |issue=1 |ISSN=1437-4331 |doi=10.1515/cclm-2012-0333 |date=December 2012}}
{{refend}}

== External links ==
* {{Official website|http://www.degruyter.com/view/j/cclm}}


[[Category:Medicinal chemistry journals]]
[[Category:Publications established in 1963]]
[[Category:Walter de Gruyter academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]