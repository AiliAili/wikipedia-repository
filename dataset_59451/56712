{{Multiple issues|
{{notability|date=November 2016}}
{{third-party|date=November 2016}}
}}

{{Infobox journal
| title = Electronic Journal of Sociology
| cover =
| discipline = [[Sociology]]
| editor = Michael Sosteric, Adam Rafalovich
| abbreviation = Electr. J. Sociol.
| country = 
| history = 1994–2008
| website = http://www.sociology.org/ejs-archives/
| ISSN = 
| OCLC = 34297444
}}
The '''''Electronic Journal of Sociology''''' was an online [[open access]] [[academic journal]] of [[sociology]]. It was established in 1994 by Michael Sosteric.<ref>Schneider, Andreas. (2001). [http://www.asanet.org/footnotes/septoct01/fn21.html The Electronic Journal of Sociology: Seven Years of Electronic Publishing]. ''ASA Footnotes''.</ref> Andreas Scheider was [[editor-in-chief]] until 2004, when Sosteric returned and Adam Rafalovich became co-editor-in-chief.<ref>Sosteric M. (2004). Science, Scholarly Communication, and Access: Open is Better. ''Electronic Journal of Sociology''.</ref> The journal implemented a new approach in 2004 whereby it decided to publish three tiers of articles: fully reviewed original articles at tier 1, editorially-reviewed reviews and commentaries at tier 2, and editorially-reviewed other works, including original papers, at tier 3. Although the journal "achieved international recognition", as an electronic journal it faced barriers.<ref>Sosteric, Mike. (1999b). [http://www.arl.org/bm~doc/sosteric.pdf At the Speed of Thought: Pursuing Non-Commercial Alternatives to Scholarly Communication]. Association of Research Libraries Newsletter.</ref>

In 1996, the journal was criticized by its rival, ''[[Sociological Research Online]]'', which suggested that its conventional approach to reviewing articles was more rigorous. The ''Electronic Journal of Sociology'' had adopted a forum with which an author could respond to the feedback or criticism of the reviewer. Its [[editorial board]] responded, speculating that the rival journal, which was affiliated with [[SAGE Publishing]], might feel threatened by their open access and lack of traditional publishing ties.<ref>EJS Collective. (1996). To Sociological Research Online.</ref> In 2008, publication was suspended.

The journal was indexed in [[SocINDEX]].<ref>{{cite web |title=Database Coverage List |url=http://www.ebscohost.com/titleLists/snh-coverage.htm |work=SocINDEX |publisher=[[EBSCO Information Services]] |accessdate=13 March 2013}}</ref>

==References==
{{reflist}}

[[Category:Sociology journals]]
[[Category:Open access journals]]
[[Category:Publications established in 1994]]
[[Category:Publications disestablished in 2008]]
[[Category:English-language journals]]