{{Use dmy dates|date=June 2011}}
[[Image:Underwater-microphone hg.jpg|thumb|upright=0.6|A hydrophone]]

{{distinguish2|[[hydraulophone]], a musical instrument}}

A '''hydrophone''' ([[Ancient Greek]] ὕδωρ = water<ref name="Liddell & Scott">Liddell, H.G. & Scott, R. (1940). ''A Greek-English Lexicon. revised and augmented throughout by Sir Henry Stuart Jones. with the assistance of. Roderick McKenzie.'' Oxford: Clarendon Press.</ref> and φωνή = sound<ref name="Liddell & Scott"/>) is a [[microphone]] designed to be used underwater for recording or listening to [[underwater sound]].  Most hydrophones are based on a [[piezoelectric]] [[transducer]] that generates electricity when subjected to a pressure change.  Such piezoelectric materials, or [[transducer]]s, can convert a sound signal into an electrical signal since [[sound]] is a pressure wave. Some transducers can also serve as a projector, but not all have this capability, and some may be destroyed if used in such a manner.

A hydrophone can "listen" to sound in air but will be less sensitive due to its design as having a good [[acoustic impedance]] match to water, which is a denser fluid than air.  Likewise, a microphone can be buried in the ground, or immersed in water if it is put in a waterproof container, but will give similarly poor performance due to the similarly bad acoustic impedance match.

==History==
[[File:Hydrophone being lowered into the Atlantic.jpg|thumb|A hydrophone being lowered into the North Atlantic]]
The earliest widely used design was the [[Fessenden oscillator]], an electrodynamically driven clamped-edge circular plate transducer (not actually an oscillator) operating at 500, 1000, and later 3000 Hz. It was originally marketed as an underwater telegraph, rather than as [[sonar]], but was later very successful, its Canadian inventor, Reginald Fessenden, was awarded the "Scientific American Magazine Gold Medal of Safety" in 1929 from the [[American Museum of Safety]], an organization for ship captains;<ref name="Frost, 2001" >{{cite journal 
  |title=Inventing Schemes and Strategies: The Making and Selling of the Fessenden Oscillator
  |publisher=[http://muse.jhu.edu/ Project MUSE]
  |journal=Technology and Culture
  |volume=42  |issue=3
  |date=July 2001 
  |last= Frost  |first=Gary Lewis 
  |url=http://muse.jhu.edu/journals/tech/summary/v042/42.3frost01.html 
  |ref=harv
  |pages=462–488
  |doi=10.1353/tech.2001.0109
}}</ref> some were still in use during [[World War II]].

[[Ernest Rutherford]], in England, led pioneer research in hydrophones using piezoelectric devices, and his only patent was for a hydrophone device. The [[acoustic impedance]] of piezoelectric materials facilitated their use as underwater transducers.  The piezoelectric hydrophone was used late in [[World War I]], by [[convoy]] escorts detecting [[U-boat]]s, greatly impacting the effectiveness of submarines.{{Citation needed|date=June 2011}}

From late in World War I until the introduction of active [[sonar]], hydrophones were the sole method for submarines to detect targets while submerged, and remain useful today.

==Directional hydrophones==
A small single cylindrical ceramic [[transducer]] can achieve near perfect omnidirectional reception.  Directional hydrophones increase sensitivity from one direction using two basic techniques:

===Focused transducers===
This device uses a single [[transducer]] element with a dish or conical-shaped sound reflector to focus the signals, in a similar manner to a reflecting telescope.  This type of hydrophone can be produced from a low-cost omnidirectional type, but must be used while stationary, as the reflector impedes its movement through water. A new way to direct is to use a spherical body around the hydrophone. The advantage of directivity spheres is that the hydrophone can be moved within the water, ridding it of the interferences produced by a conical-shaped element

===Arrays===
Multiple hydrophones can be arranged in an [[microphone array|array]] so that it will add the signals from the desired direction while subtracting signals from other directions.  The array may be steered using a [[beamforming|beamformer]].  Most commonly, hydrophones are arranged in a "line array"{{Citation needed|date=October 2007}} but may be in two- or three-dimensional arrangements.

[[SOSUS]] hydrophones, laid on the seabed and connected by underwater cables, were used, beginning in the 1950s, by the [[U.S. Navy]] to track movement of [[Soviet Union|Soviet]] submarines during the [[Cold War]] along a line from [[Greenland]], [[Iceland]] and the [[United Kingdom]] known as the [[GIUK gap]].<ref>Mackay, D.G. "[http://theses.gla.ac.uk/347/01/2008mackayMPhil.pdf Scotland the Brave? US Strategic Policy in Scotland 1953-1974]". ''Glasgow University, Masters Thesis (research).'' 2008. Accessed 12 October 2009.</ref> These are capable of clearly recording extremely low frequency [[infrasound]], including many [[List of unexplained sounds|unexplained ocean sounds]].

==See also==
*[[Communication with submarines]]
*[[Underwater acoustics]]
*[[Sonar]]
*[[Reflection seismology]]

==Notes==
{{Reflist}}

==References==
* Pike, John (1999). [[SOSUS]]. Retrieved January 28, 2005.
* Watlington, Frank (1979). ''How to build & use low-cost hydrophones.'' (ISBN 0830610790)
* Unknown. [http://www.ob-ultrasound.net/hydrophone.html hydrophone]. Retrieved January 28, 2005.
* Unknown. (2005) [http://www.glossary.oilfield.slb.com/Display.cfm?Term=hydrophone Schlumberger Oilfield Glossary: Term 'hydrophone']. Retrieved January 28, 2005.
* Onda Corporation (2015).  '[http://ondacorp.com/Handbook/ Hydrophone Handbook]'.

== External links ==
{{Commons category|Hydrophones|Hydrophone}}
* [http://dosits.org/science/measurng/1.htm DOSITS]—Hydrophone introduction at Discovery of Sound in the Sea
* [http://orcasound.net?07enohpordyhikiw10 orcasound.net]—Live hydrophone streams from killer whale habitat
* [http://pamguard.org Passive Acoustic Monitoring]—Using hydrophones to monitor underwater sounds
* [http://sonar-fs.lboro.ac.uk/uag/products/products_hydrophone.html Build your own hydrophone]—free instructions
* [http://acoustics.co.uk/  Precision Acoustics]—useful resource on hydrophones
* [http://www.bl.uk/soundarchive The British Library Sound Archive]—contains many wildlife and atmospheric recordings made using hydrophones.
* [http://www.teledyne-reson.com/products/hydrophones/ High Quality Hydrophones]— High quality manufacturer of Hydrophones. 
{{hydroacoustics}}
{{Sensors}}

[[Category:Sonar]]