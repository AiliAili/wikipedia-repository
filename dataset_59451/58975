{{Infobox journal
| title = Molecular Informatics
| formernames = Quantitative Structure-Activity Relationships, QSAR & Combinatorial Science
| cover = 
| discipline = [[Cheminformatics]], [[quantitative structure–activity relationship]]s, [[combinatorial chemistry]]
| abbreviation = Mol. Inform.
| editor = Knut Baumann, Gerhard Ecker, Jordi Mestres, Gisbert Schneider
| publisher = [[Wiley VCH]]
| frequency = Monthly
| history = 1981-present
| impact = 2.338
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1868-1751
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1868-1751/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1868-1751/issues
| link2-name = Online archive
| ISSN = 1868-1743
| eISSN = 1868-1751
| ISSN2 = 0931-8771
| ISSN2label = ''Quantitative Structure-Activity Relationships'':
| ISSN3 = 1611-020X
| ISSN3label = ''QSAR & Combinatorial Science'':
| OCLC = 605923838
| LCCN = 2010200090
| CODEN = MIONBS
}}
'''''Molecular Informatics''''' is a [[peer-reviewed]] [[scientific journal]] published by [[Wiley VCH]]. It covers research in [[cheminformatics]], [[quantitative structure–activity relationship]]s, and [[combinatorial chemistry]]. It was established in 1981 as ''Quantitative Structure-Activity Relationships'' and renamed to ''QSAR & Combinatorial Science'' in 2003, before obtaining its present name in 2010. According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.338.<ref name=WoS>{{cite book |year=2013 |chapter=Molecular Informatics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References== 
{{reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1868-1751}}
* [http://www.qsar.org/ The QSAR and Modelling Society]
* [http://www.combichem.org/ Society of Combinatorial Sciences]
* [http://www.ccl.net/ Computational Chemistry List]

[[Category:Chemistry journals]]
[[Category:Computer science journals]]
[[Category:Cheminformatics]]
[[Category:Monthly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]


{{chem-journal-stub}}
{{compu-journal-stub}}