{{Infobox boxer
| name = Susianna Kentikian
| image = Kentikian.png
| caption = Kentikian in 2008
| realname = {{nowrap|Susianna Levonovna Kentikian}}<ref name="Hymne">[http://www.abendblatt.de/daten/2008/08/26/927200.html Endlich passt die Hymne]. ''Hamburger Abendblatt''. August 26, 2008. Accessed August 26, 2008. {{de icon}}</ref>
| nickname = Killer Queen<ref name="Durchgeboxt"/>
| nationality = {{plainlist|
*Armenian
*German
}}
| weight = [[Flyweight]]
| height = 1.55 m
| reach = 
| birth_date = {{birth date and age|1987|9|11|df=y}}
| birth_place = [[Yerevan]], [[Armenian SSR]], [[Soviet Union]] (now Armenia)
| style = [[Orthodox stance|Orthodox]]
| total = 39
| wins = 36
| KO = 17
| losses = 2
| draws = 
| no contests = 1
}}

'''Susianna''' "'''Susi'''" '''Levonovna Kentikian''' ({{lang-hy|Սյուզի Կենտիկյան}}; born '''Syuzanna Kentikyan''' on 11 September 1987) is a [[Armenians in Germany|German-Armenian]] [[professional boxer]] who resides in Germany.<ref>{{boxrec|292211}}. Retrieved 4 October 2016.</ref> She was born in [[Yerevan]], [[Armenian SSR]], but left the country with her family at the age of five because of the [[Nagorno-Karabakh War]]. Kentikian has lived in [[Hamburg]] since 1996 and began boxing at the age of twelve. Following a successful [[amateur boxing|amateur]] career, she turned professional in 2005 upon signing with the Hamburg-based Spotlight Boxing promotion.

Kentikian is a two-time [[flyweight]] world champion, having held the [[World Boxing Association|WBA]] female title since 2013. Previously she held the same title from 2007 to 2012, and the [[World Boxing Organization|WBO]] title from 2009 to 2012. Additionally, she has held the [[Women's International Boxing Federation|WIBF]] flyweight title since 2015, having previously held it from 2007 to 2012.

Until 2012, Kentikian remained undefeated as a professional, winning 16 of her first 30 fights by [[knockout]] or [[technical knockout|stoppage]]. The German television station [[ZDF]] has broadcast her fights since July 2009. She had previously headlined fight cards for the television station [[ProSieben]] from 2007 to 2009. Kentikian has gained minor celebrity status in Germany and she hopes to reach a popularity similar to that of the retired German female boxing star [[Regina Halmich]].

==Early life==
Susianna Kentikian was born in [[Yerevan]], [[Armenian Soviet Socialist Republic|Armenian SSR]], the daughter of veterinary doctor Levon Kentikian and his wife Makruhi.<ref name="Wm-Titel">Jensen, Björn. [http://www.abendblatt.de/daten/2007/02/19/691561.html?s=1 "Killer Queen" krönt sich mit WM-Titel]. ''Hamburger Abendblatt''. February 19, 2007. Accessed May 26, 2008. {{de icon}}</ref> At the age of five, she left Armenia with her parents and her nine-year-old brother, Mikael, because her father was called up to serve in the military during the [[Nagorno-Karabakh War]].<ref name="Ich bin ein Killer, ich kämpfe wie ein Mann">Bösecke, Ina. [http://www.spiegel.de/sport/sonst/0,1518,466274,00.html "Ich bin ein Killer, ich kämpfe wie ein Mann"]. ''Spiegel Online''. February 16, 2007. Accessed May 1, 2007. {{de icon}}</ref> In 1992, the family first moved to Berlin, Germany and stayed at [[asylum seeker]]s' homes. However, due to the violence at these facilities and their poor knowledge of the German language, they left Berlin and moved to [[Moldova]] and later to Russia, where Kentikian went to school for a short period of time.<ref name="Ein Fliegengewicht boxt sich nach oben">Treptow, Julia. [http://www.spiegel.de/schulspiegel/leben/0,1518,394773,00.html Ein Fliegengewicht boxt sich nach oben]. ''Spiegel Online''. January 18, 2006. Accessed May 1, 2007. {{de icon}}</ref> The family returned to Germany in 1996 and relocated in [[Hamburg]], again living in government facilities for asylum seekers.<ref name="Durchgeboxt">Krohn, Anne-Dore. [http://www.zeit.de/2007/08/Boxen-08 Durchgeboxt]. ''Die Zeit''. February 15, 2007. Accessed May 1, 2007. {{de icon}}</ref> Kentikian's residence status remained uncertain for almost a decade. Several times, she and her family were taken to the airport for deportation, but the intervention of local friends such as her amateur trainer, Frank Rieth, who called lawyers, the media and local politicians, prevented their final expulsion.<ref name="Die starke Susi"/> Her family received a permanent residence permit in 2005 when she signed a three-year professional boxing contract that established a stable income.<ref name="Susi Kentikian – Hamburgs Million Dollar Baby">Hardt, Andreas. [https://www.welt.de/print-welt/article562625/Susi_Kentikian_-_Hamburgs_Million_Dollar_Baby.html Susi Kentikian - Hamburgs "Million Dollar Baby"]. ''Die Welt''. March 31, 2005. Accessed May 1, 2007.</ref>

At the age of 16, Kentikian began working as a cleaner in a local [[fitness center]] to help her family financially.<ref name="Ein Fliegengewicht boxt sich nach oben"/> She graduated from high school (''[[Realschule]]'') in the summer of 2006<ref name="Ich wollte schon als Siebenjährige Großes schaffen">[http://www.abendblatt.de/daten/2007/04/07/720463.html?prx=1 "Ich wollte schon als Siebenjährige Großes schaffen"]. ''Hamburger Abendblatt''. April 7, 2007. Accessed May 1, 2007. {{de icon}}</ref> and she eventually became a [[German citizen]] in June 2008.<ref>[http://www.abendblatt.de/daten/2008/06/16/894324.html Hamburg]. ''Hamburger Abendblatt''. June 16, 2008.. Accessed June 26, 2008. {{de icon}}</ref> She has applied for dispensation to be allowed to retain her Armenian citizenship.<ref name="Hymne"/> Kentikian now lives with her family in an apartment near her Hamburg boxing gym.<ref name="Durchgeboxt"/>

==Amateur career==
Kentikian discovered her enthusiasm for boxing when she was twelve years old after accompanying her brother to his [[boxing training]].<ref name="Durchgeboxt"/> She started with regular training herself and stated that boxing had allowed her to forget the difficulties of her life for a short time: "I could let everything out, my whole energy. If you have so many problems like our family, you need something like that."<ref name="Ich bin ein Killer, ich kämpfe wie ein Mann"/>

Kentikian won the Hamburg Championships for juniors from 2001 to 2004. She also won the Northern German Championships for juniors in 2003 and 2004, and in October 2004, she had her biggest amateur success by winning the International German Women's Amateur Championships in the [[featherweight]] division for juniors. Kentikian found it increasingly difficult to find opponents in the amateur ranks, as few boxers wanted to face her in the ring, and her status as an asylum seeker did not allow her to box outside Hamburg.<ref>[http://www.taz.de/dx/2003/01/02/a0166.1/text Susi Kentikian]. ''taz Hamburg''. January 2, 2003. Accessed June 21, 2008. {{de icon}}</ref> Kentikian's final amateur record stood at 24 wins and one loss. She later blamed overeagerness for her single loss, having fought despite health problems at the time.<ref>Kötter, Andreas. [http://www.prosieben.de/sport/boxen/artikel/49309/ ProSieben Fight Night - Interview mit Susi Kentikian]. ProSieben.de. Accessed April 13, 2008. {{de icon}} {{webarchive |url=https://web.archive.org/web/20080417113907/http://www.prosieben.de/sport/boxen/artikel/49309/ |date=April 17, 2008 }}</ref> Her aggressive style and fast [[Striking combination|combinations]], and her ambition to always attack until she knocked out the opponent earned her the nickname "Killer Queen"; she has often used the [[Killer Queen|identically-named song]] by the English rock band [[Queen (band)|Queen]] as her entrance music.<ref name="Durchgeboxt"/>

===Highlights===
*[[Hamburg]] Champion for juniors – 2001 to 2004<ref name="Portrait">[http://www.kentikian.de/de/portrait/portrait.php Susianna Kentikian | Portrait]. Kentikian.de. Accessed April 17, 2008. {{de icon}}</ref>
*[[Northern Germany|Northern German]] Champion for juniors – 2003 and 2004<ref name="Portrait"/>
*International German Amateur [[featherweight]] Champion for juniors – 2004<ref name="Portrait"/>

==Professional career==
Kentikian was discovered as a professional boxer at an exhibition fight during qualifications for the [[World Amateur Boxing Championships]]. At the beginning of 2005, she signed a three-year contract with the Hamburg boxing promoter Spotlight Boxing, a joint venture of Universum Box-Promotion, focusing on young athletes.<ref name="Susi Kentikian – Hamburgs Million Dollar Baby"/> Since then, she has been coached by Universum trainer Magomed Schaburow.<ref name="Wm-Titel"/> Kentikian started her professional career on January 15, 2005, with a win by [[unanimous decision]] over Iliana Boneva of [[Bulgaria]] on the [[undercard]] of German female boxing star [[Regina Halmich]]. Over the next 14&nbsp;months, Kentikian won nine of her eleven fights by knockout. Her unusually high knockout percentage, rarely seen in lower female weight classes, began to draw attention.<ref name="Die starke Susi"/> On July 25, 2006, she won her first belt, the International German flyweight title, against Daniela Graf by unanimous decision. In her first international title fight on September 9, 2006, Kentikian beat [[Maribel Zurita]] from the United States with a fourth round [[technical knockout]] for the [[Women's International Boxing Federation|WIBF]] InterContinental [[Flyweight]] title; the fight was stopped when Zurita was cut over the left eyebrow.<ref name="Die starke Susi"/>

===2007===
In her 15th professional bout, Kentikian fought for her first world championship in [[Cologne]], Germany, on February 16, 2007; it was also her first time headlining a [[fight card]].<ref name="Die starke Susi">Monheim, Gert. [http://www.wdr.de/unternehmen/presselounge/programmhinweise/fernsehen/2007/06/20070627_diestarkesusi.phtml Die starke Susi - Boxen zum Überleben]. Das Erste. June 27, 2007. Accessed April 16, 2008. {{de icon}}</ref> She won by a ninth round technical knockout against Carolina Alvarez of [[Venezuela]], thereby winning the vacant [[World Boxing Association|WBA]] Flyweight title. Alvarez took unanswered punches in most of the rounds and was bleeding heavily from her nose and the referee eventually stopped the fight in round nine in concern for Alvarez's health.<ref>Drexel, Fritz. [http://www.eastsideboxing.com/news.php?p=9932&more=1 Kentikian Stops Alvarez]. East Side Boxing. February 17, 2007. Accessed June 21, 2008.</ref> Six weeks later, on March 30, 2007, Kentikian made her first title defense. Before a crowd of 19,500 in the [[Kölnarena]], she fought on the undercard of the popular exhibition bout between German comedian [[Stefan Raab]] and WIBF World Champion Regina Halmich. Kentikian beat María José Núñez from [[Uruguay]] with a third round technical knockout. Núñez was knocked down in round two and Kentikian finished the fight one round later with a right [[Cross (boxing)|cross]] followed up by combinations that left Núñez defenseless on the ropes, causing the referee to step in.<ref>Drexel, Fritz. [http://www.eastsideboxing.com/news.php?p=10422&more=1 Kentikian Destroys Anchorena]. East Side Boxing. March 30, 2007. Accessed June 21, 2008.</ref>
[[Image:Kentikian-hokmi.png|thumb|Kentikian (right) in her rematch with Nadia Hokmi, December 2007]]

Kentikian next faced Nadia Hokmi of France in her second title defense on May 25, 2007. Hokmi, using her height and reach advantage, proved to be the first test of Kentikian's professional career and both boxers fought a competitive bout. While Hokmi started out slower, she managed to win several of the later rounds by landing repeated combinations. Kentikian won through a [[split decision]] for the first time in her career.<ref>Drexel, Fritz. [http://www.eastsideboxing.com/news.php?p=11095&more=1 Kentikian struggles to split decision victory over Hockmi]. East Side Boxing. May 25, 2007. Accessed April 13, 2008.</ref> The fight was voted among the five "Top Fights of the Year" by WomenBoxing.com.<ref>[http://www.womenboxing.com/awards2007.htm Womens Boxing - WBAN Awards 2007]. WomenBoxing.com. Accessed March 18, 2008.</ref> On September 7, 2007, Kentikian defended her title against Shanee Martin from the United Kingdom, winning by a third round technical knockout. Kentikian controlled her opponent from the opening bell and the referee stopped the fight after Martin was knocked down from a straight right hand in round three.<ref>Drexel, Fritz. [http://www.eastsideboxing.com/news.php?p=12328&more=1 Kentikian Destroys Martin!]. East Side Boxing. September 7, 2007. Accessed April 13, 2008.</ref>

Following the retirement of long-standing WIBF belt holder Regina Halmich, Kentikian unified the WBA and vacant WIBF Flyweight titles in her hometown of Hamburg on December 7, 2007. She met Nadia Hokmi in a rematch of their contest six months earlier. The French boxer again proved to be a tough opponent and the fight developed very similarly to their first encounter. Once more, Kentikian had the better start, but Hokmi scored during the second half of the fight, again making it a close bout. This time however, Kentikian was ahead on all three of the judges' scorecards, winning by unanimous decision.<ref>[http://www.eastsideboxing.com/news.php?p=13591&more=1 Susi Kentikian wins Halmich's belt]. East Side Boxing. December 8, 2007. Accessed June 21, 2008.</ref>

===2008===
Kentikian successfully defended her titles against Sarah Goodson of the [[Philippines]] by a third round technical knockout on February 29, 2008. Goodson, who had fought almost exclusively in lower weight classes before, was overpowered by Kentikian and the referee ended the fight after a series of body punches in round three.<ref>Dower, Jim. [http://www.boxingnews24.com/2008/03/kentikian-destroys-goodson/ Kentikian Destroys Goodson]. Boxing News 24. March 1, 2008. Accessed June 21, 2008.</ref> In her next title defense on May 10, 2008, Kentikian beat Mary Ortega from the United States with a first round technical knockout. Ortega, who had previously fought against well-known opponents such as [[Elena Reid]] and [[Hollie Dunaway]], was knocked down twice by straight right hands during the first 90&nbsp;seconds of the fight. When Kentikian had Ortega pinned against the ropes again, the referee stepped in shortly before the end of round one. The quick stoppage came as a surprise to many, including television commentator Regina Halmich, who had expected a hard-fought bout.<ref>Schmidt, Erik. [http://www.boxingnews24.com/2008/05/kentikian-destroys-ortega/ Kentikian Destroys Ortega]. Boxing News 24. May 11, 2008. Accessed June 21, 2008.</ref>

In her next title defense on August 29, 2008, Kentikian met Hager Finer of [[Israel]], Halmich's last opponent before retiring. Following a close opening round, the boxing match turned into a brawl and Finer scored during the first half of the fight. From round five onwards, Kentikian managed to take over the bout by landing the cleaner punches and she won by unanimous decision.<ref>Swiecznik, Sebastian. [http://www.eastsideboxing.com/news.php?p=17061&more=1 Zbik schools Carvalho, Kentikian beats Shmoulefeld Finer!]. East Side Boxing. August 29, 2008. Accessed August 30, 2008.</ref> On December 5, 2008, Kentikian faced Anastasia Toktaulova of Russia, the reigning GBU Flyweight Champion, although the GBU title was not on the line. During the uncharacteristic tactical fight, Kentikian managed to control her opponent from the middle of the ring in most of the rounds. The three judges all scored the bout in favor of Kentikian.<ref>Schmidt, Erik. [http://www.boxingnews24.com/2008/12/kentikian-defeats-toktaulova/ Kentikian Defeats Toktaulova]. Boxing News 24. December 6, 2008. Accessed January 5, 2009.</ref> In December 2008, she was named Germany's female boxer of the year for the first time.<ref name="Focus">[http://www.focus.de/sport/boxen/boxen-koenig-arthur-sticht-klitschkos-aus_aid_357082.html "König Arthur" sticht Klitschkos aus]. FOCUS Online. 18. Dezember 2008. Accessed January 5, 2009. {{de icon}}</ref>

===2009===
Kentikian retained her WIBF and WBA belts with a unanimous decision win over [[Elena Reid]] from the United States on March 20, 2009. Reid, who was well known in Germany after two controversial bouts with Halmich in 2004 and 2005, remained largely passive from the opening bell and Kentikian controlled her through the majority of the fight. Reid did not win a single round on the official scorecards.<ref>Persson, Ake. [http://www.eastsideboxing.com/news.php?p=19150&more=1 Kentikian Dominates Reid]. East Side Boxing. March 21, 2009. Accessed May 31, 2009.</ref> On July 4, 2009, Kentikian fought the [[Interim championship|Interim]] WBA [[Super Flyweight]] Champion Carolina Gutierrez Gaite of [[Argentina]]. Kentikian used her speed and combinations to dominate her opponent through the ten rounds, winning every round on the judges' scores.<ref>Schmidt, Erik. [http://www.boxingnews24.com/2009/07/chambers-upsets-dimitrenko-alekseev-kentikian-and-graf-also-victorious/ Chambers Upsets Dimitrenko, Alekseev, Kentikian and Graf Also Victorious]. Boxing News 24. July 4, 2009. Accessed July 5, 2009.</ref> Kentikian ended 2009 by fighting the undefeated Turkish-German [[Julia Sahin]] (20–0) on 10 October for the vacant [[World Boxing Organization|WBO]] Female Flyweight title. Kentikian overwhelmed Sahin with her higher work rate early on. Sahin spent most of the fight covering up from Kentikian's many flurries. Kentikian gave Sahin a ten-round beating and won a unanimous decision to become the new WBO Female Flyweight Champion.<ref>[http://www.boxingnews24.com/2009/10/kentikian-defeats-sahin/ Kentikian defeats Sahin]</ref>

===2010===
Kentikian, now the WIBF, WBA and WBO Female Flyweight Champion, made the first defense of all her titles against Nadia Raoui on 24 April 2010. The bout was very close, with Kentikian landing more cleaner and harder shots, yet Raoui finding much success as well. After ten close rounds, Kentikian won via split decision.

On 17 July, Susi Kentikian defended all of her titles once again against Mexico's Arely Mucino. In a disappointing turn of events, the bout was stopped in the third round after an accidental clash of heads left Kentikian with an injury, leaving her unable to continue. The fight was declared a no contest. Up until the stoppage, Kentikian was giving Mucino problems with her fast combinations, in and out attacks and power shots.<ref>Schmidt, Erik. [http://www.boxingnews24.com/2010/07/kentikian-and-mucino-ends-in-technical-draw/ Kentikian and Mucino bout ends in technical draw]</ref>

===2011===
Kentikian retained her WIBF Flyweight title against Ana Arrazola on 26 March 2011. It was an action packed ten round fight, with Susi winning almost every round on her way to a unanimous decision victory. Arrazola was docked a point in round six for a low blow.<ref>[http://www.hyefighters.com/2011/03/26/hyefighter-kentikian-wins/ Hyefighter Kentikian Wins]</ref> Kentikian finished the year by scoring another unanimous decision win, this time against Thai Teeraporn Pannimit, to retain her WIBF, WBA and WBO Female Flyweight titles. Kentikian won every round on every scorecard (100-90, 100-90, 100-90).<ref>[http://boxrec.com/media/index.php/Susi_Kentikian_vs._Teeraporn_Pannimit Susi Kentikian vs. Teeraporn Pannimit]</ref>

===2012===
Kentikian attempted to defend her WIBF and WBO Female Flyweight titles next on 16 May 2012 in [[Frankfurt, Germany]] against Melissa McMorrow.<ref>Persson, Ake. [http://www.boxingscene.com/susi-kentikian-defends-her-titles-on-16-frankfurt--51849 Susi Kentikian Defends Her Titles on May 16 in Frankfurt]</ref> Kentikian lost via controversial majority decision with scores of 95-95, 94-96 and 94-96. McMorrow had won the early rounds, but Kentikian dominated the remainder of the fight.<ref>[http://www.hyefighters.com/2012/05/16/hyefighter-kentikian-loses-in-a-majority-decision/ HyeFighter Kentikian Loses In A Majority Decision]. HyeFighters. May 16, 2012. Accessed May 17, 2012.</ref> Months later, Kentikian defend WBA Female title against Carina Moreno, but lost a close split decision.

===2013===
On February 1, 2013, Kentikian defeated Sanae Jah and won the [[Interim championship|Interim]] [[World Boxing Association|WBA]] Female [[Flyweight]] title.
On June 7, 2013, Kentikian defeated Carina Moreno and won [[World Boxing Association|WBA]] Female [[Flyweight]] title.
On December 7, 2013, Kentikian defeated Simona Galassi and defended the [[World Boxing Association|WBA]] Female [[Flyweight]] title.

===2014===
On May 31, 2014, Kentikian defeated Dan-Bi Kim and defended the [[World Boxing Association|WBA]] Female [[Flyweight]] title.
On November 8, 2014, Kentikian defeated Naoko Fujioka (Japan) and again defended the [[World Boxing Association|WBA]] Female [[Flyweight]] title.

==In the media==
At the beginning of her professional career, Kentikian was primarily featured in the local media in Hamburg and occasionally in national [[German newspapers]]; in particular, her difficult childhood and her long-time uncertain asylum status sparked interest in the press and led to comparisons with the boxing film ''[[Million Dollar Baby]]''.<ref name="Durchgeboxt"/> Her height of {{height|m=1.54|precision=0}} also drew attention, and she was dubbed "Germany's smallest professional boxer".<ref name=" Die kleinste Profiboxerin Deutschlands">Jensen, Björn. [http://www.abendblatt.de/daten/2005/01/10/385270.html?prx=1 Die kleinste Profiboxerin Deutschlands]. ''Hamburger Abendblatt''. January 10, 2005. Accessed May 1, 2007. {{de icon}}</ref> Early on, Kentikian was considered one of the big talents in German boxing<ref name="Ich bin ein Killer, ich kämpfe wie ein Mann"/> and the media mentioned her as the potential successor of record world champion [[Regina Halmich]], a goal she had also set out for herself.<ref name="Ein Fliegengewicht boxt sich nach oben"/>

In 2007, Kentikian was introduced to a much larger audience due to cooperation between German television station [[ProSieben]] and her promoter Spotlight Boxing.<ref name="Ich wollte schon als Siebenjährige Großes schaffen"/> In addition to live broadcasts of her fights during so-called "ProSieben Fight Nights,"<ref>[http://www.presseportal.de/pm/25171/1013565/prosieben_television_gmbh ProSieben setzt Kooperation mit Profiboxstall spotlight boxing fort]. ProSieben Television GmbH.  July 7, 2007. Accessed April 16, 2008. {{de icon}}</ref> she appeared several times on the popular television show [[TV total]]. She also took part in a four-round [[sparring]] session with the show's host, [[Stefan Raab]],<ref>[http://tvtotal.prosieben.de/show/letzte_sendung/2007/03/29/10090.html TV total - 29.03.2007]. ProSieben.de. Accessed April 16, 2008. {{de icon}}  {{webarchive |url=https://web.archive.org/web/20081212031725/http://tvtotal.prosieben.de/show/letzte_sendung/2007/03/29/10090.html |date=December 12, 2008 }}</ref> and participated in the competitive entertainment event [[Wok racing|World Wok Championships]], where she teamed with [[Sven Hannawald]], [[Christina Surer]] and [[Markus Beyer]] to win the four-person competition.<ref>[http://tvtotal.prosieben.de/show/specials/wokwm2007/ TV total - WOK WM 2007]. ProSieben.de. Accessed April 16, 2008. {{de icon}} {{webarchive |url=https://web.archive.org/web/20080314040647/http://tvtotal.prosieben.de/show/specials/wokwm2007/ |date=March 14, 2008 }}</ref> Her first world title defense, fighting María José Núñez on the undercard of the popular Raab vs. Halmich exhibition bout, was seen by 4.69&nbsp;million television viewers—her most watched fight to date.<ref>[http://www.presseportal.de/story.htx?nr=972265 Reifeprüfung für die "Killer Queen"]. ProSieben Television GmbH.  April 18, 2007. Accessed May 1, 2007. {{de icon}}</ref> A camera crew visited her for one year prior to her first world championship fight against Carolina Alvarez; the documentary aired in June 2007 on the German public broadcaster [[Das Erste]]. A shortened version with commentary in English was aired by the German international broadcaster [[Deutsche Welle]] in October 2007.<ref>Hungerland, Enno. [http://www.wdr.de/unternehmen/presselounge/programmhinweise/fernsehen/2007/06/20070627_diestarkesusi.phtml Die starke Susi - Boxen zum Überleben]. WDR. May 24, 2007. Accessed May 26, 2007. {{de icon}}</ref>

==Other activities==
On 24 November 2010 it was announced Susi Kentikian is the patron of an organization called ''Lebensbaum für Armenien'' (Tree of Life for Armenia), whose aim is to plant 300,000 trees in Armenia and to create jobs in Kentikian´s homeland.

==Professional boxing record==
{{BoxingRecordSummary
|draws=
|nc=1
|ko-wins=17
|ko-losses=
|dec-wins=19
|dec-losses=2
|dq-wins=
|dq-losses=
}}
{|class="wikitable" style="text-align:center; font-size:95%"
|-
!{{abbr|No.|Number}}
!Result
!Record
!Opponent
!Type
!Round, time
!Date
!Location
!Notes
|-
|39
|{{yes2}}Win
|36–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|CRO}} [[Nevenka Mikulic]]
|UD
|10
|30 Jul 2016	
|style="text-align:left;"|{{flagicon|Germany}} {{small|Alsterdorfer Sporthalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WIBF and Global Boxing Union female flyweight titles}}
|-
|38
|{{yes2}}Win
|35–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|MEX}} Susana Cruz Perez
|UD
|10
|2 Oct 2015	
|style="text-align:left;"|{{flagicon|GER}} {{small|Inselparkhalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title;<br>Won vacant WIBF and [[Global Boxing Union|GBU]] female flyweight titles}}
|-
|37
|{{yes2}}Win
|34–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|JPN}} Naoko Fujioka
|UD
|10
|8 Nov 2014	
|style="text-align:left;"|{{flagicon|GER}} {{small|Porsche-Arena, Stuttgart, Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|36
|{{yes2}}Win
|33–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|KOR}} Dan-Bi Kim
|TKO
|9 (10), {{small|1:39}}
|31 May 2014	
|style="text-align:left;"|{{flagicon|GER}} {{small|[[König Palast]], [[Krefeld]], Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|35
|{{yes2}}Win
|32–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|ITA}} Simona Galassi
|UD
|10
|7 Dec 2013	
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Porsche-Arena]], [[Stuttgart]], Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|34
|{{yes2}}Win
|31–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|USA}} Carina Moreno
|UD
|10
|6 Jul 2013	
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Westfalenhalle]], [[Dortmund]], Germany}}
|style="text-align:left;"|{{small|Won WBA female flyweight title}}
|-
|33
|{{yes2}}Win
|30–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|BEL}} Sanae Jah
|UD
|10
|1 Feb 2013
|style="text-align:left;"|{{flagicon|GER}} {{small|[[ISS Dome]], Düsseldorf, Germany}}
|style="text-align:left;"|{{small|Won vacant WBA female [[interim championship|interim]] flyweight title}}
|-
|32
|{{no2}}Loss
|29–2 {{small|(1)}}
|style="text-align:left;"|{{flagicon|USA}} Carina Moreno
|SD
|10
|1 Dec 2012
|style="text-align:left;"|{{flagicon|GER}} {{small|Burg-Wächter Castello, Düsseldorf, Germany}}
|style="text-align:left;"|{{small|Lost WBA female flyweight title}}
|-
|31
|{{no2}}Loss
|29–1 {{small|(1)}}
|style="text-align:left;"|{{flagicon|USA}} [[Melissa McMorrow]]
|{{abbr|MD|Majority decision}}
|10
|16 May 2012
|style="text-align:left;"|{{flagicon|GER}} {{small|Brandenburg-Halle, Frankfurt, Germany}}
|style="text-align:left;"|{{small|Lost WBO, and WIBF female flyweight titles}}
|-
|30
|{{yes2}}Win
|29–0 {{small|(1)}}
|style="text-align:left;"|{{flagicon|THA}} Teeraporn Pannimit
|UD
|10
|21 Oct 2011
|style="text-align:left;"|{{flagicon|GER}} {{small|Brandenburg-Halle, [[Frankfurt]], Germany}}
|style="text-align:left;"|{{small|Retained WBA, WBO, and WIBF female flyweight titles}}
|-
|29
|{{yes2}}Win
|28–0 {{small|(1)}}
|style="text-align:left;"|{{flagicon|MEX}} Ana Arrazola
|UD
|10
|26 Mar 2011
|style="text-align:left;"|{{flagicon|GER}} {{small|Universum Gym, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WIBF flyweight title}}
|-
|28
|style="background:#DDD"|{{abbr|NC|No contest}}
|27–0 {{small|(1)}}
|style="text-align:left;"|{{flagicon|MEX}} Arely Muciño
|NC
|3 (10), {{small|0:27}}
|17 Jul 2010
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Sport- und Kongresshalle]], [[Schwerin]], Germany}}
|style="text-align:left;"|{{small|Retained WBA, WBO, and WIBF female flyweight titles;<br>NC after Kentikian sustained a cut from an accidental head clash}}
|-
|27
|{{yes2}}Win
|27–0
|style="text-align:left;"|{{flagicon|GER}} Nadia Raoui
|SD
|10
|24 Apr 2010
|style="text-align:left;"|{{flagicon|GER}} {{small|Alsterdorfer Sporthalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA, WBO, and WIBF female flyweight titles}}
|-
|26
|{{yes2}}Win
|26–0
|style="text-align:left;"|{{flagicon|TUR}} [[Hülya Şahin|Julia Sahin]]
|UD
|10
|10 Oct 2009
|style="text-align:left;"|{{flagicon|GER}} {{small|StadtHalle, [[Rostock]], Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles;<br>Won vacant [[list of WBO female world champions#Flyweight|WBO female flyweight title]]}}
|-
|25
|{{yes2}}Win
|25–0
|style="text-align:left;"|{{flagicon|ARG}} Carolina Marcela Gutierrez Gaite
|UD
|10
|4 Jul 2009
|style="text-align:left;"|{{flagicon|GER}} {{small|Color Line Arena, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|24
|{{yes2}}Win
|24–0
|style="text-align:left;"|{{flagicon|USA}} [[Elena Reid]]
|UD
|10
|20 Mar 2009
|style="text-align:left;"|{{flagicon|GER}} {{small|Alsterdorfer Sporthalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|23
|{{yes2}}Win
|23–0
|style="text-align:left;"|{{flagicon|RUS}} Anastasia Toktaulova
|UD
|10
|5 Dec 2008
|style="text-align:left;"|{{flagicon|GER}} {{small|Sporthalle Brandberge, Halle, Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|22
|{{yes2}}Win
|22–0
|style="text-align:left;"|{{flagicon|ISR}} Hagar Shmoulefeld Finer
|UD
|10
|29 Aug 2008
|style="text-align:left;"|{{flagicon|GER}} {{small|Burg-Wächter Castello, Düsseldorf, Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|21
|{{yes2}}Win
|21–0
|style="text-align:left;"|{{flagicon|USA}} Mary Ortega
|TKO
|1 (10), {{small|1:53}}
|10 May 2008
|style="text-align:left;"|{{flagicon|GER}} {{small|Sporthalle Brandberge, [[Halle (Saale)|Halle]], Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|20
|{{yes2}}Win
|20–0
|style="text-align:left;"|{{flagicon|PHI}} Sarah Goodson
|TKO
|3 (10), {{small|0:57}}
|29 Feb 2008
|style="text-align:left;"|{{flagicon|GER}} {{small|Alsterdorfer Sporthalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA and WIBF female flyweight titles}}
|-
|19
|{{yes2}}Win
|19–0
|style="text-align:left;"|{{flagicon|FRA}} Nadia Hokmi
|UD
|10
|7 Dec 2007
|style="text-align:left;"|{{flagicon|GER}} {{small|Alsterdorfer Sporthalle, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title;<br>Won vacant WIBF female flyweight title}}
|-
|18
|{{yes2}}Win
|18–0
|style="text-align:left;"|{{flagicon|UK}} Shanee Martin
|TKO
|3 (10), {{small|1:14}}
|7 Sep 2007
|style="text-align:left;"|{{flagicon|GER}} {{small|Burg-Wächter Castello, [[Düsseldorf]], Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|17
|{{yes2}}Win
|17–0
|style="text-align:left;"|{{flagicon|FRA}} Nadia Hokmi
|{{abbr|SD|Split decision}}
|10
|25 May 2007
|style="text-align:left;"|{{flagicon|GER}} {{small|Fight Night Arena, Cologne, Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|16
|{{yes2}}Win
|16–0
|style="text-align:left;"|{{flagicon|URU}} Maria Jose Nunez
|TKO
|3 (10)
|30 Mar 2007
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Kölnarena]], Cologne, Germany}}
|style="text-align:left;"|{{small|Retained WBA female flyweight title}}
|-
|15
|{{yes2}}Win
|15–0
|style="text-align:left;"|{{flagicon|VEN}} Carolina Alvarez
|TKO
|9 (10), {{small|0:27}}
|16 Feb 2007
|style="text-align:left;"|{{flagicon|GER}} {{small|Fight Night Arena, [[Cologne]], Germany}}
|style="text-align:left;"|{{small|Won vacant [[list of WBA female world champions#Flyweight|WBA female flyweight title]]}}
|-
|14
|{{yes2}}Win
|14–0
|style="text-align:left;"|{{flagicon|GER}} Maja Frenzel
|TKO
|4 (10), {{small|1:35}}
|21 Nov 2006
|style="text-align:left;"|{{flagicon|GER}} {{small|Universum Gym, Hamburg, Germany}}
|style="text-align:left;"|{{small|Retained WIBF Inter-Continental flyweight title}}
|-
|13
|{{yes2}}Win
|13–0
|style="text-align:left;"|{{flagicon|USA}} [[Maribel Zurita]]
|TKO
|4 (10)
|9 Sep 2006
|style="text-align:left;"|{{flagicon|GER}} {{small|Bördelandhalle, Magdeburg, Germany}}
|style="text-align:left;"|{{small|Won vacant [[Women's International Boxing Federation|WIBF]] Inter-Continental flyweight title}}
|-
|12
|{{yes2}}Win
|12–0
|style="text-align:left;"|{{flagicon|GER}} Daniela Graf
|UD
|10
|25 Jul 2006
|style="text-align:left;"|{{flagicon|GER}} {{small|Sportschule Sachsenwald, Hamburg, Germany}}
|style="text-align:left;"|{{small|Won vacant International German female [[flyweight]] title}}
|-
|11
|{{yes2}}Win
|11–0
|style="text-align:left;"|{{flagicon|RUS}} Evgeniya Zablotskaya
|TKO
|2 (6), {{small|1:32}}
|15 Apr 2006
|style="text-align:left;"|{{flagicon|GER}} {{small|Maritim Hotel, Magdeburg, Germany}}
|
|-
|10
|{{yes2}}Win
|10–0
|style="text-align:left;"|{{flagicon|BUL}} Emilina Metodieva
|TKO
|4 (6), {{small|1:33}}
|14 Jan 2006
|style="text-align:left;"|{{flagicon|GER}} {{small|Ballhaus, Aschersleben, Germany}}
|
|-
|9
|{{yes2}}Win
|9–0
|style="text-align:left;"|{{flagicon|RUS}} Maria Krivoshapkina
|UD
|6
|13 Dec 2005
|style="text-align:left;"|{{flagicon|AUT}} {{small|Freizeit Arena, [[Sölden]], Austria}}
|
|-
|8
|{{yes2}}Win
|8–0
|style="text-align:left;"|{{flagicon|BUL}} Svetla Taskova
|TKO
|2 (6), {{small|1:08}}
|29 Oct 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|TURM ErlebnisCity, [[Brandenburg]], Germany}}
|
|-
|7
|{{yes2}}Win
|7–0
|style="text-align:left;"|{{flagicon|CZE}} Renata Vesecka
|TKO
|4 (6), {{small|0:58}}
|17 Sep 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|Harzlandhalle, [[Ilsenburg]], Germany}}
|
|-
|6
|{{yes2}}Win
|6–0
|style="text-align:left;"|{{flagicon|SVK}} Simona Pencakova
|TKO
|2 (4), {{small|1:14}}
|2 Jul 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|Color Line Arena, Hamburg, Germany}}
|
|-
|5
|{{yes2}}Win
|5–0
|style="text-align:left;"|{{flagicon|BUL}} Albena Atseva
|TKO
|2 (6)
|4 Jun 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|Ballhaus, [[Aschersleben]], Germany}}
|
|-
|4
|{{yes2}}Win
|4–0
|style="text-align:left;"|{{flagicon|SVK}} Juliia Vlasenko
|TKO
|3 (4), {{small|1:50}}
|7 May 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Volkswagen Halle]], [[Braunschweig]], Germany}}
|
|-
|3
|{{yes2}}Win
|3–0
|style="text-align:left;"|{{flagicon|SVK}} Lucie Sovijusova
|{{abbr|TKO|Technical knockout}}
|1 (4)
|9 Mar 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|Sporthalle [[Wandsbek]], Hamburg, Germany}}
|
|-
|2
|{{yes2}}Win
|2–0
|style="text-align:left;"|{{flagicon|GER}} Debbie Lohmaier
|{{abbr|KO|Knockout}}
|1 (4), {{small|1:23}}
|26 Feb 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Color Line Arena]], [[Hamburg]], Germany}}
|
|-
|1
|{{yes2}}Win
|1–0
|style="text-align:left;"|{{flagicon|BUL}} Iliana Boneva
|{{abbr|UD|Unanimous decision}}
|4
|15 Jan 2005
|style="text-align:left;"|{{flagicon|GER}} {{small|[[Bördelandhalle]], [[Magdeburg]], Germany}}
|style="text-align:left;"|{{small|Professional debut}}
|}

==Awards==
*Hamburg's sportswoman of the year – 2007<ref>[https://www.welt.de/welt_print/article1632856/Hamburgs_Sportler_des_Jahres.html Hamburgs Sportler des Jahres]. ''Die Welt''. February 5, 2008. Accessed April 13, 2008. {{de icon}}</ref>
*WBA Best Female Boxer – 2007/2008<ref>[http://www.wbaonline.com/news2008/newscena.asp The WBA congratulates 2007/2008 Award Winners]. WBAOnline.com. Accessed July 26, 2008. {{webarchive |url=https://web.archive.org/web/20080803234449/http://www.wbaonline.com/news2008/newscena.asp |date=August 3, 2008 }}</ref>
*Germany's female boxer of the year – 2008<ref name="Focus"/>

==References==
{{Reflist|30em}}

==External links==
{{commons}}
*{{Official website|http://www.kentikian-promotion.com/}} {{de icon}}
*{{Boxrec|292211}}
*[http://www.wban.org/biog/skentikian.htm Susianna Kentikian profile] at wban.org
*[http://www.lebensbaum-armenien.de/ ''Lebensbaum für Armenien''] {{de icon}}

{{s-start}}
{{s-sports}}
{{s-text|style=background:#C1D8FF; font-weight: bold;|text=Minor world boxing titles}}
{{s-break}}
{{s-vac|last=[[Regina Halmich]]}}
{{s-ttl|title=[[Women's International Boxing Federation|WIBF]] [[flyweight]] champion
|years=7 December 2007 – 16 May 2012}}
{{s-aft|after=[[Melissa McMorrow]]}}
{{s-vac|last=Eva Voraberger}}
{{s-ttl|title=WIBF flyweight champion
|years=2 October 2015 – present}}
{{s-inc|rows=2}}
{{s-vac|last=[[Eileen Olszewski]]}}
{{s-ttl|title=[[Global Boxing Union|GBU]] female flyweight champion
|years=2 October 2015 – present}}
{{s-text|style=background:#C1D8FF; font-weight: bold;|text=Major world boxing titles}}
{{s-new|rows=2}}
{{s-ttl|title=[[List of WBA female world champions#Flyweight|WBA female flyweight champion]]
|years=16 February 2007 – 1 December 2012}}
{{s-aft|after=Carina Moreno}}
{{s-ttl|title=[[List of WBO female world champions#Flyweight|WBO female flyweight champion]]
|years=10 October 2009 – 16 May 2012}}
{{s-aft|after=Melissa McMorrow}}
{{s-vac|last=Arely Muciño}}
{{s-ttl|title=WBA female flyweight champion<br>[[Interim title]]
|years=1 February 2013 – 6 July 2013<br>Won full title}}
{{s-vac|next=}}
{{s-break}}
{{s-bef|before=Carina Moreno}}
{{s-ttl|title=WBA female flyweight champion
|years=6 July 2013 – present}}
{{s-inc}}
{{s-end}}

{{featured article}}
{{Authority control}}

{{DEFAULTSORT:Kentikian, Susianna}}
[[Category:1987 births]]
[[Category:Living people]]
[[Category:Sportspeople from Yerevan]]
[[Category:Armenian women boxers]]
[[Category:German Armenians]]
[[Category:German women boxers]]
[[Category:Armenian emigrants to Germany]]
[[Category:Naturalized citizens of Germany]]
[[Category:World flyweight boxing champions]]
[[Category:Flyweight boxers]]
[[Category:World Boxing Association champions]]
[[Category:World Boxing Organization champions]]