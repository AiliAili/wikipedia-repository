{{Use dmy dates|date=March 2017}}
{{Use British English|date=March 2017}}
[[File:George-watson-graveyard.jpg|thumb|right|Watson's memorial shown in situ with – inset – a close-up to allow the wording to be read. (Click the image to load large version.)]]
'''George Watson''' (23 November 1654 in [[Edinburgh]] – 3 April 1723) was born in Scotland to parents John Watson; a merchant, and [[Marion Ewing]]. He was orphaned at an early age, but thanks to his aunt, [[Elizabeth Davidson]], he was sent in 1672 to be educated in book-keeping at [[Rotterdam]]. He returned to Edinburgh to become, in 1676, private secretary to [[Sir James Dick]]. Based partly on this experience he became one of [[Scotland]]'s most famed accountants of his time, and was appointed chief accountant to the [[Bank of Scotland]] when it was founded in 1695.

George Watson is buried in Edinburgh's [[Greyfriars Kirkyard]], and although the precise location of his remains is unknown, there is a memorial plaque in a wall in the north-west of the graveyard.

== Educational benefactions ==
Having been an early benefactor of Edinburgh's [[Merchant Maiden Hospital]], Watson subsequently bequeathed in his will a generous sum for the [[Merchant Company of Edinburgh]], with specific sums being set aside for educating pupils at the Merchant Maiden Hospital, the [[Trades Maiden Hospital]] and Heriot's Hospital (now [[George Heriot's School]]). He stipulated that all his beneficiaries were to bear the name Watson or Davidson. (This was not an unusual style of stipulation at the time; another example being the similar founding principle of Edinburgh's [[Donaldson's College|Donaldson's Hospital]] where bursaries were made available to children bearing the name of Donaldson.)

A further £144,000 [[Pound Scots]] was left for the foundation of a new charitable school ("hospital" as they were then known) for "entertaining and educating the male children and grandchildren of decayed merchants in Edinburgh". This was to become [[George Watson's College]]. That school remains in service to this day (as does George Heriot's), and "Watson's" as it is known continues to celebrate an annual "Founder’s Day".

==References==
# [http://www.undiscoveredscotland.co.uk/usbiography/w/georgewatson.html Undiscovered Scotland]

{{DEFAULTSORT:Watson, George}}
[[Category:1654 births]]
[[Category:1723 deaths]]
[[Category:Burials at Greyfriars Kirkyard]]
[[Category:People from Edinburgh]]
[[Category:Scottish accountants]]
[[Category:Scottish philanthropists]]


{{Scotland-bio-stub}}