{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{infobox book | 
| name          = Sanditon
| title_orig    =
| translator    =
| image         = <!--prefer 1st edition-->
| caption =
| author        = [[Jane Austen]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = [[Romantic novel]]
| publisher     =
| release_date  = 1817
| english_release_date =
| media_type    = Print (hardback & paperback)
| pages         =
| preceded_by   =
| followed_by   =
}}

'''''Sanditon''''' (1817) is an [[unfinished work|unfinished novel]] by the English writer [[Jane Austen]]. In January 1817, Austen began work on a new novel she called ''The Brothers'', later titled ''Sanditon'' upon its first publication in 1925, and completed twelve chapters before stopping work in mid-March 1817, probably because her illness prevented her from continuing.<ref>Claire Tomalin, ''Jane Austen''. Page 261.</ref>

==Plot==
The novel centers on Charlotte Heywood, the eldest of the daughters still at home in the large family of a country gentleman from Willingden, Sussex. The narrative opens when the carriage of Mr. and Mrs. Parker of Sanditon topples over on a hill near the Heywood home. Because Mr. Parker is injured in the crash, and the carriage needs repairs, the Parkers stay with the Heywood family for a fortnight. During this time, Mr. Parker talks fondly of Sanditon, a town which until a few years before had been a small, unpretentious fishing village. With his business partner, Lady Denham, Mr. Parker hopes to make Sanditon into a fashionable seaside resort. Mr. Parker's enormous enthusiasm for his plans to improve and modernise Sanditon has resulted in the installation of [[bathing machine]]s and the construction of a new home for himself and his family near the seashore. Upon repair of the carriage and improvement to Mr. Parker's foot, the Parkers return to Sanditon, bringing Charlotte with them as their summer guest.

Upon arrival in Sanditon, Charlotte meets the inhabitants of the town. Prominent among them is Lady Denham, a twice-widowed woman who received a fortune from her first husband and a title from her second. Lady Denham lives with her poor niece Clara Brereton, who is a sweet and beautiful, yet impoverished, young lady. Also living in Sanditon are Sir Edward Denham and his sister Esther, Lady Denham's nephew and niece by her second husband. The siblings are poor and are thought to be seeking Lady Denham's fortune. Sir Edward is described as a silly and very florid man, though handsome.

After settling in with the Parkers and encountering the various neighbours, Charlotte and Mr. and Mrs. Parker are surprised by a visit from Mr. Parker's two sisters and younger brother, all of whom are self-declared invalids. However, given their level of activity and seeming strength, Charlotte quickly surmises that their complaints are invented. Diana Parker has come on a mission to secure a house for a wealthy family from the West Indies, although she has not specifically been asked for her aid. She also brings word of a second large party, a girls' school, which is intending to summer at Sanditon. This news causes a stir in the small town, especially for Mr. Parker, whose fondest wish is the promotion of tourism in the town.

With the arrival of Mrs. Griffiths to Sanditon, it soon becomes apparent that the family from the West Indies and the girls' school group are one and the same. The visitors consist of Miss Lambe, a "half mulatto" <ref>"Of these three, and indeed of all, Miss Lambe was beyond comparison the most important and precious, as she paid in proportion to her fortune. She was about seventeen, half mulatto, chilly and tender, had a maid of her own, was to have the best room in the lodgings, and was always of the first consequence in every plan of Mrs. Griffiths." http://gutenberg.net.au/ebooks/fr008641.html</ref> rich young woman of about seventeen from the West Indies, and the two Miss Beauforts, common English girls. In short order, Lady Denham calls on Mrs. Griffiths to be introduced to Miss Lambe, the very sickly and very rich heiress that she intends her nephew Sir Edward to marry.

A carriage unexpectedly arrives bearing Sidney Parker, the second eldest Parker brother. He will be staying in town for a few days with two friends who will  join him shortly. Sidney Parker is around 27 or 28 and Charlotte finds him very good looking with a decided air of fashion.

The book fragment ends when Mrs. Parker and Charlotte visit Sanditon House, Lady Denham's residence. There Charlotte spots Clara Brereton seated with Sir Edward Denham at her side having an intimate conversation in the garden and surmises that they must have a secret understanding. When they arrive inside, Charlotte observes that a large portrait of Sir Henry Denham hangs over the fireplace, whereas Lady Denham's first husband, who owned Sanditon House, only gets a miniature in the corner—obliged to sit back in his own house and see the best place by the fire constantly occupied by Sir Henry Denham.

==Literary criticism==
The people of "modern Sanditon", as Austen calls it, have moved out of the "old house – the house of [their] forefathers" and are busily constructing a new world in the form of a modern seaside commercial town. The town of Sanditon is probably based on [[Worthing]], where Austen stayed in late 1805 when the resort was first being developed;<ref>Clarke, Jan, Jane Austen Society Report 2008, pages 86–105.</ref><ref>Halperin, John, "Jane Austen's Anti-Romantic Fragment: Some Notes on Sanditon", 1983, University of Tulsa</ref> or on [[Eastbourne]];<ref>Cullen, Pamela V., "A Stranger in Blood: The Case Files on Dr [[John Bodkin Adams]]", London, Elliott & Thompson, 2006, ISBN 1-904027-19-9</ref> or on Bognor Regis, whose founder Richard Hotham was the inspiration for Mr Parker (and the town contained a library at that time as described in the book).<ref>http://www.british-history.ac.uk/report.aspx?compid=41752</ref><ref>http://janeausten-herlifeandworks.blogspot.co.uk/2011/08/sanditon-creating-seaside-resort.html</ref> The town is less of an actual reality than it is an ideal of the inhabitants – one that they express in their descriptions. These inhabitants have a conception of the town's identity and of the way in which this identity should be spread to, and appreciated by, the world:

:"My name perhaps… may be unknown at this distance from the coast – but Sanditon itself – everybody has heard of Sanditon, – the favourite – for a young and rising bathing-place, certainly the favourite spot of all that are to be found along the coast of Sussex; – the most favoured by nature, and promising to be the most chosen by man." (''Sanditon'')

==Continuations and adaptations==
Because Austen completed setting the scene for Sanditon, it has been a favourite of "[[continuator]]s" – later writers who try to complete the novel within Austen's vision while emulating her style. Such "completed" versions of Sanditon include:
* ''Sanditon'', by Jane Austen and "another lady", ISBN 0-684-84342-0; also published as ''Sanditon'', by Jane Austen and Marie Dobbs, ISBN 3-423-12666-3 and ''Sanditon'', by Jane Austen and Anne Telscombe, ISBN 0-395-20284-1<ref>Marie Dobbs, an Australian journalist, published novels under the pseudonym "Anne Telscombe", and initially published ''Sanditon'' as "another lady" in 1975; later editions appear to have been published under all three names. See [[LCCN]] [http://lccn.loc.gov/n50008228 n50008228].</ref>
* ''A Completion of Sanditon'', by Juliette Shapiro, ISBN 1-58939-503-4 (does not include Austen's text)
* ''A Return to Sanditon: a completion of Jane Austen's fragment'', by Anne Toledo, ISBN 978-1-4580-7426-3 (includes Austen's text)
* ''Sanditon'', by Jane Austen and completed by D.J. Eden, ISBN 0-7541-1610-7
* ''Jane Austen's Sanditon: A continuation'', by Anna Austen Lefroy (Austen's niece), ISBN 0-942506-04-9 (also unfinished)
* ''Jane Austen out of the blue'', by Donald Measham, ISBN 978-1-84728-648-2
* ''Jane Austen's Charlotte'', by Jane Austen and completed by Julia Barrett, ISBN 0-87131-908-X
* ''A Cure for All Diseases'' (Canada and US title: ''The Price of Butcher's Meat'') by [[Reginald Hill]] ISBN 978-0-06-145193-5, a novel in the Dalziel and Pascoe series, is acknowledged by the author to be a "completion" of Sanditon. In Hill's novel, the village is renamed Sandytown, and lies on the Yorkshire coast.
* "[[The Lizzie Bennet Diaries#Welcome to Sanditon|Welcome to Sanditon]]", a modernised mini webseries adaptation set in California, produced by the creators of "[[Lizzie Bennet Diaries|The Lizzie Bennet Diaries]]" and premiered on 13 May 2013. Not continuing the story, the adaptation uses ''[[deus ex machina]]'' to end where Austen left off, replacing Charlotte with [Fitz]William Darcy's sister, Georgiana (Gigi) from ''[[Pride and Prejudice]]''. Near the end, William makes an appearance, pulling Gigi out from her role as Charlotte.
* ''Sanditon-Film-of-the-Play''<ref>{{Cite web|url=http://www.sanditon.info|title=Sanditon the play, finished by Chris Brindle|website=Sanditon the Play|language=en-GB|access-date=2016-07-21}}</ref>  June 2014 by Chris Brindle (Author of "Hampshire" ISBN 978-0-957-3236-0-5 and ISBN 978-0-957-3236-1-2)   Video (EAN 0724120127675) of a rehearsed reading in costume, comprising an adaptation in Act 1 of the Jane Austen dialogue, and in Act 2 an adaptation of the 1845 continuation by Anna Austen Lefroy (Austen's niece) and the author's completion. Also includes a 10-minute short film and the specially composed duet "Blue Briny Sea".
* ''Sanditon-the-Documentary'' Nov 2014 by Chris Brindle (Author of "Hampshire" ISBN 978-0-957-3236-0-5 and ISBN 978-0-957-3236-1-2) also (EAN 0724120127675) Filmed on location in Hampshire (Selborne, Chawton, Alton, Ashe, Steventon & Oakley) and Berkshire (Knowl Hill and Bray) and tells the story of the writing of Anna Austen Lefroy's Sanditon continuation and the first biography of Austen by James Edward Austen-Leigh. Music by American composer and JASNA delegate, Amanda Jacobs, performed by Vicky Clubb on the historic pianos at Chawton Cottage and Chawton House Library.

== References ==
{{reflist|colwidth=30em}}
*Austen, Jane.  ''Sanditon and Other Stories.''  Ed. Peter Washington. New York: Alfred A. Knopf; Everyman’s Library,  1996.
*Spacks, Patricia Meyer.  ''Gossip.''  New York: Alfred A. Knopf, Inc., 1985.
*Tomalin, Claire.  ''Jane Austen: A Life.''  New York: Vintage, 1997.

== External links ==
{{Portal|Jane Austen}}
{{Wikisource}}
* [http://www.pemberley.com/janeinfo/janeinfo.html Jane Austen Information Page]
* [http://www.janeausten.co.uk/ Jane Austen Centre, Bath, England]

{{Jane Austen}}

{{Authority control}}

[[Category:Novels by Jane Austen]]
[[Category:1817 novels]]
[[Category:Unfinished novels]]
[[Category:Novels published posthumously]]
[[Category:Novels set in Sussex]]
[[Category:Novels about nobility]]