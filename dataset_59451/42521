<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Caudron C.25
 | image=Caudron C.25.png
 | caption=
}}{{Infobox Aircraft Type
 | type=16-18 passenger [[airliner]]
 | national origin=[[France]]
 | manufacturer=[[Caudron]]
 | designer=Paul Deville
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Caudron C.25''' was a large, three engined, [[biplane]] [[airliner]], designed and built in [[France]] soon after the end of [[World War I]]. Its enclosed cabin could accommodate up to eighteen passengers.

==Design and development==

After the end of world War I some [[Caudron C.23]]s, a large [[trimotor]] night bomber, were modified to act as passenger aircraft, but the C.25 was the first aircraft Caudron had designed from the beginning to carry passengers. It was large, with a {{convert|25|m|ftin|abbr=on|0}} span,<ref name=Hauet1/> so Caudron built in hinges to allow the wings to be folded for easier handling on the ground. The wings were rectangular in plan, mounted without [[Stagger (aeronautics)|stagger]] and [[aircraft fabric covering|fabric covered]]. It was a [[biplane#Bays|three bay biplane]] with unusually thin, wooden, parallel, vertical [[interplane strut]]s, assisted by [[flying wire]]s of thicker than usual gauge.  The engine mountings, two pairs of parallel struts, defined the inner bay. The outer pair of these struts were doubled since this was where the wing folded and so the ends of the wing either side of the fold retained their struts.  Four vertical [[cabane strut]]s braced the centre of the upper wing from the upper [[fuselage]].<ref name=Flight1/> There were [[aileron]]s on both lower and lower planes, extending over more than half the span. These were not [[balanced rudder|aerodynamically balanced]] in the usual way but were connected to each other by three vertical rods which halfway up were hinged to the trailing edge of a narrower and shorter horizontal surface which rotated about an axis well behind its [[aerodynamic centre]]. The wires from the [[cockpit]] operated these ancillary surfaces, which then moved the ailerons, just as the [[leading edge]] of a conventional balanced aileron, extending forward of the hinge, assists the pilot's input.<ref name=Hauet1/><ref name=Flight1/>

The Caudron C.23 was powered by three {{convert|250|hp|kW|abbr=on|0|order=flip}} [[Salmson 9Z]] nine cylinder water-cooled [[radial engine]]s, each neatly cowled. Two were mounted halfway between the wings in flat sided [[nacelle]]s which curved to a point at the rear and the third was nose mounted. The wing mounted engines had fuel tanks in the nacelles and the nose engine was served by a tank behind it.  Behind it the fuselage was rectangular in section and [[plywood]] covered to the rear of the passenger cabin, after which it was fabric covered.  The two crew sat in separate, wide, side-by-side open [[cockpit]]s in the upper part of the fuselage ahead of the wing [[leading edge]]. The protrusion of their cockpits into the lower fuselage divided  the passenger space into a main cabin behind and a smaller one forwards, accessed by ducking under the bottom of the cockpits. The interior had large windows and was carefully furnished, with [[wicker]] armchairs and tables, lighting and decorated walls;<ref name=Flight1/> a toilet was provided.<ref name=Lauto/>

The horizontal tail was a biplane unit with the lower [[tailplane]] attached  to the bottom of the fuselage and the upper one on top of a small, shallow [[fin]]. The rectangular tailplanes were braced together by two interplane struts on each side; both were rectangular and carried [[elevator (aircraft)|elevator]]s. Three rudders occupied the gap between the elevators, the tips cut away for elevator movement. Only the outer rudders were balanced but these were directly connected to the central one, assisting it. The C.25 had fixed tailskid [[conventional landing gear]] with main wheels in pairs under each engine, assisted by another pair under the nose to prevent noseovers.<ref name=Hauet1/><ref name=Flight1/>

The C.25 was on display at the Paris Aero Show in 1919, though it may not have flown by then.<ref name=Flight1/>  Little is recorded on its subsequent history.

==Specifications==
{{Aircraft specs
|ref=Hauet (2001) p.142<ref name=Hauet1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One pilot plus second pilot/engineer/ radio operator
|capacity=16-18
|length m=19.00
|length note=
|span m=25.00
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|height m=5.45
|height note=
|wing area sqm=155
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=3400
|empty weight note=
|gross weight kg=5500
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=3
|eng1 name=[[Salmson 9Z]]
|eng1 type=water-cooled 9-cylinder [[radial engine|radial]]
|eng1 hp=250
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=<ref name=Flight1/>
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=165
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=6 hr
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

==References==
{{reflist|refs=

<ref name=Hauet1>{{cite book |title=Les Avions Caudrons |last=Hauet|first=André|year=2001|volume=1|publisher=Lela Presse|location=Outreau|isbn=2 914017-08-1|page=142}}</ref>

<ref name=Flight1>{{cite magazine |date=8 January 1920 |title=The Paris Aero Show 1919|magazine= [[Flight International|Flight]]|volume=XII |issue=38 |pages=35–7  |url= http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200037.html}}</ref>

<ref name=Lauto>{{cite journal|date=15 January 1920 |title=Le VI<sup>e</sup> Salon de Aéronautique|journal=L'Automobilia|volume=64 |page=29|url=http://gallica.bnf.fr/ark:/12148/bpt6k6244615j/f23 }}</ref>

}}

{{Caudron aircraft}}

[[Category:Caudron aircraft|C.025]]
[[Category:French airliners 1910–1919]]