{{Infobox journal
| cover = [[Image:J Gen Virol cover (Nov 2007).gif]]
| discipline = [[Virology]]
| abbreviation = J. Gen. Virol.
| publisher = [[Society for General Microbiology]]
| country = United Kingdom
| frequency = Monthly
| history = 1967-present
| openaccess = [[Delayed open access|Delayed]], after 12 months
| impact = 3.363
| impact-year = 2011
| website = http://jgv.sgmjournals.org/
| link1 = http://jgv.sgmjournals.org/content/current
| link1-name = Online access
| link2 = http://jgv.sgmjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 0022-1317
| eISSN = 1465-2099
| OCLC = 802624762
| LCCN = 81644163
| CODEN = JGVIAY
}}
The '''''Journal of General Virology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] that covers research into [[virus]]es affecting animals, plants, insects, bacteria, and fungi, including their [[molecular biology]], [[immunology]], and interactions with the host. [[Antiviral drug|Antiviral compounds]] are also covered. Established in 1967, it is published monthly by the [[Society for General Microbiology]]. The [[editor-in-chief]] is S. Efstathiou ([[University of Cambridge]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 3.363.<ref name=WoS>{{cite book |year=2013 |chapter=Journal of General Virology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-02-19 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://jgv.sgmjournals.org/}}

[[Category:Delayed open access journals]]
[[Category:Publications established in 1967]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Virology journals]]