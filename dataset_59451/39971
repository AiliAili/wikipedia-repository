{{for|the 19th-century baseball player|Charlie Sweeney}}
{{Infobox military person
|name=Charles W. Sweeney
|birth_date={{birth date|1919|12|27}}
|death_date= {{death date and age|2004|07|16|1919|12|27}}
|birth_place=[[Lowell, Massachusetts]]
|death_place=[[Boston, Massachusetts]]
|placeofburial=
|placeofburial_label= Place of burial
|image=669923249 m.jpg
|caption=USAF picture
|nickname=
|allegiance={{flag|United States of America}}
|branch= [[File:Seal of the US Air Force.svg|25px]] [[United States Air Force]] <br/>[[United States Army Air Corps]]
|serviceyears=1941–1976
|rank= [[File:US-O8 insignia.svg|20px]] [[Major general (United States)|Major General]]
|commands=[[393rd Bombardment Squadron]]<br/>[[102nd Tactical Fighter Wing]]
|unit=
|battles=[[World War II]]<br/>[[Atomic Bombing of Hiroshima]]<br/>[[Atomic Bombing of Nagasaki]]
|awards= [[Silver Star]]<br/>[[Air Medal]]
|laterwork=
}}
[[Major general (United States)|Major General]] '''Charles W. Sweeney''' (December 27, 1919 – July 16, 2004) was an officer in the [[U.S. Army Air Forces]] during [[World War II]] and the [[Aviator|pilot]] who flew [[Bockscar]] carrying the [[Fat Man]] [[atomic bomb]] to [[Atomic bombings of Hiroshima and Nagasaki|the Japanese city of Nagasaki]] on August 9, 1945.  Separating from active duty at the end of World War II, he later became an officer in the [[Massachusetts Air National Guard]] as the Army Air Forces transitioned to an independent [[U.S. Air Force]], eventually rising to the rank of Major General.

==509th Composite Group==
Sweeney became an instructor in the atomic missions training project, [[Project Alberta]], at [[Wendover Air Force Base|Wendover Army Airfield]], [[Utah]]. Selected to be part of the [[509th Operations Group|509th Composite Group]] commanded by Col. [[Paul Tibbets]], he was named commander of the [[320th Troop Carrier Squadron]] on 6 January 1945. Initially his squadron used [[C-47 Skytrain]] and [[C-46 Commando]] transports on hand to conduct the [[top secret]] operations to supply the 509th, but in April 1945 it acquired five [[C-54 Skymaster]]s, which had the range to deliver personnel and materiel to the western Pacific area.

On May 4, 1945, Sweeney became commander of the [[393d Bomb Squadron|393d Bombardment Squadron, Heavy]], the combat element of the 509th, in charge of 15 [[Silverplate]] B-29s and their flight and ground crews, 535 men in all. In June and July Sweeney moved his unit to North Field on the island of [[Tinian]] in the [[Mariana Islands|Marianas]].

In addition to supervising the intensive training of his flight crews during July 1945, Sweeney was slated to command the second atomic bomb mission. He trained with the crew of Captain [[Charles Donald Albury|(Charles D.) Don Albury]] aboard their B-29 ''[[The Great Artiste]]'', and was aircraft commander on the training mission of July 11. He and the crew flew five of the nine rehearsal test drops of inert [[Little Boy]] and [[Fat Man]] bomb assemblies in preparation for the missions.

On 6 August 1945, Sweeney and Albury piloted ''The Great Artiste'' as the instrumentation and observation support aircraft for the [[Atomic bombing of Hiroshima and Nagasaki|atomic bomb attack on Hiroshima]].

===The Nagasaki Mission===
On 9 August 1945, Major Sweeney commanded ''[[Bockscar]]'', which carried the atomic bomb ''[[Fat Man]]'' from the island of [[Tinian]] to Nagasaki. In addition to ''Bockscar'', the mission included two observation and instrumentation support B-29s, ''The Great Artiste'' and ''The Big Stink'', who would rendezvous with ''Bockscar'' over [[Yakushima Island]]. At the mission pre-briefing, the three planes were ordered to make their rendezvous over Yakushima at 30,000 feet due to weather conditions over [[Iwo Jima]] (the Hiroshima mission rendezvous). That same morning, on the day of the mission, the ground crew notified Sweeney that a faulty fuel transfer pump made it impossible to utilize some 625 gallons of fuel in the tail, but Sweeney, as aircraft commander, elected to proceed with the mission.<ref>Polmar, Norman, ''The Enola Gay: the B-29 that dropped the Atomic Bomb on Hiroshima'', Washington, D.C.: The Smithsonian Institution, (2004), p. 35</ref><ref>Miller, Donald, ''The Story of World War II'', New York: Simon & Schuster, ISBN 978-0-7432-1198-7, ISBN 0-7432-1198-7, p. 630</ref>

Before takeoff, Col. Tibbets warned Sweeney that he had lost at least 45 minutes of flying time because of the fuel pump problem, and to take no more than fifteen minutes at the rendezvous before proceeding directly to the primary target.<ref>Miller, Donald, pp. 630-631: Tibbets stated that he told Sweeney to "go to your rendezvous point and tell the other planes the same thing I told you at Iwo Jima [the Hiroshima mission rendezvous]: 'Make one 360-degree turn, be on my wing, or I'm going to the target anyway.'"</ref>

After takeoff from Tinian, ''Bockscar'' reached its rendezvous point and after circling for an extended period, found ''The Great Artiste'', but not ''The Big Stink''.<ref>Miller, Donald, p. 631</ref>  Climbing to 30,000 feet, the assigned rendezvous altitude, both aircraft slowly circled Yakushima Island. Though Sweeney had been ordered not to wait at the rendezvous for the other aircraft longer than fifteen minutes before proceeding to the primary target, Sweeney continued to wait for ''The Big Stink'', perhaps at the urging of Commander [[Frederick Ashworth]], the plane's weaponeer.<ref>Miller, Donald, pp. 630, 631: Tibbets noted that regardless of any advice he may have received, Sweeney was the aircraft commander, and remained responsible at all times for command of the aircraft and the mission.</ref>  After exceeding the original rendezvous time limit by a half-hour, ''Bockscar'', accompanied by the ''The Great Artiste'', proceeded to the primary target, [[Kokura]].<ref>Miller, Donald, pp. 631-632</ref>  No less than three bomb runs were made, but the delay at the rendezvous had resulted in 7/10ths cloud cover over the primary target, and the bombardier was unable to drop.<ref>Miller, Donald P., pp. 630, 632-633: In making an unheard-of third bomb run with a $25-million-dollar atomic weapon, it appeared to others that Sweeney appeared determined not to abort the mission and return with ''Fat Man'', regardless of the risk to the aircraft or the flight crew.</ref>  By the time of the third bomb run, Japanese antiaircraft fire was getting close, and Japanese fighter planes could be seen climbing to intercept ''Bockscar''.<ref>Miller, Donald, p. 633</ref>

Poor bombing visibility and an increasingly critical fuel shortage eventually forced ''Bockscar'' to divert from Kokura and attack the secondary target, Nagasaki.<ref>Miller, Donald, p. 632</ref> As they approached Nagasaki, the heart of the city's downtown was covered by dense cloud, and Sweeney and the plane's weaponeer, Commander Ashworth, initially decided to bomb Nagasaki using radar.<ref>Miller, Donald, p. 633-634</ref> However, a small opening in the clouds allowed ''Bockscar's'' bombardier to verify the target as Nagasaki. As the crew had been ordered to drop the bomb visually if possible, Sweeney decided to proceed with a visual bomb run.<ref>Miller, Donald, pp. 634-635</ref>  ''Bockscar'' then dropped ''Fat Man'', with a blast yield equivalent to 21 kilotons of [[Trinitrotoluene|TNT]]. It exploded 43 seconds later at {{convert|469|m|ft|abbr=off|sp=us}} above the ground, at least {{convert|2.5|km|mi|abbr=off|sp=us}} northwest of the planned aim point.<ref>Wainstock, Dennis D., ''The Decision to Drop the Atomic Bomb'', Praeger Publishing, ISBN 0-275-95475-7 (1996), p. 92.</ref><ref>Miller, Donald P., pp. 626, 638</ref> The failure to drop ''Fat Man'' at the precise bomb aim point caused the atomic blast to be confined to the Urakami Valley. As a consequence, a major portion of the city was protected by the intervening hills, and only 60% of Nagasaki was destroyed. The bombing also severed the Mitsubishi arms production extensively and killed an estimated 35,000-40,000 people outright, including 23,200-28,200 Japanese industrial workers, 2,000 Korean slave laborers, and 150 Japanese soldiers.<ref>{{cite book |title=Nuke-Rebuke: Writers & Artists Against Nuclear Energy & Weapons (The Contemporary anthology series) |pages=22–29 |date=May 1, 1984 |publisher=The Spirit That Moves Us Press}}</ref> Japan surrendered unconditionally six days later, ending World War II.

Low on fuel, ''Bockscar'' barely made it to the runway on Okinawa. With only enough fuel for one landing attempt, Sweeney brought ''Bockscar'' in fast and hard, ordering every available distress flare on board to be fired as he did so.<ref>Walker, Stephen, ''Shockwave: Countdown to Hiroshima'', New York: HarperCollins (2005) pp. 13-14</ref>  The number two engine died from [[fuel starvation]] as ''Bockscar'' began its final approach.<ref name="Walker, Stephen, p. 14">Walker, Stephen, p. 14</ref>  Touching the runway hard, the heavy B-29 slewed left and towards a row of parked B-24 bombers before the pilots managed to regain control.<ref name="Walker, Stephen, p. 14"/>  With both pilots standing on the brakes, Sweeney made a swerving 90-degree turn at the end of the runway to avoid going over the cliff into the ocean.<ref>Glines, C.V., ''World War II: The Second Atomic Bomb That Ended The War'', Aviation History (January 1997), pp. 36-37</ref> 2nd Lt. [[Jacob Beser]] recalled that at this point, two engines had died from fuel exhaustion, while "the centrifugal force resulting from the turn was almost enough to put us through the side of the airplane."<ref>Glines, C.V., p. 37</ref>

After ''Bockscar'' returned to Tinian, Col. Tibbets recorded that he was faced with the dilemma of considering “if any action should be taken against the airplane commander, Charles Sweeney, for failure to command.”<ref name="ReferenceA">Puttré, Michael, ''Nagasaki Revisited'', retrieved 8 April 2011</ref><ref name="Tibbets, Paul W. 1998">Tibbets, Paul W. ''Return Of The Enola Gay'', Columbus, Ohio: Mid Coast Marketing (1998), ISBN 0-9703666-0-4</ref><ref name="Miller, Donald L. 2005 pp. 361-362">Miller, Donald L., ''D-days in the Pacific'', New York: Simon & Schuster (2005), pp. 361-362</ref>  After meeting on Guam with Col. Tibbets and Major Sweeney, General [[Curtis LeMay]], chief of staff for the Strategic Air Forces, confronted Sweeney, stating ''"You fucked up, didn't you, Chuck?"'', to which Sweeney made no reply.<ref name="Miller, Donald L., pp. 361-362">Miller, Donald L., pp. 361-362</ref>  LeMay then turned to Tibbets and told him that an investigation into Sweeney's conduct of the mission would serve no useful purpose.<ref name="Miller, Donald L., pp. 361-362"/>

In November 1945, Sweeney returned with the 509th Composite Group to [[Walker Air Force Base|Roswell Army Air Base]] in [[New Mexico]] to train aircrews for the atomic testing mission, [[Operation Crossroads]].

==Post-war activities==
Sweeney left active duty with the rank of [[lieutenant colonel]] on June 28, 1946, but remained active with the [[Massachusetts Air National Guard]]. Later promoted to full [[Colonel (United States)|colonel]], on February 21, 1956, Col Sweeney was named commander of its [[102nd Air Defense Wing]] and shortly after, on April 6, was promoted to [[Brigadier general (United States)|Brigadier General]]. He retired in 1976 as a [[Major general (United States)|Major General]] in the [[Air National Guard]].<ref name=WashPostObit>[http://www.washingtonpost.com/wp-dyn/articles/A60638-2004Jul18.html Charles H. Sweeney; Led Bomb Drop Over Nagasaki (washingtonpost.com)<!-- Bot generated title -->]</ref>  He also appeared in the 1970s television series ''[[The World at War]]'' and was seen explaining the USAAF buildup to the mission raids.

Throughout his life Sweeney remained convinced of the appropriateness and necessity of the bombing. "I saw these beautiful young men who were being slaughtered by an evil, evil military force," he said in 1995. "There's no question in my mind that President Truman made the right decision." At the same time, he said, "As the man who commanded the last atomic mission, I pray that I retain that singular distinction."<ref>http://www.washingtonpost.com/wp-dyn/articles/A60638-2004Jul18.html</ref>

Near the end of his life, Sweeney wrote a controversial and factually disputed memoir of the atomic bombing and the 509th Composite Group, ''War's End: An Eyewitness Account of America's Last Atomic Mission''.<ref>Puttré, Michael, ''[http://www.mputtre.com/id45.html Nagasaki Revisited]'', retrieved 8 April 2011</ref><ref>Coster-Mullen, John, ''Atom Bombs: The Top Secret Inside Story of Little Boy and Fat Man,'' publ. J. Coster-Mullen, End Notes, (2004): Gen. [[Paul Tibbets]], Major [[Dutch Van Kirk]] (Enola Gay's navigator), and other surviving members of the 509th Composite Group were reportedly outraged at many of the factual assertions by Sweeney in ''War's End''</ref> In ''War's End'', Sweeney defended the decision to drop the atomic bomb in light of subsequent historical questioning. However, it was Sweeney's other assertions regarding the Nagasaki atomic mission, along with various anecdotes regarding the 509th and its crews that drew the most criticism. General [[Paul Tibbets]], Major [[Theodore Van Kirk|'Dutch' Van Kirk]], Colonel [[Thomas Ferebee]] and others vigorously disputed Sweeney's account of events.<ref>Coster-Mullen, John, ''Atom Bombs: The Top Secret Inside Story of Little Boy and Fat Man'', publ. J. Coster-Mullen, End Notes, (2004)</ref>  Partly in response to ''War's End'', General Tibbets issued a revised version of his own autobiography in 1998, adding a new section on the Nagasaki attack in which he harshly criticized Sweeney’s actions during the mission.<ref name="ReferenceA"/><ref name="Tibbets, Paul W. 1998"/><ref name="Miller, Donald L. 2005 pp. 361-362"/>

In his later years Charles Sweeney performed in various air shows doing many maneuvers to awe crowds. Sweeney died at age 84 on July 16, 2004 at [[Massachusetts General Hospital]] in [[Boston]].

A short documentary featuring an audio recording of Sweeney describing the Nagasaki mission preparation and execution called [http://www.mputtre.com/id39.html "Nagasaki: The Commander's Voice"] was made in 2005. The 2002 audio recording was the last one made before his death.

==Awards==
*[[Command Pilot Wings]]
*[[Silver Star]]
*[[Air Medal]]
*[[American Campaign Medal]]
*[[Asiatic-Pacific Campaign Medal]] with one [[campaign star]]
*[[World War II Victory Medal]]
*[[Armed Forces Reserve Medal]] with two bronze hourglasses.

==See also==
{{Portal|Biography|United States Air Force}}
* [[Paul Tibbets]], Sweeney's counterpart on the mission which dropped [[Little Boy|Little boy]] on [[Hiroshima]].

==References==
{{reflist|2}}

==Bibliography==
* Brooks, Lester. ''Behind Japan's Surrender: Secret Struggle That Ended an Empire''. New York: McGraw-Hill Book Company, 1968.
* Grayling, A.C. ''Among the Dead Cities''. London: Bloomsbury, 2006. ISBN 0-7475-7671-8.
* Miller, Merle and Abe Spitzer. ''[[iarchive:WeDroppedTheA-Bomb-nsia|We Dropped the A-Bomb]]''.<ref>{{Cite book|title=We Dropped The A-Bomb|last=Spitzer|first=Abe|date=1946-01-01}}</ref> New York: Thomas Y. Crowell Company, 1946.
* Olivi, Lt.Col.USAF(Ret) Fred J. ''Decision At Nagasaki: The Mission That Almost Failed''. Privately Printed, 1999. ISBN 0-9678747-0-X.
* Sweeney, Maj.Gen.USAF(Ret) Charles with James A. Antonucci and Marion K. Antonucci. ''War's End: an Eyewitness Account of America's Last Atomic Mission''. New York: Avon Books, 1997. ISBN 0-380-97349-9.
* Tomatsu, Shomei. ''11:02 Nagasaki''. Tokyo, Japan: Shashin Dojinsha, 1966.

==External links==
{{Refbegin}}
* [http://alsos.wlu.edu/qsearch.aspx?browse=people/Sweeney,+Charles Annotated bibliography for Charles Sweeney from the Alsos Digital Library for Nuclear Issues]
* {{webarchive |url=https://web.archive.org/web/20040208224517/http://www.af.mil/bios/bio.asp?bioID=7317 |date=February 8, 2004 |title=Official USAF biography BG Charles W. Sweeney }}
* [http://www.uwosh.edu/faculty_staff/earns/olivi.html Reflections from above: an American pilot's perspective on the mission which dropped the atomic bomb on Nagasaki]
* [http://www.atomicarchive.com/Docs/Hiroshima/Nagasaki.shtml Eyewitness account of atomic bombing over Nagasaki, by William Laurence, New York Times]
* {{YouTube|B72850ax58c|Sweeney explains to Reed Irvine how, incredibly, he and Paul Tibbetts became the first Americans into Japan after the bombing}}.

{{Refend}}

{{Authority control}}

{{DEFAULTSORT:Sweeney, Charles}}
[[Category:1919 births]]
[[Category:2004 deaths]]
[[Category:Recipients of the Air Medal]]
[[Category:United States Air Force generals]]
[[Category:People from Quincy, Massachusetts]]
[[Category:People associated with the atomic bombings of Hiroshima and Nagasaki]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:United States Army Air Forces officers]]
[[Category:People from Lowell, Massachusetts]]
[[Category:Recipients of the Silver Star]]