{|{{Infobox aircraft begin
 |name=Mk.4
 |image=RAF Museum Cosford - DSC08308.JPG
 |caption=Martin-Baker Mk.4P on display at the [[Royal Air Force Museum Cosford]]
}}
|}

The '''Martin-Baker Mk.4''' is a British [[ejection seat]] designed and built by [[Martin-Baker]]. Introduced in the  1950s, the Mk.4 has been installed in combat and training aircraft worldwide.<ref>Philpott 1989, pp. 77-78.</ref>

==History==
The Mk.4 seat was designed as an improved, lightweight version of earlier Martin-Baker seats for installation in a range of lighter, smaller aircraft types.<ref name="MBHIW">[http://www.martin-baker.com/products/Ejection-Seats/Mk--1-to-Mk--9/Mk--4.aspx Martin-Baker Mk.4 fact sheets] www.martin-baker.com Retrieved: 15 December 2011</ref> Improvements included a single combined seat and parachute quick release fastener (QRF) and a snubber mechanism to allow crews to lean forward without loosening the harness.<ref name="MBHIW"/> The first successful ejection using a Mk.4 seat took place in March 1957, the aircraft involved being a [[Fiat G.91]].<ref>Philpott 1989, p. 77.</ref>

==Operation sequence==
Operating either the seat pan or face blind firing handles initiates aircraft [[Aircraft canopy|canopy]] jettison, the main gun located at the rear of the seat then fires, the main gun is a telescopic tube with two explosive charges that fire in sequence. As the seat moves up its guide rails an emergency oxygen supply is activated and personal equipment tubing and communication leads are automatically disconnected, leg restraints also operate.<ref name="MBHIW"/>

A steel rod, known as the drogue gun, is fired and extracts two small drogue parachutes to stabilise the seat's descent path. A [[Atmospheric pressure|barostatic]] mechanism prevents the main parachute from opening above an [[altitude]] of 10,000&nbsp;ft (3,000&nbsp;m) A time delay mechanism operates the main parachute below this altitude in conjunction with another device to prevent the parachute opening at high speed. The seat then separates from the occupant for a normal parachute descent, a manual separation handle and [[Ripcord (skydiving)|ripcord]] is provided should the automatic system fail.<ref name="MBHIW"/>

==Applications==
The Mk.4 ejection seat has been installed in over 35 aircraft types including the following:
<br>''List from Martin-Baker.''<ref name="MBHIW"/>
{{colbegin||25em}}
*[[Aermacchi MB-326]]
*[[BAC Strikemaster]]
*[[Breguet Taon]]
*[[Dassault Étendard IV]]
*[[Dassault Mirage III]]
*[[Dassault Mystère]]
*[[Dassault Ouragan]]
*[[Dassault/Dornier Alpha Jet]]
*[[de Havilland Vampire]]
*[[Dornier Do 29]]
*[[English Electric Canberra]]
*[[English Electric Lightning]]
*[[Fouga CM.170 Magister]]
*[[HAL Ajeet]]
*[[HAL HF-24 Marut]]
*[[HAL Kiran]]
*[[Hawker Hunter]]
*[[Helwan HA-300]]
*[[Nord 1500 Griffon]]
*[[Nord Gerfaut]]
*[[SEPECAT Jaguar]]
*[[SNCASE SE-212 Durandal]]
*[[SNCASO Trident]]
*[[Sud Aviation Vautour]]
{{colend}}

==Seats on display==
A Martin-Baker Mk.4P seat is on display at the [[Royal Air Force Museum Cosford]].<ref>[http://navigator.rafmuseum.org/results.do?view=detail&db=object&pageSize=1&id=71895 Royal Air Force Museum London - Martin-Baker Mk.4P] navigator.rafmuseum.org Retrieved: 15 December 2011</ref>

==Specifications (Mk.4)==
*Maximum operating height: 50,000&nbsp;ft (15,240&nbsp;m)
*Minimum operating height: Ground level
*Minimum operating speed: 90 [[Knot (unit)|knots]] [[indicated airspeed]] (KIAS)
*Maximum operating speed: 600 KIAS

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* * Philpott, Bryan. ''Eject!! Eject!!''. Shepperton, Surrey. Ian Allan Ltd., 1989. ISBN 0-7110-1804-9
{{refend}}

==External links==
{{Commons category}}
*[http://www.flightglobal.com/pdfarchive/view/1954/1954%20-%203119.html "Getting Away With It"] a 1954 ''Flight'' article

{{Martin-Baker ejection seats}}

[[Category:Martin-Baker ejection seats]]