{{EngvarB|date=July 2016}}
{{Use dmy dates|date=July 2016}}
{{Good article}}
{{Infobox cattle breed
| name          = Belted Galloway
| image         = Belted Galloway at Gretna Green.jpg
| image_size    = 250px
| image_alt     = A belted galloway with a large white stripe down the middle of the cattle.
| image_caption = A Belted Galloway in pasture
| status        = Watched
| country       = [[Scotland]]
| distribution  =
| use           = Beef
| nickname      = {{Unbulleted list|Beltie|Oreo Cow<ref name="oreocow">[http://homestead.org/VictoriaVarga/BeltedGallowayCattle.htm Belted Galloways: The "Oreo-Cookie" Cow]</ref>|Panda Cow}}
| maleweight    = 770-850&nbsp;kg
| femaleweight  = 450-675&nbsp;kg
| color     =  Black with large white stripe around middle.
| horn     = Polled
| subspecies = Taurus
| note          =
}}
The '''Belted Galloway''' is a heritage breed of [[beef cattle]] originating from [[Galloway]] on the west side of southern [[Scotland]] <nowiki/>and adapted to living on the poor [[Upland and lowland|upland pastures]] and windswept [[moorland]]s of the region.  The exact origin of the breed is unclear although it is often surmised that the white belt for which they are named and that distinguishes the breed from the native black [[Galloway cattle]] may be the result of [[cross breeding]] with Dutch [[Dutch Belted|''Lakenvelder'']] belted cattle.

Belted Galloways are primarily [[Animal husbandry|raised]] for their quality [[Marbled meat|marbled beef]], although they are sometimes [[Milk cow|milked]] and purchased to adorn pastures due to their striking appearance.

==Breed history==
The origin of the white belt is unknown, but generally presumed to have come from cross breeding with ''Lakenvelder'' (Dutch Belted) cattle.<ref name=":0">[http://www.ansi.okstate.edu/breeds/cattle/beltedgalloway/index.html Oklahoma State University breed profile]</ref> A [[Herd book|Polled Herd Book]] was started in 1852 which registered both [[Angus cattle|Aberdeen-Angus]] and [[Galloway cattle|Galloways]]. Galloway breeders acquired their own herd book in 1878. The [[Dun gene|Dun]]<ref group=note>Dun is a light brown colour caused by the [[Dun gene]]</ref> and Belted Galloway Association was formed in Scotland in 1921, and in 1951 the name of the organisation was changed to the Belted Galloway Society and dun cattle were no longer registered. It also keeps and records pedigrees for Belted Galloways and oversees the registration of White and Red Galloways.<ref>{{Cite web|url = http://www.beltedgalloways.co.uk/history-2/|title = Belted Galloway Society - History|date = |accessdate = 20 June 2015|website = |publisher = |last = |first = }}</ref><ref name=":3">{{Cite web|url = http://www.thecattlesite.com/breeds/beef/9/belted-galloway/overview/|title = Belted Galloway Cattle|date = |accessdate = 20 June 2015|website = The Cattle Site|publisher = |last = |first = }}</ref>

Currently in the UK there is a thriving breeding programme overseen and guided by the Belted Galloway Cattle Society. Belted Galloways were first brought to the United States by Mrs. McLean of East Kortright, New York. The "American Belted Galloway Breeders Society" was formed in the United States on 1 July 1951 by Harry A. Prock of [[Whitemarsh, Pennsylvania]], Gordon Green of Quebec, Canada and Charles C. Wells of [[East Lansing, Michigan]].  It is now known as "The US Belted Galloway Society Inc."<ref>{{Cite web|url = http://beltie.org/history-attributes.php|title = History and Attributes of Belted Galloway Cattle|date = |accessdate = 20 June 2015|website = Beltie.org|publisher = |last = |first = }}</ref>

==Characteristics==
[[Image:Belted galloways bei neukoog nordstrand.JPG|thumb|Several Belted Galloways]]
Galloway cattle are naturally [[Polled livestock|polled]] (without horns). The most visible characteristics are its long hair coat and the broad white belt that completely encircles its body. Its coarse outer coat helps shed the rain, and its soft undercoat provides insulation and waterproofing, enabling the breed to spend winter outside.<ref name=":2">{{Cite web|url = https://www.rbst.org.uk/Rare-and-Native-Breeds/Cattle/Belted-Galloway|title = Rare Breeds Survival Trust&nbsp;— Belted Galloway|date = |accessdate = 20 June 2015|website = Rare Breeds Survival Trust|publisher = |last = |first = }}</ref> Black Belteds are the most prominent, but Dun and Red Belteds are also recognised by breed societies, the latter being comparatively rare and sought after. A female Belted Galloway cannot be registered in the [[Herd book|Herd Book]] if it has white above the [[dewclaw]] other than the belt, but can be registered in the Appendix.  A bull can only be registered in the Herd Book if it has no other white than the belt.<ref>{{Cite web|url = http://www.beltedgalloways.co.uk/registration/births-registrations-transfers/|title = Belted Galloway Society - Registration Criteria|date = |accessdate = 25 August 2015|website = Belted Galloway Society|publisher = |last = |first = }}</ref><!--<ref name=":1" />-->

The dun colour is caused by a mutation in the [[PMEL (gene)|PMEL]] gene, the same mutation that causes dun and silver dun in [[Highland cattle]].<ref>Schmutz, S. M. and Dreger, D. L. 2013. Interaction of MC1R and SILV alleles on solid coat colors in Highland Cattle. Animal Genetics 44:9-13.</ref>  The black and red coat colours are caused by the same alleles of the MC1R gene, E<sup>D</sup> for black and e/e for red, as in most other breeds of cattle.

Bulls weigh from {{Conv|1700|lb}} to {{Conv|2300|lb}}, with the average being around {{Conv|1800|lb}}. Cows weigh from {{Conv|1000|lb}} to {{Conv|1500|lb}}, with the average being around {{Conv|1250|lb}}. Calves generally weigh around {{Conv|70|lb}}.<ref>{{Cite web|url = http://homestead.org/VictoriaVarga/BeltedGallowayCattle.htm|title = Belted Galloway Breed Information|date = |accessdate = 25 August 2015|website = Homestead.org|publisher = |last = |first = }}</ref> Belted Galloways are generally of a quiet temperament, but still maintain a maternal instinct and will protect calves against perceived threats.<ref name=":2" /><ref>{{Cite web|url = http://www.britannicrarebreeds.co.uk/breedinfo/cow_beltedgalloway.php|title = Britannic Rare Breeds&nbsp;— Belted Galloway|date = |accessdate = 20 June 2015|website = |publisher = |last = |first = }}</ref>

They are well-suited for rough grazing land and will utilise coarse grasses other breeds would shun. They are able to maintain a good condition on less than ideal pasture, and produce high-quality beef on grass alone.<ref name=":3" />

== Population ==
Belted Galloways, also informally known as ''Belties'', are currently listed with the [[American Livestock Breeds Conservancy]] as a "recovering" breed,<ref name=":1">{{Cite web|url = http://www.livestockconservancy.org/index.php/heritage/internal/belted-galloway|title = American Livestock Conservancy&nbsp;— Belted Galloway|date = |accessdate = 20 June 2015|website = |publisher = |last = |first = }}</ref> which means there are more than 2,500 annual registrations in the United States and a global population greater than 10,000, but they were once on the "watch" list.<ref>{{Cite web|url = http://www.livestockconservancy.org/index.php/heritage/internal/parameters-cpl|title = Livestock Conservancy - Parameters for Classification|date = |accessdate = 25 August 2015|website = |publisher = |last = |first = }}</ref>  18,390 cattle were registered in the US in 2015.<ref>{{Cite web|url = http://beltie.org/PDFs/newsletter/April-2015.pdf|title = Belted Galloway Society - Newsletter|date = April 2015|accessdate = 25 August 2015|website = |publisher = |last = |first = |page = 4}}</ref>  The breed is raised in areas where the climate is similar to its native Scotland, like the [[Central coast of california|Central Coast of California]].

In the UK in 2007, they were formally removed from the [[Rare Breeds Survival Trust]]'s watch list, having recovered sufficiently from the devastation of the [[foot and mouth]] crisis of the early 2000s, to have exceeded 1500 registered breeding females.<ref>{{Cite web|url = http://www.thatsfarming.com/news/belted-galloway-cattle?section=news&filters%5Bnews%5D=agribusiness|title = That is Farming - Belted Galloway Facts|date = |accessdate = 25 August 2015|website = |publisher = |last = |first = }}</ref>

==Gallery==
<gallery mode="packed">
File:Belted galloway bull.jpg|Belted Galloway bull in Denmark
File:Belted galloway bei neukoog nordstrand.JPG|Belted Galloway in [[Nordfriesland]]
File:Belted Galloway P8250176.JPG|Beltie calf in [[Nordfriesland]]
</gallery>

==Notes==
{{reflist|group=note}}

==References==
{{reflist}}

==External links==
{{Commons|Belted Galloway}}
* [http://southwestbelties.com Southwest Belted Galloway Association]
* [http://www.discushatchery.com/vidbeltedgallowaycows.html Belted Galloway Cow Video - Gwynnbrook Farm]
* [http://www.beltie.org Belted Galloway Society of the United States of America]
* [http://www.beltedgalloway.org.au Australian Belted Galloway Association]
* [http://www.gallowaycattle.com.au Galloway Cattle & Beef Marketing Association (Australia)]
* [https://web.archive.org/web/20081014010013/http://www.nzgalloway.co.nz/client/galloway_history Galloway Cattle Society of New Zealand]
* [http://www.galloway.ca/ Canadian Galloway Association]
* [http://homepage.usask.ca/~schmutz/Galloway.html Coat Colours of the Belted Galloway]

{{British livestock|state=collapsed}}

[[Category:Conservation Priority Breeds of the Livestock Conservancy]]
[[Category:Cattle breeds originating in Scotland]]
[[Category:Dairy cattle breeds]]
[[Category:Cattle breeds]]