{{Infobox journal
| title = The Journal of Ecclesiastical History
| cover = [[File:The Journal of Ecclesiastical History.jpg]]
| editor = [[Alec Ryrie]], [[James Carleton Paget]]
| discipline = [[Church history]]
| abbreviation = J. Ecclesiast. Hist.
| publisher = [[Cambridge University Press]]
| country =
| history = 1950-present
| frequency = Quarterly
| website = http://www.journals.cambridge.org/ech
| link1 = http://journals.cambridge.org/action/displayIssue?jid=ECH&tab=currentissue
| link1-name= Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=ECH
| link2-name = Online archive
| ISSN = 0022-0469
| eISSN = 1469-7637
| LCCN = 58020553
| OCLC = 01604275
}}
'''''The Journal of Ecclesiastical History''''' is a quarterly [[peer-reviewed]] [[academic journal]] published by [[Cambridge University Press]]. It was established in 1950<ref>{{cite book |last1=Hein |first1=David |last2=Shattuck |first2=Gardiner H. |title=The Episcopalians |url=https://books.google.com/books?id=_iK5VggudLkC&pg=PA333 |year=2004 |publisher=Greenwood |isbn=9780313229589 |pages=333–34}}</ref> and covers all aspects of the history of the [[Christian Church]]. It deals with the Church both as an institution and in its relations with other [[religion]]s and society at large. The journal publishes articles and [[book review]]s.

The current [[editors-in-chief]] are [[Alec Ryrie]] ([[Durham University]]) and [[James Carleton Paget]] ([[University of Cambridge]]). The journal is regarded as highly authoritative in its field, and is compared to the American ''[[American Society of Church History#Church History|Church History]]''.<ref>{{cite book |last=Bradley |first=James E. |title=Church History: An Introduction to Research, Reference Works, and Methods |url=https://books.google.com/books?id=RpuVtoUQuy8C&pg=PA90 |year=1995 |publisher=Wm. B. Eerdmans |isbn=9780802808264 |page=90}}</ref>

==References==
{{Reflist}}

== External links ==
*{{Official website|http://www.journals.cambridge.org/ech}}

{{DEFAULTSORT:Journal of Ecclesiastical History, The}}
[[Category:Publications established in 1950]]
[[Category:Journals about ancient Christianity]]
[[Category:Quarterly journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:1950 establishments in the United Kingdom]]
[[Category:English-language journals]]