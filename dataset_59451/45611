{{Infobox military person
|name=Elmer Fowler Stone
|birth_date=1887
|death_date= {{death year and age|1936|1887}}
|birth_place= [[Livingstone, New York]]
|death_place=
|placeofburial=
|placeofburial_label= Place of burial
|image= Elmer Fowler Stone.jpg
|caption=Commander Elmer Fowler Stone
|nickname=
|allegiance= [[United States|United States of America]]
|branch= [[United States Coast Guard]]
|serviceyears=1910-1936
|rank=Commander
|commands=
|unit=
|battles=
|awards= [[Navy Cross (United States)|Navy Cross]]<br/>[[NC-4 Medal]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]] (UK)
|relations=
|laterwork=
}}
'''Elmer "Archie" Fowler Stone''' (January 22, 1887 - May 20, 1936) was a [[United States]] [[naval aviator]] and a [[commander]] in the [[United States Coast Guard]].

==Biography==
Stone was born in [[Livingstone, New York]] and grew up in [[Norfolk, Virginia]]. He joined the [[U.S. Revenue Cutter Service]] as a cadet at the [[United States Coast Guard Academy|Revenue Cutter Service School of Instruction]] on April 28, 1910.<ref name=Noble69>Noble, p 69</ref>

Elmer Stone was a [[United States Coast Guard]] aviation pioneer; in early 1915 Stone and another officer [[Norman B. Hall]], were the first to suggest that the Coast Guard develop an aviation capability. With the encouragement of their commanding officer, Captain [[Benjamin M. Chiswell]] of the {{USRC|Onondaga|1898|6}}, Lieutenants Stone and Hall approached the [[Curtiss Flying School]] in [[Newport News, Virginia]] about using aircraft in air-sea rescue operations, and participated in experimental flights in a [[Curtiss Model F]] [[flying boat]]. During the summer of 1915, Stone and Hall performed scouting patrols for ''Onondaga'', assisting in search missions that the cutter was assigned.<ref name=Evans188>Evans, p 188</ref> On March 28, 1916 he was assigned as a student aviator at the [[United States Navy]] flight facility in [[Pensacola, Florida]].<ref name=Register1916>Register of the officers, vessels and stations of the United States Coast Guard, July 1, 1916, p 18</ref> The same year he also studied aeronautical engineering at the Curtiss factory. On April 10, 1917, Stone became the Coast Guard's first aviator upon graduating from flight training at Pensacola and was appointed as Coast Guard Aviator No. 1 and Naval Aviator No. 38. On October 12, 1917, Stone was assigned to the [[Naval Air Station Rockaway|U.S. Navy Aeronautic Station]] at [[Rockaway, New York]].<ref name=Register1918>Register of the officers, vessels and stations of the United States Coast Guard, January 1, 1918, p 22</ref>

In May 1919 First Lieutenant Elmer F. Stone was the pilot on the first successful [[transatlantic flight]] on [[NC-4]] with Lieutenant Commander A. C. Read, USN, the mission commander and navigator. After the historic flight, he was awarded the British [[Air Force Cross (United Kingdom)|Air Force Cross]] by the British government (June 9, 1919), and received a promotion to the temporary rank of captain on 25 September 1919. He was awarded the [[Navy Cross (United States)|Navy Cross]] and [[Congressional Medal of Achievement]] for "distinguished service in making the first successful trans-Atlantic flight" on November 11, 1920.

For the next six years he worked with the Navy's Bureau of Aeronautics where he assisted in the development of the catapults and arresting gear of the new aircraft carriers [[USS Lexington (CV-2)|USS ''Lexington'']] and {{USS|Saratoga|CV-3|6}}, equipment still used on aircraft carriers to this day. He continued to promote aviation in the U.S. Coast Guard during the 1920s,  He collaborated with the [[Curtiss Aeroplane and Motor Company]] on development of airborne "motor lifeboats" for the explicit purpose of lifesaving missions, and in this is one of the pioneers of modern air-sea rescue. He also commanded a former [[destroyer]] that was turned over to the [[U.S. Coast Guard]] and used in the enforcement of [[Prohibition in the United States|Prohibition]].

After a tour at sea, Stone became the commanding officer of the Coast Guard Aviation Unit at [[Cape May, New Jersey]], where he continued to develop his skill at making open-ocean landings. On April 5, 1933, Stone put his open-ocean landing skills to the test when the Navy [[dirigible]] [[USS Akron (ZRS-4)|''Akron'']] went down off the Atlantic coast in a storm with only three survivors of the 76 aboard, Stone was the only pilot available willing to attempt a landing in the heavy seas. He accomplished this successfully, but was too late to save any more lives.<ref name=Beard111>Beard, p 111</ref> On December 20, 1934, he broke the world seaplane speed record, 191 miles per hour over a 3 kilometer test course.

Commander Stones' last duty was as the commanding officer of the Air Patrol Detachment in San Diego. He died of a heart attack while on duty on May 26, 1936, while inspecting a new aircraft and was buried in [[Arlington National Cemetery]]. Stone was a pivotal figure in the establishment and development of aviation for the Coast Guard and the Navy and was a favorite of many of the famous aviation figures of the day, including [[Eddie Rickenbacker]], aircraft designers [[Anthony Fokker]], [[Igor Sikorsky]], and [[Alexander P. de Seversky]].

Commander Elmer "Archie" Stone was inducted into the United States [[Naval Aviation Hall of Honor]] on May 5, 1983, and is also a member of the U.S. Coast Guard Aviation Hall of Fame.

{{Portal bar|United States Coast Guard|Biography}}

==Notes==
;Citations
{{reflist}}
*{{cite web
 | title=Commander Elmer Fowler Stone
 | publisher=United States Coast Guard web site
 | url=http://www.uscg.mil/history/people/Elmer_Stone.asp
 | accessdate=2008-06-14 }}
*{{cite web
 | title=Official orders, correspondence and documents related to E. F. Stone's aviation career
 | publisher=United States Coast Guard web site
 | url=http://www.uscg.mil/history/people/AC_legacy_of_e_stone.pdf
 |format=PDF| accessdate=2006-12-15 }}
;References used
{{refbegin}}
* {{cite book|title=Register of the officers, vessels and stations of the United States Coast Guard, July 1, 1916|year=1916|publisher=U.S. Government Printing Office}}
* {{cite book|title=Register of the officers, vessels and stations of the United States Coast Guard, January 1, 1918|year=1918|publisher=U.S. Government Printing Office}}
* {{cite book|last=Beard|first=Barrett Thomas|title=Wonderful Flying Machines: A History of U.S. Coast Guard Helicopters|year=1996|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=978-1-55750-086-1}}
* {{cite book|last=Evans|first=Stephen H.|title=The United States Coast Guard 1790–1915: A Definitive History|year=1949|publisher=Naval Institute Press, Annapolis, Maryland|isbn=}}
* {{cite book|last=Noble|first=Dennis L.|title=Historical Register U.S. Revenue Cutter Service Officers, 1790–1914|year=1990|publisher=Coast Guard Historian's Office, U.S. Coast Guard Headquarters, Washington, DC}}
* {{cite web
 | url =https://www.uscg.mil/history/articles/StoneElmerApr2010.pdf
 | title = Elmer F. Stone '13 and the Pioneering Role of Coast Guard Academy Graduates in Early Naval and Coast Guard Aviation
 | last = Thiesen, Ph.D.
 | first =William H.
 | date =April 2010
 | website =USCG.mil
 | publisher =The Foundation for Coast Guard History
 | access-date =2016-05-11
 | format=pdf}}
{{refend}}

==External links==
* [http://www.uscg.mil/history/aviationindex.asp U.S. Coast Guard web site - Aviation History index]
* [http://www.uscg.mil/history/uscghist/aviationchron.html Chronology of Coast Guard Aviation history]
* {{Find a Grave|9137}}
{{Portal|Biography|United States Coast Guard}}

{{Authority control}}

{{DEFAULTSORT:Stone, Elmer}}
[[Category:1887 births]]
[[Category:1936 deaths]]
[[Category:American aviators]]
[[Category:Aviators from New York]]
[[Category:United States Coast Guard Aviation]]
[[Category:United States Coast Guard officers]]
[[Category:United States Naval Aviators]]
[[Category:Recipients of the Navy Cross (United States)]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Congressional Gold Medal recipients]]
[[Category:Burials at Arlington National Cemetery]]