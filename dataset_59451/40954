{{Infobox person
|name          = Francis C. Speight
|image         = 
|image_size    = 
|caption       = 
|birth_name    = 
|birth_date    = {{Birth date|1816|5|16}}
|birth_place   = [[Ithaca, New York]], [[United States]]
|death_date    = {{death date and age|1877|3|20|1816|5|16}}
|death_place   = [[Manhattan, New York]]
|death_cause   = [[Pneumonia]]
|resting_place = [[Cemetery of the Evergreens, Brooklyn|Evergreen Cemetery]]
|resting_place_coordinates = 
|residence     = 
|nationality   = [[United States|American]]
|other_names   = 
|known_for     = [[NYPD]] [[police inspector]] and participant in the [[New York City Police Riot|Police Riot of 1857]] and [[New York Draft Riot|New York Draft Riots of 1863]].
|education     =
|alma_mater    = 
|employer      = [[New York City Police Department]] 
|occupation    = Police officer 
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = 
|weight        = 
|term          = 
|predecessor   = 
|successor     = 
|party         = [[Whig Party (United States)|Whig Party]]
|boards        = 
|religion      = [[Episcopal Church (United States)|Episcopalian]]
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Francis C. Speight''' (May 16, 1816 – March 20, 1877) was an American law enforcement officer and [[police inspector]] for the [[New York City Police Department]]. A noted crimefighter, credited for running out the criminal elements from [[Manhattan]]'s Eighteenth and Nineteenth Wards in the 1850s, he also took part in the [[New York City Police Riot|Police Riot of 1857]] and [[New York Draft Riot|New York Draft Riots of 1863]]. Prior to the outbreak of violence at the [[Third Avenue]] draft office, Speight was the only officer to maintain control of his station, the [[Broadway (Manhattan)|Broadway]] draft office, during the early hours of the riots.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 117) ISBN 1-56025-275-8</ref>

==Biography==

===Early life and career===
Francis C. Speight was born in [[Ithaca, New York]] on May 16, 1816. He first arrived in New York City in 1830 where he was apprenticed as a ship-smith until he was 21 years old. A strong supporter of the [[Whig Party (United States)|Whig Party]], he became involved in local politics as a young man and held considerable influence in the Eleventh Ward as a political organizer for the party. This was most evident during the [[United States presidential election, 1840|1840 U.S. presidential election]] which resulted in the successful election of [[William Henry Harrison]].<ref name="Article">"[https://query.nytimes.com/mem/archive-free/pdf?res=9B05E7DF123FE63BBC4951DFB566838C669FDE Death Of Inspector Speight. The End Of A Busy Life – Over Thirty Years In The Public Service – An Active And Efficient Officer Action Of The Police Board]". <u>New York Times.</u> 21 Mar 1877</ref>

In 1845, Speight was appointed to the police force by Alderman Jance D. Oliver and assigned to the Fifteenth Ward. His success against the criminal elements in the area soon earned him promotion to second lieutenant, a position being the modern equivalent to police sergeant. Speight's term of office expired in 1849. With the Whigs not then in power, he was not reappointed and briefly retired to private life.<ref name="Article"/>

Shortly thereafter, Speight was appointed Inspector of Customs by then Collector of the Port Hugh Maxwell. He held that position until 1853 and returned to the Municipal police the following year by appointment from the Board of Police Commissioners, then including Mayor Westervelt, City Judge Buebe and Recorder Tillon. On June 3, 1854, Speight received his commission as police captain and was made commander of the recently created Twenty-First Ward, formerly comprising the Eighteenth and Nineteenth Wards.<ref name="Article"/>

===Police captain of the 21st Precinct===
In the area where the Twenty-First Precinct was located, a particularly violent group of ''"rowdies"'' were active and where they based their criminal activities for nearly a decade. Reinforced by Fire Department roughs, these criminals had been a constant source of trouble to the local police. Speight decided to take on the criminal gang and headed police 
squads sent out at every disturbance caused by them. On one of these occasions, Speight was seriously injured after being struck on the forehead with a blunt object and confined to his bed for several weeks. The scars he received from the injury would remain for the rest of his life. Using aggressive and heavy-handed tactics, Speight directly confronted the street gang and was able to successfully drive them out of the district.<ref name="Article"/>

===Police Riot of 1857 and the New York Draft Riot of 1863===
Speight remained in command of the Twenty-First Ward until the formation of the [[New York City Police Department|Municipal Police Department]] in 1857 and turned his office to the new organization. He was among the first senior police officials to join the Metropolitans, Inspector [[Daniel C. Carpenter]] and [[George W. Dilks]] among others, and he was returned to his former post. Many of these had been former Whigs who now aligned themselves with the New York Republican Party. He also took part in the [[New York City Police Riot|Police Riot of 1857]] assisting in the arrest of Mayor [[Fernando Wood]].<ref name="Article"/>

During the early hours of the [[New York Draft Riot|New York Draft Riot of 1863]], upon news of crowds gathering at the [[Third Avenue]] draft office and in [[Central Park]], Police Superintendent [[John Alexander Kennedy]] dispatched sixty-nine patrolmen under the command of Speight and Sergeants Wade, Wolfe, John Mangin and Robert McCredie to guard the [[Broadway (Manhattan)|Broadway]] draft office. No trouble occurred there under Speight's watch and drafting went ahead as scheduled and uninterrupted until noon when it was adjourned for twenty-four hours. The same force under Captain [[Galen T. Porter]] had been overwhelmed by the mob and forced to flee from the building after a brief siege when it was set on fire with assistance from members of the Volunteer Engine Company, No. 33 ("The Black Joke"). Speight would be on constant duty throughout the riots.<ref name="Asbury"/>

===Later years and death===
On March 20, 1877, Speight died from [[pneumonia]] at his home on Eighty-Third Street. Although it was known that Speight had been ill, his sudden death was unexpected and came as a great shock to the police force.<ref name="Article"/>

His funeral was held days later at the [[Church of the Transfiguration]], popularly known at the time as "The Little Church Round the Corner", and was attended by members of the [[NYPD Police Commissioner|Board of Police Commissioners]] Williams F. Smith, [[Joel B. Erhardt]], Dewitt C. Wheeler and Sidney P. Nichols, ex-Police Commissioners Barr and Voorhis, former Police Superintendent [[George Washington Matsell]], Chief Police Clerk [[Seth C. Hawley]], Superintendent [[George Washington Walling|George W. Walling]] and all [[police captain]]s including Captain [[John Mangin]] of the [[Yonkers, New York|Yonkers Police Department]]. Politicians and city officials [[Charles F. Maclean]], [[John J. Morris]], Thomas "Big Tom" Brennan and George Starr were also in attendance.<ref name="Article2">"[https://query.nytimes.com/mem/archive-free/pdf?res=9A03E1DB163EE73BBC4C51DFB566838C669FDE The Funeral Of Inspector Speight]". <u>New York Times.</u> 24 Mar 1877</ref>

His body was escorted by aids and personal friends from his home in Carmansville to his church, his pallbearers being Police Inspectors McDermott and [[Thomas W. Thorn]]e, Captains Petty, Caffrey, Hedden, Bennett, Davis and Mount, and services performed by Rev. [[George W. Houghton]] and [[E. C. Houghton]]. The [[hymn]] "Rock of Ages" was sung by the [[church choir]] and a police battalion under Inspector [[George W. Dilks]] formed on [[29th Street (Manhattan)|Twenty-Ninth Street]] in front of the church. At the service's conclusion, a band played a [[dirge]] and the battalion presented arms as the casket was taken to [[Cemetery of the Evergreens, Brooklyn|Evergreen Cemetery]] for burial.<ref name="Article2"/>

==References==
{{Reflist}}

==Further reading==
*Barnes, David M. ''The Draft Riots in New York, July, 1863: The Metropolitan Police, Their Services During Riot Week, Their Honorable Record''. New York: Baker & Godwin, 1863.
*Bernstein, Iver. ''The New York City Draft Riots: Their Significance for American Society and Politics in the Age of the Civil War''. New York: Oxford University Press, 1991.
*Cook, Adrian. ''The Armies of the Streets: The New York City Draft Riots of 1863''. Lexington: University Press of Kentucky, 1974.
*Costello, Augustine E. ''Our Police Protectors: History of the New York Police from the Earliest Period to the Present Time''. New York: A.E. Costello, 1885. 
*Hickey, John J. ''Our Police Guardians: History of the Police Department of the City of New York, and the Policing of Same for the Past One Hundred Years''. New York: John J. Hickey, 1925.
*McCague, James. ''The Second Rebellion: The Story of the New York City Draft Riots of 1863''. New York: Dial Press, 1968.

{{DEFAULTSORT:Speight, Francis C.}}
[[Category:1816 births]]
[[Category:1877 deaths]]
[[Category:New York City Police Department officers]]
[[Category:People from Manhattan]]
[[Category:People from Ithaca, New York]]
[[Category:Deaths from pneumonia]]
[[Category:Infectious disease deaths in New York]]
[[Category:Burials at the Cemetery of the Evergreens]]