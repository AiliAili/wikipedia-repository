{{Infobox journal
| title         = Catholic Historical Review
| cover         = [[File:The Catholic Historical Review.gif]]
| editor        = Nelson Minnich
| discipline    = [[History]]
| abbreviation  =
| publisher     = [[Catholic University of America Press]]
| country       = United States
| frequency     = Quarterly
| history       = 1915–present
| openaccess    =
| license       =
| impact        =
| impact-year   =
| website       = http://cuapress.cua.edu/journals/chr.cfm
| link1         =
| link1-name    =
| link2         =
| link2-name    =
| RSS           =
| atom          =
| JSTOR         = 00088080
| OCLC          =  1553555
| LCCN          =
| CODEN         =
| ISSN          = 0008-8080
| eISSN         =
| boxwidth      =
}}
'''''The Catholic Historical Review''''' is the official organ of the [[American Catholic Historical Association]]. It was established at [[The Catholic University of America]] in 1915 by [[Peter Guilday]] and is published quarterly by The [[Catholic University of America Press]]. The first issue contained a foreword by Cardinal [[James Gibbons]] who wrote of the journal that "I bespeak for it a generous welcome by the thoughtful men and women of the country, and bestow my blessing on the unselfish, zealous labors of the devoted Faculty of the Catholic University."<ref>James Gibbons ''The Catholic Historical Review'' volume 1, issue 1, page 3, 1915</ref> Guilday was succeeded as editor by [[John Tracy Ellis]]. The journal publishes articles and book reviews received in all areas of [[history of Christianity|ecclesiastical history]]. In addition to being available in print form, ''The Catholic Historical Review'' is also available electronically through [[Project MUSE]].

==Reference list==
{{reflist}}

==Further reading==
* Gleason, Philip. "The Historiography of American Catholicism as Reflected in The Catholic Historical Review, 1915–2015." ''Catholic Historical Review'' 101#2 (2015) pp: 156-222.
* {{cite book |last=Stieg |first=Margaret F. |title=The Origin and Development of Scholarly Historical Periodicals |location=Tuscaloosa |publisher=University Alabama Press |year=1986 |isbn=0-8173-0273-5 |chapter=Topical Specialization: The ''Zeitschrift für Sozial- und Wirtschaftsgeschichte'', the ''Catholic Historical Review'', and the ''Journal of the History of Ideas'' |pages=103–123 }}

==External links==
*{{Official website|http://cuapress.cua.edu/journals/chr.cfm}}

{{DEFAULTSORT:Catholic Historical Review, The}}
[[Category:Publications established in 1915]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Religion history journals]]
[[Category:Catholic University of America]]
[[Category:Academic journals published by university presses]]


{{Catholicism-stub}}
{{reli-journal-stub}}