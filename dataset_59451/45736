{{Infobox person
| name        = William Wellman
| image       = William Wellman.jpg
| imagesize   = 250px
| caption     = William Wellman during filming of ''[[The High and the Mighty (film)|The High and the Mighty]]'', 1954

| birth_date   = {{birth date|1896|2|29}}
| birth_place  = [[Brookline, Massachusetts|Brookline]], [[Massachusetts]], U.S.
| death_date   = {{death date and age|1975|12|9|1896|2|29}}
| death_place  = [[Los Angeles]], [[California]], U.S.
| occupation  = [[Film director|Director]], [[actor]]
| yearsactive = 1919&ndash;1958
| notable_work = ''[[Wings (1927 film)|Wings]]''
| spouse      = [[Helene Chadwick]]<br>(1918&ndash;1923, ''divorce'')<br>Margery Chapin<br>(1925&ndash;1926, ''divorce'')<br>Marjorie Crawford<br>(1931&ndash;1933, ''divorce'')<br>[[Dorothy Coonan]] <br>(1934&ndash;1975, ''his death'')
| website     =  
}}
'''William Augustus Wellman'''  (February 29, 1896 &ndash; December 9, 1975) was an American [[film director]] notable for his work in [[Crime film|crime]], [[Adventure film|adventure]] and [[Action film|action]] genre films, often focusing on [[aviation]] themes, a particular passion. He also directed several well-regarded satirical comedies. Beginning his film career as an actor, he went on to direct over 80 films, at times co-credited as producer and consultant. In 1927, Wellman directed ''[[Wings (1927 film)|Wings]]'', which became the first film to win an [[Academy Award for Best Picture]] at the [[1st Academy Awards]] ceremony.<ref name=variety>[http://www.variety.com/article/VR1118008823.html?categoryId=25&cs=1 "Dorothy Wellman dies at 95."] ''[[Variety Magazine]]'', September 17, 2009. Retrieved: September 20, 2009.</ref>

==Early life==
Wellman's father, Arthur Gouverneur Wellman, was a [[New England]] [[Boston Brahmin|Brahmin]] of English-Welsh-Scottish and Irish descent. William was a great-great-great-great-great-grandson of [[Puritan]] [[Thomas Wellman]] who immigrated to the [[Massachusetts Bay Colony]] about 1640.<ref>Wellman, Joshua Wyman ''Descendants of Thomas Wellman'' (1918) Arthur Holbrook Wellman, Boston pp. 69-72&441-442</ref>  William was a great-great-great grandson of [[Francis Lewis]] of New York, one of the signatories to the [[United States Declaration of Independence|Declaration of Independence]]. His much beloved mother was an Irish immigrant named Cecilia McCarthy.

Wellman was expelled from Newton High School in [[Newtonville, Massachusetts]],<ref name="filmref">FilmReference.com [http://www.filmreference.com/Directors-Ve-Y/Wellman-William.html William Wellman]</ref> for dropping a [[stink bomb]] on the principal's head.<ref name="interview">[http://www.filmlinc.com/fcm/online/wellmanextra.htm "Wild Bill: William A. Wellman,"] ''Focus on Film'' #29. Retrieved: December 5, 2007.</ref> Ironically, his mother was a [[probation officer]] who was asked to address Congress on the subject of [[juvenile delinquency]].<ref name=imdbbio>Hopwood, Jon C. [http://imdb.com/name/nm0920074/bio ''William A. Wellman''.] ''IMDB'' biography. Retrieved: July 19, 2008.</ref> Wellman worked as a salesman and then at a lumber yard, before ending up playing professional ice hockey, which is where he was first seen by [[Douglas Fairbanks]], who suggested that with Wellman's good looks he could become a film actor.

==World War I==
[[File:Wellman and Nieuport.jpg|thumb|upright|Corporal William Wellman and ''Celia'' Nieuport 24 fighter c. 1917 (one of a series of aircraft all named after his mother)]]
[[File:William Wellman in a German Rumpler.jpg|thumb|William Wellman in a captured German [[Rumpler]] {From Wellman's 1918 account "Go Get Em...".}]]
In World War I Wellman enlisted in the [[List of ambulance drivers during World War I|Norton-Harjes Ambulance Corps]] as an ambulance driver.<ref name= "World War I">Silke, James R. "Fists, Dames & Wings." ''Air Progress Aviation Review'', Volume 4, No. 4. October, 1980. pp. 57-58.</ref> While in Paris, Wellman joined the [[French Foreign Legion]] and was assigned on December 3, 1917 as a fighter pilot and the first American to join N.87 ''escadrille'' in the [[Lafayette Flying Corps]] (not the sub-unit [[Lafayette Escadrille]] as usually stated),<ref>[http://www.angelfire.com/electronic/bodhidharma/flying_corps.html "Lafayette Flying Corps."] ''angelfire.com''. Retrieved: September 20, 2009.</ref><ref>[http://pagesperso-orange.fr/rdisa/html/Frames/lafayette.html "The Foundation."] ''Lafayette Flying Corps Memorial Foundation'', 2002. Retrieved: September 20, 2009.</ref> where he earned himself the nickname "Wild Bill" and received the [[Croix de guerre 1914–1918 (France)|Croix de Guerre]] with two palms.<ref>Curtiss, Thomas Quinn. [http://www.iht.com/articles/1994/02/09/cine.php "The Film Career of William Wellman." ]''[[International Herald Tribune]]'' (iht.com), February 9, 1994. Retrieved: December 5, 2007.</ref> N.87, ''les Chats Noir'' (Black Cat Group) was stationed at [[Lunéville]] in the [[Alsace-Lorraine]] sector and was equipped with [[Nieuport 17]] and later [[Nieuport 24]] "pursuit" aircraft. Wellman's combat experience culminated in three recorded "kills", along with five probables, although he was ultimately shot down by German anti-aircraft fire on March 21, 1918.<ref>[http://www.cbrnp.com/profiles/quarter1/nieuport-gallery.htm Color profile of Corporal Wellman's Nieuport 24 "Celia V"]</ref> Wellman survived the crash but he walked with a pronounced limp for the rest of his life.<ref name= "World War I"/>

Wellman's credits:<ref>[http://chroniclingamerica.loc.gov/lccn/sn83030214/1918-05-03/ed-1/seq-2/#date1=05%2F03%2F1918&index=0&rows=20&words=airman+Airman+Boche+Wellman&searchType=advanced&sequence=0&proxdistance=5&date2=05%2F03%2F1918&ortext=&proxtext=&phrasetext=&andtext=airman+boche+wellman&dateFilterType=range&page=1 New York Tribune May 3, 1918]</ref><ref>[https://books.google.com/books?id=NjsLAAAAYAAJ&pg=PA178-IA2&dq=Go+Get+them+by+William+Wellman&hl=en&sa=X&ved=0ahUKEwijgZLZy7DOAhUp74MKHeXUB9gQ6AEIHDAA#v=onepage&q=Go%20Get%20them%20by%20William%20Wellman&f=false Go, Get 'em!: The True Adventures of an American Aviator of the Lafayette Flying Corps 1918]</ref>
*January 19, 1918 a German "Rumpler" shot down in front of American lines in [[Lorraine]] by Wellman and Thomas Hitchcock. 
*January 20, 1918 a German "Rumpler" shot down near German Airfield at [[Mamy, France]]; Pilot killed/Gunner escaped
*March 8, 1918 forced 2 observers to jump from an Observation balloon {attack unsuccessful; balloon taken down-was ''not'' shot down}
*March 9, 1918 fired on a German "Rumpler" over Parroy; plane escaped but rear gunner killed.
*March 9, 1918 shot down a German "Rumpler"; killed the rear Gunner; Pilot killed by airman Ruamps. 
*March 9, 1918 shot down a German "Albatros" Pilot killed; plane fell into American Lines
*March 17, 1918 shot down at least two +one[?] German Patrol planes; not confirmed as fight took place above German lines.
*March 18, 1918 shot down a German "Rumpler"; not confirmed as fight took above German Lines.

Maréchal des Logis (Sergeant) Wellman received a medical discharge from the Foreign Legion and returned to the United States a few weeks later. He spoke at War Savings Stamp rallies in his French uniform. In September 1918 his book about French flight school and his eventful four months at the front, [https://books.google.com/books?id=A-7HAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false "Go Get 'Em!"] (written by Wellman with the help of Eliot Harlow Robinson) was published. He joined the [[United States Army Air Service]] but too late to fly for America in the war. Stationed at [[Rockwell Field]], [[San Diego, California|San Diego]], he taught combat tactics to new pilots.

==Film career==
While in San Diego, Wellman would fly to Hollywood for the weekends in his [[SPAD S.VII|Spad]] fighter, using Fairbanks' polo field in Bel Air as a landing strip.<ref name= "World War I"/> Fairbanks was fascinated with the true-life adventures of "Wild Bill"<ref name= "World War I"/> and promised to recommend him for a job in the movie business; he was responsible for Wellman being cast in the juvenile lead of ''The Knickerbocker Buckaroo'' (1919).<ref name="interview"/> Wellman was hired for the role of a young officer in ''[[Evangeline (1919 film)|Evangeline]]'' (1919), but was fired for slapping the leading lady, the actress [[Miriam Cooper]], who happened to be the wife of director [[Raoul Walsh]].<ref name=imdbbio />

[[File:William Wellman flight instructor.jpg|thumb|left|William Wellman as a flight instructor at [[Rockwell Field]], [[San Diego, California|San Diego]] (1919)]]
Wellman hated being an actor, thinking it an "unmanly" profession,<ref name=tcmbio>TCM [http://www.tcm.com/tcmdb/person/204015|156757/William-A-Wellman/ "William A. Wellman Biography."] TCM Retrieved: September 20, 2009.</ref> and was miserable watching himself on screen while learning the craft.<ref name=WellmanJr /> He soon switched to working behind the camera, aiming to be a director, and progressed up the line as "a messenger boy, as an assistant cutter, an assistant property man, a property man, an assistant director, second unit director and eventually... director."<ref name="interview"/> His first assignment as an assistant director for [[Bernard Durning|Bernie Durning]] provided him with a work ethic that he adopted for future film work. One strict rule that Durning enforced was no fraternization with screen femme fatales, which almost immediately Wellman broke, leading to a confrontation and a thrashing from the director. Despite his transgression, both men became lifelong friends, and Wellman steadily progressed to more difficult first unit assignments.<ref name= "World War I"/>

Wellman made his uncredited directorial debut in 1920 at Fox with ''The Twins of Suffering Creek''. The first films he was credited with directing were ''The Man Who Won'' and ''Second Hand Love'', released on the same day in 1923. After directing a dozen low-budget 'horse opera' films (some of which he would rather forget),<ref name="interview"/> Wellman was hired by [[Paramount Pictures|Paramount]] in 1927 to direct ''[[Wings (1927 film)|Wings]]'', a major war drama dealing with fighter pilots during World War I that was highlighted by air combat and flight sequences. The film culminates with the epic [[Battle of Saint-Mihiel]]. In the [[1st Academy Awards]] it was one of two films to win Best Picture (the other was ''[[Sunrise: A Song of Two Humans|Sunrise]])'', although, due to tensions within the studio regarding time and budget overages, Wellman wasn't invited to the event.<ref name=WellmanJr>Wellman, William, Jr. (2015). ''Wild Bill Wellman - Hollywood Rebel,'' pp. 71, 191, 230, 357. Pantheon Books, New York. ISBN 978-0307377708.</ref>

Wellman's other notable films include ''[[The Public Enemy]]'' ([[1931 in film|1931]]), the first version of ''[[A Star Is Born (1937 film)|A Star Is Born]]'' ([[1937 in film|1937]]), ''[[Nothing Sacred (film)|Nothing Sacred]]'' (1937), the [[1939 in film|1939]] version of ''[[Beau Geste (1939 film)|Beau Geste]]'' starring [[Gary Cooper]], ''[[Thunder Birds (1942 film)|Thunder Birds]]'' ([[1942 in film|1942]]), ''[[The Ox-Bow Incident]]'' ([[1943 in film|1943]]), ''[[Lady of Burlesque]]'' (1943), ''[[The Story of G.I. Joe]]'' ([[1945 in film|1945]]), ''[[Battleground (film)|Battleground]]'' ([[1949 in film|1949]]) and two films starring and co-produced by John Wayne, ''[[Island in the Sky (1953 film)|Island in the Sky]]'' ([[1953 in film|1953]]) and ''[[The High and the Mighty (film)|The High and the Mighty]]'' ([[1954 in film|1954]]).

While he was primarily a director, Wellman also produced ten films, one of them uncredited, all of which he also directed. His last film was ''[[Lafayette Escadrille (film)|Lafayette Escadrille]]'' ([[1958 in film|1958]]), which he produced, directed, wrote the story for and narrated. He wrote the screenplay for two other films that he directed, and one film that he did not direct, [[1936 in film|1936]]'s ''[[The Last Gangster]]''. He also wrote the story for ''A Star Is Born'' and received a story credit for both remakes in [[A Star Is Born (1954 film)|1954]] and [[A Star Is Born (1976 film)|1976]].

Wellman initially worked fast, usually satisfied with a shot after one or two takes.<ref name=WellmanJr /> And despite his reputation of not coddling his leading men and women, he coaxed Oscar-nominated performances from seven actors: [[Fredric March]] and [[Janet Gaynor]] (''A Star Is Born''), [[Brian Donlevy]] (''Beau Geste''), [[Robert Mitchum]] (''The Story of G.I. Joe''), [[James Whitmore]] (''Battleground''), and [[Jan Sterling]] and [[Claire Trevor]] (''The High and Mighty''). Regarding actors, Wellman stated in a 1952 interview, "Movie stardom isn't about acting ability - it's personality and temperament," and added, "I once directed [[Clara Bow]]. She was mad and crazy but ''what'' a personality!"<ref>Johnson, Erskine. (April 27, 1952) ''[[The Sun (Lowell)|The Lowell Sunday Sun]],'' Lowell, MA.</ref>

===Innovations===
''Wings'' led to several firsts in filmmaking including newly invented camera mounts that could be secured to plane fuselages and motor-driven cameras to shoot actors while flying as the cameramen ducked out of frame in their cockpits. Star [[Richard Arlen]] had some flying experience but co-star [[Buddy Rogers (actor)|Buddy Rogers]] had to learn to fly for the film, as stunt pilots could not be used during close-up shots. Towers up to a hundred feet tall were used to shoot low-flying planes and battle action on the ground.<ref name=WellmanJr />

During the filming of ''Beggars for Life'', a silent film starring [[Wallace Beery]], Richard Arlen and [[Louise Brooks]], sound was added to Beery's introductory scene at the behest of Paramount Studio. Wellman reportedly hung a microphone from a broom so Beery could walk and talk within the scene, avoiding the static shot required for early sound shoots.<ref name=WellmanJr />

===Awards===
In his career, Wellman won a single Academy Award, for the story of ''A Star Is Born''. He was nominated as best director three times, for ''A Star Is Born'', ''Battleground'' and ''The High and Mighty'', for which he was also nominated by the [[Directors Guild of America]] as best director. In 1973, the DGA honored him with a Lifetime Achievement Award. Wellman also has a star on the [[Hollywood Walk of Fame]], at 6125 Hollywood Blvd.<ref>All Movie [http://www.allmovie.com/cg/avg.dll?p=avg&sql=2:116375~T3 Awards], IMDB [http://imdb.com/name/nm0920074/awards Awards]</ref>

==Legacy==
Several filmmakers have examined Wellman's career. [[Richard Schickel]] devoted an episode of his PBS series ''The Men Who Made the Movies'' to Wellman in 1973,<ref>IMDB [http://www.imdb.com/title/tt0070922/ "The Men Who Made the Movies: William A. Wellman]</ref> and in 1996, Todd Robinson made the feature-length documentary ''Wild Bill: Hollywood Maverick''.<ref>IMDB [http://www.imdb.com/title/tt0114939/ "Wild Bill: Hollywood Maverick"] ''imdb.com''. Retrieved: September 20, 2009.</ref>

The Academy Archive preserved "G.I. Joe" and the Academy Award Winning film "Wings," both by Wellman.<ref>{{cite web|title=Preserved Projects|url=http://www.oscars.org/academy-film-archive/preserved-projects?title=&filmmaker=william+wellman&category=All&collection=All|website=Academy Film Archive}}</ref>

==Family==
Wellman revealed near the end of his life that he had married a French woman named Renee during his time in The Lafayette Flying Corps. She was killed in a bombing raid during the war.<ref name=WellmanJr /> He was married four times in the U.S.:
*[[Helene Chadwick]]: married (1918&ndash;1923) separated after a month; later divorced
*Margery Chapin (daughter of [[Frederic Chapin]]): married (1925&ndash;1926); together for a short time; adopted [[Robert Emmett Tansey]]'s daughter, Gloria.
*Marjorie Crawford: married (1930&ndash;1933) divorced
*[[Dorothy Coonan|Dorothy "Dottie" Coonan]]: married (March 20, 1934&ndash;1975); until his death; they had seven children - four daughters, three sons.<ref>[http://www.independent.co.uk/news/obituaries/dorothy-coonan-wellman-actress-and-dancer-who-became-a-sam-goldwyn-golden-girl-1803576.html "Dorothy Coonan Wellman: Actress and dancer who became a Sam Goldwyn 'Golden Girl'."] ''The Independent'', October 16, 2009. Retrieved: October 16, 2009.</ref>

Dorothy starred in Wellman's 1933 film ''[[Wild Boys of the Road (1933 film)|Wild Boys of The Road]]'' and had seven children with Wellman,<ref name=variety/> including actors Michael Wellman, William Wellman Jr., Maggie Wellman, and Cissy Wellman. His daughter Kathleen "Kitty" Wellman married actor [[James Franciscus]], although they later divorced. His first daughter is Patty Wellman, and he had a third son, Tim Wellman.

William Wellman, Jr. wrote two books about his father, ''The Man And His Wings: William A. Wellman and the Making of the First Best Picture'' (2006), and ''Wild Bill Wellman - Hollywood Rebel'' (2015). Wellman Jr. has been a guest-host on [[Turner Classic Movies]] to introduce films made by his father.

William Wellman died in 1975 of [[leukemia]]. He was cremated, and his ashes were scattered at sea.<ref>Find a Grave [http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=6755218 William Wellman]</ref> His widow, [[Dorothy Wellman]], died on September 16, 2009, in [[Brentwood, Los Angeles, California|Brentwood, California]], at the age of 95.<ref name=variety/>

==Selected filmography==
{{expand list|date=February 2011}}
{{col-begin}}
{{col-break}}
*''[[The Knickerbocker Buckaroo]]'' ([[1919 in film|1919]])(Wellman's debut as an actor)
*''[[The Twins of Suffering Creek]]'' ([[1920 in film|1920]]) (first film as director—uncredited)
*''[[The Man Who Won]]'' ([[1923 in film|1923]])
*''[[Second Hand Love]]'' (1923)
*''[[Big Dan (film)|Big Dan]]'' (1923)
*''[[Cupid's Fireman]]'' (1923)
*''[[The Vagabond Trail]]'' (1924)
*''[[Not a Drum Was Heard]]'' (1924)
*''[[The Circus Cowboy]]'' (1924)
*''[[When Husbands Flirt]]'' (1925)
*''[[The Boob]]'' ([[1926 in film|1926]])
*''[[You Never Know Women]]'' (1926)
*''[[The Cat's Pajamas]]'' (1926)
*''[[Wings (1927 film)|Wings]]'' ([[1927 in film|1927]])
*''[[Ladies of the Mob]]'' ([[1928 in film|1928]])
*''[[Beggars of Life]]'' (1928)
*''[[The Legion of the Condemned]]'' (1928)
*''[[Chinatown Nights (1929 film)|Chinatown Nights]]'' ([[1929 in film|1929]])
*''[[Woman Trap (1929 film)|Woman Trap]]'' (1929)
*''[[The Man I Love (1929 film)|The Man I Love]]'' (1929)
*''[[Young Eagles (film)|Young Eagles]]'' ([[1930 in film|1930]])
*''[[Dangerous Paradise]]'' (1930)
*''[[Maybe It's Love]]'' (1930)
*''[[The Public Enemy]]'' ([[1931 in film|1931]])
*''[[Other Men's Women]]'' (1931)
*''[[Night Nurse (1931 film)|Night Nurse]]'' (1931)
*''[[The Star Witness]]'' (1931)
*''[[Safe in Hell]]'' (1931)
*''[[The Hatchet Man]]'' ([[1932 in film|1932]])
*''[[So Big! (1932 film)|So Big!]]'' (1932)
*''[[Frisco Jenny]]'' (1932)
*''[[The Purchase Price]]'' (1932)
*''[[Love Is a Racket]]'' (1932)
*''[[The Conquerors (1932 film)|The Conquerors]]'' (1932)
*''[[Central Airport (film)|Central Airport]]'' ([[1933 in film|1933]])
*''[[Midnight Mary]]'' (1933)
*''[[Lilly Turner]] (1933)
*''[[Heroes for Sale (film)|Heroes for Sale]]'' (1933)
*''[[Wild Boys of the Road]]'' (1933)
*''[[College Coach]]'' (1933)
{{col-break}}
*''[[Viva Villa!]]'' ([[1934 in film|1934]]) (uncredited)
*''[[The President Vanishes (film)|The President Vanishes]]'' (1934)
*''[[Stingaree (1934 film)|Stingaree]]'' (1934)
*''[[Looking for Trouble]]'' (1934)
*''[[The Call of the Wild (1935 film)|The Call of the Wild]]'' ([[1935 in film|1935]])
*''[[The Robin Hood of El Dorado]]'' ([[1936 in film|1936]])
*''[[Small Town Girl (1936 film)|Small Town Girl]]'' (1936)
*''[[Tarzan Escapes]]'' (1936) (uncredited)
*''[[A Star Is Born (1937 film)|A Star Is Born]]'' (also Story) (1937)
*''[[Nothing Sacred (film)|Nothing Sacred]]'' (1937)
*''[[Men with Wings]]'' (1938)
*''[[Beau Geste (1939 film)|Beau Geste]]'' (1939)
*''[[The Light that Failed]]'' (1939)
*''[[Thunder Birds (1942 film)|Thunder Birds]]'' (1942)
*''[[Roxie Hart (film)|Roxie Hart]]'' (1942)
*''[[The Great Man's Lady]]'' (1942)
*''[[Lady of Burlesque]]'' (1943)
*''[[The Ox-Bow Incident]]'' (1943)
*''[[Buffalo Bill (film)|Buffalo Bill]]'' ([[1944 in film|1944]])
*''[[This Man's Navy]]'' ([[1945 in film|1945]])
*''[[The Story of G.I. Joe]]'' (1945)
*''[[Gallant Journey]]'' (1946)
*''[[Magic Town]]'' ([[1947 in film|1947]])
*''[[The Iron Curtain (film)|The Iron Curtain]]'' ([[1948 in film|1948]])
*''[[Yellow Sky]]'' (1948)
*''[[Battleground (film)|Battleground]]'' ([[1949 in film|1949]])
*''[[The Happy Years]]'' ([[1950 in film|1950]])
*''[[The Next Voice You Hear...]]'' ([[1950 in film|1950]])
*''[[Across the Wide Missouri (film)|Across the Wide Missouri]]'' ([[1951 in film|1951]])
*''[[Westward the Women]]'' (1951)
*''[[My Man and I]]'' (1952)
*''[[Island in the Sky (1953 film)|Island in the Sky]]'' (Director + Narrator (Uncredited)) ([[1953 in film|1953]])
*''[[The High and the Mighty (film)|The High and the Mighty]]'' ([[1954 in film|1954]])
*''[[Track of the Cat]]'' (1954)
*''[[Ring of Fear (film)|Ring of Fear]]'' (uncredited) (1954)
*''[[Blood Alley]]'' ([[1955 in film|1955]])
*''[[Good-bye, My Lady (film)|Good-bye, My Lady]]'' ([[1956 in film|1956]])
*''[[Darby's Rangers (1958 film)|Darby's Rangers]]'' ([[1958 in film|1958]])
*''[[Lafayette Escadrille (film)|Lafayette Escadrille]]'' (1958)
{{col-end}}

==See also==
{{Portal|Biography}}
*[[List of ambulance drivers during World War I]]

==References==
;Notes
{{reflist|2}}
;Bibliography
{{refbegin}}
* Maltin, Leonard. "William Wellman" (film documentary)." ''The High and the Mighty'' (Collector's Edition) DVD. Burbank, California: Paramount Home Entertainment, 2005
* Thompson, Frank T. ''William A. Wellman'' (Filmmakers Series). Metuchen, New Jersey: Scarecrow Press, 1983. ISBN 0-8108-1594-X
* Wellman, William A. ''Go, Get 'em! The True Adventures of an American Aviator of the Lafayette Flying Corps.'' Boston: The Page Company, 1918
* Wellman, William A. ''Growing Old Gracefully''. Self-published, 1975<!--THIS BOOK LISTED ON TCM, BUT I CAN'T FIND BACKUP FOR IT * Wellman, William A. ''Growing Old Gracefully'' 1975-->
* Wellman, William A. ''A Short Time for Insanity: An Autobiography''. New York: Hawthorn Books, 1974. ISBN 0-8015-6804-8
* Wellman, William, Jr. ''The Man And His Wings: William A. Wellman and the Making of the First Best Picture''. Praeger Publishers, New York, 2006. ISBN 0-275-98541-5
*Wellman, William, Jr. ''Wild Bill Wellman - Hollywood Rebel''. Pantheon Books, New York, 2015. ISBN 978-0307377708
{{refend}}

==External links==
{{commons category}}
*{{webarchive |url=https://web.archive.org/web/20110104090330/http://www.filmlinc.com/fcm/online/wellmanextra.htm |date=January 4, 2011 |title=1978 interview with Wellman }}
* {{IMDb name|0920074}}
* {{Amg name|116375}}
* {{Tcmdb name}}
* [https://books.google.com/books?id=A-7HAAAAMAAJ&printsec=frontcover&source=gbs_ge_summary_r&cad=0#v=onepage&q&f=false "Go Get 'Em!", by William A. Wellman, Google ebook]

{{William A. Wellman}}
{{AcademyAwardBestStory 1928–1939}}

{{Authority control}}

{{DEFAULTSORT:Wellman, William A.}}
[[Category:American film directors]]
[[Category:Western (genre) film directors]]
[[Category:American people of Irish descent]]
[[Category: American people of English descent]]
[[Category: American people of Welsh descent]]
[[Category:Deaths from leukemia]]
[[Category:1896 births]]
[[Category:1975 deaths]]
[[Category:Soldiers of the French Foreign Legion]]
[[Category:Deaths from cancer in California]]
[[Category:Best Story Academy Award winners]]