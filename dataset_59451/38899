{{Infobox historic site
| name =Bruton Dovecote
| native_name =
| native_language =
| image =Bruton Somerset Dovecote.jpg
| caption =
| locmapin =Somerset
| coordinates = {{coord|51|06|29|N|2|27|11|W|display=inline,title}}
| location =[[Bruton]], [[Somerset]], England
| area ={{convert|6|sqm}}
| built =16th or 17th century
| architect =
| architecture =
| governing_body =
| owner =[[National Trust for Places of Historic Interest or Natural Beauty|National Trust]]
| designation1 =Grade II* listed building
| designation1_offname =Dovecote about 370 metres South of Bruton Church (also known as Pigeon Tower)
| designation1_date =24 March 1961<ref name=nhle>{{National Heritage List for England |num=1056424 |desc=Dovecote about 370 metres South of Bruton Church|accessdate=6 April 2015}}</ref>
| designation1_number =1056424
| designation2 =Scheduled Ancient Monument
| designation2_offname =Prospect tower 230&nbsp;m south of King's School
| designation2_date =14 February 1953<ref name=nhlesched/>
| designation2_number =1019895
}}

The '''Bruton Dovecote''' is a limestone tower that was built between the 15th and 17th century in [[Bruton]] in the English county of [[Somerset]]. The structure was once used as a dovecote, and may have been a watchtower or prospect tower prior to this. It is a Grade II* [[Listed building (United Kingdom)|listed building]]<ref name=nhle/> and [[scheduled monument]].<ref name=nhlesched>{{National Heritage List for England |num=1019895 |desc=Prospect tower 230&nbsp;m south of King's School|accessdate=7 July 2015}}</ref><ref>{{cite web|title=Dovecote, about 370 metres South of Bruton Church (also known as Pigeon Tower), Park Wall (North side), Bruton|url=http://www.somersetheritage.org.uk/record/50820|work=Somerset Historic Environment Record|publisher=Somerset County Council|accessdate=30 December 2013}}</ref><ref>{{cite web|title=Bruton Abbey|url=http://www.pastscape.org.uk/hob.aspx?hob_id=199997|work=Pastscape|publisher=English Heritage|accessdate=30 December 2013}}</ref>

It has been associated with [[Bruton Abbey]] and the Berkley family who owned the estate after the [[Dissolution of the Monasteries|dissolution]]. It is known that the conversion to house pigeons and doves took place around 1780. It was acquired by the [[National Trust for Places of Historic Interest or Natural Beauty|National Trust]] in 1915 and they have managed the site since then undertaking restoration work.

==History==
Though the Bruton Dovecote's date of construction is not known precisely, the structure was built some time between the 15th and 17th century.<ref name=nhlesched/> Architectural historian Lydia Greeves suggests that the building was once within the [[Deer park (England)|deerpark]] of [[Bruton Abbey]] and was adapted by the monks from a [[gable]]d [[Tudor period|Tudor]] tower.<ref>{{cite book|last=Greeves|first=Lydia|title=Houses of the National Trust|year=2013|publisher=National Trust Books|location=London|isbn=9781907892486|page=364}}</ref> However, John and Pamela McCann, authors of ''The Dovecotes of Historical Somerset'', claim that the structure was not built until after the [[Dissolution of the Monasteries]] in the 1530s. The authors claim that the Berkley family, who acquired the lands of the Abbey, constructed the building as a [[prospect tower]].<ref name=mccann>{{cite book|last1=McCann|first1=John|last2=McCann|first2=Pamela|title=The Dovecotes of Historical Somerset|date=2003|publisher=Somerset Vernacular Building Group|isbn=0952382431|pages=165–167}}</ref> [[Dendrochronology|Dendrochronological]] dating commissioned by the [[National Trust for Places of Historic Interest or Natural Beauty|National Trust]]  found that timber in the door and window frames came from trees felled between 1554 and 1586.<ref>{{cite journal|last1=Alcock|first1=Nat|last2=Tyers|first2=Cathy|title=Tree-Ring Date Lists 2011|journal=Vernacular Architecture|date=2011|volume=42|pages=87–116|url=http://www.maneyonline.com/doi/pdfplus/10.1179/174962911X13159065475626|doi=10.1179/174962911X13159065475626}}</ref>

The conversion to be a dovecote took place around 1780.<ref>{{cite web|title=Tower, S of the church, Bruton|url=http://www.somersetheritage.org.uk/record/53124|work=Somerset Historic Environment Record|publisher=Somerset County Council|accessdate=30 December 2013}}</ref> Pigeons and doves were an important food source historically kept for their eggs, flesh, and dung.<ref>{{cite book|last=Wright|first=Geoffrey N.|title=Abbeys and Priories|year=2004|publisher=Shire Publications Ltd|isbn=978-0747805892|page=33|url=https://books.google.com/books?id=lWAYvsIPbMwC&pg=PA5&dq=Bruton+Dovecote#v=onepage&q=Bruton%20&f=false}}</ref> Although it is now a roofless ruin with some of the windows blocked up, it previously had a chimney and the fireplace can still be seen.<ref name=nhle/> The National Trust acquired the [[Freehold (law)|freehold]] from Sir Henry Hugh Arthur Hoare of the [[Hoare baronets]], whose family seat was at [[Stourhead]], in 1915.<ref>{{cite web|title=Acquisitions Up to December 2011|url=http://www.nationaltrust.org.uk/cs/Satellite?blobcol=urldata&blobheader=application%2Fpdf&blobheadername1=Content-Disposition&blobheadername2=MDT-Type&blobheadername3=Content-Type&blobheadervalue1=inline%3B+filename%3D297%252F965%252Fnt_acquisitions_dec2011-2%252C2.pdf&blobheadervalue2=abinary%3B+charset%3DUTF-8&blobheadervalue3=application%2Fpdf&blobkey=id&blobtable=MungoBlobs&blobwhere=1349117284549&ssbinary=true|publisher=National Trust|accessdate=1 January 2015|format=PDF}}</ref> The tower was subsequently designated as a [[scheduled monument]] in 1953 and a [[listed building]] (Grade II*) in 1961.<ref name=nhle/><ref name=nhlesched/>

In the 1980s the dovecote was inspected as part of an investigation into an outbreak of [[psittacosis]] (also known as parrot disease or parrot fever), a [[zoonosis|zoonotic]] [[infection|infectious disease]] caused by the [[bacterium]] ''[[Chlamydophila psittaci]]'', at the adjoining [[King's School, Bruton|King's School]]. No cause was found for the infection.<ref>{{cite journal|last1=Pether|first1=J.V.S.|last2= Noaha|first2=N. D.|first3= Y. K.|last3=Laua|first4= J. A.|last4= Taylor|first5= J. C.|last5= Bowiea|title=An outbreak of psittacosis in a boys' boarding school|journal=Journal of Hygiene|date=June 1984|volume=92|issue=3|pages=337–343|doi=10.1017/S002217240006455X|format=PDF|pmc=2129321|pmid=6736642}}</ref> In 2010 [[Arrested decay|restoration]] work was undertaken including repairs to the tops of the walls; these are exposed as the building no longer has a roof. [[Tie rod|Wall ties]] were used on the south west corner where the masonry was bulging.<ref>{{cite web|last=Rice|first=Rene|title=Report on Conservation works undertaken on the Dovecote, Bruton.|url=http://www.renerice.com/Rene%20Rice%20Website/Bruton/Report%20on%20Conservation%20wo.htm|publisher=Rene Rice Conservation|accessdate=30 December 2013}}</ref> In addition, car parking and educational signage was provided. This was partially funded by [[South Somerset]] council, Bruton Town Council and the [[Heritage Lottery Fund]] to a total of £105,000.<ref>{{cite web|title=Bruton Dovecote|url=http://www.southsomerset.gov.uk/media/12292/11.%20Bruton%20Dovecote.pdf|publisher=South Somerset Council|accessdate=1 January 2015|format=PDF}}</ref><ref>{{cite web|title=Project Brief|url=http://www.southsomerset.gov.uk/media/12293/11A.pdf|publisher=South Somerset Council|accessdate=1 January 2015}}</ref><ref>{{cite web|last=Allen|first=Mike|title=Bruton Dovecote|url=http://www.southsomerset.gov.uk/media/9850/7d.%20Appendix%20D.pdf|format=PDF|work=2008/09 Capital Investment Appraisals|publisher=South Somerset council|accessdate=1 January 2015}}</ref>

==Location==
The dovecote stands on Lusty Hill to the south of Bruton overlooking the town. It is approximately {{convert|370|m}} south of the [[Church of St Mary, Bruton|Church of St Mary]],<ref name=nhle/> and {{convert|230|m}} south of [[King's School, Bruton|King's School]].<ref name=nhlesched/> It is on the [[Leland Trail]].<ref>{{cite web|last=Ross|first=David|title=Bruton Dovecote|url=http://www.britainexpress.com/attractions.htm?attraction=2380|publisher=Britain Express|accessdate=30 December 2013}}</ref>

==Architecture==
The square tower was built of local coursed [[oolitic limestone]] with [[Doulting Stone Quarry|Doulting stone]] dressings. The walls are {{convert|2|ft|8|in}} thick at the base,<ref name=mccann/> and the tower is {{convert|6|sqm}} in plan.<ref name=nhlesched/> The doorway has an [[ovolo]] [[Molding (decorative)|moulded]] arch which is {{convert|5|ft|9|in}} high. This suggests a construction date in the late 16th or early 17th century. Another entrance was made for cattle to enter at a later date but this has been blocked up. Several of the windows have also been filled with stone.<ref name=mccann/> On the north-east face there were two-light [[chamfer]]ed [[mullion]]ed windows on each floor while on the north-west wall they were one- and two-light windows.<ref name=nhle/>

It has over 200 pigeon holes, which were installed after the original construction, possibly before 1780.<ref name=nhlesched/><ref>{{cite web|title=Bruton Dovecote|url=http://somersetroutes.co.uk/site/bruton-dovecote/20|publisher=Somerset Routes|accessdate=30 December 2013}}</ref><ref name=pastscape>{{cite web|title=Monument No. 1356376|url=http://www.pastscape.org.uk/hob.aspx?hob_id=1356376|website=Pastscape|publisher=English Heritage|accessdate=4 January 2015}}</ref> Six tiers of nest boxes remain; however there were previously several more, possibly as many as 850. They are made of regular blocks of
[[tufa]] each around {{convert|8|in}} square and {{convert|16|in}} deep.<ref name=mccann/>

==See also==
{{Portal|Somerset}}
*[[Grade II* listed buildings in South Somerset]]
*[[List of scheduled monuments in South Somerset]]

==References==
{{reflist|33em}}

==External links==
{{commons category inline|Bruton Dovecote}}

{{Good article}}

[[Category:Grade II* listed buildings in South Somerset]]
[[Category:Scheduled Ancient Monuments in South Somerset]]
[[Category:Dovecotes]]
[[Category:Bruton]]
[[Category:Grade II* listed ruins]]