{{good article}}
{{Use dmy dates|date=December 2013}}
{{infobox castle
| name= Alatskivi Castle
| native_name = Alatskivi loss
| image = [[File:Alatskivi loss vaadatuna väravast.JPG|300px]]
| caption = Alatskivi Castle
| location =  [[Alatskivi]], [[Alatskivi Parish]], [[Tartu County]], [[Estonia]]
| coordinates = {{coord|58.6039|27.1297|type:landmark|display=inline}}
| built = Original in 17th century, rebuilt in late 19th century
| demolished =
| map_type = Estonia
| map_caption = Alatskivi Castle in [[Alatskivi]] village, [[Estonia]]
}}
'''Alatskivi Castle''' ({{lang-et|Alatskivi loss}}, {{lang-de|Schloss Allatzkiwwi}}) is a [[Gothic Revival architecture|Neo-Gothic]] castle in [[Alatskivi]], [[Estonia]]. Dating to the 17th century, it is situated in [[Alatskivi Parish]], [[Tartu County]]. It was rebuilt in the late 19th century by Baron Arved von Nolcken, modeled on the [[British Royal Family|royal]] residence of [[Balmoral Castle|Balmoral]] in [[Scotland]]. A renovation occurred between 2005 and 2011. Five rooms on the first floor house the [[Eduard Tubin]] museum, which documents his accomplishments as a music composer and conductor. 

Alatskivi Castle is surrounded by various ancillary buildings and a forested park of 130 hectares (320 acres) area, the largest in [[Tartu County]]. The park contains many [[oak]]s, [[Fraxinus|ash]]es, [[maple]]s, [[alder]]s and an approach road lined with [[Tilia|linden]] trees.

==Location==
Alatskivi Castle is located {{convert|40|km}} north of [[Tartu]] and {{convert|205|km}} from [[Tallinn]].{{sfn|Bain|2009|p=126}}<ref name=Castle>{{Cite web|url=http://www.ecb.ee/venues/alatskivi-castle|title=Alatskivi Castle|accessdate=21 October 2013|publisher=Estonian Convention Bureau}}</ref> It is built on the high bank of [[Lake Alatskivi]] at the foot of the Alatskivi valley.<ref name=Alatskavi/> An arched entrance leads to the castle along a road lined with [[Tilia|linden]] trees.{{Sfn|Maunder|1993|p=197}}

==History==
The earliest mention of the manor was in 1601.<ref name=Alatskavi>{{Cite web|url=http://www.devepark.utu.fi/Alatskivi_2.pdf|format=pdf|title=Procedure of Protection Alatskivi Manor Kaitsekord|accessdate=27 October 2013|work=DEVEPARK |publisher=Keskkonnaamet (Estonian Environment Agency)}}</ref> [[Gustavus Adolphus of Sweden|King Gustav Adolf II]] of Sweden gave it to his secretary, Johan Adler Salvius, in 1628. In 1642, its ownership passed on to [[Hans Detterman Cronman]] (c.1600-c.1645). In 1753, it was purchased by the Stackelbergs and inherited by the Nolckens in 1870.<ref name=Alatskavi/> Baron Arved George de Nolcken (1845–1909) rebuilt the castle between 1876–1885 according to his own designs,{{sfn|Hudson|1901|p=178}} in the [[Scottish Baronial architecture|Scottish baronial style]], designed as a smaller version of Queen Victoria's Balmoral Castle in Scotland, which he had visited in 1875.<ref name=Presser>{{cite book |last1=Presser |first1=Brandon |last2=Baker |first2=Mark |first3=Peter |last3=Dragicevich |first4=Simon |last4=Richmond |first5=Andy |last5=Symington|title=Lonely Planet Estonia, Latvia & Lithuania |url=https://books.google.com/books?id=ZZgzte6Y9yQC&pg=PP98 |date=1 July 2012 |publisher=Lonely Planet |isbn=978-1-74321-304-9 |page=78}}</ref><ref name=Alatskavi/> After nationalization occurred in 1919, the castle complex was taken over by the government under the Ministry of Agriculture<ref name=Alatskavi/> and became a school, cavalry barracks, state controlled farm land, council offices, cinema and library. It has been fully refurbished to its original form based on the original pictures of the aristocracy and their descendants who resided here.<ref name=Presser/> After the 2011 restoration, the castle was opened to the public with the Alatskivi Castle Foundation administrating the castle and the manor complex.<ref>{{Cite web|url=http://www.nn44.org/?d=51&p=1&lg=en|title=Alatskivi Castle|publisher=nn44.org}}</ref>

==Features==
The writer Ain Hinsberg refers to the manor house having been designed as a mock-English castle.{{Sfn|Hinsberg|1999|p=97}} The castle is built to an asymmetrical plan, with single- and double-storied wings, [[turrets]] and a slate roof. The building has both single- and double-storied floors. It hosts seminars, training programmes and small conferences, and is fitted with three meeting rooms and dining facilities.<ref name=Alatskavi/><ref name=Castle/>

Completed in 2011, the [[Eduard Tubin]] Museum is located in five rooms on the first floor of the castle.  The main feature is devoted to the life and work of Eduard Tubin who was one of Estonia's most esteemed composers.<ref name=Museum>{{Cite web|url=http://www.alatskiviloss.ee/eng/eduard-tubin-museum/|title=Eduard Tubin – a honorary guest of Alatskivi castle|accessdate=26 October 2013|publisher=Official website of Alatskivi Loss}}</ref> The initial exhibits are of members of the Tartu school who studied with Tubin, including [[Heino Eller]], [[Eduard Oja]], [[Alfred Karindi]], [[Olav Roots]], and [[Karl Leichter]].<ref name=Museum/> Tubin's music scores, manuscripts, books, records, films and photos, musical instruments, records, books, and sketches of theatre costumes are all part of the display.<ref name=Museum/> The museum also houses a large-scale model of the castle and plays the music of Tubin.<ref name=Museum/>

==Manor Park==
The {{convert|130|ha}} large Manor Park consists of oaks, ashes, maples, alders and an approach road lined with linden trees,<ref name=Presser/> some trees being grown on terraces.<ref name=Alatskavi/> It is the largest in the Tartu County.<ref name=Alatskavi/> A hiking track is laid through the park and the [[Alatskivi Nature Reserve]]. There are two artificial reservoirs along the Alatskivi River. There is a large boulder at the extreme end of the park in Kõdesi Forest where [[Apollo Belvedere]]'s statue existed in the past, although the statue has been moved to Kadriorg Park in [[Tallinn]].<ref name=Alatskavi/> The main castle is surrounded by many stone buildings. During the 19th century, the manor had 57 buildings, of which 41 remain.<ref name=History>{{Cite web|url=http://www.alatskiviloss.ee/eng/castle/history/|title=History|accessdate=27 October 2013|publisher=Official website of the Alatskivi Loss}}</ref> These are grouped in four areas connected by roads. The first contains the castle, coaching house and cheese cellar; the second, the economic circle, contains the laundry, kitchen, stables and sheds; the third or border circle, contains the barn, mills, church and cemetery; the outer fourth circle contains the Apollo Belvedere statue and the final resting place of the Estonian folklore figure [[Kalevipoeg]].<ref name=History/>

== Gallery ==
{{Wide image|Alatskivi mõisa peahoone.jpg|900px|Front view of the Alatskivi Castle}}
<gallery>
Alatskivi cactle inside 05.jpg|Interior
Chandelier alatskivi cactle.jpg|Chandelier
Alatskivi cactle inside 04.jpg|Exhibition
Alatskivi mõisa peahoone õhust edela nurk.JPG|Castle from above
Alatskivi mõisa peahoone õhust kirde nurk.JPG|Castle from above
</gallery>
==References==
{{reflist|35em}}
;Bibliography
*{{cite book|last=Bain|first=Carolyn|title=Estonia, Latvia and Lithuania|url=https://books.google.com/books?id=7Yg-9Np1abwC&pg=PA126|date=2 May 2009|publisher=Lonely Planet|isbn=978-1-74104-770-7|ref=harv}}
*{{cite book | last = Hein| first = Ants| title = Eesti Mõisad - Herrenhäuser in Estland - Estonian Manor Houses | publisher = Tänapäev|year = 2009| location = Tallinn  | isbn = 978-9985-62-765-5 }}
*{{cite book|last=Hudson|first=Charles Edward Mogridge|title=The Manors of Wike Burnell and Wyke Waryn, Country of Worcester|url=https://books.google.com/books?id=Pr06AQAAMAAJ&pg=PA178|edition=Public domain|year=1901|publisher=Hudson|ref=harv}}
*{{cite book|last=Hinsberg|first=Ain|title=Key to Estonia|url=https://books.google.com/books?id=CotpAAAAMAAJ|year=1999|publisher=Areal|isbn=978-9985-9180-2-9|ref=harv}}
*{{cite book|last= Maunder |first=Hilke |title=Baltic States: (Estonia, Latvia, Lithuania)|url=https://books.google.com/books?id=SPIgAQAAMAAJ|year=1993|publisher=Hayit|isbn=978-1-874251-07-1|ref=harv}}
*{{cite book | last = Sakk | first = Ivar | title = Estonian Manors - A Travelogue | publisher = Sakk & Sakk OÜ| year = 2004 | location = Tallinn | isbn = 9949-10-117-4}}

==External links==
{{Commons category|Alatskivi castle}}
*{{Official website|www.alatskiviloss.ee/eng/}} {{en icon}}

{{Manor houses in Estonia}}

[[Category:Houses completed in 1885]]
[[Category:Castles in Estonia]]
[[Category:Buildings and structures in Tartu County]]
[[Category:Alatskivi Parish]]
[[Category:Manor houses in Estonia]]
[[Category:Kreis Dorpat]]
[[Category:Museums in Estonia]]
[[Category:Tourist attractions in Tartu County]]