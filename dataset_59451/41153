{{about||the pre-Hulagu Mongol governor of Persia in the mid-13th century|Arghun Aqa|the village in Iran|Arghun, Iran|the town in Afghanistan|Urgun}}

{{Infobox monarch
| name               = Arghun 
| title              = Khan
| image              = AbaqaOnHorseArghunStandingGhazanAsAChild.jpg
| caption            = An image representing Arghun (standing, holding his son [[Ghazan]]) under a royal umbrella. Beside him is his father [[Abaqa]] on a horse.
| succession         =  
| reign              = 1284– 7 March 1291
| birth_date         =
| death_date         = 
| death_place        = 
| burial_date        =
| burial_place       =
| predecessor        = [[Tekuder]]
| successor          = [[Gaykhatu]]
| royal house        =
| dynasty            = [[Ilkhanate]]
| father             = [[Abaqa]]
| mother             =
| issue              =
| religion           = [[Buddhism]]; baptized at birth as Christian
| signature          =
}}
<!-- Unsourced image removed: [[File:Ilkhanate_1256–1353.PNG|300px|thumb|Arghun (1258-1291) was the fourth ruler of the Turco-Mongol [[Ilkhanate]].{{Deletable image-caption|Wednesday, May 07 2008|date=May 2012}}]] -->

'''Arghun Khan''' a.k.a. '''Argon''' ([[Mongolian Cyrillic alphabet|Mongolian Cyrillic]]: ''Аргун хан''; c. 1258 – 7 March 1291<ref>{{Cite book|url=https://books.google.com/books?id=CHzGvqRbV_IC|title=The Empire of the Steppes: A History of Central Asia|last=Grousset|first=René|date=1970|publisher=Rutgers University Press|year=|isbn=9780813513041|location=|pages=376|language=en|via=}}</ref>) was the fourth ruler of the [[Mongol Empire|Mongol empire's]] [[Ilkhanate]], from 1284 to 1291. He was the son of [[Abaqa Khan]], and like his father, was a devout [[Buddhist]] (although pro-Christian). He was known for sending several embassies to Europe in an unsuccessful attempt to form a [[Franco-Mongol alliance]] against the Muslims in the [[Holy Land]]. It was also Arghun who requested a new bride from his great-uncle [[Kublai Khan]]. The mission to escort the young [[Kökötchin]] across Asia to Arghun was reportedly taken by [[Marco Polo]]. Arghun died before Kökötchin arrived, so she instead married Arghun's son, [[Ghazan]].

==Biography==
[[File:Arghun et Tegüder.jpeg|thumb|Arghun and [[Tekuder]]]]
Arghun was born to [[Abaqa Khan]] and his Christian princess wife, [[Haimash Khatun]]. Arghun himself had multiple wives, and his mother-in-law [[Bulughan Khatun]] raised Arghun's two sons [[Ghazan]] (whose birth mother was [[Qutlugh]]) and [[Öljeitü]] (whose birth mother was Uruk Khatun<ref name=wives>{{cite journal|title= Christian wives of Mongol khans: Tartar queens and missionary expectations in Asia |author=Ryan, James D.|journal=Journal of the Royal Asiatic Society|volume=8|date=November 1998|pages=411&ndash;421|issue=9|doi=10.1017/s1356186300010506}}</ref>), both of whom later succeeded him and eventually converted to [[Islam]]. Arghun had Öljeitü baptized as a Christian at birth, and gave him the name Nikolya "[[Nicholas]]" after [[Pope Nicholas IV]].<ref>"Arghun had one of his sons baptized, Khordabandah, the future Oljaitu, and in the Pope's honor, went as far as giving him the name Nicholas", ''Histoire de l'Empire Mongol'', Jean-Paul Roux, p.408</ref> According to the [[Dominican Order|Dominican]] missionary [[Ricoldo of Montecroce]], he was "a man given to the worst of villainy, but for all that a friend of the Christians".<ref>Jackson, p.176</ref>

One of the sisters of Arghun, [[Oljath]], was married to the Georgian King [[Vakhtang II of Georgia|Vakhtang II]].<ref>Grousset, p.846</ref>

Arghun was a Buddhist, but as did most [[Turco-Mongols]], he showed great tolerance for all faiths, even allowing Muslims to be judged under [[Sharia|Islamic Law]].  His grand [[vizier]] and minister of finance, [[Sa'ad al-Dawla]], was a [[Jew]]. Sa'ad was effective in restoring order to the Ilkhanate's government, in part by aggressively denouncing the abuses of the Mongol military leaders.<ref>Mantran, Robert (Fossier, Robert, ed.) "A Turkish or Mongolian Islam" in ''The Cambridge Illustrated History of the Middle Ages: 1250-1520'', p. 298</ref>

==Conflicts==
Arghun's reign was relatively peaceful, and there were few conflicts with his fellow Mongols. He did fight a brief campaign against the [[Chagatai Khanate]] in [[greater Khorasan|Khorasan]]. In 1289-1290, he had to deal with an upheaval of the [[Oirats|Oirat]] emir [[Nawrūz (Mongol emir)|Nauruz]], who had to flee to [[Transoxonia]].

In 1288 and 1290, he had repelled two separate invasion forces from the [[Golden Horde]] under [[Tulabuga]] in the area of the [[Caucasus]].

During Arghun's reign, the Egyptian [[Mamluk]]s were continuously reinforcing their power in Syria. The Mamluk Sultan [[Qalawun]] recaptured Crusader territories, some of which, such as [[Principality of Tripoli|Tripoli]], had been vassal states of the [[Il Khanate|Il Khans]].  The Mamluks had captured the northern fortress of [[Margat]] in 1285, [[Lattakia]] in 1287, and completed the [[Fall of Tripoli (1289)|Fall of Tripoli]] in 1289.<ref>Tyerman, p.817</ref>

==Relations with Christian powers==
{{Main|Franco-Mongol alliance}}
Arghun was one of a long line of Genghis-Khanite rulers who had endeavored to establish a [[Franco-Mongol alliance]] with the Europeans, against their common foes the Mamluks of Egypt. Arghun had promised his potential allies that if [[Jerusalem]] were to be conquered, he would have himself baptized.  Yet by the late 13th century, Western Europe was no longer as interested in the crusading effort, and Arghun's missions were ultimately fruitless.<ref>Prawdin, p. 372.  "Argun revived the idea of an alliance with the West, and envoys from the Ilkhans once more visited European courts.  He promised the Christians the Holy Land, and declared that as soon as they had conquered Jerusalem he would have himself baptised there.  The Pope sent the envoys on to Philip the Fair of France and to Edward I of England.  But the mission was fruitless.  Western Europe was no longer interested in crusading adventures.</ref>

===First mission to the Pope===
In 1285, Arghun sent an embassy and a letter to Pope [[Honorius IV]], a Latin translation of which is preserved in the [[Roman Curia|Vatican]].<ref name=runciman-398>Runciman, p.398</ref><ref>"This Arghon loved the Christians very much, and several times asked to the Pope and the king of France how they could together destroy all the Sarazins" - Le Templier de Tyr  - French original:"Cestu Argon ama mout les crestiens et plusors fois manda au pape et au roy de France trayter coment yaus et luy puissent de tout les Sarazins destruire" [http://www.fordham.edu/halsall/basis/GuillaumeTyr5.html Guillame de Tyr (William of Tyre) "Historia rerum in partibus transmarinis gestarum" #591] <!--Recommend moving this to Wikiquote --></ref>  Arghun's letter mentioned the links that Arghun's family had to Christianity, and proposed a combined military conquest of Muslim lands:<ref>''The Crusades Through Arab Eyes'' p. 254: Arghun, grandon of Hulagu, "had resurrected the most cherished dream of his predecessors: to form an alliance with the Occidentals and thus to trap the Mamluk sultanate in a pincer movement. Regular contacts were established between Tabriz and Rome with a view to organizing a joint expedition, or at least a concerted one."</ref>

{{quote|"As the land of the Muslims, that is, Syria and Egypt, is placed between us and you, we will encircle and strangle ("estrengebimus") it. We will send our messengers to ask you to send an army to Egypt, so that us on one side, and you on the other, we can, with good warriors, take it over. Let us know through secure messengers when you would like this to happen. We will chase the [[Saracens]], with the help of the Lord, the Pope, and the Great Khan."|Extract from the 1285 letter from Arghun to Honorius IV, Vatican<ref>Quote in "Histoires des Croisades III", Rene Grousset, p700</ref>}}

===Second mission, to Kings Philip and Edward===
[[File:VoyagesOfRabbanBarSauma.jpg|thumb|Arghun's ambassador [[Rabban Bar Sauma]] travelled from Peking in the East, to Rome, Paris and Bordeaux in the West, meeting with the major rulers of the period, even before [[Marco Polo]]'s return from Asia.]]
Apparently left without an answer, Arghun sent another embassy to European rulers in 1287, headed by the Ongut Turk Nestorian monk from China [[Rabban Bar Sauma]],<ref name="Rossabi2014">{{cite book|author=Morris Rossabi|title=From Yuan to Modern China and Mongolia: The Writings of Morris Rossabi|url=https://books.google.com/books?id=GXejBQAAQBAJ&pg=PA385&lpg=PA385#v=onepage&q&f=false|date=28 November 2014|publisher=BRILL|isbn=978-90-04-28529-3|pages=385–386}}</ref> with the objective of contracting a military alliance to fight the Muslims in the Middle East, and take the city of [[Jerusalem]].<ref name=runciman-398/><ref>Rossabi, p. 99</ref> The responses were positive but vague. Sauma returned in 1288 with positive letters from [[Pope Nicholas IV]], [[Edward I of England]], and [[Philip IV the Fair]] of France.<ref>Boyle, in Camb. Hist. Iran V, pp. 370-71; Budge, pp. 165-97. [http://www.encyclopediairanica.com/articles/v10f2/v10f216a.html Source]</ref>

===Third mission===
[[File:ArghunLetterToPhilippeLeBelExtract1289.jpg|thumb|1289 letter of Arghun to [[Philip IV of France|Philip the Fair]], in Mongolian language and classical [[Mongolian script]], with detail of the introduction. The letter was remitted to the French king by [[Buscarel of Gisolfe]]. The seal is that of the Great Khan, with Chinese Script: “輔國安民之寶”, which means "Seal of the upholder of the State and the purveyor of peace to the People". The paper is of [[Korea]]n manufacture. 182x25 cm. [[French National Archives]].<ref>''Grands Documents de l'Histoire de France'', Archives Nationales de France, p.38, 2007.</ref>]]In 1289, Arghun sent a third mission to Europe, in the person of [[Buscarel of Gisolfe]], a Genoese who had settled in Persia. The objective of the mission was to determine at what date concerted Christian and Mongol efforts could start. Arghun committed to march his troops as soon as the Crusaders had disembarked at [[Saint-Jean-d'Acre]]. Buscarel was in Rome between July 15 and September 30, 1289, and in Paris in November–December 1289. He remitted a letter from Arghun to Philippe le Bel, answering to Philippe's own letter and promises, offering the city of Jerusalem as a potential prize, and attempting to fix the date of the offensive from the winter of 1290 to spring of 1291:<ref>Runciman, p.401</ref>

{{quote|"Under the power of the eternal sky, the message of the great king, Arghun, to the king of France..., said: I have accepted the word that you forwarded by the messengers under Saymer Sagura ([[Rabban Bar Sauma|Bar Sauma]]), saying that if the warriors of Il Khaan invade Egypt you would support them. We would also lend our support by going there at the end of the Tiger year’s winter [1290], worshiping the sky, and settle in Damascus in the early spring [1291].

If you send your warriors as promised and conquer Egypt, worshiping the sky, then I shall give you Jerusalem. If any of our warriors arrive later than arranged, all will be futile and no one will benefit. If you care to please give me your impressions, and I would also be very willing to accept any samples of French opulence that you care to burden your messengers with.

I send this to you by Myckeril and say: All will be known by the power of the sky and the greatness of kings. This letter was scribed on the sixth of the early summer in the year of the Ox at Ho’ndlon."|Letter from Arghun to Philippe le Bel, 1289, France royal archives<ref>[http://chass.csupueblo.edu/history/seminar/sauma/saumaletter.htm Alternative translation of Arghun's letter]{{Dead link|date=December 2014}}</ref><ref>For another translation [http://www.pony-express.net/west-east/china/expo/catalogue_text/cat32_text.html here]</ref>}} <!-- Excellent quote that should be kept :) -->

Buscarello was also bearing a memorandum explaining that the Mongol ruler would prepare all necessary supplies for the Crusaders, as well as 30,000 horses.<ref name="Jean Richard, p.468">Jean Richard, p.468</ref> Buscarel then went to England to bring Arghun's message to [[Edward I of England|King Edward I]]. He arrived in London January 5, 1290. Edward, whose answer has been preserved, answered enthusiastically to the project but remained evasive about its actual implementation, for which he deferred to the Pope.<ref>"Histoire des Croisades III", p.713, Rene Grousset.</ref>

====Assembly of a raiding naval force====
In 1290, Arghun launched a shipbuilding program in Baghdad, with the intent of having war galleys which would harass the Mamluk commerce in the [[Red Sea]].  The [[Republic of Genoa|Genoese]] sent a contingent of 800 carpenters and sailors, to help with the shipbuilding. A force of [[arbaletiers]] was also sent, but the enterprise apparently foundered when the Genoese government ultimately disowned the project, and an internal fight erupted at the [[Persian Gulf]] port of [[Basra]] among the Genoese (between the [[Guelphs and Ghibellines|Guelph and Ghibelline]] factions).<ref name="Jean Richard, p.468"/><ref>"Only a contingent of 800 Genoese arrived, whom he (Arghun) employed in 1290 in building shipd at Baghdad, with a view to harassing Egyptian commerce at the southern approaches to the Red Sea", p.169, Peter Jackson, ''The Mongols and the West''</ref>

===Fourth mission===
Arghun sent a fourth mission to European courts in 1290, led by Andrew Zagan (or Chagan), who was accompanied by Buscarel of Gisolfe and a Christian named Sahadin.<ref name="runciman-402"/>

In 1291, [[Pope Nicholas IV]] proclaimed a new Crusade and negotiated agreements with Arghun, [[Hetoum II]] of Armenia, the [[Jacobite Syrian Christian Church|Jacobites]], the [[People of Ethiopia|Ethiopians]] and the [[Georgians]]. On January 5, 1291, Nicholas addressed a vibrant prayer to all the Christians to save the Holy Land, and predicators started to rally Christians to follow Edward I in a Crusade.<ref>Dailliez, p.324-325</ref>

However, the efforts were too little and too late. On May 18, 1291, [[Saint-Jean-d'Acre]] was conquered by the Mamluks in the [[Siege of Acre (1291)|Siege of Acre]].

In August 1291, Pope Nicholas  wrote a letter to Arghun informing him of the plans of Edward I to go on a Crusade to recapture the Holy Land, stating that the Crusade could only be successful with the help of the "powerful arm" of the Mongols.<ref>Schein, p.809</ref> Nicolas repeated an oft-told theme of the Crusader communications to the Mongols, asking Arghun to receive baptism and to march against the Mamluks.<ref>Jackson, p.169</ref>  However Arghun himself had died on March 10, 1291, and Pope Nicholas IV would die in March 1292, putting an end to their attempts at combined action.<ref>Runciman, p.412</ref>

Edward I sent an ambassador to Arghun's successor [[Gaikhatu]] in 1292 in the person of [[Geoffrey de Langley]], but extensive contacts would only resume under Arghun's son [[Ghazan]].

According to the 20th-century historian Runciman, "Had the Mongol alliance been achieved and honestly implemented by the West, the existence of [[Outremer]] would almost certainly have been prolonged. The Mamluks would have been crippled if not destroyed; and the Ilkhanate of Persia would have survived as a power friendly to the Christians and the West"<ref name=runciman-402>Runciman, p.402</ref>

==Death==
Arghun died on March 7, 1291,<ref name="steppes-376">"He died on March 7, 1291." ''Steppes'', p. 376</ref> and was succeeded by his brother [[Gaykhatu]].

The 13th century saw such a vogue of Mongol things in the West that many new-born children in Italy were named after Genghis-Khanite rulers, including Arghun: names such as ''Can Grande'' ("Great Khan"), ''Alaone'' ([[Hulagu]]), ''Argone'' (Arghun) or ''Cassano'' ([[Ghazan]]) are recorded with a high frequency.<ref>Peter Jackson, ''The Mongols and the West'', p.315</ref>

==Marco Polo==
{{Main|Kökötchin}}
Arghun was the stated reason why [[Marco Polo]] was able to return to [[Venice]] after 23 years of absence. Arghun, having lost his favorite wife [[Bolgana]], asked his grand uncle and ally [[Kublai Khan]] to send him one of Bolgana's relatives as a new bride. The choice fell to the 17-year-old [[Kökötchin]] ("Blue, or Celestial, Dame"). Marco Polo was given the task of accompanying the princess through land and sea routes, navigating on a Mongolian ship through the [[Indian Ocean]] to Persia. The journey took two years and Arghun died in the meantime, so Kökötchin instead married Arghun's son [[Ghazan]].

==See also==
*[[Timeline of Buddhism]] (see 1285 CE)

==Notes==
{{Reflist|2}}

==References==
*[[Le Templier de Tyr]] (c. 1300), [http://www.fordham.edu/halsall/basis/GuillaumeTyr5.html Online] (Original French).
*"The Monks of Kublai Khan Emperor of China", Sir E. A. Wallis Budge. [http://www.aina.org/books/mokk/mokk.htm Online]
*{{cite book|title=Les Templiers|author=Dailliez, Laurent |author-link=Laurent Dailliez |language=French|publisher=Editions Perrin|year=1972|isbn=2-262-02006-X}}
*[[Richard Foltz|Foltz, Richard]], ''Religions of the Silk Road'', New York: Palgrave Macmillan, 2010, ISBN 978-0-230-62125-1
*{{cite book|title=Histoire des Croisades III, 1188-1291|author=Grousset, René|language=French|publisher=Editions Perrin|year=1935|isbn=2-262-02569-X}}
*Grousset, Rene, ''The Empire of the Steppes: a History of Central Asia'', Naomi Walford, (tr.), New Brunswick, NJ: Rutgers University Press, 1970. 
*{{cite book|author=Jackson, Peter|title=The Mongols and the West: 1221-1410|year=2005|publisher=Longman|isbn=978-0-582-36896-5}}
*{{cite book|author=Lebédel, Claude|title=Les Croisades, origines et conséquences|publisher=Editions Ouest-France|language=French|year=2006|isbn=2-7373-4136-1}}
*{{cite book|author=Maalouf, Amin|title=[[The Crusades Through Arab Eyes]]|publisher=New York: Schocken Books|year=1984|isbn=0-8052-0898-4}}
*{{cite book|author=Prawdin, Michael |authorlink=Michael Prawdin|origyear=1940|year=1961|publisher=Collier-Macmillan Canada|title=Mongol Empire|isbn=1-4128-0519-8}}
*{{cite book|author=Richard, Jean|title=Histoire des Croisades|year=1996|publisher=Fayard|isbn=2-213-59787-1}}
*{{cite book|author=Rossabi, Morris|authorlink=Morris Rossabi|title=Voyager from Xanadu: Rabban Sauma and the first journey from China to the West|year=1992|isbn=4-7700-1650-6|publisher=[[Kodansha International Ltd.]]}}
*{{cite book|title=A history of the Crusades 3|author=Runciman, Steven |author-link=Steven Runciman |publisher=Penguin Books|isbn=978-0-14-013705-7|date=1987 (first published in 1952-1954)}}
*{{cite journal|title=Gesta Dei per Mongolos 1300. The Genesis of a Non-Event|author=Schein, Sylvia|journal=The English Historical Review|volume=94|issue=373| pages=805–819|date=October 1979|doi=10.1093/ehr/XCIV.CCCLXXIII.805|jstor=565554}}
*{{cite book|author=Tyerman, Christopher|year=2006|title=God's War: A New History of the Crusades|publisher=Harvard University Press|isbn=0-674-02387-0}}

==External links==
*[https://web.archive.org/web/20061207122945/http://www.electroauthor.com:80/marcotte_genealogy/Khan.htm Khan genealogy]
*[https://web.archive.org/web/20131015081646/http://www.ucalgary.ca:80/applied_history/tutor/islam/mongols/ilkhanate.html The Islamic World to 1600: The Mongol Invasions (The Il-Khanate)]

{{s-start}}
{{s-reg}}
{{succession box
|
 title=[[Ilkhanid Dynasty]] 
|
 years=1284– 7 March 1291
|
 before=[[Tekuder]]
|
 after=[[Gaykhatu]]
}}
{{s-end}}

{{Mongol Empire}}
{{Authority control}}

{{DEFAULTSORT:Arghun}}
[[Category:1258 births]]
[[Category:1291 deaths]]
[[Category:Il-Khan emperors]]
[[Category:Borjigin| ]]
[[Category:13th-century monarchs in Asia]]
[[Category:Descendants of Genghis Khan]]