{{good article}}
{{Use dmy dates|date=June 2012}}

{|{{Infobox ship begin}}
{{Infobox ship image
| image = 
|Ship image=
|Ship caption=INS ''Arihant'' during sea trials in 2014
}}
{{Infobox ship class overview
|Name= ''Arihant'' 
|Builders=Navy Shipbuilding Centre, Visakhapatnam<ref>{{Cite news|url=http://www.thehindu.com/news/cities/Visakhapatnam/contract-worker-killed-in-accident-at-navy-ship-building-centre/article5764589.ece|title=Contract worker killed in accident at navy ship building centre|newspaper=[[The Hindu]]|access-date=17 March 2016|date=8 March 2014}}</ref>
|Operators= {{navy|India}}
|In commission range=2016
|Total ships building=3<ref>{{Cite news|url=http://thediplomat.com/2016/05/why-india-needs-both-nuclear-and-conventional-submarines/|title=Why India needs submarines|newspaper=The Diplomat|access-date=17 May 2016|date=May 2016}}</ref>
|Total ships completed=
|Total ships active=1<ref name="India_triad">{{Cite web|title =Now, India has a nuclear triad |url =http://www.thehindu.com/news/national/now-india-has-a-nuclear-triad/article9231307.ece |website = The Hindu|access-date = 17 October 2016}}</ref>
|Total ships laid up=
|Total ships lost=
|Total ships retired=
|Total ships preserved=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type= [[Nuclear submarine|Nuclear powered]] [[ballistic missile submarine]]
|Ship displacement= {{convert|6000|t}} surfaced<ref name=dna-launch>{{cite news|url=http://www.dnaindia.com/india/report_india-reaches-milestone-with-launch-of-n-powered-submarine_1277227|title=India reaches milestone with launch of n-powered submarine|newspaper=DNA |date=26 July 2007|access-date=24 January 2011}}</ref>
|Ship length= {{convert|112|m|abbr=on}}<ref name=dna-launch/>
|Ship beam= {{convert|11|m|abbr=on}} 
|Ship height=
|Ship draft= {{convert|10|m|abbr=on}} 
|Ship depth=
|Ship decks=
|Ship deck clearance=
|Ship power=*1 × [[pressurised water reactor]]<ref name=toi-ntriad/>
*{{convert|83|MW|abbr=on}}
|Ship propulsion=*1 × [[propeller shaft]]
*[[Nuclear marine propulsion|Nuclear]]
|Ship speed=*''Surfaced:'' {{convert|12|-|15|kn|km/h}}
*''Submerged:'' {{convert|24|kn|km/h}} 
|Ship range= unlimited except by food supplies
|Ship endurance=
|Ship test depth= {{convert|300|m|abbr=on}}
|Ship complement= 95
|Ship time to activate=
|Ship sensors= [[USHUS (sonar)|USHUS]] sonar
|Ship EW=
|Ship armament= ''Missiles:'' 12 × [[Sagarika (missile)|K15]] [[SLBM]] (750–1900 km or 405–1026 mi range) or 4 × [[K-4 (SLBM)|K-4]] SLBM (3500 km or 1890 mi range)<ref name=toi-ntriad/>
''Torpedoes:'' 6 × 21" (533 mm) torpedo tubes – est 30 charges (torpedoes, cruise missiles or mines)<ref>{{cite web|first=John |last=Pike|url=http://www.globalsecurity.org/military/world/india/atv-specs.htm |title=Advanced Technology Vessel (ATV) |publisher=globalsecurity.org|date=27 July 2009|access-date=24 January 2011}}</ref>
|Ship notes=
}}
|}

The '''''Arihant'' class''' ([[Sanskrit]], for ''Killer of Enemies'') is a class of [[nuclear marine propulsion|nuclear-powered]] [[ballistic missile submarine]]s being built for the [[Indian Navy]]. They were developed under the US$2.9&nbsp;billion ''Advanced Technology Vessel'' (ATV) project to design and build nuclear-powered submarines.

The lead vessel of the class, {{ship|INS|Arihant}} was launched in 2009 and after extensive sea trials, was confirmed to be commissioned in August 2016.<ref name=":2" /><ref name=":0">{{cite web |url=http://www.newsx.com/national/43966-what-is-ins-arihant |title=What is INS Arihant? |website=NewsX|date=17 October 2016 |author=Ashish Singh |accessdate= 30 October 2016}}</ref><ref name="Arihant_ready">{{Cite news|title = India's first nuclear submarine INS Arihant ready or operations, passes deep sea tests|url = http://economictimes.indiatimes.com/news/defence/indias-first-nuclear-submarine-ins-arihant-ready-for-operations-passes-deep-sea-tests/articleshow/51098650.cms|newspaper=[[The Economic Times]]|date=23 February 2016|access-date=23 February 2016}}</ref>
''Arihant'' is the first ballistic missile submarine to have been built by a country other than one of the five [[permanent members of the United Nations Security Council]].<ref name=bbc2>{{cite news|last=Marcus|first=Jonathan|title=Indian-built Arihant nuclear submarine activated|url=http://www.bbc.co.uk/news/world-asia-india-23648310|access-date=12 October 2013|work=[[BBC]]|date=10 August 2013}}</ref>

==History==
In December 1971, during the [[Indo-Pakistani War of 1971]], the [[U.S. President|US President]] [[Richard Nixon]] sent a [[carrier battle group]] named [[Task Force 74]], led by the nuclear-powered {{USS|Enterprise|CVN-65|6}} into the [[Bay of Bengal]] in an attempt to intimidate India.<ref>{{Cite web|url=https://in.rbth.com/articles/2011/12/20/1971_war_how_russia_sank_nixons_gunboat_diplomacy_14041|title=1971 War: How Russia sank Nixon’s gunboat diplomacy|website=in.rbth.com|access-date=2016-04-29}}</ref><ref>{{Cite web|url=http://www.thedailystar.net/news/us-fleet-in-bay-of-bengal-a-game-of-deception|title=US Fleet in Bay of Bengal: A game of deception|date=2013-12-15|website=The Daily Star|access-date=2016-04-29}}</ref> In response, the [[Soviet Union]] sent a submarine armed with nuclear missiles from [[Vladivostok]] to trail the US task force.<ref>{{cite news|first=Rakesh|last=Krishnan Simha|url=http://indrus.in/articles/2011/12/20/1971_war_how_russia_sank_nixons_gunboat_diplomacy_14041.html|title=US-Soviet Actions in 1971 Indo-Pakistani War |publisher=Indrus.in |date=20 December 2011|access-date=22 April 2013|work=indrus.in}}</ref> The event demonstrated the significance of nuclear weapons and ballistic missile submarines to then Prime Minister [[Indira Gandhi]].<ref name=idr-arihant>{{cite news|url= http://www.indiandefencereview.com/2010/02/arihant-the-annihilator.html|title=Arihant: the annihilator|work=Indian Defence Review |date=25 October 2010|access-date=8 January 2012}}</ref> Following the 1974 [[Smiling Buddha]] nuclear test, the Director of Marine Engineering (DME) at Naval Headquarters initiated a technical feasibility study for an indigenous nuclear propulsion system (Project 932).<ref>{{cite news|first=Premvir|last=Das|url=http://www.business-standard.com/article/opinion/premvir-das-ins-arihant-a-watershed-moment-109073000008_1.html|title=Project 932|newspaper=[[Business Standard]]|date=30 July 2009|access-date=24 April 2013}}</ref>

The Indian Navy's ''Advanced Technology Vessel'' project to design and construct a nuclear submarine took shape in the 1990s.<ref>{{cite web|url=https://fas.org/nuke/guide/india/sub/ssn/index.html|title=India's SNS Project Report|publisher=Fas.org|access-date=24 April 2013}}</ref> Then [[Defence Minister of India|Defence Minister]] [[George Fernandes]] confirmed the project in 1998.<ref>{{cite news|url=http://www.indianexpress.com/res/web/pIe/ie/daily/19980519/13950884.html|title=George defends position on China|publisher=[[Indian Express]]|date=19 May 1998|access-date=24 February 2011}}</ref> The initial intent of the project was to design nuclear-powered fast attack submarines, though following [[Pokhran-II|nuclear tests conducted by India in 1998]] at [[Pokhran Test Range]] and the Indian pledge of [[no first use]], the project was re-aligned towards the design of a ballistic missile submarine in order to complete India's [[nuclear triad]].<ref name=gs-atv>{{cite web|first=John|last=Pike|url=http://www.globalsecurity.org/military/world/india/atv.htm|title=Advanced Technology Vessel (ATV)|publisher=Globalsecurity.org|access-date=24 January 2011|archive-url= https://web.archive.org/web/20101226032637/http://www.globalsecurity.org/military/world/india/atv.htm| archive-date= 26 December 2010}}</ref><ref name=dna-induction>{{cite news|url= http://www.dnaindia.com/india/report_first-indigenous-nuclear-sub-is-inducted-into-the-navy_1277218 |title=First indigenous nuclear sub is inducted into the navy |newspaper=DNA|date=26 July 2009|access-date=24 January 2011}}</ref><ref>{{cite news|url= http://www.rediff.com/news/2001/feb/16josy4.htm|title=India's nuclear sub still a distant dream|work=[[Rediff]]|date=16 February 2001|access-date=24 January 2011}}</ref>

==Description==
The ''Arihant''-class submarines are nuclear powered [[ballistic missile submarine]]s built under the ''[[Advanced Technology Vessel]]'' (ATV) project.<ref name="domain-b.com">{{cite web|url= http://www.domain-b.com/defence/general/20090716_indian_indigenous.html |title=Indian indigenous nuclear sub to be unveiled on 26 July: report|publisher=domain-b.com|date=16 July 2009|access-date=24 January 2011}}</ref><ref>{{cite news|url= http://in.reuters.com/article/companyNews/idINISL35153320090212|title=India nuclear sub project near completion|work=[[Reuters]]|date=12 February 2009|access-date=24 January 2011}}</ref><ref name="launch1">{{cite news|url= http://news.in.msn.com/national/article.aspx?cp-documentid=3097464|title=PM to launch indigenous nuke submarine by month-end|date=16 July 2009|work=[[MSN]]|access-date=19 July 2009}}</ref><ref name="launch2">{{cite news|url= http://www.hindu.com/2009/07/19/stories/2009071958490500.htm |title= Indigenous nuclear submarine goes on trial|newspaper=[[The Hindu]]|access-date=19 July 2009 |location=Chennai, India|date=19 July 2009}}</ref><ref>{{cite news|first=Hari|last=Sud|url=http://www.upiasia.com/Security/2009/08/14/indias_nuclear_submarine_and_the_indian_ocean/7676/|title=India's nuclear submarine and the Indian Ocean |publisher=upiasia.com |date=14 August 2009|access-date=24 January 2011}}</ref><ref>{{cite news|url=http://blogs.reuters.com/india/2009/07/31/indias-nuclear-submarine-dream-still-miles-to-go/|work=[[Reuters]]| title=India's nuclear submarine dream, still miles to go|date=31 July 2009|access-date=24 January 2011}}</ref> They will be the first nuclear submarines designed and built by India.<ref name=it-finaltest>{{cite news|url= http://indiatoday.intoday.in/story/Final+test+of+K-15+ballistic+missile+on+Tuesday/1/5002.html|title=Final test of K-15 ballistic missile on Tuesday|date=25 February 2008|access-date=24 January 2012}}</ref> The submarines are {{convert|112|m|abbr=on}} [[Length overall|long]] with a [[Beam (nautical)|beam]] of {{convert|11|m|abbr=on}}, a [[Draught (ship)|draught]] of {{convert|10|m|abbr=on}}, displacement of {{convert|6000|t}} and a diving depth of {{convert|300|m|abbr=on}}. The complement is about 95, including officers and sailors.<ref name="navaltech"/> The boats are powered by a single seven blade propeller powered by an {{convert|83|MW|abbr=on|adj=on}} [[pressurised water reactor]] and can achieve a maximum speed of {{convert|12|-|15|kn|km/h}} when surfaced and {{convert|24|kn|km/h}} when submerged.<ref name="navaltech"/>

The submarines have four launch tubes in their hump and can carry up to 12 [[Sagarika (missile)|K-15 Sagarika]] missiles with one warhead each (with a range of {{convert|750|km|abbr=on|disp=or}}) or 4 [[K-4 (SLBM)|K-4]] missiles (with a range of {{convert|3500|km|abbr=on|disp=or}}).<ref name=it-undersea>{{cite news|url= http://indiatoday.intoday.in/story/The+secret+undersea+weapon/1/3659.html|title=The secret undersea weapon|work=[[India Today]]|date=17 January 2008|access-date=8 January 2012}}</ref><ref name=it-secretk>{{cite news|url=http://indiatoday.intoday.in/site/story/the-secret-k-missile-family/1/120488.html|work=[[India Today]]|title=The secret 'K' missile family|date=20 November 2010|access-date=8 January 2012}}</ref> The submarines are similar to the {{sclass2-|Akula|submarine|1}} of [[Russia]].<ref name="navaltech">{{cite web|url= http://www.naval-technology.com/projects/arihant-class/|title=SSBN Arihant Class Submarine, India|publisher=naval-technology.com|access-date=22 April 2013}}</ref> The Indian Navy will train on {{INS|Chakra}}, an Akula-class submarine leased from Russia in 2012.<ref name=autogenerated1>{{cite web|url=http://www.globalsecurity.org/military/world/india/atv.htm |title=Arihant – Advanced Technology Vessel (ATV)|publisher=Global Security|access-date=8 January 2012}}</ref><ref name=it-setsail>{{cite news|url=http://indiatoday.intoday.in/story/leased-russian-n-submarine-to-set-sail-for-india-this-month-end/1/164449.html|title=Leased Russian n-submarine to set sail for India this month end|date=15 December 2011|access-date=8 January 2012}}</ref>

==Development==
The submarines are powered by a [[pressurised water reactor]] with [[highly enriched uranium]] fuel.<ref name=toi-20090717>{{cite news| url= http://articles.timesofindia.indiatimes.com/2009-07-17/india/28203215_1_nuclear-powered-submarine-atv-project-ins-arihant|newspaper=[[Times Of India]]|first=Rajat|last=Pandit|title=India set to launch nuclear-powered submarine|date=17 July 2009|access-date=17 March 2016}}</ref><ref>{{cite news|url=http://www.hindu.com/seta/2009/11/05/stories/2009110551721200.htm|title=High fissile fuel in nuclear submarine lasts long|newspaper=[[The Hindu]]|date=5 November 2009|access-date=17 March 2016|location=Chennai, India}}</ref> The miniaturized version of the reactor was designed and built by the [[Bhabha Atomic Research Centre]] (BARC) at the [[Indira Gandhi Centre for Atomic Research]] (IGCAR) in [[Kalpakkam]].<ref name=hindu-indiandesign>{{cite news|url= http://www.hindu.com/2009/08/16/stories/2009081655260900.htm|title=INS Arihant is an Indian design: Anil Kakodkar|newspaper=[[The Hindu]]|date=16 August 2009|access-date=17 March 2016|location=Chennai, India}}</ref> It included a {{convert|42|m|ft|adj=on}} section of the submarine's pressure hull containing the shielding tank with water and the reactor, a control room, as well as an auxiliary control room for monitoring safety parameters.<ref>{{cite news|url= http://www.telegraphindia.com/1090803/jsp/nation/story_11313999.jsp|title=Unveiled: Arihant’s elder brother|work=Telegraph India|date=3 August 2009|access-date=17 March 2016|location=Calcutta, India|first=G.C.|last=Shekhar}}</ref> The prototype reactor became critical on 11&nbsp;November 2003 and was declared operational on 22&nbsp;September 2006.<ref name=idr-arihant/> Successful operation of the prototype for three years enabled the production version of the reactor for ''Arihant''.<ref name="pwr_1">{{cite news|url=http://www.hindu.com/2009/08/03/stories/2009080353810100.htm |title=PWR building shows indigenous capability |first=T.S. |last=Subramanian |date=2 August 2009 |newspaper=[[The Hindu]] |access-date=17 March 2016 |location=Chennai, India |deadurl=yes |archiveurl=https://web.archive.org/web/20090808043941/http://www.hindu.com:80/2009/08/03/stories/2009080353810100.htm |archivedate=8 August 2009 |df=dmy }}</ref><ref name="pwr_2">{{cite news|url= http://www.hindustantimes.com/StoryPage/StoryPage.aspx?sectionName=HomePage&id=5721403d-8f66-43e2-92c4-4f3148032565&Headline=Arihant+propulsion+reactor+unveiled|title=Arihant propulsion reactor unveiled|first=M.R.|last=Venkatesh|date=2 August 2009|access-date=17 March 2016|newspaper=[[Hindustan Times]]}}</ref> The reactor subsystems were tested at the Machinery Test Center in [[Visakhapatnam]].<ref>{{cite web|url=http://www.nrbdrdo.res.in/nstl.html|title=Naval Research Board |publisher=DRDO|access-date=17 March 2016}}</ref> Facilities for loading and replacing the fuel cores of the naval reactors in berthed submarines were also established.<ref name=idr-arihant/>

The detailed engineering of the design was implemented at [[Larsen & Toubro]]'s submarine design center at their [[Hazira]] shipbuilding facility.<ref>{{cite press release|url= http://www.larsentoubro.com/lntcorporate/LnT_PRS/PDF/L&TPressRelease-Jul26-2009.pdf |title=Larsen and Toubro's Contribution to Arihant-class submarine|format=PDF|date=26 July 2009|access-date=17 March 2016}}</ref> [[Tata Power SED]] built the control systems for the submarine.<ref>{{cite web|title=India's first Indigenous nuclear submarine |url=http://www.jeywin.com/blog/arihant-–-india’s-first-indigenous-nuclear-submarine/|publisher=Jeywin|access-date=24 April 2013 |deadurl=yes|archiveurl=https://web.archive.org/web/20130217054406/http://www.jeywin.com/blog/arihant-%E2%80%93-india%E2%80%99s-first-indigenous-nuclear-submarine/|archive-date=17 February 2013 }}</ref> The steam turbines and associated systems integrated with the reactor were supplied by [[Walchandnagar Industries]].<ref name=dna-ppp>{{cite news|url=http://www.dnaindia.com/india/report_private-sector-played-a-major-role-in-arihant_1277435 |title=Private sector played a major role in Arihant|newspaper=DNA|date=27 April 2009|access-date=17 March 2016}}</ref> The lead vessel underwent a long and extensive process of testing after its launch in July 2009.<ref>{{cite news| url=http://www.hindu.com/2009/07/27/stories/2009072755801000.htm|location=Chennai, India|newspaper=[[The Hindu]]|title=Nuclear submarine Arihant to be fitted with K-15 ballistic missiles|date=27 July 2009|access-date=17 March 2016}}</ref> The propulsion and power systems were tested with high-pressure steam trials followed by harbor-acceptance trials that included submersion tests by flooding its ballast tanks and controlled dives to limited depths.<ref name=toi-homemade>{{cite news|url=http://articles.timesofindia.indiatimes.com/2009-12-03/india/28110779_1_ins-arihant-nuclear-powered-attack-submarines-technology-vessel|title=Home-made nuke sub INS Arihant to be inducted in 2 years|newspaper=[[Times of India]]|date=3 December 2009|access-date=17 March 2016}}</ref> INS ''Arihant''{{'}}s reactor went critical for the first time on 10 August 2013.<ref>{{cite news|url= http://www.thehindu.com/sci-tech/technology/k15-all-set-to-join-arihant/article4242325.ece|title=K-15 all set to join Arihant|newspaper=[[The Hindu]]|date=27 December 2012|access-date=17 March 2016}}</ref> On 13 December 2014, the submarine set off for its extensive sea trials.<ref>{{cite news|url=http://economictimes.indiatimes.com/news/politics-and-nation/indias-nuclear-submarine-arihant-flagged-off-for-sea-trials/articleshow/45526418.cms|title=India's nuclear submarine Arihant flagged off for sea trials|newspaper=[[The Economic Times]]|date=13 December 2014|access-date=15 December 2014}}</ref><ref>{{cite news|url=http://www.thehindu.com/news/cities/Visakhapatnam/ins-arihant-sails-out-of-harbour/article6693951.ece?homepage=true|title=INS Arihant sails out of harbour|newspaper=[[The Hindu]]|date=13 December 2014|access-date=15 December 2014}}</ref>

==Ships in class==
[[File:Arihant 1.jpg|thumb|right|Conceptual drawing of INS ''Arihant'']]
Exact number of planned submarines remains unclear, according to media reports about three to six submarines are planned to be built.<ref>{{Cite news|title=Sea trials of Indian Navy's deadliest sub going 'Very Well'|url=http://thediplomat.com/2015/05/sea-trials-of-indian-navys-deadliest-sub-going-very-well/|work=The Diplomat|date=31 May 2015|access-date=17 March 2016}}</ref><ref>{{Cite web|title =Asia's coming nuclear nightmare|url=http://nationalinterest.org/blog/the-buzz/asias-coming-nuclear-nightmare-12513|publisher=CFTNI|access-date=17 March 2016|first=David|last=Brewster}}</ref><ref>{{Cite news|title=Was 2015 a good year for India's defence sector?|url=http://www.business-standard.com/article/specials/was-2015-a-good-year-for-india-s-defence-sector-115122900193_1.html|date=31 December 2015|access-date=17 March 2016|newspaper=[[Business Standard]]}}</ref><ref>{{Cite news|title=After Arihant, Indian Navy considering n-propulsion for Aircraft Carriers|url=http://www.indiastrategic.in/topstories4382_After_Arihant_Indian_Navy_considering_n_propulsion_for_Aircraft_Carriers.htm|work=indiastrategic.in|date=31 December 2015|access-date=17 March 2016}}</ref><ref>{{Cite web|title=SSBN Arihant Class Submarine|url=http://www.naval-technology.com/projects/arihant-class/|publisher=naval-technology.com|access-date=17 March 2016}}</ref><ref>{{Cite news|title=N-capable Arihant submarine successfully test-fires unarmed missile|url=http://www.bignewsnetwork.com/news/238921543/n-capable-arihant-submarine-successfully-test-fires-unarmed-missile|work=Big News|access-date=17 March 2016}}</ref><ref>{{Cite news|title=Indian Navy soon to be the most formidable submarine force On The Planet|url=http://www.indiatimes.com/lifestyle/technology/indian-navy-soon-to-be-the-most-formidable-submarine-force-on-the-planet-heres-what-you-need-to-know-232694.html|work=indiatimes.com|access-date=17 March 2016}}</ref> The first boat of the class, {{INS|Arihant}} was commissioned in August 2016.<ref name=":2">{{Cite news|url=http://timesofindia.indiatimes.com/india/India-set-to-complete-N-triad-with-Arihant-commissioning/articleshow/54907081.cms|title=India set to complete N-triad with Arihant commissioning - Times of India|newspaper=The Times of India|access-date=2016-10-18}}</ref><ref>{{Cite news|url=http://www.tribuneindia.com/news/satisfied-with-nuclear-sub-arihant-trials-navy-chief/74351.html|title=Satisfied with nuclear sub Arihant trials: Navy Chief|newspaper=Tribune India|access-date=17 March 2016}}</ref> The first four vessels are expected to be commissioned by 2023.<ref name="toi-ntriad">{{cite news|url=http://articles.timesofindia.indiatimes.com/2012-01-02/india/30580966_1_ins-arihant-first-indigenous-nuclear-submarine-akula-ii|title = India to achieve N-arm triad in February|newspaper=[[Times of India]]|date=2 January 2012|access-date=17 March 2016}}</ref> In December 2014, the work on a second nuclear reactor began and the second boat, {{INS|Aridhaman}} is being prepared for sea trials.<ref>{{cite news|url=http://defenceradar.com/2014/12/02/work-on-second-n%C2%ADuclear-sub-reactor-begins/ |title=Work on second nuclear sub reactor begins|date=2 December 2014|access-date=17 March 2016}}</ref> The next three ships in the class, after the lead ship, will be larger and have 8 missile launch tubes to carry up to 8 K4 and a more powerful pressurized water reactor than INS Arihant. A larger follow on class to the arihant class is also planned, these new boats will be capable of carrying 12 to 16 ballistic missiles.<ref>{{Cite web|url=http://www.newindianexpress.com/nation/EXPRESS-EXCLUSIVE-Maiden-Test-of-Undersea-K-4-Missile-From-Arihant-Submarine/2016/04/09/article3370608.ece|title=EXPRESS EXCLUSIVE: Maiden Test of Undersea K-4 Missile From Arihant Submarine|website=The New Indian Express|access-date=2016-04-09}}</ref><ref name=":1">{{Cite web|url=http://thediplomat.com/2016/03/indias-undersea-deterrent/|title=India’s Undersea Deterrent|last=Diplomat|first=Saurav Jha, The|website=The Diplomat|access-date=2016-04-09}}</ref> The first submarine was commissioned into the Indian Navy in August 2016. <ref>{{Cite news|url=http://www.newsx.com/national/43966-what-is-ins-arihant|title=What is INS Arihant|date=2016-10-17|newspaper=NewsX|access-date=2016-10-17}}</ref>

{| class="wikitable"
|-
! Name
! Pennant
! Launch
! Sea Trials
! Commission
! Status
|-
| {{ship|INS|Arihant}}
| S 73 / S2<ref name=":1" /><ref>{{Cite news|url=http://www.thehindu.com/news/national/ins-arihant-may-be-of-limited-utility/article6709623.ece|title=INS Arihant may be of limited utility|date=2014-12-20|newspaper=The Hindu|language=en-IN|issn=0971-751X|access-date=2016-04-09}}</ref>
| 26 July 2009
| 13 December 2014 <ref name="INS Arihant sails out of harbour">{{cite news| url=http://www.thehindu.com/news/cities/Visakhapatnam/ins-arihant-sails-out-of-harbour/article6693951.ece?homepage=true|title=INS Arihant sails out of harbor|newspaper=[[The Hindu]]|date=13 December 2014|access-date=22 December 2014}}</ref>
| August 2016
|In service<ref name=":0" />
|-
| {{INS|Aridhaman}}
| S 74 / S3<ref name=":1" />
| ''2016''
|'' Under Sea Trials 2017''
|''Late 2018''
| construction complete<ref name="toi-ntriad" /><ref>{{cite news|url=http://khabar.ibnlive.in.com/news/53480/1|title=Second nuclear submarine under construction|work=IBN|language=Hindi|date=23 May 2011|access-date=17 March 2016}}</ref>
|-
| tbd
| S 75 / S4<ref name=":1" />
| ''2018''
| ''tbd''
| ''tbd''
|under construction<ref>{{Cite news|url=http://www.newindianexpress.com/nation/EXPRESS-EXCLUSIVE-Maiden-Test-of-Undersea-K-4-Missile-From-Arihant-Submarine/2016/04/09/article3370608.ece|title=Maiden Test of Undersea K-4 Missile From Arihant Submarine|website=The New Indian Express|access-date=1 June 2016|date=9 April 2016}}</ref>
|-
|tbd
| S 76 / S5<ref name=":1" />
|
|
|
|under construction
|}

== Timeline ==
{| class="wikitable sortable" cellpadding="1" cellspacing="1"  style="width:98%; border:1px solid black;"
|- style="background:#9cf;"
| style="width:10%;"| '''Date'''
| style="width:20%;"| '''Event'''
|- style="background:#f4f9ff;"
| 19 May 1998 || Confirmation of ATV project by the then Defence Minister [[George Fernandes]]
|- style="background:#f4f9ff;"
| 11 November 2003 || Prototype nuclear reactor becomes critical
|- style="background:#f4f9ff;"
| 22 September 2006 || Nuclear reactor is declared operational
|- style="background:#f4f9ff;"
| 26 July 2009|| Lead vessel of the class, {{INS|Arihant}} is formally launched
|- style="background:#f4f9ff;"
| 10 August 2013|| ''Arihant''{{'}}s on-board nuclear reactor attains [[Nuclear reactor physics#Criticality|criticality]]
|- style="background:#f4f9ff;"
| 13 December 2014|| INS ''Arihant'' begins extensive sea & weapons trials
|- style="background:#f4f9ff;"
| 25 November 2015|| INS ''Arihant'' successfully test-fired dummy B5 missile
|- style="background:#f4f9ff;"
| 31 March 2016|| INS ''Arihant'' successfully test-fired K4 missile
|- style="background:#f4f9ff;"
| August 2016|| INS ''Arihant'' commissioned.<ref name=":2" />
|- style="background:#f4f9ff;"
|2018||INS ''Aridhaman'' to be delivered.<ref name=":2" />
|}

==See also==
* [[List of active Indian Navy ships]]
* [[INS Varsha]]
* [[Indian Navy SSN programme]]
* [[Submarines of the Indian Navy]]

==References==
{{Reflist|25em}}

==External links==
{{commons category|Arihant class submarines}}
*[https://fas.org/nuke/guide/india/sub/ssn/index.html The Indian Strategic Nuclear Submarine Project by Mark Gorwitz]

{{Ship classes of the Indian Navy}}
{{SSBN classes in service}}
{{Submarines_of_Indian_Navy}}

{{DEFAULTSORT:Arihant-class submarine}}
[[Category:Arihant-class submarines| ]]
[[Category:Submarine classes]]