{{Infobox publisher
| image = 
| parent = 
| status = 
| founded = 1994
| founder = 
| successor = 
| country = [[United Arab Emirates]]
| headquarters = [[Sharjah (emirate)|Sharjah]]
| distribution = 
| keypeople = 
| publications = [[Scientific journal]]s, [[e-book]]s
| imprints = 
| revenue = 
| numemployees = 
| nasdaq = 
| url = {{URL|http://benthamscience.com}}
}}
'''Bentham Science Publishers''' is a company that [[publishing|publishes]] scientific, technical, and medical journals and [[e-book]]s. It publishes more than 100 [[subscription]]-based [[academic journal]]s<ref name=access/> and over 60 [[open access]] journals.<ref name=home>{{cite web |url=http://www.benthamscience.com/ |work=Bentham Science Publishers |title=Home page |accessdate=2010-12-26}}</ref> It is based at [[Sharjah (emirate)|Sharjah]] in the United Arab Emirates,<ref name=access>{{cite journal|url=http://www.aardvarknet.info/access/number50/monthnews.cfm?monthnews=11 |title=Bentham stays small for high impact |work=ACCESS – Asia's Newspaper on Electronic Information Product & Service |issue=50 |date=September 2004 |accessdate=2010-12-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20110716120253/http://www.aardvarknet.info/access/number50/monthnews.cfm?monthnews=11 |archivedate=2011-07-16 |df= }}</ref> and has operating units in the United States, Japan, China, India, and the Netherlands. More than 90 percent of the workforce is outsourced to Pakistan.<ref name=access/> Other outsource operations are in China, UK, USA, India and other countries. Its open access branch, Bentham Open Science, has received severe criticism for its questionable peer-review practices, and was listed as a "Potential, possible, or probable predatory scholarly open-access publisher" in [[Jeffrey Beall]]'s list of Predatory Publishers (which is now defunct).

== Publishing divisions ==
Bentham Science has three main operating divisions: subscription-based journals, open access titles, and e-books. They publish research literature in all areas of science, medicine, technology, humanities, and social sciences, which is available in both electronic and print versions.

Bentham Science publishes more than 100 subscription-based journals in the fields of biotechnology, biomedical, pharmaceuticals, technology, engineering, computer and social sciences. These titles are indexed in Scopus, Chemical Abstracts, MEDLINE, EMBASE, PubsHub, etc.{{citation needed|date=January 2015}}

Bentham Open Access publishes more than 60 peer-reviewed, free-to-view online journals under Bentham Open. This imprint has been identified as a [[predatory publisher]] by [[Jeffrey Beall]].<ref name="scholarlyoa">{{cite web|url=http://web.archive.org/web/20161222020349/https:/scholarlyoa.com/publishers/|title=List of publishers|date=|archive-url=http://scholarlyoa.com/publishers/|archive-date=2016-12-22|dead-url=|work=Scholarly Open Access|last1=Beall|first1=Jeffrey|accessdate=2014-12-28}}</ref>

Bentham eBooks publish text books, handbooks, monographs, biographies, autobiographies, conference proceedings and review volumes in the areas of medicine, technology, humanities, natural, and social sciences.

==Controversies and criticism==
Bentham Open journals claim to employ [[peer review]];<ref name="Bentham Open">{{cite web |url=http://www.bentham.org/open/index.htm |title=Bentham Open Home Page |work=Bentham Science Publishers Ltd. |accessdate=2010-07-29}}</ref> however, the fact that a fake paper generated with [[SCIgen]] had been accepted for publication, has cast doubt on this.<ref name="Videnskab: Chief Editor Resigns After Controversial Article on 9/11">{{cite web |url=http://videnskab.dk/content/dk/naturvidenskab/chefredaktor_skrider_efter_kontroversiel_artikel_om_911 |title=Chefredaktør skrider efter kontroversiel artikel om 9/11 |language=Danish |work=Videnskab.dk |accessdate=2010-07-29}}</ref><ref name="New Scientist: CRAP Paper">{{cite web |url=http://www.newscientist.com/article/dn17288-crap-paper-accepted-by-journal.html |title=CRAP paper accepted by journal – opinion – 11 June 2009 |work=[[New Scientist]] |accessdate=2010-07-29}}</ref><ref name="The Scientist: Editors Quit After Fake Paper Flap">{{cite web |url=http://www.the-scientist.com/blog/display/55759/ |title=Editors quit after fake paper flap |work=[[The Scientist (magazine)|The Scientist]] |accessdate=2010-07-29}}/</ref> Furthermore, the publisher is known for [[E-mail spam|spamming]] scientists with invitations to become a member of the [[editorial board]]s of its journals.<ref>[http://www.earlham.edu/~peters/fos/2008/04/some-background-on-bentham-open-but.html Some background on Bentham Open, but just some] Peter Suber, Open Access News, April 24, 2008</ref>

In 2009, the Bentham Open Science journal ''The Open Chemical Physics Journal'' published a study contending dust from the [[September 11 attacks|World Trade Center attacks]] contained "active nanothermite".<ref name="The Open Chemical Physics Journal">{{cite web |url=http://www.benthamscience.com/open/tocpj/articles/V002/7TOCPJ.htm |title=Active Thermitic Material Discovered in Dust from the 9/11 World Trade Center Catastrophe |work= Open Chemical Physics Journal |accessdate=2012-07-23}}</ref> Following publication, the journal's [[editor-in-chief]] [[Marie-Paule Pileni]] resigned stating, "They have printed the article without my authorization… I have written to Bentham, that I withdraw myself from all activities with them".<ref name="Videnskab">{{cite web |url=http://videnskab.dk/teknologi/chefredaktor-skrider-efter-kontroversiel-artikel-om-911 |title=Chefredaktør skrider efter kontroversiel artikel om 9/11 |work= Vindeskab.dk |accessdate=2012-07-23}}</ref>

In a review of Bentham Open for ''[[The Charleston Advisor]]'', [[Jeffrey Beall]] noted that "in many cases, Bentham Open journals publish articles that no legitimate peer-review journal would accept, and unconventional and nonconformist ideas are being presented in some of them as legitimate science." He concluded by stating that "the site has exploited the Open Access model for its own financial motives and flooded scholarly communication with a flurry of low quality and questionable research."<ref name="Beall09">{{cite journal |last1=Beall |first1=Jeffrey |authorlink=Jeffrey Beall |date=July 2009 |title=Bentham Open |journal=[[The Charleston Advisor]] |volume=11 |issue=1 |pages=29–32|url=http://eprints.rclis.org/13538/1/s8.pdf}}</ref> Beall has since added Bentham Open to his list of "Potential, possible, or probable predatory scholarly open-access publishers".<ref name=scholarlyoa/> 

In 2013, ''The Open Bioactive Compounds Journal'' was one of the journals that accepted an obviously bogus paper submitted as part of the ''[[Who's Afraid of Peer Review?]]'' sting.<ref>{{cite journal|url=http://science.sciencemag.org/content/suppl/2013/10/03/342.6154.60.DC1|title=Data and Documents|date=1 October 2016|publisher=|via=www.sciencemag.org}}</ref> It has since been discontinued.<ref>{{cite web|url=http://benthamopen.com/journal/index.php?journalID=tobcj|title=The Open Bioactive Compounds Journal|publisher=}}</ref>

== See also ==
* [[:Category:Bentham Science Publishers academic journals|Category: Bentham Science Publishers academic journals]]

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.benthamscience.com/}}

[[Category:Book publishing companies of the United Arab Emirates]]
[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1994]]
[[Category:Media in Sharjah]]
[[Category:Companies based in Sharjah]]
[[Category:Open access publishers]]