{{refimprove|date=January 2017}}
{{Use South African English|date=March 2015}}
{{Use dmy dates|date=May 2013}}
{{Infobox book
| italic title   = <!--(see above)-->
| name           = The Story of an African Farm
| image          = The Story of an African Farm.jpg
| image_size     = 300px
| alt            = Cocoa-brown cloth, pictorially decorated in darker brown, spines lettered in gilt
| caption        = First edition cover
| author         = [[Olive Schreiner]] (Ralph Iron)
| audio_read_by  = 
| title_orig     = 
| orig_lang_code = en
| title_working  = 
| translator     = 
| illustrator    = 
| cover_artist   = 
| country        = [[Cape Colony]]
| language       = English
| series         = 
| release_number = 
| subject        = 
| genre          = [[Bildungsroman]], [[philosophical fiction]]
| set_in         = [[Karoo]], mid-late 19th century
| published      = 
| publisher      = [[Chapman & Hall]]
| publisher2     = 
| pub_date       = 1883
| english_pub_date = 
| media_type     = 
| pages          = 644, in two volumes
| awards         = 
| isbn           = 
| oclc           = 471800625
| dewey          = 823.8
| congress       = 
| preceded_by    = 
| followed_by    = 
| native_wikisource = 
| wikisource     = The Story of an African Farm
| notes          =
| exclude_cover  = 
}}{{italic title}}
'''''The Story of an African Farm''''' (published in 1883 under the pseudonym Ralph Iron) was South African author [[Olive Schreiner]]'s first published novel. It was an immediate success and has become recognised as one of the first feminist novels.<ref>http://www.victorianweb.org/authors/schreiner/diniejko.html</ref><ref>Barends, Heidi. "Olive Schreiner's The Story of an African Farm: Lyndall as transnational and transracial Feminist." English Academy Review 32, no. 2 (2015): 101-114.</ref>

==Plot introduction==

The novel details the lives of three characters, first as children and then as adults – Waldo, Em and Lyndall – who live on a farm in the [[Karoo]] region of South Africa. The story is set in the middle- to late-19th century – the [[First Boer War]] is alluded to, but not mentioned by name. The book is semi-autobiographical: in particular, the two principal protagonists (Waldo and Lyndall) display strong similarities to Schreiner's life and philosophy.

The book was first published in 1883 in London, under the pseudonym ''Ralph Iron''. It quickly became a best-seller, despite causing some controversy over its frank portrayal of [[freethought]], feminism, premarital sex and pregnancy out of wedlock, and [[transvestitism]].

===Structure===

As there is only minimal narrative in the novel (and what there is only serves to support the book's many themes), it is a difficult book to summarise. In addition, much of the story is revealed as a series of vignettes – often devoid of context, or (deliberately) out of sequence. The author also frequently interjects into the narrative to address the reader directly. In general, the book may roughly be divided into three sections:

* The first section introduces us to the characters as children living on a farm in the [[Karoo]], and reveals their innermost thoughts and feelings. 
* The second section is entitled "Times and Seasons" and largely summarises what has gone before, and alludes to incidents still to come. It deals primarily with Waldo's journey from Christian fanaticism to despairing [[atheism]]. This section is almost completely devoid of chronological context, although much of the philosophy presented is echoed in both the first and the third sections of the book. The second section makes almost exclusive use of the first person plural pronoun ("we", "us" etc.). The use of the plural underscores the fact that Schreiner saw Waldo as something of an [[alter ego]].
* The third section covers the lives of the three main characters as adults. Again, the focus is on Waldo and Lyndall, both of whom leave the farm and travel to different parts of South Africa, Waldo to the [[Natal Province|Natal]] area, and [[Grahamstown]], and Lyndall to [[Bloemfontein]] and then to the [[South African Republic|Transvaal]].

==Plot summary==
===Childhood===

The first section of the book deals with the lives of protagonists as children and teenagers. It reveals some of the events that proved formative in the life of the children.

Waldo is initially presented as a deeply devout Christian, a philosophy he appears to have inherited from his widower father Otto, the kindly German farm-keeper. As the narrative progresses, Waldo becomes increasingly disillusioned with his faith, a crisis brought on by a series of traumatic events, as well as his growing interest in wider philosophical works.

Lyndall has no such qualms. Apparently a [[Freethought|freethinker]], she seems uninterested in religion as a whole. Instead, her focus is more on the status of women in the late 19th century. A seeker after knowledge and autonomy, she is frustrated by the limited choices offered to her as a woman. Lyndall is a [[skepticism|sceptic]] by nature, a strong-willed and independent child who does not hesitate to disobey even her adult supervisors whenever she deems them unworthy of respect.

Em, the stepdaughter of Tant (Aunt) Sannie and cousin to Lyndall is presented as a cheerful, friendly but somewhat ignorant child. Em serves as a character foil to Waldo and Lyndall; she is content to believe whatever she is told by the adults in her life. Em often becomes the scapegoat for Lyndall's rebellion.

The Englishman Bonaparte Blenkins is an inveterate liar and confidence trickster. He arrives at the farm spinning a tale of woe, presenting himself as a successful businessman who has fallen on hard times. Tant Sannie reluctantly agrees to allow Bonaparte to remain on the farm, under the care of Otto. Bonaparte welcomes the opportunity – he is, after all, only interested in winning the heart of Tant Sannie, and thereby her farm.

Bonaparte's cruelty towards the children borders on sadism. He is especially hard on Waldo whom he seems to despise. Bonaparte's actions towards Waldo provide a number of the events that lead to his crisis of faith. On the other hand, Bonaparte appears to be unsure of Lyndall, and even somewhat cowed. When Lyndall directly confronts Bonaparte regarding one of his many lies, he punishes Em in Lyndall's place. Bonaparte continues in his attempts to woo Tant Sannie, until her younger, richer niece Trana comes to visit. Tant Sannie eventually catches Bonaparte attempting to court Trana and ejects him from the farm.

Otto, the German farm-keeper and Waldo's father, is a deeply religious person. He is unceasing in his efforts to convert the native farm workers, much to their amusement. Otto is gregarious, compassionate and friendly, unwilling to think ill of anyone. He even defends Bonaparte on more than one occasion. Bonaparte repays him by contriving to have Tant Sannie terminate Otto's employment as the farm-keeper. Bonaparte succeeds, but Otto succumbs to a heart attack and dies before leaving the farm.

===Times and Seasons===

Although technically the first chapter of Part II of the book, "Times and Seasons", differs in style and narrative from those that surround it. This section deals with Waldo – his name appears just once in the chapter – in the first sentence.

"Times and Seasons" follows the journey of faith from infancy through adulthood. Although ostensibly revolving around Waldo, the frequent use of plural pronouns may indicate that the author is including herself in Waldo's journey, making this section the most personal of the book.

The first year, infancy, is marked by an innominate thirst for something to worship and a wonder at the beauty of nature. This is followed by the beginnings of a coalescence of dogma – [[Angels]] – but the wonder at the beauty of nature remains and indeed appears to increase.

The next section is marked by a specific age – seven years old. The Bible has become the primary focus, and it is read avidly. New concepts are found in its pages. These are brought to the attention of adults, who appear uninterested. At the same time, the beginnings of doubt begin to creep in. Questions are answered in a diffident manner, and the answers reluctantly accepted.

Two years pass, and in that time the natural world recedes, to be replaced by a bittersweet relationship with God and the Bible. The concept of [[Hell]] looms ever larger; doubt in the form of the [[Devil]] assails the child. He asks difficult questions – and the superficial answers of the supposedly wise adults no longer suffice. And yet at the same time, a sense of ineffable peace, the feeling of sins forgiven is felt. But this feeling does not endure – before long, the [[Devil]] again appears with snide questions and the threat of [[Hell]] and Damnation looms again. And so the cycle continues – alternating doubt and serenity.

There is no indication of how much time has passed, but the next section finds the child apparently at ease with a [[Universalism|Universalist]] concept of God. There is no threat of wrath, no Hell, no Damnation. The "Mighty Heart" loves all its children, regardless. The old, questioning Devil has been silenced.

The dream cannot continue – reality rudely awakens the dreamer. He sees the World as it really is, unjust, evil – no evidence of a supernal, all-consuming love. As a consequence, the one-time believer becomes the [[Atheism|Atheist]]. An allusion is made here to the grave of a loved one – whether this refers to Otto or Lyndall (or both) is not clear. The [[Scientific realism|Realist]] now goes through life uncaring, finding some measure of peace in manual labour and the Sciences. The wonder and beauty of Nature again reasserts itself, greatly increased. The rationalist becomes enamoured with the natural world, taking delight in its many mysteries and revelations. In a sense, the old worship of musty religion is replaced by a new and vital worship of nature in all its intricacy. With this worship comes the realisation that all is interconnected, and the section ends with Waldo beginning to live again, at ease with his new world and his place in it.

===Adulthood===

The third section of the book (technically the second chapter of Part II) opens with Waldo on the farm, making a wood-carving. Contextual clues place the time somewhere after the end of Chapter 1 ("Times and Seasons"), when Waldo has entered his rational-universalist phase and appears to be content. Em (who is said to be sixteen years old) visits Waldo with tea and cakes, and announces that the new farm-keeper has arrived, an Englishman (the book later reveals) by the name of Gregory Rose. Sometime after she leaves, a stranger on horseback appears and converses with Waldo. After inquiring about the nature and meaning of Waldo's carving, the stranger relates the Hunter's Allegory.

The Hunter's Allegory is somewhat similar to "Times and Seasons" in theme, tracing the journey from blind superstition to the painful search for Truth, this time using the literary device of [[Allegory]]. Once done with his tale, the stranger leaves after handing Waldo an unnamed book of Philosophy.

This is followed by a recounting of a letter which the new Farm-Keeper, Gregory Rose writes to his sister. From the contents of the letter, Rose is revealed as something of an arrogant misogynist, believing himself destined for higher things than farming, but denied his calling by circumstances beyond himself. Gregory also reveals that he has fallen in love with Em, and intends to marry her.

Upon taking the letter to the farm, Gregory proposes and Em, after some misgivings, accepts. Em also reveals that Lyndall will be returning from Finishing School in six months time, and is eager to introduce Gregory to her cousin.

The following chapter picks up with Lyndall having returned from boarding-school. Em shares her news, but is surprised to discover that Lyndall seems unmoved, even pitying. A little later, Lyndall accompanies Waldo as he performs his chores about the farm. It is here that Schreiner inserts something of a Feminist Manifesto – she discourses at length upon her experiences at school, and rails at the limited status that Society expects of her as a Woman. Waldo is her foil – he asks trenchant questions of her which lead to still further expositions. With some irony, Lyndall ends her diatribe with the observation that Waldo is the only person with whom she can converse – others simply bore her.

With Tant Sannie engaged to yet another husband, a Boer-wedding is planned. Gregory takes some time off to write another letter, in which he apparently denigrates Lyndall and her independent ways. From the tone of the letter, however, it is clear that Gregory somehow finds Lyndall fascinating.

At the Wedding, Gregory contrives excuses to be near Lyndall, she acknowledges him, but appears diffident. It is evident that Gregory is now deeply infatuated with Lyndall. For her part, when Lyndall finds herself in need of company, she seeks out Waldo and again converses with him beneath the stars. However, when Gregory offers to take her back to the farm, she unexpectedly accepts his invitation. Waldo drives Em home, where, we are told, she sits in the dark.

Some time passes, and Waldo has decided to leave the farm to find work. Em says her goodbyes, and then seeks out Gregory. She finds him at his usual occupation: with Lyndall, pretending to read a newspaper. Lyndall hardly acknowledges him. Once Lyndall leaves, Em tells Gregory that she wants to end their engagement. Gregory half-heartedly attempts to change her mind, but quickly agrees. He leaves Em, whistling to himself.

It is Lyndall's turn to bid farewell to Waldo. She tells him that she will never forget him, and muses upon what they may have become when they are reunited. Waldo leaves the farm; Lyndall watches him go until he is out of sight.

The next vignette finds Gregory Rose wandering the farm. It is clear that he is trying to find Lyndall, but is taking great pains to appear nonchalant. Having located Lyndall, he attempts to strike up a conversation, akin to the discourses that Lyndall and Waldo would share. Lyndall answers him, but it is clear that she is subtly mocking Gregory. After enduring no small amount of verbal abuse, Gregory confesses that he loves Lyndall, and would like nothing more than to serve only her, expecting nothing in return. Lyndall agrees to marry him, if he promises to remember his vow – he is to serve her completely, with no expectations of anything in return. From Gregory, Lyndall wants only his name – nothing else. The once-proud Gregory Rose has been shattered against the force of Lyndall's will.

The following chapter reveals some of Lyndall's motivations. A stranger has come to the farm – Lyndall suggests that he be put up in Otto's old cabin for the night. The man is, in fact, Lyndall's lover, who has come in answer to a letter that she sent to him stating that she intends to marry Gregory Rose. She refuses the offer of marriage from her lover because, she explains, she does not consider him to be a fool, as she does Gregory. She fears losing herself if she were to marry the stronger man, whereas a marriage to Gregory Rose would leave her autonomy intact. It is here that the author hints that some other matter may be pressing—it is explicit in later parts of the story: Lyndall is pregnant.

Lyndall then offers her paramour an alternative – she will leave the farm in his company, that very night, on the condition that he releases her whenever she asks. They plan to go to the [[Transvaal Colony|Transvaal]].

Lyndall returns to her room to gather her belongings. On the way there, she stops at Otto's grave to bid him farewell. It is here that she reveals that she is tired and lonely – aching for something to love. After much weeping, she returns to her room and prepares to leave the farm forever.

Time passes: Gregory is performing chores about the farm. He is a broken man – meekly accepting whatever menial work Em assigns to him. While cleaning out the loft, he comes across a chest of women's clothing. He surreptitiously tries on one of the dresses and a ''kapje'' (hooded bonnet). He appears to reach some sort of decision. Gregory descends from the loft, finds Em and tells her that he can no longer live on the farm – everything reminds him of Lyndall. He states that will search for her, whatever it may take. Gregory is under no illusions. He is fully aware that Lyndall would more than likely throw him aside should he find her. He wants only to see her again; to stand where she once stood. Gregory leaves the farm.

Seven months later, Em is startled when Waldo returns without warning one windy night, some eighteen months after he had left. While Em prepares a meal for Waldo, he begins to write a letter to Lyndall. He tells of his experiences and changes that he has undergone in his travels. Waldo continues to write throughout the night, until Em awakes and stops him. Em explains that Waldo cannot write any longer: Lyndall is dead.

Some time later, Gregory returns to the farm, alone. He relates his tale to Em.

At first, the search was without difficulty, from [[Bloemfontein]] and North to the [[Transvaal Colony|Transvaal]], from farm to farm he traced the path left by Lyndall and her lover. Eventually, however, the trail runs cold – it has not occurred to Gregory that Lyndall and her stranger may have parted ways. Unwilling to give up, Gregory travels from hotel to hotel, always without success. Nearing the end of his options, he finds himself at yet another unnamed hotel. He overhears a conversation between the landlady and a [[Mozambique|Mozambiquian]] nurse. The Nurse must leave – her husband wants her back home. The landlady frets that "the lady" is still unwell. While watching the half-closed door of the patient, Gregory catches a glimpse of Doss – Waldo's dog which he bequeathed to Lyndall. Asking the landlady for more information, Gregory is told that a young, delicate lady arrived at the hotel six months earlier. A few days after her arrival, she gave birth, but the infant died less than two hours later. The Mother, herself extremely weak, sat near the grave in the cold rain for hours. When she retired to her bed, the hotel doctor declared that she would never again rise from it.

Gregory hatches a plan. Fetching his belongings, he changes into a dress and a ''kapje''. He shaves himself. Later that day, he returns to the hotel, hoping that the landlady will not recognise him. She does not. Gregory tells her that he is a nurse looking for work. The landlady leads him into the room where he finds Lyndall and Doss. Lyndall agrees that Gregory will be her new nurse. She is very weak.

While Gregory watches over Lyndall, she grows weaker by the day. Although she makes a few half-hearted attempts to eat, or read or leave her bedroom, she never quite manages to shake off the pain that plagues her.

Eventually, Gregory and Lyndall agree to return to the farm. Gregory makes the arrangements, although he knows that Lyndall will not survive the journey. His fears are realised: a few days into the journey, Lyndall wakes one night to find that the fog has lifted from her mind. She sees clearly for the first time. She knows what is about to happen to her. There, under the stars with her eyes fixed on her reflection in a hand-mirror, Lyndall dies.

The news devastates Waldo. Throughout the following night, he searches desperately for some philosophy, some tenet of some religion that will assure him that he and Lyndall will one day be reunited. He can find none – none but the cold, unsatisfying realisation that both he and Lyndall will one day be absorbed into the great universal whole from which they sprang. With this he must be content.

The final vignette takes place at some unspecified point after Gregory's return. Tant Sannie is visiting the farm with her new husband and baby. She relates that she almost caught hold of Bonaparte while attending Church, but the shyster slipped from her fingers. Gregory sits outside, absorbed in his own pain. In a leather bag hung around his neck, he carries the only letter that Lyndall ever wrote to him, just four words: "You must marry Em." After Tant Sannie leaves, Em visits Waldo – he is in his old cabin, building a table for Em. She tells him that she and Gregory are to be married. She leaves Waldo to his work.

Waldo packs away his tools for the day, and goes outside to sit in the sunshine. He carries one of Lyndall's old dancing-slippers in his breast pocket. He appears to be content, once again aware of the wide plains that surround him, and the warmth of the sun on his hands. Em finds him there, hat drawn low, apparently asleep. She leaves a glass of milk for him, thinking that he will be glad to find it when he awakes. But Waldo will not wake again.

==Characters==
===Waldo===
Waldo is the son of the German farm-keeper, Otto Farber (see below). He is presented (initially) as intensely earnest, curious and deeply spiritual.

===Lyndall===
Lyndall is the orphaned niece of Tant Sannie (see below). Her late father was English, while Tant Sannie is Dutch. Lyndall is used to a life of hardship – made all the more difficult by her stubborn and rebellious personality. She is a precocious child, wise beyond her years, but still internally conflicted. Waldo is her best friend and confidante. She grows up to be a feminist after her studies. She dies giving birth and in the care of Gregory.

===Em===
Em is Lyndall's cousin – the stepdaughter of Tant Sannie, her English father having also died early in her childhood. Unlike her Step-Mother, Em is fluent in English as well as Dutch.

===Otto===
Otto Farber is the German farm-keeper. He is presented as a kind, unselfish and caring person.

===Tant Sannie===
Tant Sannie, often called "the Boer-woman," is the owner of the farm (having inherited the land from one of her late husbands). ''Tant'' is an [[Afrikaans]] honorific, literally meaning "aunt".

===Bonaparte Blenkins===
Bonaparte Blenkins is introduced in the first section of the book. He is something of a caricature – an inveterate liar, sadistic, a con-man and a hypocrite. He makes use of his affected piety to have Otto ejected from the farm, while he attempts to woo Tant Sannie into marriage (and thereby gain control of the farm). He ultimately fails in his endeavour, his true nature exposed, and is driven from the farm by a furious Tant Sannie. He is never heard from again, except for a brief encounter near the end of the book.

===Gregory Rose===
Gregory Rose, an Englishman, is hired to take over the duties of farm-keeper after the death of Otto.

===Servants===

Several African servants work for the farmers, including a "[[Hottentot]] maid" and several "[[Kaffir (racial term)|Kaffers]]".

==Adaptation==
The novel was made into [[The Story of an African Farm (film)|a film]] in 2004, directed by [[David Lister (director)|David Lister]].

==References==
{{reflist}}

==External links==
* {{Gutenberg|no= 1441 |name= The Story of an African Farm }}

{{Authority control}}

{{DEFAULTSORT:Story Of An African Farm}}
[[Category:South African novels]]
[[Category:Feminist novels]]
[[Category:1883 novels]]
[[Category:Works published under a pseudonym]]
[[Category:Novels set in South Africa]]
[[Category:Karoo]]
[[Category:South African novels adapted into films]]