{{Infobox military person
|name= Leland Thornton Kennedy
|image= 
|caption= 
|birth_date= {{birth date|mf=yes|1934|01|01}}
|death_date= {{death date and age|mf=yes|2003|12|28|1934|01|01}}
|nickname= Lee
|birth_place= [[Louisville, Kentucky]], U.S.
|death_place= [[Yorktown, Virginia]], U.S.
|placeofburial= [[Arlington National Cemetery]]
|allegiance= {{flag|United States of America}}
|branch= {{nowrap|{{flag|United States Air Force}}}}
|serviceyears= 1955–1985
|rank= [[File:US-O6_insignia.svg|15px]] [[Colonel (United States)|Colonel]]
|unit= 
|commands= 
|battles= [[Vietnam War]]
|awards= [[File:Air Force Cross ribbon.svg|23px|border]] [[Air Force Cross (United States)|Air Force Cross]] (2)<br/>[[File:Silver Star ribbon.svg|23px|border]] [[Silver Star]]<br/>[[File:Legion of Merit ribbon.svg|23px|border]] [[Legion of Merit]]<br/>[[File:Dfc-usa.jpg|20px|link=Distinguished Flying Cross (United States)]] [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]<br/>[[Image:Meritorious Service Medal (United States).png|border|20px]] [[Meritorious Service Medal (United States)|Meritorious Service Medal]] (3)<br/>[[Image:Air Medal ribbon.jpg|border|23px]] [[Air Medal]] (6)
|relations=
|laterwork=
}}
'''Leland Thornton "Lee" Kennedy''' (January 1, 1934 – December 28, 2003) was a career officer and pilot in the [[United States Air Force]], and a highly decorated veteran of the [[Vietnam War]]. Kennedy flew the [[EC-121 Warning Star]] during the [[Cuban Missile Crisis]], and later served two [[tour of duty|tours of duty]] in Vietnam.

In his second Vietnam tour, he distinguished himself as a [[Search and Rescue|combat search and rescue]] helicopter pilot, twice awarded the [[Air Force Cross (United States)|Air Force Cross]] in 1966 for actions occurring 15 days apart in which his crew while under intense fire  rescued seven airmen who had been shot down in enemy territory.<ref name="lee">Frisbee, "Valor: A Tale of Two Crosses"</ref> Kennedy is one of only four airmen to receive multiple awards of the Air Force Cross.

Kennedy also received the [[Silver Star]], the [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]], the [[Air Medal]] with five [[oak leaf cluster]]s, and numerous other medals and campaign ribbons during his career. After 30 years of service, Kennedy retired at [[Langley AFB]], Hampton, Virginia, in 1985.<ref name="news"/>

==Education and family==
From Kennedy's obituary:<ref name="news">(Newport News, Virginia) ''Daily Press'', December 30, 2003.</ref>

<blockquote>Leland (Lee) Thornton Kennedy, age 69, of [[Yorktown, Virginia]], died peacefully Sunday, December 28, 2003, with his family at his side. Kennedy was born January 1, 1934, in [[Louisville, Kentucky]], to Edith and William Kennedy. He graduated from the [[University of Kentucky]], Lexington, in 1955, and during college was a member of [[Air Force ROTC]] and [[Kappa Sigma]] fraternity.</blockquote>

<blockquote>After retirement, Kennedy served his community as a member of the York/Poquoson Social Service Board, and served as Chairman of that organization for three years. In addition, he incorporated his interest in woodworking and miniature ship building into his own business, "Why Knot", for many years. In recent years, Kennedy joined his love of family and his interest in history by researching the genealogical history of his family. He leaves to cherish his memory his loving bride of almost 50 years, Harriet June, as well as three children, seven grandchildren and one great-grandchild. Burial...in [[Arlington National Cemetery]], Washington, D.C.</blockquote>

==Air Force career==
[[File:Lockheed EC-121D Thailand 1972.jpg|right|thumb|EC-121 Warning Star]]
Kennedy was commissioned through the [[Air Force Reserve Officer Training Corps]] program at the University of Kentucky on 30 May 1955, and went on active duty beginning 31 August 1955. After completing [[Undergraduate Pilot Training]] at [[Vance Air Force Base]], [[Oklahoma]], Kennedy entered helicopter training and completed it in March 1957. His first assignment was at [[Oxnard Air Force Base]], [[California]], from March 1957 to March 1959, flying [[Sikorsky H-19|Sikorsky SH-19B]] [[search and rescue]] helicopters, after which he completed Weapons Controller training and was assigned to the 4640th Support Squadron at [[Norton Air Force Base]], California, from December 1960 to August 1962.<ref name="ltkvt">{{cite web | last =| first =| authorlink = | year =2013 | url= http://veterantributes.org/TributeDetail.php?recordID=797 | title = Leland T. Kennedy | work =  | publisher = Veteran Tributes| accessdate= 19 May 2013 | ref = }}</ref>

He was next assigned to the [[963d Airborne Air Control Squadron|963rd Airborne Early Warning and Control Squadron]], [[552d Air Control Wing|552nd AEWC Wing]] at [[McClellan Air Force Base]], [[California]], as a pilot flying [[EC-121 Warning Star]]s. During the [[Cuban Missile Crisis]], he was assigned to its [[966th Airborne Air Control Squadron|966th AEWC Squadron]] based at [[McCoy Air Force Base]], [[Florida]], flying specially-modified EC-121Qs on Gold Digger missions (tracking [[Lockheed U-2]] surveillance flights) and monitoring Cuban airspace.<ref name="ltkvt"/>

In April 1965, still part of the airborne early warning force, Kennedy, now a captain and aircraft commander, was one of several crews of the 552nd AEWCW to be sent to [[Tan Son Nhut Air Base]] near [[Saigon]], [[South Vietnam]], as part of the [[EC-121 Warning Star#Big Eye|Big Eye Task Force]].

Upon completion of his temporary duty in Vietnam, Kennedy completed upgrade training to the [[Sikorsky S-61R|HH-3E]] rescue helicopter at [[Sheppard Air Force Base]], [[Texas]], and became a pilot with the [[Air Rescue Service|Aerospace Rescue and Recovery Service]] (ARRS).<ref name="ltkvt"/>

===Second Southeast Asia tour===
[[File:HH-3E DaNang 37ARRS 1968.jpg|right|thumb|HH-3E "Jolly Green" helicopter. In 1966 the type did not yet have a refueling probe.]]
Kennedy was stationed at [[Udorn Royal Thai Air Force Base]], [[Thailand]], with Detachment 5 of the [[38th Rescue Squadron|38th Aerospace Rescue and Recovery Squadron]], a Combat Search and Rescue (CSAR) unit of six HH-3E "Jolly Green" helicopters. Standard operating procedures dispatched an airborne controller aircraft (a [[Lockheed HC-130|Lockheed HC-130H Hercules]] using the call sign ''Crown''), two helicopters, and two to four [[A-1 Skyraider|A-1H Sandys]] tasked as rescue escort (initially "RESCORT" but later "[[Combat Air Patrol|RESCAP]]") on recovery missions. The force was divided into two tactical elements, with the initial recovery helicopter designated as ''Jolly Low'' and the backup as ''Jolly High''.<ref group=n>Jolly High had as its primary role the recovery of Jolly Green aircrew and passengers, with a secondary mission of replacing Jolly Low in pickup attempts if necessary.</ref> The ResCAP flight was likewise divided as ''Sandy Low'' and ''Sandy High'', with Sandy Low Lead designated as the on-scene commander.<ref>Davis (1999), pp. 24–25</ref>

====5 October 1966====
On his eighth mission, and operating from a forward location at [[Nakhon Phanom Royal Thai Navy Base|Nakhon Phanom RTAFB]] (NKP), Kennedy was aircraft commander ("RCC") of Jolly Green 04<ref name="jg4">{{cite web | last = | first = | authorlink = | coauthors = | url = http://www.jollygreen.org/Research/RonThurlow/NotableH3s.htm| title = Notable H-3 Helicopters| work =  | publisher = jollygreen.org| accessdate = 7 February 2009}}</ref><ref group=n>Some sources, notably LaPointe, list this aircraft as "Jolly Green 02". However jollygreen.org, a website maintained by former SEA rescue crew members and researchers at the [[National Museum of the United States Air Force]], identifies it as "Jolly Green 04" and 66-13290. 66-13290 was placed in storage in 1991 at [[309th Aerospace Maintenance and Regeneration Group|AMARC]] on behalf of the [[National Air and Space Museum]], but since August 2007 has been restored and displayed at the [[Francis S. Gabreski Airport]] on Long Island.</ref> on a CSAR for the crew of Tempest 03, an [[F-4 Phantom II|F-4C Phantom]] of the [[8th Fighter Wing#Vietnam War|433rd Tactical Fighter Squadron]].<ref name="lee"/> His crew consisted of copilot ("RCCP") 1st Lt. Donald R. Harris, flight engineer ("HM") SSgt. Donald J. Hall,<ref group=n>Per [http://www.pownetwork.org/bios/h/h004.htm POW Network entry], SSgt. Hall went [[missing in action]] on 6 February 1967, when his HH-3E was shot down in North Vietnam.</ref> and [[pararescueman]] ("PJ") A2C Robert B. Williamson.<ref name="pjs">LaPointe (2000), p. 234</ref>

The rescue location was 300 miles distant inside North Vietnam, in a [[box canyon]] 20 miles northeast of [[Nà Sản Airport|Nà Sản]].<ref name="lee"/> The F-4 had been an escort fighter for a pair of [[B-66 Destroyer|EB-66 electronic countermeasures (ECM)]] aircraft, and had been shot down by a [[Mikoyan-Gurevich MiG-21|MiG-21]].<ref name="b66">{{cite web | last = | first = | authorlink = | coauthors = | url = http://www.b66.info/guestbook/guestbook.html| title = e-mail by Col. Lacy W. Breckenridge May 23, 2003| work =  | publisher = B-66 "Destroyer"| accessdate = 7 February 2009}}</ref> Kennedy had not yet performed a combat rescue and so received assignment as Jolly High, while Jolly Green 36, piloted by Capt. [[Oliver E. O'Mara, Jr]], was tasked to make the recovery.

In the rescue area two A-1 Sandys, after several strafing runs to drive off approaching troops, drew no fire on a final low pass over the area.<ref name="lee"/> O'Mara crested a ridgeline into the canyon and lowered his hoist to pick up the F-4's aircraft commander, Tempest 03A, with whom he was in radio communication. His helicopter was immediately hit from above by small arms fire, forcing O'Mara to pull out. Tempest 03A on his last transmission radioed that he had been hit in the chest.<ref name="pjs"/><ref group=n>Per [http://www.pownetwork.org/bios/a/a018.htm POW Network entry] Tempest 03A, the aircraft commander Capt. William R. Andrews of the 433rd TFS, was [[killed in action]].</ref> O'Mara made two further rescue attempts in the badly damaged HH-3E before his hoist was knocked out, forcing him to return to base. O'Mara was awarded the Air Force Cross in 1969 for his efforts.<ref name="lee"/>

Common procedure was that the secondary helicopter escorted a heavily damaged primary helicopter back to base, but Kennedy was asked to return to continue the pickup attempts of the F-4 backseater, Tempest 03B, Kennedy's first actual attempt at combat rescue.<ref name="lee"/> On the first try, his aircraft was hit and both enlisted men slightly wounded.<ref name="pjs3">LaPointe (2000), p. 238.</ref> The HH-3E was driven off by fire four times, taking more battle damage on each pass. Despite deteriorating weather conditions, a rupture in their forward fuel tank, and smoke in the aircraft, Kennedy's three crewmen joined him in wanting to try again.<ref name="lee"/><ref name="pjs3"/> In the poor weather, Williamson noted that better reference points were on the left side of the aircraft and urged that Lt. Harris conduct the hover.<ref name="pjs3"/>

On the fifth attempt, hearing gunfire but not struck, the crew dropped the hoist and reeled in Tempest 03B, 1st Lt. Edward W. Garland,<ref name="pjs2">LaPointe (2000), p. 236.</ref> whose parachute filled with rotor wash and nearly entangled the rotors.<ref name="pjs3"/> Kennedy exfiltrated the area at low level, with the ridgetops above them concealed by lowering clouds. Below minimum fuel levels needed to return to Udorn, he flew Jolly Green 04 to Lima Site 36, a forward operating location at Na Khang, [[Laos]]. Kennedy was recommended for the Air Force Cross and his crew members each received the [[Silver Star]] for the rescue .<ref name="pjs4">LaPointe (2000), p. 239.</ref>

====20 October 1966====
[[File:ARRS Vietnam.jpg|right|thumb|USAF combat rescue team: A-1H Sandys, HH-3E Jolly Green, and HC-130P]]
Fifteen days later, on the morning of 20 October, Kennedy launched as Jolly Green 36 (high) on a CSAR with Jolly Green 02 (low), flown by Major Adrian D. Youngblood, to recover another 433rd TFS F-4C crew, that of Avenger 03,<ref name="nam">{{cite web | last = | first = | authorlink = | coauthors = | url = http://www.pjsinnam.com/VN_History/Mission_Reports/pdf_files/20Oct1966.pdf| title = Photostat copy of mission report of Jolly Green 02, Maj. A.D. Youngblood| format = | work =  | publisher = PJ's in Vietnam| accessdate = 2 February 2009}}</ref> shot down in [[Laos]] southwest of [[Đồng Hới]], North Vietnam.  His crew this mission<ref group=n>This helicopter to date has not been documented by serial number. An 8TFW web source lists it as 65-12785, which was the helicopter deliberately crash-landed into [[Operation Ivory Coast|Sơn Tây prison]] on November 20, 1970.</ref> consisted of RCCP 1st Lt. Elmer C. Lavender, HM SSgt. Raymond Godsey, and PJ A1C Robert J. Ward.<ref name="pjs5">LaPointe (2000), p. 242.</ref>

Both F-4 crewmen had landed in trees. Their wingmen had remained in the area, making repeated low level passes without a gun or [[Ammunition#Ordnance ammunition|ordnance]] to try to slow down the hostile forces trying to kill or capture the airmen.<ref name="lacy">{{cite journal| last = Breckenridge| first = Col. Lacy W.| year = 2004 | title = The Rest of the Story| journal = USAF Air and Space Power Journal| volume = XVIII| issue = 4| pages = 12 | url = http://www.scribd.com/doc/1455525/US-Air-Force-win04| accessdate =7 February 2009 | quote = }}</ref><ref group=n>Major Breckenridge reported that his wingman, out of ordnance, dropped wing fuel tanks and lit his [[afterburner]] in treetop attempts to simulate an attack on the enemy.</ref> Jolly Green 02 lowered its hoist to the Phantom's rear seat pilot, 1st Lt. Joseph C. Merrick, who was suspended 150 feet in the air and had lashed himself to a tree.<ref name="lee"/> During the eight minutes of hover needed for Merrick to transfer from his parachute harness to the jungle penetrator and be brought up, Jolly Green 02 was hit numerous times from directly ahead and Merrick was twice wounded. Youngblood rotated its tail towards the gunfire to keep the pilots from being hit.<ref name="nam"/> As soon as Youngblood applied power to climb, the helicopter's [[transmission (mechanics)|transmission]] oil pressure failed, warning of imminent seizure of both engines. Youngblood broadcast a "[[Mayday (distress signal)|Mayday]]" and searched for a field to make an emergency landing,<ref name="pjs5"/> flying six miles before spotting one.<ref name="nam"/>

Orbiting overhead, Kennedy had been unable to see the action because of thickening clouds.<ref name="nam"/> He descended, and after observing a village next to the clearing Youngblood had spotted, directed Jolly Green 02 to a field a half mile away. Aboard Jolly Green 36, Lt. Lavender jettisoned its external fuel tanks and dumped fuel to compensate for the added weight of Youngblood's crew and the F-4 pilot.<ref name="pjs5"/> On the ground about 25 yards from the other helicopter, Kennedy continued dumping fuel with his engines running and rotor turning, despite the risk of explosive vapors.<ref name="lee"/><ref name="pjs5"/>

The crew of Jolly Green 02 and the wounded F-4 pilot ran to board Kennedy's aircraft under fire, during which both enlisted crewmen of JG 02 were also wounded. Carrying nine men, Kennedy performed a "maximum performance" liftoff.<ref name="lee"/> The A-1s covering the rescue then strafed the abandoned helicopter,<ref name="nam"/> during which Sandy 08 was shot down on its second pass and its pilot, Capt. David R. Wagener, killed.<ref name="pjs6">LaPointe (2000), P. 243.</ref> As Kennedy's HH-3E reached an altitude of 2,000 feet, he requested the location of the second crewman, which the Sandys provided.<ref name="nam"/> A [[forward air controller]] (FAC) in an [[O-1 Bird Dog|O-1E Bird Dog]] spotted the F-4's aircraft commander, Major Lacy W. Breckenridge,<ref name="lacy"/> still in a tree and used his wingtip to point to the location.<ref name="nam"/> Kennedy made a high speed descent to hover, but Communist soldiers emerged from the trees, firing at the helicopter with small arms. Airman Ward returned fire with an [[M16 rifle|M-16]].<ref name="pjs6"/> The rescue was effected although under attack the entire time. On the return flight Sandy 05, another escort, reported that it was losing power, and Kennedy escorted it to NKP, where both landed safely.<ref name="lee"/>

====16 February 1967====
Kennedy's unit was redesignated as Detachment 1, [[37th Aerospace Rescue and Recovery Squadron|37th ARRS]] on 16 January 1967<ref group=n>This was the former designation of the HC-130s of the 37th ARRS, which had become the separate 39th ARRS at Udorn in a realignment of units by aircraft type. Thereafter the 37th ARRS consisted of only HH-3E Jolly Greens, the 38th ARRS controlled only [[Kaman HH-43 Huskie|HH-43 "Pedro"]] detachments, and the new 39th ARRS operated the rescue variant of the Hercules.</ref> and relocated to NKP.<ref>Tilford (1980), p. 81</ref> On 6 February Jolly Green 05 (low) from NKP, on a CSAR to rescue a downed Forward Air Controller, was shot down with three of its four crew members [[killed in action]], including the flight engineer on Kennedy's first Air Force Cross mission.<ref group=n>The only survivor was the PJ, A2C [[Duane Hackney]]. Capt. O'Mara in JG 36, the high bird, rescued Hackney.</ref><ref name="jtc">Correll, "A Habit of Heroism"</ref>

On 16 February 1967, Dusty 71, an [[North American F-100 Super Sabre|F-100D]] flown by Col. Frank Buzze, the Deputy Commander of Operations of the [[31st Fighter Wing|31st Tactical Fighter Wing]], was shot down while conducting an interdiction mission along the [[Ho Chi Minh Trail]] in [[Saravane Province]] of southern Laos. Buzze ejected and landed on the hillside in rugged [[karst]]. Covered by his wingman and the FAC that had been working their mission, Covey 54, Buzze hid from searching [[Pathet Lao]] troops as a CSAR was launched by a pair of HH-3Es, Jolly Green 56 (low) and Jolly Green 07 (high), forward-located and on ground alert at [[Quảng Trị]] in Vietnam. From NKP, a second mission also launched, Kennedy in Jolly Green 36 (low) and Capt. Robert L. Powell in Jolly Green 37 (high). Kennedy's crew consisted of RCCP 1st Lt. James A. Colyer, Godsey as HM, and PJ A2C Robert D. Bowers.<ref name="lp292">LaPointe (2000), pp. 292–296</ref>

Jolly Green 56 located Buzze and attempted to hover for pickup but was struck repeatedly from three sides by automatic weapons fire, killing the RCC, Capt. Angelo Pullara, and wounding the RCCP Capt. Jerrold D. Ward. Led by Jolly Green 07 and assisted on the controls by the flight engineer, Ward flew the stricken helicopter to an emergency landing at nearby [[Salavan (city)|Saravane]]. After the aborted pickup, Buzze moved approximately a kilometer out of the area while A-1s continued efforts to suppress the AAA fire.  Jollys 36 and 37 arrived on scene and the on-scene commander again authenticated Buzze's identity and new location. Kennedy then attempted a pickup, but like Jolly Green 56, was struck repeatedly in the hover, damaging a fuel cell and the aft portion of the aircraft, forcing him to return to base. After further suppression runs by the Sandys, Jolly Green 37 made a third attempt and successfully picked up Buzze more than four hours after his shoot-down. For his role in the mission, Kennedy was awarded the Silver Star.<ref name="lp292"/>

===Subsequent career===
"Kennedy flew 99 missions in [[Southeast Asia]], totalling 354 hours of combat flight time...Kennedy was referred to by [[Secretary of the Air Force]] [[Harold Brown (Secretary of Defense)|Harold Brown]] as a major figure in 'one of the most outstanding human dramas in the history of the Air Force.'"<ref name="lee"/>

At the conclusion of his SEA tour, Kennedy was assigned as a CH-3E pilot with the 1042nd Test Squadron at [[Dover Air Force Base]], [[Delaware]], from August 1967 to July 1970; and with the 5040th Helicopter Squadron at [[Elmendorf Air Force Base]], [[Alaska]], from July 1970 to July 1972. From July 1972 to June 1975 he was on staff at the Headquarters, [[Alaskan Command]]. Kennedy served as Assistant Director Region Control Center with the [[21st Air Division]] at [[Hancock Field Air National Guard Base|Hancock Field]], [[New York (state)|New York]], from August 1975 to September 1976, followed by duty as Assistant Deputy Commander for Operations of the 602nd Tactical Air Control Wing at [[Bergstrom Air Force Base]], [[Texas]], from October 1976 to October 1978. He returned to Elmendorf AFB and served with Headquarters, [[Alaskan Air Command]] and as commander of the 531st Aircraft Control and Warning Group (re-designated 11th Tactical Control Group in 1981) from October 1978 to December 1982. His final assignments were as Deputy Director and then Director, Operational Plans and Support in the office of the Deputy Chief of Staff Operations, Headquarters [[Tactical Air Command]] at [[Langley Air Force Base]], [[Virginia]], from December 1982 until his retirement from the Air Force on 1 October 1985.<ref name="ltkvt"/><ref group=n>The citation for the [[Legion of Merit]] awarded Kennedy for his tour at TAC Headquarters reads in part: "As the Senior Battle Staff Director, he was responsible for major planning operations during [[Invasion of Grenada|the Grenada evacuation]] and successful recovery of a United States merchant ship from Cuban waters."</ref>

==Awards and decorations==
<center>
{|
|-
|-
|-
|colspan="4" align="center"|[[File:COMMAND PILOT WINGS.png|200px]]
|-
|-
|colspan="4" align="center" |{{Ribbon devices|number=1|type=oak|ribbon=Air Force Cross ribbon.svg|width=100}}{{Ribbon devices|number=0|type=oak|ribbon=Silver Star ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=0|type=oak|ribbon=Legion of Merit ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=oak|ribbon=Distinguished Flying Cross ribbon.svg|width=100}}
|{{Ribbon devices|number=2|type=oak|ribbon=Meritorious Service Medal ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=5|type=oak|other_device=|ribbon=Air Medal ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=|other_device=|ribbon=Joint Service Commendation ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=|ribbon=Air Force Commendation ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=0|type=|ribbon=AF Presidential Unit Citation Ribbon.png|width=100}}
|{{Ribbon devices|number=3|type=oak|other_device=v|ribbon=Outstanding Unit ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=service-star|ribbon=Outstanding Unit ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=1|type=oak|ribbon=Combat Readiness Medal ribbon.svg|width=100}}
|{{Ribbon devices|number=1|type=service-star|ribbon=National Defense Service Medal ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=oak|ribbon=Armed Forces Expedtionary Medal ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=3|type=service-star|ribbon=Vietnam Service Medal ribbon.svg|width=100}}
|{{Ribbon devices|number=0|type=|other_device=|ribbon=Air Force Overseas Short Tour Service Ribbon.svg|width=100}}
|{{Ribbon devices|number=6|type=oak|ribbon=Air Force Longevity Service ribbon.svg|width=100}}
|-
|{{Ribbon devices|number=0|type=|ribbon=USAF Marksmanship ribbon.svg |width=100}}
|{{ribbon devices|number=|type=service-star|other_device=|ribbon=Vietnam gallantry cross unit award-3d.svg|width=100}}
|{{Ribbon devices|number=0|type=service-star|ribbon=Vietnam Campaign Medal ribbon with 60- clasp.svg|width=100}}
|-
|} 

{| class="wikitable"
|-
|colspan="12" align="center" |[[USAF Aeronautical Ratings#Pilot ratings|Command Pilot]]
|-
|colspan="6" align="center"| [[Air Force Cross (United States)|Air Force Cross]] (w/ oak leaf cluster)
|colspan="6" align="center"| [[Silver Star]]
|-
|colspan="4" align="center"|[[Legion of Merit]]
|colspan="4" align="center"|[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]<ref group=n>The DFC award was for a voluntary night SAR in adverse weather on February 22, 1967.</ref>
|colspan="4" align="center"|[[Meritorious Service Medal (United States)|Meritorious Service Medal]] (three awards)
|-
|colspan="4" align="center"|[[Air Medal]] (w/ five oak leaf clusters)
|colspan="4" align="center"|[[Commendation Medal#Joint Service|Joint Service Commendation Medal]]
|colspan="4" align="center"|[[Commendation Medal#Air Force|Air Force Commendation Medal]]
|-
|colspan="4" align="center"|[[Presidential Unit Citation (United States)|Presidential Unit Citation]]
|colspan="4" align="center"|[[Air Force Outstanding Unit Award]] (with [[Combat "V"]] for Valor, four awards)
|colspan="4" align="center"|[[Air Force Outstanding Unit Award]]
|-
|colspan="4" align="center"|[[Combat Readiness Medal]] (two awards)
|colspan="4" align="center"|[[National Defense Service Medal]] (two awards)
|colspan="4" align="center"|[[Armed Forces Expeditionary Medal]]
|-
|colspan="4" align="center"|[[Vietnam Service Medal]] (w/ three campaign stars)
|colspan="4" align="center"|[[Overseas Service Ribbon#Air Force|Air Force Overseas Short Tour Service Ribbon]]
|colspan="4" align="center"|[[Air Force Longevity Service Award]] (w/ six oak leaf clusters)
|-
|colspan="4" align="center"|[[Marksmanship Ribbon#Air Force|Small Arms Expert Marksmanship Ribbon]]
|colspan="4" align="center"|[[Gallantry Cross (Vietnam)|Vietnam Gallantry Cross]] (unit award)
|colspan="4" align="center"|[[Republic of Vietnam Campaign Medal]]
|-
|}
</center>

==Citations==
[[File:US Air Force Cross medal.svg|right|thumb|100px]]

===AFC first award===
:Captain Leland T. Kennedy
:Department of the Air Force, Special Order GB-68 (February 16, 1967)
Citation:
{{quote|The President of the United States of America, authorized by Title 10, Section 8742, United States Code, takes pleasure in presenting the Air Force Cross to Captain Leland Thornton Kennedy (AFSN: 0-65194), United States Air Force, for extraordinary heroism in military operations against an opposing armed force while serving with Detachment 5, 38th Aerospace Rescue and Recovery Squadron, DaNang Air Base, Vietnam, in action in Southeast Asia on 5 October 1966. On that date, Captain Kennedy, flying as pilot of an unarmed HH-3E rescue helicopter, proceeded deep into hostile territory in attempts to rescue two downed American pilots. Disregarding his own safety, he voluntarily flew through heavy automatic weapons and intense small arms fire to reach the injured airmen. Captain Kennedy succeeded in rescuing one of the downed American; however, heavy ground fire forced him from the area before he could reach the other one. Through his extraordinary heroism, superb airmanship, and aggressiveness in the face of hostile forces, Captain Kennedy reflected the highest credit upon himself and the United States Air Force.<ref name="afc1a">{{cite web | last = | first = | authorlink = | coauthors = | url = http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=15077376 | title = Leland T. Kennedy| format = | work =  | publisher = Find a Grave| accessdate = 3 February 2009}}</ref><ref name="mthov">{{cite web | last = | first = | authorlink = | coauthors = | url = http://projects.militarytimes.com/citations-medals-awards/recipient.php?recipientid=3442| title = Leland Thornton Kennedy| format = | work =  Hall of Valor| publisher = Military Times| accessdate = 19 May 2013}}</ref>}}

===AFC second award===
:Captain Leland T. Kennedy
:Department of the Air Force, Special Order GB-156 (May 9, 1967)
Citation:
{{quote|The President of the United States of America, authorized by Title 10, Section 8742, United States Code, takes pleasure in presenting a Bronze Oak Leaf Cluster in lieu of a Second Award of the Air Force Cross to Captain Leland Thornton Kennedy (AFSN: 0-65194), United States Air Force, for extraordinary heroism in military operations against an opposing armed force as a HH-3E Helicopter Pilot in Detachment 5, 38th Aerospace Rescue and Recovery Squadron, DaNang Air Base, Vietnam, in action in Southeast Asia on 20 October 1966. On that date, Captain Kennedy successfully recovered six downed American airmen. Despite the intense, accurately directed, hostile fire which damaged his own unarmed rescue helicopter, Captain Kennedy, with indomitable courage and professional skill, chose to land next to a disabled companion helicopter and retrieve the crew, plus a previously rescued wounded F-4C pilot. With undaunted determination, Captain Kennedy, then amid hostile fire, sought and successfully recovered the second downed F-4C pilot. This event added luster to the chronicles of heroism recorded in Air Force annals and brought further credit to Captain Kennedy's ability to ignore danger while engaged in the rescue of others. Through his extraordinary heroism, superb airmanship, and aggressiveness in the face of hostile forces, Captain Kennedy reflected the highest credit upon himself and the United States Air Force.<ref name="mthov"/>}}

===Silver Star Award===
:Captain Leland T. Kennedy
:Seventh Air Force, Special Order G-140 (May 15, 1967)
Citation:
{{quote|The President of the United States of America, authorized by Act of Congress July 9, 1918 (amended by an act of July 25, 1963), takes pleasure in presenting the Silver Star to Captain Leland Thornton Kennedy (AFSN: 0-65194), United States Air Force, for gallantry in connection with military operations against an opposing armed force while serving as Pilot of an HH-3E Rescue Helicopter of Detachment 5, 38th Aerospace Rescue and Recovery Squadron, DaNang Air Base, Vietnam, in action in Southeast Asia, on 16 February 1967. On that date, Captain Kennedy, as Rescue Crew Commander of an unarmed HH-3E helicopter, assumed primary recovery responsibility on a downed pilot when another helicopter experienced extensive battle damage and loss of personnel. Without regard for his personal safety and concerned only with saving the life of the survivor, Captain Kennedy committed his aircraft into a box canyon heavily defended by automatic weapons and small arms fire. As he approached a hover near the position of the downed airman, heavy fire raked the aircraft until fuel exhaustion made it necessary to return to a recovery base. By his gallantry and devotion to duty, Captain Kennedy has reflected great credit upon himself and the United States Air Force.<ref name="mthov"/>}}

==Notes==
;Footnotes
{{reflist|group=n}}

;Citations
{{reflist|30em}}

==References==
*Davis, Larry (1999). "USAF Search & Rescue Part 2", ''Wings of Fame'' Volume 14, Aerospace Publishing Ltd, London. ISBN 1-86184-029-2
*LaPointe, Robert L. (2000). ''PJs in Vietnam: The Story of Air Rescue in Vietnam as Seen Through the Eyes of Pararescuemen'', Northern PJ Press. ISBN 0-9708671-0-7
*{{cite book|last=Tilford|first=Earl H., Jr.|title=Search and Rescue in Southeast Asia, 1961–1975|year=1980|publisher=Office of Air Force History/CreateSpace Independent Publishing Platform |isbn= 978-1477547434}}
*{{cite journal | quotes = | last = Correll| first = John T.| year = 2010| title = A Habit of Heroism | journal = AIR FORCE Magazine| volume = 93| issue =January| pages =  | url = http://www.airforcemag.com/MagazineArchive/Pages/2010/January%202010/0110heroism.aspx| accessdate =19 May 2013}}
*{{cite journal| last = Frisbee| first = John | year = 1992 | title = Valor: A Tale of Two Crosses| journal = AIR FORCE Magazine | volume = 75 | issue = February| pages =  | url = http://www.airforce-magazine.com/MagazineArchive/Pages/1992/February%201992/0292valor.aspx| accessdate =2 February 2009 | quote = }}

==External links==
{{Portal|United States Air Force}}
* [http://projects.militarytimes.com/citations-medals-awards/recipient.php?recipientid=3442 Valor Awards for Leland Thornton Kennedy, ''Military Times'' Hall of Valor]
* [http://veterantributes.org/TributeDetail.php?recordID=797 "Leland T. Kennedy" @ Veteran Tributes]
* [http://www.dean-boys.com/966/gold%20digger.htm 966th AEW&CS "Gold Digger Missions" by George Merryman]
* [http://www.jollygreen.org/Research/RonThurlow/SearchForJG36.htm The Search for Jolly Green 36]
* [http://www.virtualwall.org/da/AndrewsWR01a.htm Virtual Wall: William Richard Andrews]

{{DEFAULTSORT:Kennedy, Leland T.}}
[[Category:1934 births]]
[[Category:2003 deaths]]
[[Category:American military personnel of the Vietnam War]]
[[Category:Recipients of the Air Force Cross (United States)]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Silver Star]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:People from Louisville, Kentucky]]