{{Use dmy dates|date=July 2011}}
{{Use Australian English|date=July 2011}}
{{Infobox Journal
| cover =
| discipline = [[Law]]
| abbreviation = Co. secur. law j. 
| frequency = bimonthly (since 1989)
| website = http://sites.thomsonreuters.com.au/journals/category/company-and-securities-law-journal/
| publisher = Law Book Co, Sydney
| country = Australia
| history = 1982-
| ISSN = 0729-2775
}}
The '''''Company and Securities Law Journal''''' is a [[peer review|peer-reviewed]] [[law review|law journal]] published in Australia since 1982.<ref name=NLAcat>National Library of Australia. Catalogue. (database online) [http://catalogue.nla.gov.au/Record/475733 Company and securities law journal] accessed 18 August 2011</ref>

The general editors are [[Bob Baxt]] (founding editor) and Dr Paul Ali. The editorial board includes [[Reginald Ian Barrett]], [[Simon McKeon]] and [[Ian Ramsay]].<ref name=C&SLJ>{{cite web|url=http://sites.thomsonreuters.com.au/journals/category/company-and-securities-law-journal/|title=Company and Securities Law Journal|publisher=Thomson Reuters (Professional) Australia Limited.|accessdate=13 August 2011}}</ref>

The journal offers coverage of:<ref name=C&SLJ/> 
* Company Law
* Takeovers and Public Securities
* Corporate Insolvency
* Corporate Finance
* Securities Industry and Managed Investments
* Current Developments, Legal and Administrative
* Accounting
* Directors’ Duties and Corporate Governance
* Overseas Notes: New Zealand, United Kingdom and Europe, United States of America, Canada, Hong Kong, Singapore and Malaysia.

== Journal Rankings ==
The [[Australian Business Deans Council]] has given this journal a quality rating of "A".<ref>Australian Business Deans Council [http://www.abdc.edu.au/3.43.0.0.1.0.htm ABDC Journal Ratings List] accessed 22 August 2011</ref> The [[Australian Research Council]] has [http://www.arc.gov.au/xls/ERA2010_journal_title_list.xls ranked] this journal in the "C" [http://www.arc.gov.au/era/tiers_ranking.htm tier], although the methodology and utility of such rankings has been challenged by Australian legal scholars<ref>David Hamer, [http://www.theaustralian.com.au/higher-education/opinion/arc-rankings-poor-on-law/story-e6frgcko-1111116734303 "ARC rankings poor on law"], ''The Australian'' 25 June 2008 accessed 22 August 2011.</ref><ref>Margaret Thornton, [http://www.theaustralian.com.au/higher-education/opinion/ire-of-the-beholder/story-e6frgcm6-1111117564294#ixzz1Vksi1pkv "Ire of the beholder"], ''The Australian'' 24 September 2008 accessed 22 August 2011.</ref> and the responsible minister has indicated that this ranking system will be discontinued.<ref>Minister for Innovation, Industry, Science and Research, [http://minister.innovation.gov.au/Carr/MediaReleases/Pages/IMPROVEMENTSTOEXCELLENCEINRESEARCHFORAUSTRALIA.aspx "Improvements to excellence in research in Australia."] (Media Release) 30 May 2011 accessed 22 August 2011.</ref>

== References ==
{{Reflist}}

== External links ==
* [http://sites.thomsonreuters.com.au/journals/category/company-and-securities-law-journal/ thomsonreuters.com.au] landing page at publisher

<!--- Categories --->

[[Category:Articles created via the Article Wizard]]
[[Category:Australian law journals]]
[[Category:Corporate law]]


{{law-journal-stub}}