{{Infobox company
| name             = AkroTech Aviation
| logo             = 
| caption          = 
| type             = [[Privately held company]]
| traded_as        = 
| genre            = <!-- Only used with media and publishing companies -->
| fate             = Out of business 
| predecessor      = 
| successor        = 
| foundation       = <!-- {{Start date|YYYY|MM|DD}} -->
| founder          = 
| defunct          = after 1998
| location_city    = 
| location_country = 
| location         = 
| locations        = 
| area_served      = 
| key_people       = 
| industry         = [[Aerospace]]
| products         = [[Kit aircraft]]
| services         = 
| revenue          = 
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| owner            = 
| num_employees    = 
| parent           = 
| divisions        = 
| subsid           = 
| homepage         = <!-- {{URL|www.example.com}} -->
| footnotes        = 
| intl             = 
}}

'''AkroTech Aviation, Inc.''' was an [[United States|American]] [[aircraft manufacturer]] based in [[Scappoose, Oregon]]. The company specialized in the design and manufacture of [[Homebuilt aircraft|kits]] for [[aerobatic aircraft]]. The company went out of business after 1998.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 112. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref><ref name="EAA">{{cite web|url = http://www.eaa.org/homebuilders/kitplans/gilesg200.asp|title = Giles G-200 & G-202|accessdate = 4 December 2013|last = [[Experimental Aircraft Association]]|year = 2013 |archiveurl = https://web.archive.org/web/20130223142200/http://eaa.org/homebuilders/kitplans/gilesg200.asp |archivedate = 23 February 2013}}</ref>

The company's two designs, the single-seat [[Giles G-200]] and the two-seat [[Giles G-202|G-202]] were built from composites. The company could also supply completed ready-to-fly aircraft for the ''Experimental - exhibition'' category.<ref name="Aerocrafter"/>

== Aircraft ==
[[File:VH-YLF Giles G-202 (9635887434).jpg|thumb|right|[[Giles G-202]] performs]]
{| class="wikitable" align=center style="font-size:90%;"
|-
|+ align=center style="background:#BFD7FF"| '''Summary of aircraft built by AkroTech Aviation'''
|- style="background:#efefef;"
! Model name
! First flight
! Number built
! Type

|-
|align=left| '''[[Giles G-200]]'''
|align=center| 
|align=center|  At least 12
|align=left| Single-seat composite [[aerobatic]] [[monoplane]]
|-
|align=left| '''[[Giles G-202]]'''
|align=center| 
|align=center| At least 26<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=G-202&PageNo=1|title = Make / Model Inquiry Results|accessdate = 3 November 2014|last = [[Federal Aviation Administration]]|date = 3 November 2014}}</ref>
|align=left| Two-seats in [[tandem]] composite [[aerobatic]] [[monoplane]]
|-

|}

==References==
{{Reflist}}
<!-- ==External links== -->
{{AkroTech Aviation aircraft}}

[[Category:Defunct aircraft manufacturers of the United States]]
[[Category:Aerobatic aircraft]]
[[Category:Homebuilt aircraft]]