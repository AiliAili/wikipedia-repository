{{Politics of Abkhazia}}

The '''Public Chamber of Abkhazia''' is an advisory body to the [[President of Abkhazia|President]] of the internationally partially recognised [[Republic of Abkhazia]].

==Creation of the Public Chamber==
The ''law on the Public Chamber of Abkhazia'' entered into force on January 1, 2007 and the first session of the Public Chamber was opened by President Bagapsh on July 20 of that year. Its self-declared purpose is fivefold:
*facilitate public participation in politics
*carry out consultations amongst the public
*formulate recommendations on draft laws
*advise the President on matters of civil society development
*exercise the role of a public watchdog over the activities of executive government institutions and the respect of freedom of expression in Abkhazia

==Composition of the Public Chamber==

The Public Chamber is composed of 35 members drawn from civil society, 13 of whom are nominated by the President, 11 by local administrations and 11 by political parties and social movements.

The Chamber is headed by a [[Secretary of the Public Chamber of Abkhazia|secretary]], a position currently held by [[Natella Akaba]]. Other members include the rector of the [[Abkhazian State University]], [[Aleko Gvaramia]], the writers [[Alexei Gogoi]] and [[Daur Nachkebia]] and former [[Prime Minister of Abkhazia|Prime Minister]] [[Sokrat Jinjolia]].

On 29 October 2013, [[Natella Akaba]] was re-elected as Secretary, while former Prime Minister [[Viacheslav Tsugba]] and [[Valeri Ardzinba]] were elected Deputy Chairmen.<ref name="apress10413">{{cite news | url=http://apsnypress.info/news/10413.html | title=Нателла Акаба вновь избрана секретарем Общественной палаты РА | date=29 October 2013 | agency=[[Apsnypress]] | accessdate=November 9, 2013}}</ref>

On 2 November 2016, Akaba was again re-elected as Secretary, and [[Danil Ubiria]] and Valeri Ardzinba as deputy secretaries.<ref name="press_natella">{{cite news|last1=Jarsalia|first1=Said|title=Нателла Акаба избрана секретарем Общественной палаты четвертого созыва|url=http://apsnypress.info/news/natella-akaba-izbrana-sekretarem-obshchestvennoy-palaty-chetvertogo-sozyva-/|accessdate=2 November 2016|agency=[[Apsnypress]]|date=2 November 2016}}</ref>

==References==
*[http://www.article19.org/pdfs/publications/abkhazia-foi-report.pdf A Survey of Access to Information in Abkhazia and its Impact on People’s Lives] [[ARTICLE 19]], June 2007.
*[http://www.abkhaziagov.org/ru/president/press/news/detail.php?ID=7170 Общественная палата начинает свою работу] July 19, 2007. {{ru icon}}

{{reflist}}

[[Category:Politics of Abkhazia]]


{{Abkhazia-poli-stub}}