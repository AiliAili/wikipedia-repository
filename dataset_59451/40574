'''John Dennis Carthy''' (7/4/1923 - 13/03/1972) was a British zoologist and ethologist whose primary study subjects were the [[sensory systems]] and behaviour of [[invertebrates]].<ref name="nature">{{cite journal|title=Obituary – Dr John Dennis Carthy.|journal=Nature|volume=236|pages=472–473}}</ref> He published 11 books and numerous scientific articles.

== Life and career ==
John D Carthy went to [[Bedford School]], and received his undergraduate training at [[Christ's College, Cambridge|Christ’s College]] at the [[University of Cambridge]].<ref name="nature" /><ref name="two">{{cite journal|title=John Carthy|journal=Journal of Biological Education|date=April 1972|volume=6|issue=2|pages=i–i|doi=10.1080/00219266.1972.9653749}}</ref> During WW2 he served in the Operational Research Section of the [[RAF Bomber Command]],<ref name="three">{{cite journal|title=Contributors|journal=New Scientist|date=15 June 1961|volume=239|page=674}}</ref><ref name="four">{{cite web|title=Polish Ethological Society|url=http://ptetol.nencki.gov.pl/biogramy/carthy.htm}}</ref> after which he returned to Cambridge, and completed first his undergraduate studies and subsequently obtained a PhD at Cambridge in 1950.<ref name="nature" /> He joined the faculty of the Zoology Department at [[Queen Mary University of London#Queen Mary College|Queen Mary College]], London in 1950, where he remained until his death in 1972.<ref name="nature" />
 
In 1967 he became the first Scientific Director of the [[Field Studies Council]],<ref name=nature /> where he led the establishment of the Oil Pollution Research Unit at [http://www.field-studies-council.org/centres/orielton.aspx Orielton] and the setting up of the Epping Forest Conservation Centre.<ref name=nature /><ref name=four /><ref name="five">{{cite journal|title=Obituary- Dr J D Carthy.|journal=Habitat|date=1972|volume=8|issue=4}}</ref><ref name="six">{{cite journal|title=J. D. Carthy  M.A., Ph.D,|journal=F.I. Biol. Journal of the School Natural Science Society|date=1971|volume=9|issue=2}}</ref> He was a fellow of the [[Institute of Biology]] and a Secretary of the [http://www.asab.org/ Association for the Study of Animal Behaviour (ASAB)] and the [[Society for Experimental Biology]] and a council member of the [[Royal Entomological Society]] (vice presidency in 1962).<ref name=nature />

John Carthy was a prominent science communicator, with many appearances on popular radio and television programmes,<ref name=nature /><ref name=four /> e.g. the [[BBC]]’s "Meet the Professor", "Tonight", "Nature Parliament" and "[[Young Scientists of the Year]]".<ref>{{cite web|title=A Listen and Learn Series THE ANIMALS' WORLD - Network Three - 1 June 1960 - BBC Genome|url=http://genome.ch.bbc.co.uk/89faf4a0d85e4e898b2f684e6d52f4cf|website=genome.ch.bbc.co.uk}}</ref><ref>{{cite web|title=Science Fair 66 - BBC One London - 18 August 1966 - BBC Genome|url=http://genome.ch.bbc.co.uk/schedules/bbcone/london/1966-08-18|website=genome.ch.bbc.co.uk}}</ref><ref>{{cite web|title=Who Knows? - BBC Home Service Basic - 30 October 1959 - BBC Genome|url=http://genome.ch.bbc.co.uk/schedules/bbchomeservice/basic/1959-10-30|website=genome.ch.bbc.co.uk}}</ref>

== Books ==
* John D Carthy “Animal Navigation: how Animals Find their Way about” Allen & Unwin, London, 1956
* John D Carthy “An Introduction to the Behaviour of Invertebrates” Allen & Unwin, London, 1958 
* John D Carthy “The World of Feeling” Phoenix House, London, 1960
* John D Carthy & Charles Lionel Duddington (eds) “Viewpoints in Biology” Butterworths, London 1962
* John D Carthy & Francis John Govier Ebling (eds) “The Natural History of Aggression” Academic Press, London, New York,  1964 
* John D Carthy “The Behaviour of Arthropods” W. H. Freeman, San Francisco, 1965
* John D Carthy & Aaron E Klein “Animals and their ways; the science of animal behavior” Natural History Press, Garden City, 1965
* John D Carthy “The Study of Behaviour” St Martins Press, New York, 1966
* John D Carthy & G.E. Newell “Invertebrate Receptors” Academic Press, New York, 1968
* John D Carthy & Don R Arthur “The Biological Effects of Oil Pollution on Littoral Communities: Proceedings of a Symposium Held at the Orielton Field Centre, Pembroke, Wales, on 17th, 18th and 19th February 1968”, Field Studies Council, London, 1968.
* John D Carthy & Colin Threadgall “Animal Camouflage” McGraw Hill, New York, 1972.

== Articles ==
* Odour trails of Acanthomyops fuliginosus. Nature, 166, 1950, p.&nbsp;154;  
* The orientation of two allied species of British ant. I. Visual direction finding in Acanthomyops niger. Behaviour, 3, 1951, pp.&nbsp;275–303;   
* Odour trail laying and following in Acanthomyops fuliginosus. Behaviour, 3, 1951, pp.&nbsp;304–318;   
* Do animals see polarized light? New Scientist, 10, 1961, pp.&nbsp;660–662;   
* Color vision in invertebrates. In: Symp. of the Inst. of Biology, No. 12 “Colour and Life”, London 1964

== References ==
<!-- Inline citations added to your article will automatically display here. See https://en.wikipedia.org/wiki/WP:REFB for instructions on how to add citations. -->
{{reflist}}

{{DEFAULTSORT:Carthy, John}}
[[Category:1923 births]]
[[Category:1972 deaths]]
[[Category:British zoologists]]