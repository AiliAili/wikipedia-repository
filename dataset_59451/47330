{{Infobox pro wrestling championship
| championshipname = CWF Mid-Atlantic Tag Team Championship
| image = 
| image_size = 
| caption = 
| currentholder = Arik Royal and [[Chiva Kid]]
| won = February 11, 2012
| aired = 
| promotion = [[CWF Mid-Atlantic]]
| brand = 
| created = 2000
| mostreigns = The SouthSide Playas (3)
| firstchamp = [[The Dupps]] (Bo and Stan Dupp)
| longestreign = Rob McBride and Tank Lawson (385 days)
| shortestreign = Semper Ferocious (35 days)
| oldest = 
| youngest = 
| heaviest = 
| lightest = 
| pastnames = 
| titleretired = 
| pastlookimages = 
}}
The '''CWF Mid-Atlantic Tag Team Championship''' is a [[professional wrestling]] [[tag team]] [[Championship (professional wrestling)#Tag team championships|championship]] in [[CWF Mid-Atlantic|Carolina Wrestling Federation Mid-Atlantic]] (CWF Mid-Atlantic). It was the original tag team title of the Carolina Wrestling Federation promotion, later used in the [[Frontier Wrestling Alliance]] (2001-2004) and [[Wrestling Superstars Live|AWA Superstars]] (2005-2007) as a regional title, officially representing the [[Mid-Atlantic United States]], while it was a member of the respective governing bodies. It was introduced as the CWF Tag Team Championship in early-2000.

The inaugural champions were [[The Dupps]] ([[Bo Dupp|Bo]] and [[William Mueller|Stan Dupp]]), after being awarded the title in 2000 to become the first CWF Tag Team Champions. The SouthSide Playas (J-Money and LA Cash) hold the record for most reigns, with three. At 385 days, Rob "Boogie Woogie Man" McBride and Tank Lawson's first and only reign is the longest in the title's history. While champions, they also defended the [[WSL World Tag Team Championship|AWA World Tag Team Championship]].<ref name="Tank">{{cite web |url=http://www.angelfire.com/indie/tanklawson/main.html |title=09.08.07 - CWF Mid Atlantic Sumner Civitans SuperBrawl! - Sumner Civitans Ballfield - Greensboro NC |author= |date=September 8, 2007 |work= |publisher=TankLawson.com |accessdate=December 11, 2011}}</ref> Semper Ferocious' (Bobby Wohlfert and Devin Dalton) only reign was the shortest in the history of the title lasting only 35 days. Overall, there have been 26 reigns shared between 16 teams, with two vacancies.

==Title history==
;Key
{| class="wikitable"
|-
|'''#'''
|Order in reign history
|-
|'''Reign'''
|The reign number for the specific set of wrestlers listed
|-
|'''Event'''
|The event in which the title was won
|-
| &mdash;
|Used for vacated reigns so as not to count it as an official reign
|-
|N/A
|The information is not available or is unknown
|-
|'''+'''
|Indicates the current reign is changing daily
|}

===Names===
{|class="wikitable" border="1"
|-
!Name
!Years
|-
|CWF Tag Team Championship
|2000 &mdash; 2001
|-
|FWA-Carolinas Tag Team Championship
|2001 &mdash; 2004
|-
|AWA/CWF Mid-Atlantic Tag Team Championship
|2005 &mdash; 2007
|-
|CWF Mid-Atlantic Tag Team Championship
|2007 &mdash; 
|}

===Reigns===
As of {{CURRENTMONTHNAME}} {{CURRENTDAY}}, {{CURRENTYEAR}}.

{| class="wikitable sortable" width=98% style="text-align:center;"
!style="background:#e3e3e3" width=0%| #
!style="background:#e3e3e3" width=20%|Wrestlers
!style="background:#e3e3e3" width=0%|Reign
!style="background:#e3e3e3" width=10%|Date
!style="background:#e3e3e3" width=0%|Days<br />held
!style="background:#e3e3e3" width=16%|Location
!style="background:#e3e3e3" width=18%|Event
!style="background:#e3e3e3" width=60% class="unsortable"|Notes
!style="background:#e3e3e3;" width=0% class="unsortable"|Ref.
|-
|{{sort|01|1}}
|{{sortname|Bo|Dupp}} and {{sort|Stan Dupp|[[William Mueller|Stan Dupp]]}}<br />{{small|([[The Dupps]])}}
|{{sort|01|1}}
|{{dts|link=off|2000}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|{{sort|Live event|[[House show|Live event]]}}
| Bo and Stan Dupp were awarded the titles.
|
|-
|{{sort|1.5|&mdash;}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|2001}}
|{{sort|zz|&mdash;}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|The championship is vacated when The Dupps leave the promotion.
|align="left"|
|-
|{{sort|02|2}}
|{{sort|Kozmo|Kozmo}} and Super Kozmo<br />{{small|(Los Latinos Locos)}}
|{{sort|01|1}}
|{{dts|link=off|2001|05|19}}
|{{Age in days nts|month1=05|day1=19|year1=2001|month2=11|day2=17|year2=2001}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| In 2001, the title was renamed the FWA-Carolinas Mid-Atlantic Tag Team Championship when the Carolina Wrestling Federation became a U.S. affiliate promotion for the [[Frontier Wrestling Alliance]].
|
|-
|{{sort|03|3}}
|{{sort|L.A. Cash|L.A. Cash}} and J-Money<br />{{small|(SouthSide Playas)}}
|{{sort|01|1}}
|{{dts|link=off|2001|11|17}}
|{{Age in days nts|month1=11|day1=17|year1=2001|month2=03|day2=23|year2=2002}}
|{{sort|Graham|[[Graham, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|04|4}}
|{{sort|Storm|Joe Storm}} and Damien Storm
|{{sort|01|1}}
|{{dts|link=off|2002|03|23}}
|{{Age in days nts|month1=03|day1=23|year1=2002|month2=05|day2=11|year2=2002}}
|{{sort|Graham|[[Graham, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|05|5}}
|{{sort|L.A. Cash|L.A. Cash}} and J-Money<br />{{small|(SouthSide Playas)}}
|{{sort|02|2}}
|{{dts|link=off|2002|05|11}}
|{{Age in days nts|month1=05|day1=11|year1=2002|month2=06|day2=29|year2=2002}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|06|6}}
|{{sort|Powers|Scott Powers}} and Chris Steele<br />{{small|(Main Attraction)}}
|{{sort|02|2}}
|{{dts|link=off|2002|06|29}}
|{{sort|zz|N/A}}
|{{sort|Graham|[[Graham, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|6.5|&mdash;}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|2002}}
|{{sort|zz|&mdash;}}
|{{sort|zz|N/A}}
|{{sort|zz|N/A}}
|align="left"|The championship is vacated when Main Event are stripped as champions after failing to appear for a scheduled title defense.
|align="left"|
|-
|{{sort|07|7}}
|{{sort|Yamaha|Mikael Yamaha}} and Gemini Kid
|{{sort|01|1}}
|{{dts|link=off|2002|12|28}}
|{{Age in days nts|month1=12|day1=28|year1=2002|month2=08|day2=20|year2=2003}}
|{{sort|Graham|[[Graham, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|08|8}}
|{{sort|Attitude|Brad Attitude}} and Brad Rainz<br />{{small|(The SuperBrads)}}
|{{sort|01|1}}
|{{dts|link=off|2003|08|20}}
|{{Age in days nts|month1=08|day1=20|year1=2003|month2=12|day2=27|year2=2003}}
|{{sort|Wentworth|[[Wentworth, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
|
|
|-
|{{sort|09|9}}
|{{sort|Yamaha|Mikael Yamaha}} and Gemini Kid
|{{sort|02|2}}
|{{dts|link=off|2003|12|27}}
|{{Age in days nts|month1=12|day1=27|year1=2003|month2=01|day2=31|year2=2004}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
|
|-
|{{sort|10|10}}
|{{sort|Attitude|Brad Attitude}} and Brad Rainz<br />{{small|(The SuperBrads)}}
|{{sort|02|2}}
|{{dts|link=off|2004|01|31}}
|{{Age in days nts|month1=01|day1=31|year1=2004|month2=03|day2=27|year2=2004}}
|{{sort|Seagrove|[[Seagrove, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| This was a tournament final to [[championship unification|unify]] the ACW and CWF Tag Team Championships.
| <ref name="OWW-CWF">{{cite web |url=http://www.onlineworldofwrestling.com/results/cwf-mid-atlantic/ |title=CWF Mid-Atlantic |author= |year=2012 |work=Results |publisher=OnlineWorldofWrestling.com |accessdate=January 10, 2012}}</ref><ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=54201 |title=CWF Mid-Atlantic/ACW (31.01.2004) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|11|11}}
|{{sort|Munkey|Brass Munkey}} and American Steel Ninja<br />{{small|(The Crew)}}
|{{sort|01|1}}
|{{dts|link=off|2004|03|27}}
|{{Age in days nts|month1=03|day1=27|year1=2004|month2=02|day2=05|year2=2005}}
|{{sort|Ramseur|[[Ramseur, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
|
| <ref name="OWW-CWF"/>
|-
|{{sort|12|12}}
|{{sortname|Ivan|Koloff}} and Sean Powers<br />{{small|(Twisted Aggression)}}
|{{sort|01|1}}
|{{dts|link=off|2005|02|5}}
|{{Age in days nts|month1=02|day1=05|year1=2005|month2=04|day2=19|year2=2005}}
|{{sort|Seagrove|[[Seagrove, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| In 2005, the title was renamed the AWA Mid-Atlantic Tag Team Championship when the promotion became an affiliate for [[Wrestling Superstars Live|AWA Superstars]]. 
| <ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=22633 |title=CWF Mid-Atlantic (05.02.2005) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|13|13}}
|{{sort|J-Money|J-Money}} and Will B. Smooth<br />{{small|(SouthSide Playas)}}
|{{sort|01|1}}
|{{dts|link=off|2005|04|19}}
|{{Age in days nts|month1=04|day1=19|year1=2005|month2=02|day2=04|year2=2006}}
|{{sort|Pfafftown|[[Pfafftown, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
|
| <ref name="OWW-CWF"/>
|-
|{{sort|14|14}}
|{{sort|Stevens|Garry Stevens}} and Mitch Connor
|{{sort|01|1}}
|{{dts|link=off|2006|02|4}}
|{{Age in days nts|month1=02|day1=04|year1=2006|month2=08|day2=19|year2=2006}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| 
| <ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=68365 |title=CWF Mid-Atlantic (04.02.2006) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|15|15}}
|{{sort|McBride|Rob McBride}} and Tank Lawson
|{{sort|01|1}}
|{{dts|link=off|2006|08|19}}
|{{Age in days nts|month1=08|day1=19|year1=2006|month2=09|day2=08|year2=2007}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Live event|[[House show|Live event]]}}
| On December 2, 2006, McBride and Lawson also won the [[WSL World Tag Team Championship|AWA World Tag Team Championship]] from the Extreme Horsemen ([[Steve Corino]] and Ricky Landel) at the AWA Convention Showcase. On July 29, 2007, the title was renamed the CWF Mid-Atlantic Tag Team Championship when the promotion's membership in [[Wrestling Superstars Live|AWA Superstars]] is terminated.
| <ref name="OWW-CWF"/><ref name="McBride">{{cite web |url=http://www.angelfire.com/nc3/boogieman/main.html |title=Boogie & Tank Win Tag Team Titles! |author= |date=August 19, 2006 |work= |publisher=Boogie Woogie Man Rob McBride Online |accessdate=December 11, 2011}}</ref><ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=69388 |title=CWF Mid-Atlantic (19.08.2006) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|16|16}}
|{{sort|Ortega|Jesse Ortega}} and Gemini Kid<br />{{small|(The VIPs)}}
|{{sort|01|1}}
|{{dts|link=off|2007|09|8}}
|{{Age in days nts|month1=09|day1=08|year1=2007|month2=03|day2=15|year2=2008}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Sumner Civitans Ballfield|Sumner Civitans Ballfield (2007)}}
| 
| <ref name="Tank"/><ref name="OWW-CWF"/><ref name="McBride"/>
|-
|{{sort|17|17}}
|{{sort|Xsiris|Xsiris}} and Kamakazi Kid
|{{sort|01|1}}
|{{dts|link=off|2008|03|15}}
|{{Age in days nts|month1=03|day1=15|year1=2008|month2=07|day2=19|year2=2008}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|CWF Championship Wrestling|''CWF Championship Wrestling''}}
|This was a [[Lucha libre|Lucha rules]] match. Xsiris and Kamakazi Kid won the title wrestling as the [[masked wrestlers]] Las Chivas and revealed their identities after the match.
|<ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cwf247.com/news_story.php?sid=031720084 |title=New Mid-Atlantic Tag Team Champs Crowned! |author=CWF Mid-Atlantic |authorlink=CWF Mid-Atlantic |date=March 17, 2008 |work=News |publisher=CWF247.com |archiveurl= |archivedate= |accessdate=December 11, 2011}}</ref><ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=22646 |title=CWF Mid-Atlantic TV Taping (15.03.2008) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|17.5|&mdash;}}
|{{sort|Vacated|[[Glossary of professional wrestling terms#Vacated|Vacated]]}}
|{{sort|zz|&mdash;}}
|{{dts|link=off|2008|07|19}}
|{{sort|zz|&mdash;}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Valediction|Valediction (2008)}}
|align="left"|The championship is vacated when Xsiris and Kamakazi Kid are stripped as champions when Mikael Yamaha wrestled in place of an injured Kamakazi Kid for a scheduled title defense.
|align="left"|<ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cwf247.com/news_story.php?sid=072420081 |title=VALEDICTION - Official Event Results |author=CWF Mid-Atlantic |authorlink=CWF Mid-Atlantic |date=July 24, 2008 |work=News |publisher=CWF247.com |archiveurl= |archivedate= |accessdate=December 11, 2011}}</ref> 
|-
|{{sort|18|18}}
|{{sort|Xsiris|Xsiris}} and Mikael Yamaha
|{{sort|01|1}}
|{{dts|link=off|2008|08|02}}
|{{Age in days nts|month1=08|day1=02|year1=2008|month2=02|day2=21|year2=2009}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Hot August Night|Hot August Night (2008)}}
|Xsiris and Mikael Yamaha defeated The VIPs (Jesse Ortega and Gemini Kid) in a tournament final to win the vacant titles.
|<ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cwf247.com/news_story.php?sid=080520081 |title="TAG TEAM TOURNAMENT" - Official Action Recap |author=CWF Mid-Atlantic |authorlink=CWF Mid-Atlantic |date=August 5, 2008 |work=News |publisher=CWF247.com |archiveurl= |archivedate= |accessdate=December 11, 2011}}</ref> 
|-
|{{sort|19|19}}
|{{sort|Banks|Evan Banks}} and Michael McAllister<br />{{small|(The Syndicate)}}
|{{sort|01|1}}
|{{dts|link=off|2009|02|21}}
|{{Age in days nts|month1=02|day1=21|year1=2009|month2=06|day2=06|year2=2009}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Fallout|Fallout (2009)}}
|Banks and McAllister defeated Yamaha in a [[handicap match]].
|<ref name="OWW-CWF"/><ref>{{cite web |url=http://www.cwf247.com/news_story.php?sid=022720092 |title=Official Results - FALLOUT! |author=CWF Mid-Atlantic |authorlink=CWF Mid-Atlantic |date=February 27, 2009 |work=News |publisher=CWF247.com |archiveurl= |archivedate= |accessdate=December 11, 2011}}</ref> 
|-
|{{sort|20|20}}
|{{sort|Ortega|Jesse Ortega}} and Gemini Kid<br />{{small|(The VIPs)}}
|{{sort|02|2}}
|{{dts|link=off|2009|06|6}}
|{{Age in days nts|month1=06|day1=09|year1=2009|month2=09|day2=05|year2=2009}}
|{{sort|Oxford|[[Oxford, North Carolina]]}}
|{{sort|Wrestling at the Pavilion|Wrestling at the Pavilion (2009)}}
|This was a 3-way match also involving Arik Royale & Chase Dakota.
|<ref name="OWW-CWF"/>
|-
|{{sort|21|21}}
|{{sort|Dollar$|Donnie Dollar$}} and Corey Edsel<br />{{small|(Fatback Enterprises)}}
|{{sort|01|1}}
|{{dts|link=off|2009|09|5}}
|{{Age in days nts|month1=09|day1=05|year1=2009|month2=03|day2=06|year2=2010}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Fatback Rising 2|Fatback Rising 2 (2009)}}
| 
|<ref name="OWW-CWF"/>
|-
|{{sort|22|22}}
|{{sort|King|Marcellus King}} and Jaxon Dane<br />{{small|(Aftermath)}}
|{{sort|01|1}}
|{{dts|link=off|2010|03|6}}
|{{Age in days nts|month1=03|day1=06|year1=2010|month2=06|day2=19|year2=2010}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Beginning's End|Beginning's End (2010)}}
|This was a 3-way match involving Alex Adonis & Butch McLean. 
|
|-
|{{sort|23|23}}
|{{sort|Dollar$|Donnie Dollar$}} {{small|(2)}} and Nick Richards<br />{{small|(Fatback Enterprises)}}
|{{sort|01|1}}
|{{dts|link=off|2010|06|19}}
|{{Age in days nts|month1=06|day1=19|year1=2010|month2=10|day2=30|year2=2010}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Absolute Justice IV|Absolute Justice IV (2010)}}
|This was a 4-way match involving Aftermath, Destiny Inc., and Matt Smith & Chase Dakota. 
|
|-
|{{sort|24|24}}
|{{sort|Dillinger|Ty Dillinger}} and Ray Kandrack
|{{sort|01|1}}
|{{dts|link=off|2010|10|30}}
|{{Age in days nts|month1=10|day1=30|year1=2010|month2=01|day2=08|year2=2011}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|MASKacre!|MASKacre! (2010)}}
| 
| 
|-
|{{sort|24|24}}
|{{sort|Blade|Jason Blade}} and [[Adam Page (wrestler)|Adam Page]]<br />{{small|(Virginia Bombers)}}
|{{sort|01|1}}
|{{dts|link=off|2011|01|8}}
|{{Age in days nts|month1=01|day1=08|year1=2011|month2=08|day2=20|year2=2011}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|New Year’s Knockout|New Year’s Knockout (2011)}}
| 
| <ref>{{cite web |url=http://www.the-adams-apple.com/biography/biography.html |title=Biography |author= |year=2011 |work= |publisher=The-Adams-Apple.com |accessdate=December 11, 2011}}</ref>
|-
|{{sort|25|25}}
|{{sort|Wilkins|Roy Wilkins}} and Walter Eaton<br />{{small|(Gemini's All Stars)}}
|{{sort|01|1}}
|{{dts|link=off|2011|08|20}}
|{{Age in days nts|month1=08|day1=20|year1=2011|month2=10|day2=15|year2=2011}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Johnny Weaver Memorial 2011 Tournament|Johnny Weaver Memorial Tournament (2011)}}
| This was a [[handicap match]].
| <ref>{{cite web |url=http://www.cagematch.de/?id=1&nr=68360 |title=CWF Mid-Atlantic (20.08.2011) |author=Kreikenbohm, Philip |date= |work=Event |publisher=Cagematch.de |accessdate=December 11, 2011}}</ref>
|-
|{{sort|26|26}}
|{{sort|Wohlfert|Bobby Wohlfert}} and Devin Dalton<br />{{small|(Semper Ferocious)}}
|{{sort|01|1}}
|{{dts|link=off|2011|10|15}}
|{{Age in days nts|month1=10|day1=15|year1=2011|month2=11|day2=19|year2=2011}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|CWF Rumble|CWF Rumble (2011)}}
| 
| 
|-
|{{sort|27|27}}
|{{sort|Wilkins|Roy Wilkins}} and Walter Eaton<br />{{small|(Gemini's All Stars)}}
|{{sort|01|1}}
|{{dts|link=off|2011|11|19}}
|{{Age in days nts|month1=11|day1=19|year1=2011|month2=02|day2=11|year2=2012}}
|{{sort|Burlington|[[Burlington, North Carolina]]}}
|{{sort|Ultimate Survivor IX|Ultimate Survivor IX (2011)}}
| This was a [[Steel Cage match]].
| <ref>{{cite web |url=http://www.indyinsiders.com/cwf111911.html |title=CWF Casualties of War |author= |date=November 19, 2011 |work=CWF Mid Atlantic Report |publisher=IndyInsiders.com |accessdate=December 11, 2011}}</ref>
|-
|{{sort|27|27}}
|{{sort|Royal|Arik Royal}} and [[Chiva Kid]]<br />{{small|(A Black Guy & A Little Goat)}}
|{{sort|01|1}}
|{{dts|link=off|2012|02|11}}
|
|{{sort|Greensboro|[[Greensboro, North Carolina]]}}
|{{sort|Casualties of War|Casualties of War (2011)}}
| 
| <ref>{{cite web |url=http://www.indyinsiders.com/cwf021112.html |title=CWF Casualties of War |author= |date=February 11, 2012 |work=CWF Mid Atlantic Report |publisher=IndyInsiders.com |accessdate=December 11, 2011}}</ref>
|}

==References==
;General
*{{Cite web|url=http://www.cwf247.com/champions.php|title=Champions|publisher=[[CWF Mid-Atlantic]]|accessdate=December 12, 2011|archiveurl= |archivedate= }}
*{{cite web |url=http://www.wrestling-titles.com/us/midatlantic/cwf/cwf-ma-t.html |title=CWF Mid-Atlantic Tag Team Title |author= |year=2003 |work=The Great Hisa's Puroresu Dojo |publisher=Wrestling-Titles.com |accessdate=2010-05-30}}
*{{cite web |url=http://www.solie.org/titlehistories/ttcwfma.html |title=CWF Mid-Atlantic Tag Team Title History |author=Sawyer, Grant |year=2011 |work=Solie's Title Histories |publisher=Solie.org |accessdate=2010-05-30}}

;Specific
{{Reflist|2}}

==External links==
{{Portal|Professional wrestling}}
*{{official website|http://www.cwf247.com/}}
*[http://www.genickbruch.com/english/index.php?befehl=titles&titel=2460 CWF Mid-Atlantic Tag Team Title at Genickbruch.com]
*[http://www.cagematch.de/?id=5&nr=350 CWF Mid-Atlantic Tag Team Title at Cagematch.de]

{{DEFAULTSORT:List Of Cwf Mid-Atlantic Tag Team Champions}}
[[Category:CWF Mid-Atlantic championships]]
[[Category:Tag team wrestling championships]]
[[Category:Regional professional wrestling championships]]