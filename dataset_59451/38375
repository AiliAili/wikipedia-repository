{{good article}}
{{infobox weapon
| name               = Bold Orion
| image              = [[File:Bold_Orion_on_trailer_with_B-47_launch_aircraft_in_background.jpg|300px]]
| caption            = Bold Orion, with B-47 launch aircraft
| origin             = [[United States]]
| type               = [[Air-launched ballistic missile]]
<!-- Type selection -->
| is_missile         = yes
<!-- Service history -->
| service            = 1958–1959
| used_by            = [[United States Air Force]]
<!-- Production history -->
| designer           =
| design_date        = 1958
| manufacturer       = [[Martin Aircraft]]
| number             = 12
<!-- General specifications -->
| spec_label         = Two-stage version
| weight             =
| length             = {{convert|37|ft|m}}
| part_length        =
| width              =
| height             =
| diameter           = {{convert|0.79|m|disp=flip|sp=us}}
| crew               =
| passengers         =
<!-- Vehicle/missile specifications -->
| engine             = First stage, [[Thiokol TX-20 Sergeant]]; {{convert|6.66|kN|disp=flip|abbr=on}}
| engine_power       = Second stage, [[ABL X-248 Altair]]; {{convert|12.45|kN|disp=flip|abbr=on}}
| pw_ratio           =
| payload_capacity   =
| fuel_capacity      =
| vehicle_range      = {{convert|1100|mi|km}}
| speed              =
| guidance           =
| steering           =
<!-- Missiles only -->
| wingspan           =
| propellant         = Solid fuel
| ceiling            =
| altitude           =
| depth              =
| boost              =
| accuracy           =
| launch_platform    = [[B-47 Stratojet]]
| transport          =
}}

The '''Bold Orion''' missile, also known as '''Weapons System 199B''' ('''WS-199B'''), was a prototype [[air-launched ballistic missile]] (ALBM) developed by [[Martin Aircraft]] during the 1950s. Developed in both one- and two-[[Multistage rocket|stage]] designs, the missile was moderately successful in testing, and helped pave the way for development of the [[GAM-87 Skybolt]] ALBM. In addition, the Bold Orion was used in early [[anti-satellite weapon]]s testing, performing the first interception of a satellite by a missile.

==Design and development==
The Bold Orion missile was developed as part of [[WS-199|Weapons System 199]], initiated by the [[United States Air Force]] (USAF) in response to the U.S. Navy's [[UGM-27 Polaris|Polaris]] program,<ref>Ball 1980, p.226.</ref> with funding authorised by the [[United States Congress]] in 1957.<ref name="Yen">Yengst 2010, p.37.</ref> The purpose of WS-199 was the development of technology that would be used in new strategic weapons for the USAF's [[Strategic Air Command]], not to deliver operational weapons; a primary emphasis was on proving the feasibility of an air-launched ballistic missile.<ref name="Yen"/><ref name="DS">Parsch 2005</ref><ref name="Stares">Stares 1985, p.109.</ref>

The designation WS-199B was assigned to the project that, under a contract awarded in 1958 to Martin Aircraft, would become the Bold Orion missile.<ref name="DS"/> The design of Bold Orion was simple, using parts developed for other missile systems to reduce the cost and development time of the project.<ref name="DS"/> The initial Bold Orion configuration was a single-stage vehicle, using a [[Thiokol]] TX-20 Sergeant solid-fuel rocket.<ref name="DS"/><ref>Ordway and Wakeford 1960, p.30.</ref> Following initial testing, the Bold Orion configuration was altered to become a two-stage vehicle, an [[Allegany Ballistics Laboratory]] [[Altair (rocket stage)|Altair]] upper stage being added to the missile.<ref name="DS"/><ref>Smith 1981, p.178.</ref>

==Operational history==
Having been given top priority by the Air Force,<ref>''Missiles and Rockets'', volume 5. [https://books.google.com/books?ei=pDU2TdDeBsL78Abz3OSuCQ&ct=result&id=4jD0AAAAMAAJ&dq=%22Bold+Orion%22+%22WS-199B%22&q=%22Bold+Orion%22#search_anchor Washington Countdown]. p.9.</ref> the first flight test of the Bold Orion missile was conducted on May 26, 1958, from a [[Boeing B-47 Stratojet]] carrier aircraft,<ref name="DS"/><ref>Friedman 2000, p.122.</ref> which launched the Bold Orion vehicle at the apex of a high-speed, high-angle climb.<ref name="DS"/><ref name="SOG">Temple 2004, p.111.</ref> The [[zoom climb]] tactic, combined with the [[thrust]] from the rocket motor of the missile itself, allowed the missile to achieve its maximum range, or, alternatively, to reach space.<ref name="SOG"/>

A twelve-flight test series of the Bold Orion vehicle was conducted;<ref name="DS"/> however, despite suffering only one outright failure, the initial flight tests of the single-stage rocket proved less successful than hoped.<ref name="DS"/> Authorisation was received to modify the Bold Orion to become a two-stage vehicle; in addition to the modifications improving the missile's reliability, they increased the range of Bold Orion to over {{convert|1000|mi}}.<ref name="Stares"/><ref>Besserer and Besserer 1959, p.34.</ref> Four of the final six test firings were of the two-stage vehicle; these were considered completely successful, and established that the ALBM was a viable weapon.<ref name="Yen"/><ref name="DS"/>

===ASAT test===
The final test launch of Bold Orion, conducted on October 13, 1959, was a test of the vehicle's capabilities in the [[anti-satellite missile|anti-satellite]] role.<ref name="Peebles"/><ref>''Chronology'' 1961, p.89.</ref> Launched from an altitude of {{convert|35000|ft|m}} from its B-47 mothership, the missile successfully intercepted the [[Explorer 6]] satellite,<ref>Bowman 1986, p.14.</ref> passing its target at a range of less than {{convert|4|mi|km}} at an altitude of {{convert|156|mi|km}}.<ref>Yenne 2005, p.67.</ref><ref name="DS"/> Had the missile been fitted with a nuclear warhead, the satellite would have been destroyed.<ref name="SOG"/><ref>Bulkeley and Spinardi 1986, p.17.</ref>

The Bold Orion ASAT test was the first interception of a satellite by any method, proving that anti-satellite missiles were feasible.<ref name="Peebles">Peebles 1997, p. 65.</ref><ref>Hays 2002, p.84.</ref> However this test, along with an earlier, unsuccessful test of the [[High Virgo]] missile in the anti-satellite role,  had political repercussions; the [[Eisenhower administration]] sought to establish space as a neutral ground for everyone's usage, and the "indication of hostile intent" the tests were seen to give was frowned upon, with anti-satellite weapons development being curtailed shortly thereafter.<ref name="SOG"/><ref>Lewis and Lewis 1987, pp.93–95.</ref>

===Legacy===
The results of the Bold Orion project, along with those from the testing of the High Virgo missile, also developed under WS-199, provided data and knowledge that assisted the Air Force in forming the requirements for the follow-on WS-138A, which would produce the [[GAM-87 Skybolt]] missile.<ref name="DS"/><ref>[[International Aeronautic Federation]]. ''Interavia'' [https://books.google.com/books?id=WyU-AQAAIAAJ&q=%22Bold+Orion%22+%22WS-199B%22&dq=%22Bold+Orion%22+%22WS-199B%22&hl=en&ei=pDU2TdDeBsL78Abz3OSuCQ&sa=X&oi=book_result&ct=result&resnum=3&ved=0CC0Q6AEwAg volume 15], p.814.</ref>

==Launch history==
[[File:Bold Orion on B-47.jpg|thumb|right|Bold Orion on B-47 carrier aircraft]]
{| class="wikitable" border="1"
|-
! Date/Time (GMT)
! Rocket
! Launch site
! Outcome
! Remarks<ref name="EA">[http://www.astronautix.com/fam/bolorion.htm Bold Orion] {{webarchive |url=https://web.archive.org/web/20101120001307/http://www.astronautix.com/fam/bolorion.htm |date=November 20, 2010 }}. ''Encyclopedia Astronautica''. Accessed 2011-01-19.</ref>
|-
|| 1958-05-26        || Single-stage  || [[Cape Canaveral]] || Success || [[Apogee]] {{convert|8|km}}
|-
|| 1958-06-27        || Single-stage  || Cape Canaveral || Failure || Apogee {{convert|12|km}}
|-
|| 1958-07-18        || Single-stage  || Cape Canaveral || Success || Apogee {{convert|100|km}}
|-
|| 1958-09-25        || Single-stage  || Cape Canaveral || Success || Apogee {{convert|100|km}}
|-
|| 1958-10-10        || Single-stage  || Cape Canaveral || Success || Apogee {{convert|100|km}}
|-
|| 1958-11-17        || Single-stage  || Cape Canaveral || Success || Apogee {{convert|100|km}}
|-
|| 1958-12-08        || Two-stage  || Cape Canaveral || Success || Apogee {{convert|200|km}}
|-
|| 1958-12-16        || Two-stage  || Cape Canaveral || Success || Apogee {{convert|200|km}}
|-
|| 1959-04-04        || Two-stage  || [[AMR DZ]] || Success || Apogee {{convert|200|km}}
|-
|| 1959-06-08        || Single-stage  || AMR DZ || Success || Apogee {{convert|100|km}}
|-
|| 1959-06-19        || Single-stage  || Cape Canaveral || Success || Apogee {{convert|100|km}}
|-
|| 1959-10-13        || Two-stage  || AMR DZ || Success || Apogee {{convert|200|km}}
|}

==See also==
{{Portal|Cold War|United States Air Force}}
{{Wepscontent
|see also=
* [[Terra-3]]
|related=
* [[Alpha Draco]]
* [[High Virgo]]
|similar weapons=
* [[ASM-135 ASAT]]
* [[GAM-87 Skybolt]]
* [[NOTS-EV-2 Caleb]]
|lists=
}}

==References==
;Citations
{{reflist|2}}

;Bibliography
{{refbegin}}
*{{cite book |author=1st Session. House Committee On Science And Astronautics. U.S. Congress. 87th Congress |title=A Chronology of Missile and Astronautic Events |url=https://books.google.com/books?ei=3042TbWSD9O_gQe5kPXIAw&ct=result&id=ne4yAAAAMAAJ&dq=%22Bold+Orion%22+missile+%22October+13%22&q=%22Bold+Orion%22#search_anchor |accessdate=2011-01-19 |year=1961 |publisher=Government Printing Office |location=Washington, D.C. |asin=B000M1F3O0 }}
*{{cite book |last1=Ball |first1=Desmond |title=Politics and Force Levels: The Strategic Missile Program of the Kennedy Administration |year=1980 |publisher=University of California Press |location=Berkeley, CA |isbn=0-520-03698-0 |url=https://books.google.com/books?id=9us4Q9iFI7kC&pg=PA226 |accessdate=2011-01-19}}
*{{cite book |last1=Besserer |first1=C.W. |author2=Hazel C. Besserer |title=Guide to the Space Age |year=1959 |publisher=Prentice-Hall |location=Englewood Cliffs. NJ |asin=B004BIGGO6 }}
*{{cite book |last1=Bowman |first1=Robert |title=Star Wars: A Defense Insider's Case Against the Strategic Defense Initiative |year=1986 |publisher=Tarcher Publications |location=Los Angeles |asin=B000NQI6B6  |url=https://books.google.com/books?id=6m7fAAAAMAAJ&q=%22Bold+Orion%22+%22B-47%22&dq=%22Bold+Orion%22+%22B-47%22&hl=en&ei=eZE2Tbi-OcjSgQek2_20Aw&sa=X&oi=book_result&ct=result&resnum=2&ved=0CCgQ6AEwATge |accessdate=2011-01-19}}
*{{cite book |last1=Bulkeley |first1=Rip |author2=Graham Spinardi |title=Space Weapons: Deterrence or Delusion? |url=https://books.google.com/books?id=rmz5T3yEUBQC&pg=PA17&dq=%22Bold+Orion%22+missile&hl=en&ei=X0M2TcfLOsH78AbtpZ3BAw&sa=X&oi=book_result&ct=result&resnum=3&ved=0CC0Q6AEwAjgK#v=onepage&q=%22Bold%20Orion%22%20missile&f=false |accessdate=2011-01-19 |year=1986 |publisher=Barnes & Noble Books |location=Totowa, NJ |isbn=0-389-20640-7}}
*{{cite book |last1=Friedman |first1=Norman |title=Seapower and Space: From the Dawn of the Missile Age to Net-Centric Warfare |year=2000 |publisher=Chatham Publishing |location=London |isbn=978-1-86176-004-3}}
*{{cite book |last1=Hays |first1=Peter L. |title=United States Military Space: Into the Twenty-First Century |url=https://books.google.com/books?id=BJqSBNAMpH4C&pg=PA84&dq=%22Bold+Orion%22+missile&hl=en&ei=kjs2Tdu5C8Wt8Aa0waWaCQ&sa=X&oi=book_result&ct=result&resnum=10&ved=0CEkQ6AEwCQ#v=onepage&q=%22Bold%20Orion%22%20missile&f=false |accessdate=2011-01-19 |year=2002 |series=INSS Occasional Papers |volume=42 |publisher=Air University Press |location=Maxwell AFB, AL}}
*{{cite book |last1=Lewis |first1=John S. |author2=Ruth A. Lewis |title=Space Resources: Breaking the Bonds of Earth |url=https://books.google.com/books?id=E4onaMUeHM8C&pg=PA93&dq=%22Bold+Orion%22+missile&hl=en&ei=ckE2TZu3IoT78Aanq-nMAw&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCIQ6AEwADgK#v=onepage&q=%22Bold%20Orion%22%20missile&f=false |accessdate=2011-01-19 |year=1987 |publisher=Columbia University Press |location=New York |isbn=0-231-06498-5}}
*{{cite book |last1=Ordway |first1=Frederick Ira |author2=Ronald C. Wakeford |title=International Missile and Spacecraft Guide |year=1960 |publisher=McGraw-Hill |location=New York |asin=B000MAEGVC }}
*{{cite web |url=http://www.designation-systems.net/dusrm/app4/ws-199.html |title=WS-199 |first=Andreas |last=Parsch |year=2005 |work=Directory of U.S. Military Rockets and Missiles |publisher=designation-systems.net |accessdate=2010-12-28}}
*{{cite book |last1=Peebles |first1=Curtis |title=High Frontier: The U.S. Air Force and the  Military Space Program |url=https://books.google.com/books?id=cMgdYypcPc8C&pg=PA65 |accessdate=2010-12-28 |year=1997 |publisher=Air Force Historical Studies Office |location=Washington, D.C. |isbn=978-0-7881-4800-2}}
*{{cite book |last1=Smith |first1=Marcia S. |title=United States Civilian Space Programs, 1958–1978; Report Prepared for the Subcommittee on Space Science and Applications |volume=1 |year=1981 |publisher=Government Printing Office |location=Washington, DC |asin=B000VA45WS }}
*{{cite book |last1=Stares |first1=Paul B. |title=The Militarization of Space: U.S. Policy, 1945–1984 |year=1985 |publisher=Cornell University Press |location=Ithaca, N.Y |isbn=978-0-8014-1810-5}}
*{{cite book |last1=Temple |first1=L. Parker, III |title=Shades of Gray: National Security and the Evolution of Space Reconnaissance |url=https://books.google.com/books?id=3I8Xki-NiKMC&pg=PA111 |accessdate=2010-12-28 |year=2004 |publisher=American Institute of Aeronautics and Astronautics |location=Reston, VA |isbn=978-1-56347-723-2}}
*{{cite book |last1=Yengst |first1=William |title=Lightning Bolts: First {{sic|Manuevering|expected=Maneuvering}} Reentry Vehicles |year=2010 |publisher=Tate Publishing & Enterprises |location=Mustang, OK |isbn=978-1-61566-547-1}}
*{{cite book |last1=Yenne |first1=Bill |title=Secret Gadgets and Strange Gizmos: High-Tech (and Low-Tech) Innovations of the U.S. Military |year=2005 |publisher=Zenith Press |location=St. Paul, MN |isbn=978-0-7603-2115-7}}
{{refend}}

;Further reading
{{refbegin}}
*{{cite journal |last=Armagnac |first=Alden P. |date=Jun 1961 |title=U.S. Plans First Warship In Space |journal=Popular Science |volume=178 |issue=6 |location=New York |publisher=Popular Science Publishing |url=https://books.google.com/books?id=ASYDAAAAMBAJ&pg=PA200 |accessdate=2011-01-19}}
{{refend}}

==External links==
{{commons category|Bold Orion}}
* [http://space.skyrocket.de/doc_lau_det/bold-orion.htm Bold Orion ALBM (WS-199B)], ''Gunter's Space Page''.
* [http://space.skyrocket.de/doc_lau_det/bold-orion_2st.htm Bold Orion (2 Stage) ALBM (WS-199B)], ''Gunter's Space Page''.
* [http://www.spaceline.org/galleries/rockets/021.jpg.html Bold Orion], ''spaceline.org''.

{{USAF missiles}}
{{Missile types}}

[[Category:Air-launched ballistic missiles]]
[[Category:Anti-satellite missiles]]
[[Category:Cold War air-to-surface missiles of the United States]]
[[Category:Air-to-surface missiles of the United States]]