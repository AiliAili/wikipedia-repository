The '''sound reduction index''' is used to measure the level of [[sound insulation]] provided by a structure such as a wall, window, door, or ventilator.  It is defined in the series of international standards [[International Organization for Standardization|ISO]] 16283 (parts 1-3) and the older ISO 140 (parts 1-14), or the regional or national variants on these standards.  In the United States, the [[sound transmission class]] rating is generally used instead.  The basic method for both the actual measurements and the mathematical calculations behind both standards is similar, however they diverge to a significant degree in the detail, and in the numerical results produced.

Standardized methods exist for measuring<ref>{{cite web|url=http://www.nti-audio.com/Portals/0/data/en/NTi-Audio-AppNote-Airborne-Sound-Insulation-Index-with-XL2.pdf |title=How to Measure Airborne Sound Insulation'' }}</ref> the sound insulation produced by various structures in both laboratory and field (actual functional buildings and building sites) environments.  A number of indexes are defined which each offer various benefits for different situations.

==Weighted Difference level (D<sub>w</sub>)==
[[File:Sound-Insulation-Index-Measurement.jpg|thumb|Measuring Sound Reduction Index in a classroom]]
The most basic index is the Weighted Difference level D<sub>w</sub>. This index is defined by measuring in [[decibel]]s (dB), the noise level produced on each side of a building element under test (e.g. a wall) when noise is produced in a room on one side (or outdoors) and measured both in the room where the noise is produced and in the room on the other side of the element under test.

This measurement may be carried out by measuring the levels in [[octave]] bands, or in 1/3 octave bands. (the latter is normally used for most applications). The minimum requirements of the standards require for the frequency range from 100 [[Hertz|Hz]] to 3.15&nbsp;kHz to be measured (16 {{frac|1|3}} octave bands). In some situations measurements may be carried out in the bands down to 50&nbsp;Hz and/or up to 10&nbsp;kHz.

The measured levels in each 1/3 octave band (or octave band) from the source room (or area) (S) are then compared to the measured levels in the receiving room (R), and the difference is taken (S-R). this produces a measured difference level 'D' for each frequency band in the measured spectrum.

To produce a single [[integer]] number the measured spectrum is plotted on a graph, and compared against a reference curve (defined in ISO 717-1 for airborne sound insulation, and 717-2 for impact sound insulation). The reference curve is moved in 1 dB steps until the total of the unfavorable deviations (measured points on the graph below the reference graph) is as close to 32 as possible but not greater than 32.

The value of the reference curve at 500&nbsp;Hz is taken as the '''Weighted Difference Level, D<sub>w</sub>'''
This is considered to be approximately equal to the [[A-weighted]] level difference which would be observed if normal speech was used as the test signal.

==Sound Reduction Index (R)==
The Sound Reduction Index is expressed in decibels (dB). It is the weighted sound reduction index for a partition or single component only. This is a laboratory-only measurement, which uses knowledge of the relative sizes of the rooms in the test suite, and the [[reverberation]] time in the receiving room, and the known level of noise which can pass between the rooms in the suite by other routes (flanking) plus the size of the test sample to produce a very accurate and repeatable measurement of the performance of the sampled material or construction.

==Apparent Sound Reduction Index (R')==
This is a field measurement which attempts to measure the sound reduction index of a material on a real completed construction (e.g. a wall between two offices, houses or cinema auditoriums). It is unable to isolate or allow for the result of alternate sound transmission routes and therefore will generally produce a lower result than the laboratory measured value.

The calculation method used to produce the Sound Reduction Index takes into account the relative size of the tested rooms, and the size of the tested panel, and is therefore (theoretically) independent of these features, therefore a 1×1 panel of plasterboard ([[drywall]]) should have the same R<sub>w</sub> as a 10×10 panel.

==Normalized Level Difference (D<sub>n</sub>)==
This is an index which is measured in field conditions, between "real" rooms. It is a measurement which deliberately includes effects due to flanking routes and differences in the relative size of the rooms. It attempts however to normalize the measured difference level to the level which would be present when the rooms are furnished by measuring the quantity of acoustic absorption in the receiving room and correcting the difference level to the level which would be expected if there was 10m<sup>2</sup> Sabine absorption in the receiving room. Detailed, accurate knowledge of the dimensions of the receiving room are required.

==Standardized Level Difference (D<sub>nT</sub>)==
Similar to the normalized level difference, this index corrects the measured difference to a standardized reverberation time. For dwellings the standard reverberation time used is 0.5 seconds, for other larger spaces longer reverberation times will be used. 0.5 seconds is often cited as approximately average for a medium sized, carpeted and furnished living room. Due to not requiring detailed and accurate knowledge of the dimensions of the test rooms, this index is easier to obtain, and arguably of slightly more relevance.

Once the difference level or sound reduction index is obtained, the weighted value may be obtained from the corrected spectrum as described above from the reference curve.

==Impact sound insulation==
{{Empty section|date=October 2009}}

==Building regulations==
{{Empty section|date=October 2009}}

==References==
{{Reflist|30em}}

* ISO 16283 ([https://www.iso.org/obp/ui/#iso:std:iso:16283:-1:ed-1:v1:en Part 1: Airborne sound insulation], [https://www.iso.org/obp/ui/#iso:std:iso:16283:-2:ed-1:v1:en Part 2: Impact sound insulation], [https://www.iso.org/obp/ui/#iso:std:iso:16283:-3:ed-1:v1:en Part 3: Façade sound insulation])
*[https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/468870/ADE_LOCKED.pdf United Kingdom HM Government The Building regulations 2010 Part E: Resistance to the passage of sound]
* ISO 140 parts 1 - 14
* ISO 717 parts 1, 2
* [https://www.youtube.com/watch?v=PBxUJk7evkc 360 video of a reverberation chamber used for measuring sound insulation]

{{DEFAULTSORT:Sound Reduction Index}}
[[Category:Acoustics]]
[[Category:Construction]]
[[Category:Noise reduction]]