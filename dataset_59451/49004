{{Infobox school
| name = Kearns-Saint Ann Catholic School
| image = Kearns - St. Ann's School north elevation.JPG
| imagesize = 250px
| established = 1899
| type = [[Private school|Private]], [[Coeducational]]
| religion = [[Roman Catholic]]
| grades = [[Preschool|PS]]–[[Eighth grade|8]]
| streetaddress = 439 East 2100 South
| city = [[South Salt Lake, Utah|South Salt Lake]]
| state = [[Utah]]
| country = USA
| zipcode = 84115
| coordinates = 
| motto = Over a Century of Faith, Hope, and Love
| motto_translation =
| accreditation = [[Western Catholic Education Association]] 
| mascot = Falcons
| colors = [[Blue]] and [[Gold (color)|Gold]] {{color box|blue}}{{color box|gold}}
| free_label3       =
| free_text3        =
| free_label4       =
| free_text4        =
| homepage =http://www.ksaschool.org
}}


'''Kearns-Saint Ann School''' is a Catholic school located in [[South Salt Lake]], Utah, built in 1899.<ref>[http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref>   It is home to students in grades Preschool through 8th grade.  Kearns-Saint Ann School began as an orphanage in the late 1800s, and continues its legacy of the caring for and educating of children which was begun in 1891 by the [[Sisters of the Holy Cross]].<ref>[http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref>

==Kearns' St. Ann's Orphanage==
===Thomas Kearns===
In the late 1800s, [[Thomas Kearns]] was one of Utah’s wealthiest and most influential figures. He had made his fortune with his partners, John Judge and David Keith through their business, the Silver King Mine in [[Park City, Utah]].<ref>[http://historytogo.utah.gov/people/thomaskearns.html ], Utah State's "History to Go"</ref> He was also the publisher of the [[Salt Lake Tribune]] newspaper, and continued his leadership role in early Utah when he became a United States Senator.<ref>[http://historytogo.utah.gov/people/thomaskearns.html], Utah State's "History to Go"</ref>

===The Orphanage===
Due to the high number of mining accidents and disasters in Utah mines, many families were left without a breadwinner in the days when women could not find high-paying jobs. There was also a high number of orphaned children as a result of the mining disasters.  The Sisters of the Holy Cross saw a need for an orphanage and opened one in 1891. Bishop [[Lawrence Scanlan]], of the [[Roman Catholic Diocese of Salt Lake City|Catholic Diocese of Salt Lake City]], gave the sisters a small, two-story adobe building for the purpose of caring for the orphans.<ref>[https://www.dioslc.org/history/the-orphanage], Catholic Diocese of Salt Lake City</ref>

===The Kearns' St. Ann's Orphanage Building===
In a very short time, the orphan population grew to the point that the building was not big enough to house them.  Bishop Scanlan tried to acquire land on which to build an orphanage, but ran into financial problems.<ref>[http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref>
In 1898, Jennie Judge Kearns, wife of Senator Thomas Kearns, donated $60,000 to the diocese for the purpose of building an orphanage. Her gift covered the entire cost of purchasing the land and construction of the building.<ref>[https://news.google.com/newspapers?nid=336&dat=19001006&id=8XpaAAAAIBAJ&sjid=L0oDAAAAIBAJ&pg=4010,3197823&hl=en],  Deseret News (October 6, 1900)</ref> The furnishings were provided by other donors, a list of whom was printed in the [[Deseret News]]. The acreage on which the orphanage was built consisted of rich farmland where the sisters would grow food for feeding the orphans.<ref>[http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref> Today that land is a baseball field used by children's sports teams for practice and games. 

The building was designed and built by noted architect, [[Carl M. Neuhausen|Carl M Neuhausen]], designer of the Cathedral of the Madeleine and the Kearns Mansion, both located in Salt Lake City, UT.  He told Bishop Scanlan that he would be glad to give his services free of cost.<ref>[https://news.google.com/newspapers?nid=336&dat=19001006&id=8XpaAAAAIBAJ&sjid=L0oDAAAAIBAJ&pg=4010,3197823&hl=en], Deseret News (October 6, 1900),</ref> The cornerstone was laid on August 27, 1899. The next day the deed was officially transferred to the Diocese of Salt Lake for the sum of $10.00.<ref>[https://news.google.com/newspapers?nid=336&dat=19010828&id=gr9PAAAAIBAJ&sjid=DFQDAAAAIBAJ&pg=4272,4915513&hl=en], Deseret News (August 28, 1900)</ref> [[Kearns-St. Ann's Orphanage|Kearns-St. Ann Orphanage]] was dedicated on October 7, 1900,<ref>[https://news.google.com/newspapers?nid=336&dat=19001006&id=8XpaAAAAIBAJ&sjid=L0oDAAAAIBAJ&pg=4010,3197823&hl=en], Deseret News (October 6, 1900)</ref> and quickly became home to 92 children and the Sisters of the Holy Cross who cared for them.<ref>[https://news.google.com/newspapers?nid=336&dat=19001006&id=8XpaAAAAIBAJ&sjid=L0oDAAAAIBAJ&pg=4010,3197823&hl=en], Deseret News (October 6, 1900)</ref>
[[File:KSA early days.jpg|thumb|Kearns' St. Ann's Orphanage shortly after opening in 1900]]

===Care of the Children===
Funding for the orphanage was difficult and relied heavily on donations from benefactors in the Salt Lake area. Another wealthy miner, Patrick Phelan, left a large endowment to the orphanage when he died in 1901.<ref>[https://news.google.com/newspapers?nid=336&dat=19011011&id=Gb9PAAAAIBAJ&sjid=zVMDAAAAIBAJ&pg=3212,2051024&hl=en], Deseret News (October 11, 1901) (</ref> 

By 1902 the number of orphans had climbed to 127.  Any child who was in need of a home was accepted into the orphanage regardless of their religion.<ref>[https://news.google.com/newspapers?nid=336&dat=19020524&id=jHQzAAAAIBAJ&sjid=MUoDAAAAIBAJ&pg=1959,466472&hl=en], Deseret News (May 24, 1902)</ref> The sisters worked with the residents of Salt Lake City to provide a very loving home for the children. Many times, the local residents would provide tickets to the circus or [[Lagoon_(amusement_park)]]. Christmas especially showed the generosity of the people of Salt Lake, as no child went without a gift.<ref>[https://news.google.com/newspapers?nid=336&dat=19091224&id=pqhNAAAAIBAJ&sjid=O0gDAAAAIBAJ&pg=6711,4313242&hl=en], Deseret News (December 24, 1909)</ref> 
[[File:KSA Orphans.jpg|thumb|A gathering of the orphans on the steps of the Kearns' St. Ann's Orphanage]]

Kearns' St. Ann's Orphanage continued operating under the direction of the Sisters of the Holy Cross until 1954, when the Utah State Foster Care System was created, and the need for orphanages no longer existed.  However, the legacy of the founders continued when St. Ann's Orphanage was converted into a parochial school beginning in 1955.<ref>[http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref>

==St. Ann's School==
When St. Ann's School opened in fall of 1955, the leadership transferred from the Sisters of the Holy Cross to the [[Sisters of Charity of the Incarnate Word]].  St. Ann’s opened with an enrollment of 240 students in grades kindergarten through 4th grade.  A grade was added each year, until students in all eight grades were enrolled.<ref>  [http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref> As the number of sisters declined in America, they were replaced with lay-teachers. The sisters continued their presence in the school until the early twenty-first century.

==Kearns-Saint Ann School==
===Renovation===
In the early 1990s, St. Ann's School had reached a crisis point over the aging building.  A decision had to be made regarding whether to demolish and rebuild the nearly one-hundred year old crumbling building, or renovate it and bring it into the twenty-first century while preserving its historic integrity. Monsignor John J. Sullivan, pastor of St. Ann’s Parish met with the parish council to solve this problem. The decision was made to renovate and preserve its historic presence in Salt Lake City.<ref>[http://utahheritagefoundation.com/tours-and-events/heritage-awards/2001ha/item/450-kearns-saint-ann-school-south-salt-lake#.VZ6JjmDJDIU], Utah Heritage Foundation</ref>

Many changes had to be made to bring the building up to the code of the 1990s without taking away from the architectural integrity created by Carl M. Neuhausen, nearly one hundred years prior.  Architect James Glascock was hired to design the renovation.  The implementation of his design was directed by contractor John Cameron and Cameron Construction.<ref>[http://utahheritagefoundation.com/tours-and-events/heritage-awards/2001ha/item/450-kearns-saint-ann-school-south-salt-lake#.VZ6JjmDJDIU], Utah Heritage Foundation</ref>


In 1980 it was placed on the National Register of Historic Places<ref>[http://archive.sltrib.com/article.php?id=2993028&itype=NGPSID], [Salt Lake Tribune Newspaper]</ref>  and is listed on the Utah Division of State History as a historic site.<ref> http://heritage.utah.gov/apps/history/markers/detailed_results.php?markerid=2265], Utah Division of State History</ref> In honor of the founders and benefactors of Kearns' St. Ann's Orphanage, the school was renamed Kearns-Saint Ann School in 1991.<ref>[http://www.deseretnews.com/article/166434/ST-ANN-SCHOOL-WILL-BE-RENAMED.html?pg=all], Deseret News</ref>

===Today===

Today the school ministers to students in grades Preschool through 8th and serves a diverse student population from varied socio-economic backgrounds. Kearns-Saint Ann School has been called one of the most diverse schools in the state of Utah, with about one-fourth of its students being refugees.<ref>[http://www.ksl.com/?nid=148&sid=33969030] KSL Channel 5 (NBC), Utah (2015)</ref><ref>[https://news.google.com/newspapers?nid=336&dat=19910921&id=fUNTAAAAIBAJ&sjid=XoQDAAAAIBAJ&pg=5544,1618999&hl=en], Deseret News</ref> Kearns-Saint Ann School has been nurturing young minds since 1899.

==References==
{{reflist|}}

==External links==
*{{official website|www.ksaschool.org/}}

{{coord|40.7248|-111.8788|type:edu_region:US-UT|display=title}}

[[Category:Roman Catholic schools in Utah]]
[[Category:Orphanages in the United States]]
[[Category:Schools in Salt Lake City]]
[[Category:Educational institutions established in 1899]]
[[Category:1899 establishments in Utah]]