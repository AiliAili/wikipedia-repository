{{Infobox journal
| title = Journal of Applied Behavior Analysis
| cover = 
| discipline = Psychology, [[applied behavior analysis]]
| abbreviation = 
| editor = Gregory P. Hanley<ref name=Journal_of_ABA>{{citeweb|url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703|author=Hanley, Gregory P.|title=Journal of Applied Behavior Analysis|work=[[Wiley Online Library]]|date=2015|accessdate=March 20, 2017}}</ref>
| publisher = [[Wiley-Blackwell]] on behalf of the [[Society for the Experimental Analysis of Behavior]]
| country = [[United States]]
| frequency = Quarterly
| history = 1968–present
| impact = 1.088
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703/issues
| link2-name = Online archive
| ISSN = 0021-8855
| eISSN = 1938-3703
| OCLC = 1783308
| LCCN = 
}}
The '''''Journal of Applied Behavior Analysis''''' (JABA) is a quarterly [[peer-reviewed]] [[academic journal]] which publishes empirical research related to [[applied behavior analysis]]. It was established in 1968 and is published by [[Wiley-Blackwell]] on behalf of the [[Society for the Experimental Analysis of Behavior]]. The [[editor-in-chief]] is Gregory P. Hanley ([[Western New England University]]). 

The journal currently consists of three broad areas of research: [[secondary education|secondary]] and [[higher education]], early intensive behavioral interventions (EIBIs) for children with developmental disabilities (particularly [[autism]]), and [[behavioral medicine]] (i.e., voucher-based [[contingency management]] for [[substance abuse]] and pediatric feeding therapy).<ref name=Journal_of_ABA>{{citeweb|url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703|author=Hanley, Gregory P.|title=Journal of Applied Behavior Analysis|work=[[Wiley Online Library]]|date=2015|accessdate=March 20, 2017}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{Columns-list|colwidth=30em|
*[[Current Contents]]/Social & Behavioral Sciences<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2015-11-13}}</ref>
*[[Current Index to Statistics]]
*[[EBSCO Information Services|EBSCO databases]]
*[[Education Resources Information Center]]
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/0174763 |title=Journal of Applied Behavior Analysis |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-11-13}}</ref>
*[[InfoTrac]]
*[[ProQuest|ProQuest databases]]
*[[PsycINFO]]/[[Psychological Abstracts]]
*[[Répertoire International de Littérature Musicale]]
*[[Scopus]]
*[[Social Sciences Citation Index]]<ref name=ISI/>
*[[SocINDEX]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.088.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Applied Behavior Analysis |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
*''[[Journal of the Experimental Analysis of Behavior]]''

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1938-3703}}

{{Psychology|state=collapsed}}

{{DEFAULTSORT:Journal of Applied Behavior Analysis}}
[[Category:Behaviorism journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1968]]
[[Category:Quarterly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Psychotherapy journals]]