{{Use mdy dates|date=February 2014}}
{{Infobox legislation
|short_title=Cybercrime Prevention Act of 2012
|long_title=An Act Defining Cybercrime, Prevention, Investigation, Suppression and the Imposition of Penalties Therefor and for Other Purposes
|citation=[http://www.gov.ph/2012/09/12/republic-act-no-10175/ Republic Act No. 10175]
|territorial_extent=[[Philippines]]
|legislature=[[Congress of the Philippines]]
|enacted_by=[[House of Representatives of the Philippines]]
|date_enacted=June 4, 2012 
|enacted_by2=[[Senate of the Philippines]]
|date_enacted2=June 5, 2012
|date_signed=September 12, 2012
|signed_by=[[Benigno Aquino III]]
|date_commenced=October 3, 2012<ref group="note" name="SC">The law was [[Temporary restraining order|restrained]] by the [[Supreme Court of the Philippines|Supreme Court]] from October 9, 2012 to February 18, 2014. Sections 4(c)(3), 4(c)(4) [on online [[libel]]; only where it penalizes those who simply receive the post or react to it], 5 [only in relation to sections 4(c)(2), 4(c)(3), and 4(c)(4)], 7 [only in relation to sections 4(c)(2) and 4(c)(4)], 12, and 19 were [[Judicial review in the Philippines|struck down]] by the Court for being [[Constitutionality|unconstitutional]].</ref>
|bill=An Act Defining Cybercrime, Providing for the Prevention, Suppression and Imposition of Penalties Therefor and for Other Purposes
|bill_citation=[http://www.congress.gov.ph/download/billtext_15/hbt5808.pdf House Bill 5808]<ref group="note" name="HoR">[http://www.congress.gov.ph/legis/search/hist_show.php?congress=15&save=1&journal=&switch=0&bill_no=HB05808 Legislative History at the House of Representatives]</ref>
|bill_date=February 9, 2012
|introduced_by=Susan Yap ([[Legislative districts of Tarlac#Second District|Tarlac]])
|1st_reading=February 13, 2012
|2nd_reading=May 9, 2012
|3rd_reading=May 21, 2012
|conf_committee_passed=June 4, 2012
|bill2=An Act Defining Cybercrime, Providing for Prevention, Investigation and Imposition of Penalties Therefor and for Other Purposes
|bill_citation2=[http://www.senate.gov.ph/lis/bill_res.aspx?congress=15&q=SBN-2796 Senate Bill 2796]
|bill_date2=May 3, 2011
|introduced_by2=[[Edgardo Angara]]
|1st_reading2=May 3, 2011
|2nd_reading2=January 24, 2012
|3rd_reading2=January 30, 2012
|conf_committee_passed2=June 5, 2012
|white_paper=
|date_conf_committee=May 30, 2012
|committee_report=[http://senate.gov.ph/lisdata/1365611488!.pdf Joint Explanation of the Conference Committee on the Disagreeing Provisions of Senate Bill No. 2796 and House Bill No. 5808]
|amendments=
|repeals=
|related=
|summary=
|keywords=[[Aiding and abetting]], [[defamation]], [[fraud]], [[obscenity]], [[trespass to chattels]]
|status=in force
}}
The '''Cybercrime Prevention Act of 2012''', officially recorded as '''Republic Act No. 10175''', is a [[List of Philippine laws|law]] in the [[Philippines]] approved on September 12, 2012. It aims to address legal issues concerning online interactions and the [[Internet in the Philippines]]. Among the cybercrime offenses included in the bill are [[cybersquatting]], [[cybersex]], [[child pornography]], [[identity theft]], illegal access to data and [[libel]].<ref>[http://www.gov.ph/2012/09/12/republic-act-no-10175/ Republic Act No. 10175] AN ACT DEFINING CYBERCRIME, PROVIDING FOR THE PREVENTION, INVESTIGATION, SUPPRESSION AND THE IMPOSITION OF PENALTIES THEREFOR AND FOR OTHER PURPOSES. Approved by President of the Philippines BENIGNO S. AQUINO III on September 12, 2012</ref>

While hailed for penalizing illegal acts done via the Internet that were not covered by old laws, the act has been criticized for its provision on criminalizing libel, which is perceived to be a curtailment in freedom of expression.

On October 9, 2012, the [[Supreme Court of the Philippines]] issued a temporary restraining order, stopping implementation of the Act for 120 days, and extended it on 5 February 2013 "until further orders from the court."<ref name=gma1>{{cite news|title=SC extends TRO on cybercrime law|url=http://www.gmanetwork.com/news/story/293478/news/nation/sc-extends-tro-on-cybercrime-law|accessdate=5 February 2013|newspaper=GMA News Online|date=5 February 2013}}</ref><ref name=sunstar1>{{cite news|title=SC won't lift TRO on cybercrime law|url=http://www.sunstar.com.ph/breaking-news/2013/02/05/sc-wont-lift-tro-cybercrime-law-266578|accessdate=5 February 2013|newspaper=Sunstar|date=5 February 2013}}</ref>

On May 24, 2013, The DOJ announced that the contentious online libel provisions of the law had been dropped.<ref name="revised">[http://www.gmanetwork.com/news/story/309704/scitech/technology/doj-to-drop-online-libel-from-revised-cybercrime-law DOJ to drop online libel from revised cybercrime law] Mark Merueñas, GMA network May 23, 2013</ref>

On February 18, 2014, the Supreme Court ruled that section 5 of the law decision was constitutional, and that sections 4-C-3, 7, 12 and 19 were unconstitutional.<ref name="gmanetwork.com">{{cite news|last1=Meruenas|first1=Mark|title=Internet libel in cybercrime law constitutional – SC|url=http://www.gmanetwork.com/news/story/348945/scitech/technology/internet-libel-in-cybercrime-law-constitutional-sc|accessdate=20 May 2016|work=GMA News Online|date=18 February 2014}}</ref>

==History==
The Cybercrime Prevention Act of 2012 is the first law in the Philippines which specifically criminalizes [[computer crime]], which prior to the passage of the law had no strong legal precedent in Philippine jurisprudence.  While laws such as the [[Electronic Commerce Act of 2000]] (Republic Act No. 8792<ref>[http://www.wipo.int/wipolex/en/details.jsp?id=3453 Electronic Commerce Act of 2000] (Republic Act No. 8792)</ref>) regulated certain computer-related activities, these laws did not provide a legal basis for criminalizing crimes committed on a computer in general: for example, Onel de Guzman, the computer programmer charged with purportedly writing the [[ILOVEYOU]] computer worm, was ultimately not prosecuted by Philippine authorities due to a lack of legal basis for him to be charged under existing Philippine laws at the time of his arrest.<ref>{{cite news| url=https://www.nytimes.com/2000/08/22/business/technology-philippines-to-drop-charges-on-e-mail-virus.html | work=[[The New York Times]] | title=Technology; Philippines to Drop Charges on E-Mail Virus | first=Wayne | last=Arnold | date=22 August 2000 | accessdate=3 October 2012}}</ref>

The first draft of the law started in 2001 under the Legal and Regulatory Committee of the former Information Technology and eCommerce Council ([http://www.itecc.gov.ph/ ITECC]) which is the forerunner of the Commission on Information and Communication Technology (CICT). It was headed by former Secretary Virgilio "Ver" Peña and the Committee was chaired by Atty. Claro Parlade (+). It was an initiative of the Information Security and Privacy Sub-Committee chaired by Albert Dela Cruz who was the President of [http://www.cert.ph PHCERT] together with then Anti-Computer Crime and Fraud Division Chief, Atty. Elfren Meneses of the [http://www.nbi.gov.ph NBI]. The administrative and operational functions was provided by the Presidential Management Staff (PMS) acting as the CICT secretariat.<ref>[https://www.doj.gov.ph/files/cybercrime_office/Short%20History%20of%20the%20Dvlpt%20of%20Cybercrime.pdf] A Short History of the Development of Cybercrime Legislation in the Philippines - Geronimo L. Sy, Assistant Secretary - Department of Justice Manila</ref> 

This was superseded by several cybercrime-related bills filed in the [[14th Congress of the Philippines|14th]] and [[15th Congress of the Philippines|15th Congress]]. The Cybercrime Prevention Act ultimately was the product of House Bill No. 5808, authored by Representative [[Susan Yap-Sulit]] of the [[Legislative districts of Tarlac#Second District|second district of Tarlac]] and 36 other co-authors, and Senate Bill No. 2796, proposed by Senator [[Edgardo Angara]].  Both bills were passed by their respective chambers within one day of each other on June 5 and 4, 2012, respectively, shortly after the [[impeachment of Renato Corona]], and the final version of the Act was signed into law by President [[Benigno Aquino III]] on September 12.

==Provisions==
The Act, divided into 31 sections split across eight chapters, criminalizes several types of offense, including [[hacker (computer security)|illegal access]] (hacking), [[data destruction|data interference]], device misuse, [[cybersquatting]], computer-related offenses such as [[computer fraud]], content-related offenses such as [[cybersex]] and [[spam (electronic)|spam]], and other offenses.  The law also reaffirms existing laws against [[child pornography]], an offense under Republic Act No. 9775 (the Anti-Child Pornography Act of 2009), and [[libel]], an offense under Section 355 of the [[Revised Penal Code of the Philippines]], also criminalizing them when committed using a computer system.  Finally, the Act includes a "catch-all" clause, making all offenses currently punishable under the Revised Penal Code also punishable under the Act when committed using a computer, with severer penalties than provided by the Revised Penal Code alone.

The Act has [[universal jurisdiction]]: its provisions apply to all Filipino nationals regardless of the place of commission.  Jurisdiction also lies when a punishable act is either committed within the Philippines, whether the erring device is wholly or partly situated in the Philippines, or whether damage was done to any natural or juridical person who at the time of commission was within the Philippines.  [[Regional Trial Court]]s shall have jurisdiction over cases involving violations of the Act.

A takedown clause is included in the Act, empowering the [[Department of Justice (Philippines)|Department of Justice]] to restrict and/or demand the removal of content found to be contrary to the provisions of the Act, without the need for a court order.  This provision, originally not included in earlier iterations of the Act as it was being deliberated through Congress, was inserted during [[Senate of the Philippines|Senate]] deliberations on May 31, 2012.<ref name="takedown">{{cite web|last=Reyes|first=Karl John C.|title=Senate inserted Section 19: How the 'take-down' clause emerged in Cybercrime Law|url=http://www.interaksyon.com/article/44588/senate-inserted-section-19-how-the-take-down-clause-emerged-in-cybercrime-law||accessdate=3 October 2012|publisher=[[News5|TV5 News and Information]]|date=2 October 2012}}</ref>  Complementary to the takedown clause is a clause mandating the retention of data on computer servers for six months after the date of transaction, which may be extended for another six months should law enforcement authorities request it.

The Act also mandates the [[National Bureau of Investigation (Philippines)|National Bureau of Investigation]] and the [[Philippine National Police]] to organize a cybercrime unit Lyle Harvey Espinas, staffed by special investigators whose responsibility will be to exclusively handle cases pertaining to violations of the Act, under the supervision of the Department of Justice.  The unit is empowered to, among others, collect real-time traffic data from Internet service providers with due cause, require the disclosure of computer data within 72 hours after receipt of a court warrant from a service provider, and conduct searches and seizures of computer data and equipment.  It also mandates the establishment of special "cybercrime courts" which will handle cases involving cybercrime offenses (offenses enumerated in Section 4(a) of the Act).The [[Supreme Court of the Philippines]] declares on February 18, 2014  that the libel provisions of this act is now legal.

==Reactions==

The new Act received mixed reactions from several sectors upon its enactment, particularly with how its provisions could potentially affect freedom of expression, [[freedom of speech]] and [[data security]] in the Philippines.

The [[business process outsourcing in the Philippines|local business process outsourcing industry]] has received the new law well, citing an increase in the confidence of investors due to measures for the protection of electronic devices and online data.<ref>{{cite news|last=Agcaoili|first=Lawrence|title=IT-BPO industry welcomes passage of Cybercrime Prevention Act|url=http://www.philstar.com/cybercrime-law/2012/9/20/850809/it-bpo-industry-welcomes-passage-of-cybercrime-prevention-act|accessdate=24 September 2012|newspaper=[[The Philippine Star]]|date=20 September 2012}}</ref>  Media organizations and legal institutions though have criticized the Act for extending the definition of libel as defined in the [[Revised Penal Code of the Philippines]], which has been criticized by international organizations as being outdated:<ref>{{cite web|last=Panela|first=Shaira|title=Cybercrime Act extends reach of 'draconian', outdated libel laws|url=http://www.gmanetwork.com/news/story/274755/scitech/technology/cybercrime-act-extends-reach-of-draconian-outdated-libel-laws|publisher=[[GMA News and Public Affairs]]|accessdate=24 September 2012|date=19 September 2012}}</ref> the United Nations for one has remarked that the current definition of libel as defined in the Revised Penal Code is inconsistent with the [[International Covenant on Civil and Political Rights]], and therefore violates the respect of freedom of expression.<ref>{{cite news|last=Tiongson|first=Frank Lloyd|title=Libel law violates freedom of expression – UN rights panel|url=http://www.manilatimes.net/index.php/news/top-stories/16100-libel-law-violates-freedom-of-expression--un-rights-panel|accessdate=24 September 2012|newspaper=[[The Manila Times]]|date=30 January 2012}}</ref>

Senator [[Edgardo Angara]], the main proponent of the Act, defended the law by saying that it is a legal framework to protect freedoms such as the freedom of expression.  He asked the Act's critics to wait for the bill's implementing rules and regulations to see if the issues were addressed.<ref>{{cite news|last=Sy|first=Marvin|title='Give Cybercrime Prevention Act a chance'|url=http://www.philstar.com/headlines/2012/09/23/851973/give-cybercrime-prevention-act-chance|accessdate=5 April 2013|newspaper=[[The Philippine Star]]|date=23 September 2012}}</ref> He also added that the new law is unlike the controversial [[Stop Online Piracy Act]] and [[PROTECT IP Act]].<ref>{{cite web|last=Angara|first=Edgardo|title=Protecting our Cyberspace - The Cybercrime Prevention Act of 2012|url=http://www.edangara.com/protecting-our-cyberspace-the-cybercrime-prevention-act-of-2012-angara|publisher=EdAngara.com|accessdate=24 September 2012}}</ref>  However, Senator [[TG Guingona]] criticized the bill, calling it a prior restraint to the freedom of speech and freedom of expression.<ref>{{cite news|last=Mendes|first=Christina|title=Guingona criticizes Cybercrime Prevention Act|url=http://www.philstar.com/headlines/2012/09/22/851654/guingona-criticizes-cybercrime-prevention-act|accessdate=25 September 2012|newspaper=[[The Philippine Star]]|date=22 September 2012}}</ref>

The [[Electronic Frontier Foundation]] has also expressed concern about the Act,<ref name="effconcern">{{cite web|url=https://www.eff.org/deeplinks/2012/09/philippines-new-cybercrime-prevention-act-troubling-free-expression|title=Philippines' New Cybercrime Prevention Act Troubling for Free Expression|work=[[Electronic Frontier Foundation]]|accessdate=1 October 2012}}</ref> supporting local media and journalist groups which are opposed to it. The Centre for Law and Democracy also published a detailed analysis criticizing the law from a freedom of expression perspective.<ref name="CLDconcern">{{cite web|url=http://www.law-democracy.org/live/philippines-analysis-finds-major-problems-in-cybercrime-law/|title=Philippines: Analysis Finds Major Problems in Cybercrime Law|work=[[Centre for Law and Democracy]]|accessdate=1 January 2014}}</ref>

== Petitions to the Supreme Court ==
Several petitions have been submitted to the [[Supreme Court of the Philippines|Supreme Court]] questioning the constitutionality of the Act.<ref name="petitions">{{cite news|last=Canlas|first=Jonas|title=Suits pile up assailing anti-cybercrime law|url=http://www.manilatimes.net/index.php/news/nation/31925-suits-pile-up-assailing-anti-cybercrime-law|accessdate=27 September 2012|newspaper=[[The Manila Times]]|date=27 September 2012}}</ref> However, on October 2, the Supreme Court deferred on acting on the petitions, citing the absence of justices which prevented the Court from sitting ''en banc''.<ref name="deferral">{{cite news|last=Torres|first=Tetch|title=SC defers action on petitions vs cybercrime law|url=http://newsinfo.inquirer.net/281390/sc-defers-action-on-petitions-vs-cybercrime-law |accessdate=2 October 2012|newspaper=[[Philippine Daily Inquirer]] |date=2 October 2012}}</ref>  The lack of a temporary restraining order meant that the law went into effect as scheduled on October 3.  In protest, Filipino netizens reacted by blacking out their [[Facebook]] profile pictures and trending the hashtag #notocybercrimelaw on [[Twitter]].   [[Anonymous (group)|Anonymous]] also defaced government websites, including those of the [[Bangko Sentral ng Pilipinas]], the [[Metropolitan Waterworks and Sewerage System]] and the [[Intellectual Property Office (Philippines)|Intellectual Property Office]].

On October 8, 2012, the [[Supreme Court of the Philippines|Supreme Court]] issued a [[Injunction#Temporary restraining orders|temporary restraining order]], stopping implementation of the Act for 120 days.<ref name="TRO">{{cite news|last=Torres|first=Tetch|title=SC issues TRO vs cyber law|url=http://newsinfo.inquirer.net/285848/sc-stops-cyber-law|accessdate=9 October 2012|newspaper=[[Philippine Daily Inquirer]]|publisher=Philippine Daily Inquirer, Inc. |date=9 October 2012}}</ref>  In early December, 2012, the government requested the lifting of the TRO <ref name="TRO2">{{cite news|last=Phneah|first=Ellyne|title=Philippine govt asks court to lift injunction on Cybercrime Law|url=http://www.zdnet.com/ph/philippine-govt-asks-court-to-lift-injunction-on-cybercrime-law-7000008597/|accessdate=19 December 2012|newspaper=[[ZDNet]]|publisher=ZDNet |date=11 December 2012}}</ref>

{| class="wikitable" 
|-
! !! Petitioner!! Date of Filling
|-
| 1||Sen. [[Teofisto Guingona III]]||September 25, 2012
|-
| 2||Group of lawyers from the Ateneo School of Law||
|-
| 3||Journalists led by Alab ng Mamahayag (ALAM)||September 24, 2012
|-
| 4||Kabataan party-list Rep. [[Raymond Palatino]]||
|-
| 5||National Artist [[Bienvenido Lumbera]] et al.||
|-
| 6||Technology law experts and bloggers UP Law professor Jose Jesus M. Disini Jr, Rowena S. Disini, Lianne Ivy P. Medina, [[Janette Toral]], and Ernesto Sonido Jr.||
|-
| 7||Louis Biraogo||September 25, 2012
|-
| 8||National Union of Journalists of the Philippines and the Center for Media Freedom and Responsibility||
|-
| 9||Bloggers and Netizens for Democracy (BAND) led by Tonyo Cruz, "The Professional Heckler” and 18 more bloggers||October 4, 2012
|-
| 10||Philippine Bar Association||
|-
| 11|| Paul Cornelius Castillo and Ryan Andres|| October 3, 2012
|-
| 12||Bayan Muna. Rep. [[Neri Javier Colmenares]]||
|-
| 13||National Press Club||
|-
| 14||Philippine Internet Freedom Alliance||
|-
| 15||Harry Roque et al.||
|- 
|}

The Supreme Court scheduled the same amount of time on 15 January 2013 for oral arguments by the petitioners, and on 22 January by the Solicitor General.<ref>[http://sc.judiciary.gov.ph/pio/news/2013/01/cybercrime_advisory.pdf Advisory for the oral arguments]. Website of the [[Supreme Court of the Philippines]]</ref> On 5 February 2013 The Supreme Court extended the temporary restraining order on the law, "until further orders from the court."<ref name=gma1/><ref name=sunstar1/>

On August 2013, the Supreme Court issued a resolution ordering that the controlling title of the Cybercrime Case will be "Jose Jesus M. Disini Jr. et al. v. Secretary of Justice, et al."

==Revision of the law==
On May 24, 2013, The DOJ announced that online libel provisions of the law have been dropped, as well as other provisions that "are punishable under other laws already", like child pornography and cybersquatting. The DOJ will endorse the revised law to the next [[16th Congress of the Philippines]].<ref name="revised"/><ref>[http://www.businessmirror.com.ph/index.php/news/nation/13927-doj-deletes-libel-from-new-anti-cybercrime-bill  DOJ deletes libel from new anti-cybercrime bill] by Rene Acosta with InterAksyon.com, businessmirror.com.ph, 23 May 2013</ref>

== Supreme Court Ruling ==
On February 18, 2014, The Supreme Court ruled the online libel provision of the act to be constitutional, although it struck down other provisions, including the ones that violated the provisions on double jeopardy. The petitioners planned to appeal the decision.<ref name="gmanetwork.com"/>
<ref name="Disini, Jr. v. The Secretary of Justice, G.R. No. 203335 facts">[http://sc.judiciary.gov.ph/microsite/cybercrime/203335.php] [[Supreme Court of the Philippines]] Retrieved 4 February 2015.</ref><ref name="Disini, Jr. v. The Secretary of Justice, G.R. No. 203335 copy of decision">[https://www.scribd.com/doc/208335868/Supreme-Court-s-decision-on-the-cybercrime-law] [[Scribd]] Retrieved 4 February 2015.</ref>

==Repeal of the law==
A [[Magna Carta for Philippine Internet Freedom]] was [[crowdsourced]] by Filipino netizens with the intent of, among other things, repealing the Cybercrime Prevention Act of 2012.<ref>{{cite news |url=http://ph.news.yahoo.com/blogs/the-inbox/wisdom-crowds-crowdsourcing-net-freedom-042242158.html |title=The Wisdom of Crowds: Crowdsourcing Net Freedom |publisher=VERA Files |date=21 January 2013 |newspaper=Yahoo News Philippines |accessdate=25 January 2014 |first=Jonathan |last=De Santos}}</ref>

==Notes==
{{reflist|group="note"}}

==See also==
{{portal|Philippines}}
*[[Magna Carta for Philippine Internet Freedom]]
*[[Intellectual property protection in the Philippines]]
*[[Philippine copyright law]]
*[[Revised Penal Code of the Philippines]]
*[[Cyberbullying]]

==References==
{{reflist}}

==External links==
*{{cite web|title=AN ACT DEFINING CYBERCRIME, PROVIDING FOR THE PREVENTION, INVESTIGATION, SUPPRESSION AND THE IMPOSITION OF PENALTIES THEREFOR AND FOR OTHER PURPOSES|url=http://www.gov.ph/2012/09/12/republic-act-no-10175/|publisher=Office of the President of the Philippines Official Gazette|accessdate=24 September 2012}}
*[http://sc.judiciary.gov.ph/features/oral_arguments/cybercrime/index.php Oral Arguments by Petitioners, January 15, 2013] at the Supreme Court

[[Category:Philippine intellectual property law]]
[[Category:Computing legislation]]
[[Category:Cyberbullying]]
[[Category:Censorship in the Philippines]]
[[Category:2012 in the Philippines]]
[[Category:Internet in the Philippines]]
[[Category:Presidency of Benigno Aquino III]]