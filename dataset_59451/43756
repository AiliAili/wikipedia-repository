{{About||the fraternal organization|Tailhook Association|the scandal involving that organization|Tailhook scandal}}
[[File:F-15 Tail Hook.jpg|thumb|F-15 tailhook. Most USAF tactical jet aircraft have tailhooks for emergency use.]]
A '''tailhook''', '''arresting hook''', or '''arrester hook''' is a device attached to the [[empennage]] (rear) of some military [[fixed-wing aircraft]].  The hook is used to achieve rapid [[deceleration]] during [[Modern US Navy carrier air operations#Touchdown|routine landings]] aboard [[aircraft carrier]] [[flight deck]]s at sea, or during emergency landings or aborted takeoffs at [[Arresting gear#Land-based systems|properly equipped]] airports.

==History==
[[File:E-1B tailhook.jpg|thumb|left|Tailhook on an [[E-1B Tracer]]]]
On January 18, 1911, [[Eugene Burton Ely|Eugene Ely]] landed his [[Curtiss Aeroplane and Motor Company|Curtiss]] [[pusher aircraft|pusher airplane]] on a platform on the [[armored cruiser]] [[USS Pennsylvania (ACR-4)|USS ''Pennsylvania'']] anchored in [[San Francisco Bay]]. Ely flew from the Tanforan airfield in [[San Bruno, California]] and landed on the ''Pennsylvania'', which was the first successful shipboard landing of an aircraft.<ref>http://www.history.navy.mil/photos/events/ev-1910s/ev-1911/ely-pa.htm Accessed February 8, 2007.</ref><ref>http://www.history.navy.mil/photos/pers-us/uspers-e/eb-ely.htm Accessed February 8, 2007.</ref> This flight was also the first ever using a tailhook system, designed and built by circus performer and aviator [[Hugh Armstrong Robinson|Hugh Robinson]]. Ely told a reporter: "It was easy enough. I think the trick could be successfully turned nine times out of ten."

==Description==
[[File:Tail hook detail.jpg|thumb|Maintenanceman inspects an [[F/A-18]] tailhook prior to launch.]]
The tailhook is a strong metal bar, with its free end flattened out, thickened somewhat, and fashioned into a claw-like hook.  The hook is mounted on a swivel on the keel of the aircraft, and is normally mechanically and hydraulically held in the stowed/up position.  Upon actuation by the pilot, [[hydraulic]] or [[pneumatic]] pressure lowers the hook to the down position.  The presence of a tailhook is not evidence of an aircraft's aircraft carrier suitability.   Carrier aircraft hooks are designed to be quickly raised by the pilot after use.  Many land-based fighters also have tailhooks for use in case of a brake/tire malfunctions, aborted takeoffs, or other emergencies.  Land-based aircraft landing gear and tailhooks are typically not strong enough to absorb the impact of a carrier landing,<ref name="aerospaceweb.org">http://www.aerospaceweb.org/question/planes/q0295.shtml</ref> and some land-based tailhooks are held down with [[nitrogen]] pressure systems that must be recharged by ground personnel after actuation.<ref name="aerospaceweb.org"/>

==Arresting gear==
[[File:FA-18 Trap.jpg|thumb|An FA-18 makes an arrested landing aboard a US aircraft carrier.]]
{{Main article|Arresting gear}}
Both carrier- and land-based arresting gear consists of one or more cables (aka “arresting wires” or “cross deck pendants”) stretched across the landing area and attached on either end to arresting gear engines through “purchase cables”.

==Use==
[[File:Fa18 hook.jpg|thumb|F/A-18E Super Hornet with hook down.]]
Prior to making an "arrested landing", the pilot lowers the hook so that it will contact the ground as the aircraft wheels touch down.  The hook then drags along the surface until an arresting cable, stretched across the landing area, is engaged.  The cable lets out, transferring the energy of the aircraft to the arresting gear through the cable.  A "trap" is often-used slang for an arrested landing.  An aircraft which lands beyond the arresting cables is said to have "[[bolter (aviation)|boltered]]."  Occasionally, the tailhook bounces over one or more of the wires, resulting in a "hook skip bolter."<ref>http://www.wings-of-gold.com/cnatra/CNAF%203740.1%20(CQ)%20Sep03.pdf</ref>

In the case of an aborted land-based takeoff, the hook can be lowered at some point (typically about 1000 feet) prior to the cable.

Should a tailhook become inoperative or damaged, sea-based aircraft have limited options: they can divert to shore runways, or they can be "[[Arresting gear#Barricade|barricaded]]" on the carrier deck by a net that can be erected.

==Testing==
{{confusing|date=November 2014}}
With the advent of jet aircraft that operated from carriers and the higher landing speeds and increased loads on the aircraft's tailhook on landing aboard carriers, the US Navy in the 1950s knew they had to develop some sort of test rig.  The solution was a unit that had the tailhook fitted to it in the middle in a tunnel that was powered by two aircraft-type jet engines that powered the unit on a one-mile run.  At the end of the run is the arrest wire and the mono-track that guides it expands to slow it down after dropping it arrest gear in case of a failure. Different aircraft weights and speeds are tested by loading steel plates on the unit.<ref>[https://books.google.com/books?id=biYDAAAAMBAJ&pg=PA97&dq=popular+science+1930&hl=en&sa=X&ei=I5ICT8KZKsvlgge97s22Ag&ved=0CEsQ6AEwBjhu#v=onepage&q&f=true "Twin Jet Monorail Test Airplane Arresting Gear."] ''Popular Science'', June 1955, p. 97.</ref> Further testing in 1958 used 4 [[Allison J33]] jet engines.<ref name=jetdon>{{cite book|last=Dempewolff|first=Richard F.|title=Jet "Donkeys" for the Jets|url=https://books.google.dk/books?id=KN8DAAAAMBAJ&pg=PA72&lpg=PA72&dq=%22Jet+Donkey%22&source=bl&ots=xaMdE6YuEb&sig=KL9abFTNMtWfnH6BLlunW9LwYmk&hl=en&sa=X&ei=n66JUKq0EobXsgbvqoDwAw&redir_esc=y#v=onepage&q=%22Jet%20Donkey%22&f=false|publisher=[[Popular Mechanics]]|accessdate=25 October 2012|pages=72–75|date=June 1958}}</ref>

==See also==
{{Portal|United States Navy}}

* [[Arresting gear]]
* [[Carrier-based aircraft]]
* [[List of military aircraft of the United States (naval)]] / [[List of US Naval aircraft]]
* [[Military aviation]]
* [[Modern United States Navy carrier air operations]]
* [[NATOPS]]
* [[Naval aviation]]
* [[United States Marine Corps Aviation]]
* [[United States Naval Aviator]]

==References==
{{Reflist}}
* United States Air Force. [http://www.e-publishing.af.mil/shared/media/epubs/afh10-222v8.pdf]. ''Guide To Mobile Aircraft Arresting System Installation''. Retrieved on 3 November 2007.

==External links==
{{Commons category|Tail hooks (aircraft)}}
*[http://www.chinfo.navy.mil/navpalib/ships/carriers/ Aircraft carriers of the USA Navy]

{{Aircraft components}}

[[Category:Naval aviation technology]]
[[Category:Aircraft tail components]]