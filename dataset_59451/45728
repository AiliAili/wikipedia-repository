[[image:Waldo Waterman.jpg|thumb|200px|Waldo Waterman in 1920]]
[[Image:Waterman Aerobile in flight.jpg|thumb|Waterman Aerobile in flight.]]
'''Waldo Dean Waterman''' (June 16, 1894 – December 8, 1976) was an inventor and aviation pioneer from [[San Diego, California|San Diego]], [[California]]. His most notable contributions to aviation were the first tailless [[monoplane]] (the precursor to the [[flying wing]]{{cn|date=November 2015}}), the first aircraft with modern [[tricycle landing gear]] and the first successful low cost and simple to fly a [[Flying car (aircraft)|flying car]] which in the 1930s were commonly called '''Flivver Aircraft'''.<ref>[https://books.google.com/books?id=wygDAAAAMBAJ&pg=PA39&dq=Popular+Science+1931+plane&hl=en&ei=cIEUTfeLIcienAf0wdWFDg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCIQ6AEwADge#v=onepage&q=Popular%20Science%201931%20plane&f=true "Tailless Flivver Plane Has Pusher Propeller"] ''Popular Science'',May 1934, rare photos of aircraft</ref>

==Biography==
Waterman built his first aircraft, a biplane [[hang glider]], in 1909 while still in high school. He successfully flew the [[biplane]] hang glider on a slope near his home and by auto-tow. He then took on a partner to help build a powered aircraft that he entered in the first [[Dominguez Air Meet]] in January 1910. The aircraft was not completed in time for the meet. However, he began testing the aircraft on [[San Diego Bay|North Island]]. It was under-powered and required an auto-tow assist to get airborne. He flew the aircraft with some success but crashed, breaking both ankles.

In 1911, [[Glenn Curtiss]] moved his winter headquarters to North Island and Waterman attached himself to the Curtiss camp. In early 1912, the US Navy moved its three aircraft to Curtiss' testing station. By this time Waterman was a fixture at the station and was a frequent ride-along.

In 1912, Waterman entered the [[University of California]] as a student of mechanical engineering. When [[World War I]] broke out, and after being rejected from military service because of his broken ankles and flat feet, he became head of the Department of Theory of Flight, School of Military Aeronautics at the University of California. Later he became Chief Engineer at the U.S. Aircraft Corporation and remained to liquidate the company at the end of the war. With some assets purchased from the U.S. Aircraft Corporation, Waterman moved to [[Santa Monica, California|Santa Monica]], where he became test pilot for [[Bach Aircraft]] and established the Waterman Aircraft Manufacturing Company.<ref>{{cite journal|magazine=Air Progress Sport Aircraft|title=The Quiet Professor|author=John Underwood|date=Winter 1969}}</ref> However, he was forced out of business when the U.S. Army began dumping war-surplus aircraft on the civilian market for a tiny fraction of what Waterman could sell his custom aircraft for.

In 1929, Waterman built his first tailless [[monoplane]], the [[Waterman Whatsit|Whatsit]], which also used the then unusual [[tricycle landing gear]]. Some consider the Whatsit to be the first monoplane [[flying wing]],{{citation needed|date=August 2012}} but [[John William Dunne|J. W. Dunne]] had built [[Dunne D.6|something along the same lines]] as early as the summer of 1911 in England. The Whatsit was certainly a step in that direction, though it had a truncated fuselage and a forward trim plane. A development of the Whatsit was the [[high-wing]] [[Waterman Arrowplane]].<ref name=AB>Meaden, 1988</ref>

As well as the Whatsit, in 1930 Waterman produced another innovative design with a low-wing monoplane that could change the [[dihedral (aircraft)|dihedral]] of its wings during flight for shorter takeoffs, increased flight speed, and slower landing speeds according to the designer in its debut at the National Air races in Chicago. In addition to the wing design, he also placed the landing gears not under the fuselage but outwards under the wings.<ref name=AB/><ref>[https://books.google.com/books?id=qOIDAAAAMBAJ&pg=PA945&dq=Popular+Science+1930+plane+%22Popular+Mechanics%22&hl=en&ei=_7BlTsWeBYTWgQf9mIiLCg&sa=X&oi=book_result&ct=result&resnum=10&sqi=2&ved=0CE8Q6AEwCQ#v=onepage&q&f=true "Plane With Variable Wing Makes Flying Safer" ''Popular Mechanics'', December 1930]</ref>

[[Image:Waterman Aerobile 6.jpg|thumb|300px|The Waterman Aerobile at the Smithsonian.]]

At North Island, while experimenting with the Navy's flying boats, Glenn Curtiss is known to have talked about the possibility of a flying car. In 1917 He built a flying car he called the [[Curtiss Autoplane|Autoplane]]. The Autoplane never flew, but was exhibited at the Pan-American Aeronautic Exposition in New York City's Grand Central Palace. Waterman was certainly inspired by Curtiss and 20 years later made one of the first successful flying cars. The [[Waterman Arrowbile]] was based on the Arrowplane. It was a high-wing monoplane, with detachable wings and was powered by a Studebaker engine. Five Arrowbiles were built. Three Arrowbiles attempted a flight from Santa Monica to [[Cleveland]] but one had to turn back after only reaching [[Arizona]]. The other two finished the flight. Arrowbile No. 6 (No. 5 was never completed), rechristened the [[Waterman Arrowbile|Aerobile]] is on display at the [[Steven F. Udvar-Hazy Center]] (Smithsonian Air and Space museum extension in Dulles, Virginia).<ref name=AB/>

In the 1960s, Waterman built and flew his last aircraft. The Early Bird was based on the original [[Curtiss Model D|Curtiss Pusher]]. The Chevybird was a similar monoplane powered by a [[Chevrolet Corvair|Corvair]] engine. In the early 1970s, Waterman directed the construction of a replica of his biplane hang glider, built in his youth. This was done in conjunction with Michael Riggs of Seagull Aircraft, based in one of Waterman's Santa Monica buildings. Waterman gave Joe Faust of the Self-Soar Association $100 to support its ''Low & Slow'' periodical, in which notes about Waldo Waterman's final aircraft construction project appeared.

==See also==
[[John William Dunne]] (1875–1949), a pre-World War I pioneer of tailless aircraft, mostly in [[biplane]] form

==Notes==
{{Reflist}}

==References==

{{cite book |title=Waldo, Aviation Pioneer |last=Carpenter|first=Jack| year=1988|publisher=Bosch & Co |location=Arsdalen, Carlisle, Mass}}

{{cite journal |last=Meaden |first=Jack|year=1998 |title= The Waterman Aeroplanes|journal=Air Britain Archives|volume= |issue=3 |page=81 }}

==External links==
*[http://www.nasm.si.edu/collections/artifact.cfm?id=A19500099000 The Waterman Whatsit at the Smithsonian]
*[http://www.nasm.si.edu/collections/artifact.cfm?id=A19610156000 The Waterman Aerobile at the Smithsonian]
*[https://books.google.com/books?id=wN8DAAAAMBAJ&pg=PA488&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=aJgvTv3XKdSgsQLVyIRv&sa=X&oi=book_result&ct=result&resnum=9&sqi=2&ved=0CEkQ6AEwCA#v=onepage&q&f=true Plane Without a Tail Is Designed for Safety ''Popular Mechanics'', October 1935] article includes photos of what is thought to be #2 Aerobile?

{{Flying cars}}

{{Authority control}}

{{DEFAULTSORT:Waterman, Waldo}}
[[Category:1894 births]]
[[Category:1976 deaths]]
[[Category:Aircraft designers]]
[[Category:American aerospace engineers]]
[[Category:American aviation businesspeople]]
[[Category:American aviators]]
[[Category:Aviators from California]]
[[Category:Businesspeople from San Diego]]