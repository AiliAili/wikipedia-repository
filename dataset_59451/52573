{{Infobox journal
| title = Physiotherapy: Theory and Practice
| cover = [[File:ptp front cover.jpg]]
| discipline = [[Physiotherapy]]
| abbreviation = Physiother. Theory Pract.
| editor = Scott Hasson
| publisher = [[Informa]]
| country =
| history = 1985-present
| frequency = 8/year
| impact =
| impact-year =
| website = http://www.informahealthcare.com/ptp
| link1 = http://informahealthcare.com/toc/ptp/current
| link1-name = Online access
| link2 = http://informahealthcare.com/loi/ptp
| link2-name = Online archive
| ISSN = 0959-3985
| eISSN = 1532-5040
| LCCN = 98658551
| CODEN = PTHPEA
| OCLC = 474752352
}}
'''''Physiotherapy: Theory and Practice''''' is a [[Peer review|peer-reviewed]] [[medical journal]] covering research in physiotherapy ([[physical therapy]]). It is published 8 times a year by [[Informa]]. The journal was established  in 1985 and the [[editor-in-chief]] is Scott Hasson ([[Georgia Health Sciences University]]).<ref>{{Cite web |url=http://informahealthcare.com/page/EditorialAdvisoryBoard?journalCode=ptp |title=Editorial Board |work=Physiotherapy: Theory and Practice |accessdate=2012-12-21}}</ref>

== Article types ==
The journal publishes:
* Quantitative and qualitative research reports
* Theoretical papers
* Systematic [[literature review]]s
* Clinical case reports
* Technical clinical notes

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Academic Search Complete]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[CINAHL]]
* [[EMBASE]]
* [[EmCare]]
* [[MedLine]]/[[PubMed]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.informahealthcare.com/ptp}}

{{DEFAULTSORT:Physiotherapy: Theory And Practice}}

[[Category:English-language journals]]
[[Category:Physical therapy journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Publications established in 1985]]