{{Use dmy dates|date=January 2011}}
{{Infobox Christian leader
| type = archbishop
| honorific-prefix = The Most Reverend
| name = Robert Spence
| honorific-suffix =
| title = Archbishop of Adelaide
| image = Robert Spence (bishop) c 1920.jpg
| imagesize =
| alt = Robert Spence c. 1920
| caption = Robert Spence c. 1920
| church =
| archdiocese = [[Roman Catholic Archdiocese of Adelaide|Adelaide]]
| province =
| metropolis =
| diocese =
| see =
| enthroned = 6 July 1915
| ended = 5 November 1934
| predecessor = [[John O'Reily (bishop)|John O'Reily]]
| opposed =
| successor = [[Andrew Killian]]
| ordination = 23 December 1882
| consecration = 16 August 1914
| other_post =

<!---------- Personal details ---------->
| birth_name =
| birth_date = {{Birth date|df=yes|1860|1|13}}
| birth_place = [[Cork (city)|Cork, Ireland]]
| death_date = {{Death date and age|df=yes|1934|11|5|1860|1|13}}
| death_place = [[Adelaide, Australia]]
| nationality = [[Irish people|Irish]], [[Australian]]
| signature =
}}
'''Robert William Spence''' (13 January 1860 – 5 November 1934)<ref name="ADB - Spence, Robert William">{{cite web|last=Schumann|first=Ruth|title=Spence, Robert William|url=http://adbonline.anu.edu.au/biogs/A120039b.htm|work=Australian Dictionary of Biography|publisher=Melbourne University Press|accessdate=1 August 2010}}</ref> was an [[Australian]] [[Roman Catholic]] [[clergyman]], and the third [[Archbishop]] of [[Roman Catholic Archdiocese of Adelaide|Adelaide]].  Born in Ireland,  Spence became a [[Dominican Order|Dominican]] [[priest]], and after serving as a [[prior]] in [[Kilkenny]], moved to [[Adelaide, Australia]] in 1898.  In 1915, he became Archbishop of Adelaide, a position he held until his death in 1934.

==Early life==
Robert Spence was born on 13 January 1860 in [[Cork (city)|Cork, Ireland]].  The son of Robert Spence and his wife Ellen, née Sullivan, he received his education from the [[Congregation of Christian Brothers|Christian Brothers]] and [[Vincentian Fathers]] before entering the [[Dominican Order|Dominican]] [[novitiate]] in [[Tallaght]], outside [[Dublin]].  Having [[Profession (religious)|professed]] in 1878, Spence moved to [[Lisbon]], where he studied for the priesthood at [[Corpo Santo College]].  He was ordained a priest on 23 December 1882, and two days later, at [[Bom Sucesso (Alverca)|Bom Sucesso]] convent he celebrated the first Dominican [[Solemn Mass|high mass]] in Portugal since religious orders were suppressed there in 1833.  Returning to Ireland in 1885, he served in Cork and [[Newry]], and ran retreats throughout Ireland, earning a reputation as a zealous and forceful preacher.  In 1892, he became [[prior]] of the [[Black Abbey]] priory in [[Kilkenny]], a position he held for six years.<ref name="ADB - Spence, Robert William" />

In 1898, Spence travelled to [[Adelaide, Australia]] as prior of the first Dominican house of friars in Australia until 1901.  Spence constructed a priory at St Laurence's Church in [[North Adelaide]], and ran retreats ministering to [[religious (Catholicism)|religious]].  In 1899, he founded the Adelaide Catholic Club (intended as a Catholic version of the [[Adelaide Club]]),<ref name="Colour and Shadow">{{cite book|last=Press|first=Margaret M.|title=Colour and Shadow|year=1991|publisher=The Archdiocese of Adelaide|isbn=0-646-04777-9|pages=101–103}}</ref> and throughout his time in Adelaide he continued to participate in Catholic associations, serving as president of the state branch of the Australian Catholic Federation and reviving the North Adelaide branch of the [[Hibernian Australasian Catholic Benefit Society]].  While serving as a prior in North Adelaide from 1908, Spence became more involved in the administration of the Archdiocese, advising then Archbishop [[John O'Reily (bishop)|John O'Reily]], and often accompanying him when the Archbishop travelled.<ref name="ADB - Spence, Robert William" /><ref>Press 1991, p 155.</ref>

== Episcopacy ==
By the 1910s, Archbishop O'Reily was growing frail,<ref name="ADB - O'Reily, John">{{cite web|last=French|first=M.|title=O'Reily, John| url=http://adbonline.anu.edu.au/biogs/A110107b.htm|work=Australian Dictionary of Biography|accessdate=8 August 2010}}</ref> with many of his pastoral duties having to be filled by Bishop of [[Roman Catholic Diocese of Port Augusta|Port Augusta]] John Norton,.<ref>Press 1991, p 74.</ref>  In 1913, O'Reily requested a [[coadjutor bishop|coadjutor]], indicating his preference for Spence to be appointed.  Despite some Australian bishops raising objections to a [[religious (Catholicism)|religious]] becoming a bishop, Spence was appointed coadjutor archbishop on 13 July 1914.  He was [[consecrate#Roman Catholic Church|consecrated]] on 16 August of the same year, and when O'Reily died on 6 July 1915,<ref name="ADB - O'Reily, John"/> Spence became Archbishop.<ref name="ADB - Spence, Robert William" />

While Archbishop, Spence continued to wear the plain clothes of his Dominican order rather than the purple soutane of an archbishop.<ref name="ADB - Spence, Robert William" />  He carried on O'Reily's efforts to restructure the diocesan finances, removing much of the diocese debt.  After returning from an ''[[ad limina]]'' visit to [[Rome]] in 1921, he travelled through the archdiocese to raise funds for the completion and transformation of [[St Francis Xavier's Cathedral, Adelaide|St Francis Xavier's Cathedral]], with the new building opened in 1926.<ref name="ADB - Spence, Robert William" />

While in Ireland in 1920, Spence gave a controversial speech in [[Newry]] where he saluted the [[Flag of Ireland|Irish flag]] and alleged that "soldiers of the British Government were committing atrocities in Ireland."<ref name="ADB - Spence, Robert William" />  The incident was controversial in Australia, with Adelaide newspapers accusing him of disloyalty.<ref>Press 1991, p 89.</ref>  In July 1933, in the same month as he was made a [[count of the Holy Roman Empire]], an [[assistant at the pontifical throne]] and a companion to Pius XI, [[Andrew Killian]] was appointed to serve as Spence's coadjutor.  Spence died in Adelaide on 5 November 1934, with the [[Adelaide City Council]] adjourning as a sign of respect for the late Archbishop.<ref name="ADB - Spence, Robert William" />

== References ==
{{reflist}}

== External links ==
{{commons category|Robert Spence (bishop)}}
*[http://adbonline.anu.edu.au/biogs/A120039b.htm ''Spence, Robert William''] at the Australian Dictionary of Biography, Online Edition.

{{Bishops and Archbishops of the Roman Catholic Archdiocese of Adelaide}}


{{DEFAULTSORT:Spence, Robert William}}
[[Category:1860 births]]
[[Category:1934 deaths]]
[[Category:21st-century Roman Catholic archbishops]]
[[Category:Roman Catholic Bishops of Adelaide]]
[[Category:People from Cork (city)]]
[[Category:Disease-related deaths in South Australia]]