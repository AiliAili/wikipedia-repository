{{Featured article}}
[[Image:GMAW.welding.af.ncs.jpg|thumb|Gas metal arc welding]]
'''Gas metal arc welding''' ('''GMAW'''), sometimes referred to by its subtypes '''metal inert gas''' ('''MIG''') '''welding''' or '''metal active gas''' ('''MAG''') '''welding''', is a [[welding]] process in which an electric arc forms between a consumable [[wire]] [[electrode]] and the workpiece metal(s), which heats the workpiece metal(s), causing them to melt and join.

Along with the wire electrode, a [[shielding gas]] feeds through the welding gun, which shields the process from contaminants in the air. The process can be semi-automatic or automatic. A constant [[volt]]age, [[direct current]] power source is most commonly used with GMAW, but constant [[Current (electricity)|current]] systems, as well as [[alternating current]], can be used. There are four primary methods of metal transfer in GMAW, called globular, short-circuiting, spray, and pulsed-spray, each of which has distinct properties and corresponding advantages and limitations.

Originally developed for welding [[aluminium]] and other non-ferrous materials in the 1940s, GMAW was soon applied to [[steel]]s because it provided faster welding time compared to other welding processes. The cost of inert gas limited its use in steels until several years later, when the use of semi-inert gases such as [[carbon dioxide]] became common. Further developments during the 1950s and 1960s gave the process more versatility and as a result, it became a highly used industrial process. Today, GMAW is the most common industrial welding process, preferred for its versatility, speed and the relative ease of adapting the process to robotic automation. Unlike welding processes that do not employ a shielding gas, such as [[shielded metal arc welding]], it is rarely used outdoors or in other areas of air volatility. A related process, [[Flux-cored arc welding|flux cored arc welding]], often does not use a shielding gas, but instead employs an electrode wire that is hollow and filled with [[flux (metallurgy)|flux]].


==Development==
The principles of gas metal arc welding began to be understood in the early 19th century, after [[Humphry Davy]] discovered the short pulsed electric arcs in 1800.<ref name="anders">{{Harvnb|Anders|2003|pages=1060–9}}</ref> [[Vasily Vladimirovich Petrov|Vasily Petrov]] independently produced the continuous [[electric arc]] in 1802 (followed by Davy after 1808).<ref name="anders"/> It was not until the 1880s that the technology became developed with the aim of industrial usage. At first, carbon electrodes were used in [[carbon arc welding]]. By 1890, metal electrodes had been invented by [[Nikolay Slavyanov]] and [[C. L. Coffin]]. In 1920, an early predecessor of GMAW was invented by P. O. Nobel of [[General Electric]]. It used a bare electrode wire and direct current, and used arc voltage to regulate the feed rate. It did not use a shielding gas to protect the weld, as developments in welding atmospheres did not take place until later that decade. In 1926 another forerunner of GMAW was released, but it was not suitable for practical use.<ref>{{Harvnb|Cary|Helzer|2005|p=7}}</ref>

In 1948, GMAW was developed by the [[Battelle Memorial Institute]]. It used a smaller diameter electrode and a constant voltage power source developed by [[H. E. Kennedy]]. It offered a high deposition rate, but the high cost of inert gases limited its use to non-ferrous materials and prevented cost savings. In 1953, the use of [[carbon dioxide]] as a welding atmosphere was developed, and it quickly gained popularity in GMAW, since it made welding steel more economical. In 1958 and 1959, the short-arc variation of GMAW was released, which increased welding versatility and made the welding of thin materials possible while relying on smaller electrode wires and more advanced power supplies. It quickly became the most popular GMAW variation.

The spray-arc transfer variation was developed in the early 1960s, when experimenters added small amounts of oxygen to inert gases. More recently, pulsed current has been applied, giving rise to a new method called the pulsed spray-arc variation.<ref>{{Harvnb|Cary|Helzer|2005|pp=8–9}}</ref>

GMAW is one of the most popular welding methods, especially in industrial environments.<ref>{{Harvnb|Jeffus|1997|p=6}}</ref> It is used extensively by the sheet metal industry and, by extension, the automobile industry. There, the method is often used for arc [[spot welding]], thereby replacing [[rivet]]ing or [[resistance welding|resistance]] spot welding. It is also popular for [[automated welding]], in which robots handle the workpieces and the welding gun to speed up the manufacturing process.<ref>{{Harvnb|Kalpakjian|Schmid|2001|p=783}}</ref> GMAW can be difficult to perform well outdoors, since drafts can dissipate the shielding gas and allow contaminants into the weld;<ref>{{Harvnb|Davies|2003|p=174}}</ref> [[flux-cored arc welding|flux cored arc welding]] is better suited for outdoor use such as in construction.<ref>{{Harvnb|Jeffus|1997|p=264}}</ref><ref>{{Harvnb|Davies|2003|p=118}}</ref> Likewise, GMAW's use of a shielding gas does not lend itself to [[underwater welding]], which is more commonly performed via [[shielded metal arc welding]], flux cored arc welding, or [[gas tungsten arc welding]].<ref>{{Harvnb|Davies|2003|p=253}}</ref>

==Equipment==
To perform gas metal arc welding, the basic necessary equipment is a welding gun, a wire feed unit, a [[welding power supply]], a [[welding electrode]] wire, and a [[shielding gas]] supply.<ref>{{Harvnb|Miller Electric Mfg Co|2012|p=5}}</ref>

===Welding gun and wire feed unit===
[[Image:MIG cut-away.svg|right|thumb|300px|GMAW torch nozzle cutaway image. '''(1)''' Torch handle, '''(2)''' Molded phenolic dielectric (shown in white) and threaded metal nut insert (yellow), '''(3)''' Shielding gas diffuser, '''(4)''' Contact tip, '''(5)''' Nozzle output face]]
[[Image:GMAW application.jpg|thumb|300px|GMAW on stainless steel]]

The typical GMAW welding gun has a number of key parts&mdash;a control switch, a contact tip, a power cable, a gas nozzle, an electrode conduit and liner, and a gas hose. The control switch, or trigger, when pressed by the operator, initiates the wire feed, electric power, and the shielding gas flow, causing an electric arc to be struck. The contact tip, normally made of [[copper]] and sometimes chemically treated to reduce spatter, is connected to the welding power source through the power cable and transmits the electrical energy to the electrode while directing it to the weld area.  It must be firmly secured and properly sized, since it must allow the electrode to pass while maintaining electrical contact. On the way to the contact tip, the wire is protected and guided by the electrode conduit and liner, which help prevent buckling and maintain an uninterrupted wire feed. The gas nozzle directs the shielding gas evenly into the welding zone. Inconsistent flow may not adequately protect the weld area. Larger nozzles provide greater shielding gas flow, which is useful for high current welding operations that develop a larger molten weld pool.  A gas hose from the tanks of shielding gas supplies the gas to the nozzle. Sometimes, a water hose is also built into the welding gun, cooling the gun in high heat operations.<ref>{{Harvnb|Nadzam|1997|pp=5–6}}</ref>

The wire feed unit supplies the electrode to the work, driving it through the conduit and on to the contact tip.  Most models provide the wire at a constant feed rate, but more advanced machines can vary the feed rate in response to the arc length and voltage.  Some wire feeders can reach feed rates as high as 30.5&nbsp;m/min (1200&nbsp;in/min),<ref>{{Harvnb|Nadzam|1997|p=6}}</ref> but feed rates for semiautomatic GMAW typically range from 2 to 10&nbsp;m/min (75 – 400&nbsp;in/min).<ref name=cary-helzer2005p123-5>{{Harvnb|Cary|Helzer|2005|pp=123–5}}</ref>

===Tool style===
The most common electrode holder is a semiautomatic air-cooled holder. Compressed air circulates through it to maintain moderate temperatures. It is used with lower current levels for welding lap or butt [[Welding joint|joints]]. The second most common type of electrode holder is semiautomatic water-cooled, where the only difference is that water takes the place of air. It uses higher current levels for welding T or corner joints. The third typical holder type is a water cooled automatic electrode holder—which is typically used with automated equipment.<ref>{{Harvnb|Todd|Allen|Alting|1994|pp=351–355}}.</ref>

===Power supply===
Most applications of gas metal arc welding use a constant voltage power supply.  As a result, any change in arc length (which is directly related to voltage) results in a large change in heat input and current. A shorter arc length causes a much greater heat input, which makes the wire electrode melt more quickly and thereby restore the original arc length. This helps operators keep the arc length consistent even when manually welding with hand-held welding guns. To achieve a similar effect, sometimes a constant current power source is used in combination with an arc voltage-controlled wire feed unit.  In this case, a change in arc length makes the wire feed rate adjust to maintain a relatively constant arc length. In rare circumstances, a constant current power source and a constant wire feed rate unit might be coupled, especially for the welding of metals with high thermal conductivities, such as aluminum.  This grants the operator additional control over the heat input into the weld, but requires significant skill to perform successfully.<ref>{{Harvnb|Nadzam|1997|p=1}}</ref>

Alternating current is rarely used with GMAW; instead, direct current is employed and the electrode is generally positively charged.  Since the [[anode]] tends to have a greater heat concentration, this results in faster melting of the feed wire, which increases weld penetration and welding speed.  The polarity can be reversed only when special emissive-coated electrode wires are used, but since these are not popular, a negatively charged electrode is rarely employed.<ref>{{Harvnb|Cary|Helzer|2005|pp=118–9}}</ref>

===Electrode===
Electrode selection is based primarily on the composition of the metal being welded, the process variation being used, joint design and the material surface conditions. Electrode selection greatly influences the mechanical properties of the weld and is a key factor of weld quality. In general the finished weld metal should have mechanical properties similar to those of the base material with no defects such as discontinuities, entrained contaminants or porosity within the weld. To achieve these goals a wide variety of electrodes exist.  All commercially available electrodes contain deoxidizing metals such as [[silicon]], [[manganese]], [[titanium]] and [[aluminum]] in small percentages to help prevent oxygen porosity. Some contain denitriding metals such as titanium and [[zirconium]] to avoid nitrogen porosity.<ref>{{Harvnb|Nadzam| 1997|p=15}}</ref> Depending on the process variation and base material being welded the diameters of the electrodes used in GMAW typically range from 0.7 to 2.4&nbsp;mm (0.028 – 0.095&nbsp;in) but can be as large as 4&nbsp;mm (0.16&nbsp;in). The smallest electrodes, generally up to 1.14&nbsp;mm (0.045&nbsp;in)<ref>{{Harvnb|Craig|1991|p=22}}</ref> are associated with the short-circuiting metal transfer process, while the most common spray-transfer process mode electrodes are usually at least 0.9&nbsp;mm (0.035&nbsp;in).<ref>{{Harvnb|Craig|1991|p=105}}</ref><ref name=cary-helzer2005p121>{{Harvnb|Cary|Helzer|2005|p=121}}</ref>

===Shielding gas===
{{Main article|Shielding gas}}
[[Image:GMAW Circuit.svg|thumb|left|250px|GMAW Circuit diagram. '''(1)''' Welding torch, '''(2)''' Workpiece, '''(3)''' Power source, '''(4)''' Wire feed unit, '''(5)''' Electrode source, '''(6)''' Shielding gas supply.]]
Shielding gases are necessary for gas metal arc welding to protect the welding area from atmospheric gases such as [[nitrogen]] and [[oxygen]], which can cause fusion defects, porosity, and weld metal embrittlement if they come in contact with the electrode, the arc, or the welding metal. This problem is common to all arc welding processes; for example, in the older Shielded-Metal Arc Welding process (SMAW), the electrode is coated with a solid flux which evolves a protective cloud of carbon dioxide when melted by the arc. In GMAW, however, the electrode wire does not have a flux coating, and a separate shielding gas is employed to protect the weld. This eliminates slag, the hard residue from the flux that builds up after welding and must be chipped off to reveal the completed weld.{{sfn|Cary|Helzer|2005|pp=357–9}}

The choice of a shielding gas depends on several factors, most importantly the type of material being welded and the process variation being used. Pure inert gases such as [[argon]] and [[helium]] are only used for nonferrous welding; with steel they do not provide adequate weld penetration (argon) or cause an erratic arc and encourage spatter (with helium). Pure [[carbon dioxide]], on the other hand, allows for deep penetration welds but encourages oxide formation, which adversely affect the mechanical properties of the weld. lts low cost makes it an attractive choice, but because of the reactivity of the arc plasma, spatter is unavoidable and welding thin materials is difficult. As a result, argon and carbon dioxide are frequently mixed in a 75%/25% to 90%/10% mixture. Generally, in short circuit GMAW, higher carbon dioxide content increases the weld heat and energy when all other weld parameters (volts, current, electrode type and diameter) are held the same. As the carbon dioxide content increases over 20%, spray transfer GMAW becomes increasingly problematic, especially with smaller electrode diameters.<ref>{{Harvnb|Craig|1991|p=96}}</ref>

Argon is also commonly mixed with other gases, oxygen, helium, [[hydrogen]] and nitrogen. The addition of up to 5% oxygen (like the higher concentrations of carbon dioxide mentioned above) can be helpful in welding stainless steel, however, in most applications carbon dioxide is preferred.<ref>{{Harvnb|Craig|1991|pp=40–1}}</ref> Increased oxygen makes the shielding gas oxidize the electrode, which can lead to porosity in the deposit if the electrode does not contain sufficient deoxidizers. Excessive oxygen, especially when used in application for which it is not prescribed, can lead to brittleness in the heat affected zone. Argon-helium mixtures are extremely inert, and can be used on nonferrous materials. A helium concentration of 50–75% raises the required voltage and increases the heat in the arc, due to helium's higher ionization temperature. Hydrogen is sometimes added to argon in small concentrations (up to about 5%) for welding nickel and thick stainless steel workpieces. In higher concentrations (up to 25% hydrogen), it may be used for welding conductive materials such as copper. However, it should not be used on steel, aluminum or magnesium because it can cause porosity and [[hydrogen embrittlement]].{{sfn|Cary|Helzer|2005|pp=357–9}}

Shielding gas mixtures of three or more gases are also available. Mixtures of argon, carbon dioxide and oxygen are marketed for welding steels. Other mixtures add a small amount of helium to argon-oxygen combinations, these mixtures are claimed to allow higher arc voltages and welding speed. Helium also sometimes serves as the base gas, with small amounts of argon and carbon dioxide added. However, because it is less dense than air, helium is less effective at shielding the weld than argon&mdash;which is denser than air. It also can lead to arc stability and penetration issues, and increased spatter, due to its much more energetic arc plasma. Helium is also substantially more expensive than other shielding gases. Other specialized and often proprietary gas mixtures claim even greater benefits for specific applications.{{sfn|Cary|Helzer|2005|pp=357–9}}

The desirable rate of shielding-gas flow depends primarily on weld geometry, speed, current, the type of gas, and the metal transfer mode. Welding flat surfaces requires higher flow than welding grooved materials, since gas disperses more quickly. Faster welding speeds, in general, mean that more gas must be supplied to provide adequate coverage. Additionally, higher current requires greater flow, and generally, more helium is required to provide adequate coverage than if argon is used. Perhaps most importantly, the four primary variations of GMAW have differing shielding gas flow requirements&mdash;for the small weld pools of the short circuiting and pulsed spray modes, about 10&nbsp;[[litre|L]]/min (20&nbsp;ft³/[[hour|h]]) is generally suitable, whereas for globular transfer, around 15&nbsp;L/min (30&nbsp;ft³/h) is preferred.  The spray transfer variation normally requires more shielding-gas flow because of its higher heat input and thus larger weld pool. Typical gas-flow amounts are approximately 20&ndash;25&nbsp;L/min (40&ndash;50&nbsp;ft³/h).<ref name=cary-helzer2005p123-5/>

==== Flux-cored wire-fed ====
{{Main article|Flux-cored arc welding}}
'''Flux-cored''', '''self-shielding''' or '''gasless''' wire-fed welding had been developed for simplicity and portability.<ref name="Holster, Gasless" >{{Cite web
  |title=Gasless wire welding is a breeze
  |author=Greg Holster
  |pages=64–68
  |website=
  |url=http://www.cigweld.com.au/wp-content/uploads/2012/11/NZ-The-Shed-May-2011-p63_68-Gasless-proof-2.pdf
}}</ref> This avoids the gas system of conventional GMAW and uses a cored wire containing a solid flux. This flux vaporises during welding and produces a plume of shielding gas. Although described as a 'flux', this compound has little activity and acts mostly as an inert shield. The wire is of slightly larger diameter than for a comparable gas-shielded weld, to allow room for the flux. The smallest available is 0.8&nbsp;mm diameter, compared to 0.6&nbsp;mm for solid wire. The shield vapor is slightly active, rather than inert, so the process is always MAGS but not MIG (inert gas shield). This limits the process to steel and not aluminium.

Vaporising the additional flux requires greater heat in the wire, so these gasless machines operate as DCEP, rather than the DCEN usually used for GMAW to give deeper penetration.<ref name="Holster, Gasless" /> DCEP, or DC Electrode Positive, makes the welding wire into the positively-charged [[anode]], which is the hotter side of the arc.<ref>{{Cite web
  |title=Welding Metallurgy: Arc Physics and Weld Pool Behaviour
  |website=Canteach
  |url=https://canteach.candu.org/Content%20Library/20053426.pdf
}}</ref> Provided that it is switchable from DCEN to DCEP, a gas-shielded wire-feed machine may also be used for flux-cored wire.

Flux-cored wire is considered to have some advantages for outdoor welding on-site, as the shielding gas plume is less likely to be blown away in a wind than shield gas from a conventional nozzle.<ref name="MIG Welding, Flux-cored" >{{Cite web
  |title=How to weld with flux cored wire
  |website=MIG Welding  - The DIY Guide
  |url=http://www.mig-welding.co.uk/gasless-mig.htm
}}</ref><ref name="Warehouse, Gasless" >{{Cite web
  |title=Gas Vs Gasless Mig Welding, what’s the difference
  |website=Welder's Warehouse
  |date=4 October 2014
  |url=http://www.thewelderswarehouse.com/blog/gas-vs-gasless-mig-welding/
}}</ref> A slight drawback is that, like SMAW (stick) welding, there may be some flux deposited over the weld bead, requiring more of a cleaning process between passes.<ref name="MIG Welding, Flux-cored" />

Flux-cored welding machines are most popular at the hobbyist level, as the machines are slightly simpler but mainly because they avoid the cost of providing shield gas, either through a rented cylinder or with the high cost of disposable cylinders.<ref name="MIG Welding, Flux-cored" />

===GMAW-based 3-D printing ===
GMAW has also been used as a low-cost method to [[3-D print]] metal objects. <ref> Loose screw? 3-D printer may soon forge you a new one  http://www.nbcnews.com/technology/loose-screw-3-d-printer-may-soon-forge-you-new-2D11678840 </ref><ref>You Can Now 3D Print with Metal at Home http://motherboard.vice.com/blog/you-can-now-3d-print-with-metal-at-home  </ref><ref>Gerald C. Anzalone, Chenlong Zhang, Bas Wijnen, Paul G. Sanders and Joshua M. Pearce, “[https://www.academia.edu/5327317/A_Low-Cost_Open-Source_Metal_3-D_Printer Low-Cost Open-Source 3-D Metal Printing]” ''IEEE Access'', 1, pp.803-810, (2013). doi: 10.1109/ACCESS.2013.2293018 </ref> Various [[open source]] 3-D printers have been developed to use GMAW. <Ref> Yuenyong Nilsiam, Amberlee Haselhuhn, Bas Wijnen, Paul Sanders, & Joshua M. Pearce. [https://www.academia.edu/17673716/Integrated_Voltage_Current_Monitoring_and_Control_of_Gas_Metal_Arc_Weld_Magnetic_Ball-Jointed_Open_Source_3-D_Printer  Integrated Voltage - Current Monitoring and Control of Gas Metal Arc Weld Magnetic Ball-Jointed Open Source 3-D Printer].''Machines'' '''3'''(4), 339-351 (2015). doi:10.3390/machines3040339 </ref> Such components fabricated from aluminum compete with more traditionally manufactured components on mechanical strength. <ref>Amberlee S. Haselhuhn, Michael W. Buhr, Bas Wijnen, Paul G. Sanders, Joshua M. Pearce, [https://www.academia.edu/27308268/Structure-Property_Relationships_of_Common_Aluminum_Weld_Alloys_Utilized_as_Feedstock_for_GMAW-based_3-D_Metal_Printing  Structure-Property Relationships of Common Aluminum Weld Alloys Utilized as Feedstock for GMAW-based 3-D Metal Printing]. ''Materials Science and Engineering: A'',  '''673''', pp.  511–523 (2016). DOI: 10.1016/j.msea.2016.07.099 </ref> By forming a bad weld on the first layer, GMAW 3-D printed parts can be removed from the substrate with a hammer.<ref>Amberlee S. Haselhuhn, Bas Wijnen, Gerald C. Anzalone, Paul G. Sanders, Joshua M. Pearce, [https://www.academia.edu/14451546/In_Situ_Formation_of_Substrate_Release_Mechanisms_for_Gas_Metal_Arc_Weld_Metal_3-D_Printing In Situ Formation of Substrate Release Mechanisms for Gas Metal Arc Weld Metal 3-D Printing]. ''Journal of Materials Processing Technology''. 226, pp. 50–59 (2015). </ref><ref>Amberlee S. Haselhuhn, Eli J. Gooding, Alexandra G. Glover, Gerald C. Anzalone, Bas Wijnen, Paul G. Sanders, Joshua M. Pearce. [https://www.academia.edu/17589088/Substrate_Release_Mechanisms_for_Gas_Metal_Arc_3-D_Aluminum_Metal_Printing  Substrate Release Mechanisms for Gas Metal Arc 3-D Aluminum Metal Printing]. ''3D Printing and Additive Manufacturing''. 1(4): 204-209 (2014).  DOI: 10.1089/3dp.2014.0015 </ref>

==Operation==
[[Image:GMAW weld area.svg|thumb|right|300px|GMAW weld area. '''(1)''' Direction of travel, '''(2)''' Contact tube, '''(3)''' Electrode, '''(4)''' Shielding gas, '''(5)''' Molten weld metal, '''(6)''' Solidified weld metal, '''(7)''' Workpiece.]]
For most of its applications gas metal arc welding is a fairly simple welding process to learn requiring no more than a week or two to master basic welding technique. Even when welding is performed by well-trained operators weld quality can fluctuate since it depends on a number of external factors. All GMAW is dangerous, though perhaps less so than some other welding methods, such as [[shielded metal arc welding]].<ref>{{Harvnb|Cary|Helzer|2005|p=126}}</ref>

===Technique===
The basic technique for GMAW is quite simple, since the electrode is fed automatically through the torch (head of tip). By contrast, in [[gas tungsten arc welding]], the welder must handle a welding torch in one hand and a separate filler wire in the other, and in shielded metal arc welding, the operator must frequently chip off slag and change welding electrodes. GMAW requires only that the operator guide the welding gun with proper position and orientation along the area being welded.  Keeping a consistent contact tip-to-work distance (the ''stick out'' distance) is important, because a long stickout distance can cause the electrode to overheat and also wastes shielding gas. Stickout distance varies for different GMAW weld processes and applications.<ref>{{Harvnb|Craig|1991|p=29}}</ref><ref>{{Harvnb|Craig|1991|p=52}}</ref><ref>{{Harvnb|Craig|1991|p=109}}</ref><ref>{{Harvnb|Craig|1991|p=141}}</ref> The orientation of the gun is also important&mdash;it should be held so as to bisect the angle between the workpieces; that is, at 45 degrees for a fillet weld and 90 degrees for welding a flat surface. The travel angle, or lead angle, is the angle of the torch with respect to the direction of travel, and it should generally remain approximately vertical. However, the desirable angle changes somewhat depending on the type of shielding gas used&mdash;with pure inert gases, the bottom of the torch is often slightly in front of the upper section, while the opposite is true when the welding atmosphere is carbon dioxide.<ref>{{Harvnb|Cary|Helzer|2005|p=125}}</ref>

===Quality===
Two of the most prevalent quality problems in GMAW are [[dross]] and [[porosity]]. If not controlled, they can lead to weaker, less [[ductile]] welds. Dross is an especially common problem in aluminium GMAW welds, normally coming from particles of aluminium oxide or aluminum nitride present in the electrode or base materials. Electrodes and workpieces must be brushed with a wire brush or chemically treated to remove oxides on the surface. Any oxygen in contact with the weld pool, whether from the atmosphere or the shielding gas, causes dross as well. As a result, sufficient flow of inert shielding gases is necessary, and welding in volatile air should be avoided.<ref>{{Harvnb|Lincoln Electric|1994|loc=9.3-5 – 9.3-6}}</ref>

In GMAW the primary cause of porosity is gas entrapment in the weld pool, which occurs when the metal solidifies before the gas escapes. The gas can come from impurities in the shielding gas or on the workpiece, as well as from an excessively long or violent arc. Generally, the amount of gas entrapped is directly related to the cooling rate of the weld pool. Because of its higher [[thermal conductivity]], aluminum welds are especially susceptible to greater cooling rates and thus additional porosity. To reduce it, the workpiece and electrode should be clean, the welding speed diminished and the current set high enough to provide sufficient heat input and stable metal transfer but low enough that the arc remains steady. Preheating can also help reduce the cooling rate in some cases by reducing the temperature gradient between the weld area and the base material.<ref>{{Harvnb|Lincoln Electric|1994|loc=9.3-1 – 9.3-2}}</ref>

===Safety===
Gas metal arc welding can be dangerous if proper precautions are not taken. Since GMAW employs an electric arc, welders wear protective clothing, including heavy leather gloves and protective long sleeve jackets, to avoid exposure to extreme heat and flames. In addition, the brightness of the electric arc is a source of the condition known as [[arc eye]], an inflammation of the [[cornea]] caused by [[Ultraviolet|ultraviolet light]] and, in prolonged exposure, possible burning of the [[retina]] in the eye. Conventional welding [[Welding helmet|helmet]]s contain dark face plates to prevent this exposure. Newer helmet designs feature a [[liquid crystal]]-type face plate that self-darken upon exposure to high amounts of UV light. Transparent welding curtains, made of a [[polyvinyl chloride]] plastic film, are often used to shield nearby workers and bystanders from exposure to the UV light from the electric arc.<ref>{{Harvnb|Cary|Helzer|2005|p=42}}</ref>

Welders are also often exposed to dangerous gases and [[particulate]] matter. GMAW produces smoke containing particles of various types of [[oxide]]s, and the size of the particles in question tends to influence the toxicity of the fumes, with smaller particles presenting a greater danger. Additionally, [[carbon dioxide]] and [[ozone]] gases can prove dangerous if ventilation is inadequate. Furthermore, because the use of compressed gases in GMAW pose an explosion and fire risk, some common precautions include limiting the amount of oxygen in the air and keeping combustible materials away from the workplace.<ref>{{Harvnb|Cary|Helzer|2005|pp=52–62}}</ref>

==Metal transfer modes==
The three transfer modes in GMAW are globular, short-circuiting, and spray. There are a few recognized variations of these three transfer modes including modified short-circuiting and pulsed-spray.<ref>{{Harvnb|American Welding Society|2004|p=150}}</ref>

===Globular===
GMAW with globular metal transfer is considered the least desirable of the three major GMAW variations, because of its tendency to produce high heat, a poor weld surface, and spatter. The method was originally developed as a cost efficient way to weld steel using GMAW, because this variation uses carbon dioxide, a less expensive shielding gas than argon. Adding to its economic advantage was its high deposition rate, allowing welding speeds of up to 110&nbsp;mm/s (250&nbsp;in/min).<ref name=cary-helzer2005p117>{{Harvnb|Cary|Helzer|2005|p=117}}</ref> As the weld is made, a ball of molten metal from the electrode tends to build up on the end of the electrode, often in irregular shapes with a larger diameter than the electrode itself. When the droplet finally detaches either by gravity or short circuiting, it falls to the workpiece, leaving an uneven surface and often causing spatter.<ref>{{Harvnb|Weman|2003|p=50}}</ref> As a result of the large molten droplet, the process is generally limited to flat and horizontal welding positions, requires thicker workpieces, and results in a larger weld pool.<ref>{{Harvnb|Miller Electric Mfg Co|2012|p=14}}</ref><ref>{{Harvnb|Nadzam|1997|p=8}}</ref>

===Short-circuiting===
Further developments in welding steel with GMAW led to a variation known as short-circuit transfer (SCT) or short-arc GMAW, in which the current is lower than for the globular method. As a result of the lower current, the heat input for the short-arc variation is considerably reduced, making it possible to weld thinner materials while decreasing the amount of distortion and residual stress in the weld area. As in globular welding, molten droplets form on the tip of the electrode, but instead of dropping to the weld pool, they bridge the gap between the electrode and the weld pool as a result of the lower wire feed rate.  This causes a [[short circuit]] and extinguishes the arc, but it is quickly reignited after the [[surface tension]] of the weld pool pulls the molten metal bead off the electrode tip. This process is repeated about 100 times per second, making the arc appear constant to the human eye. This type of metal transfer provides better weld quality and less spatter than the globular variation, and allows for welding in all positions, albeit with slower deposition of weld material. Setting the weld process parameters (volts, amps and wire feed rate) within a relatively narrow band is critical to maintaining a stable arc: generally between 100 and 200 amperes at 17 to 22 volts for most applications. Also, using short-arc transfer can result in lack of fusion and insufficient penetration when welding thicker materials, due to the lower arc energy and rapidly freezing weld pool.<ref>{{Harvnb|Craig|1991|p=11}}</ref> Like the globular variation, it can only be used on ferrous metals.<ref name=cary-helzer2005p121/><ref>{{Harvnb|Cary|Helzer|2005|p=98}}</ref><ref>{{Harvnb|Weman|2003|pp=49–50}}</ref>

====Cold Metal Transfer====
For thin materials, [[Cold Metal Transfer]] (CMT) is used by reducing the current when a short circuit is registered, producing many drops per second. CMT can be used for aluminum.

===Spray===
Spray transfer GMAW was the first metal transfer method used in GMAW, and well-suited to welding aluminium and stainless steel while employing an inert shielding gas. In this GMAW process, the weld electrode metal is rapidly passed along the stable electric arc from the electrode to the workpiece, essentially eliminating spatter and resulting in a high-quality weld finish. As the current and voltage increases beyond the range of short circuit transfer the weld electrode metal transfer transitions from larger globules through small droplets to a vaporized stream at the highest energies.<ref>{{Harvnb|Craig|1991|p=82}}</ref> Since this vaporized spray transfer variation of the GMAW weld process requires higher voltage and current than short circuit transfer, and as a result of the higher heat input and larger weld pool area (for a given weld electrode diameter), it is generally used only on workpieces of thicknesses above about 6.4&nbsp;mm (0.25&nbsp;in).<ref>{{Harvnb|Craig|1991|p=90}}</ref>

Also, because of the large weld pool, it is often limited to flat and horizontal welding positions and sometimes also used for vertical-down welds. It is generally not practical for root pass welds.<ref>{{Harvnb|Craig|1991|p=98}}</ref> When a smaller electrode is used in conjunction with lower heat input, its versatility increases.  The maximum deposition rate for spray arc GMAW is relatively high—about 60&nbsp;mm/s (150&nbsp;in/min).<ref name=cary-helzer2005p121/><ref name=cary-helzer2005p117/><ref>{{Harvnb|Cary|Helzer|2005|p=96}}</ref>

===Pulsed-spray===
A variation of the spray transfer mode, pulse-spray is based on the principles of spray transfer but uses a pulsing current to melt the filler wire and allow one small molten droplet to fall with each pulse. The pulses allow the average current to be lower, decreasing the overall heat input and thereby decreasing the size of the weld pool and heat-affected zone while making it possible to weld thin workpieces. The pulse provides a stable arc and no spatter, since no short-circuiting takes place. This also makes the process suitable for nearly all metals, and thicker electrode wire can be used as well. The smaller weld pool gives the variation greater versatility, making it possible to weld in all positions. In comparison with short arc GMAW, this method has a somewhat slower maximum speed (85&nbsp;mm/s or 200&nbsp;in/min) and the process also requires that the shielding gas be primarily argon with a low carbon dioxide concentration. Additionally, it requires a special power source capable of providing current pulses with a frequency between 30 and 400 pulses per second. However, the method has gained popularity, since it requires lower heat input and can be used to weld thin workpieces, as well as nonferrous materials.<ref name=cary-helzer2005p121/><ref>{{Harvnb|Cary|Helzer|2005|p=99}}</ref><ref>{{Harvnb|Cary|Helzer|2005|p=118}}</ref><ref>{{Harvnb|American Welding Society|2004|p=154}}</ref>

==References==
{{Reflist|20em}}

==Bibliography==
{{refbegin}}
*{{Cite book|author=American Welding Society
  |publisher=American Welding Society  |location=Miami
  |title=Welding Handbook, Welding Processes, Part 1
  |date=2004
  |isbn=0-87171-729-8
  |ref=harv
}}
*{{Cite journal|doi=10.1109/TPS.2003.815477 |title=Tracking down the origin of arc plasma science-II. early continuous discharges |date=2003 |last1=Anders |first1=A. |journal=IEEE Transactions on Plasma Science |volume=31 |pages=1060–9 |ref=harv|issue=5}}
*{{Cite book
  |last=Cary  |first=Howard B.  |last2=Helzer   |first2=Scott C.
  |date=2005
  |title=Modern Welding Technology
  |location=Upper Saddle River, New Jersey  |publisher=Pearson Education
  |isbn=0-13-113029-3
  |ref=harv
}}
*{{Cite book
  |last=Craig  |first=Ed
  |date=1991
  |title=Gas Metal Arc & Flux Cored Welding Parameters
  |location=Chicago  |publisher=Weldtrain
  |isbn=978-0-9753621-0-5
  |ref=harv
}}
*{{Cite book
  |last=Davies |first=Arthur Cyril
  |title=The Science and Practice of Welding
  |date=2003
  |publisher=Cambridge University Press
  |isbn= 0-521-43566-8
  |ref=harv
}}
*{{Cite book
  |last=Jeffus |first=Larry F.
  |title=Welding: Principles and Applications
  |date=1997
  |location=
  |publisher=Cengage Learning
  |isbn= 978-08-2738-240-4
  |ref=harv
}}
*{{Cite book
  |last=Kalpakjian  |first=Serope
  |first2=Steven R.  |last2=Schmid
  |date=2001
  |title=Manufacturing Engineering and Technology
  |publisher=Prentice Hall
  |isbn=0-201-36131-0
  |ref=harv
}}
*{{Cite book|author=Lincoln Electric
  |publisher=Lincoln Electric  |location=Cleveland
  |title=The Procedure Handbook of Arc Welding
  |date=1994
  |isbn=978-99949-25-82-7
  |ref=harv
}}
*{{Cite book
  |title=Guidelines For Gas Metal Arc Welding (GMAW)
  |publisher=Miller Electric Mfg Co
  |location=Appleton, WI
  |date=2012
  |url=https://www.millerwelds.com/~/media/miller%20electric/files/pdf/resources/bookspamphlets/mig_handbook.pdf
  |author=Miller Electric Mfg Co
  |ref=harv
}}
*{{Cite book
  |title=Gas Metal Arc Welding Guidelines
  |publisher=Lincoln Electric
  |date=1997
  |url=http://www.lincolnelectric.com/assets/global/Products/Consumable_MIGGMAWWires-SuperArc-SuperArcL-50/c4200.pdf
  |editor1-first=Jeff |editor1-last=Nadzam
  |ref=harv
}}
*{{Cite book|first1=Robert H. |last1=Todd |first2=Dell K. |last2=Allen |first3=Leo |last3=Alting |title=Manufacturing processes reference guide |publisher=Industrial Press |location=New York |date=1994 |pages= |isbn=978-0-8311-3049-7 |ref=harv}}
*{{Cite book
  |last=Weman  |first=Klas
  |date=2003
  |title=Welding processes handbook
  |location=New York  |publisher=[[CRC Press]] LLC
  |isbn=0-8493-1773-8
  |ref=harv
}}
{{refend}}

==Further reading==
*{{Cite book
  |title=Health and Safety in Welding and Allied Processes
  |last=Blunt  |first=Jane  |first2=Nigel C. |last2=Balchin
  |date=2002
  |location=Cambridge, UK  |publisher=Woodhead
  |isbn=1-85573-538-5
}}
*{{Cite book
  |title=Welded Joint Design
  |last=Hicks  |first=John
  |date=1999  |publisher=Industrial Press
  |isbn=0-8311-3130-6
}}
*{{Cite book
  |last=Minnick  |first=William H.
  |date=2007
  |title=Gas Metal Arc Welding Handbook Textbook
  |publisher=[[Goodheart–Willcox]]   |location=Tinley Park
  |isbn=978-1-59070-866-8
}}
*{{Cite book
  |title=Trends in Welding Research
  |date=2003
  |location=Materials Park, Ohio  |publisher=ASM International
  |isbn=0-87170-780-2
}}

==External links==
{{Commons category|Gas metal arc welding}}
* [http://www.esabna.com/EUWeb/MIG_handbook/592mig1_1.htm ESAB Process Handbook]
* [https://www.millerwelds.com/~/media/miller%20electric/files/pdf/resources/bookspamphlets/mig_handbook.pdf Guidelines for Gas Metal Arc Welding]
* [https://www.osha.gov/SLTC/weldingcuttingbrazing/ OSHA Safety and Health Topics- Welding, Cutting, and Brazing]
* [http://files.aws.org/wj/supplement/Quimby/ARTICLE5.pdf Fume formation rates in gas metal arc welding] –  research article from the 1999 Welding Journal
{{Metalworking navbox|weldopen}}

{{DEFAULTSORT:Gas Metal Arc Welding}}
[[Category:Arc welding]]
[[Category:Industrial gases]]
[[Category:Gas technologies]]