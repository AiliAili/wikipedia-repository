{{For|other similarly named Avery homes|Avery House (disambiguation)}}
{{good article}}
{{Infobox NRHP
  | name = Avery House
  | nrhp_type = 
  | image = Avery House front (Griswold, New London County, CT).jpg
  | caption = 
  | location= NE corner of Park and Roode Rds., [[Griswold, Connecticut]]
  | coordinates = {{coord|41|36|28|N|71|54|50|W|display=inline,title}}
| locmapin = Connecticut#USA
  | built = Circa 1770
  | architecture = Colonial
  | added = September 4, 1986
  | area = {{convert|6.5|acre}}
  | governing_body = State
  | mpsub = {{NRHP url|id=64000087|title=Connecticut State Park and Forest Depression-Era Federal Work Relief Programs Structures TR}}
  | refnum = 86001726<ref name="nris">{{NRISref|2009a}}</ref>
}}
'''Avery House''', in [[Griswold, Connecticut]], also known as '''Hopeville Pond Park House''', was built around 1770. The house is a {{convert|20|ft|m}} by {{convert|40|ft|m}}, the two-story central-chimney Colonial that was originally sheathed in clapboard and topped with a gable roof. The central chimney is on a stone base and has a built-in root cellar. Alterations in the house changed the traditional five-room first floor plan by eliminating the keeping rooms and the removal of the kitchen fireplace. It retains much of its original door frames and wrought-iron latch hardware. After the rehabilitation of the property, the Avery House became the Hopeville Park manager's residence and is a part of the [[Hopeville Pond State Park]]. It was listed on the [[National Register of Historic Places]] in 1986.

== Construction ==
The architect and the date of construction is unknown.<ref name="nom">{{cite web|url={{NRHP url|id=86001726}} |title=Connecticut Historical Association Historic Resources Inventory: Hopeville Pond Park House |date=June 1985 |author=Mary E. McCahon |publisher=National Park Service}} and {{NRHP url|id=86001726|title=''Accompanying photo, exterior, undated''|photos=y}}</ref> The National Register of Historic Places nomination dates it to circa 1770 is substantiated by the construction and interior woodwork of the house.<ref name=nom /> Measuring approximately {{convert|20|ft|m}} by {{convert|40|ft|m}}, the two-story house is a central-chimney Colonial that was originally sheathed in clapboard and topped with a gable roof.<ref name=nom /> The central chimney is on a stone base and has a built-in root cellar.<ref name=nom /> At the time of its nomination, the house was using asphalt shingles on its roof.<ref name=nom /> The house is historically significant for its [[bolection]] molding and around fireplaces and shallow molded shelves above the fireplaces in the front chambers. Fireplace in the east room has a large single over-mantel panel and the west fireplace has two panels. Mary McMahon, who prepared the inventory form for the National Register of Historic Places, writes "A handsome corner cabinet with butterfly shelves is also located in the east room".<ref name=nom />  The second floor retains its original flooring and mantels, and the post and beam construction is visible throughout the interior of the house.<ref name=nom />

Alterations in the house changed the traditional five-room first floor plan by eliminating the keeping rooms and the removal of the kitchen fireplace.<ref name=nom /> Despite this, the house's original door frames and much of the doors with wrought-iron latch hardware remained by the time of its nomination in 1985.<ref name=nom /> In 1935, the [[Civilian Conservation Corps]] (CCC) rehabilitated the property for park use.<ref name="mps">{{cite web | url={{NRHP url|id=64000087}} | title=Connecticut State Park and Forest Depression-Era Federal Work Relief and or common Programs Structures Thematic Resource | publisher=National Park Service | date=25 July 1986 | accessdate=18 April 2014 | author=McCahon, Mary Elizabeth}}</ref> The house was acquired by the State of Connecticut in 1938.<ref name=nom /> The CCC shingled the exterior of the house, but it is uncertain if the "inappropriate porch" was an addition they made.<ref name=nom /> The CCC is also believed to be the builders of the garage/workshop.<ref name=nom />

== Owners ==
The owners of the house are also unknown, but two records do exist.<ref name=nom /> The 1854 Baker map of [[New London, Connecticut|New London]], listed owner of the house was Captain J. Avery.<ref name=nom /> An 1868 map lists a H. Bennett in residence.<ref name=nom /> After the rehabilitation of the property, the Avery House became the Hopeville Park manager's residence.<ref name=nom />
 
== Importance ==
The Avery House was listed on the National Register of Historic Places in 1986. According to the nomination form and the multiple property submission sheet, the house is historically significant as an example of a well-preserved house that does not have its historical integrity degraded by alterations. McMahon writes that "[t]he interior paneling is the finest in any 18th-century house owned by DEP ([[Connecticut Department of Environmental Protection]])."<ref name=nom /> Originally, the house was set to have the surrounding {{convert|400|ft|m}} as part of its designation, but this was later changed to {{convert|300|ft|m}} because it would not impact the historic setting.<ref name=nom />

==See also==
*[[National Register of Historic Places listings in New London County, Connecticut]]

==References==
{{reflist}}

{{National Register of Historic Places}}

[[Category:Houses on the National Register of Historic Places in Connecticut]]
[[Category:Houses completed in 1770]]
[[Category:Houses in Griswold, Connecticut]]
[[Category:National Register of Historic Places in New London County, Connecticut]]
[[Category:1770 establishments in Connecticut]]