The purpose of '''Lean Accounting''' is to support the [[Lean Enterprise|lean enterprise]] as a [[business strategy]].  It seeks to move from traditional [[accounting]] methods to a system that measures and motivates excellent business practices in the lean enterprise.

== Introduction ==

What we now call [[lean manufacturing]] was developed by [[Toyota]] and other Japanese companies. Toyota executives claim that the famed [[Toyota Production System]] was inspired by what they learned during visits to the [[Ford Motor Company]] in the 1920s and developed by Toyota leaders such as [[Taiichi Ohno]] and consultant [[Shigeo Shingo]] after [[World War II]].  As pioneer American and European companies embraced lean manufacturing methods in the late 1980s, they discovered that lean thinking must be applied to every aspect of the company including the financial and management accounting processes.<ref>{{cite journal|last=Emiliani|first=Bob|title=Better Thinking Better Results|journal=CLBM; 2 edition, Wethersfield, CT, USA|date=March 23, 2007}}</ref> (See also, [[William Deming]].)

There are two main thrusts for Lean Accounting. The first is the application of lean methods to the company's accounting, control, and measurement processes. This is no different from applying lean methods to any other processes. The objective is to eliminate waste, free up capacity, speed up the process, eliminate errors and defects, and make the process clear and understandable.

The second (and more important) thrust of Lean Accounting is to fundamentally change the accounting, control, and measurement processes so they motivate lean change and improvement, provide information that is suitable for control and decision-making, provide an understanding of customer value, correctly assess the financial impact of lean improvement, and are themselves simple, visual, and low-waste. Lean Accounting does not require the traditional management accounting methods like [[standard costing]], activity-based costing, variance reporting, [[cost-plus pricing]], complex transactional control systems, and untimely confusing [[financial report]]s. These are replaced by
* lean-focused performance measurements
* simple summary direct costing of the value streams
* decision-making and reporting using a ''box score''
* financial reports that are timely and presented in "[[plain language]]" that everyone can understand
* radical simplification and elimination of transactional control systems by eliminating the need for them
* driving lean changes from a deep understanding of the value created for the customers
* eliminating traditional [[budgeting]] through monthly sales, operations, and financial planning processes (SOFP)
* value-based pricing
* correct understanding of the financial impact of lean change

As an organization becomes more mature with lean thinking and methods, they recognize that the combined methods of Lean Accounting in fact creates a Lean Management System (LMS) designed to provide the planning, the operational and financial reporting, and the motivation for change required to prosper the company's on-going lean transformation.<ref name="Maskell">{{cite journal|last=Maskell|first=Brian & BMA Inc Team|title=Lean Business Management System|journal=BMA Publishing, Cherry Hill, NJ, USA|date=September 1, 2007}}</ref>

Up until 2006, the methods of Lean Accounting were not clearly defined because they had been developed by different people in different companies. A meeting was held at the 2005 Lean Accounting Summit ([http://www.leanaccountingsummit.com Lean Accounting Summit]) conference including a number of leaders in the field, and a decision was made to develop a document called "The Principles, Practices, and Tools of Lean Accounting" (PPT) ([http://maskell.com/lean_accounting/subpages/lean_accounting/la_ppt.html Lean Accounting PPT]). While the methods of lean accounting are continually evolving, the PPT lays out the primary methods of Lean Accounting and  shows how they fit together into a Lean Management System. The PPT emphasizes not only the tools and methods of Lean Accounting, but also the need for focusing on customer value and the empowerment (or respect) for people. The PPT was published in Target, the Journal of the Association of Manufacturing Excellence (AME) in 2006. ([http://www.leanaccountingsummit.com/LeanAccountingDefined-Target.pdf Lean Accounting PPT article])

=== The Vision for Lean Accounting ===

#Provide accurate, timely, and understandable information to motivate the lean transformation throughout the organization, and for decision-making leading to increased customer value, growth, profitability, and cash flow.
#Use lean tools to eliminate waste from the accounting processes while maintaining thorough financial control.
#Fully comply with generally accepted accounting principles (GAAP), external reporting regulations, and internal reporting requirements.
#Support the lean culture by motivating investment in people, providing information that is relevant and actionable, and empowering continuous improvement at every level of the organization.

=== Why is lean accounting needed? ===

There are positive and negative reasons for using Lean Accounting. The positive reasons include the issues addressed in the "Vision for Lean Accounting" shown above. Lean Accounting provides accurate, timely and understandable information that can be used by managers, sales people, operations leaders, accountants, lean improvement teams and others. The information gives clear insight into the company's performance; both operational and financial. The Lean Accounting reporting motivates people in the organization to move lean improvement forward. It is often stated that "what you measure is what will be improved." Lean accounting measures the right things for a company that wants to drive forward with lean transformation.

[[File:Benefits of Lean Accounting 3-2 (1).png|right]]

Lean Accounting is also itself lean. The information, reports, and measurements can be provided quickly and easily. It does not require the complex systems and wasteful transactions that are usually used by manufacturing companies. The simplicity of Lean Accounting frees up the time of the financial people and the operational people so that they can become more actively involved in moving the company forward towards its strategic goals. The role of the financial professional moves away from bookkeeper and reporter and towards strategic partnering with the company leaders.

At a deeper level Lean Accounting matches the cultural goals of a lean organization. The simple and timely information empowers people at all levels of the organization. The financial and performance measurement information is organized around value streams and thereby honors the lean principle of value stream management. The emphasis on customer value is also derived from the principles of lean thinking. The way a company accounts and measures its business is deeply rooted in the culture of the organization. Lean Accounting has an important role to play in developing a lean culture within an organization.

=== Why is traditional accounting not needed? ===

The negative reasons for using Lean Accounting lie with the inadequacy of traditional accounting systems to support a lean culture. Everybody working seriously on the lean transformation of their company eventually bumps up against their accounting systems. Traditional accounting systems (particularly those using [[standard costing]], activity-based costing, or other full absorption methods) are designed to support traditional management methods. As a company moves to lean thinking, many of the fundamentals of its management system change and traditional accounting, control, and measurement methods become unsuitable. Some examples of this are:

[[File:Problems with Traditional Accounting.png|right|Traditiional Accounting Pushes Back Against Lean]]

*   Traditional accounting systems are large, complex processes requiring a great deal of non-value work. Lean companies are anxious to eliminate this kind of non-value work.
*   They provide measurements and reports like labor efficiency and overhead absorption that motivate large [[batch production]] and high [[inventory]] levels. These measurements are suitable for mass production-style organizations but actively harmful to companies with lean aspirations.
*   The traditional accounting systems have no good way to identify the financial impact of the lean improvements taking place throughout the company. On the contrary, the financial reports will often show that bad things are happening when very good lean change is being made. One example of this is that traditional reporting shows a reduction in profitability when inventory is reduced. Lean companies always make significant inventory reductions and the accounting reports show negative results.<ref>{{cite journal|author1=Cooper, Robin  |author2=Maskell, Brian|title=How to Manage Through Worse-Before-Better|journal=MIT Sloan Management Review, Cambridge, MA, USA|date=Summer 2008|volume=49|issue=4|pages=58–65}}</ref>
*   Traditional accounting reports use technical words and methods like "overhead absorption", "[[gross margin]]", and many others. These reports are not widely understood within most organizations. This may be acceptable when the financial reports are restricted to senior managers, but a lean company will seek to empower the entire workforce. Clear and understandable reporting is required so that people can readily use the reports for improvement and decision-making.
*   Traditional companies use standard product (or service) costs which can be misleading when making decisions related to quoting, profitability, make/buy, sourcing, product rationalization, and so forth. Lean companies seek to have a clearer understanding of the true costs associated with their processes and value streams.

There are of course traditional methods for overcoming some of these issues and problems. Indeed, few of the methods of Lean Accounting are new ideas. They are mostly adaptations of methods that have been used for many years, and have been codified into a Lean Management System designed to support the needs of lean thinking organizations.

===Where does Lean Accounting apply?===

As with most lean methods, Lean Accounting was developed to support manufacturing companies, and most of the implementation of Lean Accounting has been within manufacturing organizations. Now that lean methods are moving into other industries like [[financial services]], [[healthcare]], [[government]], and [[education]] there are some initial examples of the application of Lean Accounting in these industries.

==Getting Started==
[[File:Basic Lean Acct Diagram-3B.png|center|550px]]

=== Application to accounting processes ===

In the early stages of lean it is important to apply lean improvement throughout the organization; and there is nowhere more suitable than the accounting processes. These include the month-end close, [[accounts payable]], [[accounts receivable]], [[payroll]], [[cost accounting]], expense reporting, and so forth.  There are three reasons for applying lean improvement methods to the accounting processes:

# The processes will be improved and the company's operations made better.
# The finance people will learn a lot about lean methods. Lean is not learned from books but by actual hands-on experience.
# The removal of waste will free up time for the finance people to work on the introduction of Lean Accounting.

Some people object to making changes to the Accounting processes because they ask why we would want to spend time making processes better when in fact we will be eliminating them in the future. The answer to this is that with lean we are always interested in making many small improvements. We are not looking for the "[[silver bullet]]" that will solve all problems. On the contrary, we are looking to engage the entire work force in many smaller changes that lead to massive improvement over time. It is, of course, our objective over time to largely eliminate most of these wasteful accounting processes, but at the earlier stages of lean change we are content to improve the processes, provide learning to the finance people, and free up their time for the more significant lean changes in the future.<ref name="Maskell"/><ref name="Fiume & Cunningham">{{cite journal|last=Fiume & Cunningham|title=Real Numbers: Management Accounting in a Lean Organization|journal=Managing Times Press; 2 edition, Durham, NC, USA|date=March 25, 2003}}</ref>

=== Lean performance measurements ===

The control of the production (and other) processes is achieved by visual performance measurements at the shop-floor and value stream level. These measurements eliminate the need for the shop-floor tracking and variance reporting favored by traditional cost accounting systems. There are (at least) three levels of operational performance measurements.

{| class="wikitable" border="1"
|-
! 
! PURPOSE
! PLAN DO CHECK ACT IMPROVEMENT
! TYPICAL FREQUENCY
|-
| Company or Plant Measurements
| Enable the senior managers of the company monitor<br /> the achievement of the company's strategy.
| Strategy Deployment
| Monthly
|-
| Value Stream Measurements
| Track the performance of the value stream and provide<br /> information to drive continuous improvement (CI).
| Continuous Improvement
| Weekly
|-
| Cell and Process Measurements
| Enable the cell team to monitor<br /> and control their own activities.
| Identify defects and eliminate them
| Hourly or by shift
|}

Continuous improvement (CI) is motivated and tracked using value stream performance boards. Typically these visual boards are updated weekly and used by the value stream CI team to identify improvement areas, initiate [[PDCA]] projects, and monitor their progress. These boards show the value stream performance measurements, [[pareto chart]]s (or other root cause analysis), and information about the CI projects. The boards also show the current and future state maps together with the project plan to move from current to future state. The Value Stream Performance Boards become “mission control” for both break-through improvement and continuous improvement of the value stream.

Typical measurements include:<ref name="Maskell & Baggaley">{{cite journal | first1 = Brian | last1 = Maskell |first2 = Bruce| last2 = Baggaley | title=Practical Lean Accounting | journal=Productivity Press| location = New York, NY|date=December 19, 2003}}</ref>
* Productivity (sales/person)
* Process control (on-time shipment to customer requirement)
* Flow (dock-to-dock days or hours)
* Quality & Standardized Work (first time through without scrap or rework)
* Linearity and overall improvement (average cost)
* People participating in CI
* Safety (Safety cross showing lost time, accidents, near-misses, etc.)

Cell and process measurements are reported frequently – often hourly – by the people working in the cell or the process. The measurements are used to control the process and identify defects. When defects are identified they are "fixed" in the short term to serve the customers today and solved over the longer term so that they never occur again.

Typical measurements include:
* Day-by-the-Hour production quantities
* First Time Thru without scrap or rework
* WIP to SWIP (work-in-process inventory within the cell or process compared to the standard work-in-process required within the process)
* Operational equipment effectiveness – OEE (for machine driven operations and particularly for bottleneck or constraint machines.)
* "Just-Do-It" suggestions per person.

For a "starter set" of lean performance measurements: [http://maskell.com/lean_accounting/subpages/lean_accounting/ppt/accounting_processes/B1aPMLinkageAnalysis.pdf Lean Performance Measurements Starter Set]

==Financial Reports for Lean Operations==

=== Value stream costing ===

Cost and profitability reporting is achieved using Value Stream Costing, a simple summary direct costing of the value streams. The value stream costs are typically collected weekly and there is little or no allocation of "overheads.” This provides financial information that can be clearly understood by everybody in the value stream which in turn leads to good decisions, motivation to lean improvement across the entire value stream, and clear accountability for cost and profitability. Weekly reporting also provides excellent control and management of costs because they can be reviewed by the value stream manager while the information is still current.<ref name="Maskell & Baggaley"/><ref>{{cite journal|last=Ed. Stenzel|first=Joe|title=Lean Accounting: Best Practices for Sustainable Integration|journal=Wiley, New York NY , USA|date=April 6, 2007}}</ref>

=== Plain language financial statements ===

Lean accounting provides financial reports that are readily understandable to anyone in the company. The income statements are in “plain language” and the information is presented in a way that is no more complicated than a [[household budget]]. Plain language income statements are easy to use because they do not include misleading and confusing data relating to standard costs and hosts of incomprehensible variance figures. When used in meetings, plain language financial statements change the question from “What does this mean?” to “What should we do?”.

=== Box Score reporting ===

[[File:Box Score 1.jpg|right|500px|Right|Example of a Box Score used for weekly value stream performance reporting]] Box Scores are used widely within lean accounting. The standard format of the box score shows a 3-dimensional view of value stream performance; operational performance measurements, financial performance, and how the value stream capacity is being used. The capacity information shows how much of the capacity within the value stream is used productively, how much is used to do non-productive activities, and how much value stream capacity is available for use. The box score shows the value stream performance on a single sheet of paper and using a simple and accessible format.

The box score shown on the right shows weekly value stream performance. Other box scores are used for decision-making, for assessing the financial impact of lean improvement, for selecting or prioritizing such issues as capital acquisitions using the 3P approach, and other reporting and decision-making requirements. Companies using lean accounting often have a standard box score format and require that all decisions relating to a value stream be presented using the standard box scores. This leads to operational and financial information being consistent and well understood when it is used.<ref name="Maskell & Baggaley"/>

==Making Decisions without the Use of Product or Process Costs==

=== Decision-Making using Box Scores and Value Stream Cost Information ===

<!-- Image with unknown copyright status removed: [[File:Box Score 2.gif|right|500px]]     -->
[[File:Box Score 22.gif|right|500px]]Routine decision-making – including quotes, profitability, make/buy, sourcing, product rationalization, and so forth – is achieved using simple yet powerful information that is readily available from the box score. There is no need to use a standard cost again for these important decisions. The Box Score shows an example of this method for decision-making related to sourcing of a new product.

Most companies using lean accounting create standard templates for the various kinds of daily routine decisions. These will include assessing the profitability of a sales order or request for quote, make-buy decisions for products or components, the impact of improvement projects, and so forth. These templates often access box score information from the lean accounting information within the company's systems. The availability of capacity is often a crucial issue when making these kinds of short-term decisions.

The box score shown in this example demonstrates a short term decision and assumes that the company's capacity and costs are largely fixed. There are two other kinds of decisions used regularly in lean companies; medium term decisions and strategic decisions. Box Scores are also used for medium term decisions but there is no assumption of fixed capacity and costs. The template shows how the capacity and resources need to be changed to fulfill the decision. These decisions are linked in the SOFP (Sales, Operations, and Financial Planning) process that typically looks out 12–18 months. The Box Score is also used for strategic decisions such as the introduction of new products, and the templates feed into the company's Strategy Deployment (Hoshin Kanri) and Target Costing processes.

The Box Score method is flexible to meet the needs of different kinds of decisions, yet using the same underlying approach that we do not try to calculate a fully absorbed product cost. Instead the impact of these decisions on the value stream as a whole is used to assess the suitability of each of our choices. This leads to better understanding and better decisions, when used with standard decision-making processes.<ref name="Maskell"/>

=== Product or service costing ===

Under most circumstances it is not necessary to calculate product or service costs.<ref name="Fiume & Cunningham"/> Traditional manufacturing companies usually calculate a fully absorbed product cost using complex methods for the allocation of overhead costs, and they use these product costs for decision-making, inventory valuation, and performance measurements in the form of variance analysis and such metrics as individual efficiency. Similar methods are used in service organizations to estimate the cost of each service they provide. Companies employing lean accounting methods recognize that standard costs and other methods for fully absorbed product or service costing lead to poor decisions and motivate anti-lean behavior.<ref>{{cite journal|last=Solomon|first=Jerrold M|title=Who's Counting? A Lean Accounting Business Novel|journal=WCM Associates, Durham, NC, USA|date=April 1, 2003}}</ref> These companies also find that there is no need to calculate a product cost because all the uses of product costs within traditional companies can be addressed in lean accounting using simpler and better methods. Decision-making, inventory valuation, performance measurements, and other uses of fully absorbed product costs are all achieved using other lean accounting methods. If a product cost is required – for reporting international transfer pricing, for example – then these can be calculated using simpler and more lean-focused methods like Features & Characteristics costing.<ref name="Maskell & Baggaley"/>

==External Reporting==

=== Closing the books ===

[[File:Lean Accounting Closing the Books Example.jpg|right|600px]]The primary collection of [[revenue]] and costs is done using Value Stream Costing, and (typically) weekly value stream income statements are used by the value stream managers to control costs and work to reduce costs. A typical lean organization will have several revenue earning or ''[[order fulfillment]]'' value streams, one or two ''new product development'' value streams, and then a small group of people and departments that ''support'' the value streams but are not in the value streams.  These external support people include, for example, a plant or division manager, [[Human resources|HR]], [[Information systems]], and so forth. The costs of these support people is relatively small in comparison to the value streams.

External reporting is achieved by taking the monthly value stream income statements and the financial statement for the support people and adding them together to provide the consolidated financial report for the company or division as a whole. This month-end close provides financial reports for the company that can be used for all external reporting. There is usually a requirement for some "below the line" adjustments to bring the income state in line with ''generally accepted accounting principles'' (GAAP). These adjustments include any change of inventory value between now and last month, group and corporate overhead allocations, and other miscellaneous adjustments like exchange rate gains and losses. The "[[bottom line]]" of the adjusted statement will of course be the same as the traditional statements. There is no formal change of accounting method and the bottom line will therefore be the same.

=== Inventory valuation ===

An important aspect of financial control is the evaluation of [[inventory]]. Lean manufacturing always leads to substantial inventory reductions. When inventories are low and under good control (using pull systems, single-piece flow, supplier partnerships, etc.), the valuation of inventory becomes much less complex. Lean Accounting contains a number of methods for valuing inventory that are simple, accurate, and often visual. Several of these methods do not require any computer-based inventory tracking at all.

=== Compliance to regulatory requirements ===

A question that always comes up when discussing lean accounting is whether these methods comply with regulatory accounting requirements and [[Generally accepted accounting principles|GAAP]] (generally accepted accounting principles). Lean accounting fully complies with all statutory and generally accepted accounting requirements in the United States and Europe, including the unique requirements of German, Swiss, and Italian regulation. Lean accounting also complies with the increasingly popular [[International Accounting Standards]] (IAS) that is seeking to create a single world-wide approach. When moving from traditional accounting methods to lean accounting there is no "change of accounting" because the external reporting outcome of lean accounting uses the same accrual based actual costing required by GAAP and statutory regulations. There is an argument that lean accounting lends itself better to statutory regulations because they require reporting at actual cost. Lean accounting uses actual costs throughout, whereas traditional accounting uses standard costs that must then be adjusted to actual costing for external reporting

==Further Simplifying the Accounting Processes==

=== Transaction elimination ===

Traditional companies use complex, transaction-based information systems like [[Manufacturing resource planning|MRPII]] or [[Enterprise Resource Planning|Enterprise]] Systems (ERP) to maintain financial and operational control of their processes. Lean organizations bring their process under good control using lean methods, visual control, low inventories, short lead times, and – most importantly – identifying the root causes of the problems that create the lack of control. Once these root causes have been addressed and the process brought under control, it is no longer necessary to use these complex and wasteful transactional systems, and they can be gradually eliminated.

In manufacturing companies the transaction-heavy documents tend to be production work orders and inventory tracking on the computer. Over time, as lean methods eliminate the need for these documents  in favor of visual management, these documents can be eliminated and the thousands of wasteful transaction can be eliminated. One large North American aircraft manufacturer eliminated three trillion transactions in one year using this approach. The "ideal" for a manufacturing company is to have only two types of transactions within the production processes; the receipt of raw materials and the shipment of finished product. These two transactions are legally required owing to change of ownership. Everything else within the production process can be addressed better, quicker, easier, and less wastefully using visual, lean methods.

Other kinds of service companies like banks, [[healthcare]], [[insurance]] and others, have similarly transaction-heavy processes that can be radically simplified through the use of lean methods of control. Almost every company can largely eliminate their purchasing and accounts payable processes together with the wasteful and complicated three-way matching through using lean methods.

Accounting controls have always been important, and it is essential that Lean Accounting enhance these controls, and does not weaken them. It is important to bring the company’s [[auditors]] into the Lean Accounting process at the earliest stages. A primary tool to ensure that Lean Accounting changes are made prudently is the ''Transaction Elimination Matrix''. Using the transaction elimination matrix we can determine what lean methods must be in place to enable us to eliminate traditional, transaction-based processes without jeopardizing financial (or operational) control. These decisions are made ahead of time and become a part of the overall lean transformation; in some cases driving the lean changes and improvements.

==Focusing on Customer Value==

=== Target costing ===

[[Target costing]] is the tool for understanding how the company creates value for the customer and what must be done to create more value. Target Costing is used when new products are being designed and/or when the value stream team needs to understand the changes required to increase the value for the customers. The outcome of this highly cross-functional and cooperative process is a series of initiatives to create more value for the customer and to bring the product costs into line with the company’s need for short and long term financial stability. These improvement initiatives encompass sales and [[marketing]], [[product design]], operations, [[logistics]], and administrative processes within the company.<ref>{{cite journal|last=Cooper & Slagmulder|title=Target Costing and Value Engineering|journal=Productivity Press, New York, NY, USA|year=1997}}</ref>

=== Value-based pricing ===

The first of the five principles of lean thinking is ''value to the customer''. The prices of products and services are set according to the value created for the customers. Lean accounting includes methods for calculating the amount of value created by a company's products and services, and form that knowledge to establish prices. This approach is in stark contrast to many traditional companies that calculate their prices using the [[Cost-plus pricing|cost-plus method]]. The cost-plus method establishes prices by calculating a fully absorbed product cost and then adding on an acceptable profit margin. This cost-plus methods leads to serious errors in pricing because it creates a false linkage between price and cost. The price of a product is unrelated to the cost of manufacturing and supplying that product. The price of a product or services is entirely determined by the amount of value created by the product in the eyes of the customers. Lean accounting methods enable value-based pricing.

== External links ==
* [http://www.leanaccountingsummit.com/LeanAccountingDefined-Target.pdf Lean Accounting: What's It All About?], collaborative article written by lean accounting thought leaders following the 2005 inaugural Lean Accounting Summit in Dearborn, MI
* [http://www.leanaccountingsummit.com/ Lean Accounting Summit], Annual gathering of the world's lean accounting thought leaders

==References==
{{Reflist}}

{{DEFAULTSORT:Lean Accounting}}
[[Category:Accounting systems]]
[[Category:Lean manufacturing]]