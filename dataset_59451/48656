{{Use dmy dates|date=December 2014}}
{{Infobox television
| show_name            = I Am a Man [[File:Republic Of Korea Broadcasting-TV Rating System(15).svg|24px|Republic_Of_Korea_Broadcasting-TV_Rating_System(15).svg]]
| image                = 나는남자다하단.jpg
| caption              = ''I Am a Man'' title card 
| show_name_2          = ''Yoo Jae-suk's I am a Man''
| genre                = Talk show, entertainment show
| creator              =
| developer            =
| writer               = 
| director             = Lee Dong-hoon
| creative_director    = 
| presenter            =
| starring             = [[Yoo Jae-suk]]<br/>[[Im Won-hee]]<br/>[[Kwon Oh-joong]]<br/>[[Jang Dong-min]]<br/>[[Heo Kyung-hwan]]
| judges               =
| voices               =
| narrated             = 
| theme_music_composer =
| opentheme            =
| endtheme             =
| composer             =
| country              = [[South Korea]]
| language             = Korean
| num_seasons          = 1 (Provisional)
| num_episodes         = [[KBS2]]: 20 + 1 Pilot<br/>[[KBS World]]: 20 + 1 Pilot 
| list_episodes        = 
| executive_producer   = 
| producer             = 
| editor               =
| location             = KBS Annex Building, Yeouido-dong, Seoul
| cinematography       = 
| camera               = [[Multicamera setup]]
| runtime              = 70 minutes
| company              = [[Korean Broadcasting System]]
| distributor          = [[KBS World]], [[:ko:KBS W|KBS W]], [[:ko:KBS 조이|KBS Joy]], [[:ko:E채널|E Channel]], 
| picture_format       = [[HDTV]] 
| audio_format         = [[Stereo sound|Stereo]]
| first_aired          = {{start date|df=yes|2014|4|9}} (pilot episode)<br>{{start date|df=yes|2014|8|8}}
| last_aired           = {{end date|df=yes|2014|12|19}}
| preceded_by          = ''Mamma Mia''
| followed_by          = ''[[Brave Family]]'' 
| related              = 
| website              = http://www.kbs.co.kr/2tv/enter/iamaman
| production_website   = 
| image_size           = 
| channel              = [[KBS2]]
}}
{{KoreanText}}

'''''I Am a Man''''' ({{ko-hhrm|나는 남자다}}), also known as ''Yoo Jae-suk's I am a Man'', is a South Korean talk show. Its initial (pilot episode) aired on 7 April 2014. The show's regular broadcast started on 8 August 2014, and ended on 19 December 2014.

The program airs on two channels, [[KBS2]] and [[KBS World]]; the latter broadcasts the episodes several days after the former. As of 10 January 2015, there have been 20 episodes aired in [[KBS2]] and 20 episodes on [[KBS World]]. The show is hosted by [[Yoo Jae-suk]], [[Im Won-hee]], [[Kwon Oh-joong]], [[Jang Dong-min]], and [[Heo Kyung-hwan]]. ''I Am a Man'' is intended largely for the male audience; female viewers are "welcome to watch it in secret."

''I Am a Man'''s show theme is a show of men, by the men, for the men.

==Broadcast dates==
{|class="wikitable"
!Broadcast Channel!!Broadcast Date!!Broadcast Time!!Remarks
|- align='center'
!rowspan="2" style="vertial-align: center;"|[[KBS2]]
|9 April 2014||Wednesday 23:10 ~ 00:30|| Pilot Episode
|- align='center'
|8 August 2014 ~ 19 December 2014<ref>http://www.kbs.co.kr/2tv/enter/iamaman/view/vod/index.html</ref> ||Friday 23:10 ~ 00:30|| Regular Episode
|- align='center'
!rowspan="5" style="vertial-align: center;"|[[KBS World]]
|7 May 2014||rowspan="2"|Wednesday 23:10 ~ 00:30||Pilot Episode
|- align='center'
|27 August 2014 ~ 29 October ||rowspan="2"| Regular Episode
|- align='center'
|7 November 2014 ~ Present||Saturday 00:10 ~ 01:30
|- align='center'
|27 August 2014 ~ 2 November||Thursday 07:15 ~ 08:25<br>Sunday 15:20 ~ 16:30||rowspan="2"|Re-run Regular Episode
|- align='center'
|10 November 2014 ~ Present||Monday 07:40 ~ 08:50<br>Friday 16:20 ~ 17:30 
|}

'''Note:''' 
* As of November 2014, some of the [[KBS World]] program schedule information may become outdated due to the [http://kbsworld.kbs.co.kr/notice_event/notice_event_view.html?no=331&mcode=notice&num=0 new program line-up].
* The broadcast time is based on GMT +9 time zone (Korea, Japan, East Timor, Palau).

== Hosts ==

* [[Yoo Jae-suk]]
* [[Im Won-hee]]
* [[Kwon Oh-joong]]
* [[Jang Dong-min]]
* [[Heo Kyung-hwan]]
* [[Kim Je-dong]] (Regular member of ''I Am a Man'' Special – Man and women special)

[[Noh Hong-chul]] was offered to host the show, but had to be replaced by [[Kwon Oh-joong]] as he was unavailable due to the show's coinciding with another program he hosts, MBC's ''[[I Live Alone (TV series)|I Live Alone]]''. He only appear on ''I Am a Man'' pilot episode.

== History==
''I Am a Man'' replaced ''Mamma Mia'', a family show taken off air after its final episode on 19 March 2014.<ref>{{cite news|url=http://www.allkpop.com/article/2014/02/kbs-cancels-mamma-mia-and-mamado-new-variety-shows-to-premiere-in-spring|title=KBS cancels 'Mamma Mia' and 'Mamado' + new variety shows to premiere in Spring|work=Allkpop|date=27 February 2014|accessdate=27 February 2014}}</ref>

One of ''I Am a Man''<nowiki>'</nowiki>s original hosts, [[Noh Hong-chul]], was replaced by [[Kwon Oh-joong]] as his other show which he is now starring in, MBC's ''[[I Live Alone (TV series)|I Live Alone]]'', occupied the same time slot.<ref>{{cite news|url=http://www.dkpopnews.net/2014/07/yoo-jae-suks-new-talk-show-im-man-to-be.html|title=Yoo Jae Suk's new talk show ''I'm A Man'' to be officially broadcast on August 8th|work=Dkpopnews|date=11 July 2014|accessdate=11 July 2014}}</ref>

TenAsia conducted an interview with ''I Am a Man''<nowiki>'</nowiki>s production director (PD) after the filming of ''I Am a Woman''<ref>http://tenasia.hankyung.com/archives/344057.</ref>

''I Am a Man'' aired its last episode on 19 December 2014. ''I Am a Man'' PD still discuss on extend this show to Season 2.<ref>{{Cite web|url = http://www.dkpopnews.net/2014/12/im-man-to-broadcast-its-last-episode-on.html|title = 'I'm A Man' to broadcast its last episode on December 19th + in discussion for season 2|date = 14 December 2014|accessdate = 18 December 2014|website = Daily Kpop News|publisher = Kpopnews|last = |first = }}</ref>

It was reported that [[KBS2]]'s new program, ''[[Brave Family]]'' will take over the broadcast slot of ''I Am a Man'' before the show begins its second season.<ref>{{Cite web|url = http://www.soompi.com/2014/12/12/cnblues-minhyuk-aoas-seolhyun-and-more-join-new-kbs-variety-show-brave-family/|title = CNBLUE’s Minhyuk, AOA’s Seolhyun, and More Join New KBS Variety Show "Brave Family" |date = 12 December 2014|accessdate = 10 January 2015|website = Soompi|publisher = Soompi|last = |first = }}</ref><ref>{{Cite web|url = http://www.allkpop.com/article/2014/12/brave-family-reported-to-be-the-follow-up-program-after-i-am-a-man|title = 'Brave Family' reported to be the follow-up program after ''I Am a Man''|date = 12 December 2014|accessdate = 10 January 2015|website = Allkpop|publisher = Allkpop|last = |first = }}</ref>

==Filming location==
''I Am a Man''<nowiki>'</nowiki>s filming location is the KBS Annex Building located in [[Yeouido-dong]], [[Seoul]]. The show is typically recorded on Sunday and aired on Friday.

==Pilot episode==
The pilot episode of ''I Am a Man'' premiered on 9 April 2014 on [[KBS2]], followed by its 7 May 2014 release on [[KBS World]] to estimate the show's potential audience. The pilot episode featured [[Bae Suzy]] and Koh Yu-Jin as special guests.<ref>{{Cite news|url=http://www.allkpop.com/article/2014/03/suzy-to-make-a-guest-appearance-on-yoo-jae-suks-new-variety-show-im-a-man|title=Suzy to make a guest appearance on Yoo Jae Suk's new variety show 'I'm A Man'|date=20 March 2014|publisher=''Allkpop''|accessdate=20 March 2014}}</ref>

The pilot episode was preceded by a teaser trailer which introduced the hosts and presented the scope of the show.<ref>{{Cite news|url=http://www.allkpop.com/article/2014/03/teaser-released-for-new-pilot-program-im-a-man-with-noh-hong-chul-yoo-jae-suk-and-lim-won-hee|title=Teaser released for new pilot program I'm a Man' with Noh Hong Chul, Yoo Jae Suk, and Im Won Hee|date=7 March 2014|publisher=''Allkpop''|accessdate=7 March 2014}}</ref>

The AGB Nielsen rating for the pilot episode was 4.1%.<ref>{{Cite news|url=http://www.newsculture.tv/sub_read.html?uid=34882&section=sc158|script-title=ko:유재석의 '나는 남자다', 정규편성 확정|date=23 June 2014|publisher=''Newsculture''|accessdate=23 June 2014|language=ko}}</ref><ref>{{Cite news|url=http://www.soompi.com/2014/04/10/yoo-jae-suks-new-show-im-a-man-gets-off-to-a-decent-start/|title=Yoo Jae Suk's New Show "I'm a Man" Gets Off to a Decent Start|date=10 April 2014|publisher=''Soompi''|accessdate=10 April 2014}}</ref><ref>{{Cite news|url=http://mwave.interest.me/enewsworld/en/article/63323/korean-tv-ratings-20140409|title=KBS Jumps in With New Drama and Variety Program|date=10 April 2014|publisher=Mvave|accessdate=10 April 2014}}</ref>

==Regular show==
The show was included in the [[KBS2]] program lineup and began airing regularly on 8 August 2014 on [[KBS2]], and on 27 August 2014 on [[KBS World]].<ref>{{cite news|url=http://www.allkpop.com/article/2014/07/yoo-jae-suks-variety-im-a-man-to-start-airing-august-8-on-a-seasonal-system|title=Yoo Jae Suk's variety 'I'm A Man' to start airing August 8 on a seasonal system|work=Allkpop|date=24 July 2014|accessdate=24 July 2014}}</ref><ref>{{cite news|url=http://www.allkpop.com/article/2014/06/yoo-jae-suks-pilot-program-im-a-man-to-start-airing-regularly-this-august|title=Yoo Jae Suk's pilot program 'I'm A Man' to start airing regularly this August|work=Allkpop|date=23 June 2014|accessdate=23 June 2014}}</ref><ref>{{cite news|url=http://www.soompi.com/2014/06/12/yoo-jae-suks-new-variety-show-im-a-man-stays-on-air/|title=Yoo Jae Suk's New Variety Show "I'm a Man" Stays on Air |work=Soompi|date=12 June 2014|accessdate=12 June 2014}}</ref>

==''I Am a Woman'' special==
''I Am a Woman'' was recorded on 12 October 2014 and subsequently broadcast on 17 October 2014 on [[KBS2]], and on 7 November 2014 on [[KBS World]]. The director of ''I Am a Man'', PD Lee Dong Hoon, supervised the production of the ''I Am a Woman'' special. The show featured an audience of a hundred women who offered feedback on the ''I Am a Man'' show from a feminine perspective. The show reused four discussion topics from the previous ''I Am a Man'' episodes.<ref>{{cite news|url=http://www.allkpop.com/article/2014/10/i-am-a-man-confirms-upcoming-i-am-a-woman-special|title='I Am a Man' confirms upcoming 'I Am a Woman' special |work=Soompi|date=7 October 2014|accessdate=8 October 2014}}</ref>

The special guests/performer for this special show was [[Beast (South Korean band)|BEAST]], a South Korean dance music group.<ref>{{cite news|url=http://www.dkpopnews.net/2014/10/b2st-to-guest-on-kbss-talkshow-im-man.html|title=B2ST to guest on KBS's talkshow 'I'm A Man' |work=Dkopnews|date=12 October 2014|accessdate=14 October 2014}}</ref>

== List of ''I Am a Man'' episodes ==
{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%; text-align:center;"
|- bgcolor="#B0C4DE" align="center"
! rowspan="2"| Episodes 
! colspan="2"| Original Broadcast Date
! rowspan="2"| <small>Theme</small><br><small>(In korean theme)
! rowspan="2"| Guests/ Performers
|- bgcolor="#B0C4DE" align="center"
| '''[[KBS World]]'''
| '''[[KBS2]]'''
|-
|Pilot Episode
|7 May
|9 April
|Culminated and came to stay & Tech Man Special<ref>{{Cite news|url=http://ent.msn.com.cn/tv/news/variety/263056.shtml|script-title=zh:刘在石新综艺《我是男人》首播引发男人共鸣|date=10 April 2014|publisher=MSN China|accessdate=9 December 2014|language=zh}}</ref><br><small>(남중 & 남고 & 공대 나온 남자 특집)</small>
|[[Yim Siwan]], [[Bae Suzy]], [[:ko:고유진|Koh Yu-Jin]]
|-
|1
|27 August
|8 August
|The only man in the world of women/ I'm the only guy at my work<br><small>(청일점 특집)</small>
|[[:ko:전인혁|Jeon In-Yuk]], [[IU (singer)|IU]]<ref>{{Cite news|url=http://www.dkpopnews.net/2014/07/iu-to-guest-on-kbs-talk-show-im-man.html|title=IU to guest on KBS' talk show 'I'm A Man'|date=28 July 2014|publisher=''Dkopnews''|accessdate=28 July 2014}}</ref><ref>{{Cite news|url=http://www.kpopstarz.com/articles/101169/20140728/singer-iu-to-be-the-first-female-guest-on-yoo-jae-suk-im-a-man.htm|title=Singer IU To Be The First Female Guest on Yoo Jae Suk's 'I'm a Man'|date=28 July 2014|publisher=Kpopstarz|accessdate=28 July 2014}}</ref>
|- 
|2
|3 September
|15 August
| What it is like to live as a tune-deaf/ Tone-deaf men who are scared of karaoke<br><small>(음치 특집)</small>
|[[Kim Yeon-woo]], [[Sistar]]<ref>{{Cite news|url=http://www.allkpop.com/article/2014/08/kim-yeon-woo-and-sistar-to-help-teach-tone-deaf-men-how-to-sing-on-upcoming-episode-of-im-a-man|title=Kim Yeon Woo and SISTAR to help teach tone-deaf men how to sing on upcoming episode of 'I'm A Man'|date=11 August 2014|publisher=''Allkpop''|accessdate=11 August 2014}}</ref><ref>{{Cite news|url=http://www.allkpop.com/article/2014/08/sistars-hyorin-and-soyu-sing-duets-with-tone-deaf-men-on-im-a-man|title=SISTAR's Hyorin and Soyu sing duets with tone-deaf men on 'I'm A Man'|date=16 August 2014|publisher=''Allkpop''|accessdate=16 August 2014}}</ref>
|- 
|3
|10 September
|22 August
| Men Who Lost More Than 10&nbsp;kg<br><small>(몸무게 10&nbsp;kg 이상 감량한 남자 특집)</small>
|[[Moon Hee-joon]], [[Girl's Day]]
|- 
|4
|17 September
|29 August
| Men Who Will Be Married Soon<br><small>(예비 신랑과 그 남자들 특집)</small> 
|[[Baek Ji-young]], [[:ko:노을 (가수)|Noel]]
|- 
|5
|24 September
|5 September
| Men from the Countryside<br><small>(상경남 특집)</small> 
| [[:ko:김영철 (희극인)|Kim Yeong-cheol]], [[Jung Eun-ji]]<ref>{{Cite news|url=http://www.allkpop.com/article/2014/08/eunji-to-appear-as-a-guest-on-an-upcoming-episode-of-im-a-man|title=Eunji to appear as a guest on an upcoming episode of 'I'm A Man'|date=25 August 2014|publisher=''Allkpop''|accessdate=25 August 2014}}</ref>
|- 
|6
|1 October
|12 September
| Man with an unusual name<br><small>(특이한 이름을 가진 남자 특집)</small> 
|[[Dong Hyun Kim]], [[:ko:유상무|Yu Sang Mu]], [[Kara (South Korean band)|Kara]]
|- 
|7
|8 October
|19 September
| Men Who Are Dating/Married To Older Women<br><small>(연상녀와 결혼(연애)중인 연하남 특집)</small><ref>{{Cite news|url=http://www.newsen.com/news_view.php?uid=201409192332517410|script-title=ko:'나는남자다' 장동민 "고등학생 때 8살 연상녀와 연애"|date=19 September 2014|publisher=''Newsen''|accessdate=20 September 2014|language=ko}}</ref><ref>{{Cite news|url=http://news.donga.com/Main/3/all/20140920/66546881/1|title=나는남자다' 최희, "연하남 만나고 싶어…고등학교만 졸업하면 OK|date=20 September 2014|publisher=''Donga''|accessdate=20 September 2014|language=ko}}</ref><ref>{{Cite news|url=http://www.ujo.co.kr/2014/09/news_38.html|script-title=ko:최희, '나는 남자다'에서 '9살 연하까지 연애 가능' 깜짝 고백 |date=20 September 2014|publisher=''Ujo''|accessdate=20 September 2014|language=ko}}</ref>
|[[:ko:최희|Choi Hee]], [[Sam Hammington]]
|- 
|8
|15 October
|26 September
|Men Who Look Young or Old<br><small>(노안 또는 동안남 특집)</small><ref>{{cite news|url=http://www.allkpop.com/article/2014/09/audience-member-calls-mc-yoo-jae-suk-out-for-coldly-rejecting-him-in-the-past|title=Audience member calls MC Yoo Jae Suk out for coldly rejecting him in the past|work=Allkpop|date=26 September 2014|accessdate=12 October 2014}}</ref>
|[[:ko:박은영 (아나운서)|Park Eun-young]], [[Son Jin-young]]
|- 
|9{{#tag:ref|Special appearance by Ahn Cheol-soo from Men from the Countryside and Pastor Ji Sangryeol from Men who Lost more than 10kg |name = I Am a Man premium guests|group = nb}}
|22 October
|3 October
|Men who look alike<br><small>(닮은꼴남 특집)</small><ref>{{Cite news|url=http://mtvdaily.asiae.co.kr/article.php?aid=1412379000775506019|script-title=ko:'나는 남자다' 유재석 도플갱어 강아지부터 조인성 닮은꼴까지 '웃음 폭탄'|date=4 October 2014|publisher=''TVDAILY''|accessdate=5 October 2014|language=ko}}</ref>
|[[Kim Tae-woo (singer)|Kim Tae-woo]], [[Lee Guk-joo]]
|- 
|10
|29 October
|10 October
|Employment and War<ref>{{cite news|url=http://www.allkpop.com/article/2014/10/yoo-jae-suk-sheds-tears-during-filming-for-i-am-a-man|title=Yoo Jae Suk sheds tears during filming for 'I Am a Man'|work=Allkpop|date=9 October 2014|accessdate=12 October 2014}}</ref><br><small>(취업준비생남 특집)</small>
|[[Kim Je-dong]], [[Hong Jin-young]]
|- 
|11{{#tag:ref|4 out of 10 (as of 10 October) topic theme was used in ''I Am a Woman'' special, which is unique name (episode 6), look-alikes (episode 9), tone deaf (episode 2), and girl-only school (pilot episode) |name = I Am a Woman topic theme|group = nb}}
|7 November
|17 October
|I Am a Woman Special show<ref>{{cite news|url=http://mwave.interest.me/enewsworld/en/article/78389/b2st-yoondujun-picks-chests-over-hips-for-girls|title=B2ST′s Yoon Du Jun Likes Girls with Nice Chests|work=Mvave|date=16 October 2014|accessdate=17 October 2014}}</ref><ref>{{cite news|url=http://www.allkpop.com/buzz/2014/10/netizens-laugh-as-an-exo-fan-sings-growl-on-tv-tone-deaf-style|title=Netizens laugh as an EXO fan sings 'Growl' on TV tone-deaf style|work=Allkpop|date=21 October 2014|accessdate=6 November 2014}}</ref><ref>{{cite news|url=http://www.allkpop.com/article/2014/10/kikwang-reveals-that-he-likes-girls-who-cant-drink|title=B2ST's Kikwang reveals that he likes girls who can't drink|work=Allkpop|date=18 October 2014|accessdate=6 November 2014}}</ref><br><small>(나는 여자다 특집)</small>
|[[Beast (South Korean band)|Beast]]
|- 
|12{{#tag:ref|Park Min Goesu (''I Am a Man'' audience on pilot episode) were invited as the single since birth |name = I Am a Man invited guests from pilot episode|group = nb}}
|14 November
|24 October
|The Single Man Special <The Single Since The Birth and The Broken-hearted Man><br> (모태솔로&이별한 남자 특집)<ref>{{Cite news|url=http://news.donga.com/Main/3/all/20141025/67428852/1|script-title=ko:'나는남자다' 신현준 "과거 여배우와 스캔들? 작품 홍보수단으로…"|date=24 October 2014|publisher=''Donga''|accessdate=25 October 2014|language=ko}}</ref><ref>{{Cite news|url=http://news.chosun.com/site/data/html_dir/2014/10/25/2014102500496.html|title=[어저께TV] '나는남자다' 유재석, 그가 무결점 1인자인 이유
|date=25 October 2014|publisher=''Chosun''|accessdate=25 October 2014|language=ko}}</ref> 
|[[Shin Hyun-joon (actor)|Shin Hyun-joon]], [[:ko:뮤지|Muzie]]
|- 
|13{{#tag:ref|Due to tight schedule, Dynamic Duo need to leave ''I Am a Man'' filming right before the secret talk starts |name = tight schedule|group = nb}}
|21 November
|31 October
| Men who love Hip-Hop and Rock<ref>{{cite news|url=http://www.kpopstarz.com/articles/130854/20141102/i-am-a-man-yoo-jae-suk-discreet-mention-of-choiza-sulli.htm|title='I Am A Man' Yoo Jae Suk, Discreet Mention of Choiza & Sulli|work=Kpopstarz|date=2 November 2014|accessdate=3 November 2014}}</ref><ref>{{cite news|url=http://www.allkpop.com/article/2014/10/yoo-jae-suk-indirectly-mentions-choizas-relationship-on-a-recent-airing-of-i-am-a-man|title=Yoo Jae Suk indirectly mentions Choiza's relationship on a recent airing of 'I Am A Man'|work=Allkpop|date=31 October 2014|accessdate=3 November 2014}}</ref><br><small>(힙합 그리고 록을 사랑하는 남자)</small>
|[[Dynamic Duo (South Korean duo)|Dynamic Duo]], [[:ko:이윤석 (희극인)|Lee Yoon-suk]], [[Crying Nut]]
|- 
|14
|28 November
|7 November
|Men who have unique jobs<br><small>(나 이런 일 하는 남자다 특집)</small>
|[[Hong Jin-ho]], [[Eun Ji-won]]
|- 
|15
|5 December
|14 November
|''I Am a Man'' special – Men and women special part 1 <Men & women who live on their own> <br><small>(남녀특집 1편 – 자취남녀 특집)</small>
|[[:ko:김나영 (방송인)|Kim Na-young]], [[Kim Je-dong]]
|- 
|16{{#tag:ref|The schedule of broadcast date on 12 December is postponed to 15 December due to [[Grand Bell Awards|The 51st Annual Daejong Film Award]] was aired by [[KBS World]]|name = The 51st Annual Daejong Film Festival|group = nb}}
|15 December
|21 November
|''I Am a Man'' special – Men and women special part 2  <nowiki><I Am a Baseball Fanatic Special></nowiki><br><small>(남녀특집 2편 – 프로야구 매니아 특집)</small>
|[[:ko:최희|Choi Hee]], [[Lee Jong-beom]], [[Kim Je-dong]], [[:ko:태미|Kyung-suk Kim (Taemi)]], Bak Ki-ryang, Lee Ji-eun, Kang Yuni
|- 
|17{{#tag:ref|This episode is rated as 19 ages above only viewers by [[KBS2]] |name = rated as 19|group = nb}}
|19 December
|28 November
|''I Am a Man'' special – Men and women special part 3 <nowiki><I Am an Alcohol Lover>  </nowiki><ref>{{cite news|url=http://www.allkpop.com/article/2014/11/yoo-jae-suk-reveals-that-hes-an-extreme-lightweight-when-it-comes-to-drinking|title=Yoo Jae Suk reveals that he's an extreme lightweight when it comes to drinking|work=Allkpop|date=29 November 2014|accessdate=9 December 2014}}</ref><br><small>(남녀특집 3편 – 주당남녀 특집)</small>
|[[Im Chang-jung]], [[Choi Yeo-jin]], [[Kim Je-dong]]
|- 
|18
|26 December
|5 December
|''I Am a Man'' special – Men and women special part 4 <Men and Women Who Were Once Innocent special><ref>{{Cite web|url = http://mwave.interest.me/enewsworld/en/article/82455/yoo-jae-suk-acts-fake-when-he-plays-with-his-kid-at-home|title = Yoo Jae Suk Acts Fake When He Plays with His Kid at Home|date = 14 December 2014|accessdate = 18 December 2014|website = Mvave|publisher = Mvave|last = Jung|first = Yeawon}}</ref><br>(남녀특집 4편 – 순진했던 남녀)
|[[Kim Je-dong]], [[Clara Lee|Clara]]
|- 
|19
|2 January 2015
|12 December
|''I Am a Man'' Special – Men and women special part 5 <Men and women who like to travel/ I Am Wanderlust!><ref>{{Cite web|url = http://www.dkpopnews.net/2014/12/kim-je-dong-reveals-yoo-jae-suks.html|title = Kim Je Dong reveals Yoo Jae Suk's skincare routine on 'I'm A Man'|date = 13 December 2014|accessdate = 18 December 2014|website = Daily Kpop News|publisher = Dkopnews|last = |first = }}</ref><br><small>(남녀특집 5편 – 역마살 남녀)</small>
|[[Kim Sook (comedian)|Kim Sook]], [[:ko:송은이|Song Eun-yi]], [[Kim Je-dong]]
|- 
|20
|9 January 2015
|19 December
|Title Match  <br><small>(</small>타이틀 매치 특집<small>)</small>
|
|}

=== Plot ===
{{hidden begin
|title = Plot summaries for the ''I Am a Man'' episodes aired on [[KBS World]]
|titlestyle = background:#D0A9F5;
}}
{|class="wikitable" style="font-size:100%;" 
|-
!Episodes!!Plot
|- align="center"
!Pilot Episode
|250 men who went to all-male schools and spent their entire lives in an all-male environment share their life experiences.
|- align="center"
!1
|147 men who grew up and lived in a female-dominated environment share their joys and agonies. 
|- align="center"
!2
|Tone-deaf men afraid of going to karaoke have their singing abilities assessed by Kim Yeonwoo, a wanna-be singer, and share what it is like to live with tone deafness.
|- align="center"
!3
|Men who lost more than 10&nbsp;kg of weight are invited to speak about weight-related issues, and discuss pros and cons of being on a diet. They are joined by a special guest, Moon Heejun. 
|- align="center"
!4
|Soon-to-be-married bachelors talk about marriage. They are joined by a female guest, Baek Jiyoung.
|- align="center"
!5
|Country boys who moved to Seoul share their thoughts on big city life. They are joined by Kim Yeongcheol, a famous comedian.
|- align="center"
!6
|Men with unusual names tell their stories. They are joined by two special guests: Yoo Sangmoo, a comedian, and Kim Donghyun, a mixed martial arts fighter.
|- align="center"
!7
|Men in relationship with an older woman tell stories they wouldn't dare to share with their significant others.
|- align="center"
!8
|Men who don't look their age tell us about the pros and cons of looking older/younger than you are.
|- align="center"
!9
|Male celebrity look-alikes share their stories. Special guests include "Gandhi" and "Obama."
|- align="center"
!10
|Men who had a hard time finding a job discuss their experiences.
|- align="center"
!11
|In this ''I Am a Woman'' special, 100 women join the ''I Am a Man'' hosts and tell their stories.
|- align="center"
!12
|In this 12th episode, 100 single men are talking about their lives being living as Single Since The Birth & The Broken-hearted Man. Guided by Muzie and Shin Hyun-joon as dating expert, they will tell ''I Am a Man'' audience on how to attract women. 
|- align="center"
!13
|men who love hip-hop and rock music talk about their love for music
|- align="center"
!14
|This week, 100 men are invited to talk about their unique jobs.
|- align="center"
!15
|In this man and women special part 1, from awkward situation between man and women turn into a hilarious situation and the best of the ''I Am a Man'' episoded
|- align="center"
!16
|In this man and women special part 2, ''I Am a Man'' hosts meet guys and girls who love Baseball
|- align="center"
!17
|In this man and women special part 3, they can't live without drinking alcohol.
|- align="center"
!18
|In this man and women special part 4, ''I Am a Man'' hosts meet man and women who were innocent in the past.
|- align="center"
!19
|In this man and women special part 5, they just not only roamed around Korea, but also all around the world.
|- align="center"
!20
|Finally, after 20 episode, ''I Am a Man'' comes to an end. It's a title match between unique names, look-alikes, tone-deaf and fun-talented people
|}
{{hidden end}}

== Ratings  ==

{| border="2" cellpadding="4" cellspacing="0" style="margin: 1em 1em 1em 0; background: #f9f9f9; border: 1px #aaa solid; border-collapse: collapse; font-size: 95%; text-align:center;"
|- bgcolor="#B0C4DE" align="center"
! rowspan="2"| Episode Number
! colspan="2"| Original Broadcast Date
! rowspan="2" | AGB Nielsen Ratings<ref name="ratings">[http://find.kbs.co.kr/web/search.php?kwd=%EB%82%98%EB%8A%94%20%EB%82%A8%EC%9E%90%EB%8B%A4&pfch=true# KBS Search result I Am a Man]{{ko icon}}</ref>
! rowspan="2"| Official YouTube Link
|- bgcolor="#B0C4DE" align="center"
| '''[[KBS2]]'''
| '''[[KBS World]]'''
|-
| Pilot Episode
| 9 April 2014
| 7 May 2014
| 4.1%
| [https://www.youtube.com/watch?v=obHU2nNEMa0]
|-
| 1
| 8 August 2014
| 27 August 2014
| 5.2%
| [https://www.youtube.com/watch?v=z4Zdp_R0m7U]
|-
| 2
| 15 August 2014
| 3 September 2014
| 4.2%
| [https://www.youtube.com/watch?v=PaaId6Hk3Z0]
|-
| 3
| 22 August 2014
| 10 September 2014
| 4.3%
| [https://www.youtube.com/watch?v=MYhD5IwTRNI]
|-
| 4
| 29 August 2014
| 17 September 2014
| 3.7%
| [https://www.youtube.com/watch?v=jGTjcztnKjo]
|-
| 5
| 5 September 2014
| 24 September 2014
| 5.0%
| [https://www.youtube.com/watch?v=QxlKLtw3c9g]
|-
| 6
| 12 September 2014
| 1 October 2014
| 5.8%
| [https://www.youtube.com/watch?v=hEBEJPx22Uk]
|-
| 7
| 19 September 2014
| 8 October 2014
| 3.6%
| [https://www.youtube.com/watch?v=7Q5lHxDlvS0]
|-
| 8
| 26 September 2014
| 15 October 2014
| 3.8%
| [https://www.youtube.com/watch?v=bxiSvfXS0yQ]
|-
| 9
| 3 October 2014
| 22 October 2014
| 5.1%
| [https://www.youtube.com/watch?v=kwnZ4ihE1yI]
|-
| 10
| 10 October 2014
| 29 October 2014
| 4.9%
| [https://www.youtube.com/watch?v=z4ngAgWZlOg]
|-
| 11
| 17 October 2014
| 7 November 2014
| {{font color|blue|6.4%}}
| [https://www.youtube.com/watch?v=QZ9cfqoUUvw]
|-
| 12
| 24 October 2014
| 14 November 2014
| 3.5%
| [https://www.youtube.com/watch?v=HlPPbaVfF6Q]
|-
| 13
| 31 October 2014
| 21 November 2014
| {{font color|red|3.5%}}
| [https://www.youtube.com/watch?v=uTDJxVB_pFY]
|-
| 14
| 7 November 2014
| 28 November 2014
| 4.9%
| [https://www.youtube.com/watch?v=k8wHwK0j4oo]
|-
| 15
| 14 November 2014
| 5 December 2014
| 5.7% 
| [https://www.youtube.com/watch?v=z4aChAfzg0c]
|-
| 16
| 21 November 2014
| 15 December 2014
| 4.3%
| [https://www.youtube.com/watch?v=BRG9-bvJs_M]
|-
| 17
| 28 November 2014
| 19 December 2014
| 5.9%
| [https://www.youtube.com/watch?v=qtTGLRD_XfQ]
|-
| 18
| 5 December 2014
| 26 December 2014
| 5.3%
| [https://www.youtube.com/watch?v=0b2tgsTiWk0]
|-
| 19
| 12 December 2014
| 2 January 2015
| 5.2%
| [https://www.youtube.com/watch?v=hz9fGTFXgLE]
|-
| 20
| 19 December 2014
| 9 January 2015
| 5.8%
| [https://www.youtube.com/watch?v=eWhXoGozT9E]
|-
|}
''All ratings are from the original episode broadcast on [[KBS2]].''

'''{{font color|blue|Blue}}''' ratings denote the highest numbers the series has garnered.<br>
'''{{font color|red|Red}}''' ratings denote the lowest numbers the series has garnered.<br>
''(-)'' denotes that episode has not yet been aired in that channel <br>
'''Note:''' Future broadcast dates are subject to change.

== Notes==
{{reflist|group=nb}}

== References ==
{{reflist}}

==External links==
*{{official website|http://www.kbs.co.kr/2tv/enter/iamaman/}} {{ko icon}}
*[https://www.youtube.com/playlist?list=PLMf7VY8La5RGROkRUs4rrbrO028-rnSoF ''I Am a Man'' KBS World playlist] at [[YouTube]]

{{I am a Man |state=expanded}}
{{KBS2 Programs}}
{{KBS Entertainment Shows |state=expanded}}

[[Category:2014 South Korean television series debuts]]
[[Category:Korean-language television programming]]
[[Category:South Korean television talk shows]]
[[Category:South Korean variety television programmes]]
[[Category:2010s South Korean television series]]