{{Infobox musical artist
| name = Simonne Jones
| image =
| caption = Jones playing Sonar Festival, Barcelona 2014
| background = solo_singer
| birth_name = Simonne Michelle Jones
| birth_date = {{Birth date and age|df=yes|1987|2|23}}
|birth_place =[[Hollywood]], [[California]], USA
| death_date =
| instrument = {{hlist|[[Singing|Vocals]]|[[guitar]]|[[Keyboard instrument|keyboards]]|[[Bass (guitar)|bass]]|[[percussion]]|[[pump organ]]|[[piano]]}}
| genre = {{hlist|[[Pop music|pop]]|[[electro pop]]|[[Alternative rock|alternative]]|[[dark pop]]}}
| occupation = {{hlist|Singer|songwriter|producer}}
| years_active = 2012–present
| label = {{hlist|[[Universal Music]]|[[Capitol Records|Capitol]]|[[Vertigo Records|Vertigo]]}}
| website = {{url|http://simonnejones.com/}}
| notable_instruments =
}}

'''Simonne Jones''' (born '''Simonne Michelle Jones''' in [[Los Angeles]], [[California]]) is a [[Record producer|producer]], singer, composer, model and visual artist based in [[Berlin]].<ref name="Lastfm">http://www.last.fm/de/music/Simonne+Jones/+wiki</ref>

== Early life ==
Simonne Jones was born and raised in [[Hollywood]], [[California]].<ref name="Engadget">{{cite web|url=https://www.engadget.com/2016/05/27/simonne-jones-gravity-interview/|title=The artist making physics and a conspiracy theory into music|publisher=}}</ref> She is of [[Cherokee]] and [[Barbadians|Barbadian]] descent.<ref name ="Lastfm"/> Her maternal great-grandfather is [[Frank Worrell|Sir Frank Mortimer Maglinne Worrell]], an accomplished West Indies cricketer and senator. She grew up playing the [[piano]] at age three, taught herself to read music by age ten and started composing music.<ref name="lenny">{{cite web|url=http://www.lennyletter.com/culture/interviews/a341/music-monday-the-science-and-magic-of-simonne-jones/|title=Music Monday: The Science and Magic of Simonne Jones|date=19 April 2016|publisher=}}</ref> As a young teen Simonne began to produce her own music while learning to become a [[multi-instrumentalist]].<ref name="Fusion">{{cite web|url=http://fusion.net/story/307211/simonne-jones-ep-gravity-interview/|title=How Simonne Jones Went From Scientist to Rising Electropop Star|publisher=}}</ref>

When Jones was 15 she left high school to homeschool herself and enrolled in local college courses.<ref name="Engadget"/> A year later, [[Elite Model Management]] signed Jones to a modeling contract at age 16 which would shape her world view and influence her work for years to come. That same year she moved across the country and began her college career at the [[University of Maryland]].<ref>{{cite web|url=http://www.lornebehrman.com/simonnejones/|title=SIMONNE JONES|publisher=}}</ref><ref>{{cite web|url=https://offtherecordwithjennryan.wordpress.com/2016/07/20/introducing-simonne-jones/|title=Introducing : Simonne Jones|first=Author Jenn|last=Ryan|date=20 July 2016|publisher=}}</ref> This would present Jones with many new resources and opportunities to pursue her scientific and artistic interests. A few highlights include:

* Awarded a research grant to implement and HIV awareness program in [[Ghana]].<ref name="huckmag">{{cite web|url=http://www.huckmagazine.com/art-and-culture/music-2/simonne-jones/|title=Simonne Jones - Bridging The Gap|date=3 February 2013|publisher=}}</ref>
* Employed by the ''Howard Hughes Medical Institute'' to conduct nuclear magnetic resonance imaging studies, and molecular cloning on mutated HIV-1 cells in an attempt to find a cure for the disease.<ref>{{cite journal|url=http://science.sciencemag.org/content/suppl/2011/10/12/334.6053.242.DC1|title=Supporting Online Material|date=23 August 2016|publisher=}}</ref>
* Debuted first exhibition as a visual artist at [[SACI|Studio Art Center International (SACI)]] in [[Florence]], [[Italy]] in 2007.
* Received her Bachelor's degree in [[Medical research|Biomedical research]], and [[Visual arts]] with honors in 2008.
* Held first solo exhibition as a painter in [[Manhattan]].
* Co-authored a research paper published in Science Magazine, the ''NMR Detection of Structures in the HIV-1 5’-Leader RNA That Regulate Genome Packaging''<ref>http://www.pubfacts.com/author/Simonne+Jones</ref>

Jones was bound for medial school and a life dedicated to research, however after being accepted into medical school she had an epiphany and moved to Berlin to pursue a career in music. [[The Guardian]] wrote, "When faced with the decision between a career in science or music, she chose both.<ref name="auto">{{cite web|url=https://www.theguardian.com/culture/2016/jul/10/bluedot-festival-frontier-jodrell-bank-jean-michel-jarre-brian-cox-underworld|title=Bluedot: exploring the festival frontier|first=Kit|last=Buchan|date=10 July 2016|publisher=|via=The Guardian}}</ref>

== Career ==

=== 2012-2015: First steps in the music industry ===
[[File:Simonne Jones at Hydrogen Festival by Dinetta.png|thumb|Simonne Jones playing Hydrogen Festival in [[Padua|Padova]], [[Italy]] 2013]]

She took her first steps as a professional musician when she was asked to perform at a [[Diesel (brand)|Diesel]] show for [[Berlin]] fashion week in 2012.<ref>{{cite web|url=http://www.bild.de/unterhaltung/musik/peaches/schoenster-alien-der-musikwelt-26044588.bild.html|title=Simonne Jones: Der schönste Alien der Musikwelt|publisher=}}</ref> Jones was taken on as a protege by Peaches, who impressed with Simonne's burgeoning talents instilled artistic autonomy by encouraging her to engineer and produce recording sessions.<ref>https://missy-magazine.de/2016/06/09/simonne-jones-kann-auch-pop/</ref> They collaborated on the song ''Free Pussy Riot'' as a political protest to the imprisonment of the [[Russians|Russian]] performance artists [[Pussy Riot]].<ref name="auto1">{{cite web|url=http://tmagazine.blogs.nytimes.com/2012/08/09/quiet-rioters-a-colorful-show-of-support-in-berlin/?_r=0|title=Quiet Rioters - A Colorful Show of Support in Berlin|first=Nadja|last=Sayej|publisher=}}</ref>

In 2013 Jones was photographed for the cover of Missy Magazine.<ref>http://missy-magazine.de/2013/02/12/ab-18-februar-die-neue-missy/</ref> That year she was accepted out of 10,000 applicants into the competitive [[Red Bull Music Academy]] in [[New York City]].<ref>{{cite web|url=http://www.redbullmusicacademy.com/about/projects/newyork2013|title=Red Bull Music Academy|publisher=}}</ref> During this time she was invited to compose and perform as a soloist with the ORSO philharmonic orchestra and choir in Freiburg Germany conducted by Wolfgang Rose.<ref>{{cite web|url=http://www.burning-music.de/2013/10/simonne-jones-interview-wenn-musikalische-welten-universum-exklusiv/|title=Simonne Jones im Interview: Wenn musikalische Welten auf das Universum treffen! - Burning Music|publisher=}}</ref> Shortly after she scored and performed in the play Jedermann directed by Bastian Kraft, premiering at the prestigious Salzburg festival, also making her acting debut playing the character Death.<ref>{{cite web|url=http://www.salzburg24.at/salzburger-festspiele-jedermann-als-pop-und-trash-im-ydp/3662406|title=Salzburger Festspiele: Jedermann" als Pop und Trash im YDP|publisher=}}</ref> Jones performed her compositions in the play as a one woman orchestra in [[China]] at Tianjin's Grand Theater, the Kurtheater Baden in [[Switzerland]], the Theater Duisburg in Duisburg Germany before the play moved to a residency at the [[Thalia Theater (Hamburg)|Thalia Theater]] in Hamburg for the next 3 years.<ref>{{cite web|url=http://www.abendblatt.de/kultur-live/article118845341/Philipp-Hochmair-der-etwas-andere-Hamburger-Jedermann.html|title=Philipp Hochmair – der etwas andere Hamburger Jedermann|first=Paul|last=Jandl|publisher=}}</ref> In 2014, she was invited to open for American rock band [[Thirty Seconds to Mars]].<ref>{{cite web|url=http://www.musicme.com/Simonne-Jones/videos/And-Jared-Leto---@piazzola-Sul-Brenta-(Pd)-14-07-13-4C494F4F3169746B557349.html|title=Vidéo-clip Simonne Jones AND JARED LETO - @PIAZZOLA SUL BRENTA (PD) 14/07/13|publisher=}}</ref>

Jones completed a six-month artist residency at Berlin's renowned art collective ''Platoon Kunsthalle'' where she constructed motion activated, [[MIDI]]-controlled, [[LED]] paintings exploring topics in physics and cosmology.<ref>{{cite web|url=http://www.platoon.org/dates/berlin-simonne-jones|title=BERLIN · SIMONNE JONES - THE SECRETS OF THE UNIVERSE|publisher=}}</ref> She engineered them using open source computer controllers with her best friend, ArbitraryY a software engineer in the aerospace industry.<ref>{{cite web|url=http://makezine.com/2016/03/03/go-behind-scenes-installing-interactive-led-art-exhibit/|title=Go Behind the Scenes of Installing an Interactive LED Art Exhibit - Make:|date=3 March 2016|publisher=}}</ref> At the time Jones was involved in DIY audio performing with self-built midi controllers, synthesizers, LED costumes and complex loop machines.<ref>{{cite web|url=http://www.klatsch-tratsch.de/2016/04/20/hier-kommt-us-newcomerin-simonne-jones-mit-gravity/273429|title=Hier kommt US-Newcomerin Simonne Jones mit "Gravity"|date=20 April 2016|publisher=}}</ref> Jones also performs as a DJ playing dark electronic music in [[nightclub]]s.<ref>{{cite web|url=https://www.youtube.com/watch?v=qGZ-Jc0MypM|title=Night of Machines @ Arena Club, Berlin|first=|last=Elektron|date=4 April 2016|publisher=|via=YouTube}}</ref>

=== 2015-present: ''Soundhunters'', ''Rub'' and ''Gravity'' ===
Jones explored music as a spiritual ritual in the [[Amazon rainforest|Brazilian Amazon]] during an immersive indigenous experience in Jean Michel Jarre's documentary Soundhunters  in 2015 with the [[Guaraní people]].<ref>{{cite web|url=http://soundhunters.arte.tv/watch/en/SoundOfLeaves|title=Soundhunters WATCH|publisher=}}</ref> She released the song "The Silver Cord" in an album curated by Jean Michel Jarre called Zoolook Revisited inspired by his 1984 Zoolook album.<ref>{{cite web|url=https://www.youtube.com/watch?v=3mdDlnswa1E|title=CONCOURS ALBUM "ZOOLOOK REVISITED"|first=|last=SOUNDHUNTERS|date=22 June 2015|publisher=|via=YouTube}}</ref>

Jones has written and produced for other artists including the song "Vaginoplasty" with Peaches and Vice Cooler for her Rub album.<ref>{{cite web|url=http://www.allmusic.com/song/vaginoplasty-mt0052330717|title=Vaginoplasty - Peaches - Song Info - AllMusic|publisher=}}</ref> She makes a cameo in the music video. Jones produced the remix "Sick in the Head" for the [[Rub (album)|Rub Remixed album]].<ref name ="pitchfork">{{cite web|url=http://pitchfork.com/news/64355-peaches-announces-rub-remix-album/|title=Peaches Announces Rub Remix Album - Pitchfork|publisher=}}</ref> Peaches also contributed a remix to Simonne's debut single as an exclusive premiere in [[Billboard|Billboard Magazine]].<ref>{{cite web|url=http://www.billboard.com/articles/news/dance/7357850/peaches-simonne-jones-gravity-remix|title=Peaches Reawakens Simonne Jones' 'Gravity' With Out-Of-This-World Verse: Exclusive|publisher=}}</ref>

Jones gave several lectures about how the integration of science and art inform her artwork including at the inaugural ''Blue Dot Festival'' at the Jodrell Bank Observatory in [[Manchester]] and the ''Make Sound Festival'' in Leicester.<ref name="auto"/>

In 2016, she used [[gravitational wave]] audio from the collision of black holes form [[LIGO]] and collaborated with [[CERN]] using representations of real time particle collision data from the large [[hadron collider]] in the song "Alchemy".<ref>http://www.popsci.com.au/make/-exclusive-listen-to-alchemy-simonne-jones-song-made-from-physics-data,435691</ref>

On 27 May 2016 Jones released the debut single and music video for "Gravity" worldwide via Universal/Capitol Records.<ref>{{cite web|url=https://www.youtube.com/watch?v=kn92fZHbWDY|title=Simonne Jones - Gravity|first=|last=simonnejonesVEVO|date=18 April 2016|publisher=|via=YouTube}}</ref>

== Musical style and influences ==
Jones produces, records and writes most of her music and lyrics alone in her studio and describes her music style as dark pop.<ref>{{cite web|url=http://flaunt.com/music/whats-yr-fridge-simmone-jones/|title=Flaunt Magazine - Music: What's In Yr Fridge, Simonne Jones?|publisher=}}</ref> Her music has experimented with elements of pop, sythpop, dream [[Pop music|pop]], [[punk rock|punk]], [[Alternative rock|alternative]], baroque pop, electronica and [[Electro music|electro]].<ref name="huckmag"/>  She uses [[Protools]], [[Logic]], [[Ableton]] and various analog synthesizers to produce.<ref>{{cite web|url=http://neuh.com/simonne-jones/|title=Simonne Jones - neuH Magazine|date=16 April 2015|publisher=}}</ref>

Fusion, who added Jones to their ''The best 25 songs of 2016'' list wrote, "Simonne uses her background in scientific theory to write electro-pop songs that lyrically reference complex concepts like relativity and gravity while sounding both otherworldly and like a hymn you’ve always known."<ref name="Fusion"/>

[[Lena Dunham|Lena Dunham's]] newsletter Lenny Letter described, "Jones, who is a lover of science and a former biomedical researcher, cites her interest in concepts like the theory of relativity and other natural phenomena in her music as being fueled by her fascination with the "unknown mysteries of the universe." <ref name="lenny"/>

Jones describes a production technique in Popular Science, "In Spooky Action I used sounds from a UK researcher who translates star pulses into audio waves to better understand them [...] I created a sound library where I can play the stars on a keyboard."<ref>{{cite web|url=http://www.popsci.com/singer-simonne-jones-fuses-pop-music-and-quantum-physics|title=Singer Simonne Jones Fuses Pop Music and Quantum Physics|publisher=}}</ref>

The music production software company [[Native Instruments]] released a Simonne Jones drum kit in the virtual instrument [[plug-in (computing)|plug-in]] called Battery of sounds specific to her production style.<ref>{{cite web|url=https://www.native-instruments.com/de/specials/soundhunters/|title=SOUNDHUNTERS|publisher=}}</ref>

Her style has been compared by industry critics and fans to [[PJ Harvey]], [[Deutsch Amerikanische Freundschaft]], and [[Depeche Mode]].<ref>{{cite web|url=http://www.redbull.com/en/music/stories/1331632286782/simonne-jones|title=Simonne Jones: The renaissance woman|first=Florian|last=Obkircher|publisher=}}</ref>

==Discography==

===Singles===

====As a lead artist====
{| class="wikitable"
|-
! Title !! Year !! Record Label !! Writer(s) !! Length
|-
| Gravity<ref>{{cite web|url=http://www.artconnect.com/projects/gravity-music-video|title=Gravity Music video|publisher=}}</ref> || 2016  || Vertigo/Capitol (Universal) || [[Kid Harpoon|Thomas Edward Percy Hull]], Simonne Jones || 3:21
|}

====As a featured artist and writer====
{| class="wikitable"
|-
! Title !! Year !! Record Label !! Writer(s) !! Length
|-
| Vaginoplasty<ref>{{cite web|url=http://www.cosmopolitan.com/entertainment/music/q-and-a/a46893/peaches-rub-interview/|title=Peaches on Her New Album and Her Big Dreams: "I Will Ride a Hot Dog Through the Air One Day"|date=29 September 2015|publisher=}}</ref> || 2016  || I U She Music/Indigo || [[Peaches (musician)|Peaches]], Vice Cooler, Simonne Jones || 4:27
|-
| Free Pussy Riot<ref name="auto1"/><ref>{{cite web|url=http://www.theglobeandmail.com/arts/music/canadian-artist-peaches-to-release-free-pussy-riot-video/article4472247/|title=Canadian artist Peaches to release 'Free Pussy Riot' video|publisher=}}</ref> || 2012  || I U She Music/Indigo || [[Peaches (musician)|Peaches]], Simonne Jones || 2:47
|}

====As a producer====
{| class="wikitable"
|-
! Title !! Year !! Record Label !! Producer !! Album !! Length
|-
| Silver Cord<ref>{{cite web|url=https://www.beatport.com/track/the-silver-cord-original-mix/7758737|title=The Silver Cord (Original Mix) by Simonne Jones on Beatport|publisher=}}</ref> || 2015  || Soundhunters || Simonne Jones || Zoolook Revisited || 3:35
|-
| Sick In The Head (Remix)<ref name ="pitchfork"/> || 2016 || I U She Music || Simonne Jones || Rub Remixed || 3:35
|}

==Tours==

===Headlining===
*Sowo tour (Italy, 2013)<ref>{{cite web|url=http://www.bandsintown.com/SimonneJones/past_events?page=4|title=Simonne Jones|publisher=}}</ref>
*[https://www.orso.co/ ORSO] philharmonic (Germany 2013)<ref>{{cite web|url=https://www.discogs.com/artist/3806171-Simonne-Jones?filter_anv=0&subtype=Vocals&type=Credits|title=Simonne Jones|publisher=}}</ref>
*Untitled tour (Italy, 2015)<ref>{{cite web|url=http://www.cronachefermane.it/2016/08/08/musica-e-solidarieta-parte-oggi-il-tangram-festival/13680/|title=Musica e solidarietà, parte oggi il Tangram Festival|date=8 August 2016|publisher=}}</ref>
*Thalia Theater tour (China, Germany, Austria, Switzerland, 2014-2016)<ref>{{cite web|url=http://www.rp-online.de/nrw/staedte/duisburg/das-leben-ist-live-der-tod-aber-auch-aid-1.4851966|title=Duisburg: Das Leben ist live, der Tod aber auch|first=Olaf|last=Reifegerste|publisher=}}</ref>

===Supporting===
*[[Poliça]] (2012)
*[[Thirty Seconds to Mars]], [[Love, Lust, Faith and Dreams Tour]] (2013)<ref>{{cite web|url=http://www.rockol.it/gallerie-fotografiche/1990/14-luglio-2013-hydrogen-festival-anfiteatro-camerini-piazzola-sul/233064|title=√ 14 luglio 2013 - Hydrogen Festival - Anfiteatro Camerini - Piazzola sul Brenta (Pd) - Simonne Jones in concerto - Rockol|first=Rockol.com|last=s.r.l.|publisher=}}</ref>
*[[Peaches (musician)|Peaches]], Rub Tour (2016)<ref>{{cite web|url=http://www.berliner-zeitung.de/kultur/musik/peaches-live-in-berlin-rubbeln-bis-die-schamlippen-brennen-23275082|title=Peaches live in Berlin: Rubbeln bis die Schamlippen brennen|first=Maike|last=Schultz|publisher=}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* [http://www.simonnejones.com simonnejones.com]

{{Authority control}}

{{DEFAULTSORT:Jones, Simonne}}
[[Category:1987 births]]
[[Category:Living people]]
[[Category:American female composers]]
[[Category:American female models]]
[[Category:American female singers]]
[[Category:American record producers]]
[[Category:American painters]]