{{featured article}}

{{Use dmy dates|date=February 2014}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = Confusion
| Type       = studio
| Artist     = [[Fela Kuti|Fela Ransome-Kuti]] and the Africa 70
| Cover      = Confusion - Fela Kuti.jpg
| Alt        = 
| Released   = 1975
| Recorded   = 1974
| Genre      = [[Afrobeat]]
| Length     = {{Duration|m=25|s=36}}
| Label      = [[EMI]]
| Producer   = Fela Ransome-Kuti
| Chronology = [[Fela Kuti]]
| Last album = ''[[Gentleman (album)|Gentleman]]''<br/>(1973)
| This album = '''''Confusion'''''<br/>(1975)
| Next album = ''Shakara''<br/>(1975)
}}
'''''Confusion''''' is a 1975 [[studio album]] by Nigerian [[Afrobeat]] musician [[Fela Kuti]] and his Africa 70 band. It was arranged, composed, and produced by Kuti, who recorded the album after choosing to emphasize his African heritage and nationalism in his music. ''Confusion'' is a commentary on the confused state of [[post-colonial]] [[Lagos]] and its lack of [[infrastructure]] and proper leadership at the time. Kuti's [[West African Pidgin English|pidgin English]] lyrics depict difficult conditions in the city, including a frenetic, multilingual [[street market|trading market]] and inextricable traffic jams in Lagos' major intersections.

''Confusion'' is a one-song Afrobeat album that begins with an entirely instrumental first half, which features [[free form jazz|free form]] interplay between Kuti's electric piano and drummer [[Tony Allen (musician)|Tony Allen]]. It leads to an extended mid-tempo section with Allen's [[polyrhythm]]s and tenor saxophone by Kuti, who subsequently delivers [[call and response|call-and-response]] vocal passages. In reviews since the record's release by [[EMI]], the album was praised by music critics, who found it exemplary of Kuti's Afrobeat style and recommended it as a highlight from his extensive catalog. In both 2000 and 2010, ''Confusion'' was [[reissue]]d and bundled with Kuti's 1973 ''[[Gentleman (album)|Gentleman]]'' album.

== Background ==
[[File:Fela Kuti (cropped).jpg|thumb|right|Kuti, photographed in 1970]]
After becoming dissatisfied with studying European composers at the [[Trinity Laban Conservatoire of Music and Dance|Trinity College of Music]], Fela Kuti formed his first band Koola Lobitos in 1961 and quickly became a popular act in the [[London]] club scene. He returned to his native [[Nigeria]] in 1963 and formed another band that played a rhythmic fusion of traditional [[highlife]] music and [[jazz]]. Kuti dubbed his hybrid style "[[Afrobeat]]", which served in part as his critique of African performers who he felt had forsaken their native musical roots for American popular music trends. In 1969, he toured with his band in [[Los Angeles]] and was introduced by a friend to the writings of [[black nationalism|Black nationalist]] and [[Afrocentrism|Afrocentrist]] proponents such as [[Malcolm X]] and [[Eldridge Cleaver]]. Inspired by what he had read, Kuti decided to write more political and critical music, and changed the name of his band to the Nigeria 70 and later the Africa 70.{{sfn|Dougan|n.d.}}

During the 1970s, Kuti began to emphasize his identification with Africa and its culture in his music and opposed the [[Postcolonialism|colonial mentality]] of identification with [[Western culture|Western powers]] such as the United States and the United Kingdom. His albums during this period expressed aspects of his ideology such as his African heritage and nationalism. On his 1973 song "Eko lie", Kuti declared that [[Lagos]] was his home rather than London or [[New York City]], where he had performed earlier in his career.{{sfn|Fairfax|1993|p=248}} Lagos and its landmark events became common themes in his songs.{{sfn|Olaniyan|2004|p=92}} He revisited the theme on ''Confusion'' in 1974 to acknowledge that he identifies with the city despite its problems.{{sfn|Fairfax|1993|p=249}}

== Composition ==
{{Listen|pos = left
 |filename     = Fela Kuti - Confusion sample.ogg
 |title        = "Confusion"
 |description  = A 30-second sample of the [[bridge (music)|bridge]] during the song's instrumental half
}}
''Confusion'' is a one-song album with a duration of 25 minutes and 36 seconds.<ref>{{harvnb|Greenberg|n.d.}}; {{harvnb|Christgau|2000}}</ref> The album's first side is entirely instrumental.{{sfn|Falola|Salm|2005|p=329}} It begins with a free-form introduction, which musically depicts the disorienting impact of Lagos' problems.<ref>{{harvnb|Christgau|2000}}; {{harvnb|Fairfax|1993|p=249}}</ref> Although he occasionally used amplified instruments, Kuti rarely employed elaborate electronic effects and instead relied on more natural sounds.{{sfn|Fairfax|1993|p=249}} Kuti, who plays [[electric piano]], and drummer [[Tony Allen (musician)|Tony Allen]] start the introduction out of tempo and exhibit abstract musical techniques, including dramatic [[free jazz]] interplay between their instruments.<ref>{{harvnb|Fairfax|1993|p=249}}; {{harvnb|Samuelson|n.d.}}</ref> Although he was known for his critical background in [[classical music|classical]] study, Kuti allowed Allen to [[musical improvisation|improvise]] in the {{proper name|Africa 70}}'s rhythm section and viewed him as a drummer with the mind of a composer, or "one who composes on the spot".{{sfn|Smith|2000}}

The introduction's keyboard [[fantasia (music)|fantasia]] gradually opens the band's languid, expansive interplay.{{sfn|Matos|2010}} The song's [[groove (music)|groove]] is established with the introduction of a bass guitar [[ostinato]] at 4:50. Rhythm and tenor guitars and a horn section are played in the next two minutes.{{sfn|May|2010}} During the [[bridge (music)|bridge]], Allen's drumming intensifies within the song's seven-minute mark, which leads to a fully developed Afrobeat section; Afrobeat is a type of loose [[funk]] music embellished with African [[syncopation]], [[rhythm and blues|R&B]]-styled horn instrumentation, and improvisatory [[solo (music)|solos]].<ref>{{harvnb|Shapiro|2009|p=99}}; {{harvnb|Moon|2008|p=436}}.</ref> ''Confusion''{{'}}s extended mid-tempo section has complex [[arrangement]]s of danceable grooves, multiple solos, and Allen's [[polyrhythm]]s.<ref>{{harvnb|Samuelson|n.d.}}; {{harvnb|Christgau|2000}}</ref> In this section, Kuti plays [[tenor saxophone]], which he had learned after the {{proper name|Africa 70}}'s original tenor player, [[Igo Chico]], left in 1973.{{sfn|Shapiro|2009|p=99}} The music is complemented by his outspoken [[call and response|call-and-response]] vocal passages.{{sfn|Brunner|2000}} The song's final 10 minutes comprise raucous trumpet, tenor saxophone solos, and Kuti's lead vocals.{{sfn|May|2010}} Its closing section revisits the abstract interplay of the introduction.{{sfn|Fairfax|1993|p=249}}

== Themes ==
[[File:Market in Lagos, Nigeria.jpg|right|thumb|Kuti describes a frenetic trading market in his portrayal of [[Lagos]] (local market pictured in 2005).]]

''Confusion'' is a commentary on the confused state of post-colonial, urban Nigeria, particularly Lagos, and its lack of [[infrastructure]] and proper leadership at the time.{{sfn|Samuelson|n.d.}} Kuti's lyrics depict the complicated, frenetic, and multilingual market of the [[Ojuelegba, Lagos|Ojuelegba]] crossroad, and in doing so addresses what Nigerian historian [[Toyin Falola]] described as the "infrastructural nightmare of Lagos and the continued [[hegemony]] of the West in all aspects of African life".{{sfn|Falola|Salm|2005|p=328}} According to ''[[Rough Guides|The Rough Guide to World Music]]'' (2006), the album uses a "hectic crossroads in Lagos ... as a metaphor to explore the problems of an entire corrupt nation".{{sfn|Broughton|Ellingham|Lusk|2006|p=302}} 

In the opening lines, Kuti comments on his [[social reality]]: "When we talk say confusion / Everything out of control".{{sfn|Ambrose|2001|p=26}} His lyrics decry what he viewed as the colonial mindset of some Africans and employ [[West African Pidgin English|pidgin English]], which was the [[lingua franca]] of most people in English-speaking West Africa; he sings the phrase "pafuka", which means "all over" or "finished", and the interjection "o" to add emphasis.<ref>{{harvnb|Moon|2008|p=436}}; {{harvnb|Fairfax|1993|pp=223, 433}}</ref> Kuti makes reference to three [[dialect]]s and currencies that make trading in urban Nigeria difficult:{{sfn|Samuelson|n.d.}} "Dem be three men wey sell for roadside-o / Dem three speak different language-o / Dem speak Lagos, [[Accra]], and [[Conakry]] / One white man come pay them money-o / He pay them for [[Pound (currency)|pounds]], [[dollar]]s and [[French franc|French money]]-o / For the thing wey he go buy from them / He remain for them to share am-o / Me I say, na confusion be that-o / He go say he pafuka o."{{sfn|Falola|Salm|2005|p=328}} ''[[African Arts (journal)|African Arts]]'' journalist E.J. Collins interpreted the latter verse as a reference to the protracted nature of [[Financial transaction|transactions]] in Lagos.{{sfn|Collins|1977|p=60}}

Kuti uses ironic humor to express pride in Nigerians' ability to drive and work within difficult conditions: "Before-before Lagos traffic na special, eh / Number one special all over the world / You go get [[Ph.D.]] for driving for Lagos, eh / You go get [[Master of Arts|M.A.]] for driving for Lagos, eh / You go get [[M.Sc.]] for driving for Lagos, oh / For me for me I like am like that, eh / Ah-ha-ha-ha, na my country – why not? / For me for me I like am like that, eh."{{sfn|Fairfax|1993|p=251}} He subsequently describes major Lagosian intersections, including Ojuelegba, [[Surulere]], and Ogogoro Centre, which lack the supervision of a [[traffic officer]]. They are characterized by aggressive, temperamental drivers, who force their vehicles into inextricable [[traffic jam]]s: "For Ojuelegba, moto dey come from south / Moto dey come from north / Moto dey come from east / Moto dey come from west / And policeman no dey for centre / Na confusion be that-i o / He go say he pafuka o."{{sfn|Fairfax|1993|p=251}}

== Release and reception ==
''Confusion'' was first released in Nigeria in 1975 by [[EMI]].{{sfn|Veal|2000|p=296}} It was [[reissue]]d in 1984 by [[EMI Records]].{{sfn|Anon.|1984|p=47}} In a retrospective review for [[AllMusic]], Sam Samuelson gave the record five stars and called it "a highly recommended 25-minute Afro-beat epic". He said that it shows Kuti and his band at the peak of their instrumental skills and vague jeers, which he felt became more explicit and intense on 1977's ''[[Zombie (album)|Zombie]]''. Samuelson found ''Confusion'' to be exemplary of Kuti's "genius" formula, in which he startles musically enraptured listeners with his commentary.{{sfn|Samuelson|n.d.}} Music journalist [[Peter Shapiro (journalist)|Peter Shapiro]] called it a lyrical masterpiece and said that the bridge following the song's "cosmic" introduction is "pretty much the pinnacle of [[Afro-futurism]]".{{sfn|Shapiro|2009|p=99}} [[Nic Harcourt]] recommended ''Confusion'' as a starting point for new listeners of Kuti's music.{{sfn|Harcourt|2005|p=13}}

In 2000, [[MCA Records]] reissued and bundled ''Confusion'' with Kuti's 1973 album ''[[Gentleman (album)|Gentleman]]''.{{sfn|Samuelson|n.d.}} It was the last installment in a 10-CD, 20-album reissue project for Kuti.{{sfn|Brunner|2000}} Rob Brunner of ''[[Entertainment Weekly]]'' gave the album's reissue an "A" and viewed it as one of Kuti's best works, while  Derrick A. Smith from ''[[All About Jazz]]'' cited ''Confusion'' as one of his "best statements on any instrument".<ref>{{harvnb|Brunner|2000}}; {{harvnb|Smith|2000}}</ref> In a four-star review, ''[[Down Beat]]'' praised the musician's combination of "raw energy and sophistication", while saying that the record sounded just as remarkable as when it was first released.{{sfn|Anon.|2001|p=76}} [[Robert Christgau]] gave the two-album reissue an "A–" in his review for ''[[The Village Voice]]'', calling ''Confusion'' "one Fela song/track/album it would be a waste to edit ... the proof of {{proper name|Africa 70}}'s presumptive funk."{{sfn|Christgau|2000}} For the annual [[Pazz & Jop]] critics poll, he ranked the reissue number 80 on his "dean's list" of 2000's best albums.{{sfn|Christgau|2001}}

In 2005, ''[[New Nation]]'' ranked ''Confusion'' 91st on their list of the "Top 100 Best Albums by Black Artists".{{sfn|Anon.|2005}} In his 2008 book ''1,000 Recordings to Hear Before You Die'', music journalist Tom Moon wrote that it is both one of Kuti's best albums and "a demonstration of just how rousing Afro-Beat's deftly interlocked rhythms can be."{{sfn|Moon|2008|p=436}} In 2010, the album was bundled again with ''Gentleman'' by [[Knitting Factory Records]] as a part of their extensive reissue of Kuti's 45-album discography. Michaelangelo Matos of ''[[Paste (magazine)|Paste]]'' magazine gave it a score of "9.3/10" and cited it as the essential release in both the reissue and Kuti's catalog: "an oasis in a sandpaper-like catalog."{{sfn|Matos|2010}}

== Track listing ==
All songs were arranged, composed, and produced by [[Fela Kuti|Fela Ransome-Kuti]].{{sfn|Anon.|1975}}

{| style="width:50%;"
|-
|  style="width:50%; vertical-align:top;"|
{{Track listing
| headline = Side one
| title1 = Confusion Pt. I
| length1 = 14:08
}}
{{Track listing
| headline = Side two
| title1 = Confusion Pt. II
| length1 = 11:28
}}
|}

* The album was released as a single track on its subsequent CD reissue.{{sfn|May|2010}}

== Personnel ==
Credits are adapted from the album's liner notes.{{sfn|Anon.|1975}}

* James Abayomi – [[drum stick|sticks]]
* [[Tony Allen (musician)|Tony Allen]] – lead drums, solo drums
* Africa 70 – band
* Lekan Animashaun – [[baritone saxophone]] 
* George Mark Bruce – [[bass guitar]]
* Segun Edo – [[tenor guitar]]
* Henry Kofi – first [[conga]]
* Daniel Koranteg – second conga
* Tony Njoku – trumpet
* Emmanuel Odenusi – [[audio engineer|engineering]], [[audio mixing|mixing]]
* Isaac Olaleye – [[maracas]]
* Remi Olowookere – graphics
* [[Fela Kuti|Fela Ransome-Kuti]] – arrangement, [[tenor saxophone]], piano, production, vocals
* Tutu Shoronmu – [[rhythm guitar]]

== See also ==
{{portal|Lagos|R&B and Soul Music}}
* [[1970s in music]]
* [[Fela Kuti discography]]
* [[Ojuelegba, Lagos]]

== References ==
{{reflist|20em}}

== Bibliography ==
{{refbegin|30em}}
* {{cite journal|ref=harv|last=Ambrose|first=Robert|year=2001|title=Confusion and Courage|journal=The Beat|volume=20|location=Los Angeles}}
* {{cite AV media notes|ref=harv|author=Anon.|year=1975|title=Confusion|others=[[Fela Kuti|Fela Ransome-Kuti]] and the Africa 70|type=[[gatefold LP]]|publisher=[[EMI Records]]|location=Nigeria|id=NEMI (LP) 004}}
* {{cite journal|ref=harv|author=Anon.|year=1984|title=Afro Hot 20|journal=[[Blues & Soul]]|issue=419|location=Croydon}}
* {{cite journal|ref=harv|author=Anon.|year=2001|issue=April|title=Review: Confusion/Gentleman|journal=[[Down Beat]]|location=Elmhurst}}
* {{cite news|ref=harv|author=Anon.|newspaper=[[New Nation]]|title=Top 100 Best Albums by Black Artists|year=2005|location=London}}
* {{cite book|ref=harv|last1=Broughton|first1=Simon|last2=Ellingham|first2=Mark|last3=Lusk|first3=Jon|year=2006|title=The Rough Guide to World Music: Africa & Middle East|publisher=[[Rough Guides]]|edition=3rd|isbn=1843535513}}
* {{cite journal|ref=harv |last=Brunner |first=Rob |date=2 June 2000 |issue=543 |url=http://www.ew.com/ew/article/0,,20233165,00.html |title=Confusion/Gentleman and Stalemate/Fear Not for Man Review |journal=[[Entertainment Weekly]] |location=New York |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EKGj59Vn?url=http://www.ew.com/ew/article/0,,20233165,00.html |archivedate=10 February 2013 |deadurl=no |df=dmy }}
* {{cite news|ref=harv |last=Christgau |first=Robert |authorlink=Robert Christgau |issue=20 June |year=2000 |url=http://www.villagevoice.com/2000-06-20/music/shuffering-and-shmiling/full/ |title=Shuffering and Shmiling |newspaper=[[The Village Voice]] |location=New York |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EKFe8URg?url=http://www.villagevoice.com/2000-06-20/music/shuffering-and-shmiling/full/ |archivedate=10 February 2013 |deadurl=no |df=dmy }}
* {{cite news|ref=harv |last=Christgau |first=Robert |issue=February |year=2001 |url=http://www.robertchristgau.com/xg/pnj/deans00.php |title=Pazz & Jop 2000: Dean's List |newspaper=The Village Voice |location=New York |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EKG6jbKl?url=http://www.robertchristgau.com/xg/pnj/deans00.php |archivedate=10 February 2013 |deadurl=no |df=dmy }}
* {{cite journal|ref=harv|last=Collins|first=E.J.|journal=[[African Arts (journal)|African Arts]]|location=Los Angeles|page=60|volume=10|issue=2|date=January 1977|title=Post-War Popular Band Music in West Africa}}
* {{cite web|ref=harv |last=Dougan |first=John |year=n.d. |url=http://www.allmusic.com/artist/fela-kuti-mn0000138833 |title=Fela Kuti |publisher=[[Allmusic]]. [[Rovi Corporation]] |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6I6aO9q8n?url=http://www.allmusic.com/artist/fela-kuti-mn0000138833 |archivedate=14 July 2013 |deadurl=no |df=dmy }}
* {{cite book|ref=harv|last=Fairfax|first=Frank Thurmond|year=1993|title=Fela, the Afrobeat King: Popular Music and Cultural Revitalization in West Africa|publisher=[[University of Michigan]]|oclc=713128918}}
* {{cite book|ref=harv|last1=Falola|first1=Toyin|authorlink1=Toyin Falola|last2=Salm|first2=Steven J.|year=2005|title=Urbanization and African Cultures|publisher=Carolina Academic Press|isbn=0890895589}}
* {{cite web|ref=harv |last=Greenberg |first=Adam |year=n.d. |url=http://www.allmusic.com/album/gentleman-confusion-mw0000058406 |title=Gentleman/Confusion – Fela Kuti, Fela Ransome-Kuti and the Africa '70 : Songs, Reviews, Credits, Awards |publisher=Allmusic. Rovi Corporation |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EfSPvdHa?url=http://www.allmusic.com/album/gentleman-confusion-mw0000058406 |archivedate=24 February 2013 |deadurl=no |df=dmy }}
* {{cite book|ref=harv|last=Harcourt|first=Nic|authorlink=Nic Harcourt|year=2005|title=Music Lust: Recommended Listening for Every Mood, Moment, and Reason|publisher=[[Sasquatch Books]]|isbn=1570614377}}
* {{cite web|ref=harv |last=May |first=Chris |issue=11 March |year=2010 |url=http://www.allaboutjazz.com/php/article.php?id=35856#.UXXSnCrD_Dc |title=Part 8 – Knitting Factory rolls out Fela Kuti reissue program (continued) |work=[[All About Jazz]] |accessdate=20 November 2012 |archiveurl=https://web.archive.org/web/20121023213538/http://www.allaboutjazz.com/php/article.php?id=35856 |archivedate=23 October 2012 |deadurl=no |df=dmy }}
* {{cite journal|ref=harv |last=Matos |first=Michaelangelo |issue=16 February |year=2010 |url=http://www.pastemagazine.com/articles/2010/02/roc-a-fela.html |title=Fela Kuti: Knitting Factory Reissues |journal=[[Paste (magazine)|Paste]] |location=Decatur |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EfRxgGM7?url=http://www.pastemagazine.com/articles/2010/02/roc-a-fela.html |archivedate=24 February 2013 |deadurl=no |df=dmy }}
* {{cite book|ref=harv|last=Moon|first=Tom|year=2008|title=1,000 Recordings to Hear Before You Die|publisher=[[Workman Publishing]]|isbn=0761153853}}
* {{cite book|ref=harv|last=Olaniyan|first=Tejumola|year=2004|title=Arrest the Music!: Fela and His Rebel Art and Politics|publisher=[[Indiana University Press]]|isbn=0253110343}}
* {{cite web|ref=harv |last=Samuelson |first=Sam |year=n.d. |url=http://www.allmusic.com/album/confusion-mw0001068890 |title=Confusion – Fela Kuti : Songs, Reviews, Credits, Awards |publisher=Allmusic. Rovi Corporation |accessdate=20 November 2012 |archiveurl=http://www.webcitation.org/6EKGZBYXf?url=http://www.allmusic.com/album/confusion-mw0001068890 |archivedate=10 February 2013 |deadurl=no |df=dmy }}
* {{cite book|ref=harv|last=Shapiro|first=Peter|authorlink=Peter Shapiro (journalist)|year=2009|editor-last=Young|editor-first=Rob|editor-link=Rob Young (writer)|chapter=Fela Kuti|title=The Wire Primers: A Guide to Modern Music|publisher=[[Verso Books]]|isbn=1844674274}}
* {{cite web|ref=harv |last=Smith |first=Derrick A. |issue=1 June |year=2000 |url=http://www.allaboutjazz.com/php/article.php?id=5744 |title=Tony Allen: Black Voices |work=All About Jazz |accessdate=20 November 2012 |archiveurl=https://web.archive.org/web/20121104113801/http://www.allaboutjazz.com/php/article.php?id=5744 |archivedate=4 November 2012 |deadurl=no |df=dmy }}
* {{cite book|ref=harv|last=Veal|first=Michael E.|title=Fela:The Life & Times of an African Musical Icon|publisher=[[Temple University Press]]|year=2000|isbn=1566397650}}
{{refend}}

== External links ==
* [http://www.acclaimedmusic.net/Current/A2688.htm ''Confusion''] at [[Acclaimed Music]] (list of accolades)
* {{Discogs master|master=237221|name=Confusion|type=album}}
* {{Discogs master|master=150517|name=Confusion'' / ''Gentleman|type=album}}

{{Fela Kuti}}

[[Category:1975 albums]]
[[Category:EMI Records albums]]
[[Category:Fela Kuti albums]]
[[Category:Afrobeat albums]]
[[Category:English-language albums]]
[[Category:Music about Lagos]]