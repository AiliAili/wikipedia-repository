{{Infobox journal
| title = Southern Humanities Review
| cover = 
| editors = Anton DiSclafani, Rose McLarney, Aaron Alford, Andrew Malan Milward
| discipline = [[Literature]]
| abbreviation = SHR
| publisher = [[Auburn University]]
| country = United States
| frequency = Quarterly
| history = 1967-present
| openaccess = 
| impact = 
| impact-year = 
| website = http://www.southernhumanitiesreview.com
| link1 = 
| link1-name =
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 01643471
| LCCN = sf77000193
| CODEN = 
| ISSN = 0038-4186
| eISSN = 
}}
'''''Southern Humanities Review''''' is a quarterly [[literary magazine|literary journal]] published by [[Auburn University]]. The current masthead consists of Anton DiScalfani and Rose McLarney (Co-Editors), Aaron Alford (Managing Editor), Andrew Malan Milward (Fiction Editor). The journal publishes fiction, poetry, and creative nonfiction. It was established in 1967 as the official organ of the Southern Humanities Council, with which it remains affiliated.

Work published in ''SHR'' is considered for Best American Essays, The Art of the Essay, Best American Poetry, Best American Short Stories, Prize Stories: O. Henry Awards, and the [[Pushcart Prize]].

== The Theodore C. Hoepfner Literary Awards ==
The Theodore C. Hoepfner Literary Awards were established to honor Theodore Christian Hoepfner, a colorful scholar in the Auburn University Department of English from 1941 until his death in 1966. Each year, the editors of ''Southern Humanities Review'' bestow the awards to an outstanding essay, story, and poem published in the previous volume. Writers hoping to have their work considered for the Hoepfner Awards need only to have their work accepted for publication in ''SHR''. The awards are announced in the first issue of each volume.

== The Auburn Witness Poetry Prize Honoring [[Jake Adam York]] ==
Founded in 2014, ''SHR'''s annual poetry contest honors the memory and legacy of the late poet [[Jake Adam York]], an alumnus of Auburn University.

== Past Editors ==
<u>Founding Editors</u><br>• Norman A. Brittin<br>• Eugene Current-Garcia<br>• Taylor D. Littleton


• Barbara A. Mowat, 1979-1983<br>• David K. Jeffrey, 1979-1983<br>• Patrick D. Morrow, 1983-1984<br>• James P. Hammersmith, 1983-1984<br>• Thomas L. Wright, 1985-1991<br>• [[R. T. Smith]], 1992-1995<br>• Virginia Kouidis, 1995-2009<br>• Dan Latimer, 1985-2012<br>• [[Skip Horack]], 2012-2014<br>• Chantel Acevedo, 2009-2015<br>• Keetje Kuipers, 2012-2016


• Karen Beckwith, Managing Editor, 1988-2014

==See also==
* [[List of literary magazines]]

==External links==
* {{Official website|http://www.southernhumanitiesreview.com}}

{{Auburn University}}

[[Category:American literary magazines]]
[[Category:American quarterly magazines]]
[[Category:Auburn University]]
[[Category:Magazines established in 1967]]
[[Category:Poetry literary magazines]]
[[Category:Magazines published in Alabama]]