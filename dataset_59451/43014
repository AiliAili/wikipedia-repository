<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Ro.43
  |image = Ro43Idro.jpg
  |caption = Ro.43 seaplane
}}{{Infobox Aircraft Type
  |type = Reconnaissance
  |manufacturer = [[IMAM]]
  |designer = 
  |first flight = 19 November 1934
  |introduced = [[1935 in aviation|1935]]
  |retired = 
  |number built = 193<ref>[http://www.alieuomini.it/catalogo/dettaglio_catalogo/imam_ro,44.html IMAM RO.43]</ref>
  |status =
  |unit cost =
  |variants with their own articles = 
  |primary user = [[Regia Marina]]
  |more users = 
}}
|}

The '''IMAM Ro.43''' was an Italian reconnaissance single float [[seaplane]], serving in the [[Regia Marina]] between 1935 and 1943. Although produced in quantity, it proved never to be suitable for its intended role as a spotter plane for warships, and although 105 remained in service when Italy entered World War II, they were already obsolete.

==Design and development==
The '''Ro.43''' was designed to meet a 1933 requirement by the ''Regia Marina'' (the Italian navy) for a [[Aircraft catapult|catapult]]-launched reconnaissance aircraft to equip the Maritime Reconnaissance Squadrons operating from its ships.  The specification called for a speed of 240&nbsp;km/h (149&nbsp;mph), with a range of 600&nbsp;km (370&nbsp;mi) or an endurance of 5.5 hours. Other contenders were the [[Piaggio P.18]] and [[Piaggio P.20|P.20]], [[CMASA MF.10]], [[CANT Z.504]] and [[Macchi C.76]].

Derived from the [[IMAM Ro.37|Ro.37]] ''Lince'' reconnaissance aircraft,<ref name="Angelucci 323">Angelucci 1981, p.323</ref> by the same designer, the Ro.43 first flew in 1934. The plane was built of steel tubes and wood covered by soft [[alloy]] and fabric.<ref>[http://www.probertencyclopaedia.com/cgi-bin/res.pl?keyword=Meridionali+Ro.43&offset=0 Meridionali RO.43]</ref> It was a two-seat [[biplane]] with folding [[Gull wing|gull]] upper and inverse gull lower wings,<ref>{{cite web|url=http://worldatwar.net/chandelle/v4/v4n1-2/ro37.html |title=Mussolini's Maid of All Work: the IMAM Ro 37 and Its Derivatives |accessdate=2007-11-19 |work= |archiveurl=https://web.archive.org/web/20070927033750/http://worldatwar.net/chandelle/v4/v4n1-2/ro37.html |archivedate=2007-09-27 |deadurl=yes |df= }}</ref> lightly armed and capable of around 300&nbsp;km/h (185&nbsp;mph) and over 1,000&nbsp;km (620&nbsp;mi) range. This performance more than met the requirements of the specification, and so it was declared the winner.

Despite this, the Ro.43 had serious problems. Its lightweight structure meant that it was too delicate for [[buoyancy]] at sea, and it had poor sea-handling qualities. These problems meant that when it was launched it was quite normal not to recover it at sea, forcing the aircraft to return to land before alighting.

==Operational history==
[[File:IMAM Ro.43.JPG|right|thumb|200x200px| Ro.43 preserved in the [[Italian Air Force Museum|Museum of the Aeronautica Militare Italiana at Vigna di Valle]]]]

The aircraft's good endurance meant that seaplanes could still be useful in the constrained [[Mediterranean Sea|Mediterranean]]. Six Ro.43 launched from [[light cruiser]]s played a role in spotting the British fleet during the [[battle of Calabria]], in the opening months of the war.<ref>Green, Jack & Massignani, Alessandro (1998). The Naval War in the Mediterranean, 1940-1943, Chatam Publishing, London, p. 70. ISBN 1-885119-61-5</ref> One of them, departing from the cruiser [[Italian cruiser Eugenio di Savoia|''Eugenio di Savoia'']], kept visual contact with the battleship [[HMS Warspite (03)|HMS ''Warspite'']] during the exchange of fire between the British capital ship and the Italian battleship [[Italian battleship Giulio Cesare|''Giulio Cesare'']] before being chased off by a [[Sea Gladiator]] from the carrier [[HMS Eagle (1918)|HMS ''Eagle'']].<ref>[http://www.regiamarina.net/detail_text_with_list.asp?nid=35&lid=1&cid=4 Action off Calabria - The battleships enter the fight]</ref>

Near the end of 1940, a lone Ro.43 launched by the heavy cruiser [[Italian cruiser Bolzano|''Bolzano'']] was the first to spot the British fleet at the beginning of the [[battle of Cape Spartivento]], at 9:45<ref>Shores, Christopher and Brian Cull with Nicola Malizia (1991).''Malta: The Spitfire Year, 1942''. Grub Street, London, p. 93. ISBN 0-948817-16-X</ref> while the seaplane of [[Italian cruiser Gorizia|''Gorizia'']] located the British convoy at 11:45.<ref>[http://digilander.libero.it/planciacomando/WW2/teulada2.htm ''Battaglia di Capo Teulada - Verso lo scontro''] {{it icon}}</ref> British [[Blackburn Skua|Skuas]] from the carrier [[HMS Ark Royal (91)|HMS ''Ark Royal'']] claimed to have shot down one seaplane after a fruitless bombing on the Italian fleet, purportedly an Ro.43 from the battleship [[Italian battleship Vittorio Veneto|''Vittorio Veneto'']].<ref>Mattesini, Francesco (2000). ''La battaglia di Capo Teulada: 27-28 novembre 1940''. Ufficio storico della Marina Militare, p. 163. {{it icon}}</ref> The performance of the Ro.43s in this battle was eulogized by the Italian supreme command.<ref>Sadkovich, James (1994). ''The Italian Navy in World War II'', Greenwood Press, Westport, p. 98. ISBN 0-313-28797-X</ref>
Another Ro.43 launched by ''Vittorio Veneto'' pinpointed the British cruiser squadron at 6:35 during the engagement near [[Gavdos]] island, the prelude of the [[Battle of Cape Matapan]], on 28 March 1941.<ref>Methidis, Alexis (2008). ''Air War Over Greece and Albania 1949-1941''. Ravi Rikhye, p. 58. ISBN 0-9776072-6-7</ref>

A cruiser-borne Ro.43 signaled the presence of the British convoy by dropping flares during the [[Second Battle of Sirte]],<ref>Woodman, Richard (2000). ''Malta Convoys''. John Murray Ltd., p. 298. ISBN 0-7195-5753-4</ref> while another seaplane from the battleship [[Italian battleship Littorio|''Littorio'']] directed the fire of the Italian fleet onto the British squadron<ref>Shores, Cull and Malizia, p.140</ref> before disengaging at 17:24.<ref>Andó and Bagnasco, p. 201</ref>

The Ro.43s continued to take part in shipborne operations as late as June 1942, during the Italian cruiser attack on the [[Operation Harpoon (1942)|Harpoon convoy]].<ref>Rivista aeronautica, Volume 62, p. 112. Ministero dell'aeronautica, 1986 {{it icon}}</ref> One of the Italian seaplanes was shot down by a [[Bristol Beaufighter]] from [[Malta]] in the course of this action.<ref>Shores, Cull and Malizia, p. 337</ref>

One hundred and five aircraft were in service at the start of World War II, more than enough to equip the major surface units of the Italian Navy, but soon a better aircraft was requested, possibly a navalized fighter. This resulted in a small series being built of a naval version of the [[Reggiane Re.2000]] that could be catapulted but was not fitted with floats so had to either return to a land base or ditch, in a similar fashion to the [[Hawker Hurricane]]s operated by British [[CAM ship]]s. The best feature were the folding wings, but even so the maximum carried on board was usually two. This, together with the modest possibilities of recovery and the lack of experience with naval aviation (even though the Italian Navy possessed a [[seaplane carrier]], the ''[[Giuseppe Miraglia]]'') limited the use of the aircraft in combat.

Around 200-240 were produced until 1941, with 48 still in service in 1943 .

==Operators==
;{{flag|Kingdom of Italy}}
*''[[Regia Marina]]''
*[[Regia Aeronautica]]

==Specifications (Ro.43)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|ref=Encyclopedia of Military Aircraft<ref>Jackson, Robert, ''The Encyclopedia of Military Aircraft'', Parragon, 2002. ISBN 0-7525-8130-9</ref>
|crew= Two
|capacity=
|length main= 9.71 m
|length alt= 31 ft 10 in
|span main= 11.57 m
|span alt= 37 ft 11 in
|height main= 3.51 m
|height alt= 11 ft 6 in
|area main= 33.36 m²
|area alt= 358 ft²
|airfoil=
|empty weight main= <!--kg-->
|empty weight alt= <!--lb-->
|loaded weight main= 2,400 kg
|loaded weight alt= 5,300 lb
|useful load main= <!--kg-->
|useful load alt= <!--lb-->
|max takeoff weight main= <!--kg-->
|max takeoff weight alt= <!--lb-->
|more general=
|engine (prop)=[[Piaggio P.X R.]]
|type of prop=9-cylinder [[radial engine]]
|number of props=1
|power main= 522 kW
|power alt= 700 hp
|power original=
|max speed main= 300 km/h
|max speed alt= 162 knots, 186 mph
|max speed more= at 2,500 m (8,200 ft)
|cruise speed main= <!--km/h-->
|cruise speed alt= <!--knots, mph-->
|never exceed speed main= <!--km/h-->
|never exceed speed alt= <!--knots, mph-->
|stall speed main= <!--km/h-->
|stall speed alt= <!--knots, mph-->
|range main= 1,500 km
|range alt= 590 nm, 678 mi
|ceiling main= 6,600 m
|ceiling alt= 21,600 ft
|climb rate main= <!--m/s-->
|climb rate alt= <!--ft/min-->
|loading main= <!--kg/m²-->
|loading alt= <!--lb/ft²-->
|thrust/weight=<!--a unitless ratio-->
|power/mass main= <!--W/kg-->
|power/mass alt= <!--hp/lb-->
|more performance=
|armament=
* 2 × 7.7 mm machine guns
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
* [[List of Interwar military aircraft]]
* [[List of aircraft of World War II]]
|see also=<!-- other relevant information -->
}}

==References==
{{reflist|2}}
{{refbegin}}

* Andó, Elio and Bagnasco, Erminio (1977). ''Navi e marinai italiani nella seconda guerra mondiale''. Albertelli, Roma. {{it icon}}
* Angelucci, Enzo (ed.). ''World Encyclopedia of Military Aircraft''. London:Jane's. 1981. ISBN 0-7106-0148-4.
* Lembo, Daniele ''Officine Meccaniche Meridionali'', Aerei nella storia magazione, Delta editions, Parma, oct-nov 2003 {{it icon}}
{{refend}}
{{Commons category|IMAM Ro.43}}

==External links==
*{{it icon}} [http://digilander.libero.it/torpedoclub/Regia%20Ricognizione2.htm Imam Ro 43]
* {{it icon}} [http://www.aeronautica.difesa.it/SitoAM/Default.asp?idsez=6&idarg=92&idente=122 ''La scheda sul Ro.43 dell'Aeronautica Italiana''].

{{IMAM aircraft}}

{{DEFAULTSORT:Imam Ro.43}}
[[Category:Italian military reconnaissance aircraft 1930–1939]]
[[Category:Floatplanes]]
[[Category:IMAM aircraft|Ro.43]]
[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Propeller aircraft]]