{{Infobox television episode
 |image_size = 280px
 |image = Game-of-thrones-s02-e06.jpg
 |caption = Jon Snow cannot bring himself to behead the wildling Ygritte.
 |title = The Old Gods and the New
 |series = [[Game of Thrones]]
 |season = 2
 |episode = 6
 |director = [[David Nutter]]
 |writer = [[Vanessa Taylor]]
 |producer =
 |music = [[Ramin Djawadi]]
 |production = 
 |photographer = [[Martin Kenzie]]
 |editor = Oral Norrie Ottey
 |airdate = {{Start date|2012|5|06}}
 |length = 54 minutes
 |guests = * [[Donald Sumpter]] as Maester Luwin
* [[Ron Donachie]] as Rodrik Cassel
* [[Natalia Tena]] as Osha
* [[Michael McElhatton]] as Roose Bolton
* [[Nonso Anozie]] as Xaro Xhoan Daxos
* [[Nicholas Blane]] as the Spice King
* [[Tom Wlaschiha]] as Jaqen H'ghar
* [[Rose Leslie]] as Ygritte
* [[Gwendoline Christie]] as Brienne of Tarth
* [[Oona Castilla Chaplin|Oona Chaplin]] as Talisa
* [[Simon Armstrong]] as Qhorin Halfhand
* [[Ralph Ineson]] as Dagmer Cleftjaw
* [[Ian Beattie]] as Meryn Trant
* [[Fintan McKeown]] as Amory Lorch
* Forbes KB as Black Lorren
* [[Amrita Acharia]] as Irri
* [[Kristian Nairn]] as Hodor
* [[Steven Cole]] as Kovarro
* [[Aimee Richardson]] as Myrcella Baratheon
* [[Art Parkinson]] as Rickon Stark
* Callum Wharry as Tommen Baratheon
 |season_list =
 |prev = [[The Ghost of Harrenhal]]
 |next = [[A Man Without Honor]]
 |episode_list = [[Game of Thrones (season 2)|''Game of Thrones'' (season 2)]]<br>[[List of Game of Thrones episodes|List of ''Game of Thrones'' episodes]]
}}

"'''The Old Gods and the New'''" is the sixth episode of the [[Game of Thrones (season 2)|second season]] of [[HBO]]'s [[medieval fantasy]] television series ''[[Game of Thrones]]''. The episode is written by [[Vanessa Taylor]] and directed by [[David Nutter]], his directorial debut for the series.

The episode's title refers to both the "[[Themes in A Song of Ice and Fire#Old Gods|Old Gods]]" of the North, and the "[[Themes in A Song of Ice and Fire#Faith of the Seven|New Gods]]," the prevalent religion in the rest of Westeros.

This episode won a [[Primetime Emmy Award]] for Outstanding Makeup for a Single-Camera Series (Non-Prosthetic).<ref name=emmy>{{cite web |url=http://www.emmys.com/shows/game-thrones |title=Game Of Thrones |publisher=Emmys.com |accessdate=March 5, 2013}}</ref>

==Plot==

===At Winterfell===
Theon Greyjoy ([[Alfie Allen]]) has taken Winterfell following his prior gambit at Torrhen's Square. Declaring himself prince and Lord of Winterfell, Theon convinces the current lord, Bran Stark ([[Isaac Hempstead-Wright]]), to yield after promising not to harm the castle's inhabitants. However, when Ser Rodrik Cassel ([[Ron Donachie]]) is captured outside of Winterfell and brought to Theon, the knight contemptuously spits on him, and Dagmer Cleftjaw suggests that Theon personally execute Ser Rodrik to save face in front of his men. Theon does so, but is unable to execute him with one blow, and has to repeatedly hack at him to decapitate him. Later, the wildling servant Osha ([[Natalia Tena]]) seduces Theon, offering herself in exchange for her freedom; unbeknownst to Theon, Osha's seduction is a ruse to allow her and Hodor ([[Kristian Nairn]]) to spirit Bran and Rickon ([[Art Parkinson]]) safely out of Winterfell.

===In the Westerlands===
Robb Stark ([[Richard Madden]]) again encounters the field nurse Talisa ([[Oona Castilla Chaplin|Oona Chaplin]]). As they talk flirtatiously with one another, Catelyn Stark ([[Michelle Fairley]]) arrives at the camp. Discovering that the nurse is Lady Talisa Maegyr from the Free City of Volantis, and sensing Robb's attraction to her, Catelyn firmly reminds her son that he is not at liberty to pursue romance: in return for Lord Walder Frey allowing the Stark armies to cross the Twins, he has promised to marry one of Frey's daughters. The Starks then receive the news of Theon Greyjoy's betrayal and the execution of Ser Rodrik. Furious, Robb declares that he will recapture Winterfell, but Lord Roose Bolton ([[Michael McElhatton]]) advises Robb that if he marches back to the North now, he will lose what he has gained against the Lannisters. Bolton proposes sending his bastard son with the forces he has left at his stronghold, the Dreadfort, to end the Ironborn occupation of Winterfell. Robb reluctantly agrees, but demands that Theon be captured alive so that Robb may understand Theon's treachery before he personally executes him.

===Beyond the Wall===
The Night's Watch expedition, led by Qhorin Halfhand ([[Simon Armstrong]]), reaches and captures a wildling watchpost. The wildlings are all killed, aside from Ygritte ([[Rose Leslie]]), a single female prisoner captured by Jon Snow ([[Kit Harington]]). After Ygritte boasts of the growing wildling army, Jon offers to execute the prisoner before rejoining the rest of the group. However, Jon finds himself unable to kill Ygritte, and his resulting hesitation allows her to escape. Jon is able to recapture Ygritte, but now finds himself separated from the main party. With night fast approaching, Jon is forced to sleep in the open. He refuses to make any fires, but is convinced by Ygritte to huddle next to her in order to share body warmth.

===In King's Landing===
Myrcella Baratheon ([[Aimee Richardson]]) is sent to Dorne as part of her arranged marriage alliance with House Martell, with the royal entourage going to the harbour to bid her farewell. While the royal entourage is returning to the Red Keep, unrest in the royal city reaches a breaking point: King Joffrey Baratheon ([[Jack Gleeson]]) is struck in the face with manure hurled by the crowd, and he reacts by ordering his guards to kill everyone in the crowd. A riot ensues and the royal family is forced to flee for safety. Tyrion Lannister ([[Peter Dinklage]]) slaps Joffrey for his foolishness and tries to take control of the situation, but the Kingsguard refuse to obey him. Trapped outside, Sansa Stark ([[Sophie Turner (actress)|Sophie Turner]]) is nearly gang-raped by several men before being rescued by Sandor "The Hound" Clegane ([[Rory McCann]]).

===At Harrenhal===
Lord Tywin Lannister ([[Charles Dance]]) grows increasingly exasperated at the incompetence of his advisors and men-at-arms, who have let sensitive military information slip into the hands of Stark loyalists by confusing addressees. Tywin observes that his cupbearer Arya Stark ([[Maisie Williams]]) can read better than many of his knights, something he finds unusual because Arya later tells him her father was a stonemason. Arya's effort to conceal her true identity is further endangered when Petyr Baelish ([[Aidan Gillen]]) unexpectedly arrives at Tywin's chambers for a meeting. Forced to wait at the table and serve wine, Arya tries to conceal her face from Baelish. Although he notices the cupbearer after she nervously spills his wine, it is unclear as to whether he recognises her as Arya. Later, one of Tywin's knights, Ser Amory Lorch ([[Fintan McKeown]]), catches Arya with a stolen parchment detailing war orders concerning her brother Robb. She manages to escape him, then finds the assassin Jaqen H'ghar ([[Tom Wlaschiha]]), who is still masquerading as a Lannister guardsman. She implores him to kill Ser Amory without delay to repay the second of the three "lives" he owes her. As Ser Amory enters Tywin's chambers to expose Arya, he drops dead with a poisoned dart lodged in his neck.

===In Qarth===
Daenerys Targaryen ([[Emilia Clarke]]) meets with the Spice King ([[Nicholas Blane]]), one of Qarth's ruling Thirteen. Daenerys' entreaties to the Spice King for ships are rebuffed as he is not moved by her passion alone, but rather tangible promises. Later, returning with her host Xaro, Daenerys and her entourage suddenly discover the dead bodies of Qartheen guards and her own ''khalasar'' strewn around Xaro's mansion. The trail of bodies leads to her antechamber, where she sees that her handmaiden Irri ([[Amrita Acharia]]) has been killed and her dragons have been stolen. The dragons are then seen being ferried to a tower by a mysterious hooded figure.

==Production==
[[File:David Nutter by Gage Skidmore 2.jpg|right|thumb|upright|The episode was directed by [[David Nutter]].]]

===Writing===
"The Old Gods and the New" is the second episode scripted by the season's new addition to the writing staff [[Vanessa Taylor]], adapting the material taken from the following chapters of [[George R.R. Martin]]'s original work ''[[A Clash of Kings]]'': Arya VIII, Daenerys III, Tyrion IX, Bran VI, Jon VI (39, 41, 42, 47, 52). Also, the opening scene with the ironborn taking Winterfell uses elements from three different chapters: Theon IV, V, and VI (51, 57 and 67).<ref name="westeros" />

Some of the most significant changes from the books include Jon not letting Ygritte leave after refusing to execute her, the executions of Rodrik and Irri (in the books Rodrik is not killed until later, and Irri is still alive by the end of the fifth book), and Arya using her second wish to kill Amory Lorch instead of Weese (a cruel understeward who has not been cast for the series). The Reed children have not been introduced yet: in the books they aid in Bran and Rickon's escape from Winterfell. Furthermore, at this point the Qarth storyline is only loosely based on the source material, and the theft of Daenerys' dragons never takes place in the books.<ref name="westeros">{{Cite web|url=http://www.westeros.org/GoT/Episodes/Entry/The_Old_Gods_and_the_New/Book_Spoilers|title=EP206: The Old Gods and the New|work=westeros.org|first=Elio|last=Garcia|accessdate=May 8, 2012}}</ref>

===Casting===
This episode features the introduction of [[Rose Leslie]] playing the Wildling woman Ygritte. The producers had seen her in ''[[Downton Abbey]]'', where she had played Gwen Dawson, and they had admired her ability to do Northern accents. The Scottish actress used a Yorkshire accent in ''Downton Abbey.''<ref>{{Cite web|url=http://www.themarysue.com/rose-leslie-talks-ygritte/|title=New Game of Thrones Actor Rose Leslie Opens Up About Getting Cast As Ygritte|work=The Mary Sue|first=Jill|last=Pantozzi|accessdate=May 8, 2012}}</ref> She was also trained in basic stage combat at the [[London Academy of Music and Dramatic Art]] and was eager to play the most physical aspects of her role.<ref>{{Cite web|url=http://crushable.com/entertainment/5-facts-about-new-game-of-thrones-cast-member-rose-leslie-177/|title=5 Facts About New Game of Thrones Cast Member Rose Leslie|work=Crushable|first=Lucia|last=Peters|accessdate=May 8, 2012}}</ref>

Two prominent recurring guests actors had their last appearance in the show. Winterfell's master-at-arms Rodrik Cassel (Ron Donachie) and the Dothraki handmaiden Irri (Amrita Acharia) were killed, and in both cases their deaths in the series was far earlier than their deaths in the original books. Acharia was surprised when she found out that Irri died, but felt that the death served a purpose making Daenerys more isolated. A scene depicting how Irri was strangled was actually filmed but was not included in the final montage. The actress revealed: "I think it's hard to be strangled onscreen because obviously to an extent to make it look real, you really have to be a bit strangled. So I had massive bruises on my neck the next day. I was proud. Battle scars."<ref>{{Cite web|url=http://www.tvguide.com/News/Game-Thrones-Amrita-Acharia-1047022.aspx|title=Game of Thrones' Amrita Acharia: Why Daenerys Has Lost So Much|work=[[TV Guide]]|first=Hanh|last=Nguyen|accessdate=May 11, 2012}}</ref>

===Filming locations===
[[File:Innenforpileporten.jpg|thumb|The riot was filmed by the Pile Gates, at the main entrance to Dubrovnik's old city.]]
The episode's interior shots continued to be filmed at Belfast's [[The Paint Hall]], while the scenes at Winterfell and Harrenhal were filmed at the sets built at Moneyglass and [[Banbridge]],<ref>{{Cite web|url=http://winteriscoming.net/2011/09/day-51-filming-during-a-hurricane/|title=Reports from the set in Iceland|work=winter-is-coming.net|accessdate=May 8, 2012}}</ref> respectively.

Iceland was used to depict the far north, and the scenes from this episode (and the next one) were filmed at the glacier Svínafellsjökull in [[Vatnajökull National Park]], close to [[Skaftafell]].<ref>{{Cite web|url=http://winteriscoming.net/2011/11/reports-from-the-set-in-iceland/|title=Day 51: Filming during a hurricane|work=winter-is-coming.net|accessdate=May 8, 2012}}</ref>

At Dubrovnik, the production used the seashore between Fort Bokar and [[Fort Lovrijenac]] to film Myrcella's departure,<ref>{{Cite web|url=http://winteriscoming.net/2011/10/this-just-in/|title=This just in|work=winter-is-coming.net|accessdate=May 8, 2012}}</ref> the Pile Gate's inner gateway for the riot scene,<ref>{{Cite web|url=http://www.portaloko.hr/clanak/snimanje-serije-igre-prijestolja-pile-vrve-kostimiranim-junacima-foto/0/19765/|title=Snimanje serije "Igre prijestolja": Pile vrve kostimiranim junacima (FOTO)|work=Portal Oko|last=AH|accessdate=May 8, 2012}}</ref> the inner terrace of Fort Lovrijenac for the refuge where the royal family hides from the mob, and the [[Rector's Palace, Dubrovnik|Rector's Palace]] for the atrium of the Spice King in Qarth. In this later location, the bust of the Croatian 16th century seaman Miho Pracat can be clearly seen.<ref>{{Cite web|url=http://www.dubrovnik-guide.net/rector_palace.htm|title=The Rector's Palace|work=Dubrovnik Guide|accessdate=May 8, 2012}}</ref>

==Reception==

===Ratings===
The viewership of the episode's first airing held steady, obtaining 3.879 million viewers and a 2.0 among the 18-49 demographic. The repeat was watched by 0.832 million additional viewers, also in line with last week's rating<ref>{{Cite web|url=http://tvbythenumbers.zap2it.com/2012/05/08/sunday-cable-ratings-nba-playoffs-game-of-thrones-the-client-list-khloe-lamar-mad-men-more/132856/|title=Sunday Cable Ratings: NBA Playoffs + 'Game of Thrones', 'The Client List', 'Army Wives,' 'Khloe & Lamar', 'Mad Men' + More|work=TV by the numbers|first=Amanda|last=Kondolojy|accessdate=May 8, 2012}}</ref> In the United Kingdom, the episode was seen by 0.870 million viewers on [[Sky Atlantic]], being the channel's highest-rated broadcast that week.<ref>{{cite web|url=http://www.barb.co.uk/whats-new/weekly-top-10?|title=Top 10 Ratings (7 - 13 May 2012)|work=[[Broadcasters' Audience Research Board|BARB]]|accessdate=January 19, 2017}}</ref>

===Critical reception===
Upon airing, the episode received overwhelming critical praise. [[Review aggregator]] [[Rotten Tomatoes]] surveyed 14 reviews and judged 100% of them to be positive. The website's critical consensus reads, "Thanks to a balance of thrilling action, complex character work, and a savage twist, 'The Old Gods and The New' justifies its deviation from the source material."<ref>{{cite web|url=http://www.rottentomatoes.com/tv/game-of-thrones/s02/e06/|title=The Old Gods and the New|work=[[Rotten Tomatoes]]|accessdate=May 3, 2016}}</ref> [[IGN]]'s Matt Fowler gave the episode a perfect 10 out of 10, noting that "Book purists will certainly have their gripes, but I found 'The Old Gods and the New' to be nothing short of an intense triumph; (sic) filled with tons of cruelty and shock."<ref>{{cite web|url=http://www.ign.com/articles/2012/05/07/game-of-thrones-the-old-gods-and-the-new-review|title=Game of Thrones: "The Old Gods and the New" Review|first=Matt|last=Fowler|date=6 May 2012|publisher=}}</ref> Todd VanDerWerff of ''[[The A.V. Club]]'' gave the episode an "A" and called it one of the best episodes of the series. He commented on how the plot was diverging more and more from [[A Clash of Kings|the novel]], but argued that the heart of the story was kept and that the changes were necessary in order to explicitly express what in the novel was characters' internal monologues. He also praised the thematic unity of the episode, achieved by making the large number of character arcs take place in the course of a single day.<ref>{{cite web|url=http://www.avclub.com/articles/the-old-gods-and-the-new-for-experts,73410/|title=Game Of Thrones (experts): "The Old Gods And The New" (for experts)|date=7 May 2012|publisher=}}</ref>

Jace Lacob of ''Televisionary'' echoed the above sentiments, calling the episode by far the best of the season thus far: "All in all, 'The Old Gods and New' represented a massive achievement for ''Game of Thrones'', a stunning display of well-crafted dialogue, subtle acting, deliberate pacing, and glorious setting, and the firm establishment that the show's continuity is well and truly separate from that of the novels." In particular, he praised the scenes between Arya and Tywin, as well as the riot in King's Landing and the near-rape of Sansa.<ref>{{cite web|url=http://www.televisionaryblog.com/2012/05/where-wild-things-are-old-gods-and-new.html|title=[Televisionary]: Where The Wild Things Are: The Old Gods and the New on Game of Thrones|publisher=}}</ref>

===Accolades===
This episode received a [[Primetime Emmy Award]] nomination for Outstanding Hairstyling for a Single-Camera Series. It won for Outstanding Makeup for a Single-Camera Series (Non-Prosthetic).<ref name=emmy/>

==References==
{{Reflist|2}}

==External links==
{{wikiquotepar|Game_of_Thrones_(TV_series)#The_Old_Gods_and_the_New_.5B2.06.5D|The Old Gods and the New}}
* [http://www.hbo.com/game-of-thrones/episodes/2/16-the-old-gods-and-the-new/index.html "The Old Gods and the New"] at [[HBO.com]]
* {{IMDb episode|2085238}}
* {{tv.com episode|game-of-thrones/the-old-gods-and-the-new-2416152}}

{{Game of Thrones Episodes}}

{{DEFAULTSORT:Old Gods and the New, The}}
[[Category:Game of Thrones episodes]]
[[Category:2012 American television episodes]]