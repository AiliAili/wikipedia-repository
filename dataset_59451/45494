{{Infobox military person
|name= James B. McGovern Jr.
|birth_date= {{birth date|1922|2|4}}
|death_date= {{death date and age|1954|5|6|1922|2|4}}
|birth_place= [[Elizabeth, New Jersey|Elizabeth]], [[New Jersey]], U.S.
|death_place= [[Laos|Northern Laos]]
|allegiance= {{flag|United States of America|1912}}
|rank= [[Captain (United States)|Captain]]
|branch= {{flagdeco|USA|1912}} [[United States Army Air Forces]]<br />{{flagicon image|Flag of the United States Central Intelligence Agency.svg}} [[Central Intelligence Agency]]
|image=Earthquake mcgoon2.jpg
|nickname= "Earthquake McGoon"
|awards= Chevalier (knight) of the [[France|French]] [[Legion of Honour]] 
|laterwork=
}}
'''James B. McGovern Jr.''' (February 4, 1922{{spaced ndash}}May 6, 1954<ref name=CIA>{{cite web |url=https://www.cia.gov/news-information/featured-story-archive/earthquake-mcgoons-final-flight.html |title=A Look Back ... Earthquake McGoon's Final Flight |publisher=Central Intelligence Agency |date=July 16, 2009 |accessdate=October 10, 2012}}</ref>) was a [[World War II]] [[Flying ace|fighter ace]] and later [[Central Intelligence Agency]] pilot.  He and co-pilot Wallace Buford were the only [[United States|Americans]] to die in combat in the [[First Indochina War]]. At the time, they were officially employees of [[Civil Air Transport]].

==Early life and education==
He was born in [[Elizabeth, New Jersey]]. After graduating from high school in 1940, he went to work for the Wright Aircraft Engineering Company in Paterson, New Jersey.<ref name=CIA/>

==Career==

===World War II===
McGovern enlisted in the [[United States Army Air Corps]] in May 1942.<ref name=CIA/> He served in [[China]] in 1944 as part of the 14th Air Force's 118th Tactical Reconnaissance Squadron, [[23rd Fighter Group]].<ref>{{cite web |url=http://www.118trs.com/squadron-roster |title=Squadron Roster |publisher=118trs.com |accessdate=October 10, 2012}}. The CIA claims he was a member of the 75th Fighter Squadron, 23rd Fighter Group.</ref> The 118th was known for its "Black Lightning" markings on its P-51s that have been carried forward to the [[Learjet 35|C-21]]s that they fly today as the [[118th Airlift Squadron]].  During this time, he was credited with shooting down four [[Empire of Japan|Japanese]] [[Mitsubishi A6M Zero|Zero]] fighters, and destroying another five on the ground.<ref>[http://www.air-america.org/newspaper_articles/Earthquake_McGoon.shtml Remains of 'Earthquake McGoon' sought after 48 years]</ref>

The nickname "Earthquake McGoon" was given to McGovern in World War II because the first four letters of his last name, and, like the namesake character in the ''[[Li'l Abner]]'' comic strip, he was a big man at {{convert|6|ft|m}} and {{convert|260|lb|kg}} (considered large for a fighter pilot).

===Prisoner of Chinese Communists===
On December 5, 1949 a plane he was piloting crash landed in [[Guangxi]] province. The plane was flying from [[Hong Kong]] to [[Kunming]]. Its automatic direction finder failed. Additional technical trouble caused a [[forced landing]] 180 miles west of [[Nanning]]. McGovern and the other passengers were captured by Chinese guerrilla fighters. On January 4, 1950 they reached Nanning and were told they were prisoners.<ref>{{cite news |last= Associated Press|first= |date= 9 March 1950 |title= M'Govern, Prisoner Loses 50 Pounds: Chinese Radio Man Escapes from Reds, Reports Communist Food Has Affected Elizabeth Pilot.|url= |newspaper= |location= Hong Kong|access-date= }}</ref> He was later released in May 1950.

===Dien Bien Phu===
His [[C-119 Flying Boxcar]] cargo plane was hit twice by ground fire, first in the port engine, then in the [[horizontal stabilizer]],<ref name=CIA/> while parachuting a [[howitzer]] to the [[Battle of Dien Bien Phu|besieged French garrison at Dien Bien Phu]] during the First Indochina War.<ref name=Six>{{cite web |url=http://www.check-six.com/Crash_Sites/CAT-149_McGoon.htm |title=The Shootdown of "Earthquake McGoon" |publisher=Check-Six.com}}</ref> He managed to fly {{convert|75|mi|km}}, but just short of a landing strip in [[Laos]], a wingtip clipped a tree.<ref name=CIA/> Moments before impact, McGovern was heard to say over his radio, "Looks like this is it, son."<ref>Fall, Bernard. "Hell in a Very Small Place: The Siege of Dien Bien Phu." Da Capo Press, 1966, p. 373-374.</ref> McGovern, his co-pilot Wallace Buford, and two French crewmen were killed. Two others were thrown clear; one later died of his injuries.  The day after, the garrison at Dien Bien Phu surrendered.

McGovern's skeletal remains were discovered in an unmarked grave in northern Laos in 2002. They were identified in September 2006 by laboratory experts at the U.S. military's Joint POW/MIA Accounting Command.<ref>[http://www.dtic.mil/dpmo/news/2007/McGovern.pdf CIA Pilot missing in action from Vietnam is identified]</ref>  He was interred in [[Arlington National Cemetery]] on May 24, 2007.

==Legacy==

===Legion of Honour===
On February 24, 2005, James McGovern was posthumously awarded (along with Buford and six other surviving pilots) the [[Legion of Honour]] with the rank of knight (chevalier) by the [[President of France]] [[Jacques Chirac]] for their actions in supplying Dien Bien Phu during the 57-day siege.<ref>{{cite web |url=http://www.air-america.org/newspaper_articles/France_Award.pdf |title=French-American Relations |publisher=Embassy of France in the US |date=February 24, 2005}}</ref>

==See also==
{{portal|United States Army}}

==References==
{{reflist}}

==External links==
*[http://www.chicagotribune.com/news/nationworld/chi-0610230147oct23,1,4018341.story?track=rss Vietnam MIA `McGoon' coming home for burial]
*[http://www.check-six.com/Crash_Sites/CAT-149_McGoon.htm The Shootdown of Earthquake McGoon]
*[http://www.facesfromthewall.com/civilians/CV1954.html James 'Earthquake McGoon' McGovern Jr. Memorial]
*[http://forum.12oclockhigh.net/showthread.php?t=6466 McGovern's two victory credits]
*[http://www.air-america.org/News/CAT_Pilots_Receive_French_Award.shtml CAT Pilots to be honored by France]
*[http://www.103rdfighterwing.com/ History of 103rd Airlift Wing]
*[http://www.google.ca/imgres?hl=en&client=firefox-a&hs=HCR&sa=X&rls=org.mozilla:en-US:official&biw=892&bih=725&tbm=isch&prmd=imvns&tbnid=IIYJT0e-9naZAM:&imgrefurl=http://deniskitchen.com/Merchant2/merchant.mvc%3FScreen%3DCTGY%26Category_Code%3Dbios.mcgoon&docid=kvFyZQcly7stCM&imgurl=http://deniskitchen.com/docs/bios/bio.mcgoon.jpg&w=359&h=399&ei=xv11UMa3Aor1iQLKmoCoAg&zoom=1&iact=rc&dur=4&sig=115753331627017030002&page=1&tbnh=179&tbnw=161&start=0&ndsp=12&ved=1t:429,r:0,s:0,i:71&tx=80&ty=82 Image of the cartoon Earthquake McGoon]

{{DEFAULTSORT:Macgovern, James B., Jr.}}
[[Category:1922 births]]
[[Category:1954 deaths]]
[[Category:United States Army officers]]
[[Category:American World War II flying aces]]
[[Category:American military personnel of World War II]]
[[Category:People of the First Indochina War]]
[[Category:Aviators from New Jersey]]
[[Category:Chevaliers of the Légion d'honneur]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:Aviators killed by being shot down]]
[[Category:People from Elizabeth, New Jersey]]