{|{{Infobox Aircraft Begin
 |name = Super2 <!-- avoid stating manufacturer (it's stated 3 lines below) unless name used by other aircraft manufacturers -->
 |image = File:arv super2 g-bpmx at kemble arp.jpg
 |caption = <!--Image caption; if it isn't descriptive, please skip-->
}}{{Infobox Aircraft Type
 |type = Light Aircraft
 |national origin = United Kingdom
 |manufacturer = ARV
 |designer = Bruce Giddings
 |first flight = 11 March 1985 
 |introduction = <!--Date the aircraft entered or will enter military or revenue service-->
 |retired = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
 |status = Production discontinued
 |primary user = <!-- list only one user; for military aircraft, this is a nation or a service arm. Please DON'T add those tiny flags, as they limit horizontal space. -->
 |more users = <!--Limited to three in total; separate using <br> -->
 |produced = <!--Years in production (e.g. 1970-1999) if still in active use but no longer built -->
 |number built = about 40
 |program cost = <!--Total program cost-->
 |unit cost = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 |developed from = <!--The aircraft which formed the basis for this aircraft-->
 |variants with their own articles = <!--Variants OF this aircraft-->
}}
|}
[[File:ARV prototype.jpg|thumb|right|ARV prototype]]
[[File:ARV Midwest.pdf|thumb|right|ARV with MidWest AE110, Ecoprop & Trelleborg tyres]]
[[File:ARV with Rotax-912.jpg|thumb|right|ARV with Rotax 912 & [[tundra tyre]]s]]
[[File:ARV with Jabiru engine.jpg|thumb|right|ARV with Jabiru 2200]]

The '''ARV Super2''' is a British two-seat, [[Strut|strut-braced]], [[shoulder wing]], [[tricycle landing gear]] [[light aircraft]] designed by Bruce Giddings.<ref>"Pilot" magazine, June 1985 page 6</ref> Available either factory-built or as a kit, it was intended to be both a cost-effective [[Trainer (aircraft)|trainer]]<ref name="Green1987">Green 1987, pp 26-27</ref> and an affordable aircraft for private owners.<ref>''Flight International'' 6 April 1985, pp. 13–14.</ref> Later called the "Opus", it gained US [[Federal Aviation Administration]] [[Light-sport aircraft]] approval in February 2008.<ref>Johnson, Dan (2102), [http://www.bydanjohnson.com/search.cfm?m_id=422 Opus Aircraft LLC], retrieved 17 March 2012</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 67. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

About 35<ref>Moss, Roger (January 2010), [http://britishaviation-ptp.com/id128.html ARV2], retrieved 17 March 2012</ref> aircraft were produced in the 1980s before the [[Isle of Wight]]-based company went into liquidation.<ref name="janes extract">[http://www.janes.com/articles/Janes-All-the-Worlds-Aircraft/OPUS-ARV-Super-2-United-States.html OPUS ARV Super 2 (United States), Aircraft - Fixed-wing - Civil]. ''Jane's.com''. Jane's Information Group. 19 February 2010. Retrieved 9 September 2010.</ref>  Subsequently there have been a number of attempts to  restart production, all unsuccessful, of which the most recent was by Opus Aircraft. In November 2013,  Opus Aircraft announced that its assets had been auctioned off successfully, adding: "We hope to see our plans continued and to see the all-aluminum plane flying by 2015".<ref name="Assets of Opus Aircraft LLC">{{cite web|url=http://www.ironhorseauction.com/index.php?subp=1&sct=109&pg=ap&pid=34868 |title=Assets of Opus Aircraft LLC |publisher=Iron Horse Auction |date= |accessdate=2013-10-18}}</ref><ref name="Opus Aircraft">{{cite web|url=http://www.opusaircraft.com |title=Opus Aircraft |publisher= |accessdate=16 December 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20141216183832/http://www.opusaircraft.com/ |archivedate=16 December 2014 |df= }}</ref>

==Design and development==
[[Richard Noble]], the world 1983 [[land speed record|land speed record holder]] and UK entrepreneur, identified a gap in the market for a low-cost lightweight two-seat trainer, after expensive product-liability lawsuits in the USA had driven the major American [[general aviation]] manufacturers temporarily to abandon production of such aircraft. Noble established a factory at [[Sandown]] on the [[Isle of Wight]] to build the ARV Super2 aircraft, with the first prototype flying on 11 March 1985.<ref name="Janes 88 p279"/> The factory used some novel manufacturing techniques, including British ALCAN's "Supral"<ref name="PilotFeb86" /> (a [[Superplasticity|superplastic]] aluminium alloy), adhesives (to reduce rivet count and save weight), and a [[bespoke]] new British engine, the [[Hewland AE75]]. These innovations gave the ARV an empty weight 40% lower than the [[Cessna 152]],<ref name="Blech p47">Blech 1986, p.47.</ref> making the Super2 both cheaper to buy and to operate. The manufacturer claimed it could reduce pilot training costs by 25%<ref name="Green1987" />

The ARV Super2 is a [[side-by-side configuration]] two-seater with a [[shoulder wing]] for improved visibility.<ref name="PilotFeb86">"Pilot" magazine February 1986 pages 32-33</ref>  The wing is [[Forward-swept wing|swept forward]] 5° to maintain correct [[center of gravity]] balance.<ref name="PilotFeb86" /> The wing area is a small {{convert|92|sqft|m2|abbr=on}}, giving a wing loading of 11.9&nbsp;lb/ft² (58.1&nbsp;kg/m²).<ref name="PilotFeb86" />  The forward sweep may also promote a [[Forward-swept wing#Inward spanwise flow|spanwise airflow]] inwards towards the root, thereby reducing the likelihood of a wingtip stall.  The ARV's tapered [[fibreglass]] wingtips help to reduce drag from [[wingtip vortices]].

The ARV is constructed mainly of [[aluminium]] alloy, with fibreglass wingtips, cowlings and canopy frame.<ref name="Green1987" /><ref name="PilotJun85">"Pilot" magazine, June 1985 pages 5-6</ref> The cockpit is a stiff [[monocoque]] of "Supral" alloy for lightness and improved crash protection. Aft of the cockpit bulkhead, the ARV is conventionally built, with frames, longerons and a [[stressed skin]] forming a [[semimonocoque|semi-monocoque]].  Skin sections are both glued and riveted. The aircraft has twin control sticks.  Ailerons, elevators and flaps are pushrod-controlled, but the rudder and trim are cable-linked.  The rudder pedals also control a steerable nosewheel, but the hand-operated [[disc brake]]s are not differential and do not contribute to steering.<ref name="PilotFeb86" />

The AE75 engine, a {{convert|49|kg|lb|0|abbr=on}}<ref name="PilotJun85" />{{convert|75|hp|kW|0|abbr=on}}<ref name="Green1987" /> inverted three-cylinder water-cooled two-stroke unit with [[dual ignition]] and a 2.7:1 reduction gearbox,<ref>"Air Pictorial" magazine April 1986</ref> was specially developed for the ARV by [[Hewland]] from their existing two-cylinder [[microlight]] engine. The AE75 engine has a [[Time between overhaul|TBO]] of only 800 hours, and, in the absence of continuing factory support, many ARVs have had their AE75s replaced with engines such as the [[Rotax 912]],<ref name="GINFOXARV">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=XARV|title = GINFO Search Results G-XARV|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref> the [[Rotax 914]],<ref name="GINFOZARV">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=ZARV|title = GINFO Search Results G-ZARV|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref><ref name="GINFOOTAL">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=OTAL|title = GINFO Search Results G-OTAL|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref> or the [[Jabiru 2200]].<ref>[[Civil Aviation Authority (United Kingdom)]], [http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=DEXP GINFO Search Results], retrieved 15 December 14</ref> Three ARVs were originally manufactured with the [[MidWest AE series|MidWest]] twin-rotor [[Wankel engine#Aircraft engines|wankel engine]].<ref name="GINFOORIX">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=ORIX|title = GINFO Search Results G-ORIX|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref><ref name="GINFOBWBZ">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=BWBZ|title = GINFO Search Results G-BWBZ|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref><ref name="GINFOBMWJ">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=BMWJ|title = GINFO Search Results G-BMWJ|accessdate = 7 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|authorlink = |date=August 2011}}</ref><ref>"Pilot" magazine April 1994 page 22</ref>

The Super2 gained [[Type certificate|airworthiness certification]] in July 1986,<ref name="Janes 88 p279"/> and soon after entered production.  In November 1985, Noble had reached an agreement to supply ARV parts to Canada's Instar Aviation, where the aircraft was intended to be assembled for the North American market. [[Transport Canada]] had agreed to certify the aircraft based on it "meeting UK criteria", but in the end these Canadian production plans came to nothing.<ref>"Canadian Aviation" magazine December 1986</ref>

Originally, ARVs were available either as kit-built aircraft (subject to [[Popular Flying Association|PFA]] Permit), or factory built (and subject to the [[Civil Aviation Authority (United Kingdom)|CAA]] [[Type certificate|Certificate of Airworthiness]]). In the spring of 1990 Aviation Scotland Limited was to restart production and in 1993 that company intended to set up another facility in [[Sweden]] to build ARVs.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 174. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref> In the late 1990s the aircraft was sold in kit form in the USA as the Highlander by Highlander Aircraft Corporation of [[Golden Valley, Minnesota]].<ref name="Aerocrafter" />

In 2004, the CAA reclassified all ARVs as PFA (now [[Light Aircraft Association|LAA]]) Permit aircraft. The ARV Super2 was originally intended to be aerobatic, but the Isle of Wight factory closed before CAA clearance was obtained, so aerobatics remain prohibited. Also, the factory had planned to develop a four-seater version, wing-tip tanks and floats.<ref>"Pilot" magazine February 1996 page 34</ref>

Opus Aircraft upgraded some specifications for the aircraft, increasing the [[Vne]] to {{convert|134|kn|km/h|0|abbr=on}} and increasing the gross weight to {{convert|1168|lb|kg|0|abbr=on}}. The company intended to equip the aircraft with the [[Rotax 912UL]] or 912A of {{convert|80|hp|kW|0|abbr=on}}.<ref name="Opusspecs">{{cite web|url = http://www.opusaircraft.com/specs.html|title = Aircraft Specifications|accessdate = 5 August 2011|last = Opus Aircraft|authorlink = | archiveurl= https://web.archive.org/web/20110715013313/http://www.opusaircraft.com/specs.html| archivedate= 15 July 2011 <!--DASHBot-->| deadurl= no}}</ref>

===Production history===
The ARV Super2 has had an interrupted production history, with a number of successive companies producing 40 aircraft in total.<ref>{{cite web|url=http://britishaviation-ptp.com/arv.html|title=ARV|work=britishaviation-ptp.com|accessdate=29 April 2015}}</ref>

Shortly after initial aircraft deliveries began, there were a number of forced landings.  These were due to gearbox failures induced by propeller vibration, and in November 1987 the [[Civil Aviation Authority (United Kingdom)|CAA]] grounded the aircraft.<ref>''Flight International'', 14 November 1987, p.16</ref>  Although these problems were quickly resolved, the aircraft's reputation suffered.<ref>"Flyer" magazine, February 1994, page 13</ref> Buyers and investors lost confidence, leading to the closure of the Isle of Wight factory and the company was forced into [[Administration (insolvency)|administration]]. This resulted in a [[management buyout]] and the company being renamed ''Island Aircraft''.<ref>Belch 1989, pp. 92-94.</ref> Some 30 or so aircraft were completed by ARV and then by Island Aircraft at Sandown.  Production was then transferred first to Scotland and then to Sweden, where the ARV was renamed the "Opus 280",<ref>''Flight International'' 11–17 August 1993, p.24.</ref><ref>Daly 1994, p.30.</ref> However, no aircraft were produced in Sweden before the proprietors went bankrupt in 1995.<ref>''Flight International''. 27 September - 3 October 1995. p.6.</ref> After yet another unsuccessful attempt to restart production in [[Ohio]], [[United States|USA]], all manufacturing rights (plus a selection of aircraft parts) were sold to a new consortium, "Opus Aircraft".<ref name="OpusHome">{{cite web|url = http://www.opusaircraft.com/|title = Welcome to  Opus Aircradft, LLC. Manufacturers of the OPUS Super2 Sport Aircraft|accessdate = 19 August 2011|last = Opus Aircraft}}</ref>  Opus Aircraft established a factory at  Rockingham, near [[Stoneville, North Carolina]],<ref name="Grady16Oct13" /><ref>{{cite web|url=http://www.bydanjohnson.com/index.cfm?b=6&m=5&id=41 |title=LSA Videos |publisher=ByDanJohnson.com |date= |accessdate=2012-10-27}}</ref> and in  February 2008, the "Opus Super 2" was granted FAA [[Light Sport Aircraft]] approval.<ref name="janes extract"/> The factory produced an "Opus Super 2" prototype, but the proposed resumption of production never occurred.<ref name="Grady16Oct13" /> The company, valued at [[US$]]8M, was offered for sale for US$2.9M, and in October 2013 the company was successfully bought at auction.<ref name="janes extract"/><ref name="Assets of Opus Aircraft LLC"/><ref name="Opus Aircraft"/><ref name="Grady16Oct13">{{cite news|url = http://www.avweb.com/avwebflash/news/LSA-Manufacturer-To-Be-Sold-At-Auction220785-1.html|title = LSA Manufacturer To Be Sold At Auction|accessdate = 17 October 2013|last = Grady|first = Mary|date = 16 October 2013| work = AVweb}}</ref><ref name="Opushome">{{cite web|url = http://www.opusaircraft.com/|title = Opus Aircraft home|accessdate = 5 August 2011|last = Opus Aircraft|authorlink = |date=February 2008}}</ref><ref name="Opusopp">{{cite web|url = http://www.opusaircraft.com/Opportunities.html|title = Opus Aircraft Opportunities|accessdate = 5 August 2011|last = Opus Aircraft|authorlink = |year = n.d.| archiveurl= https://web.archive.org/web/20110715013326/http://www.opusaircraft.com/Opportunities.html| archivedate= 15 July 2011 <!--DASHBot-->| deadurl= no}}</ref> No further restart of production has materialized.

==Operational history==
The ARV Super2 has received favourable reviews which describe it as an aircraft with excellent visibility that is pleasant to fly.<ref>"A little sweetheart" in "Flyer" magazine February 1994 page 13</ref> The small wing area and the fairly high wing loading produces handling that is responsive but stable.<ref name="ReferenceA">"Pilot" magazine April 1994 page 26</ref> A drawback is the shortage of luggage space which makes the ARV unsuitable for touring.{{Citation needed|date=September 2016}} The second prototype, G-STWO, has a fuselage luggage compartment aft of the fuel tank which is accessed from the starboard aside, but no other ARVs have this feature.{{Citation needed|date=September 2016}}

After initial production commenced at least two flying schools adopted the ARV, but today most Super2s in the UK are flown  privately.<ref>"Flyer" magazine March 1991 page 12</ref><ref>"Pilot" magazine April 2003 page 66</ref><ref name="GINFOall">{{cite web|url = http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=arv1|title = GINFO Search Results Summary|accessdate = 16 August 2011|last = [[Civil Aviation Authority (United Kingdom)]]|date=August 2011}}</ref>

The [[Hewland AE75]] engine, which is no longer available, has a [[Time between overhaul|life]] of 800 hours, after which time alternative engines are installed. Replacement engines have all been heavier than the {{convert|49|kg|lb|0|abbr=on}} original, needing a lead counterweight in the tail to maintain balance. The empenage is short-coupled and its area is a little marginal for the landing flare, so flaring with a stopped engine might prove difficult.<ref>"Pilot" magazine April 2003 page 65</ref>

LAA-approved modifications include [[ground-adjustable propeller]]s, [[tundra tyre]]s, additional fuel tanks and fuel-injection. The LAA has approved the fitting and testing of [[vortex generator]]s, with a view to reducing stall speed and improving landing flare.<ref>LAA mod nos:10360, 10906, & 12250</ref>
<!-- ==Accidents==
Please don't include accidents here unless they make the standard explained at [[WP:AIRCRASH]]
-->

==Specifications (ARV Super 2)==
{{Aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's All the World's Aircraft 1988-89 <ref name="Janes 88 p279">Taylor 1988, p.279.</ref>  
|crew=Two
|capacity=
|length main= 5.49 m
|length alt= 18 ft 0 in
|span main= 8.69 m
|span alt= 28 ft 6 in
|height main= 2.31 m
|height alt= 7 ft 7 in
|area main= 8.59 m²
|area alt= 92.5 ft²
|airfoil=NACA 2415 (modified)
|empty weight main= 306 kg
|empty weight alt= 675 lb
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt=  
|max takeoff weight main= 499 kg
|max takeoff weight alt= 1,100 lb
|more general=
|engine (prop)=[[Hewland AE75]]
|type of prop= 3-cylinder liquid-cooled inline engine
|number of props=1
|power main= 57 kW
|power alt= 77 hp
|power original=
|max speed main= 190 km/h
|max speed alt= 103 knots, 118 mph
|max speed more= at 610 m (2,000 ft)
|cruise speed main= 153 km/h
|cruise speed alt= 83 knots, 95 mph
|cruise speed more=(econ cruise)
|stall speed main= 88 km/h
|stall speed alt= 48 knots, 55 mph
|never exceed speed main=232 km/h
|never exceed speed alt= 126 knots, 149 mph
|range main= 500 km
|range alt= 270 NM, 311 mi
|ceiling main= m
|ceiling alt= ft
|climb rate main= 3.2 m/s
|climb rate alt= 620 ft/min
|loading main= 58.1 kg/m²
|loading alt= 11.9 lb/ft²
|thrust/weight=
|power/mass main= 0.11 kW/kg
|power/mass alt= 0.07 hp/lb
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related= <!-- related developments -->

|similar aircraft=<!-- similar or comparable aircraft -->
*[[Malmö MFI-9|Bölkow Junior]]
*[[Saab Safari]]

|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}

==References==

===Notes===
{{reflist|30em}}

===Bibliography===
{{refbegin}}
*"[http://www.flightglobal.com/pdfarchive/view/1987/1987%20-%202330.html Engine troubles grounds ARV Super 2]", ''Flight International'', 14 November 1987. p.&nbsp;16.
*"[http://www.flightglobal.com/pdfarchive/view/1993/1993%20-%201965.html Sweden helps ARV Super 2 to live again]". ''Flight International'', 11–17 August 1993. p.&nbsp;24.
* Blech, Robin. "[http://www.flightglobal.com/pdfarchive/view/1986/1986%20-%202442.html Super 2 Comes of Age]". ''[[Flight International]]'', 13 September 1985. pp.&nbsp;48–52.
* Blech, Robin. "[http://www.flightglobal.com/pdfarchive/view/1989/1989%20-%201490.html Super 2 - back from the brink]". ''Flight International'', 20 May 1989. pp.&nbsp;92–94.
* Daly, Kieran. "[http://www.flightglobal.com/pdfarchive/view/1994/1994%20-%202277.html Light Work]". ''Flight International'', 21–27 September 1994. p.&nbsp;30.
* Green, William: ''Observers Aircraft''. Frederick Warne Publishing, 1987. ISBN 0-7232-3458-2.
* [http://www.flightglobal.com/pdfarchive/view/1985/1985%20-%201039.html "New UK trainer aims to break US mould"]. ''Flight International'', 6 April 1985, Vol. 127, No. 3954. pp.&nbsp;13–14.
* Taylor, John W.R., ed. ''Jane's All the World's Aircraft 1988-89''. Coulsden, Surrey, UK: Jane's Information Group, 1988. ISBN 0-7106-0867-5.
{{refend}}

==External links==
{{commons category|ARV Super2}}
*[http://wayback.archive.org/web/*/http://www.opusaircraft.com Opus Aircraft archives] on [[Archive.org]]

{{DEFAULTSORT:ARV Super2}}
[[Category:British civil utility aircraft 1980–1989]]
[[Category:Light-sport aircraft]]
[[Category:Shoulder-wing aircraft]]
[[Category:Forward-swept-wing aircraft]]
[[Category:Homebuilt aircraft]]