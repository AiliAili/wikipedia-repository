{{Infobox journal
| title = '''Asian Ethnology'''
| cover = To illustrate the cover of Asian Ethnology.jpg
| editor = [[Benjamin Dorman]] and [[Frank J. Korom]]
| discipline = [[Asian Studies]], [[Ethnology]], [[Religious Studies]]
| language = [[English language|English]]
| abbreviation =
| publisher = [[Nanzan Institute for Religion and Culture]]
| country = [[Japan]]
| frequency = Semi-annual
| history = 1942 to present
| openaccess = All issues
| license =
| impact =
| impact-year =
| website = http://nirc.nanzan-u.ac.jp/en/publications/asian-ethnology/
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR = 18826865
| OCLC = 298239510
| LCCN =
| CODEN =
| ISSN = 1882-6865 
| eISSN =
}}

'''''Asian Ethnology''''' is an [[open access journal|open access]] semi-annual, [[peer-reviewed]] journal dedicated to the promotion of research on the peoples and [[Culture of Asia|cultures of Asia]].<ref>{{cite web |url=http://nirc.nanzan-u.ac.jp/publications/afs/afsMain.htm |title=Asian Ethnology |publisher=[[Nanzan Institute for Religion and Culture]] |accessdate=6 February 2012}}</ref> It was first published in 1942 at the [[Catholic University of Peking]] as ''Folklore Studies''<ref>{{cite journal |jstor=3182920 |title=Folklore Studies - Volume One (cover page) |publisher=[[The Catholic University of Peking]] }}</ref> and subsequently at [[Nanzan University]], where from 1963 to 2007 it was known as ''Asian Folklore Studies''.<ref>{{cite web |url=http://www.jstor.org/journal/asiafolkstud |title=Asian Folklore Studies - publication information |publisher=[[JSTOR]]  |accessdate=7 February 2012}}</ref>

''Asian Ethnology'' presents formal essays and analyses, research reports, and critical book reviews relating to a wide range of topical categories, including
* narratives, performances, and other forms of cultural representation
* [[popular religious]] concepts
* vernacular approaches to health and healing
* local ecological/environmental knowledge
* [[collective memory]] and uses of the past
* cultural transformations in [[diaspora]]
* [[transnational flows]]
* [[material culture]]

The journal is indexed in [[Arts and Humanities Citation Index]], [[Bibliography of Asian Studies]], [[Directory of Open Access Journals]], and [[EBSCO: Academic Search Complete]].

== References ==
{{Reflist}}

== External links ==
* [http://nirc.nanzan-u.ac.jp/en/publications/asian-ethnology/ Journal homepage]
* [http://www.jstor.org/journal/asianeth JSTOR]

{{Italic title}}

[[Category:Asian folklore]]
[[Category:Japanese studies]]
[[Category:Works about Asia]]
[[Category:Publications established in 1942]]
[[Category:English-language journals]]
[[Category:Open access journals]]