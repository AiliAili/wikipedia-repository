{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{{Orphan|date=December 2013}}

'''Emily Lyle''' (born 19 December 1932 in [[Glasgow]]) is a Scottish ballad scholar and Senior Research Fellow in the School of Celtic and Scottish Studies at the [[University of Edinburgh]].

== Biography ==

Emily Lyle grew up in [[Kilbarchan]], [[Renfrewshire]], [[Scotland]]. She studied English Language and Literature at [[St. Andrews University]] (M.A. 1954), followed by an education course at the [[University of Glasgow]] (Diploma in Education, 1955).

For six years she taught English in secondary schools in Britain and New Zealand before she was appointed as a Lecturer in English at [[Outwood Academy Ripon|Ripon College]] of Education in Yorkshire (1961–65). While employed as a Senior Lecturer in English at [[Neville’s Cross College]] in [[Durham, England|Durham]] (1965–68), she wrote her doctoral dissertation "A Study of Thomas the Rhymer and Tam Lin in Literature and Tradition" (1967) at the Institute of Folk-Life Studies at the [[University of Leeds]].   Moving away from the teaching of English literature, she soon established herself in the field of Scottish Studies.

== Academic career ==

In 1976-77, she went to Australia as a visiting Fellow at the [[Australian National University]]. There she collected oral material from those with Scottish connections, some of which is included in the CD “Chokit on a Tattie” (focussing on children’s songs and rhymes), and in a forthcoming issue of the journal ''Tochter''. In 1977, Lyle donated copies of her tape recordings to the [[National Library of Australia]].<ref>{{cite web|title=Emily Lyle Collection at the National Library of Australia|url=http://www.nla.gov.au/selected-library-collections/lyle-collection}}</ref> The work she did as a Fellow of the [[Radcliffe Institute]] of [[Harvard University]] in 1974-75 gave her increased visibility as a ballad scholar and led to many more visits to Harvard, including an appointment at the [[Center for the Study of World Religions]] in 1995.

She was appointed as a Research Fellow  at the [[School of Scottish Studies]] of the University of Edinburgh from 1970 to 1995 and as a Lecturer from 1995 to 1998. In 1978 she was Visiting Professor of Folklore at the [[University of California at Los Angeles]]; from 1979-1982 she was a Visiting Lecturer in Folklife Studies at the [[University of Stirling]] from 1979 to 1982. Since 1998 she has been an Honorary Fellow in the School of Celtic and Scottish Studies at Edinburgh.<ref>{{cite web|title=Staff Profile (University of Edinburgh)|url=http://www.ed.ac.uk/schools-departments/literatures-languages-cultures/celtic-scottish-studies/staff/dr-emily-lyle}}</ref>

== Traditional Cosmology Society ==

{{Empty section|date=August 2015}}

== Honours ==

*The Folklore Society’s Coote Lake Research Medal (1987)
*The [[Royal Society of Edinburgh]]’s Henry Duncan Prize Lecture<ref>{{cite web|title=Royal Society of Edinburgh Henry Duncan Prize Lecture|url=http://www.royalsoced.org.uk/659_HenryDuncanPrizeLectureship.html}}</ref> awarded for contributions to Scottish culture (1997)
*Fellow of the [[Association for Scottish Literary Studies]]<ref>{{cite web|title=Association for Scottish Literary Studies, Honorary Fellowships|url=http://www.arts.gla.ac.uk/ScotLit/ASLS/HonFels.html}}</ref>
*Honorary Life Member of the Traditional Music and Song Association of Scotland 
*The [[Saltire Society]]/[[National Library of Scotland|National Library of Scotland's]] Research Book of the Year Prize for 2003, jointly with Katherine Campbell, for Volume 8 of ''The Greig-Duncan Folk Song Collection''
*Recipient of the [[Festschrift]] ''Emily Lyle: The Persistent Scholar''

== Selected Publications ==

=== Books ===

*''Andrew Crawfurd’s Collection of Ballads and Songs'', ed. 2 vols.  Edinburgh: Scottish Text Society, 1975, 1996.
*''Ballad Studies'', ed.  Cambridge: D.S. Brewer Ltd., Folklore Society Mistletoe Series; and Rowman and Littlefield, Totowa, N.J., 1976. 
*''The Greig-Duncan Folk Song Collection'', ed. with others. 8 vols. Aberdeen: Aberdeen University Press, and Edinburgh: Mercat Press, 1981-2002.
*''Scottish Ballads'', ed. Edinburgh: Canongate, 1994; Barnes and Noble, New York, 1995.
*''The Song Repertoire of Amelia and Jane Harris'', ed. jointly with Anne Dhu McLucas and Kaye McAlpine. Edinburgh: Scottish Text Society, 2002.
*''Fairies and Folk: Approaches to the Scottish Ballad Tradition''. B•A•S•E (Ballads and Songs – Engagements) 1. Trier: WVT Wissenschaftlicher Verlag Trier, 2007.
*''Ten Gods: A New Approach to Defining the Mythological Structures of the Indo-Europeans''. Newcastle upon Tyne: Cambridge Scholars Publishers, 2012.
*''Robert Burns and the Discovery and Re-Creation of Scottish Song'' (joint with Katherine Campbell). Glasgow: Musica Scotica Trust Publications. Forthcoming, 2013.

=== Articles ===

*"Songs from South-West Scotland, 1825-1830: Motherwell's Personal Records in Relation to Records in Crawfurd’s Collection." ''Singing the Nations: Herder's Legacy''. Eds. Dace Bula and Sigrid Rieuwerts. B•A•S•I•S (Ballads and Songs – International Studies) 4. Trier: WVT Wissenschaftlicher Verlag Trier, 2008. 188-98. 
*"The Gest of Robyn Hode" (text and headnote). ''The Chepman and Millar Prints''. Ed. Sally Mapstone. Edinburgh: National Library of Scotland and Scottish Text Society, 2008. DVD.
*and Katherine Campbell. "The Perfect Fusion of Words and Music: The Achievement of Robert Burns." ''Musica Scotica: 800 Years of Scottish Music. Proceedings from the 2005 and 2006 Conferences''. Ed. Kenneth Elliott, et al. Glasgow: Musica Scotica Trust Publications, 2008. 19-27.
*"Three Notes on 'King Orphius'." ''Scottish Literary Review'' 1 (2009): 51-68. 
*"Robert Burns: Man with a Mission." ''The Folklore Historian'' 26 (2009): 3-18. 
*"The Tale of the Bold Braband in The Complaynt of Scotland." ''Review of Scottish Culture'' 22 (2010): 196-201.
*"'Robin Hood in Barnsdale Stood': A New Window on the Gest and Its Precursors." ''Child’s Children: Ballad Study and Its Legacies ed. Joseph Harris and Barbara Hillers''. B•A•S•I•S (Ballads and Songs – International Studies). Trier: Wissenschaftlicher Verlag Trier, 2012. 71-96.
*"Genre: Ballad." ''The Edinburgh Companion to Scottish Traditional Literatures''. Eds. Sarah M. Dunnigan and Suzanne Gilbert. Edinburgh: Edinburgh University Press (forthcoming).

== References ==

{{reflist}}

== External links ==
* http://www.folklore-society.com/awards/cootelake.asp
* http://www.nla.gov.au/media-releases/scottish-gaelic-scholar-receives-national-folk-fellowship

{{DEFAULTSORT:Lyle, Emily}}
[[Category:Living people]]
[[Category:1932 births]]
[[Category:Scottish ballads]]
[[Category:Academics of the University of Edinburgh]]
[[Category:Alumni of the University of St Andrews]]
[[Category:Alumni of the University of Glasgow]]
[[Category:Academics of the University of Leeds]]
[[Category:Harvard University faculty]]
[[Category:People from Renfrewshire]]