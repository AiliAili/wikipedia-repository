{{Infobox journal
| title = Marine Resource Economics
| cover =
| editor = [[Martin D. Smith]]
| discipline = [[Economics]]
| abbreviation = Mar. Resour. Econ.
| publisher = [[MRE Foundation]]
| country =
| frequency = Quarterly
| history = 1984–present
| openaccess =
| license =
| impact = 1.083
| impact-year = 2011
| website = http://marineresourceeconomics.com/
| link1 = http://marineresourceeconomics.com/toc/mare/current
| link1-name = Online access
| link2 = http://marineresourceeconomics.com/loi/mare
| link2-name = Online archive
| JSTOR =
| OCLC = 790420618
| LCCN = 84644303
| CODEN = JMREDD
| ISSN = 0738-1360
| eISSN =
}}
'''''Marine Resource Economics''''' is a quarterly [[peer review|peer-reviewed]] [[academic journal]] covering [[environmental economics]]. It is published by the [[MRE Foundation]] in affiliation with the [[North American Association of Fisheries]] and the [[International Institute of Fisheries Economics and Trade]]. The current [[editor-in-chief]] is [[Martin D. Smith]] ([[Nicholas School of the Environment]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 1.083.<ref name=WoS>{{cite book |year=2013 |chapter=Marine Resource Economics |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2013-01-23 |series=[[Web of Science]] |postscript=.}}</ref>

== Most-cited papers ==
{{cleanup-list|section|date=January 2013}} <!-- limit to three top articles -->
According to Google Scholar, the following papers have been cited most often: 
* Farming the Sea, F Asche, Vol. 23(4), 2008.
* The Efficiency Gains from Fully Delineating Rights in an ITQ Fishery, C Costello, RT Deacon, Vol. 22(4), 2010.
* Valuing Beach Access and Width with Revealed and Stated Preference Data, JC Whitehead, CF Dumas, J Herstine, J Hill, R Buerger, Vol. 23(2), 2008.
* TURFs and ITQs: Collective vs. Individual Decision Making, JP Cancino, H Uchida, JE Wilen, Vol. 22(4), 2010.
* The Salmon Disease Crisis in Chile, F Asche, H Hansen, R Tveteras, S Tveteras, Vol. 24(4), 2009.
* Sharing Rules and Stability in Coalition Games with Externalities, LG Kronbak, M Lindroos, Vol. 22(2), 2010.
* Social, Economic, and Regulatory Drivers of the Shark Fin Trade, S Clarke, EJ Milner-Gulland, T Bjorndal, Vol. 22(3), 2010.
* An evaluation of sustainable seafood guides: implications for environmental groups and the seafood industry", CA Roheim, Vol. 24(3) 301-310, 2009.
* Bioeconometrics: Empirical Modeling of Bioeconomic Systems, MD Smith, Vol. 23(1), 2010.
* Economic Values of Dolphin Excursions in Hawaii: A Stated Choice Analysis, W Hu, K Boehle, LJ Cox, M Pan, Vol. 24(1), 2009.

== See also ==
* [[Environmental resources management]]
* [[Marine ecosystem]]

== References ==
{{Reflist}}

==External links==
* {{Official website|http://marineresourceeconomics.com/}}

{{DEFAULTSORT:Marine Resource Economics}}
[[Category:Economics journals]]
[[Category:Resource economics]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1984]]
[[Category:English-language journals]]
[[Category:Environmental social science journals]]