'''Samuel David Leidesdorf''', (1881-1968) internationally known [[accountant]], recently inducted into the [[CPA Hall of Fame]].

Leidesdorf, was born on September 25, 1881 in [[New York City]]. He attended the [[New York School of Accounting]] and [[Pace College]]. In 1905, he began his own accounting firm, [[S.D. Leidesdorf & Co.]], [[certified public accountant]]s, which grew and eventually became one of the largest accounting firms in the nation. The 1915 New York City Directory shows him living at 302 West 79th and with the S.D. Leidesdorf Co office at 417 5th Avenue.<ref>[http://content.ancestry.com/Browse/view.aspx?dbid=8773&path=New+York+City.1915.L.33 1915 New York City Directory], online at www.ancestry.com</ref>  His World War I Draft Registration Card shows he had moved to 309 W 86th.<ref>[http://content.ancestry.com/Browse/view.aspx?dbid=6482&path=New+York.Manhattan+City.126.L.94&fn=Samuel%20David&ln=Leidesdorf&st=r&pid=17519580&rc=&zp=100 WW1 Draft Registration Card], online at www.ancestry.com</ref> In 1929, he helped arrange the sale of [[Newark, New Jersey|Newark-based]] [[Bamberger's]] department store, to the [[Macy's|R.H. Macy]] Co.<ref>[http://www.commentarymagazine.com/Summaries/V35I5P41-1.htm ''Commentary'' magazine]</ref>  [[Fortune Magazine]] recognized his company as a “Pioneering Firm” in 1932.

In 1931 he was on the executive committee of the [[American Jewish Committee]].<ref>[http://scplweb.santacruzpl.org:2281/hqoweb/library/do/books/results/image?urn=urn:proquest:US;glhbooks;Genealogy-glh47067257;-1;-1;&polarity=&scale=&hitthreadid=347 ''The American Jewish yearbook''], Jewish Publication Society of America, Philadelphia. 1931. 532 pages</ref>  He served on the [[State Council of Accountancy]], from 1934 to 1942.  When [[Einstein]] arrived in the US, Leidesdorf was his accountant.<ref>[http://www.crossingwallstreet.com/archives/2005/12/albert_einstein.html Crossingwallstreet.com Archives]</ref>  He soon began dedicating himself to what would become a lifetime of philanthropic endeavors for medical and educational institutions, and many other charitable causes, including the [[Red Cross]], the [[United Jewish Communities|United Jewish Appeal]], the [[Young Women's Christian Association]] and the [[United Negro College Fund]]. "In a letter written to Mr. S. D. Leidesdorf of New York, [[John D. Rockefeller, Jr.|Rockefeller]] solicited Leidesdorf to serve as a member of the Foundation Committee of the UNCF, working with W. D. Embree, the Chairman."<ref>[http://scplweb.santacruzpl.org:2299/itx/retrieve.do?subjectParam=Locale%2528en%252C%252C%2529%253AFQE%253D%2528su%252CNone%252C10%2529Leidesdorf%2524&contentSet=IAC-Documents&sort=DateDescend&tabID=T002&sgCurrentPosition=0&subjectAction=DISPLAY_SUBJECTS&prodId=ITOF&searchId=R1&currentPosition=4&userGroupName=scruzpl&resultListType=RESULT_LIST&sgHitCountType=None&qrySerId=Locale%28en%2C%2C%29%3AFQE%3D%28TX%2CNone%2C10%29Leidesdorf%24&inPS=true&searchType=BasicSearchForm&displaySubject=&docId=A98253620&docType=IAC ''The early years of the United Negro College Fund, 1943-1960''. Shuana K. Tucker. The Journal of African American History (Fall 2002): p416(17).]</ref>

Mr. Leidesdorf was active in civic and community service and received numerous honors and awards, including an honorary degree as doctor of humane letters from [[Hebrew Union College]], the Medal of the [[National Fund for Medical Education]], the [[Herbert H. Lehman Human Relations Award]] of the [[American Jewish Committee]] and the [[Medal for Distinguished Service]] from the [[Protestant Council]] of the City of New York.  In 1958, Mr. Leidesdorf received [[The Hundred Year Association of New York]]'s Gold Medal Award "in recognition of outstanding contributions to the City of New York." He was a founding Trustee for the [[Institute for Advanced Study]].

In 1948, Mr. Leidesdorf began his tenure on the board of the New York University Medical Center serving as the first Chair of the Building Committee. Later he served as the Chair of the board from 1956 until his death on September 21, 1968.

He was chairman of the board of [[New York University School of Medicine|NYU Medical Center]] from 1956 to 1968.  At the time of his death in 1968, the firm he founded was in the top 10 of accounting firms in the United States.  The company he started merged to become [[Ernst & Whinney]] in 1979 and [[Ernst and Young]] in 1989.

With his wife Elsa Grunwald, he had two children Helen and Arthur (1918–91).  Arthur D. like his father, was a philanthropist.  Arthur also married Tova, a prior beauty-contest winner and International Socialite, who continues his philanthropic work.  Tova was honored in 2003 as "Woman of the Year" by the [[Shaare Zedek Medical Center]].

==Notes==
<references/>

==Resources==
*[http://www.nysscpa.org/society/PR/5-18-05releaseb.htm Hall of Fame induction]
*[http://www.transmediagroup.com/newsroom/2003/1118Tova.htm Street named for him]
*[http://www.med.nyu.edu/alumni/clubs/leidesdorf.html Leidesdorf Club]
*[http://ias.edu/Newsroom/announcements/Uploads/view.php?cmd=view&id=262 IAS site]
*[http://www.familytreeexpert.com/legacy/samuel_david_leidesdorf.htm Picture]
*1930 US Federal Census
*Social Security Death Index
*[http://trees.ancestry.com/owt/person.aspx?pid=174438456 OneWorldTree]
*World War I Draft Registration Card, published by ancestry.com

==Further reading==
*Biography Index. A cumulative index to biographical material in books and magazines. Volume 4: September, 1955-August, 1958. New York: H.W. Wilson Co., 1960. (BioIn 4)
*Biography Index. A cumulative index to biographical material in books and magazines. Volume 8: September, 1967-August, 1970. New York: H.W. Wilson Co., 1971. (BioIn 8)
*Who Was Who in America. Volume 5, 1969-1973. Chicago: Marquis Who's Who, 1973. (WhAm 5)

{{Authority control}}
{{DEFAULTSORT:Leidesdorf, Samuel D.}}
[[Category:American accountants]]
[[Category:1881 births]]
[[Category:1968 deaths]]
[[Category:Pace University alumni]]
[[Category:American Jews]]
[[Category:People from New York City]]