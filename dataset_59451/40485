{{Infobox restaurant
| name              = Gustu
| logo              = 
| logo_width        = 
| logo_alt          = 
| image             = Front side of Restaurant Gustu, La Paz.jpg
| image_width       = 
| image_alt         = 
| image_caption     = 
| pushpin_map       = 
| map_width         = 
| map_alt           = 
| map_caption       = 
| slogan            = 
| established       = 2012
| closed            = <!-- {{End date|YYYY|MM|DD}} --> 
| current-owner     = 
| previous-owner    = 
| chef              =
| head-chef         = Kamilla Seidler
| food-type         = Modern Cuisine
| dress-code        = None
| rating            = No. 17 in Latin America
| street-address    = No. 300 Calle 10, Calacoto
| city              = [[La Paz]]
| county            = 
| state             = 
| postcode          = 
| country           = [[Bolivia]]
| iso_region        = 
| coordinates_display  = 
| latitude          = 
| longitude         = 
| latd              = 
| latm              = 
| lats              = 
| latNS             = 
| longd             = 
| longm             = 
| longs             = 
| longEW            = 
| coordinates       = 
| seating-capacity  = 
| reservations      = 
| other-locations   = 
| other-information = 
| website           = {{URL|www.restaurantgustu.com}}
}}
'''Gustu''', the [[quechuan languages|quechua]] word for flavour, is a  restaurant and bar placed in [[La Paz]], [[Bolivia]], which opened in April 2012. Gustu is under the management of [[Chef de cuisine|Head Chef]] Kamilla Seidler and [[Chief executive officer|CEO]] Michelangelo Cestari.<ref name=":0">{{Cite web|title = Interview with Kamilla Seidler, chef at Gustu (La Paz)|url = http://www.gastroeconomy.com/2015/05/kamilla-seidler-those-who-had-the-guts-to-believe-in-gustu-now-reap-the-rewards/|accessdate = 2015-07-10}}</ref><ref name="comosur.com">{{Cite web|title = Catching Up With The Chefs From Bolivia’s Gustu {{!}} Como Sur {{!}} South American Gastronomy|url = http://comosur.com/2013/10/02/catching-up-with-the-chefs-from-bolivias-gustu/|accessdate = 2015-07-10}}</ref> It is considered among South America's 50 best restaurants, landing the number 32 spot on The Latin America’s 50 Best Restaurants 2014 list made by the British magazine [[Restaurant (magazine)|Restaurant]].<ref name=":0" />

== History ==
Gustu opened its doors in April 2012 and soon was noticed by major international newspapers and magazines along with its founder [[Claus Meyer]] at the front.<ref>{{Cite web|title = The Best of 2013: Gustu {{!}} Como Sur {{!}} South American Gastronomy|url = http://comosur.com/2013/12/30/the-best-of-2013-gustu/|accessdate = 2015-07-10}}</ref><ref>{{Cite web|title = Claus Meyer, el chef más famoso del mundo, abre Gustu en La Paz - La Razón|url = http://www.la-razon.com/index.php?_url=/suplementos/financiero/Claus-Meyer-famoso-Gustu-Paz_0_1807019356.html|website = www.la-razon.com|accessdate = 2015-07-10}}</ref> Most noteworthy is the appearances in [[The New York Times]],<ref>{{Cite news|title = In Bolivia, High Goals and Visions of Haute Cuisine|url = https://www.nytimes.com/2012/11/07/world/europe/in-bolivia-high-goals-and-visions-of-haute-cuisine.html|newspaper = The New York Times|date = 2012-11-06|access-date = 2015-07-09|issn = 0362-4331|first = William|last = Neuman}}</ref><ref>{{Cite news|title = Chic Stops in the Suburbs of La Paz, Bolivia|url = https://www.nytimes.com/2014/06/08/travel/chic-stops-in-the-suburbs-of-la-paz-bolivia.html|newspaper = The New York Times|date = 2014-06-05|access-date = 2015-07-09|issn = 0362-4331|first = Nicholas|last = Gill}}</ref><ref>{{Cite news|title = 52 Places to Go in 2015|url = https://www.nytimes.com/interactive/2015/01/11/travel/52-places-to-go-in-2015.html|newspaper = The New York Times|date = 2015-01-09|access-date = 2015-07-09|issn = 0362-4331}}</ref> [[Theguardian.com|The Guardian]],<ref>{{Cite web|title = Gustu, Bolivia: the surprise restaurant venture by Noma's Claus Meyer|url = https://www.theguardian.com/travel/2013/jun/13/gustu-restaurant-la-paz-bolivia-review|website = the Guardian|accessdate = 2015-07-09|first = Ed|last = Stocker}}</ref><ref>{{Cite web|title = Noma co-founder cooks up new scheme in Bolivia|url = https://www.theguardian.com/world/2013/apr/05/noma-cofounder-new-scheme-bolivia|website = the Guardian|accessdate = 2015-07-09|first = Dan Collyns in La|last = Paz}}</ref><ref>{{Cite web|title = A culinary tour of Bolivia: cooking with altitude|url = https://www.theguardian.com/travel/2013/jun/15/boliva-la-paz-culinary-tour-gustu|website = the Guardian|accessdate = 2015-07-09|first = Ed|last = Stocker}}</ref> [[FT Press|Financial Times]],<ref>{{Cite news|title = Gustu, Bolivia|url = http://www.ft.com/intl/cms/s/0/f51a149a-66dd-11e2-a805-00144feab49a.html#axzz3eqplgBOm|newspaper = Financial Times|date = May 17, 2013<!-- 6:42 pm-->|access-date = 2015-07-09|issn = 0307-1766|first = Nicholas|last = Lander}}</ref><ref>{{Cite news|title = Bolivia’s indigenous people flaunt their new-found wealth|url = http://www.ft.com/intl/cms/s/0/9265426c-7594-11e4-a1a9-00144feabdc0.html#axzz3eqplgBOm|newspaper = Financial Times|date = December 4, 2014<!-- 9:18 pm-->|access-date = 2015-07-09|issn = 0307-1766|first = Andres|last = Schipani}}</ref><ref>{{Cite web|url = http://blogs.ft.com/beyond-brics/2013/04/02/bolivia-calling-all-gastro-tourists/|title = Bolivia calling all gastro tourists|date = April 2, 2013|accessdate = July 9, 2015|website = Financial Times|publisher = Financial Times|last = Mapstone|first = Naomi}}</ref> [[Food and Wine Magazine|Food and Wine]],<ref>{{Cite web|title = Is Gustu the World's Best  New Restaurant?|url = http://www.foodandwine.com/articles/is-gustu-the-worlds-best-new-restaurant|accessdate = 2015-07-09}}</ref> [[Bloomberg News|Bloomberg]],<ref>{{Cite web|title = Noma Owner Opens Gustu in Bolivia to Replicate No. 1 Copenhagen|url = https://www.bloomberg.com/news/articles/2013-03-08/noma-owner-opens-gustu-in-bolivia-to-replicate-no-1-copenhagen|website = Bloomberg.com|accessdate = 2015-07-09|first = Ryan|last = Sutton}}</ref><ref>{{Cite web|title = Gustu Brings Bolivia High-End Indigenous Cuisine|url = https://www.bloomberg.com/news/videos/b/9fab3f8c-13de-4959-a09e-7fdf4cebe657|website = Bloomberg.com|accessdate = 2015-07-09}}</ref><ref>{{Cite web|title = Noma's Next Act: Not Noma|url = https://www.bloomberg.com/slideshow/2013-03-08/noma-s-next-act-.html|website = Bloomberg|accessdate = 2015-07-09}}</ref><ref>{{Cite web|title = Noma Owner Meyer Serves Llama at New Bolivian Shop|url = https://www.bloomberg.com/news/videos/b/dd45f7b5-4ed8-4ff3-af36-c7c7f1044bf9|website = Bloomberg.com|accessdate = 2015-07-09}}</ref> [[Eater magazine|Eater]]<ref>{{Cite web|title = Claus Meyer's Gustu Aims to Elevate Bolivian Cuisine|url = http://www.eater.com/2013/3/8/6470011/claus-meyers-gustu-aims-to-elevate-bolivian-cuisine|website = Eater|accessdate = 2015-07-09}}</ref><ref>{{Cite web|title = Food Writers and Experts on the Untold Stories of 2013|url = http://www.eater.com/2013/12/27/6307981/food-writers-and-experts-on-the-untold-stories-of-2013|website = Eater|accessdate = 2015-07-09}}</ref> and [[CNN]].<ref>{{Cite web|title = Gustu, el restaurante que puso a Bolivia en el mapa gastronómico|url = http://cnnespanol.cnn.com/2014/12/01/gustu-el-restaurante-que-puso-a-bolivia-en-el-mapa-gastronomico/|website = CNNEspañol.com|accessdate = 2015-07-09|first = Por CNN en|last = Español}}</ref>

Being a part of [[Melting Pot Bolivia]], a [[Non-governmental organization|NGO]] project made by Claus Meyer in cooperation with [[IBIS Denmark]],<ref>{{Cite news|title = Claus Meyer's Food Empire|url = https://www.wsj.com/news/articles/SB10001424052702303914304579192251690597682|newspaper = Wall Street Journal|date = 2013-12-05T16:59:00.000Z|access-date = 2015-07-10|issn = 0099-9660|first = Jay|last = Cheshes}}</ref> Gustu was meant as both a   restaurant and a   cooking school.<ref name=":1">{{Cite web|title = Kamilla Seidler: La cocina puede cambiar el mundo|url = http://tiempo.infonews.com/nota/157210/kamilla-seidler-la-cocina-puede-cambiar-el-mundo|accessdate = 2015-07-10}}</ref><ref>{{Cite web|title = Bolivia, entre los lugares imperdibles del mundo|url = http://www.eldeber.com.bo/tendencias/bolivia-lugares-imperdibles-del-mundo.html|accessdate = 2015-07-10}}</ref> After they both experienced success, Gustu departed from Melting Pot Bolivia  and IBIS Denmark, and also created GustuBar, a Bar & Lounge with the same philosophy   as the restaurant itself.<ref>{{Cite web|title = Magacín Lifestyle junio 2015|url = http://issuu.com/magacinlifestyle/docs/magac__n_lifestyle_junio_2015/68?e=15914531/13556235|accessdate = 2015-07-10}}</ref>

== Philosophy ==
Gustu works with the philosophy that they  "can change the world through food" <ref>{{Cite web|title = The Bolivian Gastronomic Movement - Gustu Restaurant|url = http://restaurantgustu.com/eng/about-us/the-bolivian-gastronomic-movement/|website = restaurantgustu.com|accessdate = 2015-07-09}}</ref>''.'' Something  Claus Meyer implemented when he introduced the "Manifesto of the New Bolivian Cuisine".<ref>{{Cite web|title = ‘Improving the World through Food’: Bolivia’s Gustu Restaurant|url = http://theculturetrip.com/south-america/bolivia/articles/bolivia-s-gustu-restaurant-/|website = theculturetrip.com|accessdate = 2015-07-10}}</ref> Most of the students at [[Gustu School]] are underprivileged youngsters from Bolivia.<ref name="comosur.com"/><ref name="Restaurante Gustu en La Paz">{{Cite web|title = Restaurante Gustu en La Paz|url = http://www.lostiempos.com/observador-economico/pequenas-y-medianas-empresas/pequenas-y-medianas-empresas/20130409/restaurante-gustu-en-la-paz_208697_447548.html|website = www.lostiempos.com|accessdate = 2015-07-10}}</ref>

== Food ==
Gustu applies the "kilometer 0 philosophy", meaning that all products used in their dishes and drinks are exclusively born, planted, developed and/or transformed in Bolivian territory.<ref name=":1" /><ref name="Restaurante Gustu en La Paz"/>

== Recognition ==
In 2013, Gustu won Best New Restaurant  in  South America  and  Best Restaurant  in  South America, from "[[Como Sur – South American Gastronomy]]".<ref name=":2">{{Cite web|title = Como Sur’s Best Of 2013 Awards: THE RESULTS ARE IN! {{!}} Como Sur {{!}} South American Gastronomy|url = http://comosur.com/2014/01/20/como-surs-best-of-2013-awards-the-results-are-in/|accessdate = 2015-07-10}}</ref><ref name=":3">{{Cite web|title = (Best Of Year 1) Gustu, Part 2 {{!}} Como Sur {{!}} South American Gastronomy|url = http://comosur.com/2014/05/03/best-of-year-1-gustu-part-2/|accessdate = 2015-07-10}}</ref><ref name=":6">{{Cite web|title = Gustu, simplemente el mejor restaurante - Diario Pagina Siete|url = http://www.paginasiete.bo/gente/2014/1/22/gustu-simplemente-mejor-restaurante-11987.html|accessdate = 2015-07-10}}</ref> Kamilla Seidler won Best Chef in South America  decided by  Como Sur – South American Gastronomy.<ref name=":2" /><ref name=":3" /><ref name=":6" />

In 2014, Gustu placed 32nd on the Latin America’s 50 Best Restaurants  in ''Restaurant'' Magazine,<ref>{{Cite web|title = Latin America's 50 Best Restaurants 1-50|url = http://www.theworlds50best.com/latinamerica/en/the-list.html#t31-40|website = www.theworlds50best.com|accessdate = 2015-07-10|first = http://theworlds50best.asia|last = }}</ref><ref name=":4">{{Cite web|title = Gustu {{!}} Latin America's 50 Best Restaurants|url = http://www.theworlds50best.com/latinamerica/en/The-List/31-40/gustu.html|website = www.theworlds50best.com|accessdate = 2015-07-10|first = http://theworlds50best.asia|last = }}</ref> and won the S. Pellegrino Best Restaurant in Bolivia  award.<ref name=":4" /> It again won Best Restaurant in South America   from  Como Sur – South American Gastronomy, <ref name=":5">{{Cite web|title = Como Sur Best of 2014 Awards: The Winners (PO / ES) {{!}} Como Sur {{!}} South American Gastronomy|url = http://comosur.com/2015/01/19/como-sur-best-of-2014-awards-the-winners-po-es/|accessdate = 2015-07-10}}</ref> and Kamilla Seidler  again won the award for    Best Chef  in  South America.<ref name=":5" />

In 2015, Gustu climbed 15 positions on the Latin America's 50 Best Restaurants and is now placed in place No.17.

==References==
{{Reflist|2}}

{{coord missing|Bolivia}}

[[Category:Restaurants in Bolivia]]
[[Category:Buildings and structures in La Paz Department (Bolivia)]]
[[Category:2012 establishments in Bolivia]]
[[Category:Restaurants established in 2012]]