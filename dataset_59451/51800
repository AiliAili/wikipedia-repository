{{Infobox journal
| title = Journal of Comparative Psychology
| cover =[[File:Journal of Comparative Psychology.gif|150px]]
| editor = [[Josep Call]]
| discipline = [[Comparative psychology]]
| abbreviation = J. Comp. Psychol.
| publisher = [[American Psychological Association]]
| country = United States
| frequency = Quarterly
| history = 1921-present
| openaccess = 
| impact = 2.344
| impact-year = 2014
| website = http://www.apa.org/pubs/journals/com/index.aspx
| link1 = http://content.apa.org/journals/com
| link1-name = Online access
| OCLC = 08997203
| LCCN = 83648068
| CODEN = JCOPDT
| ISSN = 0735-7036
| eISSN = 1939-2087
}}
The '''''Journal of Comparative Psychology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]]. It covers research from a comparative perspective on the behavior, cognition, perception, and social relationships of diverse species.<ref>{{cite web |date=23 July 2012 | url=http://www.apa.org/pubs/journals/com/index.aspx |title=Journal of Comparative Psychology|publisher=[[American Psychological Association]] |accessdate=2012-07-23}}</ref>

==History==
The journal was established in 1921 through the merger of ''Psychobiology'' and the ''Journal of Animal Behavior''. It was renamed ''[[Journal of Comparative and Physiological Psychology]]'' in 1947, and reestablished in 1983 when the journal was split into [[Behavioral Neuroscience (journal)|''Behavioral Neuroscience'']] and the ''Journal of Comparative Psychology''. Past [[editors-in-chief]] include [[Jerry Hirsch]] (1983), [[Gordon Gallup]] (1989), [[Charles Snowdon]] (1994), [[Meredith West]] (2001), and [[Gordon Burghardt]] (2005).<ref name="dewsbury">Dewsbury, Donald A. (June 2012). [http://www.apadivisions.org/division-6/publications/newsletters/neuroscientist/2012/06/comparative-historical-trends.aspx Historical trends in American comparative psychology.] ''The Behavioral Neuroscientist and Comparative Psychologist''</ref> The current editor is [[Josep Call]].

== Abstracting and indexing ==
The journal is abstracted and indexed by [[MEDLINE]]/[[PubMed]] and the [[Social Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.344, ranking it 25th out of 129 journals in the category "Psychology, Multidisciplinary".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychology, Multidisciplinary |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official|http://www.apa.org/pubs/journals/com/index.aspx}}

[[Category:Quarterly journals]]
[[Category:Comparative psychology journals]]
[[Category:Publications established in 1921]]
[[Category:American Psychological Association academic journals]]