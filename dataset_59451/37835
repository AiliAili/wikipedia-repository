{{good article}}
{{Use British English|date=February 2017}}
{{use dmy dates|date=May 2016}}
{{infobox country at games
| NOC = BHU
| NOCname = [[Bhutan Olympic Committee]]
| games = Summer Olympics
| year = 2016
| flagcaption = 
| oldcode = 
| website = {{url|bhutanolympiccommittee.org }}
| location = [[Rio de Janeiro]]
| competitors = 2
| sports = 2
| flagbearer = [[Karma (archer)|Karma]] (opening)<br>[[Kunzang Lenchu]] (closing)
| rank = 
| gold = 0
| silver = 0
| bronze = 0
| officials = 
| appearances = auto
| app_begin_year = 1984
| app_end_year = 
| summerappearances = 
| winterappearances = 
| seealso = 
}}'''Bhutan''' competed at the '''2016 Summer Olympics''' in [[Rio de Janeiro]], which was held from 5 to 21 August 2016. The country's participation in Rio de Janeiro marked its ninth appearance in the Summer Olympics since its début at the [[1984 Summer Olympics]]. The delegation included two female athletes, [[Karma (archer)|Karma]] in the women's [[Archery at the 2012 Summer Olympics – Women's individual|individual archery]] tournament and [[Lenchu Kunzang|Kunzang Lenchu]] in the women's [[Shooting at the 2012 Summer Olympics – Women's 10 metre air rifle|10 metre air rifle]] shooting contest. Both qualified for the Games through [[Wild card (sports)|wildcard]] places<nowiki/> because they did not match the required qualification standards. Karma was selected as the flag bearer for the [[2016 Summer Olympics opening ceremony|opening ceremony]] while Lenchu held it at the [[2016 Summer Olympics closing ceremony|closing ceremony]]. Karma was eliminated at the Round of 64 while Lenchu exited the competition after the shooting qualification round.

==Background==
Bhutan, a country in [[South Asia|Southern Asia]], participated in nine Summer Olympic Games between its debut in the 1984 Summer Olympics in Los Angeles, United States and the 2016 Summer Olympics in Rio de Janeiro, Brazil.<ref name="Sports Reference - Countries - Bhutan">{{cite web | url=http://www.sports-reference.com/olympics/countries/BHU/ | title=Sports Reference – Countries – Bhutan | publisher=[[Sports Reference]] | accessdate=26 July 2016}}</ref> The highest number of Bhutanese athletes participating in a Summer Games is six in both the 1984 Summer Olympics and the 1992 Summer Olympics in Barcelona, Spain.<ref name="Sports Reference - Countries - Bhutan"/> Bhutan competed at the Rio Summer Games from 5 to 21 August 2016.<ref name=pre2016>{{cite news|last=Wangano|first=Lucky|title=Bhutanese athletes briefed on Zika virus pre-Rio 2016|url=http://www.pressreader.com/bhutan/business-bhutan/20160730/281513635524106|work=[[Business Bhutan]]|date=30 July 2016|accessdate=5 February 2017}}</ref> No Bhutanese athlete has ever won a medal at the Olympics.<ref name="Sports Reference - Countries - Bhutan"/> The two athletes who were selected to represent Bhutan at the Rio Games were [[Karma (archer)|Karma]] in the [[Archery at the 2016 Summer Olympics – Women's individual|women's individual archery]] tournament and [[Kunzang Lenchu]] in the [[Shooting at the 2016 Summer Olympics – Women's 10 metre air rifle|women's 10 metre air rifle]] shooting contest.<ref>{{cite news|last=Duphu|first=Cheten|title=Two Bhutanese to compete at Olympic Games Rio 2016|url=http://www.bbs.bt/news/?p=60980|work=[[Bhutan Broadcasting Service]]|date=4 August 2016|accessdate=5 February 2017}}</ref> Along with the two athletes, the Secretary General of Bhutan's Olympic Committee, and two coaches, the country's delegation was led by the heir presumptive to the Bhutan throne [[Jigyel Ugyen Wangchuck]].<ref name=pre2016/> Karma was selected as the flag bearer for the [[2016 Summer Olympics opening ceremony|opening ceremony]] while Lenchu held it at the [[2016 Summer Olympics closing ceremony|closing]] ceremony.<ref>{{cite news|last1=Petersen|first1=Barry|last2=Herbert|first2=T. Sean|title=Bhutan's archers aim high for Olympic glory|url=http://www.cbsnews.com/news/bhutans-archers-aim-high-for-olympic-glory/|work=[[CBS News]]|date=7 August 2016|accessdate=5 February 2017}}</ref><ref>{{cite web|title=Rio 2016 Closing Ceremony - Flag Bearers|url=https://stillmed.olympic.org/media/Document%20Library/OlympicOrg/News/2016/08/2016-08-21-Rio-2016-Closing-Ceremony-Flag-Bearers.pdf|publisher=International Olympic Committee|date=21 August 2016|accessdate=5 February 2017}}</ref>

==Archery==
{{main|Archery at the 2016 Summer Olympics|Archery at the 2016 Summer Olympics – Qualification}}

[[File:Rio 2016 - Men's archery finals (29229294462).jpg|thumb|right|The [[Sambadrome Marquês de Sapucaí|Sambódromo]], where Karma competed in Archery events.]]

The 2016 Summer Games marked Karma's Olympic début.<ref name=previewarchery>{{cite web|last=Stanley|first=John|title=For Bhutan, Rio Hopes Rest With Karma (Literally)|url=https://www.archery360.com/2016/08/05/bhutan-rio-hopes-rest-karma-literally/|work=Archery 360|date=5 August 2016|accessdate=5 February 2017}}</ref> She qualified for the [[Archery at the 2016 Summer Olympics – Women's individual|women's individual archery]] tournament after receiving a [[Wild card (sports)|wildcard]] from the Tripitate Commission in June 2016 based on her results at the [[2015 Asian Archery Championships]] and the [[2016 Archery World Cup]] qualification event in [[Antalya]].<ref name=previewarchery/><ref name=wafinterview>{{cite web|last=Stanley|first=John|title=Karma: "Win or lose, I want to compete with champions|url=https://worldarchery.org/news/143720/karma-win-or-lose-i-want-compete-champions|publisher=[[World Archery Federation]]|date=15 August 2016|accessdate=5 February 2017}}</ref> In an interview with ''[[Kuensel]]'' before the Games, Karma said: "I’m very excited and nervous at the same time. It is time for me to show what I’ve learned so far against the world’s best archers.”,<ref>{{cite news|last=Tshedup|first=Younten|title=Women archers to lead Bhutan at Olympics|url=http://www.kuenselonline.com/women-archers-to-lead-bhutan-at-olympics/|work=[[Kuensel]]|date=10 July 2016|accessdate=5 February 2017}}</ref> and to ''Archer 360'', "While in the Sambodromo, I’m hoping to make my country proud. I have a good chance if I am able to maintain my home scores."<ref name=previewarchery/> She competed on 5 August in the ranking round, finishing 60th out of 64th competitors with 588 points. Karma scored 81 points less than the leading competitor, [[South Korea at the 2016 Summer Olympics|South Korea]]'s [[Choi Mi-sun]].<ref>{{cite news|title=Olympics-Archery-Women's individual ranking round results|url=http://www.reuters.com/article/olympics-archery-women-results-idUSISS645268|agency=[[Reuters]]|date=5 August 2016|accessdate=5 February 2017}}</ref> On 8 August she competed in the Round of 64 and was drawn against [[Tuyana Dashidorzhieva]] of [[Russia at the 2016 Summer Olympics|Russia]], the fifth ranking athlete in the ranking round.<ref name=archeryR64>{{cite web|title=Bhutan’s Rio 2016 Olympian|url=http://www.baf.org.bt/bhutans-rio-2016-olympian/|publisher=Bhutan Archery Federation|date=10 August 2016|accessdate=5 February 2017}}</ref> Karma tied with Dashidorzhieva in the first set before the Russian outscored her by three points to secure the second set. Karma and Dashidorzieva tied with each other in the game's following two sets until the Russian won the final set to win the match. This meant Karma was eliminated from the competition.<ref name=archeryR64/> After the events ended, Karma stated that despite being defeated she enjoyed the match and wanted to compete with archery champions whether she won or lost: "I remember being a bit nervous with the big cameras in the blinds. But I felt excited that it was the first time in my life that this would happen,"<ref name=wafinterview/>

{|class="wikitable" style="font-size:90%"
|-
!rowspan=2|Athlete
!rowspan=2|Event
!colspan="2"|Ranking round
!Round of 64
!Round of 32
!Round of 16
!Quarterfinals
!Semifinals
!colspan="2"|Final / {{abbr|BM|Bronze medal match}}
|- style="font-size:95%"
!Score
!Seed
!Opposition<br />Score
!Opposition<br />Score
!Opposition<br />Score
!Opposition<br />Score
!Opposition<br />Score
!Opposition<br />Score
!Rank
|- align=center
|align=left|[[Karma (archer)|Karma]]
|align=left|[[Archery at the 2016 Summer Olympics – Women's individual|Women's individual]]
|588
|60
|{{flagIOCathlete|[[Tuyana Dashidorzhieva|Dashidorzhieva]]|RUS|2016 Summer}}<br>'''L''' 3–7
|colspan=6|Did not advance
|}

==Shooting==
{{main|Shooting at the 2016 Summer Olympics|Shooting at the 2016 Summer Olympics – Qualification}}
[[File:Stand de tiro de 10 metros onde aconteceu a qualificação dos atletas do tiro esportivo.jpg|thumb|The [[National Shooting Center (Brazil)|National Shooting Center]], where Lenchu participated in shooting events.]]
Kunzang Lenchu also made her first appearance in the Olympics at the 2016 Games.<ref name=":0">{{cite news|url=http://www.kuenselonline.com/shooting-her-way-to-rio/|title=Shooting her way to Rio|last=Tshedup|first=Younten|date=15 May 2016|newspaper=|archive-url=|archive-date=|dead-url=|publisher=|work=Kuensel|accessdate=16 May 2016}}</ref> She qualified for the women's 10 metre air rifle shooting tournament after being awarded a wildcard place from the Tripartite Commission.<ref name=":1">{{Cite news|url=http://indianexpress.com/article/sports/sport-others/rio-2016-olympics-weekly-bulletin-on-indias-preparations-for-the-olympics-2849510/|title=Rio 2016 Olympics: Weekly bulletin on India’s preparations for event|last=|first=|date=13 June 2016|newspaper=[[The Indian Express]]|access-date=5 February 2017|archive-url=|archive-date=|dead-url=|work=|via=}}</ref> Lenchu spent time in India preparing for the Games because of a lack of shooting facilities in Bhutan.<ref name=":1" /> Before the Games she said that she was disappointed at finishing 12th at the [[2016 South Asian Games|South Asian Games]] in her contest and affirmed she would "do her best" at the Olympics.<ref name=":0" /> On 6 August Lenchu competed in the qualification round of the women's 10 metre air rifle. She finished 45th out of 51 athletes with a score of 404.9 points.She scored 16 points less than the highest-scoring competitor [[Du Li]] of [[China at the 2016 Summer Olympics|China]]. Lenchu scored two 11 points less than [[Yi Siling]] from China who was the lowest scoring qualifier and therefore her competition ended at the qualifying round.<ref>{{Cite web|url=http://www.espn.co.uk/olympics/summer/2016/results/_/discipline/37/event/93|title=2016 Summer Olympics – Results – Shooting – Women 10m air rifle|website=[[ESPN]]|archive-url=|archive-date=|dead-url=|access-date=6 February 2017}}</ref>
{| class="wikitable" style="font-size:90%"
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan=2|Qualification
!colspan=2|Final
|-style="font-size:95%"
!Points
!Rank
!Points
!Rank
|-align=center
|align=left|[[Kunzang Lenchu]]
|align=left|[[Shooting at the 2016 Summer Olympics – Women's 10 metre air rifle|Women's 10 m air rifle]]
|404.9
|45
|colspan=2|Did not advance
|}
<small>Qualification Legend: '''Q''' = Qualify for the next round; '''q''' = Qualify for the bronze medal (shotgun)</small>

==References==
{{reflist|30em}}

{{Nations at the 2016 Summer Olympics}}
{{Portal bar|2010s|Bhutan|Olympics}}

[[Category:Nations at the 2016 Summer Olympics]]
[[Category:Bhutan at the Summer Olympics by year|2016]]
[[Category:2016 in Bhutan]]