{{primary sources|date=September 2011}}
The '''Design Automation Conference''', or '''DAC''', is an annual event, a combination of a technical conference and a trade show, both specializing in [[electronic design automation]] (EDA). 
 
DAC is the oldest and largest conference in EDA, started in 1964. It grew out of the SHARE ("Society to Help Avoid Redundant Effort") design automation workshop. Its originators Marie Pistilli and Pasquale (Pat) Pistilli were honored by the EDA community. Pat received the highest honor in EDA industry, the [[Phil Kaufman Award]], for this effort<ref>[http://dsp-fpga.com/articles/looking-back-at-the-milestones-as-dac-50-approaches/ "Looking back at the milestones as DAC-50 approaches"], Mike Demler, March 19, 2013 (retrieved April 4, 2013)</ref> and Marie was honored by having an award established in her name, Marie R. Pistilli Women in EDA Achievement Award,<ref>[https://dac.com/content/women-electronic-design "Women in Electronic Design"]</ref> known as the Marie R. Pistilli Women in Engineering Achievement Award since 2016.<ref>[https://dac.com/sites/default/files/files/2016/53dac_wwed_nomination.pdf "2016 Marie R. Pistilli Women in Engineering Achievement Award Nominations"] (retrieved February 29, 2016)</ref>

Up until the mid-'70s, DAC had sessions on all types of design automation, including mechanical and architectural.  After that, for all intents and purposes, only topics concerned with electronic design have been included. Also until the mid-'70s, DAC was strictly a technical conference. Then a few companies started to request space to show their products, and within a few years the trade show portion of DAC became the main focus of the event. The first commercial DAC was held in June 1984. As a rough metric of the importance of the trade show portion, about 5,500 people attended DAC in 2005, whereas ICCAD, at least as strong technically but with no trade show, drew perhaps a tenth as many attendees.

Other similar conferences are the [[International Conference on Computer-Aided Design]], or ICCAD (technical only, no trade show), [[Design Automation and Test in Europe]] (DATE), [[Asia and South Pacific Design Automation Conference]] (ASPDAC), and International Symposium on Quality Electronic Design (ISQED).

Over the past few years the conference location has been alternating among [[San Diego]], [[Anaheim]], and [[San Francisco]].  The conference is usually held in June.

DAC is sponsored by several professional societies: ACM-SIGDA ([[Association for Computing Machinery]], [[Special Interest Group on Design Automation]]), EDAC ([[Electronic design automation|EDA]] Consortium), and IEEE-CEDA ([[Institute of Electrical and Electronics Engineers]], [[IEEE Council on Electronic Design Automation]]), in technical cooperation with IEEE-SSCS ([[IEEE Solid-State Circuits Society]]). DAC is organized by hundreds of volunteer committee members from EDA companies and academia.<ref>http://www2.dac.com/committees.aspx.</ref>

== See also ==
*[[electronic design automation]]
*[[:Category:Electronic design automation software|EDA Software Category]]
*[[International Conference on Computer-Aided Design]]
*[[Asia and South Pacific Design Automation Conference]]
*[[Design Automation and Test in Europe]]
*[[Symposia on VLSI Technology and Circuits]]

== References ==
{{Reflist}}
{{Refbegin}}
*[http://www.dac.com Main DAC web site]
{{Refend}}

== External links ==
* [http://www.dac.com/dac+2012.aspx DAC-2012]
{{IEEE conferences}}
[[Category:Electronic design automation conferences]]
[[Category:IEEE conferences]]
[[Category:International conferences in the United States]]
{{compu-conference-stub}}