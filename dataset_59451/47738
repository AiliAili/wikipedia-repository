The '''Entrevues Belfort film festival''' is an annual international film festival, founded in 1986 by Janine Bazin and held in [[Belfort]], France. Through its First Films International Competition (from 1st to 3rd film), the festival is dedicated to support emerging filmmakers. Alongside of the International Competition, the festival focuses on rediscovering filmmakers from the cinematic heritage with retrospectives, tributes and encounters.<ref>{{cite web|title=Entrevues Belfort - Official website|url=http://www.festival-entrevues.com/en/presentation}}</ref> It also offers a Post-production Grant for first international features films.<ref>{{Cite web|title=[Films en cours] presentation |url=http://www.festival-entrevues.com/en/films-en-cours-presentation |website=www.festival-entrevues.com |access-date=2016-01-18 |deadurl=yes |archiveurl=https://web.archive.org/web/20160113102123/http://www.festival-entrevues.com/en/films-en-cours-presentation |archivedate=2016-01-13 |df= }}</ref><br />
Since 1986, the festival has been successively directed by Janine Bazin (1986-2000), Bernard Benoliel (2001-2004), Catherine Bizern (2005-2012) and Lili Hinstin (since 2013).<ref>"Lili Hinstin, directrice de Belfort, [[Cahiers du cinéma]], n°691, July–August 2013, p.81</ref>
Many first films of now renowned young directors were selected by EntreVues Belfort, such as:
* [[Darren Aronofsky]]
* [[Alain Guiraudie]]
* [[Chen Kaige]]
* [[Abdellatif Kechiche]]
* Wang Chao
* [[Athina Rachel Tsangari]]
* Rabah Ameur-Zaimèche
* Angela Shanelec
* [[Patricia Mazuy]]
* [[Yousry Nasrallah]]
* [[Olivier Assayas]]
* [[Jean-Claude Brisseau]]
* [[João Botelho]]
* [[Laurent Cantet]]
*  [[Leos Carax]]
* [[Pedro Costa]]
* [[Idrissa Ouedraogo]]
* [[François Ozon]]
* [[Nicolas Philibert]]
* [[Paulo Rocha (film director)|Paulo Rocha]]
* [[Walter Salles]]
* Claire Simon
* [[Lars Von Trier]], and also, more recently, Laurent Achard, [[Brillante Mendoza]], Albert Serra, Sophie Letourneur or Tariq Teguia.<ref>[[Jacques Mandelbaum]], "Teguia fait tomber la foudre sur Belfort", ''[[Le Monde]]'', 6 December 2013</ref>
Since 1986, retrospectives have been dedicated to international directors such as: [[Michelangelo Antonioni]], [[David Cronenberg]], [[Michael Cimino]], [[Jerzy Skolimowski]], [[Barbet Schroeder]], [[Paul Schrader]], [[Milos Forman]], [[Marco Bellocchio]], [[Paul Verhoeven]], [[Abel Ferrara]], [[Jean-Claude Brisseau]], [[Jean-Pierre Mocky]], [[John Carpenter]],<ref>Julien Gester, "On reflet le match avec Carpenter", ''Liberation'', 27 November 2013</ref> [[Jacques Doillon]],<ref>Caroline Châtelet, "Doillon cinéaste obstiné", ''Novo'', December–January 2014</ref> [[Kiyoshi Kurosawa]], [[Otar Iosseliani]] or [[Bong Joon-ho]].<ref>{{Cite web|title = EastAsia » Retour sur les courts-métrages de Bong Joon-ho (Festival EntreVues Belfort)|url = http://eastasia.fr/2015/12/19/retour-sur-les-courts-metrages-de-bong-joon-ho-festival-entrevues-belfort/|access-date = 2016-01-18}}</ref>

In 2015, the festival has celebrated its 30th edition.<ref>{{Cite web|title = Cinema and the Class Struggle on Notebook|url = https://mubi.com/notebook/posts/cinema-and-the-class-struggle|website = MUBI|access-date = 2016-01-18}}</ref>

== Awards ==

'''Grand Prix Janine Bazin - Best Feature Film'''
* 2015: ''Ben Zaken'' by Efrat Corem (Israel, 90 min)
* 2014: ''I am the people (Je suis le peuple''), by Anna Roussillon (France, 111 min)
* 2013: ''Revolution Zendj'', by Tariq Teguia (Algeria - France - Lebanon - Qatar, 137 min)
* 2012: ''[[Leviathan (2012 film)|Leviathan]]'', by Verena Paravel and [[Lucien Castaing-Taylor]] (UK - France - USA, 87 min)
* 2011: ''L'Estate di Giacomo'', by Alessandro Comodin (France - Italia - Belgium, 78 min)

'''Grand Prix Best Short Film'''
* 2015: ''Antonio, lindo Antonio'' by Ana Maria Gomes (France, 42 min)
* 2014: ''A Tale'', by Katrin Thomas (Germany, 14 min)
* 2013: ''Peine perdue'', by Arthur Harari (France, 38 min)
* 2012: ''Vilaine fille mauvais garçon'', by Justine Triet (France, 30 min)
* 2012 Mention: ''Ovos de dinossauro na sala de estar'', by Rafael Urban (Brazil, 12 min)
* 2011: ''Drari'', by Kamal Lazraq (France, 41 min)

'''Distribution Support Award'''
* 2015: ''Bienvenue à Madagascar'' by Franssou Prenant (France, 102 min)
* 2014: ''[[The Mend (film)|The Mend]]'', by John Magary (USA, 111 min)
* 2013: ''[[See You Next Tuesday (film)|See You Next Tuesday]]'', by Drew Tobia (USA, 82 min)

'''One + One Award'''
* 2015: ''Bienvenue à Madagascar'' by Franssou Prenant (France, 102 min)
* 2014 :''Hillbrow'', by Nicolas Boone (France, 32 min)
* 2013: ''Juke Box'', by Ilan Klipper (France, 23 min)
* 2012: ''[[Leviathan (2012 film)|Leviathan]]'', by Verena Paravel and [[Lucien Castaing-Taylor]] (UK - France - USA, 87 min)
* 2011: ''Le Sommeil d'or'', by Davy Chou (France - Cambodia, 96 min)
* 2011 Mention: ''Le Marin masqué'', by Sophie Letourneur (France, 36 min)

'''CAMIRA (Cinema And Moving Images Research Assembly) Award''' :
* 2015: ''Roundabout in my head'' by Hassen Ferhani (Documentary, Algeria - France - Qatar - Libanon - Netherlands, 100 min)
* 2014 :''Hillbrow'', by Nicolas Boone (France, 32 min)

'''Audience Feature Film Award'''
* 2015: ''Roundabout in my head'' by Hassen Ferhani (Documentary, Algeria - France - Qatar - Libanon - Netherlands, 100 min)
* 2014: ''Je suis le peuple'', by Anna Roussillon (France, 111 min)
* 2013: ''Round Trip'', by Meyar Al-Roumi (France - Syria, 74 min)
* 2012: ''Everybody In Our Family (Papa vient dimanche)'', by Radu Jude (Romania, 108 min)
* 2011: ''Louise Wimmer'', by Cyril Mennegun (France, 80 min)

'''Audience Short Film Award'''
* 2015: ''Antonio, lindo Antonio'' by Ana Maria Gomes (France, 42 min)
* 2014 :''Hillbrow'', by Nicolas Boone (France, 32 min)
* 2013: ''Être vivant'', by Emmanuel Gras (France, 16 min)
* 2012: ''Vilaine fille mauvais garçon'', by Justine Triet (France, 30 min)
* 2011: ''Un Monde sans femmes'', by Guillaume Brac (France, 58 min)

'''Documentaire sur Grand Ecran Award''' (until 2012)
* 2012: ''[[Leviathan (2012 film)|Leviathan]]'', by Verena Paravel and [[Lucien Castaing-Taylor]] (UK - France - USA, 87 min)
* 2011: ''L'Eté de Giacomo'', by Alessandro Comodin (France - Italia - Belgium, 78 min)

'''French Film Award''' (until 2012)
* 2012: ''Ma Belle gosse'', by Shalimar Preuss (France, 80 min)
* 2011: ''Dernière séance'', by Laurent Achard (France, 78 min)
* 2011 Mention: ''OK, Enough, Goodbye'', by Rania Attieh and Daniel Garcia (Lebanon, 93 min)

'''Janine Bazin Award for Best Actor/Actress''' (until 2012, then it became the ''Grand Prix Janine Bazin - Best Feature'')
* 2012: Mihaela Sirbu in ''Everybody In Our Family (Papa vient dimanche)'', by Radu Jude (Romania, 108 min)
* 2011: Laure Calamy in ''Un Monde sans femmes'', by Guillaume Brac (France, 58 min)
* 2011: Daniel Arzrouni in ''OK, Enough, Goodbye'', by Rania Attieh and Daniel Garcia (Lebanon, 93 min)

== References ==

{{reflist}}

== External links ==
* [http://www.festival-entrevues.com/en Entrevues Belfort International Film Festival - Official Website]

[[Category:Film festivals in France]]