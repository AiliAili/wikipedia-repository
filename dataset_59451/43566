{|{{Infobox Aircraft Begin
 | name=Grunau 9
 | image=PH-94 Grunau 9, ESG.jpg
 | caption=Later version with vertical backrest strut within the A-frame
}}{{Infobox Aircraft Type
 | type=Single  seat [[primary glider]]
 | national origin=[[Germany]]
 | manufacturer=Edmund Schneider, Grunau (ESG) 
 | designer=[[Edmund Schneider]]
 | first flight=1928
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Djävlar Anamma]]
 | variants with their own articles=
}}
|}

The  '''ESG Grunau 9''', later known as the '''ESG 29''' and post-1933 as the '''DFS 108-10''', was one of the first [[primary glider]]s, built in Germany from the late 1920s.  It was widely sold.

==Design and development==
The Grunau 9 was a German single seat [[training aircraft|trainer glider]], one of the first of a group that later became known as [[primary glider]]s.  It was developed by [[Edmund Schneider]] from [[Alexander Lippisch]]'s [[Djävlar Anamma]] ({{lang-de|Hols der Teufel}}, {{lang-en|to the Devil with it}}) via the [[Espenlaub]] primary. The Grunau 9 was produced in numbers and was sold widely.<ref name=SimonsI/>[[File:Grunau G 9 3.jpg|thumb|In front of the Werfthalle Grunau]][[File:Grunau G 9 1.jpg|thumb|alt=|After crash landing]]
[[File:Grunau 9 001.jpg|alt=|thumb|Grunau 9 with streamlined fairings]]
The core of the flat frame [[fuselage]] was formed with a horizontal beam about {{convert|2|m|ftin|abbr=on}} long, to which two other  converging [[strut]]s were attached, making overall a vertical A-frame. The downward sloping extremities of these beams carried a slightly deeper horizontal box structure below the cross beam, with the open pilot's seat and controls upon it. On some later aircraft there was an extra vertical member for the lower cross beam to the wing root to provide the pilot with a backrest.  Others enclosed him or her in a simple, light, short nacelle between the nose and the backrest strut. The rear part of the fuselage frame was based on two longer beams reaching to the tail.  The upper one was horizontal and attached to both converging A-frame beams, near but not at its apex. The lower one sloped upwards and was attached to the rear sloping part of the A-frame just below the cross-member.  These two rear fuselage beams were cross braced with three more struts, one vertical about halfway to the tail, forming two bays which were crossed diagonally by the other two struts.  There was another, short vertical strut in the rear bay between the upper and diagonal members.  For landings a skid ran between three projecting ends of the forward and lower A-frame.<ref name=SimonsI/>

The Grunau 9 had almost rectangular, two spar, wooden structured, two piece wings with [[aircraft fabric covering|fabric covering]] everywhere except the [[leading edge]]s, which were [[plywood]] covered. Short, simple rectangular, cropped [[ailerons]] reached to the square [[wing tip]]s.  They were attached to the upper fuselage beam with their leading edges at the forward sloping member and a [[chord (aircraft)|chordwise gap]] between their roots.  Each wing was braced with a pair of [[landing wires]] from the apex of the A-frame to the upper wing at outboard points on the forward and aft spars and by pairs of [[flying wire]]s from  below the wing to the lower horizontal A-frame member. There were also bracing wires from the wing rear spars to the tail to retrain its lateral movement.  The vertical [[rudder]] hinge was at the end of the fuselage, with a rudder that was rectangular apart from its sloping lower edge.  A triangular tailplane was mounted on the upper, horizontal fuselage beam with the [[elevator (aircraft)|elevator]] hinge in line with the rudder's.  The rectangular elevators therefore required a cut-out for rudder movement; like the rudder and tailplane, the elevators were fabric covered.  A [[fin]] was provided by fabric covering the near triangular area of the rear fuselage between the rudder hinge, the upper and lower beams and the diagonal between them.<ref name=SimonsI/>

The Grunau 9 first flew in 1928.  The following year, Schneider made changes to the tail and introduced a new and (it turned out) temporary naming convention involving the year, redesignating it as the ESG 29, though it was not a unique name.<ref name=SimonsI/>   After its formation in 1933, the [[Deutsche Forschungsanstalt für Segelflug]] (DFS) gave it the type number DFS 108-10.<ref name=Ogden/> The positioning of a wooden strut immediately in front of the pilot's head led to the type being nicknamed the ''Schädelspalter'', or skullsplitter.<ref>
[http://martinsimons.com.au/other-writing/the-skullsplitter Martin Simons: The Skullsplitter]</ref> Large numbers were built and sold widely over several years.<ref name=SimonsI/> At least one [[Netherlands|Dutch]] registered Grunau 9 remained active after [[World War II]].

==Aircraft on display==
From:Aviatiom Museums and Collections of Mainland Europe (2009)<ref name=Ogden/>
*[[Finnish Aviation Museum]], [[Helsinki]]: ''G-36''
*[[Icelandic Aviation Museum]], [[Akureyri]]: unmarked. Built 1938, still airworthy but last flown June 2004.<ref name=FlugMus/>
*[[Norwegian Aviation Museum]], [[Bodø]]: Grunau 9 ''LN-GAH''
*[[Segelflyg Museum]], [[Falköping]]:''SE-27''

These are original Grunau 9s.  Other museums worldwide have originals not on public display, others have reproductions.

==Specifications (1930 model)==
{{Aircraft specs
|ref=Sailplanes (2006) (apart from span and length, from 1930 Schneider catalogue)<ref name=SimonsI/> 
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=5.55
|length note=
|span m=10.78
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=16.06
|wing area note=
|aspect ratio=7.2
|empty weight kg=86
|empty weight note=
|gross weight kg=150
|gross weight note=weights approximate
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=10:1<ref name=WasserF/>
|sink rate ms=1.3
|sink rate note=at {{convert|15|km/h|mph  kn|abbr=on|0}}<ref name=WasserF/>
|lift to drag=
|wing loading kg/m2=9.3
|wing loading note=

|more performance=
}}

==See also==
*[[Hanna Reitsch]]

==References==
{{Reflist|refs=

<ref name=SimonsI>{{cite book |title=Sailplanes 1920-1945 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9806773 4 6|pages=38–40}}</ref>

<ref name=Ogden>{{cite book |title=Aviation Museums and Collections of Mainland Europe |last= Ogden |first=Bob |pages=98, 303, 398, 558 |edition= |year=2009|publisher= Air Britain (Historians) Ltd|location= |isbn=978 0 85130 418 2}}</ref>

<ref name=FlugMus>{{cite web |url=http://flugsafn.is/index.php?option=com_content&view=article&id=80&Itemid=143&lang=en |title=Grunau 9 in the Icelandic Aviation Museum|accessdate=2 February 2014}}</ref>

<ref name=WasserF>{{cite web |url=http://www.segelflugmuseum.de/grunau_9__esg_29_4119.html?psid=779a0e5098aee6e668e873ddb2bb879b |title=Grunau 9 im Segelflugmuseum|accessdate=2 February 2014}}</ref>

}}

[[Category:German sailplanes 1920–1929]]
[[Category:Glider aircraft]]