'''Anne Ferran''' (born 1949) is an Australian photographer.

==Background==

Anne Ferran was born on May 10, 1949<ref name="c1">Newton, Gael (1988). Shades of Light. Canberra: Australian National Gallery. p. 157. ISBN 0642081522.</ref> in Sydney, New South Wales.<ref name="c3">Judd, Craig. Anne Ferran's birds in space Photofile, No. 94, Autumn/Winter 2014: [63]-70.</ref> Ferran began exhibiting her photographic work in the early 1980s.<ref name="c10">The University of Sydney 2014, Associate Professor Anne Ferran, viewed 20 April 2015, <sydney.edu.au/sca/about/people/profiles/anne.ferran.php</ref>

In 1986 she relocated to Europe after being awarded a Visual Arts Board travel grant from an Australian committee.<ref name="c10"/> She then took up a six-month residency at the Power Studio Cité Internationale des Arts in [[Paris]].<ref name="c8"/> Ferran returned from overseas to [[Sydney]] to complete her Masters of Fine Arts, but then shifted to [[Melbourne]] in 1995 only a year after graduating. In 2003 she received a residency in [[London]] from the Australian Council.<ref name="c22second">Press Release 2003, 1-38, October 15 – November 15, Stills Gallery, Paddington NSW Australia.</ref> Ferran currently lives in Sydney and is employed as an associate professor at the [[University of Sydney]].<ref name="c10"/>

==Education==
Ferran has a BA from the University of Sydney, a BA from [[Sydney College of the Arts]] (1985), and an MFA from the College of Fine Arts, [[University of New South Wales]].<ref name="c10"/>

==Career==

Ferran was first recognized as a contemporary photographic artist during the 1980s <ref name="c10"/> due to her works: Carnal Knowledge and Scenes on the Death of Nature.<ref name="c10"/> As well as film and digital photography, Ferran uses a variety of different medias such as videography and a series of textile works.<ref name="c10"/> Her photographs have been exhibited both across Australia and internationally.<ref name="c10"/> The Art Gallery of New South Wales, Art Gallery of South Australia, Monash University, National Gallery of Australia, National Gallery of Victoria and Queensland Art Gallery have all featured Ferran’s work.<ref name="c10"/> Internationally, Ferran’s exhibits have been displayed in three different countries: New Zealand, Japan and the United States of America.<ref name="c10"/>

Ferran’s work is motivated by the Australian colonial period; her main interest involves exploring the lives of nameless women and children.<ref name="c10"/> Later in her life Ferran’s interests have extended to histories of birds and the way that their natural environments are changing.<ref name="c10"/>

==Notable works==

===Scenes on the Death of Nature (1986)===

Scenes on the Death of Nature toured Australia from 1986 until 2006. It first stopped at The Australian Centre for Contemporary Art in South Bank, from March 19, 1987 until April 19, 1987. After this brief stint in Melbourne the exhibition was moved to the Art Gallery of New South Wales in Sydney, from February 10, 1989, until May 15, 1989. It returned to the Art Gallery of New South Wales in 1993 once more for the exhibition entitled ‘Points of view: Australian Photography 1985-95’.<ref name="c4">Australian Photography: The 1980s, Anne Ferran, Australian National Gallery, 1988</ref>
The exhibition consisted of five large (148.5&nbsp;cm x 109.5&nbsp;cm) <ref name="c4"/>  black and white prints of young women draped across one another.<ref name="c5" >{{ cite news |last=Green |first=J |date=25 March 1987 |title=Escape to the world of subtlety|publisher=Melbourne Times|url=https://www.accaonline.org.au/sites/default/files/1987_25_March_Melbourne%20Times_Review.pdf}}</ref> The women were dressed in long flowing plain white garments with stony facial expressions in order to recreate the appearance of a neoclassical sculpture.<ref name="c6">) Frost, A 2014, 'Anne Ferran: Shadow Land – Review', The Guardian, viewed 20 April 2015, < https://www.theguardian.com/culture/australia-culture-blog/2014/feb/19/anne-ferran-shadow-land-review</ref> All of the images in Scenes of death of nature contain elements of feeling imprisoned; the black frames that house the photographs themselves are representative of the square wrought iron bars used in prisons and institutions during 19th century.<ref name="c10"/> It focused on the mistreatment of incarcerated colonial women.<ref name="c8">Australian National Gallery 1988, Australian Photography: The 1980s, Australian National Gallery, Canberra ACT</ref>

===Carnal Knowledge 1984===

Ferran created Carnal Knowledge whilst residing in the Hyde Park Barracks in Sydney in 1995 with acclaimed artist Anne Brennan; the barracks were formerly a convict barracks and a female asylum.<ref name="c9">Art Gallery NSW nd, [http://www.artgallery.nsw.gov.au/collection/works/186.1986.6/ Carnal Knowledge], Art Gallery NSW, viewed 20 April 2015</ref> Carnal Knowledge was heavily influenced by historical events that had occurred in the barracks to those who had lived there. Ferran displayed artifacts that had been recovered from underneath the barracks floorboards in her exhibition; using these items to evoke an understanding for the women who had lived and died in the asylum. These artifacts were also accompanied by a series of haunting photographs that are representative of young women who were residents in the Asylum. Ferran has since acknowledged the fact that the Hyde Park Barracks has altered her future artistic approach, as she is now able to appreciate the beauty in empty spaces.<ref name="c13">Barrett, J, & Miller, J 2014, Australian Artists in the contemporary museum, Ashgate Publishing Company, Surrey.</ref> Ferran used her own daughter and friends as subjects in this project in an attempt to add a maternal component to the photographs.<ref name="c9"/>

Dealing with the act of sex, and while the series of 13 images are heavily sexualised, the images do not contain any nudity. The images are close ups of emotionless faces, which have been given the effect of weather work stone to create the appearance of the passing of time.<ref name="c9"/> The exhibition was on show at the Art Gallery of New South Wales in Sydney alongside other photographic series for the Australian Perspecta exhibition in 1985. It returned again to the Art Gallery of New South Wales in July 1999 for the exhibition ‘What is this thing called photography?’<ref name="c9"/>

===Lost to worlds 2008===

Lost to worlds includes over a decade's worth of photographic work.  The project was undertaken in Tasmanian at the remnants of two female convict prisons sites.  The prisons were situated in the center of Tasmanian on the border of the small town of Ross;<ref name="c10"/> all that remains of the prisons today are piles of dirt and a mess of stones. The images themselves are dominated by the landscape, barely giving the viewer any other perspectives, only occasionally offering a sight of the horizon or sky.  The six images in the series are digitally printed (120&nbsp;cm x 120&nbsp;cm) <ref name="c22">Sutton gallery 2015,exhibitioninfo, Anne ferran, [http://www.suttongallery.com.au/exhibitions/exhibitioninfo.php?id=66 lost to worlds], Fitzroy VIC AUSTRALIA</ref> onto sheets of aluminium and set up so that when viewers move around the gallery observing the photographs, the reflections from the aluminium give the images an element of blurred motion.<ref name="c11">Stills Gallery 2015, Anne Ferran - Lost to worlds, Stills Gallery, viewed 23 April 2015, <http://www.stillsgallery.com.au/exhibitions/2009/index.php?obj_id=ferran</ref> Ferran uses the large empty field to find elegance through the stillness in this series of visually confronting images, the series is symbolic of a fragmented past.<ref name="c10"/>

===Other exhibitions===

*Box Of Birds                                 
*Female House Of Correction
*Birds Of Darlinghurst                
*Rydalmere Vertical            
*Songbirds Are Everywhere         
*Soft Caps
*Lost To Worlds 2008                   
*Scenes On The Death Of Nature
*Backwater                                     
*Untitled Photograms
*Twice Removed                            
*Insula
*1-38

==References==
{{reflist}}

{{authority control}}

{{DEFAULTSORT:Ferran, Anne}}
[[Category:Artists from Sydney]]
[[Category:1949 births]]
[[Category:Living people]]
[[Category:Australian women artists]]
[[Category:Australian women photographers]]
[[Category:University of Sydney alumni]]
[[Category:University of New South Wales alumni]]
[[Category:University of Sydney faculty]]
[[Category:20th-century Australian artists]]
[[Category:20th-century women artists]]
[[Category:21st-century Australian artists]]
[[Category:21st-century women artists]]