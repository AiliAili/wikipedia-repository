{{Infobox military person
|name=Alexandru Şerbănescu
|birth_date={{Birth date|1912|5|17|df=yes}}
|death_date={{Death date and age|1944|8|18|1912|5|17|df=yes}}
|birth_place=[[Colonești, Olt|Coloneşti]], [[Olt County]]
|death_place=[[Viperești|Ruşavăţ]], [[Buzău County]]
|image=Serbanescu.jpg
|caption=Alexandru Şerbănescu 
|nickname=''Alecu''
|allegiance={{flagicon|Romania}} [[Kingdom of Romania]] 
|serviceyears=1933-1942 (Army)<br>1942-1944 (Air Force)
|rank=[[Captain (land and air)|Captain]] 
|branch=[[Romanian Army]]<br>[[Romanian Air Force]]
|commands=57th Fighter Squadron <br/> [[9th Fighter Group]] 
|unit=[[7th Fighter Group]]
|battles=[[World War II]]
*[[Eastern Front (World War II)|Eastern front]]
|awards=[[Order of Michael the Brave]] <br/> [[Order of the Star of Romania]] <br/> [[Iron Cross]]
|laterwork=
}}

'''Alexandru "Alecu" Şerbănescu''' (17 May 1912 in [[Colonești, Olt|Coloneşti]], [[Olt County]] &ndash; 18 August 1944 in [[Viperești|Ruşavăţ]], [[Buzău County]]) was a leading [[Romania]]n [[Fighter aircraft|fighter]] pilot and [[flying ace]] in [[World War II]]. In the spring of 1942 he was assigned as a [[fighter pilot|pilot]] to the 7th Fighter Group,<ref>{{ro icon}} Tudor, p. 14</ref> which fought with the German forces against the Soviet Union on the [[Eastern Front (World War II)|Eastern Front]], including [[Battle of Stalingrad|Stalingrad]]. He flew mostly [[IAR-80]] and [[Messerschmitt Bf-109|Messerchmitt Bf-109]] fighters.

==Early military career==
He graduated from the Military College at [[Târgovişte]] and the [[Infantry]] Officers School at [[Sibiu]] in 1933 and was soon appointed the commander of the 3rd [[Vânători de Munte|Mountain Troops]] battalion, located at Braşov. In 1942, he joined the Air Force Flight School in [[Ghimbav]] and became soon a fighter pilot within the 7th Fighter Group.<ref name="Timpul">{{ro icon}} [http://www.timpul.mdl.net/Rubric.asp?idIssue=303&idRubric=4183 ''Şoimii Grupului Şerbănescu'', Şerbănescu's Eagles], [[Timpul]], May 4, 2006. Retrieved on February 20, 2008.</ref>

==Eastern front==
The 7th Fighter Group was detached in 1942 to the [[Battle of Stalingrad|Stalingrad front]]. Şerbănescu distinguished himself in the fierce battles during the retreat from the airfields around Stalingrad. When [[Soviet Union|Soviet]]s broke the German and Romanian defenses in November 1942 and approached the Romanian airfield where the 7th Fighter Group was stationed, Şerbănescu successfully organized the defense of the airbase against the Allied forces, helped by his infantry experience. He had at his disposal only two [[anti-aircraft]] guns (one [[Rheinmetall]] 37mm and one 75mm [[Vickers]]-Reşiţa gun), the 20mm guns on the Bf-109Es and a company of ill-equipped and trained soldiers. The Romanians' camouflaged positions and well-led defense stopped the Soviet tanks attacking the airfield for two days. The Bf 109's 20&nbsp;mm guns were used as [[antitank]] weapons on the ground (by lifting the airplane's tail on barrels), this being a unique case of airplane-tank duel. On November 23, 1942 the Romanians evacuated eight Bf-109E (another 3 were lost while they tried to take off under fire). Each airplane carried two or three people in the cockpit. After this, what remained of Şerbănescu's unit was stationed on the [[Morozovsk]]aya airfield and was soon withdrawn to Romania for rest and recuperation.<ref name="Nitu">{{en icon}} [http://www.elknet.pl/acestory/serban/serban.htm ''Alexandru Şerbănescu - from Infantry to Aviation''], Victor Niţu, Octavian Ghiţă and Dariusz Tyminski, April 29, 2001. Retrieved on 21 February 2008.</ref>

==Back to Romania==
<!-- Deleted image removed: [[File:Alexandru Serbanescu.jpg|thumb|right|Alexandru Şerbănescu (second from right), together with his squadron comrades {{Deletable image-caption|date=May 2012}}]] -->
On March 29, 1943, Şerbănescu was appointed commander of the 57th Fighter Squadron, equipped with the new Messerschmitt Bf-109G, and promoted to the rank of [[Captain (land and air)|Captain]]. Between June and August 1943 he shot down 28 Allied aircraft, and received the highest Romanian [[military decoration]], [[Order of Michael the Brave]], 3rd Class. On October 23, the 9th Fighter Group replaced the battle-exhausted 7th Fighter Group, but Şerbănescu and the other aces remained. He kept fighting and shooting down airplanes of the Allies and, as a result, he was named the Group's commander on February 13, 1944. In May 1944 the Red Army entered Romania and occupied northern [[Bessarabia]] and northern [[Moldavia]], but they were stopped after some fierce fighting (''see also: [[Second Battle of Târgu Frumos|Battle of Târgul Frumos]]''), in which the pilots also played a very important role. On June 11, Şerbănescu shot down his first [[USAAF]] aircraft, a [[B-17 Flying Fortress]], his 45th victory. This was followed by two Allied [[P-51 Mustang]]s shot down on July 31 and on August 4 respectively, which were his last kills.<ref name="Foundation">{{ro icon}} [http://club.neogen.ro/FUNDATIAErouCapAviatorAlexandruSerbanescu/biografie-alexandru-serbanescu/58118/2 Alexandru Şerbănescu Foundation], January 10, 2008. Retrieved on 21 February 2008.</ref>

==Death==
On August 18, 1944, Alexandru Şerbănescu took off on his last mission. On that day, he and his twelve wingmen, together with twelve other fighters from the 9th Fighter Group, attacked a swarm of Mustangs and Lightnings. When [[Lieutenant]] Dobran and [[Adjutant]] Dârjan tried to clear his tail, it was too late. His last words were: "My boys, I'm going down...". Apparently his radio wasn't functioning properly and he could not hear his wingmen's warnings. Following Şerbănescu's death, all Romanian fighters were issued orders to refrain from engaging the Americans until a new strategy would be adopted. Five days later, on August 23, 1944, a [[King Michael's Coup|coup d'état led by King Michael of Romania]] deposed Marshal [[Ion Antonescu]] and Romania switched to the Allied side.<ref name="Patrascu" />

During his entire piloting career, he was credited with 47 confirmed victories (and 8 probable) in aerial combat which, with eight unconfirmed, yielded 55 points in the Romanian scoring system, second only to [[Constantin Cantacuzino (aviator)|Constantin Cantacuzino]] with 69.<ref name="Patrascu" />

==Legacy==
Today, a boulevard in [[Bucharest]] bears his name and passes very close to [[Aurel Vlaicu Airport]]. Each year, on 18 August, veterans, air force officers and aviation enthusiasts gather at his tomb to commemorate him. On August 18, 2004, the [[30th Honor Guard Regiment (Romania)|30th Honor Guard Regiment]] commemorated, with military honors, the sixty years that had passed since Capt. Şerbănescu was killed in action.<ref name="Patrascu">{{en icon}} [http://www.worldwar2.ro/arr/p006.htm ''Cpt. av. Alexandru Şerbănescu''], Bogdan Patrascu, Unofficial site. Retrieved on February 21, 2008.</ref>

On December 1, 2006, the [[RoAF 95th Air Base|95th Air Base]] of the [[Romanian Air Force]] received the honorific title ''Cpt. Av. Alexandru Şerbănescu''.<ref>{{ro icon}} [http://www.event365.ro/Baza_Aeriana_Bacau_a_primit_numele_pilotului_Alexandru_Serbanescu-n41058.html ''Bacău Air Base received Alexandru Şerbănescu honorific name''], event365.ro, December 3, 2006. Retrieved on 21 February 2008.</ref>

==See also==
{{commons category|Alexandru Șerbănescu}}
*[[List of World War II flying aces from Romania]]
*[[Horia Agarici]]
*[[Constantin Cantacuzino (aviator)|Constantin Cantacuzino]]
*[[Romanian Air Force]]

==References==

===Notes===
{{reflist}}

===Further reading===
*Dénes Bernád, ''Rumanian Aces of World War 2'', [[Osprey Publishing]], [[Botley, Oxfordshire]], 2003 ISBN 1-84176-535-X
*Vasile Tudor, ''Un nume de legenda - Cpt. av. erou Alexandru Şerbănescu'' (A legendary name, Captain Alexandru Şerbănescu), Editura MODELISM, 1998.
*Ion Bucurescu, ''Aviaţia Română pe Frontul de Est şi în apărarea teritoriului Vol. I; II'' (Romanian Aviation on Eastern Front and defending the national territory), Editura Fast Print, 1994.

{{DEFAULTSORT:Serbanescu, Alexandru}}
[[Category:1912 births]]
[[Category:1944 deaths]]
[[Category:Romanian military personnel killed in World War II]]
[[Category:Recipients of the Order of Michael the Brave]]
[[Category:Romanian Air Force officers]]
[[Category:Romanian World War II flying aces]]