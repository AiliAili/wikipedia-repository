{{Infobox person
| name                  = Vanessa Cantave
| image                 =
| image_size            = 
| caption               = 
| birth_name            = Vanessa Cantave
| birth_date            = {{Birth date and age|1977|10|7}}
| birth_place           = [[Washington, D.C.]]
| cooking_style         = [[New American cuisine|New American]], [[French cuisine|French]]
| education             = [[James Madison University]]<br />[[French Culinary Institute]]
| occupation            = Co-founder and executive chef of Yum Yum Catering and Events, entertaining expert
| years_active          = 2005–present
| website               = {{url|http://www.yumyumchefs.com}}
}}

'''Vanessa Cantave''' (born October 7, 1977) is an American [[chef]]. She is the co-founder and executive chef of the catering company Yum Yum Catering and Events in [[DUMBO, Brooklyn]].

==Early life and education==

Vanessa Cantave was born in [[Washington, D.C.]], in 1977. She is of [[Haiti]]an descent.<ref name="bravotv">DiSpirito, Rocco. [http://www.bravotv.com/roccos-dinner-party/season-1/blogs/rocco-dispirito/bon-appetit "Bon Appetit"], ''[[Rocco's Dinner Party]]'' Blog, 21 July 2011. Retrieved on 21 August 2012.</ref> Her father served in the [[United States Army]], which resulted in Cantave's moving around a lot as a child.<ref>Jamison, Charles N., Jr., [http://www.yumyumchefs.com/images/UPTOWN_FEB09.pdf "Yum Yum Chefs"], ''[[Uptown (magazine)|Uptown]]'', New York, February/March 2009. Retrieved on 21 August 2012.</ref> She lived in [[Mons, Belgium]], until the age of six. Her family then returned to the U.S. and lived in various places including [[Arizona]], suburban [[Chicago]], and northern [[Virginia]].

Cantave graduated from [[James Madison University]] in [[Harrisonburg, Virginia|Harrisonburg]], Virginia, in 1999. She earned her [[B.A.]] in [[political science]] with a [[Academic minor|minor]] in French.<ref name="vanessacantave">[http://www.vanessacantave.com "Meet Vanessa"], Retrieved on 21 August 2012.</ref>

==Career==

After completing her degree, Cantave worked in several positions in [[banking]], [[marketing]] and [[advertising]]. She worked at the [[Federal Deposit Insurance Corporation]] in Washington, D.C, and later the [[Federal Reserve]] in [[Atlanta, Georgia]]. She then moved to [[New York City]] and accepted a position at the [[Ogilvy & Mather]] agency. In 2005, Cantave attended an open house for the [[French Culinary Institute]], where she says she “fell in love.” She decided to quit her job and pursue her passion for food.<ref>[http://video.foxbusiness.com/v/1064706196001/would-you-leave-wall-st-to-start-a-business/ "Small Business Spotlight" Interview], [[FOX Business Network]], 19 July 2011. Retrieved on 21 August 2012.</ref>

Cantave began attending the French Culinary Institute (FCI) in 2005. While attending FCI, she interned at the Daniel restaurant in [[Manhattan|Manhattan's]] upper east side, where she studied under renowned French chef [[Daniel Boulud]].<ref name="sohautestyle">Gibbons, Nicole. [http://www.sohautestyle.com/?s=vanessa+cantave&x=0&y=0 "Yum Yum Chefs"], ''So Haute'', 24 February 2009. Retrieved on 21 August 2012.</ref> After graduating as the [[valedictorian]] of her class, she began working as a private chef in New York City and shortly thereafter co-founded her own catering company, Yum Yum Chefs, Inc. (later changed to Yum Yum Catering and Events).<ref name="sohautestyle" /> Based in DUMBO, Brooklyn, Yum Yum offers catering services for private clients and corporations and themed cooking classes.<ref name="vanessacantave" />

==Television and press==

In 2011, Cantave won the [[Bravo (U.S. TV channel)|Bravo]] [[Reality television|reality]] cooking competition ''[[Rocco's Dinner Party|Rocco’s Dinner Party]]''.<ref name="bravotv" /> She has appeared on a number of other television programs, including [[NBC]]’s ''[[The Today Show]]''.<ref>[http://today.msnbc.msn.com/id/47250634/ns/today-food/t/try-sauted-chicken-thighs-spring-peas/ Cantave shared three recipes on the Today Show that were later posted on Today's website]</ref>

==References==
{{reflist}}

==External links==
* [http://www.yumyumchefs.com/ Official website]

{{DEFAULTSORT:Cantave, Vanessa}}
[[Category:Living people]]
[[Category:1977 births]]
[[Category:American chefs]]
[[Category:American people of Haitian descent]]
[[Category:James Madison University alumni]]