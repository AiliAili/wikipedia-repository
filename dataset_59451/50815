{{Use mdy dates|date=February 2013}}
{{Infobox journal
| title = CA: A Cancer Journal for Clinicians
| cover = CA - A Cancer Journal for Clinicians cover image.jpg
| editor = [[Otis Brawley|Otis Webb Brawley]], Ted Gansler
| discipline = [[Oncology]]
| former_names =
| abbreviation = CA: Cancer J. Clin.
| publisher = [[Wiley-Blackwell]] for the [[American Cancer Society]]
| country =
| frequency = Bimonthly
| history = 1950–present
| openaccess = Yes
| license =
| impact = 131.723
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.3322/(ISSN)1542-4863
| link1 = http://onlinelibrary.wiley.com/journal/10.3322/(ISSN)1542-4863/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.3322/(ISSN)1542-4863/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 1044790
| LCCN = 55030061
| CODEN = CAMCAM
| ISSN = 0007-9235
| eISSN = 1542-4863
}}
'''''CA: A Cancer Journal for Clinicians''''' is a bimonthly [[Peer review|peer-reviewed]] [[medical journal]] published for the [[American Cancer Society]] by [[Wiley-Blackwell]]. The journal covers aspects of [[cancer]] research on [[diagnosis]], [[therapy]], and [[Preventive medicine|prevention]].<ref>{{cite journal | last1 = Gansler | first1 = Ted | last2 = Ganz | first2 = Patricia A | last3 = Grant | first3 = Marcia | last4 = Greene | first4 = Frederick L | last5 = Johnstone | first5 = Peter | last6 = Mahoney | first6 = Martin | last7 = Newman | first7 = Lisa A | last8 = Oh | first8 = William K | last9 = Thomas | first9 = Charles R | last10 = Thun | first10 = Michael J | last11 = Vickers | first11 = Andrew J | last12 = Wender | first12 = Richard C | last13 = Brawley | first13 = Otis Webb | title = Sixty Years of CA: A Cancer Journal for Clinicians | journal = CA: A Cancer Journal for Clinicians | volume = 60 | issue = 6 | issn = 1542-4863 | doi = 10.3322/caac.20088 | pages = 345–350 | year = 2010}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[CINAHL]]
* [[Current Contents]]/Clinical Medicine
* [[EMBASE]]
* [[MEDLINE]]
* [[ProQuest]]
* [[Science Citation Index]]
* [[Scopus]]
* [[VINITI]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 131.723, ranking it first out of 211 journals in the category "Oncology".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact:Oncology |title=2015 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science|series=[[Web of Science]] }}</ref> The journal has the highest impact factor of the 11,961 journals rated in the Science edition of the ''Journal Citation Reports''.<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact:All journals |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.3322/(ISSN)1542-4863}}
{{American Cancer Society}}
{{DEFAULTSORT:CA - A Cancer Journal for Clinicians}}
[[Category:Oncology journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1950]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:1950 establishments in the United States]]
[[Category:Academic journals associated with learned and professional societies of the United States]]
[[Category:American Cancer Society]]