{{Infobox military unit
|unit_name= Abkhazian Air Force
|image=[[File:Apsny Flag With Helicopter.jpg|300px]]
|caption=
|start_date= [[1992 in aviation|1992]]
|country=
|allegiance= [[Abkhazia]]
|branch=
|type=
|role=
|size= 250 personnel (2001){{citation needed|date=June 2015}}<br>15+ aircraft (2011){{citation needed|date=June 2015}}
|command_structure=
|garrison=
|garrison_label=
|equipment=
|equipment_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|battles= [[Georgian-Abkhaz Conflict|Abkhaz-Georgian War]], [[2008 South Ossetia war]]
|anniversaries= Aviation Day, 27 August
|decorations=
|battle_honours=
<!-- Commanders -->
|current_commander=
|current_commander_label=
|ceremonial_chief=
|ceremonial_chief_label=
|colonel_of_the_regiment=
|colonel_of_the_regiment_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:Abkhaz Roundel.svg|100px]]
|identification_symbol_label=Roundel
|identification_symbol_2=
|identification_symbol_2_label=
<!-- Aircraft -->
|aircraft_attack=  [[Aero L-39|L-39]], [[MI-24]]
|aircraft_Fighter= [[Sukhoi Su-27 Flanker]]
|aircraft_bomber=
|aircraft_electronic= 	

|aircraft_interceptor=
|aircraft_recon=
|aircraft_patrol=
|aircraft_trainer= [[Yakovlev Yak-52|Yak-52]]
|aircraft_transport= [[Mil Mi-8|Mi-8]], [[Antonov An-2|An-2]]
}}
The '''Abkhazian Air Force''' is a small [[air force]] operating from [[Abkhazia]].  Few details are available on its formation, but it is reported to have been established by [[Viyacheslav Eshba]] based upon several [[Yakovlev Yak-52|Yak-52]] trainer aircraft armed with machine guns.<ref name="Berkeley-1998">Slavic & East European Collections at UC Berkeley (June 1998). [http://www.lib.berkeley.edu/doemoff/slavic/pdfs/army698.pdf]. ''Army & Society in Georgia: Military Chronicle – Miscellany''.  Drawn from an entry published in ''7 Dge'', No. 72, June 22–23, p.3 (reprinted from "''Abkhazia''" No. 5, a periodical issued in Russia).  Retrieved 17 January 2007.</ref>  Its first combat mission was conducted on 27 August 1992, which has come to be celebrated in Abkhazia as "Aviation Day."  The Abkhaz Air Force claims to have made 400 operational flights during the [[Georgian-Abkhaz Conflict|1992-1993 Abkhaz-Georgian war]].<ref name="Berkeley-1998"/>  Abkhaz combat losses during the civil war are uncertain, but include a Yak-52 on a reconnaissance mission near [[Sukhumi]] on 4 July 1993.<ref name="ACIG">Cooper, Tom. (September 29, 2003). [http://www.acig.org/artman/publish/article_282.shtml Georgia and Abkhazia, 1992-1993: the War of Datchas]. ''Air Combat Information Group (ACIG)''.  Retrieved 17 January 2007.</ref>

Besides the Yak-52, aircraft operated by the Abkhaz Air Force during the war reportedly included at least a pair each of [[Sukhoi Su-25]] and [[Sukhoi Su-27|Su-27]] fighters and five [[Aero L-39|L-39 Albatros]] jet trainers, as well as a few [[Mil Mi-8]] helicopters and several other unidentified light aircraft.<ref name="WAF">{{cite web|url=http://www.worldairforces.com/Countries/abkhazian/abk.html|title=World Air Forces|work=Abkhazian Air Force|accessdate=2007-01-16| archiveurl= https://web.archive.org/web/20070115114921/http://worldairforces.com/Countries/abkhazian/abk.html| archivedate= 15 January 2007 <!--DASHBot-->| deadurl= no}}</ref>  However, the Russians flew numerous sorties in support of the Abkhazians and it is unclear which of these aircraft were truly Abkhazian-operated.  (There are also claims that Russian aircrew were instructed to cover up the [[Military aircraft insignia|national insignia]] on their aircraft and then flew raids against Georgian positions.<ref name="ISCIP">Institute for the Study of Conflict, Ideology, and Policy (24 October 2001). [http://www.bu.edu/iscip/digest/vol6/ed0617.html]. ''The NIS Observed: An Analytical Review – Caucasus: Georgia'', Vol. VI, No. 17.  Drawn from an entry published in "''Moskovskiye Novosti''" 22 October 2001.  Retrieved 17 January 2007.</ref>)  The sophisticated Su-27s in particular appear to have been operated only by the Russians, not the Abkhazians.  The Russians flew Su-27s from [[Gudauta]] Airbase, and during the attack on Sukhumi, one of them was shot down by an [[S-75 Dvina]] (NATO reporting name: SA-2 "Guideline") [[surface-to-air missile]] on 19 March 1993 (although it remains unknown who fired the missile).<ref name="ACIG"/>  It is unclear whether Su-25s said to have been in Abkhazian service during the civil war were actually theirs or [[Russian Air Force]] aircraft, although at least two seem to have been obtained prior to the withdrawal of Russian combat aircraft from Gudauta AB in 2001.

In the autumn of 2001, Abkhazia's air force was reported to comprise 250 personnel,  1 Su-25, 2 L-39, 1 Yak-52, and 2 Mi-8.<ref name="Berkeley-2001">Slavic & East European Collections at UC Berkeley (September–October 2001). [http://www.lib.berkeley.edu/doemoff/slavic/pdfs/army0901.pdf]. ''Army & Society in Georgia: Military Chronicle – Armed forces of Abkhazia''. Drawn from an entry published in "''Kviris Palitra''" No. 44, October 29-November 4, 2001, p.9. Retrieved 17 January 2007.</ref> The display of three L-39s at a parade in 2004 suggests a possible recent acquisition.<ref name="WAF"/>  In February 2007 a Russian website reported that Abkhazia has 2 Su-27 fighters,  1 Yak-52, 2 Su-25 attack aircraft, 2 L-39 combat trainers, 1 An-2 light transport, 7 Mi-8 helicopters and 3 Mi-24 helicopters.<ref name="Sedognia-2007">{{cite news | script-title=ru:Почему Грузия проиграет будущую войну | language = Russian | publisher =Sedognia.ru | date = 2007-02-27 | url =http://segodnia.ru/index.php?pgid=2&partid=45&newsid=3622 | accessdate = 2007-10-14 | archiveurl= https://web.archive.org/web/20070929001634/http://segodnia.ru/index.php?pgid=2&partid=45&newsid=3622| archivedate= 29 September 2007 <!--DASHBot-->| deadurl= no}}</ref> However, an undated 2007 Abkhaz source gave the inventory for the Abkhazian Air Force as 1 MiG-21, 1 Su-25, 2 L-39, 1 Yak-52, and 2 Mi-8.<ref>Abkhaz.org. (Undated; 2007 copyright). [http://abkhazia.e-caucasia.com/index.php?option=com_content&task=view&id=123&Itemid=37 Abkhazian Army] {{webarchive |url=https://web.archive.org/web/20071212045904/http://abkhazia.e-caucasia.com/index.php?option=com_content&task=view&id=123&Itemid=37 |date=December 12, 2007 }}. Retrieved 19 November 2007.</ref>  In March 2008, a military aviation enthusiast website repeated this inventory but added 9 Mi-24/35 attack helicopters,<ref>MilAvia Press.  [http://www.milaviapress.com/orbat/abkhazia/index.php Order of Battle - Abkhazia] (as updated March 2008).  Retrieved 12 April 2008.</ref> but a photo from December 2009 of an Abkhazian Airbase confirms 2 Mi-24/35 attack helicopters, 1 Mi-8 helicopter (Also Present A Mi-8 with UN Markings and an other with no markings), 4 L-39 Combat Trainers, and 2 An-2 light transports along with the single Yak-52 (With a Russian Civil Aircraft Tail Number) there are also photos showing a second Mi-17.{{citation needed|date=June 2015}}

== Equipment ==
An accounting of exact types, quantities, and service dates for aircraft serving in the Abkhazian Air Force is difficult to accurately provide due to a number of factors including Abkhazia's disputed status, a lack of official available information, multiple conflicts over the course of its existence, and the regular involvement of Russian aircraft and pilots in the conflicts and region.  In general, the air force has relied on aircraft inherited from the former Soviet forces based in Abkhazia with possible reinforcement in recent years by Russia with second-hand aircraft.  No traditional contracts for aircraft purchases by Abkhazia have been reported.

=== Aircraft ===
{{avisummarybytype|combat=10|recon=|transport=2|training=6|ah=3|uh=3|hc=|glider=}}
{{avilisthead|mil-current}}
|-
| [[Sukhoi Su-27]]
| USSR / Russia
| jet
| [[fighter aircraft|fighter]]
| 
| 6
| 
| Two reported but most likely operated by [[Russia]]<ref name="WAF"/><ref name="Sedognia-2007"/>
|-
| [[Sukhoi Su-25]]
| USSR / Russia
| jet
| [[attack aircraft|attack]]
| 
| 3
| 
| Reported in service from 1992<ref name="WAF"/><ref name="Berkeley-2001"/><ref name="Sedognia-2007"/>
|-
| [[Mikoyan-Gurevich MiG-21]]
| USSR
| jet
| [[fighter aircraft|fighter]]
| 
| 1
| 
| Reported in service from 2001 to 2007<ref name="Berkeley-2001"/><ref name="Sedognia-2007"/>
|-
| [[Aero L-39]]
| Czechoslovakia
| jet
| [[training aircraft|trainer/attack]]
| 
| 5
| 
| Reported in service from 1992<ref name="WAF"/><ref name="Berkeley-2001"/><ref name="Sedognia-2007"/><ref name="Aleshru">https://www.flickr.com/photos/aleshru/4182579146/</ref>
|-
| [[Yakovlev Yak-52]]
| USSR
| propeller
| [[training aircraft|trainer]]
| 
| 1
| 
| Initial equipment in 1991/2.<ref name="Berkeley-1998"/><ref name="WAF"/> Single example reported since 2001<ref name="Berkeley-2001"/><ref name="Sedognia-2007"/>
|-
| [[Antonov An-12]]
| USSR
| propeller
| [[Military transport aircraft|transport]]
| 
| 2
| 
| Reported since 2007<ref name="Sedognia-2007"/><ref name="Aleshru"/>
|-
| [[Mil Mi-24|Mil Mi-24/35]]
| USSR / Russia
| helicopter
| [[attack helicopter|attack]]
| 
| 3
| 
| Reported in service from 2007<ref name="Sedognia-2007"/><ref name="Aleshru"/>
|-
| [[Mil Mi-8]]
| USSR / Russia
| [[Helicopter]]
| [[transport helicopter|transport]]
| 
| 3
| 
| Reported in service since 1992.<ref name="Berkeley-1998"/><ref name="WAF"/><ref name="Berkeley-2001"/><ref name="Aleshru"/> Up to 7 reported in 2007<ref name="Sedognia-2007"/>
|}

==See also==
*[[Georgian Air Force]]

==References==
{{reflist}}

{{Air Forces of the countries former Soviet Union}}
{{European topic
 |name   = Air forces in Europe
 |state  = 
 |title=[[Air forces]] in Europe
 |suffix=_Air_Force
 |countries_only=yes
 |UK_only=yes
}}
{{air forces}}

[[Category:Military units and formations established in 1992]]
[[Category:Air forces by country]]
[[Category:Air forces]]
[[Category:Military of Abkhazia]]