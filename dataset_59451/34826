{{good article}}

{{Infobox storm
| name = 1899 New Richmond tornado
| image = New Richmond Tornado.gif{{!}}border
| alt = 
| caption = Ruins of the town's [[Methodism|Methodist]] [[Church (building)|Church]] following this deadly tornado.
| formed = June 12, 1899 6:00 p.m. [[Central Time zone|CST]]
| active =
| dissipated =
| lowest pressure =  
| lowest temperature =
| tornadoes = 
| fujitascale = F5
| tornado duration = 
| highest winds = 
| gusts =
| maximum snow = 
| power outages =
| casualties =  117 fatalities, 125 injuries
| damages = $300,000<br>(${{formatprice|{{Inflation|US|300000|1899}}}} in {{Inflation-year|US}} dollars{{inflation-fn|US}})
| affected = West-central [[Wisconsin]]
| location = 
| current advisories =
| enhanced =
| notes = 
}}

The '''1899 New Richmond Tornado''' was an estimated F5 tornado that nearly destroyed the town of [[New Richmond, Wisconsin]], on June 12, 1899, killing 117 and injuring 125 people. More than $300,000 (USD) (${{formatnum:{{Inflation|US|300000|1899|r=-3}}}} in today's dollars) in damage was reported.<ref name="Grazulis"/><ref name="tornado project">[[The Tornado Project]]. [http://www.tornadoproject.com/toptens/8.htm #8: The New Richmond Tornado]. Retrieved on 2007-06-27.</ref>{{Inflation-fn|US}}

== History ==
June 12, 1899, was the day of the Gollmar Brothers [[Circus]], which drew hundreds of visitors in addition to the town's 2,500 inhabitants. Around 3:00&nbsp;p.m., clouds began to build, and the sky became quite dark. As the circus ended for the day around 4:30 PM, a heavy rain, with some [[hail]], began to fall. The rain let up around 5:00 PM, and people began to head home for the day. By 6:00 PM, the streets of New Richmond were full of tourists, travelers and residents.<ref name="history">Sather, Mary. "[http://nrheritagecenter.org/cyclone.htm They Built Their City Twice: A History of New Richmond, Wisconsin]" Retrieved 2009-02-10.</ref>

Meanwhile, the tornado had already touched down to the southwest. It began as a waterspout on the east bank of [[Lake St. Croix Beach, Minnesota|Lake St. Croix]], about 15&nbsp;miles (24.1&nbsp;km) from New Richmond, at around 5:30 PM. The tornado was described as a "boiling cloud", which seemed to skirt the hills to the east of Lake St. Croix, and then head straight for New Richmond. Passing over mostly open country, it destroyed several farm buildings near the rural communities of [[Burkhardt, Wisconsin|Burkhardt]] and [[Boardman, Wisconsin|Boardman]] as it traveled northeast. Three people were killed there.<ref name="Grazulis">{{cite book|last=Grazulis|first=Thomas P|title=Significant Tornadoes 1680–1991|date=July 1993|publisher=The Tornado Project of Environmental Films|location=[[St. Johnsbury, VT]]|isbn=1-879362-03-1}}</ref><ref name="history"/>

There was little warning in New Richmond. The tornado was completely illuminated by lightning, but it was visible for only a few minutes before it reached the town. Homes and businesses were demolished and torn from their foundations, flying debris filled the air, and people were swept away. The brick, three-story Nicolett Hotel was completely leveled by the tornado.<ref>{{cite web|author=extremeplanet|url=http://extremeplanet.me/2013/01/17/the-indefinitive-list-of-the-strongest-tornadoes-ever-recorded-pre-1970/|title=The Indefinitive List of the Strongest Tornadoes Ever Recorded (Pre-1970): Part I &#124;|publisher=Extremeplanet.me|date=2013-01-17| accessdate=2013-06-20}}</ref> Some barely had time to scramble for shelter in storm cellars, but many were caught in the streets. Even some who did take refuge were killed anyway, such as those who ran into the O.J. Williams dry goods store. The store was swept away, and the people taking shelter in the basement were pelted to death by flying bricks and timbers.<ref name="Grazulis"/><ref name="history"/> Most people who could not find shelter were killed. Debris of all sorts flew through the air at tremendous speeds. A 3,000&nbsp;pound safe was flung a block from its original location. Trees and timbers were hurled "like [[javelin]]s" through the air, and the intense winds swept people up and threw them against walls or the ground.<ref name="history"/> The northern residential section of town was completely obliterated by the tornado, with nothing left but small pieces of debris scattered about. Every home in that area was leveled or swept away. The tornado dissipated shortly after exiting the town.<ref>{{cite web|author=extremeplanet|url=http://extremeplanet.me/2013/01/17/the-indefinitive-list-of-the-strongest-tornadoes-ever-recorded-pre-1970/|title=The Indefinitive List of the Strongest Tornadoes Ever Recorded (Pre-1970): Part I &#124;|publisher=Extremeplanet.me|date=2013-01-17| accessdate=2013-08-04}}</ref>

Shortly thereafter, another storm with strong winds swept through, sending people back into their shelters. It is likely that some died in fires while potential help was hiding from the possibility of another tornado.<ref name="history"/>

== Aftermath ==
All but the extreme western end of the town was obliterated by the tornado and subsequent fires. More than 300&nbsp;buildings were destroyed, and the only significant surviving structures were the [[Catholic]] and [[Baptist]] churches. The town's electrical plant and water facilities were destroyed, so fires ran rampant through the scattered debris. Many bodies found in the aftermath were burnt beyond recognition&mdash; it was impossible to tell if they died from the tornado or from being trapped and burned alive.<ref name="history"/>

Twenty-six families experienced multiple deaths, and six reported four or more deaths in the family. One case of loss stemming from the tornado which stands out particularly was that of Stephen N. Hawkins, a prominent lawyer and member of the community. According to historian Mary Sather, he is regarded as the man who lost the most in the storm. His wife Margaret, along with their two daughters Millie and Evangeline and their youngest son Walter, were all killed in the tornado. Stephen survived, though badly injured, as did his oldest son Fred, who suffered only minor injuries. Another son, Robert, was also spared as he'd been away from town when the storm hit. In addition, Margaret's aged father, Anthony Early, along with an unnamed niece and the family's hired girl were also killed.

After order was restored, authorities determined that a total of 117&nbsp;people had been killed (including three fatalities at Boardman and two near Clear Lake) and more than 150 injured. This is the ninth highest death toll for any single tornado in American history.<ref name="Grazulis"/> The New Richmond Tornado is generally assumed to have been an F5 tornado, with winds in excess of 261&nbsp;mph.{{citation needed|date=June 2013}}

The town was so completely damaged that it had to be essentially rebuilt. Damage claims exceeded $300,000 ($7&nbsp;million in 2006 [[United States dollar|dollars]]), however, damages may have been as high as $600,000 ($14&nbsp;million in 2006 dollars).<ref name="history"/><ref name="dictionary">[[Wisconsin Historical Society]]. [http://www.wisconsinhistory.org/dictionary/index.asp?action=view&term_id=10466&term_type_id=3&term_type_text=Things&letter=N Dictionary of Wisconsin History: New Richmond tornado (1899)]. Retrieved on 2007-06-27.</ref>

[[Image:New Richmond Tornado panorama.jpg|thumb|center|800px|[[Panorama|Panoramic]] view of the damage]]

==References==
{{Reflist}}

==External links==
{{Commons category|1899 New Richmond tornado}}
*[http://docs.lib.noaa.gov/rescue/mwr/027/mwr-027-07-0299b.pdf The Tornado at New Richmond, Wis. (PDF)] ''[[Monthly Weather Review]]''

{{Deadliest United States tornadoes}}

{{DEFAULTSORT:1899 New Richmond Tornado}}
[[Category:F5 tornadoes|N]]
[[Category:Tornadoes of 1899|N]]
[[Category:Tornadoes in Wisconsin|N]]
[[Category:1899 in Wisconsin|N]]
[[Category:1899 natural disasters in the United States]]