{{Infobox journal
| title = Complexity
| cover = [[File:Complexity-journal-covers.jpg]]
| discipline = [[Complex systems]]
| editor = [[Peter Schuster]], [[Alfred Hübler]]
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Bimonthly
| history = 1995–present
| impact = 3.514
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-0526
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-0526/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-0526/issues
| link2-name = Online archive
| ISSN = 1076-2787
| eISSN = 1099-0526
| CODEN = COMPFS
| LCCN = 95641033
| OCLC = 30446118
}}
'''''Complexity''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] covering the field of [[complex systems|complex adaptive systems]]. The journal's scope includes [[Chaos theory]], [[genetic algorithm]]s, [[Cellular automaton|cellular automata]], [[Artificial neural network|neural network]]s, [[evolutionary game theory]], and [[econophysics]].

From 2017, '''''Complexity''''' was transferred to [[Hindawi Publishing Corporation]].

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[CompuMath Citation Index]]
* [[Computer & Information Systems Abstracts]]
* [[Computing Reviews]]
* [[Current Index to Statistics]]
* [[Elsevier BIOBASE]]
* [[Inspec]]
* [[Mathematical Reviews]]/[[MathSciNet]]/[[Current Mathematical Publications]]
* [[PSYNDEX]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.514.<ref name=WoS>{{cite book |year=2016 |chapter=Complexity |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of systems science journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1099-0526}}

{{DEFAULTSORT:Complexity}}
[[Category:Publications established in 1995]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Systems journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Hindawi Publishing academic journals]]