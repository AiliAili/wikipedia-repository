"'''Dr. Ox's Experiment'''" ({{lang-fr|Une fantaisie du docteur Ox}}, "A Fantasy of Doctor Ox")<ref name="Ox">A Fantasy of Dr Ox, Jules Verne, trans. Andrew Browne, [[Hesperus Press]], 2003</ref> is a [[short story]] by the [[French language|French]] writer and pioneer of [[science-fiction]], [[Jules Verne]], published in 1872. It describes an experiment by one Dr. Ox and his assistant Gedeon Ygene. A prosperous scientist Dr. Ox offers to build a novel gas lighting system to an unusually stuffy [[Flanders|Flemish]] town of Quiquendone. As the town bore no charges, the offer is gladly accepted. The hidden interest of Dr. Ox is however not lighting, but large scale experiment on effect of oxygen on plants, animals and humans. He uses electrolysis to separate water into hydrogen and oxygen. The latter is being pumped to the city causing accelerated growth of plants, excitement and aggressiveness in animals and humans. The story ends up by destruction of the oxygen factory of Dr. Ox – by accident, oxygen and hydrogen got mixed causing a major explosion. Jules Verne acknowledges in the epilogue that the described effect of oxygen is a pure fiction invented by him.

The text was re-published in a Verne short-story anthology, ''[[Doctor Ox]]'', in 1874.

==Operas==
The story was adapted by [[Jacques Offenbach]] as ''[[Le docteur Ox]]'', an [[opéra-bouffe]] in three acts and six tableaux, premiered on 26 January  1877 with a libretto by [[Arnold Mortier]], [[Philippe Gille]] and Verne himself. Annibale Bizzelli composed another version, ''il Dottor Oss''.

It was also adapted by [[Gavin Bryars]] as ''[[Doctor Ox's Experiment (opera)|Doctor Ox's Experiment]]'',  an opera in two acts with a libretto by [[Blake Morrison]], first performed on 15 June 1998.

==References==
{{reflist}}

==External links==
* [https://books.google.com/books?hl=en&lr=&id=-QECAAAAQAAJ&oi=fnd&pg=PR13&dq=dr.+ox%27s+experiment+jules+verne&ots=fz7lxt49dc&sig=pxG4P0Z8dR8PT3TfwpCL5_5vrDY#PPA34,M1 Dr. Ox's Experiment] (Translation from 1874)
* [https://archive.org/details/droxsexperimento00vern Dr. Ox's experiment, and other stories, Internet Archive]
* [https://ebooks.adelaide.edu.au/v/verne/jules/v52dr/ E-book at University of Adelaide] 
* {{librivox book | title=Doctor Ox's Experiment | author=Jules Verne}}

{{Jules Verne}}

[[Category:1872 short stories]]
[[Category:Short stories by Jules Verne]]
[[Category:Belgium in fiction]]
[[Category:Novels set in Belgium]]
[[Category:Fictional mad scientists]]
[[Category:Works set in Flanders]]
[[Category:Novels adapted into operas]]


{{sf-story-stub}}