{{Infobox journal
| title = American Journal of Evaluation 
| cover = [[File:American Journal of Evaluation.tif]]
| editor = Sharon F. Rallis 
| discipline = [[Evaluation]], [[research methods]]
| former_names =
| abbreviation = Am. J. Eval.
| publisher = [[Sage Publications]]
| country =
| frequency = Quarterly
| history = 1981-present
| openaccess =
| license =
| impact = 1.804 
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201729/title
| link1 = http://aje.sagepub.com/content/current
| link1-name = Online access
| link2 = http://aje.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 38534330
| LCCN = 98660819
| CODEN =
| ISSN = 1098-2140
| eISSN = 1557-0878
}}
The '''''American Journal of Evaluation ''''' is a [[peer-reviewed]] [[academic journal]] that covers research on methods of [[evaluation]]. It is a quarterly [[journal]] and its first publication was in 1981. The [[editor-in-chief]], Sharon F. Rallis, serves as the Dwight W. Allen Distinguished Professor in [[Educational Policy Institute|Educational Policy]] and Reform in the Department of Educational Policy, Research & Administration in the College of Education, as well as the director of the Centre for Education Policy, at the [[University of Massachusetts Amherst]]. She has more than 40 years of experiment in education. Sharin F. Rallis is also a writer who has already published several books, articles and technical reports about evaluation methodology. She is a past president of the [[American Evaluation Association]] which sponsors the ''American Journal of Evaluation''.<ref>Rallis, S. (2015). ''Sharon F. Rallis, Ed.D. | College of Education''. [online] Umass.edu. Available at: <nowiki>https://www.umass.edu/education/faculty-staff-listings/SharonRallis</nowiki> [Accessed 5 Nov. 2015].</ref>

The [[American Evaluation Association]] is a professional association where students, funders, managers, practitioners, faculty and government decision-makers can discuss about the different fields of [[evaluation]]. It cooperates with the American Journal of Evaluation when writing articles. The ''American Journal of Evaluation'' is published by [[SAGE Publications]].<ref>SAGE, (2015). ''American Journal of Evaluation''. [online] Available at: <nowiki>http://www.sagepub.in/journals/Journal201729?q=mid%2520wicket&prodTypes=any&subject=L00&pageTitle=productsSearch</nowiki> [Accessed 5 Nov. 2015].</ref>

== History ==
The ''American Journal of Evaluation'' was for the very first time published in February 1981. Its first [[Article (publishing)|article]] was called “''News of the network''”. This first [[journal]] was composed of 25 articles and had approximatively 100 pages.<ref>Aje.sagepub.com, (2015). ''Table of Contents — February 1981, 2 (1)''. [online] Available at: <nowiki>http://aje.sagepub.com/content/2/1.toc</nowiki> [Accessed 30 Oct. 2015].</ref>

In order to develop the company, [[SAGE Publications]] decided, in 2005, to put an online version of the [[journal]]. All articles are, since then, available online and accessible to everyone on their official [[website]].<ref>Sack, J. (2015). ''HighWire About "American Journal of Evaluation"''. [online] Highwire.stanford.edu. Available at: <nowiki>http://highwire.stanford.edu/announce/details.dtl?journalcode=spaje</nowiki> [Accessed 2 Nov. 2015].</ref>

== SAGE Publications ==
[[SAGE Publications]] is an independent company which was created by Sara Miller McCune and Georges D. McDune in 1965. It has published more that 800 journals and 800 books including the American Journal of Evaluation. [[Sage Publications|SAGE Publications]] is a worldwide company which has more that 1,500 employees mostly based in [[Los Angeles]], [[London]], [[New Delhi]], [[Singapore]] and [[Washington, D.C.|Washington DC]].

It is a leading independent publisher since 1965 about [[scholarship]] and [[research]]. Its goal is to disseminate research teaching on a global scale and educate future generations. It began as a [[social science]]s publisher because they believed in the important of this discipline. They have expanded their topics into [[engineering]] and [[medicine]], but most of people know [[SAGE Publications]] as the social sciences publisher in the world.<ref>Uk.sagepub.com, (2015). ''Company Information | SAGE Publications Ltd''. [online] Available at: <nowiki>https://uk.sagepub.com/en-gb/eur/company-information</nowiki> [Accessed 2 Nov. 2015].</ref>

They are different from the other publishers as their belief is that research methods are not just something that schools around the world do but actually need to be informed about. Being an independent company is their main power, they always have a long term view and can build sustainably for the future. [[SAGE Publications]] grows by taking risks such as making research and [[discourse analysis]] about the African phenomena. They are always trying to be visionaries in order to improve their quality of research.

SAGE books are mainly used by teachers who can improve their teaching methods thanks to analysis they can find in those books. Books are made in order to encourage students to develop their critical spirit. Furthermore, [[Sage Publications|SAGE Publications]] works a lot with their authors because they want them to feel that the company cares about their books. They work in [[partnership]] with them so as to have a sustainable relation. They distribute their books all around the world and have international writers because they think that ideas shouldn’t be limited by [[geography]].

For [[SAGE Publications]], publishing is not only a commercial business but also a way to support social causes. For their future, they are creating new [[Computing platform|platforms]] for communication: [[Social media]] communities or new research tools to meet their partner’s needs. The two most important things to them are:
* Succeeding commercially
* Quality of the content and the impact of their publishing on the world at large

== Purpose ==
The ''American Journal of Evaluation'' gives an inside view of how to make an [[evaluation]]. They try to make everybody understand the making and the utility of an evaluation. Each tool used to make the evaluation is identified and explained by this [[journal]]. The ''American Journal of Evaluation'' helps to better understand this field of evaluation which can be misread as it is a very large and specific [[Discipline (academia)|discipline]]. Moreover, it distinguishes the evaluations which are made by professionals and that people can trust and the others which are not based on relevant tools. It describes the different steps used by the experts to build their evaluation.<ref>Gargani, J. (2011). More Than 25 Years of the American Journal of Evaluation: The Recollections of Past Editors in Their Own Words. ''American Journal of Evaluation'', 32(3), pp.428-447.</ref>

The ''American Journal of Evaluation'' also helps people to learn about the different subjects on which the evaluation is about. All of the articles are published in order to educate future generation and to utilise relevant teaching tools. Thus, the ''American Journal of Evaluation'' follows the [[SAGE Publications]] mentality.

== Abstracting and indexing ==
According to the ''[[Journal Citation Reports]]'', for its first year of publication, in 2009, the ''American Journal of Evaluation'' had an [[impact factor]] of 0.942.<ref>Researchgate.net, (2015). ''American Journal of Evaluation''. [online] Available at: <nowiki>http://www.researchgate.net/journal/1098-2140_American_Journal_of_Evaluation</nowiki> [Accessed 1 Nov. 2015]</ref> It reached 1.16 in 2011<ref>Leigh Wang, L. (2011). Journals in Assessment, Evaluation, Measurement, Psychometrics, and Statistics.</ref> whereas in 2013 the journal was ranking 27 out of 93 journals in the category  "Social Sciences, Interdisciplinary"<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Social Sciences, Interdisciplinary|title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=Web of Science |postscript=.}}</ref> with an impact factor of 0.965. Recent figures show that the impact factor was reaching 1.804 in 2014.<ref>Journal-database.com, (2015). ''American Journal Of Evaluation Impact Factor (IF) 2015|2014 - Impact Factor Search''. [online] Available at: <nowiki>http://www.journal-database.com/journal/american-journal-of-evaluation.html</nowiki> [Accessed 3 Nov. 2015].</ref> The [[impact factor]] is a measure of the frequency with which the "average article" in a journal has been cited in a particular year or period.<ref>Reuters, T. (2015). ''The Thomson Reuters Impact Factor - IP & Science - Thomson Reuters''. [online] Wokinfo.com. Available at: <nowiki>http://wokinfo.com/essays/impact-factor/</nowiki> [Accessed 3 Nov. 2015]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201729/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Business and management journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1981]]