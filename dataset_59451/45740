{{Infobox astronaut
| name          =Alvin S. White
| image         =
| type          =[[USAF]] [[Test pilot|Test Pilot]] 
| nationality   =American
| birth_name    =Alvin Swauger White
| birth_date    =9 December 1918
| death_date    ={{Death date and age|2006|4|29|1918|12|9}}
| birth_place   =[[Berkeley, California]], U.S.
| death_place   =[[Tucson, Arizona]], U.S.
| occupation    =[[Test pilot]]
| alma_mater    =[[University of California at Davis]]<br>[[University of California, Berkeley]], B.S. 1947
| rank          =
| selection     =[[List of astronauts by selection#1957|1957 MISS Group]]
| time          =
| mission       =None
| insignia      =
}}
'''Alvin Swauger "Al" White''' (December 9, 1918 – April 29, 2006) was an American [[test pilot]] and [[mechanical engineer]]. He flew the [[maiden flight]]s of both [[XB-70 Valkyrie]] aircraft, the first 2,000&nbsp;mph flight, and all subsequent [[Mach number|Mach]] 3 exploration flights.<ref name=AWOH>{{cite web  |url = http://www.cityoflancasterca.org/Index.aspx?page=206#white |title = 1994 Honorees |date=July 13, 2006 |accessdate = February 3, 2011 |work = Aerospace Walk of Honor |publisher=City of Lancaster, California}}</ref>

==Biography==
Born December 9, 1918, to Harold H. White, Sr. and Ruth A. Winkleman in [[Berkeley, California]], he enrolled in the [[University of California at Davis]] in 1936 to study [[Electrical Engineering]], and transferred to the campus at [[University of California, Berkeley|Berkeley]] two years later. He began his flying career in the [[Civilian Pilot Training Program]], receiving his license in 1940.<ref name=LA_Times_Obit>{{cite web
 |title              = Obituary: Alvin S. White, 87; Longtime Test Pilot
 |date             = May 5, 2006
 |url                = http://articles.latimes.com/2006/may/05/local/me-white5
 |publisher    = Los Angeles Times
 |location       = Los Angeles, California
 |accessdate  = February 4, 2011
}}</ref> During [[World War II]], he enlisted in the [[U.S. Army Air Forces]] as an Aviation Cadet, graduated from training at [[Williams Air Force Base|Williams Field]], Arizona in 1942.  He later flew bomber escorts and strafing missions over Europe in the [[P-51 Mustang]] with the [[355th Fighter Wing|355th Fighter Group]] from [[Normandy landings|D-Day]] through [[Victory in Europe Day|V-E Day]].<ref name=LA_Times_Obit/>

After the war, White completed his [[Bachelor of Science]] degree in [[Mechanical Engineering]] at the University of California in 1947, and went on to become an engineering test pilot for the U.S. Air Force and [[North American Aviation]].<ref name=LA_Times_Obit/> He participated in a variety of flight test programs over his career, including the [[F-86 Sabre]] jet, the [[F-100 Super Sabre]] series, the [[YF-107]] and the [[North American X-15|X-15]].<ref name=AWOH/> In 1958, White was selected for the U.S. Air Force's [[Man In Space Soonest]] manned spaceflight program. The program ended early due to financial and technical difficulties. In 1961, he was selected as chief test pilot for the flight test program of the [[XB-70 Valkyrie]], the world's largest supersonic aircraft, piloting the first flights of both planes and taking the aircraft through the buildup programs to flight at Mach 3.<ref name=AWOH/> On June 8, 1966, he was the sole survivor of [[XB-70 Valkyrie#Mid-air collision|the mid-air collision]] that destroyed the XB-70 #2 prototype, and killed his co-pilot, Major Carl Cross, USAF, and noted aviator [[Joseph A. Walker|Joe Walker]].<ref name=LA_Times_Obit/> White ejected from the XB-70, sustaining serious injuries, including one arm being crushed as it was caught in the clamshell-like escape capsule as it closed around him just before ejection from the aircraft.
[[File:North American XB-70A Valkyrie just after collision 061122-F-1234P-037.jpg|thumb|White's XB-70 shortly after the collision with Walker's [[F-104 Starfighter]], seen in flames]]

Later in 1966, White joined [[Trans World Airlines]] as Manager of Flight Operations, Research and Development.<ref name=LA_Times_Obit/> In 1969, he became a consultant in the field of aviation and aeronautics, working primarily as an expert witness in accident investigation litigation, requiring simulation of accident flight conditions in a comparable aircraft.<ref name=LA_Times_Obit/>

After 8,500 hours of flying time in over 125 different aircraft, he retired from the ranks of active pilots and settled in [[Tucson, Arizona]]. He died in Arizona in 2006.<ref name=LA_Times_Obit/>

==Honors==
White's military decorations include the [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] and the [[Air Medal]] with nine oak leaf clusters.<ref name=AWOH/> A past President<ref name=SETP_Pres>{{cite web
 |title              = SETP Presidents
 |url                = http://www.setp.org/setp-personnel/presidents.html
 |publisher    = The Society of Experimental Test Pilots
 |location       = Lancaster, California
 |accessdate  = February 4, 2011
}}</ref> and founding member of the [[Society of Experimental Test Pilots]], White earned some of the top awards for his profession, including the [[Iven C. Kincheloe Award]],<ref name=SETP_Kincheloe>{{cite web | title = Iven C. Kincheloe Award Recipients | url = http://www.setp.org/winners/iven-c-kincheloe-recipients.html | accessdate = February 4, 2011 |publisher=Society of Experimental Test Pilots |location = Lancaster, California}}</ref> the [[Octave Chanute Award]],<ref name=AIAA>{{cite web |url = http://www.aiaa.org/content.cfm?pageid=401&AwardView=Detail&participationID=19999&fromX=Search |title = Octave Chanute Award, 1965 |accessdate =February 4, 2011 |publisher=American Institute of Aeronautics and Astronautics |location=Reston, Virginia}}</ref> and the [[Harmon Trophy]],<ref name=Harmon_Trophy>{{cite web
 |title              = Harmon Trophy Presentation, 1994
 |url                = http://www.lbjlibrary.org/collections/on-this-day-in-history/october.html#13th
 |publisher    = Lyndon Baines Johnson Library & Museum
 |location       = Austin, Texas
 |accessdate  = February 4, 2011
}}</ref> which was presented by President [[Lyndon B. Johnson]]. In 1994, he was inducted into the [[Aerospace Walk of Honor]].<ref name=AWOH/>

==References==
{{reflist}}

==External links==
{{Portal|Biography}}
*{{cite web
 |first       = Joachim
 |last        = Becker
 |title       = Alvin Swauger White
 |url         = http://www.spacefacts.de/bios/candidates/english/white_alvin.htm
 |work        = SpaceFacts
 |publisher   =
 |location    = Germany
 |date           = June 28, 2009
 |accessdate  = February 4, 2011
}}

{{DEFAULTSORT:White, Alvin S.}}
[[Category:1918 births]]
[[Category:2006 deaths]]
[[Category:University of California, Davis alumni]]
[[Category:University of California, Berkeley alumni]]
[[Category:Harmon Trophy winners]]
[[Category:American test pilots]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the Air Medal]]
[[Category:Aviators from California]]
[[Category:American astronauts]]
[[Category:People from Berkeley, California]]