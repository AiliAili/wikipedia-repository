{{Infobox language family
|name=Ibero-Caucasian
|altname=Caucasian
|acceptance=defunct
|region=[[Caucasus]]
|familycolor=Caucasian
 |family   = Proposed language family
|child1=[[Northwest Caucasian languages|Northwest Caucasian]]
|child2=[[Northeast Caucasian languages|Northeast Caucasian]]
|child3=[[Kartvelian languages]]
|glotto=none
}}

The term '''Ibero-Caucasian''' (or '''Iberian-Caucasian''') was proposed by [[Georgia (country)|Georgian]] linguist [[Arnold Chikobava]] for the union of the three language families that are specific to the Caucasus, namely

* '''[[Kartvelian languages|Kartvelian]]'''
* '''[[Northwest Caucasian languages|Northwest Caucasian]]''', also called Circassian
* '''[[Northeast Caucasian languages|Northeast Caucasian]]''', also called Nakho–Dagestanian.

The Ibero-Caucasian phylum would also include three extinct languages: [[Hattic language|Hattic]], connected by some linguists to the Northwest (Circassian) family, and [[Hurrian language|Hurrian]] and [[Urartian language|Urartian]], connected to the Northeast (Nakh–Dagestanian) family.

== Family status ==
The affinities between the three families are disputed.  A connection between the Northeast and Northwest families is seen as likely by many linguists; see the article on the [[North Caucasian languages]] for details.

On the other hand, there are no known affinities between South Caucasian and the northern languages, which are two unrelated phyla even in [[Joseph H. Greenberg|Greenberg]]'s deep classification of the world's languages.  "Ibero-Caucasian" therefore remains at best a convenient geographical designation.

== Family name ==
The "Iberian" in the family name refers to [[Caucasian Iberia]] — a kingdom centered in Eastern [[Georgia (country)|Georgia]] which lasted from the 4th century BC to the 5th century AD, and is not related to the [[Iberian Peninsula]].

==See also==
* [[Chikobava Institute of Linguistics]]
* [[Languages of the Caucasus]]

==Main research centers==
* [http://caucasiology.tsu.ge/ Tsu Institute of caucasiology]
* [http://www.acnet.ge/ike.htm Chikobava Institute of Linguistics] of the [[Georgian Academy of Sciences]] ([[Tbilisi]]).
* [http://lingua1.phil.uni-jena.de/ssm/ Department of Caucasiology] at the [[University of Jena]] ([[Germany]]).
* [http://www.tsu.edu.ge/eng/faculties_e/10e.htm Faculty of Philology] at the  [[Tbilisi State University]] (Tbilisi).

==Main publications==

* ''The Yearbook of the Iberian-Caucasian Linguistics'' (Tbilisi).
* ''Revue de Kartvelologie et Caucasologie'' ([[Paris]]).

== Bibliography ==

*Tuite, Kevin (2008): "[http://www.mapageweb.umontreal.ca/tuitekj/caucasus/IberoCaucasian.pdf The Rise and Fall and Revival of the Ibero-Caucasian Hypothesis]", ''Historiographia Linguistica'' Vol. 35, No. 1-2., pp.&nbsp;23–82.

{{Authority control}}
[[Category:Proposed language families]]
[[Category:Languages of the Caucasus]]