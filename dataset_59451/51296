{{Infobox journal
| title = European Urban and Regional Studies
| cover = [[File:European Urban and Regional Studies journal front cover.gif]]
| editor = Adrian Smith 
| discipline = [[Urban studies]]
| former_names = 
| abbreviation = Eur. Urban Reg. Stud.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1994-present
| openaccess = 
| license = 
| impact = 1.673
| impact-year = 2011
| website = http://eur.sagepub.com/
| link1 = http://eur.sagepub.com/content/current
| link1-name = Online access
| link2 = http://eur.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 613531914
| LCCN = 97652843 
| CODEN = 
| ISSN = 0969-7764
| eISSN = 1461-7145
}}
'''''European Urban and Regional Studies''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that publishes articles in the field of [[urban studies]] related to processes of urban and regional development in Europe. It was established in 1994 and is published by [[SAGE Publications]].

== Abstracting and indexing ==
''European Urban and Regional Studies'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 1.673, ranking it 27th out of 89 in the category "Environmental Studies",<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: 'Environmental Studies' |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2012-11-20 |series=Web of Science |postscript=.}}</ref> and 4th out of 37 journals in the category 'Urban Studies'.<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Urban Studies |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2012-11-20 |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://eur.sagepub.com/}}

[[Category:Quarterly journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Publications established in 1994]]
[[Category:English-language journals]]
[[Category:Sociology journals]]
[[Category:Urban studies and planning journals]]