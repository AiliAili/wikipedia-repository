{{Infobox Theatre
|name           = Abbey Theatre
|image          = Abbey_theatre_nuneaton_nuneaton_september_2012.jpg
|caption        = Auditorium
|address        = Pool Bank Street 
|city           = Nuneaton
|country        = England
|designation    = 
|latitude       =
|longitude      =
|architect      = 
|owner          = Nuneaton Arts Council
|capacity       = 248
|type           = [[Receiving house]]
|opened         = 1969
|yearsactive    =
|rebuilt        = 2001
|closed         =
|othernames     =
|production     =
|currentuse     = Theatre
|website        = [http://www.abbeytheatre.co.uk abbeytheatre.co.uk]
}}

The '''Abbey Theatre''' is a theatre situated on Pool Bank Street in the Abbey Green quarter of [[Nuneaton]], England.

The venue hosts a wide variety of other performances including visiting opera and ballet companies, touring shows, musicals, pantomime and drama.

The theatre's chief executive is Tony Deeming. It is run as a trust by Nuneaton Arts Council.

With a regular annual attendance of over 30,000, the Abbey Theatre is the busiest theatre in Nuneaton, and the busiest venue for musicals in [[Warwickshire]].

==History==
The Nuneaton Arts Council was founded in 1969 by a group of local arts devotees who noted the continuing demise of other venues in [[Nuneaton]]. They saw the need to provide a focal point for all aspects of arts, and so what was until then a [[Territorial Army (United Kingdom)|Territorial Army]] drill hall was converted into the Abbey Theatre.

On 22 January 2007, a performance of the ''Pied Piper of Hamelin'' went up half an hour late after contractors had inadvertently concreted emergency doors shut.<ref>{{cite news |date=23 January 2007 |title=Bungling staff throw panto into chaos |url=http://www.nuneaton-news.co.uk/Bungling-staff-throw-panto-chaos/story-20278623-detail/story.html |newspaper=Nuneaton News}}</ref>

In August 2008, new security fencing was installed at the theatre to help keep anti-social behaviour away from the theatre and encourage more theatergoers to enjoy the diverse range of productions performed at the Abbey Theatre.<ref>{{cite news |date=27 August 2008 |title=Theatre given new security gating |url=http://www.nuneaton-news.co.uk/Theatre-given-new-security-gating/story-20278291-detail/story.html |newspaper=Nuneaton News}}</ref>

In 2008, Masqueraders Theatrical society became another society that had to move from the Bedworth Civic Hall to the Abbey Theatre, due to the costs of hiring the Bedworth venue.<ref>{{cite news |date=30 April 2008 |title=Theatre group jump Civic ship |url=http://www.nuneaton-news.co.uk/Theatre-group-jump-Civic-ship/story-20279016-detail/story.html |newspaper=Nuneaton News}}</ref>

November 2008 saw an entire winter season at the Abbey nearly come to a complete halt because due to the demolition of the old Ritz music hall, which was to become a supermarket, access to the backstage areas of the theatre were lost. Last minute talks only narrowly avoided all shows in that month being cancelled.<ref>{{cite news |date=26 November 2008 |title=Theatre Under Threat |url=http://www.nuneaton-news.co.uk/THEATRE-THREAT/story-20276835-detail/story.html |newspaper=Nuneaton News}}</ref>

August 2012 saw the theatre granted planning permission to install air conditioning in all public areas of the building, improving the customer experience, as previously watching a show at the Abbey was a hot affair.<ref>{{cite news |date=17 August 2012 |title=Air conditioning for Nuneaton theatregoers |url=http://www.nuneaton-news.co.uk/Air-conditining-Nuneaton-theatregoers/story-20282966-detail/story.html |newspaper=Nuneaton News}}</ref>

In June 2014, the theatre took part in the UK [[Heritage Open Days|Heritage Open Weekend]] and the public were invited to take a look around the theatre.<ref>{{cite web|url=http://www.heritageopendays.org.uk/directory/abbey-theatre-heritage-open-weekend|title=Abbey Theatre: Heritage Open Weekend|author=Heritage Open Days|work=heritageopendays.org.uk}}</ref>

In March 2015, the theatre was the venue for what was thought to be the largest ghost hunters convention in the UK.<ref>{{cite web|url=http://www.coventrytelegraph.net/news/local-news/ghost-hunters-converge-nuneaton-theatre-8498274|title=Ghost hunters to converge on Nuneaton theatre |work=Coventry Telegraph|author=Mike Malyon|date=22 January 2015}}</ref> During March 2015 the venue was also used for a Beats project which was featured on [[BBC Radio One]].<ref>{{cite news |date=13 March 2015 |title=North Warwickshire projects stars on Radio One |url=http://www.nuneaton-news.co.uk/North-Warwickshire-projects-stars-Radio/story-26167203-detail/story.html |newspaper=Nuneaton News}}</ref>

The theatre was discussed in the national press in December 2013 as an interview was published with the pantomime dame that features in one of its biggest pantomimes 'Abbey Players'.<ref>{{cite news |date=1 December 2013 |title=Is this the end of the pantomime dame? Productions do away with characters as they bid to become more politically correct |url=http://www.dailymail.co.uk/news/article-2516608/Is-end-pantomime-dame-Productions-away-characters-bid-politically-correct.html |work=Daily Mail Online}}</ref><ref>{{cite news |date=20 December 2011 |title=Christmas panto: How playing Widow Twankey brought out my inner Les Dawson! |url=http://www.dailymail.co.uk/news/article-2076411/Christmas-panto-How-playing-Widow-Twankey-brought-inner-Les-Dawson.html |work=Daily Mail Online}}</ref>

==Facilities==

===The main house===

The main house seats 248 people with space for 4 wheelchair users with carers. The stage is 4 foot 6 inches high from floor level, the seating is raked to the back of the auditorium.

===Bar===
The bar can be used for non theatre events and has in the past hosted smaller events such as comedy nights, small acoustic sessions, and meetings.  In the bar area are chairs, sofa's and various screens for showing content

===Milby room===

The Milby room can accommodate 30 people. It is used as a rehearsal space for various groups, for local chess club meetings every Monday evening, and by the local neighborhood watch team for local resident meetings.

===Etone Lounge===

This is the biggest space outside of the main house and can be used to put on small productions, as this space can seat up to 50 people, this space is mainly used for rehearsals for the local groups as well as for meetings if required.

==The future==

In February 2014 it was announced that the Abbey Theatre would close its doors and a new venue would take its place in a new creative quarter being created in the town which would also include an art house, cinema, with bars and restaurants to complement this area.<ref>{{cite news |date=10 February 2014 |title=Plans revealed for new theatre in town |url=http://www.nuneaton-news.co.uk/Plans-revealed-new-theatre-town/story-20598553-detail/story.html |newspaper=Nuneaton News}}</ref><ref>{{cite news |date=11 February 2014 |title=Ambitious new theatre plans are given Nuneaton Town Hall backing |url=http://www.nuneaton-news.co.uk/Ambitious-new-theatre-plans-given-Town-Hall/story-20605449-detail/story.html |newspaper=Nuneaton News}}</ref> The new venue proposals include three auditoriums, rehearsal and set building space. The new main auditorium will include 350 seats in a stalls and balcony arrangement, as well as a full backstage dressing room and green room complex for better facilities for visiting companies. Although a name has not been chosen yet for the new venue, Nuneaton Arts Council have confirmed it will not be called the Abbey Theatre.

No date has been formally set for the Abbey to be vacated although it is expected this will not take place until the new venue is up and running or by the end of the 2017 season.

==References==
{{reflist}}

==External links==
* [http://www.abbeytheatre.co.uk/ Abbey Theatre homepage].
* [https://www.ents24.com/nuneaton-events/abbey-theatre/ Ents24].
* [https://www.list.co.uk/place/64055-abbey-theatre/ List].
* [http://www.whatsonstage.com/nuneaton/theatres/abbey-theatre-and-arts-centre_1864/ Whats on Stage].
* [http://www.skiddle.com/whats-on/Coventry/The-Abbey-Theatre-%26-Arts-Centre/ Skiddle].
* [http://www.chortle.co.uk/venues/11/england_-_midlands_west_midlands/3118/nuneaton_abbey_theatre_%26_arts_centre/ Chortle].
* [http://connect-caller.co.uk/abbey-theatre-nuneaton-phone-number/ Connect Caller].

[[Category:Theatres in Warwickshire]]
[[Category:Buildings and structures in Nuneaton]]