'''Jack Daws''' (born June 9, 1970)<ref>[http://jackdawsart.com/press.html jackdawsart.com]</ref> is a Seattle-based American artist. Working with assisted readymades, mixed media sculpture, and photography, his work addresses a range of socio-political and cultural issues. He lives and works on [[Vashon Island, Washington]].

== Early life and education ==

Daws was born in 1970 in [[Pulaski County, Kentucky]], where he lived until 1991. At age twenty he accepted a scholarship to attend the [[Atlanta College of Art]], but dropped out before completing the first semester.<ref>Hackett, Regina. (May 16, 2007) [http://blog.seattlepi.com/art/2007/05/16/the-pennies-of-jack-daws/ The Pennies of Jack Daws] ''Seattle Post-Intelligencer''.</ref>

== Art practice ==

Much of Daws's work reflects his interest in socio-political issues, particularly those of racial and cultural identity. Some of his most controversial works play on [[stereotypes]] of African American, Native American, and Mexican American culture. ''Seattle Post-Intelligencer'' art critic, Regina Hackett writes: "In Seattle, no white artist has pushed the edge of racial outrage as clearly as Jack Daws, who is hardwired to go over the line... He's not expressing hatred or taking a fast ride on any one hot button. All hot buttons are dear to him."<ref>Hackett, Regina. (November 6, 2008) [http://blog.seattlepi.com/art/2008/11/06/nayland-blake-and-jack-daws-race-and-representation/ Nayland Blake and Jack Daws: Race and Representation] ''Seattle Post-Intelligencer''.</ref>

Other works have addressed the [[September 11 attacks]] (''Two Towers'', 2003), the [[Florida election recount]] in the 2000 U.S. presidential race (''Serfs Up!'', 2004), [[police brutality]] (''Better You Than Me'', 2007), the [[war on drugs]] (''Misdemeanor Sculpture'', 2001, ''Anywhere But Here'', 2002, ''Ceci N'est Pas Une Bong'', 2004), and [[Israel–United States relations]] (''King of Israel'', 2007). To date however, the work Daws is perhaps best known for is his piece entitled ''Counterfeit Penny'', 2006.

== ''Counterfeit Penny'', 2006 ==

In 2006, Daws hired metalsmiths to make a mold of a 1970 U.S. penny and cast it in 18-karat gold. He then hired another metalsmith to copper plate it. Wanting to see how it would age, he carried the sculpture in his pocket for six months, during which time it developed a brown patina just like an ordinary penny. On March 28, 2007, Daws intentionally put the 'penny' in circulation at Los Angeles International Airport. It was discovered in Brooklyn two and a half years later by Jessica Reed, a graphic designer and coin collector. Reed noticed it while paying for groceries at a Greenpoint C-Town. It seems that either the copper plating had begun to wear off, revealing the gold underneath, or it had oxidized in such a way as to give it a golden tint. Either way, it caught Reed's eye and she held on to it.

Reed carried the 'coin' in a change purse for months before doing a quick Internet search for "gold penny 1970." Within minutes she was directed to the website of Daws's Seattle art dealer, the Greg Kucera Gallery.<ref>[http://www.gregkucera.com/daws.htm Greg Kucera Gallery]</ref> There she read a copy of a 2007 press release that described the sculpture as being a little smaller than a real penny, and almost twice as heavy. Reed measured it and weighed it. In disbelief, she contacted the Kucera Gallery, and was put in touch with Daws. After a brief telephone conversation Daws confirmed that she had discovered the ''Counterfeit Penny'' sculpture.  Jennifer 8. Lee of the ''New York Times'' wrote: "Most counterfeiting takes something that is nearly worthless and turns it into something perceived to have value. Mr. Daws did just the opposite. He took value &ndash; approximately $100 worth of gold &ndash; and turned it into something perceived as nearly worthless, one cent."<ref>8. Lee, Jennifer. (November 4, 2009) [http://cityroom.blogs.nytimes.com/2009/11/04/maker-learns-the-fate-of-a-penny-made-of-gold/?_r=0 Brooklyn Woman Finds Counterfeit Penny Made of Gold] ''New York Times''.</ref>

== Awards/collections ==

Daws was awarded a [[Pollock-Krasner Foundation]] Grant<ref>[http://www.pkf-imagecollection.org/artist/Jack_Daws/works/#!4200 Pollock-Krasner Foundation]</ref> in 2006 and an [[Artist Trust]]/Washington State Arts Commission Fellowship<ref>[http://www.artisttrust.org/index.php/award-winners/artist-profile/jack_daws Artist Trust]</ref> in 2008. He was a finalist for a 2013 Neddy at Cornish Award<ref>[http://www.cornish.edu/news/article/the_neddy_at_cornish_announces_recipients_of_the_25000_neddy_at_cornish_201/ Neddy at Cornish Award, Cornish College of the Arts]</ref> and received a 2013 Special Recognition Betty Bowen Award<ref>[http://www.seattleartmuseum.org/Visit/bowen.asp Betty Bowen Award, Seattle Art Museum]</ref> from the [[Seattle Art Museum]].

His work is in the permanent collections of the University of Washington's [[Henry Art Gallery]], the [[Tacoma Art Museum]], and Western Bridge (William and Ruth True Collection).<ref>[http://www.gregkucera.com/daws_resume.htm Greg Kucera Gallery]</ref>

His work was also included in the 2013 book ''Wild Art'', published by [[Phaidon Press]].<ref>[http://www.phaidon.com/store/art/wild-art-9780714865676/ ''Wild Art'', [[Phaidon Press]], 2013]</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://www.jackdawsart.com}}

{{DEFAULTSORT:Daws, Jack}}
[[Category:1970 births]]
[[Category:Living people]]
[[Category:Artists from Kentucky]]
[[Category:Artists from Washington (state)]]
[[Category:Artists from Seattle]]
[[Category:American conceptual artists]]
[[Category:Sculptors from Kentucky]]
[[Category:Sculptors from Washington (state)]]