{{Infobox journal
| title = Letters in Mathematical Physics
| cover = [[File:Letters in Mathematical Physics cover.jpg]]
| editor = V. Bach; E. Frenkel; M. Kontsevich; D. Kreimer; N.A. Nekrasov; M. Porrati; D. Sternheimer
| discipline = [[Mathematical physics]]
| abbreviation = Lett. Math. Phys.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Monthly
| history = 1975-present
| openaccess =
| impact = 1.819
| impact-year = 2011
| website = http://www.springer.com/journal/11005/
| link1 = http://www.springerlink.com/content/0377-9017
| link1-name = online access
| link2 =
| link2-name =
| JSTOR =
| OCLC  = 37915830
| LCCN  =
| CODEN =
| ISSN  = 0377-9017
| eISSN = 0377-9017
}}
'''''Letters in Mathematical Physics''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in [[mathematical physics]] published by [[Springer Science+Business Media]]. It publishes letters and longer research articles, occasionally also articles containing topical reviews. It is essentially a platform for the rapid dissemination of short contributions in the field of mathematical physics. In addition, the journal publishes contributions to modern mathematics in fields which have a potential physical application, and developments in theoretical physics which have potential mathematical impact. The [[editor in chief|editors]] are Volker Bach, Edward Frenkel, [[Maxim Kontsevich]], [[Dirk Kreimer]], Nikita Nekrasov, [[Massimo Porrati]], and Daniel Sternheimer.

==Abstracting and indexing ==
The following services abstract or index ''Letters in Mathematical Physics'': [[InfoTrac|Academic OneFile]], [[EBSCO Publishing|Academic Search]], [[Astrophysics Data System]], [[Chemical Abstracts Service]], [[Current Contents]]/Physical, Chemical and Earth Sciences, [[Current Index to Statistics]], [[EBSCO Industries|EBSCO]], EI-[[Compendex]], [[International Nuclear Information System|INIS Atomindex]], [[Inspec]], [[Mathematical Reviews]], [[ProQuest]], [[Science Citation Index]], [[Scopus]], [[Serials Solutions|Summon by Serial Solutions]], and [[Zentralblatt MATH]]. According to the [[Journal Citation Reports]], its 2011 [[impact factor]] is 1.819 and 2012 impact factor is 2.415.

== External links ==
* {{Official|http://www.springer.com/journal/11005/}}

[[Category:Physics journals]]
[[Category:Mathematics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1975]]
[[Category:English-language journals]]