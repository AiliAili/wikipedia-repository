{{Infobox military conflict
| conflict    = Battle of Oriskany
| partof      = the [[American Revolutionary War]]
| image       = [[File:Herkimer at oriskany.jpg|300px]]
| caption     = ''[[Nicholas Herkimer|Herkimer]] at the Battle of Oriskany''<br>Painting by [[Frederick Coffay Yohn|F.C. Yohn]], c. 1901,<br> now in the public library in [[Utica, New York]].<ref name="Dieffenbacher12">[[#Dieffenbacher|Dieffenbacher]], p. 12</ref>
| date        = August 6, 1777
| place       = near [[Oriskany, New York]];<br>in present-day [[Whitestown, New York|Whitestown]] / [[Rome, New York|Rome]],<br>[[Oneida County, New York]]
| coordinates = {{coord|43|10.6|N|75|22.2|W|region:US_type:city_scale:10000|display=inline,title}}
| result      = Anglo-Indian tactical victory<br>American strategic victory
| combatant1  = {{flag|United States|1777}}
----
[[Oneida people|Oneida]] {{refn|[[Oneida tribe|Oneida]]}}
| combatant2  = {{flagcountry|Kingdom of Great Britain}}<br>
* {{flagicon|Kingdom of Great Britain}} [[Loyalist (American Revolution)|Loyalist]]s
* {{Flagicon image|Flag of Hesse.svg|size=23px}} [[Landgraviate of Hesse-Kassel|Hesse]]
----
[[File:Flag_of_the_Iroquois_Confederacy.svg|23px]] [[Iroquois Confederacy|Iroquois]]{{refn|[[Mohawk nation|Mohawk]], [[Seneca Nation|Seneca]]}}
[[Native Americans in the United States|Native Americans]]{{refn|[[Wyandot people|Huron]], [[Nipissing First Nation]]}}
| commander1  = {{flagicon|United States|1777}} [[Nicholas Herkimer]]{{KIA}}<br> {{flagicon|United States|1777}} [[Peter Gansevoort]]
| commander2  = {{flagicon|Kingdom of Great Britain}} [[Sir John Johnson, 2nd Baronet|Sir John Johnson]]<br/>
{{flagicon|Kingdom of Great Britain}} [[John Butler (pioneer)|John Butler]]
----
[[File:Flag_of_the_Iroquois_Confederacy.svg|23px]] [[Joseph Brant]]
| strength1   = 720-740 militia<br>60-100 Indians <ref name="GlatthaarUSStrength">Figures obtained from [[#Glatthaar|Glatthaar (2006)]], pp. 160,356.  He notes on p. 356 that 10–20% of Herkimer's column falls out before reaching the battlefield.  The Oneida figures are his estimate, based in part on oral tradition, and actually include at least one Mohawk, Louis Atayataronghta (see battle description for quote).</ref>
| strength2   = 500 regulars, militia and Indians <ref name="Glatthaar164"/>
| casualties1 = 385 killed<br>50 wounded<br>30 captured<ref name="Watt316_20">[[#Watt|Watt (2002)]] pp. 316–320</ref>  
| casualties2 = Indians: 65 killed or wounded<br>British: 7 killed, 21 wounded, missing, or captured<ref name="Watt194">[[#Watt|Watt (2002)]], p. 194</ref>
| campaignbox = {{Campaignbox American Revolutionary War: Saratoga campaign}}
}}

The '''Battle of Oriskany''', fought on August 6, 1777, was one of the bloodiest battles in the North American         [[theater (warfare)|theater]] of the [[American Revolutionary War]] and a significant engagement of the [[Saratoga campaign]]. An American party trying to relieve the siege of [[Fort Stanwix]] was ambushed by a party of [[Loyalist (American Revolution)|Loyalist]]s and allies of several Native American tribes. This was one of the few battles in the war in which almost all of the participants were North American: Loyalists and allied Indians fought against [[Patriot (American Revolution)|Patriot]]s and allied Oneida in the absence of [[Kingdom of Great Britain|British]] soldiers.

Early in the [[siege of Fort Stanwix]], an American relief force from the [[Mohawk Valley]] under General [[Nicholas Herkimer]], numbering around 800 men of the [[Tryon County militia]], and a party of [[Oneida tribe|Oneida]] warriors, approached in an attempt to raise the siege. British commander [[Barry St. Leger]] authorized an intercept force consisting of a [[Hanau]] [[jäger (military)|Jäger]] (light infantry) detachment, [[Sir John Johnson, 2nd Baronet|Sir John Johnson]]'s [[King's Royal Regiment of New York]], Indian allies from the [[Iroquois|Six Nations]], particularly [[Mohawk nation|Mohawk]] and [[Seneca people|Seneca]]; and other tribes to the north and west, and Indian Department Rangers, totaling at least 450 men.

The Loyalist and Indian force ambushed Herkimer's force in a small valley about six miles (10&nbsp;km) east of [[Fort Stanwix]], near the present-day village of [[Oriskany, New York]]. During the battle, Herkimer was mortally wounded.  The battle cost the Patriots approximately 450 casualties, while the Loyalists and Indians lost approximately 150 dead and wounded.  The result of the battle remains ambiguous because the apparent Loyalist victory was significantly affected by a sortie from Fort Stanwix in which the Loyalist camps were sacked, spoiling morale among the allied Indians.

For the [[Iroquois|Iroquois nations]], the battle marked the beginning of a [[civil war]], as Oneida warriors under [[Colonel Louis]] and [[Han Yerry]] allied with the American cause and fought against members of other Iroquois nations. There were also internal divisions among the Oneida, some of whom went to Canada as allies of the British. The site is known in oral histories of the Iroquois nations as "A Place of Great Sadness."<ref name="sadness">[[#Bilharz|Bilharz (2009)]], p. 93.</ref> The site has been designated as a [[National Historic Landmark]] and is marked by a battle monument at the [[Oriskany Battlefield State Historic Site]].

== Background ==
{{further|Saratoga campaign|Siege of Fort Stanwix}}
In June 1777, the [[British Army during the American War of Independence|British Army]], under the command of [[John Burgoyne|General "Gentleman Johnny" Burgoyne]], launched a two-pronged attack from [[Province of Quebec (1763-1791)|Quebec]]. Burgoyne's objective was to split [[New England]] from the other colonies by gaining control of [[New York (state)|New York]]'s [[Hudson Valley]]. The main thrust came south across [[Lake Champlain]] under Burgoyne's command; the second thrust was led by Lt. Colonel [[Barry St. Leger]] and was intended to come down the [[Mohawk Valley]] and meet Burgoyne's army near [[Albany, New York|Albany]].<ref name="K84">[[#Ketchum|Ketchum (1997)]], p. 84</ref>

St. Leger's expedition consisted of about 1,800 men, who were a mix of British [[regular army|regulars]], [[Hessian (soldiers)|Hessian]] Jägers from Hanau, Loyalists, Indians of several tribes, including the Mohawk and Seneca of the Iroquois, and Rangers.  They traveled up the [[Saint Lawrence River]] and along the shore of [[Lake Ontario]] to the [[Oswego River (New York)|Oswego River]], which they ascended to reach the [[Oneida Carry]] (present-day [[Rome, New York]] later developed here). They began to [[Siege of Fort Stanwix|besiege Fort Stanwix]], a [[Continental Army]] post guarding the [[portage]].<ref name="Nickerson195_9">[[#Nickerson|Nickerson (1967)]], pp. 195–199</ref>

==Prelude==
Alerted to the possibility of a British attack along the [[Mohawk River]], [[Nicholas Herkimer]], the head of [[Tryon County, New York|Tryon County]]'s [[Committee of Safety (American Revolution)|Committee of Safety]], issued a proclamation on July 17 warning of possible military activity and urging the people to respond if needed.<ref name="Glatthaar159_60">[[#Glatthaar|Glatthaar (2006)]], pp. 159–160</ref>  Warned by friendly Oneidas on July 30 that the British were just four days from Fort Stanwix, Herkimer put out a call-to-arms.  The force raised totaled 800 from the [[Tryon County militia]]; it was composed primarily of poorly trained farmers, who were chiefly of [[German Palatines|Palatine German]] descent. Setting out on August 4, the column camped near the Oneida village of Oriska on August 5.  While a number of the militia dropped out of the column due to their lack of conditioning, Herkimer's forces were augmented by a company of 60 to 100 Oneida warriors, led primarily by Han Yerry, a strong supporter of the Patriot cause.<ref name="Glatthaar160">[[#Glatthaar|Glatthaar (2006)]], p. 160</ref>  That evening, Herkimer sent three men toward the fort with messages for the fort's commander, Colonel [[Peter Gansevoort]].  Gansevoort was to signal the receipt of the message with three cannon shots, and then [[sortie]] to meet the approaching column.<ref name="Glatthaar160"/>  Due to difficulties in penetrating the British lines, these couriers did not deliver the message until late the next morning, after the battle was already underway.<ref name="Glatthaar161">[[#Glatthaar|Glatthaar (2006)]], p. 161</ref>

[[File:Oriskany creek.jpg|thumb|250px|right|The site of the ambush at [[Oriskany Creek|Bloody Creek]], New York.]]
St. Leger learned on August 5 from a messenger sent by [[Molly Brant]] to her brother [[Joseph Brant]], the [[Mohawk nation|Mohawk]] leader who led a portion of St. Leger's Indian contingent, that Herkimer and his relief expedition were on their way.<ref name="Glatthaar163">[[#Glatthaar|Glatthaar (2006)]], p. 163</ref> St. Leger sent a detachment of light infantry from [[Sir John Johnson, 2nd Baronet|Sir John Johnson]]'s [[King's Royal Regiment of New York|Royal Yorkers]] toward the position that evening to monitor Herkimer's position, and Brant followed early the next morning with about 400 Indians and [[Butler's Rangers]].  Although many of the Indians were armed with [[musket]]s, some were not, and only carried [[tomahawk (axe)|tomahawk]] and spear.<ref name="Glatthaar164">[[#Glatthaar|Glatthaar (2006)]], p. 164</ref>

==Battle==
On the morning of August 6, Herkimer held a war council.  Since his force had not yet heard the expected signal from the fort, he wanted to wait.  However, his captains pressed him to continue, accusing Herkimer of being a [[Loyalist (American Revolution)|Tory]] because his brother was serving under St. Leger.<ref name="Nickerson202">[[#Nickerson|Nickerson (1967)]], p. 202</ref>  Stung by these accusations, Herkimer ordered the column to march on toward Stanwix.<ref name="Nickerson203">[[#Nickerson|Nickerson (1967)]], p. 203</ref>

About six miles (9.6&nbsp;km) from the fort, the road dipped more than fifty feet (15 m) into a marshy ravine, where a stream about three feet (1 m) wide meandered along the bottom.<ref name="Nickerson205"/>  [[Sayenqueraghta]] and [[Cornplanter]], two Seneca war chiefs, chose this place to set up an ambush.<ref name="Watt135">[[#Watt|Watt (2002)]], p. 135</ref>  While the King's Royal Yorkers waited behind a nearby rise, the Indians concealed themselves on both sides of the ravine.  The plan was for the Yorkers to stop the head of the column, after which the Indians would attack the extended column.<ref name="Nickerson205">[[#Nickerson|Nickerson (1967)]], p. 205</ref>  At about 10 am, Herkimer's column, with Herkimer on horseback near the front, descended into the ravine, crossed the stream, and began ascending the other side.<ref name="Glatthaar163"/>

[[File:Herkimer monument.jpg|thumb|left|300px|upright|Monument marking location of tree to which Herkimer was taken]]
Contrary to the plan, the Indians lying in wait near the rear of the column, apparently unable to contain themselves any longer, opened fire, taking the column completely by surprise.  Leading the 1st Regiment (Canajoharie district), Colonel Ebenezer Cox was shot off his horse and killed in the first volley.  Herkimer turned his horse to see the action, and was very shortly thereafter struck by a ball, which shattered his leg and killed the horse.<ref name="Glatthaar166">[[#Glatthaar|Glatthaar (2006)]], p. 166</ref>  He was carried by several of his officers to a beech tree, where his men urged him to retire from further danger. He defiantly replied, "I will face the enemy", and calmly sat leaning against the tree, smoking a pipe and giving directions and words of encouragement to the men nearby.<ref name="Nickerson207">[[#Nickerson|Nickerson (1967)]], p. 207</ref>

As the trap had been sprung too early, portions of the column had not yet entered the ravine.<ref name="Glatthaar166"/>  Most of these men panicked and fled; some of the attacking Indians pursued them, resulting in a string of dead and wounded that extended for several miles.<ref name="Nickerson206">[[#Nickerson|Nickerson (1967)]], p. 206</ref>  Between the loss of the column rear and those killed or wounded in the initial volleys, only about one half of Herkimer's men were likely still fighting thirty minutes into the battle.<ref name="Glatthaar166"/>  Some of the attackers, notably those not armed with muskets, waited for the flash of an opponent's musket fire before rushing to attack with the tomahawk before the enemy had time to reload, a highly effective tactic against those men who did not have [[bayonet]]s.<ref name="Glatthaar166"/><ref name="Nickerson208">[[#Nickerson|Nickerson (1967)]], p. 208</ref>  Louis Atayataronghta, a Mohawk warrior fighting with Herkimer's men, shot one of the enemy whose fire had been devastating in its accuracy, noting that "every time he rises up he kills one of our men".<ref name="Glatthaar167">[[#Glatthaar|Glatthaar (2006)]], p. 167</ref>

Herkimer's men eventually rallied, fighting their way out of the ravine to the crest just to its west.  John Johnson, concerned about the militia's tenacity, returned to the British camp and requested some reinforcements from St. Leger shortly before a thunderstorm broke out.  Another seventy men headed back with him toward the battle.<ref name="Watt174">[[#Watt|Watt (2002)]], p. 174</ref>  The thunderstorm caused a one-hour break in the fighting,<ref name="Nickerson207"/> during which Herkimer regrouped his militia on the higher ground.  He instructed his men to fight in pairs: while one man fired and reloaded, the other waited and then only fired if attacked. Ordered to fire in relays, the pairs were to try to keep at least one weapon loaded at all times, to reduce the effectiveness of the tomahawk attacks.<ref name="Nickerson208"/>

[[John Butler (pioneer)|John Butler]], the leader of the rangers, took time during the thunderstorm to question some of the captives, and learned about the three-cannon signal.  When Johnson and his reinforcements arrived, Butler convinced them to turn their coats inside out to disguise themselves as a relief party coming from the fort.<ref name="Watt17980">[[#Watt|Watt (2002)]], pp. 179–180</ref> When the fighting restarted, Johnson and the rest of his Royal Yorkers joined the battle, but one of the Patriot militiamen, Captain Jacob Gardinier, recognized the face of a Loyalist neighbor.  Close combat, at times hand-to-hand fighting between neighbors, continued for some time.<ref name="Nickerson208"/><ref name="Glatthaar168">[[#Glatthaar|Glatthaar (2006)]], p. 168</ref>

[[File:Earl Ralph Marinus Willett.jpg|thumb|right|215px|Lt. Col. [[Marinus Willett]], a 1791 portrait by [[Ralph Earl (artist)|Ralph Earl]]]]

===Sortie from Fort Stanwix===
When Herkimer's messengers reached the fort around 11 am, Colonel Gansevoort began organizing the requested sortie.  After the heavy thunderstorm passed, Lieutenant Colonel [[Marinus Willett]] led 250 men from the fort, and proceeded to raid the nearly deserted enemy camps to the south of the fort.  Driving away the few British and Indians left in those camps (who included women) and taking four prisoners along the way,<ref name="Glatthaar171">[[#Glatthaar|Glatthaar (2006)]], p. 171</ref> the Patriots collected blankets and other personal possessions from the Indian camps. They also successfully raided John Johnson's camp, taking his letters and other writings (including an intercepted letter to Gansevoort from his fiancée).<ref name="Nickerson210">[[#Nickerson|Nickerson (1967)]], p. 210</ref><ref name="Watt196">[[#Watt|Watt (2002)]], p. 196</ref>

One of the Indians who had stayed behind to guard the camp ran to the battlefield to alert fellow warriors that their camps were being raided.<ref name="Watt185">[[#Watt|Watt (2002)]], p. 185</ref> They disengaged with cries of "''Oonah, oonah!''", the [[Seneca nation|Seneca]] signal to retire, and headed for the camps to protect their women and possessions.  This forced the smaller number of German and Loyalist combatants to also withdraw.<ref name="Nickerson208"/>

==Aftermath==

===Patriots===
The battered remnant of Herkimer's force, with Herkimer seriously wounded and many of its captains killed, retreated to [[Fort Dayton]].  The wounded Herkimer was carried by his men from the battlefield. His leg was amputated, but the operation went poorly and he died on August 16.<ref name="Glatthaar169">[[#Glatthaar|Glatthaar (2006)]], p. 169</ref>  While the Indians retrieved most of their dead from the battlefield the following day, many dead and wounded Patriots were left on the field.  When [[Benedict Arnold]]'s relief column marched through the scene several weeks later, the stench and grisly scene was, according to various accounts, quite memorable.<ref name="Watt263">[[#Watt|Watt (2002)]], p. 263</ref>

[[File:Joseph Brant painting by George Romney 1776 (2).jpg|thumb|left|upright|215px|The [[Mohawk nation|Mohawk]] leader Chief [[Joseph Brant]], 1776 portrait by [[George Romney (painter)|George Romney]]]]
When General [[Philip Schuyler]] learned of the retreat from Oriskany, he immediately organized additional relief to be sent to the area.  The siege at Fort Stanwix was eventually lifted on August 21 when a relief column led by General [[Benedict Arnold]] approached.  While still at Fort Dayton, Arnold sent messengers into the British camp who convinced the British and Indian besiegers that his force was much larger than it was in fact.<ref name="Glatthaar174_5">[[#Glatthaar|Glatthaar (2006)]], pp. 174–5</ref>

===Loyalists===
Loyalist [[John Butler (pioneer)|John Butler]] was promoted to [[Lieutenant Colonel]] for his role in the battle, and authorized to raise a regiment that became known as [[Butler's Rangers]].<ref name="Bowler">[[#Bowler|Bowler (2000)]]</ref>  After the siege was lifted, some Loyalists returned to Quebec. Others (including numerous warriors from various tribes) joined Burgoyne's campaign on the Hudson.<ref name=Watt269>[[#Watt|Watt (2002)]], p. 269</ref>

===Native Americans===
Brant and [[Sayenqueraghta]], the principal Seneca chief, proposed the next day to continue the fighting by pursuing the Colonials downriver toward [[German Flatts, New York|German Flatts]] but St. Leger turned their proposal down.<ref name="Kelsay208">[[#Kelsay|Kelsay (1984)]], p. 208</ref> This battle marked the beginning of the civil war in the Iroquois Confederacy. It was the first time that their peoples had fought against each other. Four of the Iroquois nations: Mohawk, Seneca, Cayuga and Onondaga, were allied with the British, as were some Oneida. The Iroquois in St. Leger's camp met in council and decided to send the rebel-allied Oneida a bloody hatchet.<ref name="Watt196"/> Brant's Mohawks raided and burned the Oneida settlement of Oriska later in the siege. In retaliation, the Oneida plundered the Mohawk castles of Tiononderoge and Canajoharie.  They later raided the Fort Hunter Mohawk, prompting most of the remaining Mohawk in central New York to flee to Quebec.<ref name="Glatthaar177">[[#Glatthaar|Glatthaar (2006)]], p. 177</ref>

According to a mid-19th century account, Brant's Indians were said to have tortured and eaten some of their prisoners.<ref name="Stone459_60">[[#Stone|Stone (1865)]], pp. 459–460</ref> However, modern historians dispute this.  It is likely that some of the prisoners taken were ritually killed (which to Europeans is extremely similar to torture); there does not appear to be any evidence of cannibalism (ritual or otherwise). John Butler reported that four prisoners held by the Indians "were conformable to the Indian custom afterwards killed."<ref name="Watt197">[[#Watt|Watt (2002)]], p. 197</ref>

===Winners and losers===
The battle was, based on the percentage of casualties suffered, one of the bloodiest of the war.<ref>[[#Canfield|Canfield (1909)]], p. 57</ref>  About half of Herkimer's force was killed or wounded, as was about 15% of the British force.<ref name="Watt316_20"/><ref name="Watt194"/>

St. Leger claimed the battle as a victory, as he had stopped the American relief column, but the Americans maintained control of the battlefield after the withdrawal of the opposing Indians.  The British victory was tempered by the discontent of the Indians after the battle. When they joined the expedition, they expected that the British forces would do most of the fighting. They were the dominant fighters in this action, and some suffered the loss of their personal belongings taken during the American sortie from the fort.  This blow to their morale contributed to the eventual failure of St. Leger's expedition.<ref name="Nickerson211">[[#Nickerson|Nickerson (1967)]], p. 211</ref>

==Legacy==
[[File:Oriskany officers.jpg|thumb|right|250px|Monument to the unknown Tryon County patriots]]
[[Governor Blacksnake|Blacksnake]], one of the Indians at the battle, was interviewed many years afterwards. He recalled, "I thought at that time the Blood Shed a Stream running down on the {{sic|dec|ending}} ground."<ref name="Watt177">[[#Watt|Watt (2002)]], p. 177</ref>  A monument commemorating the battle was erected in 1884 at 43°10.6'N 75°22.2'W,<ref>[[#NYHS14|Cookinham (1915)]], p. 340.</ref> and much of the battlefield is now preserved in the [[Oriskany Battlefield State Historic Site]].  The site was recognized as a [[National Historic Landmark]] in 1962,<ref name="nhlsum">[[#NHLSUM|NHL summary listing]]</ref> and added to the [[National Register of Historic Places]] in 1966.<ref name="nris">[[#NRHP|NRHP]]</ref>

Nicholas Herkimer was honored when the [[Herkimer (town), New York|town of Herkimer]] and [[Herkimer County, New York]] were named for him.<ref>[[#Benton|Benton (1856)]], p. 164</ref>

==Representation in popular culture==
*Colonial settlement and wars in the Mohawk Valley, including the Battle of Oriskany, were memorialized by [[Walter D. Edmonds]] in his 1937 novel, ''[[Drums Along the Mohawk (novel)|Drums Along the Mohawk]]'' and its film adaptation.  
*The battle was honored by the naming of the aircraft carrier {{USS|Oriskany|CV-34|6}}, launched in 1945.<ref>[[#DANFS|Oriskany DANFS entry]]</ref>
*The battle acts as the crux of [[Charles Fenno Hoffman]]'s 1840 novel ''Greyslaer''.

== See also ==
*[[Adam Helmer|Adam F. Helmer]], one of Herkimer's messengers
*[[Sampson Sammons]], a Colonel

== Footnotes ==
{{reflist|colwidth=24em}}

== References ==
{{refbegin}}
*{{cite book |title=A history of Herkimer County|url=https://books.google.com/books?id=G1IOAAAAIAAJ&pg=PA164#v=onepage|last=Benton|first=Nathaniel Soley|year=1856|location=Albany|publisher=J. Munsell|ref=Benton|oclc=1634048}}
*{{cite book |url=http://www.nps.gov/fost/historyculture/upload/oriskany_report.pdf |first=Joy |last=Bilharz |title=Oriskany: A Place of Great Sadness/ A Mohawk Valley Battlefield Ethnography |publisher=National Park Service |date=February 2009 |accessdate=2016-08-21 |oclc=464237289}}
*{{cite DCB|url=http://www.biographi.ca/en/bio/butler_john_1796_4E.html|title=Butler, John |volume=4|first=R. Arthur|last=Bowler|last2=Wilson |first2=Bruce G.|ref=Bowler|accessdate=2010-02-17}}
*{{cite book |url=https://books.google.com/books?id=0CsVAAAAYAAJ&dq=history%20of%20oneida%20county&pg=PA57#v=onepage&q=bloody%20creek&f=false |title=Things worth knowing about Oneida County|first=William Walker |last=Canfield |ref=Canfield |last2=Clark |first2=J. E. |year=1909 |publisher=T. J. Griffiths |location=Utica, New York |oclc=6674932}}
*{{cite book |chapterurl=https://books.google.com/books?id=WtwTAAAAYAAJ&dq=oriskany%201884&pg=PA340#v=onepage&q=oriskany%201884&f=false |first=Henry J. |last=Cookinham |chapter=The Battle of Oriskany |title=Proceedings of the New York Historical Association, Volume 14 |ref=NYHS14 |publisher=New York Historical Association |date=1915}}
*{{cite book|title=Herkimer County: Valley Towns|first=Jane W|last=Dieffenbacher|author2=Herkimer County Historical Society|publisher=Arcadia Publishing|year=2002|isbn=978-0-7385-0977-8|ref=Dieffenbacher|oclc=50147332|location=Charleston, SC}}
*{{cite book|first=Allan D|last=Foote|title=Liberty March, The Battle of Oriskany|year=1998|publisher=North Country Books|isbn=0-925168-72-6|ref=Foote}}
*{{cite book|title=Forgotten Allies: The Oneida Indians and the American Revolution|first=Joseph T|last=Glatthaar|author2=Martin, James Kirby|ref=Glatthaar|publisher=Hill and Wang|location=New York|year=2006|isbn=978-0-8090-4601-0|oclc=63178983}}
*{{cite book|first=Nelson|last=Greene|title=History Of The Mohawk Valley, Gateway To The West, 1614–1925|year=1925|publisher=Reprint Services Corp|isbn=0-7812-5180-X|ref=Greene}}
*{{cite book|first=Isabel|last=Kelsay|title=Joseph Brant 1743–1807 Man of Two Worlds|year=1984|isbn=0-8156-0182-4|ref=Kelsay|publisher=Syracuse University Press|location=Syracuse, NY}}
*{{cite book|first=Alice P|last=Kenney|title=Stubborn for Liberty: The Dutch in New York|year=1975|publisher=Syracuse University Press|location=Syracuse, NY|isbn=0-8156-0113-1|ref=Kenney}}
*{{cite book|title=Saratoga: Turning Point of America's Revolutionary War|first=Richard M|last=Ketchum|publisher=Henry Holt|year=1997|isbn=978-0-8050-6123-9|ref=Ketchum|location=New York|oclc=41397623}}
*{{NRISref|2007a}}
*{{cite web|url=http://tps.cr.nps.gov/nhl/detail.cfm?ResourceId=401&ResourceType=Site|title=Oriskany Battlefield|date=2007-09-11|work=National Historic Landmark summary listing|publisher=National Park Service|ref=NHLSUM}}
*{{cite DANFS|url=http://www.history.navy.mil/danfs/o4/oriskany.htm|title=Oriskany|accessdate=2009-12-07|ref=DANFS}}
*{{cite book|title=The Turning Point of the Revolution|first=Hoffman|last=Nickerson|publisher=Kennikat|year=1967 |orig-year=1928|location=Port Washington, NY|oclc=549809|ref=Nickerson}}
*{{cite book|first=William Leete|last=Stone|authorlink=William Leete Stone, Sr.|title=Life of Joseph Brant|year=1865 |orig-year=1838|ref=Stone|url=https://books.google.com/books?id=FL0RAAAAYAAJ|publisher=J. Monsell|location=Albany, NY|oclc=3509591}}
*{{cite book|first=Gavin K|last=Watt|title=Rebellion in the Mohawk Valley: The St. Leger Expedition of 1777|location=Toronto|publisher=Dundurn|year=2002|isbn=1-55002-376-4|oclc=49305965|ref=Watt}}
{{refend}}

==Further reading==
{{refbegin}}
*{{cite book |last=Berleth|first=Richard|title=Bloody Mohawk: the French and Indian War & American Revolution on New York's Frontier |publisher=Black Dome Press |location=Hensonville, NY |year=2009 |isbn=978-1-883789-66-4 |oclc=454376897}}
*{{cite book|first=Walter D|last=Edmonds|authorlink=Walter D. Edmonds|title=[[Drums Along the Mohawk (novel)|Drums Along the Mohawk]]|year=1937|publisher=Little, Brown|isbn=0-8156-0457-2}}
{{refend}}

== External links ==
{{Wikisource1911Enc|Oriskany}}
{{commonscat|Battle of Oriskany}}
* [http://www.royalyorkers.ca/  The King's Royal Regiment of New York]
* [https://web.archive.org/web/20090531145432/http://www.nps.gov:80/history/nr/twhp/wwwlps/lessons/79oriskany/79oriskany.htm ''The Battle of Oriskany: "Blood Shed a Stream Running Down",'' a National Park Service Teaching with Historic Places (TwHP) lesson plan]
* {{Find a Grave|50519849|Captain Johannes Dygert killed at Oriskany}}

{{New York in the American Revolutionary War}}

{{good article}}

{{DEFAULTSORT:Battle Of Oriskany}}
[[Category:1777 in New York]]
[[Category:Battles involving Great Britain|Oriskany]]
[[Category:Battles involving the Iroquois|Oriskany]]
[[Category:Battles involving the United States|Oriskany]]
[[Category:Battles of the American Revolutionary War|Oriskany|History of New York]]
[[Category:Conflicts in 1777]]
[[Category:New York in the American Revolution]]
[[Category:Oneida County, New York]]
[[Category:August 1777 events]]