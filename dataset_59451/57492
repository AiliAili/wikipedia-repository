[[File:Iags-logo.jpg|thumbnail|International Association of Genocide Scholars logo]]

The '''International Association of Genocide Scholars''' (IAGS) is an international [[non-partisan]] organization that seeks to further research and teaching about the nature, causes, and consequences of [[genocide]], and to advance policy studies on the prevention of genocide.<ref name="Forsythe2009">{{cite book |first1=David P. |last1=Forsythe |title=Encyclopedia of Human Rights |url=https://books.google.com/books?id=1QbX90fmCVUC&pg=RA4-PA476 |year=2009 |publisher=[[Oxford University Press]] |isbn=978-0-19-533402-9 |pages=4–}}</ref><ref name="Totten2007">{{cite book |first1=Samuel |last1=Totten |authorlink=Samuel Totten |title=The Prevention and Intervention of Genocide: An Annotated Bibliography |url=https://books.google.com/books?id=pPK_8vRHWw0C&pg=PA1097 |year=2007 |publisher=[[Routledge]] |isbn=978-0-415-95358-0 |pages=1097–}}</ref><ref name="TottenPedersen2012">{{cite book |first1=Samuel |last1=Totten |first2=Jon E. |last2=Pedersen |title=Educating about Social Issues in the 20th and 21st Centuries: A Critical Annotated Bibliography |url=https://books.google.com/books?id=pDvaJLsGE40C&pg=PA422 |date=January 2012 |publisher=IAP |isbn=978-1-61735-572-1 |pages=422–}}</ref><ref name="Bartrop2012">{{cite book |first1=Paul R. |last1=Bartrop |title=A Biographical Encyclopedia of Contemporary Genocide |url=https://books.google.com/books?id=55NPpA6EvyMC&pg=PA389 |date=30 July 2012 |publisher=[[ABC-CLIO]] |isbn=978-0-313-38679-4 |pages=389}}</ref><ref name="Ball2011">{{cite book |first1=Howard |last1=Ball |title=Genocide: A Reference Handbook |url=https://books.google.com/books?id=hpVo-cu8wNUC&pg=PA271 |year=2011 |publisher=ABC-CLIO |isbn=978-1-59884-488-7 |pages=271}}</ref> The association, founded in 1994 by [[Israel Charny]], [[Helen Fein]] (its first president), [[Robert Melson]], and Roger Smith, meets to consider comparative research, recent works, case studies, the links between genocide and other [[human rights]] violations, and prevention and punishment of genocide, information which is published in the official journal of the association. A central aim of the association is to draw academics, activists, artists, genocide survivors, journalists, jurists, and public policy makers into the study of genocide, with prevention as the end goal. Membership is open to interested persons worldwide.

In 1997, the association unanimously passed a formal resolution affirming the [[Armenian Genocide]],<ref name="Lewy2012">{{cite book |first1=Guenter |last1=Lewy |title=Essays on Genocide and Humanitarian Intervention |url=https://books.google.com/books?id=Fv9axBw40EUC&pg=PA5 |date=15 April 2012 |publisher=University of Utah Press |isbn=978-1-60781-187-9 |pages=5}}</ref><ref name="MacDonald2008">{{cite book |first1=David B. |last1=MacDonald |title=Identity Politics in the Age of Genocide: The Holocaust and Historical Representation |url=https://books.google.com/books?id=ZK2WE_2H3UEC&pg=PA135 |year=2008 |publisher=Routledge |isbn=978-0-415-43061-6 |pages=135}}</ref> and also sent an open letter to the [[Prime Minister of Turkey]].<ref name="HenhamBehrens2013">{{cite book |first1=Ralph |last1=Henham |first2=Paul |last2=Behrens |title=The Criminal Law of Genocide: International Comparative and Contextual Aspects |url=https://books.google.com/books?id=tK_pHoNlUYIC&pg=PA17 |date=1 February 2013 |publisher=[[Ashgate Publishing]] |isbn=978-1-4094-9591-8 |pages=17}}</ref> In December 2007 the organization passed another resolution reaffirming the Armenian Genocide and officially recognizing both the [[Greek genocide|Greek]] and [[Assyrian Genocide]]<nowiki>s</nowiki>.<ref>[http://genocidescholars.org/images/PRelease16Dec07IAGS_Officially_Recognizes_Assyrian_Greek_Genocides.pdf International Association of Genocide Scholars] {{webarchive|url=https://web.archive.org/web/20080227043831/http://genocidescholars.org/images/PRelease16Dec07IAGS_Officially_Recognizes_Assyrian_Greek_Genocides.pdf |date=2008-02-27 }}</ref>

[[File:Genocide Scholars-IAGS Visitors Walk Past Memorial Sign - Olimpo Detention and Torture Center - Buenos Aires - Argentina.jpg|thumb|Genocide Scholars-IAGS Visitors Walk Past Memorial Sign - {{Interlanguage link multi|Olimpo Detention and Torture Center|pt|3=El Olimpo}} - Buenos Aires - Argentina]]
The official [[peer-reviewed]] [[academic journal]] of the association is called ''Genocide Studies and Prevention''.<ref>{{cite web |url=http://www.genocidescholars.org/publications/iags-journal |title=IAGS Journal |work=Homepage |publisher=International Association of Genocide Scholars |accessdate=22 March 2015}}</ref>

== Officers ==
Current officers of the association are:
*President, [[William Schabas]]
*First Vice-President, Alex Hinton
*Second Vice-President, Daniel Feierstein
*Secretary/ Treasurer, Jutta Lindert

=== Former presidents ===
The following persons have been president of the association:
*[[Helen Fein]]
*Frank Chalk
*[[Robert Melson]]
*Roger Smith
*[[Israel W. Charny]]
*[[Gregory H. Stanton]]

== References ==
{{Reflist|30em}}

== External links ==
{{Portal|Genocide}}
* {{Official website|http://www.genocidescholars.org/}}
*[https://web.archive.org/web/20130520120915/http://www.yale.edu/gsp/colonial/puerto-rico/ Yale University Genocide Studies on Ponce de Leon]

[[Category:Genocide]]
[[Category:International criminal law]]
[[Category:International learned societies]]
[[Category:Organizations established in 1994]]