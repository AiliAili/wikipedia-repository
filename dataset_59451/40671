{{Infobox company
|name = Marsh, LLC
|type = [[Subsidiary]] ([[Limited liability company|LLC]]) 
|slogan = "We help clients better quantify and manage risk to survive and thrive."
|key_people     = {{plainlist|
* [[Peter Zaffino]] (CEO)
* John Q. Doyle (President)
* Dennis Kane (Senior Vice President)
* Courtney Leimkuhler (CFO)
* David Batchelor (Vice Chairman)
* Ed Dandridge (CMO & CCO)
* Mary Anne Elliott (CHRO)<ref>{{cite 
web|title=Marsh 
Dennis Kane (Senior Vice President) Leadership|url=https://www.marsh.com/us/about-marsh/leadership.html|website=Marsh.com|accessdate=30 June 2016}}</ref>
}}
|revenue  = $5.73 billion (2015)<ref>{{cite news|last1=Hofmann|first1=Mark|title=Marsh & McLennan reports higher quarterly revenue|url=http://www.businessinsurance.com/article/20160204/NEWS06/160209894/marsh-mclennan-reports-higher-quarterly-revenue|accessdate=30 June 2016|work=Business Insurance|date=4 Feb 2016|ref=BI 2015 FY results}}</ref>
|num_employees = ~30,200 (2015)<ref name="2015_10K_AnnualReport">{{cite web| url=https://www.sec.gov/Archives/edgar/data/62709/000006270916000040/mmc1231201510k.htm#sF24F1E9BDE1E5C13B79FC2365D1C04ED  |title=Marsh & McLennan Companies 2015 Annual Report|publisher=sec.gov |accessdate=2016-06-30}}</ref>
|industry        = [[Professional services]] 
|products       = [[Insurance Broker]]age and [[Risk Management]]
|parent         = [[Marsh & McLennan Companies]] 
|homepage       = [http://www.marsh.com/ www.marsh.com]
}}

'''Marsh''' is a global professional services firm, headquartered in [[New York City]] with operations in [[Insurance Broker|insurance broking]] and [[risk management]].  Marsh is a subsidiary of [[Marsh & McLennan Companies]] and a member of its Risk & Insurance Services business unit. Marsh's $5.7 billion in 2015 revenue accounted for 44% of the [[parent company]]'s total fiscal year revenue.<ref name="2015_10K_AnnualReport" />

==History==
Marsh’s insurance brokering and risk management business began in the first decade of the twentieth century, when it was formed by Henry Marsh and Donald McLennan with operations in [[New York City|New York]] and [[Chicago]], where McLennan had studied railroad risk and insurance needs by spending 30 days riding the rails.<ref name="1984_NYT">{{cite news|last1=Arenson|first1=Karen|title=Marsh's Enduring Strategy|url=https://www.nytimes.com/1984/05/05/business/marsh-s-enduring-strategy.html?pagewanted=1|accessdate=1 July 2016|work=New York Times|date=5 May 1984}}</ref> At the time, the company was known as Marsh & McLennan (M&M).<ref name="IRMI_journal_history">{{cite journal|last1=Bogardus|first1=John|title=Formulas for Success: History of Marsh & McLennan and Alexander & Alexander|journal=International Risk Management Institute|date=Sep 2003|url=https://www.irmi.com/articles/expert-commentary/formulas-for-success|accessdate=1 July 2016}}</ref>
Today, Marsh’s insurance brokerage unit operates as a separate business, known simply as Marsh, in a [[holding company]] structure, yet many media reports still erroneously refer to this brokerage unit as Marsh & McLennan.<ref name="WSJ_Feb2016_EarningsStory">{{cite news|last1=Dulaney|first1=Chelsey|title=Marsh & McLennan Revenue Growth Tops Expectations|url=https://www.wsj.com/articles/marsh-mclennan-revenue-growth-tops-expectations-1454590942|accessdate=1 July 2016|work=Wall Street Journal|issue=5 Feb 2016|date=4 Feb 2016}}</ref><ref name="Bloomberg_Doyle_Hiring">{{cite news|last1=Basak|first1=Sonali|last2=Katz|first2=Lily|title=Marsh & McLennan Hires AIG's Doyle for Brokerage Unit|url=https://www.bloomberg.com/news/articles/2016-03-07/marsh-mclennan-hires-ex-aig-executive-john-doyle-as-president|accessdate=1 July 2016|work=Bloomberg|publisher=Bloomberg|date=8 Mar 2016}}</ref>

The 1960s era was a particularly notable period for the company, including an [[initial public offering]] in 1962 and a 1969 reorganization that brought about the holding company configuration, whereby the company began to offer its services under the banners of separately managed companies.<ref>{{cite journal|last1=St. James Press|title=History of Marsh & McLennan Companies, Inc.|journal=International Directory of Company Histories|date=2002|volume= 45|url=http://www.fundinguniverse.com/company-histories/marsh-mclennan-companies-inc-history/|accessdate=1 July 2016}}</ref>

In 2001, as the company continued to expand globally, Marsh acquired South African company [[Alexander Forbes Group Holdings|Alexander Forbes]]’ African risk management and brokerage businesses in 11 countries for $115.5 million.<ref>{{cite news|title=Marsh Acquires Alexander Forbes' African Risk Business|url=http://www.reuters.com/article/marsh-acquisition-idUSL5E7K11S220110901|accessdate=1 July 2016|publisher=Reuters|date=1 Sep 2011}}</ref>

In the wake of the [[September 11, 2001 terrorist attacks]], which resulted in the deaths of many employees of both Marsh and Marsh & McLennan, Marsh’s risk management division formed a crisis consulting practice, led by [[Paul Bremer]], and formed an alliance with [[Control Risks Group]], a security and crisis-management specialist.<ref>{{cite news|last1=Oster|first1=Christopher|title=Marsh & McLennan Unit to Unveil Plan To Launch a Crisis-Consulting Practice|url=https://www.wsj.com/articles/SB1002749380783066520|accessdate=1 July 2016|work=Wall Street Journal|issue=11 Oct 2011}}</ref>

In 2004, Marsh was involved in a brokerage [[bid rigging]] scandal that plagued much of the insurance industry, including brokerage rivals [[Aon (company)|Aon]] and [[Willis Group]], and insurer [[AIG]].<ref>{{cite news|last1=Treaster|first1=Joseph|title=Insurance Investigations Under Way Over Fees|url=https://www.nytimes.com/2004/04/24/business/insurance-investigations-under-way-over-fees.html|accessdate=1 July 2016|work=New York Times|issue=24 April 2004|date=24 April 2004}}</ref> In a lawsuit, [[Eliot Spitzer]], then [[New York State]]’s attorney general, accused Marsh of not serving as an unbiased broker, leading to increased costs for clients and higher fees for Marsh. In early 2005, Marsh agreed to pay $850 million to settle the lawsuit and compensate clients whose commercial insurance it arranged from 2001 to 2004.<ref>{{cite news|last1=Treaster|first1=Joseph|title=Insurance Broker Settles Spitzer Suit for $850 Million|url=https://www.nytimes.com/2005/02/01/business/insurance-broker-settles-spitzer-suit-for-850-million.html|accessdate=1 July 2016|work=New York Times|issue=1 Feb 2005|date=1 Feb 2005}}</ref>

After the scandal, Marsh had nearly two years of financial struggles, with lagging profits and decreased brokerage revenue. But, by Q3 2006, brokerage revenue had stopped declining, and the company’s profits again began to grow, thanks in part to a [[cost-cutting]] regime.<ref>{{cite news|last1=Treaster|first1=Joseph|title=Profit Soars at Marsh, Aided by Cost-Cutting|url=https://www.nytimes.com/2006/11/02/business/02insure.html|accessdate=1 July 2016|work=New York Times|date=2 Nov 2006}}</ref>

By 2011, Marsh had largely recovered, and the share price of its parent Marsh & McLennan increased by 20% that year. Dan Glaser, who joined as Marsh CEO in 2007, said that by 2011, Marsh had essentially entered an optimization stage following the turmoil.<ref>{{cite news|last1=Elstein|first1=Aaron|title=Marsh regains its mojo after scandals|url=http://www.crainsnewyork.com/article/20120226/PROFESSIONAL_SERVICES/302269976/marsh-regains-its-mojo-after-scandals|accessdate=1 July 2016|work=Crain's New York Business|date=26 Feb 2012}}</ref>

In April 2011, Marsh named [[Peter Zaffino]] as CEO, succeeding Glaser, who then became chief operating officer of Marsh & McLennan, only to later be named group CEO.<ref>{{cite news|title=MMC announces new Marsh, Guy Carp CEOs|url=http://www.reactionsnet.com/Article/2811645/MMC-announces-new-Marsh-Guy-Carp-CEOs.html|work=Reactions|publisher=Euromoney Institutional Investor PLC|date=20 Apr 2011}}</ref><ref>{{cite news|last1=Jacobs|first1=Marci|title=Marsh & McLennan Rebounds to Level Before Spitzer's Suit|url=https://www.bloomberg.com/news/articles/2013-10-28/marsh-mclennan-rebounds-to-level-before-spitzer-s-2004-lawsuit|accessdate=1 July 2016|work=Bloomberg|publisher=Bloomberg|date=28 Oct 2013}}</ref> In 2016, John Doyle, previously of [[AIG]] joined Marsh as president.<ref name="Bloomberg_Doyle_Hiring" />

==Lines of Business==

===Insurance Brokerage===
In its brokerage business, Marsh represents commercial businesses searching for insurance and arranges for coverage with other companies that actually offer the policies themselves.<ref name="1984_NYT" />

===Risk Management===
Marsh’s risk management services include business risk analysis of the [[geopolitical]] environment, [[cybersecurity]] consulting, business interruption potential, and regulatory issues.<ref name="2015_10K_AnnualReport" /> Risk modeling, [[big data]], and advanced analytics have led to more effective Marsh risk management services.<ref>{{cite news|last1=Dipietro|first1=Ben|title=Why the Global Risks for Companies Are Growing|url=https://www.wsj.com/articles/why-the-global-risks-for-companies-are-growing-1464660005|accessdate=1 July 2016|work=Wall Street Journal|date=30 May 2016}}</ref>

==References==
{{reflist|30em}}

==External links==
* [https://www.marsh.com Official Corporate Website]
* [http://www.mmc.com Marsh & McLennan Companies Website (parent company)]

[[Category:Companies based in New York City]]
[[Category:Companies established in the 1900s]]