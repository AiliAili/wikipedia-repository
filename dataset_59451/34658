{{Infobox road
|state=MI
|type=US
|route=131
|maint=none
}}
There are six '''business routes of US Highway&nbsp;131''' in the state of [[Michigan]], and previously there was one [[bypass route]]. All of the [[business route]]s are former sections of [[U.S. Route 131|US Highway 131]] (US&nbsp;131). These former sections of the mainline highway, along with the necessary connecting roads, allow traffic to access the downtowns business districts of cities bypassed by sections of US&nbsp;131 built since the 1950s. The extant business loops connect to [[Constantine, Michigan|Constantine]], [[Three Rivers, Michigan|Three Rivers]], [[Kalamazoo, Michigan|Kalamazoo]], [[Grand Rapids, Michigan|Grand Rapids]], [[Big Rapids, Michigan|Big Rapids]], [[Cadillac, Michigan|Cadillac]], and [[Manton, Michigan|Manton]]. The former bypass route in Grand Rapids allowed traffic to bypass that city's downtown at a time when US&nbsp;131 still ran through the heart of the city.

==Constantine==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Constantine, Michigan|Constantine]]
|formed=October 30, 2013<ref name=fox2013/>
|length_mi=3.656
|length_ref=<ref name="PRFA">{{cite MDOT PRFA |access-date= October 31, 2014}}</ref>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is a [[business loop]] running through downtown [[Constantine, Michigan|Constantine]]. It starts south of the village in [[Constantine Township, Michigan|Constantine Township]] near some farm fields and runs northward. The highway passes a mobile home park and a couple of businesses before veering northwesterly on Washington Street through a residential neighborhood on the south side of Constantine. Bus. US&nbsp;131 crosses the [[St. Joseph River (Lake Michigan)|St. Joseph River]] and then turns northeasterly on Broad Street. The loop runs parallel to the banks of the river as the highway exits the northern side of the village. Bus. US&nbsp;131 ends in Constantine Township where it intersects the northern end of the Constantine Bypass.<ref name=MDOT15/><ref>{{google maps |url= https://www.google.com/maps/dir/41.8183782,-85.6638974/41.8710648,-85.6647557/@41.8448045,-85.707411,8119m/data=!3m2!1e3!4b1 |title= Overview Map of Bus. US&nbsp;131 in Constantine |access-date= September 7, 2015}}</ref>

The [[Michigan Department of Transportation]] (MDOT) has been studying a freeway upgrade for US&nbsp;131 through [[St. Joseph County, Michigan|St. Joseph County]] for several years. In late 2005, MDOT decided not to pursue an upgrade of the highway, but rescinded that decision in April 2006<ref name=jessup>{{cite news|last=Jessup |first=Kathy |title=Deal Will Open Door to US&nbsp;131 Expansion, Funding |work=[[Kalamazoo Gazette]] |url=http://www.mlive.com/news/kzgazette/index.ssf?/kzgazette/stories/20060420_deal.html |date=April 20, 2006 |accessdate=February 5, 2011 |archiveurl=https://web.archive.org/web/20121012060744/http://www.mlive.com/news/kzgazette/index.ssf?/kzgazette/stories/20060420_deal.html |archivedate=October 12, 2012 |oclc=9940379 |deadurl=yes |df= }}</ref> under political pressure in the state.<ref name=heinlein-2005-11-18>{{cite news |last=Heinlein |first=Gary |title= State Road Repairs Put on Fast Track |work= The Detroit News |date= November 18, 2005 |page= A1 |issn= 1055-2715}}</ref> The department began building a two-lane bypass west of Constantine.<ref name=rietsma-2008-05-21>{{cite news|last=Riestma |first=Jeff |url=http://www.mlive.com/news/index.ssf/2008/05/michigan_department_of_transpo.html |title=Michigan Department of Transportation Updates US&nbsp;131 Plans |work=Kalamazoo Gazette |date=May 21, 2008 |accessdate=February 5, 2011 |archiveurl=http://www.webcitation.org/5wGYvWT2d?url=http://www.mlive.com/news/index.ssf/2008/05/michigan_department_of_transpo.html |archivedate=February 5, 2011 |oclc=9940379 |deadurl=yes |df= }}</ref> The [[American Association of State Highway and Transportation Officials]], which is in charge of US Highway numbering assignments and routings, approved the relocation of the US 131 designation out of Constantine on November 16, 2012. The group's Special Committee on U.S. Route Numbering also approved the creation of a business route for the old highway through town.<ref name=AASHTO2012-11-16>{{AASHTO minutes |year= 2012A |accessdate= November 29, 2012 |page= 3 }}</ref> The bypass opened on October 30, 2013,<ref name=fox2013>{{cite news |first= Robb |last= Westaby |date= October 30, 2013 |title= New US&nbsp;131 Bypass Opens |url= http://fox17online.com/2013/10/18/new-us-131-bypass-opens/#axzz2jFPLGDMw |location= Grand Rapids, MI |publisher= [[WXMI-TV]] |accessdate= October 30, 2013}}</ref> and the business route designation was put into service that day.<ref name=MDOT14C>{{cite MDOT map |year= 2014 |section= N9}}</ref>

'''Major intersections'''<br/>
{{MIinttop|county=St. Joseph|length_ref=<ref name=PRFA/>}}
{{MIint
|location=Constantine Township
|mile=0.000
|road={{jct|state=MI|US|131|city1=Three Rivers|location2=[[Middlebury, Indiana|Middlebury]]}}
|notes=}}
{{jctbridge
|state=MI
|location=Constantine
|mile=1.770
|mile2=1.974
|bridge=Bridge over [[St. Joseph River (Lake Michigan)|St. Joseph River]] }}
{{MIint
|location=Constantine Township
|mile=3.656
|road={{jct|state=MI|US|131|city1=Three Rivers|location2=[[Middlebury, Indiana|Middlebury]]}}
|notes=}}
{{jctbtm}}

==Three Rivers==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Three Rivers, Michigan|Three Rivers]]
|length_mi=2.982
|length_ref=<ref name=PRFA/>
|established=1953<ref name=MSHD53-04TR>{{cite MDOT map |date= 1953-04-15 |section= N9}}</ref><ref name=MSHD53-10TR>{{cite MDOT map |date= 1953-10-01 |section= N9}}</ref>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is a {{convert|3|mi|km|adj=mid|-long|spell=in}} [[business loop]] running through downtown [[Three Rivers, Michigan|Three Rivers]]. It follows [[M-60 (Michigan highway)|M-60]] (Michigan Avenue) eastward from a commercial district on the main highway into downtown. Bus. US&nbsp;131/M-60 passes through a residential neighborhood and crosses the Rocky River near its confluence with the [[St. Joseph River (Lake Michigan)|St. Joseph River]]. On the east side of the river, the business loop turns north-northwesterly onto Main Street, separating from M-60 at an intersection that also features [[M-86 (Michigan highway)|M-86]]. Bus. US&nbsp;131 (Main Street) passes several more businesses and turns due north through a residential area. Near the northern terminus, the business loop passes a handful of businesses before ending at an intersection with US&nbsp;131 on the north side of town.<ref>{{cite MDOT map|year= 2015 |section= N9}}</ref><ref>{{google maps |url= https://www.google.com/maps/dir/41.9415309,-85.650962/41.9716578,-85.6349975/@41.9565614,-85.6603239,4251m/data=!3m2!1e3!4b1!4m9!4m8!1m5!3m4!1m2!1d-85.6328952!2d41.9439783!3s0x8817071078518531:0x2cb7357c6893075c!1m0!3e0 |title= Overview Map of Bus. US&nbsp;131 in Three Rivers |link= no |access-date= September 7, 2015}}</ref>

The Three Rivers Bypass opened in the middle of 1953, and the former route of the highway through downtown was redesignated as the business loop at that time.<ref name=MSHD53-04TR/><ref name=MSHD53-10TR/>

'''Major intersections'''<br/>
{{MIinttop|county=St. Joseph|location=Three Rivers|length_ref=<ref name=PRFA/>}}
{{MIint
|mile=0.000
|type=concur
|road={{jct|state=MI|US|131|M|60|dir2=west|city2=Constantine|city1=Kalamazoo|city3=Cassopolis}}
|notes=Western end of M-60 concurrency}}
{{MIint
|mile=1.003
|type=concur
|road={{jct|state=MI|M|60|dir1=east|city1=Jackson}}<br/>{{jct|state=MI|M|86|dir1=east|city1=Coldwater}}
|notes=Eastern end of M-60; western terminus of M-86}}
{{MIint
|mile=2.982
|road={{jct|state=MI|US|131|city1=Constantine|city2=Kalamazoo}}
|notes=}}
{{jctbtm|keys=concur}}

==Kalamazoo==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Kalamazoo, Michigan|Kalamazoo]]
|length_mi=9.999
|length_ref=<ref name=PRFA/>
|established=1964<ref name=MSHD64K/><ref name=MSHD65K/>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is an almost {{convert|10|mi|km|adj=mid|-long}} [[business loop]] running through downtown [[Kalamazoo, Michigan|Kalamazoo]]. The southern end of the highway is at an interchange for exit&nbsp;36 along [[U.S. Route 131|US&nbsp;131]] west of downtown Kalamazoo in [[Oshtemo Township, Michigan|Oshtemo Township]]. Bus. US&nbsp;131 [[concurrency (road)|runs concurrently]] with [[Interstate 94 Business (Kalamazoo, Michigan)|Business Loop Interstate 94]] (BL I-94) eastward along the five-lane Stadium Drive through a commercial area north of Asylum Lake. Near the campus of [[Western Michigan University]], BL I-94/Bus. US&nbsp;131 turns northeasterly along the edge of the campus. Northeast of [[Waldo Stadium]] and the campus, the street name changes to Michigan Avenue. BL I-94/Bus. US&nbsp;131 merges with [[M-43 (Michigan highway)|M-43]] at the intersection with Main Street, and Michigan Avenue turns eastward as a one-way, four-lane street into downtown Kalamazoo. This intersection also marks the western end of the [[one-way pair]]ing of Michigan Avenue (eastbound) and Kalamazoo Avenue (westbound), using Michikal Street to connect the two southbound. The two directions of traffic are separated by three blocks as they travel through downtown. At the one-way pairing of Park Street (northbound) and Westnedge Avenue (southbound), Bus. US&nbsp;131 turns north–south; south of this complex of intersecting one-way streets, Westnedge and Park carries [[M-331 (Michigan highway)|M-331]], an [[unsigned highway]]. Westnedge and Park run two blocks apart on four-lane streets and pass through residential areas north of downtown. North of town, Westnedge curves northeasterly to run next to Park, and the two streets form the opposing two-lane [[carriageway]]s of a [[freeway]]. This freeway runs north and then turns westward. There is a half [[folded diamond interchange]] for Douglas Avenue. West of that interchange, the freeway turns northwesterly, passing between suburban residential subdivisions before terminating at a partial interchange with US&nbsp;131. At this endpoint, northbound Bus. US&nbsp;131 traffic must enter northbound US&nbsp;131, and only southbound US&nbsp;131 traffic has access to the southbound direction of the business loop.<ref name=MDOT15K>{{cite MDOT map|year= 2015 |inset= Kalamazoo}}</ref><ref name=googleK>{{google maps |url= https://www.google.com/maps/dir/42.2686428,-85.6530688/42.3310978,-85.6634968/@42.3002266,-85.6607294,15374m/am=t/data=!3m1!1e3 |title= Overview Map of Bus. US&nbsp;131 in Kalamazoo |access-date= November 4, 2015 |link= no}}</ref>

On December 7, 1959, the [[Interstate 94 in Michigan|I-94]]/[[U.S. Route 12 in Michigan|US&nbsp;12]] freeway opened on the south side of Kalamazoo,<ref name=MC1959-12-31>{{cite news |title=Marshall Area Chronology for 1959 |url= http://newspaperarchive.com/marshall-evening-chronicle/1959-12-31/pageno-209234899?tag=interstate+94+michigan&rtserp=tags/interstate-94-michigan?psb=date&pci=7&psi=52&ndt=by&py=1950&pey=1959 |work= Marshall Evening Chronicle |date= December 31, 1959 |pages= 4–5 |access-date= August 21, 2013 |via= [[NewspaperArchive.com]] }}</ref> and US&nbsp;12 was shifted to follow the new freeway. The former route through downtown Kalamazoo and along Stadium Drive was redesignated Bus. US&nbsp;12.<ref name=MSDH60K>{{cite MDOT map |year= 1960 |inset= Kalamazoo}}</ref> The next year, it was renumbered BL I-94.<ref name=MSDH61K>{{cite MDOT map |year= 1961 |inset= Kalamazoo}}</ref> In 1963, the US&nbsp;131 freeway opened on the northwest side of Kalamazoo. US&nbsp;131 followed M-43 eastward from the end of the freeway into the downtown area to reconnect to its original routing along Westnedge and Park.<ref name=MSHD62K>{{cite MDOT map |year=1962 |inset= Kalamazoo}}</ref><ref name=MSHD63K>{{cite MDOT map |year=1963 |inset=Kalamazoo}}</ref> The next year, another section of US&nbsp;131 opened southwest of Kalamazoo, and US&nbsp;131 was rerouted out of downtown completely.<ref name=MSHD64K>{{cite MDOT map |year=1964 |inset= Kalamazoo}}</ref><ref name=MSHD63K/> The freeway section north of downtown opened in 1964, and Bus. US&nbsp;131 was designated along it into downtown and along BL I-94 back out to the US&nbsp;131 freeway west of downtown.<ref name=MSHD64K/><ref name=MSHD65K>{{cite MDOT map |year=1965 |inset=Kalamazoo}}</ref> Later in 1965, Kalamazoo and Michikal avenues were transferred to state jurisdiction to set up the one-way pairing east–west through downtown. Kalamazoo and Michikal are then signed as southbound Bus. US&nbsp;131 from Westnedge Avenue to Stadium Drive while Main Street and Michigan Avenue continued to serve northbound traffic only between Stadium Drive and Park Street.<ref name=MSHD65K/><ref name=MSHD66K>{{cite MDOT map |year=1966 |inset=Kalamazoo}}</ref>{{Clear}}

'''Major intersections'''<br/>
{{MIinttop|county=Kalamazoo|length_ref=<ref name=PRFA/>|unnum=yes}}
{{MIint
|location=Oshtemo Township
|mile=0.000
|type=concur
|road={{jct|state=MI|US|131|BL|94|dab2=Kalamazoo|dir2=west|city2=Three Rivers|city1=Grand Rapids}}
|notes=Western end of BL I-94 concurrency; exit&nbsp;36 on US&nbsp;131}}
{{MIint
|location=Kalamazoo
|lspan=2
|type=concur
|mile=3.825
|road={{jct|state=MI|M|43|dir1=west|city1=South Haven}}
|notes=Western end of M-43 concurrency}}
{{MIint
|mile=3.987
|mile2=4.112
|type=concur
|road={{jct|state=MI|BL|94|M|43|dab1=Kalamazoo|dir1=east|dir2=east|name2=Michigan Avenue/Kalamazoo Avenue}}<br/>{{jct|state=MI|M|331|dir1=south|noshield1=yes|name1=Westnedge Avenue/Park Street}}
|notes=Eastern end of BL I-94/M-43 concurrency; northern terminus of unsigned M-331}}
{{jctplace
|state=MI
|location=Kalamazoo Township
|lspan=3
|mile=5.801
|place=Eastern end of freeway}}
{{MIint
|mile=6.472
|road=Douglas Avenue
|notes=Half-folded diamond interchange}}
{{MIint
|mile=9.999
|type=incomplete
|road={{jct|state=MI|US|131|dir1=north|city1=Grand Rapids}}
|notes= Northbound exit and southbound entrance only}}
{{jctbtm|keys=concur,incomplete}}

==Grand Rapids bypass==
{{infobox road small
|state=MI
|type=US 1948
|subtype=Byp
|route=131
|location=[[Grand Rapids, Michigan|Grand Rapids]]
|length_mi=14.253
|length_ref=<ref name=PRFA/>
|established=1945<ref name=MSHD45GR>{{cite MDOT map |date= 1945-10-01 |inset= Grand Rapids}}</ref>
|decommissioned=1953<ref name=MSHD53-04GR>{{cite MDOT map |date=1953-04-15 |inset=Grand Rapids}}</ref><ref name=MSHD53-10GR>{{cite MDOT map |date=1953-10-01|inset=Grand Rapids}}</ref>
}}
'''Bypass US Highway&nbsp;131''' ('''Byp. US&nbsp;131''') was a [[bypass route]] around downtown [[Grand Rapids, Michigan|Grand Rapids]]. The highway followed 28th Street eastward from the intersection with [[U.S. Route 131|US&nbsp;131]] (Division Street) south of downtown. From this starting point eastward, it [[concurrency (road)|ran concurrently]] with [[U.S. Route 16 Bypass (Grand Rapids, Michigan)|Byp. US&nbsp;16]] and [[M-21 Bypass (Grand Rapids, Michigan)|Byp M-21]]. [[M-37 (Michigan highway)|M-37]] also followed 28th Street at the time eastward from the intersection at Kalamazoo Avenue. At the intersection with East Beltline and Broadmoor avenues in [[Paris Township, Kent County, Michigan|Paris Township]] (now Kentwood), Byp.US&nbsp;16/Byp. US&nbsp;131/Byp.M-21 turned northward to follow East Beltline, and M-37 turned southward on Broadmoor. The bypass route continued northward through [[Grand Rapids Township, Michigan|Grand Rapids Township]]. At the intersection with [[U.S. Route 16 in Michigan|US&nbsp;16]]/[[M-50 (Michigan highway)|M-50]] (Cascade Road), Byp. US&nbsp;16 ended, and Byp. M-21 ended less than half a mile (0.4&nbsp;km) north of that at the intersection with [[M-21 (Michigan highway)|M-21]] (Fulton Street). North of the intersection with Fulton Street, Byp. US&nbsp;131 was the only designation along East Beltline Avenue at the time. The highway ran northward into [[Plainfield Township, Kent County, Michigan|Plainfield Township]], and it terminated at an intersection with US&nbsp;131 at Plainfield Avenue and Northland Drive just south of the [[Grand River (Michigan)|Grand River]].<ref name=MSHD53-04GR/>

By 1945, Byp. US&nbsp;131, along with the other various bypass routes, was designated along the Grand Rapids beltline system, replacing [[M-114 (Michigan highway)|M-114]]. Byp. US&nbsp;131 followed the south beltline (28th Street) and East Beltline Avenue between Division Avenue and Plainfield Avenue, allowing north–south traffic to bypass downtown Grand Rapids.<ref name=MSHD45GR/> In late 1949, M-37 was rerouted onto 28th Street between Kalamazoo and East Beltline/Broadmoor avenues.<ref name=MSHD49-07GR>{{cite MDOT map |date= 1949-07-01 |inset= Grand Rapids}}</ref><ref name=MDSHD50-04GR>{{cite MDOT map |date= 1950-04-15 |inset=Grand Rapids}}</ref> Then in 1953, US&nbsp;131 was rerouted onto the beltlines to replace its bypass route; the former mainline through downtown was redesignated [[#Grand Rapids business loop|Bus. US&nbsp;131]].<ref name=MSHD53-04GR/><ref name=MSHD53-10GR/>

'''Major intersections'''<br/>
{{MIinttop|former=yes|county=Kent|length_ref=<ref name=PRFA/>}}
{{MIint
|location=Grand Rapids
|type=concur
|mile=0.000
|road={{jct|state=MI|US 1948|131|name1=Division Street|city1=Cadillac|city2=Kalamazoo}}<br/>{{jct|state=MI|US 1948-Byp|16|M 1948-Byp|21|dir1=west|dir2=west|name2=28th Street|dab1=Grand Rapids|dab2=Grand Rapids|city1=Muskegon|city2=Holland}}
|notes=Western end of Byp. US&nbsp;16/Byp. M-21 concurrency}}
{{MIint
|location= Paris Township
|ctdab=Kent
|lspan=2
|type=concur
|mile=1.924
|road={{jct|state=MI|M 1948|37|dir1=north|city1=Grand Rapids|name1=Kalamazoo Avenue}}
|notes=Western end of M-37 concurrency}}
{{MIint
|type=concur
|mile=4.188
|road={{jct|state=MI|M 1948|37|dir1=south|city1=Hastings|name1=Broadmoor Avenue}}
|notes=Eastern end of M-37 concurrency}}
{{MIint
|location=Grand Rapids Township
|lspan=2
|type=concur
|mile=7.277
|road={{jct|state=MI|US 1948|16|M 1948|50|name2=Cascade Road|city1=Grand Rapids|city2=Lansing}}<br/>{{jct|state=MI|US 1948-Byp|16|dir1=west|dab1=Grand Rapids}}
|notes=Northern end of Byp. US&nbsp;16 concurrency}}
{{MIint
|mile=7.710
|type=concur
|road={{jct|state=MI|M 1948|21|name1=Fulton Street|city1=Grand Rapids|city2=Flint}}<br/>{{jct|state=MI|M 1948-Byp|21|dir1=west|dab1=Grand Rapids}}
|notes=Northern end of Byp. M-21 concurrency}}
{{MIint
|location=Plainfield Township
|ctdab=Kent
|mile=14.253
|road={{jct|state=MI|US 1948|131|city1=Cadillac|city2=Grand Rapids|name1=Plainfield Avenue/Northland Drive}}
|notes=}}
{{jctbtm||keys=concur}}

==Grand Rapids business loop==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Grand Rapids, Michigan|Grand Rapids]]
|length_mi=2.419
|length_ref=<ref name=PRFA/>
|established=1953<ref name=MSHD53-04GR/><ref name=MSHD53-10GR/>
}}
'''Business US Highway&nbsp;131''' ('''BUS US&nbsp;131''') is a {{convert|2.4|mi|km|adj=mid|-long}} [[business loop]] running through downtown [[Grand Rapids, Michigan|Grand Rapids]]. The southern end is at a partial interchange with [[U.S. Route 131|US&nbsp;131]] south of the S-Curve near the [[Grand River (Michigan)|Grand River]]. Only northbound traffic on US&nbsp;131 can directly access the business loop at this interchange, running northward on a long ramp that connects to Ottawa Avenue at an intersection with Cherry Street. Bus. US&nbsp;131 follows Ottawa Avenue northward between parking lots, and after one block, it turns eastward onto Oakes Street immediately south of the [[Van Andel Arena]]. The business loop travels three blocks along Oakes Street past commercial properties before turning northward onto Division Street. As the highway runs along Division Street, it passes the various businesses, the campuses of [[Kendall College of Art and Design]] and [[Grand Rapids Community College]]. It also passes under Michigan Avenue at the base of the hill that makes up the [[Grand Rapids Medical Mile|Medical Mile]]. Immediately north of the Michigan Avenue overpass, Bus. US&nbsp;131 passes under [[Interstate 196]] (I-196) without an interchange. Division Street here runs at the base of Belknap Hill through an area of the city dominated by industrial-type properties. North of an intersection with Coldbrook Street, the business loop switches to Plainfield Avenue and turns northeasterly. One block later, Bus. US&nbsp;131 turns westward onto Leonard Street. The business loop follows Leonard for a few blocks and then crosses the Grand River. On the western bank of the river, Bus. US&nbsp;131 meets its parent highway at exit&nbsp;87 and terminates. The streets that carry the business loop have either two or four lanes with an additional center turn lane.<ref name=MDOT15GR>{{cite MDOT map|year= 2015 |inset= Grand Rapids}}</ref><ref name=googleGR>{{google maps |url= https://www.google.com/maps/dir/42.9553633,-85.6712271/42.9848291,-85.676733/@42.9701361,-85.6905645,7605m/data=!3m1!1e3!4m9!4m8!1m5!3m4!1m2!1d-85.6689842!2d42.9784732!3s0x8819ac340d2d9e8d:0x6ed76ae5c61c956a!1m0!3e0 |title= Overview Map of Bus. US&nbsp;131 in Grand Rapids |access-date= November 4, 2015 |link= no}}</ref>

The designation was created when US&nbsp;131 was rerouted on the Grand Rapids beltline system in 1953. Previously, a [[U.S. Route 131 Bypass (Grand Rapids, Michigan)|Bypass US&nbsp;131]] (BYP US&nbsp;131) designation was used on the beltline system following 28th Street and East Beltline Avenue. After the change, the former mainline through downtown on Division Street and Plainfield Avenue was redesignated Bus. US&nbsp;131, and the mainline US&nbsp;131 replaced Byp. US&nbsp;131.<ref name=MSHD53-04GR/><ref name=MSHD53-10GR/> In 1961, the US&nbsp;131 freeway was extended northward to the west of downtown, and between 28th Street and the downtown exit, it was initially designated as part of Bus. US&nbsp;131.<ref name=MSHD61GR>{{cite MDOT map |year= 1961 |inset= Grand Rapids}}</ref><ref name=MSHD62GR>{{cite MDOT map |year= 1962 |inset= Grand Rapids}}</ref> In December 1962, the [[Interstate 296|I-296]]/US&nbsp;131 freeway was completed west of downtown.<ref name=296opening>{{cite news |title= Driver's Boon: Int.&nbsp;296 Opening Finishes City Freeway, Links Kalamazoo, Muskegon |work=[[The Grand Rapids Press]] |date= December 17, 1962 |page= A1 |oclc= 9975013}}</ref> Afterwards, Bus. US&nbsp;131 was truncated on its southern end to the modern exit&nbsp;84B interchange on the south side of downtown. At the same time, the northern end was also truncated, using Coldbrook Street and Monroe Avenue to Leonard Street to connect back to the freeway.<ref name=MSHD62GR/><ref name=MSHD63GR>{{cite MDOT map |year= 1963 |inset= Grand Rapids}}</ref>

In 1986, the city and state transferred jurisdiction over some streets at the northern end of the business loop, smoothing out the routing of the northern end of Bus. US&nbsp;131 and eliminating some extra turns between Plainfield Avenue and Leonard Street.<ref name=MDOT86GR>{{cite MDOT map |year= 1986 |inset= Grand Rapids}}</ref><ref name=MDOT87GR>{{cite MDOT map |year= 1987 |inset= Grand Rapids}}</ref> The construction of the [[Van Andel Arena]] in 1995 and 1996 severed the connection from southbound Bus. US&nbsp;131 to US&nbsp;131.<ref name=MDOT95GR>{{cite MDOT map |year= 1995 |inset= Grand Rapids}}</ref><ref name=MDOT96GR>{{cite MDOT map |year= 1996 |inset= Grand Rapids}}</ref> This gap was rejoined with the reconstruction of the "S-Curve" in 1999.<ref name=MDOT99GR>{{cite MDOT map |year= 1999 |inset= Grand Rapids}}</ref><ref name=MDOT00GR>{{cite MDOT map |year= 2000 |inset= Grand Rapids}}</ref>

'''Major intersections'''<br/>
{{MIinttop|county=Kent|location=Grand Rapids|length_ref=<ref name=PRFA/>}}
{{MIint
|mile=0.000
|road={{jct|state=MI|US|131|city1=Cadillac|city2=Kalamazoo}}
|notes=Exit&nbsp;84B on US&nbsp;131}}
{{MIint
|mile=2.419
|road={{jct|state=MI|US|131|city1=Cadillac|city2=Kalamazoo|name1=[[Interstate 296|I-296]]}}
|notes=Exit&nbsp;87 on US&nbsp;131}}
{{jctbtm}}

==Big Rapids==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Big Rapids, Michigan|Big Rapids]]
|length_mi=6.860
|length_ref=<ref name=PRFA/>
|established=1983<ref name=MDOT83BR/><ref name=MDOT84BR/>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is a {{convert|6.9|mi|km|adj=mid|-long}} [[business loop]] running through downtown [[Big Rapids, Michigan|Big Rapids]]. The southern end of the highway is at an interchange with [[U.S. Route 131|US&nbsp;131]] and [[M-20 (Michigan highway)|M-20]] west of downtown in [[Big Rapids Township, Michigan|Big Rapids Township]]. At this interchange, M-20 leaves the freeway to [[concurrency (road)|run concurrently]] eastward with the business loop along the four-lane Perry Street into town. Bus. US&nbsp;131/M-20 passes several commercial properties on the western edge of Big Rapids before crossing the city limits. South of Perry Street in the city is part of the campus of [[Ferris State University]], including [[Katke Golf Course]]; the north side of the street contains various businesses. At the intersection with State Street, Bus. US&nbsp;131/M-20 turns northward toward downtown Big Rapids, passing the northern end of the FSU campus. North of campus, the business loop passes through residential neighborhoods. At the intersection with Maple Street immediately west of downtown, M-20 turns easterly and separates from Bus. US&nbsp;131. North of the downtown area, State Street ends, and the business loop follows Northland Drive parallel to the [[Muskegon River]]. On the northern edge of the city, the highway passes [[Roben–Hood Airport]] before exiting town as a two-lane road. Running in a rural area in Big Rapids Township, the business loop turns westward onto 19&nbsp;Mile Road. Bus. US&nbsp;131 ends at an interchange with US&nbsp;131 and County Road [[B-96 (Michigan county highway)|B-96]].<ref name=MDOT15>{{cite MDOT map|year= 2015 |sections= H9, I9, J9, N9}}</ref><ref name=googleBR>{{google maps |url= https://www.google.com/maps/dir/43.6868014,-85.5197844/43.7427592,-85.5296121/@43.7144653,-85.5416774,15024m/data=!3m1!1e3!4m9!4m8!1m5!3m4!1m2!1d-85.4889252!2d43.7134519!3s0x881f2cc52babe989:0x1c5253b7aabfa900!1m0!3e0 |title= Overview Map of Bus. US&nbsp;131 in Big Rapids |access-date= November 4, 2015 |link= no}}</ref>

In 1983 with the extension of the US&nbsp;131 freeway west of Big Rapids to the interchange with 19&nbsp;Mile Road, the former route of US&nbsp;131 through Big Rapids on State Street and Northland Drive north of Perry Street was designated as a business loop, using M-20 (Perry Street) to connect back to the freeway on the southern end. At the time, 19&nbsp;Mile Road between the freeway Northland Drive was used by the mainline to connect to its pre-existing routing north of Big Rapids to [[Reed City, Michigan|Reed City]].<ref name=MDOT83BR>{{cite MDOT map |year= 1983 |section= J9}}</ref><ref name=MDOT84BR>{{cite MDOT map |year= 1984 |section= J9}}</ref> The US&nbsp;131 freeway was completed north of 19 Mile Road in 1986, and Bus. US&nbsp;131 was extended over the 19 Mile Road portion of US&nbsp;131 to connect with its parent route at exit 142 northwest of Big Rapids.<ref name=MDOT86BR>{{cite MDOT map |year= 1986 |section= J9}}</ref><ref name=MDOT87BR>{{cite MDOT map |year= 1987 |section= J9}}</ref>

'''Major intersections'''<br/>
{{MIinttop|county=Mecosta|length_ref=<ref name=PRFA/>}}
{{MIint
|location= Big Rapids Township
|mile=0.000
|type=concur
|road={{jct|state=MI|US|131|M|20|dir2=west|city1=Cadillac|city2=Grand Rapids|city3=White Cloud}}
|notes=Western end of M-20 concurrency; exit&nbsp;139 on US&nbsp;131}}
{{MIint
|location=Big Rapids
|mile=2.814
|type=concur
|road={{jct|state=MI|M|20|dir1=east|city1=Mount Pleasant|name1=Maple Street}}
|notes=Northern end of M-20 concurrency}}
{{MIint
|location=Big Rapids Township
|mile=6.860
|road={{jct|state=MI|US|131|city1=Cadillac|city2=Grand Rapids}}<br/>{{jct|state=MI|CDH|B-96|dir1=west|name1=19&nbsp;Mile Road}}
|notes=Exit&nbsp;142 on US&nbsp;141; eastern terminus of B-96; roadway continues as B-96}}
{{jctbtm|keys=concur}}

==Cadillac==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Cadillac, Michigan|Cadillac]]
|length_mi=5.597
|length_ref=<ref name=PRFA/>
|established=2001<ref name=bornheimer/>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is a {{convert|5.60|mi|km|adj=mid|-long}} [[business loop]] running through downtown [[Cadillac, Michigan|Cadillac]]. The southern end is at an interchange with [[U.S. Route 131|US&nbsp;131]]/[[M-55 (Michigan highway)|M-55]] in [[Clam Lake Township, Michigan|Clam Lake Township]] south of Cadillac, and the business loop runs northward along Mitchell Street away from the freeway, passing several businesses and the Maple Hill Cemetery. North of the cemetery, Mitchell Street descends a hill through a residential neighborhood south of downtown. At Howard Street, the business loop turns northwesterly into downtown near [[Lake Cadillac]]. North of downtown, Mitchell Street turns more northerly and passes the [[Wexford County Civic Center]]. At the intersection with Boon Road next to the [[Wexford County Airport]], Bus. US&nbsp;131 turns eastward onto Boon Road to connect back to the freeway in [[Haring Township, Michigan|Haring Township]]. The business loop has at least four lanes, two in each direction, and some segments also feature a center turn lane.<ref name=MDOT15/><ref name=googleC>{{google maps|url=  https://www.google.com/maps/dir/44.2204428,-85.3959749/44.2813716,-85.3893656/@44.2509949,-85.4331096,14889m/data=!3m2!1e3!4b1!4m9!4m8!1m5!3m4!1m2!1d-85.4015451!2d44.2528707!3s0x881f08e12a9c33c9:0xb339ce73de5d2d36!1m0!3e0 |title= Overview Map of Bus. US&nbsp;131 in Cadillac |access-date= November 4, 2015 |link= no}}</ref>

The [[Michigan Department of Transportation]] (MDOT) started construction of a freeway bypass of Cadillac in 1999.<ref name=heinlein>{{cite news |first=Gary |last= Heinlein |title= State to Spend $6.5B on Roads: Metro Detroit to Get Lion's Share Under Engler's 5-Year Plan |work= The Detroit News |date= February 4, 1999 |page=1A |issn= 1055-2715}}</ref> With the completion of the southern half of the bypass in 2000, M-55 was rerouted out of town along the new freeway.<ref name=bauza-2000>{{cite news |last=Bauza |first=Margarita |title= Cadillac Hears Sounds of Silence Thanks to Bypass |work=The Grand Rapids Press |date=November 15, 2000 |page=D1 |oclc= 9975013}}</ref> Once the northern half of the bypass was completed on October 30, 2001, US&nbsp;131 was rerouted out of town along the new highway to the end of the freeway where it joined the old alignment north of town. Mitchell Street through downtown Cadillac was redesignated as Bus. US&nbsp;131.<ref name=bornheimer>{{cite news |last=Bornheimer |first=Hank |title= Temporary Recreation Trail Ready for Traffic—You Can Walk, Run, Skate or Bike the Cadillac Bypass—until Tuesday |work= The Grand Rapids Press |date= October 27, 2001 |page=A1 |oclc= 9975013}}</ref>

'''Major intersections'''<br/>
{{MIinttop|county=Wexford|length_ref=<ref name=PRFA/>}}
{{MIint
|location=Clam Lake Township
|mile=0.000
|road={{jct|state=MI|US|131|M|55|city1=Petoskey|city2=Grand Rapids|city3= Manistee}}
|notes=Exit&nbsp;177 on US&nbsp;131}}
{{MIint
|location=Haring Township
|mile=5.597
|road={{jct|state=MI|US|131|city1=Petoskey|city2=Grand Rapids}}
|notes=Exit&nbsp;183 on US&nbsp;131}}
{{jctbtm}}

==Manton==
{{infobox road small
|state=MI
|type=US
|route=131
|subtype=BUS
|location=[[Manton, Michigan]]
|length_mi=5.667
|length_ref=<ref name=PRFA/>
|established=2003<ref name=MDOT03M/><ref name=MDOT04M/>
}}
'''Business US Highway&nbsp;131''' ('''Bus. US&nbsp;131''') is a {{convert|5+2/3|mi|km|adj=mid|-long}} [[business loop]] running through downtown [[Manton, Michigan|Manton]]. The business loop starts at an interchange with [[U.S. Route 131|US&nbsp;131]] and [[M-42 (Michigan highway)|M-42]] east of downtown Manton in [[Cedar Creek Township, Wexford County, Michigan|Cedar Creek Township]]. Bus. US&nbsp;131 [[concurrency (road)|runs concurrently]] with M-42 for about a mile (1.6&nbsp;km) westerly into town along a roadway with a travel lane in each direction separated by a center turn lane. At the intersection with Michigan Avenue (Old US&nbsp;131), M-42 terminates and Bus. US&nbsp;131 turns northward onto Michigan Avenue. The business loop runs through downtown Manton before exiting the northern end of town. North of Manton, the turn lane is eliminated, and the surrounding landscapes are dominated by farm fields broken by intermittent patches of forest. Bus. US&nbsp;131 curves northwesterly before returning to a due northward course. As the business loop approaches northern end of the freeway mainline in [[Liberty Township, Wexford County, Michigan|Liberty Township]], it curves northeasterly to meet it at an [[at-grade intersection]].<ref name=MDOT15/><ref name=googleM>{{google maps|url= https://www.google.com/maps/dir/44.403788,-85.3820726/44.4679704,-85.4013659/@44.4358713,-85.4300653,14842m/data=!3m1!1e3 |title= Overview Map of Bus. US&nbsp;131 in Manton |access-date= November 4, 2015 |link= no}}</ref>

With the completion of the Manton Bypass in 2003, US&nbsp;131 was rerouted out of downtown Manton. The former route through town was designated Bus. US&nbsp;131, using M-42 to connect back to the freeway east of town.<ref name=MDOT03M>{{cite MDOT map |year=2003 |section= H9}}</ref><ref name=MDOT04M>{{cite MDOT map |year= 2004 |section= H9}}</ref>

'''Major intersections'''<br/>
{{MIinttop|county=Wexford|length_ref=<ref name=PRFA/>}}
{{MIint
|location=Cedar Creek Township
|ctdab=Wexford
|mile=0.000
|type=concur
|road={{jct|state=MI|US|131|city1=Cadillac|city2=Petoskey}}<br/>{{jct|state=MI|M|42|dir1=east|city1=Lake City}}
|notes=Eastern end of M-42 concurrency; exit&nbsp;191 on US&nbsp;131}}
{{MIint
|location=Manton
|mile=0.976
|type=concur
|road={{jct|state=MI|M|42|dir1=east|city1=Lake City}}
|notes=Western end of M-42 concurrency at western terminus of M-42}}
{{MIint
|location=Liberty Township
|ctdab=Wexford
|mile=5.667
|road={{jct|state=MI|US|131|city1=Cadillac|city2=Petoskey}}
|notes=}}
{{jctbtm|keys=concur}}

==See also==
*{{portal-inline|Michigan Highways}}

==References==
{{reflist|30em}}

==External links==
{{columns-list|2|
<!--*{{osmrelation-inline|443212|Constantine Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/US-131BR_Constantine.html Constantine Bus. US&nbsp;131] at Michigan Highways
<!--*{{osmrelation-inline|443212|Three Rivers Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-TR Three Rivers Bus. US&nbsp;131] at Michigan Highways
<!--*{{osmrelation-inline|443212|Kalamazoo Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-K Kalamazoo Bus. US&nbsp;131] at Michigan Highways
<!--*{{osmrelation-inline|443212|Grand Rapids Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-GR Grand Rapids Bus. US&nbsp;131] at Michigan Highways
<!--*{{osmrelation-inline|443212|Big Rapids Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-BR Big Rapids Bus. US&nbsp;131] at Michigan Highways
<!--*[http://www.state-ends.com/michigan/us/131/bus/big_rapids/ Big Rapids Bus. US&nbsp;131] at Michigan Highway Ends-->
<!--*{{osmrelation-inline|443212|Cadillac Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-C Cadillac Bus. US&nbsp;131] at Michigan Highways
<!--*[http://www.state-ends.com/michigan/us/131/bus/cadillac/ Cadillac Bus. US&nbsp;131] at Michigan Highway Ends-->
<!--*{{osmrelation-inline|443212|Manton Bus. US&nbsp;131}}-->
*[http://www.michiganhighways.org/listings/MichHwysBus96-496.html#US-131BUS-M Manton Bus. US&nbsp;131] at Michigan Highways
<!--*[http://www.state-ends.com/michigan/us/131/bus/manton Manton Bus. US&nbsp;131] at Michigan Highway Ends-->
}}

{{US 31}}

{{good article}}

{{DEFAULTSORT:31-1}}
[[Category:Special routes of the United States Numbered Highway System|*31-1]]
[[Category:U.S. Highways in Michigan|31-1B]]
[[Category:U.S. Route 131|Business]]