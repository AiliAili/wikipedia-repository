{{About|the novel in verse by Alexander Pushkin}}
{{Redirect|Onegin|the 1999 film based on the novel|Onegin (film)|the opera singer|Sigrid Onégin}}
{{Infobox book
| name         = Eugene Onegin
| author       = [[Alexander Pushkin]]
| language     = [[Russian language|Russian]]
| country      = Russia
| genre        = [[Novel]], [[Verse (poetry)|verse]]
| publisher    =
| 
| title_orig   = Евгений Онегин
| translator   = [[Vladimir Nabokov]], [[Charles Hepburn Johnston|Charles Johnston]], [[James E. Falen]], and [[Walter W. Arndt]].
| image        = Eugene Onegin book edition.jpg
| image_size = 250px
| caption = First edition of the novel
| illustrator  =
| cover_artist =
| series       =
|release_date  = 1825–1832 (in serial), 1833 (single volume)
| media_type   = Print (hardback & paperback)
| pages        =
| preceded_by  =
| followed_by  =
}}
'''''Eugene Onegin''''' ({{lang-ru|Евге́ний Оне́гин}}, [[BGN/PCGN romanization of Russian|BGN/PCGN]]: ''Yevgeniy Onegin'') is a [[novel in verse]] written by [[Alexander Pushkin]].

''Onegin'' is considered a classic of [[Russian literature]], and its [[eponym]]ous protagonist has served as the model for a number of Russian literary heroes (so-called [[Superfluous man|''superfluous men'']]). It was published in serial form between 1825 and 1832. The first complete edition was published in 1833, and the currently accepted version is based on the 1837 publication.

Almost the entire work is made up of 389 [[stanza]]s of [[iambic tetrameter]] with the unusual [[rhyme scheme]] "AbAbCCddEffEgg", where the uppercase letters represent [[feminine rhyme]]s while the lowercase letters represent [[masculine rhyme]]s. This form has come to be known as the "[[Onegin stanza]]" or the "Pushkin sonnet."

The innovative rhyme scheme, the natural tone and diction, and the economical transparency of presentation all demonstrate the virtuosity which has been instrumental in proclaiming Pushkin as the undisputed master of [[Russian poetry]].

The story is told by a narrator (a lightly fictionalized version of Pushkin's public image), whose tone is educated, worldly, and intimate. The narrator digresses at times, usually to expand on aspects of this social and intellectual world. This allows for a development of the characters and emphasizes the drama of the plot despite its relative simplicity.

The book is admired for the artfulness of its verse narrative as well as for its exploration of life, death, love, ennui, convention and passion.
{{TOC limit}}

==Main characters==
[[File:Eugene Onegin's portrait by Pushkin.jpg|thumb|Eugene Onegin as imagined by Alexander Pushkin, 1830.]]
*Eugene Onegin: A dandy from Saint Petersburg, about 25. An arrogant, selfish and world-weary cynic.
*Vladimir Lensky: A young poet, about 18. A very romantic and naïve dreamer.
*Tatyana Larina: A shy and quiet, but passionate, landowner's daughter. Pushkin referred to her as aged 17 in a letter to [[Pyotr Vyazemsky]].
*Olga Larina: Tatyana's younger sister.

==Plot==
In the 1820s, Eugene Onegin is a bored Saint Petersburg dandy, whose life consists of balls, concerts, parties and nothing more. One day he inherits a landed estate from his uncle. When he moves to the country, he strikes up a friendship with his neighbor, a starry-eyed young poet named Vladimir Lensky. One day, Lensky takes Onegin to dine with the family of his fiancée, the sociable but rather thoughtless Olga Larina. At this meeting he also catches a glimpse of Olga's sister Tatyana. A quiet, precocious romantic and the exact opposite of Olga, Tatyana becomes intensely drawn to Onegin. Soon after, she bares her soul to Onegin in a letter professing her love. Contrary to her expectations, Onegin does not write back. When they meet in person, he rejects her advances politely but dismissively and condescendingly. This famous speech is often referred to as ''Onegin's Sermon'': he admits that the letter was touching, but says that he would quickly grow bored with marriage and can only offer Tatyana friendship; he coldly advises more emotional control in the future, lest another man take advantage of her innocence.

[[File:Eugene Onegin 01 (Kardovsky).jpg|thumb|upright|Onegin by [[Dmitry Kardovsky]], 1909]]

Later, Lensky mischievously invites Onegin to Tatyana's [[name day]] celebration promising a small gathering with just Tatyana, Olga, and their parents. When Onegin arrives, he finds instead a boisterous country ball, a rural parody of and contrast to the society balls of St. Petersburg he has grown tired of. Onegin is irritated with the guests who gossip about him and Tatyana, and with Lensky for persuading him to come. He decides to avenge himself by dancing and flirting with Olga. Olga is insensitive to her fiancé and apparently attracted to Onegin. Earnest and inexperienced, Lensky is wounded to the core and challenges Onegin to fight a duel; Onegin reluctantly accepts, feeling compelled by social convention. During the duel, Onegin unwillingly kills Lensky. Afterwards, he quits his country estate, traveling abroad to deaden his feelings of remorse.

Tatyana visits Onegin's mansion, where she looks through his books and his notes in the margins, and begins to question whether Onegin's character is merely a collage of different literary heroes, and if there is, in fact, no "real Onegin".

Several years pass, and the scene shifts to St. Petersburg.  Onegin has come to attend the most prominent balls and interact with the leaders of old Russian society. He sees the most beautiful woman, who captures the attention of all and is central to society's whirl, and he realizes that it is the same Tatyana whose love he had once turned away. Now she is married to an aged prince (a general). Upon seeing Tatyana again, he becomes obsessed with winning her affection, despite the fact that she is married. However, his attempts are rebuffed. He writes her several letters, but receives no reply. Eventually Onegin manages to see Tatyana and presents to her the opportunity to finally elope after they have become reacquainted. She recalls the days when they might have been happy, but that time has passed. Onegin repeats his love for her. Faltering for a moment, she admits that she still loves him, but she will not allow him to ruin her and declares her determination to remain faithful to her husband. She leaves him regretting his bitter destiny.

[[File:Eugene Onegin illustration.jpg|thumb|Onegin by [[Elena Samokysh-Sudkovskaya]], 1908]]

==Major themes==
{{Synthesis |section |date = September 2011}}
One of the main themes of ''Eugene Onegin'' is the relationship between fiction and real life. People are often shaped by art and the work is packed with allusions to other major literary works.

Another major element is Pushkin's creation of a woman of intelligence and depth in Tatyana, whose vulnerable sincerity and openness on the subject of love has made her the heroine of countless Russian women, despite her apparent naivete. Pushkin, in the final chapter, fuses his Muse and Tatyana's new 'form' in society after a lengthy description of how she has guided him in his works.

[[File:Pushkin Selfportrait with Onegin.jpg|thumb|left|upright|A sketch by Pushkin of himself and Onegin lounging in St. Petersburg]]

Perhaps the darkest theme – despite the light touch of the narration – is his presentation of the deadly inhumanity of social convention. Onegin is its bearer in this work. His induction into selfishness, vanity, and indifference occupies the introduction, and he is unable to escape it when he moves to the country. His inability to relate to the feelings of others and his frozen lack of empathy – the cruelty instilled in him by the "world" – is epitomized in the very first stanza of the first book by his stunningly self-centred thoughts about being with the dying uncle whose estate he is to inherit:

:"But God how deadly dull to sample
:sickroom attendance night and day
:...
:and sighing ask oneself all through
:"When will the devil come for you?"<ref>C H Johnston's translation, adapted slightly</ref>

However, the "devil comes for Onegin" when he literally kills the innocent and the sincere, shooting Lensky in the duel, and metaphorically kills innocence and sincerity when he rejects Tatyana. She learns her lesson, and armoured against feelings and steeped in convention she crushes his later sincerity and remorse. (This epic reversal of roles, and the work's broad social perspectives, provide ample justification for its subtitle "a novel in verse".)

Tatyana's nightmare illustrates the concealed aggression of the "world". She is chased over a frozen winter landscape by a terrifying bear (representing the ferocity of Onegin's inhuman persona) and confronted by demons and goblins in a hut she hopes will provide shelter. This is contrasted to the open vitality of the "real" people at the country ball, giving dramatic emphasis to the war of warm human feelings with the chilling artificiality of society.

So, Onegin has lost his love, killed his only friend, and found no satisfaction in his life. He is a victim of his own pride and selfishness. ''He is doomed to loneliness, and this is his tragedy''.

The conflict between art and life was no mere fiction in Russia. It is illustrated by Pushkin's own fate, his having been killed in a duel. He was driven to death, falling victim to the social conventions of Russian high society.

==Composition and publication==
[[File:Onegin3.jpg|thumb|upright|Onegin proposes to Tatiana, late 19th century illustration by [[Pavel Sokolov (painter)|Pavel Sokolov]]]]
As with many other 19th century [[novel]]s, ''Onegin'' was written and published [[Serial (literature)|serially]], with parts of each chapter often appearing published in magazines before the first printing of each chapter. Many changes, some small and some large, were made from the first appearance to the final edition during Pushkin's lifetime. The following dates mostly come from Nabokov's study of the photographs of Pushkin's drafts that were available at the time, as well as other people's work on the subject.

The first stanza of chapter 1 was started on May 9, 1823, and except for three stanzas (XXXIII, XVIII and XIX), the chapter was finished on October 22. The remaining stanzas were completed and added to his notebook by the first week of October 1824. Chapter 1 was first published as a whole in a booklet on February 16, 1825, with a foreword which suggests that Pushkin had no clear plan on how (or even whether) he would continue the novel.

Chapter 2 was started on October 22, 1823 (the date when most of chapter 1 had been finished), and finished by December 8, except for stanzas XL and XXXV, which were added sometime over the next three months. The first separate edition of chapter 2 appeared on October 20, 1826.

Many events occurred which interrupted the writing of chapter 3. In January 1824, Pushkin stopped work on ''Onegin'' to work on ''[[The Gypsies (poem)|The Gypsies]]''. Except for XXV, stanzas I–XXXI were added on September 25, 1824. Nabokov guesses that Tanya's Letter was written in [[Odessa]] between February 8 and May 31, 1824. Pushkin incurred the displeasure of the Tsarist regime in Odessa and was restricted to his family estate Mikhaylovskoye in [[Pskov]] for two years. He left Odessa on July 21, 1824, and arrived on August 9. Writing resumed on September 5, and chapter 3 was finished (apart from stanza XXXVI) on October 2. The first separate publication of chapter 3 was on October 10, 1827.

Chapter 4 was started in October 1824, by the end of the year Pushkin had written 23 stanzas and had reached XXVII by January 5, 1825, at which point he started writing stanzas for Onegin's Journey and worked on other pieces of writing. He thought that it was finished on September 12, 1825, but later continued the process of rearranging, adding and omitting stanzas until the first week of 1826. The first separate edition of chapter 4 appeared with chapter 5 in a publication produced between January 31 and February 2, 1828.

The writing of chapter 5 began on January 4, 1826, and 24 stanzas were complete before the start of his trip to petition the Tsar for his freedom. He left for this trip on September 4 and returned on November 2, 1826. He completed the rest of the chapter in the week November 15 to 22, 1826. The first separate edition of chapter 5 appeared with chapter 4 in a publication produced between January 31 and February 2, 1828.

When Nabokov carried out his study on the writing of Onegin, the manuscript of chapter 6 was lost, but we know that Pushkin started chapter 6 before finishing chapter 5. Most of  chapter 6 appears to have been written before the beginning of December 19, 1826 when Pushkin returned to Moscow after exile in his family estate.  Many stanzas appeared to have been written between November 22 and 25, 1826. On March 23, 1828, the first separate edition of chapter 6 was published.

Pushkin started writing chapter 7 in March 1827, but aborted his original plan for the plot of the chapter and started on a different tack, completing the chapter on November 4, 1828. The first separate edition of chapter 7 was first printed on March 18, 1836.

Pushkin intended to write a chapter called "Onegin's Journey", which occurred between the events of chapters 7 and 8, and in fact was supposed to be the eighth chapter. Fragments of this incomplete chapter were published, in the same way that parts of each chapter had been published in magazines before each chapter was first published in its first separate edition. When Pushkin first completed chapter 8 he published it as the final chapter and included within its denouement the line ''nine cantos I have written'' still intending to complete this missing chapter. When Pushkin finally decided to abandon this chapter he removed parts of the ending to fit with the change.

Chapter 8 was begun before December 24, 1829, while Pushkin was in Petersburg. In August 1830, he went to [[Boldino]] (the Pushkin family estate)<ref>http://www.government.nnov.ru/?id=3721, retrieved 13 July 2007.</ref><ref>http://www.russianmuseums.info/M1890", retrieved 13 July 2007.</ref> where, due to an epidemic of [[cholera]], he was forced to stay for three months. During this time, he produced what Nabokov describes as an "incredible number of masterpieces" and finished copying out chapter 8 on September 25, 1830. During the summer of 1831, Pushkin revised and completed chapter 8 apart from "Onegin's Letter", which was completed on October 5, 1831. The first separate edition of chapter 8 appeared on January 10, 1832.

Pushkin wrote at least 18 stanzas of a never-completed tenth chapter. It contained many satires and even direct criticism on contemporary Russian rulers, including the [[Nicholas I of Russia|Emperor himself]]. Afraid of being prosecuted for dissidence, Pushkin burnt most of the tenth chapter. Very little of it survived in Pushkin's notebooks.<ref>http://www.chernov-trezin.narod.ru/Onegin.htm «ЕВГЕНИЙ ОНЕГИН». СОЖЖЕННАЯ ГЛАВА. опыт реконструкции формы</ref>

The first complete edition of the book was published in 1833. Slight corrections were made by Pushkin for the 1837 edition. The standard accepted text is based on the 1837 edition with a few changes due to the Tsar's censorship restored.

==The duel==
In Pushkin's time, the early 19th century, [[duel]]s were very strictly regulated. A second's primary duty was to prevent the duel from actually happening, and only when both combatants were unwilling to stand down were they to make sure that the duel proceeded according to formalised rules.<ref name="Lotman duel">{{ru icon}} [[Yuri Lotman]], [http://www.gumer.info/bibliotek_Buks/Literat/Lotm_EO/09.php Роман А.С. Пушкина «Евгений Онегин». Комментарий. Дуэль.], retrieved 16 April 2007.</ref> A challenger's second should therefore always ask the challenged party if he wants to apologise for his actions that have led to the challenge.

In ''Eugene Onegin'', Lensky's second, Zaretsky, does not ask Onegin even once if he would like to apologise, and because Onegin is not allowed to apologise on his own initiative, the duel takes place, with fatal consequences. Zaretsky is described as "classical and pedantic in duels" (chapter 6, stanza XXVI), and this seems very out of character for a nobleman. Zaretsky's first chance to end the duel is when he delivers Lensky's written challenge to Onegin (chapter 6, stanza IX). Instead of asking Onegin if he would like to apologise, he apologises for having much to do at home and leaves as soon as Onegin (obligatorily) accepts the challenge.

On the day of the duel, Zaretsky gets several more chances to prevent the duel from happening. Because dueling was forbidden in the [[Russian Empire]], duels were always held at dawn. Zaretsky urges Lensky to get ready shortly after 6&nbsp;o'clock in the morning (chapter 6, stanza XXIII), while the sun only rises at 20 past 8, because he expects Onegin to be on time. However, Onegin oversleeps (chapter 6, stanza XXIV), and arrives on the scene more than an hour late.<ref name="Lotman duel"/> According to the dueling codex, if a duelist arrives more than 15 minutes late, he automatically forfeits the duel.<ref name="Dueling codex">V. Durasov, [http://www.gumer.info/bibliotek_Buks/Literat/Lotm_EO/09.php Dueling codex], as cited in [[Yuri Lotman]], Пушкин. Биография писателя. Статьи и заметки., retrieved 16 April 2007.</ref> Lensky and Zaretsky have been waiting all that time (chapter 6, stanza XXVI), even though it was Zaretsky's duty to proclaim Lensky as winner and take him home.

When Onegin finally arrives, Zaretsky is supposed to ask him a final time if he would like to apologise. Instead, Zaretsky is surprised by the apparent absence of Onegin's second. Onegin, against all rules, appoints his servant Guillot as his second which was the last action to take from a noble man (chapter 6, stanza XXVII), a blatant insult for the nobleman Zaretsky.<ref name="Lotman duel" /> Zaretsky angrily accepts Guillot as Onegin's second. By his actions, Zaretsky does not act as a nobleman should, in the end Onegin wins the duel.<ref name="Lotman duel" />

Onegin himself, however, tried as he could to prevent the fatal outcome, and killed Lensky unwillingly and almost by accident. As the first shooter, he couldn't show that he was deliberately trying to miss the opponent, because this was considered as a serious insult and could create a formal reason to appoint another duel. Instead, he tried to minimize his chances by shooting without precise aiming, from the maximal possible distance, not even trying to come closer and get a clear shot.<ref name="Lotman duel" />

==Translations==
Translators of ''Eugene Onegin'' have all had to adopt a trade-off between precision and preservation of poetic imperatives. This particular challenge and the importance of ''Eugene Onegin'' in Russian literature have resulted in an impressive number of competing translations.

===Into English===

====Arndt and Nabokov====
[[Walter W. Arndt]]'s 1963 translation (ISBN 0-87501-106-3) was written keeping to the strict rhyme scheme of the Onegin stanza and won the [[Bollingen Prize|Bollingen Prize for translation]]. It is still considered one of the best translations.{{Citation needed|date=November 2009}}

[[Vladimir Nabokov]] severely criticised Arndt's translation, as he had criticised many previous (and later) translations. Nabokov's main criticism of Arndt's and other translations is that they sacrificed literalness and exactness for the sake of preserving the melody and rhyme.

Accordingly, in 1964 he published in four volumes his own translation, which conformed scrupulously to the sense while completely eschewing melody and rhyme. The first volume contains an introduction by Nabokov and the text of the translation. The Introduction discusses the structure of the novel, the Onegin stanza in which it is written and Pushkin's opinion of Onegin (using Pushkin's letters to his friends); and gives a detailed account of both the time over which Pushkin wrote Onegin and the various forms any part of it appeared in publication before Pushkin's death (after which there is a huge proliferation of the number of different editions). The second and third volumes consist of very detailed and rigorous notes to the text. The fourth volume contains a facsimile of the 1837 edition. The discussion of the Onegin stanza contains the poem ''On Translating "Eugene Onegin"'', which first appeared in print in ''[[The New Yorker]]'' on January 8, 1955, and is written in two Onegin stanzas.<ref name="Nabokov1955">{{Cite news|url=https://www.newyorker.com/archive/1955/01/08/1955_01_08_034_TNY_CARDS_000247922|title=On Translating "Eugene Onegin"|last=Nabokov|first=Vladimir|date=1955-01-08|publisher=[[The New Yorker]]|pages=34|accessdate=18 October 2008}} (Poem is reproduced [http://www.tetrameter.com/nabokov.htm  here]</ref> The poem is reproduced there both so that the reader of his translation would have some experience of this unique form, and also to act as a further defence of his decision to write his translation in prose.

Nabokov's previously close friend [[Edmund Wilson]] reviewed Nabokov's translation in the ''New York Review of Books'', which sparked an exchange of letters there and an enduring falling-out between them.<ref name="Wilson1965">{{Cite journal|last=Wilson|first=Edmund|date=1965-06-15|title=The Strange Case of Pushkin and Nabokov|journal=[[The New York Review of Books]]|volume=4|issue=12|url=http://www.nybooks.com/articles/12829|accessdate=18 October 2008}}</ref>

[[John Bayley (writer)|John Bayley]] has described Nabokov's commentary as '"by far the most erudite as well as the most fascinating commentary in English on Pushkin's poem", and "as scrupulously accurate, in terms of grammar, sense and phrasing, as it is idiosyncratic and Nabokovian in its vocabulary". Some consider this "Nabokovian vocabulary" a failing, for it might require even educated speakers of English to reach for the dictionary on occasion.{{Citation needed|date=November 2009}} However, it is generally agreed that Nabokov's translation is extremely accurate.

====Other English translations====
Babette Deutsch published a translation in 1935 preserving the Onegin stanzas.

The [[Pushkin Press]] published a translation in 1937 (reprinted 1943) by the Oxford scholar [[Oliver Elton]], with illustrations by [[Mstislav Dobuzhinsky|M. V. Dobujinsky]].

In 1977, [[Charles Hepburn Johnston|Charles Johnston]] published [http://lib.ru/LITRA/PUSHKIN/ENGLISH/onegin_j.txt another translation] trying to preserve the Onegin stanza, which is generally considered to surpass Arndt's. Johnston's translation is influenced by Nabokov. [[Vikram Seth]]'s novel ''[[The Golden Gate (Vikram Seth novel)|The Golden Gate]]'' was inspired by this translation.

[[James E. Falen]] (the professor of Russian at the [[University of Tennessee]]) published a translation in 1995 which was also influenced by Nabokov's translation, but preserved the Onegin stanzas (ISBN 0809316307). This translation is considered to be the most faithful one to Pushkin's spirit according to Russian critics and translators.{{citation needed|date=January 2017}}

[[Douglas Hofstadter]] published a translation in 1999, again preserving the Onegin stanzas, after having summarised the controversy (and severely criticised Nabokov's attitude towards verse translation) in his book ''[[Le Ton beau de Marot]]''. Hofstadter's translation has a unique lexicon of both high and low register words, as well as unexpected and almost reaching rhymes that give the work a comedic flair.

Tom Beck published a translation in 2004, preserving the Onegin stanzas. (ISBN 1-903517-28-1)

Wordsworths Classics in 2005 published a translation by Roger Clarke into English prose, seeking to retain the lyricism of Pushkin's Russian. Some consider it the very best English translation.{{citation needed|date=January 2017}}

In April 2008, [[Henry M. Hoyt]] published, through Dog Ear Publishing, a translation which preserves the meter of the Onegin stanza, but is unrhymed, his stated intention being to avoid the verbal changes forced by the invention of new rhymes in the target language while  preserving the rhythm of the source. (ISBN 978-159858-340-3)

In September 2008, [[Stanley Mitchell]], emeritus professor of aesthetics at the [[University of Derby]], published, through [[Penguin Books]], a complete translation, again preserving the Onegin stanzas in English. (ISBN 978-0-140-44810-8)

There are a number of [http://www-users.york.ac.uk/~pml1/onegin/welcome.htm lesser known English translations].

===Into other languages===

====French====
There are at least eight published French translations of ''Eugene Onegin''. The most recent appeared in 2005: the translator, André Markovicz, respects Pushkin's original stanzas.<ref name="Pushkin2005">{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by André Markovicz)|title=Eugène Onéguine|publisher=[[Actes Sud]], 2005|isbn=978-2-7427-5700-8|year=2005}}</ref> Other translations include those of Paul Béesau (1868), Gaston Pérot (1902, in verse), Nata Minor (who received the Prix Nelly Sachs, given to the best translation into French of poetry), Roger Legras, Maurice Colin, Michel Bayat and Jean-Louis Backès (who does not preserve the stanzas).<ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Paul Béesau)|title=Eugène Onéguine|publisher=Paris, A. Franck, 1868|oclc=23735163|language=French}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Gaston Pérot)|title=Eugène Onéguine|publisher=Paris etc. : Tallandier, 1902|oclc=65764005|language=French}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Nata Minor)|title=Eugène Oniéguine|publisher=[[Éditions du Seuil]], 1997|isbn=2-02-032956-5|language=French|year=1998}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Roger Legras)|title=Eugène Oniéguine|publisher=L'Age d'Homme, 1994|isbn=978-2-8251-0495-8|language=French|year=1994}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Maurice Colin)|title=Eugène Oniéguine|publisher=Paris : Belles Lettres, 1980|isbn=978-2-251-63059-5|oclc=7838242|language=French|year=1980}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Michel Bayat)|title=Eugène Oniéguine|publisher=Compagnie du livre français, 1975|oclc=82573703|language=French}}
</ref><ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Jean-Louis Backès)|title=Eugène Onéguine|publisher=Paris|date=1967–1968|oclc=32350412|language=French}}
</ref>
As a 20-year-old, former French President [[Jacques Chirac]] also wrote a translation, which was never published.<ref name="Ref_2008">{{Cite news|url=http://tempsreel.nouvelobs.com/actualites/international/20080612.OBS8212/russie__chirac_decore_salue_la_voie_de_la_democratie.html?idfx=RSS_notr|title=Russie : Chirac, décoré, salue la "voie de la démocratie"|date=2008-06-23|publisher=[[Nouvel Observateur]]|language=French|accessdate=18 October 2008}}</ref><ref name="Tondre2000">{{Cite book|last=Tondre|first=Jacques Michel|title=Jacques Chirac dans le texte|publisher=Paris : Ramsay|year=2000|isbn=978-2-84114-490-7|oclc=47023639|language=French}} ([http://anarcho-monarchiste.over-blog.org/article-6908920-6.html Relevant excerpt])</ref>

====German====
There are at least a dozen published translations of ''Onegin'' in [[German language|German]].
* Carl Friedrich von der Borg, Eugenius Onegin, first part published in "Der Refraktor. Ein Centralblatt Deutschen Lebens in Russland." Dorpat, 1836 in five series, starting with the 14th issue on 1 August 1836, ending with the 18th issue on 29 August 1836
* R. Lippert, Verlag von Wilhelm Engelmann, Leipzig 1840
* [[Friedrich von Bodenstedt]], Verlag der Deckerschen Geheimen Ober-Hofbuchdruckerei, Berlin 1854
* Adolf Seubert, Reclam, Leipzig 1872/73
* Dr. Blumenthal, Moscow 1878
* Dr. Alexis Lupus, nur das 1. Kapitel, Leipzig and St. Petersburg 1899
* Theodor Commichau, Verlag G. Müller, Munich and Leipzig 1916
* Theodor Commichau and Arthur Luther, 1923
* Theodor Commichau, Arthur Luther and Maximilian Schick, SWA-Verlag, Leipzig and Berlin 1947
* Elfriede Eckardt-Skalberg, Verlag Bühler, Baden-Baden 1947
* Johannes von Guenther, Reclam, Leipzig 1949
* Theodor Commichau and Konrad Schmidt, Weimar 1958
* Theodor Commichau and Martin Remané, Reclam, Leipzig 1965
* Manfred von der Ropp and Felix Zielinski, Winkler, Munich 1972
* Kay Borowsky, Reclam, Stuttgart 1972 (translation of prose)
* Rolf-Dietrich Keil, Wilhelm Schmitz Verlag, Gießen 1980
* Ulrich Busch, [[Manesse Verlag]], Zurich 1981
* Sabine Baumann, unter Mitarbeit von Christiane Körner, Stroemfeld, Frankfurt am Main 2009

====Italian====
There are several Italian translations of ''Onegin''. One of the earliest was published by G. Cassone in 1906. Ettore Lo Gatto translated the novel twice, in 1922 in prose and in 1950 in hendecasyllables.<ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Ettore Lo Gatto)|title=Eugenio Onieghin; romanzo in versi|publisher=Sansoni, 1967|oclc=21023463|language=Italian}}
</ref>
More recent translations are those by Giovanni Giudici (a first version in 1975, a second one in 1990, in lines of unequal length) and by [[Pia Pera]] (1996).<ref>
{{Cite book|last=Pushkin|first=Aleksandr|others=(translation by Giovanni Giudici)|title=Eugenio Onieghin di Aleksandr S. Puskin in versi italiani|publisher=Garzanti, 1999|isbn=978-88-11-66927-2|oclc=41951692|language=Italian|year=1999}}
</ref>

====Hebrew====
* [[Avraham Shlonsky]], 1937
* Avraham Levinson, 1937

====Esperanto====
* Trans. [[Nikolai Vladimirovich Nekrasov|Nikolao Nekrasov]], published by [[Sennacieca Asocio Tutmonda]], 1931

====Spanish====
*Eugene Onegin – Novel in Verse. Direct Spanish version Russian original poetic form, notes and illustrations by Alberto Musso Nicholas. Mendoza, Argentina, Zeta Publishers, April 2005

==Film, TV or theatrical adaptations==
[[File:Nicky & Ella.jpg|thumb|[[Nicholas II of Russia|Nicholas of Russia]] and [[Princess Elisabeth of Hesse and by Rhine (1864–1918)|Elisabeth of Hesse]] as Eugene and Tatiana]]
===Opera===
{{Main|Eugene Onegin (opera)}}
The 1879 opera [[Eugene Onegin (opera)|''Eugene Onegin'']], by [[Pyotr Ilyich Tchaikovsky|Tchaikovsky]], based on the story, is perhaps the version that most people are familiar with. There are many recordings of the score, and it is one of the most commonly performed operas in the world.

===Ballet===
[[John Cranko]] choreographed a three-act ballet using [[Pyotr Ilyich Tchaikovsky|Tchaikovsky's]] music in an arrangement by [[Kurt-Heinz Stolze]]. However, Stolze did not use any music from Tchaikovsky’s [[Eugene Onegin (opera)|opera of the same name]]. Instead, he orchestrated some little-known piano works by Tchaikovsky such as ''[[The Seasons (Tchaikovsky)|The Seasons]]'', along with themes from the opera ''[[Cherevichki]]''<ref>[http://dev.radenterprises.co.uk/pdf/36pp_guidebook4-5-final.pdf Alternative Music for Grades 1–5]</ref> and the latter part of the symphonic fantasia ''[[Francesca da Rimini (Tchaikovsky)|Francesca da Rimini]]''.<ref>[http://johnamismusic.blogspot.com/2007/04/royal-ballet-is-tops.html John Amis online]</ref>

Choreographer [[Boris Eifman]] staged a modern rendition of ''Eugene Onegin'' as a ballet taking place in modern Moscow. Performed by Eifman Ballet of St. Petersburg, music by Alexander Sitkovetsky, with excerpts from Tchaikovsky opera ''Eugene Onegin''.<ref>[https://www.youtube.com/watch?v=0bbDNWcy7iU Eifman Ballet of St. Petersburg: ONEGIN ]</ref><ref>http://www.startribune.com/entertainment/onstage/43695332.html Eifman's 'Onegin'</ref>

Most recently [[Lera Auerbach]] created a ballet score titled ''Tatiana'', with a libretto written by [[John Neumeier]] for his choreographic interpretation and staging of [[Alexander Pushkin]]'s ''Eugene Onegin'', for a co-production by the Hamburg State Opera and the Stanislavski and Nemirovich-Danchenko Moscow Academic Music Theatre in Moscow.<ref>[http://www.sikorski.de/8642/en/new_neumeier_ballet_tatiana_by_lera_auerbach.html#.VXX4M2pxVTM.link New Neumeier Ballet ''Tatiana'' by Lera Auerbach]
</ref>

===Incidental music===
A staged version was adapted by [[Sigizmund Krzhizhanovsky]] and slated for production in the [[Soviet Union]] in 1936, directed by [[Alexander Tairov]] and with [[incidental music]] by [[Sergei Prokofiev]], as part of the centennial celebration of Pushkin's death. However, due to threats of Stalinist repercussion for artistic liberties taken during the production and artistic differences between Tairov and Krzhizhanovsky, rehearsals were abandoned and the production was never put on.<ref>[http://www.princeton.edu/main/news/archive/S32/86/37C73/index.xml?section=topstories News at Princeton], 'Eugene Onegin' project a mosaic of multidisciplinary productions, February 7, 2012</ref>

===Play===
[[Christopher Webber]]'s play ''Tatyana'' was written for [[Nottingham Playhouse]] in 1989. It successfully combines spoken dialogue and narration from the novel, with music arranged from [[Pyotr Ilyich Tchaikovsky|Tchaikovsky]]'s operatic score, and incorporates some striking theatrical sequences inspired by Tatyana's dreams in the original. The title role was played by [[Josie Lawrence]], and the director was Pip Broughton. In 2016, the Vancouver, Canada [[Arts Club Theatre Company]] staged a musical version called ''Onegin'' by [[Amiel Gladstone]] and [[Veda Hille]]

2016 Theatre production by the legendary Vakhtangov State Academic Theatre of Russia. This production was referred to as "exuberant, indelible and arrestingly beautiful" by the New York Times, starring the incomparable Sergey Makovetskiy in the title role, is a sumptuous work that will leave you with enough beautiful memories and images

===Film===
*In 1911, the first screen version of the novel was filmed: the Russian silent film ''Yevgeni Onegin'' ("Eugene Onegin"), directed by Vasili Goncharov and starring Arseniy Bibikov, Petr Birjukov and [[Pyotr Chardynin]].
*In 1919 in Germany was produced a silent film ''Eugen Onegin'', based on the novel. The film was directed by Alfred Halm, starring [[Frederic Zelnik]] as Onegin.
*In 1958 [[Lenfilm]] produced a TV film ''[[Eugene Onegin (film)|Eugene Onegin]]'', which was, actually, not a screen version of the novel, but a screen version of the opera ''[[Eugene Onegin (opera)|Eugene Onegin]]'' by [[Pyotr Tchaikovsky|Tchaikovsky]]. The film was directed by Roman Tikhomirov and starred Vadim Medvedev as Onegin, Ariadna Shengelaya as Tatyana and Igor Ozerov as Lensky. The principal solo parts were performed by notable opera singers of the [[Bolshoi Theatre]]. The film was well received by critics and viewers.
*In 1972, Zweites Deutsches Fernsehen (ZDF) produced a music film ''Eugen Onegin''
*In 1988, Decca/Channel 4 produced a film adaptation of Tchaikovsky's opera, directed by Petr Weigl. [[Georg Solti|Sir Georg Solti]] acted as the conductor, while the cast featured Michal Dočolomanský as Onegin and [[Magdaléna Vášáryová]] as Tatyana. One major difference from the novel is the duel: Onegin is presented as deliberately shooting to hit and is unrepentant at the end.
*In 1994, the TV film ''Yevgeny Onyegin'' was produced, directed by Humphrey Burton, starring Wojtek Drabowicz as Onyegin.
*The 1999 film, ''[[Onegin (film)|Onegin]]'', is an English adaptation of Pushkin's work, directed by [[Martha Fiennes]], and starring [[Ralph Fiennes]] as Onegin, [[Liv Tyler]] as Tatiana and [[Toby Stephens]] as Lensky. The film compresses the events of the novel somewhat: for example, the Naming Day celebrations take place on the same day as Onegin's speech to Tatiana. The 1999 film, much like the 1988 one, also gives the impression that during the duel sequence Onegin deliberately shoots to kill. This screen version was also criticized for a number of mistakes and inconsistencies.

===Audiobook===
In 2012, [[Stephen Fry]] recorded an audiobook of the novel in the translation by [[James E. Falen]].

==Footnotes==
{{Reflist|30em}}

==References==
*Aleksandr Pushkin, London 1964, Princeton 1975, ''Eugene Onegin a novel in verse. Translated from Russian with a commentary by [[Vladimir Nabokov]]'' ISBN 0-691-01905-3
*Alexander Pushkin, Penguin 1979 ''Eugene Onegin a novel in verse. Translated by Charles Johnston, Introduction and notes by Michael Basker, with a preface by John Bayley (Revised Edition)'' ISBN 0-14-044803-9
*Alexandr Pushkin, Basic Books; New Ed edition, ''Eugene Onegin: A Novel in Verse'' Translated by [[Douglas Hofstadter]] ISBN 0-465-02094-1
*[[Yuri Lotman]], Пушкин. Биография писателя. Статьи и заметки. Available online: [http://www.gumer.info/bibliotek_Buks/Literat/Lotm_Pusch/index.php]. Contains detailed annotations about Eugene Onegin.
* [http://white.narod.ru A.A. Beliy], [http://white.narod.ru/Onegin.htm «Génie ou neige»], "Voprosy literaturi", n. 1, Moscow 2008, p.&nbsp;115; contains annotations about Eugene Onegin.

==External links==
* [http://www.rvb.ru/pushkin/01text/04onegin/01onegin/0836.htm?start=0&length=all ''Yevgeny Onegin''] The full text of the poem in Russian
*  A bogus version of [https://web.archive.org/web/20091109053203/http://scaramouche2004.webs.com/thexthchapter.htm The Xth Chapter]
* [http://lib.ru/LITRA/PUSHKIN/ENGLISH/onegin_j.txt ''Eugene Onegin'' at lib.ru] Charles Johnston's complete translation
* [http://www.poetryloverspage.com/yevgeny/pushkin/evgeny_onegin.html The Poetry Lovers' Page] (a translation by Yevgeny Bonver)
* [http://www.pushkins-poems.com/ Pushkin's Poems] (a translation by G. R. Ledger with more of Pushkin's poetry)
* [http://adaweb.walkerart.org/influx/muntadas/nytbooks.html What's Gained in Translation] An article by [[Douglas Hofstadter]] on the book, which explains how he can judge the relative worth of different translations of Onegin without being able to read Russian
* {{librivox book | title=Eugene Onéguine | author=Alexander PUSHKIN}}
* [http://fryreadsonegin.com/ An Audiobook Narrated by Stephen Fry]

{{Alexander Pushkin}}
{{Eugene Onegin}}

{{Authority control}}

[[Category:1833 novels]]
[[Category:Novels by Aleksandr Pushkin]]
[[Category:Poetry by Aleksandr Pushkin]]
[[Category:Russian novels adapted into films]]
[[Category:Novels first published in serial form]]
[[Category:Sonnet studies]]
[[Category:Verse novels]]
[[Category:Characters in Russian novels of the 19th century|Onegin, Eugene]]