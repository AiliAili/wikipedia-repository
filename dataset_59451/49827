{{about|the book|the band|Green Carnation|the symbol|LGBT symbols#Other symbols}}
{{EngvarB|date=January 2017}}
{{Use dmy dates|date=January 2017}}
{{italictitle}}
{{more footnotes|date=September 2014}}
[[Image:greencarnation.jpg||thumb|right|'''The Green Carnation''' book cover]]
'''''The Green Carnation''''', first published anonymously in 1894, was a scandalous novel by [[Robert Smythe Hichens|Robert Hichens]] whose lead characters are closely based on [[Oscar Wilde]] and [[Lord Alfred Douglas]] &ndash; also known as "Bosie", whom the author personally knew. It was an instant ''[[succès de scandale]]'' on both sides of the Atlantic.

The reviewer for ''[[The Observer]]'' wrote, "''The Green Carnation'' will be read and discussed by everyone... nothing so impudent, so bold, or so delicious has been printed these many years."

The book features the characters of "Esmé Amarinth" (Wilde), and "Lord Reginald (Reggie) Hastings" (Douglas).  The words put in the mouths of the hero and his young friend in the story are mostly gathered from the sayings of their originals.  Robert Hichens spent nearly a year "in the company of the men" and was able to accurately recreate the atmosphere and relationship between Oscar and Bosie.

The book was withdrawn from circulation in 1895, but by that time the damage had been done.  Wilde soon stood three consecutive trials for [[Labouchere Amendment|gross indecency]] and was sentenced to two years at hard labour.  ''The Green Carnation'' was one of the works used against him by the prosecution.

''The Green Carnation'' was republished in 1948 with an introduction by the author, which also included Wilde's letter to ''[[The Pall Mall Gazette]]'', 2 October 1894, denying he was the anonymous author.  It was reissued in paperback in this form in 1992, and republished again in 2006 as a hardcover with a foreword by [[Anthony Wynn]].

In the letter Wilde wrote:<blockquote>Sir. Kindly allow me to contradict, in the most emphatic manner, the suggestion, made in your issue of Thursday last, and since then copied into many other newspapers, that I am the author of ''The Green Carnation''. I invented that magnificent flower. But with the middle-class and mediocre book that usurps its strangely beautiful name I have, I need hardly say, nothing whatsoever to do. The Flower is a work of Art. The book is not.<ref>{{cite web|last1=Cacciottolo|first1=Mario|title=Oscar Wilde's jail key and letter on display in Malta|url=http://www.bbc.co.uk/news/world-europe-39230065|website=BBC News|accessdate=10 March 2017|date=10 March 2017}}</ref></blockquote>

==References==
{{Reflist}}

== External links ==
{{wikisource|The Green Carnation}}
* [http://www.gutenberg.org/etext/24499 Project Gutenberg edition]
* [http://oscar-wilde-in-america.tumblr.com/post/54258574098/the-green-carnation-wildes-letter-to-pall-mall Wilde's letter to the Pall Mall Gazette] denying authorship of ''The Green Carnation''.
* {{librivox book | title=The Green Carnation | author=Robert Smythe HICHENS}}

{{Authority control}}

{{DEFAULTSORT:Green Carnation, The}}
[[Category:1894 novels]]
[[Category:British LGBT novels]]
[[Category:19th-century British novels]]
[[Category:English novels]]
[[Category:British satirical novels]]
[[Category:Cultural depictions of Oscar Wilde]]
[[Category:Roman à clef novels]]