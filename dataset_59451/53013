{{unreferenced|date=April 2017}}
{{cleanup|date=October 2010}}
'''The Public-Access Computer Systems Review''' (abbreviated ''PACS Review'') was a free electronic [[academic journal|journal]] about end-user computer systems in [[library|libraries]]. 

== History ==
It was established in 1989 by Charles W. Bailey, Jr., who served as Editor-in-Chief for volumes one (1990) to seven (1996). It was published by the [[University of Houston]] Libraries.

The establishment of the journal was announced on the PACS-L list on August 16, 1989. A call for papers was issued on October 16, 1989. The publication of the first issue was announced on January 3, 1990. The journal was cataloged on [[OCLC]] on February 1, 1990 (record number 20987125) by the [[Library of Congress]] National Serials Data Program (it was assigned an [[ISSN]] number at the same time).

Initially, the journal published scholarly papers (Communications section), columns, and reviews. Papers in the Communications section were selected by the Editor-in-Chief and the Associate Editor, Communications. A private mailing list was utilized for communication with editorial staff and Editorial Board members. Most communication with authors was done via e-mail, including paper submission.

The ''PACS Review'' was published three times a year. New issue announcements were distributed as e-mail messages on the PACS-L list, and users retrieved the [[ASCII]] article files from the University of Houston's LISTSERV via e-mail. (LISTSERV distribution was suspended in 1999.)

Authors retained the copyright to ''PACS Review'' articles, and they gave the University of Houston the nonexclusive right to publish the articles in the journal and in future publications. Authors could republish their articles elsewhere, but they agreed to mention prior publication of the articles in the PACS Review within these works. Copying of ''PACS Review'' articles was permitted for educational, noncommercial use by academic computer centers, individual scholars, and libraries.

On October 29, 1991, the journal adopted a more flexible publication schedule that reduced article publication time.

A Refereed Articles section of the journal was announced on November 11, 1991, and a call for papers was issued on February 6, 1992. The Refereed Articles section included papers that were [[peer review]]ed by Editorial Board members using a double-blind review procedure, which was usually conducted via e-mail. The publication of the first refereed paper was announced on April 6, 1992.

Between 1992 and 1996, the first five volumes of ''The Public-Access Computer Systems Review'' were also published in book form by the [[Library and Information Technology Association]] (LITA). Walt Crawford prepared the camera-ready copy for these volumes and Charles W. Bailey, Jr. provided editorial support.

Starting on April 6, 1992, ''PACS Review'' issue publication announcements were also distributed on the PACS-P list.

On January 29, 1994, the distribution of the journal via University of Houston Libraries' [[Gopher (protocol)|Gopher]] server was announced. (Gopher distribution was suspended in 1998.) The journal ceased publishing reviews in 1994.

On March 9, 1995, the distribution of the journal via University of Houston Libraries' Web server was announced.

Starting with the first issue of volume six (March 21, 1995), the ''PACS Review''
# published articles in both ASCII and [[HTML]] formats, 
# offered HTML articles with both internal and external links, and 
# gave authors the option of updating the HTML version of their articles. The first updated article was "Network-Based Electronic Publishing of Scholarly Works: A Selective Bibliography" [http://info.lib.uh.edu/pr/v6/n1/bail6n1.html] by Charles W. Bailey, Jr. An archive of the 26 versions of this article is available.

At the end of 1996, Mr. Bailey stepped down as Editor-in-Chief.

Pat Ensor and Thomas C. Wilson became Editors-in-Chief in January 1997. They edited volumes eight (1997) and nine (1998). Publication of the last issue was announced on June 18, 1998. Papers were under consideration for publication until August 2000, when the journal ceased operation.

During its nine years of publication, the ''PACS Review'' published 42 issues that included 112 articles, columns, reviews, and editorials.

The PACS Review was indexed in ''Current Index to Journals in Education'', ''Information Science Abstracts'', and ''Library Literature''.

== Articles that discuss the ''PACS Review'' ==
The following articles discuss the ''PACS Review'':
* Bailey, Charles W., Jr. "Electronic (Online) Publishing in Action . . . The Public-Access Computer Systems Review and Other Electronic Serials." ''ONLINE'' 15 (January 1991): 28-35.
* Ensor, Pat, and Thomas Wilson. "Public-Access Computer Systems Review: Testing the Promise." ''The Journal of Electronic Publishing'' 3, no. 1 (1997). [http://www.press.umich.edu/jep/03-01/pacs.html]
* Moothart, Tom. "Charles W. Bailey, Jr.: Editor, Publisher, Innovator." ''Serials Review'' 23, no. 1 (1997): 59-62.

== Columnists ==
The following individuals wrote columns for the ''PACS Review'':
* Priscilla Caplan (1992-1998)
* Walt Crawford (1989-1995)
* Martin Halbert (1990-1993)

== External links ==
* [http://info.lib.uh.edu/pr/pacsrev.html Home page] of the ''PACS Review''.

{{DEFAULTSORT:Public-Access Computer Systems Review}}
[[Category:Publications established in 1990]]
[[Category:Publications disestablished in 1998]]
[[Category:Library science journals]]