{{Infobox journal
| title = eco.mont
| italic title = no<!-- see displaytitle below -->
| cover = [[File:Cover ecomont journal.jpg]]
| discipline = [[Mountain research]]
| abbreviation =
| editor = [[Axel Borsdorf]], [[Günter Köck]]
| publisher = [[Austrian Academy of Sciences]] and [[University of Innsbruck|Innsbruck University Press]]
| country = [[Austria]]
| history = 2009-present
| impact = 0.306
| impact-year = 2012
| frequency = Biannual
| website = http://ecomont.mountainresearch.at
| link2 = http://ecomont.mountainresearch.at/index.php/editions
| link2-name = Online archive
| ISSN = 2073-106X
| eISSN = 2073-1558
| OCLC = 786451449
}}
{{DISPLAYTITLE:''eco.mont''}}
'''''eco.mont''' – Journal on Protected Mountain Areas Research and Management'' is a [[peer-reviewed]] [[open access]] [[scientific journal]] on [[mountain research]] in [[protected area]]s. 

== Overview ==
The journal was established by the [[Alpine Network of Protected Areas]], the [[International Scientific Committee on Research in the Alps]], the [[Austrian Academy of Sciences]], and the [[University of Innsbruck]]. The [[editors-in-chief]] are [[Axel Borsdorf]] and [[Günter Köck]]. The journal covers research on protected areas in the Alps and in other mountain regions.<ref>[http://dx.doi.org/10.1553/eco.mont1s3 Editorial of ''eco.mont'' vol. 1(1)]</ref> From 2015 the journal is published as open access.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Science Citation Index Expanded]], [[Current Contents]]/Agriculture, Biology & Environmental Sciences,<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-04-14}}</ref> and [[Scopus]].<ref name=SCOPUS>{{cite web |url=http://files.sciverse.com/documents/xlsx/title_list.xlsx |title=Scopus Title List |publisher=[[Elsevier]] |work=What does Scopus cover |accessdate=2013-06-26}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://ecomont.mountainresearch.at}}

{{DEFAULTSORT:eco.mont}}
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 2009]]
[[Category:Geography journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Academic journals published by university presses]]