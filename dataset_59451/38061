{{Infobox baseball biography
| name       = Ed Blake
| image      =
| position   = [[Pitcher]]
| bats       = Right
| throws     = Right
| birth_date  = {{Birth date|mf=yes|1925|12|23}}
| birth_place = [[St. Louis, Missouri]]
| death_date  = {{death date and age|mf=yes|2009|4|15|1925|12|23}}
| death_place = [[Swansea, Illinois]]
|debutleague = MLB
| debutdate  = May 1
| debutyear  = {{Baseball year|1951}}
| debutteam  = Cincinnati Reds
|finalleague = MLB
| finaldate  = April 28
| finalyear  = {{Baseball year|1957}}
| finalteam  = Kansas City Athletics
| stat3label = [[Strikeout]]s
| stat3value = 1
|statleague = MLB
| stat1label = [[Win–loss record (pitching)|Win–loss record]]
| stat1value = 0–0
| stat2label = [[Earned run average]]
| stat2value = 8.31
| teams      =
* [[Cincinnati Reds]] ({{Baseball year|1951}}–{{Baseball year|1953}})
* [[Kansas City Athletics]] ({{Baseball year|1957}})
}}
'''Edward James Blake''' (December 23, 1925 – April 15, 2009) was an American right-handed [[pitcher]] in [[Major League Baseball]] who played four seasons with the [[Cincinnati Reds]] and the [[Kansas City Athletics]]. In eight career games, Blake pitched 8⅔ [[innings pitched|innings]] and had an 8.31 [[earned run average]] (ERA).

After graduating high school in East St. Louis, Blake played in the [[farm system]] of the nearby [[St. Louis Cardinals]] before enlisting in the army. He returned to baseball after being wounded in the military service, pitching in the minors for five years before making his major league debut for the Cincinnati Reds. He pitched for them on and off for three years, then spent the next six years pitching for the [[Toronto Maple Leafs (International League)|Toronto Maple Leafs]]. His last major league appearance was a stint with the Athletics in 1957, and two years later his professional baseball career ended. After retirement he became a [[plumber]], and died in 2009.

==Early life==
Born in [[St. Louis, Missouri]] to Edward and Katherine Blake, he attended [[Central Catholic High School (St. Louis, Missouri)|Central Catholic High School]] in East St. Louis, where he was a baseball teammate of [[Hank Bauer]], and graduated in 1943.<ref name=bndblake>{{cite news|url=http://www.thedeadballera.com/Obits/Obits_B/Blake.Ed.Obit.html|title=Former big league pitcher Ed Blake dies in Swansea|last=Sanders|first=Norm|page=3D|work=[[Blakesville News-Democrat]]|date=2009-04-17}}</ref> Blake pitched batting practice for the [[St. Louis Cardinals]] during the [[1943 World Series]] after his high school graduation, in what he considered to be the highlight of his professional career.<ref name=bndblake/> He signed with the Cardinals in 1944 and spent the season with three different minor league teams: the [[Columbus Red Birds]] of the [[American Association (20th century)|American Association]], the [[Allentown Cardinals]] of the [[Interstate League]] and the [[Mobile Bears]] of the [[Southern Association]].<ref name=biw>{{cite web|first=Gary|last=Bedingfield|url=http://baseballinwartime.co.uk/player_biographies/blake_ed.htm|title=Baseball in Wartime – Ed Blake|publisher=Baseball in Wartime|accessdate=2009-04-19}}</ref>

==Military service and 1940s career==
After the 1944 season ended, Blake enlisted in the [[United States Armed Forces|Army]] and fought with the 40th Infantry Division in the [[Asiatic-Pacific Theater|Pacific Theater of Operations]].<ref name=biw/> While in the [[Philippines]], Blake was wounded, and was out of action for nine months as he recovered.<ref name=biw/> As the 1946 season began, he rejoined the Cardinals, now fully recovered, and spent the season with the [[Columbus Cardinals]]. Blake started the season losing his first three starts, but then won 13 straight and finished the season with a 16 wins, eight losses, and a 3.51 earned run average.<ref name=biw/> In February 1947, Blake was among a group of 92 Cardinals players invited to a dinner for Cardinals minor leaguers, where accusations in regards to Cardinals players being paid poorly were brought up and discussed.<ref>{{cite news|first=Edgar G.|last=Brands|title=Breadon Dines 92 Redbirds at Cardinal Pep Party|work=[[The Sporting News]]|page=13|date=February 26, 1947}}</ref> He spent the following season playing for both Columbus teams, playing 29 games in total and finishing the season with a combined 4.46 ERA.<ref name=brm>{{cite web|url=http://www.baseball-reference.com/minors/player.cgi?id=blake-001edw|title=Ed Blake Minor League Statistics|work=Baseball-Reference.com|accessdate=2009-04-19}}</ref> Before the 1948 season began, Blake was promoted to the [[Rochester Red Wings]] of the [[International League]]. He had a 7–6 record and a 3.88 ERA in 34 games, seven of them starts.<ref name=brm/> In 1949, Blake spent most of the season playing for Rochester. On August 9, 1949 he was traded by the Cardinals to the [[Cincinnati Reds]] in exchange for [[Mike Schultz (1940s pitcher)|Mike Schultz]], ending his career with the Cardinals.<ref name=retrosheet>{{cite web|url=http://www.retrosheet.org/boxesetc/B/Pblake101.htm|title=Ed Blake's Career Statistics|publisher=Retrosheet, Inc|accessdate=2009-04-17}}</ref> Blake finished the season with the [[Syracuse Chiefs]] and finished the season with a combined 5–4 record and a 4.78 ERA.<ref name=brm/>

==Minor leagues and Reds career==
Blake began the 1950 season with Syracuse Chiefs and spent most of the season as a [[starting pitcher]] instead of a reliever. He started in 23 games and finished the season with a 12–8 record and an ERA of 3.51.<ref name=brm/> In 1951, Blake was considered to have a small chance to make the major league roster.<ref>{{cite news|first=Tom|last=Swope|title=Few of Sewell's Kids Pushing Reds' Vets|work=The Sporting News|page=21|date=April 4, 1951}}</ref> Nonetheless, Blake got his first taste of the major leagues that season. He made his major league debut on May 1, 1951 against the [[Philadelphia Phillies]].<ref name=retrosheet/> Blake played three games during his time on the Reds in 1951, and finished two of those games. He pitched four innings, allowing five runs and three [[home run]]s, finishing the season with an ERA of 11.25.<ref name=br>{{cite web|url=http://www.baseball-reference.com/players/b/blakeed01.shtml|title=Ed Blake Statistics|work=Baseball-Reference.com|accessdate=2009-04-22}}</ref> While on the Reds in 1951, he was part of a Reds "bullpen union" led by [[Jim Blackburn]], which drew up a series of humorous requests including a smoking lounge and sandwiches between double headers.<ref>{{cite news|first=Tom|last=Swope|title=Cincy 'Bullpen Union' Drafts By-Laws|work=The Sporting News|page=11|date=April 18, 1951}}</ref> He spent most of the 1951 season with the Columbus Red Birds, playing in 27 games for them. In 27 pitching appearances, 23 of them starts, Blake went 7–15 with a 5.91 ERA.<ref name=brm/> Despite the record, Blake led the Red Birds in [[innings pitched]] and finished tied for second in wins, though he did also lead the team, which went 53–101, in losses.<ref>{{cite web|url=http://www.baseball-reference.com/minors/team.cgi?id=15767|title=1951 Columbus Red Birds|work=Baseball-Reference.com|accessdate=2009-04-22}}</ref>

Blake spent most of 1952 with the [[Milwaukee Brewers (AA)|Milwaukee Brewers of the American Association]], but he also pitched a few games for the Reds during the [[1952 Cincinnati Reds season]]. He pitched in two games for the Reds in 1952, coming in to finish both games for the Reds. He pitched three [[shutout]] innings, allowing three hits in these two games.<ref name=br/> During his time with the Brewers, he pitched in 21 games. He started 19 of the 21 games, won ten and lost three, and had a 3.96 ERA.<ref name=brm/> As the 1953 season began, Blake again had a stint with Cincinnati, but spent most of the season with the [[Indianapolis Indians]]. He pitched in one game for the Reds on April 17, 1953 against the [[Milwaukee Brewers]], allowing two earned runs on a hit and a [[base on balls|walk]] without getting a batter out.<ref name=retrosheet/> During his time with the Indianapolis Indians, he pitched in 29 games, and was the workhorse of the Indians' squad. He finished the season with a 14–7 record, a 3.76 ERA, and 208 innings pitched,<ref name=brm/> and led the team in wins, innings pitched, and starting appearances.<ref>{{cite web|url=http://www.baseball-reference.com/minors/team.cgi?id=20827|title=1953 Indiandpolis Indians|work=Baseball-Reference.com|accessdate=2009-04-22}}</ref>

==Maple Leafs and Athletics career==
The 1953 season marked Blake's last appearance with the Reds. On February 1, 1954, Blake was purchased from the Reds by the [[Toronto Maple Leafs (International League)|Toronto Maple Leafs]], an unaffiliated team of the American Association.<ref name=br/> He played for the Maple Leafs for a total of six seasons, where he had some of his best professional career seasons. In 1954, he teamed with [[Connie Johnson]] to lead the Maple Leafs to a 97-57 record. He finished the 1954 season with a 15–9 record, a 3.92 ERA, 30 games started, and 209 innings pitched, leading the team in the latter two categories.<ref name=brm/> The following season, he combined with Johnson and [[Jack Crimian]] to help Toronto win 94 games. On July 10, Blake was the first pitcher in the [[International League]] to win 13 games when he beat the Columbus squad 7–4.<ref>{{cite news|title=International League – Toronto|work=The Sporting News|page=38|date=July 20, 1955}}</ref> He finished the season with 17 wins, 13 losses, and a 3.94 ERA.<ref name=brm/> In 1956, Blake was a member of the "big three" for the Maple Leafs, along with [[Don Johnson (pitcher)|Don Johnson]] and International League Most Valuable Player [[Lynn Lovenguth]]. The three combined for 750 innings pitched and 57 [[complete game]]s while often working on two days' rest.<ref name=toronto>{{cite news|first=Neil|last=MacCarl|title=Homer Barrage, Big Three on Hill Won for Toronto|work=The Sporting News|page=39|date=September 19, 1956}}</ref> He finished the season with 17 wins, 11 losses, and a 2.61 ERA.<ref name=brm/> He also led the team in shutouts with six.<ref name=toronto/>

Blake was drafted at the end of the 1956 season by the [[Kansas City Athletics]] in the [[rule 5 draft]]. The day after being drafted, the Athletics planned to include him in an eight-player deal with the [[Detroit Tigers]] involving [[Virgil Trucks]] and others, but his inclusion in the trade was vetoed by Commissioner [[Ford Frick]], claiming his inclusion was "against the spirit of the draft."<ref>{{cite news|first=Carl|last=Lundquist|title=Frick Stops Blake's Shuffle to Bengals After Draft by A's|work=The Sporting News|page=9|date=December 12, 1956}}</ref> Blake was replaced in the trade by former Toronto teammate Jack Crimian. While he spent most of the 1957 season with Toronto, he had one final major league stint with the Athletics, pitching in two games in April.<ref name=br/> His 1957 stint in Toronto was not as successful as previous seasons, as he finished with eight wins, nine losses, and a 5.54 ERA.<ref name=brm/> He spent five weeks of the 1958 season unable to play due to injury.<ref>{{cite news|first=Cy|last=Kritzer|title=Flag Contenders Struck by Injuries|work=The Sporting News|page=33|date=July 16, 1958}}</ref> Despite this, he was still able to pitch in 23 games, winning nine and bringing his ERA down to 3.54.<ref name=br/> In 1959, Blake split time between Toronto and the [[Houston Buffs]], winning three games and losing ten in his final professional season of baseball.<ref name=brm/>

==Personal and later life==
After his playing career ended, Blake went on to become a plumber. He spent nearly 50 years in the plumbing business and was formerly the president of Plumbers Local 360.<ref name=bndblake/> Blake was married to his wife, Carol Jean, for 47 years before her death. With Carol, he had a son, Ed, and a daughter, Peggy.<ref name=bndblake/> His son, also named Ed Blake, was a former pitcher who played in the [[Baltimore Orioles]]' farm system from 1970 to 1973, playing for the AA affiliate [[Asheville Orioles]] at the peak of his career.<ref>{{cite web|url=http://www.baseball-reference.com/minors/player.cgi?id=blake-002edw|title=Edward Blake (Jr.) Minor League Statistics|work=Baseball-Reference.com|accessdate=2009-04-19}}</ref> His grandson, Ed Blake III, has continued in the footsteps of his father and grandfather as a pitcher in high school. Blake died in [[Swansea, Illinois]] at the age of 83 after a long illness, and is interred at Mount Carmel Cemetery in [[Belleville, Illinois]].<ref name=bndblake/><ref name=retrosheet/>

==References==
{{reflist}}

==External links==
{{Baseballstats|br=b/blakeed01|fangraphs=1001035|cube=ed-blake|brm=blake-001edw}}

{{Good article}}

{{DEFAULTSORT:Blake, Ed}}
[[Category:1925 births]]
[[Category:2009 deaths]]
[[Category:Cincinnati Reds players]]
[[Category:Disease-related deaths in Illinois]]
[[Category:Kansas City Athletics players]]
[[Category:Syracuse Chiefs players]]
[[Category:Indianapolis Indians players]]
[[Category:Toronto Maple Leafs (International League) players]]
[[Category:Major League Baseball pitchers]]
[[Category:Baseball players from Missouri]]
[[Category:Sportspeople from St. Louis]]