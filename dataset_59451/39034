{{DISPLAYTITLE:Business School (''The Office'')}}
{{Infobox Television episode
| title        =Business School
| series       =[[The Office (U.S. TV series)|The Office]]
| image        =
| caption      =
| season       =3
| episode      =17 <!-- THIS IS THE 17TH EPISODE OF THE SEASON. HOUR-LONG EPISODES ARE CONSIDERED TWO EPISODES. PLEASE DO NOT CHANGE EPISODE NUMBERS IN THIS ARTICLE. -->
| airdate      =February 15, 2007
| production   =317
| writer       =[[Brent Forrester]]
| director     =[[Joss Whedon]]
| guests       =*[[Creed Bratton]] as [[Creed Bratton (character)|Creed Bratton]]
*[[Rashida Jones]] as [[Karen Filippelli]]
*Michael Patrick McGill as Kenny Anderson
| episode_list =[[List of The Office (U.S. TV series) episodes|List of ''The Office'' (U.S.) episodes]]
| season_list  =
| prev         =[[Phyllis' Wedding]]
| next         =[[Cocktails (The Office)|Cocktails]]
}}

"'''Business School'''" is the seventeenth episode of the third season of the American [[comedy]] [[television program|television series]] ''[[The Office (U.S. TV series)|The Office]]'', and the show's forty-fifth episode overall. Written by [[Brent Forrester]], and directed by ''[[Buffy the Vampire Slayer (TV series)|Buffy the Vampire Slayer]]'' and ''[[Firefly (TV series)|Firefly]]'' creator [[Joss Whedon]], the episode aired on [[NBC]] on February 15, 2007.<ref name="updated">{{cite press release|url=http://nbcumv.com/release_detail.nbc/entertainment-20070126000000-nbcprimetimeschedu.html|publisher=NBC|date=2007-01-26|title=NBC Primetime Schedule for Sunday, Feb 11 2007 {{ndash}} Saturday, Feb 17 2007}}</ref>

In the episode, Michael is invited by Ryan to speak to his [[business school]] class.  When many of the students question the usefulness of paper in a computerized world, Michael attempts to inform the class of how essential paper is.  Meanwhile, a [[bat]] becomes trapped in the office, leading Dwight on a mission to protect the employees.

==Plot==
[[Ryan Howard (The Office)|Ryan Howard]] ([[B. J. Novak]]) invites [[Michael Scott (The Office)|Michael Scott]] ([[Steve Carell]]) to speak at his business school class. Michael is excited, but Ryan admits in a talking head interview that he has only invited Michael because his professor promised to bump up the grade of any student who brings his boss into class. During his introduction of Michael, Ryan predicts that [[Dunder Mifflin]] will become obsolete within five to ten years. However, Michael could not hear him and proceeds to ruin the event with his antics (including tearing pages out of a student's textbook to prove you "can't learn from textbooks"). One of Ryan's classmates asks for Michael's opinion of Ryan's prediction. Infuriated and hurt, Michael punishes Ryan by relocating his desk to the "annex," where [[Kelly Kapoor]] ([[Mindy Kaling]]) works. Kelly babbles uncontrollably in excitement at the news. 

Meanwhile, [[Dwight Schrute]] ([[Rainn Wilson]]) discovers a bat in the ceiling. It flies through the office, sending the employees scurrying for cover. [[Jim Halpert]] ([[John Krasinski]]) and [[Karen Filippelli]] exploit Dwight's paranoia, and pretend that Jim was bitten by the bat and is turning into a [[vampire]]. Jim calls animal control, but Dwight insists on catching the bat himself even after 5:00 passes and the other employees go home, save Meredith who continues to hide in the breakroom for fear of the bat. Dwight eventually catches the bat with a garbage bag after it lands on Meredith's head.

[[Pam Beesly]] ([[Jenna Fischer]]), in the meantime, invites her co-workers to her art show after work. At the show, [[Oscar Martinez (The Office)|Oscar Martinez]] ([[Oscar Nunez]]) and his partner Gil critique her art, with Gil dismissing it as "motel art", not realizing that she is standing right behind them. None of her other co-workers show, leaving her feeling alienated and sad. Roy comes by and compliments her work, but obviously does so as a boyfriend's gesture with no sincere appreciation for art. The general attendees show little interest in her drawings, and she begins to doubt her abilities. As she begins to take her work down, Michael arrives, having been delayed by the trip back to the office to move Ryan's things. He expresses sincere awe for her work and asks to buy her drawing of the office building. Pam embraces him as her eyes tear up. Michael hangs Pam's drawing on the wall next to his office.

== Production ==
[[Image:Joss Whedon premiere.jpg|right|thumb|150px|Director [[Joss Whedon]]]]
"Business School" was the second ''Office'' episode written by Brent Forrester.  Forrester had previously written "[[The Merger (The Office)|The Merger]]".  The episode was the first to be directed by [[Joss Whedon]].  Whedon, who is a friend of both producer [[Greg Daniels]] and Jenna Fischer, and also met most of the production staff prior to the episode, stated that he chose to direct the episode "because I already know the writing staff and a bunch of the cast, and I adore the show."<ref name="whedon">[http://www.avclub.com/content/interview/joss_whedon/3 Interview with Joss Whedon] {{webarchive |url=https://web.archive.org/web/20071011140313/http://www.avclub.com/content/interview/joss_whedon/3 |date=October 11, 2007 }} [[The A.V. Club]], retrieved June 24, 2008</ref>  When informed that the episode was about a bat entering into the office and one of the characters pretending to be a vampire, Whedon thought that it was a joke, stating "Didn't I just leave this party?" in reference to ''[[Buffy the Vampire Slayer (TV series)|Buffy the Vampire Slayer]]''.  In an interview featured on the [[The Office (U.S. season 3)|third season]] DVD, Whedon joked that the "Business School" episode and his former TV show were very similar because "''Buffy'' [...] was sad and depressing but... it was funny. Especially when people died. And a lot of people do die in ['Business School']."<ref>{{cite interview |last= Whedon|first= Joss |title= Joss Whedon Interview|type= DVD}}</ref> But upon completing the episode, Whedon stated "That was just coincidence. But that's how that happened. God, it was fun."<ref name="whedon"/>  Whedon stated that he was surprised with the amount of input he was allowed with the script.  "I wouldn't say freedom to do things with it...But way more input was asked for than I would have ever anticipated."  At Pam's art show, the pieces which she was supposed to have painted didn't suit Whedon.  "I got to the set and saw Pam's art, and I was like, 'This is not right.'"  Whedon said that he held up filming for over an hour until the staff was able create new pieces of art.<ref name="whedon"/>

For the scenes which involved filming with a bat, the production team used an actual bat, an animated bat, and a mechanical bat.  When around the actual bat, [[Kate Flannery]], who portrays [[Meredith Palmer]], stated that "we had to be extremely quiet around [it], basically pretending to scream."<ref>[http://www.officetally.com/more-on-kate-and-the-bat More on Kate and the bat] ''OfficeTally'', retrieved June 24, 2008</ref>  [[California State University, Northridge]] served as the backdrop for Ryan's business school and the art show.<ref>[http://www.csun.edu/licensing/filming.php CSUN Licensing – Facilities Use] [[California State University Northridge]], retrieved June 24, 2008</ref>

==Reception==
The episode received generally good reviews from critics.  Brian Zoromski, of [[IGN]], stated that "'Business School' was an exercise in what works best in an ''Office'' episode."  Zoromski also praised Joss Whedon's directorial debut for the show, stating that "Whedon's direction and sense of humor was both excellently put to use and alluded to in the scenes in which Jim hilariously pretended to become a vampire."  Zoromski went on to say that the acting of John Krasinski and [[Rashida Jones]], who portrays [[Karen Filippelli]], helped to make the vampire scenes the funniest parts of the episode. He gave the episode a 9.1 out of 10.<ref>[http://tv.ign.com/articles/765/765385p1.html ''The Office'': "Business School" Review] ''[[IGN]]'', retrieved June 24, 2008</ref>  Abby West, of ''[[Entertainment Weekly]]'', stated that "This show has always been able to turn on a dime and take the comedy to a soul-stirring dramatic climax with just the lightest of touches, and last night was no different."  West went on to praise Michael and Pam's scene at the art show as one of these moments.<ref>[http://www.ew.com/ew/article/0,,20012160,00.html Gone Batty] ''[[Entertainment Weekly]]'', retrieved June 24, 2008</ref>

==References==
{{Reflist|2}}

==External links==
*[https://web.archive.org/web/20121113211753/http://www.nbc.com/the-office/episode-guide/season-3/59065/business-school/episode-317/59340/ "Business School"] at NBC.com
*{{IMDb episode|0964922|Business School}}
*{{Tv.com episode|964761}}

{{TheofficeusEpisodes}}

[[Category:2007 television episodes]]
[[Category:The Office (U.S. season 3) episodes]]
{{good article}}