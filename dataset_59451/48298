{{Infobox scientist|name = Guang R. Gao|native_name = 高光荣|awards = [[List of Fellows of the Association for Computing Machinery|ACM Fellow]], IEEE Fellow|fields = Computer Architecture|known_for = Dataflow|Institutions = University of Delaware|doctoral_advisor = Jack Dennis}}'''Guang R. Gao''' is a computer scientist and a Professor of Electrical and Computer Engineering at the [[University of Delaware]].  Gao is a founder and Chief Scientist of ETI (ET International Inc., Newark, Delaware).

== Education and Early Life ==

 Born and brought up in a prominent Physician family in China, Gao has received a strict education from early childhood both in traditional knowledge of Chinese history and culture, as well as in western science and English.  Gao has shown his strong interests and curiosity in science subjects, and received his education in the elite Tsinghua University in Beijing.   In Jan. 1980,  Gao has left China and pursued his graduate education in the United States.  He received his Master and Ph.D degree in 1982 and 1986 respectively both in Computer Science at [[Massachusetts Institute of Technology]](MIT).  Gao is the first person from mainland China received a Computer Science Ph.D degree from MIT.<ref name="gao-springer">{{cite web|url =http://www.springerimages.com/Images/ComputerScience/1-10.1007_s00450-011-0165-1-1|title=SpringerImages - Guang R. Gao|publisher=Springer|accessdate=January 7, 2013 }}</ref>

== Career and Legacy ==
Gao has begun his research career in the west as a junior faculty member at McGill University in 1987—at a time that the computer science community has a wide spread doubt on the future of parallel computing  in general and dataflow model of parallel computation in particular.<ref name="gao-mcgill">{{cite web|url =http://www.dbi.udel.edu/People/gao.html|title=Delaware Biotechnology Institute|publisher=Delaware Biotechnology Institute|accessdate=January 7, 2013 }}</ref>

Gao has devoted a majority of his research and academic careers in carrying on the legacy of the MIT dataflow model research that he has participated and contributed during his Ph.D  project under  Prof. Jack. B. Dennis and Arvind.  The legacy of Gao’ own research is to show that the fundamental value of the dataflow model  of computation can be effectively explored and efficiently realized – and the superiority dataflow can be demonstrated even in parallel computer systems that are made of classical microprocessors with von-Neumann architectures and other components.  To this end, Gao has led a series of parallel architecture and system projects where various aspects of datalow models are improved and integrated in the design and implementation – ranging from innovations in programming paradigms, architecture features, system software technology,  including novel program optimization and runtime system techniques.  Gao’s contribution is recognized by receiving the ACM Fellow and IEEE Fellow in 2007.

Gao' research focused in dataflow models, parallel computing, computer system architecture, program analysis and optimization techniques.<ref name="gao-springer" /><ref name="gao-dhsa">{{cite web|url =http://www.delawarehsa.org/profiles/UD/gao.html|title=Delaware Health Sciences Alliance (DHSA) - Profiles|publisher=Delaware Health Sciences Alliance|accessdate=January 7, 2013 }}</ref><ref name="gao-udaily">{{cite web|url =http://www.udel.edu/udaily/2011/feb/gao-supercomputing-022111.html|title=UD's Gao makes historic contributions to extreme scale computing|publisher=University of Delaware|accessdate=January 7, 2013 }}</ref>

·        Dataflow models for computation and refinement and exptension

·        Multithreaded programming/execution models inspired by dataflow

·        Computer system architecture

·        Compiler optimization models and technqies and inspired by dataflow

·        Software pipelining

·        Program analysis techniques

The legacy of Gao’s work  has also been associated with his entrepreneur effort to initiate the technology transfer and commercialization  of the dataflow R&D results for real world applications through ETI (ET International Inc.). ETI is a company started in 2000 as a University of Delaware spinoff  with Gao as its co-founder.<ref name="gao-eti">{{cite web|url =http://www.etinternational.com/|title=ETI - Solutions for Technical and Business Computing|publisher=ETI International, Inc.|accessdate=January 7, 2013 }}</ref>  A unique achievement of Gao’s team at ETI is its critical role in the now legendary supercomputing system project known as IBM Cyclops-64 Supercomputer. The success of the TNT on Cyclops64 supercomputer is recognized by the selection ETI as winner of  a Supercomputing ''disruptive technology award'' in 2007.ET International, Inc was highlighted for "Sparking Economic Growth" by The Science Coalition in 2013.<ref>{{Cite web|title = The Science Coalition|url = http://www.sciencecoalition.org/successstories-list|website = www.sciencecoalition.org|accessdate = 2016-01-11}}</ref>

Through 30+ years persistent R&D and entrepreneur effort – Gao and his students have propelled the impact of MIT dataflow model of computation beyond their laboratory in  the US to other parts of the world including EU and Asia.

== Awards and Recognitions ==

* Gauss Award: Awarded during the International Supercomputing Conference 2011 held in Hamburg, Germany for the paper "Experiments with the Fresh Breeze Tree-Based Memory Model", co-authored with [[Jack Dennis]] from MIT and Xiaoxuan Meng from the [[University of Delaware]].<ref>{{cite web|url =http://www.isc-events.com/isc11/Program/At-a-Glance/Awards|title=Awards - International Supercomputing Conference|publisher=Prometeus GmbH |accessdate=January 7, 2013 }}</ref><ref>{{cite web|url =http://www.udel.edu/udaily/2012/aug/gao-gauss-award-080411.html|title=Engineering professor Guang R. Gao wins international recognition|publisher=University of Delaware |accessdate=January 7, 2013 }}</ref>
* [[IEEE]] Fellow, 2008: Awarded for his contributions in Parallel Computer Architectures and Compilers.<ref>{{cite web|url =http://www.ieee.org/membership_services/membership/fellows/alphabetical/gfellows.html|title=IEEE - Fellows|publisher=[[IEEE]]|accessdate=January 7, 2013 }}</ref>
* ACM Fellow, 2007: Awarded for his contributions in Multiprocessor Computers and Compiler Optimization Techniques.<ref>{{cite web|url =http://fellows.acm.org/fellow_citation.cfm?id=1954601&srt=all|title=ACM: Fellows Awards/Guang R. Gao|publisher=ACM|accessdate=January 7, 2013 }}</ref>
* China Computer Federation (CCF) Overseas Outstanding Achievement Award

== References ==

{{reflist}}

== External links ==

* [http://www.capsl.udel.edu/~ggao/ Guang R. Gao Home Page]
* [http://www.capsl.udel.edu/ CAPSL Home Page]
* [http://www.udel.edu/udaily/2011/may/carney-etinternational-050611.html Congressman Carney tours UD spin-off focused on supercomputing]

{{DEFAULTSORT:Gao, Guang R.}}
[[Category:American computer scientists]]
[[Category:American electrical engineers]]
[[Category:Fellows of the Association for Computing Machinery]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Massachusetts Institute of Technology alumni]]