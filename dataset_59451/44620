{{Italic title}}
{{History of Georgia (country)}}
The '''[[Divan]] of the [[Kingdom of Abkhazia|Abkhazian Kings]]''' ({{lang-ka|აფხაზთა მეფეთა დივანი, ''Ap’xazta mep’eta divani''}}, which is often translated as the '''''Chronicles of the Abkhazian Kings''''') is a short medieval document composed in Georgian in the late 10th or early 11th century. It has come down to us as a 15th-century copy. The text was first studied and published by the [[Georgia (country)|Georgian]] scholar [[Ekvtime Takaishvili]]. It has also been translated into [[English language|English]] and [[Russian language|Russian]].

It is usually attributed to the first king of all-Georgia, [[Bagrat III of Georgia|Bagrat III]], who began his reign as the Abkhazian king in 978. Somewhat of a manifesto-style, this document may have been issued by Bagrat, a representative of the new dynasty of the [[Bagrationi]], in support of his rights to the Abkhazian throne.

The ''Divan'' lists 22 successive rulers from Anos to Bagrat, and styles each of them as “king” (Georgian: ''mepe'') (though until the mid-780s they functioned as the [[archon]]s under the [[Byzantine empire|Byzantine]] authority). The text does provide the information about the family relationships among these rulers as well as the duration of the last 11 kings’ reigns, but lacks chronology. The two kings of the [[Shavliani]] clan (878–887) are omitted probably because they were regarded as usurpers. The dates and achievements of the most of the early Abkhazian rulers remain conjectural.

The names below are given in original transliteration. The dates are as per Prince [[Cyril Toumanoff]] and other modern scholars. 
*Anos (Georgian: ანოს) (c. 510–530)
*Ghozar (ღოზარ) (c. 530–550)
*Istvine (ისტვინე) (c. 550–580)
*Phiniktios (ფინიქტიოს) (c. 580–610)
*Barnuk (ბარნუკ) (c. 610–640)
*Dimitri I (დიმიტრი) (c. 640–660)
*Theodos I (თეოდოს) (c. 660–680)
*Konstanti I (კონსტანტი) (c. 680–710)
*Theodor (თეოდორ) (c. 710–730)
*Konstanti II (კონსტანტი) (c. 730–745)
*[[Leon I of Abkhazia|Leon I]] (ლეონ) (c. 745–767)
*[[Leon II of Abkhazia|Leon II]] (ლეონ) (c. 767–811)
*[[Theodosius II of Abkhazia|Theodos II]] (თეოდოს) (c. 811–837)
*[[Demetrius II of Abkhazia|Dimitri II]] (დიმიტრი) (c. 837–872)
*[[George I of Abkhazia|Giorgi I Aghts’epeli]] (გიორგი აღწეფელი) (c. 872–878)
*[[Bagrat I of Abkhazia|Bagrat I]] (ბაგრატ) (c. 887–898)
*[[Constantine III of Abkhazia|Konstanti III]] (კონსტანტი) (c. 898–916)
*[[George II of Abkhazia|Giorgi II]] (გიორგი) (c. 916–960)
*[[Leon III of Abkhazia|Leon III]] (ლეონ) (c. 960–969)
*[[Demetrius III of Abkhazia|Dimitri III]] (დიმიტრი) (c. 969–976)
*[[Theodosius III of Abkhazia|Theodos III Brma]] (თეოდოს ბრმა) (c. 976–978)
*[[Bagrat III of Georgia|Bagrat III]] (ბაგრატ) (978–1014)

==References==
*S. H. Rapp, ''Studies In Medieval Georgian Historiography: Early Texts And Eurasian Contexts'', Peeters Bvba (September 25, 2003) ISBN 90-429-1318-5 pages 144, 230-237, 481-484

[[Category:Georgian chronicles]]
[[Category:Kings of Abkhazia]]
[[Category:10th-century history books]]
[[Category:King lists]]