{{Infobox journal
| title = European Journal of Clinical Nutrition
| cover = 
| former_names = Journal of Human Nutrition, Human Nutrition, Nutrition
| abbreviation = Eur. J. Clin. Nutr.
| discipline = [[Nutrition science]]
| editor = Manfred J. Müller
| publisher = [[Nature Publishing Group]]
| country = 
| history = 1947-present
| frequency = Monthly
| openaccess = 
| license = 
| impact = 2.709
| impact-year = 2014
| ISSN = 0954-3007
| eISSN = 1476-5640
| CODEN = EJCNEQ
| JSTOR = 
| LCCN = 78641817
| x= sn88026529
| OCLC = 39737748
| website = http://www.nature.com/ejcn/index.html
| link1 = http://www.nature.com/ejcn/journal/v68/n11/index.html
| link1-name = Online access
| link2 = http://www.nature.com/ejcn/archive/index.html
| link2-name = Online archive
}}
The '''''European Journal of Clinical Nutrition''''' is a monthly [[peer-reviewed]] [[medical journal]] covering [[nutrition science]] and published by the [[Nature Publishing Group]]. It was established in 1947 by [[John Waterlow]] as '''''Nutrition''''' and renamed '''''Journal of Human Nutrition''''' in 1976.<ref name=LC>{{cite web |url=http://lccn.loc.gov/78641817 |title=Journal of Human Nutrition |work=Library of Congress Catalog |publisher=[[Library of Congress]] |accessdate=2014-12-03}}</ref> In 1982 its name was changed to '''''Human Nutrition''''' and the journal was split into two sections: '''''Human Nutrition: Applied Nutrition''''' and '''''Human Nutrition: Clinical Nutrition'''''. These two sections were combined again in 1988 with the journal obtaining its current name.<ref>{{cite journal |last1=Shetty |first1=P. |title=A progress report and a tribute to our founding editor |journal=European Journal of Clinical Nutrition |date=January 2011 |volume=65 |issue=1 |pages=1–2 |doi=10.1038/ejcn.2010.271}}</ref> The [[editor-in-chief]] is Manfred J. Muller ([[University of Kiel]]).

==Abstracting and indexing==
The journal is abstracted and indexed in:<ref>{{cite web |url=http://www.nature.com/ejcn/about.html |title=About |publisher=[[Nature Publishing Group]] |work=European Journal of Clinical Nutrition Website |accessdate=2 December 2014}}</ref>
{{columns-list|colwidth=30em|
* [[Elsevier Biobase]]/Current Awareness in Biological Sciences
* [[BIOSIS]]
* [[CAB Abstracts]]
* [[CINAHL]]
* [[Current Contents]]/Clinical Medicine
* Current Contents/Life Sciences
* [[EMBASE]]/[[Excerpta Medica Database]]
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]
* [[Science Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal had a 2013 [[impact factor]] of 2.95, ranking it 28th out of 78 journals in the category "Nutrition & Dietetics".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Nutrition & Dietetics |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official website|http://www.nature.com/ejcn/index.html}}

[[Category:Nutrition and dietetics journals]]
[[Category:Nature Publishing Group academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1947]]