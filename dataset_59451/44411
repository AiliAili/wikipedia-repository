{{Use mdy dates|date=January 2014}}
{{Infobox airport
| name         = Worcester Regional Airport
| image        = Worcesterairport logo.jpg
| image2       = Worcester Airport Aerial.jpg
| caption2     = Aerial view 
| IATA         = ORH
| ICAO         = KORH
| FAA          = ORH
| type         = Public
| owner        = [[Massachusetts Port Authority]] (Massport)
| operator     = Massachusetts Port Authority (Massport)
| city-served  = Worcester, Massachusetts
| location     = [[Worcester, Massachusetts]]
| elevation-f  = 1,009
| elevation-m  = 308
| coordinates  = {{coord|42|16|02|N|071|52|33|W|region:US-MA_type:airport|display=inline}}
| website      = [http://www.massport.com/worcester-airport/ www.massport.com/worcester-airport]
| image_map              = KORH Airport Diagram.svg
| image_mapsize          = 180
| image_map_alt          = A map with a grid overlay showing the terminals runways and other structures of the airport.
| image_map_caption      = FAA airport diagram
| pushpin_map            = USA Massachusetts#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Massachusetts / United States
| pushpin_label          = '''ORH'''
| pushpin_label_position = left
| r1-number    = 11/29
| r1-length-f  = 7,000
| r1-length-m  = 2,134
| r1-surface   = Asphalt/grooved
| r2-number    = 15/33
| r2-length-f  = 5,000
| r2-length-m  = 1,524
| r2-surface   = Asphalt/grooved
| stat-year    = 2015
| stat1-header = Total passengers
| stat1-data   = 117,604
| stat2-header = Aircraft Operations
| stat2-data   = 40,207
| stat3-header = Based Aircraft
| stat3-data   = 74
| footnotes    = Source: [[Federal Aviation Administration]]<ref name=FAA>{{FAA-airport|ID=ORH|use=PU|own=PU|site=09462.*A}}. Retrieved February 21, 2016.</ref>
}}

'''Worcester Regional Airport''' {{Airport codes|ORH|KORH|ORH}} is a public airport located three miles (5&nbsp;km) west of the central business district of [[Worcester, Massachusetts|Worcester]], a city in [[Worcester County, Massachusetts|Worcester County]], Massachusetts, United States. The main airport property lies within Worcester and [[Leicester, Massachusetts|Leicester]], with substantial supporting facilities in [[Paxton, Massachusetts|Paxton]]. Once owned by the City of Worcester, the airport has been owned and operated by the [[Massachusetts Port Authority]] (Massport) since June 2010.<ref>{{cite web |url=http://transportation.blog.state.ma.us/blog/2010/06/massport-worcester-airport-deal-completed.html |title=Massport, Worcester Airport Deal Completed |author=[[Massport]] |coauthors= |date=June 22, 2010 |publisher=[[Massachusetts Department of Transportation]] (MASSDOT) |accessdate=June 26, 2010 |quote= |ref= |separator= |postscript= }}</ref>

==History==
[[File:Downtown Worcester, Massachusetts.jpg|thumb|left|250px|Downtown [[Worcester, Massachusetts|Worcester]], with Worcester Regional Airport tower in the background]][[File:Worcester Airport.JPG|thumb|left|250px|Worcester Regional Airport]]Worcester's entry into the world of aviation began in 1925, when city officials commissioned a study to examine suitable sites for the city's first airport. On the list of probable sites was the land owned by a wealthy local citizen, Whitin Whitall. In 1927, Whitall, independently of the city commission, set up an airport on his land in North [[Grafton, Massachusetts|Grafton]], {{convert|500|ft|m}} above sea level. [[Grafton Airport (Massachusetts)|This two-runway airport]] opened for leisure travel on October 12, 1927.<ref>{{cite book | author=Southwick, Albert B. | title=Once-Told Tales of Worcester County | location=Worcester | publisher=Databooks | year=1994}}</ref>

As air travel became more popular throughout the country and Central Massachusetts, the question of airport expansion became the subject of a second study commissioned by the Worcester city government. The Grafton airport was deemed too small to accommodate the air travel needs of the region. The location of the present airport, Tatnuck Hill, an area that straddles the borders of Worcester, Leicester, and Paxton, was high on the commission's list. One problem noted by the commission and several prominent citizens was the weather: at {{convert|1000|ft|m|-1|sing=on}} above sea level, the Tatnuck site was often surrounded by fog. Despite this problem, the city eventually chose Tatnuck as the new site, and construction began in 1944. The airport was ceremoniously opened on May 4, 1946, and started regular passenger service one week later on May 10, 1946. The Grafton airport remained in operation until 1951, when the owners, due to the dwindling traffic, decided to dismantle the airport. The land was redeveloped as a residential neighborhood.<ref>{{cite web | url=http://www.airfields-freeman.com/MA/Airfields_MA_W.htm#grafton | title=Abandoned & Little-Known Airfields: Grafton, MA | author=Freeman, Paul | work=[[Abandoned & Little-Known Airfields]]: Western Massachusetts | date=March 13, 2010 | accessdate=October 16, 2010}}</ref> [[Leicester Airport (Massachusetts)|Leicester Airport]], a small private airfield also built during the first half-century of aviation, was active until the 1970s. It still sits, now mostly overgrown in the shadow of Worcester Regional.<ref>{{cite web | url=http://www.airfields-freeman.com/MA/Airfields_MA_W.htm#leicester | title=Abandoned Airfields: Leicester, MA | author=Freeman, Paul | work=[[Abandoned & Little-Known Airfields]]: Western Massachusetts | date=March 13, 2010 | accessdate=October 16, 2010}}</ref>

Millions of dollars were spent replacing the old terminal, which hosted a half-dozen airlines before its demolition. In the mid 1980s and early 1990s, major carriers, such as [[Piedmont Airlines|Piedmont]], [[Northwest Airlines]], [[Continental Airlines|Continental]], and [[USAir]] all flew mainline jets into Worcester. In addition, smaller carriers, like [[New York Air]] and [[Presidential Airways (scheduled)|Presidential Airways]] also had jet service. The small terminal had two ground level jetways built to accommodate the growth. But one by one, those carriers left. A succession of second-tier air carriers have come and gone over the last decade.

[[Allegiant Air]] began service to [[Orlando Sanford International Airport|Orlando/Sanford, FL]] (SFB) on December 22, 2005, using [[McDonnell-Douglas MD-80]] type aircraft. The airline expanded to 4 flights per week in March 2006. Allegiant announced on August 22, 2006, that they would cut ties with the airport, citing high fuel costs and passenger loads in the 80% range as the reason for departure. The departure came as a huge surprise to the city as service was reported to be going great throughout Allegiant's entire tenure at the airport.<ref>{{cite news | url=http://nl.newsbank.com/nl-search/we/Archives?p_product=WO&p_theme=wo&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=113B70D44AB32A90&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM | title=Allegiant Air will leave Worcester | work=Worcester Telegram and Gazette | date=August 23, 2006 | accessdate=October 16, 2010}}</ref>

On September 4, 2008, [[Direct Air]] announced they would begin service to Worcester beginning in November 2008, with flights from [[Orlando Sanford International Airport|Orlando/Sanford, FL]] and [[Charlotte County Airport|Fort Myers/Punta Gorda, FL]]. The flights were initially operated by [[Virgin America]] using [[Airbus A320]] aircraft, however Direct Air was forced to return the aircraft in June 2009 to suffice Virgin's rapidly expanding domestic routes. Following this Direct Air began carrying out flights on [[Boeing 737-400|Boeing 737-400's]] owned by [[Xtra Airways]]. Due to this being a wet-lease agreement, there were times where the aircraft was unavailable and other aircraft had to be chartered for the flights. Such examples include an [[Airbus A320]] from [[USA 3000]] and a [[Boeing 757]] from [[North American Airlines]]. In March 2009, Direct Air added additional flights to [[Myrtle Beach International Airport|Myrtle Beach, SC]]. In July 2010, Direct Air expanded their Worcester service further to [[Palm Beach International Airport|West Palm Beach, FL]]. The airline had further plans to launch flights to [[Luis Muñoz Marín International Airport|San Juan, Puerto Rico]] and [[Lynden Pindling International Airport|Nassau, Bahamas]] but in March 2012 Direct Air suspended all operations and filed for Chapter 7 bankruptcy on April 12, 2012.<ref>{{cite news|url=http://www.boston.com/businessupdates/2013/04/03/massport-media-advisory-sparks-speculation-jetblue-service-for-worcester/4OFDwoZZ6HYX77M8mGqutO/story.html?s_campaign=8315 |title=Massport media advisory sparks speculation of JetBlue service for Worcester |newspaper=Boston Globe |date=April 3, 2013 |accessdate=January 11, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20131112125915/http://www.boston.com/businessupdates/2013/04/03/massport-media-advisory-sparks-speculation-jetblue-service-for-worcester/4OFDwoZZ6HYX77M8mGqutO/story.html?s_campaign=8315 |archivedate=November 12, 2013 }}</ref>

On April 3, 2013, it was announced that [[JetBlue Airways]] will offer daily flights to [[Orlando, Florida]] and [[Fort Lauderdale, Florida]], starting November 7, 2013.<ref>{{cite news|url=http://m.telegram.com/article/20111110/news/111109104 |title=Direct Air puts Bahamas, San Juan flights on hold |newspaper=Worcester Telegram |date=April 3, 2012 |accessdate=November 25, 2015 }} {{Dead link|date=April 2014|bot=RjwilmsiBot}}</ref> This came after over a year of negotiating with the airline that included a competition among local residents to help advertise the city. This became the first mainline service out of Worcester in over a decade. The airline currently uses the 100 seat [[Embraer 190]] for their flights, although occasionally they are substituted for the 165 seat [[Airbus A320]]. With the airport's current terminal facility JetBlue can operate two aircraft at a time with the pair of jetways and ticket counters.

A revamp of the defunct airline [[Air Florida]] had planned to fly out of Worcester at the beginning of 2015 as a scheduled charter operation but the airline never got off the ground.

Worcester Regional Airport was used for shooting of the films ''[[Captain Phillips (film)|Captain Phillips]]'', ''[[Knight and Day]]'', ''[[The Judge (2014 film)|The Judge]]'' and ''[[The Sea of Trees]]''.<ref>Sheehan, Nancy. "Airport taking off with movie industry." Telegram & Gazette, Mar 23 2012.</ref>

On November 17, 2015, [[Rectrix Aviation]] opened a brand new [[fixed-base operator|FBO]] building and hangar in Worcester. Rectrix also plans to launch flights between Worcester and [[Cape Cod]] possibly as soon as the summer of 2016 with their newly established commercial airline operation.<ref>http://capecodtimes.com/article/20151216/NEWS/151219601</ref> On October 14, 2016, Rectrix announced intentions to begin commercial service between Worcester and [[Baltimore-Washington International Airport|Baltimore]] within the next year. The flight is expected to be once daily each way, continuing to [[Sarasota, Florida]] after a quick stop at BWI.<ref>http://www.telegram.com/news/20161014/rectrix-aviation-to-offer-worcester-to-baltimore-flight</ref>

On February 28, 2017, JetBlue announced it will expand its service at the airport, adding a daily non-stop flight to [[New York City|New York, New York]]. While JetBlue did not provide a start date, airline officials said the new flights would not begin until after the completion of the cat III instrument landing system.<ref name="Vaccaro">{{cite news |last=Vaccaro |first=Adam |date=2017-02-28  |title=JetBlue to Offer New York-Worcester Route |url=https://www.bostonglobe.com/business/2017/02/28/worcester-big-apple-jetblue-offer-new-york-route/tKt1JOanevmJqmMP6cZr3N/story.html | work=The Boston Globe |location=Boston, Massachusetts |access-date=2017-02-28 }}</ref>

===Massport===
The airport had been under an operating agreement with [[Massport]], the Massachusetts Port Authority for several years. Under the agreement, the city and Massport paid the operating deficit together.

* '''July 1, 2004 – June 30, 2005''' – Massport pays 100% of operating deficit not including debt service
* '''July 1, 2005 – June 30, 2006''' – Massport pays 85% of operating deficit not including debt service
* '''July 1, 2006 – June 30, 2007''' – Massport pays 68% of operating deficit not including debt service<ref>{{cite web | url=http://www.massport.com/massport/Documents/Investor%20Relations%20Documents/Comprehensive%20Annual%20Financial%20Report%20-%20Year%20Ended%20June%2030,%202005.pdf | title=Comprehensive Annual Financial Report | publisher=Massport | date=June 30, 2005 | accessdate=October 16, 2010 | format=PDF}}</ref>

By law, Worcester had to transfer ownership of the airport to Massport sometime in 2009 or 2010.<ref>{{cite web | url=http://www.mass.gov/legis/laws/seslaw09/sl090025.htm | title=Chapter 25 of the Acts of 2009, Section 148 | publisher=Massachusetts General Court | date=June 25, 2009 | accessdate=October 16, 2010}}</ref> As of July 1, 2010, Massport is the owner and operator of the airport.

==Statistics==
In the calendar year of 2015, JetBlue carried an estimated 119,000 passengers between the two destinations, which averages to about 85% [[Passenger load factor|capacity]] per flight.<ref>http://www.bostonglobe.com/business/2016/02/26/jetblue-worcester-service-takes-off-first-two-years/LytDMEJ2YUb2UfW1vErhoO/story.html</ref>

For 12-month period ending September 30, 2015, the airport had 40,207 aircraft operations, an average of 110 per day: 92% [[general aviation]], 4% scheduled commercial, 3% military and 2% [[air taxi]]. There are 74 aircraft based at this airport: 93% single engine and 7% multi-engine.<ref name=FAA />

At its peak in 1989, Worcester Airport served about 354,000 passengers. In 2009, the airport served fewer than 50,000 passengers,<ref>{{cite news | url=http://www.boston.com/business/articles/2010/06/01/ailing_worcester_airport_seeking_its_niche/ |title=Ailing airport seeking its niche | work=The Boston Globe | author=Chase, Katie Johnston | date=June 1, 2010 |accessdate=October 16, 2010}}</ref> though 107,000 passengers used the airport in 2011.<ref>{{cite news  | url=http://www.wbjournal.com/article/20120109/PRINTEDITION/301099979 | title=Briefing: Worcester Regional Airport Economic Impact | date=January 9, 2012 | work=Worcester Business Journal Online | accessdate=March 3, 2013}}</ref>

On June 10, 2016 JetBlue and the City of Worcester celebrated the 300,000th passenger since beginning service in 2013, an average of about 84%.<ref>http://www.telegram.com/news/20160610/plane-pull-for-autism-community-celebrates-worcester-regional-airport-milestone</ref>

==Facilities and Infrastructure==
[[File:Worcester Airport Curbfront.JPG|thumb|Worcester Airport terminal curbfront]]
Worcester Regional Airport covers an area of 1,000 acres (4&nbsp;km²) which contains two [[runway]]s: 11/29 measuring 7,000 x 150&nbsp;ft (2,134 x 46 m) and 15/33 measuring 5,000 x 100&nbsp;ft (1,524 x 30 m).<ref name=FAA /> Runways 11 and 29 are instrumented with [[Instrument landing system|ILS]] equipment.<ref>{{cite web | url=http://www.airnav.com/airport/KORH | title=KORH: Worcester Regional Airport | work=FAA Information | publisher=Airnav.com | date=September 23, 2010 | accessdate=October 16, 2010}}</ref> [[Engineered materials arrestor system|EMAS]] pads are located at the starting thresholds of runways 11 and 29.<ref>FAA Airport Diagram.</ref>

The airport passenger terminal has four [[jetway]] gates (two of which are operational) and two ramp level gates for regional carriers. The terminal also houses two baggage carousels and a [[Transportation Security Administration|TSA]] installed passenger and baggage screening system.<ref>{{cite web | url=http://www.worcesterma.gov/development/airport | title=Worcester Regional Airport | publisher=City of Worcester Economic, Neighborhood & Workplace Development | year=2010 | accessdate=October 16, 2010}}</ref>

[[WBZ-TV]] operates a doppler [[weather radar]] station at the airport.

[[Rectrix Aviation]] is the [[fixed-base operator]] at the airport.

===Category III Landing System===

On April 28, 2016, Massport approved funding for the installation of a [[Instrument landing system#ILS categories|Category IIIb]] [[instrument landing system]] at ORH. The geographic location of the airport, on top of the tallest hill in the city reaching approximately 1,000 feet above sea level, leaves Worcester on average with 40 more days of fog a year than nearby Boston.<ref>http://worcestermag.com/2015/07/30/worcester-regional-airport-wants-to-be-in-whole-new-category/35383</ref> The installation of the Category IIIb landing system will allow aircraft to land and depart in virtually all weather conditions. There are no Category IIIc airports in the United States; it is simply not allowed for safety reasons. The installation of the landing system also will include a jug-handle taxiway at the approach end of Runway 11. Construction has begun and the ILS is expected to be operational sometime in late 2017.

==Airlines and destinations==
[[File:N184JBORH.jpg|thumb|A JetBlue [[Embraer 190]] landing in Worcester.]]

===Passenger===
{{Airport destination list
| [[JetBlue Airways]] | [[Fort Lauderdale–Hollywood International Airport|Fort Lauderdale]], [[John F. Kennedy International Airport|New York-JFK]] (start date TBD),<ref name="Vaccaro">{{cite news |last=Vaccaro |first=Adam |date=2017-02-28  |title=JetBlue to Offer New York-Worcester Route |url=https://www.bostonglobe.com/business/2017/02/28/worcester-big-apple-jetblue-offer-new-york-route/tKt1JOanevmJqmMP6cZr3N/story.html | work=The Boston Globe |location=Boston, Massachusetts |access-date=2017-02-28 }}</ref> [[Orlando International Airport|Orlando]]
}}

===Historical service===
*[[Northeast Airlines]], 1946–1971
*[[Mohawk Airlines]], 1965–1971 (merged with Allegheny Airlines)
*Statewide Airlines, 1965–1966
*[[Executive Airlines]], 1969–1972
*[[Delta Air Lines]], 1971–1979
*[[Allegheny Airlines]], 1971–1975
*Pilgrim Airlines, 1974–1975
*Precision Airlines, 1978–1981
*[[Bar Harbor Airlines]], 1980–1998
*[[Piedmont Airlines]], 1986–1989 (merged with US Airways)
*[[Continental Airlines]], 1987–1988
*[[Northwest Airlines]], 1988–1990
*[[US Airways Express]], 1988–2003
*[[Continental Express]], 1990–1998
*[[Carnival Air Lines]], 1993–1994
*Florida Shuttle, 1993–1994
*[[United Express]], 1998 (April–November)
*[[American Eagle Airlines]], 2000–2002
*[[Delta Connection]], 2000–2002
*[[Pan American Airways]], 2001–2002
*[[Allegiant Air]], 2005–2006
*[[Direct Air]], 2008–2012
*[[JetBlue Airways]], 2013–Present

-Source<ref>{{cite news|url=http://www.telegram.com/assets/pdf/WT17668313.PDF |title=Worcester regional Airport carrier history 1946–2012 |format=PDF |work=Worcester Telegram & Gazette |accessdate=January 11, 2014}}</ref>

==Ground transportation==
Three rental car agencies are located in the terminal building at Worcester Regional Airport. [[Avis Rent a Car System|Avis]], [[Hertz]], and [[Thrifty Rent A Car|Thrifty]] all have concession stands across from the baggage claim.

The [[Worcester Regional Transit Authority]] (WRTA)'s [http://www.therta.com/schedules/route-2/#route-map route #2] bus connects [[Union Station (Worcester, Massachusetts)|Union Station]], a regional [[MBTA Commuter Rail]], [[Amtrak]], and bus transportation hub in the Downtown Worcester district, with the airport. Union Station is the western terminus of the [[Massachusetts Bay Transportation Authority]]'s [[Framingham/Worcester Line]], with eastbound service to [[Back Bay (Massachusetts Bay Transportation Authority)|Back Bay]] and [[South Station (Massachusetts Bay Transportation Authority)|South Station]] in Boston. Additionally, service via Amtrak's ''[[Lake Shore Limited]]'' Boston section to/from [[Albany, New York]], with connections to Chicago (formerly also the ''[[Northeast Regional (Amtrak)|Regional]]'''s ''[[New Haven-Springfield Line|Inland Route]]'') stops at this location, as does intercity ([[Peter Pan Bus Lines]]), ([[Greyhound Bus Lines]]), and other local WRTA bus services at Union Station.

The airport presently lacks a direct connection to an [[Interstate Highway]]. However, a number of Interstate routes such as: I-290, [[Massachusetts Turnpike|I-90]], [[Interstate 190 (Massachusetts)|I-190]], [[Interstate 395 (Connecticut-Massachusetts)|I-395]], [[Interstate 495 (Massachusetts)|I-495]], and routes: [[Massachusetts Route 9|MA-9]], [[Massachusetts Route 122|MA-122]], and [[Massachusetts Route 146|MA-146]] provide access through smaller access roads.<ref>{{cite web | url=http://www.massport.com/worce/worce_direct.html | title=To and From Worcester Regional Airport | publisher=MassPort | year=2010 | accessdate=October 16, 2010}}</ref> Travel time to reach the airport is approximately 5–10 minutes after exiting Interstate I-290, Worcester's primary access via interstate highway from the north and the south also with direct access to the [[Massachusetts Turnpike]].

==References==
{{reflist|colwidth=30em}}

==External links==
{{Portal|Massachusetts|Aviation}}
* [http://www.massport.com/worcester-airport/ Worcester Regional Airport] (Official website)
* {{Facebook|WorcesterRegionalAirport}}
* [http://www.worcesterregionalflightacademy.com/ Worcester Regional Flight Academy]
* {{FAA-diagram|00652|Worcester Regional Airport}}
* {{US-airport|ORH}}

{{MA Airport}}
{{Worcester, Massachusetts}}

[[Category:Airports in Massachusetts]]
[[Category:Massachusetts Port Authority]]
[[Category:Transportation in Worcester, Massachusetts]]
[[Category:Buildings and structures in Worcester, Massachusetts]]
[[Category:Leicester, Massachusetts]]