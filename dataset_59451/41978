{{Taxobox
| image = Anser cygnoides-Beijing.jpg
| image_width =
| image_caption = ''Anser cygnoides cygnoides'' in China
| status = VU
| status_system = IUCN3.1
| status_ref = <ref>{{IUCN|id=22679869 |title=''Anser cygnoides'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Bird|Aves]]
| subclassis = [[Neornithes]]
| infraclassis = [[Neognathae]]
| superordo = [[Galloanseres]]
| ordo = [[Anseriformes]]
| familia = [[Anatidae]]
| subfamilia = [[Anserinae]]
| tribus = [[Anserini]]
| genus = ''[[Anser (bird)|Anser]]''
| species = '''''A. cygnoides'''''
| binomial = ''Anser cygnoides''
| binomial_authority = ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
| subdivision_ranks = [[Subspecies]]
| subdivision =
''A. c. cygnoides'' <small>([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])</small><br/>
''A. c. domesticus''{{Verify source|date=May 2009}}<!-- listed in several sources, but was it ever validly described, or is it a common nomen nudum?! --> &ndash; <small>[[Chinese goose|Chinese]] and [[African goose|African geese]]</small>
| range_map = Anser cygnoides distribution map.png
| range_map_width = 240px
| range_map_caption = Breeding (northern areas) in orange and wintering (southern areas) ranges in blue
| synonyms =
''Anas cygnoid'' <small>[[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]]</small><br />
''Anas cygnoides'' <small>[[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]] ([[emendation (taxonomy)|emendation]])</small><br />
''Cycnopsis cygnoides'' <small>(''[[lapsus]]'')</small><br />
''Cygnopsis cygnoides'' <small>([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])</small>
}}

The '''swan goose''' (''Anser cygnoides'') is a rare large [[goose]] with a natural breeding range in inland [[Mongolia]], northernmost [[China]], and southeastern [[Russia]]. It is [[bird migration|migratory]] and winters mainly in central and eastern China. Vagrant birds are encountered in [[Japan]] and [[Korea]] (where it used to winter in numbers when it was more common), and more rarely in [[Kazakhstan]], [[Laos]], coastal [[Siberia]], [[Taiwan]], [[Thailand]] and [[Uzbekistan]].<ref name = Carboneras>{{cite book|last=Carboneras|first= Carles |year=1992|editor=del Hoyo, Josep|editor2=Elliott, Andrew|editor3=Sargatal, Jordi |title=[[Handbook of Birds of the World]]. Volume 1: Ostrich to Ducks|page=581|publisher=Lynx Edicions|location=Barcelona|isbn=84-87334-10-5}}</ref><ref name=Madge>{{cite book|last=Madge|first=Steve |last2= Burn|first2=Hilary |year=1987|title=Wildfowl: an identification guide to the ducks, geese and swans of the world|series=[[Helm Identification Guides]]|publisher=Christopher Helm|location=London|isbn= 0-7470-2201-1}}</ref>

While uncommon in the wild, this [[species]] has been [[domesticated]]. Introduced and [[Feral animal|feral]] populations of its domestic breeds occur in many places outside its natural range. The wild form is also kept in collections, and escapes are not unusual amongst feral flocks of other ''[[Anser (bird)|Anser]]'' and ''[[Branta]]'' geese.

==Description==
The swan goose is large and long-necked for its [[genus]], wild birds being {{convert|81|–|94|cm|in|abbr=on}} long (the longest ''[[Anser (bird)|Anser]]'' goose) and weighing {{convert|2.8|–|3.5|kg|lb|abbr=on}} or more (the second-heaviest ''Anser'', after the [[greylag goose]], ''A. anser''). The sexes are similar, although the male is larger, with a proportionally longer bill and neck; in fact the largest females are barely as large as the smallest males. Typical measurements of the wing are {{convert|45|–|46|cm|in|abbr=on}} in males, {{convert|37.5|–|44|cm|in|abbr=on}} in females; the bill is about {{convert|8.7|–|9.8|cm|in|abbr=on}} long in males and {{convert|7.5|–|8.5|cm|in|abbr=on}} in females. The [[Tarsus (skeleton)|tarsus]] of males measures around {{convert|8.1|cm|in|abbr=on}}.<ref name = Carboneras /><ref name=Madge/> The wingspan of adult geese is {{convert|160|-|185|cm|in|abbr=on}}.<ref>{{cite book|last=Ogilvie|first= M. A.|last2= Young|first2= S. |year=2004|title=Wildfowl of the World|publisher=New Holland Publishers|isbn= 978-1-84330-328-2}}</ref>

The upperparts are greyish-brown, with thin light fringes to the larger feathers and a [[maroon (colour)|maroon]] hindneck and cap (reaching just below the eye).  The [[remiges]] are blackish, as are the entire underwing and the white-tipped [[rectrices]], while the upper- and undertail coverts are white. A thin white stripe surrounds the bill base. Apart from darker streaks on the belly and flanks, the underside is pale buff, being especially light on the lower head and foreneck which are sharply delimited against the maroon. In flight, the wings appear dark, with no conspicuous pattern. Uniquely among its [[genus]], the long, heavy bill is completely black; the legs and feet, on the other hand, are orange as in most of its relatives. The eyes' [[iris (eye)|irides]] are maroon. Juveniles are duller than adult birds, and lack the white bill base and dark streaks on the underside.<ref name = Carboneras /><ref name=Madge/>

The voice is a loud drawn-out and ascending honking ''aang''. As a warning call, a similar but more barking honk is given two or three times in short succession.<ref name=Madge/>

The [[karyotype]] of the swan goose is 2n=80, consisting of four pairs of [[macrochromosome]]s, 35 pairs of [[microchromosome]]s, and a pair of [[sex chromosome]]s. The two largest macrochromosome pairs as well as the [[Z chromosome|Z (female) chromosome]] are [[submetacentric]], while the third-largest [[chromosome]] pair is [[acrocentric]] and the fourth-largest is [[Centromere#Metacentric|metacentric]]. The [[W chromosome]]s are acrocentric too, as are the larger microchromosomes, the smaller ones probably being [[telocentric]]. Compared to the greylag goose, there seems to have been some [[Mutation#By effect on structure|rearrangement]] on the fourth-largest chromosome pair.<ref>{{cite journal|last=Wójcik|first=Ewa |last2= Smalec|first2=Elżbieta |year=2008|title= Description of the ''Anser cygnoides'' Goose Karyotype|journal=Folia Biologica |location=Kraków|volume=56|issue=1-2|pages=37–42|doi=10.3409/fb56_1-2.37-42|url=http://www.ingentaconnect.com/search/download?pub=infobike%3a%2f%2fisez%2ffb%2f2008%2f00000056%2fF0020001%2fart00006&mimetype=application%2fpdf |format=PDF}}</ref>

==Ecology==
[[File:Anser cygnoides MWNH 0947.JPG|left|thumb|Egg, Collection [[Museum Wiesbaden]]]]
It inhabits [[steppe]] to [[taiga]] and mountain valleys near [[freshwater]], grazing on plants such as [[Cyperaceae|sedge]]s (Cyperaceae), and rarely swimming. It forms small flocks outside the breeding season. In the winter, it grazes on plains and stubble fields, sometimes far from water. Birds return from the winter quarters around April, and the breeding season starts soon thereafter. It breeds as single pairs or loose groups near marshes and other [[wetland]]s, with nesting activity starting about May. The [[clutch (eggs)|clutch]] is usually 5–6 but sometimes up to 8 [[egg (biology)|egg]]s, which are laid in a shallow nest made from plants, placed directly on the ground, often on a small [[Hillock|knoll]] to keep it dry. The [[precocial]] young hatch after about 28 days and become sexually mature at 2–3 years of age. Around late August/early September, the birds leave for winter quarters, where they gather in small groups to [[moult]] their worn plumage.<ref name = Carboneras /><ref name=Madge/>

The swan goose was uplisted from [[Near Threatened]] to [[Vulnerable species|Vulnerable]] on the [[IUCN Red List]] in 1992 and further to [[Endangered]] in 2000, as its population is declining due to habitat loss and excessive hunting and (particularly on the [[Sanjiang Plain]] in [[China]]) egg collecting. But new research has shown it to be not as rare as it was believed, and consequently, it was downlisted to Vulnerable status again in 2008. Still, less than 500 pairs might remain in [[Russia]], while in [[Mongolia]] numbers are unknown though about 1,000 were seen at [[Ögii Lake]] in 1977. Favorite wintering locations in China are [[Lake Dongting]], [[Lake Poyang]], the [[Yancheng Coastal Wetlands]] and other locations around the lower [[Yangtze River]], where some 60,000 individuals may be found each year &ndash; though this may be almost the entire world population. Until the 1950s, the species wintered in small numbers (up to about 100 birds annually) in [[Japan]], but [[habitat destruction]] has driven them away. Altogether, between 60,000 and 100,000 adult Swan Geese remain in the wild today.<ref name=Carboneras/><ref>{{cite web|author=BirdLife International|year=2008|url=http://www.birdlife.org/action/science/species/global_species_programme/whats_new.html |title=2008 IUCN Red List status changes|accessdate=23 May 2008}}</ref>

==Domestication==
[[File:Anser cygnoides01.jpg|thumb|left|White [[Chinese goose]]]]
Though the majority of [[domestic geese]] are descended from the [[greylag goose]] (''A. anser''), two breeds are direct descendants of the swan goose: the [[Chinese goose]] and the [[African goose]]. These breeds have been domesticated since at least the mid-18th century &ndash; perhaps even (in [[China]]) since around 1000 BC. They vary considerably from their wild parent in appearance, temperament, and ability to produce meat and eggs; the most conspicuous feature is the prominent [[Beak#Cere|bill knob]].<ref name=Madge/><ref>{{cite book|editor-last=Buckland|editor-first=Roger |editor2-last= Guy|editor2-first= Gérard
|year=2002|chapter= Goose Production|title=FAO Animal Production and Health Papers|volume=154|isbn= 92-5-104862-2}}</small> [ftp://ftp.fao.org/docrep/fao/005/y4359E/y4359E00.pdf PDF fulltext]</ref><ref>{{cite book|last=Ekarius|first=Carol |year=2007|title=Storey's Illustrated Guide to Poultry Breeds|publisher=Storey Publishing|location=North Adams, Massachusetts|isbn=978-1-58017-667-5}}</ref>

[[Charles Darwin]] studied goose breeds as part of his work on the theory of [[evolution]]. He noted that the external differences between Chinese geese and breeds descended from the Greylag goose belied a rather close relationship:
<blockquote>"The hybrids from the common and Chinese geese (''A. cygnoides''), species which are so different that they are generally ranked in distinct genera,<ref>The swan goose had been separated in the [[monotypic]] genus ''Cygnopsis'' (alternatively spelled ''Cycnopsis'') by [[Johann Friedrich von Brandt]] in 1836, some 20 years before Darwin wrote this. ''Cygnopsis cygnoides'' essentially means "[[Swan]] lookalike", ''Cygnopsis'' signifying "looking like a swan" and ''cygnoides'' "similar to a swan".</ref> have often bred in this country with either pure parent, and in one single instance they have bred ''inter se''."<ref>{{cite book|authorlink=Charles Darwin|last=Darwin|first=Charles|year=1859|title=On the Origin of Species by Means of Natural Selection, or the Preservation of Favoured Races in the Struggle for Life|publisher=John Murray|location=London|url=http://darwin-online.org.uk/content/frameset?itemID=F373&viewtype=text&pageseq=1 |page= 253}}</ref></blockquote>

==References==
{{Reflist}}

==External links==
{{commons|Anser cygnoides}}
*[http://ibc.lynxeds.com/species/swan-goose-anser-cygnoides Media of the swan goose] at the Internet Bird Collection

{{Use dmy dates|date=March 2017}}

{{DEFAULTSORT:Goose, Swan}}
[[Category:Anser (bird)|Swan Goose]]
[[Category:Geese|Swan Goose]]
[[Category:Birds of Asia]]
[[Category:Birds of Mongolia]]
[[Category:Birds of Manchuria]]
[[Category:Birds described in 1758]]