{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Henley Beach South
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 2,289
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name=ABS2006>{{Census 2006 AUS |id =SSC41761 |name=Henley Beach South (State Suburb) |accessdate=1 February 2012| quick=on}}</ref><br />2,218 <small>''(2001 Census)''</small><ref name=ABS2001>{{Census 2001 AUS |id =SSC41741 |name=Henley Beach South (State Suburb) |accessdate=1 February 2012| quick=on}}</ref>
| est                 = 
| postcode            = 5022<ref name=Postcodes>{{cite web |url=http://www.postcodes-australia.com/areas/sa/adelaide/henley+beach+south |title=Henley Beach South, South Australia (Adelaide) |author= |date= |work=Postcodes-Australia |publisher=Postcodes-Australia.com |accessdate=1 February 2012}}</ref>
| area                = 
| dist1               = 9
| dir1                = W
| location1           = [[Adelaide city centre]]<ref name=Postcodes/>
| lga                 = [[City of Charles Sturt]]<ref name=CharlesSturt>{{cite web|url=http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |title=City of Charles Sturt Wards and Council Members |author= |date= |work= |publisher=City of Charles Sturt |accessdate=1 February 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110805135335/http://www.charlessturt.sa.gov.au/webdata/resources/files/Wards_and_Council_Members_Contact_Details.pdf |archivedate=5 August 2011 |df=dmy-all }}</ref>
| stategov            = [[Electoral district of Colton|Colton]] <small>''(2011)''</small><ref name=ECSA>{{cite web|url=http://www.ecsa.sa.gov.au/apps/news/?year=2010 |title=Electoral Districts - Electoral District for the 2010 Election |author= |date= |work= |publisher=Electoral Commission SA |accessdate=1 February 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110822191822/http://www.ecsa.sa.gov.au/apps/news/?year=2010 |archivedate=22 August 2011 |df=dmy-all }}</ref>
| fedgov              = [[Division of Hindmarsh|Hindmarsh]] <small>''(2011)''</small><ref name=AEC>{{cite web|url=http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Hindmarsh&filterby=Electorate&divid=185 |title=Find my electorate: Hindmarsh |author= |date= |work= |publisher=Australian Electoral Commission |accessdate=1 February 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110930072611/http://apps.aec.gov.au/eSearch/LocalitySearchResults.aspx?filter=Hindmarsh&filterby=Electorate&divid=185 |archivedate=30 September 2011 |df=dmy-all }}</ref>
| near-n              = [[Henley Beach, South Australia|Henley Beach]]
| near-ne             = [[Fulham, South Australia|Fulham]]
| near-e              = [[Fulham, South Australia|Fulham]]
| near-se             = [[West Beach, South Australia|West Beach]]
| near-s              = [[West Beach, South Australia|West Beach]]
| near-sw             = 
| near-w              = ''[[Gulf St Vincent]]''
| near-nw             = 
}}

'''Henley Beach South''' is a coastal [[Suburbs and localities (Australia)|suburb]] of [[Adelaide]], [[South Australia]]. It is located in the [[City of Charles Sturt]].

==Geography==
[[File:WestBeachOverview.jpg|right|thumb| West beach and Henley Beach (background)]]

Henley Beach South lies between [[Henley Beach Road, Adelaide|Henley Beach Road]] and the [[River Torrens]] outlet.<ref name=UBD>{{cite book |title=Adelaide and surrounds street directory|last= |first= |authorlink= |coauthors= |edition= 49th|year=2011 |publisher=UBD |location= |isbn=978-0-7319-2652-7 |page= }}</ref> To the north, is [[Henley Beach]] and to the south is [[West Beach, South Australia|West Beach]].

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 2,289 persons in Henley Beach South on census night. Of these, 51.6% were male and 48.4% were female.<ref name=ABS2006/>

The majority of residents (75.5%) are of Australian birth, with a further 5.4% identifying [[England]] as their country of origin.<ref name=ABS2006/>

The age distribution of Henley Beach South residents is skewed slightly higher than the greater Australian population. 71.1% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 28.9% were younger than 25 years, compared to the Australian average of 33.5%.<ref name=ABS2006/>

==Politics==

===Local government===
Henley Beach South is part of Henley Ward in the [[City of Charles Sturt]] [[Local government in Australia|local government area]], being represented in that council by Jim Fitzpatrick and Robert Randall.<ref name=CharlesSturt/>

===State and federal===
Henley Beach South lies in the state [[Electoral districts of South Australia|electoral district]] of [[Electoral district of Colton|Colton]]<ref name=ECSA/> and the federal [[Divisions of the Australian House of Representatives|electoral division]] of [[Division of Hindmarsh|Hindmarsh]].<ref name=AEC/> The suburb is represented in the [[South Australian House of Assembly]] by [[Paul Caica]]<ref name=ECSA/> and [[Australian House of Representatives|federally]] by [[Steve Georganas]].<ref name=AEC/>

==Community==

===Schools===
Henley Beach Primary School is located on Hazel Terrace.<ref>{{cite web |url=http://www.australianschoolsdirectory.com.au |title=Australian Schools Directory |author= |date= |work= |publisher=Australian Schools Directory |accessdate=1 February 2012}}</ref>

==Facilities and attractions==

===Parks===
'''[[Torrens Linear Park|Linear Park]]''', lies along the [[River Torrens]] on the suburb's southern boundary. The beach of '''Henley Beach South''' extends the length of the suburb. There is also significant greenspace in the vicinity of Lexington Road.<ref name=UBD/>

==Transportation==

===Roads===
Henley Beach South is serviced by [[Henley Beach Road, Adelaide|Henley Beach Road]], connecting the suburb to [[Adelaide city centre]]. [[Seaview Road, Adelaide|Seaview Road]] runs along the coast.<ref name=UBD/>

===Public transport===
Henley Beach South is serviced by public transport run by the [[Adelaide Metro]].<ref name=Metro>{{cite web|url=http://timetables.adelaidemetro.com.au/ttsearch.php |title=Public Transport in Adelaide |author= |date= |work=Adelaide Metro official website |publisher=Dept. for Transport, Energy and Infrastructure, Public Transport Division |accessdate=1 February 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110426182058/http://timetables.adelaidemetro.com.au/ttsearch.php |archivedate=26 April 2011 |df=dmy-all }}</ref>

===Bicycle routes===
A bicycle path extends through [[Torrens Linear Park|Linear Park]].<ref name=UBD/>

==See also==
{{Commons category|Henley Beach South, South Australia}}
* [[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.charlessturt.sa.gov.au |title=City of Charles Sturt |author= |date= |work=Official website |publisher=City of Charles Sturt |accessdate=1 February 2012}}

{{Coord|-34.927|138.501|format=dms|type:city_region:AU-SA|display=title}}

{{City of Charles Sturt suburbs}}

[[Category:Suburbs of Adelaide]]
[[Category:Beaches of South Australia]]