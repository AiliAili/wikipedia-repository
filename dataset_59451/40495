'''Lloyd Alfred "Hap" Glaudi''' (November 7, 1912 – December 29, 1989) was lead sportscaster for New Orleans CBS affiliate [[WWL-TV]].  He was part of a trio of colorful sportscasters that graced the airwaves in New Orleans for the decades beginning in the 1960s extending up until almost 2000.{{weasel-inline|date=March 2011}}  The trio included Glaudi, [[Wayne Mack]] of the New Orleans NBC affiliate [[WDSU-TV]], and [[Buddy Diliberto]] of then New Orleans ABC affiliate [[WVUE-TV]].  All three had distinct personas, fitting of [[Culture of New Orleans|New Orleans's unique culture]], with Glaudi being known as the "Dean of New Orleans Sportscasters".{{weasel-inline|date=March 2011}} Photographs of Hap Glaudi appear at the Radio Theatre of New Orleans website.<ref name="NORT" >[http://neworleansradiotheatre.org/wwloncam1.html New Orleans Radio Theatre Photographic Essay.]</ref>

== Early life and education ==

A lifelong resident of New Orleans, Lloyd Alfred Glaudi was born on November 7, 1912.  He attended [[Jesuit High School (New Orleans)]], giving him his first connection to institutions owned and operated by the [[Society of Jesus]].  It was at Jesuit High School that Glaudi earned the moniker "Hap", as he was always happy and that characterization was contracted to "Hap".  Fellow broadcaster Ron Brocato reported that Glaudi himself financed his high school education through fortunate winnings at the horse race track.  Brocato reported, "He [Glaudi] earned his tuition betting on a winning longshot at the [[Fair Grounds Race Course|Fair Grounds]] given to him by a bookie.”<ref name="Brocato1">{{Cite web  | last = Brocato  | first = Ron  | authorlink =  | coauthors =  | title = Mr. Prep Sports  | work = Sports Blogs  | publisher = NewOrleans.Com Media L.L.C  | date = 10 August 2009  | url = http://www.neworleans.com/sports/sports-blogs/ron-brocato-blog/186759-mr-prep-sports.html  | doi =  | accessdate = 19 March 2011}}</ref> During his freshman year at Jesuit High he entered the English Writing Contest with a story about an exercise boy at the New Orleans Fairgrounds who discovers a criminal plot. His story won the Gold Medal and the then school president told Hap he wouldn't have to pay tuition in the future.

Glaudi continued to higher education at [[Loyola University of New Orleans]], maintaining strong ties to the [[Society of Jesus]]. Glaudi commenced his professional journalism career with the now defunct ''[http://chroniclingamerica.loc.gov/lccn/sn87062393/ New Orleans Item]'' newspaper, one of two major daily afternoon newspapers serving New Orleans at the time.  During Glaudi's 25-year tenure with the ''Item'', Glaudi became feature sportswriter for high school sports in the Greater New Orleans area, endearing him to many locals.  Prep sports, together with horse racing, became particular areas of passion for Glaudi as a sports journalist.  Healthy competition at the time between Glaudi and [[New Orleans Times-Picayune]] sports writer N. Charles Wicker made this a "Golden Age of Prep Sports", according to journalist Brocato's reports.<ref name="Brocato1" />

== Television broadcasting career ==

Hap Glaudi's transition to broadcast journalism coincided with the rise of local television news, as afternoon daily newspapers declined in circulation.  Glaudi became WWL-TV's lead sportscaster in 1964, a tenure that would extend until 1978.  It is noteworthy that Jesuit-educated Glaudi would sign on professionally with then Jesuit-owned [[WWL-TV]], true to his Jesuit roots. During this time, WWL-TV emerged as the premier local television news station in New Orleans, with Glaudi as lead sportscaster.  As WWL-TV's evening news long remained at the top of the local television ratings, and with Glaudi handling the sports portion of the evening news, the local broadcast community informally dubbed Glaudi the "Dean of New Orleans Sportscasters".  With a soft spot for horse racing and prep sports, these remained a focal point of Glaudi's broadcasts, continuing even after the arrival of the [[New Orleans Saints]] to the market in 1967.

He was often teamed with newscasters [[Garland Robinette]] and [http://www.wwltv.com/on-tv/bios/66110237.html Angela Hill] as well as meteorologists Al Duckworth and [[Nash Roberts]], providing a team chemistry with considerable appeal to the New Orleans broadcasting market.<ref name="NORT2">[http://www.neworleansradiotheatre.org/wwloncam2.html Team of Robinette, Hill, Glaudi, and Duckworth.]</ref>  A flavor for Hap Glaudi's distinct style is easily seen in WWL-TV's own video report of Glaudi's death on December 29, 1989.<ref name="Obit">[http://www.casttv.com/video/1xftst1/hap-glaudi-death-12-29-89-wwl-tv-new-orleans-la-video WWL-TV tribute to Glaudi's passing on December 29, 1989.]</ref>  He was succeeded on WWL-TV by [[Jim Henderson (sportscaster)|Jim Henderson]]. A history of New Orleans television, including Glaudi and the WWL-TV news team of the 60s and 70s has been published.<ref>Dominic Massa, ''New Orleans Television (Images of America)'', Arcadia Publishing, 2008, ISBN 0-7385-5404-9.</ref>

== Other work ==

Glaudi added radio to his broadcasting outlets featuring affiliated radio station [[WWL (AM)]], which broadcast New Orleans Saints football games.  Glaudi provided post-game analysis in the form of a call-in radio show variously named "Hap's Fifth Quarter" or "Hap's Point After", in addition to other broadcasting duties for WWL.  Outspoken and passionate though he was about the Saints, Glaudi never took issue with even the most opinionated and overbearing of callers.<ref name="Radio Career">[http://www.rexnelsonsouthernfried.com/?p=1860 Glaudi's post-game radio show] for New Orleans Saints games.</ref>

WWL (AM) being a 50,000 watt "[[clear-channel station]]", Glaudi's radio broadcasts were heard through much of the United States and even into Ontario, Canada.  It was a result of his AM radio broadcasts that Glaudi began to have national exposure.

New Orleans during the 1960s suffered a deep racial divide that extended into prep sports.  Publicly prodded by Glaudi, then predominately white Jesuit High School and then predominately African-American [[St. Augustine High School (New Orleans)|St. Augustine High School]] agreed to play each other in high school basketball in 1965, being the two top New Orleans area prep basketball teams.  This event commenced the end of the racial divide in high school sports in the New Orleans area.<ref name="Black sports">[http://www.black-collegian.com/extracurricular/sports/passglory1199.shtml R.L Stockard, "The Black Collegian On-Line".]</ref>  This event inspired the made-for-television movie ''[[Passing Glory]]''.

Jesuit High School (New Orleans) offers a [http://www.jesuitnola.org/admissions/admissions-fullscholarships-285.htm Lloyd (Hap) Glaudi Scholarship].

== Personal life ==

Glaudi appeared in a December 19, 1964, episode of CBS-TV's hit show ''[[Gunsmoke]]'', "Aunt Thede," in a bit part as a Townsman. A common practice at the time was for the major networks to select individuals that they perceived as local celebrities to take bit parts in the parent network's popular television shows both to promote their show and their affiliates.

Glaudi lived in New Orleans all of his life, with his wife Millie and three daughters, who maintain close ties to the Greater New Orleans area, until his death of [[lung cancer]] at age 77, on December 29, 1989.

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist|2}}

== External links ==
* [http://www.neworleansradiotheatre.org/index.htm New Orleans Radio Theatre]

[[Category:Articles created via the Article Wizard]]
[[Category:New Orleans television reporters]]
[[Category:Jesuit High School (New Orleans) alumni]]
[[Category:Culture of New Orleans]]