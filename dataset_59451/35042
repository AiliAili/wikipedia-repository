{{Good article}}
{{Infobox football match
| title                   = 1978 European Super Cup
| image                   = 
| team1                   = [[R.S.C. Anderlecht|Anderlecht]]
| team1association        = {{flagicon|BEL|size=30px}}
| team1score              = 4
| team2                   = [[Liverpool F.C.|Liverpool]]
| team2association        = {{flagicon|ENG|size=30px}}
| team2score              = 3
| details                 = on aggregate
| firstleg                = First leg
| team1score1             = 3
| team2score1             = 1
| date1                   = 4 December 1978
| stadium1                = [[Constant Vanden Stock Stadium|Emile Versé Stadium]]
| city1                   = [[Brussels]]
| referee1                = [[Károly Palotai]] ([[Hungarian Football Federation|Hungary]])
| attendance1             = 35,000
| secondleg               = Second leg
| team1score2             = 1
| team2score2             = 2
| date2                   = 19 December 1978
| stadium2                = [[Anfield]]
| city2                   = [[Liverpool]]
| referee2                = [[Nicolae Rainea]] ([[Romania]])
| attendance2             = 23,598
| previous                = [[1977 European Super Cup|1977]]
| next                    = [[1979 European Super Cup|1979]]
}}

The '''1978 European Super Cup''' was a [[association football|football]] match played over two legs between [[Liverpool F.C.|Liverpool]] of [[England]] and [[R.S.C. Anderlecht|Anderlecht]] of [[Belgium]]. The first leg was played at the [[Constant Vanden Stock Stadium|Emile Versé Stadium]], [[Brussels]] on 4 December 1978 and the second leg was played on 19 December 1978 at [[Anfield]], Liverpool. It was the annual [[UEFA Super Cup|European Super Cup]] contested between the winners of the [[UEFA Champions League|European Cup]] and the [[UEFA Cup Winners' Cup|European Cup Winners' Cup]]. Liverpool were the reigning champions, while Anderlecht were appearing in the competition for the second time after winning the [[1976 European Super Cup|1976 edition]].

The teams qualified for the competition by winning the European Cup and European Cup Winners' Cup. Anderlecht won the [[1977–78 European Cup Winners' Cup]] beating Austrian team [[FK Austria Wien|Austria Wien]] 4–0 in the [[1978 European Cup Winners' Cup Final|final]]. Liverpool qualified by winning the [[1977–78 European Cup]]. They beat Belgian team [[Club Brugge KV|Club Brugge]] 1–0 in the [[1978 European Cup Final|final]].

Watched by a crowd of 35,000 at the [[Constant Vanden Stock Stadium|Emile Versé Stadium]], Anderlecht took an early lead in the first half of the first leg when [[Franky Vercauteren]] and [[François Van der Elst]] scored. Liverpool midfielder [[Jimmy Case]] scored in between the Anderlecht goals. Anderlecht extended their lead in the second half courtesy of a [[Rob Rensenbrink]] goal to secure a 3–1 victory. A crowd of 23,598 saw Liverpool take the lead in the second leg at [[Anfield]], when [[Emlyn Hughes]] scored. Anderlecht equalised in the second half when Van der Elst scored. A late goal by [[David Fairclough]] meant Liverpool won the second leg 2–1. Thus, Anderlecht won the tie 4–3 on aggregate to secure their second Super Cup triumph.

==Background==
The [[UEFA Super Cup|European Super Cup]] was founded in the early 1970s, as a means to determine the best team in Europe and serve as a challenge to [[AFC Ajax|Ajax]], the strongest club side of its day.<ref name="sco">{{cite web|url=http://www.uefa.com/uefasupercup/history/ |title=Club competition winners do battle |work=UEFA.com |accessdate=24 November 2015}}</ref> The proposal by Dutch journalist [[Anton Witkamp]], a football match between the holders of the [[UEFA Champions League|European Cup]] and [[UEFA Cup Winners' Cup|Cup Winners' Cup]], failed to receive [[UEFA]]'s backing,<ref name="sco"/> given the recent Cup Winners' Cup winners [[Rangers F.C.|Rangers]] had been banned from European competition.{{#tag:ref|In 1972, [[Rangers F.C.|Rangers]] was banned from European competition for two years after fans clashed with Spanish police while celebrating the club's victory over [[FC Dynamo Moscow|Dynamo Moscow]] in the [[1972 European Cup Winners' Cup Final|European Cup Winners' Cup Final]]. The ban was later reduced to one year on appeal.<ref>{{cite news |first=Jonathan |last=Wilson |title='The behaviour of the Scottish fans was shocking and ugly' |url=https://www.theguardian.com/sport/blog/2008/may/13/thebehaviourofthescottish |newspaper=The Observer |location=London |date=13 May 2008 |accessdate=24 November 2015}}</ref>|group="n"}} Witkamp nonetheless proceeded with his vision, a two-legged match played between Ajax and Rangers in January 1973.<ref name="sco"/> The competition was endorsed and recognised by UEFA a year later.<ref name="sco"/>

Anderlecht qualified for the Super Cup by winning the [[1977–78 European Cup Winners' Cup]]. They beat Austrian team [[FK Austria Wien|Austria Wien]] 4–0 in the [[1978 European Cup Winners' Cup Final|final]]. The result meant Anderlecht won the competition for the second time after the first victory in [[1976 European Cup Winners' Cup Final|1976]].<ref>{{cite web|url= http://en.archive.uefa.com/competitions/ecwc/history/season=1975/intro.html |title=1975/76: Anderlecht win six-goal thriller |publisher=UEFA |date=1 June 1976 |accessdate=15 June 2015 }}</ref> Anderlecht were appearing in the competition for the second time after they won the [[1976 European Super Cup|1976 edition]].<ref>{{cite web|url=http://www.uefa.com/uefasupercup/history/season=1976/index.html |title=1976: Anderlecht leave Bayern blushing |publisher=UEFA |accessdate=15 June 2015 }}</ref>

Liverpool qualified for the competition as winners of the [[1977–78 European Cup]]. They defeated Belgian team [[Club Brugge KV|Club Brugge]] 1–0 in the [[1978 European Cup Final|1978]] to win the [[UEFA Champions League|European Cup]] for the second consecutive season.<ref>{{cite news|url=https://www.theguardian.com/football/blog/2013/may/23/liverpool-great-european-cup-teams |title=The great European Cup teams: Liverpool 1977–84 |work=The Guardian |location=London |date=23 May 2013 |accessdate=3 June 2015 |first=Paul |last=Wilson }}</ref> They were the current holders of the Super Cup after beating German team [[Hamburger SV|Hamburg]] in the [[1977 European Super Cup|previous season's]] competition.<ref>{{cite web|url=http://www.uefa.com/uefasupercup/history/season=1977/index.html |title=1977: McDermott treble lifts Liverpool |publisher=UEFA |accessdate=15 June 2015 }}</ref>

Both teams had exited the respective European competitions they were competing in before the competition. Anderlecht were eliminated in the second round of the [[1978–79 European Cup Winners' Cup]] by eventual winners [[FC Barcelona|Barcelona]]. Anderlecht won the first leg 3–0, but a 3–0 victory by Barcelona in the second leg meant the tie went to [[Overtime (Sports)|extra-time]] and a subsequent [[Penalty shootout (association football)|penalty shootout]], which they lost 4–1.<ref>{{cite web|url=http://www.rsssf.com/ec/ec197879.html#cwc |title=Cup Winners' Cup 1978–79 |publisher=Rec. Sport. Soccer Statistics Foundation (RSSSF) |date=4 June 2015 |accessdate=16 June 2015 |first=Karel |last=Stokkermans }}</ref> Liverpool were competing in the [[1978–79 European Cup]] and were eliminated in the first round by the eventual winners [[Nottingham Forest F.C.|Nottingham Forest]] of England in the first round. Forest won the first leg 2–0 and a 0–0 draw in the second leg saw them progress at Liverpool's expense.<ref>{{cite web|url=http://www.rsssf.com/ec/ec197879.html#cc |title=Champions' Cup 1978–79 |publisher=Rec. Sport. Soccer Statistics Foundation (RSSSF) |date=4 June 2015 |accessdate=16 June 2015 |first=Karel |last=Stokkermans |deadurl=yes |archiveurl=https://web.archive.org/web/20150611025021/http://www.rsssf.com/ec/ec197879.html |archivedate=11 June 2015 |df= }}</ref>

==First leg==
===Summary===
[[File:François Van der Elst.jpg|thumb|right|[[François Van der Elst]], who scored Anderlecht's second goal in the first leg.]]
The first leg was held at the [[Constant Vanden Stock Stadium|Emile Versé Stadium]], the home ground of Anderlecht. It was the home side that opened the scoring in the 17th minute. Striker [[Rob Rensenbrink]] advanced down the right-hand side of the pitch and passed the ball across the Liverpool penalty area towards midfielder [[Franky Vercauteren]] who headed the ball into the Liverpool goal. Five minutes later, Anderlecht came close to extending their lead, but [[Benny Nielsen (footballer)|Benny Nielsen]]'s shot was saved by Liverpool goalkeeper [[Ray Clemence]]. Liverpool started to exert themselves upon the match following this and equalised in the 27th minute when midfielder [[Jimmy Case]] scored from the edge of the Anderlecht penalty area. Five minutes before half-time Anderlecht extended their lead. Defender [[François Van der Elst]] found space in the Liverpool defence and his shot beat Clemence in the Liverpool goal to give Anderlecht a 2–1 lead. Anderlecht defender [[Jean Thissen]] was shown a yellow card before the end of the half for a late tackle on Liverpool striker [[Kenny Dalglish]].<ref name=post>{{cite news|url=http://www.lfchistory.net/Images/newspapers/echo/1978-12-04-anderlecht-1063.jpg |title=Now those Reds need a Super comeback |work=Liverpool Daily Post |date=5 December 1978 }}</ref>

Anderlecht began the second half the better of the two sides as they pushed forward in an attempt to extend their lead. Rensenbrink, in particular, was in good form as he caused numerous problems for Liverpool defender [[Emlyn Hughes]], who started because of an injury to [[Phil Thompson]] after being absent himself for a lengthy period. Liverpool replaced [[David Johnson (footballer born 1951)|David Johnson]] with midfielder [[Steve Heighway]] in an attempt to get back into the match but it was to no avail as Anderlecht extended their lead late in the second half when Rensenbrink scored. No further goals were scored and the referee [[Károly Palotai]] blew for full-time with the final score 3–1 to Anderlecht.<ref name=post />

Liverpool manager [[Bob Paisley]] was critical of his team's performance in the first leg: "We threw it away, our attitude was wrong and we were careless. Anderlecht are a great team going forward, but we never attacked them as we should. Our approach seems to have gone a bit wrong and we've lost our scoring touch where earlier in the season our finishing was great."<ref>{{cite news|url=http://www.lfchistory.net/Images/newspapers/echo/1978-12-04-anderlecht-1063-postmatch.jpg |title=Aftermath |work=Liverpool Echo |date=5 May 1978 |first=Michael |last=Charters }}</ref>

===Details===
{{footballbox
|date=4 December 1978
|time=20:00 [[Central European Time|CET]]
|team1=[[R.S.C. Anderlecht|Anderlecht]] {{flagicon|BEL}}
|score=3–1
|report=[http://www.uefa.com/uefasupercup/history/season=1978/index.html Report]<br><ref>{{cite web |url=http://www.lfchistory.net/SeasonArchive/Game/1063 |title=Anderlecht 3–1 Liverpool |publisher=LFCHistory |accessdate=15 December 2011}}</ref>
|team2={{flagicon|ENG}} [[Liverpool F.C.|Liverpool]]
|goals1= [[Franky Vercauteren|Vercauteren]] {{goal|17}}<br />[[François Van der Elst|Van der Elst]] {{goal|38}}<br />[[Rob Rensenbrink|Rensenbrink]] {{goal|87}}
|goals2= [[Jimmy Case|Case]] {{goal|27}}
|stadium=[[Constant Vanden Stock Stadium|Emile Versé Stadium]], [[Brussels]]
|attendance=35,000
|referee=[[Károly Palotai]] ([[Hungarian Football Federation|Hungary]]) }}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size:90%" cellspacing="0" cellpadding="0"
|colspan=4|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|NED}} [[Nico de Bree]]
|-
|DF ||'''2''' ||{{flagicon|BEL}} [[François Van der Elst]] 
|-
|DF ||'''3''' ||{{flagicon|BEL}} [[Hugo Broos]]
|-
|DF ||'''4''' ||{{flagicon|NED}} [[Johnny Dusbaba]]
|-
|DF ||'''5''' ||{{flagicon|BEL}} [[Jean Thissen]] || {{yel|45}}
|-
|MF ||'''6''' ||{{flagicon|BEL}} [[Franky Vercauteren]]
|-
|FW ||'''7''' ||{{flagicon|DEN}} [[Benny Nielsen (footballer)|Benny Nielsen]]
|- 
|MF ||'''8''' ||{{flagicon|NED}} [[Ruud Geels]] 
|-
|MF ||'''9''' ||{{flagicon|NED}} [[Arie Haan]]
|-
|MF ||'''10''' ||{{flagicon|BEL}} [[Ludo Coeck]]
|-
|FW ||'''11''' ||{{flagicon|NED}} [[Rob Rensenbrink]]
|-
|colspan=2|'''Substitutes:'''
|-
|GK ||'''12''' ||{{flagicon|BEL}} [[Jacky Munaron]]
|-
|FW ||'''13''' ||{{flagicon|BEL}} [[Ronny Martens]]
|-
|DF ||'''14''' ||{{flagicon|BEL}} [[Gilbert Van Binst]]
|-
|MF ||'''15''' ||{{flagicon|NED}} [[Matty Van Toorn]]
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|BEL}} [[Raymond Goethals]]
|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align=center
|colspan="4"|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[Ray Clemence]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Phil Neal]] 
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Alan Kennedy]]
|-
|CB ||'''4''' ||{{flagicon|ENG}} [[Emlyn Hughes]]
|-
|LM ||'''5''' ||{{flagicon|ENG}} [[Ray Kennedy]]
|-
|CB ||'''6''' ||{{flagicon|SCO}} [[Alan Hansen]]
|-
|CF ||'''7''' ||{{flagicon|SCO}} [[Kenny Dalglish]] 
|-
|RM ||'''8''' ||{{flagicon|ENG}} [[Jimmy Case]]
|-
|CF ||'''9''' ||{{flagicon|ENG}} [[David Johnson (footballer born 1951)|David Johnson]] || || {{suboff|54}}
|-
|CM ||'''10'''||{{flagicon|ENG}} [[Terry McDermott]]
|-
|CM ||'''11'''||{{flagicon|SCO}} [[Graeme Souness]]
|-
|colspan=3|'''Substitutes:''' 
|-
|MF ||'''12''' ||{{flagicon|IRL}} [[Steve Heighway]] || || {{subon|54}}
|-
|GK ||'''13''' ||{{flagicon|ENG}} [[Steve Ogrizovic]]
|-
|FW ||'''14''' ||{{flagicon|ENG}} [[David Fairclough]]
|-
|MF ||'''15''' ||{{flagicon|ENG}} [[Sammy Lee (footballer)|Sammy Lee]]
|-
|DF ||'''16''' ||{{flagicon|ENG}} [[Brian Kettle]]
|-
|colspan=3|'''Manager:''' 
|-
|colspan=4|{{flagicon|ENG}} [[Bob Paisley]]
|}
|}

==Second leg==
===Summary===
[[File:DAVID FAIRCLOUGH.JPG|thumb|left|[[David Fairclough]] (pictured in 2008), who scored the last goal in the second leg.]]
Anderlecht's 3–1 victory in the first leg meant that Liverpool needed to score two goals to force the tie into extra-time. Despite heavy fog at [[Anfield]], the match went ahead and Liverpool opened the scoring in the 13th minute. A shot by midfielder [[Jimmy Case]] was saved by Anderlecht goalkeeper [[Nico de Bree]], but the ball rebounded to [[Emlyn Hughes]] who scored to give Liverpool a 1–0 lead. Following the goal, the Liverpool fans chanted "Oggy, Oggy tell us who scored" towards goalkeeper Ogrizovic, as the heavy fog made it difficult to identify players. Ogrizovic made a number of vital saves to keep Liverpool's hopes alive, saving from [[François Van der Elst]] and [[Rob Rensenbrink]].<ref name=echo>{{cite news|url= http://www.lfchistory.net/Images/newspapers/echo/1978-12-19-anderlecht-1066.jpg |title=Reds lose with style |work=Liverpool Echo |date=20 December 1978 |first=Ian |last=Hargraves }}</ref>

Liverpool continued to attack to try and score the goal they needed to level the tie, but they were unable to do so with chances not being converted, including a shot by [[Kenny Dalglish]], which was saved by de Bree. Liverpool were made to pay for their missed chances in the 71st minute when Van der Elst scored. A series of passes between himself and Rensenbrink saw him in space in the Liverpool penalty area and his shot went into the Liverpool goal to level the score at 1–1 and extend Anderlecht's lead in the tie to 4–2. Liverpool scored in the 87th minute when a pass by defender [[Phil Thompson]] was headed down by Dalglish to substitute [[David Fairclough]] who scored to make the score 2–1. However, Liverpool were unable to find the third goal they needed to send the match into extra time.<ref name=echo /> Thus, despite losing the match 2–1, Anderlecht won the Super Cup 4–3 on aggregate to become the first club to win the Super Cup for the second time.<ref>{{cite web|url=http://www.uefa.com/uefasupercup/history/season=1978/ |title=1978:Anderlecht back on top |publisher=UEFA |accessdate=15 June 2015 }}</ref> 

Following the match, referee [[Nicolae Rainea]] explained his decision to play the match despite heavy fog: "When I went out there before kick-off I decided I could see well enough and so I decided to play. I am afraid many spectators would not get a very clear view but there was no time I had any thoughts of abandoning the game." Liverpool manager Bob Paisley was critical of the decision to play the match: "You can't play football in conditions like that, it's ridiculous." Paisley suggested the competition should be played in April when the weather was better: "I think a match like this, between two leading team should be played in better weather, say in April. I know it's difficult, but it's farcical when good players like these have to slither about in fog and can't see each other."<ref>{{cite news|url=http://www.lfchistory.net/Images/newspapers/echo/1978-12-19-anderlecht-1066.jpg |title=Referee explains decision to let Anfield tie start |work=Liverpool Echo |date=20 December 1978 |first=Ian |last=Hargraves }}</ref>

===Details===
{{footballbox
|date=19 December 1978
|time= 19:30 [[Greenwich Mean Time|GMT]]
|team1=[[Liverpool F.C.|Liverpool]] {{flagicon|ENG}}
|score=2–1
|report=[http://www.uefa.com/uefasupercup/history/season=1978/index.html Report]<br><ref>{{cite web |url=http://www.lfchistory.net/SeasonArchive/Game/1066 |title=Liverpool 2–1 Anderlecht |publisher=LFCHistory |accessdate=15 December 2011}}</ref>
|team2={{flagicon|BEL}} [[R.S.C. Anderlecht|Anderlecht]]
|goals1= [[Emlyn Hughes|Hughes]] {{goal|13}}<br />[[David Fairclough|Fairclough]] {{goal|87}}
|goals2= [[François Van der Elst|Van der Elst]] {{goal|71}}
|stadium=[[Anfield]], [[Liverpool]]
|attendance=23,598
|referee=[[Nicolae Rainea]] ([[Romania]]) }}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size:90%" cellspacing="0" cellpadding="0"
|colspan="4"|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|ENG}} [[Steve Ogrizovic]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Phil Neal]] 
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Emlyn Hughes]]
|-
|CB ||'''4''' ||{{flagicon|ENG}} [[Phil Thompson]]
|-
|LM ||'''5''' ||{{flagicon|ENG}} [[Ray Kennedy]]
|-
|CB ||'''6''' ||{{flagicon|SCO}} [[Alan Hansen]]
|-
|CF ||'''7''' ||{{flagicon|SCO}} [[Kenny Dalglish]] 
|-
|RM ||'''8''' ||{{flagicon|ENG}} [[Jimmy Case]]
|-
|CF ||'''9''' ||{{flagicon|ENG}} [[David Fairclough]]
|-
|CM ||'''10'''||{{flagicon|ENG}} [[Terry McDermott]]
|-
|CM ||'''11'''||{{flagicon|SCO}} [[Graeme Souness]]
|-
|colspan=3|'''Substitutes:''' 
|-
|FW ||'''12''' ||{{flagicon|ENG}} [[David Johnson (footballer born 1951)|David Johnson]]
|-
|GK ||'''13''' ||{{flagicon|ENG}} [[Ray Clemence]]
|-
|MF ||'''14''' ||{{flagicon|IRL}} [[Steve Heighway]]
|-
|MF ||'''15''' ||{{flagicon|ENG}} [[Sammy Lee (footballer)|Sammy Lee]]
|-
|DF ||'''16''' ||{{flagicon|ENG}} [[Brian Kettle]]
|-
|colspan=3|'''Manager:''' 
|-
|colspan=4|{{flagicon|ENG}} [[Bob Paisley]]

|}
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align=center
|colspan=4|
|-
!width=25| !!width=25|
|-
|GK ||'''1''' ||{{flagicon|NED}} [[Nico de Bree]]
|-
|DF ||'''2''' ||{{flagicon|BEL}} [[Gilbert Van Binst]] 
|-
|DF ||'''3''' ||{{flagicon|NED}} [[Matty Van Toorn]]
|-
|DF ||'''4''' ||{{flagicon|NED}} [[Johnny Dusbaba]]
|-
|DF ||'''5''' ||{{flagicon|BEL}} [[Jean Thissen]]
|-
|MF ||'''6''' ||{{flagicon|BEL}} [[Franky Vercauteren]]
|-
|FW ||'''7''' ||{{flagicon|BEL}} [[François Van der Elst]]
|- 
|MF ||'''8''' ||{{flagicon|NED}} [[Ruud Geels]] || || {{suboff|46}}
|-
|MF ||'''9''' ||{{flagicon|NED}} [[Arie Haan]]
|-
|MF ||'''10''' ||{{flagicon|BEL}} [[Ludo Coeck]]
|-
|FW ||'''11''' ||{{flagicon|NED}} [[Rob Rensenbrink]]
|-
|colspan=2|'''Substitutes:'''
|-
|FW || ||{{flagicon|BEL}} [[Ronny Martens]] || || {{subon|46}}
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|BEL}} [[Raymond Goethals]]
|}
|}

==Post-match==
Anderlecht finished the [[1978–79 Belgian First Division]] in second place, four points behind champions [[K. S. K. Beveren|Beveren]]. Thus, they would compete in the [[1979–80 UEFA Cup]].<ref>{{cite web|url=http://www.rsssf.com/tablesb/belghist.html |title=Belgium – Final Tables 1895–2008 |publisher=Rec. Sport. Soccer Statistics Foundation (RSSSF) |date=10 May 2013 |accessdate=16 June 2015 |first1=Phil |last1=Ploquin |first2=Luc |last2=Nackaerts |first3=Jereon |last3=Coolsaet }}</ref> Liverpool finished the [[1978–79 Football League]] in first place, eight points clear of second-placed [[Nottingham Forest F.C.|Nottingham Forest]]. Their domestic championship triumph meant they would compete in the [[UEFA Champions League|European Cup]] the following season.<ref>{{cite web|url=http://www.rsssf.com/engpaul/FLA/1978-79.html |title=Season 1978–79 |publisher=Rec. Sport. Soccer Statistics Foundation (RSSSF) |accessdate=16 June 2015 |first=Paul |last=Felton }}</ref>

==Notes==
{{reflist|group=n}}

==References==
{{Reflist|2}}

{{European Super Cup Finals}}
{{1978–79 in European football (UEFA)}}
{{Liverpool F.C. matches}}
{{R.S.C. Anderlecht matches}}

[[Category:1978–79 in European football|Super Cup]]
[[Category:UEFA Super Cup]]
[[Category:Liverpool F.C. matches|Super Cup 1978]]
[[Category:R.S.C. Anderlecht matches|Super Cup 1978]]
[[Category:1978–79 in Belgian football|European Super Cup, 1978]]
[[Category:International club association football competitions hosted by Belgium]]
[[Category:International club association football competitions hosted by England]]
[[Category:1978–79 in English football|UEFA]]
[[Category:December 1978 sports events]]
[[Category:Sports competitions in Brussels]]
[[Category:International sports competitions hosted by Liverpool]]
[[Category:1970s in Brussels]]
[[Category:20th century in Liverpool]]