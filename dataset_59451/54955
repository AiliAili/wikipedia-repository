{{Infobox settlement
|name          = Gantiadi
|native_name            = {{lang|ka|განთიადი}} <small>{{ge icon}}</small><br>{{lang|ab|Цандрыҧшь}} <small>{{ab icon}}</small>
|other_name             = Tsandrypsh
|settlement_type        = [[Urban-type settlement]]
|image_skyline          = Tsandrypsh.JPG
|imagesize              = 
|image_caption          = Basilica in Gantiadi (6th century AD)
|image_map= Tsandryphš.svg
|map_caption=Location in Abkhazia
|pushpin_map           = Georgia
|pushpin_label_position =right
|pushpin_map_caption    = Location in Georgia
| subdivision_type       = [[List of sovereign states|Country]]
| subdivision_name        = [[Georgia (country)|Georgia]] ([[Abkhazia]]<ref name=status group=note/>)
| subdivision_type1       = [[Administrative divisions of Abkhazia|District]]
| subdivision_name1       =  [[Gagra District|Gagra]] 
|leader_title           = Mayor
|leader_name            = [[Albert Tarkil]]<ref name=gcity-admin>{{cite web|title=Администрация городов, сёл и посёлков Гагрского района|url=http://gagra.biz/administratsiya/administratsiya-gorodov-sjol-i-posjolkov-gagrskogo-rajona|publisher=[[Gagra District]]Administration|accessdate=28 May 2012}}</ref>
|leader_title2          = First Deputy Mayor
|leader_name2           = [[Karapet Karagozyan]]<ref name=gcity-admin/> 
|leader_title3          = Second Deputy Mayor
|leader_name3           = [[Grigori Kasparyan]]<ref name=gcity-admin/>
|established_title      = 
|established_date       = 
|area_total_km2           = 
|population_as_of               =2011
|population_footnotes           =
|population_total               =5170
|population_density_km2         =
|timezone               =  [[Moscow Time|MSK]]
|utc_offset             = +3
|coordinates            = {{coord|43|22|N|40|05|E|region:{{Xb|ABK}}|format=dms|display=inline,title}}
|elevation_footnotes    =  
|elevation_m            = 
|elevation_ft           =
|blank_name             = [[Köppen climate classification|Climate]]
|blank_info             = [[Humid subtropical climate|Cfa]]
|website                = 
|footnotes              =
}} 
'''Gantiadi''' ({{lang-ka|განთიადი}} {{IPA-ka|gɑntʰiɑdi||Gantiadi.ogg}}; {{lang-ru|Гантиади}}) or '''Tsandryphsh''' ({{lang-ab|Цандрыҧшь}}; {{lang-ru|Цандрыпш}}), is an [[urban-type settlement]] on the [[Black Sea]] coast in [[Georgia (country)|Georgia]], in the [[Gagra District]] of [[Abkhazia]],<ref name=status group=note>{{Abkhazia-note}}</ref> 5&nbsp;km from the [[Russia]]n border.

== Name ==
[[File:Tsandripsh Basilika.jpg|100x150px|thumbnail|left|Basilica]]
Gantiadi in historical times, was known as '''Sauchi''' ({{lang-ru|Саучи}}). Then, until 1944 as '''Yermolov''', after the Russian general [[Aleksey Petrovich Yermolov]]. From 1944 until 1991, the settlement was known as '''Gantiadi''' ({{lang-ka|განთიადი}}, {{lang-ru|Гантиади}}), from the Georgian word for ''Dawn''. After the 1992-93 [[War in Abkhazia (1992–93)|war in Abkhazia]], Gantiadi was renamed as Tsandrypsh by the de facto government, but the name Gantiadi is still used informally among Abkhazians and widely in other languages.<ref name="pashkov"/> The name Tsandrypsh derives from the princely family Tsanba.

== History ==
Gantiadi is said to have been the historical capital of the principality of [[Saniga]] before the 6th century AD. It later became the capital of [[Sadzen]].<ref name="pashkov">{{cite web|url=http://www.tsandripsh.ru|script-title=ru:Поселок Цандрипш (Цандрыпш)|last=Pashkov|first=O.V.|year=2010|language=Russian|accessdate=7 January 2010}}</ref>

==Demographics==
In 2011, Gantiadi had a population of 5,170. Of these, 55.9% were [[Armenians]], 19.6% [[Abkhaz people|Abkhaz]], 18.4% [[Russians]], 1.2% [[Ukrainians]], 0.9% [[Georgians]] and 0.7% [[Greeks]].<ref>[http://www.ethno-kavkaz.narod.ru/gagra11.html 2011 Census results]</ref>

== Main sights ==
Tsandryphsh houses a 6th century Georgian Christian [[Gantiadi church|church]].<ref>''V. Jaoshvili, R. Rcheulishvili'', [[Georgian Soviet Encyclopedia]], V. 2, p.&nbsp;680, Tbilisi, 1977.</ref> A personal residence of [[Joseph Stalin]] is also located here.{{fact|date=October 2014}}

== External links ==
* [http://www.tsandripsh.ru De facto government's website of Gantiadi]

==Notes==
{{Reflist|group=note}}

==References==
{{Reflist}}

{{Gagra District}}
{{Administrative divisions of Abkhazia}}

[[Category:Populated places in Gagra District]]


{{Abkhazia-geo-stub}}
{{Georgia-geo-stub}}