{{about||the American financier|John W. Campbell (New York)|other people|John Campbell (disambiguation)}}

{{lead too short|date=February 2016}}
{{Infobox writer<!-- for more information see [[:Template:Infobox writer/doc]] -->
| name = John W. Campbell
|image= Johnwcampbell1965.jpg
| caption = Campbell in 1965.
| pseudonym = Don A. Stuart
| birth_name = John Wood Campbell Jr.
| birth_date = {{birth date|mf=yes|1910|6|8}}
| birth_place = [[Newark, New Jersey]], United States
| death_date = {{death date and age|mf=yes|1971|7|11|1910|6|8}}
| death_place = [[Mountainside, New Jersey]], United States
| occupation = Magazine editor, writer
| alma_mater = [[MIT]] (no degree)<br />[[Duke University]] (BS, physics, 1932)
| nationality= [[Americans|American]]
| period     = 1930–1971
| genre      = [[Science fiction]]
| notableworks= <!-- concisely list the primary magazine here? -->
| influenced = [[Isaac Asimov]]
|signature=Firma_Campbell.png
}}
'''John Wood Campbell Jr.''' (June 8, 1910&nbsp;– July 11, 1971) was an [[American people|American]] [[science fiction]] writer and editor. As editor of ''Astounding Science Fiction'' (later called ''[[Analog Science Fiction and Fact]]'') from late 1937 until his death, he is generally credited with shaping the [[Golden Age of Science Fiction]].

[[Isaac Asimov]] called Campbell "the most powerful force in science fiction ever, and for the first ten years of his editorship he dominated the field completely."<ref>Isaac Asimov: ''[[I. Asimov: A Memoir]]'', p. 73.</ref>

As a writer, Campbell published super-science [[space opera]] under his own name and moody<!--, less pulpish--> stories under his primary and most famous pseudonym, Don A. Stuart. Campbell also wrote under the pen names Karl Van Kampen and Arthur McCann.<ref>The Encyclopedia of Science Fiction (John Clute), 1995, St. Martin's Griffin, pp. 187–188</ref> He stopped writing fiction after he became editor of ''Astounding''.

His novella ''"[[Who Goes There?]]"'' was adapted as the films ''[[The Thing from Another World]]'' (1951), ''[[The Thing (1982 film)|The Thing]]'' (1982), and ''[[The Thing (2011 film)|The Thing]]'' (2011) .

== Biography ==

Campbell was born in [[Newark, New Jersey]]<ref name=Ash>{{cite book| first=Brian| last=Ash|title=Who's Who in Science Fiction|publisher=Elm Tree Books| location=London| year=1976| pages = 63|isbn=0-241-89383-6}}</ref> in 1910. His father was a cold, impersonal, and unaffectionate electrical engineer. His mother, Dorothy (née Strahern) was warm but changeable of character and had an [[identical twin]] who visited them often and who disliked young John. John was unable to tell them apart and was frequently coldly rebuffed by the person he took to be his mother.<ref name = AMZ1963August/>

Campbell attended the [[Massachusetts Institute of Technology]] (MIT), where he was befriended by the eminent mathematician [[Norbert Wiener]] (who coined the term ''[[cybernetics]]''). He began writing science fiction at age 18 and sold his first stories quickly. From January 1930 to June 1931, ''Amazing'' published six of his short stories, one novel, and six letters.<ref name=isfdb/> Thus he was a well-known pulp writer at 21 – but he had failed German and MIT had dismissed him. After one year at [[Duke University]], he graduated with a Bachelor of Science in physics in 1932.<ref name=ASF1971October>{{cite journal|date=October 1971| title = ''Analog Science Fiction/Science Fact''| page = 4}}</ref><ref name=Nicholls>{{cite book| author = [[Malcolm J. Edwards]]| chapter= CAMPBELL, JOHN W(OOD) Jr| editor= [[John Clute|Clute, John]] |editor2=[[Peter Nicholls (writer)|Nicholls, Peter]]| title= [[The Encyclopedia of Science Fiction]]| publisher= [[St. Martin's Press]]| location= New York| page= 199|isbn= 0-312-09618-6| year = 1994| origyear= 1993}}</ref>

Campbell and Dona Stewart married in 1931. They divorced in 1949 and he married Margaret (Peg) Winter in 1950. He spent most of his life in New Jersey and died at home of heart failure.<ref>http://www.nndb.com/people/982/000163493/</ref><ref>''Introduction: The Father of Science Fiction'', by [[Isaac Asimov]], in ''Astounding'' edited by [[Harry Harrison (writer)|Harry Harrison]], p. ix.</ref> He was an atheist.<ref>Paul Malmont (2011). The Astounding, the Amazing, and the Unknown: A Novel. Simon and Schuster. p. 34. ISBN 978-1-4391-6893-6. For, even though John W. Campbell was an avowed atheist, when the most powerful ed at Street & Smith lost his temper, he put the fear of God into others.</ref>

== Writing career ==
[[File:Amazing stories 193001.jpg|thumb|right|Campbell's first published story, "When the Atoms Failed", was cover-featured in the January 1930 issue of ''[[Amazing Stories]]'']]
[[File:John W Campbell 193102.jpg|thumb|right|Campbell as depicted in the January 1932 issue of ''[[Wonder Stories]]'']]
[[File:Amazing stories 193610.jpg|thumb|right|The first installment of Campbell's serial "Uncertainty" took the cover of the October 1936 issue of ''[[Amazing Stories]]'']]Editor [[T. O'Conor Sloane]] lost Campbell's first manuscript that he accepted for ''[[Amazing Stories]]'', entitled "Invaders of the Infinite".<ref name=Nicholls /> "When the Atoms Failed" appeared in January 1930, followed by five more during 1930 in that monthly and in ''Amazing Stories Quarterly'', also edited by Sloane. Three were part of a space opera series featuring the characters Arcot, Morey, and Wade, and a "complete novel" in the series, "Islands of Space", was the cover story in the Spring 1931 ''Quarterly''.<ref name=isfdb/> During 1934–35 a serial novel, ''The Mightiest Machine'', ran in ''Astounding Stories'', edited by [[F. Orlin Tremaine]], and several stories featuring lead characters Penton and Blake appeared from late 1936 in ''[[Thrilling Wonder Stories]]'', edited by [[Mort Weisinger]].<ref name=isfdb/>

The early work for ''Amazing'' established Campbell's reputation as a writer of space adventure. When in 1934 he began to publish stories with a different tone he wrote as Don A. Stuart, a pseudonym derived from his wife's maiden name.<ref name=AMZ1963August>{{cite journal|date=August 1963| title = ''Amazing Stories''| page = 101}}</ref>

From 1930 until the later part of that decade, Campbell was prolific and successful under both names. Three significant stories published under the pseudonym are "Twilight" (''Astounding'', November 1934), "Night" (''Astounding'', October 1935), and "[[Who Goes There?]]" (''Astounding'', August 1938). "Who Goes There?", about a group of Antarctic researchers who discover a crashed alien vessel, formerly inhabited by a malevolent shape-changing occupant, was filmed as ''[[The Thing from Another World]]'' (1951), ''[[The Thing (1982 film)|The Thing]]'' (1982), and again as [[The Thing (2011 film)|''The Thing'']] (2011). It was published in ''Astounding'' almost a year after Campbell became its editor and it was his last significant piece of fiction, at age 28.

Campbell held the amateur radio [[call sign]] W2ZGU, and wrote many articles on electronics and radio for a wide range of magazines.<!-- where does this belong? -->

Between December 11, 1957, and June 13, 1958, he hosted a weekly science fiction radio program called ''[[Exploring Tomorrow]].'' The scripts were written by authors such as [[Gordon R. Dickson]] and [[Robert Silverberg]].

== Influence ==

Tremaine hired Campbell to succeed him<ref>[http://www.fiawol.org.uk/fanstuff/then%20archive/newworlds/NT19.htm NOVAE TERRAE #19 – Vol. 2 No. 7, page 18]; published December 1937; archived at FIAWOL.org.uk; retrieved July 31, 2014</ref> as the editor of ''Astounding'' from its October 1937 issue.<ref name=isfdb/><ref name=EarlydelRey>{{cite book| first=Lester| last=del Rey|authorlink= Lester del Rey| title=The Early del Rey| publisher=Ballantine Books| location=New York| year=1976| pages=4–7, 18 |isbn=0-345-25063-X}}</ref><ref name=Tuck>{{cite book| first=Donald H.| last=Tuck|authorlink= Donald H. Tuck| title=The Encyclopedia of Science Fiction and Fantasy: Volume 1| publisher=Advent: Publishers, Inc.| location=Chicago| year=1974| page = 87 |isbn=0-911682-20-1}}</ref> Campbell was not given full authority for ''Astounding'' until May 1938,<ref name=delRey>{{cite book| first=Lester| last=del Rey|authorlink= Lester del Rey| title=The World of Science Fiction and Fantasy: The History of a Subculture| publisher=Ballantine Books| location=New York| year=1979| page = 91 |isbn=0-345-25452-X}}</ref> but had been responsible for buying stories somewhat earlier.<ref name =EarlydelRey /><ref name=Tuck /><ref name=ASF1937November>{{cite journal|date=November 1937| title = ''Astounding Science-Fiction''| page = 159}} The statement of ownership in the November 1937 issue listed Tremaine as the editor as of October 1, 1937.</ref><ref name=ASF1938April>An editorial notice in the April 1938 issue made it clear Campbell was responsible for stories appearing as early as February. The editorial note was not signed, but it refers to stories bought for the last three issues, one of which (Lester del Rey's "The Faithful") is known to have been bought by Campbell. See the citation from ''The Early del Rey'' for del Rey's account of that sale. For the editorial note, see {{cite journal|date=April 1938| title = ''Astounding Science-Fiction''| page = 125}}</ref> He began to make changes almost immediately, instigating a "mutant" label for unusual stories, and in March 1938 changing the title from ''Astounding Stories'' to ''Astounding Science-Fiction.''

[[Lester del Rey]]'s first story, in March 1938, was an early find for Campbell, and in 1939, he published such an extraordinary group of new writers for the first time that the period is generally regarded as the beginning of the "Golden Age of Science Fiction," and the July 1939 issue in particular.<ref>For example, the Nicholls ({{cite book| editor= [[John Clute|Clute, John]] |editor2=[[Peter Nicholls (writer)|Nicholls, Peter]]| title= [[The Encyclopedia of Science Fiction]]| publisher= [[St. Martin's Press|St. Martin's Press, Inc.]]| location= New York| page= 199 |isbn=0-312-09618-6| year= 1994| origyear= 1993}}) says "The beginning of Campbell's particular Golden Age of SF can be pinpointed as the summer of 1939," and goes on to begin the discussion with the July 1939 issue. Lester del Rey ({{cite book| first=Lester| last=del Rey|authorlink= Lester del Rey| title=The World of Science Fiction and Fantasy: The History of a Subculture| publisher=Ballantine Books| location=New York| year=1979| page = 94 |isbn=0-345-25452-X}}) comments that "July was the turning point."</ref> The July issue contained [[A. E. van Vogt]]'s first story, "Black Destroyer," and Asimov's early story "Trends"; August brought [[Robert A. Heinlein]]'s first story, "[[Life-Line]]", and the next month [[Theodore Sturgeon]]'s first story appeared.

Also in 1939, Campbell started the fantasy magazine ''[[Unknown (magazine)|Unknown]]'' (later ''Unknown Worlds'').<ref>
{{cite web
|url=http://www.philsp.com/data/data370.html
|title=Unknown
|publisher=www.philsp.com
|accessdate=2009-07-01
}}
</ref> Although ''Unknown'' was canceled after only four years, a victim of wartime paper shortages, the magazine's editorial direction was significant in the evolution of modern fantasy.<ref>{{cite book|last=Joshi|first=S T|title=Icons of horror and the supernatural  |publisher=Greenwood Press|date=December 2006|page=600 |isbn=978-0-313-33780-2|url=https://books.google.com/?id=CNXZZdER5mQC&pg=PA600&lpg=PA600&dq=unknown+magazine+john+w+campbell|accessdate=2009-07-01}}</ref>

[[Image:Astounding November 1949.jpg|thumb|The November 1949 issue.]]
Campbell is widely considered to be the single most important and influential editor in the early history of science fiction. [[The Encyclopedia of Science Fiction]] wrote: "More than any other individual, he helped to shape modern sf."<ref name=Nicholls /> After 1950, new magazines such as ''[[Galaxy Science Fiction]]'' and ''[[The Magazine of Fantasy & Science Fiction]]'' moved in different directions and developed talented new writers who were not directly influenced by him. Campbell often suggested story ideas to writers (including, famously, "Write me a creature that thinks ''as well as'' a man, or ''better than'' a man, but not ''like'' a man"), and sometimes asked for stories to match cover paintings he had already bought.

Campbell had a strong formative influence on Asimov and eventually became a friend.<ref>{{cite book |title= Isaac Asimov: The Foundations of Science Fiction|last= Gunn|first= James|authorlink= James Gunn (author)|year= 1982|publisher=Oxford University Press|location= Oxford|isbn= 0-19-503059-1|pages= 12–13, 20}}</ref> Asimov said of Campbell's influence on the field: "By his own example and by his instruction and by his undeviating and persisting insistence, he forced first ''Astounding'' and then all science fiction into his mold. He abandoned the earlier orientation of the field. He demolished the stock characters who had filled it; eradicated the [[penny dreadful]] plots; extirpated the Sunday-supplement science. In a phrase, he blotted out the purple of pulp. Instead, he demanded that science-fiction writers understand science and understand people, a hard requirement that many of the established writers of the 1930s could not meet. Campbell did not compromise because of that: those who could not meet his requirements could not sell to him, and the carnage was as great as it had been in Hollywood a decade before, while silent movies had given way to the talkies."<ref>''Introduction: The Father of Science Fiction'', by Isaac Asimov, in ''Astounding'' edited by [[Harry Harrison (writer)|Harry Harrison]], pp. ix–x.</ref>

The most famous example of the type of speculative but plausible science fiction that Campbell demanded from his writers is "[[Deadline (science fiction story)|Deadline]]," a short story by [[Cleve Cartmill]] that appeared during the wartime year of 1944, a year before the detonation of the first [[atomic bomb]]. As [[Ben Bova]], Campbell's successor as editor at ''Analog,'' wrote, it "described the basic facts of how to build an atomic bomb. Cartmill and Campbell worked together on the story, drawing their scientific information from papers published in the technical journals before the war. To them, the mechanics of constructing a uranium-fission bomb seemed perfectly obvious." The FBI descended on Campbell's office after the story appeared in print and demanded that the issue be removed from the newsstands. Campbell convinced them that by removing the magazine "the FBI would be advertising to everyone that such a project existed and was aimed at developing nuclear weapons" and the demand was dropped.<ref>''Through Eyes of Wonder'', by Ben Bova, pp. 66–67.</ref>

Campbell was also responsible for the grim and controversial ending of [[Tom Godwin]]'s famous short story "[[The Cold Equations]]". Writer [[Joseph L. Green|Joe Green]] recounted that Campbell had "three times sent 'Cold Equations' back to Godwin, before he got the version he wanted&nbsp;... Godwin kept coming up with ingenious ways to save the girl! Since the strength of this deservedly classic story lies in the fact that the life of one young woman must be sacrificed to save the lives of many, it simply would not have the same impact if she had lived."<ref>''Our Five Days with John W. Campbell'', by Joe Green, ''The Bulletin of the Science Fiction and Fantasy Writers of America'', Fall 2006, No. 171, p. 13.</ref>

==Views==

===Slavery===
Green wrote that Campbell "enjoyed taking the 'devil's advocate' position in almost any area, willing to defend even viewpoints with which he disagreed if that led to a livelier debate." As an example, he wrote, Campbell "pointed out that the much-maligned 'peculiar institution' of [[slavery]] in the [[American South]] had in fact provided the blacks brought there with a higher standard of living than they had in Africa&nbsp;... I suspected, from comments by Asimov, among others&nbsp;– and some ''Analog'' editorials I had read&nbsp;– that John held some [[racist]] views, at least in regard to blacks." Finally, however, Green agreed with Campbell that "rapidly increasing mechanization after 1850 would have soon rendered slavery obsolete anyhow. It would have been better for the USA to endure it a few more years than suffer the truly horrendous costs of the Civil War."<ref>''Our Five Days with John W. Campbell'', by Joe Green, ''The Bulletin of the Science Fiction and Fantasy Writers of America'', Fall 2006, No. 171, p. 15.</ref>

In a June 1961 editorial called "Civil War Centennial," Campbell argued that slavery had been a dominant form of human relationships for most of history and that the present was unusual in that anti-slavery cultures dominated the planet. He wrote, "It's my bet that the South would have been integrated by 1910. The job would have been done&nbsp;– and done right&nbsp;– half a century sooner, with vastly less human misery, and with almost no bloodshed&nbsp;... The only way slavery has ever been ended, anywhere, is by introducing industry&nbsp;... If a man is a skilled and competent machinist&nbsp;– if the [[lathe]]s work well under his hands&nbsp;– the industrial management will be forced, to remain in business, to accept that fact, whether the man be black, white, purple, or polka-dotted."<ref>Editorial of June 1961, ''Analog'', p. 5.</ref>

According to [[Michael Moorcock]], "He also, when faced with the [[Watts riots]] of the mid-sixties, seriously proposed and went on to proposing that there were 'natural' slaves who were unhappy if freed. I sat on a panel with him in 1965, as he pointed out that the worker bee when unable to work dies of misery, that the [[moujik]]s when freed went to their masters and begged to be enslaved again, that the ideals of the anti-slavers who fought in the Civil War were merely expressions of self-interest and that the blacks were 'against' emancipation, which was fundamentally why they were indulging in 'leaderless' riots in the suburbs of Los Angeles."<ref>''Starship Stormtroopers'', by Michael Moorcock</ref>

=== Smoking ===

Campbell was a heavy smoker throughout his life and was seldom seen without his customary cigarette holder. In the ''Analog'' of September, 1964, nine months after the [[Surgeon General of the United States|Surgeon General]]'s first major warning about the dangers of cigarette smoking had been issued on January 11, Campbell ran an editorial, "A Counterblaste to Tobacco" that took its title from the [[A Counterblaste to Tobacco|similarly named anti-smoking book]] by [[James I of England]].<ref>Editorial of June 1961, ''Analog,'' p. 8.</ref> In it, he stated that the connection to lung cancer was "esoteric" and referred to "a barely determinable possible correlation between cigarette smoking and cancer." He claimed that tobacco's calming effects led to more effective thinking.<ref>Editorial of September 1964, ''Analog'', p. 8.</ref>

However, in a one-page piece about automobile safety in the ''Analog'' dated May, 1967, Campbell wrote of "people suddenly becoming conscious of the fact that cars kill more people than cigarettes do, even if the antitobacco alarmists were completely right..." <ref>"Unsafe at High Speed," May 1967, ''Analog'', p. 80.</ref>

=== Pseudoscience and fringe politics ===
In the 1930s Campbell became interested in [[Joseph Banks Rhine|Joseph Rhine's]] [[Extrasensory Perception (book)|theories]] about [[Extrasensory perception|ESP]] (Rhine had already founded Parapsychology Laboratory at Duke University when Campbell was a student there),<ref>[https://archive.org/stream/WhoGoesThere_414/WhoGoesThere.txt Who Goes There?]  "I guess you and I, Doc, weren't so sensitive – if you want to  believe in telepathy." "I have to," Copper sighted. "Dr. Rhine of Duke University has shown that it exist, shown that some  are much more sensitive than others." -August 1938</ref> and over the following years his growing interest in parapsychology would be reflected in the stories he published when he encouraged the writers to include these topics in their tales,<ref>[http://www.depauw.edu/sfs/interviews/williamson54interview.htm An Interview with Jack Williamson] "He had gotten interested in the work that Joseph Rhine was conducting in psi phenomena at Duke University. I had written "With Folded Hands" without consultation with Campbell at all. He liked it and accepted it for publication, but he suggested that I look into Rhine."</ref> leading to the publication of numerous works about [[telepathy]] and other [[psionic]] abilities. His increasing tendencies would eventually start to isolate and alienate him from some of his own writers. He wrote favorably about such things as the "[[Dean drive]]", a device that supposedly produced thrust in violation of [[Sir Isaac Newton|Newton's]] [[Newton's laws of motion|third law]], and the "[[Hieronymus machine]]", which could supposedly amplify [[Psi (parapsychology)|psi]] powers.<ref name=ASF1950April /><ref>Science-fiction writer and critic Damon Knight commented in his book ''[[In Search of Wonder]]'': "In the pantheon of magazine science fiction there is no more complex and puzzling figure than that of John Campbell, and certainly none odder." Knight also wrote a four-stanza ditty about some of Campbell's new interests. The first stanza reads:
:Oh, the Dean Machine, the Dean Machine,
:You put it right in a submarine,
:And it flies so high that it cannot be seen&nbsp;–
:The wonderful, wonderful Dean Machine!</ref><ref>In 1957, novelist and critic [[James Blish]] tallied: "From the professional writer's point of view, the primary interest in ''Astounding Science Fiction'' continues to center on the editor's preoccupation with extrasensory powers and perceptions ('psi') as a springboard for stories&nbsp;... 113 pages of the total editorial content of the January and February 1957 issues of this magazine are devoted to psi, and 172 to non-psi material&nbsp;... By including the first part of a serial that later becomes a novel about psi the total for these first two issues of 1957 is 145 pages of psi text, and 140 pages of non-psi." James Blish, ''The Issues at Hand,'' pp. 86–87.</ref>

In 1949, Campbell also became interested in [[Dianetics]]. He wrote of [[L. Ron Hubbard]]'s initial article in ''Astounding'' that "It is, I assure you in full and absolute sincerity, one of the most important articles ever published."<ref name=ASF1950April>{{cite journal|date=April 1950| title = ''Astounding Science Fiction''| page = 132}}</ref> He also claimed to have successfully used dianetic techniques himself.

Asimov wrote: "A number of writers wrote pseudoscientific stuff to ensure sales to Campbell, but the best writers retreated, I among them.&nbsp;..."<ref>''I. Asimov,'' Isaac Asimov, p. 74.</ref> Elsewhere Asimov went on to further explain, "Campbell championed far-out ideas&nbsp;... He pained very many of the men he had trained (including me) in doing so, but felt it was his duty to stir up the minds of his readers and force curiosity right out to the border lines. He began a series of editorials&nbsp;... in which he championed a social point of view that could sometimes be described as far right (he expressed sympathy for [[George Wallace]] in the 1968 national election, for instance).  There was bitter opposition to this from many (including me – I could hardly ever read a Campbell editorial and keep my temper)."<ref>''Introduction: The Father of Science Fiction'', by [[Isaac Asimov]], in ''Astounding'' edited by [[Harry Harrison (writer)|Harry Harrison]], p. xii.</ref>

== Assessment by peers ==
[[Damon Knight]] described Campbell as a "portly, bristled-haired blond man with a challenging stare."<ref>''Hell's Cartographers'', edited by Brian W. Aldiss and Harry Harrison, p. 114.</ref> "Six-foot-one, with hawklike features, he presented a formidable appearance," said [[Sam Moskowitz]].<ref>Moskowitz</ref> "He was a tall, large man with light hair, a beaky nose, a wide face with thin lips, and with a cigarette in a holder forever clamped between his teeth,"  wrote Asimov.<ref name=IAsimov72>''I. Asimov'', Isaac Asimov, p. 72.</ref>

[[Algis Budrys]] wrote that "John W. Campbell was the greatest editor SF has seen or is likely to see, and is in fact one of the major editors in all English-language literature in the middle years of the twentieth century. All about you is the heritage of what he built".<ref>''Benchmarks Revisited 1983–1986'', Ansible Editions, 2013, p.241</ref>

Asimov said that Campbell was "talkative, opinionated, quicksilver-minded, overbearing. Talking to him meant listening to a monologue&nbsp;..."<ref name="IAsimov72"/> Knight agreed: "Campbell's lecture-room manner was so unpleasant to me that I was unwilling to face it. Campbell talked a good deal more than he listened, and he liked to say outrageous things."<ref>''Hell's Cartographers'', edited by Brian W. Aldiss and Harry Harrison, p. 133.</ref>

British novelist and critic [[Kingsley Amis]] dismissed Campbell brusquely: "I might just add as a sociological note that the editor of ''Astounding,'' himself a deviant figure of marked ferocity, seems to think he has invented a psi machine."<ref>''[[New Maps of Hell (book)|New Maps of Hell]],'' Kingsley Amis, 1960, p. 84.</ref>

British SF novelist [[Michael Moorcock]], as part of his ''Starship Stormtroopers'' editorial, claimed Campbell's ''Stories'' and its writers were "wild-eyed paternalists to a man, fierce anti-socialists" with "[stories] full of crew-cut wisecracking, cigar-chewing, competent guys (like Campbell's image of himself)", who had success because their "work reflected the deep-seated conservatism of the majority of their readers, who saw a Bolshevik menace in every union meeting".  He viewed Campbell as turning the magazine into a vessel for right-wing politics, "by the early 1950s&nbsp;... a crypto-fascist deeply philistine magazine pretending to intellectualism and offering idealistic kids an 'alternative' that was, of course, no alternative at all".<ref>[http://flag.blackened.net/liberty/moorcock.html "Starship Stormtroopers" by Michael Moorcock]</ref>

SF writer [[Alfred Bester]], an editor of ''Holiday Magazine'' and a sophisticated [[Manhattan]]ite, recounted at some length his "one demented meeting" with Campbell, a man he imagined from afar to be "a combination of [[Bertrand Russell]] and [[Ernest Rutherford]]." The first thing Campbell said to him was that [[Sigmund Freud|Freud]] was dead, destroyed by the new discovery of [[Dianetics]], which, he predicted, would win L. Ron Hubbard the [[Nobel Peace Prize]]. Campbell ordered the bemused Bester to "think back. Clear yourself. Remember! You can remember when your mother tried to abort you with a button hook. You've never stopped hating her for it." Bester commented: "It reinforced my private opinion that a majority of the science-fiction crowd, despite their brilliance, were missing their marbles."<ref>''Hell's Cartographers'', edited by Brian W. Aldiss and Harry Harrison, p. 57.</ref>

Campbell died in 1971 at the age of 61 in [[Mountainside, New Jersey]].<ref>{{cite book |last1= Solstein|first1= Eric|last2= Moosnick|first2= Gregory|editor1-first= |editor1-last= |editor1-link= |others= |title= John W. Campbell's Golden Age of Science Fiction: Text Supplement to the DVD|url=http://www2.ku.edu/~sfcenter/JWC_Study_Supplement.pdf|accessdate= 2010-05-28|date=May 23, 2002|publisher=Digital Media Zone|pages= 98–100|chapter=Appendix F: Obituary from The New York Times (July 13, 1971)}}</ref> At the time of his sudden death after 34 years at the helm of ''Analog,'' Campbell's quirky personality and eccentric editorial demands had alienated a number of his most illustrious writers to the point that they no longer submitted works to him.

Asimov remained grateful for Campbell's early friendship and support. He dedicated ''[[The Early Asimov]]'' (1972) to him, and concluded it by stating that "There is no way at all to express how he meant to me and how much he did for me except, perhaps, to write this book evoking, once more, those days of a quarter century ago".<ref name="asimov1972">{{cite book|title=The Early Asimov or, Eleven Years of Trying|publisher=Doubleday|url=https://openlibrary.org/books/OL5296223M/The_early_Asimov|author=Asimov, Isaac|year=1972|page=564}}</ref>

His final word on Campbell, however, was that "in the last twenty years of his life, he was only a diminishing shadow of what he had once been."<ref>''I. Asimov'', p. 74.</ref> Even Heinlein, perhaps Campbell's most important discovery and a "fast friend,"<ref>''[[Grumbles from the Grave]]'', edited by Virginia Heinlein, p. 8.</ref> eventually tired of him.<ref>''Grumbles from the Grave'', edited by Virginia Heinlein, p. 36: "When [[Podkayne of Mars|Podkayne]] was offered to him, he wrote Robert, asking what he knew about raising young girls in a few thousand carefully chosen words. The friendship dwindled, and was eventually completely gone."</ref><ref>''Grumbles from the Grave'', edited by Virginia Heinlein, p. 152: In 1963, Heinlein wrote his agent to say that a rejection from another magazine was "pleasanter than offering copy to John Campbell, having it bounced (he bounced both of my last two [[Hugo Award]] winners)&nbsp;– and then have to wade through ten pages of his arrogant insults, explaining to me why my story is no good."</ref>

[[Poul Anderson]] wrote that Campbell "had saved and regenerated science fiction", which had become "the product of hack pulpsters" when he took over ''Astounding''. "By his editorial policies and the help and encouragement he gave his writers (always behind the scenes), he raised both the literary and the intellectual standard anew. Whatever progress has been made stems from that renaissance".<ref>[https://books.google.com/books?id=LhkmASDA39gC&pg=PT68&lpg=PT68&dq=%22product+of+hack+pulpsters%22&source=bl&ots=5tgegPvmlN&sig=v0Jcs1hLSfjEvr4_q5ya5smIEhk&hl=en&sa=X&ved=0CB8Q6AEwAGoVChMIg7nLrqC9xwIVB5INCh149Av-#v=onepage&q=%22product%20of%20hack%20pulpsters%22&f=false "John Campbell", in ''All One Universe'', Macmillan, 1997]</ref>

==Awards and honors==
Shortly after Campbell's death, the University of Kansas science fiction program – now the [[Center for the Study of Science Fiction]] – established the annual [[John W. Campbell Memorial Award for Best Science Fiction Novel]] and also renamed after him its annual [[Campbell Conference (science fiction)|Campbell Conference]]. The [[World Science Fiction Society]] established the annual [[John W. Campbell Award for Best New Writer]]. All three memorials became effective in 1973.<!-- sources are our two awards and one conference article -->

The [[EMP Museum#Science Fiction and Fantasy Hall of Fame|Science Fiction and Fantasy Hall of Fame]] inducted Campbell in 1996, in its inaugural class of two deceased and two living persons.<ref name=sfhof-old>
[http://www.midamericon.org/halloffame/ "Science Fiction and Fantasy Hall of Fame"]. Mid American Science Fiction and Fantasy Conventions, Inc. Retrieved 2013-03-23. This was the official website of the hall of fame to 2004.</ref>

Campbell and ''Astounding'' shared one of the inaugural [[Hugo Awards]] with [[H. L. Gold]] and ''Galaxy'' at the 1953 [[World Science Fiction Convention]]. Subsequently, they won the [[Hugo Award for Best Professional Magazine]] seven times to 1965.<ref name=SFAwards/>

==Works==
{{Main article|John W. Campbell bibliography}}
This shortened bibliography lists each title once. Some titles that are duplicated are different versions, whereas other publications of Campbell's with different titles are simply selections from or retitlings of other works, and have hence been omitted. The main bibliographic sources are footnoted from this paragraph and provided much of the information in the following sections.<ref name=Nicholls/><ref name=Tuck /><ref name=Currey>{{cite book|first=L.W.|last=Currey|title=Science Fiction and Fantasy Authors: A Bibliography of First Printings of Their Fiction| publisher=G.K. Hall & Co.|location=Boston| year=1979|page=97|isbn=0-8161-8242-6}}</ref><ref name=Contento>{{cite web|url=http://contento.best.vwh.net|title=Index to Science Fiction Anthologies and Collections, Combined Edition}}</ref><ref name=Reginald>{{cite book|first=R.|last=Reginald|title=Science Fiction and Fantasy Literature: Volume 1: Indexes to the Literature|publisher=Gale Research Company|location=Detroit|year=1979|pages=88–89|isbn=0-810-31051-1}}</ref>

===Novels===
* ''[[The Mightiest Machine]]'' (1947)
* ''[[The Incredible Planet]]'' (1949)
* ''[[The Black Star Passes]]'' (1953)
* ''[[Islands of Space]]'' (1956)
* ''[[Invaders from the Infinite]]'' (1961)
* ''[[The Ultimate Weapon]]'' (1966)

=== Short story collections and omnibus editions ===
* ''[[Who Goes There? (collection)|Who Goes There?]]'' (1948)
* ''[[The Moon is Hell]]'' (1951)
* ''[[Cloak of Aesir]]'' (1952)
* ''The Planeteers'' (1966)
* ''The Best of John W. Campbell'' (1973)
* ''[[The Space Beyond]]'' (1976)
* ''The Best of John W. Campbell'' (1976) (Differs from 1973 version)
* ''A New Dawn: The Don A. Stuart Stories of John W. Campbell, Jr.'' (2003)

=== Edited books ===
* ''From Unknown Worlds'' (1948)
* ''The Astounding Science Fiction Anthology'' (1952)
* ''Prologue to Analog'' (1962)
* ''Analog I'' (1963)
* ''Analog II'' (1964)
* ''Analog 3'' (1965)
* ''Analog 4'' (1966)
* ''Analog 5'' (1967)
* ''Analog 6'' (1968)
* ''Analog 7'' (1969)
* ''Analog 8'' (1971)

=== Nonfiction ===
*  Editorial Number Three: "Letter From the Editor", in ''A Requiem for Astounding'' (1964)
* ''Collected Editorials from Analog'' (1966)
* ''The John W. Campbell Letters, Volume 1'' (1986)
* ''The John W. Campbell Letters with Isaac Asimov & A.E. van Vogt, Volume II'' (1993)

=== Memorial works ===
Memorial works ([[Festschrift]]) include:
* ''[[Astounding: The John W. Campbell Memorial Anthology]]'' (1973) edited by [[Harry Harrison (writer)|Harry Harrison]]

==See also==
{{Portal bar |Science fiction }} <!-- delete "bar" if/when there are enough ordinary See also -->

== References ==
;Citations
{{Reflist|25em |refs=
<ref name=isfdb>
{{isfdb name |14}} ('''ISFDB'''). Retrieved 2013-04-13. Select a title to see its linked publication history and general information. Select a particular edition (title) for more data at that level, such as a front cover image or linked contents.</ref>

<!-- some awards refs -->
<ref name=SFAwards>
[http://www.locusmag.com/SFAwards/Db/NomLit22.html#773 "Campbell, John W."] ''The Locus Index to SF Awards: Index of Literary Nominees''. [[Locus Publications]]. Retrieved 2013-04-13.</ref>
}}
;Sources
* Isaac Asimov: ''[[I. Asimov: A Memoir]]'', [[Doubleday (publisher)|Doubleday]], New York, 1994 ISBN 0-385-41701-2
* Sam Moskowitz: "John W. Campbell: The Writing Years", in ''Amazing Stories'', August 1963; Ziff-Davis Publishing Corporation. Reprinted in ''Seekers of Tomorrow, Masters of Modern Science Fiction'', Sam Moskowitz, [[Ballantine Books]], New York, 1967
* ''Hell's Cartographers, Some Personal Histories of Science Fiction Writers'', edited by [[Brian W. Aldiss]] and [[Harry Harrison (writer)|Harry Harrison]], [[Harper & Row]], New York, 1975 ISBN 0-06-010052-4
* ''New Maps of Hell'', Kingsley Amis, [[Ballantine Books]], New York, 1960
* ''[[The Encyclopedia of Science Fiction]]'', edited by [[John Clute]] & [[Peter Nicholls (writer)|Peter Nicholls]], [[St. Martin's Press]], New York, 1993 ISBN 0-312-09618-6
* ''Grumbles from the Grave'', selected letters of Robert A. Heinlein, edited by Virginia Heinlein, [[Del Rey Books]], New York, 1989 ISBN 0-345-36246-2
* ''Astounding'', edited by Harry Harrison, [[Random House]], New York, 1973 ISBN 0-394-48167-4)
* ''Through Eyes of Wonder'', by [[Ben Bova]], Addisonian Press, Reading, Massachusetts, 1975, ISBN 0-201-09206-9
* ''A Requiem for Astounding'', by Alva Rogers, with editorial comments by Harry Bates, F. Orlin Tremaine, and John W. Campbell, Advent:Publishers, Chicago, 1964
* ''More Issues at Hand'', by James Blish, writing as William Atheling, Jr., Advent:Publishers, Inc. Chicago, 1970
* ''Our Five Days with John W. Campbell'', by Joe Green, ''The Bulletin of the Science Fiction and Fantasy Writers of America'', Fall 2006, No. 171, pp.&nbsp;13–16

== Further reading ==
*John W Campbell, William F Nolan, ''Who Goes There? The Novella That Formed The Basis Of "The Thing"'' (Rocket Ride Books, 2009). ISBN 978-0-9823322-0-7
*John Hamilton, ''The Golden Age and Beyond: The World of Science Fiction'', pp.&nbsp;8–11 ([[ABDO Publishing Company]], 2007). ISBN 978-1-59679-989-9

==External links==

===Audio===
{{wikiquote}}
* [https://archive.org/details/Exploring_Tomorrow John W. Campbell as host of the Mutual Broadcasting System's ''Exploring Tomorrow'' (1957–58)]

===Biography and criticism===
*{{sfhof |926 |John W. Campbell Jr.}}
* [http://www.thewaythefutureblogs.com/2009/12/astounding-campbell-years Astounding: The Campbell Years] by [[Frederik Pohl]]
* “John W. Campbell, Jr.” by [[Ben Bova]], ''Analog'' June 2015 (thousandth issue)

===Bibliography and works===
* {{isfdb name|14}}
* {{Goodreads author}}
* {{IBList|type=author|id=4602|name=John W. Campbell}}
* {{IMDb name|0132168}}
* {{Gutenberg author |id=Campbell,+John+Wood}}
* {{Internet Archive author |sname=John Wood Campbell |sopt=t}}
* {{Librivox author |id=816}}
* [http://www.catch22.com/SF/ARB/SFC/Campbell,JW.php3 John Wood Campbell], Alpha Ralpha Boulevard
* {{dmoz|Arts/Literature/Genres/Science_Fiction/Authors/C/Campbell,_John_W./}}

{{Good article}}
{{The Thing}}
{{Science fiction}}

{{Authority control}}

{{DEFAULTSORT:Campbell, John W.}}
[[Category:1910 births]]
[[Category:1971 deaths]]
[[Category:American atheists]]
[[Category:American science fiction writers]]
[[Category:Science fiction editors]]
[[Category:20th-century American novelists]]
[[Category:Duke University alumni]]
[[Category:Science Fiction Hall of Fame inductees]]
[[Category:Writers from Newark, New Jersey]]
[[Category:Analog Science Fiction and Fact people]]
[[Category:Unknown (magazine)]]
[[Category:Street & Smith]]
[[Category:American male novelists]]
[[Category:Hugo Award-winning editors]]
[[Category:American male short story writers]]
[[Category:20th-century American short story writers]]
[[Category:American speculative fiction editors]]