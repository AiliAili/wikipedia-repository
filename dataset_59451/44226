{{Infobox airport
| name         = Lørenskog Heliport, Ahus
| nativename   = 
| image        = LN-OOH.jpg
| image-width  = 300px
| caption      = [[Eurocopter EC135|Eurocopter EC-135P2+]]
| IATA         =
| ICAO         = ENLX
| type         = Private
| owner        = [[Oslo University Hospital]]
| operator     = [[Norsk Luftambulanse]]
| city-served  = [[Lørenskog]], [[Norway]]
| location     = [[Akershus University Hospital]], Lørenskog
| metric-elev  = yes
| elevation-f  =
| elevation-m  =
| website      = 
| coordinates  = {{coord|59|55|56|N|10|59|13|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label          = '''ENLX'''
| pushpin_label_position = right
| pushpin_map_caption    = Location within Norway
| metric-rwy   = yes
| h1-number    = 
| h1-length-m  = 20
| h1-length-f  =
| h1-surface   = Asphalt
}}
'''Lørenskog Heliport, Ahus''' ({{airport codes||ENLX|p=n}}; {{lang-no|Lørenskog helikopterplass, Ahus}}) is a [[heliport]] situated on the premises of [[Akershus University Hospital]] (Ahus) in [[Lørenskog]], [[Norway]]. It servers as the main base for [[air ambulance]] helicopters in the central part of [[Eastern Norway]]. It is a base for a [[Eurocopter EC135|Eurocopter EC-135P2+]] and a [[Eurocopter EC145|Eurocopter EC145 T2]], both operated by [[Norsk Luftambulanse]] (NLA). The base became the first permanent ambulance heliport in the country when it opened in 1978. The facility is owned by [[Oslo University Hospital]].

==History==
After NLA was founed in 1977, it started working to establish a permanent ambulance helicopter base in the central parts of Eastern Norway.<ref>Andersen: 22</ref> Initially the foundation started fund-raising to run a one-year trial with a helicopter based near Oslo. The proposal was rejected by the [[Ministry of Health and Care Services|Ministry of Social Affairs]], which stated that the need for such a service would be primarily in remote locations and that an equal service for the whole country would have to be established for state funding.<ref name="Andersen: 30">Andersen: 30</ref>

As a promotional stunt, NLA allied with the [[Norwegian Automobile Federation]] (NAF) and brought an [[ADAC]] helicopter to various sites in Eastern Norway. By indicated that each location was a possible candidate for a base, they were able to garnish support from the various local newspapers. However, NAF did not have funds to follow through on the project and pulled out.<ref>Andersen: 34</ref>

Norsk Luftambulanse started discussions with [[Akershus County Municipality]], who were positive to the service.This resulted in Akershus Central Hospital (SiA, today Ahus) being selected as the initial base.<ref>Andersen: 31</ref> NLA's first helicopter was a [[MBB Bo 105|Messerschmitt-Bölkow-Blohm Bo 105]] which was leased from the manufacturer and named ''Bård 1''.<ref>Andersen: 29</ref> To aid funding, it received advertisements.<ref name="Andersen: 30"/> The base was inaugurated on 7 June 1978 as Lørenskog Heliport, Central Hospital.<ref>Andersen: 14</ref>

The initial base was a simple structure consisting of a plastic hangar and a small room at the hospital. The crew received dorms in a nearby dormitory.<ref name=a128>Andersen: 128</ref> NLA did not have an [[air operator's certificate]] to begin with and subcontracted operations to [[Mørefly]].<ref name=a16>Andersen: 16</ref> High operating costs caused NLA to switch operators to [[Partnair]] from 1 May 1979. They proposed moving the base to [[Oslo Airport, Fornebu]] to cut costs, but this was rejected.<ref>Andersen: 49</ref> In its first year of operation, the base carried out 216 missions.<ref>{{cite book |publisher=[[Ministry of Health and Care Services|Ministry of Health and Social Affairs]] |title=Luftambulansetjenesten i Norge |work=[[Norwegian Official Report]] |url=http://www.regjeringen.no/Rpub/NOU/19981998/008/PDFA/NOU199819980008000DDDPDFA.pdf |format=PDF |language=Norwegian |accessdate=30 November 2014 |volume=8 |year=1998 |page=13}}</ref> Until [[Stavanger Heliport, University Hospital|Stavanger Heliport, Central Hospital]] opened in 1981, Lørenskog was the only ambulance helicopter base in Norway.<ref name=a128 />

[[File:LN-OOM.jpg|thumb|left|The [[Eurocopter EC145|Eurocopter EC145 T2]]]]
The original base had limited facilities and long distances between the crew rooms and the helicopter. This caused the need for a new, compact base, which opened in November 1987.<ref name=a128 /> NLA located its Global Medical Services to the heliport. The division was responsible for handling international calls from members regarding medical questions.<ref>Andersen: 21</ref> It was moved to [[Ullevål University Hospital]] in [[Oslo]] in 2002.<ref>Andersen: 212</ref> With the opening of the new Ahus in 2008, a new, integrated base was built, allowing the heliport to administratively integrated into the [[emergency department]] of the hospital.<ref name=a128 />

==Facilities==
The heliport is located on the outskirts of Akershus University Hospital, about {{convert|300|m|sp=us}} from the emergency department. The asphalt heliport tarmac measures {{convert|20.55|m|sp=us}}. The facilities include a hangar for two helicopters and a fuel tank.<ref name=landingsforhold>{{cite web |url=http://www.luftambulanse.no/system/files/internett-vedlegg/landingsforhold_ved_sykehus-_rapport_versjon_ad-motet_16_des_2013_komplett.pdf |title=Landingsforhold ved sykehus |publisher=[[Norwegian Air Ambulance]] |format=PDF |language=Norwegian |accessdate=28 November 2014 |page=27}}</ref>  The facility is owned and operated by Innlandet Hospital Trust, part of [[Southern and Eastern Norway Regional Health Authority]]. It features a [[hangar]] and a single asphalt [[helipad]].<ref name=about>{{cite web |url=http://www.luftambulanse.no/baser/l%C3%B8renskog |title=Lørenskog |publisher=[[National Air Ambulance Service of Norway]] |language=Norwegian |accessdate=30 November 2014}}</ref>

The heliport is situated about {{convert|20|km|sp=us}} from downtown Oslo,<ref name=a128 /> where the helicopters can reach [[Ullevål University Hospital]] and [[Rikshospitalet]], neither of which have a base for helicopters.<ref name=landingsforhold />

==Operations==
Lørenskog is the only base in Norway which operates two ambulance helicopters. The ambulance helicopters are operated by Norsk Luftambulanse on contract with the National Air Ambulance Service. They have a [[Eurocopter EC135|Eurocopter EC-135P2+]] and a larger [[Eurocopter EC145|Eurocopter EC145 T2]] stationed at the heliport. The EC-135 has room for a crew of three, consisting of a pilot, rescue swimming and an [[anesthesiologist]], as well as two stretchers. Medical staff are provided by Oslo University Hospital. The helicopters flew 2197 missions lasting 1596 hours in 2013.<ref name=about />

==References==
{{reflist|30em}}

==Bibliography==
{{commons category|Lørenskog Heliport, Ahus}}
* {{cite book |last=Andersen |first=Rune |title=Når det haster |publisher=Orion Forlag |year=2007 |location=Oslo |isbn=978-82-458-0838-4 |language=Norwegian}}

{{Airports in Norway}}
{{Portal bar|Aviation|Medicine|Norway}}

{{DEFAULTSORT:Lorenskog Heliport, Ahus}}
[[Category:Heliports in Norway]]
[[Category:Airports in Akershus]]
[[Category:Lørenskog]]
[[Category:Airports established in 1978]]
[[Category:1978 establishments in Norway]]