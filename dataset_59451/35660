{{Infobox album
| Name         = Above and Beyoncé
| Type         = remix
| Longtype      = / [[video|<span style="background: {{Infobox Album/color|Video}}">video</span>]]
| Artist       = [[Beyoncé]]
| Cover        = Above and B.jpg
| Italic title = force
| Released     = June 16, 2009
| Recorded     = 2008–2009
| Genre        = {{flatlist|
*[[Dance music|Dance]]
*[[electronica]]
*[[Contemporary R&B|R&B]]
}}
| Length       = 51:10 <small>(video collection)</small><br>50:04 <small>(dance mixes)</small>
| Label        = [[Columbia Records|Columbia]]
| Director     = {{flatlist|
*[[Jake Nava]]
*[[Beyoncé|Beyoncé Knowles]]
*[[Sophie Muller]]
*[[Melina Matsoukas]]
*[[Philip Andelman]]
*Ed Burke
}}
| Last album   = ''[[I Am... Sasha Fierce]]''<br />(2008)
| This album   = '''''Above and Beyoncé: Video Collection & Dance Mixes'''''<br />(2009)
| Next album   = ''[[I Am... Yours: An Intimate Performance at Wynn Las Vegas]]''<br />(2009)
| Misc        = {{Extra chronology
| Artist = Beyoncé 
| Type = video
| Last album = ''[[The Beyoncé Experience Live]]''<br />(2007)
| This album = '''''Above and Beyoncé: Video Collection & Dance Mixes'''''<br />(2009)
| Next album   = ''[[I Am... Yours: An Intimate Performance at Wynn Las Vegas]]''<br />(2009)}}
}}
'''''Above and Beyoncé: Video Collection & Dance Mixes''''' is a [[remix album|remix]] and video album by American [[contemporary R&B|R&B]] singer [[Beyoncé]]. Consisting of two discs, the album features [[music video]]s on one disc and dance remixes on the other. It was initially released on June 16, 2009 through [[Wal-Mart]] stores and [[J&R]] exclusively, although it was later made available through other retailers. ''Above and Beyoncé'' peaked at number thirty-five on the [[Billboard 200|''Billboard'' 200]], and was received positively by Andy Kellman of [[Allmusic]], who rated it three out of five stars. It also appeared on [[Billboard charts|''Billboard''{{'s}} component charts]], [[Top R&B/Hip-Hop Albums]] and [[Dance/Electronic Albums]]. The remix version of "Ego" included on the album was nominated for [[Grammy Award for Best Rap/Sung Collaboration|Best Rap/Sung Collaboration]] at the [[52nd Grammy Awards]].

==Background and release==
''Above and Beyoncé'' comprises two discs. The first contains the [[music video]]s of six of the [[single (music)|singles]] from her 2008 studio album, ''[[I Am... Sasha Fierce|I&nbsp;Am... Sasha Fierce]]'': "[[If I Were a Boy]]", "[[Single Ladies (Put a Ring on It)]]", "[[Diva (Beyoncé song)|Diva]]", "[[Halo (Beyoncé song)|Halo]]", [[Broken-Hearted Girl]]" and "[[Ego (Beyoncé song)|Ego]]". It also features a "fan exclusive" cut of the "Ego" video and [[making-of|behind-the-scenes]] footage of the shooting of the videos. The second disc hosts [[electronica]] [[dance music|dance]] [[remix]]es of the songs,<ref>{{cite web|url=http://www.mtv.com/news/articles/1611872/beyonce-knew-perfect-person-remix-ego-kanye-west.jhtml|title=Beyonce Knew The Perfect Person To Remix 'Ego': Kanye West!|publisher=[[MTV News]]. [[MTV Networks]]|first=Jayson|last=Rodriguez|date=May 20, 2009|accessdate=February 24, 2012}}</ref><ref>{{cite web|url=http://www.rap-up.com/2009/05/27/beyonce-goes-above-beyond-with-dance-album/ |title=Beyoncé Goes Above & Beyond with Dance Album |work=[[Rap-Up]] |publisher=Devin Lazerine |first=Georgette |last=Cline |date=May 27, 2010 |accessdate=March 13, 2011}}</ref> as well as of the album's sixth single, "[[Sweet Dreams (Beyoncé song)|Sweet Dreams]]". A mix of "Ego" with a [[rap]] verse from [[Kanye West]] closes the album.
On June 15, 2009, the behind-the-scenes footage was broadcast on [[Black Entertainment Television|BET]]'s ''[[Access Granted]]''.<ref>{{cite episode|series=Access Granted|title=Beyoncé|serieslink=Access Granted|network=[[Black Entertainment Television]]. [[BET Networks]]|airdate=June 15, 2009}}</ref><ref name="bet" /> The remix video for "Ego" premiered afterwards.<ref name="bet">{{cite press release|url=http://www.beyonceonline.com/us/news/ego-remix-video-premieres-special-bet-acces-granted |title='Ego' Remix Video Premieres on Special BET Access Granted! |publisher=Beyoncé's Official Website |date=June 15, 2009 |accessdate=March 11, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20090619083306/http://www.beyonceonline.com:80/us/news/ego-remix-video-premieres-special-bet-acces-granted |archivedate=June 19, 2009 |df= }}</ref>

The [[cover art]] of ''Above and Beyoncé'' was previewed by ''[[People (magazine)|People]]'' magazine in May 2009.<ref>{{cite web|url=http://www.people.com/people/article/0,,20280141,00.html |title=First Look: Beyoncé's New DVD Cover Revealed! |first=Brian |last=Orloff |work=[[People (magazine)|People]] |publisher=[[Time Inc]] |date=May 20, 2009 |archiveurl=https://web.archive.org/web/20090601183254/http://www.people.com/people/article/0,,20280141,00.html |archivedate=June 1, 2009|accessdate=March 11, 2011}}</ref> The album was originally released exclusively to [[Wal-Mart]] stores and [[J&R]], on June 16, 2009.<ref name="PR">{{cite press release|url=http://www.beyonceonline.com/us/news/beyonc%C3%A9-release-above-and-beyonce-video-collection-dance-mixes|title=Beyoncé to Release Above And Beyonce Video Collection & Dance Mixes! |publisher=Beyoncé's Official Website  |date=May 26, 2009 |archiveurl=https://web.archive.org/web/20120314145646/http://www.beyonceonline.com/us/news/beyonc%C3%A9-release-above-and-beyonce-video-collection-dance-mixes |archivedate=March 14, 2012}}</ref> The dance mixes only were made available via [[MP3]] format through [[Amazon.com]] on June 16,<ref name="amazonus">{{cite web|url=http://www.amazon.com/Above-And-Beyonc%C3%A9-Dance-Mixes/dp/B002C3I032 |title=Above And Beyoncé Dance Mixes |publisher=[[Amazon.com]] (US)|accessdate=March 11, 2011}}</ref> and the whole album was released to the online store on November 3, 2009.<ref name="amazonus2">{{cite web|url=http://www.amazon.com/Above-Beyonce-Video-Collection-Dance/dp/B002JIH8U6 |title=Above & Beyonce Video Collection & Dance Mixes |publisher=Amazon.com (US)|accessdate=March 11, 2011}}</ref> The [[iTunes Store]] began selling the remixes on February 1, 2010.<ref name="itunesus">{{cite web| url=http://itunes.apple.com/us/album/above-and-beyonce-dance-mixes/id318741465 |title=Above and Beyoncé&nbsp;– Dance Mixes |publisher=[[iTunes Store]] (US). [[Apple Inc]] |accessdate=March 11, 2011}}</ref>

==Reception==
[[Allmusic]]'s Andy Kellman called the album "a nice set for devoted fans who haven't already shelled out for all the mixes", and awarded it three out of five stars.<ref name="AMGreview">{{cite web |url=http://www.allmusic.com/album/above-and-beyonc-video-collection-dance-mixes-r1619459/review |title=Above and Beyoncé: Video Collection & Dance Mixes |first=Andy |last=Kellman |publisher=[[Allmusic]]. [[Rovi Corporation]] |accessdate=December 14, 2010}}</ref> In 2011, Maura Gavaghan, writing for [[MTV]], said: "The creative title of this video album alone is a reason for every devoted fan... to buy a copy".<ref name="newsroom">{{cite web |url=http://newsroom.mtv.com/2011/10/31/must-have-items-to-be-a-true-beyonce-stan/|title=Must-Have Items To Be A True Beyonce Stan|publisher=[[MTV]]. MTV Networks|first=Maura|last=Gavaghan|date=October 31, 2011|accessdate=February 27, 2012}}</ref> She added that the dance remixes of the songs make a "dance party in a neatly packaged DVD set".<ref name="newsroom" /> ''Above and Beyoncé'' debuted at number thirty-six on the [[Billboard 200|''Billboard'' 200]] chart dated July 4, 2009,<ref>{{cite web|url=http://www.billboard.com/charts/2009-07-04/billboard-200?order=gainer |title=Billboard 200 |date=July 4, 2009 |work=[[Billboard (magazine)|Billboard]] |publisher=[[Prometheus Global Media]] |accessdate=March 11, 2011}}</ref> selling 14,000 copies that week.<ref>{{cite journal|url=https://books.google.com/books?id=Y6Dx4qbQ3dUC&pg=PT52&lpg=PT52#v=onepage |title=Dancing Diva |magazine=Billboard |publisher=Prometheus Global Media |volume=121 |issue=26 |page=53 |date=July 4, 2009 |issn=0006-2510}}</ref> It later peaked at number thirty-five, spending fourteen weeks on the chart.<ref name="charts">{{cite web|url=http://www.allmusic.com/album/above-and-beyonc-video-collection-dance-mixes-r1619459/charts-awards |title=Above and Beyoncé: Video Collection & Dance Mixes&nbsp;– Billboard Albums |publisher=Allmusic. Rovi Corporation |accessdate=March 11, 2011}}</ref><ref>{{cite web|url={{BillboardURLbyName|artist=beyoncé|chart=all}} |title=Above and Beyoncé: Video Collection & Dance Mixes |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011 |archiveurl=https://web.archive.org/web/20110329184310/{{BillboardURLbyName|artist=beyoncé|chart=all}} |archivedate=29 March 2011 |deadurl=no |df= }} ''Note: To interpret the number of weeks in the chart, the user must navigate the visualizer, which can be launched on the left of the screen.''</ref> On the [[Top R&B/Hip-Hop Albums]] chart, ''Above and Beyoncé'' debuted<ref>{{cite web|url=http://www.billboard.com/charts/2009-07-04/r-b-hip-hop-albums?order=gainer |title=R&B/Hip-Hop Albums |date=July 4, 2009 |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011| archiveurl= https://web.archive.org/web/20110330121530/http://www.billboard.com/charts/2009-07-04/r-b-hip-hop-albums?order=gainer| archivedate= 30 March 2011 <!--DASHBot-->| deadurl= no}}</ref> and peaked at number twenty-three, lasting forty-three weeks in the chart.<ref>{{cite web|url={{BillboardURLbyName|artist=beyoncé|chart=R&B/Hip-Hop Albums B}} |title=Beyoncé: Chart History |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011}}</ref> Among the [[Dance/Electronic Albums]] chart, the album debuted at number two, placing below [[Lady Gaga]]'s ''[[The Fame]]''.<ref>{{cite web|url=http://www.billboard.com/charts/2009-07-04/dance-electronic-albums |title=Dance/Electronic Albums |date=July 4, 2009 |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011| archiveurl= https://web.archive.org/web/20110329231058/http://www.billboard.com/charts/2009-07-04/dance-electronic-albums| archivedate= 29 March 2011 <!--DASHBot-->| deadurl= no}}</ref> It spent a total of forty-eight weeks on the component chart, including twenty-four weeks in the top ten.<ref>{{cite web|url={{BillboardURLbyName|artist=beyoncé|chart=R&B/Hip-Hop Albums B}} |title=Beyoncé: Chart History |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011}} ''Note: To interpret the number of weeks in the chart's top ten, the user must navigate the visualizer, which can be launched on the left of the screen.''</ref>
''Above and Beyoncé'' was ranked at number nine on the year-end Dance/Electronic Albums Chart for 2009,<ref name="2009DE">{{cite web|url=http://www.billboard.biz/bbbiz/charts/yearendcharts/2009/dance-electronic-albums |title=Dance/Electronic Albums |year=2009 |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011}}</ref> and number twenty-one on the 2010 chart.<ref name="2010DE">{{cite web|url=http://www.billboard.biz/bbbiz/charts/yearendcharts/2010/dance-electronic-albums |title=Dance/Electronic Albums |year=2010 |work=Billboard |publisher=Prometheus Global Media |accessdate=March 11, 2011}}</ref>

==Track listing==
{{tracklist
| writing_credits = yes
| extra_column    = Director(s)<ref name="Above" />
| headline        = Disc one: Video Collection

| title1          = [[If I Were a Boy]]
| writer1         = [[Toby Gad]], [[BC Jean]]
| extra1          = [[Jake Nava]]
| length1         = 5:06

| title2          = [[Single Ladies (Put a Ring on It)]]
| writer2         = [[Tricky Stewart]], [[The-Dream]], [[Kuk Harrell]], [[Beyoncé]]
| extra2          = Jake Nava
| length2         = 3:19

| title3          = [[Diva (Beyoncé song)|Diva]]
| writer3         = Beyoncé, [[Bangladesh (producer)|Bangladesh]], [[Sean Garrett]]
| extra3          = [[Melina Matsoukas]]
| length3         = 4:06

| title4          = [[Halo (Beyoncé song)|Halo]]
| writer4         = Beyoncé, [[Ryan Tedder]], [[E. Kidd Bogart]]
| extra4          = Philip Andelman
| length4         = 3:45

| title5          = [[Broken-Hearted Girl]]
| writer5         = [[Babyface (musician)|Babyface]], [[Stargate (production team)|StarGate]], Beyoncé
| extra5          = [[Sophie Muller]]
| length5         = 4:40

| title6          = [[Ego (Beyoncé song)|Ego]]
| note6           = Remix featuring [[Kanye West]]
| writer6         = [[Blac Elvis]], Harold Lilly, Beyoncé
| extra6          = Beyoncé, Frank Gatson Jr.
| length6         = 4:53

| title7          = Ego
| note7           = Fan exclusive
| writer7         = Williams, Lilly, Beyoncé
| extra7          = Beyoncé, Frank Gatson Jr.
| length7         = 3:57

| title8          = Behind the Scenes: The Videos
| writer8         = 
| extra8          = Ed Burke
| length8         = 19:02
}}
{{tracklist
| writing_credits = yes
| extra_column    = Producer(s)<ref name="Above" /><ref name="iam">{{cite AV media notes |title=I Am... Sasha Fierce |titlelink=I Am... Sasha Fierce |others=Beyoncé |id=0088697417352 |year=2008 |type=Platinum Edition |publisher=Columbia Records}}</ref>
| headline        = Disc two: Dance Mixes

| title1          = If I Were a Boy
| note1           = Maurice Joshua Mojo UK Remix
| writer1         = Gad, Jean
| extra1          = Toby Gad, Beyoncé, [[Maurice&nbsp;Joshua]]*
| length1         = 6:29

| title2          = Single Ladies (Put a Ring on It)
| note2           = DJ Escape & Tony Coluccio Remix – Club Version
| writer2         = Tricky Stewart, The-Dream, Harrell, Beyoncé
| extra2          = Tricky Stewart, The-Dream, Beyoncé, DJ Escape*, Tony Coluccio*
| length2         = 6:57

| title3          = Diva
| note3           = Karmatronic Club Remix
| writer3         = Beyoncé, Bangladesh, Garrett
| extra3          = Bangladesh, Sean Garrett, Beyoncé, Achilles Sparta*, Peter Krajezar*
| length3         = 5:08

| title4          = Halo
| note4           = Dave Audé Club Remix
| writer4         = Beyoncé, Tedder, E. Kidd Bogart
| extra4          = Ryan Tedder, Beyoncé, [[Dave&nbsp;Audé]]*
| length4         = 8:55

| title5          = Broken-Hearted Girl
| note5           = Catalyst Remix
| writer5         = BabyFace, StarGate, Beyoncé
| extra5          = StarGate, Beyoncé, James&nbsp;Cruz*
| length5         = 4:46

| title6          = Ego
| note6           = OK DAC Remix
| writer6         = Blac Elvis, Lilly, Beyoncé
| extra6          = Blac Elvis, Harold Lilly, Beyoncé, OK DAC*
| length6         = 6:29

| title7          = [[Sweet Dreams (Beyoncé song)|Sweet Dreams]]
| note7           = Harlan Pepper & AG III Remix
| writer7         = Beyoncé, [[Jim Jonsin]], [[Wayne Wilkins]], [[Rico Love]]
| extra7          = Jim Jonsin, Wayne Wilkins, Rico Love, Beyoncé, Peterson & Alan Gordan*
| length7         = 6:43

| title8          = Ego
| note8           = Remix featuring Kanye West
| writer8         = Blac Elvis, Lilly, Beyoncé
| extra8          = Blac Elvis, Harold Lilly, Beyoncé
| length8         = 4:44
}}

===Digital download version EP===

{{tracklist
| collapsed = yes
| writing_credits = yes
| extra_column    = Producer(s)<ref name="Above" /><ref name="iam">{{cite AV media notes |title=I Am... Sasha Fierce |titlelink=I Am... Sasha Fierce |others=Beyoncé |id=0088697417352 |year=2008 |type=Platinum Edition |publisher=Columbia Records}}</ref>
| headline        = Above and Beyoncé Dance Mixes

| title1          = If I Were a Boy
| note1           = Maurice Joshua Mojo UK Remix
| writer1         = Gad, Jean
| extra1          = Toby Gad, Beyoncé, Maurice Joshua*
| length1         = 6:29

| title2          = Single Ladies (Put a Ring on It)
| note2           = DJ Escape & Tony Coluccio Remix – Club Version
| writer2         = Tricky Stewart, The-Dream, Harrell, Beyoncé
| extra2          = Tricky Stewart, The-Dream, Beyoncé, DJ Escape*, Tony Coluccio*
| length2         = 6:57

| title3          = Diva
| note3           = Karmatronic Club Remix
| writer3         = Beyoncé, Bangladesh, Garrett
| extra3          = Bangladesh, Sean Garrett, Beyoncé, Achilles Sparta*, Peter Krajezar*
| length3         = 5:08

| title4          = Halo
| note4           = Dave Audé Club Remix
| writer4         = Beyoncé, Tedder, E. Kidd Bogart
| extra4          = Ryan Tedder, Beyoncé, Dave Audé*
| length4         = 8:55

| title5          = Broken-Hearted Girl
| note5           = Catalyst Remix
| writer5         = Babyface, StarGate, Beyoncé
| extra5          = StarGate, Beyoncé, James&nbsp;Cruz*
| length5         = 4:46

| title6          = Ego
| note6           = OK DAC Remix
| writer6         = Blac Elvis, Lilly, Beyoncé
| extra6          = Blac Elvis, Harold Lilly, Beyoncé, OK DAC*
| length6         = 6:29

| title7          = Sweet Dreams
| note7           = Harlan Pepper & AG III Remix
| writer7         = Beyoncé, Jim Jonsin, Wayne Wilkins, Rico Love
| extra7          = Jim Jonsin, Wayne Wilkins, Rico Love, Beyoncé, Peterson & Alan Gordan*
| length7         = 6:43

| title8          = Ego
| note8           = Remix featuring Kanye West
| writer8         = Blac Elvis, Lilly, Beyoncé
| extra8          = Blac Elvis, Harold Lilly, Beyoncé
| length8         = 4:44
}}
<small>(*) denotes remix and additional production</small>

==Personnel==
Credits for ''Above and Beyoncé'' are adapted from [[Allmusic]] and CD's [[liner notes]].<ref name="Above">{{cite AV media notes |title=Above and Beyoncé: Video Collection & Dance Mixes |others=[[Beyoncé]] |id=88697 53949 9 |year=2009 |type=Remix album |publisher=[[Columbia Records]]}}</ref><ref>{{cite web|url=http://www.allmusic.com/album/above-and-beyonc-video-collection-dance-mixes-r1619459/credits |title=Above and Beyoncé: Video Collection & Dance Mixes&nbsp;– Credits |publisher=Allmusic. Rovi Corporation |accessdate=March 11, 2011}}</ref>
{{Div col|cols=3}}
*Kory Aaron&nbsp;– assistance
*Damien Alexander&nbsp;– [[artists and repertoire]]
*Phillip Andelman&nbsp;– direction
*[[Dave Audé]]&nbsp;– [[record producer|production]], [[remixing]]
*[[Babyface (musician)|Babyface]]&nbsp;– writing
*Christian Baker&nbsp;– assistance
*[[Bangladesh (producer)|Bangladesh]]&nbsp;– writing
*Tim Blacksmith&nbsp;– management
*[[E. Kidd Bogart]]&nbsp;– writing
*Ed Burke&nbsp;– direction
*Domenic Capello&nbsp;– [[audio mixing (recorded music)|mixing]]
*Jim Caruana&nbsp;– [[audio engineer|engineering]], vocal engineering
*Fusako Chubachi&nbsp;– art direction
*Tony Coluccio&nbsp;– remixing
*Tom Coyne&nbsp;– [[audio mastering|mastering]]
*James Cruz&nbsp;– mastering
*Kim Dellara&nbsp;– [[executive producer|executive production]]
*DJ Escape&nbsp;– remixing
*[[The-Dream]]&nbsp;– writing
*[[Mikkel Storleer Eriksen]]&nbsp;– engineering, [[musical instrument|instrumentation]], writing
*Jens Gad&nbsp;– [[drum]]s
*[[Toby Gad]]&nbsp;– [[arrangement]], engineering, production, instrumentation, writing
*Tim Gant&nbsp;– [[keyboard instrument|keyboards]]
*[[Sean Garrett]]&nbsp;– writing
*Alan Gordon&nbsp;– engineering
*Matt Green&nbsp;– assistance
*[[Kuk Harrell]]&nbsp;– engineering, writing
*Matt Hennessy&nbsp;– engineering
*Ty Hunter&nbsp;– stylist
*[[Jim Jonsin]]&nbsp;– production
*[[Maurice Joshua]]&nbsp;– remixing
*Grant Jue&nbsp;– production
*Chris Kantrowitz&nbsp;– executive production
*Anthony Kilhoffer&nbsp;– vocal engineering
*Kimberly Kimble&nbsp;– hair stylist
*Juli Knapp&nbsp;– artists and repertoire
*[[Beyoncé]]&nbsp;– executive production, production, writing, [[singing|vocals]] vocal production
*[[Mathew Knowles]]&nbsp;– executive production
*[[Tina Knowles]]&nbsp;– creative consultant
*Melissa Larsen&nbsp;– production
*Harold Lilly&nbsp;– production, writing
*Peter Lindbergh&nbsp;– [[photography]]
*[[Rico Love]]&nbsp;– producer, writing, vocals, vocal production
*Philip Margiziotis&nbsp;– [[French horn|horn]]
*[[Melina Matsoukas]]&nbsp;– direction
*Jake McKim&nbsp;– artists and repertoire
*Michael Paul Miller&nbsp;– assistance
*[[Sophie Muller]]&nbsp;– director
*[[Jake Nava]]&nbsp;– director
*Jeff Pantaleo&nbsp;– executive production
*[[Dave Pensado]]&nbsp;– mixing
*[[Jim Jonsin]]&nbsp;– writing
*Hagai Shaham&nbsp;– production
*[[Mark "Spike" Stent]]&nbsp;– mixing
*[[Tricky Stewart]]&nbsp;– writing
*[[Ryan Tedder]]&nbsp;– arrangement, engineering, instrumentation, production, writing
*Francesca Tolot&nbsp;– [[make-up]]
*Lidell Townsell&nbsp;– keyboards
*Jennifer Turner&nbsp;– marketing
*Randy Urbanski&nbsp;– assistance
*Miles Walker&nbsp;– engineering
*Wayne Wilkins&nbsp;– production, writing
*[[Blac Elvis]]&nbsp;– writing
*Dontae Winslow&nbsp;– [[trumpet]]
*John Winter&nbsp;– production
*Andrew Wuepper&nbsp;– assistance
{{Div col end}}

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{| class="wikitable sortable"
! Chart (2009)
! Peak<br>position
|-
|US [[Billboard 200|''Billboard'' 200]]<ref name="charts"/>
|align="center"|35
|-
|US [[Dance/Electronic Albums]]<ref name="charts"/>
|align="center"|2
|-
|US [[Top R&B/Hip-Hop Albums]]<ref name="charts"/>
|align="center"|23
|}
{{col-2}}

===Year-end charts===
{| class="wikitable"
! Chart
! Position
|-
|US Dance/Electronic Albums (2009)<ref name="2009DE"/>
|align="center"|9
|-
|US Dance/Electronic Albums (2010)<ref name="2010DE"/>
|align="center"|21
|}
{{col-end}}

==Release history==
{| class="wikitable"
|-
! Country
! Date
! Format
! Label
|-
|United States<ref name="amazonus" />
|rowspan="3"|June 16, 2009
|[[Music download|Digital download]] (dance mixes only)
|rowspan="2"|[[Columbia Records]]
|-
|United States<ref group="nb">Only at Wal-Mart and J&R Music New York</ref><ref name="PR"/>
|rowspan="7"|[[Compact Disc|CD]]/[[DVD]]
|-
|Japan<ref>{{cite web|url=http://www.amazon.co.jp/Above-Beyonce-Video-Collection-Dance/dp/B002DYAU74|title=Above and Beyonce: Video Collection & Dance Mixes|publisher=Amazon.com (Japan)|language=Japanese|accessdate=February 27, 2012}}</ref><ref>{{cite web|url=http://www.hmv.co.jp/en/product/detail/3602055|title=Above And Beyonce Video Collection & Dance Mixes|publisher=[[HMV Group]] (Japan)|language=Japanese|accessdate=February 27, 2012}}</ref>
|[[Sony Music Entertainment Japan|Sony Music Japan]]
|-
|Germany<ref>{{cite web|url=http://www.amazon.de/Above-Beyonce-Video-Collection-Dance/dp/B002DYAU74/ref=sr_1_1?ie=UTF8&qid=1330358737&sr=8-1|title=Above And Beyonce Video Collection & Dance Mixes|publisher=Amazon.com (Germany)|language=German|accessdate=February 27, 2012}}</ref><ref>{{cite web|url=http://itunes.apple.com/de/album/above-and-beyonce-dance-mixes/id318741465|title=Above and Beyoncé - Dance Mixes|publisher=iTunes Store (Germany). Apple Inc|language=German|accessdate=February 27, 2012}}</ref>
|June 18, 2009
|rowspan="3"|[[Sony Music Entertainment|Sony Music]]
|-
|Canada<ref>{{cite web|url=http://www.amazon.ca/Above-Beyonce-Video-Collection-Dance/dp/B002DYAU74/ref=sr_1_1?s=music&ie=UTF8&qid=1330362065&sr=1-1|title=Above & Beyonce Video Collection & Dance Mixes|publisher=Amazon.com (Canada)|accessdate=February 27, 2012}}</ref>
|August 18, 2009
|-
|Canada<ref>{{cite web|url=http://www.amazon.ca/Above-Beyonce-Video-Collection-Dance/dp/B002JIH8U6/ref=sr_1_2?s=music&ie=UTF8&qid=1330360793&sr=1-2|title=Above & Beyonce Video Collection & Dance Mixes|publisher=Amazon.com (Canada)|accessdate=February 27, 2012}}</ref>
|rowspan="3"|November 3, 2009
|-
|United Kingdom<ref>{{cite web|url=http://www.amazon.co.uk/Above-Beyonce-Video-Collection-Dance/dp/B002DYAU74/ref=sr_1_1?ie=UTF8&qid=1330359485&sr=8-1|title=Above & Beyonce Video Collection & Dance Mixes|publisher=Amazon.com (UK)|accessdate=February 27, 2012}}</ref><ref>{{cite web|url=http://www.amazon.co.uk/Above-Beyonce-Video-Collection-Dance/dp/B002JIH8U6/ref=sr_1_2?ie=UTF8&qid=1330359485&sr=8-2|title=Above & Beyonce Video Collection & Dance Mixes|publisher=Amazon.com (UK)|accessdate=February 27, 2012}}</ref>
|[[RCA Records]]
|-
|United States<ref group="nb">Available to all retailers</ref><ref name="amazonus2" />
|Columbia Records
|-
|Austria<ref>{{cite web|url=http://itunes.apple.com/at/album/above-and-beyonce-dance-mixes/id318741465|title=Above and Beyoncé - Dance Mixes|publisher=iTunes Store (Austria). Apple Inc|language=German|accessdate=February 27, 2012}}</ref>
|rowspan="4"|February 1, 2010
|rowspan="4"|Digital download (dance mixes only)
|rowspan="3"|Sony Music
|-
|Canada<ref>{{cite web|url=http://itunes.apple.com/ca/album/above-and-beyonce-dance-mixes/id318741465|title=Above and Beyoncé - Dance Mixes|publisher=iTunes Store (Canada). Apple Inc|accessdate=February 27, 2012}}</ref>
|-
|Switzerland<ref>{{cite web|url=http://itunes.apple.com/ch/album/above-and-beyonce-dance-mixes/id318741465|title=Above and Beyoncé - Dance Mixes|publisher=iTunes Store (Switzerland). Apple Inc|language=Swiss|accessdate=February 27, 2012}}</ref>
|-
|United States<ref name="itunesus" />
|Columbia Records
|}

==Notes==
{{Portal|Music|R&B and Soul Music}}
{{Reflist|group=nb}}

==References==
{{reflist|30em}}

{{Wikipedia books|I Am... Sasha Fierce}}

{{Beyoncé}}
{{Good article}}

{{DEFAULTSORT:Above and Beyonce: Video Collection and Dance Mixes}}
[[Category:Beyoncé video albums]]
[[Category:2009 video albums]]
[[Category:Music video compilation albums]]
[[Category:2009 remix albums]]
[[Category:Remix EPs]]
[[Category:Columbia Records remix albums]]
[[Category:Columbia Records video albums]]
[[Category:English-language remix albums]]
[[Category:English-language video albums]]