{{Infobox scientist
| name              = Alexander Haddow
| image             =
| image_size        = 
| alt               = 
| caption           = 
| birth_name        = Alexander John Haddow
| birth_date        = {{Birth date|df=yes|1912|12|27}}
| birth_place       = Glasgow, Scotland
| death_date        = {{Death date and age|df=yes|1978|12|26|1912|12|27}}
| death_place       = Glasgow, Scotland
| nationality       = British
| fields            = Medical Entomology,<br>Administrative Medicine
| workplaces        = [[Uganda Virus Research Institute]], [[University of Glasgow]]
| alma_mater        = [[University of Glasgow]]
| thesis_title      = 
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = 
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         = Zika Virus, Yellow Fever, Burkitt's Lymphoma
| awards            = Chalmers Medal, [[Royal Society of Tropical Medicine and Hygiene]] {{small|(1957)}}<br>Bellahouston Gold Medal, University of Glasgow {{small|(1961)}}<br>[[Keith Medal]] {{small|(1968)}}
| spouse            = Margaret Ronald Scott Orr
| children          = David Lindsay Haddow,<br>Alastair Douglas Haddow
}}

Prof '''Alexander John Haddow''' CMG [[Royal Society of London|FRS]] [[FRSE]] FRCPSG FRES (27 December 1912 – 26 December 1978).<ref name=UoG_Story>{{cite web|url=http://www.universitystory.gla.ac.uk/biography/?id=WH2116&type=P&o=&start=0&max=20&l | title=University of Glasgow Story: Alec Haddow |publisher=University of Glasgow |accessdate=29 April 2016}}</ref> [[Fellow of the Royal Society|FRS]] [[Order of St Michael and St George|CMG]] was a Scottish entomologist recognised for his work at the [[Uganda Virus Research Institute]], including the discovery of the [[Zika virus]], and research into the insect vectors of the [[yellow fever]] virus. Other notable work included relating the incidence of [[Burkitt’s lymphoma]] to climatic conditions<ref name=Burkitt_01>{{cite journal|title=Epidemiology of Burkitt’s Lymphoma|year=1971|last1=Burkitt|first1=D. P.|journal=Proceedings of the Royal Society of Medicine|volume=64|pages=909–910}}</ref> and the discovery of several previously unknown viruses in east Africa, particularly [[arbovirus]]es.

Haddow spent most of his research career in Uganda, where he pioneered a method for studying the prevalence and habits of biting insects (particularly mosquitoes) known as the 24-hour catch.<ref name=Mosquito_Field_Ecology>{{cite book|title=Mosquito Ecology Field Sampling Methods|edition=3|last1=Silver|first1=J. B.|publisher=Springer|page=503}}</ref> In 1953, Haddow was appointed the Director of the Institute and he remained in this position until his return to the University of Glasgow in 1965, where he took up largely administrative posts for the remainder of his career.

==Early life==
Alexander John Haddow was born in Glasgow in 1912, the son of Alexander and Margaret (née Whyte); he was one of two children along with his younger sister Marion. Haddow reportedly showed a strong interest in insects from an early age.<ref name=Garnham>{{cite journal|title=Alexander John Haddow. 27 December 1912 - 26 December 1978.|year=1980|last1=Garnham|first1=P. C. C.|journal=Biographical Memoirs of Fellows of the Royal Society|volume=26|pages=225–254|doi=10.1098/rsbm.1980.0006}}</ref> He attended Hillhead High School in Glasgow, followed by the University of Glasgow where he attained first class Honours in Zoology in 1934. He went on to obtain his medical degree there in 1938, followed by a DSc in 1957<ref name=DSc>{{cite thesis |last=Haddow |first=Alexander J. |date=1957 |title= Studies on the natural history of yellow fever in East Africa, with notes on other insect-borne infections |type=D.Sc. |chapter= |publisher=University of Glasgow |docket= |oclc= |url= http://encore.lib.gla.ac.uk/iii/encore/record/C__Rb1630787?lang=eng |access-date=8 June 2016}}</ref> and MD in 1961.<ref name=MD>{{cite thesis |last=Haddow |first=Alexander J.|date=1961 |title= Circadian rhythms in the biting diptera : a factor in the transmission of insect-borne disease |type=M.D. |chapter= |publisher=University of Glasgow |docket= |oclc= |url= http://encore.lib.gla.ac.uk/iii/encore/record/C__Rb1631233?lang=eng |access-date=8 June 2016}}</ref>

==Career==
Haddow first began his research into tropical diseases in 1941 in Kisumu, Kenya, where he studied the prevalence and habits of mosquito species in local huts,<ref name=Haddow_01>{{cite journal|title=The Mosquito Fauna and Climate of native Huts at Kisumu, Kenya|year=1942|last1=Haddow|first1=A. J.|journal=Bulletin of Entomological Research|volume=33|pages=91–142|doi=10.1017/s0007485300026389}}</ref> becoming the first researcher to build controlled experimental huts according to specification.<ref name=Mosquito_Field_Ecology_2>{{cite book|title=Mosquito Ecology Field Sampling Methods|edition=3|last1=Silver|first1=J. B.|publisher=Springer|page=1425}}</ref> In 1942, Haddow joined the Virus Research Institute at Entebbe, Uganda (now known as the Uganda Virus Research Institute) as a medical entomologist. The main research focus was on yellow fever but the Institute was also concerned with previously undiscovered diseases, particularly arboviruses. Here, Haddow’s work mainly involved catching and documenting biting insects over full 24-hour days in and near the jungle. In order to study the biting habits of mosquitoes at varying levels in the trees, platforms were built in the forest canopy and understory. Caught mosquitoes were typically separated over one-hour intervals, allowing Haddow to identify the biting cycles and vertical distributions of different species.<ref name=Haddow_02>{{cite journal| doi=10.1017/S0007485300026900|title= Studies of the Biting-Habits of African Mosquitos. An Appraisal of Methods Employed, With Special Reference to the Twenty-Four-Hour Catch |year=1954|last1=Haddow|first1=A. J.|journal=Bulletin of Entomological Research|volume=45|pages=199–242}}</ref> The first isolation of the Zika virus from mosquitoes was made in 1948 from one of Haddow's catches of ''[[Aedes africanus]]'' in the Zika forest<ref name=Dick_01>{{cite journal|title=Zika Virus (I) Isolations and Serological Specificity|year=1952|last1=Dick|first1=G. W. A.|last2=Kitchen|first2=S. F.|last3=Haddow|first3=A. J.|journal=Transactions of the Royal Society of Tropical Medicine and Hygiene|volume=46|issue=5|pages=509–520|doi=10.1016/0035-9203(52)90042-4|pmid=12995440}}</ref>

In 1953, Haddow was promoted to Director of the Institute. Under his leadership, the Institute continued to research yellow fever and to attempt to discover and document emerging viruses affecting the local population. Other notable viruses that were first documented by Institute researchers during this time were: [[O'nyong'nyong]], [[Chikungunya]], [[Semliki Forest Virus|Semliki Forest]], [[Bunyamwera virus|Bunyamwera]] and [[West Nile Virus|West Nile]].

In 1961, a steel tower was erected in the Zika forest to allow mosquitoes to be caught simultaneously at six different levels in and above the forest, giving more detailed information on the vertical migrations of mosquito species. Known as ‘Haddow’s Tower’, the structure is still used for mosquito research in the Zika forest<ref name=CNN>{{cite web|url=http://edition.cnn.com/2016/02/02/health/zika-forest-viral-birthplace/|title=Zika virus birthplace: Uganda’s Zika Forest|publisher=CNN|accessdate=2 May 2016}}</ref>

During his lifetime, Haddow’s most prominent work was considered to be that on yellow fever and Burkitt's lymphoma.<ref name=UoG_Story /> The Zika virus has since emerged as an important human disease, but it was not considered so until very recently.<ref name=CNN /> Haddow was awarded the Chalmers Medal for Tropical Medicine and Hygiene in 1953, and elected to the Royal Society of London in 1972.<ref name=Garnham /> After he left Uganda, he returned to Glasgow where he was made a Professor of Administrative Medicine at the University, before becoming Dean of the Faculty of Medicine in 1970.

==Personal life==
Haddow married Margaret “Peggy” Orr in Mombasa, Kenya, in 1946. They had two sons, David and Alastair. Haddow had a keen interest in traditional Highland Bagpipe music – he was a prominent member of the Glasgow [[Piobaireachd]] Society<ref name=UoG_Library>{{cite web|url=https://universityofglasgowlibrary.wordpress.com/2013/08/30/this-week-in-the-archive-services-searchroom/|title=This week in the archive services searchroom…|publisher=The University of Glasgow Library|accessdate=2 May 2016}}</ref> and wrote a book on the subject, published poshumously, titled ''"The History and Structure of Ceol Mor"''. The University of Glasgow Archive Services holds a collection of Haddow’s documents including research materials from his time at the Virus Research Institute and records relating to his studies into Piobaireachd Music.<ref name=Archives_Hub>{{cite web|url= http://archiveshub.ac.uk/data/gb248-dc/068| title= Papers of Alexander John Haddow|publisher=Archives Hub|accessdate=8 June 2016}}</ref> 
The [[Hunterian Museum and Art Gallery|University of Glasgow's Hunterian Museum]] also holds numerous artefacts donated by Haddow that represent his varied interests, including sketches of monkeys, game heads, ethnographic items from Uganda and archaeological finds from the Antonine Wall.<ref name=Hunterian>{{cite web|url=http://www.huntsearch.gla.ac.uk/cgi-bin/foxweb/huntsearch/SummaryResults.fwx?collection=all&Searchterm=haddow| title=Hunterian Museum Search Results |publisher=Hunterian Museum and Art Gallery, University of Glasgow |accessdate=29 April 2016}}</ref> Haddow died on December 26, 1978.

==References==
{{reflist|30em}}
{{authority control}}

{{DEFAULTSORT:Haddow, Alexander John}}
[[Category:1912 births]]
[[Category:1978 deaths]]
[[Category:Scottish entomologists]]
[[Category:Companions of the Order of St Michael and St George]]
[[Category:Fellows of the Royal Society]]
[[Category:People from Glasgow]]
[[Category:Alumni of the University of Glasgow]]