{{EngvarB|date=December 2013}}
{{Use dmy dates|date=December 2013}}
{{Infobox musical artist
| name = England
| background = group_or_band
| years_active = 1975–1978, 2006
| label = [[Arista Records]]
| origin = [[Maidstone]], [[Kent]], [[England]]
| past_members = Martin Henderson<br/>Frank Holland<br/>Jode Leigh<br/>Robert Webb<br/>Mark Ibbotson<br/>Jaffa Peckham<br/>[[Jamie Moses]]
}}

'''England''' were a [[progressive rock]] group active in the late 1970s, and briefly reformed in 2006. The band is notable for their album ''Garden Shed'' released on [[Arista Records]], and for keyboardist Robert Webb playing a [[Mellotron]] sawn in half.<ref>{{Cite web|title = The Restoration Of The Mellotron - Half’A’Tron|url = http://www.soundonsound.com/sos/mar15/articles/half-a-tron.htm|website = www.soundonsound.com|accessdate = 2015-10-04}}</ref>

== Biography ==

The band was formed in 1975 by drummer Mark Ibbotson, and, after a variety of prototype groups, stabilised around a line-up of himself, bassist Martin Henderson, guitarist [[Jamie Moses]] and keyboardist Robert Webb.<ref name="background"/> Moses and Webb had previously collaborated on an unreleased album.{{sfn|Awde|2008|p=358}} Ibbottson owned a Mk II [[Mellotron]], which Webb took an interest in, later saying "it opens up possibilities. It's having control like the conductor of an orchestra."{{sfn|Awde|2008|p=361}} To make the instrument portable, Webb sawed the instrument in half, rehousing the left-hand manual and the right-hand tapes (which contained the lead sounds on a Mk II model) it in a new case.{{sfn|Awde|2008|p=363}} After a number of gigs at the [[Hazlitt Theatre]] in [[Maidstone]], Moses quit and was replaced by Frank Holland. In March 1976, immediately following a showcase gig that resulted in a contract with [[Arista Records]], Ibbotson quit the band and was replaced by Jode Leigh.<ref name="background"/>

The band spent most of 1976 rehearsing and recording material for Arista. The single "Paraffinalea"  was released in February 1977. [[Anne Nightingale]] gave a positive review of the single, saying the band was "destined for great things."<ref>{{cite news|url=http://www.gardenshedmusic.com/what_they_said.cfm|first=Anne|last=Nightingale|authorlink=Anne Nightingale|title=Paraffinalea (review)|work=[[Daily Express]]|date=22 February 1977|accessdate=2 September 2003}}</ref> This was followed by the album ''Garden Shed'', but by this time, [[punk rock]] was popular, and this, combined with a general lack of interest in promoting the band, meant that it was a commercial failure. [[Melody Maker]] described the album as "[[Yes (band)|Yes]] in toyland". The band split from their management and moved into a music shop in Hastings{{sfn|Awde|2008|p=365}} but the financial situation didn't improve and the group split in autumn 1978.<ref name="background"/>

A resurrected version of the band, featuring Mark Ibbotson and (apparently) Frank Holland, but without Robert Webb, appeared in 1983-84 long enough to record two singles for [[Jet Records]] (best known for their involvement with ELO's early career). Little is known of this incarnation of the group beyond what can be gleaned from the singles themselves.

Henderson later became touring bassist for [[Jeff Beck]],<ref name="background"/> while Moses later became a touring guitarist for [[Queen + Paul Rodgers]].{{sfn|Awde|2008|p=358}} Ibbotson became [[The Pretty Things]]' manager in 1985, while Holland joined that band as a guitarist,<ref>{{cite journal|url=https://books.google.com/books?id=6A0EAAAAMBAJ&pg=PA15&dq=pretty+things+frank+holland#v=onepage&q=pretty%20things%20frank%20holland&f=false|title=Pretty Things Rage on in New Set|publisher=[[Billboard (magazine)|Billboard]]|date=13 March 1999|page=15|accessdate=2 September 2013}}</ref> positions they retain to this day. The band continue to receive critical praise, with Music Mart magazine describing a CD reissue of ''Garden Shed'' "as good as prog could get in the 70s".<ref>{{cite journal|url=http://www.gardenshedmusic.com/what_they_said.cfm|first=Dave|last=Etheridge|title=Garden Shed (review)|publisher=Music Mart|date=September 2005|accessdate=2 September 2013}}</ref> Webb and Henderson reactivated the band in 2006 for a brief tour,<ref name="background">{{cite web|url=http://www.backgroundmagazine.nl/QLinks/InterviewEngland.html|title=Nostalgia: 'Garden Shed' by England|first=Henri|last=Strik|publisher=Background Magazine|year=2005|accessdate=2 September 2013}}</ref> including an appearance at the Baja Prog Fest in [[Mexicali]].{{sfn|Awde|2008|p=372}}

== Discography ==
* ''The Imperial Hotel (EP)'' (1975) 
* ''Garden Shed'' (1977)
* ''Victoriana'' b/w ''Hearts Made Of Glass'' (Jet Records single 1983)
* ''London Story'' b/w ''Under the Pier'' (Jet Records single 1984)
* ''The Last of the Jubblies'' (1996)
* ''Live in Japan: Kikimimi'' (2006)<ref>{{cite web|url=http://www.allmusic.com/artist/england-mn0000200303|title=England : Discography|publisher=Allmusic|accessdate=2 September 2013}}</ref>

== References ==

{{reflist}}
{{refbegin}}
* {{cite book
|first=Nick
|last=Awde
|title=Mellotron : The Machines and the Musicians that Revolutionised Rock
|publisher=Bennett & Bloom
|year=2008
|isbn=978-1-898948-02-5
|ref=harv
}}
{{refend}}

== External links ==
* {{Official website|http://www.gardenshedmusic.com/}}

{{Authority control}}

[[Category:English progressive rock groups]]