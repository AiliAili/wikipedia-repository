{{Infobox court case |
name=Australian Competition and Consumer Commission v Baxter Healthcare Pty Ltd |
court=[[High Court of Australia]] |
image=Coat of Arms of Australia.svg |
date decided=29 August 2007 |
full name=Australian Competition and Consumer Commission v Baxter Healthcare Pty Limited & Ors |
citations={{Cite AustLII|HCA|38|2007}} |
transcripts={{Cite AustLII|HCATrans|202|2007}} |
judges=[[Murray Gleeson|Gleeson]] CJ, [[William Gummow|Gummow]], [[Michael Kirby (judge)|Kirby]], [[Kenneth Hayne|Hayne]], [[Ian Callinan|Callinan]], [[Dyson Heydon|Heydon]] and [[Susan Crennan|Crennan]] JJ |
prior actions=''Australian Competition and Consumer Commission v Baxter Healthcare'' {{Cite AustLII|FCA|581|2005}}; ''Australian Competition and Consumer Commission v Baxter Healthcare'' {{Cite AustLII|FCAFC|128|2006}} |
subsequent actions=''Australian Competition and Consumer Commission v Baxter Healthcare'' {{Cite AustLII|FCAFC|141|2008}} |
opinions=
'''(5:1:1)''' The immunity of governments from the restrictive trade practices provisions of the ''[[Trade Practices Act 1974]]'' does not extend to trading corporations that provide goods or services to the government <small>(per Gleeson CJ, Gummow, Hayne, Heydon and Crennan JJ; Callinan J dissenting and Kirby J deciding on different grounds)</small><br />
'''(5:1:1)''' Derivative governmental immunity covers persons other than the government where applying the statute to those persons would have the effect of divesting the government of proprietary, contractual, or other legal rights or interests <small>(per Gleeson CJ, Gummow, Hayne, Heydon and Crennan JJ; Callinan J dissenting and Kirby J deciding on different grounds)</small>|
}}

'''''Australian Competition and Consumer Commission v Baxter Healthcare Pty Ltd''''' (''Baxter'') was a decision of the [[High Court of Australia]], which ruled on 29 August 2007 that Baxter Healthcare Proprietary Limited, a [[tendering|tenderer]] for various government contracts, was bound by the ''[[Trade Practices Act 1974]]'' (TPA, Australian legislation governing anti-competitive behaviour) in its trade and commerce in tendering for government contracts. More generally, the case concerned the principles of derivative [[Crown immunity|governmental immunity]]:<ref>There are differing opinions whether the immunity in Australia should be referred to as "Crown immunity" or "governmental immunity". For example, {{Harvnb|Wright|2008}} refers to governmental immunity; while {{Harvnb|Seddon|2009}} refers to Crown immunity. Without judging the merits of which term is more appropriate, this article uses "governmental immunity"</ref> whether the immunity of a government from a statute extends to third parties that conduct business with the government.

The High Court's judgment marked a successful appeal for the [[Australian Competition and Consumer Commission]], the Australian regulator of anti-competitive conduct, having lost at first instance and on appeal in the [[Federal Court of Australia]]. The ACCC was again successful when the case was remitted to the Federal Court for reconsideration, ending eight years of litigation between the parties. The High Court's judgment was received as a significant precedent in the law of derivative governmental immunity in Australia.

==Background==

===Facts===
Baxter Healthcare Proprietary Limited (Baxter), was the Australian subsidiary of the multinational health care company [[Baxter International]]. Baxter manufactured [[Intravenous therapy|intravenous]] (IV) and [[peritoneal dialysis]] (PD) fluids at various plants in Australia.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/FCA/2005/581.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2005] [[Federal Court of Australia]] 581 at [6]-[7]</ref> Because of the cost of importing sterile IV fluids and the absence of a rival domestic producer, Baxter was a monopoly supplier of sterile IV fluids in the Australian market. Its monopoly covered large volume parenteral fluids, irrigating solutions and [[Parenteral nutrition|parenteral nutrition fluids]]. However, Baxter faced competition in the market for [[peritoneal dialysis]] fluids (PD fluids).<ref name="Duns51">{{Harvnb|Duns|2008|p=51}}</ref>

A number of [[States and territories of Australia|state governments]] issued [[tendering|requests for tender]] for the supply of sterile fluids and PD fluids.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2007/38.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] 38 at [10]</ref> Baxter responded to the requests with tenders that put forward two alternative pricing options: either a state could purchase sterile fluids and PD fluids as a bundled package at a discounted rate, or the state could buy each product separately but at a higher rate.<ref name="Corones 2007 374">{{Harvnb|Corones|2007|p=374}}</ref>

===Legislation===
Section&nbsp;46 of the TPA prohibited corporations from misusing market power.<ref name="TPA s46">''[http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/ Trade Practices Act 1974].'' (Commonwealth), [http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/s46.html s.46]</ref> Section&nbsp;47 prohibited [[exclusive dealing]].<ref name="TPA s47">''[http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/ Trade Practices Act 1974].'' (Commonwealth), [http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/s47.html s.47]</ref> The critical provision to the case was Section&nbsp;2B of the TPA. Section&nbsp;2B provided that Sections&nbsp;46 and 47 of the TPA:

<blockquote>bind the Crown in right of each of the States, of the Northern Territory and of the Australian Capital Territory, so far as the Crown carries on a business, either directly or by an authority of the State or Territory<ref name="TPA s2B">''[http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/ Trade Practices Act 1974].'' (Commonwealth), [http://www.austlii.edu.au/au/legis/cth/consol_act/tpa1974149/s2b.html s.2B] Section&nbsp;2B provides that the Crown is bound by Part IV of the TPA, which includes Sections&nbsp;46 and 47.</ref></blockquote>

Section&nbsp;2B thus provided an immunity from state and territory governments from Sections&nbsp;46 and 47 of the TPA insofar as the governments were not carrying on a business.

===The Australian Competition and Consumer Commission===
The [[Australian Competition and Consumer Commission]] (ACCC), the Australian government authority responsible for regulating the TPA, commenced an action in the [[Federal Court of Australia]] seeking declarations that Baxter's bundling pricing structure in its tenders had contravened [[Australian Competition and Consumer Commission v Baxter Healthcare#Legislation|Sections&nbsp;46 and 47]] of the TPA. The ACCC sought the imposition of injunctions and pecuniary penalties by the court.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/FCA/2005/581.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2005] [[Federal Court of Australia]] 581 at [1]</ref>

===Derivative governmental immunity before ''Baxter''===
Derivative governmental immunity refers to the extension of a government's immunity from a statute to a non-government party on the basis the government would be affected if the statute was to apply to the other party.<ref>{{Harvnb|Wright|2008|p=116}}</ref>

Prior to ''Baxter'', the leading case on derivative governmental immunity in Australia was the 1979 High Court judgment in ''Bradken Consolidated Ltd v Broken Hill Pty Co Ltd''.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/1979/15.html ''Bradken Consolidated Ltd v Broken Hill Pty Co Ltd''] [1979] [[High Court of Australia]] 15</ref><ref>{{Harvnb|Wright|2008|pp=116–120}}</ref><ref>{{Harvnb|Heydon|1989|p=[2.115]}}</ref> In ''Bradken'', the High Court upheld a claim for derivative governmental immunity by equipment suppliers to the Queensland Commissioner for Railways. The majority's conclusion was that if the Queensland government was immune from the TPA, it would prejudice the Queensland government if the contracts and arrangements it entered were subject to the TPA through the other parties to the contracts and arrangements.<ref>{{Harvnb|Wright|2008|pp=116–117}}</ref>

''Bradken'''s application of the principle of derivative governmental immunity had been subject to criticism. Robertson Wright SC, a [[Senior Counsel]] specialising in competition and trade practices law,<ref>{{cite web|url=http://12thfloor.com.au/robertson-wright.htm|title=Robertson Wright SC|publisher=Wentworth Selborne Chambers (12th floor)|accessdate=13 January 2010}}</ref> argued there are "a number of difficulties" with the judgment, including the "unsatisfactory nature" of the authorities it relied on.<ref>{{Harvnb|Wright|2008|p=117}}. Wright details his criticisms in {{citation|last=Wright|first=Robertson|year=2007|title=The future of Derivative Crown Immunity—with a competition law perspective|journal=Competition and Consumer Law Journal |publisher=Lexis Nexis Australia |location=Sydney |volume=14 |page=240 |issn=1039-5598 }}</ref> The High Court's judgment in ''Baxter'' would mark a retreat from ''Bradken''.<ref>{{Harvnb|Corones|2007|p=375}}</ref>

===Federal Court litigation===
The ACCC conceded that the state governments were not carrying on businesses in procuring the medical products. This meant that the governments were immune from Sections&nbsp;46 and 47 of the TPA. Baxter argued that this immunity extended to itself, by claiming derivative governmental immunity.<ref name="Seddon168">{{Harvnb|Seddon|2009|p=168}}</ref> On 16 May 2005, The [[Federal Court of Australia]] ([[James Allsop|Allsop J]] presiding) found in Baxter's favour at first instance.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/FCA/2005/581.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2005] [[Federal Court of Australia]] 581</ref> While the court held that Baxter would have breached the TPA, governmental immunity under Section&nbsp;2B extended to Baxter Healthcare.<ref name="Corones 2007 374"/> Allsop J's judgment was upheld unanimously on appeal to the full bench of the Federal Court (Justices [[John Mansfield (judge)|Mansfield]], [[John Dowsett|Dowsett]] and [[Roger Gyles|Gyles]] presiding).<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/FCAFC/2006/128.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2006] [[Federal Court of Australia]] Full Court 128 at [1]</ref> The full bench expressed unease about its own judgment, stating that the question of derivative governmental immunity should be left to the High Court for reconsideration of ''Bradken''.<ref>{{Harvnb|Wright|2008|p=122}}</ref>

==High Court appeal==
The ACCC was granted special leave to appeal to the High Court against the judgment of the full bench of the Federal Court.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/other/HCATrans/2007/60.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] Transcripts 60</ref> The appeal was heard on 16 May 2007. The [[Australian Government Solicitor]] acted for the ACCC, with [[Lindsay Foster]] as [[Senior Counsel]]; [[Blake Dawson]] and [[David Yates (judge)|David Yates SC]] represented Baxter. In addition to Baxter, the states of Western Australia, South Australia and New South Wales were respondents to the appeal.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/other/HCATrans/2007/202.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] Transcripts 202</ref> The High Court decided on 29 August 2007, by a 6–1 majority, to allow the ACCC's appeal and remit the matter back to the full bench of Federal Court for reconsideration. The majority held that Baxter was not covered by derivative governmental immunity in its dealings with the state governments.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2007/38.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] 38</ref>

===Judgments===

====Joint judgment====
Five judges ([[Chief Justice of Australia|Chief Justice]] [[Murray Gleeson|Gleeson]] and Justices [[William Gummow|Gummow]], [[Kenneth Hayne|Hayne]], [[Dyson Heydon|Heydon]] and [[Susan Crennan|Crennan]]) joined in the leading majority judgment allowing the ACCC's appeal. The joint judgment reasoned that Parliament could not have intended for corporations that do business with the government to be exempt from the restrictive trade practices provisions in Part&nbsp;IV of the TPA in respect of that business. Emphasising the overall purpose of the TPA, the judges reasoned that the purpose would not be fulfilled if Baxter could claim derivative immunity.<ref>{{Harvnb|Seddon|2009|p=169}}</ref> In response to the concern that one party to a transaction (the government) would be immune from the TPA while the other party would be bound by it, the joint judgment reasoned there was "nothing unusual" about such an outcome.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2007/38.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] 38 at [44]</ref>

The joint judgment did not rule out derivative governmental immunity in all cases. In determining the scope of whether governmental immunity from a statutory provision extends to a party dealing with the government, the judgment adopted the following position of Justice [[Frank Kitto|Kitto]], then dissenting, in the 1955 High Court case of ''Wynyard Investments v Commissioner for Railways (NSW)'':<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/1955/72.html ''Wynyard Investments v Commissioner for Railways''] [1955] [[High Court of Australia]] 72</ref><ref>{{Harvnb|Corones|2007|p=376}}</ref>

<blockquote>The object in view is to ascertain whether the Crown has such an interest in that which would be interfered with if the provision in question were held to bind the corporation that the interference would be, for a legal reason, an interference with some right, interest, power, authority, privilege, immunity or purpose belonging or appertaining to the Crown.</blockquote>

''Wynyard Investments'' was a case about governmental immunity generally (not derivative governmental immunity specifically), but Justice Kitto's dissenting judgment extended to derivative governmental immunity.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/1955/72.html ''Wynyard Investments v Commissioner for Railways''] [1955] [[High Court of Australia]] 72, Kitto J at [5]</ref>

Having examined the characteristics of the TPA as a law to promote competitive behaviour, the joint judgment held that the extension of derivative governmental immunity from the TPA to a trading corporation would be a "remarkable" conclusion and "far beyond what is necessary to protect the legal rights of governments, or to prevent a divesting of proprietary, contractual or other legal rights and interests."<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2007/38.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] 38 at [64]</ref>

====Kirby J====
[[Michael Kirby (judge)|Justice Kirby]]'s judgment agreed with the outcome of the joint judgment, for different reasons. Kirby criticised the concept of governmental immunity itself, stating that "persisting with [governmental immunity] into the twenty-first century is unacceptable."<ref name="Duns 2008 53–54">{{Harvnb|Duns|2008|pp=53–54}}</ref><ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/cases/cth/HCA/2007/38.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2007] [[High Court of Australia]] 38 at [97]</ref>

====Callinan J====
[[Ian Callinan|Justice Callinan]] dissented from the majority, holding that derivative governmental immunity extended to Baxter. Callinan followed ''Bradken'', concluding that it remained authoritative.<ref name="Duns 2008 53–54"/>

==Reaction to judgment==

===Significance===
Robertson Wright, writing after the judgment was handed down, claimed that ''Baxter'' represented a change to the law, drawing the following conclusions from the judgment:
* The application of derivative governmental immunity depends on a construction of the particular statute (particularly the object and purpose of the statute).
* As a general rule, derivative governmental immunity applies if the statute's coverage of a person would divest the government of proprietary, contractual, or other legal rights or interests (as opposed to commercial or policy rights or interests).<ref>{{Harvnb|Wright|2008|p=126}}</ref>

The judgment was reported in the press as a significant legal victory for the ACCC.<ref>{{cite news|url=http://www.brisbanetimes.com.au/news/business/back-to-the-trade-law-books-as-samuel-scores/2007/08/29/1188067191295.html|title=Back to the trade law books as Samuel scores|work = [[Brisbane Times]]|last=Maiden|first=Malcolm|date=29 August 2007|publisher=[[Fairfax Media]]|accessdate=13 January 2010}}</ref> The judgment was also received as an "historic decision" setting a precedent for government procurement, on the basis that businesses might no longer be able to rely on immunity from the TPA when contracting with governments.<ref>{{cite news|url=http://www.theage.com.au/news/business/businesses-could-lose-immunity-after-ruling/2007/08/29/1188067189790.html|title=Businesses could lose immunity after ruling|work =[[The Age]]|last=Murphy|first=Mathew|date=30 August 2007|publisher=[[Fairfax Media]]|accessdate=13 January 2010}}</ref>

===Criticism===
Nicholas Seddon, a lawyer and academic specialising in commercial and government law,<ref>{{cite web|url=http://www.blakedawson.com/Templates/Profiles/x_profile_content_page.aspx?id=8833|title=Nicholas Seddon: Special Counsel, Canberra|publisher=[[Blake Dawson]]|accessdate=13 January 2010}} At the time of writing his criticisms, Seddon was an employee of [[Blake Dawson]], the firm that represented Baxter throughout the Federal Court and High Court litigation.</ref><ref>{{cite web|url=http://law.anu.edu.au/scripts/StaffDetails.asp?StaffID=358|title=ANU College of Law: Our Staff|publisher=[[Australian National University]]|accessdate=13 January 2010}}</ref> claimed the High Court's judgment leaves "many uncertainties", particularly regarding whether derivative governmental immunity will extend to private sector providers carrying out governmental functions that have been contracted to them (as opposed to merely providing goods or services to the government).<ref>{{Harvnb|Seddon|2009|p=171}}</ref> Robertson Wright echoed these concerns, arguing the joint judgment did not "set out in as helpful detail" as it might the factors to be taken into account in deciding whether governmental immunity will derive to a party dealing with the government.<ref>{{Harvnb|Wright|2008|pp=126–127}}</ref> Seddon also criticised the outcome of the case itself, arguing it is "difficult to see how derivative immunity does not inevitably flow" from the governmental immunity from the TPA. He suggested that the application of the TPA to a party that deals with a government compromises the intention of Parliament that State and Territory governments should be immune from the TPA.<ref>{{Harvnb|Seddon|2009|p=170}}</ref>

==Later action==
The ACCC was successful, by a 2–1 majority, upon the remittal of the case to the full bench of the Federal Court. The full bench found that Baxter contravened Sections&nbsp;46 and 47 of the TPA.<ref>[http://www.austlii.edu.au/au/cases/cth/FCAFC/2008/141.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2008] [[Federal Court of Australia]] Full Court 141</ref> That judgment ended the eight-year-long litigation between Baxter and the ACCC. It was the first time in over 10 years that a corporation had unsuccessfully defended a prosecution brought by the ACCC for an alleged contravention of Section&nbsp;46.<ref>{{Cite news
 |last=Watts 
 |first=Kate 
 |author2=Gordon, Tova ([[Minter Ellison]]) 
 |title=Baxter litigation finally concludes 
 |newspaper=Lawyers Weekly 
 |location= 
 |pages= 
 |language= 
 |publisher=Lexis Nexis 
 |date=September 2008 
 |url=http://www.lawyersweekly.com.au/blogs/trade_practices/archive/2008/10/01/australian-amp-amp-new-zealand-trade-practices-law-bulletin-september-2008.aspx 
 |accessdate=16 December 2009 
 |archiveurl=https://web.archive.org/web/20091025033136/http://www.lawyersweekly.com.au/blogs/trade_practices/archive/2008/10/01/australian-amp-amp-new-zealand-trade-practices-law-bulletin-september-2008.aspx 
 |archivedate=25 October 2009 
 |deadurl=no 
 |df= 
}}</ref> The full bench declared that Baxter breached Sections&nbsp;46 and 47 of the TPA, but left it to the ACCC to seek pecuniary penalties.<ref>[http://www.austlii.edu.au/au/cases/cth/FCAFC/2008/141.html ''Australian Competition and Consumer Commission v Baxter Healthcare''] [2008] [[Federal Court of Australia]] Full Court 141 at [257]</ref> Baxter was refused special leave to appeal to the High Court against the full bench's judgment.<ref>[http://www.austlii.edu.au/cgi-bin/sinodisp/au/other/HCATrans/2009/20.html ''Baxter Healthcare v Australian Competition and Consumer Commission''] [2009] [[High Court of Australia]] Transcripts 20</ref>

==References==
{{Reflist|colwidth=30em}}

===Cited texts===
{{Portal|Australia}}
* {{citation |last=Corones |first=Stephen |year=2007 |title=Negotiating to supply government entities: when does the Trade Practices Act apply? |journal=Australian Business Law Review |publisher=Thomson LBC |location=Sydney |volume=35 |pages=374–378 |issn=0310-1053}}
* {{citation |last=Duns |first=John |year=2008 |title=The High Court rules on derivative Crown immunity: ACCC v Baxter Healthcare Pty Ltd |journal=Trade Practices Law Journal |publisher=Thomson LBC |location=Sydney |volume=16 |pages=51–54 |issn=1039-3277}}
* {{citation|last=Heydon|first=J D|title=Trade Practices Law: restrictive trade practices, deceptive conduct and consumer protection|publisher=Thomson LBC|location=Sydney|year=1989|edition=December 2009|isbn=0-455-20912-X}}
* {{citation|last=Seddon|first=Nicholas|title=Government Contracts: Federal, State and Local|publisher=The Federation Press|location=Sydney|year=2009|edition=4th|isbn=978-1-86287-740-5}}
* {{citation|last=Wright|first=Robertson|year=2008|title=Derivative governmental immunity: Lessons from ''Baxter'' and the Trade Practices Act|journal=Competition and Consumer Law Journal |publisher=Lexis Nexis Australia |location=Sydney |volume=16 |pages=114–136 |issn=1039-5598}}

{{Featured article}}

{{DEFAULTSORT:Australian Competition And Consumer Commission V Baxter Healthcare}}
[[Category:High Court of Australia cases]]
[[Category:2007 in Australian law]]
[[Category:2007 in case law]]
[[Category:Sovereign immunity]]
[[Category:Baxter International]]
[[Category:Pharmaceutical industry in Australia]]
[[Category:Competition law]]