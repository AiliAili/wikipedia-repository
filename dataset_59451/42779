<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Minihawk
 | image=File:Falconar Minihawk Prototype.jpg
 | caption=Minihawk prototype
}}{{Infobox Aircraft Type
 | type=[[Amateur-built aircraft]]
 | national origin=[[Canada]]
 | manufacturer=[[Falconar Avia]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production (2012)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=At least one
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]11,600 (kit only, less engine, 1998)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Falconar Minihawk''' is a [[Canada|Canadian]] [[amateur-built aircraft]], produced by [[Falconar Avia]]. The aircraft is supplied as a kit or plans for amateur construction.<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', page 162-164. BAI Communications. ISBN 0-9636409-4-1</ref>

==Design and development==
The aircraft features a cantilever [[low-wing]], a two-seats-in-[[side-by-side configuration]] enclosed cockpit under a sliding canopy, fixed [[tricycle landing gear]] or, optionally, [[conventional landing gear]], and a single engine in [[tractor configuration]].<ref name="Aerocrafter" />

The Minihawk is made from wood, with its flying surfaces covered in [[Aircraft dope|doped]] [[aircraft fabric]]. Its {{convert|25.5|ft|m|1|abbr=on}} span wing has an area of {{convert|106|sqft|m2|abbr=on}} and mounts [[Flap (aircraft)|flaps]]. The cockpit is {{convert|40|in|cm|0|abbr=on}} wide. The aircraft's recommended engine power range is {{convert|65|to|100|hp|kW|0|abbr=on}} and standard engines used include the {{convert|100|hp|kW|0|abbr=on}} [[Continental O-200]] [[four-stroke]] powerplant. Construction time from the supplied kit is estimated as 1500 hours.<ref name="Aerocrafter" /><ref name="Falconar">{{cite web|url = http://members.shaw.ca/falconark/FA/minihawk.htm|title = Minihawk|accessdate = 11 November 2012|last = Falconar Avia|date = 1 July 2012}}</ref>

==Operational history==
Even though a prototype was constructed, by November 2012 no examples were [[Aircraft registration|registered]] in its home country with [[Transport Canada]].<ref name="TCCAR">{{cite web|url=http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp |title=Canadian Civil Aircraft Register |accessdate=11 November 2012 |last=[[Transport Canada]] |date=11 November 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20110718042755/http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp |archivedate=18 July 2011 |df= }}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Minihawk) ==
{{Aircraft specs
|ref=Purdy<ref name="Aerocrafter" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=18
|length in=0
|length note=
|span m=
|span ft=25
|span in=6
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=106
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=645
|empty weight note=
|gross weight kg=
|gross weight lb=1070
|gross weight note=
|fuel capacity={{convert|15|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental O-200]]
|eng1 type=four cylinder, air-cooled, [[four stroke]] [[aircraft engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->

|prop blade number=2
|prop name=metal
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=128
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=115
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=50
|stall speed kts=
|stall speed note=flaps down
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=570
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1350
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=10.0
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[Mini-Hawk Tiger-Hawk]] - American aircraft with a similar name

==References==
{{reflist}}

==External links==
*{{Official website|http://members.shaw.ca/falconark/FA/minihawk.htm}}
{{Falconar Avia aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Falconar aircraft|Minihawk]]