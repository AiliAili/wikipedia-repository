{{Use American English|date=October 2013}}
{{Use mdy dates|date=October 2013}}
{{good article}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = Between the Times and the Tides
| Type       = studio
| Artist     = [[Lee Ranaldo]]
| Cover      = Between the Times and the Tides.jpg
| Alt        = A man with a guitar case walks in front of a black wall decorated with stick figure art.
| Released   = {{Start date|2012|03|20|mf=yes}}
| Recorded   = January–July 2011 at Echo Canyon West in [[Hoboken, New Jersey|Hoboken]], [[New Jersey]]
| Genre      = [[Alternative rock]], [[experimental rock]]
| Length     = {{Duration|m=47|s=39}}
| Label      = [[Matador Records|Matador]]
| Producer   = [[John Agnello]], Lee Ranaldo
| Last album = ''Maelstrom from Drift''<br />(2008)
| This album = '''''Between the Times and the Tides'''''<br />(2012)
| Next album = ''[[Last Night on Earth (Lee Ranaldo album)|Last Night on Earth]]''<br />(2013)
| Misc       = {{Singles
  | Name           = Between the Times and the Tides
  | Type           = studio
  | Single 1       = Off the Wall
  | Single 1 date  = January 14, 2012
  | Single 2       = Shouts
  | Single 2 date  = May 8, 2012
 }}
}}
'''''Between the Times and the Tides''''' is the ninth studio album by the American [[alternative rock]] musician [[Lee Ranaldo]], released on March 20, 2012&nbsp;on [[Matador Records]]. His first release on Matador Records and since [[Sonic Youth]]'s indefinite hiatus, the album features a more straightforward songwriting approach to his prior material and includes guest musicians such as [[Nels Cline]], [[John Medeski]] and [[Leah Singer]]. The album was originally intended to be a minimalist [[Acoustic music|acoustic]] album but its sound was developed by Ranaldo during its recording at Echo Canyon West in [[Hoboken, New Jersey|Hoboken]], [[New Jersey]] during a seven-month period in early 2011.

''Between the Times and the Tide''{{'}}s sound was influenced by [[Joni Mitchell]], [[Neil Young]] and [[Leonard Cohen]], as well as contemporary artists such as [[Cat Power]]. The title of the album comes from a lyric in the song "Xtina as I Knew Her". Upon its release, ''Between the Times and the Times'' received positive critical acclaim and charted in four countries, including Belgium, France, Germany and the United States. Its two singles—"Off the Wall" and "Shouts"—were moderate critical successes but failed to chart. The album's release was followed by two tours of North America and Oceania from April to October 2012, which included appearances at various music festivals, including [[All Tomorrow's Parties (music festival)#I'll Be Your Mirror|All Tomorrow's Parties I'll Be Your Mirror]] and the [[Melbourne International Arts Festival]].

==Background==
During his time as the lead guitarist of [[Sonic Youth]], Lee Ranaldo had considered recording a "simple, acoustic album" of original songs "with just an acoustic guitar and a microphone."<ref name="qint">{{cite web|url=http://thequietus.com/articles/08376-lee-ranaldo|title=Features &#124; A Quietus Interview &#124; Ebbing Tides: An Interview with Lee Ranaldo|work=[[The Quietus]]|last=Woolfrey|first=Chris|date=March 28, 2012|accessdate=January 13, 2013}}</ref> After the release of ''Maelstrom from Drift''—a collection of group improvisations, live recordings and home studio sessions—in 2008, Ranaldo's plans developed further. Following Sonic Youth's indefinite hiatus,<ref>{{cite web|url=http://www.nme.com/news/sonic-youth/59821|title=Sonic Youth's Kim Gordon and Thurston Moore announce marriage split &#124; News|work=[[NME]]|publisher=[[IPC Media]]|date=October 15, 2011|accessdate=January 13, 2013}}</ref> he began arranging the material with contributors such as [[Nels Cline]], [[John Medeski]] and [[Bob Bert]]. All three musicians, except Bert, had recorded renditions of [[Bob Dylan]] songs for the [[I'm Not There (soundtrack)|soundtrack]] to his 2007&nbsp;biopic, ''[[I'm Not There]]'', as The Million Dollar Bashers.<ref name="gw">{{cite web|url=http://www.guitarworld.com/interview-sonic-youths-lee-ranaldo-discusses-gear-and-his-new-album-between-times-and-tides|title=Interview: Sonic Youth's Lee Ranaldo Discusses Gear and His New Album, Between the Times and the Tides|work=[[Guitar World]]|publisher=[[NewBay Media]]|last=Fanelli|first=Damian|date=April 6, 2012|accessdate=January 13, 2013}}</ref> Most of the material he began arranging had been written between June and August 2010&nbsp;when the songs "just kept flowing out behind that one ["Lost"]."<ref name="gw" />

==Recording==
Ranaldo originally recorded acoustic [[Demo (music)|demos]] for ''Between the Times and the Tides'' on his [[iPhone]]. He later rerecorded them in-studio with [[Steve Shelley]], the drummer of Sonic Youth. According to Ranaldo, the subsequent demo recording led to the development of the songs, which then became "guitar-and-bass-and-drums demos."<ref name="qint" /> He referred to the recording process of the album as "gradual but really organic."<ref name="td">{{cite web|url=http://www.tonedeaf.com.au/features/interviews/217971/lee-ranaldo.htm|title=We Chat with Lee Ranaldo|work=Tone Deaf|last=Henriques-Gomes|first=Luke|date=October 24, 2012|accessdate=January 13, 2013}}</ref> When he began recording the album in January 2011&nbsp;at Echo Canyon West in [[Hoboken, New Jersey|Hoboken]], [[New Jersey]],<ref name="notes">{{cite AV media notes|title=Between the Times and the Tides|others=[[Lee Ranaldo]]|year=2012|type=Album notes|publisher=[[Matador Records]]|id=OLE-980-1}}</ref> Ranaldo "started laying them down in a little bit more hi–fi fashion",<ref name="gw" /> using studio equipment and [[Effects unit|effects pedals]].

Ranaldo used his signature [[Fender Jazzmaster]] guitar during the recording sessions of ''Between the Times and the Times'' and a custom-built electric guitar by Jarrell Guitars. Several [[C. F. Martin & Company|Martin]] and [[Gibson Guitar Corporation|Gibson]] acoustic guitars were used, including a Martin D-35, Martin 00018, Gibson J-50&nbsp;and Gibson J-45. Various equipment—such as [[Fender Prosonic]] and [[Fender Super Reverb]] amplifiers, [[Ibanez]] Analog Delay, Voodoo Lab Sparkle Drive, BJF Electronics' Honey Bee and DOD Two Second Digital Delay effects pedals—was used during the recording sessions to contribute to the album's sound.<ref name="gw" />

==Composition==
''Between the Times and the Tides'' features material composed solely by Ranaldo. The album features a more "song-led approach" to his previous material, which is largely considered [[noise rock]]. According to Ranaldo, he did not "deliberately" suppress noise composition but with earlier acoustic versions of the songs he would "often kind of 'Sonic' them up."<ref name="qint" /> The album's overall sound has been described as "a set of catchy, poppy tunes that establish a new aspect in [Ranaldo's] career."<ref name="qint" /> Ranaldo further explained that "it wasn't a conscious choice to write pop songs, they just sprung out of the guitars."<ref name="td" />

[[File:Neil Young 1996.jpg|thumb|right|Several musicians, including [[Neil Young]] (''pictured in 1996''), influenced ''Between the Times and the Tides''{{'}} sound]]
Certain musicians influenced Ranaldo's songwriting including [[Joni Mitchell]], [[Neil Young]], [[Leonard Cohen]] and [[Cat Power]], which he referred to as "a kind of older generation of songwriting."<ref name="qint" /> He also mentioned [[Bob Dylan]]'s ''[[Blonde on Blonde]]'' (1966) as a particular influence.<ref name="td" /> Ranaldo stated that "these songs were allowed to breathe because they weren't sent through the fan machine. When Sonic Youth writes music, we write everything in a very communal way. It doesn't matter who brought something in initially, it all gets transformed by the band. In this case, I was making decisions, and I wanted them to have a certain kind of simplicity. They ended up more personal for that reason."<ref>{{cite web|url=http://www.interviewmagazine.com/music/lee-ranaldo-between-the-times-and-the-tides|title=Lee Ranaldo's Mirror Reflections|work=[[Interview (magazine)|Interview]]|publisher=[[Peter M. Brant#Brant Publications, Inc.|Brant Publications]]|last=Bacher|first=Danielle|date=September 11, 2012|accessdate=January 13, 2013}}</ref>

All of the songs on ''Between the Times and the Tides'' are composed in [[Scordatura|alternate guitar tunings]], except "Fire Island" which is composed in [[standard tuning]].<ref name="gw" /> Speaking of the use of tunings on the album, Ranaldo said "they're all brand-new tunings on this album, none of them are from the Sonic Youth period. They're things that started happening, and you just sort of roll with it."<ref name="gw" /> He further explained that "Fire Island" was the first song he composed in standard tuning since Sonic Youth's [[Sonic Youth (album)|debut EP]], released in 1982. "Waiting on a Dream", the album's opening track, contains a guitar riff that, according to Damian Fanelli, resembles [[The Rolling Stones]]' 1966&nbsp;song "[[Paint It, Black]]" and "Tomorrow Never Comes", the album's closing track, references [[The Beatles]]' 1966&nbsp;song "[[Tomorrow Never Knows]]."<ref name="gw" /><ref name="td" />

==Packaging==
{{quote box|width=30em|bgcolor=#c6dbf7|"The figures from [[Maya Barkai]]'s public artwork cover three sides of a city block with images of the [[Walking Men Worldwide|'walking man']] symbol as it's interpreted in 99 different global cities. It's a beautiful work, and it's in my neighborhood. I didn't really realize it at the time, but I like the fact that the cover photo was taken locally, in my 'hood.  The artwork actually has the names of the various cities over each figure—we had to remove them to put the album title there, but I really liked the image of me walking with all these city names over my head; it felt like an image that reflected the traveling minstrel/touring life."
|source=—Lee Ranaldo on ''Between the Time and the Tides''{{'}} cover art.<ref name="nya">{{cite web|url=http://blog.downtownny.com/2012/05/sonic-youth-walking-through-lower-manhattan|title=Sonic Youth: Walking Through Lower Manhattan|last=Simmons|first=J.|date=May 29, 2012|publisher=Alliance for Downtown New York|accessdate=November 1, 2012}}</ref>}}

Ranaldo and Matt De Jong designed ''Between the Times and the Tides''{{'}} artwork, which included photographs by Kelly Jeffrey and Ranaldo's son, Cody.<ref name="notes" /> The front cover art features Ranaldo walking past [[Walking Men Worldwide|"Walking Men 99"]]—an art project in [[Lower Manhattan]], [[New York City|New York]], created by artist Maya Barkai.<ref name="nya" /> Ranaldo became interested in the project as he passed it daily and contacted Barkai who was "humbled to be approached by Lee."<ref name="wm">{{cite web|url=http://walking-men.com/|title=Walking Men Worldwide|work=walking-men.com|date=March 27, 2012|accessdate=November 1, 2012}}</ref>

Speaking of the album cover, Ranaldo said: "in some ways the whole album started with the cover photo, and I kind of built it up from there. A young Canadian photographer took that photo of me in September 2010 during an interview session about legendary Canadian group [[The Nihilist Spasm Band]]. When I saw the photo I thought it looked so much like a cool album cover. At that point I only had three or four acoustic demos going, and it remained in the back of my mind that if I ever made enough songs for an album, I’d use that pic as the cover. So that helped push me to develop the songs."<ref name="nya" /><ref name="wm" />

==Release==
On November 10, 2011, ''Between the Times and the Tides'' was announced for release through an online [[blog]] post by [[Matador Records]].<ref>{{cite web|url=http://www.matadorrecords.com/matablog/2011/11/10/coming-march-20-lee-ranaldos-between-the-times-the-tides/|title=Coming March 20, Lee Ranaldo's Between the Times & the Tides|publisher=[[Matador Records]]|date=November 10, 2011|accessdate=October 31, 2012}}</ref> The title of the album comes from a lyric in the song "Xtina as I Knew Her".<ref>{{cite web|url=http://www.flushthefashion.com/music/lee-ranaldo-interview|title=Interview with Lee Ranaldo: Between the Times and the Tides|work=Flush the Fashion|last=Williams|first=Nick|date=March 12, 2012|accessdate=January 13, 2013}}</ref> The album was released worldwide on March 20, 2012&nbsp;on Matador. It was released in a number of formats, including [[Compact disc|CD]], [[Gramophone record|LP]], [[FLAC]] and [[MP3]].<ref>{{cite web|url=http://store.matadorrecords.com/between-the-times-the-tides|title=Between the Times & the Tides|publisher=[[Matador Records]]|accessdate=October 31, 2012}}</ref> Demo versions of "Stranded", "Shouts" and "Waiting on a Dream" were included as bonus tracks on the [[iTunes Store|iTunes]] version of the release.<ref name="itunes">{{cite web|url=https://itunes.apple.com/ca/album/between-times-tides-bonus/id505258069|title=Music – Between the Times and the Tides (Bonus Track version) by Lee Ranaldo|work=[[iTunes Store]]|publisher=[[Apple Inc.|Apple]]|accessdate=October 30, 2012}}</ref> Two [[Single (music)|singles]] were released from the album: "Off the Wall" and "Shouts". "Off the Wall" was released as a 7" record and [[Music download|digital download]] package on January 14<ref>{{cite web|url=http://www.sonicyouth.com/symu/lee/2012/01/08/between-the-tides-the-times-lr-lp-hits-032012/|title=Between the Times & the Tides LP|publisher=sonicyouth.com|date=January 8, 2012|accessdate=October 31, 2012}}</ref> and "Shouts", which featured alternate mixes not found on the album, was released on May 8.<ref>{{cite web|url=http://www.sonicyouth.com/symu/lee/2012/05/08/tour-only-album-and-single/|title=Tour-Only Album and Single|publisher=sonicyouth.com|date=May 8, 2012|accessdate=October 31, 2012}}</ref>

Ranaldo promoted ''Between the Times and the Tides''{{'}} release with a 28-date tour throughout the United States and Europe, with [[Wilco]], [[Disappears]] and [[M. Ward]] supporting. The tour began on April 10, 2010&nbsp;at The Satellite in [[Los Angeles]], [[California]] and concluded on August 3&nbsp;at the Highland Bowl in [[Rochester, New York|Rochester]], [[New York (state)|New York]].<ref>{{cite web|url=http://pitchfork.com/news/46089-wilco-and-lee-ranaldo-to-tour-together/|title=Wilco and Lee Ranaldo to Tour Together &#124; News|last=Pelly|first=Jen|date=April 10, 2012|publisher=[[Pitchfork Media]]|accessdate=November 1, 2012}}</ref> In October, Ranaldo performed three shows in Australia at the Oxford Art Factory in [[Sydney]] on October 20, The Zoo in [[Brisbane]] on October 21&nbsp;and at The Hi-Fi on October 24, as part of the [[Melbourne International Arts Festival|Melbourne Festival]].<ref>{{cite web|url=http://musicfeeds.com.au/news/lee-ranaldo-of-sonic-youth-announces-australian-tour-october-2012/|title=Lee Ranaldo (of Sonic Youth) Announces Australian Tour October 2012 – Music News, Reviews, Interviews and Culture|last=Zanotti|first=Marc|date=August 7, 2012|work=[[Music Feeds]]|accessdate=November 1, 2012}}</ref> Local Australian bands Pony Face and We All Want To supported Ranaldo's performances in Sydney and Brisbane.<ref>{{cite web|url=http://www.tonedeaf.com.au/news/tournews/212755/lee-ranaldo-2012-tour-supports-announced.htm|title=Lee Ranaldo 2012 Tour Supports Announced|last=Semo|first=Esther|date=October 12, 2012|publisher=Tone Deaf|accessdate=November 1, 2012}}</ref>

''Between the Times and the Tides'' charted in four countries upon its release. It reached number 79&nbsp;on the [[Ultratop|Belgian Albums Chart]] in [[Flanders]],<ref name="bel">{{cite web|url=http://www.ultratop.be/nl/showitem.asp?interpret=Lee+Ranaldo&titel=Between+The+Times+And+The+Tides&cat=a|title=Lee Ranaldo – Between the Times and the Tides|work=[[Ultratop]]|publisher=Hung Medien|language=Dutch|accessdate=October 30, 2012}}</ref> number 149&nbsp;on the [[Syndicat National de l'Édition Phonographique|French Albums Chart]]<ref name="fr">{{cite web|url=http://lescharts.com/showitem.asp?interpret=Lee+Ranaldo&titel=Between+The+Times+And+The+Tides&cat=a|title=Lee Ranaldo – Between the Times and the Tides|work=''lescharts.com''|publisher=Hung Medien|language=French|accessdate=October 30, 2012}}</ref> and number 92&nbsp;on the [[Media Control Charts|German Albums Chart]].<ref name="de">{{cite web|url=http://www.officialcharts.de/album.asp?artist=Lee+Ranaldo&title=Between+The+Times+And+The+Tides&cat=a&country=de|title=Album – Lee Ranaldo, Between the Times and the Tides|publisher=[[Media Control Charts]]|language=German|accessdate=October 31, 2012}}</ref> In the U.S, the album peaked at number 13&nbsp;on ''[[Billboard (magazine)|Billboard]]''{{'}}s [[Top Heatseekers|Heatseeker Albums]] chart<ref name="amc">{{cite web|url={{Allmusic|class=album|id=between-the-times-and-the-tides-mw0002293926|tab=awards|pure_url=yes}}|title=Between the Times and the Tides – Lee Ranaldo: Awards|work=[[Allmusic]]|publisher=[[Rovi Corporation]]|accessdate=October 30, 2012}}</ref> and number 24&nbsp;on the [[Billboard charts#Albums|Tastemaker Albums]] chart.<ref name="bb">{{cite web|url=http://www.billboard.com/artist/306852/lee-ranaldo/chart?f=407|title=Lee Ranaldo – Chart history|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=October 24, 2013}}</ref>

==Reception==
{{Album ratings
| rev1 = [[AllMusic]]
| rev1Score = {{Rating|4|5}}<ref name="am">{{cite web|url=http://allmusic.com/album/between-the-times-and-the-tides-mw0002293926|title=Between the Times and the Tides – Lee Ranaldo: Songs, Reviews, Credits, Awards|last=Phares|first=Heather|work=[[AllMusic]]|publisher=[[All Media Guide|All Media Network]]|accessdate=October 31, 2012}}</ref>
| rev2 = ''[[The A.V. Club]]''
| rev2Score = B<ref name="av">{{cite web|url=http://www.avclub.com/articles/lee-ranaldo-between-the-times-and-the-tides,71084/|title=Lee Ranaldo: Between the Times and the Tides &#124; Music &#124; MusicalWork Review|last=Sharp|first=Elliott|date=March 20, 2012|work=[[The A.V. Club]]|publisher=[[The Onion]]|accessdate=October 31, 2012}}</ref>
| rev3 = [[Drowned in Sound]]
| rev3Score = 8/10<ref name="dis">{{cite web|url=http://drownedinsound.com/releases/16895/reviews/4144674|title=Lee Ranaldo – Between the Times and the Tides // Releases|last=Moores|first=J.R.|date=March 21, 2012|work=[[Drowned in Sound]]|publisher=Silentway|accessdate=October 31, 2012}}</ref>
| rev4 = ''[[Filter (magazine)|FILTER]]''
| rev4Score = 86%<ref name="fm">{{cite web|url=http://filtermagazine.com/index.php/reviews/entry/lee_ranaldo|title=Reviews – Lee Ranaldo|last=Poin|first=Loren Auda|date=March 19, 2012|work=[[Filter (magazine)|FILTER]]|publisher=FILTER Creative Group|accessdate=October 31, 2012}}</ref>
| rev5 = ''[[The Guardian]]''
| rev5Score = {{Rating|2|5}}<ref name="gdn">{{cite web|url=https://www.theguardian.com/music/2012/mar/18/lee-ranaldo-between-the-times-review|title=Lee Ranaldo: Between the Times and the Tides – review &#124; Music|last=Fox|first=Killian|date=March 18, 2012|work=[[The Guardian]]|publisher=[[Guardian Media Group]]|accessdate=October 31, 2012}}</ref>
| rev6 = ''[[MSN Music]]''
| rev6Score = A–<ref name="Christgau">{{cite web|last=Christgau|first=Robert|authorlink=Robert Christgau|date=April 20, 2012|url=http://www.robertchristgau.com/xg/cg/ew2012-04.php|title=Loudon Wainwright III/Lee Ranaldo|work=[[MSN Music]]|accessdate=October 2, 2015}}</ref>
| rev7 = ''[[NME]]''
| rev7Score = 7/10<ref name="nme">{{cite web|url=http://www.nme.com/reviews/lee-ranaldo/12892|title=Album Reviews – Lee Ranaldo – Between the Times and the Tides|last=Doran|first=John|date=March 16, 2012|work=[[NME]]|publisher=[[IPC Media]]|accessdate=October 31, 2012}}</ref>
| rev8 = ''[[Pitchfork Media]]''
| rev8Score = 5.2/10<ref name="pfm">{{cite web|url=http://pitchfork.com/reviews/albums/16413-between-the-times-and-the-tides/?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+PitchforkAlbumReviews+%28Pitchfork%3A+Album+Reviews%29|title=Lee Ranaldo: Between the Times and the Tides &#124; Album Reviews|last=Currin|first=Grayson|date=March 21, 2012|publisher=[[Pitchfork Media]]|accessdate=October 31, 2012}}</ref>
| rev9 = ''[[Q (magazine)|Q]]''
| rev9Score = {{Rating|4|5}}<ref>{{cite journal|journal=[[Q (magazine)|Q]]|location=London|title=none|page=102|date=April 2012}}</ref>
| rev10 = ''[[Rolling Stone]]''
| rev10Score = {{Rating|3|5}}<ref name="rs">{{cite web|url=http://www.rollingstone.com/music/albumreviews/between-the-times-and-the-tide-20120504|title=Between the Times and the Tides &#124; Album Reviews|last=Hermes|first=Will|date=May 4, 2012|work=[[Rolling Stone]]|publisher=Wenner Media|accessdate=October 31, 2012}}</ref>
}}
''Between the Times and the Tides'' received generally positive reviews from critics. At [[Metacritic]], which assigns a normalised rating out of 100&nbsp;to reviews from mainstream publications, it received an average score of 74, based on 24&nbsp;reviews.<ref name="mc">{{cite web|url=http://www.metacritic.com/music/between-the-times-and-the-tides/lee-ranaldo|title=Between the Times and the Tides Reviews, Ratings, Credits and More|work=[[Metacritic]]|publisher=[[CBS Interactive]]|accessdate=October 31, 2012}}</ref> Writing for ''[[MSN Music]]'', [[Robert Christgau]] said Ranaldo's beautifully played guitar work and [[Monophony|monophonic]] compositions complement his good-natured, cogent lyrics, which are "always palpable whether the songs reach out or recalibrate his options".<ref name="Christgau"/> [[AllMusic]] reviewer Heather Phares wrote that he "expands on those qualities in his music and reveals new ones", "displays a strong and surprising classic rock streak" and "gets downright hippie-ish, in a good way."<ref name="am" /> In a review for ''[[The A.V. Club]]'', Elliott Sharp said "Ranaldo's songs mostly adhere to a guitar-driven melancholic-pop mold, with songs like 'Off The Wall,' 'Angles,' and 'Lost' recalling Sonic Youth’s 2006&nbsp;effort ''[[Rather Ripped]]'' as well as early [[R.E.M.]]".<ref name="av" /> Spencer Grady from [[BBC Music]] compared the music to [[The Byrds]], [[Neil Young]] and R.E.M., and added that "flaws which initially seem awkward begin to make perfect sense after a few listens".<ref name="bbc">{{cite web|url=http://www.bbc.co.uk/music/reviews/6z5c|title=Music – Review of Lee Ranaldo – Between the Times and the Tides|last=Grady|first=Spencer|date=March 12, 2012|work=[[BBC Music]]|publisher=[[British Broadcasting Corporation]]|accessdate=October 31, 2012}}</ref> [[Drowned in Sound]]'s J.R. Moores said it was "advertised as his first 'rock album'" and impresses sonically.<ref name="dis" /> ''[[Filter (magazine)|Filter]]'' critic Loren Auda Poin wrote "at times sounding like [[Jimi Hendrix|Hendrix]] operating a theremin, and elsewhere resembling the mournful cries of lonesome satellites", Ranaldo's songs "are accomplished and take surprising turns, shot through with a mellow fury that's endlessly appealing."<ref name="fm" />

In a less enthusiastic review for ''[[The Guardian]]'', Killian Fox said ''Between the Times and the Tides'' was "more interesting sonically in the tension between questing guitars and straightforward song structures than it is in terms of lyrics, which aim to be down to earth but end up middle of the road."<ref name="gdn" /> ''[[NME]]'' writer John Doran wrote, "sure enough here are a wealth of rock gems that shine with a warm-hearted, Neil Young-like intensity. Those wanting clangour and dissonance will be disappointed, but everyone else will be pleasantly surprised."<ref name="nme" /> Writing for [[Pitchfork Media]], Grayson Currin deemed it a "motley assortment of Sonic Youth nods, acoustic entreaties, and cloying pop-rockers" but that "Ranaldo's opportunity to step out of the Sonic Youth shadows and into his own proper spotlight is mostly a miss made of mediocrity."<ref name="pfm" /> Ben Graham from ''[[The Quietus]]'' summarised the album as: "while it may be traditional in structure, ''Between the Times and the Tides'' is innovative and inimitable in actual performance. The playing may draw on jazz, country, rock and Ranaldo's own avant-garde catalogue."<ref name="tq">{{cite web|url=http://thequietus.com/articles/08430-lee-ranaldo-between-the-times-and-the-tides-review|title=Reviews &#124; Lee Ranaldo|last=Graham|first=Ben|date=April 3, 2012|work=[[The Quietus]]|accessdate=October 31, 2012}}</ref> In ''[[Rolling Stone]]'', [[Will Hermes]] found it "great to hear the third voice in Sonic Youth stretching out. But it's also a reminder of their irreplaceable magic."<ref name="rs" />

==Track listing==
{{Track listing
| total_length    = 47:39
| all_writing     = [[Lee Ranaldo]]
| title1          = Waiting on a Dream
| length1         = 6:14
| title2          = Off the Wall
| length2         = 3:04
| title3          = Xtina as I Knew Her
| length3         = 7:04
| title4          = Angles
| length4         = 3:17
| title5          = Hammer Blows
| length5         = 4:04
| title6          = Fire Island (Phases)
| length6         = 6:07
| title7          = Lost (Planet Nice)
| length7         = 3:59
| title8          = Shouts
| length8         = 4:54
| title9          = Stranded
| length9         = 4:20
| title10         = Tomorrow Never Comes
| length10        = 4:30
}}
{{Track listing
| collapsed      = yes
| headline        = [[iTunes Store|iTunes]]<ref name="itunes" /> bonus tracks
| total_length    = 63:28
| title11         = Stranded
| note11          = demo version
| length11        = 4:33
| title12         = Shouts
| note12          = demo version
| length12        = 4:50
| title13         = Waiting on a Dream
| note13          = demo version
| length13        = 6:26
}}

==Personnel==
All personnel credits adapted from ''Between the Times and the Tides''{{'}} liner notes.<ref name="notes" />

;Performer
*[[Lee Ranaldo]]&nbsp;– vocals, guitar, [[Record producer|production]], [[Sound recording and reproduction|recording]], design

;Other musicians
*[[Alan Licht]]&nbsp;– guitar, [[marimba]]
*[[Nels Cline]]&nbsp;– guitar, [[lap steel guitar]]
*Irwin Menken&nbsp;– bass
*[[Steve Shelley]]&nbsp;– drums
*[[John Medeski]]&nbsp;– piano, [[Organ (music)|organ]]
*Kathy Leisen&nbsp;– backing vocals <small>(1, 2, 4, 8)</small>
*[[Bob Bert]]&nbsp;– [[conga]]s <small>(5, 8)</small>
*[[Leah Singer]]&nbsp;– backing vocals <small>(8)</small>

;Technical personnel
*[[John Agnello]]&nbsp;– production, [[Audio engineering|engineering]], [[Audio mixing (recorded music)|mixing]]
*Aaron Mullan&nbsp;– assistant mixing, recording
*Ted Young&nbsp;– assistant mixing
*Bentley Anderson&nbsp;– recording
*Tim Glasgow&nbsp;– recording
*Greg Calbi&nbsp;– [[Audio mastering|mastering]]

;Art personnel
*Matt De Jong&nbsp;– design, layout
*Kelly Jeffrey&nbsp;– photography
*Cody Ranaldo&nbsp;– photography, digital editing

==Chart positions==
{| class="wikitable sortable plainrowheaders" style="text-align:center"
! scope="col"| Chart (2012)
! scope="col"| Peak<br />position
|-
! scope="row"| [[Ultratop|Belgian Albums Chart]] ([[Flanders]])<ref name="bel" />
| 79
|-
! scope="row"| [[Syndicat National de l'Édition Phonographique|French Albums Chart]]<ref name="fr" />
| 149
|-
! scope="row"| [[Media Control Charts|German Albums Chart]]<ref name="de" />
| 92
|-
! scope="row"| [[Top Heatseekers|U.S. ''Billboard'' Heatseeker Albums]]<ref name="amc" />
| 13
|-
! scope="row"| [[Billboard charts#Albums|U.S. ''Billboard'' Tastemaker Albums]]<ref name="bb" />
| 24
|}

==References==
{{reflist|30em}}

==External links==
*{{URL|http://www.sonicyouth.com/symu/lee/2012/01/08/between-the-tides-the-times-lr-lp-hits-032012/|''Between the Times and the Tides''}} at the official [[Sonic Youth]] web site
*{{Discogs master|419209|title=Between the Times and the Tides|type=album}}

{{Lee Ranaldo}}
{{Sonic Youth}}

[[Category:2012 albums]]
[[Category:Lee Ranaldo albums]]
[[Category:Matador Records albums]]
[[Category:English-language albums]]
[[Category:Albums produced by John Agnello]]