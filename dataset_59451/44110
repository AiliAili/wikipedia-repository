{{Infobox airport
| name         = Ålesund Airport, Sørneset
| nativename   = {{smaller|{{lang|no|Ålesund sjøflyplass, Sørneset}}}}
| image        = Aradi 196 at Sørneset.png
| image-width  = 300px
| caption      = [[Luftwaffe]] [[Arado Ar 196]] at the airport during the 1940s
| IATA         =
| ICAO         =
| type         = Private
| owner-oper   =
| city-served  = [[Ålesund]], [[Norway]]
| location     = Sørneset, Ålesund
| elevation-f  = 0
| elevation-m  = 0
| coordinates  = {{coord|62.46868|N|6.22640|E|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = 1
| pushpin_label_position =
| pushpin_label          = Sørvika
| pushpin_map_alt        =
| pushpin_mapsize        = 300
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| metric-elev  = y
| metric-rwy   = y
| r1-number    = 
| r1-length-f  =
| r1-length-m  =
| r1-surface   = Water
| stat-year    =
| stat1-header =
| stat1-data   =
| stat2-header =
| stat2-data   =
| stat3-header =
| stat3-data   =
| footnotes    = 
}}

'''Ålesund Airport, Sørneset''' ({{lang-no|Ålesund sjøflyplass, Sørneset}}) was a [[water aerodrome]] and later [[heliport]] situated at Nørvevika and later Sørneset in [[Ålesund]], [[Norway]]. The [[airport]] was, to a varying degree, used between 1929 and 1979. It has since been demolished.

The first use of Nørvevika was in November 1929 as a temporary base for the [[Royal Norwegian Navy Air Service]]. When [[Norwegian Air Lines]] commenced scheduled flights along the coast in 1935, they also took the facilities into use. The Navy Air Service returned in 1939 and decided to build a base, but there was no time to carry out the plans before the [[Operation Weserübung|German invasion]] the following year. The [[Luftwaffe]] used the base for two [[Arado Ar 196]] from 1941 to 1943, moving the base to Sørneset during the [[Second World War]].

Lufttransport moved to the base in 1946 and carried out commercial flights in 1948 and 1949. After shutting down, these were taken over by [[Vestlandske Luftfartsselskap]] in 1950. They were retained until 1957, and the following season [[Ålesund Airport, Vigra]] opened. The final operator at Sørneset was [[Mørefly]], who at first operated seaplanes and later focused on [[helicopter]]s. They abandoned the site in 1979, moving to Vigra.

==History==

===Establishment===
Sporadic flights with seaplanes to Ålesund started in 1920, although not with more than a few private flights per year.<ref>Hjelle: 11</ref> From November 1929 the Royal Norwegian Navy Air Service set up a base at Skutvika near the town center, which they used to search for [[Atlantic herring|herring]]. This base was found to be unsuitable due to the amount of ship traffic.<ref>Hafsten & Arheim: 62</ref> When they returned the following year, the Herring Association had instead selected a more suitable base at Nørvevika. There they established at wooden shed with a tin roof which acted as a hangar for two [[Hansa-Brandenburg W.33]] aircraft.<ref>Hafsten & Arheim: 63</ref> Due to harsh treatment the winter weather gave the aircraft, the air service terminated these operations the following year.<ref>Hafsten & Arheim: 64</ref>

Norwegian Air Lines carried out a trial postal route from Ålesund to [[Tromsø]] for four weeks in 1934, using a [[Junkers W 34]] from 7 June to 3 August.<ref>Nerdrum: 78</ref> The trials were successful and the following year the airline commenced flights with the larger [[Junkers Ju 52]].<ref name=h12>Hjelle: 12</ref> The routes were summer-only; for instance in 1938 they lasted from 4 April to 30 September.<ref>Nerdrum: 92</ref> These services continued until 4 September 1939, when all commercial flights were banned.<ref name=h12 />

The need for a naval seaplane base in Ålesund resumed in 1939. This was caused by the [[Phoney War]] and the need to station aircraft along the [[Møre og Romsdal]] coast to maintain Norwegian neutrality. Nørvevika was found to be the most suitable location and selected.<ref>Hafsten & Arheim: 42</ref> Funding of 225,000 [[Norwegian krone]] was granted, which would include a hangar which could fit three [[Heinkel He 115]] seaplanes. However, the entire issue was stopped during the [[eminent domain|expropriation]] process. The site had several owners and one of these died in the process. No work had therefore been carried out upon the German invasion on 9 April 1940.<ref>Hafsten & Arheim: 43</ref>

Luftwaffe started looking for a site for an airport between [[Bergen]] and [[Trondheim]] in January 1941. At first they suggested [[Spjelkavik]], but this was changed and [[Aukra Airport, Gossen]] built instead. However, the Luftwaffe still needed a base for seaplanes to operate near Ålesund.<ref>Flatmark: 118</ref> For this they carried out the plans which the Naval Air Service had made and erected a seaplane base later in 1941.<ref name=h12 /> This was at first built at Nørvevika, but was later relocated slightly to Sørneset. The ships were anchored to a [[buoy]] and a barracks was built on land.<ref name=f126>Flatmark: 126</ref> It was manned with two Arado Ar 196 of 2. Kustenfliegergruppe 406. One of these crashed on 18 March 1943 at Rambjøra in [[Haram]], killing both on board.<ref>Flatmark: 214</ref> The second crashed at Volsdalen in Ålesund on 18 October.<ref>Flatmark: 228</ref>

===Scheduled services===
The airline Lufttransport was founded in 1946 with a base at Sørneset. It eventually operated a fleet of four aircraft, including a [[Grumman Widgeon]] and a [[Fairchild 24]]. The airline built a hangar at the airport. It started a scheduled service to [[Oslo Airport, Fornebu]] three times a week in 1948, with stopovers in [[Molde]] and [[Kristiansund]] using the Widgeon. It also made attempts to fly to Bergen and Trondheim. However, the company had difficulties making a profit and shut down in 1949.<ref name=h12 /> Ownership of the airport passed to Ålesund Bil og Flyservice.<ref name=h13>Hjelle: 13</ref>

Vestlandske Luftfartselskap took over the coastal services in 1950. They operated summer routes from Bergen to Trondheim and hand intermediate stops in Ålesund and Kristiansund, later also in Molde. At first they used a four-passenger [[Republic RC-3 Seabee]], later a [[Short SA.6 Sealand]] with twice the passenger capacity. The routes operated until 1957. They were considered taken over by [[Widerøe]] and [[Solbergfly]],<ref name=h13 /> but the 1958 opening of [[Ålesund Airport, Vigra]] made this an implausible ordeal.<ref>Hjelle: 18</ref>

===Mørefly===
A group of enthusiasts bought a [[Luscombe 8|Luscombe 8 Silvaire]] and two years later they established the airline Mørefly. It took various general aviation contracted work, such as cargo transport, aerial photography and searching for [[Atlantic herring|herring]] steams.<ref name=h121>Hjelle: 121</ref> It also took over ownership of the airport.<ref name=h13 /> From 1957 they set up an air ambulance service out of Sørneset using a Seabee.<ref name=h121 />

Mørefly continued to operate various [[general aviation]] services out of Sørneset. From 1966 it also became a heliport when Mørefly bought its first helicopter, a [[Bell 47|Bell 47G4]].<ref name=h123>Hjelle: 123</ref> Operations included air ambulance, [[search and rescue]], cargo transport, herring patrol, aerial photography and a [[scuba diver]] standby for the fishing fleet.<ref name=h121 />

The airline decided in 1977 that it would relocate to Vigra. It built a new hangar there and relocated in 1979, abandoning Sørneset as an aerodrome.<ref name=h123 /> For a while the hangar was used by Ålesund Last og Buss.<ref>Nilsen: 97</ref> The hangar and barracks were still in place in 1988,<ref name=f126 /> but were in the early 1990s demolished and the area used for housing.<ref>Grytten (1998): 25</ref>

==Facilities==
The water aerodrome was physically located at two different locations in the bay of Nørvevika. The first was named Nørvevik and the second Sørneset.<ref name=f126 /> Sørneset is a headland situated on the island of [[Nørvøya]], between the Nørvevika and Nørvesundet.<ref>Grytten (1997): 234</ref> The water aerodrome consisted of a buoy, a [[slipway]], a hangar and a barracks used for offices. In addition there was a small floating dock.<ref>Flatmark: 127</ref> From 1966 the aerodrome also featured a [[helipad]].<ref name=h123 />

==References==
{{reflist|30em}}

==Bibliography==
* {{cite book |last=Flatmark |first=Jan Olav |title=Ålesund i hverdag og krig |year=1988 |location=Ålesund |publisher=Nordvest Forlag |isbn=82-90330-44-8 |language=Norwegian |url=http://www.nb.no/nbsok/nb/9d8e04271ea291d4a6a649270a3167eb}}
* {{cite book |last=Grytten |first=Harald |title=Byleksikon: Litt om mye i Ålesund |year=1997 |location=Ålesund |publisher=Sunnmørsposten Forlag |isbn=82-91450-05-6 |language=Norwegian |url=http://www.nb.no/nbsok/nb/ac84df129a2f48d3929739e5f649f620}}
* {{cite book |last=Grytten |first=Harald |title=Hjemsted og by: Ålesund 1948–1998 |volume=1 |year=1998 |location=Ålesund |publisher=Sunnmørsposten Forlag |isbn=82-91450-06-4 |language=Norwegian |url=http://www.nb.no/nbsok/nb/fbee71220ae206574024ae1663dd39d5}}
* {{cite book |last=Hafstad |first=Bjørn |last2=Arheim |first2=Tom |title=Marinens flygevåpen 1912–1944 |publisher=TankeStreken |year=2003 |isbn=82-993535-1-3 |language=Norwegian}}
* {{cite book |last=Hjelle |first=Bjørn Owe |title=Ålesund lufthavn Vigra |location=Valderøya |year=2007 |isbn=978-82-92055-28-1 |language=Norwegian}}
* {{cite book |last=Nerdrum |first=Johan |title=Fugl fønix: En beretning om Det Norske Luftfartselskap |year=1986 |publisher=Gyldendal Norsk Forlag |location=Oslo |isbn=82-05-16663-3 |language=Norwegian |url=http://www.nb.no/nbsok/nb/2d5877f30681b237f6e2f72b209dfa48?}}
* {{cite book |last=Nilsen |first=Oddvar |last2=Giske |first2=Kari |last3=Dybvik |first3=Peder Otto |title=Ålesund: om by og næringsliv |year=1998 |publisher=Nordvestlandet Forlag |location=Ålesund |language=Norwegian |isbn=82-90330-76-6 |url=http://www.nb.no/nbsok/nb/7a518420e48c08072915a50b0fd33aa8}}

{{Airports in Norway}}

{{DEFAULTSORT:Alesund Airport, Sorneset}}
[[Category:Water aerodromes in Norway]]
[[Category:Heliports in Norway]]
[[Category:Royal Norwegian Navy Air Service stations]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Airports in Møre og Romsdal]]
[[Category:Defunct airports in Norway]]
[[Category:Buildings and structures in Ålesund]]
[[Category:Airports established in 1941]]
[[Category:Airports disestablished in 1979]]
[[Category:1941 establishments in Norway]]
[[Category:1979 disestablishments in Norway]]
[[Category:Military installations in Møre og Romsdal]]