<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Monoplane
 | image=Hadasyde monoplane.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Sports aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=Handasyde Aircraft Company
 | designer=[[G.H. Handasyde]]
 | first flight=9 September 1923
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Handasyde monoplane''' was a single-seat light aircraft built for the 1923 [[Lympne light aircraft trials|Lympne motor glider competition]].  It competed there but won no prizes.

==Design and development==
In 1920 [[George Handasyde]] left the [[Martinsyde]] company he had formed before [[World War I]], with Helmut Martin, to form the Handasyde Aircraft Company.<ref name="OrdH">{{Harvnb|Ord-Hume|2000|pages=376–7}}</ref> Two of the draughtsmen he took on became well known, [[Sydney Camm]] and [[F. P. Raynham|Freddie Raynham]].  Handasyde,  Raynham and Camm together designed a successful [[Handasyde glider|glider]] for the [[Itford]] glider competition in 1922 in which  Raynham set a British record of 113 min, though this was only good enough for second place when Alexis Maneyrol set a world record of 201 min.   The 1923 Lympne motor glider competition was a natural outcome from the Itford event, and Handasyde produced a monoplane for it.<ref name="OrdH"/>

The all-wood Handasyde was a high-wing semi-cantilever monoplane, with parallel-chord wings that had slightly tapered tips.<ref name="OrdH"/>  The wings were built up around two spars and covered with 1&nbsp;mm plywood from the rear spar at mid-chord forward; on each side a lift strut linked the wing at about 1/3 span to the fuselage lower longeron.  The wing was then fabric covered, with a wire-supported scalloped trailing edge. It carried narrow ailerons outboard.  The fuselage was rectangular in cross section, tapering more in elevation than in plan.  There were no fixed stabilisers at the rear, only an all moving rudder and undivided elevator.  Both these surfaces were hinged to provide aerodynamic balance.  The single cockpit was over the wing and towards the trailing edge, with some instruments built into the wing surface. Ahead of the wing the fuselage was short, with the air-cooled 750 cc Douglas flat-twin driving a two-bladed propeller.  The diminutive undercarriage had thin and small diameter wheels (12 in or 305&nbsp;mm) on a single axle mounted on short vertical extensions from the lower longerons.<ref name="OrdH"/>

The Handasyde monoplane made its first flight on 9 September 1923, piloted by Raynham.<ref name="OrdH"/>  Despite sharing the engine troubles experienced by most contestants, it managed to fly for 158.5 miles (255&nbsp;km)  at 65.7 mpg (23.3&nbsp;km/litre).  This was well behind the eventual winners, the Wren<ref>{{Harvnb|Ord-Hume|2000|pages=335}}</ref> and the ANEC&nbsp;I,<ref>{{Harvnb|Ord-Hume|2000|pages=225}}</ref> both of which achieved 87.5 mpg (31.0&nbsp;km/litre).
[[File:Handasyde mono details.png|thumb]]
The Handasyde Company failed in 1923 and the monoplane's end is not recorded.<ref name="OrdH"/> 
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications==
[[File:Handasyde 3-view.png|thumb]]
{{Aircraft specs
|ref={{harvnb|Ord-Hume|2000|page=377}}
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=
|length ft=19
|length in=2
|length note=
|span m=
|span ft=30
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=4
|height in=8.5
|height note=
|wing area sqm=
|wing area sqft=135
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=300
|empty weight note=
|gross weight kg=
|gross weight lb=500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Douglas aeroengines|Douglas]]
|eng1 type=750 cc air cooled flat twin
|eng1 kw=<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=62
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=53
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{Commons category|Handasyde Monoplane}}

===Citations===
{{reflist}}

===Cited sources===
{{refbegin}}
*{{cite book |title= British Light Aeroplanes|last=Ord-Hume|first=Arthur W.J.G.| year=2000|volume=|publisher=GMS Enterprises |location=Peterborough |isbn=978-1-870384-76-6 |ref=harv}}
{{refend}}

<!-- ==External links== -->

[[Category:British sport aircraft 1920–1929]]