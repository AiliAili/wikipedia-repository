{{featured article}}
{{Infobox Bach composition
| title                 = {{lang|de|O heilges Geist- und Wasserbad}}
| bwv                   = 165
| type                  = [[Church cantata (Bach)|Church cantata]]
| image                 = Schlosskirche Weimar 1660.jpg
| alt                   = Painting of the interior of the church Schloßkirche, viewed along the nave towards the altar, showing two balconies and the organ on a third level above the altar
| caption               = The [[Schlosskirche, Weimar|''Schlosskirche'' in Weimar]]
| occasion              = [[Trinity Sunday|Trinity]]
| performed             = {{Timeline-event|date={{Start date|1715|06|16|df=y}}|location=[[Weimar]]}}
| movements             = 6
| text_poet             = [[Salomon Franck]]
| chorale               = by [[Ludwig Helmbold]]
| vocal                 = [[SATB|{{abbr|SATB|soprano, alto, tenor and bass}}]] choir and solo
| instrumental          = {{hlist | 2 violins | viola | cello | bassoon | continuo }}
}}
'''''{{lang|de|O heilges Geist- und Wasserbad}}''''' (O holy bath of Spirit and water{{efn|Although grammatically ''heiliges'' agrees with ''Bad'' instead of ''Geist'', [[Pamela Dellal|Dellal]] translates "O bath of Holy Spirit and of water,<!-- {{sfn|Dellal|2012}} --> and W. Murray Young "O Holy Ghost and water bath" (as cited at bach-cantatas)}}), '''{{nowrap|BWV 165}}''',{{efn|"BWV" is [[Bach-Werke-Verzeichnis]], a thematic catalogue of Bach's works.}} is a [[Bach cantata|church cantata]] by [[Johann Sebastian Bach]]. He composed it in [[Weimar]] for [[Trinity Sunday]] and led the first performance on 16 June 1715.

Bach had taken up regular cantata composition a year before when he was promoted to concertmaster at the Weimar court, writing one cantata per month to be performed in the ''{{lang|de|Schlosskirche}}'', the court chapel in the ducal [[Schloss Weimar|''Schloss'']]. ''{{lang|de|O heilges Geist- und Wasserbad}}'' was his first cantata for Trinity Sunday, the feast day marking the end of the first half of the [[liturgical year]]. The [[libretto]] by the court poet [[Salomo Franck]] is based on the day's [[lectionary|prescribed gospel reading]] about the meeting of Jesus and [[Nicodemus]]. It is close in content to the gospel and connects the concept of the [[Trinity (Christianity)|Trinity]] to [[baptism]].

The music is structured in six movements, alternating [[aria]]s and [[recitative]]s, and scored for a small ensemble of four vocal parts, strings and continuo. The voices are combined only in the closing chorale, the fifth stanza of [[Ludwig Helmbold]]'s [[hymn]] "{{lang|de|Nun laßt uns Gott dem Herren}}", which mentions scripture, baptism and the [[Eucharist in the Lutheran Church|Eucharist]], in a summary of the cantata's topic. Based on the text full of [[Baroque]] imagery, Bach composed a sermon in music, especially in the two recitatives for the bass voice, and achieved contrasts in expression. He led the first performance, and probably another on the Trinity Sunday concluding his first year as [[Thomaskantor]] in [[Leipzig]] on 4 June 1724.

{{TOC limit|3}}

== Background ==

{{for|details on Bach's promotion|Erschallet, ihr Lieder, erklinget, ihr Saiten! BWV 172#Background}}

[[File:1662 Wilhelm Ernst.jpg|thumb|upright|alt=portrait of the duke Wilhelm Ernst, dressed in armour and with a long white wig|Wilhelm Ernst, {{nowrap|Duke of Saxe-Weimar}}]]
On 2 March 1714 Bach was appointed ''{{lang|de|Konzertmeister}}'' (concert master) of the Weimar ''{{lang|de|Hofkapelle}}'' ([[court chapel]]) of the co-reigning dukes [[William Ernest, Duke of Saxe-Weimar|Wilhelm Ernst]] and [[Ernest Augustus I, Duke of Saxe-Weimar-Eisenach|Ernst August]] of Saxe-Weimar.{{sfn|Wolff|2002|p=147}} The position was created for him, possibly on his demand, giving him "a newly defined rank order" according to [[Christoph Wolff]].{{sfn|Wolff|2002|p=155}}

From 1695, an arrangement shared the responsibility for church music at the ''{{lang|de|Schlosskirche}}'' (court church) between the ''{{lang|de|[[Kapellmeister]]}}'' [[Johann Samuel Drese|Samuel Drese]] and the ''{{lang|de|Vize-Kapellmeister}}'' [[Georg Christoph Strattner]], who took care of one Sunday per month while the ''{{lang|de|Kapellmeister}}'' served on three Sundays. The pattern probably continued from 1704, when Strattner was succeeded by Drese's son [[Johann Wilhelm Drese|Johann Wilhelm]]. When ''{{lang|de|Konzertmeister}}'' Bach also assumed the principal responsibility for one cantata a month,{{sfn|Wolff|2002|p=147}} the ''{{lang|de|Kapellmeister}}'s'' workload was further reduced to two Sundays per month.{{sfn|Wolff|2002|p=147}}

The performance venue on the third tier of the court church, in German called  ''{{lang|de|Himmelsburg}}'' (Heaven's Castle), has been described by Wolff as "congenial and intimate",{{sfn|Wolff|2002|pp=157–159}} calling for a small ensemble of singers and players.{{sfn|Wolff|2002|pp=157–159}} Performers of the cantatas were mainly the core group of the ''{{lang|de|Hofkapelle}}'', formed by seven singers, three leaders and five other instrumentalists. Additional players of the military band were available when needed, and also town musicians and singers of the [[Gymnasium (Germany)|gymnasium]]. Bach as the concertmaster probably led the performances as the first violinist, while the organ part was played by Bach's students such as Johann Martin Schubart and [[Johann Caspar Vogler]].{{sfn|Wolff|2002|pp=157–158}} Even in settings like chamber music, Bach requested a strong continuo section with cello, bassoon and [[violone]] in addition to the keyboard instrument.{{sfn|Wolff|2002|pp=157–160}}

=== Monthly cantatas from 1714 to 1715 ===

While Bach had composed vocal music only for special occasions until his promotion, the regular chance to compose and perform a new work resulted in a program into which Bach "threw himself wholeheartedly", as Christoph Wolff notes.{{sfn|Wolff|2002|p=156}} In his first cantata of the series, {{lang|de|[[Himmelskönig, sei willkommen, BWV 182|''Himmelskönig, sei willkommen'', BWV 182]]}}, for the double feast of [[Palm Sunday]] and [[Annunciation]], he showed his skill in an elaborate work in eight movements, for four vocal parts and at times ten-part instrumental writing,{{sfn|Wolff|2002|p=156}} and presenting himself as a violin soloist.{{sfn|Wolff|2002|p=158}}

The following table of works performed by Bach as concertmaster between 1714 and the end of 1715 is based on tables by Wolff and [[Alfred Dürr]].{{sfn|Wolff|2002|pp=162–163}} According to Dürr, ''{{lang|de|O heilges Geist- und Wasserbad}}'' is the eleventh cantata composition of this period.{{sfn|Dürr|2006|p=14}} The works contain [[aria]]s and recitatives, as in contemporary opera, while earlier cantatas had concentrated on biblical text and chorale.{{sfn|Dürr|1971|p=29}} Some works, such as ''{{lang|de|[[Widerstehe doch der Sünde, BWV 54|Widerstehe doch der Sünde]]}}'', may have been composed earlier.

{| class="wikitable sortable plainrowheaders"
|-
|+ Bach's monthly cantatas from 1714 to 1715
|-
! scope="col" | Date
! scope="col" | Occasion
! scope="col" | BWV
! scope="col" | Incipit
! scope="col" | Text source
|-
| scope="row" style="text-align: right;" | {{sortdate|25 March 1714}} || {{hs|N04}} [[Annunciation]], {{nowrap|[[Palm Sunday]]}} || scope="row" style="text-align: right;" | 182 || ''{{lang|de|[[Himmelskönig, sei willkommen, BWV 182|Himmelskönig, sei willkommen]]}}'' || Franck?
|-
| scope="row" style="text-align: right;" | {{sortdate|21 April 1714}} || {{hs|N06}} [[Jubilate Sunday|Jubilate]] || scope="row" style="text-align: right;" | 12 || ''{{lang|de|[[Weinen, Klagen, Sorgen, Zagen, BWV 12|Weinen, Klagen, Sorgen, Zagen]]}}'' || Franck?
|-
| scope="row" style="text-align: right;" | {{sortdate|20 May 1714}} || {{hs|N07}} Pentecost || scope="row" style="text-align: right;" | 172 || ''{{lang|de|[[Erschallet, ihr Lieder, erklinget, ihr Saiten! BWV 172|Erschallet, ihr Lieder]]}}'' || Franck?
|-
| scope="row" style="text-align: right;" | {{sortdate|17 June 1714}} || {{hs|N09}} Third Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 21 || ''{{lang|de|[[Ich hatte viel Bekümmernis, BWV 21|Ich hatte viel Bekümmernis]]}}'' || Franck?
|-
| scope="row" style="text-align: right;" | {{sortdate|15 July 1714}} || {{hs|N11}} Seventh Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 54 || ''{{lang|de|[[Widerstehe doch der Sünde, BWV 54|Widerstehe doch der Sünde]]}}'' || [[Georg Christian Lehms|Lehms]]
|-
| scope="row" style="text-align: right;" | {{sortdate|12 August 1714}} || {{hs|N12}} Eleventh Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 199 || ''{{lang|de|[[Mein Herze schwimmt im Blut, BWV 199|Mein Herze schwimmt im Blut]]}}'' || [[Georg Christian Lehms|Lehms]]
|-
| scope="row" style="text-align: right;" | {{sortdate|2 December 1714}} || {{hs|N01}} First Sunday in [[Advent]] || scope="row" style="text-align: right;" | 61 || ''{{lang|de|[[Nun komm, der Heiden Heiland, BWV 61|Nun komm, der Heiden Heiland]]}}'' || [[Erdmann Neumeister|Neumeister]]
|-
| scope="row" style="text-align: right;" | {{sortdate|30 December 1714}} || {{hs|N02}} Sunday after Christmas || scope="row" style="text-align: right;" | 152 || ''{{lang|de|[[Tritt auf die Glaubensbahn, BWV 152|Tritt auf die Glaubensbahn]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|24 March 1715}} ? || {{hs|N03}} Oculi || scope="row" style="text-align: right;" | 80a || ''{{lang|de|[[Alles, was von Gott geboren, BWV 80a|Alles, was von Gott geboren]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|31 April 1715}} || {{hs|N05}} Easter || scope="row" style="text-align: right;" | 31 || ''{{lang|de|[[Der Himmel lacht! Die Erde jubilieret, BWV 31|Der Himmel lacht! Die Erde jubilieret]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|16 June 1715}} || {{hs|N08}} [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 165 || ''{{lang|de|O heilges Geist- und Wasserbad}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|14 July 1715}} || {{hs|N10}} Fourth Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 185 || ''{{lang|de|[[Barmherziges Herze der ewigen Liebe, BWV 185|Barmherziges Herze der ewigen Liebe]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|6 October 1715}} ? || {{hs|N13}} 16th Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 161 || ''{{lang|de|[[Komm, du süße Todesstunde, BWV 161|Komm, du süße Todesstunde]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{nowrap| {{sortdate|3 November 1715}} ?}} || {{hs|N14}} 20th Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 162 || ''{{lang|de|[[Ach! ich sehe, itzt, da ich zur Hochzeit gehe, BWV 162|Ach! ich sehe, itzt, da ich zur Hochzeit gehe]]}}'' || Franck
|-
| scope="row" style="text-align: right;" | {{sortdate|24 November 1715}} || {{hs|N15}} 23rd Sunday after [[Trinity Sunday|Trinity]] || scope="row" style="text-align: right;" | 163 || ''{{lang|de|[[Nur jedem das Seine, BWV 163|Nur jedem das Seine]]}}'' || Franck
|-
|}

== Topic and text ==

=== Trinity Sunday ===
Bach composed ''{{lang|de|O heilges Geist- und Wasserbad}}'' for [[Church cantata (Bach)#Trinity|Trinity Sunday]],{{sfn|Dürr|2006|p=371}} the Sunday concluding the first half of the liturgical year.{{sfn|Werthemann|2015}} The prescribed readings for the day were from the [[Epistle to the Romans]], "What depth of the riches of the wisdom and knowledge of God" ({{Sourcetext|source=Bible|version=King James|book=Romans|chapter=11|verse=33|range=–36}}), and from the [[Gospel of John]], the meeting of Jesus and [[Nicodemus]] ({{Sourcetext|source=Bible|version=King James|book=John|chapter=3|verse=1|range=–15}}).{{sfn|Dürr|2006|p=371}}{{sfn|Werthemann|2015}}

In Leipzig, Bach composed two more cantatas [[Church cantata (Bach)#Trinity|for the occasion]] which focused on different aspects of the readings, {{lang|de|[[Höchsterwünschtes Freudenfest, BWV 194|''Höchsterwünschtes Freudenfest'', BWV 194]]}}, first composed for the inauguration of church and organ in [[Störmthal]] on 2 November 1723,{{sfn|Dürr|2006|p=715}} {{lang|de|[[Es ist ein trotzig und verzagt Ding, BWV 176|''Es ist ein trotzig und verzagt Ding'', BWV 176]]}} (1725){{sfn|Dürr|2006|p=374}} and the [[Chorale cantata (Bach)|chorale cantata]] {{lang|de|[[Gelobet sei der Herr, mein Gott, BWV 129|''Gelobet sei der Herr, mein Gott'', BWV 129]]}} (1726).{{sfn|Dürr|2006|p=377}} Scholars debate if Bach performed on Trinity Sunday of 1724, which fell on 4 June, ''{{lang|de|Höchsterwünschtes Freudenfest}}'' or ''{{lang|de|O heilges Geist- und Wasserbad}}'' or both.{{sfn|Dürr|1971|p=40}}

=== Cantata text ===
The libretto was written by the court poet, Salomon Franck, and published in ''{{lang|de|Evangelisches Andachts-Opffer}}'' in 1715.{{sfn|Ambrose|2012}} The opening refers to Jesus' words in John 3:5: "Except a man be born of water and of the Spirit, he cannot enter into the kingdom of God."({{Sourcetext|source=Bible|version=King James|book=John|chapter=3|verse=5}}){{sfn|Hofmann|1996|p=6}} The second movement, a [[recitative]], reflects upon birth in the Spirit as [[baptism]] through God's grace: "{{lang|de|Er wird im Geist und Wasserbade ein Kind der Seligkeit und Gnade}}" (In the bath of spirit and water he becomes a child of blessedness and grace).{{sfn|Dellal|2012}} Movement 3, an aria for alto, considers that the bond has to be renewed throughout life, because it will be broken by man, reflected in movement 4. The last aria is a prayer for the insight that the death of Jesus brought salvation,{{sfn|Dürr|2006|p=371}} termed "{{lang|de|Todes Tod}}" (death's death).{{sfn|Hofmann|1996|p=6}} The cantata concludes with the fifth stanza of [[Ludwig Helmbold]]'s [[hymn]] of 1575, "{{lang|de|Nun laßt uns Gott dem Herren}}", mentioning scripture, baptism and the [[Eucharist in the Lutheran Church|Eucharist]].{{sfn|Ambrose|2012}}{{sfn|Browne|2007}} Bach used the eighth and final stanza, "Erhalt uns in der Wahrheit" (Keep us in the truth), to conclude his cantata [[Gott der Herr ist Sonn und Schild, BWV 79|''Gott der Herr ist Sonn und Schild'', BWV 79]].{{sfn|Browne|2007}}

Salomon expresses his thought in [[Baroque]] style rich in imagery. The image of the serpent is used in several meanings: as [[Serpent (Bible)|the serpent]] which seduced Adam and Eve to sin in paradise, as [[Nehushtan|the symbol]] which [[Moses]] erected in the desert, and related to the gospel's verse 14: "And as Moses lifted up the serpent in the wilderness, even so must the Son of man be lifted up".{{sfn|Dürr|2006|p=373}}

== Performance and publication ==

Bach led the first performance of the cantata on 16 June 1715. The performance material for Weimar is lost.{{sfn|Mincham|2010}} Bach performed the work again as [[Thomaskantor]] in [[Leipzig]]. Extant performance material was prepared by his assistant [[Johann Christian Köpping]].{{sfn|Hofmann|1996|p=7}} The first possible revival is the Trinity Sunday of Bach's first year in office, 4 June 1724,{{sfn|Bach digital|2014}} also the conclusion of his first year and first Leipzig cantata cycle, because he had assumed the office on the first Sunday after Trinity the year before. Bach made presumably minor changes.{{sfn|Dürr|2006|p=372}}

The cantata was published in the {{lang|de|Bach-Ausgabe}}, the first edition of Bach's complete works by the {{lang|de|[[Bach-Gesellschaft]]}}, in 1887 in volume 33, edited by [[Franz Wüllner]]. In the second edition of the complete works, the {{lang|de|[[Neue Bach-Ausgabe]]}}, it appeared in 1967, edited by Dürr, with a ''{{lang|de|Kritischer Bericht}}'' (Critical report) following in 1968.{{sfn|Bach digital|2014}}

== Music ==

=== Scoring and structure ===

The title on the copy by Johann Christian Köpping is: "Concerto a 2&nbsp;Violi:1&nbsp;Viola. Fagotto Violoncello S.A.T.e Basso e Continuo&nbsp;/ di Joh:Seb:Bach" (Concerto for 2&nbsp;violins, 1&nbsp;viola. Bassoon Cello S.A.T and Bass and Continuo&nbsp;/ by Joh:Seb:Bach).{{sfn|Grob|2014}} The cantata in six movements is scored like [[chamber music]] for four vocal soloists ([[soprano]], [[alto]], [[tenor]] and [[Bass (voice type)|bass]]), a [[SATB|four-part choir (SATB)]] in the closing chorale, two [[violin]]s (Vl), [[viola]] (Va), [[bassoon]] (Fg), [[cello]] (Vc) and [[basso continuo]] (Bc).{{sfn|Dürr|2006|p=373}}{{sfn|Hofmann|1996|p=7}} The bassoon is called for, but has no independent part.{{sfn|Ambrose|2012}}{{sfn|Mincham|2010}} The duration is given as about 15 minutes.{{sfn|Dürr|2006|p=371}}

In the following table of the movements, the scoring follows the Neue Bach-Ausgabe,{{sfn|Bach digital|2014}} and the abbreviations for voices and instruments the [[List of Bach cantatas#Abbreviations|list of Bach cantatas]]. The [[Key (music)|keys]] and [[time signature]]s are taken from the Bach scholar [[Alfred Dürr]], using the symbol for common time (4/4). The instruments are shown separately for winds and strings, while the continuo, playing throughout, is not shown.

{{Classical movement header | show_text_source = yes | work = ''O heilges Geist- und Wasserbad'', BWV 165 | instruments1 = Strings | instruments2 = Bass}}

{{Classical movement row
| number = [[#1|1]]
| title = ''{{lang|de|O heilges Geist- und Wasserbad}}''
| text_source = Franck
| type = Aria
| vocal = S
| instruments1 = 2Vl Va
| instruments2 = Fg Bc
| key = {{nowrap|[[G major]]}}
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#2|2]]
| title = ''{{lang|de|Die sündige Geburt verdammter Adamserben}}''
| text_source = Franck
| type = Recitative
| vocal = B
| instruments1 = 
| instruments2 = Bc
| key = {{nowrap|[[C minor]] –}} {{nowrap|[[A minor]]}}
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#3|3]]
| title = ''{{lang|de|Jesu, der aus großer Liebe}}''
| text_source = Franck
| type = Aria
| vocal = A
| instruments1 = 
| instruments2 = Bc
| key = C minor
| time = 12/8
}}
{{Classical movement row
| number = [[#4|4]]
| title = ''{{lang|de|Ich habe ja, mein Seelenbräutigam}}''
| text_source = Franck
| type = Recitative
| vocal = B
| instruments1 = 2Vl Va
| instruments2 = Fg Bc
| key = {{nowrap|[[B minor]] –}} {{nowrap|G major}}
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#5|5]]
| title = ''{{lang|de|Jesu, meines Todes Tod}}''
| text_source = Franck
| type = Aria
| vocal = T
| instruments1 = {{nowrap|2Vl ([[Unison|unis.]])}}
| instruments2 = 
| key = G major
| time = {{music|common-time}}
}}
{{Classical movement row
| number = [[#6|6]]
| title = ''{{lang|de|Sein Wort, sein Tauf, sein Nachtmahl}}''
| text_source = Helmbold
| type = Chorale
| vocal = SATB
| instruments1 = 2Vl Va
| instruments2 = Fg Bc
| key = 
| time = {{music|common-time}}
}}

{{End}}

=== Movements ===

[[File:Crijn Hendricksz.jpeg|thumb|alt=Jesus (left, in profile, pointing with his right hand) and Nicodemus (facing the viewer, with a turban, with his right hand on his heart) sit at a table, arguing over books|''Jesus teaches Nicodemus'' (seen here in a seventeenth-century painting) was a theme used by both composers and artists.]]

The cantata consists of solo movements closed by a four-part chorale. Arias alternate with two recitatives, both sung by the bass. [[John Eliot Gardiner]] summarizes: "It is a true sermon in music, based on the Gospel account of Jesus' night-time conversation with Nicodemus on the subject of 'new life', emphasising the spiritual importance of baptism."{{sfn|Gardiner|2008|p=6}} He points out the many musical images of water.{{sfn|Gardiner|2008|p=6}}

==== 1 ====

In the first aria, "'''{{lang|de|O heilges Geist- und Wasserbad}}'''" (O bath of Holy Spirit and of water),{{sfn|Dellal|2012}} the [[ritornello]] is a [[fugue]], whereas in the five vocal sections the soprano and violin I are a duo in imitation on the same material. These sections are composed in [[Symmetry (music)|symmetry]], A B C B' A'. The theme of B involves an [[Inversion (music)|inversion]] of material from A, that of C is derived from measure 2 of the ritornello. Dürr writes:{{quote|The prominent use made of formal schemes based on the principles of symmetry and inversion is in all probability intentional, serving as a symbol of the inner inversion of mankind&nbsp;— his rebirth in baptism.{{sfn|Dürr|2006|p=373}}}}

==== 2 ====

The first recitative, "'''{{lang|de|Die sündige Geburt verdammter Adamserben}}'''" (The sinful birth of the cursed heirs of Adam),{{sfn|Dellal|2012}} is [[Glossary of musical terminology#secco|secco]], but several phrases are close to an [[arioso]].{{sfn|Dürr|2006|pp=373–374}} The musicologist Julian Mincham notes that Bach follows the meaning of the text closely, for example by "rhythmic dislocations for death and destruction", a change in harmony on "poisoned", and "the complete change of mood at the mention of the blessed Christian". He summarizes: "Here anger and resentment at Man’s inheritance of suppurating sin is contrasted against the peace and joy of God-given salvation".{{sfn|Mincham|2010}}

==== 3 ====

The second aria, "'''{{lang|de|Jesu, der aus großer Liebe}}'''" (Jesus, who out of great love),{{sfn|Dellal|2012}} accompanied by the continuo, is dominated by an expressive [[Motif (music)|motif]] with several upward leaps of [[Interval (music)#Main intervals|sixths]], which is introduced in the ritornello and picked up by the alto voice in four sections.{{sfn|Dürr|2006|p=374}} Mincham notes that "the mood is serious and reflective but also purposeful and quietly resolute".{{sfn|Mincham|2010}}

==== 4 ====

The second recitative, "'''{{lang|de|Ich habe ja, mein Seelenbräutigam}}'''" (I have indeed, o bridegroom of my soul),{{sfn|Dellal|2012}} is accompanied by the strings (''accompagnato''), marked by Bach "Rec: con Stroment" (Recitative: with instruments).{{sfn|Grob|2014}} The German musicologist [[Klaus Hofmann]] notes that the text turns to mysticism, reflecting the Bridegroom, Lamb of God and the serpent in its double meaning.{{sfn|Hofmann|1996}} The text is intensified by several [[melisma]]s, a marking "[[Tempo (music)|adagio]]" on the words "{{lang|de|hochheiliges Gotteslamm}}" (most holy Lamb of God),{{sfn|Dellal|2012}} and by melodic parts for the instruments. Gardiner notes that Bach has images for the [[Nehushtan|serpent]] displayed in the desert by [[Moses]], and has the accompaniment fade away on the last line "{{lang|de|wenn alle Kraft vergehet}}" (when all my strength has faded).{{sfn|Gardiner|2008|p=6}}

==== 5 ====

The last aria, "'''{{lang|de|Jesu, meines Todes Tod}}'''" (Jesus, death of my death),{{sfn|Dellal|2012}} is set for tenor, accompanied by the violins in [[unison]], marked "Aria Violini unisoni e Tenore".{{sfn|Grob|2014}} The image of the serpent appears again, described by the composer and musicologist [[William G. Whittaker]]: "the whole of the [[obbligato]] for violins in unison is constructed out of the image of the bending, writhing, twisting reptile, usually a symbol of horror, but in Bach's musical speech a thing of pellucid beauty".{{sfn|Gardiner|2008|p=6}}

==== 6 ====

[[File:Nikolaus-Selnecker.jpg|thumb|upright|alt=black-and-white portrait of a man with a cap and a long beard, framed on top by an arch inscribed with "Nicolaus Selneccerus"|Nikolaus Selnecker, who wrote the hymn tune]]
The cantata closes with a four-part setting of the chorale stanza, '''{{lang|de|Sein Wort, sein Tauf, sein Nachtmahl}}''' (His word, His baptism, His communion).{{sfn|Dürr|2006|p=374}}{{sfn|Dellal|2012}}
The text in four short lines summarizes that Jesus helps any in need by his words, his baptism and his communion, and ends in the prayer that the Holy Spirit may teach to faithfully trust in this.{{sfn|Dürr|2006|p=372}}{{sfn|Grob|2014}}

{{columns|width=auto
| col1 = <poem>Sein Wort, sein Tauf, sein Nachtmahl
Dient wider allen Unfall,
der Heilge Geist im Glauben
lehr uns darauf vertrauen.</poem>
| col2 = <poem>His Word, His Baptism, His Supper
Serve to protect us against all disaster,
May the Holy Spirit teach us
To rely upon this in faith.{{sfn|Dürr|2006|p=372}}</poem>
}}

The hymn tune by [[Nikolaus Selnecker]] was first published in Leipzig in 1587 in the hymnal ''Christliche Psalmen, Lieder vnd Kirchengesenge'' (Christian psalms, songs and church chants).{{sfn|Braatz|Oron|2005}} Bach marked the movement: "{{lang|la|Chorale. Stromenti concordant}}", indicating that the instruments play {{lang|it|[[colla parte]]}} with the voices.{{sfn|Grob|2014}}

== Selected recordings ==

The recordings are provided by Aryeh Oron on the Bach-Cantatas website.{{sfn|Oron|2012}} Choirs and orchestras are roughly grouped:

# Large choirs (red background): Boys (choir of all male voices)
# Medium-size choirs, such as Chamber, Chorale (choir dedicated mostly to church music), Motet

# Large orchestras (red background): Symphony
# Chamber orchestra
# Orchestra on period instruments (green background)

{{Cantata discography header|work=''O heilges Geist- und Wasserbad'', BWV 165 |show_choir_type=yes |show_orchestra_type=yes }}

{{Cantata discography row
| id = Rilling
| title = ''{{lang|de|Die Bach Kantate}} Vol. 38''
| conductor = {{sortname|Helmuth|Rilling}}
| choir = [[Gächinger Kantorei]]
| orchestra = [[Bach-Collegium Stuttgart]]
| soloists = {{plainlist|
* [[Arleen Augér]]
* [[Alyce Rogers]]
* [[Kurt Equiluz]]
* [[Wolfgang Schöne]]
}}
| label = [[Hänssler Classic|Hänssler]]
| year = {{Start date|1976}}
| choir_type = 
| orchestra_type = Chamber
}}
{{Cantata discography row
| id = Leongardt
| title = ''J. S. Bach: {{lang|de|[[Bach cantatas (Teldec)|Das Kantatenwerk]]}} • Complete Cantatas. Folge / Vol. 39''
| conductor = {{sortname|Gustav|Leonhardt}}
| choir = [[Collegium Vocale Gent]]
| orchestra = [[Leonhardt-Consort]]
| soloists = {{plainlist|
* Soloist of the [[Tölzer Knabenchor]]
* [[Paul Esswood]]
* [[Kurt Equiluz]]
* [[Max van Egmond]]
}}
| label = [[Teldec]]
| year = {{Start date|1987}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Suzuki
| title = ''J.&nbsp;S. Bach: Cantatas Vol. 4 – {{lang|de|Man singet mit Freuden}}, Cantatas • 49 • 145 • 149 • 174 (Cantatas from Leipzig 1726–29)''
| conductor = {{sortname|Masaaki|Suzuki}}
| orchestra = [[Bach Collegium Japan]]
| soloists = {{plainlist|
* [[Aki Yanagisawa]]
* [[Akira Tachikawa]]
* [[Makoto Sakurada]]
* {{nowrap|[[Stephan Schreckenberger]]}}
}}
| label = [[BIS Records|BIS]]
| year = 1996
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Koopman
| title = ''J.&nbsp;S. Bach: Complete Cantatas Vol. 3''
| conductor = {{sortname|Ton|Koopman}}
| orchestra = [[Amsterdam Baroque Orchestra & Choir]]
| soloists = {{plainlist|
* [[Caroline Stam]]
* [[Elisabeth von Magnus]]
* [[Paul Agnew]]
* [[Klaus Mertens]]
}}
| label = [[Antoine Marchand]]
| year = 1997
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Leusink
| title = ''Bach Edition Vol. 5 – Cantatas Vol. 2''
| conductor = {{sortname|Pieter Jan|Leusink}}
| choir = [[Holland Boys Choir]]
| orchestra = [[Netherlands Bach Collegium]]
| soloists = {{plainlist|
* [[Ruth Holton]]
* [[Sytse Buwalda]]
* [[Nico van der Meel]]
* [[Bas Ramselaar]]
}}
| label = [[Brilliant Classics]]
| year = {{Start date|1999}}
| choir_type = Boys
| orchestra_type = Period
}}
{{Cantata discography row
| id = Gardiner
| title = ''Bach Cantatas Vol. 27: Blythburgh/Kirkwell / For Whit Tuesday / For Trinity Sunday''
| conductor = {{sortname|John Eliot|Gardiner}}
| choir = [[Monteverdi Choir]]
| orchestra = [[English Baroque Soloists]]
| soloists = {{plainlist|
* [[Ruth Holton]]
* [[Daniel Taylor (countertenor)|Daniel Taylor]]
* [[Paul Agnew]]
* [[Peter Harvey (bass)|Peter Harvey]]
}}
| label = [[Soli Deo Gloria (record label)|Soli Deo Gloria]]
| year = {{Start date|2000}}
| choir_type = 
| orchestra_type = Period
}}
{{Cantata discography row
| id = Schwarz
| title = ''J. S. Bach: {{lang|de|O heiliges Geist- und Wasserbad, BWV 165''}}
| conductor = {{sortname|Gotthold|Schwarz}}
| choir = [[Thomanerchor]]
| orchestra = [[Gewandhausorchester]]
| soloists = {{plainlist|
* [[Heike Kumlin]]
* [[Alexandra Röseler]]
* [[Martin Krumbiegel]]
* [[Gotthold Schwarz]]
}}
| label = [[Mitteldeutscher Rundfunk|MDR]], ({{lang|de|MDR-Figaro-Reihe Kantate)}}
| year = {{Start date|2003}}
| choir_type = Boys
| orchestra_type = Symphony
}}

{{End}}

== Notes ==
{{notelist}}

== References ==
{{reflist | 17em }}

== Bibliography ==
''Scores''
* {{IMSLP|work=O heilges Geist- und Wasserbad, BWV 165 (Bach, Johann Sebastian)}}
* {{cite web
  | url = http://www.bach-digital.de/receive/BachDigitalWork_work_00000200?lang=en
  | title = O heil'ges Geist- und Wasserbad BWV 165; BC A 90 / Sacred cantata (Trinity Sunday)
  | publisher = [[Leipzig University]]
  | year = 2014
  | accessdate = 13 June 2014
  | ref = {{sfnRef|Bach digital|2014}}
  }}

''Books''
* {{cite book
  | last = Dürr
  | first = Alfred
  | authorlink = Alfred Dürr
  | title = Die Kantaten von Johann Sebastian Bach
  | year = 1971
  | publisher = Deutscher Taschenbuchverlag
  | isbn = 3-423-04080-7
  | volume = 1
  | edition = 4
  | language = German
  | ref = harv
  }}
* {{cite book
  | last = Dürr
  | first = Alfred
  | authorlink = Alfred Dürr
  | others = Translated by [[Richard D. P. Jones]]
  | url = https://books.google.de/books?id=m9JuwslMcq4C&pg=PA371
  | title = The Cantatas of J. S. Bach: With Their Librettos in German-English Parallel Text
  | publisher = Oxford University Press
  | year = 2006
  | isbn = 978-0-19-929776-4
  | ref = harv
  }}
* {{cite book
  | last = Wolff
  | first = Christoph
  | authorlink = Christoph Wolff
  | url = https://books.google.com/books?id=NHpL3cQg0_UC&pg=PA147
  | title = Johann Sebastian Bach: The Learned Musician
  | publisher = Oxford University Press
  | isbn = 978-0-393-32256-9
  | year = 2002
  | ref = harv
  }}

''Online sources''

The complete recordings of Bach's cantatas are accompanied by liner notes from musicians and musicologists; Gardiner commented on his [[Bach Cantata Pilgrimage]], Hofmann wrote for [[Masaaki Suzuki]], and Wolff for [[Ton Koopman]].

* {{cite web
  | last = Ambrose
  | first = Z. Philip
  | url = http://www.uvm.edu/~classics/faculty/bach/BWV165.html
  | title = BWV 165 O heilges Geist- und Wasserbad
  | year = 2012
  | publisher = University of Vermont
  | accessdate = 20 February 2015
  | ref = harv
  }}
* {{cite web
  | last1 = Braatz
  | first1 = Thomas
  | last2 = Oron
  | first2 = Aryeh
  | url = http://www.bach-cantatas.com/CM/Nun-lasst-uns-Gott.htm
  | title = Chorale Melodies used in Bach's Vocal Works / Nun laßt uns Gott dem Herren
  | publisher = bach-cantatas
  | year = 2005
  | accessdate = 30 May 2012
  | ref = harv
  }}
* {{cite web
  | last1 = Browne
  | first1 = Francis
  | url = http://www.bach-cantatas.com/Texts/Chorale047-Eng3.htm
  | title = Nun laßt uns Gott dem Herren / Text and Translation of Chorale
  | publisher = bach-cantatas
  | year = 2007
  | accessdate = 30 May 2012
  | ref = harv
  }}
* {{cite web
  | last = Dellal
  | first = Pamela
  | authorlink = Pamela Dellal
  | url = http://www.emmanuelmusic.org/notes_translations/translations_cantata/t_bwv165.htm#pab1_7
  | title = BWV 165&nbsp;– O heilges Geist- und Wasserbad
  | year = 2012
  | publisher = Emmanuel Music
  | accessdate = 13 June 2014
  | ref = harv
  }}
* {{cite web
  | last = Gardiner
  | first = John Eliot
  | authorlink = John Eliot Gardiner
  | url = http://www.bach-cantatas.com/Pic-Rec-BIG/Gardiner-P27c%5Bsdg138_gb%5D.pdf
  | title = Cantatas for Trinity Sunday / St Magnus Cathedral, Kirkwall
  | publisher = bach-cantatas
  | format = PDF
  | year = 2008
  | accessdate = 30 May 2012
  | ref = harv
  }}
* {{cite web
| last = Grob
| first = Jochen
| url = http://www.s-line.de/homepages/bachdiskographie/textkangeist/bwv165text.html
| title = BWV 165 / BC A 90
| publisher = s-line.de
| year = 2014
| language = German
| accessdate = 20 February 2015
| ref = harv
}}
* {{cite web
  | last = Hofmann
  | first = Klaus
  | authorlink = Klaus Hofmann
  | url = http://www.bach-cantatas.com/Pic-Rec-BIG/Suzuki-C04c%5BBIS-CD801%5D.pdf
  | title = BWV 165: O heilges Geist- und Wasserbad / (O Holy Spiritual and Water Bath)
  | publisher = bach-cantatas
  | format = PDF
  | year = 1996
  | accessdate = 30 May 2012
  | ref = harv
  }}
* {{cite web
  | last = Mincham
  | first = Julian
  | url = http://www.jsbachcantatas.com/documents/chapter-62-bwv-165.htm
  | title = Chapter 62: BWV 165, O heilges Geist- und Wasserbad / Oh sacred spring and spirit.
  | publisher = jsbachcantatas.com
  | year = 2010
  | accessdate = 31 May 2012
  | ref = harv
  }}
* {{cite web
  | last = Oron
  | first = Aryeh
  | url = http://www.bach-cantatas.com/BWV165.htm
  | title = Cantata BWV 165 O heilges Geist- und Wasserbad
  | year = 2012
  | publisher = bach-cantatas
  | accessdate = 20 February 2015
  | ref = harv
  }}
* {{cite web
  | last = Werthemann
  | first = Helene
  | url = http://www.bachkantaten.ch/Daten12/06/Einfuehrung.htm
  | title = Bachkantaten in der Predigerkirche
  | year = 2015
  | publisher = Bachkantaten
  | language = German
  | accessdate = 7 April 2015
  | ref = harv
  }}

== External links ==
* {{cite book
  | last = Arnold
  | first = Jochen
  | url = https://books.google.com/books?id=YtZu6mJnokoC&pg=PA38
  | title = Von Gott poetisch-musikalisch reden: Gottes verborgenes und offenbares Handeln in Bachs Kantaten
  | publisher = [[Vandenhoeck & Ruprecht]]
  | year = 2009
  | isbn = 978-3-647-57124-9
  | language = German
  }}
* {{cite web
  | last = Koster
  | first = Jan
  | url = http://www.let.rug.nl/Linguistics/diversen/bach/weimar2.html
  | title = Weimar 1708–1717
  | publisher = let.rug.nl
  | year = 2011
  | accessdate = 16 December 2011
  }}
* {{cite web
  | last = Leonard
  | first = James
  | url = http://www.allmusic.com/composition/cantata-no-165-o-heilges-geist-und-wasserbad-bwv-165-bc-a90-mc0002356174
  | title = Johann Sebastian Bach  Cantata No. 165, "O heilges Geist- und Wasserbad," BWV 165 (BC A90)
  | publisher = AllMusic
  | year = 2015
  | accessdate = 7 April 2015
  }}
* {{cite book
  | last = Zedler
  | first = Günther
  | url = https://books.google.com/books?id=Vvc4cQMTFKkC&dq=BWV+172+d%C3%BCrr+Einf%C3%BChrung+in+die+Kantate+BWV+165&hl=de
  | title = Die erhaltenen Kirchenkantaten Johann Sebastian Bachs (Mühlhausen, Weimar, Leipzig I): Besprechungen in Form von Analysen – Erklärungen – Deutungen
  | publisher = BoD – Books on Demand
  | year = 2008
  | language = German
  | page = 102
  | isbn = 978-3-8370-4401-0
  }}

{{Church cantatas by Johann Sebastian Bach}}
{{Bach cantatas|state=expanded}}
{{authority control}}
{{DISPLAYTITLE:''O heilges Geist- und Wasserbad'', BWV 165}}

{{DEFAULTSORT:O Heilges Geist Und Wasserbad Bwv 165}}
[[Category:1715 compositions]]
[[Category:Cantatas by Johann Sebastian Bach]]