{{Infobox journal
| title         = Food Quality and Preference
| cover         = 
| editor        = Armand V. Cardello, Sara R. Jaeger, John Prescott 
| discipline    = [[Food Science]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = Food Qual Prefer
| publisher     = Elsevier
| country       = 
| frequency     = 8 issues a year
| history       = 1988 - Present
| openaccess    = 
| license       = 
| impact        = 2.727
| impact-year   = 2013
| website       = http://www.journals.elsevier.com/food-quality-and-preference
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0950-3293
| eISSN         = 
| boxwidth      = 
}}

{{Italic title}}
'''''Food Quality and Preference''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of sensory and consumer science, published in 8 issues per year by [[Elsevier]]. 

Its scope covers consumer and market research, sensory science, sensometrics and sensory evaluation, nutrition and food choice, as well as food research, product development and sensory quality assurance. It is the official journal of the [http://www.sensometric.org/ The Sensometric Society] and the [http://www.e3sensory.eu/ The European Sensory Science Society].

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.272.<ref name=WoS>{{cite book |year=2014 |chapter=Food Quality and Preference |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2014-10-26 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.elsevier.com/wps/find/journaldescription.cws_home/405859/description#description}}

[[Category:Elsevier academic journals]]
[[Category:Food studies journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1988]]