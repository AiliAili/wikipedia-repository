{{Infobox airport
| name = Dickson Field
| FAA = 75WT
| type = Private
| owner-oper = Robert Dickson
| location = [[Oroville, Washington|Oroville]], [[Washington (state)|Washington]], [[United States]]
| city-served = [[Okanogan County]]
| elevation-f = 3,214
| coordinates = {{coord|48|59|3|N|119|17|54|W|region:US-WA_type:airport|display=inline,title}}
| image_map = Washington in United States.svg
| image_map_alt = A western state is situated in a country, which is highlighted in a purple-like color.
| image_map_caption = Location of the airport's state, Washington, United States
| r1-number = 17/35
| r1-length-f = 500
| r1-length-m = 152
| r1-surface = [[Lawn|Turf]]
| footnotes = Source: [[Federal Aviation Administration]]<ref name="FAA">{{cite web |url=http://nfdc.faa.gov/nfdcApps/airportLookup/airportDisplay.jsp?category=nasr&airportId=75W5 |title=FAA Airport Master Record for 75WT |publisher=[[Federal Aviation Administration]] |accessdate=December 26, 2012 |date=December 26, 2012}}</ref>
}}
'''Dickson Field''' {{airport codes|||75WT}} is a private airport located {{convert|7|mi|km}} east of [[Oroville, Washington|Oroville]], [[Washington (state)|Washington]], which is a city in the [[Okanogan County|Okanogan]] region of [[United States]]. It provides private [[general aviation]] services and has a {{convert|500|by|40|ft|abbr=on|0}} runway numbered 17–35, but the nearby [[Dorothy Scott Airport]] handles most of the city's aviation services.

== History ==
It is owned and operated by Robert Dickson, serving the Okanogan area itself.<ref name="FAA"/> As it is a private airport, permission is required prior to landing at the Dickson Field.<ref name="AirNav">{{cite web |url=http://www.airnav.com/airport/75WT |title=75WT – Dickson Field Airport |publisher=[[AirNav]] |accessdate=December 26, 2012}}</ref> Rights for permanent public use of the Dickson Field were approved in January&nbsp;2007.<ref name="AirNav"/> The airport obtained no passengers in 2011.<ref name="FAA"/>

No accidents or incidents have occurred at the Dickson Field throughout its history. [[Dorothy Scott Airport]]—a public airport situated in the same city—is generally used for the city's public aviation services, having been classified as an [[airport of entry]] by the [[Federal Aviation Administration]] (FAA).<ref>{{cite web |url=https://nfdc.faa.gov/nfdcApps/airportLookup/airportDisplay.jsp?category=nasr&airportId=0S7 |title=FAA Airport Master Record for 75WT |publisher=[[Federal Aviation Administration]] |accessdate=December 26, 2012}}</ref>

== Facilities ==
The airport is {{coord|48|59|3|N|119|17|54|W|region:US-WA_type:airport|display=inline}} off of 9 Mile Road and [[Canada]]'s [[Crowsnest Highway]]. The airport maintains no facilities or services, and is generally used for private general aviation services.<ref name="FAA"/> Its airfield has a {{convert|500|by|40|ft|abbr=on|0}} runway numbered 17–35.<ref name="FAA"/> The nearest radio navigation aids from the airport that help the pilot are located in three cities: [[Omak]], [[Penticton]], [[Naramata, British Columbia|Naramata]].<ref name="AirNav"/>

== See also ==
{{Portal|Aviation|Transport|United States|Washington}}
* [[List of airports in Washington]]

== References ==
{{Reflist}}

== External links ==
* {{US-airport-minor|75WT}}

{{Airports in Washington}}

[[Category:Airports in Washington (state)]]
[[Category:Buildings and structures in Okanogan County, Washington]]
[[Category:Transportation in Okanogan County, Washington]]