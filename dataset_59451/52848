{{Infobox journal
| title = Small Group Research
| cover = [[File:Small Group Research.tif]]
| editor = Aaron M. Brower, Joann Keyton
| discipline = [[Social psychology]]
| former_names = 
| abbreviation = Small Group Res.
| publisher = [[SAGE Publications]]
| country = 
| frequency = Bimonthly 
| history = 1970-present
| openaccess = 
| license = 
| impact = 1.345
| impact-year = 2011
| website = http://sgr.sagepub.com/
| link1 = http://sgr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://sgr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1046-4964
| eISSN = 1552-8278
| OCLC = 41205740
| LCCN = 90643145
}}

'''''Small Group Research''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that covers research in the field of [[social psychology]], dealing with the [[psychology]] and  organizational behavior of small groups and communication processes within them. The [[editors-in-chief]] are Aaron M. Brower ([[University of Wisconsin]]) and Joann Keyton ([[North Carolina State University]]). It was established in 1970 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Small Group Research'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2011 [[impact factor]] is 1.345, ranking it 28th out of 59 journals in the category "Psychology, Social",<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Psychology, Social |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> 72nd out of 166 journals in the category "Management",<ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Management |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref> and 35th out of 72 journals in the category "Psychology, Applied".<ref name=WoS2>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Psychology, Applied |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://sgr.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Social psychology journals]]
[[Category:Publications established in 1970]]