{{Infobox organization
| name = American Society for Biochemistry and Molecular Biology
| logo = 
| type =
| founded_date = December 26th, 1906
| founder = [[John Jacob Abel]]
| location = [[Rockville, Maryland]]
| origins =
| key_people = Steven McKnight: President<br />Toni Antalis: Treasurer<br />Karen Allen: Secretary
| area_served =
| product =
| focus =
| method =
| revenue =
| endowment =
| num_volunteers =
| num_employees = 41
| num_members = 12,000
| subsid =
| owner =
| Non-profit_slogan =
| homepage = [http://www.asbmb.org www.asbmb.org]
| dissolved =
| footnotes =
}}
The '''American Society for Biochemistry and Molecular Biology''' (ASBMB) is a [[learned society]] that was founded on December 26, 1906 at a meeting organized by [[John Jacob Abel]] ([[Johns Hopkins University]]).<ref>Bradshaw, R. A., Hancock, C. C., Kresge, N. (2009) The ASBMB Centennial History: 100 Years of the Chemistry of Life</ref> The roots of the society were in the [[American Physiological Society]], which had been formed some 20 years earlier.

The ASBMB was originally called the '''American Society of Biological Chemists''', before obtaining its current name in 1987. The society is based in [[Rockville, Maryland]]. ASBMB's mission is to advance the science of [[biochemistry]] and [[molecular biology]] through publication of [[scientific journal|scientific]] and educational journals, the organization of scientific meetings, advocacy for funding of basic research and education, support of science education at all levels, and by promoting the diversity of individuals entering the scientific workforce. The organization currently has over 12,000 members.

== Publications ==
The American Society for Biochemistry and Molecular Biology publishes three research journals and a monthly magazine covering society news and activity updates. ASBMB journals are [[Peer review|peer-reviewed]] and cover research in the fields of [[microbiology]], molecular genetics, [[RNA]]-related research, [[proteomics]], [[genomics]], transcription, peptides, [[cell signaling]], lipidomics, and [[systems biology]]. All articles are published online as "Papers in Press" upon acceptance.

* The ''[[Journal of Biological Chemistry]]'' publishes research in any area of biochemistry or molecular biology, both in print and online, weekly.
* ''[[Molecular & Cellular Proteomics]]'' is a monthly online only publication. Articles appearing in MCP "...describe the structural and functional properties of proteins and their expression, particularly with respect to the developmental time courses of the organism of which they are a part."<ref>[http://mcponline.org/misc/mission.dtl MCP Mission Statement]</ref> The journal also publishes other content such as "HUPO views" (reports from the [[Human Proteome Organization]]),<ref>[http://www.mcponline.org/cgi/content/full/6/6/951 Ralph A. Bradshaw and Alma L. Burlingame,"Welcome, HUPO" ''Molecular & Cellular Proteomics'' 6:951, 2007.]</ref><ref>[http://www.mcponline.org/cgi/content/full/6/6/951-a|Peipei Ping, Rolf Apweiler and John Bergeron,MCP and HUPO: An Era of New Partnership, ''Molecular & Cellular Proteomics'' 6:951-952, 2007.]</ref> proceedings from HUPO meetings,<ref>[http://www.mcponline.org/content/vol6/issue6/ MCP Supplement: HUPO 4th Annual World Congress, August 29-September 1, 2005, Munich]</ref> and the proceedings of the International Symposium On Mass Spectrometry In The Life Sciences.<ref>[http://www.mcponline.org/content/vol7/issue4/ Special Issue: 8th International Symposium On Mass Spectrometry In The Life Sciences]</ref>
* The ''[[Journal of Lipid Research]]'' covers "...the science of [[lipid]]s in health and disease. The Journal emphasizes lipid function and the biochemical and [[Genetics|genetic]] regulation of lipid [[metabolism]]. In addition, JLR publishes manuscripts on patient-oriented and [[epidemiology|epidemiological]] research relating to altered lipid metabolism, including modification of dietary lipids."<ref>[http://www.jlr.org/site/home/about/index.xhtml About ''The Journal of Lipid Research'': AIM AND SCOPE]</ref>
* ''[[ASBMB Today]]'' is the society's monthly news magazine. It contains extensive coverage of awards, meetings, research highlights, job placement advertising and human interest articles. All ASBMB members receive a complimentary subscription to ''ASBMB Today''.

== Meetings ==
ASBMB hosts and sponsors numerous meetings each year. The annual meeting is held each April in conjunction with the [[Experimental Biology meeting]]. Additionally, themed special symposia are organized throughout the year.

==Awards==
The Mildred Cohn Award in Biological Chemistry was established in 2013 to honor the scientific achievements of [[Mildred Cohn]], the first female president of the society. The award of $5,000 is presented annually to a scientist who has made substantial advances in understanding biological chemistry using innovative physical approaches. The recipient is expected to deliver the Mildred Cohn Award lecture at the annual meeting. <ref> {{cite web|url=http://www.asbmb.org/Page.aspx?id=16225|title=Mildred Cohn Award in Biological Chemistry|publisher=American Society for Biochemistry and Molecular Biology|accessdate= 1 May 2016}} </ref>

Recipients:

Source: [http://www.asbmb.org/awards/cohn/ American Society for Biochemistry and Molecular Biology]
*2016 – [[Eva Nogales]]
*2015 - [[Judith P. Klinman]]
*2014 - Lila M. Gierasch
*2013 - [[Jennifer Doudna|Jennifer A. Doudna]]

== Advocacy ==
The Public Affairs Office works with the PAAC to advocate for increased research budgets for the major governmental funding agencies, primarily the [[National Institutes of Health]] and the [[National Science Foundation]]. ASBMB has developed a set of recommendations<ref>[http://www.asbmb.org/asbmbtoday/asbmbtoday_article.aspx?id=16052 Charles Brenner and Dagmar Ringe, "Response to the New MCAT:  ASBMB Premedical Curriculum Recommendations" "ASBMB Today" 12-14, March 2012]</ref> for [[pre-medical]] course requirements consistent with the new [[Medical College Admission Test]]. Advocacy efforts also focus on protecting the conditions that promote a successful research environment. In addition, the office works to maintain a healthy relationship between ASBMB members, government officials, and the public in order to foster awareness of the importance of science to everyday life.

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.asbmb.org/}}
* [http://aok.lib.umbc.edu/specoll/ASBMB/index.php American Society for Biochemistry and Molecular Biology records, 1906-2005] at the University of Maryland

==See also==
*[[Belgian Society of Biochemistry and Molecular Biology]]
*[[International Union of Biochemistry and Molecular Biology]]

{{DEFAULTSORT:American Society For Biochemistry And Molecular Biology}}
[[Category:Molecular biology organizations]]
[[Category:Biology societies]]
[[Category:Chemistry societies]]
[[Category:Scientific organizations based in the United States]]
[[Category:Organizations based in Maryland]]
[[Category:Scientific organizations established in 1906]]
[[Category:1906 establishments in Maryland]]