{{Infobox journal
| title = Current Opinion in Neurology
| cover = [[File:2013 cover Curr Opin Neurol.jpg]]
| editor = Richard S.J. Frackowiak
| discipline = [[Neurology]]
| former_names = Current Opinion in Neurology and Neurosurgery
| abbreviation = Curr. Opin. Neurol.
| publisher = [[Lippincott Williams & Wilkins]]
| country =
| frequency = Bimonthly
| history = 1988-present
| openaccess =
| license =
| impact = 5.307
| impact-year = 2014
| website = http://journals.lww.com/co-neurology/pages/default.aspx
| link1 = http://journals.lww.com/co-neurology/pages/currenttoc.aspx
| link1-name = Online access
| link2 = http://journals.lww.com/co-neurology/pages/issuelist.aspx
| link2-name = Online archive
| JSTOR =
| OCLC = 716432133
| LCCN = sn93003117
| CODEN = CONEEX
| ISSN = 1350-7540
| eISSN = 1473-6551
}}
'''''Current Opinion in Neurology''''' is a bimonthly [[Peer review|peer-reviewed]] [[medical journal]] covering [[neurology]]. The journal publishes [[editorial]]s and [[review]]s, but not [[original research]] articles. It is published by [[Lippincott Williams & Wilkins]] and the [[editor-in-chief]] is Richard S.J. Frackowiak ([[University College London]]). The journal was established in 1988 as ''Current Opinion in Neurology and Neurosurgery'' and obtained its current name in 1993.<ref name=PubMed>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/9319162 |title=Current Opinion in Neurology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2013-10-31}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Index medicus]]/[[MEDLINE]]/[[PubMed]],<ref name=PubMed /> [[Science Citation Index]], [[Current Contents]]/Clinical Medicine, and [[BIOSIS Previews]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2013-10-31}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.307.<ref name=WoS>{{cite book |year=2015 |chapter=Current Opinion in Neurology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>
==See also==
*[[Current Opinion (Lippincott Williams & Wilkins)]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.lww.com/co-neurology/pages/default.aspx}}

[[Category:Neurology journals]]
[[Category:Publications established in 1988]]
[[Category:Bimonthly journals]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:English-language journals]]