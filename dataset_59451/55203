{{Infobox journal
| cover = 
| editor = [[Kavita Bala]]
| discipline = [[Computer graphics (computer science)|computer graphics]]
| language = English
| abbreviation = ACM Trans. Graph.
| publisher = [[Association for Computing Machinery|ACM]]
| frequency = Bimonthly
| history = 1982–present
| website = http://www.acm.org/tog
| link1 = http://portal.acm.org/tog
| link1-name = Online access
| link2 = http://portal.acm.org/tog/archive
| link2-name = Online archive
| ISSN = 0730-0301
| eISSN = 1557-7368
| OCLC = 7941481
}}
'''''ACM Transactions on Graphics''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] that covers the field of [[Computer graphics (computer science)|computer graphics]]. It was established in 1982 and is published by the [[Association for Computing Machinery]]. Starting in 2003, all papers accepted for presentation at the annual [[SIGGRAPH]] conference are printed in a special summer issue of the journal. Beginning in 2008, papers presented at [[SIGGRAPH#SIGGRAPH events around the world|SIGGRAPH Asia]] are printed in a special November/December issue.

In a 2008 ranking, the journal had the 5th highest [[impact factor]] in the field of [[computer science]].<ref>{{cite web
| title=Top journals in computer science
| date=14 May 2009
| work=[[Times Higher Education]]
| accessdate=22 August 2009
| url=http://www.timeshighereducation.co.uk/story.asp?sectioncode=26&storycode=406557
}}</ref> A more recent ranking by [[Thomson Reuters InCites]] gave the journal an impact factor of 3.725, placing it first among [[software engineering]] journals.{{citation needed|date=June 2016}}

==References==
{{reflist}}

==External links==
*{{Official website|http://tog.acm.org/}}


{{DEFAULTSORT:Acm Transactions on Graphics}}
[[Category:Computer graphics]]
[[Category:Computer science journals]]
[[Category:Association for Computing Machinery academic journals|Transactions on Graphics]]
[[Category:Bimonthly journals]]


{{compu-journal-stub}}