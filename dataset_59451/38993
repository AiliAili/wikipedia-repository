{{Use mdy dates|date=July 2014}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Burning Up
| Cover          = Madonna-Burning-Up-vinyl-single-twelve-inch.jpg
| Caption        =
| Alt            = A montage of Madonna's face. The images are arranged in square boxes in four rows, each row consisting of five boxes. The color of the images in the boxes are different and are made to appear as if they are painted.
| Artist         = [[Madonna (entertainer)|Madonna]]
| Album          = [[Madonna (Madonna album)|Madonna]]
| Border         = yes
| B-side         = "Physical Attraction"
| Released       = {{Start date|1983|3|9}}
| Format         = {{hlist|[[Gramophone record|7"]]|[[12-inch single|12"]]}}
| Recorded       = 1982
| Studio         = [[Sigma Sound Studios]]<br /><small>([[New York City]], [[New York (state)|New York]])</small>
| Genre          = {{hlist|[[Dance-rock]]<ref>{{harvnb|Fouz-Hernández|Jarman-Ivens|2004|p=190}}</ref>|[[post-disco]]<ref>{{harvnb|Brackett|Hoard|2004|p=508}}</ref>}}
| Length         = 3:45
| Label          = {{hlist|[[Sire Records|Sire]]|[[Warner Bros. Records|Warner Bros.]]}}
| Writer         = Madonna
| Producer       = {{hlist|[[Reggie Lucas]]}}
| Last single    = "[[Everybody (Madonna song)|Everybody]]"<br/>(1982)
| This single    = "'''Burning Up'''"<br/>(1983)
| Next single    = "[[Holiday (Madonna song)|Holiday]]"<br/>(1983)
| Misc           = {{External music video|{{YouTube|pufec0Hps00|"Burning Up"}}}}
}}

"'''Burning Up'''" is a song by American singer [[Madonna (entertainer)|Madonna]] from her eponymous debut [[studio album]] ''[[Madonna (Madonna album)|Madonna]]'' (1983). It was released as the album's second [[Single (music)|single]] on March 9, 1983, in some countries as a double-A side single with "'''Physical Attraction'''". The song was presented as an early recorded demo by Madonna to [[Sire Records]] who green-lighted the recording of the single after the first single "[[Everybody (Madonna song)|Everybody]]" became a dance hit. Madonna collaborated with [[Reggie Lucas]], who produced the single while [[John Benitez]] provided the guitar riffs and backing vocals. Musically, the song incorporates instrumentation from bass guitar, synthesizers and drums, and the lyrics talk of the singer's lack of shame in declaring her passion for her lover.

Released with "Physical Attraction" on the B side, the song was given mixed reviews from contemporary critics and authors, who noted the song's darker, urgent composition while praising its dance beats. The single failed to do well commercially anywhere, except the dance chart in the United States, where it peaked at three, and the Australian charts, where it was a top 20 hit. After a number of live appearances in clubs to promote the single, it was added to the set-list of the 1985 [[The Virgin Tour|Virgin Tour]]. An electric guitar version was performed on the 2004 [[Re-Invention World Tour]] and the 2015–16 [[Rebel Heart Tour]].

The accompanying music video of the song portrayed Madonna in the classic submissive female positions, while writhing in passion on an empty road, for her lover who appeared to come from her behind on a car. The video ended showing Madonna driving the car instead, thereby concluding that she was always in charge. Many authors noted that the "Burning Up" music video was a beginning of Madonna's depiction of her taking control of a destabilized male sexuality.

==Background==
In 1982, Madonna was living in New York and trying to launch her musical career. Her Detroit boyfriend, [[Stephen Bray|Steve Bray]], became the drummer for her band. Abandoning hard rock, they were signed by a music management company, Gotham records, and decided to pursue music in the funk genre. They soon dropped those plans.<ref name="rikky1">{{harvnb|Rooksby|2004|p=9}}</ref> Madonna carried rough tapes of three songs with her: "[[Everybody (Madonna song)|Everybody]]", "Ain't No Big Deal" and "Burning Up". Madonna presented "Everybody" to the DJ [[Mark Kamins]] who, after hearing the song, took her to [[Sire Records]], where she was signed for a single deal.<ref name="rikky2">{{harvnb|Rooksby|2004|p=10}}</ref> When "Everybody" became a dance hit, Sire Records decided to follow up with an album for her. However, Madonna chose not to work with either Bray or Kamins, opting instead for Warner Brothers producer [[Reggie Lucas]]. Michael Rosenblatt, the A&R director of Sire Records, explained to Kamins that they wanted a producer who had more experience in directing singers; hence they appointed Lucas.<ref name="morton1">{{harvnb|Morton|2002|p=255}}</ref> He pushed Madonna in a more pop direction and produced "Burning Up" and "Physical Attraction" for her.<ref name="rikky2"/>

While producing the tracks, Lucas radically changed their structure from the original demo versions. Madonna did not accept the changes, so [[John Benitez|John "Jellybean" Benitez]], a DJ at the Funhouse Disco, was called in to remix the tracks.<ref name="rikky2"/> He added some extra guitar riffs and vocals to "Burning Up".<ref name="rikky2"/> Sire Records backed up the single by sending Madonna on a series of personal appearances in clubs around New York, where she performed the single. They also hired a stylist and jewelry designer called Maripol, who helped Madonna with the single cover.<ref name="clerk1">{{harvnb|Clerk|2002|p=36}}</ref> The cover for the 12-inch dance single for "Burning Up" was designed by Martin Burgoyne.<ref name="morton1"/>

==Composition==
{{listen
 | pos          = left
 | filename     = Madonna-burning up.ogg
 | title        = "Burning Up"
 | description  = The [[chorus effect|chorus]] of "Burning Up", which is repeated three times, while being backed by a single guitar arrangement.
}}
Musically, "Burning Up" has a starker arrangement brought about by [[bass (sound)|bass]], [[Guitar|single guitar]] and [[drum machine]].<ref name="rikky3">{{harvnb|Rooksby|2004|p=12}}</ref> The guitar riffs in the songs were not characteristics of Madonna's later records. The [[tom-tom drum]] beats used in the song were reminiscent of the records of singer [[Phil Collins]].<ref name="rikky3"/> It also incorporated [[electric guitar]]s and the most state-of-the-art synthesizers of that time.<ref name="slant"/> The [[chorus effect|chorus]] is a repetition of the same three lines of the lyrics, while the [[Bridge (music)|bridge]] consists of a series of double entendres in regards to the lyrics of the song which describes what she is prepared to do for her lover and that she is individualistic and shameless.<ref name="rikky3"/>

According to the sheet music published at Musicnotes.com by [[Alfred Publishing]], "Burning Up" is written in the [[time signature]] of common time with a dance beat [[tempo]] of 138 [[beats per minute]]. The song is composed in the [[key (music)|key]] of [[B minor]], with Madonna's vocals ranging from the tonal nodes of A<sub>3</sub> to B<sub>4</sub>. "Burning Up" follows a basic sequence of Bm–Bm–A–E as its [[chord progression]].<ref name="sheet">{{cite journal|last=Ciccone|first=Madonna|title=Digital Sheet Music&nbsp;– Madonna&nbsp;– Burning Up|year=1983|publisher=Musicnotes.com. [[Alfred Publishing]]}}</ref>

==Release and reception==
[[File:BurningUpJason.jpg|thumb|Madonna performing "Burning Up" during the [[Re-Invention World Tour]] (2004)|alt=Madonna wearing olive green clothes sings to a microphone while holding a black electric guitar in her hands.]]
"Burning Up" was released on March 9, 1983.<ref>{{Cite web|url=http://www.madonna.com/discography/index/album/albumId/32/|title=Madonna Burning Up|publisher=Icon: Official Madonna Website|accessdate=March 3, 2013}}</ref> Like its predecessor "Everybody", the song failed to enter the [[Billboard Hot 100|''Billboard'' Hot 100]] chart, and "Burning Up" also did not chart in the [[Bubbling Under Hot 100 Singles]] chart.<ref>{{harvnb|Grant|Neupert|2003|p=4}}</ref> It did manage to peak at number three on the ''[[Billboard (magazine)|Billboard]]'' [[Hot Dance Club Songs|Hot Dance Club Play]], staying on the chart for 16 weeks.<ref name="dance">{{harvnb|Grant|Neupert|2003|p=9}}</ref> The song was a top 20 hit in Australia in June 1984, peaking at number 13, after having originally charted in the lower reaches of the top 100 in November 1983.<ref name="kent">{{cite book|last=Kent|first=David|authorlink=David Kent (historian)|title=Australian Chart Book 1970–1992|edition=illustrated|publisher=Australian Chart Book|location=St Ives, N.S.W.|year=1993|isbn=0-646-11917-6|page=188}} N.B. The Kent Report chart was licensed by [[Australian Recording Industry Association|ARIA]] from mid 1983 until June 19, 1988.</ref> The song was also used as background music for a scene in the 1984 film ''[[The Wild Life (1984 film)|The Wild Life]]''.<ref name="rikky3"/>

Author Rikky Rooksby, in his book ''The Complete Guide to the Music of Madonna'', commented that the song was noticeably weaker compared to other singles like "[[Lucky Star (Madonna song)|Lucky Star]]" and "[[Borderline (song)|Borderline]]".<ref name="rikky3"/> Sal Cinquemani of [[Slant Magazine]] denoted the track as edgy and punk-infused.<ref name="slant">{{cite web|url=http://www.slantmagazine.com/oldurlredirect.php?type=music&ID=118|title=Madonna (Remastered)|last=Cinquemani|first=Sal|date=September 9, 2001|publisher=[[Slant Magazine]]|accessdate=July 1, 2009}}</ref> [[Stephen Thomas Erlewine]] from [[Allmusic]] commented that "Burning Up" and B side "Physical Attraction" had a darker, carnal urgency in their composition.<ref>{{cite web|url={{Allmusic|class=album|id=r12203|pure_url=yes}}|title=Madonna&nbsp;— Overview |last=Erlewine|first=Stephen Thomas|authorlink=Stephen Thomas Erlewine|date=July 3, 1998|publisher=[[AllMusic]]. [[Rovi Corporation]]|accessdate=July 2, 2009}}</ref> Don Shewey from ''[[Rolling Stone]]'' called the song "simple stuff" while complimenting the B side, saying: "'Physical Attraction' is practically a capsule history of high-school proms, with its sly references to [[The Association]]'s "[[Cherish (The Association song)|Cherish]]" and [[Olivia Newton-John]]'s "[[Physical (Olivia Newton-John song)|Physical]]."<ref>{{cite web|url=http://www.rollingstone.com/music/reviews/album/3045/21667|title=Madonna: Madonna Music Review|last=Shewey|first=Don|date=September 29, 1983|work=[[Rolling Stone]]|publisher=[[Jann Wenner]]|accessdate=July 2, 2009}}</ref> [[Robert Christgau]] called the 12-inch pair of "Burning Up" and "Physical Attraction" electroporn.<ref>{{cite web|url=http://www.robertchristgau.com/get_artist.php?name=Madonna |title=Consumer Guide Reviews: Madonna |last=Christgau |first=Robert |date=February 4, 1983 |work=[[Robert Christgau]] |accessdate=July 2, 2009 |deadurl=yes |archiveurl=https://web.archive.org/web/20110606103847/http://www.robertchristgau.com/get_artist.php?name=madonna |archivedate=June 6, 2011 |df=mdy }}</ref> Santiago Fouz-Hernández in his book ''Madonna's drowned worlds'' complimented the song for having upbeat dance music.<ref>{{harvnb|Fouz-Hernández|Jarman-Ivens|2004|p=59}}</ref> Jim Farber from ''[[Entertainment Weekly]]'' commented that "Burning Up" proved that Madonna could rock also.<ref>{{Cite news|last=Farber|first=Jim|title=Music News: The Girl Material|url=http://www.ew.com/ew/article/0,,255548,00.html|work=[[Entertainment Weekly]]|publisher=[[Time Inc.]]|date=July 27, 2001|accessdate=March 11, 2009}}</ref>

==Music video==
[[File:Burningupmusicvideo.jpg|left|thumb|alt=Refer to caption.|Madonna in a white dress lying on the road while writhing in passion for her lover, in the music video for "Burning Up"]]

Sire Records commissioned a music video for the song to be directed by [[Steve Barron]]. Madonna's friend [[Debi Mazar]] was hired as the make-up artist for the video while Maripol was the stylist with Madonna's then boyfriend Ken Compton appearing as her onscreen lover. By the time the video was released, [[MTV]] had begun to show dance music videos. Hence the music video of "Burning Up" became a minor hit on the channel.<ref name="morton2"/> The narrative of the video shows Madonna in a white dress, as she sings the song proclaiming her helpless passion for her lover.<ref name="allen1">{{harvnb|Allen|1987|p=281}}</ref> She wore her famous rubber bracelets which were actually typewriter belts.<ref name="rikky3"/> Her love for the boy portrayed her as a helpless victim like the stereotyped female portrayed in many silent movies. At one point in the video Madonna is shown being hit by a car driven by a young man, played by Compton. By the end of the song Madonna is shown driving the car, with a knowing, defiant smile on her lips and has ditched the man, thereby giving the message that she was in charge, a theme recurrent throughout her career.<ref name="morton2"/> Baron explained the development process behind the video:
<blockquote>
I went to New York to meet (Madonna), begrudgingly, and showed up at an address at SoHo, which turned out to be a squat basically. Madonna was scantily clad, working out to a massive disco track. She was charismatic. She kept putting her head down on the table and talking to me, very flirtatious, and that gave me the idea for the scene in "burning up", where her face is on the road, and the camera's really low and close.<ref>{{harvnb|Tannenbaum|Marks|2011|p=15}}</ref>
</blockquote>
Though the lyrics of the song like "Do you want to see me down on my knees?" portray female helplessness, the video performance acts as a counter-text to it.<ref name="allen1"/> When this line is sung, Madonna is shown kneeling on the road in front of the advancing car, then turns her head back while exposing her throat back in a posture of submission. However, her voice tone and her look at the camera portray a hardness and defiance that contradict the submissiveness of her body posture and turn the question of the line into a challenge for her lover.<ref name="allen1"/>

Author [[Andrew Morton (writer)|Andrew Morton]], in his [[Madonna (book)|biography on Madonna]], commented that the video was America's first introduction to Madonna's sexual politics.<ref name="morton2">{{harvnb|Morton|2002|p=256}}</ref> Author Robert Clyde Allen in his book ''Channels of Discourse'' compared the video with that of "[[Material Girl]]". According to him both the videos have an undermining ending, while employing a consistent series of puns and exhibiting a parodic amount of excess associated with Madonna's style. The discourses included in the video are those of sexuality and religion. Allen wrote that Madonna's image of kneeling and singing about 'burning in love' performed the traditional ideological work of using the subordination and powerlessness of women in [[Christianity]] to naturalize their equally submissive position in patriarchy.<ref name="allen1"/> Author Georges-Claude Guilbert in his book ''Madonna as postmodern myth'' commented that the representation of the male character becomes irrelevant as Madonna destabilizes the fixing and categorization of male sexuality in the video.<ref>{{harvnb|Guilbert|2002|p=79}}</ref> Her utterance of having "no shame" was interpreted by author James B. Twitchell, in his book ''For Shame'', as an attempt to separate herself from contemporary female artists of that era.<ref>{{harvnb|Twitchell|1998|p=109}}</ref>

==Live performances and covers==
{{See also|List of cover versions of Madonna songs|List of Madonna tribute albums}}
[[File:7179-Madonna-Rebel-Heart-Tour (26399370813).jpg|thumb|upright|Madonna performing "Burning Up" during the [[Rebel Heart Tour]] (2015-16)|alt=Refer to caption]]
Before its release, Madonna promoted the single by performing at different clubs around New York.<ref name="clerk1"/> Madonna was a professional performer by that time and was assisted by dancers Erika Belle and Bags Rilez to promote it.<ref name="morton2"/> After promoting in New York in numerous nightclubs and pubs, she traveled to London to promote it in clubs like [[Heaven (nightclub)|Heaven]], [[Koko (venue)|Camden Palace]], Beatroot Club as well as [[The Haçienda]] in Manchester. However, those performances were not well received by the British audience.<ref>{{harvnb|Morton|2002|p=258}}</ref> The song was performed on [[The Virgin Tour]] in 1985 but was omitted from the ''[[Madonna Live: The Virgin Tour]]'' VHS released by Warner Home Video.<ref>{{cite video|people=Madonna |title=[[Madonna Live: The Virgin Tour]]|medium=VHS|publisher= Warner Home Video |date=1985|id={{UPC|38105-3}}}}</ref> [[Jon Pareles]] from ''[[The New York Times]]'' felt that Madonna posed like [[Marilyn Monroe]] during the performance of the song.<ref>{{cite news|url=https://query.nytimes.com/gst/fullpage.html?res=9A0CE0DF163BF930A25752C0A963948260|title=Recent Releases: Madonna|date=January 13, 1985|accessdate=November 16, 2010|last=Pareles|first=Jon|authorlink=Jon Pareles|work=[[The New York Times]]}}</ref> Mikel Longoria from ''[[The Dallas Morning News]]'' called the performance as "crisp and energetic".<ref>{{cite news|work=[[The Dallas Morning News]]|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=DM&p_theme=dm&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0ED3CD5E603F05D7&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Madonna's material whirl stirs Convention Center|last=Longoria|first=Mikel|date=May 6, 1985|accessdate=November 16, 2010}}</ref>

Madonna included the song on the set-list of her 2004 [[Re-Invention World Tour]] in the military segment. She was dressed in military garments and played the [[electric guitar]] for the performance. As she sang the song, the backdrops displayed scenes of war and sex which were scrambled to appear as if they have been shot with a camcorder. Kelefa Sanneh from ''The New York Times'' described the performance and the video backdrops as being reminiscent of the prisons in [[Abu Ghraib]].<ref>{{cite web|url=https://www.nytimes.com/2004/05/26/arts/pop-review-madonna-s-latest-self-a-mix-of-her-old-ones.html|title=Madonna's Latest Self, a Mix of Her Old Ones|last=Sanneh|first=Kelefa|date=May 26, 2004|work=The New York Times|accessdate=July 2, 2009}}</ref> Sal Cinquemani from ''[[Slant Magazine]]'' commented that "it was a hoot to see her [Madonna] strap on an electric guitar and sing classics like 'Burning Up'."<ref>{{cite web|url=http://www.slantmagazine.com/music/features/reinventiontour.asp|title=Madonna: Live @ Madison Square Garden |last=Cinquemani|first=Sal|date=October 17, 2004|work=Slant Magazine|accessdate=July 2, 2009}}</ref> Madonna also included a remixed version of the song on the 2015 [[Rebel Heart Tour]]. Similar to the Re-Invention World Tour, the singer played the [[electric guitar]] during the performance, dressed in a black short nun's outfit.<ref>{{cite web|last1=Quarles|first1=Alicia|title=Madonna kicks off 'Rebel Heart' tour in Montreal|url=http://www.kentucky.com/2015/09/10/4028824/madonna-kicks-off-rebel-heart.html|work=[[Lexington Herald-Leader]]|accessdate=September 12, 2015|date=September 10, 2015}}</ref>

During Madonna's induction at the 2008 [[Rock and Roll Hall of Fame]], "Burning Up" was performed by [[Iggy Pop]] and the punk rock band [[The Stooges]], along with "[[Ray of Light (song)|Ray of Light]]".<ref>{{cite news|url=http://www.billboard.com/articles/news/1046300/madonna-mellencamp-lead-rock-halls-08-class|title=Madonna, Mellencamp Lead Rock Hall's '08 Class|last=Cohen|first=Jonathan|date=March 11, 2008|work=[[Billboard (magazine)|Billboard]]|publisher=Nielsen Business Media, Inc|accessdate=July 2, 2009}}</ref> In 2010, [[Jonathan Groff]] covered the song for American television show ''[[Glee (TV series)|Glee]]''. His version was included in the [[extended play]] titled ''[[Glee: The Music, The Power of Madonna]]'', and it was also released as a bonus track to the [[iTunes Store]].<ref>{{cite web|url=http://itunes.apple.com/us/preorder/glee-the-music-power-madonna/id363320646|title=Glee&nbsp;– The Music, the Power of Madonna|publisher=iTunes|accessdate=April 13, 2010}}</ref> Singer [[Britney Spears]] covered "Burning Up" in 2011 during select shows, for her [[Femme Fatale Tour]].<ref>{{cite web|url=http://buzzworthy.mtv.com/2011/06/10/britney-spears-cover-madonna-burning-up/|title=Listen To Britney Spears Cover Madonna's 'Burning Up'|last=Peck|first=Jamie|date=June 10, 2011|accessdate=June 10, 2011|publisher=[[MTV]] ([[MTV Networks]])}}</ref> The performance featured her straddling a giant, glittering guitar, ten feet high and twice as long. However, Barry Walters from ''[[Rolling Stone]]'' felt that the cover she sang on the tour, "lacked Madge's  [Madonna's] authority."<ref>{{cite news|url=http://www.rollingstone.com/music/news/britney-spears-proves-shes-still-got-it-at-femme-fatale-tour-kickoff-20110617|title=Britney Spears Proves She's Still Got It at 'Femme Fatale' Tour Kickoff|last=Walters|first=Barry|date=June 17, 2011|accessdate=June 17, 2011|work=[[Rolling Stone]]|publisher=[[Jann Wenner]]}}</ref> Studio recording of the cover, described by Sarah Maloy of ''Billboard'' as "glammed-up without a hint of the '80s to be found", leaked on June 10, 2011.<ref>{{cite web|url=http://www.billboard.com/articles/news/470594/billboard-bits-jennifer-hudson-food-poisoned-britneys-madonna-cover-leaks|title=Billboard Bits: Jennifer Hudson Food Poisoned, Britney's Madonna Cover Leaks|last=Maloy|first=Sarah|date=June 10, 2011|work=[[Billboard (magazine)|Billboard]]|publisher=[[Prometheus Global Media]]|accessdate=October 7, 2011}}</ref> Singer [[Isadar]] included a cover of "Burning Up" as a bonus track on his 2006 compilation album, ''Scratching The Surface: Vol 2 Electro-Voice Sampler''.<ref>{{cite web|url=http://www.allmusic.com/album/scratching-the-surface-vol-2-electro-voice-sampler-r950979|title=Scratching The Surface: Vol 2 Electro-Voice Sampler > Review|publisher=Allmusic. Rovi Corporation|accessdate=January 7, 2014}}</ref>

==Track listing and formats==
{{col-begin}}
{{col-2}}
*''' U.S. / Europe /  Australia 12" Single'''<ref name="Releasenotes">{{cite AV media notes|title=Burning Up|others=[[Madonna (entertainer)|Madonna]]|date=1983|type=US 12-inch Single liner notes|publisher=[[Sire Records]]|id=9 29715-0}}</ref><ref name="ReferenceA">{{cite AV media notes|title=Burning Up|others=[[Madonna (entertainer)|Madonna]]|date=1983|type=French 12-inch Single liner notes|publisher=[[Sire Records]]|id=92-9609-7}}</ref><ref name="ReferenceB">{{cite AV media notes|title=Burning Up|others=[[Madonna (entertainer)|Madonna]]|date=1983|type=Australian 12-inch Single liner notes|publisher=[[Sire Records]]|id=0-29715}}</ref>
#"Burning Up" (12" version)&nbsp;– 5:56
#"Physical Attraction" (Album version)&nbsp;– 6:35

*''' Europe/Japan 7" Single'''<ref name="Releasenotes" />
#"Burning Up" (7" version)&nbsp;– 3:50
#"Physical Attraction" (7" version)&nbsp;– 3:52

{{col-2}}
*''' Australia 7" Single'''<ref name="Releasenotes" />
#"Burning Up" (Alternate album version)&nbsp;– 4:48
#"Physical Attraction" (7" version)&nbsp;– 3:52

*'''Germany / UK CD Maxi Single (1995)'''<ref name="ReferenceA"/>
#"Burning Up" (12" version)&nbsp;– 5:56
#"Physical Attraction" (Album version)&nbsp;– 6:34
{{col-end}}

==Credits and personnel==
*[[Madonna (entertainer)|Madonna]]&nbsp;– [[Singing|vocals]], [[Songwriter|writer]]
*[[Reggie Lucas]]&nbsp;– [[record producer|producer]], guitars, [[drum machine|drum programming]]
*[[Butch Jones]]&nbsp;– [[synthesizer]]
*[[John Benitez|John "Jellybean" Benitez]]&nbsp;– [[audio mixing (recorded music)|remixing]]
*Fred Zarr&nbsp;– synthesizer, electric and acoustic piano
*Dean Gant&nbsp;– electric and acoustic piano
*Bobby Malach&nbsp;– tenor [[saxophone]]
*Ed Walsh&nbsp;– synthesizer
*[[Gwen Guthrie]]&nbsp;– background vocals
*Brenda White&nbsp;– background vocals
*Chrissy Faith&nbsp;– background vocals
*Martin Burgoyne&nbsp;- [[Album cover|artwork]]

Credits adapted from the album and the single liner notes.<ref name="liner">{{cite AV media notes |title=[[Madonna (Madonna album)|Madonna]] |others=[[Madonna (entertainer)|Madonna]] |year=1983 |type=LP, Vinyl, CD |publisher=[[Sire Records]]|id=9 23867-1 }}</ref><ref>{{cite AV media notes |title=Burning Up |others=[[Madonna (entertainer)|Madonna]] |year=1983 |type=
Vinyl, 12", 45 RPM, Maxi-Single |publisher=[[Sire Records]]|id=9 29715-0 A}}</ref>

==Charts==
{| class="wikitable sortable plainrowheaders"
|-
! Chart (1983–84)
! Peak<br>position
|-
!scope="row"| Australia ([[Kent Music Report]])<ref name="kent"/>
| align="center"|13
|-
{{singlechart|Billboarddanceclubplay|3|artist=Madonna|song=Burning Up|rowheader=true|accessdate=May 27, 2014}}
|}

==References==

===Footnotes===
{{Reflist|colwidth=30em}}

===Sources===
{{refbegin|colwidth=30em}}
*{{Cite book
 | last = Allen
 | first = Robert Clyde
 | title = Channels of discourse: television and contemporary criticism
 | year = 1987
 | publisher = [[Routledge]]
 | isbn = 0-416-07082-5
 | ref = harv
 }}
*{{cite book
 |first= Nathan
 |last= Brackett
 |first2= Christian
 |last2= Hoard
 |year= 2004
 |title= [[The Rolling Stone Album Guide|The New Rolling Stone Album Guide]]
 |publisher= [[Simon & Schuster]]
 |isbn= 0-394-72107-1
 |quote= That gift was there on her 1983 debut, which combined proven postdisco club tracks like "Everybody" and "Burning Up" with breakthrough pop hits "Borderline" and "Lucky Star."
 |ref=harv
 }}
*{{Cite book
 | last = Clerk
 | first = Carol
 | title = Madonnastyle
 | year = 2002
 | publisher = [[Music Sales Group|Omnibus Press]]
 | isbn = 0-7119-8874-9
 | ref = harv
 }}
*{{Cite book
 | last = Fouz-Hernández
 | first = Santiago
 | first2 = Freya
 | last2 = Jarman-Ivens
 | title = Madonna's Drowned Worlds
 | publisher = [[Ashgate Publishing|Ashgate Publishing, Ltd]].
 | isbn = 0-7546-3372-1
 | year = 2004
 | ref = harv
 }}
*{{Cite book
 | last = Grant
 | first = Robert M.
 | first2 = Kent E.
 | last2 = Neupert
 | title = Cases in contemporary strategy analysis
 | publisher = [[Wiley-Blackwell]]
 | year = 2003
 | isbn = 1-4051-1180-1
 | ref = harv
 }}
*{{Cite book
 | last = Guilbert
 | first = Georges-Claude
 | title = Madonna as postmodern myth
 | publisher = McFarland
 | year = 2002
 | isbn = 0-7864-1408-1
 | ref = harv
 }}
*{{Cite book
 | last = Morton
 | first = Andrew
 | title = [[Madonna (book)|Madonna]]
 | publisher = [[Macmillan Publishers]]
 | year = 2002
 | isbn = 0-312-98310-7
 | authorlink = Andrew Morton (writer)
 | ref = harv
 }}
*{{Cite book
 | last = Rooksby
 | first = Rikky
 | title = The Complete Guide to the Music of Madonna
 | year = 2004
 | publisher = [[Music Sales Group|Omnibus Press]]
 | isbn = 0-7119-9883-3
 | ref = harv
 }}
*{{cite book
 | last = Tannenbaum
 | first = Rob
 | first2 = Craig
 | last2 = Marks
 | title = I Want My MTV: The Uncensored Story of the Music Video Revolution
 | publisher= Penguin Books
 | year = 2011
 | isbn = 9781101526415
 | ref = harv
 }}
*{{Cite book
 | last = Twitchell
 | first = Robert B.
 | title = For Shame: The Loss of Common Decency in American Culture
 | year = 1998
 | publisher = Macmillan Publishers
 | isbn = 0-312-19453-6
 | ref = harv
 }}
{{refend}}

==External links==
* {{MetroLyrics song|madonna|burning-up}}<!-- Licensed lyrics provider -->

{{Madonna songs}}
{{Good article}}

[[Category:1983 songs]]
[[Category:1983 singles]]
[[Category:Dance-rock songs]]
[[Category:Madonna (entertainer) songs]]
[[Category:Post-disco songs]]
[[Category:Sire Records singles]]
[[Category:Music videos directed by Steve Barron]]
[[Category:Songs written by Madonna (entertainer)]]
[[Category:Sonic Youth songs]]