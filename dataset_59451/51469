{{Infobox journal
|title = Historia Mathematica
| cover = [[File:Historia Mathematica.gif]]
| discipline = [[History of mathematics]]
| abbreviation =
| formernames = Notae de Historia Mathematica
| editor = Tom Archibald. Reinhard Siegmund-Schultze
| publisher = [[Elsevier]]
| country =
| history = 1974&mdash;present
| frequency = Quarterly
| impact = 0.435
| impact-year = 2014
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/622841/description
| link1 = http://www.sciencedirect.com/science/journal/03150860
| link1-name = Online access
| ISSN = 0315-0860
| OCLC = 2240703
| LCCN = 75642280
| CODEN = HIMADS
}}
'''''Historia Mathematica: International Journal of History of Mathematics''''' is an [[academic journal]] on the [[history of mathematics]] published by [[Elsevier]]. It was established by [[Kenneth O. May]] in 1971 as the free newsletter ''Notae de Historia Mathematica'', but by its sixth issue in 1974 had turned into a full journal.

== Editors ==
The [[editor in chief|editors]] of the journal have been:
* Kenneth O. May, 1974–1977
* [[Joseph Dauben|Joseph W. Dauben]], 1977–1985
* [[Eberhard Knobloch]], 1985–1994
* [[David E. Rowe]], 1994–1996
* [[Karen Hunger Parshall]], 1996–2000
* [[Craig Fraser]] and [[Umberto Bottazzini]], 2000–2004
* [[Craig Fraser]], 2004–2007
* [[Benno van Dalen]], 2007–2009
* [[June Barrow-Green]] and [[Niccolò Guicciardini]], 2010–2013
* [[Niccolò Guicciardini]] and [[Tom Archibald]], 2013-2015
* [[Tom Archibald]] and [[Reinhard Siegmund-Schultze]], 2016-present

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Mathematical Reviews]], [[Science Citation Index|SCISEARCH]], and [[Scopus]].

== References ==
* {{cite journal|first=Robin E.|last=Rider|journal=Isis|volume=81|issue=2|pages=297&ndash;298|title=Review|year=1990|jstor=233701|doi=10.1086/355352 }}
* [http://www.unizar.es/ichm/history.htm A Brief History of the International Commission on the History of Mathematics (ICHM)]

== External links ==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/622841/description}}
* [http://www.unizar.es/ichm/hm.htm ''Historia Mathematica''] at the [[International Commission on the History of Mathematics]]

[[Category:Elsevier academic journals]]
[[Category:Quarterly journals]]
[[Category:History of science journals]]
[[Category:Publications established in 1974]]
[[Category:English-language journals]]
[[Category:History of mathematics journals]]