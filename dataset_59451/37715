{{good article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=Bundesarchiv Bild 101I-185-0116-22A, Bucht von Kotor (-), jugoslawische Schiffe.jpg
|image alt=two naval ships side by side alongside a dock with mountains in the background
|Ship image size=300px
|Ship caption=The name ship of the class ''Beograd'' (right) and the flotilla leader {{ship|Yugoslav destroyer|Dubrovnik||2}} in the [[Bay of Kotor]] after being captured by Italy
}}
{{Infobox ship class overview
|Name=''Beograd'' class
|Builders=*''[[Ateliers et Chantiers de la Loire]]''
*''[[Brodosplit|Jadranska brodogradilišta]]''
|Operators=*{{navy|Kingdom of Yugoslavia}}
*{{flagicon|Italy|1861}} [[Regia Marina]]
*{{navy|Nazi Germany}}
|Class before={{ship|Yugoslav destroyer|Dubrovnik||2}}
|Class after={{ship|Yugoslav destroyer|Split||2}}
|Subclasses=
|Cost=
|Built range=1937–1939
|In service range=1939–1945
|In commission range=
|Total ships building=
|Total ships planned=3
|Total ships completed=3
|Total ships cancelled=
|Total ships lost=3
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class=[[Destroyer]]
|Ship displacement=*{{convert|1210|t|LT}} (standard)
*{{convert|1655|t|LT}} (full load)
|Ship length={{convert|98|m|ftin|abbr=on}}
|Ship beam={{convert|9.45|m|ftin|abbr=on}}
|Ship draught={{convert|3.18|m|ftin|abbr=on}}
|Ship power=*3 × [[Yarrow Shipbuilders|Yarrow]] [[water-tube boiler]]s
*{{convert|40000|shp|lk=in|abbr=on}}
|Ship propulsion=*2 × shafts
*Curtis or Parsons [[steam turbine]]s
|Ship speed={{convert|38|kn}}
|Ship range={{convert|1000|nmi}}
|Ship complement=145
|Ship armament=
*4 × {{convert|120|mm|in|abbr=on}} guns (4 × 1)
*4 × {{convert|40|mm|in|abbr=on}} (2 × 2) [[Anti-aircraft warfare|anti-aircraft gun]]s
*6 × {{convert|550|mm|in|abbr=on}} [[torpedo tubes]] (2 × 3)
*2 × [[machine gun]]s
*30 [[naval mine]]s
|Ship notes=
}}
|}
The '''''Beograd'' class''' were three [[destroyer]]s built for the [[Royal Yugoslav Navy]] in the late 1930s based on a French design. {{ship|Yugoslav destroyer|Beograd||2}} was built in France and {{ship|Yugoslav destroyer|Zagreb||2}} and {{ship|Yugoslav destroyer|Ljubljana||2}} were built in the [[Kingdom of Yugoslavia]]. During the [[World War II]] [[Nazi Germany|German]]-led [[Axis powers|Axis]] [[invasion of Yugoslavia]] in April 1941, ''Zagreb'' was [[scuttled]] to prevent its capture, and the other two were captured by the [[Kingdom of Italy|Italians]]. The [[Regia Marina|Royal Italian Navy]] operated the two captured ships as [[convoy]] escorts between Italy, the [[Aegean Sea]] and [[North Africa]], but one was lost in the [[Gulf of Tunis]] in April 1943. The other was captured by the Germans in September 1943 after the Italian surrender, and was subsequently operated by the [[Kriegsmarine|German Navy]]. There are conflicting reports about the fate of the final ship, but it was lost in the final weeks of the war. In 1967, a French film was made about the scuttling of ''Zagreb''. In 1973, [[Josip Broz Tito]] posthumously awarded the two officers who scuttled ''Zagreb'' with the [[Order of the People's Hero]].

==Background==
Following the demise of the [[Austro-Hungarian Empire]] and the creation of the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (KSCS) at the conclusion of [[World War I]], Austria-Hungary transferred the vessels of the former [[Austro-Hungarian Navy]] to the new nation. The [[Kingdom of Italy]] was unhappy with this, and convinced the Allies to share the Austro-Hungarian ships among the victorious powers. As a result, the only modern sea-going vessels left to the KSCS were 12 [[torpedo boat]]s,{{sfn|Chesneau|1980|p=355}} and they had to build their naval forces from scratch.{{sfn|Novak|2004|p=234}} In 1929, the name of the state was changed to the Kingdom of Yugoslavia. In the early 1930s, the [[Royal Yugoslav Navy]] ({{lang-sh|Kraljevska Jugoslavenska Ratna Mornarica}}, KJRM) pursued the [[flotilla leader]] concept, which involved building large [[destroyer]]s similar to the World War I [[Royal Navy]] [[V and W-class destroyer]]s.{{sfn|Freivogel|2014|p=83}} In the [[interwar period|interwar]] [[French Navy]], these ships were intended to operate with smaller destroyers, or as half-flotillas of three ships. The Royal Yugoslav Navy decided to build three such flotilla leaders, ships that would have the ability to reach high speeds and with a long endurance. The long endurance requirement reflected Yugoslav plans to deploy the ships into the central [[Mediterranean]], where they would be able to operate alongside French and British warships. The pursuit of this concept resulted in the construction of the destroyer {{ship|Yugoslav destroyer|Dubrovnik||2}} in 1930–1931. Soon after she was ordered, the onset of the [[Great Depression]] meant that only one ship of the planned half-flotilla was ever built.{{sfn|Freivogel|2014|p=84}}

Despite the fact that a half-flotilla of large destroyers was not going to be built, the idea that ''Dubrovnik'' might operate with a number of smaller destroyers persisted. In 1934, the KJRM decided to acquire three such destroyers to operate in a [[division (naval)|division]] led by ''Dubrovnik''.{{sfn|Jarman|1997|p=543}} The ''Beograd'' class was developed from a French design, and the [[name ship]] of the class, {{ship|Yugoslav destroyer|Beograd||2}}, was built by [[Ateliers et Chantiers de la Loire]] at [[Nantes]], France, whereas the remaining ships of the class, {{ship|Yugoslav destroyer|Zagreb||2}} and {{ship|Yugoslav destroyer|Ljubljana||2}}, were built by ''[[Brodosplit|Jadranska brodogradilišta]]'' at [[Split, Croatia|Split]], Yugoslavia, under French supervision. Two more ships of the class were planned, but not built.{{sfn|Chesneau|1980|pp=357–358}}

==Description and construction==
The ships had an [[Length overall|overall length]] of {{convert|98|m|ftin|abbr=on}}, a [[Beam (nautical)|beam]] of {{convert|9.45|m|ftin|abbr=on}}, and a normal [[Draft (hull)|draught]] of {{convert|3.18|m|ftin|abbr=on}}. Their standard [[Displacement (ship)|displacement]] was {{convert|1210|t|LT}}, and they displaced {{convert|1655|t|LT}} at full load.{{sfn|Chesneau|1980|p=357}} ''Beograd'' was powered by Curtis [[steam turbine]]s, and ''Zagreb'' and ''Ljubljana'' used [[Parsons Marine Steam Turbine Company|Parsons]] steam turbines. Regardless of the turbines used, they drove two propellors, using steam generated by three [[Yarrow Shipbuilders|Yarrow]] [[water-tube boiler]]s. Their turbines were rated at {{convert|40000|shp|lk=in|abbr=on}}{{refn|One source gives a rating of {{convert|44000|shp|lk=in|abbr=on}} for ''Beograd''.{{sfn|Lenton|1975|p=106}}|group = lower-alpha}} and they were designed to propel the ships at a top speed of {{convert|38|kn|lk=in}}. They carried {{convert|120|t|LT}} of [[fuel oil]],{{sfn|Chesneau|1980|p=357}} which gave them a radius of action of {{convert|1000|nmi}}.{{sfn|Lenton|1975|p=106}} Their crews consisted of 145 personnel, including officers and enlisted men.{{sfn|Chesneau|1980|p=357}}

Their main armament consisted of four [[Škoda Works|Škoda]] {{convert|120|mm|in|abbr=on}} L/46{{refn|L/46 denotes the length of the gun. In this case, the L/46 gun is 46 [[Caliber (artillery)|calibre]], meaning that the gun was 46 times as long as the diameter of its bore.|group = lower-alpha}} [[superfiring]] guns in single mounts, two [[bow (ship)|forward]] of the [[superstructure]] and two [[aft]], protected by [[gun shield]]s. Their secondary armament consisted of four [[Bofors 40 mm gun|Bofors]] {{convert|40|mm|in|abbr=on}} [[Anti-aircraft warfare|anti-aircraft gun]]s in two twin mounts,{{sfn|Chesneau|1980|p=357}}{{sfn|Jarman|1997|p=738}}{{sfn|Campbell|1985|p=394}} located on either side of the aft shelter deck.{{sfn|Whitley|1988|p=312}} They were also equipped with two triple mounts of {{convert|550|mm|in|abbr=on}} [[torpedo tubes]] and two [[machine gun]]s.{{sfn|Chesneau|1980|p=357}} Their [[fire-control system]]s were provided by the Dutch firm of Hazemayer.{{sfn|Jarman|1997|p=738}} As built, they could also carry 30 [[naval mine]]s.{{sfn|Chesneau|1980|p=357}}

==Ships==
{| class="wikitable"
|-
!Ship
!Builder{{sfn|Chesneau|1980|p=357}}
!Laid down{{sfn|Cernuschi|O'Hara|2005|p=99}}{{sfn|Jarman|1997|p=738}}
!Launched{{sfn|Chesneau|1980|p=357}}
!Commissioned{{sfn|Whitley|1988|p=312}}
!Fate
|-
|{{ship|Yugoslav destroyer|Beograd||2}}
|''[[Ateliers et Chantiers de la Loire]]'', [[Nantes]]
|rowspan=3|{{centre|1936}}
|{{centre|23 December 1937}}
|{{centre|28 April 1939}}
|Captured by Italy, 17 April 1941, renamed ''Sebenico''<br/>Captured by Germans 1943, renamed ''TA43''<br/>Scuttled/sunk 30 April/1 May 1945{{sfn|Chesneau|1980|pp=357–358}}{{sfn|Brescia|2012|p=134}}{{sfn|Brown|1995|p=149}}
|-
| {{ship|Yugoslav destroyer|Zagreb||2}}
|rowspan=2|''[[Brodosplit|Jadranska brodogradilišta]]'', [[Split, Croatia|Split]]
|{{centre|30 March 1938}}
|{{centre|August 1939}}
|Scuttled, 17 April 1941{{sfn|Chesneau|1980|pp=357–358}}
|-
| {{ship|Yugoslav destroyer|Ljubljana||2}}
|{{centre|28 June 1938}}
|{{centre|December 1939}}
|Captured by Italy, 17 April 1941, renamed ''Lubiana''<br/>Lost 1 April 1943{{sfn|Chesneau|1980|pp=357–358}}{{sfn|Brescia|2012|p=134}}{{sfn|Brown|1995|p=83}}
|}

==Service==
At the time of the outbreak of [[World War II]], all three ships had only been in commission for a short time. Their only significant pre-war task was undertaken by ''Beograd'' in May 1939, involving the transportation of a large part of Yugoslavia's [[gold reserve]] to the United Kingdom for safekeeping.{{sfn|Hoptner|1963|p=156}} On 24 January 1940, ''Ljubljana'' ran into a [[reef]] off the Yugoslav port of [[Šibenik]]. The hull side was breached and despite efforts to get the ship into the port, it sank close to shore, and some of the crew swam to safety. Only one of the crew died, and the captain was arrested pending an investigation.{{sfn|The Examiner|26 September 1940|p=1}} When Yugoslavia was drawn into the war by the [[Nazi Germany|German]]-led [[Axis powers|Axis]] [[invasion of Yugoslavia|invasion]] on 6 April 1941, ''Beograd'' and ''Zagreb'' were allocated to the 1st Torpedo [[Division (naval)|Division]] at the [[Bay of Kotor]] along with ''Dubrovnik'',{{sfn|Niehorster|2016}} but ''Ljubljana'' was still under repair at Šibenik.{{sfn|Brescia|2012|p=134}}{{sfn|Chesneau|1980|p=301}} On 9 April, ''Beograd'' and other vessels were sent on an mission to support an attack on the Italian enclave of [[Zadar|Zara]] on the [[Dalmatia]]n coast, but the naval prong of the attack was aborted when ''Beograd'' suffered engine damage from near misses by Italian aircraft. She returned to the Bay of Kotor for repairs.{{sfn|Whitley|1988|p=312}} ''Beograd'' and ''Ljubljana'' were captured in port by Italian forces on 17 April,{{sfn|Chesneau|1980|p=301}}{{sfn|Brown|1995|p=44}} but on the same day, two of ''Zagreb'''s officers [[scuttling|scuttled]] her to prevent her capture, and were killed by the resulting explosions.{{sfn|Maritime Museum of Montenegro|2007}}

In Italian service, ''Beograd'' and ''Ljubljana'' were repaired, re-armed, and renamed ''Sebenico'' and ''Lubiana'' respectively. ''Sebenico'' was commissioned into the [[Regia Marina|Royal Italian Navy]] in August 1941, and ''Lubiana'' in October or November 1942. They both served mainly as [[convoy]] escorts between Italy and the [[Aegean Sea|Aegean]] and [[North Africa]], with ''Sebenico'' completing more than 100 convoy escort missions over a two-year period. Neither ship was involved in any notable action.{{sfn|Brescia|2012|p=134}}{{sfn|Chesneau|1980|p=301}}{{sfn|Whitley|1988|p=186}}{{sfn|Rohwer|Hümmelchen|1992|pp=193 & 203}} ''Lubiana'' was either sunk off the Tunisian coast by British aircraft on 1 April 1943,{{sfn|Chesneau|1980|p=358}} or [[Ship grounding|ran aground]] in the [[Gulf of Tunis]] on 1 April 1943, and was lost.{{sfn|Brescia|2012|p=134}}{{sfn|Brown|1995|p=83}} ''Sebenico'' was captured by the Germans in [[Venice]] after the [[Italian Armistice]] in September 1943 in a damaged condition. She was repaired, re-armed, and renamed ''TA43'' and entered service in the ''[[Kriegsmarine]]'' (German Navy).{{sfn|Lenton|1975|p=106}}{{sfn|Chesneau|1980|p=358}}{{sfn|Brown|1995|p=94}}{{sfn|Rohwer|Hümmelchen|1992|p=231}} ''TA43'' served on escort and [[minelayer|mine-laying]] duties in the northern [[Adriatic Sea]], but saw little action.{{sfn|O'Hara|2013|p=181}}{{sfn|Whitley|1988|p=80}} She was either damaged by artillery fire on 30 April 1945 at [[Trieste]] then scuttled,{{sfn|Chesneau|1980|p=358}} or just scuttled on 1 May.{{sfn|Brescia|2012|p=134}}{{sfn|Brown|1995|p=149}} In 1967, a French film, ''[[Adriatic Sea of Fire|Flammes sur l'Adriatique]]'' (Adriatic Sea of Fire) was made, portraying the scuttling of ''Zagreb'' and the events leading up to it.{{sfn|La Cinémathèque française|2001}} In 1973, the [[President of Yugoslavia]] and wartime [[Yugoslav Partisans|Partisan]] leader [[Josip Broz Tito]] posthumously awarded the [[Order of the People's Hero]] to the two officers who scuttled ''Zagreb''.{{sfn|Luković|2016}}

==Notes==
{{reflist|group=lower-alpha}}

==Footnotes==
{{reflist|20em}}

==References==
===Books===
{{refbegin}}
* {{cite book
  | last = Brescia
  | first = Maurizio
  | year = 2012
  | title = Mussolini's Navy
  | publisher = Seaforth Publishing
  | location = Barnsley, South Yorkshire
  | isbn = 978-1-59114-544-8
  | ref = harv
  }}
* {{cite book
  | last = Brown
  | first = David
  | title = Warship Losses of World War Two
  | year = 1995
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-1-55750-914-7
  | ref = harv
  }}
* {{cite book
  | last = Campbell
  | first = John
  | title = Naval Weapons of World War Two
  | year = 1985
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-329-2
  | ref = harv
  }}
*{{cite book
  | editor-last = Jordan
  | editor-first = John
  | publisher = Conway Maritime Press
  | location = London, England
  | year = 2005
  | title = Warship 2005
  | isbn = 1-84486-003-5
  | last1 = Cernuschi
  | first1 = Enrico
  | last2 = O'Hara
  | first2 = Vincent O.
  | chapter = The Star-Crossed ''Split''
  | pages = 97–110
  | lastauthoramp = y
  |ref = harv
  }}
* {{cite book
  | editor-last = Chesneau
  | editor-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-146-5
  | ref = harv
  }}
* {{cite book
  | last = Hoptner
  | first = Jacob B.
  | title = Yugoslavia in Crisis, 1934–1941
  | year = 1963
  | publisher = Columbia University Press
  | location = New York, New York
  | oclc = 310483760
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 2
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | last = Lenton
  | first = H.T.
  | year = 1975
  | title = German Warships of the Second World War
  | publisher = Macdonald and Jane's
  | location = London, England
  | isbn = 978-0-356-04661-7
  | ref = harv
  }}
* {{cite book
  | last1 = Novak
  | first1 = Grga
  | year = 2004
  | title = Jadransko more u sukobima i borbama kroz stoljeća
  | trans_title = The Adriatic Sea in Conflicts and Battles Through the Centuries
  | language = Croatian
  | volume = 2
  | publisher = Marjan tisak
  | location = Split, Croatia
  | isbn = 978-953-214-222-8
  | ref = harv
  }}
* {{cite book
  | last = O'Hara
  | first = Vincent
  | title = The German Fleet at War, 1939–1945
  | year = 2013
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-1-61251-397-3
  | ref = harv
  }}
* {{cite book
  | last1 = Rohwer
  | first1 = Jürgen
  | authorlink = Jürgen Rohwer
  | last2 = Hümmelchen
  | first2 = Gerhard
  | title = Chronology of the War at Sea 1939–1945: The Naval History of World War Two
  | url = https://books.google.com/books?id=l6za1mEmQE4C
  | year = 1992
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-1-55750-105-9
  | lastauthoramp = y
  | ref = harv
  }}
* {{cite book
  | last = Whitley
  | first = M. J.
  | title = Destroyers of World War Two: An International Encyclopedia
  | url = https://books.google.com/books?id=u4XfAAAAMAAJ
  | year = 1988
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-326-7
  | ref = harv
  }}
{{refend}}

===Periodicals===
{{refbegin}}
*{{cite journal
  | last = Freivogel
  | first = Zvonimir
  | year = 2014
  | title = From Glasgow to Genoa under Three Flags – The Yugoslav Flotilla Leader Dubrovnik
  | url = http://ejournal6.com/journals_n/1404051978.pdf
  | journal = Voennyi Sbornik
  | publisher = Academic Publishing House Researcher
  | volume = 4
  | issue = 2
  | pages = 83–88
  | doi=
  | accessdate = 25 October 2014
  | ref = harv
  }}
* {{cite news
 | title       =Yugoslav Destroyer Hits Reef: Only One of Crew Lost
 | author      =
 | first       =
 | last        =
 | authorlink  =
 | authorlink2 =
 | author2     =
 | author3     =
 | author4     =
 | author5     =
 | author6     =
 | author7     =
 | url         = http://trove.nla.gov.au/ndp/del/article/92662829
 | format      =
 | agency      =
 | newspaper   =The Examiner
 | publisher   =
 | location    =Launceston, Tasmania
 | date        =26 January 1940
 | page        =1
 | accessdate  =29 September 2013
 | ref         ={{harvid|The Examiner26 September 1940}}
}}
{{refend}}

===Websites===
{{refbegin}}
* {{cite web
| url =  http://cinema.encyclopedie.films.bifi.fr/imprime.php?pk=46468
| title = ''Flammes sur l'Adriatique'' (1967) – Alexandre Astruc
| trans-title = ''Adriatic Sea of Fire'' (1967) – Alexandre Astruc 
| accessdate = 7 November 2016
| publisher  = [[Cinémathèque Française|La Cinémathèque française]]
| year   = 2001
| last = 
| first = 
| ref = {{harvid|La Cinémathèque française|2001}}
}}
* {{cite web
| last = Luković
| first = Siniša
| url = http://www.vijesti.me/vijesti/zagreb-umire-zagreb-se-ne-predaje-883178
| title = “Zagreb” umire, “Zagreb” se ne predaje
| trans-title = "Zagreb" is dying, "Zagreb" will not surrender
| year = 2016
| website = Vijesti online
| publisher = Vijesti
| accessdate = 5 November 2016
| ref = harv
}}
* {{cite web
  | url =  http://www.niehorster.org/040_yugoslavia/41-04-06/navy.html
  | title = Balkan Operations Order of Battle Royal Yugoslavian Navy 6th April 1941
  | accessdate = 4 November 2016
  | publisher  = Dr. Leo Niehorster
  | year   = 2016
  | last = Niehorster
  | first = Dr. Leo
  | ref = harv
  }}
* {{cite web
| url = http://www.museummaritimum.com/eng/prvi%20i%20drugi%20sv%20rat.htm
| title = World War I and II
| year = 2007
| website = Maritime Museum of Montenegro
| accessdate = 25 September 2013
| ref = {{harvid|Maritime Museum of Montenegro2007}}
}}
{{refend}}
 <!-- non-breaking space to keep AWB drones from altering the space before the navbox-->

{{Beograd class destroyers}}
{{Yugoslav Ships |state=collapsed}}

[[Category:Destroyer classes]]
[[Category:Beograd-class destroyers| ]]
[[Category:1930s ships]]