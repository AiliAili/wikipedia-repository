{{Infobox video game
 |title       = Bertie the Brain
 |image       = File:Bertie the Brain - Life.jpg
 |caption     = Comedian [[Danny Kaye]] photographed having just won against the machine
 |designer    = [[Josef Kates]]
 |released    = 1950
 |genre       = [[Tic-Tac-Toe]]
 |modes       = [[Single-player video game|Single-player]]
 |platforms   = [[PC game|Computer game]]
}}
'''''Bertie the Brain''''' was an early [[PC game|computer game]], and one of the first games developed in the [[early history of video games]]. It was built in [[Toronto]] by [[Josef Kates]] for the 1950 [[Canadian National Exhibition]]. The four meter tall computer allowed exhibition attendees to play a game of [[tic-tac-toe]] against an [[Artificial intelligence (video games)|artificial intelligence]]. The player entered a move on a lit keypad in the form of a three-by-three grid, and the game played out on a grid of lights overhead. The machine had an adjustable [[difficulty level]]. After two weeks on display by [[Rogers Vacuum Tube Company|Rogers Majestic]], the machine was disassembled at the end of the exhibition and largely forgotten as a curiosity.

Kates built the game to showcase his [[additron tube]], a miniature version of the [[vacuum tube]], though the [[transistor]] overtook it in computer development shortly thereafter. Patent issues prevented the additron tube from being used in computers besides ''Bertie'' before it was no longer useful. ''Bertie the Brain'' is a candidate for the first video game, as it was potentially the first computer game to have any sort of visual display of the game. It appeared only three years after the 1947 invention of the [[cathode-ray tube amusement device]], the earliest known [[interactivity|interactive]] [[electronic game]] to use an electronic display. ''Bertie''{{'}}s use of lightbulbs rather than a screen with real-time [[video game graphics|visual graphics]], however, much less moving graphics, does not meet some definitions of a video game.

==History==
''Bertie the Brain'' was a [[PC game|computer game]] of [[tic-tac-toe]], built by [[Josef Kates|Dr. Josef Kates]] for the 1950 [[Canadian National Exhibition]].<ref name="CNE"/> Kates had previously worked at [[Rogers Vacuum Tube Company|Rogers Majestic]] designing and building radar tubes during [[World War II]], then after the war pursued graduate studies in the computing center at the [[University of Toronto]] while continuing to work at Rogers Majestic.<ref name="Spacing"/> While there, he helped build the University of Toronto Electronic Computer ([[UTEC]]), one of the first working computers in the world. He also designed his own miniature version of the [[vacuum tube]], called the [[additron tube]], which he registered with the [[Radio Electronics Television Manufacturers' Association]] on 20 March 1951 as type 6047.<ref name="Spacing"/><ref name="Tube1"/><ref name="Tube2"/><ref name="Tube3"/>

After filing for a patent for the additron tube, Rogers Majestic pushed Kates to create a device to showcase the tube to potential buyers. Kates designed a specialized computer incorporating the technology and built it with the assistance of engineers from Rogers Majestic.<ref name="Spacing"/><ref name="Priest"/> The large, four meter tall metal computer could only play tic-tac-toe and was installed in the Engineering Building at the Canadian National Exhibition from 25 August–9 September 1950.<ref name="Spacing"/><ref name="CNEdates"/>

The additron-based computer, labeled as "Bertie the Brain" and subtitled "The Electronic Wonder by Rogers Majestic", was a success at the two-week exhibition, with attendees lining up to play it. Kates stayed by the machine when possible, adjusting the difficulty up or down for adults and children. Comedian [[Danny Kaye]] was photographed defeating the machine (after several attempts) for ''[[Life (magazine)|Life]]'' magazine.<ref name="Spacing"/>

==Gameplay==
''Bertie the Brain'' was a game of tic-tac-toe in which the player would select the position for their next move from a grid of nine lit buttons on a raised panel. The moves would appear on a grid of nine large squares set vertically on the machine as well as on the buttons, with either an X- or O-shaped light turning on in the corresponding space. The computer would make its move shortly after. A pair of signs to the right of the playfield, alternately lit up with "Electronic Brain" and an X or "Human Brain" and an O, marked which player's turn it was, and would light up along with "Win" when a player had won. ''Bertie'' could be set to several difficulty levels.<ref name="Spacing"/> The computer responded almost instantly to the player's moves and at the highest difficulty level was almost unbeatable.<ref name="Priest"/>

==Legacy==
[[File:Additron Tube schematic.png|thumb|Circuitry schematic from the US patent for the [[additron tube]]]]
After the exhibition, ''Bertie'' was dismantled and "largely forgotten" as a novelty. Kates has said that he was working on so many projects at the same time that he had no energy to spare for preserving it, despite its significance.<ref name="Spacing"/> Despite being the first implemented computer game—preceded only by theorized chess programs—and featured in a ''Life'' magazine article, the game was largely forgotten, even by video game history books.<ref name="Priest"/> ''Bertie'''s primary purpose, to promote the additron tube, went unfulfilled, as it was the only completed application of the technology. By the time Rogers Majestic pushed Kates to develop a working model for the Exhibition, he had been working on the tubes for a year, developing several revisions, and the University of Toronto team felt that the development was too slow to attempt to integrate them into the UTEC.<ref name="TCR"/>

Although other firms expressed interest to Kates and Rogers Majestic in using the tubes, issues with acquiring patents prevented him, Rogers Majestic, or the University of Toronto from patenting the tubes anywhere outside Canada until 1955, and the patent application was not accepted in the United States until March 1957, six years after filing.<ref name="TCR"/><ref name="AdditronUSPatent"/> By then, research and use of vacuum tubes was heavily waning in the face of the rise of the superior [[transistor]], preventing any re-visitation of ''Bertie'' or similar machines.<ref name="TCR"/> Kates went on to a distinguished career in Canadian engineering, but did not return to working on vacuum tubes or computer games.<ref name="Priest"/><ref name="TCR"/>

''Bertie'' was created only three years after the 1947 invention of the [[cathode-ray tube amusement device]], the earliest known [[interactivity|interactive]] [[electronic game]], and while non-visual games had been developed for research computers such as [[Alan Turing]] and Dietrich Prinz's chess program for the [[Ferranti Mark 1]] at the [[University of Manchester]], ''Bertie'' was the first computer-based game to feature a visual display of any sort.<ref name="Spacing"/><ref name="TVGD"/><ref name="Replay"/> ''Bertie'' is considered under some definitions in contention for the title of the first video game.  While definitions vary, the prior cathode-ray tube amusement device was a purely analog electrical game, and while ''Bertie'' did not feature an electronic screen it did run on a computer.<ref name="EVG"/> Another special-purpose computer-based game, ''[[Nimrod (computing)|Nimrod]]'', was built in 1951, while the software-based tic-tac-toe game ''[[OXO]]'' and a [[draughts]] program by [[Christopher Strachey]] were in 1952 the first [[PC game|computer games]] to display [[video game graphics|visuals]] on an electronic screen rather than light bulbs.<ref name="Priest"/><ref name="TVGD"/><ref name="TCU"/>

== References ==
{{reflist|30em|refs=

<ref name="CNE">{{cite news |first=Marlene |last=Simmons |title=Bertie the Brain programmer heads science council |url=https://news.google.com/newspapers?id=rKYyAAAAIBAJ&sjid=pe0FAAAAIBAJ&pg=916,3790974&dq=josef-kates&hl=en |accessdate=2014-11-16 |newspaper=[[Ottawa Citizen]] |date=1975-10-09 |page=17}}</ref>

<ref name="Spacing">{{cite journal |url=http://spacing.ca/toronto/2014/08/13/meet-bertie-brain-worlds-first-arcade-game-built-toronto/ |title=Meet Bertie the Brain, the world's first arcade game, built in Toronto |last1=Bateman |first1=Chris |date=2014-08-13 |journal=[[Spacing (magazine)|Spacing Magazine]] |accessdate=2014-11-16 |archiveurl=https://web.archive.org/web/20151222164300/http://spacing.ca/toronto/2014/08/13/meet-bertie-brain-worlds-first-arcade-game-built-toronto/ |archivedate=2015-12-22 |deadurl=no}}</ref>

<ref name="Tube1">{{cite journal |journal=RTMA Engineering Department |title=Release #951 |date=1951-03-20}}</ref>

<ref name="Tube2">{{cite journal |last=Sibley |first=L. |date=2007 |title=Weird Tube of the Month: The 6047 |journal=Tube Collector |publisher=Tube Collectors Association |volume=9 |issue=5 |page=20}}</ref>

<ref name="Tube3">{{cite journal |last=Osborne |first=C. S. |date=2008 |title=The Additron: A Binary Full Adder in a Tube |journal=Tube Collector |publisher=Tube Collectors Association |volume=10 |issue=4 |page=12}}</ref>

<ref name="CNEdates">{{cite book |title=F.H. Varley: Portraits Into the Light |last=Varley |first=Frederick |authorlink=Frederick Varley |publisher=Dundurn Press |date=2007 |page=119 |isbn=978-1-55002-675-7}}</ref>

<ref name="TCR">{{cite book |title=The Computer Revolution in Canada: Building National Technological Competence |publisher=[[MIT Press]] |last=Vardalas |first=John N. |date=2001 |pages=31–33 |isbn=978-0-262-22064-4}}</ref>

<ref name="TVGD">{{cite book |title=The Video Game Debate: Unravelling the Physical, Social, and Psychological Effects of Video Games |last1=Kowert |first1=Rachel |last2=Quandt |first2=Thorsten |date=2015-08-27 |page=3 |publisher=[[Routledge]] |isbn=978-1-138-83163-6}}</ref>

<ref name="TCU">{{cite book |title=The Computing Universe: A Journey through a Revolution |last1=Hey |first1=Tony |last2=Pápay |first2=Gyuri |publisher=[[Cambridge University Press]] |date=2014-11-30 |page=174 |isbn=978-0-521-15018-7}}</ref>

<ref name="Priest">{{cite web |url=https://videogamehistorian.wordpress.com/2014/01/22/the-priesthood-at-play-computer-games-in-the-1950s/ |title=The Priesthood At Play: Computer Games in the 1950s |work=They Create Worlds |last=Smith |first=Alexander |date=2014-01-22 |accessdate=2015-12-18 |archiveurl=https://web.archive.org/web/20151222172058/https://videogamehistorian.wordpress.com/2014/01/22/the-priesthood-at-play-computer-games-in-the-1950s/ |archivedate=2015-12-22 |deadurl=no}}</ref>

<ref name="AdditronUSPatent">{{cite patent |country=US |number=2784312 |status=patent |title=Electronic Vacuum Tube |fdate=1951-01-17 |gdate=1957-03-05 |invent1=[[Josef Kates|Kates, Josef]]}}</ref>

<ref name="Replay">{{cite book |title=[[Replay: The History of Video Games]] |last=Donovan |first=Tristan |publisher=Yellow Ant |date=2010-04-20 |isbn=978-0-9565072-0-4 |pages=1–9}}</ref>

<ref name="EVG">{{cite book |title=Encyclopedia of Video Games: The Culture, Technology, and Art of Gaming |last=Wolf |first=Mark J. P. |date=2012-08-16 |publisher=[[Greenwood Publishing Group]] |isbn=978-0-313-37936-9 |pages=XV–7}}</ref>
}}

{{good article}}
{{Early history of video games}}

[[Category:1950 video games]]
[[Category:Arcade games]]
[[Category:Early computers]]
[[Category:Early history of video games]]