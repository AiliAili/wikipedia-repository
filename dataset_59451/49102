{{Infobox military person
| name              = Antoni Stephan Koper
| image         =
| image_size    = 
| alt           = 
| caption       = Portrait of Antoni Koper by an unknown artist, c. 1938
| birth_date    = {{birth date|1906|09|06}}
| birth_place   = Warsaw, Poland
| death_date    = {{Death date and age|1990|06|13|1906|09|06}}
| death_place   = Arlington, Virginia, US
| placeofburial = 
| placeofburial_label = 
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| allegiance    ={{Plainlist|
*[[Second Polish Republic]]
*[[Polish government-in-exile]]
}}
| branch        = {{Plainlist|
*[[Polish Land Forces]]
*[[Polish Home Army|Home Army]]
*[[1st Armoured Division (Poland)]]
}}
| serviceyears  = 1939–1947
| rank          = Lieutenant
| servicenumber = <!-- Do not use data from primary sources such as service records -->
| unit          = 
| commands      = 
| battles       = {{Plainlist|
*[[Invasion of Poland]]
*[[Warsaw Uprising]]
}}
| battles_label = 
| awards        =[[Yad Vashem|Yad Vashem Award]]
| memorials     =[[Garden of the Righteous Among the Nations]]
| spouse        = <!-- Add spouse if reliably sourced -->Sophie Margulies Koper
| relations     =[[Peter Koper]] (son)
| laterwork     ={{plainlist|
*[[Defense Language Institute]]
*[[United States Information Agency]]
*[[Voice of America]]
 }}
| signature     = 
| signature_size =
| signature_alt =
| website       = <!-- {{URL|example.com}} -->
}}

'''Antoni Stefan Koper''' (September 6, 1906 – June 13, 1990) was active in the [[Polish resistance movement in World War II|Polish resistance movement]] during [[World War II]] and served as a [[lieutenant]] in the Polish [[Home Army]]. He helped rescue [[Jews]] from the [[Warsaw Ghetto]] and fought in the [[Warsaw Uprising]]. After escaping from a [[Nazi]] prison camp, he first fled to [[London]], and then emigrated to the United States. There, he worked for the [[Defense Language Institute]], [[United States Information Agency]], and the [[Voice of America]]. He died of [[cancer]] in 1990.

==Early life and the Invasion of Poland==

Antoni Koper was born in [[Warsaw]], Poland in 1906, and was graduated from the [[University of Warsaw]].<ref name="WP obit" /><ref name="TWT obit">{{cite news|title=Metropolitan Obituaries: Antoni Atefan Koper, 83, hero in Polish resistance|work=The Washington Times|date=15 June 1990}}</ref> He had chosen a career in [[journalism]], but the September 1939 [[Invasion of Poland]] by [[Germany]] found him on the front, fighting with the [[Polish Army]]. After Germany annexed Poland weeks later, Koper returned home to occupied Warsaw. Because he was not permitted to work as a journalist under the occupation government, a friend hired him at the municipal tax bureau, where he was tasked with collecting city taxes due from Jews now residing in the ghetto. The tax bureau provided Koper with an ''Ausweiss'', an identity card allowing him to move freely about Warsaw, including [[Warsaw ghetto|Warsaw's Jewish ghetto]], without fear of being caught in a [[Roundup (history)|Nazi roundup]] and possibly transported to a labor camp.<ref name=Koper /><ref name="Yad Vashem">{{cite web|title=The Righteous Among the Nations|url=http://db.yadvashem.org/righteous/family.html?language=en&itemId=4043986|website=yadvashem.org|publisher=Yad Vashem|accessdate=22 July 2015}}</ref>

==Occupation of Warsaw==
During the occupation of Warsaw, Koper and a friend spent their nights publishing underground newspapers and forging travel and identification documents on a secret printing press.<ref name="WP obit" /><ref name="TWT obit" /><ref name="Yad Vashem" /> During the day, instead of collecting taxes, Koper would use his ''Ausweiss'' to visit Jewish friends in the ghetto, to whom he smuggled such luxuries as soap and Portuguese sardines, as well as the forged papers needed to escape to the [[Aryan]] side of the city.<ref name=Koper /><ref name="Yad Vashem" />

Among the friends he visited in the ghetto was Sophie Fanny Margulies, whom he had first met in 1935 while she was also studying journalism at the University of Warsaw. She had been relocated to the ghetto in October 1940, but she also held an ''Ausweiss'', provided to her from a friend on the [[Jewish Council]], and thus had avoided transport to the camps.<ref name=Koper /> The rest of her family was less fortunate, and had been transported to their deaths at [[Treblinka]] during the [[Grossaktion Warsaw]].<ref name="Yad Vashem" />

Marguiles recalled how Koper visited her in October 1942, to inform her that his ''Ausweiss'' was about to expire; if she wanted to escape, they must start planning now. Margulies was ready, having twice been nearly caught up in roundups in spite of her papers. On a snowy night in early February 1943, Marguiles followed Koper's instructions, finding her way at the prescribed time to a specified place along the ghetto wall where a secret opening was revealed to her, allowing her to pass through the wall to a waiting horse-drawn carriage which took her to Koper's apartment.<ref name=Koper />

Koper and his mother Marta cared for an apartment building located at 6 Ratuszowa Street, in the right bank suburb of [[Praga]], and lived in apartment 13.<ref name=Paullson>{{cite book|last1=Paulsson|first1=Gunnar S.|title=Secret City: The Hidden Jews of Warsaw, 1940–1945|date=2002|publisher=Yale University Press|isbn=9780300095463|page=44}}</ref> In addition to Sophie Marguiles, Koper also hid Bronislawa and Henryk Finkelstein, Dr. Maximilian Ciesieleski,<ref name="Yad Vashem" /> MIeczyslaw Goldstein<ref name=Paullson /> and Marek Stok.<ref name=Grynberg>{{cite book|editor1-last=Grynberg|editor1-first=Michael|title=Words to Outlive Us: Eyewitness Accounts from the Warsaw Ghetto|date=1 November 2003|publisher=Macmilllian|isbn=9780805058338|page=470}}</ref> They were among thirteen ghetto refugees who hid in Koper's apartment for various periods of time between 1942 and 1944,<ref name=Paullson /> including unaccompanied children who were eventually relocated to [[Catholic]] [[orphanage]]s.<ref name="Yad Vashem" /> Marguiles took an alias, and spent the months preceding the [[Warsaw Uprising]] helping Koper deliver the underground newspaper he was publishing.<ref name=Koper />

Koper was also part of the intelligence operation that warned the [[Allies of World War II|Allies]] about the [[Operation Barbarossa|German invasion of the Soviet Union]].<ref name="TWT obit" />

Koper persisted in his dangerous resistance and humanitarian efforts, undaunted by extraordinary personal risk, and then, as a member of the Polish Home Army, fought the Germans in the Warsaw Uprising in August 1944.<ref name="WP obit" /><ref name="TWT obit" /><ref name="Yad Vashem" /> When Warsaw fell, the Germans took Koper prisoner.<ref name="WP obit" /><ref name="TWT obit" /> A few months later, he escaped a [[Nazi]] prison camp and crossed the front to join the Polish Army.<ref name="WP obit" /><ref name="Yad Vashem" /> He published newspapers and worked in [[counter-intelligence]] until the end of the war.<ref name="TWT obit" />

==After the war==

Once hostilities ended, Koper married Sophie Margulies, who had also fought in the Warsaw Uprising as a nurse, treating Polish Home Army casualties during the battle.<ref name=Koper /><ref name="Yad Vashem" /> They relocated to [[Quakenbrück]], Germany, then occupied by the [[1st Armoured Division (Poland)|First Armoured Polish Division]], where Koper wrote news articles for ''Polish Soldiers Daily''. There, in 1947, Sophie gave birth to their son, [[Peter Koper|Peter]].<ref name=Koper />

Before emigrating to the United States, the Koper family spent a brief time in London, where Koper earned a doctorate in journalism from the Polish University of London.<ref name="WP obit" /><ref name=Koper />

In 1952, Koper moved his family to the United States to take a position as a professor at the [[Defense Language Institute]] in [[Monterey, California]]. In 1958, they relocated to [[Washington, DC]], where Koper worked at the United States Information Agency (USIA) until he retired in 1979.<ref name="TWT obit" /> At USIA, Koper edited ''Ameryka'', the Polish language version of the [[Cold War]] era "soft propaganda" magazine, ''[[Amerika (magazine)|Amerika]]''.<ref name="WP obit" /><ref name=Koper>{{cite web |last1=Koper |first1=Sophie |title=Oral History Interview |url=http://collections.ushmm.org/search/catalog/irn508494 |website=United States Holocaust Memorial Museum |accessdate=18 August 2015}}</ref>

Koper was president of the Polish Veterans Association in Washington D.C. in December 1981, when [[martial law in Poland|martial law]] was declared in Poland. "We're sitting helplessly on the sidelines. There is no communication, the post office doesn't work, the telephone doesn't work, traveling is impossible. It's unbelievable that in this time and age, the country can be padlocked completely," Koper told a reporter.<ref name=Granat>{{cite news|last1=Diane|first1=Granat|title=Poles Here Wait, Pray for Poland|url=http://www.washingtonpost.com/pb/archive/local/1981/12/25/poles-here-wait-pray-for-poland/2c001075-643c-42fb-930d-04c7e9913121/?resType=accessibility|accessdate=22 October 2015|work=The Washington Post|date=25 December 1981}}</ref> Before martial law ended, Koper was called out of retirement to work as an editor for [[Voice of America]].<ref name="WP obit">{{cite web |title=Air Force Brigadier General Eugene A. Stalzer Dies at 70 |url=http://www.washingtonpost.com/archive/local/1990/06/15/air-force-brigadier-general-eugene-a-stalzer-dies-at-70/60dd1871-2308-49db-bf38-9220094c0f5a/ |website=The Washington Post |accessdate=31 August 2015 |date=15 June 1990}}</ref><ref name="TWT obit" />

In 1989, fifty years after the Invasion of Poland, Koper and his wife returned to Poland on what he called a "sentimental journey," to see how Poland, and the people had changed, and to visit with the few survivors. He recalled the beauty of Warsaw before the invasion, and the unbelievable destruction wrought by the Germans. He told USA Today, "You can't really go home again, but for a visit you can try."<ref name=Koper /><ref>{{cite news |author1=Walte, Juan J. |author2=Johnson, Kevin |title=50 years, 50M lives ago; Poles in US recall WWII on anniversary |work=USA Today |date=1 September 1989}}</ref> Of their visit to Poland, Sophie Koper later remembered, "I did not find my Warsaw."<ref name=Koper />

In June, 1990, the Israeli Government awarded Koper the [[Yad Vashem]] award in recognition of his valor. A week later, on June 13, 1990, Koper died of cancer in [[Arlington, Virginia]]. He was survived by his wife and son.<ref name="WP obit" /><ref name="TWT obit" />

A month later, the Israeli diplomatic delegation to the United States honored Koper posthumously at a ceremony attended by his family in Washington, D.C.  His name appears on the Walls of Honor in Yad Vashem's [[Garden of the Righteous Among the Nations]].<ref name="Yad Vashem" />

Koper's history of the [[Polish underground press]] in Germany was published posthumously in 1993.<ref>{{cite journal |last1=Koper |first1=Antoni Stefan |title=Prasa polska w Niemczech |journal=Historical Papers (Paris) |date=1993 |volume=105 |issue=481 |pages=3–37 |trans_title=Polish Press in Germany|publisher=Literary Institute (Paris) |language=Polish |issn=0044-4391}}</ref>

==References==
{{Reflist|30em}}

==External links==
* [http://db.yadvashem.org/righteous/family.html?language=en&itemId=4043986 Antoni Koper at yadvashem.org]
* [http://collections.ushmm.org/search/catalog/irn508494 Sophie Koper at ushmm.org]
{{authority control}}
{{DEFAULTSORT:Koper, Antoni}}
[[Category:1990 deaths]]
[[Category:Polish journalists]]
[[Category:1906 births]]
[[Category:University of Warsaw alumni]]
[[Category:Polish resistance fighters of World War II]]
[[Category:Polish emigrants to the United States]]
[[Category:Defense Language Institute faculty]]
[[Category:Polish Righteous Among the Nations]]
[[Category:Deaths from cancer]]