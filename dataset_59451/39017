{{good article}}
{{infobox country at games
| NOC = BDI
| NOCname = Comité National Olympique du Burundi
| games = Summer Olympics
| year = 2008
| flagcaption = 
| oldcode = 
| website = 
| location = [[Beijing]]
| competitors = '''3'''
| sports = '''2'''
| flagbearer = [[Francine Niyonizigiye]]
| rank = 
| gold = 0
| silver = 0
| bronze = 0
| officials = 
| appearances = auto
| app_begin_year = 1996
| app_end_year = 
| summerappearances = 
| winterappearances = 
| seealso = 
}}

[[Burundi]] participated in the [[2008 Summer Olympics]], held in [[Beijing]], [[China]] from 8 to 24 August 2008.<ref name="Countries Participating in Olympics">{{cite web | url=http://olympics.sporting99.com/participating-countries.html | title=Countries Participating in Olympics | publisher=Sporting99.com | accessdate=26 September 2014}}</ref> It was Burundi's fourth appearance in the summer Olympics since its debut in 1996.<ref name="Sports Reference - Countries - Burundi">{{cite web | url=http://www.sports-reference.com/olympics/countries/BDI/ | title=Sports Reference – Countries – Burundi | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> The Burundi team included three athletes: runners [[Joachim Nshimirimana]] and [[Francine Niyonizigiye]], as well as swimmer [[Elsie Uwamahoro]].<ref name="Sports Reference - Beijing 2008 - Burundi">{{cite web | url=http://www.sports-reference.com/olympics/countries/BDI/summer/2008/ | title=Sports Reference – Beijing 2008 – Burundi | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> Niyonizigiye, a marathon runner, was flag bearer for the [[2008 Summer Olympics opening ceremony|opening ceremony]].<ref name=" List of flagbearers Bejing 2008">{{cite web | url=http://www.olympic.org/Documents/Reports/EN/en_report_1344.pdf | title=List of flagbearers Bejing &#91;sic&#93; 2008 | publisher=olympic.org | date=8 August 2008 | accessdate=26 September 2014 | pages=2}}</ref> None of the Burundi athletes progressed further than the qualifying round.<ref name="Sports Reference - Beijing 2008 - Burundi - Swimming">{{cite web | url=http://www.sports-reference.com/olympics/countries/BDI/summer/2008/SWI/ | title=Sports Reference – Beijing 2008 – Burundi – Swimming | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref><ref name="Sports Reference - Beijing 2008 - Burundi - Athletics">{{cite web | url=http://www.sports-reference.com/olympics/countries/BDI/summer/2008/ATH/ | title=Sports Reference – Beijing 2008 – Burundi – Athletics | publisher=[[Sports Reference]] | accessdate=28 September 2014}}</ref>

==Background==
Burundi had participated in three previous Summer Olympics, between its debut in the [[1996 Summer Olympics]] in [[Atlanta]], [[United States]] and the 2008 Summer Olympics in Beijing.<ref name="Sports Reference - Countries - Burundi" /> At its debut, the country sent [[Burundi at the 1996 Summer Olympics|seven athletes]] to the games, all of whom competed in [[Athletics at the 1996 Summer Olympics|athletics]].<ref name="Sports Reference - Atlanta 1996 - Burundi">{{cite web | url=http://www.sports-reference.com/olympics/countries/BDI/summer/1996/ | title=Sports Reference – Atlanta 1996 – Burundi | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> The largest number of Burundian athletes participating in the summer games is seven in 1996 and [[Athens 2004|2004]].<ref name="Sports Reference - Countries - Burundi" /> The only Burundian competitor to win a medal at the Olympics was [[Vénuste Niyongabo]], who won gold in the [[Athletics at the 1996 Summer Olympics – Men's 5000 metres|men's 5000 metres in 1996]].<ref name="Sports Reference - Atlanta 1996 - Burundi" /><ref name="Sports Reference - Athletes - Burundi - Vénuste Niyongabo">{{cite web | url=http://www.sports-reference.com/olympics/athletes/ni/venuste-niyongabo-1.html | title=Sports Reference – Athletes – Burundi – Vénuste Niyongabo | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> Three athletes from Burundi were selected to compete in the 2008 Olympics: [[Joachim Nshimirimana]] in the men's marathon, [[Francine Niyonizigiye]] in the women's track and field 5000 metres and [[Elsie Uwamahoro]] in the women's 50 metre freestyle swimming.<ref name="Sports Reference - Beijing 2008 - Burundi - Swimming"/><ref name="Sports Reference - Beijing 2008 - Burundi - Athletics"/>

==Athletics==
{{main|Athletics at the 2008 Summer Olympics}}

===Men's competition===
At the 2008 Olympics, Burundi was represented by one male in athletics,<ref name="Sports Reference - Beijing 2008 - Burundi - Athletics" /> marathon runner [[Joachim Nshimirimana]].<ref name="Sports Reference - Beijing 2008 - Burundi - Athletics" /> At age 35, Nshimirimana was the country's oldest competitor, and was competing at his second Olympics. He competed in the 2004 Olympics, also in marathon.<ref name="Sports Reference - Athletes - Burundi - Joachim Nshimirimana">{{cite web | url=http://www.sports-reference.com/olympics/athletes/ns/joachim-nshimirimana-1.html | title=Sports Reference – Athletes – Burundi – Joachim Nshimirimana | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> He competed on the 24 August in Beijing,<ref name="IAAF - Results - Olympic Games - 2008 - Marathon - Men - Final - Result">{{cite web | url=http://www.iaaf.org/results/olympic-games/2008/the-xxix-olympic-games-3659/men/marathon/final/result | title=IAAF – Results – Olympic Games – 2008 – Marathon – Men – Final – Result | publisher=[[IAAF]] | accessdate=26 September 2014}}</ref> and finished 68th out of 95{{efn|19 of the 95 athletes did not finish.<ref name="IAAF - Results - Olympic Games - 2008 - Marathon - Men - Final - Result" />}} in a time of 2{{nbsp}}hours, 29{{nbsp}}minutes and 55{{nbsp}}seconds, over 23{{nbsp}}minutes behind the winner, [[Samuel Kamau Wanjiru]].<ref name="IAAF - Results - Olympic Games - 2008 - Marathon - Men - Final - Result" />

{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Final
|-style="font-size:95%"
!Result
!Rank
|-align=center
|align=left|[[Joachim Nshimirimana]]
|align=left|[[Athletics at the 2008 Summer Olympics – Men's marathon|Marathon]]
|2:29:55	
|68
|}

===Women's competition===
[[File:Beijing National Stadium 1.jpg|thumb|250px|right|The [[Beijing National Stadium]], where Nshimirimana and Niyonizigiye competed in track and field events]]

[[Francine Niyonizigiye]], Burundi's flag bearer at the opening ceremony,<ref name=" List of flagbearers Bejing 2008" /> competed on Burundi's behalf at the Beijing Olympics, in the women's 5000{{nbsp}}metre races.<ref name="Sports Reference - Athletes - Burundi - Francine Niyonizigiye" /> Born in 1988, Niyonizigiye was 19 years old at the 2008 Olympics.<ref name="Sports Reference - Athletes - Burundi - Francine Niyonizigiye">{{cite web | url=http://www.sports-reference.com/olympics/athletes/ni/francine-niyonizigiye-1.html | title=Sports Reference – Athletes – Burundi – Francine Niyonizigiye | publisher=[[Sports Reference]] | accessdate=26 September 2014}}</ref> Niyonizigiye previously competed in the 2004 Olympics, also in the 5000 metres.<ref name="Sports Reference - Athletes - Burundi - Francine Niyonizigiye" /> During the qualification round, which took place on August 19, Niyonizigiye competed in the sixteen-person second heat.{{efn|One of the 16 athletes did not start the event.<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Results">{{cite web | url=https://www.iaaf.org/results/olympic-games/2008/the-xxix-olympic-games-3659/women/5000-metres/heats/result | title=IAAF – Results – Olympic Games – Beijing 2008 – 5000 metres – Women – Heats – Results | publisher=[[IAAF]] | accessdate=26 September 2014}}</ref>}} She finished the race in 17{{nbsp}}minutes, 8.44{{nbsp}}seconds, finishing in fourteenth place, the last of the finishing athletes, after [[Russia at the 2008 Summer Olympics|Russia]]'s [[Yelena Zadorozhnaya]] was disqualified.<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Results" /> Niyonizigiye finished just under a{{nbsp}}minute behind thirteenth placing [[Hungary at the 2008 Summer Olympics|Hungarian]] [[Krisztina Papp]].<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Results" /> The heat itself was led by [[Ethiopia at the 2008 Summer Olympics|Ethiopia]]'s [[Meseret Defar]] (14{{nbsp}}minutes 56.32{{nbsp}}seconds).<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Results" /> Of the 32 competitors to finish the event's first round, Niyonizigiye ranked 31st.<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Summary">{{cite web | url=http://www.iaaf.org/results/olympic-games/2008/the-xxix-olympic-games-3659/women/5000-metres/heats/summary | title=IAAF – Results – Olympic Games – Beijing 2008 – 5000 metres – Women – Heats – Summary | publisher=[[IAAF]] | accessdate=28 September 2014 | archiveurl=https://web.archive.org/web/20131107103121/http://www.iaaf.org/results/olympic-games/2008/the-xxix-olympic-games-3659/women/5000-metres/heats/summary | archivedate=7 November 2013}}</ref> She did not advance to later rounds.<ref name="IAAF - Results - Olympic Games - Beijing 2008 - 5000 metres - Women - Heats - Summary" />

{| class="wikitable" style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Final
|-style="font-size:95%"
!Time
!Rank
!Time
!Rank
|-align=center
|align=left|[[Francine Niyonizigiye]]
|align=left|[[Athletics at the 2008 Summer Olympics – Women's 5000 metres|5000 m]]
|17:08.44	
|14
|colspan=2|Did not advance
|}

==Swimming==
{{main|Swimming at the 2008 Summer Olympics}}

Burundi had one woman competing in swimming at the 2008 Olympics,<ref name="Sports Reference - Beijing 2008 - Burundi - Swimming" /> [[Elsie Uwamahoro]], who competed in the [[Swimming at the 2008 Summer Olympics – Women's 50 metre freestyle|50-metre freestyle]].<ref name="Sports Reference - Beijing 2008 - Burundi - Swimming" /> Beijing was Uwamahoro's first Olympics.<ref name="Sports Reference - Athletes - Burundi - Elsie Uwamahoro">{{cite web | url=http://www.sports-reference.com/olympics/athletes/uw/elsie-uwamahoro-1.html | title=Sports Reference – Athletes – Burundi – Elsie Uwamahoro | publisher=[[Sports Reference]] | accessdate=28 September 2014}}</ref> She was selected to compete in the second heat on 15 August.<ref name="Sports Reference - Beijing 2008 - Swimming - 50 metres freestyle - Women">{{cite web | url=http://www.sports-reference.com/olympics/summer/2008/SWI/womens-50-metres-freestyle-round-one.html | title=Sports Reference – Beijing 2008 – Swimming – 50 metres freestyle – Women | publisher=[[Sports Reference]] | accessdate=28 September 2014}}</ref> Uwamahoro finished seventh out of eight athletes in her heat, in a time of 36.86{{nbsp}}seconds. She finished ahead of [[Mariama Souley Bana]] from [[Niger at the 2008 Summer Olympics|Niger]] (40.83{{nbsp}}seconds). The heat was led by Zakia Nassar from [[Palestine at the 2008 Summer Olympics|Palestine]] and [[Karishma Karki]] from [[Nepal at the 2008 Summer Olympics|Nepal]] who swam 31.97{{nbsp}}and{{nbsp}}32.35{{nbsp}}seconds, respectively.<ref name="Sports Reference - Beijing 2008 - Swimming - 50 metres freestyle - Women" /> Her time placed her 86th out of 90 athletes and she did not qualify for the semi-finals.<ref name="Olympics results - Beijing 2008 - Swimming - 50 metres freestyle - Women">{{cite web | url=http://www.olympic.org/olympic-results/beijing-2008/swimming/50m-freestyle-w| title=Olympics results – Beijing 2008 – Swimming – 50 metres freestyle – Women | publisher=olympic.org | accessdate=28 September 2014}}</ref>

{|class=wikitable style="font-size:90%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="2"|Heat
!colspan="2"|Semifinal
!colspan="2"|Final
|-style="font-size:95%"
!Time
!Rank
!Time
!Rank
!Time
!Rank
|-align=center
|align=left|[[Elsie Uwamahoro]]
|align=left|[[Swimming at the 2008 Summer Olympics – Women's 50 metre freestyle|Women's 50 m freestyle]]
|36.86
|86
|colspan=4|Did not advance
|}

==Notes==
{{notelist|30em}}

==References==
{{Reflist|30em}}

{{Nations at the 2008 Summer Olympics}}

[[Category:Nations at the 2008 Summer Olympics]]
[[Category:Burundi at the Summer Olympics by year|2008]]
[[Category:2008 in Burundi|O]]