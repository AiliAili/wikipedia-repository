{{Infobox military person
|name=Antonio Maldonado
|birth_date= {{Birth year and age|1941}}
|death_date=
|birth_place= [[Comerio, Puerto Rico]]
|death_place=
|placeofburial=
|placeofburial_label= Place of burial
|image=Antonio Maldonado.jpg
|image_size=150
|caption='''Brigadier General Antonio Maldonado'''<br/><small>Youngest pilot and Aircraft Commander of a [[B-52 Stratofortress]] nuclear bomber</small>
|nickname='''"Tony"'''
|allegiance= {{flagicon|United States}} [[United States|United States of America]]
|branch= [[File:Puerto Rico Air National Guard.JPEG|25px]]&nbsp;[[Puerto Rico Air National Guard]] 
[[File:Seal of the US Air Force.svg|25px]]&nbsp;[[United States Air Force]]
|serviceyears=Puerto Rico Air National Guard (PRANG) 1960-1964<br> United States Air Force (USAF) 1964-1991
|rank=[[File:US-O7 insignia.svg|25px]]<br/>[[Brigadier general (United States)|Brigadier General]]
|commands=
|unit=[[34th Bomb Squadron]], [[60th Bombardment Squadron]], 67th Tactical Fighter Wing, [[432nd Tactical Reconnaissance Wing]]
|battles=[[Vietnam War]]
|awards=[[Defense Superior Service Medal]]<br/>[[Legion of Merit]]<br/>[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]<br/>[[Meritorious Service Medal (United States)|Meritorious Service Medal]] with three oak leaf clusters<br/>[[Air Medal]] with 10 oak leaf clusters<br/>[[Air Force Commendation Medal]]
|laterwork=
}}
[[Brigadier general (United States)|Brigadier General]] '''Antonio Maldonado''' (born 1941), was an officer of the [[United States Air Force]], who in 1967 became the youngest pilot and Aircraft Commander of a [[B-52 Stratofortress]] nuclear bomber. He served as Chief, U.S. Office of Defense Cooperation, [[Madrid, Spain]]. He was the senior Department of Defense representative to Spain and senior advisor to the US Ambassador to Spain. During the [[Gulf War|Persian Gulf War]] in 1991 he coordinated the overall US offensive operations from Spain.<ref name="USAF BIO">[http://www.af.mil/AboutUs/Biographies/Display/tabid/225/Article/106333/brigadier-general-antonio-maldonado.aspx USAF BIO.]</ref>

==Early years==
Maldonado was born in [[Comerio]], a town located in the center-eastern region of Puerto Rico. He is the youngest of twelve brothers and sisters born to Flor Maldonado Colón from Barranquitas and Carmen López Rodriguez from Naranjito. His family moved to the capital of the island, [[San Juan, Puerto Rico|San Juan]] and there he attended Central High School. Maldonado graduated from high school in May 1959 and continued his academic education in the [[University of Puerto Rico]] where he earned a Bachelor of Arts degree in business administration in May 1964. Started his military career as an enlisted Operation Specialist with the [[198th Airlift Squadron|198th TFS]] in the [[Puerto Rico Air National Guard]] while he was still a student at the University of Puerto Rico during the early 60's. Upon his graduation, Maldonado received the commission of [[second lieutenant#United States|second lieutenant]] in the United States Air Force as a Distinguished Graduate from the [[Air Force Reserve Officer Training Corps]] program. He married Carmen Gonzalez from San Juan, Puerto Rico.<ref name="USAF BIO"/>

==Military career==
[[File:B52.climbout.arp.jpg|right|thumb|250px|B-52 - Type of aircraft flown by Maldonado]]
Maldonado was sent to [[Moody Air Force Base]] in [[Georgia (U.S. state)|Georgia]] where he completed his Air Force pilot training in August 1965. He was then assigned as a B-52 pilot with the 34th Bomb Squadron at [[Wright Patterson Air Force Base]] in [[Ohio]] and in January 1969 became the youngest person to become an Aircraft Commander in said aircraft with the 60th Bombardment Squadron at [[Ramey Air Force Base]] in [[Puerto Rico]].<ref name="Lopez">Ildelfonso Lopez, ''Tras las Huellas de Nuestro Paso'', Pg. 34, Publisher: AEELA, 1998, Retrieved June 6, 2007; Publisher: El Mundo, February 28, 1968, retrieved April 25, 2013.</ref>  During his tour at Ramey he earned his Master of Business Administration degree at the [[Inter American University of Puerto Rico]] in May 1969. In 1970,he was assigned to the 67th Tactical Fighter Wing at [[Mountain Home Air Force Base]] in [[Idaho]], where he trained as an [[F-4C]] Phantom pilot.<ref name="USAF BIO"/>

==Vietnam War==
In January 1971, Maldonado was transferred to the 432nd Tactical Fighter Reconnaissance Wing, [[Udon Thani International Airport|Udon Royal Thai Air Force Base]] in [[Thailand]]. His active participation in the [[Vietnam War]] included 183 air combat missions over [[North Vietnam|North]] and [[South Vietnam]], [[Laos]] and [[Cambodia]] logging more than 400 combat flying hours in the F-4C Phantom.<ref name="USAF BIO"/>

==Return to the United States==
In December 1971, Maldonado returned to the United States upon completing his tour of duty in Vietnam. He was assigned to [[Bergstrom Air Force Base]] in [[Texas]], as an RF-4C Instructor Pilot. In December 1972 he was selected for a special assignment as an International Politico-military Affairs Officer to the [[U.S. Southern Command]], [[Albrook Air Force Base]] in the [[Panama Canal Zone]]. Maldonado left Panama to attend the Army Command and General Staff College from July 1976 to July 1977.<ref name="USAF BIO"/>

Among the assignments which he held from 1977 to 1982 were the following: B-52 Instructor Pilot and Chief of the Command Control Division (1977–1979) for the 42nd Bombardment Wing at [[Loring Air Force Base]], [[Maine]]; Commander of the 28th Bombardment Squadron and later Assistant Deputy Commander for Operations, 19th Bombardment Wing at [[Robins Air Force Base]], [[Georgia (U.S. state)|Georgia]] (1979–1981). He continued his military academic education by attending the [[Air War College]] at [[Maxwell Air Force Base]] in [[Alabama]] graduating in May 1982 as a Distinguished Graduate.<ref name="USAF BIO"/>

==Pentagon assignments==
Upon his graduation from the Air War College, Maldonado was assigned to [[Headquarters, United States Air Force]] at [[the Pentagon]] in [[Washington, D.C.]], as Chief, Western Hemisphere Division, Office of the Deputy Director for Plans and Policy. In June 1983 he was named Chief, Strategic Offensive Forces Division, Directorate of Plans.<ref name="USAF BIO"/>

In April 1984, Maldonado was transferred to [[K.I. Sawyer Air Force Base]] in [[Michigan]]. During the years which he spent there (1984–1987) he assumed  various leadership positions: Deputy Commander for Operations (1984), 410th Bombardment Wing; Vice Commander (September 1984)and Commander(July 1985). While commanding the 410th, General Maldonado won numerous top Air Force awards including the coveted Omaha Trophy (best combat Wing) and the 390th Bombardment Group Memorial Trophy (best Wing Commander).<ref name="USAF BIO"/>

On May 1987, Maldonado was reassigned once more to the Pentagon where he served as Chief, Strategic Operations Division, Operations Directorate, Office of the [[Joint Chiefs of Staff]]. In June 1988 he became Deputy Director for Operations, National Military Command Center, the Pentagon. On September 1 of that same year, he was promoted to the rank of [[Brigadier general (United States)|Brigadier General]] becoming the second Air Force Officer from Puerto Rico and the first from a Puerto Rico University to reach the general officer ranks.<ref name="Kelly">,
John T. Kelly, Publisher El Nuevo Dia, April 10, 1988; El Mundo, February 8, 1988, retrieved April 24, 2013.</ref>

On July 1989, Maldonado was named Chief, U.S. Office of Defense Cooperation, [[Madrid, Spain]], becoming the senior Department of Defense representative to that country. His responsibilities included providing overall direction to U.S. elements in Spain on status of forces, security assistance programs and other defense and base agreement matters. He also provided overall coordination for US offensive operations out of Spain during the 1991 Persian Gulf War.<ref name="USAF BIO"/>
Brigadier General Maldonado retired from the United States Air Force on September 1, 1991 with more than 4,000 hours of flight, after 27 years of service of active duty service.

==Later years==
After Maldonado retired from the Air Force in 1991, he was named President of "Fomento Industrial de Puerto Rico" (Puerto Rico Industrial Development Company) by the [[Government of Puerto Rico]], a position which he held until January 1993.<ref name="Lopez"/> In 1993 he became the CEO of the [[American Red Cross]], Puerto Rico Region. In 1999 he became Executive Director of [[Palmas Del Mar Beach Resort|Palmas Del Mar]] Homeowners Association, in [[Humacao, Puerto Rico]]. Maldonado is a board member of the US [[National Marrow Donor Program]] and a member of the Board of Governors PR Aqueducts Authority.<ref>{{cite web|url=http://www.marrow.org/NEWS/News_Releases/2003/20030213_board.html |title=National Marrow Donor Program |access-date=2007-03-04 |dead-url=yes |archiveurl=https://web.archive.org/web/20071109122337/http://www.marrow.org/NEWS/News_Releases/2003/20030213_board.html |archivedate=2007-11-09 |df= }}</ref> Maldonado is married to Ilia Lopez from Humacao, Puerto Rico and has four daughters Carmen Zaydee, Joyce, Suzy and Elsie Oshiry and grandchildren Calvin, Isabella, Gabriella, Allegra and Spencer.<ref name="Kelly"/>

==Awards and recognitions==
Among Brigadier General Antonio Maldonado's decorations and medals were the following:
*[[File:US Defense Superior Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Defense Superior Service Medal]] with oak leaf cluster
*[[File:Legion of Merit ribbon.svg|60px]]&nbsp;&nbsp;[[Legion of Merit]]
*[[File:Distinguished Flying Cross ribbon.svg|60px]]&nbsp;&nbsp;[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]],
*[[File:Meritorious Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Meritorious Service Medal (United States)|Meritorious Service Medal]] with three oak leaf clusters,
*[[File:Air Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Air Medal]] with 10 oak leaf clusters,
*[[File:Air Force Commendation ribbon.svg|60px]]&nbsp;&nbsp;[[Air Force Commendation Medal]]
*[[File:Air_Force_Longevity_Service_ribbon.svg|60px]]&nbsp;&nbsp;[[Air Force Longevity Service Award]]
*[[File:National Defense Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[National Defense Service Medal]] with bronze service star,
*[[File:Armed Forces Expedtionary Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Armed Forces Expeditionary Medal]] with bronze service star,
*[[File:Vietnam Service Medal ribbon.svg|60px]]&nbsp;&nbsp;[[Vietnam Service Medal]] with bronze service star
*[[File:Vietnam Campaign Medal ribbon with 60- clasp.svg|60px]]&nbsp;&nbsp;[[Vietnam Campaign Medal]]
* [[File:VNCivilActionsRibbon-2.svg|60px]]&nbsp;&nbsp;[[Vietnam Civil Actions Medal]]
* [[File:Vietnam gallantry cross unit award-3d.svg|60px]]&nbsp;&nbsp;[[Vietnam Cross of Gallantry]] with Palm Streamer
* [[File:Inter-American Defense Board Medal.jpg|60px]]&nbsp;&nbsp;[[Inter-American Defense Board Medal]]
'''Badges:'''
* [[File:COMMAND PILOT WINGS.png|100px]]&nbsp;&nbsp;[[USAF Aeronautical Ratings#Pilot ratings|Command pilot]]
* [[File:Joint Chiefs of Staff seal.svg|60px]]&nbsp;&nbsp;[[Office of the Joint Chiefs of Staff Identification Badge]]

==See also==
{{Portal|Biography|Puerto Rico|United States Air Force}}
* [[List of Puerto Ricans]]
* [[List of Puerto Rican military personnel]]
* [[Hispanics in the United States Air Force]]

==References==
{{Reflist}}

==Further reading==
*''Puertorriquenos Who Served With Guts, Glory, and Honor. Fighting to Defend a Nation Not Completely Their Own''; by : Greg Boudonck; ISBN 978-1497421837

{{DEFAULTSORT:Maldonado, Antonio}}
[[Category:Air War College alumni]]
[[Category:American military personnel of the Vietnam War]]
[[Category:American Red Cross personnel]]
[[Category:Interamerican University of Puerto Rico alumni]]
[[Category:United States Air Force generals]]
[[Category:People from Comerío, Puerto Rico]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Puerto Rican United States Air Force personnel]]
[[Category:Puerto Rican aviators]]
[[Category:Puerto Rican military personnel]]
[[Category:Puerto Rican military officers]]
[[Category:1941 births]]
[[Category:Living people]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Defense Superior Service Medal]]
[[Category:United States Air Force officers]]