{{Infobox non-profit
| name              = International Mountain Society
| image             =  
| type              = International organization
| founded_date      = {{Start date|1980}}
| founders           = Jack D. Ives, Roger Barry, Mischa Plam, Walther Manshard
| founding_location = [[Boulder, Colorado]], USA
| tax_id            = 
| registration_id   =  
| headquarters      = [[Bern]], [[Switzerland]]
| origins           = 
| key_people        = 
| area_served       = Worldwide
| product           = 
| mission           = The IMS aims to promote sustainable mountain development through improved communication among institutions and individuals, with a particular focus on mountain ecoregions in the developing world.
| focus             = Sustainable mountain development
| method            = 
| revenue           = 
| endowment         = 
| num_volunteers    = 
| num_employees     = 
| num_members       = 
| leader_title      =  
| leader_name       =  
| non-profit_slogan = 
| former name       = 
| homepage          = {{URL|http://www.mrd-journal.org/ims.asp}}
| dissolved         = 
| footnotes         = 
}}

The '''International Mountain Society'''  ('''IMS''') is a scientific  research society focusing on the dissemination of information about [[mountain research]] and mountain development throughout the world, but particularly in developing regions.<ref>{{cite book |last=Gerrard
 |first=John  |date=1990 |title=Mountain Environments: An Examination of the Physical Geography of Mountains |publisher=MIT Press |page=1 |url=https://books.google.com/books?id=jHnrVEyMhkQC&pg=PA1&lpg=PA1&dq=%22International+Mountain+Society%22&source=bl&ots=vzNDln4KaO&sig=rsmTgdrhyIYxHH7c2NnxZN14_5c&hl=en&sa=X&ved=0ahUKEwiCgubZ8N_NAhUHcRQKHTAwAOY4FBDoAQg-MAY#v=onepage&q=%22International%20Mountain%20Society%22&f=false |via=Google |isbn=0-262-07128-2}}</ref> IMS is the copyright-holder and co-publisher, along with the [[United Nations University]] (UNU), of  the quarterly journal '''Mountain Research and Development''' ('''MRD'''). IMS was incorporated in [[Boulder, Colorado]] in 1980. Since 2000 the IMS has been based in [[Bern, Switzerland]].<ref name="mrd-ims">{{cite web |url = http://www.mrd-journal.org/ims.asp |title = International Mountain Society | publisher = Mountain Research and Development | accessdate = July 4, 2016}}</ref> 
Membership in the IMS, which includes subscription to MRD, is available to individuals and to organizations.<ref name="mrd-ims" />
 
== Aims  ==

The mission statement of both the IMS and its journal, ''Mountain Research and Development'', is "to strive for a better balance between mountain environment, development of resources and the well-being of mountain peoples."<ref>{{Citation | author=<!--Staff writer(s); no by-line.--> |title=Organizational Information |work=Mountain Research and Development |date= May 1981 |volume=1 |issue=1 |page=inside cover }}</ref> The practical purpose of the IMS was to provide a vehicle for collaboration with [[United Nations University]] in the publication of a new mountain research and applied study journal.<ref>{{Citation |author=Soedjatmoko |title=Foreword |work=Mountain Research and Development |date= May 1981 |volume=1 |issue=1 |pages=1 }}</ref> The intent was to build on the expanding commitments of a number of international institutions that were beginning to respond to the perceived widespread problems facing the mountains and mountain peoples of the world.<ref name="Jack D. Ives 3–4">{{Citation |author=Jack D. Ives |title=Editorial |work=Mountain Research and Development | publisher=United Nations University and International Mountain Society |date= May 1981 |volume=1 |issue=1 |pages=3–4}}</ref>

== History ==
The International Mountain Society grew out of multidisciplinary collaborations of participants in the [[UNESCO]] [[Man and the Biosphere Programme]] (MAB), especially MAB Project 6 (study of the impact of human activities on mountain ecosystems). IMS and the new journal, MRD, were aided financially and intellectually by the UNU,  Arbeitsgemeinschaft für Hochgebirgsforschung, UNESCO, the Commission on High Altitude Geoecology of the [[International Geographical Union]] (IGU), the [[United Nations Environment Programme]] (UNEP), and the [[International Union for Conservation of Nature and Natural Resources]] (IUCN).<ref>{{Citation |author=Jack D. Ives |title=Editorial |work=Mountain Research and Development |date= May 1981 |volume=1 |issue=1 |pages=3–4 }}</ref> The intellectual underpinnings included the Biosphere Reserve concept that was applied by UNESCO in promoting research, training, and genepool conservation in designated areas, as well as the UNU's approach to mountain research that emphasized the importance of highland-lowland interactive systems.<ref name="Jack D. Ives 3–4"/>

The specific impetus for the founding of the IMS stemmed from 1974 conference on the Development of Mountain Environment that was convened by the German Technical Cooperation Agency (GTZ) in Munich. Participants, including John Cool, Frank Davidson, Klaus Lampe, A.D.Moddie, Joseph Stein, B.B. Vohra, and J.D. Ives, concluded that there was a need for an organization linking mountain researchers and institutions and also for a journal focused on mountain research. These points came up again in a 1976 workshop convened in Cambridge, Massachusetts. At the time of incorporation in Boulder, Colorado, the officers of IMF were:
* President: Jack D. Ives, [[University of Colorado, Boulder]], USA
* Vice-President: Corneille Jest, Centre national de la recherche scientifique, [[Paris, France]]
* Vice-President: Heinz Loeffler, [[University of Vienna]], [[Vienna, Austria]]
* Secretary: Roger G. Barry, [[University of Colorado Boulder]], USA
* Treasurer: Misha Plam, University of Colorado, Boulder, USA<ref name="Jack D. Ives 3–4"/>

== Achievements ==

The primary product of the International Mountain Society is the quarterly peer-reviewed journal Mountain Research and Development. The editorial policies emphasize  the need for accelerated interdisciplinary international research, both pure and applied. The membership of the founding editorial and advisory boards was international, representing sixteen nations, and special attention was paid to facilitating publication in English of manuscripts submitted in any language.
	MRD was edited by Jack Ives and Pauline Ives, and Jack Ives also served as President of the IMS until  2000, when both the journal and the organization were transferred to Bern University, Switzerland, with Hans Hürni assuming responsibility.<ref>{{Citation |author=Jack D. Ives |title=Editorial |work=Mountain Research and Development |date= May 1999 |volume=19 |issue=4 |pages=3–4 }}</ref>  
	MRD has been able to ensure worldwide publication of a wide range of mountain research results. Serving as a linkage between many international institutions, universities, and NGOs, in collaboration with [[Maurice Strong]], Secretary General of  the 1992 [[United Nations Conference on Environment and Development (UNCED)]], also known as the [[Rio de Janeiro Earth Summit]], the IMS contributed to the adoption of Chapter 13: Mountains o  [[Agenda 21]], the Rio action plan. This, in turn, led to the United Nations designation of 2002 as the International Year of Mountains and  December 11 as the annual [[International Mountain Day]].<ref>{{cite book |last=Ives |first=Jack |date=2013 |title=Sustainable Mountain Development: Getting the facts right |publisher=Himalayan Association for the Advancement of Science |page=205 |isbn= 978-9937261951}}</ref><ref>{{Citation |authors=Hanspeter Liniger, Gudrun Schwilch and Hans Hurni |title=Editorial |work=Mountain Research and Development |date= 2002 |volume=22 |issue=1 |pages=3 }}</ref>

==References==
{{Reflist}}



[[Category:Organisations based in Bern]]
[[Category:Research institutes in Switzerland]]
[[Category:Mountains]]