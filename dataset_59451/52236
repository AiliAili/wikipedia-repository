{{Infobox journal
| title = Mathematical Programming
| cover = [[File:MPcover2010.jpg]]
| editor = Alex Shapiro (series A), Jong-Shi Pang (series B)
| discipline = [[Mathematics]], [[computer science]]
| formernames = Mathematical Programming Studies
| abbreviation = Math. Prog.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency =
| history = 1971-present
| openaccess =
| license =
| impact = 1.984
| impact-year = 2013
| website = http://www.springer.com/mathematics/journal/10107
| link1 = http://www.springerlink.com/content/103081/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 1585989
| LCCN = 74618643
| CODEN =
| ISSN = 0025-5610
| eISSN = 1436-4646
}}
'''''Mathematical Programming''''' is a [[Peer review|peer-reviewed]]  [[scientific journal]] that was established in 1971 and is published by [[Springer Science+Business Media]]. It is the official journal of the [[Mathematical Optimization Society]] and consists of two series: ''A'' and ''B''. The "A" series contains general publications, the "B" series focuses on topical [[Optimization (mathematics)|mathematical programming]] areas. The [[editor-in-chief]] of Series A is Alex Shapiro ([[Georgia Institute of Technology|Georgia Tech]]); for Series B this is Jong-Shi Pang ([[University of Southern California|USC]]).

== History ==
The journal has been published by Springer since January 1999. '''''Mathematical Programming Studies''''' is the predecessor of the Series B part of this journal.

== Abstracting and indexing ==
''Mathematical Programming'' is abstracted and indexed in:<ref name=home>{{cite web |url=http://www.springer.com/mathematics/journal/10107 |title=About Mathematical Programming |format= |work= |accessdate=2010-07-15}}</ref>
{{columns-list|colwidth=30em|
* [[ABI/INFORM]]
* ABS Academic Journal Quality Guide
* [[Academic OneFile]]
* [[Academic Search]]
* [[Compendex]]
* [[Computer Science Index]]
* [[Current Abstracts]]
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Current Index to Statistics]]
* [[Digital Bibliography & Library Project]]
* [[Digital Mathematics Registry]]
* [[GeoRef]]
* [[International Abstracts in Operations Research]]
* [[Mathematical Reviews]]
* [[PASCAL (database)|PASCAL]]
* [[Science Citation Index]]
* [[Scopus]]
* [[TOC Premier]]
* [[VINITI Database RAS]]
* [[Zentralblatt MATH]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 1.970.<ref name=WoS>{{cite book |year=2012 |chapter=Mathematical Programming |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-06-11 |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.springer.com/mathematics/journal/10107}}

[[Category:Mathematics journals]]
[[Category:English-language journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1971]]