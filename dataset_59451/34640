{{good article}}
{{Use Canadian English|date=February 2015}}
{{Italic title}}
[[File:Die Passion Eines Menschen 22.png|thumb|alt=A black-and-white illustration.  A group of workers on the right battles against a gun-wielding group on the left.  In the centre, facing left, a man in the foreground raises his hand.|The protagonist leads a workers' revolt]]
<!-- No infobox, please -->
'''''25 Images of a Man's Passion''''', or '''''The Passion of a Man''''' is the first [[wordless novel]] by [[Flemish people|Flemish]] artist [[Frans Masereel]] (1889–1972), first published in 1918 under the French title {{lang|fr|''25 images de la passion d'un homme''}}.  The silent story is about a young working-class man who leads a revolt against his employer.  The first of dozens of such works by Masereel, the book is considered to be the first wordless novel, a genre that saw its greatest popularity in Europe in the 1920s and 1930s.  Masereel followed the book in 1919 with his best-known work, ''[[Passionate Journey]]''.

Masereel had grown up reading revolutionary socialist literature, and expressed his politics in ''A Man's Passion''; the work is also filled with religious imagery, as with the Common Man taking the role of the martyred Christ.  It owed its visual style to [[Expressionism]] and mediaeval [[woodcut]]s.  The book was popular, particularly in German editions, which had introductions by writers [[Max Brod]], [[Hermann Hesse]], and [[Thomas Mann]].

==Background==

[[Frans Masereel]] (1889–1972){{sfn|Beronä|2007|p=v}} was born into a French-speaking family{{sfn|Willett|2005|p=132}} in [[Blankenberge]], Belgium.  When he was five his father died, and his mother remarried to a doctor in [[Ghent]], whose political beliefs left an impression on the young Masereel.{{sfn|Beronä|2007|p=v}}  Masereel grew up reading [[Marxism|Marxist]], [[Socialism|socialist]], and [[anarchism|anarchist]] works by such writers as [[Karl Marx]] and [[Peter Kropotkin]],{{sfn|Cohen|1977|p=182}} and often accompanied his stepfather in socialist demonstrations.  After a year at the [[Royal Academy of Fine Arts (Ghent)|Ghent Academy of Fine Arts]] in 1907, Masereel left to study art on his own in Paris.{{sfn|Beronä|2007|p=v}}  During World War I he volunteered as a translator for the [[International Red Cross and Red Crescent Movement|Red Cross]] in Geneva, drew newspaper political cartoons, and copublished a magazine called ''Les Tablettes'', in which he published his first [[woodcut]] prints.{{sfn|Beronä|2007|p=vi}}

In the early 20th century there was a revival in interest in mediaeval woodcuts, particularly in religious books such as the ''[[Biblia pauperum]]''.{{sfn|Willett|2005|p=126}}  The woodcut is a less refined medium than the [[wood engraving]] that replaced it—artists of the time took to the rougher woodcut to express angst and frustration.{{sfn|Willett|2005|p=127}}  From 1917 Masereel began publishing books of woodcut prints,{{sfn|Beronä|2007|pp=vi–vii}} using similar imagery to make political statements on the strife of the common people rather than to illustrate the lives of Christ and the saints.{{sfn|Willett|2005|p=127}}  In 1918 he created the first such book to feature a narrative, ''25 Images of a Man's Passion'',{{sfn|Beronä|2007|pp=vi–vii}} which is thus the earliest example of the [[wordless novel]] genre.{{sfn|Willett|2005|pp=112}}

==Synopsis==

''25 Images of a Man's Passion'' tells of a young man who protests injustice against the working class in an industrialized society.  The man is born to an unwed mother, struggles to make a living, and drinks and whores with his coworkers.  He educates himself by reading and talking with his coworkers, and is executed by the authorities for leading a revolt against his employer.{{sfn|Beronä|2008|p=16}}

<gallery mode="packed" heights="200px" caption="Scenes from ''25 Images of a Man's Passion''">

Frans Masereel (1918) 25 umages de la passion d'un homme - title page.png|alt=A black-and-white title page in French that reads, "25 images de la passion d'un homme.  Dessinées et gravées sur bois par Frans Masereel 1918"|
Die Passion Eines Menschen 02.jpg|alt=A black-and-white illustration of a woman holding a baby being chased out of a building|
Frans Masereel (1918) Die Passion Eines Menschen 8.jpg|alt=A black-and-white illustration of a large police officer strangling a smaller man|
Frans Masereel (1918) Die Passion Eines Menschen 24.jpg|alt=A black-and-white illustration of a courthouse with a crucifix hung prominently above the judge's seat|
Frans Masereel (1918) Die Passion Eines Menschen 25.jpg|alt=A black-and-white illustration of a man standaing against a wall, his hands bound behind his back|

</gallery>

==Style and analysis==

The title and content of the book have biblical resonances with the mediaeval woodcuts from which they draw inspiration.  In line with Masereel's politics, the Common Man is martyred instead of Christ;{{sfn|Willett|2005|pp=126–127}} during his trial, Christ on the crucifix shines light upon the man.{{sfn|Willett|2005|pp=114}}  The cover of the German edition depicts the main character burdened Christ-like with a crucifix.{{sfn|Willett|2005|pp=112}}

[[File:Fritz Kortner in Die Wandlung 1919.jpg|thumb|alt=A black-and-white photo of a man and woman on a stage|The story parallels that of [[Ernst Toller]]'s ''The Transformation''.<br />''Pictured:'' a production of ''The Transformation'' on 30 September 1919]]

Visually, the book owes much to [[Expressionism]], though experts disagree on whether to label Masereel's work Expressionist; critic Lothar Lang finds Masereel's revolutionary politics to set Masereel apart from the Expressionists.{{sfn|Willett|2005|pp=126–127}}  Perry Willet finds parallels between the story arc of Masereel's book and that of [[Expressionism (theatre)|Expressionist playwright]] [[Ernst Toller]]'s ''The Transformation''{{efn|{{lang-de|Die Wandlung}} }} (1919), though Masereel's work was the more political—Toller lacked Masereel's commitment to socialism.{{sfn|Willett|2005|pp=114}}  Socialist themes of the martyrdom of the working class were common in wordless novels;{{sfn|Willett|2005|pp=114}} with the city as a backdrop to a worker's struggle against oppression, the book set the tone and themes for future wordless novels by Masereel and other artists, such as the American [[Lynd Ward]].{{sfn|Cohen|1977|p=182}}

==Publication and reception==

Printed from twenty-five woodcut blocks, the book was first released in 1918 by Édition de Sablier, a Swiss publishing house of which Masereel was a co-sponsor.  It was first offered as a numbered collectors' edition, and followed by [[Paperback#Trade paperback|trade editions]].{{sfn|Willett|2005|pp=112}}  [[Kurt Wolff (publisher)|Kurt Wolff]] produced an inexpensive German edition ({{lang|de|''Die Passion eines Menschen''}}) in 1921.{{sfn|Beronä|2008|p=16}}  The German edition was particularly popular, and its several editions had introductions by writers [[Max Brod]], [[Hermann Hesse]], and [[Thomas Mann]].{{sfn|Willett|2005|pp=112}}  In the same Expressionistic style, Masereel followed ''Man's Passion'' with ''[[Passionate Journey]]''{{efn|{{lang-fr|Mon livres d'heures}}, also known as ''My Book of Hours''}} (1919), ''[[The Sun (wordless novel)|The Sun]]''{{efn|{{lang-fr|Le soleil}} }} (1919), ''[[Story Without Words]]''{{efn|{{lang-fr|Histoire sans paroles}} }} (1920), and ''[[The Idea (wordless novel)|The Idea]]''{{efn|{{lang-fr|Idée}} }} (1920).{{sfn|Willett|2005|p=118}}

==See also==

* ''[[Gods' Man]]''
* [[Graphic novel]]

{{Portal bar|Comics|Novels|Socialism|Visual arts}}

==Notes==

{{Notelist|colwidth=40em}}

==References==

{{Reflist|colwidth=20em}}

===Works cited===

{{Refbegin}}

* {{cite book |last = Beronä |first = David A. |title = Wordless Books: The Original Graphic Novels |url = https://books.google.com/books?id=YVh2QgAACAAJ |year = 2008 |publisher = [[Abrams Books]] |isbn = 978-0-8109-9469-0 |ref = harv }}<!-- Beronä 2008 -->
* {{cite journal
|ref       = harv
|title     = The Novel in Woodcuts: A Handbook
|jstor       = 3831165
|first     = Martin S.
|last      = Cohen
|journal   = Journal of Modern Literature
|volume    = 6
|issue     = 2
|date=April 1977
|pages     = 171–195
|publisher = [[Indiana University Press]]}}
* {{cite book
|last         = Beronä
|first        = David
|chapter      = Introduction
|pages        = v–ix
|editor-last  = Beronä
|editor-first = David
|title        = Frans Masereel: Passionate Journey: A Vision in Woodcuts
|year         = 2007
|publisher    = [[Dover Publications]]
|isbn         = 978-0-486-13920-3
|ref          = harv}}
* {{cite book
|last         = Willett
|first        = Perry
|chapter      = The Cutting Edge of German Expressionism: The Woodcut Novel of Frans Masereel and Its Influences
|pages        = 111–134
|editor-last  = Donahue
|editor-first = Neil H.
|title        = A Companion to the Literature of German Expressionism
|url          = https://books.google.com/books?id=zjvV48n-ngUC&pg=PA111
|year         = 2005
|publisher    = [[Camden House]]
|isbn         = 978-1-57113-175-1
|ref          = harv}}

{{Refend}}

==External links==
* [http://www.frans-masereel.de/15377_Passion.html ''25 Images of a Man's Passion''] online at the Frans-Masereel-Foundation website

{{Wordless novels}}

[[Category:1918 books]]
[[Category:Woodcut novels by Frans Masereel]]
[[Category:Social realism]]