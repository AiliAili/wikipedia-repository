{{ infobox bibliographic database
| title       = Ei Compendex
| image       = 
| caption     = 
| producer    = [[Elsevier]]
| country     = 
| history     = 1884-present
| languages   = 
| providers   = 
| cost        = 
| disciplines = Engineering
| depth       = 
| formats     = 
| temporal    = 
| geospatial  = 
| number      = 
| updates     = 
| p_title     = Engineering Index
| p_dates     = 
| ISSN        = 0742-1974
| web         = http://www.elsevier.com/solutions/engineering-village/content/compendex
| titles      =  http://www.elsevier.com/__data/assets/excel_doc/0006/251196/Compendex-source-list_02272015.xlsx
}}

'''Ei Compendex''' is an [[engineering]] [[bibliographic database]] published by [[Elsevier]].
It indexes [[scientific literature]] pertaining to engineering materials. Beginning in 1884, it was compiled by hand under the original title of ''Engineering Index''.
The name "Compendex" stands for COMPuterized ENgineering inDEX.<ref>[http://www.ei.org/evhistory ''EI History'']. Elsevier Engineering Information. Accessed May 6, 2009.</ref>  
As a result of computerization in 1967, the first electronic Engineering Index bulletin was sent to 500 subscribers.<ref name=hane/>
Elsevier purchased the parent company Engineering Information in 1998.<ref name=hane> Hane, Paula J. [http://newsbreaks.infotoday.com/NewsBreaks/Elsevier-Science-Acquires-Engineering-Information-18017.asp  Elsevier Science Acquires Engineering Information]. Information Today, Inc. 06 February 1998</ref> 

==Coverage==
Ei Compendex currently contains over 18 million records as of July 28, 2015 <ref>[http://www.ei.org ''EI Welcome]. Elsevier Engineering Information. Accessed February 20, 2013.</ref> and references over 5,000 international sources including [[Academic journal|journals]], [[Academic conference|conferences]] and [[trade publication]]s. Approximately 1,000,000 new records<ref>[http://ei.org/compendex-ei-backfile ''Compendex & EI Backfile'']. Elsevier Engineering Information. Accessed February 20,2013.</ref> are added to the database annually from over 190 disciplines within the engineering field. Coverage is from 1970 to the present, and is updated weekly.<ref>[http://www.stn-international.com/uploads/tx_ptgsarelatedfiles/COMPENDEX_01.pdf File summary]. STN.[[FIZ Karlsruhe]]. Accessed June 16 2014.</ref>

Coverage of engineering subjects include [[nuclear engineering|nuclear technology]], [[bioengineering]], [[transportation]], [[chemical engineering|chemical]] and process engineering, light and optical technology, [[agricultural engineering]] and [[food technology]], [[computer]]s and [[data processing]], [[applied physics]], [[electronics]] and [[communications]], control, [[civil engineering|civil]], [[mechanical engineering|mechanical]], [[materials engineering|materials]], [[petroleum engineering|petroleum]], [[aerospace engineering|aerospace]] and [[automotive engineering]] as well as multiple subtopics within all these and other major [[Engineering|engineering fields]].

==References==
<references/> 

== External links ==
* [http://www.ei.org/compendex Compendex and EI backfile]
* [http://www.stn-international.com/index.php?id=176 Compendex on STN]
* [http://www.stn-international.com/uploads/tx_ptgsarelatedfiles/COMPENDEX_01.pdf STN Compendex file summary.]. PDF download. January 2014
* [http://www.ei.org/evhistory Ei and Compendex History]

{{Reed Elsevier}}

[[Category:Bibliographic databases in engineering]]
[[Category:Online databases]]