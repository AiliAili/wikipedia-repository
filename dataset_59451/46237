[[File:SH-60F dipping sonar.jpg|240px|right|thumb|U.S. Navy SH-60F helicopter with AQS-13F transducer deployed]]
The '''AQS-13''' (AN/AQS-13) series [[sonar]]s, AQS-13, AQS-13A, AQS-13B, AQS-13E and AQS-13F served as the helicopter [[dipping sonar]] systems for the [[United States Navy]]. These systems were deployed as the primary inner zone [[anti-submarine warfare]] (ASW) sensor on [[aircraft carrier]] based helicopters for over 5 decades.<ref>{{cite web|title=ASW Helicopters|url=http://www.globalsecurity.org/military/systems/aircraft/rotary-asw.htm|publisher=Global Security}}</ref> Companion versions with the AQS-18 designation were exported to various nations around the globe.

==Purpose==

Airborne sonar systems provide a light-weight, mobile detection sensor for locating and tracking submarines. Although limited in performance compared to larger, ship-mounted sonars, these helicopter-borne systems have the advantage of rapid deployment/retrieval times, and rapid transition between search areas. Additional advantages over ship sonars include absence of flow noise and engine noise, and the elimination of [[Doppler effect|Doppler shift]] induced by a moving signal source. Deployed from Aircraft Carriers or other ships, these systems enable the aircraft to locate, identify and attack submerged targets within the flight radius from the home ship.

==Description==

The AQS-13 systems are all primarily [[active sonar]] transmitting in the upper end of the medium frequency sonar range. These systems offered the additional capabilities of voice communication, [[bathythermography]] and rudimentary passive sonar. Helicopter borne active sonar has significant advantages over other sensors and is especially effective in the inner zone where noise from the ships of the [[carrier battle group]] can interfere with passive sensors. The components of the AQS-13 are informally divided into two groups, the "wet end" and "dry end." The "dry end" involves the processing of the acoustic signals to obtain tactical data. The "wet end" components are those necessary to deploy the acoustic unit into the ocean and retrieve it again. These "wet" components include the acoustic elements in a submersible unit, the reel & cable and reeling machine. The [[hydrophone]] and projector elements are housed within the submersible unit or [[transducer]]. The transducer, also called the "dome", a term borrowed from ship-board sonars, is lowered or "dipped" from the helicopter on a cable by means of the hydraulic reeling machine. The dip depth of the transducer is selected by the operator to achieve the maximum detection probability at the dip location on that particular day as determined by the study of ocean conditions (see [[Underwater acoustics|Underwater Acoustics]]). During active search, the acoustic pulse is emitted from the projector assembly. Echos or "returns" are received by the [[hydrophone]], routed through the sonar cable, processed in the aircraft and displayed on a [[cathode ray tube]] (CRT) in a [[plan position indicator]] (PPI) format. Returns were also processed and made available to the RO-358 chart recorder in the AQS-13E and earlier systems. This data was made available to the aircraft tactical computer in the AQS-13F. The various functions were selectable by the operator, such as pulse lengths, range scales and other modes to enhance operations for the particular conditions. Sensor elements on the reeling machine monitor the relative angle of the deployed sonar cable and provide flight reference signals to the aircraft stabilization equipment in order to maintain a steady hover position over the submerged transducer. Tactical data of the target is obtained from the acoustic returns, including range, bearing and relative speed. Later versions of the AQS-13 were also capable of processing acoustic signals transmitted to the aircraft from [[sonobuoys]].

==History==

===AQS-13===

The AQS-13 system was introduced to the U.S. fleet aboard the [[Sikorsky SH-3D]] [[anti-submarine warfare helicopter]] in the mid-1960s.<ref>{{cite web|author=Tailspin |url=http://tailhooktopics.blogspot.com/2013/02/us-navy-asw-sh-3-sea-king-variations.html |title=Tailhook Topics Drafts: US Navy ASW SH-3 Sea King Variations |publisher=Tailhooktopics.blogspot.com |date=2013-02-18 |accessdate=2013-04-12}}</ref> This was an upgrade from the AQS-10 system carried aboard the SH-3A helicopter. The AQS-13 offered an improved reeling machine and longer cable or "wet-end" than the AQS-10. The "dry-end" components of the system remained essentially the same as the AQS-10. Developed in the 1950s, these components utilized primarily vacuum tube technology. The RO-358 [[chart recorder]] provided a means of recording a permanent record of target data as well as additional means for target evaluation.<ref>{{cite web|title=Military Specification MIL-D-81622A(AS) Section 3.3.6.2|url=http://mil-spec.tpub.com/MIL-D/MIL-D-81622A/MIL-D-81622A00052.htm}}</ref><ref>{{cite web|title=Aviation Electronics Technician 1|url=http://navyaviation.tpub.com/14030/css/14030_92.htm|work=Figure 4-16}}</ref>

===AQS-13A===
[[File:AQS-13 Dipping sonar.jpg|right|thumbnail|AQS-13A/B/E hydrophone/projector]]
The AQS-13A system was an upgrade to the basic AQS-13 system incorporated into fleet systems in the late-1960s/early-1970s. The upgrade was primarily to incorporate [[built-in test equipment]] (BITE) circuitry, providing a method for testing system circuitry in the sonar set.

===AQS-13B===

The AQS-13B system was introduced to the U.S. fleet as standard equipment aboard the Sikorsky SH-3H Sea King helicopter, replacing the SH-3D in the late-1970s. The AQS-13B was a significant upgrade from the AQS-13A. The "dry-end" components were all replaced with solid state circuitry in a more compact set of replaceable assemblies. The "wet-end" components were essentially the same as the AQS-13A<ref>{{cite web|title=MILITARY SPECIFICATION DETECTING-RANGING SET, SONAR AN/AQS-13B|url=http://www.everyspec.com/MIL-SPECS/MIL-SPECS-MIL-D/MIL-D-81873_24742/}}</ref> and the RO-358 was maintained as part of the system.  The display remained a standard PPI display. The system was built with the potential to upgrade with an acoustic processor.

===AQS-13E===

The AQS-13B system was upgraded to the AQS-13E beginning in the late-1970s and early-1980s with the addition of a sonar data computer. This added the capability to digitally process acoustic sonar and [[sonobuoy]] signals while still retaining the original analog processing capability. The computer processed data was available for display on the system CRT in various formats. Tactical target data derived from the acoustic signals could be transferred electronically from the sonar data computer to the aircraft tactical computer. To enhance target detection, a longer, shaped pulse was used in conjunction with the computer processing.

===AQS-13F===

The AQS-13F system was introduced to the U.S. fleet as standard equipment aboard the [[Sikorsky SH-60 Seahawk|Sikorsky SH-60F Seahawk]] helicopter,<ref>{{cite web|title=Sikorsky Wins Navy Helicopter Contract|url=http://articles.chicagotribune.com/1985-03-12/business/8501140589_1_sh-60b-seahawk-united-states-navy-contract|publisher=Chicago Tribune}}</ref> that replaced the SH-3H as the inner-zone ASW platform aboard aircraft carriers in the late-1980s/early-1990s.<ref>{{cite web|title=S-60B (SH-60B Seahawk, SH-60F CV, HH-60H Rescue Hawk, HH-60J Jayhawk, VH-60N)|url=http://www.sikorskyarchives.com/S-60B%20(SH-60B%20Seahawk,%20SH-60F%20CV,%20HH-60H%20Rescue%20Hawk,%20HH-60J%20Jayhawk,%20VH-60N).php|publisher=Sikorsky}}</ref> Key components of the AQS-13F system had origins in the made-for-export AQS-18 sonar developed for the [[German Navy]] for use in the [[Westland Lynx|Sea Lynx]] helicopter.<ref>{{cite web|last=Moxon|first=Julian|title=CV Helo joins the US Navy|url=http://www.flightglobal.com/FlightPDFArchive/1989/1989%20-%202103.PDF|publisher=Flight International}}</ref> The AQS-13F offered improved acoustic processing, a longer transmit pulse, faster reeling machine, longer cable and increased acoustic transmit power. The processed target data could also be displayed on the aircraft multifunction displays and/or recorded via the aircraft mission tape recorder. Due to the limited funding approved by Congress, the aircraft systems were limited to "off the shelf" technology where ever possible.<ref>{{cite web|title=Case Study of the Navy CV Inner Zone Anti-Submarine Warefare Helicopter Program|url=http://gao.gov/assets/210/208676.pdf|publisher=Government Accounting Office}}</ref> This resulted in the use of the same "dry-end" as the AQS-13E, even though more modern technology was available.

==Manufacturer==

The AQS-13 series systems were manufactured by a division of [[Bendix Corporation]] in [[Sylmar, California]]. This division went through multiple ownerships and name changes over the years, including ownership by [[Allied Signal]]<ref>{{cite web|last=VARTABEDIAN|first=RALPH|title=Allied-Signal to Sell Oceanics, 2 Other Units : Completes Consolidation of Garrett and Bendix|url=http://articles.latimes.com/1988-03-31/business/fi-976_1_business-units|publisher=Los Angeles Times}}</ref> and is currently [[L-3 Communications|L-3 Communications Ocean Systems]].<ref>{{cite web|url=http://www2.l-3com.com/os/products.html |title=L3 Communications - Ocean Systems, Products - Airborne Systems/Seaborn Systems |publisher=.l-3com.com |date= |accessdate=2013-04-12}}</ref>

==Export Versions==

===AQS-502===

The export version of the AQS-13B sonar used in the [[Royal Canadian Navy]]'s [[Sikorsky CH-124 Sea King]] helicopter.<ref>{{cite web|title=AN/AQA to AN/AQS - Equipment Listing|url=http://www.designation-systems.net/usmilav/jetds/an-aq.html#_AQS|publisher=Andreas Parsch}}</ref>

===AQS-18===

The AQS-18 is the export version of the US Navy's AQS-13F. The original AQS-18 was developed for the German Navy from a drawing-board-only plan for an AQS-13D sonar for the US Navy. This version of the AQS-18 was initially deployed in the Sea Lynx helicopter in the early 1980s.

===AQS-18(V)===
AQS-18(V) Later variations were sold as the AQS-18(V) to countries around the world. Individual variations are distinct to each customer and used aboard various platforms. Earlier versions shared higher degree of commonality with the German AQS-18 and later versions more with the USN's AQS-13F. Users include the [[Hellenic Navy]] (Greece)<ref>{{cite web|title=AB.212 ASW (Greece)|url=http://www.harpoondatabases.com/Platform.aspx?DB=4&Type=Plane&ID=772|publisher=Dimitris "Sunburn" Dranidis}}</ref> and the Portuguese Navy.<ref>{{cite web|title=Super Lynx MK 95|url=http://www.the-grey-lynx.com/10_portugal/version_history_po.htm|publisher=Luis Laranjeira}}</ref>

===AQS-18(V)-3===

The AQS-18 (V)-3 is one export version of the U. S. Navy’s AQS-13F helicopter-borne dipping sonar and is used by many friendly nations, including the [[Republic of China]]<ref>{{cite web|title=Sikorsky S-70C(M) Thunderhawk|url=http://www.taiwanairpower.org/navy/s70cm.html|publisher=taiwanairpower.org}}</ref> and the [[Republic of Korea]].<ref>{{cite web|title=Super Lynx Mk 99|url=http://www.the-grey-lynx.com/12_skorea/version_history_kr.htm|publisher=Luis Laranjeira}}</ref><ref>http://www2.l-3com.com/os/pdfs/aqs18v-3_feb08.pdf</ref> It contains many of the high-performance features of the U. S. Navy version. In high reverberation-limited conditions the sonar system transmits a specially shaped pulse and its [[digital signal processor]] employs [[Fast Fourier Transform]] techniques to increase detection capabilities. A high source level provides long range search capabilities, improved [[figure of merit]], and a high-speed reeling machine to achieve maximum depth and retrieve the sonar transducer rapidly.

===AQS-18A===

The AQS-18A was developed in support of the Italian Navy and later sold to various international customers, including the Turkish Navy and the Egyptian Air Force. The system shared a common "wet end" with the U.S. Navy's AQS-13F, but had an improved "dry end" with a more modern processor, operator interface and display. These enhancements allowed for longer acoustic pulses and improved processing techniques resulting in improved tactical performance. The Turkish version was sold aboard the AB-212ASW aircraft and the Egyptian version was sold aboard the Kaman [[SH-2G Super Seasprite]] helicopter.

==Users==
*{{ALG}} - [[Algerian Navy]]
*{{USA}} - [[United States Navy]]
*{{GER}} - [[German Navy]]
*{{AUS}} - [[Australian Navy]]
*{{CAN}} - [[Royal Canadian Navy]]
*{{ROC-TW}} - [[Republic of China Navy]]
*{{KOR}} - [[Republic of Korea Navy]]
*{{GRE}} - [[Hellenic Navy]]
*{{POR}} - [[Portuguese Navy]]
*{{SPA}} - [[Spanish Navy]]
*{{BRA}} - [[Brazilian Navy]]
*{{ITA}} - [[Italian Navy]]
*{{TUR}} - [[Turkish Navy]]
*{{EGY}} - [[Egyptian Air Force]]

==References==
{{Reflist|2}}

{{DEFAULTSORT:AN AQS-13}}
[[Category:Anti-submarine warfare]]
[[Category:Military sonar equipment of the United States]]
[[Category:Military electronics of the United States]]