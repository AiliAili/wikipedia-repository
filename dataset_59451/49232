{{Orphan|date=March 2016}}

'''Barbara C. Lee, RN, PhD''' (born 1949) has directed the [[National Children's Center for Rural and Agricultural Health and Safety]], located in [[Marshfield, Wisconsin]], since its formal establishment in 1997 (with federal funding from the [[National Institute for Occupational Safety and Health]]).<ref name=":1">{{Cite news|url=|title=Saving Lives: the Marshfield-based National Farm Medicine Center and its director, Barbara Lee, work to reduce traumatic agricultural injuries and deaths|last=Mulhern|first=B|date=2008|work=|access-date=|via=}}</ref><ref name=":6">{{Cite web|title = 30 {{!}} July {{!}} 2014 {{!}} BARN OnAir & OnLine 24/7/365|url = https://brianallmerradionetwork.wordpress.com/2014/07/30/|access-date = 2016-02-18}}</ref><ref name=":7">{{Cite news|url = http://www.iatp.org/files/143_2_99619.pdf|title = Putting Farm Safety for Kids First New local efforts focus on families|last = Merrill|first = Lorraine Stuart|date = February 2016|work = |access-date = |via = }}</ref><ref name=":2">{{Cite web|title = CDC - Childhood Agricultural Injury Prevention Initiative: Progress and Proposed Future Activities July 1999 - NIOSH Workplace Safety and Health Topic|url = http://www.cdc.gov/niosh/topics/childag/childagz.html|website = www.cdc.gov|access-date = 2016-02-18}}</ref> The focus of Lee's professional career has been advocating for the safety of children who live, visit and work on farms in the U.S.<ref name=":1" />  She led the national initiative to develop an action plan for childhood agricultural injury prevention that was funded through the [[United States Congress]]<ref name=":1" /><ref name=":3">{{Cite journal|last=Hard|first=David L.|date=2012-01-01|title=Partnering Strategies for Childhood Agricultural Safety and Health|journal=Journal of agromedicine|volume=17|issue=2|pages=225–231|doi=10.1080/1059924X.2012.658341|issn=1059-924X|pmc=4678869|pmid=22490034}}</ref>

Lee was instrumental in the 2007 formation of the Agricultural Safety and Health Council of America (ASHCA), a coalition of agribusinesses and farm organizations promoting occupational safety in agriculture,<ref name=":1" /><ref>{{Cite web|url=https://issuu.com/benbauer6/docs/ashca-2015-fall-newsletter|title=ASHCA News - Summer 2015 Newsletter|website=Issuu|access-date=2016-03-21}}</ref>  and served as its Administrative Director until 2016.<ref name=":5">{{Cite web|title = Agricultural Safety and Health Council of America {{!}} National Council Works for a Safe and Healthy Agricultural Industry|url = http://www.ashca.org|website = www.ashca.org|access-date = 2016-02-22}}</ref>  Lee was first woman President (1995–1996) of the International Society for Agricultural Safety and Health, formerly known as National Institute for Farm Safety;

== Biography ==
Barbara Christine Smith was born and raised in [[Fond du Lac, WI]], one of Mary Lou and Donald Smith's seven children. Her father and brothers were general contractors for large builders and her mother's extended family were all dairy farmers.

In 1971 she received a BSN from the [[College of St. Teresa of Minnesota]];<ref name=":1" /> in 1985 she received a MSN from the [[University of Wisconsin–Eau Claire]]; in 1995 she received a PhD from the [[University of Wisconsin–Milwaukee]], School of Nursing.

From 1972 to 1985 she working in nursing, and was then an instructor at St. Joseph's Hospital School of Nursing, Marshfield, WI. In 1987 she became Assistant Director, National Farm Medicine Center, Marshfield Medical Research Foundation, and then its director from 2000 to 2012. Since then she has been Director of the National Children's Center for Rural and Agricultural Health and Safety,<ref name=":1" /><ref name=":6">{{Cite web|title = 30 {{!}} July {{!}} 2014 {{!}} BARN OnAir & OnLine 24/7/365|url = https://brianallmerradionetwork.wordpress.com/2014/07/30/|access-date = 2016-02-18}}</ref><ref name=":7">{{Cite news|url = http://www.iatp.org/files/143_2_99619.pdf|title = Putting Farm Safety for Kids First New local efforts focus on families|last = Merrill|first = Lorraine Stuart|date = February 2016|work = |access-date = |via = }}</ref><ref name=":2">{{Cite web|title = CDC - Childhood Agricultural Injury Prevention Initiative: Progress and Proposed Future Activities July 1999 - NIOSH Workplace Safety and Health Topic|url = http://www.cdc.gov/niosh/topics/childag/childagz.html|website = www.cdc.gov|access-date = 2016-02-18}}</ref> and Senior Research Scientist, National Farm Medicine Center.<ref>{{Cite web|title = Marshfield Clinic Research Foundation|url = http://www.marshfieldresearch.org/profiles/1423|website = www.marshfieldresearch.org|access-date = 2016-02-18}}</ref>

== Honors and awards ==
* 2016	Named as one of "50 Distinguished Alumni" of [[University of Wisconsin-Milwaukee College of Nursing]]
* 2013	Inducted into the Royal Academy of Agriculture as a Fellow, Stockholm, Sweden
* 2011	Awarded Honorary Doctorate from [[Swedish University of Agricultural Sciences]] (SLU), Uppsala, Sweden, one of nine international recipients.

== Advisory Roles and External Committees ==
In the past decade, Lee has served on:  the National Steering Committee of the National Coordinated Child Safety Initiative (NCCSI) (2015–present); the National Advisory Committee of the National Tractor Safety Coalition;  the National Advisory Board (2014–present) of  National Safety in Agriculture for Youth . She was also  Co-Chair of National Occupational Research Agenda) Council for Agriculture, Forestry and Fishing Sector. National Institute for Occupational Safety and Health (2007-2010).

== References ==

{{Reflist}}

{{DEFAULTSORT:Lee, Barbara C.}}
[[Category:Articles created via the Article Wizard]]
[[Category:American agriculturalists]]
[[Category:Living people]]
[[Category:1949 births]]
[[Category:College of Saint Teresa alumni]]
[[Category:University of Wisconsin&ndash;Milwaukee alumni]]
[[Category:University of Wisconsin&ndash;Eau Claire alumni]]
[[Category:People from Fond du Lac, Wisconsin]]