<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Mono-Z CH 100
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft|Homebuilt]] light aircraft
 | national origin=[[Canada]]
 | manufacturer=[[Zenair]]
 | designer=Chris Heintz
 | first flight=8 May 1975
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= [[Zenair CH 200]]
 | variants with their own articles=
}}
|}
The '''Zenair Mono-Z CH 100''' is a single-seat, single-engined [[Canada|Canadian]] light aircraft of the 1970s.  It is a smaller version of the [[Zenair CH 200]] with a less powerful engine, which was sold as a homebuilt aircraft by [[Zenair]].

==Development and design==

After emigrating to Canada and setting up [[Zenair]] to sell plans and kits for [[Homebuilt aircraft|amateur construction]] of his [[Zenair CH 200|Zenith]] two-seat-light aircraft, the German aircraft designer Chris Heintz started design of a smaller, single-seat development of the Zenith, the Mono-Zenith.<ref name="Janes 76 p458">Taylor 1976, p.458.</ref> The Mono-Z CH 100 is similar to the Zenith that preceded it, a low-winged [[Cantilever#In aircraft|cantilever]] [[monoplane]] of all metal construction. The aircraft features a large cockpit for taller pilots, with a pilot and baggage combined weight allowance of {{convert|240|lb|kg|0|abbr=on}} and removable wings for storage and towing the aircraft behind a car. The factory claimed a build time of 600 hours. It is designed to be powered by engines from 45 to 100&nbsp;hp (33.5 to 74.5&nbsp;kW).<ref name="Janes 82 p493-4">Taylor 1982, pp. 493–494.</ref><ref name="ZenPam">[[Zenair]], ''Zenair pamphlet'', circa 1986.</ref>

The first CH 100 made its maiden flight on 8 May 1975, powered by a 55&nbsp;hp (41&nbsp;kW) [[Volkswagen air-cooled engine]] of 1600 cc, with 110 sets of plans and kits sold by 1982.<ref name="Janes 82 p494"/>  Zenair continued to produce kits until 1988.<ref>[http://www.zenair-deutschland.de/Downloads/pdf/601-Design-History.pdf Chris Heintz:Light Aircraft Design History] {{webarchive |url=https://web.archive.org/web/20110719115519/http://www.zenair-deutschland.de/Downloads/pdf/601-Design-History.pdf |date=July 19, 2011 }}. ''zenair-deutschland.de''. Retrieved 28 February 2010.</ref>

==Operational history==
A total of three CH 100s were registered in Canada since 1987 and none are registered in 2010.<ref name="TCCARS">{{cite web|url = http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/historysearch.asp|title = Canadian Civil Aircraft Register Historical Information |accessdate = 2010-02-28|last = [[Transport Canada]]|authorlink = |date=February 2010}}</ref>

==Specifications (65 hp engine)==
{{Aircraft specs
|ref=Jane's All The World's Aircraft 1988-89<ref name="Janes 88 p514">Taylor 1988, p.514.</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=19
|length in=6
|length note=
|span m=
|span ft=22
|span in=0
|span note=
|height m=
|height ft=6
|height in=6
|height note=
|wing area sqm=
|wing area sqft=91.5
|wing area note=
|aspect ratio=5.28:1<ref name="Janes 82 p494">Taylor 1982, p.494.</ref>
|airfoil=GAW-1 (modified)
|empty weight kg=
|empty weight lb=630
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=960
|max takeoff weight note=
|fuel capacity=14.4 US Gallons (55 L)
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Volkswagen air-cooled engine|Volkswagen]]
|eng1 type=
|eng1 kw=<!-- prop engines -->
|eng1 hp=65<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=125
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=110
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=48<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=400
|range nmi=
|range note=with maximum fuel
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=12000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=820
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=*[[Zenair CH 200]]
*[[Zenair CH 150]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1976-77''. London:Jane's Yearbooks, 1976. ISBN 0-354-00538-3.
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1982-83''. London:Jane's Yearbooks, 1982. ISBN 0-7106-0748-2.
*[[John W. R. Taylor|Taylor, John W. R.]] ''Jane's All The World's Aircraft 1988-89''. Coulsdon, UK:Jane's Defence Data, 1988. ISBN 0-7106-0867-5.
<!-- insert the reference sources here -->
{{refend}}

==External links==
*[http://picasaweb.google.com/lh/photo/9BDe5wkmQgcqpRMP986kbQ Photo of a CH 100]
{{Zenair}}

[[Category:Canadian sport aircraft 1970–1979]]
[[Category:Homebuilt aircraft]]
[[Category:Zenair aircraft|CH 100]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]