{{Infobox engineer
|image                = 
|image_size           = 
|caption              = 
|name                 = Granville Eastwood Bradshaw
|nationality          = [[United Kingdom|British]]
|birth_date           = 1887
|birth_place          = Preston, Lancashire, England
|death_date           = 1969
|death_place          = Hitchin, Hertfordshire, England
|education            =
|spouse               = 
|parents              = William & Annie Bradshaw
|children             =
|discipline           = 
|institutions         = [[Royal Aeronautical Society]]
|practice_name        = [[ABC Motors]] 
|significant_projects = Motorcycle and Aero-engines
|significant_design   = 
|significant_advance  =
|significant_awards   = [[Order of the British Empire|Officer of the Order of the British Empire]]
}}

'''Granville Eastwood Bradshaw''' [[Order of the British Empire|OBE]], [[Royal Aeronautical Society|AFRAeS]] (1887–1969) was an English engineer and inventor who designed motorcycle and aero-engines.

==History==
Bradshaw was born in [[Preston, Lancashire]] in 1887 as the son of William and Annie Bradshaw. His father was a jeweler and optician.

Bradshaw's early work was involved with the early pioneers of flight, which led him to become an expert on stressing. He designed the [[Star Monoplane]] including the engine for [[Star Aircraft]] when he was 19, which he later flew.<ref>{{cite web|url=http://www.historywebsite.co.uk/Museum/Transport/planes/Star.htm|title=Star Aircraft |publisher=Wolverhampton History and Heritage |date=1921-02-14 |accessdate=2013-07-19}}</ref> He then started to work on aero-engines and was the co-founder of the All-British Engine Company (later [[ABC Motors]] then Walton Motors).

The ABC radial aero-engines designed and built during the First World War were extremely advanced and the government placed large orders for the [[ABC Dragonfly|Dragonfly]].  A number of aircraft were designed to use the Dragonfly, but the engines were plagued by problems and failed to live up to the promises. The design was taken over by the [[Royal Aircraft Establishment]] at [[Farnborough Airfield|Farnborough]] to try to resolve the issues, but with the end of the war, it was abandoned.

[[Image:British ABC WASP.jpg|right|thumb|A Wasp engine]]

He was appointed an [[Order of the British Empire|Officer of the Order of the British Empire]] for his war work in 1918.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%200641.html |title=Flight 13 June 1918 Birthday Honours for War Workers |publisher=Flight Global |date= |accessdate=2009-03-28}}</ref>

At the end of 1918 it was announced<ref>ABC Motors Full Page Advert, Motor Cycle, 26th December 1918</ref> that ABC Motors Ltd had transferred its motor cycle manufacturing and selling rights to [[Sopwith Aviation Company|Sopwith Aviation Co Ltd]], with Granville Bradshaw of ABC Motors Ltd concentrating on design. This allowed him to sell his designs to other companies. He designed a number of engines for [[Phelon & Moore|Panther motorcycles]]. He also designed motorcycle engines where the cylinder barrels were oil cooled, and these were made under licence by J Walmsley & Co (Preston) Ltd.<ref name="Walmsley">"Bradshaw Oil-Cooled Engines on the Market", The Motor Cycle, November 10th, 1921</ref> The flat-twin 500cc version of this engine was utilised from 1921 on the [[Zenith Motorcycles|Zenith-Bradshaw]] motorcycle. A single cylinder 348cc version was optional for [[OK-Supreme]], Sheffield-Henderson, [[Dot Cycle and Motor Manufacturing Company|Dot]], Orbit, and Coventry-Mascot in 1922;<ref>"Olympia - A Review of 1923 models", The Motor Cycle, November 30th, 1922</ref> and a 1100cc V-twin version of this oil-cooled engine was adopted for the [[Belsize Motors|Belsize]] light car.<ref name="Walmsley"/>

His biggest seller was selling patents for gambling machines, although he lost all the money he made in further business deals. He later concentrated on [[Swing-piston engine|toroidal internal-combustion engines]]. Bradshaw produced a long list of inventions and designs, although very few achieved commercial success.

A biography ''Granville Bradshaw: a flawed genius?'' by Barry Jones was published in 2008.

==Family==
In 1911 he married Violet Elsie Partridge in Wolverhampton. Violet petitioned for divorce in 1926.<ref>{{cite web|url=http://www.nationalarchives.gov.uk/catalogue/displaycataloguedetails.asp?CATLN=7&CATID=-3131299&FullDetails=True&j=1&Gsm=2008-08-08 |title=National Archives Petition for Divorce |publisher=[[The National Archives (United Kingdom)|The National Archives]] |accessdate=2009-03-28}}</ref> Bradshaw married again in 1927 to Muriel Mathieson in Kensington, London. He died in 1969 at Hitchin in Hertfordshire.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1969/1969%20-%200834.html |title=Flight International |publisher=Flight Global |date=1969-05-08 |accessdate=2009-03-28}}</ref>

==See also==
* [[ABC motorcycles]]
* [[ABC Motors]]

==Notes==
{{reflist}}

==External links==
* [http://www.realclassic.co.uk/books/granvillebradshaw.html Granville Bradshaw]

{{DEFAULTSORT:Bradshaw, Granville}}
[[Category:1887 births]]
[[Category:1969 deaths]]
[[Category:English aerospace engineers]]
[[Category:English aviators]]
[[Category:English engineers]]
[[Category:English inventors]]
[[Category:Officers of the Order of the British Empire]]
[[Category:People from Preston, Lancashire]]