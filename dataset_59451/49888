{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Sundering Flood
| title_orig    =
| translator    =
| image         = <!--prefer 1st edition-->
| caption =
| author        = [[William Morris]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = [[Fantasy novel]]
| publisher     = [[Kelmscott Press]]
| release_date  = 1897
| english_release_date =
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 508 pp
| isbn          = 978-1514832653 
| preceded_by   =
| followed_by   =
}}
[[File:Sundering Flood.jpg|thumb|Frontispiece map from the first edition]]
'''''The Sundering Flood''''' is a [[fantasy]] novel by [[William Morris]], perhaps the first modern fantasy writer to unite an imaginary world with the element of the supernatural, and thus the precursor of much of present-day fantasy literature.<ref>{{cite book|author=[[L. Sprague de Camp|De Camp, L. Sprague]]| work=[[Literary Swordsmen and Sorcerers]]: The Makers of Heroic Fantasy|page= 40 |isbn= 0-87054-076-9}}</ref>  
''The Sundering Flood'' was Morris' last work of fiction, completed only in rough draft, with the ending dictated from his deathbed. It was edited posthumously by his daughter [[May Morris|May]] into finished form for publication and published in 1897.

Morris considered his fantasies a revival of the medieval tradition of [[Chivalry|chivalrous]] romances; consequently, they tend to have sprawling plots of strung-together adventures.{{citation needed|date=September 2013}} His use of archaic language has been seen as difficult by some modern readers.{{citation needed|date=September 2013}}

==Plot summary==
'''Osberne Wulfgrimsson''' and '''Elfhild''' are lovers who live on opposite sides of the '''Sundering Flood''', an immense river. When Elfhild disappears during an invasion by the '''Red Skinners''', the heartbroken Osberne takes up his magical sword '''Boardcleaver''' and joins the army of '''Sir Godrick of Longshaw''', in whose service he helps dethrone the tyrannical king and [[plutocracy]] of merchants ruling the city at the mouth of the river. Afterwards he locates Elfhild, who had fled with a relative, a wise woman skilled in the magical arts, and taken refuge in the '''Wood Masterless'''. Elfhild tells Osberne of their adventures ''en route'' to safety. Afterwards they return together to '''Wethrmel''', Osberne's home, and all ends happily.

==Publication history==
It was first published  posthumously in hardcover by Morris' [[Kelmscott Press]] in 1897.<ref>[[Eugene D. LeMire|LeMire, Eugene D.]], ''A Bibliography of William Morris'', New Castle,  Del., Oak Knoll Press, 2006, pp. 218-223.</ref> Its importance in the history of fantasy literature was recognized by its republication by [[Ballantine Books]] as the fifty-seventh volume of the celebrated [[Ballantine Adult Fantasy series]] in May, 1973. The Ballantine edition includes an introduction by [[Lin Carter]].

===Copyright===
The [[copyright]] for this story has expired and it is now in the [[public domain]] everywhere in the world.

==References==
{{reflist}}

==External links==
*[http://www.gutenberg.org/etext/25547 ''The Sundering Flood''] in ''The Collected Works of William Morris Volume XXI'', Longmans Green & Co, 1914, at Project Gutenberg

{{William Morris}}

{{DEFAULTSORT:Sundering Flood, The}}
[[Category:1897 novels]]
[[Category:Novels by William Morris]]
[[Category:Fantasy novels]]
[[Category:Fictional rivers]]
[[Category:19th-century British novels]]