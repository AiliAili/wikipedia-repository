{{italic title}}
{{Infobox Journal
| title        = Journal of Chemical Theory and Computation
| cover        = [[File:JCTC logo.png|250px]]  [[File:Jctc cover.jpg|150 px]]
| editor       = William L. Jorgensen, Gustavo E. Scuseria
| discipline   = [[Chemistry]]
| abbreviation = J. Chem. Theory Comput.
| publisher    = [[American Chemical Society]]
| country      = {{Flag|USA}}
| frequency    = Monthly
| history      = 2005- present
| impact       = 5.301
| impact-year  = 2015
| website      = http://pubs.acs.org/journal/jctcce
| RSS          = http://pubs.acs.org/action/showFeed?ui=0&mi=qjmolc&ai=53h&jc=jctcce&type=etoc&feed=rss
| CODEN        = jctcce
| ISSN         = 1549-9618
| eISSN        = 1549-9626
}}
The '''''Journal of Chemical Theory and Computation''''' is a [[Peer review|peer-reviewed]] [[scientific journal]], established in 2005 by the [[American Chemical Society]].<ref>{{cite web|url=http://pubs.acs.org |title=ACS Publications : Homepage |publisher=Pubs.acs.org |accessdate=25 February 2015}}</ref> It is indexed in [[Chemical Abstracts Service]] (CAS), [[Scopus]], [[British Library]], and [[Web of Science]]. The current editors are William L. Jorgensen<ref>{{cite web|url=http://pubs.acs.org/page/jctcce/profile.html |title=William L. Jorgensen |publisher=Pubs.acs.org |accessdate=25 February 2015}}</ref> and Gustavo E. Scuseria.<ref>{{cite web|url=http://pubs.acs.org/page/jctcce/profile1.html |title=Gustavo E. Scuseria |publisher=Pubs.acs.org |accessdate=25 February 2015}}</ref> Currently as of the year 2015, JCTC has 11 volumes.<ref>{{cite web|title=List of Issues|url=http://pubs.acs.org/loi/jctcce|publisher=ACS|accessdate=1 December 2015}}</ref>

==Scope==
Much of the JCTC reports on new theories, methods and applications of quantum chemical knowledge, such as electronic structure, molecular mechanics and statistical mechanics. Research of computational applications such as [[Ab initio quantum chemistry methods|ab initio quantum mechanics]], [[Monte Carlo analysis|Monte Carlo simulations]] and [[Implicit solvation|solvation model]] are discussed among others. It is stated that "the Journal favors submissions that include advances in theory or methodology with applications to compelling problems".<ref name=scope>{{cite web|title=Journal Scope|url=http://pubs.acs.org/page/jctcce/about.html|website=Journal of Chemical Theory and Computation|accessdate=24 November 2015}}</ref>

==History==
The first issue of JCTC was published in 2005 as a bimonthly [[journal]] with 133 articles published in the first volume. In 2008, JCTC increased their output to become a monthly publication, producing 12 issues per year. The journal came about when Jorgensen noticed that although theory and computation could be found in many journals, the field did not have a dedicated journal. In year 2008, JCTC took professor Ursula Rothlisberger from the [[ETH Zurich|Swiss Federal Institute of Technology ]] as their associate editor. In the year 2009, the editorial team was further expanded with the addition of Professor Gustavo Scuseria from [[Rice University]].<ref>{{cite web|title=Journal of Chemical Theory and Computation|url=http://archive.sciencewatch.com/inter/jou/2009/09junJofCTandC/|website=Science Watch|publisher=Thomson Reuters|accessdate=1 December 2015}}</ref>

==Statistics== 
JCTC is ranked number 4 highest in the list of "Physical and Theoretical Chemistry" journals [[SCImago Journal Rank]]. JCTC has a weighted rank indicator (SJR) of 2,481 as of the year 2014. To give perspective, the popular multidisciplinary journal [[Nature (journal)]] has an SJR factor of 17,313. The journal had an average of 65.73 references per document in 2014.<ref>{{cite web|title=Journal Rankings|url=http://scimagojr.com/journalrank.php?area=1600&category=1606&country=all&year=2014&order=sjr&min=0&min_type=cd|publisher=SciMago Journal & Country Rank|accessdate=1 December 2015}}</ref> In 2015, the journal had a total of 20,778 citations and an impact factor of 5.301.<ref name=scope />

==See also==
{{Div col|1}}
* [[Physical Chemistry Chemical Physics]]
* [[Journal of Chemical Physics]]
* [[Computational and Theoretical Chemistry]] (formerly known as THEOCHEM)
* [[Journal of Computational Chemistry]]
* [[Annual Review of Physical Chemistry]]
* [[International Journal of Quantum Chemistry]]
{{Div col end}}

==References==
{{Reflist}}

[[Category:American Chemical Society academic journals]]
[[Category:Chemistry journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2005]]
[[Category:Computational chemistry]]
[[Category:Quantum chemistry]]
[[Category:Theoretical chemistry]]