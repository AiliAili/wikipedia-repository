{{good article}}
{{Infobox football match
| title                   = 1988 Football League Cup Final
| image                   = [[File:1988 Football League Cup Final programme.png|frameless|upright]]
| caption                 = The [[Football programme|match programme]] cover
| event                   = [[1987–88 Football League Cup]]
| team1                   = [[Luton Town F.C|Luton Town]]
| team1association        =
| team1score              = 3
| team2                   = [[Arsenal F.C.|Arsenal]]
| team2association        =
| team2score              = 2
| details                 =
| date                    = 24 April 1988<ref>{{cite news |first=Sandy |last=Smithies |title=Watching brief |page=39 |newspaper=The Guardian |location=London |date=23 April 1988}}</ref>
| stadium                 = [[Wembley Stadium (1923)|Wembley Stadium]]
| city                    = London
| man_of_the_match1a      = [[Andy Dibble]] (Luton Town)<ref>{{cite news |first=Nigel |last=Clarke |title=King Andy |page=32 |newspaper=Daily Mirror |location=London |date=25 April 1988}}</ref>
| man_of_the_match1atitle =
| referee                 = [[Joe Worrall]] ([[Cheshire County Football Association|Cheshire]])
| attendance              = 95,732
| weather                 = Sunny<ref>{{cite news |title=Weather |page=32 |newspaper=The Observer |location=London |date=24 April 1988}}</ref>
| previous                = [[1987 Football League Cup Final|1987]]
| next                    = [[1989 Football League Cup Final|1989]]
}}
The '''1988 Football League Cup Final''' (also known as the '''Littlewoods Challenge Cup Final''' for sponsorship reasons) was an [[association football]] match between [[Luton Town F.C.|Luton Town]] and [[Arsenal F.C.|Arsenal]] on 24 April 1988 at [[Wembley Stadium (1923)|Wembley Stadium]], London. It was the final match of the [[1987–88 Football League Cup|1987–88 staging]] of the [[Football League Cup]]. Luton were making their first League Cup Final appearance, while the competition holders Arsenal were appearing in their fourth final.

Each club needed to get past five rounds to reach the showpiece event at Wembley. Both clubs made comfortable progress; Luton scored 14 goals and conceded three, Arsenal on the other hand conceded two fewer. Luton for the final were without [[Darron McDonough]] who injured himself in training, but [[David Preece (footballer, born 1963)|David Preece]] and [[Ricky Hill]] both returned to the side after lengthy periods of treatment. Goalkeeper [[Les Sealey]] was not fit in time to play, so [[Andy Dibble]] deputised in goal for only his sixth appearance of the season. [[Gus Caesar]] replaced [[David O'Leary]] in Arsenal's starting eleven, as the Irishman was ruled out for the final having damaged his achilles weeks prior.

Arsenal entered the match as favourites, but went behind early when [[Brian Stein]] scored. Luton's disciplined approach, coupled with a strong performance by Dibble, contained Arsenal to few chances throughout the game. The holders equalised and took the lead in quick succession through [[Martin Hayes (footballer)|Martin Hayes]] and [[Alan Smith (footballer, born 1962)|Alan Smith]], but failed to seal the win when [[Nigel Winterburn]] missed a penalty. A revitalised Luton staged a late comeback; Caesar's failed clearance led to [[Danny Wilson (footballer, born 1960)|Danny Wilson]] equalising, and in the 90th minute, Brian Stein scored the winner after poor defending from Arsenal.

The 1988 final was Luton's first major cup victory; their manager [[Ray Harford]] later described it as the greatest win in his time at the club. Luton did not qualify for European football the following season, despite winning the League Cup as [[UEFA]] chose not to relax its [[Heysel Stadium disaster#English club ban|ban on English teams]]. Arsenal manager [[George Graham (footballer)|George Graham]] in the meantime strengthened his squad as a result of his team's poor defensive display. Caesar found his playing time limited in subsequent seasons, as the manager brought in [[Steve Bould]]. The 1988 final regarded as one of the best in the competition's history and most exciting at Wembley, and has been likened to the [[1979 FA Cup Final|"five-minute" FA Cup Final]] of 1979.<ref name=times>{{cite news |title=Stein's final flourish gives Luton the Cup |first=Stuart |last=Jones |page=38 |newspaper=The Times |date=25 April 1988}}</ref>

==Route to the final==
{{Main article|1987–88 Football League Cup}}

===Arsenal===
{| class="wikitable plainrowheaders" style="text-align:center;margin-left:1em;float:right"
<!-- |+ '''Arsenal''' -->
!width="25" | Round
!width="150" | Opposition
!width="50" | Score
|-
!scope=row style="text-align:center" rowspan="2" | 2nd
| [[Doncaster Rovers F.C.|Doncaster Rovers]] ([[Belle Vue (Doncaster)|a]])
| 3–0
|-
| Doncaster Rovers ([[Arsenal Stadium|h]])
| 1–0
|-
!scope=row style="text-align:center"| 3rd
| [[Bournemouth A.F.C.|Bournemouth]] (h)
| 3–0
|-
!scope=row style="text-align:center"| 4th
| [[Stoke City F.C.|Stoke City]] (h)
| 4–0
|-
!scope=row style="text-align:center"| 5th
| [[Sheffield Wednesday F.C.|Sheffield Wednesday]] ([[Hillsborough Stadium|a]])
| 1–0
|-
!scope=row style="text-align:center" rowspan="2" | Semi-final
| [[Everton F.C.|Everton]] ([[Goodison Park|a]])
| 1–0
|-
| Everton (h)
| 3–1
|-
| colspan="3" style="background-color:white;"| <small>'''Key:''' (h) = Home venue; (a) = Away venue.</small>
|}

Arsenal entered the competition in the second round, as one of the 22 teams from the Football League First Division. They were drawn against [[Doncaster Rovers F.C.|Doncaster Rovers]]; the first leg was staged at [[Belle Vue (Doncaster)|Belle Vue]] on 23 September 1987.<ref>{{cite news |title=Fixtures: Soccer |page=36 |newspaper=The Guardian |location=London |date=23 September 1987}}</ref> Arsenal eased to a 3–0 win, with goals from [[Perry Groves]], [[Alan Smith (footballer, born 1962)|Alan Smith]] and [[Steve Williams (footballer, born 1958)|Steve Williams]].<ref>{{cite news |title=Groves leads the charge |first=Michael |last=Henderson |page=28 |newspaper=The Guardian |location=London |date=24 September 1987}}</ref> A fortnight later, midfielder [[David Rocastle]] scored the only goal in the second leg to give Arsenal a 4–0 [[Two-legged tie|aggregate]] scoreline win.<ref>{{cite news |title=Results |page=32 |newspaper=The Guardian |location=London |date=7 October 1987}}</ref> [[A.F.C. Bournemouth|Bournemouth]] were Arsenal's opponents in the third round. The match was played at [[Arsenal F.C.|Highbury]] on 27 October 1987. Arsenal needed 33 minutes to open the scoring, when [[Michael Thomas (footballer, born 1967)|Michael Thomas]] converted a [[Penalty kick (association football)|penalty kick]]. Smith extended their lead and in the second half, [[Kevin Richardson (footballer)|Kevin Richardson]] scored Arsenal's third, profiting from a mix-up between the Bournemouth defenders. The win was Arsenal's tenth in succession at Highbury, setting a new club record in the process.<ref>{{cite news |title=Arsenal claim club record as the resistance breaks down |first=Clive |last=White |page=47 |newspaper=The Times |date=28 October 1987}}</ref>

In the fourth round Arsenal faced [[Stoke City F.C.|Stoke City]] of the Second Division at home. They made light work of the opposition, winning by three goals and once more set a unique record: [[David O'Leary]]'s strike meant all 10 regular outfield players each had scored for Arsenal during the season.<ref name=stoke>{{cite news |title=Arsenal's aura of invincibility is becoming a luxury |first=Stuart |last=Jones |page=47 |newspaper=The Times |date=18 November 1987}}</ref> In his match report for ''[[The Times]]'', Stuart Jones assessed: "Having experienced northing but victory since the end of August, Arsenal are walking around with almost too much belief," and felt complacency was the only issue preventing the club from reaching the quarter-finals.<ref name=stoke/>

Arsenal profited from a mistake [[Sheffield Wednesday F.C.|Sheffield Wednesday]] goalkeeper [[Martin Hodge]] to progress to the semi-finals, where they were paired up against [[Everton F.C.|Everton]].<ref>{{cite news |title=Hand-out by Hodge to Arsenal |first=Martin |last=Searby |page=42 |newspaper=The Times |date=21 January 1988}}</ref> The first leg was staged at [[Goodison Park]] and saw the Arsenal players, rejuvenated by a five-day break in Marbella, come away with a 1–0 win.<ref name=ever>{{cite news |title=Last word for the voices of dissent |first=Stuart |last=Jones |page=36 |newspaper=The Times |date=8 February 1988}}</ref> Groves scored the only goal of the match when he managed to get a shot in from [[Kenny Sansom]]'s [[Direct free kick|free kick]].<ref name=ever/> Arsenal secured a place in the final with a 3–1 victory in the second leg.<ref>{{cite news |title=Everton's season reduced to one of trivial pursuit |first=Stuart |last=Jones |page=44 |newspaper=The Times |date=25 February 1988}}</ref>

===Luton Town===
{| class="wikitable plainrowheaders" style="text-align:center;margin-left:1em;float:right"
<!-- |+ '''Luton''' -->
!width="25" | Round
!width="150" | Opposition
!width="50" | Score
|-
!scope=row style="text-align:center" rowspan="2" | 2nd
| [[Wigan Athletic F.C.|Wigan Athletic]] ([[Springfield Park (Wigan)|a]])
| 1–0
|-
| Wigan Athletic ([[Kenilworth Road|h]])
| 4–2
|-
!scope=row style="text-align:center"| 3rd
| [[Coventry City F.C.|Coventry City]] ([[Filbert Street|n]]){{dagger|alt=neutral}}
| 3–1
|-
!scope=row style="text-align:center"| 4th
| [[Ipswich Town F.C.|Ipswich Town]] ([[Portman Road|a]])
| 1–0
|-
!scope=row style="text-align:center"| 5th
| [[Bradford City A.F.C.|Bradford City]] (h)
| 2–0
|-
!scope=row style="text-align:center" rowspan="2" | Semi-final
| [[Oxford United F.C.|Oxford United]] ([[Manor Ground (Oxford)|a]])
| 1–1
|-
| Oxford United (h)
| 2–0
|-
| colspan="3" style="background-color:white;"| <small>'''Key:''' (h) = Home venue; (a) = Away venue. {{dagger|alt=neutral}} Luton drawn as home side, but match took place at a neutral venue.</small>
|}
Luton Town, also of the First Division, began the competition in the second round and played [[Wigan Athletic F.C.|Wigan Athletic]] over two legs. The first, staged at Wigan's [[Springfield Park (Wigan)|Springfield Park]] ended as a 1–0 win for the visitors, courtesy of [[Mickey Weir]]'s goal.<ref>{{cite news |title=Last night's soccer |page=36 |newspaper=The Guardian |location=London |date=23 September 1987}}</ref> In the return leg, [[Mick Harford]] scored a hat-trick to give Luton a comfortable 4–2 victory (5–2 on aggregate).<ref>{{cite news |title=Lyall made to bite on bullet |first=Mike |last=Rowbottom |page=32 |newspaper=The Guardian |location=London |date=7 October 1987}}</ref> [[Coventry City F.C.|Coventry City]] were Luton's opponents in the third round. The tie was played at [[Filbert Street]] – a neutral venue – given Luton had banned away supporters from its home, [[Kenilworth Road]] as a consequence of the [[1985 Kenilworth Road riot|rioting]] which marred English football in 1985.<ref name=cove>{{cite news |title=Harford's double sinks Coventry |first=Chris |last=Moore|page=47 |newspaper=The Times |date=28 October 1987}}</ref> At Filbert Street, Luton started strongly and took a 30th-minute lead when Weir scored. Further goals from Harford saw them progress into the next round.<ref name=cove/>

In the last 16 of the competition, Luton faced [[Ipswich Town F.C.|Ipswich Town]] of the Second Division. Without several senior players because of [[Cup-tied|ineligibility]] and injury, Luton scored early through [[Brian Stein]]'s strike, and then produced a solid defensive performance, reliant on goalkeeper [[Les Sealey]] to earn a place in the quarter-finals.<ref name=seal>{{cite news |title=Luton win a tribute to Sealey |first=Clive |last=White |page=47 |newspaper=The Times |date=18 November 1987}}</ref> The result marked Ipswich's first defeat of the season, having earlier recorded 10 wins and two draws at [[Portman Road]].<ref name=seal/> Luton hosted [[Bradford City F.C.|Bradford City]] in the fifth round of the competition on 19 January 1988. A mistake by Bradford goalkeeper [[Paul Tomlinson]] handed Luton the lead four minutes before the hour, when he gave away a free kick for handling the ball in his area.<ref name=sem>{{cite news |title=Goalkeeping error sets Luton on way to semi-final place |first=Dennis |last=Signy |page=43 |newspaper=The Times |date=20 January 1988}}</ref> A shot from [[Danny Wilson (footballer, born 1960)|Danny Wilson]] in the 65th minute resulted in Luton's second goal; though his effort was saved by Tomlinson, the goalkeeper could not fend off the advancing Harford's header.<ref name=sem/>

Luton faced [[Oxford United F.C.|Oxford United]] in the semi-final which was played over two-legs. The first leg was at Oxford's home ground [[Manor Ground (Oxford)|Manor Ground]], where Luton had beaten their opponents 5–2 earlier in the league season.<ref>{{cite news |title=One out of two for Saunders |first=Stuart |last=Jones |page=46 |newspaper=The Times |date=11 February 1988}}</ref> On a wet, muddy surface, both clubs struggled to control the tempo, though Luton played the more incisive football and looked threatening in their opponent's half. Harford's 19th goal of the season gave Luton the breakthrough in the tie, but Oxford equalised from the penalty spot. The home side were awarded a second penalty when [[Dean Saunders]] was brought down in Luton's area, but Sealey saved his attempt. Having received consent from the police to stage the second leg at Kenilworth Road, a near-capacity crowd saw Luton win 2–0.<ref>{{cite news |title=Luton grasp the chance of a Wembley stage |first=Stuart |last=Jones |page=32 |newspaper=The Times |date=29 February 1988}}</ref>

==Pre-match==
Arsenal, the match favourites and holders of the Football League Cup,<ref>{{cite news |title=Arsenal ready to take a one-way glory road |first=Steve |last=Curry |page=43 |newspaper=Daily Express |location=London |date=23 April 1988}}</ref> were making their fourth final appearance in the competition.<ref name=arse>{{cite news |title= Arsenal's League Cup Finals – A history |url=http://www.arsenal.com/news/news-archive/arsenal-s-league-cup-finals-a-history |publisher=Arsenal F.C |date=26 January 2011 |accessdate=5 June 2016}}</ref> They had won the League Cup once before, in [[1987 Football League Cup Final|1987]], and lost two consecutive finals in [[1968 Football League Cup Final|1968]] and [[1969 Football League Cup Final|1969]].<ref name=arse/> By contrast Luton Town were making their first League Cup final appearance. The club enjoyed relative success in the cups during the [[1987–88 Luton Town F.C. season|1987–88]] season, reaching the [[1987–88 FA Cup#Semi-Finals|last four of the FA Cup]] and [[1988 Full Members Cup Final|final]] of the [[Full Members Cup]].<ref>{{cite news |title=Caught in Time: Luton Town win the League Cup, 1988 |first=Greg |last=Struthers |url=http://www.thesundaytimes.co.uk/sto/sport/article89860.ece |newspaper=The Sunday Times |date=24 April 2005 |accessdate=19 July 2016}}</ref> Arsenal and Luton had only played each other once in the League Cup; [[George Graham (footballer)|George Graham]] the present-day manager of Arsenal, scored the decider in a third-round tie on 6 October 1970.<ref name="statf">{{cite web |title=Arsenal's head-to-head comparison with Luton Town |url=http://www.statto.com/football/teams/arsenal/luton-town/head-to-head |publisher=Statto Organisation |accessdate=4 June 2016}}</ref><ref>{{cite news |title=Luton grasp the chance of a Wembley stage |first=Geoffrey |last=Green |page=13 |newspaper=The Times |date=7 October 1970}}</ref> Luton's last victory against Arsenal came in March 1986, a 3–0 win in an FA Cup fifth round, second replay.<ref name="statf"/>

Luton manager [[Ray Harford]] had doubts over his team selection for the final. Though buoyed by the return of [[David Preece (footballer, born 1963)|David Preece]] and [[Ricky Hill]] to full training, he was without [[Darron McDonough]] who injured his knee ligaments in training.<ref>{{cite news |title=Happy return for King Billy |first=Michael |last=Ralph |page=20 |newspaper=The Observer |location=London |date=24 April 1988}}</ref> Harford said the instability presented a selection dilemma, telling reporters: "I think I have got to break somebody's heart by telling him he is not playing."<ref name="prem">{{cite news |title=Harford the heart-breaker |first=Dennis |last=Signy |page=47 |newspaper=The Times |date=21 April 1988}}</ref> He admitted Luton's poor run of form since losing the Full Members Cup was a reason why he would make changes for the Wembley final, and noted his side's difficulty on grass away from home.<ref name="prem"/> Harford identified [[David Rocastle]] as Arsenal's biggest threat and felt they had few weaknesses in the side other than the ability to finish chances. He believed O'Leary absence through injury was a big loss for Arsenal, and implied that Williams was needed in midfield: "Without him they don't have a leader. He has a presence and that Wembley flair."<ref name=prev>{{cite news |title=Harford can be match-winner |first=Clive |last=White |page=41 |newspaper=The Times |date=23 April 1988}}</ref>

Like Harford, Graham had issues selecting his side, going as far to say "it's the hardest line-up I have had to choose."<ref name=gus>{{cite news |title=Harford aiming to bury Caesar |first1=Barry |last1=Flatman |first2=Kevin |last2=Moseley |page=44 |newspaper=Daily Express |location=London |date=23 April 1988}}</ref> Already without O'Leary, Graham was waiting for [[Paul Davis (footballer, born 1961)|Paul Davis]] to pass a fitness test before finalising his first eleven; the England international had caught a virus a week before the final and subsequently missed training.<ref>{{cite news |title=Chance to strike right note |first=David |last=Lacey |page=15 |newspaper=The Guardian |location=London |date=23 April 1988}}</ref> Arsenal defender [[Gus Caesar]], who deputised for O'Leary, relished the challenge of being up against Harford: "[He] is aggressive, but I'm not worried. I played with [[Tony Adams]] for two years in the youth team. We can always draw on that experience. It's instinctive."<ref name=gus/>

The final was broadcast live in the United Kingdom on [[ITV (TV channel)|ITV]], presented by [[Elton Welsby]] with commentary from [[Brian Moore (commentator)|Brian Moore]] and [[David Pleat]].<ref>{{cite news |title=Sport on TV |page=38 |newspaper=The Times |date=23 April 1988}}</ref><ref>{{cite web |title=Wembley Gold |url=http://www.itvsportarchive.com/index.php/wembley-gold?highlight=WyJsdXRvbiIsImFyc2VuYWwiXQ== |publisher=ITV Sport Archive |date=23 April 1988 |accessdate=5 June 2016}}</ref> The winners stood to receive [[Pound sterling|£]]75,000 in prize money, while the losing finalists earnt £25,000.<ref name=prev/>

==Match==

===Summary===
Sealey was not fit in time for the final, so Harford selected [[Andy Dibble]] to deputise, in only his sixth game of the season.<ref name=lineup>Moore, Pleat (1988). Event occurs before [[Kick-off (association football)|kick-off]]; live coverage between 14:27pm and 14:30pm.</ref> [[Mal Donaghy]] passed a fitness test and partnered [[Steve Foster]] in defence, while [[Rob Johnson (footballer, born 1962)|Rob Johnson]] was preferred as left back to [[Ashley Grimes (footballer, born 1957)|Ashley Grimes]].<ref name=lineup/> Hill and Preece came back into the side in midfield, and Brian Stein played behind Harford and [[Kingsley Black]].<ref name=lineup/> As expected for Arsenal, Graham paired Caesar with Adams in central defence, and Davis returned to the first eleven after his short illness. Up front, Smith was positioned alongside Groves.<ref name=lineup/> Harford set his team up in a [[Formation (association football)#4–3–3|4–3–3]] formation, whereas Graham went for the traditional  [[Formation (association football)#4–4–2|4–4–2]] system: a four-man [[Defender (association football)|defence]] (comprising two [[Defender (association football)#Centre-back|centre-backs]] and [[Defender (association football)#Full-back|left and right full-backs]]), four [[midfielders]] (two in the [[Midfielder#Central midfielder|centre]], and one on each [[Midfielders#Wide midfielder|wing]]) and two [[Forward (association football)#Centre forward|centre-forwards]].<ref name=lineup/>

Luton kicked-off the final, and both sides enjoyed early spells of possession, moving the ball about briskly. Arsenal tested the Luton back four in the eighth minute, when Davis' pass went over Johnson and the ball was collected by Thomas on the right flank.<ref name=eigh>Moore, Pleat (1988). Event occurs around the eight-minute mark; live coverage between 14:37pm and 14:39pm.</ref> Charging towards the [[penalty area]], he was impeded by the incoming Dibble and moved near the byline, but Johnson obstructed his eventual shot at goal.<ref name=eigh/> Minutes later, Luton had their first chance of the match from a long free kick, taken by [[Tim Breacker]]. The ball reached the Arsenal penalty area, and goalkeeper [[John Lukic]] failed to collect it; Harford got his head to the ball, but it just went over the crossbar.<ref>Moore, Pleat (1988). Event occurs around the 11th-minute mark; live coverage between 14:40pm and 14:42pm.</ref> Another Luton free kick, this time in the 13th minute, led to the opening goal. The Arsenal defence failed to clear Preece's incoming delivery, and Foster, thinking quick, managed to slip an angled pass in Brian Stein's direction.<ref name=guardian>{{cite news |title=Luton steal home at the last gasp |first=David |last=Lacey |page=44 |newspaper=The Guardian |location=London |date=25 April 1988}}</ref> The midfielder scored – it was the second goal Arsenal had conceded in the competition.<ref name=guardian/>
[[File:Dibble, Andy.jpg|thumb|[[Andy Dibble]] (pictured in 2010) made a series of saves to deny Arsenal from extending their lead.]]
Arsenal began to play with purpose once going behind, but for the rest of the half struggled to get the better of Luton's defence. A long ball by Davis sent Rocastle charging forward in the 16th minute, and momentarily upset Luton's shape, but it was caught by Dibble.<ref>Moore, Pleat (1988). Event occurs around the 16th-minute mark; live coverage between 14:45pm and 14:47pm.</ref> The Luton goalkeeper was again called into action when Arsenal were awarded a set piece; Sansom slid the ball to [[Nigel Winterburn]], whose shot was gathered.<ref>Moore, Pleat (1988). Event occurs around the 18th-minute mark; live coverage between 14:47pm and 14:49pm.</ref> Foster made a timely challenge on Davis on the edge of the penalty area to end another Arsenal attack; Graham's team had been more threatening on right-hand flank with Rocastle and Winterburn.<ref>Moore, Pleat (1988). Incident occurs between the 31st and 35th minute of the match; live coverage between 15:01pm and 15:06pm.</ref> Harford, who had been a long figure up front for Luton, still managed to trouble the Arsenal defence with little service; near the end of the first half, he collected a pass from the left, bypassed his marker Adams and aimed his shot at goal, which went wide of the right-hand post.<ref>Moore, Pleat (1988). Event occurs around the 44th-minute mark; live coverage between 15:13pm and 15:15pm.</ref>

Luton came close to scoring their second goal of the match when Harford got away from Caesar and ran towards the left byline. His [[Cross (football)|cross]] into the penalty area was met by Brian Stein's head, but Lukic made a crucial save, tipping the ball wide.<ref name=times/> Both managers brought on substitutes after the hour mark – Mark Stein came on for Harford, while [[Martin Hayes (footballer)|Martin Hayes]] replaced Groves.<ref name=guardian/> The latter substitution worked to Arsenal's advantage, as it brought about their equaliser in the 72nd minute.<ref name=guardian/> Davis' free kick delivered in was only half cleared by Foster and amongst the scramble, Hayes drove the ball in the net.<ref name=guardian/> Arsenal continued to pile on pressure, and took the lead three minutes later, when Smith received a pass from Thomas to score.<ref name=guardian/>

The turning point of the match came with ten minutes left. After a period of fluent football by the Arsenal forwards which saw Smith hit the woodwork and Hayes, Thomas and Rocastle all had shots saved by Dibble, Arsenal were awarded a penalty after Rocastle was fouled in the box.<ref name=times/> Dibble saved Winterburn's spot-kick by turning the ball round the post.<ref name=times/> A newly inspired Luton equalised with seven minutes remaining when Caesar mis-kicked a clearance on the edge of his penalty area, enabling Luton's Wilson to head the ball into the Arsenal goal from a Mark Stein cross.<ref name=times/> With less than a minute to go, Adams fouled Mark Stein, and from the resulting free kick, Brian Stein scored his second goal of the match to put the game beyond the reach of the defending champions.<ref name=guardian/>

===Details===
{{football box
|date=24 April 1988
|time=14:30 [[British Summer Time|BST]]
|team1=[[Luton Town F.C|Luton Town]]
|score=3–2
|report=
|team2=[[Arsenal F.C.|Arsenal]]
|goals1=[[Brian Stein|Stein]] {{goal|13||90}}<br />[[Danny Wilson (footballer, born 1960)|Wilson]] {{goal|82}}
|goals2=[[Martin Hayes (footballer)|Hayes]] {{goal|71}}<br />[[Alan Smith (footballer, born 1962)|Smith]] {{goal|74}}
|stadium=[[Wembley Stadium (1923)|Wembley]], London
|attendance=95,732
|referee=[[Joe Worrall]] ([[Cheshire County Football Association|Cheshire]]) }}
{| width=92% |
|-
|{{Football kit
 | pattern_la = _shoulder_stripes_white_shirt_alt
 | pattern_b  = _vneckonwhite
 | pattern_ra = _shoulder_stripes_white_shirt_alt
 | pattern_sh = _white stripes
 | pattern_so =
 | leftarm    = 000066
 | body       = 000066
 | rightarm   = 000066
 | shorts     = 000066
 | socks      = FFFFFF
 | title      = Luton Town
}}
|{{Football kit
 | pattern_la = _shoulder_stripes_white_shirt_alt
 | pattern_b  =
 | pattern_ra = _shoulder_stripes_white_shirt_alt
 | leftarm    = CC0000
 | body       = CC0000
 | rightarm   = CC0000
 | shorts     = FFFFFF
 | socks      = CC0000
 | title      = Arsenal
}}
|}
{| width=100%
|valign=top width=50%|
{| style=font-size:90% cellspacing=0 cellpadding=0
|colspan=4|
|-
!width=25| !!width=25|
|-
|GK ||'''1'''||{{flagicon|WAL}} [[Andy Dibble]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Tim Breacker]]
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Rob Johnson (footballer, born 1962)|Rob Johnson]]
|-
|CM ||'''4''' ||{{flagicon|ENG}} [[Ricky Hill]]
|-
|CB ||'''5''' ||{{flagicon|ENG}} [[Steve Foster]] ([[Captain (association football)|c]])
|-
|CB ||'''6'''||{{flagicon|NIR}} [[Mal Donaghy]]
|-
|RM ||'''7'''||{{flagicon|NIR}} [[Danny Wilson (footballer, born 1960)|Danny Wilson]]
|-
|CM ||'''8''' ||{{flagicon|ENG}} [[Brian Stein]]
|-
|CF ||'''9'''||{{flagicon|ENG}} [[Mick Harford]] || || {{suboff}}
|-
|CM ||'''10'''||{{flagicon|ENG}} [[David Preece (footballer, born 1963)|David Preece]] || || {{suboff}}
|-
|LM ||'''11''' ||{{flagicon|NIR}} [[Kingsley Black]]
|-
|colspan=3|'''Substitutes:'''
|-
|DF || ||{{flagicon|EIR}} [[Ashley Grimes (Irish footballer)|Ashley Grimes]] || || {{subon}}
|-
|FW || ||{{flagicon|ENG}} [[Mark Stein (footballer)|Mark Stein]] || || {{subon}}
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|ENG}} [[Ray Harford]]
|}
|valign="top" width="50%"|
{| style="font-size:90%" cellspacing="0" cellpadding="0" align=center
|colspan=4|
|-
!width=25| !!width=25|
|-
|GK ||'''1'''||{{flagicon|ENG}} [[John Lukic]]
|-
|RB ||'''2''' ||{{flagicon|ENG}} [[Nigel Winterburn]]
|-
|LB ||'''3''' ||{{flagicon|ENG}} [[Kenny Sansom]]
|-
|CM ||'''4''' ||{{flagicon|ENG}} [[Michael Thomas (footballer, born 1967)|Michael Thomas]]
|-
|CB ||'''5''' ||{{flagicon|ENG}} [[Gus Caesar]]
|-
|CB ||'''6'''||{{flagicon|ENG}} [[Tony Adams (footballer)|Tony Adams]] ([[Captain (association football)|c]])
|-
|RM ||'''7''' ||{{flagicon|ENG}} [[David Rocastle]]
|-
|CM ||'''8'''||{{flagicon|ENG}} [[Paul Davis (footballer, born 1961)|Paul Davis]]
|-
|CF ||'''9'''||{{flagicon|ENG}} [[Alan Smith (footballer, born 1962)|Alan Smith]]
|-
|CF ||'''10'''||{{flagicon|ENG}} [[Perry Groves]] || || {{suboff}}
|-
|LM ||'''11''' ||{{flagicon|ENG}} [[Kevin Richardson (footballer)|Kevin Richardson]]
|-
|colspan=3|'''Substitutes:'''
|-
|MF || ||{{flagicon|ENG}} [[Martin Hayes (footballer)|Martin Hayes]] || || {{subon}}
|-
|CF || ||{{flagicon|EIR}} [[Niall Quinn]]
|-
|colspan=3|'''Manager:'''
|-
|colspan=4|{{flagicon|SCO}} [[George Graham (footballer)|George Graham]]
|}
{| width=82% style="font-size: 90%"
| width=50% valign=top|
'''Match rules'''
*90 minutes.
*30 minutes of extra-time if necessary.
*Replay if scores still level.
*Two named substitutes.
*Maximum of two substitutions.
|}
|}

==Post-match and legacy==
Luton's cup win was the club's first major piece of silverware in its history.<ref>{{cite news |title=History |url=http://www.lutontown.co.uk/club/history/ |publisher=Luton Town F.C |date=27 August 2012 |accessdate=5 June 2016}}</ref> A jubilant Harford described it as "the greatest win in my time at Luton," having thought his team had lost the match when Smith scored.<ref name=postm>{{cite news |title=Harford finds it hard to smile in victory |page=38 |newspaper=The Times |date=25 April 1988}}</ref> He commended his goalkeeper, saying: "Considering all the circumstances, I think Dibble had to be the man of the match, and as for the young boy, [Kingsley] Black was sensational. He has so much talent." The stand-in goalkeeper for Luton was delighted with his role in the final, but made it pertinent that he needed regular first-team football, or he would be tempted to leave the following season.<ref name=caps>{{cite news |first=Barry |last=Flatman |title=Andy caps a supershow |page=40 |newspaper=Daily Express |location=London |date=25 April 1988}}</ref> Brian Stein spoke of his surprise of scoring the winner, and said: "I asked the referee how long was left and he just blew the whistle."<ref name=caps/> Luton held a civic ceremony two days after the final, where the trophy was presented in front of the club's supporters.<ref name=celeb>{{cite news |title=Final tickets flaw exposed |first=Dennis |last=Signy |page=46 |newspaper=The Times |date=27 April 1988}}</ref> It was there the stem of the trophy had become noticeably damaged; Luton the following morning returned it to the competition's sponsors for repairs.<ref name=celeb/>

Graham felt his team "...&nbsp;were there when we were leading 2–1," and expressed his sympathy for Winterburn, "He is a good signing for Arsenal and to miss a penalty so close to the end was a great disappointment to him."<ref name=postm/> He extended his congratulations to Luton, in particular Dibble, telling reporters: "Their goalkeeper was absolutely superb. He kept them in the game when they could have been finished."<ref name=caps/> The Arsenal manager also revealed the club would take action against Williams for disappearing on the matchday.<ref>{{cite news |title=Williams faces rap |page=40 |newspaper=Daily Express |location=London |date=25 April 1988}}</ref> The defeat prompted Graham to strengthen his defence over the summer; he was unable to convince his first choice [[Gary Pallister]] to join the club,<ref>{{cite news |title=Arsenal turn to Bould |page=41 |newspaper=The Times |date=30 April 1988}}</ref> but managed to sign 25-year-old defender [[Steve Bould]] from Stoke.<ref>{{cite news |title=Bould signs on for Arsenal |page=38 |newspaper=The Times |date=4 June 1988}}</ref> Bould went on to displace Caesar in the starting eleven, whose career stalled after the 1988 final.<ref>{{cite book |first=Nick |last=Hornby |title=Fever Pitch |url=https://books.google.co.uk/books?id=9fBuFuklv-sC&pg=PT169#v=onepage&q&f=false |page=169 |publisher=Penguin |year=2005 |isbn=0-14-192654-6}}</ref>

Although Luton won the League Cup, they did not earn a place in the [[UEFA Cup]], as UEFA chose not to relax its [[Heysel Stadium disaster#Aftermath|ban on English teams from playing in European club competitions]] during the late 1980s.<ref>{{cite news |first=Miguel |last=Delaney |title=What if&nbsp;... England hadn't been banned |url=http://www.espnfc.co.uk/story/1481816/delaney-what-if-england-hadnt-been-banned-from-europe |publisher=ESPN FC |date=21 June 2013 |accessdate=5 June 2016}}</ref> [[David Evans (British politician)|David Evans]], the Luton chairman was against his club participating in European football, though he never publicised his reasoning.<ref>{{cite news |first=Ian |last=Ridley |title=Wombling path towards Europe |page=38 |newspaper=The Guardian |location=London |date=4 June 1988}}</ref>

The match is considered as one of the best League Cup finals, and greatest Cup shocks in the competition's history.<ref>{{cite news |title=Chelsea vs Tottenham: The 10 best ever League Cup finals |url=http://www.telegraph.co.uk/sport/football/competitions/league-cup/11437484/Chelsea-vs-Tottenham-The-10-best-ever-League-Cup-finals.html |website=The Telegraph |date=27 February 2015 |accessdate=5 June 2016}}</ref><ref>{{cite news |first1=Simon |last1=Rice |first2=Majid |last2=Mohamed |title=The greatest League Cup shocks |url=http://www.independent.co.uk/sport/football/news-and-comment/the-greatest-league-cup-shocks-1791532.html |newspaper=The Independent |location=London |date=12 December 2012 |accessdate=5 June 2016}}</ref> In 2015, Luton's victory was ranked 12th in a list of the "50 greatest things from the past three decades", by readers of the ''Luton on Sunday''.<ref>{{cite news |title=Luton Town's cup win over Arsenal in 1988 picked by locals as great thing from last 30 years |url=http://www.luton-dunstable.co.uk/Luton-Town-s-cup-win-Arsenal-1988-picked-locals/story-27867158-detail/story.html |newspaper=Luton on Sunday |date=25 September 2015 |accessdate=5 June 2016}}</ref>

==References==
'''General'''
*{{cite AV media |people=[[Brian Moore (commentator)|Moore, Brian]], [[David Pleat|Pleat, David]] (commentators), [[Elton Welsby|Welsby, Elton]] (presenter), [[Ian St John|St John, Ian]] (studio guest) |date=24 April 1988 |title=Littlewoods Cup Final |trans_title=Arsenal v Luton |medium=Television production |publisher=ITV Sport}}

'''Specific'''
{{reflist|30em}}

==Further reading==
*{{cite news |title=The Joy of Six: League Cup final memories |url=https://www.theguardian.com/sport/blog/2009/feb/27/league-cup-finals-carling-football |first=Scott |last=Murray |first2=Rob |last2=Smyth |website=theguardian.com |date=27 February 2009 |accessdate=5 June 2016}}

{{Football League Cup Finals}}
{{Luton Town F.C. matches}}
{{Arsenal F.C. matches}}
{{1987–88 in English football}}

{{EngvarB|date=April 2014}}
{{Use dmy dates|date=April 2014}}

[[Category:EFL Cup Finals]]
[[Category:Arsenal F.C. matches|League Cup Final 1988]]
[[Category:Luton Town F.C. matches|League Cup Final 1988]]
[[Category:1987–88 Football League|League Cup Final]]
[[Category:April 1988 sports events]]
[[Category:1988 in London]]