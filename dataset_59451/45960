{{Infobox NCAA Ice Hockey Tournament |
| Year=2014
| Gender=Women's
| Division=National Collegiate
| Image=2014 Women's Frozen Four Logo.jpg
| ImageSize=200px
| Caption=2014 Women's Frozen Four logo
| Teams=8
| FrozenFourArena= [[TD Bank Sports Center]]
| FrozenFourCity=[[Hamden, Connecticut]]
| Champions= Clarkson
| TitleCount= 1st
| ChampGameCount= 1st
| ChampFFCount= 1st
| RunnerUp= Minnesota
| GameCount= 6th
| RunnerFFCount= 10th
| Semifinal1= Wisconsin
| FrozenFourCount= 7th
| Semifinal2= Mercyhurst
| FrozenFourCount2= 4th
| Coach= Shannon Desrosiers and Matt Desrosiers
| CoachCount= 1st
| MOP= Jamie Lee Rattray
| MOPTeam= Clarkson
| Attendance= 6,744
}}

The '''2014 NCAA National Collegiate Women's Ice Hockey Tournament''' involved eight schools in [[Single-elimination tournament|single-elimination]] play to determine the national champion of women's [[National Collegiate Athletic Association|NCAA]] [[Division I (NCAA)|Division I]] [[college sports|college]] [[ice hockey]]. The quarterfinals were contested at the campuses of the seeded teams on March 15, 2014. The Frozen Four was played on March 21 and 23, 2014 at [[TD Bank Sports Center]] in [[Hamden, Connecticut]] with [[Quinnipiac University]] as the host.<ref name= "bracket">{{cite web | url=http://www.ncaa.com/interactive-bracket/icehockey-women/nc/2013 |author=<!--Staff writer(s); no by-line.--> |title= Women's Ice Hockey Bracket| website= NCAA.com| publisher= NCAA| accessdate= March 11, 2014}}</ref>

Clarkson University defeated the University of Minnesota 5–4 in the national championship game, in the process becoming the fourth school to have won a National Collegiate championship. This championship was the first by a team not from the [[Western Collegiate Hockey Association|WCHA]] as well as the first by a team from the Eastern United States.<ref name="bracket" /> It also proved to be the final game for Clarkson's co-head coach Shannon Desrosiers, who had finished her sixth season sharing head coaching duties with her husband Matt. About a month after the championship game, Shannon stepped down, leaving Matt in sole charge. Shannon cited a wish to spend more time raising the couple's young daughter and soon-to-be-born second child.<ref>{{cite press release | url=http://www.clarksonathletics.com/news/2014/4/21/GEN_0421142908.aspx?path=whock |title=Shannon Desrosiers to Step Down as Clarkson Women's Hockey Co-Head Coach| date= April 21, 2014 | website=ClarksonAthletics.com| publisher=Clarkson Athletics| accessdate=April 23, 2014}}</ref>

== Qualifying teams ==

The winners of the [[ECAC Hockey|ECAC]], [[Western Collegiate Hockey Association|WCHA]], and [[Hockey East]] tournaments all received automatic berths to the NCAA tournament. The other five teams were selected at-large. The top four teams were then seeded and received home ice for the quarterfinals.<ref name= "selection">{{cite web | url=http://www.ncaa.com/news/icehockey-women/article/2014-03-09/committee-releases-eight-team-field-national-championship |author=<!--Staff writer(s); no by-line.--> |title= Committee releases eight-team field for national championship tournament| date= March 10, 2014| website= NCAA.com| publisher= NCAA| accessdate= March 11, 2014}}</ref>
{|
| valign=top |
{| class="wikitable"
! colspan="7" style="background:#ffdead;" | 
|-
!style="background: #e3e3e3;"|Seed
!style="background: #e3e3e3;"|School
!style="background: #e3e3e3;"|Conference 
!style="background: #e3e3e3;"|Record
!style="background: #e3e3e3;"|Berth type
!style="background: #e3e3e3;"|Appearance
!style="background: #e3e3e3;"|Last bid
|-
|align=center|1
|[[Minnesota Golden Gophers women's ice hockey|Minnesota]]
|[[Western Collegiate Hockey Association|WCHA]]
|36–1–1
|Tournament champion
|12th
|2013
|-
|align=center|2
|[[Cornell Big Red women's ice hockey|Cornell]]
|[[ECAC Hockey|ECAC]]
|24–5–4
|Tournament champion
|5th
|2013
|-
|align=center|3
|[[Clarkson Golden Knights women's ice hockey|Clarkson]]
|[[ECAC Hockey|ECAC]]
|28–5–5
|At-large bid
|3rd
|2013
|-
|align=center|4
|[[Wisconsin Badgers women's ice hockey|Wisconsin]]
|[[Western Collegiate Hockey Association|WCHA]]
|27–7–2
|At-large bid
|8th
|2012
|-
|
|[[Harvard Crimson women's ice hockey|Harvard]]
|[[ECAC Hockey|ECAC]]
|23–6–4
|At-large bid
|10th
|2013
|-
|
|[[Boston College Eagles women's ice hockey|Boston College]]
|[[Hockey East]]
|27–6–3
|At-large bid
|5th
|2013
|-
|
|[[Mercyhurst Lakers women's ice hockey|Mercyhurst]]
|[[College Hockey America|CHA]]
|23–8–4
|At-large bid
|10th
|2013
|-
|
|[[Boston University Terriers women's ice hockey|Boston University]]
|[[Hockey East]]
|24–12–1
|Tournament champion
|5th
|2013
|}
|}

== Bracket ==
<ref name="bracket" /><br>''Quarterfinals held at home sites of seeded teams''
{{8TeamBracket
| RD1=National Quarterfinals <br> March 15
| RD2=National Semifinals  <br> March 21
| RD3=National Championship <br> March 23
| RD1-seed1= 1
| RD1-team1= '''Minnesota'''
| RD1-score1= '''5'''
| RD1-seed2=
| RD1-team2= Boston University
| RD1-score2= 1
| RD1-seed3= 4
| RD1-team3= '''Wisconsin'''
| RD1-score3= '''2'''
| RD1-seed4=
| RD1-team4= Harvard
| RD1-score4= 1
| RD1-seed5= 2
| RD1-team5= Cornell
| RD1-score5= 2
| RD1-seed6=
| RD1-team6= '''Mercyhurst'''
| RD1-score6= '''3'''
| RD1-seed7= 3
| RD1-team7= '''Clarkson'''
| RD1-score7= '''3'''
| RD1-seed8=
| RD1-team8= Boston College
| RD1-score8= 1
| RD2-seed1= 1
| RD2-team1= '''Minnesota'''
| RD2-score1= '''5'''
| RD2-seed2= 4
| RD2-team2= Wisconsin
| RD2-score2= 3
| RD2-seed3=
| RD2-team3= Mercyhurst
| RD2-score3= 1
| RD2-seed4= 3
| RD2-team4= '''Clarkson'''
| RD2-score4= '''5'''
| RD3-seed1= 1
| RD3-team1= Minnesota
| RD3-score1= 4
| RD3-seed2= 3
| RD3-team2= '''Clarkson'''
| RD3-score2= '''5'''
}}
<small>Note: * denotes overtime period(s)</small>

== See also ==

*[[2014 NCAA Division I Men's Ice Hockey Tournament]]

== References ==
{{reflist}}

{{NCAA Women's Division I Ice Hockey Tournament}}
{{2013–14 NCAA Division I championships navbox}}

[[Category:NCAA Women's Ice Hockey Championship]]
[[Category:2013–14 NCAA Division I women's hockey season|1]]