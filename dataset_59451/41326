{{Use mdy dates|date=December 2014}}
{{Infobox Congressman
| name =Julia Carson
| image name =carson julia.jpg
| imagesize =220px
| birth_date={{birth date|1938|07|08}}
| death_date={{Death date and age|2007|12|15|1938|07|08|mf=y}}
| birth_place =[[Louisville, Kentucky]]
| death_place =[[Indianapolis, Indiana]]
|resting_place=[[Crown Hill Cemetery]]<br>[[Indianapolis, Indiana]]
| state = [[Indiana]]
| district = {{ushr|Indiana|7|7th}}
| term_start =January 3, 2003
| term_end = December 15, 2007
| preceded = [[Brian Kerns]]
| succeeded = [[André Carson]] 
| state3 = [[Indiana]]
| district3 = {{ushr|Indiana|10|10th}}
| term_start3 =January 3, 1997
| term_end3 = January 3, 2003
| preceded3 = [[Andrew Jacobs, Jr.]]
| succeeded3 = District eliminated in [[United States congressional apportionment|reapportionment]]
|order4=
|office4=Member of the [[Indiana State Senate]]
|term_start4=1977
|term_end4=1991
|order5=
|office5=Member of the [[Indiana House of Representatives]]
|term_start5=1973
|term_end5=1977
| party =[[Democratic Party (United States)|Democrat]] 
| religion = [[Baptist]]
| spouse = Divorced
| alma_mater=[[Indiana University-Purdue University Indianapolis]]
| occupation= political assistant 
}}
'''Julia May Carson''' (July 8, 1938 – December 15, 2007), born '''Julia May Porter''', was a member of the [[United States House of Representatives]] for {{ushr|Indiana|7|}} from 1997 until her death in 2007 (numbered as the 10th District from 1997 to 2003).  Carson was the first woman and first [[African American]] to represent the 7th District. She was also the second African American woman elected to [[United States Congress|Congress]] from [[Indiana]], after [[Katie Hall (politician)|Katie Hall]].

==Life and political career==
Carson was born in Louisville, Kentucky. The daughter of Velma V. Porter, she moved to [[Indianapolis, Indiana|Indianapolis]] while still a girl and worked in various positions to support her family. She graduated from [[Crispus Attucks High School]] in 1955 in Indianapolis.<ref name=is>{{cite news |first=Rob|last=Schneider|title= Carson remembered: Congresswoman gave voice to disadvantaged|url=http://www.indystar.com/apps/pbcs.dll/article?AID=/20071216/SPECIAL21/712160384 |work= [[Indianapolis Star]] |publisher= |date=December 16, 2007 |accessdate=December 16, 2007 |archiveurl = https://web.archive.org/web/20071219114344/http://www.indystar.com/apps/pbcs.dll/article?AID=/20071216/SPECIAL21/712160384 |archivedate = December 19, 2007}}</ref> She then attended [[Martin University]] in Indianapolis and [[Indiana University – Purdue University Indianapolis]]. She was a member of [[Zeta Phi Beta]] sorority.

In 1965, while working as a secretary at [[United Auto Workers|UAW]] Local 550, she was hired away by newly elected congressman [[Andrew Jacobs, Jr.|Andrew Jacobs]] to do casework in his Indianapolis office. When his own electoral prospects looked dim in 1972, he encouraged Carson to run for the [[Indiana House of Representatives]], which she did; she was elected in 1972, serving as a member for four years. In 1976, she successfully ran for the [[Indiana Senate]], where she served for 14 years.

In 1990 she was elected as a trustee for [[Center Township, Marion County, Indiana|Center Township]] (downtown Indianapolis), and was responsible for running [[Welfare (financial aid)|welfare]] in central Indianapolis. Carson served six years as a trustee, creating a $6 million [[Economic surplus|surplus]] from the office's $20 million debt.<ref name='cancer'>{{cite web |url=http://www.cnn.com/2007/POLITICS/11/25/carson.cancer/ | title=Congresswoman has terminal cancer | publisher=[[CNN]] | date=November 25, 2007}}</ref> Jacobs has said Carson "not only took cheats off the welfare rolls, she [[lawsuit|sued]] them to get the money". When Jacobs retired in 1996, Carson ran as his replacement in what was then the 10th District, and won [[Democratic Party (United States)|Democratic]] endorsement despite being heavily outspent by party chairman Ann DeLaney, 49 percent to 31 percent.

In the general election she faced [[Republican Party (United States)|Republican]] [[Virginia Murphy Blankenbaker]], a state senator and stockbroker who, like Carson, was a grandmother with [[Liberalism|liberal]] views on [[abortion]] and the [[death penalty]]. Each raised a similar sum of money, but Carson won 53 percent that November.

==House record==
Carson had a reputation for being somewhat unpredictable, including votes for [[anti-terrorism]] bills and normal [[trade]] relations with [[China]]. Carson opposed the [[Iraq war]] resolution in 2002.

Carson's legislative record included leading Congress to pass a House measure awarding [[Rosa Parks]] the [[Congressional Gold Medal]]; cosponsoring, with Sen. [[Richard Lugar]], the removal of [[bureaucracy|bureaucratic]] bottlenecks on child [[health insurance]]; and the commemoration of the life and accomplishments of author [[Kurt Vonnegut]] (H.RES.324<ref>{{cite web|url=http://thomas.loc.gov/cgi-bin/query/z?c110:H.RES.324:|title=Honoring the life and accomplishments of Kurt Vonnegut, Jr. and extending the condolences of the House of Representatives to his family on the occasion of his death. (Introduced in House)|accessdate=July 26, 2009}}</ref>).

Some of her other Congressional accomplishments included critical funding to revitalize the Fall Creek Neighborhood in [[Indianapolis, Indiana|Indianapolis]] which today includes some of the finest examples of reclaimed urban landscape in the U.S. She also provided outstanding support for the new terminal for the [[Indianapolis International Airport]], which opened November 12, 2008.

She focused key attention on health care for U.S. veterans, and frequently visited ailing vets at the Roudebush Medical Center in Indianapolis.

She was the first recipient of the [[Frank O'Bannon]] Award from Indiana Stonewall Democrats. She was a co-sponsor of the Equal Employment Non-Discrimination Act and a member of the Gay, Lesbian, Bisexual and Transgender Equality Caucus in the U.S. House led by U.S. Representative [[Barney Frank]], D-Massachusetts.

Carson was reelected with little difficulty in 1998 and 2000. Her 2000 campaign attracted a personal appearance by President [[Bill Clinton]] that drew thousands to the Indiana State Fairgrounds. In 2006, she traveled from Washington, D.C. to Indianapolis aboard [[Air Force One]] with President [[George W. Bush]] to appear at Indiana Black Expo.

Her poor health and physical struggles led to tighter-than-expected races afterward.  In the 2002 election, her district was renumbered as the 7th District after Indiana lost a Congressional district after the 2000 [[census]], and was made slightly more Republican than its predecessor. Carson faced public affairs specialist Brose McVey. In a heated campaign that led to Carson leaving the stage in protest in their final pre-election debate, she won re-election 53 percent to 44 percent. She was re-elected by just over 11 points in 2004 defeating Republican Andrew Horning and Libertarian Barry Campbell.

Carson defeated [[Eric Dickerson (politician)|Eric Dickerson]] in the [[Indiana's 7th congressional district election, 2006|2006 elections]] 54 percent to 46 percent, a narrow 8-point margin in a year when most incumbent Democrats skated to victory.<ref name='election'>{{cite web | url=http://www.in.gov/apps/sos/election/general/general2006;jsessionid=a3SUQAAWNALg?page=office&countyID=-1&officeID=5&districtID=-1 | title=Indiana General Election November 7, 2007 | publisher=[[Todd Rokita|Secretary of State of Indiana]] | date=November 6, 2007}}</ref>  In the same election, Democratic challengers toppled Republican incumbents in three Indiana districts much more conservative than Carson's.

Carson was a member of the [[Congressional Black Caucus]].

She was one of the 31 who voted in the House not to count the [[United States Electoral College|electoral votes]] from [[Ohio]] in the [[United States presidential election, 2004|2004 presidential election]].<ref name='clerk'>{{cite web | url=http://clerk.house.gov/evs/2005/roll007.xml | title=Final Vote Results for Roll Call 7 | publisher=[[Clerk of the United States House of Representatives|Office of the Clerk]] | date=January 6, 2005}}</ref>

==Illness and death==
On September 29, 2007, the ''[[Indianapolis Star]]'' reported that Carson had been an in-patient at Methodist Hospital in Indianapolis for the preceding eight days.<ref name='leave'>{{cite web | url=http://www.indystar.com/apps/pbcs.dll/article?AID=/20070929/LOCAL19/309290003/-1/RSS | title=Carson hospitalized with leg infection | author=Maureen Groppe | publisher=[[The Indianapolis Star]] | date=September 29, 2007}}</ref> She was being treated for an infection in her leg near the area where a vein was removed in 1996 during double bypass heart surgery. Before her hospitalization was revealed, Carson missed 42 of 77 votes during the month. Year-to-date, Carson had participated in 87 percent of the House votes.

On November 25, 2007, the ''Star'' reported that Carson had been diagnosed with terminal [[lung cancer]].<ref name='cancer'/> During her treatment for the leg infection, the cancer was discovered by Carson's doctors. Carson had battled it before, but it had gone into [[remission (medicine)|remission]]. In a statement, Carson said she was ready to return to Washington before "the second shoe fell—heavily."

According to her friend, former U.S. Representative Andy Jacobs, Carson died at about 9:15 AM on December 15, 2007.<ref name=schneiderstar>Schneider, Mary Beth. [http://www.indystar.com/apps/pbcs.dll/article?AID=/20071215/LOCAL/71215001/0/NLETTER09 Congresswoman Julia Carson dies], ''[[Indianapolis Star]]'', December 15, 2007. Accessed 2007-12-15.{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref>

On December 21, 2007 Julia Carson's casket was taken to the [[Indiana Statehouse]] in downtown Indianapolis by horse-drawn military caisson. Carson became the ninth Hoosier to lie in repose at the Statehouse Rotunda. An early-morning service was held in the statehouse where Indiana Governor [[Mitch Daniels]] and Carson's grandson, City-County Councilman [[André Carson]], gave remarks.<ref>http://www.wthr.com/global/story.asp?s=7528891</ref> Thousands of Hoosiers paid last respects to Carson by visiting the casket and attending an evening ceremony held in the Statehouse, including the Reverend [[Jesse Jackson]], Indianapolis Mayor [[Bart Peterson]] (D), former U.S. Representative [[Andrew Jacobs, Jr.]], D-Ind., U.S. Representative [[Brad Ellsworth]], D-Indiana, U.S. Representative [[Baron Hill (politician)|Baron Hill]], D-Indiana, U.S. Representative [[Sheila Jackson Lee]], D-Texas, U.S. Representative [[Diane Watson]], D-California, and former [[Gary, Indiana]] mayor [[Richard Hatcher]]. Rudy Clay, mayor of Gary at the time, presented a key to the city to the Carson family.

The funeral for Julia Carson, held on December 22, 2007 at Eastern Star Baptist Church in Indianapolis, brought thousands of citizens together to pay last respects. Those who spoke at the funeral included Governor Daniels (R), U.S. Senator [[Richard Lugar]], R-Indiana, U.S. Senator [[Evan Bayh]], D-Ind., former U.S. Senator [[Birch Bayh]], D-Indiana, U.S. Representative [[Pete Visclosky]], D-Indiana, U.S. Representative [[Stephanie Tubbs Jones]], D-Ohio, Indiana House Speaker [[B. Patrick Bauer]], D-South Bend, Indianapolis Mayor Peterson, radio host and Hoosier native [[Tavis Smiley]], and the Reverend [[Louis Farrakhan]]. The funeral services aired on live television in central Indiana. Carson was buried in [[Crown Hill Cemetery]] in Indianapolis. The graveside ceremony included a three-volley salute.

[[Indiana's 7th congressional district special election, 2008|A special election]] was held on March 11, 2008 to determine Carson's replacement,<ref name=SpecialElectionEO>[http://www.in.gov/gov/files/EO_08-01.pdf Executive Order 08-01] Mitch Daniels. January 7, 2008.</ref> and her grandson [[André Carson]] won the election, defeating his Republican opponent, State Representative [[Jon Elrod]] and Libertarian opponent Sean Shepard.

Andre Carson also won the May 2008 Democratic Primary for Congress against six opponents. Andre Carson won the endorsement of U.S. Senator [[Barack Obama]], D-Illinois, for the primary victory.

==Committees and subcommittees==
* [[United States House Committee on Financial Services|Committee on Financial Services]] (ranked 10th of 32 Democrats)
** [[United States House Financial Services Subcommittee on Financial Institutions and Consumer Credit]]
** [[United States House Financial Services Subcommittee on Housing and Community Opportunity]]
* [[United States House Committee on Transportation and Infrastructure|Committee on Transportation & Infrastructure]]
**[[United States House Transportation Subcommittee on Railroads, Pipelines, and Hazardous Materials|Subcommittee on Railroads, Pipelines, and Hazardous Materials]]
** [[United States House Transportation Subcommittee on Highways and Transit|Subcommittee on Highways and Transit]]

==Group ratings (109th Congress)==
(January 2005 – January 2007)
*[[National Journal]]
** Economic: 86.5% Liberal, 10.5% Conservative
** Social: 90.5% Liberal, 9% Conservative
** Foreign: 83% Liberal, 15.5% Conservative
*[[Americans for Democratic Action]]: 95
*[[American Civil Liberties Union]]: 100
*[[Chamber of Commerce of the United States]]: 41.5
*[[Christian Coalition of America|Christian Coalition]]: 25 (108th Congress)
*[[American Conservative Union]]: 2
*[[National Taxpayers Union]]: 11.5
*[[League of Conservation Voters]]: 94.5
*[[Human Rights Campaign]]: 88 (108th Congress)

==Electoral history==
{| class="wikitable" style="margin:0.5em ; font-size:95%"
|+ {{ushr|Indiana|10|}}: Results 1996–2000<ref name="clerkresults">{{cite web|url=http://clerk.house.gov/member_info/electionInfo/index.html |title=Election Statistics |accessdate=January 10, 2008 |publisher=Office of the Clerk of the House of Representatives |deadurl=yes |archiveurl=https://web.archive.org/web/20070725184700/http://clerk.house.gov/member_info/electionInfo/index.html |archivedate=July 25, 2007 |df=mdy }}</ref>
!|Year
!
!|Democrat
!|Votes
!|Pct
!
!|Republican
!|Votes
!|Pct
!
!|3rd Party
!|Party
!|Votes
!|Pct
!
|-
|[[U.S. House election, 1996|1996]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |85,965
|{{Party shading/Democratic}} |53%
|
|{{Party shading/Republican}} |{{nowrap|[[Virginia Murphy Blankenbaker]]}}
|{{Party shading/Republican}} align="right" |72,796
|{{Party shading/Republican}} |45%
|
|{{Party shading/Libertarian}} |Kurt St. Angelo
|{{Party shading/Libertarian}} |[[Libertarian Party (United States)|Libertarian]]
|{{Party shading/Libertarian}} align="right" |3,605
|{{Party shading/Libertarian}} align="right" |2%
| |'''*'''
|-
|[[U.S. House election, 1998|1998]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |69,682
|{{Party shading/Democratic}} |58%
|
|{{Party shading/Republican}} |{{nowrap|[[Gary A. Hofmeister]]}}
|{{Party shading/Republican}} align="right" |47,017
|{{Party shading/Republican}} |39%
|
|{{Party shading/Libertarian}} |Fred C. Peterson
|{{Party shading/Libertarian}} |[[Libertarian Party (United States)|Libertarian]]
|{{Party shading/Libertarian}} align="right" |2,719
|{{Party shading/Libertarian}} align="right" |2%
| |'''*'''
|-
|[[U.S. House election, 2000|2000]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |91,689
|{{Party shading/Democratic}} |59%
|
|{{Party shading/Republican}} |{{nowrap|[[Marvin B. Scott]]}}
|{{Party shading/Republican}} align="right" |62,233
|{{Party shading/Republican}} |40%
|
|{{Party shading/Libertarian}} |NaIlah Ali
|{{Party shading/Libertarian}} |[[Libertarian Party (United States)|Libertarian]]
|{{Party shading/Libertarian}} align="right" |2,780
|{{Party shading/Libertarian}} align="right" |2%
|
|}
{{refbegin}}<nowiki>*</nowiki>Write-in and minor candidate notes:  In 1996, write-ins received 7 votes.  In 1998, Wayne J. Wohlfert received 18 votes.
{{refend}}

{| class="wikitable" style="margin:0.5em ; font-size:95%"
|+ {{ushr|Indiana|7|}}: Results 2002–2006<ref name="clerkresults" />
!|Year
!
!|Democrat
!|Votes
!|Pct
!
!|Republican
!|Votes
!|Pct
!
!|3rd Party
!|Party
!|Votes
!|Pct
!
|-
|[[U.S. House election, 2002|2002]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |77,478
|{{Party shading/Democratic}} |53%
|
|{{Party shading/Republican}} |{{nowrap|[[Brose A. McVey]]}}
|{{Party shading/Republican}} align="right" |64,379
|{{Party shading/Republican}} |44%
|
|{{Party shading/Libertarian}} |Andrew M. Horning
|{{Party shading/Libertarian}} |[[Libertarian Party (United States)|Libertarian]]
|{{Party shading/Libertarian}} align="right" |3,919
|{{Party shading/Libertarian}} align="right" |3%
| |'''*'''
|-
|[[U.S. House election, 2004|2004]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |121,303
|{{Party shading/Democratic}} |54%
|
|{{Party shading/Republican}} |{{nowrap|[[Andrew Horning]]}}
|{{Party shading/Republican}} align="right" |97,491
|{{Party shading/Republican}} |44%
|
|{{Party shading/Libertarian}} |Barry Campbell
|{{Party shading/Libertarian}} |[[Libertarian Party (United States)|Libertarian]]
|{{Party shading/Libertarian}} align="right" |4,381
|{{Party shading/Libertarian}} align="right" |2%
|
|-
|[[Indiana's 7th congressional district election, 2006|2006]]
|
|{{Party shading/Democratic}} |'''Julia Carson'''
|{{Party shading/Democratic}} align="right" |74,750
|{{Party shading/Democratic}} |54%
|
|{{Party shading/Republican}} |{{nowrap|[[Eric Dickerson (politician)|Eric Dickerson]]}}
|{{Party shading/Republican}} align="right" |64,304
|{{Party shading/Republican}} |46%
|
|
|
|
|
|
|}
{{refbegin}}<nowiki>*</nowiki>Write-in and minor candidate notes:  In 2002, James (Jim) Kell Jeffries received 64 votes.
{{refend}}

==References==
{{reflist|2}}

==External links==
{{Portal|Biography}}
* {{Find a Grave|23411184}}
* [http://juliacarson.house.gov/ U.S. Congresswoman Julia Carson], U.S. House site
{{CongLinks | congbio=c000191 | fec=H6IN10141 | ontheissuespath=IN/Julia_Carson.htm | opensecrets=N00003758 | votesmart=}}
* [http://www.sourcewatch.org/index.php?title=Julia_Carson Profile] at [[SourceWatch]]
* [http://www.juliacarson.org/ Julia Carson for Congress], Campaign site

{{s-start}}
{{s-par|us-hs}}
{{USRepSuccessionBox
| state=Indiana
| district=10
| before=[[Andrew Jacobs, Jr.]]
| reason=District eliminated in [[United States congressional apportionment|reapportionment]]
| years=January 3, 1997 – January 3, 2003}}
{{USRepSuccessionBox
| state=Indiana
| district=7
| before=[[Brian Kerns]]
| after=[[André Carson]]
| years=January 3, 2003 – December 15, 2007}}
{{s-end}}
{{IndianaUSRepresentatives}}
{{Authority control}}

{{DEFAULTSORT:Carson, Julia}}
[[Category:1938 births]]
[[Category:2007 deaths]]
[[Category:Indiana Democrats]]
[[Category:Deaths from lung cancer]]
[[Category:Indiana State Senators]]
[[Category:Members of the Indiana House of Representatives]]
[[Category:Members of the United States House of Representatives from Indiana]]
[[Category:Politicians from Indianapolis]]
[[Category:Politicians from Louisville, Kentucky]]
[[Category:Women in Indiana politics]]
[[Category:Burials at Crown Hill Cemetery]]
[[Category:Baptists from the United States]]
[[Category:Female members of the United States House of Representatives]]
[[Category:Deaths from cancer in Indiana]]
[[Category:Women state legislators in Indiana]]
[[Category:African-American members of the United States House of Representatives]]
[[Category:African-American women in politics]]
[[Category:Indiana University – Purdue University Indianapolis alumni]]
[[Category:Democratic Party members of the United States House of Representatives]]
[[Category:20th-century American politicians]]
[[Category:African-American state legislators in Indiana]]
[[Category:20th-century women politicians]]
[[Category:21st-century American politicians]]
[[Category:21st-century women politicians]]