{{COI|date=February 2016}}
{{Infobox university
|logo            = DKICoP logo.png
|name            = The Daniel K. Inouye<br> College of Pharmacy
|motto           = ''[[Ho'omalu Ola]]'' ([[Hawaiian language|Hawaiian]])
|mottoeng        = [[Guardians of Health]]
|image_name      = 
|established     = 2007
|type            = [[Public university|Public]]
|staff           =
|faculty         = 
|dean            = Carolyn Ma (Interim)
|doctoral        = Pharm.D.
|city            = Hilo
|state           = Hawai'i
|country         = United States
|colors          = Pele Crimson & Volcanic Black {{color box|#8B0000}}{{color box|#000000}}
|nickname        = DKICP
|affiliations    = [[University of Hawaii at Hilo]]
|website         = http://pharmacy.uhh.hawaii.edu/
}}
'''The Daniel K. Inouye College of Pharmacy''' is [[University of Hawaii at Hilo#Colleges|one of six colleges]] within the public [[University of Hawaii at Hilo|University of Hawai'i at Hilo]] (UH Hilo) and is also commonly known by the acronym "DKICP". Graduates of this four-year program are awarded a Doctor of Pharmacy degree (Pharm.D.) and it is fully accredited<ref name="ACPE Accreditation Status">{{cite web|url=https://www.acpe-accredit.org/shared_info/programsSecure.asp?sortby=state#HI |title=ACPE Accreditation Status |accessdate=January 20, 2016}}</ref> by the [[Accreditation Council for Pharmacy Education]], "the sole accreditor of the Doctor of Pharmacy programs in the United States."<ref name=ACPE>{{cite web|url=https://www.acpe-accredit.org/students/faqs.asp#8 |title=ACPE FAQ Page |publisher=www.acpe-accredit.org/ |accessdate=January 20, 2016}}</ref>

The College of Pharmacy was founded in 2007 and graduated its inaugural class in 2011. It is the only pharmacy school in the state of Hawai'i, and is also home to a Ph.D. program. In 2012, one year after the first class graduated, U.S. News & Report ranked the UH Hilo College of Pharmacy rank #74<ref name="US News & Report Pharmacy Rankings">{{cite web|url=http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-health-schools/pharmacy-rankings/page+3 |title=US News Pharmacy School Ranking |accessdate=January 20, 2016}}</ref> of nearly 200 pharmacy schools throughout the United States.

== History ==

=== Daniel K. Inouye ===

The late United States Senator [[Daniel Inouye|Daniel K. Inouye]], after whom the school is named, broached the idea of establishing a school of pharmacy in Hawai'i in 1998. Even before that, he had been a supporter of rural America, sponsoring bills that sought to provide better education and healthcare in under-served areas.{{or|secondary source needed for these claims|date=February 2016}}

The senator was able to secure funding for the school in 2001. Then in 2004, legislation was passed in the state legislature to fund the college. The legislation, supported by Hawai'i State Representatives Chang, Evans, Herkes, Hale and others was aptly titled "Encouraging a College of Pharmacy at the University of Hawai'i at Hilo".<ref name="ENCOURAGING A COLLEGE OF PHARMACY AT THE UNIVERSITY OF HAWAII AT HILO.">{{cite web|url=http://www.capitol.hawaii.gov/session2004/status/HCR158.asp |archive-url=https://web.archive.org/web/20090619154733/http://www.capitol.hawaii.gov:80/session2004/status/HCR158.asp |dead-url=yes |archive-date=June 19, 2009 |title=Hawai'i State Legislature |accessdate=January 20, 2016 }}</ref> By the end of 2004, approximately $20 million was allotted to the development of the College of Pharmacy. Later that year, the search for a dean for the College of Pharmacy was approved by State Legislature.<ref name=Emergence>{{cite book|last1=Knehans|first1=Amy|last2=Morris|first2=Maggie|title=Emergence of the University of Hawai'i at Hilo College of Pharmacy|date=2012|isbn=9780615596259|url=http://pharmacy.uhh.hawaii.edu/news/cophistory.pdf|publisher=The University of Hawai‘i at Hilo College of Pharmacy.}}</ref> In 2011 Senator Inouye supported pharmacists by introducing the S.48 - Pharmacist Student Loan Repayment Eligibility Act of 2011, which helped student pharmacists afford their education.<ref name="S.48 - Pharmacist Student Loan Repayment Eligibility Act of 2011">{{cite web|url=https://www.congress.gov/bill/112th-congress/senate-bill/48/text| title=S.48 - Pharmacist Student Loan Repayment Eligibility Act of 2011 |accessdate=January 20, 2016}}</ref>

=== The first Dean ===

John Pezzuto (Ph.D.) was selected to be the founding dean of the new College of Pharmacy in Hilo. Pezzuto had been at Rutgers University, University of Medicine and Dentistry of New Jersey, Massachusetts Institute of Technology, University of Illinois at Chicago, and College of Pharmacy, Nursing and Health Sciences at Purdue University. His cancer research established his relationship with the Cancer Research Center of Hawai'i.<ref name=Emergence/>

=== Accreditation ===

The [[Accreditation Council for Pharmacy Education|ACPE]] granted the college accreditation in 2011.<ref name="Institute of Education Sciences">{{cite web|title=College Navigator: UH Hilo|url=http://nces.ed.gov/collegenavigator/?q=hilo&s=HI&id=141565#accred|website=National Center for Education Statistics|accessdate=February 20, 2016}}</ref>

=== Emblem ===

The school uses an emblem with a torch entwined by a snake superimposed on the outline of the volcano [[Kīlauea|Kilauea]]. The torch symbolizes enlightenment; combined the serpent, usually seen on the [[Rod of Asclepius]] or [[Bowl of Hygieia|Bowl of Hygeia]], this the symbol of medicine and pharmacy, respectively. The volcano pays homage to the home of the college and emphasizes the unique environment to learn and practice pharmacy.<ref name=Emergence/>

== Campus ==
The current campus consists of four main buildings located just above the main UH Hilo campus. Across the intersection is the [[Imiloa Astronomy Center of Hawaii|Imiloa Astronomy Center]]. One of the buildings houses laboratories for research projects that are undertaken by the faculty and students. The main lecture halls, LPLH1&2,<ref name=Emergence/> are named after J.M. Long for his donation to the school, and consist of two large rooms that each accommodate roughly 90 students, with a lounge and restrooms separating the halls. The campus also has a mock pharmacy, wherein students may practice counseling or participate in practical exams. This building also has faculty office spaces, a pharmacy laboratory for compounding lessons, and a sterile hood room to simulate IV preparations. The fourth main building has more office space for administration as well as study and breakout rooms.

The DKICP also has laboratory operations at the Pana'ewa Rainforest Zoo & Gardens, located roughly fifteen minutes south of Hilo. The campus also has several faculty members with offices at the Annex and the Hilo Medical Center, approximately five minutes from campus. The Hawai'i Island Family Health Center is also a partner with the campus and clinical pharmacy professors work on site.

In 2010, Governor Linda Lingle approved funding requests<ref name="Governor Lingle Senate Request">{{cite web|url=http://www.capitol.hawaii.gov/session2010/Bills/SR29_.pdf |archive-url=https://web.archive.org/web/20170114055450/http://www.capitol.hawaii.gov/session2010/Bills/SR29_.pdf |dead-url=yes |archive-date=January 14, 2017 |title=Governor Lingle Senate Request |accessdate=January 20, 2016 }}</ref> and the schematics for a permanent campus were introduced in 2011, the work of WCIT Architecture of Honolulu. Construction of the building was then delayed multiple times due to funding obstacles and stalls in legislation. However, as of 2016 the process is back in motion and the bids for construction are currently being discussed.

== Academics ==

=== Pharm.D. ===

The DKICP offers the [[Doctor of Pharmacy]] degree to students who complete the four-year program which consists of six semesters plus a fourth year dedicated to rotations. The first year of the program is primarily an introduction to important pharmacy topics, such as "Self-Care" and "Biostatistics". The second and third years cover "integrated therapeutics," which encompasses [[pathophysiology]], pharmacology of treatments, medicinal chemistry, and therapeutics. Other core classes are: "Communications for Pharmacists", "Healthcare Systems" and "Complementary Medicine". There are also electives, such as "veterinary medicine", and a full semester course that focuses on antibiotics.

During these first three years of schooling, introductory experiential courses are embedded throughout the semester and also during a rotational block in the summer each year. This allows students to gradually merge textbook learning and hands-on experience. The fourth and final year is dedicated to advanced experiential courses, which are divided into six-week blocks with four required core rotations and several elective rotations.

=== BAPS Program ===

The Bachelor of Arts in Pharmacy Studies (BAPS) is an option for students who are accepted to the school without completing the undergraduate degree.

=== Ph.D. Program ===

The [[Doctor of Philosophy|Ph.D.]] in Pharmaceutical Sciences is focused on "natural products discovery and development".<ref name=Programs>{{cite web|url=http://pharmacy.uhh.hawaii.edu/academics/graduate/ | title=DKICP Programs |accessdate=January 20, 2016}}</ref> This program takes full advantage of the fact that the DKICP is located on an island with a unique diverse flora.

=== Pre-Pharmacy Program ===

This is a preparatory program that guides undergraduate students toward becoming competitive candidates for any Doctor of Pharmacy program. The curriculum for the Pre-Pharmacy Program is about two to three years long and does not guarantee acceptance into DKICP.

=== M.S. Clinical Psychopharmacology ===

The [[Master of Science]] in Clinical Psychopharmacology (MSCP) degree is a two-year program involving both didactic and experiential courses. The program requires a doctoral degree in clinical psychology with an active license.

==Reputation & Community Involvement==
*Every year, students of the DKICP host a health fair for the community, providing free screening, educational booths on diseases and medications, counseling, vaccinations and more.<ref>{{cite web|last1=Kakugawa-Leong|first1=Alyson|title=DKICP hosts 7th Annual Health Fair at Prince Kuhio Plaza|url=http://hilo.hawaii.edu/news/press/release/1703|website=University of Hawai'i Press Release|accessdate=February 20, 2016}}</ref>
*The [[American Journal of Pharmaceutical Education]] published a review of available literature regarding rural health pharmacy education throughout the United States. In this review, it underlined the DKICP's ongoing efforts in improving "health care not only to rural areas but to all areas of the state".<ref>{{cite journal|last1=Thrasher|first1=Kim|last2=O'Connor|first2=Shanna K.|last3=Joyner|first3=Pamela U.|title=Rural Health in Pharmacy Curricula|journal=American Journal of Pharmaceutical Education|date=November 12, 2012|volume=76 (9)|issue=180|url=http://www.ajpe.org/doi/pdf/10.5688/ajpe769180|accessdate=January 29, 2016}}</ref>
*With the opening of the DKICP, [[pharmacy residency]] programs in Hawai'i have doubled to twelve total resident positions throughout the state.<ref>{{Cite journal|title = The Daniel K. Inouye College of Pharmacy Scripts|journal = Hawai'i Journal of Medicine & Public Health|date = 2015-05-01|issn = 2165-8218|pmc = 4443620|pmid = 26019990|pages = 185–190|volume = 74|issue = 5|first = Carolyn|last = Ma|first2 = Sheri|last2 = Tokumaru|first3 = Roy|last3 = Goo|first4 = Anita|last4 = Ciarleglio}}</ref>
*Beyond the obvious health benefits of having a clinical college in the area, a study was conducted to gauge the positive ''economic'' impact of the school on the local community which it estimated to be a $50 million stimulus. This was considered an underestimate upon further review because it did not take into account extramural funding, such as awards and grants earned by the DKICP, which "has exceeded $40 million".<ref>{{Cite journal|title = The Daniel K. Inouye College of Pharmacy Scripts|journal = Hawai'i Journal of Medicine & Public Health|date = 2015-03-01|issn = 2165-8218|pmc = 4363934|pmid = 25821655|pages = 120–128|volume = 74|issue = 3|first = John M|last = Pezzuto|first2 = Carolyn SJ|last2 = Ma|first3 = Carolyn|last3 = Ma}}</ref>
*Between 2011 and 2012, the Narcotics Enforcement Division, which handles Hawaii’s Uniform [[Controlled Substances Act|Controlled Substance]] Act, and the DKICP partnered and collected more than 3.5 metric tons of unused or unwanted medications that needed to be disposed of safely.<ref>{{Cite journal|title = Drug Take Back in Hawai‘i: Partnership Between the University of Hawai‘i Hilo College of Pharmacy and the Narcotics Enforcement Division|journal = Hawai'i Journal of Medicine & Public Health|date = 2014-01-01|issn = 2165-8218|pmc = 3901169|pmid = 24470984|pages = 26–31|volume = 73|issue = 1|first = Carolyn S|last = Ma|first2 = Forrest|last2 = Batz|first3 = Deborah Taira|last3 = Juarez|first4 = Lani C|last4 = Ladao}}</ref> These medications would have otherwise been disposed of down toilets, drains and dumps.

== References ==

{{Reflist}}

{{University of Hawaii at Hilo}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:2007 establishments in Hawaii]]
[[Category:Education in Hawaii County, Hawaii]]
[[Category:Educational institutions established in 2007]]
[[Category:Pharmacy schools in Hawaii]]
[[Category:University of Hawaii]]