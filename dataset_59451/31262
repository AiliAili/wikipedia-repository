{{for|his nephew, an American football player|William Goebel (American football)}}
{{Use mdy dates|date=January 2012}}
{{Infobox Governor
|name        = William Justus Goebel
|image       = William Goebel circa 1889.jpg
|order       = 34th
|office      = Governor of Kentucky
|term_start  = January 31, 1900
|term_end    = February 3, 1900
|lieutenant  = J. C. W. Beckham
|predecessor = [[William S. Taylor]]
|successor   = [[J. C. W. Beckham]]
|birth_date  = {{birth date|1856|01|04}}
|birth_place = [[Carbondale, Pennsylvania]]<ref name=powell>{{cite book |last=Powell |first=Robert A. |title=''Kentucky Governors'' |publisher=Kentucky Images |location=Frankfort, Kentucky |year=1976 |oclc=2690774}}</ref>
|death_date  = {{death date and age|mf=yes|1900|02|03|1856|01|04}}
|death_place = [[Frankfort, Kentucky]]
|party       = [[Democratic Party (United States)|Democrat]]
|profession  = Lawyer
|relations   = [[Justus Goebel]], brother
|order2      = [[President Pro Tempore of the Kentucky Senate]]
|term_start2 = 1896
|term_end2   = 1900
|predecessor2=
|successor2  =
|signature   = William Goebel Signature.svg
}}
'''William Justus Goebel''' (January 4, 1856 – February 3, 1900)<!--Many sources incorrectly list his death as in 1899--> was an American [[Politics of the United States|politician]] who served as the [[List of Governors of Kentucky|34th]] [[Governor of Kentucky]] for four days in 1900 after having been mortally wounded by an assassin the day before he was sworn in. Goebel remains the only [[Governor#United States|state governor]] in the United States to be assassinated while in office.<ref name=kye-assassination>{{cite book |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], [[Lowell H. Harrison]], and [[James C. Klotter]] |title=''The Kentucky Encyclopedia'' |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |chapter=Goebel Assassination}}</ref>

A skilled politician, Goebel was well able to broker deals with fellow lawmakers, and equally able and willing to break the deals if a better deal came along. His tendency to use the state's [[political machine]]ry to advance his personal agenda earned him the nicknames "Boss Bill", "the [[Kenton County, Kentucky|Kenton]] King", "Kenton Czar", "King William I", and "William the Conqueror".<ref name=klotter>{{cite book |authorlink=James C. Klotter |last=Klotter |first=James C. |title=''William Goebel: The Politics of Wrath'' |year=1977 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-0240-5 |url=https://books.google.com/books?id=cw-1wFeQoIgC}}</ref>

Goebel's abrasive personality made him many political enemies, but his championing of [[populism|populist]] causes, like railroad regulation, also won him many friends. This conflict of opinions came to a head in the Kentucky gubernatorial election of 1899. Goebel, a [[Democratic Party (United States)|Democrat]], divided his party with self-serving political tactics at a time when Kentucky [[Republican Party (United States)|Republicans]] were finally gaining strength, having elected the party's first governor four years previously. These dynamics led to a close contest between Goebel and [[William S. Taylor]]. In the politically chaotic climate that resulted, Goebel was assassinated. Everyone charged in connection with the murder was either acquitted or eventually pardoned, and the identity of his assassin remains uncertain.

==Early life==
Wilhelm Justus Goebel was born January 4, 1856, in [[Albany Township, Bradford County, Pennsylvania]],<ref>Heverly, Clement F., ''History and Geography of Bradford County Pennsylvania, 1615–1924''. Towanda, PA: Bradford County Historical Society. 1926. p 469</ref> the son of Wilhelm and Augusta (Groenkle) Goebel, immigrants from [[Hanover]], Germany. The first of four children, he was born two months premature and weighed less than three pounds. His father served as a private in Company B, 82nd Pennsylvania Infantry Regiment during the [[American Civil War]], and Goebel's mother raised her children alone, teaching them much about their [[Germans|German]] heritage. Wilhelm spoke only German until the age of six, but embraced the culture of his birth country as well, adopting the [[Anglicisation#Proper names|English spelling]] of his name.<ref name=klotter />

Discharged from the army in 1863, Goebel's father moved his family to [[Covington, Kentucky]]. William attended school in Covington and was then apprenticed to a jeweler in [[Cincinnati]], Ohio. After a brief time at Hollingsworth Business College, he became an apprentice in the law firm of [[John W. Stevenson]], who had served as governor of Kentucky from 1871 to 1877. Goebel eventually became Stevenson's partner and executor of his estate.<ref name=klotter /> Goebel graduated from [[Cincinnati Law School]] in 1877, then enrolled at [[Kenyon College]] in [[Gambier, Ohio]], but withdrew to support his family on the death of his father. Goebel was in private practice for several years, before partnering with Kentucky state representative [[John G. Carlisle]] for five years. He then rejoined Stevenson in Covington as a partner.<ref name=powell />

==Personal characteristics==
Goebel was never known as a particularly genial person in public. He belonged to few social organizations, and greeted none but his closest friends with a smile or handshake.  He was rarely linked romantically with a woman,<ref name=klotter /> and is the only governor of Kentucky who never married.<ref name=powell /> His physical features bespoke his taciturn nature. Journalist [[Irvin S. Cobb]] remarked that Goebel's appearance was "reptilian", while others commented on his contemptuous lips, sharp nose, and humorless eyes. Neither was Goebel a gifted public speaker, eschewing flowery imagery and relying on his deep, powerful voice and forceful delivery to drive home his points.<ref name=klotter />

While lacking in the social qualities common to politicians, one characteristic served Goebel well in the political arena{{spaced ndash}}his intellect. Goebel was well-read, and supporters and opponents both conceded that his mental prowess was impressive. Cobb concluded that he had never been more impressed with a man's intellect than he had been with Goebel's.<ref name=klotter />

==Political career==
[[File:William Goebel statue.png|thumb|left|200px|Statue of Goebel in front of the [[Old State Capitol (Kentucky)|Old State Capitol]] in Frankfort]]
In 1887, James W. Bryan vacated his seat in the [[Kentucky Senate]] to pursue the office of [[Lieutenant Governor of Kentucky|lieutenant governor]]. Goebel decided to seek election to the vacant seat representing the Covington area. His platform of railroad regulation and championing labor causes, combined with the influence of Stevenson, his former partner, should have given Goebel an easy victory, but this was not to be. A third political party, the [[Labor Party (United States, 19th century)|Union Labor party]], had risen to power in the area with a platform similar to Goebel's. However, while Goebel had to stick close to his allies in the Democratic party, the Union Labor party courted the votes of both Democrats and Republicans, and made the election close{{spaced ndash}}decided in Goebel's favor by a mere fifty-six votes.<ref name=klotter />

With only the two years remaining in former senator Bryan's term to distinguish himself before a re-election bid, Goebel took aim at a large and popular target: the [[Louisville and Nashville Railroad]]. A proposal from pro-railroad legislators in the Kentucky House of Representatives to abolish Kentucky's Railroad Commission was passed and sent to the Senate. Senator [[Cassius Clay (1810–1903)|Cassius M. Clay]] responded by proposing a committee to investigate lobbying by the railroad industry. Goebel served on the committee, which uncovered significant violations by the railroad lobby.<ref name=hood>{{cite book |last=Hood |first=Fred J. |title=''Kentucky: Its History and Heritage'' |chapter=Goebel's Campaign for Railroad Regulation, 1888–1900 |publisher=Forum Press |location=St. Louis, Missouri |year=1978 |isbn=0-88273-019-3}}</ref> Goebel also helped defeat the bill to abolish the Railroad Commission in the Senate. These actions made him a hero in his district. He ran for a full term as senator unopposed in 1889, and won another term in 1893 by a three-to-one margin over his Republican opponent.<ref name=klotter />

In 1890, Goebel was a delegate to Kentucky's fourth [[Constitutional convention (political meeting)|constitutional convention]],<ref name=kye>{{cite book |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], [[Lowell H. Harrison]], and [[James C. Klotter]] |title=''The Kentucky Encyclopedia'' |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |chapter=Goebel, William|chapter-url=http://www.kyenc.org/entry/g/GOEBE01.html}}</ref> which produced the current [[Kentucky Constitution]].<ref>{{cite book |title=Kentucky Government: Informational Bulletin No. 137 (Revised) |chapter=Constitutional Background|publisher=Kentucky Legislative Research Commission |location=Frankfort, Kentucky |date=February 2003}}</ref> Despite the high honor of being chosen as a delegate, Goebel showed little interest in participating in the process of creating a new constitution. The convention was in session for 250 days; Goebel was present for just 100 of them.<ref name=klotter /> He did, however, successfully secure the inclusion of the Railroad Commission in the new constitution. As a constitutional entity, the Commission could only be abolished by an amendment ratified by popular vote. This would effectively protect the Commission from ever being unilaterally dismantled by the [[Kentucky General Assembly|General Assembly]].<ref name=hood />

===Duel with John Sanford===
[[File:Goebel William.jpg|thumb|Portrait by George Debereiner]]
In 1895, Goebel engaged in what many observers considered a [[duel]] with [[John Lawrence Sanford]]. Sanford, a former [[Confederate States of America|Confederate]] general staff officer turned banker, had clashed with Goebel before. Goebel's successful campaign to remove tolls from some of Kentucky's [[Toll road|turnpikes]] had cost Sanford a good deal of money. It was widely believed that Sanford later blocked Goebel's appointment to the Kentucky Court of Appeals, then the highest court in the state.<ref name=woodson>{{cite book |last=Woodson |first=Urey |authorlink=Urey Woodson |title=The First New Dealer, William Goebel: His origin, ambitions, achievements, his assassination, loss to the state and nation; the story of a great crime |publisher=The Standard Press |location=Louisville, Kentucky |year=1939}}</ref> In response to this, Goebel had written an article in a local newspaper referring to Sanford as "[[Gonorrhea]] John".<ref name=mcqueen>{{cite book |last=McQueen |first=Keven |others=Ill. by Kyle McQueen |title=''Offbeat Kentuckians: Legends to Lunatics'' |chapter=William Goebel: Assassinated Governor |year=2001 |publisher=McClanahan Publishing House |location=Kuttawa, Kentucky |isbn=0-913383-80-5 }}</ref>

The duel occurred as Goebel and two of his acquaintances went to cash a check in Covington. Goebel suggested they avoid Sanford's bank, but Sanford, standing outside the bank, engaged the trio in conversation before they could cross the street to another bank. As Sanford greeted Goebel's friends, he offered his left hand, his right remaining on a pistol in his pocket. Goebel, noticing this and being likewise armed, clutched the revolver in his own pocket. Sanford asked Goebel, "I understand that you assume authorship of that article?"; "I do", replied Goebel. Witnesses agree that both men fired at each other, but none was sure who fired first. Goebel was uninjured, the bullet passing through his coat and ripping his trousers; Sanford was struck in the head and died five hours later.<ref name=woodson /> Goebel was acquitted, pleading self-defense, but the incident would haunt his political career.<ref name=klotter /> The acquittal was also significant because of prohibitions against duelling in the Kentucky Constitution. If Goebel had been convicted, he would have been ineligible to hold any public office.<ref name=kyconst>{{cite web |url=http://lrc.ky.gov/lrcpubs/IB59.pdf |title=The Constitution of the Commonwealth of Kentucky: Informational Bulletin No. 59 |publisher=Kentucky Legislative Research Commission |date=October 2005 |accessdate=October 9, 2007 |format=PDF}}</ref>

===Goebel Election Law===
Democrats, who controlled the General Assembly, felt that county election commissioners had been unjust in selecting local election officials, and that this injustice had contributed to the election of [[Republican Party (United States)|Republican]] governor [[William O. Bradley]] in 1895 and Republican president [[William McKinley]] in 1896. Goebel proposed a bill, known as the "Goebel Election Law", which passed along sharp party lines and over Governor Bradley's veto, created a three-member state election commission, appointed by the General Assembly, to select county election commissioners. This system proved to be just as manipulable as the one it replaced, allowing the Democratically controlled General Assembly to appoint fellow Democrats to the election commission.<ref name=hood />

Many voters decried the bill as a self-serving attempt by Goebel to increase his political power, and the election board remained a controversial issue until its abolition in a special session of the legislature in 1900.<ref>{{cite book |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], [[Lowell H. Harrison]], and [[James C. Klotter]] |title=''The Kentucky Encyclopedia'' |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |chapter=Goebel Election Law|chapter-url=http://www.kyenc.org/entry/g/GOEBE03.html}}</ref>
Despite rising to the office of [[President Pro Tempore of the Kentucky Senate|President Pro Tempore]] in 1896, Goebel became the subject of much opposition from constituencies of both parties in Kentucky after the passage of the law.

===Gubernatorial election of 1899===
{{main|Kentucky gubernatorial election, 1899}}
Three men sought the Democratic nomination for governor at the 1899 party convention in [[Louisville, Kentucky|Louisville]]{{spaced ndash}}Goebel, [[Parker Watkins Hardin|Wat Hardin]], and [[William Johnson Stone|William J. Stone]]. When Hardin appeared to be the front-runner for the nomination, Stone and Goebel agreed to work together against him. Stone's supporters would back whomever Goebel picked to preside over the convention. In exchange, half the delegates from Louisville, who were pledged to Goebel, would vote to nominate Stone for governor. Goebel would then drop out of the race, but would name many of the other officials on the ticket. As word of the plan spread, Hardin dropped out of the race, believing he would be beaten by the Stone–Goebel alliance.<ref name=klotter />

Goebel took a calculated risk by breaking the agreement once his choice was installed as presiding officer. Hardin, seeing that Stone had been betrayed and hoping he might now be able to secure the nomination, re-entered the contest. Several chaotic ballots resulted in no clear majority for anyone, and Goebel's hand-picked chairman announced the man with the lowest vote total in the next canvass would be dropped. It turned out to be Stone. This put Stone's backers in a difficult position. They were forced to choose between Hardin, who was seen as a pawn of the railroads, or Goebel, who had turned against their man. Enough of them sided with Goebel to give him the nomination.<ref name=klotter /> Goebel's tactics, while not illegal, were unpopular and divided the party.<ref name=kye-musichall>{{cite book |editor=Kleber, John E. |others=Associate editors: [[Thomas D. Clark]], [[Lowell H. Harrison]], and [[James C. Klotter]] |title=''The Kentucky Encyclopedia'' |year=1992 |publisher=The University Press of Kentucky |location=Lexington, Kentucky |isbn=0-8131-1772-0 |chapter=Music Hall Convention}}</ref> A disgruntled faction calling themselves the "Honest Election Democrats" held a separate convention in [[Lexington, Kentucky|Lexington]] and nominated [[John Y. Brown (1835-1904)|John Y. Brown]] for governor.<ref name=hood />

Republican [[William S. Taylor]] defeated both Democratic candidates in the general election, but his margin over Goebel was only 2,383 votes.<ref name=mcqueen /> Democrats in the General Assembly began making accusations of voting irregularities in some counties, but in a surprise decision, the Board of Elections created by the Goebel Election Law and manned by three hand-picked Goebel Democrats, ruled 2–1 that the disputed ballots should count, saying the law gave them no legal power to reverse the official county results and that under the Kentucky Constitution the power to review the election lay in the General Assembly. The Assembly then invalidated enough Republican ballots to give the election to Goebel. The Assembly's Republican minority was incensed, as were voters in traditionally Republican districts. For several days, the state hovered on the brink of a possible civil war.<ref name=klotter /><ref name=hood />

==Assassination and aftermath==
[[File:HW Goebel Assassination.png|thumb|right|250px|Sketch of the assassination in ''[[Harper's Weekly]]'', 1900]]
While the election results remained in dispute, Goebel, despite being warned of a rumored assassination plot against him, walked flanked by two bodyguards to the [[Old State Capitol (Kentucky)|Old State Capitol]] on the morning of January 30, 1900. Reports conflict about what happened, but some five or six shots were fired from the nearby State Building, one striking Goebel in the chest and wounding him seriously. Taylor, serving as governor pending a final decision on the election, called out the [[militia]] and ordered the General Assembly into a special session, not in Frankfort, but in [[London, Kentucky]], a Republican area.<ref name=mcqueen /> The Republican minority obeyed the call and went to London. Democrats resisted the move, many going instead to [[Louisville, Kentucky|Louisville]]. Both groups claimed authority, but the Republicans were too few to muster a [[quorum]].<ref name=woodson />

[[File:Goebel fell plaque.png|thumb|left|A plaque in front of the [[Old State Capitol (Kentucky)|Old State Capitol]] marks where Goebel fell after being shot.]]
The day after being shot, the dying Goebel was sworn in as governor. In his only act, Goebel signed a proclamation to dissolve the militia called up by Taylor, which was ignored by the militia's Republican commander. Despite the care of 18 physicians, Goebel died the afternoon of February 3, 1900.<ref>{{cite news |title=Goebel Dies At 6:45 P. M. |url=http://pqasb.pqarchiver.com/ajc_historic/access/548505622.html?dids=548505622:548505622&FMT=ABS&FMTS=ABS:AI&date=Feb+04,+1900&author=&pub=The+Atlanta+Constitution&desc=GOEBEL+DIES+AT+6:45+P.+M.;+BROTHER+ARIVES+TOO+LATE&pqatl=google |quote=The bullet fired by an unknown assassin last Tuesday morning ended the life of Governor Goebel at 6:45 o'clock this evening. The only persons present at the deathbed were Governor Goebel's sister, Mrs.&nbsp;... attendance at Governor Goebel's bedside Justus Goebel another brother who has been hurrying&nbsp;... He died at 6:45&nbsp;... |newspaper=[[Atlanta Constitution]] |date=February 4, 1900 |accessdate=December 29, 2010 }}</ref> Journalists recalled his [[last words]] as "Tell my friends to be brave, fearless, and loyal to the common people." Skeptic [[Irvin S. Cobb]] uncovered another story from some in the room at the time. On having eaten his last meal, the governor supposedly remarked "Doc, that was a damned bad oyster." In respect of Goebel's displeasure with the Louisville and Nashville Railroad, the governor's body was transported not by the L&N direct line, but circuitously from his hometown of Covington north across the [[Ohio River]] to Cincinnati, and then south to Frankfort on the [[Queen and Crescent Railroad]].<ref name=klotter />

===Resolution of the election===
With Goebel dead, tensions began to ease. The idea of Goebel's [[Lieutenant Governor of Kentucky|lieutenant governor]], [[J. C. W. Beckham]], as governor was more palatable to much of the opposition than civil war in the state, though many of them may have preferred war to a Goebel governorship. After a lengthy meeting, a bipartisan compromise was drafted which would have ended the matter. The terms called for Republican recognition of Goebel's rightful election (and Beckham's subsequent right to govern). Republicans would also remove the militia from Frankfort. Democrats would, in turn, extend immunity to any Republican official found to have ties to the assassination, stop contesting elections for other state offices, and work to pass a nonpartisan election reform bill. The agreement needed only Taylor's signature to become effective. Unwilling to relinquish his office, Taylor balked.<ref name=klotter />

Compromise having been exhausted, both sides agreed to litigate the matter. The Kentucky Court of Appeals found that the General Assembly had acted legally in declaring Goebel the winner of the election. That decision was appealed to the [[Supreme Court of the United States]]. Arguments were presented in the case (''[[Taylor v. Beckham]]'') on April 30, 1900, and on May 21, the justices decided 8–1 not to hear the case, allowing the Court of Appeals' decision to stand.<ref>{{cite web |url=http://laws.findlaw.com/us/178/548.html |title=Taylor v. Beckham, 178 U.S. 548 (1900) |publisher=FindLaw.com |accessdate=March 6, 2007}}</ref> The lone dissension was that of Associate Justice [[John Marshall Harlan]], a Kentucky native.<ref name=woodson /> and member of the [[Republican Party (United States)|Republican Party]].

===Trials and investigations===
[[File:Gov William Goebel grave.jpg|thumb|Monument in Frankfort Cemetery]]
During the ensuing assassination investigation, suspicion naturally focused on deposed governor Taylor, who fled to [[Indianapolis]], Indiana under the looming threat of indictment.<ref name=kye /> The governor of [[Indiana]] refused to extradite Taylor, and he was thus never questioned about his knowledge of the plot to kill Goebel. Taylor became a successful lawyer in Indiana, and was pardoned in 1909 by Beckham's successor, Republican [[Augustus E. Willson]].<ref name=mcqueen />

Sixteen people, including Taylor, were eventually indicted in the assassination of Governor Goebel. Three accepted immunity from prosecution in exchange for testimony. Only five ever went to trial, two of those being acquitted.<ref name=kye-assassination /> Convictions were handed down against Taylor's Secretary of State [[Caleb Powers]], Henry Youtsey, and Jim Howard. The prosecution charged that Powers was the mastermind, having a political opponent killed so that his boss, Governor Taylor, could stay in office. Youtsey was an alleged intermediary, and Howard, who was said to have been in Frankfort to seek a pardon from Taylor for the killing of a man in a family feud,<ref name=mcqueen /> was accused of being the actual assassin.<ref name=kye-assassination />

The trials were fraught with irregularities. All three judges were pro-Goebel Democrats,<ref name=mcqueen /> and at one point the juror pool of 368 people was found to have only eight Republicans. Republican appeals courts overturned Powers' and Howard's convictions, though Powers was tried three more times, resulting in two convictions and a [[hung jury]] and Howard was tried and convicted twice more. Both men were pardoned in 1908 by Governor Augustus E. Willson.

Youtsey, who received a [[life imprisonment|life sentence]], did not appeal, but after two years in prison, he turned [[state's evidence]]. In Howard's second trial, Youtsey claimed that ex-governor Taylor had discussed an assassination plot with Youtsey and Howard. He backed the prosecution's claims that Taylor and Powers worked out the details, he acted as an intermediary, and Howard fired the shot. On cross examination, the defense pointed out contradictions in the details of Youtsey's story, but Howard was still convicted. Youtsey was paroled in 1916 and was pardoned in 1919 by Democratic governor [[James D. Black]].<ref name=kye-assassination /><ref name=klotter /><ref name=mcqueen />

Most historians agree that the assassin of Governor Goebel will never be conclusively identified.<ref name=kye-assassination />

==Legacy==
Goebel Avenue in [[Elkton, Kentucky]] and Goebel Park in [[Covington, Kentucky]] are named in honor of the late governor.<ref>{{cite book | url=https://books.google.com/books?id=IuGCoLRCN-kC&lpg=PP1&pg=PA155#v=onepage&q&f=false | title=The WPA Guide to Kentucky | publisher=University Press of Kentucky | date=1996 | accessdate=24 November 2013 | author=Federal Writers' Project | page=155}}</ref>

==See also==
* [[History of Kentucky]]
* [[List of assassinated American politicians]]

==References==
{{Reflist|30em}}

==Further reading==
{{Commons category}}
*{{cite book |last=Cobb |first=Irvin S. |authorlink=Irvin S. Cobb |title=Exit Laughing |publisher=The Bobbs-Merrill Company |year=1941}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[William S. Taylor]]}}
{{s-ttl|title=[[Governor of Kentucky]]|years=1900}}
{{s-aft|after=[[J. C. W. Beckham]]}}
{{s-end}}

{{Governors of Kentucky}}
{{Featured article}}

{{Authority control}}

{{DEFAULTSORT:Goebel, William}}
[[Category:1856 births]]
[[Category:1900 deaths]]
[[Category:People from Carbondale, Pennsylvania]]
[[Category:American people of German descent]]
[[Category:Kentucky Democrats]]
[[Category:Governors of Kentucky]]
[[Category:Kentucky State Senators]]
[[Category:Assassinated American politicians]]
[[Category:Duellists]]
[[Category:People murdered in Kentucky]]
[[Category:Unsolved murders in the United States]]
[[Category:Populism]]
[[Category:Deaths by firearm in Kentucky]]
[[Category:1900 murders in the United States]]
[[Category:Murdered lawyers]]
[[Category:Kenyon College alumni]]
[[Category:University of Cincinnati alumni]]
[[Category:Democratic Party state governors of the United States]]
[[Category:19th-century American politicians]]