{{Infobox journal
| title = Herpetologica
| cover =
| discipline = [[Herpetology]]
| abbreviation = Herpetologica
| editor = Stephen J. Mullin
| publisher = [[Allen Press]] on behalf of the [[Herpetologists' League]]
| country = United States
| frequency = Quarterly
| history = 1936-present
| impact = 1.140
| impact-year = 2014
| openaccess = 
| website = http://www.hljournals.org/loi/herp
| link1 = http://www.bioone.org/loi/herp
| link1-name = Online access at [[BioOne]]
| JSTOR = 00180831
| ISSN = 0018-0831
| eISSN = 1938-5099
| OCLC = 875838938
| LCCN = 41018025
| CODEN = HPTGAP
}}
'''''Herpetologica''''' is a quarterly [[peer-reviewed]] [[scientific journal]] published by  [[Allen Press]] on behalf of the [[Herpetologists' League]] covering [[herpetology]]. It was established in 1936 by [[Chapman Grant]]. The [[editor-in-chief]] is Stephen J. Mullin ([[Eastern Illinois University]]).

== Abstracting and indexing information ==
The journal is abstracted and indexed in:
*[[BIOSIS Previews]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-06-04}}</ref>
*[[Current Contents]]/Agriculture, Biology & Environmental Sciences<ref name=ISI/>
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-06-04}}</ref>
*[[The Zoological Record]]<ref name=ISI/>
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.140, ranking it 69th out of 154 journals in the category "Zoology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Zoology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{reflist}}

==External links==
*{{Official website|http://www.hljournals.org/loi/herp}}

[[Category:Herpetology journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1936]]


{{zoology-journal-stub}}