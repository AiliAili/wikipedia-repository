{{Multiple issues|
{{Orphan|date=October 2015}}
{{original research|date=October 2015}}
}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}

Before [[Charles Darwin]] and his groundbreaking [[theory of evolution]], primates were mainly used as caricatures of human nature. Although comparisons between man and animal are rather old, it was not until the findings of science that mankind recognised itself as a part of the animal kingdom.<ref>Cf. Andreas Blühm and Louise Lippincott, ''Tierschau. Wie unser Bild vom Tier entstand'' (Köln: Wallraf-Richartz-Museum & Fondation Corboud, 2007), 14.</ref> [[Caricatures]] of Darwin and his evolutionary theory reveal how closely science was intertwined with both the arts and the public during the [[Victorian era]].<ref>Diana Donald, introduction to ''Endless Forms. Charles Darwin, Natural Science and the Visual Arts'', ed. Diana Donald and Jane Munro (Cambridge: Fitzwilliam Museum, 2009), 1.</ref> They display the general perception of Darwin, his “monkey theory”<ref>Julia Voss, “Monkeys, Apes and Evolutionary Theory: from Human Descent to King Kong,” ''Endless Forms. Charles Darwin, Natural Science and the Visual Arts'', ed. Diana Donald and Jane Munro (Cambridge: Fitzwilliam Museum, 2009), 222.</ref> and apes in 19th-century England.
[[File:Man is But a Worm.jpg|thumb|upright=1.2|Shortly after the Publication of ''On the Origin of Species'' Linley Sambourne's cartoon ''Man Is But a Worm'' was published in ''Punchs'''s Almanack. It depicts the evolution of the worm into the human – in this case, the English gentleman – as a means of ridiculing Darwin's theory.]]

== Image description ==
[[File:Faustin Betbeder The London Sketch-Book 1874 Prof. Darwin.jpg|thumb|left|upright=0.8|''Prof. Darwin'' in ''Figaro'''s London Sketch Book of Celebrities, 18 February 1874.]]

=== ''Professor Darwin'' ===
We see Darwin portrayed as a monkey with his own human head. He holds a mirror up to another monkey which is sitting next to him. It seems as if he would invite the monkey to ponder over himself and his existence. This is underlined by the two accompanying quotations of [[Shakespeare]]: "This is the ape of form" (from: [[Love's Labour's Lost]], act 5, scene 2) and "Some four or five descents since" ([[All's Well that Ends Well]], act 3, scene 7). Darwin's facial expression seems to encourage the monkey to recognise their common ancestry. The ape, in turn, looks into the mirror and tries to touch the reflection to literally grasp Darwin's suggestion and to assure himself of the authenticity of their kinship.{{citation needed|date=October 2015}}

=== ''Man Is But a Worm'' ===
[[Linley Sambourne]] drew a "wild evolutionary polonaise"<ref>Julia Voss, "Variieren und Selektieren: Die Evolutionstheorie in der englischen und deutschen illustrierten Presse im 19. Jahrhundert,” ''Darwin. Kunst und die Suche nach den Ursprüngen'', ed. Pamela Kort and Max Hollein (Köln: Wienand, 2009), 250.</ref>  which spirals up from C H A O S and ends in the English gentleman holding a cylinder. Among the stages in the process are the earthworm, the monkey and the cave man. Clocks are displayed in the background; the path on which the evolution proceeds is labelled as "times meter" both indicating that the evolution is depicted in time lapse. Darwin is enthroned next to the gentleman and seems to watch the whole development. Thereby he resembles one of the figures of [[Michelangelo]]'s ceiling fresco in the [[Sistine Chapel]].<ref name="CfVoss">Cf. Voss, "Variieren und Selektieren," 250.</ref>

== Interpretation ==

=== ''Professor Darwin'' ===
This caricature offers various starting points for an art-historical analysis. It was published three years after Darwin's work ''[[The Descent of Man]]'' (1871). Here, Darwin finally takes a stand and argues that humans and monkeys share a common ancestor. In the caricature, however, this view is put into question. Moreover, it goes back on the widespread assumption that humans exhibit certain animal features – the ape as a mirror for humankind so to speak. In this respect the caricature stands also in the tradition of [[vanitas]] which is symbolised by the hand mirror reflecting human stupidity. The fact that the ape-like Darwin is holding the mirror and not the real ape shows that Darwin and his theory should be ridiculed. Darwin himself has acknowledged that "[he] has given man a pedigree of prodigious length, but not, it may be said, of noble quality."<ref>Charles Darwin, ''The Descent of Man, and Selection in Relation to Sex'' (New York: D. Appleton, ²1874), 165.</ref> Consequently, the ape is not enhanced in status through his kinship with man.{{citation needed|date=October 2015}}
After the publication of ''The Descent of Man'' Darwin was increasingly identified with the theory of evolution although his friend [[Thomas Henry Huxley]] was the first to put it forward. As a result, Darwin himself was considered more and more as a suitable object to caricature. The cover of the French satirical magazine ''La Petite Lune'' is a telling example of the paradigmatic representation of Darwin in contemporary cartoons and caricatures.<ref>Cf. Janet Browne, "Darwin in Caricature: A Study in the Popularization and Dissemination of Evolution,” ''Proceedings of the American Philosophical Society'' 145 (2001): 506.</ref> [[File:Darwin as monkey on La Petite Lune.jpg|thumb|upright=0.9|Front page of the French satirical magazine ''La Petite Lune'' by André Gill (1871?).]]

=== ''Man Is But a Worm'' ===
Sambourne's caricature deals in a playful manner with the topics "evolution" and "descent of man." The title alludes to a publication of Darwin entitled ''The Formation of Vegetable Mould through the Action of Worms, with Observations of their Habits'' which was issued in October 1881.<ref name="CfVoss" /> In the caricature, evolution is associated with metamorphosis and portrayed as a gradual process which inevitably leads to advancement. This, however, conveys a reduced if not false picture of Darwin's theory in which competition, hereditary transmission, coincidence and selection play a major role.<ref>Cf. Bernd Herkner, "Die Welt des Charles Darwin," ''Darwin. Kunst und die Suche nach den Ursprüngen'', ed. Pamela Kort and Max Hollein (Köln: Wienand, 2009), 259.</ref> Besides, these circular images of evolutionary progression form a striking contrast to Darwin’s own linear and branching evolutionary trees. In ''[[The Origin of Species]]'' Darwin "took pains to emphasize that evolution was neither progressive nor circular."<ref>Browne, "Darwin in Caricature," 501.</ref>

That the earthworm is transformed into a monkey and not into another animal might indicate that the thesis of a kinship between ape and human received a wider acceptance among the British public (at this point in time ''The Descent of Man'' was published 10 years ago). Still, the monkey is depicted as the underdeveloped version of a human. At the same time, however, the "superior" human being or rather the English gentleman is ridiculed: he too is descended from an earthworm.{{citation needed|date=October 2015}}

== Precursors and reception ==
Precursors for the depiction of anthropomorphic animals were the works of [[Jean Ignace Isidore Gérard Grandville|Grandville]] who portrayed individuals for example in ''Les Métamorphoses du jour'' (1828–29) with the bodies of men and faces of animals. The caricatures of [[Charles Philipon]] can also be considered as a role model for the illustrators of ''[[Punch (magazine)|Punch]]'' and other English satirical magazines.<ref>Cf. Voss, "Variieren und Selektieren," 254.</ref> The cyclical depiction of Darwin's evolutionary theory might have been modelled on the wood engravings of the illustrator [[Charles H. Bennett (illustrator)|Charles H. Bennett]] which show transformations of humans into immobile objects and vice versa.{{citation needed|date=October 2015}}

The caricatures provide not only insights into the public perception of Darwin's evolutionary theory but also played an essential part in its dissemination and popularisation.<ref>Cf. Browne, "Darwin in Caricature," 508-09.</ref> During the 1860s and 1870s the kinship between ape and man was doubted more frequently than in later decades where this idea received a wider acceptance. Today we meet the evolutionary theory on T-shirts in the form of [[graphic arts]]. Quite often, it is the lifestyle of modern man that is ridiculed. The monkey is now only depicted as the underdeveloped stage of the human and does not function as the caricature of human behaviour any more. Likewise, Darwin has lost his prominent position as a motif for caricatures.{{citation needed|date=October 2015}}

== Origin ==
* The caricature ''Prof. Darwin'' was published on 18 February 1874 three years after the publication of Darwin's seminal work ''The Descent of Man'' in ''Figaro'''s London Sketch Book of Celebrities. The artist is unknown.{{citation needed|date=October 2015}}
* ''Man Is But a Worm'', a caricature by Edward Linley Sambourne, was printed in ''Punch'''s Almanack for 1882 on 6 December 1881.{{citation needed|date=October 2015}}
* A drawing done by Edward Linley Sambourne references Darwin. This drawing was  It is titled, "Man is But a Worm." On December 6, 1881, this drawing was put into Punch's Almanac. <ref>{{cite web|last1=Tucker|first1=Jennifer|title=What our most famous evolutionary cartoon gets wrong - The Boston Globe|url=https://www.bostonglobe.com/ideas/2012/10/27/what-our-most-famous-evolutionary-cartoon-gets-wrong/drKMD5121W6EUxXJ4pF0YL/story.html|website=BostonGlobe.com|publisher=© 2016 BOSTON GLOBE MEDIA PARTNERS, LLC|accessdate=October 29, 2016}}</ref>
== Sources ==
{{reflist}}

== See also ==
* [[Reactions to On the Origin of Species]]
* [[Portraits of Charles Darwin]]

== Further reading ==
* Browne, Janet: “Making Darwin: Biography and the Changing Representations of Charles Darwin.” ''The Journal of Interdisciplinary History'' 40 (2010): 347–373.
* Browne, Janet: “Charles Darwin as a Celebrity.” ''Science in Context'' 16 (2003): 175–194.
* Donald, Diana and Jane Munro: ''Endless Forms. Charles Darwin, Natural Science and the Visual Arts''. Cambridge, 2009.
* Gapps, Suzanne: “Charles Darwin as an Icon.” ''Culture and Organization'' 12 (2006): 341–357.
* Janson, Horst W.: ''Apes and Ape Lore in the Middle Ages and the Renaissance''. London, 1952.
* Kemp, Martin: ''The Human Animal in Western Art and Science''. Chicago und London, 2007.
* Kort, Pamela and Max Hollein: ''Darwin. Kunst und die Suche nach den Ursprüngen''. Köln, 2009.
* Larson, Barbara and Fae Brauer: ''The Art of Evolution. Darwin, Darwinisms, and Visual Culture''. Hanover, 2009.
* Voss, Julia: ''Darwins Bilder. Ansichten der Evolutionstheorie 1837–1874''. Frankfurt a.M., ²2009.
* Voss, Julia: “Darwin oder Moses? Funktion und Bedeutung von Charles Darwins Porträt im 19. Jahrhundert.” ''NTM Zeitschrift für Geschichte der Wissenschaften, Technik und Medizin'' 16 (2008): 213–243.

== External links ==
* [[s:The Descent of Man (Darwin)|Darwin's ''The Descent of Man'']]
* [http://europeana.eu/portal/search.html?query=charles+henry+bennett Wood engravings by Charles H. Bennett]

<!--Do not include any categories - these don't need to be added until the article is accepted; They will just get removed by a bot!-->

[[Category:Cultural depictions of Charles Darwin]]
[[Category:Caricature]]