{{Use dmy dates|date=October 2015}}
{{refimprove|date=October 2015}}
{{Infobox publisher
| image = [[File:Inderscience.svg|260px]]
| parent =
| status =
| founded = 1979
| founder =
| successor =
| country = Switzerland
| headquarters = Geneva
| distribution = Worldwide
| keypeople =
| publications = 
| topics = science, engineering and economics
| genre =
| imprints =
| revenue =
| numemployees =
| nasdaq =
| url = {{URL|http://www.inderscience.com}}
}}

'''Inderscience Publishers''' is an [[academic publisher]] based in [[Geneva|Geneva, Switzerland]] with editorial offices in [[Olney, Buckinghamshire|Olney, UK]]. It publishes [[peer review|peer-reviewed]] journals in the fields of science, engineering and technology; management, public and business administration; environment, ecological economics and sustainable development; computing, ICT and internet/web services.  All papers submitted to Inderscience journals are double-blind [[Scholarly peer review|refereed]].  

== Rankings ==
Inderscience has 223 journals indexed in [[Scopus]] and ranked by [[SCImago Journal Rank]]. The top five of these journals are: ''International Journal of Technology Management'' (est. 1986, [[h-index]] = 42); ''International Journal of Environment and Pollution'' (est. 1991, [[h-index]] = 35); ''International Journal of Mobile Communications'' (est. 2004, [[h-index]] = 32);  ''International Journal of Vehicle Design'' (est. in 1978, [[h-index]] = 32) and ''International Journal of Nanotechnology'' (est. 2004, [[h-index]] = 27).<ref>http://www.scimagojr.com/journalsearch.php?q=inderscience</ref>

The German based [[:de:Verband der Hochschullehrer für Betriebswirtschaft|VHB]] Jourqual 3 ranks B (on a scale from A+ to D) the ''International Journal of Entrepreneurial Venturing'' and the ''European Journal of International Management''.  In addition, Jourqual 3 ranks C the ''International Journal of Business Environment, International Journal of Technology Management'',  ''International Journal of Entrepreneurship and Small Business'' and the ''International Journal of Economics and Accounting''.<ref>http://vhbonline.org/en/service/jourqual/vhb-jourqual-3/complete-list-of-the-journals/</ref>

== See also ==
* [[Academic publishing]]
* [[Serials, periodicals and journals]]
* [[Bibliometrics]]
* [[Impact factor]]
* [[Scholar Indices and Impact]]

== External links ==
* {{Official website|www.inderscienceonline.com}}

== References==
{{Reflist}}

[[Category:Academic publishing companies]]
[[Category:Publishing companies of Switzerland]]
[[Category:Publishing companies established in 1979]]
[[Category:1979 establishments in Switzerland]]
[[Category:Companies based in Geneva]]