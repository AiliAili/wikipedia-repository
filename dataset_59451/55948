{{Infobox journal
| abbreviation=BMC Med.
| title=BMC Medicine
| history=2003–present
| openaccess = yes
| license = CC-BY-2.0
| publisher=[[BioMed Central]]
| country=[[United Kingdom]]
| ISSN=1741-7015
| website=http://www.biomedcentral.com/bmcmed/
| discipline = [[Medicine]]
| link1 = http://www.biomedcentral.com/bmcmed/archive/
| link1-name = Online archive
| OCLC=53806969
| LCCN = 2004243044
| impact=7.249
| impact-year = 2014
| RSS = http://www.biomedcentral.com/bmcmed/rss/
}}
'''''BMC Medicine''''' is a [[Peer review|peer-reviewed]] electronic-only [[medical journal]] published since 2003 by [[BioMed Central]]. It is described as "the flagship medical journal of the BMC series, publishing original research, commentaries and reviews that are either of significant interest to all areas of medicine and clinical practice, or provide key translational or clinical advances in a specific field".<ref>{{Cite web|title = BMC Medicine {{!}} About|url = https://www.biomedcentral.com/bmcmed/about|website = www.biomedcentral.com|accessdate = 2015-10-21}}</ref>

Like the other journals in the BMC stable, BMC Medicine is an [[open access journal]], funded by [[Article processing charge|article processing fees]].

The journal is [[Indexing and abstracting service|abstracted and indexed]] in [[Chemical Abstracts Service|CAS]], [[BIOSIS Previews|BIOSIS]], [[Embase]], [[MEDLINE]], [[PubMed Central]], [[Science Citation Index|Science Citation Index Expanded]] and [[Scopus]]. According to the [[Journal Citation Reports]], the journal has a 2014 [[impact factor]] of 7.36.
== References ==
{{Commonscat|Media from BMC Medicine}}
{{reflist}}

[[Category:BioMed Central academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2003]]
[[Category:General medical journals]]
[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Online-only journals]]


{{med-journal-stub}}