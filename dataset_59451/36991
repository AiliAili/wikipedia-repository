{{Infobox single |
| Name           = Baby Don't Cry
| Cover          = Baby don't cry cd+dvd.jpg
| Alt            = A close-up face shot of Namie Amuro, with the song and artist's name at the bottom centre of the image.
| Artist         = [[Namie Amuro]]
| from Album     = [[Play (Namie Amuro album)|Play]]
| B-side         = "Nobody"
| Released       = January 24, 2007
| Format         = {{hlist|[[CD single]]|[[DVD single]]|[[Music download|digital download]]}}
| Recorded       = 2005<br>On Air Azabu Studio<br><small>([[Minato-ku, Tokyo]])</small>
| Genre          = {{hlist|[[Pop music|Pop]]|[[R&B]]}}
| Length         = 5:22
| Label          = [[Avex Trax]]
| Writer         = Nao'ymt
| Producer       = Nao'ymt
| Last single = "[[Can't Sleep, Can't Eat, I'm Sick/Ningyo]]" <br />(2006)
| This single = "'''Baby Don't Cry'''" <br /> (2007)
| Next single = "[[Funky Town (Namie Amuro song)|Funky Town]]" <br /> (2007)
| Misc        =  
}}

"'''Baby Don't Cry'''" is a song recorded by Japanese recording artist [[Namie Amuro]] for her eighth studio album, ''[[Play (Namie Amuro album)|Play]]'' (2007). It was written, composed, arranged, and produced by Japanese musician Naoaki Yamato, under the alias Nao'ymt. The single also included the B-side track "Nobody", a re-recorded version of her single "[[White Light/Violet Sauce|White Light]]". It premiered on January 24, 2007 as the third single from the album in Japan. It was also released worldwide on February 21, 2007 through Avex Entertainment Inc. Musically, "Baby Don't Cry" is a [[Pop music|pop ballad]], influenced by [[R&B]] music. 

Upon its release, the track garnered generally favorable reviews from [[Music journalism|music critics]], who highlighted the song as one of Amuro's best singles, and commended the production and her vocals. It achieved success in Japan, peaking at number three on the [[Oricon Singles Chart]]. The single has been certified within three different categories by the [[Recording Industry Association of Japan]] (RIAJ). An accompanying music video was shot by Masashi Muto; it features Amuro walking around a city, singing in the winter time. With additional promotion through Japanese commercials and television shows, the song has been performed on several concert tours by Amuro, including her 2007 Play tour and 2008 Best Fiction tour.

==Background and release==
On December 28, 2006, Japanese magazine ''CD Journal'' confirmed the release of a new single, entitled "Baby Don't Cry".<ref>{{cite news |author=CD Journal Staff|url=http://www.cdjournal.com/main/news/-/13835|title=安室奈美恵、ニュー・シングル＆ライヴDVDが登場！|work=CD Journal|date=December 28, 2006|accessdate=April 23, 2016|language=Japanese}}</ref> It was written, composed, arranged, and produced by Japanese musician Naoaki Yamato, under the alias Nao'ymt. The song was recorded in 2006 at On Air Azabu Studio, [[Minato-ku, Tokyo]] by Toshihiro Wako. The song contains backing vocals by Japanese vocalist Hiromi.<ref name="albumnotes">{{cite AV media notes |title=Play|others=Namie Amuro|first=Namie|last=Amuro|year=2007|type=CD album; Liner notes|publisher=[[Avex Trax]], Avex Entertainment Inc.|id=AVCD-23343|location=Japan}}</ref> The single also included the B-side track "Nobody", a re-recorded version of her single "[[White Light/Violet Sauce|White Light]]". It was intended to appear on Amuro's eighth studio album, ''[[Play (Namie Amuro album)|Play]]'', as the original version was omitted. However, the re-recorded version did not end up on the final track list.<ref>{{cite AV media notes |title=White Light/Violet Sauce|others=Namie Amuro|first=Namie|last=Amuro|year=2005|type=CD single; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVCD-30889|location=Japan}}</ref> It premiered on January 24, 2007 as the third single from the album in Japan. It was also released worldwide on February 21, 2007 through Avex Entertainment Inc.<ref name="amazon">{{cite news |author=Amazon Japan Staff|url=http://www.amazon.co.jp/Baby-Dont-Cry-Namie-Amuro/dp/B000LV64EE|title=Namie Amuro – Baby Don't Cry|work=[[Amazon.com|Amazon]]|date=January 24, 2007|accessdate=April 23, 2016|language=Japanese}}</ref> 

The CD single contains both "Baby Don't Cry" and "Nobody", plus shortened versions of the tracks. These versions were used for various Japanese television commercials. The CD artwork features a close-up facial shot of Amuro, whilst the [[DVD single]] has a close up with her face and eyes closed.<ref name="singlenotes">{{cite AV media notes |title=Baby Don't Cry|others=Namie Amuro|first=Namie|last=Amuro|year=2007|type=CD single; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVCD-31176|location=Japan}}</ref> The DVD single includes the music video to "Baby Don't Cry".<ref name="dvdnotes">{{cite AV media notes |title=Baby Don't Cry|others=Namie Amuro|first=Namie|last=Amuro|year=2007|type=CD and DVD single; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVCD-31175|location=Japan}}</ref> Both formats features an extra lyric booklet, printed on plain white paper.<ref name="singlenotes"/><ref name="dvdnotes"/> Musically, "Baby Don't Cry" is a [[Pop music|pop ballad]], influenced by [[R&B]] and [[J-pop]] music.<ref name="amazon"/> Japanese music editor Random J compared the composition to the works of American recording artist, [[Janet Jackson]].<ref name="randomj">{{cite news |author=Random J|url=http://randomjpop.blogspot.co.nz/2008/08/album-review-namie-amuro-play.html|title=Namie Amuro – Play (album review)|work=Random J|date=August 18, 2008|accessdate=April 23, 2016}}</ref> ''CD Journal'' staff member's felt the song's composition was "simplistic", and described the lyrical content as "painful".<ref name="cdjournal">{{cite news |author=CD Journal Staff|url=http://artist.cdjournal.com/d/baby-dont-cry/4106120724|title=Namie Amuro – Baby Don't Cry|work=CD Journal|date=January 24, 2007|accessdate=April 23, 2016|language=Japanese}}</ref>

==Critical response==
Upon its release, "Baby Don't Cry" received positive reviews from most [[Music journalism|music critics]]. Random J, reporting from his own blogsite, highlighted it as one of the album's best tracks; he felt it was a "really nice song with a hot beat which is carried by a stellar vocal performance."<ref name="randomj"/> Staff members from [[Amazon.com|Amazon]] were positive in their review, labeling the song and Amuro's vocal delivery as "impressive". They also complimented Nao'ymt's "perfect" collaboration with Amuro.<ref name="amazon"/> An editor writing from ''CD Journal'' praised the song's mixture of R&B and pop genres. They also felt it was a "classic representation" of Amuro's music inside of the 21st century.<ref name="cdjournal"/> [[AllMusic]]'s editor Adam Greenberg commended the song's departure from the album's [[electronic music]], saying "'Baby Don't Cry' finally gives a peek at Amuro's vocals with less electronic additions...".<ref>{{cite news |author=Greenberg, Adam|url=http://www.allmusic.com/album/play-mw0001544412|title=Namie Amuro – Play (album review)|work=[[AllMusic]]|date=June 27, 2007|accessdate=April 23, 2016}}</ref> AllMusic staff selected the single as one of Amuro's best songs in her discography.<ref>{{cite news |author=AMG Staff|url=http://www.allmusic.com/artist/namie-amuro-mn0000362954/songs|title=Namie Amuro – Biography|work=AllMusic|date=2013|accessdate=April 23, 2016}}</ref>

==Commercial performance==
Commercial, "Baby Don't Cry" was successful in Japan. It debuted at number three on the [[Oricon|Oricon Daily Singles Chart]], and stayed there for four days. This resulted in a debut position of number three on the [[Oricon Singles Chart]], two positions behind entries by [[Mr. Children]] and Masafumi Akikawa; it sold 52,168 units within its first week of sales.<ref name="oricon-positions">{{cite web | script-title=ja:安室奈美恵のリリース一覧 | trans_title=List of Namie Amuro's Releases | url=http://www.oricon.co.jp/prof/artist/191925/products/release/ |language=Japanese | work=[[Oricon]] | accessdate=June 4, 2014}}</ref><ref name="sales">{{cite web | title=オリコンランキング情報サービス「you大樹」 | trans_title=Oricon Ranking Information Service 'You Big Tree' | url=http://ranking.oricon.co.jp |language=Japanese | work=[[Oricon]] |subscription=yes | accessdate=May 21, 2014}}</ref><ref>{{cite news |author=Oricon Style|url=http://www.oricon.co.jp/rank/js/w/2007-02-05/|title=Oricon Singles Chart – Chart Week February 5, 2007|work=Oricon|date=February 5, 2007|accessdate=April 23, 2016|language=Japanese}}</ref> This became Amuro's first single in six years to sell over 50,000 units in its first week since "[[Say the Word]]".<ref name="sales"/> The following week, it fell to number four with 24,643 units sold within its second week of sales.<ref>{{cite news |author=Oricon Style|url=http://www.oricon.co.jp/rank/js/w/2007-02-12/|title=Oricon Singles Chart – Chart Week February 12, 2007|work=Oricon|date=February 12, 2007|accessdate=April 23, 2016|language=Japanese}}</ref> It slipped again to number seven with 18,663 units sold in its third week of sales,<ref>{{cite news |author=Oricon Style|url=http://www.oricon.co.jp/rank/js/w/2007-02-19/|title=Oricon Singles Chart – Chart Week February 19, 2007|work=Oricon|date=February 19, 2007|accessdate=April 23, 2016|language=Japanese}}</ref> and had its last top ten appearance the following week at number nine; it sold 13,088 units.<ref>{{cite news |author=Oricon Style|url=http://www.oricon.co.jp/rank/js/w/2007-02-26/|title=Oricon Singles Chart – Chart Week February 26, 2007|work=Oricon|date=February 26, 2007|accessdate=April 23, 2016|language=Japanese}}</ref> It lasted 15 weeks in the Top 200 chart, and sold over 144,081 units by the end of 2007; it ranked at number 48 on Oricon's Annual Singles Chart.<ref name="yearly">{{cite news |author=Oricon Style|url=http://www.oricon.co.jp/rank/js/y/2007/p/5/|title=Oricon Annual Singles Chart – Chart Year 2007|work=Oricon|date=December 2007|accessdate=April 23, 2016|language=Japanese}}</ref> This became Amuro's highest selling single since "Say the Word" (with 184,000 units sold),<ref name="sales"/> and was her highest selling single until it was outsold by her 2011 single "[[Sit! Stay! Wait! Down!/Love Story]]" (with 162,000 units sold).<ref name="sales"/>{{efn|group=upper-alpha|Namie Amuro released a three-track [[extended play]]/single, ''[[60s 70s 80s]]'' in 2008, and outsold both "Say the Word", "Baby Don't Cry", and "Sit! Stay! Wait! Down!/Love Story" by physical sales (with a total of 293,000 units).<ref name="RIAJ-mar2008">{{cite web | script-title=ja:ゴールド等認定作品一覧　2008年3月 | trans_title=Works Receiving Certifications List (Gold, etc) (March 2008) | url=http://www.riaj.or.jp/data/others/gold/200803.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=April 10, 2008 | accessdate=December 31, 2013}}</ref> However, because it was released as an extended play, despite marketed as a single by Avex Trax, it does not count as being mentioned as her best selling single.}} "Baby Don't Cry" was certified gold by the [[Recording Industry Association of Japan]] (RIAJ) for shipments of 100,000 units.<ref name="RIAJ-feb2007">{{cite web | script-title=ja:ゴールド等認定作品一覧　2007年2月 | trans_title=Works Receiving Certifications List (Gold, etc) (February 2007) | url=http://www.riaj.or.jp/data/others/gold/200702.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=March 10, 2007 | accessdate=January 23, 2014}}</ref>

The single was certified million in May 2007 by the RIAJ for ringtone sales of one million units in Japan. This is her first single to be certified million by RIAJ since her 1997 single "[[How to Be a Girl]]", and was her sixth overall.<ref name="RIAJ-aug1997">{{cite journal |title=GOLD ALBUM 他認定作品 1997年6月度 |trans_title=Gold Albums, and other certified works. June 1997 Edition |url=http://www.riaj.or.jp/issue/record/1997/199708.pdf |format=PDF | journal=The Record |type=Bulletin |language=Japanese |location=[[Chūō, Tokyo]] |publisher=[[Recording Industry Association of Japan]] |publication-date=August 10, 1997 |volume=454 |page=9 |archiveurl=https://web.archive.org/web/20131103051739/http://www.riaj.or.jp/issue/record/1997/199708.pdf |archivedate=November 3, 2013 |accessdate=April 27, 2014}}</ref><ref name="RIAJ-may2007digi">{{cite web | script-title=ja:レコード協会調べ　5月度有料音楽配信認定 | trans_title=Record Association Investigation: May Digital Music Download Certifications | url=http://www.riaj.or.jp/data/others/chart/w070620_3.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=June 20, 2007 | accessdate=December 13, 2013}}</ref> Although the song has not charted on any digital record charts in Japan, it was cerfifed double platinum by the RIAJ for cellphone sales of 500,000 units.<ref name="RIAJ-apr2011digi">{{cite web | script-title=ja:レコード協会調べ　4月度有料音楽配信認定 | trans_title=Record Association Investigation: April Digital Music Download Certifications | url=http://www.riaj.or.jp/data/others/chart/w110520.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=May 20, 2011 | accessdate=June 10, 2014}}</ref> It was certified gold in December 2012 by the RIAJ for digital sales of 100,000 units.<ref name="RIAJ-dec2012digi">{{cite web | script-title=ja:レコード協会調べ　12月度有料音楽配信認定 | trans_title=Record Association Investigation: December Digital Music Download Certifications | url=http://www.riaj.or.jp/data/others/chart/w130118.html | publisher=[[Recording Industry Association of Japan]] |language=Japanese | date=January 18, 2013 | accessdate=June 10, 2014}}</ref> 

==Live performances and music video==
The single has been performed on several tours conducted by Amuro, and has appeared on Japanese commercials and television series. "Baby Don't Cry" made its debut live performance on her Play Tour (2008), which was in support of the album with the same title. The live DVD was released on February 27, 2008.<ref>{{cite AV media notes |title=Play Tour 2008|others=Namie Amuro|first=Namie|last=Amuro|year=2008|type=Live DVD; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVBD-91533|location=Japan}}</ref><ref>{{cite AV media notes |title=Play Tour 2008|others=Namie Amuro|first=Namie|last=Amuro|year=2008|type=Live Blu-Ray; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVXD-91833|location=Japan}}</ref> It was included on Amuro's [[Greatest Hits|greatest hits]] concert tour, Best Fiction 2008–2009; it was performed as the final song from the setlist. The live DVD was released on September 9, 2009.<ref>{{cite AV media notes |title=Best Fiction Tour 2008-2009|others=Namie Amuro|first=Namie|last=Amuro|year=2009|type=Live DVD; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVBD-91736|location=Japan}}</ref><ref>{{cite AV media notes |title=Best Fiction Tour 2008-2009|others=Namie Amuro|first=Namie|last=Amuro|year=2009|type=Live Blu-Ray; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVXD-91606|location=Japan}}</ref> The track appeared on her Past<Future concert tour in 2010, and was included on the live release on December 12, 2010.<ref>{{cite AV media notes |title=Past<Future Tour 2010|others=Namie Amuro|first=Namie|last=Amuro|year=2010|type=Live DVD; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVBD-91829|location=Japan}}</ref> In celebration of Amuro's 20th music career anniversary, the single was included on her 5 Major Dome concert tour in Japan; the song was performed as an encore track. The live DVD was released on February 27, 2013.<ref>{{cite AV media notes |title=Namie Amuro 5 Major Domes Tour 2012: 20th Anniversary Tour|others=Namie Amuro|first=Namie|last=Amuro|year=2013|type=Live DVD; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVBD-92026|location=Japan}}</ref> The song's most recent appearance was her Live Style concert tour (2014), where it was included on as the last track from the setlist. The live DVD was released on February 11, 2015.<ref>{{cite AV media notes |title=Live Style 2014|others=Namie Amuro|first=Namie|last=Amuro|year=2015|type=Live DVD; Liner notes|publisher=Avex Trax, Avex Entertainment Inc.|id=AVBN-99019|location=Japan}}</ref>

"Baby Don't Cry" was used as the theme song for the Japanese television drama series, ''[[Himitsu no Hanazono]]'' (2007). Alongside this, it was included in three Japanese commercials: the Avex Trax "Myu-Umo" music commercial, the Pokemero Joysound commercial, and for the [[Fuji TV]] television series, ''Secret Garden''.<ref name="cdjournal"/> An accompanying music video was shot in Japan by Masashi Muto. It opens with Amuro running on outdoor steps, and walking along a pavement bridge. As the chorus starts, intercut scenes of trees are shown, as she walks through the city. The second verse has her walking through a park, with yellow-ish leaves on the ground. She witnessed two small children on a park bench, holding the chain to a dog. The third chorus then has her walking through a city square, and ends with her standing on a beach front at sunset. A short version of the video was uploaded on Amuro's [[YouTube]] page on November 16, 2011,<ref>{{cite news |author=Amuro, Namie|url=https://www.youtube.com/watch?v=51POa7njG_4|title=Namie Amuro – Baby Don't Cry (short music video)|work=Namie Amuro; published through [[YouTube]]|date=November 16, 2011|accessdate=April 23, 2016}}</ref> whilst a full version was uploaded one month later.<ref>{{cite news |author=Amuro, Namie|url=https://www.youtube.com/watch?v=8NKyeUpO_3I|title=Namie Amuro – Baby Don't Cry (full music video)|work=Namie Amuro; published through YouTube|date=December 1, 2011|accessdate=April 23, 2016}}</ref>

==Track listings and formats==
{{col-begin}}
{{col-2}}
*'''Japanese CD single'''<ref name="singlenotes"/>
# "Baby Don't Cry" – 5:22
# "Nobody" – 4:46
# "Baby Don't Cry" (TV Mix) – 5:22
# "Nobody" (TV Mix) – 4:46

*'''Japanese DVD single'''<ref name="dvdnotes"/>
# "Baby Don't Cry" – 5:22
# "Nobody" – 4:46
# "Baby Don't Cry" (TV Mix) – 5:22
# "Nobody" (TV Mix) – 4:46
# "Baby Don't Cry" (Music video) – 5:25
{{col-2}}
*'''Digital download'''<ref name="amazon"/>
# "Baby Don't Cry" – 5:22
# "Nobody" – 4:46
{{col-end}}

==Credits and personnel==
Credits adapted from the liner notes of the ''Play'' album.<ref name="albumnotes"/>
;'''Recording'''
*Recorded at On Air Azabu Studio, [[Minato-ku, Tokyo]] (2005).
{{col-begin}}
{{col-2}}
;'''Personnel'''
*[[Namie Amuro]] – vocals, background vocals
*Naoaki "Nao'ymt" Yamato – songwriting, composition, producing, arranging
*Toshihiro Wako – recording
*Yoshiaki Onishi – mixing
{{col-2}}
*Hiromi – background vocals
*Vision Factory – management
*Masashi Muto – video director 
{{col-end}}

==Charts==
{{col-begin}}
{{col-2}}

===Charts===
{| class="wikitable plainrowheaders"
!Chart (2007)
!Peak<br>position
|-
!scope="row"|Japan Daily Chart ([[Oricon]])<ref name="oricon-positions"/>
| style="text-align:center;"|3
|-
!scope="row"|Japan Weekly Chart ([[Oricon]])<ref name="oricon-positions"/>
| style="text-align:center;"|3
|-
|}
{{col-2}}

===Year-end charts===
{| class="wikitable plainrowheaders"
!Chart (2007)
!Peak<br>position
|-
!scope="row"|Japan Yearly Chart ([[Oricon]])<ref name="yearly"/>
| style="text-align:center;"|48
|-
|}
{{col-end}}

==Certifications==
{{Certification Table Top}}
{{Certification Table Entry |region=Japan |artist=Namie Amuro |title=Baby Don't Cry |type=single |award=Gold |certref=<ref name="RIAJ-feb2007"/> |relyear=2007 |autocat=yes |salesamount=144,081 |salesref=<ref name="sales"/> |note=Physical}}
{{Certification Table Entry |region=Japan |artist=Namie Amuro |title=Baby Don't Cry |type=single |award=Platinum |number=2 |certref=<ref name="RIAJ-apr2011digi"/> |relyear=2007 |autocat=yes |note=Cellphone}}
{{Certification Table Entry |region=Japan |artist=Namie Amuro |title=Baby Don't Cry |type=single |award=Million |certref=<ref name="RIAJ-may2007digi"/> |relyear=2007 |autocat=yes |note=Ringtone}}
{{Certification Table Entry |region=Japan |artist=Namie Amuro |title=Baby Don't Cry |type=single |award=Gold |certref=<ref name="RIAJ-dec2012digi"/> |relyear=2007 |autocat=yes |note=Digital}}
{{Certification Table Bottom}}

==Release history==
{| class="wikitable plainrowheaders"
|-
!scope="col"|Region
!scope="col"|Date
!scope="col"|Format
!scope="col"|Label
|-
!scope="row"|Japan<ref name="singlenotes"/><ref name="dvdnotes"/><ref>{{cite web|url=https://itunes.apple.com/jp/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=[[iTunes Store]] (Japan)|date=January 24, 2007|accessdate=April 23, 2016|language=Japanese}}</ref>
|January 24, 2007
|{{hlist|[[CD single]]|[[DVD single]]|[[Music download|digital download]]}}
|{{hlist|[[Avex Trax]]|Avex Entertainment Inc.}}
|-
!scope="row"|United States<ref>{{cite web|url=https://itunes.apple.com/us/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (United States of America)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|rowspan="10"|February 21, 2007
|rowspan="10"|Digital download
|rowspan="10"|Avex Entertainment
|-
!scope="row"|Australia<ref>{{cite web|url=https://itunes.apple.com/au/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Australia)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|-
!scope="row"|New Zealand<ref>{{cite web|url=https://itunes.apple.com/nz/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (New Zealand)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|-
!scope="row"|Canada<ref>{{cite web|url=https://itunes.apple.com/ca/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Canada)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|-
!scope="row"|United Kingdom<ref>{{cite web|url=https://itunes.apple.com/gb/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (United Kingdom)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|-
!scope="row"|Germany<ref>{{cite web|url=https://itunes.apple.com/de/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Germany)|date=February 21, 2007|accessdate=April 23, 2016|language=German}}</ref>
|-
!scope="row"|Ireland<ref>{{cite web|url=https://itunes.apple.com/ie/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Ireland)|date=February 21, 2007|accessdate=April 23, 2016}}</ref>
|-
!scope="row"|France<ref>{{cite web|url=https://itunes.apple.com/fr/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (France)|date=February 21, 2007|accessdate=April 23, 2016|language=French}}</ref>
|-
!scope="row"|Spain<ref>{{cite web|url=https://itunes.apple.com/es/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Spain)|date=February 21, 2007|accessdate=April 23, 2016|language=Spanish}}</ref>
|-
!scope="row"|Taiwan<ref>{{cite web|url=https://itunes.apple.com/tw/album/baby-dont-cry-single/id212294445|title=Baby Don't Cry – Single – by Namie Amuro|publisher=iTunes Store (Taiwan)|date=February 21, 2007|accessdate=April 23, 2016|language=Chinese}}</ref>
|-
|}

==See also==
*"[[White Light/Violet Sauce|White Light]]" – Corresponding song that relates to B-side track, "Nobody".

==Notes==
{{notelist-ua}}

==References==
{{reflist|2}}

==External links==
*[http://namieamuro.jp/discography/single30.html "Baby Don't Cry"] – Namie Amuro's official website.

{{Namie Amuro singles}}
{{Good article}}

{{DEFAULTSORT:Baby Don't Cry (Namie Amuro Song)}}
[[Category:2007 singles]]
[[Category:Namie Amuro songs]]
[[Category:Japanese television drama theme songs]]
[[Category:2005 songs]]
[[Category:Avex Trax singles]]