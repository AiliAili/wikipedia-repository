<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name= Venom
 |image= File:Vickers Venom.jpg
 |caption= Vickers Venom on a test flight in 1936
}}{{Infobox aircraft type
 |type= Fighter
 |national origin= United Kingdom
 |manufacturer= Vickers-Armstrongs Ltd
 |designer=
 |first flight= 17 June 1936
 |introduced=
 |retired=
 |status=
 |primary user=
 |number built= 1
 |developed from= [[Vickers Jockey]]
 |variants with their own articles=
}}
|}

The '''Vickers Type 279 Venom''' was a low-wing [[monoplane]] single-seat, single-engined, eight-gun [[Fighter aircraft|fighter]], powered by a [[radial engine]]. It was fast and manoeuvrable but its engine lacked the power and development potential of its [[Rolls-Royce Merlin|Merlin]]-powered competitors. Only one was built.

==Design and development==
The Vickers Venom<ref>[[#refAndrewsandMorgan1988|Andrews and Morgan 1988]], pp. 246–50, 254.</ref> was designed to meet [[List of Air Ministry Specifications#1930-1939|Air Ministry specification F.5/34]] which called for a single-seat eight-gun aircraft with the high maximum speed and rate of climb needed to catch {{convert|200|mph|kph}} bombers flying at {{convert|15000|ft|m}}. The aircraft would have to use a radial engine as it was intended to be used overseas in hot climates. Vickers based their entrant on the earlier [[Vickers Jockey|Type 151 Jockey]] fighter, using the same wing and tail airfoil sections and dimensions but replacing the Wibault-Vickers corrugated construction of the Jockey with a modern stressed skin structure.<ref name="Interceptor p63">[[#refGoulding1986|Goulding 1986]], p. 63.</ref>
[[File:Vickers 279 Venom on ground.jpg|300px|thumb|left]]
The Venom (originally known as the Jockey Mk II) was a low-wing monoplane, with square-tipped constant chord wings and tailplane. The fin, too, was square-tipped but the rather angular appearance did not extend to the fuselage, which tapered rearwards from the engine's long chord cowling back to the tail.  The pilot sat over the wing in a perspex-enclosed cockpit, which had unusual additional windows in the fuselage for an enhanced side and downwards view.  The inverted U-shaped fairing behind the cockpit extended back to the base of the fin.

The fuselage was an alloy-skinned monocoque structure of polygonal section, and the thicker plates of the wings were also stressed, taking the drag loads. The wings carried 90° flaps and there was a wide track, inward-retracting main undercarriage plus a small fixed tailwheel. Both the flaps and undercarriage were electrically operated.  The deep RAF 34 wing section eased the installation of the required eight Browning machine guns.  The Venom was powered by a {{convert|625|hp|kW}} [[Bristol Aquila]] AE-3S [[sleeve valve]] [[radial engine]], hinge-mounted so it could be swung sideways for easy maintenance.  The Aquila drove a three-bladed propeller. [[Joseph Summers|Joseph "Mutt" Summers]] flew the Venom on her first flight, 17 June 1936.  Unlike the Hurricane and Spitfire, the Venom was fitted with full armament from its first flight.<ref name="Mason fighter p263">[[#refMason1992|Mason 1992]], p. 265.</ref>

==Testing and evaluation==
Publicly unveiled at the 1936 [[Society of British Aircraft Constructors]] (SBAC) display, the Venom appeared in natural polished metal and silver dope on the fabric control surfaces, with private venture registration markings: PVO-10.<ref name= "Lewis p. 279">[[#refLewis1967|Lewis 1967]], p. 279.</ref> In testing, the Venom performed well on the limited power of the Aquila, achieving a maximum speed of {{convert|312|mph|kph}} and possessing an excellent climb rate.  The compact radial engine gave the Venom a better rate of roll and turn than its long nosed water-cooled competitors but it was soon clear the potential power from the [[Rolls-Royce Merlin|Merlin]] was greater than was likely to be available from the Aquila in the near future, with only limited development of this engine being undertaken, while no other engines were available suitable for fitting in such a small airframe.<ref name="Interceptor p64">[[#refGoulding1986|Goulding 1986]], p. 64.</ref> [[Jeffrey Quill]] flew the Venom from [[Southampton Airport|Eastleigh]], mixing it with Spitfires but the Venom's flying was limited by engine problems. Later flying with [[Royal Air Force roundels|RAF roundel]]s and marked with "3" on the fuselages sides, the Venom was painted a cream colour for exhibition purposes.<ref name= "Lewis p. 279"/>  The need to concentrate on the Spitfire led to the scrapping of the sole Venom prototype in 1939, after a crash in testing.<ref name= "Lewis p. 279"/><ref name="Andrews p250">[[#refAndrewsandMorgan1988|Andrews and Morgan 1988]], p. 250.</ref>

==Specifications==
<!--Use one OR other of the two specification templates. Delete the template code of the one you do not use. aero-specs is designed to handle the specification of gliders and lighter-than-air craft well. They each have their own documentation. In aircraft-specifications the parameter "xxxx more" allows for the addition of a qualifier to the value eg "at low level", "unladen". -->
{{aerospecs
|ref=Vickers Aircraft since 1908<ref name="Andrews Vickers p254">[[#refAndrewsandMorgan1988|Andrews and Morgan 1988]], p. 254.</ref><!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=one
|capacity=
|length m=7.48
|length ft=24
|length in=2
|span m=9.98
|span ft=32
|span in=9
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.00
|height ft=10
|height in=9
|wing area sqm=13.56
|wing area sqft=146
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=1,885
|gross weight lb=4,156
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Bristol Aquila]] AE-3S 9-cylinder sleeve valve radial
|eng1 kw=466<!-- prop engines -->
|eng1 hp=625<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=502
|max speed mph=at 16,500 ft (5,030 m) 312
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=9,760
|ceiling ft=32,000
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=15.2
|climb rate ftmin=3,000
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=8×0.303 in (7.7 mm) [[Browning machine gun]]s
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
;Notes
{{Reflist|2}}
;Bibliography
{{Refbegin}}
* <cite id=refAndrewsandMorgan1988>Andrews, C. F. and E. B. Morgan. ''Vickers Aircraft since 1908, 2nd ed.'' London: Putnam, 1988. ISBN 0-85177-815-1. </cite>
* <cite id=refGoulding1986>Goulding, James. ''Interceptor''. London: Ian Allen, 1986. ISBN 0-7110-1583-X.</cite>
* <cite id=refLewis1967>Lewis, Peter. "Vickers Type 279 Venom." ''Air Pictorial'', Volume 29, No. 8, August 1967.</cite>
* <cite id=refMason1992>Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.</cite>
{{Refend}}

==External links==
{{commons category|Vickers Venom}}
* [http://www.1000aircraftphotos.com/APS/1960.htm Jacques Trempe Collection No. 1960. Vickers 279, Aeroplane Photo Supply (APS) Photo No. 1434 Venom (O-10)]
* [http://www.flightglobal.com/pdfarchive/view/1936/1936%20-%203094.html ''Flight'' 1936], contemporary reporting

{{Vickers aircraft}}
{{Use dmy dates|date=September 2010}}

[[Category:British fighter aircraft 1930–1939]]
[[Category:Vickers aircraft|Venom]]