{{Infobox military person
| name          =Carl Degelow
| image         =
| caption       =
| birth_date          = <!-- {{Birth date and age|YYYY|MM|DD}} -->5 January 1891
| death_date          =  {{Death date and age|df=yes|1970|11|9|1891|1|5}}
| placeofburial_label = 
| birth_place  =[[Munsterdorf]], Germany
| death_place  =[[Hamburg]], Germany
| placeofburial =
| nickname      =Charly
| allegiance    =Germany
| branch        =Infantry, Air Service
| serviceyears  =1914 - circa 1945
| rank          =Major
| unit          =Infantry Regiment 88,<br/>FAA 216,<br/>''[[Jagdstaffel 36]]'',<br/>''[[Jagdstaffel 7]]''
| commands      =''[[Jagdstaffel 40]]''
| battles       =
| awards        =''[[Pour le Merite]]'',<br/>[[Royal House Order of Hohenzollern]],<br/>[[Iron Cross]]
| relations     =
| laterwork     =[[Luftwaffe]] in World War II.
}}
'''Carl "Charly" Degelow''' (5 January 1891 – 9 November 1970) ''[[Pour le Merite]]'', [[Royal House Order of Hohenzollern]],<ref name="theaerodrome.com">The Aerodrome websites' page on the Hohenzollern Order http://www.theaerodrome.com/medals/germany/prussia_rhoh.php?pageNum_recipients=1&totalRows_recipients=91#recipients Retrieved 26 November 2012.</ref> [[Iron Cross]],<ref>The Aerodrome's website on the Iron Cross http://www.theaerodrome.com/medals/germany/prussia_ic.php?pageNum_recipients=4&totalRows_recipients=225#recipients Retrieved 26 November 2012.</ref>  was a [[German Empire|German]] fighter pilot during [[World War I]]. He was credited with 30 victories, and was the last person to win the military [[Pour le Merite]].<ref>{{cite web|url=http://www.pourlemerite.org/ |title=Orden Pour le Mérite |publisher=Pourlemerite.org |date= |accessdate=2012-11-26}}</ref>

==Life before aviation==
Carl Degelow was born on 5 January 1891 in [[Münsterdorf]], [[Schleswig-Holstein]], in the [[Kingdom of Prussia]].<ref name=Lines96>Franks et al 1993, pp. 96-97.</ref>

In 1912-1913, before World War I broke out, Degelow worked in the [[United States]] as an industrial [[chemist]]. His specialty was the manufacture of [[cement]], and he was known to have visited [[Chicago]] and [[El Paso]].<ref name=Lines96/><ref>{{cite web|url=http://www.theaerodrome.com/forum/people/32548-carl-degelow-questions.html |title=Carl Degelow Questions |publisher=Theaerodrome.com |date= |accessdate=2012-11-26}}</ref>

He returned to Germany just before World War I erupted and enlisted in Nassauischen Infanterie-Regiment Nr 88. Degelow initially served with distinction in this infantry regiment in both France and Russia, winning both classes of the [[Iron Cross]] and earning promotion in rank from [[Gefreiter]] to [[Vizefeldwebel]]. He was also wounded in the arm while fighting in Russia. He was [[commission (document)|commissioned]] as a ''[[Leutnant]]'' on 31 July 1915. He was seriously wounded in the arm in Russia in 1915, and commissioned in July 1915 while he was still hospitalized.<ref name=Lines96/>

==Flying service==
* See also [[Aerial victory standards of World War I]]

Degelow transferred to the air service in 1916 and trained as a pilot. His first assignment was to ''Flieger-Abteilung'' (''Artillerie''): Flier Detachment (Artillery) 216 as an artillery spotter at the beginning of 1917, directing and correcting artillery fire from an [[Albatros C.V]].<ref name=Lines96/><ref>{{cite web|url=http://www.westernfrontassociation.com/index.php?option=com_content&view=article&id=689:degelow&catid=74:the-aces&Itemid=109 |title=DEGELOW, Carl. (1891-1970) |publisher=Westernfrontassociation.com |date=2008-08-04 |accessdate=2012-11-26}}</ref>  On 22 May 1917, he claimed a French [[Caudron G.IV]], but it was unconfirmed. Three days later, he shot down another Caudron; for this one he got credit.<ref name=Lines96/><ref name="ReferenceA">{{cite web|url=http://www.theaerodrome.com/aces/germany/degelow.php |title=Carl Degelow |publisher=Theaerodrome.com |date= |accessdate=2012-11-26}}</ref>

He was reassigned to the Prussian ''[[Jagdstaffel 36]]'' for transitional training into flying [[Pfalz D.III]] fighters in August 1917, but lasted fewer than four days. The process involved zeroing in an aircraft's machine guns on a firing range. Degelow accidentally wounded a member of the unit and was hastily reassigned on 17 August to Prussian ''[[Jasta 7]]'' to fly a [[Pfalz D.III]] under the leadership of Leutnant [[Josef Jacobs]].<ref name=Lines96/><ref name=Pfalz58-59>VanWyngarden 2006, pp. 58-59.</ref>

Degelow chose to fly a Pfalz because the [[Albatros D.V]] available to him had a reputation at the time for losing its wings in a dive.<ref name=Pfalz62>VanWyngarden 2006, p. 62.</ref> He began his string of victories with ''Jasta 7'' on an uncertain note. He filed three widely spaced consecutive victories that were unconfirmed as the enemy planes landed on the Allied side of the front lines. By German regulations, these could not be considered confirmed aerial victories.<ref name=Pfalz58-59/>

In September, he was almost shot down by a [[Bristol F.2 Fighter]] after it hit his oil tank, which misted his flying goggles and blurred his vision. Only Jacobs' intervention saved Degelow. Degelow claimed Jacobs destroyed the attacker, although the victory is not listed on Jacobs' records.{{citation needed|date=January 2013}}

On 25 January 1918 he finally was credited with his second victory, after four unconfirmed claims.<ref name=Lines96/>  On 23 March, while landing in a 30 kilometer per hour (19&nbsp;mph) wind, he destroyed his aircraft by upsetting upon landing but was unhurt. The photo of his inverted aircraft shows he sported a running stag insignia on his plane.<ref name=Pfalz58-59/>

His third confirmed victory did not come until 21 April 1918.<ref name="ReferenceA"/> He scored his fourth victory and transferred on 16 May 1918 to the Royal Saxon ''[[Jagdstaffel 40]]''. Degelow became the squadron's commanding officer on 11 July 1918, after [[Helmut Dilthey]] was killed. During this time, he also flew a borrowed [[Fokker Dr.1]] Triplane, though he doesn't seem to have claimed any successes with it.<ref name=Lines96/><ref>Franks, VanWyngarden 2001, p. 70.</ref>

He became an ace on 18 June, with the first of his two June victories.<ref name="ReferenceA"/> On the 25th, he took his new [[Fokker D.VII]] on a test flight, came upon a dogfight between D. VIIs of another Jasta and [[Sopwith Camels]], and shot one of the Camels down. The D.VII remained his mount for the remainder of the war. His had the front three-quarters of the fuselage painted black; the rear quarter was white. Emblazoned on the side was a white running stag with gold antlers and hooves, its head pointed toward the propeller.{{citation needed|date=January 2013}}

In July, he scored six times.<ref name="ReferenceA"/> On 9 August, he was awarded the Knight's Cross with Swords of the [[Royal House Order of Hohenzollern]].<ref name="theaerodrome.com"/> In September, he again scored six times. October saw ten victories on ten different days. The victory on the 4th was over a Canadian naval ace, Captain [[John Edmund Greene|John Greene]] of No. 213 Squadron. Degelow scored once more, on 4 November, just before the end of his and Germany's war. Thus credited with 30 confirmed victories and four unconfirmed, he was the last German pilot and final German serviceman to be awarded the [[Pour le Mérite|Blue Max]], on 9 November 1918, the day the Kaiser abdicated, and two days before the end of the war. The abdication ended Imperial decrees and orders such as awards for the "Blue Max".<ref name=Lines96/><ref>{{cite web|url=http://www.pourlemerite.org |title=Orden Pour le Mérite |publisher=Pourlemerite.org |date= |accessdate=2012-11-26}}</ref> Degelow also received the [[Iron Cross]], 1st and 2nd class.

==Post World War I==
Immediately postwar, Degelow helped found the ''Hamburger Zeitfetwilligen Korps'' to battle communists in Germany.<ref name=Lines96/> He wrote his war memoirs, ''With the White Stag Through Thick and Thin'' in 1920. They were published in English in 1979 as ''Germany's Last Knight of the Air'' by Peter Kilduff, who added some additional commentary.<ref>{{cite web|url=http://www.theaerodrome.com/forum/2001/11178-carl-degelow-s-book.html |title=Carl Degelow's book |publisher=Theaerodrome.com |date= |accessdate=2012-11-26}}</ref>

Degelow remained in the reserves between World Wars I and II. During the first days of the Nazi regime, he was jailed for several days for failure to give the Nazi salute on parade. When someone recognized the [[Pour le Merite]] on his uniform, he was quickly released.{{citation needed|date=January 2013}}

By the time World War II began, he was a Hauptmann (captain).<ref>{{cite web|url=http://www.theaerodrome.com/forum/people/931-anything-known-about-degelow-s-career-wwii.html |title=Anything known about Degelow's career in WWII? |publisher=Theaerodrome.com |date= |accessdate=2012-11-26}}</ref> Degelow went on to serve as a Major in the [[Luftwaffe]] during World War II.<ref name=Lines96/><ref name="ReferenceA"/>

He died in [[Hamburg]], Germany on 9 November 1970.<ref name=Lines96/>

==See also==
* Degelow, Carl; Kilduff, Peter. ''Germany's Last Knight of the Air: The Memoirs of Major Carl Degelow''. W. Kimber, 1979. ISBN 0718301463, 9780718301460.

==References==
{{Reflist|2}}

==Bibliography==
* [[Norman Franks|Franks, Norman]]; Bailey, Frank W.; Guest, Russell. ''Above the Lines: The Aces and Fighter Units of the German Air Service, Naval Air Service and Flanders Marine Corps, 1914–1918''. Grub Street, 1993. ISBN 0-948817-73-9, ISBN 978-0-948817-73-1.
* Norman Franks, Greg VanWyngarden. ''Fokker Dr I Aces of World War I''. Osprey Publishing, 2001. ISBN 1-84176-223-7, ISBN 978-1-84176-223-4.
* Greg VanWyngarden. ''Pfalz Scout Aces of World War 1''. Osprey Publishing, 2006. ISBN 1841769983, 9781841769981.

{{wwi-air}}

{{Authority control}}

{{DEFAULTSORT:Degelow, Carl}}
[[Category:1891 births]]
[[Category:1970 deaths]]
[[Category:People from Steinburg]]
[[Category:People from the Province of Schleswig-Holstein]]
[[Category:German World War I flying aces]]
[[Category:Recipients of the Pour le Mérite (military class)]]
[[Category:Knights of the House Order of Hohenzollern]]
[[Category:Luftwaffe personnel of World War II]]
[[Category:Luftstreitkräfte personnel]]
[[Category:Prussian Army personnel]]