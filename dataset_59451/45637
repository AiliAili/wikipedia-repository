{{other|James Templer (disambiguation)}}
{{Infobox military person
|name= James Templer
|birth_date=27 May 1846
|death_date= 2 January 1924<ref name="TemplerTree">http://www.templerfamily.co.uk/Templer%20Trees/Templer%20A/GGtextGC.htm#I0309</ref>
|birth_place= [[Greenwich]], [[Kent]], [[England]]<ref name="TemplerTree"/>
|death_place= [[Lewes]], [[Sussex]], [[England]]<ref name="TemplerTree"/>
|image= Aviation in Britain Before the First World War RAE-O956.jpg
|caption= 
|nickname= 
|allegiance= {{flagicon|United Kingdom}} [[United Kingdom]]
|serviceyears= 
|rank= [[Colonel (United Kingdom)|Colonel]]
|branch= [[British Army]]
|commands= 
|unit= [[King's Royal Rifle Corps]]<br>[[Royal Engineers]] 
|battles= [[Second Boer War]]
|awards= 
|laterwork= 
}}

[[Colonel (United Kingdom)|Colonel]] '''James Lethbridge Brooke Templer''' (27 May 1846 &ndash; 2 January 1924) was an early British military pioneer of balloons.<ref name="templerfamily.co.uk">http://www.templerfamily.co.uk/Templers/People/James%20Lethbridge%20Brooke%20Templer/James_LB_Templer.htm</ref>  He was an officer in the [[King's Royal Rifle Corps]]<ref name="RE_museum">http://www.remuseum.org.uk/corpshistory/rem_corps_part8.htm</ref> and [[Royal Engineers]].<ref>http://myancestors.wordpress.com/2007/11/25/fatal-balloon-accident-near-bridport/</ref>  Templer set out a  scientific foundation for British military ballooning.  In particular he worked out routines for balloon handling, how to use hydrogen in cylinders and methods for training observers.<ref>http://www.crossandcockade.com/pdf/Observers.pdf</ref>

==Biography==
James Templer was the son of [[John Charles Templer | John Templer]].  He was educated at [[Harrow School|Harrow]] and [[Trinity College, Cambridge]].<ref>{{acad|id=TMLR865JL|name=Templer, James Lethbridge Brooke}}</ref> Whilst serving in the King's Royal Rifle Corps Templer became interested in military ballooning.<ref name="ARWE">{{cite book |last=Burke |first=Edmund |author2=Ivison Stevenson Macadam |title=The Annual Register of World Events |year=1925 |publisher=Longmans, Green |pages=109 }}</ref>

In 1878 Captain Templer and Captain C M Watson started the first regular [[School of Ballooning|British Army balloon school]] at Woolwich.<ref>{{cite book |last=Turner |first=Charles Cyril |title=The Old Flying Days |origyear=1927 |year=1972 |publisher=Ayer Publishing |isbn=0-405-03783-X |pages=5 and 6|chapter=XXII}}</ref>  The school was started with Templer's own balloon, the ''Crusader''.<ref>http://www.century-of-flight.freeola.com/new%20site/balloons/Military%20balloons%201850.htm</ref>  At the same time, Templer was appointed Instructor in Ballooning to the Royal Engineers.<ref>http://www.flying-museum.org.uk/the_early_days.htm</ref>  The following year Captain Templer took command of the newly established military balloon department at Chatham.<ref>http://infomotions.com/etexts/gutenberg/dirs/etext97/aadow10.htm</ref>

On 10 December 1881 Captain Templer was accompanied by [[Walter Powell (1842-1881)|Walter Powell]] the MP for [[Malmesbury (UK Parliament constituency)|Malmesbury]] and Mr. A. Agg-Gardner, brother of [[James Agg-Gardner]] - then between terms as MP for [[Cheltenham (UK Parliament constituency)|Cheltenham]] - in the balloon ''Saladin''.  The group departed [[Bath, Somerset|Bath]] and headed towards Dorset.  In time they found themselves within half a mile of the sea near Eypesmouth which is to the west of [[Bridport]].  As the balloon was rapidly drifting seaward, they attempted to descend.  The balloon touched the ground a mere 150 yards from the cliff edge.  The balloon dragged along and ground and Templer exited the basket holding the valve line in his hand.  As the balloon had just been lightened, it rose about eight feet and Agg-Gardner jumped out breaking his leg.  Powell was now the only occupant of the balloon.  Templer, who had still hold of the line, shouted to Powell to climb down the line.  Powell made a move for the rope but the balloon rose, tearing the line out of Templer's hands.  The balloon climbed rapidly and Powell was taken out to sea. He was never seen again.<ref name="templerfamily.co.uk"/>

By 1885, Templer had achieved the rank of [[Major (United Kingdom)|major]].  During the British Army's expedition to the Sudan in 1885, Templer took three balloons.  He was mentioned in despatches for his actions during the Hasheen engagement.<ref name="ARWE"/><ref>http://www.aerodacious.com/Left002.HTM</ref>

In 1888 Templer was accused, arrested and charged with providing the Italian Government with British secrets about military ballooning.  The case was found to be without foundation and Templer was honourably acquitted.<ref>{{cite news| url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9D04E3DF1230E633A2575AC2A9629C94699FD7CF&oref=slogin | work=The New York Times | title=Major Templer's Escape; Discussion Of The Case In English Army Circles. The Feeling Toward His Accuser-- Curious Social Ethics--Drill Reform--Coming To Boston | date=29 April 1888}}</ref>

During the [[Second Boer War]] (1899-1902), Templer served in the Scientific and Departmental Corps as a [[Lieutenant-Colonel (United Kingdom)|lieutenant colonel]].<ref>http://www.angloboerwar.com/Other/transvaal_war_album.htm</ref>  He was able to put his interest in [[Traction engine|steam traction]] to use when he served as Director of Steam Road Transport during the war.<ref name="RE_museum"/> He was [[mentioned in despatches]] (29 November 1900) by [[Frederick Roberts, 1st Earl Roberts|Lord Roberts]], Commander-in-Chief during the early part of the War.<ref>{{LondonGazette| issue=27443| supp=|startpage=3965| endpage=3967| date=17 June 1902}}</ref>

By 1902, Templer had reached the rank of colonel and he decided that it was time to construct a British military airship. Under Templer's direction, in 1905 the Balloon Factory relocated to [[Royal Aircraft Establishment|Farnborough]].<ref>http://www.aiaa.org/Participate/Uploads/Sites-by-state-for-web4.doc</ref> where work could be started on an airship shed. This, and a shortage of money, delayed the project.  Work  on the [[British Army Dirigible No 1]], named ''Nulli Secundus'' ("Second to none") was not complete until 1907 by which time Templer was no longer the superintendent of the Balloon Factory, Colonel [[John Capper|Capper]] having taken over in 1906.<ref>{{cite book |last=Turner |first=Charles Cyril |title=The Old Flying Days |origyear=1927 |year=1972 |publisher=Ayer Publishing |isbn=0-405-03783-X |pages=294|chapter=XXII}}</ref>

Templer continued as the superintendent of the [[School of Ballooning|Balloon Factory]] until retiring from service in 1908.

He died at Laughton Grange in the [[Sussex]] town of [[Lewes]] on 2 January 1924.<ref name="TemplerTree"/>

==Family==
Templer married Florence Henrietta Gilliat at Chorley Wood in [[Watford]] on 12 January 1889.<ref name="TemplerTree"/> Their daughter Ursula Florence Templer was born in London the following year on 29 August 1890.<ref>http://www.templerfamily.co.uk/Templer%20Trees/Templer%20A/GGtextGC.htm#I0419</ref>

==References==
{{Reflist}}

==External links==
{{Commonscat|James Templer}}

{{s-start}}
{{s-mil}}
|-
{{s-new|reason=School established}}
{{s-ttl|title=Superintendent of the [[School of Ballooning|Balloon School]]<br><small>Initially with Capt C M Watson</small>|years=1878 &ndash; 1906}}
{{s-aft|after=[[John Capper|J E Capper]]}}
{{s-end}}

{{DEFAULTSORT:Templer, James Lethbridge Brooke}}
[[Category:King's Royal Rifle Corps officers]]
[[Category:Royal Engineers officers]]
[[Category:British balloonists]]
[[Category:People educated at Harrow School]]
[[Category:Alumni of Trinity College, Cambridge]]
[[Category:1846 births]]
[[Category:1924 deaths]]