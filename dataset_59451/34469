{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = 1.000.000
| Cover          = OneMillionsong.jpg
| Artist         = [[Alexandra Stan]] featuring [[Carlprit]]
| Alt            = Stan sitting on a TV with Carlprit's face on it.
| Album          = [[Saxobeats]]
| Released       = 20 January 2012
| Format         = {{flat list|
*[[music download|Digital download]]
*[[CD single]]
}}
| Length         = 3:18
| Genre          = {{flat list|
*[[R&B]]
*[[Pop music|pop]]
}}
| Label          = {{flat list|
*Vae Victis
*E2
}}
| Writer         = {{flat list|
*Andrei Nemirschi
*Marcel Prodan
*Marcian Alin Soare
}}
| Producer       = {{flat list|
*Andrei Nemirschi
*Marcel Prodan
}}
| Chronology     = [[Alexandra Stan]] singles
| Last single    = "[[Get Back (ASAP)]]"<br />(2011)
| This single    = "'''1.000.000'''"<br />(2011)
| Next single    = "[[Lemonade (Alexandra Stan song)|Lemonade]]"<br /> (2012)
| Misc           = {{Extra chronology
| Artist         = [[Carlprit]] singles
| Type           = singles
| Last single    = "1234"<br />(2011)
| This single    = "'''1.000.000'''<br />(2011)
| Next single    = "Fiesta"<br />(2012)
}}
}}
"'''1.000.000'''" is a song recorded by Romanian recording artist [[Alexandra Stan]] for her debut studio album, ''[[Saxobeats]]'' (2011). Featuring the vocal collaboration of German-Zimbabwean rapper [[Carlprit]], it was made available as a [[music download|digital download]] on 22 February 2012 through Vae Victis and E2. The track was written by Andei Nemirschi, Marcel Prodan and Marcian Alin Soare, while being produced by Nemirschi and Prodan. Musically, "1.000.000" is an [[R&B]] and [[pop music|pop]] song which incorporates [[hip hop]] beats in its instrumentation; Stan repeats the word "million" over the chorus, which is reminiscent of Romanian band M&G's track "Milioane".

An accompanying music video for the single was uploaded on Prodan's [[YouTube]] channel on 22 December 2011, with it being filmed by Iulian Moga in [[Bucharest]] in the spawn of thirty hours. The visual portrays both Stan and Carlprit performing to the song in front of a [[graffiti]] backdrop and inside a mirrored room; one piece of the singer's clothing included a pair of shoes which was custom made in about forty-eight hours straight. With [[music journalism|music critics]] generally praising "1.000.000", the recording experienced minor commercial success in Europe. It reached number seventeen on native [[Romanian Radio Airplay Chart|Airplay 100]], number thirty-four in Italy and number 203 in Russia.

==Background and composition==
After Carlprit's involvement with Stan in a remix of her 2011 single "[[Mr. Saxobeat]]",<ref name="mix1"/> her manager and collaborator Marcel Prodan suggested that the pair work together in the future, which would result in "1.000.000".<ref name="utv interview">{{cite news|url=http://www.utv.ro/muzica/dance/interviu-cu-alexandra-stan-pe-platoul-de-filmare-a-clipului-1-000-000-video-interviu/|title=Interviu cu Alexandra Stan pe platoul de filmare a clipului "1.000.000"|date=|author=|publisher=Utv|language=Romanian|accessdate=8 September 2016|trans_title=Interview with Alexandra Stan at the shootings for her music video for "1.000.000" }}</ref> The song aired for the first time on French radio station Puls Radio.<ref name="utv background">{{cite news|url=http://www.utv.ro/muzica/dance/alexandra-stan-a-lansat-piesa-%E2%80%9Cone-million%E2%80%9D-audio/|title=Alexandra Stan a lansat piesa "One Million"|date=|author=|publisher=Utv|language=Romanian|accessdate=8 September 2016|trans_title=Alexandra Stan has released the song "One Million" }}</ref> The track was written by Andrei Nemirschi, Prodan and Marcian Alin Soare, while being produced by Prodan and Nemirschi.<ref name="credits"/> "1.000.000" incorporates [[hip hop]] beats in its instrumentation, whilst Stan provides "warm" vocals.<ref name="media pro">{{cite news|url=http://www.mediapromusic.ro/stiri/8839913-alexandra-stan-1.000.000|title=Alexandra Stan – 1.000.000|date=|author=|publisher=MediaPro Music|language=Romanian|accessdate=7 September 2016}}</ref> Carlprit opens the song by addressing the lyrics "You're one in a million" to Stan; she repeats the word "million" over the chorus, which is reminiscent of "Milioane" recorded by Romanian band M&G.<ref name="direct lyrics">{{cite news|url=http://www.directlyrics.com/alexandra-stan--1000000-music-video-news.html|title=Alexandra Stan – 1.000.000|date=22 December 2011|author=Apaza, Kevin|publisher=[[SpinMedia|Direct Lyrics]]}}</ref><ref name="utv">{{cite news|url=http://www.utv.ro/muzica/dance/alexandra-stan-a-lansat-piesa-%E2%80%9Cone-million%E2%80%9D-audio/|title=Alexandra Stan a lansat piesa "One Million"|date=|author=|publisher=Utv|language=Romanian|trans_title=Alexandra Stan has released the song "One Million"}}</ref><ref name="plagiat">{{cite news|url=http://popcrush.com/alexandra-stan-plagiarism/|title='Mr. Saxobeat' Hitmaker Alexandra Stan Accused of Plagiarism|date=18 October 2011|author=Cheung, Nadine|publisher=[[PopCrush]]|accessdate=27 December 2016}}</ref><ref name="popmatters">{{cite web|url=http://www.popmatters.com/review/154510-alexandra-stan-saxobeats/|title=Alexandra Stan: Saxobeats|publisher=[[PopMatters]]|author=Schiller, Mike|accessdate=21 June 2016|date=20 February 2012}}</ref> Italian publication L'Altra Pagina described the single as an evolution in Stan's artistry, pointing out the absence of a saxophone in its composition unlike her previous material, and its style as being oriented to [[pop music|pop]] rather than her past [[dance music|dance]] works.<ref name="laltra pagina">{{cite news|url=http://www.laltrapagina.it/mag/1-000-000-un-nuovo-tormentone-per-alexandra-stan-dopo-mr-saxobeat/|title=1.000.000: un nuovo "tormentone" per Alexandra Stan dopo Mr. Saxobeat|date=24 January 2012|author=|publisher=''L'Altra Pagina''|language=Italian|trans_title=1.000.000: a new "blockbuster" for Alexandra Stan after Mr. Saxobeat|archivedate=14 September 2016|archiveurl=https://web.archive.org/web/20160914070923/http://www.laltrapagina.it/mag/1-000-000-un-nuovo-tormentone-per-alexandra-stan-dopo-mr-saxobeat/}}</ref> According to Rodrigo of ''Yam-Magazine'', the single is of the [[R&B]] and pop genre.<ref name="yam">{{cite news|url=http://www.yam-mag.com/reviews/music-reviews/alexandra-stan-saxobeats/|title=Alexandra Stan – Saxobeats|author=Rodrigo|date=17 January 2012|publisher=''Yam Magazine''|accessdate=27 December 2016}}</ref>

==Reception==
[[File:Carlprit_in_london.jpg|thumb|200px|right|"1.000.000" featured the vocal collaboration of German-Zimbabwean rapper [[Carlprit]].<ref name="credits"/>]]The recording was generally acclaimed by [[music journalism|music critics]]. Kevin Apaza, writing for [[SpinMedia|Direct Lyrics]], called the song "catchy and "super infectious", suggesting that "if promoted correctly '1.000.000' can slay European, and US charts."<ref name="direct lyrics"/> He went on praising its chorus for being "too cute", and labelling the recording as being "so simple, so generic, but so fire at the same time."<ref name="direct lyrics"/> Although Romanian music website Utv felt that the refrain's lyrics were "repetitive and monosyllabic", they acclaimed the track's rhythm and stated, "Moreover, Alexandra Stan accustomed us that once she releases a new single, this must be different from the previous ones."<ref name="utv"/> German magazine Klatsch–Tratsch said that "1.000.000" contains "gentle and sometimes quite very romantic tones",<ref name="klatsch tratsch">{{cite web | title=Shootingstar Alexandra Stan kommt mit ihrem Debütalbum "Saxobeats" | trans_title=Shooting star Alexandra Stan releases her debut album "Saxobeats"| url=http://www.klatsch-tratsch.de/2011/08/16/shootingstar-alexandra-stan-kommt-mit-ihrem-debutalbum-saxobeats/86495|date=16 August 2011|language=German | publisher=''Klatsch-Tratsch''|accessdate=12 June 2016}}</ref> while [[AllMusic]]'s Celeste Rhoads called the recording "addictive" during his review of ''Saxobeats''.<ref name="allmusic">{{cite web|url=http://www.allmusic.com/album/saxobeats-mw0002216607|title=Saxobeats – Alexandra Stan|publisher=[[AllMusic]]|author=Rhoads, Celeste|accessdate=21 June 2016}}</ref> German portal Mix1 expected the single to become a hit and awarded it a score 6 out of 8.<ref name="mix1">{{cite web | title=Alexandra Stan feat. Carlprit mit der Single "1.000.000"| trans_title=Alexandra Stan feat. Carlprit with the single "1.000.000"| url=http://www.mix1.de/music/alexandra-stan-feat-carlprit/1-000-000/|language=German | publisher=Mix1|accessdate=7 September 2016}}</ref> Music website Digijunkies praised Carlprit's contribution on "1.000.000", concluding that the recording is "supple on the dance floor and affectionate in the ear canal."<ref name="digi junkies">{{cite web | title=Alexandra Stan: Die neue Sommer-Hitsingle "1.000.000"| trans_title=Alexandra Stan: The new hit single for the summer: "1.000.000"| url=http://www.digijunkies.de/alexandra-stan-die-neue-sommer-hitsingle-1-000-000-40434|language=German | publisher=Digi Junkies|accessdate=7 September 2016}}</ref> In a mixed review for ''Saxobeats'', ''Yam Magazine'' declared that the single "stands out for sounding different than the rest of the tracks [...] and Stan expresses herself well in the genre."<ref name="yam"/>

Commercially, "1.000.000" experienced minor success in Europe. On her native [[Romanian Radio Airplay Chart|Airplay 100]], it entered the top twenty of the chart on 2 January 2012 at number seventeen,<ref name="Romania"/> following which it dropped to number eighteen the next week,<ref>{{cite web |url=http://mediaforest.ro/WeeklyCharts/HistoryWeeklyCharts.aspx|title=Media Forest – Know You Are On Air|publisher=[[Media Forest]] |accessdate=8 September 2015}} ''Note: Select '2012' and 'Week: 02. Period: 09-01-12 15-01-12' from the drop-down menu.''</ref> and returned to its peak position on 16 January 2012.<ref>{{cite web |url=http://mediaforest.ro/WeeklyCharts/HistoryWeeklyCharts.aspx|title=Media Forest – Know You Are On Air|publisher=[[Media Forest]] |accessdate=8 September 2015}} ''Note: Select '2012' and 'Week: 03. Period: 16-01-12 22-01-12' from the drop-down menu.''</ref> In Russia, the song opened the [[Tophit]] chart at position 230 on 22 January 2012, with it reaching its highest peak at number 203 after two weeks.<ref name="Russia"/> The track managed to chart at number thirty-four in Italy in its sixth week,<ref name="Italy"/> and at twenty-one in Spain.<ref name="Spain"/>

==Music video==
An accompanying music video for the song was uploaded onto the [[YouTube]] channel of Prodan's label, Maan Studio, on 22 December 2011.<ref name="video">{{cite web | title=Alexandra Stan feat. Carlprit – 1.000.000 (Official video)| trans_title=| url=https://www.youtube.com/watch?v=cs4InpxwGw0|language= | publisher=[[YouTube]]|accessdate=7 September 2016|date=22 December 2011}}</ref> It was directed by Iulian Moga in [[Bucharest]] in the spawn of thirty hours, in over five different settings, with Stan wearing six outfits throughout the clip.<ref name="ziarul ring">{{cite web | title=Alexandra Stan isi lanseaza in exclusivitate la MTV clipul "1.000.000"| trans_title=Alexandra Stan releases her music video for "1.000.000" exclusively for MTV| url=http://www.ziarulring.ro/monden/alexandra-stan-isi-lanseaza-in-exclusivitate-la-mtv-clipul-1-000-000|language=Romanian | publisher=''Ziarul Ring''|accessdate=7 September 2016}}</ref><ref name="libertatea">{{cite web | title=Alexandra Stan, mai sexy ca niciodată. Arată de milioane în videoclipul "1.000.000"| trans_title=Alexandra Stan, sexier than ever. She looks million-worth in the music video for "1.000.000"| url=http://www.libertatea.ro/monden/vedete-de-la-noi/alexandra-stan-mai-sexy-ca-niciodata-arata-de-milioane-in-videoclipul-1000000-fotovideo-682843|language=Romanian | publisher=''[[Libertatea]]''|accessdate=8 September 2016|date=23 December 2011}}</ref><ref name="enational">{{cite web | url=http://www.enational.ro/old/alexandra-stan-a-filmat-un-videoclip-de-milioane-110896.html/| title=Alexandra Stan a filmat un videoclip de milioane!|language=Romanian | publisher=''National''|accessdate=8 September 2016|author=Purice, Ciprian|date=7 December 2012|trans_title=Alexandra Stan has fimed a million-worth music video}}</ref> About the clothing, she confessed during an interview that she chose to look "precious and glamorous mixed with something urban, R&B and hip-hop" after consulting with her stylist Andra Moga.<ref name="utv interview"/> One of her pieces included a pair of shoes which were custom made in a span of forty-eight hours.<ref name="utv interview"/> Premiering on [[MTV]] on 21 December 2011 at 12:00, the visual does not have a plot, serving to emphasis her image.<ref name="ziarul ring"/><ref name="devorator monden">{{cite web | title=Maine, Alexandra Stan va lansa videoclipul "1.000.000" feat. Carlprit| trans_title=Tomorrow, Alexandra Stan will release the music video for "1.000.000" done in collaboration with Carlprit| url=http://devoratormonden.ro/maine-alexandra-stan-va-lansa-videoclipul-1-000-000-feat-carlprit|language=Romanian | publisher=Devorator Monden|accessdate=8 September 2016|date=20 December 2011|author=Baltaretu, Eugen|archivedate=21 September 2016|archiveurl=https://web.archive.org/web/20160921183230/http://devoratormonden.ro/maine-alexandra-stan-va-lansa-videoclipul-1-000-000-feat-carlprit}}</ref>

The clip opens with Carlprit riding a sparkling bicycle in front of a [[graffiti]] backdrop, with Stan sitting on a TV in front of him. Subsequently, they are both presented in a mirrored room. Until the track's breakdown is played, the two further dance to the song, following which the singer is portrayed captured in a cage and later laying on the floor covered by silver. The visual ends with the screen becoming dark and Stan looking into the camera.<ref name="video"/> Website Direct Lyrics named the music video "cute", further explaining that "although nothing really happens, Alexandra just hangs around wearing different clothes in different scenes, she takes this opportunity to further strut her sexy figure."<ref name="direct lyrics"/> [[Los 40 Principales]] cited the visual for "1.000.000" as one of Stan's best clips ever.<ref name="los40">{{cite web | title=Los mejores videoclips de Alexandra Stan | trans_title=The best videos of Alexandra Stan | url=http://los40.com/los40/2014/08/14/actualidad/1408031064_063471.html|date=14 August 2014|language=Spanish | publisher=[[Los 40 Principales]]|accessdate=20 June 2016}}</ref>

==Track listing==
;Official versions{{efn|group=upper-alpha|This acts as a summary of all versions of the single found on its digital releases.<ref name="iTunes.de"/><ref name="iTunes.it"/>}}
#1.000.000 (feat. Carlprit) – 3:18
#1.000.000 (feat. Carlprit) [Rico Bernasconi Remix] – 5:29
#1.000.000 (feat. Carlprit) [Rico Bernasconi Remix Edit] – 2:38
#1.000.000 (feat. Carlprit) [Maan Studio Remix] – 4:29

==Credits and personnel==
Credits adapted from the liner notes of ''Saxobeats'' and ''The Collection''.<ref name="credits">{{cite AV media notes |title=Unlocked |titlelink=Unlocked (Alexandra Stan album) |others=[[Alexandra Stan]] |year=2014 |type=Liner notes |publisher=[[Roton (label)|Roton]] (Barcode: 8693644012601) |location=[[Bucharest]], Romania}}</ref><ref>{{cite AV media notes |title=The Collection|others=[[Alexandra Stan]] |year=2015 |type=Liner notes |publisher=[[Victor Entertainment]] (Barcode: 4988002698196) |location=[[Tokyo]], Japan}}</ref>

*Alexandra Stan – lead [[vocals]]
*Carlprit – featured artist
*Iulian Moga – [[Music director|director]]
*Andrei Nemirschi – composer, producer, [[photography]]
*Marcel Prodan – composer, producer
*Marcian Alin Soare – composer

==Charts==
{| class="wikitable sortable plainrowheaders"
|-
! Chart (2012)
! Peak<br />position
|-
!scope="row"|Italy ([[Federation of the Italian Music Industry|FIMI]])<ref name="Italy">{{cite web |url=http://www.fimi.it/classifiche#/category:digital/id:1014|title=FIMI: Classifica settimanale WK 6 (dal 2012-02-06 al 2012-02-12) |publisher=[[Federation of the Italian Music Industry]] |accessdate=17 August 2015}}</ref>
| style="text-align:center;"|34
|-
!scope="row"|Romania ([[Romanian Radio Airplay Chart|Airplay 100]])<ref name="Romania">{{cite web |url=http://mediaforest.ro/WeeklyCharts/HistoryWeeklyCharts.aspx|title=Media Forest – Know You Are On Air|publisher=[[Media Forest]] |accessdate=8 September 2015}} ''Note: Select '2012' and 'Week: 01. Period: 02-01-12 08-01-12' from the drop-down menu.''</ref>
| style="text-align:center;"|17
|-
!scope="row"|Russia ([[Tophit]])<ref name="Russia">{{cite web |url=http://www.tophit.ru/ru/tracks/36661/view|title=Alexandra Stan feat. Carlprit – 1.000.000 – Tophit |publisher=[[Tophit]] |language=Russian|accessdate=8 September 2016}}</ref>
|style="text-align:center;"|203
|-
!scope="row"|Spain Airplay ([[PROMUSICAE]])<ref name="Spain">{{cite web |url=http://www.promusicae.es/listas/semanales|title=Weekly charts – Charts – Promusicae – Productores de Musica de España|publisher=[[PROMUSICAE]] |language=Spanish|accessdate=18 September 2016}} ''Note: Select 'Radio' from the first drop-down menu and then search for '2012' and 'Semana 18 – (30/04 a 06/05)' in the latter two, respectively.''</ref>
|style="text-align:center;"|21
|}

==Release history==
{| class="wikitable plainrowheaders"
|-
! scope="col"| Territory
! scope="col"| Format(s)
! scope="col"| Date
! scope="col"| Label
|-
!scope="row"| Italy<ref name="iTunes.it">{{cite web|url=https://itunes.apple.com/it/album/1.000.000-feat.-carlprit-single/id493989621|work=iTunes Store|date=20 January 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single di Alexandra Stan su Apple Music}}</ref>
| rowspan="7"|Digital single
| rowspan="1"|20 January 2012
| rowspan="1"|Vae Victis/<br />E2
|-
!scope="row"| Spain<ref>{{cite web|url=https://itunes.apple.com/es/album/1.000.000-feat.-carlprit-single/id500153998|work=iTunes Store|date=2 February 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single de Alexandra Stan en Apple Music}}</ref>
| rowspan="1"|2 February 2012
| rowspan="1"|Blanco y Negro
|-
!scope="row"| Japan<ref>{{cite web|url=https://itunes.apple.com/jp/album/1.000.000-feat.-carlprit-single/id508375405|work=iTunes Store|date=21 March 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single by Alexandra Stan on Apple Music}}</ref>
| rowspan="1"|21 March 2012
| rowspan="1"|Play On/<br />Jeff
|-
!scope="row"| Denmark<ref>{{cite web|url=https://itunes.apple.com/dk/album/1.000.000-feat.-carlprit-single/id541129698|work=iTunes Store|date=13 July 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single by Alexandra Stan on Apple Music}}</ref>
| rowspan="4"|13 July 2012
| rowspan="4"|Sony
|-
!scope="row"| Germany<ref name="iTunes.de">{{cite web|url=https://itunes.apple.com/de/album/1.000.000-feat.-carlprit-single/id542654266|work=iTunes Store|date=12 July 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single von Alexandra Stan auf Apple Music}}</ref>
|-
!scope="row"| Netherlands<ref>{{cite web|url=https://itunes.apple.com/nl/album/1.000.000-feat.-carlprit-single/id542654266|work=iTunes Store|date=12 July 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single van Alexandra Stan op Apple Music}}</ref>
|-
!scope="row"| Sweden<ref>{{cite web|url=https://itunes.apple.com/se/album/1.000.000-feat.-carlprit-single/id541129698|work=iTunes Store|date=12 July 2012|accessdate=8 September 2016|title=1.000.000 (feat. Carlprit) – Single av Alexandra Stan pa Apple Music}}</ref>
|-
!scope="row"| United Kingdom<ref>{{cite web|url=https://www.amazon.co.uk/000-Alexandra-Feat-Carlprit-Stan/dp/B0089GH25S/ref=sr_1_1?s=music&ie=UTF8&qid=1473321631&sr=1-1&keywords=1.000.000+alexandra+stan|publisher=[[Amazon.com|Amazon]]|accessdate=8 September 2016|title=1.000.000 by Alexandra Stan feat. Carlprit: Amazon.co.uk: Music}}</ref>
|CD single
|{{N/A}}
|{{N/A}}
|-
|}

==Notes==
{{notelist-ua|2}}

==External links==
*{{YouTube|id=cs4InpxwGw0|title=Official music video}}
*{{MetroLyrics song|alexandra-stan|one-million}}<!-- Licensed lyrics provider -->

==References==
{{reflist|2}}

{{Alexandra Stan}}
{{Good article}}

[[Category:2012 singles]]
[[Category:Alexandra Stan songs]]
[[Category:English-language Romanian songs]]
[[Category:Romanian songs]]
[[Category:German songs]]