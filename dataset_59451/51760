{{Infobox journal
| title = Journal of Business and Technical Communication
| cover = [[File:Journal of Business and Technical Communication.jpg]]
| editor = David R. Russell
| abbreviation = J. Bus. Tech. Comm.
| discipline = [[Business]], [[communication]]
| publisher = [[SAGE Publications|Sage Publications]]
| country =
| frequency = Quarterly
| history = 1987-present
| impact = 0.656
| impact-year = 2010
| website = http://jbt.sagepub.com/
| link1 = http://jbt.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jbt.sagepub.com/content
| link2-name = Online archive
| ISSN = 1050-6519
| eISSN= 1552-4574
| OCLC = 18973702
| LCCN = 91641390
| CODEN = JBTCE9
}}
The '''''Journal of Business and Technical Communication''''' is a quarterly [[peer review|peer-reviewed]] [[academic journal]] that focuses on communication best practices, problems, and trends in corporate and educational venues. The journal was established in 1987 and is published by [[Sage Publications]]. The [[editor-in-chief]] is David R. Russell ([[Iowa State University]]) Article types include case studies, commentaries, book and software reviews, and comments and responses.

== Abstracting and indexing ==
The ''Journal of Business and Technical Communication'' is abstracted and indexed in [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2010 [[impact factor]] is 0.656, ranking it 41 out of 67 journals in the category "Communication"<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Communication |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> and 81 out of 103 journals in the category "Business". <ref name=WoS1>{{cite book |year=2011 |chapter=Journals Ranked by Impact: Business |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.sagepub.com/journals/Journal200791/title}}

[[Category:SAGE Publications academic journals]]
[[Category:Business and management journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1987]]
[[Category:English-language journals]]
[[Category:Communication journals]]


{{business-journal-stub}}