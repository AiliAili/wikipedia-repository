{{distinguish2|the [[Journal of Korean Studies]], which was published from 1979–1992 and revived in 2008}}
{{Infobox Journal
| cover	=	[[Image:KoreanStudiescover.gif]]
| discipline	=	[[Korean studies]]
| abbreviation	=	''KS''
| frequency	=	annual
| website	=	http://www.hawaii.edu/korea/pages/Publications/ks.html
| publisher	=	[[University of Hawaii Press]]
| country	=	[[United States|USA]]
| history	=	1977 to present
| ISSN	=	0145-840X
| eISSN	=	1529-1529
}}
'''''Korean Studies''''' is an international, [[academic journal]] that seeks to further scholarship on Korea and Koreans abroad by providing a forum for interdisciplinary and multicultural articles, book reviews, and essays in the humanities and social sciences. 

The journal was founded at the [[University of Hawaii]] Center for Korean Studies under the directorship of [[Dae-Sook Suh]]. Volumes 1-3 list the Center faculty, but no editor. Volumes 4-6 list [[Peter D. Lee]] as editor, and subsequent volumes show editorial successions by Center faculty at intervals of (usually) three to six years. The journal continues to be edited at the UH Center for Korean Studies and published by the [[University of Hawaii Press]].

The cover design changed in 1996, on its 20th anniversary. The translation of its title as ''Hangukhak'', which appears in Korean script on the cover, has generated some controversy because only South Koreans call their country [[Hanguk]]. North Koreans call theirs Chosŏn (also spelled Joseon).

''Korean Studies'' appears annually. However, volumes 25–26 (2001–2002) contain two issues each. Its first electronic edition appeared in 2000 on [[Project MUSE]].

==See also==
* ''[[Korea Journal]]''

==External links==
* [http://www.hawaii.edu/korea/ Sponsor homepage]
* [http://www.uhpress.hawaii.edu/journals/ks/ Publisher homepage]
* [http://muse.jhu.edu/journals/ks/ MUSE homepage]

[[Category:Korean studies]]
[[Category:Asian studies journals]]
[[Category:Cultural journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1977]]
[[Category:Annual journals]]
[[Category:Academic journals published by university presses]]