{{Use South African English|date=May 2013}}
{{Use dmy dates|date=May 2013}}
{{Infobox military unit
|unit_name=1st South African Infantry Brigade
|image=
|caption=
|dates= Sep 1915 – ?<br />1926 – Jan 1943
|country={{flag|Union of South Africa|1928}}
|allegiance= Allied Forces, World War I and World War II
|branch=[[South African Army]]
|type= Infantry
|role=
|size= Brigade
|command_structure=
|garrison=
|garrison_label=
|nickname=
|patron=
|motto=
|colors=
|colors_label=
|march=
|mascot=
|equipment=
|equipment_label=
|battles=
|battles_label=
|anniversaries=
|decorations=
|battle_honours=
|disbanded=1 Jan 1943
<!-- Commanders -->
|commander1=Brig-Gen [[Henry Lukin|H. T. Lukin]]<br />Brig-Gen F. S. Dawson<br />Brig Gen W. E. C. Tanner
|commander1_label=Commander WWI
|commander2=Brig J. Daniel<br />Brig [[Dan Pienaar|D. H. Pienaar]]<br />Brig J. B. Kriegler<br />Brig J. P. A. Furstenberg<br />Brig E.P. Hartshorn<br />Brig [[Christiaan du Toit|C. L. de w. du Toit]]
|commander2_label=Commander World War II
<!-- Insignia -->
|identification_symbol=
|identification_symbol_label=
|identification_symbol_2=
|identification_symbol_2_label=
|identification_symbol_3=
|identification_symbol_3_label=
|identification_symbol_4=
|identification_symbol_4_label=
}}
{{Campaignbox Somme 1916}}
{{Campaignbox East African Campaign (World War II)}}
{{Campaignbox Western Desert}}

The '''South African 1st Infantry Brigade''' was an [[infantry]] [[Brigade (military)|brigade]] of the [[South African Army|army]] of the [[Union of South Africa]] during World Wars I and II. During [[World War I]], the Brigade served as a British formation in [[Egypt]] and on the [[Western Front (World War I)|Western Front]], most famously the [[Battle of Delville Wood]]. It was reactivated at the start of the [[Second World War]] as a South African formation and served in [[East African Campaign (World War II)|East Africa]] and the [[North African Campaign|Western Desert]]; the Brigade disbanded on 1 January 1943.
{{TOClimit}}

==World War I==
When the [[First World War]] broke out in 1914, the South African government chose to join the war on the side of the [[Allies of World War I|Allies]]. General [[Louis Botha]], the then prime minister, faced widespread [[Afrikaner]] opposition to fighting alongside Great Britain so soon after the [[Second Boer War]] and had to put down a revolt by some of the more militant elements before he could mobilise and deploy troops as an expeditionary force (some 67,000 troops) to invade [[German South-West Africa]] (now [[Namibia]]).

The South African Union Defence Act of 1914 prohibited the deployment of South African troops beyond the borders of the South Africa and its immediate neighbouring territories. To send troops to Europe to support the Commonwealth in World War I, Generals Botha and Smuts created the South African Overseas Expeditionary Force. However, because of the limitations of the Defence Act, they issued a General Order (Order 672 of 1915) which stated that "[[South African Overseas Expeditionary Force|The South African Overseas Expeditionary Force]] will [sic] be Imperial and have the status of regular British Troops."  "Status" was meant to imply administrative purposes, as Britain was paying for the maintenance of the force in the field for the sake of local political sensitivities. Regrettably, this Administrative Order later meant that the South African units which served as part of the [[South African Overseas Expeditionary Force|Overseas Expeditionary Force]] were not, as South African units, entitled to retain [[Regimental Colours]] awarded to them for battles fought as "British" units.<ref>Digby p.416</ref> The 1st Infantry Brigade Group was the first unit to be formed as a constituent part of the South African Overseas Expeditionary Force.

===Mobilisation===
[[Image:CAP BADGE 1ST SA INFANTRY BRIGADE.jpg|thumb|right|120px|Cap badge of 1st SA Infantry Brigade, 1914]]
The brigade was commanded by Brigadier General [[Henry Lukin|H.T. (Tim) Lukin]] and consisted of four regiments<ref name="SAMHJ">S Afr MHJ Vol 7 No 2</ref> recruited from existing military units as well as amongst civilians. Regiments were raised in the four provinces of South Africa:
* The 1st South African Infantry Regiment was commanded by Lt Col F.S. Dawson with the regiment being raised from the [[Cape Province]] and known as "The Cape Regiment."  A Company ([[Western Cape|Western Province]]) was made up from men mostly from the [[Cape Town Rifles|Duke of Edinburgh's Rifles]]. B. Company was recruited from the [[Eastern Cape|Eastern Province]] and C Company was from [[Kimberley, Northern Cape|Kimberley]], with many of the men being ex [[Kimberley Regiment]]. D Company was recruited from [[Cape Town]].<ref>Digby p. 18</ref>
* The 2nd South African Infantry Regiment was commanded by Lt Col W.E.C. Tanner, this Regiment was raised from [[Natal, South Africa|Natal]] and [[Orange Free State]]. Many volunteers were from the [[Kaffrarian Rifles]].<ref name="Digby p. 19">Digby p. 19</ref>
* The 3rd South African Infantry Regiment. Commander: Lt Col E.F. Thackeray and raised from [[Transvaal Province|Transvaal]] and [[Rhodesia]]. The regiment was generally known as "The Transvaal Regiment."  B Company were mostly from the [[Witwatersrand Rifles]] while C Company were men from the [[Rand Light Infantry]].<ref name="Digby p. 19"/>
* The 4th SA Infantry Regiment was led by Lt Col F.A. Jones, DSO and became known as the "South African Scottish." It was raised from the [[Cape Town Highlanders Regiment]] and the area of Cape Town (A Company) while members of 1st Bn [[Transvaal Scottish Regiment]] made up most of B Company. C Company came from 2nd Bn Transvaal Scottish Regiment and recruits encouraged by the Caledonian Societies of Natal and Orange Free State made up D Company.<ref name="Digby p. 19" />

The Brigade, numbering 160 officers and 5 648 other ranks, embarked for England from Cape Town and were quartered at Bordon in Hampshire, where, for the next two months, they underwent training.<ref name="SAMHJ" />

===North Africa===
{{main|Senussi Campaign}}

During December 1915 it was decided to send the South African brigade to Egypt, where the [[Senussi]] tribe led by Gaafer Pasha, was [[Senussi Campaign|threatening to overrun the country]]. On 23 January 1916 the 2nd South African Infantry Regiment first saw action at Halaxin.
Brig Gen Lukin's column of the [[Western Frontier Force]] comprised; 1st and 3rd South African Infantry Regiments, The [[Dorsetshire Yeomanry]], the 1st/6th [[Royal Scots]], a squadron of The [[Royal Buckinghamshire Yeomanry]] and the [[1/1st Nottinghamshire Royal Horse Artillery|Nottinghamshire Battery]] of the Royal Horse Artillery.<ref name="SAMHJ" />
They marched along the coast and engaged the enemy at the [[Battle of Agagia]] on 26 February 1916. With the aid of the Dorsetshire Yeomanry's cavalry the Senussi were routed and [[Jaafar Pasha|Gaafer Pasha]] and his staff captured.<ref>Digby p.66</ref> After successfully bringing this brief campaign to a close, Brig Gen Lukin and his brigade were transferred to France.

===Western Front===

====The Somme Offensive====
[[Image:German trench Delville Wood September 1916.jpg|right|thumb|An abandoned German trench in Delville Wood near Longueval, Somme, France.]]
The [[Battle of the Somme|Somme offensive]] opened on 1 July 1916 and was initially intended as a diversionary battle to draw German forces away from the French front at Verdun which was under severe pressure.<ref>Hart p.36</ref> Allied Command hoped that the preliminary barrage would destroy the German trenches, exterminating the defenders and enabling the Allied infantry to occupy the German lines with minimal opposition. This initial bombardment failed to neutralise the German infantry and British suffered in excess of 54 000 casualties in the first day's fighting, of whom over 19 000 were killed.<ref name="SAMHJ" />

The losses of 1 July 1916 were considerable and only countered partially by the successes achieved the same day in [[Henry Rawlinson, 1st Baron Rawlinson|Gen Rawlinson]]'s [[XIII Corps (United Kingdom)|XIII Corps]] sector, and this was to dramatically influence the South African Brigade in the coming offensive. [[Douglas Haig, 1st Earl Haig|General Haig]] realised that he had to capitalise on the successes achieved on the right of the British line and he urged General Rawlinson to exploit this by securing [[Mametz Wood]] and the Contalmaison area to prepare for an attack on the German second line on the [[Longueval]]-[[Bazentin le Petit]] ridge. This attack would extend on the right to Longueval Village and [[Delville Wood]]. First, however, [[Battle of Bazentin Ridge|Bernafay Wood]] and [[Battle of Bazentin Ridge|Trones Wood]], which were situated to the south of Longueval Village and Delville Wood, would have to be captured.<ref name="SAMHJ" />

====Longueval====
The plan called for the [[9th (Scottish) Division]] (which included the 1st South African Infantry Brigade) to be brought forward from reserve to the new line extending from Montauban to the south of Trones Wood. After coming forward, the division was told to prepare for the second stage of the battle, an assault on [[Longueval]] scheduled for 7 July. In advancing to the start-line, the 2nd South African Infantry Regiment (at that time, the reserve battalion), relieved two battalions of the 27th Brigade in Bernafay Wood and incurred over 200 casualties in the process.<ref name="SAMHJ" />

General Rawlinson decided on a night advance and dawn attack to take the village. The attacking force would consist of the 26th and 27th Brigades of the 9th Division, which would assault the village of Longueval at dawn on 14 July with the South African brigade remaining in reserve. As arranged, at dawn they stormed the German positions and fought their way into Longueval, where hard hand-to-hand fighting ensued. By 0805, the intensity of the fighting compelled Maj Gen W.T. Furse, (Commander of 9th Division), to order the 1st South African Infantry Regiment to advance from reserve in support of the 27th Brigade and by 1230, he instructed the remaining three South African regiments to take and hold Delville Wood as soon as the entire town of Longueval was in Allied hands.<ref name="Hart_284">Hart p.284</ref>

At 1300, 12th Royal Scots had pushed through the northern half of Longueval Village but were forced back by a machine gun in the north west corner of Delville Wood. This part of the wood was to remain strongly held and defended by the Germans. Due to delays in preparing the regiments on the start line, as well as problems related to co-ordinating the artillery support, the South African attack was delayed to 06h00 the following morning, particularly as Longueval village had not yet been totally captured and holding the town was considered essential to the capture of Delville Wood.<ref name="SAMHJ" />

====Delville Wood====

=====South Africans enter the Wood=====
[[Image:Battle of the Somme 1916 map.png|thumb|Map of the Battle of the Somme, depicting Delville Wood]]
The start of what was to become a legend of South African perseverance, loss and tragedy started at 0600 on 15 July 1916. The three remaining regiments of the South African brigade who were under the command of Lt Col William Tanner of the 2nd Regiment, advanced towards the wood with the 2nd and 3rd Regiments in the lead, followed by the 4th Regiment which was in support . The North West corner of the town and the wood was clearly strongly held by the Germans, but the positions in the rest of the wood were unclear, with the South Africans being uncertain as to who were friendly forces and who were enemy. Led by a guide from the 5th Camerons, the three regiments advanced from the junction of Montauban and Bazentin Roads through a portion of the south end of Longueval and across the fields to Buchanan Street trench. Tanner established his headquarters at Buchanan Street and sent the 3rd Regiment to the far side of the wood. Tanner's 2nd Regiment followed the 3rd Regiment but branched off to the north. C Company, 2nd Regiment manned the southern perimeter close to Longueval.<ref name="Hart_284"/> (See Map).

Shelling was extremely heavy with severe losses. Medical orderlies were being called for everywhere, on all fronts the supply of stretchers soon ran out. In addition, on the eastern perimeter there was confusion as to whether the men moving about outside the wood were French or German. The destruction of a Lewis Gun from this area soon confirmed that the forces were German. By 1000 casualties were mounting, particularly amongst Vickers and Lewis gun sections and calls for artillery support were coming in from all three regiments. By noon, ammunition stocks were running seriously low, and by 1600 the Germans mounted a strong counter attacks on the left flank (2nd Regiment) but were repelled. As dusk fell, the South Africans manning the perimeters entrenched themselves, despite continuous enemy shelling and sniping.<ref name="SAMHJ" />

The morning of 16 July, Brig Gen Lukin was ordered to support an attack by 11th Royal Scots (part of 27th Brigade) on the orchard situated in the northern sector of Longueval, between North Street and Flers Road. The 11th Royal Scots would attack along North Street, whilst B and C Companies of the 1st South African Infantry Regiment would attack northwards in the wood parallel to the Royal Scots.<ref name="SAMHJ" /> The combined attack was launched at l000 and was met by machine gun and rifle fire. Both assaults failed and survivors scrambled back to their positions, to face a day of shelling and sniping (Refer Map). Later the morning, Brig Gen Lukin visited Lt Col F.S. Dawson (OC of 1st South African Infantry Regiment) in Longueval and Dawson stressed to the brigade commander that the men were exhausted. Lukin replied that there could be no relief for several days. German artillery continued to pound the South Africans in the wood for the remainder of the day and well into the night.<ref name="SAMHJ" />

====='Friendly-fire' incidents=====
During the night of 16/17 July the north-west corner of Delville Wood was subjected to an Allied artillery barrage to support a combined attack by the 27th Brigade and 1st South African Infantry Regiment to be initiated by dawn. Once again the attack met with fierce resistance and it too failed. Brig Gen Lukin again visited the battalion commanders in Longueval during the day and on his return to brigade headquarters he telephoned Maj Gen Furse and pointed out that his troops were exhausted. Furse replied that the wood was to be held at all costs. By mid morning, medical orderlies could no longer cope with all of the wounded.<ref name="SAMHJ" /> The Germans were becoming more active in the north western sector of Delville Wood and at 1400 German batteries from Ginchy began bombarding the wood followed by an attack from the north-west, reaching Princes Street, but they were halted and then driven back by a counter-attack. That night the British artillery fired on the Germans who were east of Delville Wood with many shells falling short, amongst the South Africans. This was again followed by German artillery commencing their barrage on the wood. Many of the 186 German guns involved had been hurriedly transported from Verdun and explosions illuminated the forest in flashes, making sleep virtually impossible.<ref name="SAMHJ" />

Fighting continued throughout the day and that night, the Germans withdrew from the north-west corner of Delville Wood and northern Longueval to enable their artillery to bombard the entire Wood and village. This withdrawal allowed the 1st South African Regiment to push northwards and to link up with the 76th Brigade (3rd Division), which was similarly advancing on Longueval. The junction did not last long; at 08h00 on 18 July the German artillery commenced firing on Delville Wood again, but this time from three sides and the bombardment endured for seven-and-a-half hours. At times the incidence of explosions was seven per second. On that day, in an area less than one square mile, 20 000 shells fell.<ref>Hart p.287</ref>
<!---
Add to Gallantry awards:
"It was considered by many that Lt Philip's arrival in Delville Wood was instrumental in the success of the South African defence. Although severely wounded on the night of 18/19 July he remained at his post and was conspicuous in leading bombing counter attacks to meet the enerny's assaults. He was awarded the Military Cross (MC). He was wounded at the Butte de Warlencourt and died on 16 October 1916."
--->

=====Thackeray replaces Tanner=====
At 1450 Lukin advised Tanner, who had been wounded, that he was to assign command of the forces in the Wood to Colonel Thackeray of the 3rd Regiment.<ref>Hart p.288</ref>  He was instructed to bring forward all scratch reinforcements he could find and to take over command of the South African troops in the Wood, which he did – entering the wood with 150 men, all of whom were battle-weary as the result of three days fighting.  All Companies were by now calling for reinforcements or requesting authority to withdraw from the area being pounded by artillery.  The reply was that "...Delville Wood is to be held at all costs."  Casualties were further increasing by the hour in all sectors and in the early afternoon, A and C Companies of the 3rd Regiment were overrun by the Germans, who approached from the rear; through the devastated wood.   Mud blown up by the intense barrage had caused most weapons to stop working, cleaning equipment had all been consumed and the troops had now been without food for over 72 hours and more importantly – they were now without water too.<ref name="SAMHJ" />

Another German attack at 1700 was rebuffed but by now, companies were reduced to so few men, that they could no longer be considered as viable fighting units.  The South Africans still held an uncertain perimeter but German incursions through their line into the wood were now becoming more and more frequent, simply due to the lack of troops to cover the long perimeter line.<ref name="SAMHJ" />

=====Loss of the 3rd Regiment=====
The Germans commenced their advance at 0600 on 19 July.  Colonel Konemann led a German force comprising elements of the 153rd Infantry Reserve Regiment and two companies of the 52nd Infantry Reserve Regiment from the north into Delville Wood, attacking B Company of the 3rd South African Regiment.  The 2nd Regiment had been decimated the previous day and had left a large gap on the left flank of the 3rd Regiment and this was where the German penetration was made.  With so few men left, the German assault could not be countered and the remaining members of the 3rd Regiment were taken prisoner.<ref>Hart p.289</ref>

The Wood was by now, void of any vegetation and German machine guns and snipers were taking their toll those left within the 2nd Regiment.  Continued calls for reinforcements were met with words of encouragement, rather than with fresh troops – as fighting on all remaining fronts prevented any troop movement and had already consumed all available reserves.  At dawn on 20 July, Colonel Thackeray despatched a message to Lukin, urgently requesting supplies, water and ammunition. Despite their perilous situation, the South African survivors continued to fight.<ref>Digby p.134</ref>

Unknown to Thackeray, The Royal Welsh Fusiliers (Headquarters, Machine Gunners and signallers) were trying to advance to relieve the South Africans, but were continually driven back and were unable to reach them.  By 1300 Thackeray sent a signal to Lukin stating that "....Urgent. My men are on their last legs. I cannot keep some of them awake. They drop with their rifles in hand asleep in spite of heavy shelling. We are expecting an attack. Even that cannot keep some of them from dropping down. Food and water has not reached us for two days – though we have managed on rations of those killed ...but must have water."<ref name="Digby p.136">Digby p.136</ref>

=====Relief=====
Efforts by the Brigade Major John Mitchell-Baker eventually managed to secure additional troops to try to relieve the remaining South Africans.  At 1615 Brigadier-General H.W. Higginson of the 53rd Brigade reported that The Suffolk [Suffolk Regiment] and 6 R Berks [6th Battalion, Royal Berkshire Regiment] had been ordered to relieve them.<ref name="Hart p.291">Hart p.291</ref> When the Suffolks and Berks reached them, Thackeray and his remaining two officers, Lt Edward Phillips and 2 Lt Garnet Green, had all been wounded. He and Phillips led the 120 survivors of the 3rd Regiment out of the Wood. Green brought up the rear and was the last South African to leave the wood.<ref name="Digby p.136" />

On reaching safety, Thackeray reported "...I am glad to report that the troops under my command (the 3rd Regiment) carried out your instructions to hold Delville Wood at all costs and that not a single detachment of this regiment retired from their position, either on the perimeter of the Wood or from the support trenches."<ref name="SAMHJ" />

Historians today agree that the losses incurred by the South African Infantry Brigade holding Delville Wood had no strategic purpose, as did that of the entire Somme offensive, of which Delville Wood formed a small part.<ref name="SAMHJ" />

====Casualties====

The most costly action that the South African forces on the Western Front fought was the [[Battle of Delville Wood]] in 1916 – of the 3,153 men from the brigade who entered the wood, only 780 were present at the roll call after their relief.<ref name="Hart p.291" />

===Demobilisation===

====The end in France====
By the time the South Africans crossed the River Selle at Le Cateau, it was evident that the war was drawing to a close. The SA Brigade was withdrawn from the line at 0130 on 20 October 1918 and marched via Reumont to Serain.<ref name="SAMHJ" /> The brigade remained at Serain until 1 November 1918.<ref>Digby p. 377</ref>

====Volunteers to support the White Russians====
In December 1917, the [[Don Cossacks]] had risen in revolt against the Communist Government in Russia. With light skirmishes at first, in the areas of [[Odessa]], [[Kiev]], [[Oryol|Orel]], [[Voronezh]], [[Isartzin]] and even reaching to [[Astrakhan]] at the mouth of the [[Volga]], the uprising grew in size and geographic distribution. In August 1918, a small [[Allied intervention in the Russian Civil War|British-French-American force]] under command of Major General Sir [[Edmund Ironside, 1st Baron Ironside|Edmund Ironside]] had arrived at [[Archangelsk]], with the stated purpose of retrieving war material loaned to the previous Tsarist Regime. They were also tasked with to moving south to link up with the 42,000 strong [[Czech Legion]] enveloped in Russia, assisting them to return home. It was hoped that the presence of this Allied force, as well as the Czech force moving back towards Germany, would firstly invigorate the White Russian counter-revolution to oust the Communists and secondly, to encourage the Czechs to take up arms against Germany – with the aim of re-opening a second front against Germany. By this time, a number of South African officers were already fighting on the side of the White Russians against the Communists.<ref>Digby p. 366</ref> With the signing of the Armistice marking the end of the war on 11 November 1918, thousands of South Africans were released from their duties in Western Europe,<ref>Digby p. 368</ref> many of whom preferred to volunteer for services in support of the White Russians, rather than returning home. Many South Africans not only joined the White Russian forces, but were awarded honours for service in Russia, including Lt-Col H.H. Jenkins, the erstwhile commander of the 1st South African Infantry Regiment<ref>Digby p.371</ref> as well as the new commander of the 4th South African Infantry Regiment, Lt-Col D.M. McCloud,<ref>Digby p.372</ref> with the men either joining General Ironside's staff, or affiliating themselves directly with White Russian forces. Two [[Victoria Cross]] holders also joined this voluntary force.<ref>Digby p. 376</ref>

==World War II==
Brigades were reformed in the [[Active Citizen Force]] (ACF) for the first time since the [[First World War]] in 1926.<ref>http://ibiblio.org/hyperwar/UN/SouthAfrica/EAfrica/EAfrica-A1.html</ref> In 1934 a 1st Brigade of the ACF of the UDF was listed as comprising 1 [[Royal Natal Carbineers]], 2 RNC, the Umvoti and [[Natal Mounted Rifles]], and the [[Durban Light Infantry]].<ref>Defence Dept. Annual Report, 30 June 1934, p.3, via http://ibiblio.org/hyperwar/UN/SouthAfrica/EAfrica/EAfrica-A2.html</ref> Brigade commanders were nominated by 1934 as well, and Colonel H. Mayne VD became commander of the 1st Brigade. In 1940, the [[Union Defence Force (South Africa)|Union Defence Forces]] formed a new series of divisions for service in World War II. The 1st Brigade was earmarked for service with the [[1st South African Division]].

The brigade assembled in [[Pretoria]] under the command of Colonel John Daniel in early May 1940. He was replaced a few weeks later by Brigadier [[Dan Pienaar]]. On formation the brigade included three infantry [[battalion]]s, the 1st Battalion [[Transvaal Scottish Regiment]], the 1st Battalion, [[Cape Town Rifles|Duke of Edinburgh's Own Rifles]] and the 1st Battalion of the Royal Natal Carbineers. Soon after its formation, the brigade received transport for equipping one motorised battalion, and this was assigned to the 1st Transvaal Scottish.

The Brigade assembled at Sonderwater, located east of [[Cullinan, Gauteng|Cullinan]], and took part in a pre-departure [[parade]] attended by [[Jan Smuts|General J.C. Smuts]], the prime minister and defence minister, on 13 July 1940.<ref name="D.D. Form 293">D.D. Form 293</ref> The date also celebrated the [[Delville Wood]] Day, as the anniversary of a battle on the Western Front in July 1916 when the 1st South African Brigade had advanced into the Delville Wood. The next day the Brigade entrained for [[Durban]], and on 16 July embarked by ship for [[Mombasa]], [[Kenya]] where it commenced training not far from [[Nairobi]] in the Kenyan Highlands.<ref name="D.D. Form 293" /> On 6 September 1940, the 1st Transvaal Scottish was transferred to the 2nd East African Brigade under British command, and took part in the first action involving South African ground troops in the Second World War near [[Liboi]] when a column was attacked by a force of [[Bands (Italian Army irregulars)|Banda]] and [[Italian Colonial Empire|Italian Colonial]] infantry.<ref>Orpen Vol VIII</ref>

Although nominally part of the 1st South African Division, the brigade was deployed under 11th and 12th African Divisions. It fought in the campaign in Italian Somaliland, and in the conquest of Ethiopia in 1941.

From East Africa, the brigade – reassigned to 1st South African Division – was transferred to Egypt. It fought in the North Africa campaign from July 1941 until after the Battle of [[El Alamein]] in October/November 1942. The brigade returned to South Africa in January 1943, and was converted into the [[1st South African Armoured Brigade]], to serve as a training formation for the rest of the war.

==Order of battle==

===World War I===
Brigade commanded by Brigadier-General H.T. Lukin<ref name="SAMHJ">S Afr MHJ Vol 7 No 2</ref>
* 1st South African Infantry Regiment: Lt-Col F.S. Dawson<ref name="PAP">Digby p.400</ref>
* 2nd South African Infantry Regiment: Lt-Col W.E.C. Tanner<ref name="PAP" />
* 3rd South African Infantry Regiment: Lt-Col E.F. Thackerey<ref name="PAP" />
* 4th South African Infantry Regiment: Lt-Col F.A. Jones <small>DSO</small><ref name="PAP" />

===World War II===
Order of Battle as at 17 October 1942.<ref>Orpen Vol III Appendix 4</ref> 
Brigade commanded by Brigadier E.P. Hartshorn
* 1st [[Duke of Edinburgh's Own Rifles]] SA Infantry Corps (Lieutenant Colonel S.B. Gwillam)
* 1st [[Royal Natal Carabineers]] SA Infantry Corps (Lieutenant Colonel Len Hay, M.C.)
* 1st [[Transvaal Scottish Regiment|Transvaal Scottish]] SA Infantry Corps
* One Sqn 3rd SA Armoured Car Regt SA Tank Corps
* 3rd and 4th Anti-Tank Batteries SA Artillery Corps
* 1st Light Anti-Aircraft Battery SA Artillery Corps
* 1st Field Company SA Engineering Corps
* 11th and 15th Field Batteries of 4th Field Regt SA Artillery Corps
* 7th, 19th and 20th Field Batteries of 7th Field Regt SA Artillery Corps

==Battle honours==

===World War I===
Honours shown in bold are emblazoned on the regimental colours of the four regiments:<ref>Digby Appendix D</ref>
{{colbegin}}
* 1st South African Infantry Regiment
** '''Agagiya, Egypt''' 1916
** '''Somme''' 1916 '''Delville Wood'''
** Le Transloy, '''Arras''' 1917
** Scarpe 1917
** '''Ypres''' 1917
** '''Menin Road''', Lys
** '''Messines''' 1918
** '''Kemmel''' 1918
** Hindenburg Line, '''Cambrai''' 1918
** '''Selle''', France and Flanders 1916–1918
* 2nd South African Infantry Regiment
** '''Egypt''' 1916
** '''Somme''' 1916
** '''Delville Wood''' Le Transloy
** '''Arras''' 1917
** Scarpe 1917
** '''Ypres''' 1917, '''Menin Road'''
** '''Lys, Messines''' 1918
** '''Kemmel''' 1918
** Hindenburg Line, '''Cambrai''' 1918
** '''Selle''', France and Flanders 1916–1918
* 3rd South African Infantry Regiment
** '''Agagiya, Egypt''' 1916
** '''Somme''' 1916
** '''Delville Wood'''
** Le Transloy, '''Arras''' 1917
** '''Scarpe''' 1917
** '''Ypres''' 1917,
** '''Menin Road'''
** '''Passchendaele'''
** '''France and Flanders''' 1916–1918
* 4th South African Infantry Regiment
** '''Egypt''' 1916
** '''Somme''' 1916
** '''Delville Wood''' Le Transloy
** '''Arras''' 1917
** Scarpe 1917
** '''Ypres''' 1917, '''Menin Road'''
** Lys, '''Messines''' 1918
** Kemmel 1918
** '''Hindenburg Line, Cambrai''' 1918
** '''Pursuit to Mons, France and Flanders''' 1916–1918
{{colend}}
<!---
a. Record which honours were not recognised, b. Narrate Anglican church declaring World War I colours as being unwelcome in post Apartheid South Africa
--><!--
This section does not belong under this article, we should merely cite the incidents where members of the brigade committed deeds for which they were awarded key medals.  Page is about the Brigade and not about South African VC's or MC's

==Gallantry awards==
===World War I===
{{Standard table|0}}
! style="text align: left; background: #eeefff;"|Name
! style="text align: left; background: #eeefff;"|Rank
! style="text align: left; background: #eeefff;"|Unit
! style="text align: left; background: #eeefff;"|Date
! style="text align: left; background: #eeefff;"|Award
! style="text align: left; background: #eeefff;"|Citation
|-
|W.H. Barrand<ref>NASA Volume: 724 System: 01 Reference: 9/227/36</ref>
|Captain
|
|
|[[Military Cross]]
|
|-
|[[William Frederick Faulds]]<ref name="samilitaryhistory.org">{{cite web|url=http://samilitaryhistory.org/vol102gs.html|title=The South African Military History Society|work=Military History Journal – Vol 10 No 3|accessdate=2009-07-23|archiveurl=http://www.webcitation.org/5iXTzNTRK?url=http://samilitaryhistory.org/vol102gs.html|archivedate=25 July 2009|deadurl=no}}</ref>
|Private
|1st South African Infantry Battalion
|18 July 1916
|[[Victoria Cross]]
|"A bombing party under Lieut Craig attempted to rush over 40 yards (36 m) of ground which lay between the British and enemy trenches. Coming under very heavy rifle and machine gun fire the officer and the majority of the party were killed and wounded. Unable to move, Lieut Craig lay midway between the two lines of trench, the ground being quite open. In full daylight, Pte Faulds, accompanied by two other men, climbed over the parapet, ran out, picked up the officer, and carried him back... Two days later Private Faulds again showed most conspicuous bravery in going out alone to bring in a wounded man, and carried him nearly half a mile to a dressing-station... The artillery fire was at the time so intense that stretcher-bearers and others considered that any attempt to bring in the wounded man meant certain death... " ''Delvile Wood''
|-
|[[William Frederick Faulds]]<ref name="samilitaryhistory.org"/>
|Temporary Lieutenant
|1st South African Infantry Battalion
|22 March 1918
|[[Military Cross]]
|"In the retirement from the line east of Hendicourt, 22nd March, 1918, he was commanding one of the platoons which formed the rear-guard. He handled his men most ably, and exposed himself freely. Though the enemy pressed hard, he, by his fearless and able leadership, checked them, and enabled the remainder of the battalion to withdraw with slight loss"  ''Hendicourt''
|-
|Louis Esselen<ref>NASA Volume: 1277 System: 01 Reference: 34/828</ref>
|Captain
|
|
|[[Military Cross]]
|Awarded but was declined by Capt. Esselen. 
|-
|William Henry Hewitt<ref>Uys ''For Valour'' p.245</ref>
|Lance-Corporal
|2nd South African Infantry Battalion
|20 September 1917
|[[Victoria Cross]]
|"Lance Corporal Hewitt attacked a "pill box" with his section and tried to rush the doorway. The garrison however, proved very stubborn, and in the attempt this non-commissioned officer received a severe wound. Nevertheless, he proceeded to the loophole of the "pill-box" where, in his attempts to put a bomb into it, he was again wounded in the arm. Undeterred however, he eventually managed to get a bomb inside, which caused the occupants to dislodge, and they were successfully and speedily dealt with by the remainder of the section" ''Ypres, Belgium''
|-
|E. Hill<ref>NASA Volume: 724 System: 01 Reference: 9/227/38</ref>
|Captain
|
|
|[[Military Cross]]
|
|-
|Tom Maginess<ref>NASA Volume: 724 System: 01 Reference: 9/227/32</ref>
|Captain
|
|
|[[Military Cross]]
|
|-
|G.H.B. Raymond<ref>NASA Volume: 723 System: 01 Reference: 9/227/27</ref>
|Captain
|
|
|[[Military Cross]]
|
|-
|J. Traas<ref>NASA Volume: 1282 System: 01 Reference: 34/1047</ref>
|Captain
|
|
|[[Military Cross]]
|
|-
|W.J. Wright<ref>NASA Volume: 723 System: 01 Reference: 9/227/29</ref>
|Captain
|
|
|[[Military Cross]] and Bar
|
|}

===World War II===
{{Standard table|0}}
! style="text align: left; background: #eeefff;"|Name
! style="text align: left; background: #eeefff;"|Rank
! style="text align: left; background: #eeefff;"|Unit
! style="text align: left; background: #eeefff;"|Date of action
! style="text align: left; background: #eeefff;"|Award
! style="text align: left; background: #eeefff;"|Citation
|-
|Allen Harry Ernest Marsden Shaw
|Lieutenant (Acting Captain)
|1st [[Duke of Edinburgh's Own Rifles]]
|21 April 1941 and 16 May 1941
|[[Military Cross]]
|Leadership during the capture of an enemy roadblock in the advance on [[Dessie]]; and holding the furthest forward position during 2nd Battle of [[Amba Alagi]], credited with contributing to the final surrender of the [[Duke of Aosta]] during the [[East African Campaign (World War II)|East African Campaign]].<ref>{{LondonGazette|issue=35526|startpage=1694|date=14 April 1942|accessdate=23 July 2009}}</ref><ref>{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7658896|title=Recommendations for Honours and Awards (Army)—Image details—Shaw, Allen Harry Ernest Marsden|work=DocumentsOnline|publisher=[[The National Archives]]|format=fee usually required to view pdf of full original recommendation|accessdate=23 July 2009}}</ref><ref>Orpen Vol III Pg401</ref>
|-
|[[Quentin Smythe|Quentin George Murray Smythe]]
|Sergeant
|[[Royal Natal Carabineers]]
|5 June 1942
|[[Victoria Cross]] 
|During an attack on an enemy strong-point, in which his officer was severely wounded, Sergeant Smythe took command of a platoon although himself suffering from a shrapnel wound to the forehead. The strong-point having been overrun, our troops came under enfilade fire from an enemy machine-gun nest. Realising the threat to his position, Sergeant Smythe himself stalked and destroyed the nest with hand grenades, capturing the crew. Although weak from loss of blood, he continued to lead the advance and, on encountering and enemy anti-tank position, captured the crew. He was directly responsible for killing several of the enemy, shooting some and bayoneting another as they withdrew.

After consolidating the position, he successfully defeated an enemy attempt at encirclement.

Throughout the engagement, Sergeant Smythe displayed leadership which was an inspiration to those who followed him.<ref>{{LondonGazette|issue=35526|supp=yes|startpage=3954|date=8 September 1942|accessdate=23 July 2009}}</ref><ref>Uys p332</ref>
|-
|}

END OF SECTION TO CUT
Maybe it belongs in some other page!
--->

==See also==
{{portal|War}}
* [[1st Infantry Division (South Africa)|1st South African Infantry Division]]
* [[Delville Wood]]
* [[Battle of the Somme]]

==Citations==
{{reflist|20em}}

==References==

;Books
{{refbegin}}
* {{cite book |last=Digby |first=Peter K. |title=Pyramids and Poppies: The 1st SA Infantry Brigade in Libya, France and Flanders: 1915–1919 |year=1993 |publisher=Ashanti |location=Rivonia |isbn=1-874800-53-7}}
* {{cite book |last=Hart |first=Peter |title=The Somme |year=2006 |publisher=Cassell Military Paperbacks |location=London |isbn=978-0-304-36735-1}}
* {{cite book |last=Hartshorn |first=E. P. Brigadier |title=Avenge Tobruk |year=1960 |publisher=Purnell & Sons |location=Cape Town |oclc=  26084683}}
* Malcolm, Horace Thomas. Form D.D. 293. Record of Service.
* {{cite book |last=Orpen |first=N. |series=South African Forces World War II |title=East African and Abyssinian Campaigns |year=1968 |publisher=Purnell |location=Cape Town |volume=I |edition=online |url=http://www.ibiblio.org/hyperwar/UN/SouthAfrica/EAfrica/index.html |accessdate=8 September 2016 |oclc=165024905}}
* {{cite book |last=Orpen |first=N. |title=War in the Desert |year=1971 |publisher=Purnell |location=Cape Town |series=South African Forces World War II |volume=III |isbn=978-0-360-00151-0}}
* {{cite book |last=Orpen |first=N. |title=Salute the Sappers: The Operations of the South African Engineer Corps in the North African and Italian Theatres of War |year=1982 |publisher=Purnell |location=Cape Town |series=South African Forces World War II |volume=VIII |others=Part 2 |isbn=978-0-620-05376-1}}
* {{cite book |last=Uys |first=Ian |title=Delville Wood |year=1983 |publisher=Uys |location=Rensburg |isbn=978-0-620-06611-2}}
* {{cite book |last=Uys |first=Ian |title=For Valour: The History of South Africa's Victoria Cross Heroes |year=1973 |publisher=Uys |location=Johannesburg |isbn=978-0-620-00822-8}}

;Websites
* {{cite web|url=http://www.national.archsrch.gov.za/sm300cv/smws/sm300dl |title=National Archives of South Africa (NASA) |work=On-line index of archived data (Use Database SAB: "Public Records of Central Government since 1910") |accessdate=2009-07-23 |archiveurl=http://www.webcitation.org/5iXTyvXka?url=http%3A%2F%2Fwww.national.archsrch.gov.za%2Fsm300cv%2Fsmws%2Fsm300dl |archivedate=25 July 2009 |deadurl=no |df=dmy }}
* {{cite web|url=http://samilitaryhistory.org/vol072iu.html |title=The South African Military History Society |work=The South Africans at Delville Wood |pages=Military History Journal (S Afr MHJ) |volume=VII |issue=2 |accessdate=2009-07-23 |archiveurl=http://www.webcitation.org/5iXU0fHWz?url=http%3A%2F%2Fsamilitaryhistory.org%2Fvol072iu.html |archivedate=25 July 2009 |deadurl=no |df=dmy }}

{{World War I}}
{{World War II}}

[[Category:Infantry brigades of South Africa in World War II]]
[[Category:Military units and formations established in 1940]]
[[Category:Allied intervention in the Russian Civil War]]
[[Category:Expatriate units and formations]]
[[Category:Military units and formations of South Africa in World War I]]
[[Category:South Africa–Soviet Union relations]]