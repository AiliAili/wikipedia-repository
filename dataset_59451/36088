{{good article}}
{{Infobox video game
|title = Alien Soldier
|image = Alien Soldier Coverart.png
|caption = Japanese cover art
|developer = [[Treasure (company)|Treasure]]
|publisher = [[Sega]]
|designer = 
|producer = Hideyuki Suganami
|composer = Norio Hanzawa
|released= <!-- Do not list emulated releases here (i.e. Virtual Console) per [[Template:Infobox_video_game#platforms]] -->{{Vgrelease|JP|February 24, 1995|PAL|June 1, 1995|NA|1995 <small>([[Sega Channel]])</small>}}
|genre = [[Shoot 'em up#Run and gun|Run and gun]]
|modes = [[Single-player video game|Single-player]]
|platforms = [[Sega Genesis|Mega Drive]]<!-- Do not list emulated releases here (i.e. Virtual Console) per [[Template:Infobox_video_game#platforms]] -->
}}

'''''Alien Soldier'''''{{efn|{{nihongo|''Alien Soldier''|エイリアンソルジャー|Eirian Sorujā}}}} is a 1995 [[Side-scrolling game|side-scrolling]] [[Shoot 'em up#Run and gun|run and gun]] video game developed by [[Treasure (company)|Treasure]] for the [[Sega Genesis|Mega Drive]]. Retail copies were released in Japan and [[PAL territories]] while in North America it was only available exclusively via the [[Sega Channel]] cable service. The story follows a powerful being named Epsilon-Eagle, who after being nearly killed becomes determined to avenge his near death and save his planet. The character has a variety of weapons and moves that the player must master to complete the game. Many gameplay ideas are borrowed from Treasure's earlier Mega Drive release, ''[[Gunstar Heroes]]''. However, ''Alien Soldier'' puts an emphasis on challenging [[boss (video game)|boss]] fights with short and easy levels serving as downtime in-between.

Development lasted two years and was led by Hideyuki Suganami, who originally wanted to make the entire game himself but ultimately received support. He had ambitious plans for ''Alien Soldier'' but in order to meet his deadline, the game was released at roughly half-completion. ''Alien Soldier'' was released towards the end of the Mega Drive's lifecycle and was explicitly targeted for "hardcore" Mega Drive gamers. Critics have praised the game for its graphics, soundtrack, and overall intensity, recommending it for fans of ''Gunstar Heroes'' and the run and gun genre. Some negative critique was directed towards the hard difficulty, steep learning curve, and unorthodox gameplay. It was ported to the [[PlayStation 2]] in Japan, and also re-released worldwide on the [[Wii]] [[Virtual Console]] and [[Steam (software)|Steam]].

==Gameplay==
''Alien Soldier'' is a [[Side-scrolling game|side-scrolling]] [[Shoot 'em up#Run and gun|run and gun]] video game in which the player controls the main character, Epsilon-Eagle, through 25 stages and 31 bosses.<ref name=ign/><ref name=eurog/> The gameplay has been compared heavily to ''[[Gunstar Heroes]]''; however, putting a much larger emphasis on boss fights, and lacking any 2-player option.<ref name=ign/> The stages are notably short, populated with weak enemies to serve as downtime between the more difficult boss battles. There are two difficulties in the game, "supereasy" and "superhard". Enemies on the hard setting are only slightly more difficult; however, there are no passwords or unlimited continues as in the easy mode.<ref name=gamespot/>

[[File:AlienSoldierScreenshot.jpeg|thumb|Epsilon-Eagle firing at enemies in the first stage]]
Epsilon-Eagle can run, [[double jump (video gaming)|double jump]], hover in the air, and use six different types of weapons. Only four can be equipped at any given time, but the player may choose which weapons they would like to equip before the game starts. Each gun has its own ammunition bar which can be replenished; if it's depleted the player will be left with a little firepower. The player can also perform a dash across the screen, which when at full health, will become a deadly move called "Phoenix Force" that will damage any enemies in its path. There is also a counter move, that if timed properly, will change enemy bullets into health.<ref name=gamespot/> Epsilon-Eagle can attack in either fixed-fire or free-moving styles. The first makes him immovable while firing a weapon, instead allowing for quick aiming, while the second allows walking and shooting simultaneously with the sacrifice of directional accuracy.<ref name=ign/>

==Synopsis==
The premise of ''Alien Soldier'' is provided with a long text scroll at the start of the game. After the game has begun, it is not referred to again.<ref name=ign/> In the year 2015, the "A-Humans" of "A-Earth"{{efn|The Japanese version uses the terms "A-Humans" and "A-Earth" while the European version uses "Sierrans" and "Sierra"}} have created genetically engineered A-Humans capable of super intelligence and strength, as well as parasitic co-existence with machinery and animals, particularly humans. A terrorist organization known as "Scarlet" rose up within this race and sought to dominate the rest of the A-Humans and A-Earth by locking the planet down and keeping anyone else out. During the height of Scarlet's power, an assassination attempt on the group's leader, Epsilon-Eagle, was carried out by a special forces group. Scarlet fought back with their powers, and the battle somehow breached the space-time continuum. Epsilon was gravely injured and cast somewhere into the continuum.

Seemingly gone forever, another Scarlet member known as Xi-Tiger took control of the organization. Under his rule, Scarlet became too brutal even for themselves, and they called for Epsilon to reclaim his position. More or less isolated from the rest of the group, Xi sought to find and assassinate Epsilon himself. He planned an attack on an A-Human research laboratory, where children with special abilities had been kidnapped and experimented on. Upon arriving, Xi-Tiger sensed the presence of Epsilon in one of the boys. However, he was unsure because he couldn't pinpoint the evil from Epsilon, who had entered the boy's body and was now living as a parasite. Xi-Tiger took a young girl hostage and threatened to kill her unless Epsilon revealed himself. The boy flew into a rage and morphed his body into Epsilon himself. Xi seemed to sense this strange power, and in fear, killed the girl and fled. Epsilon had completely split his dual personality apart; with both "good" and "evil" Epsilons now chasing after Xi-Tiger.<ref name=as>''Alien Soldier'', Sega Mega Drive, Japan 1995, Treasure Co., Ltd.</ref>

==Development and release==
[[File:Sega-Mega-Drive-JP-Mk1-Console-Set.jpg|thumb|right|Japanese Sega Mega Drive]]
Development of ''Alien Soldier'' lasted two years and was led by Hideyuki Suganami, who from the start wanted to make the entire game himself. Despite [[32-bit]] [[History of video game consoles (fifth generation)|fifth generation]] hardware already on the market, he chose to program the game for the [[16-bit]] Mega Drive, claiming that he may have been too captivated by the idea of making an "action shooting" game.<ref name=nami/> Treasure explicitly targeted the game for "hardcore" Mega Drive players and designed its difficulty and end-game scoring methods with this in mind.<ref name=maegawa1/>

Suganami was deeply invested in ''Alien Soldier'' and devoted a lot of personal time to developing it.<ref name=nami/><ref name=maegawa1/> He originally had ambitious plans for a large backstory, but in order to make their deadline of January 3, 1995, the majority of it was cut from the game. As the deadline approached, he came to realize he would not be able to complete ''Alien Soldier'' himself, and so additional staff were added to provide him support. He worked overtime during the New Years holidays in order to complete the game. The market for the Mega Drive was quickly shrinking, and the game had to be released in a half-finished state.<ref name=maegawa1>{{cite book|title=The Style of Games|url=http://shmuplations.com/aliensoldier/|language=Japanese|chapter=Masato Maegawa and Alien Soldier|archiveurl=https://web.archive.org/web/20161004224651/http://shmuplations.com/aliensoldier/|archivedate=October 4, 2016|deadurl=no}}</ref> After its release, Suganami wished he continued working on it, believing he could have improved on the story and graphics.<ref name=nami>{{cite web|title=エイリアンソルジャー|url=http://chibarei.blog.jp/gsl/words/aliensoldier/aliensoldier.html|website=chibarei.blog.jp|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20160304093057/http://chibarei.blog.jp/gsl/words/aliensoldier/aliensoldier.html|archivedate=March 4, 2016|deadurl=no}}</ref>

''Alien Soldier'' was released in 1995 on a physical cartridge format in Australia, Europe and Japan. In North America, the game was delivered exclusively through the [[Sega Channel]] cable service.<ref name=ign/> The game's first re-release was in Japan on the [[PlayStation 2]] as part of the ''[[Sega Ages#Sega Ages 2500|Sega Ages 2500]]: Gunstar Heroes Treasure Box'' compilation.<ref>{{cite web|title=Sega Ages: Gunstar Heroes Treasure Box Import Impressions|url=http://www.gamespot.com/articles/sega-ages-gunstar-heroes-treasure-box-import-impressions/1100-6145955/|website=GameSpot|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20140828091154/http://www.gamespot.com/articles/sega-ages-gunstar-heroes-treasure-box-import-impressions/1100-6145955/|archivedate=28 August 2014|deadurl=no}}</ref> ''Alien Soldier'' was released again worldwide in fall 2007 on the [[Wii]] via the [[Virtual Console]] download service, and again for [[Microsoft Windows]] on [[Steam (software)|Steam]] on January 6, 2011.<ref name=ign/><ref name=steam>{{cite web|title=Alien Soldier on Steam|url=http://store.steampowered.com/app/71116/|website=Steam|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20161231080351/http://store.steampowered.com/app/71116//|archivedate=31 December 2016|deadurl=no}}</ref>

==Reception==
{{Video game reviews
| GR = 82%<ref name=grankings>{{cite web|title=Alien Soldier for Genesis - GameRankings|url=http://www.gamerankings.com/genesis/564342-alien-soldier/index.html|website=GameRankings|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20160531175421/http://www.gamerankings.com/genesis/564342-alien-soldier/index.html|archivedate=31 May 2016|deadurl=no}}</ref>
| GSpot = 8/10<ref name=gamespot>{{cite web|last1=Provo|first1=Frank|title=Alien Soldier Review|url=http://www.gamespot.com/reviews/alien-soldier-review/1900-6182745/|website=GameSpot|accessdate=28 January 2016}} {{webarchive |url=https://web.archive.org/web/*/http://www.gamespot.com/wii/action/aliensoldier/review.html |date=* }}</ref>
| IGN = 8/10<ref name=ign>{{cite web|last1=Thomas|first1=Lucas M.|title=Alien Soldier Review|url=http://www.ign.com/articles/2007/11/27/alien-soldier-review|website=IGN|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20160820121946/http://www.ign.com/articles/2007/11/27/alien-soldier-review|archivedate=20 August 2016|deadurl=no}}</ref>
| NLife = 8/10<ref name=nlife>{{cite web|last1=Calvert|first1=Darren|title=Review: Alien Soldier (Virtual Console / Sega Mega Drive)|url=http://www.nintendolife.com/reviews/2007/11/alien_soldier_virtual_console|website=Nintendo Life|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20160811105009/http://www.nintendolife.com/reviews/2007/11/alien_soldier_virtual_console|archivedate=11 August 2016|deadurl=no}}</ref>
| EuroG = 8/10<ref name=eurog>{{cite web|title=Alien Soldier Review|url=http://www.eurogamer.net/articles/r_vcroundup_101107?page=2|website=Eurogamer|accessdate=13 January 2016|archiveurl=https://web.archive.org/web/20160816045504/http://www.eurogamer.net/articles/r_vcroundup_101107?page=2|archivedate=16 August 2016}}</ref>
| Fam = 24/40<ref name=fam>{{cite journal|title=NEW GAMES CROSS REVIEW: エイリアン ソルジャー|journal=Weekly Famicom Tsūshin|date=3 March 1995|issue=324|page=41}}</ref>
}}
For its original [[Mega Drive]] release, ''[[Famitsu]]'' provided ''Alien Soldier'' with a score of 24 out of 40.<ref name=fam/> Frank Provo of [[GameSpot]] reviewed the Wii Virtual Console release in 2007, citing the excellent graphics, sound, and general intensity of the game. He noted initial difficulty adjusting to the controls and game design. However, once accustomed, he said, "you start totally feeling what it must be like to be an army of one trading firepower with some of the universe's largest, most elaborate creatures. That's a great feeling." <ref name=gamespot/> Lucas M. Thomas of [[IGN]] noted the game's many similarities to ''[[Gunstar Heroes]]'' and recommended ''Alien Soldier'' to those who enjoyed it. "''Alien Soldier'' is a long-lost piece of [[Treasure (company)|Treasure]]'s action gaming legacy," said Thomas. "It's got the fast-firing, high-energy, overly-explosive intensity that fans of the company have come to expect."<ref name=ign/> Darren Calvert of [[Nintendo Life]] described the graphics and animation as some of the best on the Mega Drive. He found the game difficult, but still enjoyable for fans of the run and gun genre.<ref name=nlife/> Dan Whitehead of [[Eurogamer]] described the game as "manically-paced" and "bizarrely creative" while also providing a disclaimer that it's "really hectic and difficult if you're not into this sort of thing."<ref name=eurog/>
{{clear}}

==Notes==
{{notelist}}

==References==
{{reflist|30em}}

==External links==
* {{Official website|http://vc.sega.jp/vc_aliensoldier/}} {{ja icon}}
* {{MobyGames|id=/alien-soldier}}

{{Treasure video games}}

[[Category:1995 video games]]
[[Category:Run and gun games]]
[[Category:Sega Genesis games]]
[[Category:Terrorism in fiction]]
[[Category:Treasure (company) games]]
[[Category:Video games set in 2015]]
[[Category:Virtual Console games]]