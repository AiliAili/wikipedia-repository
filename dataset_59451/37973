{{good article}}
{{Use dmy dates|date=September 2013}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Black & Blue
| Cover          = Miike Snow - Black & Blue Cover.jpg
| Border         = 
| Alt            = 
| Caption        = 
| Artist         = [[Miike Snow]]
| Album          = [[Miike Snow (album)|Miike Snow]]
| B-side         = 
| Released       = {{Start date|2009|10|15|df=y}}
| Format         = {{flatlist|
*[[Gramophone record|12" vinyl]]
*[[Music download|digital download]]
}}
| Recorded       = Robotberget ([[Stockholm|Stockholm, Sweden]])
| Genre          = {{flatlist|
*[[Blue-eyed soul]]
*[[electronica]]
}}
| Length         = {{Duration|m=3|s=41}}
| Label          = [[Columbia Records|Columbia]]
| Writer         = {{flatlist|
*[[Bloodshy & Avant|Christian Karlsson]]
*[[Bloodshy & Avant|Pontus Winnberg]]
*[[Andrew Wyatt]]
*[[Henrik Jonback]]
*[[Juliet Richardson]]
}}
| Producer       = Miike Snow
| Certification  = 
| Last single    = "[[Animal (Miike Snow song)|Animal]]"<br/>(2009)
| This single    = "'''Black & Blue'''"<br/>(2009)
| Next single    = "[[Silvia (song)|Silvia]]"<br/>(2010)
| Misc           =
}}
"'''Black & Blue'''" is a song performed by Swedish [[indie pop]] band [[Miike Snow]]. It was released as the second single from the band's 2009 [[Miike Snow (album)|self-titled debut album]] on 15 October 2009 through [[Columbia Records]]. The song was written by the band alongside [[Henrik Jonback]] and [[Juliet Richardson]]. Band members Christian Karlsson and Pontus Winnberg of production duo [[Bloodshy & Avant]] had conceptualized the chorus prior to forming the band with [[Andrew Wyatt]] in 2007, but saved the idea for the Miike Snow project rather than offering it to another artist. "Black & Blue" is a [[blue-eyed soul]] and [[electronica]] song that is sung in a breathy [[falsetto]] style with piano and synthesizer instrumentation.

Critics were generally positive towards "Black & Blue". The majority of them praised the piano elements and Wyatt's vocals. The single is among the band's most successful releases; it peaked at number 64 on the [[UK Singles Chart]] and reached the top 10 on the [[UK Dance Chart]] and the Flemish [[Ultratop#Ultratip|Ultratip]] chart. Vince Haycock directed its accompanying [[music video]], in which actor [[Jeff Stewart (actor)|Jeff Stewart]] portrays a man who creates music with eccentric instruments in his apartment.

==Background==
"Black & Blue" was written by [[Miike Snow]]'s three members, [[Bloodshy & Avant|Christian Karlsson, Pontus Winnberg]] and [[Andrew Wyatt]], in collaboration with [[Henrik Jonback]] and [[Juliet Richardson]].<ref name="liner"/> Parts of the chorus were initially conceptualized by Karlsson and Winnberg, also known as [[Bloodshy & Avant]], prior to forming the band in 2007.<ref name="concept"/><ref>{{cite web|url=http://www.allmusic.com/artist/miike-snow-mn0001583976 | title= Miike Snow – Biography | publisher= [[AllMusic]] and [[Rovi Corporation]] | last=Birchmeier |first=Jason |accessdate= 10 December 2012}}</ref> Karlsson told music website [[musicOMH]], "It's the only song on the album that we had an idea before and we kind of saved [it], like we want this song for our own project. When we met Andrew [Wyatt] and we decided to start the band, it was only an idea, but we played it for Andrew and he really liked it."<ref name="concept">{{cite web|url=http://www.musicomh.com/music/features/miike-snow_1009.htm |title=Interview: Miike Snow |publisher=[[musicOMH]]. OMH Media |last=O'Driscoll |first=Ceiri |date=October 2009 |accessdate=14 September 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20121003051657/http://www.musicomh.com/music/features/miike-snow_1009.htm |archivedate=3 October 2012 |df=dmy }}</ref> It was recorded at Robotberget, the band's own studio in [[Stockholm]], Sweden.<ref name="liner"/><ref>{{cite web|url=http://www.gq.com/style/ten-essentials/201203/miike-snow-interview-10-essentials|title=10 Essentials: Miike Snow|work=[[GQ]]|publisher=[[Condé Nast Publications]]|date=28 March 2012|last=McKnight|first=Ren|accessdate=2 March 2013}}</ref> The band produced the track and then [[audio mixing (recorded music)|mixed]] it with Anders Hvenare.<ref name="liner"/> [[Columbia Records]] released "Black & Blue" as the album's second single on 15 October 2009 in Europe.<ref name="europe"/> The [[music download|digital]] release includes the original version and remixes by [[Caspa]], Jaymo & Andy George, [[Netsky (musician)|Netsky]], Savage Skulls and [[Tiga (musician)|Tiga]].<ref name="7digital"/> In the United Kingdom, the [[iTunes Store]] version comes with the original track, remixes by Tiga and Caspa, and [[Mark Ronson]]'s remix of the album's first single, "[[Animal (Miike Snow song)|Animal]]".<ref name="itunes"/> A [[gramophone record|12" vinyl]] was released in the United Kingdom on 19 October 2009, featuring the aforementioned "Black & Blue" remixes, excluding Netsky's.<ref name="12 inch"/><ref name="amazon vinyl">{{cite web|url=http://www.amazon.co.uk/Black-Blue-VINYL-Miike-Snow/dp/B002PLQHGI|title=Black & Blue <nowiki>[</nowiki>12" vinyl<nowiki>]</nowiki>|publisher=Amazon.co.uk|accessdate=2 March 2013}}</ref>

==Composition==
{{Listen
| filename    = Miike Snow - Black & Blue.ogg
| title       = "Black & Blue"
| description = A 22-second sample of "Black & Blue", showcasing the piano instrumentation, synth backing and Wyatt's breathy vocals.
| pos         = right
}}
"Black & Blue" is a [[blue-eyed soul]] and [[electronica]] piano piece.<ref name="Bergstrom"/><ref name="Klapper"/> According to sheet music published at Musicnotes.com by [[Universal Music Publishing Group]], the song is written in the time signature of [[Time signature|common time]] with a moderate beat rate of 135 [[Tempo|beats per minute]]. It is written in the [[Key (music)|key]] of [[A-flat major|A{{music|b}} major]] and Wyatt's vocal range spans the notes of [[E♭ (musical note)|E{{music|b}}<sub>4</sub>]] to [[F (musical note)|F<sub>5</sub>]]. It has an A{{music|b}}–B{{music|b}}m–Fm<sub>7</sub>–Gm [[chord progression]] and a [[verse-chorus form|verse-chorus]] structure.<ref name="sheet">{{cite web|title=Miike Snow, 'Black & Blue'&nbsp;– Composition Sheet Music|publisher=Musicnotes.com. [[Universal Music Publishing Group]]}}</ref> Wyatt sings moody and melancholic lyrics in a "breathy" and "whispery" [[falsetto]] vocal style.<ref name="Klapper"/><ref name="McAlpine"/><ref name="Sullivan">{{cite news|url=https://www.theguardian.com/music/2009/oct/22/miike-snow-black-blue-review|title=Miike Snow: Black & Blue|last=Sullivan|first=Caroline|date=22 October 2009|work=[[The Guardian]]|publisher=[[Guardian News and Media]]|accessdate=2 March 2013}}</ref><ref name="Mitchell">{{cite web|url= http://www.theskinny.co.uk/music/records/97394-miike_snow_black_blue|title=Miike Snow – Black and Blue|work=[[The Skinny (magazine)|The Skinny]]|publisher=Radge Media|last=Mitchell|first=Paul|date=6 October 2009|accessdate=14 September 2012}}</ref> The production features piano chords over "buzzing" [[synthesizer]]s.<ref name="Ashman">{{cite web|url=http://drownedinsound.com/releases/14819/reviews/4138282|title=Miike Snow – Miike Snow|last=Ashman|first=Neil|date=12 October 2009|publisher=[[Drowned in Sound]]|accessdate=13 September 2012}}</ref> According to [[BBC Music]] critic Fraser McAlpine, it includes [[Coldplay]]-esque breakdown sections and a "ghosty, mournful piano playing doomy dark chords".<ref name="McAlpine"/> McAlpine compared the verses to "[[Music Sounds Better with You]]" (1998) by French [[house music|house]] band [[Stardust (band)|Stardust]],<ref name="McAlpine"/> and Ben Hogwood of musicOMH likened the "soulful approach" to [[gospel music]] and works by American duo [[Outkast]].<ref name="Hogwood">{{cite web|url=http://www.musicomh.com/albums/miike-snow_1009.htm|title=Miike Snow – Miike Snow|publisher=[[musicOMH]]. OMH Media|last=Hogwood|first=Ben|archiveurl=https://web.archive.org/web/20091020074753/http://www.musicomh.com/albums/miike-snow_1009.htm|archivedate=20 October 2009}}</ref> Elliott Townsend of ''[[URB (magazine)|URB]]'' called it an "[[Synthpop#21st century revival|electro-pop]] throwback" to [[Curtis Mayfield]]'s "[[Move On Up]]" (1970).<ref>{{cite web|url=http://www.urb.com/reviews/cd/feature.php?ReviewId=1245|title=Miike Snow :: Miike Snow|last=Townsend|first=Elliott|date=11 June 2009|work=[[URB (magazine)|URB]]|publisher=NativeSon Media|archiveurl=https://web.archive.org/web/20090622035811/http://www.urb.com/reviews/cd/feature.php?ReviewId=1245|archivedate=22 June 2009}}</ref>

==Critical reception==
"Black & Blue" received a positive response from music critics. John Bergstrom of [[PopMatters]] described it as "smooth, winning blue-eyed soul",<ref name="Bergstrom">{{cite web|url=http://www.popmatters.com/pm/review/94833-miike-snow-miike-snow/|title=Miike Snow: Miike Snow|publisher=[[PopMatters]]. Sarah Zupko|last=Bergstrom|first=John|date=11 June 2009|accessdate=13 September 2012}}</ref> and Neil Ashman of [[Drowned in Sound]] commented that its breathy vocals, piano chords and synth backing made it a "successful blend of melancholy and dancefloor-ready rhythms".<ref name="Ashman"/> Caroline Sullivan of ''[[The Guardian]]'' deemed the falsetto vocals and synths "naggingly infectious", citing it as the band's strong point: "the trio may be faceless, but they write a good tune".<ref name="Sullivan"/> [[Sputnikmusic]]'s staff reviewer Rudy Klapper felt that the "[[techno]] swirl" of the song "nail[s] the juxtaposition between Wyatt's moody lyrics and the irrepressible production".<ref name="Klapper">{{cite web|url=http://www.sputnikmusic.com/review/31047/Miike-Snow-Miike-Snow/|title=Miike Snow – Miike Snow|publisher=[[Sputnikmusic]]|last=Klapper|first=Rudy|date=21 June 2009|accessdate=14 September 2012}}</ref> Fraser McAlpine of BBC Music rated "Black & Blue" four stars out of five and recommended it for when "you just need something to lift the spirits a bit, without someone shouting something stupid in your ear".<ref name="McAlpine"/> He continued, "It basically wants to rub your shoulders at the end of a long hard day, and whisper something funny in your ear while your shoulders untie."<ref name="McAlpine">{{cite web|url=http://www.bbc.co.uk/blogs/chartblog/2009/10/miike_snow_black_blue.shtml|title=Miike Snow – 'Black & Blue'|publisher=[[BBC Music]]. [[BBC]]|last=McAlpine|first=Fraser|date=18 October 2009|accessdate=14 September 2012}}</ref> Natalie Kaye of [[Contactmusic.com]] said that although it begins melancholy, it "swells to become one of the noisiest points on ''Miike Snow''".<ref>{{cite web|url=http://www.contactmusic.com/album-review/miike-snow-miike-snowx03x12x09|title=Miike Snow – Miike Snow Album Review|last=Kaye|first=Natalie|publisher=[[Contactmusic.com]]|accessdate=5 March 2013}}</ref> Paul Mitchell of ''[[The Skinny (magazine)|The Skinny]]'' felt that Wyatt channels [[Paddy McAloon]]'s breathy vocal style on "Black & Blue", but without the "lyrical wit".<ref name="Mitchell"/> He rated the song three stars out of five and called it "decent", finding the remixes available on the release more interesting.<ref name="Mitchell"/> A more critical review came from [[Pitchfork Media]]'s Marc Hogan, who wrote that it "promisingly&nbsp;... splits the difference between [[Prince (musician)|Prince]] and piano-pop, only to underwhelm as a whole".<ref>{{cite web|url=http://pitchfork.com/reviews/albums/13189-miike-snow/|title=Miike Snow: Miike Snow|publisher=[[Pitchfork Media]]|last=Hogan|first=Marc|date=11 June 2009|accessdate=14 September 2012}}</ref>

==Chart performance==
In the issue dated 17 October 2009 of the [[UK Singles Chart]], "Black & Blue" debuted at number 95.<ref name="archive">{{cite web|url=http://www.theofficialcharts.com/search-results-album/_/Black+&+Blue#single|title=The Official Charts Company - Black & Blue by Miike Snow Search |date=6 May 2013|publisher=The Official Charts Company}}</ref> It fell off the following week, before re-entering the next at number 64, which became its peak.<ref name="archive"/> The song spent three weeks on the chart.<ref name="sc_UKchartstats_Miike Snow"/> In the issue dated 30 October 2009, the single entered the [[UK Dance Chart]] at number two, which became its peak.<ref name="sc_UKdance_Miike Snow"/> It spent five weeks on the chart; its final appearance was on 28 November 2009 at number 19.<ref>{{cite web|url=http://www.officialcharts.com/archive-chart/_/18/2009-11-28/|title=Top 40 Dance Singles Archive – 28th November 2009|publisher=UK Dance Chart. Official Charts Company|accessdate=2 March 2013}}</ref> "Black & Blue" is the band's highest-peaking entry on both the UK Singles Chart and the UK Dance Chart.<ref name="archive"/><ref>Peak chart positions for singles on the UK Dance Chart:
*"Black & Blue": {{cite web|url=http://www.officialcharts.com/archive-chart/_/18/2009-10-31/|title=Top 40 Dance Singles Archive – 31st October 2009|publisher=UK Dance Chart. Official Charts Company|accessdate=10 December 2012}}
*"Silvia": {{cite web|url=http://www.officialcharts.com/archive-chart/_/18/2010-02-06/|title=Top 40 Dance Singles Archive – 6th February 2010|publisher=UK Dance Chart. Official Charts Company|accessdate=10 December 2012}}
*"Paddling Out": {{cite web|url=http://www.officialcharts.com/archive-chart/_/18/2012-03-24/|title=Top 40 Dance Singles Archive – 24th March 2012|publisher=UK Dance Chart. Official Charts Company|accessdate=10 December 2012}}
</ref> In Belgium, the song peaked at number ten on the Flemish [[Ultratop#Ultratip|Ultratip]] chart, where it spent five weeks in total.<ref name="sc_Flanders Tip_Miike Snow"/>

==Music video==
Vince Haycock directed the [[music video]] for "Black & Blue" on location in [[London Borough of Hackney|Hackney]], London in August 2009.<ref>{{cite web|url=http://www.videostatic.com/vs/2009/08/shot-miike-snow-vincent-haycock-director.html|title=SHOT: Miike Snow – Vincent Haycock, director|date=24 August 2009|last=Gottlieb|first=Steven|accessdate=14 September 2012|publisher=Video Static}}</ref><ref name="promo">{{cite web|url=http://www.promonews.tv/2009/09/24/miike-snow%E2%80%99s-black-blue-by-vince-haycock/|title=Miike Snow's Black & Blue by Vince Haycock|publisher=Promo News|date=24 September 2009|accessdate=3 March 2013}}</ref> The clip features ''[[The Bill]]'' actor [[Jeff Stewart (actor)|Jeff Stewart]] as a bearded man.<ref>{{cite web|url=http://www.bbc.co.uk/news/entertainment-arts-14526650|title=Ex-Bill star Stewart says winning award 'extraordinary'|date=15 August 2011|publisher=[[BBC News]]. BBC|accessdate=2 March 2013}}</ref><ref>{{cite web|url=http://www.thesun.co.uk/sol/homepage/showbiz/bizarre/2643402/Ex-Bill-star-Jeff-Stewart-starring-in-Miike-Snow-video.html|title=A hol-billy|date=18 September 2009|work=[[The Sun (United Kingdom)|The Sun]]|publisher=|accessdate=2 March 2013}}</ref> The video begins with the man waking up in his apartment at noon. He conducts various experiments to create music, and finally finishes a robotic mannequin that plays the drums. This is interspersed with shots of Miike Snow band members driving a car with Wyatt singing in the backseat. Happy with his invention, the man then walks outside in his bathrobe, headphones and sunglasses past three frolicking teenagers, and hugs one of them. He continues to walk down the street where he finally meets the members of Miike Snow at a crosswalk. He removes his bathrobe, headphones and sunglasses, and walks away happily. The video premiered online in September 2009,<ref name="promo"/> before being released to the iTunes Store on 9 October 2009.<ref>{{cite web|url=https://itunes.apple.com/gb/music-video/black-blue/id334111217|title=Black & Blue by Miike Snow|publisher=iTunes Store (United Kingdom). Apple|accessdate=3 March 2013}}</ref> Regarding the video, Fraser McAlpine of BBC Music remarked: "It's basically what Santa does on his days off."<ref name="McAlpine"/>

==Formats and track listings==
{{col-begin}}
{{col-2}}
*'''12" remix vinyl'''<ref name="12 inch">{{cite AV media notes|title=Black & Blue|others=Miike Snow|date=2009|type=12"|publisher=[[Columbia Records|Columbia]]|id=LC00162|location=United Kingdom}}</ref>
#"Black & Blue" ([[Tiga (musician)|Tiga]] remix) – 6:21
#"Black & Blue" ([[Caspa]] remix) – 5:35
#"Black & Blue" (Jaymo & Andy George's Moda mix) – 5:14
#"Black & Blue" (Savage Skulls remix) – 7:23

*'''Digital EP'''<ref name="7digital">{{cite web|url=http://www.7digital.com/artist/miike-snow/release/black-and-blue-1|title=Black & Blue (2009)|publisher=[[7digital]] (United Kingdom)|accessdate=13 September 2012}}</ref>
#"Black & Blue" – 3:41
#"Black & Blue" (Tiga remix) – 6:21
#"Black & Blue" (Caspa remix) – 5:35
#"Black & Blue" (Jaymo & Andy George's Moda mix) – 5:14
#"Black & Blue" ([[Netsky (musician)|Netsky]] remix) – 5:16
#"Black & Blue" (Savage Skulls remix) – 7:23
{{col-2}}
*'''UK iTunes EP'''<ref name="itunes">{{cite web|url=http://itunes.apple.com/gb/album/black-blue-ep/id333011981|title=Black & Blue – EP by Miike Snow|publisher=[[iTunes Store]] (United Kingdom). [[Apple Inc.|Apple]]|accessdate=13 September 2012}}</ref>
#"Black & Blue" – 3:41
#"[[Animal (Miike Snow song)|Animal]]" ([[Mark Ronson]] remix) – 4:30
#"Black & Blue" (Tiga remix) – 6:18
#"Black & Blue" (Caspa remix) – 5:35
{{col-end}}

==Credits and personnel==
*[[Songwriter|Songwriting]] – [[Bloodshy & Avant|Christian Karlsson, Pontus Winnberg]], [[Andrew Wyatt]], [[Henrik Jonback]], [[Juliet Richardson]]
*[[Record producer|Production]] – Miike Snow
*[[Audio mixing (recorded music)|Mixing]] – Anders Hvenare, Miike Snow
*[[Audio mastering|Mastering]] – Ted Jensen

Credits are adapted from the ''Miike Snow'' liner notes.<ref name="liner">{{cite AV media notes
| title       = Miike Snow
| titlelink   = Miike Snow (album)
| others      = [[Miike Snow]]
| year        = 2009
| publisher   = [[Downtown Records|Downtown]]
| id = DWT70085}}</ref>

==Charts==
{| class="wikitable sortable plainrowheaders"
!scope="col"|Chart (2009–10)
!scope="col"|Peak<br />position
|-
!scope="row"{{singlechart|Flanders Tip|10|artist=Miike Snow|song=Black & Blue|accessdate=17 September 2012}}
|-
!scope="row"{{singlechart|UKdance|2|artist=Miike Snow|song=Black & Blue|date=2009-10-31|accessdate=14 September 2012}}
|-
!scope="row"{{singlechart|UKchartstats|64|artist=Miike Snow|song=Black & Blue|songid=34282|accessdate=14 September 2012}}
|}

==Release history==
{| class="wikitable plainrowheaders"
|-
!scope="col"| Country
!scope="col"| Date
!scope="col"| Format
!scope="col"| Label
|-
!scope="row"|Europe<ref name="europe">{{cite web|url=https://itunes.apple.com/se/album/black-blue/id333024599|title=Black & Blue av Miike Snow|publisher=iTunes Store (Sweden). Apple|accessdate=3 March 2013|language=Swedish}}</ref><ref>{{cite web|url=https://itunes.apple.com/be/album/black-blue/id333024599|title=Black & Blue by Miike Snow|publisher=iTunes Store (Belgium). Apple|accessdate=3 March 2013}}</ref><ref>{{cite web|url=https://itunes.apple.com/de/album/black-blue/id333024599|title=Black & Blue von Miike Snow|publisher=iTunes Store (Germany). Apple|accessdate=3 March 2013|language=German}}</ref>
|15 October 2009
|rowspan="2"|[[Music download|Digital download]]
|rowspan="3"|[[Columbia Records]]
|-
!scope="row" rowspan="2"|United Kingdom<ref name="amazon vinyl"/><ref name="amazon">{{cite web|url=http://www.amazon.co.uk/Black-Blue/dp/B002QIXHMM|title=Black & Blue: Miike Snow|publisher=[[Amazon.com|Amazon.co.uk]]|accessdate=3 March 2013}}</ref>
|16 October 2009
|-
|19 October 2009
|[[gramophone record|12" vinyl]]
|}

==References==
{{reflist|30em}}

==External links==
*{{Youtube|-RGAPWxTe5g|Official music video (Vevo)}}
*{{MetroLyrics song|miike-snow|black-blue}}<!-- Licensed lyrics provider -->

{{Miike Snow}}

{{DEFAULTSORT:Black and Blue (Miike Snow song)}}
[[Category:2009 singles]]
[[Category:Songs written by Christian Karlsson (record producer)]]
[[Category:Songs written by Pontus Winnberg]]
[[Category:Songs written by Andrew Wyatt]]
[[Category:Songs written by Henrik Jonback]]
[[Category:Miike Snow songs]]
[[Category:2009 songs]]
[[Category:Columbia Records singles]]