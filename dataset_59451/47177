{{Notability|Institutions|date=February 2012}}
'''Complete Vocal Institute''' (often abbreviated to CVI) is an educational institution, located at Kultorvet in [[Copenhagen]]. The Institute was opened in 2005 and uses a teaching method called [[Complete Vocal Technique]] (often abbreviated to CVT), which was developed by singer and voice researcher Cathrine Sadolin. CVI educates professionals and semi-professional singers and teachers and there are ongoing about 250 singers (2012) associated with the longer courses.

The theoretical and practical basis for school - Complete Vocal Technique - is also the name of a book on techniques written by Cathrine Sadolin. The educational book Complete Vocal Technique incl. audio examples and exercises are published in seven languages: Danish, Swedish, Finnish, English, Dutch, French and German.
Demand for courses at the school was right from the start so great that CVI in both 2007 and 2008 were among Denmark’s fastest growing companies.<ref>{{cite web|title=Gazelle list 2007|url=http://img.borsen.dk/img/cms/tuksi4/media/cmsmedia/753_content_2_7209.pdf|accessdate=8 February 2012}}</ref><ref>{{cite web|title=What is a Gazelle company|url=http://img.borsen.dk/img/cms/tuksi4/media/cmsmedia/854_content_2_2415.pdf|accessdate=8 February 2012}}</ref><ref>{{cite web|title=Gazelle list 2008|url=http://img.borsen.dk/img/cms/tuksi4/media/cmsmedia/851_content_2_1641.pdf}}</ref>

== Complete Vocal Technique==
[[Complete Vocal Technique|Main Article about Complete Vocal Technique]]

Complete Vocal Technique is divided into four main principles and by combining parts of these principles the singers can produce the sounds they want. This also makes it possible to pinpoint and correct specific problems and errors without having to change the parts the singer are happy with.

=== The four principles ===
* Three basic principles (to ensure vocal health)
* Four modes Neutral, Curbing, Overdrive, Edge (to select the "gear" you want to sing in)
* Sound color (make the voice lighter or darker)
* Effects (to acquire special sounds)

CVI sees all sounds as equal and all sounds can be made in a healthy way. Singing technique is therefore a technique for all styles of singing. Authorized CVT teachers should not express their personal taste. It is only the singer, who decides what he or she wants to work with and it's only the singer who has influence on the final result.

When the method is called Complete Vocal Technique it refers to the idea that the techniques cover all the sounds the human voice can produce. The method is not perceived as complete in the sense, that there is no room for improvement. Research is ongoing and techniques are updated regularly.
The goal of Complete Vocal Institute is to explain the voice in a simple and understandable manner and ensure that the desired sounds are produced in a healthy way so that the singers avoid damage to their voice.

== The courses==

Singers with a professional or semi-professional background can apply for admission to a number of courses of varying length and content. Singers can choose to follow the courses in Copenhagen or in a number of countries, where there are authorized CVT teachers. The shortest courses are 1–5 days while it takes three years to become a licensed CVT teacher. Singers who want to focus exclusively on their own singing can choose between courses of three months to one year. Subsequently, the singers can choose to continue on so-called Advanced courses.

'''The 3-year singer/teacher-education''' - is required to be licensed CVT teacher. The first 23 teachers completed the course in 2005 and were assigned to CVI when the Institute opened the same year. Today (2012) there are 131 licensed CVT teachers from 12 countries and a further 128 are taking the course.
Teachers are trained in identifying the singers' learning type and adapting the tuition after each singer. There is a large age spread among the singers at the teacher education and they come from the Nordic countries and Europe. There are also trained teachers from countries such as Egypt, Philippines and USA. After graduating as a CVT-teacher their typically teach at conservatories, universities, theaters, studios, music schools and privately.

== Background and research==

The education system used on CVI is created by Cathrine Sadolin. Due to [[asthma]] from childhood she received singing lessons to control her breathing. Because of the illness, she had to develop her own techniques to produce the desired sounds. Her first step was to understand the body and the vocal physiology and she experimented with producing the desired sounds in other ways than the already known methods. 

Through research of the anatomy and physiology and all types of singers, she found out that there is an underlying structure behind the sounds that a human can do. This was divided into four different modes that cover all the sounds the human voice can produce.
She worked also with a range of professional singers to try out the techniques, when they wanted to produce specific sounds or had problems with their voice. The work formed the basis for the techniques Complete Vocal Institute currently uses and Cathrine Sadolin has continued the voice research together with physicians and acousticians.
Cathrine Sadolin has been invited to speak in the voice conferences in Europe and USA since 1996.

In 2007, she started to collaborate with consultant ENT Surgeon Julian McGlashan from Nottingham University Hospital.
Their first joint research study was published in February 2007 on auditive research, where speech therapists had to determine which voice modes was used by listening to song samples. After that they cooperated to show the [[Larynx|laryngeal gestures]] in the various effects e.g. distortion, [[Growling|growl]], [[Guttural|grunt]], creaks and creaking, rattle, Scream, vocal breaks, etc.

Since then they have worked with visual detection, where test subjects had to determine the different singing modes without hearing the sound, but by looking at photos and videos of the [[larynx]] during singing, recorded with an [[endoscope]] camera. The camera is led in through the nose so it can record the [[vocal folds]] and larynx movements during singing. 
Furthermore [[Electroglottograph|EGG]] measurements where electrodes are placed on the larynx to record the vocal movements during singing and speaking are used to detect the various vocal modes.

Currently (2012) Cathrine Sadolin and Julian McGlashan are researching the terminology used in describing singing and the use of the voice.

Together with sound specialist Eddy B. Brixen (from DPA) and EBB-consult, there is ongoing research in making [[Spectrum analyzer|spectral analysis]] of the sound spectrum, the voice produces with the various modes.

In addition to testing the techniques in a scientific way another goal is that an outsider will be able to identify the different voice modes on a purely objective basis, without prior knowledge of singing.

== Notes ==

{{Reflist}}

== References ==
* [http://gupea.ub.gu.se/bitstream/2077/3949/1/HT06-1190-03.pdf Andersson, Margareta & Tanggaard, Ulla, Göteborg Universitet, Arbete med sceniskt uttryck inom sångundervisning: En jämförande studie om sångundervisning inom olika genrer, 2007]
* [http://kmh.diva-portal.org/smash/get/diva2:389573/FULLTEXT01 Josefine Gustafsson, Teknik eller interpretation, 2009]
* [https://pure.ltu.se/ws/files/32558845/LTU-EX-2011-32528604.pdf%20 Malin Huuva & Josefine Wilén,  Luleå tekniska universitet, Sångundervisning i Samklang, 2010]
*  [http://gupea.ub.gu.se/bitstream/2077/18333/1/gupea_2077_18333_1.pdf Liljeland, Cecilia, Göteborg Universitet, 2008, Vägen til en Sångröst]
* [http://pure.ltu.se/portal/files/31044487/LTU-LAR-EX-07093-SE.pdf%20 Cecilia Pekkari, Luleå tekniska universitet, 2004, Lärande och arbetsformer för sång inom populärmusikaliska genrer]
* [http://gupea.ub.gu.se/bitstream/2077/21648/1/gupea_2077_21648_1.pdf Reinedahl, Ester-Marie, Göteborg Universitet, 2009, Ulike metoder hvorved man kan undervise sangelever om pust og støtteteknikk]
* [http://kmh.diva-portal.org/smash/get/diva2:450078/FULLTEXT01 Anne Widegren, Kungl. Musikhögskolan i Stockholm, Gör sångpedagogen någon skillnad?, 2008]
* [http://www.pevoc.org/pevoc08/programme/program/indexe02e.html?action=papers&ID=12698&conViewSession=395&do=open&showDay=42&amsysID=820381627097342bcc3a37b1e80d3b45 Pan european voice conference 2009]

== External links ==
* http://www.completevocalinstitute.com/

== Notes and references added ==

{{coord missing|Denmark}}

[[Category:Music schools in Denmark]]