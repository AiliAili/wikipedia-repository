{{Infobox company
| name           = AXS.com
| logo           = 
| type           = Subsidiary of [[Anschutz Entertainment Group]]
| foundation     = 2011
| area_served    = Worldwide
| industry       = Entertainment
| location       = [[Los Angeles, California]]|
| key_people     = Bryan Perez (President, Digital, Ticketing and Media - AEG)
<br />Tom Andrus (GM/SVP AXS.com)
<br />Blaine LeGere (SVP, Ticketing – AEG)
<br />Todd Sims (SVP, Strategy & Business Development - AEG)
<br />Doug Lyons (VP, Product Management - AXS.com)
<br />Azat Aslanyan (VP, Engineering - AXS.com)
<br />Sandeep Khera (VP, CRM - AXS.com)
<br />Dean DeWulf (VP, AXS Europe)
| num_employees  = [[Full-time]]: 65<br>[[Part-time]]:
| products       = Ticket Sales<br />Ticketing Technology<br />Event/Venue Marketing
| homepage       = {{url|axs.com}}
}}

'''AXS''' (pronounced "access")<ref name="NYT20110822"/> is a digital marketing platform for purchasing tickets for sports and entertainment events in the US, and overseas. It was developed and is operated by [[Anschutz Entertainment Group]] (AEG) in partnership with Outbox Technologies.<ref name="NYT20110822"/>

== Background ==

[[Anschutz Entertainment Group]] (AEG), valued at over 8 billion US dollars,<ref name="Forbes20121015"/> is the world's largest owner of sports teams and sports events and is also the world's second largest presenter of live music and entertainment events (after [[Live Nation Entertainment]]). [[Live Nation Entertainment]] was formed by the merger of [[Live Nation (events promoter)|Live Nation]] and [[Ticketmaster]], a ticket sale giant, in early 2010.<ref name="Reuters20110823" /> AEG had been licensing Ticketmaster’s software before the merger, and as part of the [[United States Department of Justice]] (DOJ) conditions for approving the merger deal, AEG was allowed to continue licensing Ticketmaster’s software for up to five years while it established its own ticketing business.<ref name="Reuters20110823" />

AEG partnered with Outbox Enterprises, a start up company, in which AEG is both an equity partner and a client, to develop AXS.<ref name="LAT20110824">{{cite news|author=Alex Pham, Los Angeles Times |url=http://articles.latimes.com/2011/aug/24/business/la-fi-ct-aeg-tickets-react-20110824 |title=Outbox poised to challenge Ticketmaster through deal with AEG - Los Angeles Times |publisher=Articles.latimes.com |date=2011-08-24 |accessdate=2013-11-18}}</ref> The initial AXS deployment was August 2011<ref name="NYT20110822">{{cite news|author=Ben Sisario |url=https://www.nytimes.com/2011/08/23/business/media/aeg-starts-ticket-selling-site-to-take-on-ticketmaster.html |title=Ticketmaster Competitor to Unveil a Web Site |publisher=New York Times |date=August 22, 2011 |deadurl=no |accessdate=2013-12-17}}</ref><ref name="LAT20130205"/><ref>{{cite web|url=http://www.ticketnews.com/news/AEG-launches-digital-marketing-effort-for-axs111511827 |title=AEG launches digital marketing effort for axs.com |publisher=TicketNews |date=2011-11-15 |accessdate=2013-11-18}}</ref> and venues and services have been added in a phased roll out. As of August 2013, AXS was the exclusive or the primary ticket provider for over 30 US venues and 9 UK venues.{{Citation needed|date=November 2013}} The first [[Staples Center]] concert available only through AXS was [[Beyoncé]] in 2013; both [[Los Angeles Lakers]] and the [[Los Angeles Clippers]] are still using Ticketmaster.<ref name="LAT20130204">{{cite news |url=http://articles.latimes.com/2013/feb/04/entertainment/la-et-ms-aeg-20130204 |title=AEG moves to battle Ticketmaster head on - Los Angeles Times |publisher=Articles.latimes.com |date=2013-02-04 |accessdate=2013-11-18 |first1=Todd |last1=Martens |first2=Wesley |last2=Lowery}}</ref>
The [[white-label product|white label technology]] Outbox developed enables AEG to sell tickets under either the AXS brand name or under local venue name brands, which have considerable local support, while providing centralized [[Customer relationship management|CRM services]] for either approach.<ref name="Reuters20110823">{{cite news|url=http://www.reuters.com/article/2011/08/23/idUS338350698120110823 |title=AEG Enters Competition with Live Nation Ticketmaster |publisher=Reuters |date=2011-08-23 |accessdate=2013-11-18}}</ref>

In January 2014, AEG announced that AXS had purchased [[Examiner.com]], a user generated news site, in order to leverage the site's entertainment content.<ref>{{cite web|title=Examiner.com Acquired by AXS|website=AEG Worldwide|url=http://www.aegworldwide.com/about/newsdetail/12142#.VDLu4vldWHQ|date=23 January 2014|accessdate=6 October 2014}}</ref>

== Innovations ==

=== Fair AXS ===

AXS aims to block large volume, automated purchases by computer programs used by [[ticket resale]]rs by using a "waiting room" facility on a separate server.<ref name="LAT20130205"/> Users log their personal details and purchase information prior to tickets going on sale, and are screened for multiple purchases. However, this process does not work and majority of loyal fans are denied tickets due to the amount of tickets that are sold to ticket scalpers. The server has passed stress testing by 1 million users, so the site should not crash.<ref name="LAT20130205">{{cite news |author=Todd Martens |url=http://articles.latimes.com/2013/feb/05/entertainment/la-et-ms-ticketmaster-aeg-axs-change-way-tickets-sold-20130205 |title=Can AEG's AXS change the way tickets are sold? - Los Angeles Times |publisher=Articles.latimes.com |date=2013-02-05 |accessdate=2013-11-18}}</ref><ref>{{cite web|author=Mark Sweney and Ellis Schindler |url=https://www.theguardian.com/media/2012/oct/22/aeg-ticketmaster-fan-friendly-online-booking |title=AEG takes on Ticketmaster with new 'fan-friendly' online booking service &#124; Media |publisher=theguardian.com |date= |accessdate=2013-11-18}}</ref>

=== AXS Invite ===

AXS selectively offers an add-on feature, AXS Invite, which lets ticket purchasers reserve adjacent seats for friends, who have up to 48 hours to decide on receiving email or [[social media]] notification.<ref name="HP20121012"/> Invite is not available when tickets are initially sold, is only available at some venues, and is unlikely to help at oversubscribed shows.<ref name="HP20121012">{{cite news|url=http://www.huffingtonpost.com/2012/10/12/aeg-adjacent-seats-axs-invite_n_1961554.html |title=AEG's AXS Invite Will Allow You To Reserve An Adjacent Seat For A Friend |publisher=Huffingtonpost.com |date= 2012-10-12|accessdate=2013-11-18}}</ref> AXS acknowledges that the feature is "really about finding a way to sell more tickets", while enhancing customer convenience.<ref name="HP20121012"/>

== Partnerships ==

=== Carbonhouse ===

Carbonhouse, a website developer with over 100 clients, was acquired by AEG.<ref name="VT20130204"/> This will allow integration of additional features into the AXS ticketing platform.<ref name="VT20130204">{{cite web|last=Brooks |first=Dave |url=http://www.venuestoday.com/news/detail/aeg-acquires-carbonhouse |title=AEG Acquires Carbonhouse |publisher=Venues Today |date=2013-02-04 |accessdate=2013-11-18}}</ref>

=== Stubhub ===

AEG has entered a partnership with [[StubHub]], a secondary ticketing service owned by eBay, to place tickets from StubHub in AXS ticket listings.<ref>{{cite web|url=http://www.billboard.com/biz/articles/news/touring/1083085/aeg-stubhub-partnership-brings-benefits-far-beyond-secondary |title=AEG, StubHub Partnership Brings Benefits Far Beyond Secondary Ticketing |publisher=Billboard |date=2012-11-12 |accessdate=2013-11-18}}</ref>

== References ==

{{reflist|2|refs=

<ref name="Forbes20121015">{{cite news |url=http://www.forbes.com/sites/mikeozanian/2012/10/15/value-of-aeg-disclosed-to-be-at-least-8-billion/ |title=Value Of AEG Disclosed To Be At Least $8 Billion |publisher=Forbes |date= |accessdate=2013-11-18 |first=Mike |last=Ozanian}}</ref>

<ref name="NYT20110822">{{cite news|author=Ben Sisario |url=https://www.nytimes.com/2011/08/23/business/media/aeg-starts-ticket-selling-site-to-take-on-ticketmaster.html |title=Ticketmaster Competitor to Unveil a Web Site |publisher=New York Times |date=August 22, 2011 |deadurl=no |accessdate=2013-12-17}}</ref>

}}

[[Category:Online retailers of the United States]]
[[Category:Companies based in Los Angeles County, California]]
[[Category:Ticket sales companies]]
[[Category:Companies established in 2011]]