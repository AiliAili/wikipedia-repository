{{distinguish|Open Humanities Press}}
{{Infobox publisher
| name         = Open Library of Humanities
| image        = 
| caption      =
| parent       = 
| status       = 
| traded_as    =
| predecessor  =
| founded      = 2015
| founder      = <!-- or | founders = -->
| successor    = 
| country      = United Kingdom
| headquarters = [[London]], England
| distribution = 
| keypeople    = 
| publications = Academic journals
| topics       = Humanities
| genre        = 
| imprints     = 
| revenue      = 
| owner        =
| numemployees =
| url          = {{URL|https://www.openlibhums.org}}
}}

The '''Open Library of Humanities''' is a non-profit [[open access]] publisher for the humanities and social sciences,<ref name=about>{{cite web|title=About|url=https://www.openlibhums.org/about/|work=Open Library of Humanities|accessdate=19 November 2013|year=2013}}</ref><ref name=che>{{cite web|last=Howard|first=Jennifer|title=Project Aims to Bring PLOS-Style Openness to the Humanities|url=http://chronicle.com/article/Project-Aims-to-Bring/136889/|work=The Chronicle of Higher Education|accessdate=19 November 2013|date=29 January 2013}}</ref> led by the academics Dr. Martin Paul Eve and Dr. Caroline Edwards.<ref>Adeline Koh, 'Mellon Funding for the Open Library of the Humanities', ''The Chronicle of Higher Education'', April 18, 2014, http://chronicle.com/blogs/profhacker/mellon-funding-for-the-open-library-of-the-humanities/56649.</ref> It is also a [[megajournal]] which was initially modelled on the [[Public Library of Science]] but is not affiliated with it.<ref name=about />

==History==
The Open Library of Humanities is being funded by core grants from the [[Andrew W. Mellon Foundation]],<ref>{{cite web|title=Funding from the Andrew W. Mellon Foundation|url=https://www.openlibhums.org/2014/04/07/funding-from-the-andrew-w-mellon-foundation/|work=Open Library of Humanities|accessdate=7 April 2014|date=7 April 2014}}</ref><ref>{{cite web|url=http://www.bbk.ac.uk/news/birkbeck-awarded-741-000-grant-for-new-humanities-open-access-model-of-publishing|title=Birkbeck awarded $741,000 grant for new humanities open-access model of publishing|publisher=}}</ref> and will also use a library partnership subsidy model to cover costs.<ref>{{cite web|title=Open Access Monographs in the Humanities and Social Sciences Conference Report|url=https://www.jisc-collections.ac.uk/Reports/oabooksreport/|publisher=Jisc Collections and OAPEN|accessdate=19 November 2013|page=10|year=2013}}</ref> It has a number of advisory committees, such as the Academic Steering & Advocacy Committee which includes PLOS co-founder [[Michael Eisen]],<ref name=che /> Quebec-based academic [[Jean-Claude Guédon]], and the Director of Scholarly Communication of the [[Modern Language Association]], [[Kathleen Fitzpatrick (American academic)|Kathleen Fitzpatrick]].<ref>{{cite web|title=Academic Steering & Advocacy Committee|url=https://www.openlibhums.org/committees/academic-steering-advocacy-committee/|work=Open Library of Humanities|accessdate=19 November 2013|year=2013}}</ref> There is an Internationalisation committee to develop an international strategy.<ref>{{cite web|last=Schwartz|first=Meredith|title=Open Library of Humanities Begins Infrastructure Phase|url=http://lj.libraryjournal.com/2013/02/oa/open-library-of-humanities-begins-infrastructure-phase/|work=Library Journal|accessdate=19 November 2013|date=14 February 2013}}</ref> A member of this committee, Dr. Francisco Osorio, has written that the open access model of the Open Library of Humanities may be beneficial for researchers publishing in languages other than English.<ref>{{cite web|last=Osorio|first=Francisco|title=Open Library of Humanities: mega journals seeing from the south|url=http://www.facso.uchile.cl/noticias/90036/open-library-of-humanities-mega-journals-seeing-from-the-south|work=Facultad de Ciencias Sociales, Universidad de Chile|accessdate=19 November 2013|date=5 April 2013}}</ref>

The technical platform is based on [[Open Journal Systems]] and will use an XML-first publication environment.<ref>{{cite web|title=Roadmap for Technical Pilot|url=https://www.openlibhums.org/2013/05/15/roadmap-for-technical-pilot/|work=Open Library of Humanities|accessdate=19 November 2013|date=15 May 2013}}</ref> The [[University of Lincoln]], in partnership with  the [[Public Knowledge Project]], offered a funded place for an MSc by Research in Computer Science in order to develop an open-source [[XML]] typesetting tool as proposed by the Open Library of Humanities technical roadmap.<ref>{{cite web|title=Funding Opportunity in MSc Computer Science by Research|url=http://www.lincoln.ac.uk/home/course/cmsresms/fundedplace/|work=University of Lincoln|accessdate=19 November 2013|year=2013}}</ref> In November 2013 it was announced that the Public Knowledge Project will be funding the development of the typesetter, known as meTypeset.<ref>{{cite web|title=PKP supporting OLH development of in-house typesetter |url=http://pkp.sfu.ca/pkp-supporting-olh-development-of-in-house-typesetter/|work=Public Knowledge Project|accessdate=21 November 2013|date=20 November 2013}}</ref>

The Open Library of Humanities publishing model relies on support from an international collection of [[libraries]] which in effect allows the publication of articles without the need for [[Article Processing Charges]].<ref>{{cite web|url=https://www.openlibhums.org/site/about/|title=Open Library of Humanities|publisher=}}</ref>

The project was officially launched, Monday 28 September 2015.<ref>{{cite web|url=https://about.openlibhums.org/2015/09/28/olh-launches/|title=OLH Launches|work=Open Library of Humanities}}</ref>

==References==
{{reflist}}

==External links==
*[https://www.openlibhums.org/ Open Library of Humanities]

[[Category:Non-profit academic publishers]]
[[Category:Open access publishers]]
[[Category:Andrew W. Mellon Foundation]]
[[Category:Open access projects]]
[[Category:Continuous journals]]


{{Publish-company-stub}}