{{featured article}}
{{Taxobox
| name = King Island emu
| status = EX
| extinct = 1822
| status_system = IUCN3.1
| status_ref = <ref name = IUCN>{{IUCN|assessor=BirdLife International |version=2013.2 |year=2012 |id=22728643 |title=''Dromaius minor'' |downloaded=20 January 2014}}</ref>
| image = Dromaius peroni.jpg
| image_width = 250px
| image_caption = 1907 illustration by [[John Gerrard Keulemans]], based on the Paris skin
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[bird|Aves]]
| superordo = [[Paleognathae]]
| ordo = [[Casuariiformes]]
| familia = [[Dromaiidae]]
| genus = ''[[Dromaius]]''
| species = [[Dromaius novaehollandiae|''D. novaehollandiae'']]
| subspecies = †'''''D. n. minor'''''
| trinomial = ''Dromaius novaehollandiae minor''
| trinomial_authority = (<small>[[Walter Baldwin Spencer|Spencer]], 1906</small>)
| synonyms ={{collapsible list|bullets = true|title=<small>List</small>
|''Casuarius diemenianus'' <small>Jennings,1827</small>
|''Dromaeus parvulus'' <small>[[William John Broderip|Broderip]], 1842</small>
|''Dromaeus minor'' <small>[[Walter Baldwin Spencer|Spencer]], 1906</small>
|''Dromaius peroni'' <small>[[Walter Rothschild, 2nd Baron Rothschild|Rothschild]], 1907</small>
|''Dromaius bassi'' <small>[[William Vincent Legge|Legge]], 1907</small>
|''Dromaius parvulus'' <small>[[Gregory Mathews|Mathews]], 1910</small>
|''Dromiceius spenceri'' <small>Mathews, 1912</small>
|''Peronista peroni'' <small>(Mathews, 1913)</small>
|''Dromaius diemenianus'' <small>(Morgan & [[George Miksch Sutton|Sutton]], 1928)</small>}}
| range_map = Dromaius distribution.png
| range_map_width = 250px
| range_map_caption = Geographic distribution of emu taxa and historic shoreline reconstructions around Tasmania, ''D.&nbsp;n.&nbsp;minor'' in red}}

The '''King Island emu''' (''Dromaius novaehollandiae minor'') is an [[extinct]] [[subspecies]] of [[emu]] that was [[endemic]] to [[King Island, Tasmania|King Island]], which is situated in the [[Bass Strait]] between mainland [[Australia]] and [[Tasmania]]. Its closest relative may be the extinct [[Tasmanian emu]] (''D. n. diemenensis''), as they belonged to a single population until less than 14,000 years ago, when Tasmania and King Island were still connected. The small size of the King Island emu may be an example of [[insular dwarfism]].

The King Island emu was the smallest of all emus, and had darker [[plumage]] than the mainland emu. It was black and brown, and had naked blue skin on the neck, and its chicks were striped like those on the mainland. The subspecies was distinct from the likewise diminutive [[Kangaroo Island emu]] (''D. baudinianus'') in a number of [[osteological]] details, including size. The behaviour of the King Island emu probably did not differ much from that of the mainland emu. The birds gathered in flocks to forage and during breeding time. They fed on berries, grass and seaweed. They ran swiftly, and could defend themselves by kicking. The nest was shallow, and consisted of dead leaves and moss. Seven to nine eggs were laid, which were [[Avian incubation|incubated]] by both parents.

Europeans discovered the King Island emu in 1802 during early expeditions to the island, and most of what is known about the bird in life comes from an interview French naturalist [[François Péron]] conducted with a [[seal hunting|sealer]] there, as well as depictions by artist [[Charles Alexandre Lesueur]]. They had arrived on King Island in 1802 with [[Baudin expedition to Australia|Nicolas Baudin's expedition]], and in 1804 several live and stuffed King and Kangaroo Island emus were sent to France. The two live King Island specimens were kept in the [[Jardin des Plantes]], and the remains of these and the other birds are scattered throughout various museums in Europe today. The logbooks of the expedition did not specify from which island each captured bird originated, or even that they were [[taxonomically]] distinct, so their status remained unclear until more than a century later. Hunting pressure and fires started by early settlers on King Island likely drove the wild population to extinction by 1805. The two captive specimens in Paris both died in 1822 and are believed to have been the last of their kind.

==Taxonomy==
[[File:Freycinet Australian birds.jpg|thumb|left|1807 [[cartouche]] of a small emu and other birds from a map of south east Australia and [[Kangaroo Island]]]]
There was long confusion regarding the [[taxonomic]] status and geographic origin of the small island emu taxa from [[King Island (Tasmania)|King Island]] and [[Kangaroo Island]], since specimens of both populations were transported to France as part of the same [[French expedition to Australia]] in the early 1800s. The logbooks of the expedition failed to clearly state where and when the small emu individuals were collected, and this has resulted in a plethora of [[scientific names]] subsequently being coined for either bird, many on questionable grounds, and the idea that all specimens had originated from Kangaroo Island.<ref name="Fuller Extinct">{{cite book
 | last = Fuller
 | first = E.
 | authorlink = Errol Fuller
 | title = Extinct Birds
 | publisher = Comstock
 | series =
 | volume =
 | edition = revised
 | location = New York
 | year = 2001
 | page = 33
 | doi =
 | isbn = 978-0-8014-3954-4
 | mr =
 | zbl = }}</ref> Furthermore, in 1914, L. Brasil argued the expedition did not encounter emus on King Island, because the weather had been too bad for them to leave their camp.<ref>{{Cite journal | last1 = Brasil | first1 = L. | title = The Emu of King Island | doi = 10.1071/MU914088 | journal = Emu | volume = 14 | issue = 2 | pages = 88–97| year = 1914 | pmid =  | pmc = }}</ref> The French also referred to both emus and [[cassowaries]] as "casoars" at the time, which has led to further confusion.<ref name="Mighty"/>

[[Louis Jean Pierre Vieillot]] coined the [[binomial nomenclature|binomial]] ''Dromaius&nbsp;ater'' in 1817.<ref>{{cite journal | last = Vieillot |first=L. J. P. | year = 1817 | title =''Dromaius ater''| url = | journal = Nouveau Dictionnaire d'Histoire Naturelle | volume = 11 | language = French | page = 212 }}</ref> In 1906, [[Walter Baldwin Spencer]] coined the name ''Dromaius&nbsp;minor'' based on some [[Pleistocene]] [[subfossil]] bones and eggshells found on King Island the same year, believing they were the first physical evidence of an emu from there.<ref>{{Cite journal
| last1 = Spencer
| first1 = W. B.
| first2 = J. A.
| last2 = Kershaw
| title = A Collection of sub-fossil birds and marsupial remains from King Island, Bass Straits
| journal =  Memoirs of the National Museum of Melbourne
| volume =  3
| pages = 5–35
| date =
| year = 1906 }}</ref> [[William Vincent Legge]] also coined a name for these remains, ''Dromaius&nbsp;bassi'', but at a later date.<ref>{{Cite journal | last1 = Legge | first1 = W. V. | title = The Emus of Tasmania and King Island | doi = 10.1071/MU906116 | journal = Emu | volume = 6 | issue = 3 | pages = 116–119 | year = 1906 | pmid =  | pmc = }}</ref> In his 1907 book ''[[Extinct Birds (Rothschild book)|Extinct Birds]]'', [[Walter Rothschild]] stated that Vieillot's description actually referred to the mainland emu, and that the name ''D.&nbsp;ater'' was therefore invalid. Believing the skin in [[Muséum national d'Histoire naturelle]] of Paris was from Kangaroo Island, he made it the [[type specimen]] of his new species ''Dromaius&nbsp;peroni'', named after the French naturalist [[François Péron]], who is the main source of information about the bird in life.<ref name="Rothschild"/>
[[File:Lesueur Emu.jpg|thumb|left|[[Charles Alexandre Lesueur]]'s 1807 plate of the head, wing and feathers of a specimen possibly belonging to this subspecies]]
The Australian amateur ornithologist [[Gregory Mathews]] coined further names in the early 1910s, including a new [[genus]] name, ''Peronista'', as he believed the King and Kangaroo Island birds were generically distinct from the mainland emu.<ref name="Mathews">{{Cite book | last1 = Mathews | first1 = G. M. | last2 = Iredale | first2 = T. | year=1921 | title = A Manual of the Birds of Australia | url = http://www.biodiversitylibrary.org/item/49436#page/33/mode/1up | volume=Vol. 1 | page = 5 | publisher=H. F. & G. Witherby | doi = 10.5962/bhl.title.14116}}</ref> Later writers claimed that the subfossil remains found on King and Kangaroo Islands were not discernibly different, and that they therefore belonged to the same [[taxon]].<ref name="Extinct Birds"/><ref>{{Cite journal | last1 = Morgan | first1 = A. M. | last2 = Sutton | first2 = J. | doi = 10.1071/MU928001 | title = A critical description of some recently discovered bones of the extinct Kangaroo Island Emu (''Dromaius diemenianus'') | journal = Emu | volume = 28 | pages = 1–19  | year = 1928 | pmid =  | pmc = }}</ref> In 1959, the French ornithologist [[Christian Jouanin]] proposed that none of the skins were actually from Kangaroo Island, after inspecting expedition and museum documents.<ref>{{Cite journal
 | last = Jouanin
 | first = C.
 | title = Les emeus de l'expédition Baudin
 | journal = L'Oiseau et la Revue Française d'Ornithologie
 | volume = 29
 | pages = 168–201
| language = French
 | year = 1959 }}</ref> In 1990, Jouanin and [[Jean-Christophe Balouet]] used environmental [[forensics]] to demonstrate that the mounted skin in Paris came from King Island, and that at least one live bird had been brought from each island.<ref>{{cite journal
| last1 = Balouet
| first1 = J. C.
| last2 =  Jouanin
| first2 = C.
|year= 1990
|title= Systématique et origine géographique de émeus récoltés par l'expédetion Baudin
| journal = L'Oiseau et la Revue Française d'Ornithologie
| volume = 60
| pages = 314–318
| language = French}}</ref> All scientific names given to the [[Kangaroo Island emu]] were therefore based on specimens from King Island or were otherwise [[valid name (zoology)|invalid]], leaving it nameless. More recent finds of subfossil material and subsequent studies on King and Kangaroo Island emu, notably by [[Shane A. Parker]] in 1984, confirmed their separate geographic origin and distinct [[morphology (biology)|morphology]]. Parker named the Kangaroo Island bird ''Dromaius&nbsp;baudinianus'', after [[Nicolas Baudin]], the leader of the French expedition. The name ''Dromaius ater'' was kept for the King Island emu.<ref name="Parker 1984">{{cite journal | last = Parker | first = S. A. | year = 1984 | title = The extinct Kangaroo Island Emu, a hitherto unrecognised species | url = | journal = [[Bulletin of the British Ornithologists' Club]] | volume = 104 | issue = | pages = 19–22 }}</ref>

There are few morphological differences that distinguish the extinct insular emus from the mainland emu besides their size, but all three taxa were most often considered distinct species. A 2011 genetic study of [[nuclear DNA|nuclear]] and [[mitochondrial DNA]], which was extracted from five subfossil King Island emu bones, showed that its genetic variation fell within that of the extant mainland emus. It was therefore interpreted as [[conspecific]] with the emus of the Australian mainland, and was reclassified as a [[subspecies]] of ''[[Dromaius&nbsp;novaehollandiae]]'', ''D. n. ater''. Other animals present on King Island are also considered as subspecies of their mainland or Tasmanian counterparts rather than distinct species. The authors suggested that further studies using different methods might be able to find features that distinguish the taxa.<ref name=Heupink/> In its 2013 edition, [[The Howard and Moore Complete Checklist of the Birds of the World]] emended the trinomial name of the King Island emu to ''D. n. minor'', based on Spencer's ''D. minor'', on the ground that Vieillot's ''D. ater'' was originally meant for the mainland emu.<ref>
{{cite book
| last1 = Howard
| first1 = R.
| last2 = Moore
| first2 = A.
|year= 2013
| page = 6
|title= The Howard and Moore Complete Checklist of the Birds of the World, Volume 1: Non-passerines
|publisher= Dickinson & Remsen
|isbn= 978-0956861108
|location=}}</ref> This rationale was accepted by the [[IOC World Bird List]], which used ''D. n. minor'' thereafter.<ref name="IOC">{{cite web|url=http://www.worldbirdnames.org/updates/subspecies/|publisher=IOC World Bird List|title=Subspecies Updates|author=Gill, F.|author2=Donsker, D.|accessdate=July 13, 2015|year=2015}}</ref>

===Evolution===
[[File:King Island Emu.jpg|thumb|1834 illustration of the Paris skin by [[Louis Jean Pierre Vieillot]]]]
During the [[Late Quaternary]] period (0.7 million years ago), small emus lived on a number of offshore islands of mainland Australia. In addition to the King Island emu, these included taxa found on Kangaroo Island (''D.&nbsp;baudinianus'') and Tasmania (''[[D.&nbsp;n.&nbsp;diemenensis]]''), all of which are now extinct. The smallest taxon, the King Island emu, was confined to a small island situated in the [[Bass Strait]] between [[Tasmania]] and Victoria, approximately 100&nbsp;km (62&nbsp;mi) from both coasts. King Island was once part of the [[land bridge]] which connected Tasmania and mainland Australia, but rising sea levels following the [[last glacial maximum]] eventually isolated the island. As a result of [[phenotypic plasticity]] the King Island emu population possibly underwent a process of [[insular dwarfism]].<ref name=Heupink/>

According to the authors of the 2011 genetic study, the close relation between the King Island and mainland emus indicates that the former population was isolated from the latter relatively recently, due to sea level changes in the Bass Strait, as opposed to a [[founder effect|founding]] emu lineage that diverged from the mainland emu far earlier and had subsequently gone extinct on the mainland.<ref name=Heupink/> Models of sea level change indicate that Tasmania, including King Island, was isolated from the Australian mainland around 14,000 years ago. Up to several thousand years later King Island was then separated from Tasmania.<ref>{{Cite journal | last1 = Lambeck | first1 = K. | last2 = Chappell | first2 = J. | title = Sea level change through the last glacial cycle | doi = 10.1126/science.1059549 | journal = Science | volume = 292 | issue = 5517 | pages = 679–686 | year = 2001 | pmid = 11326090 | pmc = | bibcode = 2001Sci...292..679L }}</ref> This scenario would suggest that a population ancestral to both the King Island and Tasmanian emu was initially isolated from the mainland taxon, after which the King Island and Tasmanian populations were separated. This in turn indicates that the likewise extinct Tasmanian emu is probably as closely related to the mainland emu as is the King Island emu, with both the King Island and Tasmanian emu being more closely related to each other. Fossil emu taxa show an average size between that of the King Island emu and mainland emu. Hence, mainland emus can be regarded as a large or gigantic form.<ref name=Heupink>{{Cite journal | last1 = Heupink | first1 = T. H. | last2 = Huynen | first2 = L. | last3 = Lambert | first3 = D. M. | editor1-last = Fleischer | editor1-first = Robert C. | title = Ancient DNA suggests dwarf and 'giant' emu are conspecific | journal = PLoS ONE | volume = 6 | issue = 4 | pages = e18728 | year = 2011 | pmid = 21494561 | pmc = 3073985 | doi = 10.1371/journal.pone.0018728| bibcode = 2011PLoSO...618728H }}</ref>

==Description==
[[File:Emu size.png|thumb|left|upright|Size comparison between a human, the mainland emu, and the King Island emu]]
The King Island emu was the smallest emu taxon, and was about half the size of the mainland birds. It was about {{convert|87|cm|in|abbr=on}} tall. According to François Péron's interview with a local [[seal hunting|sealer]], the largest specimens were up to 137&nbsp;cm (4.5&nbsp;ft) in length, and the heaviest weighed 20 to 23&nbsp;kg (45 to 50&nbsp;lb). It had a darker [[plumage]], with extensive black feathers on the neck and head, and blackish feathers on the body, where it was also mixed with brown.<ref name="Extinct Birds"/> The bill and feet were blackish, and the naked skin on the side of the neck was blue.<ref name="Rothschild">{{Cite book
  | last = Rothschild
  | first = W.
  | authorlink = Walter Rothschild, 2nd Baron Rothschild
  | title = Extinct Birds
  | publisher = Hutchinson & Co
  | year = 1907
  | location = London
  | pages = 235–237
  | url = https://archive.org/stream/extinctbirdsatte00roth#page/234/mode/2up
}}</ref> The 2011 genetic study did not find [[genes]] commonly associated with [[melanism]] in birds, but proposed the dark colouration could be due to alternative genetic or non-genetic factors.<ref name=Heupink/> Péron stated there was little difference between the sexes, but that the male was perhaps brighter in colouration and slightly larger. The juveniles were grey, while the chicks were striped like other emus. There were no seasonal variations in plumage.<ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|year= 2012
|title= Extinct Birds
|publisher= A & C Black
|location= London
|pages= 19–21
|isbn=978-1-4081-5725-1}}</ref> Since the female mainland emus are on average larger than the males, and can turn brighter during the mating season, contrary to the norm in other bird species, some of these observations may have been based on erroneous [[conventional wisdom]].<ref name="Mighty"/>

Subfossil remains of the King Island emu show that the [[tibia]] was about 330&nbsp;mm (13&nbsp;in) long, and the [[femur]] was 180&nbsp;mm (7&nbsp;in) long. The [[pelvis]] was 280&nbsp;mm (11&nbsp;in) long, 64&nbsp;mm (2.5&nbsp;in) wide at the front, and 86&nbsp;mm (3&nbsp;in) wide at the back.<ref name="Rothschild"/> The [[tarsometatarsus]] averaged 232&nbsp;mm (9&nbsp;in) in length. In males, the [[tibiotarsus]] averaged 261&nbsp;mm (10&nbsp;in), whereas it averaged 301&nbsp;mm (12&nbsp;in) in females. In contrast, the same bones measured 269&nbsp;mm (10.5&nbsp;in) and 305&nbsp;mm (12&nbsp;in) in the Kangaroo Island emu. Apart from being smaller, the King Island emu differed osteologically from the Kangaroo Island emu in the intertrochlear [[foramen]] of the tarsometatarsus usually being fully or partially abridged. The outer [[Trochlear process|trochlea]] was more incurved towards the middle trochlea in the Kangaroo Island bird, whereas they were parallel in the King Island emu.<ref name="Parker 1984"/>
[[File:Emu skulls.png|thumb|Comparison of the cranium contour in mainland (A, B, C) and King Island emus (D, E)]]
The King Island emu and the mainland emu show few morphological differences other than their significant difference in size. Mathews stated that the legs and bill were shorter than those of the mainland emu, yet the toes were nearly of equal length, and therefore proportionally longer. The tarsus of the King Island emu was also three times longer than the [[culmen (bird)|culmen]], whereas it was four times longer in the mainland emu.<ref name="Mathews"/> Additional traits that supposedly distinguish this bird from the mainland emu have previously been suggested to be the distal foramen of the tarsometatarsus, and the contour of the [[cranium]]. However, the distal foramen is known to be variable in the mainland emu showing particular diversity between juvenile and adult forms and is therefore taxonomically insignificant.<ref>{{cite journal |last1=Patterson |first1=C. |last2=Rich |first2=P. |year=1987 |title=The fossil history of the Emus, ''Dromaius'' (Aves: Dromaiinae) |url=http://biostor.org/reference/114156 |journal=Records of the South Australian Museum |volume=21 |issue=2 |pages=85–117 |oclc=156775809}}</ref> The same is true of the contour of the cranium, which is more dome-shaped in the King Island emu, a feature that is also seen in juvenile mainland emus.<ref name=Heupink/>

==Behaviour and ecology==
[[File:Centenaire de la fondation du Muséum d'histoire naturelle 10 juin 1793 - 10 juin 1893 - volume commémoratif (1893) (19965217444).jpg|left|thumb|1893 Keulemans illustration based on the Paris skin]]
Péron's interview describes some aspects of the behaviour of the King Island emu. He writes that the bird was generally solitary but gathered in flocks of ten to twenty at breeding time, then wandered off in pairs. They ate berries, grass and seaweed, and foraged mainly during morning and evening. They were swift runners, but were apparently slower than the mainland birds, due to being fat. They swam well, but only did so when necessary. They reportedly liked the shade of lagoons and the shoreline, rather than open areas. They used a claw on each wing for scratching themselves. If unable to flee from the [[hunting dogs]] of the sealers, they would defend themselves by kicking, which could inflict a great deal of harm.<ref>{{cite journal
 | last1 = Milne-Edwards
 | first1 = M.
 | first2 = E.
 | last2 = Oustalet
 | title = Note sur l'Émeu noir (''Dromæs ater'' V.) de l'île Decrès (Australie)
 | journal = Bulletin du Muséum d'Histoire Naturelle
 | volume = 5
 |url= http://biodiversitylibrary.org/page/5029585
 | pages = 206–214
 | language = French
 | year = 1899 }}</ref>
[[File:Pink Mountain Berries Lake St Clair.jpg|thumb|Berries of ''[[Leptecophylla juniperina]]'', which was among the diet of this emu]]
Captain [[Matthew Flinders]] did not encounter emus when he visited King Island in 1802, but his naturalist, Robert Brown, examined their dung and noted they had chiefly fed on the berries of ''[[Leptecophylla juniperina]]''.<ref name="Mighty"/> An account by English ornithologist [[John Latham (ornithologist)|John Latham]] about the "[[Van Diemen's]] cassowary" may also refer to the King Island emu, based on the small size described. In addition to a physical description, he stated that they gathered in groups of 70 to 80 individuals in a given location while foraging, behaviour that was exploited by hunters.<ref name="Extinct Birds"/>

Péron stated that the nest was usually situated near water and on the ground under the shade of a bush. It was constructed of sticks and lined with dead leaves and moss; it was oval in shape and not very deep. He claimed that seven to nine eggs were laid always on 25 and 26 July, but the selective advantage of this breeding synchronisation is unknown. The female [[Avian incubation|incubated]] the eggs, but the male apparently developed a [[brood patch]], which indicates it contributed as well. The non-incubating parent also stayed by the nest, and the chicks left the nest two to three days after hatching.<ref name="Extinct Birds"/> The eggs were preyed upon by snakes, rats, and [[quolls]].<ref name="New creatures"/> Péron gave the incubation period as five or six weeks, but since the mainland emu incubates for 50 to 56 days, this may be too short. He stated a mother emu would defend its young from [[crows]] with its beak, but this is now known to be strictly male behaviour.<ref name="Mighty"/>

==Relationship with humans==
[[File:Château de Malmaison animals.jpg|thumb|Two small emus and other animals outside [[Château de Malmaison]], from the [[Book frontispiece|frontispiece]] of the Baudin expedition's report]]
The emus of King Island were first recorded by Europeans when a party from the ship ''[[HMS Lady Nelson (1798)|Lady Nelson]]'', led by [[John Murray (Australian explorer)|John Murray]], visited the island in January 1802. The bird was sporadically mentioned by travellers henceforward, but not in detail.<ref name="Mighty"/> Captain [[Nicolas Baudin]] visited King Island later in 1802, during an 1800–04 French expedition to map the coast of Australia. Two ships, ''[[French corvette Naturaliste|Le Naturaliste]]'' and ''[[French corvette Géographe|Le Géographe]]'', were part of the expedition, which also brought along naturalists who described the local wildlife.<ref name="Extinct Birds"/> François Péron, a naturalist who was part of Baudin's expedition, visited King Island and was the last person to record descriptions of the King Island emu from the wild.<ref name=Heupink/> At one point, Péron and some of his companions became stranded due to storms and took refuge with some [[seal hunters]]. They were served emu meat, which Péron described in favourable terms as tasting halfway "between that of the turkey-cock and that of the young pig".<ref name="Mighty"/>

Péron did not report seeing any emus on the island himself, which might explain why he described them as being the size of mainland birds. Instead, most of what is known about the King Island emu today stems from a 33-point [[questionnaire]] that he used to interview a local English sealer, Daniel Cooper, about the bird. In accordance with a request by the authorities for the expedition to bring back useful plants and animals, Péron asked if the emus could be bred and fattened in captivity, and received a variety of cooking recipes. Péron's questionnaire remained unpublished until 1899, and very little was therefore known about the bird in life until then.<ref name="Mighty"/>

===Transported specimens===
[[File:King Island Emu skeleton.jpg|thumb|upright|left|Mounted skeleton, [[Royal Zoological Museum, Florence]]]]
Several emu specimens belonging to the different subspecies were sent to France, both live and dead, as part of the expedition. Some of these exist in European museums today. ''Le Naturaliste'' brought one live specimen and one skin of the mainland emu to France in June 1803. ''Le Géographe'' collected emus from both King and Kangaroo Island, and at least two live King Island individuals, assumed to be a male and female by some sources, were taken to France in March 1804. This ship also brought skins of five juveniles collected from different islands. Two of these skins, of which the provenance is unknown, are presently kept in Paris and Turin; the rest are lost.<ref name="Extinct Birds"/> In addition to rats, cockroaches, and other inconveniences aboard the ships, the emus were incommoded by the rough weather which caused the ships to shake violently; some died as a result, while others had to be force fed so they did not starve to death. In all, ''Le Géographe'' brought 73 live animals of various species back to France.<ref name="New creatures"/>
[[File:Charles-Alexandre Lesueur on cage.jpg|thumb|Self portrait of Lesueur lying on a cage containing a bird in ''le Géographe'']]
The two individuals brought to France were first kept in captivity in the menagerie of [[Empress Josephine]], and were moved to the [[Jardin des Plantes]] after a year.<ref name="Mighty"/> The "female" died in April 1822, and its skin is now mounted in the [[Muséum national d'Histoire naturelle]] of Paris. The "male" died in May 1822, and is preserved as a skeleton in the same museum.<ref name="Extinct Birds"/> A feather of the Paris skin was given to the [[Tasmanian Museum and Art Gallery]], the only confirmed feather belonging to this subspecies currently in Australia.<ref name="New creatures">{{cite book
| last1 = Pfennigwerth
| first1 = S.
|year= 2013
| pages = 172–213
|title=Discovery and Empire: The French in the South Seas
|chapter=New Creatures Made Known: Some Animal Histories of the Baudin Expedition
|editor=West-Sooby, J.
|publisher= University of Adelaide Press
|url=http://www.adelaide.edu.au/press/titles/discovery/discovery-ebook.pdf
|isbn=9781922064523}}</ref> The Paris skin contains several bones, but not the pelvis, which is an indicator of sex, so the supposed female identity is unconfirmed. Péron noted that the small emus brought to France were distinct from those of the mainland, but not that they were distinct from each other, or which island each had come from, so their provenance was unknown for more than a century later.<ref name="Mighty"/>

There is also a skeleton in [[Museo di Storia Naturale di Firenze]], which it obtained from France in 1833, but was mislabelled as a [[cassowary]] until correctly identified by Italian zoologist [[Enrico Hillyer Giglioli]] in 1900.<ref name="Nature">{{Cite journal | last1 = Giglioli | first1 = H. H. | title = A third specimen of the extinct ''Dromaius ater'', Vieillot; found in the R. Zoological Museum, Florence | doi = 10.1038/062102a0 | journal = Nature | volume = 62 | issue = 1596 | page = 102 | year = 1900 | url = http://extinct-website.com/pdf/naturelond62londuoft1.pdf| pmc = | bibcode = 1900Natur..62..102G }}</ref> Several elements of this skeleton are missing, and some have been replaced with wooden copies. Its right metatarsus was damaged during life and had healed incorrectly.<ref>{{Cite journal | last1 = Giglioli | first1 = H. H. | doi = 10.1111/j.1474-919X.1901.tb07516.x | title = On a specimen of the extinct ''Dromaeus ater'' discovered in the Royal Zoological Museum, Florence | journal = Ibis | volume = 43 | pages = 1–10 | year = 1901| url = http://extinct-website.com/pdf/j.1474-919X.1901.tb07516.xnew.pdf| pmc = }}</ref> It was thought to be a male, but is now known to be a composite of two individuals. A fourth specimen was thought to be kept in the [[Liverpool Museum]], but it may simply be a juvenile mainland emu.<ref name="Extinct Birds"/> Apart from the King Island emu specimens brought to France, a few are also known to have been brought to mainland Australia in 1803, but their fate is unknown.<ref name="Mighty"/>

===Contemporary depictions===
[[File:Baudin emus.jpg|thumb|left|1807 plate by Lesueur, perhaps showing this subspecies on the right]]
Péron's 1807, three-volume account of the expedition, ''Voyage de découverte aux terres Australes'', contains an illustration (plate 36) of "casoars" by [[Charles-Alexandre Lesueur]], who was the resident artist during Baudin's voyage. The caption states the birds shown are from "Ile Decrès", the French name for Kangaroo Island, but there is confusion over what is actually depicted.<ref name="Extinct Birds"/> The two adult birds are labelled as a male and female of the same species, surrounded by juveniles. The family-group shown is improbable, since breeding pairs of the mainland Emu split up once the male begins incubating the eggs. Lesueur's preparatory sketches also indicate these may have been drawn after the captive birds in Jardin des Plantes, and not wild ones, which would have been harder to observe for extended periods.<ref name="Mighty"/>
[[File:Lessueur Emu.jpg|upright|thumb|Preparatory sketches for the 1807 plate]]
The Australian museum curator, Stephanie Pfennigwerth, has instead proposed that the larger, light-ruffed "male" was actually drawn after a captive Kangaroo Island emu, that the smaller, dark "female" is a captive King Island emu, that the scenario is fictitious, and the sexes of the birds indeterminable. They may instead only have been assumed to be male and female of the same species due to their difference in size. A crooked claw on the "male" has also been interpreted as evidence that it had lived in captivity, and it may also indicate that the depicted specimen is identical to the Kangaroo Island emu skeleton in Paris, which has a deformed toe. The juvenile on the right may have been based on the Paris skin of an approximately five-month-old King Island emu specimen, which may in turn be the individual that died on board ''le Geographe'' during rough weather, and was presumably stuffed there by Lesueur himself. The chicks may instead simply have been based on those of mainland emus, as none are known to have been collected.<ref name="Mighty">{{Cite journal | last1 = Pfennigwerth | first1 = S. | doi = 10.3366/E0260954109001661 | title = (William T. Stearn Prize 2009) "The mighty cassowary": The discovery and demise of the King Island emu | journal = Archives of Natural History | volume = 37 | pages = 74–90 | year = 2010 | pmid =  | pmc = }}</ref>

===Extinction===
[[File:Elephant seals on King Island.jpg|thumb|left|1807 plate by Lesueur showing [[elephant seals]] and [[Seal hunting|sealers]] on King Island]]
The exact cause for the extinction of the King Island emu is unknown. Soon after the bird was discovered, sealers settled on the island because of the abundance of [[elephant seals]]. Péron's interview with Daniel Cooper suggested that they likely contributed to the demise of the bird by hunting it, and perhaps by starting fires. Péron described how dogs were purpose-trained to hunt down the emus; Cooper even claimed to have killed no fewer than 300 emus himself.<ref name=Heupink/> Cooper had been on the island for six months, which suggests he killed 50 birds a month. His group of sealers consisted of eleven men as well as his wife, and they alone may have killed 3,600 emus by the time Péron visited them.<ref name="Mighty"/>

Péron explained that the sealers consumed an enormous quantity of meat, and that their dogs killed several animals each day. He also observed such hunting dogs being released on Kangaroo Island, and mused that they might wipe out the entire population of [[kangaroos]] there in some years, but he did not express the same sentiment about the emus of King Island.<ref name="Mighty"/> [[Natural fires]] may also have played a role.<ref name="Extinct Birds"/> It is probable that the two captive birds in France, which died in 1822, outlived their wild fellows on King Island, and were therefore the last of their kind.<ref name="Fuller Extinct"/> Though Péron stated King Island "swarmed" with emus in 1802, they may have become [[extinct in the wild]] as early as 1805.<ref name="Mighty"/>

In 1967, when the King Island emu was still thought to be only known from prehistoric remains, [[James Greenway]] questioned whether they could have been exterminated by a few natives, and speculated that fires started by prehistoric men or [[lightning]] may have been responsible. At this time, the mainland emu was also threatened by overhunting, and Greenway cautioned that it could end up sharing the fate of its island relatives if no measures were taken in time.<ref name =Greenway>{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection 13
 | series =
 | volume =
 | edition =
 | location = New York
 | year = 1967
 | pages = 141–144
 | doi =
 | isbn = 978-0-486-21869-4
 | mr =
 | zbl =  }}</ref>

==References==
{{reflist|30em}}

{{Casuariiformes}}

{{taxonbar}}

[[Category:Birds described in 1817]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds of Tasmania]]
[[Category:Dromaius]]
[[Category:Extinct birds of Australia]]
[[Category:Extinct flightless birds]]
[[Category:King Island (Tasmania)]]