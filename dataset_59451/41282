{{Spanish name|Balaguer|Ricardo}}
{{Infobox officeholder
|name			= Joaquín Balaguer
|image			= Joaquin Balaguer.jpg
|caption                   =Joaquin Balaguer, 1988
|predecessor2		= [[Héctor García-Godoy]]
|successor2		= [[Antonio Guzmán Fernández]]
|vicepresident2		= [[Francisco Augusto Lora]]<br>[[Carlos Rafael Goico]]
|party			= [[Dominican Party]]<br><small>(1957–1961)</small><br>[[Independent (politics)|Independent]]<br><small>(1961–1966)</small><br>[[Social Christian Reformist Party]]<br><small>(1966–1996)</small>
|office1		= [[List of Presidents of the Dominican Republic|41st, 45th & 49th President of the Dominican Republic]]
|predecessor1		= [[Salvador Jorge Blanco]]
|successor1		= [[Leonel Fernández]]
|vicepresident1		= [[Carlos Morales Troncoso]]<br>[[Jacinto Peynado y Garrigosa]]
|birth_date		= 1 September 1906
|birth_place		= [[Bisonó]], [[Dominican Republic]]
|death_date		= 14 July 2002 (aged 95)
|death_place		= [[Santo Domingo]], [[Dominican Republic]]
|religion		= [[Catholicism]], [[Freemason]]
|term_start1		= 16 August 1986
|term_end1		= 16 August 1996
|term_start2		= 1 July 1966
|term_end2		= 16 August 1978
|term_start3		= 3 August 1960
|term_end3		= 16 January 1962
|predecessor3		= [[Héctor Trujillo]]
|successor3		= [[Rafael Filiberto Bonnelly]]
|vicepresident3		= [[Rafael Filiberto Bonnelly]]
|office4		= [[List of Vice Presidents of the Dominican Republic|Vice President of the Dominican Republic]]
|term_start4		= 16 August 1957
|term_end4		= 3 August 1960
|president4		= [[Héctor Trujillo]]
|predecessor4		= [[Manuel de Jesús Troncoso de la Concha|Manuel Troncoso de la Concha]]
|successor4		= [[Rafael Filiberto Bonnelly]]
}}

''' Joaquín Antonio Balaguer Ricardo''' (1 September 1906 &ndash; 14 July 2002) was the President of the [[Dominican Republic]] who served three non-consecutive terms for that office for his first term from 1960 to 1962, again for a second term from 1966 to 1978, and again for a third and final term from 1986 to 1996.

==Early Life and Introduction to Politics==
Balaguer was born in [[Villa Bisonó]] (also known as Navarrete), [[Santiago Province (Dominican Republic)|Santiago Province]] in the northwestern corner of the Dominican Republic. His father was Joaquín Jesús Balaguer Lespier,<ref name=Genealog/> a [[Puerto Rican people|Puerto Rican]] native of [[Catalan people|Catalan]] and [[France|French]] ancestry,<ref name=Genealog>{{cite web|title=Ancestros, descendientes y parientes colaterales de Joaquín Balaguer|url=http://hoy.com.do/capsulas-genealogicas-53/|accessdate=1 May 2014|work=Cápsulas Genealógicas|date=16 September 2006|archiveurl=https://web.archive.org/web/20140222070545/http://hoy.com.do/capsulas-genealogicas-53/ |archivedate=22 February 2014|publisher=Hoy}}</ref><ref>{{cite book|title=Globalización y localidad: espacios, actores, movilidades e identidades|year=2007|publisher=La Casa Chata|location=Mexico City|isbn=978-968-496-595-9|page=400|url=https://books.google.com/books?id=-EOPND5pEUEC&pg=PA400&lpg=PA400&dq=1920+censo+Rep%C3%BAblica+Dominicana+puertorrique%C3%B1os&source=bl&ots=WJd3lN7Zex&sig=O5cK0eykTNAPYPXCT1IcIc0tix4&hl=es&sa=X&ei=86KkUeiLNfGL0QGa14G4AQ&ved=0CEgQ6AEwBg#v=onepage&q=1920%20censo%20Rep%C3%BAblica%20Dominicana%20puertorrique%C3%B1os&f=false|author=Jorge Duany|editor=Margarita Estrada, Pascal Labazée|accessdate=28 May 2013|chapter=La migración dominicana hacia Puerto Rico: una perspectiva transnacional|quote=(...) Los historiadores han documentado la creciente presencia puertorriqueña en la República Dominicana durante el primer tercio del siglo XX. En 1920, el censo dominicano contó 6069 puertorriqueños residentes en la República Dominicana. Como resultado, los inmigrantes de segunda generación generalmente se identificaron como dominicanos, no como puertorriqueños. Los casos más célebres son los expresidentes Joaquín Balaguer y Juan Bosch, ambos de ascendencia dominicana y puertorriqueña. (...)}}</ref> and his mother was Carmen Celia Ricardo Heureaux,<ref name=Genealog/> daughter of Manuel de Jesus Ricardo and Rosa Amelia Heureaux (of French descent),<ref name=Genealog/> who was also a cousin of President [[Ulises Heureaux]].<ref name=Genealog/>{{efn|name= |[[Ulises Heureaux]]’s father was D’Assas Heureaux, a Haitian [[mulatto]]<ref>{{cite book|url=https://translate.google.com/translate?hl=en&sl=es&u=https://books.google.com/books%3Fid%3D_Q1pBfcSPmEC%26pg%3DPA9%26lpg%3DPA9%26dq%3DD%27assas%2BHeureaux%26source%3Dbl%26ots%3DaoGh5wmtus%26sig%3DS85jk_cK0RZet4fUnr0FKqdqs-8&prev=search |title=Ulises Heureaux: biography of a dictator |author=Sang, Mu-Kien Adriana |page=9 |accessdate=2 December 2014}}</ref><ref>{{cite book|url=https://books.google.com/books?id=P4f78sO07lAC&pg=PA40&lpg=PA40&dq=balaguer+de+haitiano+origen+Heureaux&source=bl&ots=xJ3OTCM5hz&sig=z9FJ8wiJsjkLHPp6ikL_iN9V28s | title=Peña Gómez in Haitian society |author=Manati |page=40 |year=2001 |accessdate=2 December 2014}}</ref><ref>{{cite web|url=https://books.google.com/books?id=rSbIAAAAQBAJ&pg=PA129&lpg=PA129&dq=Heureaux+haitien&source=bl&ots=EAjmS4fhEy&sig=mlxSrafK2dC2sw6IKoPG4JoE3oo&hl=en&sa=X&ei=f1OKVKXFOfSCsQTWnIHAAg&ved=0CD0Q6AEwCA#v=onepage&q=Heureaux%20haitian&f=false |title=Haitian-Dominican Counterpoint: Nation, Race and State on Hispaniola |author=Matibag, Eugenio |page=129 |accessdate=3 December 2014}}</ref> son of Pierre Alejandro (Pierre-Alexandre) Heureaux, a Frenchman and Roselia Jean-Louis, an African-born slave.<ref>{{cite book |last=Cassá |first=Roberto |title=Ulises Heureaux: el tirano perfecto |url=https://books.google.com/books?id=moZtAAAAMAAJ |publisher=Tobogán |accessdate=9 May 2014 |authorlink=Roberto Cassá |page=15 |language=Spanish |year=2001 |quote=Su madre, Josefa Leibert, era nativa de Saint Thomas, y su padre, D’Assas Heureaux, hijo de un francés, fue uno de los tantos haitianos que prefirió hacerse dominicano cuando se declaró la independencia en 1844. Padre y madre eran mulatos, aunque de piel oscura. |location=Santo Domingo |isbn=9789993483496 |oclc=51220478}}</ref><ref>{{cite web|url=http://www.carlstroem.com/heureaux/d1.htm |title=Descendants of Doyen Heureaux |accessdate=2 December 2014}}</ref>}} Balaguer was the only son in a family of several daughters.<ref name=Genealog/>

From a very early age, Balaguer felt an attraction to literature, composing verses that were published in local magazines even when he was very young.<ref>{{Cite web|url=http://www.educando.edu.do/articulos/estudiante/joaquin-balaguer-el-escritor/|title=Joaquín Balaguer: El escritor|website=www.educando.edu.do|language=en|access-date=2017-01-23}}</ref>  After graduating from school, Balaguer earned a law degree from the [[Universidad Autónoma de Santo Domingo|University of Santo Domingo]] and studied for a brief period at the University of Paris I Pantheon-Sorbonne <ref>{{Cite web|url=http://www.cidob.org/content/pdf/50817|title=Joaquín Balaguer|last=|first=|date=|website=|publisher=|access-date=}}</ref> As a youth, Balaguer wrote of the awe with which he was struck by his father’s fellow countryman, the [[Harvard University|Harvard]] graduate and political leader from Puerto Rico, [[Pedro Albizu Campos|Pedro Albizu]]. Despite the profound differences regarding their ethical and world visions, [[Pedro Albizu Campos|Albizu]]’s fiery and charismatic rhetoric captured Balaguer’s imagination and his recollection of this occasion was a harbinger of his passion for politics and intellectual debate <ref>{{Cite news|url=http://www.listindiario.com/la-republica/2012/07/14/239703/balaguer-pidio-le-sacaran-el-corazon|title=Balaguer pidió le sacaran el corazón|last=Diario|first=Listin|date=2012-07-14|newspaper=listindiario.com|language=es|access-date=2017-01-23}}</ref>

Balaguer’s political career began in 1930 (before [[Rafael Trujillo]] took control of the government) when he was appointed Attorney in the Court of Properties. In later years, he served as Secretary of the Dominican Legation in Madrid (1932–1935), Undersecretary of the Presidency (1936), Undersecretary of Foreign Relations (1937), Extraordinary Ambassador to Colombia and Ecuador (1940–1943 and 1943–1947), Ambassador to Mexico (1947-1949), Secretary of Education (1949–1955), and Secretary of State of Foreign Relations (1953–1956).{{citation needed|date=December 2015}}

There has been much discussion regarding Balaguer’s role during the ''Era of Trujillo''{{citation needed|date=December 2015}}, especially the relationship between the diminutive soft-spoken scholar and the boisterous ''Generalissimo''. Throughout the three decades working as a Trujillista politician, Balaguer was seen alternately both as a mere employee or as a distinguished close counselor of Trujillo. Despite the fact that Trujillo notoriously enjoyed humiliating and insulting his "servants" in public, the dictator never tried to degrade Balaguer nor to play practical jokes on him.{{citation needed|date=December 2015}}

Balaguer reciprocated Trujillo’s respect by spending the three decades of the Era as one of the most efficient public aides of the dictatorship, without seeming perturbed or showing the smallest gesture of disgust for the excesses and aberrations that were common at the time.{{citation needed|date=December 2015}} Balaguer was, without doubt, a useful minister of Trujillo, although it is not entirely possible to speak of total loyalty.

{{ahnentafel top|width=95%}}
<center>
{{Ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
| 1=  1. Joaquín Antonio Balaguer Ricardo
| 2=  2. [[File:Flag of Cross of Burgundy.svg|24px|link=Spanish Empire]][[File:PR Flag of 1873.jpg|24px|link=Captaincy General of Puerto Rico]] Joaquín Jesús Balaguer Lespier
| 3=  3. Carmen Celia Ricardo Heureaux<br />(half-cousin of [[Ulises Heureaux]])
| 4=  4. {{Flagicon|SPA}}{{Flagicon|CAT}} José Antonio Balaguer 
| 5=  5. [[File:Flag of Cross of Burgundy.svg|24px|link=Spanish Empire]][[File:PR Flag of 1873.jpg|24px|link=Captaincy General of Puerto Rico]] Isabel Lespier
| 6=  6. Manuel de Jesús Ricardo
| 7=  7. Rosa Amelia Heureaux Domínguez<br />(half-aunt of [[Ulises Heureaux]])

|14= 14. {{Flagicon|FRA}} Pierre-Alexandre Heureaux (grandfather of [[Ulises Heureaux]])
|15= 15. Josefa Domínguez
}}</center>
{{Ahnentafel bottom}}

==First presidency and its aftermath==
When Trujillo arranged to have his brother [[Héctor Trujillo|Héctor]] re-elected to the presidency in 1957, he chose Balaguer as vice-president. Three years later, when pressure from the [[Organization of American States]] (OAS) convinced the dictator that it was inappropriate to have a member of his family as president, Trujillo forced his brother to resign, and Balaguer succeeded to the post.{{citation needed|date=July 2015}}

The situation was dramatically altered, however, when Trujillo was assassinated in May 1961. Trujillo's son, [[Ramfis Trujillo|Ramfis]], initially inherited power with Balaguer as his puppet. They initially took steps to liberalize the regime, granting some civil liberties and easing Trujillo's tight censorship of the press{{citation needed|date=July 2015}}. Meanwhile, he revoked the nonaggression pact made with Cuba in January 1961. These measures did not go nearly far enough for a populace who had no memory of the instability and poverty that preceded Trujillo, and wanted more freedom and a more equitable distribution of wealth.{{citation needed|date=July 2015}} At the same time, Ramfis' reforms went too far for the hard-line ''trujillistas'' led by his own uncles, [[Héctor Trujillo|Héctor]] and [[José Arismendi Trujillo]]. As the OAS continued economic sanctions imposed for Trujillo's attempted murder of [[Venezuela]]n President [[Romulo Betancourt]], Ramfis warned that the country could descend into civil war between left and right.{{citation needed|date=July 2015}}

Although official and unofficial repression of the opposition parties (the [[Dominican Revolutionary Party]] and National Civic Union, as well as the communist Dominican Popular Movement) continued, Balaguer publicly condemned this repression and in September he pledged to form a coalition government. Hector and Jose Trujillo left the country in October but the opposition parties demanded Ramfis withdraw from the government as well. At the end of October, Ramfis announced that he would resign if the OAS agreed to lift the economic sanctions. The OAS agreed on November 14 but Ramfis’ uncles returned to the country the following day, hoping to lead a military coup. Ramfis resigned and went into exile on November 17 and rumours circulated that Air Force general [[Fernando Arturo Sánchez|Fernando Arturo Sánchez Otero]] would support pro-Castro revolutionaries. The United States now sent a small fleet of ships and 1,800 marines to patrol Dominican waters. The US consul informed Balaguer that these forces stood ready to intervene at his request, and would be supported by forces from Venezuela and Colombia. Air Force general [[Pedro Rafael Ramón Rodríguez Echavarría]] announced his support for Balaguer and bombed pro-Trujillo forces. The Trujillo brothers again fled the country on November 20 and Echavarría became [[List of Dominican Secretary of Armed Forces|Secretary of Armed Forces]].<ref>Giancarlo Soler Torrijos , ''One Round for Us and Freedom'', [[Life (magazine)|Life Magazine]]'', 1 December 1961.</ref><ref>''In the Shadow of the United States'' (2008), p. 52</ref>

The Union Civica Nacional (UCN) called a national strike and demanded the formation of a provisional government under their leader, [[Viriato Fiallo]], with elections to be delayed until 1964. The military were vehemently against the UCN taking power and Echaverría proposed a continuation of the Balaguer regime until the elections. The American consul mediated between the two sides and in January 1962 final agreement led to the creation of a seven-member Council of State, led by Balaguer but including members of the UCN, to replace both the Dominican Congress and the President and his cabint until the election.<ref>Eric Thomas Chester, ''Rag-tags, Scum, Riff-raff, and Commies'' (2001), p. 27<!-- ISBN needed --></ref> The OAS finally lifted sanctions against the country upon the formation of the council. However, popular unrest against Balaguer continued and many saw Echaverría as positioning himself to seize power. Military forces opened fire on demonstrators on 14 January which led to rioting the following day. On 16 January, Balaguer resigned and Echaverría staged a military coup d'état and arrested the other member of the council. With the US supporting the UCN and a new national strike beginning immediately, Echaverría was arrested by other officers two days later. The Council of State was restored under the leadership of [[Rafael Filiberto Bonnelly|Rafael Bonnelly]] and Balaguer went into exile in [[New York (state)|New York]] and [[Puerto Rico]].<ref>Harry Kantor, "The Dominican Crisis", ''The Lingering Crisis'' (1969), p. 1-19<!-- ISBN needed --></ref>

During those years the Dominican Republic had only seven months of true democracy, under the presidency of [[Juan Bosch]] from February 1963. When a military coup overthrew Bosch, the country began a tumultuous period which by 24 April 1965 saw the start of the [[Dominican Civil War]]. Military officers had revolted against the provisional Junta to restore Bosch, whereupon U.S. President [[Lyndon Johnson]], under the pretext of eliminating Communist influence in the Caribbean sent 42,000 U.S. troops to defeat the revolt in [[Operation Power Pack]], on 28 April. The provisional government, headed by [[Héctor García-Godoy]], announced general elections for 1966. Balaguer seized his chance, once he had the backing of the United States government, he returned to Dominican Republic with the purpose of destroying the popular groups that had participated in the rebellions of 1965. These groups of rebels consisted of young people who wanted change, who understood that Balaguer was no better than Trujillo.<ref>Escalante, F., & Muñiz, M. (1995). The secret war: CIA covert operations against Cuba, 1959-62. Melbourne: Ocean Press. </ref> He formed the Reformist Party and entered the presidential race against Bosch, campaigning as a moderate conservative advocating gradual and orderly reforms. He quickly gained the support of the establishment and easily defeated Bosch, who ran a somewhat muted campaign out of fear of military retribution.{{citation needed|date=July 2015}}

=="The Twelve Years" (1966-1978)==

Balaguer found a nation severely beaten by decades of turbulence, with few short times of peace, and virtually ignorant of democracy and human rights. He sought to pacify the enmities surviving from the Trujillo regime and from the 1965 civil war, but political murders continued to be frequent during his administration. He succeeded in partially rehabilitating the public finances, which were in a chaotic state, and pushed through a modest program of economic development.  He was easily reelected in 1970 against fragmented opposition and won again in 1974 after changing the voting rules in a way that led the opposition to boycott the race.

During his years as president (known popularly in Dominican politics as simply "the twelve years"), Balaguer ordered the construction of schools, hospitals, dams, roads, and many important buildings. He also presided over steady economic growth. Granted although he did fund public housing and opened public schools and expanded education during his term, he also assassinated more than 3,000 Dominicans between 1966 and 1974. Additionally over 300 politicians became millionaires during his "presidency".<ref>Escalante, F., & Muñiz, M. (1995). The secret war: CIA covert operations against Cuba, 1959-62. Melbourne: Ocean Press.</ref>  However, his administration soon developed a distinct [[authoritarianism|authoritarian]] cast, constitutional guarantees notwithstanding. Political opponents were jailed and sometimes killed, and opposition newspapers were occasionally seized. Despite his authoritarian methods, Balaguer had far less power than Trujillo, and his rule was milder.

==Defeat and return to power==

In 1978 Balaguer sought another term.  However, by this time, inflation was on the rise, and the great majority of the people had gotten little benefit from the economic boom of the past decade.  Balaguer faced [[Antonio Guzmán Fernández|Antonio Guzmán]], a wealthy rancher running under the banner of the [[Dominican Revolutionary Party]].  When election returns showed an unmistakable trend in Guzmán's favor, the military stopped the count.  However, amid vigorous protests at home and strong pressure abroad, the count resumed.  When the returns were all in, Guzmán handed Balaguer the first loss of his electoral career.<ref name="Buckman">{{cite book |title=The World Today Series: Latin America 2007 |last=Buckman |first=Robert T. |authorlink= |year=2007 |publisher=Stryker-Post Publications |location=Harpers Ferry, West Virginia |ISBN=978-1-887985-84-0}}</ref>  When Balaguer left office that year, it marked the first time in the Dominican Republic's history that an incumbent president peacefully surrendered power to an elected member of the opposition.

In the 1982 elections, the PRD's [[Salvador Jorge Blanco]] defeated Balaguer, who had merged his party with the Social Christian Revolutionary Party to form the [[Social Christian Reformist Party]] two years earlier.

Balaguer ran again in 1986, and took advantage of a split in the PRD and an unpopular austerity program to win the presidency again after an eight-year absence.  By this time, he was 80 years old and almost completely blind (he had suffered from [[glaucoma]] for many years).

==Third presidency==

Balaguer's third presidency was considerably more liberal than the previous one.  He was much more tolerant of opposition parties and human rights.

He undertook massive infrastructure projects, such as the construction of highways, bridges, schools, housing projects and hospitals. Following the style of Trujillo, these highly visible projects were very publicized over government-controlled media and through grandiose public ceremonies designed to enhance Balaguer's popularity. The projects were also used as a means to reward his political supporters with lucrative public works contracts.  The economy also improved considerably.

Balaguer was narrowly reelected in 1990, defeating his old foe Juan Bosch by only 22,000 votes out of 1.9 million votes cast amid charges of fraud.

For the 500th anniversary of [[Christopher Columbus]]' landing in the [[Americas]] and the visit of [[Pope]] [[John Paul II]], Balaguer spent millions on a restoration of parts of historic, colonial [[Santo Domingo]], and on sprucing up the parts of the City to be transversed by the Pope, including the construction of a grand new avenue lined with modern housing blocks.

More controversial was that Balaguer spent two hundred million [[US Dollars]] on the construction of a massive ten-story [[Columbus Lighthouse]].  Completed in 1992, the Columbus Lighthouse was designed to beam the image of a [[Christian cross]] into the night sky and to be visible for tens of miles.  Since completion, the Columbus Lighthouse, which supposedly houses Columbus' remains, has been a minor tourist attraction.  Its light has almost never been used due to extremely high energy costs and frequent blackouts in the country.  However, its symbolism and expense were the source of much controversy.

In January 1994 he decided to run again for the presidency, even when he was almost 90 years old and completely [[Blindness|blind]]. This time, his most prominent opponent was [[José Francisco Peña Gómez|José Francisco Peña]] of the PRD.

The campaign was one of the nastiest in Dominican history.  Balaguer frequently played up Peña's [[Haiti]]an ancestry to his advantage; Dominicans historically have a deep fear and mistrust of anyone with Haitian blood.  For example, Balaguer claimed that Peña would try to merge the country with Haiti if elected.  When the returns were announced, Balaguer was announced as the winner by only 30,000 votes.  However, many PRD supporters showed up to vote only to discover their names had vanished from the rolls.  Peña screamed fraud, and called a [[general strike]].  Demonstrations took place in support of the strike.

An investigation later revealed that the electoral board didn't know the total number of registered voters, and the voting lists distributed at polling stations didn't match those given to the parties.  The investigation also revealed that about 200,000 people had been removed from the polls.  Amid such questions about the poll's legitimacy, Balaguer agreed to hold new elections in 1996—in which he would not be a candidate.  It would be the first presidential election since 1966 in which Balaguer's name did not appear on the ballot.

In the 1996 election, Balaguer's vice president, Jacinto Peynado, finished well short of making it to the runoff.   Balaguer then threw his support to the [[Dominican Liberation Party]]'s [[Leonel Fernández]] in an unusual coalition with Bosch, his political foe of over 30 years.

==Death and legacy==
In 2000, Balaguer sought the presidency yet again. He won around 23% of the votes in [[Dominican Republic presidential election, 2000|the election]], with PLD candidate [[Danilo Medina]] just barely nosing him out for a spot in the runoff with PRD candidate [[Hipólito Mejía]].  Balaguer hinted that his supporters might split their votes between Mejía and Medina in the runoff.  Medina would have needed nearly all of Balaguer's supporters to cross over to him in order to have any realistic chance of overcoming a 25-point deficit in the first round. When it was apparent Medina would be lucky to get half of them, he pulled out of the runoff, handing the presidency to Mejía.

On July 14, 2002, Joaquín Balaguer died of [[heart failure]] at Santo Domingo's Abreu Clinic at the age of 95.

He was a [[Polarization (politics)|polarizing]] figure who could incite as much hate as love from the population.

Ronald Reagan once said of him "President Balaguer has been a driving force throughout his country's democratic development. In 1966 he led democracy's return to the Dominican Republic after years of political uncertainty and turmoil. Indeed, he is, in many ways, the father of Dominican democracy" and Jimmy Carter complimented him saying "President Balaguer has set an example for all leaders in this nation in changing his own country and his own people away from a former totalitarian government to one of increasingly pure democracy."

He is one of the central characters in [[Mario Vargas Llosa]]'s novel ''[[The Feast of the Goat]]''.

==Bibliography==

Balaguer was a prolific author, who wrote many books for contemporary Dominican literature. His most famous work was his only narrative novel, called ''"Los Carpinteros".'' The most controversial of his works is perhaps ''"Memorias de un Cortesano en la Era de Trujillo"'', in which Balaguer, shielded by his political power admitted knowing the truth about the death of the revolutionary journalist [[Orlando Martínez Howley|Orlando Martínez]]. Balaguer left a blank page in the middle of the book to be filled in at the time of his death.

Balaguer explored several branches of literature. As a thorough researcher, he published many biographical books still used as reference, along with compilations and analysis of Dominican folk poets. As a poet, he was mostly of Post-Romantic influence, and his style remained strictly unchanged along his long career. Other themes, despite the sorrow expressed, are mostly noble: and idyllic view of nature, nostalgia, and memoirs of the past.

His total list of literary works is as follows:

* Salmos paganos (1922)
* Claro de luna (1922)
* [[Tebaida lírica]] (1924)
* Nociones de métrica castellana (1930)
* Azul en los charcos (1941)
* La realidad dominicana (1941)
* El [[Trujillo-Hull Treaty|Tratado Trujillo‑Hull]] y la liberación financiera de la República Dominicana (1941)
* La política internacional de Trujillo (1941)
* Guía emocional de la ciudad romántica (1944)
* Letras dominicanas (1944)
* Heredia, verbo de la libertad (1945)
* Palabras con acentos rítmicos (1946)
* Realidad dominicana. Semblanza de un país y un régimen (1947)
* Los próceres escritores (1947)
* Semblanzas literarias (1948)
* En torno de un pretendido vicio prosódico de los poetas hispanoamericanos (1949)
* Literatura dominicana (1950)
* El Cristo de la libertad (1950)
* [[Federico García Godoy]] (antología, 1951)
* El principio de alternabilidad en la historia dominicana (1952)
* [[Juan Antonio Alix]]: [[Décima]]s (Prólogo y recopilación, 1953)
* Consideración acerca de la producción e inversión de nuestros impuestos (1953)
* Apuntes para una historia prosódica de la métrica castellana (1954)
* El pensamiento vivo de Trujillo (1955)
* Historia de la literatura dominicana (1956)
* Discursos. Panegíricos, política y educación política internacional (1957)
* Colón, precursor literario (1958)
* El centinela de la frontera. Vida y hazañas de [[Antonio Duvergé]] (1962)
* El Reformismo: filosofía política de la revolución sin sangre (1966)
* Misión de los intelectuales (Discurso, 1967)
* Con Dios, con la patria y con la libertad (Discurso, 1971)
* Conjura develada (Discurso, 1971)
* Ante la tumba de mi madre (1972)
* Temas educativos y actividades diplomáticas (1973)
* La marcha hacia el Capitolio (1973)
* Discursos. Temas históricos y literarios (1973)
* Temas educativos y actividades diplomáticas (1974)
* Cruces iluminadas (1974)
* La palabra encadenada (1975)
* [[José Martí|Martí]], crítica e interpretación (1975)
* La cruz de cristal (1976)
* Discursos escogidos (1977)
* Discurso en el develamiento de la estatua del poeta [[Fabio Fiallo]] (1977)
* Juan Antonio Alix, crítica e interpretación (1977)
* Pedestales. Discursos históricos (1979)
* Huerto sellado. Versos de juventud (1980)
* Mensajes al pueblo dominicano (1983)
* Entre la sangre del 30 de mayo y la del 24 de abril (1983)
* La isla al revés (1983)
* Galería heroica (1984)
* Los carpinteros (1984)
* La venda transparente (1987)
* Memorias de un cortesano de la «Era de Trujillo» (1988)
* Romance del caminante sin destino ([[Enrique Blanco]]) (1990)
* Voz silente (1992)
* De vuelta al capitolio 1986‑1992 (1993)
* España infinita (1997)
* Grecia eterna (1999)
* La raza inglesa (2000)

==Notes==
{{notelist|30em}}

==References==
<references/>
* [http://www.diariolibre.com/app/article.aspx?id=66437&commentMode=true Diariolibre.com]
* [http://www.presidency.ucsb.edu/ws/index.php?pid=35601&st=joaquin+balaguer&st1= UCSB.edu]
* [http://www.presidency.ucsb.edu/ws/index.php?pid=6598&st=joaquin+balaguer&st1= UCSB.edu]

==External links==
* [http://www.joaquinbalaguer.com.do Official page]
* [http://www.cidob.org/biografias_lideres_politicos/america_central_y_caribe/republica_dominicana/joaquin_balaguer_ricardo Biography by CIDOB Foundation] (in Spanish)
* [http://www.memoria.com.mx/163/bolivar.htm Memoria.com]
* {{Find a Grave|6639728}}
* [http://orlandomartinez.8m.com/ 8M.com]

{{s-start}}
{{s-off}}
{{s-bef|before=[[Héctor B. Trujillo]]}}
{{s-ttl|title=[[List of Presidents of the Dominican Republic|President of the Dominican Republic]]|years=1960&ndash;1962}}
{{s-aft|after=Civic-Military Council}}
{{s-bef|before=[[Héctor García-Godoy]]<br>(provisional)}}
{{s-ttl|title=[[List of Presidents of the Dominican Republic|President of the Dominican Republic]]|years=1966&ndash;1978}}
{{s-aft|after=[[Antonio Guzmán Fernández|Antonio Guzmán]]}}
{{s-bef|before=[[Salvador Jorge Blanco]]}}
{{s-ttl|title=[[List of Presidents of the Dominican Republic|President of the Dominican Republic]]|years=1986&ndash;1996}}
{{s-aft|after=[[Leonel Fernández Reyna|Leonel Fernández]]}}
{{s-end}}

{{Authority control}}

{{DEFAULTSORT:Balaguer, Joaquin}}
[[Category:1906 births]]
[[Category:2002 deaths]]
[[Category:People from Santiago Province (Dominican Republic)]]
[[Category:Dominican Republic people of Catalan descent]]
[[Category:Dominican Republic people of French descent]]
[[Category:Dominican Republic people of Puerto Rican descent]]
[[Category:Dominican Republic people of Spanish descent]]
[[Category:Dominican Republic anti-communists]]
[[Category:Social Christian Reformist Party politicians]]
[[Category:Presidents of the Dominican Republic]]
[[Category:Dominican Republic poets]]
[[Category:Male poets]]
[[Category:University of Paris alumni]]
[[Category:Blind politicians]]
[[Category:Politicians with physical disabilities]]
[[Category:Vice Presidents of the Dominican Republic]]
[[Category:Dominican Republic novelists]]
[[Category:Male novelists]]
[[Category:Dominican Republic male writers]]
[[Category:Ambassadors of the Dominican Republic to Colombia]]
[[Category:Ambassadors of the Dominican Republic to Ecuador]]
[[Category:Ambassadors of the Dominican Republic to Mexico]]
[[Category:20th-century poets]]
[[Category:20th-century novelists]]
[[Category:Dominican Republic expatriates in France]]