{{Infobox person
|name        = George C. Christie
|image       = 
|caption     = 
|birth_date  ={{Birth date and age|1934|03|03}}
|birth_place = [[New York, New York]]
|death_date  = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
|death_place = 
|title       = [[James Buchanan Duke|James B. Duke]] Emeritus Professor of Law
|known_for   = 
|occupation  = Legal academic and author
|nationality = American
|citizenship = [[United States]]
}}

'''George C. Christie''' (born March 3, 1934) is the [[James Buchanan Duke|James B. Duke]] Emeritus Professor of Law at [[Duke University School of Law]] in [[Durham, North Carolina|Durham]], [[North Carolina]], where he taught [[jurisprudence]] and [[tort law]] before retiring from teaching in 2013.<ref>{{cite web |url=https://law.duke.edu/news/former-students-faculty-pay-tribute-christie |title=Former students, faculty pay tribute to Christie |publisher=Duke Law |accessdate=2015-08-21}}</ref>

== Early life and education ==
Christie was born in [[New York City]] in 1934. His father, himself a lawyer, emigrated from [[Greece]] to the United States in 1920.<ref name="Athens">{{cite web |url=https://law.duke.edu/news/professor-george-christie-be-honored-university-athens |title=Professor George Christie to be Honored by University of Athens |publisher=Duke Law |accessdate=2015-08-23}}</ref> He was awarded his [[Bachelor of Arts|A.B.]] in 1955 and his [[Juris Doctor|J.D.]] in 1957, both from [[Columbia University]]. While at Columbia he was editor-in-chief<ref name="ColumLRev">{{cite web |url=http://columbialawreview.org/wp-content/uploads/2016/04/1957-CLR-MASTHEAD.pdf |title=Columbia Law Review vol. 57 (1957) Masthead |publisher=Colum. L. Rev. |accessdate=2016-09-13}}</ref> of the [[Columbia Law Review]]. In 1962 he was granted a Diploma in International Law from [[Cambridge University]]. Later, in 1966 he received [[Doctor of Juridical Science|S.J.D.]] from [[Harvard Law School|Harvard University]].<ref name="mdale">{{cite web |url=http://www.martindale.com/Search_Tools/Law_Schools/schl0228.aspx |title=Law School Profile: Duke University School of Law |publisher=martindale.com |accessdate=2015-08-21 }}</ref>

== Career ==
Christie was admitted to the bar in New York in 1957 and in the [[District of Columbia]] in 1958. He spent two years in private practice in Washington D.C. and from 1960-1961 he was a Ford Fellow at Harvard Law School. From 1961-1962 he was a [[Fulbright Program|Fulbright Scholar]] at Cambridge University. Following his time at Cambridge he became a member of the faculty at [[University of Minnesota Law School]]. In 1966 he left the University of Minnesota and returned to Washington D.C. where he served as the Assistant General Counsel for the Near East and South Asia of the [[Agency for International Development]].<ref>{{cite web |url=https://law.duke.edu/fac/christie |title=George C. Christie |publisher=Duke Law |accessdate=2015-08-21}}</ref>

He has been on the faculty of Duke University School of Law since 1967.

He received an Honorary Doctorate from the [[University of Athens]] in 2007.<ref name="Athens"/>

Christie influenced the Duke University academic governance through the so-called ''Christie Rule'', recommended by a committee he was chairing in 1972. The rule aims to guarantee that the voice of the faculty be heard prior to the Board of Trustees reaching significant decisions.<ref>{{cite web |url=https://academiccouncil.duke.edu/academic-council-history |title=Academic Council History |publisher=Duke University |accessdate=2015-08-23}}</ref>

== Selected Works ==

=== Books ===
* ''Philosopher Kings? The Adjudication of Conflicting Human Rights and Social Values'', [[Oxford University Press]] 2011, ISBN 978-0-195-34115-7
* ''The Notion of an Ideal Audience in Legal Argument'', [[Springer Science+Business Media|Kluwer Academic]] 2000, ISBN 0-7923-6283-7, translated in [[French language|French]] with introduction by [[Guy Haarscher]] as ''L'auditoire universel dans l'argumentation juridique'', Bruylant 2005, ISBN 978-2-8027-2035-5
* ''Law, Norms and Authority'', [[Duckworth Overlook|Duckworth]] 1982, ISBN 0-7156-1593-9

=== Case Books ===
* ''Advanced Torts: Cases and Materials'', [[West (publisher)|West Academic Publishing]], 1st ed. 2004, 2d ed. 2012 (with Joseph Sanders), ISBN 978-0-314-28182-1
* ''Cases and Materials on the Law of Torts'', West Academic Publishing 1983, 5th ed. 2012 (with James E. Meeks & Jonathan Cardi), ISBN 978-0-314-26694-1
* ''Jurisprudence: Text and Readings on the Philosophy of Law'', West Academic Publishing, 1973, 3d ed. 2008 (with Patrick H. Martin), ISBN 978-0-314-17073-6

== References ==
{{Reflist}}

== External links ==
* [https://law.duke.edu/fac/christie Duke University Law School personal page]
* [http://scholarship.law.duke.edu/do/search/?q=author_lname%3A%22Christie%22%20AND%20author_fname%3A%22George%22 Articles available in the Duke Law Scholarship Repository]
* [http://papers.ssrn.com/sol3/cf_dev/AbsByAuth.cfm?per_id=199288 Author page] on [[Social Science Research Network|SSRN]]

{{Authority control}}

{{DEFAULTSORT:Christie, George C.}}
<!--- Categories --->
[[Category:1934 births]]
[[Category:Living people]]
[[Category:American legal scholars]]
[[Category:Philosophers of law]]
[[Category:Harvard Law School alumni]]
[[Category:Columbia Law School alumni]]
[[Category:Duke University School of Law faculty]]
[[Category:American people of Greek descent]]