{{Infobox Radio show
| show_name = Flywheel, Shyster, and Flywheel
| image =
| imagesize =
| caption =
| other_names = ''Beagle, Shyster, and Beagle''
| format = [[Situation comedy]]
| runtime = 30 minutes
| country = United States
| language = English
| home_station = [[NBC]] [[Blue Network]]
| syndicates =
| television =
| starring = [[Groucho Marx]]<br />[[Chico Marx]]
| creator =
| writer = {{hlist|[[Nat Perrin]]|[[Arthur Sheekman]]|[[George Oppenheimer]]|[[Tom McKnight (writer)|Tom McKnight]]}}
| director = Nat Perrin<br />Arthur Sheekman
| producer =
| executive_producer =
| narrated =
| record_location = [[WABC (AM)#As WJZ|WJZ]], New York City<br />[[RKO Pictures]], Los Angeles
| first_aired = November 28, 1932
| last_aired = May 22, 1933
| num_series = 1
| num_episodes = 26
| audio_format = [[Stereophonic sound]]
| opentheme =
| endtheme =
| website =
| podcast =
}}
'''''Flywheel, Shyster, and Flywheel''''' is a [[situation comedy]] [[radio show]] starring two of the [[Marx Brothers]], [[Groucho Marx|Groucho]] and [[Chico Marx|Chico]], and written primarily by [[Nat Perrin]] and [[Arthur Sheekman]]. The series was originally broadcast in the United States on the [[NBC|National Broadcasting Company]]'s [[Blue Network]] beginning November 28, 1932, and ended May 22, 1933. Sponsored by the [[Standard Oil]] Companies of [[Esso|New Jersey]], Pennsylvania and [[Standard Oil of Louisiana|Louisiana]] and the Colonial Beacon Oil Company, it was the Monday night installment of the ''[[Five-Star Theater]]'', an [[old-time radio]] variety series that offered a different program each weeknight. Episodes were broadcast live from NBC's [[WABC (AM)#As WJZ|WJZ]] station in New York City and later from a sound stage at [[RKO Pictures]] in Los Angeles, California, before returning to WJZ for the final episodes.

The program depicts the misadventures of a small New York [[law firm]], with Groucho as attorney Waldorf T. Flywheel (a crooked lawyer) and Chico as Flywheel's assistant, Emmanuel Ravelli (a half-wit who Flywheel uses as a fall-guy). The series was originally titled ''Beagle, Shyster, and Beagle'', with Groucho's character named Waldorf T. Beagle, until a real lawyer from New York named Beagle contacted NBC and threatened to file a lawsuit unless the name was dropped. Many of the episodes' plots were partly or largely based upon [[Marx Brothers#Filmography|Marx Brothers films]].

The show garnered respectable ratings for its early evening time slot, although a second season was not produced. It was thought that, like most radio shows of the time, the episodes had not been recorded. The episodes were thought entirely lost until 1988, when 25 of the 26 scripts were rediscovered in the [[Library of Congress]] storage and republished. Adaptations of the recovered scripts were performed and broadcast in the UK, on [[BBC Radio 4]], between 1990 and 1993. In 1996, some recordings of the original show were discovered (all recorded from the final three episodes), including a complete recording of the last episode to air.

==Early development==
In 1932 [[Texaco]] introduced its "Fire Chief" gasoline to the public, so named because its [[octane rating]] was 66, higher than the United States government's requirements for fire engines.<ref>[[#refTexaco2005|Texaco, 2005]]</ref> To advertise its new premium grade fuel, Texaco approached [[vaudeville]] comic [[Ed Wynn]] to star in a radio show titled ''Fire Chief''.<ref name="Time">[[#refTime1932|''Time'', 1932]]</ref><ref>[[#refTexaco1993|Texaco, 1993]]</ref> Wynn played the [[fire chief]] in front of an audience of 700 and the show was aired live over the [[NBC Red Network]], beginning April 24, 1932.<ref>[[#refDunning1998|Dunning, 1998; p. 218]]</ref> It immediately proved popular with over two million regular listeners<ref name="Time" /> and a Co-Operative Analysis of Broadcasting (CAB) Rating of 44.8%.<ref name="OOTR">[[#refOOTR1994|The Original Old Time Radio, 1994]]</ref>

Upon seeing the success of Wynn's ''Fire Chief'', the Standard Oils in New Jersey, Louisiana and Pennsylvania, and Colonial Beacon, decided to sponsor their own radio program to promote [[Esso]] Gasoline and Essolube Motor Oil.<ref name="Barson vii">[[#refBarson1988|Barson, 1988; p. vii]]</ref> They turned to the advertising agency [[McCann Erickson]], which developed ''[[Five-Star Theater]]'', a variety series that offered a different show each night of the week.<ref name="Barson vii" /> [[Groucho Marx|Groucho]] and [[Chico Marx]], one half of the popular vaudeville and film stars the [[Marx Brothers]], were approached to appear in a comedy show. [[Harpo Marx|Harpo]] and [[Zeppo Marx|Zeppo]] were not required, as their trademark [[shtick]]s of mute and [[double act|straight man]] did not work well on radio.<ref name="Barson viii">[[#refBarson1988|Barson, 1988; p. viii]]</ref> Before this decision was officially reached, early drafts of the scripts featured [[guest appearance]]s written for both absent brothers, with Harpo being represented through honks of his horn and other trademark sound effects.<ref name="Louvish253">[[#refLouvish2000|Louvish, 2000; p. 253]]</ref>

[[Nat Perrin]] and [[Arthur Sheekman]], who had contributed to the scripts of the Marx Brothers' films ''[[Monkey Business (1931 film)|Monkey Business]]'' (1931) and ''[[Horse Feathers]]'' (1932), were enlisted to write the comedy show.<ref name="Barson viii" /> It was titled ''Beagle, Shyster, and Beagle'', and its premise involved an unethical lawyer/private detective and his bungling assistant.{{refn|"[[wikt:Beagle|Beagle]]" sometimes means investigator;<ref>[[#refDicDefBeagle|''Webster's Revised Unabridged Dictionary'': "Beagle"]]</ref> "[[wikt:Shyster|Shyster]]" an unethical, unscrupulous lawyer.<ref>[[#refDicDefShyster|''The American Heritage Dictionary of the English Language'': "Shyster"]]</ref>|group="n"}}

==Casting==
{{double image|right|Groucho_Marx.jpg|130|Chico_Marx.jpg|120|[[Groucho Marx]] as Waldorf T. Flywheel and [[Chico Marx]] as Emmanuel Ravelli}}
Groucho Marx played Waldorf T. Beagle (later renamed Waldorf T. Flywheel), and Chico played Emmanuel Ravelli, the same name as the Italian character he played in the film ''[[Animal Crackers (1930 film)|Animal Crackers]]'' (1930). Mary McCoy played secretary Miss Dimple, and it is thought that [[Broderick Crawford]] also appeared as various characters.<ref name="Louvish255">[[#refLouvish2000|Louvish, 2000; p. 255]]</ref> Groucho and Chico shared a weekly income of $6,500 for appearing in the show.<ref>[[#refCox2001|Cox, 2001; p. 203]]</ref> During the [[Great Depression in the United States|Great Depression]], this was considered a high sum for 30 minutes' work, especially since radio scripts required no memorization and only a few minutes were needed for costume, hair and makeup.<ref name="Barson vii" /> By comparison, [[Greta Garbo]]'s weekly salary from [[Metro-Goldwyn-Mayer]] during the same period was also $6,500, though this was for a 40- or 50-hour week.<ref name="Barson vii" /> Wynn was paid $5,000 a week for ''Fire Chief''.<ref name="Time" /><ref>[[#refDunning1998|Dunning, 1998; p. 219]]</ref> In contrast, almost two-thirds of American families were living on fewer than $26 a week.<ref name="OTRR">[[#refOTRR|''The Old Radio Times'', 2006]]</ref> In a classic Marxian twist, Harpo was paid a weekly salary for ''not'' appearing on the show, even though his mute character would have little to do in a radio program anyway.<ref>[[#refKanfer2000|Kanfer, 2000; p. 165]]</ref>

==Production==
''Five-Star Theater'' was broadcast from NBC's [[flagship station]], [[WPLJ#Early years|WJZ]] in [[New York City]].<ref name="Barson viii" /> Because Groucho, Chico, Perrin, and Sheekman were living and working in [[Hollywood, Los Angeles, California|Hollywood]], they had to make a three-day train journey from [[Pasadena, California|Pasadena]] each week, and then another three-day trip back. The first episode was written as they took their first train ride to New York.<ref name="Barson xiv">[[#refBarson1988|Barson, 1988; p. xiv]]</ref>

A number of ''Flywheel, Shyster, and Flywheel''{{'}}s scripts reused plots from Marx Brothers films. The plot of Episode 17 was suggested by the stolen painting plot in ''Animal Crackers'', though it was a "Beauregard" in the film, not a [[Rembrandt]].<ref name="Barson xi">[[#refBarson1988|Barson, 1988; p. xi]]</ref> The 23rd episode also reused scenes from ''Animal Crackers'', including the stolen diamond plot and Groucho's lines regarding the need for a seven-cent [[Nickel (United States coin)|nickel]]. ''Monkey Business'' influenced two skits in Episode 25, and ''[[The Cocoanuts]]'' gave Episode 19 its plot.<ref name="Barson xi" /><ref name="Allen1988">[[#refAllen1988|Allen, ''New York Times'', 1988]]</ref> Episode 26, ''The Ocean Cruise'', lifted some scenes virtually unchanged from the Marx Brothers' film ''[[Animal_Crackers_(1930_film)|Animal Crackers]]''. Vaudeville acts of the 1920s typically based their stage routines on the assumption that all audiences were [[heterosexual]];<ref name = "Capusto">[[#refCapusto2000|Capusto, 2000; p. 15]]</ref> some episodes of ''Flywheel, Shyster, and Flywheel'' accordingly included relatively low-key [[homosexual]] jokes taken from the Marx Brothers' stage act.<ref name="Capusto"/>

Despite reusing some scripts from other sources, Perrin said that he and Sheekman "had {{interp|their}} hands full turning out a script each week".<ref name="Barson xi"/> They found help from Tom McKnight and George Oppenheimer, whose names were passed along to Groucho. Perrin explained, "{{interp|Groucho}} was in the men's room during a break, and he was complaining to the guy standing next to him, 'Geez, I wish we could find another writer or two to make life easier.' Suddenly there's a voice from one of the stalls: 'I've got just the guys for you!' Having Tom and George ''did'' make life easier, although Arthur and I went over their scripts for a light polishing."<ref name="Barson xi" />

After traveling to New York to perform the first seven episodes, the four men decided to broadcast from Los Angeles instead. NBC did not have a studio on the [[West Coast of the United States|West Coast]], so for the next thirteen weeks, between January 16 and April 24, 1933, the show was transmitted from a borrowed empty [[soundstage]] at [[RKO Radio Pictures]].<ref name="Barson ix">[[#refBarson1988|Barson, 1988; p. ix]]</ref> Folding chairs were brought in for the audience of around thirty or forty people – coming from vaudeville, Groucho and Chico preferred to perform to a crowd – and were quickly cleared out at the end of each performance so that the stage would be ready for any filming the following day.<ref name="Barson xiv" /> The last four episodes of the show were performed back at WJZ in New York.<ref name="Barson ix" />

Chico was often late for rehearsals, so Perrin would have to stand in for him on the read-throughs. When Chico eventually made his appearance, Perrin remembers, "he'd be reading Ravelli's lines and Groucho would tell him to stop {{interp|and make me}} 'show him how the line should be read'. My Italian accent was better than Chico's, you see. But Chico didn't care."<ref name="Barson xiv" />

==Episodes==
{{main article|List of Flywheel, Shyster, and Flywheel episodes}}
''Flywheel, Shyster, and Flywheel'' aired Monday nights at 7:30&nbsp;p.m. on the [[NBC Blue Network]] to thirteen [[network affiliate]]s in nine [[Eastern United States|Eastern]] and [[Southern United States|Southern]] states.<ref name="Barson viii" /> Twenty-six episodes were made, which were broadcast between November 28, 1932 and May 22, 1933.<ref name="Barson viii" /><ref name="Barson ix" /> Each episode is introduced by the Blue Network announcer and features about fifteen minutes of drama and ten minutes of orchestral music between acts. The episodes end with Groucho and Chico – not in character, but as themselves – performing a 60-second skit promoting Esso and Essolube.

{{Listen
 |filename=Flywheel_episode_25_(second_half).ogg
 |title="Episode 25"
 |description=Second half of "Episode 25" (Duration: 15 minutes, 3 seconds)
 |filename2=Flywheel_episode_26.ogg
 |title2="Episode 26"
 |description2=Complete audio recording of "Episode 26". (Duration: 29 minutes, 3 seconds)
 |type=speech
 |header=Episodes of ''Flywheel, Shyster, and Flywheel''
}}
[[File:Vc116.jpg|thumb|middle|A sample page from a January 23, 1933, typed manuscript of the 1932&ndash;33 NBC radio show ''Flywheel, Shyster, and Flywheel'' that starred the [[Marx Brothers]] Groucho and Chico]]
{| class="wikitable" background:#FFFFFF;"
|-
! style="background-color: #75B2DD; color:#fff; text-align: center;"|Episode #
! style="background-color: #75B2DD; color:#fff; text-align: center; border-right:2px solid grey;"|Airdate
! style="background-color: #75B2DD; color:#fff; text-align: center;"|Episode #
! style="background-color: #75B2DD; color:#fff; text-align: center;"|Airdate
|-
| align="center"| 1
| align="center" style="border-right:2px solid grey;"| November 28, 1932<ref>[[#refBarson1988|Barson, 1988; p. 1]]</ref>
| align="center"| 14
| align="center"| January 27, 1933<ref>[[#refBarson1988|Barson, 1988; p. 165]]</ref>
|-
| align="center"| 2
| align="center" style="border-right:2px solid grey;"| December 5, 1932<ref>[[#refBarson1988|Barson, 1988; p. 15]]</ref>
| align="center"| 15
| align="center"| March 6, 1933<ref>[[#refBarson1988|Barson, 1988; p. 179]]</ref>
|-
| align="center"| 3
| align="center" style="border-right:2px solid grey;"| December 12, 1932<ref>[[#refBarson1988|Barson, 1988; p. 27]]</ref>
| align="center"| 16
| align="center"| March 13, 1933<ref>[[#refBarson1988|Barson, 1988; p. 193]]</ref>
|-
| align="center"| 4
| align="center" style="border-right:2px solid grey;"| December 19, 1932<ref>[[#refBarson1988|Barson, 1988; p. 39]]</ref>
| align="center"| 17
| align="center"| March 20, 1933<ref>[[#refBarson1988|Barson, 1988; p. 207]]</ref>
|-
| align="center"| 5
| align="center" style="border-right:2px solid grey;"| December 26, 1932<ref>[[#refBarson1988|Barson, 1988; p. 49]]</ref>
| align="center"| 18
| align="center"| March 27, 1933<ref>[[#refBarson1988|Barson, 1988; p. 217]]</ref>
|-
| align="center"| 6
| align="center" style="border-right:2px solid grey;"| January 2, 1933<ref>[[#refBarson1988|Barson, 1988; p. 63]]</ref>
| align="center"| 19
| align="center"| April 3, 1933<ref>[[#refBarson1988|Barson, 1988; p. 229]]</ref>
|-
| align="center"| 7
| align="center" style="border-right:2px solid grey;"| January 9, 1933<ref>[[#refBarson1988|Barson, 1988; p. 75]]</ref>
| align="center"| 20
| align="center"| April 10, 1933<ref>[[#refBarson1988|Barson, 1988; p. 243]]</ref>
|-
| align="center"| 8
| align="center" style="border-right:2px solid grey;"| January 16, 1933<ref>[[#refBarson1988|Barson, 1988; p. 87]]</ref>
| align="center"| 21
| align="center"| April 17, 1933<ref name="Barson257">[[#refBarson1988|Barson, 1988; p. 257]]</ref>
|-
| align="center"| 9
| align="center" style="border-right:2px solid grey;"| January 23, 1933<ref>[[#refBarson1988|Barson, 1988; p. 101]]</ref>
| align="center"| 22
| align="center"| April 24, 1933<ref name="Barson257" />
|-
| align="center"| 10
| align="center" style="border-right:2px solid grey;"| January 30, 1933<ref>[[#refBarson1988|Barson, 1988; p. 113]]</ref>
| align="center"| 23
| align="center"| May 1, 1933<ref>[[#refBarson1988|Barson, 1988; p. 271]]</ref>
|-
| align="center"| 11
| align="center" style="border-right:2px solid grey;"| February 6, 1933<ref>[[#refBarson1988|Barson, 1988; p. 127]]</ref>
| align="center"| 24
| align="center"| May 8, 1933<ref>[[#refBarson1988|Barson, 1988; p. 285]]</ref>
|-
| align="center"| 12
| align="center" style="border-right:2px solid grey;"| January 13, 1933<ref>[[#refBarson1988|Barson, 1988; p. 139]]</ref>
| align="center"| 25
| align="center"| May 15, 1933<ref>[[#refBarson1988|Barson, 1988; p. 299]]</ref>
|-
| align="center"| 13
| align="center" style="border-right:2px solid grey;"| January 20, 1933<ref>[[#refBarson1988|Barson, 1988; p. 153]]</ref>
| align="center"| 26
| align="center"| May 22, 1933<ref>[[#refBarson1988|Barson, 1988; p. 315]]</ref>
|}

==Reception==
''Flywheel, Shyster, and Flywheel'' was not a success for Standard Oil. Although the successful Marx films ''Monkey Business'' and ''Horse Feathers'' contained plots involving adultery,<ref name="Barson ix" /> ''[[Variety (magazine)|Variety]]'' did not appreciate them in the radio show:

{{quote|text=That's fine stuff for children! Chances are that if the Marxes proceed with their law office continuity along lines like this they will never be able to hold a kid listener. Firstly because parents don't want their children to hear about bad wives and divorces, and this isn't an agreeable theme to kids. Which means that if the Marxes don't look out, whatever kid following they have on the screen will be totally lost to them on the air. It's quite likely the Marxes can make themselves on the air. But they will have to use more headwork than their first effort displayed.<ref name="Barson viii"/>}}

Despite the content, Groucho's 13-year-old son [[Arthur Marx|Arthur]] found the show "extremely funny", albeit conceding that he may have been "a very easy audience".<ref>[[#MarxA1988|Marx, A, 1988; p. 190]]</ref>

Following the airing of the first episodes, a New York attorney named Morris Beagle filed a lawsuit for $300,000 alleging his name had been slandered,<ref name="Louvish255" /> and that its use was damaging his business and his health.<ref>[[#refDygert1939|Dygert, 1939; p. 144]]</ref> He also claimed that people were calling his law firm and asking, "Is this Mr. Beagle?" When he answered, "Yes", the callers would say, "How's your partner, Shyster?" and hang up the phone.<ref>[[#refGroucho1995|Groucho, 1995; p. 332]]</ref> The sponsors and studio executives panicked,<ref name="Barson xiv" /> and from episode four the title of the show was changed to "''Flywheel, Shyster, and Flywheel''", and Walter T. Beagle was renamed Waldorf T. Flywheel. It was explained in the episode that the character had divorced and reverted to his "maiden name".<ref>[[#refEp4|''Flywheel, Shyster, and Flywheel'': Episode 4, 1932]]</ref>

The CAB Rating for the show was 22.1% and placed 12th among the highest rated evening programs of the 1932–33 season.<ref name="OOTR" /><ref>[[#refWertheim1979|Wertheim, 1979; p. 123]]</ref> The CAB Rating was not disappointing – popular established shows such as ''[[The Shadow#Radio program|The Shadow]]'' and ''The Adventures of Sherlock Holmes'' did not perform as well – but it was less than half of Texaco's ''Fire Chief'', which got a 44.8% CAB Rating and was the third highest-rated program of the season.<ref name="OOTR" /><ref name="Barson x">[[#refBarson1988|Barson, 1988; p. x]]</ref> One reason for the lower ratings may be because of the time slot the show aired. In September 1932, only 40% of radio owners were listening to the radio at 7:00&nbsp;p.m., whereas 60% listened at 9:00&nbsp;p.m.<ref name="Barson x" /> The 1932–1933 season's top-rated shows, ''[[The Chase and Sanborn Hour]]'', [[Jack Pearl]]'s ''Baron Münchhausen'', and ''Fire Chief'' all aired after 9:00&nbsp;p.m.<ref name="Barson x" /> Standard Oil decided it could not compete with Texaco in the ratings and ''Five-Star Theater'' was not renewed for a second season.<ref name="Barson x" />

In his 1959 autobiography, ''Groucho and Me'', Groucho comments, "We thought we were doing pretty well as comic lawyers, but one day a few Middle East countries decided they wanted a bigger cut of the oil profits, or else. When this news broke, the price of gasoline nervously dropped two cents a gallon, and Chico and I, along with the other shows, were dropped from the network."<ref>[[#refGroucho1995|Marx, G, 1995; p. 333]]</ref> In his 1976 book, ''The Secret Word Is Groucho'', he writes, "Company sales, as a result of our show, had risen precipitously. Profits doubled in that brief time, and Esso felt guilty taking the money. So Esso dropped us after twenty-six weeks. Those were the days of guilt-edged securities, which don't exist today."<ref>[[#refSecret1976|Marx, G, 1976.]]</ref>

However, the show was later praised by other comedians of the time. In 1988, [[Steve Allen]] said, "when judged in relation to other radio comedy scripts of the early 30s, they hold up very well indeed and are, in fact, superior to the material that was produced for the Eddie Cantor, Rudy Vallee, Joe Penner school. The rapid-fire jokes {{interp|...}} run the gamut from delightful to embarrassing."<ref name="Allen1988" /> [[George Burns]] also found it "funny".<ref>[[#refBurns1989|Burns, 1989; p. 125]]</ref> Modern reviews of ''Flywheel, Shyster, and Flywheel'' have also been positive. The ''[[New York Times]]''{{'}} Herbert Mitgang described it as "one of the funniest {{interp|...}} radio shows of the early 1930s", adding that "the radio dialogue was so witty and outrageous, {{interp|an}} innocent form of original comedy – as well as serious drama".<ref name="NYT">[[#refMitgang1988|Mitgang, ''New York Times'', 1988]]</ref> Rob White of the [[British Film Institute]] said the show "glitter[s] with a thousand-and-one sockeroos."<ref>[[#refWhite2002|White, 2002; p. 265]]</ref>

==Rediscovery of the show==
The episodes of ''Flywheel, Shyster, and Flywheel'' were recorded, but for many years it was thought the recordings had not been preserved.<ref name="Louvish255" /> At the time of the broadcasts, pre-recorded shows were frowned upon by advertisers and audiences.<ref>[[#refBarson1988|Barson, 1988; p. xii]]</ref> However, in 1988, Michael Barson, who worked in the [[United States Copyright Office]] at the [[Library of Congress]] discovered that the scripts for twenty-five of the twenty-six episodes had been submitted to the Office, where they had been placed in storage.<ref>[[#refBarson1988|Barson, 1988; p. xiii]]</ref> Nobody was aware that they still existed and their copyrights had not been renewed. This meant that ''Flywheel, Shyster, and Flywheel'' had fallen into the [[public domain]].<ref name="NYT" /> The scripts were published that same year by [[Pantheon Books|Pantheon]] in a book titled ''Flywheel, Shyster, and Flywheel: The Marx Brothers' Lost Radio Show'', edited by Michael Barson and with an interview with Perrin.<ref name="NYT" /> In October 1988, ''Flywheel, Shyster and Flywheel'' scenes were broadcast for the first time since the show went off the air in 1933 when [[National Public Radio]], a [[non-profit]] media organization that provides content to [[public radio]] [[List of NPR stations|stations]] around the [[United States]], aired an 18-minute recreation of ''Flywheel, Shyster and Flywheel'' in markets such as [[Chicago, Illinois]], [[Dallas, Texas]], and [[Los Angeles, California]], using [[Washington, D.C.]]-based [[Arena Stage (Washington, D.C.)|Arena Stage]] actors to perform the Chico and Groucho lead roles from the published scripts.<ref>[[#refKogan1988|Kogan, ''Chicago Tribune'', 1988]]</ref><ref>[[#refPerkins1988|Perkins, ''Dallas Morning News'', 1988]]</ref><ref>[[#refWeinstein1988|Weinstein]], ''Los Angeles Times'', 1988</ref> Years later, three recordings of ''Flywheel, Shyster, and Flywheel'' were found, including a five-minute excerpt of Episode 24 and a fifteen-minute recording of Episode 25.<ref>[[#refMarxBros|The Marx Brothers: Streaming Audio Files]]</ref> A complete recording of Episode 26 exists and was broadcast on [[BBC Radio 4]] in 2005.<ref>[[#refBBCe26|''The Marx Brothers on Radio'', 2005]]</ref>

==BBC Radio adaptation==
{{see also|List of Flywheel, Shyster, and Flywheel (1990 radio series) episodes}}[[File:Flywheel BBC.jpg|thumb|The first series of the 1990 BBC remake was released on audio cassette in 1991.]]

In 1990 the [[British Broadcasting Corporation]]'s [[BBC Radio 4|Radio 4]] aired a version of ''Flywheel, Shyster, and Flywheel''. [[Michael Roberts (impersonator)|Michael Roberts]] and [[Frank Lazarus (performer)|Frank Lazarus]] performed the lead roles of Flywheel and Ravelli, wearing make-up and clothing similar to Groucho and Chico. The regular cast also included [[Lorelei King]] in all the female roles,  [[Graham Hoadly]]  as many of the other male characters  and guest-starred [[Spike Milligan]] and [[Dick Vosburgh]].<ref>[[#refCNN|Hume, CNN, 1990]]</ref> The scripts for the BBC series were adapted for a modern British audience by [[Mark Brisenden]] and were produced and directed by [[Dirk Maggs]].<ref name="MaggsS1">[[#refMaggsS1|Dirk Maggs Productions: Flywheel, Shyster & Flywheel Series 1]]</ref> Each episode incorporated material from two or three different original episodes, and occasionally included additional jokes from Marx Brothers' films.

Commenting on the series, Maggs has said it was his favorite among the comedies he had directed,<ref>[[#refBBCMaggs|Maggs, BBC Online, 2004]]</ref> and described how they were performed.

{{Quote|The great thing about audience shows is doing the effects live on stage. BBC Radio Light Entertainment tended to have the effects operator hidden away behind curtains so they wouldn't distract the audience! A few Light Entertainment Producers like me have reasoned over the years that the spot effects are part of the entertainment so we brought the operator out front. And in the case of ''Flywheel'' we dressed him or her up as Harpo! Michael Roberts who played Groucho came out with such good ad libs that I was always happy to cut scripted gags to keep them. One great one was when he and Frank as Flywheel and Ravelli find themselves in a pigsty – the rest of the cast pushed in to make pig voices – and Mike ad libbed, "Imagine – two nice Jewish boys surrounded by ham" – it brought the house down.|Dirk Maggs<ref name="MaggsS1" />}}

Six episodes were performed and recorded at the [[Paris Theatre]] and aired weekly between June 2 and July 7, 1990.<ref name="MaggsS1" /> The success of the first series led to another two being commissioned. The second series aired from May 11 to June 15, 1991,<ref>[[#refMaggsS2|Dirk Maggs Productions: Flywheel, Shyster & Flywheel Series 2]]</ref> and the third from July 11 to August 15, 1992.<ref>[[#refMaggsS3|Dirk Maggs Productions: Flywheel, Shyster & Flywheel Series 3]]</ref> The first series was made available by [[BBC Enterprises]] on a two-cassette release in 1991, but the second and third series were not.<ref>[[#refBBCaudio|Flywheel, Shyster and Flywheel Attorneys At Law: Marx Brothers' Lost Radio Scripts, 1991]]</ref>

==Notes==
{{reflist|group="n"|}}

==Citations==
{{reflist|25em}}

==References==
{{refbegin|25em}}
* {{cite news
 |first=Steve
 |last=Allen
 |title=Christmas books; Nobody Told a Bad Joke Better
 |url=https://query.nytimes.com/gst/fullpage.html?res=940DE5DA133FF937A35751C1A96E948260
 |work=[[New York Times]]
 |date=December 4, 1988
 |accessdate=December 24, 2008
|ref=refAllen1988}}
* {{cite book
 |editor=Barson, Michael
 |title=Flywheel, Shyster, and Flywheel: The Marx Brothers' Lost Radio Show
 |year=1988
 |publisher=[[Pantheon Books]]
 |isbn=0-679-72036-7
|ref=refBarson1988}}
* {{cite web
 |url=http://dictionary.reference.com/browse/beagle
 |title=Beagle
 |accessdate=December 9, 2008
 |year=2008
 |work=Webster's Revised Unabridged Dictionary
 |publisher=Dictionary.com
|ref=refDicDefBeagle}}
* {{cite book
 |last=Burns
 |first=George
 |authorlink=George Burns
 |title=All My Best Friends
 |date=November 6, 1989
 |publisher=[[G. P. Putnam's Sons]]
 |isbn=0-399-13483-2
|ref=refBurns1989}}
* {{cite book
 |last=Capusto
 |first=Stephen
 |title=Alternate Channels: The Uncensored Story of Gay and Lesbian Images on Radio and Television
 |year=2000
 |publisher=[[Ballantine Books]]
 |isbn=0-345-41243-5
|ref=refCapusto2000}}
* {{cite book
 |last=Cox
 |first=Jim
 |authorlink=Jim Cox (radio)
 |title=The Great Radio Audience Participation Shows: Seventeen Programs from the 1940s and 1950s
 |year=2001
 |publisher=[[McFarland & Company]]
 |location=[[Jefferson, North Carolina]]
 |isbn=0-7864-1071-X
|ref=refCox2001}}
* {{cite book
 |last=Dunning
 |first=John
 |authorlink=John Dunning (writer)
 |title=On the Air: The Encyclopedia of Old-Time Radio
 |year=1998
 |publisher=[[Oxford University Press]]
 |isbn=0-19-507678-8
|ref=refDunning1998}}
* {{cite book
 |last=Dygert
 |first=Warren Benson
 |title=Radio As an Advertising Medium
 |year=1939
 |publisher=[[McGraw-Hill]]
|ref=refDygert1939}}
* {{cite episode
 |title=Episode 4
 |series=Flywheel, Shyster, and Flywheel
 |credits=[[Nat Perrin]], [[Arthur Sheekman]] (writers); [[Groucho Marx]], [[Chico Marx]] (starring)
 |network=[[NBC Blue Network]]
 |station=[[WPLJ#Early years|WJZ]]
 |location=[[New York City]]
 |airdate=December 19, 1932
 |transcript=MRS DIMPLE: "Law offices of Flywheel, Shyster, and Flywheel ... Yes I know that used to be the name of the firm, but the boss got a divorce and changed his name back to Flywheel."
|ref=refEp4}}
* {{cite news
 |title=Gag Tycoon
 |url=http://www.time.com/time/magazine/article/0,9171,744522,00.html?iid=chix-del
 |work=[[Time (magazine)|Time]]
 |date=October 3, 1932
 |accessdate=December 7, 2008
|ref=refTime1932}}
* {{cite news
 |first=Peter
 |last=Hume
 |title=Long Lost Marx Brothers Radio Scripts
 |url=https://www.youtube.com/watch?v=jePGu4oTGV0
 |format=FLV
 |publisher=[[CNN]] / [[YouTube]]
 |date=May 1, 1990
 |accessdate=December 26, 2008
|ref=refCNN}}
* {{cite news
 |author=Jennings, Robert
 |author2=Boenig, Wayne
 |title=How Groucho and his Brothers Left Their Marx on Network Radio, Pt. 2
 |work=The Old Radio Times
 |publisher=The Old-Time Radio Researchers
 |pages=9–12
 |date=October 2006|ref=refOTRR}}
* {{cite book
 |last=Kanfer
 |first=Stefan
 |title=Groucho: The Life and Times of Julius Henry Marx
 |year=2000
 |publisher=[[Random House]]
 |location=[[New York City]]
 |isbn=0-375-40218-7
|ref=refKanfer2000}}
* {{cite news
 |last=Kogan
 |first=Rick
 |authorlink=Rick Kogan
 |title=Joan Eposito has Stars in Her Eyes<!-- Note: the article is about Joan Esposito, but there is a typo in the headline -->
 |department=Tempo
 |newspaper=[[Chicago Tribune]]
 |date=October 7, 1988
 |page=4
 |accessdate=November 28, 2010
 |url=http://articles.chicagotribune.com/1988-10-07/features/8802060003_1_space-program-radio-ratings-spring-ratings-period/2|ref=refKogan1988}}
* {{cite book
 |last=Louvish
 |first=Simon
 |authorlink=Simon Louvish
 |title=Monkey Business: The Lives and Legends of the Marx Brothers
 |year=2000
 |publisher=[[Macmillan Publishers]]
 |location=[[New York City]]
 |isbn=0-312-25292-7
|ref=refLouvish2000}}
* {{cite video
 |people=[[Dirk Maggs|Maggs, Dirk]] (producer, director)
 |date=May 7, 1990
 |title=Flywheel, Shyster and Flywheel Attorneys At Law: Marx Brothers' Lost Radio Scripts
 |medium=[[Compact Cassette|Cassette]]
 |publisher=[[BBC Enterprises]]
|ref=#refBBCaudio}}
* {{cite book
 |last=Marx
 |first=Arthur
 |authorlink=Arthur Marx
 |title=My Life with Groucho: A Son's Eye View
 |year=1988
 |publisher=[[Barricade Books]]
 |isbn=0-942637-45-3
|ref=refMarxA1988}}
* {{cite book
 |last=Marx
 |first=Groucho
 |authorlink=Groucho Marx
 |title=Groucho and Me
 |year=1995
 |publisher=Da Capo Press
 |isbn=0-306-80666-5
|ref=refGroucho1995}}
* {{cite book
 |last=Marx
 |first=Groucho
 |authorlink=Groucho Marx
 |title=The Secret Word Is Groucho
 |year=1976
 |publisher=[[G. P. Putnam's Sons]]
 |isbn=0-399-11690-7
|ref=refSecret1976}}
* {{cite news
 |first=Herbert
 |last=Mitgang
 |title=Meet Flywheel & Ravelli, Hucksters
 |url=https://query.nytimes.com/gst/fullpage.html?res=940DE2D9163EF932A1575BC0A96E948260
 |work=[[New York Times]]
 |date=August 21, 1988
 |accessdate=December 10, 2008
|ref=refMitgang1988}}
* {{cite news
 |last=Perkins
 |first=Ken
 |title=Electric Jungle Will Be Jumping
 |department=Today
 |newspaper=[[Dallas Morning News]]
 |date=October 8, 1988
 |page=7C
 |accessdate=November 28, 2010
 |url=http://articles.chicagotribune.com/1988-10-07/features/8802060003_1_space-program-radio-ratings-spring-ratings-period/2|ref=refPerkins1988}}
* {{cite book
 |last=Smith
 |first=Andrew T.
 |title=Marx and Re-Marx: Creating and Recreating the Lost Marx Brothers Radio Series
 |year=2010
 |publisher=[[BearManor Media]]
 |isbn=1-593936-09-5
|ref=refMarxA1988}}
* {{cite web
 |url=http://dictionary.reference.com/browse/shyster
 |title=Shyster
 |year=2004
 |accessdate=December 9, 2008
 |work=The American Heritage Dictionary of the English Language, Fourth Edition
 |publisher=Dictionary.com
 |format=Houghton Mifflin Company
|ref=refDicDefShyster}}
* {{cite web
 |url=http://www.texaco.com/heritage/texaco_heritage.html
 |title=Texaco Heritage
 |date=December 2, 2005
 |accessdate=December 7, 2008
 |publisher=[[Texaco]]
 |format=Flash. (Select Products", "Timeline", "1928 – 1941")
|ref=refTexaco2005}}
* {{cite web
 |url=http://www.texaco.com/sitelets/history/history_ad.html
 |title=Texaco's History
 |date=December 4, 1993
 |accessdate=December 7, 2008
 |publisher=[[Texaco]]
 |archiveurl=https://web.archive.org/web/20030717121015/http://www.texaco.com/sitelets/history/history_ad.html
 |archivedate=July 17, 2003
|ref=refTexaco1993}}
* {{cite web
 |url=http://www.old-time.com/ratings/by%20season/1930s/19321933eve.html
 |title=The Highest Rated Evening Programs: 1932–1933 Season
 |year=1994
 |accessdate=December 7, 2008
 |publisher=The Original Old Time Radio
|ref=refOOTR1994}}
* {{cite AV media
 |title=The Marx Brothers on Radio
 |url=http://www.bbc.co.uk/radio4/comedy/pip/qud1w
 |publisher=[[BBC Radio 4]]
 |date=December 27, 2005
|ref=#refBBCe26}}
* {{cite web
 |url=http://www.marx-brothers.org/whyaduck/sounds/streaming.htm
 |title=The Marx Brothers: Streaming Audio Files
 |accessdate=December 25, 2008
 |publisher=The Marx Brothers
|ref=refMarxBros}}
* {{cite web
 |url=http://www.bbc.co.uk/radio4/hitchhikers/webchat.shtml
 |title=Transcript of live webchat with Dirk Maggs
 |accessdate=December 26, 2008
 |date=July 2004
 |publisher=[[BBC Online]]
|ref=refBBCMaggs}}
* {{cite news
 |last=Weinstein
 |first=Steve
 |title=Radio
 |department=Calendar
 |newspaper=[[Los Angeles Times]]
 |date=October 10, 1988
 |page=2
 |accessdate=November 28, 2010
 |url=http://articles.latimes.com/1988-10-10/entertainment/ca-2715_1_radio-dial|ref=refWeinstein1988}}
* {{cite book
 |last=Wertheim
 |first=Arthur Frank
 |title=Radio Comedy
 |year=1979
 |publisher=[[Oxford University Press]]
 |isbn=0-19-502481-8
|ref=refWertheim1979}}
* {{cite book
 |last=White
 |first=Rob
 |title=British Film Institute Film Classics: The Best of International Cinema 1916–1981
 |date=November 22, 2002
 |publisher=[[Routledge]]
 |isbn=1-57958-328-8
|ref=refWhite2002}}
* {{cite web
 |url=http://www.dswilliams.co.uk/dirk%20maggs/Flywheel%20series%201%20dirk%20maggs.htm
 |title=Flywheel, Shyster & Flywheel Series 1
 |accessdate=December 25, 2008
 |last=Williams
 |first=David
 |publisher=Dirk Maggs Productions
 |ref=refMaggsS1}}
* {{cite web
 |url=http://www.dswilliams.co.uk/dirk%20maggs/Flywheel%20series%202%20dirk%20maggs.htm
 |title=Flywheel, Shyster & Flywheel Series 2
 |accessdate=December 25, 2008
 |last=Williams
 |first=David
 |publisher=Dirk Maggs Productions
|ref=refMaggsS2}}
* {{cite web
 |url=http://www.dswilliams.co.uk/dirk%20maggs/Flywheel%20series%203%20dirk%20maggs.htm
 |title=Flywheel, Shyster & Flywheel Series 3
 |accessdate=December 25, 2008
 |last=Williams
 |first=David
 |publisher=Dirk Maggs Productions
|ref=refMaggsS3}}
{{refend}}

==External links==
{{Portal|Comedy|Radio}}
* [http://www.bbc.co.uk/programmes/b00dzvzc/episodes/guide ''Flywheel, Shyster and Flywheel'' - Episode guide - BBC Radio 4 Extra]
* BBC adaptation director's website details for [http://www.dirkmaggs.dswilliams.co.uk/dirk%20maggs/Flywheel%20series%201%20dirk%20maggs.htm series 1], [http://www.dirkmaggs.dswilliams.co.uk/Flywheel%20series%202%20dirk%20maggs.htm series 2] and [http://www.dirkmaggs.dswilliams.co.uk/dirk%20maggs/Flywheel%20series%203%20dirk%20maggs.htm series 3].
* [http://www.britishcomedy.org.uk/comedy/flywheel.htm britishcomedy.org.uk details]

{{Marx Brothers}}

{{featured article}}

{{DEFAULTSORT:Flywheel, Shyster, And Flywheel}}
[[Category:Fictional law firms]]
[[Category:American comedy radio programs]]
[[Category:1930s American radio programs]]
[[Category:1932 radio programme debuts]]
[[Category:1933 radio programme endings]]
[[Category:Marx Brothers]]
[[Category:NBC Blue Network radio programs]]