{{Refimprove|date=December 2016}}
[[File:Absorbing panels, Downtown Recording.jpg|thumb|Sound baffles on the wall of a recording studio]]
A '''sound baffle''' is a construction or device which reduces the strength (level) <!-- not the "intensity" --> of [[airborne sound]]. Sound baffles are a fundamental tool of [[noise mitigation]], the practice of minimizing [[noise pollution]] or [[reverberation]]. An important type of sound baffle is the [[noise barrier]] constructed along [[highways]] to reduce sound levels at properties in the vicinity. Sound baffles are also applied to walls and ceilings in building interiors to [[absorption (acoustics)|absorb]] sound energy and thus lessen reverberation.<ref>{{Cite web|url=http://soundcontroltech.com/baffles-banners-clouds/what-is-an-acoustic-baffle/|title=What is an Acoustic Baffle?|website=soundcontroltech.com|access-date=2016-12-16}}</ref>

==Highway noise barriers==
{{Main|Noise barrier}}
The technology for accurate prediction of the effects of noise barrier design using a [[computer model]] to analyze [[roadway noise]] has been available since the early 1970s. The earliest published scientific design of a noise barrier may have occurred in [[Santa Clara County]], [[California]] in 1970 for a section of the [[Foothill Expressway]] in [[Los Altos, California]]. The county used a computer model to predict the effects of sound propagation from [[roadways]], with variables consisting of [[vehicle]] [[speed]], ratio of trucks to automobiles, road surface type, roadway [[geometric]]s, micro-[[meteorology]] and the design of proposed [[soundwalls]].

==Interior sound baffle design==
{{Main|Architectural acoustics}}
Since the early 1900s, scientists have been aware of the utility of certain types of interior coatings or baffles to improve the [[acoustics]] of [[concert halls]], [[theaters]], [[conference rooms]] and other spaces where sound quality is important. By the mid-1950s, [[Bolt, Beranek and Newman]] and a few other U.S. research organizations were developing technology to address sound quality's design challenges. This design field draws on several disciplines including [[acoustical science]], computer modeling, [[architecture]] and [[materials science]]. Sound baffles are also used in [[speaker cabinets]] to absorb [[energy]] from the pressure created by the speakers, thus reducing cabinet [[resonance]].
 
In 1973, Pearl P. Randolph, a [[school bus]] driver in [[Virginia]], won a new school bus in a national contest held by [[Wayne Corporation]] for the suggestion that sound baffles be installed in the ceiling of [[school buses]]. In 1981, they were first made mandatory by the [[state of California]].

==Vehicle exhaust sound baffles==
{{Main|Muffler}}
Baffles are also found in the exhaust pipes of vehicles, particularly motorcycles.

==See also==
*[[Noise pollution]]
*[[Noise health effects]]{{Commonscat|Sound baffles}}

==References==
{{Reflist}}

{{DEFAULTSORT:Sound Baffle}}
[[Category:Acoustics]]
[[Category:Ceilings]]
[[Category:Noise pollution]]
[[Category:Noise reduction]]
[[Category:Sound]]