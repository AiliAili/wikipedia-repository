{{for|the American bank robber and member of the [[John Dillinger|John Dillinger gang]]|James "Oklahoma Jack" Clark}}
{{Infobox criminal
| name   = Jim Clark
| image_name     = 
| image_size     = 
| image_caption  = 
| birth_date  = {{Birth date|1902|2|7}}
| birth_place = [[Mountainburg, Arkansas]], [[United States]]
| death_date  = {{Death date and age|1974|6|9|1902|2|7}} 
| death_place = [[Oklahoma]]
| cause          = 
| alias          =
| charge         =
| conviction     = [[Armed robbery]]
| conviction_penalty        = 99 years imprisonment
| conviction_status         = Paroled in 1969
| occupation     = Bank robber
| spouse         = 
| parents        =
| children       =
}}
 

'''Jim Clark''' (February 26, 1902-June 9, 1974) was an American bank robber and Depression-era outlaw. A longtime career criminal in Oklahoma during the 1920s, Clark was associated with [[Wilbur Underhill]], [[Harvey Bailey]] and [[Robert Brady (criminal)|Robert "Big Bob" Brady]] and remained a [[public enemy]] in the state of [[Kansas]] until his capture and imprisonment in 1934.<ref name="Newton">Newton, Michael. ''The Encyclopedia of Robberies, Heists, and Capers''. New York: Facts On File Inc., 2002. (pg. 58-59) ISBN 0-8160-4488-0</ref>

==Early life and criminal career==
Jim Clark was born in [[Mountainburg, Arkansas]] on February 26, 1902. In 1923, the 21-year-old Clark was arrested in Oklahoma and sent to the state reformatory in [[Granite, Oklahoma|Granite]]. He was eventually released from the reformatory and drifted to Texas where he found work in the oil fields. By 1927, he had begun smuggling bootleg liquor across the border from [[Ciudad Juárez|Juarez, Mexico]] and was jailed for 30 days after a botched robbery that same year. Returning to Oklahoma, he was again arrested for burglary and sentenced to five years imprisonment on March 31, 1928.<ref name="Newton"/>

Clark was released after serving less than a year of his sentence and quickly returned to his criminal career. On March 14, 1932, he was sentenced to two years in prison for stealing a car in [[Sequoyah County, Oklahoma]]. Six weeks later, Clark officially became a fugitive when he walked off from the [[prison]] camp in [[Colby]]{{dn|date=January 2014}} on April 25. His escape lasted only briefly however when he was arrested with Frank Sawyer and Ed Davis riding in a stolen car near [[Rich Hill, Missouri]] on June 17. Their arrest occurring hours after the robbery of $47,000 from a bank in [[Fort Scott, Kansas]], in actuality having been committed by the [[Barker Gang]], Clark and his two accomplices were wrongly convicted of the robbery and imprisoned at the state penitentiary at Lansing.<ref name="Newton"/>

==Time with the Bailey-Underhill gang==
On May 30, 1933, Clark was one of 11 convicts who escaped with Underhill from Lansing using pistols smuggled inside the prison from friends. Authorities later believed [[Frank Nash|Frank "Jelly" Nash]] was responsible for having orchestrated the mass escape.<ref name="Newton"/> Clark and another escapee, Clifford Dopson, became separated from the others shortly after their escape. They managed to [[hitch hike|hitch a ride]] on [[U.S. Route 66|Route 66]] with a young Joliet couple the next morning and, drawing their guns, took them hostage. They forced the couple, B.K. Blair and Alice Braithwaite, to drive until reaching [[Neosho, Missouri]] whereupon they released them and took over their car. During the ride, one of the convicts claimed they ''"had to kill a [[police officer|bull]] last night"'' which may have referred to the murder of [[World War I]] [[war hero]] police officer Otto Durkee in [[Chetopa, Kansas]].<ref>Breuer, William B. ''J. Edgar Hoover and his G-Men''. Westport, Connecticut: Greenwood Publishing Group, 1995. (pg. 52) ISBN 0-275-94990-7</ref> <ref>[http://www.odmp.org/officer/4398-night-policeman-otto-laduke-durkee ODMP memorial]</ref>

Clark spent the next several weeks on the run with [[Wilbur Underhill]], [[Harvey Bailey]] and [[Robert Brady (criminal)|Robert "Big Bob" Brady]], briefly hiding out in [[Cookson Hills, Oklahoma]], before hitting their first bank together. On July 3, Clark and the others robbed $11,000 from a bank in [[Clinton, Oklahoma]] and then, seven weeks later, raided another bank in [[Kingfisher, Oklahoma|Kingfisher]] on August 7. Plans to rob another bank in [[Brainerd, Minnesota]] fell through when Bailey was arrested by federal agents three days later while visiting [[Machine Gun Kelly|George "Machine Gun" Kelly]]'s ranch in [[Paradise, Texas]]. Underhill also left around this time leaving Clark and Brady to continue on their own.<ref name="Newton"/> 

Their last robbery together turned into a disaster when they attempted to hold up a bank in [[Frederick, Oklahoma]] on October 6, 1933. They escaped with only $5,000, missing $80,000 in the vault and the teller's cages, and took three hostages with them as they made their getaway. After switching cars in [[Indiahoma]], they raced across Texas heading for New Mexico. When police discovered the first getaway car, police found a map charting their escape route and telephoned ahead to New Mexico authorities with a description of the fugitives. Clark and Brady were stopped near [[Tucumcari, New Mexico]]<ref>King, Jeffrey. ''The Life and Death of Pretty Boy Floyd''. Kent, Ohio: Kent State University Press, 1999. (pg. 134) ISBN 0-87338-650-7</ref><ref>Bishop, Mark and Omus Sours. ''The Face of Death''. Victoria, British Columbia: Trafford Publishing, 2003. (pg. 191) ISBN 1-55395-730-X</ref> and, although Clark gave his name as "F.N. Atwood", authorities believed Clark was Wilbur Underhill before fingerprints established his identity. The charges against the two for their latest crime spree were waived as both men were returned to serve the rest of their sentence in Lansing.<ref name="Newton"/>

==Partnership with Frank Delmar==
Clark and Brady were placed into [[solitary confinement]] upon their arrival in Lansing. Restrictions on their movements were gradually relaxed over the next three months and, on January 19, 1934, he and Brady took part in yet another prison break when they and five other inmates escaped from a kitchen work detail. Clark and Brady split up soon after they got on the outside, with Clark joining Frank Delmar in kidnapping schoolteacher Louis Dresser. The two drove his car to Oklahoma where they released their hostage once they met with Clark's girlfriend Goldie Johnson who was waited for them with a car. Dresser was apparently so upset by the incident that he mistakenly identified Johnson as [[Bonnie Parker]].<ref name="Newton"/> 

He and Demar soon began robbing banks in Kansas and Oklahoma. In one of their first robberies together, he and Delmar raided a bank in [[Goodland, Kansas]] for $2,000 on February 9. Clark was shot in both feet during their escape by a police officer shooting from underneath a nearby car. He spent the next three months recovering from his wounds and, on May 9, robbed a bank in [[Wetumka, Oklahoma]] for $500. On May 31, he returned with Delmar to the same bank in Kingfisher that  he had robbed with Underhill, Bailey and Brady in August 1933.<ref name="Newton"/> 

After robbing another bank in [[Crescent, Oklahoma]] on June 20, he revisited the same Clinton bank he had robbed with his old partners the previous year and stole $13,000. He and Goldie Johnson were also suspected of robbing a bank in [[Oxford, Kansas]], but both denied this and no charges were ever filed.<ref name="Newton"/>

==Capture and imprisonment==
By the summer of 1934, Clark had been declared a "[[public enemy]]" and Kansas governor personally offered a $200 reward for his capture as did the state banking association. A special police unit was established by Kansas authorities who finally tracked him down in Tulsa and arrested him on August 1. Clark was later tried and convicted on federal bank robbery charges and given a 99-year prison sentence. On January 14, 1935, Clark was sent to Leavenworth where he spent two years before his transfer to Alcatraz in 1937. Repeated disciplinary infractions forced his return to Leavenworth a year later where he reportedly assumed control of illegal gambling and loan shark for the next decade.<ref name="Newton"/> 

His activities were eventually discovered by prison officials and Clark was sent back to Alcatraz in January 1948. He remained there for over twelve years until his eventual return to Leavenworth in 1960 and then to [[Seagoville, Texas]] nine years later.<ref name="Newton"/>

==Release and later years==
After thirty-five years in prison, Clark was released on parole on December 9, 1969. The 67-year-old Clark returned to Oklahoma to marry the widow of his brother and where he would spend the rest of his life. He worked as a ranch hand for several years and, when old age prevented him continuing, Clark managed a commercial parking lot for a local bank until his death on June 9, 1974.<ref name="Newton"/>

==References==
{{Reflist}}

==Further reading==
*[[Courtney Ryley Cooper|Cooper, Courtney Ryley]]. ''Ten Thousand Public Enemies''. Boston: Little, Brown & Co., 1935.
*Johnson, Lester Douglas. ''The Devil's Front Porch''. Lawrence: University Press of Kansas, 1970.
*Kirchner, Larry R. ''Robbing Banks: An American History, 1831-1999''. Rockville Centre, New York: Sarpedon, 2000. ISBN 1-885119-64-X
*Milner, E. R. ''The Lives and Times of Bonnie and Clyde''. Carbondale: Southern Illinois University Press, 1996. ISBN 0-8093-1977-2

{{Authority control}}
{{DEFAULTSORT:Clark, Jim}}
[[Category:1902 births]]
[[Category:1974 deaths]]
[[Category:People from Crawford County, Arkansas]]
[[Category:American bank robbers]]
[[Category:Depression-era gangsters]]