<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= 9-092
 |image=Zündapp Z 9-092 iim Technikmuseum Hugo Junkers Dessau 2010-08-06 Detail 01.jpg
 |caption=Rear view of the 9-092 engine on display at the [[Technikmuseum Dessau]] 
}}{{Infobox Aircraft Engine
 |type=[[Air-cooled engine|Air-cooled]] [[Inline engine (aviation)#Inverted engine|inverted]] [[inline-four engine]]
 |manufacturer=[[Zündapp]]
 |first run={{Avyear|1938}}
 |major applications=[[Gotha Go 150]], [[Siebel Si 202]]B
 |number built = c.200 
 |program cost = 
 |unit cost = 
 |developed from = [[Zündapp Z 9-090]]
 |developed into = 
 |variants with their own articles =
}}
|}

The '''Zündapp 9–092''' or '''Z 92''' was a German four-cylinder, air-cooled, inline [[Aircraft engine|aero engine]] made by [[Zündapp]] and used in light aircraft of the late-1930s.<ref>[http://www.flightglobal.com/pdfarchive/view/1938/1938%20-%201629.html ''Flight'' - 'The Belgrade Show' - 9 June 1938, p. 561.] Retrieved: 18 December 2012</ref>

==Design and development==
The engine was developed from the smaller [[Zündapp 9-090]]. This inverted engine featured dual gear-driven [[camshaft]]s with the valve rocker cover acting as the oil tank. It featured a single [[Robert Bosch GmbH|Bosch]] [[magneto ignition]] system. A total of approximately 200 engines were produced.

==Applications==
*[[Brunswick LF-1 Zaunkönig]]
*[[Bücker Bü 180]]
*[[Fieseler Fi 253]]
*[[Gotha Go 150]]
*[[Klemm Kl 105]]
*[[Möller Stürmer]]
*[[Siebel Si 202]]

==Engines on display==
*Preserved examples of the 9-092 engine are on public display at the [[Deutsches Museum]], [[Munich]] and [[Technikmuseum Dessau]].

==Specifications==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully-formatted line with <li> -->
|ref=
|type=Four-cylinder air-cooled [[straight engine|inverted inline]] piston aircraft engine
|bore= 85 mm (3.35 in)
|stroke= 88 mm (3.46 in)
|displacement=2 L (122 cu in)
|length= 800 mm
|diameter=
|width= 350 mm
|height= 
|weight=60 kg (132 lb
|valvetrain=Overhead valve
|supercharger=
|turbocharger=
|fuelsystem= Downdraught carburettor
|fueltype=
|oilsystem=
|coolingsystem=Air-cooled
|power=37 kw (50 hp) at 2,300 rpm
|specpower=
|compression= 6.2:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=0.62 kW/kg (0.38 hp/lb)
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=
<!-- designs which were developed into or from this aircraft: -->
|related=
<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]

<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[ERCO I-L 116]]
*[[de Havilland Gipsy Minor]]
*[[Walter Mikron]]

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

== References ==
{{Commons category|Zündapp Z 9-092}}
{{reflist}}
;Bibliography
{{refbegin}}
* Kyrill von Gersdorff, Helmut Schubert, Stefan Ebert: ''Flugmotoren und Strahltriebwerke'', Bernard & Graefe Verlag München, ISBN 978-3-7637-6128-9
{{refend}}

{{DEFAULTSORT:Zundapp 9-092}}
[[Category:Air-cooled aircraft piston engines]]
[[Category:Aircraft piston engines 1930–1939]]
[[Category:Inverted aircraft piston engines]]