{{featured article}}
{{italic title}}
[[File:Famous fantastic mysteries 193909-10 v1 n1.jpg|thumb|right|The cover of the first issue, dated September/October 1939]]

'''''Famous Fantastic Mysteries''''' was an American [[science fiction]] and [[fantasy literature|fantasy]] [[pulp magazine]] published from 1939 to 1953. The editor was [[Mary Gnaedinger]]. It was launched by the [[Frank A. Munsey Company|Munsey Company]] as a way to reprint the many science fiction and fantasy stories which had appeared over the preceding decades in the Munsey magazines, such as ''[[Argosy (magazine)|Argosy]]''. The first issue was dated September/October 1939; the magazine was immediately successful, and less than a year later a companion magazine, ''[[Fantastic Novels]]'', was launched.

Frequently reprinted authors included [[George Allan England]], [[A. Merritt]], and [[Austin Hall (writer)|Austin Hall]]; the artwork was also a major reason for the success of the magazine, with artists such as [[Virgil Finlay]] and [[Lawrence Stevens (artist)|Lawrence Stevens]] contributing some of their best work.  In late 1942 [[Popular Publications]] acquired the title from Munsey, and ''Famous Fantastic Mysteries'' stopped reprinting short stories from the earlier magazines. It continued to reprint longer works, including titles by [[G. K. Chesterton]], [[H. G. Wells]], and [[H. Rider Haggard]]. Original short fiction also began to appear, including [[Arthur C. Clarke]]'s "Guardian Angel", which would later form the first section of his novel ''[[Childhood's End]]''. In 1951 the publishers experimented briefly with a large digest format, but returned quickly to the original pulp layout. The magazine ceased publication in 1953, almost at the end of the pulp era.

==Publication history==
By the early decades of the 20th century, science fiction (sf) stories were frequently seen in popular magazines.<ref name=TTM_16-23>Ashley, ''Time Machines'', pp.&nbsp;16–23.</ref> The [[Frank A. Munsey Company|Munsey Company]], a major [[pulp magazine]] publisher, printed a great deal of science fiction in these years,<ref name="TTM_16-23"/> but it was not until 1926 that ''[[Amazing Stories]]'', the first pulp magazine specializing in science fiction appeared.<ref name=SFE_SFM>Malcolm Edwards & Peter Nicholls, "SF Magazines", in Clute & Nicholls, ''Encyclopedia of Science Fiction'', pp.&nbsp;1066–1068.</ref>  Munsey continued to print sf in ''[[Argosy (magazine)|Argosy]]'' during the 1930s, including stories such as [[Murray Leinster]]'s ''The War of the Purple Gas'' and [[Arthur Leo Zagat]]'s "Tomorrow", though they owned no magazines that specialized in science fiction.<ref name=SFFWFM_FFM>Thomas D. Clareson, "Famous Fantastic Mysteries", in Tymn & Ashley, ''Science Fiction, Fantasy and Weird Fiction Magazines'', pp.&nbsp;211–216.</ref>  By the end of the 1930s science fiction was a growing market,<ref name=SFE_SFM/> with several new sf magazines launched in 1939.<ref name=TTM_A2>Ashley, ''Time Machines'', pp.&nbsp;237–255.</ref>  That year Munsey took advantage of science fiction's growing popularity by launching ''Famous Fantastic Mysteries'' as a vehicle for reprinting the most popular fantasy and sf stories from the Munsey magazines.<ref name=TTM_150-1>Ashley, ''Time Machines'', pp.&nbsp;150–151.</ref>

The first issue was dated September/October 1939, and was edited by Mary Gnaedinger.  The magazine immediately became successful and went to a monthly schedule starting in November 1939.  Demand for reprints of old favorites was so strong that Munsey decided to launch an additional magazine, ''[[Fantastic Novels]]'', in July 1940.<ref name=TTM_150-1/> The two magazines were placed on alternating bimonthly schedules,<ref name=SFE_SFM/> but when ''Fantastic Novels'' ceased publication in early 1941 ''Famous Fantastic Mysteries'' remained bimonthly until June 1942.<ref name=Tuck_FFM>"Famous Fantastic Mysteries", in Tuck, ''Encyclopedia of Science Fiction and Fantasy, Vol. 3'', pp.&nbsp;555–556.</ref>  Munsey sold ''Famous Fantastic Mysteries'' to [[Popular Publications]], a major pulp publisher, at the end of 1942; it appears to have been a sudden decision, since the editorial in the December 1942 issue discusses a planned February issue that never materialized, and mentions forthcoming reprints that did not appear.  The first issue from Popular appeared in March 1943, and only two more issues appeared that year; the September 1943 issue marked the beginning of a regular quarterly schedule.  It returned to a bimonthly schedule in 1946 which it maintained with only slight deviations until the end of its run.<ref name=SFFWFM_FFM/>

In 1949, [[Street & Smith]], one of the longest established and most respected publishers, shut down all of their pulp magazines: the pulp era was drawing to a close.  Popular Publications was the biggest pulp publisher, which helped their titles last a little longer, but ''Famous Fantastic Mysteries'' finally ceased publication in 1953, only a couple of years before the last of the pulps ceased publication.<ref name=TTM_220-225>Ashley, ''Time Machines'', pp.&nbsp;220–225.</ref>

==Contents and reception==
[[File:Famous Fantastic Mysteries August 1942 cover.jpg|left|thumb|Cover of the August 1942 issue, by Virgil Finlay]]Munsey's plan for the magazines was laid out in a note that appeared in the first four issues: "This magazine is the answer to thousands of requests we have received over a period of years, demanding a second look at famous fantasies which, since their original publication, have become accepted classics.  Our choice has been dictated by ''your'' requests and our firm belief that these are the aces of imaginative fiction."<ref name=SFFWFM_FFM/>  The first issue included Ray Cummings' "The Girl in the Golden Atom" and A. Merritt's "The Moon Pool", both popular stories by well-known authors.<ref name=TTM_150-1/>  Merritt's sequel, "The Conquest of the Moon Pool", began serialization in the next issue, with illustrations by [[Virgil Finlay]].  Finlay did many illustrations for ''Famous Fantastic Mysteries'' over its lifetime, and became one of its most popular artists.  [[Frank R. Paul]] began illustrating for the magazine with the third issue; he was not as capable an artist as Finlay but was very popular with the readers.<ref name=TTM_150-1/>  The first five covers were simply tables of contents, but with the sixth issue, dated March 1940, pictorial covers began, with Finlay the artist for that first cover.<ref name=SFFWFM_FFM/>   Three early covers in 1940 were painted by Paul, but thereafter almost every cover was painted by either Finlay, [[Lawrence Stevens (artist)|Lawrence Stevens]], or his son, [[Peter Stevens (artist)|Peter Stevens]], including every single issue from February 1941 through April 1950.<ref name=Day_169-70>Day, ''Index to the Science-Fiction Magazines'', pp.&nbsp;169–170.</ref><ref name=BDSFFA_LSS>Robert Weinberg, "Lawrence Stern Stevens", in Weinberg, ''Biographical Dictionary'', pp.&nbsp;260–262.</ref><ref name=BDSFFA_PS>Robert Weinberg, "Peter Stevens", in Weinberg, ''Biographical Dictionary'', pp.&nbsp;262–263</ref><ref name=T_386>Ashley, ''Transformations'', p.&nbsp;386.</ref>  The high quality of the artwork helped make the magazine one of the most popular of its day,<ref name=EncSF_FFM/> and sf historian Thomas Clareson has suggested that it was Finlay's work in ''Famous Fantastic Mysteries'' and ''Fantastic Novels'' that made his reputation.<ref name=SFFWFM_FFM/>

The decision to launch ''Fantastic Novels'' was taken partly because there were a great many book-length works that readers wanted to see reprinted.<ref name=TTM_150-1/>  Gnaedinger commented that "Everyone seems to have realized that although [the] set-up of five to seven stories with two serials running, was highly satisfactory, that the long list of novels would have to be speeded up somehow".<ref name=SFFWFM_FFM/>  When ''Fantastic Novels'' was launched, ''Famous Fantastic Mysteries'' was partway through serialization of ''The Blind Spot'', by [[Austin Hall (writer)|Austin Hall]] and [[Homer Eon Flint]], with the third episode appearing in the May/June 1940 issue.  Rather than complete the serialization, Gnaedinger decided to print the novel in its entirety in the first issue of ''Fantastic Novels'', ensuring that readers of ''Famous Fantastic Mysteries'' would also acquire the new magazine.<ref name=TTM_150-1/>  After ''Fantastic Novels'' ceased publication in 1941, ''Famous Fantastic Mysteries'' changed its policy, and began publishing a complete novel in every issue, rather than several stories and one or two serials running concurrently.  Usually there were also short stories, but occasionally a particularly long novel would appear alone in the issue: this happened, for example, with the February 1942 issue, which contained [[Francis Stevens]]' ''The Citadel of Fear'', and no other fiction.<ref name=SFFWFM_FFM/>

When Munsey sold ''Famous Fantastic Mysteries'' to Popular, the editorial policy changed again, to exclude reprints of short fiction that had previously appeared in magazine form.  Book length fiction continued to be reprinted, as did some shorter works that had appeared only in books, such as [[William Hope Hodgson]]'s "The Derelict", and [[Robert W. Chambers]]' "The Mask", both of which appeared in the December 1943 issue.  The reprinted novels included [[G. K. Chesterton]]'s ''[[The Man Who Was Thursday]]'', [[H. G. Wells]]' ''[[The Island of Dr. Moreau]]'', [[H. Rider Haggard]]'s ''The Ancient Allan'', and works by [[Algernon Blackwood]], [[Lord Dunsany]], and [[Arthur Machen]].<ref name=SFFWFM_FFM/>  Some of the reprinted material was abridged,<ref name="EncSF_FFM">{{cite web | url = http://sf-encyclopedia.com/entry/famous_fantastic_mysteries | title = Culture: Famous Fantastic Mysteries: SFE: Science Fiction Encyclopedia | accessdate= March 17, 2013|publisher=Gollancz}}</ref>  but despite this, ''Famous Fantastic Mysteries'' did an important service to its readers by making works available that had been long out of print, and which in some cases had only been previously published in the U.K., making their appearance in the magazine the first chance many subscribers would have had to read them.<ref name=SFFWFM_FFM/>

Some original material also appeared after Popular acquired the magazine.  Contributors who published original stories in ''Famous Fantastic Mysteries'' included [[Henry Kuttner]], [[Ray Bradbury]], and [[C. L. Moore]].<ref name=EncFant_FFM>Mike Ashley, "Famous Fantastic Mysteries", in Clute & Grant, ''Encyclopedia of Fantasy'', p.&nbsp;334.</ref>  [[Arthur C. Clarke]]'s story "Guardian Angel" appeared in the April 1950 issue; it was later turned into the first section of his novel ''[[Childhood's End]]''.<ref name=ISOW_187>Knight, ''In Search of Wonder'', p.&nbsp;187.</ref><ref name=issues>See the individual issues. For convenience, an online index is available at {{Cite web| url = http://www.isfdb.org/wiki/index.php/Series:Famous_Fantastic_Mysteries | title = Series: Famous Fantastic Mysteries&nbsp;— ISFDB | accessdate=17 March 2013 |publisher=Al von Ruff (Publisher)}}</ref>

==Bibliographic details==
{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-right: 2em; text-align: center; float: left"
! !!Jan !! Feb !!Mar !!Apr !!May !!Jun !!Jul !!Aug !!Sep !!Oct !!Nov !!Dec
|-
!1939
|| || || || || || || || ||bgcolor=#ccffff|1/1 || ||bgcolor=#ccffff|1/2 ||bgcolor=#ccffff|1/3
|-
!1940
|bgcolor=#ccffff|1/4 ||bgcolor=#ccffff|1/5 ||bgcolor=#ccffff|1/6 ||bgcolor=#ccffff|2/1 ||bgcolor=#ccffff|2/2 || || ||bgcolor=#ccffff|2/3 || ||bgcolor=#ccffff|2/4 || ||bgcolor=#ccffff|2/5
|-
!1941
|| ||bgcolor=#ccffff|2/6 || ||bgcolor=#ccffff|3/1 || ||bgcolor=#ccffff|3/2 || ||bgcolor=#ccffff|3/3 || ||bgcolor=#ccffff|3/4 || ||bgcolor=#ccffff|3/5
|-
!1942
|| ||bgcolor=#ccffff|3/6 || ||bgcolor=#ccffff|4/1 || ||bgcolor=#ccffff|4/2 ||bgcolor=#ccffff|4/3 ||bgcolor=#ccffff|4/4 ||bgcolor=#ccffff|4/5 ||bgcolor=#ccffff|4/6 ||bgcolor=#ccffff|5/1 ||bgcolor=#ccffff|5/2
|-
!1943
|| || ||bgcolor=#ccffff|5/3 || || || || || ||bgcolor=#ccffff|5/4 || || ||bgcolor=#ccffff|5/5
|-
!1944
|| || ||bgcolor=#ccffff|5/6 || || ||bgcolor=#ccffff|6/1 || || ||bgcolor=#ccffff|6/2 || || ||bgcolor=#ccffff|6/3
|-
!1945
|| || ||bgcolor=#ccffff|6/4 || || ||bgcolor=#ccffff|6/5 || || ||bgcolor=#ccffff|6/6 || || ||bgcolor=#ccffff|7/1
|-
!1946
|| ||bgcolor=#ccffff|7/2 || ||bgcolor=#ccffff|7/3 || ||bgcolor=#ccffff|7/4 || ||bgcolor=#ccffff|7/5 || ||bgcolor=#ccffff|8/1 || ||bgcolor=#ccffff|8/2
|-
!1947
|| ||bgcolor=#ccffff|8/3 || ||bgcolor=#ccffff|8/4 || ||bgcolor=#ccffff|8/5 || ||bgcolor=#ccffff|8/6 || ||bgcolor=#ccffff|9/1 || ||bgcolor=#ccffff|9/2
|-
!1948
|| ||bgcolor=#ccffff|9/3 || ||bgcolor=#ccffff|9/4 || ||bgcolor=#ccffff|9/5 || ||bgcolor=#ccffff|9/6 || ||bgcolor=#ccffff|10/1 || ||bgcolor=#ccffff|10/2
|-
!1949
|| ||bgcolor=#ccffff|10/3 || ||bgcolor=#ccffff|10/4 || ||bgcolor=#ccffff|10/5 || ||bgcolor=#ccffff|10/6 || ||bgcolor=#ccffff|11/1 || ||bgcolor=#ccffff|11/2
|-
!1950
|| ||bgcolor=#ccffff|11/3 || ||bgcolor=#ccffff|11/4 || ||bgcolor=#ccffff|11/5 || ||bgcolor=#ccffff|11/6 || ||bgcolor=#ccffff|12/1 || ||
|-
!1951
|bgcolor=#ccffff|12/2 || ||bgcolor=#ccffff|12/3 || ||bgcolor=#ccffff|12/4 || ||bgcolor=#ccffff|12/5 || || ||bgcolor=#ccffff|12/6 || ||bgcolor=#ccffff|13/1
|-
!1952
|| ||bgcolor=#ccffff|13/2 || ||bgcolor=#ccffff|13/3 || ||bgcolor=#ccffff|13/4 || ||bgcolor=#ccffff|13/5 || ||bgcolor=#ccffff|13/6 || ||bgcolor=#ccffff|14/1
|-
!1953
|| ||bgcolor=#ccffff|14/2 || ||bgcolor=#ccffff|14/3 || ||bgcolor=#ccffff|14/4 || || || || || || 
|-
|colspan="13" style="font-size: 8pt; text-align:left"|Issues of ''Famous Fantastic Mysteries'', identifying volume and issue numbers.<br/>Mary Gnaedinger was editor throughout.
|}Mary Gnaedinger was the editor of ''Famous Fantastic Mysteries'' for all 81 issues.  The magazine was launched as a bimonthly in September 1939, and was converted to monthly from the second issue, in November 1939.  The May 1940 issue was followed by August 1940, which began a bimonthly sequence that lasted until June 1942, which began another monthly sequence that ran through the end of 1942.  The next issue, March 1943, was followed by a September issue that inaugurated a quarterly sequence that ran until December 1945, which began another bimonthly run.  This lasted until the final issue in June 1953 with only two irregularities: October 1950 was followed by January 1951, and July 1951 was followed by October 1951.  ''Famous Fantastic Mysteries'' was published by the Munsey Corporation until the end of 1942, and by Popular Publications, thereafter.<ref name=Tuck_FFM/>  The magazine was initially 128 pages long.  This was cut to 112 pages with the October 1940 issue, and then returned to 128 pages for the June 1941 issue.  From June 1942 to March 1944 the page count was 144; it was cut to 132 in June 1944 and again to 112 in January 1951, where it remained until the end of the run.  The price was 15 cents throughout, except for the period from October 1940 to April 1941 during which it was 10 cents.<ref name=SFFWFM_FFM/>  ''Famous Fantastic Mysteries'' began as a pulp, and remained in that format throughout its run except for a brief experiment in 1951 in which it was reduced to large digest size.<ref name="EncFant_FFM"/>

A Canadian reprint edition, with identical contents and dates, began in February 1948, from All Fiction Field, Inc.; in October 1951 the publisher became Popular Publications, Toronto, but this was just a name change rather than a change of ownership.  The final Canadian issue was dated August 1952; these issues were half an inch longer than the U.S. versions.<ref name=Tuck_FFM/>  In addition, the Canadian edition of ''[[Super Science Stories]]'', which had initially reprinted from its U.S. namesake and from the U.S. edition of ''[[Astonishing Stories]]'', began to reprint almost entirely from ''Famous Fantastic Mysteries'' beginning with the August 1944 Canadian issue.<ref name=SFFWFM_FFM/><ref name=TTM_217>Ashley, ''Time Machines'', p.&nbsp;217.</ref>  As a nod to the change in source material, the title of the Canadian edition was changed to ''Super Science and Fantastic Stories'' starting with the December 1944 issue.<ref name=TTM_217/>  A Mexican magazine, ''Los Cuentos Fantasticos'', which published 44 issues between 1948 and 1953, reprinted stories from both ''Famous Fantastic Mysteries'' and ''Astounding Science Fiction'', mostly (though not entirely) without obtaining permission first.<ref name=T_304>Ashley, ''Transformations'', p.&nbsp;304.</ref>

An anthology, ''Famous Fantastic Mysteries: 30 Great Tales of Fantasy and Horror from the Classic Pulp Magazines Famous Fantastic Mysteries and Fantastic Novels'', appeared in 1991, edited by Stefan R. Dziemianowicz, Robert E. Weinberg and [[Martin H. Greenberg]], and drawing almost all of its contents from ''Famous Fantastic Mysteries''.<ref name=ESF_MHG>John Clute, "Martin H. Greenberg", in Clute & Nicholls, ''Encyclopedia of Science Fiction'', pp.&nbsp;522–524.</ref>

==Footnotes==
{{reflist|25em}}

==References==
*{{cite book | first=Mike | last=Ashley | title=The Time Machines:The Story of the Science-Fiction Pulp Magazines from the beginning to 1950| publisher=Liverpool University Press| location=Liverpool| year=2000 | isbn= 0-85323-865-0}}
*{{cite book | first=Mike | last=Ashley | title=Transformations:The Story of the Science-Fiction Magazines from 1950 to 1970| publisher=Liverpool University Press| location=Liverpool| year=2005 | isbn= 0-85323-779-4}}
*{{cite book|last=Clute|first= John|author2=Grant, John| title= The Encyclopedia of Fantasy| year=1997| publisher= St. Martin's Press, Inc| location= New York| isbn= 0-312-15897-1}}
*{{cite book|last=Clute|first= John|author2=Nicholls, Peter| title= The Encyclopedia of Science Fiction| year=1993| publisher= St. Martin's Press, Inc| location= New York| isbn= 0-312-09618-6}}
*{{cite book| first=Donald B. | last=Day | title=Index to the Science-Fiction Magazines| publisher=Perri Press | location=Portland OR | year=1952 }}
*{{cite book |first=Damon |last=Knight |title=In Search of Wonder |year=1974 |edition=reprint of 1967 2nd |origyear=1956 |location=Chicago|publisher=Advent: Publishers, Inc |isbn=0-911682-15-5 }}
*{{cite book| first=Donald H. | last=Tuck | title=The Encyclopedia of Science Fiction and Fantasy: Volume 3| publisher=Advent: Publishers, Inc | location=Chicago | year=1982 | isbn= 0-911682-26-0}}
*{{cite book | first=Marshall B. | last=Tymn |author2=Ashley, Mike| title=Science Fiction, Fantasy and Weird Fiction Magazines|publisher=Greenwood Press | location=Westport, CT| year=1985 | isbn= 0-313-21221-X}}
* {{cite book |first=Robert |last=Weinberg |authorlink=Robert Weinberg (author) |title=A Biographical Dictionary of Science Fiction and Fantasy Artists |publisher=Greenwood Press |location=Westport, CT|year=1985 |isbn=0-313-21221-X}}
==External links==
{{Commons category| Famous Fantastic Mysteries}}
* [http://www.isfdb.org/wiki/index.php/Series:Famous_Fantastic_Mysteries Famous Fantastic Mysteries] on the [[Internet Speculative Fiction Database]]
{{ScienceFictionPulpMagazines}}

[[Category:Defunct science fiction magazines of the United States]]
[[Category:Fantasy fiction magazines]]
[[Category:Pulp magazines]]
[[Category:Magazines disestablished in 1953]]
[[Category:Magazines established in 1939]]
[[Category:Science fiction magazines established in the 1930s]]
[[Category:American bi-monthly magazines]]
[[Category:American monthly magazines]]