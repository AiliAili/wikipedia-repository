{{Infobox casino
| name                = Cal Neva Resort & Casino
| logo                =
| logo_size           =
| logo_alt            =
| logo_caption        =
| image               =Cal Neva Lodge.jpg
| image_size          = 
| image_alt           = 
| image_caption       = The Cal-Neva Lodge & Casino in 1962
| pushpin_map          = Nevada
| map_alt             = 
| map_caption         = Cal Neva Lodge & Casino
| map_size            =
| latitude            = 
| longitude           =
| map_dot_label       =
| relief              = 
| etymology           = 
| casino_type       = Land-based
| address             = 2 Stateline Road, [[Crystal Bay, Nevada]], United States
| location = 
| theme=
| coordinates         = {{coord|39|13|29|N|120|0|20|W|display=inline}}
| altitude            =
| date_opened         = {{Start date and age|1926}}
| owner               = Criswell-Radovan, LLC
| architect           = 
| rooms               = 220
|space_gaming={{convert|116000|sqft|m2|abbr=on}}
|shows=
|attractions=
|notable_restaurants=
| parking             = 
| website               = {{URL|calnevaresort.com/}}
| references          = 
| footnotes           =
}}
'''Cal Neva Resort & Casino''', previously known as the '''Calneva Resort''', '''Cal-Neva Lodge''',<ref>{{cite news
|title=Blame for Fires is Blamed Upon Deer Hunters
|url=https://www.newspapers.com/clip/6820020/
|accessdate=29 September 2016
|work=Reno Gazette
|publisher=[[Newspapers.com]] 
|date=15 August 1928
|page=8
|quote=Cal-Neva Lodge   
}} {{open access}}</ref><ref>{{cite news
|title=SF Wife Cites Gaming Checks Of  Clara Bow : Cal-Neva Manager Made Too Conceited by Incident, Divorce Plea Charges
|url=https://www.newspapers.com/clip/6819897/
|accessdate=29 September 2016
|work=Oakland Tribune
|publisher=[[Newspapers.com]]
|date=30 October 1930
|page=21
|quote=Cal-Neva Lodge        
}} {{open access}}</ref> is a [[resort]] and [[casino]] straddling the border between [[Nevada]] and [[California]] on the shores of [[Lake Tahoe]]. The original building was constructed in 1926, and became famous when the national media picked up a story about actress [[Clara Bow]] cancelling [[cheque|checks]] she owed to the Cal Neva worth $13,000 in 1930.<ref>{{cite news
|title=Former Gambling Czar 'Bones' Remmer Dead At 65
|url=https://news.google.com/newspapers?nid=1817&dat=19630613&id=3focAAAAIBAJ&sjid=9poEAAAAIBAJ&pg=7367,1916256
|accessdate=29 September 2016
|work=The Tuscaloosa News
|publisher=Google News
|date=Jun 13, 1963
|page=7
}} {{open access}}</ref> After a fire, the building burnt down in 1937 and was rebuilt over the course of 30 days. In 1960, entertainer [[Frank Sinatra]] purchased the Resort alongside [[Dean Martin]] and Chicago mobster [[Sam Giancana]].

Under Sinatra, the Celebrity Room was added alongside a [[helipad]] and it opened year round. His ownership gradually increased over the following two years until he owned 50 percent outright. But Giancana's attendance at the property first provoked a rift between Sinatra and share holder [[Hank Sanicola]], and later resulted in Sinatra's gambling license being suspended by the [[Nevada Gaming Control Board]]. He initially leased the property to [[Jack L. Warner]] and later in 1968 to a group of investors. It passed through the hands of a series of investment groups until 1976 when it was bought by [[Kirk Kerkorian]]. The property is owned by Criswell-Radovan, LLC, which closed the resort on September 5, 2013, for renovations and declared bankruptcy in 2016.<ref>http://www.tahoedailytribune.com/news/owners-of-lake-tahoes-famed-cal-neva-resort-file-for-bankruptcy/</ref>

==History==
===Early period===
The Cal-Neva Lodge was built in 1926 by real estate developer Robert P. Sherman.<ref>{{cite news|last1=Carrerowbonanza|first1=Jack|title=Sinatra did it his way at the Cal-Neva|url=http://www.tahoedailytribune.com/article/20040709/NEWS/107090007|accessdate=November 15, 2015|work=Tahoe Daily Tribune|date=July 7, 2004}}</ref> In 1929, [[Canadian Army]] Colonel H. H. Betts went missing from his room at the Cal-Neva; his body was found over a year later some five miles away from the hotel.<ref name=bones>{{cite news|title=Human Bones Are Identified|url=https://www.newspapers.com/clip/3626261//|accessdate=November 14, 2015|work=The Ogden Standard-Examiner|publisher=[[Newspapers.com]]|date=September 11, 1930|page=4}} {{open access}}</ref> The lodge was first made nationally famous later that year when actress [[Clara Bow]] cancelled [[Cheque|checks]] owed to the casino worth $13,000;<ref>{{cite news|title=Cal-Neva Inn Keep Must Pay Alimony|url=https://www.newspapers.com/clip/3626292//|accessdate=November 14, 2015|work=Oakland Tribune|publisher=[[Newspapers.com]]|date=December 14, 1930|page=14}} {{open access}}</ref> she claimed that she had believed that the chips she was using while playing [[blackjack]] were worth 50 [[Cent (currency)|cents]] when they were actually valued at [[United States dollar|$]]100.<ref>{{cite news
|title=Clara Bow 'Alibi' Hit by Will Rogers
|url=https://www.newspapers.com/clip/
|accessdate=29 September 2016
|quote=Cal-Neva
|work=Santa Cruz Evening News
|location=Santa Cruz, California
|publisher=[[Newspapers.com]]
|date= September 26, 1930
|page=5
}} {{open access}}</ref><ref>{{cite news|title=Clara Bow Admits Gambling When Will Rogers 'Tells All'|url=https://news.google.com/newspapers?nid=1144&dat=19300925&id=hr4cAAAAIBAJ&sjid=N44EAAAAIBAJ&pg=4749,1040363&hl=en|accessdate=December 11, 2015|work=The Pittsburgh Press|date=September 25, 1930|page=1}}</ref>

A group of investors bought the Cal-Neva in 1935, and recruited William "Bones" Remmer<ref>{{cite news
|title=Ex-gambling Czar Remmer Dies At 65
|url=https://news.google.com/newspapers?nid=888&dat=19630614&id=cpNPAAAAIBAJ&sjid=dFIDAAAAIBAJ&pg=2802,1568751
|accessdate=29 September 2016
|work=St. Petersburg Times
|publisher=Google News
|date=June 14, 1963
|page=5
}} {{open access}}</ref> to act as its president and the [[pit manager]] of the casino.<ref name=moe89>[[#woodsmoe1996|Woods Moe (1996)]]: p. 89</ref> That year a 13-year-old [[Judy Garland]] performed at Cal Neva for the first time.<ref>{{cite web|url=http://www.sfgate.com/travel/article/Historic-Cal-Neva-resort-shuts-down-3193954.php|title=Historic Cal Neva resort shuts down|publisher=''San Francisco Chronicle''|date=April 1, 2010|accessdate=November 16, 2015}}</ref>
The original building burnt down in 1937 causing $200,000 worth of damage;<ref name=burns>{{cite news|title=Cal-Neva Resort Burns, Damage is Over $200,000|url=https://www.newspapers.com/clip/3626378//|accessdate=November 14, 2015|work=The Fresno Bee|publisher=[[Newspapers.com]]|date=May 17, 1937|page=3}} {{open access}}</ref> the incident which was thought to have been [[arson]] but no charges were ever brought.<ref name=lank198/> The reconstruction effort took 30 days, with 500 men working on the project.<ref>[[#larson2008|Larson (2008)]]: p. 96</ref> State legislation was changed shortly afterwards to allow gambling, and following the purchase by developer Norman Blitz it became one of Nevada's earliest legalized casinos and rumors abounded that there was an intention by the owners of the lodge to put a gambling boat on the lake.<ref name=lank198>[[#lankford2010|Lankford (2010)]]: p. 198</ref><ref name=tahoeclosed/><ref>{{cite news|title=Nevada Plans to Emulate Old Monte Carlo|url=https://www.newspapers.com/clip/3626328//|accessdate=November 14, 2015|work=Warren Times Mirror|publisher=[[Newspapers.com]]|date=March 21, 1931|page=1}} {{open access}}</ref>

Towards the end of the [[Second World War]], the lodge was purchased from Remmer by Sanford Adler and a group of associates for $700,000;<ref>[[#woodsmoe1996|Woods Moe (1996)]]: p. 31</ref><ref>{{cite news|title=$700,000 Deal|url=https://www.newspapers.com/clip/3626516//|accessdate=November 14, 2015|work=Long Beach Independent|publisher=[[Newspapers.com]]|date=June 5, 1948|page=6}} {{open access}}</ref> Adler renamed the property to "Cal Neva" in order to match Adler's Cal Neva hotel in [[Reno, Nevada]].<ref>[[#woodsmoe2008|Woods Moe (2008)]]: p. 207</ref> However, this formatting of the name had been in common use since before the fire.<ref name=bones/> 
In 1946, [[Xavier Cugat]] was paid a reported $22,000 for a two-week stint at Cal Neva Lodge—this was reportedly the first nationally famous band to perform there.<ref>{{cite book|title=Billboard|url=https://books.google.com/books?id=-hkEAAAAMBAJ&pg=PT18|date=3 August 1946|publisher=Nielsen Business Media, Inc.|page=18|issn=0006-2510}}</ref>

Ownership was transferred once again on March 21, 1955, when a group led by Bert "Wingy" Grober purchased the lodge for $1 million.<ref name=moe89/><ref>{{cite news|title=Trio Seeks to Buy Lake Tahoe Casino|url=https://www.newspapers.com/clip/3626567//|accessdate=November 14, 2015|work=The San Bernardino County Sun|publisher=[[Newspapers.com]]|date=December 19, 1954|page=42}} {{open access}}</ref> During this period, the hotel was frequented by members of the [[Kennedy family]] including [[John F. Kennedy|John F.]] and his brother [[Robert Kennedy|Robert]].<ref name=moe89/> Author Scott Lankford claims that John F. used the lodge to carry on an "endless series of extramarital affairs with wealthy divorcees and Tahoe's notoriously ubiquitous prostitutes".<ref name="lank198"/> The lodge served as accommodation during the [[1960 Winter Olympics]], held at nearby [[Squaw Valley Ski Resort]].<ref name=skiing200>{{cite book|title=Skiing|url=https://books.google.com/books?id=HFJvPTKttHEC&pg=PA200|date=October 1970|page=200|issn=0037-6264}}</ref> It has remained popular with skiers over the decades since.<ref name=skiing200/><ref name="Skiing1989">{{cite book|title=Skiing|url=https://books.google.com/books?id=3QyZyX1qsEQC&pg=PA103|date=December 1989|page=103|issn=0037-6264}}</ref>

===Ownership by Frank Sinatra===
[[File:Frank Sinatra laughing.jpg|thumb|right|upright|Frank Sinatra became the majority stakeholder in the Cal Neva Lodge in 1960.]]
[[Frank Sinatra]] first visited Cal Neva in 1951; his trip made the national press as he overdosed on sleeping pills and this was reported to the local sheriff.<ref>[[#kelley1986|Kelley (1986)]]: p. 168</ref> He continued to attend the lodge and Grober would talk up his appearances to other guests such as when on one occasion, Sinatra played a [[jam session]] with [[big band]] leader [[Harry James]] and actress and singer [[Betty Grable]].<ref name=wings /> Sinatra publicly bought the resort in 1960 through his company, Park Lake Enterprises.<ref>[[#freedland1997|Freedland (1997)]]: p. 292</ref><ref>[[#clarke1998|Clarke (1998)]]: p. 225</ref> Initially, he owned 25 percent of the property with [[Hank Sanicola]] and [[Paul D'Amato|Paul "Skinny" D'Amato]] who each held 13 percent.<ref name=freed270/>  Other smaller shareholders included [[Dean Martin]]. Chicago mobster [[Sam Giancana]] was said to be a silent partner in the business;<ref name=freed270>[[#freedland1997|Freedland (1997)]]: p. 270</ref> D'Amato acted as Giancana's man.<ref>[[#kelley1986|Kelley (1986)]]: p. 312</ref>  Sinatra gradually expanded his ownership of the casino; by 1962 he owned more than 50 percent share, with Sanicola holding 33 percent and [[Sanford Waterman]] owning the remaining shares.<ref name=kelly313>[[#kelley1986|Kelley (1986)]]: p. 313</ref>

During this period, the hotel was frequented by members of the Kennedy family including John F. Kennedy and his brother Robert F Kennedy and father Joseph P. Kennedy.<sup>[[Cal Neva Lodge & Casino#cite note-moe89-11|[11]]]</sup> Author Scott Lankford claims that The Kennedy Clan used the lodge to carry on an "endless series of extramarital affairs with wealthy divorcees and Tahoe's notoriously ubiquitous prostitutes".<sup>[[Cal Neva Lodge & Casino#cite note-lank198-14|[14]]]</sup>

One of Lankford’s most fascinating stories of The Cal Neva Lodge involve the prohibition era tunnels that traveled underground, below the suites of the financial elite and movie stars, such as Marilyn Monroe. He stated that Monroe, who kept a permanent bungalow (#4), next to Franks (#5), and between the one maintained by the Kennedy clan (#3), was a constant visitor to Bungalow #3, at all hours of the day and night, and known for visiting JFK most often, was also known to visit Father Joe and brother Bobby.

Sinatra was known to keep thousands of Dollars both in cash and in United States Mint bags holding Brilliant Uncirculated Morgan silver dollars, that he  kept in a private underground Vault within the tunnel, connected to his Bungalow #5.

The shareholders decided to open the property year-round; it had only previously opened for the summer season.<ref name=clarke226>[[#clarke1998|Clarke (1998)]]: p. 226</ref> Sinatra built the Celebrity Room theater and installed a helicopter pad on the roof. He re-utilized [[Prohibition in the United States|prohibition]]-era smuggling tunnels beneath the property to allow mob members to move around the property without being seen by the public.<ref name=tahoeclosed/>

The [[FBI]] already had the lodge under investigation at the time due to the connection to [[Joseph P. Kennedy, Sr.]], who was staying there at the time that Sinatra's deal was finalized. There were concerns that Kennedy was involved in the arrangement of a casino for use by the [[American mafia]].<ref>[[#kuntzkuntz2000|Kuntz & Kuntz (2000)]]: p. 141</ref>  The FBI suspected that the expansion was made using funds borrowed from [[Jimmy Hoffa]].<ref>[[#kuntzkuntz2000|Kuntz & Kuntz (2000)]]: p. 180</ref> Following a request by [[Robert Kennedy]], who had concerns over the press coverage of his and his brother's relationships with [[Marilyn Monroe]], Sinatra made accommodation available for her for a weekend prior to her death on August 4, 1962.<ref>[http://themarilynmonroecollection.com/marilyn-monroe-pucci-blouse/ Marilyn Monroe's Personal Pucci Blouse - The Marilyn Monroe Collection LLC]</ref> During this period she was not allowed to leave and only Giancana was allowed to visit her. Even her former husband, [[Joe DiMaggio]], was turned away.<ref>[[#freedland1997|Freedland (1997)]]: p. 299</ref> She attempted suicide through an overdose, but contacted the reception desk and was rushed to hospital where she had her stomach pumped.<ref name=kelly314/> The cabin, known as Monroe's, and another, known as Sinatra's, are still part of the guest accommodation.<ref name=tahoeclosed/> Also in that year, a federal investigation took place into a prostitution ring being run from the foyer of the Cal Neva.<ref name=kelly314>[[#kelley1986|Kelley (1986)]]: p. 314</ref>
[[File:Cal Neva Lodge 2.jpg|thumb|left|Postcard of the lodge, c. early 1960s.]]
The Sinatra period saw extravagant parties and visits by celebrities such as [[Judy Garland]], [[Liza Minnelli]], [[Kim Novak]], [[Shirley MacLaine]], [[Sammy Davis Jr.]], [[Tony Curtis]], [[Janet Leigh]], [[Lucille Ball]], [[Desi Arnaz]] and [[Richard Crenna]]. However, his mood swings would sometimes determine how he responded to patrons. Journalist [[Herb Caen]] reported that he could be dismissive and insulting to those who annoyed him.<ref name=kelly313/> Giancana was banned from the state of Nevada, but Sinatra continued to allow him to stay at the lodge which resulted in Sanicola seeking to remove himself as a shareholder. Although Sinatra sought to explain that Giancana was only visiting his girlfriend, [[Phyllis McGuire]], the disagreement resulted in the end of Sinatra and Sanicola's friendship.<ref>[[#freedland1997|Freedland (1997)]]: p. 298</ref> After Giancana was spotted on the premises, Sinatra had his gambling license removed by the [[Nevada Gaming Control Board]].<ref name=tahoeclosed/>{{refn|Sinatra did not gain back his gambling license until 1981, following a hearing of the Nevada Gaming Commission and the intervention of [[Ronald Reagan]], who acted as a character witness.<ref>{{cite news|title=Gaming License in Nevada Goes to Sinatra with Praise|url=https://www.nytimes.com/1981/02/20/us/gaming-license-in-nevada-goes-to-sinatra-with-praise.html|accessdate=November 14, 2015|work=New York Times|date=February 20, 1981}}</ref>|group="n"}} Following this, Sinatra decided he wanted to get out of entertainment property ownership as he was being heavily criticized in the national press and pursued by law enforcement over illegal activities at the casino.<ref>[[#kelley1986|Kelley (1986)]]: p. 323</ref> The Cal Neva Lodge was leased to [[Jack L. Warner]] in a deal which also saw a majority stake in [[Reprise Records]] sold to [[Warner Bros. Records]] and Sinatra gaining a one-third ownership in the new company.<ref>[[#clarke1998|Clarke (1998)]]: p. 228</ref>

===Later period===
[[File:Cal Neva Resort (2040890245).jpg|thumb|right|The Cal Neva Resort in 2007]]
After leasing it to Warner for four years, Sinatra sought to sell the property to [[Howard Hughes]] in 1967 as part of the renewal of his contract at the [[Sands Hotel and Casino]].<ref>[[#kelley1986|Kelley (1986)]]: p. 371</ref> Following a fallout between Hughes and Sinatra, the entertainer instead signed a contract with [[Caesars Palace]]. This included a requirement for Sinatra's stake in Cal Neva to be purchased by his new employer for $2 million,<ref>{{cite news|title=Reports Say Sinatra Punched in Nevada|url=https://www.newspapers.com/clip/3627065//|accessdate=November 14, 2015|work=Delaware County Daily Times|publisher=[[Newspapers.com]]|date=September 12, 1967|page=1}} {{open access}}</ref><ref>{{cite news|title=Sinatra Sale|url=https://www.newspapers.com/clip/3627177//|accessdate=November 14, 2015|work=The Independent|publisher=[[Newspapers.com]]|date=November 14, 1967|page=2}} {{open access}}</ref> but instead the lodge passed into the hands of the same owners as Reno's Club Cal-Neva in 1968 for $1.4 million.<ref name=julyset/><ref name=former>{{cite news|title=Sinatra's Former Casino Reopens|url=https://www.newspapers.com/clip/3627293//|accessdate=November 14, 2015|work=The Fresno Bee|publisher=[[Newspapers.com]]|date=May 29, 1969|page=9}} {{open access}}</ref>

The new owners oversaw the construction of a ten-storey expansion to the property. This added a further 200 rooms to the lodge, for which permission was given although the Nevada State Park Advisory Committee opposed the move.<ref name=lodgeaddition>{{cite news|title=Cal-Neva Lodge Addition is Set|url=https://www.newspapers.com/clip/3627268//|accessdate=November 14, 2015|work=The Fresno Bee|publisher=[[Newspapers.com]]|date=November 8, 1968|page=11}} {{open access}}</ref> They sought to re-open the Cal Neva on July 1, 1969,<ref name=julyset>{{cite news|title=Opening Date New Cal-Neva Lodge Set July 1|url=https://www.newspapers.com/clip/3627248//|accessdate=November 14, 2015|work=The Times|publisher=[[Newspapers.com]]|date=April 18, 1969|page=20}} {{open access}}</ref> but instead it partly opened in May of that year with the new expansion opening the following month.<ref name=former/> The Ohio Real Estate Investment Trust purchased the lodge in 1970 for $6 million,<ref>{{cite news|title=Ohio Investment Co. To Buy 2nd Casino|url=https://www.newspapers.com/clip/3627461//|accessdate=November 14, 2015|work=Washington C.H. Record-Herald|publisher=[[Newspapers.com]]|date=August 21, 1970|page=9}} {{open access}}</ref> with the aim to lease it to the U.S. Capital Corporation, who in turn sought to sublease it to Tahoe Crystal Bay Inc.<ref>{{cite news|title=Purchase of Casino Proposed|url=https://www.newspapers.com/clip/3627472//|accessdate=November 14, 2015|work=News-Journal|publisher=[[Newspapers.com]]|date=July 8, 1970|page=12}} {{open access}}</ref> The stockholders of the Ohio Real Estate Investment Trust filed a lawsuit against the company in 1973, which resulted in the Cal Neva being placed into federal receivership.<ref name=2nevada>{{cite news|title=2 Nevada Clubs Bought for $9 Million|url=https://www.newspapers.com/clip/3627567//|accessdate=November 14, 2015|work=The Logan Daily News|publisher=[[Newspapers.com]]|date=February 24, 1975|page=7}} {{open access}}</ref> After opening year round since Sinatra purchased the lodge, it closed for the winter season in 1974 due to a drop in visitors blamed on the [[1973 oil crisis]].<ref>{{cite news|title=Cal-Neva Lodge Will Close|url=https://www.newspapers.com/clip/3627519//|accessdate=November 14, 2015|work=Galesburg Register-Mail|publisher=[[Newspapers.com]]|date=January 5, 1974|page=8}} {{open access}}</ref>

The Cal Neva was sold in 1975 alongside the [[Crystal Bay Club]] to BKJ Corporation, who consisted of Everett Brunzell, Charles Ketchum and Norman Jenson. The two clubs were purchased for a total of $9 million.<ref name=2nevada/> But their licensing application for Cal Neva was turned down by the Nevada Gaming Commission due to a their lack of ongoing finances.<ref>{{cite news|title=Miden Slot Permit Denied|url=https://www.newspapers.com/clip/3627645//|accessdate=November 14, 2015|work=The Edwardsville Intelligencer|publisher=[[Newspapers.com]]|date=October 17, 1975|page=57}} {{open access}}</ref> The [[First National Bank of Nevada]] foreclosed on the property in early 1976 before selling it to [[Kirk Kerkorian]], a significant shareholder in [[Metro-Goldwyn-Mayer]].<ref>{{cite news|title=Kerkorian Buys Casino at Tahoe|url=https://www.newspapers.com/clip/3627665//|accessdate=November 14, 2015|work=The San Bernardino County Sun|publisher=[[Newspapers.com]]|date=November 23, 1976|page=15}} {{open access}}</ref> He owned the Cal Neva through his Tracinda Investment Corporation, a different entity than the one through which he owns his MGM shares. As part of the re-launch of the lodge, he convinced Dean Martin to return for a performance, for which [[Cary Grant]] was also in attendance.<ref>{{cite news|last1=Eder|first1=Shirley|title=Others Share Time With Cary|url=https://www.newspapers.com/clip/3627722//|accessdate=November 14, 2015|work=Santa Ana Register|publisher=[[Newspapers.com]]|date=April 29, 1977|page=58}} {{open access}}</ref>

Jon Perroton was arrested in 1985 for fraud after convincing [[Hibernia National Bank]] to loan him $23 million towards the purchase price of Cal Neva. He had claimed that he had the backing of [[Sheraton Hotels and Resorts]]. He was subsequently sentenced to 20 years in custody.<ref>{{cite news|title=Con Artist Gets 20 Years|url=https://www.newspapers.com/clip/3627783//|accessdate=November 14, 2015|work=Ukiah Daily Journal|publisher=[[Newspapers.com]]|date=June 23, 1985|page=2}} {{open access}}</ref> That same year, the lodge was purchased by Chuck Bleuth. Under the new owner, it was renovated and the exterior of the main building restored to how it looked in the 1960s.<ref name=castle569/> 
On September 10–12, 1993, the lodge hosted the second annual Daow Aga [[Pow wow]] (a local [[Native Americans in the United States|Native American]] meeting) on Lake Tahoe.<ref>{{cite book|title=The Native Magazine: 1993–1995|url=https://books.google.com/books?id=cWhyAAAAMAAJ|year=1993|publisher=Native Network|page=22}}</ref> In 1998 the hotel was named "Nevada's Best Getaway" and cited as one of America's 50 Flagship Hotels.<ref name="ICP">{{cite book|title=Insurance Conference Planner|url=https://books.google.com/books?id=7BgtAQAAMAAJ|year=1998|publisher=Bayard Publications|page=77}}</ref>

In 2005, Bleuth sold the lodge onto [[Ezri Namvar]]. However the casino was in severe decline; by 2009 the revenue was around half of that which it received in 1992. [[Canyon Capital Advisors]] foreclosed on a $25 million loan, and an auction open across two states failed to result in any bidders. The area had seen several other casinos nearby close around the same time.<ref name=tahoeclosed/>

==Architecture and location==
[[File:Cal Neva casino.jpg|thumb|left|The casino]]
The Cal Neva Lodge and Casino overlooks [[Lake Tahoe]] with the property split across the California/Nevada border near [[Crystal Bay, Nevada|Crystal Bay]].<ref name=castle569>[[#castle1995|Castle (1995)]]: p. 569</ref><ref name=castle469>[[#castle1995|Castle (1995)]]: p. 469</ref> The main dining room has a white line indicating the state border running down the middle of the room,<ref>[[#lankford2010|Lankford (2010)]]: p. 197</ref> and continues outside through a swimming pool.<ref name=wings>{{cite news|last1=Wilson|first1=Earl|title=Sinatra With Wings? That's How It Looks!|url=https://www.newspapers.com/clip/3626657//|accessdate=November 14, 2015|work=Janesville Daily Gazette|publisher=[[Newspapers.com]]|date=July 25, 1958|page=7}} {{open access}}</ref> The interior is decorated in the lodge-style.<ref name=moe90/> The entrance had been replaced several times over the years, although the exterior of the buildings have remained almost the same since the <!--which year--> reconstruction.<ref>[[#larson2008|Larson (2008)]]: p. 97</ref> Prior to the expansion under Sinatra, the lodge had 55 rooms and 11 lodges,<ref name=moe90>[[#woodsmoe1996|Woods Moe (1996)]]: p. 90</ref> but this was increased to 220<!--all lake-facing or only some? according to the source, all of them. Not sure how that works though. --> lake-facing rooms following the expansion in 1969.<ref name=lodgeaddition /><ref>[[#castle1995|Castle (1995)]]: p. 570</ref>
As of 1998 it had 182 rooms and suites and eight conference rooms, catering for up to 400 people.<ref name="ICP"/>

===Celebrity and Indian rooms===
Constructed following the purchase by Sinatra and his associates, the Celebrity room and theater hosted both the singer and his [[Rat Pack]] friends as well as other singers of the day.<ref name=tahoeclosed/> It is located near to the casino area on the Nevada side of the border,<ref name=castle569/> and has sat up to 700 people.<ref>{{cite news|title=Cal-Neva Lodge Seats 700 in Celebrity Room|url=https://www.newspapers.com/clip/3627365//|accessdate=November 14, 2015|work=The Times|publisher=[[Newspapers.com]]|date=August 15, 1969|page=19}} {{open access}}</ref> The Indian room featured [[Native Americans in the United States|Native American]] memorabilia and wood-beamed ceilings,<ref name=castle469/> and is sited in the California side of the hotel. It acted as both a museum to the nearby [[Washoe people]], as well as operating as a ballroom.<ref name=castle569/> The Indian room underwent renovation in the late 1980s, when it was described by ''[[Skiing (magazine)|Skiing]]'' magazine as being "a spacious area with a large fireplace".<ref name="Skiing1989"/>

===Current renovations===
[[File:Cal Neva Casino, 2010.jpg|thumb|Hotel facade in 2010]]
Criswell-Radovan, LLC, a [[Napa, California|Napa]], California-based development company which acquired the property in spring 2013, closed it as a whole on September 5, 2013 for a complete renovation.<ref name=tahoeclosed>{{cite news|title=Frank Sinatra's Lake Tahoe casino shuts down|url=http://usatoday30.usatoday.com/travel/destinations/2010-03-31-lake-tahoe-rat-pack-casino_N.htm|accessdate=November 14, 2015|work=USA Today|date=March 31, 2010|archiveurl=https://web.archive.org/web/20150205040816/http://usatoday30.usatoday.com/travel/destinations/2010-03-31-lake-tahoe-rat-pack-casino_N.htm|archivedate=February 5, 2015}}</ref> The development project was originally estimated to take at least a year and was to include an overhaul of the 10-story Cal Neva, including interior guest rooms, the Circle Bar, the casino, Frank Sinatra's Celebrity Room theater and a complete exterior upgrade. The casino upgrade sought to restore the Cal Neva's original 6,000-square-foot gaming floor, complete with a full slot machine display and the return of table games. Criswell-Radovan was aiming to reopen the property in conjunction with what would have been Sinatra's 99th birthday on December 12, 2014.<ref name=oldtahoeAP>{{cite news|last1=Griffith|first1=Martin|title=Sinatra's Old Tahoe Resort to Get Major Makeover|url=http://bigstory.ap.org/article/sinatras-old-tahoe-resort-get-major-makeover-0|accessdate=November 14, 2015|work=Associated Press|date=September 8, 2013|archiveurl=https://web.archive.org/web/20131007164933/http://bigstory.ap.org/article/sinatras-old-tahoe-resort-get-major-makeover-0|archivedate=October 7, 2013}}</ref> However, reopening was first delayed to January 2015, then summer 2015, and then pushed into 2016. The delays were attributed to a combination of factors: the need for construction to proceed carefully and the difficulties in obtaining financing.  The company obtained a $29 million loan in November 2014 that allowed construction to begin.<ref>[http://www.rntl.net/calneva.htm Cal Neva Lake Tahoe remodel construction]</ref>  An additional $20 million equity line of credit is also financing the project.

The present owners of the property, Criswell-Radovan, have filed for bankruptcy.<ref>{{cite web|url=http://www.redbluffdailynews.com/business/20160712/owner-of-tahoes-famed-cal-neva-lodge-files-for-bankruptcy|title=Owner of Tahoe's famed Cal Neva Lodge files for bankruptcy|date=July 12, 2016|accessdate=December 29, 2016}}</ref>

Hill said the bankruptcy filing guarantees that that the Cal Neva won’t reopen this year but that Criswell-Radovan has a permit allowing construction until October 15, 2018.<ref name=MacMillan2014>{{cite web|last1=MacMillan|first1=Kevin|title=Lake Tahoe's Cal Neva reopening delayed yet again; 2016 now eyed|url=http://www.tahoedailytribune.com/news/13664953-113/radovan-resort-cal-neva|website=Tahoe Daily Tribune|accessdate=November 14, 2015|date=November 5, 2014}}</ref>

==Notes==
{{Reflist|group="n"}}

==References==
{{Reflist|30em}}

'''Sources'''
{{Refbegin|40em}}
*{{cite book|last1=Castle|first1=Ken|title=Tahoe: The Complete Guide|date=1995|publisher=Foghorn Press |location=San Francisco|isbn=9780935701029|ref=castle1995}}
*{{cite book|last1=Clarke|first1=Donald|title=All Or Nothing At All: A Life of Frank Sinatra|date=1998|publisher=Thorndike Press |location=Thorndike, ME.|isbn=9780754011408|ref=clarke1998}}
*{{cite book|last1=Freedland|first1=Michael|title=All The Way: A Biography of Frank Sinatra|date=1997|publisher=St. Martin's Press|location=New York|isbn=9780312191085|ref=freedland1997}}
*{{cite book|last1=Kelley|first1=Kitty|title=His Way|date=1986|publisher=Bantam Books|location=New York|isbn=9780553051377|ref=kelley1986}}
*{{cite book|last1=Kuntz|first1=Tom|last2=Kuntz|first2=Phil|title=The Sinatra Files: The Secret FBI Dossier|date=2000|publisher=Three Rivers Press|location=New York|isbn=9780812932768|ref=kuntzkuntz2000}}
*{{cite book|last1=Lankford|first1=Scott|title=Tahoe Beneath the Surface|date=2010|publisher=Sierra College Press|location=Berkeley, CA|isbn=9781597141390 |ref=lankford2010}}
*{{cite book|last1=Larson|first1=Sara|title=Lake Tahoe|date=2008|publisher=Arcadia Pub.|location=Charleston, SC|isbn=9780738558493 |ref=larson2008}}
*{{cite book|last1=Woods Moe|first1=Albert|title=Nevada's Golden Age of Gambling|date=1996|publisher=Nevada Collectables|location=Reno, NV|isbn=9780965521505|ref=woodsmoe1996}}
*{{cite book|last1=Woods Moe|first1=Albert|title=The Roots of Reno|date=2008|publisher=BookSurge|isbn=9781439211991 |ref=woodsmoe2008}}
{{Refend}}

==External links==
* {{Official website|http://calnevaresort.com/}}
{{Commons category-inline}}

{{coord|39|13|29.3|N|120|00|19.9|W|type:landmark_region:US-NV|display=title}}

{{Reno Casinos}}
{{Good article}}

{{DEFAULTSORT:Cal Neva Lodge and Casino}}
[[Category:1926 establishments in California]]
[[Category:1926 establishments in Nevada]]
[[Category:Buildings and structures in Washoe County, Nevada]]
[[Category:Casinos in California]]
[[Category:Casinos in Nevada]]
[[Category:Hotels established in 1926]]
[[Category:Hotels in California]]
[[Category:Hotels in Nevada]]
[[Category:Resorts in California]]
[[Category:Resorts in Nevada]]