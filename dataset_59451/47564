{{advert|date=September 2013}}

{{Infobox company |
  name   = Dominick and Dickerman LLC|
  logo   =  |
  type   = Private|
  foundation     = [[New York City]], [[New York (state)|New York]], U.S. (1870)|
  location       = 570 Lexington Avenue<br>[[New York City]]|
  key_people     =  |
  num_employees  = |
  aum         = |
  industry       = [[Financial Services]] |
  products       = |
  homepage       = [http://www.dominickanddickerman.com www.dominickanddickerman.com]
}}
 
Dominick and Dickerman is an investment and merchant banking firm, located in New York City.  From 1899 through 2015, the firm was known as Dominick and Dominick.  Following the sale of the wealth management business, the firm reverted to its original name, Dominick and Dickerman. 

The firm was founded in 1870 and is one of the oldest, continuously operated [[financial services]] institutions in the [[United States]].  Dominick & Dickerman LLC services its individual and corporate clients primarily through three business divisions: [[Private Wealth Management]], [[Investment Banking]] and Institutional Sales.  Private Wealth Management offers clients the full spectrum of wealth management solutions, including investment strategies, asset allocation, wealth and [[estate planning]], [[insurance]] products and [[alternative investments]].  The Investment Banking team services public and private corporations around the world by raising capital, developing and implementing strategic merger and acquisition plans, and advising senior management teams on a variety of governance, operations and growth issues.  Institutional Sales recommends and executes trading strategies for institutional clients in the United States and abroad.  Dominick & Dominick LLC is headquartered in [[New York City]] and has offices in  [[Basel]], Switzerland.  Dominick & Dominick LLC is a member of the [[Securities Investor Protection Corporation]] and the [[Financial Industry Regulatory Authority]].

==History==

===Founding===
The company was founded on June 15, 1870 as Dominick & Dickerman by William Gayer Dominick and Watson Bradley Dickerman. Dominick was born in Chicago, and moved to New York as a child. In 1869, at the age of 25, he purchased membership on the New York Stock Exchange. At the NYSE, Dominick met Connecticut-born Dickerman and they went into business forming a [[brokerage firm|stock brokerage firm]].<ref>The New York Times Obituary. "William Gary Dominick" The New York Times 1 September 1985: n. pag. Print</ref> Dominick's brothers, George and Bayard Dominick, also joined the Exchange and became partners in the firm.<ref>{New York Times. "Anniversay Celebrated: Dominick & Dominick, Brokers, Observe Fiftieth Year in Business." The New York TImes 15 June 1920: n. pag. Print}</ref>

Dominick & Dickerman opened its first branch in 1889 in Cincinnati, where the firm was one of only two exchange members. A year later, Dickerman left the firm when he was elected president of the New York Stock Exchange. He would serve as president from 1890 to 1892, then return to the firm. His cofounder, William Dominick, died in 1895 at the age of 50 to typhoid fever. Dickerman would retire in 1909, passing away at the age of 77 in 1923.

Dominick & Dickerman changed its name in 1899 to Dominick & Dominick, adding several new partners including Milnor B. Dominick, Andrew V. Stout, J.A. Barnard, and Bernon S. Prentice. The firm was one of the original sources for [[closed-end fund]]s, launching The Dominick fund, Inc in 1920 by selling 200,000 shares for a raise of $10 million. Despite the [[Wall Street Crash of 1929|stock market crash in 1929]], the fund survived and was listed in 1959 on the NYSE before it was merged with [[Putnam Investments|Putnam Fund]] in 1973.<ref>SEC "SEC NEWS DIGEST" www.sec.gov/news/digest/1974/dig100274.pdf SEC Docket, Vol 6, No. 8 3 October 1974</ref>

===Expansion===

In 1936 Dominick & Dominick expanded through acquisition, merging with A. Iselin & Co., also one of Wall Street’s oldest firms. Several months earlier the patriarch of the firm, Adrian Iselin, died at the age of 89. He had joined the firm, which his father formed, as a 22-year-old in 1868. At the time of the merger, Dominick & Dominick had 13 partners, including Gayer G. Dominick (senior partner since 1926), Bayard Dominick, and Gardner Dominick Stout. It next picked up several partners from Iselin & Co., as well as Iselin Securities Corporation, which brought with it an office in Paris, and the Iselin Corporation of Canada with its office in Montreal. Because Dominick & Dominick already maintained a London office, the London office of Iselin Securities was closed.
Other European offices were subsequently opened, and Dominick & Dominick soon had a presence in all of the major cities in Europe.<ref>{Iselin Firm to End, Joining Dominicks." The New York Times 17 June 1973}</ref>

===World War II===

A large number of the firm's employees and partners entered the military, including Gayer Dominick. The firm was content to just keep its doors opened and operate. Gayer Dominick had been with the firm since 1909 after graduating from [[Yale University]]. In 1935 he was elected a governor of the New York Stock Exchange and helped to hire the first paid president of the Exchange, at the behest of the new [[U.S. Securities and Exchange Commission|Securities and Exchange Commission (SEC).]] He then left the family firm in 1938 to enter public service, working for the [[Office of Price Administration]] in the Roosevelt administration.<ref>New York Times. GAYER DOMINICK, BROKER, 74, DIES; Ex-Partner in Firm Here -- Led Stock Exchange in '35. http://select.nytimes.com/gst/abstract.html?res=F20E15F8355B147A93CBA81783D85F458685F9 New York Times Staff. August 19, 1961</ref>

===Post-War expansion===

After World War II came to an end and following a brief economic recession as the United States reverted to a peace-time footing, the economy enjoyed a long period of growth. Dominick & Dominick benefited from the country's prosperity. Some of the firm's most notable transactions during the postwar years involved Yonkers, New York-based  Alexander Smith Carpet Company and Canada's Great Plains Oil. In the late 1950s Dominick & Dominick was also part of a banking syndicate that managed the initial public offering (IPO) of stock issued by [[Arvida Corporation]], which was formed in Florida in 1958 to sell the real estate holdings of [[Arthur Vining Davis]]. The IPO gained attention because of objections raised by the SEC to the way the managers had announced the stock sale before filing a registration statement with the SEC, a violation of the law.

Dominick & Dominick ended registration as a partnership, reorganizing as a corporation in 1964. The 1960s also saw the firm spread its operation across the country, taking advantage of a bull market to build up a domestic retail brokerage business. In 1962 an office in Chicago was opened. Dominick & Dominick gained a major presence in New England in 1966 by acquiring the firm of Townsend, Dabney, Tyson. Not only did the firm pick up a large Boston office but another 15 offices throughout the Northeast. About 30 additional branch offices across the United States were opened by the end of the 1960s.<ref>New York Times Staff."Schoellkopf, Hutton & Pomeroy To Merge with Dominick Firm." The New York Times 19 February 1960: n. pag. Print</ref><ref>New York Times Staff "Dominick Planning to Acquire Dittmar" The New York Times 24 December 1970: n. pag. Print</ref>

In 1970 Dominick & Dominick pursued a merger with [[E. W. Clark & Co.|Clark, Dodge & Co., Inc.]], a similar size firm, but called it off, electing instead to continue a program of opening new offices and pursuing the acquisition of smaller firms. This plan was also eventually terminated, however, as the stock market began to experience one of the worst bear markets in a generation, and Dominick & Dominick found that it had stretched itself far too thin.<ref>Vartan, Vartanig G. "Dominick to Quit Retail Brokerage." The New York Times 31 July 1973: n. pag. Print</ref>

===Dark period===
Strapped for cash the firm sold four of its five seats on the New York Stock Exchange and one of two seats on the [[American Stock Exchange]]. It also sold a significant stake in the business for $7.25 million to an investment group led by Pierce National Life Insurance Company (now [[Liberty Corporation]]), which was in turn controlled by [[Joe L. Allbritton]], founder of [[Allbritton Communications Company]].<ref>Vartan, Vartanig G. "Served Carriage Trade" February 28, 1973 The New York Times, Page 58 Column 8. Print.</ref> While the infusion of capital was welcome, Dominick & Dominick still found itself in a difficult position and decided to exit the domestic retail brokerage business and to sell the bulk of its branch offices.
The firm's chairman and chief executive, Peter M. Kennedy, explained to the New York Times that "a national retail structure is not right for a firm of our size. We either had to be bigger or smaller." He added, "We are not going out of business. We are just changing the nature of our business." <ref>Allan, John H. "Two Wall Street Firms Undergo Changes." The New York Times 22 Feb. 1973: n. pag. Print.</ref>
Dominick & Dominick retained a modest retail business but mostly chose to focus on core strengths, including institutional business, money management, corporate finance, municipal bonds, and its international business. It was also in 1973 The Dominick fund, which had about $55 million in assets, was taken over by [[Putnam Investments|Putnam Fund]].<ref>{Vartan, Vartanig G. "Dominick to Quit Retail Brokerage." The New York Times 31 July 1973: n. pag. Print}</ref>

Over the next 20 years, Dominick & Dominick reduced its work force and closed offices in an attempt to focus on more profitable financial services such as research. The firm also became involved in the fixed income area, making [[Corporate bonds|corporate]] and [[municipal bonds]], [[Eurobond]]s, and [[Treasury Notes]] available to its clients, and launched managed futures programs to participate in the global currency markets. The firm also did a healthy business providing clearing services to more than 100 [[National Association of Securities Dealers]] firms; its Dominick & Dominick Advisors unit provided investment and portfolio management services to high-net-worth investors and institutions in the United States, Europe, and Asia.<ref>{Dominick Branches Sold to Other Firms." The New York Times 8 Aug. 1973}</ref>

===21st century===
By the start of the new century, Dominick & Dominick was in decline and needed an infusion of partner capital. In October 2003 the firm brought in a new president and CEO, hiring 58-year-old Michael J. Campbell, a former Marine who had 30 years of experience in the industry, including a 25-year tenure with [[Donaldson, Lufkin & Jenrette]] (DLJ) and a stint with [[Credit Suisse First Boston]] after Credit Suisse acquired DLJ. With DLJ Campbell managed the private client group, expanding the firm's high-net-worth and midsized institutional investor brokerage business from 75 advisors to a network of more than 500 investment professionals.<ref>Business Wire. DLJ's Michael J. Campbell Joins Dominick & Dominick as CEO; Assembles New Senior Management Staff http://findarticles.com/p/articles/mi_m0EIN/is_2003_Oct_6/ai_108541151/ Business Staff. October 6, 2003</ref>

Campbell joined Dominick & Dominick in 2003, bringing senior management from DLJ and Credit Suisse First Boston. The new management relocated the firm headquarters from lower Manhattan to Midtown Manhattan to an office on 52nd St. In addition, Campbell wasted little time in recruiting new advisors from Credit Suisse and other major financial firms.

Dominick & Dominick's first branch office to open under Campbell's management was in the fall of 2004 when an operation in Miami was opened to focus on wealthy Central and South American residents. It was not an acquisition, as Dominick was absorbing the Miami office of Pennsylvania-based First Security Investments, which had been opened by another former DLJ employee, Alain O'Hayon, who stayed on to manage the office. Campbell was very familiar with the potential of a Miami operation, having built up an office in the city for DLJ from just two brokers to more than 70.<ref>On Wall Street. Michael Campbell
The President and CEO of Dominick & Dominick {{cite web
 |url=http://www.onwallstreet.com/ows_issues/2010_7/life-stories-2667389-1.html 
 |title=Archived copy 
 |accessdate=2011-05-26 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20111002045304/http://www.onwallstreet.com/ows_issues/2010_7/life-stories-2667389-1.html 
 |archivedate=2011-10-02 
 |df= 
}} Pat Olson July 1, 2010</ref>

In 2006 another regional office was added in Atlanta.

A year later, in 2007, Dominick & Dominick launched a new [[risk arbitrage]] group with the hope to develop synergies with the firm's existing institutional and high-net-worth client base.<ref>Business Wire. DLJ's Michael J. Campbell Joins Dominick & Dominick as CEO; Assembles New Senior Management Staff http://findarticles.com/p/articles/mi_m0EIN/is_2003_Oct_6/ai_108541151/ Business Staff. October 6, 2003</ref>

==Stanford Financial Group receivership==
On November 13, 2009, the US District Court ordered the Brokerage Accounts of [[Stanford Financial Group]] Brokerage to be transferred to Dominick & Dominick LLC. The Stanford Group was the firm run by [[Allen Stanford]]. Both Stanford Group Company and Dominick & Dominick LLC use [[Pershing LLC]] as their [[Clearing house (finance)|clearing firm]]. The transfer became effective on January 20, 2010.<ref>Stanford Financial Group Receivership http://stanfordfinancialreceivership.com</ref>

==Former employees==
* Michael Campbell: Former Head of Wealth Management at [[Donaldson, Lufkin & Jenrette|Donaldson, Lufkin and Jenrette]]
* [[Chuck Carney]]: [[College Football Hall of Fame]] member
* [[Ray Dalio]]: Founder of [[Bridgewater Associates]]
* Susan Edwards: Executive Director of the [[Frist Center for the Visual Arts|Frist Center for Visual Arts]]
* [[Habibollah Hedayat]]: Founder of the Institute of Nutrition and Food Science of Iran
* [[John G. W. Husted, Jr.]]: Briefly engaged to [[Jacqueline Kennedy Onassis]] prior to her marriage to [[John F. Kennedy]]
*  [[Peter Kellogg|Peter Rittenhouse Kellogg]]: Former Partner at [http://www.answers.com/topic/spear-leeds-kellogg#ixzz24NOCaYZh Spear, Leeds & Kellogg Specialists LLC], purchased by [[Goldman Sachs]] in 2000 for $6.3 billion.
* Peter Kennedy: Former Dominick & Dominick Investment Banker, Former Owner of [http://www.answers.com/topic/drexel-heritage-furnishings-inc Drexel Furniture Co]
* [[Seymour H. Knox III]]: philanthropist and founder/owner of the [[NHL]] [[Buffalo Sabres]] <ref> [http://www.tennessean.com/article/20110731/ENTERTAINMENT05/307310050/Frist-leader-Susan-Edwards-fuels-passion-arts-Music-City www.tennessean.com]</ref>
* Avery Rockefeller Jr: Former Chairman of the Board of Governors for [[Financial Industry Regulatory Authority|FINRA]]. Son of [[Avery Rockefeller]], great, great grandson of [[William Rockefeller]].
* [[Jim Rogers]]: Co-Founder of the [[Quantum Fund]]
* [[Marion Sandler]]: Billionaire founder of [[Golden West Financial Corporation]], predecssor to [[World Savings Bank]].<ref>https://www.bloomberg.com/news/2012-06-04/marion-sandler-home-lender-who-made-billions-dies-at-81.html</ref>
* [[Gardiner Dominick Stout]], Dominick & Dominick partner, President [[American Museum of Natural History]]
* [[Gaylord K. Swim]]: [[Sutherland Institute|Founder of Sutherland Institute]]
* [[Bronson Thayer]]: Chairman of the Board and Chief Executive Office of Bay Cities Bank

==References==
;Notes
{{reflist|2}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
;Bibliography
* Allan, John H. "Two Wall Street Firms Undergo Changes." The New York Times 22 Feb. 1973: n. pag. Print.
* New York Times. "Anniversay Celebrated: Dominick & Dominick, Brokers, Oberve Fiftieth Year in Business." The New York TImes 15 June 1920: n. pag. Print
* Staff. "Dominick Branches Sold to Other Firms." The New York Times 8 Aug. 1973: n. pag. Print.
* Staff. "Iselin Firm to End, Joining Dominicks." The New YOrk Times 17 June 1973: n. pag. Print.
* Vartan, Vartanig G. "Dominick to Quit Retail Brokerage." The New York Times 31 July 1973: n. pag. Print
* Dominick & Dominick LLC Homepage. "About US." Dominick & Dominick LLC. N.p., n.d. Web. 27 Apr. 2011. <http://www.dominickanddominick.com/>.

==External links==
* {{Official website|http://www.dominickanddominick.com}}

{{DEFAULTSORT:Dominick and Dominick}}
[[Category:Companies established in 1870]]
[[Category:Investment companies based in New York City]]
[[Category:Companies based in New York City]]
[[Category:Financial services companies based in New York City]]
[[Category:Brokerage firms]]
[[Category:1870 establishments in New York]]