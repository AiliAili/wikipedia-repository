{{Use dmy dates|date=February 2013}}
{{Infobox journal
| cover = [[File:EJNcover.gif]]
| editor = John J. Foxe, Paul Bolam
| discipline = [[Neuroscience]]
| abbreviation = Eur. J. Neurosci.
| publisher = [[Wiley-Blackwell]]
| country =
| frequency = Biweekly
| history = 1989–present
| openaccess = [[Hybrid open access journal|Hybrid]]
| impact = 3.753
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-9568
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-9568/currentissue
| link1-name = Online access
| OCLC = 39503252
| LCCN =
| CODEN = EJONEI
| ISSN = 0953-816X
| eISSN = 1460-9568
}}
The '''''European Journal of Neuroscience''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of developmental, molecular, cellular, systems, behavioral, and cognitive [[neuroscience]]. It was established in 1989 with [[Rainer Guillery]] (then at the [[University of Oxford]]) as the founding [[editor-in-chief]].<ref>{{cite journal |doi=10.1111/j.1460-9568.1989.tb00768.x|pmid=12106168 |title=Editorial |journal=European Journal of Neuroscience |volume=1 |issue=1 |pages=1|year=1989 |last1=Guillery |first1=R. W.}}</ref> Currently the journal is edited by [[John J. Foxe]] ([[University of Rochester]]) and [[Paul Bolam]] ([[Oxford University]]). The journal is published by the [[Federation of European Neuroscience Societies]] in collaboration with [[Wiley-Blackwell]]. Authors can elect to have accepted articles published as [[open access (publishing)|open access]].<ref name="OnlineOpen">{{cite web |url=http://www3.interscience.wiley.com/authorresources/onlineopen.html |title=Author Resources: OnlineOpen |work=Wiley InterScience |accessdate=13 October 2009}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Elsevier BIOBASE|BIOBASE]], [[Biological Abstracts]], [[BIOSIS Previews]], [[EMBASE|EMBASE/Excerpta Medica]], [[Index Medicus]]/[[MEDLINE]], [[Science Citation Index|Neuroscience Citation Index]], [[PsycINFO]], [[PubMed]], [[Science Citation Index]], and [[The Zoological Record]]. According to the ''[[Journal Citation Reports]],'' its 2012 [[impact factor]] is 3.658, ranking it 79th out of 252 journals in the category "Neuroscience".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Neuroscience|title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}
{{Portal|Neuroscience}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1460-9568}}

{{DEFAULTSORT:European Journal Of Neuroscience}}
[[Category:Neuroscience journals]]
[[Category:Publications established in 1989]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Biweekly journals]]
[[Category:Hybrid open access journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]