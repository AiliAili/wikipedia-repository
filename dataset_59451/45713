{{Infobox person
| name                      = Diana Barnato Walker 
| image                     = Taxi Walker.jpg
| image_size                = 300px
| alt                       = Barnato Walker at the controls of an Airspeed Oxford
| caption                   = Barnato Walker at the controls of an [[Airspeed Oxford]]
| birth_name                = Diana Barnato
| birth_date                = {{Birth date|1918|1|15|df=y}}  
| birth_place               = 
| death_date                = {{Death date and age|2008|4|28|1918|1|15|df=y}} 
| death_place               = [[Surrey]], [[England]]
| death_cause               = [[Pneumonia]]
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 = 
| residence                 = 
| nationality               = British
| citizenship               = 
| education                 = 
| alma_mater                = 
| occupation                = [[Aviator]]
| home_town                 = 
| spouse                    = {{marriage|Derek Ronald Walker<br>|1944|1945|reason=his death}}
| partner                   = [[Whitney Straight]]
| children                  = Barney Barnato Walker
| parents                   = [[Woolf Barnato]]<br>Dorothy Maitland Falk
| relatives                 = [[Barney Barnato]] (grandfather)
| callsign                  = 
| awards                    = [[Order of the British Empire|MBE]]
| signature                 = 
| signature_alt             = 
| signature_size            = 
}}
'''Diana Barnato Walker''' [[MBE]] [[FRAeS]] (15 January 1918 – 28 April 2008) was an English [[aviator]], the first British woman to break the [[sound barrier]].<ref name="DailyMail1">{{cite news |url=http://www.dailymail.co.uk/pages/live/femail/article.html?in_article_id=564237&in_page_id=1879 |title=She flew Spitfires and was the first woman to break the sound barrier - the very racy life of the original fast lady |first=Tony |last=Rennell |work=[[Daily Mail]]|date=6 May 2008 |location=London |accessdate=6 May 2008}}</ref><ref name="NYTObit">{{cite news|last1=Burns|first1=John F.|title=Diana Barnato Walker, Acclaimed Pilot, Dies at 90|url=https://www.nytimes.com/2008/05/12/world/europe/12walker.html?_r=0|accessdate=29 March 2016|publisher=''[[The New York Times]]''|date=May 12, 2008}}</ref>

==Early life==
Diana Barnato was born 15 January 1918.  In 1936, at the age of 18, she was a [[debutante]] and was presented to [[King Edward VIII]] at [[Buckingham Palace]].<ref name="Glancey">{{cite news|last1=Glancey|first1=Jonathan|title=Diana Barnato Walker She flew warplanes and was the first British woman to break the sound barrier|url=https://www.theguardian.com/uk/2008/may/08/military.gender|accessdate=29 March 2016|publisher=''The Guardian''|date=May 7, 2008}}</ref>

From an early age, she became interested in aircraft and at age 20 she decided to become a pilot. Her initial training was in  [[Tiger Moth]]s at the [[Brooklands]] Flying Club, the  aerodrome being located within the famous [[motor racing]] circuit in [[Surrey]].  She showed a natural aptitude for flying and made her first solo flight after only six hours of dual instruction.<ref name="NYTObit"/>

===Family===
Diana's parents were [[Woolf Barnato]] (1895-1948), Chairman of [[Bentley Motors]] and a leading member of their racing team, and his first wife, Dorothy Maitland Falk (1893-1961),<ref name="Wainwright">{{cite web|last1=Haines|first1=Barbara|title=Dorothy Maitland Falk Wainwright|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=135995655|website=findagrave.com|publisher=Find A Grave Memorial|accessdate=29 March 2016}}</ref> an American from [[White Plains, New York]], who were married at the [[The Ritz Hotel, London|Ritz Carlton in London]].<ref name="FalkDead">{{cite news|last1=Staff|title=MRS. FLORENCE FALK DEAD.|url=http://timesmachine.nytimes.com/timesmachine/1931/08/18/118224068.html?pageNumber=21|accessdate=29 March 2016|publisher=''[[The New York Times]]''|date=August 18, 1931}}</ref> Her paternal grandfather was [[Barney Barnato]] (1851–1897), a co-founder of the [[De Beers]] mining company in [[Johannesburg]].<ref name="NYTObit"/> Her maternal grandparents were American stockbroker Herbert Valentine Falk and Florence Maude Whittaker.<ref name="Wainwright"/>  While married from 1915-1933, her parents had two children,  Virginia Barnato (1916-1980)<ref name="Wainwright"/> and Diana.

Her father remarried twice, first to Jacqueline Claridge Quealy from 1933–1947, and second, to Joan Jenkinson from 1947 until his death in 1948. Quealy was the daughter of a wealthy American [[colliery]] owner, and with her, Barnato had two more children, half-brothers to Virginia and Diana, Michael Jay Barnato, and Peter Woolf (1934–1959). In 1925, her mother remarried to Lt. Richard Butler Wainwright.<ref name="Wainwright"/>

==World War II==
===Red Cross===
Soon after the outbreak of World War II, Diana volunteered to become a [[Red Cross]] nurse.  In 1940 she was serving as a nurse in France before the evacuation of the [[British Expeditionary Force (World War II)|British Expeditionary Force]] from [[Dunkirk]] and later drove ambulances in London during the [[The Blitz|Blitz]].<ref name="DailyMail1"/>

===Air Transport Auxiliary===
[[File:Walker Spitfire.jpg|thumb|right|Barnato Walker climbing into the cockpit of a Spitfire while serving with the [[Air Transport Auxiliary]].]]In early 1941 she applied to become one of the first women pilots of the [[Air Transport Auxiliary]] (ATA) and successfully took her initial assessment flying test at their headquarters at [[White Waltham Airfield|White Waltham]], Berkshire, on 9 March 1941 with the ATA's Chief Flying Instructor, A.R.O. Macmillan, in the Tiger Moth's rear seat.<ref name="F1"/>

Diana was admitted to the ATA's Elementary Flying Training School at White Waltham on 2 November 1941.  After a lengthy period of intensive flight instruction and tests in primary training aircraft, she joined her first ATA Ferry Pool (FP), No.15 FP at [[RAF Hamble]], Hampshire, on 9 May 1942.  She soon began to deliver low-powered single engine aircraft from factory or repair base to storage units and [[RAF]] and Naval flying units.<ref name="DailyMail1"/>

Further advanced training permitted her to deliver several hundred [[Supermarine Spitfire|Spitfire]]s, [[Hawker Hurricane|Hurricane]]s, [[North American Mustang|Mustang]]s, [[Hawker Tempest|Tempest]]s and other high performance fighter aircraft.  After yet further training, Diana became eligible to deliver twin-engined aircraft and delivered [[Armstrong Whitworth Whitley|Whitleys]], [[Bristol Blenheim|Blenheims]], [[de Havilland Mosquito|Mosquito]]s, [[B-25 Mitchell|Mitchell]]s and [[Vickers Wellington|Wellington]]s, normally flying solo when doing so.  She continued intensive flying with the ATA until the organisation was disbanded in late 1945.  By that time she had flown 80 types of aircraft and had delivered 260 Spitfires.<ref name="Glancey"/><ref name="F1"/>

==Post War==
===Women's Junior Air Corps===
After the war's end, Diana continued to fly and gained her commercial flying licence.  For many years she was a volunteer pilot with the [[Girls Venture Corps Air Cadets|Women's Junior Air Corps]] (WJAC), later the [[Girls Venture Corps Air Cadets|Girls' Venture Corps]], giving flights to air-minded teenage girls to encourage them to enter the aviation industry.  In July 1948, an aircraft that she was flying burst into flames near [[White Waltham]].  Rather than bail out and lose the WJAC's aircraft, she switched off the fuel and glided the aircraft back.<ref name="DailyMail1"/>

===Air Speed Record===
On 26 August 1963 she flew an [[English Electric Lightning|English Electric Lightning T4]] to Mach 1.6 (1,262&nbsp;mph) after convincing [[Secretary of State for Air|the Air Minister]] to let her fly it with [[Squadron Leader]] [[Ken Goodwin (RAF officer)|Ken Goodwin]] as her [[check pilot]], and so became the first British woman to break the [[sound barrier]]. She also established by this flight a world [[Flight airspeed record|air speed record]] for women.<ref name="F1">{{cite web |url= http://www.nationalcoldwarexhibition.org/explore/aircraft-information.cfm?aircraft=English%20Electric%20Lightning%20F1&topic=Top%20Fifteen |title=English Electric Lightning F1 Top Fifteen |work=nationalcoldwarexhibition.org |year=2012 |accessdate=21 September 2012}}</ref>

==Personal life==
She became engaged to [[Squadron Leader]] Humphrey Trench Gilbert [[Distinguished Flying Cross (United Kingdom)|DFC]] of [[No. 65 Sqn RAF]] in April 1942, but he was killed in a flying accident 2 May 1942 when Spitfire BL372/YT-Z crashed at Loves Farm, [[Cutlers Green]], [[Thaxted]], Essex.  With him in the Spitfire was Flt Lt David Gordon Ross.  They took off from [[Great Sampford]], the [[RAF Debden]] satellite station, having consumed 6-8 bottles each of [[Benskins Brewery|Benskins Colne Springs beer]], according to the licensee of the pub.  This information was not revealed until after the [[Court of Enquiry]].  The CO tried to borrow a [[Miles Magister|Magister]] but his [[flight sergeant]], realising that he was in no fit state to fly, told him it was unserviceable. The CO then took a Spitfire.<ref name="NYTObit"/>

Two years later, she married [[Wing commander (rank)|Wing Commander]] Derek Ronald Walker RAF on 6 May 1944.  He continued active flying operations until he was killed on 14 November 1945 in bad weather while flying a [[North American Mustang]] fighter between two UK airfields.<ref name="NYTObit"/>

Diana vowed never to marry again. For 30 years she was the lover of [[Whitney Straight]],<ref name="Glancey"/> also a pilot and a pre-war champion racing driver, like her father.  In 1947, the couple had a son:<ref name="DailyMail1"/>
*Barney Barnato Walker (b. 1947)<ref name="NYTObit"/>
Diana never asked Straight to leave his wife; her only comment was: "I was perfectly content. I had my own identity":<ref name="DailyMail1"/>

Shortly after her record-breaking flight in 1963, Diana was found to have cancer, and subsequently had three operations.  Walker was awarded the [[MBE]] in 1965 for services to aviation, and was a Fellow of the [[Royal Aeronautical Society]]. She died on 28 April 2008, aged 90.<ref name="NYTObit"/>

==References==
;Notes
{{reflist|30em}}
;Sources
* Diana Barnato Walker (1994) ''Spreading My Wings: one of Britain's top woman pilots tells her remarkable story''. Sparkford: Patrick Stephens ISBN 1-85260-473-5

==External links==
* {{cite news |url= http://www.telegraph.co.uk/news/obituaries/1927116/Diana-Barnato-Walker.html? |title=Diana Barnato Walker |work=[[The Daily Telegraph]] |date=4 May 2008  |location=London |issn=0307-1235 |oclc=49632006 |accessdate=12 December 2011}}
* {{cite news |url= https://www.nytimes.com/2008/05/12/world/europe/12walker.html?_r=1&ref=obituaries&oref=slogin |title=Diana Barnato Walker, Acclaimed Pilot, Dies at 90 |first=John F. |last=Burns |work=[[The New York Times]] |date=12 May 2008  |issn=0362-4331 |accessdate=12 December 2011}}
* {{cite news |url= https://www.theguardian.com/uk/2008/may/08/military.gender |title=Obituary: Diana Barnato Walker |first=Jonathan |last=Glancey |work=[[The Guardian]] |date=8 May 2008  |location=London |issn=0261-3077 |oclc=60623878 |accessdate=12 December 2011}}
* {{cite news |url= http://www.independent.co.uk/news/obituaries/diana-barnato-walker-aviator-who-was-the-first-british-woman-to-break-the-sound-barrier-824292.html |title=Diana Barnato Walker: Aviator who was the first British woman to break the sound barrier |first=Philip |last=Jarrett |work=[[The Independent]] |date=9 May 2008|location=London |issn=0951-9467 |oclc=185201487 |accessdate=12 December 2011}}

{{Use dmy dates|date=April 2012}}

{{Authority control}}

{{DEFAULTSORT:Walker, Diana Barnato}}
[[Category:1918 births]]
[[Category:2008 deaths]]
[[Category:British women in World War II]]
[[Category:Female aviators]]
[[Category:English aviators]]
[[Category:Brooklands people]]
[[Category:Members of the Order of the British Empire]]
[[Category:Fellows of the Royal Aeronautical Society]]
[[Category:Air Transport Auxiliary pilots]]
[[Category:British female aviators]]