{{Use dmy dates|date=April 2012}}
{{Infobox military person
| name          =Edward "Teddy" Mortlock Donaldson
| image         =TeddyDonaldson.jpg
| caption       =Edward Mortlock Donaldson
| birth_date   ={{Birth date|df=yes|1912|02|12}}
| death_date  ={{Death date and age|df=yes|1992|06|02|1912|02|12}}
| placeofburial =[[St Andrew's Church, Tangmere]], [[West Sussex]]
| birth_place  =[[Negeri Sembilan]], [[British Malaya]]
| death_place  =[[Royal Naval Hospital Haslar]], [[Hampshire]]
| nickname      =Teddy
| birth_name    =Edward Mortlock Donaldson
| allegiance    ={{flag|United Kingdom}}
| branch        ={{air force|United Kingdom}}
| serviceyears  =1931–1961
| rank          =[[Air Commodore]]
| servicenumber =
| unit          =
| commands      =RAF Flying College<br/>[[RAF Fassberg]]<br/>[[High Speed Flight RAF|High Speed Flight]]<br/>[[RAF Milfield]]<br/>[[RAF Colerne]]<br/>[[No. 151 Squadron RAF|No.&nbsp;151 Squadron]]
| battles       =World War II
* [[Battle of Dunkirk]]
* [[Battle of Britain]]
| awards        =[[Companion of the Order of the Bath]]<br/>[[Commander of the Order of the British Empire]]<br/>[[Distinguished Service Order]]<br/>[[Air Force Cross (United Kingdom)|Air Force Cross]] & [[Medal bar|Bar]]<br/>[[Mentioned in Despatches]]<br/>[[Legion of Merit]] (United States)
| relations     =
| laterwork     =Air Correspondent for the ''[[Daily Telegraph]]''
}}
[[Air Commodore]] '''Edward "Teddy" Mortlock Donaldson''' {{postnominals|country=GBR|size=100%|sep=,|CB|CBE|DSO|AFC}} & [[Medal bar|Bar]] (12 February 1912 – 2 June 1992) was a [[Royal Air Force]] (RAF) [[flying ace]] of the Second World War, and a former holder of the [[flight airspeed record|airspeed]] world record.

==Biography==
Born in [[Negeri Sembilan]],<ref name="DNW"/> then part of [[British Malaya]], his father C.E. Donaldson was a [[judge]]. One of four brothers, three of whom would serve as fighter pilots with the RAF, and gain the [[Distinguished Service Order]] (DSO). Educated in England at the [[King's School, Rochester]]<ref name="DNW"/> and [[Christ's Hospital]],<ref name="Griffiths">{{cite web|url=http://web.singnet.com.sg/~tonym/mortl002.html|title=The Banking Mortlocks|publisher=RJH Griffiths|year=2000|accessdate=13 July 2009}}</ref> he then studied at [[McGill University]] in Canada.<ref name="RAFWeb">{{cite web|url=http://www.rafweb.org/Biographies/Donaldson.htm|title=Edward Mortlock Donaldson|publisher=rafweb.org|accessdate=13 July 2009}}</ref>

===RAF career===
Donaldson joined two of his brothers in the RAF in 1931, granted a [[Commissioned officers|short service commission]] his first posting being to No. 3 Squadron flying [[Bristol Bulldog]]s.<ref name="RAFWeb"/>

In 1932 he was runner up in the R.A.F. Wakefield Boxing Championship, which he won the following year.<ref name="DNW"/> In 1933 the crack-shot won the RAF's Gunnery Trophy One, known as the ''[[Brooke-Popham Air Firing Trophy]],'' and won it again in 1934. In 1935 he became a [[stunt pilot]] as a member of the No. 3 Squadron aerobatic team of five Bulldogs,<ref name="RAFWeb"/> which he led in 1937 and 1938 at the International [[Zurich]] Rally.<ref name="DNW"/>

When the Second World War broke out, [[Squadron Leader]] Donaldson was commanding [[No. 151 Squadron RAF|No. 151 Squadron]] flying the [[Hawker Hurricane]].<ref>{{cite web|url=http://www.the-battle-of-britain.co.uk/pilots/Do-pilots.html|title=Edward Mortlock Donaldson|publisher=the-battle-of-britain.co.uk|accessdate=13 July 2009}}</ref> In their first engagement over France, they destroyed six enemy aircraft, shooting down many more in the following months including at the [[Battle of Dunkirk]].<ref name="Griffiths"/> For his leadership of the squadron during the battle and his personal tally of eleven kills, plus ten probable destructions, Donaldson was awarded the DSO.<ref name="RAFWeb"/><ref>{{cite web|url=https://docs.google.com/gview?a=v&q=cache:PBencm-m9R0J:www.london-gazette.co.uk/issues/34860/supplements/3251/page.pdf+Edward+Mortlock+Donaldson&hl=en&gl=uk|title=Supplement to the London Gazette|publisher=London Gazette|date=28 May 1940|accessdate=13 July 2009}}</ref>

In desperate need for pilots, the RAF choose to transfer Donaldson to the gunnery instructor school. Posted to Canada, Donaldson self-penned an RAF training booklet titled ''Notes on Air Gunnery and Air Fighting,'' which after the United States entered the war, provided the basis for his instruction.<ref name="DNW"/> As liaison to the [[US Army Air Force]],<ref name="RAFWeb"/> his booklet was replicated over 7,500 times, and helped teach USAAF gunnery instructors.<ref name="DNW"/>

On his return to England in 1944, he converted to jet aircraft and commanded the first operational [[Gloster Meteor]] squadron, at [[RAF Colerne]].<ref name="RAFWeb"/>

===Airspeed record===
During the Second World War, most of the pre-war airspeed records had been broken. The RAF decided to recapture the [[flight airspeed record]] with its new generation of jet aircraft, and set up a new High Speed Flight squadron. [[Group Captain]] Donaldson was selected to command the Air Speed Flight, established at the start of 1946. On 7 September 1946, he established a new official world record of {{convert|615.78|mph|km/h kn|abbr=on}} in a [[Gloster Meteor|Gloster Meteor F.4]] over [[Littlehampton]],<ref name="RAFWeb"/> although some unofficial [[Messerschmitt Me 262|Me-262]] and [[Messerschmitt Me 163 Komet|Me-163]] flights in the Second World War achieved higher speeds. As a result, he was awarded a [[Medal bar|Bar]] to his [[Air Force Cross (United Kingdom)|Air Force Cross]].<ref>{{cite web|url=http://google.com/search?q=cache:WlmF34ut0-kJ:www.london-gazette.co.uk/issues/37977/supplements/2603/page.pdf+Edward+Mortlock+Donaldson&cd=9&hl=en&ct=clnk&gl=uk&client=firefox-a|title=Supplement to the London Gazette, June 12, 1947|publisher=London Gazette|date=12 June 1947|accessdate=13 July 2009}}</ref>

===Later RAF career===
During the early 1950s, Donaldson served in West Germany and commanded [[RAF Fassberg]] and [[RAF Wunstorf]] airfields, gaining appointment to [[Commander of the Order of the British Empire]] in June 1953, and advancement to [[air commodore]] in July 1955 after attending the [[Joint Services Staff College (UK)|Joint Services Staff College]]. From 1956 to 1958 he served as Deputy Commander of Air Forces in the [[Arabian Peninsula]]. On return to England, his final appointment was as Commandant of the [[RAF Manby|RAF College]], [[Manby]].<ref name="DNW"/>

===Personal life===
Donaldson married Winifred Constant in 1936, and the couple had two daughters. After they were divorced in 1944, in the same year he married Estellee Holland, and the couple had one son. After they were divorced in 1956, he married Anne Sofie Stapleton in 1957, whom he divorced in 1982.<ref>{{cite web|url=http://www.unithistories.com/officers/RAF_officers_D01.html|title=Edward Mortlock Donaldson|publisher=unithistories.com|accessdate=14 July 2009}}</ref>

Donaldson retired as an air commodore in 1961, and became the Air Correspondent for ''[[The Daily Telegraph]],'' until 1979.<ref name="RAFWeb"/> He retired to his home in [[Selsey]], and died at the [[Royal Naval Hospital Haslar]] on 6 June 1992. Donaldson is buried at [[St Andrew's Church, Tangmere]].<ref name="DNW"/>

==Memorial==
Donaldson's "Star" Meteor is on display at the [[Tangmere Military Aviation Museum]], together with that of later 1953 record holder, Squadron Leader [[Neville Duke]], who flew a [[Hawker Hunter]] at {{convert|727|mph|km/h}}.

Donaldson lived at Iron Latch Cottage, Selsey, where a later [[blue plaque]] was placed on the beach at the bottom of Park Lane to mark the event. Donaldson has a second plaque at No. 86, Grafton Road, Selsey.

Donaldson's medals and flight books were sold at auction for £4,800 in June 2004.<ref name="DNW">{{cite web|url=http://www.dnw.co.uk/medals/auctionarchive/viewspecialcollections/itemdetail.lasso?itemid=41458|title=Sold by Order of the Sole Beneficiary of the Estate, Medals and Collections of the Late Air Commodore E. M. Donaldson, C.B., C.B.E., D.S.O., A.F.C.|publisher=Dix Noonan Webb|date=6 July 2004|accessdate=13 July 2009}}</ref>

==Service history==
{| class="wikitable"
|-
!Date
!Notes
|-
|26 June 1931
|Appointed to a Short Service Commission. Initial Officer Training, RAF Depot
|-
|11 July 1931
|U/T Pilot, No 2 FTS
|-
|20 June 1932
|Pilot, No 3 Sqn.
|-
|23 April 1936
|Supernumerary, RAF Depot.
|-
|22 March 1937
|Act Officer Commanding, No 72 Sqn.
|-
|26 July 1937
|Flight Commander, No 1 Sqn.
|-
|1937
|Recommended for AFC, turned down
|-
|29 March 1938
|Granted a Permanent Commission in the rank of Flight Lieutenant
|-
|5 May 1938
|Attended Instructor's Course, Central Flying School. (graded B)
|-
|Aug 1938
|QFI, No 7 FTS.
|-
|14 November 1938
|Officer Commanding, No 151 Sqn.
|-
|5 August 1940
|CFI, No 5 FTS.
|-
|''Unknown''
|Officer Commanding, ? School
|-
|1 Jun 1941
|Mentioned in Dispatches
|-
|30 September 1941
|Awarded AFC
|-
|1941
|Liaison Officer, USAAF (USA)
|-
|''Unknown''
|Supernumerary, Polish Wing, [[RAF Northolt]]
|-
|''Unknown''
|Group Captain, Fighter Control Unit, 2nd TAF.
|-
|1944
|Attended Empire Central Flying School
|-
|''Unknown''
|Officer Commanding, [[RAF Colerne]]
|-
|''Unknown''
|Officer Commanding [[RAF Milfield]]
|-
|Jul 1946
|Officer Commanding, RAF High Speed Flight
|-
|7 September 1946
|Breaks [[Flight airspeed record]], reaching {{convert|615.78|mph|km/h}} in a [[Gloster Meteor|Gloster Meteor F Mk4]] over [[Littlehampton]].<ref name="RAFWeb"/>
|-
|1947
|SASO, HQ No 12 Group
|-
|12 June 1947
|Awarded AFC Bar
|-
|15 February 1949
|Awarded Legion of Merit, USA
|-
|1951
|Officer Commanding, [[RAF Fassberg]]
|-
|''Unknown''
|Officer Commanding, [[RAF Wunsdorf]]
|-
|1 June 1953
|Awarded CBE
|-
|16 April 1954
|Director of Operational Training
|-
|11 December 1956
|Deputy Commander, HQ British Forces, Arabian Peninsula
|-
|12 November 1958
|AOC/Commandant, RAF Flying College
|-
|1, 1 January 1960
|Awarded Commander of the Bath in [[New Year Honours]]
|-
|21 March 1961
|Retired
|}

==Combat record==
''The following table is not complete in numbers or detail.''
{| class="wikitable"
|-
!  style="width:120px; text-align:center;"|Date
!  style="width:100px; text-align:center;"|Service
!  style="width:80px; text-align:center;"|Flying
!  style="width:170px; text-align:center;"|Kills
!  style="width:170px; text-align:center;"|Probables
!align=center|Notes
|-
|17 May 1940
|[[Royal Air Force]]
|[[Hawker Hurricane|Hurricane]]
|2 * [[Junkers Ju 87]]
|1 * Junkers Ju 87
|50miles east of [[Valenciennes]]
|-
|18 May 1940
|Royal Air Force
|Hurricane
|
|1 * [[Messerschmitt Bf 110]]
|Over [[Vitry-en-Artois]] airfield
|-
|22 May 1940
|Royal Air Force
|Hurricane
|1 * Junkers Ju 87
|1 * Junkers Ju 87
|Over [[Merville, Nord|Merville]]
|-
|29 May 1940
|Royal Air Force
|Hurricane
|1/2 * [[Junkers Ju 88]]
|
|
|-
|30 May 1940
|Royal Air Force
|Hurricane
|
|1 * Junkers Ju 88
|Gazetted for DSO
|-
|1 June 1940
|Royal Air Force
|Hurricane
|1 * Messerschmitt Bf 110
|
|[[Battle of Dunkirk]]
|-
|2 June 1940
|Royal Air Force
|Hurricane
|
|
|Squadron moved from France to [[RAF Tangmere]]
|-
|27 June 1940
|Royal Air Force
|Hurricane
|
|
|On return from France protecting squadron of [[Basil Embry]], shot down over [[English Channel]]
|-
|12 July 1940
|Royal Air Force
|Hurricane
|
|
|Commencement of the [[Battle of Britain]]. Shot down over England by [[Dornier Do 17]], lands [[RAF Martlesham Heath]]
|-
|14 July 1940
|Royal Air Force
|Hurricane
|1 * [[Messerschmitt Bf 109]]
|
|Over [[Dover Straits]]
|-
|26 July 1940
|Royal Air Force
|Hurricane
|
|
|Final flight of Battle of Britain
|-
|27 July 1940
|Royal Air Force
|
|
|
|Given leave, on return posted to No. 5 F.T.S. as Chief Flying Instructor
|-
!align=center|TOTALS
!
!
!5.5 kills
!4 probable
!
|}

==References==
{{Reflist|30em}}

== Further reading ==
* Thomas, Nick. (2008). ''RAF Top Gun: The Story of Battle of Britain Ace and World Air Speed Record Holder Air Cdre E.M. 'Teddy' Donaldson CB, CBE, DSO, AFC*''. LoM (USA) Pen & Sword Books ISBN 978-1-8441-5685-6

{{Authority control}}

{{DEFAULTSORT:Donaldson}}
[[Category:People from Negeri Sembilan]]
[[Category:People educated at King's School, Rochester]]
[[Category:People educated at Christ's Hospital]]
[[Category:McGill University alumni]]
[[Category:Royal Air Force officers]]
[[Category:The Few]]
[[Category:British World War II flying aces]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Companions of the Order of the Bath]]
[[Category:Foreign recipients of the Legion of Merit]]
[[Category:1912 births]]
[[Category:1992 deaths]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Britannia Trophy winners]]
[[Category:Royal Air Force pilots of World War II]]