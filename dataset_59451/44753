{{distinguish|Judicial review}}
{{multiple issues|
{{over-coverage|date=April 2011}}
{{refimprove|date=April 2010}}
}}

A '''law review''' (or '''law journal''') is a [[scholarly journal]] focusing on legal issues. In the US, law reviews are normally published by an organization of students at a [[law school]] or through a [[bar association]]. Outside of North America, law reviews are usually edited by senior academics/faculty.

Law reviews should not be confused with non-scholarly publications such as the ''[[New York Law Journal]]'' or ''[[The American Lawyer]]'', which are independent, professional newspapers and news-magazines that cover the daily practice of law (see [[legal periodical]]).

==Overview==
The primary function of a law review is to publish [[scholarship]] in the field of law. Law reviews publish lengthy, comprehensive treatments of subjects ("articles"), generally written by [[law professor]]s, [[judge]]s, or legal practitioners, as well as shorter pieces, commonly called "notes" and "comments," written by [[law student]] "members" of the law review.

Law review articles often express the thinking of specialists or experts with regard to problems with current law and potential solutions to those problems. Historically, law review articles have been influential in the development of the law; they have been frequently cited as persuasive authority by the courts in the United States.  For example, Justice [[Stanley Mosk]] of the [[Supreme Court of California]] admitted that he got the idea for [[market share liability]] from the ''[[Fordham Law Review]]'' comment cited extensively in the court's landmark decision in ''[[Sindell v. Abbott Laboratories]]'' (1980).<ref>Hon. Stanley Mosk, Oral History Interview (Berkeley: California State Archives Regional Oral History Office, 1998), 62.</ref>  However, in recent years, some have claimed that the traditional influence of law reviews is declining.<ref>[[Adam Liptak]]. [https://select.nytimes.com/2007/03/19/us/19bar.html When Rendering Decisions, Judges Are Finding Law Reviews Irrelevant]. ''[[The New York Times]]''. 19 March 2007.</ref>

Most major American law schools publish a law review (or "law journal"), generally dealing with all areas of law and named after the school, and some publish specialized reviews, dealing with a particular area of the law, in addition to or in place of the general law review, such as [[civil rights and civil liberties]], [[international law]], [[environmental law]], or [[human rights]]. There are also a small number of journals focusing on statutory, regulatory, and public policy issues. See [[List of law reviews in the United States]] for details.

In recent years, many law reviews have started to publish online-only content in addition to their respective print issues. They offer freely available pieces of short-form legal scholarship, analysis, and commentary. Further, some law journals have abandoned print entirely, instead choosing to publish all of their content only on the Internet.

==Editorial staff==

=== United States ===
{{Main category|American law journals}}
As law professor [[Erwin N. Griswold]] wrote of the ''Harvard Law Review'': "Some people are concerned that a major legal periodical in the United States is edited and managed by students. It is an unusual situation, but it started that way, and it developed mightily from its own strength".<ref>Erwin N. Griswold, [http://www.harvardlawreview.org/Centennial.shtml "The ''Harvard Law Review''&nbsp;– Glimpses of Its History as Seen by an Aficionado"] (1987).</ref> During the 1990s, the [[American Bar Association]] followed suit and began coordinating its own practitioner journals with law schools, courting student editorial bodies for publications including ''[[Administrative Law Review]]'', ''[[The International Lawyer]]'', ''[[Public Contract Law Journal]]'', and ''[[The Urban Lawyer]]''. Despite Griswold's confidence in student editors, criticism of this practice continues. In 2004, Judge [[Richard Posner]] wrote a critical account entitled "Against the Law Reviews" in the magazine ''Legal Affairs''.<ref>{{cite web|url=http://www.legalaffairs.org/issues/November-December-2004/review_posner_novdec04.msp|title=Legal Affairs|publisher=}}</ref> However, Posner also wrote that his own time as president of the ''Harvard Law Review'' represented a "Golden Age ... for student-edited law reviews".<ref>Posner, Richard A., “The Future of the Student-Edited Law Review” in Stan. L. Rev. Vol. 47, No. 6, (1995): 1133</ref>

=== Canada ===
{{Main category|Canadian law journals}}
In Canada, the fully student-run law reviews (without a Faculty editor-in-chief) include, in order of the frequency they are cited by the Supreme Court of Canada: the ''[[McGill Law Journal]]/Revue de droit de McGill'', the ''[[Queen's Law Journal]]'', the ''[[Alberta Law Review]]'', ''[[University of Toronto Faculty of Law Review]]'', the ''[[University of Ottawa Law Review]]'', and the ''[[University of British Columbia Law Review]]''. Membership requires demanding time commitments, and many editors move on to top clerkships, top articling or first year associate positions both inside and outside of Canada, or eventually join the legal professoriate in Canada. The country also has several specialized publications run entirely by students.

=== Europe ===
{{Main category|German law journals}}
Outside of North America, student-run law reviews are the exception rather than the norm. In Continental Europe law reviews are almost uniformly edited by academics. However, a small number of student-edited law reviews have recently sprung into existence in Germany (''Ad Legendum'', ''Bucerius Law Journal'', ''Freilaw Freiburg Law Students Journal'', ''[[Goettingen Journal of International Law]]'', ''Hanse Law Review'', [[Heidelberg Law Review]], ''Marburg Law Review''), the Netherlands ([[Groningen_Journal_of_International_Law|''Groningen Journal of International Law'']]) and the Czech Republic (''Common Law Review''). Two student-run publications have also been established in Italy: ''[[Bocconi Legal Papers]]'', adopting the format of a working paper series, as a way to complement&nbsp;– rather than compete with&nbsp;– peer-reviewed publications and offer scholars an additional round of feedback;<ref>Longobardi, F. & Russi, L. (2009), [http://www.germanlawjournal.com/index.php?pageID=11&artID=1152 A Tiny Heart Beating: Student-Edited Legal Periodicals in Good Ol' Europe], ''German Law Journal'' 10:7</ref> ''University of Bologna Law Review'', a double-blind peer reviewed law journal, run by University of Bologna, School of Law students, which follows The Bluebook: A Uniform System of Citation.<ref>{{cite web|url=https://bolognalawreview.unibo.it|title=University of Bologna Law Review|author=|publisher=Bolognalawreview.unibo.it|accessdate=29 August 2016}}</ref> 

==== Belgium ====

In Belgium, the oldest and most prominent student-edited law review is ''[[Jura Falconis]]''. It was founded by a group of students from the Law Faculty of the ''[[Katholieke Universiteit Leuven]]'' who, in 1964, conceived the idea of producing their own law journal grafted on the famous American law reviews. Since then, ''Jura Falconis'' has grown into a very solid and most unusual value in the Belgian legal literature.

==== France ====
The leading law reviews in France are those of [[Dalloz]] and LexisNexis.

==== United Kingdom ====
{{Main category|British law journals}}
Within the [[United Kingdom]], as in much of the Commonwealth outside of North America (a notable exception being Australia), all of the leading law reviews are edited and run by academics. The leading law reviews in the United Kingdom and the Commonwealth more generally are the ''[[Law Quarterly Review]]'' (first published 1885),<ref>[http://www.sweetandmaxwell.co.uk/Catalogue/ProductDetails.aspx?recordid=473 Official website of LQR]</ref> the ''[[Modern Law Review]]'' (first published 1937),<ref>[http://www.modernlawreview.org.uk Official website of MLR]</ref> the ''[[Cambridge Law Journal]]'' (first published 1973),<ref>[https://www.cambridge.org/core/journals/cambridge-law-journal Official website of CLJ]</ref> The ''[[Oxford Journal of Legal Studies]]'' (first published 1981)<ref>[https://academic.oup.com/ojls Official website of OJLS]</ref> and [[Legal Studies (law journal) |Legal Studies]] (first published 1981).<ref>[http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1748-121X Official website of Legal Studies]</ref> None is student-run or -edited.

The ''UCL Jurisprudence Review'' was the first student law review when it began publishing in 1994. Since then, the ''Birkbeck Law Review'', ''Bristol Law Review'', ''Cambridge Student Law Review'', ''Durham Law Review'', ''Edinburgh Student Law Review'', ''Exeter Student Law Review'', ''Warwick Student Law Review'', ''Southampton Student Law Review'', the ''UCL Human Rights Review'' and the ''Student Journal of Law'' have also emerged.

==== Italy ====
''[[Bocconi Legal Papers]]'' is a student-edited law journal in Italy. It is a project sponsored by [[Bocconi School of Law]] and is published by a group of students belonging to the same institution, under the supervision of several faculty advisors.

The ''[[University of Bologna Law Review]]''  is a student-run law journal published by the Department of Legal Studies of the [[University of Bologna]], and officially sponsored by [[Cleary Gottlieb Steen & Hamilton LLP]]. Its Advisory Board is composed of several scholars and practitioners from all over the world.

==== Ireland ====
{{Main category|Irish law journals}}
The main professionally edited law reviews in Ireland include the following:
*''Bar Review'' 
*''Commercial Law Practitioner''
*''Construction, Engineering and Energy Law Journal''
*''Conveyancing and Property Law Journal''
*''Dublin University Law Journal''
*''Hibernian Law Journal''
*''Irish Criminal Law Journal''
*''Irish Employment Law Journal''
*''Irish Planning and Environmental Law Journal''
*''Irish Probate Law Journal''
*''[[Irish Law Times]]''
*''Irish Journal of Family Law''
*''Irish Journal of Practice and Procedure''
*''Medico-Legal Journal of Ireland''
*''The Irish Jurist''

The leading student law reviews are the ''Dublin City University Law Review'', the ''[[Trinity College Law Review]]'', the ''[[UCD Law Review]]'', the ''Irish Student Law Review'' and the ''Cork Online Law Review''.

==== Nordic countries ====
In Iceland, ''Úlfljótur Law Review'', has been in publication since 1947. In 2007 it celebrated its 60th anniversary. Since its creation in 1947 it has been edited and run by students at the Department of Law, University of Iceland. ''Úlfljótur Law Review'' is the most senior of all academic journals still in publication at the university and held in great respect by Icelandic jurists and legal scholars.

In Finland, ''Helsinki Law Review'', edited by students at the University of Helsinki, has been active since 2007. Earlier, the University of Turku published ''Turku Law Journal'' from 1999 to 2003.

Sweden's first law review is ''Juridisk Publikation''. The first number of ''Juridisk Publikation'' was published in April 2009. It originated as a review by students from Stockholm University. It is now delivered to Swedish law students from all universities, as well as to most legal libraries in the country. ''Juridisk Publikation'' is edited by top students from the law schools in Lund, Stockholm Uppsala and Goteborg. The publication is anonymously peer reviewed by a board of leading Swedish legal practitioners and academics.

In Norway, the first student edited law review ''Jussens Venner'' was founded in 1952 by students Carsten Smith and Torkel Opsahl (both of whom later became distinguished academics). Occasionally it features peer-reviewed articles, but its editors are composed of one student from the Faculty of Law at the University of Oslo and one student from the Faculty of Law at the University of Bergen. Its articles are mainly related to the curriculum at these universities.

=== Argentina ===
In spite of some few exceptions, in Argentina almost all law reviews are run by publishing houses or law professors. In both cases, the involvement of students in the day to day creation of these reviews is fully narrowed. Among these few exceptions, it should be mentioned the case of ''[[Revista Lecciones y Ensayos]]'', a law review ran by students of the School of Law of the [[University of Buenos Aires]].<ref name=index>[http://www.derecho.uba.ar/publicaciones/lye/index.php Revista Lecciones y Ensayos] ''website of the University of Buenos Aires School of Law''.</ref>

=== Australia ===
{{Main category|Australian law journals}}
In Australia, the leading student-edited peer-reviewed academic law reviews are the ''[[Melbourne University Law Review]]'', ''[[Sydney Law Review]]'', ''[[Federal Law Review]]'', ''[[Monash University Law Review]]'' and [[UNSW Faculty of Law#Faculty publications|''UNSW Law Journal'']].

The ''Melbourne University Law Review'' regularly outperforms ''Sydney Law Review'' on impact, citation in journal and cases and combined rankings.<ref name="lawlib" /> These publications are among the most-cited law reviews by the High Court of Australia and among the most cited non-US reviews by US journals.<ref>{{cite web|url=http://lawlib.wlu.edu/LJ/index.aspx |title=Law Journals: Submissions and Ranking |publisher=Lawlib.wlu.edu |date=22 August 2011 |accessdate=9 February 2012}}</ref>

The top international law journal in Australia is the ''[[Melbourne Journal of International Law]]'', also a student-edited peer-reviewed academic law review. ''The Melbourne Journal of International Law'' is also considered to be more influential and prestigious than most generalist law reviews in Australia.<ref name="lawlib" />

=== Brazil ===
In Brazil, law reviews are usually run by academics as well, but there are efforts by students to change this; for example: ''University of Brasilia Law Students Review'' (re-established in 2007), the ''Review of the Academic Center Afonso Pena'' from the [[Federal University of Minas Gerais]] (published since 1996), and the ''Alethes Periodic'' from [[Federal University of Juiz de Fora]]. However, academics and official rankings usually refuse to evaluate student law reviews as "equals".{{citation needed|date=January 2015}} To pursue academic recognition by the Brazilian Ministry of Education, review bodies must include post-graduated and ranked academics, which prevents student law reviews to even be recognized or compared to other similar legal periodicals.

=== China ===

In China, there are law reviews run by academics, as well as law reviews run by students.

The ''China Law Journal'' is an attempt to create a legal publication, that is produced from all groups related to law, including lawyers, academics, students, members of the judiciary, procurators and anyone else in related fields with an interest in China.<ref name="lawlib">{{cite web|url=http://www.chinalawjournal.org|title=China Law Journal|publisher=}}</ref>

=== India ===
{{Main category|Indian law journals}}
Among academic law journals in India, the ''Journal of Indian Law Institute'' and the ''Delhi Law Review'' published by the Faculty of Law, [[University of Delhi]] since 1972 are most prominent and respected among Indian legal scholars and academicians.{{Citation needed|date=May 2011}} National Law Schools/Universities are now leading the law review publication field,{{Citation needed|date=April 2011}} with notable reviews{{According to whom|date=April 2011}} being the ''[[Nalsar University of Law|NALSAR Student Law Review]]'' and the ''[[National Law School of India Review]]'' and lawbright, eJournal.

=== Mexico ===
The ''[[Mexican Law Review]]'', the law review of the [[National Autonomous University of Mexico]], Mexico's preeminent university, is edited by professors and is therefore a closer cousin to peer-reviewed social science journals than to typical student-run law journals.

== Online legal research providers ==
Online legal research providers such as [[Westlaw]] and [[LexisNexis]] give users access to the complete text of most law reviews published beginning from the late 1980s. Another such service, [[Heinonline]], provides actual scans of the pages of law reviews going back to the 1850s.

==Student activity==
Membership on the law review staff is highly sought after by some law students, as it often has a significant impact on their subsequent careers as attorneys. Many [[U.S. federal judges]] and partners at the most prestigious [[law firms]] were members or editors of their school's law review. There are a number of reasons why journal membership is desired by some students:
* Some see the intense writing, research and editing experience as invaluable to the student's development as an attorney;
* Others see the selection process as helping differentiate the best and the brightest from an already strong group of law students.
At schools with more than one law review, membership on the main or flagship journal is normally considered more prestigious than membership on a specialty law journal. This is not the case at all schools, however. At many schools, the more prestigious journal is the specialty journal; a low-ranked general journal will rarely attract as much attention as a category-leading specialized journal. Often the best indicator is the age of the journal; a newer journal will rarely have the same clout with employers that the older journal has, even when the older journal is specialized. In any case, membership on any such journal is a valuable credential when searching out employment after law school.

The paths to membership vary from law school to law school, and also from journal to journal, but generally contain a few of the same basic elements. Most law reviews select members after their first year of studies either through a writing competition (often referred to as "writing on" to the law review), their first-year grades (referred to as "grading on" to the law review) or some combination thereof.<ref>Wes Henricksen, ''[http://www.cap-press.com/books/1826 Making Law Review: The Expert's Guide to Mastering the Write-On Competition]'' (2008).</ref> Most Canadian law reviews, however, do not take grades into considerations and cannot be submitted with the application. A number of schools will also grant membership to students who independently submit a publishable article. The write-on competition usually requires applicants to compose a written analysis of a specific legal topic, often a recent Supreme Court decision. The written submissions are often of a set length, and applicants are sometimes provided with some or all of the background research. Submissions normally are graded blindly, with submissions identified only by a number which the graders will not be able to connect to a particular applicant. A student who has been selected for law review membership is said to have "made the law review."

Secondary journals vary widely in their membership process. For example, at [[Yale Law School]], the only one of its nine journals that has a competitive membership process is the flagship ''Yale Law Journal''&nbsp;– all others are open to any Yale Law student who wishes to join. By contrast, other secondary journals may have their own separate membership competition, or may hold a joint competition with the main law review.

A law review's membership is normally divided into staff members and editors. On most law reviews, all 2Ls (second-year students) are staff members while some or all 3Ls (third-year students) serve as editors. 3Ls also typically fill the senior editorial staff positions, including senior articles editor, senior note & comment editor, senior managing editor, and, the most prestigious of all, editor-in-chief of the law review. (Upon graduation, the editor-in-chief of the law review can often expect to be highly recruited by the most prestigious law firms.) As members, students are normally expected to :
* Write a note or comment of publishable quality (although it need not actually be published), and to
* Edit and [[case citation|cite-check]] the articles that are being published by the law review, ensuring that references support what the author claims they support and that footnotes are in proper ''[[Bluebook]]'' format, depending on the publication's preference.
The editorial staff is normally responsible for reviewing and selecting articles for publication, managing the editing process, and assisting members in writing their notes and comments. Depending on the law school, students may receive academic credit for their work on the law review, although some journals are entirely extracurricular.

==History of law reviews==

===United States===
{{See also|List of law reviews in the United States}}
{{Refimprove section|date=April 2011}}
The ''[[University of Pennsylvania Law Review]]'' is the oldest law review in the U.S., having published continuously since 1852. Also among the oldest and most storied law review publications are the ''[[Albany Law Review]]'', successor to the ''Albany Law School Journal'', which began in 1875 and was the nation's first student-edited law review;<ref>{{cite web|url=http://www.albanylawreview.org/sub.php?id=10 |title=Albany Law School&nbsp;– Journal |publisher=Albanylawreview.org |date= |accessdate=9 February 2012}}; Jonathan Lippman, ''The New York Court of Appeals, Albany Law School, and The Albany Law Review: Institutions Dedicated to the Evolution of the Law in New York State'', 75 Alb. L. Rev. 9, 10 (2011/2012); Spencer M. Ritchie, ''The Journal's Journey: a History of the Mississippi Law Journal'', 81 Miss. L. J. 1527, 1528 n.7 (2012); Whit Pierce & Anne Reuben, ''The Law Review is Dead; Long Live the Law Review: A Closer Look at the Declining Judicial Citation of Legal Scholarship'', 45 Wake Forest L. Rev. 1185, 1188 n.17 (2010); Michael Closen & Robert Dzielak, ''The History and Influence of the Law Review Institution'', 30 Akron L. Rev. 15, 34 (1996); Michael Swygert & Jon Bruce, ''The Historical Origins, Founding, and Early Development of Student-Edited Law Reviews'', 36 Hastings L. J. 739, 764 (1986); Robert Emery, ''The Albany Law School Journal: The Only Surviving Copy'', 89 L. Lib. J. 463, 464 (1997).</ref> the ''[[Columbia Law Review]]'', successor to the ''Columbia Jurist'', beginning in 1885; the ''[[Harvard Law Review]]'', beginning in 1887; ''[[Yale Law Journal]]'', beginning in 1891; ''[[West Virginia Law Review]]'', beginning in 1894; and ''[[Penn State Law Review|Dickinson Law Review]]'', beginning in 1897. The first law review originating outside of the Northeast was the ''[[Michigan Law Review]]'', beginning in 1902; followed by the ''[[Northwestern University Law Review]]'', beginning in 1906; and the ''Kentucky Law Journal'', beginning in 1910. The ''[[California Law Review]]'', beginning in 1912, was the nation's first law review published west of Illinois.

==See also==
{{Main category|Law journals}}
* [[Bar journal]]

==References==
{{Reflist|30em}}

==External links==
* [http://faculty.law.pitt.edu/hibbitts/lastrev.htm Bernard Hibbitts, Last Writes? Re-assessing the Law Review in the Age of Cyberspace]
* [http://www.nyls.edu/library/research_tools_and_sources/current_awareness_sources/top_law_reviews_-_current_issues Current Issues of Top Law Reviews]
{{use dmy dates|date=March 2015}}

{{DEFAULTSORT:Law Review}}
[[Category:Law journals| ]]