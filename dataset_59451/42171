<!-- text begins below this table - scroll down to edit-->
{|{{Infobox Aircraft Begin
  |name = Airco DH.2
  |image = Airco D.H.2 ExCC.jpg
  |caption =
}}{{Infobox Aircraft Type
  |type = [[Fighter aircraft|Fighter]]
  |manufacturer = [[Airco]] 
  |designer =[[Geoffrey de Havilland]]
  |first flight = July 1915
  |introduced = 
  |retired =
  |status =
  |primary user = [[Royal Flying Corps]]
  |more users = 
  |produced =
  |number built =453
  |unit cost =
  |developed from = [[Airco DH.1]]
  |variants with their own articles =
}}
|}

The '''Airco DH.2''' was a single-seat [[biplane]] "[[Pusher configuration|pusher]]" aircraft which operated as a [[Fighter aircraft|fighter]] during the [[World War I|First World War]]. It was the second pusher design by [[Geoffrey de Havilland]] for [[Airco]], based on his earlier [[Airco DH.1|DH.1]] two-seater. The DH.2 was the first effectively armed British single-seat fighter and enabled [[Royal Flying Corps]] (RFC) pilots to counter the "[[Fokker Scourge]]" that had given the Germans the advantage in the air in late 1915. Until the British developed a [[synchronization gear|synchronisation gear]] to match the German system, pushers such as the DH.2 and the [[Royal Aircraft Factory F.E.2|F.E.2b]] carried the burden of fighting and escort duties.

==Design and development==
Early air combat over the [[Western Front (World War I)|Western Front]] indicated the need for a single-seat fighter with forward-firing armament. As no means of firing forward through the propeller of a tractor aeroplane was available to the British, Geoffrey de Havilland designed the DH.2 as a smaller, single-seat development of the earlier two-seat DH.1 pusher design. The DH.2 first flew in July 1915.<ref name="mason fighter p42">Mason 1992, p. 42.</ref>

The DH.2 was armed with a single [[.303 British|.303&nbsp;in (7.7&nbsp;mm)]] [[Lewis gun]] which was originally able to be positioned on one of three flexible mountings in the cockpit, with the pilot transferring the gun between mountings in flight at the same time as flying the aircraft. Once pilots learned that the best method of achieving a kill was to aim the aircraft rather than the gun, the machine gun was fixed in the forward-facing centre mount, although this was initially banned by higher authorities until a clip which fixed the gun in place, but could be released if required, was approved.<ref name="Goulding">Goulding 1986, p. 11.</ref> A clip was devised by Major [[Lanoe Hawker]], who also improved the gunsights and added a ring sight and an "aiming off model" that helped the gunner allow for [[Deflection (ballistics)|leading]] a target.<ref name="Guttmann">Guttman 2009, p. 31</ref>

The majority of DH.2s were fitted with the 100&nbsp;hp (75&nbsp;kW) [[Gnôme Monosoupape]] [[rotary engine]], but later models received the 110&nbsp;hp (82&nbsp;kW) [[Le Rhône 9J]].<ref>Sharpe 2000, p. 20.</ref>

Other sources advise the [[Gnôme Monosoupape]], nine-cylinder, air-cooled rotary, {{convert|100|hp|kW|abbr=on}} engine was retained in the DH. 2 design despite its tendency for shedding cylinders in midair; one DH.2 was fitted experimentally with a {{convert|110|hp|kW|abbr=on}} [[le Rhône 9J]].<ref>Munson 1968, p. 99.</ref>

A total of 453 DH.2s were produced by Airco.<ref name= "Airco DH-2">[http://www.classicfighters.co.nz/ac/dh2/index.shtml Airco DH-2]</ref>

==Operational service==
[[File:Airco DH2 2.jpg|thumb|Early DH.2 taking off from airfield at Beauvel, France]] 
After evaluation at [[Hendon Aerodrome|Hendon]] on 22 June 1915, the first DH.2 arrived in [[France]] for operational trials with [[No. 5 Squadron RAF|No. 5 RFC Squadron]] but was shot down and its pilot killed (although the DH.2 was recovered and repaired by the Germans).<ref name= "Airco DH-2"/> [[No. 24 Squadron RAF|No. 24 Squadron RFC]], the first [[Squadron (aviation)|squadron]] equipped with the DH.2 and the first complete squadron entirely equipped with single-seat fighters in the RFC, arrived in France in February 1916.<ref name="mason fighter p41">Mason 1992, p. 41.</ref> The DH.2 ultimately equipped seven fighter squadrons on the [[Western Front (World War I)|Western Front]]<ref name="Jackson DH p48"/> and quickly proved more than a match for the [[Fokker Eindecker]]. DH.2s were also heavily engaged during the [[Battle of the Somme]], No. 24 Squadron alone engaging in 774 combats and destroying 44 enemy machines.<ref name="mason fighter p41"/> The DH.2 had sensitive controls and at a time when service training for pilots in the RFC was very poor it initially had a high accident rate, gaining the nickname "The Spinning Incinerator",<ref name="Raleigh v1 p427-8">Raleigh 1922, pp. 427–428.</ref><ref>Funderburk,  p.  83</ref>  but as familiarity with the type increased, it was recognised as very manoeverable and relatively easy to fly.<ref>Cheesman 1960, p. 40.</ref> The rear-mounted rotary engine made the DH.2 easy to stall, but also made it highly maneuverable.<ref name="Guttmann"/>

The arrival at the front of more powerful German [[Tractor configuration|tractor]] biplane fighters such as the [[Halberstadt D.II]] and the [[Albatros D.I]], which appeared in September 1916, meant that the DH.2 was outclassed in turn. It remained in first line service in France, however, until No. 24 and [[No. 32 (The Royal) Squadron RAF|No. 32 Squadron RFC]] completed re-equipment with [[Airco DH.5]]s in June 1917, and a few remained in service on [[Macedonian front (World War I)|the Macedonian front]], “A” Flight of No. 47 Squadron and a joint R.F.C. / [[R.N.A.S.]] fighter squadron <ref name="Munson, p. 99">Munson, p. 99</ref> and X” Flight <ref name="Munson, p. 99"/> in [[Sinai and Palestine Campaign|Palestine]] until late autumn of that year. By this time the type was totally obsolete as a fighter, although it was used as an advanced trainer into 1918. DH.2s were progressively retired and at war's end no surviving airframes were retained.

In 1970, Walter M. Redfern from [[Seattle]], [[Washington (U.S. state)|Washington]] built a replica DH.2 powered by a [[Kinner]] 125-150&nbsp;hp engine and subsequently, Redfern sold plans to home builders. Currently a number of the DH.2 replicas are flying worldwide.<ref>[http://www.aircraftworlddirectory.com/civil/r/redferndh2.htm "Redfern DH-2."] ''aircraftworlddirectory.com''. Retrieved: 10 January 2010.</ref>

==DH.2 Aces==
Distinguished pilots of the DH.2 included [[Victoria Cross]] winner [[Lanoe Hawker]] (seven victories, though none in the DH.2), who was the first commander of No. 24 Squadron and [[Alan Wilkinson (aviator)|Alan Wilkinson]]. The commander of [[No. 32 Squadron RAF|No. 32 Squadron]], [[Lionel Rees]] won the Victoria Cross flying the D.H.2 for single-handedly attacking a formation of ten German two-seaters on 1 July 1916, destroying two.<ref name="Jackson DH p48">Jackson 1987, p. 48.</ref> [[James McCudden]] became an ace in DH.2s to start his career as the [[British Empire]]'s fourth-ranking ace of the war.<ref name="Guttman 2009, p. 91">Guttman 2009, p. 91.</ref> German ace and tactician [[Oswald Boelcke]] was killed during a [[dogfight]] with No. 24 Squadron DH.2s due to a collision with one of his own wingmen, [[Erwin Böhme]]. Fourteen aces scored five or more aerial victories using the DH.2; many went on to further success in later types also.

[[Lanoe George Hawker]] V.C., [[Distinguished Service Order|D.S.O.]], commanding officer of No. 24 Squadron flying a DH. 2 was shot down by [[Manfred von Richthofen]]  of [[Jasta 2]] flying an [[Albatros D.II]].

{|class="wikitable"
|+'''DH.2 Aces'''<ref name="Guttman 2009, p. 91"/>
!Pilot
|victories
|-
|[[Patrick Anthony Langan-Byrne]]
|10 
|-
| [[Alan Wilkinson (aviator)|Alan Wilkinson]] 
| 10 
|-
| [[Selden Long]] 
| 9 
|-
| [[Arthur Gerald Knight]] 
| 8 
|-
| [[Eric C. Pashley]]
|  8 
|-
| [[John Oliver Andrews]] 
|  7
|-
| [[Sidney Cowan]] 
| 7 
|-
| [[Hubert Jones]] 
|7 
|-
| [[William Curphey]]
| 6 
|-
| [[Stanley Cockerell]] 
| 5 
|-
| [[Henry Evans (RFC officer)|Henry Evans]] 
| 5 
|-
| [[James McCudden]] 
|  5 
|-
| [[Robert Saundby]]
|  5
|-
| [[Harry Wood (aviator)|Harry Wood]] 
| 5 
|}

==Operators==
;{{UK}}
*[[Royal Flying Corps]]
**[[No. 5 Squadron RAF|No. 5 Squadron RFC]]
**[[No. 11 Squadron RAF|No. 11 Squadron RFC]]
**[[No. 17 Squadron RAF|No. 17 Squadron RFC]]
**[[No. 18 Squadron RAF|No. 18 Squadron RFC]]
**[[No. 24 Squadron RAF|No. 24 Squadron RFC]]
**[[No. 29 Squadron RAF|No. 29 Squadron RFC]]
**[[No. 32 Squadron RAF|No. 32 Squadron RFC]]
**[[No. 41 Squadron RAF|No. 41 Squadron RFC]]
**[[No. 47 Squadron RAF|No. 47 Squadron RFC]]
**[[No. 111 Squadron RAF|No. 111 Squadron RFC]]

==Specifications (DH.2)==
[[File:Airco D.H.2 British First World War single seat fighter rigging drawing.jpg|thumb|Airco DH.2 drawing]]
{{Aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=''Warplanes of the First World War - Fighters Volume One''<ref name = "brucev1">Bruce 1965, p. 128.</ref>
|crew=one
|capacity=
|payload main=
|payload alt=
|length main= 25 ft 2½ in 
|length alt=7.69 m
|span main=28 ft 3 in 
|span alt=8.61 m
|span sweep=
|height main=9 ft 6½ in 
|height alt=2.91 m
|area main= 249 ft² 
|area alt= 23.13 m²
|airfoil=
|empty weight main= 942 lb 
|empty weight alt= 428 kg
|loaded weight main= 
|loaded weight alt= 
|useful load main= 
|useful load alt= 
|max takeoff weight main=1,441 lb  
|max takeoff weight alt= 654 kg
|more general=
|afterburning thrust alt= 
|engine (prop)=[[Gnôme Monosoupape]] 
|type of prop=[[rotary engine]] 
|number of props=1
|power main= 100 hp 
|power alt=75 kW
|power original=
|max speed main=93 mph  
|max speed alt= 150 km/h
|max speed more= at sea level
|cruise speed main= 
|cruise speed alt= 
|stall speed main= 
|stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main= 250 mi 
|range alt=400 km
|ceiling main= 14,000 ft 
|ceiling alt= 4,265 m
|climb rate main= 545 ft/min 
|climb rate alt= 166 m/min
|loading main= 5.79 lb/ft²
|loading alt= 28.3 kg/m²
|thrust/weight=
|power/mass main= 0.069 hp/lb
|power/mass alt= 110 W/kg
|more performance=*'''Endurance''' 2¾ hours<br />
*'''Climb to {{convert|5000|ft|m|abbr=on}}''' 24 minutes 45 seconds
|armament=1 × .303 in (7.7 mm) [[Lewis gun]] using 47-round [[drum magazine]]s
|avionics=
}}

==See also==
{{Aircontent
|related=
* [[Airco DH.1]]
* [[Redfern DH-2]], a replica of the DH.2 for amateur construction
|similar aircraft=
* [[Vickers FB.5]]
* [[Royal Aircraft Factory FE.2|RAF F.E.2b]]
* [[Royal Aircraft Factory F.E.8|RAF F.E.8]]
* [[Breguet 5]]
}}

==References==
;Notes
{{reflist|30em}}

;Bibliography
{{refbegin}} 
* Bruce, J.M. ''Warplanes of the First World War - Fighters Volume One''. London: MacDonald & Co., 1965.
* Cheesman, E.F., ed. ''Fighter Aircraft of the 1914-1918 War''. Herts, UK: Harleyford, 1960.
*Funderburk, Thomas R. ''The Fighters: The Men and Machines of the First Air War''. New York: Grosset & Dunlap, 1962.
* Goulding, James. ''Interceptor: RAF Single Seat Multi-Gun Fighters''. London: Ian Allen Ltd., 1986. ISBN 0-7110-1583-X.
* Guttman, Jon. ''Pusher Aces of World War 1''. Jon Guttman. Osprey Pub Co, 2009. ISBN 1-84603-417-5, ISBN 978-1-84603-417-6.
* Jackson, A.J. ''De Havilland Aircraft since 1909''. London: Putnam, Third edition, 1987. ISBN 0-85177-802-X.
* Mason, Francis K. ''The British Fighter since 1912''. Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* Miller, James F. "DH 2 vs Albatros D I/D II - Western Front 1916 (Osprey Duel ; 42)". Oxford, UK: Osprey Publishing, 2012. ISBN 978-1-84908-704-9
*Munson, Kenneth. ''Fighters Attack and Training Aircraft 1914-1919''. New York: Macmillan, 1968.
* Raleigh, Walter. ''The War In The Air: Being the Story of the part played in the Great War by the Royal Air Force, Vol I''. Oxford, UK: Clarendon Press, First edition 1922, 2002 (reprint). ISBN 978-1-84342-412-3.
* Sharpe, Michael. ''Biplanes, Triplanes, and Seaplanes''. London: Friedman/Fairfax Books, 2000. ISBN 1-58663-300-7.
{{refend}}

==External links==
{{Commons category|Airco DH.2}}
*[http://www.theaerodrome.com/aircraft/gbritain/airco_dh2.php Airco D.H.2] at theaerodrome.com

{{De Havilland aircraft}}
{{wwi-air}}

{{DEFAULTSORT:Airco DH.002}}
[[Category:Airco aircraft|DH.002]]
[[Category:British fighter aircraft 1910–1919]]
[[Category:Single-engined pusher aircraft]]
[[Category:Biplanes]]
[[Category:Rotary-engined aircraft]]
[[Category:Military aircraft of World War I]]