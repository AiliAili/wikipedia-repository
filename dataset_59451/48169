{{Infobox Judge
|honorific-prefix    =
|name                = Richard M. Givan
|honorific-suffix    =
|image               =
|imagesize           =
|caption             =
| office              = [[Indiana Supreme Court|Indiana Supreme Court Justice]]
| term_start          = January 6, 1969
| term_end            = December 31, 1994
| nominator           = 
| appointer           = 
| predecessor         = David Lewis<ref>{{cite web|url= http://www.indianacourts.org/Justices/bychron.aspx|title=Supreme Court Justices|accessdate=28 January 2009}}</ref>
| successor           = Myra Selby
|birth_date          = 
|birth_place         = 
|death_date          = 
|death_place         = 
|restingplace        =
|restingplacecoordinates =
|party               =
|children            =
|alma_mater          = 
|occupation          = 
|religion            =
|signature           =
|website             =
|footnotes           =
}}
{{Infobox Judge
|honorific-prefix    =
|name                = 
|honorific-suffix    =
|image               =
|imagesize           =
|caption             =
|office              = [[Indiana Supreme Court|Indiana Supreme Court Chief Justice]]
|term_start          = 1974
|term_end            = March, 1987
|nominator           =
|appointer           =
|predecessor         = Norman Arterburn <ref>{{cite web|url= http://www.in.gov/judiciary/citc/justice-bios/arterburn.html|title=Indiana Supreme Court Justice Biographies|accessdate=24 July 2009}}</ref>
|successor           = Randall T. Shepard<ref>{{cite web|url= http://www.indianacourts.org/Justices/bychron.aspx|title=Supreme Court Justices|accessdate=23 July 2009}}</ref>
|birth_date          = {{birth date|1921|6|7}}
|birth_place         = [[Indianapolis, Indiana]]
|death_date          = {{death date and age|2009|7|22|1921|6|7}}
|death_place         = [[Plainfield, Indiana]]
|restingplace        =
|restingplacecoordinates =
|party               =
|children            =
|alma_mater          = [[Indiana University]]
|occupation          = [[Lawyer]]<br>[[Judge]]
|religion            =
|signature           =
|website             =
|footnotes           =
}}
'''Richard M. Givan''' (1921–2009) served as the 96th Justice of the [[Indiana Supreme Court]] from January 6, 1969 until his retirement December 31, 1994. He served as Chief Justice from 1974 until March 1987.
  
==Early life==
Givan was born June 7, 1921 in [[Indianapolis]]. He graduated from [[Decatur Central High School]] in Indianapolis in 1939. He received an LL.B. from [[Indiana University]] in 1951, and was admitted to the Indiana bar in 1952.<ref>{{cite web|url= http://www.in.gov/judiciary/citc/justice-bios/givan.html|title=Indiana Supreme Court Justice Biographies|accessdate=28 January 2009}}</ref>

While he was a law student, he was assistant librarian for the Indiana Supreme Court in 1949, and then became a research assistant for the Indiana Supreme Court. He was appointed deputy [[public defender]] of Indiana after graduation from law school and served in that post until 1954.

==Career==
From 1954 to 1966, he was Assistant Attorney General of Indiana, pleading cases before both the Indiana and [[Supreme Court of the United States|Supreme Court]]s. In 1967, he was a representative and ranking member of the Judiciary Committee in the Indiana Legislature. He was elected to the Indiana Supreme Court in 1968 and served continuously until his retirement in December 1994. He was also Chairman of the Board of Directors of the Indiana Judicial Conference from 1974 to 1987, served on the Board of Managers of the Indiana Judges Association from 1975 to 1987, and became an Indiana Judicial College Graduate in 1989.

In addition to his legal career, Givan served as a pilot in the U.S. [[United States Army Air Corps|Army Air Corps]] during World War II and was later a flight instructor with the Air Corps Reservists.

==Controversy==
In 1984, a group known as "Remember Baby Doe - Retire Judge Givan Committee" sought to ouster Givan from his position as Chief Justice after the Indiana Supreme Court refused to hear a case regarding the death of an infant with [[Down syndrome]]. The group placed several advertisements in Indiana newspapers and asked voters to oppose Givan in the November 6 election. Givan denied claims that the decision established "quality of life" as a judicial criterion. Givan explained that the Supreme Court was only asked to determine if the original court had jurisdiction over the matter.<ref>{{cite news|url=https://query.nytimes.com/gst/fullpage.html?sec=health&res=9401EFD9123BF937A35753C1A962948260|title=GROUP SEEKS OUSTER OF JUDGE IN 'BABY DOE' CASE|author=AP|accessdate=28 January 2009 | work=The New York Times | date=1984-10-04}}</ref>

==Death==
Richard Givan died on July 21, 2009, in [[Plainfield, Indiana]].  On July 23, 2009, Indiana Governor [[Mitch Daniels]] ordered all flags at the Statehouse and across the state be flown at [[half-mast]] as a tribute to Chief Justice Givan.  The flags were to remain at half-mast through July 28, 2009, the date of Givan's funeral.<ref>http://www.wishtv.com/dpp/news/indiana/Flags_lowered_for_Ind_chief_justice_20090723</ref>

== References ==
{{reflist}}

{{s-start}}
{{succession box
| before= Norman Arterburn
| title= [[Indiana Supreme Court|Chief Justice of the Indiana Supreme Court]]
| years= 1974&ndash;1987
| after= [[Randall T. Shepard]]}}
{{s-end}}

{{DEFAULTSORT:Givan, Richard M.}}
[[Category:1921 births]]
[[Category:2009 deaths]]
[[Category:Politicians from Indianapolis]]
[[Category:Members of the Indiana House of Representatives]]
[[Category:Chief Justices of the Indiana Supreme Court]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots of World War II]]
[[Category:American military personnel of World War II]]
[[Category:20th-century American judges]]