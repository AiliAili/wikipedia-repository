{{Infobox journal
| title = Group Processes & Intergroup Relations
| cover = [[File:Group Processes & Intergroup Relations front cover.jpg]]
| editor = [[Dominic Abrams]], Michael A Hogg 
| discipline = [[Social psychology]]
| former_names =
| abbreviation = Group Process. Intergr. Relat.
| publisher = [[SAGE Publications]]
| country =
| frequency = Bimonthly
| history = 1998-present
| openaccess =
| license =
| impact = 1.528
| impact-year = 2012
| website = http://gpi.sagepub.com/
| link1 = http://gpi.sagepub.com/content/current
| link1-name = Online access
| link2 = http://gpi.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 477354069
| LCCN =
| CODEN =
| ISSN = 1368-4302
| eISSN = 1461-7188
}}
'''''Group Processes & Intergroup Relations''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] that covers research in the field of [[social psychology]], including organizational and management sciences, [[political science]], [[sociology]], language and communication, cross cultural psychology, and [[international relations]], among others. The journal's [[editors-in-chief]] are Dominic Abrams ([[University of Kent]]) and Michael Hogg ([[Claremont Graduate University]]). It was established in 1998 and is currently published by [[SAGE Publications]].

== Abstracting and indexing ==
''Group Processes & Intergroup Relations'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 1.528, ranking it 28th out of 60 in the category "Psychology, Social".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Psychology, Social |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

==External links==
* {{Official website|1=http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal200785}}

{{DEFAULTSORT:Group Processes and Intergroup Relations}}
[[Category:Sociology journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1998]]