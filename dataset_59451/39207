{{for|the secret Romanian fraternal society|Călușari}}
{{Good article}}
{{Infobox television episode
| title = The Calusari
| series = [[The X-Files]]
| season = [[The X-Files (season 2)|2]]
| episode = 21
| airdate = April 14, 1995
| production = 2X21
| writer = Sara Charno
| director = [[Mike Vejar]]
| length = 44 minutes
| guests =  
*[[Helene Clarkson]] as Maggie Holvey
*Joel Palmer as Charlie and Michael Holvey
*[[Lilyan Chauvin]] as Golda
*[[Kay E. Kuter]] as the Head Calusari
*Ric Reid as Steve Holvey
*[[Christine Willes]] as Agent Karen Kosseff
*[[Bill Dow]] as [[List of minor The X-Files characters#Dr. Charles (Chuck) Burks|Dr. Charles Burks]]<ref name=plot/>
| prev = [[Humbug (The X-Files)|Humbug]]
| next = [[F. Emasculata]]
| episode_list = [[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}

"'''The Căluşari'''" is the twenty-first episode of the [[The X-Files (season 2)|second season]] of the American [[science fiction on television|science fiction]] television series ''[[The X-Files]]''. It originally aired on the [[Fox Broadcasting Company|Fox]] network on April 14, 1995. It was written by Sara B. Charno and directed by [[Michael Vejar]]. "The Căluşari" is a [[List of Monster-of-the-Week characters in The X-Files|"Monster-of-the-Week"]] story, unconnected to the series' wider [[Mythology of The X-Files|mythology]], or fictional history. It earned a [[Nielsen rating|Nielsen household rating]] of 8.3, being watched by 7.9 million households in its initial broadcast. Due to perceived inconsistencies in the plot, "The Căluşari" received mixed reviews from television critics.

The show centers on [[Federal Bureau of Investigation|FBI]] special agents [[Fox Mulder]] ([[David Duchovny]]) and [[Dana Scully]] ([[Gillian Anderson]]) who work on cases linked to the paranormal, called [[X-File]]s. In this episode, a photograph taken just before the death of a two-year-old boy yields evidence of some supernatural intervention which piques Mulder and Scully's curiosity. When another death in the family occurs, the grandmother of the remaining child requests the aid of some Romanian ritualists, named the [[Căluşari|Calusari]], in order to cleanse the home of evil.

The script for "The Căluşari" was inspired by Charno's experience as a doctor of [[Eastern medicine]]. The inspiration for the entry came from an idea series creator [[Chris Carter (screenwriter)|Chris Carter]] had involving someone getting hanged with a garage-door opener. Because "The Căluşari" was heavy in terms of violence, Fox's standards and practices department took issues with several scenes. In addition, Carter re-cut the episode after it was completed in order to make it scarier.

== Plot ==

In Murray, [[Virginia]], the Holvey family visits a local [[amusement park]]. When the youngest child, Teddy, lets his balloon fly away, his father, Steve, gives him the balloon that belongs to his older brother, Charlie (Joel Palmer). When the boys' mother, Maggie ([[Helene Clarkson]]), is in the bathroom, the strap in Teddy's [[stroller]] comes undone. Teddy follows the balloon floating under its own power out of the restroom and onto the tracks of the park's tour train, leading to him getting killed by the train. Charlie is the only member of the Holvey family not to grieve Teddy's death at the scene.

Three months later, [[Fox Mulder]] ([[David Duchovny]]) shows [[Dana Scully]] ([[Gillian Anderson]]) a photo taken moments before Teddy's death, showing that the balloon moved horizontally against the wind. [[List of minor The X-Files characters#Dr. Charles .28Chuck.29 Burks|Chuck Burks]] ([[Bill Dow]]), a digital photo expert, uses software to uncover evidence of [[electromagnetism|electromagnetic]] disturbances in the shape of a child, holding the balloon. During a visit by the agents, the Holveys adamantly dispute Mulder's theory that Teddy was led onto the tracks. Scully sees Golda ([[Lilyan Chauvin]]), Maggie's elderly [[Romania]]n mother, drawing a [[swastika]] on Charlie’s hand. Scully hypothesizes that the Holvey children may be victims of [[Munchausen by proxy]], perpetrated by their grandmother. At a meeting with Steve Holvey, he explains that Golda was against his marriage to Maggie, and that strange things began to happen when Teddy was born and Golda came to live with the family. Steve hints that Golda might be hurting Charlie, leading Scully to suggest that they visit a [[social worker]] named Karen Kosseff ([[Christine Willes]]). While preparing to take Charlie to a session with Kosseff, Steve's tie is caught in the seemingly-malfunctioning garage door, strangling and hanging him. Charlie, having been mysteriously locked in the car, starts crying over his father's death.

Investigating Steve's death, the police find evidence of the ritualistic sacrifices in Golda's room. Mulder finds a film of fine dust in the garage, which Chuck identifies it as [[vibhuti]], a residual sign of spiritual energy. Golda and three elderly [[Căluşari]] mystics conduct a ritual in her room. Meanwhile, during Charlie's appointment with Kosseff, the child goes into convulsions. Kosseff and Maggie see smoke coming from under Golda's door, coming across their ritual. Maggie is horrified, and forces the old men to leave the house. However, Golda grabs Charlie and pulls him into her room in an attempt to complete the ritual. However, Charlie quickly gains the upper hand and brings a pair of dead [[chicken]]s back to life and makes them kill her.

When Kosseff asks Charlie about the incident, he insists that he was not in his grandmother's room, and that it was a boy named Michael. Maggie is terrified at the claim, explaining to the agents that Michael was Charlie’s [[Stillbirth|stillborn]] twin, about whom she and Steve agreed to never tell Charlie. Maggie also explains that Golda had told the parents that a ritual should be performed to separate the spirits of the twins, but Steve refused. Charlie has another seizure and is hospitalized. Michael knocks out the nurse and afterward convinces Maggie, by pretending to be Charlie, to take him home. Scully sees them leaving, and checks on Charlie. They find the nurse and Charlie still in the hospital room. Mulder, now convinced that Michael's spirit is behind the killings, sends Scully to the Holvey residence to stop him.

Maggie tries to complete her mother's ritual, but Michael tries to intervene. Back at the hospital, Mulder joins the Căluşari as they perform an [[exorcism]] on Charlie. As Mulder helps with the ritual, Scully arrives at the Holvey house, and finds Maggie being attacked by Michael. Scully is tossed across the room by an unseen force. Just as Michael is about to stab Scully, the exorcism ends, and Michael's spirit disappears, sparing both Scully and Maggie's lives.<ref name=plot>Lowry, pp. 213–215</ref> Maggie returns to the hospital and is reunited with Charlie. Before the agents leave, the head elder of the Căluşari says it's over for the time being and cautiously forewarns Mulder that "it knows you."

==Production==
[[File:Indian Swastika.svg|thumb|right|150px|Golda draws a left-facing [[swastika]] on Charlie's hand, a protective symbol in many Eastern religions.]]
The episode was written by Sara Charno and directed by [[Mike Vejar]].<ref name="DVD"/> Before becoming a writer, Charno had been a doctor of [[Eastern medicine]]. According to writer [[Frank Spotnitz]], her "esoteric knowledge that none of the rest of [the writers] had about all kinds of things" was put to use in the script for "The Calusari".<ref name=bb/> The episode was based largely on an idea that series creator [[Chris Carter (screenwriter)|Chris Carter]] had; his thought revolved around a "garage-door opener hanging".<ref name=prodinfo/> When Charlie stands over his grandmother and begins speaking in Romanian, he utters the words "You are too late to stop us."<ref name=prodinfo/> [[Christine Willes]], who plays the part of Agent Kosseff, reprises her role; she originally appeared in the earlier episode "[[Irresistible (The X-Files)|Irresistible]]".<ref name=prodinfo/>

During production of the episode, the producers "agonized" over the teaser—due to the fact that it featured the death of a small child—as well as the darkness of the entire episode. Fox's standards and practices department took issues with Steve's strangulation scene; in the end, the sequence was left in the episode, but Steve's face was obscured to "soften the impact".<ref name=prodinfo>Lowry, p. 215</ref> Although the episode's filming went along smoothly, the final cut "didn't pass muster".<ref name=bb/> Spotnitz explained that Carter "spent a lot of time in the editing room trying to figure out how to make this more terrifying."<ref name=bb/> Spotnitz later noted that Carter's dedication impacted his work ethic and proved that something could be so "much better […] if you didn't give up."<ref name=bb>Hurwitz and Knowles, p. 63</ref>

== Broadcast and reception ==

"The Calusari" originally aired on the [[Fox Broadcasting Company|Fox]] network on April&nbsp;14,&nbsp;1995, and was first broadcast in the United Kingdom on [[BBC One]] on February 6, 1996.<ref name="DVD">{{cite DVD notes |title=The X-Files: The Complete Second Season |titlelink=The X-Files (season 2) |origyear=1994–95 |others=[[David Nutter]], et al |type=booklet |publisher=[[Fox Broadcasting Company|Fox]]}}</ref> The episode earned a [[Nielsen rating|Nielsen household rating]] of 8.3 with a 16 share, meaning that roughly 8.3 percent of all television-equipped households, and 16&nbsp;percent of households watching TV, were tuned in to the episode.<ref name="comp"/> A total of 7.9&nbsp;million households watched this episode during its original airing.<ref name="comp">Lowry, p. 248</ref> "The Calusari" is the only episode of the series to have received an explicit rating of "18" in the United Kingdom by the [[BBFC]] for "occasional strong horror" and themes involving "demonic possession".<ref>{{cite web| url=http://www.bbfc.co.uk/website/Classified.nsf/0/8D701FE5C5F6B7ED8025660B00302B30?OpenDocument| title=The X Files – The Calusari rated 18 by the BBFC| publisher=[[British Board of Film Classification]]| accessdate=19 December 2008}}</ref><ref>{{cite web|title=Search << British Board of Film Classification|url=http://www.bbfc.co.uk/search/?searchwhere=db&q=x+files|publisher=British Board of Film Classification|accessdate=24 July 2012}}</ref>

"The Calusari" received mixed reviews, with critics citing inconsistencies in the plot as the main detractions. ''[[Entertainment Weekly]]'' gave the episode a "B–" rating, calling it "an ''[[The Exorcist (film)|Exorcist]]''/''[[The Omen|Omen]]'' rip-off, but a classy one".<ref>{{cite journal|url=http://www.ew.com/ew/article/0,,295179_4,00.html|title=X Cyclopedia: The Ultimate Episode Guide, Season 2|work=[[Entertainment Weekly]]|publisher=[[Time Inc.|Time Inc]]|date=29 November 1996|accessdate=16 February 2012}}</ref> Todd VanDerWerff of ''[[The A.V. Club]]'' gave it a "C+", writing that it was "an episode with a lot of great and spooky moments", but "a messy, chaotic story that could have been much better developed, and too many things that happen [...] just because the writers thought it would be cool if they happened".<ref name=av/> However, while he was "not sure everything hangs together" and he wished for more backstory, VanDerWerff did praise some "really great moments", particularly the opening teaser.<ref name=av>{{cite web|first=Todd|last=VanDerrWerff|url=http://www.avclub.com/articles/dd-kalmhumbugthe-calusari,42292/|title=''The X-Files'': "Død Kalm"/"Humbug"/"The Calusari"|work=[[The A.V. Club]]|publisher=[[The Onion]]|date=20 June 2010|accessdate=16 February 2012}}</ref> John Keegan from Critical Myth, while calling the episode "a mixed bag", awarded it a 7 out of 10.<ref name=crit/> He praised the entry's "fascinating implications [about] the mythology hidden within the events depicted", and noted that it was "well directed and acted".<ref name=crit/> Despite this, he was more critical of the episode's plot and wrote that there were "clear logical flaws [...] and the subject matter can be disturbing. This is an episode that falls heavily to subjective interpretation."<ref name=crit>{{cite web|last=Keegan|first=John|title=The Calusari|url=http://www.entil2001.com/series/x-files/reviews/season2/2-21.html|publisher=Critical Myth|accessdate=23 July 2012}}</ref> [[Robert Shearman]] and [[Lars Pearson]], in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', gave the episode a largely negative review and rated it one-and-a-half stars out of five. The two called it a "pale retread of ''The Exorcist''" and noted that many of the episode's elements, like the chicken-sacrificing grandmother and the Calusari members, were "tremendously crass".<ref name=shear/> Shearman and Pearson, however, did enjoy the episode's dialogue, praising one scene in particular where the spirit of Michael torments his mother by asking to be taken to the amusement park and ride the train that killed his younger brother. Regardless, however, the duo concluded that "there's something stale and pointless at [the episode's] heart."<ref name=shear>Shearman and Pearson, pp. 50–51</ref>

== References ==
;Footnotes
{{reflist|2}}
;Bibliography
*{{cite book |author1=Hurwitz, Matt  |author2=Knowles, Chris  |lastauthoramp=yes | title = The Complete X-Files: Behind the Series the Myths and the Movies | location = New York, US | publisher = Insight Editions | year = 2008 | isbn = 1933784725 }}
*{{Cite book |title=The Truth is Out There: The Official Guide to the X-Files |first=Brian |last=Lowry |publisher=Harper Prism |year=1995 |isbn=0061053309 }}
*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=097594469X}}

== External links ==
{{wikiquote|The_X-Files|TXF Season 2}}
*[https://web.archive.org/web/20010413100917/http://www.thexfiles.com/episodes/season2/2x21.html "The Calusari"] on TheXFiles.com
* {{imdb episode|0751222}}
* {{tv.com episode|the-xfiles/the-calusari-535}}

{{TXF episodes|2}}

{{DEFAULTSORT:Calusari, The}}
[[Category:1995 American television episodes]]
[[Category:Exorcism in fiction]]
[[Category:Fictional representations of Romani people]]
[[Category:Maryland in fiction]]
[[Category:The X-Files (season 2) episodes]]
[[Category:Virginia in fiction]]
[[Category:Washington, D.C. in fiction]]