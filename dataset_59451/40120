{{Infobox church
| name = All Saints Church, Acton
| fullname = 
| image = File:All Saints church, Acton, Suffolk - geograph.org.uk - 151409.jpg
| imagesize = 
| imagealt = 
| landscape = 
| caption = 
| location = Melford Road, Acton, Sudbury, Suffolk, CO10 0BA
| country = England
| coordinates = 
| osgraw = TL8922345230
| denomination = [[Anglican]]
| churchmanship = Central Anglican
| membership = 
| attendance = 
| website = 
| former name = 
| bull date = 
| founded date = 
| founder = 
| dedication = 
| dedicated date = 
| consecrated date = 
| cult = 
| relics = 
| events = 
| past bishop = 
| people = 
| status = [[Parish church]]
| functional status = 
| heritage designation = Grade I
| designated date = 23 March 1961
| architect = Whitworth Co-partnership
| architectural type = [[Church (building)|Church]]
| style = 
| groundbreaking = 
| completed date = Circa 12th Century
| construction cost = 
| closed date = 
| demolished date = 
| capacity = 
| length = 
| width = 
| width nave = 
| height = 
| diameter = 
| other dimensions = 
| floor count = 
| floor area =
| materials = 
| parish = Acton
| deanery = Sudbury
| archdeaconry = Sudbury
| diocese = [[Diocese of St Edmundsbury and Ipswich|St Edmundsbury and Ipswich]]
| province = [[Province of Canterbury|Canterbury]]
| curate = 
| priestincharge = Caroline Hallett
| priest = 
| asstpriest = 
| reader = Gerry Higginson
| chapterclerk = 
| laychapter = 
| warden = Gerry Higginson <br />Christopher Moss
| logo = 
| logosize = }}

'''All Saints Church''' is located in the village of [[Acton, Suffolk|Acton]] near [[Sudbury, Suffolk|Sudbury]]. It is an [[Anglican]] [[parish church]] in the deanery of Sudbury, part of the archdeaconry of Ipswich, and the [[Diocese of St Edmundsbury and Ipswich]].<ref>{{cite web|title=Listing on A Church Near You|url=https://www.achurchnearyou.com/acton-all-saints/|website=A Church Near You|publisher=The Church of England|accessdate=29 January 2017}}</ref>

All Saints Church was [[Listed building|listed]] at Grade&nbsp;I on 23 March 1961.<ref name="Acton All Saints Listing">{{cite web|title=British listed buildings|url=http://www.britishlistedbuildings.co.uk/en-277799-church-of-all-saints-acton-suffolk#.WI3AzFOLSUk|website=British Listed Buildings|accessdate=29 January 2017}}</ref>

==Construction==

The [[Domesday Book]] mentions Acton as a Church with 30 acres of land. The original building was constructed circa 1250.<ref name="TMCS" /><ref name="Official About Us">{{cite web|title=About Us|url=http://allsaintschurchacton.vpweb.co.uk/About-Us.html|website=All Saints Church Acton|accessdate=29 January 2017}}</ref> The South aisle of the Church was constructed in the 15th Century, and the upper part of the Church tower was pulled down in the late 19th Century for safety,<ref>{{cite web|title=Acton Genealogical Records|url=http://forebears.co.uk/england/suffolk/acton|website=Forbears|accessdate=29 January 2017}}</ref> to be rebuilt in the 1920's.<ref name="TMCS" />

==Memorials==
===Brasses===

All Saints Church has a collection of Medieval Brass, some of which is the oldest brass in Suffolk and third oldest in [[England]], and which was described by the [[Victoria and Albert Museum]] as "The finest military brass in existence".<ref name="Suffolk Churches">{{cite web|title=All Saints, Acton|url=http://www.suffolkchurches.co.uk/acton.html|website=Suffolk Churches|publisher=Simon Knott|accessdate=29 January 2017}}</ref><ref name="TMCS">{{cite web|title=Acton, All Saints|url=http://www.themcs.org/churches/Acton%20All%20Saints.html|website=The Medieval Combat Society|publisher=The Medieval Combat Society|accessdate=29 January 2017}}</ref>

The collection of Brasses includes Robert de Bures, Lady Alice Byran and Henry de Bures, as well as some smaller brasses created of later Byrans. The clothing of Robert de Bures in this Brass indicates that he was a knight of the Crusade in the last Crusade to the Holy Lands which took place in 1270.<ref name="Geni Robert De Bures">{{cite web|title=Robert De Bures, Jr.|url=https://www.geni.com/people/Robert-De-Bures-Jr/6000000007150808702|website=Geni|publisher=Geni.com|accessdate=29 January 2017}}</ref> He is buried within All Saints Church, although his family doesn't appear to have held lands in Acton until his second marriage.<ref name="Geni Robert De Bures" />

===Jennans Memorial===

The Jennans family vault was added with the South aisle in the 15th Century.<ref name="TMCS" /> The memorial itself was added in the 1700's dedicated to Robert Jennans who died in 1732 and was [[Adjutant]] to the [[Duke of Marlborough]],<ref name="Acton All Saints Listing" /> it features Robert Jennans and the allegorical figure of grief.<ref name="Suffolk Churches" /> Details of Both Robert Jennans and his son William Jennans are recorded on the memorial.<ref name="Suffolk Churches" /><ref name="Bury Past and Present">{{cite web|last1=Milburn|first1=Betty|title=Images of Acton|url=http://www.burypastandpresent.org.uk/suffolk-villages/acton.shtml|website=Bury Past and Present|accessdate=29 January 2017}}</ref>

==Bells==

The Church has five bells, which were overhauled by [[Whitechapel Bell Foundry]] and rehung in 1926.<ref name="TMCS" /><ref name="Dove Details">{{cite web|last1=Baldwin|first1=John|title=Acton|url=http://dove.cccbr.org.uk/detail.php?searchString=Acton&numPerPage=10&Submit=Go&searchAmount=%3D&searchMetric=cwt&sortBy=Place&sortDir=Asc&DoveID=ACTON++SUF|website=Dove Details|accessdate=29 January 2017}}</ref><ref name="Suffolk Bells">{{cite web|title=Acton All Saints|url=http://www.suffolkbells.org.uk/towers/acton1.php|website=Suffolk Bells|publisher=The Suffolk Guild of Ringers|accessdate=29 January 2017}}</ref> The bells were founded between 1659 and 1747, with weights ranging between 4-1-4 and 8-1-4 cwt.<ref name="Dove Details" />

==Zeppelin==
The church displays a WW1 bomb dropped by a Zeppelin on the Parish on August 7, 1916.<ref>{{cite web|last1=Pye|first1=Adrian|title=WW1 Bomb dropped by a Zeppelin|url=http://www.geograph.org.uk/photo/4623155|website=Geograph|accessdate=29 January 2017}}</ref><ref>{{cite book|last1=Harris|first1=Brian|title=Harris's Guide to Churches and Cathedrals|date=5 October 2006|publisher=Ebury Press|isbn=978-0091912512|page=5|edition=First|url=https://books.google.com/?id=bBj6l7e-kAoC&pg=PA5&lpg=PA5&dq=zeppelin+acton+church#v=onepage&q=zeppelin%20acton%20church&f=false|accessdate=29 January 2017}}</ref>

== See also ==
*[[Grade I listed buildings in Suffolk]]

== References ==
{{reflist}}

==External links==
{{Official website|http://allsaintschurchacton.vpweb.co.uk/}}

[[Category:Church of England churches in Suffolk]]
[[Category:Grade I listed churches in Suffolk]]