{{Infobox journal
| title         = Journal of Pharmacology and Experimental Therapeutics
| cover         = [[File:JPETcover.gif]]
| editor        = [[Michael F. Jarvis]]
| discipline    = [[Pharmacology]], [[therapeutics]]
| peer-reviewed = 
| language      = English
| abbreviation  = J. Pharm. Exp. Ther.
| publisher     = [[American Society for Pharmacology and Experimental Therapeutics]]
| country       = [[United States]]
| frequency     = 12/year
| history       = 1909–present
| openaccess    = After 12 months
| license       = 
| impact        = 3.972
| impact-year   = 2014
| website       =  http://jpet.aspetjournals.org/
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 1606914
| LCCN          = sf80000806
| CODEN         = JPETAB
| ISSN          = 0022-3565
| eISSN         = 1521-0103
| boxwidth      = 
}}

The '''''Journal of Pharmacology and Experimental Therapeutics''''' (a.k.a. JPET) is a [[peer-reviewed]] [[pharmacology]] [[scientific journal|journal]] published since 1909 by the [[American Society for Pharmacology and Experimental Therapeutics]] (ASPET).<ref name="urlASPET home page">{{cite web |url=http://www.aspet.org/ |title=ASPET home page |format= |work= |accessdate=2009-01-26}}</ref> The journal publishes mainly [[original research]] articles, and accepts papers covering all aspects of the interactions of chemicals with biological systems.

[[John Jacob Abel]] founded ASPET in December 1908<ref>{{cite book|title=The Development of American Pharmacology: John J. Abel and the Shaping of a Discipline|authors=[[John Parascandola]]|pages=212|publisher=Johns Hopkins University Press|year=1992|isbn=0-8018-4416-9}}</ref> when he invited 18 pharmacologists to his laboratory in order to organize a new society. At the end of the meeting Abel announced the establishment of the JPET.<ref name=abouta>{{cite web|url=http://www.aspet.org/public/aspet/history.html|title=History of ASPET 1908-2008|accessdate=2009-01-20}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal received a 2014 [[impact factor]] of 3.972, ranking it 41st out of 254 journals in the category ''Pharmacology & Pharmacy''.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Pharmacology & Pharmacy |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |series=Web of Science |postscript=.}}</ref>

==References==
{{reflist}}

[[Category:English-language journals]]
[[Category:Pharmacology journals]]
[[Category:Publications established in 1909]]
[[Category:Monthly journals]]