{{Infobox journal
| title = Perspectives on Politics
| cover =
| editor = Jeffrey C. Isaac
| discipline = Political science
| former_names =
| abbreviation = Perspect. Polit.
| publisher = [[Cambridge University Press]] on behalf of the [[American Political Science Association]]
| country = United States
| frequency = Quarterly
| history = 2003-present
| openaccess =
| license =
| impact = 2.462
| impact-year = 2015
| website = http://www.apsanet.org/perspectives
| link1 = https://journals.cambridge.org/action/displayJournal?jid=PPS
| link1-name = Journal page at publisher's website
| link2 = https://www.cambridge.org/core/journals/perspectives-on-politics/latest-issue
| link2-name = Online access
| link3 = https://www.cambridge.org/core/journals/perspectives-on-politics/all-issues
| link3-name = Online archive
| JSTOR = 15375927
| OCLC = 865260348
| LCCN = 2001215286
| CODEN =
| ISSN = 1537-5927
| eISSN = 1541-0986
}}
'''''Perspectives on Politics''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[political science]]. It was established in 2003 and is published by [[Cambridge University Press]] on behalf of the [[American Political Science Association]]. The [[editor-in-chief]] is Jeffrey C. Isaac ([[Indiana University Bloomington]]); the founding editor was [[Jennifer Hochschild]].

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Social Sciences Citation Index]] and [[Current Contents]]/Social & Behavioral Sciences.<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=21 January 2014 }}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.462, ranking it 10th out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.apsanet.org/content_3246.cfm}}

[[Category:Political science journals]]
[[Category:Publications established in 2003]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Academic journals associated with learned and professional societies]]