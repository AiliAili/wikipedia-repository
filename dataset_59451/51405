{{Infobox journal
| title = Girlhood Studies
| cover = [[File:Girlhood Studies journal Summer 2013 cover.jpg|200px|Summer 2013 cover]]
| editor = Claudia Mitchell, Jacqueline Reid-Walsh
| discipline = [[Sociology]]
| publisher = [[Berghahn Books]]
| frequency = Biannually
| history = 2008–present
| website = http://journals.berghahnbooks.com/ghs/
| link1 = http://berghahn.publisher.ingentaconnect.com/content/berghahn/gs
| link1-name = Online archive
| ISSN = 1938-8209
| eISSN = 1938-8322
| OCLC = 144560104
}}
'''''Girlhood Studies: An Interdisciplinary Journal''''' is a [[Peer review|peer-reviewed]] [[academic journal]] established in 2008 by [[Jackie Kirk]] and published by [[Berghahn Books]]. It discusses [[girl]]hood in the fields of [[education]], [[social service]], and [[health care]]. The [[editors-in-chief]] are Claudia Mitchell ([[McGill University]]) and Jacqueline Reid-Walsh ([[Penn State University]]). ''Girlhood Studies'' received the Best New Journal in the Social Sciences & Humanities from the Association of American Publishers in 2009.<ref>{{cite news|title=Girlhood Studies: An Interdisciplinary Journal Wins Major Award|url=http://www.history.vt.edu/Jones/SHCY/Newsletter15/GirlhoodStudies.html|newspaper=SHCY Bulletin|date=Spring 2010}}</ref>

== Abstracting and indexing ==
''Girlhood Studies'' is abstracted and indexed in:
* [[Feminist Periodicals]]
* [[International Bibliography of Periodical Literature]]
* [[MLA International Bibliography]]

==References==
{{reflist}}

== External links ==
* {{Official|http://journals.berghahnbooks.com/ghs/}}

[[Category:English-language journals]]
[[Category:Sociology journals]]
[[Category:Biannual journals]]
[[Category:Berghahn Books academic journals]]
[[Category:Publications established in 2008]]