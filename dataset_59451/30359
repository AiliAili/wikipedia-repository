{{ Planetary nebula |
| image = [[Image:NGC6543.jpg|250px|alt=An object resembling a red eye, with a blue pupil, red-blue iris and a green brow. Another green "brow" is placed under the eye, symmetrically versus the pupil]]
|caption=Composite image using optical images from the [[Hubble Space Telescope|HST]] and X-ray data from the [[Chandra X-ray Observatory]]
| name = Cat's Eye Nebula
| type = –
| epoch = [[J2000]]
| ra = {{RA|17|58|33.423}}<ref name="simbad"/>
| dec = {{DEC|+66|37|59.52}}<ref name="simbad" />
| dist_ly = {{val|3.3|0.9|ul=kly}} ({{val|1.0|0.3|ul=kpc}})<ref name="Reedetal1999">{{Harv|Reed|Balick|Hajian|Klayton|1999}}</ref>
| appmag_v = 9.8B<ref name="simbad" />
| size_v = Core: 20″<ref name="Reedetal1999" />
| constellation = [[Draco (constellation)|Draco]]
| radius_ly = Core: 0.2 [[light year|ly]]<ref group=note>Distance × sin(diameter_angle / 2 ) = 0.2 ly. radius</li></ref>
| absmag_v = {{val|-0.2|+0.8|-0.6}}B<ref group=note>9.8B apparent magnitude – 5×{log(1.0 ± 0.3 kpc distance) − 1} = −{{val|0.2|+0.8|-0.6}}B absolute magnitude</li></ref>
| notes = complex structure
| names = [[New General Catalogue|NGC]] 6543,<ref name="simbad" /> Snail&nbsp;Nebula,<ref name="simbad" /> Sunflower&nbsp;Nebula,<ref name="simbad" /> (includes [[Index Catalogue|IC]] [[IC 4677|4677]]),<ref name="simbad" /> Caldwell 6}}
{{Sky|17|58|33.423|+|66|37|59.52|3300}}

The '''Cat's Eye Nebula''' or '''[[New General Catalogue|NGC]] 6543''', is a relatively bright [[planetary nebula]] in the northern constellation of [[Draco (constellation)|Draco]], discovered by [[William Herschel]] on February 15, 1786. It was the first planetary nebula whose [[spectroscopy|spectrum]] was investigated by the English [[amateur astronomy|amateur astronomer]] [[William Huggins]], demonstrating that planetary nebulae were gaseous and not stellar in nature. Structurally, the object has had high-resolution images by the [[Hubble Space Telescope]] revealing knots, jets, bubbles and complex arcs, being illuminated by the central hot [[planetary nebula|planetary nebula nucleus]] (PNN).<ref name="shaw">{{Cite journal
  | last = Shaw
  | first = R. A.
  | title = Thee evolution of Planetary Nebula Nuclei (PNN)
  | journal = Ph.D. Thesis, Illinois Univ., Urbana-Champaign
  | date = 1985
  | bibcode = 1985PhDT........13S}}</ref> 
It is a well-studied object that has been observed from [[radio]] to [[X-ray]] [[Electromagnetic Spectrum|wavelengths]].

==General information==

NGC 6543 is a high [[northern hemisphere|northern]] [[declination]] deep-sky object and is near the apparent position of the [[Ecliptic pole|North Ecliptic Pole]]. It has the combined [[apparent magnitude|magnitude]] of 8.1, with high [[surface brightness]]. Its small bright inner nebula subtends an average of 16.1 [[arcsecond|arcsec]], with the outer prominent condensations about 25 arcsec.<ref name=Reedetal1999-3>{{Harv|Reed|Balick|Hajian|Klayton|1999|p=2433}}</ref> Deep images reveal an extended [[halo (optical phenomenon)|halo]] about 300 arcsec or 5 [[arcminutes|arcmin]] across,<ref name=Balick2001-1/> that was once ejected by the central [[planetary nebula|progenitor]] star during its [[red giant]] phase.

Observations show the bright nebulosity has temperatures between {{val|7000}} and {{val|9000|ul=K}}, whose densities average of about {{val|5000}} particles per cubic centimetre.<ref name=Wesson1>{{Harv|Wesson|Liu|2004|pp=1026, 1028}}</ref> Its outer halo has the higher temperature around {{val|15000|u=K}}, but is of much lower density.<ref name=Wesson2>{{Harv|Wesson|Liu|2004|p=1029}}</ref> Velocity of the fast [[stellar wind]] is about {{val|1900|u=km/s}}, where [[Spectroscopy|spectroscopic analysis]] shows the current rate of mass loss averages {{val|3.2|e=-7}} solar masses per year, equivalent to twenty trillion [[ton]]s per second (20&nbsp;Eg/s).<ref name=Wesson1/>

[[File:Cat's Eye Halo.jpg|thumb|An optical image of the nebula's surrounding halo]]

Surface temperature for the central PNN is about {{val|80000|u=K}}, being {{val|10000}} times as luminous as the sun. [[Stellar classification]] is O7&nbsp;+&nbsp;[[Wolf-Rayet star|[WR]]]–type star<ref name=Wesson1/> Calculations suggest the PNN is over one [[solar mass]], from a theoretical initial 5 solar masses.<ref>{{Harv|Bianchi|Cerrato|Grewing|1986}}</ref> The central Wolf-Rayet star has a radius of {{solar radius|0.65}} (452,000 km).<ref>[http://www.constellation-guide.com/cats-eye-nebula-ngc-6543/ Constellation Guide]</ref>  The Cat's Eye Nebula, given in some sources, lies about three thousand light-years from Earth.<ref>{{Cite APOD|title=The Cat's Eye Nebula From Hubble|date=13 May 2007|accessdate=October 26, 2011}}</ref>

==Observations==
The Cat's Eye was the first planetary nebula to be observed with a [[spectroscope]] by [[William Huggins]] on August 29, 1864.<ref name=Kwok1>{{harv|Kwok|2000|p=1}}</ref> Huggins' observations revealed that the nebula's spectrum was non-continuous and made of a few bright emission lines, first indication that planetary nebulae consist of tenuous ionised gas. Spectroscopic observations at these wavelengths are used in abundance determinations,<ref name=Wesson3>{{Harv|Wesson|Liu|2004|pp=1026–1027, 1040–1041}}</ref> while images at these wavelengths have been used to reveal the intricate structure of the nebula.<ref name=Balick1987-1/>

===Infrared observations===
Observations of NGC 6543 at [[far-infrared]] wavelengths (about 60&nbsp;μm) reveal the presence of [[interstellar cloud|stellar dust]] at low temperatures. The dust is believed to have formed during the last phases of the progenitor star's life. It absorbs light from the central star and re-radiates it at [[infrared]] wavelengths. The spectrum of the infrared dust emission implies that the dust temperature is about 85&nbsp;K, while the mass of the dust is estimated at {{val|6.4|e=-4}} solar masses.<ref name=Klaas>{{Harv|Klaas|Walker|Müller|Richards|2006|p=523}}</ref>

Infrared emission also reveals the presence of [[Ion (physics)|un-ionised]] material such as [[molecular hydrogen]] (H<sub>2</sub>) and [[argon]]. In many planetary nebulae, molecular emission is greatest at larger distances from the star, where more material is un-ionised, but molecular hydrogen emission in NGC 6543 seems to be bright at the inner edge of its outer halo. This may be due to [[shock waves]] exciting the H<sub>2</sub> as ejecta moving at different speeds collide. The overall appearance of the Cat's Eye Nebula in infrared (wavelengths 2–8&nbsp;μm) is similar in visible light.<ref name=Hora>{{Harv|Hora|Latter|Allen|Marengo|2004|p=299}}</ref>

===Optical and ultraviolet observations===

The Hubble Space Telescope image produced here is in false colour, designed to highlight regions of high and low [[ion (physics)|ionisation]]. Three images were taken, in filters isolating the light emitted by singly ionised [[hydrogen]] at 656.3&nbsp;[[nanometre|nm]], singly ionised [[nitrogen]] at 658.4&nbsp;nm and doubly ionised [[oxygen]] at 500.7&nbsp;nm. The images were combined as red, green and blue channels respectively, although their true colours are red, red and green. The image reveals two "caps" of less ionised material at the edge of the nebula.<ref name=Wesson4>{{Harv|Wesson|Liu|2004|pp=1027–1031}}</ref>

===X-ray observations===
[[File:Cat's Eye Nebula.X-ray image.jpg|thumb|X-ray image of Nebula.]]
In 2001, observations at [[X-ray]] wavelengths by the [[Chandra X-ray Observatory]] revealed the presence of extremely hot gas within NGC 6543 with the temperature of {{val|1.7|e=6|u=K}}.<ref name=Chu>{{Harv|Chu|Guerrero|Gruendl|Williams|2001}}</ref> It is thought that the very hot gas results from the violent interaction of a fast stellar wind with material previously ejected. This interaction has hollowed out the inner bubble of the nebula.<ref name=Balick1987-1/> Chandra observations have also revealed a [[wikt:point source|point source]] at the position of the central star. The spectrum of this source extends to the hard part of the X-ray spectrum, to 0.5–{{val|1.0|ul=keV}}. A star with the [[photosphere|photospheric]] temperature of about {{val|100000|u=K}} would not be expected to emit strongly in hard X-rays, and so their presence is something of a mystery. It may suggest the presence of a high temperature [[accretion disk]] within a [[binary star]] system.<ref name=Guerrero>{{Harv|Guerrero|Chu|Gruendl|Williams|2001}}</ref> The hard X-ray data remain intriguing more than ten years later: the Cat's Eye was included in a 2012 Chandra survey of 21 central stars of planetary nebulae (CSPNe) in the solar neighborhood, which found: "All but one of the X-ray point sources detected at CSPNe display X-ray spectra that are harder than expected from hot (~{{val|100000|u=K}}) central star photospheres, possibly indicating a high frequency of binary companions to CSPNe. Other potential explanations include self-shocking winds or PN mass fallback." <ref name=Kastner>{{Harv|Kastner et al.|2012}}</ref>

==Distance==
Planetary nebulae distances like NGC 6543 are generally very inaccurate and not well known.<ref name=Reedetal1999-1>{{Harv|Reed|Balick|Hajian|Klayton|1999|p=2430}}</ref> Some recent Hubble Space Telescope observations of NGC 6543 taken several years apart determine its distance from the angular expansion rate of 3.457 milliarcseconds per year. Assuming a line of sight expansion velocity of 16.4&nbsp;km·s<sup>−1</sup>, this implies that NGC 6543's distance is {{val|1001|269}}&nbsp;[[parsec]]s ({{val|3|e=19|u=m}} or {{val|3300}}&nbsp;[[light-years]]) away from Earth.<ref name=Reedetal1999-2>{{Harv|Reed|Balick|Hajian|Klayton|1999|pp=2433–2438}}</ref> Several other distance references, like what is quoted in [[SIMBAD]] in 2014 Stanghellini, L., et al. (2008) suggest the distance is {{val|1623}} parsecs ({{val|5300}} light-years).

==Age==
The angular expansion of the nebula can also be used to estimate its age. If it has been expanding at a constant rate of 10 milliarcseconds a year, then it would take {{val|1000|260|u=years}} to reach a diameter of 20 arcseconds. This may be an upper limit to the age, because ejected material will be slowed when it encounters material ejected from the star at earlier stages of its evolution, and the [[interstellar medium]].<ref name=Reedetal1999-2/>

==Composition==
[[Image:Catseye-big.jpg|thumb|alt=Blue-green diffuse disk with complex circular structure in its center. The disk is crossed by s-shaped brown curve.|Image of NGC 6543 processed to reveal the concentric rings surrounding the inner core. Also visible are the linear structures, possibly caused by precessing jets from a binary central star system.]]

Like most astronomical objects, NGC 6543 consists mostly of [[hydrogen]] and [[helium]], with heavier elements present in small quantities. The exact composition may be determined by spectroscopic studies. Abundances are generally expressed relative to hydrogen, the most abundant element.<ref name=Wesson2/>

Different studies generally find varying values for elemental abundances. This is often because [[spectrograph]]s attached to telescopes do not collect all the light from objects being observed, instead gathering light from a slit or small [[aperture]]. Therefore, different observations may sample different parts of the nebula.

However, results for NGC 6543 broadly agree that, relative to hydrogen, the helium abundance is about 0.12, [[carbon]] and [[nitrogen]] abundances are both about {{val|3|e=-4}}, and the [[oxygen]] abundance is about {{val|7|e=-4}}.<ref name=Wesson3/> These are fairly typical abundances for planetary nebulae, with the carbon, nitrogen and oxygen abundances all larger than the values found for the sun, due to the effects of [[nucleosynthesis]] enriching the star's atmosphere in heavy elements before it is ejected as a planetary nebula.<ref>{{Harv|Hyung|Aller|Feibelman|Lee|2000}}</ref>

Deep spectroscopic analysis of NGC 6543 may indicate that the nebula contains a small amount of material which is highly enriched in heavy elements; this is discussed below.<ref name=Wesson3/>

==Kinematics and morphology==
The Cat's Eye Nebula is structurally a very complex nebula, and the mechanism or mechanisms that have given rise to its complicated morphology are not well understood.<ref name=Balick1987-1/> The central bright part of the nebular consists of the inner elongated bubble (inner ellipse) filled with hot gas. It in turn is nested into a pair of larger spherical bubbles conjoined together along their waist. The waist is observed as the second larger ellipse lying perpendicular to the bubble with hot gas.<ref name=Reedetal1999-4>{{Harv|Reed|Balick|Hajian|Klayton|1999|pp=2438–2440}}</ref>

The structure of the bright portion of the nebula is primarily caused by the interaction of a fast [[stellar wind]] being emitted by the central PNN with the visible material ejected during the formation of the nebula. This interaction causes the emission of X-rays discussed above. The stellar wind, blowing with the velocity as high as {{val|1900|u=km/s}}, has 'hollowed out' the inner bubble of the nebula, and appears to have burst the bubble at both ends.<ref name=Balick1987-1>{{Harv|Balick|Preston|1987|pp=958, 961–963}}</ref>

It is also suspected that the central WR:+O7 spectral class PNN star, HD 1064963 / BD +66 1066  / PPM 20679 <ref name="simbad">{{Harv|SIMBAD|2006}}</ref> of the nebula may be generated by a [[binary star]].<ref name="simbad"/> The existence of an [[accretion disk]] caused by mass transfer between the two components of the system may give rise to [[polar jet]]s, which would interact with previously ejected material. Over time, the direction of the polar jets would vary due to [[precession]].<ref>{{Harv|Miranda|Solf|1992}}</ref>

Outside the bright inner portion of the nebula, there are a series of concentric rings, thought to have been ejected before the formation of the planetary nebula, while the star was on the [[asymptotic giant branch]] of the [[Hertzsprung-Russell diagram]]. These rings are very evenly spaced, suggesting that the mechanism responsible for their formation ejected them at very regular intervals and at very similar speeds.<ref name=Balick2001-1>{{Harv|Balick|Wilson|Hajian|2001|p=354}}</ref> The total mass of the rings is about 0.1 solar masses.<ref name=Balick2001-2/> The pulsations that formed the rings probably started 15,000 years ago and ceased about {{val|1000}} years ago, when the formation of the bright central part began (see above).<ref name=Balick2001-3>{{Harv|Balick|Wilson|Hajian|2001|pp=359–360}}</ref>

Further out, a large faint halo extends to large distances from the star. The halo again predates the formation of the main nebula. The mass of the halo is estimated as 0.26–0.92 solar masses.<ref name=Balick2001-2>{{Harv|Balick|Wilson|Hajian|2001|p=358}}</ref>

==Notes==
{{reflist|group=note}}

==References==
{{Reflist|30em}}

==Cited sources==
{{Refbegin|30em}}
* {{citation
 | last1 = Balick
 | first1 = Bruce
 | last2 = Preston
 | first2 = Heather L.
 | title = A wind-blown bubble model for NGC 6543
 | journal = Astronomical Journal
 | date = October 1987
 | volume = 94
 | issue = 
 | pages = 958–963
 | bibcode = 1987AJ.....94..958B
 | doi = 10.1086/114528
}}
* {{citation
 | last1 = Balick
 | first1 = Bruce
 | last2 = Wilson
 | first2 = Jeanine
 | last3 = Hajian
 | first3 = Arsen R.
 | title = NGC 6543: The Rings Around the Cat's Eye
 | journal = Astronomical Journal
 | date = 2001
 | volume = 121
 | issue = 1| pages = 354
 | bibcode = 2001AJ....121..354B
 | doi = 10.1086/318052
}}
* {{citation
 | last1 = Bianchi
 | first1 = L.
 | last2 = Cerrato
 | first2 = S.
 | last3 = Grewing
 | first3 = M.
 | title = Mass loss from central stars of planetary nebulae—the nucleus of NGC 6543
 | journal = Astronomy and Astrophysics
 | date = November 1986
 | volume = 169
 | issue = 1–2
 | pages = 227–236
 | bibcode = 1986A&A...169..227B
}}
* {{citation
 | last1 = Chu
 | first1 = You-Hua
 | last2 = Guerrero
 | display-authors = 4
 | first2 = Martı´n A.
 | last3 = Gruendl
 | first3 = Robert A.
 | last4 = Williams
 | first4 = Rosa M.
 | last5 = Kaler
 | first5 = James B.
 | title = Chandra reveals the X-ray glint in the cat's eye
 | journal = Astrophysical Journal
 | date = 2001
 | volume = 553
 | issue = 1
 | pages = L69–L72
 | bibcode = 2001ApJ...553L..69C
 | doi = 10.1086/320495
|arxiv = astro-ph/0101444 }}
* {{citation
 | last1 = Guerrero
 | first1 = Martín A.
 | last2 = Chu
 | display-authors = 4
 | first2 = You-Hua
 | last3 = Gruendl
 | first3 = Robert A.
 | last4 = Williams
 | first4 = Rosa M.
 | last5 = Kaler
 | first5 = James B.
 | title = The Enigmatic X-Ray Point Sources at the Central Stars of NGC 6543 and NGC 7293
 | journal = Astrophysical Journal
 | date = 2001
 | volume = 553
 | issue = 1
 | pages = L55–L58
 | bibcode = 2001ApJ...553L..55G
 | doi = 10.1086/320509
|arxiv = astro-ph/0104270 }}
* {{citation
 | last1 = Hora
 | first1 = Joseph L.
 | last2 = Latter
 | display-authors = 4
 | first2 = William B.
 | last3 = Allen
 | first3 = Lori E.
 | last4 = Marengo
 | first4 = Massimo
 | last5 = Deutsch
 | first5 = Lynne K.
 | last6 = Pipher
 | first6 = Judith L.
 | title = Infrared Array Camera (IRAC) Observations of Planetary Nebulae
 | journal = Astrophysical Journal Supplement Series
 | date = 2004
 | volume = 154
 | issue = 1
 | pages = 296–301
 | bibcode = 2004ApJS..154..296H
 | doi = 10.1086/422820
}}
* {{citation
 | last1 = Hyung
 | first1 = S.
 | last2 = Aller
 | display-authors = 4
 | first2 = L. H.
 | last3 = Feibelman
 | first3 = W. A.
 | last4 = Lee
 | first4 = W. B.
 | last5 = de Koter
 | first5 = A.
 | title = The optical spectrum of the planetary nebula NGC 6543
 | journal = [[Monthly Notices of the Royal Astronomical Society]]
 | date = 2000
 | volume = 318
 | issue = 1
 | pages = 77–91
 | bibcode = 2000MNRAS.318...77H
 | doi = 10.1046/j.1365-8711.2000.03642.x
}}
* {{citation
 | last1 = Kastner
 | first1 = J.
 | last2 = Montez
 | display-authors = 4
 | first2 = R. Jr.
 | last3 = Balick
 | first3 = B.
 | last4 = Frew
 | first4 = D. J.
 | last5 = and 22 coauthors
 | first5 = 
 | title = The Chandra X-Ray Survey of Planetary Nebulae (CHANPLANS): Probing Binarity, Magnetic Fields, and Wind Collisions
 | journal = Astronomical Journal
 | date = 2012
 | volume = 144
 | issue = 2
 | pages = 18
 | bibcode = 2012AJ....144...58K
 | doi = 10.1088/0004-6256/144/2/58
 | arxiv = 1204.6055
 | ref = {{sfnRef|Kastner et al.|2012}}
}}
* {{citation
 | last1 = Klaas
 | first1 = U.
 | last2 = Walker
 | display-authors = 4
 | first2 = S. J.
 | last3 = Müller
 | first3 = T. G.
 | last4 = Richards
 | first4 = P. J.
 | last5 = Schreiber
 | first5 = J.
 | title = Multi-aperture photometry of extended IR sources with ISOPHOT. I. The nature of extended IR emission of planetary Nebulae
 | journal = Astronomy and Astrophysics
 | date = 2006
 | volume = 452
 | issue = 2
 | pages = 523–535
 | bibcode = 2006A&A...452..523K
 | doi = 10.1051/0004-6361:20053245
}}
* {{citation
 | last1 = Kwok
 | first1 = Sun
 | author1-link = Sun Kwok
 | title = The origin and evolution of planetary nebulae
 | chapter = Chapter1: History and overview
 | date = 2000
 | publisher = Cambridge University Press
 | pages = 1–7
 | isbn = 0-521-62313-8
 | url = https://books.google.com/?id=7NfqpZxO_o0C
}}
* {{citation
 | last1 = Miranda
 | first1 = L. F.
 | last2 = Solf
 | first2 = J.
 | title = Long-slit spectroscopy of the planetary nebula NGC 6543—Collimated bipolar ejections from a precessing central source?
 | journal = Astronomy and Astrophysics
 | date = 1992
 | volume = 260
 | issue = 1–2
 | pages = 397–410
 | bibcode = 1992A&A...260..397M
}}
* {{citation
 | last1 = Moore
 | first1 = S. L.
 | title = Observing the Cat's Eye Nebula
 | journal = Journal of the British Astronomical Association
 | date = 2007
 | volume = 117
 | issue = 5
 | pages = 279–280
 | bibcode = 2007JBAA..117R.279M
}}
* {{citation
 | last1 = Reed
 | first1 = Darren S.
 | last2 = Balick
 | display-authors = 4
 | first2 = Bruce
 | last3 = Hajian
 | first3 = Arsen R.
 | last4 = Klayton
 | first4 = Tracy L.
 | last5 = Giovanardi
 | first5 = Stefano
 | last6 = Casertano
 | first6 = Stefano
 | last7 = Panagia
 | first7 = Nino
 | last8 = Terzian
 | first8 = Yervant
 | title = Hubble Space Telescope Measurements of the Expansion of NGC 6543: Parallax Distance and Nebular Evolution
 | journal = Astronomical Journal
 | date = 1999
 | volume = 118
 | issue = 5
 | pages = 2430–2441
 | bibcode = 1999AJ....118.2430R
 | doi = 10.1086/301091
|arxiv = astro-ph/9907313 }}
* {{citation
 | last1 = SIMBAD
 | title = Results for Cat's Eye Nebula
 | authorlink = SIMBAD
 | journal = 
 | date = December 22, 2006
 | publisher = SIMBAD, Centre de Données Astronomiques de Strasbourg
 | url = http://simbad.u-strasbg.fr/simbad/sim-id?protocol=html&Ident=Cat's+Eye+Nebula
}}
* {{citation
 | last1 = Wesson
 | first1 = R.
 | last2 = Liu
 | first2 = X.-W.
 | title = Physical conditions in the planetary nebula NGC 6543
 | journal = Monthly Notices of the Royal Astronomical Society
 | date = 2004
 | volume = 351
 | issue = 3
 | pages = 1026–1042
 | bibcode = 2004MNRAS.351.1026W
 | doi = 10.1111/j.1365-2966.2004.07856.x}}
{{Refend}}

==External links==
{{Commons category|Cat&#39;s Eye Nebula}}
* [http://www.spacetelescope.org/news/html/heic0414.html Cat's Eye Nebula Release at ESA/Hubble]
* [http://www.spacetelescope.org/images/archive/freesearch/cat's+eye+nebula/viewall/1 Cat's Eye Nebula images at ESA/Hubble]
* [http://chandra.harvard.edu/photo/2001/1220/index.html Chandra X-Ray Observatory Photo Album: NGC 6543]
* [[Astronomy Picture of the Day]]
** [http://antwrp.gsfc.nasa.gov/apod/ap991031.html The Cat's Eye Nebula] October 31, 1999
** [http://apod.nasa.gov/apod/ap100509.html Halo of the Cat's Eye] 2010 May 9
** [http://apod.nasa.gov/apod/ap160703.html The Cat's Eye Nebula] 2016 July 3
* [http://hubblesite.org/newscenter/newsdesk/archive/releases/1995/01/ Hubble Probes the Complex History of a Dying Star]—HubbleSite article about the Cat's Eye Nebula.
* [http://www.dsi-astronomie.de/NGC6543.html NGC6543 The Cats Eye Nebula]
* [http://hubblesite.org/sci.d.tech/behind_the_pictures/meaning_of_color/catseye.shtml Hubble's Color Toolbox: Cat's Eye Nebula]—article showing image composite process used to produce an image of the nebula
* {{WikiSky}}
* [http://www.constellation-guide.com/cats-eye-nebula-ngc-6543/ Cat's Eye Nebula at Constellation Guide]

{{featured article}}

{{Catalogs|IC=4677|C=6|NGC=6543}}
{{Caldwell catalogue}}
{{Ngc70}}

[[Category:Caldwell objects|006b]]
[[Category:Draco (constellation)]]
[[Category:NGC objects|6543]]
[[Category:Planetary nebulae]]
[[Category:Astronomical objects discovered in 1786]]