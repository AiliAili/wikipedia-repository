{{Infobox journal
| title = Applied Linguistics
| cover = [[File:Applied Linguistics cover.gif]]
| editor = John Hellermann, Anna Mauranen
| discipline = [[Applied linguistics]]
| former_names = 
| abbreviation = Appl. Linguist.
| publisher = [[Oxford University Press]]
| country =
| frequency = Quarterly
| history = 1980–present
| openaccess = 
| license = 
| impact = 1.469
| impact-year = 2009
| website = http://applij.oxfordjournals.org/
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 643108697
| LCCN = 81643225
| CODEN = 
| ISSN = 0142-6001
| eISSN = 1477-450X
}}
'''''Applied Linguistics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[applied linguistics]] established in 1980 and published by [[Oxford University Press]]. It appears four times a year. Current [[Editor in chief|editors in chief]] are John Hellermann (Portland State University) and Anna Mauranen (University of Helsinki).

==Aims and scope==
The journal publishes both research papers and conceptual articles in all aspects of applied linguistics, such as [[lexicography]], [[corpus linguistics]], [[multilingualism]], [[discourse analysis]], and [[language education]], aiming at promoting discussion among researchers in different fields.<ref>{{cite web|url=http://www.oxfordjournals.org/our_journals/applij/about.html|title=About the journal| accessdate=6 January 2011|year=2010|publisher=Oxford University Press}}</ref> It features a "Forum" section, introduced in 2001, intended for short contributions, such as responses to articles and notices about current research.<ref name="editorial">{{cite journal|title=Editorial|author1=M Bygate |author2=K Hyltenstam |author3=C Kramsch |journal=Applied Linguistics|year=2001|volume=22|issue=1|pages=i|doi=10.1093/applin/22.1.i}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed by:
{{columns-list|2|
* [[Linguistic Bibliography|Linguistic Bibliography/Bibliographie Linguistique ]]
* British Education Index
* [[Current Contents]]
* Education Research Abstracts
* Educational Management Abstracts
* [[International Bibliography of the Social Sciences]]
* [[Journal Citation Reports]]/Social Sciences Edition
* Linguistics & Language Behavior Abstracts
* Periodicals Index Online
* [[ProQuest]]
* [[PsychLIT]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* Studies on Women and Gender Abstracts
* The Standard Periodical Directory
}}

==References==
{{Reflist}}

== External links ==
* {{Official website|1=http://applij.oxfordjournals.org/}}

{{DEFAULTSORT:Applied Linguistics}}
[[Category:English-language journals]]
[[Category:Linguistics journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1980]]