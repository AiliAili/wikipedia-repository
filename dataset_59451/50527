{{Infobox journal
| title = The Annals of Thoracic Surgery
| cover = [[File:The Annals of Thoracic Surgery.gif]]
| editor = L. Henry Edmunds
| discipline = [[Pulmonology|Pulmonary disease]], [[surgery]]
| abbreviation = Ann. Thorac. Surg.
| publisher = [[Elsevier]]
| country =
| frequency = Monthly
| history = 1965-present
| impact = 3.849
| impact-year = 2014
| website = http://www.elsevier.com/wps/find/journaldescription.cws_home/505747/description#description
| link1 = http://www.sciencedirect.com/science/journal/00034975
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 1481414
| LCCN =
| CODEN =
| ISSN = 0003-4975
| eISSN = 1552-6259
}}
'''''The Annals of Thoracic Surgery''''' is a [[Peer review|peer-reviewed]] [[medical journal]] that was established in 1965. It covers the fields of [[pulmonology|pulmonary disease]]s and [[surgery]]. It is the official journal of the [[Society of Thoracic Surgeons]] and the [[Southern Thoracic Surgical Association]].

==Article retraction practice==
In 2004 ''The Annals of Thoracic Surgery'' published a study comparing two heart drugs. In January 2011 the journal retracted the study. The journal's [[editor-in-chief]], L. Henry Edmunds, was contacted by [[Retraction Watch]] to get details about the cause of the retraction. Edmunds replied that journalists and bloggers need not discuss article retraction and that it was sufficient for the public to know that the article had been retracted. Edmunds went on to say that the reasons why a journal might retract an article are personal in the same way that the reasons for a marital [[divorce]] are.<ref>{{cite news |title=Now you see it, now you don't: why journals need to rethink retractions |author=[[Ben Goldacre]] |url=https://www.theguardian.com/commentisfree/2011/jan/15/bad-science-academic-journal-retraction |newspaper=[[The Guardian]] |date=15 January 2011 |accessdate=15 January 2011}}</ref>

==Current editorial board==
'''Editor''':
G. Alexander Patterson, MD, St. Louis, MO, USA

'''Past Editors''':
L. Henry Edmunds Jr, MD, 2000-2015
Thomas B. Ferguson MD, 1984-2000
Herbert Sloan MD, 1969-1984
John D. Steele MD, 1964-1969

'''Deputy Editors'''

'''Original Articles'''
Verdi J. DiSesa MD, Philadelphia, PA, USA
Mark K. Ferguson MD, Chicago, IL, USA
Thomas E. MacGillivray MD, Boston, MA, USA
Constantine Mavroudis MD, Orlando, FL, USA

'''Feature Articles'''
John A. Odell MD, Jacksonville, FL, USA

'''Special Articles'''
Douglas J. Mathisen MD, Boston, MA, USA

'''CME'''
Colleen G. Koch MD, Cleveland, OH, USA

'''Associate Editors'''

'''Associate Editors for CME'''
Katherine P. Grichnik MD, Durham, NC, USA
Kirk R. Kanter MD, Atlanta, GA, USA
John A. Kern MD, Charlottesville, VA, USA
Kemp H. Kernstine MD, Dallas, TX, USA

'''Ethics Editor'''
Robert M. Sade MD, Charleston, SC, USA

'''Statistics Editor'''
Gary L. Grunkemeier PhD, Portland, OR, USA

'''Feature Article Editors'''
Derek R. Brinster MD, Richmond, VA, USA
David B. Campbell MD, Hershey, PA, USA
Rick A. Esposito MD, Manhassett, NY, USA
[[Richard Lee (Cardiac Surgeon)]] MD, St. Louis, MO, USA
S. Bert Litwin MD, Boca Raton, FL, USA
M. Blair Marshall MD, Washington, DC, USA
Joseph I. Miller MD, Atlanta, GA, USA
Wayne E. Richenbacher, Iowa City, IA, USA
Jorge D. Salazar MD, Jackson, MS, USA
Mark S. Slaughter MD, Louisville, KY, USA
Thomas J. Vander Salm MD, Salem, MA, USA

'''Surgical Heritage Editor'''
Nicholas T. Kouchoukos MD, St. Louis, MO, USA

'''Editorial Board''':
Mark S. Allen MD, Rochester, NY, USA
Nasser K. Altorki MD, New York, NY, USA
Hisao Asamura MD, Tokyo, Japan
Ko Bando MD, Nasushiobara, Japan
Stefano Benussi MD, Milan, Italy
William J. Brawn MD, Birmingham, UK
Alessandro Brunelli, Ancona, Italy
Albert T. Cheung, Philadelphia, PA, USA
John V. Conte MD, Baltimore, MD, USA
Steven R. DeMeester MD, Los Angeles, CA, USA
Jessica S. Donington MD, New York, NY, USA
Fred H. Edwards MD, Jacksonville, FL, USA
Anthony L. Estrera MD, Houston, TX, USA
Wentao Fang MD, Shanghai, China
Hiran C. Fernando MD, Boston, MA, USA
James S. Gammie MD, Baltimore, MD, USA
Robert C. Gorman MD, Philadelphia, PA, USA
Michael Grimm MD, Vienna, Austria
Eugene A. Grossi MD, New York, NY, USA
George L. Hicks, Jr MD, Rochester, NY, USA
Jennifer C. Hirsch-Romano MD, Ann Arbor, MI, USA
Marshall L. Jacobs MD, Baltimore, MD, USA
Michael E. Jessen MD, Dallas, TX, USA
David R. Jones MD, Charlottesville, VA, USA
A. Pieter Kappetein MD, PhD, Rotterdam, The Netherlands
Ki-Bong Kim MD, Seoul, Korea
James K. Kirklin MD, Birmingham, AL, USA
Arkalgud Sampath Kumar MD, Delhi, India
Tomislav Mihaljevic MD, Cleveland, OH, USA
R. Scott Mitchell MD, Stanford, CA, USA
Yoshifumi Naka MD, New York, NY, USA
Louis P. Perrault MD, Montreal, Quebec, Canada
Richard L. Prager MD, Ann Arbor, MI
Gaetano Rocco MD, Naples, Italy
Juan Rosai MD, Milan, Italy
Marc Ruel MD, Ottawa, Ontario, Canada
Shunji Sano MD, Okayama, Japan
Joseph B. Shrager MD, Stanford, CA, USA
Patricia A. Thistlethwaite MD, San Diego, CA, USA
Vinod H. Thourani MD, Atlanta, GA, USA
Glen S. Van Arsdell MD, Toronto, Ontario, Canada
Pascal R. Vouhe MD, Paris, France
Thomas K. Waddell MD, Toronto, Ontario, Canada
Thomas Walther MD, Nauheim, Germany
Song Wan MD, Hong Kong, China
Cameron D. Wright MD, Boston, MA, USA
Qing-yu Wu, Beijing, China
<ref>http://www.annalsthoracicsurgery.org/edboard</ref>

==References==
{{reflist}}

==External links==
* {{Official website|http://www.elsevier.com/wps/find/journaldescription.cws_home/505747/description#description}}
* [http://www.sts.org The Society of Thoracic Surgeons]

{{DEFAULTSORT:ANNALS OF THORACIC SURGERY}}
[[Category:Publications established in 1965]]
[[Category:Surgery journals]]
[[Category:Elsevier academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]
[[Category:1965 establishments in the United States]]