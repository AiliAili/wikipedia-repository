{| {{Infobox ship begin}}
{{Infobox ship image
 | Ship image   = File:SMS Nassau NH 46827.jpg
 | Ship image size = 300
 | Ship caption = ''Nassau'', very early in her career
}}
{{Infobox ship career
 | Hide header = 
 | Ship country = [[German Empire|Germany]]
 | Ship flag = {{shipboxflag|German Empire|naval}}
 | Ship name = ''Nassau''
 | Ship namesake = [[Duchy of Nassau]] part of the [[Provinces of Prussia|Prussian province]] of [[Hesse Nassau]]{{sfn|Staff|p=23}}
 | Ship ordered = 
 | Ship builder = [[Kaiserliche Werft Wilhelmshaven|Kaiserliche Werft]], [[Wilhelmshaven]]
 | Ship laid down = 22 July 1907
 | Ship launched = 7 March 1908
 | Ship acquired = 
 | Ship commissioned = 1 October 1909
 | Ship decommissioned = 
 | Ship in service = 
 | Ship out of service = 
 | Ship struck = 
 | Ship reinstated = 
 | Ship honours = 
 | Ship fate = Scrapped 1921
 | Ship status = 
 | Ship notes = 
}}
{{Infobox ship characteristics
 | Ship class = {{sclass-|Nassau|battleship}}
 | Ship displacement =* Designed: {{convert|18570|t|LT|abbr=on}}
* Full load: {{convert|21000|t|LT|abbr=on}}
 | Ship length = {{convert|146.1|m|ftin|abbr=on|0}}
 | Ship beam = {{convert|26.9|m|ftin|abbr=on|0}}
 | Ship draft = {{convert|8.9|m|ftin|abbr=on|0}}
 | Ship power = {{convert|16181|kW|ihp|lk=in|abbr=on|order=flip}}
 | Ship propulsion = 3-shaft vertical triple expansion
 | Ship speed = {{convert|20|kn|lk=in}}
 | Ship range = At {{convert|12|kn}}: {{convert|8300|nmi|abbr=on|lk=in}}
 | Ship complement =40&nbsp;officers, 968&nbsp;men
 | Ship armament =* 12 × [[28 cm SK L/45 gun|{{convert|28|cm|in|abbr=on|0}} guns]]
* 12 × [[15 cm SK L/45|{{convert|15|cm|in|abbr=on|0}} guns]]
* 16 × [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on|0}} guns]]
* 6 × {{convert|45|cm|in|abbr=on|0}} torpedo tubes
 | Ship armor =* Belt: {{convert|270|mm|in|abbr=on}}
* Turrets: {{convert|280|mm|in|abbr=on}}
* Deck: {{convert|80|mm|in|abbr=on}}
* Conning Tower: {{convert|400|mm|in|abbr=on}}
 | Ship notes =* Double bottom: 88%
* Watertight compartments: 16
}}
|}
'''SMS ''Nassau'''''{{efn|name=SMS}} was the first [[dreadnought]] [[battleship]] built for the [[Imperial German Navy]], a response to the launching of the British battleship {{HMS|Dreadnought|1906|6}}.{{sfn|Hore|p=67}} ''Nassau'' was [[keel laying|laid down]] on 22 July 1907 at the [[Kaiserliche Werft Wilhelmshaven|Kaiserliche Werft]] in [[Wilhelmshaven]], and [[ceremonial ship launching|launched]] less than a year later on 7 March 1908, approximately 25&nbsp;months after ''Dreadnought''. She was the [[lead ship]] of [[Nassau-class battleship|her class]] of four battleships, which included {{SMS|Posen||2}}, {{SMS|Rheinland||2}}, and {{SMS|Westfalen||2}}.

''Nassau'' saw service in the [[North Sea]] at the beginning of [[World War I]], in the II Division of the [[I Battle Squadron]] of the German [[High Seas Fleet]]. In August 1915, she entered the [[Baltic Sea]] and participated in the [[Battle of the Gulf of Riga]], where she engaged the Russian battleship {{ship|Russian battleship|Slava||2}}. Following her return to the North Sea, ''Nassau'' and her sister ships took part in the [[Battle of Jutland]] on 31 May&nbsp;– 1 June 1916. During the battle, ''Nassau'' collided with the British destroyer {{HMS|Spitfire|1912|6}}. ''Nassau'' suffered a total of 11&nbsp;killed and 16&nbsp;injured during the engagement.

After World War I, the bulk of the High Seas Fleet was interned in [[Scapa Flow]]. As they were the oldest German dreadnoughts, the ''Nassau''-class ships were for the time permitted to remain in German ports. After the [[scuttling of the German fleet in Scapa Flow|German fleet was scuttled]], ''Nassau'' and her three sisters were surrendered to the victorious [[Allies of World War I|Allied powers]] as replacements for the sunken ships. ''Nassau'' was ceded to [[Empire of Japan|Japan]] in April 1920. With no use for the ship, Japan sold her to a British wrecking firm which then [[ship breaking|scrapped]] her in [[Dordrecht]], [[Netherlands]].

== Description ==
{{Main|Nassau-class battleship}}
[[File:Nassau class main weapon.svg|thumb|left|Line drawing of a ''Nassau''-class battleship showing the disposition of the main battery]]
''Nassau'' was {{convert|146.1|m|ftin|abbr=on|0}} long, {{convert|26.9|m|ftin|abbr=on}} wide, and had a draft of {{convert|8.9|m|ftin|abbr=on|0}}. She displaced {{convert|18873|t|LT|abbr=on}} with a normal load, and {{convert|20535|t|LT|abbr=on}} fully laden. The ship had a crew of 40&nbsp;officers and 968&nbsp;enlisted men. ''Nassau'' retained three-shafted [[Steam engine#Multiple expansion engines|triple expansion engines]] with coal-fired boilers instead of more advanced [[turbine]] engines. Her propulsion system was rated at {{convert|16181|kW|ihp|lk=in|abbr=on|order=flip}} and provided a top speed of {{convert|20|kn}}. She had a cruising radius of {{convert|8300|nmi|lk=in}} at a speed of {{convert|12|kn}}.{{sfn|Gröner|p=23}} This type of machinery was chosen at the request of both Admiral [[Alfred von Tirpitz]] and the Navy's construction department; the latter stated in 1905 that the "use of turbines in heavy warships does not recommend itself."{{sfn|Herwig|pp=59–60}} This decision was based solely on cost: at the time, [[Parsons Marine Steam Turbine Company|Parsons]] held a monopoly on steam turbines and required a 1&nbsp;million [[German gold mark|gold mark]] royalty fee for every turbine engine made. German firms were not ready to begin production of turbines on a large scale until 1910.{{sfn|Staff|pp=23, 35}}

''Nassau'' carried twelve [[28 cm SK L/45 gun|{{convert|28|cm|in|abbr=on|0}} SK L/45]]{{efn|name=gun nomenclature}} guns in an unusual hexagonal configuration. Her secondary armament consisted of twelve [[15 cm SK L/45|{{convert|15|cm|in|abbr=on|0}} SK L/45 guns]] and sixteen [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on|0}} SK L/45 guns]], all of which were mounted in [[casemate]]s.{{sfn|Gröner|p=23}} The ship was also armed with six {{convert|45|cm|in|abbr=on|0}} submerged [[torpedo tube]]s. One tube was mounted in the bow, another in the stern, and two on each broadside, on either ends of the [[torpedo bulkhead]].{{sfn|Gardiner & Gray|p=140}} The ship's [[belt armor]] was {{convert|270|mm|abbr=on}}{{sfn|Staff|p=22}} thick in the central portion of the hull, and the armored deck was {{convert|80|mm|abbr=on|0}} thick. The main battery turrets had {{convert|280|mm|abbr=on|0}} thick sides, and the [[conning tower]] was protected with {{convert|400|mm|abbr=on|0}} of armor plating.{{sfn|Gröner|p=23}}

== Service history ==
[[File:SMS Nassau NH 46828.jpg|thumb|left|''Nassau'' early in her career, c. 1909–1910]]
''Nassau'' was ordered under the provisional name ''Ersatz Bayern'', as a replacement for the old {{sclass-|Sachsen|ironclad|2}} {{SMS|Bayern|1878|2}}. She was laid down on 22 July 1907 at the ''[[Kaiserliche Werft Wilhelmshaven|Kaiserliche Werft]]'' in [[Wilhelmshaven]], under construction number 30.{{sfn|Gröner|p=23}} Construction work proceeded under absolute secrecy; detachments of soldiers were tasked with guarding the shipyard itself, as well as contractors that supplied building materials, such as [[Krupp]].{{sfn|Hough|p=26}} The ship was launched on 7 March 1908; she was christened by [[Princess Hilda of Nassau]], and the ceremony was attended by Kaiser [[Wilhelm II, German Emperor|Wilhelm II]] and [[Duke Henry of Mecklenburg-Schwerin|Prince Henry of the Netherlands]], representing his wife's [[House of Orange-Nassau]].{{sfn|Hildebrand Röhr & Steinmetz|p=135}}

[[Fitting-out]] work was delayed significantly when a dockyard worker accidentally removed a blanking plate from a large pipe, which allowed a significant amount of water to flood the ship. The ship did not have its [[watertight bulkhead]]s installed, so the water spread throughout the ship and caused it to list to port and sink {{convert|1.6|m|abbr=on}} to the bottom of the dock. The ship had to be pumped dry and cleaned out, which proved to be a laborious task.{{sfn|Staff|p=23}} The ship was completed by the end of September 1909. She was commissioned into the High Seas Fleet on 1 October 1909,{{sfn|Gröner|p=23}} and [[sea trial|trials]] commenced immediately.{{sfn|Staff|p=23}} {{HMS|Dreadnought|1906|6}}, the ship that spurred ''Nassau''{{'}}s construction, had been launched 25&nbsp;months before ''Nassau'', on 2 February 1906.{{sfn|Gardiner & Gray|p=21}} The ship cost the German navy 37,399,000&nbsp;gold marks.{{sfn|Gröner|p=23}}

[[File:First and second battleship squadrons and small cruiser of the - NARA - 533188-2 restored.jpg|thumb|''Nassau'' and the rest of the [[I Battle Squadron]] in [[Kiel]] before the war]]
On 16 October 1909, ''Nassau'' and her sister {{SMS|Westfalen||2}} participated in a ceremony for the opening of the new third entrance in the Wilhelmshaven Naval Dockyard.{{sfn|Staff|pp=23–24}} They took part in the annual maneuvers of the [[High Seas Fleet]] in February 1910 while still on trials. ''Nassau'' finished her trials on 3 May and joined the newly created [[I Battle Squadron]] of the High Seas Fleet. Over the next four years, the ship participated in the regular series of squadron and fleet maneuvers and training cruises. The one exception was the summer training cruise for 1912 when, due to the [[Agadir Crisis]], the cruise only went into the Baltic.{{sfn|Staff|p=24}} On 14 July 1914, the annual summer cruise to Norway began. The threat of war caused Kaiser Wilhelm II to cancel the cruise after two weeks, and by the end of July the fleet was back in port.{{sfn|Staff|p=11}} War between [[Austria-Hungary]] and [[Kingdom of Serbia|Serbia]] broke out on the 28th, and in the span of a week all of the major European powers had joined the conflict.{{sfn|Heyman|p=xix}}

''Nassau'' participated in most of the fleet advances into the North Sea throughout the war.{{sfn|Staff|p=26}} The first operation was conducted primarily by Rear Admiral [[Franz von Hipper]]'s [[battlecruiser]]s; the ships bombarded the English coastal towns of [[Raid on Scarborough, Hartlepool and Whitby|Scarborough, Hartlepool, and Whitby]] on 15–16 December 1914.{{sfn|Tarrant|p=31}} A German battlefleet of 12 dreadnoughts—including ''Nassau''—and eight [[pre-dreadnought battleship|pre-dreadnoughts]] sailed in support of the battlecruisers. On the evening of 15 December, they came to within {{convert|10|nmi|abbr=on}} of an isolated squadron of six British battleships. Skirmishes in the darkness between the rival [[destroyer]] screens convinced the German fleet commander, Admiral [[Friedrich von Ingenohl]], that the entire [[Grand Fleet]] was deployed before him. Under orders from the Kaiser to not risk the fleet, von Ingenohl broke off the engagement and turned the battlefleet back towards Germany.{{sfn|Tarrant|pp=31–33}}

=== Battle of the Gulf of Riga ===
{{Main|Battle of the Gulf of Riga}}
[[File:SMS Nassau.png|thumb|SMS ''Nassau'', probably before the war||alt=A large gray battleship moves slowly through calm seas|''Nassau'' underway]]

In August 1915, the German fleet attempted to clear the [[Gulf of Riga]] to facilitate the capture of [[Riga]] by the [[German Army (German Empire)|German Army]]. To do so, the German planners intended to drive off or destroy the Russian naval forces in the area, which included the pre-dreadnought battleship {{ship|Russian battleship|Slava||2}} and a number of gunboats and destroyers. The German naval force would also lay a series of [[naval mine|minefields]] in the northern entrance to the gulf to prevent Russian naval reinforcements from being able to enter the area. The fleet that assembled for the assault included ''Nassau'' and her three sister ships, the four {{sclass-|Helgoland|battleship|2}}s, and the battlecruisers {{SMS|Von der Tann||2}}, {{SMS|Moltke||2}}, and {{SMS|Seydlitz||2}}. The force would operate under the command of Vice Admiral Franz von Hipper. The eight battleships were to provide cover for the forces engaging the Russian flotilla. The first attempt on 8 August was unsuccessful, as it had taken too long to clear the Russian minefields to allow the minelayer {{SMS|Deutschland|1914|2}} to lay a minefield of her own.{{sfn|Halpern|pp=196–197}}

On 16 August 1915, a second attempt was made to enter the gulf: ''Nassau'' and ''Posen'', four light cruisers, and 31&nbsp;torpedo boats managed to breach the Russian defenses.{{sfn|Halpern|p=197}} On the first day of the assault, the German [[minesweeper]] ''T 46'' was sunk, as was the destroyer ''V 99''. The following day, ''Nassau'' and ''Posen'' engaged in an artillery duel with ''Slava'', resulting in three hits on the Russian ship that forced her to retreat. By 19 August, the Russian minefields had been cleared, and the flotilla entered the gulf. Reports of Allied [[submarine]]s in the area prompted the Germans to call off of the operation the following day.{{sfn|Halpern|pp=197–198}} ''Nassau'' and ''Posen'' remained in the Gulf until 21 August, and while there assisted in the destruction of the [[gunboat]]s {{ship|Russian gunboat|Sivuch||2}} and {{ship|Russian gunboat|Korietz||2}}.{{sfn|Staff|p=24}} Admiral Hipper later remarked that,
<blockquote>
"To keep valuable ships for a considerable time in a limited area in which enemy submarines were increasingly active, with the corresponding risk of damage and loss, was to indulge in a gamble out of all proportion to the advantage to be derived from the occupation of the Gulf ''before'' the capture of Riga from the land side."{{sfn|Halpern|p=198}}
</blockquote>

=== Battle of Jutland ===
[[File:Map of the Battle of Jutland, 1916.svg|thumb|350px|Maps showing the maneuvers of the British (blue) and German (red) fleets on 31 May&nbsp;– 1 June 1916]]
{{Main|Battle of Jutland}}
''Nassau'' took part in the inconclusive Battle of Jutland on 31 May&nbsp;– 1 June 1916, in the II Division of the I&nbsp;Battle Squadron. For the majority of the battle, the I&nbsp;Battle Squadron formed the center of the [[line of battle]], behind Rear Admiral Behncke's III&nbsp;Battle Squadron, and followed by Rear Admiral Mauve's elderly pre-dreadnoughts of the II&nbsp;Battle Squadron. ''Nassau'' was the third ship in the group of four, behind ''Rheinland'' and ahead of ''Westfalen''; ''Posen'' was the squadron's flagship.{{sfn|Tarrant|p=286}} When the German fleet reorganized into a nighttime cruising formation, the order of the ships was inadvertently reversed, and so ''Nassau'' was the second ship in the line, astern of ''Westfalen''.{{sfn|Tarrant|p=203}}

Between 17:48 and 17:52, eleven German dreadnoughts, including ''Nassau'', engaged and opened fire on the British 2nd Light Cruiser Squadron; ''Nassau''{{'}}s target was the cruiser {{HMS|Southampton|1912|2}}. ''Nassau'' is believed to have scored one hit on ''Southampton'', at approximately 17:50 at a range of {{convert|20100|yd|m|abbr=on}}, shortly after she began firing. The shell struck ''Southampton'' obliquely on her port side, and did not cause significant damage.{{sfn|Campbell|p=54}} ''Nassau'' then shifted her guns to the cruiser {{HMS|Dublin|1912|2}}; firing ceased by 18:10.{{sfn|Campbell|p=99}} At 19:33, ''Nassau'' came into range of the British battleship {{HMS|Warspite|03|2}}; her main guns fired briefly, but after the 180&nbsp;degree turn by the German fleet, the British ship was no longer within reach.{{sfn|Campbell|p=154}}

''Nassau'' and the rest of the I&nbsp;Squadron were again engaged by British light forces shortly after 22:00, including the light cruisers {{HMS|Caroline|1914|2}}, {{HMS|Comus|1914|2}}, and {{HMS|Royalist|1915|2}}. ''Nassau'' followed her sister ''Westfalen'' in a 68° turn to starboard in order to evade any [[torpedo]]es that might have been fired. The two ships fired on ''Caroline'' and ''Royalist'' at a range of around {{convert|8000|yd|m|abbr=on}}.{{sfn|Campbell|p=257}} The British ships turned away briefly, before turning about to launch torpedoes.{{sfn|Campbell|pp=257–258}} ''Caroline'' fired two at ''Nassau''; the first passed close to her bows and the second passed under the ship without exploding.{{sfn|Campbell|p=258}}

[[File:HMSSpitfireJutlanddamage.jpg|thumb|left|Damage to HMS ''Spitfire'' after being rammed by ''Nassau'']]
At around midnight on 1 June, the German fleet was attempting to pass behind the British Grand Fleet when it encountered a line of British destroyers. ''Nassau'' came upon the destroyer {{HMS|Spitfire|1912|2}}, and in the confusion, attempted to ram her. ''Spitfire'' tried to evade, but could not maneuver away fast enough, and the two ships collided. ''Nassau'' fired her forward 11-inch guns at the destroyer.  They could not depress low enough for ''Nassau'' to be able to score a hit; nonetheless, the blast from the guns destroyed ''Spitfire''{{'}}s [[bridge (nautical)|bridge]]. At that point, ''Spitfire'' was able to disengage from ''Nassau'', and took with her a 6&nbsp;m (20&nbsp;ft) portion of ''Nassau''{{'}}s side plating. The collision disabled one of ''Nassau''{{'}}s 15&nbsp;cm (5.9&nbsp;in) guns, and left a 3.5&nbsp;m (11.5&nbsp;ft) gash above the waterline; this slowed the ship to {{convert|15|kn}} until it could be repaired.{{sfn|Tarrant|p=220}} During the confused action, ''Nassau'' was hit by two {{convert|4|in|cm|abbr=on}} shells from the British destroyers, which damaged her searchlights and inflicted minor casualties.{{sfn|Campbell|p=287}}

Shortly after 01:00, ''Nassau'' and {{SMS|Thüringen||2}} encountered the British armored cruiser {{HMS|Black Prince|1904|2}}. ''Thüringen'' opened fire first, and pummeled ''Black Prince'' with a total of 27&nbsp;heavy-caliber shells and 24&nbsp;shells from her secondary battery. ''Nassau'' and {{SMS|Ostfriesland||2}} joined in, followed by {{SMS|Friedrich der Grosse|1911|2}}. The heavy fire quickly disabled the British cruiser and set her alight; following a tremendous explosion, she sank, taking her entire crew with her.{{sfn|Tarrant|p=225}} The sinking ''Black Prince'' was directly in the path of ''Nassau''; to avoid the wreck, the ship had to steer sharply towards the III&nbsp;Battle Squadron. It was necessary for ''Nassau'' to reverse her engines to full speed astern to avoid a collision with {{SMS|Kaiserin||2}}. ''Nassau'' then fell back into a position between the pre-dreadnoughts {{SMS|Hessen||2}} and {{SMS|Hannover||2}}.{{sfn|Tarrant|p=225}} At around 03:00, several British destroyers attempted another torpedo attack on the German line. At approximately 03:10, three or four destroyers appeared in the darkness to port of ''Nassau''; at a range of between {{convert|5500|yd|m|abbr=on}} to {{convert|4400|yd|m|abbr=on}}, ''Nassau'' briefly fired on the ships before turning away 90° to avoid torpedoes.{{sfn|Campbell|p=300}}

Following her return to German waters, ''Nassau'', her sisters ''Posen'' and ''Westfalen'', and the ''Helgoland''-class battleships {{SMS|Helgoland||2}} and ''Thüringen'', took up defensive positions in the [[Jadebusen|Jade]] [[roadstead]] for the night.{{sfn|Tarrant|p=263}} In the course of the battle, ''Nassau'' was hit twice by secondary shells, though these hits caused no significant damage.{{sfn|Tarrant|p=296}} Her casualties amounted to 11&nbsp;men killed and 16&nbsp;men wounded.{{sfn|Tarrant|p=298}} During the course of the battle, she fired 106&nbsp;main battery shells and 75&nbsp;rounds from her secondary guns.{{sfn|Tarrant|p=292}} Repairs were completed quickly, and ''Nassau'' was back with the fleet by 10 July 1916.{{sfn|Campbell|p=336}}

=== Later operations ===
[[File:SMS Nassau illustration.jpg|thumb|left|A recognition drawing prepared by the Royal Navy, with ''Nassau''{{'}}s main battery turned to starboard]]
[[Action of 19 August 1916|Another fleet advance]] followed on 18–22&nbsp;August, during which the I&nbsp;Scouting Group battlecruisers were to bombard the coastal town of [[Sunderland, Tyne and Wear|Sunderland]] in an attempt to draw out and destroy Beatty's battlecruisers. As only two of the four German battlecruisers were still in fighting condition, three dreadnoughts were assigned to the Scouting Group for the operation: {{SMS|Markgraf||2}}, {{SMS|Grosser Kurfürst|1913|2}} (or ''Großer''{{efn|name=sharp S}} ''Kurfürst''), and the newly commissioned {{SMS|Bayern||2}}. The High Seas Fleet, including ''Nassau'',{{sfn|Staff|p=24}} would trail behind and provide cover.{{sfn|Massie|p=682}} At 06:00 on 19 August, ''Westfalen'' was torpedoed by the British submarine {{HMS|E23}} {{convert|55|nmi}} north of [[Terschelling]]; the ship remained afloat and was detached to return to port.{{sfn|Staff|p=26}} The British were aware of the German plans and [[sortie]]d the Grand Fleet to meet them. By 14:35, Admiral Scheer had been warned of the Grand Fleet's approach and, unwilling to engage the whole of the Grand Fleet just 11&nbsp;weeks after the close call at Jutland, turned his forces around and retreated to German ports.{{sfn|Massie|p=683}}

Another sortie into the North Sea followed on 19–20 October. On 21 December, ''Nassau'' ran aground in the mouth of the [[Elbe]]. She was able to free herself, and repairs were effected in [[Hamburg]] at the Reihersteig Dockyard until 1 February 1917.{{sfn|Staff|p=24}} The ship was part of the force that steamed to Norway to intercept a heavily escorted British convoy on 23–25 April, though the operation was canceled when the battlecruiser ''Moltke'' suffered mechanical damage and had to be towed back to port.{{sfn|Massie|p=748}} ''Nassau'', ''Ostfriesland'', and ''Thüringen'' were formed into a special unit for Operation ''Schlußstein'', a planned occupation of [[Saint Petersburg]].{{sfn|Staff|pp=43–44}} On 8 August, ''Nassau'' took on 250 soldiers in Wilhelmshaven and then departed for the Baltic.{{sfn|Staff|p=24}} The three ships reached the Baltic on 10 August, but the operation was postponed and eventually canceled.{{sfn|Staff|pp=43–44}} The special unit was dissolved on 21 August, and the battleships were back in Wilhelmshaven on the 23rd.{{sfn|Staff|pp=44, 46}}

''Nassau'' and her three sisters were to have taken part in a [[Naval order of 24 October 1918|final fleet action]] at the end of October 1918, days before the [[Armistice of 11 November 1918|Armistice]] was to take effect. The bulk of the High Seas Fleet was to have sortied from their base in Wilhelmshaven to engage the British Grand Fleet; Scheer—by now the [[Grand Admiral]] (''Großadmiral'') of the fleet—intended to inflict as much damage as possible on the British navy, to improve Germany's bargaining position, despite the expected casualties. Many of the war-weary sailors felt that the operation would disrupt the peace process and prolong the war.{{sfn|Tarrant|pp=280–281}} On the morning of 29 October 1918, the order was given to sail from Wilhelmshaven the following day. Starting on the night of 29 October, sailors on ''Thüringen'' and then on several other battleships [[Wilhelmshaven mutiny|mutinied]].{{sfn|Tarrant|pp=281–282}} The unrest ultimately forced Hipper and Scheer to cancel the operation.{{sfn|Tarrant|p=282}}

=== Fate ===
Following the German collapse in November 1918, a significant portion of the High Seas Fleet was interned in [[Scapa Flow]]. ''Nassau'' and her three sisters were not among the ships listed for internment, so they remained at German ports.{{sfn|Hore|p=67}} During this period, from November to December, [[Hermann Bauer]] served as the ship's commander.{{sfn|Hildebrand Röhr & Steinmetz|p=133}} On 21 June 1919, Rear Admiral [[Ludwig von Reuter]], under the mistaken impression that the Armistice would expire at noon that day, [[Scuttling of the German fleet in Scapa Flow|ordered his ships be scuttled]] to prevent their seizure by the British.{{sfn|Herwig|p=256}} As a result, the four ''Nassau''-class ships were ceded to the various Allied powers as replacements for the ships that had been sunk.{{sfn|Hore|p=67}} ''Nassau'' was awarded to Japan on 7 April 1920, though the Japanese had no need for the ship. They, therefore, sold her in June 1920 to British shipbreakers, who scrapped the ship in [[Dordrecht]].{{sfn|Gröner|p=23}}

== Notes ==
{{portal|Battleships}}

'''Footnotes'''
{{notes
 | notes =
{{efn
 | name = SMS
 | "SMS" stands for "''[[Seiner Majestät Schiff]]''", or "His Majesty's Ship" in German.
}}

{{efn
 | name = gun nomenclature
 | In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun quick firing, while "L/45" provides the length of the gun regarding the diameter of the barrel. In this case, the L/45 gun is 45 [[Caliber (artillery)|caliber]], which means that the gun is 45&nbsp;times as long as its diameter.
}}

{{efn
 | name = sharp S
 | This is the German "sharp S"; see [[ß]].
}}

}}

'''Citations'''
{{reflist|20em}}

== References ==
* {{cite book
  | last = Campbell
  | first = John
  | year = 1998
  | title = Jutland: An Analysis of the Fighting
  | publisher = Conway Maritime Press
  | location = London
  | isbn = 978-1-55821-759-1
  | ref = {{sfnRef|Campbell}}
 }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
 }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
 }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
 }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title="Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
 }}
* {{cite book
  | last = Heyman
  | first = Neil M.
  | year = 1997
  | title = World War I
  | publisher = Greenwood Publishing Group
  | location = Westport, CT
  | isbn = 978-0-313-29880-6
  | ref = {{sfnRef|Heyman}}
 }}
* {{cite book
  | last1 = Hildebrand
  | first1 = Hans H.
  | last2 = Röhr
  | first2 = Albert
  | last3 = Steinmetz
  | first3 = Hans-Otto
  | year = 1993
  | title = Die Deutschen Kriegsschiffe
  | volume = 6
  | location = [[Ratingen]]
  | publisher = Mundus Verlag
  | isbn = 978-3-8364-9743-5
  |asin=B003VHSRKE |asin-tld=de
  | ref = {{sfnRef|Hildebrand Röhr & Steinmetz}}
 }}
* {{cite book
  | last = Hore
  | first = Peter
  | year = 2006
  | title = Battleships of World War I
  | publisher = Southwater Books
  | location = London
  | isbn = 978-1-84476-377-1
  | oclc = 77797289
  | ref = {{sfnRef|Hore}}
 }}
* {{cite book
  | last = Hough
  | first = Richard
  | authorlink = Richard Hough
  | year = 2003
  | title = Dreadnought: A History of the Modern Battleship
  | publisher = Periscope Publishing
  | location = Penzance, Cornwall, UK
  | isbn = 978-1-904381-11-2
  | ref = {{sfnRef|Hough}}
 }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = New York City
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
 }}
* {{cite book
  | last = Staff
  | first = Gary
  | year = 2010
  | title = German Battleships: 1914–1918
  | volume = 1
  | publisher = Osprey Books
  | location = Oxford
  | isbn = 978-1-84603-467-1
  | oclc = 705750106
  | ref = {{sfnRef|Staff}}
 }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
 }}
* {{cite book
  | last = Breyer
  | first = Siegfried
  | year = 1970
  | title = Schlachtschiffe und Schlachtkreuzer 1905–1970
  | publisher = J. F. Lehmanns Verlag
  | location = München
  | isbn = 3-88199-474-2
 }}

{{Nassau class battleship}}
{{featured article}}

{{DEFAULTSORT:Nassau}}
[[Category:Nassau-class battleships]]
[[Category:World War I battleships of Germany]]
[[Category:Ships built in Wilhelmshaven]]
[[Category:1908 ships]]