{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name = Gz II Godz
| Type = studio album
| Artist = [[G-Side]]
| Cover = 
| Alt = 
| Released = {{Start date|2014|05|15}}
| Recorded = 
| Genre = [[hip hop music|Hip hop]]
| Length = {{Duration|m=55|s=37}} 
| Label = 
| Producer = Block Beattaz, Stacktrace, Blaque
| Last album = ''[[Island (G-Side album)|Island]]''<br/>(2011)
| This album = '''''Gz II Godz'''''<br/>(2014)
| Next album = 
}}
{{Album ratings
| rev1 = [[Exclaim!]]
| rev1score = 8/10<ref name=exclaim>{{cite web|url=http://exclaim.ca/Music/article/g-side-gz_to_godz|title=G-Side: Gz II Godz|work=[[Exclaim!]]|first=Samantha|last=O'Connor|date=May 15, 2014|accessdate=June 3, 2015}}</ref>
| rev2 = [[Pitchfork Media]]
| rev2score = 7.2/10<ref name=pitchfork>{{cite web|url=http://pitchfork.com/reviews/albums/19413-g-side-gz-ii-godz/|title=G-Side: Gz II Godz|work=[[Pitchfork Media]]|first=David|last=Turner|date=May 29, 2014|accessdate=June 3, 2015}}</ref>
| rev3 = [[Spin (magazine)|Spin]]
| rev3score = 8/10<ref name=spin>{{cite web|url=http://www.spin.com/2014/05/g-side-gz-ii-gdz/|title=With Comeback ‘Gz II Godz,’ G-Side’s Trippy Trap Thrives on the Other Side of Hype|work=[[Spin (magazine)|Spin]]|first=Brandon|last=Soderberg|date=May 22, 2014|accessdate=June 3, 2015}}</ref>
}}
'''''Gz II Godz''''' is a studio album by American hip hop duo [[G-Side]].<ref>{{cite web|url=http://www.stereogum.com/1681596/stream-g-side-gz-ii-godz/mp3s/album-stream/|title=Stream G-Side: Gz II Godz|work=[[Stereogum]]|first=Tom|last=Breihan|date=May 14, 2014|accessdate=June 3, 2015}}</ref> It was released on May 15, 2014.<ref>{{cite web|url=http://www.thefader.com/2014/05/14/g-sides-back-stream-their-gz-ii-godz-visual-album-now|title=G-Side's Back, Stream their Gz II Godz Visual Album Now|work=[[The Fader]]|first=Zara|last=Golden|date=May 14, 2014|accessdate=June 3, 2015}}</ref> The cover art is designed by John Turner Jr.<ref>{{cite web|url=http://www.al.com/entertainment/index.ssf/2014/05/g-side_huntsville_rap_duo_stre.html|title=G-Side, Huntsville rap duo, streaming upcoming album 'movie' YouTube, release date set|work=[[AL.com]]|first=Matt|last=Wake|date=May 14, 2014|accessdate=June 3, 2015}}</ref>

==Reception==
David Turner of [[Pitchfork Media]] gave ''Gz II Godz'' a 7.2 out of 10, saying: "Honesty has always been located at the core of G-Side's music, but they remain striving nonetheless, and ''Gz II Godz'' is the work of a duo that have only gotten wiser since they last rhymed together."<ref name=pitchfork/> Brandon Soderberg of [[Spin (magazine)|Spin]] gave the album an 8 out of 10, describing it as "quintessential G-Side: tragically optimistic trappers trying to make good when they’ve been handed a fairly garbage hand."<ref name=spin/> Samantha O'Connor of [[Exclaim!]] said: "In an era drowning in 808s and synths, the diverse project is an invigorating buffet, proving that the pair knows who they are sonically but aren't afraid to experiment."<ref name=exclaim/>

On May 20, 2014, it was chosen by [[Stereogum]] as the Album of the Week.<ref>{{cite web|url=http://www.stereogum.com/1682541/album-of-the-week-g-side-gz-ii-godz/franchises/album-of-the-week/|title=Album Of The Week: G-Side Gz II Godz|work=[[Stereogum]]|first=Tom|last=Breihan|date=May 20, 2014|accessdate=June 3, 2015}}</ref>

==Track listing==
{{Track listing
| extra_column = Producer(s)
| title1 = Resurrection
| extra1 = Block Beattaz
| length1 = 0:55
| title2 = G Side Back
| extra2 = Block Beattaz
| length2 = 1:30
| title3 = Statue
| extra3 = Block Beattaz
| length3 = 3:36
| title4 = I Do
| note4 = featuring Mic Strange
| extra4 = Block Beattaz
| length4 = 4:40
| title5 = 2004
| note5 = featuring Joi Tiffany
| extra5 = Block Beattaz
| length5 = 4:19
| title6 = Dead Fresh
| note6 = featuring Kristmas and Grilly
| extra6 = Block Beattaz, STACKTRACE
| length6 = 4:10
| title7 = Higher
| note7 = featuring Joi Tiffany
| extra7 = Block Beattaz
| length7 = 3:33
| title8 = Bassheadz
| note8 = featuring G Mane
| extra8 = Block Beattaz, Blaque
| length8 = 4:57 
| title9 = Gold
| note9 = featuring Ink
| extra9 = Block Beattaz
| length9 = 4:29
| title10 = In Luv with Jhene Aiko
| extra10 = Block Beattaz
| length10 = 4:01
| title11 = Elbow Smash
| extra11 = Block Beattaz
| length11 = 4:09
| title12 = Muffins
| extra12 = Block Beattaz
| length12 = 4:20
| title13 = 1 Thing
| extra13 = Block Beattaz
| length13 = 2:40
| title14 = Create
| note14 = featuring Codie Global
| extra14 = Block Beattaz
| length14 = 4:27
| title15 = Last Words
| extra15 = Block Beattaz
| length15 = 0:39
}}

==References==
{{reflist}}

[[Category:2014 albums]]
[[Category:G-Side albums]]