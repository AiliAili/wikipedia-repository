{{good article}}
{|{{Infobox ship begin |infobox caption=''Bruix''}}
{{Infobox ship image
|Ship image=[[File:Amiral charner.jpg|300px]]
|Ship caption=[[Sister ship]] {{ship|French cruiser|Amiral Charner||2}} at anchor, c. 1897
}}
{{Infobox ship career
|Hide header=
|Ship country=France
|Ship flag={{shipboxflag|France|naval}}
|Ship name=''Bruix''
|Ship namesake=[[Étienne Eustache Bruix]]
|Ship ordered=
|Ship builder=[[Arsenal de Rochefort]]
|Ship laid down=September 1890
|Ship launched=2 August 1894
|Ship completed=
|Ship commissioned= 1 December 1896
|Ship recommissioned=
|Ship decommissioned=
|Ship in service=
|Ship out of service=
|Ship refit=
|Ship struck=21 June 1920
|Ship fate=Sold for [[ship breaking|scrap]], 21 June 1921
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{Sclass-|Amiral Charner|cruiser|0}} [[armored cruiser]]
|Ship displacement={{convert|4748|t|LT|0}}
|Ship length={{convert|110.2|m|ftin|abbr=on}}
|Ship beam={{convert|14.04|m|ftin|abbr=on}}
|Ship draft={{convert|6.06|m|ftin|abbr=on}}
|Ship power=*16 × [[Belleville boiler]]s
*{{convert|9000|ihp|0|abbr=on|lk=in}}
|Ship propulsion=2 screws; 2 × [[Marine steam engine#Triple or multiple expansion|Triple-expansion steam engines]]
|Ship speed={{convert|17|kn|lk=in}}
|Ship range={{convert|4000|nmi|lk=in|abbr=on}} at {{convert|10|kn}}
|Ship complement=16 officers and 378 enlisted men
|Ship crew=
|Ship armament=*2 single Mle 1887 {{convert|194|mm|abbr=on}} guns
*6 single Mle 1887 {{convert|138|mm|abbr=on}} guns
*4 single [[quick-firing gun|QF]] Mle 1891 {{convert|65|mm|abbr=on}} guns
*4 single QF Mle 1885 {{convert|47|mm|abbr=on}} [[Hotchkiss gun]]s
*8 single 5-barreled {{convert|37|mm|abbr=on}} revolver Hotchkiss guns
*4 × 450 mm (17.7 in) [[torpedo tube]]s
|Ship armor=*[[Belt armor|Waterline belt]]: {{convert|60|-|90|mm|in|abbr=on|1}}
*[[Deck (ship)|Deck]]: {{convert|40|-|50|mm|in|abbr=on|1}}
*[[Gun turret]]s: {{convert|92|mm|in|abbr=on|1}}
*[[Conning tower]]: {{convert|92|mm|in|abbr=on|1}}

}}
|}
'''''Bruix''''' was one of four {{Sclass-|Amiral Charner|cruiser|0}} [[armored cruiser]]s built for the French Navy in the 1890s. She served in the Atlantic Ocean, the Mediterranean, and in the [[Far East]] before [[World War I]]. In 1902 she aided survivors of the devastating eruption of [[Mount Pelée]] on the island of [[Martinique]] and spent several years as [[guardship]] at [[Crete]], protecting French interests in the region in the early 1910s.

At the beginning of the war in August 1914, ''Bruix'' was assigned to protect troop convoys from [[French North Africa]] to France before she was transferred to the Atlantic to support [[West Africa Campaign (World War I)#Kamerun|Allied operations against the German colony of Kamerun]] in September. She was briefly assigned to support [[Naval operations in the Dardanelles Campaign|Allied operations in the Dardanelles]] in early 1915 before she began patrolling the Aegean Sea and Greek territorial waters.

The ship was [[Ship decommissioning|decommissioned]] in Greece at the beginning of 1918 and recommissioned after the end of the war in November for service in the [[Black Sea]] against the [[Bolshevik]]s. ''Bruix'' returned home later in 1919 and was reduced to [[Reserve fleet|reserve]] before she was sold for [[ship breaking|scrap]] in 1921.

==Design and description==
[[File:Amiral-Charner Brassey's1902.png|thumb|left|Line drawing from [[Brassey's Naval Annual]] 1902]]
The ''Amiral Charner''-class ships were designed to be smaller and cheaper than the preceding armored cruiser design, the {{ship|French cruiser|Dupuy de Lôme||2}}. Like the older ship, they were intended to fill the commerce-raiding strategy of the [[Jeune École]].<ref>Feron, pp. 8–9</ref>

The ship measured {{convert|106.12|m|ftin|sp=us}} [[Length between perpendiculars|between perpendiculars]], with a [[beam (nautical)|beam]] of {{convert|14.04|m|ftin|sp=us}}. ''Bruix'' had a forward [[draft (ship)|draft]] of {{convert|5.55|m|ftin|sp=us}} and drew {{convert|6.06|m|ftin|sp=us}} aft. She [[Displacement (ship)|displaced]] {{convert|4748|t|LT|sp=us}} at normal load and {{convert|4990|t|LT|sp=us}} at [[deep load]].<ref name=f8>Feron, p. 15</ref>

The ''Amiral Charner'' class had two 4-cylinder [[Marine steam engine#Triple or multiple expansion|triple-expansion steam engine]]s, each driving a single [[propeller shaft]]. Steam for the engines was provided by 16 [[Belleville boiler]]s and they were rated at a total of {{convert|9000|PS|lk=on}} using [[Mechanical draft|forced draught]]. ''Bruix'' had a designed speed of {{convert|19|knots|lk=in}}, but only reached a maximum speed of {{convert|18.37|kn}} from {{convert|9107|PS}} during [[sea trial]]s on 15 September 1896. The ship carried up to {{convert|535|t|sp=us}} of coal and could steam for {{convert|4000|nmi|lk=in}} at a speed of {{convert|10|kn}}.<ref>Feron, pp. 15, 17, 25</ref>

The ships of the ''Amiral Charner'' class had a [[Main battery|main armament]] that consisted of two [[Canon de 194 mm Modèle 1887]] guns that were mounted in single [[gun turret]]s, one each fore and aft of the [[superstructure]]. Their secondary armament comprised six [[Canon de 138.6 mm Modèle 1887]] guns, each in single gun turrets on each [[broadside]]. For anti-[[torpedo boat]] defense, they carried four {{convert|65|mm|in|adj=on|sp=us}} guns, four {{convert|47|mm|in|adj=on|sp=us}} and eight {{convert|37|mm|in|adj=on|sp=us}} five-barreled revolving [[Hotchkiss gun]]s. They were also armed with four {{convert|450|mm|in|adj=on|1|sp=us}} pivoting [[torpedo tube]]s; two mounted on each broadside above water.<ref>Feron, pp. 11, 15</ref>

The side of the ''Amiral Charner'' class was generally protected by {{convert|92|mm|sp=us}} of steel armor, from {{convert|1.3|m|ftin|sp=us}} below the [[waterline]] to {{convert|2.5|m|ftin|sp=us}} above it. The bottom {{convert|20|cm|in|1|sp=us}} tapered in thickness and the armor at the ends of the ships thinned to {{convert|60|mm|sp=us}}. The curved protective [[deck (ship)|deck]] had a thickness of {{convert|40|mm|sp=us}} along its centerline that increased to {{convert|50|mm|sp=us}} at its outer edges. Protecting the boiler rooms, engine rooms, and [[magazine (artillery)|magazine]]s below it was a thin splinter deck. A watertight internal [[cofferdam]], filled with [[cellulose]], ran the length of the ship from the protective deck<ref name=f3>Feron, pp. 12, 15</ref> to a height of {{convert|4|ft|m|1|disp=flip|sp=us}} above the waterline.<ref>Chesneau & Kolesnik, p. 304</ref> The ship's [[conning tower]] and turrets were protected by 92 millimeters of armor.<ref name=f3/>

==Construction and career==
''Bruix'', named after Admiral [[Étienne Eustache Bruix]],<ref>Silverstone, p. ?</ref> was [[keel laying|laid down]] at the [[Arsenal de Rochefort]] on 9 November 1891. She was [[Ship naming and launching|launched]] on 2 August 1894 and [[Ship commissioning|commissioned]] for trials on 15 April 1896. The ship was temporarily assigned to the Northern Squadron (''Escadre du Nord'') on 24 November for the visit of [[Tsar]] [[Nicholas II of Russia]] and his wife to [[Dunkerque]] on 5–9 October. The ship's steering broke down on 7 October and she had to return to Rochefort for repairs. Trials lasted until early December and ''Bruix'' was officially commissioned on 15 December and assigned to the Northern Squadron.<ref>Feron, p. 25</ref>

On 18 August 1897, together with the [[protected cruiser]] {{ship|French cruiser|Surcouf||2}}, she escorted the armored cruiser {{ship|French cruiser|Pothuau||2}} that carried [[President of France|President]] [[Félix Faure]] on a state visit to Russia. Shortly after departure, ''Bruix'' fractured a [[piston rod]] in her port engine which forced her to return to port. Her repairs and armament trials lasted until January 1898, although the last of the trials was not completed until 25 February. ''Bruix'' was then assigned to the Far Eastern Squadron where she was based at [[Saigon]], [[French Indochina]] until October,<ref>Feron, pp. 25, 27</ref> although she made a port visit to [[Manila]], the Philippines, on 5 May after the American victory in the [[Battle of Manila Bay]].<ref>Cooling, p. 95</ref> While returning home in November, she damaged her starboard propeller on the 20th while transiting the [[Suez Canal]]. The ship reached Toulon on the 28th and was under repair until January 1899 before rejoining the Northern Squadron on 3 February.<ref name=f7/>

''Bruix'' made port visits in Spain and Portugal in June before another piston rod was damaged on the 7th. She began a refit on 20 September that lasted until 4 November that modified her for service as a flagship. On 20 November the ship became the flagship of a cruiser [[division (naval)|division]]. In 1901 she participated in the annual fleet maneuvers with the rest of the Northern Squadron. During this training the British steamer {{SS|Paddington}} collided with the ship, lightly damaging the plating of her armored ram on 27 June. [[Bilge keels]] were fitted to ''Bruix'' in November–December and she remained at the dockyard until 10 January 1902 to evaluate the operation of her turrets. The ship was assigned to the Atlantic Division in April and visited several Spanish ports during the month and into May.<ref name=f7>Feron, p. 27</ref>

After the devastating eruption of Mount Pelée on 5 May, ''Bruix'', now the flagship of [[Rear Admiral]] (''contre-amiral'') [[Palma Gourdon]],<ref>Naval Notes June 1902, p. 822</ref> was ordered to [[Fort-de-France]] to render assistance to the survivors where she remained until 19 August.<ref name=f7/> On 30 November Rear Admiral [[Joseph Bugard]] hoisted his flag aboard ''Bruix''.<ref>Naval Notes December 1902, p. 1603</ref> The ship spent most of the next several years either commissioned with a reduced complement or assigned to the reserve. The ship was reactivated in late 1906 for service with the Far Eastern Squadron and departed Toulon on 15 November, accompanied by her [[sister ship]] {{ship|French cruiser|Chanzy||2}}. They arrived at Saigon on 10 January 1907 and ''Bruix'' was in [[Nagasaki]], Japan when ''Chanzy'' [[Ship grounding|ran aground]] off the Chinese coast on 20 May. The ship then participated in the unsuccessful effort to rescue her sister. ''Bruix'' spent most of her tour in the Far East [[wikt:Showing the flag|showing the flag]] in Russia, China and Japan and departed for home on 26 April 1909. While passing through the Suez Canal, she accidentally collided with the Italian steamer {{SS|Nilo}} before arriving at Toulon on 2 August. She began an [[wikt:overhaul|overhaul]] several weeks later that was repeatedly delayed by labor shortages at the dockyard. She was finally towed to the dockyard at [[Bizerte]], in [[French North Africa]], in June 1911 and her overhaul was completed in January 1912.<ref name=f28/>

Briefly assigned to the reserve, ''Bruix'' was recommissioned on 13 May for service with the Levant Division as the guardship for Crete. She relieved her sister {{ship|French cruiser|Amiral Charner||2}} at Souda Bay on 9 July and spent the next two years in the [[Levant]].<ref name=f28/> During the [[Italo-Turkish War]], her [[captain (nautical)|captain]] protested the bombardment of fleeing Turkish troops near the port of [[Kalkan]] on 3 October by the protected cruiser {{ship|Italian cruiser|Coatit||2}} as a breach of international law.<ref>Beehler, pp. 94–95</ref> On 8 November the ship assisted in the [[marine salvage|refloating]] of the Russian protected cruiser {{ship|Russian cruiser|Oleg||2}}. Although formally assigned to the Tunisian Squadron on 13 January 1913, ''Bruix'' remained in the Levant. Later in the year, she assisted in the salvage of the steamer {{SS|Sénégal}} that had struck a [[naval mine|mine]] at [[Smyrna]], Turkey, that had been laid by the Italians during the war.<ref name=f28/> In March 1914 ''Bruix'' escorted [[William, Prince of Albania]] during his voyage from Trieste to [[Durazzo]], Albania to take up his throne.<ref>Heaton-Armstrong, Belfield, & Destani, p. 17</ref> The ship returned to Bizerta on 25 April 1914 and began a refit that lasted until July.<ref name=f28/>

When World War I began in August, she was assigned to escort convoys between Morocco and France and general patrols together with her sisters {{ship|French cruiser|Latouche-Tréville||2}} and ''Amiral Charner''. ''Bruix'' was sent to support the Allied campaign against [[Kamerun]] in September and bombarded several small towns as part of her contribution before returning home later in the year.<ref>Corbett, pp. 264–65, 273–74, 368, 370, 397</ref> After several short refits, ''Bruix'' was assigned to the Dardanelles squadron in February 1915 although the ship spent most of her time patrolling the Aegean. On 31 January 1918, she was placed in reserve at [[Salonika]]. ''Bruix'' was recommissioned on 29 November and transferred to [[Constantinople]] where she was assigned to the armored cruiser division of the 2nd Squadron on 2 December. Between March and May 1919 she patrolled the [[Black Sea]] as part of the Allied intervention against the Bolsheviks and took part in the evacuation of German and Allied troops from [[Mykolaiv|Nikolaev]], Ukraine in March and from [[Odessa]] in April. Her crew did not participate in the [[mutiny]] that occurred aboard some French ships in [[Sevastopol]], Crimea in April. ''Bruix'' departed the Black Sea for Constantinople on 5 May and then sailed for Toulon on 22 May where she was assigned to the reserve upon arrival. Proposals that she be converted into an [[accommodation ship]] or a merchant ship were judged impractical and she was stricken from the [[Navy List]] on 21 June 1920. ''Bruix'' was sold for scrap a year later, to the day, together with two other obsolete warships, for the price of 436,000 [[French franc|francs]].<ref name=f28>Feron, p. 28</ref>

== Notes ==
{{Reflist|30em}}

==Bibliography==
* {{Cite book |last=Beehler|first=William Henry|title=The History of the Italian-Turkish War: September 29, 1911, to October 18, 1912|year=1913|location=Annapolis|publisher=United States Naval Institute|url=https://books.google.com/books?id=OWcoAAAAYAAJ&printsec=frontcover#v=onepage&q&f=false|oclc=1408563}}
* {{cite book|title=Conway's All the World's Fighting Ships 1860–1905|editor1-last=Chesneau|editor1-first=Roger|editor2-last=Kolesnik|editor2-first=Eugene M.|publisher=Conway Maritime Press|location=Greenwich|year=1979|isbn=0-8317-0302-4|lastauthoramp=y}}
*{{cite book|last=Cooling|first=Benjamin Franklin|title=USS Olympia: Herald of Empire|year=2000|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=1-55750-148-3}}
* {{cite book|last=Corbett|first=Julian|authorlink=Julian Corbett|title=Naval Operations to the Battle of the Falklands|edition=2nd, reprint of the 1938|series=History of the Great War: Based on Official Documents|volume=I|publisher=Imperial War Museum and Battery Press|location=London and Nashville, Tennessee|isbn=0-89839-256-X}}
*{{cite book|last1=Heaton-Armstrong|first1=Duncan|last2=Belfield|first2=Gervase|last3=Destani|first3= Bejtullah D.|title=The Six Month Kingdom: Albania 1914|year=2005|publisher=I.B. Tauris, in association with the Centre for Albanian Studies|location=London|isbn=1-85043-761-0|lastauthoramp=y}}
*{{cite book|last=Feron|first=Luc|chapter=The Armoured Cruisers of the ''Amiral Charner'' Class|editor=Jordan, John|publisher=Conway|location=London|year=2014|title=Warship 2014|isbn=978-1-84486-236-8}} 
*{{cite journal|date=June 1902|title=Naval Notes|journal=Journal of the Royal United Services Institute|publisher=Royal United Services Institute|volume=46|issue=292|url=https://books.google.com/books?id=eiYmAQAAIAAJ&pg=PA822&dq=cruiser+bruix}} 
*{{cite journal|date=December 1902|title=Naval Notes|journal=Journal of the Royal United Services Institute|publisher=Royal United Services Institute|volume=46|issue=298|url=https://books.google.com/books?id=RgEOAAAAYAAJ&pg=PA1605&dq=cruiser+bruix}} 
* {{cite book|last=Roksund|first=Arne|title=The Jeune École: The Strategy of the Weak|publisher=Brill|location=Leiden|year=2007|series=History of Warfare|volume=43|isbn=978-90-04-15723-1}}
* {{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}

{{Amiral Charner-class cruiser}}

{{DEFAULTSORT:Bruix}}
[[Category:Amiral Charner-class cruisers]]
[[Category:Cruisers of the French Navy]]
[[Category:Ships built in France]]
[[Category:1894 ships]]
[[Category:World War I cruisers of France]]