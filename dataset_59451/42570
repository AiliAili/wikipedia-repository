<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox aircraft begin
 |name= Firecat, Turbo Firecat
 |image= File:Tracker2.jpg
 |caption= A French Turbo Firecat over Sausset-Les-Pins
}}{{Infobox aircraft type
 |type= [[Fire-fighting aircraft]]
 |national origin = [[Canada]] 
 |manufacturer= [[Conair Group|Conair]]
 |designer=
 |first flight=
 |introduced= 1978 (Firecat) <br> 1988 (Turbo Firecat)
 |retired= Retired in Canada 2012
 |status= Active in France with Sécurité Civile (Turbo Firecat only)
 |primary user= [[Conair Group|Conair]]
 |more users= [[Sécurité Civile]]
 |produced=
 |number built= 35
 |developed from= 
 |variants with their own articles=
}}
|}

The '''Conair Firecat''' is a [[fire-fighting aircraft]] developed in [[Canada]] in the 1970s by modifying military surplus [[Grumman S-2 Tracker]]s. The modifications were developed by the maintenance arm of the [[Conair Group]], which became a separate company called [[Cascade Aerospace]].<ref>[http://www.cascadeaerospace.com/aboutus/History/index.htm Cascade Aerospace history] retrieved 2008-01-15.</ref>

==Development==
The Firecats are retrofitted [[Grumman S-2 Tracker]]s. Conair bought a large number of Trackers formerly operated by the [[Canadian Navy]] and a small number of ex-[[United States Navy]] aircraft as well.<ref name=CT/> The Trackers are modified for [[aerial firefighting]] as Firecats by raising the cabin floor by 20&nbsp;cm (8&nbsp;in) and fitting a 3,296 litre (870 U.S. gal) retardant tank where the [[torpedo]] bay is normally located. All superfluous military equipment is removed and the empty weight is almost 1,500&nbsp;kg lower than a Tracker's.<ref name=AWD/> The first aircraft was modified in 1978.<ref name=AWD/>  Some examples have been re-engined with [[turboprop]] engines and are known as '''Turbo Firecats''', these feature a larger tank and extra underwing fuel tanks; the Maximum Take Off Weight (MTOW) is increased by 680&nbsp;kg (1,500&nbsp;lb) to 12,480&nbsp;kg (27,500&nbsp;lb), while the lighter turbine engines also reduce the empty weight. The first Turbo Firecat was produced in 1988.<ref name=AWD>[http://www.aircraftworlddirectory.com/civil/c/conairfirecat.htm Aircraft World Directory Firecat page] retrieved 2008-01-18. {{webarchive |url=https://web.archive.org/web/20111002190617/http://www.aircraftworlddirectory.com/civil/c/conairfirecat.htm |date=2 October 2011 }}</ref>

==Operational history==

Conair commenced Firecat operations in 1978.<ref>Lavender, Bill. [http://www.agairupdate.com/aau/articles/2003/July2003.html "Conair working fires in Canada", ''AgAir Update'' magazine, Perry, GA, July 2003 (online version)]. Retrieved 2008-01-18.</ref> Firecats and Turbo Firecats were previously in service with Conair and the [[Saskatchewan|Government of Saskatchewan]] in Canada<ref name=CT>[http://www.s2ftracker.com/canadatrackers.htm Tracker survivors in Canada] retrieved 2008-01-18.</ref><ref>{{Cite web
| url = http://environment.gov.sk.ca/aviation
| title = Aviation Operations - Environment - Government of Saskatchewan
| website = environment.gov.sk.ca
| access-date = 2016-02-28
}}</ref> and were also used by the [[Ontario|Government of Ontario]].<ref>[http://www.bushplane.com/data/displaydetail.lasso?-KeyValue=display11 Canadian Bushplane Heritage Centre Tracker page] retrieved 2008-01-18.</ref> The [[Sécurité Civile]] organisation in [[France]] took delivery of 14 Firecats over a period of five years commencing in May 1982.<ref name="SC"/> It has had its examples further converted and is now standardized on the Turbo Firecat.<ref>[http://www.s2ftracker.com/francetrackers.htm Tracker survivors in France] retrieved 2008-01-18.</ref> A total of 35 Firecat and Turbo Firecat conversions have been performed;<ref>[http://www.uswarplanes.net/tracker.html USA Warplanes Tracker page] retrieved 2008-01-18.</ref> four Firecats and three Turbo Firecats have crashed in France, reflecting the hazardous nature of firebombing operations.<ref name="SC">[http://www.pilotesdufeu.com/tracker/FIRECAT/Walk_around_P/Caracteristiques_P/Historique_P/body_historique_p.html History of Sécurité Civile Firecat operations] (in French). Retrieved: 18 August 2008.</ref><ref>[http://www.pilotesdufeu.com/tracker/TURBO/Walk_around_T/Caracteristiques_T/Historique_T/body_historique_t.html History of Sécurité Civile Turbo Firecat operations] (in French). Retrieved: 18 August 2008.</ref><ref>[http://www.s2ftracker.com/transition.htm List of Tracker crashes since 2000] retrieved 2008-01-18.</ref> As of 2016; a total of 9 Turbo Firecats remain in service for Sécurité Civile in France.

Similar conversions are performed by another company [[Marsh Aviation]] in the [[United States]]. These are known as Marsh Turbo Trackers and feature [[Garrett AiResearch TPE-331]] turboprop engines.<ref>[http://rgl.faa.gov/regulatory_and_guidance_Library%5CrgMakeModel.nsf/0/9BE12A55DCC29FEA86256B7B0059297F/$FILE/A51NM.pdf Turbo Tracker Type Certificate] retrieved 2008-01-18.</ref>

==Variants==
;Firecat: Original version, fitted with [[Wright R-1820]] radial [[piston engine]]s as fitted to standard Grumman Trackers

;Turbo Firecat: Version fitted with two [[Pratt & Whitney Canada PT6]]A-67AF turboprop engines

<!-- ==Operators== -->

==Aircraft on display==
*[[Canadian Museum of Flight]], Langley, British Columbia<ref name="Niles07Oct12">{{cite news|url = http://www.avweb.com/avwebflash/news/Firecat_GrummanCS2F_Firefighter_Museum_207452-1.html|title = Firecat Goes To Museum|accessdate = 8 October 2012|last = Niles|first = Russ|date = 7 October 2012| work = AVweb}}</ref>
*[[Canadian Bushplane Heritage Centre]], Sault Ste. Marie, Ontario
*[[Reynolds-Alberta Museum]], Wetaskiwin, Alberta

==Specifications (Turbo Firecat) ==
{{aerospecs
|ref=
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met

|crew= 1
|capacity= 3,395 l (897 U.S. gal) of water, plus 173 l (46 U.S. gal) of foam concentrate
|length m= 13.26
|length ft= 43
|length in= 6
|span m= 22.12
|span ft= 72
|span in= 7
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m= 5.05
|height ft= 16
|height in= 7
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=6,803
|empty weight lb=15,000
|gross weight kg=12,473
|gross weight lb=27,500
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=2
|eng1 type=[[Pratt & Whitney Canada PT6A]]-67AF
|eng1 kw=<!-- prop engines -->761
|eng1 hp=<!-- prop engines -->1,220
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=407
|max speed mph=253
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->5
|endurance min=<!-- if range unknown -->6
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
* [[S-2 Tracker]]
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{Reflist|30em}}
* {{cite magazine|last=Stitt|first=Robert M|title=Turbo Firecat...fighting flames with turbines|magazine=[[Air International]]|date=June 1991|pages=289–294|issn=0306-5634}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |page=922 }}
* [http://www.cascadeaerospace.com/products/Turbo%20Firecat/ Manufacturer's website]

==External links==
{{Commons category|S-2 Tracker waterbomber}}
*[http://www.pilotesdufeu.com/tracker/ Tracker, Firecat and Turbo Firecat History and Photos (in French)]

{{Use dmy dates|date=May 2011}}

[[Category:Canadian special-purpose aircraft 1970–1979]]
[[Category:Conair aircraft|Firecat]]
[[Category:Grumman aircraft]]
[[Category:Aerial firefighting]]