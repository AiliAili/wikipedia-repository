'''Acoustic droplet vaporization''' ('''ADV''') is the process by which [[superheating|superheated]] liquid [[droplet]]s are [[phase transition|phase-transitioned]] into [[gas bubble]]s by means of [[ultrasound]].  [[Perfluorocarbon]]s and [[halocarbon]]s are often used for the [[dispersed medium]], which forms the core of the droplet.<ref name="UMBRSUG">{{cite web|url=http://www.ultrasound.med.umich.edu/Projects/ADV.html|title=Acoustic Droplet Vaporization|last=Carson|first=Paul L.|work=University of Michigan Basic Radiological Sciences and Ultrasound Group|publisher=[[University of Michigan]]|accessdate=30 June 2013|display-authors=etal}}</ref>  The [[surfactant]], which forms a stabilizing shell around the dispersive medium, is usually composed of [[albumin]] or [[lipid]]s.<ref name="UMBRSUG" />

There exist two main hypothesis that explain the mechanism by which ultrasound induces [[vaporization]].<ref name="UMBRSUG" />  One poses that the ultrasonic field interacts with the dispersed medium so as to cause vaporization in the bubble core.  The other suggests that shockwaves from [[Cavitation#Intertial cavitation|inertial cavitation]], occurring near or within the droplet, cause the dispersed medium to vaporize.<ref>{{cite journal|last=Kripfgans|first=Oliver D.|date=July 2004|title=On the acoustic vaporization of micrometer-sized droplets|journal=[[Journal of the Acoustical Society of America]]|volume=116|issue=1|pages=272–281|url=http://scitation.aip.org/journals/doc/JASMAN-ft/vol_116/iss_1/272_1.html|bibcode = 2004ASAJ..116..272K |doi = 10.1121/1.1755236 }}</ref>

== See also ==
* [[Acoustic droplet ejection]]

== References ==
{{reflist}}

[[Category:Phase transitions]]