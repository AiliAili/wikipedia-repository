{{Infobox subdivision type
| name               = Provinces of Afghanistan
| alt_name           =''[[wilaya]]t''
| map                = [[File:Afghanistan provinces numbered gray.PNG|250px]]
| category           = [[Unitary state]]
| territory          = [[Islamic Republic of Afghanistan]]
| start_date         =
| current_number     = 34 provinces
| number_date        =
| population_range   = 147,964 ([[Nuristan Province|Nimruz]]) – 4,372,977 ([[Kabul Province|Kabul]])
| area_range         = {{Convert|711|sqmi|km2|order=flip|abbr=on}} ([[Kapisa Province|Kapisa]]) – {{Convert|22,619|sqmi|km2|order=flip|abbr=on}} ([[Helmand Province|Helmand]]) 
| government         = Provincial government, [[Government of Afghanistan|National government]]
| subdivision        = [[Districts of Afghanistan|District]]
}}

[[Afghanistan]] is made up of 34 [[province]]s ({{lang|fa|ولايت}} ''[[Wilayah|wilåyat]]''). The '''provinces of Afghanistan''' are the primary [[administrative division]]s. Each province encompasses a number of [[districts of Afghanistan|districts]] or usually over 1,000 villages. 

Provincial governments are led by a [[List of current governors of Afghanistan|governor]] who is appointed by the [[President of Afghanistan]].<ref>{{cite news|last=Ahmed|first=Azam|title=For Afghan Officials, Facing Prospect of Death Is in the Job Description|url=https://www.nytimes.com/2012/12/09/world/asia/for-afghan-officials-prospect-of-death-comes-with-territory.html|accessdate=18 June 2013|newspaper=[[New York Times]]|date=December 8, 2012}}</ref> Each province is represented in the [[government of Afghanistan]] by two members in the [[House of Elders]]. One is elected by the provincial council to a four-year term while the second is elected by the district councils to a three-year term. Representation in the [[House of the People (Afghanistan)|House of the People]] is directly from the districts, although in each province, two or more of the representatives must be women. They are appointed by the [[President of Afghanistan]]. 

Provincial governors have played a critical role in the reconstruction of the Afghan state following the creation of the new government under [[Hamid Karzai]].<ref>{{Cite book|url=https://doi.org/10.1017/CBO9781139161817.001|title=Building a Theory of Strongman Governance in Afghanistan|last=Mukhopadhyay|first=Dipali|publisher=|year=|isbn=|location=|pages=43|language=en|doi=10.1017/cbo9781139161817.001}}</ref> According to international security scholar, Dipali Mukhopadhyay, many of the provincial governors are former [[warlord]]<nowiki/>s who have been incorporated into the political system.<ref>{{Cite book|url=https://doi.org/10.1017/CBO9781139161817.001|title=Building a Theory of Strongman Governance in Afghanistan|last=Mukhopadhyay|first=Dipali|publisher=|year=|isbn=|location=|pages=43|language=en|doi=10.1017/cbo9781139161817.001}}</ref>

==Provinces of Afghanistan==

[[File:Afghanistan_provinces_named.png|thumb|250px|right|Afghanistan political map- provinces named.]]

{{Politics of Afghanistan}}

{| class="wikitable sortable" style="font-size:90%;"
|+  Provinces of Afghanistan<ref>References and details on data provided in the table can be found within the individual provincial articles.</ref>
|-
!align="left"|Province
!align="left"|Map #
!align="right"|[[ISO 3166-2:AF]]<ref>[[ISO 3166-2:AF]] ([[International Organization for Standardization|ISO]] [[ISO 3166-2|3166-2]] codes for the provinces of Afghanistan)</ref>
!align="left"|Centers
!align="right"|Population (2015)<ref>[http://www.geohive.com/cntry/afghanistan.aspx Afghanistan at GeoHive]</ref>
!align="right"|Area (km²)
!align="right"|# Districts
!align="right"|U.N. Region
|-
| [[Badakhshan Province|Badakhshan]] || style="text-align:center;"| 30 || style="text-align:center;"| AF-BDS || [[Fayzabad, Badakhshan|Fayzabad]] || align=right| 950,953 || align=right| 44,059 || align=right| 29 || North East Afghanistan
|-
| [[Badghis Province|Badghis]] || style="text-align:center;"| 4|| style="text-align:center;"| AF-BDG || [[Qala i Naw, Afghanistan|Qala i Naw]] || align=right| 495,958 || align=right| 20,591 || align=right| 7 || West Afghanistan
|-
| [[Baghlan Province|Baghlan]] || style="text-align:center;"| 19 || style="text-align:center;"| AF-BGL || [[Puli Khumri]] || align=right| 910,784 || align=right| 21,118 || align=right| 16 || North East Afghanistan
|-
| [[Balkh Province|Balkh]] || style="text-align:center;"| 13 || style="text-align:center;"| AF-BAL || [[Mazar-i-Sharif]] || align=right| 1,325,659 || align=right| 17,249 || align=right| 15 || North West Afghanistan
|-
| [[Bamyan Province|Bamyan]] || style="text-align:center;"| 15 || style="text-align:center;"| AF-BAM || [[Bamyan, Afghanistan|Bamyan]] || align=right| 747,218 || align=right| 14,175 || align=right| 7 || Central Afghanistan
|-
| [[Daykundi Province|Daykundi]] || style="text-align:center;"| 10 || style="text-align:center;"| AF-DAY || [[Nili, Afghanistan|Nili]] || align=right| 924,339 || align=right| 18,088 || align=right| 8 || South West Afghanistan
|-
| [[Farah Province|Farah]] || style="text-align:center;"| 2 || style="text-align:center;"| AF-FRA || [[Farah, Afghanistan|Farah]] || align=right| 507,405 || align=right| 48,471 || align=right| 11 || West Afghanistan
|-
| [[Faryab Province|Faryab]] || style="text-align:center;"| 5 || style="text-align:center;"| AF-FYB || [[Maymana]] || align=right| 998,147 || align=right| 20,293 || align=right| 14 || North West Afghanistan
|-
| [[Ghazni Province|Ghazni]] || style="text-align:center;"| 16 || style="text-align:center;"| AF-GHA || [[Ghazni]] || align=right| 1,228,831 || align=right| 22,915 || align=right| 19 || South East Afghanistan
|-
| [[Ghōr Province|Ghor]] || style="text-align:center;"| 6 || style="text-align:center;"| AF-GHO || [[Chaghcharan]] || align=right| 790,296 || align=right| 36,479 || align=right| 10 || West Afghanistan
|-
| [[Helmand Province|Helmand]] || style="text-align:center;"| 7 || style="text-align:center;"| AF-HEL || [[Lashkar Gah]] || align=right| 924,711 || align=right| 58,584 || align=right| 13 || South West Afghanistan
|-
| [[Herat Province|Herat]] || style="text-align:center;"| 1 || style="text-align:center;"| AF-HER || [[Herat]] || align=right| 1,890,202 || align=right| 54,778 || align=right| 15 || West Afghanistan
|-
| [[Jowzjan Province|Jowzjan]] || style="text-align:center;"| 8 || style="text-align:center;"| AF-JOW || [[Sheberghan]] || align=right| 540,255 || align=right| 11,798 || align=right| 9 || North West Afghanistan
|-
| [[Kabul Province|Kabul]] || style="text-align:center;"| 22 || style="text-align:center;"| AF-KAB || [[Kabul]] || align=right| 4,372,977 || align=right| 4,462 || align=right| 18 || Central Afghanistan
|-
| [[Kandahar Province|Kandahar]] || style="text-align:center;"| 12 || style="text-align:center;"| AF-KAN || [[Kandahar]] || align=right| 1,226,593 || align=right| 54,022 || align=right| 16 || South East Afghanistan
|-
| [[Kapisa Province|Kapisa]] || style="text-align:center;"| 29 || style="text-align:center;"| AF-KAP || [[Mahmud-i-Raqi]] || align=right| 441,010 || align=right| 1,842 || align=right| 7 || Central Afghanistan
|-
| [[Khost Province|Khost]] || style="text-align:center;"| 26 || style="text-align:center;"| AF-KHO || [[Khost]] || align=right| 574,582 || align=right| 4,152 || align=right| 13 || South East Afghanistan
|-
| [[Kunar Province|Kunar]] || style="text-align:center;"| 34 || style="text-align:center;"| AF-KNR || [[Asadabad, Afghanistan|Asadabad]] || align=right| 450,652 || align=right| 4,942 || align=right| 15 || North East Afghanistan
|-
| [[Kunduz Province|Kunduz]] || style="text-align:center;"| 18 || style="text-align:center;"| AF-KDZ || [[Kunduz]] || align=right| 1,010,037 || align=right| 8,040 || align=right| 7 || North East Afghanistan
|-
| [[Laghman Province|Laghman]] || style="text-align:center;"| 32 || style="text-align:center;"| AF-LAG || [[Mihtarlam]] || align=right| 445,588 || align=right| 3,843 || align=right| 5 || East Afghanistan
|-
| [[Logar Province|Logar]] || style="text-align:center;"| 23 || style="text-align:center;"| AF-LOG || [[Pul-i-Alam]] || align=right| 392,045 || align=right| 3,880  || align=right| 7 || Central Afghanistan
|-
| [[Maidan Wardak Province|Maidan Wardak]] || style="text-align:center;"| 21 || style="text-align:center;"| AF-WAR || [[Maidan Shar]] || align=right| 596,287 || align=right| 9,934 || align=right| 9 || Central Afghanistan
|-
| [[Nangarhar Province|Nangarhar]] || style="text-align:center;"| 33 || style="text-align:center;"| AF-NAN || [[Jalalabad, Afghanistan|Jalalabad]] || align=right| 1,517,388 || align=right| 7,727 || align=right| 23 || East Afghanistan
|-
| [[Nimroz Province|Nimruz]] || style="text-align:center;"| 3 || style="text-align:center;"| AF-NIM || [[Zaranj]] || align=right| 164,978 || align=right| 41,005 || align=right| 5 || South West Afghanistan
|-
| [[Nuristan Province|Nuristan]] || style="text-align:center;"| 31 || style="text-align:center;"| AF-NUR || [[Parun]] || align=right| 147,967 || align=right| 9,225 || align=right| 7 || North East Afghanistan
|-
| [[Paktia Province|Paktia]] || style="text-align:center;"| 24 || style="text-align:center;"| AF-PIA || [[Gardez]] || align=right| 551,987 || align=right| 6,432 || align=right| 11 || South East Afghanistan
|-
| [[Paktika Province|Paktika]] || style="text-align:center;"| 25 || style="text-align:center;"| AF-PKA || [[Sharana, Afghanistan|Sharana]] || align=right| 434,742 || align=right| 19,482 || align=right| 15 || South East Afghanistan
|-
| [[Panjshir Province|Panjshir]] || style="text-align:center;"| 28 || style="text-align:center;"| AF-PAN || [[Bazarak, Panjshir|Bazarak]] || align=right| 153,487 || align=right| 3,610 || align=right| 5 || North East Afghanistan
|-
| [[Parwan Province|Parwan]] || style="text-align:center;"| 20 || style="text-align:center;"| AF-PAR || [[Charikar]] || align=right| 664,502 || align=right| 5,974 || align=right| 9 || Central Afghanistan
|-
| [[Samangan Province|Samangan]] || style="text-align:center;"| 14 || style="text-align:center;"| AF-SAM || [[Samangan]] || align=right| 387,928 || align=right| 11,262 || align=right| 5 || North West Afghanistan
|-
| [[Sar-e Pul Province|Sar-e Pol]] || style="text-align:center;"| 9 || style="text-align:center;"| AF-SAR || [[Sar-e Pol city|Sar-e Pol]] || align=right| 559,577 || align=right| 16,360 || align=right| 7 || North West Afghanistan
|-
| [[Takhar Province|Takhar]] || style="text-align:center;"| 27 || style="text-align:center;"| AF-TAK || [[Taloqan]] || align=right| 983,336 || align=right| 12,333 || align=right| 16 || North East Afghanistan
|-
| [[Urozgan Province|Urozgan]] || style="text-align:center;"| 11 || style="text-align:center;"| AF-URU || [[Tarinkot]] || align=right| 386,818 || align=right| 12,696 || align=right| 6 || Central Afghanistan
|-
| [[Zabul Province|Zabul]] || style="text-align:center;"| 17 || style="text-align:center;"| AF-ZAB || [[Qalat, Zabul Province|Qalat]] || align=right| 304,126 || align=right| 17,343 || align=right| 9 || South East Afghanistan
|}

== See also ==
*[[List of current governors of Afghanistan]]
*[[Districts of Afghanistan]]

== References ==
{{Reflist}}

== External links ==
{{commons category|Provinces of Afghanistan}}
*[http://www.aims.org.af/index.aspx Afghanistan Information Management Services (AIMS)]
*[http://www.afghan-web.com/politics/governors.html Provincial Governors]

{{Provinces of Afghanistan}}
{{Articles on first-level administrative divisions of Asian countries}}

{{use dmy dates|date=March 2012}}

[[Category:Provinces of Afghanistan| ]]
[[Category:Subdivisions of Afghanistan]]
[[Category:Lists of country subdivisions|Afghanistan, Provinces]]
[[Category:First-level administrative country subdivisions|Provinces, Afghanistan]]
[[Category:Afghanistan geography-related lists]]