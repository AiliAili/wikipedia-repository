{{Infobox journal
| title = Neuropsychology
| abbreviation = 
| cover = [[File:Neuropsychology_journal_cover.gif]]
| editor = Gregory G. Brown
| discipline = [[Neuropsychology]]
| frequency = Bimonthly
| history = 1987-present
| publisher = [[American Psychological Association]]
| country = United States
| impact = 3.269
| impact-year = 2014
| license = 
| website = http://www.apa.org/pubs/journals/neu/
| link1 = http://psycnet.apa.org/journals/neu/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| LCCN = 
| CODEN = 
| ISSN = 0894-4105
| eISSN = 1931-1559
| OCLC = 48793510
}}
'''''Neuropsychology''''' '''(journal)''' is a [[Peer review|peer-reviewed]] [[academic journal]] that was established in 1987 and covers [[neuropsychology]]. It is published by the [[American Psychological Association]].

The current [[editor-in-chief]] is Gregory G. Brown ([[UC San Diego School of Medicine]]). The journal publishes original, empirical research on the relation between brain and human cognitive, emotional, and behavioral function. <ref>{{cite web |date=18 October 2012 | url=http://www.apa.org/pubs/journals/neu/index.aspx |title=Neuropsychology|publisher=[[American Psychological Association]] |accessdate=2012-10-18}}</ref>

According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 3.269, ranking it 16th among 119 journals in the category "Psychology, Clinical".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychology, Clinical |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2015-08-25|work=Web of Science |postscript=.}}</ref>

==References==
{{reflist}}

== External links ==
* {{Official|http://www.apa.org/pubs/journals/neu/index.aspx}}

[[Category:English-language journals]]
[[Category:American Psychological Association academic journals]]


{{psych-journal-stub}}