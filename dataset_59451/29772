{{Infobox song <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Angel of Death
| Artist         = [[Slayer]]
| Album          = [[Reign in Blood]]
| Released       = October 7, 1986
| track_no       = 1
| Recorded       = 1986, [[Los Angeles]], [[California]]
| Genre          = [[Thrash metal]]
| Length         = 4:51
| Label          = [[Def Jam Records|Def Jam]]
| Writer         = [[Jeff Hanneman]]
| Producer       = [[Rick Rubin]]
| Misc           =
{{Extra music sample|filename=Angel of Death clip.ogg|type=song|format=[[Ogg]]|title=Angel of Death}}
}}

"'''Angel of Death'''" is the opening track on the American [[thrash metal]] band [[Slayer]]'s 1986 album ''[[Reign in Blood]]''. The lyrics and music were written by guitarist [[Jeff Hanneman]]. They detail the [[Nazi Germany|Nazi]] physician [[Josef Mengele]]'s  [[Nazi human experimentation|human experiments]] at the [[Auschwitz concentration camp]] during [[World War II]]. 

Although the lyrics describe Mengele's abuses rather than endorsing them, "Angel of Death" led to accusations of Nazi sympathizing and [[racism]] against the band, which they vigorously denied but which followed them throughout their early career. Despite the controversy and the resulting delay in the release of ''Reign in Blood'', the song remains a live favorite, and has appeared on all of Slayer's [[live album]]s.

The song has been described as highly influential in the development of thrash or [[speed metal]], and is highly regarded by some critics; [[AllMusic]]'s Steve Huey called it a classic and the album "the pinnacle of speed metal".<ref name="Reign in Blood - Review ">{{cite web
|title=Reign in Blood - Review
|author=
|publisher=[[AllMusic]]
|date=
|url={{Allmusic|class=album|id=r18220/review|pure_url=yes}}
|accessdate=2007-07-03}}</ref> The [[Half-time_(music)#Half-time|half-time]] riff was sampled by [[Public Enemy (band)|Public Enemy]] in their 1988 song "[[It Takes a Nation of Millions to Hold Us Back|She Watch Channel Zero?!]]"<ref name="An exclusive  oral history of Slayer"/>

==Composition and origins==
Slayer guitarist [[Jeff Hanneman]] wrote "Angel of Death" after reading books about [[Nazi Germany|Nazi]] physician [[Josef Mengele]] while on tour with the band.<ref name="An exclusive oral history of Slayer"/> He said that he remembered "stopping someplace where I bought two books on Mengele. I thought, 'This has gotta be some sick shit.' So when it came time to do the record, that stuff was still in my head&mdash;that's where the lyrics to 'Angel of Death' came from."<ref name="An exclusive oral history of Slayer"/> 

The lyrics are written both from Mengele's point of view and from that of a detached observer condemning his actions.<ref>Ferris, D.X. "Slayer's Reign in Blood". p 114</ref> They detail Mengele's surgical experiments on patients at the Auschwitz concentration camp during World War II.<ref name="SLAYER's KING Says RICK RUBIN's Collaboration With METALLICA Was 'Slap In The Face'">{{cite web
|title=Slayer's King Says Rick Rubin's Collaboration With Metallica Was 'Slap In The Face'
|publisher=[[Blabbermouth.net]]
|date=2006-06-01
|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=54475
|accessdate=2007-03-21| archiveurl= https://web.archive.org/web/20070326144751/http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=54475| archivedate= 26 March 2007 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite book
|title=Sells Like Teen Spirit: Music, Youth Culture, and Social Crisis
|author=Moore, Ryan
|publisher=New York University Press
|year=2009
|isbn=0-8147-5747-2
|page=101}}</ref> Mengele's explorations were conducted on such groups as [[Dwarfism|dwarfs]] and [[twins]], and included both physical and psychological examinations.<ref name="Josef Mengele">{{cite web
  |title = Josef Mengele
  |publisher = auschwitz.dk
  |url =http://www.auschwitz.dk/Mengele/id17.htm
  |accessdate=2007-02-21| archiveurl= https://web.archive.org/web/20070223062155/http://auschwitz.dk/Mengele/id17.htm| archivedate= 23 February 2007 <!--DASHBot-->| deadurl= no}}</ref><ref name="moreorless : heroes & killers of the 20th century - Josef Mengele">{{cite web
 |title=moreorless : heroes & killers of the 20th century - Josef Mengele 
 |publisher=Moreorless.com 
 |date=2001-04-30 
 |url=http://www.moreorless.au.com/killers/mengele.html 
 |accessdate=2006-12-01 
 |archiveurl=https://web.archive.org/web/20061205212657/http://www.moreorless.au.com/killers/mengele.html 
 |archivedate=5 December 2006 
 |deadurl=yes 
 |df= 
}}</ref> Among the tests he performed that are mentioned in "Angel of Death" are experimental surgeries performed without [[anesthesia]], [[transfusions|transfusion]] of blood between twins, [[Solitary confinement|isolation]] endurance, [[Chemical warfare|gassing]], injections with lethal [[microorganism|germs]], [[sex change operation]]s, the removal of [[organ (anatomy)|organ]]s and [[Limb (anatomy)|limbs]], and [[abacination]].<ref name="Westworld Online interview with Kerry King ">{{cite web
|title=Westworld Online interview with Kerry King
|author=Roberts, Michael
|publisher=Slayersaves.com
|date=
|url=http://www.slayersaves.com/interviews_kerry3.htm
|archiveurl=https://web.archive.org/web/20061018174731/http://www.slayersaves.com/interviews_kerry3.htm
|archivedate=2006-10-18
|accessdate=2007-03-21}}</ref>

==Controversy==
[[File:Slayer eagle.png|thumb|180px|Graphic used by the band in the 1990s<ref name="Slayer Eagle (logo)">{{cite web |title=Slayer Eagle (logo) |publisher= TheGiant.Org | url= http://www.thegiant.org/wiki/index.php/Slayer_Eagle_%28logo%29 |accessdate= 2016-10-08| archiveurl= https://web.archive.org/web/20160329153738/http://thegiant.org/wiki/index.php/Slayer_Eagle_(logo)| archivedate= 29 March 2016 <!--DASHBot-->| deadurl= no}}</ref>]]

The lyrics of "Angel of Death" delayed the release of ''[[Reign in Blood]]''. The band were signed to [[Def Jam Records]], whose distributor, [[Columbia Records]], refused to release the album due to its subject matter and artwork, which they believed were "too graphic".<ref name="An exclusive oral history of Slayer"/> ''Reign in Blood'' was eventually distributed by [[Geffen Records]] on October 7, 1986. However, due to the controversy, ''Reign in Blood'' did not appear on Geffen Records' official release schedule.<ref name="An exclusive oral history of Slayer"/>

"Angel of Death" caused outrage among [[The Holocaust|Holocaust]] survivors, as well as their families and the general public. The controversy led to accusations of Nazi sympathizing which have followed Slayer throughout their career.<ref name="An exclusive oral history of Slayer">{{cite web
  | title = An exclusive oral history of Slayer
  | publisher = Decibel Magazine
  | url = http://www.decibelmagazine.com/features_detail.aspx?id=4566
  | archiveurl = https://web.archive.org/web/20060813155123/http://www.decibelmagazine.com/features_detail.aspx?id=4566
  | archivedate = 2006-08-13
  | accessdate = 2006-12-03 }}</ref> People took Hanneman's interest in Nazi history and his collection of Nazi medals, his most prized item being a German [[Knight's Cross of the Iron Cross|Knight's Cross]],<ref name="SLAYER - Jeff Hanneman">{{cite web
  | title =  Slayer - Jeff Hanneman
  | author = Lahtinen, Lexi
  | publisher = Metal-rules.com
  | date = 2006-12-18
  | url = http://www.metal-rules.com/zine/index.php?option=content&task=view&id=737&Itemid=60
  | accessdate = 2006-12-27 | archiveurl= https://web.archive.org/web/20070129051102/http://www.metal-rules.com/zine/index.php?option=content&task=view&id=737&Itemid=60| archivedate= 29 January 2007 <!--DASHBot-->| deadurl= no}}</ref> as evidence of sympathizing.<ref name="An exclusive oral history of Slayer"/> Hanneman objected, stating: {{quote|I know why people misinterpret it &ndash; it's because they get this knee&ndash;jerk reaction to it. When they read the lyrics, there's nothing I put in the lyrics that says necessarily he was a bad man, because to me &ndash; well, isn't that obvious? I shouldn't have to tell you that.<ref name="Knac.com interview with Jeff Hanneman">{{cite web
 |title=Knac.com interview with Jeff Hanneman 
 |author=Davis, Brian 
 |publisher=Knac.com 
 |url=http://www.knac.com/article.asp?ArticleID=3153 
 |accessdate=2006-12-13 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20070928010337/http://www.knac.com/article.asp?ArticleID=3153 
 |archivedate=2007-09-28 
 |df= 
}}</ref>}} According to guitarist [[Kerry King]]: "Yeah, 'Slayer are Nazis, fascists, communists'—all that fun shit. And of course we got the most flak for it in Germany. I was always like, 'Read the lyrics and tell me what's offensive about it. Can you see it as a documentary, or do you think Slayer's preaching fucking World War II?' People get this thought in their heads—especially in Europe—and you'll never talk them out of it."<ref name="An exclusive oral history of Slayer"/>
<!-- NO FAIR USE RATIONALE EXISTS FOR SOUND SAMPLE IN THIS ARTICLE
{{Sound sample box align left|Music sample:}}
{{Listen
|filename= Angel of Death clip.ogg
|title="Angel of Death" (1986)
|description=A 28 second sample from "Angel of Death", illustrating Araya's scream at the beginning of the song coupled with quick beats.
|format=[[Ogg]]}}
{{sample box end}}
-->

The song drew accusations of racism, which the band has denied.<ref name="An exclusive oral history of Slayer"/> The band members are often asked about the accusations in interviews, and have stated numerous times that they do not condone racism and are merely interested in the subject.<ref name="Slayer's Tom Araya on Satanism, serial killers and his lovable kids">{{cite web
  | title = Slayers Tom Araya on Satanism, serial killers and his lovable kids
  | author = Cummins, Johnson
  | publisher =MontrealMirror.com
  | url = http://www.montrealmirror.com/ARCHIVES/2002/012402/music1.html
  | accessdate = 2006-12-02}}</ref>

Hanneman also wrote "SS-3", a song about senior [[Schutzstaffel|SS]] commander [[Reinhard Heydrich]], which appeared on the band's 1994 album ''[[Divine Intervention (album)|Divine Intervention]]''. The song "[[Jihad (song)|Jihad]]" from their 2006 album ''[[Christ Illusion]]'' has drawn comparison to "Angel of Death".<ref name="Slayer expects backlash with 'Jihad'">{{cite web
  | title = Slayer expects blacklash with 'Jihad'
  | author = Kornelis, Chris
  | publisher = spokane7.com
  | date = 2006-06-14
  | url = http://www.spokane7.com/music/stories/?ID=3613
  | accessdate = 2007-02-21 |archiveurl = https://web.archive.org/web/20070927182238/http://www.spokane7.com/music/stories/?ID=3613 |archivedate = September 27, 2007}}</ref> "Jihad" deals with the [[September 11, 2001, attacks]], and is told from a [[terrorist]]'s perspective. Vocalist Araya expected the subject matter to create a similar backlash to that of "Angel of Death", although it did not materialise,<ref name="LiveDaily Interview: Tom Araya of Slayer">{{cite web
  | title = LiveDaily Interview: Tom Araya of Slayer
  | author = Gargano, Paul
  | publisher = LiveDaily.com
  | date = 2007-01-25
  | url = http://www.livedaily.com/interviews/LiveDaily_Interview_Tom_Araya_of_Slayer-11402.html?t=1
  | accessdate = 2007-02-21 |archiveurl = https://web.archive.org/web/20070205223956/http://www.livedaily.com/interviews/LiveDaily_Interview_Tom_Araya_of_Slayer-11402.html?t=1 |archivedate = February 5, 2007}}</ref> in part, he believes, due to people's view that the song is "just Slayer being Slayer".<ref name="Luxi">{{cite web
  | title =  Slayer - Jeff Hanneman
  | author = Lahtinen, Luxi
  | publisher = Metal-rules.com
  | date = 2006-12-18
  | url = http://www.metal-rules.com/zine/index.php?option=content&task=view&id=737&Itemid=60
  | accessdate = 2006-12-27 | archiveurl= https://web.archive.org/web/20070129051102/http://www.metal-rules.com/zine/index.php?option=content&task=view&id=737&Itemid=60| archivedate= 29 January 2007 <!--DASHBot-->| deadurl= no}}</ref>

==Music and structure==
At 4 minutes and 51 seconds, "Angel of Death" is the longest track on the album, which is 29 minutes in total.<ref name="An exclusive oral history of Slayer"/> It is one of the most [[song structure|structurally]] conventional songs on the album, featuring prominent [[Verse–chorus form|verses and choruses]], which most of the songs eschew. Araya's vocal performance begins with a piercing, wordless scream. "[G]uitarists Kerry King and Jeff Hanneman deliver their intricate riffs [and] drummer Dave Lombardo performs some of the most powerful drumming ever recorded" at 210 [[beats per minute]].<ref name="The Devil in Music">{{cite web
  | title =The Devil in Music
  | author = Begrand, Adrien
  | publisher = Popmatters.com
  | url = http://www.popmatters.com/music/reviews/s/slayer-soundtrack.shtml
  | accessdate = 2007-02-22 | archiveurl= https://web.archive.org/web/20070315203558/http://www.popmatters.com/music/reviews/s/slayer-soundtrack.shtml| archivedate= 15 March 2007 <!--DASHBot-->| deadurl= no}}</ref><ref name="Andrew Haug speaks with Dave Lombardo from Slayer">{{cite web
  |title=Andrew Haug speaks with Dave Lombardo from Slayer
  |author=Haug, Andrew
  |publisher=Abc.net.au
  |date=2006-10-13
  |url=http://www.abc.net.au/triplej/racket/listen/audio_alpha_S.htm
  |accessdate=2007-02-09}}</ref>

When drummer Lombardo left Slayer in 1992, they recruited a full-time replacement in [[Forbidden (band)|Forbidden]] drummer [[Paul Bostaph]].<ref name=" PAUL BOSTAPH of Exodus, ex-Slayer"/> Bostaph made one mistake out of the nine songs the band trialled him with, on "Angel of Death".<ref name=" PAUL BOSTAPH of Exodus, ex-Slayer">{{cite web |title= Paul Bostaph of Exodus, ex-Slayer |author=Syrjälä, Marko |publisher=Metal-Rules.com |date=2007-02-05 |url=http://www.metal-rules.com/zine/index.php?option=content&task=view&id=797&Itemid=60
|accessdate=2007-03-07}}</ref> Before the "big double bass part" there is a lead section, which Bostaph could not understand, as he had to learn from live records recorded with Lombardo.<ref name=" PAUL BOSTAPH of Exodus, ex-Slayer"/> Bostaph could not tell how many revolutions the guitar riff goes before the bass sequence. The band members told him there were eight, "perfecting" the song afterwards.<ref name=" PAUL BOSTAPH of Exodus, ex-Slayer"/>

==Reception==
Although "Angel of Death" did not chart, it was highly praised by critics reviewing ''Reign in Blood''. Clay Jarvas of ''[[Stylus Magazine]]'' observed how the song "smokes the asses of any band playing fast and/or heavy today. Lyrically outlining the horrors to come, while musically laying the groundwork for the rest of the record: fast, lean and filthy."<ref name="Slayer Reign in Blood">{{cite web
 |title=Slayer Reign in Blood 
 |author=Jarvis, Clay 
 |publisher=[[Stylus Magazine]] 
 |date=2003-09-01 
 |url=http://www.stylusmagazine.com/reviews/slayer/reign-in-blood.htm 
 |accessdate=2007-02-18 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20060511163047/http://www.stylusmagazine.com:80/reviews/slayer/reign-in-blood.htm 
 |archivedate=2006-05-11 
 |df= 
}}</ref> Adrien Begrand of [[PopMatters]] remarked that "There's no better song to kick things off than the masterful 'Angel of Death', one of the most monumental songs in metal history."<ref name="The Devil in Music"/>

==References==
{{Reflist|colwidth=30em}}

==External links==
*{{MetroLyrics song|slayer|angel-of-death}}<!-- Licensed lyrics provider -->

{{Slayer}}
{{featured article}}

{{DEFAULTSORT:Angel Of Death (Song)}}
[[Category:1986 songs]]
[[Category:Obscenity controversies in music]]
[[Category:Slayer songs]]
[[Category:Songs about the Holocaust]]
[[Category:Song recordings produced by Rick Rubin]]