{{Good article}}
{{DISPLAYTITLE:Afterbirth (''American Horror Story'')}}
{{Infobox television episode
| title = Afterbirth
| series = [[American Horror Story]]
| image = 
| caption = 
| season = 1
| episode = 12
| airdate = {{Start date|2011|12|21}}
| production = 1ATS11
| music = 
{{Unbulleted list|"El Infiél" by Marvelous Toy|"[[The Little Drummer Boy]]" by The [[Harry Simeone]] Chorale|"Tonight You Belong to Me" by [[Patience and Prudence]]|"[[Twisted Nerve#Soundtrack|Twisted Nerve]]" by [[Bernard Herrmann]]}}
| guests = 
* [[Kate Mara]] as Hayden McClaine
* [[Charles S. Dutton]] as Detective Granger
* [[Frances Conroy]] as Moira O'Hara
* [[Alexandra Breckenridge]] as Young Moira
* [[Lily Rabe]] as Nora Montgomery
* [[Christine Estabrook]] as Marcy
* [[Michael Graziadei]] as Travis Wanderley
* [[Anthony Ruivivar]] as Miguel Ramos
* [[Lisa Vidal]] as Stacy Ramos
* [[Malaya Drew|Malaya Rivera Drew]] as Detective Barrios
* [[David Anthony Higgins]] as Stan
* [[Brennan Mejia]] as Gabriel Ramos
* [[Rebecca Wisocky]] as Lorraine Harvey
* [[Kyle Davis (actor)|Kyle Davis]] as Dallas
* [[Azura Skye]] as Fiona
* [[Jamie Brewer]] as Adelaide Langdon
* [[Meredith Scott Lynn]] as Helen
* [[Mena Suvari]] as [[Elizabeth Short]]
| writer = [[Jessica Sharzer]]
| director = Bradley Buecker
| length = 52 minutes
| episode_list = [[American Horror Story: Murder House|''American Horror Story'' (season 1)]]<br>[[List of American Horror Story episodes|List of ''American Horror Story'' episodes]]
| prev = [[Birth (American Horror Story)|Birth]]
| next = [[Welcome to Briarcliff]]
}}

"'''Afterbirth'''" is the [[List of American Horror Story episodes|twelfth episode]] of the [[American Horror Story: Murder House|first season]] of the [[television series]] ''[[American Horror Story]]'' and the [[season finale]], which premiered on [[FX (TV channel)|FX]] on December 21, 2011. The episode was written by [[Jessica Sharzer]] and directed by Bradley Buecker. Due to a very aggressive production schedule it was previously announced that the show's first season would be cut short. This was the last episode to feature the Harmons. Murphy announced that a new cast will return for the second season.

In the aftermath of family tragedy, Ben ([[Dylan McDermott]]) tries to take his child out of the Murder House. Meanwhile, Violet ([[Taissa Farmiga]]) and Vivien ([[Connie Britton]]) accustom themselves to their new living arrangements, Constance ([[Jessica Lange]]) raises Tate's child as her own and a new family buys the house. [[Kate Mara]] and [[Charles S. Dutton]] guest star as Hayden McClaine and Detective Granger, respectively.

The episode was well received by the majority of television critics and viewership and ratings for the episode reached a season high with 3.22 million. It garnered a 1.7 rating in the 18–49 demographic, according to [[Nielsen Media Research]]. This episode is rated [[TV-MA|TV-MA (LSV)]].

==Plot==

===Nine months ago===
In Boston, after Vivien ([[Connie Britton]]) catches Ben ([[Dylan McDermott]]) cheating on her, he begs for her forgiveness, and says he would do anything to save their marriage. This includes getting out of [[Boston]] and relocating to a different city. He shows her pictures of a great house in an older district of [[Los Angeles]], claiming Vivien loves old houses. He tells her the price of the house is strangely lower than those around it, and that it would be a great place for the family to start over.

===Present===
Days after Vivien's death, Ben attempts to find Violet ([[Taissa Farmiga]]) in the house. He goes to see Constance ([[Jessica Lange]]), who has been taking care of the baby since Vivien's death. He discovers that she is Tate's mother, argues with her, and threatens her before leaving with the child. Back at the house, Moira ([[Frances Conroy]])  reminds Vivien that she can show herself to Ben at will, allowing Vivien to ease his sadness, but Vivien notes she'd rather not give Ben reason to stay.

Ben starts drinking and plans to commit suicide, but Vivien appears to stop him. He then reconciles with Vivien and Violet's ghosts, who encourage him to take the child and start a new life away from the house. Before he can leave, Hayden ([[Kate Mara]]), with the help of Dallas and Fiona, hangs him from a chandelier, killing him and making it appear like a suicide. Hayden takes the baby, but Constance reclaims the baby with Travis' ([[Michael Graziadei]]) assistance. Later, she tells the police that she found Ben after he hanged himself and that Violet must have run off with the baby.

Moira explains that while some ghosts in the house only wish for the living to suffer the same fates they did, other ghosts are innocent and do not wish to see any more bloodshed. The Harmons realize the house is a danger to those living in it and resolve to protect future residents by scaring them away. Both of Moira's ghostly forms (Conroy and [[Alexandra Breckenridge]]) and other ghosts, including nurse Gladys (Celia Finkelstein), Beau (Sam Kinsey), the exterminator ([[W. Earl Brown]]), and the Black Dahlia ([[Mena Suvari]]), join the Harmons to protect the Ramoses, a new family that moved in, against malicious ghosts.

Meanwhile, Tate, still in love with Violet, is concerned that she is lonely. He attempts to kill Gabriel Ramos ([[Brennan Mejia]]), thinking he might be a better boyfriend for Violet, but she stops Tate, and gives him a goodbye kiss, allowing the boy to escape. Gabriel and his family flee the home, while the Harmons happily watch them drive off.

Tate seeks counsel from Ben, who wants nothing to do with him. Tate finally takes responsibility for his actions and apologizes, before asking for Ben's friendship.

In the basement, Vivien discovers that the other twin had actually lived for a brief moment before dying, and its ghost is being taken care of by Nora ([[Lily Rabe]]), who finds that she does not have the constitution to be a caring mother and returns the baby to Vivien. Vivien asks Moira to join their family as the baby's godmother, which she happily accepts. Together, as one happy family, they all celebrate Christmas by decorating a tree in the abandoned house with various ornaments found in the attic. Watching from outside, Hayden tells Tate that Violet will never forgive him for what he did and that he should get over her, but he resolves to wait for her "forever".

===Three years later===
Constance returns home from the hairdresser to find that her grandson Michael, the [[Antichrist]], has killed his nanny. She smiles and remarks, "Now what am I going to do with you?" as Michael gleefully smiles back, while swinging back and forth in his rocking chair.

==Production==
{{Quote box|quoted=true|salign=right|width=240px|quote=What you saw in the finale was the end of the Harmon house. The second season of the show will be a brand-new home or building to haunt. Just like this year, every season of this show will have a beginning, middle and end. [The second season] won't be in L.A. It will obviously be in America, but in a completely different locale."|source=– Murphy on ''American Horror Story''{{'s}} second season.<ref name="Mullins">{{cite web|url=http://uk.eonline.com/news/watch_with_kristin/american_horror_story__season_two_scoop/282480?cmpid=rss-000000-rssfeed-365-topstories|title=American Horror Story Season Two Scoop: New House and (Mostly) New Faces|last=Mullins|first=Jenna|website=E! Online|date=December 22, 2011}}</ref>}}

The episode was written by consulting producer [[Jessica Sharzer]] and directed by Bradley Buecker.<ref name="afterbirth">{{cite episode|title=Afterbirth|series=American Horror Story|serieslink=American Horror Story|credits=[[Jessica Sharzer]] (writer), Bradley Buecker (director)|network=FX|airdate=December 21, 2011|season=1|number=12}}</ref> Due to a "very aggressive" production schedule and the series' pilot shoot having to wait for co-creators Ryan Murphy and Brad Falchuk's other show, ''Glee'', to wrap its second season production, it was announced that the show's first season finale would be thirty minutes shorter than planned. An option was given to Murphy by the network to drop the thirteenth episode altogether and air an hour-long finale, but Murphy came up with a plan for a ninety-minute episode.<ref>{{cite web|last=Andreeva|first=Nellie|date=November 18, 2011|url=http://deadline.com/2011/11/american-horror-story-two-hour-finale-will-be-trimmed-to-90-minutes/|title='American Horror Story' Two-Hour Finale Will Be Trimmed To 90 Minutes|website=Deadline.com|accessdate=November 24, 2011}}</ref>

After the episode aired, Murphy spoke of his plans to change the cast and location for the second season.<ref>{{cite web|url=http://artsbeat.blogs.nytimes.com/2011/12/22/american-horror-story-will-scare-up-a-new-cast-and-new-haunted-home-for-season-2/|title='American Horror Story' Will Scare Up a New Cast and New Haunted Home for Season 2|last=Itzkoff|first=Dave|website=[[The New York Times]]|accessdate=December 22, 2011|date=December 22, 2011}}</ref> He did say, however, that some actors who starred in the first season would be returning. He announced, "The people that are coming back will be playing completely different characters, creatures, monsters, etc. [The Harmons] stories are done. People who are coming back will be playing entirely new characters."<ref name="Mullins"/>

In an interview with ''[[Entertainment Weekly]]'', Murphy commented on the structure of the series' first season. "I loved how it began. I loved it in the middle and I loved the end. The only thing really frustrating for me, to be honest with you, is that sometimes people would write this idea that we were making it up as we went along and I wanted to say, 'Really?' But I think now people are writing and saying, 'Oh yeah!' I'm excited for people to see it on DVD, because now that they know how it ended, [they can] go back and see all of the little things, like people who have no reflections in mirrors. When you go back, you will see everything was set up."<ref name="Murphy">{{cite web|url=http://insidetv.ew.com/2011/12/22/american-horror-story-ryan-murphy-season-finale/|title='American Horror Story': Ryan Murphy talks the game-changing season finale -- EXCLUSIVE|website=Entertainment Weekly|last=Stack|first=Tim|accessdate=December 22, 2011|date=December 22, 2011}}</ref> When asked if the House "pulled" the Harmons to it, Murphy responded, "I don't know if they were targeted. I don't think the Internet site had any supernatural pull to it...although that would have been hilarious. I thought it was two things: it was house porn because I think that house really is extraordinary. And also I think there's always that allure in American lives of the fresh start, moving West, starting fresh."<ref name="Murphy"/>

==Reception==
In its original American broadcast, "Afterbirth" was seen by an estimated 3.22 million household viewers and earned a 1.7 rating share among adults aged 18–49, according to [[Nielsen ratings|Nielsen Media Research]], its highest numbers of the season.<ref name="Gorman">{{cite web|url=http://tvbythenumbers.zap2it.com/2011/12/22/wednesday-cable-ratings-american-horror-story-rises-leads-night-sons-of-guns-moonshiners-top-chef-hot-in-cleveland-more/114490/|title=Wednesday Cable Ratings: 'American Horror Story' Finale Rises, Leads Night + 'Sons Of Guns,' 'Moonshiners,' 'Top Chef', 'Hot In Cleveland' & More|website=TV by the Numbers|last=Gorman|first=Bill|date=December 22, 2011|accessdate=December 22, 2011}}</ref> The first season tied with the [[TNT (TV channel)|TNT]] series ''[[Falling Skies]]'' as the biggest new cable series of the year among adults 18-49.<ref name="VanDerWerff">{{cite web|url=http://www.avclub.com/articles/american-horror-story-to-completely-ditch-season-o,67002/|title=American Horror Story to completely ditch season one characters, story, do something new in season two|last=VanDerWerff|first=Todd|website=The A.V. Club|accessdate=December 22, 2011}}</ref>

[[Rotten Tomatoes]] reports an 82% approval rating, based on 11 reviews. The critical consensus reads, ""Afterbirth" is an enjoyable season finale that manages to deliver a few last minute scares as it ties up loose ends."<ref>{{cite web|url=https://www.rottentomatoes.com/tv/american_horror_story/s01/e12/|title=Afterbirth – American Horror Story: Murder House, Episode 12|website=Rotten Tomatoes|accessdate=October 21, 2016}}</ref> James Queally from ''[[The Star-Ledger]]'' commented about the season and its finale, "After a wildly uneven first nine episodes... and two red-hot episodes leading up to the finale... "Afterbirth" starts out flying high at the way too fast pace that has made ''American Horror Story'' an enjoyable ride these past two weeks. But somewhere in the final 30 minutes of this episode, "Afterbirth" turns into a wandering epilogue."<ref>{{cite web|url=http://www.nj.com/entertainment/tv/index.ssf/2011/12/american_horror_story_finale_r.html|title='American Horror Story' finale review: 'Afterbirth' or What to Do in L.A. When You're Dead|website=NJ.com|last=Queally|first=James|date=December 21, 2011|accessdate=December 22, 2011}}</ref> Todd VanDerWerff of ''[[The A.V. Club]]'' stated, "Part of the fun of those early episodes – even when I really didn't like them – was that you never quite had a good sense of what the hell kind of show you were watching. The finale sticks us into what seems to be a pretty basic setup for season two."<ref>{{cite web|url=http://www.avclub.com/articles/afterbirth,66780/|title=Afterbirth|website=[[The A.V. Club]]|last=VanDerWerff|first=Todd|date=December 21, 2011|accessdate=December 22, 2011}}</ref> Tim Stack of ''[[Entertainment Weekly]]'' called "Afterbirth" "an exhilarating shocker."<ref name="Murphy"/> Matt Fowler at [[IGN]] gave the episode a score eight out of ten, signifying a "great" rating. He stated, ""Afterbirth" was a very odd finale and I'm pretty sure that it was nothing like most of us thought it would be."<ref>{{cite web|url=http://tv.ign.com/articles/121/1215108p1.html|title=American Horror Story: "Afterbirth" Review|website=IGN|last=Fowler|first=Matt|date=December 21, 2011|accessdate=March 18, 2012}}</ref>

The [[Horror Writers Association]] gave "Afterbirth" the 2011 [[Bram Stoker Award|Bram Stoker Award for Superior Achievement in a Screenplay]].<ref>{{cite web|url=http://horror.org/2011-bram-stoker-award-winners-and-vampire-novel-of-the-century-award-winner/|title=Bram Stoker Award™ winners and Vampire Novel of the Century Award winner|website=Horror.org|date=April 1, 2012}}</ref>

==References==
{{Reflist|30em}}

==External links==
{{wikiquote|American Horror Story#Afterbirth_.5B1.12.5D|Afterbirth}}
{{Portal|Horror fiction|Television}}
* {{IMDb episode|2074510|Afterbirth}}
* [http://www.tv.com/shows/american-horror-story/afterbirth-1628394/ "Afterbirth"] at [[TV.com]]
* [http://www.tvguide.com/tvshows/american-horror-story-2011/episode-12-season-1/murder-house/319613 "Afterbirth"] at [[TV Guide]].com

{{American Horror Story}}

[[Category:American Horror Story: Murder House episodes]]
[[Category:Christmas television episodes]]