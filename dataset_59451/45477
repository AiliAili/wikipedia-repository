<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
|name=Josef Mai
|image= Josef Mai.jpg
|caption=
|birth_date= <!-- {{Birth date and age|YYYY|MM|DD}} -->3 March 1887
|death_date=  {{Death date and age|df=yes|1982|1|18|1887|3|3}}
|birth_place=Ottorowo, Province of Posen, Kingdom of Prussia, German Empire
|death_place=Germany
|placeofburial=
|placeofburial_label=
|nickname=
|allegiance=German Empire
|branch=Air Service
|serviceyears=1915–1918
|rank=Major
|unit=Kasta 29, Jagdstaffel 5
|commands=
|battles=Verdun, Somme
|awards=[[Iron Cross]] First and Second Class
|relations=
|laterwork=[[World War II]] service
}}
Major '''Josef Mai''' (3 March 1887 – 18 January 1982) [[Iron Cross]] First and Second Class, was a World War I fighter pilot credited with 30 victories.<ref name="theaerodrome.com">{{cite web|url=http://www.theaerodrome.com/aces/germany/mai.php |title=Josef Mai |publisher=Theaerodrome.com |date= |accessdate=2015-07-26}}</ref><ref name="military-art.com">{{cite web|url=http://www.military-art.com/mall/more.php?ProdID=14747 |title=Edition information |publisher=Military-art.com |date= |accessdate=2015-07-26}}</ref>

==Early life==
Josef Mai was born in [[Otorowo, Greater Poland Voivodeship|Ottorowo]], [[Province of Posen]]. His original military service began on 3 October 1907 with the 10th Lancers. When World War I began Mai was part of the offensive aimed at the French capital of [[Paris]]. He later took part in the fighting around [[Warsaw]], [[Poland]]. In 1915 he campaigned along the [[Dniester River]]. He also served at the battles of [[Verdun]] and the [[Battle of the Somme|Somme]].<ref name="flieger-album.de">[https://archive.is/20081004071613/http://www.flieger-album.de/geschichte/portraits/portraitjosefmai.php]  </ref>

==Aerial service==
Mai joined the German air service in 1915; he trained at the Fokker plant at [[Leipzig]].<ref name="flieger-album.de"/> He originally served in Kasta 29, flying reconnaissance aircraft in 1916. He then underwent fighter training and joined [[Jagdstaffel]] 5 in March 1917. As a [[Vizefeldwebel]], he was one of three non-commissioned pilots (along with [[Fritz Rumey]] and [[Otto Koennecke]]) who flew together so successfully they ended up claiming 40% of the Jasta's victories between them, and making Jasta 5 the third highest scoring unit of the war.<ref>{{cite web|url=http://www.theaerodrome.com/services/germany/jasta/jasta5.php |title=Jasta 5 |publisher=Theaerodrome.com |date= |accessdate=2015-07-26}}</ref> The trio was nicknamed "The Golden Triumvirate".<ref>{{cite book |title=Albatros Aces of World War I |page= 39 }}</ref>

Mai scored his first victory on 20 August 1917, flying an Albatros D.V, and downing a Sopwith Camel of No. 70 Squadron. His fifth victory, over a [[RAF SE.5a]], was on 30 November.
Mai did not score again until 13 January 1918. On 25 April 1918 he forced down British 18-kill ace Lt [[Maurice Newnham]] of No. 65 Squadron, for his tenth victory.<ref name="theaerodrome.com"/>

By May 1918 Jasta 5 was sharing an airstrip with [[Jagdgeschwader 1 (World War I)|Jagdgeschwader 1]], and as the "Flying Circus" re-equipped with new [[Fokker D.VII]]s, Mai started flying a cast-off [[Fokker Dr.I]] triplane. He flew this triplane (Serial No. 139/17) for his next victory, over a pair of aces in a No. 11 Squadron [[Bristol F.2B]]. Pilot Lt [[Herbert Sellars]] (8 claims) was killed although Observer Lt. [[Charles Robson (aviator)|Charles Robson]] survived and taken prisoner.<ref name="theaerodrome.com"/> He claimed three victories with the Triplane.

Mai was prone to paint his planes in a "zebra stripe" pattern,with black and white striping on the fuselage angled to the left viewed from the starboard side.<ref>{{cite web|url=http://www.first-world-war.com/se_5.htm |title=S.E.5 Aircraft Art - British WW1 Aviation Prints |publisher=First-world-war.com |date= |accessdate=2015-07-26}}</ref> his theory being the optical illusion would help to throw off an enemy pilot's aim.<ref>{{cite book |title=Fokker D.VII Aces of World War I |page=39  }}</ref> Painted on this background was his insignia of a star and crescent. His Albatros and D.VII were known to bear this paint scheme although his Dr.l paint scheme is uncertain.<ref>{{cite book |title=Albatros Aces of World War I |page= 23 }}</ref>

On 19 August 1918 he had his most successful day. He attacked two [[Bristol F.2B]] fighters from No. 48 Squadron, [[Royal Air Force]]. As he hit one Brisfit with incendiary ammunition, the other swerved away from the incoming fire and collided with his wrecked companion. Mai followed up this double kill by downing a 56 Squadron SE.5a later.<ref>{{cite book |title=Fokker D.VII Aces of World War I |pages=40–41 }}</ref>

On 3 September he was wounded in action in the left thigh. Nevertheless he scored again two days later, and added five more victories during September.<ref name="theaerodrome.com"/>

Mai's 26th victory was a No. 64 Squadron SE5a on 5 September 1918. On 27 September 1918, the day of his 29th success, Mai was promoted to Leutnant.<ref name="flieger-album.de"/> His friend Fritz Rumey was also killed in action on that day.<ref>{{cite web|url=http://www.theaerodrome.com/aces/germany/rumey.php |title=Fritz Rumey |publisher=Theaerodrome.com |date= |accessdate=2015-07-26}}</ref>

Mai claimed his 30th and last score, a [[Bristol F.2B]] of 20 Squadron, killing the 6-kill ace crew of Lt. [[Nicholson Boulton]] and 2/Lt. C.H. Chase on 29 September 1918.<ref name="theaerodrome.com"/> 15 had been claimed with the Fokker D.VII, 12 with the Albatros and 3 with the Fokker Dr.I.

Mai was nominated for Germany's highest honor, the [[Pour le Merite]], or Blue Max. Before it could be approved the war ended with Germany's loss.<ref name="theaerodrome.com"/><ref name="flieger-album.de"/>

==Postwar life==
Mai is believed to have become a flying instructor for the [[Luftwaffe]] during World War II.<ref name="military-art.com"/> He died at the age of 94 in January 1982.

==References==
{{Reflist}}

==Bibliography==
* Norman Franks, et al. ''Fokker D VII Aces of World War I: Part 2''. Osprey Publishing, 2004. ISBN 1-84176-729-8, ISBN 978-1-84176-729-1.
* Norman Franks, et al. ''Fokker Dr I Aces of World War I''. Osprey Publishing, 2001. ISBN 1-84176-223-7, ISBN 978-1-84176-223-4.
* Norman Franks. ''Albatros Aces of World War I''. Osprey Publishing, 2000. ISBN 1-85532-960-3, ISBN 978-1-85532-960-7.

{{wwi-air}}


{{DEFAULTSORT:Mai, Josef}}
[[Category:German World War I flying aces]]
[[Category:Recipients of the Iron Cross (1914), 1st class]]
[[Category:1887 births]]
[[Category:1982 deaths]]