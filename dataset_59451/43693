{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=''SS'' class 
 |image=File:Aviation in Britain Before the First World War RAE-O809.jpg
 |caption=An SS class airship using a [[B.E.2c]] fuselage as a gondola 
}}{{Infobox Aircraft Type
 |type=Patrol airship (submarine scout)
 |national origin=United Kingdom
 |manufacturer=[[Royal Aircraft Establishment|Royal Aircraft Factory]]<br>[[RNAS Kingsnorth]]<br>[[RNAS Capel]]<br>[[Royal Naval Air Service|RNAS]] [[Wormwood Scrubs]]<br>[[Vickers]]  
 |designer=
 |first flight={{avyear|1915}}
 |introduced=18 March 1915 
 |retired=
 |status=
 |primary user=[[Royal Navy]]
 |more users=
 |produced=
 |number built=60 original SS class <ref name=BE2c/><br>(158 all variants) <ref name=AHT/>
 |variants with their own articles=[[SSP class airship|SSP]], [[SSZ class airship|SSZ]] and [[SST class airship|SST class]]
}}
|}

'''SS''' ('''''Submarine Scout''''' or '''''Sea Scout''''') '''class airships''' were simple, cheap and easily assembled small non-rigid airships or "[[blimp]]s" that were developed as a matter of some urgency to counter the German [[U-boat]] threat to British shipping during World War I. The class proved to be versatile and effective, with a total of 158 being built in several versions.<ref name=AHT>[http://aht.ndirect.co.uk/airships/ss/index.html SS class airship.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref>

==Requirement==
Soon after the outbreak of World War I, the threat to British shipping from German submarines became increasingly apparent, with numerous losses occurring during October and November 1914. Then, on 4 February 1915, a communiqué issued by the [[Kaiserliche Marine|Imperial German Admiralty]] declared that: "All the waters surrounding Great Britain and Ireland, are hereby declared to be a war zone. From February 18 onwards every enemy merchant vessel found within this war zone will be destroyed."<ref name=Uft>[http://www.usbornefamilytree.com/neville1883.htm Neville Florian Usborne], Usborne family tree. Retrieved on 19 March 2009.</ref>

The situation had become critical and the [[Admiralty]] recognised that [[airship]]s would be effective at spotting submarines and useful for Fleet observations, but at that time Britain's airship fleet consisted of just seven craft – four [[Royal Naval Air Service|RNAS]] airships (HMA 17,18,19, and 20), two continental ships and a small [[Willows airships|Willows]] training craft – with only four airfields existing that possessed hangars capable of housing them.<ref name=AHT/><ref>The initial four airfields that possessed hangars capable of housing airships were located at RAE [[Farnborough Airfield|Farnborough]], at the Vickers production facility in [[Barrow-in-Furness]], at [[Wormwood Scrubs]] Naval Air Station in London and at RNAS [[Kingsnorth (Medway)|Kingsnorth]] near Hoo on the Medway.</ref> Consequently, on 28 February the [[First Sea Lord]], [[John Fisher, 1st Baron Fisher|Admiral Lord Fisher]] called a meeting with Commander [[Edward Masterman|E A D Masterman]] (Officer Commanding the Naval Airship Section) and representatives from [[Vickers]] and the London-based firm of [[Airco|Airships Limited]] to discuss the possibilities of creating a fleet of suitable patrol airships, sometimes referred to as "scouts".<ref name=AHT/>

==Design and development==
The type was to have a speed of {{convert|40|–|50|mph|km/h|abbr=on}}, carry a crew of two, {{convert|160|lb|kg|abbr=on}} of bombs, [[Radio|wireless]] equipment, fuel for eight hours flying, and capable of reaching an altitude of {{convert|5000|ft|m|abbr=on}}. Most importantly the design had to be simple, in order to ease production and to facilitate training of the crews, since the new airships, designated the "Submarine Scout" or "Sea Scout" (SS) class, needed to be operational within weeks rather than months.<ref name=AHT/>

===Prototype===
The prototype SS craft was created at [[RNAS Kingsnorth]] on the [[Hoo Peninsula]],<ref>Barnes & James (1989), p.13.</ref> and was effectively a [[Royal Aircraft Factory B.E.2|B.E.2c]] aeroplane fuselage and engine minus wings, tailfin and elevators, slung below the disused envelope from airship [[Willows airships|HMA No. 2 (Willows No. 4)]] that had been lying deflated at the [[Royal Aircraft Establishment]] (RAE), [[Farnborough Airfield]].<ref>Whale (2008), p.53.</ref> It was ready for evaluation trials within a fortnight of approval being granted for the scheme,<ref name=W54>Whale (2008), p.54.</ref> and on 18 March 1915 the first SS class airship entered service. The whole process had taken less than three weeks, and voicing his approval, Admiral Fisher made the famous comment: "Now I must have forty!"<ref name=AHT/>

The officer commanding the Kingsnorth facility was Wing-Commander [[Neville Usborne|N. F. Usborne]], who also assisted in the design of the airship. In recognition of his contributions the following comment was made: "Admiral [[Murray Sueter|Sueter]] desires to place on record his high appreciation of the hard work and devotion to the airship cause displayed by Commander Usborne. Far into the night and the early hours of the morning this scientific officer worked to make these airships a success and due to him in large part their wonderful success was due."<ref name=Uft/>

Two private firms, [[Armstrong Whitworth Aircraft|Armstrong Whitworth]] and Airships Ltd., were also invited to submit designs and consequently three versions of the SS class blimp were produced: the SS "B.E.2c", the SS "Armstrong Whitworth" and the SS "Maurice Farman" (thus named because the car designed by Airships Ltd. resembled a [[Farman Aviation Works|Farman]] aeroplane body).<ref name=W54/>

===Envelope===
The envelope of the experimental prototype had a volume of {{convert|20500|ft3|m3|abbr=on}} of hydrogen gas, but production models used a {{convert|60000|ft3|m3|abbr=on}} envelope of similar shape that provided a typical gross lift of {{convert|4180|lb|kg|abbr=on}},<ref name=W54/> a net lift of {{convert|1434|lb|kg|abbr=on}} and a disposable lift of {{convert|659|lb|kg|abbr=on}} with full fuel tanks and a crew of two on board.<ref name=W55>Whale (2008), p.55.</ref> Each of the SS versions used similar envelopes that were composed of four layers: two of rubber-proofed fabric with a layer of rubber between them, and a further rubber layer on the inner, or gas surface. The external surface had five coats of [[Aircraft dope|dope]] applied to it to protect it from the elements and to render the envelope completely gastight. The first two coats were of "Delta dope" (a flexible dope used for the first time in 1913 on the British Army [[semi-rigid airship]] ''Delta''<ref>[http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%200086.html "Delta dope"] flightglobal.com. Retrieved on 27 March 2009.</ref>), followed by two of aluminium dope and finally one of aluminium varnish. To stiffen the nose of the envelope and to prevent it blowing in, 24 canes were arranged radially from its centre and covered with an aluminium cap.<ref name=W55/>

The envelope contained two [[ballonet]]s of {{convert|6375|ft3|m3|abbr=on}} each instead of just one as used on the prototype. These were supplied with air from the propeller draught via a scoop and a slanting aluminium tube to the underside of the envelope, and then via horizontal fabric hoses containing non-return fabric valves known as "crab-pots".<ref>Whale (2008), pp.55–56.</ref>

===Planes===
The original design featured four fins (or planes) and rudders set radially to the envelope: two horizontal fins, and two below the envelope in an inverted [[V-tail]] configuration; however, in some cases the two lower fins were replaced with a single central fin that carried a larger rudder.<ref name=W57>Whale (2008), p.57.</ref><ref>Compare photographs on the [http://aht.ndirect.co.uk/airships/ss/index.html SS class] and [http://aht.ndirect.co.uk/airships/Technical%20Spec/SS%20Airships%20BE2C.htm B.E.2c car] web pages.</ref> The fins were identical in size and shape, and were constructed of [[spruce]], aluminium, and steel tubing, braced with wire and covered with doped fabric.<ref name=W57/>

===Versions===

====SS B.E.2c====
Similar to the prototype, the production car was a wingless B.E.2c fuselage stripped of various fittings, and equipped with two [[Fraxinus|ash]] skids in place of the wheeled undercarriage. Mounted at the front of the car was an air-cooled {{convert|75|hp|kW|abbr=on}} Renault engine driving a {{convert|9|ft|m|abbr=on}} diameter four-bladed propeller.<ref name=AHT/><ref name=W57/>

The pilot was seated behind the observer, who also served as the wireless operator. A camera was fitted,<ref name=Vivian>[http://www.nalanda.nitc.ac.in/resources/english/etext-project/history/aerohist/part3.chapter5.html  British Airship Design.] Charles Vivian, E., ''A History of Aeronautics'' pt.3, ch.V. Retrieved on 28 March 2009.</ref> and the armament consisted of bombs carried in frames suspended about the centre of the undercarriage and a [[Lewis Gun]] mounted on a post adjacent to the pilot's seat.<ref name=LG>The Lewis Gun was not always carried on early models.</ref> The bomb sight and release mechanism were located on the outside of the car on the [[starboard]] side of the pilot's position.<ref name=W58>Whale (2008), p.58.</ref>

====SS Maurice Farman====
The Airships Ltd. design initially used {{convert|60000|ft3|m3|abbr=on}}, and later {{convert|70000|ft3|m3|abbr=on}} envelopes. Dual controls were fitted for the pilot and the observer/wireless operator. Occasionally a third seat was fitted to carry a passenger or an engineer. Renault engines were normally fitted, mounted at the rear of the car in [[pusher configuration]], but a [[Rolls-Royce Hawk]] proved effective in one instance. The type was slightly slower than the SS B.E.2c, but the cars were roomier and more comfortable.<ref>Whale (2008), pp.58–59.</ref>

====SS Armstrong Whitworth====
The version fitted with the Armstrong Whitworth [[Armstrong Whitworth F.K.3|F.K.]] car was similar in many respects to the B.E.2c type, but had a single-skid landing gear with buffers, and required the larger {{nowrap|70,000 cu ft}} envelope to maintain a reasonable margin of lift. A water-cooled {{convert|100|hp|kW|abbr=on}} [[Green Engine Co|Green]] engine was fitted in [[tractor configuration]], and fuel was carried in two aluminium tanks supported in fabric slings suspended from the envelope, saving {{convert|100|lb|kg|abbr=on}} in weight compared to the internal tanks fitted to the B.E.2c.<ref name=W59>Whale (2008), p.59.</ref>

===Airship stations===
[[File:Submarine Scout airship Q27563.jpg|thumb|right|An SS airship lands after a patrol, showing the large crew required for handling on the ground. Another SS airship can be seen in the air.]]
At the same time a number of new air stations were set up as well as a training station at [[Cranwell]].<ref>The first new air stations were sited at [[RNAS Capel|Capel-le-Ferne]] near [[Folkestone]], [[Polegate]] near [[Eastbourne]], [[Marquise, Pas-de-Calais|Marquise]] near [[Boulogne]] on the French coast, [[Luce Bay]] near [[Stranraer]] in Scotland, and in [[Anglesey]].</ref> The [[rigid airship]] programme was also gathering momentum, and these stations were later joined by several more that together formed a chain all around the UK coast.<ref name=AHT/><ref>Further airship stations were established at [[Longside]] near [[Aberdeen]], [[East Fortune]] on the [[Firth of Forth]], [[RNAS Howden|Howden]] on the Humber, [[RNAS Pulham|Pulham]] in [[Norfolk]], [[Mullion, Cornwall|Mullion]] in [[Cornwall]], and [[Pembroke, Pembrokeshire|Pembroke]] in West Wales.</ref>

==Production==
Initially undertaken by the [[Royal Aircraft Establishment|Royal Aircraft Factory]] at Farnborough, production was soon transferred to Kingsnorth, and in addition shortly afterwards to Vickers' works at [[Barrow-in-Furness]] and to the [[Wormwood Scrubs]] Naval Air Station in London. However, construction at each of the facilities was hampered by aeroplane orders affecting the supply of envelopes.<ref name=AHT/>

In total, some 60 examples of the three versions of SS class blimp were assembled,<ref name=BE2c/> costing around £2,500 each<ref name=AHT/> (equivalent to £{{formatnum:{{inflation|UK|2500|1918|{{Inflation-year|UK}}|r=-4}}}} in {{Inflation-year|UK}}, when adjusted for [[inflation]]).

==Service history and legacy==
During the entire war there was only one instance of a ship being sunk whilst being escorted by an airship. During the final 15 months of the war SS type airships carried out over 10,000 patrols, flying nearly one-and-a-half million miles in more than 50,000 hours. A total of 49 U-boats were sighted, 27 of which were attacked from the air or by ships.<ref name=Uft/>

An SS B.E.2c set the current altitude record for a British airship when it reached {{convert|10300|ft|m|abbr=on}} in the summer of 1916,<ref name=W59/> and the sole Hawk-engined SS Maurice Farman on one occasion carried out an extended patrol of 18 hours 20 minutes. Also in the summer of 1916, an Armstrong Whitworth model coated with black dope carried out night-time operations over France, proving that airships could be of value when operating with military forces over land.<ref>Whale (2008), p.60.</ref>

The SS type was further developed with purpose-built cars to create the [[SSP class airship|SSP]] (Pusher), [[SSZ class airship|SSZ]] (Zero), [[SST class airship|SST]] (Twin) and [[SST class airship|SSE]] (Experimental SST) types. Demand for the versatile "Sea Scouts" was so great that a grand total of 158 of all versions and variants were constructed,<ref name=AHT/> some of which were acquired by France, Italy and the United States.<ref name=Uft/>

Although the SS class types proved invaluable, their use was restricted to coastal patrols in reasonably fair weather owing to their low engine power and comparatively small size. For work farther out at sea and in all weathers, three further classes were developed: the [[Coastal class airship|''Coastal'']], the [[C Star class airship|''C*'']] and [[NS class airship|''North Sea'']]-class ships.<ref name=Vivian/> Each had larger engines and envelopes, carried more crew, and had greater patrol duration than the previous class ships.<ref name=AHT/>

==Operators==
;{{flagu|France|naval}}
;{{flag|Kingdom of Italy}}
;{{flag|Japan|naval}}
*The [[Imperial Japanese Navy]] acquired an SS-3 from the Royal Navy in 1921; it exploded in a hangar at [[Yokosuka Naval District|Yokosuka Naval Base]] in 1922, just weeks after its maiden flight in Japan. The IJN then completed a domestically-produced replica in 1923, which flew once from Yokosuka to Osaka and back to [[Kasumigaura Air Field]]; it exploded in midair in 1924, killing all of its crew.<ref>{{cite web|last1=Starkings|first1=Peter|title=Japanese Military Airships 1910–1945|url=http://www.j-aircraft.com/research/jas_jottings/japanese_airships.htm|accessdate=8 September 2015}}</ref>
;{{flagu|United Kingdom|naval}}
*[[Royal Navy]]
;{{flagu|United States|1911}}
*[[United States Navy]]

==Specifications (typical)==
{{aerospecs
|ref=<ref name=AHT/>
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->eng
|crew=
|capacity=
|length m=43.7
|length ft=143  
|length in=5
|span m=
|span ft=
|span in=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->8.5
|dia ft=<!-- airships etc -->27  
|dia in=<!-- airships etc -->9
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=13.3
|height ft=43 
|height in=9
|wing area sqm=
|wing area sqft=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->1,700
|volume ft3=<!-- lighter-than-air -->60,000
|aspect ratio=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=
|gross weight lb=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=Green/Renault/Rolls-Royce
|eng1 kw=<!-- prop engines -->56
|eng1 hp=<!-- prop engines -->75  
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=80
|max speed mph=50 
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m= 1,500
|ceiling ft=greater than 5,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1={{convert|160|lb|kg|abbr=on}} bombs
|armament2=[[Lewis Gun]] <ref name=LG/><ref name=W58/>
|armament3=
|armament4=
|armament5=
|armament6=
}}

===Comparative specifications===
{| class=wikitable
|-
! width="100"|General characteristics !! width="125"|SS <sup>[BE]</sup><ref name=BE2c>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SS%20Airships%20BE2C.htm SS (B.E.2c car) data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref> !! width="125"|SS <sup>[MF]</sup><ref name=Farman>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SS%20Airships%20Farman.htm SS (Farman car) data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref> !! width="125"|SS <sup>[AW]</sup><ref name=AW>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SS%20Airships%20Armstrong.htm SS (Armstrong Whitworth car) data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref> !! width="125"|SSP<ref name=SSP>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SSP%20Airships.htm SSP data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref> !! width="125"|SSZ<ref name=SSZ>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SSZ%20Airships.htm SSZ data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref> !! width="125"|SST<ref name=SST>[http://aht.ndirect.co.uk/airships/Technical%20Spec/SST%20Airships.htm SST data.] Airship Heritage Trust. Retrieved on 18 March 2009.</ref>
|-  
| Length || {{convert|143|ft|5|in|m|abbr=on}} || {{convert|143|ft|5|in|m|abbr=on}} || {{convert|143|ft|5|in|m|abbr=on}} || {{convert|143|ft|5|in|m|abbr=on}} || {{convert|143|ft|5|in|m|abbr=on}} || {{convert|165|ft|m|abbr=on}}
|-
| Diameter || {{convert|27|ft|9|in|m|abbr=on}} || 30 / 32&nbsp;ft || {{convert|32|ft|m|abbr=on}} || {{convert|30|ft|m|abbr=on}} || {{convert|30|ft|m|abbr=on}} || {{convert|35|ft|6|in|m|abbr=on}}
|-
| Height || {{convert|43|ft|5|in|m|abbr=on}} || {{convert|43|ft|5|in|m|abbr=on}} || {{convert|43|ft|5|in|m|abbr=on}} || {{convert|43|ft|5|in|m|abbr=on}} || {{convert|46|ft|m|abbr=on}} || {{convert|49|ft|m|abbr=on}}
|-
| Volume || {{convert|60000|cuft|m3|abbr=on}} || 60 / 70,000 cu ft || {{convert|70000|cuft|m3|abbr=on}} || {{convert|70000|cuft|m3|abbr=on}} || {{convert|70000|cuft|m3|abbr=on}} || {{convert|100000|cuft|m3|abbr=on}}
|-
| Ballonet <ref>SSTs had four ballonets; all others having two.</ref> || {{convert|12750|cuft|m3|abbr=on}} || 12,750 / 19,600 cu ft || {{convert|19600|cuft|m3|abbr=on}} || {{convert|19600|cuft|m3|abbr=on}} || {{convert|19600|cuft|m3|abbr=on}} || {{convert|19600|cuft|m3|abbr=on}}
|-
| Gross lift || 1.85 t || 2.2 t <ref>70,000 cu ft envelope</ref>|| 2.2 t || 2.2 t || 2.2 t || 3.1 t
|-
| Disposable lift || 0.64 t || 0.6 / ~0.8 t || 0.7 t || 0.7 t || 0.6 t || 1.0 t
|-
| Car length || {{convert|24|ft|m|abbr=on}} || {{convert|20|ft|m|abbr=on}} || {{convert|26|ft|m|abbr=on}} || {{convert|26|ft|m|abbr=on}} || {{convert|17|ft|6|in|m|abbr=on}} || {{convert|17|ft|6|in|m|abbr=on}}
|-
| Engine type || [[Renault]] || Renault || [[Green Engine Co|Green]] || [[Rolls-Royce Hawk|Rolls-Royce]]/Green || Rolls-Royce || Rolls-Royce/[[Sunbeam Motor Car Company#Sunbeam-Coatalen engines|Sunbeam]]
|-
| Engine power || 70 / 75&nbsp;hp || {{convert|75|hp|abbr=on}} || {{convert|100|hp|abbr=on}} || 75 / 100&nbsp;hp || {{convert|75|hp|abbr=on}} || 75 / 100&nbsp;hp
|-
| Fuel capacity || 60 gal || 64 gal || 90 gal || 120 gal || 120 gal || 120 gal          
|-
! '''Performance''' !! !! !! !! !! !!
|-  
| Maximum speed || 50 / 52&nbsp;mph <ref>52 mph with three planes</ref> || {{convert|40|mi/h|km/h|abbr=on}} || {{convert|45|mi/h|km/h|abbr=on}} || {{convert|52|mi/h|km/h|abbr=on}} || {{convert|53|mi/h|km/h|abbr=on}} || {{convert|57|mi/h|km/h|abbr=on}}
|-
| Endurance<br>(full speed) || 7–8 hours || 7–8 hours || 12 hours || 12 hours || 17 hours || 17 hours
|-
| Endurance<br>(half speed) || 14–16 hours || 14–16 hours || 24 hours || 24 hours || || 
|-
| Rate of climb || 700&nbsp;ft/min || 790&nbsp;ft/min || 500&nbsp;ft/min || 500&nbsp;ft/min || 1,200&nbsp;ft/min || 1,200&nbsp;ft/min
|}

<sub>Notes: · [BE] B.E.2c car · [MF] Farman car · [AW] Armstrong Whitworth car</sub>

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
*[[British blimps operated by the USN]]
*[[List of aircraft of the Royal Naval Air Service]]
*[[List of British airships]]
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
* {{cite book
  | last = Barnes C.H. & James D.N
  | first =
  | authorlink =
  | coauthors =
  | title =Shorts Aircraft since 1900
  | publisher =Putnam
  | year = 1989
  | location =London 
  | pages =560
  | url =
  | doi =
  | isbn = 0-85177-819-4}}
* {{cite book
  | last = Whale
  | first = George
  | authorlink =
  | coauthors =
  | title = British Airships: Past Present and Future 
  | publisher = Bastian Books 
  | year = 2008
  | location = Toronto, Canada
  | pages = 124
  | url =
  | doi =
  | isbn = 0-554-30772-3}}  
{{refend}}

== External links ==
{{Commons category|SS-class blimp}}
*[http://aht.ndirect.co.uk/airships/ss/index.html SS class airships on the Airship Heritage Trust website]
*[http://www.usbornefamilytree.com/neville1883.htm Commander N. F. Usborne's involvement with the development of the SS class airship.]
*[http://www.britishpathe.com/record.php?id=75875 Newsreel footage of an SS Class airship being launched and in flight]

{{RNAS blimps}}
{{wwi-air}}

[[Category:Airships of the United Kingdom]]
[[Category:British patrol aircraft 1910–1919]]
[[Category:Military airships of World War I]]
[[Category:Vickers airships]]