{{Infobox book | 
| name          = A Strange Manuscript Found in a Copper Cylinder
| title_orig    = 
| translator    = 
| image         = File:Strange manuscript.jpg
| caption       = Cover
| author        = [[James De Mille]]
| illustrator   = 
| cover_artist  = 
| country       = Canada
| language      = English
| series        = 
| genre         = [[Fantasy novel]]
| publisher     = [[Harper & Brothers]]
| release_date  = 1888
| english_release_date =
| media_type    = Print ([[Hardcover|Hardback]])
| pages         = 291 pp
| isbn          = 978-1515037941 
| preceded_by   = 
| followed_by   = 
}}

'''''A Strange Manuscript Found in a Copper Cylinder''''' is the most popular book by [[James De Mille]]. It was [[Serial (literature)|serial]]ized posthumously and anonymously<ref>{{cite web|url=http://www.biographi.ca/EN/ShowBio.asp?BioId=39065 |title=De Mille, James |publisher=Dictionary of Canadian Biography |accessdate=2013-09-15}}</ref> in ''[[Harper's Weekly]]'',<ref name="standish">{{Cite book
  | last = Standish
  | first = David
  | title = Hollow earth: the long and curious history of imagining strange lands, fantastical creatures, advanced civilizations, and marvelous machines below the earth's surface
  | publisher = Da Capo Press
  | year = 2006
  | url = https://books.google.com/?id=J1BC7eFLBFcC&pg=PA203&lpg=PA203
  | isbn = 0-306-81373-4}}
</ref>
and published in book form by Harper and Brothers of [[New York City]] during 1888. It was serialized subsequently in the United Kingdom and Australia, and published in book form in the United Kingdom and Canada. Later editions were published from the plates of the Harper and Brothers first edition, during the late 19th and early 20th centuries.

The satiric and fantastic romance is set in an imaginary semi-[[tropical]] land in [[Antarctica]] inhabited by [[prehistoric]] monsters and a cult of death-worshipers called the Kosekin. Begun many years before it was published, it is reminiscent of [[Edgar Allan Poe]]'s ''[[The Narrative of Arthur Gordon Pym of Nantucket]]'' and anticipates the exotic locale and fantasy-adventure elements of works of the "[[Lost World (genre)|Lost World genre]]" such as [[Arthur Conan Doyle]]'s ''[[The Lost World (Arthur Conan Doyle)|The Lost World]]'' and [[Edgar Rice Burroughs]]' ''[[The Land That Time Forgot (novel)|The Land That Time Forgot]]'', as well as innumerable prehistoric world movies based loosely on these and other works. The title and locale were inspired by [[Edgar Allan Poe]]'s ''[[MS. Found in a Bottle|Ms. Found in a Bottle]]''.

It was unfortunate for De Mille's reputation as a writer that this work was published after ''[[She (novel)|She]]'' and ''[[King Solomon's Mines]]''. Although [[H. Rider Haggard]]'s works were well known by then, the actual composition of De Mille's romance pre-dated the publication of the popular romances and his ideas were not in the least derivative from Haggard's better known works.<ref>{{cite news |title=Literary Notes |work=New York Times |page=3 |date=May 14, 1888 |accessdate=15 January 2009|quote= |url=https://query.nytimes.com/gst/abstract.html?res=9901E0DF1E38E533A25757C1A9639C94699FD7CF }}</ref>

==Plot summary==
The main story of the novel is the narrative of the adventures of Adam More, a British sailor shipwrecked on a homeward voyage from [[Tasmania]]. After passing through a subterranean tunnel of volcanic origin, he finds himself in a "lost world" of prehistoric animals, plants and people sustained by volcanic heat despite the long Antarctic night.

A secondary plot of four [[yacht]]smen who find the [[manuscript]] written by Adam More and sealed in a [[copper]] cylinder forms a frame for the central narrative. They comment on More's report, and one identifies the Kosekin language as a [[Semitic language]], possibly derived from [[Hebrew language|Hebrew]].

In his strange volcanic world, More also finds a well-developed human society which in the tradition of topsy-turvy worlds of [[folklore]] and [[satire]] (compare [[Sir Thomas More]]'s ''[[Utopia (book)|Utopia]]'', ''[[Erewhon]]'' by [[Samuel Butler (1835-1902)|Samuel Butler]], or [[Charlotte Perkins Gilman]]'s ''[[Herland (novel)|Herland]]'') has reversed the values of [[Victorian era|19th century Western]] society: wealth is scorned and poverty is revered, death and darkness are preferred to life and light. Rather than accumulating wealth, the natives seek to divest themselves of it as quickly as possible. Whatever they fail to give away to wealthy people is confiscated by the government, which imposes the burden of wealth upon its unfortunate subjects at the beginning of the next year of [[reverse taxation]] as a form of punishment.

==Critical reception==

Contemporary reviews of the novel were coloured by the previous release of ''[[She (novel)|She]]'' and ''[[King Solomon's Mines]]'' by [[H. Rider Haggard]].  A review in the ''[[New York Times]]'', 21 May 1888, notes that "if the author of 'A Strange Manuscript' were living he would find it a quite hopeless task to persuade people that he had not read and imitated ''She'' and ''King Solomon's Mines''"
while a review in the ''[[Brooklyn Daily Eagle]]'', 3 June 1888, called the book a "somewhat belated story".  Both reviews, along with a July 1888 review in ''The Week'', a Toronto newspaper, commented positively on the illustrations by [[Gilbert Gaul (artist)|Gilbert Gaul]].<ref>{{cite book | last=De Mille | first=James | authorlink=James De Mille | title=A Strange Manuscript Found in a Copper Cylinder |editor=Daniel Burgoyne| location=Peterborough | publisher=Broadview Press | year=2011 | pages=337–342}}</ref>

== See also ==
*[[Lost World (genre)]]

==References==
<references/>
A scholarly edition of the work was published by the Centre for Editing Early Canadian Texts (CEECT) (see below). This edition is the source of the information provided by this article.
*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=27}}

==Bibliography==
*''A Strange Manuscript Found in a Copper Cylinder'' was first published in nineteen installments in ''Harper's Weekly'', Vol. 32, from January 7, 1888 (No. 1620) to May 12, 1888 (No. 1638). Each installment was accompanied by an illustration by Gilbert Gaul.
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. First American Edition. Harper and Brothers, New York, 1888.
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. First British Edition. Chatto and Windus, London, 1888.
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. First Canadian Edition. Robinson, Montreal, 1888.
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. New Canadian Library Edition. McClelland and Stewart, Toronto, 1969.
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. Edited by Malcolm Parks. Carleton University Press, Ottawa, 1986. ISBN 0-88629-039-2 (hardcover), ISBN 0-7735-2167-4 (paperback)
*De Mille, James. ''A Strange Manuscript Found in a Copper Cylinder''. Edited by Daniel Burgoyne. Broadview Press, Peterborough, 2011. ISBN 978-1-55111-959-5.

== External links ==
*[http://www.biographi.ca/EN/ShowBio.asp?BioId=39065 Biography at the ''Dictionary of Canadian Biography Online'']
{{gutenberg|no=6709|name=A Strange Manuscript Found in a Copper Cylinder}}
*[http://gaslight.mtroyal.ca/strgmenu.htm The book as serialized in ''Harper's Weekly'' in 1888]

{{DEFAULTSORT:Strange Manuscript Found in a Copper Cylinder, A}}
[[Category:1888 novels]]
[[Category:Canadian fantasy novels]]
[[Category:New Canadian Library]]
[[Category:Novels set in Antarctica]]
[[Category:Hollow Earth in fiction]]
[[Category:Lost world novels]]
[[Category:Novels first published in serial form]]
[[Category:Works published anonymously]]
[[Category:Works originally published in Harper's Weekly]]
[[Category:Novels published posthumously]]
[[Category:Harper & Brothers books]]
[[Category:Dystopian novels]]