{{redirect|Scaled Composites Voyager|the Scaled Composites SpaceShipTwo "Voyager"|VSS Voyager}}
{{redirect|Voyager aircraft|the RAF Voyager|Future Strategic Tanker Aircraft|other uses|Voyager (disambiguation)}}
{|{{Infobox Aircraft Begin
  |name =Model 76 Voyager
  |image =Voyager aircraft.jpg
  |caption =Voyager returning from its flight
}}{{Infobox Aircraft Type
  |type =Record plane
  |manufacturer = Rutan Aircraft Factory
  |designer = [[Burt Rutan]]
  |first flight =June 22, 1984
  |introduced =1984
  |retired =1987
  |status =
  |primary user =
  |more users =
  |produced =
  |number built =1
  |unit cost =
  |variants with their own articles =
}}
{{Infobox Aircraft Career
  |type = 
  |other names = 
  |construction number = 
  |construction date = 
  |civil registration = N269VA
  |first flight = 
  |flights = 
  |total hours = 
  |total distance = 
  |fate = 
  |preservation= [[National Air and Space Museum]]
}}
|}
The '''Rutan Model 76 Voyager''' was the first [[aircraft]] to fly around the world without stopping or refueling. It was piloted by [[Dick Rutan]] and [[Jeana Yeager]]. The flight took off from [[Edwards Air Force Base]]'s 15,000 foot (4,600 m) runway in the [[Mojave Desert]] on December 14, 1986, and ended 9 days, 3 minutes and 44 seconds later on December 23, setting a [[flight endurance record]]. The aircraft flew westerly 26,366 statute miles (42,432&nbsp;km; the [[Fédération Aéronautique Internationale|FAI]] accredited distance is 40,212&nbsp;km)<ref>{{cite web|url=http://www.fai.org/fai-record-file/?recordId=8391|title=Official FAI database|accessdate=2012-12-23}}</ref> at an average altitude of 11,000 feet (3,350&nbsp;m).

==Design and development==
[[File:Voyager Aircraft.JPG|thumb|right|Voyager on display in the [[National Air and Space Museum]]]]

The aircraft was first imagined by [[Jeana Yeager]], Dick Rutan, and Dick's brother [[Burt Rutan]] as they were at lunch in 1981. The initial idea was first sketched out on the back of a [[napkin]]. Voyager was built in [[Mojave, California]], over a period of five years. Voyager was built mainly by a group of volunteers working under both the Rutan Aircraft Factory and an organization set up under the name Voyager Aircraft.

The [[airframe]], largely made of [[fiberglass]], [[carbon fiber]], and [[Kevlar]], weighed 939 pounds (426&nbsp;kg) when empty. With the engines included, the unladen weight of the plane was 2250&nbsp;lb (1020.6&nbsp;kg). However, when it was fully loaded before the historic flight, it weighed 9,694.5 pounds (4,397&nbsp;kg) due to the large amount of fuel required for the long-distance flight.<ref name="NASM">[https://airandspace.si.edu/collection-objects/rutan-voyager Rutan Voyager{{spaced ndash}}Smithsonian National Air and Space Museum]</ref> The aircraft had an estimated [[lift to drag ratio]] (L/D) of 27.<ref>David Noland, "Steve Fossett and Burt Rutan's Ultimate Solo: Behind the Scenes," ''Popular Mechanics'', Feb. 2005 ([http://www.popularmechanics.com/science/air_space/1262012.html?page=3 web version])</ref>  The [[Canard (aeronautics)|canard]] and wing airfoils were custom designed and the aircraft was analyzed using [[computational fluid dynamics]].<ref>Lednicer, David, "A VSAERO Analysis of Several Canard Configured Aircraft," SAE paper 881485, presented at the SAE Aerospace Technology Conference and Exposition, Anaheim, California, October 1988.</ref>  [[Vortex generator]]s were added to the [[Canard (aeronautics)|canard]], to reduce sensitivity to surface contamination.<ref>Bragg, M.B. and Gregorek, G.M., "An Experimental Study of a High Performance Canard Airfoil with Boundary Layer Trip and Vortex Generators," AIAA Paper No. 86-0781-CP, The 14th Aerodynamic Testing Conference Publication, March 1986.</ref>

Voyager had [[Push-pull configuration|front and rear propellers, powered by separate engines]]. It was originally flown on June 22, 1984, powered by [[Lycoming O-235]] engines with fixed-pitch propellers.{{sfn|Yeager|Rutan|Patton|1987|p=107}} In November 1985, the aircraft was rolled out, fitted with world-flight engines, an air-cooled [[Continental O-240|Teledyne Continental O-240]] in the forward location and a liquid-cooled [[Continental O-200|Teledyne Continental IOL-200]] in the aft location.{{sfn|Yeager|Rutan|Patton|1987|p=121}}  Both were fitted with wooden, variable pitch electrically actuated, [[MT-Propeller]]s.{{sfn|Yeager|Rutan|Patton|1987|p=124}}  The plan was for the rear engine to be operated throughout the flight. The front engine was intended to provide additional power for takeoff and the initial part of the flight at heavy weights.

On July 15, 1986, Rutan and Yeager completed a test flight, off the coast of California, in which they flew for 111 hours and 44 minutes, traveling {{convert|11857|mi||adj=pre|[[Mile#Statute mile|statute]]}} ,{{sfn|Yeager|Rutan|Patton|1987|p=181}} breaking the previous record held since 28 May 1931 by a Bellanca CH-300 fitted with a DR-980 diesel engine, piloted by Walter Edwin Lees and Frederic Brossy, set a record for staying aloft for 84 hours and 32 minutes without being refueled.  . The first attempt at this flight was marred by the failure of a propeller pitch change motor and they had to make an emergency landing at [[Vandenberg Air Force Base]].{{sfn|Yeager|Rutan|Patton|1987|p=66}} On a test flight on September 29, 1986, the airplane had to make an emergency landing due to a propeller blade departing the aircraft.{{sfn|Yeager|Rutan|Patton|1987|p=198}}  As a result, the decision was made to switch to aluminium [[Hartzell Propeller|Hartzell]] hydraulically actuated propellers.{{sfn|Yeager|Rutan|Patton|1987|p=209}} In a crash program, Hartzell made custom propellers for the aircraft, which were first flown on November 15, 1986.{{sfn|Yeager|Rutan|Patton|1987|p=213}}<ref>Roncz, John G., "Propeller Development for the Rutan Voyager," SAE paper 891034, presented at the SAE General Aviation Aircraft Meeting & Exposition, Wichita, Kansas, April 1989.</ref>

==World flight==
Voyager's world flight takeoff took place on the longest runway at [[Edwards Air Force Base|Edwards AFB]] at 8:01 am local time on December 14, 1986, with 3,500 of the world's press in attendance.{{sfn|Norris|1988|p=19}} As the plane accelerated, the tips of the wings, which were heavily loaded with [[fuel]], were damaged as they unexpectedly flew down and scraped against the runway, ultimately causing pieces [[Wingtip device|(winglets)]] to break off at both ends.  (The pilot had wanted to gain enough speed that the inner wings, rather than the fragile outer wings, would lift the plane; in 67 test flights, the plane had never been loaded to capacity.)  The aircraft accelerated very slowly and needed approximately 14,200 feet (2.7&nbsp;mi)(4.3&nbsp;km) of the runway to gain enough speed to lift from the ground, the wings arching up dramatically just before take-off. The two damaged winglets remained attached to the wings by only a thin layer of carbon fiber and were removed by flying the Voyager in a [[Slip (aerodynamics)|slip]] which introduced side-loading, tearing the winglets off completely.  Some of the carbon fiber skin was pulled off in the process, exposing the blue form core. Burt Rutan following with pilot [[Mike Melvill]] determined Voyager was still within its performance specifications despite the damage and decided to allow the flight to continue.  During the flight, the two pilots had to deal with extremely cramped quarters. To reduce [[stress (medicine)|stress]], the two had originally intended to fly the plane in three-hour shifts, but flight handling characteristics while the plane was heavy prevented routine changeovers and they became very fatigued. Rutan reportedly stayed at the controls without relief for almost the first three days of the flight.

[[File:VoyagerAircraftWingtipAtNASM.jpg|thumb|left|Damaged left wingtip]]
The plane also continuously reminded the pilots of its pitch instability and fragility. They had to maneuver around bad weather numerous times, most perilously around the {{convert|600|mi|km|adj=mid|sigfig=1|-wide}} [[1986 Pacific typhoon season#Typhoon Marge (Aning)|Typhoon Marge]].<ref name="JTWC3">Joint Typhoon Warning Center (1987). [http://www.usno.navy.mil/NOOC/nmfc-ph/RSS/jtwc/atcr/1986atcr/pdf/chapter3.pdf Chapter 3: Northwest Pacific and North Indian Ocean Tropical Cyclones.] Retrieved on 2007-12-19.</ref> [[Libya]] denied access to the country's [[airspace]] in response to [[Operation El Dorado Canyon]] earlier that year, forcing precious fuel to be used. There were contentious radio conversations between pilot Dick and his brother as Dick flew around weather and at one time, turned around and began doubling back.  The belief on the ground was that he was flying the airplane sloppily and was wasting fuel. The strong and often conflicting personalities of Melvill and the Rutan brothers made for many vigorous disagreements over the radio during the world flight.{{citation needed|date=November 2014}}  As they neared [[California]] to land, a [[fuel pump]] failed and had to be replaced with its twin pumping fuel from the other side of the aircraft.

In front of 55,000 spectators and a large press contingent, including 23 live feeds breaking into scheduled broadcasting across Europe and North America, the plane safely came back to earth, touching down at 8:06&nbsp;a.m. at the same airfield 9 days after take-off. Rutan made three low passes over the landing field before putting Voyager down.  The average speed for the flight was {{convert|116|mph}}. There were {{convert|106|lb}} of fuel remaining in the tanks,<ref name="NASM"/> only about 1.5% of the fuel they had at take-off.

Sanctioned by the FAI and the [[Aircraft Owners and Pilots Association|AOPA]], the flight was the first successful aerial nonstop, non-refueled circumnavigation of the Earth that included two passes over the Equator (as opposed to shorter ostensible "circumnavigations" circling the North or South Pole).  This feat has since been accomplished only one other time, by [[Steve Fossett]] in the [[Global Flyer]]. For the feat, Yeager, the Rutans, and crew chief/builder Bruce Evans received the 1986 [[Collier Trophy]].<ref name="larson">{{Cite journal | last = Larson| first = George C.| title = From Point A to Point A|journal = Air & Space Smithsonian|date=January 2012}}, p. 84.</ref>

Voyager is now on display at the [[Smithsonian Institution]]'s [[National Air and Space Museum]] in [[Washington, DC]].

==Specifications==
{{aerospecs
|ref=<ref name="NASM"/>
|met or eng?=eng
|genhide=
|crew=Two pilots
|capacity=
|length m=8.90
|length ft=29
|length in=2
|span m=33.80
|span ft=110
|span in=8
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.10
|height ft=10
|height in=3
|wing area sqm=33.72
|wing area sqft=363
|empty weight kg=1020.6
|empty weight lb= 2250
|gross weight kg=4397.4
|gross weight lb=9694.5
|eng1 number=1
|eng1 type=Teledyne [[Continental O-240]]
|eng1 kw= 100
|eng1 hp=130
|eng2 number=1
|eng2 type=Teledyne [[Continental O-200|Continental IOL-200]]
|eng2 kw=81<!-- prop engines -->
|eng2 hp=110
|perfhide=
|max speed kmh=196
|max speed mph=122
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=42,212
|range miles=24,986
|endurance h=216
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

}}

==See also==
* The [[Virgin Atlantic GlobalFlyer]] was a [[jet engine|jet-powered]] aircraft also designed by Burt Rutan and built by Scaled Composites, in which [[Steve Fossett]] made the first ''solo'' nonstop flight around the world in 2005.
* [[VSS Voyager]] is another Burt Rutan-designed craft.

==References==

=== Notes ===
{{reflist|30em}}

=== Bibliography ===
{{Refbegin|30em}}
* David H. Onkst. [http://www.centennialofflight.gov/essay/Explorers_Record_Setters_and_Daredevils/rutan/EX32.htm Dick Rutan, Jeana Yeager, and the Flight of the Voyager.] U.S. Centennial of Flight Commission.
* {{cite journal|url=http://www.time.com/time/magazine/article/0,9171,963119,00.html|title=Flight of Fancy|publisher=[[Time (magazine)|Time]]|first1=Richard|last1=Stengel|first2=Scott |last2=Brown|location=Mojave|date=December 29, 1986}}
* {{cite book|first=Jack|last=Norris|title=Voyager The World Flight; The Official Log, Flight Analysis and Narrative Explanation|location=Northridge, California|year=1988|isbn=0-9620239-0-6|ref=harv}}
* {{cite book|first1=Jeana|last1=Yeager|first2=Dick|last2=Rutan|first3=Phil|last3=Patton|title=Voyager|location=New York, New York|publisher=Alfred A. Knopg|year=1987|isbn=1-885283-24-5|ref=harv}}
{{refend}}

==External links==
{{commons category-inline|Rutan Voyager}}

{{Portal bar|Aviation}}

[[Category:Rutan aircraft|Voyager]]
[[Category:Individual aircraft|Voyager]]
[[Category:United States experimental aircraft 1980–1989]]
[[Category:Individual aircraft in the collection of the Smithsonian Institution]]
[[Category:Canard aircraft]]