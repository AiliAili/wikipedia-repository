'''Bengt-Olov Palmqvist''' [bɛŋkt- olov palmqvist] is a Swedish-Australian [[musicology|musicologist]] and music theorist, with particular expertise on the subject of [[rhythm]], within [[music theory]]. He has published a series of work-books titled ''"Refinement of Rhythm"'', that are used widely as resources for rhythmic dictation, grounding and research.<ref>{{cite web|title=Refinement of Rhythm, About the Author|url=http://www.refinementofrhythm.com.au/author.html|work=Refinement of Rhythm|publisher=Parallels}}</ref> He is also noted among musicians in Australia for his choral conducting, composing and jazz arranging skills.<ref>{{cite web|title=Academic Staff - A/Bengt-Olov Palmqvist|url=http://music.anu.edu.au/academic-staff/Bengt-Olov-Palmqvist|work=Academic Staff - School of Music}}</ref> 

==Education==

Palmqvist graduated with a Bachelor of Music from Ingesund College of Music, Arvika, and a Master of Music in Pedagogy, Aural and Music Theory from the Royal College of Music in Stockholm.<ref>{{cite web|title=Academic Staff - A/Bengt-Olov Palmqvist|url=http://music.anu.edu.au/academic-staff/Bengt-Olov-Palmqvist|work=Academic Staff - School of Music}}</ref> He currently is an associate professor in Aural and Music Theory at the [[Australian National University]] School of Music.

==Refinement of Rhythm==

In 2005, Palmqvist created two workbooks in a series titled ''"Refinement of Rhythm"'', compiled to assist college and university music students in their aural studies from a preliminary to very advanced stage. It is one of the only books compiled for this purpose to date, that focuses solely on one aspect of music theory and aural practice: rhythm. As such, it has been generally well received by universities and higher music learning facilities internationally, and throughout Australia.<ref>{{cite web|last=Caeser|first=Michael|title=Review|url=http://www.refinementofrhythm.com.au/reviews.html|work=Refinement of Rhythm|publisher=Parallels|accessdate=14 November 2011}}</ref>

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
*
*
*
*

{{DEFAULTSORT:Palmqvist, Bengt-Olov}}
[[Category:Swedish musicologists]]
[[Category:Living people]]