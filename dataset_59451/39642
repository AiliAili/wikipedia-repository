'''Psychoacoustics''' is the scientific study of [[sound]] [[perception]]. More specifically, it is the branch of science studying the [[psychological]] and [[physiological]] responses associated with sound (including [[speech]] and [[music]]). It can be further categorized as a branch of [[psychophysics]]. Psychoacoustics received its name from a field within psychology—i.e., recognition science—which deals with all kinds of human perceptions. It is an interdisciplinary field of many areas, including psychology, acoustics, electronic engineering, physics, biology, physiology, and computer science.<ref>{{cite book|last1=Ballou|first1=G|title=Handbook for Sound Engineers|date=2008|publisher=Burlington: Focal Press|page=43|edition=Fourth}}</ref>

==Background==

Hearing is not a purely mechanical phenomenon of [[wave propagation]], but is also a sensory and perceptual event; in other words, when a person hears something, that something arrives at the [[ear]] as a mechanical sound wave traveling through the air, but within the ear it is transformed into neural [[action potentials]]. These nerve pulses then travel to the brain where they are perceived. Hence, in many problems in acoustics, such as for [[Auditory system|audio processing]], it is advantageous to take into account not just the mechanics of the environment, but also the fact that both the ear and the brain are involved in a person’s listening experience.

The [[inner ear]], for example, does significant [[signal processing]] in converting sound [[waveform]]s into neural stimuli, so certain differences between waveforms may be imperceptible.<ref>{{cite book | title = The Sense of Hearing | author = Christopher J. Plack | publisher = Routledge | year = 2005 | isbn = 0-8058-4884-3 | url = https://books.google.com/books?id=DoGzm3soUoMC&pg=PA65&dq=ear+hearing+cochlea++inauthor:plack }}</ref>   [[Data compression]] techniques, such as [[MP3]], make use of this fact.<ref>{{cite book | title = The Sound Blaster Live! Book |author1=Lars Ahlzen |author2=Clarence Song | publisher = No Starch Press | year = 2003 | isbn = 1-886411-73-5 | url = https://books.google.com/books?id=tKO-truWww8C&pg=PA310&dq=mp3++imperceptible+ear }}</ref> In addition, the ear has a nonlinear response to sounds of different intensity levels; this nonlinear response is called [[loudness]]. [[Telephone network]]s and audio [[noise reduction]] systems make use of this fact by nonlinearly compressing data samples before transmission, and then expanding them for playback.<ref>{{cite book | title = Modern dictionary of electronics | author = Rudolf F. Graf | publisher = Newnes | year = 1999 | isbn = 0-7506-9866-7 | url = https://books.google.com/books?id=o2I1JWPpdusC&pg=PA137&dq=compression+expansion+noise-reduction+telephone }}</ref>    Another effect of the ear's nonlinear response is that sounds that are close in frequency produce phantom beat notes, or [[intermodulation]] distortion products.<ref>{{cite book | title = Handbook of Clinical Audiology |author1=Jack Katz |author2=Robert F. Burkard |author3=Larry Medwetsky  |last-author-amp=yes | publisher = Lippincott Williams & Wilkins | year = 2002 | isbn = 0-683-30765-7 | url = https://books.google.com/books?id=Aj6nVIegE6AC&pg=PA43&dq=beat+distortion++ear }}</ref>

The term "psychoacoustics" also arises in discussions about cognitive psychology and the effects that personal expectations, prejudices, and predispositions may have on listeners' relative evaluations and comparisons of sonic aesthetics and acuity and on listeners' varying determinations about the relative qualities of various musical instruments and performers. The expression that one "hears what one wants (or expects) to hear" may pertain in such discussions.{{Citation needed|date=September 2015}}

== Limits of perception ==
[[File:Perceived Human Hearing.svg|thumb|An [[equal-loudness contour]]. Note peak sensitivity around {{nowrap|2–4 kHz,}} the [[Voice frequency|frequency around which the human voice centers.]]]]

The human ear can nominally hear sounds in the range {{nowrap|20 [[Hertz|Hz]]}} {{nowrap|(0.02 kHz)}} to {{nowrap|20,000 Hz}} {{nowrap|(20 kHz).}}  The upper limit tends to decrease with age; most adults are unable to hear above 16&nbsp;kHz. The lowest frequency that has been identified as a musical tone is 12&nbsp;Hz under ideal laboratory conditions.<ref name=Olson/> Tones between 4 and 16&nbsp;Hz can be perceived via the body's [[sense of touch]].

Frequency resolution of the ear is 3.6&nbsp;Hz within the octave of {{nowrap|1000 – 2000 Hz.}}  That is, changes in pitch larger than 3.6&nbsp;Hz can be perceived in a clinical setting.<ref name=Olson>{{cite book |title=Music, Physics and Engineering |last=Olson |first=Harry F. |authorlink=Harry F. Olson |year= 1967|publisher=Dover Publications |pages=248–251 |isbn=0-486-21769-8 |url=https://books.google.com/books?id=RUDTFBbb7jAC }}</ref> However, even smaller pitch differences can be perceived through other means. For example, the interference of two pitches can often be heard as a repetitive variation in volume of the tone. This amplitude modulation occurs with a frequency equal to the difference in frequencies of the two tones and is known as [[beat (acoustics)|beating]].

The [[semitone]] scale used in Western musical notation is not a linear frequency scale but [[Logarithmic scale|logarithmic]]. Other scales have been derived directly from experiments on human hearing perception, such as the [[mel scale]] and [[Bark scale]] (these are used in studying perception, but not usually in musical composition), and these are approximately logarithmic in frequency at the high-frequency end, but nearly linear at the low-frequency end.

The intensity range of audible sounds is enormous. Human ear drums are sensitive to variations in the sound pressure, and can detect pressure changes from as small as a few [[micropascal]]s to greater than {{nowrap|1 [[Bar (unit)|bar]].}}  For this reason, [[sound pressure|sound pressure level]] is also measured logarithmically, with all pressures referenced to {{nowrap|20 [[Pascal (unit)|µPa]]}} (or 1.97385&times;10<sup>−10</sup> [[atmosphere (unit)|atm]]). The lower limit of audibility is therefore defined as {{nowrap|0 [[decibel|dB]],}} but the upper limit is not as clearly defined. The upper limit is more a question of the limit where the ear will be physically harmed or with the potential to cause [[noise-induced hearing loss]].

A more rigorous exploration of the lower limits of audibility determines that the minimum threshold at which a sound can be heard is frequency dependent.  By measuring this minimum intensity for testing tones of various frequencies, a frequency dependent [[absolute threshold of hearing]] (ATH) curve may be derived.  Typically, the ear shows a peak of sensitivity (i.e., its lowest ATH) between {{nowrap|1–5 kHz,}} though the threshold changes with age, with older ears showing decreased sensitivity above 2&nbsp;kHz.<ref name=Fastl>{{cite book |title=Psychoacoustics: Facts and Models |last=Fastl |first=Hugo | last2=Zwicker| first2=Eberhard |year= 2006 |publisher=Springer |pages=21–22 |isbn=978-3-540-23159-2}}</ref>

The ATH is the lowest of the [[equal-loudness contour]]s. Equal-loudness contours indicate the sound pressure level (dB SPL), over the range of audible frequencies, that are perceived as being of equal loudness.  Equal-loudness contours were first measured by Fletcher and Munson at [[Bell Labs]] in 1933 using pure tones reproduced via headphones, and the data they collected are called [[Fletcher–Munson curves]].  Because subjective loudness was difficult to measure, the Fletcher–Munson curves were averaged over many subjects.

Robinson and Dadson refined the process in 1956 to obtain a new set of equal-loudness curves for a frontal sound source measured in an [[anechoic chamber]]. The Robinson-Dadson curves were standardized as [[International Organization for Standardization|ISO]] 226 in 1986. In 2003, {{nowrap|ISO 226}} was revised as [[equal-loudness contour]] using data collected from 12 international studies.

==Sound localization==
{{Main|Sound localization}}

[[Sound localization]] is the process of determining the location of a sound source. The brain utilizes subtle differences in loudness, tone and timing between the two ears to allow us to localize sound sources.<ref name="Thompson">Thompson, Daniel M. Understanding Audio: Getting the Most out of Your Project or Professional Recording Studio. Boston, MA: Berklee, 2005. Print.</ref> Localization can be described in terms of three-dimensional position: the [[azimuth]] or horizontal angle, the [[zenith]] or vertical angle, and the distance (for static sounds) or velocity (for moving sounds).<ref name="Roads">Roads, Curtis. The Computer Music Tutorial. Cambridge, MA: MIT, 2007. Print.</ref> Humans, as most [[tetrapoda|four-legged animals]], are adept at detecting direction in the horizontal, but less so in the vertical due to the ears being placed symmetrically. Some species of [[owls]] have their ears placed asymmetrically, and can detect sound in all three planes, an adaption to hunt small mammals in the dark.<ref>Lewis, D.P. (2007): Owl ears and hearing. Owl Pages [Online]. Available: http://www.owlpages.com/articles.php?section=Owl+Physiology&title=Hearing [2011, April 5]</ref>

== Masking effects ==
{{Unreferenced section|date=June 2016}}
{{Main|Auditory masking}}

[[File:Audio Mask Graph.png|thumb|Audio masking graph]]Suppose a listener cannot hear a given acoustical signal under silent condition. When a signal is playing while another sound is being played (a masker) the signal has to be stronger for the listener to hear it. The masker does not need to have the frequency components of the original signal for masking to happen. A masked signal can be heard even though it is weaker than the masker. Masking happens when a signal and a masker are played together. It also happens when a masker starts after a signal stops playing. The effects of backward masking is weaker than forward masking. The masking effect has been widely used in psychoacoustical research. With masking you can change the levels of the masker and measure the threshold, then create a diagram of a psychophysical tuning curve that will reveal similar features. Masking effects are also used for audio encoding. The masking effect is used in lossy encoders. It can eliminate some of the weaker sounds, so the listener can not hear the difference. This technique has been used in MP3's.

== Missing fundamental ==
{{Main|Missing fundamental}}

When presented with a [[Harmonic series (music)|harmonic series]] of frequencies in the relationship 2''f'', 3''f'', 4''f'', 5''f'', etc. (where ''f'' is a specific frequency), humans tend to perceive that the pitch is ''f''.

== Software ==<!-- This section is linked from [[MPEG-1 Audio Layer II]] -->
[[File:Acustic Block Diagram.svg|thumb|Perceptual audio coding uses psychoacoustics-based algorithms.]]

The psychoacoustic model provides for high quality [[lossy data compression|lossy signal compression]] by describing which parts of a given digital audio signal can be removed (or aggressively compressed) safely—that is, without significant losses in the (consciously) perceived quality of the sound.

It can explain how a sharp clap of the hands might seem painfully loud in a quiet library, but is hardly noticeable after a car backfires on a busy, urban street.  This provides great benefit to the overall compression ratio, and psychoacoustic analysis routinely leads to compressed music files that are 1/10th to 1/12th the size of high quality masters, but with discernibly less proportional quality loss. Such compression is a feature of nearly all modern lossy audio compression formats. Some of these formats include [[Dolby Digital]] (AC-3), [[MP3]], [[Opus (audio format)|Opus]], [[Ogg Vorbis]], [[Advanced Audio Coding|AAC]], [[Windows Media Audio|WMA]], [[MPEG-1 Layer II]] (used for [[digital audio broadcasting]] in several countries) and [[ATRAC]], the compression used in [[MiniDisc]] and some [[Walkman]] models.

Psychoacoustics is based heavily on [[human anatomy]], especially the ear's limitations in perceiving sound as outlined previously.  To summarize, these limitations are:

*[[High frequency limit]]
*[[Absolute threshold of hearing]]
*[[Temporal masking]]
*[[Simultaneous masking]]

Given that the ear will not be at peak perceptive capacity when dealing with these limitations, a compression algorithm can assign a lower priority to sounds outside the range of human hearing.  By carefully shifting bits away from the unimportant components and toward the important ones, the algorithm ensures that the sounds a listener is most likely to perceive are of the highest quality.

== Music ==
Psychoacoustics includes topics and studies that are relevant to [[music psychology]] and [[music therapy]]. Theorists such as [[Benjamin Boretz]] consider some of the results of psychoacoustics to be meaningful only in a musical context.<ref>{{cite book|last=Sterne|first=Jonathan|title=The Audible Past: Cultural Origins of Sound Reproduction|year=2003|publisher=Duke University Press|location=Durham}}</ref>

[[Irv Teibel]]'s ''[[Environments (album series)|Environments series]]'' LPs (1969–79) are an early example of commercially available sounds released expressly for enhancing psychological abilities.<ref>{{cite web|last1=Cummings|first1=Jim|title=Irv Teibel died this week: Creator of 1970s "Environments" LPs|url=http://earthear.com/blog/archives/198|website=Earth Ear|accessdate=18 November 2015}}</ref>

== Applied psychoacoustics ==
[[File:Psychoacoustic Model.svg|thumb|Psychoacoustics Model]]

Psychoacoustics has long enjoyed a symbiotic relationship with [[computer science]], [[computer engineering]], and [[computer network]]ing. Internet pioneers [[J. C. R. Licklider]] and [[Robert Taylor (computer scientist)|Bob Taylor]] both completed graduate-level work in psychoacoustics, while [[BBN Technologies]] originally specialized in consulting on acoustics issues before it began building the first [[Packet-switching|packet-switched]] computer networks.

Licklider wrote a paper entitled "[[A duplex theory of pitch perception]]".<ref name=" Raychel Rappold">{{cite book |url=http://www.cs.rit.edu/~rpretc/imm/project1/biography.html|first = Raychel |last=Rappold|title=Biography|publisher=Rochester University|accessdate=2015-08-08}}</ref>

Psychoacoustics is applied within many fields from software development, where developers map proven and experimental mathematical patterns; in digital signal processing, where many audio compression codecs such as [[MP3]] and [[Opus (audio format)|Opus]] use a psychoacoustic model to increase compression ratios; in the design of (high end) audio systems for accurate reproduction of music in theatres and homes; as well as defense systems where scientists have experimented with limited success in creating new acoustic weapons, which emit frequencies that may impair, harm, or kill.<ref>http://www.nationaldefensemagazine.org/archive/2002/March/Pages/Acoustic-Energy4112.aspx</ref> It is also applied today within music, where musicians and artists continue to create new auditory experiences by masking unwanted frequencies of instruments, causing other frequencies to be enhanced. Yet another application is in the design of small or lower-quality loudspeakers, which can use the phenomenon of [[missing fundamental]]s to give the effect of bass notes at lower frequencies than the loudspeakers are physically able to produce (see references).

==See also==
{{Portal|Music}}

=== Related fields ===
* [[Cognitive neuroscience of music]]
* [[Music psychology]]

=== Psychoacoustic topics ===
{{colbegin||30em}}
*[[A-weighting]], a commonly used perceptual loudness [[transfer function]]
*[[ABX test]]
*[[Auditory illusion]]s
*[[Auditory scene analysis]] incl. 3D-sound perception, localisation
*[[Binaural beats]]
*[[Scale illusion|Deutsch's Scale illusion]]
*[[Equivalent rectangular bandwidth]] (ERB)
*[[Franssen effect]]
*[[Glissando illusion]]
*[[Haas effect]]
*[[Hypersonic effect]]
*[[Language processing]]
*[[Levitin effect]]
*[[Musical tuning]]
*[[Noise health effects]]
*[[Octave illusion]]
*[[Pitch (music)]]
*[[Precedence effect]]
*[[Psycholinguistics]]
*[[Rate-distortion theory]]
*[[Sound localization]]
*[[Sound of fingernails scraping chalkboard]]
*[[Source separation]]
*[[Sound masking]]
*[[Speech recognition]]
*[[Timbre]]
*[[Tritone paradox]]
{{colend}}

==References==

===Notes===
{{reflist}}

===Sources===
{{refbegin}}
*E. Larsen and R.M. Aarts (2004), [http://www.dse.nl/~rmaarts/ Audio Bandwidth extension. Application of Psychoacoustics, Signal Processing and Loudspeaker Design.], J. Wiley.
*{{cite journal |date=March 2002 |title=Reproducing Low-pitched Signals through Small Loudspeakers |url=http://www.extra.research.philips.com/hera/people/aarts/papers/aar02n4.pdf |format=PDF|journal=Journal of the Audio Engineering Society |volume=50 |issue=3 |pages=147–64 |author1=Larsen E. |author2=Aarts R.M. }}
*{{cite journal |date=February 2006 |title=The Role of Biological System other Than Auditory Air-conduction in the Emergence of the Hypersonic Effect |journal=Brain Research |volume= 1073-1074|pages=339–347 |doi=10.1016/j.brainres.2005.12.096 |pmid=16458271 |author1=Oohashi T. |author2=Kawai N. |author3=Nishina E. |author4=Honda M. |author5=Yagi R. |author6=Nakamura S. |author7=Morimoto M. |author8=Maekawa T. |author9=Yonekura Y. |author10=Shibasaki H. }}blah
{{refend}}

==External links==
*[http://www.newmusicbox.org/article.nmbx?id=4077 The Musical Ear—Perception of Sound]
*{{cite journal |vauthors=Müller C, Schnider P, Persterer A, Opitz M, Nefjodova MV, Berger M |title=[Applied psychoacoustics in space flight] |language=German |journal=Wien Med Wochenschr |volume=143 |issue=23–24 |pages=633–5 |year=1993 |pmid=8178525 }}—Simulation of Free-field Hearing by Head Phones
*[http://lame.sourceforge.net/gpsycho.php GPSYCHO—An Open-source Psycho-Acoustic and Noise-Shaping Model for ISO-Based MP3 Encoders.]
*[http://www.pcmag.com/encyclopedia_term/0,2542,t=perceptual+audio+coding&i=49099,00.asp Definition of: perceptual audio coding]
*[http://people.bath.ac.uk/eespjl/courses/Audio/demo/applets/Masking.html Java appletdemonstrating masking]
*[http://hyperphysics.phy-astr.gsu.edu/hbase/sound/soucon.html HyperPhysics Concepts—Sound and Hearing]
*[http://pactlab-dev.spcomm.uiuc.edu/drupal/infostructure/node/10 The MP3 as Standard Object]

{{Acoustics}}
{{Music cognition}}

{{Authority control}}

[[Category:Psychoacoustics| ]]
[[Category:Cognitive musicology]]
[[Category:Music psychology]]