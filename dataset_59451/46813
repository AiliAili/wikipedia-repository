{{advert|date=December 2015}}
{{Infobox architect
| name                  = Vincent Callebaut
| image                 = Picture of Vincent Callebaut face.jpg
| image_size            = <!-- if image is smaller than 250px -->
| caption               = 
| birth_date            = {{Birth date|mf=yes|1977|05|27}}
| birth_place           = La Louvière {{flag|Belgium}}
| death_date            = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place           = 
| nationality           =  Belgian
| alma_mater            = [[Université libre de Bruxelles]]
| influences            = 
| influenced            = 
| practice              = 
| significant_buildings = Agora Garden [[Taipei]]
| significant_projects  = Dragonfly, Lilypad, King Forest
| significant_design    = 
| awards                = [[Prix Godecharle]] (2001), [[International Architecture Awards]] (2014)
}}
'''Vincent Callebaut''' (b. May 27, 1977 - ) is a Belgian ecological [[architect]]. He designs futuristic-like [[ecodistrict]] projects which take account of several aspects of sustainability (renewable energies, biodiversity, urban agriculture).

==Biography==

Vincent Callebaut was born in 1977 in Belgium. He graduated in 2000 from the Institute [[Victor Horta]], he moved to Paris to intern with architects [[Odile Decq]] and [[Massimiliano Fuksas]] and then founed his own company, « ''Vincent Callebaut Architectures'' ».

==Virtual projects==

===Dragonfly===

The project consists on implementing "[[Vertical farming|vertical farms]]" in [[Manhattan]] along the East River at the South edge of the Roosevelt Island. In a way to rethink the city's food production and encourage each inhabitant to participate to the gardens, one of the visions of the Dragonfly project was defined as: 'Eating an apple just picked out of a collective orchard in the fourth floor while looking at New York through the window and then go back to your office just in the upper floor.'<ref name="culturebox">{{cite web|title=culturebox|url=http://culturebox.francetvinfo.fr/tendances/architecture/architecte-visionnaire-vincent-callebaut-invente-la-ville-de-demain-148595|website=culturebox.francetvinfo.fr|accessdate=2 December 2014}}</ref> This project was inspired by the Japanese movie "[[Castle in the Sky|Laputa : Castle in the sky]]".<ref name="FranceAmerique">{{cite web|title=Dragonfly|url=http://www.france-amerique.com/articles/2009/07/31/dragonfly_une_ferme_dans_un_gratte-ciel.html|website=france-amerique.com|accessdate=2 December 2014}}</ref>

The Dragonfly building is {{convert|575|m}} high, shaped as firefly wings. It is composed of two towers related by a 'bio-climatic' glasshouse.  It deploys itself between two crystalline wings made out of glass and steel. The structure in honeycomb stitch allows the sunlight to pass through the building.<ref name="urbanism" /> This concept is seen as a feeder farm and reconnects the consumers with producers. This prototype of urban farm would have superimposed 132 floors and estending {{convert|600|m}} vertically with garden vegetables, fields, production of meat, milk, poultry and eggs where people could grow their own food. The Dragonfly can accommodate 28 different agricultural fields for the production. It is self-sufficient in energy, water and bio-fertilizing, powered by sunshield and eolian systems. These combine solar and wind power, which would make the Dragonfly self-sufficient.<ref name="inhabitat">{{cite web|title=Inhabitat|url=http://inhabitat.com/dragonfly-urban-agriculture-concept-for-ny/|website=inhabitat.com|accessdate=2 December 2014}}</ref>
Everything is recyclable in a continuous auto-feeding so that nothing is lost.  The spaces between the wings are designed to take advantage of solar energy by accumulating warm air in the structure during winter. During the summer it is cooling which is facilitated through natural ventilation and "evapo-perspiration" from the plants.<ref name="inhabitat" /> 

The objectives of the Dragonfly project were defined as:
*Creating a proximity between the inhabitants and their productions.
*Reducing intermediaries in the production mode.
*Getting close to self-sufficiency which leads to financial savings.<ref name="urbanism">{{cite web|title=Dragonfly|url=http://projets-architecte-urbanisme.fr/projet-dragonfly-de-vincent-callebaut-ferme-verticale-dans-new-york/|website=Architecture Urbanisme|accessdate=16 November 2014}}</ref>

===Lilypad===
{{Main| Floating ecopolis}}
The Lilypad or [[Floating ecopolis]] project is based on the idea of creating a place for future refugees of [[Current sea level rise]] caused by [[Global warming]].

===Agora Garden===


Vincent Callebaut created Agora Garden, a sustainable tower development in [[Taipei]]. This building promotes vertical construction in an overpopulated city.<ref name=FranceInter>{{cite web|title=France inter|url=http://www.franceinter.fr/player/reecouter?play=676754|website=franceinter.fr|accessdate=2 December 2014}}</ref> It is a concept of eco-construction to reduce the carbon footprint of its inhabitants.<ref name=Dezeen>{{cite web|title=Dezeen|url=http://www.dezeen.com/2013/04/05/agora-garden-by-vincent-callebaut/|website=dezeen.com|accessdate=2 December 2014}}</ref>
Vincent Callebaut Architectures SARL replied to an invitation to tender in November 2010.  As of 2014 the project is under construction, and due for completion in 2016.<ref name=culturebox />

The shape of the building looks like a molecule of DNA with its double helix. This was designed to represent a symbol of life and dynamism.<ref name=FranceInter /> The surface area is about {{convert|50000|m²}}.

The aim of the project is to represent a perfect symbiosis between the human being and nature. Vincent Callebaut aims to develop an avant-gardist architecture and tries to institute a new life style in harmony with nature. The building is intended to guarantee environmental norms in order to obtain the Green Building Label delivered by the Home Affairs Ministry of Taipei.

The project addresses four ecologic objectives of the [[Copenhagen Accord]]:<ref name="Dezeen" />

* Diminution of the global warming.
* Protection of the biodiversity. 
* Protection of the environment and the quality of life. 
* Management of the natural resources and waste.

Agora Garden develops the [[Cradle to Cradle]] concept : "nothing is lost and everything transforms itself" ([[Antoine Lavoisier]]). All materials are recycled or recyclable in order to imitate the processes of natural ecosystems. For example, at the top there is a huge free access garden covered by photovoltaic panels which produce electricity for the building. The tower is surrounding by {{convert|20|m|adj=on}} trees which increase the biodiversity in the city.<ref name=Dezeen />

Agora Garden is composed of twenty levels in double helix formation, twisted at ninety degrees. The twist satisfies four major objectives. First of all, thanks to this architecture, the morphology of the building changes according to its orientation. On the East/West side we can see a rhomboidal pyramid, when one the North/South side we can see a reverse pyramid. Then, all flats have their own balconies looking like hanging garden. Thirdly, there is no vis-à-vis so inhabitants have their intimacy and a panoramic view on Taipei.<ref name=FranceInter />

The six main components of the project are :

*The forestair : the building is bordered by a forest which ensure the confidentiality of the inhabitants. The light is over present and the car parks, the swimmings pool and the fitness are naturally ventilated.
*The indoorr and the outdoor are well connected thanks to all the bay windows.
*The central score is composed of two staircases, four high speed elevators, a car elevator and two sky garages.
*The apartmentss measure {{convert|650|m2}}. It could seem vast for European people but in that country, three generations live together in the same flat.
*The landscapes balconies project is to build gardens which cover the entire building which bring back nature in the city. Inhabitants could easily cultivate fruits, vegetasbles and aromates so that they could be self-sufficient. Therefore, this gardens take part of the sustainability process. Indeed, there are compost spacess from waste to organic fertilizers, full cells and rainwater can be recycle thanks to reservoirs.
*Located at a {{convert|100|m}} high, a huge photovoltaic pergola of {{convert|1000|m²}} transforms solar rays into electric energy which is directly reintroduced sinto the network of the building.

===The Gate===

The Gate is under construction in Cairo (Egypt). It is a multi-use complex with housing, work spaces and facilities like sports. The roof top is a garden where you can swim or run.

==References==

{{reflist}}

{{DEFAULTSORT:Category}}
[[Category:Belgian architects]]
[[Category:Architecture]]
[[Category:Sustainable architecture]]
[[Category:1977 births]]
[[Category:Living people]]