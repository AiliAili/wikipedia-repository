{{Infobox journal
| title = Annual Review of Anthropology
| cover =
| editor = Donald Brenneis, Karen B. Strier
| discipline = Anthropology
| former_names = Biennial Review of Anthropology
| abbreviation = Annu. Rev. Anthropol.
| publisher = [[Annual Reviews]]
| country =
| frequency = Annual
| history = 1959-present
| impact = 2.246
| impact-year = 2013
| website = http://www.annualreviews.org/journal/anthro
| link1 = http://www.annualreviews.org/loi/anthro
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR = 00846570
| OCLC = 1783647
| LCCN = 72082136
| CODEN = ARAPCW
| ISSN = 0084-6570
| eISSN =
}}
The '''''Annual Review of Anthropology''''' is an annual [[peer-reviewed]] [[academic journal]] that was established in 1972. It covers significant developments on all aspects of [[anthropology]], including [[archaeology]], [[biological anthropology]], [[linguistics]] and communicative practices, [[area studies|regional studies]] and international anthropology, and [[sociocultural anthropology]]. The [[editors-in-chief]] are Donald Brenneis ([[University of California, Santa Cruz]]) and Karen B. Strier ([[University of Wisconsin-Madison]]). The journal was established as "Biennial Review of Anthropology" which was published by the [[Stanford University Press]] from 1959 to 1971 or 1972.

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.246, ranking it 8th out of 81 journals in the category "Anthropology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Anthropology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.annualreviews.org/journal/anthro}}

[[Category:Annual Reviews academic journals|Anthropology]]
[[Category:Anthropology journals]]
[[Category:Annual journals]]
[[Category:Publications established in 1959]]
[[Category:English-language journals]]



{{Anthropology-journal-stub}}