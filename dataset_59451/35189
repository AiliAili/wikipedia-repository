{{Distinguish2|the 1998 Pepsi 400 presented by DeVilbiss at Michigan International Speedway}}
{{Infobox NASCAR race report
| Type           =  NASWINSTON
| Race Name      = Pepsi 400
| Details ref    = <ref name="seasonschedule">{{cite web | url=http://www.nascar.com/races/cup/1998/data/schedule.html | title=1998 NASCAR Winston Cup Schedule | work=NASCAR.com | publisher=Turner Sports | accessdate=2012-06-06}}</ref><ref name="officialraceresults">{{cite web | url=http://www.nascar.com/races/cup/1998/30/data/results_official.html | title=1998 Official Race Results : Pepsi 400 | work=NASCAR.com | publisher=Turner Sports | accessdate=2012-06-06 | archiveurl=https://web.archive.org/web/20040410145512/http://www.nascar.com/races/cup/1998/30/data/results_official.html | archivedate=2004-04-10}}</ref>
| Fulldate       = {{Start date|1998|10|17}}
| Year           = 1998
| Race_No        = 30
| Season_No      = 33
| Image          = Pepsi 400 Logo.jpg
| image-size     = 150px
| Location       = [[Daytona International Speedway]] in [[Daytona Beach, Florida]]
| Course_mi      = 2.5
| Course_km      = 4.0
| Distance_laps  = 160
| Distance_mi    = 400
| Distance_km    = 643.738
| Weather        = Temperatures descending as low as {{convert|72|F|C}}; wind speeds approaching {{convert|13|mph|km/h}}<ref>{{cite web|url=http://www.almanac.com/weather/history/FL/Daytona%20Beach/1998-10-07 |title=Weather of the ''1998 Pepsi 400'' |publisher=The Old Farmers' Almanac |accessdate=2013-06-25|archiveurl=http://www.webcitation.org/6HntaF86B?url=http://m.almanac.com/weather/history/FL/Daytona%20Beach/1998-10-07|archivedate=2013-07-02|deadurl=no}}</ref>
| Avg            = {{convert|144.549|mi/h}}
| Pole_Driver    = [[Bobby Labonte]]
| Pole_Team      = [[Joe Gibbs Racing]]
| Pole_Time      = 46.485
| Most_Driver    = [[Jeff Gordon]]
| Most_Team      = [[Hendrick Motorsports]]
| Most_laps      = 49
| Car            = 24
| First_Driver   = Jeff Gordon
| First_Team     = Hendrick Motorsports
| Network        = [[The Nashville Network|TNN]]
| Announcers     = [[Eli Gold]], [[Dick Berggren]] and [[Buddy Baker]]
}}

The '''1998 [[Pepsi 400]]''' was a [[NASCAR]] [[Sprint Cup Series|Winston Cup Series]] [[stock car]] race held on October 17, 1998, at [[Daytona International Speedway]] in [[Daytona Beach, Florida]]. Originally scheduled to be held on the [[Fourth of July]], the race was postponed until the fall due to widespread [[wildfire]]s in central Florida; it was the first [[superspeedway]] race to be held at night, and the first time (and, as of 2015, the only time) there were two consecutive points-paying restrictor plate races, with the Winston 500 being run first on October 11.

Contested over 160&nbsp;laps, it was the thirtieth race of the [[1998 NASCAR Winston Cup Series|1998 season]]. [[Jeff Gordon]] of [[Hendrick Motorsports]] took his eleventh win of the season, while [[Bobby Labonte]] finished second and [[Mike Skinner (racing driver)|Mike Skinner]] finished third. Gordon retained his point lead on the way to his third [[Sprint Cup Series|Winston Cup]] championship title.

==Report==

===Background===
[[File:Daytona International Speedway.svg|thumb|200px|left|[[Daytona International Speedway]], where the race was held.]]
Daytona International Speedway is one of six  [[superspeedway]]s to hold [[NASCAR]] races, the others being [[Michigan International Speedway]], [[California Speedway]], [[Indianapolis Motor Speedway]], [[Pocono Raceway]] and [[Talladega Superspeedway]].<ref name="tacksnascar">{{cite web|title=NASCAR Race Tracks|url=http://www.nascar.com/races/tracks/|accessdate=28 June 2011|work=NASCAR.com| publisher=Turner Sports| archiveurl= https://web.archive.org/web/20110628180503/http://www.nascar.com/races/tracks/| archivedate= 28 June 2011 <!--DASHBot-->| deadurl= no}}</ref> The standard track at Daytona International Speedway is a four-turn superspeedway that is {{convert|2.5|mi|km}} long.<ref name="daytrack">{{cite web|title=NASCAR Tracks&nbsp;— The Daytona International Speedway|url=http://www.speedway-guide.com/Daytona-International-Speedway.html|work=Speedway Guide|accessdate=28 June 2011| archiveurl= https://web.archive.org/web/20110716114746/http://www.speedway-guide.com/Daytona-International-Speedway.html| archivedate= 16 July 2011 <!--DASHBot-->| deadurl= no}}</ref> The track's turns are banked at 31 [[Degree (angle)|degrees]], while the front stretch, the location of the finish line, is banked at 18 degrees.<ref name=daytrack /> [[John Andretti]] was the defending race winner.<ref name="2010 race">{{cite web|url=http://www.racing-reference.info/race/1997_Pepsi_400/W |title=1997 Pepsi 400|work=Racing-Reference|publisher=USA Today Sports Media Group |accessdate=2012-06-06}}</ref> The event was the fifth of five night races held during the 1998 Winston Cup Series season.<ref>{{cite news|title=Pepsi 400 at Daytona is finally here|date=October 15, 1998|work=[[Williamson Daily News]]|page=8|location=Williamson, WV}}</ref>

The 1998 Pepsi 400 was originally scheduled to be run on July 4, 1998, as the seventeenth race of the 33-event Winston Cup Series schedule.<ref>{{cite news|title=Season has raised some hot points of its own|last=Willis|first=Ken|date=October 15, 1998|work=[[The Daytona Beach News-Journal]]|page=1B|location=Daytona Beach, FL}}</ref> It was the first [[superspeedway]] race, and first NASCAR event held at Daytona, to be run at night following the installation of [[Musco]] lighting at the Daytona International Speedway;<ref>{{cite news|title=Postponement of race better late than never|last=Willis|first=Ken|date=July 3, 1998|work=The Daytona Beach News-Journal|page=1B|location=Daytona Beach, FL}}</ref> it was also scheduled to be broadcast live on [[CBS]], the first stock car event to be televised live on [[primetime television|primetime]] [[network television]].<ref>{{cite web|url=http://articles.baltimoresun.com/1997-11-20/sports/1997324137_1_cbs-prime-daytona-500|title=CBS's plans for 1998 Pepsi 400 mark a first for auto coverage|last=Kent|first=Milton|date=November 20, 1997|work=[[The Baltimore Sun]]|accessdate=2012-06-06|location=Baltimore, MD}}</ref>

During the days leading up to the scheduled start of practice at 3pm, Thursday, July 2, 1998, concerns rose on account of the [[1998 Florida wildfires|massive wildfire outbreak]] that was underway in central Florida; thousands of people were forced to evacuate the area, and [[Interstate 95]], the primary north–south thoroughfare through the region, was closed. At 10am on July 2, NASCAR announced that the race was being postponed; while July 25 was an open date, the decision was made to reschedule the race for October 17, to allow additional time for the wildfires to be controlled.<ref name="Lub">{{cite web|url=http://lubbockonline.com/stories/070398/0606980002.shtml|title=Wildfires postpone Pepsi 400|date=July 3, 1998|work=[[Lubbock Avalanche-Journal]]|accessdate=2012-06-06|location=Lubbock, TX}}</ref> At the time of the postponement, the race was sold out; this was the first time the summer race at [[Daytona International Speedway]] had achieved sellout status.<ref>{{cite web|url=http://articles.sun-sentinel.com/1998-10-11/news/9810100269_1_race-fans-speedway-nascar-winston-cup|title=Wait For Pepsi 400 Under Lights Is Over|last=Macur|first=Juliet|date=October 11, 1998|work=[[South Florida Sun-Sentinel]]|accessdate=2012-06-06|location=Fort Lauderdale, FL}}</ref>

The rescheduling meant that the race would not be televised on CBS, as the network did not want to compete against [[Fox Broadcasting Company|Fox]]'s broadcast of Game 1 of the [[Major League Baseball]] [[1998 World Series|World Series]], also scheduled for October 17.<ref name="Lub"/>  On July 21, it was announced that [[The Nashville Network]], a [[cable television|cable]] affiliate of CBS, would air the rescheduled race live in its entirety.<ref>{{cite news|title=CBS affiliate TNN will air Pepsi 400|date=July 22, 1998|work=The Daytona Beach News-Journal|page=1B|location=Daytona Beach, FL}}</ref>

Prior to the race, [[Jeff Gordon]] led the [[List of NASCAR Sprint Cup Series champions|Drivers' Championship]] with 4632 points, and [[Mark Martin]] was in second with 4344 points. [[Dale Jarrett]] was third in the Drivers' Championship with 4098 points, [[Rusty Wallace]] was fourth with 3883 points, and [[Jeff Burton]] was in fifth with 3805 points.<ref name="driver">{{cite web|title=Driver's Championship Classification|url=http://www.nascar.com/races/cup/2011/16/data/standings_official.html|work=NASCAR.com|publisher=Turner Sports|accessdate=2012-06-06}}</ref> In the [[NASCAR Manufacturers' Championship|Manufacturers' Championship]], [[Chevrolet]] was tied with [[Ford Motor Company|Ford]] for the lead with 216 points each; [[Pontiac]] followed in third with 138 points.<ref name=manufacturers>{{cite web|title=NASCAR Manufacturers' Championship|url=http://www.jayski.com/stats/1998/manu98.htm|work=Jayski's Silly Season Site|publisher=ESPN Internet Ventures|accessdate=January 19, 2015}}</ref>

===Practice and qualifying===
Practice and first round qualifying was held on Thursday, October 15, 1998; [[Bobby Labonte]] led pre-qualifying practice with a lap time of 46.722 seconds.<ref name="Quals">{{cite web|url=http://articles.sun-sentinel.com/1998-10-16/news/9810160059_1_bobby-labonte-fastest-practice-lap-dale-jarrett|title=Bobby Labonte on Pepsi Pole|last=Brioso|first=Cesar|date=October 16, 1999|work=South Florida Sun-Sentinel|accessdate=2012-06-06|location=Fort Lauderdale, FL}}</ref> Five [[Ford Motor Company|Ford]] teams, those of drivers [[Chad Little]], [[Jimmy Spencer]], [[Rich Bickle]], [[Dick Trickle]] and [[Billy Standridge]], ran [[Ford Thunderbird (tenth generation)|Thunderbird]]-bodied race cars, instead of the standard [[Ford Taurus (third generation)|Taurus]] ran at most 1998 Winston Cup Series races, believing the Thunderbird to have an aerodynamic advantage at the [[restrictor plate]] racetracks.<ref name="Jay1">{{cite web|url=http://www.jayski.com/past/1998/981012.htm|title=Jayski's Silly Season Site: Past News October 12-18, 1998|last=Adamczyk|first=Jay|date=October 18, 1998|work=Jayski's Silly Season Site|publisher=ESPN|accessdate=2012-06-06}}</ref> [[Randal Ritter]]'s car failed to pass inspection due to extreme irregularities in its construction, and the team withdrew before practice began.<ref name="Jay1"/>

Bobby Labonte posted the fastest time in first round qualifying, a lap of 46.485 seconds ({{convert|193.611|mi/h}}), winning the [[Bud Pole Award]]; Jeff Burton was second fastest. [[Jeremy Mayfield]] qualified 25th, the last car to qualify for the race on the first day of time trials.<ref name="Quals"/> Second round qualifying was held during the afternoon on Friday, October 16; the fastest car in the session, placing 26th on the starting grid, was the No. 07 Chevrolet driven by [[Dan Pardus]], qualifying at a speed of {{convert|189.945|mi/h}}. [[Kenny Wallace]] and [[Rich Bickle]] also improved their qualifying times and made the starting field for the race; Bickle was the slowest car to qualify on time, at {{convert|188.608|mi/h}}. [[Bobby Hamilton]], [[Jimmy Spencer]], [[Ricky Craven]], [[Johnny Benson, Jr.]], [[Ricky Rudd]], [[Kyle Petty]] and [[Darrell Waltrip]] received provisional starting positions;<ref name="NYT">{{cite news|url=https://select.nytimes.com/gst/abstract.html?res=F30615F7355E0C748DDDA90994D0494D81|title=Pardus, at Last, Makes Winston Cup Debut|date=October 17, 1998|work=[[The New York Times]]|location=New York|accessdate=2012-06-06}}</ref> [[Rick Mast]], [[Dick Trickle]], [[Rick Wilson (racing driver)|Rick Wilson]], [[Robert Pressley]] and [[Gary Bradberry]] failed to make the field for the event.<ref name="RRR">{{cite web|url=http://www.racing-reference.info/race/1998_Pepsi_400/W|title=1998 Pepsi 400|work=Racing-Reference|publisher=USA Today Sports Media Group|accessdate=2012-06-06}}</ref>

[[Ernie Irvan]], 14th in Winston Cup Series points entering the event, opted to sit out the majority of practice and qualifying, having suffered injuries in a crash the previous week at [[Talladega Superspeedway]]; Ricky Craven practiced and qualified the No. 36 Pontiac for Irvan.<ref>{{cite news|title=Hurting Irvan sits out 400 qualifying|date=October 16, 1998|work=[[Fort Worth Star-Telegram]]|pages=7 Sports|location=Fort Worth, TX}}</ref> Irvan did run a few laps during practice on Friday night, October 16; the "Happy Hour" practice session began at 7:30pm and was scheduled to run for two and a half hours, but was delayed during its duration for an hour and 45 minutes, as the track was dried following afternoon thunderstorms.<ref>{{cite web|url=http://www.n-jcenter.com/1998/Oct/17/RAC5.htm |title=Rough drafts can spark wrecks |last=Hornack |first=Ken |date=October 17, 1998 |work=The Daytona Beach News-Journal |accessdate=2012-06-06 |location=Daytona Beach, FL |deadurl=yes |archiveurl=https://web.archive.org/web/19991001073424/http://www.n-jcenter.com/1998/Oct/17/RAC5.htm |archivedate=October 1, 1999 }}</ref> [[Bobby Hamilton]] posted the fastest speed in the session, {{convert|191.345|mi/h}}. Johnny Benson's car suffered a [[hood (vehicle)|hood]] failure during the session, the hood flying off of the car.<ref name="Jay1"/>

===Race===
[[File:1998Pepsi400Daytona.jpg|thumb|right|The 1998 Pepsi 400 was the first race held at [[Daytona International Speedway]] under the lights.]]
The race was held starting at 8pm on Saturday, October 17, 1998. Ernie Irvan dropped to the rear of the field prior to the start of the race, due to a driver change; Ricky Craven had qualified the car. Although Bobby Labonte started on pole, [[Dale Jarrett]], who had started third, led the first lap of the race; [[Dale Earnhardt]] took the lead on lap two and held it through the first [[Racing flags#Yellow flag|caution period]] of the race, for rain, for three laps starting at lap 13. [[The Big One (NASCAR)|A large crash]] occurred on lap 32; initiated by [[Kevin Lepage]] losing control of his car in turn two, the wreck collected eleven cars, including [[Hut Stricklin]], [[Geoff Bodine]] and three-time Winston Cup champion [[Darrell Waltrip]]; Earnhardt lost the lead during [[pit stop]]s under caution to [[Rusty Wallace]], with [[Dave Marcis]] pitting a lap later than the leaders to pick up five bonus points for leading a lap. Lepage suffered a fractured shoulder in a fall exiting his car.<ref name="RRR"/><ref name="AP">{{cite web|url=http://www.apnewsarchive.com/1998/Gordon-Wins-Pepsi-400/id-eee4b33dc61dc837cda156bd164ac1c6|title=Gordon wins Pepsi 400|date=October 17, 1998|publisher=Associated Press|accessdate=2012-06-06}}</ref>

The race resumed on lap 39; after Earnhardt took the lead for one lap at lap 44 Jarrett assumed the lead and held it for the next 37 laps, until pit stops shuffled the field and saw Earnhardt, [[Jeff Gordon]], and [[Bobby Labonte]] swapping the lead among themselves. [[Jimmy Spencer]] and [[Jeremy Mayfield]] each also led a single lap during the long green-flag period that ended on lap 107 when [[Dan Pardus]] hit the wall in turn two. Gordon had been leading at the time of the caution; he lost the lead to rookie [[Kenny Irwin, Jr.]] during pit stops, and Irwin retained the lead after the resumption of racing on lap 110, leading 15 laps in total until being passed by Gordon on lap 123 on account of having a flat tire, just before the caution came out once again for a rain shower.<ref name="RRR"/><ref name="AP"/>

The brief four-lap caution saw Gordon retaining the lead; he continued to pace the field as Irwin, on lap 141, hit the Thunderbird of [[Chad Little]], setting off a nine-car accident that brought out a caution. Gordon continued to lead on the restart, and on lap 155, five laps from the finish of the event, rain began to fall once again; the caution flag was thrown, then the [[Racing flags#Red flag|red flag]], stopping the race to allow the track to be dried and the event to finish under green.<ref name="RRR"/><ref name="AP"/>

After a 37-minute red flag period, the race resumed; Gordon was able to hold off his challengers over the final three laps to take his eleventh win of the season.<ref name="RRR"/><ref name="AP"/> In the midst of the final sprint, [[Chad Little]] ended up getting turned from behind by [[Jimmy Spencer]] while in a three wide battle exiting turn 4 and crashed hard into the inside wall, failing to make it to the stripe and finishing 20th. Gordon set an average speed of {{convert|144.549|mi/h}} while leading 49 of the race's 160 laps. Gordon received $184,325 in [[purse money]].<ref name="AP"/> [[Bobby Labonte]] finished second; [[Mike Skinner (racing driver)|Mike Skinner]] was third, while [[Jeremy Mayfield]] and [[Rusty Wallace]] finished in the top five.<ref name="RRR"/> The fastest lap of the race was set by [[Dale Earnhardt]], at {{convert|191.383|mi/h}}.<ref name="PPG">{{cite book|title=The Official NASCAR Preview and Press Guide '98|year=1999|publisher=UMI Publications|location=Charlotte, NC|page=213|asin=B0041UX75U}}</ref>

Gordon, who had entered the race with the points lead, extended it to 358 points over [[Mark Martin]];<ref name="AP"/> nobody would challenge Gordon over the remainder of the season, and he would go on to win his third Winston Cup Series championship; he was the youngest driver to accomplish the feat.<ref>{{cite news|url=https://news.google.com/newspapers?id=5mJDAAAAIBAJ&sjid=960MAAAAIBAJ&pg=1180,689748&dq=gordon+wins+1998+championship&hl=en|title=Can Jeff Gordon Top His Phenomenal 1998 Season|date=January 29, 1999|work=The Newberry Observer|page=7|accessdate=2012-07-03|location=Newberry, SC}}</ref> The race took two hours, forty-six minutes, and two seconds to complete, and the margin of victory was 0.176 seconds.<ref name="officialraceresults"/>

=== Statistical notes ===
The 1998 Pepsi 400 was the only Winston Cup Series start by [[Dan Pardus]],<ref name="NYT"/> and the last by [[Billy Standridge]].<ref>{{cite web|url=http://www.racing-reference.info/rquery?id=standbi01&trk=t0&series=W|title=Billy Standridge - NASCAR Sprint Cup Results|work=Racing-Reference|publisher=USA Today Sports Media Group|accessdate=2012-06-06}}</ref> It was also the last race for which [[Rick Wilson (racing driver)|Rick Wilson]] attempted to qualify.<ref>{{cite news|title=Wilson back in driver's seat|last=Kelly|first=Godwin|date=October 6, 1998|work=The Daytona Beach News-Journal|page=2B|location=Daytona Beach, FL}}</ref>

The Pepsi 400 would return to its traditional July date in 1999, and has been held at night every year since the 1998 event, except in 2014 when it was postponed to Sunday due to rain.<ref>{{cite web|url=http://articles.sun-sentinel.com/2003-07-04/news/0307030639_1_lowest-rated-night-nascar-races-winston-cup|title=Under The Bright Lights|last=Diaz|first=George|date=July 4, 2003|work=South Florida Sun-Sentinel|accessdate=2012-07-03|location=Fort Lauderdale, FL}}</ref>

==Results==

===Qualifying===
{| class="wikitable"
|-
! scope="col" | {{Tooltip|No.|Car number}}
! scope="col" | Driver
! scope="col" | Team
! scope="col" | Manufacturer
! scope="col" | {{Tooltip|Time|Time in seconds}}
! scope="col" | {{Tooltip|Speed|Speed in MPH}}
! scope="col" | {{Tooltip|Grid|Starting position}}
|-
|  scope="row" | 18 ||[[Bobby Labonte]] ||[[Joe Gibbs Racing]] ||[[Pontiac]] ||46.485 ||193.611 ||1
|-
|  scope="row" | 99 ||[[Jeff Burton]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||46.710 ||192.678 ||2
|-
|  scope="row" | 88 ||[[Dale Jarrett]] ||[[Robert Yates Racing]] ||[[Ford Motor Company|Ford]] ||46.873 ||192.008 ||3
|-
|  scope="row" | 5 ||[[Terry Labonte]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||46.884 ||191.963 ||4
|-
|  scope="row" | 3 ||[[Dale Earnhardt]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||46.928 ||191.783||5
|-
|  scope="row" | 6 ||[[Mark Martin]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||46.957 ||191.665||6
|-
|  scope="row" | 2 ||[[Rusty Wallace]] ||[[Penske Racing South]] ||[[Ford Motor Company|Ford]] ||46.990 ||191.530 ||7
|-
|  scope="row" | 24 ||[[Jeff Gordon]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||47.003 ||191.477 ||8
|-
|  scope="row" | 31 ||[[Mike Skinner (racing driver)|Mike Skinner]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||47.008 ||191.457 ||9
|-
|  scope="row" | 55 ||[[Hut Stricklin]] ||[[Andy Petree Racing]] ||[[Chevrolet]] ||47.012 ||191.440 ||10
|-
|  scope="row" | 28 ||[[Kenny Irwin, Jr.]] # ||[[Robert Yates Racing]] ||[[Ford Motor Company|Ford]] ||47.072 ||191.196 ||11
|-
|  scope="row" | 22 ||[[Ward Burton]] ||[[Bill Davis Racing]] ||[[Pontiac]] ||47.096 ||191.099 ||12
|-
|  scope="row" | 30 ||[[Derrike Cope]] ||[[Bahari Racing]] ||[[Pontiac]] ||47.097 ||191.095 ||13
|-
|  scope="row" | 43 ||[[John Andretti]] ||[[Petty Enterprises]] ||[[Pontiac]] ||47.143 ||190.909 ||14
|-
|  scope="row" | 1 ||[[Steve Park]] # ||[[Dale Earnhardt, Inc.]] ||[[Chevrolet]] ||47.146 ||190.896 ||15
|-
|  scope="row" | 33 ||[[Ken Schrader]] ||[[Andy Petree Racing]] ||[[Chevrolet]] ||47.148 ||190.888 ||16
|-
|  scope="row" | 40 ||[[Sterling Marlin]] ||[[Team Sabco]] ||[[Pontiac]] ||47.170 ||190.799 ||17
|-
|  scope="row" | 47 ||[[Billy Standridge]] ||[[Standridge Motorsports]] ||[[Ford Motor Company|T-Bird]] ||47.228 ||190.565 ||18
|-
|  scope="row" | 11 ||[[Brett Bodine]] ||[[Brett Bodine Racing]] ||[[Ford Motor Company|Ford]] ||47.268 ||190.404 ||19
|-
|  scope="row" | 97 ||[[Chad Little]] ||[[Roush Racing]] ||[[Ford Motor Company|T-Bird]] ||47.300 ||190.275 ||20
|-
|  scope="row" | 7 ||[[Geoff Bodine]] ||[[Geoff Bodine Racing]] ||[[Ford Motor Company|Ford]] ||47.331 ||190.150 ||21
|-
|  scope="row" | 16 ||[[Kevin Lepage]] # ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||47.344 ||190.098 ||22
|-
|  scope="row" | 91 ||[[Andy Hillenburg]] ||[[LJ Racing]] ||[[Chevrolet]] ||47.374 ||189.978 ||23
|-
|  scope="row" | 96 ||[[Steve Grissom]] ||[[American Equipment Racing]] ||[[Chevrolet]] ||47.374 ||189.978 ||24
|-
|  scope="row" | 12 ||[[Jeremy Mayfield]] ||[[Penske-Kranefuss Racing]] ||[[Ford Motor Company|Ford]] ||47.447 ||189.685 ||25
|-
|  scope="row" | 07 ||[[Dan Pardus]] ||[[Midwest Transit Racing]] ||[[Chevrolet]] ||47.382 ||189.946 ||26‡
|-
|  scope="row" | 42 ||[[Joe Nemechek]] ||[[Team Sabco]] ||[[Chevrolet]] ||47.455 ||189.653 ||27
|-
|  scope="row" | 81 ||[[Kenny Wallace]] ||[[FILMAR Racing]] ||[[Ford Motor Company|Ford]] ||47.472 ||189.585 ||28
|-
|  scope="row" | 21 ||[[Michael Waltrip]] ||[[Wood Brothers Racing]] ||[[Ford Motor Company|Ford]] ||47.488 ||189.522 ||29
|-
|  scope="row" | 71 ||[[Dave Marcis]] ||[[Marcis Auto Racing]] ||[[Chevrolet]] ||47.532 ||189.346 ||30
|-
|  scope="row" | 94 ||[[Bill Elliott]] ||[[Bill Elliott Racing]] ||[[Ford Motor Company|Ford]] ||47.538 ||189.322 ||31
|-
|  scope="row" | 46 ||[[Jeff Green (racing driver)|Jeff Green]] ||[[Team Sabco]] ||[[Chevrolet]] ||47.565 ||189.215 ||32
|-
|  scope="row" | 9 ||[[Jerry Nadeau]] # ||[[Melling Racing]] ||[[Ford Motor Company|Ford]] ||47.568 ||189.203 ||33
|-
|  scope="row" | 13 ||[[Ted Musgrave]] ||[[Elliott-Marino Racing]] ||[[Ford Motor Company|Ford]] ||47.649 ||188.881 ||34
|-
|  scope="row" | 50 ||[[Wally Dallenbach, Jr.]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||47.692 ||188.711 ||35
|-
|  scope="row" | 98 ||[[Rich Bickle]] ||[[Cale Yarborough Racing]] ||[[Ford Motor Company|T-Bird]] ||47.718 ||188.608 ||36
|-
|  scope="row" | 4 ||[[Bobby Hamilton]] ||[[Morgan-McClure Motorsports]] ||[[Chevrolet]] ||colspan=2|<center>Provisional</center> ||37
|-
|  scope="row" | 23 ||[[Jimmy Spencer]] ||[[Travis Carter Enterprises]] ||[[Ford Motor Company|T-Bird]] ||colspan=2|<center>Provisional</center> ||38
|-
|  scope="row" | 36 ||[[Ricky Craven]] † ||[[MB2 Motorsports]] ||[[Pontiac]] ||colspan=2|<center>Provisional</center> ||39
|-
|  scope="row" | 26 ||[[Johnny Benson, Jr.]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||colspan=2|<center>Provisional</center> ||40
|-
|  scope="row" | 10 ||[[Ricky Rudd]] ||[[Rudd Performance Motorsports]] ||[[Ford Motor Company|Ford]] ||colspan=2|<center>Provisional</center> ||41
|-
|  scope="row" | 44 ||[[Kyle Petty]] ||[[Petty Enterprises|PE2]] ||[[Pontiac]] ||colspan=2|<center>Provisional</center> ||42
|-
|  scope="row" | 35 ||[[Darrell Waltrip]] ||[[Tyler Jet Motorsports]] ||[[Pontiac]] ||colspan=2|<center>Past Champion</center> ||43
|-
|colspan="7"| <center>'''Failed to Qualify'''</center>
|-
|  scope="row" | 90 ||[[Dick Trickle]] ||[[Donlavey Racing]] ||[[Ford Motor Company|T-Bird]] ||48.092 ||187.141 ||
|-
|  scope="row" | 75 ||[[Rick Mast]] ||[[RahMoc Enterprises]] ||[[Ford Motor Company|Ford]] ||48.093 ||187.137 ||
|-
|  scope="row" | 41 ||[[Rick Wilson (racing driver)|Rick Wilson]] ||[[Larry Hedrick Motorsports]] ||[[Chevrolet]] ||48.441 ||185.793 ||
|-
|  scope="row" | 77 ||[[Robert Pressley]] ||[[Jasper Motorsports]] ||[[Ford Motor Company|Ford]] ||48.806 ||184.404 ||
|-
|  scope="row" | 78 ||[[Gary Bradberry]] ||[[Triad Motorsports]] ||[[Ford Motor Company|Ford]] ||48.864 ||184.185 ||
|-
|  scope="row" | 68 ||[[Randal Ritter]] ||Ritter Racing ||[[Chevrolet]] ||colspan=2|<center>Withdrew</center> ||
|-class="sortbottom"
|colspan="8"|{{center|{{small|# [[NASCAR Rookie of the Year|Rookie of the Year candidate]] / † Driver change following qualifying / ‡ Fastest second round qualifier<br>Source:<ref name="Jay1"/><ref>{{cite web|url=http://www.motorsport.com/nascar-cup/news/daytona-pepsi-starting-grid/|title=Daytona Pepsi Starting Grid|date=October 17, 1998|work=Motorsport.com|accessdate=2012-06-06}}</ref>}}}}
|}

===Race results===
{| class="wikitable" border="1"
|-
! scope="col" | {{Tooltip|Pos|Finishing position}}
! scope="col" | {{Tooltip|Grid|Starting position}}
! scope="col" | {{Tooltip|No.|Car number}}
! scope="col" | Driver
! scope="col" | Team
! scope="col" | Manufacturer
! scope="col" | {{Tooltip|Laps|Laps completed}}
! scope="col" | Points
|-
| scope="row" | 1||8 ||24 ||[[Jeff Gordon]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||160 ||185{{smallsup|2}}
|-
| scope="row" | 2||1 ||18 ||[[Bobby Labonte]] ||[[Joe Gibbs Racing]] ||[[Pontiac]] ||160 ||175{{smallsup|1}}
|-
| scope="row" | 3||9 ||31 ||[[Mike Skinner (racing driver)|Mike Skinner]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||160 ||165
|-
| scope="row" | 4||25 ||12 ||[[Jeremy Mayfield]] ||[[Penske-Kranefuss Racing]] ||[[Ford Motor Company|Ford]] ||160 ||165{{smallsup|1}}
|-
| scope="row" | 5||7 ||2 ||[[Rusty Wallace]] ||[[Penske Racing South]] ||[[Ford Motor Company|Ford]] ||160 ||160{{smallsup|1}}
|-
| scope="row" | 6||4 ||5 ||[[Terry Labonte]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||160 ||150
|-
| scope="row" | 7||12 ||22 ||[[Ward Burton]] ||[[Bill Davis Racing]] ||[[Pontiac]] ||160 ||146
|-
| scope="row" | 8||39† ||36 ||[[Ernie Irvan]] ||[[MB2 Motorsports]] ||[[Pontiac]] ||160 ||142
|-
| scope="row" | 9||16 ||33 ||[[Ken Schrader]] ||[[Andy Petree Racing]] ||[[Chevrolet]] ||160 ||138
|-
| scope="row" | 10||5 ||3 ||[[Dale Earnhardt]] ||[[Richard Childress Racing]] ||[[Chevrolet]] ||160 ||139{{smallsup|1}}
|-
| scope="row" | 11||37 ||4 ||[[Bobby Hamilton]] ||[[Morgan-McClure Motorsports]] ||[[Chevrolet]] ||160 ||130
|-
| scope="row" | 12||38 ||23 ||[[Jimmy Spencer]] ||[[Travis Carter Enterprises]] ||[[Ford Motor Company|T-Bird]] ||160 ||132{{smallsup|1}}
|-
| scope="row" | 13||2 ||99 ||[[Jeff Burton]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||160 ||124
|-
| scope="row" | 14||14 ||43 ||[[John Andretti]] ||[[Petty Enterprises]] ||[[Pontiac]] ||160 ||121
|-
| scope="row" | 15||31 ||94 ||[[Bill Elliott]] ||[[Bill Elliott Racing]] ||[[Ford Motor Company|Ford]] ||160 ||118
|-
| scope="row" | 16||6 ||6 ||[[Mark Martin]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||160 ||115
|-
| scope="row" | 17||27 ||42 ||[[Joe Nemechek]] ||[[Team Sabco]] ||[[Chevrolet]] ||160 ||112
|-
| scope="row" | 18||17 ||40 ||[[Sterling Marlin]] ||[[Team Sabco]] ||[[Chevrolet]] ||160 ||109
|-
| scope="row" | 19||33 ||9 ||[[Jerry Nadeau]] # ||[[Melling Racing]] ||[[Ford Motor Company|Ford]] ||160 ||106
|-
| scope="row" | 20||20 ||97 ||[[Chad Little]] ||[[Roush Racing]] ||[[Ford Motor Company|T-Bird]] ||159 ||103
|-
| scope="row" | 21||30 ||71 ||[[Dave Marcis]] ||[[Marcis Auto Racing]] ||[[Chevrolet]] ||159 ||105{{smallsup|1}}
|-
| scope="row" | 22||42 ||44 ||[[Kyle Petty]] ||[[Petty Enterprises|PE2]] ||[[Pontiac]] ||159 ||97
|-
| scope="row" | 23||3 ||88 ||[[Dale Jarrett]] ||[[Robert Yates Racing]] ||[[Ford Motor Company|Ford]] ||159 ||99{{smallsup|1}}
|-
| scope="row" | 24||23 ||91 ||[[Andy Hillenburg]] ||[[LJ Racing]] ||[[Chevrolet]] ||159 ||91
|-
| scope="row" | 25||19 ||11 ||[[Brett Bodine]] ||[[Brett Bodine Racing]] ||[[Ford Motor Company|Ford]] ||159 ||88
|-
| scope="row" | 26||40 ||26 ||[[Johnny Benson, Jr.]] ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||159 ||85
|-
| scope="row" | 27||41 ||10 ||[[Ricky Rudd]] ||[[Rudd Performance Motorsports]] ||[[Ford Motor Company|Ford]] ||159 ||82
|-
| scope="row" | 28||43 ||35 ||[[Darrell Waltrip]] ||[[Tyler Jet Motorsports]] ||[[Pontiac]] ||158 ||79
|-
| scope="row" | 29||24 ||96 ||[[Steve Grissom]] ||[[American Equipment Racing]] ||[[Chevrolet]] ||157 ||76
|-
| scope="row" | 30||35 ||50 ||[[Wally Dallenbach, Jr.]] ||[[Hendrick Motorsports]] ||[[Chevrolet]] ||156 ||73
|-
| scope="row" | 31||29 ||21 ||[[Michael Waltrip]] ||[[Wood Brothers Racing]] ||[[Ford Motor Company|Ford]] ||144 ||70
|-
| scope="row" | 32||11 ||28 ||[[Kenny Irwin, Jr.]] # ||[[Robert Yates Racing]] ||[[Ford Motor Company|Ford]] ||140 ||72{{smallsup|1}}
|-
| scope="row" | 33||15 ||1 ||[[Steve Park]] # ||[[Dale Earnhardt, Inc.]] ||[[Chevrolet]] ||139 ||64
|-
| scope="row" | 34||34 ||13 ||[[Ted Musgrave]] ||[[Elliott-Marino Racing]] ||[[Ford Motor Company|Ford]] ||116 ||61
|-
| scope="row" | 35||28 ||81 ||[[Kenny Wallace]] ||[[FILMAR Racing]] ||[[Ford Motor Company|Ford]] ||113 ||58
|-
| scope="row" | 36||26 ||07 ||[[Dan Pardus]] ||[[Midwest Transit Racing]] ||[[Chevrolet]] ||99 ||55
|-
| scope="row" | 37||32 ||46 ||[[Jeff Green (racing driver)|Jeff Green]] ||[[Team Sabco]] ||[[Chevrolet]] ||98 ||52
|-
| scope="row" | 38||13 ||30 ||[[Derrike Cope]] ||[[Bahari Racing]] ||[[Pontiac]] ||67 ||49
|-
| scope="row" | 39||36 ||98 ||[[Rich Bickle]] ||[[Cale Yarborough Racing]] ||[[Ford Motor Company|T-Bird]] ||58 ||46
|-
| scope="row" | 40||22 ||16 ||[[Kevin Lepage]] # ||[[Roush Racing]] ||[[Ford Motor Company|Ford]] ||31 ||43
|-
| scope="row" | 41||21 ||7 ||[[Geoff Bodine]] ||[[Geoff Bodine Racing]] ||[[Ford Motor Company|Ford]] ||31 ||40
|-
| scope="row" | 42||10 ||55 ||[[Hut Stricklin]] ||[[Andy Petree Racing]] ||[[Chevrolet]] ||31 ||37
|-
| scope="row" | 43||18 ||47 ||[[Billy Standridge]] ||[[Standridge Motorsports]] ||[[Ford Motor Company|T-Bird]] ||19 ||34
|-class="sortbottom"
|colspan="9"|{{center|{{small|# [[NASCAR Rookie of the Year|Rookie of the Year candidate]] / † Driver change following qualifying<br>Source:<ref name="officialraceresults" />}}}}
|- class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|1}} Includes five bonus points for leading a lap}}}}
|- class="sortbottom"
|colspan="9"|{{center|{{small|{{smallsup|2}} Includes ten bonus points for leading the most laps}}}}
|}

== References ==
{{reflist|2}}

{{NASCAR next race|
|  Series        = Winston Cup Series
|  Season        = 1998 
|  Previous_race = [[1998 Winston 500]] 
|  Next_race     = [[1998 Dura Lube/Kmart 500]] 
}}
{{Portal bar|1990s|Motorsport|Florida|United States}}

{{NASCAR on TNN}}

{{good article}}

[[Category:1998 in Florida|Pepsi 400]]
[[Category:1998 NASCAR Winston Cup Series|Pepsi 400]]
[[Category:NASCAR races at Daytona International Speedway]]