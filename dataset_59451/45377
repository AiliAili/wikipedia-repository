<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Sir Victor Emmanuel Groom
| image         =
| caption       =
| birth_date    = {{Birth date|df=yes|1898|08|04}} 
| death_date    = {{Death date and age|1990|12|06|1898|08|04|df=yes}}
| birth_place   = [[Peckham]], [[London]], England
| death_place   = [[Surrey]], England
| placeofburial_label =
| placeofburial =
| nickname      =
| allegiance    = United Kingdom
| branch        = [[British Army]]<br/>[[Royal Air Force]]
| serviceyears  = 1916–1955
| rank          = [[Air Marshal]]
| unit          = [[Artists Rifles]], [[London Regiment]]<br/>[[West Yorkshire Regiment]]<br/>[[No. 20 Squadron RAF|No. 20 Squadron RFC/RAF]]<br/>[[No. 111 Squadron RAF]]<br/>[[No. 14 Squadron RAF]]<br/>[[No. 55 Squadron RAF]]
| commands      = [[No. 28 Squadron RAF]]<br/>[[No. 58 Squadron RAF]]<br/>[[RAF Marham]]<br/>[[No. 205 Group RAF]]<br/>[[Near East Air Force (Royal Air Force)|Middle East Air Force]]<br/>[[RAF Technical Training Command|Technical Training Command]]
| battles       = [[First World War]]<br/>[[Second World War]]
| awards        = [[Knight Commander of the Royal Victorian Order]]<br/>[[Knight Commander of the Order of the British Empire]]<br/>[[Companion of the Order of the Bath]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & [[Medal bar|Bar]]<br/>[[Mentioned in Despatches]]
| relations     =
| laterwork     =
}}
[[Air Marshal]] '''Sir Victor Emmanuel Groom''', {{postnominals|country=GBR|size=100%|sep=,|KCVO|KBE|CB|DFC1}} (4 August 1898 – 6 December 1990) was a senior officer in the British [[Royal Air Force]] and a [[flying ace]] of the [[First World War]] credited with [[List of World War I aces credited with 8 victories|eight aerial victories]].<ref name=drome>{{cite web |url=http://www.theaerodrome.com/aces/england/groom.php |title=Victor Groom |website=The Aerodrome |accessdate=14 March 2010}}</ref> He rose to become a consequential participant in air operations to support [[Operation Overlord]], the invasion of France during the Second World War.<ref name=air>{{cite web |url=http://www.rafweb.org/Biographies/Groom.htm |title=Air Marshal Sir Victor Groom |first=M. B. |last=Barrass |work=Air of Authority – A History of RAF Organisation |accessdate=14 March 2010}}</ref>

==World War I==
Groom was educated at [[Alleyn's School]], Dulwich. He enlisted into the [[Artists Rifles]], [[London Regiment]], as a private in 1916<ref name=air/> and was commissioned as a second lieutenant in the [[West Yorkshire Regiment]] on 26 April 1917<ref>{{London Gazette |issue=30100 |supp=yes |startpage=5307 |date=29 May 1917}}</ref> before being attached to the [[Royal Flying Corps]] in September.<ref name=drome/> He was appointed a [[flying officer]] on 30 January 1918<ref name=air/> and placed on the General List of the Royal Flying Corps.<ref>{{London Gazette |issue=30546 |supp=yes |startpage=2574 |date=27 February 1918 |nolink=yes}}</ref> On 18 March, he was assigned to [[No. 20 Squadron RAF|No. 20 Squadron]] as a [[Bristol F.2 Fighter]] pilot.<ref name=drome/><ref name=air/>

On his first combat flight, his formation leader fired a [[Very flare]] that landed in the rear cockpit of Groom's Bristol. While the observer burned his hands smothering the fire that threatened to set off munitions, Groom safely landed.<ref name=air/> Having survived friendly fire, Groom began to triumph over enemy fire on 8 May 1918, when he began a string of eight victories that would take him through to 30 July. Groom's final tally was three enemy planes set afire, four otherwise destroyed, and one driven down out of control; his observer/gunner for all these victories was [[Ernest Hardcastle]].<ref name=drome/>

Groom went on leave, was laid low by influenza, and did not return to combat in France. Instead, upon recovery, he was posted to [[No. 111 Squadron RAF|No. 111 Squadron]] in Egypt.<ref name=air/>

==Between the World Wars==
On 1 August 1919, Groom was granted a permanent commission as a lieutenant.<ref>{{London Gazette |issue=31486 |startpage=9870 |date=1 August 1919 |nolink=yes}}</ref> On 15 December, he switched to [[No. 14 Squadron RAF|No. 14 Squadron]] in [[Mandatory Palestine|Palestine]] and on 22 May 1920, he continued Middle Eastern service with a new posting, to [[No. 55 Squadron RAF|No. 55 Squadron]].<ref name=air/>

On 25 March 1922, Groom returned home to begin a decade of staff assignments,<ref name=air/> enlivened only by being promoted from [[flying officer]] to [[flight lieutenant]] on 1 July 1924.<ref>{{London Gazette |issue=32952 |startpage=5089 |date=1 July 1924 |nolink=yes}}</ref> He was given command of [[No. 28 Squadron RAF|No. 28 Squadron]] on 26 October 1932.<ref name=air/> On 1 October 1934 he was promoted to [[squadron leader]], his rank catching up with his position.<ref>{{London Gazette |issue=34092 |startpage=6180 |date=2 October 1934 |nolink=yes}}</ref> He moved on to command of [[No. 58 Squadron RAF|No. 58 Squadron]], then on to staff work at Headquarters Bomber Command, even as he continued to ascend in rank. On 1 January 1938, having earned an [[OBE]] at Bomber Command,<ref name=air/> he was promoted to [[Wing Commander (rank)|wing commander]].<ref>{{London Gazette |issue=34468 |startpage=8194 |date=31 December 1937 |nolink=yes}}</ref>

==World War II==

On 1 September 1940, he was promoted to temporary group captain<ref>{{London Gazette |issue=34949 |startpage=5580 |date=20 September 1940 |nolink=yes}}</ref> and became Station Commander at [[RAF Marham]].<ref name=air/> In 1941, he moved back into staff work in the [[Directorate of Plans]].<ref name=air/> The following year, he became head of the RAF Air Staff planning for Operation Overlord.<ref name=air/> As part of his duties there, he accompanied [[Air Chief Marshal]] [[Trafford Leigh-Mallory]] to North Africa to study operations there and bring home lessons learned.<ref name=air/> In the meantime, he continued his climb through the ranks: on 18 November 1942, he was confirmed as a group captain.<ref>{{London Gazette |issue=35809 |supp=yes |startpage=5267 |date=4 December 1942 |nolink=yes}}</ref> On 1 June 1943, he was bumped up to temporary air commodore<ref>{{London Gazette |issue=36067 |supp=yes |startpage=2881 |date=22 June 1943 |nolink=yes}}</ref> and on 8 September 1943 he was promoted to acting air vice marshal<ref>{{London Gazette |issue=36285 |supp=yes |startpage=5438 |date=14 December 1943 |nolink=yes}}</ref> on appointment as Senior Air Staff Officer at Headquarters [[Second Tactical Air Force]].<ref name=air/> On 8 September 1944 he was promoted to air commodore while acting as air vice marshal.<ref>{{London Gazette |issue=36809 |supp=yes |startpage=5383 |date=2 November 1944 |nolink=yes}}</ref> In August 1945 he became Air Officer Administration at Headquarters [[RAF Flying Training Command|Flying Training Command]].<ref name=air/>

==Post World War II==
Groom became Director General of Manning for the RAF on 1 January 1947 and took command of [[No. 205 Group RAF|No. 205 Group]] on 20 October 1949.<ref name=air/> On 1 January 1952, he was promoted from air vice marshal to [[air marshal]]<ref>{{London Gazette |issue=39429 |supp=yes |startpage=59 |date=28 December 1951 |nolink=yes}}</ref> and appointed Knight Commander of the Order of the British Empire.<ref name="ReferenceA">{{London Gazette |issue=39421 |supp=yes |startpage=9 |date=1 January 1952 |nolink=yes}}</ref> He took command of the [[Near East Air Force (Royal Air Force)|Middle East Air Force]] in February 1952 and [[RAF Technical Training Command|Technical Training Command]] in July 1952.<ref name=air/>

Groom retired on 26 September 1955 and died on 6 December 1990.<ref name=air/>

==Honours and awards==
*[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] – 2 November 1918 
:''An officer of great courage and dash who never hesitates to attack the enemy regardless of the superiority in numbers. While on a recent patrol this officer was one of a formation of eight that engaged twenty-five hostile scouts. Lieut. Groom shot down one, and his observer (Lieut. Hardcastle) a few minutes later destroyed another. On a later date, accompanied by the same observer, they were attacked by twelve scouts; two of these they shot down''.<ref>{{London Gazette |issue=30989 |supp=yes |startpage=12966 |date=2 November 1918 |nolink=yes}}</ref>
* Bar to the Distinguished Flying Cross – 19 August 1921 
:''For conspicuous skill and gallantry under fire. While taking part in a bombing expedition from Mosul on 5 May 1921, an aeroplane was shot down by rifle fire in hostile country three miles west of Batas. Flying Officer Groom at once landed and picked up the crew of this machine while under enemy fire. He then successfully took off down hill and returned safely to Mosul with two passengers in the back seat and a third lying on one of the planes. This officer, in addition to showing great promptitude and gallantry, also displayed marked skill in first landing safely under most difficult conditions and then taking off with a very excessive load''.<ref>{{London Gazette |issue=32429 |startpage=6602 |date=19 August 1921 |nolink=yes}}</ref>
*[[Officer of the Order of the British Empire]] – 11 July 1940<ref name=air/>
*[[Commander of the Order of the British Empire]] – 5 July 1945<ref>{{London Gazette |issue=37161 |supp=yes |startpage=3489 |date=3 July 1945 |nolink=yes}}</ref>
*[[Mentioned in Despatches]] – 1 January 1946<ref name=air/>
*[[Knight Commander of the Order of the British Empire]] – 1 January 1952<ref name="ReferenceA"/>
*[[Knight Commander of the Royal Victorian Order]] – 16 July 1953<ref>{{London Gazette |issue=39912 |supp=yes |startpage=3917 |date=14 July 1953 |nolink=yes}}</ref>

==References==
{{Reflist|30em}}

{{s-start}}
{{s-mil}}
{{succession box|
 title=Commander-in-Chief [[Near East Air Force (Royal Air Force)|RAF Middle East Air Force]]|
 before=[[John Baker (RAF officer)|Sir John Baker]]|
 after=[[Arthur Sanders (RAF officer)|Sir Arthur Sanders]]|
 years=February 1952 – May 1952}}
|-
{{s-bef|before=[[John Whitworth-Jones|Sir John Whitworth-Jones]]}} 
{{s-ttl|title=Air Officer Commanding-in-Chief [[RAF Technical Training Command|Technical Training Command]]|years=1952–1955}}
{{s-aft|after=[[George Beamish|Sir George Beamish]]}}
{{end}}

{{Use dmy dates |date=April 2012}}

{{DEFAULTSORT:Groom, Victor}}
[[Category:1898 births]]
[[Category:1990 deaths]]
[[Category:British World War I flying aces]]
[[Category:Companions of the Order of the Bath]]
[[Category:Knights Commander of the Order of the British Empire]]
[[Category:Knights Commander of the Royal Victorian Order]]
[[Category:Royal Air Force air marshals]]
[[Category:British Army personnel of World War I]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Royal Flying Corps officers]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:People from Peckham]]
[[Category:People educated at Alleyn's School]]