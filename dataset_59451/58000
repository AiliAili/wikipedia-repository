{{Infobox journal
| title         = Journal of Clinical Microbiology
| cover         = [[File:Journal of Clinical Microbiology cover.gif|200px]]
| editor        = [[Gary V. Doern]]
| discipline    = [[Microbiology]]
| abbreviation  = J. Clin. Microbiol.
| publisher     = [[American Society for Microbiology]]
| country       = United States
| frequency     = Monthly
| history       = 1975-present
| openaccess    = [[Delayed open access journal|Delayed]], after 6 months
| license       = 
| impact        = 4.232
| impact-year   = 2013
| website       = http://jcm.asm.org/
| link1         = http://jcm.asm.org/content/current
| link1-name    = Online access
| link2         = http://jcm.asm.org/content/by/year
| link2-name    = Online archive
| link3         = http://www.ncbi.nlm.nih.gov/pmc/journals/81/latest/
| link3-name    = PubMed Central archive
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 01799460
| LCCN          = 
| CODEN         = JCMIDW
| ISSN          = 0095-1137
| eISSN         = 1098-660X
| boxwidth      = 
}}
The '''''Journal of Clinical Microbiology''''' is a monthly [[medical journal]] published by the [[American Society for Microbiology]]. The journal was established in 1975. The [[editor-in-chief]] is [[Gary V. Doern]]. It is a [[delayed open access journal]] full text content is available free after a six-month [[Embargo (academic publishing)|embargo]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
*[[Biotechnology Citation Index]]<ref name=UWEB/>
*[[CAB Abstracts]]<ref name= CABAB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/cab-abstracts/ |title= Serials cited |work= [[CAB Abstracts]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
*[[Cambridge Scientific Abstracts]]
*[[Chemical Abstracts]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-27}}</ref>
*[[Current Contents]]/Clinical Medicine<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-27}}</ref>
*Current Contents/Life Sciences<ref name=ISI/>
*[[Elsevier BIOBASE]]<ref name=UWEB>{{cite web |title= ''Journal of Clinical Microbiology'' |url= http://www.ulrichsweb.serialssolutions.com/title/1419661499140/63362 |work= [[Ulrichsweb]] |publisher= [[ProQuest]] |accessdate=2014-12-27 |subscription= yes}}</ref>
*[[Embase]]<ref name=UWEB/>
*[[Global Health]]<ref name= CABGH>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/global-health/ |title= Serials cited |work= [[Global Health]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
*[[Index Medicus]]/[[MEDLINE]]/[[PubMed]]/[[PubMed Central]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7505564 |title= ''Journal of Clinical Microbiology'' |work= [[United States National Library of Medicine|NLM]] Catalog |publisher=[[National Center for Biotechnology Information]] |format= |accessdate=2014-12-27}}</ref>
*[[International Bibliography of Periodical Literature]]<ref name=UWEB/>
*[[Science Citation Index]]<ref name=ISI/>
*[[Scopus]]<ref name=UWEB/>
*[[Tropical Diseases Bulletin]]<ref name= CABTDB>{{cite web |url= http://www.cabi.org/publishing-products/online-information-resources/tropical-diseases-bulletin/ |title= Serials cited |work= [[Tropical Diseases Bulletin]] |publisher= [[CABI (organisation)|CABI]] |accessdate=2014-12-27}}</ref>
*[[Zoological Record]]<ref name=ISI/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 4.232, ranking it 23rd out of 119 journals in the category "Microbiology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Microbiology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
*[[Clinical medicine]]
*[[Clinical research]]
*[[Medical microbiology]]

==References==
{{reflist|30em}}

== External links ==
*{{Official website|http://jcm.asm.org/}}
*[http://www.asm.org/ American Society for Microbiology]

[[Category:Delayed open access journals]]
[[Category:Microbiology journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1975]]
[[Category:Applied microbiology journals]]