{{Infobox Hurricane
| Name=Tropical Depression Ten
| Type=Tropical depression
| Year=2007
| Basin=Atl
| Image location=TD Ten 21 sept 2007 1650Z.jpg
| Image name=Tropical Depression Ten near landfall
| Formed=September 21, 2007
| Dissipated=September 22, 2007
| 1-min winds=30
| Pressure=1005
| Damages=6.2
| Inflated=1
| Fatalities=None reported
| Areas=[[Florida]], [[Georgia (U.S. state)|Georgia]], [[Alabama]]
| Hurricane season=[[2007 Atlantic hurricane season]]
}}
'''Tropical Depression Ten''' was a short-lived [[tropical cyclone]] that made [[landfall (meteorology)|landfall]] on the [[Florida Panhandle]]<!--the rainfall map shows the track as crossing the peninsula as well; is that a mistake, or should the intro mention this?--> in September&nbsp;2007. The system developed as a [[subtropical cyclone|subtropical depression]] on September&nbsp;21 in the northeastern [[Gulf of Mexico]] from the interaction of a [[tropical wave]], the tail end of a [[cold front]], and an [[Cold-core low|upper-level low]]. Initially containing a poorly defined circulation and intermittent thunderstorm activity, the system transitioned into a tropical depression after [[atmospheric convection|convection]] increased over the center. Tracking northwestward, the depression moved ashore near [[Fort Walton Beach, Florida|Fort Walton Beach]] early on September&nbsp;22 and [[tropical cyclone#Dissipation|dissipated]] over southeastern [[Alabama]] shortly thereafter.

Initially the depression was forecast to move ashore as a minimal tropical storm, and the threat of the depression prompted state of emergency declarations in Mississippi and Louisiana. It was the first tropical cyclone to threaten the New Orleans area since [[Hurricane Katrina]] and the destructive [[2005 Atlantic hurricane season|2005 hurricane season]]. Overall impact from the cyclone was minor and largely limited to light rainfall. However, the precursor system spawned a damaging tornado in [[Eustis, Florida]], where 20&nbsp;houses were destroyed and 30&nbsp;more were damaged.

== Meteorological history ==
{{storm path|10-L 2007 track.png}}
Tropical Depression Ten formed from the complex interaction between an upper-level [[low-pressure area|low]], a [[tropical wave]] that produced [[Tropical Storm Ingrid (2007)|Tropical Storm Ingrid]], and the tail end of a [[cold front]]. By September&nbsp;17, the system produced widespread thunderstorm activity over [[The Bahamas]] and western Atlantic Ocean.<ref name="tcr">{{cite web|author=Jamie Rhome|year=2007|title=Tropical Depression Ten Tropical Cyclone Report|publisher=National Hurricane Center|accessdate=2007-10-16|format=PDF|url={{NHC TCR url|id=AL102007_Ten}}}}</ref> The upper-level low over the [[Florida Panhandle]] increased [[convection]] across the area, and on September&nbsp;18 the system began crossing [[Florida]].<ref>{{cite web|author=Avila|year=2007|title=September 18 Tropical Weather Outlook|publisher=National Hurricane Center|accessdate=2007-09-21|url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Outlook-A/2007091821.ABNT20}}{{Dead link|date=March 2015}}</ref> Initially very disorganized, surface pressures gradually decreased across the region, with a weak low-pressure area developing on September&nbsp;19.<ref>{{cite web|author=Pasch|year=2007|title=September 19 Tropical Weather Outlook|publisher=National Hurricane Center|accessdate=2007-09-21|url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Outlook-A/2007091909.ABNT20}}{{Dead link|date=March 2015}}</ref>

A [[Hurricane Hunters|reconnaissance aircraft]] flight into the system on September&nbsp;20 reported a well-defined low and strong wind gusts in [[squall]]s as the system tracked into the northeastern [[Gulf of Mexico]], along with limited and disorganized thunderstorm activity.<ref>{{cite web|author=Pasch|year=2007|title=September 20 Tropical Weather Outlook|publisher=National Hurricane Center|accessdate=2007-09-21|url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Outlook-A/2007092021.ABNT20}}{{Dead link|date=March 2015}}</ref> Convection gradually became better organized, with a well-defined [[tropical cyclone#Physical structure|band]] in its eastern semicircle and intermittent thunderstorm activity near the center. Despite an overall disorganized structure, with a poorly defined circulation and an upper-level low aloft, the [[National Hurricane Center]] initiated advisories on Subtropical Depression Ten at 1500&nbsp;[[Coordinated Universal Time|UTC]] on September&nbsp;21 while it was located about 40&nbsp;miles (60&nbsp;km) south of [[St. Vincent Island, Florida]], citing "the potential for additional development right along the coastline."<ref name="disc1">{{cite web|author=Franklin|year=2007|title=Subtropical Depression Ten Discussion One|publisher=National Hurricane Center|accessdate=2007-09-21|url=http://www.nhc.noaa.gov/archive/2007/al10/al102007.discus.001.shtml?}}</ref> In post-analysis, it was classified a subtropical cyclone three hours earlier.<ref name="tcr"/>

With a mid-level [[ridge (meteorology)|ridge]] to its northwest, the [[subtropical cyclone|subtropical depression]] was anticipated to parallel the coastline of the [[Gulf Coast of the United States]]. As a result, it was forecast to attain winds of 45&nbsp;mph (75&nbsp;km/h) and move ashore along southern [[Mississippi]].<ref name="disc1"/> The circulation became better defined as convection modestly increased over the center, and within six hours of its development the system [[tropical cyclogenesis|transitioned]] into a tropical depression. The cyclone continued tracking northwestward,<ref name="disc2">{{cite web|author=Franklin|year=2007|title=Tropical Depression Ten Discussion Two|publisher=National Hurricane Center|accessdate=2007-09-21|url=http://www.nhc.noaa.gov/archive/2007/al10/al102007.discus.002.shtml?}}</ref> making landfall around 0000&nbsp;UTC on September&nbsp;22 near [[Fort Walton Beach, Florida]], with winds of 35&nbsp;mph (55&nbsp;km/h).<ref name="pa2a">{{cite web|author=Avila|year=2007|title=Tropical Depression Ten Public Advisory Two-A|publisher=National Hurricane Center|accessdate=2007-09-21|url=http://www.nhc.noaa.gov/archive/2007/al10/al102007.public_a.002.shtml?}}</ref> The cloud pattern deteriorated as it tracked inland, and 3&nbsp;hours after it moved ashore the National Hurricane Center issued its last advisory on the depression.<ref name="disc3">{{cite web|author=Avila|year=2007|title=Tropical Depression Ten Discussion Three|publisher=National Hurricane Center|accessdate=2007-09-21|url=http://www.nhc.noaa.gov/archive/2007/al10/al102007.discus.003.shtml?}}</ref> As the depression tracked into [[Alabama]], it became increasingly disorganized,<ref name="hpc4a"/> and the system dissipated as a tropical cyclone early on September 22.<ref name="tcr"/> Its remnant surface low continued west-northwest before dissipating near the [[Louisiana]]/[[Texas]] border early on September&nbsp;23.<ref name="rain">{{cite web|author=David M. Roth|year=2007|title=Tropical Depression #10 - September 18–23, 2007|publisher=Hydrometeorological Prediction Center|url=http://www.wpc.ncep.noaa.gov/tropical/rain/td102007.html|accessdate=2007-10-18|archiveurl = https://web.archive.org/web/20080616074635/http://www.hpc.ncep.noaa.gov/tropical/rain/td102007.html |archivedate =2008-06-16|deadurl=yes}}</ref>

== Preparations and impact ==
[[Image:Eustis, Florida Tornado Damage (1).jpg|right|thumb|Tornado damage in Eustis, Florida]]
[[Image:Tropical Depression Ten 2007 rainfall.gif|thumb|right|Total rainfall from the depression]]

The combination of [[wind shear]] and low-level [[hydrodynamical helicity|helicity]] produced moderate convection across central [[Florida]] in association with the precursor low pressure system. Late on September&nbsp;20, a [[supercell]] developed near [[Lake Apopka]], and tracking quickly northward it spawned an [[Enhanced Fujita scale|EF1]] [[tornado]] near [[Eustis, Florida|Eustis]]; the tornado tracked for 1.83&nbsp;mi (2.94&nbsp;km) and reached winds of about 100&nbsp;mph (160&nbsp;km/h).<ref>{{cite web|author=Melbourne, Florida National Weather Service|year=2007|title=Eustis Tornado|accessdate=2007-09-23|url=http://www.srh.noaa.gov/mlb/surveys/092007/index.html |archiveurl=https://web.archive.org/web/20081016130800/http://www.srh.noaa.gov/mlb/surveys/092007/index.html |archivedate=2008-10-16}}</ref> The tornado destroyed 20&nbsp;homes, left 30&nbsp;others severely damaged, injured one person, and caused power outages for about 300&nbsp;people.<ref>{{cite news|author=CBS.com |title=Florida Tornado Strikes 50 Homes |accessdate=2007-09-21 |url=http://www.cbsnews.com/stories/2007/09/21/storm/main3285673.shtml |archiveurl=http://www.webcitation.org/5qXxCQDLk?url=http://www.cbsnews.com/stories/2007/09/21/storm/main3285673.shtml |archivedate=2010-06-17 |work=CBS News |date=2007-09-21 |deadurl=yes |df= }}</ref> Damage totaled $6.2&nbsp;million (2007&nbsp;USD).<ref>{{cite web|author=National Climatic Data Center|accessdate=2008-02-20|year=2007|title=Event Report for Florida|url=http://www4.ncdc.noaa.gov/cgi-win/wwcgi.dll?wwevent~ShowEvent~450914|archiveurl=https://web.archive.org/web/20110520002428/http://www4.ncdc.noaa.gov/cgi-win/wwcgi.dll?wwevent~ShowEvent~450914|archivedate=2011-05-20}}</ref> Tornadoes were also reported near [[Marianna, Florida|Marianna]] and [[Chipley, Florida|Chipley]].<ref>{{cite web|author=Godsey & Jamski |year=2007 |title=Tropical Depression Ten Post-Tropical Cyclone Report |publisher=Tallahassee, Florida National Weather Service |accessdate=2007-09-24 |url=http://www.srh.noaa.gov/data/warn_archive/TAE/PSH/0924_205249.txt |archiveurl=http://www.webcitation.org/5qXxL87Js?url=http://www.srh.noaa.gov/data/warn_archive/TAE/PSH/0924_205249.txt |archivedate=2010-06-17 |deadurl=yes |df= }}</ref> The precursor low pressure system also generated [[lightning]] that stuck and killed a man in [[Hendry County, Florida]].<ref>{{cite web|author=|title=Lightning Event Report for Florida|year=2007|publisher=National Climatic Data Center|accessdate=2016-01-22|url=http://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=66113}}</ref>

Outer rainbands began affecting coastal sections of the [[Florida Panhandle]] by about 12&nbsp;hours prior to the formation of the depression.<ref>{{cite web|author=Avila|year=2007|title=September 20 Tropical Weather Outlook (2)|publisher=National Hurricane Center|accessdate=2007-09-21|url=ftp://ftp.met.fsu.edu/pub/weather/tropical/Outlook-A/2007092102.ABNT20}}{{Dead link|date=March 2015}}</ref> Coinciding with the first advisory on the depression, the [[National Hurricane Center]] issued a [[tropical cyclone warnings and watches|tropical storm warning]] from [[Apalachicola, Florida]], westward to the mouth of the [[Mississippi River]].<ref name="pa1">{{cite web|author=Franklin|year=2007|title=Subtropical Depression Ten Public Advisory One|publisher=National Hurricane Center|accessdate=2007-09-21|url=http://www.nhc.noaa.gov/archive/2007/al10/al102007.public.001.shtml?}}</ref> Shortly thereafter, an inland tropical storm warning was issued for [[Pearl River County, Mississippi|Pearl River]], [[Walthall County, Mississippi|Walthall]], and [[Pike County, Mississippi|Pike counties]] in [[Mississippi]] and [[Washington Parish, Louisiana|Washington Parish]] in [[Louisiana]]. Additionally, the [[New Orleans]] [[National Weather Service]] issued a [[coastal flood watch]] for four parishes in southeastern Louisiana.<ref>{{cite web|author=New Orleans National Weather Service |year=2007 |title=Tropical Depression Ten Local Statement |accessdate=2007-09-21 |url=http://www.nhc.noaa.gov/text/WTUS84-KLIX.shtml |deadurl=yes |archiveurl=http://www.webcitation.org/5S21arNCh?url=http://www.nhc.noaa.gov/text/WTUS84-KLIX.shtml |archivedate=September 22, 2007 |df= }}</ref> In [[Mississippi]], [[List of Governors of Mississippi|Governor]] [[Haley Barbour]] declared a state of emergency. Officials ordered a mandatory evacuation for residents in shallow areas and in mobile homes for [[Jackson County, Mississippi|Jackson]], [[Harrison County, Mississippi|Harrison]], and [[Hancock County, Mississippi|Hancock counties]].<ref>{{cite news | url=http://www.natchezdemocrat.com/2007/09/22/after-storm-preparations-miss-avoids-tropical-jolt/ | title=After storm preparations, Miss. avoids tropical jolt | work=natchezdemocrat.com | date=22 September 2007 | accessdate=June 7, 2015 | location=Biloxi}}</ref> Officials in [[New Orleans]] opened three emergency shelters,<ref>{{cite web|author=Times-Piscayne|year=2007|title=Nagin announces storm shelter locations|accessdate=2007-09-21|url=http://blog.nola.com/times-picayune/2007/09/nagin_annouces_storm_shelter_l.html}}</ref> citing the potential need of shelter for citizens in about 17,000&nbsp;[[FEMA trailer]]s after [[Hurricane Katrina]]. Due to the threat of the cyclone, [[List of Governors of Louisiana|Louisiana governor]] [[Kathleen Blanco]] declared a state of emergency and placed the state's National Guard and other disaster services on reserve.<ref>{{cite news|author=Michael Winter|year=2007|title=Gulf Coast braces for first tropical storm since Katrina|publisher=USAToday.com|accessdate=2007-09-21|url=http://blogs.usatoday.com/ondeadline/2007/09/gulf-coast-brac.html|archiveurl=https://web.archive.org/web/20071010040528/http://blogs.usatoday.com/ondeadline/2007/09/gulf-coast-brac.html|archivedate=2007-10-10}}</ref>

Waves of about 5&nbsp;feet (1.5&nbsp;m) and rip currents were reported along the west coast of Florida.<ref>{{cite web|author=Tampa Bay National Weather Service |year=2007 |title=Coastal Hazard Message |accessdate=2007-09-22 |url=http://www.srh.noaa.gov/data/warn_archive/TBW/CFW/0921_114356.txt |archiveurl=http://www.webcitation.org/5qXxKj124?url=http://www.srh.noaa.gov/data/warn_archive/TBW/CFW/0921_114356.txt |archivedate=2010-06-17 |deadurl=yes |df= }}</ref> However, no [[beach erosion]] was reported.<ref name="ncdc"/> Rainfall associated with the system peaked at 7.29&nbsp;inches (185&nbsp;mm) at [[Hastings, Florida|Hastings]].<ref name="rain"/> Elsewhere, rainfall totals reached 1.46&nbsp;inches (37.1&nbsp;mm) in [[Albany, Georgia]], and 0.51&nbsp;inches (13&nbsp;mm) in [[Dothan, Alabama]].<ref name="hpc4a">{{cite web | url=http://www.wpc.ncep.noaa.gov/tropical/tropical_advisories.php?storm=10&adnum=4&dt=2007092209&status=remnants | title=Remnants of 10 Advisory Number 4 | publisher=Weather Prediction Center | date=22 September 2007 | accessdate=June 7, 2015}}</ref> Wind gusts from the storm peaked at 46&nbsp;mph (74&nbsp;km/h) in [[Milton, Florida]], which blew down a few trees in [[Escambia County, Florida|Escambia County]].<ref>{{cite web|author=Beeler |year=2007 |title=Tropical Depression Ten Post Tropical Cyclone Report |publisher=Mobile, Alabama National Weather Service |accessdate=2007-09-24 |url=http://www.srh.noaa.gov/data/warn_archive/MOB/PSH/0924_172212.txt |archiveurl=http://www.webcitation.org/5qXxLRdp7?url=http://www.srh.noaa.gov/data/warn_archive/MOB/PSH/0924_172212.txt |archivedate=2010-06-17 |deadurl=yes |df= }}</ref> Overall damage from the depression was minimal.<ref name="tcr"/> [[Storm surge]] ranged from {{convert|2.5|to|4.1|ft|m}} along the Panhandle.<ref name="ncdc">{{cite web|author=|title=Tropical Depression Ten Event Report for Florida|year=2007|publisher=National Climatic Data Center|accessdate=2008-11-11|url=http://www4.ncdc.noaa.gov/cgi-win/wwcgi.dll?wwevent~ShowEvent~684643}}{{Dead link|date=July 2013}}</ref>

Prior to the storm's development, several oil and gas companies removed unneeded workers from offshore oil platforms in the northern Gulf of Mexico; [[Shell Oil Company]] evacuated about 700&nbsp;employees, while [[Noble Energy]] removed its workforce of about 300&nbsp;people from two oil rigs.<ref>{{cite news|author=Brett Canton |date=2007-09-19 |title=As tropical low lurks, offshore companies exercise caution |publisher=Houston Chronicle |accessdate=2007-09-21 |url=http://www.redorbit.com/news/business/1072198/as_tropical_low_lurks_offshore_companies_exercise_caution/index.html |archiveurl=http://www.webcitation.org/5qXxHomkn?url=http://www.redorbit.com/news/business/1072198/as_tropical_low_lurks_offshore_companies_exercise_caution/index.html |archivedate=2010-06-17 |deadurl=yes |df= }}</ref> [[ExxonMobil]] cut its output by about 1,000&nbsp;barrels of oil and {{convert|55000|ft3}}.<ref>{{cite news|author=Erwin Seba|date=2007-09-20|title=Oil firms pull U.S. Gulf workers|publisher=Reuters|accessdate=2007-09-21|url=http://www.reuters.com/article/businessNews/idUSN1924225720070920}}</ref> With 27.7% of the daily crude oil production halted due to the depression, oil prices rose further after days of increasing levels, and on September&nbsp;20 reached a record rate of over $84 per barrel.<ref>{{cite web|author=Reuters|year=2007|title=Foul weather slashes U.S. Gulf oil production|accessdate=2007-09-21|url=http://www.signonsandiego.com/news/business/20070921-0707-storm-energy-outages.html}}</ref>

=== Tornadoes ===
{| class="wikitable sortable" style="width:100%;"
|+ List of confirmed tornadoes{{refn|All dates are based on the local [[time zone]] where the tornado touched down; however, all times are in [[Coordinated Universal Time]] for consistency.||group="nb"|name="Date/Time"}}
|-
! scope="col"  style="width:3%; text-align:center;"|[[Enhanced Fujita scale|EF#]]
! scope="col"  style="width:7%; text-align:center;" class="unsortable"|Location
! scope="col"  style="width:6%; text-align:center;" class="unsortable"|County / Parish
! scope="col"  style="width:5%; text-align:center;"|State
! scope="col"  style="width:6%; text-align:center;"|Start Coord.
! scope="col"  style="width:7%; text-align:center;"|Date
! scope="col"  style="width:6%; text-align:center;"|Time ([[Coordinated Universal Time|UTC]])
! scope="col"  style="width:6%; text-align:center;"|Path length
! scope="col"  style="width:6%; text-align:center;"|Max width
! scope="col"  style="width:6%; text-align:center;"|Damage{{refn|All damage totals are in 2007 [[United States dollars|USD]] unless otherwise stated.||group="nb"|name="Damage"}}
! scope="col" class="unsortable" style="width:48%; text-align:center;"|Summary
! scope="col" class="unsortable" style="width:48%; text-align:center;"|Refs
|-
|bgcolor=#{{storm colour|cat1}} | EF1
|[[Eustis, Florida|Eustis]] area
|[[Lake County, Florida|Lake]]
|[[Florida|FL]]
|{{Coord|28.84|-81.68|name=Eustis (Sept. 20, EF1)}}
|{{sort|0920|September 20}}
|0257&nbsp;– 0305
|{{convert|1.83|mi|km|abbr=on|sortable=on}}
|{{convert|100|yd|m|abbr=on|sortable=on}}
|$6,200,000
|Seven homes were destroyed, 27 homes sustained major damage, and 81 homes reported minor damage.
|<ref name="0920rpt">{{cite web|title=20070920's Storm Reports (1200 UTC - 1159 UTC)|url=http://www.spc.noaa.gov/climo/reports/070920_rpts.html|work=[[Storm Prediction Center]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=2007-09-20|accessdate=2014-02-22}}</ref><ref>{{cite report|author=National Weather Service office in Tallahassee, Florida|title=Florida event report: EF1 tornado|url=https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=60913|publisher=National Climatic Data Center|work=National Oceanic and Atmospheric Administration|year=2007|accessdate=2014-02-22}}</ref>
|-
|bgcolor=#{{storm colour|cat1}} | EF1
|W of [[Mayo, Florida|Mayo]]
|[[Lafayette County, Florida|Lafayette]]
|[[Florida|FL]]
|{{Coord|30.05|-83.25|name=Mayo (Sept. 20, EF1)}}
|{{sort|0920|September 20}}
|0420&nbsp;– 0421
|{{convert|0.25|mi|km|abbr=on|sortable=on}}
|{{convert|35|yd|m|abbr=on|sortable=on}}
|$2,000
|Large oak trees were uprooted.
|<ref name="0920rpt"/><ref>{{cite report|author=National Weather Service office in Tallahassee, Florida|title=Florida event report: EF1 tornado|url=https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=60211|publisher=National Climatic Data Center|work=National Oceanic and Atmospheric Administration|year=2007|accessdate=2014-02-22}}</ref>
|-
|bgcolor=#{{storm colour|storm}} | EF0
|SE of [[Clio, Alabama|Clio]]
|[[Barbour County, Alabama|Barbour]]
|[[Alabama|AL]]
|{{Coord|31.66|-85.57|name=Blue Spgs (Sept. 21, EF0)}}
|{{sort|0921|September 21}}
|2234&nbsp;– 2235
|{{convert|0.36|mi|km|abbr=on|sortable=on}}
|{{convert|20|yd|m|abbr=on|sortable=on}}
|{{N/A}}
|Tornado reported by a Sheriff's deputy; no damage.
|<ref>{{cite web|title=20070921's Storm Reports (1200 UTC - 1159 UTC)|url=http://www.spc.noaa.gov/climo/reports/070921_rpts.html|work=[[Storm Prediction Center]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=2007-09-21|accessdate=2014-02-22}}</ref><ref>{{cite report|author=National Weather Service office in Birmingham, Alabama|title=Alabama event report: EF0 tornado|url=https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=61185|publisher=National Climatic Data Center|work=National Oceanic and Atmospheric Administration|year=2007|accessdate=2014-02-22}}</ref>
|-
|bgcolor=#{{storm colour|storm}} | EF0
|[[Jerome, Florida|Jerome]] area
|[[Collier County, Florida|Collier]]
|[[Florida|FL]]
|{{Coord|26.00|-81.40|name=Jerome (Sept. 22, EF1)}}
|{{sort|0922|September 22}}
|0100&nbsp;– 0105
|{{convert|0.5|mi|km|abbr=on|sortable=on}}
|{{convert|20|yd|m|abbr=on|sortable=on}}
|{{N/A}}
|Several motorists reported a tornado touchdown; no reported damage.
|<ref>{{cite web|title=20070922's Storm Reports (1200 UTC - 1159 UTC)|url=http://www.spc.noaa.gov/climo/reports/070922_rpts.html|work=Storm Prediction Center|publisher=National Oceanic and Atmospheric Administration|date=2007-09-22|accessdate=2014-02-22}}</ref><ref>{{cite report|author=National Weather Service office in Miami, Florida|title=Florida event report: EF0 tornado|url=https://www.ncdc.noaa.gov/stormevents/eventdetails.jsp?id=60550|publisher=National Climatic Data Center|work=National Oceanic and Atmospheric Administration|year=2007|accessdate=2014-02-22}}</ref>
|}

== See also ==
{{Commons category|Tropical Depression Ten (2007)}}
{{Portal|Tropical cyclones}}
* [[List of Florida hurricanes (2000–present)]]
* [[Timeline of the 2007 Atlantic hurricane season]]
{{Clear}}

== Footnotes ==
{{reflist|group=nb}}

== References ==
{{Reflist|30em}}

{{2007 Atlantic hurricane season buttons}}

{{Featured article}}

{{DEFAULTSORT:10 (2007)}}
[[Category:2007 Atlantic hurricane season]]
[[Category:Atlantic tropical depressions|10 2007]]
[[Category:Hurricanes in Florida]]