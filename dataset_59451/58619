{{italictitle}}
{{Infobox Journal
| title        = Journal of Thermal Analysis and Calorimetry
| cover        = [[File:Journal of Thermal Analysis and Calorimetry.jpg]]
| editor       = Judit Simon
| discipline   = [[Chemistry]]
| abbreviation = J. Therm. Anal. Calorim.
| publisher    = [[Springer Science+Business Media|Springer]]
| country      = [[Netherlands]]
| frequency    = Monthly
| history      = 1969-present
| openaccess   =
| impact       = 2.042
| impact-year  = 2014
| website      = http://www.springer.com/journal/10973/
| link1        = http://www.springerlink.com/content/1388-6150
| link1-name   = Online access
| link2        =
| link2-name   = 
| RSS          = http://www.springerlink.com/content/ 1388-6150?sortorder=asc&export=rss RSS
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1388-6150
| eISSN        = 1572-8943 }}
The '''''Journal of Thermal Analysis and Calorimetry''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Springer Science+Business Media]]<ref>[http://www.springer.com/10973  Journal's homepage]</ref> in cooperation with Akadémiai Kiadó. Formerly this journal was known as '''''Journal of Thermal Analysis'''''. It publishes papers covering all aspects of [[calorimetry]], [[thermal analysis]], and experimental thermodynamics. Some of the subjects covered are [[thermogravimetry]], differential scanning calorimetry of all types, derivative thermogravimetry, [[thermal conductivity]], [[thermomechanical analysis]], and the theory and instrumentation for thermal analysis and calorimetry.

== Impact factor ==
The ''Journal of Thermal Analysis and Calorimetry'' had a 2014 [[impact factor]] of 2.042,<ref>[http://admin.isiknowledge.com ISI Web of Knowledge]</ref> ranking it 37th out of 74 in the subject category 'Analytical Chemistry' and 75th out of 139 in 'Physical Chemistry'.

== Editor ==
The [[editor in chief]] of this journal is Alfréd Kállay-Menyhárd([[Budapest University of Technology and Economics]]).
The deputy [[editor in chief]] of this journal is Imre Miklós Szilágyi([[Budapest University of Technology and Economics]]).

== External links ==
*{{Official website|http://www.springer.com/journal/10973}}

== References ==
{{reflist}}

[[Category:Chemistry journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Publications established in 1969]]
[[Category:English-language journals]]
[[Category:Monthly journals]]