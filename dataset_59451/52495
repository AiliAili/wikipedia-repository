{{Infobox journal
| title = Pakistaniaat
| cover = [[File:Pakistaniaat.jpg]]
| editor = [[Masood Ashraf Raja]]
| discipline = [[Area studies]]
| abbreviation = Pakistaniaat
| publisher = [[University of North Texas]]
| country = United States
| frequency = Triannually
| history = 2009-present
| openaccess = Yes
| license = [[Creative Commons licenses|Creative Commons: Attribution-NC-Share Alike]]
| impact =
| impact-year =
| website = http://pakistaniaat.org
| link1 = http://www.pakistaniaat.org/issue/archive
| link1-name = Online archive
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN =
| CODEN =
| ISSN = 1948-6529
| eISSN = 1946-5343
}}
'''''Pakistaniaat: A Journal of Pakistan Studies''''' ({{lang-ur|{{URDU|پاکستانیات}}}}) is a [[Peer review|peer-reviewed]] [[open access]] [[academic journal]] established in 2009, which covers research on [[Pakistan studies]].<ref>[http://www.pakistaniaat.org/about/editorialPolicies#focusAndScope Editorial Policies<!-- Bot generated title -->]</ref> It is published triannually by the English Department of the [[University of North Texas]]<ref>{{cite web|url=http://www.engl.unt.edu/ |title=Department of English |publisher=Engl.unt.edu |date= |accessdate=2013-01-21}}</ref> and is also sponsored by the [[American Institute of Pakistan Studies]].<ref>[http://www.pakistanstudies-aips.org/resources/publications/index.html AIPS Resources: Publications<!-- Bot generated title -->]</ref> ''Pakistaniaat'' occasionally publishes special issues, for example on the [[Indo-Pakistani War of 1971]].<ref>{{cite web|url=http://www.pakistaniaat.org/issue/view/369|editor=Cara Cilano|title=Pakistaniaat: Special Issue on 1971 Indo-Pakistan War|accessdate=2011-07-18}}</ref>

== Abstracting and indexing ==
A member of [[The Council of Editors of Learned Journals]],<ref>{{cite web|url=http://www.celj.org/memberJournalsList/p|title=CELJ|accessdate=2011-07-17}}</ref> ''Pakistaniaat'' is indexed in LivRe!,{{clarify|date=September 2013}} the [[Modern Language Association|MLA International Bibliography]], and [[Ulrich's Periodicals Directory]].

== References ==
{{Reflist|30em}}

== External links ==
* {{Official website|http://pakistaniaat.org}}

{{DEFAULTSORT:Pakistaniaat}}
[[Category:Articles created via the Article Wizard]]
[[Category:Asian studies journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2009]]
[[Category:University of North Texas]]
[[Category:Pakistan studies]]
[[Category:Creative Commons-licensed journals]]
[[Category:Works about Pakistan]]
[[Category:Academic journals published by universities and colleges]]