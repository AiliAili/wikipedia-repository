{{Other uses|Arthur Murray (disambiguation)}}
{{citation style|date=January 2013}}
{{Infobox military person
|honorific_prefix  =
|name              =Arthur W. Murray
|honorific_suffix  =
|native_name       =
|native_name_lang  =
|image         =Arthur Kit Murray by X-1.jpg
|image_size    =
|alt           =
|caption       =Kit Murray by X-1A
|birth_date    ={{birth date|1918|12|26}}
|death_date    ={{Death date and age|2011|07|25|1918|12|26}}
|birth_place   =[[Cresson, Pennsylvania]]
|death_place   =[[West, Texas]]
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =Kit
|birth_name    =
|allegiance    ={{flag|United States of America|23px}}
|branch        ={{air force|United States|23px}}
|serviceyears  =1939–1961
|rank          =[[File:US-O5 insignia.svg|23px]] [[Lieutenant Colonel (United States)|Lieutenant Colonel]]
|servicenumber =
|unit          =
|commands      =
|battles       =
|battles_label =
|awards        =[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]<br>[[Air Medal]]<br>[[Aerospace Walk of Honor]]
|spouse        = <!-- Add spouse if reliably sourced -->
|relations     =
|laterwork     =Aerospace manager
|signature     =
|website       = <!-- {{URL|example.com}} -->
}}
'''Arthur Warren "Kit" Murray''' (December 26, 1918 – July 25, 2011)<ref name=LATimes>{{cite news |date=August 3, 2011 |url = http://www.latimes.com/news/obituaries/la-me-kit-murray-20110803,0,2966443.story |title =Arthur 'Kit' Murray dies at 92 |accessdate = August 5, 2011 |work=Los Angeles Times |first=McClatchy |last=Newspapers}}</ref> was a United States [[test pilot]]. He flew test flights on the [[Bell X-1]] and the [[Bell X-5]] aircraft.

== Early life ==
Arthur Warren Murray was born to Charles C. "Chester" and Elsie Murray in the small town of [[Cresson, Pennsylvania|Cresson]] nestled in the [[Allegheny Mountains]] of [[Pennsylvania]] on December 26, 1918.

== Military career ==
With [[World War II]] already underway in Europe, he joined the [[United States Army]] in 1939, and served in the [[Cavalry]]. Kit volunteered for pilot training the day after the [[attack on Pearl Harbor]], and by 1943 was flying the [[Curtiss P-40|P-40]] as a fighter pilot in Africa. His unit worked its way across the continent from [[Casablanca]] to [[Tunisia]], escorting [[B-25 Mitchell|B-25]], [[B-26 Marauder|B-26]] and [[A-20 Havoc|A-20]] bombers as well as performing [[dive bomber|dive bombing]] and [[strafing]] missions. His unit was proud to never have lost a bomber to enemy fighters while under their escort.

=== Test piloting ===
After a year tour in Africa, Kit returned to the United States as a [[P-47 Thunderbolt|P-47]] instructor at [[Bradley International Airport|Bradley Field]] near [[Hartford, Connecticut]]. He was then assigned as a maintenance flight test pilot and sent to Maintenance Engineering School at [[Chanute Air Force Base]]. After completion of that school his commander found out about the Flight Test School at [[Wright-Patterson Air Force Base|Wright Field]] and decided to send him there. Here was where Kit got his big break as he quickly found out this school was not for functional test flights, but for experimental test programs. He kept his mouth shut and stuck with the program, and soon was offered the opportunity to be the first permanent test pilot to be assigned to Muroc Airfield (later [[Edwards Air Force Base]]) in the California [[desert]]. Until then, pilots were based at the Wright Field Test Center and assigned TDY as needed to Muroc. [[Chuck Yeager]] was making such trips out there from the Test Center while he was flying the [[Bell X-1]] on the first [[supersonic]] test flights. In early tests Kit was able to fly some of America’s earliest jet aircraft including the [[Bell XP-59]] and the [[P-80 Shooting Star|P-80]].  He also flew the [[P-51 Mustang|P-51]], [[F-82 Twin Mustang|P-82]], [[F-84 Thunderjet|F-84]], [[B-25 Mitchell|B-25]], [[XB-43 Jetmaster|B-43]], [[B-45 Tornado|B-45]] and many other fighter and bomber aircraft.

Kit flight tested the [[Bell X-1|X-1A and X-1B]], the [[X-4 Bantam|X-4]], the [[Bell X-5|X-5]],<ref>[http://www.dfrc.nasa.gov/gallery/photo/X-1A/HTML/E-2490.html X-1A E-2490: X-1A in flight over lakebed<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20071008205801/http://www.dfrc.nasa.gov/gallery/photo/X-1A/HTML/E-2490.html |date=October 8, 2007 }}</ref> and also flew the [[Convair XF-92|XF-92A]].<ref name=NASA_XF92A>{{cite web |url = http://cio.gsfc.nasa.gov/centers/dryden/pdf/121582main_FS-080-DFRC.pdf |title = XF-92A |accessdate =August 26, 2009 |work = NASA Facts |publisher=National Aeronautics and Space administration |page = 2 |format = [[PDF]]}}</ref> In the X-1A, Kit set altitude records of over 90,000 feet<ref name=Crossfield>{{cite book |last=Crossfield |first=A. Scott |authorlink=Albert Scott Crossfield |author2=Blair Jr., Clay |title=Always Another Dawn |publisher=Arno Press |year=1960 |isbn=0-405-03758-9 |page = 184}}</ref> and was considered at the time, 1954, America’s first space pilot. He was the first to see the curvature of the earth and the sky dark at mid-day.<ref name=AWOH/> The X-1A was powered by four rocket motors using liquid oxygen and alcohol as fuel. Looking rather exotic even in photos today, the X-1 used  nitrogen tanks to pressurize many of the systems including the fuel tanks, cockpit and the landing gear system. However, the flight controls were completely conventional with strictly mechanical linkage and no hydraulic boost.

The X-1A was launched from the belly of a B-29 and later a B-50, and the flight profile had him using a 45 degree [[Tait-Bryan angles|pitch]] attitude with airspeeds reaching about [[Mach number|Mach]] 2. On his first couple of high altitude flights, Kit said his plane would snap into a spin when the motors burned out while approaching his peak altitude. He finally figured that the rocket motors were installed very slightly offset which, to keep it going straight, was causing him to have to cross control the plane increasingly as it accelerated. When the engines shut off, the cross-control condition, which was keeping the airplane from [[Yaw angle|yawing]], now became the perfect spin entry input.

After two flights involving supersonic spin recovery, Kit was quick to neutralize the controls immediately upon motor shutdown in later flights. He had taped a string in front of the windshield to determine his rudder trim input. Kit was the first pilot to fly the X1-B aircraft in powered flight, and he said it was a much straighter flying rocket ship than the X-1A. The X-4 he flew was basically a flying wing type aircraft (no horizontal tail) and the X-5 was a variable sweep test platform.

Kit was a test pilot at Muroc/Edwards from 1949 to 1955, an unusually long time for that assignment. Kit’s next Air Force assignment was in Paris, France.<ref name=AWOH>{{cite web|url=http://www.cityoflancasterca.org/Index.aspx?page=204#murray |title=1996 Honorees |accessdate=August 26, 2009 |work=Aerospace Walk of Honor |publisher=City of Lancaster, California |deadurl=yes |archiveurl=https://web.archive.org/web/20100625224129/http://cityoflancasterca.org/Index.aspx?page=204 |archivedate=June 25, 2010 |df= }}</ref> He was in charge of technology integration for the U.S. Regional Organization there and was privileged to fly some of Europe’s top airplanes at the time, including the Italian [[Fiat]] G-91, the French Mystere, and the British [[Gloster Javelin|Javelin]]. After that one-year assignment he went to [[Wright-Patterson Air Force Base]] as head of new developments at the Systems Project Office.

During his time there, 1958–1960, he was Air Force manager for the [[North American X-15|X-15]] program,<ref name=AWOH/> which attained record altitudes of 354,000 feet and a speed record of 4,534 m.p.h. (Mach 6.7). The X-15 program contributed enormously to the [[NASA|space program]] and high speed aircraft research, and was acclaimed as the most successful test program of its type. Kit held the rank of Major at the time, but this was considered a [[Colonel]]’s job. He was approached by [[Boeing]] in 1960. He retired with over 20 years of military service and became Boeing's "company astronaut" managing crew integration for the space program.<ref name=AWOH/> In that capacity he massaged the gap between engineers and scientists who just wanted [[astronaut]]s to ride in a sealed capsule, and pilots who wanted to be able to see what was going on during flights. Kit worked for Boeing on many space program projects from 1960 to 1969, from the [[X-20 Dyna-Soar|X-20]] (a single place space shuttle) to the [[Apollo program]]. He was Technical Integration Manager for Boeing at [[Cape Canaveral]].<ref name=AWOH/>

=== Engineering ===
In 1969 Kit moved to the [[Fort Worth, Texas|Ft. Worth]] area to become Air Force Requirements Engineer for [[Bell Helicopter]] in the tilt rotor program.<ref name=AWOH/> He worked for them until 1971, then gradually slowed down in retirement, but still doing many things interesting to him. He managed a hunting club, flew some charter work for Mustang Aviation in [[Dallas]] then did some courtroom reporting for the [[Bosque County, Texas|Bosque County]] newspaper. Kit also was project manager for the restoration of the Bosque County Courthouse, taking it back to its 1886 splendor.

== Personal life ==
Arthur Murray married Elizabeth Ann (Betty Anne) Strelic in 1943. They had six children, Michael, John, Christopher, Catherine, Patrick, Peter. The family fostered a seventh child, Elizabeth Anne(Betsy) from the time of her birth in 1963 until the couple separated in 1966. Arthur remarried in 1970 to Dallas Interior Designer Ann Humphreys. Ann now lives in Central Texas on a small horse farm. They combined their  efforts and expertise  in renovating the [[Bosque County]] Court House and charter members of TETRA, an equine trail riding Organization.

Kit Murray died on July 25, 2011 at a nursing home in the town of [[West, Texas]] at the age of 92.<ref>{{cite news|last=Gately|first=Paul|title=U.S. Jet Flight Pioneer Test Pilot Dies Here |url=http://www.kwtx.com/home/headlines/US_Jet_Flight_Pioneer_Test_Pilot_Dies_Here.html|accessdate=January 3, 2015|newspaper=KWTX News Channel 10}}</ref>

== Honors ==
Murray was awarded the following decorations for his military service: [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]] and the [[Air Medal]] with eight [[oak leaf cluster]]s.<ref name=AWOH/> He is a Fellow of the [[Society of Experimental Test Pilots]] and recipient of the French Medal of the City of Paris.<ref name=AWOH/> In 1996, Murray was inducted into the [[Aerospace Walk of Honor]] in [[Lancaster, California]] that honors test pilots who have contributed to aviation and space research and development.<ref name=AWOH/>

== References ==
{{reflist|colwidth=30em}}

== External links ==
*[http://www.astronautix.com/astros/murray.htm Murray test flight chronology]
*[http://eaa59.org/publish/profile_Kit_Murry.html Experimental Aircraft Association profile of Kit Murray]
*[http://thetartanterror.blogspot.com/2007/03/arthur-kit-murray.html Kit Murray Biography by the TartanTerror]
*[http://www.af.mil/shared/media/document/AFD-080123-063.pdf Milestones in Aerospace History at Edwards AFB]
*{{cite web |url = http://www.aderholdfuneralhome.com/sitemaker/memsol.cgi?page=profile&section=info&user_id=420373 |title =Arthur Kit Murray Life Legacy |accessdate = July 30, 2011 |publisher=Memorial Solutions |work=Aderhold Funeral Home}}

{{DEFAULTSORT:Murray}}
[[Category:1918 births]]
[[Category:U.S. Air Force Test Pilot School alumni]]
[[Category:2011 deaths]]
[[Category:American test pilots]]
[[Category:Recipients of the Air Medal]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Flight altitude record holders]]