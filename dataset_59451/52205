{{Italic title}}
{{Infobox journal
| title = Macromolecular Chemistry and Physics
| formernames = Die Makromolekulare Chemie/Macromolecular Chemistry
| cover = [[Image:MCP cover1709.jpg]]
| discipline = [[Polymer science]]
| abbreviation = Macromol. Chem. Phys.
| editor = Stefan Spiegel
| publisher = [[Wiley-VCH]]
| country = 
| history = 1947-present
| frequency = Biweekly
| impact = 2.616
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-3935
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-3935/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-3935/issues
| link2-name = Online links
| ISSN = 1022-1352
| eISSN = 1521-3935
| OCLC = 231476857
| LCCN = 94652062
| CODEN = MCHPES
}}
'''''Macromolecular Chemistry and Physics''''' is a biweekly [[Peer review|peer-reviewed]] [[scientific journal]] covering [[polymer science]]. It publishes Full Papers, Talents, Trends, and Highlights in all areas of [[polymer science]], from [[chemistry]] to [[physical chemistry]], [[physics]], and [[materials science]].
 
==History==
''Macromolecular Chemistry and Physics'' was established in 1947 as ''Die Makromolekulare Chemie/Macromolecular Chemistry'' by [[Hermann Staudinger]]<ref name=editorial>{{cite journal |title=As time goes by... |journal=Macromolecular Chemistry and Physics |date=2003-02-18 |first=Ingrid |last=Meisel |volume=2004 |issue=1 |pages=13–14 |doi= 10.1002/macp.200290077}}</ref> and obtained its current title in 1994.<ref name=access>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-3935/issues |title=All Issues |publisher=[[Wiley-VCH]] |work=Macromolecular Chemistry and Physics |accessdate=2013-07-20}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.616.<ref name=WoS>{{cite book |year=2015 |chapter=Macromolecular Chemistry and Physics |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

==See also==
* ''[[Macromolecular Rapid Communications]]'', 1979
* ''[[Macromolecular Theory and Simulations]]'', 1992
* ''[[Macromolecular Materials and Engineering]]'', 2000
* ''[[Macromolecular Bioscience]]'', 2001
* ''[[Macromolecular Reaction Engineering]]'', 2007

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1521-3935}}

[[Category:Physical chemistry journals]]
[[Category:Materials science journals]]
[[Category:Publications established in 1947]]
[[Category:Biweekly journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]