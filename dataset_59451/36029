{{Use dmy dates|date=June 2013}}
{{Use British English|date=June 2013}}
{{good article}}
{{Infobox Christian leader
| type        = Bishop
| name        = Albin
| title       = [[Bishop of Brechin]]
| image       = Albin.jpg
| alt         = 
| caption     = Bishop Albin's seal
| church      = [[Christianity]]
| archdiocese = 
| diocese     = 
| see         = [[Diocese of Brechin]]
| term        = 1246?&ndash;1269
| predecessor = [[Gregory of Brechin|Gregory]] 
| successor   = [[William de Crachin]]
<!--   Orders   -->
| ordination     = 
| ordinated_by   = 
| consecration   = Between 19 July 1246 and 13 May 1247
| consecrated_by = 
| rank           = 
<!--   Personal details   -->
| birth_date    = unknown
| birth_place   = unknown
| death_date    = 1269
| death_place   = 
| previous_post = [[Precentor]] of [[Brechin Cathedral|Brechin]] (left by 1247)
}}

'''Albin''' (or '''Albinus''') (died 1269) was a 13th-century [[prelate]] of the [[Kingdom of Scotland]]. A university graduate, Albin is known for his ecclesiastical career in the [[diocese of Brechin]], centred on [[Angus, Scotland|Angus]] in east-central [[Scotland]]. 

Almost certainly a native of Angus, he appears to be a descendant of [[David of Scotland, Earl of Huntingdon]], brother of King [[William I of Scotland]], through an illegitimate son whom Earl David settled in the area around [[Brechin]].

Albin, himself an illegitimate child, made his career as a churchman in the local diocese, and served for some time as [[precentor]] of [[Brechin Cathedral]] before, in 1246, being elected [[Bishop of Brechin]]. He remained Bishop of Brechin until his death in 1269.

==Biography==
===Background and early career===
Albin's family and origin are unclear. It has been suggested that he may have been a son of [[Henry de Brechin]] (died 1244 or 1245), the bastard son of [[David of Scotland, Earl of Huntingdon|David of Huntingdon]] ("Earl David") and the younger brother of King [[William the Lion]]; Henry bore the title Lord of [[Brechin]], and was given lands there by his father. The strongest evidence of Albin's relationship with Henry is that his episcopal [[Seal (device)|seal]] bore the arms of Earl David.<ref>See references in first paragraph of Watt, ''Dictionary'', s.v. "Albin", p.&nbsp;5, ch. 1.</ref> 
[[File:Alexander III and Ollamh Rígh.JPG|thumb|left|Late medieval depiction of the coronation of Alexander III, King of the Scots; Albin was presumably present, though Brechin was not one of the senior bishoprics of the kingdom.]]
Albin's family certainly had strong connections in the church of Brechin. A known kinsman (''nepos'') of Albin's, Adam, held the position of [[Archdeacon of Brechin]], probably by 1242, but certainly by 1264.<ref>Watt, ''Dictionary'', p. 6; understanding of possession of the archdeaconry in this period is complicated, as the archdeacon called Adam in 1242 may not be the ''nepos'' Adam of 1264; see Watt, ''Fasti Ecclesiae'', p. 54.</ref> It has been suggested that this Adam was Adam de Brechin, probable son of Henry de Brechin's successor William de Brechin (died  between 1286 and 1294), who held  [[benefice]]s in the see of Brechin in 1274.<ref name=Watt-6>Watt, ''Dictionary'', p.&nbsp;6.</ref>

Albin became [[precentor]] of [[Brechin Cathedral]], the first known person to hold that office; he was not, however, recorded in that position until the summer of 1246, when he was confirmed as [[Bishop of Brechin]].<ref>Watt, ''Dictionary'', p. 6; Watt, ''Fasti Ecclesiae'', p. 45.</ref> Because Albin was born illegitimately, he needed [[Dispensation (Catholic Church)|papal dispensation]] to hold that office, which he obtained from the [[papal legate]] [[Otto of Tonengo]], [[Bishop of Porto]], in the autumn or early winter of 1239, when that legate visited Scotland.<ref>Dowden, ''Bishops'', p. 175; Watt, ''Dictionary'', pp.&nbsp;5&ndash;6.</ref> 

By 1246, Albin was styled "Master", indicating that he had completed many years of university study; what he studied, and at which university, is unknown.<ref>Watt, ''Dictionary'', p.&nbsp;5.</ref>{{clear}}

===Accession to Brechin bishopric===
Albin became Bishop of Brechin following an election and then a successful appeal for confirmation to the papacy. [[Pope Innocent IV]]'s mandate for confirmation gave the details of the election. Following the death of [[Gregory of Brechin|Gregory]], Bishop of Brechin, the [[cathedral chapter]] selected three of their members to elect the next bishop, and they unanimously forwarded their precentor, Albin. Because of Albin's "defect of birth" (i.e. his [[illegitimacy]]), they supplicated the papacy to repeat the earlier dispensation.<ref name=Dow175-Watt6>Dowden, ''Bishops'', p.&nbsp;175; Watt, ''Dictionary'', p. 6.</ref>  

The Pope followed legate Otto's earlier dispensation, and on 19 July 1246 issued the mandate for confirmation and consecration to the [[Kingdom of Scotland]]'s three senior bishops: [[David de Bernham]], [[Bishop of St Andrews]]; [[William de Bondington]], [[Bishop of Glasgow]]; and [[Geoffrey de Liberatione]], [[Bishop of Dunkeld]].<ref name=Dow175-Watt6/> The consecration took place some time before 13 May 1247, the date Albin was given his first recorded task as a consecrated bishop, when he, [[Clement of Dunblane|Clement]], [[Bishop of Dunblane]], and David de Bernham, Bishop of St Andrews, were authorised to perform the episcopal consecration of [[Peter de Ramsay]] as [[Bishop of Aberdeen]].<ref>Watt, ''Dictionary'', p.&nbsp;6; Watt, ''Fasti Ecclesiae'', p. 39.</ref>

===Early episcopate, 1240s===
Albin witnessed a royal [[charter]] at [[Forfar]] on 4 July 1246.<ref name=Dow175-Watt6/> In the following year, on 11 July and 8 August 1248, the Pope wrote to Albin commanding him to ensure that a settlement between [[Inchaffray Abbey]] in [[Strathearn]], [[diocese of Dunblane]], and Bishop Clement of Dunblane be peacefully kept, with Inchaffray being portrayed as the side in more need of protection.<ref>Lindsay, Dowden & Thomson (eds.), ''Charters of Inchaffray'', nos. 78 & 79; Watt, ''Dictionary'', p.&nbsp;6.</ref> 

In the same period, he ordained [[vicarage]]s for [[Arbroath Abbey]] in Angus, and on 22 September 1248 settled a long-standing property dispute between the church of Brechin and Arbroath Abbey.<ref>Dowden, ''Bishops'', pp.&nbsp;175&ndash;6; Watt, ''Dictionary'', p.&nbsp;6.</ref> However, he had left Scotland by the following month, and was in [[Kingdom of England|England]], at [[Finchale Priory]] near [[Durham, England|Durham]], where he granted many [[indulgence]]s.<ref name=Dow176-Watt6>Dowden, ''Bishops'', p. 176; Watt, ''Dictionary'', p.&nbsp;6.</ref> 

Following the death of [[Alexander II of Scotland]] on 6 July 1249, the accession of the boy king, [[Alexander III of Scotland]], meant [[Minor (law)|minority]] administration, and as a result, factional politics. The government during this minority was divided between a faction centred on [[Walter Comyn, jure uxoris Earl of Menteith|Walter Comyn]], husband of [[Mary I, Countess of Menteith|the countess of Menteith]], and [[Alan Durward]]; the Comyn faction held the ascendancy between 1249 and 1252, and again between 1255 and 1257; the Durward faction held the ascendancy between 1252 and 1255, and again between 1257 and 1258.<ref>See Hammond, "Durward Family", pp.&nbsp;118&ndash;38, and Young, "Political Role of Walter Comyn", pp.&nbsp;131&ndash;49, for details.</ref> 

The available sources give no clue as to Albin's pattern of allegiance in these factional politics, and neither do his recorded activities. Professor [[D. E. R. Watt|Donald Watt]] has suggested that Albin was probably aligned with the Durward faction, in contrast, for instance, to the allegedly Comyn aligned Clement of Dunblane.<ref>Watt, ''Dictionary'',&nbsp;pp. 5&ndash;7.</ref> Watt even argued that Albin's postulation was probably due to the influence of Alan Durward, who at the time was a close advisor of King Alexander II.<ref name=Watt-6/>

===Middle episcopate, 1250s===
[[File:Brechin, Cathedral and Round Tower.jpg|thumb|Modern photograph of Brechin Cathedral and round tower.]]
Despite Albin's suggested Durwardite allegiance, Bishop Albin, Bishop David de Bernham of St Andrews and [[Abel de Gullane]], [[Archdeacon of St Andrews]], issued a letter of protest against the behaviour of the Durward dominated government; they criticised Durward's onslaught on the "liberties of the church", probably in the aftermath of the [[translation (relics)|translation]] of the relics of [[Saint Margaret of Scotland|St Margaret]] to [[Dunfermline Abbey]] on 19 June 1250.<ref name=Watt-6/>

In either 1253 or 1254, Albin was an [[assessor (law)|assessor]] at a court held by the [[Justiciar of Scotia]], [[Alexander Comyn, Earl of Buchan]].<ref name=Watt-6/> In April 1253, he summoned Bishop David de Bernham to appear before the papal curia, in order to resolve a dispute he and the [[culdee]]s of St Mary's were having with [[St Andrews Cathedral Priory]].<ref name=Watt-6/> 

Albin performed various tasks in this period on behalf of the papacy. On 15 May 1253, Bishop Albin and [[Richard de Inverkeithing]], Bishop of Dunkeld, were appointed to be papal mandatories, and instructed by the papacy to protect Bishop William de Bondington from being summoned to lay courts on account of matters concerning his bishopric.<ref name=Watt-6/>

Bishop Albin, with the Archdeacon of Brechin, was named as a papal mandatory again on 4 January 1254 and authorised to put Nicholas de Hedon in possession of the [[deanery]] of [[Elgin Cathedral]].<ref name=Watt-6/> Along with Clement of Dunblane, Albin was named by the pope as a conservator of the privileges given to Abel de Gullane, newly provided Bishop of St Andrews, on 23 March.<ref>Watt, ''Dictionary'', pp.&nbsp;6&ndash;7.</ref> 

On 22 June, Bishop Albin assisted the justiciar Alexander Comyn in conducting a [[Beating the bounds|perambulation]] in eastern Angus.<ref name=Watt-7>Watt, ''Dictionary'', p.&nbsp;7.</ref> Albin appears to have left Scotland again some time after this, as he appears active around Durham again in either 1254 or 1255.<ref name=Watt-7/> Back in Scotland, at Arbroath on 21 September 1256, he and Bishop Clement of Dunblane passed judgment on William de Mydford, vicar of the [[parish church]] of [[Dundee]], after Mydford had been withholding the revenues due to the church's [[Rector (ecclesiastical)|rector]], [[Lindores Abbey]].<ref name=Watt-7/> 

During this period, Albin was involved in a political controversy regarding succession to an [[earldom]]. An alleged papal bull, dated 13 December 1255, had named Bishop Albin as a papal mandatory, along with [[Robert de Stuteville]], with instructions to investigate Alan Durward's claim to the [[earldom of Mar]]; however, the bull was denounced as a forgery on 28 March 1257, after Durward's rivals had seized power.<ref name=Watt-6/> During the months preceding this denunciation, Albin was once again out of Scotland; on 4 March 1257 his presence was once again recorded at Durham.<ref name=Watt-7/>

===Later episcopate, 1260s===
For three years, Albin's activities are unreported until, on 30 April 1260, he was recorded as being at [[Montrose, Angus|Montrose]], again as a papal mandatory. He gave judgment on a dispute between [[Archibald (d. 1298)|Archibald]], [[Bishop of Moray]], and the latter's cathedral chapter, concerning the bishop's rights of [[Canonical visitation|visitation]].<ref name=Watt-7/> On 13 June 1263, Albin, [[Roger (Bishop of Ross)|Roger]], [[Bishop of Ross (Scotland)|Bishop of Ross]], and Richard de Inverkeithing, Bishop of Dunkeld, were selected by the papacy to judge the fitness and, if appropriate, consecrate [[Walter de Baltrodin]] as [[Bishop of Caithness]].<ref>Dowden, ''Bishops'', p.&nbsp;226; Watt, ''Dictionary'', p.&nbsp;7.</ref>

In the following year, Albin was involved in a controversy regarding the archdeaconry of Brechin. On 23 January 1364, [[papal judge-delegate]]s were appointed to investigate allegations of [[nepotism]] which had been made against him. The allegations centred on Bishop Albin's handling of a vacancy to the Brechin archdeaconry; Albin had given [[collation]] of the archdeaconry to the [[Abbot of Arbroath]], who then appointed Adam, one of Albin's relatives.<ref name=Watt-7/>  The outcome of the case is not known, and it is therefore unclear whether or not Adam was deposed as archdeacon.<ref name=Watt-7/> All that can be confirmed is that no other archdeacon is attested by name until 1284.<ref>Watt, Dictionary, p.&nbsp;7; Watt, ''Fasti Ecclesiae'', p. 54.</ref>

The stay of Cardinal Ottobono Fieschi (later [[Pope Adrian V]]) in England from 29 October 1265, until July 1268, led to a great deal of diplomatic activity in the Scottish church, in which Albin was perhaps involved.<ref>Watt, ''Medieval Church Councils'', p. 91; the 19th century ''Biographical Dictionary of the Society for the Diffusion of Useful Knowledge'' attributes to Albin a role not confirmed by other sources.</ref> Cardinal Ottobono imposed a general tax on the English church, which he extended to Scotland. King Alexander III forbade the payment of this money, and appealed to the papacy; but in 1267 the Scottish clergy persuaded Alexander to abandon his appeal, while they made their own reduced payments.<ref>Watt, ''Medieval Church Councils'', p.&nbsp;91-2.</ref> Meanwhile, Cardinal Ottobono's legatine council, to which the Scottish church had sent four delegates, promulgated [[canon law|canons]] for the English church which he expected the Scottish church to incorporate; the Scottish church does not appear to have done so, however.<ref>Watt, ''Medieval Church Councils'', pp.&nbsp;93-4;</ref>

Bishop Albin witnessed William de Brechin's foundation charter of [[Maison Dieu, Brechin|Maison Dieu]] chapel, Brechin, sometime between March and July 1267,<ref name=Watt-7/> the last known recorded reference to him until 1269. His death in that year was reported in the ''[[Chronicle of Melrose]]'', but without any details of the cause, or location, or Albin's age.<ref>Anderson, ''Early Sources'', vol. ii, p.&nbsp;663.</ref>

===General notes about Albin's episcopate===
During Albin's episcopate, the incorporation of the ''[[Céli Dé]]'' ("vassal of God") into the cathedral chapter was probably brought near to completion. The ''Céli Dé'' were the Scottish monks who formed the base of the pre-[[Gregorian Reform|Reform]], pre-13th century monastery of Brechin before it was organised into a bishopric in the 12th century. A bull of Pope Innocent IV of 18 February 1250 stated that:{{quote|The brethren who have been wont to be in the church of Brechin were called ''Keledei'' and now by change of name are styled canons<ref>Cowan & Easson, ''Medieval Religious Houses'', pp. 47, 203.</ref>}} These ''Céli Dé'' had been proclaimed as part of the secular cathedral by an act of Bishop Gregory, Albin's predecessor.<ref>Cowan & Easson, ''Medieval Religious Houses'', p.&nbsp;203.</ref>

During his episcopate, Albin is said to have attracted Egbert, an [[English people|English]] [[Arabic]] scholar and [[Carmelite]] [[friar]], to teach in Brechin.<ref>Watt, ''Dictionary'', pp. 5, 7.</ref>  A later tradition held that a now obscure local [[martyr]] named [[Stolbrand]], "martyr of Brechin", had been translated to Brechin Cathedral during Albin's episcopate; the date given is 2 January but the year is not recorded.<ref name=Watt-7/>

==Notes==
{{reflist|2}}

==References==
{{refbegin}}
* [[Alan Orr Anderson|Anderson, Alan Orr]], ''Early Sources of Scottish History: AD 500–1286'', 2 Vols, (Edinburgh, 1922)
* Cowan, Ian B. & Easson, David E., ''Medieval Religious Houses: Scotland <small>With an Appendix on the Houses in the Isle of Man</small>'', Second Edition, (London, 1976)
* [[John Dowden|Dowden, John]], ''The Bishops of Scotland'', ed. J. Maitland Thomson, (Glasgow, 1912)
* Hammond, Matthew H., "The Durward family in the thirteenth century", in [[Steve Boardman (historian)|Steve Boardman]] & Alasdair Ross (eds.), ''The Exercise of Power in Medieval Scotland, c.1200–1500'', (Dublin, 2003), pp.&nbsp;118–38
* Lindsay, William Alexander, Dowden, John, & Thomson, J. Maitland (eds.), ''Charters, bulls and other documents relating to the Abbey of Inchaffray, chiefly from the originals in the charter chest of the Earl of Kinnoull'', (Publications of the Scottish History Society ; v. 56; Edinburgh, 1908)
* Stanesby, John Tatam, [https://books.google.com/books?id=c14MAAAAYAAJ&printsec=titlepage&dq=albin+of+brechin "Albin"], in ''[https://books.google.com/books?id=c14MAAAAYAAJ&pg=PA435&dq= The Biographical Dictionary of the Society for the Diffusion of Useful Knowledge, Vol. 1, Part 1]'', (London, 1842), p.&nbsp;702
* [[D. E. R. Watt|Watt, D. E. R.]], ''A Biographical Dictionary of Scottish Graduates to A. D. 1410'', (Oxford, 1977)
* Watt, D. E. R., ''Fasti Ecclesiae Scotinanae Medii Aevi ad annum 1638'', 2nd Draft, (St Andrews, 1969)
* Watt, D. E. R., ''Medieval Church Councils in Scotland'', (Edinburgh, 2000)
* Young, Alan, "The Political Role of Walter Comyn, Earl of Menteith During the Minority of Alexander III of Scotland", in K. J. Stringer (ed.), ''Essays on the nobility of medieval Scotland'', (Edinburgh, 1985), pp.&nbsp;131–49
{{refend}}

{{s-start}}
{{s-rel}}
{{succession box |
before= [[Gregory of Brechin|Gregory]] |
title=[[Bishop of Brechin]] |
years= 1246&ndash;1269 |
after= [[William de Crachin]]}}
{{s-end}}

{{DEFAULTSORT:Albin Of Brechin}}
[[Category:1269 deaths]]
[[Category:13th-century Roman Catholic bishops]]
[[Category:Bishops of Brechin]]
[[Category:People from Angus]]
[[Category:Year of birth unknown]]