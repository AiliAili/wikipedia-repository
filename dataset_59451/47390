{{POV|date=February 2017}}
{{advert|date=February 2017}}
{{Use dmy dates|date=April 2014}}
{{more footnotes|date=September 2013}}
{{Infobox person
|name        = Sankar Datta
|image       = Sankar Datta Teaching.jpg
|caption     =
|birth_date  = {{birth date and age|mf=yes|1958|02|17}}
|birth_place = [[Jamshedpur]], [[Bihar]], [[India]]
|spouse      = Sonja Datta
|children    = Soumya Sankar
|known_for   = [[PRADAN]], [[BASIX (India)]], [[The Livelihood School]], [[Microfinance]], [[Livelihood]]
|occupation  = [[Development Professional]], [[Academic]], [[Professor]]
}}

Dr. '''Sankar Datta'''<ref name="Sankar Datta">{{cite web|title=Sankar Datta|url=http://azimpremjiuniversity.edu.in/sankar-datta|publisher=Azim Premji University|accessdate=2 July 2013}}</ref> (born February 17, 1958), is an [[India]]n academic & professional development worker. He has been engaged in rural livelihood promotion / support activities for more than three decades since the early 1980s. Most of his field work has been in the undulating terrains of central India, from Chhota Udaipur in the west to [[Jharkhand]] in the east inhabited by various tribal groups.

Datta is a management Graduate from the very first batch of [[Institute of Rural Management Anand]],Gujarat,<ref>{{cite web|last=Datta|first=Sankar|title=Author information|url=http://swb.co.in/author-info/7039|work=http://swb.co.in/|publisher=Scholars without Borders|accessdate=2 July 2013}}</ref> with graduation in Agriculture from [[G. B. Pant University of Agriculture and Technology]] and Ph.D in [[Economics]] from [[Sardar Patel University]].

== Background ==

Datta is currently Development Evangelist, well known for his work in the field on livelihood support, as a part of institutions like PRADAN and  BASIX. He  was a Professor and Head in the [[Azim Premji University]], Bengaluru,<ref name="Sankar Datta"/> and leading the Livelihood Initiative of the University as well as a Member of the Favculty of Institute of Rural Management, Anand (IRMA) and Indian Institute of Management-Ahmedabad (IIM-A).
Having started his journey as a Spearhead Team Leader organizing soyabean farmers in central parts of India, he continued working with small rural producers as part of the founding teams of [[BASIX (India)|BASIX]]/ Indian Grameen Services<ref>{{cite web|last=Datta|first=Sankar|title=IGS eyes ‘poorer’ terrains to meet micro finance targets|url=http://financialexpress.com/news/IGS-eyes--poorer--terrains-to-meet-micro-finance-targets/241666|publisher=The Financial Express|accessdate=2 July 2013}}</ref><ref>{{cite web|last=Datta|first=Sankar|title=India: Indian Grameen Services (IGS) Eyes ‘Poorer’ to Meet Microfinance Targets|url=http://microcapital.org/news-wire-india-indian-grameen-services-igs-eyes-%E2%80%98poorer%E2%80%99-to-meet-microfinance-targets/|publisher=MicroCapital|accessdate=2 July 2013}}</ref> along with [[Vijay Mahajan]], [[Deep Joshi]], and [[PRADAN]], two notable livelihood promotion and support institutions of India before joining the University.<ref>{{cite web|last=Datta|first=Sankar|title=Welcome to Indian Grameen Services|url=http://igsindia.org.in/|work=http://igsindia.org.in/|publisher=Indian Grameen Services}}</ref>

He was also the member of faculty of premier Institutions of India such as [[Institute of Rural Management Anand]],<ref>{{cite journal |last=IRMA |journal=NETWORK |date=December 1998 |volume=4 |series=4 |issue=70 |page=8 |title=Editorial |url=https://www.irma.ac.in/pdf/network_issue/70.PDF}}</ref><ref>{{cite web|title=Factors Affecting Performance Of Village Level Organisations|url=http://www.irma.ac.in/researchandpublication/pub_catalogue_download.php?rp_category_id=2|work=INSTITUTE OF RURAL MANAGEMENT ANAND Research and Publication/ Catalogue / Working Papers|publisher=IRMA|accessdate=2 July 2013}}</ref> as well as policy forums such as Working Group of the [[Planning Commission (India)]] for the [[12th Five-Year Plan (India)]].<ref name="planningcommission.nic.in">{{cite web |title=Working Group on Mahatma Gandhi National Rural Employment Guarantee Act (MNREGA) |url=http://planningcommission.nic.in/aboutus/committee/wrkgrp12/rd/wg_mgrega.pdf |publisher=Planning Commission ( Rural Development Division) |deadurl=no |archiveurl=https://web.archive.org/web/20111013132248/http://planningcommission.nic.in/aboutus/committee/wrkgrp12/rd/wg_mgrega.pdf |archivedate=2011-10-13 |accessdate=22 April 2014}}</ref>

Datta is known in India’s development domain for his work in the area of Livelihoods.<ref>{{cite web|title=SPEAKERS-SANKAR DATTA|url=http://financialeducationsummit.org/2007/speakerinfo.php?profile=datta|work=CITI-FINANCIAL EDUCATIONAL SUMMIT|publisher=http://financialeducationsummit.org/|accessdate=2 July 2013}}</ref><ref>{{cite web |title=Land for Infrastructure Development: Livelihoods and Fair Compensation |url=http://iitk.ac.in/3inetwork/html/reports/IIR2009/IIR_2009_Final_July%2009.pdf|work=INDIA INFRASTRUCTURE REPORT 2009|publisher=Oxford University Press |author1=Datta, Sankar|authorlink=Land for Infrastructure Development: Livelihoods and Fair Compensation|author2=Mahajan,Vijay|author3=Singha,Ashok Kumar |archivedate=2012-04-11 |archiveurl=https://web.archive.org/web/20120411082053/http://idfc.com/pdf/report/IIR-2009.pdf|pages=131–141|year=2009 |deadurl=no |accessdate=22 April 2014}}</ref>

== Early life ==

Sankar Datta was (born February 17, 1958) in Jamshedpur, Bihar, India. He spent his early childhood in [[Bihar]]. He did his schooling from [[Patha Bhavana]], Santiniketan.

== Academic Background ==

Datta's early school education was done at [[Visva-Bharati University|Patha Bhavana, Viswa Bharati, Santiniketan]], an institution founded by Rabindranath Tagore.
He completed his post-graduation in [[Rural Management]] from [[Institute of Rural Management Anand]], after completing his graduation in Agriculture (B Sc Ag & AH) from [[G. B. Pant University of Agriculture and Technology]], and did his [[Doctor of Philosophy|Ph.D]] on Factors Affecting Performance of Village Level Organisations in the area of Economics from [[Sardar Patel University]]. Thereafter he pursued higher studies in [[Agribusiness]] management from the Cornell University.

== Professional career ==

=== As an academic ===

*Datta is presently Professor, leading the Livelihood Initiatives.<ref name="Sankar Datta"/>
*He was earlier the Dean of [http://thelivelihoodschool.in/ The Livelihood School] set up by [[BASIX (India)|BASIX]], undertaking research and training in promotion of livelihoods, especially of the poor households of the country;
*He was a member of the faculty in [https://www.irma.ac.in/ Institute of Rural Management, Anand (IRMA)] and Ravi Mathai Centre for Educational Innovation (RM-CEI) of [[Indian Institute of Management Ahmedabad]].
*He was also visiting faculty for several institutions like XIMB, MANAGE, KIIT-SRM, TISS among others

=== As a Practitioner/Development Professional ===
Datta has been closely associated with promotion of Rural Livelihoods as a Practitioner.
* He was in-charge of operations while setting up the [[BASIX (India)|BASIX]] group of Companies, a new generation livelihood support institution<ref>{{cite web|title=Sankar Datta|url=http://basixindia.com/index.php?option=com_content&task=view&id=115&Itemid=138|work=BASIX India Website|accessdate=2 July 2013}}</ref>
* He had initiated and headed the first livelihood project of [http://www.pradan.net/ PRADAN], a major livelihood support NGO of India
* He was Team-Leader of the Spearhead Team responsible for promotion of soybean in MP and organizing the network of producer co-operatives

== Other Professional Engagements ==
* Served on the Board of several institutions<ref>{{cite web|title=Board Members- Mother Earth|url=http://motherearth.co.in/about-us/board-members/|website=motherearth.co.in|accessdate=2 July 2013}}</ref>  including Indian Grameen Services,<ref>{{cite web|last=Datta|first=Sankar|title=Board Members - Indian Grameen Services|url=http://igsindia.org.in/about-us/board-members|website=igsindia.org.in/|publisher=Indian Grameen Services|accessdate=2 July 2013}}</ref> Village Financial Services Ltd,<ref>{{cite web|last=Datta|first=Sankar|title=Board of Directors|url=http://www.village.net.in/board_of_directors/|work=http://village.net.in/|publisher=Village Financial Services|accessdate=2 July 2013}}</ref> IndusTree Crafts Ltd. Purbanchal Maitree Development Society.<ref>{{cite web|last=Datta|first=Sankar|title=Board of Director|url=http://www.mymaitri.org/our_board.html|work=Maitri|accessdate=2 July 2013}}</ref>
* Member of the Advisory Committee of Centre for Management in Agriculture (CMA) of the IIM-Ahmedabad
* Member of the Livelihood Advisory Group of the Sustainable Livelihoods India Initiative organized by ACCESS Development Services.
* Member of the Planning Commission Working Group for the 12th Five-Year Plan on MGNREGA.<ref name="planningcommission.nic.in" />

== Publications ==

Apart from presenting various papers<ref>{{cite web|title=Sankar Datta -Publications and Writings |url=http://azimpremjiuniversity.edu.in/pdf/sankardattapublications.pdf |publisher=Azim Premji University|accessdate=2 July 2013}}</ref><ref>{{cite journal |last=Datta|first=Sankar|title=Power, Politics and Village-Level Organisations: Oilseeds Growers' Co-operatives in MP|journal=Economic and Political Weekly| date=October 1997 |volume=32 |issue =39 (September 27 - October 3, 1997)|pages=A114-A120 |url=http://www.epw.in/review-agriculture/power-politics-and-village-level-organisations-oilseeds-growers-co-operatives-mp. |deadurl=no |accessdate=22 April 2014}}</ref><ref>{{cite journal |last=Datta|first=Sankar|author2=N. Srinivasan|title=Consolidating the Growth of Microfinance Microfinance India: State of the Sector Report 2008 by N. Srinivasan Review by: Sankar Datta|journal=Economic and Political Weekly|date=July 2008|volume= 44|series=No. 30|pages=31–32|jstor=40279309}}</ref><ref>{{cite web|title=Books- Lead Author - Sankar Datta|url=http://www.sagepub.in/authorDetails.nav?contribId=658846|website=www.sagepub.in|publisher=SAGE Publications India Pvt. Ltd.|accessdate=2 July 2013}}</ref><ref>{{cite web|title=Rural Livelihood in Andhra Pradesh|url=http://www.apmas.org/pdf/Acknowledgement.pdf|work=Mahila Abhivruddhi Society, Andhra Pradesh (APMAS) and Indian School of Livelihood Promotion|publisher=Mahila Abivruddhi Society, Andhra Pradesh (APMAS),|accessdate=2 July 2013|author=Edited by Dr. S.Srinivas, APMAS,Dr. Sankar Datta, ISLP and Prof. S.J.Phansalkar|date=January 2006}}</ref>  on the livelihood challenges faced by the poor in various national and international forums he has published several books.<ref>{{cite web |title=Savings of the Poor: How they do it..|url=http://www.amazon.com/Savings-Poor-How-they/dp/8182910870/ref=sr_1_5?s=books&ie=UTF8&qid=1372762892&sr=1-5|work=www.amazon.com|author=Sankar Datta|author2=S L Narayana, Sirnivas |archiveurl=https://web.archive.org/web/20140422153751/http://www.amazon.com/Savings-Poor-How-they/dp/8182910870/ref=sr_1_5?s=books&ie=UTF8&qid=1372762892&sr=1-5 |date=23 December 2010 |deadurl=no |archivedate=2014-04-22 |accessdate=22 April 2014}}</ref> One of his frequently used publications is the [http://www.ruralfinance.org/library/policy-advice/livelihood-promotion/livelihood-promotion-details/ru/?no_cache=1&srec=10382&tdet=training&tdet2=&tdet3=2&referer=MTA1MjU%3D Resource Book for Livelihood Promotion<ref>{{cite web|last=Datta|first=Sankar|title=A resource book for livelihood promotion|url=http://www.amazon.com/resource-book-livelihood-promotion/dp/B007HGSTYE/ref=sr_1_1?s=books&ie=UTF8&qid=1372762892&sr=1-1|work=http://www.amazon.com/|publisher=Ford Foundation|accessdate=2 July 2013}}</ref> ],<ref>{{cite web|title=An Introduction to Livelihood Promotion|url=http://www.ruralfinance.org/library/policy-advice/livelihood-promotion/livelihood-promotion-details/ru/?no_cache=1&srec=10382&tdet=training&tdet2=&tdet3=2&referer=MTA1MjU%3D|work=http://www.ruralfinance.org/|publisher=http://www.ruralfinance.org/|accessdate=28 June 2013}}</ref> co-authored with [[Vijay Mahajan]] and Gitali Thakur.
He has also been involved in designing and editing the first five years of the State of India's Livelihood Report.<ref>http://www.accessdev.org/downloads/the_soil_report_2008.pdf</ref>

== Expertise ==

*Rural Development in General and livelihood promotion & support in specific.<ref>{{cite news|title=Sankar Datta: Face to face with Sarpanches|url=http://www.business-standard.com/article/opinion/sankar-datta-face-to-face-with-sarpanches-108113001027_1.html|accessdate=2 July 2013|newspaper=Business Standard|date=November 30, 2008}}</ref>  
*Dryland farming and Value Chain Management<ref>{{cite web|last=Datta|first=Sankar|title=Sankar Datta on the need for integrated financial services|url=http://microlinks.kdid.org/learning-marketplace/blogs/2011-seep-annual-conference-sankar-datta-need-integrated-financial-servic#sthash.C2ssYf9q.dpuf|work=http://microlinks.kdid.org/|publisher=Microlinks|accessdate=2 July 2013}}</ref>
*Gandhian economics for supporting livelihoods
*Value Chain Development

== References ==
{{Reflist|30em}}

== External links ==
*http://azimpremjiuniversity.edu.in/sankar-datta
*http://jstor.org/stable/4405894
*http://jstor.org/stable/40279309
*http://epw.in/authors/sankar-datta
*http://in.linkedin.com/in/sankardatta
*http://sagepub.in/authorDetails.nav?contribId=658846
*http://financialeducationsummit.org/2007/speakerinfo.php?profile=datta
*http://microlinks.kdid.org/learning-marketplace/blogs/2011-seep-annual-conference-sankar-datta-need-integrated-financial-servic
*http://69.89.31.196/~basixind/index.php?option=com_content&task=view&id=115&Itemid=138
*http://village.net.in/board_of_directors/
*http://igsindia.org.in/about-us/board-members
*https://openknowledge.worldbank.org/bitstream/handle/10986/12793/701640ESW0P1250ings000LowResolution.txt?sequence=2
*http://dscindia.org/UserFiles/Downloads/DSC-LARA-2-Livelihood-010411.pdf?typ..
*http://indiamicrofinance.com/sankar-datta-livelihood-school.html
*http://mymaitri.org/our_board.html
*http://sagepub.in/books/Book238935?prodTypes=any&seriesId=Series2195&sortBy=defaultPubDate%20desc&fs=1
*http://uk.sagepub.com/books/Book238935?seriesId=Series2195&fs=1
*http://swb.co.in/author-info/7039
*http://accessdev.org/downloads/soil_report_2010.pdf
*http://mainumby.org.bo/wp-content/uploads/2011/01/Bangalore-meeting-report-FINAL.pdf
*http://pubs.iied.org/pdfs/G02735.pdf
*http://azimpremjiuniversity.edu.in/talk-apu-community-university-aesthetic-rabindranath-pedagogy-and-festival
*https://www.youtube.com/watch?v=E3bfMf21c14
*http://indiadevelopmentblog.com/2009/01/almost-live-blogging-basix-and-sks.html
*http://aajeevika.gov.in/studies/pradan-publications/wpc-report.pdf
*http://dscindia.org/UserFiles/Downloads/DSC-LARA-2-Livelihood-010411.pdf?typ..
*http://lawbooks.com.au/book-search/search.do?authorName=%22Sankar%20Datta%20(Livelihood%20School%20and%20Indian%20Grameen%20Services,%20Hyderabad)%22&txtQuery=%22Sankar%20Datta%20(Livelihood%20School%20and%20Indian%20Grameen%20Services,%20Hyderabad)%22&searchBy=author

{{DEFAULTSORT:Datta, Sankar}}
[[Category:1958 births]]
[[Category:Living people]]
[[Category:Bengali people]]
[[Category:People from Jamshedpur]]