{{Infobox journal
| title = Books in Canada
| cover = [[file:Books in Canada cover October 1971.png]]
| caption = October 1971 cover
| editor = [[Olga Stein]]
| discipline = Literary criticism
| peer-reviewed = 
| language = English
| former_names = 
| abbreviation = 
| publisher = The Canadian Review of Books Ltd.
| country = Canada
| frequency = 9 issues per year
| history = 1971–2008
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.booksincanada.com/home.asp
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC  = 421761698
| LCCN  = 
| CODEN = 
| ISSN  = 0045-2564
| eISSN = 
| boxwidth = 
}}
'''''Books in Canada''''' was a monthly magazine that reviewed Canadian literature, published in print form between 1971 and 2008. In its heyday it was the most influential literary magazine in Canada.

==Foundation==
One of the co-founders of ''Books in Canada'' in 1971 was the radio producer, book publisher and jazz music columnist Val Clery (1924–1996).{{sfn|Clery, Val, University of Toronto}}
He decided the magazine was needed after writing a report for the Canadian Book Publishers' Council on promotion of books in Canada, and was the first editor of the magazine.{{sfn|Woodcock|2009}}
The journal received subsidies from the Canadian government.{{sfn|Ariss|2011}}
It was published by Bedford House Publishing Corp.{{sfn|Woodcock|2009}}

==Contents==
''Books in Canada''  included reviews of Canadian poetry, literature and non-fiction books.
Authors such as [[Margaret Atwood]], [[Margaret Laurence]], [[bpNichol]] and [[Michael Ondaatje]] contributed reviews.{{sfn|Books in Canada, Collectors' Additions Canada}}
It also included interviews with authors and profiles of authors, and other topics.{{sfn|Woodcock|2009}}
The author, journalist and second-hand bookstore owner Donald Herbert Bell (1936–2004) wrote a column called "Founde Bookes" that described his activity locating and selling books.{{sfn|Don Bell fonds, Concordia University}}

==History==
''Books in Canada'' was the most comprehensive book review journal in the 1980s and early 1990s, giving a broad overview of the Canadian literary scene that was valued by writers who wanted to keep in touch.{{sfn|Henighan|2002|p=166-167}}
''Books in Canada'' appeared nine times per year. It was sold in book stores and newsstands across the country, and by subscription.
Circulation was about 12,000 per issue.{{sfn|Woodcock|2009}}
According to [[Stephen Henighan]] it was sometimes facile, and sometimes paid too much attention to activity in Toronto, but it published reviews by freelancers from across the country and covered almost all Canadian fiction and poetry publications. The magazine was readable, light and journalistic.{{sfn|Henighan|2002|p=167}}

''Books in Canada''  was acquired by Adrian Stein in 1995.{{sfn|Adams|2008}}
The magazine was published through Stein's company named The Canadian Review of Books Ltd.{{sfn|Books in Canada, Collectors' Additions Canada}}
Olga Stein, wife of Adrian Stein, edited the journal.{{sfn|Ariss|2011}}
The Steins converted ''Books in Canada'' to a new format.
Henighan suggests that it became more selective about what it chose to review, ignoring important works in favor of obscure academic books and lengthy opinion columns. 
Circulation dropped sharply.{{sfn|Henighan|2002|p=167}}
Early in 2000 the journal suspended publication.{{sfn|Adams|2008}}

In 2001 Adrian Stein made a deal with [[Amazon.com]], an online book retailer, which generated some controversy.{{sfn|Ariss|2011}}
Stein sold Amazon the electronic rights to old reviews, while Amazon provided cash and advertising to the journal, which resumed publishing in July 2001.{{sfn|Adams|2008}} With the new Amazon arrangement, print circulation increased dramatically to 100,000 copies per issue while subscriptions remained the same. Amazon offered every Canadian customer a complimentary copy of the publication in the mail.{{fact|date=October 2014}}

In its last years ''Books in Canada'' was sometimes narrowly focused on Toronto, sometimes attempted to address the global market.{{sfn|Henighan|2002|p=168}}
The [[Canada Council for the Arts]], [[Ontario Arts Council]] and [[Canadian Heritage]] Magazine Fund reduced or dropped funding.{{sfn|Adams|2008}}
The magazine published an extensive defense of the Canadian newspaper publisher [[Conrad Black]] before his trial for fraud in the U.S.{{sfn|Ariss|2011}}
The January/February 2008 issue had an article titled "The Knotted Knickers of Naomi Klein" with a caricature of the author.{{sfn|Sexism on the cover of Books in Canada?, Rabble}}
The magazine continues to provide an online archive of its reviews.{{sfn|Canadian Literary Journals, Zines, and Periodicals}}

==First Novel Award==
{{main|Amazon.ca First Novel Award}}
''Books in Canada'' established its prestigious "Books in Canada First Novel Award" in 1976. 
It was later sponsored by [[SmithBooks]], a bookseller, and known as the "Smithbooks/Books in Canada First Novel Award".{{sfn|Van Herk|2009}}
When Smithbooks was acquired by [[Chapters]] in 1995 the award was renamed the "Chapters/Books in Canada First Novel Award".
Later the award became the "Amazon.ca/Books in Canada First Novel Award".
After ''Books in Canada'' closed it became the "Amazon.ca First Novel Award".{{sfn|Van Herk|2009}}

==References==
{{reflist |colwidth=30em}}
==Sources==
{{refbegin}}
*{{cite journal|ref=harv|url=http://www.theglobeandmail.com/technology/this-award-night-comes-with-a-twist/article1062728/
 |last=Adams|first=James |title=This award night comes with a twist|date=2008-10-01|journal=[[The Globe and Mail]]|accessdate=2014-08-11}}
*{{cite web|ref=harv|url=http://savoirs.usherbrooke.ca/handle/11143/2841
 |last=Ariss|first=Michelle |title=The rise and demise of a book-review magazine interpreting the cultural work of Books in Canada (1971-2008)
 |year=2011|accessdate=2014-08-11}}
*{{cite web|ref={{harvid|Books in Canada, Collectors' Additions Canada}}|url=http://www.collectorsadditionscanada.com/#!books-in-canada/c4om
 |title=Books in Canada|work=Collectors' Additions Canada|accessdate=2014-08-11}}
*{{cite web|ref={{harvid|Canadian Literary Journals, Zines, and Periodicals}}|url=http://www.abclc.ca/canadian-literary-journals-zines-and-periodicals/
 |title=Canadian Literary Journals, Zines, and Periodicals|work=Canadian Literature Centre|publisher=University of Alberta|accessdate=2014-08-11}}
*{{cite web|ref={{harvid|Clery, Val, University of Toronto}}|url=http://mediacommons.library.utoronto.ca/fonds/clery-val
 |title=Clery, Val|publisher= University of Toronto|accessdate=2014-08-11}}
*{{cite web|ref={{harvid|Don Bell fonds, Concordia University}}|url=http://archives.concordia.ca/P235
 |title=Don Bell fonds|publisher=Concordia University|accessdate=2014-08-11}}
*{{cite book|ref=harv
 |last=Henighan|first=Stephen|title=When Words Deny the World: The Reshaping of Canadian Writing
 |url=https://books.google.com/books?id=ZyoRP9c5dFcC&pg=PA166|accessdate=2014-08-11
 |year=2002|publisher=The Porcupine's Quill|isbn=978-0-88984-240-3}}
*{{cite web|ref={{harvid|Sexism on the cover of Books in Canada?, Rabble}}|url=http://rabble.ca/babble/babble-book-lounge/sexism-cover-books-canada
 |title=Sexism on the cover of Books in Canada?|work=Rabble.ca|date=22 March 2008|accessdate=2014-10-18}}
*{{cite journal|ref=harv|url=http://www.theglobeandmail.com/arts/books-and-media/judging-first-fiction-first/article4290576/|journal=[[The Globe and Mail]]
 |last=Van Herk|first=Aritha|title=Judging first fiction first|date=2009-10-09|accessdate=2014-08-11}}
*{{cite web|ref=harv|url=http://www.thecanadianencyclopedia.ca/en/article/books-in-canada/
 |last=Woodcock|first=George|date=2009-05-18|work=The Canadian Encyclopedia|publisher=Historic Canada|location=Toronto|title=Books in Canada|accessdate=2014-08-11}}
{{refend}}
{{DEFAULTSORT:Books in Canada}}
[[Category:1971 establishments in Ontario]]
[[Category:2008 disestablishments in Ontario]]
[[Category:Canadian literary magazines]]
[[Category:Defunct literary magazines]]
[[Category:Defunct magazines of Canada]]
[[Category:Magazines established in 1971]]
[[Category:Magazines disestablished in 2008]]
[[Category:Magazines published in Toronto]]
[[Category:Canadian monthly magazines]]