{{Use mdy dates|date=September 2016}}
{{Infobox military award
|name=Hero of Belarus
|image=[[Image:Medal Hero of Belarus.svg|100px]]
|caption=
|awarded_by=[[Belarus]]
|type= Medal
|eligibility= 
|for= those who "''perform great deeds in the name of freedom, independence and prosperity of the Republic of Belarus''".
|status= Currently awarded
|description=
|clasps=
|established= April 13, 1995
|first_award= [[Uladzimir Karvat]]
|last_award=
|total=
|posthumous=2<ref name="Герои Беларуси">Belarusian Awards [http://www.belawards.narod.ru/KHeroBlr.htm List of Heroes of Belarus]. Retrieved May 17, 2005. {{ru icon}}</ref>
|recipients=11<ref name="Герои Беларуси"/>
|individual=
|higher=
|same=
|lower= 
|image2=
|caption2=
}}

'''Hero of Belarus''' ({{lang-be|Герой Беларусi}}; [[Lacinka alphabet|Łacinka]]: Hieroj Biełarusi; [[Russian language|Russian]]: Герой Беларуси, [[Romanization of Belarusian|transliterated]] ''Geroy Belarusi'') is the highest [[Hero (Title)|title]] that can be bestowed on a [[citizen]] of [[Belarus]]. The title is awarded to those "who perform great deeds in the name of freedom, independence and prosperity of the Republic of Belarus".<ref name="N3599">National Legal Internet Portal of the Republic of Belarus [http://pravo.by/webnpa/text_txt.asp?RN=V19503599 Resolution N 3599-XII]. Retrieved May. 16 2005. {{ru icon}}</ref> The deed can be for [[Military of Belarus|military]] performance, [[Economy of Belarus|economic]] performance or great service to the State and society. The design of the medal is similar to that of its predecessor, [[Hero of the Soviet Union]]. Similar titles to the Hero of Belarus include the [[Russia]]n [[Hero of the Russian Federation]], [[Ukraine|Ukrainian]] [[Hero of Ukraine]], and [[Uzbekistan|Uzbek]] [[Hero of Uzbekistan]]. Since its creation, the title has been awarded to ten people.

== Legislation ==
The title and other Belarusian state awards, their creation, nomination and awarding, are regulated by a set of legal documents: Belarusian Constitution, Law N 288-З "About state awards of the Republic of Belarus" and presidential decrees.

The title was created by the Belarus [[Supreme Soviet]] on April 13, 1995, with the passage of [[Resolution (law)|Resolution]] N 3726-XII, titled "System of State Awards for the Republic of Belarus". Alongside the Hero of Belarus title, the resolution authorized the creation of [[medal]]s, [[Order (decoration)|orders]], and titles that can be presented by the Belarusian government. The creation of the awards was a way to honor those who have made valuable contributions to Belarus, irrespective of whether they were performed by a citizen or a foreigner.<ref name="N3726">{{cite web|url=http://bankzakonov.com/republic_pravo_by_2010/blockc2/rtf-u5u8n7.htm|title=О государственных наградах Республики Беларусь|website=Bankzakonov.com|accessdate=September 16, 2016}}</ref>

This resolution was outlawed and replaced by Law N 288-З on May 18, 2004.

== Recommendation process and awarding ==
To be considered for title, a person must perform a deed that greatly benefits the state and Belarusian society at large. The title can be awarded to those serving in the military, public service or private enterprise. It can only be awarded once to an individual, and can be awarded [[Posthumous recognition|posthumously]]. The official criteria are stated in Chapter 2, Article 5 of Law N 288-З. Chapter 3, Article 60 of Law N 288-З allows any group (association) of workers to submit a recommendation (petition) for an individual to be awarded the title. Governmental bodies, the National Assembly, the Council of Ministers, cabinet officials and public unions, among others, are also eligible to submit nominations for the title. The candidate is evaluated, and if deemed worthy, the nomination is forwarded to the Council of Ministers and then, after having been allowed by the Governmental Secretariat of the Belarusian Security Council, to the President of the Republic.

Under the Belarus [[Constitution]] (Part 4, Chapter 3, Article 84), the President of the Republic has the power to bestow state awards. To announce an award, the President issues a [[decree]] conferring the title to a person.<ref name="const">President of the Republic of Belarus [http://www.president.gov.by/en/press19332.html Constitution of Belarus -Duties of the President] {{webarchive|url=https://web.archive.org/web/20071217035445/http://www.president.gov.by/en/press19332.html |date=December 17, 2007 }}. Retrieved November 25, 2006.</ref> Within two months, the title will be presented by the President in a formal setting, usually at the Presidential Palace in the capital city of [[Minsk]]. A certificate (''gramota'') will also be presented to the recipient, signed by the President of the Republic.

== Privileges ==
Initially, social privileges for those, who achieved Hero of Belarus, according to the Resolution of the Supreme Council of the Republic of Belarus under April 13, 1995 N 3727-XII,<ref>[http://www.bankzakonov.com/d2008/time79/lav79258.htm]{{dead link|date=September 2016}}</ref> were equal to the privileges for people, whom [[Hero of Soviet Union]], [[Hero of Socialist Labour]], [[Order of Glory]] or [[Order of Labor Glory]] was given. These privileges are regulated by Resolution of the Supreme Council of the Republic of Belarus under February 21, 1995 N 3599-XII.<ref name="N3599"/>

Now this question is regulated by Law of the Republic of Belarus under June 14, 2007 N 239-З "About social privileges, rights and guarantees to a definite category of citizens".<ref>{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=H10700239|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref>

According to it, Heroes of Belarus have rights:
* of free supplying with medicines in the list of main medicines, which are given under recipes in hospitals;
* of free production and repairing of dentures in governmental organizations domiciliary and of free supplying with other technical means of rehabilitation in accordance with the Governmental register of technical means of social rehabilitation;
* of pecuniary aid for recovery or, instead of pecuniary aid, of free out-of-turn sanitary-resort recovery;
* of free trip on all kinds of urban transport (except taxi) irrespective of domicile;
* of free trip on the transport (railway, water, auto, but not taxi) of  suburban traffic;
* of one free roundtrip by rail, airplane, or truck per year of interurban traffic;
* rent-free housing, utilities, and services within 20 square meters of living space;
* of free home phone installation if the extension of the subscriber line is less than 500 meters;
* of free usage of the home phone (except interurban and international talks).
The consumptions, connected with these privileges, are financed from means of republican and local budgets and of governmental social insurance.

== Construction and display ==
[[Image:Hero Belarus medal 1996 project.png|thumb|1996–1999 medal]]
The recipient of the title is given a medal called the Medal of the Hero of Belarus (Belarusian: медаль Героя Беларусі, Russian: медаль Героя Беларуси). The [[star]] and suspension are made of [[gold]], and thus it is nicknamed "Gold Star", as was its predecessor, the [[Hero of the Soviet Union]]. The star has a total [[diameter]] of 33&nbsp;millimeters, and is attached to a rectangular suspension device (boot tree). In the center of the rectangle is a ribbon of two sections of red and one section of green. The red and green bars on the ribbon evoke the design and colors of the [[Flag of Belarus|national flag]]. The amount of gold is set at 585–1 Test, with the total [[weight]] of the medal being 19&nbsp;[[grams]].<ref name="medal">President of the Republic of Belarus [http://www.president.gov.by/press28330.html Medal – Hero of Belarus design]. Retrieved November 25, 2005. {{ru icon}}</ref> The present design was enacted into law by Presidential Decree Number 516 on September 6, 1999.<ref>[http://www.bankzakonov.com/obsch/razdel192/time5/lavz4619.htm]{{dead link|date=September 2016}}</ref>

The design of the medal is modeled after one used for the Hero of the Soviet Union. Unlike the Hero of the Soviet Union, [[Hero of Socialist Labor]] and the [[Hero of the Russian Federation]] titles, there is no engraving on the reverse of the star. The ribbon also copies the Soviet Hero medal's ribbon, since a flag design was also used to make the ribbon of that medal. The medal is always worn in full on the left side of the breast above all other medals and orders. Chapter 4, Article 69 of Law N 288-З states that any awards and titles presented by the Soviet Union and the [[Byelorussian SSR]] must be placed after awards from the Republic of Belarus.

=== 1996 medal ===
When the title was created, a suggested medal was drafted and designed by the government. The major difference between this medal and the current medal is the design of the suspension and the star medallion at the bottom: the top suspension is longer than the bottom, and the bottom star is outlined differently and is adorned with [[ruby|rubies]]. The suspension was made from gold-plated silver, while the suspension of the current medal is made from gold, as the medal. The design of the star evokes the [[Marshal's Star]], which [[Marshal of the Soviet Union|Soviet Marshals]] wore around their necks. This medal was adopted by Presidential Decree Number 26 on May 15, 1996.<ref name="belawards">Belarusian Awards [http://www.belawards.narod.ru/HHeroBlr.htm History of the Hero of Belarus]. Retrieved May 14, 2005. {{ru icon}}</ref><ref>{{cite web|url=http://old.bankzakonov.com/obsch/razdel192/time7/lavz6637.htm|title=Президент Республики Беларусь - Об утверждении описания орденов,|website=Old.bankzakonov.com|accessdate=September 16, 2016}}</ref>

== Recipients ==
The first award in history was given to [[Uladzimir Karvat]] (posthumous) in 1996. The first "group awarding" took place on June 30, 2001 to [[Pavel Mariev]], [[Mikhail Karchmit]], [[Vital Kramko]] and [[Alaksandar Dubko]] (posthumous),<ref name="sb">Soviet Byelorussia [http://sb.by/print.php?articleID=6254 New Heroes of Belarus] {{webarchive |url=https://web.archive.org/web/20051210043335/http://sb.by/print.php?articleID=6254 |date=December 10, 2005 }}. July 5, 2001. Retrieved May 19, 2005. {{ru icon}}</ref> while [[Kirill Vakhromeev]], [[Mikhail Savicki]], Mikhail Vysotsky, Piotr Prokopovich and Vasily Revyako were presented their titles on March 1, 2006.<ref name="Герои Беларуси" />

Uladzimir Karvat, a military pilot, was flying his training aircraft [[Sukhoi Su-27]]p on May 23, 1996. The plane caught fire and Karvat was ordered to eject to safety. Unknown to the ground crew, the plane would have crashed in an area full of civilians. Seeing the civilians on the ground, Karvat steered the plane away until it crashed one&nbsp;kilometer from the [[Brest Voblast]] settlements of [[Arabawshchyna]] and [[Vyalikaye Hatsishcha]], killing him instantly. President [[Alexander Lukashenko]] issued Decree Number 484 on November 21, 1996, which posthumously awarded Karvat the title Hero of Belarus.<ref name="karvat">{{cite web|url=http://armyby.vif2.ru/karvat.html |title=Archived copy |accessdate=2005-05-15 |deadurl=yes |archiveurl=https://web.archive.org/web/20051027061351/http://armyby.vif2.ru/karvat.html |archivedate=October 27, 2005 |df=mdy }}</ref><ref>{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=P39600484|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref> The crash site has been converted to a memorial for Karvat, where a copy of Lukashenko's decree is on the tail fin of the Sukhoi Su-27p.<ref name="memorial">{{cite web|url=http://www.airforce.ru/memorial/belorussia/arabovshina/arabovshina.htm|title=Памятник В.Карвату|website=Airforce.ru|accessdate=September 16, 2016}}</ref>

Pavel Mariev was awarded the title for his work in the automobile industry as manager of [[Belarusian Auto Works]], a leading producer of Belarusian automobiles.<ref>{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=P30100360|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref>

Vital Kramko and Mikhail Karchmit were awarded for their work in the agriculture industry. Kramko is the chairman of the [[Hrodna Voblast]] agricultural collective "October", while Karchmit was the director of the [[Minsk Voblast]] cooperative "Snov" until his death in 2004.<ref name="karcmit">{{cite web|url=http://www.president.gov.by/press17427.html |title=Archived copy |accessdate=2006-11-26 |deadurl=yes |archiveurl=https://web.archive.org/web/20071229225851/http://www.president.gov.by/press17427.html |archivedate=December 29, 2007 |df=mdy }}</ref><ref>{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=P30100362|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref>

Alaksandar Dubko was posthumously awarded the hero title for his long service to the Belarusian and Soviet governments.<ref name="Герои Беларуси" /><ref>{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=P30100361|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref>

Kirill Vakhromeev, the current Metropolitan of Minsk and Slutsk, the Patriarchal Exarch of All Belarus, was awarded the hero title for his work to restore spirituality among the Belarusian population.<ref>The National State Teleradiocompany [http://www.tvr.by/eng/news.asp?id=15852&date=21.03.2006%2014:16:00 Filaret celebrates birthday]. Aired March 21, 2006. Retrieved November 25, 2006.</ref>

Mikhail Savicki was awarded the hero title for his long contributions to the Belarusian arts and for helping to set up art academies in the republic.<ref name="fourheroes">{{cite web|url=http://www.pravo.by/webnpa/text.asp?RN=P30600135|title=ЭТАЛОН-ONLINE|website=Pravo.by|accessdate=September 16, 2016}}</ref>

Mikhail Vysotsky was awarded for running the research enterprise "Belavtotraktorostroyeniye", a part of the National Academy of Sciences.<ref name="fourheroes"/>

Piotr Prokopovich was awarded for his work as the chairman of the [[National Bank of Belarus]].<ref name="fourheroes"/>

Vasily Revyako was awarded for his work at the Hrodna Voblast agricultural co-op "Progress-Vertelishki".<ref name="fourheroes"/><ref name="2006heroes">President of the Republic of Belarus [http://www.president.gov.by/en/press24141.html Head of State Confers the “Hero of Belarus” Title]. Printed March 2, 2006. Retrieved November 25, 2006.</ref>

[[Darya Domracheva]] was awarded for her progress in biathlon after The Olympic Games in Sochi, where Darya won 3 gold medal in personal starts.

== In culture ==
[[File:Stamp of Belarus. Karvat V. N.jpg|thumb|right|[[Uladzimir Karvat]] on stamp of Belarus]]
The medal was featured on a stamp, [[Postage stamps and postal history of Belarus|released]] by [[Belposhta]], commemorating the 3rd year of Karvat's death. Designed by V. Volynets, the 25,000 rouble stamp featured the medal on the left and Karvat's photo on the right in full color. In the white text below the medal, it says "Hero of Belarus, Uładzimir Mikalaevič Karvat, (28.11.1958 – 23.05.1996)". It was issued on August 12, 1999, and had a print run of 90,000.<ref>{{cite web|url=http://www.belpost.by/eng/stamps/stamp-catalogue/1999/|title=1999 - Belpost|website=Belpost.by|accessdate=September 16, 2016}}</ref>

The hero title was also featured in a set of stamps released by BELPOST in December 2006, depicting the state awards of Belarus. On the first issue card, the medal is displayed in full color next to drawings of the state emblem, state flag and the Presidential Palace. A first day postal marking from Minsk also uses an outline of the medal, which is adorned with wreaths and the text "National Decorations of the Republic of Belarus" written in the Belarusian language.

Another set of stamps, devoted to orders and medals of Belarus, was put into circulation by Ministry of communication and informatization of Belarus in August 2008. The designers are Ivan Lukin and Oleg Gaiko. The printing is offset and full-color. Paper is chalk-overly and rubberized. The size of stamps are 29,6х52 mm.<ref>{{cite web|url=http://www.belpost.by/eng/stamps/stamp-catalogue/2008/19-2008-08-25/|title=State Awards of the Republic of Belarus |website=Belpost.by|accessdate=September 16, 2016}}</ref>

Television channel "CTV (Minsk)" released a film, devoted to the Heroes of Belarus.<ref>{{cite web|url=http://ctv.by/proj/~group__m11=409~page__n1101=1~news__n1101=30389 |title=Archived copy |accessdate=2010-01-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20120111153619/http://www.ctv.by/proj/~group__m11%3D409~page__n1101%3D1~news__n1101%3D30389 |archivedate=January 11, 2012 |df=mdy }}</ref>

==See also==
{{Commons|Hero of Belarus}}
* [[Orders, decorations, and medals of Belarus]]
* [[Gold star]]
* [[Hero (title)]]
* [[Hero of the Russian Federation]]
* [[Hero of Ukraine]]
* [[State decoration]]

==References==
{{Reflist|30em}}

==External links==
* Belawards.com [http://www.belawards.narod.ru/HHeroBlr.htm History and Facts about the title Hero of Belarus] {{ru icon}}
* Ministry of Defense of the Republic of Belarus [http://www.deti.mil.by/paradeground_6_10/order/ Orders and Medals of the Republic of Belarus] {{ru icon}}
* National Assembly of the Republic of Belarus [http://www.sovrep.gov.by/index_eng.php/.455.1561...1.0.0.html Biography of Maryjaŭ (with a photo wearing his medal)] {{en icon}} {{ru icon}}
* Marat Kazey secondary school of general education (Stankof) [http://schoolnet.by/~stankovo/Geroes%20Belorusi.htm Biographies of all Heroes of Belarus] {{ru icon}}
* ODM of Belarus – [http://www.medals.org.uk/belarus/belarus001.htm Hero of the Republic of Belarus] {{en icon}}

{{featured article}}

{{DEFAULTSORT:Hero Of Belarus}}
[[Category:Orders, decorations, and medals of Belarus]]
[[Category:Awards established in 1995]]
[[Category:National Heroes|Belarus]]