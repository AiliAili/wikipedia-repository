<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= I-14 / ANT-31
 | image=Tupolev I-14.JPG
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Fighter aircraft|Fighter]]
 | national origin=[[Soviet Union]]
 | manufacturer=[[Tupolev]]
 | designer=
 | first flight=27 May 1933
 | introduced=1935
 | retired=
 | status=
 | primary user=[[Soviet Air Force]]
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=20
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}
The '''Tupolev I-14''' (also designated '''ANT-31''') was a [[Soviet Union|Soviet]] [[fighter aircraft]] of the 1930s. It was a single-engined, single-seat [[monoplane]] with retractable undercarriage, designed to carry heavy armament, and as such was one of the most advanced fighters of its time. It was ordered into production, but this was cancelled after only a small number had been built, the competing [[Polikarpov I-16]] being preferred.

==Development and design==
In 1932, the Soviet Air Force developed a requirement for a high-speed [[monoplane]] fighter to serve alongside agile but slower [[biplane]] fighters.<ref name="Gunston Russ p301">Gunston 1995, p.301.</ref> In order to meet this requirement, the Tupolev design bureau assigned a team led by [[Pavel Sukhoi]]. Sukhoi's team produced the '''ANT-31''', a low-wing monoplane with an unbraced [[Cantilever#In aircraft|cantilever]] wing, retractable undercarriage, an enclosed cockpit and heavy cannon armament.  As such, it was one of the most advanced fighters in the world.<ref name="Gunston Russ p401">Gunston 1995, p.401.</ref>

The aircraft had a metal [[monocoque]] fuselage, while the wings were of corrugated metal construction. The mainwheels of the [[conventional landing gear]] retracted backwards into the wing, being operated by cables driven by a handwheel turned by the pilot. The first prototype was powered by an imported 433&nbsp;kW (580&nbsp;hp) [[Bristol Mercury]] [[radial engine]] enclosed by an [[NACA cowling]] and driving a two-bladed wooden propeller.  It was armed with a single [[PV-1 machine gun]], with provision for two [[Leonid Kurchevsky|Kurchevsky]] APK-37 [[Recoilless rifle|recoilless]] [[autocannon]] under the wing.<ref name="Gunston Russ p401-2">Gunston 1995, pp. 401–402.</ref><ref name="Duffy p75">Duffy and Kandalov 1996, p.75.</ref>

The ANT-31, given the air force designation '''I-14''' (''Istrebitel'' – fighter), made its maiden flight on 27 May 1933. It proved agile but difficult to handle, and with the [[Supercharger#Aircraft|supercharged]] Mercury was underpowered, particularly at low altitude.<ref name="Duffy p75"/><ref name="Gunston Russ p402"/> It was therefore decided to build a second prototype, the '''I-14bis''' (also known as the ANT-31bis and the I-142 with a more powerful (531&nbsp;kW (712&nbsp;kp) [[Wright R-1820|Wright Cyclone]] engine, also imported, an uncorrugated wing and a new undercarriage.  The I-14bis demonstrated excellent performance, although handling was still difficult, and an order was placed for production of 55 aircraft, to be powered by the [[Shvetsov M-25]], a licensed version of the Cyclone, with an armament of two 45&nbsp;mm (1.8&nbsp;in) Kurchevsky APK-11 recoilless cannons and two [[ShKAS machine gun]]s.<ref name="Duffy p75"/><ref name="Gunston Russ p402"/>

==Operational history==

Deliveries began from the GAZ-125 factory at [[Irkutsk]], [[Siberia]]<ref name="Gunston Russ pXXXII">Gunston 1995, p.XXXII.</ref> in November 1936.<ref name="Gunston Russ p402"/>   The aircraft's armament had changed to a single ShKAS machine gun and a 20&nbsp;mm [[ShVAK cannon]]<ref name="Fly gun p309"/> as Kurchevsky's recoilless guns had fallen out of favour (with Kurchevsky himself soon to be arrested).<ref name="Gunston Russ pXIV">Gunston 1995, p.XIV.</ref> By this time, the rival [[Polikarpov I-16]] fighter was well established in production and service, and production of the I-14 was stopped after 18 had been built, the type soon being phased out of service.<ref name="Duffy p75"/>

==Operators==
;{{USSR}}
*[[Soviet Air Force]]

==Specifications (production I-14)==
{{Aircraft specs
|ref=Tupolev: The Man and His Aircraft<ref name="Duffy p209">Duffy and Kandalov 1996, p.209.</ref>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=one
|capacity=
|length m=6.11
|length ft=
|length in=
|length note=
|span m=11.25
|span ft=
|span in=
|span note=
|height m=3.14
|height ft=
|height in=
|height note=
|wing area sqm=16.8
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=1170
|empty weight lb=
|empty weight note=
|gross weight kg=1540
|gross weight lb=
|gross weight note=<ref name="Gunston Russ p402">Gunston 1995, p.402.</ref>
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Shvetsov M-25]]
|eng1 type=9-cylinder air-cooled [[radial engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=699.7<!-- prop engines -->
|eng1 note=
|power original=
|thrust original=
|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=449
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1050
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=9430
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=6.5 min to 5,000 m (16,400 ft)<ref name="Gunston Russ p402"/>
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|guns= 1× [[7.62×54mmR]] (0.30 in) [[ShKAS machine gun]] and 1× [[20×99mmR]] (0.79 in) [[ShVAK cannon]]<ref name="Fly gun p309">Williams and Gustin 2003, p.309.</ref>
|bombs=
|rockets=
|missiles=
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|other armament=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=*[[Polikarpov I-16]]
*[[Grigorovich IP-1]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{Reflist}}

==References==
{{commons category|Tupolev I-14}}
{{refbegin}}
* Duffy, Paul and Andrei Kandalov. ''Tupolev,: The Man and His Aircraft''. Shrewsbury, UK: Airlife Publishing, 1996. ISBN 1-85310-728-X.
* [[Bill Gunston|Gunston, Bill]]. ''The Osprey Encyclopedia of Russian Aircraft 1875–1995''. London:Osprey, 1995. ISBN 1-85532-405-9.
* Williams, Anthony G. and Emmanuel Gustin.''Flying Guns: World War II''. Shrewsbury, UK: Airlife Publishing, 2003. ISBN 1-84037-227-3.
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Tupolev aircraft}}
{{Soviet fighter aircraft}}

[[Category:Soviet fighter aircraft 1930–1939]]
[[Category:Tupolev aircraft|I-14]]