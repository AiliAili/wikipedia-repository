{{Use dmy dates|date=November 2015}}
{{Use Australian English|date=November 2015}}
{{Infobox dam
| name                 = Canning Dam
| image                = Canning Dam, Perth (1).jpg
| image_caption        = The dam wall and spillway
| dam_crosses          = [[Canning River (Western Australia)|Canning River]]
| location             = [[Roleystone, Western Australia]]
| operator             = [[Water Corporation]]
| dam_length           = {{convert|466|m|ft}}
| dam_height           = {{convert|66|m|ft}}
| dam_width_base       =
| construction_began   = 1933
| opening              = 1940
| cost                 = £1.1&nbsp;million ([[Australian Pound|AU£]], in 1940)
| res_capacity_total   = {{convert|90352|Ml|acre.ft}}
| res_catchment        = {{convert|804|km2|sqmi}}<ref group="nb">Small discrepancies exist.</ref>
| res_surface          = {{convert|501|ha|sqmi}}
| coordinates          = {{coord|32|9|9|S|116|7|42|E|type:landmark|display=inline,title}}
}}
The '''Canning Dam''' and reservoir provide a major fresh water resource for the city of [[Perth, Western Australia]]. The dam is situated on the [[Darling Scarp]] and is an impoundment of the [[Canning River (Western Australia)|Canning River]]. It is noted for its innovative structural and hydraulic design that was considered to be at the forefront of concrete [[gravity dam]] design at the time of construction.<ref name=AHD>{{cite AHD|101649|Canning Dam and Reservoir, McNess Dr, Roleystone, Western Australia | accessdate = 29 November 2008}}</ref> The Canning Dam was Perth's primary water supply up until the 1960s when other sources of fresh water were tapped. Currently the dam supplies approximately 20&nbsp;percent of Perth's fresh water. Inflow into the Canning Reservoir is estimated to be {{convert|22|GL}} and has a storage capacity of {{convert|90352|Ml}}.

Since its completion in 1940, the Canning Dam has contributed to a wide range of [[Human impact on the environment|environmental and ecological problems]] in surrounding regions, problems include more common [[algal blooms]], [[habitat loss]] and [[sedimentation]]. Despite these issues, Canning Dam and the adjacent parks and forests provide a variety of recreational activities for the public such as [[bushwalking]], historic walks and [[picnic|picnic facilities]].

== History ==
Development of the Canning River as a source of water for Perth was first proposed in a report of the first Metropolitan Water Works Board of Perth in 1896.<ref name=AHD /> Investigation of the site began in 1897 when engineer Thomas Hodgson surveyed and proposed the dam's current location as a possible site.<ref name="CCC">{{cite web|url=http://register.heritage.wa.gov.au/PDF_Files/C%20-%20A-D/03709%20CC%20Channel%20(P-AD).PDF|title=Register of Heritage Places – Canning contour channel|last=Day|first=Cathy |date=February 2003|publisher=Heritage Council of WA | format = PDF |accessdate=8 December 2008}}</ref> However, despite the recommendations of further inquiries, and an extreme shortage of water in some years, government funds were not allocated for the construction of a dam until the [[Great Depression]] in the 1930s.

In 1924 a small pipehead dam was built {{convert|6|km|0}} downstream from the present Canning Dam. It was only intended as a quick fix to the water supply problem and it soon became apparent that a major reservoir was needed, although it would be nine years before work on the current Canning Dam would begin. The new dam was completed in 1940 at a cost of [[Australian Pound|AU£]]1.1&nbsp;million.<ref name=WaterCorpHist>{{cite web
 |last=Water Corporation 
 |title=The Canning Dam 
 |url=http://www.watercorporation.com.au/D/dams_canning.cfm 
 |accessdate=29 November 2008 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20080907110705/http://www.watercorporation.com.au./D/dams_canning.cfm 
 |archivedate=7 September 2008 
 |df=dmy 
}}</ref> Engineer [[Russell Dumas]] designed the dam and directed most of its construction.<ref name=AHD/>

A further improvement was made in 1951 when a concrete-lined channel was constructed to divert stream flow from the nearby [[Kangaroo Gully]] catchment. The Canning Dam and reservoir was Perth's primary source of water until the boom growth of the city in the 1960s and the completion of [[Serpentine Dam, Western Australia|Serpentine Dam]] in 1961.<ref name=AHD />
In 1975 the reservoir was connected to Perth's Integrated Water Supply Scheme by the [[Canning Tunnel]].<ref>Day, p.2</ref> Prior to its opening water had flowed through the [[Canning Contour Channel]] to [[Gosnells, Western Australia|Gosnells]].<ref name=AHD />

The Canning Dam and reservoir still supplies approximately 20&nbsp;percent of Perth's drinking water requirements and plays an important role in the context of the development of Perth.

The Canning reservoir is also used to store water from the newly completed [[Kwinana Desalination Plant]].<ref>{{cite news
 | title = WA dam levels up
 | publisher = ABC News
 | date = 3 March 2008
 | url = http://www.abc.net.au/news/stories/2008/03/03/2178513.htm
 | accessdate = 7 December 2008}}
</ref> Treated water can be pumped from the plant to the reservoir through the new Forrestdale Pumping Station.

==Hydrology==
The Canning Dam Catchment lies within the [[Darling Scarp]] which forms part of an [[Archaean Shield]] composed largely of [[granite]] with some invaded linear belts of metamorphosed [[sedimentary]] and [[volcanic]] rocks.<ref name=CRC>{{cite journal
 | last = Department of Water
 | title = Canning River Catchment Area Drinking Water Source Protection Plan
 | work = Water Resource Protection Series
 | issn = 1326-7442
 |date=June 2007
 | format =PDF
 | url = http://portal.water.wa.gov.au/portal/page/portal/WaterManagement/Publications/PlansandAssessments/Content/Canning%20DWSPP.pdf
 | accessdate = 7 December 2008}}</ref>

The dam wall is situated in a narrow [[Canyon|gorge]] running east and west, with rock sides sloping upward from the river bed. Behind the dam wall, the south branch of the [[Canning River]] joins the main stream, with the impounded water forming a lake which stretches back in three major arms to the east south-east and south.<ref name=CRC/>
[[File:Canning Dam, Perth (2).jpg|thumb|right|Canning Dam, as it appeared at 34.4% of capacity]]
The catchment has an area of {{convert|804|km2}}. The reservoir is at {{convert|200|m}} [[Australian Height Datum|AHD]] and the highest point of the catchment, [[Mount Cooke]] is at {{convert|582|m}} AHD.<ref name=CRC />

Climatically, the area receives about {{convert|900|mm}} of rainfall per annum with most of this falling between May and September.<ref name=CRC /> There is widespread variability of rainfall across the catchment however, from between {{convert|700|and|1300|mm}}.<ref name=CRC/>

Since 1975 long-term average rainfalls at the dam wall have decreased by 20 percent and streamflow into the catchment by approximately 60 percent—the average annual inflow between 1948 and 1974 was {{convert|52|GL}} which had reduced to {{convert|22|GL}} between 1975 and 2004.<ref name=CRC /><ref>{{cite web
 |last=Water Corporation 
 |title=Statewide Dam Storage – Canning Dam 
 |url=http://www.watercorporation.com.au/d/dams_storagedetail.cfm?id=11933 
 |accessdate=29 November 2008 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20081002074350/http://www.watercorporation.com.au:80/d/dams_storagedetail.cfm?id=11933 
 |archivedate=2 October 2008 
 |df=dmy 
}}</ref>

== Construction ==
[[File:Canning Dam, construction of dam wall, 1939.jpg|thumb|right|Construction of dam wall, 1939]]The construction of Canning Dam ended a long period during which Perth's water supply was generally unsatisfactory in quality (either due to [[salinity]] or [[bacterial]] pollution or both) and in quantity.<ref name=AHD /> The project, the biggest public works program of the decade, stimulated significant growth in the local economy and provided desperately needed work for around five hundred men.<ref name=WaterCorpHist />

Several innovative design concepts and construction methods which were new to Australia were introduced on the project, while others which were used on the nearby [[Wellington Dam]], were improved upon at the Canning dam site.

At the Canning Dam and indeed all dams, care had to be taken to prevent water seepage between the foundation rock and the structure of the dam.<ref name=AHD /> At the Canning in addition to cutting back the foundation to solid unfractured rock, a cut-off [[trench]] was sited near the upstream face of the dam, down stream which a rock filled drain containing open jointed pipes was provided to intercept any seepage between the rock face and the concrete of the dam. The inclusion of an internal drainage system was considered innovative at the period.<ref name=AHD /> Near vertical tubular 8 in (200&nbsp;mm) cut-off drains were provided at five-foot (1.52 m) intervals along the dam to relieve internal seepage through the concrete.

While state of the art materials-handling methods were used, in some instances labour-saving machinery worked beside operations intended to maximise the labour content. Sustenance workers were employed chiefly for on-site preparation, road construction, foundation excavation, clearing timber from the reservoir basin, and on some concreting operations. Skilled workers were required on the dam for fixing the [[formwork]] where the concrete was poured. These workers were probably employed at normal day labour rates, the main employment method used on the project.<ref name=AHD /> Bulk handling of cement was also used for the first time in Australia, this saw a significant saving as opposed to bagged cement which was the standard practice of the day.

Generally construction work proceeded smoothly and from an engineering point of view there were few setbacks.<ref name=CCC /> However, one did occur in the early stages of construction. In March 1934 there was a violent storm bringing {{convert|130|mm|in|2|abbr=on}} of rain in less than two days. As a result, the river rose rapidly which flooded the dam foundation workings. Pumps had to be installed and work resumed three days later.

The dam was completed in September 1940. When built it was the longest concrete gravity dam in Australia,<ref name=AHD /> and also the second highest after the [[Burrinjuck Dam]] in New South Wales. As of 1997 of the 90 large concrete and masonry gravity dams in Australia the Canning Dam is still the fifth highest and the sixth longest.<ref name=AHD /> It is the largest concrete dam in Western Australia in terms of length of crest and volume of concrete.

The final cost of the dam was significantly less than had been originally budgeted for,<ref name=CCC /> and the work was completed on schedule to a date that was calculated seven years previously.

===Remedial works===
Recently the Canning Dam has been subjected to considerable cracking of the upper parts of the dam and upper gallery. Investigations have shown that cracking was due to strong AAR ([[Alkali–silica reaction|alkali aggregate reactivity]]) in the concrete.<ref>{{cite journal
 | first = Bruce
 | last = Bulley |author2=Wark, Bob |author3=Somerford, Michael
 | title = Dams in the Darling Range
 | publisher = Australian Geomechanics Symposium
 | year = 2003
 | url = http://www.ghd.com.au/aptrixpublishing.nsf/AttachmentsByTitle/PP+DarlingDams+PDF/$FILE/Bulley_DarlingDams.pdf
 | archiveurl=https://web.archive.org/web/20071110211739/http://www.ghd.com.au/aptrixpublishing.nsf/AttachmentsByTitle/PP+DarlingDams+PDF/$FILE/Bulley_DarlingDams.pdf
 | archivedate=10 November 2007
 | format = PDF
 | accessdate = 6 December 2008}}</ref> AAR results in swelling of the concrete, which may cause secondary compressive stresses, localised map cracks, and, ultimately structural cracks. In addition, the concrete [[tensile strength]] and [[Elasticity (physics)|elasticity]] significantly decreases. Many old concrete dams are known to suffer from AAR, including [[Fontana Dam]] in Tennessee and Pian Telessio dam in Italy among others.<ref name="Saouma">{{cite journal
 |author1=Saouma, V. |author2=Perotti, L. |author3=Shimpo, T | title = Stress Analysis of Concrete Structures Subjected to Alkali-Aggregate Reactions
 | journal = ACI Structural Journal
 | issue = Sep/Oct 2007
 | year = 2007
 | url =
}}</ref>

Extensive remedial works were undertaken between 1999 & 2001 to strengthen the dam wall.<ref>{{cite conference
 | first = Tony
 | last = Moulds
 | title = Canning Dam Remedial Works 1999–2001
 | booktitle = 11th National Conference on Engineering Heritage: Federation Engineering a Nation; Proceedings
 | pages = 229–236
 | publisher = Institution of Engineers
 | year = 2001
 | location = Barton, ACT
 | url = http://search.informit.com.au/documentSummary;dn=521070448448728;res=IELENG
 | doi =
 | id = {{Listed Invalid ISBN|1-74092-215-5}}
 | accessdate = 12 December 2008}}</ref> This work involved removing the top {{convert|3.8|m|ft|abbr=on}} of the existing dam wall and drilling/blasting through the dam wall plus up to a further {{convert|70|m|ft|abbr=on}} into the bedrock below. The top section of the wall was then rebuilt using reinforced concrete. Finally, permanent, re-stressable [[Earth anchor|ground anchors]] were then installed through the formed and drilled holes from the crest to be stressed and grouted into the foundation rock. At time of completion, these were the largest capacity and longest permanent [[Earth anchor|ground anchors]] ever installed in the world.<ref>{{cite web
 | last = Structural Systems
 | title = Canning Dam Ground Anchors
 | url = http://www.structuralsystems.com.au/ssl/cases/pdfs/AUS/SSL-PDS-PT016-CanningDam.pdf
 | format = PDF
 | accessdate = 12 December 2008}}</ref>

An innovative drilling and blasting technique called Penetrating Cone Fracture (PCF) was used in the remedial works process. PCF was chosen over conventional drilling and/or blasting techniques due to the reduced risk of damage to the existing structure from vibration, as well as lower noxious fume and dust levels.<ref>{{cite web
 | last = Rockbreaking Solutions
 | title = Penetrating Cone Fracture (PCF) – Frequently Asked Questions
 | publisher = Alterrain
 | year = 2002
 | url = http://www.alterrain.com.au/pdfs/PCF_FAQ.pdf
 | format = PDF
 | accessdate = 12 December 2008}}</ref>

== Environmental issues ==
Since the construction of the Canning Dam, among other [[drinking water]] supply dams, water flow into the [[Canning River (Western Australia)|Canning River]] has been reduced by up to 96%.<ref>{{cite web
 | last = Landcare
 | authorlink = Landcare Australia
 | title = Workplace Giving Towards a Healthier Canning River
 | publisher = Landcare
 | url = http://www.landcareonline.com/case_study.asp?cID=110
 | accessdate = 29 November 2008 |archiveurl = https://web.archive.org/web/20080531141914/http://www.landcareonline.com/case_study.asp?cID=110 |archivedate = 31 May 2008}}</ref> A number of freshwater fish species which are endemic to the south-west of Western Australia are found in the Canning River system, however studies of fish and fish habitats in the area have shown that fish numbers are low due to a loss of habitat and a loss of linkage between breeding areas due to low flows, preventing fish migrating upstream and reaching important breeding and nursery grounds.<ref>{{cite web
 | last = Department of Environment
 | title = Canning River flows
 | publisher = Government of Western Australia
 | url = http://portal.environment.wa.gov.au/pls/portal/docs/PAGE/ADMIN_SRT/BROCHURES/BRCCANNINGFLOWS_0311.PDF
 | format = PDF
 | accessdate = 1 December 2008}}</ref> Stagnant water caused by a lack of water flow has provided a suitable habitat for successful breeding of an introduced pest, the [[Mosquitofish in Australia|mosquitofish]].<ref name=srt>{{cite web
 | last = Swan River Trust
 | title = Caring for the Canning
 | publisher = Government of Western Australia
 | url = http://portal.environment.wa.gov.au/pls/portal/docs/PAGE/ADMIN_SRT/REPORTS/CARING_FOR_CANNING_FINAL.PDF
 | format = PDF
 | accessdate = 6 December 2008}}</ref>

Damming of the Canning caused dramatic flow reductions that significantly altered downstream aquatic [[macroinvertebrate]] communities.<ref>{{cite journal
 | last = Saunders, D.L.; Meeuwig, J.J.; Vincent, A.C.J
 | title = Freshwater Protected Areas: Strategies for Conservation
 | journal = Conservation Biology
 | volume = 16
 | issue = 1
 | pages = 30–41
 | publisher = Wiley Interscience
 | location =
 | year = 2002
 | doi = 10.1046/j.1523-1739.2002.99562.x
 | first1 = D. L.
 | last2 = Meeuwig
 | first2 = J. J.
 | last3 = Vincent
 | first3 = A. C. J.}}</ref> The lack of water flow has also resulted in a poor flushing effect below the dam wall. An excessive amount of nutrients from fertilizers and animal waste has caused [[algal blooms]] and [[eutrophication]].<ref>{{cite web
 | last = Water and Rivers Commission
 | title = Algal Blooms
 | work = Water Facts 6
 | publisher = Government of Western Australia
 | year = 1998
 | url = http://www.nynrm.sa.gov.au/Portals/5/pdf/LandAndSoil/19.pdf
 | format = PDF
 | accessdate = 12 December 2008}}</ref>

Many river pools which are an important summer refuge and habitat for aquatic and terrestrial flora and fauna have been lost due to [[sedimentation]] and modification of the flow regime caused by impoundments in the [[Canning River]].<ref name=srt />

Periodic flooding of the Canning River from the dam is required to disperse seed, stimulate [[germination]] and ensure [[seedlings]] survive, recharge shallow [[groundwater]] tables that are important during periods of drought and to discourage and prevent weed growth.<ref name=srt /> However, during times of low rainfall periodic flooding is reduced.

== Recreation ==
A number of recreation activities occur in and around the dam and catchment area.<ref name=CRC /> Canning Dam features a number of picnic areas (with gas [[barbecue]]s), look outs and historic walks – many with disabled access.<ref name=map>{{cite web
 |last=Water Corporation 
 |title=Canning dam facilities brochure 
 |url=http://www.watercorporation.com.au/_files/PublicationsRegister/12/canning.pdf 
 |format=PDF 
 |accessdate=29 November 2008 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20070926231635/http://www.watercorporation.com.au/_files/PublicationsRegister/12/canning.pdf 
 |archivedate=26 September 2007 
 |df=dmy 
}}</ref> [[Bushwalking]] occurs throughout the catchment, particularly along the [[Bibbulmun Track]], which passes through the catchment about 10&nbsp;km upstream of Canning Dam. Several [[mountain bike]] trails run either through the Canning National Park or adjacent State Forest areas.<ref>{{cite web
 | last = Experience Perth
 | title = Parks and Picnic Spots
 | publisher = Perth Region Tourism Organisation Inc
 | year = 2007
 | url = http://www.experienceperth.com/en/Top+things+to+do+in+Perth/Parks+and+Picnic+Spots/Parks+and+Picnic+Spots+Inner.htm
 | accessdate = 8 December 2008}}</ref>

The Canning Reservoir, Canning River and tributaries are illegally fished for [[marron]], especially during summer. Boating, fishing and swimming are prohibited in the reservoir for health and hygiene reasons. Unauthorised camping (including overnight stays and/or outside of designated areas) and unauthorised trail establishment occur more and more frequently in the Canning catchment.<ref name=CRC />

==Gallery==
<gallery>
Image:Canning Dam, Perth (1).jpg|The dam wall and slip-way of [[w:Canning Dam]] in Perth, Western Australia.
Image:Canning Dam panorama.jpg|Spillway
Image:Canning Dam, Perth (4).jpg|Spillway, reservoir side
Image:Canning dam panorama 4.jpg|Panorama
Image:Canningdam plaque.jpg|Commemorative plaque
Image:Canningdam panorama1.jpg|Canning Dam wall, from the top of the walkway.
Image:Canningdam panorama2.jpg|Canning Dam catchment, reservoir side.
Image:Canningdam spillway.jpg|View of the dam including the spillway.
</gallery>

==See also==
{{Commons|Canning Dam}}
{{portal|Western Australia}}
*[[Canning River]]
*[[Canning Tunnel]]
*[[List of reservoirs and dams in Australia]]
*[[Swan River (Western Australia)|Swan River]]
*[[Serpentine Dam, Western Australia]]

==Notes==
<references group="nb" />

==References==
{{Reflist|33em}}

==External links==
* [https://web.archive.org/web/20080907110705/http://www.watercorporation.com.au./D/dams_canning.cfm The Website for the Canning Dam]

{{Good article}}

[[Category:Heritage places in Perth, Western Australia]]
[[Category:Canning River (Western Australia)]]
[[Category:Dams completed in 1940]]
[[Category:Dams in Western Australia]]
[[Category:Reservoirs in Western Australia]]
[[Category:1940 establishments in Australia]]