{{COI|date=July 2015}}
{{EngvarB|date=September 2014}}
{{Use dmy dates|date=June 2016}}
{{Infobox person
|name          = Muqeem Khan<br>{{small|{{Nastaliq|مقیم خان}}}}
| image       =MuqeemKhan.jpg |200px|center|Muqeem Khan
| image_size  = 250px
| birth_date  = 1968
| birth_place = [[Karachi]], Pakistan
| occupation  = Professor, researcher, filmmaker, media artist 
| notable_works = First Pakistani [[vfx]] artist who started working in the Hollywood film industry in 1996.
| education     = Karachi Academy, Delhi College, [[National College of Arts|National College of Arts (NCA)]]
| alma_mater = [[Ohio State University]], [[Columbus, Ohio]], US, PhD in Progress at [[UNSW Art & Design]], [[University of New South Wales]], Sydney
| fields = Arts, design
|website      = {{URL|muqeemkhan.com}}
}}

'''Muqeem Khan''' ({{lang-ur|{{Nastaliq| مقیم خان}}}}) (born 1968) is the first [[Pakistani]] who started working in the Hollywood [[visual effects]] industry in 1996.<ref name=anim2/><ref name=TEDTalk>[http://talentsearch.ted.com/video/Muqeem-Khan-Playful-technology ''• Presentation, "Preserving our Past with Toys of the Futurea", TED@Doha Summit, Doha, Qatar, April, 2012.'', February 25, 2013]</ref><ref name=BrightFuture>[http://www.muqeemkhan.com/BrightFuture.html ''• "Bright future for gaming industry", Gulf Times, Qatar, April 2012.'']</ref><ref name=Blending>[http://www.muqeemkhan.com/press/Qatar_Tribune_Nation_page_23.pdf'' • "Blending Real With Virtual", Qatar Tribune, Doha, Qatar, May, 2009.'']</ref><ref name=Digital>[http://www.thenews.com.pk/TodaysPrintDetail.aspx?ID=159280&Cat=4&dt=1/24/2009'' • "Digital age in Pakistan not a distant dream", The News, Karachi, Pakistan, January, 2011.'']</ref><ref name=Final-Fantasy>[https://movies.nytimes.com/movie/246683/Final-Fantasy-the-Spirits-Within/credits'' • Final-Fantasy-the-Spirits-Within&nbsp;– Cast, Crew, Director and Awards&nbsp;– NYTimes.com. (n.d.). Retrieved September 11, 2013, from http://movies.nytimes.com/movie/246683/Final-Fantasy-the-Spirits-Within/credits'']</ref><ref name=Total/> He is an animator,<ref name=anim1>[http://www.newslinemagazine.com/2010/04/interview-muqeem-khan-animator/ ''• Interview: Muqeem Khan, Animator, Newsline, Karachi, Pakistan, April., 2010.]</ref> artist,<ref name=art1>[http://www.qatar.vcu.edu/oldsite/news/vcuqatar-presents-annual-faculty-exhibition-2007-2008/ ''• "Horoof-e-Moqatiat", Calligraphy Work, Virginia Commonwealth University in Qatar Faculty Exhibition, VCUQ Gallery, Doha, Qatar, Dec., 2007.]</ref><ref name=art2>[http://h-net.msu.edu/cgi-bin/logbrowse.pl?trx=vx&list=H-Islamart&month=0712&week=a&msg=4Ka9lk6Lf/07wmVgM8Y73g&user=&pw= ''• "Muqeem Khan by Muqeem Khan", Animation, Virginia Commonwealth University in Qatar Faculty Exhibi- tion, VCUQ Gallery, Doha, Qatar, Dec., 2007.]</ref> [[interaction designer]],<ref name=interaction1>[http://www.andrew.cmu.edu/user/myousuf/Ya%20Hala/read.html ''• "Platform Expo brings gaming experts to Doha]</ref> percussionist,<ref name=music>[http://www.pakistantoday.com.pk/2012/05/24/uncategorized/‘kingdom-of-women’-sole-pakistani-nom-at-cannes/ ''• Background Music Design, "Kingdom of Women," by Amna Khaishgi, MOSAIC South Asian Festival. Missis- sauga, August 2012.]</ref><ref name=music2>[http://mubi.com/films/kingdom-of-women ''• KINGDOM OF WOMEN]</ref> filmmaker<ref name=anim2>[http://tribune.com.pk/story/110559/animation-in-pakistani-feature-films/ ''• Animation in Pakistani feature films, The Express Tribune, Lahore, Pakistan, January, 2011.]</ref> and [[academician]].<ref name=academic1>[http://www.qatar.northwestern.edu/index.html ''• Associate Professor in Residence&nbsp;– Northwestern University in Qatar.]</ref><ref name=academic2>[http://www.qatar.vcu.edu ''• Assistant Professor of Graphic & Interior Design&nbsp;– Virginia Commonwealth University in Qatar.]</ref><ref name=academic3>[http://www.aus.edu ''• Assistant Professor of Digital Design&nbsp;– School of Architecture & Design, American University of Sharjah, UAE.]</ref> As a student at [[National College of Arts|National College of Arts (NCA)]], Muqeem developed passion for [[computer graphics]] nearly twenty-five years ago in his early student life by doing some [[Composition (visual arts)|compositions]] and screen [[credits]] in [[BASIC]] language on [[IBM XT]], [[ZX Spectrum]] and [[Commodore 64]] computers.
<!-- -->

== Film credits ==

''[[Final Fantasy: The Spirits Within]]''<ref name=Final-Fantasy/><ref name=film1>[http://www.youtuberepeater.com/watch?v=YxQfd0_DEZ4&name=Muqeem+Khan+News1+Morning+Masala ''• Muqeem Khan&nbsp;– News1 Morning Masala]</ref><ref name=film2>[http://www.viewconference.it/2007/g07_Khan.php ''• Pragmatised Emotions: The Art of the Future Designers]</ref><ref name=film3>[http://www.qatar-tribune.com/data/20120615/content.asp?section=Nation3_5 ''• PEF seminar highlights secret of visual effect, animation]</ref><ref name=film4>[http://www.qatar-tribune.com/data/20120526/content.asp?section=nation1_2 ''• Animation expert gives tips to students]</ref><ref name=film5>[http://beta.dawn.com/news/607513/zooming-in-our-man-in-hollywood ''• Zooming In: Our man in Hollywood]</ref><ref name=film6>[http://www.eurosis.org/cms/?q=node/2221 ''• GAMEON-ARABIA'2012, December 10–12, 2012, AOU, Muscat, Oman, Conference Tutorial]</ref><ref name=film7>[http://home.eyesonff.com/content/235-fftsw-credits.html ''• Final Fantasy Credits]</ref>&nbsp;– July 2001 (USA) Visual Effects, [[Square (company)|Square USA]], [[Honolulu]], [[Hawaiian Islands|Hawaii]] • [[Visual effects]], [[Rigid body dynamics|Rigid]]/[[Soft body dynamics]] and [[Deformation (engineering)|Deformation]]s

''[[Armageddon (1998 film)|Armageddon]]''<ref name=anim2/><ref name=film1/><ref name=film2/><ref name=film3/><ref name=film4/><ref name=film5/><ref name=film6/>- December 1998 (USA) [[Visual effects]], [[Dream Quest Images]], [[The Walt Disney Company]], California • Sparks, fire and debris for the exploding shuttle
• Effect animation for the debris and rocks• Gases and asteroids

''[[Deep Rising]]''<ref name=anim2/><ref name=film1/><ref name=film2/><ref name=film3/><ref name=film4/><ref name=film5/><ref name=film6/>&nbsp;– January 1998 (USA) [[Visual effects]], [[Dream Quest Images]], [[The Walt Disney Company]], California • Digital rain and tracking of digital cameras

''[[George of the Jungle (film)|George of the Jungle]]''<ref name=anim2/><ref name=film1/><ref name=film2/><ref name=film3/><ref name=film4/><ref name=film5/><ref name=film6/>&nbsp;– July 1997 (USA) [[Visual effects]], [[Dream Quest Images]], [[The Walt Disney Company]], California • Visual effects and elephant's interaction with the ground

''[[Flubber (film)|Flubber]]''<ref name=anim2/><ref name=film1/><ref name=film2/><ref name=film3/><ref name=film4/><ref name=film5/><ref name=film6/>- November 1997 (USA) [[Visual effects]], [[Dream Quest Images]], [[The Walt Disney Company]], California• Visual effects for clouds and wisps • Compositing and digital elements for the 3D environment

== Background ==

Beside filmmaking and other creative genres such as poetry, [[calligraphy]] and painting, Khan also loves music and as a percussionist, he has been performing [[Tabla]] with various musicians around the world. He is a frequent presenter and speaker on [[VFX]] in motion pictures, animation and [[emerging technologies]]<ref name=TEDTalk/><ref name=Total>[http://www.muqeemkhan.com/press/Total%20Realism%20Penunsula%202013.pdf ''• "Total realism with computer graphics no distant dream", Qatar Tribune, Doha, Qatar, March, 2013.'', February 25, 2013]</ref>

He has presented his research in conferences such as 'ACM Graphite' in Singapore, 'Information Visualization' in London, VIEW Conferences in Italy, ascilite conference in Australia, [[TED (conference)|TED@Doha Summit]], 'Computers in Education' in [[New Zealand]] and International Games Innovation Conference (IGIC) in New York. Muqeem has also served as a member of technical committee, IASTED, The International Association of Science and Technology Development, [[Alberta]], Canada and member of the Editorial Board for "Design Behaviors", the College of Design, [[Hanyang University]] and Design Research & Education Lab, [[Korea]]. He has also served as a program committee member for IGIC 2012&nbsp;– International Games Innovation Conference.

He received his Master of Arts ([[MDes]]) in [[industrial design]] in 1996 with specialisation in [[computer graphics]] and animation from Advanced Computing Center for Arts and Design (ACCAD) and the Department of Industrial, Interior, and Visual Communication at [[Ohio State University]], [[Columbus, Ohio|Columbus]] [[Ohio]]. He also obtained his Bachelor of Science degree in [[Industrial Design]] from the same university in 1994. His research interests include the use of emerging [[Haptic perception|haptic]] and motion detecting techniques in the context of [[Intangible Cultural Heritage]] (ICH) and [[Digital Intangible Heritage (DIH)|Digital Intangible Heritage]]. Besides teaching [[interactive design]], [[graphic design]], interior design and concepts for [[emerging technologies]] at [[Virginia Commonwealth University]] and [[Northwestern University]] in Qatar, Khan has been teaching predominantly [[2D animation|2D]] and [[3D animation]] classes for over 12 years in the [[Persian Gulf]] region. As a PhD researcher, his research interests include the use of emerging motion-detecting techniques in the context of [[Intangible Cultural Heritage]] (ICH). He has awarded research grants such as Undergraduate Research Experience Program (UREP)<ref name=research1>[http://technology.qatar.tamu.edu/rc/2156.aspx ''• "Awarded as a Collaborative Faculty, Qatar Foundation Undergraduate Research Experience Program (UREP), "Integration of Sensors and Data Acquisition for a Motorized Wheel Chair Simulator Platform with a Virtual Environment," Faculty Research Collaboration with [[Texas A&M University]] at [[Qatar]] [[Doha]], 2011– 2012.]</ref> in 2011 and in 2013 National Priorities Research Program (NPRP)<ref name=research2>[http://qatar-news.northwestern.edu/qnrf-grant/ ''• "Muqeem Khan (2013), Awarded as a Lead Principal Investigator (LPI), National Priorities Research Program (NPRP), "Kinesthetic Learning System for Arabic Indigenous Dances," Qatar, 2013.]</ref> from Qatar National Research Funds (QNRF).

== VFX animation and mixed reality experimentation ==
<gallery>
|upright|left|Playful technology to keep cultural heritage alive&nbsp;– TED@Doha, Feb 2013
File:VFX demo reel.webm|VFX in Motion Pictures - 1996-2001
File:Ardha.webm|Arabic Sword Dance (Al Ardha) - 2012
File:Planet Earth by Muqeem Khan.webm|Planet Earth - Short Animation - 2005
</gallery>

== Artwork==
<gallery>

File:Nuqta - Calligraphy on 4X3 feet Clear Glass.jpg|Calligraphy on 4X3 feet Clear Glass - For more work:http://www.muqeemkhan.com/artsDesign.html

File:Making_of_Nuqta.jpg|Calligraphy Work on a Clear Grass with Marker - For more work:http://www.muqeemkhan.com/artsDesign.html
</gallery>

== Urdu Poetry==
<gallery>
|
|
|
|
|
|
|
</gallery>

== References ==
{{Reflist|30em}}

== External links ==

* [http://tribune.com.pk/story/950709/meet-muqeem-khan-first-pakistani-visual-effects-artist-in-hollywood/ Meet the first Pakistani visual effects artist in Hollywood], The Express Tribune, September 2015.
* [http://www.indipool.com/indias-best-design-studio-jury-panel/ India’s Best Design Studio Jury panel], POOL, international design magazine from India, May 2015.
* [http://www.houseofpakistan.com/visual-effects-artists-contribution-pakistan/  Visual Wizardry: Pakistani’s Creating Visual Effects Magic], House of Pakistan, January 2015.
* [http://dt.bh/adding-life-to-pictures/ ADDING LIFE TO PICTURES], Daily Tribune, Manama, Bahrain, April 2014.
* [http://www.muqeemkhan.com/press/Khan_QatarTribune_March_12_2014.pdf Virtual Ardha & Future of Technology], Qatar Tribune, Doha Qatar, March 2014.
* TED@Doha " Muqeem Khan: [http://talentsearch.ted.com/video/Muqeem-Khan-Playful-technology Playful technology to keep cultural heritage alive]
* [http://thepeninsulaqatar.com/qatar/238882-preserving-the-sword-dance.html Preserving the sword dance], The Peninsula, Doha, Qatar, May 2013.
* [http://www.muqeemkhan.com/press/Muqeem_Khan_Society_FEB_2013.pdf TECHNICALLY SPEAKING...], Society, Doha, Qatar, May 2013.
* [http://www.muqeemkhan.com/BrightFuture.html Bright future for gaming industry], Gulf Times, Qatar, April 2012.
*  All there for a robust film industry: Experts", Peninsula, Doha, Qatar, Oct 2011.
* [http://www.thepeninsulaqatar.com/qatar/168154-dtff-to-screen-six-films-mad%3Ee-by-nu-q-students.html DTFF to screen six films made by NU-Q students]", Peninsula, Doha, Qatar, Oct 2011.
* [http://www.muqeemkhan.com/press/Dialogue_Khan.pdf Combine Design Thinking with Digital Technology], Slogan, Karachi Pakistan, January 2011.
* [http://callcenterinfo.tmcnet.com/news/2012/09/25/6602604.htm Muqeem to bridge cultural gap], Technology Marketing Corporation (TMC), CT, USA, September 2012.
* [http://www.qatar-tribune.com/data/20120508/content.asp?section=nation4_3 Visualisation contest winners awarded], Qatar tribune, Qatar, May 2012.
* [http://www.thefreelibrary.com/Platform+expo+brings+gaming+experts+to+Doha.-a0313982864 Platform expo brings gaming experts to Doha], Gulf Times, Qatar, May 2012.
* [http://www.qatar-tribune.com/data/20120508/content.asp?section=nation4_3 TAMUQ awards winners of visualisation contest], Gulf Times, Qatar, April 2012.
* [http://gulftoday.ae/portal/767be9c7-cf63-42bc-b886-805f507c241d.aspx UAE-based filmmaker makes it to Cannes], The Gulf Today, United Arabs Emirates, April 2012.
* [http://www.timeoutdoha.com/community/features/31711-platform-doha-gaming-expo#.UjFKFhaz4os Platform Doha gaming expo], TimeOut Doha, Qatar, April 2012.
* [http://www.thepeninsulaqatar.com/qatar/168154-dtff-to-screen-six-films-made-by-nu-q-students.html DTFF to screen six films made by NU-Q students], Peninsula, Doha, Qatar, Oct 2011.

{{DEFAULTSORT:Khan, Muqeem}}
[[Category:University of New South Wales faculty]]
[[Category:Living people]]
[[Category:Visual effects artists]]
[[Category:Pakistani animators]]
[[Category:Special effects people]]
[[Category:Animation educators]]
[[Category:Pakistani graphic designers]]
[[Category:Pakistani expatriates in Qatar]]
[[Category:Pakistani artists]]
[[Category:Pakistani calligraphers]]
[[Category:Pakistani designers]]
[[Category:Pakistani educators]]
[[Category:Pakistani painters]]
[[Category:Pakistani scholars]]
[[Category:1968 births]]
[[Category:Pakistani emigrants to the United States]]
[[Category:Articles containing video clips]]
[[Category:Contemporary artists]]
[[Category:American artists of Pakistani descent]]
[[Category:Pakistani art collectors]]
[[Category:Ohio State University alumni]]
[[Category:Artists from Karachi]]
[[Category:American University of Sharjah]]
[[Category:National College of Arts alumni]]
[[Category:Northwestern University faculty]]
[[Category:Virginia Commonwealth University faculty]]
[[Category:Pakistani expatriates in the United Arab Emirates]]
[[Category:American University of Sharjah faculty]]
[[Category:Interior design]]
[[Category:Product design]]
[[Category:Industrial design]]