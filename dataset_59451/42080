<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=AM-FIB
 | image=Polaris Flying Inflatable Boat 01.jpg
 | caption=Polaris FIB hull and motor on display at [[Sun 'n Fun]] 2004
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]]
 | national origin=[[Italy]]
 | manufacturer=[[Polaris Motor]]
 | designer=
 | first flight=
 | introduced=mid-1980s
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] [[Euro|€]] [[Pound sterling|£]] (date) -->
 | developed from= 
 | variants with their own articles=[[Polaris AM-FIB]]
}}
|}
The '''Polaris FIB''' ("Flying Inflatable Boat") is an [[Italy|Italian]] [[flying boat]] [[ultralight trike]], designed and produced by [[Polaris Motor]] of [[Gubbio]]. The aircraft was introduced in the mid-1980s and remains in production. It is supplied as a complete ready-to-fly-aircraft.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 218. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 107. Pagefast Ltd, Lancaster UK, 2003. ISSN 1368-485X</ref>

==Design and development==
The FIB complies with the [[Fédération Aéronautique Internationale]] [[microlight]] category, including the category's maximum gross weight of {{convert|450|kg|lb|0|abbr=on}}. The FIB has a maximum gross weight of {{convert|406|kg|lb|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA04" />

The aircraft features a [[Flying wires|cable-braced]] [[hang glider]]-style [[high-wing]], weight-shift controls, a two-seats-in-[[tandem]] open cockpit, an [[inflatable boat]] hull and a single engine in [[pusher configuration]]. The FIB has no wheeled [[landing gear]], but as a result of customer demand it was later developed into the amphibious [[Polaris AM-FIB]].<ref name="WDLA11" />

The FIB's single surface wing is made from bolted-together [[aluminum]] tubing and covered in [[Dacron]] sailcloth. The {{convert|11.15|m|ft|1|abbr=on}} span wing is supported by a single tube-type [[kingpost]] and uses an "A" frame weight-shift control bar. The powerplant is a twin cylinder, liquid-cooled, [[two-stroke]], [[dual-ignition]] {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine.<ref name="WDLA11" />

The aircraft has an empty weight of {{convert|216|kg|lb|0|abbr=on}} and a gross weight of {{convert|406|kg|lb|0|abbr=on}}, giving a useful load of {{convert|190|kg|lb|0|abbr=on}}. With full fuel of {{convert|40|l}} the payload is {{convert|161|kg|lb|0|abbr=on}}.<ref name="WDLA11" /><ref name="WDLA04" />

The company continues to develop the design and in 2010 introduced a new hull shape to increase performance in the water and in the air.<ref name="WDLA11" />

Dimitri Delemarie, writing in ''The World Directory of Leisure Aviation 2011-12'', said of the design, "It will never win any speed records, but if there were an award for fun, it would be right up there at the top."<ref name="WDLA11" />

==Operational history==
The FIB is used by a number of government operators, including police and coastguards.<ref name="WDLA04" />

==Variants==
In the early 2000s the company offered a version with the same wing, but without a boat hull, using a conventional minimalist trike frame mounted on wheeled [[landing gear]] or optionally [[ski]]s. Even though it did not have a boat hull it was still marketed under the FIB name.<ref name="WDLA04" />
<!-- ==Aircraft on display== -->

==Specifications (FIB) ==
{{Aircraft specs
|ref=Bayerl<ref name="WDLA11" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=
|length ft=
|length in=
|length note=
|span m=11.15
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=19.6
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=216
|empty weight lb=
|empty weight note=
|gross weight kg=406
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|40|l}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 582]]
|eng1 type=twin cylinder, liquid-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=48
|eng1 hp=

|prop blade number=3
|prop name=composite
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=80
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=70
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=48
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=4.5
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=20.7
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Polaris FIB}}
*{{Official website|http://www.polarismotor.it/}}
{{Polaris Motor aircraft}}

[[Category:Italian sport aircraft 1980–1989]]
[[Category:Italian ultralight aircraft 1980–1989]]
[[Category:Single-engined pusher aircraft]]
[[Category:Ultralight trikes]]