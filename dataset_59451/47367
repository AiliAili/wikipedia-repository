{{Use dmy dates|date=July 2015}}
{{Use Australian English|date=March 2012}}
'''Daniel Askill''' (born 1977) is an Australian filmmaker and artist who uses film, photographs, [[video installation]] and sculpture in his work.<ref name="PrismLA">{{cite web|title=Prismla artist biography|url=http://www.prismla.com/exhibit/three-rituals/|work=Three Rituals Exhibit Page|accessdate=7 October 2011}}</ref> He has directed short films, [[music videos]], commercials and fashion films.<ref name="Pedestrian Interview">{{cite web|title=Daniel Askill of Collider Interview|url=http://www.pedestrian.tv/pop-culture/features/daniel-askill-of-collider-interview/3282.htm|publisher=Pedestrian.tv|accessdate=7 October 2011}}</ref><ref name="Mr Porter">{{cite web|title=MY SPACE: MR DANIEL ASKILL|url=http://www.mrporter.com/intl/journal.mrp?feature=journal_issue7feature7#|publisher=Mr. Porter|accessdate=7 October 2011}}</ref> He is currently based between Sydney and New York.<ref name="PrismLA" />

==Early life and work==
Askill was born in Sydney in 1977. He initially studied visual communication<ref>{{cite web|title=We Have Decided Not To Die Press Kit|url=http://www.wehavedecidednottodie.com/presskit.pdf|accessdate=7 October 2011}}</ref> at the [[University of Technology Sydney]] in 1997<ref name="Galleries">{{cite web|title=Where&What + Meet The Sydney Creatives Behind Collider|url=http://www.thegaleries.com/blog/article/wherewhat-meet-the-sydney-creatives-behind-collider|work=The Galeries Blog|publisher=The Galeries|accessdate=7 October 2011}}</ref> before moving to London, studying at the [[Central Saint Martins College of Art and Design]] in 1999.<ref name="Movement">{{cite web|last=Georgiou|first=Oliver|title=Science of Movement|url=http://www.theblackmail.com.au/issue/art/science-of-movement/|publisher=The Blackmail...|accessdate=7 October 2011}}</ref>
 
Working freelance in London, he was involved in design work and film direction.<ref name="Pedestrian Interview" /> Also during his time in London, he engaged in visual design work for [[Alexander McQueen]].<ref name="Dillinger">{{cite web|title=Daniel Askill: Dillinger Magazine|url=http://www.dillinger.fr/post/613755153/daniel-askill-born-in-sydney-in-1977-daniel|publisher=Dillinger Magazine|accessdate=9 October 2011}}</ref>

In 2001, Askill returned to Sydney and founded multimedia production and design studio Collider with his colleagues Andrew van der Westhuyzen and Sam Zalaiskalns.<ref name="Galleries" /> The studio has since expanded and presently has a large roster of staff and directors.

In addition to his background in film and video, Askill also has a background in music composition and performance.<ref name="Movement" /><ref>{{cite news|title=Daniel Askill's Modern Worship|url=http://www.abc.net.au/arts/stories/s3207903.htm|accessdate=7 October 2011|newspaper=ABC Arts Online|date=4 May 2011}}</ref> He also recorded an album with [[shakuhachi]] player [[Riley Lee]] when he was 19.<ref name="Dillinger" /><ref>{{cite web|title=Australian Music Centre – Riley Lee Discography Page|url=http://www.australianmusiccentre.com.au/artist/lee-riley|publisher=Australian Music Centre|accessdate=9 October 2011}}</ref> With Collider, Askill directed short films, fashion films and commercials for companies including Sony, Dior Homme and Xbox.<ref name="Movement" />

==We Have Decided Not To Die==
In 2003, Askill wrote and directed the critically acclaimed,<ref>{{cite news|last=Catsoullis|first=Jeanette|title=Alternatives to the Multiplex in 'The World According to Shorts'|url=https://movies.nytimes.com/2006/07/21/movies/21shor.html?scp=2&sq=daniel%20askill%20we%20have%20decided%20not%20to%20die&st=cse|accessdate=7 October 2011|newspaper=New York Times|date=21 July 2006}}</ref> surreal short film ''We Have Decided Not To Die''. Exhibited in various international film festivals, the film won prizes at the [[Clermont-Ferrand]] festival in France,<ref>{{cite web|last=Mair|first=Tracey|title=Australian success at Clermont-Ferrand Short Film Festival, France|url=http://afcarchive.screenaustralia.gov.au/newsandevents/mediarelease/2004/release_271.aspx|publisher=Australian Film Commission Press Releases|accessdate=7 October 2011}}</ref> Melbourne International in Australia, Brookyln International<ref>{{cite web|title=Brooklyn Film Festival film details – We Have Decided Not To Die|url=http://www.brooklynfilmfestival.org/films/detail.asp?fid=354|accessdate=9 October 2011}}</ref> and [[South by Southwest]] in the United States.<ref>{{cite web|title=Festivals and Awards: International Awards Winners 2004|url=http://www.screenaustralia.gov.au/productions/F_A_Success/Recent_Int_Awards04.aspx|publisher=Screen Australia|accessdate=7 October 2011}}</ref><ref>{{cite web|title=We Have Decided Not To Die|url=http://www.wehavedecidednottodie.com/}}</ref> The work is notable for its portrayal of the human body, ritual and use of visceral special effects. It was described by film critic Susan Shineberg in the [[Sydney Morning Herald]] as "a breathtaking, burnished triptych, it evokes a surreal, ritualistic world whose characters appear to float free of space and time".<ref>{{cite news|last=Shineberg|first=Susan|title=Ten minutes to die for|url=http://www.smh.com.au/articles/2004/02/12/1076548153494.html?from=storyrhs|accessdate=7 October 2011|newspaper=Sydney Morning Herald|date=13 February 2004}}</ref>

==Works==
Along with his video art he has also directed numerous [[music videos]] for artists such as [[Sia Furler|Sia]], 
[[These New Puritans]] and [[Phoenix (band)|Phoenix]].<ref>{{cite web|title=Update: Phoenix to work with Jordan and Daniel Askill's father, Michael|url=http://www.pedestrian.tv/music/news/update-phoenix-to-work-with-daniel-and-jordan-aski/34996.htm|publisher=Pedestrian.tv|accessdate=7 October 2011}}</ref> He directed Phoenix's Rally (2007)<ref>{{cite web|title=Phoenix – Rally|url= http://collider.com.au/projects/Phoenix_Rally/|work=Collider Projects Page}}</ref> and [[Consolation Prizes]] (2006)<ref>{{cite web|title=Phoenix – Consolation Prizes|url=http://collider.com.au/projects/Phoenix_Consolation_Prizes/|work=Collider Project Page|accessdate=7 October 2011}}</ref>

Askill was also commissioned to make films for fashion companies like [[Ksubi]],<ref>{{cite web|title=Dazed Digital: Ksubi Kolors Film|url=http://www.dazeddigital.com/fashion/article/10278/1/exclusive-ksubi-kolors-film|publisher=Dazed Digital|accessdate=9 October 2011}}</ref> [[Another Magazine]], [[Dior]]<ref>{{cite web|title=Farhenheit 32 by Daniel Askill & Hedi Slimane|url=https://www.youtube.com/watch?v=olnA0fdUFaY|accessdate=9 October 2011}}</ref> and Acne.<ref>{{cite web|title=Askill on Acne for Cruise 11 (video)|url=http://www.thevine.com.au/tv/watch/askill-on-acne-for--cruise-1120101126.aspx|publisher=The Vine|accessdate=9 October 2011}}</ref>
 
In 2009, Askill collaborated with the [[Sydney Dance Company]] in its creative work ''We Unfold''. Askill was commissioned to provide video art to screen alongside the work<ref>{{cite web|title=We Unfold|url=http://www.sydneydancecompany.com/work/current/we-unfold/|publisher=Sydney Dance Company}}</ref> which was exhibited at the Venice Biennale in 2010.<ref>{{cite news|last=Mackrell|first=Judith|title=Dance finds a perfect partner in Venice|url=https://www.theguardian.com/stage/2010/jun/07/venice-contemporary-dance-festival|accessdate=7 October 2011|newspaper=The Guardian|date=7 June 2010}}</ref>
 
Askill's most recent, solo exhibitions ''Modern Worship'' (2011) and ''Three Rituals'' (2011) have been exhibited in Los Angeles and Sydney. The works are described as "a meditation on the notion of ritual and how it can be viewed through the eyes of modern culture"<ref name="PrismLA" /> and use video installation, photography and sculpture.<ref name="PrismLA" />

==List of Works==

===Film===
* ''We Have Decided Not to Die'' (2003)

===Artistic Works===
* ''Modern Worship'' (2011)
* ''Slow Work on a Bright Screen (2010)''
* ''Triptych and Transforming'' (2010)
* ''Artefacts from The Fifth Ritual'' (2009)
* ''We Unfold'' (2009)
* ''Suspending Disbelief'' (2008)
* ''Angel'' (2007)

===Music videos===
* [[Sia Furler|Sia]] – [[The Greatest (Sia song)|The Greatest]] (2016)
* [[Sia Furler|Sia]] – [[Cheap Thrills (Performance Edit)]] (2016)
* [[Sia Furler|Sia]] – [[Big Girls Cry]] (2015)
* [[Sia Furler|Sia]] – [[Elastic Heart]] (2015)
* [[Sia Furler|Sia]] – [[Chandelier (Sia song)|Chandelier]] (2014)
* [[These New Puritans]] – Fragment Two (2013)
* [[WIM (band)|WIM]] – [[See You Hurry]]<ref>{{cite web|title=See You Hurry Project Page|url=http://collider.com.au/projects/WIM_See_You_Hurry/|accessdate=7 October 2011}}</ref> (2011)
* [[These New Puritans]] – We Want War (2009)<ref>{{cite web|title=We Want War Project Page|url=http://collider.com.au/projects/These_New_Puritans_We_Want_War/|accessdate=7 October 2011}}</ref>
* [[Digitalism (band)|Digitalism]] – Pogo (2007)<ref>{{cite web|title=Pogo Project Page|url=http://collider.com.au/projects/Digitalism_Pogo/|accessdate=7 October 2011}}</ref>
* [[Phoenix (band)|Phoenix]] – Rally (2007)<ref>{{cite web|title=Rally Project Page|url=http://collider.com.au/projects/Phoenix_Rally/|accessdate=7 October 2011}}</ref>
* [[Placebo (band)|Placebo]] – Follow the Cops (2006)<ref>{{cite web|title=Follow the Cops Project Page|url=http://collider.com.au/projects/Placebo_Follow_the_Cops/|accessdate=7 October 2011}}</ref>
* [[Phoenix (band)|Phoenix]] – Consolation Prizes (2006)<ref>{{cite web|title=Consolation Prizes Project Page|url=http://collider.com.au/projects/Phoenix_Consolation_Prizes/|accessdate=7 October 2011}}</ref>
* [[Unkle]] feat. Ian Brown – Reign (2005)<ref>{{cite web|title=Reign Project Page|url=http://collider.com.au/projects/Unkle_Reign/}}</ref>
* [[Sia Furler|Sia]] – [[Breathe Me]] (2004)<ref>{{cite web|title=Breathe Me Project Page|url=http://collider.com.au/projects/Sia_Breathe_Me/}}</ref>

===Commercials===

Askill has also directed a number of commercials for companies including, Sony, BMW, Dior Homme, and Xbox.<ref name="Movement" />

* [[Sony]] – New York City Ballet
* [[Smirnoff]] – Purity
* [[BMW]] – Climate Control
* [[History (U.S. TV channel)|History Channel]] – Hero
* [[Xbox]] – Faces
* Dior Homme – [[Jude Law]]
* [[Cadillac]] – Roll
* [[Airfrance]] – Swimming Pool
* [[Lexus]] – Merge
* [[Hummer]] – Chairs
* [[Panasonic]]  – Olympic Art

==Selected Exhibitions and screenings==

* Prism, Los Angeles- ''Three Rituals'' – 2011<ref name="PrismLA" />
* Gallery A.S, Sydney – ''Modern Worship'' – 2011<ref>{{cite web|title=Daniel Askill Modern Worship|url=http://sydney.concreteplayground.com.au/event/18812/daniel-askill-modern-worship.htm|work=Concrete Playground}}</ref>
* Nam June Paik Art Center, Seoul, Korea- ''Souvenirs from Earth'' – 2011<ref>{{cite web|title=Souvenirs From Earth Artist Biography – Daniel Askill|url=http://www.souvenirsfromearth.tv/artists/maske.php?id=268|accessdate=7 October 2011}}</ref>
* Venice Biennale, Venice, Italy – ''We Unfold'' – 2010<ref>{{cite web|title=Event page – Sydney Dance Company "We Unfold"|url=http://www.labiennale.org/en/dance/archive/festival/program/sydney-dance1.html?back=true|work=Venice Biennale Programme|accessdate=10 October 2011}}</ref>
* World Expo, Shanghai, China – ''We Unfold'' – 2010<ref>{{cite news|last=Machalias|first=Helen|title=we unfold {{!}} Sydney Dance Company|url=http://www.australianstage.com.au/201005113485/reviews/canberra/we-unfold-%7C-sydney-dance-company.html|accessdate=7 October 2011|newspaper=Australian Stage|date=11 May 2010}}</ref>
* [[ASVOFF]] Centre Pompidou, Paris, France- ''Nocturna'' – 2010<ref>{{cite web|title=ASVOFF 3 Winners|url=http://www.ashadedviewonfashionfilm.com/IMG/pdf/ASVOFF_3_WINNERS.pdf|publisher=ASVOFF|accessdate=10 October 2011}}</ref>
* RAFW, Sydney- ''Suspending Disbelief'' – 2009
* Monster Children Gallery, Sydney- ''Artefacts From The Fifth Ritual'' – 2009<ref>{{cite web|title=Monster Children – Exhibition by Daniel Askill.|url=http://monsterchildren.com/contributors/monster-children/artefacts-from-the-fifth-ritual-an-exhibition-by-daniel-askill/|accessdate=10 October 2011}}</ref>
* Palais de Tokyo, Paris – ''France Artcurial for Amnesty International'' – 2008<ref>{{cite web|title=http://artforum.com/news/week=200850|url=http://artforum.com/news/week=200850|work=International News Digest 2008|publisher=ARTFORUM}}</ref>
* [[:de:Werkleitz Gesellschaft|Werkleitz Biennale, Halle, Germany]] – ''Happy Believers'' – 2006 (group)<ref>{{cite web|title=Werkleitz Biennale 2006 Programme|url=http://biennale2006.werkleitz.de/html_en/pro_so_askill.html|accessdate=7 October 2011}}</ref>
* Institute of Contemporary Art, London, UK – ''onedotzero'' – 2004<ref>{{cite web|title=onedotzero – We Have Decided Not To Die|url=http://www2.onedotzero.com/track.php?id=65&dvd=2810308|accessdate=10 October 2011}}</ref>
* ARTSPACE, Sydney, Australia – ''We Have Decided Not To Die'' – 2003

== References ==
{{Reflist|2}}

== External links ==
* [http://www.danielaskill.com/ Daniel Askill's homepage]
* [http://collider.com.au/ Collider]
* [http://www.wehavedecidednottodie.com/ We Have Decided Not To Die]
* [http://www.prismla.com/exhibit/three-rituals/ Three Rituals exhibit at Prism LA]
* [http://www.mrporter.com/intl/journal.mrp?feature=journal_issue7feature7# Interview with Daniel Askill at Mr Porter]
* [http://www.abc.net.au/arts/stories/s3207903.htm ABC Arts Online: Daniel Askill's Modern Worship]

{{DEFAULTSORT:Askill, Daniel}}
[[Category:Articles created via the Article Wizard]]
[[Category:Artists from Sydney]]
[[Category:Australian film directors]]
[[Category:Australian filmmakers]]
[[Category:1977 births]]
[[Category:Living people]]
[[Category:University of Technology Sydney alumni]]
[[Category:Alumni of Central Saint Martins]]