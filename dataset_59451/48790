Between 1665 and 1670, seven '''Iroquois settlements on the north shore of [[Lake Ontario]] '''in present-day [[Ontario]], collectively known as the “Iroquois du Nord” villages, were established by [[Seneca people|Senecas]], [[Cayuga people|Cayugas]],and [[Oneida people|Oneidas]]. The villages consisted of Ganneious, Kente, Kentsio, Ganaraske, [[Ganatsekwyagon]], [[Teiaiagon]], and Quinaouatoua. The villages were all abandoned by 1701.{{sfnp|Jordan|2013|p=37}}

== Arrival ==

[[File:Iroquois Settlement on the north shore of Lake Ontario 1665-1701.tiff|thumbnail|left| Iroquois Settlement on the north shore of Lake Ontario 1665–1701]]

The northern shores of [[Lake Ontario]] were first settled as early as eleven thousand years ago. While humans have lived along the northern shores of [[Lake Ontario]] for a long time, they have not been continuously settled. The [[Huron-Wendat Nation|Huron-Wendat]] had developed a distinct homeland along the northern shores of Lake Ontario in the 15th century, but moved north toward [[Georgian Bay]] by 1615, abandoning the northern shores of Lake Ontario.{{sfnp|Pendergast|1998}}  The Iroquois raided the Huron in Ontario during the first half of the 17th century and began to establish greater control over the hunting grounds that existed between Lake Ontario and Lake Simcoe. By the 1640s the Huron-Wendat population had been reduced considerably by [[Epidemic|epidemics]]. In 1649, the Iroquois defeated the [[Huron-Wendat Nation|Huron]], [[Wyandot people|Petun]], and then the [[Neutral Nation|Neutral]], effectively destroying their enemies in Ontario. {{sfnp|Myrvold|1997|p=13-14}}

After the destruction of the Huron in southern Ontario the Iroquois began to make more frequent excursions on the northern shores of Lake Ontario. In the 1660s, the Iroquois began to expand their settlements north. A number of theories try to explain why the Iroquois began settling the northern shores of Lake Ontario. Economic reasons are considered the strongest motivation. By the 1640s the [[beaver]] had disappeared through over hunting in the traditional Iroquois homeland in modern-day [[New York (state)|New York]] state. The Iroquois were competing with both the Huron, [[Ottawa (people)|Ottawa]], and [[Algonquin people|Algonquin]]s for the [[Fur trade in North America|fur trade]]. By establishing settlements on the northern shores of Lake Ontario the Iroquois were able to re-establish control on the flow of furs from the north and west towards [[Albany, New York|Albany]] and [[Montreal]].{{sfnp|Adams|1986|p=8}}

The Iroquois settlement into Ontario was part of a broader expansion of Iroquois groups in the mid 17th century. During this time the Iroquois also moved into what is today [[Ohio]], [[Pennsylvania]], and [[Quebec]]. Often these settlements were significantly closer to European settlements and have been characterized as Iroquois [[Colonies]]. {{sfnp|Jordan|2013|p=37-39}}

== Description ==

The seven villages that were settled on the northern shores of Lake Ontario from east to west are:

* Ganneious - on the site of present day [[Napanee]]
* Kente - on the [[Bay of Quinte]]
* Kentsio - on [[Rice Lake (Ontario)|Rice Lake]]
* Ganaraske - on the site of present day [[Port Hope, Ontario|Port Hope]]
* [[Bead Hill archaeological site|Ganatsekwyagon]] - at the mouth of the [[Rouge River (Ontario)|Rouge River]]
* [[Teiaiagon]] - at the mouth of the [[Humber River (Ontario)|Humber River]]
* Quinaouatoua (or Tinawatawa) - Near modern day [[Hamilton, Ontario|Hamilton]]

Little is known about the seven villages due to an absence of detailed archaeological evidence. The most comprehensive archaeological evidence gathered to date is from the [[Bead Hill archaeological site]], which is believed to be the site of Ganatsekwyagon along the [[Rouge River (Ontario)|Rouge River]].

The villages do share some common traits that are evident from the available sources. The northern villages were likely seasons campsite prior to becoming larger settlements. They are located at strategic points controlling access to Lake Ontario and near seasonally abundant fish and games. Six of the seven villages are all located on the best agricultural land found along the northern shores of the lake, according to the [[Canada Land Inventory]]. The Iroquois likely grew [[Squash (plant)|squash]], [[corn]], and [[beans]].{{sfnp|Adams|1986|p=8}}

The villages were also significant staging points for hunting parties moving north and for fur trading. The villages were the scene of extensive trade between both French, Dutch, English, and Ottawa traders and the Iroquois. The villages were also the site of violence due the exchange of fur for liquor. There are number of incidents that record instances of maiming and death at  Ganneious, Teiaiagon, and Ganatsekwyagon due to drinking.

As in other Iroquois settlements longhouses were in parallel to each other and surrounded by palisades. The estimated size of the villages varies from 500 to 800 persons. The villages would have had 20 to 30 structures.  Quinaouatoua, was perhaps the smallest with a population of less than 100 in the fall of 1669.{{sfnp|Konrad|1981|p=138}}

The villages were settled by different tribes. The Seneca settled the westernmost villages of Quinaouatoua, [[Teiaiagon]], and [[Ganatsekwyagon]]. The [[Cayuga people|Cayuga]] settled Ganaraske,  Kente, and Kentsio, and the [[Oneida people|Oneida]] settled Ganneious along the eastern edge of the lake.  The villages were connected to each other by a system of trail and water routes.

While each village is identified with one group, there is a strong likelihood that the villages continued a common Iroquois practice of incorporating and adopting large groups of outsiders into settlements. For example, a [[Neutral Nation|Neutral]] style longhouse was found at the [[Bead Hill archaeological site]], which was initially settled by the [[Seneca people|Seneca]]. Existing texts also characterized populations in Iroquois communities distant from the homeland as being multinational and multilingual.{{sfnp|Jordan|2013|p=37-39}}

== Relations with the French ==

In 1668, the French began to visit the Iroquois villages to convert the local population to [[Christianity]]. Abbé Trouvé and [[François de Salignac de la Mothe-Fénelon (missionary)|François de Salignac de la Mothe-Fénelon]] were sent by [[François de Laval]] from Montreal to establish a [[Sulpician]] mission in the village of Kente. The mission was deserted in 1680 due to a lack of success and funding.<ref>{{cite web|title=Settlers to Prince Edward County: A historical examination of who they were and why they came|url=http://navalmarinearchive.com/research/settlers/set_page2.html|publisher=Naval Marine Archive: The Canadian Collection|accessdate=5 February 2014|ref=harv}}</ref> [[François de Salignac de la Mothe-Fénelon (missionary)|Abbé Fénelon]] then went on a tour of other villages and would spend the winter of 1669 in the village of Ganatsekwyagon.  François-Saturnin Lascaris d'Urfé visited a number of the towns on the North shore of Lake Ontario. French explorers Pere and Joilet also passed through the village of [[Ganatsekwyagon]] in 1669 on their way to [[Lake Superior]].{{sfn|Marcel}}

Relations between the Iroquois du Nord and the French were tense due the intermediate conflicts known as the [[Beaver Wars]]. The villages were settled during a time of relative peace. In 1673 when the French established their first settlement along Lake Ontario, [[Fort Frontenac]], in present day [[Kingston, Ontario]], many Iroquois from the nearby village of Ganneious resettled closer to the Fort.{{sfnp|Adams|1986|p=9}} Relations deteriorated as the political situation in present day [[New York (state)|New York]] state changed, and in 1687 the French attacked the Iroquois, destroying villages in both New York state and along the northern shores of Lake Ontario.

The establishment of Fort Frontenac also appears to have shifted influence from Ganatsekwyagon to Teiaiagon. Most evidence indicates that Ganatsekwyagon was the more important settlement on the north shore due to its strategic position on the Rouge River arm of the [[Toronto Carrying-Place Trail]]. Following the construction of Fort Frontenac, Teiaiagon became more travelled for two reasons. First, the construction of the fort shifted the Iroquois toward the western route around Lake Ontario and second the French anchored at Teiaiagon instead of Ganatsekwyagon due to the superior anchorage for French trade barques.{{sfnp|Konrad|1981|p=134}}

== Abandonment ==

In [[Anishinaabe]] oral tradition holds that the Iroquois abandoned their villages north of Lake Ontario following a number of decisive battles won by the [[Anishinaabe]] in south and central Ontario during the [[Beaver Wars]]. In the [[Great Peace of Montreal]], signed in 1701, the Iroquois Confederacy agreed to remain on the south shore of Lake Ontario. By 1701 the [[Anishinaabe]] group called the [[Mississaugas|Mississauga]] had moved into the area between [[Lake Erie]] and the [[Rouge River (Ontario)|Rouge River]].{{sfnp|Fairburn|2013|p=36}}

The easternmost villages of Kente and Ganneious were reportedly destroyed in 1687 by [[Jacques-René de Brisay de Denonville, Marquis de Denonville|Jacques Rene de Brisay de Denonville]]. His troops took 200 prisoners from both villages, to fight in the Beaver Wars, before destroying them.{{sfnp|Barr|2006|p=87}} There are no accounts on the fate or condition of either Ganatsekwyagon or [[Teiaiagon]] after fighting broke out in 1687. It is assumed that, since both villages were no longer secure, they were abandoned some weeks earlier and the inhabitants fled to the south shore of [[Lake Ontario]].{{sfnp|Myrvold|1997|p=18}}

Following the abandonment of the north of Lake Ontario by Iroquois some French geographers incorrectly place the Iroqouis du Nord and their villages on maps of southern Ontario as late as 1755. This would cause confusion among historians in the future when the [[Mississaugas|Mississauga]] took possession of the northern shore of Lake Ontario.{{sfnp|Schmalz|1991|p=29}}

== Historical maps showing Iroquois settlement on the north shore ==

The following maps show evidence of the Iroquois settlements on the north shore of Lake Ontario.{{sfnp|Konrad|1981|p=130}}

*Plans des forts faicts par le RegimentlCarignan salieres sur la Riviere de/Richelieu dicte autrement des Iroquois en/la Nouvelle France. Le Mercier. 1666. 1 printed map. France, Minis&e des Colonies, No. 493; Public Archives of Canada (hereafter PAC), National Map Collection (hereafter NMC), H3/901/1666 
*“Carte du Lac Ontario . . .,” Galinee. 1670. 1 ms. map. France, Archives des Cartes et Plans de la Marine imperiale; PAC, NMC, A/902/1670. In: Plans, Cartes, Vues et Dessins relatifs h 1’Histoire de la Nouvelle France. P. L. Morin. Paris, 1852–53, V. 1, No. 15; J. H. Coyne “Exploration of the Great Lakes, 1669-70 . . .” Ontario Historical Society Papers and Records, V. 4, Toronto, [1903] 
*“Carte de la decouverte du Sr Jolliet . . .,” [Jolliet]. [1674]. 1 ms. map France, Bibliothèque Nationale (hereafter BN), Service hydrographique, Recueil 67, NO. 52; PAC, NMC, Ph/900 [1674-1701] 
*“Carte de la descouverte du Sr Jolliet . . .,” [Jolliet]. [1674-51. 1 ms. map. France, Service Historique de la Marine, Bibliothèque, (hereafter SHM, B), 4044B, No. 37; PAC, NMC, H2/903/ [1675] 
*“Carte g[e]ne[ra]lle de la France septentrionnale, contenant la decouverte du Pays des Ilinois . . .,” [Franquelin]. 1678. 1 ms. map. France, SHM, B, Recueil66, No. 11; PAC, NMC, H3/900/[16781]
*“Carte/pour servir a l’Cclaircissement/du Papier Terrier/de la Nouvelle France”, Franquelin. 1678. 1 section of a ms. map. France, BN, Cartes et plans, Service hydrographique, pf. 125, div. 1, p.&nbsp;1; PAC, NMC, H2/900/1678 
*“Cartes des Grands Lacs . . .,” Franquelin. 1679. 1 ms. map. France, Depot des cartes et plans de la Marine, Service hydrographique, Bibliothèque (hereafter DMSH, B) Recueil 67, No. 43; PAC, NMC, H3/902/1679 
*“Carte du tours du Saint-Laurent . . .,” Belmont. 1680. 1 ms. map. France, BN, Geographic, Ge. DD. 2989, Ministere des affaires etrangeres, Depot geographique, Archives, No. 8662; PAC, NMC, H1/902/[1680] 
*[Carte des Grands Lacs] “Lac Ontario/Ott/De Frontenac.” Bernou. 1680. 1 of a series of 6 ms. maps. France, DMSH, B, Recueil 67, No. 47; PAC, NMC, H3/902/[1680]

==See also==
* [[St. Lawrence Iroquoians]]

== Notes ==

{{reflist|2}}

== References ==

{{refbegin}}
*{{cite journal|last=Adams|first=Nick|title=Iroquois Settlement at Fort Frontenac in the Seventeenth and Early Eighteenth Centuries|journal=Ontario Archaeology|year=1986|volume=46|url=http://www.ontarioarchaeology.on.ca/publications/pdf/oa46-1-adams.pdf|accessdate=4 February 2014|ref=harv}}  
*{{cite book|last=Barr|first=Daniel P.|title=Unconquered: The Iroquois League at War in Colonial America|year=2006|publisher=Praeger Publishers|location=Westport CT|isbn=978-0275984663|url=https://books.google.com/?id=vi1ROx0PmI4C&pg=PA87&dq=Ganneious#v=onepage&q=Ganneious&f=false|ref=harv}}
*{{cite book|last=Fairburn|first=Jane|title=Along the Shore: Rediscovering Toronto's Waterfront Heritage|year=2013|publisher=ECW Press|location=Toronto|isbn=978-1770410992|url=https://books.google.com/?id=Cc3yAAAAQBAJ&pg=PA36&lpg=PA36&dq=Ganatsekwyagon#v=onepage&q=Ganatsekwyagon&f=false|ref=harv}}
*{{cite journal|last=Jordan|first=Kurt A.|title=Incorporation and Colonization: Postcolumbian Iroquois Satellite Communities and Processes of Indigenous Autonomy|journal=American Athropologist|year=2013|volume=115|issue=1|ref=harv}}
*{{cite journal|last=Konrad|first=Victor|title=An Iroquois frontier: the north shore of Lake Ontario during the late seventeenth century|journal=Journal of Historical Geography|year=1981|volume=7|issue=2|pages=129–144|ref=harv|doi=10.1016/0305-7488(81)90116-x}}
*{{cite web|last=Marcel|first=C.M.W|title=Iroquois origins of modern Toronto|url=http://www.counterweights.ca/2006/08/iroquois/|publisher=counterweights|accessdate=2 February 2014|ref=harv}}
*{{cite book|last=Myrvold|first=Barbara|title=The people of Scarborough : a history|year=1997|publisher=City of Scarborough Public Library Board|location=Toronto|isbn=0968308600|url=http://static.torontopubliclibrary.ca/da/pdfs/238353.pdf|ref=harv}}
*{{cite journal|last=Pendergast|first=James F.|title=The Confusing Identities Attributed to Stadacona and Hochelaga|journal=Journal of Canadian Studies|year=1998|volume=32|issue=4|ref= harv}}
*{{cite book|last=Schmalz|first=Peter S.|title=The Ojibwa of Southern Ontario|year=1991|publisher=University of Toronto Press|location=Toronto|isbn=978-0802067784|url=https://books.google.com/?id=espKE9_839wC&pg=PA29&dq=iroquois+du+nord#v=onepage&q=iroquois%20du%20nord&f=false|ref=harv}}
*{{cite web|title=Settlers to Prince Edward County: A historical examination of who they were and why they came|url=http://navalmarinearchive.com/research/settlers/set_page2.html|publisher=Naval Marine Archive: The Canadian Collection|accessdate=5 February 2014|ref=harv}}

{{Iroquois Confederacy}}
{{Iroquois settlement of the northern shores of Lake Ontario}}

{{DEFAULTSORT:Iroquois}}
[[Category:Iroquois]]
[[Category:Indigenous peoples of the Northeastern Woodlands]]
[[Category:History of Ontario by location]]
[[Category:Seneca tribe]]
[[Category:First Nations history in Ontario]]
[[Category:Iroquois populated places]]
[[Category:Former populated places in Ontario]]
[[Category:History of indigenous peoples of North America]]
[[Category:Human migrations]]