<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = R-760 Whirlwind
 |image = N3N female mechanic 1942 535576.jpg
 |caption = Female mechanic working on the R-760 engine of a U.S. Navy N3N trainer in October 1942
}}
{{Infobox Aircraft Engine
 |type=Air-cooled 7-cylinder [[Radial engine|radial piston engine]]
 |manufacturer=[[Wright Aeronautical Corporation]]<br>[[Naval Aircraft Factory]] (under license)
 |national origin=United States
 |major applications=[[N3N Canary]] U.S. Navy primary trainer<br>Other U.S. Navy trainers<br>Various civil utility aircraft
 |produced={{avyear|1929}}-{{avyear|1945}}
 |number built=around 1400
}}
|}

The '''Wright R-760 Whirlwind''' was a series of seven-cylinder air-cooled [[radial engine|radial]] [[aircraft engine]]s built by the [[Wright Aeronautical]] division of [[Curtiss-Wright]]. These engines had a [[Engine displacement|displacement]] of 756&nbsp;in³ (12.4&nbsp;L) and power ratings of 225-350&nbsp;hp (168-261&nbsp;kW).<ref>Gunston 1989, p.181.</ref>

==Design and development==
Wright introduced the J-6 Whirlwind family in 1928 to replace the nine-cylinder [[Wright R-790|R-790]] series. The J-6 family included varieties with five, seven, and nine cylinders.  The seven-cylinder version was originally known as the '''J-6 Whirlwind Seven''', or '''J-6-7''' for short.  The U.S. government designated it as the '''R-760'''; Wright later adopted this and dropped the J-6 nomenclature.

Like all the members of the J-6 Whirlwind family, the R-760 had larger cylinders than the R-790. The piston [[stroke (engine)|stroke]] of 5.5&nbsp;in (14.0&nbsp;cm) was unchanged, but the cylinder [[bore (engine)|bore]] was expanded to 5.0&nbsp;in (12.7&nbsp;cm) from the R-790's bore of 4.5&nbsp;in (11.4&nbsp;cm). While the R-790 was [[Naturally-aspirated engine|naturally aspirated]], the R-760, like the other J-6 engines, had a gear-driven [[supercharger]] to boost its power output.

Over time, Wright refined the R-760, using suffix letters to indicate successive versions. The original R-760 (or J-6-7) was rated for 225&nbsp;hp (168&nbsp;kW),<ref name=CW40p11>Curtiss-Wright (1940), p. 11</ref> while the R-760E of 1931 could do 250&nbsp;hp (186&nbsp;kW) thanks to an improved [[cylinder head]] design.<ref name=CW83>Curtiss-Wright (1983), p. 1</ref><ref name=CW40p13>Curtiss-Wright (1940), p. 13</ref><ref name=ATC26>FAA Type Certificate Data Sheet ATC 26</ref> Wright later added another suffix to show different power levels. The R-760E-1, introduced the same year as the R-760E, had a takeoff power rating of 300&nbsp;hp (224&nbsp;kW) thanks to [[Compression ratio|higher-compression]] pistons and a greater [[RPM]] limit.<ref name=CW83 /><ref name=CW40p14>Curtiss-Wright (1940), p. 14</ref><ref name=TC94>FAA Type Certificate Data Sheet TC 94</ref> The even more powerful R-760E-2 of 1935 could reach 350&nbsp;hp (261&nbsp;kW) for takeoff due to increased supercharging and an even higher RPM limit.<ref name=CW83 /><ref name=CW40p17>Curtiss-Wright (1940), p. 17</ref><ref name=TC155>FAA Type Certificate Data Sheet TC 155</ref> On the other hand, the R-760E-T, designed for trainer aircraft, had the R-760E-1's high-compression pistons, but the supercharger was removed, thus giving just 235&nbsp;hp (175&nbsp;kW).<ref name=CW83 /><ref name=CW40p16>Curtiss-Wright (1940), p. 16</ref><ref name=TC126>FAA Type Certificate Data Sheet TC 126</ref>

==Operational history==

The R-760 was a direct replacement for the R-790, with similar displacement and power. The U.S. Navy used it as the powerplant for several biplane [[Trainer (aircraft)|primary trainers]], including the [[Consolidated NY]], the [[Curtiss Fledgling|Curtiss N2C Fledgling]], and the [[Naval Aircraft Factory]] [[N3N Canary]]. The last of these was produced in large numbers, with most of the engines built under license by the Naval Aircraft Factory. Trainers usually had the unsupercharged R-760E-T engine.

A variety of civil [[utility aircraft]] also used the R-760, including models built by [[Beechcraft]], [[Cessna]], [[Curtiss-Wright]], [[Howard DGA-8]], [[Stearman Aircraft|Stearman]], [[Stinson Aircraft Company|Stinson]], and [[Waco Aircraft Company|Waco]]. These aircraft generally used the various supercharged versions of the R-760.

Production of the R-760 continued until 1945, with about 1400 examples being built by Wright, and more under licence by foreign manufacturers such as [[Fábrica Nacional de Motores]] in [[Brazil]].<ref name=CW83 /><ref>{{Citation|title=Summary of Wright Engine Shipments: 1920 to 1963|url=http://enginehistory.org/Wright/WrightProd.pdf|format=PDF|accessdate=December 10, 2009}}. Transcribed from Wright Aeronautical documents by Robert J. Neal T; available from the Aircraft Engine Historical Society's [http://enginehistory.org/reference.htm reference page].</ref>

==Variants==
* '''J-6-7 (R-760)''': 225&nbsp;hp (168&nbsp;kW) at 2,000 RPM.<ref name=CW40p11 />
* '''R-760E''': 250&nbsp;hp (186&nbsp;kW) at 2,000 RPM. Higher power from improved cylinder head.<ref name=CW83 /><ref name=CW40p13 /><ref name=ATC26 />
* '''R-760E-1''': 285&nbsp;hp (213&nbsp;kW) at 2,100 RPM, 300&nbsp;hp (224&nbsp;kW) at 2,250 RPM for takeoff. Higher compression ratio.<ref name=CW83 /><ref name=CW40p14 /><ref name=TC94 />
* '''R-760E-2''': 320&nbsp;hp (239&nbsp;kW) at 2,200 RPM, 350&nbsp;hp (261&nbsp;kW) at 2,400 RPM for takeoff. Increased supercharging, slightly higher compression ratio.<ref name=CW83 /><ref name=CW40p17 /><ref name=TC155 />
* '''R-760E-T''': 235&nbsp;hp (175&nbsp;kW) at 2,000 RPM. Unsupercharged version of R-760E-1 for trainer aircraft.<ref name=CW83 /><ref name=CW40p16 /><ref name=TC126 />
* '''R-760-2''', '''-4''', '''-8''': 235&nbsp;hp (175&nbsp;kW) at 2,000 RPM. U.S. Navy versions of R-760E-T.<ref name=CW83 /><ref>FAA Type Certificate Data Sheet 5E-6</ref>

==Applications==
*[[Abrams P-1 Explorer]]
*[[Beechcraft Staggerwing]]
*[[Cessna DC-6|Cessna DC-6B Scout]]
*[[Consolidated NY|Consolidated NY-3]]
*[[Curtiss Fledgling|Curtiss N2C-2 Fledgling]]
*[[Fairchild Model 45]]
*[[Howard DGA-8]] and DGA-15W
*[[Naval Aircraft Factory N3N Canary]]
*[[St. Louis YPT-15]]
*[[Stearman C3#Variants|Stearman C3R Business Speedster]]
*[[Stinson Junior]]
*[[Stinson Reliant]]
*[[Waco 10|Waco CSO and CTO]]
*[[Waco Standard Cabin series|Waco CJC, CJC-S, DJC, DJC-S, and DJS]] 
*[[Waco Custom Cabin Series|Waco CUC, DQC-6, EQC-6, DGC-7 & 8, EGC-7 & 8]] 
*[[Waco G series|Waco CRG]]

==Engines on display==
Wright R-760 engines on display are uncommon, but there is an R-760E-2 exhibited at the [[Evergreen Aviation & Space Museum]] in [[McMinnville, Oregon]].<ref>{{Citation|title=AEHS List: Engines in Museums Worldwide|url=http://www.enginehistory.org/aehs_list.htm#Evergreen%20Aviation%20Museum|accessdate=2009-12-13}}.</ref><ref>{{Citation|title=Aircraft Engines|url=http://passion-aviation.qc.ca/engines.htm|accessdate=2009-12-13}}. This personal collection of museum aircraft engine photos includes a photo of the museum's R-760E-2 under the section for the Evergreen Aviation Museum.</ref>

==Specifications (R-760E-2)==
{{pistonspecs|
<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=[[FAA]] [[type certificate]] data sheet for the R-760E-2;<ref name=TC155 /> dimensions from Curtiss-Wright (1983).
|type=7-cylinder supercharged air-cooled radial piston engine
|bore=5.0 in (127 mm)
|stroke=5.5 in (140 mm)
|displacement=756 in³ (12.4 L)
|length=42.44 in (107.8 cm)
|diameter=45.0 in (114.3 cm)
|width=
|height=
|weight=570 lb (259 kg)
|valvetrain=2 [[4-stroke cycle engine valves|valves]] per cylinder, [[pushrod]]-actuated
|supercharger=gear-driven, 9.17:1 [[impeller]] [[gear ratio]]
|turbocharger=
|fuelsystem=
|fueltype=80 [[Octane rating|octane]]
|oilsystem=
|coolingsystem=
|power=320 hp (239 kW) at 2,200 RPM at sea level; 350 hp (261 kW) at 2,400 RPM for takeoff
|specpower=0.42 hp/cu-in (19.3 kW/L)
|compression=6.3:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight=0.56 hp/lb (0.92 kW/kg)
|designer=
|reduction_gear=Direct drive
|general_other=
|components_other=
|performance_other=
}}

{| class="wikitable" style="text-align:center"
|+ Specifications for different R-760 variants
|-
! Engine !! Power, continuous !! Power, takeoff !! Compression ratio !! Supercharger gear ratio !! Octane rating !! Dry weight
|-
! R-760E<ref name=ATC26 />
| 250&nbsp;hp (186&nbsp;kW) at 2,000 RPM || || 5.1:1 || 7.05:1 || 73 || 530&nbsp;lb (240&nbsp;kg)
|-
! R-760E-1<ref name=TC94 />
| 285&nbsp;hp (213&nbsp;kW) at 2,100 RPM || 300&nbsp;hp (224&nbsp;kW) at 2,250 RPM || 6.1:1 || 7.05:1 || 73 || 565&nbsp;lb (256&nbsp;kg)
|-
! R-760E-2<ref name=TC155 />
| 320&nbsp;hp (239&nbsp;kW) at 2,200 RPM || 350&nbsp;hp (261&nbsp;kW) at 2,400 RPM || 6.3:1 || 9.17:1 || 80 || 570&nbsp;lb (259&nbsp;kg)
|-
! R-760E-T<ref name=TC126 />
| 235&nbsp;hp (175&nbsp;kW) at 2,000 RPM || || 6.1:1 || none || 73 || 540&nbsp;lb (245&nbsp;kg)
|}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=
* [[Wright Whirlwind]]
<!-- designs which were developed into or from this aircraft: -->
|related=
*[[Wright R-790|Wright J-5/R-790]]
*[[Wright R-540|Wright J-6-5/R-540]]
*[[Wright R-975|Wright J-6-9/R-975]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
* [[List of aircraft engines]]

<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Armstrong Siddeley Cheetah]]
*[[Jacobs R-755]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*Gunston, Bill. ''World Encyclopedia of Aero Engines''. Cambridge, England. Patrick Stephens Limited, 1989. ISBN 1-85260-163-9
*''Jane's Fighting Aircraft of World War II''. London. Studio Editions Ltd, 1989. ISBN 0-517-67964-7
*{{Citation|title=Wright Engines - Past and Present|url=http://enginehistory.org/Wright/CWthrough1940.pdf|format=PDF|last=Curtiss-Wright|author-link=Curtiss-Wright|year=1940|pages=11, 13, 14, 16, 17|accessdate=December 12, 2009}}. Available from the Aircraft Engine Historical Society's [http://enginehistory.org/reference.htm reference page].
*{{Citation|title=Historical Engine Summary (Beginning 1930)|url=http://enginehistory.org/Wright/CWafter1930_2.pdf|format=PDF|last=Curtiss-Wright|author-link=Curtiss-Wright|year=1983|page=1|accessdate=December 12, 2009}}. Available from the Aircraft Engine Historical Society's [http://enginehistory.org/reference.htm reference page].
The following [[Federal Aviation Administration]] [[type certificate]] data sheets, all available from the FAA's [http://rgl.faa.gov Regulatory and Guidance Library]:
*R-760E: {{Citation|title=Type Certificate Data Sheet ATC 26|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library%5CrgMakeModel.nsf/0/39701E9ED6D9711E862572C0006DAE05?OpenDocument|accessdate=December 12, 2009}}.
*R-760E-1: {{Citation|title=Type Certificate Data Sheet TC 94|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library%5CrgMakeModel.nsf/0/C977C51BF88A3FD78525670E0066874E?OpenDocument|accessdate=December 12, 2009}}.
*R-760E-2: {{Citation|title=Type Certificate Data Sheet TC 155|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library%5CrgMakeModel.nsf/0/CBA8CD496573FDAE8525670E00668E3B?OpenDocument|accessdate=December 12, 2009}}.
*R-760E-T: {{Citation|title=Type Certificate Data Sheet TC 126|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library%5CrgMakeModel.nsf/0/44F8133CCDA8C47C8525670E006694EB?OpenDocument|accessdate=December 12, 2009}}.
*R-760-2, -4, -8: {{Citation|title=Type Certificate Data Sheet 5E-6|url=http://rgl.faa.gov/Regulatory_and_Guidance_Library%5CrgMakeModel.nsf/0/D5E437989E4DAFE48525676D004ABF35?OpenDocument|accessdate=December 12, 2009}}.
{{refend}}

==External links==
{{commons category}}
* Engine Data Sheets: US Aero Engines — [http://www.oldengine.org/members/diesel/Duxford/usaero16.htm first R-760 page], [http://www.oldengine.org/members/diesel/Duxford/usaero17.htm second R-760 page]

{{Wright aeroengines}}
{{US military piston aeroengines}}

[[Category:Radial engines]]
[[Category:Wright aircraft engines|R-760]]
[[Category:Aircraft piston engines 1920–1929]]