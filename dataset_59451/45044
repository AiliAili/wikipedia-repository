{{Use dmy dates|date=August 2011}}
{{Use Australian English|date=August 2011}}
{{Infobox Bridge |
|image=TomDiverDerrickBridgeJuly08.jpg
|bridge_name='Diver' Derrick Bridge
|official_name=Tom 'Diver' Derrick Bridge
|location=[[Port Adelaide]], [[Australia]]
|carries=Motor vehicles, pedestrians and bicycles
|crosses=[[Port River]]
|open=Pedestrians - 01/08/2008, Road Traffic - 03/08/2008
|design=Single-leaf [[Bascule bridge]]
|width=4 lanes
|length=57.8 metres (189&nbsp;ft)
|height=10 metres (32&nbsp;ft)
|coordinates={{coord|-34.838283|138.506806|type:landmark|display=title,inline}}
}}

The '''Tom 'Diver' Derrick Bridge''', commonly referred to as the 'Diver' Derrick Bridge, is an opening single-leaf [[bascule bridge]] over the [[Port River]], [[Port Adelaide]]. It was opened for traffic on 3 August 2008. The entire bridge project cost approximately $178 million. It is located between Docks 1 and 2 at Port Adelaide and links to Francis Street to the east and Victoria Road to the west.<ref>{{cite web |url=http://www.dtei.sa.gov.au/infrastructure/port_river_expressway/content/stage_2_and_3.html |archive-url=https://archive.is/20071221152607/http://www.dtei.sa.gov.au/infrastructure/port_river_expressway/content/stage_2_and_3.html |dead-url=yes |archive-date=21 December 2007 |title=Stage 2 of the Port River Expressway |accessdate=2008-05-25 |date=25 June 2008 |publisher=South Australian Department for Transport, Energy and Infrastructure}} </ref> It has been built at the same time as a rail bridge, the Mary MacKillop Bridge ({{coord|-34.837887|138.506948|type:landmark|display=inline}}), which is located right next to it.

The bridge was the subject of a successful community campaign, spearheaded by the local Semaphore & Port Adelaide [[Returned and Services League of Australia|RSL]] Sub-Branch and the ''[[Portside Messenger]]'' newspaper, to name it after Port Adelaide [[World War II]] hero, [[Tom Derrick|Tom 'Diver' Derrick]] [[Victoria Cross|VC]], [[Distinguished Conduct Medal|DCM]] instead of the State Government's unpopular choice of the 'Power Bridge'.

== History ==
For years, debate raged in Port Adelaide about the need for a third crossing over the Port River, in addition to the existing Birkenhead Bridge and Jervois Bridge. Ideas for the crossing included an under-river tunnel, closed bridge, causeway and opening bridge. In the early 2000s the State Government announced the Port River Expressway project, a highway linking Port Adelaide to Adelaide's northern suburbs. The project included a closed bridge across the Port River. However the Port Adelaide community were concerned a closed bridge would cut-off the Port River and block larger boats from accessing the Port's inner harbour. After a strong community campaign, the State Government agreed to build a high-level single-leaf bascule opening bridge, instead of a closed bridge. AbiGroup was awarded the tender to build the bridge in 2005 and construction began the following year. The twin bascule bridges were designed by the New York City based engineering firm, Hardesty & Hanover, LLP. The bridge was expected to be complete in December 2007, but due to delays it was opened for vehicles on 3 August 2008.

== Name ==
[[Image:Tom Derrick VC 016247.jpg|thumb|175px|right|Tom 'Diver' Derrick]]
[[File:Diver Derrik and Mary McKillop bridges open in 2010.jpg|thumb|200px|left|Tom 'Diver' Derrick and Mary MacKillop Bridge raised]]

The road and rail bridges were originally referred to as the PRExy Bridges, short for '''P'''ort '''R'''iver '''Ex'''presswa'''y'''. In 2004, South Australian Premier [[Mike Rann]] announced the road crossing would be named the 'Power Bridge' after the [[Port Adelaide Football Club]] which had recently won the AFL premiership. The State Government later announced it would hold a public competition to name the second bridge. But in 2005, in the wake of the death of [[Pope John Paul II]], Mr Rann announced the rail bridge would be named after [[Saint Mary MacKillop]].

Later in 2005, it was suggested the bridge be named after Derrick. This idea gradually garnered grassroots support in Port Adelaide.<ref>{{cite news |first=Adam|last=Todd|title=Diver would be proud of this|url=http://www.messengerwest.com.au/article/2008/05/07/4829_west_news.html|publisher=Portside Messenger|date=7 May 2008|accessdate=2008-06-25}}</ref> In May 2008, the Portside Messenger collected a 3500-signature petition in support of the Derrick name.<ref>{{cite news |first=Adam|last=Todd|title=A new dawn: We've taken too long on 'Diver' issue - Foley|url=http://www.messengerwest.com.au/article/2008/05/28/5108_west_news.html|publisher=Portside Messenger|date=28 May 2008|accessdate=2008-06-25}}</ref> Port Adelaide-Enfield Council also unanimously supported naming the bridge after Derrick.

On 5 June 2008, the State Government announced it had changed its mind and would name the bridge after Derrick.<ref>{{cite news |first=Adam|last=Todd|title=In honour of a true Port hero|url=http://www.messengerwest.com.au/article/2008/06/11/5304_west_news.html|publisher=Portside Messenger|date=11 June 2008|accessdate=2008-06-25}}</ref><ref>{{cite news |title=Name backdown for new Port bridge|url=http://www.abc.net.au/news/stories/2008/06/05/2265672.htm|publisher=ABC News|date=5 June 2008|accessdate=2008-06-25}}</ref> The Port Adelaide Football Club agreed to relinquish its naming rights to the bridge.

== See also ==
*[[Port Adelaide]]
*[[Port River]]
*[[Port River Expressway]]
*[[Bascule bridge]]

== References ==
{{reflist}}

== External links ==
*[https://www.sa.gov.au/topics/transport-travel-and-motoring/boating-and-marine/boat-operators-licences-and-permits/opening-the-port-river-bridges State Government Opening the Port River bridges]
*[https://www.youtube.com/watch?v=BJfUtnwNJvE YouTube video of Kevin Foley discussing the bridge's name]

[[Category:Bascule bridges]]
[[Category:Bridges in South Australia]]
[[Category:Bridges completed in 2008]]