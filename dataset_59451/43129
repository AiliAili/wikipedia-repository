<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= FB-22
 |image=
 |caption= Line drawing of early FB-22 design without vertical stabilizers
}}{{Infobox aircraft type
 |type= [[Stealth aircraft|Stealth]] [[bomber]]
 |manufacturer= [[Lockheed Martin]]
 |designer=
 |first flight=
 |introduction=
 |retired=
 |produced=
 |number built=
 |status= Design proposal<!-- canceled -->
 |unit cost=
 |primary user= 
 |more users=
 |developed from= [[Lockheed Martin F-22 Raptor]]
 |variants with their own articles=
}}
|}

The '''Lockheed Martin FB-22''' was a proposed [[bomber aircraft]] intended to enter service with the [[United States Air Force]].  Its design was derived from the [[Lockheed Martin F-22 Raptor|F-22 Raptor]]. The FB-22 was canceled following the 2006 [[Quadrennial Defense Review]].

==Design and development==
In 2001, Lockheed Martin began studies on the feasibility of the FB-22 as the company sought to leverage the design and capabilities of the F-22 Raptor. Experience gleaned from [[Operation Enduring Freedom]] in Afghanistan demonstrated the value of a bomber that could remain in theatre in the absence of [[surface-to-air missile]]s. The F-22, while designed as an [[air superiority fighter]], embodied some degree of [[Attack aircraft|air-to-ground attack]] ability.<ref name=Roche>{{Cite news |last=Wolfe |first=Frank |url= https://www.highbeam.com/doc/1G1-85125159.html |title=Roche: FB-22 Concept Leverages Avionics, Radar Work On F-22 |work=Defense Daily |issue=214 |volume=20 |date=26 April 2002 |subscription=yes |via=[[HighBeam Research]]}}</ref>

One primary objective of the internal studies was to exploit the F-22's air-to-ground capability while keeping costs to a minimum. To this end, the company devised several concepts that saw significant structural redesigns with respect the fuselage and wings, while retaining much of the F-22's avionics. With an early design, Lockheed Martin lengthened and widened the fuselage to increase the internal weapons load; it was later found that doing so would have incurred a cost penalty of 25–30% in weight, materials and development. Instead, the company left the fuselage intact as it enlarged the wing to a more [[Delta wing|delta shape]].<ref name=Raptor_as_bomber/><ref name=Refine>{{Cite journal |last=Trimble |first=Stephen|title=Lockheed refines FB-22 concept |journal=[[Flight International]] |location= London, UK |publisher=Reed Business Information |date=4–10 January 2005 |volume=167 |issue=4966 |page=12}}</ref> The wing, which was around three times that of the F-22, enabled the storage of a much larger amount of weapons and fuel. Various figures give the payload of the FB-22 to be 30 to 35 [[Small Diameter Bomb]]s; this is compared to the F-22's payload of eight of such {{Convert|250|lb|kg|sigfig=2|adj=on}} weapons. Unlike the F-22, the FB-22 was designed to be able to carry bombs up to {{Convert|5,000|lb|kg|sigfig=2}} in size. With stealth, the aircraft's maximum combat load was to have been {{Convert|15,000|lb|kg|sigfig=3}}; without stealth, {{Convert|30,000|lb|kg|sigfig=3}}.<ref name=Roche/><ref name=Raptor_as_bomber/>

Range was almost tripled from {{convert|600|mi|km|sigfig=2}} to more than {{convert|1600|mi|km|sigfig=2}}, which could have been extended by external fuel tanks. This placed the aircraft in the category of a regional bomber, comparable to that of the [[F-111]], as it was intended to replace the [[F-15E Strike Eagle]] and take over some of the missions of the [[B-1 Lancer|B-1]] and B-2.<ref name=Roche/><ref name=Knight_Ridder>{{Cite news |url= https://www.highbeam.com/doc/1G1-89874016.html |title=Air Force Considers F-22 Bomber; Lockheed Would Be Prime Contractor |last=Whittle |first=Richard |work=Knight Ridder Tribune Business News |date=30 July 2002}}</ref> According to the ''Air Force magazine'', the combination of range and payload of the FB-22 would have given the concept a comparable effectiveness to that of the B-2 armed with {{Convert|2,000|lb|kg|sigfig=2|adj=on}} bombs.<ref name=Long_arm>{{cite journal |author=Tirpak, John A. |journal=Air Force|accessdate=8 March 2017 |type=magazine |edition= |series= |date= October 2002 |origyear= |publisher=[[Air Force Association]] |location=Arlington, Virginia |oclc=5169825 |doi= |pages=28–34 |title=Long Arm of the Air Force|url=http://www.airforcemag.com/MagazineArchive/Documents/2002/October%202002/1002longarm.pdf |volume=85 |issue=10 |issn=0730-6784 }}</ref> The design could also have been adapted to use a more powerful engine, such as the [[F-35 Lightning II]]'s [[Pratt & Whitney F135]], or the [[General Electric/Rolls-Royce F136]].<ref name=Smarter_Bomber>{{cite web |last=Sweetman |first=Bill |authorlink= Bill Sweetman |url= http://www.popsci.com/military-aviation-space/article/2002-06/smarter-bomber |title=Smarter Bomber |work= ''Popular Science'', 12 June 2002 |accessdate= 13 July 2011}}</ref> While an early FB-22 concept featured no tailplanes, the design incorporated twin tailplanes and likely would have fixed engine nozzles as opposed to the thrust vectoring nozzles on the F-22.<ref name=Miller_p76-7>Miller 2005, pp. 76-77.</ref> The FB-22 was to have a maximum speed of Mach 1.92.<ref name=Refine/> Because the aircraft was to emphasize air-to-ground capability while maintaining its stealth characteristics, it would not have been able to [[dogfight]].<ref name=Long_arm/>

One aspect that arose during the early stages of the design process was the consideration that Boeing would be responsible for the final assembly of the aircraft. At the time, Lockheed Martin was making the mid-fuselage at its plant in [[Fort Worth, Texas]], while assembling the plane in [[Marietta, Georgia]]. However, since Boeing was responsible for the manufacturing of parts of the fuselage and more crucially, the wings—as well as integrating the avionics—it was considered prudent to give final assembly to Boeing.<ref name=Knight_Ridder/>

Other than the wings, the aircraft would have retained much of the design of the F-22. This included 80% of the [[avionics]], software, and flight controls. This commonality would have also significant reduced the costs of software integration.<ref name=Roche/>

In February 2003, during a session with the [[House Committee on Armed Services]], [[Secretary of the Air Force|Air Force Secretary]] [[James G. Roche|James Roche]] said that he envisioned a force of 150 FB-22s would equip the service.<ref>{{cite web |last=Cortes |first=Lorenzo |url=http://www.highbeam.com/doc/1G1-100568978.html |title=Air Force Leaders Address Potential For 150 FB-22s |work=Defense Daily |date=28 February 2003 |accessdate=3 May 2015 |subscription=yes |via=[[HighBeam Research]]}}</ref> In 2004, Lockheed Martin officially presented the FB-22 to the Air Force to meet its requirement for a potential [[strategic bomber]] as a interim solution to become operational by 2018.<ref>{{Cite journal |author1=Doyle, Andrew |author2=La Franchi, Peter |author3=Morrison, Murdo |author4=Sobie, Brendan |title=FB-22 proposed to US Air Force |journal=[[Flight International]] |location= London, UK |publisher=Reed Business Information |date=2-8 March 2004 |volume=165 |issue=4923 |page=21 |accessdate=8 March 2017}}</ref><ref>{{Cite journal |last=Hebert |first=Adam J |journal=Air Force |type=magazine |edition= |series= |date= November 2004 |origyear= |publisher=[[Air Force Association]] |location=Arlington, Virginia |oclc=5169825 |doi= |pages=26–31 |title=Long-Range Strike in a Hurry|url=http://www.airforcemag.com/MagazineArchive/Documents/2004/November%202004/1104strike.pdf |volume=87 |issue=11 |issn=0730-6784 |accessdate=11 March 2017}}</ref> Because of the work already done on the F-22, the cost of developing the FB-22 was estimated to be as low as 25% of developing a new bomber,<ref name=Raptor_as_bomber/> with development expected to be US$5–7 billion (2002 dollars), including the airframe cost of US$1 billion (2003 dollars).<ref name=Knight_Ridder/><ref>{{cite web |first=Lorenzo |last=Cortes|url=http://www.highbeam.com/doc/1G1-100568877.html |title=Air Force Issues Clarification On FB-22, FY '11 Delivery Date Possible |date=10 March 2003 |work=Defense Daily |accessdate=1 May 2015 |subscription=yes |via=[[HighBeam Research]]}}</ref> It was later revealed that six different versions of the bomber were submitted, as targets, payload and range had yet to be defined.<ref name=Raptor_as_bomber>{{cite journal |author=Tirpak, John A. |journal=Air Force |accessdate=8 March 2017 |type=magazine |edition= |series= |date= January 2005 |origyear= |publisher=[[Air Force Association]] |location=Arlington, Virginia |oclc=5169825 |doi= |pages=28–33 |title=The Raptor as Bomber |url=http://www.airforcemag.com/MagazineArchive/Documents/2005/January%202005/0105raptor.pdf |volume=88 |issue=1 |issn=0730-6784}}</ref> In addition, as a stealth bomber, the FB-22 was designed to carry weapons externally while maintaining stealth with the assistance of detachable and faceted pods dubbed "wing weapons bay"; previously, an aircraft could only remain stealthy if it carried its weapons internally.<ref name=Raptor_as_bomber/> However, the FB-22 in its planned form appears to have been canceled in the wake of the 2006 [[Quadrennial Defense Review]] and subsequent developments as the Department of Defense favored a bomber with much greater range.<ref name=QDR_2006>{{cite web |url=http://www.defenselink.mil/qdr/report/Report20060203.pdf |title=Quadrennial Defense Review Report |work= ''U.S. Department of Defense'', 6 February 2006 |format=PDF |accessdate= 13 July 2011}}</ref><ref>{{Cite journal |last=Hebert |first=Adam J |url=http://www.airforcemag.com/MagazineArchive/Documents/2006/October%202006/10062018.pdf |title=The 2018 Bomber and Its Friends |journal=Air Force magazine |location=Arlington, Virginia |publisher=Air Force Association |pages=24–29 |date=October 2006 |volume=89 |issue=10}}</ref><ref>{{cite web|url=http://www.afa.org/mitchell/reports/0207bombers.pdf |title=Return of the Bomber, The Future of Long-Range Strike |page=28| work=Air Force Association'', February 2007'' |format=PDF |accessdate= 13 July 2011}}</ref>

==Specifications (proposed)==
{{Aircraft specs
|ref= Miller,<ref name=Miller_p76-7/> Tirpak<ref name=Raptor_as_bomber/>
|prime units?= imp
|genhide=Y
|crew= 2 (pilot, co-pilot)
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=120000
|max takeoff weight note=
|fuel capacity=
|more general=

|eng1 number=
|eng1 name=
|eng1 type=
|eng1 kn=
|eng1 lbf=
|eng1 note=
|thrust original=
|eng1 kn-ab=
|eng1 lbf-ab=
|more power=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=1.92
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi= 1800 
|range note=(combat radius)<ref name=Raptor_as_bomber/>
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits= 6 ''g''
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=

|guns= 
|bombs= 30 × GBU-39 [[Small Diameter Bomb]]s 
|rockets= 
|missiles= 2 × [[AIM-120 AMRAAM]]
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{Portal|United States Air Force|Aviation}}
{{aircontent
|see also=<!-- other related articles that have not already linked: -->
|related=<!-- designs which were developed into or from this aircraft: -->
* [[Lockheed Martin F-22 Raptor]]
* [[Lockheed Martin X-44 MANTA]]
|similar aircraft=<!-- aircraft that are of similar role, era, and capability this design: -->
* [[Rockwell B-1 Lancer]]
* [[General Dynamics F-111 Aardvark|General Dynamics FB-111]]
* [[Sukhoi Su-34]]
|lists=<!-- relevant lists that this aircraft appears in: -->
* [[List of bomber aircraft]]
}}

==References==
{{Reflist|40em}}

* Miller, Jay. ''Lockheed Martin F/A-22 Raptor, Stealth Fighter''. Aerofax, 2005. ISBN 1-85780-158-X.

==External links==
*[http://www.globalsecurity.org/military/systems/aircraft/fb-22.htm FB-22 Fighter Bomber page] on GlobalSecurity.org
*[http://www.aviationweek.com/aw/generic/story_generic.jsp?channel=awst&id=news/sb03_2.xml "Doubts Surround Bomber's Future"]. ''[[Aviation Week]]'', 24 February 2003.
*[http://www.flightglobal.com/articles/2004/05/25/182020/experimental-technology-could-be-applied-to-fb-22-bomber.html "Experimental technology could be applied to FB-22 bomber variant"]. ''[[Flight International]]'', 25 May 2004.

{{Lockheed Martin aircraft}}
{{US bomber aircraft}}

[[Category:Lockheed Martin aircraft|FB-022]]
[[Category:Cancelled military aircraft projects of the United States]]
[[Category:Twinjets]]
[[Category:Delta-wing aircraft]]