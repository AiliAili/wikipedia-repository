{{Infobox television episode
| title        = A Song of Ass and Fire
| series = South Park
| season       = 17
| episode      = 8
| image        =
| caption      =
| director     = [[Trey Parker]]
| writer       = Trey Parker
| guests       =
| airdate      = {{Start date|2013|11|20
| mf=yes}}
| production   = 1708
| episode_list = [[South Park (season 17)|''South Park'' (season 17)]]<br>[[List of South Park episodes|List of ''South Park'' episodes]]
| prev         = [[Black Friday (South Park)|Black Friday]]
| next         = [[Titties and Dragons]]
}}
'''"A Song of Ass and Fire"''' is the eighth episode in the [[South Park (season 17)|seventeenth season]] of the American animated television series ''[[South Park]]''. The 245th episode of the series overall, it first aired on [[Comedy Central]] in the United States on November 20, 2013. The episode serves as a continuation of the previous episode, "[[Black Friday (South Park)|Black Friday]]", in which the children of South Park, role-playing as characters from ''[[Game of Thrones]]'', are split into two factions over whether to collectively purchase bargain-priced [[Xbox One]] or [[PlayStation 4]] video game consoles at an upcoming [[Black Friday (shopping)|Black Friday]] sale at the local mall, where [[Randy and Sharon Marsh|Randy Marsh]] has been made the Captain of mall security. The story arc concludes with the following episode, "[[Titties and Dragons]]".<ref>{{cite web|url=http://www.southparkstudios.com/news/kq4zni/episode-1709-titties-and-dragons-press-release |title=Episode 1709 "Titties and Dragons" Press Release |publisher=South Park Studios |date=December 1, 2013 |accessdate=December 2, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131203022507/http://www.southparkstudios.com/news/kq4zni/episode-1709-titties-and-dragons-press-release |archivedate=December 3, 2013 |df= }}</ref>

==Plot==
In continuation from the previous episode, the [[List of students at South Park Elementary|children of South Park]] are split into two factions over whether to purchase bargain-priced [[Xbox One]]s or [[PlayStation 4]]s at an upcoming [[Black Friday (shopping)|Black Friday]] sale at the South Park Mall. Kenny, role-playing as Lady McCormick, is revealed to have joined [[Stan Marsh|Stan]]'s pro-PlayStation 4 faction because [[Eric Cartman|Cartman]], who leads the pro-Xbox One faction, would not make him a princess. Meanwhile, as pre-Black Friday violence increases, the South Park Mall, where [[Randy and Sharon Marsh|Randy Marsh]] has been made the Captain of security, announces a 10% increase to the discount given to the first people in the mall, leading to more shoppers gathering in front of the mall ahead of the sale.

Cartman becomes increasingly agitated with the Xbox people's training following Kenny's betrayal, and informs a worried Kyle that he has a plan. Seeking an advantage to counter the PS4 faction's increasing ranks, Cartman contacts [[Microsoft]], but CEO [[Steve Ballmer]] dismisses the importance of the children's conflict and the commercialization of the console wars. When Microsoft Chairman [[Bill Gates]] learns of this, he has Ballmer murdered in order to personally see to it that Xbox wins the console wars. Gates allies himself with Channel 9's ''Morning News'' correspondent Niles Lawson, promising that the Black Friday violence will ensure high ratings. Lawson, playing both sides of the conflict, then informs [[Sony]]'s CEO of this during a post-coital discussion, and in response, the CEO gives Kenny a brooch that turns him into a Japanese princess.

Cartman also sends [[Butters Stotch|Butters]] and [[List of students at South Park Elementary#Others|Scott Malkinson]] to the New Mexico home of ''A Game of Thrones'' author [[George R. R. Martin]] for information on upcoming storylines, but Martin does nothing but regale the two children with plot points that emphasize male characters' penises. As Butters and Malkinson are about to leave, Martin tells them that he has connections with which he may be able to help their cause.

Lawson then reports that the mall, on Martin's suggestion, has moved the Black Friday sale one week from November 29 to December 6, and is now offering 96% off purchases to the first 100 people inside the mall, an announcement that leads to a brawl outside the mall.

==Production==
Series co-creators [[Trey Parker]] and [[Matt Stone]] stated that they struggled when deciding where to take the plot next after establishing that this would be the second episode in a three-part story arc. Originally, the episode consisted almost entirely of each console side's leader recruiting recurring characters from all throughout the series' history to join their side; [[crab people]] and [[underpants gnomes]] among others made appearances before being cut. This premise was scrapped because Parker and Stone felt it wasn't enough like the content in ''[[Game of Thrones]]''.<ref name="Commentary">{{cite video |people=[[Trey Parker|Parker, Trey]]; [[Matt Stone|Stone, Matt]] |date=2014 |title=South Park season 17 DVD commentary for the episode "A Song of Ass and Fire" |medium=DVD |publisher=[[Comedy Central]]}}</ref>

The Japanese Princess Kenny sequence was taken directly from the video game ''[[South Park: The Stick of Truth]]'', which Parker and Stone worked on.<ref name="Commentary"/>

==Reception==
Max Nicholson of [[IGN]] gave the episode a score of 8.4 out of 10, slightly lower than "Black Friday", saying: "Though not quite as strong as last week's Console War episode, 'A Song of Ass and Fire' was nevertheless entertaining and featured a number of great laugh-out-loud moments."<ref name=IGNReview>{{cite web|last=Nicholson|first=Max|title=South Park: 'A Song of Ass and Fire' Review|url=http://www.ign.com/articles/2013/11/21/south-park-a-song-of-ass-and-fire-review|publisher=[[IGN]]|date=November 21, 2013}}</ref>

Marcus Gilmer from ''[[The A.V. Club]]'' gave the episode an [[A- (grade)|A−]], writing that the last two episodes "prove there's still plenty of life left in the show". He wrote: "Among all of these bigger thematic elements are, of course, great jokes and gags: Cartman's 'wizard and a king' exchange with the Microsoft operator; the boys having to tilt Cartman to get him through the McCormick's doorway; George R. R. Martin torturing poor Butters by not letting the wiener thing go and promising the pizzas (or dragons) are on their way and will be amazing. And, of course, the great anime-aping segment."<ref name=AVReview>{{cite web|last=Gilmer|first=Marcus|title=South Park: 'A Song Of Ass And Fire'|url=http://www.avclub.com/review/a-song-of-ass-and-fire-105855|publisher=''[[The A.V. Club]]''|date=November 20, 2013}}</ref>

==References==
{{Reflist|30em}}

==External links==
* [http://www.southparkstudios.com/full-episodes/s17e08-a-song-of-ass-and-fire "A Song of Ass and Fire"] Full episode at South Park Studios.
* {{IMDb episode|3237526}}
* {{Tv.com episode|2994837}}
* [http://www.southparkstudios.com/news/5448sr/episode-1708-a-song-of-ass-and-fire-press-release "Episode 1708 'A Song of Ass and Fire' Press Release"]. South Park Studios. November 10, 2013.

{{South Park episodes|17}}

{{DEFAULTSORT:Song Of Ass And Fire}}
[[Category:A Song of Ice and Fire]]
[[Category:Anime-influenced animation]]
[[Category:Game of Thrones]]
[[Category:PlayStation 4]]
[[Category:South Park episodes in multiple parts]]
[[Category:South Park (season 17) episodes]]
[[Category:Xbox One]]
[[Category:Cultural depictions of Bill Gates]]