:''For others with this name, see [[John Riddle (disambiguation)]]''
'''John Paul Riddle''' (May 19, 1901 – April 6, 1989) was a [[aviator|pilot]] and an [[aviation]] enthusiast, most well known for co-founding what later became [[Embry-Riddle Aeronautical University]] (ERAU).

== Personal life ==
Riddle was born on May 19, 1901<ref name="SSDI">[http://www.footnote.com/page/44729742_john_p_riddle/ John P. Riddle (19 May 1901 – 6 April 1989)]. Source: [[Social Security Death Index]].</ref> in [[Pikeville, Kentucky|Pikeville]], [[Kentucky]].<ref name="society">{{cite web
|url = http://givingto.erau.edu/riddlesociety.html
|title = The John Paul Riddle Society
|accessdate = 2007-08-21}}</ref> He attended [[Pikeville College]] in his hometown and graduated in 1920.<ref>[http://www.historicmarkers.com/ky/81770-John-Paul-Riddle-1901-1989/ John Paul Riddle 1901-1989] Historical Marker Society of America. Retrieved on 2010-12-26</ref> After college he married Adele Goeser and they had six children. He served in the [[US Army Air Service]] from 1920 to 1922. After serving in the military, he became a [[barnstorming|barnstormer]].<ref name="FAHS">{{cite web
|url = http://www.sun-n-fun.org/content/interior.asp?section=museum&body=displays/fahs05&p=1
|title = Florida Aviation Historical Society: Inductees For 2005
|accessdate = 2007-08-21 |archiveurl = http://web.archive.org/web/20071023035134/http://www.sun-n-fun.org/content/interior.asp?section=museum&body=displays/fahs05&p=1 <!-- Bot retrieved archive --> |archivedate = 2007-10-23}}</ref> He last lived in [[Coral Gables, Florida|Coral Gables]], [[Florida]]. At 87 years old, Riddle came down with an illness and died a few days later, on April 6, 1989.<ref name="SSDI" />
<ref name="obituary">{{cite news 
  | last = Cook
  | first = Joan
  | title = John Paul Riddle, 87, Aviation Education Leader
  | pages = B12
  | publisher = [[The New York Times]]
  | date = 1989-04-11
  | url = http://select.nytimes.com/gst/abstract.html?res=FA0717F93B5D0C728DDDAD0894D1484D81
  | accessdate = 2007-08-21 }}</ref> Riddle's ashes were scattered over the Atlantic and he is remembered by a marker with his Royal Air Force cadets buried at Oak Ridge Cemetery in [[Arcadia, Florida]].<ref>{{cite web|url=http://dhr.dos.state.fl.us/wwii/sites.cfm?PR_ID=212 |title=Oak Ridge Cemetery |accessdate=2007-08-21 |deadurl=yes |archiveurl=https://web.archive.org/web/20040427102816/http://dhr.dos.state.fl.us/wwii/sites.cfm?PR_ID=212 |archivedate=April 27, 2004 }}</ref>

== Embry-Riddle ==

On December 17, 1925, exactly 22 years after the [[Wright Brothers]]' first flight, Riddle and [[T. Higbee Embry]] formed the Embry-Riddle Company at [[Lunken Airport]] in [[Cincinnati]], [[Ohio]]. Riddle had met Embry two years prior, while Riddle was [[barnstorming]] in Ohio. He had landed at [[Polo Field]], offered Embry a ride in his plane, and from then on they were good friends.<ref name="GreatAirports">{{cite book
 | first=Geoffrey
 | last=Arend
 | year=1986
 | title=Great Airports: Miami International
 | publisher =Air Cargo News, Inc
 | location = New York
 | pages = 206–212 }}
</ref> Riddle was named general manager, and the two began to sell [[Waco Aircraft Company|Waco Aircraft]] in Cincinnati. In spring of 1926, the Embry-Riddle Company opened the '''Embry-Riddle Flying School'''. The school grew rapidly in 1928 and 1929, until the Embry-Riddle Company (now the '''Embry-Riddle Aviation Corporation''') was merged with the [[Aviation Corporation|Aviation Corporation (AVCO)]] of [[Delaware]]. AVCO phased out the Embry-Riddle Flying School in the fall of 1930. Shortly after, AVCO became American Airways (the predecessor of [[American Airlines]]), and the Embry-Riddle Company was gone.<ref>{{cite book
  | last =Craft
  | first =Stephen
  | title =Embry-Riddle and American Aviation
  | url =http://www.erau.edu/er/abouterau/erauandeaf.pdf}}
</ref>

In 1939, Riddle was ready to get back into the business of training pilots. He contacted Embry, who had no interest in reentering a partnership with Riddle. Riddle, now living in [[Miami]], [[Florida]], found a partner in John G. McKay and his wife, Isabel. Keeping the Embry-Riddle name, they reestablished the Embry-Riddle School of Aviation (now the [[Embry-Riddle Aeronautical University]]), partnering with the [[University of Miami]] to provide flight training under the [[Civilian Pilot Training Program]], increasing the number of pilots immediately proceeding [[World War II]]. Riddle and McKay also formed the Riddle Aeronautical Institute at Carlstrom Field in [[Arcadia, Florida]] on March 22, 1941 for the purpose of training pilots for the [[United States Army Air Corps]]. A separate division of Embry-Riddle provided technical training in maintenance and metal work.

McKay purchased Riddle's share of Embry-Riddle in 1944 and from then on the two co-founders of the original Embry-Riddle were no longer involved.<ref>{{cite book
  | last =Craft
  | first =Stephen
  | title =Embry-Riddle in a World at War
  | url =http://www.erau.edu/er/abouterau/er-worldwar.pdf}}
</ref>

== Other companies ==
Riddle also founded many other aviation related companies and schools. The J.P. Riddle Company was a school founded in 1939. The flying and technical school was so large that it contracted five flying schools to the [[United States|US]] and [[United Kingdom|British]] governments. A [[Brazil]]ian school, Escola Tecnica de Aviacao, was established in 1943 and was located in [[Sao Paulo, Brazil]]. In May 1945, J.P. Riddle Company used some aircraft to relocate American transport instructors to his school in Brazil. This regularly scheduled service was operated by [[Riddle Airlines]], which was incorporated by Riddle.<ref name="FAHS"/>

== Use of Riddle's name ==

* The John Paul Riddle Student Center at ERAU's Daytona Beach campus has been named after Riddle.
* The [[call sign]] for aircraft at both ERAU campuses use the phrase "Riddle" followed by the numbers (two at the Prescott campus) in its registration. This call sign can only be used in the local area, as it is under an agreement with local [[air traffic control]].
* The John Paul Riddle Society is a gift society at Embry-Riddle reserved for those who have donated more than $500,000 to the University.

== Awards ==
* Wesley L. McDonald Elder Statesman Award - 1986<ref>{{cite web
 | url = http://www.naa.aero/html/awards/index.cfm?cmsid=166
 | title = National Aeronautic Association Awards
 | accessdate = 2007-08-21}}</ref>
* Pikeville College Outstanding Alumni Award - 1988<ref>{{cite web
 | url = http://alumni.pc.edu/award/outstanding.htm
 | title = Pikeville College - Outstanding Alumni Award Recipients
 | accessdate = 2007-08-21}}</ref>
* Florida Aviation Hall of Fame<ref>{{cite web
 | url = http://www.sun-n-fun.org/content/news/story.asp?newsid=240&section=yearround&body=news
 | title = Florida Aviation Hall Of Fame Inductees Named
 | accessdate = 2007-08-21 |archiveurl = http://web.archive.org/web/20071110233057/http://www.sun-n-fun.org/content/news/story.asp?newsid=240&section=yearround&body=news <!-- Bot retrieved archive --> |archivedate = 2007-11-10}}</ref>

== See also ==
* [[Embry-Riddle Aeronautical University]]

== References ==
{{reflist}}

== External links ==
* [http://www.erau.edu Embry-Riddle Aeronautical University]
* [http://givingto.erau.edu/societies/riddle.html The John Paul Riddle Society]

{{Embry–Riddle Aeronautical University}}
{{DEFAULTSORT:Riddle, John Paul}}
[[Category:1901 births]]
[[Category:1989 deaths]]
[[Category:American aviators]]
[[Category:United States Army Air Forces soldiers]]
[[Category:University of Pikeville alumni]]
[[Category:People from Coral Gables, Florida]]
[[Category:Embry–Riddle Aeronautical University]]