{{Infobox publisher
| image = [[File:McFarland & Company logo.JPG]]
| parent = Independent
| status = Active
| founded = 1979
| founder = Robert Franklin
| successor = 
| country = United States
| headquarters = [[Jefferson, North Carolina]]
| distribution = Worldwide
| keypeople = Robert Franklin, Rhonda Herman
| publications = academic and adult nonfiction, scholarly journals
| topics = pop culture, sports, military history, transportation, chess, medieval studies, literary criticism, librarianship
| genre = 
| imprints = 
| revenue = 
| numemployees = About 50
| nasdaq = 
| url = {{URL|http://www.mcfarlandbooks.com}}
}}
'''McFarland & Company, Inc.''' is an independent [[book publisher]] of primarily academic and adult nonfiction based in [[Jefferson, North Carolina]]. Its president and [[editor-in-chief]] is Robert Franklin, who founded the company in 1979.<ref>{{cite news|url=http://www.mountaintimes.com/mtweekly/2004/0930/mcfarland.php3|title=McFarland President To Speak At Entrepreneurial Conference|last=Roark|first=Fawn|work=[[Mountain Times]]|date=September 30, 2004 |accessdate=March 22, 2017}}</ref><ref name=mccompany>{{Cite web |url=http://www.mcfarlandbooks.com/about-mcfarland/company-history/ |title=Company History |work=McFarlandBooks.com |publisher=McFarland & Company |accessdate=March 22, 2017}}</ref> McFarland employs a staff of about 50, and as of 2017 had published approximately 5,100 titles.<ref name=mccompany/><ref name="MT03312005">{{cite web|url=http://archives.mountaintimes.com/mtweekly/2005/0331/mcfarland.php3|title=McFarland & Company Announces Promotion|work=[[Mountain Times]]|date=March 31, 2005 |accessdate=March 22, 2017}}</ref> For much of its history, McFarland has focused on small print runs, of about 600 copies per book.<ref>{{cite journal|last1=Slide|first=Anthony|title=A Publishing Phenomenon that Begins and Ends with Scarecrow Press|journal=Film History|date=2010|volume=22|issue=3|pages=300-301|doi=10.2979/fil.2010.22.3.298|url=http://www.jstor.org/stable/10.2979/fil.2010.22.3.298|accessdate=23 March 2017|quote=The initial print run for a book in the Filmmakers series, and, for that matter, most if not all Scarecrow titles, was six hundred copies. A similar print run has been the norm at McFarland and Greenwood Press.}}</ref>

==Subject matter==
McFarland & Company mainly focuses on selling to [[libraries]]. The publisher utilizes direct mailing to connect with enthusiasts in niche categories.<ref>{{cite journal|last1=Slide|first=Anthony|title=A Publishing Phenomenon that Begins and Ends with Scarecrow Press|journal=Film History|date=2010|volume=22|issue=3|page=304|doi=10.2979/fil.2010.22.3.298|url=http://www.jstor.org/stable/10.2979/fil.2010.22.3.298|accessdate=23 March 2017|quote=McFarland [...] books were primarily aimed at the library market. It was a mail order publisher with no interest in bookstore sales, but unlike its major competitor, virtually from the start all of its books were typeset.}}</ref> The company is known for its [[sports]] literature, especially [[baseball history]], as well as books about chess and film.<ref>{{cite news|first=Amy|last=Martinez|url=http://seattletimes.com/html/businesstechnology/2017889877_amazonpublisher02.html|title=<nowiki>Amazon.com</nowiki> Trying to Wring Deep Discounts from Publishers|work=[[Seattle Times]]|date=March 1, 2012 |accessdate=December 13, 2013}}</ref><ref>{{cite journal|last1=Slide|first=Anthony|title=A Publishing Phenomenon that Begins and Ends with Scarecrow Press|journal=Film History|date=2010|volume=22|issue=3|page=305|doi=10.2979/fil.2010.22.3.298|url=http://www.jstor.org/stable/10.2979/fil.2010.22.3.298|accessdate=23 March 2017|quote=Most film scholars, students and buffs would assume that McFarland’s main thrust has been towards film book Publishing [but] it is the largest publisher of military memoirs and baseball-oriented titles. It is also rich in books on women’s, African-American, and gender studies, on U.S. history, and is proud of its automotive line. It also boasts of being the most prestigious publisher of historical and reference books on chess.}}</ref> According to the ''[[Mountain Times]],'' McFarland publishes about 275 scholarly monograph and reference book titles a year.<ref name="MT03312005"/><ref name="MT12132007">{{Cite news |url=http://archives.mountaintimes.com/mtweekly/2007/1213/mcfarland.php3 |title=VP Celebrates 25 Years at McFarland |date=December 13, 2007 |work=[[Mountain Times]] |accessdate=December 13, 2013}}</ref>

==List of scholarly journals==
The following [[academic journals]] are published by McFarland & Company.<ref>{{Cite web |url=http://www.mcfarlandbooks.com/customers/journals/ |title=Journals |work=McFarlandBooks.com |publisher=McFarland & Company |accessdate=March 23, 2017}}</ref>
* ''Base Ball: A Journal of the Early Game''
* ''Black Ball: A Journal of the Negro Leagues''
* ''Clues: A Journal of Detection''
* ''[[Journal of Information Ethics]]''
* ''[[North Korean Review]]''

==References==
{{Reflist|30em}}

==External links==
* {{Official website|http://www.mcfarlandbooks.com/}}

{{DEFAULTSORT:McFarland And Company}}
[[Category:Companies based in North Carolina]]
[[Category:Publishing companies established in 1979]]
[[Category:Publishing companies of the United States]]