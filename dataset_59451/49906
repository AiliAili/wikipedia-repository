{{other uses|The Woman Who Did (disambiguation)}}
{{italic title}}
[[File:WomanWhoDid.png|thumb|From the original edition illustrated by [[Aubrey Beardsley]]]]
'''''The Woman Who Did''''' (1895) is a novel by [[Grant Allen]] about a young, self-assured [[bourgeoisie|middle-class]] woman who defies convention as a matter of principle and who is fully prepared to suffer the consequences of her actions. It was first published in [[London]] by [[John Lane (publisher)|John Lane]] in a series intended to promote the ideal of the "[[New Woman]]". It was adapted into a British silent film in 1915, ''[[The Woman Who Did (1915 film)|The Woman Who Did]]'', which was directed by [[Walter West (director)|Walter West]], and later into a 1925 German film, ''[[Die Frau mit dem schlechten Ruf]]''.

==Plot summary==
Herminia Barton, the [[University of Cambridge|Cambridge]]-educated daughter of a clergyman, frees herself from her parents' influence, moves to [[London]] and starts living alone. As she is not a woman of independent means, she starts working as a teacher. When she meets and falls in love with Alan Merrick, a lawyer, she suggests they live together without getting married. Reluctantly, he agrees, and the couple move to Italy. There, in [[Florence]], Merrick dies of [[typhoid]] before their daughter Dolores is born. Legal technicalities and the fact that the couple were not married prevent Herminia from inheriting any of Merrick's money.

Dreaming of being a role model for Dolores and her friends, Herminia returns to England and raises her daughter as a [[single mother]]. She wants to show the younger generation that even as a woman there is something one can do about the unfair position of women in society—a small step maybe, but with more and larger steps to follow soon. However, Dolores turns out to be ashamed of her mother's unmarried state and gradually turns against her. Eventually, Herminia chooses to make a huge sacrifice for her daughter's benefit and commits [[suicide]].

==Reception==
Allen was sympathetic to the [[feminism|feminist]] cause and saw his novel as a way to promote women's rights. However, the novel was controversial right from the start, with conservative readers as well as feminists criticizing Allen for the heroine he had invented. For example, [[Annie Sophie Cory|Victoria Crosse]] wrote her novel ''The Woman Who Didn't'' (1895) as a response to Allen's book and [[Mrs. Lovett Cameron]] wrote ''The Man Who Didn't''.<ref name=oup>[http://oxfordindex.oup.com/view/10.1093/oi/authority.20110803095544386 Mrs Lovett Cameron], OxfordIndex.oup.com, retrieved 23 February 2014</ref>

Whereas Herminia Barton questions the institution of [[marriage]] by refusing to get married herself, Victoria Crosse's heroine Eurydice Williamson—"the woman who didn't"—remains faithful to her impossible husband although, during a passage from India, she meets a man who falls in love with her. Similarly, Lovett Cameron's hero is a married man who resists the temptation to stray.

Another novel written in reply to Allen's work, [[Lucas Cleeve]]'s  ''The Woman Who Wouldn't'', (1895) sold well and received hostile reviews. The author said of this:

<blockquote>"If one young girl is kept from a loveless, mistaken marriage, if one frivolous nature is checked in her career of flirtation by remembrance of Lady Morris, I shall perhaps be forgiven by the public for raising my feeble voice in answer to the ''The Woman Who Did''".<ref name="Cambridge">{{cite book|author1=Lorna Sage |author2=Germaine Greer |author3=Elaine Showalter |title=The Cambridge guide to women's writing in English|url=https://books.google.com/books?id=NB59uc9_ss8C&pg=PA136&lpg=PA136&dq=Tales+of+the+Sun+Or+Folklore+of+Southern+India+by+Mrs+Howard+Kingscote,#v=onepage&q=Tales%20of%20the%20Sun%20Or%20Folklore%20of%20Southern%20India%20by%20Mrs%20Howard%20Kingscote%2C&f=false|year=1999 |publisher=Cambridge University Press|isbn=978-0-521-66813-2|chapter=Cleeve, Lucas}}</ref></blockquote>

The campaigner [[Emma Brooke]] saw this novel and her own novel "The Superfluous Woman" as important in trying to resolve the "Sex Question" which she thought dominated intellectual debate in the 1880s. She was annoyed when [[H. G. Wells]] reinvented the question when he spoke to the Fabian Society in 1906.<ref name=tandf>[http://www.tandfonline.com/doi/pdf/10.1080/09612020300200353 Emma Brooke:Fabian, feminist and writer], Kay Daniels, Women’s History Review, Volume 12, Number 2, 2003</ref>

==References==
{{Reflist}}

==External links==
* {{gutenberg|no=4396|name=The Woman Who Did}}
* {{FadedPage|id=20120514|name=The Woman Who Did}}
* [http://www.blackmask.com/books61c/womandiddex.htm The full text of ''The Woman Who Did'' online]
* [http://ehlt.flinders.edu.au/english/GA/PunchParody.htm "The Woman Who Wouldn't Do"], a [[parody]] first published in ''[[Punch magazine|Punch]]'' No.108 (March 30, 1895) 153.
* [http://librivox.org/the-woman-who-did-by-grant-allen/ Free MP3 audiobook of The Woman Who Did] from [http://librivox.org LibriVox]

:<small>'''Note''': "She-Note Series" is an allusion to John Lane's "Keynote Series" in which ''The Woman Who Did'' was published.</small>

{{DEFAULTSORT:Woman Who Did, The}}
[[Category:1895 novels]]
[[Category:Canadian novels adapted into films]]
[[Category:John Lane (publisher) books]]
[[Category:19th-century Canadian novels]]