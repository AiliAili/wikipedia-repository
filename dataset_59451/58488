{{italictitle}}
{{Infobox Journal
| title = Journal of Scottish Philosophy
| cover = [[File:Journal of Scottish Philosophy.jpg|150px]]
| discipline = [[Philosophy]]
| website = http://www.eupjournals.com/journal/jsp
| link1 = http://www.scottishphilosophy.org/index.html
| link1-name = International Association for Scottish Philosophy
| publisher = [[Edinburgh University Press]]
| country = [[United Kingdom]]
| abbreviation = 
| history = 2003–present
| frequency = Bi-yearly
| ISSN = 1479-6651
| eISSN = 1755-2001
| Jstor =
| OCLC = 60628593
| LCCN =
}}
The '''''Journal of Scottish Philosophy''''' is an [[academic journal]] of [[philosophy]]. The journal focuses particularly on the writings of the [[Scottish Enlightenment]] philosopher [[Thomas Reid]] (1710–96), and on the influence of [[Scotland|Scottish]] philosophy on the foundations of theology and education in North America.

The journal publishes issues on special themes, as well as general issues, and includes reviews of relevant publications alongside scholarly articles. It is published twice yearly by [[Edinburgh University Press]], in March and September, on behalf of the [[Center for the Study of Scottish Philosophy]] at [[Princeton Theological Seminary]] in the [[United States]].<ref>[http://libweb.ptsem.edu/collections/scottish Center for the Study of Scottish Philosophy] at the [[Princeton Theological Seminary]] website. Retrieved on 2009-07-13.</ref>

The journal was founded in 2003 under the auspices of "The Reid Project" at the [[University of Aberdeen]] in Scotland. In 2003 The Reid Project was expanded into an established Center for the Study of Scottish Philosophy (CSSP).

== Editor ==
The [[Editor in chief]] is Gordon Graham ([[Princeton Theological Seminary]]).<ref>[http://www.eupjournals.com/page/jsp/editorialBoard ''Journal of Scottish Philosophy'' Editorial Board]. Retrieved on 2009-07-13.]</ref>

== References ==
{{reflist}}

== External links ==
* {{Official|http://www.eupjournals.com/journal/jsp}}
* [http://www.scottishphilosophy.org International Association for Scottish Philosophy]
* [http://libweb.ptsem.edu/collections/scottish/Default.aspx?menu=372&image=388&subText=440 Center for the Study of Scottish Philosophy] at [[Princeton Theological Seminary]]

{{DEFAULTSORT:Journal Of Scottish Philosophy}}
[[Category:Edinburgh University Press academic journals]]
[[Category:Philosophy journals]]
[[Category:Scottish magazines]]
[[Category:Publications established in 2003]]
[[Category:2003 in Scotland]]
[[Category:University of Aberdeen]]
[[Category:Princeton Theological Seminary]]
[[Category:Biannual journals]]