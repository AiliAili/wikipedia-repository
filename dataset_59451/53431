{{distinguish|Chartered Accountant|Certified Practising Accountant|Chartered Professional Accountant}}
{{refimprove|date=September 2015}}

{{Infobox company
| name = Certified Public Accountant (CPA)
| logo =
| type = [[Qualified accountants]]
| foundation = [[United States]]
| slogan =
| location = United States
| key_people =
| industry = Accounting and finance
| products =
| revenue =
| operating_income =
| net_income =
| num_employees =
| parent =
| subsid =
|
}}
{{accounting}}
'''Certified Public Accountant''' ('''CPA''') is the title of qualified accountants in numerous countries in the English-speaking world. In the [[United States]], the CPA is a license to provide accounting services directly to the public. It is awarded by each of the 50 states for practice in that state. Additionally, almost every state (49 out of 50) has passed mobility laws in order to allow practice in their state by CPAs from other states. Although state licensing requirements vary, the minimum standard requirements include the passing of the [[Uniform Certified Public Accountant Examination]], 150 semester units of college education, and one year of accounting related experience. 

Continuing professional education (CPE) is also required to maintain licensure. Individuals who have been awarded the CPA but have lapsed in the fulfillment of the required CPE or have requested to be converted to inactive status are in many states permitted to use the designation "CPA Inactive" or an equivalent phrase. In most U.S. states, only CPAs are legally able to provide to the public attestation (including auditing) opinions on [[financial statement]]s. Many CPAs are members of the [[American Institute of Certified Public Accountants]] and their state CPA society.

State laws vary widely regarding whether a non-CPA is even allowed to use the title accountant. To illustrate, [[Texas]] prohibits the use of the designations "accountant" and "auditor" by a person not certified as a Texas CPA, unless that person is a CPA in another state, is a non-resident of Texas, and otherwise meets the requirements for practice in Texas by out-of-state CPA firms and practitioners.<ref>{{cite web|url=http://codes.lp.findlaw.com/txstatutes/OC/5/A/901/J/901.453 |title=Texas OC. Code Ann. Sec. 901.453 |publisher=Codes.lp.findlaw.com |date= |accessdate=2011-11-26}}</ref>

[[Uniform Certified Public Accountant Examination#Non-U.S. candidates|Many other countries]] also use the title CPA to designate local public accountants.

==Services provided==
The primary functions performed by CPAs relate to [[assurance services]]. In assurance services, also known as [[financial audit]] services, CPAs attest to the reasonableness of disclosures, the freedom from material misstatement, and the adherence to the applicable [[generally accepted accounting principles]] (GAAP) in financial statements. CPAs can also be employed by corporations—termed "the private sector"—in finance functions such as Chief Financial Officer (CFO) or finance manager, or as CEOs subject to their full business knowledge and practice. These CPAs do not provide services directly to the public.

Although some CPA firms serve as business consultants, the consulting role has been under scrutiny following the [[Enron scandal]] where [[Arthur Andersen]] simultaneously provided audit and consulting services which affected their ability to maintain independence in their audit duties. This incident resulted in many accounting firms divesting in their consulting divisions, but this trend has since reversed. In audit engagements, CPAs are (and have always been) required by professional standards and Federal and State laws to maintain independence (both in fact and in appearance) from the entity for which they are conducting an attestation (audit and review) engagement.  However, most individual CPAs who work as consultants do not also work as auditors.

CPAs also have a niche within the income tax return preparation industry. Many small to mid-sized firms have both a tax and an auditing department. Along with attorneys and [[enrolled agent]]s, CPAs may represent taxpayers in matters before the [[Internal Revenue Service]].

Whether providing services directly to the public or employed by corporations or associations, CPAs can operate in virtually any area of finance including:
* Assurance and attestation services
* [[Corporate finance]] (merger and acquisition, initial public offerings, share and debt issuings)
* [[Corporate governance]]
* [[Estate planning]]
* [[Financial accounting]]
* [[Governmental accounting]]
* [[Financial analysis]]
* [[Financial planning]]
* [[Forensic accounting]] (preventing, detecting, and investigating financial frauds)
* [[Income tax]]
* Information technology, especially as applied to accounting and auditing
* [[Management consulting]] and performance management
* [[Tax preparation]] and planning
* [[Venture Capital]]
* [[Financial reporting]]
* [[Regulatory reporting]]

==CPA exam==
{{Main article|Uniform Certified Public Accountant Examination}}

In order to become a CPA in the United States, the candidate must sit for and pass the [[Uniform Certified Public Accountant Examination]] (Uniform CPA Exam), which is set by the [[American Institute of Certified Public Accountants]] ([[AICPA]]) and administered by the [[National Association of State Boards of Accountancy]] (NASBA). The CPA designation was first established in law in New York State on April 17, 1896.<ref>{{cite web|author=Flesher, D.L.|author2=Previts, G.J.|author3=Flesher, T.K.|last-author-amp=yes|title=Profiling the New Industrial Professionals: The First CPAs of 1896–97|publisher=Business & Economic History, volume 25, 1996|url=http://www.h-net.org/~business/bhcweb/publications/BEHprint/v025n1/p0252-p0266.pdf|accessdate=2013-10-18}}</ref>

Eligibility to sit for the Uniform CPA Exam is determined by individual state boards of accountancy. Many states have adopted what is known as the "150 hour rule" (150 college semester units or the equivalent), which usually requires an additional year of education past a regular 4 year college degree, or a master's degree.  (As such, universities commonly offer combined 5-year bachelor's/master's degree programs, allowing a student to earn both degrees while receiving the 150 hours needed for exam eligibility.)

The Uniform CPA Exam tests general principles of state law such as the laws of [[contract]]s and [[Law of agency|agency]] (questions not tailored to the variances of any particular state) and some federal laws as well.<ref>{{cite web|title=See generally ''Uniform CPA Examination: Examination Content Specifications'', American Inst. of Certified Public Accountants, p. 11-12 (orig. issued June 14, 2002; references updated October 19, 2005)|url=http://www.cpa-exam.org/download/CPA_Exam_CSOs_revised_10_05.pdf|publisher=Cpa-exam.org|accessdate=2013-10-18}}</ref>

==Other licensing and certification requirements==
Although the CPA exam is uniform, licensing and certification requirements are imposed separately by each state's laws and therefore vary from state to state.

State requirements for the CPA qualification can be summed up as the ''Three Es''—Education, Examination and Experience. The education requirement normally must be fulfilled as part of the eligibility criteria to sit for the Uniform CPA Exam. The examination component is the Uniform CPA Exam itself. Some states have a two-tier system whereby an individual would first become certified—usually by passing the Uniform CPA Exam. That individual would then later be eligible to be licensed once a certain amount of work experience is accomplished. Other states have a one-tier system whereby an individual would be certified and licensed at the same time when both the CPA exam is passed and the work experience requirement has been met.

Two-tier states include [[Alabama]], [[Florida]], [[Illinois]], [[Montana]], and [[Nebraska]]. The trend is for two-tier states to gradually move towards a one-tier system. Since 2002, the state boards of accountancy in [[Washington (state)|Washington]] and [[South Dakota]] have ceased issuing CPA "certificates" and instead issue CPA "licenses." Illinois planned to follow suit in 2012.<ref>{{cite web|title=Legislative sunset provision for two-tier designation of CPAs in Illinois (Illinois General Assembly)|url=http://www.ilga.gov/legislation/billstatus.asp?DocNum=6415&GAID=10&GA=96&DocTypeID=HB&LegID=52442&SessionID=76|publisher=Ilga.gov|accessdate=2013-10-18}}</ref>

A number of states are two-tiered, but require work experience for the CPA certificate, such as [[Ohio]].

===Work experience requirement===
The experience component varies from state to state:
* The two-tier states generally do not require that the individual have work experience to receive a CPA certificate. (Work experience is required, however, to receive a license to practice.)
* Some states, such as [[Colorado]] and [[Massachusetts]], will waive the work experience requirement for those with a higher academic qualification compared to the state's requirement to appear for the [[Uniform Certified Public Accountant Examination|Uniform CPA]].  As of July 1, 2015, Colorado no longer offers the education in lieu of experience option and all new applicants must have at least one year of work experience.
* The majority of states still require work experience to be of a ''public accounting'' nature, namely two years audit or tax experience, or a combination of both. An increasing number of states, however, including [[Oregon]], [[Virginia]], [[Georgia (U.S. state)|Georgia]] and [[Kentucky]], accept experience of a more general nature in the accounting area. This allows persons to obtain the CPA designation while working for a corporation's finance function.
* The majority of states require an applicant's work experience to be verified by someone who is already licensed as a CPA. This requirement can cause difficulties for applicants based outside the United States. However, some states such as Colorado and Oregon will accept work experience certified by a [[Chartered Accountant]] as well.

===Ethics===
Over 40 of the state boards now require applicants for CPA status to complete a special examination on ethics, which is effectively a ''fifth exam'' in terms of requirements to become a CPA.  The majority of these will accept the AICPA self-study ''Professional Ethics for CPAs'' [[#Continuing professional education|CPE]] course or another course in general professional ethics. Many states, however, require that the ethics course include a review of that state's specific rules for professional practice.

===Continuing professional education===
Like other professionals, CPAs are required to take [[continuing education]] courses toward [[continuing professional development]] (continuing professional education [CPE]) to renew their license. Requirements vary by state (Wisconsin does not require any CPE for CPAs<ref>{{cite web|url=https://www.sequoiacpe.com/wisconsin-cpe-requirements |title=Wisconsin CPA CPE Requirements |publisher=Continuing Education Requirements |date= |accessdate=2014-03-10}}</ref><ref>{{cite web|url=http://www.mypescpe.com/open/viewStateLicDisplay.Open.cmc |title=State Requirements |publisher=Professional Education Services CPE |date= |accessdate=2014-03-10}}</ref>) but the vast majority require an average of 40 hours of CPE every year with a minimum of 20 hours per calendar year. The requirement can be fulfilled through attending live seminars, webcast seminars, or through self-study (textbooks, videos, online courses, all of which require a test to receive credit).<ref>{{cite web|url=http://www.aicpa.org/Advocacy/State/DownloadableDocuments/AICPA-NASBA-Final-CPE-Standards.pdf |title=Statement of Standards for CPE Programs|publisher=AICPA |date= |accessdate=2014-03-10}}</ref> In general, state boards accept group live and group internet-based credits for all credit requirements, while some states cap the number of credits obtained through the self-study format. All CPAs are encouraged to periodically review their state requirements.<ref>{{cite web|url=https://www.ceworkshops.com/2015-Continuing-Professional-Education-Snapshot_b_5.html |title=2015 Continuing Professional Education Snapshot|publisher=McDevitt & Kline, LLC |date= |accessdate=2015-08-18}}</ref> As part of the CPE requirement, most states require their CPAs to take an ethics course at some frequency (such as every or every other renewal period). Ethics requirements vary by state and the courses range from 2–8 hours. AICPA guidelines grant licensees 1 hour of CPE credit for every 50 minutes of instruction.<ref>{{cite web|url=http://www.mypescpe.com/CPE-State-Accreditation-Information.cmc?&view=4D |title=CPE State Accreditation Information |publisher=Professional Education Services CPE |date= |accessdate=2014-03-10}}</ref><ref>{{cite web|url=http://www.cpa2biz.com/content/media/producer_content/generic_template_content/cperequirements.jsp |title=CPE Requirements |publisher=AICPA |date= |accessdate=2014-03-10}}</ref>

===Loss of licensure===
A CPA license may be suspended or revoked for various reasons.  Common reasons include:
*Failure to pay state licensing fees
*Failure to complete continuing education requirements
*"Discreditable acts", which can include 1) failure to follow applicable standards (such as auditing standards when examining financial statements, or tax code when preparing tax returns) or 2) violation of criminal laws (such as felony convictions; a notable example of a CPA who's license has been revoked is [[John David Battaglia]], convicted of the capital murders of his children)<ref>{{cite web|url=https://www.tsbpa.texas.gov/php/fpl/indlookup.php?x=5Rp8bo1dZ%2BE%3D |title=Texas State Board of Public Accountancy Individual Detail |publisher=Tsbpa.texas.gov |date= |accessdate=2017-02-17}}</ref>

==Other accounting designations sometimes confused with CPA==
Many states had (although a few still do) a second tier of accountant qualification in addition to that of CPA, usually entitled "Public Accountant" or "Licensed Public Accountant" (with designatory letters "PA" or "LPA"), although other titles have included "Registered Public Accountant" (RPA), "Accounting Practitioner" (AP), and "Registered Accounting Practitioner" (RAP). Such designations served to help regulate the practice of public accounting in that state by grandfathering through licensure non-CPA accountants who were already practicing public accounting before a regulatory state accountancy law was enacted. The majority of states have closed the designation "Public Accountant" (PA) to new entrants, with only five states continuing to offer the designation. Many PAs belong to the [[National Society of Accountants]].

Many states prohibit the use of the designations "Public Accountant" or "Licensed Public Accountant" (or the abbreviations "PA" or "LPA") by a person who is not certified as a PA in that state.

In [[Australia]] the designation "Public Accountant" is used by members of the [[Institute of Public Accountants]] of Australia.

==Inter-state practice==
An accountant is required to meet the legal requirement of any state in which they want to practice. Also, the term "practice of public accounting" and similar terms are given definitions PA status under reciprocity to a CPA licensed in another state. CPAs from other states with less stringent educational requirements may not be able to benefit from these provisions. This does not affect those CPAs who do not plan to offer services directly to the public. Moreover, most states would grant the temporary practicing rights to a CPA of another state.

===Practice mobility===

In recent years, practice mobility for CPAs has become a major business concern for CPAs and their clients. Practice mobility for CPAs is the general ability of a licensee in good standing from a substantially equivalent state to gain practice privilege outside of his or her home state without getting an additional license in the state where the CPA will be serving a client or an employer. In today’s digital age, many organizations requiring the professional services of CPAs conduct business on an interstate and international basis and have compliance responsibilities in multiple jurisdictions.  As a result, the practice of CPAs often extends across state lines and international boundaries.<ref name="UAA Background">{{cite web|url=http://www.aicpa.org/Advocacy/State/DownloadableDocuments/UAASixthEdition.pdf |title=August 2011 : We are pleased to announce that the Uniform Accountancy Act (UAA), Sixth Edition, August 2011, is now available |publisher=AICPA |deadurl=yes |archiveurl=https://web.archive.org/web/20130208124716/http://www.aicpa.org/Advocacy/State/DownloadableDocuments/UAASixthEdition.pdf |archivedate=February 8, 2013 }}</ref>

Differing requirements for CPA certification, reciprocity, temporary practice and other aspects of state accountancy legislation in the 55 U.S. licensing jurisdictions (the 50 states, Puerto Rico, the District of Columbia, the U.S. Virgin Islands, Guam and the Commonwealth of the Northern Mariana Islands) make the interstate practice and mobility of CPAs more complicated. By removing boundaries to practice in the U.S., CPAs are able to more readily serve individuals and businesses in need of their expertise. At the same time, the state board of accountancy’s ability to discipline is enhanced by being based on a CPA and the CPA firm’s performance of services (either physically, electronically or otherwise within a state), rather than being based on whether a state license is held.<ref name="UAA Background" />

The [[American Institute of Certified Public Accountants]] (AICPA) and the [[National Association of State Boards of Accountancy]] (NASBA) have analyzed the current system for gaining practice privileges across state lines and have endorsed a uniform mobility system. This model approach is detailed through the substantial equivalency provision (Section 23) of the Uniform Accountancy Act (UAA). The UAA is an "evergreen" model licensing law co-developed, maintained, reviewed and updated by the AICPA and NASBA.  The model provides a uniform approach to regulation of the accounting profession.<ref name="UAA Background" />

Under Section 23 of the UAA, a CPA with a license in good standing from a jurisdiction with CPA licensing requirements essentially equivalent to those outlined in the UAA is deemed to be “substantially equivalent,” or a licensee who individually meets the requirements of:
* Obtaining 150 credit hours (150 college semester units or the equivalent) with a baccalaureate degree 
* Minimum one year of CPA experience 
* Passing the Uniform CPA Examination

Uniform adoption of the UAA’s substantial equivalency provision creates a system similar to the nation’s driver’s license program by providing CPAs with mobility while retaining and strengthening state boards’ ability to protect the public interest. The system enables consumers to receive timely services from the CPA best suited to the job, regardless of location, and without the hindrances of unnecessary filings, forms and increased costs that do not protect the public interest.<ref name="UAA Background" />

As of October 2012, a total of 49 out of the 50 states and the District of Columbia have passed mobility laws and are now in the implementation and navigation phases.<ref>{{cite web|url=http://www.aicpa.org/Advocacy/State/PublishingImages/CPA_Mobility_Map.pdf |title=AICPA : CPA Mobility Map |publisher=Aicpa.org |accessdate=2013-10-18}}</ref>  Only the Commonwealth of the Northern Mariana Islands, the Virgin Islands, Hawaii, Puerto Rico and Guam have not passed mobility laws. On September 20, California Governor Jerry Brown signed legislation that permits cross-border mobility for CPAs.  The law went into effect July 1, 2013.<ref>{{cite web|url=http://www.journalofaccountancy.com/News/20126510 |title=California becomes 49th state to pass CPA mobility law |publisher=Journalofaccountancy.com |date=2012-09-21 |accessdate=2012-12-12}}</ref>  The District of Columbia passed mobility laws that went into effect on October 1, 2012.<ref>{{cite web|url=http://www.journalofaccountancy.com/News/20126555 |title=CPA mobility law takes effect in Washington |publisher=Journalofaccountancy.com |date= |accessdate=2012-12-12}}</ref>

==AICPA membership==
The CPA designation is granted by individual state boards, not the [[American Institute of Certified Public Accountants]] (AICPA). Membership in the AICPA is not obligatory for CPAs, although some CPAs do join. To become a full member of AICPA, the applicant must hold a valid CPA certificate or license from at least one of the fifty-five U.S. state/territory boards of accountancy; some additional requirements apply.

AICPA members approved a proposed bylaw amendment to make eligible for voting membership individuals who previously held a CPA<ref>{{cite web|url=http://www.cpaexam.com |title=Bisk CPA Review |publisher=CPA Exam |date= |accessdate=2013-10-18}}</ref> certificate/license or have met all the requirements for CPA certification in accordance with the Uniform Accountancy Act (UAA).  The AICPA announced its plan to accept applications from individuals meeting these criteria, beginning no later than January 1, 2011.

==State CPA association membership==
CPAs may also choose to become members of their local state association or society (also optional). Benefits of membership in a state CPA association range from deep discounts on seminars that qualify for continuing education credits to protecting the public and profession's interests by tracking and lobbying legislative issues that affect local state tax and financial planning issues.

CPAs who maintain state CPA society memberships are required to follow a society professional code of conduct (in addition to any code enforced by the state regulatory authority), further reassuring clients that the CPA is an ethical business professional conducting a legitimate business who can be trusted to handle confidential personal and business financial matters. State CPA associations also serve the community by providing information and resources about the CPA profession and welcome inquiries from students, business professionals and the public-at-large.

CPAs are not normally restricted to membership in the state CPA society in which they reside or hold a license or certificate. Many CPAs who live near state borders or who hold CPA status in more than one state may join more than one state CPA society.

'''State Associations'''
* [[Ficpa|Florida Institute of Certified Public Accountants (FICPA)]]

==See also==
* [[Accountant]]
* [[Certified General Accountant|Certified General Accountant (Canada)]]
* [[CPA Magazine]]
* [[Institute of Certified Public Accountants in Ireland|Institute of Certified Public Accountants in Ireland (CPA Ireland)]]
* [[Certified Management Accountant]]
* [[Certified National Accountant|Certified National Accountant (Nigeria)]]
* [[Institute of Public Accountants]]
* [[CPA Australia|Certified Practising Accountant (Australia)]]
* [[Association of Chartered Certified Accountants|Chartered Certified Accountant (ACCA)]]
* [[Chartered Global Management Accountant]]
* [[Legal liability of certified public accountants]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Accounting}}
* [http://www.accountingmajors.com/accountingmajors/articles/statecpas.html Directory of State CPA Societies]
* [[Ficpa|Florida Institute of Certified Public Accountants (FICPA)]]

[[Category:Accounting qualifications]]
[[Category:Accounting in the United States]]