{{unreferenced|date=April 2011}}
The '''Mycological Society of America''' ('''MSA''') is a [[learned society]] that serves as the professional organization of [[mycologist]]s in the [[United States|U.S.]] and [[Canadian|Canada]]. It was founded in 1932. The Society's constitution states that "The purpose of the Society is to promote and advance the science of mycology and to foster and encourage research and education in mycology in all its aspects."  Members of the MSA meet annually to exchange information and build understanding of [[fungi]].

==Publications==
''[[Mycologia]]'' is the official scholarly journal of the Mycological Society of America. Six issues are published each year; members receive a subscription as a benefit of membership.  Both members and non-members are invited to submit scholarly manuscripts for publication. As of 2002, ''Mycologia'' issues are available to subscribers online as well as in print. 

''Inoculum'' is the Society's bimonthly newsletter. Though published in print through 2006, as of 2007 ''Inoculum'' is published online only and is freely accessible through the [http://www.msafungi.org MSA website].

The Mycological Society of America also publishes ''Mycologia Memoirs'', an occasional series of scholarly monographs on aspects of fungal biology.

==Membership==

In 2006, MSA membership included about 1190 members drawn from 51 different countries. The society's constitution states that "Membership in the Society shall be open to persons or organizations who share the stated purposes of the Society." Members are eligible for many different annual awards that recognize scholarship, pedagogy, and research potential. The Mycological Society of America may bestow honorary membership on particularly distinguished [[mycologist]]s who reside outside North America.

===Honorary members===
*1951, [[Ernst A Gäumann]], Switzerland d.1963 – Mycologia 57:1-5, 1965 
*1955, [[Franz Petrak]], Austria d.1973 
*1965, [[G.C. Ainsworth]], United Kingdom d.1998 – Mycologia 91(4): 714, 1999; [[J.A. von Arx]], Netherlands d.1988; [[C.T. Ingold]], United Kingdom; [[Grace M. Waterhouse]], United Kingdom d.1996 
*1966 [[Lilian E. Hawker]], United Kingdom d.1991; [[John Axel Nannfeldt|J.A. Nannfeldt]], Sweden d.1985 – Mycologia 78: 692-693, 1986 
*1973, [[M.B. Ellis]], United Kingdom d.1996; [[Roger Heim]], France d.1979 – Mycologia 72:1063-1064, 1980; [[Keisuke Tubaki]], Japan 
*1983, [[R.W.G. Dennis]], United Kingdom d.2003; [[R. Kühner]], France d.1996 – Mycologia 91(4): 707, 1999; [[Emil Müller (mycologist)|Emil Müller]], Switzerland; [[C. V. Subramanian]], India 
*1985, [[John Webster (mycologist)|John Webster]], United Kingdom 
*1987, [[Colin Booth (mycologist)|Colin Booth]], United Kingdom; [[Gastón Guzmán]], Mexico; [[Meinhard Moser]], Austria d.2002 - Inoculum 53(6):14. 2002. 
*1988, [[Leif Ryvarden]], Norway 
*1989, [[Nils Fries]], Sweden d.1994 – Inoculum 46(3): 3, 1995.
*1992, [[E.J.H. Corner]], United Kingdom d.1996 – Mycologia 90(4): 732, 1998; [[Vera Holubová-Jechová]], Czech Republic d.1993 – Ceská Mykologie 47(1): 83, 1993. 
*1993, [[Lennart Holm]], Sweden; [[Erast Parmasto]], Estonia; [[Josef Poelt]], Austria d.1995 – Inoculum 46(3): 3, 1995; [[Jorge E. Wright]], Argentina
*1994, [[David L. Hawksworth]], United Kingdom; [[Brian C. Sutton]], United Kingdom; [[Joseph Wessels]], Netherlands 
*1995, [[Karl Esser]], Germany 
*1996, [[Junta Sugiyama]], Japan; [[Anthony P.J. Trinci]], United Kingdom 
*1997, [[Walter Gams]], Netherlands 
*1998, [[Ludmila Marvanová]], Czech Republic; [[Roy Watling]], United Kingdom 
*2000, [[David J. Read]], United Kingdom 
*2001, [[Birgitt Nordbring-Hertz]], Sweden; [[John Pitt (mycologist)|John Pitt]], Australia 
*2002, [[Ove E. Eriksson]], Sweden 
*2003, [[Jeremy Burdon]], Australia; [[Tsuguo Hongo]], Japan; [[Egon Horak]], Switzerland 
*2004, [[Rob Samson]], The Netherlands
*2005, [[Franz Oberwinkler]], Germany
*2006, [[Michael Wingfield]], South Africa
*2007, [[Jan Stenlid]], Sweden
*2008, [[Angela Restrepo]], Colombia; [[Gioconda San-Blas]], Venezuela

==External links==
* [http://www.msafungi.org/ Official webpage of the Mycological Society of America].
* [http://www.mycologia.org/ Mycologia], the scholarly journal of the MSA.
* [[North American Mycological Association]], MSA's sister society for amateur mycologists [http://www.namyco.org/]

[[Category:Mycology organizations]]
[[Category:Professional associations based in the United States]]
[[Category:Learned societies of the United States]]
[[Category:Natural Science Collections Alliance members]]
[[Category:1932 establishments in the United States]]
[[Category:Scientific organizations established in 1932]]