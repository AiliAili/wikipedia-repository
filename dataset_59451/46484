{{pp-move-indef|small=yes}}
{{Use British English|date=March 2014}}
{{Use dmy dates|date=March 2014}}
{{Infobox film
| name = Belle
| image = Belle poster.jpg
| border = yes
| alt = 
| caption = Theatrical release poster
| director = [[Amma Asante]]
| producer = [[Damian Jones]]
| writer = [[Misan Sagay]]
| starring = {{Plainlist| 
* [[Gugu Mbatha-Raw]]
* [[Tom Wilkinson]]
* [[Miranda Richardson]]
* [[Penelope Wilton]]
* [[Sam Reid (actor)|Sam Reid]]
* [[Matthew Goode]]
* [[Emily Watson]]<!--Per billing block on poster.-->
}}
| music = [[Rachel Portman]]
| cinematography = Ben Smithard
| editing = [[Pia Di Ciaula]]<br />Victoria Boydell
| production companies = {{Plainlist| 
* DJ Films
* [[Isle of Man Film]]
* [[The Pinewood Studios Group|Pinewood Pictures]]
* [[British Film Institute|BFI]]
}}
| distributor = [[Fox Searchlight Pictures]]
| released = {{Film date|df=yes|2013|09|08|[[2013 Toronto International Film Festival|TIFF]]|2014|05|02|United States|2014|06|13|United Kingdom}}
| runtime = 104 minutes<!--Theatrical runtime: 104:17--><ref>{{cite web|title=''BELLE'' (PG)|url=http://www.bbfc.co.uk/releases/belle-film|work=[[20th Century Fox]]|publisher=[[British Board of Film Classification]]|date=25 March 2014|accessdate=27 March 2014}}</ref>
| country = United Kingdom
| language = English
| budget = $10.9 million
| gross = $16.5 million<ref>{{cite web|url= http://www.boxofficemojo.com/movies/?page=intl&id=belle.htm|title= Belle (2014)|work= [[Box Office Mojo]]|publisher= [[Amazon.com]]|accessdate= 28 July 2014}}</ref>
}}
'''''Belle''''' is a 2013 British [[Historical period drama|period]] [[drama film]] directed by [[Amma Asante]], written by [[Misan Sagay]] and produced by [[Damian Jones]]. It stars [[Gugu Mbatha-Raw]], [[Tom Wilkinson]], [[Miranda Richardson]], [[Penelope Wilton]], [[Sam Reid (actor)|Sam Reid]], [[Matthew Goode]], [[Emily Watson]], [[Sarah Gadon]], [[Tom Felton]] and [[James Norton (actor)|James Norton]].<ref>{{cite web|last=Mitchell |first=Wendy |url=http://www.screendaily.com/reports/spotlight-on-international-projects/belle/5048581.article |title=Belle &#124; Features &#124; Screen |publisher=Screendaily.com |date=2 November 2012 |accessdate=1 July 2013}}</ref><ref>{{cite news|url=http://www.hollywoodreporter.com/news/amma-asante-belle-first-production-still-385818 |title=Amma Asante Unveils First Glimpse of 'Belle' |work=The Hollywood Reporter |date= 2 November 2012|accessdate=1 July 2013 |first=Stuart |last=Kemp}}</ref>

The film is inspired by the 1779 painting of [[Dido Elizabeth Belle]] beside her cousin [[Lady Elizabeth Murray]], at [[Kenwood House]], which was commissioned by their great-uncle, [[William Murray, 1st Earl of Mansfield]], then [[Lord Chief Justice of England]]. Very little is known about the life of Dido Belle, who was born in the [[West Indies]] and was the illegitimate [[mixed-race]] daughter of Mansfield's nephew. She is found living in poverty by her father and entrusted to the care of Mansfield and his wife. The fictional film centres on Dido's relationship with an aspiring lawyer; it is set at a time of legal significance, as a court case is heard on what became known as the [[Zong massacre|''Zong'' massacre]], when slaves were thrown overboard from a slave ship and the owner filed with his insurance company for the losses. Lord Mansfield rules on this case in England's [[Court of King's Bench]] in 1786, in a decision seen to contribute to the Abolition of the Slave Trade Act of 1807.

==Plot==
Dido Elizabeth Belle Lindsay was born in 1761, the natural daughter of Maria Belle, an enslaved African woman in the West Indies, and Captain Sir John Lindsay, a British [[Royal Navy]] officer. After the death of Dido's mother, Captain Lindsay takes Dido from the slums of the West Indies in 1765 and entrusts her to his uncle William Murray, 1st Earl of Mansfield, the [[Lord Chief Justice of England and Wales|Lord Chief Justice]], and his wife, Elizabeth, who live at [[Kenwood House]], an estate in [[Hampstead]] outside London. Lord and Lady Mansfield raise Dido as a free gentlewoman, together with their niece Lady Elizabeth Murray, who came to live with them after her mother died and her father remarried. When the two cousins reach adulthood, the Mansfields commission an oil portrait of their two great-nieces, but Dido is unhappy about sitting for it as she is worried that it will portray her as a subordinate, similar to other portraits she has seen depicting aristocrats with black servants. Dido's father dies and leaves her the generous sum of £2,000 a year, enough to make her an heiress. Lady Elizabeth, by contrast, will have no income from her father, whose son from his new wife has been named his sole heir. Arrangements are made for Elizabeth to have her coming out to society, but Lord and Lady Mansfield believe no gentleman will agree to marry Dido because of her mixed-race status, so while she will travel to London with her cousin, she will not be "out" to society.

Lord Mansfield agrees to take a vicar's son, John Davinier, into an apprenticeship for law. In 1783, Mansfield hears the case of ''[[Gregson v. Gilbert]]'', regarding the payment of an insurance claim, for slaves killed when thrown overboard by the captain of a slave-ship &mdash; an event now known as the [[Zong Massacre|''Zong'' massacre]]. Dido helps her uncle with his correspondence and after John tells her about the Zong case, she begins sneaking correspondence to him which he believes will advance the cause of the [[Abolitionism|abolitionists]]. Lord Mansfield and John have a disagreement on the main issue of the case. John is told not to see Dido again, and his apprenticeship is at an end.  Dido’s aunts, Lady Mansfield and Lady Mary Murray, Lord Mansfield's sister, seek to steer Dido into an engagement with Oliver Ashford, son of a scheming grand dame and younger brother to the bigoted James Ashford. At first James is interested in Elizabeth but stops courting her once he finds out she will have no inheritance. Oliver, who is without fortune, proposes to Dido and she accepts, even though she continues to see John. James takes Dido apart, tells her she will disgrace his family's name, then insults and manhandles her. Dido would later tell Elizabeth of his true character and says she will give part of her inheritance to her for a dowry so she can find a different match. Lord Mansfield finds out about Dido's visits to John and confronts both of them. During the confrontation, John professes his love for Dido. Sometime later, Dido meets with Oliver and breaks off their engagement.

Dido is relieved when the painting is unveiled, showing her as Elizabeth's equal. She tells Lord Mansfield that the portrait commission proves that he can defy convention. Dido sneaks into the balcony of the Inn of Court, so that she can hear Lord Mansfield narrowly rule that the Gregson slave-trading syndicate, based in Liverpool, is not due insurance payments for the slaves the crew threw overboard during the voyage. The ship's officers claimed they ordered this action because they were out of potable water. Lord Mansfield discovers, however, that the ''Zong'' passed by many ports without stopping for more water, before killing the slaves. It appeared to Lord Mansfield that the slaves were over-crowded, making them sick and not likely to fetch a high price at auction, so the officers decided they would be worth more in insurance payments after their loss. Lord Mansfield sees John and Dido outside the Court after his ruling and says that Dido can only marry a gentleman. Therefore, he agrees to resume John's apprenticeship in law, so that he can become a lawyer. Dido and John share a kiss, both in full acknowledgement of their romantic feelings.

On-screen text informs the viewer that Dido and John married and had two sons (while they actually had 3 sons in real life), that Elizabeth also married and had children, and that the painting hung at Kenwood House until 1922, when it was moved to Scone palace in Scotland, the birthplace of Lord Mansfield.

==Cast==
* [[Gugu Mbatha-Raw]] as [[Dido Elizabeth Belle]], the protagonist. She is a strong-willed and well-educated young woman, born from Captain Sir John Lindsay's affair with a slave woman. Since her father acknowledged her as his child and later left her a considerable inheritance, she's part of the high society and free not to pursue a marriage. She gradually befriends John Davinier over the Zong case, and her input eventually leads Lord Mansfield to rule against the traders.
* [[Tom Wilkinson]] as [[William Murray, 1st Earl of Mansfield]], the great-uncle of Dido and Elizabeth (Dido's father being the son of Lord Mansfield's sister, and Elizabeth's father the son of Lord Mansfield's brother), who's their guardian since they were children. As Lord Chief Justice, he is the most powerful judge in England. Upon Dido's arrival to his mansion, he quickly takes to loving her as if she was her own, despite her heritage.
* [[Sam Reid (actor)|Sam Reid]] as John Davinier, a vicar's son who seeks apprenticeship from Lord Mansfield in pursuit of a career as a lawyer. He's idealistic and passionate in his moral convictions, and meets with an abolitionist group.
* [[Emily Watson]] as Lady Elizabeth Mansfield, Lord Mansfield's wife. She is very caring toward her nieces, although she sees order and etiquette as essential. She tries very hard to find Elizabeth a suitable match.
* [[Sarah Gadon]] as [[Lady Elizabeth Murray]], Elizabeth is cheerful and affectionate toward her cousin Dido, and the two of them are inseparable since childhood and consider themselves sisters. Due to Elizabeth's father having a male heir from his second marriage, she's left without a dowry, which complicates her finding a suitable husband.
* [[Miranda Richardson]] as Lady Ashford, a scheming lady who attempts to marry off her two sons to the Mansfield girls. She feels contempt for Dido's heritage, but she's able to put that aside in order to let his son James put his hands on Dido's dowry.
* [[Penelope Wilton]] as Lady Mary Murray, the manager of the household and governess of Dido and Elizabeth. Her demeanor is firm but caring, and teaches Elizabeth and Dido the ways of etiquette, music and embroidery. She is a spinster but had a one-time gentleman caller.
* [[Tom Felton]]<ref>{{cite web | url = http://www.empireonline.com/news/story.asp?NID=35112 | title = Tom Felton Rings The Belle |work = Empire |date = | accessdate =1 July 2013}}</ref> as James Ashford, the arrogant firstborn of Lord and Lady Ashford who also shares his mother's contempt for Dido's heritage & even assaults her at one point. He courts Elizabeth until he discovers her impoverished status.  
* [[James Norton (actor)|James Norton]] as Oliver Ashford, the younger son of Lord and Lady Ashford. He is attracted to Dido's beauty and wealth, but he is not completely immune from his family's prejudices.
* [[Matthew Goode]] as Captain [[John Lindsay (Royal Navy officer)|Sir John Lindsay]], the birth father of Dido. After Dido's mother died, he acknowledged his daughter and took her to the Mansfield household, begging his uncle and aunt to take her into their guardianship. He dies while Dido is still young without her having a chance to know him better.
* [[Alex Jennings]] as Lord Ashford, a high-ranked judge and Lady Ashford's husband. He hurries Lord Mansfield toward a decision on the Zong case.
* [[imdbname:4285713|Bethan Mary-James]] as Mabel, a dark-skinned servant at the Mansfield household. Dido worries that she may be a slave, but Lord Mansfield assures her Mabel is free and being paid a "very respectable" wage.
* [[James Northcote (actor)|James Northcote]] as Mr. Vaughn, Elizabeth's more gentle suitor.

==Painting==
[[File:Dido Elizabeth Belle.jpg|thumb|1779 painting of [[Dido Elizabeth Belle]] (1761–1804) and her cousin [[Lady Elizabeth Murray]] (1760–1825).]]
The 1779 painting, once thought to be by [[Johann Zoffany]], is now attributed to an unknown artist.<ref>{{cite web|title=Dido and Elizabeth Portrait|url=http://www.english-heritage.org.uk/content/images/people-faces/portraits/__upload__img_200__18_dido.jpg|website=English Heritage|accessdate=July 2013}}</ref> The painting hung in [[Kenwood House]] until 1922 and now hangs at [[Scone Palace]] in [[Perthshire]], [[Scotland]]. It was one of the first European portraits to portray a black women on an equal eye-line with a white aristocrat, though distinctions are implied by the poses, as Elizabeth's "formality and bookishness are contrasted with the wild and exotically turbanned 'natural' figure of Belle."<ref>Bindman, David, & [[Henry Louis Gates]], ''The Image of the Black in Western Art'', Harvard University Press, 2010, xviii.</ref><ref name="heritage1">English Heritage, {{cite web|title=Slavery and Justice at Kenwood House, Part 1|url=https://www.english-heritage.org.uk/content/imported-docs/p-t/leafletslave1final.pdf|accessdate=16 June 2014|publisher=English Heritage}}</ref>

The painting is replicated in the film with the faces of the actresses portraying the characters replacing those in the original. Dido's finger-to-cheek gesture is absent in the fictionalised version, as is her feathered turban. The original picture is shown on screen at the end of the film.

==Production==
Filming began on 24 September 2012. The film was shot on location in the [[Isle of Man]],<ref name=BBC_B>{{cite news |url= http://www.bbc.co.uk/news/world-europe-isle-of-man-19872128 |title=Isle of Man praised by Belle movie team |publisher= [[BBC News]] |date=9 October 2012 |accessdate=1 July 2013}}</ref><ref name="BBC_a">{{cite news |url= http://www.bbc.co.uk/news/world-europe-isle-of-man-19542706 |title= Filming of movie Belle 'to bring £1m' to Isle of Man |publisher= BBC News |date=9 October 2012|accessdate=1 July 2013}}</ref> Oxford<ref>{{cite web |first= Andrew |last= French |url= http://www.oxfordmail.co.uk/archive/2012/10/23/10000204.Oxford_is_the_backdrop_for_star_studded_movie/ |title= Oxford is the backdrop for star-studded movie | publisher= Oxford Mail |date=23 October 2012 |accessdate=1 July 2013}}</ref> and London. It is the first major British motion picture to be shot in true-4K, using Sony’s F65 CineAlta digital production camera.<ref>{{cite web |url= http://cineuropa.org/nw.aspx?t=newsdetail&l=en&did=230509 |title= Belle first major British 4K film |publisher= Cineuropa |date= |accessdate=1 July 2013}}</ref> The film was produced by DJ Films, [[Isle of Man Film]], and [[The Pinewood Studios Group|Pinewood Pictures]] with support from the [[British Film Institute|BFI]].<ref>{{cite web |author= |url= http://www.bfi.org.uk/news/full-cast-announced-bafta-award-winner-amma-asantes-belle |title= Full cast announced for BAFTA Award winner Amma Asante’s Belle |publisher= British Film Institute |date=7 September 2012 |accessdate=1 July 2013}}</ref>

Production designer [[Simon Bowles]] created the 18th-century Bristol Docks on the [[Isle of Man]] and created Kenwood House, based on a number of stately homes in the London area.

Original music for the film was composed by [[Rachel Portman]].<ref>{{cite web|url=http://filmmusicreporter.com/2013/05/08/rachel-portman-scoring-belle |title=Rachel Portman Scoring ‘Belle’ |work=Film Music Reporter |date=8 May 2013 |accessdate=1 July 2013}}</ref>

==Historical references==
The film is a work of historical fiction, inspired by a painting and the evidence that Dido was brought up at [[Kenwood House]]. The relative lack of details about Dido Elizabeth Belle allowed screenwriter Misan Sagay considerable artistic licence in framing the young woman's story, within the broader historical context of the slave economy and the abolition movement.

[[William Murray, 1st Earl of Mansfield]], who was [[Lord Chief Justice of England]] from 1756 to 1788, presided over two important cases, ''[[Somerset v Stewart]]'' in 1772 and the [[Zong massacre#Legal proceedings|''Zong'' insurance claims case]] in 1783, which helped lay the groundwork for Britain's  [[Slave Trade Act 1807]]. As in the film, he was the great-uncle of [[Dido Elizabeth Belle]] and [[Lady Elizabeth Murray]].

At the suggestion of the producers, [[HarperCollins]] published a companion book by biographer [[Paula Byrne]] recounting the lives of the film's principal characters.

==Historical accuracy==
James Walvin OBE, professor emeritus of the University of York, said of ''Belle'': "Much of the historical evidence is there – though festooned in the film with imaginary relishes and fictional tricks. Partly accurate, the whole thing reminded me of the classic Morecombe and Wise sketch with Andre Previn (Eric bashing away on the piano): all the right notes – but not necessarily in the right order."<ref name=HistoryExtra>{{cite web|title=Historian at the Movies: Belle reviewed|url=http://www.historyextra.com/feature/international-history/historian-movies-belle-reviewed|date=2 July 2014|website=History Extra}}</ref> Reviewing the film for ''History Extra'', the official website of ''BBC History Magazine'', Walvin noted that while the second half of the film centres on Dido Elizabeth Belle's involvement in the Zong case, in reality she was "nowhere to be found in the Zong affair".<ref name=HistoryExtra /> In the film "Tom Wilkinson’s Mansfield finds his cold legal commercial heart softened, and edged towards abolition by the eyelash-fluttering efforts of his stunning great niece" and his "adjudication becomes, not a point of law, but the first bold assertion towards the end of slavery". Walvin points out that "he merely stated that there should be another hearing of the Zong case – this time with evidence not known at the earlier hearing". Walvin awarded the film one star for enjoyment and two for historical accuracy.

==Authorship==
Some press coverage ahead of filming, cited [[Amma Asante]] as the sole writer of ''Belle'', as well as director.<ref>{{cite news|url=http://www.hollywoodreporter.com/news/belle-amma-asante-penelope-wilton-tom-felton-368382 |title=Penelope Wilton and Tom Felton Join Cast of Amma Asante's 'Belle' |work=The Hollywood Reporter|date= 7 September 2012|accessdate=1 July 2013|first=Stuart |last=Kemp}}</ref> Press releases that followed [[Fox Searchlight]]'s acquisition of the film, gave the final credit determined by the [[Writers Guild of America, West]] as "Written by Misan Sagay".<ref>{{cite web|first=Cameron |last=Cook |url=http://www.foxsearchlight.com/post/3763/exclusive-belle-gallery-at-usa-today/ |title=Exclusive BELLE gallery at USA Today |publisher=Fox Searchlight |date=22 July 2013 |accessdate=7 June 2014}}</ref><ref>{{cite web|last=Rosser |first=Michael |url=http://www.screendaily.com/news/fox-searchlight-acquires-belle/5057909.article/ |title=Fox Searchlight acquires Belle |publisher=Screendaily.com |date=1 July 2013 |accessdate=7 June 2014}}</ref>
[[Baz Bamigboye]] of ''[[The Daily Mail]]'' wrote that cast members [[Tom Wilkinson]] and [[Penelope Wilton]] had expressed 'incredulity' at the accreditation decision, because Wilkinson and Wilton had "only seen and worked from a script written by Amma", whose writing and direction of her debut film won her the [[Carl Foreman]] award at the [[British Academy of Film and Television Arts|BAFTAs]] in 2005.<ref name="dailymail-dispute">{{cite news|last=Bamigboye |first=Baz |url=http://www.dailymail.co.uk/tvshowbiz/article-2426045/BAZ-BAMIGBOYE-Im-going-fired--Cate-Blanchett-spent-week-thinking-going-axed-Woody-Allen.html |title=Ouch! Rival writers sharpen their nibs |work=Daily Mail |date=19 September 2013|accessdate=19 September 2013 |location=London}}</ref><ref>{{cite web|url=http://awards.bafta.org/award/2005/film/carl-foreman-award-for-special-achievement-by-british-director-producer-or-writer-first-feature) |title=BAFTA Film Awards 2005 |publisher=British Academy of Film and Television Arts |date= |accessdate=7 June 2014}}</ref> The article reported that Asante had been hired to re-draft an original screenplay written by Misan Sagay, after Sagay left the project due to serious ill-health. The subsequent arbitration process undertaken by the Writers Guild of America, determined that Sagay provided the bulk of content used in the script, so Sagay was awarded sole writing credit. Producer [[Damian Jones]] confirmed, "There was a WGA arbitration. The WGA made its decision on writing credits. And the production respects and abides by their decision."<ref name="dailymail-dispute" />

==Release==
In July 2013, it was announced that [[Fox Searchlight Pictures]] had acquired distribution rights for the film in the UK and USA.<ref>{{cite news|last=Kemp |first=Stuart |url=http://www.hollywoodreporter.com/news/fox-searchlight-picks-up-belle-578007 |title=Fox Searchlight Picks Up 'Belle' for North America, U.K. |work=Hollywood Reporter |date=1 July 2013 |accessdate=1 July 2013}}</ref> ''Belle'' [[Film premiere|premiered]] at the [[2013 Toronto International Film Festival]] on 8 September 2013.<ref name="TIFF2013">{{cite news|url=https://www.theguardian.com/film/2013/jul/23/toronto-film-festival-lineup |title=Toronto film festival 2013: the full line-up |accessdate=24 July 2013|work=The Guardian|location=London|date=23 July 2013}}</ref><ref name="TIFF">{{cite web |url=http://tiff.net/filmsandschedules/festival/2013/belle |title=Belle |accessdate=7 August 2013 |work=TIFF}}</ref> The film was released on 2 May 2014 in the United States, 9 May in Canada and 13 June 2014 in the United Kingdom.<ref>{{cite web|last=Internet Movie Database |url=http://www.imdb.com/title/tt2404181/releaseinfo?ref_=tt_ov_inf |title=Belle Release Info |accessdate=17 May 2014}}</ref>

==Reception==
The film received positive reviews from critics. Review aggregation website [[Rotten Tomatoes]] gives the film a "Certified Fresh" score of 83% based on reviews from 127 critics, with an average rating of 7/10. The site's consensus states, "It boasts all the surface beauty that fans of period pictures have come to expect, but ''Belle'' also benefits from its stirring performances and subtle social consciousness."<ref>{{cite web | url = http://www.rottentomatoes.com/m/belle_2014/ | title =Belle (2014) | work = [[Rotten Tomatoes]] | publisher = [[Flixster]] }}</ref> Critic [[Mark Kermode]] named it his fourth-favourite film of 2014.<ref>{{cite web | url = https://www.youtube.com/watch?v=pCWtjAN5cVo | title =My Top Ten Films of 2014 - Part 2 | work = Kermode Uncut | publisher = [[BBC]] }}</ref>

===Accolades===
{| class="wikitable plainrowheaders sortable" style="font-size: 95%;"
|-
! scope="col" class="unsortable" |Award
! scope="col" |Date of ceremony
! scope="col" |Category
! scope="col" |Recipients and nominees
! scope="col" |Result
|-
! scope="row" rowspan="2"| [[African-American Film Critics Association]]<ref>{{cite web|url=http://www.hollywoodreporter.com/news/african-american-film-critics-association-754985|title=African-American Film Critics Association Lauds 'Selma'|last=Kilday|first=Gregg|date=8 December 2014|work=[[The Hollywood Reporter]]|accessdate=13 December 2014}}</ref>
| rowspan="2"| 8 December 2014
| Best Actress
| Gugu Mbatha-Raw
| {{Won}}
|-
| Best Film
| ''Belle''
| {{Nom}}
|-
! scope="row" rowspan="5"| [[Black Reel Awards]]<ref>{{cite web|url=http://www.hitfix.com/in-contention/dear-white-people-selma-lead-15th-annual-black-reel-awards-nominations-2|title='Dear White People,' 'Selma' lead 15th annual Black Reel Awards nominations|last=Tapley|first=Kristopher|date=17 December 2014|publisher=[[HitFix]]|accessdate=3 January 2015}}</ref><ref>{{cite web|url=http://www.hitfix.com/in-contention/selma-wins-six-black-reel-awards-including-best-film|title='Selma' wins six Black Reel Awards, including best film|last=Tapley|first=Kristopher|date=20 February 2015|publisher=[[HitFix]]|accessdate=15 March 2015}}</ref>
| rowspan="5"| [[Black Reel Awards of 2015|19 February 2015]]
| [[Black Reel Award for Best Actress|Best Actress]]
| Gugu Mbatha-Raw
| {{Won}}
|-
| [[Black Reel Award for Best Director|Best Director]]
| Amma Asante
| {{Nom}}
|-
| [[Black Reel Award for Best Ensemble|Best Ensemble]]
| Toby Whale
| {{Nom}}
|-
| [[Black Reel Award for Best Film|Best Film]]
| ''Belle''
| {{Nom}}
|-
| [[Black Reel Award for Best Screenplay, Adapted or Original|Best Screenplay]]
| Misan Sagay
| {{Nom}}
|-
! scope="row" rowspan="2"| [[British Independent Film Awards]]<ref>{{cite web|url=http://www.digitalspy.co.uk/movies/news/a607496/71-and-pride-lead-british-independent-film-awards-2014-nominations.html#~oYDTP0LJ08cd2W|title='71 and Pride lead British Independent Film Awards 2014 nominations|last=Alexander|first=Susannah|date=3 November 2014|publisher=[[Digital Spy]]|accessdate=16 December 2014}}</ref><ref>{{cite news|url=https://www.theguardian.com/film/2014/dec/08/british-independent-film-awards-pride-belle-71-brendan-gleeson|title=Pride, '71 and Belle head prizes at British independent film awards|last=Ellis-Petersen|first=Hannah|date=8 December 2014|work=[[The Guardian]]|accessdate=16 December 2014}}</ref>
| rowspan="2"| [[British Independent Film Awards 2014|7 December 2014]]
| [[BIFA Award for Best Performance by an Actress in a British Independent Film|Best Actress in a British Independent Film]]
| Gugu Mbatha-Raw
| {{Won}}
|-
| Best Newcomer
| Gugu Mbatha-Raw
| {{Nom}}
|-
! scope="row"| [[Chicago Film Critics Association]]<ref>{{cite web|url=http://www.chicagofilmcritics.org/awards/130-birdman-leads-2014-cfca-nominations|title="Birdman" Leads 2014 CFCA Nominations|date=12 December 2014|publisher=[[Chicago Film Critics Association]]|accessdate=12 December 2014}}</ref>
| [[Chicago Film Critics Association Awards 2014|15 December 2014]]
| Most Promising Performer
| Gugu Mbatha-Raw
| {{Nom}}
|-
! scope="row"| [[Empire Awards]]<ref>{{cite web|url=http://www.digitalspy.co.uk/movies/news/a631016/the-imitation-game-leads-empire-awards-2015-nominations.html#~p5dWfPiROCgHuX|title=The Imitation Game leads Empire Awards 2015 nominations|last=Tobin|first=Christian|date=24 February 2015|publisher=[[Digital Spy]]|accessdate=15 March 2015}}</ref>
| [[20th Empire Awards|29 March 2015]]
| [[Empire Award for Best Female Newcomer|Best Female Newcomer]]
| Gugu Mbatha-Raw
| {{Nom}}
|-
! scope="row"| [[London Film Critics' Circle]]<ref>{{cite web|url=http://www.digitalspy.co.uk/movies/news/a616954/mr-turner-leads-london-critics-circle-film-awards-nominations.html#~oYDGhlzciVf0ns|title=Mr Turner leads London Critics' Circle Film Awards nominations|last=Finbow|first=Katy|date=16 December 2014|publisher=[[Digital Spy]]|accessdate=16 December 2014}}</ref>
| [[London Film Critics Circle Awards 2014|18 January 2015]]
| [[London Film Critics' Circle Award for British Actress of the Year|British Actress of the Year]]
| Gugu Mbatha-Raw
| {{Nom}}
|-
! scope="row"| [[Miami International Film Festival]]<ref>{{cite web|url=http://www.signis.net/article.php3?id_article=6216|title=First SIGNIS Prize at the Miami International Film Festival 2014|publisher=[[SIGNIS]]|accessdate=16 December 2014}}</ref>
| 15 March 2014
| SIGNIS Award
| ''Belle''
| {{Won}}
|-
! scope="row" rowspan="5"| [[NAACP Image Award]]<ref>{{cite web|url=http://popwatch.ew.com/2014/12/09/naacp-image-awards-nominations/|title=NAACP Image Awards announce nominations for film and TV|last=Jue|first=Teresa|date=9 December 2014|work=[[Entertainment Weekly]]|accessdate=11 December 2014}}</ref><ref>{{cite web|url=http://deadline.com/2015/02/naacp-image-award-winners-2015-image-awards-winners-list-1201368201/|title='Selma', 'Black-Ish,' 'HTGAWM' Dominate NAACP Image Awards – Complete Winners List|date=6 February 2015|publisher=[[Deadline.com]]|accessdate=16 February 2015}}</ref>
| rowspan="5"| 6 February 2015
| [[NAACP Image Award for Outstanding Actress in a Motion Picture|Outstanding Actress in a Motion Picture]]
| Gugu Mbatha-Raw
| {{Nom}}
|-
| Outstanding Directing in a Motion Picture
| Amma Asante
| {{Nom}}
|-
| Outstanding Independent Motion Picture
| ''Belle''
| {{Won}}
|-
| [[NAACP Image Award for Outstanding Motion Picture|Outstanding Motion Picture]]
| ''Belle''
| {{nom}}
|-
| Outstanding Writing in a Motion Picture
| Misan Sagay
| {{Won}}
|-
! scope="row" rowspan="2"| [[Palm Springs International Film Festival]]<ref>{{cite web|url=http://www.psfilmfest.org/archive/featurefestivalawards.aspx|title=Feature Festival Awards Archive|publisher=Palm Springs International Film Society|accessdate=16 December 2014}}</ref><ref>{{cite news|url=http://archive.desertsun.com/article/20140103/LIFESTYLES010201/301030032/Palm-Springs-film-fest-Belle-director-incredible-weekend|title=Palm Springs film fest: 'Belle' director: 'It's an incredible weekend'|last=Fessier|first=Bruce|date=3 January 2014|work=[[The Desert Sun]]|accessdate=16 December 2014}}</ref>
| rowspan="2"| 3–13 January 2014
| Audience Award for Best Narrative Feature
| ''Belle''
| {{Nom}}
|-
| Directors to Watch
| Amma Asante
| {{Won}}
|-
! scope="row" rowspan="2"| [[Satellite Award]]<ref>{{cite web|url=http://www.thewrap.com/birdman-leads-satellite-awards-nominations/|title='Birdman' Leads Satellite Awards Nominations|last=Pond|first=Steve|date=1 December 2014|publisher=[[TheWrap]]|accessdate=16 December 2014}}</ref>
| rowspan="2"| 15 February 2015
| [[Satellite Award for Best Actress – Motion Picture|Best Actress in a Motion Picture]]
| Gugu Mbatha-Raw
| {{nom}}
|-
| [[Satellite Award for Best Costume Design|Best Costume Design]]
| Anushia Nieradzik
| {{nom}}
|-
! scope="row" rowspan="4"| [[Women Film Critics Circle]]<ref>{{cite web|url=http://www.hitfix.com/in-contention/the-homesman-leads-2014-women-film-critics-circle-nominations|title='The Homesman' leads 2014 Women Film Critics Circle nominations|last=Tapley|first=Kristopher|date=14 December 2014|publisher=[[HitFix]]|accessdate=12 January 2015}}</ref><ref>{{cite web|url=http://www.hitfix.com/in-contention/still-alice-cleans-up-at-the-women-film-critics-circle-awards|title='Still Alice' cleans up at the Women Film Critics Circle Awards|last=Tapley|first=Kristopher|date=22 December 2014|publisher=[[HitFix]]|accessdate=12 January 2015}}</ref>
| rowspan="4"| 16 December 2014
| Best Female Images in a Movie
| ''Belle''
| {{Nom}}
|-
| Best Movie by a Woman
| Amma Asante
| {{Nom}}
|-
| Best Woman Storyteller
| Misan Sagay
| {{Nom}}
|-
| Karen Morley Award
| ''Belle''
| {{Won}}
|-
|}

==See also==
* [[List of films featuring slavery]]

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Belle (2013 film)}}
* {{rotten-tomatoes|belle_2014|Belle}}
* {{IMDb title|2404181|Belle}}
* {{metacritic film|belle|Belle}}
{{Amma Asante}}

{{DEFAULTSORT:Belle}}
[[Category:2013 films]]
[[Category:2010s drama films]]
[[Category:2010s historical films]]
[[Category:British films]]
[[Category:British drama films]]
[[Category:British historical films]]
[[Category:English-language films]]
[[Category:Films about interracial romance]]
[[Category:Films about race and ethnicity]]
[[Category:Films about racism]]
[[Category:Films about slavery]]
[[Category:Films set in the 1780s]]
[[Category:Films set in London]]
[[Category:Films set in country houses]]
[[Category:Films shot in the Isle of Man]]
[[Category:Fox Searchlight Pictures films]]
[[Category:Icon Productions films]]