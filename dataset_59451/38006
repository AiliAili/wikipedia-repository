{{good article}}
{{Infobox lighthouse       
  | name = Black Rock Harbor Light<br>''Fayerweather Island''
  | image_name = Black Rock Harbor Lighthouse.JPG
  | caption = Black Rock Harbor Light in 2005
  | location = [[Fayerweather Island]]<br> [[Connecticut]]<br>[[United States]]
  | pushpin_map = Connecticut
  | relief = 1
  | pushpin = lighthouse
  | pushpin_map_caption = Connecticut
  | coordinates = {{coord|41|8|32.6|N|73|13|2.7|W|region:US-CT_type:landmark|display=inline,title}}
  | coordinates_footnotes = 
  | yearbuilt = 1808 (first)
  | yearlit = 1823 (current)
  | automated =  
  | yeardeactivated = 1932
  | foundation = fieldstone basement
  | construction = granite rubble and brownstone block tower
  | shape = octagonal prism tower with balcony and lantern
  | marking = white tower, black lantern
  | height = {{convert|41|ft|abbr=on}}
  | currentlens = decorative solar light
  | lens = 8 lamps, {{convert|14|in}} parabolic reflectors
  | range = {{convert|11|nmi}}
  | characteristic = F W
  | ARLHS = USA-059
  | managingagent = City of Bridgeport<ref>[https://www.unc.edu/~rowlett/lighthouse/ct.htm Black Rock Harbor] ''The Lighthouse Directory''. University of North Carolina at Chapel Hill. Retrieved 18 June 2016</ref><ref>[http://www.uscg.mil/history/weblighthouses/LHCT.asp Connecticut Historic Light Station Information & Photography] United States Coast Guard. Retrieved 18 June 2016</ref>
}}

'''Black Rock Harbor Light''', also known as '''Fayerweather Island Light''', is a [[lighthouse]] in [[Bridgeport, Connecticut]], United States which stands on the south end of [[Fayerweather Island]] and marks the entrance to [[Black Rock Harbor]].  The first lighthouse at the site, built by [[Abisha Woodward]] under contract with the United States government, was a wooden tower that was lit and made operational by 1808. A storm destroyed the tower in 1821 and the current, stone lighthouse was erected in its place in 1823. The Black Rock Harbor Light was an active navigational aid until 1933 when it was replaced by two automatic lights offshore. The beacon was subsequently given to the City of Bridgeport in 1934.  Two significant efforts during the 1980s and 1990s served to restore the aging tower and the light was relit as a non-navigational aid in 2000.  Black Rock Lighthouse is listed as a contributing property for Bridgeport's [[Seaside Park (Connecticut)|Seaside Park]] historic district.
[[File:Black Rock Harbor Light nowadays photo by Ting Xu.jpeg|thumb|Black Rock Harbor Light on Fayerweather Island]]

==Construction==
[[File:Black Rock Harbor Light.jpg|thumb|left|19th century view of the Light]]

In 1807, the United States government bought the {{convert|9.5|acre|ha|abbr=on}} of land upon which the lighthouse stands from Daniel Fayerweather for $200.<ref name="site">{{cite web | url=http://www.newenglandlighthouses.net/faywerweather-island-light-history.html | title=History of Fayerweather Island Lighthouse, Bridgeport, Connecticut | publisher=New England Lighthouses | accessdate=27 March 2014 | author=D'Entremont, Jeremy}}</ref> The government appropriated $5000 for the light station in February 1807 and contracted [[Abisha Woodward]] to construct the light.<ref name=site /><ref name="dentremont">{{cite web|url=http://lighthouse.cc/fayerweather/history.html|title=Fayerweather Island Lighthouse History|accessdate=2008-05-29|author=Jeremy D'Entremont}}</ref> The first Black Rock Harbor Light was a 40-foot (12.1 meter) octagonal wooden tower built in 1808.<ref name=site /> Abisha Woodward, who previously constructed the 1801 [[New London Harbor Light]] and the 1802 [[Falkner Island Light]], constructed a wooden tower that was lit and made operational by October 1808.<ref name="Dentremont" /> The lightkeeper's home was a small one and a half story home built on the opposite side of a marsh, several hundred feet away from the tower.<ref name="Dentremont" /> Another brick structure was built to house the oil.<ref name="Dentremont" />  The total cost of the wooden lighthouse, and likely the accompanying keeper's house and oil house, was listed at $4604.69.<ref name="book1">{{cite web | url=https://archive.org/details/cu31924021901842 | title=Statement of appropriations and expenditures for public buildings, rivers and harbors, forts, arsenals, armories, and other public works, from March 4, 1789, to June 30, 1882 | publisher=Washington Government Printing Office | year=1882 | accessdate=27 March 2014 | pages=349–350}}</ref>

On September 3, 1821, the wooden tower was destroyed in the [[1821 Norfolk and Long Island hurricane|Norfolk and Long Island hurricane]] and was replaced with a 40-foot (12.1 meter) octagonal stone tower at a cost of $2300.53.<ref name="ludlum2">{{cite web|author=David Ludlum|url=http://myweb.fsu.edu/jelsner/temp/HHITProject/HHITyears/1821/storm1_pt2_ludlum.pdf|title=The Norfolk and Long Island Hurricane - Sept. 3-4 - Pt. 2|accessdate=May 5, 2014|format=PDF}}</ref><ref name="Dentremont">{{cite book | title=The Lighthouses of Connecticut | publisher=Commonwealth Editions | author=D'Entremont, Jeremy | pages=49–53}}</ref><ref name="book1" /> Completed in 1823, the new tower was made of coursed sandstone [[ashlar]] and rubble mortar and was claimed by its builder to "withstand the storm of ages."<ref name=friends /> This boisterous claim was countered by Edmund Blunt, an American Coast Pilot, who stated, "a more contemptible Lighthouse does not disgrace Long Island Sound, most shamefully erected and badly kept."<ref name=friends /> In 1835, $2052.63 was used to preserve the buildings on Fayerweather Island, including the lighthouse.<ref name="book1" /> Three years later, in 1838, on the report of a Captain Gregory, another $1529.60 was used to build a seawall to protect the buildings.<ref name="book1" /> Another $15,000 would be used from 1847 through 1849 to complete the seawall.<ref name="book1" /> The original keeper's quarters, then referred to as a "dilapidated old edifice", was replaced in 1879.<ref name="Dentremont" />

== Service ==
The lighthouse originally had a whale oil spider lamp from the [[Stratford Point Light]], but it was upgraded to a system with eight lamps in 1830.<ref name="friends">{{cite web|url=http://www.lighthousefriends.com/light.asp?ID=789 |title=Fayerweather Island (Black Rock Harbor), CT |publisher=Lighthouse Friends |accessdate=27 March 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140625215532/http://www.lighthousefriends.com/light.asp?ID=789 |archivedate=25 June 2014 |df= }}</ref> In 1838, it was reported by Lieutenant George Bache that the reflectors were out of alignment and the light was barely visible in hazy conditions.<ref name="Dentremont" />  In 1854, a fifth-order Fresnel lens was installed.<ref name="friends" /> The Black Rock Harbor Light was deactivated in 1932 following the construction of two offshore automatic lights.<ref name="Dentremont" />

== Restoration ==
<!-- Deleted image removed: [[File:Black Rock Lighthouse.JPG|thumbnail|A recent image of the Lighthouse]] -->
The lighthouse was given to the city of [[Bridgeport, Connecticut]] in 1934 and became a part of Seaside Park.<ref name=site /> In the following years, vandals "gutted the interior" of the lighthouse.<ref name=site /> In 1977, the keeper's quarters building was destroyed in a fire.<ref name="nps"/> The first restoration effort came in 1983 when the Friends of Seaside Park  restored the tower.<ref name="Dentremont" /> Fayerweather Island was cleaned of debris, landscaped and established as a nature preserve.<ref name=site /> They installed steel "windows" and also secured the entry with a steel panel, but vandals again forced their way into the lighthouse.<ref name=site />

A second effort was mounted by David Grant Grimshaw and Patricia Roche in 1993.<ref name=site /> A sum of $25,000 cash and in-kind services was raised with fundraising and annual Preservation Balls; it was later matched by the City of Bridgeport's Board of Park Commissioners with another grant of $25,000.<ref name=site /> During the night of the 1996 Preservation Ball the lighthouse was mysteriously lit, but Grimshaw did not arrange the illumination.<ref name=site /> D'Entremont writes that "maybe the spirit of Kate Moore had grown tired of waiting for the restoration."<ref name=site /> The 1998 restoration was conducted under the direction of David Barbour, a local architect. The restoration included masonry repairs, reglazing the lantern room, rust removal from the railings, new doors and windows with vandal-proof steel panes.<ref name=site /> The lighthouse was repainted with graffiti-resistant paint in the original paint and mortar colors.<ref name=site /> The light, relit in 2000, does not serve as an active navigational aid.<ref name="nps">{{cite web|url=http://www.nps.gov/history/maritime/light/blackrk.htm|title=Inventory of Historic Light Stations--Connecticut Lighthouses--Black Rock Harbor Light|publisher=National Park Service|accessdate=2008-05-29}}</ref> The light is powered by solar panels on the top of the lighthouse; it was donated by United Illuminating and Bridgeport Energy.<ref name=site /><ref name="Dentremont" /> In 2004, vandals smashed the solar panels and new panels with protective cages were installed in 2007.<ref name=site />

== Access ==
The lighthouse is listed as a contributing property for Bridgeport's [[Seaside Park (Connecticut)|Seaside Park]] historic district, which was listed on July 1, 1982.<ref name="nris">{{Cite web|url={{NRHP url|id=82004373}}|title=National Register Information System|date=|work=National Register of Historic Places|publisher=National Park Service}}</ref> The grounds are accessible by parking at Seaside Park and crossing a breakwater, but the lighthouse is not open to the public.<ref name="main">{{cite web | url=http://www.newenglandlighthouses.net/fayerweather-island.html | title=Fayerweather Island (Black Rock Harbor) Lighthouse | publisher=New England Lighthouses.net | accessdate=4 May 2014 | author=D'Entremont, Jeremy}}</ref>

== List of keepers ==
{| class="wikitable"
|-
! Name !! Year !! Service Notes
|-
| John&nbsp;Maltbie<ref name=site />  || 1808‑1809 || Died in service after five months.<ref name=site /><ref name=friends />
|-
|  Charles&nbsp;Isaac&nbsp;Judson<ref name=site />    ||  1809‑1814     ||  Died in service.<ref name=site />
|-
|   Daniel&nbsp;Willson (Wilson)<ref name=site />   ||    1814‑c.&nbsp;1817   ||    
|-
|   Stephen&nbsp;Moore<ref name=site /> ||    1817‑1871  || A West Indies trader.<ref name="Dentremont" /> As his health failed, his duties were performed by his daughter, Catherine Moore, until his death.<ref name="Dentremont" /> 
|-
|   Catherine&nbsp;(Kate)&nbsp;Moore<ref name=site />   ||  1871‑1878     ||   Daughter of Stephen Moore. Performed the duties of a keeper from the age of 12.<ref name="Dentremont" /> Credited with saving 21 lives. Actual service is longer than official service due to her father's failing health; became keeper after his death until her retirement.<ref name="Dentremont" /> 
|-
|   Leonard&nbsp;Clark<ref name=site />   ||    1878‑1906  ||   Veteran of the Civil War and whaling captain. Served to his death in 1906.<ref name=friends />
|-
|   Mary&nbsp;Elizabeth&nbsp;Clark<ref name=site />   ||   1906    ||   Keeper for two months after her husband, Leonard Clark, died.<ref name=friends />
|-
|   John&nbsp;D.&nbsp;Davis<ref name=site />  ||   1906‑1932    ||   Previously in the Irish Lighthouse service. Last official keeper.<ref name=friends />
|-
|    Charles&nbsp;H.&nbsp;Gilmore<ref name=site />  ||     c.&nbsp;1933‑1952   ||   Caretaker.
|-
|}

== Notes ==
* The construction date of the lighthouse have been subject to some variance in reports. D'Entremont cites the contractor and that the lighthouse was in operation by October 1808.<ref name="dentremont" /> Various sources, including the [[National Park Service]], lists the established date as 1809.<ref>{{cite book | title=1994 Inventory of Historic Light Stations | publisher=U.S. Department of the Interior | author=Foster, Kevin | year=1994 | page=386 | isbn=0-16-045100-0}}</ref>{{rp|29}} D'Entremont's corrections and details are reflected for these dates.
* According to Lighthouse Friends, Isaac Judson was the second keeper and served from 1808 to 1817, with the position passing to Stephen Moore.<ref name="friends"/> D'Entremont's details are reflected due to the comprehensive listing of the keepers.<ref name=site />
* Historically, Fayerweather Island was also referred to as Fairweather Island.<ref name="dentremont" />

==See also==
{{stack|{{Portal|Connecticut|Lighthouses}}}}
*[[History of Bridgeport, Connecticut]]
*[[List of lighthouses in Connecticut]]
{{Clear}}

==References==
{{reflist}}

==External links==
{{commons category|Black Rock Harbor Lighthouse}}
* [http://www.uscg.mil/ United States Coast Guard] 

{{Lighthouses of Connecticut}}

[[Category:Buildings and structures in Bridgeport, Connecticut]]
[[Category:Lighthouses completed in 1808]]
[[Category:Lighthouses completed in 1823]]
[[Category:Lighthouses in Fairfield County, Connecticut]]
[[Category:Transportation in Bridgeport, Connecticut]]