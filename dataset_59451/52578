{{cleanup|date=December 2010}}
'''Plagiarism detection''' is the process of locating instances of [[plagiarism]] within a work or document. The widespread use of computers and the advent of the Internet has made it easier to plagiarize the work of others. Most cases of plagiarism are found in academia, where documents are typically essays or reports. However, plagiarism can be found in virtually any field, including novels, scientific papers, art designs, and source code.

Detection of plagiarism can be either manual or software-assisted. Manual detection requires substantial effort and excellent memory, and is impractical in cases where too many documents must be compared, or original documents are not available for comparison. Software-assisted detection allows vast collections of documents to be compared to each other, making successful detection much more likely.  

The practice of plagiarizing by use of sufficient word substitutions to elude detection software is known as [[rogeting]].<ref name=Grove>{{cite news|last1=Grove|first1=Jack|title=Sinister buttocks? Roget would blush at the crafty cheek  Middlesex lecturer gets to the bottom of meaningless phrases found while marking essays|url=https://www.timeshighereducation.co.uk/news/sinister-buttocks-roget-would-blush-at-the-crafty-cheek/2015027.article|accessdate=15 July 2015|publisher=Times Higher Education|date=7 August 2014}}</ref>

==Software-assisted detection==
Computer-assisted plagiarism detection (CaPD) is an [[Information retrieval|Information retrieval (IR)]] task supported by specialized IR systems, referred to as plagiarism detection systems (PDS).

===In text documents===
Systems for text-plagiarism detection implement one of two generic detection approaches, one being external, the other being intrinsic.<ref name="Stein07"/>
External detection systems compare a suspicious document with a reference collection, which is a set of documents assumed to be genuine.<ref name=Potthast09/>
Based on a chosen [[Information retrieval#Model types|document model]] and predefined similarity criteria, the detection task is to retrieve all documents that contain text that is similar to a degree above a chosen threshold to text in the suspicious document.<ref name=Stein07a/>
Intrinsic PDS solely analyze the text to be evaluated without performing comparisons to external documents. This approach aims to recognize changes in the unique writing style of an author as an indicator for potential plagiarism.<ref name=MeyerZuEissen06/>
PDS are not capable of reliably identifying plagiarism without human judgment. Similarities are computed with the help of predefined document models and might represent false positives.<ref name=Bao06/><ref name=Clough00/><ref name="Culwin01"/><ref name=Lancaster03/><ref name=Maurer07/>

====Approaches====
The figure below represents a classification of all detection approaches currently in use for computer-assisted plagiarism detection. The approaches are characterized by the type of similarity assessment they undertake: global or local. Global similarity assessment approaches use the characteristics taken from larger parts of the text or the document as a whole to compute similarity, while local methods only examine pre-selected text segments as input.

[[File:PDS Classification.png|thumb|center|upright=2.0|Classification of computer-assisted plagiarism detection methods]]

=====Fingerprinting=====
Fingerprinting is currently the most widely applied approach to plagiarism detection. This method forms representative digests of documents by selecting a set of multiple substrings ([[n-gram]]s) from them. The sets represent the fingerprints and their elements are called minutiae.<ref name=Hoad03/><ref name=Stein05/>
A suspicious document is checked for plagiarism by computing its fingerprint and querying minutiae with a precomputed index of fingerprints for all documents of a reference collection. Minutiae matching with those of other documents indicate shared text segments and suggest potential plagiarism if they exceed a chosen similarity threshold.<ref name="Brin95"/> Computational resources and time are limiting factors to fingerprinting, which is why this method typically only compares a subset of minutiae to speed up the computation and allow for checks in very large collection, such as the Internet.<ref name=Hoad03 />

=====String matching=====
[[String matching]] is a prevalent approach used in computer science. When applied to the problem of plagiarism detection, documents are compared for verbatim text overlaps. Numerous methods have been proposed to tackle this task, of which some have been adapted to external plagiarism detection. Checking a suspicious document in this setting requires the computation and storage of efficiently comparable representations for all documents in the reference collection to compare them pairwise. Generally, suffix document models, such as [[suffix tree]]s or suffix vectors, have been used for this task. Nonetheless, substring matching remains computationally expensive, which makes it a non-viable solution for checking large collections of documents.<ref name=Monostori00/><ref name=Baker93/><ref name=Khmelev03/>

=====Bag of words=====
Bag of words analysis represent the adoption of [[Vector space model|vector space retrieval]], a traditional IR concept, to the domain of plagiarism detection. Documents are represented as one or multiple vectors, e.g. for different document parts, which are used for pair wise similarity computations. Similarity computation may then rely on the traditional [[Cosine similarity|cosine similarity measure]], or on more sophisticated similarity measures.<ref name=Si97/><ref name="Dreher07"/><ref name=Muhr09/>

=====Citation analysis=====
Citation-based plagiarism detection (CbPD)<ref name=Gipp14/> relies on [[citation analysis]], and is the only approach to plagiarism detection that does not rely on the textual similarity.<ref name=Gipp10/> CbPD examines the citation and reference information in texts to identify similar [[pattern]]s in the citation sequences. As such, this approach is suitable for scientific texts, or other academic documents that contain citations. Citation analysis to detect plagiarism is a relatively young concept. It has not been adopted by commercial software, but a first prototype of a citation-based plagiarism detection system exists.<ref name=Gipp13/> Similar order and proximity of citations in the examined documents are the main criteria used to compute citation pattern similarities. Citation patterns represent subsequences non-exclusively containing citations shared by the documents compared.<ref name=Gipp10/><ref name=Gipp11a/> Factors, including the absolute number or relative fraction of shared citations in the pattern, as well as the probability that citations co-occur in a document are also considered to quantify the patterns’ degree of similarity.<ref name=Gipp10/><ref name=Gipp11a/><ref name=Gipp11/><ref name=Gipp09/>

=====Stylometry=====
[[Stylometry]] subsumes statistical methods for quantifying an author’s unique writing style<ref name=Holmes98/><ref name=Juola08/> and is mainly used for authorship attribution or intrinsic CaPD. By constructing and comparing stylometric models for different text segments, passages that are stylistically different from others, hence potentially plagiarized, can be detected.<ref name=MeyerZuEissen06 />

====Performance====
Comparative evaluations of plagiarism detection systems<ref name=Potthast09 /><ref name=HTW04/><ref name=HTW08/><ref name=HTW10/><ref name=Potthast10/><ref name=Potthast11/> indicate that their performance depends on the type of plagiarism present (see figure). Except for citation pattern analysis, all detection approaches rely on textual similarity. It is therefore symptomatic that detection accuracy decreases the more plagiarism cases are obfuscated.

[[File:PD Methods Detection Performance.png|thumb|center|upright=2.0|Detection performance of CaPD approaches depending on the type of plagiarism being present]]

Literal copies, aka copy and paste (c&p) plagiarism, or modestly disguised plagiarism cases can be detected with high accuracy by current external PDS if the source is accessible to the software. Especially substring matching procedures achieve a good performance for c&p plagiarism, since they commonly use lossless document models, such as [[suffix tree]]s. The performance of systems using fingerprinting or bag of words analysis in detecting copies depends on the information loss incurred by the document model used. By applying flexible chunking and selection strategies, they are better capable of detecting moderate forms of disguised plagiarism when compared to substring matching procedures.

Intrinsic plagiarism detection using [[stylometry]] can overcome the boundaries of textual similarity to some extent by comparing linguistic similarity. Given that the stylistic differences between plagiarized and original segments are significant and can be identified reliably, stylometry can help in identifying disguised and [[Paraphrasing of copyrighted material|paraphrased]] plagiarism. Stylometric comparisons are likely to fail in cases where segments are strongly paraphrased to the point where they more closely resemble the personal writing style of the plagiarist or if a text was compiled by multiple authors. The results of the International Competitions on Plagiarism Detection held in 2009, 2010 and 2011,<ref name="Potthast09" /><ref name="Potthast10" /><ref name="Potthast11" /> as well as experiments performed by Stein,<ref name=Stein11/> indicate that stylometric analysis seems to work reliably only for document lengths of several thousand or tens of thousands of words, which limits the applicability of the method to CaPD settings.

An increasing amount of research is performed on methods and systems capable of detecting translated plagiarisms. Currently, cross-language plagiarism detection (CLPD) is not viewed as a mature technology<ref name=Potthast10a/> and respective systems have not been able to achieve satisfying detection results in practice.<ref name=HTW10 />

Citation-based plagiarism detection using citation pattern analysis is capable of identifying stronger paraphrases and translations with higher success rates when compared to other detection approaches, because it is independent of textual characteristics.<ref name=Gipp10 /><ref name=Gipp11 /> However, since citation-pattern analysis depends on the availability of sufficient citation information, it is limited to academic texts. It remains inferior to text-based approaches in detecting shorter plagiarized passages, which are typical for cases of copy-and-paste or shake-and-paste plagiarism; the latter refers to mixing slightly altered fragments from different sources.<ref name=Weber-Wulff08/>

====Software====
	The design of plagiarism detection software for use with text documents is characterized by a number of factors:{{citation needed|date=September 2011}}
{| class="wikitable"
|-
! Factor
! Description and alternatives
|-
| '''Scope of search'''
| In the public internet, using search engines / Institutional databases / Local, system-specific database.{{citation needed|date=September 2011}}
|-
| '''Analysis time'''
| Delay between the time a document is submitted and the time when results are made available.{{citation needed|date=September 2011}}
|-
| '''Document capacity / Batch processing'''
| Number of documents the system can process per unit of time.{{citation needed|date=September 2011}}
|-
| '''Check intensity'''
| How often and for which types of document fragments (paragraphs, sentences, fixed-length word sequences) does the system query external resources, such as search engines.
|-
| '''Comparison algorithm type'''
| The algorithms that define the way the system uses to compare documents against each other.{{citation needed|date=September 2011}}
|-
| '''Precision and Recall'''
| Number of documents correctly flagged as plagiarized compared to the total number of flagged documents, and to the total number of documents that were actually plagiarized. High precision means that few [[false positives]] were found, and high recall means that few [[false negatives]] were left undetected.{{citation needed|date=September 2011}}
|}

Most large-scale plagiarism detection systems use large, internal databases (in addition to other resources) that grow with each additional document submitted for analysis. However, this feature is considered by some as a [[Turnitin#Concerns about violation of student copyright in the United States|violation of student copyright]].{{citation needed|date=September 2011}}

The following systems are mostly web-based, and are closed-source, with the exception of CitePlag and CopyTracker. The following list is non-exhaustive (see also [[Comparison of anti-plagiarism software]]):
{{col-begin|width=70%}}
{{col-break}}

:;Free of charge
:Chimpsky<ref>[http://chimpsky.uwaterloo.ca Chimpsky] at [[University of Waterloo]]. Canada.</ref><ref name="Abdelmoneim"/><ref name="Koovakkai"/>
:CitePlag<ref name="Gipp11a" /><ref name="Meuschke2012"/>
:CopyTracker<ref name="Abdelmoneim"/><ref name="Koovakkai"/><ref>{{cite web|url=http://copytracker.org |title=CopyTracker.org |publisher=[[École Centrale de Lille]]. France|archiveurl=https://web.archive.org/web/20120918012057/http://copytracker.org/|archivedate=2012-09-18}}</ref>
:[[eTBLAST]]
:Plagium<ref name="Koovakkai"/><ref name="Mapes"/>
:SeeSources<ref name="Koovakkai"/><ref>[http://www.plagscan.com/seesources/ SeeSources] at PlagScan.com. Germany.</ref>

{{col-break}}

:;Commercial
:[[Attributor]]
:[[Copyscape]]
:[[PlagTracker]]
:Iparadigms: [[Ithenticate]], [[Turnitin]]
:PlagiarismDetect<ref name="Abdelmoneim"/><ref name="Koovakkai"/>
:PlagScan<ref name="Abdelmoneim"/><ref name="Koovakkai"/>
:[[Unplag]]
:VeriGuide<ref>[http://www.veriguide.org Veriguide.org]. Department of Computer Science and Engineering, The [[Chinese University of Hong Kong]].</ref><ref name="King"/>
:The Plagiarism Checker<ref>[http://www.dustball.com/cs/plagiarism.checker PlagiarismChecker] at Dustball.com. US.</ref><ref name="Vij"/>
{{col-end}}

===In source code===
Plagiarism in computer source code is also frequent, and requires different tools than those used for text comparisons in document. Significant research has been dedicated to academic source-code plagiarism.<ref>[http://www.ics.heacademy.ac.uk/resources/assessment/plagiarism/research_sourcecode.html "Plagiarism Prevention and Detection - On-line Resources on Source Code Plagiarism"]. [[Higher Education Academy]], [[University of Ulster]].</ref>

A distinctive aspect of source-code plagiarism is that there are no [[essay mill]]s, such as can be found in traditional plagiarism. Since most programming assignments expect students to write programs with very specific requirements, it is very difficult to find existing programs that already meet them. Since integrating external code is often harder than writing it from scratch, most plagiarizing students choose to do so from their peers.

According to Roy and Cordy,<ref>Roy, Chanchal Kumar;Cordy, James R. (September 26, 2007).[http://research.cs.queensu.ca/TechReports/Reports/2007-541.pdf "A Survey on Software Clone Detection Research"]. School of Computing, [[Queen's University]], Canada.</ref> source-code similarity detection algorithms can be classified as based on either
* Strings – look for exact textual matches of segments, for instance five-word runs. Fast, but can be confused by renaming identifiers.
* Tokens – as with strings, but using a [[Lexical analysis|lexer]] to convert the program into [[token (parser)|token]]s first. This discards whitespace, comments, and identifier names, making the system more robust to simple text replacements. Most academic plagiarism detection systems work at this level, using different algorithms to measure the similarity between token sequences.
* [[parse tree|Parse Trees]] – build and compare parse trees. This allows higher-level similarities to be detected. For instance, tree comparison can normalize conditional statements, and detect equivalent constructs as similar to each other.
* [[call graph|Program Dependency Graphs]] (PDGs) – a PDG captures the actual flow of control in a program, and allows much higher-level equivalences to be located, at a greater expense in complexity and calculation time.
* Metrics – metrics capture 'scores' of code segments according to certain criteria; for instance, "the number of loops and conditionals", or "the number of different variables used". Metrics are simple to calculate and can be compared quickly, but can also lead to false positives: two fragments with the same scores on a set of metrics may do entirely different things.
* Hybrid approaches – for instance, parse trees + [[suffix tree]]s can combine the detection capability of parse trees with the speed afforded by suffix trees, a type of string-matching data structure.

The previous classification was developed for [[code refactoring]], and not for academic plagiarism detection (an important goal of refactoring is to avoid duplicate code, referred to as [[code clone]]s in the literature). The above approaches are effective against different levels of similarity; low-level similarity refers to identical text, while high-level similarity can be due to similar specifications. In an academic setting, when all students are expected to code to the same specifications, functionally equivalent code (with high-level similarity) is entirely expected, and only low-level similarity is considered as proof of cheating.

==See also==
* [[:Category:Plagiarism detectors]]
* [[Comparison of anti-plagiarism software]]
* [[Locality sensitive hashing]]
* [[Nearest neighbor search]]
* [[Kolmogorov complexity#Compression]] – used to estimate similarity between token sequences in several systems

==References==
{{reflist|30em|refs=
<ref name="Abdelmoneim">{{cite web |title=Plagiarism What is it? How to avoid it? |last=Abdelmoneim |first=Salah-Eldin |work=14th Alexandria Anaesthesia & Intensive Care Conference
 |publisher=Alexandria Faculty of Medicine |date=September 30, 2010 |url=http://www.alexaic.com/alexaicfiles/presentation2010/day3/028001.pdf }}</ref>

<ref name="Koovakkai">{{cite web |title=Diagnosing Plague: Tools And Techniques For Detecting Plagiarism |last=Koovakkai |first=Dineshan  |work=8th International CALIBER - 2011, Goa University, Goa |date=March 2–4, 2011 |publisher=INFLIBNET Centre, Ahmedaba, India |url=http://shodhganga.inflibnet.ac.in/dxml/bitstream/handle/1944/1629/37.pdf?sequence=1 }}</ref>

<ref name="Mapes">{{cite web |title=Steal this story? Beware Net’s plagiarism ‘cops’ |last=Mapes |first=Diane |publisher=MSNBC.com |date=September 10, 2009 |url=http://today.msnbc.msn.com/id/32657885/ns/today-today_tech/ }}</ref>

<ref name="King">{{cite book
 |title=Database Systems for Advanced Applications: 15th International Conference, 2010, Tsukuba, Japan, Proceedings, Part II 
 |first=Irwin |last=King
 |chapter=Introduction to Social Computing
 |page=482
 |editors=Kitagawa, et al.
 |date=April 1, 2010
 |publisher= |url=
 }}</ref>

<ref name="Vij">{{cite journal
 |title=Encouraging Academic Honesty through Anti-plagiarism Software
 |last=Vij |first=Rajeev |last2=Soni |first2=Navin Kumar |last3=Makhdumi |first3=Gayas 
 |work=7th International CALIBER-2009, Pondicherry University, Pondicherry
 |date=February 25–27, 2009 
 |publisher=INFLIBNET Centre, Ahmedabad, India
 |page=444
 |url=http://ir.inflibnet.ac.in/dxml/bitstream/handle/1944/1068/55.pdf?sequence=1
 |archiveurl=https://web.archive.org/web/20120324194342/http://shodhganga.inflibnet.ac.in/dxml/bitstream/handle/1944/1629/37.pdf?sequence=1|archivedate=2012-03-24
}}</ref>

<ref name="Stein07">{{citation	
 | last=Stein | first=Benno | last2=Koppel | first2=Moshe
 | last3=Stamatatos | first3=Efstathios
 | title=Plagiarism Analysis, Authorship Identification, and Near-Duplicate Detection PAN’07
 | journal=SIGIR Forum
 | volume=41
 | issue=2
 |date=Dec 2007
 | doi=10.1145/1328964.1328976
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2007o.pdf
}}</ref>

<ref name=Potthast09>{{citation				
 | last=Potthast | first=Martin | last2=Stein | first2=Benno
 | last3=Eiselt | first3=Andreas | last4=Barrón-Cedeño | first4=Alberto
 | last5=Rosso | first5=Paolo
 | contribution=Overview of the 1st International Competition on Plagiarism Detection
 | year=2009
 | title=PAN09 - 3rd Workshop on Uncovering Plagiarism, Authorship and Social Software Misuse and 1st International Competition on Plagiarism Detection
 | series=CEUR Workshop Proceedings
 | volume=502
 | pages = 1–9
 | issn=1613-0073
 | url=http://www.uni-weimar.de/medien/webis/research/events/pan-09/pan09-papers-final/potthast09-overview-first-international-competition-plagiarism-detection.pdf
}}</ref>

<ref name=Stein07a>{{citation
 | last=Stein  | first=Benno | last2=Meyer zu Eissen | first2=Sven
 | last3=Potthast | first3=Martin
 | contribution=Strategies for Retrieving Plagiarized Documents
 | year=2007
 | title=Proceedings 30th Annual International ACM SIGIR Conference
 | pages=825–826
 | publisher=ACM
 | ISBN=978-1-59593-597-7
 | doi=10.1145/1277741.1277928
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2007f.pdf
}}</ref>

<ref name=MeyerZuEissen06>{{citation
 | last=Meyer zu Eissen | first=Sven | last2=Stein | first2=Benno
 | contribution=Intrinsic Plagiarism Detection
 | year=2006
 | title=Advances in Information Retrieval 28th European Conference on IR Research, ECIR 2006, London, UK, April 10–12, 2006 Proceedings
 | series=Lecture Notes in Computer Science
 | volume=3936
 | pages=565–569
 | publisher=Springer
 | doi=10.1007/11735106_66
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2006d.pdf
}}</ref>

<ref name=Bao06>{{citation
 | last=Bao | first=Jun-Peng | last2=Malcolm | first2=James A.
 | contribution=Text similarity in academic conference papers
 | year=2006
 | title=2nd International Plagiarism Conference Proceedings
 | publisher=Northumbria University Press
 | url=http://www.plagiarismadvice.org/images/stories/old_site/media/2006papers/JunPengBao.pdf
}}</ref>

<ref name=Clough00>{{citation
 | last=Clough | first=Paul
 | title=Plagiarism in natural and programming languages an overview of current tools and technologies
 | year=2000
 | type=Technical Report
 | publisher=Department of Computer Science, University of Sheffield
 | url=http://www.ir.shef.ac.uk/cloughie/papers/plagiarism2000.pdf
}}</ref>

<ref name="Culwin01">{{citation
 | last=Culwin | first=Fintan | last2=Lancaster | first2=Thomas
 | title=Plagiarism issues for higher education
 | journal=Vine
 | volume=31
 | issue=2
 | year=2001
 | pages=36–41
 | doi=10.1108/03055720010804005
 | url=http://www.essaycoursework.com/howtowriteessaynet/pdf/plagiarism-higheredu.pdf
}}</ref>

<ref name=Lancaster03>{{citation
 | last=Lancaster | first=Thomas
 | title=Effective and Efficient Plagiarism Detection
 | year=2003
 | type=PhD Thesis
 | publisher=School of Computing, Information Systems and Mathematics South Bank University
 | url=http://www.bcu.academia.edu/documents/0009/4554/Lancaster_2003.pdf
}}</ref>

<ref name=Maurer07>{{citation
 | last=Maurer | first=Hermann | last2=Zaka | first2=Bilal
 | contribution=Plagiarism - A Problem And How To Fight It
 | year=2007
 | title=Proceedings of World Conference on Educational Multimedia, Hypermedia and Telecommunications 2007
 | pages=4451–4458
 | publisher=AACE
 | url=http://www.editlib.org/p/26021
}}</ref>

<ref name=Hoad03>{{citation
 | last=Hoad  | first=Timothy | last2=Zobel | first2=Justin
 | title=Methods for Identifying Versioned and Plagiarised Documents
 | journal=Journal of the American Society for Information Science and Technology
 | volume=54
 | issue=3
 | year=2003
 | pages=203–215
 | doi=10.1002/asi.10170
 | citeseerx = 10.1.1.18.2680 
 | url=http://goanna.cs.rmit.edu.au/~jz/fulltext/jasist-tch.pdf
}}</ref>

<ref name=Stein05>{{citation
 | last=Stein  | first=Benno
 | contribution=Fuzzy-Fingerprints for Text-Based Information Retrieval
 |date=July 2005
 | title=Proceedings of the I-KNOW ‘05, 5th International Conference on Knowledge Management, Graz, Austria
 | pages=572–579
 | publisher=Springer, Know-Center
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2005a.pdf
}}</ref>

<ref name="Brin95">{{citation
 | last=Brin  | first=Sergey | last2=Davis | first2=James
 | last3=Garcia-Molina | first3=Hector
 | contribution=Copy Detection Mechanisms for Digital Documents
 | year=1995
 | title=Proceedings of the 1995 ACM SIGMOD International Conference on Management of Data
 | pages=398–409
 | publisher=ACM
 | isbn=1-59593-060-4
 | doi=10.1145/223784.223855
 | url=http://ilpubs.stanford.edu:8090/112/1/1995-43.pdf
}}</ref>

<ref name=Gipp14>{{citation
  | last=Gipp| first=Bela | contribution=Citation-based Plagiarism Detection - Detecting Disguised and Cross-language Plagiarism using Citation Pattern Analysis
  | date= 2014
  | publisher=Springer Vieweg Research
  | isbn=978-3-658-06393-1
  | url = http://www.springer.com/springer+vieweg/it+%26+informatik/k%C3%BCnstliche+intelligenz/book/978-3-658-06393-1
}}</ref>

<ref name=Gipp13>{{citation
  | last=Gipp | first=Bela| last2=Meuschke| first2=Norman | last3=Breitinger | first3=Corinna 
  | last4=Lipinski | first4=Mario | last5=Nürnberger | first5=Andreas
  | contribution=Demonstration of Citation Pattern Analysis for Plagiarism Detection
  | date=28 Jul 2013 - Aug. 1| title=Proceedings of the 36th International ACM SIGIR Conference on Research and Development in Information Retrieval
  | publisher=ACM
  | doi=10.1145/2484028.2484214
  | url=http://sciplore.org/wp-content/papercite-data/pdf/gipp13.pdf
}}</ref>

<ref name=Monostori00>{{citation
  | last=Monostori  | first=Krisztián  | last2=Zaslavsky  | first2=Arkady 
  | last3=Schmidt  | first3=Heinz 
  | contribution=Document Overlap Detection System for Distributed Digital Libraries
  | year=2000
  | title=Proceedings of the fifth ACM conference on Digital libraries
  | pages=226–227
  | publisher=ACM
  | isbn=1-58113-231-X 
  | doi=10.1145/336597.336667
  | url=http://www.csse.monash.edu.au/projects/MDR/papers/dl2000-monostori.pdf
}}</ref>

<ref name=Baker93>{{citation 
 |last=Baker |first=Brenda S. | authorlink = Brenda Baker
 |title=On Finding Duplication in Strings and Software
 |date=February 1993
 |type=Technical Report
 |publisher=AT&T Bell Laboratories, NJ
 |url=http://cm.bell-labs.com/cm/cs/doc/93/2-bsb-1.ps.gz
 |format=gs 
}}</ref>

<ref name=Khmelev03>{{citation 
 |last=Khmelev
 |first=Dmitry V.
 |last2=Teahan
 |first2=William J.
 |contribution=A Repetition Based Measure for Verification of Text Collections and for Text Categorization
 |year=2003
 |title=SIGIR'03: Proceedings of the 26th annual international ACM SIGIR conference on Research and development in information retrieval
 |pages=104–110
 |publisher=ACM
 |doi=10.1145/860435.860456
 |citeseerx = 10.1.1.9.6155 
}}</ref>

<ref name=Si97>{{citation
 | last=Si  | first=Antonio  | last2=Leong  | first2=Hong Va
 | last3=Lau | first3=Rynson W. H.
 | contribution=CHECK: A Document Plagiarism Detection System
 | year=1997
 | title=SAC ’97: Proceedings of the 1997 ACM symposium on Applied computing
 | pages = 70–77
 | publisher = ACM
 | isbn=0-89791-850-9
 | doi=10.1145/331697.335176
 | url=http://www.cs.cityu.edu.hk/~rynson/papers/sac97.pdf
}}</ref>

<ref name="Dreher07">{{citation	
 | last=Dreher | first=Heinz
 | title=Automatic Conceptual Analysis for Plagiarism Detection
| journal=Information and Beyond: The Journal of Issues in Informing Science and Information Technology
 | volume=4
 | year=2007
 | pages=601–614
 | url=http://proceedings.informingscience.org/InSITE2007/IISITv4p601-614Dreh383.pdf
}}</ref>

<ref name=Muhr09>{{citation				
 | last=Muhr | first=Markus | last2=Zechner | first2=Mario
 | last3=Kern | first3=Roman | last4=Granitzer | first4=Michael
 | contribution=External and Intrinsic Plagiarism Detection Using Vector Space Models
 | year=2009
 | title=PAN09 - 3rd Workshop on Uncovering Plagiarism, Authorship and Social Software Misuse and 1st International Competition on Plagiarism Detection
 | series=CEUR Workshop Proceedings
 | volume=502
 | pages=47–55
 | issn=1613-0073
 | url=http://www.uni-weimar.de/medien/webis/research/events/pan-09/pan09-papers-final/zechner09-external-and-intrinsic-plagiarism-detection-using-vsm.pdf
}}</ref>

<ref name=Gipp09>{{citation
 | last=Gipp | first=Bela | last2=Beel | first2=Jöran
 | contribution=Citation Proximity Analysis (CPA) - A new approach for identifying related work based on Co-Citation Analysis
 |date=July 2009
 | title=Proceedings of the 12th International Conference on Scientometrics and Informetrics (ISSI’09)
 | pages=571–575
 | publisher=International Society for Scientometrics and Informetrics
 | issn=2175-1935
 | url=http://www.sciplore.org/publications/2009-Citation_Proximity_Analysis_(CPA)_-_A_new_approach_for_identifying_related_work_based_on_Co-Citation_Analysis_--_preprint.pdf}}</ref>

<ref name=Gipp10>{{citation
 | last=Gipp | first=Bela
 | last2=Beel | first2=Jöran
 | contribution=Citation Based Plagiarism Detection - A New Approach to Identifying Plagiarized Work Language Independently 
 |date=June 2010
 | title=Proceedings of the 21st ACM Conference on Hypertext and Hypermedia (HT'10)
 | pages=273–274
 | publisher=ACM
 | isbn=978-1-4503-0041-4
 | doi=10.1145/1810617.1810671
 | url=http://www.sciplore.org/publications/2010-Citation_Based_Plagiarism_Detection_-_A_New_Approach_to_Identify_Plagiarized_Work_Language_Independently_-_preprint.pdf
}}</ref>

<ref name=Gipp11>{{citation
 | last=Gipp | first=Bela | last2=Meuschke | first2=Norman | last3=Beel | first3=Jöran
 | contribution=Comparative Evaluation of Text- and Citation-based Plagiarism Detection Approaches using GuttenPlag
 |date=June 2011
 | title=Proceedings of 11th ACM/IEEE-CS Joint Conference on Digital Libraries (JCDL’11)
 | pages=255–258
 | publisher=ACM
 | isbn=978-1-4503-0744-4
 | doi=10.1145/1998076.1998124
 | url=http://www.sciplore.org/publications/2011-Comparative_Evaluation_of_Text-_and_Citation-based_Plagiarism_Detection_Approaches_using_GuttenPlag.pdf
}}</ref>

<ref name=Gipp11a>
{{citation
 | last=Gipp | first=Bela | last2=Meuschke | first2=Norman
 | contribution=Citation Pattern Matching Algorithms for Citation-based Plagiarism Detection: Greedy Citation Tiling, Citation Chunking and Longest Common Citation Sequence
 |date=September 2011
 | title=Proceedings of the 11th ACM Symposium on Document Engineering (DocEng2011)
 | pages=249–258
 | publisher=ACM
 | isbn= 978-1-4503-0863-2
 | doi=10.1145/2034691.2034741
 | url=http://www.sciplore.org/publications/2011-Citation_Pattern_Matching_Algorithms_for_Citation-based_Plagiarism_Detection--Greedy_Citation_Tiling,_Citation_Chunking_and_Longest_Common_Citation_Sequence.pdf
}}</ref>

<ref name=Holmes98>{{citation
 | last=Holmes | first=David I.
 | title=The Evolution of Stylometry in Humanities Scholarship
 | journal=Literary and Linguistic Computing
 | volume=13
 | issue=3
 | year=1998
 | pages=111–117
 | doi=10.1093/llc/13.3.111
}}</ref>

<ref name=Juola08>{{citation
 | last=Juola | first=Patrick
 | title=Authorship Attribution
 | journal=Foundations and Trends Information Retrieval
 | volume=1
 | year=2006
 | pages=233–334
 | issn=1554-0669
 | doi=10.1561/1500000005
 | url=http://www.mathcs.duq.edu/~juola/papers.d/fnt-aa.pdf
}}</ref>

<ref name="Meuschke2012">{{cite conference
 |last=Meuschke |first=Norman |last2=Gipp |first2=Bela |last3=Breitinger |first3=Corinna 
 |title=CitePlag: A Citation-based Plagiarism Detection System Prototype (conference paper)
 |date=May 31, 2012
 |conference=5th Annual Plagiarism Conference |location=Newcastle upon Tyne, UK
}}</ref>

<ref name=HTW04>{{citation
 | url=http://plagiat.htw-berlin.de/ff-alt/05hilfen/programme.html
 | title=Portal Plagiat - Softwaretest 2004
 | language = German
 | publisher=HTW University of Applied Sciences Berlin 
 | accessdate=October 6, 2011
}}</ref>

<ref name=HTW08>{{citation
 | url=http://plagiat.htw-berlin.de/software/2008/
 | title=Portal Plagiat - Softwaretest 2008
 | language = German
 | publisher=HTW University of Applied Sciences Berlin 
 | accessdate=October 6, 2011
}}</ref>

<ref name=HTW10>{{citation
 | url=http://plagiat.htw-berlin.de/software/2010-2/
 | title=Portal Plagiat - Softwaretest 2010
 | language = German
 | publisher=HTW University of Applied Sciences Berlin 
 | accessdate=October 6, 2011
}}</ref>

<ref name=Potthast10>{{citation
 | last=Potthast | first=Martin | last2=Barrón-Cedeño | first2=Alberto
 | last3=Eiselt | first3=Andreas | last4=Stein | first4=Benno
 | last5=Rosso | first5=Paolo
 | contribution=Overview of the 2nd International Competition on Plagiarism Detection
 | year=2010
 | title=Notebook Papers of CLEF 2010 LABs and Workshops, 22–23 September, Padua, Italy
 | url=http://clef2010.org/resources/proceedings/clef2010labs_submission_125.pdf
}}</ref>

<ref name=Potthast11>{{citation
 | last=Potthast | first=Martin | last2=Eiselt| first2=Andreas
 | last3=Barrón-Cedeño| first3=Alberto | last4=Stein| first4=Benno
 | last5=Rosso| first5=Paolo
 | contribution=Overview of the 3rd International Competition on Plagiarism Detection
 | year=2011
 | title=Notebook Papers of CLEF 2011 LABs and Workshops, 19–22 September, Amsterdam, Netherlands
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2011t.pdf
}}</ref>

<ref name=Stein11>{{citation
 | last=Stein | first=Benno | last2=Lipka | first2=Nedim
 | last3=Prettenhofer | first3=Peter
 | title=Intrinsic Plagiarism Analysis
 | journal=Language Resources and Evaluation
 | volume=45
 | issue=1
 | year=2011
 | pages=63–82
 | issn=1574-020X
 | doi=10.1007/s10579-010-9115-y
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2011a.pdf
}}</ref>

<ref name=Potthast10a>{{citation
 | last=Potthast | first=Martin | last2=Barrón-Cedeño | first2=Alberto
 | last3=Stein | first3=Benno | last4=Rosso | first4=Paolo
 | title=Cross-Language Plagiarism Detection
 | journal=Language Resources and Evaluation
 | volume=45
 | issue=1
 | year=2011
 | pages=45–62
 | issn=1574-020X
 | doi=10.1007/s10579-009-9114-z
 | url=http://www.uni-weimar.de/medien/webis/publications/papers/stein_2011b.pdf
}}</ref>

<ref name=Weber-Wulff08>{{citation
 | last=Weber-Wulff | first=Debora
 | contribution=On the Utility of Plagiarism Detection Software
 | date=June 2008 
 | title=In Proceedings of the 3rd International Plagiarism Conference, Newcastle Upon Tyne
 | url=http://archive.plagiarismadvice.org/images/stories/old_site/media/2008papers/P21%20Weber-Wulff.pdf
}}</ref>

}}<!-- closure for refs= -->

==Literature==
* Carrol, J. (2002). A'' handbook for deterring plagiarism in higher education''. Oxford: The Oxford Centre for Staff and Learning Development, Oxford Brookes University. (96 p.), ISBN 1873576560
* Zeidman, B. (2011). ''The Software IP Detective’s Handbook''.  Prentice Hall. (480 p.), ISBN 0137035330

== External links ==
* [http://www.checktext.org Online Plagiarism Search]
* [http://dejavu.vbi.vt.edu/dejavu Déjà Vu: a Database of Duplicate Citations in the Scientific Literature]
* [http://www.dmoz.org/Reference/Education/Educators/Academic_Dishonesty/Plagiarism/Detection/ Plagiarism Detection category at Open Directory Project]

{{DEFAULTSORT:Plagiarism Detection}}
[[Category:Educational assessment and evaluation]]
[[Category:Plagiarism]]