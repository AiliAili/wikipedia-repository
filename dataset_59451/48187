'''Eliza Anderson Godefroy''' was most likely the first woman to edit a general-interest magazine in the United States. In 1806 and 1807, at the age of 26, she was the founder and editor of a Baltimore publication called ''The Observer.''<ref>Wexler 2010, pp. 100, 105.</ref>

Godefroy—or Anderson, as she was then known—was the daughter of a prominent Baltimore physician, Dr. John Crawford. In 1799, at the age of 19, she married a local merchant named Henry Anderson, but he had abandoned his wife and infant daughter by 1801. In 1805, Anderson accompanied her friend and fellow Baltimorean [[Elizabeth Patterson Bonaparte]] during a journey to Europe that was undertaken in a vain attempt to convince [[Napoleon Bonaparte]] to recognize her marriage to his youngest brother, [[Jérôme-Napoléon Bonaparte]].<ref>Wexler 2010, p. 103.</ref>

At some point after Anderson returned to Baltimore in November 1805, she became associated with a weekly publication called ''The Companion and Weekly Miscellany'', which was published from November 1804 until October 1806. While the use of pseudonyms in the magazine—conventional at the time—makes it difficult to be sure exactly who was editing it, it appears that at some point in September 1806 Anderson became its editor. Shortly thereafter she decided to cease publication of ''The Companion'' and start a new magazine, ''The Observer'', that would cover a broader range of subjects and adopt a more satirical tone.<ref>Wexler 2010, pp. 103-105, 113.</ref>

Although initially there appears to have been some attempt to hide the fact that the editor of the new publication was female, after a few weeks Anderson adopted the pen name "Beatrice Ironside" for her editorial columns.<ref>Wexler 2010, p. 105.</ref> Shortly thereafter, Anderson addressed the matter of her gender head on, acknowledging that "much curiosity had been excited to know, what manner of woman our female editor may be," and undertaking to introduce herself to her readership.<ref>''The Observer'', February 21, 1807.</ref>

While a few widows had taken on the role of editing or publishing newspapers after the death of their husbands, the idea of a female editor was certainly a novel one in 1807.<ref>Wells 2008 pp. 166, 167-68.</ref> Secondary sources on the history of women editors have overlooked Anderson, identifying the first woman to edit a magazine as Mary Clarke Carr, who published the ''Intellectual Regale, or Ladies Tea Tray'', in Philadelphia beginning in 1814—seven years after Anderson began publishing ''The Observer''.<ref>Okker 1995, pp. 7-8; Branson 2008, p. 23.</ref> Moreover, Carr's magazine, like almost all magazines edited by women in the 19th century, was aimed at a female audience, while Anderson's publication was directed at both men and women.<ref>Branson 2008, p. 13; Okker 1995, pp. 1, 8, 13, 15-17.</ref>

Almost immediately, Anderson became embroiled in what was to be the first of a number of journalistic vendettas: her featured columnist, "Benjamin Bickerstaff," quit in a huff after Anderson made some sarcastic remarks about young Baltimore ladies and what Anderson perceived to be their affectations.<ref>Wexler 2010, pp. 105-06.</ref> Later in 1807, Anderson's sarcasm about the state of the arts and culture in Baltimore, which she found sadly lacking, brought accusations that she was elitist and unpatriotic.<ref>Wexler 2010 pp. 110-11, 115-18; Wood 2009, pp. 571-72.</ref> Further criticism and outrage came her way when, in the fall of 1807, Anderson's translation of a scandalous French novel, ''Claire d'Albe'' by [[Sophie Cottin]], was published.<ref>Cottin 1799/2002, p. xxvii; Wexler 2010, pp. 118-120.</ref>

By the end of 1807, having endured a fair number of attacks and weary of coping with subscribers who balked at paying their bills, Anderson decided to cease publication of ''The Observer''. She identified her gender as the primary source of the animosity against her. In one of the magazine's final issues, Anderson wrote, "It was a ''Woman'' who was [''The Observer'''s] editor, this was all that was necessary to render its enemies BRAVE, and this was enough to embolden the most ''pusillanimous Wight'' to assume the garb of the Lion."<ref>''The Observer'', December 19, 1807.</ref>  While there were certainly other reasons for the hostility Anderson encountered—her merciless satire, and the fact that her translation of ''Claire d'Albe'' offended contemporary standards of decency—it appears that the feeling against her was intensified by the fact that she was a woman.

In 1808, Anderson married the French architect [[Maximilian Godefroy]], who designed several notable buildings in Baltimore.<ref>Wexler 2010, p. 120.</ref> Godefroy found it difficult, however, to support himself in Baltimore, and in 1819 the family embarked for Europe. Shortly after they sailed, Mrs. Godefroy's 19-year-old daughter from her first marriage, also named Eliza, was taken ill with yellow fever aboard the ship and died. Eventually the Godefroys traveled to England and then to France, where Godefroy eked out a living as a government architect. Eliza Anderson Godefroy died in Laval, France, on October 2, 1839, at the age of fifty-nine.<ref>Wexler 2010, pp. 123-125.</ref>

==Footnotes==
{{Reflist}}

== References ==
* Branson, Susan (2008).''Dangerous to Know: Women, Crime, and Notoriety in the Early Republic''. University of Pennsylvania Press.
* Cottin, Sophie (Margaret Cohen trans., after Eliza Godefroy)(1799/2002). ''Claire d'Albe: An English Translation''. The Modern Language Association of America.
* Okker, Patricia (1995). ''Our Sister Editors: Sarah J. Hale and the Tradition of Nineteenth-Century American Women Editors''. University of Georgia Press.
* ''The Observer'', Baltimore, January–December 1807.
* Wells, Jonathan Daniel (2008). "A Voice in the Nation: Women Journalists in the Early Nineteenth-Century South," American Nineteenth-Century History, 9, p.&nbsp;166.
* Wexler, Natalie (2010). "`What Manner of Woman Our Female Editor May Be': Eliza Crawford Anderson and the Baltimore Observer, 1806-1807," ''Maryland Historical Magazine'', 105(2), p.&nbsp;100.
* Wood, Gordon (2009). ''Empire of Liberty: A History of the Early Republic, 1789-1815''.Oxford University Press.

{{DEFAULTSORT:Godefroy, Eliza Anderson}}
[[Category:Articles created via the Article Wizard]]
[[Category:1789 births]]
[[Category:1839 deaths]]
[[Category:American women writers]]
[[Category:19th-century women writers]]