{{Good article}}
{{DISPLAYTITLE:The Beginning and the End (''Millennium'')}}
{{Infobox television episode
| title = The Beginning and the End
| image =
| caption = 
| series = [[Millennium (TV series)|Millennium]]
| season = 2
| episode = 1
| airdate = September 19, 1997
| production = 5C01
| writer = [[Glen Morgan]]<br/>[[James Wong (producer)|James Wong]]
| director = [[Thomas J. Wright]]
| guests =
*[[Doug Hutchison]] as Polaroid Man
*Allan Zinyk as Brian Roedecker
*Norman Armour as Suited Man
*[[Mitchell Kosterman]] as Sheriff
*Judith Maxie as Finley
*Drew Reichelt as Dicky Bird Perkins
*Alan Robertson as Elderly Man
| prev = [[Paper Dove]]
| next = [[Beware of the Dog (Millennium)|Beware of the Dog]]
| episode_list = [[Millennium (season 2)|List of season 2 episodes]]<br>[[List of Millennium episodes|List of ''Millennium'' episodes]]
}}

"''''The Beginning and the End'''" is the first episode of the [[Millennium (season 2)|second season]] of the [[Television in the United States|American]] [[Crime (genre)|crime]]-[[Thriller (genre)|thriller]] television series ''[[Millennium (TV series)|Millennium]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] on September 19, 1997. The episode was written by [[Glen Morgan]] and [[James Wong (producer)|James Wong]], and directed by [[Thomas J. Wright]]. "The Beginning and the End" featured a guest appearance by [[Doug Hutchison]] as the Polaroid Man.

In this episode, [[Millennium Group]] profiler [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) must track down the man who has kidnapped his wife Catherine ([[Megan Gallagher]]). During his hunt, Group member Peter Watts ([[Terry O'Quinn]]) reveals that the Group is much more secretive and mysterious than Black had ever known.

"The Beginning and the End" marks the first episode produced with Morgan and Wong as co-executive producers; their tenure in charge of the series would last the entirety of the second season. Guest star Hutchison was a frequent collaborator with the writers, having worked together in several other series. The episode was seen by approximately 7.15 million households in its original broadcast, and has received mixed to positive reviews from television critics.

==Plot==

The episode begins [[in media res]] from the ending of the preceding episode, "[[Paper Dove]]", showing [[Millennium Group]] member [[Frank Black (character)|Frank Black]] ([[Lance Henriksen]]) returning by plane to Seattle with his wife Catherine ([[Megan Gallagher]]) and daughter Jordan ([[Brittany Tiplady]]). As Black takes Jordan to their car, Catherine is drugged and kidnapped by a strange man ([[Doug Hutchison]]). The abductor—the Polaroid Man—hides Catherine in his car and escapes with her to the mountains overlooking the city.

Black's fellow Group members arrive to help, though he had not yet contacted any of them. They set up roadblocks throughout the city but are unsuccessful in finding Catherine. Black returns home, where his colleague Peter Watts ([[Terry O'Quinn]]) tells him about trying to conceive a son with his wife. Watts had once been assigned to a child-murder case in which the dismembered infant's body had been found in a cooler. He believed that God would reward him with his longed-for son if he could find the killer—years later, he still only has his three daughters, which has caused him to realize it is impossible to sacrifice one thing to gain another. Watts then has a Group member install software on Black's computer, allowing him access to sensitive documents—Black comments that he thought he already had full access before. Watts also explains that the Group's interest in Black is the reason for the Polaroid Man's actions. Elsewhere, the Polaroid Man ties up Catherine in a dark room.

Black struggles to find anything useful while investigating the abduction. However, he begins to experience seemingly-psychic visions which lead him to believe she is being held in their former home. The police raid the address but find it empty; Black finds a polaroid of another house inside. He is able to track down the address of this house, but goes alone this time. Reaching it, he finds Catherine in the basement, bound to a rafter. He goes to untie her but is blinded by a camera flash. He struggles with the Polaroid Man, which we see through a series of photographs taken by the man's camera. Black is able to wrest the Polaroid Man's knife away from him and stab him to death.

Returning home, Catherine packs a suitcase for Black, telling him that she cannot have him in their home for the time being, believing that he sacrificed a part of himself in killing her attacker. She hopes that time apart might help him recover what he is missing inside; he takes the case and drives off.

==Production==

[[File:Harrison and Byrne-Talking Heads.jpg|thumb|right|"The Beginning and the End" features the song "[[Life During Wartime (song)|Life During Wartime]]" by [[Talking Heads]] ''(pictured 1978)''.]]

"The Beginning and the End" was written by frequent collaborators [[Glen Morgan]] and [[James Wong (producer)|James Wong]], and directed by [[Thomas J. Wright]]. Wright had previously directed five episodes of the first season—"[[Dead Letters (Millennium)|Dead Letters]]", "[[The Wild and the Innocent (Millennium)|The Wild and the Innocent]]", "[[The Thin White Line (Millennium)|The Thin White Line]]", "[[Powers, Principalities, Thrones and Dominions]]" and "[[Paper Dove]]"<ref name="season1book">{{cite DVD notes |title=Millennium: The Complete First Season |titlelink=Millennium (season 1) |origyear=1996–1997 |others=[[David Nutter]], et al |type=booklet |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref>—and would go on to direct a further twenty episodes over the series' run.<ref name="season2book">{{cite DVD notes |title=Millennium: The Complete Second Season |titlelink=Millennium (season 2) |origyear=1997–1998 |others=[[Thomas J. Wright]], et al |type=booklet |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref><ref name="season3book">{{cite DVD notes |title=Millennium: The Complete Third Season |titlelink=Millennium (season 3) |origyear=1998–1999 |others=[[Thomas J. Wright]], et al |type=booklet |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> Wright would also go on to direct "[[Millennium (The X-Files)|Millennium]]", the series' [[fictional crossover|crossover]] episode with its [[sister show]] ''[[The X-Files]]''.<ref name="millenniumxfiles">{{cite episode| episodelink=Millennium (The X-Files) | title=Millennium| series=[[The X-Files]] |credits =[[Thomas J. Wright]] (director); [[Vince Gilligan]] & [[Frank Spotnitz]] (writers) | network = [[Fox Broadcasting Company|Fox]] | season = 7 | number = 4 |airdate= November 28, 1999}}</ref> The episode was the fourth to have been written by Morgan and Wong, after "[[Dead Letters (Millennium)|Dead Letters]]", "[[522666]]" and "[[The Thin White Line (Millennium)|The Thin White Line]]" in the first season.<ref name="season1book"/> The duo would go on to pen a further eleven episodes over the course of the second season,<ref name="season2book"/> having taken the roles of co-executive producers for the season.<ref name="fall">{{cite web |url=http://www.highbeam.com/doc/1G1-56443413.html |title=Fall Watch; 'Millennium' takes new turn |publisher=''[[The Boston Herald]]'' |first=Harvey |last=Soloman |date=September 18, 1997 |accessdate=May 19, 2012}} {{subscription required}}</ref>

Doug Hutchison's character, credited here as "Polaroid Man", had previously appeared in the first season finale "Paper Dove". In that episode the character was credited as "The Figure", and been portrayed by Paul Raskin.{{sfn|Genge|1997|p=110}} The character had been a presence in the series since "[[Pilot (Millennium)|Pilot]]", but had often merely been alluded to without being seen.<ref>{{cite web |url=http://www.highbeam.com/doc/1G1-84000830.html |title=Anxiously Awaiting the 'Millennium': Chris Carter Anticipates the Hysteria |work=[[Los Angeles Daily News]] |date=October 25, 1996 |first= Jonathan |last=Storm |accessdate=July 4, 2012}} {{subscription required}}</ref> Hutchison had worked with Morgan and Wong several times before; they had first met during production of "[[Squeeze (The X-Files)|Squeeze]]", an episode of ''Millennium''{{'s}} [[sister show]] ''[[The X-Files]]'',{{sfn|Lowry|1996|p=105}} and again on Morgan and Wong's short-lived series ''[[Space: Above and Beyond]]''.<ref name="pearly">{{cite episode| episodelink=Pearly (Space: Above and Beyond) | title=Pearly | series=[[Space: Above and Beyond]] |credits =[[Charles Martin Smith]] (director); [[Richard Whitley]] (writer) | network = [[Fox Broadcasting Company|Fox]] | season = 1 | number = 18 |airdate= March 24, 1996}}</ref>

Discussing plans for the season, Morgan noted that "the Millennium Group is a much deeper organization" than seen in the first season, adding that "they're considering [Frank Black] for a candidate for the group (and) trying to show him that at the millennium there's going to be an event - either fire and brimstone or harmonic convergence". Wong spoke about how the character of Catherine Black, saying "there's a different relationship between Frank and his family this season because of the separation ... I think that will not only bring some kind of heartfelt drama but humor into it".<ref name="fall"/> The episode makes use of the [[Talking Heads]] song "[[Life During Wartime (song)|Life During Wartime]]", taken from their 1979 album ''[[Fear of Music]]''.<ref name="avc"/>

==Broadcast and reception==

"The Beginning and the End" was first broadcast on the [[Fox Broadcasting Company|Fox network]] on September 19, 1997.{{sfn|Shearman|Pearson|2009|p=145}} The episode earned a [[Nielsen rating]] of 7.3 during its original broadcast, meaning that {{nowrap|7.3 percent}} of households in the United States viewed the episode. This represented approximately {{nowrap|7.15 million}} households, and left the episode the fifty-third most-viewed broadcast that week.<ref name="ratings">{{cite web |url=http://www.highbeam.com/doc/1G1-67736489.html |title=NBC Holds off Charge By CBS |work=[[Rocky Mountain News]] |date=September 25, 1997 |first=Dave |last=Bauder |accessdate=July 5, 2012}} {{Subscription required}}</ref>{{#tag:ref|Each ratings point represented 980,000 households during the 1997–1998 television season.<ref name="ratings"/>|group="nb"}}

The episode received mixed to positive reviews from critics. Writing for ''[[The Buffalo News]]'', Alan Pergament rated the episode three stars out of five, describing it as "moody" but "muddled and confusing". Pergament noted that "the suspense of the premiere actually is enhanced by all the summer speculation about the future role of Gallagher", and felt that Morgan and Wong were "trying to bring "Millennium" into "X-File" territory".<ref>{{cite web |url=http://www.highbeam.com/doc/1P2-22946002.html |title=Time for 'Simpsons'; Forget 'Timecop' |work=[[The Buffalo News]] |date=September 19, 1997 |first=Alan |last=Pergament |accessdate=June 5, 2012}} {{subscription required}}</ref> ''[[The A.V. Club]]''{{'s}} Todd VanDerWerff rated the episode a A−, finding that it allows the series to "[turn] a corner, from a serial killer show with stained-glass window overtones, to a show that revels in those overtones, a show that plays in age-old symbols with a decided taste of the weird". VanDerWerff also felt that a monologue delivered by O'Quinn was an example of Morgan and Wong's best writing, and that the actor's delivery was the key to keeping the scene serious in tone.<ref name="avc">{{cite web |url=http://www.avclub.com/articles/reduxthe-beginning-and-the-end,53332/ |title="Redux"/"The Beginning and the End" {{!}} The X-Files/Millennium {{!}} TV Club |work=[[The A.V. Club]] |first=Todd |last=VanDerWerff |date=March 19, 2011  |accessdate=June 5, 2012}}</ref> Bill Gibron, writing for [[DVD Talk]], rated the episode 4.5 out of 5, noting that it "starts Season 2 off in high style". Gibron found that the episode "has a nice sense of internal adventure to it, giving us a chance to learn more about our main players while setting the groundwork for some certified surreality to come".<ref name="dvdtalk">{{cite web |url=http://www.dvdtalk.com/reviews/13926/millennium-season-2/?___rd=1 |title=Millennium: Season 2: DVD Talk Review of the DVD Video |first=Bill |last=Gibron |publisher=[[DVD Talk]] |date=January 3, 2005 |accessdate=June 5, 2012}}</ref> [[Robert Shearman]] and [[Lars Pearson]], in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', rated "The Beginning and the End" two-and-a-half stars out of five. Shearman felt that the episode had "an overwritten quality", citing the Polaroid Man's dialogue as the main example of this; he also believed that the development of Catherine Black as a character seemed poorly executed, with the character lacking enough depth to "retain the audience's sympathies".{{sfn|Shearman|Pearson|2009|p=145}}

==Notes==

<references group="nb"/>

==Footnotes==
{{reflist|colwidth=33em}}

===References===

*{{cite book | year=1997|first1=N. E. |last1=Genge | title=Millennium: The Unofficial Companion Volume Two |publisher=Century|isbn=0712678697 |ref=harv}}
*{{Cite book |title=The Truth is Out There: The Official Guide to the X-Files |first=Brian |last=Lowry |publisher=Harper Prism |year=1995 |isbn=0-06-105330-9 |ref=harv}}
*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=097594469X |ref=harv}}

==External links==
* {{IMDb episode|0648262}}
* {{Tv.com episode|25887}}

{{Millennium episodes|2}}

{{DEFAULTSORT:Beginning And The End, The}}
[[Category:Millennium (TV series) episodes]]
[[Category:1997 television episodes]]
[[Category:Television episodes in multiple parts]]