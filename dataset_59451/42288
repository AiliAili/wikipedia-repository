<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Avocet
  |image = AvAvocet.jpg
  |caption = <!--image caption; if it isn't descriptive it should be left blank-->
}}{{Infobox Aircraft Type
  |type = Fighter
  |manufacturer = [[Avro]]
  |designer = [[Roy Chadwick]]
  |first flight = 1927
  |introduced = <!--date the aircraft entered or will enter military or revenue service-->
  |introduction= <!--date the aircraft will enter military or revenue service, if the aircraft is still in development -->
  |retired = <!--date the aircraft left military or revenue service. If vague or multiples, it probably should be skipped-->
  |status = Prototype<!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Air Force]] 
  |more users = <!--limited to three "more users" total. please separate with <br/>-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = Two
  |unit cost = 
  |developed from = <!-- the aircraft which formed the basis for the topic type -->
  |variants with their own articles = <!-- variants OF the topic type -->
}}
|}

The '''Avro Type 584 Avocet ''' was a [[United Kingdom|British]] single-engined naval [[Fighter aircraft|fighter]] prototype, designed and built by [[Avro]]. While the Avocet was not built in numbers, one of the [[prototype]]s was used as a [[seaplane]] [[trainer (aircraft)|trainer]] for the [[Royal Air Force]]'s (RAF) High Speed Flight.

==Design and development==
The Avro 584 Avocet was designed by Avro's chief designer, [[Roy Chadwick]] to meet the requirements of [[List of Air Ministry Specifications|Specification 17/25]] for a Naval fighter.<ref name="mason fighter">{{Cite book|author=Mason, Francis K|title=The British Fighter since 1912|publisher=Naval Institute Press|year=1992|isbn= 1-55750-082-7}}</ref> It was a single-engined, all-metal [[biplane]], powered by a 230&nbsp;hp [[Armstrong Siddeley Lynx]] [[Piston engine|engine]], having interchangeable wheels and floats. Although it did not have [[folding wing]]s, it was designed to be easily dismantled for storage onboard ship.

Two prototypes were built, the first flying as a landplane in December 1927 and the second prototype flying as a seaplane in April 1928.<ref name="mason fighter"/> Both prototypes were evaluated for the [[Fleet Air Arm]] at [[RAF Martlesham Heath]], where, owing to the low-powered engine, their performance was seen to be unimpressive,<ref name="jackson avro">{{cite book |last= Jackson|first= A J |title=Avro Aircraft since 1908 |edition= 2nd|year= 1990 |publisher= Putnam Aeronautical Books|location= London |isbn= 0-85177-834-8}}</ref> and it was not ordered into production.

==Operational history==
Although no production occurred, the second prototype was used by the RAF's High Speed Flight at [[RAF Calshot|Calshot]] as a seaplane trainer for [[Schneider Trophy]] pilots.<ref name="jackson avro"/>

==Operators==
;{{UK}}
*[[Royal Air Force]]
**[[High Speed Flight RAF]]

==Specifications (Avocet (Wheeled undercarriage))==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=The British Fighter since 1912 <ref name="mason fighter"/> 
|crew=one
|capacity=
|length main= 24 ft 6 in
|length alt= 7.50 m
|span main= 29 ft 0 in
|span alt= 8.84 m
|height main= 11 ft 8⅜ in
|height alt= 3.57 m
|area main= 308 ft²
|area alt= 28.6 m²
|airfoil=
|empty weight main= 1,612 lb
|empty weight alt= 733 kg
|loaded weight main= 2,495 lb
|loaded weight alt= 1,134 kg
|useful load main= 
|useful load alt= 
|max takeoff weight main= 
|max takeoff weight alt= 
|more general=
|engine (prop)=[[Armstrong Siddeley Lynx]] IV
|type of prop= seven cylinder [[Radial engine|radial]]
|number of props=1
|power main= 230 hp
|power alt= 172 kW
|power original=
|max speed main= 133 mph
|max speed alt= 116 kn, 214 km/h
|cruise speed main= 
|cruise speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|stall speed main=  
|stall speed alt=  
|range main= 
|range alt= 
|ceiling main= 23,000 ft
|ceiling alt= 7,000 m
|climb rate main= 
|climb rate alt= 
|loading main= 8.10 lb/ft²
|loading alt= 40 kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.092 hp/lb
|power/mass alt= 0.15 kW/kg
|more performance=
|armament=*2 × fixed [[.303 British|.303 in]] (7.7 mm) [[Vickers machine gun]]s
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=*[[Vickers Vireo]]<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->
}}

==References==
{{Reflist}}

==External links==
{{commons category|Avro Avocet}}
* [http://www.britishaircraft.co.uk/aircraftpage.php?ID=260 British Aircraft Directory] (Dead link)

{{Avro aircraft}}

[[Category:British fighter aircraft 1920–1929]]
[[Category:Avro aircraft|Avocet]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]