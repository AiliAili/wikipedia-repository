[[File:Jonathan Wolfson CEO Solazyme.jpg|thumb|Jonathan S. Wolfson]]
'''Jonathan S. Wolfson''' is an American entrepreneur. He is the co-founder and Chief Executive Officer (CEO) of [[TerraVia]],<ref name="solazyme">{{cite web|url=http://solazyme.com/management-team |title=Management Team |publisher=Solazyme |date= |accessdate=2013-11-16}}</ref> a renewable oil and bioproducts company that produces, sustainable, pure, high-performance tailored algal oils using fermentation for multiple industries.<ref>{{cite news|url=https://www.nytimes.com/2013/06/23/business/for-solazyme-a-side-trip-on-the-way-to-clean-fuel.html?pagewanted=all|title=For Solazyme, a Side Trip on the Way to Clean Fuel|author=Diane Cardwell|newspaper=[[The New York Times]]|accessdate=2013-11-16}}</ref> In 2013, Wolfson was named by ''Forbes'' as one of the top twelve "Disruptors in Business."<ref name="forbes">{{cite web|url=http://www.forbes.com/special-report/2013/disruptors/jonathan-wolfson.html |title=Jonathan Wolfson - The Disruptors 2013 |publisher=Forbes |date=2013-10-30 |accessdate=2013-11-16}}</ref>

==Education==
Jonathan S. Wolfson has masters degrees in law and business from NYU.<ref>{{cite web|url=http://www.law.nyu.edu/alumni/almo/pastalmos/2011-12almos/jonathanwolfsonfebruary |title=Alumnus of the Month &#124; NYU School of Law |publisher=Law.nyu.edu |date= |accessdate=2013-11-16}}</ref>

==Business career==
Wolfson has held a variety of positions in finance, business, and law. Jonathan served as Vice President of Finance and Business Development at 7thOnline, as well as co-founder and Chief Operating Officer of InvestorTree.<ref name="mercurynews">{{cite web|url=http://www.mercurynews.com/ci_20972697/mercury-news-interview-jonathan-wolfson-solazyme-ceo-co-founder |title=Mercury News interview: Jonathan Wolfson, co-founder and CEO of Solazyme - San Jose Mercury News |publisher=Mercurynews.com |date= |accessdate=2013-11-16}}</ref> In 2003, Jonathan moved from New York to Palo Alto, California to found the renewable oil company, TerraVia (formerly Solazyme), with college friend Harrison Dillon.<ref name="nytimes">{{cite news|url=https://www.nytimes.com/2013/06/23/business/for-solazyme-a-side-trip-on-the-way-to-clean-fuel.html|title=For Solazyme, a Side Trip on the Way to Clean Fuel|author=Diane Cardwell|newspaper=[[The New York Times]]|accessdate=2013-11-16}}</ref> Jonathan first founded TerraVia with the goal of producing oil from microalgae for advanced biofuels, but along the way discovered that the company’s technology could produce tailored oils for chemicals, food and personal care applications as well.<ref name="nytimes"/> To date, TerraVia has commercial sales agreements with Unilever, Mitsui, Goulston, and more.<ref>{{cite news|url=https://www.nytimes.com/2013/09/25/business/energy-environment/unilever-to-buy-oil-derived-from-algae-from-solazyme.html|title=Unilever to Buy Oil Derived From Algae From Solazyme |author=Diane Cardwell|newspaper=[[The New York Times]]|accessdate=2013-11-16}}</ref>

==Other interests==
Wolfson has also served as adjunct assistant professor of economics at Hunter College of the City University of New York.<ref name="mercurynews" />  Wolfson is an active participant in many advisory groups, including sitting on the board of the Center for American Progress (CAP) Clean Tech Council. He is also a member of the board of directors of the Biotechnology Industry Organization (BIO) and is on the governing board of BIO's Industrial and Environmental Section. Additionally, Wolfson sits on the supervisory board of Avantium, a renewable chemicals company, and on the Executive Council for the Advanced Biofuels Association (ABFA).<ref name="solazyme" />

==Recognition==
In September 2009, Wolfson accepted the "Green Leap" award from the [[Clinton Global Initiative]] for his commitment to commercializing Solazyme's breakthrough technology for renewable oil production for fuel and for food.<ref>{{cite web|last=Garber |first=Megan |url=https://www.theatlantic.com/national/archive/2011/09/a-conversation-with-jonathan-wolfson-ceo-of-solazyme/245271/ |title=A Conversation With Jonathan Wolfson, CEO of Solazyme - Nicholas Jackson |publisher=The Atlantic |date=2011-09-21 |accessdate=2013-11-16}}</ref>

In 2013, Wolfson was named by ''Forbes'' as one of the top twelve "Disruptors in Business".<ref name="forbes" />

In 2015, Wolfson received the George Washington Carter Award for innovation in industrial biotechnology from the Biotechnology Industry Organization (BIO).

==References==
{{Reflist}}

{{DEFAULTSORT:Wolfson, Jonathan}}
[[Category:American business executives]]
[[Category:Living people]]