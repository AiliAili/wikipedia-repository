{{Infobox journal
| title = PLOS ONE
| cover = [[File:PLOS ONE logo 2012.svg]]
| editor = [[Joerg Heber]]
| discipline = [[Multidisciplinarity|Multidisciplinary]]
| abbreviation = PLOS ONE
| publisher = [[Public Library of Science]]
| country =
| frequency = Upon acceptance
| history = 2006–present
| openaccess = Yes
| license = [[Creative Commons Attribution License]] 4.0 International
| impact = 3.057
| impact-year = 2015
| website = http://journals.plos.org/plosone/
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 228234657
| LCCN = 2006214532
| CODEN =
| ISSN =
| eISSN = 1932-6203
}}
'''''PLOS ONE''''' (originally ''PLoS ONE'') is a [[peer-reviewed]] [[open access]] [[scientific journal]] published by the [[Public Library of Science]] (PLOS) since 2006. The journal covers [[primary research]] from any discipline within [[science]] and [[medicine]]. Operating under a pay-to-publish model, ''PLOS ONE'' publishes approximately 50% of submitted manuscripts.<ref name=":0" /> All submissions go through a pre-publication review by a member of the board of academic editors, who can elect to seek an opinion from an external reviewer. According to the journal, papers are not to be excluded on the basis of lack of perceived importance or adherence to a scientific field. Although the number of submissions decreased from 2013 to 2014, ''PLOS ONE'' remained the world’s largest journal by number of papers published (about 30,000 a year, or 85 papers per day). Numbers decreased further to 22,000 published papers in 2016,<ref name=":0">{{Cite web|url=http://retractionwatch.com/2017/03/15/plos-one-faced-decline-submissions-new-editor-speaks/|title=PLOS ONE has faced a decline in submissions – why? New editor speaks|date=2017-03-15|website=Retraction Watch|access-date=2017-03-24}}</ref>
and since 2017 ''PLOS ONE'' is the second largest journal (after ''[[Scientific Reports]]'').<ref>{{Cite web|url=https://scholarlykitchen.sspnet.org/2017/04/06/scientific-reports-overtakes-plos-one-as-largest-megajournal/|title=Scientific Reports Overtakes PLOS ONE As Largest Megajournal|last=By|date=2017-04-06|website=The Scholarly Kitchen|access-date=2017-04-06}}</ref>

== History ==

=== Development ===
The [[Gordon and Betty Moore Foundation]] awarded PLOS a $9 million grant in December 2002 and $1 million grant in May 2006 for its financial sustainability and launch of new free-access biomedical journals.<ref>{{cite web |url= http://www.moore.org/newsitem.aspx?id=524|title=Gordon and Betty Moore Foundation |year=|accessdate=December 17, 2002}}</ref><ref>{{cite web |url= http://www.moore.org/init-grants-awarded.aspx?init=116|title=Gordon and Betty Moore Foundation |year=|accessdate=May 2006}}</ref> Later, ''PLOS ONE'' was launched in December 2006 as a [[software release life cycle|beta version]] named ''PLoS ONE''. It launched with Commenting and Note making functionality, and added the ability to rate articles in July 2007. In September 2007 the ability to leave "[[trackback]]s" on articles was added.<ref>{{cite web|url=http://blogs.plos.org/plos/2007/09/trackbacks-are-here|author=Zivkovic, Bora|title=Trackbacks are here!}}</ref> In August 2008 it moved from a weekly publication schedule to a daily one, publishing articles as soon as they became ready.<ref>[http://www.dipity.com/plosone/PLoS-ONE-Milestones/ PLOS ONE Milestones], a timeline on [[Dipity]]</ref> In October 2008 ''PLoS ONE'' came out of "beta". Also in September 2009, as part of its [[Article level metrics|Article-Level Metrics]] program, ''PLoS ONE'' made the full online usage data—e.g., [[HTML]] [[page view]]s, [[PDF]], [[XML]] downloads—for every published article publicly available. In mid-2012, as part of a [[rebranding]] of PLoS as PLOS, the journal changed its name to ''PLOS ONE''.<ref name=Knutson2012)>{{cite web |url=http://blogs.plos.org/plos/2012/07/new-plos-look/ |title=New PLOS look |author=David Knutson |date=July 23, 2012 |work=PLOS BLOG |publisher=Public Library of Science |accessdate=6 August 2012|archiveurl=http://www.webcitation.org/69j2pzKwV |archivedate=6 August 2012}}</ref>

=== Output and turnaround ===

{| class="wikitable" align="right"
|-
! Year !! Papers Published
|-
| 2007 || 1,200<ref name="scimag2">{{cite web|url=http://news.sciencemag.org/people-events/2014/06/output-drops-worlds-largest-open-access-journal|title=Output Drops at World's Largest Open Access Journal|first=Jocelyn|last=Kaiser|publisher=Science Magazine|date=2014-06-04|accessdate=2015-10-26}}</ref> 
|-
| 2008 || 2,800<ref name="scimag2"/>
|-
| 2009 || 4,406<ref name="poeticecon">{{cite web|url=http://poeticeconomics.blogspot.com/2011/01/plos-one-now-worlds-largest-journal.html|first=Heather|last=Morrison|date=5 January 2011|accessdate=16 January 2011|work=Poetic Economics Blog|title=PLoS ONE: now the world's largest journal?}}</ref>
|-
| 2010 || 6,749<ref name="poeticecon" />
|-
| 2011 || 13,798<ref name="Taylorsquelching">Taylor, Mike. "[http://blogs.discovermagazine.com/crux/2012/02/21/its-not-academic-how-publishers-are-squelching-science-communication/ It’s Not Academic: How Publishers Are Squelching Science Communication]." ''[[Discover Magazine]]''. February 21, 2012. Retrieved on March 3, 2012.</ref>
|-
| 2012 || 23,468<ref>{{cite web|url=http://blogs.plos.org/everyone/2013/01/03/2012review/|first=Krista|last=Hoff|date=3 January 2013|accessdate=21 May 2013|work=everyONE Blog|title=''PLOS ONE'' Papers of 2012}}</ref>
|-
| 2013 || 31,500<ref>{{cite web|author= Kayla Graham |url=http://blogs.plos.org/everyone/2014/01/06/thanking-peer-reviewers/ |title=Thanking Our Peer Reviewers – EveryONEEveryONE |publisher=Blogs.plos.org |date=2014-01-06 |accessdate=2015-05-17}}</ref>
|-
| 2014 || 30,040<ref>{{Cite web|url=http://www.bioxbio.com/if/html/PLOS-ONE.html|title=PLoS One Impact Factor{{!}}2016{{!}}2015{{!}}2014 - BioxBio|website=www.bioxbio.com|access-date=2016-10-17}}</ref>
|-
| 2015 || 28,107<ref>{{Cite web|url=https://scholarlykitchen.sspnet.org/2016/02/02/as-plos-one-shrinks-2015-impact-factor-expected-to-rise/|title=As PLOS ONE Shrinks, 2015 Impact Factor Expected to Rise|last=Davis|first=Phil|date=2016-02-02|website=The Scholarly Kitchen|access-date=2016-10-17}}</ref>
|-
|2016
|22,054<ref>{{Cite web|url=https://scholarlykitchen.sspnet.org/2017/01/05/plos-one-output-drops-again-in-2016/|title=PLOS ONE Output Drops Again In 2016|last=Davis|first=Phil|date=2017-01-05|website=The Scholarly Kitchen|access-date=2017-01-05}}</ref>
|-
|}

The number of papers published by ''PLOS ONE'' grew rapidly from inception to 2013 and has since declined somewhat.  By 2010, it was estimated   to have become the largest journal in the world,<ref name="poeticecon"/> and in 2011, 1 in 60 articles indexed by PubMed were published by ''PLOS ONE''.<ref>{{cite web|url=http://blogs.plos.org/everyone/2011/12/20/plos-one-five-years-many-milestones/|first=Stacey|last=Konkeil|date=20 December 2011|accessdate=24 December 2011|work=everyONE Blog|title=''PLOS ONE'': Five Years, Many Milestones}}</ref>

At ''PLOS ONE'', the median review time has grown from 37 days to 125 days over the first ten years of operation, according to Himmelstein's analysis, done for ''[[Nature (journal)|Nature]]''. The median between acceptance and posting a paper on the site has decreased from 35 to 15 days over the same period. Both numbers for 2016 roughly correspond to the industry-wide averages for biology-related journals.<ref>{{cite journal 
|last1=Kendall
|first1=Powell
|title=Does it take too long to publish research?
|journal=[[Nature (journal)|Nature]]
|volume= 530
|issue=7589
|pages= 148–151
|date=11 February 2016
|doi=10.1038/530148a
|pmid=26863966
|url=http://www.nature.com/polopoly_fs/1.19320!/menu/main/topColumns/topLeftColumn/pdf/530148a.pdf
|accessdate=2016-03-10}}</ref><ref>{{cite web
 | url =http://blog.dhimmel.com/history-of-delays/
 | title =The history of publishing delays
 | last =Himmelstein
 | first =Daniel
 | date =10 February 2016
 | website =Satoshi Village
 | publisher =
 | access-date =2016-03-10}}</ref>

=== Management ===
The founding managing editor was Chris Surridge.<ref>{{cite web|url=http://poynder.blogspot.com/2006/06/open-access-stage-two.html|first=Richard|last=Poynder|date=15 June 2006|accessdate=27 March 2011|work=Open and Shut Blog|title=Open Access: Stage Two}}</ref> He was succeeded by Peter Binfield in March 2008, who was publisher until May 2012.<ref>{{cite web|url=http://blogs.plos.org/plos/2012/05/publisher-of-plos-one-moves-to-new-open-access-initiativ/|first=Peter|last=Jerram|date=8 May 2012|accessdate=22 June 2012|work=The official PLOS Blog|title=Publisher of PLOS ONE moves to new Open-Access initiative}}</ref> Damian Pattinson then held the chief editorial position until December 2015.<ref>{{Cite web|url=http://www.stm-publishing.com/research-square-hires-damian-pattinson-former-editorial-director-of-plos-one/|title=Research Square hires Damian Pattinson, former Editorial Director of PLOS ONE {{!}} STM Publishing News|website=www.stm-publishing.com|access-date=2016-09-17}}</ref> Joerg Heber was confirmed as [[editor-in-chief]] from November 2016.<ref>{{Cite web|url=http://blogs.plos.org/plos/2016/09/plos-appoints-dr-joerg-heber-editor-in-chief-of-plos-one/|title=PLOS appoints Dr. Joerg Heber Editor-in-Chief of PLOS ONE {{!}} The Official PLOS Blog|date=2016-09-16|access-date=2016-09-17}}</ref>

== Publication concept ==
''PLOS ONE'' is built on several conceptually different ideas compared to traditional peer-reviewed scientific publishing in that it does not use the perceived importance of a paper as a criterion for acceptance or rejection. The idea is that, instead, ''PLOS ONE'' only verifies whether experiments and data analysis were conducted rigorously, and leaves it to the scientific community to ascertain importance, post publication, through debate and comment.<ref name=MacCallum />
{{cquote|Each submission will be assessed by a member of the ''PLOS ONE'' Editorial Board before publication. This pre-publication peer review will concentrate on technical rather than subjective concerns and may involve discussion with other members of the Editorial Board and/or the solicitation of formal reports from independent referees. If published, papers will be made available for community-based open peer review involving online annotation, discussion, and rating.<ref name="PLOS ONE Journal Information">[http://www.plosone.org/static/information.action PLOS ONE Journal Information]. Plosone.org (2012-09-04). Retrieved on 2013-06-20.</ref>|PLOS ONE}}

According to ''[[Nature (journal)|Nature]]'', the journal's aim is to "challenge [[academia]]'s obsession with journal status and [[impact factor]]s".<ref name=judge>{{cite journal
 | author = Giles, J. | year = 2007 | url = http://www.nature.com/nature/journal/v445/n7123/full/445009a.html
 | title = Open-Access Journal Will Publish First, Judge Later
 | journal = [[Nature (journal)|Nature]] | volume = 445 | page = 9
 | doi = 10.1038/445009a
 | pmid = 17203032
 | issue = 7123
}}</ref> Being an online-only publication allows ''PLOS ONE'' to publish more papers than a print journal. In an effort to facilitate publication of research on topics outside, or between, traditional science categories, it does not restrict itself to a specific scientific area.<ref name=MacCallum>{{cite journal|last=MacCallum|first=C. J.|year=2006|url=http://biology.plosjournals.org/perlserv/?request=get-document&doi=10.1371/journal.pbio.0040401|title=ONE for All: The Next Step for PLOS|doi=10.1371/journal.pbio.0040401|journal=PLoS Biol.|volume=4|issue=11|page=e401|pmid=17523266|pmc=1637059}}</ref>

Papers published in ''PLOS ONE'' can be of any length, contain full color throughout, and contain supplementary materials such as multimedia files. Reuse of articles is subject to a [[Creative Commons licenses|Creative Commons Attribution License]], version 2.5. In the first four years following launch, it made use of over 40,000 external peer reviewers.<ref>{{cite web|url=http://blogs.plos.org/everyone/2010/12/29/thanking-our-peer-reviewers-2/|title=Thanking PLOS ONE Peer Reviewers|work=PLOS ONE|date=Dec 2010|accessdate=16 January 2011}}</ref> The journal uses an international board of academic editors with over 6,000 academics handling submissions and publishes approximately 50&thinsp;% of all submissions, after review by, on average, 2.9 experts.<ref>{{cite web|url=http://www.plosone.org/static/review.action|title=PLOS ONE Editorial and Peer-Review Process|work=PLOS ONE|year=2008|accessdate=12 December 2013}}</ref> Registered readers can leave comments on articles on the website.<ref name=judge />

== Business model ==
[[File:Welcome, Nature. Seriously (from PLoS) (5405189157).jpg|thumb|A welcome message from [[PLoS]] to [[Nature Publishing Group]] on the launch of [[Scientific Reports]],<ref>Allen, Liz (January 19, 2011) "[http://blogs.plos.org/plos/2011/01/welcome-nature-seriously-2/ Welcome, Nature. Seriously]", ([http://www.webcitation.org/64iEVCOB5 WebCite])</ref> inspired by a similar message sent in 1981 by [[Apple Inc.|Apple]] to [[IBM]] upon the latter's entry into the [[personal computer]] market with its [[IBM Personal Computer]].<ref>[https://www.flickr.com/photos/dullhunk/5405231875/ Welcome message from Apple to IBM] ([ WebCite])</ref>]]
As with all journals of the Public Library of Science, ''PLOS ONE'' is financed by charging authors a [[Article Processing Charge|publication fee]]. The [[Author-pays model|"author-pays" model]] allows PLOS journals to provide all articles to everybody for free (i.e., open access) immediately after publication.  As of October 2015, ''PLOS ONE'' charged authors US$1,495<ref>{{cite web|url=http://blogs.plos.org/plos/2015/09/plos-publication-costs-update/|title= PLOS Publication Costs Update |work=PLOS ONE|accessdate=3 July 2016}}</ref> to publish an article. Depending on circumstances, it may waive or reduce the fee for authors who do not have sufficient funds.<ref>{{cite web|url=http://www.plos.org/publications/publication-fees/|title=Publication Fees|publisher=PLOS|accessdate=1 January 2015}}</ref> This model has drawn criticism, however. In 2011 [[Richard Poynder]] posited that journals such as PLoS ONE that charge authors for publication rather than charging users for access may produce a [[conflict of interest]] that reduces peer review standards (accept more articles, earn more revenue).<ref>{{cite web|url=http://poynder.blogspot.com/2011/03/plos-one-open-access-and-future-of.html|first=Richard|last=Poynder|date=7 March 2011|accessdate=27 March 2011|work=Open and Shut Blog|title=PLOS ONE, Open Access, and the Future of Scholarly Publishing}}</ref> [[Stevan Harnad]] instead argues for a "no fault" peer-review model, in which authors are charged for each round of peer review, regardless of the outcome, rather than for publication.<ref>{{cite web|url=http://www.dlib.org/dlib/july10/harnad/07harnad.html|first=Stevan|last=Harnad|date=June–July 2011|accessdate=27 March 2011|title=No-Fault Peer Review Charges: The Price of Selectivity Need Not Be Access Denied or Delayed|work=D-Lib Magazine|doi=10.1045/july2010-harnad}}</ref> PLoS had been operating at a loss until 2009 but covered its operational costs for the first time in 2010,<ref>{{cite web|url=http://blogs.plos.org/plos/2011/07/2010-plos-progress-update/|title=2010 PLoS Progress Update|author=Peter Jerram|date=July 20, 2011|archiveurl=http://www.webcitation.org/64jfbeM0k|archivedate=January 16, 2012|accessdate=January 16, 2012}}</ref> largely due to the growth of ''PLOS ONE''.

=== Influence ===
{{Main article|Mega journal}}
The "''PLOS ONE'' model" has inspired a series of other journals,<ref name=Sitek>{{cite book
 | doi = 10.1007/978-3-319-00026-8_9
| chapter = Open Access: A State of the Art
| title = Opening Science
| page = 139
| year = 2014
| last1 = Sitek | first1 = Dagmar
| last2 = Bertelmann | first2 =Roland
| isbn = 978-3-319-00025-1
|editor1= Sönke Bartling |editor2= Sascha Friesike
|publisher=Springer
|url=http://book.openingscience.org/tools/open_access_state_of_the_art.html
}}</ref><ref name=Jackson>Rhodri Jackson and Martin Richardson, "Gold open access: the future of the academic journal?", Chapter 9 in Cope and Phillip (2014), p.223-248. ''The Future of the Academic Journal'', 2nd ed., Chandos Publishing, Jul 1, 2014, 478 pages.</ref><ref name=Bjork>Bo-Christer Björk and David Solomon, [http://www.wellcome.ac.uk/stellent/groups/corporatesite/@policy_communications/documents/web_document/wtp055910.pdf Developing an Effective Market for Open Access Article Processing Charges], March 2014, 69 pages. Final Report to a consortium of research funders comprising [[Jisc]], [[Research Libraries UK]], [[Research Councils UK]], the [[Wellcome Trust]], the [[Austrian Science Fund]], the [[Luxembourg National Research Fund]], and the [[Max Planck Institute for Gravitational Physics]].</ref> having broad scope and low selectivity, now called [[megajournal]]s, and a pay-to-publish model, usually published under [[Creative Commons licenses]].

==Reception==
In September 2009, ''PLOS ONE'' received the Publishing Innovation Award of the [[Association for Learned and Professional Society Publishers]].<ref>{{cite web|url=http://www.alpsp.org/ngen_public/default.asp?ID=251&groupid=192&groupname=About+ALPSP|title=ALPSP Awards 2010–finalists announced|work=ALPSP|accessdate=9 September 2010}}</ref> The award is given in recognition of a "truly innovative approach to any aspect of publication as adjudged from originality and innovative qualities, together with utility, benefit to the community and long-term prospects". In January 2010, it was announced that the journal would be included in the ''[[Journal Citation Reports]]'',<ref>{{cite web |work=PLOS Blogs |title=PLOS ONE indexed by Web of Science |url=http://www.plos.org/cms/node/506 |first=Mark |last=Patterson |accessdate=9 September 2010 |date=5 January 2010}}</ref> and the journal received an impact factor of 4.411 in 2010. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.057.<ref name=WoS>{{cite book |year=2016 |chapter=JOURNALNAME |title=2015 [[Journal Citation Reports]] |publisher=[[Clarivate Analytics]] |edition=Science |series=[[Web of Science]]}}</ref>

== Abstracting and indexing ==
The articles are indexed in:<ref name="PLOS ONE Journal Information" />
{{columns-list|colwidth=30em|
* [[AGRICOLA]]
* [[BIOSIS Previews]]
* [[Chemical Abstracts Service]]
* [[EMBASE]]
* [[Food Science and Technology Abstracts]]
* [[GeoRef]]
* [[MEDLINE]]/PubMed
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[The Zoological Record]]
}}

== Controversies ==

=== Sexist peer review ===
On April 29, 2015, Fiona Ingleby and Megan Head, postdoctoral fellows at the [[University of Sussex]] and [[Australian National University]] respectively, posted a rejection letter, which they said was sent to them by a peer reviewer for a journal they did not wish to name. The excerpt made negative comments about women's aptitude for science and advised Ingleby and Head to find male co-authors. Shortly afterward, the journal was reported to be ''PLOS ONE''. By May 1, ''PLOS'' announced that it was severing ties with the reviewer responsible for the comments and asking the editor who relayed them to step down. ''PLOS ONE'' director Damian Pattinson also stated that the journal was considering moving away from the tradition of anonymous peer review.<ref name="scimag">{{cite web|url=http://news.sciencemag.org/scientific-community/2015/04/sexist-peer-review-elicits-furious-twitter-response|title=PLOS ONE ousts reviewer, editor after sexist peer-review storm|first=Rachel|last=Bernstein|publisher=Science Magazine|date=2015-05-01|accessdate=2015-10-27}}</ref>

=== CreatorGate <!--[[CreatorGate]] redirects here -->===
<!-- DONT change section title to #CreatorGate; this will break some wikisyntax --> 
[[File:Creación de Adán (Miguel Ángel).jpg|thumb|''[[The Guardian]]'' and ''[[Washington Post]]'' illustrated #CreatorGate by [[Michelangelo]]'s ''[[The Creation of Adam]]'']]
On March 3, 2016, the editors of ''PLOS ONE'' initiated a reevaluation of an article about the functioning of the human hand<ref>{{cite journal
| last       = Liu
| first      = Ming-Jin
| last2      = Xiong
| first2     = Cai-Hua
| last3      = Xiong
| first3     = Le
| last4      = Huang
| first4     = Xiao-Lin
| date       = January 5, 2016
| title      = Biomechanical Characteristics of Hand Coordination in Grasping Activities of Daily Living
| url        = http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0146193
| journal    = PLOS ONE
| publisher  = 
| volume     = 11
| issue      = 1
| pages      = e0146193
| bibcode    = 
| doi        = 10.1371/journal.pone.0146193
| pmc        = 4701170
| pmid       = 26730579
| access-date= 2016-03-09
}}{{Retracted paper|intentional=yes}}</ref> due to outrage among the journal's readership over a reference to "Creator" inside the paper.<ref>{{cite web
 | url =https://www.theguardian.com/science/2016/mar/07/hand-of-god-scientific-plos-one-anatomy-paper-citing-a-creator-retracted-after-furore
 | title =Hand of God? Scientific anatomy paper citing a 'creator' retracted after furore
 | last =Davis
 | first =Nicola
 | date =7 March 2016
 | website =
 | publisher =[[The Guardian]]
 | access-date =2016-03-09}}</ref> The authors, who received grants from the Chinese [[National Basic Research Program]] and [[National Natural Science Foundation of China]] for this work, responded by saying "Creator" is a poorly-translated idiom ([[wikt:造化|造化]]([[wikt:者|者]]), literally "(that which) creates or transforms")<ref>{{cite web
|title=The hand of god
|first=Victor|last=Mair
|authorlink=Victor Mair
|url=http://languagelog.ldc.upenn.edu/nll/?p=24360
|website=[[Language Log]]
|date=4 March 2016|accessdate=10 March 2016}}</ref> which means "nature" in the Chinese language. Despite the authors' protests, the article was [[Retraction#Retraction in science|retracted]].<ref>{{cite journal
| last       = The ''PLOS ONE'' Staff
| first      = 
| date       = March 4, 2016
| title      = Retraction: Biomechanical Characteristics of Hand Coordination in Grasping Activities of Daily Living
| url        = http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0151685
| journal    = PLOS ONE
| publisher  = 
| volume     = 11
| issue      = 3
| pages      = e0151685
| bibcode    = 
| doi        = 10.1371/journal.pone.0151685
| pmc        = 4778690
| pmid       = 26943177
| access-date= 2016-03-15
}}</ref> "Creator" is found in the paper in three sentences:
* "The explicit functional link indicates that the biomechanical characteristic of tendinous connective architecture between muscles and articulations is the proper design by the Creator to perform a multitude of daily tasks in a comfortable way".
* "Hand coordination should indicate the mystery of the Creator’s invention".
* "In conclusion, our study can improve the understanding of the human hand and confirm that the mechanical architecture is the proper design by the Creator for dexterous performance of numerous functions following the evolutionary remodeling of the ancestral hand for millions of years".
A less sympathetic explanation for the use of "Creator" was suggested to ''[[The Chronicle of Higher Education]]'' by Chinese-language experts who noted that the academic editor listed on the paper, Renzhi Han, previously worked at the Chinese Evangelical Church in Iowa City.<ref>{{cite web
 | url =http://chronicle.com/article/Paper-Praising-Creator-/235610
 | title =Paper Praising ‘Creator’ Puts Fear of God in Open-Access Giant  
 | last =Basken
 | first =Paul
 | date =7 March 2016
 | website =
 | publisher =[[The Chronicle of Higher Education]]
 | access-date =2016-03-09}}</ref>

Sarah Kaplan of ''[[The Washington Post]]'' presented detailed analysis of the problem, which she named ''#CreatorGate'', and concluded that the journal’s hasty retraction may have been an even bigger offense than the publication of the paper in the first place.<ref>{{cite web
 | url =https://www.washingtonpost.com/news/morning-mix/wp/2016/03/08/creatorgate-how-a-study-on-hands-sparked-a-scandal-about-science-god-and-ethics-in-publishing/
 | title =#CreatorGate: How a study on hands sparked an uproar about science, God and ethics in publishing
 | last =Kaplan
 | first =Sarah
 | date =8 March 2016
 | website =
 | publisher =[[The Washington Post]]
 | access-date =2016-03-09}}</ref> To contrast ''PLOS ONE'''s handling of the problem, she used a 12-year history of retraction of [[MMR vaccine controversy|the fraudulent paper on vaccine and autism]] by ''[[The Lancet]]'' and no retraction of debunked study on "[[GFAJ-1#Criticism|arsenic life]]" by ''[[Science]]''.<ref>{{cite journal 
|last1=Wakefield 
|first1=AJ 
|authorlink=Andrew Wakefield
|last2=Murch
|first2=SH
|last3=Anthony
|first3=A
|last4=Linnell
|first4=J 
|last5=Casson
|first5=DM 
|last6=Malik
|first6=M 
|last7=Berelowitz
|first7=M 
|last8=Dhillon
|first8=AP 
|last9=Thomson
|first9=MA 
|last10=Harvey
|first10=P 
|last11=Valentine
|first11=A 
|last12=Davies
|first12=SE 
|last13=Walker-Smith
|first13=JA  
|title=Ileal-lymphoid-nodular hyperplasia, non-specific colitis, and pervasive developmental disorder in children 
|journal=[[The Lancet]]
|volume=351 
|issue=9103 
|pages=637–641 
|year=1998 
|doi=10.1016/S0140-6736(97)11096-0 
|pmid=9500320 
|url=http://www.thelancet.com/journals/lancet/article/PIIS0140-6736%2897%2911096-0/
|accessdate=2016-03-09}}{{Retracted paper|intentional=yes}}</ref><ref>{{cite journal 
|last1=Wolfe-Simon 
|first1=Felisa 
|authorlink=Felisa Wolfe-Simon
|last2=Blum 
|first2=Jodi Switzer 
|last3=Kulp 
|first3=Thomas R. 
|last4=Gordon 
|first4=Gwyneth W. 
|last5=Hoeft 
|first5=Shelley E. 
|last6=Pett-Ridge 
|first6=Jennifer 
|last7=Stolz 
|first7=John F. 
|last8=Webb 
|first8=Samuel M. 
|last9=Weber 
|first9=Peter K. 
|last10=Davies 
|first10=P. C. W. 
|last11=Anbar 
|first11=A. D. 
|last12=Oremland 
|first12=R. S. 
|title=A bacterium that can grow by using arsenic instead of phosphorus 
|journal=[[Science (journal)|Science]] 
|date=2 December 2010 
|doi=10.1126/science.1197258 
|pmid=21127214 
|url=http://www.sciencemag.org/content/332/6034/1163.full 
|accessdate=2016-03-09 
|volume=332 
|issue=6034 
|pages=1163–1166 
|display-authors=}}</ref> Others added the history of the article in ''[[Nature (journal)|Nature]]'' on "[[water memory]]" that was not retracted either.<ref>{{cite journal 
|last1=Cressey
|first1=Daniel
|title=Paper that says human hand was 'designed by Creator' sparks concern. Apparently creationist research prompts soul searching over process of editing and peer review
|journal=[[Nature (journal)|Nature]]
|volume=531 
|issue=7593
|pages=143 
|date=10 March 2016 
|doi=10.1038/531143f
|pmid= 
|url=http://www.nature.com/polopoly_fs/1.19499!/menu/main/topColumns/topLeftColumn/pdf/531143f.pdf
|accessdate=2016-03-10}}</ref>

[[Jonathan Eisen]], chair of advisory board of a sister journal ''[[PLOS Biology]]'' and an advocate for [[open-access journal|open-access]], defended ''PLOS ONE'' for prompt response to [[social media]], which in his words "most journals pretend doesn’t even exist".<ref>{{cite web
 | url =https://www.wired.com/2016/03/science-journal-invokes-creator-science-pushes-back/
 | title =A Science Journal Invokes 'the Creator,' and Science Pushes Back
 | last =Kotack
 | first =Madison
 | date =3 March 2016
 | website =
 | publisher =[[Wired (magazine)|Wired]]
 | access-date =2016-03-09}}</ref> David Knutson issued a statement about the paper processing at ''PLOS ONE'', which praised the importance of post-publication peer review and described their intention to offer open signed reviews in order to ensure accountability of the process.<ref>{{cite web
 | url =https://forbetterscience.wordpress.com/2016/03/04/hand-of-god-paper-retracted-plos-one-could-not-stand-by-the-pre-publication-assessment/
 | title =Hand of God paper retracted: PLOS ONE "could not stand by the pre-publication assessment"
 | last =Schneider
 | first =Leonid
 | date =4 March 2016
 | website =
 | publisher =For Better Science
 | access-date =2016-03-09}}</ref> From March 2 to 9, the research article received total 67 post-publication reader comments and 129 responses on ''PLOS ONE'' site, the first one from cell biologist turned science journalist Leonid Schneider who cited a [[Religious views on masturbation|Wikipedia article]] rather than scientific literature as an authority.<ref>{{cite journal
 | url =http://journals.plos.org/plosone/article/comments?id=10.1371/journal.pone.0146193
 | title =Reader Comments on Biomechanical Characteristics of Hand Coordination in Grasping Activities of Daily Living
 | date =9 March 2016
 | website =PLOS ONE
 | access-date =2016-03-09
 | doi=10.1371/journal.pone.0146193
 | pmid=26730579
 | volume=11
 | issue=1
 | pmc=4701170
 | page=e0146193 | last1 = Liu | first1 = MJ | last2 = Xiong | first2 = CH | last3 = Xiong | first3 = L | last4 = Huang | first4 = XL}}</ref><ref>{{cite journal
 | url =http://journals.plos.org/plosone/article/comment?id=info%3Adoi%2F10.1371%2Fannotation%2Ff71531b7-212f-4d9b-bd1c-154268917973
 | title =Proper design by the Creator?
 | last =Schneider
 | first =Leonid
 | date =2 March 2016
 | website =PLOS ONE
 | access-date =2016-03-09}}</ref> Signe Dean of [[Special Broadcasting Service|SBS]] put #CreatorGate in perspective: it is not the most scandalous retraction in science, yet it shows how a social media outrage storm does expedite a retraction.<ref>{{cite web
 | url =http://www.sbs.com.au/topics/science/fundamentals/article/2016/03/07/not-just-creatorgate-most-scandalous-retractions-science
 | title =Not just #creatorgate: Most scandalous retractions in science
 | last =Dean
 | first =Signe
 | date =7 March 2016
 | website =
 | publisher =[[Special Broadcasting Service|SBS]]
 | access-date =2016-03-09}}</ref>

The dissemination activity on social media within one week of publicity was: 
* 1,309 tweets which share the article on [[Twitter]]; 
* 7,432 posts, 1,516 shares and 4,570 Likes on [[Facebook]].
The article was viewed 169,926 times on ''PLOS'' site in the first ten days of March, compared to 555 views in January and 116 views in February.<ref>{{cite journal
 | url =http://journals.plos.org/plosone/article/metrics?id=10.1371%2Fjournal.pone.0146193
 | title =Metrics of Biomechanical Characteristics of Hand Coordination in Grasping Activities of Daily Living
 | date =10 March 2016
 | website =PLOS ONE
 | access-date =2016-03-10
 | doi=10.1371/journal.pone.0146193
 | pmid=26730579
 | volume=11
 | issue=1
 | pmc=4701170
 | page=e0146193 | last1 = Liu | first1 = MJ | last2 = Xiong | first2 = CH | last3 = Xiong | first3 = L | last4 = Huang | first4 = XL}}</ref>

On March 10, 2016, ''[[BioLogos]]'', a website of a Christian advocacy group established by the 16th Director of the [[National Institutes of Health]] [[Francis Collins]] after publication of his book ''[[The Language of God: A Scientist Presents Evidence for Belief]]'', started Blog series ''Faith and Science Seeking Understanding'' to review the controversy raised by #CreatorGate.<ref>{{cite web
 | url =http://biologos.org/blogs/kathryn-applegate-endless-forms-most-beautiful/series/reviewing-creatorgate
 | title =Faith and Science Seeking Understanding: Reviewing #Creatorgate - Blog Series
 | last =
 | first =
 | date =10 March 2016
 | website =[[BioLogos]]
 | publisher =
 | access-date =2016-03-12}}</ref> The series follows the accusations of anti-Christian/anti-”design” bias in the scientific world by [[Ken Ham]] of ''[[Answers in Genesis]]'' and [[David Klinghoffer]] of ''[[Evolution News and Views]]''.<ref>{{cite web
 | url =https://answersingenesis.org/who-is-god/creator-god/secularist-intolerance-against-scientific-paper-briefly-mentions-creator/
 | title =Secularist Intolerance Against Scientific Paper That Briefly Mentions Creator
 | last =Ham
 | first =Ken
 | authorlink=Ken Ham
 | date =6 March 2016
 | website =[[Answers in Genesis]]
 | publisher =
 | access-date =2016-03-12}}</ref><ref>{{cite web
 | url =http://www.evolutionnews.org/2016/03/plos_one_creato102667.html
 | title =PLOS ONE "Creator" Scandal Enters Witch-hunt Territory
 | last =Klinghoffer
 | first =David
 | authorlink=David Klinghoffer
 | date =7 March 2016
 | website =Evolution News and Views
 | publisher =[[Discovery Institute]]
 | access-date =2016-03-12}}</ref> ''BioLogos'' authors argue that avoiding mentions of God in scientific literature is not a [[censorship]] but a rule of a successful game akin to the rules of [[soccer]] or [[American football|football]].<ref>{{cite web
 | url =http://biologos.org/blogs/kathryn-applegate-endless-forms-most-beautiful/reviewing-creatorgate-why-a-scientist-shouldnt-use-the-word-creator-in-their-articles
 | title =Reviewing #Creatorgate: Why a scientist shouldn't use the word "Creator" in their articles
 | last =Applegate
 | first =Kathryn
 | date =10 March 2016
 | website =[[BioLogos]]
 | publisher =
 | access-date =2016-03-12}}</ref><ref>{{cite web
 | url =http://biologos.org/blogs/jim-stump-faith-and-science-seeking-understanding/reviewing-creatorgate-how-science-is-like-soccer
 | title =Reviewing #Creatorgate: How Science is Like Soccer
 | last =Stump
 | first =Jim
 | date =10 March 2016
 | website =[[BioLogos]]
 | publisher =
 | access-date =2016-03-12}}</ref>

== References ==
{{Reflist|30em}}

== External links ==
{{Commons category|Media from PLOS ONE|PLOS ONE}}
* {{Official website|http://www.plosone.org/}}

{{PLoS|state=collapsed}}

[[Category:Creative Commons Attribution-licensed journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2006]]
[[Category:Multidisciplinary scientific journals]]
[[Category:English-language journals]]
[[Category:PLoS academic journals]]
[[Category:Continuous journals]]