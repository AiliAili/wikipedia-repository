{{good article}}
{{Infobox album <!--See Wikipedia:WikiProject_Albums -->
| Name        = Barbra Streisand's Greatest Hits Volume 2
| Type        = [[Greatest hits]]
| Artist      = [[Barbra Streisand]]
| Cover      = Streisandhits2.jpg
| Border      = yes
| Alt         = Close-up image of Barbra Streisand facing to the left in black and white.
| Released    = November 15, 1978
| Recorded    = July 1970–February 1978
| Genre       = {{hlist|[[Pop music|Pop]]|[[soft rock]]|[[Traditional pop music|traditional pop]]}}
| Length      = 39:35
| Label       = [[Columbia Records|Columbia]]
| Producer    = {{hlist|[[Charles Koppelman]] <small>(executive)</small>|Barbra Streisand|[[Phil Ramone]]||[[Gary Klein (producer)|Gary Klein]]|[[Charles Calello|Charlie Calello]]|[[Bob Gaudio]]|[[Marty Paich]]|[[Richard Perry]]|[[Tommy LiPuma]]}}
| Last album  = ''[[Songbird (Barbra Streisand album)|Songbird]]''<br />(1978)
| This album  = '''''Barbra Streisand's Greatest Hits Volume 2'''''<br />(1978)
| Next album  = ''[[The Main Event (1979 film)#Soundtrack|The Main Event]]''<br />(1979)
| Misc = {{Singles
 | Name = Barbra Streisand's Greatest Hits Volume 2
 | Type = Greatest hits
 | Single 1 = [[You Don't Bring Me Flowers]]
 | Single 1 date = {{Start date|1978|10|07|mf=y}}
}}}}

'''''Barbra Streisand's Greatest Hits Volume 2''''' is the second [[greatest hits album]] recorded by American vocalist [[Barbra Streisand]]. It was released on November 15, 1978 by [[Columbia Records]]. The album is a compilation consisting of ten commercially successful singles from the singer's releases in the 1970s, with a majority of them being [[Cover version|cover songs]]. It also features a new version of "[[You Don't Bring Me Flowers]]", which was released as the collection's only single on October 7, 1978. Originating on Streisand's previous album, ''[[Songbird (Barbra Streisand album)|Songbird]]'', the new rendition is a duet with [[Neil Diamond]] who had also recorded the song for his [[You Don't Bring Me Flowers (album)|1978 album of the same name]]. The idea for the duet originated from DJ Gary Guthrie who sold the idea to the record label for $5 million. 

Critically appreciated, ''Barbra Streisand's Greatest Hits Volume 2'' received a perfect five star rating from both [[AllMusic]] and ''[[Rolling Stone]]''. It was also a commercial success, topping the charts in Canada, New Zealand, the United Kingdom and the United States, and peaking at number two in Australia. The album later received certifications in a total of six countries, including in Australia, Canada and the United States. In the latter country, it was certified 5x Platinum and sold over 5 million copies according to the [[Recording Industry Association of America]].

== Promotion and development ==
In May 1978, Streisand released her twentieth studio album ''[[Songbird (Barbra Streisand album)|Songbird]]'' that featured the song "[[You Don't Bring Me Flowers]]".<ref name="Songbird liner notes">{{cite AV media notes |title=[[Songbird (Barbra Streisand album)|Songbird]] |edition=Vinyl release |others=Barbra Streisand |year=1978 |type=Liner notes |publisher=[[Columbia Records|Columbia]] |id= JC 35375}}</ref> Despite not being released as a physical or commercial single from ''Songbird'', it was distributed in a [[Gramophone record|7" record]] format on October 7, 1978.<ref name="New Zealand single">{{cite web|title=Charts.org.nz – Barbra Streisand & Neil Diamond – You Don't Bring Me Flowers (Song)|url=http://www.charts.org.nz/showitem.asp?interpret=Barbra+Streisand+%26+Neil+Diamond&titel=You+Don%27t+Bring+Me+Flowers&cat=s|publisher=Hung Medien|accessdate=March 10, 2017|date=October 7, 1978}}</ref> However, the version that appears on ''Barbra Streisand's Greatest Hits Volume 2'' is a duet with American singer [[Neil Diamond]], who also contributed to the song's lyrics.<ref name="You Don't Bring Me Flowers liner notes">{{cite AV media notes |title=''"[[You Don't Bring Me Flowers]]"'' |edition=Vinyl release |others=Barbra Streisand and [[Neil Diamond]] |year=1978 |type=Liner notes |publisher=Columbia |id= 3-10840}}</ref> As Streisand released ''Songbird'', Diamond recorded a version of "You Don't Bring Me Flowers" originally meant for his [[You Don't Bring Me Flowers (album)|1978 album of the same name]]. Because both versions of the single were recorded in the same [[Key (music)|key]], American DJ Gary Guthrie combined the two songs together while playing records at a local radio station in [[Louisville, Kentucky]]. Guthrie pitched the idea to [[CBS Records International]] (the international arm of Columbia) for a $5 million contract, to which they eventually accepted, despite CBS breaching the contract initially.<ref name="The Super Seventies review">{{cite news|title="You Don't Bring Me Flowers" – Barbra Streisand & Neil Diamond|url=http://www.superseventies.com/sw_youdontbringmeflowers.html|accessdate=March 10, 2017|publisher=[[MTV Classic (U.S. TV network)|The Super Seventies]]}}</ref> Their collaboration was a global, commercial success, topping the charts in both the United States and Canada.<ref name="Billboard Hot 100 December 2, 1978">{{cite web|title=The Hot 100 – The Week Of December 2, 1978|url=http://www.billboard.com/charts/hot-100/1978-12-02|work=Billboard |accessdate=November 14, 2016 |date=December 2, 1978}}</ref><ref name="Canada single week">{{cite web|title=Top RPM Singles: Issue 0056a|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.0056a&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.0056a.gif&Ecopy=nlc008388.0056a|publisher=''[[RPM (magazine)|RPM]]''. [[Library and Archives Canada]]|accessdate=March 10, 2017}}</ref> The version with Diamond has since sold more than 2 million copies in the United States.<ref name="United States certification" />

A sequel to her first greatest hits album, ''[[Barbra Streisand's Greatest Hits]]'' (1970), the second volume contains ten singles released during Streisand's second decade in the recording industry, ranging from "[[Stoney End (song)#Barbra Streisand version|Stoney End]]" (1970) to "You Don't Bring Me Flowers" (1978). Unlike the former release, ''Barbra Streisand's Greatest Hits Volume 2'' did not feature any new material (referring to the other's inclusion of "Sam, You Made the Pants Too Long" in 1970).<ref name="Volume 1 liner notes">{{cite AV media notes |title=[[Barbra Streisand's Greatest Hits]] |edition=Vinyl release |others=Barbra Streisand |year=1970 |type=Liner notes |publisher=Columbia |id= CK-9968}}</ref> The songs featured on the record were recorded between July 1970 and February 1978.<ref name="Songbird liner notes" /><ref name="Stoney End liner notes">{{cite AV media notes |title=[[Stoney End (Barbra Streisand album)|Stoney End]] |edition=Vinyl release |others=Barbra Streisand |year=1971 |type=Liner notes |publisher=Columbia |id= KC 30378}}</ref> Overall, it features a total of three number-one hits ("[[The Way We Were (song)|The Way We Were]]", "[[Evergreen (Love Theme from A Star Is Born)|Evergreen]]", and "You Don't Bring Me Flowers"), two top-ten singles ("Stoney End" and "[[My Heart Belongs to Me]]"), and three [[Top 40]] songs ("Sweet Inspiration" / "[[Where You Lead]]", "[[Songbird (Barbra Streisand song)|Songbird]]", and "Prisoner").<ref name="allmusicreview" /> "[[All in Love Is Fair#Barbra Streisand version|All in Love Is Fair]]" and "Superman" are the two other songs on the track listing.<ref name="Barbra Streisand's Greatest Hits Volume 2 2 liner notes" /> [[Columbia Records]] released the compilation on November 15, 1978.<ref name="Official liner notes" /> The label also issued an [[8-track tape|8-track cartridge]] version of the album in 1978, with a differing track listing; single "You Don't Bring Me Flowers" was split into two separate parts increasing the amount of tracks on the record from ten to eleven.<ref name="Barbra Streisand's Greatest Hits Volume 2 2 liner notes" /> In 1987, the album was released in a compact disc format.<ref name="Amazon CD">{{cite web|title=Barbra Streisand's Greatest Hits, Vol. 2: Barbra Streisand|url=https://www.amazon.com/Barbra-Streisands-Greatest-Hits-Vol/dp/B0000025EV|publisher=[[Amazon.com]]|accessdate=March 9, 2017|date=July 6, 1987}}</ref>

== Critical reception ==
{{Album ratings
| rev1 = [[AllMusic]]
| rev1Score = {{rating|5|5}}<ref name="allmusicreview">{{cite web|url=http://www.allmusic.com/album/barbra-streisands-greatest-hits-vol-2-mw0000195912|title=Barbra Streisand – ''Barbra Streisand's Greatest Hits, Vol. 2'' |publisher=[[AllMusic]]|last1= Ruhlmann|first1= William|accessdate=March 9, 2017}}</ref>
| rev2 = ''[[Rolling Stone]]''
| rev2Score = {{rating|5|5}}<ref name="Rolling Stone review">{{harvnb|Marsh|Swenson|1983|p=494}}</ref>
}}
''Barbra Streisand's Greatest Hits Volume 2'' was critically acclaimed by [[Music journalism|music critics]]. It was given a perfect five out of five stars rating by [[AllMusic]]'s William Ruhlmann, who called it a "genre-defining album [...] that drew upon the rock revolution to redefine classic pop for a new generation". He also gave praise towards the album for successfully capturing the best of her "contemporary [[Soft rock|soft-rock]] [and] highly successful" singles from her "largely inconsistent" albums. Additionally, Ruhlmann claimed that the success of the record stemmed from the fact that her singles in the 1970s were more "precious" and not always "show music material", contrasting to her songs in the 1960s.<ref name="allmusicreview" /> As part of ''[[Rolling Stone]]''{{'}}s ''The New Rolling Stone Record Guide'', released in 1983, they rated the collection a perfect five stars. Streisand's first volume from 1970 and ''Guilty'' from 1980 also achieved the same status.<ref name="Rolling Stone review" />

== Commercial performance ==
The compilation album was a success, topping the charts in four countries. In the United States, ''Barbra Streisand's Greatest Hits Volume 2'' debuted at number seven on the [[Billboard 200|''Billboard'' 200]] chart for the week ending December 2, 1978 (also serving as the week's highest new entry).<ref name="Billboard 200 December 2, 1978">{{cite web |url=http://www.billboard.com/charts/billboard-200/1978-12-02 |title=Billboard 200: The Week Of December 2, 1978 |work=Billboard |accessdate=March 10, 2017|date= December 2, 1978}}</ref> The following week it rose to number three and on January 6, 1979, it topped the chart.<ref name="Billboard 200 December 9, 1978">{{cite web |url=http://www.billboard.com/charts/billboard-200/1978-12-09 |title=Billboard 200: The Week Of December 9, 1978 |work=Billboard |accessdate=March 10, 2017|date= December 9, 1978}}</ref><ref name="Billboard 200 January 6, 1979">{{cite web |url=http://www.billboard.com/charts/billboard-200/1979-01-06 |title=Billboard 200: The Week Of January 6, 1979 |work=Billboard |accessdate=March 10, 2017|date= January 6, 1979}}</ref> The record spent a total of 46 weeks on the ''Billboard'' 200,<ref name="United States" /> and by December 1984, the album and Streisand's ''[[Guilty (Barbra Streisand album)|Guilty]]'' (1980) had both sold over 4 million physical copies in the United States, becoming quadruple certified by the [[Recording Industry Association of America|RIAA]].<ref name="4x Platinum RIAA">{{cite journal|author1=''Billboard'' staff|title=RIAA Multi-Platinum Awards|journal=[[Billboard (magazine)|Billboard]]|date=December 15, 1984|volume=96|issue=50|page=79|url=https://books.google.com/books?id=RiQEAAAAMBAJ|accessdate=March 9, 2017|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> It would later be re-certified to 5x Platinum on October 28, 1994.<ref name="United States certification" /> It was the one of the United States' best-selling albums in 1979, coming in at number 28 on ''Billboard''{{'}}s annual year-end chart.<ref name="Year-end United States" /> ''[[Billboard (magazine)|Billboard]]''{{'}}s Fred Bronson wrote in ''The Billboard Book of Number One Hits'' that the commercial and critical achievements of "You Don't Bring Me Flowers" is what made ''Barbra Streisand's Greatest Hits Volume 2'' a certified Platinum album in the US.<ref name="Billboard Book of Number One Hits">{{harvnb|Bronson|1988|p=291}}</ref>

On Canada's chart, compiled by ''[[RPM (magazine)|RPM]]'', it debuted at number 60 on the week ending December 9, 1978.<ref name="Canada debut">{{cite web|title=Top RPM Albums: Issue 0055a|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.0055a&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.0055a.gif&Ecopy=nlc008388.0055a|publisher=''RPM''. Library and Archives Canada|accessdate=March 10, 2017}}</ref> Four weeks later, it would top the chart on January 13, 1979.<ref name="Canada" /> Overall, it spent a total of 20 weeks charting in Canada<ref name="Canada finale">{{cite web|title=Top RPM Albums: Issue 4742b|url=http://www.bac-lac.gc.ca/eng/discover/films-videos-sound-recordings/rpm/Pages/image.aspx?Image=nlc008388.4742b&URLjpg=http%3a%2f%2fwww.collectionscanada.gc.ca%2fobj%2f028020%2ff4%2fnlc008388.4742b.gif&Ecopy=nlc008388.4742b|publisher=''RPM''. Library and Archives Canada|accessdate=March 10, 2017}}</ref> and later received a triple platinum certification from [[Music Canada]] on March 1, 1979.<ref name="Canada certification" /> It was also Streisand's first chart-topping album in the United Kingdom, where it spent four consecutive weeks at the highest ranking position and later was certified Platinum for sales upwards of 300,000 copies.<ref name="United Kingdom" /><ref name="United Kingdom certification" />

In New Zealand, the album debuted at number five on January 28, 1979, becoming the chart's highest new entry.<ref name="New Zealand debut">{{cite web|title=Charts.org.nz – Album Top 40 – 28/01/1979|url=http://www.charts.org.nz/weekchart.asp?year=1979&date=19790128&cat=a|publisher=Hung Medien|accessdate=March 9, 2017|date=January 28, 1979}}</ref> The following week, it reached it topped the chart and did so for four consecutive weeks; overall, it spent a total of 19 weeks charting in that country.<ref name="New Zealand" /> It also charted in Australia, where it peaked at number two according to the [[Kent Music Report]]. The [[Australian Recording Industry Association]] certified ''Barbra Streisand's Greatest Hits Volume 2'' double Platinum in 2000, signifying sales upwards of 140,000.<ref name="kent" /> Although the compilation did not chart in Hong Kong, the [[International Federation of the Phonographic Industry]] certified the album Platinum for sales of 20,000 copies in 1982.<ref name="Hong Kong certification" />

== Track listing ==
{{Track listing
| headline       = ''Barbra Streisand's Greatest Hits Volume 2''{{spaced ndash}} {{nobold|Standard edition}}<ref name="Official liner notes">{{cite AV media notes |title=Barbra Streisand's Greatest Hits Volume 2 |edition=CD release |others=Barbra Streisand |year=1984 |type=Liner notes |publisher=[[CBS Records International|CBS]], [[Sony Music|Sony Music Entertainment]] |id= 35DP 161}}</ref>
| total_length       = 39:35
| extra_column       = Producer(s)

| title1         = [[Evergreen (Love Theme from A Star Is Born)|Evergreen]]
| note1         = from ''[[A Star Is Born (soundtrack)|A Star Is Born]]'', 1976
| writer1        = {{hlist|Barbra Streisand|[[Paul Williams (songwriter)|Paul Williams]]}}
| length1        = 3:09
| extra1        = {{hlist|Streisand|[[Phil Ramone]]}}

| title2         = Prisoner
| note2         = from ''[[Eyes of Laura Mars#Soundtrack|Eyes of Laura Mars]]'', 1978
| writer2        = {{hlist|[[Karen Lawrence (singer-songwriter)|Karen Lawrence]]|John Desautels}}
| length2        = 3:57
| extra2         = {{hlist|[[Charles Koppelman]]|[[Gary Klein (producer)|Gary Klein]]}}

| title3         = [[My Heart Belongs to Me]]
| note3         = from ''[[Superman (Barbra Streisand album)|Superman]]'', 1977
| writer3        = [[Alan Gordon (songwriter)|Alan Gordon]]
| length3        = 3:24
| extra3         = {{hlist|Klein|[[Charles Calello|Charlie Calello]]}}

| title4         = [[Songbird (Barbra Streisand song)|Songbird]]
| note4         = from ''[[Songbird (Barbra Streisand album)|Songbird]]'', 1978
| writer4        = {{hlist|Dave Wolfert|Stephen Nelson}}
| length4        = 3:48
| extra4         = {{hlist|Koppelman|Klein}}

| title5         = [[You Don't Bring Me Flowers]]
| note5         = with [[Neil Diamond]]
| writer5        = {{hlist|[[Alan and Marilyn Bergman|Alan Bergman]]|[[Alan and Marilyn Bergman|Marilyn Bergman]]|Diamond}}
| length5        = 3:26
| extra5         = [[Bob Gaudio]]

| title6         = [[The Way We Were (song)|The Way We Were]]
| note6         = from ''[[The Way We Were (Barbra Streisand album)|The Way We Were]]'', 1974
| writer6        = {{hlist|A. Bergman|M. Bergman|[[Marvin Hamlisch]]}}
| length6        = 3:30
| extra6        = [[Marty Paich]]

| title7         = Sweet Inspiration" / "[[Where You Lead]]
| note7         = from ''[[Live Concert at the Forum]]'', 1972
| writer7        = {{hlist|[[Dan Penn]]|[[Spooner Oldham]]|[[Carole King]]|Toni Stern}}
| length7        = 6:20
| extra7         = [[Richard Perry]]

| title8         = [[All in Love Is Fair#Barbra Streisand version|All in Love Is Fair]]
| note8         = from ''The Way We Were''
| writer8        = [[Stevie Wonder]]
| length8        = 3:52
| extra8        = [[Tommy LiPuma]]

| title9         = Superman
| note9         = from ''Superman''
| writer9        = Richie Snyder
| length9        = 2:50
| extra9        = Klein

| title10         = [[Stoney End (song)|Stoney End]]
| note10         = from ''[[Stoney End (Barbra Streisand album)|Stoney End]]'', 1971
| writer10        = [[Laura Nyro]]
| length10        = 2:58
| extra10        = Perry
}}

{{Track listing
| collapsed = yes
| headline = ''Barbra Streisand's Greatest Hits Volume 2''{{spaced ndash}} {{nobold|8-track cartridge edition}}<ref name="Barbra Streisand's Greatest Hits Volume 2 2 liner notes">{{cite AV media notes |title=Barbra Streisand's Greatest Hits Volume 2 | edition=8-track cartridge |others=Barbra Streisand |year=1978 |type=Liner notes |publisher=Columbia |id=FCA 35679}}</ref>
| total_length = 39:42

| title1         = Evergreen
| length1        = 3:09

| title2         = My Heart Belongs to Me
| length2        = 3:24

| title3         = Stoney End
| length3        = 2:58

| title4         = Prisoner
| length4        = 3:57

| title5         = All in Love Is Fair
| length5        = 3:52

| title6         = You Don't Bring Me Flowers
| note6         = Beginning) (with Neil Diamond
| length6        = 2:02

| title7         = You Don't Bring Me Flowers
| note7         = Conclusion) (with Neil Diamond
| length7        = 1:31

| title8         = The Way We Were
| length8        = 3:30

| title9         = Songbird
| length9        = 3:48

| title10         = Sweet Inspiration" / "Where You Lead
| length10        = 6:20

| title11         = Superman
| length11        = 2:50
}}

== Personnel ==
Credits adapted from the liner notes of the CD edition of ''Barbra Streisand's Greatest Hits Volume 2''.<ref name="Official liner notes" />

{{col-begin}}
{{col-2}}
* Barbra Streisand{{spaced ndash}} vocals, production {{small|(track 1)}}, songwriter {{small|(track 1)}}
* Alan Bergman{{spaced ndash}} songwriter {{small|(tracks 5, 6)}}
* Marilyn Bergman{{spaced ndash}} songwriter {{small|(tracks 5, 6)}}
* Charlie Calello{{spaced ndash}} production, arrangements {{small|(track 3)}}
* Larry Carlton{{spaced ndash}} rhythm arrangements {{small|(tracks 4, 9)}}
* Nick de Caro{{spaced ndash}} arrangements {{small|(track 8)}}, orchestra arrangements {{small|(track 4)}}, string and horn arrangements {{small|(track 9)}}
* John Desautels{{spaced ndash}} songwriter {{small|(track 2)}}
* Neil Diamond{{spaced ndash}} composition {{small|(track 5)}}, songwriter {{small|(track 5)}}
* Bob Gaudio{{spaced ndash}} production {{small|(track 5)}}
* Alan Gordon{{spaced ndash}} songwriter {{small|(track 3)}}
* Marvin Hamlisch{{spaced ndash}} composition {{small|(track 6)}}, songwriter {{small|(track 6)}}
* Don Hannah{{spaced ndash}} arrangements {{small|(track 7)}}
* Carole King{{spaced ndash}} composition {{small|(track 7)}}, songwriter {{small|(track 7)}}
* Gary Klein{{spaced ndash}} production {{small|(tracks 2, 3, 4, 9)}}
* Charles Koppelman{{spaced ndash}} executive production {{small|(tracks 2, 4)}}
* Karen Lawrence{{spaced ndash}} songwriter {{small|(track 2)}}
{{col-2}}
* Alan Lindgren{{spaced ndash}} arrangements {{small|(track 5)}}
* Tommy LiPuma{{spaced ndash}} production {{small|(track 8)}}
* Stephen Nelson{{spaced ndash}} songwriter {{small|(track 4)}}
* Laura Nyro{{spaced ndash}} songwriter {{small|(track 10)}}
* Spooner Oldham{{spaced ndash}} songwriter {{small|(track 7)}}
* Gene Page{{spaced ndash}} arrangements {{small|(track 10)}}
* Marty Paich{{spaced ndash}} production, arranging {{small|(track 6)}}
* Dan Penn{{spaced ndash}} songwriter {{small|(track 7)}}
* Richard Perry{{spaced ndash}} production {{small|(tracks 7, 10)}}
* Phil Ramone{{spaced ndash}} production {{small|(track 1)}}
* Richie Snyder{{spaced ndash}} songwriter {{small|(track 9)}}
* Toni Stern{{spaced ndash}} songwriter {{small|(track 7)}}
* Paul Williams{{spaced ndash}} songwriter {{small|(track 1)}}
* Dave Wolfert{{spaced ndash}} songwriter {{small|(track 4)}}
* Stevie Wonder{{spaced ndash}} songwriter {{small|(track 8)}}
{{col-end}}

== Charts ==
{{col-begin}}
{{col-2}}

=== Weekly charts ===
{| class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (1978–1979)
! scope="col"| Peak<br /> position
|-
! scope="row"| Australia Albums ([[Kent Music Report]])<ref name="kent">{{cite book|title=[[Kent Music Report|Australian Chart Book 1970–1992]]|last=Kent|first=David|authorlink=David Kent (historian)|publisher=Australian Chart Book, St Ives, N.S.W |year=1993|isbn=0-646-11917-6}}</ref>
| 2
|-
{{albumchart|Canada|1|chartid=0076a|rowheader=true|accessdate=March 8, 2017|refname="Canada"}}
|-
{{albumchart|New Zealand|1|artist=Barbra Streisand|album=Greatest Hits Vol. 2|rowheader=true|accessdate=March 8, 2017|refname="New Zealand"}}
|-
{{albumchart|UK|1|artist=Barbra Streisand|rowheader=true|accessdate=March 8, 2017|refname="United Kingdom"}}
|-
{{albumchart|Billboard200|1|artist=Barbra Streisand|rowheader=true|accessdate=March 8, 2017|refname="United States"}}
|-
|}
{{col-2}}

=== Year-end charts ===
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
! scope="col"| Chart (1979)
! scope="col"| Position
|-
! scope="row"| Australia Albums (Kent Music Report)<ref name="kent" />
| 14
|-
! scope="row"| Canada Top Albums/CDs (''RPM'')<ref name="Canada year-end">{{cite web|title=Top Albums/CDs - Volume 32, No. 13, December 22 1979|url=http://www.collectionscanada.gc.ca/rpm/028020-119.01-e.php?brws_s=1&file_num=nlc008388.6920&type=1&interval=24&PHPSESSID=mhe12pta2k83e08udtq66ot062|publisher=''RPM''. Library and Archives Canada|accessdate=March 11, 2017}}</ref>
| 33
|-
!scope="row"| UK Albums (OCC)<ref name="UKYearend">{{Cite web|url=http://uktop40.republika.pl/najlep%20sprzalbumy%20uk%201979.html|title=Najlepiej sprzedające się albumy w W.Brytanii w 1979r|language=Polish|accessdate=November 27, 2010|archiveurl=https://web.archive.org/web/20110831204012/http://uktop40.republika.pl/najlep%20sprzalbumy%20uk%201979.html|archivedate=August 31, 2011|df=mdy}}</ref>
| 6
|-
!scope="row"| US ''Billboard'' 200<ref name="Year-end United States">{{cite journal|author1=''Billboard'' staff|title=1: Top Albums of the Year|journal=Billboard|date=December 22, 1979|volume=91|issue=51|page=12|url=https://books.google.com/books?id=CSQEAAAAMBAJ|accessdate=March 8, 2017|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref>
| 28
|-
|}
{{col-end}}

== Certifications ==
{{Certification Table Top}}
{{Certification Table Entry|region=Australia|type=album|artist=Barbra Streisand|title=Greatest Hits Volume 2|award=Platinum|number=2|relyear=1978|certyear=2000|accessdate=March 9, 2017 |refname="Australia certification"}}
{{Certification Table Entry|region=Canada|type=album|artist=Barbra Streisand|title=Greatest Hits, Vol. II|award=Platinum|number=3|relyear=1978|certyear=1979|accessdate=March 9, 2017 |refname="Canada certification"}}
{{Certification Table Entry|region=Hong Kong|type=album|artist=Barbra Streisand|title=Greatest Hits|relyear=1979|award=Platinum|certyear=1982|refname="Hong Kong certification"}}
{{Certification Table Entry|region=New Zealand|type=album|title=Barbra Streisand's Greatest Hits Vol. 2|artist=Barbra Streisand|award=Platinum|relyear=1979 |certyear=1979|certref=<ref>{{cite web|url=http://nztop40.co.nz/chart/albums?chart=2845|title=NZ Top 40 Albums Chart|accessdate=March 10, 2017|publisher =[[Recorded Music NZ]]|archivedate=March 1, 2016|archiveurl=https://web.archive.org/web/20160301202049/http://nztop40.co.nz/chart/albums?chart=2845}}</ref> |refname="New Zealand certification"}}
{{Certification Table Entry|region=United Kingdom|type=album|artist=Barbra Streisand|title=Barbra Streisand's Hits Vol 2|award=Platinum|relyear=1979|certyear=1979|accessdate=March 9, 2017|refname="United Kingdom certification"}}
{{Certification Table Entry|region=United States|type=album|artist=Barbra Streisand|title=Greatest Hits Volume II|award=Platinum|number=5|relyear=1978|certyear=1994|accessdate=March 9, 2017|refname="United States certification"}}
{{Certification Table Bottom|nounspecified=yes}}

== See also ==
* [[List of Billboard 200 number-one albums of 1979]]
* [[List of UK Albums Chart number ones of the 1970s]]

== References ==
=== Footnotes ===
{{Reflist|30em}}

=== Sources ===
*{{cite book|last1=Bronson|first1=Fred|title=The Billboard Book of Number One Hits|date=1988|publisher=Nielsen Business Media, LLC. |ref=harv|isbn= 0-8230-7677-6 }}
*{{cite book|last1=Marsh|first1=Dave|last2=Swenson|first2=John|title=The New Rolling Stone Record Guide|date=October 12, 1983|publisher=Random House, Rolling Stone Press |ref=harv|isbn= 0-394-72107-1 }}

== External links ==
* {{Discogs master|74241|Barbra Streisand's Greatest Hits Volume 2|type=album}}

{{Barbra Streisand}}

[[Category:1978 greatest hits albums]]
[[Category:Barbra Streisand compilation albums]]
[[Category:Columbia Records compilation albums]]