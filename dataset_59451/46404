{{Infobox scientist
| name = Fedor Ilyich Baranov
| image = Fedor_Ilyich_Baranov.jpg
| image_size = 
| alt = 
| caption = 
| birth_date = {{Birth date|1886|04|01}}
| birth_place = [[Oryol|Oryol (or Orel)]]
| death_date = {{Death date and age|1965|07|30|1886|04|01}}
| death_place = 
| residence = 
| citizenship = 
| nationality = Russian
| fields = 
| workplaces = 
| alma_mater = [[Saint Petersburg State Polytechnical University|Saint Petersburg Polytechnical Institute]]
| doctoral_advisor = 
| academic_advisors = 
| doctoral_students = 
| notable_students = N. N. Andreev, A. L. Friedman
| known_for = [[Fisheries science]]
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences = 
| influenced = [[Bill Ricker]], [[Ray Beverton]], [[Sidney Holt]]
| awards = Veshnyakov Gold Medal by the Russian Society for Fishing and Hunting;<ref name=andreev>{{cite book |last=Andreev |first=N. N. |editor-first=P.|editor-last=Greenberg |title=Selected Works on Fishing Gear. Volume I: Commercial Fishing Techniques |publisher=Keter Publishing House, Jerusalem |year=1976 |pages=ix–xv |chapter=Professor F. I. Baranov, a great scientist and distinguished pioneer of fishery science (introduction) |isbn=9780706515619}}</ref> Honored Scientist of the [[Russian Soviet Federative Socialist Republic|RSFSR]] (1945); [[Order of Lenin]] (1951)<ref name=KSTU>[http://www.klgtu.ru/ru/departments/fpr/promriba/baranov.php Fedor Ilyich Baranov] Department of Industrial Fisheries, Kaliningrad State Technical University</ref>
| signature = <!--(filename only)-->
| signature_alt = 
| footnotes = 
| spouse = 
}}

'''Fedor Ilyich Baranov''' ([[Russian language|Russian]]: Фёдор Ильич Баранов) (1 April 1886 – 30 July 1965) was a founder of [[fisheries science]], and has been called the "grandfather of [[fisheries population dynamics]]".<ref name=Quinn2003>{{Cite journal | last = Quinn | first = Terrance J. II| title = Ruminations on the development and future of population dynamics models in fisheries | doi = 10.1111/j.1939-7445.2003.tb00119.x | journal = Natural Resource Modeling | volume = 16 | issue = 4 | pages = 341–392| year = 2003| pmid =  | pmc = }}</ref> He is best known for setting the foundations for quantitative fisheries science (including the Baranov catch equation) as well as for his contributions to development of fishing technology.

==Life and career==

Baranov graduated as a marine engineer from St. Petersburg Polytechnical Institute in 1909.<ref name=andreev/> He had a keen interest in fishing, and made his career in this area, first in improving fishing techniques but soon also in setting the foundations for the theory of fishing and fishery production.<ref name=KSTU/> In 1915, Baranov was appointed as a professor at the Department of Commercial Fisheries of the Moscow Agricultural Academy.<ref name=andreev/> Baranov was the first head of the Department of Industrial Fisheries, an institute created as a part of the Moscow Institute of Fisheries (Moskovskiy Tekhnicheskiy Institut Rybnoy Promyshlennosti i Khozyaystva, Mosrybvtuz) in 1930; he worked there until his retirement in 1959.

Baranov's ideas were often ahead of his time, and he was engaged in some disputes with other Soviet scientists.<ref name=andreev/><ref name=Quinn2003/><ref>Ben-Yami, M. 2010. [http://www.benyami.org/KSTU-2010-lecture.doc From F.I. Baranov to fishing quotas: on the problems of the contemporary fisheries management] Scientific Conference at the Kaliningrad State Technical University</ref> Many of Baranov’s peers felt that his theories were anti-Marxist, and he even risked being sent to a [[Gulag]] [[labor camp|labor colony]], only to be saved by N. N. Andreev, another fisheries technologist (and former student of Baranov<ref name=KSTU/>) and secretary of the Communist Party Committee at the Fisheries Institute.<ref name=WGFTFB2009>{{cite book 
|last=Eayrs
|first=S.
|last2=Milliken
|first2=H.
|last3=Ben-Yami
|first3=M.
|last4=DeAlteris
|first4=J.
|title=Report of the ICES - FAO Working Group on Fishing Technology & Fish Behaviour (WGFTFB), 18-22 May 2009, Ancona, Italy 
|publisher=ICES, Copenhagen
|year=2009
|pages=246–250
|chapter=Eulogy to professor Alexander L. Fridman, ScD (1926–2007) (Annex 9) 
|url=http://www.ices.dk/reports/SSGESST/2009/WGFTFB09.pdf}}</ref>

==Scientific impact==

The most famous work of Baranov is his paper from 1918, ''On the question of the biological basis of fisheries'' .<ref name=baranov1918>{{Citation
| last = Baranov
| first = F. I.
| title = К вопросу о биологических основаниях рыбного хозяйства (K voprosu o biologicheskikh osnovaniyakh rybnogo khozyaistva)
| journal = Izvestiya otdela rybovodstva i nauchno-promyslovykh issledovanii
| volume = 1 | issue = 1 
| year = 1918
| pages = 81–128
}}</ref>
In this paper, he presents the basic theory of fish population dynamics, including the famous catch equation. The paper has been translated to English at least three times, with some variations in the title:
* ''On the question of the biological foundations of fisheries'', by the British Foreign Office in 1938, on the initiative of E. S. Russell, the then Director of the Lowestoft Laboratory. This is the translation known to [[Ray Beverton]] and [[Sidney Holt]].<ref name=beverton>Anderson, E. D. 2002. The Raymond J. H. Beverton Lectures at Woods Hole, Massachusetts. Three Lectures on Fisheries Science given May 2–3, 1994. NOAA Technical Memorandum NMFS-F/SPO-54. US Department of Commerce.</ref> This copy was passed to the California State Fisheries Laboratory in 1943.
* Translation by [[Milner Baily Schaefer|Milner Schaefer]], date unknown.<ref name=Quinn2003/> It is possible that this is the British translation that was copied to the California State Fisheries Laboratory, attributed to Schaefer by mistake.
* ''On the question of the biological basis of fisheries'' by [[Bill Ricker]] in 1945.<ref name=Quinn2003/>
* ''The biological foundations of fishery'' by E. Vilim and the Israel Program for Scientific Translations, published in 1977.
Another key paper in fish population dynamics was ''On the question of the dynamics of the fishing industry'' from 1925,<ref>Baranov, F. I. 1925. К вопросу о динамике рыбного промысла. Byulleten Rybnogo Khozyaistva, 8: 7–11</ref> also translated to English many times.<ref name=Quinn2003/>

Baranov wrote in Russian, which delayed the spread of his ideas outside the [[Soviet Union]]. However, by the end of the 1930s, his work was known in the west. Nevertheless, according to Ray Beverton,<ref name=beverton/> Baranov largely disappeared from the western perspective after publishing his key papers in 1918 and 1925; Beverton knew of no western scientists who had met Baranov.

In the 1970s, a comprehensive compilation of Baranov's work was translated by the Israel Program for Scientific Translations and published in three volumes entitled ''Selected Works on Fishing Gear'':
* Volume I: Commercial Fishing Techniques. 652 pp. (1976)
** Introduction by N. N. Andreev
* Volume II: Theory and Practice of Commercial Fishing. 266 pp. (1977)
* Volume III: Theory of Fishing. 242 pp. (1977)
** On overfishing. Originally published in 1914.
** The biological foundations of fishery. Originally published in 1918.
** The dynamics of fishing. Originally published in 1925.
All the volumes were edited by P. Greenberg and translated by E. Vilim and published by the [[Keter Publishing House]], Jerusalem.

Although Baranov's ideas have been very influential to fisheries science as we know it today, it is often acknowledged that the significance of his work was only slowly recognized,<ref name=Quinn2003/> and perhaps he has not yet received credit from the western world that he deserves.<ref name=WGFTFB2009/> Despite numerous translations, his texts remain difficult to access and are mostly known through citations.

===The catch equation===

The catch equation, commonly referred to as the Baranov catch equation, gives catch (in numbers) as a function of initial population abundance ''N''<sub>''0''</sub> and fishing ''F'' and natural mortality ''M'':
:: <math>C = \frac{F}{F+M} (1-e^{-(F+M)T}) N_0</math>

where ''T'' is the time period and is usually left out (i.e. ''T=1'' is assumed). The equation assumes that fishing and natural mortality occur simultaneously and thus "compete" with each other. The first term expresses the proportion of deaths that are caused by fishing, and the second and third term the total number of deaths.<ref>Baranov, FI (1918) "On the question of the biological basis of fisheries" ''Izvestiya'', '''1''': 81–128. (Translated from Russian by W.E. Ricker, 1945)</ref>

According to Terrance Quinn,<ref name=Quinn2003/> this equation is probably the most used in all of fisheries modeling.

==Publications==
* Baranov, F. I. 1914. The capture of fish by gillnets. Mater. Poznaniyu Russ. Rybolov. 3 (6): 56-99. (in Russian)
* Baranov, F. I. 1918. К вопросу о биологических основаниях рыбного хозяйства (K voprosu o biologicheskikh osnovaniyakh rybnogo khozyaistva - On the question of the biological basis of fisheries). Izvestiya otdela rybovodstva i nauchno-promyslovykh issledovanii 1 (1): 81–128. 
* Baranov, F. I. 1925. К вопросу о динамике рыбного промысла (On the question of the dynamics of the fishing industry). Byulleten’ Rybnogo Khozyaistva, 8: 7–11. 
* Baranov, F. I. 1976-1977. Selected works on fishing gear. Israel Program for Scientific Translations, Jerusalem.

==References==
{{reflist}}

{{Fisheries scientists}}
{{Authority control}}
{{DEFAULTSORT:Baranov, Fedor}}
[[Category:1886 births]]
[[Category:1965 deaths]]
[[Category:Fisheries science]]
[[Category:Russian ichthyologists]]
[[Category:Soviet ichthyologists]]