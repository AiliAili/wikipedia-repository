{{Use dmy dates|date=March 2012}}
{{good article}}
{{Infobox military unit
|unit_name=1st Brigade, 7th Infantry Division
|image=7th Infantry Division CSIB.svg
|caption=7th Infantry Division shoulder sleeve insignia
|country={{Flagicon|USA}} United States of America
|allegiance=
|type=[[Infantry]] 
|branch=[[File:United States Department of the Army Seal.svg|20px]] [[United States Army]]
|dates=1917&nbsp;– 21<br />1963&nbsp;– 71<br />1974&nbsp;– 94
|specialization=Light infantry
|command_structure=[[7th Infantry Division (United States)|7th Infantry Division]]
|nickname=
|colors=
|march=
|mascot=
|battles=World War I<br/>[[United States invasion of Panama|Panama]]
|notable_commanders=
|anniversaries=
|identification_symbol=
|identification_symbol_label=Distinctive unit insignia
}}
{{US 7th Infantry Division}}
The '''1st Brigade, 7th Infantry Division''' was an [[infantry]] [[brigade]] of the [[United States Army]], and a part of the [[7th Infantry Division (United States)|7th Infantry Division]]. The brigade was based at [[Fort Ord]], California for most of its history. After the [[Korean War]], it was activated as a brigade in 1963, and was returned to the United States where it saw action in [[Operation Just Cause]] and [[Operation Golden Pheasant]] before being finally deactivated in 1995.

==History==

===Post-Korean War===
In the wake of the [[Korean War]], between 1953 and 1971, the 7th Infantry Division defended the [[Korean Demilitarized Zone]]. Its main garrison was [[Camp Casey, South Korea]]. During this period, the division was restructured in compliance with the [[Reorganization Objective Army Divisions]] tables of organization.<ref name="McGrath188">McGrath, p. 188.</ref> In 1963, the division's former headquarters company grew into the 1st Brigade, 7th Infantry Division.<ref name="McGrath188"/> On 2 April 1971, the division and its brigades returned to the United States and inactivated at [[Fort Lewis]], [[Washington (state)|Washington]].<ref name="Lineage">{{cite web|title=Lineage and Honors Information: 7th Infantry Division |url=http://www.history.army.mil/html/forcestruc/lineages/branches/div/007id.htm |publisher=[[United States Army Center of Military History]] |year=2009 |accessdate=27 April 2009 |archiveurl=http://www.webcitation.org/5iQIAjXxo?url=http%3A%2F%2Fwww.history.army.mil%2Fhtml%2Fforcestruc%2Flineages%2Fbranches%2Fdiv%2F007id.htm |archivedate=21 July 2009 |deadurl=no |df=dmy }}</ref>

[[File:Operation Just Cause.jpg|thumb|left|250px|Tactical map of [[Operation Just Cause]].]]
In October 1974 the 7th and two brigades reactivated at their former garrison, Fort Ord.<ref name="Lineage"/> The division was tasked to keep a close watch on South American developments. It trained at Fort Ord, [[Camp Roberts, California|Camp Roberts]], and [[Fort Hunter Liggett]]. On 1 October 1985 the division redesignated as the 7th Infantry Division (Light), organized again as a light infantry division. The various battalions of the 17th, 31st, and 32nd Regiments moved from the division, replaced by battalions from other regiments, including battalions from the [[21st Infantry Regiment (United States)|21st Infantry Regiment]], the [[27th Infantry Regiment]], and the [[9th Infantry Regiment]]. The 27th Infantry and the 9th Infantry Regiment participated in [[Operation Golden Pheasant]] in Honduras.<ref name="GSO">{{cite web | title=GlobalSecurity.org: 7th Infantry Division | url=http://www.globalsecurity.org/military/agency/army/7id.htm |work=[[GlobalSecurity]] | year=2003 | accessdate=27 April 2009| archiveurl= https://web.archive.org/web/20090417092334/http://www.globalsecurity.org/military/agency/army/7id.htm| archivedate= 17 April 2009 | deadurl= no}}</ref> In 1989 the 1st Brigade (or [[9th Infantry Regiment (United States)|9th Infantry Regiment]] as it was more commonly known), 7th Infantry Division participated in [[Operation Just Cause]] in Panama.<ref name="GSO"/>

The [[1991 Base Realignment and Closure Commission]] recommended the closing of Fort Ord due to the escalating cost of living on the central California coastline. By 1994, the garrison was to be closed and the division was to relocate to [[Fort Lewis]], [[Washington (U.S. state)|Washington]].<ref>{{cite web | title=GlobalSecurity.org: Fort Ord | url=http://www.globalsecurity.org/military/facility/fort-ord.htm |work=[[GlobalSecurity]] | year=2003 | accessdate=27 April 2009| archiveurl= https://web.archive.org/web/20090418184706/http://www.globalsecurity.org/military/facility/fort-ord.htm| archivedate= 18 April 2009 <!--DASHBot-->| deadurl= no}}</ref>

In 1993 the division was slated to move to Fort Lewis, WA and inactivate as part of the post-[[Cold War]] drawdown of the US Army. The 1st Brigade relocated to Ft. Lewis and was later reflagged as the 2nd Brigade of the [[2nd Infantry Division (United States)|2nd Infantry Division]] while the division headquarters formally inactivated on 16 June 1994 at Fort Lewis.<ref name="Lineage"/>

==Honors==

===Unit decorations===

{| class="wikitable" style="float:left;"
|- style="background:#efefef;"
! Ribbon
! Award
! Year
! Notes
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]]
||'''[[Meritorious Unit Commendation]] (Army)'''
||1953
||for service in Korea
|-
||[[File:Korean Presidential Unit Citation.png|50px]]
||'''[[Republic of Korea Presidential Unit Citation]]'''
||1950
||for the [[Inchon Landings]]
|-
||[[File:Korean Presidential Unit Citation.png|50px]]
||'''[[Republic of Korea Presidential Unit Citation]]'''
||1950–1953
||for service in Korea
|-
||[[File:Korean Presidential Unit Citation.png|50px]]
||'''[[Republic of Korea Presidential Unit Citation]]'''
||1945–1948; 1953–1971
||for service in Korea
|-
||[[File:Philippines Presidential Unit Citation.png|50px]]
||'''[[Philippine Presidential Unit Citation]]'''
||1944–1945
||for service in the Philippines during World War II
|}
{{-}}

===Campaign streamers===

{| class="wikitable" style="float:left;"
|- style="background:#efefef;"
! Conflict
! Streamer
! Year(s)
|-
|| World War I
|| [[Lorraine (province)|Lorraine]]
|| 1918
|-
|| World War II
|| [[Aleutian Islands]]
|| 1943
|-
|| World War II
|| Eastern Mandates
|| 1944
|-
|| World War II
|| [[Leyte]]
|| 1945
|-
|| World War II
|| [[Ryukyus]]
|| 1945
|-
|| [[Korean War]]
|| UN Defensive
|| 1950
|-
|| [[Korean War]]
|| UN Offensive
|| 1950
|-
|| [[Korean War]]
|| CCF Intervention
|| 1950
|-
|| [[Korean War]]
|| First UN Counteroffensive
|| 1950
|-
|| [[Korean War]]
|| CCF Spring Offensive
|| 1951
|-
|| [[Korean War]]
|| UN Summer-Fall Offensive
|| 1951
|-
|| [[Korean War]]
|| Second Korean Winter
|| 1951–1952
|-
|| [[Korean War]]
|| Korea, Summer-Fall 1952
|| 1952
|-
|| [[Korean War]]
|| Third Korean Winter
|| 1952–1953
|-
|| [[Korean War]]
|| Korea, Summer 1953
|| 1953
|}
{{-}}

==References==
{{reflist|colwidth=30em}}

==Sources==
* {{cite book|first=John J. |last=McGrath |title= The Brigade: A History: Its Organization and Employment in the US Army |year=2004 |publisher=Combat Studies Institute Press |isbn=978-1-4404-4915-4}}
* {{cite book |title= Army Almanac: A Book of Facts Concerning the Army of the United States |year=1959 |publisher=United States Government Printing Office |asin=B0006D8NKK}}

==External links==
* [http://www.carson.army.mil/UNITS/F7ID/F7ID.htm 7th Infantry Division Home Page]

[[Category:Infantry brigades of the United States Army|Division 007 01]]
[[Category:Military units of the United States Army in South Korea]]