{{coord|-34.902|138.582|type:landmark_region:AU|format=dms|display=title}}
The '''Bowden development''' is an urban development in the Australian state of [[South Australia]] on a site formerly owned by the [[Clipsal]] corporation in the suburb of [[Bowden, South Australia|Bowden]], within the [[City of Charles Sturt]], in the [[Adelaide|Adelaide metropolitan area]]. The site covers an area of {{convert|10.25|ha}} and is bounded by Park Terrace to the south, the [[Outer Harbor railway line]] to the west, Drayton Street to the north and Sixth and Seventh Streets to the east.

==History==
[[File:Bowden clipsal.JPG|thumb|400px|The Clipsal factory in Bowden in the foreground]] 
The Bowden site was occupied by Clipsal, a company manufacturing conduit and electrical accessories, in 1936. The opportunity for an urban development on the site grew out of the [[Government of South Australia|South Australian Government]]'s plans for eleven [[transport-oriented development]]s in the Adelaide metropolitan area, combined with Clipsal's decision that the Bowden site is surplus to company requirements and plan to vacate. The site was originally offered for sale in early 2008 with offers to be received by July 2008. Offers in the realm of 75 to 80 million Australian dollars were expected however evidently not attained; as the [[2008 economic crisis]] accelerated and falling property prices diminished the likelihood of a sale at expected prices, the South Australian government intervened and announced in October 2008 that it would purchase the site. It was revealed in November 2008 the government had agreed to pay A$52.5 million.<ref>{{cite web|url=http://www.ministers.sa.gov.au/news.php?id%3D3956%26print%3D1 |title=Archived copy |accessdate=2008-11-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20081206075532/http://www.ministers.sa.gov.au:80/news.php?id=3956 |archivedate=2008-12-06 |df= }}</ref> The Clipsal Industries and Gerard Corporations will continue to lease and occupy the site until the end of 2009 at which time the purchaser will gain vacant possession of the site.<ref>http://www.clipsalsiteforsale.com/clipsalbrochure.pdf{{dead link|date=November 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==2008–2011==

The South Australian Government anticipates that the site will be used to develop around 1,500 residential apartments, retail outlets and commercial offices around a town centre.<ref>{{cite web|url=http://www.ministers.sa.gov.au/news.php?id%3D3826%26print%3D1 |title=Archived copy |accessdate=2008-11-19 |deadurl=yes |archiveurl=https://web.archive.org/web/20081206075528/http://www.ministers.sa.gov.au:80/news.php?id=3826 |archivedate=2008-12-06 |df= }}</ref>  No specific site plans have yet been drafted. The SA Government is promoting the plan as an economically, socially and environmentally sustainable development which will be combined with upgrading the public transport infrastructure to the area, such as a new electrified rail and tram system.<ref>http://www.independentweekly.com.au/news/local/news/general/housing-for-clipsal-site/1342897.aspx</ref> Bowden is served by [[Bowden railway station]] which is a lower frequency station.

Following demolition, site cleanup, surveying, planning and approval, individual sites have commenced remediation and planning, and development is expected to be completed by 2020 to 2025. The development is expected to increase the population of Bowden, which was 648 in the 2006 census, by 3,500.<ref>{{Census 2006 AUS|id=SSC41161|name=Bowden (State Suburb)|accessdate=2008-06-07| quick=on}}</ref>

In April 2011, the plans for the development were approved <ref name="approval">[http://www.adelaidenow.com.au/news/south-australia/bowden-tod-gets-the-nod/story-e6frea83-1226043678620 Adelaide Now].</ref> and an information and sales centre opened in March 2012.

==References and notes==
{{reflist}}

==External links==
* [https://renewalsa.sa.gov.au/projects/bowden/ Renewal SA Bowden project page]
* [http://lifemoreinteresting.com.au/ Life More Interesting website]

[[Category:Real estate in Australia]]
[[Category:Urban planning]]
[[Category:Transit-oriented developments]]
[[Category:Adelaide]]