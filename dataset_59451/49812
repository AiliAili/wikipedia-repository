{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Tales of the Grotesque and Arabesque
| title_orig   = 
| translator   = 
| image        = GrotesqueAndArabesque.jpg
| caption = Title page for volume I
| author       = [[Edgar Allan Poe]]
| cover_artist = 
| country      = United States
| language     = English
| series       = 
| genre        = [[Horror (fiction)|Horror]] [[short stories]] and [[satire]]
| publisher    = Lea & Blanchard
| release_date = [[1840 in literature|1840]]
| media_type   = 
| pages        = 
| isbn         = 
| preceded_by  = 
| followed_by  =
}}
'''''Tales of the Grotesque and Arabesque''''' is a collection of previously published [[short story|short stories]] by [[Edgar Allan Poe]], first published in [[1840 in literature|1840]].

==Publication==
It was published by the [[Philadelphia]] firm Lea & Blanchard and released in two volumes. The publisher was willing to print the collection based on the recent success of Poe's story "[[The Fall of the House of Usher]]". Even so, Lea & Blanchard would not pay Poe any [[royalties]]; his only payment was 20 free copies.<ref name="Meyers, Jeffrey 1992. p. 113">Meyers, Jeffrey. ''Edgar Allan Poe: His Life and Legacy''. Cooper Square Press, 1992. p. 113</ref> Poe had sought [[Washington Irving]] to endorse the book, writing to him, "If I could be permitted to add ''even a word or two'' from yourself... ''my fortune would be made''".<ref>Neimeyer, Mark. "Poe and Popular Culture," collected in ''The Cambridge Companion to Edgar Allan Poe''. Cambridge University Press, 2002. p. 207. ISBN 0-521-79727-6</ref>

In his [[preface]], Poe wrote the now-famous quote defending himself from the criticism that his tales were part of "Germanism". He wrote, "If in many of my productions [[fear|terror]] has been the thesis, I maintain that terror is not of [[Germany]] but of the [[soul]]".

The collection was dedicated to Colonel [[William Drayton]], anonymous author of ''The South Vindicated from the Treason and Fanaticism of the Northern Abolitionists'' (Philadelphia: H. Manly, 1836),<ref>http://www.eapoe.org/pstudies/ps1970/p1974201.htm</ref> whom Poe likely met while stationed in [[Charleston, South Carolina]]; when Drayton moved to [[Philadelphia, Pennsylvania]], Poe continued to correspond with him.<ref>Quinn, Arthur Hobson. ''Edgar Allan Poe: A Critical Biography''. Baltimore: The Johns Hopkins University Press, 1998. p. 129. ISBN 0-8018-5730-9</ref> Drayton was a former [[member of Congress]] turned [[judge]] and may have [[subsidy|subsidized]] the book's publication.<ref name="Meyers, Jeffrey 1992. p. 113"/>

==Critical response==
Contemporary reviews were mixed. The anonymous critic in the ''Boston Notion'' suggested that Poe's work was better suited for readers of the future; people of the time should consider it "below the average of newspaper trash... wild, unmeaning, pointless, aimless... without anything of elevated fancy or fine humor". ''Alexander's Weekly Messenger'', on the other hand, remarked that the stories were the "playful effusion of a remarkable and powerful intellect". Likewise, the ''New York Mirror'' complimented the author's intellectual capacity, his vivid descriptions, and his opulent imagination. Even with those positive reviews, the edition did not sell well. When Poe requested a second release in 1841 with eight additional tales included, the publisher declined.<ref>Meyers, Jeffrey. ''Edgar Allan Poe: His Life and Legacy''. Cooper Square Press, 1992. p. 113-4.</ref>

=="Grotesque" and "Arabesque"==
When its publication was announced in ''[[Burton's Gentleman's Magazine]]'', its one-line description said that its title "pretty well indicates their [stories'] character".<ref>''Burton's Gentleman's Magazine'', January 1840. p. 58</ref> There has been some debate, however, over the meaning of Poe's terms "[[Grotesque#In art history|Grotesque]]" and "[[Arabesque (Islamic art)|Arabesque]]". Poe probably had seen the terms used by Sir [[Walter Scott]] in his essay "On the Supernatural in Fictitious Composition".<ref>Levin, Harry. "Notes from Underground" as collected in ''Twentieth Century Interpretations of Poe's Tales'', William L. Howarth, editor. Englewood Cliffs, NJ: Prentice-Hall Inc, 1971. p. 24</ref> Both terms refer to a type of [[Islamic art]] used to decorate walls, especially in [[mosque]]s. These art styles are known for their complex nature. Poe had used the term "arabesque" in this sense in his essay "[[The Philosophy of Furniture]]".<ref>Levin, Harry. "Notes from Underground" as collected in ''Twentieth Century Interpretations of Poe's Tales'', William L. Howarth, editor. Englewood Cliffs, NJ: Prentice-Hall Inc, 1971. p. 24</ref>

Poe may have been using these terms as subdivisions of [[Gothic art]] or [[Gothic architecture]] in an attempt to establish similar subdivisions in [[Gothic fiction]]. For example, the "grotesque" stories are those where the character becomes a [[caricature]] or [[satire]], as in "[[The Man That Was Used Up]]". The "arabesque" stories focus on a single aspect of a character, often [[psychology|psychological]], such as "[[The Fall of the House of Usher]]".<ref>Hoffman, Daniel. ''Poe Poe Poe Poe Poe Poe Poe''. Louisiana State University Press, 1998. pp. 203-6</ref> A distant relative of Poe, modern scholar [[Harry Lee Poe]], wrote that "grotesque" means "horror", which is gory and often disgusting, and "arabesque" means "terror", which forsakes the blood and gore for the sake of frightening the reader.<ref>Poe, Harry Lee. ''Edgar Allan Poe: An Illustrated Companion to His Tell-Tale Stories''. New York: Metro Books, 2008: 65–66. ISBN 978-1-4351-0469-3</ref> Even so, accurately defining Poe's intentions for the terms is difficult and subdividing his tales into one category or another is even more difficult.<ref>Levin, Harry. "Notes from Underground" as collected in ''Twentieth Century Interpretations of Poe's Tales'', William L. Howarth, editor. Englewood Cliffs, NJ: Prentice-Hall Inc, 1971. p. 25</ref>

==Contents==
{{wikisource}}
''Vol. I''
*"[[Morella (short story)|Morella]]"
*"[[Lionizing]]"
*"[[William Wilson (short story)|William Wilson]]"
*"[[The Man That Was Used Up|The Man That Was Used Up — A Tale of the Late Bugaboo and Kickapoo Campaign]]"
*"[[The Fall of the House of Usher]]"
*"[[The Duc de L'Omelette]]"
*"[[MS. Found in a Bottle]]"
*"[[Bon-Bon (short story)|Bon-Bon]]"
*"[[Shadow — A Parable]]"
*"[[The Devil in the Belfry]]"
*"[[Ligeia]]"
*"[[King Pest — A Tale Containing an Allegory]]"
*"[[A Predicament#How to Write a Blackwood Article|The Signora Zenobia]]"
*"[[A Predicament|The Scythe of Time]]"
''Vol. II''
*"[[Epimanes]]"
*"[[Siope]]"
*"[[The Unparalleled Adventure of One Hans Pfaall]]"
*"[[A Tale of Jerusalem]]"
*"[[Von Jung]]"
*"[[Loss of Breath]]"
*"[[Metzengerstein]]"
*"[[Berenice (short story)|Berenice]]"
*"[[Why the Little Frenchman Wears His Hand in a Sling]]"
*"[[The Visionary]]"
*"[[The Conversation of Eiros and Charmion]]"
*"Appendix" (to be appended to the "Hans Pfaall" story).

Poe later revised several of these tales and republished them under new titles:
:"The Visionary" as "The Assignation"
:"Siope—A Fable" as ""Silence—A Fable"
:"Von Jung" as "Mystification"
:"The Signora Zenobia" as "How to Write a Blackwood Article"
:"The Scythe of Time" as "A Predicament"
:"Epimanes" as "Four Beasts in One—The Homo-Cameleopard"
:"Shadow—A Fable" as "Shadow—A Parable"

==References==
{{reflist|2}}

{{Edgar Allan Poe}}

[[Category:Books by Edgar Allan Poe]]
[[Category:1840 short story collections]]
[[Category:Single-writer short story collections]]