{{DISPLAYTITLE:eDreams}}
{{Infobox company
| name = eDreams ODIGEO
| native_name = 
| native_name_lang =    <!-- Use ISO 639-1 code, e.g. "fr" for French. For multiple names in different languages, use {{lang|[code]|[name]}}. -->
| logo = eDreams Logo.png
| logo_size = 200px
| logo_alt =
| logo_caption = 
| logo_padding =
| type = [[:es:Sociedad de responsabilidad limitada|S.L.]]
| founded =             {{start date|2000}}
| founder =             <!-- or: | founders = -->
| hq_location = 
| hq_location_city = 
| hq_location_country = 
| area_served =         <!-- or: | areas_served = -->
| key_people = Dana Dunne, CEO
| industry = Tourism
| products =  	flights, hotels, vacation packages, car rentals, cruises, travel insurance
| brands = 
| services = 
| revenue = €463.3 million<ref>{{cite web|url=http://www.reuters.com/article/idUSFWN19B00E|title=BRIEF-eDreams FY 2015/2016 EBITDA up 15 pct|date=20 June 2017|publisher=|accessdate=18 February 2017|via=Reuters}}</ref>
| parent =               eDreams ODIGEO
| former_name = 
| website =             {{URL|edreamsodigeo.com/}}
}}
'''eDreams''' is an [[online]] [[travel agency]] that offers deals in regular and charter flights, low-cost airlines, hotels, car rental, dynamic packages, holiday packages and travel insurance within its localized sites across 33 countries and territories, offering prices in local currencies - [[Argentina]], [[Australia]], [[Brazil]], [[Canada]] (English and French), [[Chile]], [[Colombia]], [[Germany]], [[Egypt]], [[Spain]], [[France]], [[Greece]], [[Hong Kong]], [[Indonesia]], [[India]], [[Italy]], [[Japan]], [[Morocco]], [[Mexico]], [[Netherlands]], [[New Zealand]], [[Peru]], [[Philippines]], [[Portugal]], [[Russia]], [[Switzerland]] (French, Italian and German), [[Singapore]], [[South Africa]], [[Thailand]], [[Turkey]], [[United Arab Emirates]], [[United Kingdom]], [[United States of America]] (English and Spanish) and [[Venezuela]] – and one common website for the rest of the world in English.<ref>[http://www.edreams.com/edreams/english/call_center/?seccion=faq_quienes&item=item15 – About eDreams] (English)</ref>

The company operates through computer reservation systems [[Computer reservations system|(CRS)]] like [[Amadeus CRS|Amadeus]], [[Galileo CRS|Galileo]], [[Sabre (computer system)|Sabre]], and [[Worldspan]], where they compare, filter, and re-sell.<ref>– Freyer, Walter: Tourismus: Einführung in die Fremdenverkehrsökonomie, 8. Aufl., Múnich/Viena, Oldenbourg 2006. (funcionamiento de CRS) (German)</ref> eDreams also collaborates with more than 450 airlines on 155,000 different routes as well as with more than 855,000 hotels around the world in 40,000 destinations.<ref>[http://investing.businessweek.com/research/stocks/private/people.asp?privcapId=92093 – Portfolio] (Last update: 09-28-2012)</ref>

The leadership team includes Dana Dunne (Chief Executive Officer), Philip Wolf (Chairman), Javier Pérez-Tenessa (Honorary Chairman) <ref>{{cite web|last1=Schaal|first1=Dennis|title=eDreams Shuffles CEOs and Appoints Philip Wolf as Chairman|url=http://skift.com/2015/01/26/edreams-shuffles-ceos-and-appoints-philip-wolf-as-chairman/|publisher=Skift|accessdate=21 February 2015}}</ref> Carsten Bernhard (Chief Technology Officer), David Elizaga (Chief Financial Officer), Gerrit Goedkoop (Chief Operating Officer).<ref>{{Cite news|url=http://www.edreamsodigeo.com/about-us/leadership/|title=Leadership - eDreams ODIGEO|newspaper=eDreams ODIGEO|language=en-US|access-date=2017-01-10}}</ref> and Jerome Laurent (Chief Marketing Officer) <ref>{{cite web|title=Chief Marketing Officer)|url=https://www.yahoo.com/news/survey-says-europes-best-hotels-iberian-peninsula-091543704.html}}</ref>

== History ==
The company was founded by Javier Pérez-Tenessa de Block, Mauricio Prieto and James Hare in Silicon Valley in 2000, with the support of European and American financial groups such as DCM-Doll Capital Management, Apax Partners, Atlas Venture and 3i Group, among others.<ref>{{cite web|url=http://www.edreamsodigeo.com/about-us/history/|title=History - eDreams ODIGEO|work=edreamsodigeo.com|accessdate=18 February 2017}}</ref>  In the year 2000, the company moved its headquarters to Barcelona, Spain, and launched in the Spanish and Italian markets, becoming the first online travel agency to offer its services in Spain.  Javier Perez-Tenessa served as CEO from 2000 to 2015. James Hare served as President and Managing Director Italy from 2000 to 2010.

In October 2006, the American Private Equity firm, [[TA Associates]], acquired eDreams for €153 million, becoming the first Leveraged Buy Out (LBO) for an Internet company in Southern Europe and, at the time, the largest to date.<ref>[http://www.ta.com/news/news_detail.asp?id=133 TA Associates Completes €153M Leveraged Buyout] (Last update: 10-28-2006, English)</ref> In July 2010, the European Private Equity firm, [[Permira]], purchased eDreams from TA Associates for a sum close to €350 million. Permira became the majority shareholder of the company and in June 2011 eDreams ODIGEO was formed from the merger with Go Voyages and acquisition of Opodo and Travellink, becoming the largest online travel company in Europe, and among the top five largest in the world.<ref>[http://www.ft.com/cms/s/0/93c10f76-99aa-11df-a852-00144feab49a,dwp_uuid=e315c972-de9c-11da-acee-0000779e2340.html#axzz161iX1N3m – Permira complete first buy-out for a year] (Last update: 07-27-2010)</ref>

In 2012, eDreams launched its free iPhone App.  Numerous additional releases have included features such as the inclusion of in-app push notifications and travel guides for iOS and Android Devices.

In September 2013, eDreams ODIGEO acquired the travel search engine Liligo.<ref>{{cite web|url=http://www.latribune.fr/entreprises-finance/services/transport-logistique/20100922trib000551867/le-site-voyages-sncf.com-rachete-liligo.html|title=Le site Voyages-SNCF.com rachète Liligo|work=latribune.fr|accessdate=18 February 2017}}</ref>

In April 2014, the company completed its Initial Public Offering on the Madrid Stock exchange, which valued the company at around $1.5 billion.<ref name="reuters.com">{{cite web|url=http://www.reuters.com/article/odigeo-ipo-idUSL5N0MV2FV20140403|title=UPDATE 1-Spain's eDreams Odigeo valued at $1.5 bln in IPO|date=3 April 2017|publisher=|accessdate=18 February 2017|via=Reuters}}</ref> This was the first ever IPO of an Internet Startup in Spain.

eDreams has faced legal challenges in Europe, alongside other large Internet companies such as Google and Expedia. In 2011, the Italian regulator fined eDreams and Expedia for unfair commercial practices, due in part to pressure from offline travel agencies.<ref>{{cite web|url=http://www.tnooz.com/article/italian-regulator-hits-expedia-opodo-and-edreams-with-heavy-fines/|title=Italian regulator hits Expedia, Opodo and eDreams with heavy fines - Tnooz|work=tnooz.com|accessdate=18 February 2017}}</ref>

In 2015, the Civil Aviation Authority (CAA) launched an investigation of price transparency, culminating with CAA's Director of Consumers and Markets Group, Richard Moriarty, stating: “We are pleased that eDreams and Opodo worked with us constructively and we welcome the significant number of changes they have made to their websites." <ref>{{cite web|url=https://www.caa.co.uk/News/Opodo-and-eDreams-update-their-websites-to-improve-clarity-and-price-transparency/?catid=4294967496|title=Opodo and eDreams update their websites to improve clarity and price transparency - UK Civil Aviation Authority|work=caa.co.uk|accessdate=18 February 2017}}</ref>

In addition, eDreams has had several public legal disputes with some of the company's commercial airline partners.  Most notably, Ryanair and eDreams have had a multi-year public relations battle, with Ryanair accusing both Google and eDreams of unfair practices <ref>{{cite web|url=https://www.rte.ie/news/business/2015/1201/750492-ryanair-to-sue-google-and-edreams/|title=Ryanair to sue Google and eDreams|date=1 December 2015|work=rte.ie|accessdate=18 February 2017}}</ref> along with eDreams making numerous counter claims against Ryanair.<ref>{{cite web|url=http://www.edreamsodigeo.com/press-releases/2014/10/response-from-edreams-to-the-statement-by-ryanair/|title=Response from eDreams to the statement by Ryanair - eDreams ODIGEO|date=29 October 2014|work=edreamsodigeo.com|accessdate=18 February 2017}}</ref><ref>{{cite web|url=https://www.tnooz.com/article/ryanair-attacks-edreams-over-google-advertising/|title=eDreams hits back over Ryanair claims it is misleading consumers on Google|work=tnooz.com|accessdate=18 February 2017}}</ref>

In January 2015, eDreams announced a management transition with former Chief Operating Officer Dana Dunne replacing co-founder Javier Perez-Tenessa as CEO, and non-Executive Director Philip Wolf becoming Chairman.<ref>{{cite web|url=http://www.edreamsodigeo.com/press-releases/2015/01/edreams-odigeo-appoints-dana-dunne-as-ceo/|title=eDreams ODIGEO appoints Dana Dunne as CEO - eDreams ODIGEO|date=26 January 2015|work=edreamsodigeo.com|accessdate=18 February 2017}}</ref> The remaining co-founders departed soon after, with James Hare resigning from the Board in March 2015.<ref>{{cite web|url=http://www.europapress.es/turismo/agencias-ttoo/noticia-james-hare-confundador-compania-dimite-consejero-edreams-odigeo-20150326105819.html|title=James Hare, cofundador de la compañía, dimite como consejero de eDreams Odigeo|first=Europa|last=Press|date=26 March 2015|work=europapress.es|accessdate=18 February 2017}}</ref><ref name="edreamsodigeo1">{{cite web|url=http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2014/03/HR_EDREAMS_26Marzo2015_JHare.pdf |format=PDF |title=HECHO RELEVANTE |website=Edreamsodigeo.com |accessdate=2016-09-16}}</ref>

At the 2016 British Travel Awards, the company's Opodo website won the "Best Flight Booking Website"  award for the second year in a row.<ref name="edreamsodigeo.com">{{cite web|url=http://www.edreamsodigeo.com/uncategorized/2016/11/opodo-named-best-flight-booking-website-second-year-row/|title=Opodo named “Best Flight Booking Website” for second year in a row - eDreams ODIGEO|date=24 November 2016|work=edreamsodigeo.com|accessdate=18 February 2017}}</ref>

== Revenue and Expansion ==
eDreams earns its revenue through the sale of its travel products and advertising.

In fiscal year (FY) 2011, eDreams was Europe's largest online travel agency group, with gross bookings of €3.9 billion, and more than 14 million customers worldwide.<ref>{{cite web|url=https://www.brightwire.com/news/230122-odigeo-achieved-a-turnover-of-eur-3-9-billion-in-fy-2011/|title=Brightwire|work=brightwire.com|accessdate=18 February 2017}}</ref>

In FY 2012, the company continued its internationalization plan and expanded into 10 new country markets: Egypt, Hong Kong, Indonesia, Morocco, New Zealand, Singapore, South Africa, and Thailand.<ref>{{cite web|url=http://blog.edreams.com/edreams-expands-to-10-new-countries/|title=eDreams Expands to 10 New Countries - eDreams Travel Blog|work=edreams.com|accessdate=18 February 2017}}</ref>

In the same year, the company's gross bookings were €4.15 billion <ref>[http://www.odigeo.com/sites/www.odigeo.com/files/Press%20Kit%20ODIGEO.pdf/ - ODIGEO Media Kit]</ref> with over 8.7 million online purchases.<ref name="edreamsodigeo.com1">[http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2014/03/eDreams_ODIGEO_Consolidated_Annual_Report_March_2014.pdf eDreams ODIGEO Consolidated Annual Report March 2014]</ref>

In FY 2013, eDreams continued to be the largest online seller of flights in the world and Europe’s largest e-commerce company in terms of profits, with business volume nearing €4.4 billion <ref>{{cite web|url=http://www.edreamsodigeo.com/press-releases/2014/06/edreams-odigeo-closes-2013-with-business-volume-nearing-e4-4-billion/|title=eDreams ODIGEO closes 2013 with business volume nearing €4.4 billion - eDreams ODIGEO|date=20 June 2014|work=edreamsodigeo.com|accessdate=18 February 2017}}</ref> and nearly 9.8 million purchases.<ref name="edreamsodigeo.com1"/>

In FY 2014, eDreams had gross bookings of over €4.2 billion with over 9.7 million online purchases.<ref name="edreamsodigeo.com2">[http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2015/08/Annual_Report_FY_2015_16.pdf eDreams ODIGEO Annual Report 2015-2016]</ref>

In FY 2015 (ended in March 2016), eDreams had gross bookings of over €4.5 billion with nearly 10.7 million online purchases.<ref name="edreamsodigeo.com2"/>

==IPO==

Independent Board Member James Hare (James Otis Hare II) oversaw the public launch on April 4, 2014.<ref name="edreamsodigeo1">{{cite web|url=http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2014/03/HR_EDREAMS_26Marzo2015_JHare.pdf |format=PDF |title=HECHO RELEVANTE |website=Edreamsodigeo.com |accessdate=2016-09-16}}</ref>

eDreams offered its stock at 10.25 Euros per share.<ref>{{cite web|url=http://tech.eu/news/edreams-odigeo-ipo-aftermath/ |title=After IPO, eDreams Odigeo shares plummet 75% in 5 months |publisher=Tech.eu |date=2014-09-11 |accessdate=2016-09-07}}</ref>

That stock price had fallen to 1.02 Euros by October 24 of 2014, wiping out around one billion Euros of market capitalization.<ref>{{cite web|author=|url=http://www.bloomberg.com/quote/EDR:SM|title=EDR:Soc.Bol SIBE Stock Quote|website=Bloomberg.com |date=2016-06-09 |accessdate=2016-09-07}}</ref>

Some commentators called the launch “Europe’s worst performing IPO of 2014”.<ref>{{cite web|url=http://www.tnooz.com/article/recovering-ipo-nightmare-edreams-odiego-says-growing/ |title=Down 60%, eDreams Odigeo shares try to escape post-IPO nightmare |publisher=Tnooz.com |date=2014-08-29 |accessdate=2016-09-07}}</ref>

eDreams moved quickly, asking their shareholders for authorization to “Discharge to Mr. James Otis Hare for the exercise of his mandate as director of the Company until his resignation as of 25 March, 2015.” <ref>http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2014/08/eDreams_ODIGEO-Voting_formAGM_2015.pdf</ref>

eDreams issued the following announcement: “Effective March 25, 2015, eDreams ODIGEO (“the Company”) accepts the resignation of Mr. James Hare as an Independent member from the Board of Directors”.<ref> http://www.edreamsodigeo.com/wp-content/uploads/sites/19/2014/08/Annual_Corporate_Governance_Report_31March2015.pdf</ref>

In June 2015, CEO Dana Dunne introduced a new strategy focusing on mobile, revenue diversification and customer experience improvements, which led to a strong turnaround in business performance.<ref>{{cite web|url=https://www.tnooz.com/article/edreams-odigeo-half-year-results-2015/|title=eDreams Odigeo strategy begins to pay off|website=Tnooz|language=en-US|access-date=2016-06-16}}</ref>

Dunne's new strategy caused that stock price to rise above 3 euros by January 2017. <ref>[http://www.bloomberg.com/quote/EDR:SM]</ref>

==Studies==
eDreams performs studies related to the travel industry by conducting surveys to travellers and since 2012 has released the following studies: “Best Airports In the World” <ref>{{cite web|title=eDreams Reveals Best Airports in the World #BestAirports|url=http://www.prnewswire.co.uk/news-releases/edreams-reveals-best-airports-in-the-world-bestairports-249909791.html}}</ref>[42], “Best Airlines in the World” and “Best Hotels in the World”  <ref>{{cite web|title=Survey says Europe's best hotels are on the Iberian Peninsula|url=https://www.yahoo.com/news/survey-says-europes-best-hotels-iberian-peninsula-091543704.html}}</ref>[44].

==Awards==
At the 2016 British Travel Awards, the company's Opodo website won the "Best Flight Booking Website"  award for the second year in a row.<ref name="edreamsodigeo.com"/>

eDreams has also won: eAwards “Best international website 2013”,<ref>{{cite web|url=http://www.the-emagazine.com/ecommerce/casos/item/793-eawards-barcelona-2013|title=eAwards Barcelona 2013 - eShow Magazine Mobile|work=the-emagazine.com|accessdate=18 February 2017}}</ref> Premios Emprendedores “Featured company 2012”,<ref>{{cite web|url=http://especiales.emprendedores.es/emprendedores/premios/2012/|title=Revista Emprendedores. PREMIOS EMPRENDEDORES|work=emprendedores.es|accessdate=18 February 2017}}</ref> Premios Imán de comercio electrónico “Special interactive formats 2008”,<ref>{{cite web|url=http://www.europapress.es/economia/noticia-economia-empresas-edreams-galardonada-premios-iman-comercio-electronico-foro-viajes-20080613182242.html|title=eDreams, galardonada en los Premios Imán de comercio electrónico por su foro de viajes|first=Europa|last=Press|date=13 June 2008|work=europapress.es|accessdate=18 February 2017}}</ref> and Premio iBest “Best Spanish tourism and travel website 2001”,<ref>[http://www.hosteltur.com/02557_edreams-gana-premio-ibest-mejor-web-turismo-viajes.html - Best Spanish tourism and travel website 2001].</ref> eAWARDS (EMOTA), Best International Expansion 2014 and CFI.co “Best Online Travel Partner 2015.

eDreams was a finalist in the 2013 eCommerce Awards Spain in the “Best Spanish Webshop” category.<ref>{{cite web|url=http://2013.premios-ecommerce.com/edreams/|title=eDreams|work=premios-ecommerce.com|accessdate=18 February 2017}}</ref>

In 2012 the company was a finalist in the following award programs: #IABInspirational in the “Digital advertiser of the year” category,<ref>{{cite web|url=http://www.marketingdirecto.com/especiales/festival-inspirational-2012-especiales/84-campanas-finalistas-optaran-a-premio-en-el-iabinspirational-2012/|title=84 campañas finalistas optarán a premio en el #IABInspirational 2012 - Marketing Directo|date=27 November 2012|work=marketingdirecto.com|accessdate=18 February 2017}}</ref> eCommerce Awards in the “Best travel and tourism platform” category,<ref>{{cite web|url=http://2012.premios-ecommerce.com/candidatos/viajes-y-turismo|title=PREMIOS E-COMMERCE - E-commerce Awards 2012 España > Premio a la mejor plataforma de viajes y turismo|work=premios-ecommerce.com|accessdate=18 February 2017}}</ref> Premios de Internet “Best company” category,<ref>{{cite web|url=http://www.sociosinversores.es/premio-mejor-empresa-de-internet-2012/|title=SociosInversores.com, líderes en inversión colectiva|work=sociosinversores.es|accessdate=18 February 2017}}</ref> and in the European Business Awards “Best company” category <ref>[http://www.businessawardseurope.com/entries/detail/spain/3564 - European Business Awards “Best company”].</ref>

== References ==
{{reflist|30em}}

== External links ==
* [http://www.edreamsodigeo.com/]

[[Category:Online travel agencies]]