{{Infobox journal
| title = Chemical Reviews
| cover = [[Image:Chemical Reviews cover.jpg|150 px]]
| editor = Sharon Hammes-Schiffer 
| discipline = [[Chemistry]]
| abbreviation = Chem. Rev.
| publisher = [[American Chemical Society]]
| country = United States
| frequency = Biweekly
| history = 1924-present
| impact = 37.369
| impact-year =2015
| website = http://pubs.acs.org/journals/chreay/index.html
| link1 = http://pubs.acs.org/toc/chreay/current
| link1-name = Online access
| link2 = http://pubs.acs.org/loi/chreay
| link2-name = Online archive
| ISSN =0009-2665
| eISSN = 1520-6890
| CODEN = CHREAY
| OCLC = 473768321
| LCCN = 25015032
}}
'''''Chemical Reviews''''' is a monthly [[peer-reviewed]] [[scientific journal]] published by the [[American Chemical Society]]. It publishes [[review article]]s on all aspects of [[chemistry]]. It was established in 1924 by [[William Albert Noyes]] ([[University of Illinois]]). {{As of|2015|1|1}} the editor-in-chief is [[Sharon Hammes-Schiffer]] (University of Illinois at Urbana-Champaign).<ref name=Editor>{{cite web|title=Sharon Hammes-Schiffer joins Chemical Reviews as new editor-in-chief|url=http://www.acs.org/content/acs/en/pressroom/newsreleases/2014/december/sharon-hammes-schiffer-joins-chemical-reviews-as-new-editor-in-chief.html|website=ACS Chemistry for Life|publisher=American Chemical Society|date=December 2, 2014}}</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Chemical Abstracts Service]], [[CAB International]], [[EBSCOhost]], [[ProQuest]], [[PubMed]], [[Scopus]], and the [[Science Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 37.369.<ref name=WoS>{{cite book |year=2016 |chapter=Chemical Reviews |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://pubs.acs.org/journals/chreay/index.html}}

[[Category:American Chemical Society academic journals]]
[[Category:Review journals]]
[[Category:Chemistry journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1924]]

{{chem-journal-stub}}