{{Underlinked|date=June 2015}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=PIK-5
 | image=PIK-5b.JPG
 | caption=PIK-5b in [[Finnish Aviation Museum]]
}}{{Infobox Aircraft Type
 | type=Training glider
 | national origin=Finland
 | manufacturer=[[Polyteknikkojen Ilmailukerho]]
 | designer= Kaarlo J. Temmes
 | first flight=September 1946
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=34
 | developed from= 
 | variants with their own articles=
}}
|}

The '''PIK-5''' was a training [[Glider (aircraft)|glider]] produced in Finland in the 1940s and 1950s,<ref name="JAE">Taylor 1989, p.726</ref> equipping the country's gliding clubs with an aircraft greater in performance than primary gliders but less than competition [[Glider (sailplane)|sailplanes]].<ref name="Hardy">Hardy 1982, p.74</ref> The aircraft had a pod-and-boom configuration, with a high, strut-braced [[monoplane]] wing and a [[cruciform tail]] carried at the end of a tailboom that extended from a position high on the [[aft]] end of the pod.<ref name="PIK">"PIK-sarjan lentokoneet"</ref>

The prototype first flew in September 1946<ref name="KI">"PIK-5c Cumulus (OH-151)"</ref> and testing continued until it was badly damaged in a crash in summer 1948<ref name="Tiusanen">Tiusanen 1952, P.12</ref> Over the subsequent months, the wings were repaired, and a new fuselage constructed to a revised design. This was completed the following winter, and flights recommenced.<ref name="Tiusanen"/> However, this aircraft, now known as the '''PIK-5B''', was destroyed in a crash in summer 1951.<ref name="Tiusanen"/> Again, it was rebuilt with modifications, particularly to the wing structure, resulting in the '''PIK-5C''' version.<ref name="Tiusanen"/> This version first flew on 5 July 1952<ref name="Tiusanen"/> and went on to become the pattern for around 30 similar machines that would be built over the ensuing years.<ref name="KI" />

<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==
* '''PIK-5'''
* '''PIK 5A'''
* '''PIK-5B'''
* '''PIK-5C'''

<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (PIK-5C) ==
{{Aircraft specs
|ref=<ref name =PIK5C>{{cite web |title=PIK-5c Cumulus (OH-151) |work=Karhulan Ilmailukerho website |url=http://users.kymp.net/mode0449/pik5c.htm |accessdate=2009-01-09}}</ref> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=180–191|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=6.4
|length ft=
|length in=
|length note=
|span m=12.4
|span ft=
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=14.7
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=10.4
|airfoil=Göttingen 533
|empty weight kg=120
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=210
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=45
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=190
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|120|km/h|mph kn|abbr=on|1}}
*'''Aerotow speed:''' {{convert|20|km/h|mph kn|abbr=on|1}}
*'''Winch launch speed:''' {{convert|90|km/h|mph kn|abbr=on|1}}
*'''Terminal velocity:''' with full air-brakes at max all-up weight {{convert|180|km/h|mph kn|abbr=on|0}}
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+4 -2
|roll rate=<!-- aerobatic -->
|glide ratio=18 at {{convert|60|km/h|mph kn|abbr=on|1}}
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=0.85
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=at {{convert|52|km/h|mph kn|abbr=on|1}}
|lift to drag=
|wing loading kg/m2=14.3
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|PIK-5}}
*{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde Volume II|year=1963|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=180–191|edition=1st|author2=K.G. Wilkinson |language=English, French, German}}
* {{cite book |last= Hardy |first= Michael |title=Gliders and Sailplanes of the World |year=1982 |publisher=Ian Allan |location=Shepperton }}
* {{cite web |title=PIK-5c Cumulus (OH-151) |work=Karhulan Ilmailukerho website |url=http://users.kymp.net/mode0449/pik5c.htm |accessdate=2009-01-09}}
* {{cite web |title=PIK-sarjan lentokoneet |work=Polyteknikkojen Ilmailukerho website |url=http://pik.tky.fi/joomla/index.php?option=com_content&task=view&id=43&Itemid=108 |accessdate=2009-01-08 }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
* {{cite journal |last=Tiusanen |first=Keijo |title=PIK-5c |journal=Ilmailu |issue=9 |date=1952 |pages=12}}
<!-- ==External links== -->

{{PIK aircraft}}

[[Category:Finnish sailplanes 1940–1949]]
[[Category:PIK aircraft]]
[[Category:Glider aircraft]]