{{Use mdy dates|date=February 2012}}
{{Good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1984
| Track=1984 Atlantic hurricane season summary map.png
| First storm formed=June 11, 1984
| Last storm dissipated=December 24, 1984
| Strongest storm name=[[Hurricane Diana (1984)|Diana]]
| Strongest storm pressure=949
| Strongest storm winds=115
| Average wind speed=1
| Total depressions=18
| Total storms=13
| Total hurricanes=5
| Total intense=1
| Fatalities=35
| Damages=66.4
| Inflated=1
| five seasons=[[1982 Atlantic hurricane season|1982]], [[1983 Atlantic hurricane season|1983]], '''1984''', [[1985 Atlantic hurricane season|1985]], [[1986 Atlantic hurricane season|1986]]
}}
<!-- Created with subst: of [[Template:Hurricane season single]]. -->The '''1984 Atlantic hurricane season''' was the busiest since [[1971 Atlantic hurricane season|1971]]. It officially began on June 1, 1984, and lasted until November 30, 1984. These dates conventionally delimit the period of each year when most [[tropical cyclone]]s form in the [[Atlantic basin]]. The 1984 [[Atlantic hurricane season|season]] was an active one in terms of named storms, but most of them were weak and stayed at sea.  Most of the [[cyclone]]s tracked through the northwest subtropical Atlantic west of the 50th meridian to near the Eastern coast of the United States between mid-August and early October.  The most damaging storm was [[Hurricane Diana (1984)|Hurricane Diana]], which caused $65.5&nbsp;million (1984&nbsp;dollars) in damage in [[North Carolina]]. Diana was the first hurricane to strike a [[Nuclear reactor|nuclear power plant]] without incident; it was also the first major hurricane to strike the [[U.S. East Coast]] in nearly 20&nbsp;years. Also of note was [[Hurricane Lili (1984)|Hurricane Lili]], which lasted well after the official end of the season. It was downgraded from a named storm on December 24.  Damage overall from the tropical cyclones in 1984 totaled $66.4&nbsp;million (1984 USD).

==Season summary==
Six storms during the season had [[subtropical cyclone|subtropical characteristics]] at some point in their track, those being Subtropical Storm One,<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/trop/prelim01.gif|title=Subtropical Storm One Preliminary Report|last=Sheets|first=Robert|date=October 4, 1984|publisher=National Hurricane Center|accessdate=October 3, 2010}}</ref> Tropical Storm Cesar,<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/cesar/prelim01.gif|title=Tropical Storm Cesar Preliminary Report|year=1984|publisher=National Hurricane Center|accessdate=October 3, 2010}}</ref> Hurricane Hortense,<ref name="tcr1">{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/hortense/prelim01.gif|title=Hurricane Hortense Preliminary Report #1|last=Lawrence|first=Miles|year=1984|publisher=[[National Hurricane Center]]|accessdate=October 3, 2010}}</ref> Hurricane Josephine,<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/josephin/prelim01.gif|title=Hurricane Josephine Preliminary Report|last=Sheets|first=Robert|date=October 27, 1984|publisher=National Hurricane Center|accessdate=October 3, 2010}}</ref> [[Hurricane Klaus (1984)|Hurricane Klaus]],<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/klaus/prelim01.gif|title=Hurricane Klaus Preliminary Report|year=1984|publisher=National Hurricane Center|accessdate=October 3, 2010}}</ref> and [[Hurricane Lili (1984)|Hurricane Lili]].<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/lili/prelim01.gif|title=Hurricane Lili Preliminary Report|date=c. 1985|publisher=National Hurricane Center|accessdate=October 3, 2010}}</ref>

The season's activity was reflected with a cumulative [[accumulated cyclone energy]] (ACE) rating of 84,<ref>{{cite web|author=Hurricane Research Division|publisher=National Oceanic and Atmospheric Administration|date=March 2011|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=July 23, 2011|url=http://www.aoml.noaa.gov/hrd/hurdat/Comparison_of_Original_and_Revised_HURDAT_mar11.html}}</ref> which is classified as "near normal".<ref name="background">{{cite web|author=[[National Oceanic and Atmospheric Administration]]|url=http://www.cpc.ncep.noaa.gov/products/outlooks/background_information.shtml|title=Background information: the North Atlantic Hurricane Season|date=May 27, 2010|publisher=[[Climate Prediction Center]]|accessdate=March 30, 2011| archiveurl= https://web.archive.org/web/20110510081844/http://www.cpc.ncep.noaa.gov/products/outlooks/background_information.shtml| archivedate=May 10, 2011 <!--DASHBot-->| deadurl= no}}</ref> ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed, so storms that last a long time, as well as particularly strong hurricanes, have high ACEs. ACE is only calculated for full advisories on tropical systems at or exceeding 34&nbsp;[[knot (unit)|knot]]s (39&nbsp;mph, 63&nbsp;km/h) or tropical storm strength. Although officially, [[subtropical cyclone]]s are excluded from the total,<ref>{{cite web|author=David Levinson|date=August 20, 2008|title=2005 Atlantic Ocean Tropical Cyclones|publisher=National Climatic Data Center|accessdate=July 23, 2011|url=http://www.ncdc.noaa.gov/oa/climate/research/2005/2005-atlantic-trop-cyclones.html}}</ref> the figure above includes periods when storms were in a subtropical phase.

==Systems==
<timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/06/1984 till:31/12/1984
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/06/1984

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:11/06/1984 till:14/06/1984 color:TD text:"One"
  from:18/06/1984 till:20/06/1984 color:TD text:"Two"
  from:25/07/1984 till:28/07/1984 color:TD text:"Three"
  from:18/08/1984 till:21/08/1984 color:TS text:"One"
  from:28/08/1984 till:06/09/1984 color:TS text:"Arthur"
  from:30/08/1984 till:04/09/1984 color:TS text:"Bertha"
   barset:break
  from:31/08/1984 till:02/09/1984 color:TS text:"Cesar"
  from:06/09/1984 till:08/09/1984 color:TD text:"Seven"
  from:08/09/1984 till:16/09/1984 color:C4 text:"[[Hurricane Diana (1984)|Diana]]"
  from:14/09/1984 till:15/09/1984 color:TS text:"Edouard"
  from:15/09/1984 till:20/09/1984 color:TS text:"Fran"
  from:16/09/1984 till:19/09/1984 color:TS text:"Gustav"
   barset:break
  from:23/09/1984 till:02/10/1984 color:C1 text:"Hortense"
  from:25/09/1984 till:01/10/1984 color:TS text:"Isidore"
  from:07/10/1984 till:18/10/1984 color:C2 text:"[[Hurricane Josephine (1984)|Josephine]]"
  from:25/10/1984 till:29/10/1984 color:TD text:"Seventeen"
  from:05/11/1984 till:13/11/1984 color:C1 text:"[[Hurricane Klaus (1984)|Klaus]]"
  from:22/11/1984 till:29/11/1984 color:TD text:"Nineteen"
  barset:break
  from:12/12/1984 till:24/12/1984 color:C1 text:"[[Hurricane Lili (1984)|Lili]]"
  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/06/1984 till:01/07/1984 text:June
  from:01/07/1984 till:01/08/1984 text:July
  from:01/08/1984 till:01/09/1984 text:August
  from:01/09/1984 till:01/10/1984 text:September
  from:01/10/1984 till:01/11/1984 text:October
  from:01/11/1984 till:01/12/1984 text:November
  from:01/12/1984 till:31/12/1984 text:December

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir-Simpson Hurricane Scale]])"

</timeline>

===Tropical Depression One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1-L Jun 13 1984 1800Z.jpg
|Track=1-L 1984 track.png
|Formed=June 11
|Dissipated=June 14
|1-min winds=30
|Prepressure=≤
|Pressure=1016
}}
By June 11, an upper level low caused thunderstorm development off the Florida coast, which caused the formation of a tropical depression.  Moving westward, the depression moved into St. Augustine, causing a total of {{convert|5.02|in|mm}} of rainfall at [[Jacksonville Beach, Florida]] as its main thunderstorm activity was concentrated north of the center.  It dissipated as a tropical cyclone on June 14 while moving through the Florida panhandle.<ref>{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=DN&s_site=philly&p_multi=PIDN&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB296A509DA6C8E&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Tropical Depression Hits Florida Coast|date=June 13, 1984|accessdate=March 6, 2010|publisher=[[Philadelphia Daily News]]}}</ref><ref name="TD1-2">{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=MH&s_site=miami&p_multi=MH&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB35FD3513DC90B&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Harbinger of Hurricane Fading Fast|date=June 14, 1984|accessdate=March 6, 2010|publisher=[[The Miami Herald]]}}</ref><ref name="cliqr">{{cite web|url=http://www.wpc.ncep.noaa.gov/research/roth/ebtrk_nhc_final.txt|author=David M. Roth|year=2010|accessdate=April 13, 2010|publisher=[[Hydrometeorological Prediction Center]]|title=CLIQR database}}</ref>  The small remnant low continued moving westward inland of the Gulf coast, causing occasional redevelopment of thunderstorm activity as the system moved into Louisiana, before both the thunderstorm activity and low pressure area dissipated by June 17.<ref>{{cite web|author=David M. Roth|url=http://www.wpc.ncep.noaa.gov/tropical/rain/td01aof1984.html|title=Tropical Depression One&nbsp;– June 12–17, 1984|date=August 4, 2008|publisher=[[Hydrometeorological Prediction Center]]|accessdate=April 24, 2008}}</ref><ref>[[NOAA]]. [http://docs.lib.noaa.gov/rescue/dwm/1984/19840611-19840617.djvu Daily Weather Maps: June 11–17, 1984.] Retrieved on April 24, 2008.</ref>
{{Clear}}

===Tropical Depression Two===
{{Infobox Hurricane Small
|Basin=Atl
|Track=Tropical_Depression_Two_1984_rainfall.gif
|Image=Img-1984-06-19-18-GOE-5-IR edited.jpg
|Formed=June 18
|Dissipated=June 20
|1-min winds=30
|Pressure=1008
}}
An upper level low moving across the southern Gulf of Mexico spawned an area of thunderstorm activity over the Mexican isthmus on June 16.  The thunderstorm area moved northwest, pulsing in intensity, until flaring up into a larger area of deeper convection early in the morning of June 18.  A surface low formed, and the system was considered well-enough organized to be a tropical depression, the second of the season, while located southeast of [[Brownsville, Texas]].<ref name="TD2">{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=MH&s_site=miami&p_multi=MH&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB35FDA26874527&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=New Tropical Depression Forms In Gulf Of Mexico|date=June 19, 1984|accessdate=March 6, 2010|publisher=[[The Miami Herald]]}}</ref>  On June 19, vertical wind shear from the west-southwest halted further development, and the tropical depression began a general weakening trend which continued past its landfall point in northeast Mexico. By early morning of June 20, the system completely dissipated.<ref name="cliqr"/><ref>{{cite web|url=http://www.wpc.ncep.noaa.gov/tropical/rain/td02aof1984.html|title=Tropical Depression Two&nbsp;— June 16–20, 1984|author=David M. Roth|publisher=[[Hydrometeorological Prediction Center]]|date=March 8, 2010|accessdate=March 8, 2010}}</ref>
{{clear}}

===Tropical Depression Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=3-L Jul 25 1984 1950Z.jpg
|Formed=July 25
|Dissipated=July 26
|1-min winds=30
|Pressure=1000
}}
A tropical depression formed on July 25<ref name="TD3-2">{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=MH&s_site=miami&p_multi=MH&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB3600CF76ABDB2&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Potential Tropical Storm Monitored|date=July 25, 1984|accessdate=March 6, 2010|publisher=[[The Miami Herald]]}}</ref> and moved westward, producing rainfall up to six inches in Barbados on July 26.<ref name="TD3">{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=MH&s_site=miami&p_multi=MH&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB3600F2CEE7ABB&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Tropical Depression Hits Caribbean|date=July 26, 1984|accessdate=March 6, 2010|publisher=[[The Miami Herald]]}}</ref> The depression dissipated on July 26.<ref name="cliqr"/> Two commercial fisherman were reported missing near [[St. Lucia]].<ref name="TD3-3">{{cite web|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=MH&s_site=miami&p_multi=MH&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=0EB3601509C9B4B5&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM|title=Storm Weakens In Caribbean Two Fishermen Missing After Ignoring Warnings|publisher=[[The Miami Herald]]|date=July 28, 1984|accessdate=March 6, 2010}}</ref>
{{Clear}}

===Subtropical Storm One===
{{Infobox Hurricane Small
|Basin=Atl
|Type=subtropical
|Image=Subtropical Storm One (1984).JPG
|Track=1984 Atlantic subtropical storm 1 track.png
|Formed=August 18
|Dissipated=August 21
|1-min winds=50
|Pressure=1000
}}
A weak front generated a low pressure system that organized into a [[subtropical]] depression north of [[Bermuda]] on August 18. The depression headed northeast and strengthened to a subtropical storm. It is believed to have merged with a front on August 21. The history of Subtropical Storm One is not entirely certain, as satellite images were largely unavailable due to a failure of the [[Visible infrared spin-scan radiometer|VISSR]] unit on GOES EAST (then [[GOES]]-5), and this system remained at the fringe of the GOES WEST and [[Meteosat]] throughout its existence.<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/trop/prelim01.gif|title=Subtropical Storm One: 18–21 August 1984|author=[[National Hurricane Center]]|publisher=[[National Oceanic and Atmospheric Administration]]|year=1984|accessdate=February 17, 2010}}</ref> Winds of 65&nbsp;mph (100&nbsp;km/h) was reported on the southwest coast of [[Newfoundland (island)|Newfoundland]]. In addition, a weather office on the island reported rainfall at {{convert|2.1|in|mm|abbr=on}}.<ref>{{cite web|url=http://www.ec.gc.ca/Hurricane/default.asp?lang=En&n=FA6CA6E9-1|title=1984-Subtrop 1|date=September 14, 2010|publisher=[[Environment Canada]]|accessdate=July 10, 2011}}</ref>
{{Clear}}

===Tropical Storm Arthur===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Arthur (1984).JPG
|Track=Arthur 1984 track.png
|Formed=August 28
|Dissipated=September 5
|1-min winds=45
|Pressure=1004
}}
The 1984 season's first named storm occurred later than usual, forming on August 28. Arthur formed east of the [[Windward Islands]] and tracked generally northwest. It was downgraded to a depression on September 1 after being negatively impacted by vertical [[wind shear]], and dissipated several days later.<ref>{{cite web|author=Gilbert Clark|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/arthur/prelim01.gif|title=Tropical Storm Arthur: 28 August to 5 September 1984|year=1984|publisher=[[National Hurricane Center]]|accessdate=April 13, 2010}}</ref> Arthur was a minimal tropical storm,{{Atlantic hurricane best track}} and caused no significant weather on land.
{{Clear}}

===Tropical Storm Bertha===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Bertha (1984).JPG
|Track=Bertha 1984 track.png
|Formed=August 30
|Dissipated=September 4
|1-min winds=35
|Pressure=1007
}}
Bertha was a short-lived tropical storm that formed in the mid-Atlantic on August 31, in close proximity to the east of Arthur. The storm took a parabolic path to the north and northeast before merging with a [[cold front]] on September 4. Bertha never approached land and caused no reported damage.{{Atlantic hurricane best track}}
{{Clear}}

===Tropical Storm Cesar===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Cesar (1984).JPG
|Track=Cesar 1984 track.png
|Formed=August 31
|Dissipated=September 2
|1-min winds=50
|Pressure=994
}}
A second storm formed on August 31 as a non-tropical low strengthened into Tropical Storm Cesar off the [[East Coast of the United States]]. Cesar traveled east-northeast and strengthened gradually until it became [[extratropical]] and merged with another system off the coast of [[Newfoundland (island)|Newfoundland]] on September 2.{{Atlantic hurricane best track}}
{{Clear}}

===Tropical Depression Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=TD7 06 sept 1984 1800Z.JPG
|Track=Tropical_Depression_Seven_1984_rainfall.gif
|Formed=September 6
|Dissipated=September 8
|1-min winds=30
}}
A tropical wave moved across Central America into the far eastern north Pacific Ocean by August 28.  The system moved westward with no signs of development until September 1, when an upper level low to its north across the Gulf of Mexico caused an area of thunderstorms to form just south of the Mexican coastline.  An upper trough developed across the southern Plains of the United States, which slowly lured the northern portion of this increasingly large disturbance northward through the Mexican Isthmus.  The southern portion moved westward, developing into [[1984 Pacific hurricane season#Hurricane Marie|Hurricane Marie]].  For a short while, Marie acted as a source of vertical wind shear from the west for this system, halting further development.<ref>{{cite web|author=David M. Roth|publisher=[[Hydrometeorological Prediction Center]]|date=March 9, 2010|url=http://www.wpc.ncep.noaa.gov/tropical/rain/td07aof1984.html|accessdate=March 9, 2010|title=Tropical Depression Seven&nbsp;— September 1–8, 1984}}</ref>

By September 6, the disturbance had emerged into the southwest Gulf of Mexico and consolidated into a smaller system which had enough organization to be classified as a tropical depression, the seventh of the season.  The depression moved north-northwest into northeast Mexico on the afternoon of September 7, dissipating completely on September 8.<ref name="cliqr"/>
{{Clear}}

===Hurricane Diana===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Diana 1984 satellite image at peak.jpg
|Track=Diana 1984 track.png
|Formed=September 8
|Dissipated=September 16
|1-min winds=115
|Pressure=949
}}
{{Main|Hurricane Diana (1984)}}
On September 8, an [[extratropical cyclone]] organized into Tropical Storm Diana north of the [[Bahamas]]. Diana proved difficult for meteorologists to forecast, initially moving westward towards [[Cape Canaveral]], but then turned to the north and paralleled the coastline.<ref name="PrelimRPT1">{{cite web|author=Harold P. Gerrish|date=October 18, 1984|accessdate=December 9, 2008|title=Preliminary Report: Hurricane Diana|page=1|publisher=[[National Hurricane Center]]|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/diana/prelim01.gif}}</ref> On September 11, the storm reached hurricane strength, and continued to intensify to a [[Saffir-Simpson Hurricane Scale|Category 4 hurricane]]. Diana moved north-northeast, and performed a small anti-cyclonic loop before striking near [[Cape Fear (headland)|Cape Fear]] as a minimal Category&nbsp;2 hurricane on September 13. A weakened Tropical Storm Diana curved back out to sea and headed northeast until it became extratropical near Newfoundland on September 16.{{Atlantic hurricane best track}}

Damage estimates were set at $65.5&nbsp;million. Three indirect deaths were associated with Diana. Diana was the first hurricane to strike a nuclear power plant&nbsp;— the Carolina Power and Light Brunswick Nuclear Power Plant recorded sustained hurricane-force winds, but there was no damage to the facility.<ref name="PrelimRPT3">{{cite web| author=Harold P. Gerrish| date=October 18, 1984| title=Preliminary Report: Hurricane Diana|page=3|publisher=[[National Hurricane Center]]| accessdate=December 9, 2008|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/diana/prelim03.gif}}</ref>
{{Clear}}

===Tropical Storm Edouard===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Edouard (1984).JPG
|Track=Edouard 1984 track.png
|Formed=September 14
|Dissipated=September 15
|1-min winds=55
|Pressure=998
}}
The origins of Tropical Storm Edouard are unclear, but an area of persistent organized storms formed in the [[Bay of Campeche]], which strengthened into a tropical storm on September 14. Edouard rapidly intensified, with wind speeds reaching 65&nbsp;mph (100&nbsp;km/h) in 18&nbsp;hours. Following its strengthening, Edouard dissipated even more quickly, degenerating into an area of thunderstorms the next day.<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/edouard/prelim01.gif|title=Preliminary Report: Tropical Storm Edouard – 13 to 15 September 1984|publisher=[[National Hurricane Center]]|page=1|year=1984|accessdate=March 6, 2010}}</ref> The remnants of Edouard moved over land near the port of [[Veracruz, Veracruz|Veracruz]].<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/edouard/prelim02.gif|title=Preliminary Report: Tropical Storm Edouard – 13 to 15 September 1984|publisher=[[National Hurricane Center]]|page=2|year=1984|accessdate=March 6, 2010}}</ref>
{{Clear}}

===Tropical Storm Fran===
{{Main|Tropical Storm Fran (1984)}}
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Fran (1984).JPG
|Track=Fran 1984 track.png
|Formed=September 15
|Dissipated=September 20
|1-min winds=55
|Pressure=994
}}
On September 14, a well-defined tropical wave exited the coast of Africa. The next day, it had rapidly organized into a tropical depression. On the afternoon of September 16 the depression attained tropical storm strength, and it was given the name Fran. It turned to the northwest, and passed very near the [[Cape Verde]].<ref name="nhc report">{{cite web|title=Nation Hurricane Center Report|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/fran/prelim01.gif|publisher=National Hurricane Center|accessdate=September 18, 2010}}</ref> 31 people were killed in the country.<ref>{{cite web|title=The Deadliest Atlantic Tropical Cyclones|url=http://www.nhc.noaa.gov/pastdeadlyapp1.shtml?|publisher=National Hurricane Center|accessdate=September 19, 2010}}</ref> Fran continued between the northwest and west-northwest on September 17–18 as it continued to organize. During this period satellite imagery indicated that Fran peaked with winds of {{convert|65|mph|km/h|abbr=on}} and a minimum surface pressure of {{convert|994|mbar|inHg|abbr=on|lk=on|sigfig=4}}. As Fran passed the Cape Verde islands weather stations reported {{convert|35|mph|km/h}} winds, which is tropical depression force. During the period of September 19–20 Fran turned towards westward and began to encounter strong [[Wind shear|upper level wind shear]], which caused Fran to dissipate on September 20.<ref name="nhc report" />
{{Clear}}

===Tropical Storm Gustav===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Gustav (1984).JPG
|Track=Gustav 1984 track.png
|Formed=September 16
|Dissipated=September 19
|1-min winds=45
|Pressure=1006
}}
Gustav spent most of its life as a well-organized tropical depression, which formed on September 16 in the open Atlantic south of [[Bermuda]]. The depression moved north, and its motion stalled over [[Bermuda]] on September 17. A day later, the depression had strengthened to a tropical storm and was named Gustav. Tropical Storm Gustav headed northeast until it was absorbed by a front on September 19.{{Atlantic hurricane best track}}
{{clear}}

===Hurricane Hortense===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Hortense (1984).JPG
|Track=Hortense 1984 track.png
|Formed=September 23
|Dissipated=October 2
|1-min winds=65
|Pressure=993
}}
A large frontal system spawned a subtropical depression early on September&nbsp;23, about 385&nbsp;miles (620&nbsp;km) east of Bermuda. Ship and satellite data confirmed its development, and indicated the system intensified into a subtropical storm later on September&nbsp;23. Initially the cyclone moved toward the south-southwest, although on September&nbsp;24 it turned to the west. That day, the [[Hurricane Hunters]] reported that the system transitioned into a tropical cyclone; as such, it was named Tropical Storm Hortense. The newly-tropical storm quickly intensified while turning to the northwest, and late on September&nbsp;25 Hortense attained hurricane status, about 300&nbsp;miles (475&nbsp;km) southeast of Bermuda.<ref name="tcr1"/>

Twelve hours after reaching hurricane status, Hortense began a sharp weakening trend while passing east of Bermuda. By September&nbsp;27 it was a minimal tropical storm, and subsequently it executed a clockwise loop to the southwest. The intensity of Hortense fluctuated slightly over the subsequent few days, although it never regained its former intensity. On September&nbsp;30, after turning to the west and later to the north, the storm passed just 7&nbsp;mi (11&nbsp;km) west of Bermuda. As the storm was so weak, the island only reported winds of 18&nbsp;mph (30&nbsp;km/h).<ref name="tcr1"/> Hortense accelerated to the northeast, moving rapidly across the north Atlantic before being absorbed by a larger extratropical storm late on October&nbsp;2, northwest of the Azores.<ref name="mwr">{{cite journal|author=Miles B. Lawrence and Gilbert B. Clark|date=July 1985|title=Atlantic Hurricane Season of 1984|journal=Monthly Weather Review|publisher=National Oceanic and Atmospheric Administration|volume=113|accessdate=December 11, 2010|url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1984.pdf|format=PDF| archiveurl= https://web.archive.org/web/20101207002351/http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1984.pdf| archivedate=December 7, 2010 <!--DASHBot-->| deadurl= no | doi = 10.1175/1520-0493(1985)113<1228:ahso>2.0.co;2| pages=1228–1237|bibcode = 1985MWRv..113.1228L }}</ref>
{{Clear}}

===Tropical Storm Isidore===
{{Main|Tropical Storm Isidore (1984)}}
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Isidore (1984).JPG
|Track=Isidore 1984 track.png
|Formed=September 25
|Dissipated=October 1
|1-min winds=50
|Pressure=999
}}
A tropical depression formed on September 25 off the southeastern [[Bahamas]]. The  depression headed west, and was upgraded to a tropical storm in the central Bahamas on September 26. It struck the US coast near [[Jupiter, Florida]]. Retaining tropical storm strength, Isidore curved to the northeast, emerging over water near [[Jacksonville, Florida]]. Isidore continued northeast until it was absorbed by a front on October 1.{{Atlantic hurricane best track}} Total damages were estimated at over $750,000 (1984 US dollars). One death from electrocution was reported.<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/isidore/prelim04.gif|author=Harrold P. Gerrish|title=Preliminary Report: Tropical Storm Isidore – 25 September to 1 October 1984|date=October 29, 1984|page=4|accessdate=March 6, 2010|publisher=[[National Hurricane Center]]}}</ref>
{{clear}}

===Hurricane Josephine===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Josephine Oct 12 1984 2025Z.png
|Track=Josephine 1984 track.png
|Formed=October 7
|Dissipated=October 18
|1-min winds=90
|Pressure=965
}}
{{main|Hurricane Josephine (1984)}}
Josephine became a named storm on October 8 while northeast of [[Puerto Rico]]. It briefly moved west then turned almost due north. While it stayed well away from the U.S. coast, Josephine was a large storm and sustained tropical storm winds were measured at the [[Diamond Shoals]] of [[Cape Hatteras]]. When it passed 36°N latitude (roughly level with [[Norfolk, Virginia]]), Josephine curved to the southeast, then back to the northeast. It continued on this path until it made a cyclonic loop beginning on October 17 while becoming [[extratropical]]. The storm lost its identity on October 21.{{Atlantic hurricane best track}} The hurricane caused wave damage to coastal areas, but primarily posed a threat to the shipping lanes of the North Atlantic.<ref>{{cite web|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/josephin/prelim01.gif|title=Preliminary Report: Hurricane Josephine – 7 to 21 October 1984|author=Robert Sheets|date=October 27, 1984|accessdate=March 6, 2010|publisher=[[National Hurricane Center]]}}</ref>

Offshore, a sailboat with six crewmen on it became disabled due to high waves, estimated to have exceeded {{convert|15|ft|m|abbr=on}}, produced by the hurricane. All of the people on the ship were quickly rescued after issuing a distress signal by a nearby tanker vessel.<ref name="TD1">{{cite web|author=Associated Press|publisher=The Day|date=October 11, 1984|accessdate=January 22, 2010|title=Hurricane Josephine scaring mariners now|url=https://news.google.com/newspapers?id=GnUfAAAAIBAJ&sjid=DHIFAAAAIBAJ&pg=4961,2471264&dq=hurricane+josephine+1984&hl=en}}</ref> In [[Massachusetts]], one man drowned after falling off his boat on North River amidst large swells produced by the storm. In [[Long Island]], New York and parts of [[New Jersey]], tides between {{convert|2|and|4|ft|m|abbr=on}} above normal resulted in minor coastal flooding.<ref name="TFLS1">{{cite news|author=Associated Press|work=The Free Lance-Star|date=October 15, 1984|accessdate=January 25, 2010|title=Hurricane Josephine moves away from land|url=https://news.google.com/newspapers?id=8-oQAAAAIBAJ&sjid=-YsDAAAAIBAJ&pg=5409,2218270&dq=hurricane+josephine&hl=en}}</ref>

{{clear}}

===Tropical Depression Seventeen===
{{Infobox Hurricane Small
|Basin=Atl
|Image=TD 27 oct 1984 2100Z.JPG
|Track=Oct1984tdfilledrainblk.gif
|Formed=October 25
|Dissipated=October 28
|1-min winds=30
|Pressure=1013
}}
This system was recognized as the seventeenth tropical depression of the season by the National Hurricane Center after the season ended.<ref name="cliqr"/>  A retrograding [[cold-core low|upper-level low]] spurred the development of a low east of the Bahamas on October 25. The system tracked westward with limited shower and thunderstorm activity, crossing Florida on October 26 before moving into the Gulf of Mexico.  Once the system moved into the north-central Gulf, deep convection began to develop near its center, expanding in intensity and coverage near and after landfall in extreme southeast Mississippi.  The small system accelerated rapidly to the north and northeast ahead of an approaching cold front, moving across the Tennessee Valley and central Appalachians before linking up with the front and becoming a weak [[extratropical cyclone]].  The nontropical cyclone then moved through coastal New England.<ref name="cliqr"/><ref>{{cite web|author=David M. Roth|date=May 11, 2008|url=http://www.wpc.ncep.noaa.gov/tropical/rain/oct1984td.html|title=Tropical Depression: October 25–29, 1984|publisher=[[Hydrometeorological Prediction Center]]|accessdate=November 13, 2008| archiveurl= https://web.archive.org/web/20081005044526/http://www.wpc.ncep.noaa.gov/tropical/rain/oct1984td.html| archivedate=October 5, 2008 <!--DASHBot-->| deadurl= no}}</ref>
{{Clear}}

===Hurricane Klaus===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Klaus (1984).JPG
|Track=Klaus 1984 track.png
|Formed=November 5
|Dissipated=November 13
|1-min winds=80
|Pressure=971
}}
{{Main|Hurricane Klaus (1984)}}
Forming from a broad area of low pressure on November 5, Klaus maintained a northeast movement throughout much of its path. After making landfall on extreme eastern [[Puerto Rico]], it passed to the north of the [[Leeward Islands]], resulting in strong southwesterly winds and rough seas. Klaus attained hurricane status and reached peak winds of 90&nbsp;mph (145&nbsp;km/h) before becoming extratropical over cooler waters on November 13.<ref name="Klaustcr1">{{cite web|publisher=[[National Hurricane Center]]|year=1984|title=Hurricane Klaus Preliminary Report Page One|accessdate=April 13, 2010|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/klaus/prelim01.gif}}</ref> The storm dropped heavy rainfall in Puerto Rico, causing minor flooding and light damage. Klaus caused heavy marine damage in the Leeward Islands, including wrecking at least three ships. The Virgin Islands experienced heavy damage, as well.  Damage from the storm totaled to $152&nbsp;million (1984&nbsp;USD), and the hurricane killed two on Dominica.<ref name="Klaustcr2">{{cite web|publisher=[[National Hurricane Center]]|year=1984|title=Hurricane Klaus Preliminary Report Page Two|accessdate=October 21, 2006|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/klaus/prelim02.gif}}</ref>
{{Clear}}

===Tropical Depression Nineteen===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Surface analysis of cyclone November 23, 1984.jpg
|Track=
|Formed=November 22
|Dissipated=November 29
|1-min winds=30
|Pressure=1005
}}
{{main|Late November 1984 Nor'easter}}
A low pressure system formed east of [[Florida]] on November 22 and rode up the [[East Coast of the United States]] producing heavy rain before curving back out to sea and dissapating on November 26. The storm left one fatality and $7.4 million (1984 USD) in damage. There has been evidence that the November storm may have become a [[subtropical cyclone]] east of [[Bermuda]]. The remnants of the cyclone contributed to the [[Late November 1984 Nor'easter]].
{{clear}}

===Hurricane Lili===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Lili (1984).JPG
|Track=Lili 1984 track.png
|Formed=December 12
|Dissipated=December 24
|1-min winds=70
|Pressure=980
}}
{{Main|Hurricane Lili (1984)}}
Hurricane Lili was one of only four [[North Atlantic tropical cyclone|Atlantic tropical cyclone]]s on record to reach hurricane status in the month of December.{{Atlantic hurricane best track}}<ref name="EpsilonTCR">{{
cite web
| author=National Hurricane Center
| year=2006
| title=Tropical Cyclone Report: Hurricane Epsilon
| publisher=[[National Oceanic and Atmospheric Administration]]
| accessdate=February 14, 2006
| url={{NHC TCR url|id=AL302005_Epsilon}}
| format=PDF
}}</ref>  Lili developed as a [[subtropical cyclone]] which originated from a [[surface weather analysis|frontal trough]] to the south of [[Bermuda]] on December 12. It tracked southeastward,<ref>{{cite web|publisher=[[National Hurricane Center]]|year=1984|title=Hurricane Lili Preliminary Report Page 1|accessdate=April 13, 2010|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/lili/prelim01.gif}}</ref> then northward, slowly attaining [[tropical cyclogenesis|tropical characteristics]] and becoming a hurricane on December 20. Lili turned to the south and southwest, briefly threatening the northern [[Caribbean Sea|Caribbean]] islands before weakening and dissipating near the coast of the [[Dominican Republic]].  Lili was the longest lasting tropical cyclone outside of the [[List of Atlantic hurricane seasons|Atlantic hurricane season]], as well as the strongest hurricane to form during the month of December. It briefly threatened to pass through the [[Leeward Islands]] as a minimal hurricane, though upon passing through the area as a dissipating tropical depression Lili produced light rainfall and no reported damage.<ref name="tcr2">{{cite web|publisher=[[National Hurricane Center]]|year=1984|title=Hurricane Lili Preliminary Report Page 2|accessdate=March 31, 2007|url=http://www.nhc.noaa.gov/archive/storm_wallets/atlantic/atl1984-prelim/lili/prelim02.gif}}</ref>
{{Clear}}

==Storm names==
The following names were used for named storms that formed in the north Atlantic in 1984. No names were retired, so the same list of names was used again in the [[1990 Atlantic hurricane season|1990 season]].<ref>{{cite web|url=http://www.nhc.noaa.gov/retirednames.shtml|title=Retired Hurricane Names Since 1954|author=[[National Hurricane Center]]|publisher=[[National Oceanic and Atmospheric Administration]]|year=2009|accessdate=September 13, 2009}}</ref> This is the first time these names were used since the post-1978 naming change, except for Bertha and Fran which were previously used in 1957 and 1973.{{Atlantic hurricane best track}} Names that were not assigned are marked in {{tcname unused}}.
{| width="80%" clear="both"
|
* Arthur
* Bertha
* Cesar
* [[Hurricane Diana (1984)|Diana]]
* Edouard
* [[Tropical Storm Fran (1984)|Fran]]
* Gustav
|
* Hortense
* [[Tropical Storm Isidore (1984)|Isidore]]
* [[Hurricane Josephine (1984)|Josephine]]
* [[Hurricane Klaus (1984)|Klaus]]
* [[Hurricane Lili (1984)|Lili]]
* {{tcname unused|Marco}}
* {{tcname unused|Nana}}
|
* {{tcname unused|Omar}}
* {{tcname unused|Paloma}}
* {{tcname unused|Rene}}
* {{tcname unused|Sally}}
* {{tcname unused|Teddy}}
* {{tcname unused|Vicky}}
* {{tcname unused|Wilfred}}
|}

===Retirement===
{{See also|List of retired Atlantic hurricane names}}
The [[World Meteorological Organization]] did not retire any names used in the 1984 season.

==See also==
{{Portal|Tropical cyclones}}
*[[List of Atlantic hurricanes]]
*[[List of Atlantic hurricane seasons]]
*[[1984 Pacific hurricane season]]
*[[1984 Pacific typhoon season]]
*[[1984 North Indian Ocean cyclone season]]
*Southern Hemisphere tropical cyclone seasons: [[1983–84 Southern Hemisphere tropical cyclone season|1983–84]], [[1984–85 Southern Hemisphere tropical cyclone season|1984–85]]

==References==
{{Reflist|2}}

==External links==
* [http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1984.pdf Monthly Weather Review]
* [http://www.wpc.ncep.noaa.gov/tropical/rain/1984.html U.S. Rainfall information for tropical cyclones from 1984]

{{1984 Atlantic hurricane season buttons}}
{{TC Decades|Year=1980|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1984 Atlantic Hurricane Season}}
[[Category:1984 Atlantic hurricane season| ]]
[[Category:Articles which contain graphical timelines]]