{{good article}}
{{Infobox University Boat Race
| name= 122nd Boat Race
| image = 
| caption = 
| winner = Oxford
| margin = 6 and 1/2 lengths
| winning_time= 16 minutes 58 seconds
| date= {{Start date|1976|03|20|df=y}}
| umpire = [[Farn Carpmael|P. N. Carpmael]]<br>(Cambridge)
| prevseason= [[The Boat Race 1975|1975]]
| nextseason= [[The Boat Race 1977|1977]]
| overall =68&ndash;53
| reserve_winner=Isis
| women_winner=Oxford
}}
The 122nd [[The Boat Race|Boat Race]], an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]], took place on 20 March 1976 and was won by Oxford by six-and-a-half lengths in 16 minutes 58 seconds, the fastest time in the history of the race.  The race was umpired by former Cambridge rower [[Farn Carpmael]]. It was the first race in the event for which an official weigh-in was held, and featured the heaviest rower ever in Steve Plunkett.

Oxford's Isis won the 12th running of the reserve race against Cambridge's Goldie, in a record time of 17 minutes 37 seconds.  In the 31st [[Women's Boat Race]], Oxford defeated Cambridge.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 April 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 24 July 2014 | publisher = The Boat Race Company Ltd}}</ref>  The rivalry is a "hotly contested point of honour" between the two universities.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=7 April 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1975|1975 race]] by five-and-a-half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher = The Boat Race Company Ltd| title = Boat Race – Results| accessdate = 30 August 2014}}</ref> and led overall with 68 victories to Oxford's 52 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Ltd| title = Classic moments – the 1877 dead heat | accessdate = 6 June 2014}}</ref>  The umpire for the race was [[Farn Carpmael]], who had rowed for Cambridge in [[The Boat Race 1930|1930]] and [[The Boat Race 1931|1931 races]].<ref>Burnell pp. 49, 73</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

By invitation from the Oxford boat club president Graham Innes,<ref>Dodd, p. 240</ref> Oxford were coached by former [[Blue (university sport)|Blue]] [[Daniel Topolski|Dan Topolski]] while Cambridge were led by Czechoslovakian former international rower [[Bohumil Janoušek]] (more commonly known as Bob Janousek).<ref>{{Cite news | title = Crews race to become the underdogs| first = Jim | last = Railton | date = 21 March 1976 | page = 10 | issue = 59651| work = [[The Times]]}}</ref>  Preparations during the week before the race were underwhelming for both crews.  Oxford's performance against a [[University of London]] crew was described by Jim Railton of ''[[The Times]]'' as "abysmal" while Cambridge "disgraced themselves" in a subsequent two-and-a-half-length defeat by their own reserve crew, Goldie.<ref name=pedigree/>  Both crews had faced the "Lubrication Laboratory" crew from [[Imperial College London]], a "hotbed of rowing",<ref>{{Cite book | url = https://books.google.com/books?id=x4u4ikoj1M8C&pg=PA534&lpg=PA534&dq=lubrication+laboratory+rowing | title = The History of Imperial College London, 1907–2007 | first = Hannah | last = Gay | date = February 2007 | isbn = 9781860947087}}</ref> in the lead-up to the official race, but changes in personnel and differences in conditions and race lengths did not demonstrate a clear favourite.<ref>{{ Cite news | title = Vultures leave Cambridge with a morsel of hope| first = Jim | last = Railton | work = [[The Times]] | issue = 59655| date = 17 March 1976}}</ref>

The day before the race, British bookmaking company [[Ladbrokes]] announced that they would sponsor the race from the following year.  From 1977, each boat club would be awarded £10,000 and would compete for The Ladbroke Cup.<ref>{{Cite news | title = Ladbroke Group will sponsor the Boat Race with £20,000 from next year | first = Jim | last = Railton | work = [[The Times]] | date = 20 March 1976 | page = 1 | issue = 59658}}</ref>

==Crews==
For the first time in the history of the event, an official weigh-in was held, organised by ''[[The Sunday Times]]'' and weighing machine manufacturers [[W & T Avery Ltd.]]<ref>Burnell, p. 53</ref>  Oxford's crew was the heaviest of all time, at an average of just under 14&nbsp;[[Stone (unit)|st]] 1&nbsp;[[Pound (mass)|lb]] (89.0&nbsp;kg) per rower, {{convert|4.5|lb|kg}} more than their opposition,<ref name=pedigree/> and the first time in the race history, a crew weighed more than an average of 14&nbsp;st (88.7&nbsp;kg).<ref>Burnell, p. 44</ref>  The Dark Blue crew also included Steve Plunkett who, at 16&nbsp;st 5&nbsp;lb (103.6&nbsp;kg) was the heaviest rower in the history of the race.<ref name=pedigree/>  The Cambridge crew were inexperienced, their only Blue being the [[Cambridge University Boat Club]] president [[Henry Clay (rower)|Henry Clay]] who had rowed in the [[The Boat Race 1975|1975 race]].  In contrast, Oxford welcomed back five of their 1975 crew including [[Coxswain|cox]] Ashton Calvert, and also included two from the victorious 1975 Isis crew.<ref name=pedigree/>  Two non-British rowers participated, Americans Dick Cashin (of Harvard) for Cambridge and Ken Brown (of Cornell) for Oxford.<ref>Burnell, p. 39</ref>

{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] ||D. R. H. Beak|| [[Oriel College, Oxford|Oriel]] || 13 st 6 lb || D. J. Searle || [[St Catharine's College, Cambridge|St Catharine's]] || 12 st 7 lb
|-
| 2 || G. S. Innes (P) || [[Oriel College, Oxford|Oriel]] || 13 st 10 lb || R. R. A. Breare || [[Pembroke College, Cambridge|Pembroke]] || 14 st 6 lb
|-
| 3 || A. D. Edwards || [[St Peter's College, Oxford|St Peters]] || 13 st 0 lb || M. R. Gritten || [[Queens' College, Cambridge|Queens']] || 14 st 0 lb
|-
| 4 || R. S. Mason || [[Keble College, Oxford|Keble]] || 13 st 6 lb || M. P. Wells || [[Selwyn College, Cambridge|Selwyn]] || 14 st 12 lb
|-
| 5 || S. G. H. Plunkett || [[The Queen's College, Oxford|Queen's]] || 16 st 3 lb || P. B. Davies || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 14 st 1 lb
|-
| 6 || K. C. Brown || [[Christ Church, Oxford|Christ Church]] || 14 st 5 lb || R. M. Cashin || [[Trinity College, Cambridge|1st & 3rd Trinity]] || 14 st 12 lb
|-
| 7 || A. J. Wiggins || [[Keble College, Oxford|Keble]] || 13 st 5 lb || [[Henry Clay (rower)|J. H. Clay]] (P) || [[Pembroke College, Cambridge|Pembroke]] || 12 st 11 lb
|-
| [[Stroke (rowing)|Stroke]] ||  A. G. H. Baird|| [[Christ Church, Oxford|Christ Church]] || 12 st 10 lb || R. Harpum || [[Jesus College, Oxford|Jesus]]  || 12 st 4 lb
|-
| [[Coxswain (rowing)|Cox]] || J. N. Calvert || [[St Edmund Hall, Oxford|St Edmund Hall]] || 9 st 4 lb || J. P. Manser || [[Sidney Sussex College, Cambridge|Sidney Sussex]]  || 9 st 5 lb
|-
!colspan="7"|Source:<ref name=pedigree>{{Cite news | title = Oxford have a winning pedigree | first = Jim | last = Railton | date = 20 March 1976 | page = 6 | work = [[The Times]] | issue = 59658}}</ref><br> (P) &ndash; boat club president<ref>Burnell, pp. 51&ndash;52</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg |right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss and elected to start from the Surrey station.<ref>Burnelll, p. 83</ref>  Leading from the outset, and in calm river conditions, Oxford were a length ahead within a minute.  Four lengths up by the Mile Post, which they reached in a record 3 minutes 35 seconds,<ref name=inarow/> the Dark Blues reduced their rating.<ref name=disaster/> Further milestone records were broken at [[Hammersmith Bridge]], Chiswick Steps and [[Barnes Railway Bridge|Barnes Bridge]].<ref name=inarow/>  Oxford reduced their rating but still extended their lead to six lengths by the finishing post, in a record-breaking time of 16 minutes 58 seconds, 37 seconds quicker than their [[The Boat Race 1974|1974 colleagues]] who had previously held the record.<ref name=disaster>{{Cite news | title = Competitive disaster |first = John | last = Rodda|work = [[The Guardian]] | date = 22 March 1976 | page = 15}}</ref>  Although Cambridge also beat the existing record, they finished a distant 22 seconds and six-and-a-half lengths behind.<ref name=inarow/>

==Reaction==
Oxford coach Topolski said of his successful crew: "they got three [lengths] up, then they really sat on it and cruised.  The enjoyed the row.  It's a natural thing when the other crew is so far behind."<ref name=inarow>{{Cite news | title = Oxford records all in a row | |first = Julie |last = Welch | page =23 | work = [[The Observer]] | date = 21 March 1976}}</ref>  He was critical of the Cambridge crew: "They didn't really have a lot of talent."<ref name=inarow/>

The official winning distance caused controversy.  A discrepancy between the official margin (six-and-a-half lengths) and those reported in various British newspapers, including ''The Sunday Times'', ''[[The Daily Telegraph]]'' and ''[[The Times]]'' resulted in a delay in bookmakers paying out on winning bets.<ref>{{Cite news | title = Lengthy calculations left in the distance | first = Jim  | last = Railton |work = [[The Times]] | date = 23 March 1976 | page = 10 | issue = 59660}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0091513405 | publisher =Stanley Paul |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1976}}

[[Category:The Boat Race]]
[[Category:1976 in English sport]]
[[Category:1976 in rowing]]
[[Category:March 1976 sports events]]