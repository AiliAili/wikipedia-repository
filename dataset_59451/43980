<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= HF118
 |image=GE Honda HF120 200811.jpg 
 |caption= 
}}
{{Infobox Aircraft Engine
 |type= [[Turbofan]] 
 |national origin = [[Japan]] / [[United States]]
 |manufacturer= [[GE Honda Aero Engines]] 
 |first run= 2009<ref>{{cite press release |url= http://www.geaviation.com/press/business_general/bus_20091019.html |title= GE Honda's HF120 Engine Completes First Run Test |publisher= GE Aviation |date= October 19, 2009}}</ref>
 |major applications= [[Honda HA-420 HondaJet]]
 |number built =  
 |program cost = 
 |unit cost = 
 |developed from = 
 |developed into = 
 |variants with their own articles = 
}}
|}

The '''GE Honda HF120''' is a small [[turbofan]] for the light-[[business jet]] market, the first engine to be produced by [[GE Honda Aero Engines]].

==Development==
[[File:HondaJet 005.jpg|thumb|An HF120 engine mounted above the wing of a [[Honda HA-420 HondaJet]]]]

Succeeding Honda's original '''HF118''' prototype, the HF120 was undergoing testing in July 2008, with certification targeted for late 2009.<ref>{{cite press release |url= http://www.geaviation.com/press/gehonda/gehonda_20080728.html |title= Engine Prototyping prepares GE Honda Aero Engines for certification tests |publisher= GE Aviation Press Release |date= July 28, 2008}}</ref> The first engines were produced at GE's factory, but in November 2014 production shifted to [[Burlington, North Carolina]].<ref>{{cite news |url= http://www.ainonline.com/aviation-news/2014-11-12/ge-honda-transitions-hf120-production-n-carolina |title= GE Honda Transitions HF120 Production to N. Carolina |work= Aviation International News |date= 12 November 2014 |author= Curt Epstein}}</ref> The U.S. [[Federal Aviation Administration]] (FAA) awarded Part 33 certification to the HF120 turbofan engine in December 2013, and production certification in 2015.<ref>{{cite news |author= Chad Trautvetter |url= http://www.ainonline.com/aviation-news/business-aviation/2015-03-18/ge-honda-wins-faa-production-certificate-hf120-engine |title= GE Honda Wins FAA Production Certificate for HF120 Engine |work= Aviation International News |date=18 March 2015}}</ref>

== Design ==

The engine has a wide-chord swept fan, two-stage low-pressure compressor and counter rotating high-pressure compressor based on a titanium impeller, for a {{convert|2,050|lbf|kN|abbr=on}} takeoff thrust.<ref>{{cite web|url=http://www.flightglobal.com/directory/searchresults.aspx?navigationItemId=382&manufacturerType=Engine&searchMode=manufacturer&Keyword=&Manufacturer=22635&GOADVANCEDSEARCH=SEARCH |title=GE Honda Aero Engines |work=Flight International |deadurl=unfit |archiveurl=https://web.archive.org/web/20110811022133/http://www.flightglobal.com/directory/searchresults.aspx?navigationItemId=382&manufacturerType=Engine&searchMode=manufacturer&Keyword=&Manufacturer=22635&GOADVANCEDSEARCH=SEARCH |archivedate=August 11, 2011 }}</ref> The HF120 engine’s components interact with greater efficiency by incorporating 3D aerodynamic design and its effusion-cooled combustor design emits few NOx, CO and HC. Noise levels are quieter than Stage 4 requirements.<ref>{{cite web |url= http://gehonda.com/engine/ |title= HF120 Engine |publisher= GE Honda Aero Engines}}</ref>

In May 2016 [[time between overhaul]] was 2,500 h and should mature to 5,000 h ; a midlife hot-section inspection isn't required and it should remain on wing 40% longer than other engines and have lower operating costs : Honda maintenance plan should run at $139 per engine per hour.<ref>{{cite news |url= http://aviationweek.com/bca/ge-honda-hf120-h1a-turbofans |title= GE Honda HF120-H1A Turbofans |date= May 26, 2016 |author= Fred George |work= Business & Commercial Aviation |publisher= Aviation Week}}</ref>

== Applications ==

The engine is offered as a retrofit to the [[Cessna CitationJet]] CJ1 by [[Sierra Industries]],<ref>{{cite web |author= Chad Trautvetter |url= http://www.ainonline.com/aviation-news/ainalerts/2014-09-18/sierra-selects-ge-honda-hf120-sapphire-citationjet-upgrade-program |title= Sierra Selects GE Honda HF120 for Sapphire CitationJet Upgrade Program |work= Aviation International News |date= 18 September 2014}}</ref> in partnership with GE Honda, besides the [[Hondajet]].<ref>{{citation |author= Dan Parsons |url= https://www.flightglobal.com/news/articles/nbaa-ge-honda-launches-cessna-jet-engine-retrofit-programme-404987/ |title= GE Honda launches Cessna jet engine retrofit programme |work=[[Flightglobal]] |date= 20 October 2014}}</ref>

==Specifications==

{{jetspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref= Honda<ref name="HF120">{{cite web |title=  HF120 Turbofan Engine |publisher= Honda Worldwide |url= http://world.honda.com/HondaJet/Background/TurbofanEngine/ |accessdate= July 2016}}</ref>
|type= [[Turbofan]] engine
|length= {{convert|44|in|cm}}
|diameter=maximum: {{convert|21.2|in|cm}}
|weight=<{{convert|400|lb|abbr=on}}
|compressor= One wide-chord fan, two axial low-pressure stages, one centrifugal high-pressure stage.
|combustion=compact reverse flow
|turbine= One axial high-pressure stage, two axial low-pressure stages.
|fueltype=
|oilsystem=
|thrust=takeoff thrust: {{convert|2,050|lbf|kN|abbr=on}}
|compression=24
|bypass=2.9
|aircon=
|turbinetemp=
|fuelcon=
|specfuelcon=<{{convert|0.7|lb/lbf/h|g/kN/s|abbr=on}}
|thrust/weight=>5
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{reflist}}

==External links==
{{Commons category}}
{{official website|http://www.gehonda.com/engine/}}

{{Joint development aeroengines}}

[[Category:Medium-bypass turbofan engines]]
[[Category:Honda engines]]
[[Category:Turbofan engines 2000–2009]]
[[Category:Centrifugal-flow turbojet engines]]