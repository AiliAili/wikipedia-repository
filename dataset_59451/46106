{{Infobox automobile
| name         = Alfa Romeo Giulietta Sprint Speciale<br>Alfa Romeo Giulia Sprint Speciale
| image        = 1964 Alfa Romeo Giulia Sprint Speciale - green - fvr.jpg
| caption      = Alfa Romeo Giulia Sprint Speciale
| manufacturer = [[Alfa Romeo]]
| aka          = Giulietta SS<br>Giulia SS
| production   = 1959&ndash;1966
| assembly     = 
| designer     = [[Franco Scaglione]] at [[Gruppo Bertone|Bertone]]
| class        = [[Sports car]]
| body_style   = 2-door [[coupé]]
| layout       = [[Front-engine, rear-wheel-drive layout|Front-engine, rear-wheel-drive]]
| platform     = 
| related      = {{unbulleted list |[[Alfa Romeo Giulietta (750/101)|Alfa Romeo Giulietta Sprint]]|Alfa Romeo Giulietta Sprint Zagato|Alfa Romeo Giulia Sprint}}
| engine       = {{unbulleted list |1.3 L ''[[Alfa Romeo Twin Cam engine|AR 00120]]'' [[DOHC]] [[Inline-four engine|I4]]|1.6 L ''[[Alfa Romeo Twin Cam engine|AR 00121]]'' DOHC I4}}
| transmission = 5-speed manual
| wheelbase    = {{convert|2250|mm|in|1|abbr=on}}
| length       = {{convert|4120|mm|in|1|abbr=on}}
| width        = {{convert|1660|mm|in|1|abbr=on}}
| height       = {{convert|1245|mm|in|1|abbr=on}}
| weight       = {{convert|860|kg|lb|abbr=on}} (Giulietta)<br> {{Convert|950|kg|lb|abbr=on}} (Giulia)
| predecessor  = 
| successor    = 
| sp = uk
}}
The '''Alfa Romeo Giulietta Sprint Speciale''' (''Tipo 750 SS/101.20'', Italian for "Type 750 SS/101.20") and '''Alfa Romeo Giulia Sprint Speciale''' (''Tipo 101.21''), also known as '''Giulietta SS''' and '''Giulia SS''', are small [[sports car]]s manufactured by [[Alfa Romeo]] from 1959 to 1966.

==Giulietta Sprint Speciale==
===''Tipo 750 SS''===
[[File:SprintSpecial.jpg|thumb|left|"Low nose" Giulietta Sprint Speciale.]]
The first prototype of the Giulietta SS was presented in 1957 at the Turin Motor Show.<ref name="geraldo">{{Cite web|url=http://www.geraldo.at/geraldo/SS/history.htm|title=Sprint Speciale - History|accessdate=2012-08-25|work=geraldo.at}}</ref> After two more prototypes were presented in car shows, the official presentation of the production version for the press was on 24 June 1959 on the Monza race track.<ref name="geraldo"/> The first 101 cars produced had "low nose" and 750 SS designation. 100 cars minimum were needed to [[Homologation|homologate]] a car in FIA regulations. While there were some all-aluminium cars produced, the majority of these cars had steel bodies with aluminium doors, engine bonnet and boot lid. Also first cars were equipped with [[Weber carburetor|Weber]] 40 DCO3 [[carburettors]], later changed to 40 DCOE2.
The [[drag coefficient]] of the Sprint Speciale is 0.28,<ref name="hemmings">{{Cite web|url=http://www.hemmings.com/hmn/stories/2009/01/01/hmn_feature9.html|title=1959-'66 Alfa Romeo Sprint Speciale|accessdate=2012-08-25|work=hemmings.com}}</ref> the same as a [[Chevrolet Corvette (C6)]], and was not surpassed for more than twenty years. Cars used the 1,290&nbsp;cc [[Alfa Romeo Twin Cam engine]], a design with hemispheric combustion chambers and valves controlled directly by twin overhead camshafts.<ref name="supercars">{{Cite web|url=http://www.supercars.net/cars/1967.html|title=1963 Alfa Romeo Giulia Sprint Speciale|accessdate=2012-08-25|work=supercars.net}}</ref>

===''Tipo 101.20''===
[[File:1961 Alfa Romeo Giulietta Sprint Speciale dash.jpg|thumb|left|Dashboard of a 1961 Giulietta Sprint Speciale.]]
Small changes to a production version included steel doors, Weber 40 DCOE2 carburetors, higher front nose, removal of plexiglas windows. Bumpers were fitted front and rear, also cars had some minimal sound-proofing. With the 1290&nbsp;cc engine and {{convert|100|hp|kW|0|abbr=on}} of power the maximum speed was around {{Convert|200|km/h|mi/h|0|abbr=on}}. The 1.3 litre engine and gearbox was the same as used in race-oriented Giulietta Sprint Zagato. All Giuliettas SS had three-shoe drum brakes at front wheels and drum brakes at the rear. Export versions had 101.17 designation. Side badges had "Giulietta Sprint Speciale" script.
{{clear}}

==Giulia Sprint Speciale==
===Giulia Sprint Speciale Bertone Prototipo===
There was a prototype by Bertone of a replacement for Giulietta SS, named "'''Giulia SS Bertone Prototipo'''", but the new shape did not enter production and the next generation Giulia SS carried over an unchanged  Giulietta SS body.

<gallery caption="Giulia Sprint Speciale Bertone Prototipo" mode=packed>
File:Alfa Romeo Giulia SS Bertone prototype (7872528248).jpg|
File:Alfa Romeo Giulia SS Bertone prototype (7872533524).jpg|
File:Alfa Romeo Giulia SS Bertone prototype (7872538770).jpg|
</gallery>

===''Tipo 101.21''===
[[File:Alfa Romeo Giulia SS back.jpg|thumb|Alfa Romeo Giulia Sprint Speciale, rear view.]]

The bigger engine 1.6 L Giulia series replaced the Giulietta and was introduced at the March 1963 Geneva Motor Show.<ref name="geraldo" /> As Giulietta is the [[diminutive]] for Giulia in Italian, the new Giulia name was a wordplay hinting that the new car was a grown-up version of the Giulietta. In spite of a Giulia SS prototype, Alfa Romeo decided to retain the Giulietta-shaped SS in production. The 1,570cc engine made up to {{Convert|200|km/h|mi/h|abbr=on}} possible. The 1,570cc engine with Weber 40 DCOE2 carburetors was taken from Giulia Sprint Veloce and delivered {{convert|112|hp|kW|0|abbr=on}} of power. Most Giulia SS had disc brakes at front wheels. An easy way to distinguish the Giulia SS from the Giulietta SS is by the dashboard. The Giulia has a leather underside with the glovebox at a different angle than the main fascia. The dashboard in the Giulietta is sloping and painted in one colour without a leather underside. Side badges carried "Giulia SS" scripts.
Production ended in 1965, with a last single Sprint Speciale completed in 1966.{{sfn|Fusi|1978|pages=862–866}}

1,366 Giulietta Sprint Speciale and 1,400 Giulia Sprint Speciale were produced.{{sfn|Fusi|1978|pages=859 and 866}} 25 cars were converted to right hand drive by RuddSpeed.

==See also==
*[[Alfa Romeo Giulietta (750/101)]]
*[[Alfa Romeo Giulia]]
*[[SSZ Stradale]]

==Notes==
{{reflist}}

==References==
{{refbegin}}
* {{cite book |first=Luigi |last=Fusi |title=Alfa Romeo—Tutte le vetture dal 1910—All cars from 1910 |date=1978 |publisher=Emmeti Grafica editrice |location=[[Milan]] |edition=3rd |pages=549–551, 583–584, 857–859 and 862–866|ref={{sfnref|Fusi|1978}}}}
* {{cite book |first=R. M. |last=Clarke |title=Alfa Romeo Giulietta Gold Portfolio 1954-1965 |publisher=Brooklands Books |edition=rev. |date=22 January 2006 |page=149}}
* {{cite book |first=David |last=Owen |title=Alfa Romeo: Always With Passion |publisher=Haynes Publishing |edition=2nd |date=October 2004 |pages=61–62}}
{{refend}}

==External links==
{{commons category inline|Alfa Romeo Giulietta Sprint Speciale}}
*[http://www.veloceregister.net/ss1.html Sprint Speciale Register]

{{Alfa Romeo}}
{{Alfa Romeo Post War}}

[[Category:Alfa Romeo vehicles|Giulietta Sprint Speciale]]
[[Category:1960s automobiles]]
[[Category:Cars introduced in 1959]]
[[Category:Coupes]]
[[Category:Rear-wheel-drive vehicles]]