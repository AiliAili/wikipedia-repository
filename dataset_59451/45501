{{Infobox engineer
|image                =Artem Mikoyan.jpg
|alt                  =
|caption              =
|nationality          = [[Soviet Union]]
|birth_name                 = Artem Ivanovich Mikoyan
|birth_date           = {{OldStyleDate|5 August|1905|23 July}} 
|birth_place          = [[Sanahin]], [[Elisabethpol Governorate]], Russian Empire (present-day Armenia)
|death_date           = {{death date and age|1970|12|9|1905|8|5|df=y}}
|death_place          = Moscow, Soviet Union
|education            = [[Zhukovsky Air Force Academy]]
|spouse               = 
|parents              = 
|children             = 
|discipline           = [[Aeronautical Engineering]]
|institutions         = 
|practice_name        = 
|employer             = [[Mikoyan-Gurevich]] design bureau
|significant_projects = 
|significant_design   = [[Mikoyan-Gurevich MiG-1|MiG-1]]<br />[[Mikoyan-Gurevich MiG-3|MiG-3]]<br />[[Mikoyan-Gurevich MiG-15|MiG-15]] <br /> [[Mikoyan-Gurevich MiG-17|MiG-17]]<br />[[Mikoyan-Gurevich MiG-21|MiG-21]]<br />[[Mikoyan-Gurevich MiG-23|MiG-23]]<br />[[Mikoyan-Gurevich MiG-25|MiG-25]]
|significant_advance  = 
|significant_awards   = [[Hero of Socialist Labor]] (twice) <br /> [[Stalin Prize]] (1941, 1947, 1948, 1949, 1952, 1953)
}}

'''Artem (Artyom) Ivanovich Mikoyan''' ({{lang-ru|Артём Ива́нович Микоя́н}}; {{lang-hy|Արտյոմ (Անուշավան) Հովհաննեսի Միկոյան ''Artyom (Anushavan) Hovhannesi Mikoyan''}}; {{OldStyleDate|5 August|1905|23 July}} &ndash; 9 December 1970) was a [[Soviet Union|Soviet]] [[Armenians|Armenian]] [[aircraft]] designer. In partnership with [[Mikhail Gurevich (aircraft designer)|Mikhail Gurevich]] he designed many of the famous [[MiG]] military aircraft.

==Early life and career==
Mikoyan was born in [[Sanahin]], [[Armenia]] on 5 August 1905.<ref>[http://www.time.com/time/magazine/article/0,9171,809881-2,00.html The Survivor - TIME]</ref> His older brother, [[Anastas Mikoyan]], would become a senior Soviet politician. He completed his basic education and took a job as a machine-tool operator in [[Rostov]], then worked in the "Dynamo" factory in [[Moscow]] before being conscripted into the military.<ref name=armenian>{{cite journal|last1=Ambartsumian|first1=Victor|authorlink1=Victor Ambartsumian|title=Artem Mikoyan|journal=Հայկական սովետական հանրագիտարան (Soviet Armenian Encyclopedia)|date=1981|volume=7|page=542|url=https://hy.wikisource.org/wiki/%D4%B7%D5%BB:%D5%80%D5%A1%D5%B5%D5%AF%D5%A1%D5%AF%D5%A1%D5%B6_%D5%8D%D5%B8%D5%BE%D5%A5%D5%BF%D5%A1%D5%AF%D5%A1%D5%B6_%D5%80%D5%A1%D5%B6%D6%80%D5%A1%D5%A3%D5%AB%D5%BF%D5%A1%D6%80%D5%A1%D5%B6_%28Soviet_Armenian_Encyclopedia%29_7.djvu/542|language=Armenian}}</ref> After military service he joined the [[Zhukovsky Air Force Academy]], where he created his first plane, graduating in 1936.<ref name=zabecki>{{cite book|editor1-last=Zabecki|editor1-first=David T.|title=World War II in Europe: An Encyclopedia|date=2015|publisher=Routledge|isbn=113581242X|page=415|url=https://books.google.com/books?id=Mq_lCAAAQBAJ}}</ref> He worked with [[Polikarpov]] before being named head of a new aircraft design bureau in Moscow in December 1939. Together with [[Mikhail Gurevich (aircraft designer)|Mikhail Gurevich]], Mikoyan formed the [[Mikoyan-Gurevich]] design bureau, producing a series of fighter aircraft. In March 1942, the bureau was renamed OKB MiG (''Osoboye Konstruktorskoye Büro''), ANPK MiG (''Aviatsionnyy nauchno-proizvodstvennyy kompleks'') and OKO MiG. The [[MiG-1]] proved to be a poor start, the [[MiG-3]] went into production but only occasionally could it fight in its intended high-level [[interceptor aircraft|interceptor]] role. Further [[Mikoyan-Gurevich DIS|MiG-5]], [[Mikoyan-Gurevich MiG-7|MiG-7]] and [[Mikoyan-Gurevich MiG-8|MiG-8 ''Utka'']] did not progress beyond research prototypes.

==Jet aircraft designs==
[[File:Артем Иванович Микоян.jpg|thumbnail|left|210px|Artem Mikoyan]]
[[File:2014 Prowincja Lorri, Sanahin, Muzeum braci Mikojanów (06).jpg|thumb|Artem Mikoyan monument. Mikoyan Brothers Museum in Sanahin,]]
[[File:2014 Prowincja Lorri, Sanahin, Muzeum braci Mikojanów (01).jpg|thumbnail|Memorial to Artem Mikoyan in Sanahin]]
Early post-war designs were based on [[Alexander Yakovlevich Bereznyak|domestic works]] as well as captured German jet fighters and information provided by Britain or the US. By 1946, Soviet designers were still having trouble in perfecting the German-designed, axial-flow [[BMW 003#Post-war use|BMW 109-003]] jet engine — blueprints for the 109-003 turbojet had been seized by Soviet forces from the Basdorf-Zühlsdorf plant near Berlin and from the Central Works near [[Nordhausen]]. Production of the 003 was set up at the "Red October" GAZ 466 (''Gorkovsky Avtomobilny Zavod'', or Gorky Automobile Plant) in [[Leningrad]], where the 003 jet engine was mass-produced from 1947 under the designation RD-20 (''reactivnyi dvigatel'', or "jet drive").<ref>{{cite book | last = Albrecht | first = Ulrich | authorlink = | coauthors = | title = The Soviet Armaments Industry | publisher = Routledge | date = 1994 | location = | pages = | url = | doi = | id = | isbn = 3-7186-5313-3}}</ref> New Soviet airframe designs from their design bureaus, and near-sonic wing designs were threatening to outstrip development of the jet engines needed to power them. Soviet aviation minister [[Mikhail Khrunichev]] and aircraft designer [[Alexander Sergeyevich Yakovlev]] suggested to [[Joseph Stalin]] that the USSR buy advanced jet engines from the British. Stalin is said to have replied: "What fool will sell us his secrets?"<ref name=gordon>{{cite book|last1=Gunston|first1=Bill|last2=Gordon|first2=Yefim|title=MiG aircraft since 1937|date=1998|publisher=Naval Institute Press|page=56|url=https://books.google.com/books?id=Lc8hAQAAIAAJ}}</ref> However, he gave his assent to the proposal, and Artem Mikoyan, engine designer [[Klimov]], and other officials traveled to the United Kingdom to request the engines. To Stalin's amazement, the British Labour government and its pro-Soviet Minister of Trade, Sir [[Stafford Cripps]] were willing to provide technical information and a licence to manufacture the [[Rolls-Royce Nene]] centrifugal-flow jet engine. This engine was reverse-engineered and produced in modified form as the Soviet [[Klimov VK-1]] jet engine, later incorporated into the [[Mikoyan-Gurevich MiG-15|MiG-15]] (Rolls-Royce later attempted to claim £207 millions in licence fees, without success).<ref name=gordon /><ref name=caygill>{{cite book|last1=Caygill|first1=Peter|title=Sound barrier : the rocky road to mach 1.0+|date=2006|publisher=Pen & Sword Aviation|location=Barnsley|isbn=1844154564|page=130|url=https://books.google.com/books?id=oBvOAwAAQBAJ}}</ref>

In the interim, on 15 April 1947, Council of Ministers issued a decree #493-192, ordering the Mikoyan OKB to build two prototypes for a new jet fighter. As the decree called for first flights as soon as December of that same year, the designers at OKB-155 fell back on an earlier troublesome design, the [[Mikoyan-Gurevich MiG-9|MiG-9]] of 1946. The MiG-9 used a pair of the RD-20 Soviet copies of the BMW 003 for its power, which proved to be unreliable, with the airframe's straight-winged design suffering from control problems.

The prototype-only [[Mikoyan-Gurevich I-270]] of the immediate post-war era was a rocket-powered. "straight-winged" point-defense fighter design based on captured examples of, and documentation for the never-produced German [[Messerschmitt Me 263]], which had some influence on future MiG jet fighter designs.  Thanks to the MiG OKB designing the very first airworthy swept-wing Soviet aircraft design of any type in 1945, the strictly experimental [[Mikoyan-Gurevich MiG-8]] ''Utka'' canard pusher monoplane, the swept-wing research from it and captured German research documents allowed the Soviets to eventually develop the prototype design for the single-jet equipped [[MiG-15]] fighter, the [[Mikoyan-Gurevich MiG-15#Design and development|I-310]]. With the Klimov VK-1 version of the British Nene jet engine, this design became the mass-produced [[Mikoyan-Gurevich MiG-15|MiG-15]], which first flew on 31 December 1948, some fifteen months after the first prototype of its American swept-winged counterpart, the [[North American F-86 Sabre#Development|XP-86 Sabre]] first flew.<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=2297 "North American F-86 Sabre (Day-Fighter A, E and F Models)."] ''National Museum of the United States Air Force.'' Retrieved: 7 June 2011.</ref> Despite its mixed origins, this aircraft had excellent performance and formed the basis for a number of future fighters. The MiG-15 was originally intended to intercept American bombers such as the [[B-29 Superfortress]], and was even evaluated in mock air-to-air combat trials with interned ex-U.S. B-29 bombers as well as the later Soviet B-29 copy, the [[Tupolev Tu-4]]. A variety of MiG-15 variants were built, but the most common was the MiG-15UTI (NATO 'Midget') two-seat trainer. Over 18,000 MiG-15s were eventually manufactured, then came the [[Mikoyan-Gurevich MiG-17|MiG-17]], and [[Mikoyan-Gurevich MiG-19|MiG-19]].

The MiG-15s were the jets used during the Korean War by Communist forces, and '''"MIG Alley"''' was the name given by U.S. Air Force pilots to the northwestern portion of [[North Korea]], where the [[Yalu River]] empties into the [[Yellow Sea]]. During the [[Korean War]], it was the site of numerous [[dogfight]]s between U.S. fighter jets and those of the Communist forces, particularly the [[Soviet Union]]. The [[F-86 Sabre]] and the Soviet-built Mikoyan-Gurevich MiG-15 fighters were the aircraft used throughout most of the conflict, with the area's nickname derived from the latter. Because it was the site of the first large-scale jet-vs-jet air battles, MIG Alley is considered the birthplace of jet fighter combat.

==Later work==

From 1952 Mikoyan also designed [[missile]] systems to particularly suit his aircraft, such as the famous [[MiG-21]]. He continued to produce high performance fighters through the 1950s and 1960s.

He was twice awarded the highest civilian honour, the [[Hero of Socialist Labor]] and was a deputy in six [[Supreme Soviet]]s.

After Mikoyan's death, the name of the design bureau was changed from ''Mikoyan-Gurevich'' to simply ''Mikoyan''. However, the designator remained ''MiG''. Many more designs came from the design bureau such as the [[Mikoyan-Gurevich MiG-23|MiG-23]], [[Mikoyan MiG-29|MiG-29]] and [[Mikoyan MiG-35|MiG-35]] and [[Mikoyan MiG-29K|variations]].

After suffering from a stroke that occurred in 1969, Mikoyan died the following year and was buried in the [[Novodevichy Cemetery]] in Moscow.<ref name=zabecki />

==Honours and awards==
Some of his awards and honours include:<ref name=armenian /><ref name=zabecki />
*Twice [[Hero of Socialist Labour]]
*Six [[Order of Lenin|Orders of Lenin]]
*[[Order of the Red Banner]]
*[[Order of the Patriotic War|Order of the Patriotic War 1st class]]
*Two [[Order of the Red Star|Orders of the Red Star]]
*[[Lenin Prize]] (1962)
*[[USSR State Prize|Stalin Prizes]] (1941, 1947, 1948, 1949, 1952, 1953)

==References==
{{Reflist}}

{{Authority control}}

{{DEFAULTSORT:Mikoyan, Artem Ivanovich}}
[[Category:1905 births]]
[[Category:1970 deaths]]
[[Category:Russian aerospace engineers]]
[[Category:Russian Armenians]]
[[Category:Armenian engineers]]
[[Category:Armenian inventors]]
[[Category:Armenian academics]]
[[Category:Burials at Novodevichy Cemetery]]
[[Category:People from Lori Province]]
[[Category:Heroes of Socialist Labour]]
[[Category:Stalin Prize winners]]
[[Category:Soviet engineers]]
[[Category:20th-century engineers]]
[[Category:Soviet politicians]]
[[Category:Communist Party of the Soviet Union members]]
[[Category:Soviet military personnel]]
[[Category:Russian inventors]]
[[Category:Full Members of the USSR Academy of Sciences]]
[[Category:Mikoyan]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Lenin Prize winners]]
[[Category:Members of the Supreme Soviet of the Soviet Union]]