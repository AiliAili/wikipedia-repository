{{Infobox journal
| title = Journal of Rheology
| cover = [[File:CoverIssueJournalofRheology.jpg]]
| discipline = [[Physics]]
| peer-reviewed = 
| abbreviation = J. Rheol.
| impact = 2.916
| impact-year = 2015
| editor = Ralph Colby
| website = http://www.journalofrheology.org/
| publisher = [[American Institute of Physics]]
| country = USA
| history = 1929–present
| frequency = bimonthly
| formernames = Journal of Rheology ''(1929–1932)''; Physics/Journal of Applied Physics ''(1933–1939)''; Transactions of The Society of Rheology ''(1957–1977)''
| ISSN = 0148-6055
| eISSN = 
| CODEN = JORHD2
| LCCN = 78646482 
| OCLC = 3526986
}}
'''''Journal of Rheology''''' is a [[peer review|peer-reviewed]] [[scientific journal]] publishing [[original research|original (primary) research]] on all aspects of [[rheology]], the study of those properties of materials which determine their response to mechanical force. It is published bimonthly by the Society of Rheology through the [[American Institute of Physics]]. 

The [[editor-in-chief]] of ''Journal of Rheology'' is Ralph Colby.<ref>{{cite web|title=Editors|url=http://sor.scitation.org/jor/info/editors|website=Journal of Rheology|publisher=American Institute of Physics|accessdate=6 January 2017}}</ref>

==Publication history==
The publication of ''Journal of Rheology'' has seen three phases.  The journal was first published as ''Journal of Rheology'' between 1929 and 1932.  In 1933 the journal was subsumed as a section (called Rheology Numbers) of the journal ''Physics'', and then the ''[[Journal of Applied Physics]]. '' From 1957, the Society of Rheology reestablished the journal as a separate publication, initially named ''Transactions of the Society of Rheology'', renamed ''Journal of Rheology'' from 1977.

== Abstracting and indexing ==
''Journal of Rheology'' is abstracted and indexed in the following databases:<ref name=masterList>{{Cite web
  | title =Master Journal search 
  | work =Coverage  
  | publisher =[[Thomson Reuters]]
  | date =
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=0148-6055
  | format = 
  | accessdate =2017-01-06}}</ref><ref name=cassi>{{Cite web
  | title =''Journal of Rheology'' 
  | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication) 
  | publisher =[[American Chemical Society]]
  | date =
  | url = http://cassi.cas.org/publication.jsp?P=eCQtRPJo9AQyz133K_ll3zLPXfcr-WXfJmdj1To7iOwCTfP59CaW-PrjHdPIpRc8Ms9d9yv5Zd9E8xUXr5Yowctu6inlelCMMs9d9yv5Zd9cyfk2voiGkjyD_XfEfCZv
  | format = 
  | accessdate =2017-01-06}}</ref>
* [[Chemical Abstracts Service]] – [[CASSI]]
* [[Science Citation Index]] – [[Web of Science]]
* [[Engineering Index]]
* [[Applied Mechanics Reviews]]
* RAPRA Abstracts
* [[Physics Abstracts]]
* SPIN

== See also ==
{{Portal|Physics}}
* ''[[Journal of Applied Physics]]''
* [[List of scientific journals in physics]]

== References ==
<references/>

== External links ==
* {{Official website|http://www.journalofrheology.org/}}
* [http://www.rheology.org/sor/  Society of Rheology homepage]

[[Category:Physics journals]]
[[Category:American Institute of Physics academic journals]]
[[Category:Publications established in 1929]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Fluid dynamics journals]]