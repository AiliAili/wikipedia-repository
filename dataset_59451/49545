{{Infobox book
| name          = An Antarctic Mystery
| title_orig    = Le Sphinx des glaces
| translator    = [[Frances Cashel Hoey|Mrs. Cashel Hoey]]
| image         = La esfinge de los hielos.jpg
| caption = Frontispiece of French edition
| author        = [[Jules Verne]]
| illustrator   = [[Georges Roux (illustrator)|Georges Roux]]
| cover_artist  =
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #44
| subject       =
| genre         = [[Adventure novel]]
| publisher     = [[Pierre-Jules Hetzel]]
| pub_date      = 1897
| english_pub_date = 1898
| media_type    = Print ([[Hardcover|Hardback]])
| pages         =
| isbn          =
| oclc          =
| preceded_by   = [[Clovis Dardentor]]
| followed_by   = [[The Mighty Orinoco]]
}}

'''''An Antarctic Mystery''''' ({{lang-fr|Le Sphinx des glaces}}, ''The Sphinx of the Ice Fields'') is a two-volume novel by [[Jules Verne]]. Written in [[1897 in literature|1897]],  it is a response to [[Edgar Allan Poe]]'s 1838 novel ''[[The Narrative of Arthur Gordon Pym of Nantucket]]''. It follows the adventures of the narrator and his journey from the [[Kerguelen Islands]] aboard ''Halbrane''.

Neither Poe nor Verne had actually visited the remote Kerguelen Islands, located in the south [[Indian Ocean]],<ref>[[Jean-Paul Kauffmann|Kauffmann, Jean-Paul]] ''The Arch of Kerguelen: Voyage to the Islands of Desolation'' Translated by Patricia Clancy. Edinburgh. Four Walls Eight Windows (November 5, 2000) ISBN 978-1-56858-168-2</ref> but their works are some of the few literary (as opposed to exploratory) references to the archipelago.

==Plot==
[[File:Verne-sfinga-mapa.jpg|thumb|left|Map of Antarctic region, according to Verne]]

=== Volume 1 ===
The story is set in 1839, eleven years after the events in [[The Narrative of Arthur Gordon Pym of Nantucket|''Arthur Gordon Pym'']], one year after the publication of that book.

The narrator is a wealthy American '''Jeorling''', who has entertained himself with private studies of the wildlife on the [[Kerguelen Islands]] and is now looking for a passage back to the USA. ''Halbrane'' is one of the first ships to arrive at Kerguelen, and its captain '''Len Guy''' somewhat reluctantly agrees to have Jeorling as a passenger as far as [[Tristan da Cunha]].

Underway, they meet a stray iceberg with a dead body on it, which turns out to be a sailor from ''Jane''. A note found with him indicates that he and several others including ''Jane's'' captain '''William Guy''' had survived the assassination attempt at Tsalal and are still alive.

Guy, who had talked to Jeorling earlier about the subject of Pym, reveals himself to be the brother of William Guy. He decides to try to come to the rescue of ''Jane''{{'}}s crew. After taking on provisions on Tristan da Cunha and the [[Falkland Islands|Falklands]], they head South with Jeorling still on board. They also take aboard another mysterious sailor named '''Hunt''' who is eager to join the search for undisclosed reasons.

Extraordinarily mild weather allows the ''Halbrane'' to make good progress, and they break the pack ice barrier, which surrounds an ice-free Antarctic ocean, early in summer. They find first Bennet's islet, where ''Jane'' had made a stop, and finally Tsalal. But the island is completely devastated, apparently by a recent massive earthquake, and deserted. They find the remains of Tsalal's natives, who apparently died long ''before'' the earthquake, and the collar of Pym's dog, '''Tiger''', but no trace of ''Jane''.

=== Volume 2 ===
At this point, Hunt is revealed to be '''Dirk Peters'''. On their travel south of Tsalal, he and Pym had become separated, and only Peters made it safely back to the States where he, not Pym, instigated the publication of their voyage. Pym's diary, in Peters' possession, had apparently been significantly embellished by Poe. Upon returning home, Peters took on a new identity, because he was too ashamed of having resorted to cannibalism on the wreck of ''Grampus''.

Guy and Peters decide to push further south, much to the chagrin of a part of the crew led by one seaman '''Hearne''', who feels they should abandon the rescue attempt and head home before the onset of winter.

[[File:Verne-Sphinx1.jpg|thumb|right|The wreck of the ''Halbrane'']]
Not much later, in a freak accident, ''Halbrane'' is thrown upon an iceberg and subsequently lost. The crew makes it safely onto the iceberg, but with only one small boat left, it is doomed to drift on. The iceberg drifts even past the South Pole, before the whole party is cast ashore on a hitherto unknown land mass still within the pack ice barrier. Hearne and his fellows steal the last remaining boat, trying to make it to the open sea on their own, and making the situation even bleaker for those left behind who now face the prospect of wintering in the Antarctic.

They are lucky, however, as shortly thereafter they see a small boat of aboriginal style drifting by. Peters is the first to react as he swims out toward the boat and secures it. But Peters finds more: In the boat, there are captain William Guy and the three surviving seamen of his crew, semiconscious and close to death by starvation. Peters brings them ashore, and the men from ''Halbrane'' nurse them back to life.

William Guy then recounts their story. Shortly after the explosion of ''Jane'' (and presumably the departure of Pym's company), Tiger appeared again. Rabid, he bit and infected the natives who quickly fell victim to the new disease. Those who could fled to the neighboring islands, where they perished later in the course of the earthquake.

Up to this point, William Guy and his men had lived fairly comfortably on Tsalal, which was now their own, but after the quake found their position untenable and made a desperate attempt in the boat to escape north.

[[File:Verne-Sphinx.jpg|thumb|right|The Sphinx]]
The combined crews of ''Halbrane'' and ''Jane'' decide to try to make it north in their newly acquired boat. They make good progress, until they notice the appearance of strong magnetic forces. They find the source of it, the Ice Sphinx: A huge mountain magnetically "charged" by the particle streams that get focused on the poles through Earth's magnetic field.

Here, they find the remains of Hearne's team, which came to grief when the Ice Sphinx's immense magnetic forces attracted their iron tools and boat components to it and smashed them on its rocks. The boat of Joerling and the others only escaped destruction because, being built by the natives, it contained no iron parts.

At the foot of the Sphinx, they also find the body of Pym, who came to death the same way. Peters dies from grief on the same spot. The others embark again in their boat, and finally reach the open ocean and are rescued.

==See also==
{{portal|Novels}}
*''[[A Strange Discovery]]''

==Footnotes==
{{Refimprove|date=July 2009}}
{{Reflist}}

==External links==
{{commons category|The Sphinx of the Ice Fields}}
*[http://www.bbc.co.uk/news/uk-scotland-north-east-orkney-shetland-17638144 BBC report on copy donated to charity shop that sold for almost £1,000]
{{wikisourcelang|fr|Le Sphinx des Glaces}}
{{Gutenberg|no=10339|name=An Antarctic Mystery}}
*{{fr icon}} [http://www.ebooksgratuits.com/ebooks.php?auteur=Verne_Jules ''Le Sphinx des glaces'']
* {{librivox book | title=An Antarctic Mystery | author=Jules Verne}}

{{Verne}}

{{DEFAULTSORT:Antarctic Mystery, An}}
[[Category:1897 novels]]
[[Category:The Narrative of Arthur Gordon Pym of Nantucket]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in Antarctica]]
[[Category:Lost world novels]]
[[Category:Kerguelen Islands]]
[[Category:1839 in fiction]]
[[Category:Sequel novels]]
[[Category:Works of Edgar Allan Poe in popular culture]]
[[Category:Novels set in the Indian Ocean]]