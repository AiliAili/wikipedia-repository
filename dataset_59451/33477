{{Use dmy dates|date=March 2017}}
{{Use British English|date=March 2017}}

{{featured article}}
{{italic title}}
{{Coord|5.421022|N|100.343677|E|display=title}}
<!-- Geobox as per [[Liberty Bell]] -->
{{Geobox|Monument
|category=[[Cannon]]
|name=The ''Seri Rambai''
|image=Seri-rambai-cannon-penang.jpg
|image_size=280
|image_caption=The ''Seri Rambai'' at Fort Cornwallis, George Town, Penang, Malaysia
|image_alt=The ''Seri Rambai'' at Fort Cornwallis, George Town, Penang, Malaysia
|country=Malaysia
|state_type=State
|state=[[Penang]]
|region_type=City
|region=[[George Town, Penang|George Town]]
|free_type=Cast
|free=1603, by Jan Burgerhuis
|free1_type=Material
|free1=Bronze
|map=Lion-on-seri-rambai.jpg
|map_size=280
|map_caption=A decorative band in front of the [[trunnions]] features three pairs of [[Lion (heraldry)|heraldic lions]]. Each pair faces an urn filled with flowers.
|map_alt=
}}

The '''''Seri Rambai''''' is a 17th-century Dutch cannon displayed at [[Fort Cornwallis]] in [[George Town, Penang|George Town]], the capital city of the [[States and federal territories of Malaysia|Malaysian state]] of [[Penang]] and a [[UNESCO]] [[World Heritage Site]]. It is the largest bronze gun in Malaysia, a fertility symbol and the subject of legends and prophecy.

The cannon's history in the [[Malacca Straits]] began in the early 1600s, when [[Dutch East India Company]] officers gave it to the [[Sultan of Johor]] in return for trading concessions. Less than a decade later [[Johor Sultanate|Johor]] was destroyed, the sultan captured and the ''Seri Rambai'' taken to [[Sultanate of Aceh|Aceh]]. Near the end of the eighteenth century the cannon was sent to [[Selangor]] and mounted next to one of the town's hilltop forts. In 1871 pirates seized a Penang [[junk (ship)|junk]], murdered its passengers and crew, and took the stolen vessel to Selangor. The [[Straits Settlements|British colonial government]] responded by burning the town, destroying its forts and confiscating the ''Seri Rambai''.

The gun was originally displayed at George Town's [[Esplanade, Penang|Esplanade]]; in the 1950s it was moved to Fort Cornwallis.

== Background ==
Southeast Asia abounds with tales of historic cannon: many are said to be imbued with supernatural powers; some are revered for their cultural and spiritual significance; others are notable for having been present at defining moments in the region's history.{{sfnm|Andaya|1992|1pp=48–49|Watson Andaya|2011|2pp=26–28}} Burma's ''[[Hmannan Yazawin|Glass Palace Chronicle]]'' recounts a story about the [[Burmese–Siamese War (1765–1767)|Burmese-Siamese war]] (1765–1767) that illustrates the divine properties ascribed to certain cannon. After attempts to repel Burmese attacks on the [[Ayutthaya Kingdom|Siamese capital]] had proved unsuccessful, the King of Siam ordered that the city's guardian spirit, a great cannon called ''Dwarawadi'', be used to halt the advance. The gun was ceremoniously hoisted and aimed at the enemy's camp, but the powder failed to ignite. Fearing the guardian of the city had abandoned them, the king's officials implored their sovereign to surrender.{{sfnm|Phraison Salarak|1914–1915|1pp=47–48}}{{efn|Another famous gun from Thailand's [[Ayutthaya Kingdom|Ayuthaya period]] is the ''Phra Phirun'', an episode about which is recorded in the ''Royal Chronicles of Ayuthaya''. The story describes how King [[Narai]] sought to demonstrate the resourcefulness of his close friend and confidante, [[Constantine Phaulkon|Constance Phaulkon]]. The king ordered his courtiers to determine the weight of the cannon by whatever means they deemed appropriate. The noblemen discussed the king's request and constructed an enormous set of weighing scales. The attempt ended in failure. Phaulkon solved the problem by loading the gun onto a barge and marking the waterline on the boat's side. He then replaced the cannon with bricks and stones until the barge sank to the same level. By individually weighing the bricks and stones he was able to calculate the cannon's weight. Less than a century later the ''Phra Phirun'' was destroyed during the Burmese-Siamese war.{{sfnm|Sewell|1922|1pp=22–23}}}}

One of [[Jakarta]]'s best known fertility symbols is the ''Si Jagur'', a Portuguese cannon exhibited next to the city's [[Jakarta History Museum|History Museum]].{{sfnm|Samodro|2011|1pp=193–199|Gibson-Hill|1953|2p=161|}} The writer [[Aldous Huxley]] described the gun as a "prostrate God" that women caressed, sat astride and prayed to for children.{{sfnm|Huxley|1926|1pp=205–207}} Near the entrance to [[Ministry of Defence (Thailand)|Thailand's Ministry of Defence]] building in [[Bangkok]] is a cannon known as the ''[[Phaya Tani]]'', an enormous gun captured from the [[Pattani Kingdom|Sultanate of Pattani]] in 1785.{{sfnm|Watson Andaya|2013|1pp=41–45|Sewell|1922|2pp=15–17}} The cannon is a symbol of cultural identity in Pattani and the profound sense of loss caused by its seizure is still felt today: when Bangkok refused to return the gun and in 2013 sent a replica instead, suspected insurgents bombed the replica nine days later.{{sfnm|Watson Andaya|2013|1pp=41–45|Replica Cannon Bombed Nine Days After its Installation (''Isranews Agency'')|2013|}}

== The ''Seri Rambai'' ==
The ''Seri Rambai'' is a Dutch cannon displayed on the ramparts of [[Fort Cornwallis]] in [[George Town, Penang|George Town]], the capital city of [[Penang]] and a [[UNESCO]] [[World Heritage Site]].{{sfnm|Far and Malay (''The Sunday Times'')|2013}} Two articles about the cannon have been published in the ''[[Journal of the Malaysian Branch of the Royal Asiatic Society]]''. The first was a brief summary of the gun's history in the [[Malacca Straits]]; the second a detailed study researched by [[Carl Alexander Gibson-Hill]], a former director of [[National Museum of Singapore|Singapore's National Museum]] and president of the city-state's Photographic Society.{{sfnm|Douglas|1948|1pp=117–118|Gibson-Hill|1953|2pp=157–161|Dr Gibson-Hill Found Dead in Bath (''The Straits Times'')|1963}} Newspapers have also discussed the cannon: in 2013 the ''[[Sunday Times]]'' began a feature about Penang with the comment "Cannons don't often have names, but the Seri Rambai, on the walls of Fort Cornwallis, is something rather special".{{sfnm|Far and Malay (''The Sunday Times'')|2013}}

The ''Seri Rambai'' is a [[Caliber#Pounds as a measure of cannon bore|28-pounder]], 127.5 inches (3.25&nbsp;m) long with a calibre of 6.1 inches; (15&nbsp;cm); the barrel measures 118.75 inches; (3.02 m). It was cast in 1603 and is the largest bronze gun in Malaysia. In front of the dolphin handles is a decorative band featuring three pairs of [[Lion (heraldry)|heraldic lions]] with long, spiralling tails. Each pair faces a vase containing flowers. Between the handles and the [[Dutch East India Company]]'s seal is a [[Jawi alphabet|Javi]] inscription, inlaid with silver, celebrating the gun's capture in 1613. The base ring is incised with the gunsmith's signature and date of manufacture.{{sfnm|Gibson-Hill|1953|1pp=149, 157–158, 172}}{{efn|The ''Seri Rambai'' is made of bronze, an alloy of copper and tin, but like most bronze artillery pieces is commonly referred to as a brass cannon or brass gun.{{sfnm|Lefroy|1864|1p=5}} A cannon displayed next to the flagpole at the [[Royal Hospital Chelsea]] shows how this differing terminology can lead to confusion: while the gun is labelled "Brass Cannon (Siamese)", an article published in the ''Journal of the Malaysian Branch of the Royal Asiatic Society'' points out that it is made of bronze.{{sfnm|Scrivener (1981)||1p=169|Sweeney (1971)||2p=52}}}}{{efn|Dr Gibson-Hill renders the gunsmith's signature as IAN BERGERUS.{{sfnm|Gibson-Hill|1953|1p=157}} The correct name is Jan Burgerhuis (also spelled Burgerhuys), a Dutchman whose [[foundry]] in [[Middelburg]] supplied cannon to the [[Admiralty of Zeeland]] and made bells for churches in Scotland.{{sfnm|1a1=Bouchaud|1a2=Peyronnet|1a3=Krompholtz|1a4=Labourdette|1y=2014|1p=144|2a1=Puype|2a2=Van Der Hoeven|2y=1996|2pp=24, 26|3a1=Clouston|3y=1947–48|3p=175}} Other ordnance known to originate from the foundry includes two bronze cannon displayed at [[Fort Belvoir]], [[Virginia]]. They were given to Japan by the Dutch East India Company in the late 17th century and captured by American troops after World War II. Both guns were cast in the 1620s by Jan's son, Michael.{{sfnm|Bronze Cannon Conservation: Fort Belvoir (''Conservation Solutions'')|}} Another cannon made by Michael was auctioned at [[Bonhams]] in 2007. The catalogue noted its "raised band of scrolling foliage centred, at the top, on a vase containing fruit between foliated horses", a design feature similar to that on the ''Seri Rambai''. The gun was part of a private collection and no indication of its provenance was given.{{sfnm|A Very Fine and Impressive Dutch 24 Pdr. Bronze Cannon (''Bonhams'')|}}}}

== History ==
=== The ''Santa Catarina'' incident ===
The Dutch bid to control southeast Asia's spice trade hinged on two principal strategies: the first: the first was to directly attack Iberian assets in the region, including Portugal's stronghold at [[Portuguese Malacca|Malacca]] and Spanish shipping between [[Manila Galleon|Manila and Acapulco]]; the second was to forge alliances with local rulers and offer protection in exchange for trading concessions.{{sfnm|Borschberg|2002|1pp=59–60}} An important alliance was consolidated in 1603 when Dutch East India Company ships joined forces with the [[Johor Sultanate|Sultanate of Johor]] to capture the ''[[Santa Catarina (ship)|Santa Catarina]]'', a Portuguese [[carrack]] transiting the [[Singapore Straits]].{{sfnm|Borschberg|2002|1pp=60–61|Borschberg|2004|2pp=13–15}} The vessel's pillaged cargo was later sold in Europe for approximately 3.5&nbsp;million [[florin]]s, equivalent to half the Dutch East India Company's paid capital and double that of the [[East India Company|British East India Company]].{{sfnm|Borschberg|2010|1p=68}} Soon after this triumph, possibly in 1605, Dutch officers presented the ''Seri Rambai'' to [[Sultan of Johor|Johor's sultan]].{{sfnm|Gibson-Hill|1953|1pp=159–160}}{{efn|An early eighteenth-century history of Malacca written by [[François Valentijn]] records a Dutch commander arriving in Johor towards the end of 1605 and presenting the sultan with two bronze guns and a letter from [[Maurice, Prince of Orange|Prince Maurice of Nassau]]. Dr Gibson-Hill considers it "most likely" that the ''Seri Rambai'' was one of the two guns.{{sfnm|Gibson-Hill|1953|1pp=159–160}}}}

=== Sultanate of Aceh ===
One of Johor's main rivals at the time was the [[Aceh Sultanate|Sultanate of Aceh]], a cosmopolitan entrepòt and centre for religious and ideological learning. Aceh's rise to power began in the early 1500s: during the following decades the sultanate expanded its territories in [[Sumatra]] and sought military assistance from [[Suleiman the Magnificent]] in a quest to banish the Portuguese from Malacca.{{sfnm|Reid|2006|1pp=39–41, 47–48, 56–57, 59–60}} In 1613 Aceh launched an attack of Johor, destroying its capital and taking as prisoners the sultan, his family and entourage. The ''Seri Rambai'' was captured during the assault: a Javi inscription on the gun's barrel records the event and the senior [[Acehnese people|Acehnese]] officers involved.{{sfnm|Douglas|1948|1pp=17–18}}{{efn|The Javi inscription translates as "Captive of the Sultan. Taken by us, Sri Perkasa Alam Johan Berdaulat, at the time when we ordered Orang Kaya Seri Maharaja with his captains and Orang Kaya Laksamana and Orang Kaya Raja Lela Wangsa to attack Johor, in the year 1023 A.H.".{{sfnm|Douglas|1948|1pp=17–18}} As [[Anthony Reid (academic)|Professor Anthony Reid]] notes in his ''Verandah of Violence: The Background to the Aceh Problem'', Sri Perkasa Alam was the formal name for [[Iskander Muda]], the Sultan of Aceh.{{sfnm|Reid|2006|1p=55}}}}
{{quote box
|style=font-size: 85%; width: 30%; margin: 12px 20px 0px 0px; padding: 8px 15px 10px 15px; float: left; border: solid 1px #b0b0b0; background-color: cornsilk
|text=The former Kings of Acheen were on very friendly terms with the Salengore Chiefs, and the King now possesses many large Guns which he procured at Acheen. In a large piece of brass ordnance, a long 32 Pounder, I believe, which was presented to him by the King of Acheen, which is mounted on the Hill, the Natives say there is a White Snake, which comes out every Sunday, and goes to sleep inside the remainder of the Week. They fancy this is a Spirit, and if any person touches it, he is sure to fall sick. The Malays have always some remarkable or superstitious story concerning their particular Guns, and invent the most incredible Tales.
|author=[[John Anderson (diplomatic writer)|John Anderson]], ''Political and Commercial Considerations Relative to the Malayan Peninsula and the British Settlements in the Straits of Malacca'', 1824.{{sfnm|Anderson|1824|1pp=195–196}}}}

=== The Selangor incident ===
There is no recorded history of the cannon between 1613 and 1795, when the Acehnese sent the ''Seri Rambai'' to [[Sultan of Selangor|Sultan Ibrahim]] of [[Selangor]] in return for his brother's services in a military campaign.{{sfnm|Douglas|1948|1p=118}} The Selangor incident began in June 1871 when pirates commandeered a Penang [[junk (ship)|junk]], killing its thirty-four passengers and crew, and taking the vessel to Selangor. The [[Straits Settlements|British colonial government]] responded swiftly: a steamer and [[Royal Navy]] warship were dispatched to Selangor with instructions to arrest the pirates and recover the stolen junk.{{sfnm|Affair with Pirates (''The Straits Times'')|1920}} After a series of skirmishes and the arrival of support troops and artillery, the town was burned, the forts demolished and the ''Seri Rambai'' taken to Penang.{{sfnm|Affair with Pirates (''The Straits Times'')|1920|Gibson-Hill|1953|2pp=160–161}} The loss of the cannon was deeply felt in Selangor: a local prophecy maintains that only when the gun is returned will the town regain its former eminence.{{sfnm|Watson Andaya|2011|1p=28}}

=== Penang ===
According to legend the ''Seri Rambai'' was not formally unloaded in Penang but cast into shallow waters off George Town and left for almost a decade. The story describes how it was eventually retrieved by a Selangor nobleman who tied a length of thread to the gun and ordered it to float ashore.{{sfnm|Douglas|1948|1p=118|Gibson-Hill|1953|2p=161}} Until the 1950s the cannon was exhibited at the [[Esplanade, Penang|Esplanade]] in the heart of George Town, adjacent to Fort Cornwallis.{{sfnm|1a1=Gibson-Hill|1y=1953|1p=157|2a1=Bouchaud|2a2=Peyronnet|2a3=Krompholtz|2a4=Labourdette|2y=2014|2p=129}}{{efn|Vintage postcards and an old photograph of the ''Seri Rambai'' on Penang's Esplanade are reproduced in Professor Jin Seng Cheah's book, ''Penang: 500 Early Postcards''. In plates 132 and 133 the cannon can be seen next to the bandstand, pointing out to sea; plate 123 shows the gun in the same position shortly after WWII.{{sfnm|Cheah|2012|1pp=84–85}}}} It was here that the gun acquired its Malay name, ''Seri Rambai'', and reputation as a fertility symbol.{{sfnm|Douglas|1948|1p=118|Gibson-Hill|1953|2p=161}} The cannon was removed during the [[History of Penang#World War II|Japanese occupation]] in World War II, but restored to the Esplanade once hostilities had ceased.{{sfnm|Gibson-Hill|1953|1p=157}} In 1953 an article in the ''[[The Straits Times|Straits Times]]'' discussed plans to find old cannon for display at Fort Cornwallis, adding that the cannon then nearest to the fort was on the Esplanade, 200 yards away.{{sfnm|Wanted: Old Cannon for Fort (''The Straits Times'')|1953}}  By 1970 the ''Seri Rambai'' was mounted on the ramparts of Fort Cornwallis, albeit missing a wheel for its carriage.{{sfnm|Dutch Carriage for Cannon (''The Straits Times'')|1970}}

== The "Floating Cannon" of Butterworth ==
Near the ferry terminal in [[Butterworth, Penang|Butterworth]] is an old, rusted cannon that according to a local Chinese tradition was once the ''Seri Rambai's'' female partner. The story tells how it abandoned its "mate" and floated across the channel from Penang to Butterworth. A Malay tradition ascribes a different history to the Butterworth cannon, but believes the ''Seri Rambai'' is one of a pair.{{sfnm|Coope|1947|1pp=126–128}} The possibility that the ''Seri Rambai'' might have a twin or "relative" is not without precedent. A researcher studying Jakarta's ''Si Jagur'' found a similar gun in Lisbon's Military Museum and surmised that both had been cast by Manuel Tavares Bocarro, a Portuguese founder in [[Macau]].{{sfnm|Guedes|2011|1pp=56–57}} An oft-told story holds that Pattani's ''Phaya Tani'' had a twin, the ''Seri Negara''. Both were captured during Siam's conquest of the sultanate and ordered to be taken to Bangkok. One version of the tale describes how the ''Seri Negara'' fell into Pattani Bay while being ferried to the ship; another claims it was lost at sea when the Siamese vessel foundered and sank.{{sfnm|Syukri|1985|1p=71|Sewell|1922|2pp=15–17}}

== Notes ==
{{notes|colwidth=50em}}

== Citations ==
{{Reflist|colwidth=30em}}

== Sources ==
{{Refbegin}}
'''Books / Monographs'''
*{{citation
|last=Andaya |first=Leonard Y.
|authorlink=Leonard Andaya
|editor-last=Tarling |editor-first=Nicholas
|editorlink=Nicholas Tarling
|title=The Cambridge History of Southeast Asia: From c. 1500 to c. 1800
|volume=2
|url=https://books.google.com/books?id=GIz4CDTCOwcC&pg=PA48
|year=1992
|pages=1–57
|chapter=Chapter 1: Interactions with the Outside World and Adaption in Southeast Asian Society, 1500–1800
|publisher=Cambridge University Press
|location=Cambridge
|isbn=0-521-66370-9
|ref={{sfnRef|Andaya1992}}
}}
*{{citation
|last=Anderson |first=John
|authorlink=John Anderson (diplomatic writer)
|title=Political and Commercial Considerations Relative to the Malayan Peninsula and the British Settlements in the Straits of Malacca
|url=https://books.google.com/books?id=MEYIAAAAQAAJ&pg=PA195
|year=1824
|publisher=William Cox
|location=Prince of Wales Island
|oclc=17294743
|ref={{sfnRef|Anderson1824}}
}}
*{{citation
|last=Borschberg |first=Peter
|title=The Singapore and Melaka Straits: Violence, Security and Diplomacy in the 17th Century
|url=https://books.google.com/books?id=Rb6cBgAAQBAJ&pg=PA68
|year=2010
|publisher=NUS Press
|location=Singapore
|ref={{sfnRef|Borschberg2010}}
|isbn=9971694646
}}
*{{citation
|last1=Bouchaud |first1=Jērôme
|last2=Peyronnet |first2=Claire
|last3=Krompholtz |first3=Pierrette
|last4=Labourdette |first4=Jean Paul
|last5=Auzias |first5=Dominique
|title=Petite Fute Malaisie Singapour: 2014–2015
|language=French
|url=https://books.google.com/books?id=_fKRAgAAQBAJ&pg=PA144#v=onepage&q&f=false
|year=2014
|publisher=Nouvelles éd. de l'Université
|location=Paris
|ref={{sfnRef|BouchaudPeyronnetKrompholtzLabourdette2014}}
|isbn=2746969734
}}
*{{citation
|last=Cheah |first=Jin Seng
|title=Penang: 500 Early Postcards
|url=https://books.google.com/books?id=7ftEFcwwx20C
|year=2012
|publisher=Editions Didier Millet
|location=Singapore
|ref={{sfnRef|Cheah2012}}
|isbn=9671061710
}}
*{{citation
|last=Huxley |first=Aldous
|authorlink=Aldous Huxley
|title=Jesting Pilate: an Intellectual Holiday
|url=https://archive.org/stream/jestingpilateint00huxl#page/205/mode/1up
|year=1926
|publisher=George H. Doran
|location=New York City
|oclc=643322
|ref={{sfnRef|Huxley1926}}
}}
*{{citation
|last=Lefroy |first=Brigadier-General John Henry
|authorlink=John Henry Lefroy
|title=Official Catalogue of the Museum of Artillery in the Rotunda, Woolwich
|url=https://books.google.com/books?id=X8o_AAAAcAAJ&pg=PA5
|year=1864
|publisher=Her Majesty's Stationery Office
|location=London
|oclc=25085586
|ref={{sfnRef|Lefroy1864}}
}}
*{{citation
|last1=Puype |first1=Jan Piet
|last2=Van Der Hoeven |first2=Marco
|title=The Arsenal of the World: the Dutch Arms Trade in the Seventeenth Century
|url=https://books.google.com/?id=BxkhAQAAIAAJ&q=burgerhuys
|year=1996
|publisher=Batavian Lion International
|location=Amsterdam
|ref={{sfnRef|PuypeVan Der Hoeven1996}}
|isbn=9067074136
}}
*{{citation
|last=Reid |first=Anthony
|authorlink=Anthony Reid (academic)
|title=Verandah of Violence: The Background to the Aceh Problem
|url=https://books.google.com/books?id=PfF8Y9nZE3oC&pg=PA39
|year=2006
|publisher=NUS Press
|location=Singapore
|ref={{sfnRef|Reid2006}}
|isbn=0-295-98633-6
}}
*{{citation
|last=Syukri |first=Ibrahim
|title=History of the Malay Kingdom of Patani
|url=https://books.google.com/?id=6md5AAAAIAAJ
|year=1985
|publisher=Ohio University Press
|location=USA
|ref={{sfnRef|Syukri1985}}
|isbn=0-89680-123-3
}}
*{{citation
|last=Watson Andaya |first=Barbara
|authorlink=Barbara Watson Andaya
|editor-last=Jory
|editor-first=Patrick
|title=Ghosts of the Past in Southern Thailand: Essays on the History and Historiography of Pattani
|url=https://books.google.com/books?id=PIDGBgAAQBAJ&pg=PA41
|publisher=NUS Press
|location=Singapore
|year=2013
|pages=31–52
|chapter=Chapter 2: Gates, Elephants, Cannon and Drums: Symbols and Sounds in the Creation of a Pattani Identity
|isbn=9971696355
|ref={{sfnRef|Watson Andaya2013}}
}}
'''Journals / Magazines'''
*{{Citation
|last=Borschberg |first=Peter
|year=2002
|title=The Seizure of the ''Santo Antônio'' at Patani
|journal=Journal of the Siam Society
|volume=90 |pages= 59–72
|url=http://www.siamese-heritage.org/jsspdf/2001/JSS_090_0e_Borschberg_SeizureOfSantoAntonioAtPatani.pdf
|ref={{sfnRef|Borschberg2002}}
|issn=0857-7099
}}
*{{Citation
|last=Borschberg |first=Peter
|year=2004
|title=The ''Santa Catarina'' Incident of 1603
|journal=Revista de Cultura
|publisher=Instituto Cultural do Governo da R.A.E. de Macau
|location=Macao
|volume=11 |pages=13–25
|url=http://edocs.icm.gov.mo/rc/RC11I12.pdf
|archiveurl=https://web.archive.org/web/20160512061422/http://edocs.icm.gov.mo/rc/RC11I12.pdf
|archivedate=12 May 2016
|ref={{sfnRef|Borschberg2004}}
|issn=1682-1106
}}
*{{Citation
|last=Clouston |first=R. W. M.
|year=1947–48
|title=The Church Bells of Renfrewshire and Dunbartonshire
|journal=Proceedings of the Society of Antiquaries of Scotland
|publisher=Society of Antiquaries of Scotland
|location=Edinburgh
|volume=82 |pages=146–192
|url=http://archaeologydataservice.ac.uk/archiveDS/archiveDownload?t=arch-352-1/dissemination/pdf/vol_082/82_146_192.pdf
|ref={{sfnRef|Clouston1947–48}}
|issn=0081-1564
}}
*{{Citation
|last=Coope |first=A.E.
|year=1947
|title=The Floating Cannon of Butterworth
|journal=Journal of the Malayan Branch of the Royal Asiatic Society
|volume=20 |issue=1 |pages=126–128
|jstor=41560008
|ref={{sfnRef|Coope1947}}
|issn=0126-7353
}}
*{{Citation
|last=Douglas |first=Dato F. W.
|year=1948
|title=The Penang Cannon, Si Rambai
|journal=Journal of the Malayan Branch of the Royal Asiatic Society
|volume=21 |issue=1 |pages=117–118
|jstor=41560479
|ref={{sfnRef|Douglas1948}}
|issn=0126-7353
}}
*{{Citation
|last=Gibson-Hill |first=C.A.
|authorlink=Carl Alexander Gibson-Hill
|year=1953
|title=Notes on the Old Cannon Found in Malaya, and Known to be of Dutch Origin
|journal=Journal of the Malayan Branch of the Royal Asiatic Society
|volume=26 |issue=1 |pages=145–174
|jstor=41502911
|ref={{sfnRef|Gibson-Hill1953}}
|issn=0126-7353
}}
*{{Cite magazine
|last=Guedes |first=João
|title=Weapons of Yesteryear: Portuguese Cannon Foundries in Macao
|magazine=Macao Magazine
|publisher=Government Information Bureau
|publication-place=Macao
|date=October 2011
|pages=54–58
|url=http://macaomagazine.net/sites/default/files/MM9.pdf
|archiveurl=https://web.archive.org/web/20160403155318/http://macaomagazine.net/sites/default/files/MM9.pdf
|archivedate=3 April 2016
|ref={{sfnRef|Guedes2011}}
|issn=2076-5479
}}
*{{Citation
|last=Phraison Salarak |first=Luang
|year=1914–1915
|title=Intercourse between Burma and Siam as Recorded in Hmannan Yazawindawgyi
|journal=Journal of the Siam Society
|volume=11 |issue=3 |pages=1–67
|url=http://www.siamese-heritage.org/jsspdf/1911/JSS_011_3b_LuangPhraisonSalarak_IntercourseBetweenSiamAndBurmaPartII.pdf
|ref={{sfnRef|Phraison Salarak1914–1915}}
|issn=0857-7099
}}
*{{Citation
|last=Samodro
|year=2011
|title=Makna Tanda Gestur Seksual pada Meriam Si Jagur di Museum Fatahilah, Jakarta
|journal=Mudra : Jurnal Seni Budaya
|publisher=Sekolah Tinggi Seni Indonesia
|location=Denpasar
|volume=26 |issue=2 |pages=193–200
|url=http://repo.isi-dps.ac.id/1689/1/949-3447-1-PB.pdf
|archiveurl=https://ia601503.us.archive.org/14/items/MudraJurnalSeniBudaya/Mudra%20-Jurnal-Seni-Budaya.pdf
|archivedate=23 April 2016
|language=Indonesian
|ref={{sfnRef|Samodro2011}}
|issn=0854-3461
}}
*{{Citation
|last=Scrivener |first=R.S.
|year=1981
|title=The Siamese Brass Cannon in the Figure Court of the Royal Hospital, Chelsea, London
|journal=Journal of the Siam Society
|volume=69 |pages=169–170
|url=http://www.siamese-heritage.org/jsspdf/1981/JSS_069_0m_Scrivener_SiameseBrassCannonInChelseaLondon.pdf
|ref={{sfnRef|Scrivener (1981)}}
|issn=0857-7099
}}
*{{Citation
|last=Sewell |first=C.A. Seymour
|year=1922
|title=Notes on Some Old Siamese Guns
|journal=Journal of the Siam Society
|volume=15 |issue=1 |pages=1–43
|url=http://www.siamese-heritage.org/jsspdf/1921/JSS_015_1d_Sewell_SomeOldSiameseGuns.pdf
|ref={{sfnRef|Sewell1922}}
|issn=0857-7099
}}
*{{Citation
|last=Sweeney |first=Amin
|year=1971
|title=Some Observations on the Malay Sha'ir
|journal=Journal of the Malaysian Branch of the Royal Asiatic Society
|volume=44 |issue=1 |pages=52–70
|jstor=41492377
|ref={{sfnRef|Sweeney (1971)}}
|issn=0126-7353
}}
*{{Citation
|last=Watson Andaya |first=Barbara
|authorlink=Barbara Watson Andaya
|year=2011
|title=Distant Drums and Thunderous Cannon: Sounding Authority in Traditional Malay Society
|journal=International Journal of Asia Pacific Studies
|publisher=Universiti Sains Malaysia
|location=Penang
|volume=7 |issue=2 |pages=17–33
|url=http://web.usm.my/ijaps/articles/ART2-IJAPS7(2)2011_pg17-33.pdf
|archiveurl=https://web.archive.org/web/20160403191451/http://web.usm.my/ijaps/articles/ART2-IJAPS7(2)2011_pg17-33.pdf
|archivedate=3 April 2016
|ref={{sfnRef|Watson Andaya2011}}
|issn=1823-6243
}}
'''Newspapers / News Agencies'''
*{{Citation
|title=Affair with Pirates
|newspaper=The Straits Times
|publication-place=Singapore
|date=4 October 1920
|page=10
|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article/straitstimes19201004-1.2.56.aspx
|ref={{sfnRef|Affair with Pirates (.27.27The Straits Times.27.27)1920}}
}}
*{{Citation
|title=Dr Gibson-Hill Found Dead in Bath
|newspaper=The Straits Times
|publication-place=Singapore
|date=20 August 1963
|page=1
|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article/straitstimes19630820-1.2.6.aspx
|ref={{sfnRef|Dr Gibson-Hill Found Dead in Bath (.27.27The Straits Times.27.27)1963}}
}}
*{{Citation
|title=Dutch Carriage for Cannon
|newspaper=The Straits Times
|publication-place=Singapore
|date=8 September 1970
|page=3
|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article/straitstimes19700908-1.2.26.aspx
|ref={{sfnRef|Dutch Carriage for Cannon (.27.27The Straits Times.27.27)1970}}
}}
*{{Citation
|title=Far and Malay
|newspaper=The Sunday Times
|publication-place=London
|date=24 January 2013
|url=http://www.thesundaytimes.co.uk/sto/travel/Destinations/Asia/article1200622.ece
|archiveurl=http://www.webcitation.org/6glcTOXBl
|archivedate=14 April 2016
|ref={{sfnRef|Far and Malay (.27.27The Sunday Times.27.27)2013}}
}}
*{{Citation
|title=Replica Cannon Bombed Nine Days After its Installation
|newspaper=Isranews Agency
|publication-place=Bangkok
|date=13 June 2013
|url=http://www.isranews.org/south-news/English-Article/67-%E0%B9%80%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%94%E0%B9%88%E0%B8%99-%E0%B8%A0%E0%B8%B2%E0%B8%84%E0%B9%83%E0%B8%95%E0%B9%89/21777-Replica-cannon-bombed-nine-days-after-its-installation.html
|archiveurl=https://web.archive.org/web/20160408142054/http://www.isranews.org/south-news/English-Article/67-%E0%B9%80%E0%B8%A3%E0%B8%B7%E0%B9%88%E0%B8%AD%E0%B8%87%E0%B9%80%E0%B8%94%E0%B9%88%E0%B8%99-%E0%B8%A0%E0%B8%B2%E0%B8%84%E0%B9%83%E0%B8%95%E0%B9%89/21777-Replica-cannon-bombed-nine-days-after-its-installation.html
|archivedate=8 April 2016
|ref={{sfnRef|Replica Cannon Bombed Nine Days After its Installation (.27.27Isranews Agency.27.27)2013}}
}}
*{{Citation
|title=Wanted: Old Cannon for Fort
|newspaper=The Straits Times
|publication-place=Singapore
|date=18 February 1953
|page=4
|url=http://eresources.nlb.gov.sg/newspapers/Digitised/Article/straitstimes19530218-1.2.67.aspx
|ref={{sfnRef|Wanted: Old Cannon for Fort (.27.27The Straits Times.27.27)1953}}
}}
'''Websites'''
* {{cite web
|url=https://www.bonhams.com/auctions/14956/lot/192/
|title=A Very Fine and Impressive Dutch 24 Pdr. Bronze Cannon
|website=[[Bonhams]]
|archiveurl=https://web.archive.org/web/20160415235233/https://www.bonhams.com/auctions/14956/lot/192/
|archivedate=15 April 2016
|ref={{sfnRef|A Very Fine and Impressive Dutch 24 Pdr. Bronze Cannon (.27.27Bonhams.27.27)}}
}}
*{{cite web
|url=https://conservationsolutionsinc.com/projects/view/75/bronze-cannon-conservation-fort-belvoir/
|title=Bronze Cannon Conservation: Fort Belvoir
|website=Conservation Solutions
|archiveurl=https://web.archive.org/web/20160424075625/https://conservationsolutionsinc.com/projects/view/75/bronze-cannon-conservation-fort-belvoir/
|archivedate=24 April 2016
|ref={{sfnRef|Bronze Cannon Conservation: Fort Belvoir (.27.27Conservation Solutions.27.27)}}
}}
{{Refend}}

[[Category:Individual cannons]]
[[Category:Tourist attractions in George Town]]
[[Category:History of Johor]]