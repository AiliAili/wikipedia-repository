{{Redirect|Thundercup}}
{{Infobox person
| name  = Monica Lin
| image = 
| image_size = 
| birth_date  = 
| birth_place = [[Los Angeles, CA]]
| parents     = 
| occupation = [[Marketing director]], [[Internet personality]]
| alma_mater  = 
| home_town  = [[Los Angeles, CA]]<br/>
[[Taipei, Taiwan]]
| organization = Popular Demand
| known_for =
| website = 
}}

'''Monica Lin''', better known as '''Thundercup''', is a [[Taiwanese-American]] marketing director and Internet personality. She currently serves as Marketing Director at Popular Demand, a Los Angeles based clothing brand.<ref name=suburban>{{cite news
| author = The Surburban Family
| title = Inner-View: Thundercup & Kiona Stowers Of Popular Demand Brand.
| quote = 
| newspaper = Suburban Lifestyles Magazine
| date = September 6, 2016
| pages =
| url = http://www.sbrbnlifestyles.com/thundercup-and-kiona-stowers-of-pd
| accessdate =  February 10, 2017
}}</ref>.

==Early life==

Monica Lin was born and raised in Los Angeles, CA until she was 9 years old. Her parents are Taiwanese. She then moved to Taiwan to live with her mother and attended international school overseas before returning to the U.S. for high school<ref name=cnk>{{cite news
| author = Chan-Lo
| title = Chick Chat: Meet the Woman Behind This Powerhouse Street Wear Brand
| quote = 
| newspaper = Chicks N Kicks
| date = July 28, 2016
| pages =
| url = http://cnkdaily.com/features/2016/7/28/chick-chat-meet-the-woman-behind-this-powerhouse-street-wear-brand
| accessdate =  
}}</ref>
<ref name=infinite>{{cite news
| author = Devon Travern
| title = Movers & Shakers Presents: The Engine Behind Popular Demand, Thundercup
| quote = 
| newspaper = Infinite Magazine
| date = November 7, 2016
| pages =
| url = http://www.weareinfinite.co/2016/11/11411
| accessdate =  
}}</ref>. 

==Career==

Monica Lin discovered her affinity for fashion through her mom, who exposed her to different international brands, such as [[Comme des Garçons]] and [[Bape]]<ref name="infinite" /><ref name=dishh>{{cite news
| author = Kevin Mordukhayev
| title = Meet Thundercup: The Girl Bossing Around the Boys of Streetwear
| quote = 
| newspaper = The Dishh
| date = 
| pages =
| url = http://thedishh.com/2016/05/11/meet-thundercup-girl-bossing-around-boys-streetwear
| accessdate =  February 10, 2017
}}</ref>. In the early stages of her career, she started a streetwear blog called “Thundercup"<ref name="cnk" />. Her blog gained momentum when she was the first to debut photos of rapper [[Lil Wayne|Lil Wayne’s]] TRUKFIT clothing line<ref name="cnk" /><ref name=meet>{{cite news
| author = Jocelyn Vega
| title = Meet The Industry: Monica Lin aka Thundercup
| quote = 
| newspaper = Meet The Industry
| date = 
| pages =
| url = https://meettheindustry.net/2016/06/13/meet-the-industry-monica-lin
| accessdate = 
}}</ref>. Her blog coverage was posted by various media outlets, with notable ones that included [[MTV]]<ref>{{cite news
| author = Nadeska Alexis
| title = Photos: Lil Wayne's Trukfit Skateboard Clothing Line
| quote = 
| newspaper = MTV News
| date = January 10, 2012
| pages =
| url = http://www.mtv.com/news/2496800/photos-of-lil-wayne-trukfit-skateboard-clothing-line/
| accessdate = 
}}</ref>, [[HipHopDX]]<ref>{{cite news
| author = Steven Horowitz
| title = Lil Wayne Unveils Skateboard-Inspired Clothing Line "Trukfit"
| quote = 
| newspaper = HipHopDX
| date = January 9, 2012
| pages =
| url = http://hiphopdx.com/news/id.17993/title.lil-wayne-unveils-skateboard-inspired-clothing-line-trukfit
| accessdate = 
}}</ref>, and [[Complex (magazine)|Complex]]<ref>{{cite news
| author = Teofilo Killip
| title = More Looks Into Lil Wayne’s Skateboarding-Inspired Trukfit Clothing Line
| quote = 
| newspaper = Complex Magazine
| date = January 9, 2012
| pages =
| url = http://www.complex.com/style/2012/01/more-looks-into-lil-waynes-skateboarding-inspired-trukfit-clothing-line
| accessdate = 
}}</ref>. She used her blog as a networking tool<ref name="infinite" />.

In 2012, Monica began working for Popular Demand as an intern<ref name="cnk" /><ref name="infinite" /><ref name="meet" />. She interned for a month before she got hired after doing product placement for the brand by serving as the stylist for numerous hip hop music videos<ref name="cnk" />. One notable music video she styled was rapper [[E-40]]’s “[[Function (song)|Function]] (Remix)” music video<ref name="cnk" />, which featured Problem, Young Jeezy, Chris Brown, French Montana, and Red Café. Since serving as the Marketing Director for Popular Demand, she has lead numerous partnerships and collaborations for the brand<ref name="infinite" />, including ones with [[Roscoe's House of Chicken and Waffles|Roscoe’s Chicken and Waffles]]<ref>{{cite news
| author = Jenn Harris
| title = Wear your love for Roscoe's House of Chicken and Waffles on a T-shirt
| quote = 
| newspaper = Los Angeles Times
| date = December 3, 2013
| pages =
| url = http://www.latimes.com/food/dailydish/la-dd-roscoes-popular-demand-waffle-shirts-20131202-story.html
| accessdate = 
}}</ref><ref>{{cite news
| author = Farley Elliott
| title = Roscoe's Chicken & Waffles: Dinner, Hats AND T-Shirts
| quote = 
| newspaper = Los Angeles Times
| date = December 6, 2013
| pages =
| url = http://www.laweekly.com/content/printView/4178253
| accessdate = 
}}</ref>, [[Def Jam Records]]<ref>{{cite news
| author = Wall-Doe
| title = Check out the Popular Demand X Def Jam Collaboration Hoodie
| quote = 
| newspaper = The Source
| date = October 4, 2013
| pages =
| url = http://thesource.com/2013/10/04/check-out-the-popular-demand-x-def-jam-collaboration-hoodie
| accessdate = 
}}</ref>, rapper [[Ty Dolla $ign]]<ref>{{cite news
| author = Dan Hyman
| title = Ty Dolla $ign Honestly Could Not Be More Hyped About His New Album
| quote = 
| newspaper = GQ Magazine
| date = November 12, 2015
| pages =
| url = http://www.gq.com/story/ty-dolla-sign-debut-album-qa
| accessdate = 
}}</ref><ref>{{cite news
| author = Paul Meara
| title = Ty Dolla $ign Delivers Free TC With Model Behavior
| quote = 
| newspaper = BET
| date = November 13, 2015
| pages =
| url = http://www.bet.com/news/music/2015/11/13/ty-dolla-sign-free-tc-model-billboard.html
| accessdate = 
}}</ref>, and more.

Since 2014, she has been featured in various YouTube videos with [[The Fung Brothers]], including “Life of a Sneakerhead 4 W/ a Girl".

In 2015, she launched a radio show on TheMixShow.com called “The Plug” with different special guests<ref name="suburban" />, including [[Lil Dicky]], who did a [https://www.youtube.com/watch?v=dRWIA2iS8RU freestyle] on the show. 

Monica has also worked on various collaborations on her own as an influencer with brands including Puma, Nike<ref name="infinite" />, and [[The Grammy Awards]]<ref>{{cite news
| author = 
| title = The Grammys: Who Will Win Best Rap Album?
| quote = 
| newspaper = The Recording Academy
| date = February 2, 2017
| pages =
| url = https://www.grammy.com/videos/the-grammys-who-will-win-best-rap-album
| accessdate = 
}}</ref><ref>{{cite news
| author = 
| title = Who Should Win GRAMMY For Best Rap Album?
| quote = 
| newspaper = HipHopDX
| date = February 4, 2017
| pages =
| url = http://hiphopdx.com/videos/id.25819/title.who-should-win-grammy-for-best-rap-album
| accessdate = 
}}</ref>

==References==
{{Reflist}}

==External Links==
*[http://www.populardemandshop.com Popular Demand website]

{{DEFAULTSORT:Lin, Monica}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:American Internet celebrities]]
[[Category:American people of Taiwanese descent]]
[[Category:People from Los Angeles]]
[[Category:American marketing people]]