{{Distinguish2|the Australian anthropological journal [[Australian Anthropological Society|The Australian Journal of Anthropology]] formerly known as "Mankind"}}
{{Infobox journal
| title = Mankind Quarterly
| cover =
| discipline = [[Anthropology]]
| abbreviation = Mank. Q.
| editor = Gerhard Meisenberg, [[Richard Lynn]]
| publisher = [[Council for Social and Economic Studies]]
| country = United States
| frequency = Quarterly
| history = 1961&ndash;present
| impact =
| impact-year =
| website = http://www.mankindquarterly.com
| ISSN = 0025-2344
| OCLC = 820324
| LCCN = 63024971
| CODEN = MKQUA4
}}
{{Infobox journal
| title = Mankind Quarterly Monographs
| discipline = [[Anthropology]]
| abbreviation = Mank. Q. Monogr.
| publisher = [[Ulster Institute for Social Research]]
| country = United Kingdom
| ISSN = 0893-4649
| OCLC = 149980257
| LCCN = sf89030002
| CODEN = MAQUE6
}}
The '''''Mankind Quarterly''''' is a [[Peer review|peer-reviewed]] [[academic journal]] dedicated to [[physical anthropology|physical]] and [[cultural anthropology]], published by the [[Ulster Institute for Social Research]] in [[London]]. It contains articles on [[human evolution]], [[intelligence (trait)|intelligence]], [[ethnography]], [[linguistics]], [[mythology]], [[archaeology]], etc. The journal aims to unify anthropology with [[biology]].

It has been called a "cornerstone of the scientific racism establishment" and a "white supremacist journal",<ref>Joe L. Kincheloe, et. al, ''Measured Lies: The Bell Curve Examined'', Palgrave Macmillan, 1997, pg. 39</ref> "[[scientific racism]]'s keepers of the flame",<ref>William H. Tucker, ''The funding of scientific racism: Wickliffe Draper and the Pioneer Fund'', University of Illinois Press, 2002, pg. 2</ref> a journal with a "racist orientation" and an "infamous racist journal",<ref>Ibrahim G. Aoudé, ''The ethnic studies story: politics and social movements in Hawaiʻi'', University of Hawaii Press, 1999 , pg. 111</ref> and "journal of 'scientific racism'".<ref>Kenneth Leech, ''Race'', Church Publishing, Inc., 2005, pg. 14</ref>

==History==

''Mankind Quarterly'' was founded in 1961. The founders were [[Robert Gayre]], [[Henry Garrett]], [[Roger Pearson (anthropologist)|Roger Pearson]], [[Corrado Gini]], Luigi Gedda (Honorary Advisory Board),<ref>{{cite journal | pmid = 19848223 | volume=20 | title=Against UNESCO: Gedda, Gini and American scientific racism | journal=Med Secoli | pages=907-35 | author=Cassata F}}</ref> [[Otmar Freiherr von Verschuer|Otmar von Verschuer]] and [[Reginald Ruggles Gates]]. It was originally published in [[Edinburgh]], [[Scotland]], by the [[International Association for the Advancement of Ethnology and Eugenics]].

Its foundation may in part have been a response to the 1954 [[Supreme Court of the United States|U.S. Supreme Court]] decision ''[[Brown v. Board of Education]]'' which ordered the desegregation of schools in the United States.<ref>Schaffer, Gavin "‘Scientific’ Racism Again?”:1 Reginald Gates, the Mankind Quarterly and the Question of “Race” in Science after the Second World War'' Journal of American Studies (2007), 41: 253-278 Cambridge University Press.{{request quote|date=February 2017}}</ref><ref name="Jackson2005p148">{{Cite book |title=Science for Segregation: Race, Law, and the Case against Brown v. Board of Education |last=Jackson |first= John P. |publisher=[[NYU Press]] |year=2005 |isbn=978-0-8147-4271-6 |layurl=http://www.historycooperative.org/journals/lhr/25.2/br_19.html |laydate=30 August 2010}}{{request quote|date=February 2017}}</ref>

Already in 1961, the journal came under heavy criticism when renowned physical anthropologist [[Juan Comas]] published a series of scathing critiques of the journal. He argued that the journal was reproducing discredited racial ideologies, such as [[Nordicism]] and [[anti-Semitism]], under the guise of science.<ref>{{cite journal | author = Comas Juan | year = 1961 | title = Scientific" Racism Again?. | url = | journal = Current Anthropology | volume = 2 | issue = 4| pages = 303–340 }}</ref><ref>{{cite journal | author = Comas Juan | year = 1962 | title = More on "Scientific" Racism | url = | journal = Current Anthropology | volume = 3 | issue = 3| pages = 284–302 }}</ref> The critique prompted a series of responses and rebuttals to Comas' critique from the editors of the journal, published in the journal itself &ndash; including a highly critical review of Comas' book ''Racial Myths'' by James A. Gregor, among more or less direct attacks on Comas. Comas then argued in ''[[Current Anthropology]]'' that the journal's review of his book ''Racial Myths'' was politically motivated, and misrepresented the field of physical anthropology by adhering to outdated racial ideologies, for example by claiming that [[Jews]] were considered a "biological race" by the racial biologists of the time. Other anthropologists complained that paragraphs that did not agree with the racial ideology of the editorial board were deleted from published articles without the authors' agreement.<ref>{{cite journal |author1=Ehrenfels UR von |author2=Madan T. N. |author3=Comas Juan | year = 1962 | title = Mankind Quarterly Under Heavy Criticism: 3 Comments on Editorial Practices | url = | journal = Current Anthropology | volume = 3 | issue = 2| pages = 154–158 }}</ref><ref>John P. Jackson. 2005.  Science for Segregation: Race, Law, and the Case Against Brown V. Board of Education. NYU Press 151-154</ref><ref name=Erickson>Paul A. Erickson, Liam Donat Murphy. 2013. Readings for A History of Anthropological Theory. University of Toronto Press, p. 534</ref><ref>{{cite journal | author = Harrison G. Ainsworth | year = 1961 | title = The Mankind Quarterly | url = | journal = Man | volume = 61 | issue = | pages = 163–164 }}</ref>

The strong criticism meant that few academic anthropologists would publish in the journal or serve on its board; when Gates died, [[Carleton S. Coon|Carleton Coon]], an anthropologist sympathetic to the hereditarian and racialist view of the journal, was asked to replace him, but he rejected the offer stating that "I fear that for a professional anthropologist to accept membership on your board would be the kiss of death". Nonetheless, the journal continued to be published supported by grant money.<ref name=Erickson/> Publisher Roger Pearson received over a million dollars in grants from the [[Pioneer Fund]] in the 1980s and 1990s.<ref name="Tucker2002" /><ref name="Mehler-funding">Mehler, Barry (July 7, 1998). [http://www.ferris.edu/ISAR/Institut/pioneer/search.htm Race Science and the Pioneer Fund] Originally published as "The Funding of the Science" in ''Searchlight'', No. 277.</ref><ref>Genoves, Santiago. "Racism and" The Mankind Quarterly"." American Association for the Advancement of Science, 1961.</ref>

In 1979, publication was transferred to the [[Council for Social and Economic Studies]] in [[Washington, D.C.]]. In January 2015, the [[Ulster Institute for Social Research]] in [[London]] took over as publishers.

== Editors ==
The [[editor-in-chief]] is Gerhard Meisenberg ([[Dominica]], East Caribbean). The assistant editor is [[Richard Lynn]], psychologist and former board member and grantee of the Pioneer Fund.<ref>http://www.rlynn.co.uk/</ref><ref>{{citation | publisher = Mankind Quarterly |year= |title= Editorial Panel | journal= |volume= | url = http://www.mankindquarterly.org/about | accessdate = 15 May 2015}}</ref>

==Criticism==
Many of those involved with the journal are connected to academic [[hereditarianism]]. The journal has been criticized as being political and strongly right-leaning,<ref>e.g., Arvidsson, Stefan (2006), Aryan Idols: Indo-European Mythology as Ideology and Science, translated by Sonia Wichmann, Chicago and London: The University of Chicago Press.</ref> racist or fascist.<ref>{{cite book|author=Schaffer, Gavin|title=Racial science and British society, 1930-62|publisher=Palgrave Macmillan|place=Basingstoke|year=2008}}</ref><ref>{{cite journal|author=Gelb, Steven A.|title=Heart of Darkness: The Discreet Charm of the Hereditarian Psychologist|journal=The Review of Education/Pedagogy/Cultural Studies|volume=19|issue=1|year=1997|pages=129–139|doi=10.1080/1071441970190110}}</ref> Pearson has responded that much of anthropology is politicised in the opposite way and claims that the most vocal critics of the journal often identify with the [[Political radicalism|radical]] tradition in anthropology.<ref>Roger Pearson, "Activist Lysenkoism: The Case of Barry Mehler." In Race, Intelligence and Bias in Academe (Washington: Scott-Townsend Publishers, 1997).</ref>

In 1963, after the journal's first issue, contributors [[Baron Omar Rolf von Ehrenfels|U. R. Ehrenfels]], [[Triloki Nath Madan|T. N. Madan]], and [[Juan Comas]] said that the journal's editorial practice was biased and misleading.<ref>{{cite journal|author1=Ehrenfels, U. R. | authorlink1= Baron Omar Rolf von Ehrenfels | author2 = Madan, T. N. | authorlink2 = Triloki Nath Madan | author3 = Comas, J. | authorlink3 = Juan Comas |year=1962|title=Mankind Quarterly Under Heavy Criticism: 3 Comments on Editorial Practices|jstor=2739528|journal=Current Anthropology|volume=3|issue=2|pages=154–158|doi=10.1086/200265}}</ref> This was denied by editors of the journal.<ref>{{cite journal|author1=Gates, R. R.  |author2=Gregor, A. J. |lastauthoramp=yes |year=1963|title=Mankind Quarterly: Gates and Gregor Reply to Critics|jstor=2739826|journal=Current Anthropology|volume=4|issue=1|pages=119–121|doi=10.1086/200345}}</ref>

During the "[[The Bell Curve|Bell Curve]] wars" of the 1990s, the journal received attention when opponents of ''The Bell Curve'' publicized the fact that some of the works cited by ''Bell Curve'' authors [[Richard Herrnstein]] and [[Charles Murray (political scientist)|Charles Murray]] had first been published in ''Mankind Quarterly''.<ref name="Tucker2002">{{Cite book |title=The funding of scientific racism: Wickliffe Draper and the Pioneer Fund |last=Tucker |first=William H. |authorlink=William H. Tucker |publisher=[[University of Illinois Press]] |year=2007 |isbn=978-0-252-07463-9 |layurl=http://www.press.uillinois.edu/books/catalog/65rwe7dm9780252074639.html |laydate=4 September 2010}}</ref> In ''[[The New York Review of Books]]'', Charles Lane referred to ''The Bell Curve''{{'}}s "tainted sources", that seventeen researchers cited in the book's bibliography had contributed articles to, and ten of these seventeen had also been editors of, ''Mankind Quarterly'', "a notorious journal of 'racial history' founded, and funded, by men who believe in the genetic superiority of the white race."<ref>[http://www.nybooks.com/articles/2008 ''"The Bell Curve" and Its Sources'', Harry F. Weyher, reply by Charles Lane]</ref> The journal continues to publish hereditarian perspective articles, stating that "much of this science has stood the test of time", and "the editors still welcome controversy and new ideas".<ref>{{cite web|title=History and Philosophy|url=http://www.mankindquarterly.org/about/|publisher=Mankind Quarterly|accessdate=3 September 2016}}</ref>

==See also==
* ''[[Journal of Social, Political, and Economic Studies]]''

== References ==
{{reflist|35em}}

== Further reading ==
* {{Cite book |title=Inside the League |last1=Anderson |first1=Scott |last2=Anderson |first2=Jon Lee |publisher=Dodd, Mead |year=1986 |location= |isbn=978-0-396-08517-1}}
* {{Cite book |title=The Science and Politics of Racial Research |last=Tucker |first=William H. |publisher=University of Illinois Press |year=1996 |isbn=978-0-252-06560-6 |layurl=http://www.press.uillinois.edu/books/catalog/75xps5et9780252020995.html |laydate=7 November 2010}}
* {{Cite book |title=The Cattell Controversy: Race, Science, and Ideology |last=Tucker |first=William H. |authorlink=William H. Tucker |publisher=[[University of Illinois Press]] |year=2009 |isbn=978-0-252-03400-8 |layurl=http://www.insidehighered.com/news/2009/03/20/cattell |laydate=30 August 2010}}

== External links ==
* {{Official website|http://www.mankindquarterly.com}}

[[Category:Biological anthropology]]
[[Category:Race and intelligence controversy]]
[[Category:Publications established in 1960]]
[[Category:Anthropology journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Eugenics]]
[[Category:Racism]]
[[Category:Scientific racism]]