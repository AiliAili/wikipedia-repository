<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=L.66
 | image=Albatros L.66_side.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Light aircraft
 | national origin=[[Germany]]
 | manufacturer=[[Albatros Flugzeugwerke]]
 | designer=
 | first flight=1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=10
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Albatros L.66''' was a simple, low powered, two seat sports and training [[parasol wing]] [[monoplane]], built in [[Germany]] in the mid-1920s.

==Design and development==
The L.66 was intended to be a low cost, easy maintenance two-seater. Thus a low power engine was required and the prototype was fitted with a {{convert|30|hp|kW|abbr=on|0}} [[Haacke HFM-2]] [[flat twin]], though other engines could be used. It was a [[cantilever]] [[parasol]] [[monoplane]] with an aerodynamically thick [[airfoil]] section wing, which had a simple, rectangular plan and significant [[dihedral (aircraft)|dihedral]].  The wing was a wooden structure with two [[spar (aviation)|spars]], the [[aileron]]s mounted directly to a rounded groove in the rear spar.  It was linked to the upper [[fuselage]] by a rather complicated set of [[strut]]s.<ref name=Flight/>

Traditionally, Albatros had constructed fuselages with wood frames but the L.66 marked a departure, with a welded steel tube [[longeron]] and strut framework [[aircraft fabric covering|covered with fabric]]. The sides were flat and almost airfoil shaped, with a blunt, rounded nose and taper aft. The nose mounted engine, driving a two blade [[propeller (aircraft)|propeller]], was raised above the fuselage with a [[aircraft fairing|fairing]] behind it, enclosing the oil tank and with the instrument panel attached to its rear. The fuel tank was in the wing centre section, feeding the engine by gravity. Further aft, pilot and passenger sat [[tandem#Side-by-side seating|side by side]] in the {{convert|1.2|m|ftin|abbr=on|0}} wide fuselage; tandem controls could be fitted if required. The [[empennage]] was simple in shape and action as there were no fixed surfaces only an all-moving [[rudder]] and [[elevator (aircraft)|elevator]]. Both control surfaces were [[balanced rudder|balanced]].  The [[conventional undercarriage]] was also very simple, with the mainwheels attached to a single axle which passed through the fuselage, sitting the L.66 close to the ground.  The tailskid was linked to the rudder for steering on the ground<ref name=Flight/>

==Operational history==
[[File:Albatros L.66 front.png|thumb|right|Prototype with Haake engine]]
The first L.66 was flying in mid-1924 with the Haake engine.<ref name=Flight/> In all ten were built.<ref name=HAv/>

==Variants==
;L.66: Haake engine.<ref name=Flight/>
;L.66a: Slightly larger and heavier, with Stahlwerk Mark St.M3 or Anzani engine.<ref name=HAv/>
;L.66c:<ref name=HAv/>
;L.67:Lighter version, [[Bristol Cherub]] or Anzani engine. Two built.<ref name=HAv2/>

==Specifications (Haake engine) ==
{{Aircraft specs
|ref=Flight, 6 June 1924, pp.385<ref name=Flight/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|capacity=Two
|length m=5.40
|length note=<ref name=HAv/>
|span m=9.00
|span note=<ref name=HAv/>
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=13.5
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=220
|empty weight note=
|gross weight kg=395
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=33 L
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Haacke HFM-2]]
|eng1 type=air-cooled, [[horizontally opposed]] two cylinder
|eng1 hp=30
|eng1 note=
|power original=
|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=100
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=3 hr at full power
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=about 15 min to 1,000 m (3,280 ft)
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{Commons category|Albatros L.66}}
{{reflist|refs=

<ref name=Flight>{{cite magazine |last= |first= |authorlink= |date=12 June 1924|title= The German exhibits|magazine= [[Flight International|Flight]]|volume=XV |issue=24 |pages=384–5 |id= |url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200384.html}}</ref>

<ref name=HAv>{{cite web |url=http://www.histaviation.com/albatros_l_66.html|title=Albatros L.66 |author= |date= |work= |publisher= |accessdate=6 August 2013}}</ref>

<ref name=HAv2>{{cite web |url=http://www.histaviation.com/albatros_l_67.html|title=Albatros L.67 |author= |date= |work= |publisher= |accessdate=7 August 2013}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

{{albatros aircraft}}

[[Category:German sport aircraft 1920–1929]]
[[Category:Parasol-wing aircraft]]
[[Category:Albatros aircraft|L.66]]