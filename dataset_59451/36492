{{good article}}
{{Advanced Placement}}

'''Advanced Placement Statistics''' ('''AP Statistics''', '''AP Stat''' or '''AP Stats''') is a college-level [[high school]] [[statistics]] course offered in the United States through the [[College Board]]'s [[Advanced Placement]] program. This course is equivalent to a one semester, non-[[calculus]]-based introductory college statistics course and is normally offered to [[eleventh grade|juniors]] and [[twelfth grade|seniors]] in high school. 

One of the College Board's more recent additions,<ref name="mulekar4">Mulekar (2004), p. 4</ref> the AP Statistics exam was first administered in May 1996 to supplement the AP program's math offerings, which had previously consisted of only [[Advanced Placement Calculus|AP Calculus AB and BC]].<ref name="piccolino">{{cite journal |last=Piccolino |first=Anthony V. |date=May 1996 |title=The Advanced Placement course in statistics: Increasing students' options |journal= The Mathematics Teacher|volume= 89|issue= 5|pages=376}}</ref> In the [[United States]], enrollment in AP Statistics classes has increased at a higher rate than in any other AP class.<ref>{{cite book |title= Statistics: A Desktop Quick Reference|last=Boslaugh |first=Sarah |author2=Paul Andrew Watters|year= 2008 |publisher=O'Reilly |isbn=0-596-51049-7|url= https://books.google.com/books?id=ZnhgO65Pyl4C&printsec=frontcover|accessdate=2009-07-30}}</ref>

Students may receive college credit or upper-level college course placement upon the successful completion of a three-hour exam ordinarily administered in May. The exam consists of a multiple choice section and a free response section that are both 90 minutes long. Each section is weighted equally in determining the students' composite scores.

==History==
{| align="left" class="wikitable" style="margin-right:1em;"
|+ Number of examinees
|-
! Year 
! Students
|-
! 1997<ref name="hinders3">Hinders (2007), p.3</ref>
| 7,667
|-
! 1998<ref name="brumbaugh01">{{cite book |title=Teaching secondary mathematics |last=Brumbaugh |first=Douglas K. |author2=David Rock |year=2001 |publisher=Lawrence Erlbaum Associates |isbn=0-8058-3599-7 |page=408 |url= https://books.google.com/books?q=ap+statistics+curriculum&um=1&as_brr=3|accessdate=July 31, 2009}}</ref>
| 15,486
|-
! 1999<ref name="brumbaugh01"/>
| 25,240
|-
! 2000<ref>{{cite book |title=Apex AP Statistics |last=Apex Learning |publisher=Kaplan |isbn=0-7432-0190-6 |url= http://www.amazon.com/Apex-AP-Statistics-Learning/dp/0743201906|accessdate=August 10, 2009}}</ref>
| 34,118
|-
! 2001<ref name="CB01"/>
| 41,034
|-
! 2002<ref name="CB02"/>
| 49,824
|-
! 2003<ref name="CB03"/>
| 58,230
|-
! 2004<ref name="CB04"/>
| 65,878
|-
! 2005<ref name="CB05"/>
| 76,786
|-
! 2006<ref name="CB06"/>
| 88,237
|-
! 2007<ref name="CB07"/>
| 98,033
|-
! 2008<ref name="CB08"/>
| 108,284
|-
! 2009<ref name="CB09"/>
| 116,876
|-
! 2012<ref name="rodriguez2012">{{cite news |title=More than 1 million and counting: the growth of Advanced Placement Statistics |last=Rodriguez |first=Robert |year=2012 |journal=AmStat News|url=http://magazine.amstat.org/blog/2012/09/01/prescolumnsept2012/|accessdate=October 8, 2012}}</ref>
| 152,750
|-
!2013<ref>http://media.collegeboard.com/digitalServices/pdf/ap/apcentral/ap13_statistics_ScoringDist.pdf</ref>
| 169,508
|-
!2014<ref>http://media.collegeboard.com/digitalServices/pdf/ap/apcentral/ap14-statistics-score-dist.pdf</ref>
| 184,173
|-
!2015<ref>https://secure-media.collegeboard.org/digitalServices/pdf/research/2015/Student-Score-Distributions-2015.pdf</ref>
| 195,526
|-
|}
The Advanced Placement program has offered students the opportunity to pursue college-level courses while in high school. Along with the [[Educational Testing Service]], the College Board administered the first AP Statistics exam in May 1997.<ref name="piccolino"/> The course was first taught to students in the 1996-1997 [[academic year]].<ref>{{cite book |title=Developing Students' Statistical Reasoning: Connecting Research and Teaching Practice |last=Garfield |first=Joan B. |author2=Dani Ben-Zvi |year=2008 |publisher=Springer |isbn=1-4020-8382-3 |page=8 |url= https://books.google.com/books?id=lAZMh4aRmA4C&printsec=frontcover&q=|accessdate=August 11, 2009}}</ref> Prior to that, the only mathematics courses offered in the AP program included AP Calculus AB and BC. Students who didn't have a strong background in college-level math, however, found the AP Calculus program inaccessible and sometimes declined to take a math course in their senior year. Since the number of students required to take statistics in college is almost as large as the number of students required to take calculus, the College Board decided to add an introductory statistics course to the AP program. Since the prerequisites for such a program doesn't require mathematical concepts beyond those typically taught in a second-year [[algebra]] course, the AP program's math offerings became accessible to a much wider audience of high school students. The AP Statistics program addressed a practical need as well, since the number of students enrolling in [[academic major|majors]] that use statistics has grown.<ref name="piccolino"/> A total of 7,667 students took the exam during the first administration, which is the highest number of students to take an AP exam in its first year.<ref name="hinders3"/> Since then, the number of students taking the exam rapidly grew to 98,033 in 2007, making it one of the 10 largest AP exams.

==Course==
If the course is provided by their school, students normally take AP Statistics in their junior or senior year and may decide to take it concurrently with a [[pre-calculus]] course.<ref name="mulekar4"/> This offering is intended to imitate a one-semester, non-calculus based college statistics course, but high schools can decide to offer the course over one [[Academic term|semester]], two [[Academic term|trimesters]], or a full [[Academic term|academic year]].<ref name="piccolino"/>

The six-member AP Statistics Test Development Committee is responsible for developing the curriculum. Appointed by the College Board, the committee consists of three college statistics teachers and three high school statistics teachers who are typically asked to serve for terms of three years.<ref name="mulekar3">Mulekar (2004), p. 3</ref><ref name="hinders4">Hinders (2007), p. 4</ref>

===Curriculum===
[[Image:Black cherry tree histogram.svg|thumb|right|Students are expected to be able to interpret graphs, such as this [[histogram]], and analyze its characteristics, including [[average|center]], [[Statistical dispersion|spread]], shape, [[outlier]]s, clusters, and gaps.<ref name="koehler7-9"/>]]
Emphasis is placed not on actual arithmetic computation, but rather on conceptual understanding and interpretation.<ref name="mulekar5">Mulekar (2004), p. 5</ref> The course curriculum is organized around four basic themes; the first involves [[data analysis|exploring data]]<ref name="piccolino"/> and covers 20–30% of the exam. Students are expected to use graphical and numerical techniques to analyze distributions of data, including [[univariate]], [[bivariate data|bivariate]], and [[categorical data|categorical]] data.<ref name="koehler7-9">Koehler (2008), pp. 7–9</ref> The second theme involves [[design of experiments|planning and conducting a study]] and covers 10–15% of the exam. Students must be aware of the various methods of [[data collection]] through [[sampling (statistics)|sampling]] or [[experiment]]ation and the sorts of conclusions that can be drawn from the results.<ref>Koehler (2008), pp. 8–9</ref> The third theme involves [[probability]] and its role in anticipating patterns in distributions of data. This theme covers 20–30% of the exam.<ref>Koehler (2008), pp. 8, 10</ref> The fourth theme, which covers 30–40% of the exam, involves [[statistical inference]] using [[point estimation]], [[confidence intervals]], and [[significance test]]s.<ref>Koehler (2008), pp. 8, 11</ref>

==Exam==
{{see also|Advanced Placement Exams}}
Along with the course curriculum, the exam is developed by the AP Statistics Test Development Committee as well. With the help of other college professors, the committee creates a large pool of possible questions that is pre-tested with college students taking statistics courses. The test is then refined to an appropriate level of difficulty and clarity.<ref name="hinders4"/> Afterwards, the Educational Testing Service is responsible for printing and administering the exam.<ref name="mulekar3"/>

===Structure===
The exam is offered every year in May.<ref name="mulekar8">Mulekar (2004), p. 8</ref> Students are not expected to memorize any formulas; rather, a list of common statistical formulas related to [[descriptive statistics]], [[probability]], and [[inferential statistics]] is provided. Moreover, tables for the [[normal distribution|normal]], [[Student's t]] and [[chi-squared distribution]]s are given as well.<ref name="piccolino"/> Students are also expected to use graphing calculators with statistical capabilities.<ref name="simmons6"/> The exam is three hours long with ninety minutes allotted to complete each of its two sections: multiple choice and free-response.<ref>Berger (1997), p. 4</ref> The multiple choice portion of the exam consists of forty questions with five possible answers each.<ref name="simmons5">Simmons (2009), p. 5</ref> The free response section contains six open-ended questions that are often long and divided into multiple parts.<ref name="simmons5"/> The first five of these questions may require twelve minutes each to answer and normally relate to one topic or category. The sixth question consists of a broad-ranging investigative task and may require approximately twenty-five minutes to answer.<ref name="simmons6">Simmons (2009), p. 6</ref>

===Grading===
{| align="right" class="wikitable"
|+ AP Statistics exam grade distributions
|-
! Year
! 5
! 4
! 3
! 2
! 1
! Mean<br /> grade
|-
! 2001<ref name="CB01">{{cite web |title=2001 Advanced Placement Examination National Report |url=http://www.collegeboard.com/ap/pdf/01_national_summary.pdf |year=2001 |work=College Board |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 11.5%
| 23.4%
| 24.9%
| 19.1%
| 21.1%
|align="right" |2.85
|-
! 2002<ref name="CB02">{{cite web |title=2002: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/homepage/21071.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 11.2%
| 21.8%
| 23.9%
| 19.2%
| 23.9%
|align="right" |2.77
|-
! 2003<ref name="CB03">{{cite web |title=2003: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/homepage/28073.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 13.2%
| 22.3%
| 26.4%
| 19.5%
| 18.6%
|align="right" |2.92
|-
! 2004<ref name="CB04">{{cite web |title=2004: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/homepage/38410.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 12.6%
| 22.4%
| 24.8%
| 19.8%
| 18.1%
|align="right" |2.87
|-
! 2005<ref name="CB05">{{cite web |title=2005: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/homepage/47492.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 12.6%
| 22.8%
| 25.3%
| 19.2%
| 20.1%
|align="right" |2.88
|-
! 2006<ref name="CB06">{{cite web |title=2006: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/exam/exam_questions/151261.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 12.6%
| 22.2%
| 25.3%
| 18.3%
| 21.6%
|align="right" |2.86
|-
! 2007<ref name="CB07">{{cite web |title=2007: Statistics Grade Distributions |url=http://apcentral.collegeboard.com/apc/members/exam/exam_questions/185632.html |year=2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 10, 2009}}</ref>
| 11.9%
| 21.5%
| 25.4%
| 17.1%
| 24.1%
|align="right" |2.80
|-
! 2008<ref name="CB08">{{cite web |title=AP Statistics Student Grade Distributions: AP Examinations - May 2008 |url= http://apcentral.collegeboard.com/apc/public/repository/ap08_Statistics_GradeDistributions.pdf |year=2008 |work=AP Central |publisher= [[College Board]]|accessdate=August 10, 2009}}</ref>
| 12.9%
| 22.7%
| 23.7%
| 18.8%
| 21.8%
|align="right" |2.86
|-
! 2009<ref name="CB09">{{cite web |title=AP Statistics Student Grade Distributions: AP Examinations - May 2009 |url= http://apcentral.collegeboard.com/apc/public/repository/ap09_Statistics_GradeDistributions.pdf |year=2009 |work=AP Central |publisher= [[College Board]]|accessdate=October 13, 2009}}</ref>
| 12.3%
| 22.3%
| 24.2%
| 19.1%
| 22.2%
|align="right" |2.83
|-
! 2010<ref name="CB10">{{cite web |title=AP Statistics Student Grade Distributions: AP Examinations - May 2010 |url=http://apcentral.collegeboard.com/apc/public/repository/2010_Statistics_Score_Dist.pdf |year=2010 |work=AP Central
|publisher= [[College Board]]|accessdate=December 4, 2010}}</ref>
| 12.8%
| 22.4%
| 23.5%
| 18.2%
| 23.1%
|align="right" |2.84
|-
! 2011<ref>{{cite web|title=2011 AP Exam Score Distribution|url=http://www.totalregistration.net/index.php?option=com_content&view=article&id=443&Itemid=118|publisher=Total Registration|accessdate=20 June 2013}}</ref>
| 12.1%
| 21.3%
| 25.1%
| 17.7%
| 23.8%
|align="right" |2.80
|-
! 2012<ref name="e2013"/>
| 12.6%
| 20.2%
| 25.0%
| 18.8%
| 23.4%
|align="right" |2.81
|-
! 2013<ref name="e2013">{{cite web|title=2013 AP Exam Score Distribution|url=http://www.totalregistration.net/index.php?option=com_content&view=article&id=487&Itemid=118|publisher=Total Registration|accessdate=5 July 2013}}</ref>
| 12.2%
| 20.9%
| 25.7%
| 18.1%
| 23.1%
|align="right" |2.80
|-
! 2014<ref name="e2014">{{cite web|title=2014 Student Score Distributions|url=http://media.collegeboard.com/digitalServices/pdf/research/2014/STUDENT-SCORE-DISTRIBUTIONS-2014.pdf|publisher=The College Board|accessdate=8 July 2015}}</ref>
| 14.3%
| 20.9%
| 24.5%
| 17.8%
| 22.6%
|align="right" |2.86
|-
! 2015
| 13.2%
| 18.9%
| 25.2%
| 18.9%
| 23.8%
|align="right" |2.79
|-
! 2016<ref name="sd2016">{{cite web|title=2016 Student Score Distributions|url=http://www.totalregistration.net/AP-Exam-Registration-Service/2016-AP-Exam-Score-Distributions.php|publisher=Total Registration|accessdate=13 July 2016}}</ref>
| 13.9%
| 21.7%
| 24.7%
| 15.7%
| 24%
|align="right" |2.86
|-
|}
The multiple choice section is scored immediately after the exam by computer.<ref name="mulekar8"/> One point is awarded for each correct answer, no points are credited or deducted for unanswered questions, and points are no longer deducted for having an incorrect answer.<ref>Berger (1997), p. 5</ref>

Students' answers to the free-response section are reviewed in early June by readers that include high school and college statistics teachers gathered in a designated location.<ref name="mulekar8"/><ref name="simmons7">Simmons (2009), p. 7</ref> The readers use a pre-made rubric to assess the answers and normally grade only one question in a given exam. Each question is graded on a scale from 0 to 4, with a 4 representing the most complete response. Communication and clarity in the answers receive a lot of emphasis in the grading.<ref name="mulekar8"/>

Both sections are weighted equally when the composite score is calculated.<ref name="simmons5"/> The composite score is reported on a scale from 1 to 5, with a score of 5 being the highest possible.<ref name="simmons7"/>

==See also==
*[[Statistics education]]

==Notes==
<references/>

==References==
*{{cite book |title= AP Statistics: The Best Test Preparation for the Advanced Placement Exam|last=Berger |first=Jack J. |author2= The Staff of Research and Education Association |year= 1997 |publisher=Research & Education Association |isbn=0-87891-082-4 |url= https://books.google.com/books?id=Sf_rPhW7rEQC&pg=PA3&lpg=PA3&dq=ap+statistics|accessdate=July 30, 2009}}
*{{cite book |title= 5 Steps to a 5 AP Statistics, 2008–2009 Edition |last=Hinders |first=Duane C. |year= 2007 |publisher=McGraw-Hill Professional |isbn=0-07-148856-1 |url= https://books.google.com/books?id=WRNZlbe5foYC&printsec=frontcover#v=onepage&q=|accessdate=July 30, 2009}}
*{{cite web |title=Statistics: Course Description|url=http://apcentral.collegeboard.com/apc/public/repository/ap08_statistics_coursedesc.pdf |last=Koehler |first=Kenneth |first2=Kathy |last2=Fritz |first3=Dorinda |last3=Hewit |first4=Chris |last4=Olsen |first5=Thomas |last5=Short |first6=Josh |last6=Tabor |first7=Robert |last7=Taylor |first8=Christine |last8=Franklin |first9=Richard |last9=Chilcoat |first10=Jeff |last10=Haberstroh |first11=Luis |last11=Saldivia|date=May 2009 |work=AP Central |publisher=[[College Board]] |accessdate=August 3, 2009}}
*{{cite book |title= Cracking the AP Statistics Exam: 2004–2005|last=Mulekar |first=Madhuri S. |author2=Princeton Review|year= 2004 |publisher=The Princeton Review |isbn=0-375-76390-2 |url= https://books.google.com/books?id=GG1Kug-0P8UC&printsec=frontcover&q=|accessdate=July 31, 2009}}
*{{cite book |title= Kaplan AP Statistics 2009|last=Simmons |first=Bruce |first2= Mary Jean |last2=Bland |first3=Barbara |last3=Wojciechowski|year= 2009 |publisher=Kaplan Publishing |isbn=1-4195-5246-5 |url= https://books.google.com/books?id=9_cMBWFkDwoC&printsec=frontcover|accessdate=July 30, 2009}}

==Further reading==

===Textbooks===
*{{cite book |title=The practice of statistics: TI-83 graphing calculator enhanced |first=Daniel S.|last=Yates |first2=David S.|last2=Moore|authorlink2=David S. Moore |first3=George P. |last3=McCabe |year=1998 |publisher=Macmillan |isbn=0-7167-3370-6 |url=https://books.google.com/books?id=E9_RDLMoEkMC&printsec=frontcover&q= |accessdate=2009-08-04}}
*{{cite book |title=Stats: Modeling the World |first=David E. |last=Bock |author2=Paul F. Velleman |author3=Richard D. De Veaux  |edition=3rd |year=2010 |publisher=Pearson/Addison-Wesley/Prentice-Hall |isbn=0-13-135958-4 |url=http://www.pearsonschool.com/index.cfm?locator=PSZ4Z4&PMDBSUBCATEGORYID=&PMDBSITEID=2781&PMDBSUBSOLUTIONID=&PMDBSOLUTIONID=&PMDBSUBJECTAREAID=&PMDBCATEGORYID=&PMDbProgramID=62361 |accessdate=2010-02-18}}
*{{cite book |title=Introduction to Statistics and Data Analysis |first=Roxy |last=Peck |author2=Chris Olsen |author3=Jay L. Devore  |edition=3rd |year=2008 |publisher=Cengage Learning |isbn=0-495-55783-8 |url=https://books.google.com/books?id=2VkNiakfaUEC&printsec=frontcover&q= |accessdate=2009-08-04}}
*{{cite book |title=Statistics in Action: Understanding a World of Data |first=Ann E. |last=Watkins |author2=Richard L. Scheaffer |author3=George W. Cobb  |year=2004 |publisher=Key Curriculum Press |isbn=1-55953-313-7}}
*{{cite book |title=Advanced High School Statistics |first=David M. |last=Diez |author2=Christopher D. Barr |author3= Mine Çetinkaya-Rundel  |author4= Leah Dorazio |year=2015 |publisher=OpenIntro, Inc |isbn=1-94345-000-5 |url=https://www.openintro.org/stat/textbook.php?stat_book=aps}}

===Teaching guides===
*{{cite book |title=Teaching Statistics: More Data, Less Lecturing |last=Cobb |first=George |editor=In Steen, Lynn Arthur |year=1992 |publisher=Mathematical Association of America |location=Washington, D.C. |isbn= }}
*{{cite journal |title=Statistics for the Twenty-First Century |editor=Gordon, Florence and Sheldon |year=1992 |journal=MAA Notes |volume=26 |publisher=Mathematical Association of America |location=Washington, D.C. |isbn= }}
*{{cite journal |title=Teaching Statistics: Resources for Undergraduate Instructors |editor=Moore, Thomas |year=2000 |journal=MAA Notes |volume=52 |publisher=Mathematical Association of America |location=Washington, D.C. |isbn= }}
*{{cite book |title=Principles and Standards for School Mathematics |edition=3 |last=National Council of Teachers of Mathematics |year=2003 |publisher=National Council of Teachers of Mathematics |location=Reston, VA |isbn= }}

==External links==
*[http://www.collegeboard.com/student/testing/ap/about.html Advanced Placement website]
*[http://www.collegeboard.com/student/testing/ap/sub_stats.html?stats AP Statistics information]

{{Education by subject}}

[[Category:Statistics education]]
[[Category:Mathematics education|Statistics]]