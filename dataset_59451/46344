{{Orphan|date=July 2013}}
{{Infobox writer  <!--For more information, see [[:Template:Infobox Writer/doc]].-->
| name = Gauri Ayyub
| honorific_prefix = 
| honorific_suffix = 
| image = 
| image_size = 
| alt = 
| caption = 
| native_name = 
| native_name_lang = 
| pseudonym = 
| birth_name = 
| birth_date = {{birth date|1931|02|13}} 
| birth_place = Patna
| death_date = {{death date and age|1998|07|13|1931|02|13}} 
| death_place = Kolkata 
| resting_place = 
| occupation = Social worker, activist, writer and teacher
| language = Bengali
| nationality = Indian
| ethnicity = Bengali
| citizenship = 
| education = Bankipur Girls’ High School
| alma_mater = Magadh Mahila College
| period = 
| genre =         <!-- or: | genres =      -->
| subject = Social and political issues
| movement = 
| notableworks = Tuchcha Kichu Sukh Dukkha, Door Prodesher Sankirno Path, Ei je Ahana
| spouse = Abu Sayeed Ayyub
| partner =       <!-- or: | partners =    -->
| children = 
| relatives = 
| awards = 
| signature = 
| signature_alt = 
| years_active = 
| module = 
| website = 
| portaldisp =    <!-- "on", "yes", "true", etc; or omit -->
}}

{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}

'''Gauri Ayyub''' (1931&nbsp;– 1998) was a social worker, activist, writer and teacher based in [[Kolkata]] (Calcutta) for most of her life. Married to the philosopher and literary critic, Abu Sayeed Ayyub<ref name="asa">{{cite book |last=Qureshi |first=Mahmud Shah |year=2012 |chapter=Ayyub, Abu Sayeed |chapter-url=http://en.banglapedia.org/index.php?title=Ayyub,_Abu_Sayeed |editor1-last=Islam |editor1-first=Sirajul |editor1-link=Sirajul Islam |editor2-last=Jamal |editor2-first=Ahmed A. |title=Banglapedia: National Encyclopedia of Bangladesh |edition=Second |publisher=[[Asiatic Society of Bangladesh]]}}</ref> (1906–1982), Gauri was a writer in her own right, and is known for her short stories, translations, and numerous articles on social issues. She is recognised for her role in the propagation of communal harmony in Bengal, active assistance to the [[Bangladesh Liberation War]] of 1971 and vocal opposition to the curbing of human rights during the declaration of [[The Emergency (India)|emergency in India in 1974]]. She assisted writer and social worker [[Maitreyi Devi]] in founding ''Khelaghar'',<ref name="khela">[http://khelaghar.in/ Khelaghar]</ref> initially as a shelter for Bangladeshi children orphaned during the war of 1971. After Maitreyi Devi died in 1990, Ayyub took charge<ref>Winter, Joe (2013) ''Calcutta Song'', Peridot Press, pp 150–151. ISBN 978 1 908095 70 1</ref> of Khelaghar and it still runs as an orphanage that follows the educational principles of [[Rabindranath Tagore]], emphasising holistic development of children in a natural surrounding.

Gauri Ayyub studied philosophy at [[Visva-Bharati University]], Santiniketan and education at the [[University of Calcutta]]. During 1963–91, she was a professor and later the head of the department of education at the [[Shri Shikshayatan College]], an affiliated college of the University of Calcutta. Though stricken with [[rheumatoid arthritis]] early in her life, she carried on her numerous activities in spite of often severe pain and disability, in addition to caring for her ailing husband, bedridden for the last decade of his life.

== Biography ==

=== Early life ===
Gauri Datta was born on 13 February 1931 at Patna. Her father, professor Dhirendra Mohan Datta, was a philosopher, writer and teacher. Her mother, Nirupama Datta, ran her business. Gauri had four brothers and four sisters. Her family had roots in the erstwhile [[East Pakistan]] and the occasional arduous journey from Patna to [[Mymensingh]] (now in [[Bangladesh]]) formed an important part of her early memories. Her [[Gandhian]] father encouraged a frugal lifestyle that strongly influenced her adult life and thoughts.<ref name="amit">Chaudhuri, Amitabha  ''Gauri amader jiboner anyatamo pradhan bismay (Amazing Gauri)'' in Ed: Nahar, Miratun (2001). ''Kritajnatar Ashrubindu (Gauri Ayyub: A Memorial Volume)'', p. 22. Dey’s Publishing, Kolkata. ISBN 81-7612-750-7.</ref>

=== Education ===
Gauri went to Bankipur Girls’ High School and stood first among girls in the statewide final examination in 1947. After two years of intermediate education at Magadh Mahila College,<ref>[http://www.magadhmahilacollege.org/ Magadh Mahila College]</ref> she enrolled at [[Patna University]]. While here, she was arrested for her involvement in an anti-imperialist student movement. The couple of nights that she had to spend in jail shaped her future life, since her father promptly packed her off in 1950 to [[Visva Bharati University]]. There, she completed her B.A. in philosophy (1952) without political distractions. However, during this period, she met and soon fell in love with Abu Sayeed Ayyub, her teacher and 25 years her senior.<ref name="amit" /> She earned a teachers' training degree in 1953 and completed her M.A. in education from the [[University of Calcutta]] in 1955. During her stay in [[Shantiniketan]], she organised a literary festival<ref>Ghosh, Shankha (1998) ''Atmabishvas e sahaj'', Chaturanga Vol. 58 No. 2, pp. 164–5</ref> along with Nemai Chattopadhyay<ref>[https://www.theguardian.com/culture/2011/apr/03/nimai-chatterji-obituary Nemai Chattopadhyay]</ref> that, in hindsight, is of great significance. This three-day festival starting on 21 February 1953, brought together Bengali writers and poets of [[Bangladesh]] (then East Pakistan) and [[West Bengal]]. Most significantly, it commemorated the shooting down of several Bengali students, exactly a year back in Dhaka, who had protested against the imposition of Urdu as the national language of Pakistan. Though not widely recognised, this was the first public commemoration of the Bhasha Andolan ([[Bengali language movement]]), later institutionalised as the [[International Mother Language Day]] by [[UNESCO]], and widely celebrated in both Bangladesh and West Bengal as "Bhasha Dibas".

=== Professional life ===
After short stints of teaching at Ushagram Methodist Mission (1953), [[South Point School]] (1955–57) and [[Jodhpur Park Girls' High School]], she joined the [[Shri Shikshayatan College]]<ref>[http://www.shrishikshayatancollege.org/ Shri Shikshayatan College]</ref> in 1963 and headed the department of education till a year before her retirement in 1991.<ref>Chakraborty, Amita (1998). "Manabikatai chhilo tar dhyangyan (Humanism was her guiding principle)", ''Rabindra Bhabna'', Vol. 12, Issue 3, p.42</ref>

Gauri Ayyub died in her home in Kolkata (13 July 1998) at 67, spending her last days in agony from acute arthritis, often refusing to meet even her friends and well wishers, a tragic and uncharacteristic end for such an affectionate, social and active personality.<ref>''Pratideen'' [http://epratidin.in/ Obituary] 14 July 1998</ref>

== Other activities ==

=== Educator ===
Her commitment to teaching went beyond her professional career. She wrote on educational issues and taught Bengali to several foreign students and scholars. Among the Japanese students she interacted with were Masayuki Usuda, Nariaki Nakazato and Kyoko Niwa, who later went on to achieve recognition as scholars on India and Bengal. She developed a distinctive teaching style, throwing her students in at the deep end, usually by starting with a [[Rabindranath Tagore]] novel.<ref>Niwa, Kyoko  ''Gauridir smriti (Memories of Gauri-di)'' in Ed: Nahar, Miratun (2001). ''Kritajnatar Ashrubindu (Gauri Ayyub: A Memorial Volume)'', p.37. Dey’s Publishing, Kolkata. ISBN 81-7612-750-7.</ref> Her serious interest in Tagore studies was manifested by her involvement with the Tagore Research Institute in Kolkata and in several of her articles (see below). Teaching and social work for her were often inextricably linked. Recognizing that the [[Religious conflicts in India|communal divide between Hindus and Muslims]] in India had a strong socio-economic origin and the more disadvantaged community had to be brought to the mainstream, she organised – in her own small way – English coaching classes for poor Muslim students who had enrolled for the civil service entrance tests.

=== Social worker ===
Her entry into active social work was triggered by the riot between Hindus and Muslims that ravaged Kolkata in 1964.<ref>[http://news.bbc.co.uk/onthisday/hi/dates/stories/january/13/newsid_4098000/4098363.stm Calcutta riots]</ref> Under the leadership of [[Maitreyi Devi]], she and several other intellectuals and social workers of the time, founded the Council for Promotion of Communal Harmony. The activities of the CPCH were not limited to intellectual discussions, but involved camps and often risky visits to troubled areas and encounters with the active fundamentalist elements from both communities.<ref name="hal">Haldar, Niranjan ''Gauri Ayyub smaraney (In memory of Gauri Ayyub)'', in Ed: Nahar, Miratun (2001). ''Kritajnatar Ashrubindu (Gauri Ayyub: A Memorial Volume)'', p. 60. Dey’s Publishing, Kolkata. ISBN 81-7612-750-7.</ref> Though serious communal riots never recurred in Bengal since 1964, Gauri – through her writing and by other means – continued with her efforts to bridge the social, economic and religious barriers between the two communities. She was also seriously involved in the popularisation of family planning among economically disadvantaged families. She was an office bearer of the [[Family Planning Association of India]].<ref>[http://www.fpaindia.org/ Family Planning Association]</ref>

Gauri Ayyub played an active role in aid of the Bangladesh freedom movement of 1971, providing not only moral and social support for many displaced people, but actively arranging for aid, shelter and healthcare for refugees. One of her most significant contributions was in co-founding "Khelaghar",<ref name="khela" /> a shelter for Bangladeshi children orphaned during the atrocities. In 2012, the Government of Bangladesh honoured her (posthumously) with the Friends of Liberations War Honour<ref>[http://archive.thedailystar.net/newDesign/news-details.php?nid=254732 Liberation War Honour]</ref> in recognition of her many contributions.

=== Social activist ===
Never someone to shy away from controversy and debate, she continued to get involved in a series of sociopolitical issues. She was particularly perturbed by the imposition of emergency during 1975–77, and the consequent curtailment of civil liberties.<ref name="hal" /> During this period, she kept risking arrest and imprisonment by attending rallies to raise the collective social conscience and holding closed door meetings (often at her own home) with prominent leaders such as [[Jayaprakash Narayan]] and social activists such as [[Gour Kishore Ghosh]], who both spent much of this period behind bars.<ref>Dutta, Jyotirmoy (1998) ''Dheu-samudrer shikharey ekti batighar'', Desh 8 August (1998) p. 80</ref> However, she never showed interest in actually joining mainstream politics, which she probably felt would have compromised her independence.

=== Writer ===
Her active social conscience forced her to write regularly on outstanding social and political issues of her time. The relatively few short stories that she wrote in Bengali were marked not only for their perceptiveness, but "unostentatious beauty".<ref>Majumdar, Swapan (1987). ''Indian Literature'', Vol. 30, No. 6, p.38 (http://www.jstor.org/stable/23337257)</ref> She collaborated with her husband in producing two books of translations <ref>Abu Sayeed Ayyub and Gauri Ayyub (1976) ''Ghalib-er Ghazal thekey'', Dey's Publishing, Kolkata</ref><ref>Abu Sayeed Ayyub and Gauri Ayyub (1987) ''Meer-er Ghazal thekey'', Dey's Publishing, Kolkata</ref> of the Urdu poets, Ghalib and Meer. She took formal lessons in Urdu for this purpose. She also collaborated with her erstwhile student, Kyoko Niwa, to translate a travelogue by the famed 17th century Japanese poet, [[Matsuo Basho]], into Bengali.<ref>Sachidanand, Uma ''Translation and Research on Japanese Literature in India'', in Ed. George, P.A. (2006) ''East Asian Literature: An interface with Inda'', p.267, Northern Book Center, New Delhi, ISBN 81-7211-205-X</ref> Not reflected in her own bibliography, however, is her very significant contribution to the transcription and production of much of Abu Sayeed Ayyub’s <ref name="asa" /> literary output, a substantial part of which came after he was physically incapacitated by [[Parkinson’s Disease]]. For many years, she would assiduously transcribe the dictated manuscripts and copy them again and again, incorporating repeated corrections, till they finally met the exacting literary standards of her husband.

=== Grandmother ===
Her deep affection and interest in her only grandchild, Shreya Ahana, resulted in a small book of memories, humorous yet timeless and poignant. The underlying pathos in ''Ei je Ahana'' (Here is Ahana) arose from the essential transitoriness of their interaction: the grandmother was eternally torn away from her granddaughter who was with her parents in Mumbai, by her many commitments in Kolkata.

== Literary works ==

=== Books ===
* ''Tuchcha Kichu Sukh Dukkha'' (Dey’s Publishing, 1986): Collection of short stories in Bengali.
* ''Door Prodesher Sankirno Path'' (Dey’s 1990): Bengali Translation of a Japanese Travelogue (with Kyoko Niwa). 
* ''Ei je Ahana'' (Papyrus,1996): Stories of her granddaughter (in Bengali).

=== Selected articles ===
* ''Kabilnamah thekey Basudhara: Asampurna Parichoyer sutrey (An Incomplete Understanding)'', Chaturanga September 1984, pp 412–422
* ''Hindu-Musalman Birodh: Rabindranather Chokhey (The Hindu-Muslim Conflict: Tagore’s Perspective)'', Chaturanga January 1988, pp 777–790
* ''Rabindrik Shikshay Mukti O Srinkhala (Freedom and Discipline in Tagore’s Teachings)'', Rabindra Bhavna Vol. 18, No.1 (1994) pp 1–27
* ''Khandita Pratima (Dissected Idols)'', Desh 14 August 1997, pp 28–32

== Further reading ==
* Nahar, Miratun (ed.) ''Kritajnatar Ashrubindu – Gauri Ayyub: Smarakgrantha (A Memorial Publication)'', (Dey’s Publishing, Kolkata 2001). 
* Winter, Joe ''Gauri Ayyub – An Integrated Human Being'', ibid, pp 119–120.

== References ==
{{reflist}}

{{DEFAULTSORT:Ayyub, Gauri}}
[[Category:1931 births]]
[[Category:1988 deaths]]
[[Category:Indian women social workers]]
[[Category:Indian women activists]]
[[Category:Writers from Kolkata]]
[[Category:Patna University alumni]]
[[Category:Visva-Bharati University alumni]]
[[Category:University of Calcutta alumni]]
[[Category:University of Calcutta faculty]]
[[Category:20th-century Indian short story writers]]
[[Category:Bengali writers]]
[[Category:20th-century Indian translators]]
[[Category:Indian women short story writers]]
[[Category:Indian women novelists]]
[[Category:Writers from Patna]]
[[Category:20th-century Indian women writers]]
[[Category:20th-century Indian novelists]]
[[Category:Women writers from Bihar]]
[[Category:Indian women translators]]