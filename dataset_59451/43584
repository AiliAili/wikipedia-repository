<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox Aircraft Begin
 | name=Type 166
 | image=Short-166-chb100.jpg
 | caption=Short Admiralty Type 166 on HMS Ark Royal, 1916.
}}{{Infobox Aircraft Type
 | type=[[Torpedo-bomber]] and [[reconnaissance aircraft|reconnaissance floatplane]]
 | national origin={{flag|United Kingdom}}
 | manufacturer=[[Short Brothers]]
 | designer=
 | first flight=1916
 | introduced=
 | retired=
 | status=
 | primary user=[[Royal Naval Air Service]]
 | number built=26
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Short Type 166''' was a 1910s [[United Kingdom|British]] two-seat reconnaissance, bombing and [[torpedo]] carrying [[Short Folder|folder]] [[seaplane]], designed by [[Short Brothers]].

==Development==
The Short Type 166 was designed as a 'folder' aircraft to operate from the [[HMS Ark Royal (1914)|Ark Royal]] as a torpedo-bomber. Six aircraft, known within Shorts as the ''Type A'', were originally ordered before the outbreak of [[World War I]], and were assigned the Admiralty serial numbers 161 to 166.<ref name=BandJp101/> As was normal at the time, the type was designated the Admiralty Type 166, after the naval serial number of the last aircraft in the batch. Sometimes, the aircraft are referred to as the Short S.90 (S.90 was the manufacturer's serial number of the first aircraft, naval serial 161).

The ''Type 166'' was similar to the earlier Short Type 136, but slightly larger, and was designed from the start as a torpedo carrier, although it was never used in that rôle.<ref name=BandJp101>Barnes & James, p.101.</ref>

==Design==
The ''Type 166'' was a two-bay biplane with twin wooden pontoon floats, with a water rudder fitted to the tail float, plus a stabilizing float mounted near the wing-tip under each lower wing. It was powered by a nose-mounted 200&nbsp;hp (149&nbsp;kW) Salmson engine.

A follow-on order for 20 aircraft was assembled by [[Westland Aircraft]] at its Yeovil factory.<ref name="whl">[http://history.whl.co.uk/short166.html Westland History - Short 166]</ref> The Westland built aircraft did not have provision for a torpedo, but could carry three 112&nbsp;lb bombs, and were fitted to carry radio equipment. There was also a [[Lewis gun]], that was operated by the observer in the rear cockpit.<ref name=BandJp101/>

==Operators==
;{{flag|Greece|old}}
*[[Hellenic Navy]]
;{{UK}}
*[[Royal Naval Air Service]]

==Specifications==
{{Aircraft specs
|ref=Westland History,<ref name="whl" /> Barnes & James<ref>Barnes & James, p.106</ref>
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=12.38
|length ft=
|length in=
|span m=17.45
|span ft=
|span in=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|height m=4.29
|height ft=
|height in=
|height note=<ref name="Bruce p482">Bruce 1957, p.482.</ref>
|wing area sqm=
|wing area sqft=575
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=3,500
|empty weight note=
|gross weight kg=
|gross weight lb=4,580
|gross weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Salmson 2M7]]
|eng1 type=14-cyl. 2-row water-cooled radial piston engine
|eng1 kw=149.1
|eng1 hp=<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=65
|max speed kts=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|cruise speed kts=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|range km=
|range miles=
|range nmi=
|combat range km=
|combat range miles=
|combat range nmi=
|endurance=4 hours
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following 
specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=
*[[List of aircraft of the Royal Naval Air Service]]
*[[List of seaplanes and flying boats]]
}}

==References==
===Notes===
{{reflist}}

===Bibliography===
*{{cite book|last1=Barnes|first1=C.H.|last2=James|first2=D.N|title=Shorts Aircraft since 1900|publisher=Putnam|year=1989|location=London|pages=560|isbn=0-85177-819-4}}
*{{cite book|last=Bruce|first=J.M.|title=British Aeroplanes 1914-18|year=1957|publisher=Putnam
|location=London}}
*{{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}
*[http://www.flightglobal.com/pdfarchive/view/1956/1956%20-%201802.html Flight International 1956]

==External links==
{{commons category|Short Admiralty Type 166}}
*[http://flyingmachines.ru/Site2/Crafts/Craft29691.htm Short Admiralty Type 166 at flyingmachines.ru]

{{Short Brothers aircraft}}
{{Admiralty aircraft type numbers}}

[[Category:British bomber aircraft 1910–1919]]
[[Category:Floatplanes]]
[[Category:Short Brothers aircraft|Type 166]]
[[Category:Single-engine aircraft]]
[[Category:Biplanes]]
[[Category:Propeller aircraft]]