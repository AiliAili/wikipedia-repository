{{Orphan|date=November 2015}}

{{eastern name order|Chan S.K. Hugo}}
{{Infobox president
|name= Dr. Hugo Sai K. Chan
|image= 
|birth_place=[[Hong Kong]]
|alma_mater= <br> [[LLB]], [[University of Southampton]] (1973 - 1978); [[D.Litt.]] (Doctor of Letters), [[Olivet Nazarene University]](Hons)}}

'''Dr. Hugo Sai Keung Chan''' ({{zh|t=陳世強|}}; pinyin: Chen Shi Qiang, born 1955 in Hong Kong): is a member of the Election Committee of the Chief Executive of Hong Kong SAR,<ref>http://www.christianweekly.net/4DACTION/W4D_CWREAD/13294/11/BACK/</ref> and the National Director of the Full Gospel Business Men's Fellowship International in Hong Kong.

==Education and Legal Career==
In 1973, Dr. Chan went to England to further his studies and read law at the [[University of Southampton]]. After finishing his Solicitors Qualifying Examination in 1979, he returned to Hong Kong and started his legal career at Messrs, Johnson, Stokes & Master. He is a partner of the law firm of Liau, Ho and Chan,<ref>[http://www.lhc.com.hk/home.htm], Liau, Ho & Chan Solicitors & Notaries</ref> focusing on Property Developments, Commercial and China Business. Dr. Chan is a Notary Public, a China Appointed Attesting Officer and a Member of the Hong Kong Law Society Mainland Legal Affairs Committee.

==Family Background==
Dr. Chan met his wife Yuenyi at University and got married to her in 1981. They have 3 daughters and 3 grandchildren.

Dr. Chan comes from one of the oldest indigenous clans in the New Territories of Hong Kong. His Hakka family ancestral hall, [[Sam Tung Uk Museum]], situated at the centre of Tsuen Wan, is now a museum of local traditions and culture. Dr. Chan’s father Chan Lau Fong (陳流芳), JP., BBS., had served as District Board Chairman of Tsuen Wan from 1994 - 1999.

==Business==
Dr. Chan is a partner of AR Evans Capital Partners Ltd <ref>{{cite web|url=http://www.arevans.com/keyman05.htm |title=Key Individuals Mr. Hugo Chan - A R Evans Capital Partners Limited &#124; Capital &#124; Financial &#124; Investment Bank in Asia |publisher=Arevans.com |date= |accessdate=2011-11-24}}</ref> which provides corporate advisory and mergers & acquisitions services.

==Community Services==
Dr. Chan is the Chairman of the Hong Kong Character City Movement,<ref>http://www.theotexture.com/hong-kong-character-city-movement Chairman of Hong Kong Character City Movement</ref><ref>{{cite web|url=http://www.charactercity.hk/boardofdirectors.html |title=香港有品 |publisher=Charactercity.hk |date= |accessdate=2011-11-24}}</ref> which is a non-profit organization promoting the culture of good character and conducting training amongst schools, corporations and institutions. He is also the Chairman of the Happymen Foundation,<ref>http://www.docstoc.com/docs/25204384/HAPPYMEN-FOUNDATION-CHARITY-GOLF-DAY Happymen Foundation</ref> a non-profit organisation promoting the culture of gratitude and projects of happiness.

During 2003 to 2006, Dr. Chan was the Host to a weekly TV program「星火飛騰」on Asia Television Hong Kong. He is the Co-Founder of the Kingdom Revival Times「國度復興報」, a leading Christian Newspaper published by the Kingdom Ministry Hong Kong.

Dr. Chan is the Founding Vice-President of the Hong Kong Professionals and Senior Executives Association,<ref>http://www.rthk.org.hk/rthk/news/englishnews/20070905/news_20070905_56_429527.htm,Hong Kong Professionals and Senior Executives Association</ref> which is an association consisting of leaders from the major professions, as well as the business and academic communities.

On 30 October 2011, Dr. Chan was elected as one of the 10 members representing the Christian Sector in the 1200 Member Election Committee of the Chief Executive of Hong Kong Special Administrative Region.

Dr. Chan is the National Director of the Full Gospel Business Men's Fellowship International in Hong Kong.<ref>http://www.fgbmfamerica.com/wordpress/?p=555,Full Gospel Business Men's Fellowship International</ref>

Dr. Chan has previously served as a Member of China People's Political Consultative Conference in Shenzhen and Changchun. He is an Appointed Legal Advisor to the Overseas Chinese Returnees in Shenzhen.

In recognition of his contributions to the Christian and business communities.,<ref>{{cite web|url=http://www.alpha.org.hk/eng/people/event_review/GCAC/review_GCAC.htm |title=Global Chinese Alpha Conference |publisher=Alpha.org.hk |date=2007-04-11 |accessdate=2011-11-24}}</ref> Dr. Chan was conferred an Honorary Doctor Degree of Letters (D. Litt) by the Olivet Nazarene University, USA.<ref>{{cite web|url=http://www.olivet.edu/news/newsDetails.aspx?Channel=%2FChannels%2FSite+Wide&WorkflowItemID=b344d27d-471f-4060-ac02-f736715fb0a0 |title=Awarded D.Litt by Olivet Nazarene University |publisher=Olivet.edu |date=2010-05-08 |accessdate=2011-11-24}}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Chan, Hugo}}
[[Category:Hong Kong legal professionals]]
[[Category:Hong Kong people of Hakka descent]]
[[Category:People from Bao'an County]]
[[Category:Indigenous inhabitants of the New Territories in Hong Kong]]
[[Category:Living people]]