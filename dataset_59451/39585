{{Infobox college coach
| name = Ralph Friedgen
| image = FriedgenLastGameCropped.jpg
| alt = 
| caption = Friedgen in 2010
| sport = [[American football|Football]]
| current_title = 
| current_team = 
| current_conference = 
| current_record = 
| contract = 
| birth_date = {{Birth date and age|1947|4|4|mf=y}}
| birth_place = [[Harrison, New York]]
| death_date = 
| death_place = 
| alma_mater = 
| player_years1 = 1966–1968
| player_team1 = [[Maryland Terrapins football|Maryland]]
| player_positions = [[Offensive guard]]
| coach_years1 = 1969–1971
| coach_team1 = [[Maryland Terrapins football|Maryland]] ([[Graduate assistant|GA]])
| coach_years2 = 1973–1976
| coach_team2 = [[The Citadel Bulldogs football|The Citadel]] (DL)
| coach_years3 = 1977–1979
| coach_team3 = [[The Citadel Bulldogs football|The Citadel]] (Offensive coordinator)
| coach_years4 = 1980
| coach_team4 = [[William & Mary Tribe football|William & Mary]] (OC)
| coach_years5 = 1981
| coach_team5 = [[Murray State Racers football|Murray State]] (OC)
| coach_years6 = 1982–1986
| coach_team6 = [[Maryland Terrapins football|Maryland]] (OC/OL)
| coach_years7 = 1987–1991
| coach_team7 = [[Georgia Tech Yellow Jackets football|Georgia Tech]] (OC/QB)
| coach_years8 = 1992–1993
| coach_team8 = [[San Diego Chargers]] (RGC/TE)
| coach_years9 = 1994–1996
| coach_team9 = [[San Diego Chargers]] (OC)
| coach_years10 = 1997–2000
| coach_team10 = [[Georgia Tech Yellow Jackets football|Georgia Tech]] (OC/QB)
| coach_years11 = 2001–2010
| coach_team11 = [[Maryland Terrapins football|Maryland]]
| coach_years12 = 2014–2015
| coach_team12 = [[Rutgers]] (OC/SA)
| overall_record = 75–50
| bowl_record = 5–2
| tournament_record = 
| championships = 1 [[Atlantic Coast Conference|ACC]] (2001)
| awards = [[Broyles Award]] (1999)<br>[[AFCA Coach of the Year|AFCA COY]] (2001)<br>[[Associated Press College Football Coach of the Year Award|Associated Press COY]] (2001)<br>[[Eddie Robinson Coach of the Year|Eddie Robinson COY]] (2001)<br>[[George Munger Award]] (2001)<br>[[Home Depot Coach of the Year Award|Home Depot COY]] (2001)<br>[[Sporting News College Football Coach of the Year|Sporting News COY]] (2001)<br>[[Walter Camp Coach of the Year Award|Walter Camp COY]] (2001)<br>[[Touchdown Club of Columbus#The Woody Hayes Trophy|Woody Hayes Trophy]] (2001)<br />[[Bobby Dodd Coach of the Year Award|Bobby Dodd COY]] (2001)<br>2× [[Atlantic Coast Conference football honors|ACC Coach of the Year]] (2001, 2010)
| coaching_records = 
}}

'''Ralph Harry Friedgen''' (born April 4, 1947) is an [[American football]] coach. He was most recently the special assistant coach for [[Rutgers Scarlet Knights football|Rutgers]] after serving as the [[offensive coordinator]] in the 2014 season. He was the head coach at the [[University of Maryland, College Park]] from 2000 to 2010.  After the 2010 regular season, it was announced that Friedgen would not be returning for the 2011 season, ending his ten-year run as head coach.  Affectionately known as "The Fridge", Friedgen was previously an [[offensive coordinator]] at Maryland, [[Georgia Tech Yellow Jackets football|Georgia Tech]], and in the [[National Football League]] (NFL) with the [[San Diego Chargers]].

==Early life and education==
Friedgen was born on April 4, 1947 in [[Harrison, New York]]. His father, "Big Ralph" Friedgen, attended [[Fordham University]], where he played from 1938 to 1939, and coached [[high school football]] for 30&nbsp;years.<ref>[http://www.fordhamsports.com/auto_pdf/p_hotos/s_chools/ford/sports/m-footbl/auto_pdf/History ''2010 Fordham Football Media Guide''] (PDF), "All-Time Roster", p. 113, Fordham University, 2010.</ref><ref name=lords>[http://sportsillustrated.cnn.com/vault/article/magazine/MAG1024026/index.htm Lords Of Discipline; From Maryland to Missouri, from Northwestern to Alabama, a new wave of hard-line coaches is laying down the law], ''Sports Illustrated'', October 22, 2001.</ref> The younger Friedgen worked under his father as a water boy and manager, and the two often attended [[New York Giants]] and [[New York Jets|Jets]] games together.<ref name=home>Ben Weber, [http://findarticles.com/p/articles/mi_m0FIH/is_2_72/ai_n18614091/?tag=content;col1 You can go home again! It took Ralph Friedgen 32 years to become the head coach at his alma mater, and only one year to start making history!], '' Coach and Athletic Director, September 2002.</ref> He attended [[Harrison High School (New York)|Harrison High School]] where he played [[quarterback]] on his father's team.<ref name=home/> John Nugent, the head coach of Harrison's rival [[Rye High School (Rye, New York)|Rye High School]], recommended that his brother, Maryland head coach [[Tom Nugent]], recruit Friedgen.<ref name=home/> His recruitment was handled by [[Lee Corso]], then an assistant coach at the school.<ref name=home/> After his first season at Maryland, Nugent was fired as head coach, and his successor [[Lou Saban]] moved Friedgen to [[fullback (American football)|fullback]] to fill in for an injured teammate.<ref name=home/> The following year, Maryland had a new coach, and [[Bob Ward (American football)|Bob Ward]] again changed Friedgen's position, this time to [[offensive guard]], although he had never [[block (American football)|block]]ed before.<ref name=home/> Upset about the constant turnover at head coach and position changes, Friedgen received a favorable recommendation to transfer from coach Ward, but his father said, "You can transfer, but when you get home, the key you have is not going to fit the door because we're changing the lock. Quitters don't live here."<ref name=home/> He remained at Maryland as a guard and later said the experience taught him a lesson in perseverance.<ref name=home/> As an undergraduate, he was a member of [[Phi Delta Theta]] Fraternity.<ref>http://www.phideltdc.com/chapters.html</ref> After completion of his [[bachelor's degree]] in [[physical education]] in 1970, Friedgen served as a graduate assistant at his alma mater, before later accepting positions on the staffs of [[The Citadel (Military College)|The Citadel]], [[College of William and Mary|William and Mary]], and [[Murray State University|Murray State]]. Joining him on many of these coaching stops was [[Frank Beamer]], who is the former coach at [[Virginia Tech]].

==Assistant coaching career==
Friedgen returned to the University of Maryland in 1982 to serve as [[offensive coordinator]] under head coach [[Bobby Ross]], who was his mentor during his tenure at [[The Citadel (Military College)|The Citadel]]. During this time period, he had a hand in the development of [[quarterback]]s [[Stan Gelbaugh]] and [[Frank Reich]], and most notably [[Boomer Esiason]]. It was also during this time that the University of Maryland football program was a perennial top-20 team, winning three consecutive [[Atlantic Coast Conference]] championships from 1983 to 1985 and appearing in prominent bowl games. Following a sub-par 1986 season, and amidst an athletic department quagmire due in large part to the [[Len Bias]] incident, Friedgen followed Ross to [[Georgia Institute of Technology|Georgia Tech]], a period lasting four years. In 1990, Georgia Tech went from being unranked in the preseason to achieving an 11-0-1 record and a share of the national championship with [[University of Colorado at Boulder|Colorado]]. In 1992, Friedgen followed Ross again, this time to the [[National Football League|NFL]]'s [[San Diego Chargers]], where he orchestrated an offense that led the franchise to an appearance in [[Super Bowl XXIX]].<ref>{{cite web|url=http://umterps.collegesports.com/sports/m-footbl/mtt/friedgen_ralph00.html|title=Player Bio: Ralph Friedgen|work=umterps.cstv.com|publisher=Maryland Terrapins|accessdate=2007-08-04}}</ref> In 1997, Friedgen returned to [[Georgia Institute of Technology|Georgia Tech]], where, as offensive coordinator, he developed the balanced offensive attack (200 yards on the ground, 200 yards through the air) that would become his trademark. During his second year, the [[Yellow Jackets]] were co-champions of the ACC, defeated [[University of Notre Dame|Notre Dame]] in the [[Gator Bowl]], and ended the season ranked among the nation's top 10 teams. In 1999, he was the winner and awarded the [[Frank Broyles]] Award, given to the nation's top assistant coach. Friedgen brought 32 years of assistant coaching experience (including 21 years as an offensive coordinator either in college or the NFL) with him upon his return to College Park.

==Head coaching career==
In November 2000, Ralph Friedgen was named the Head Coach of the University of Maryland football team. He was charged with rebuilding a struggling program that had only one winning season and no bowl game appearances since 1990.

===2001 season===
{{Main|2001 Maryland Terrapins football team}}
Friedgen's tenure opened against [[2001 North Carolina Tar Heels football team|North Carolina]], and early in the first quarter, running back [[Willie Parker]] ran 77&nbsp;yards for a touchdown.<ref>[http://www.usatoday.com/sports/scores101/101244/101244402.htm North Carolina vs. Maryland], ''USA Today'', September 1, 2001.</ref> Maryland came back to win, 23–7, which made Friedgen the first Terrapins coach to win his opener since [[Tom Nugent]] in 1959.<ref>[https://news.google.com/newspapers?id=66E_AAAAIBAJ&sjid=81UMAAAAIBAJ&pg=4629,6756902&dq=ralph+friedgen+north-carolina&hl=en Terps Enjoy Opening Victory], ''The Mount Airy News'', September 3, 2001.</ref> During the season, Friedgen led Maryland to a surprising 10–2 record, a top 10 national ranking, the first outright [[Atlantic Coast Conference]] (ACC) title by a team other than [[Florida State University|Florida State]] since the Seminoles entered the league in 1992 and the school's first conference title since 1985, and an appearance in the 2002 [[Orange Bowl (game)|Orange Bowl]]&mdash;the Terrapins' first major bowl bid in more than two decades. Friedgen received numerous "Coach of the Year" plaudits including the [[Bobby Dodd Coach of the Year Award]], the [[Eddie Robinson Coach of the Year]], [[The Home Depot Coach of the Year Award]], and the [[Walter Camp Coach of the Year]].

Throughout the year, Friedgen had challenged his players with the phrase "Are you in or are you out?" After the dramatic first-year turnaround, he was a high-profile candidate for an NFL position, and his players repeated his question. He remained at Maryland and said, "Last year the kids made a commitment to me and I realized it was my turn. We've got plenty left to do. This program has not yet arrived."<ref>Albert Chen, [http://sportsillustrated.cnn.com/vault/article/magazine/MAG1026414/index.htm 16 Maryland: Now that everyone is committed to the program, the Terps are ready to prove that last season was no shell game], ''Sports Illustrated'', August 12, 2002.</ref>

===2002 season===
{{Main|2002 Maryland Terrapins football team}}
Friedgen's second year began with a 1–2 record, and he implored his team to consider it the start of a "new season". Maryland then won nine of its remaining ten regular season games, including a come-from-behind [[homecoming]] victory against the [[Philip Rivers]]-led 15th-ranked [[2002 NC State Wolfpack football team|NC State]] team. ''Sports Illustrated'' credited a "stifling defense", dynamic special teams play, and an offense that thrived under quarterback [[Scott McBrien]] and a simplified playbook despite the loss of leading rusher [[Bruce Perry]] to injury.<ref>Daniel G. Habib, [http://sportsillustrated.cnn.com/vault/article/magazine/MAG1027619/2/index.htm College Football], ''Sports Illustrated'', November 25, 2002.</ref> Maryland ended the season with a 30-3 victory over [[2002 Tennessee Volunteers football team|Tennessee]] in the [[2002 Peach Bowl]], the school's first bowl victory since the [[Cherry Bowl]] in 1985. The team achieved a final record of 11–3, matching the school record for wins in a season first set by the [[1976 Maryland Terrapins football team|1976 team]].

===2003 season===
{{Main|2003 Maryland Terrapins football team}}
In 2003, the Terrapins finished with a 10-3 record, including a 41–7 victory over rival [[West Virginia University|West Virginia]] in the 2004 [[Gator Bowl]]. The University of Maryland football team became one of five programs nationally to reach the ten-win plateau from 2001 to 2003, and Friedgen became the first coach in [[Atlantic Coast Conference|ACC]] history to win ten or more games in his first three seasons as a head coach.

===2004 season===
{{Main|2004 Maryland Terrapins football team}}
The 2004 season was the first disappointment of Friedgen's tenure, and the team failed to qualify for a bowl game. Highlights included, on October 30, an upset of fifth-ranked [[2004 Florida State Seminoles football team|Florida State]] to take away the first-ever Maryland win in that series, as well as the first defeat of a top-five team since 1982. On November 27, a 13–7 win over [[Wake Forest University|Wake Forest]] gave Friedgen his 36th win as head coach, which made him the winningest fourth-year coach in conference history.

===2005 season===
{{Main|2005 Maryland Terrapins football team}}
The 2005 season again saw Maryland fail to qualify for a bowl game. Friedgen's team started out with a 4–2 start, but a lack of offensive efficiency and a propensity for unforced turnovers&mdash;both of which may be attributed in part to an injury sustained by quarterback [[Sam Hollenbach]]&mdash;caused the team to win only one of its last five games to finish with a 5–6 record for a second consecutive season.  The 2005 season did see the first [[Crab Bowl Classic]] game since 1965.

===2006 season===
{{Main|2006 Maryland Terrapins football team}}
After two losing seasons, Friedgen led a Maryland resurgence in 2006 and ended speculation on his job security.<ref name=jobneverindoubt/> ''[[The Baltimore Sun]]'' surmised that Friedgen would have repeated as ACC Coach of the Year had it not been for [[2006 Wake Forest Demon Deacons football team|Wake Forest]]'s impressive season under [[Jim Grobe]].<ref name=jobneverindoubt>[http://pqasb.pqarchiver.com/baltsun/access/1169976451.html?dids=1169976451:1169976451&FMT=ABS&FMTS=ABS:FT&type=current&date=Nov+26%2C+2006&author=RICK+MAESE&pub=The+Sun&desc=FOR+ABOUT+7+MILLION+REASONS%2C+FRIEDGEN%3FS+JOB+NEVER+IN+DOUBT&pqatl=google FOR ABOUT 7 MILLION REASONS, FRIEDGEN'S JOB NEVER IN DOUBT], ''The Baltimore Sun'', November 26, 2006.</ref> Maryland qualified for a bowl game for the first time since 2003. Despite being outgained by every one of its eleven Division I FBS opponents, the Terrapins started the season 8–2, highlighted by a 28–26 victory over [[2006 Virginia Cavaliers football team|Virginia]] in which the team stormed back from a 20–0 halftime deficit to defeat the Cavaliers. Maryland also became the first team since 1985 to defeat both Florida State and [[2006 Miami Hurricanes football team|Miami]] in the same year. The Terrapins defeated [[2006 Purdue Boilermakers football team|Purdue]] 24–7 in the [[Champs Sports Bowl]] to give Friedgen his 50th win as Maryland's head coach. Friedgen's 50 wins in six seasons ties him with former [[North Carolina Tar Heels football|North Carolina]] head coach [[Dick Crum (football coach)|Dick Crum]] for the second most wins by a sixth-year coach in the ACC (former [[Clemson Tigers football|Clemson]] head coach [[Danny Ford]] holds the record with 52 wins in his first six seasons.)

===2007 season===
{{Main|2007 Maryland Terrapins football team}}
The 2007 season saw Maryland defeat tenth-ranked [[Rutgers]] on the road, as well as eighth-ranked [[Boston College]] in the regular-season home finale. This marked the first time in history that the school had beaten two top-10 teams in the same season. Maryland joined [[University of Kentucky|Kentucky]], [[Louisiana State University|LSU]], and [[University of Illinois, Urbana-Champaign|Illinois]] as the only teams to accomplish this feat in 2007. A 37–0 shutout of NC State allowed the Terrapins to qualify for postseason play for the fifth time in Friedgen's seven seasons.

On December 28, Maryland played [[Oregon State]] in the [[Emerald Bowl]] and scored on the first drive. The Terrapins eventually lost, 21–14.

===2008 season===
{{Main|2008 Maryland Terrapins football team}}
Maryland entered [[2008 NCAA Division I FBS football season|the 2008 season]] with 30 senior players, the largest class since Friedgen took over as head coach in 2001.<ref>[http://web1.ncaa.org/football/exec/roster?year=2008&org=392 2008 Maryland Football Roster], National Collegiate Athletic Association, 2008, retrieved February 2, 2009.</ref><ref>Patrick Stevens, [http://www.washingtontimes.com/news/2008/dec/30/terps-seniors-seek-a-proper-send-off Terps' seniors seek a proper send off], ''[[The Washington Times]]'', December 30, 2008.</ref> Despite the experienced team, expectations were low and the ACC's preseason poll projected Maryland to finish fifth among the six teams in the Atlantic Division.<ref>[http://www.theacc.com/sports/m-footbl/spec-rel/072108aae.html 2008 ACC Football Preseason Selections], Atlantic Coast Conference, July 21, 2008.</ref> At the end of summer training, and amidst some controversy, senior Jordan Steffy was named the starting quarterback over junior [[Chris Turner (American football)|Chris Turner]] who had finished the 2007 campaign atop the [[depth chart]].<ref>[http://www.washingtontimes.com/weblogs/d1scourse/2008/Aug/18/direct-from-franklin/ Direct from Franklin], ''The Washington Times'', August 18, 2008.</ref>

In the season-opener, Maryland used all three of its quarterbacks to edge Division I FCS [[Delaware Fightin' Blue Hens football|Delaware]], 14–7.<ref>[http://sports.espn.go.com/ncf/playbyplay?gameId=282430120&period=0 Delaware vs. Maryland Complete Play-by-Play], ESPN, August 30, 2008.</ref> The following week, the Terrapins were beaten decisively by [[Middle Tennessee Blue Raiders football|Middle Tennessee State]], 14–24,<ref>[http://sports.espn.go.com/ncf/recap?gameId=282502393 Craddock, Middle Tennessee surprise Maryland], ESPN, September 6, 2008.</ref> and some pundits predicted Friedgen was on the coaching "hot seat" and that his job was in peril.<ref>[http://voices.washingtonpost.com/terrapins-insider/2008/09/friedgen_off_the_hot_seat.html Friedgen off the hot seat?], ''The Washington Post'', September 13, 2008.</ref><ref>[http://collegefootball.rivals.com/content.asp?SID=1144&CID=839343 Could Friedgen really be on the hot seat?], [[Rivals.com]], August 19, 2008, retrieved February 2, 2009.</ref> Maryland rebounded to record wins against four Top-25 ranked opponents. In week twelve, the Terrapins possessed a 7–3 record and were ranked first in the Atlantic Division with two regular season games remaining. Maryland lost both, however, and the team's standing fell.<ref>[http://sports.espn.go.com/ncf/recap?gameId=283340103 Another Flutie makes key play to carry BC into ACC Championship], ESPN, November 29, 2008.</ref> The [[2008 Humanitarian Bowl|Humanitarian Bowl]] in [[Boise, Idaho]] selected the Terrapins to play the [[Western Athletic Conference]]'s number-two team, [[2008 Nevada Wolf Pack football team|Nevada]]. In an offensive shoot-out against the nation's number-five offensive team, Maryland triumphed with a final result of 42–35.<ref>[http://sports-ak.espn.go.com/ncf/recap?gameId=283652440 Scott benched early, then leads Maryland past Nevada], ESPN, Associated Press, December 30, 2008.</ref> The Terrapins posted an 8–5 (4–4 ACC) record and Friedgen extended his postseason tally to 4–2, with twice as many bowl wins as any other coach in school history.

===2009 season===
{{Main|2009 Maryland Terrapins football team}}
The Terrapins finished the season 2–10, with narrow victories against the [[James Madison Dukes football|James Madison Dukes]] and [[Clemson Tigers football|Clemson Tigers]]. [[Turnover (football)|Turnovers]], a lack of talent, injuries, inexperience, and poor [[offensive line]] play were cited as causes for the poor season.<ref name="wsh_times_28_11_2009">{{cite news|url=http://www.washingtontimes.com/news/2009/nov/28/breaking-down-why-terrapins-broke-down/?page=2|title=Breaking down why Terrapins broke down|last=Stevens|first=Patrick|date=28 November 2009|work=[[The Washington Times]]|accessdate=28 November 2009}}</ref>

Friedgen's job security was questioned,<ref name="washingtonpost.com">[http://www.washingtonpost.com/wp-dyn/content/article/2009/11/05/AR2009110504786.html Terps relish chance to right the ship], ''The Washington Post'', November 5, 2009.</ref> but with a $1.75 million salary, there was some question as to whether the University of Maryland can afford to buy out his remaining contract.<ref name="washingtontimes.com">[http://washingtontimes.com/news/2009/oct/26/first-down-40478986/ Maryland Football Sinking], ''The Washington Times'', October 26, 2009.</ref> According to contracts, Friedgen was to retire after the 2011 season and [[James Franklin (American football coach)|James Franklin]] would have become the new head coach.<ref name="washingtonpost.com"/> ''[[The Baltimore Sun]]'' chronicled fans frustrations with Friedgen, his staff, and the arrangement with Franklin.<ref name="balt_sun_nov_1_2009">{{cite news|url=http://www.baltimoresun.com/sports/terps/bal-sp.terpsfoot01nov01,0,2881580.story|title=Terps fans restless over coach-in-waiting|last=Barker|first=Jeff|date=November 1, 2009|work=The Baltimore Sun|accessdate=22 November 2009}}</ref>

In November 2009, ''[[The Washington Post]]'' quoted anonymous sources that a buyout of Friedgen's contract of over $4 million was possible.<ref name="wp_11_22_2009">{{cite news|url=http://www.washingtonpost.com/wp-dyn/content/article/2009/11/21/AR2009112101712.html?hpid=artslot|title=Sources: Friedgen buyout a possibility|last=Prisbell|first=Eric|author2=Steve Yanda |date=November 22, 2009|work=[[The Washington Post]]|accessdate=22 November 2009}}</ref>  Friedgen's attorney, Jack Reale, said that neither he nor Friedgen had been approached about a buyout.  The article also said that Friedgen "privately resented" the athletic department naming Franklin his successor, due to how it affects other members of the coaching staff.<ref name="wp_11_22_2009" />  It was also reported that Friedgen has support from the leaders of the [[Terrapin Club]] and the [[Maryland Gridiron Network]] booster groups.<ref name="bsun_11_22_2009">{{cite news|url=http://www.baltimoresun.com/sports/terps/bal-sp.terpsfoot22nov22,0,4519986.story|title=Speculation about Friedgen's job after loss|last=Barker|first=Jeff|date=November 22, 2009|work=[[Baltimore Sun]]|accessdate=22 November 2009}}</ref> Two former Terp basketball players and prominent members of the State University system, [[Len Elmore]] and [[Tom McMillen]], expressed doubt that public funds would be used to buy out Friedgen's contract, and boosters of the program said they knew of no effort to raise private funds for that purpose.<ref name="bsun_11_28_2009">{{cite news|url=http://www.baltimoresun.com/sports/terps/bal-sp.terpsfoot24nov24,0,5437494.story|title=Buying out Friedgen could be problem, UM officials say|last=Barker|first=Jeff|date=November 24, 2009|work=The Baltimore Sun|accessdate=28 November 2009}}</ref>

On December 1, 2009, it was announced Friedgen would return to coach Maryland in 2010.<ref name="bsun_12_1_2009">{{cite news|url=http://www.baltimoresun.com/sports/terps/bal-friedgen-stays,0,4646431.story?track=rss|title=Friedgen will return for 10th season|last=Barker|first=Jeff|date=December 1, 2009|work=The Baltimore Sun|accessdate=1 December 2009}}</ref>

===2010 season===
{{Main|2010 Maryland Terrapins football team}}
[[File:FriedgenLastGame.jpg|thumb|right|Friedgen and his team take the field in the [[2010 Military Bowl]] ]]
Maryland went 8–4 during the regular season.<ref name="wpNCSTate2010">{{cite news|last=Prisbell|first=Eric|title=Maryland football earns 38-31 win over North Carolina State |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/11/27/AR2010112703662.html|accessdate=28 November 2010|newspaper=[[The Washington Post]]|date=28 November 2010}}</ref> For the turnaround, the Atlantic Coast Conference named Friedgen the ACC Coach of the Year.<ref name=coy>[http://www.umterps.com/sports/m-footbl/spec-rel/113010aaa.html Friedgen, O'Brien Honored by the ACC Friedgen earns league's coach of the year award for the second time; O'Brien first-ever Terp rookie of the year], University of Maryland, November 30, 2010.</ref>

After Maryland won seven of its first ten games, questions about Friedgen's job security diminished.<ref name="2Nov2010Feinstein">{{cite news|last=Feinstein|first=John|title=Looks like Friedgen might be sticking around for a while  |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/11/01/AR2010110106539.html|accessdate=14 November 2010|newspaper=[[The Washington Post]]|date=2 November 2010}}</ref><ref name="washpost13Nov2010">{{cite news|last=Yanda|first=Steve|title=Maryland-Virginia has implications beyond this season  |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/11/12/AR2010111203252.html|accessdate=14 November 2010|newspaper=[[The Washington Post]]|date=13 November 2010}}</ref>  On November 19, 2010, Maryland athletic director [[Kevin Anderson (athletic director)|Kevin Anderson]] announced that Friedgen would be returning for the 2011 season, the final year of his contract.<ref name="sun_staying">{{cite news|last=Barker|first=Jeff|title=Friedgen returning to Maryland for final year of contract |url=http://articles.baltimoresun.com/2010-11-19/sports/bs-sp-terps-friedgen-1119-20101118_1_money-for-special-football-maryland-gridiron-network-larry-grabenstein|accessdate=21 November 2010|newspaper=Baltimore Sun|date=18 November 2010}}</ref>  Despite this announcement, after offensive coordinator [[James Franklin (American football coach)|James Franklin]] accepted a job at [[Vanderbilt Commodores football|Vanderbilt]] and offered positions to four other members of the staff,<ref>[http://www.washingtonpost.com/wp-dyn/content/article/2010/12/17/AR2010121706734.html Maryland is likely to ask Friedgen to accept buyout], ''The Washington Post'', December 17, 2010.</ref> Anderson did not answer questions about Friedgen's future on December 17.<ref name="espn17Dec2010">{{cite web|last=Schad|first=Joe|title=Sources: Ralph Friedgen buyout on tap? |url=http://sports.espn.go.com/ncf/news/story?id=5931102&campaign=rss&source=ESPNHeadlines|work=ESPN.com|accessdate=18 December 2010}}</ref>
On December 18, 2010, it was reported in The Washington Post that the school was terminating him as head coach and had offered him a buyout of his remaining contract valued at $2,000,000.<ref>{{cite news | url = http://www.washingtonpost.com/wp-dyn/content/article/2010/12/18/AR2010121802878.html | title = Ralph Friedgen out as Maryland football coach | last1 = Prisbel | first1= Eric | last2 = Yanda | first2= Steve | work = The Washington Post | date = December 18, 2010 | accessdate = 2010-12-18}}</ref>  On December 20, 2010, athletic director [[Kevin Anderson (athletic director)|Kevin Anderson]] made an official announcement that Friedgen would not be returning for the 2011 season.<ref name="washPostFridgeFired">{{cite news|last=Yanda|first=Steve|author2=Prisbell, Eric|title=Maryland fires Ralph Friedgen after coach refuses to retire  |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/12/20/AR2010122005589.html|accessdate=21 December 2010|newspaper=[[The Washington Post]]|date=20 December 2010}}</ref>  In an interview with [[WNST]] in Baltimore, Friedgen said he was so angry over the firing that he burned his Maryland diploma and now roots mainly for Georgia Tech, though later revoked that statement.<ref>[http://espn.go.com/college-football/story/_/id/7039655/maryland-terrapins-former-coach-ralph-friedgen-says-burned-diploma Ralph Friedgen still mad about dismissal]. [[ESPN]], 2011-09-30.</ref><ref name=wpostOct2011>{{cite news|last=Prisbell|first=Eric|title=Former Maryland coach Ralph Friedgen: ‘I did not actually burn my diploma. I was only trying to make a joke’|url=http://www.washingtonpost.com/blogs/terrapins-insider/post/former-maryland-coach-ralph-friedgen-i-did-not-actually-burn-my-diploma-i-was-only-trying-to-make-a-joke/2011/10/06/gIQA4Mv0PL_blog.html|work=[[The Washington Post]]|accessdate=2 January 2013|date=October 6, 2011}}</ref>

===Coaching tree===
While at Maryland, Friedgen had many assistants continue their coaching careers elsewhere. Four of these individuals have received head coaching positions themselves and are denoted below in '''boldface'''.<ref>[http://www.washingtontimes.com/weblogs/d1scourse/2009/Jan/06/where-are-they-now-former-friedgen-assistants/ Where are they now? Former Friedgen assistants], ''The Washington Times'', January 6, 2009.</ref>
*'''[[James Franklin (American football coach)|James Franklin]]''' – Current [[Penn State Nittany Lions football|Penn State]] head coach. Franklin was one of two [[Ron Vanderlinden]] assistants retained when Friedgen took over in 2001. He was Maryland [[wide receivers coach]] from 2000 to 2004 and offensive coordinator from 2008 to 2010.<ref name="vanderbilt.edu">[http://www.vanderbilt.edu/coach/franklin-official-release/ James Franklin: Vanderbilt Football Head Coach], Vanderbilt University, December 17, 2010.</ref>
*'''[[Mike Locksley]]''' – Former [[New Mexico Lobos football|New Mexico]] head coach and current Maryland offensive coordinator. Locksley was one of two [[Ron Vanderlinden]] assistants retained when Friedgen took over in 2001. He served as Maryland's [[running backs coach]] from 1997 to 2002. The last two seasons were under Friedgen, during which time Locksley also served as the recruiting coordinator.<ref name="vanderbilt.edu"/>
*'''[[Charlie Taaffe]]''' – Current [[UCF Knights football|UCF]] offensive coordinator. From 2007 to 2008, he was the head coach for the [[Hamilton Tiger-Cats]] of the [[Canadian Football League]]. Charlie Taaffe served as offensive coordinator and quarterbacks coach under Ralph Friedgen from 2001 to 2005. In his first campaign with the Terrapins, he helped guide the program to the 2001 Atlantic Coast Conference Championship and a spot on the FedEx Orange Bowl. In each of his first two seasons at Maryland, the Terrapins broke their school record for scoring, registering 390 points in 2001 and then 451 points the following year. Maryland played in three bowl games during Taaffe's tenure, recording victories in the [[2002 Peach Bowl]] over Tennessee and the [[2004 Gator Bowl]] versus West Virginia. Under Taaffe's tutelage, quarterbacks Shaun Hill and Scott McBrien both garnered All-ACC honors.
*'''[[Bill O'Brien (American football)|Bill O'Brien]]''' – [[New England Patriots]] offensive assistant coach and [[quarterbacks coach]] on Head Coach Bill Belichick's staff, and current Head Coach of the [[Houston Texans]]. Bill O'Brien served 14 seasons as an assistant coach at the collegiate level, including four seasons as an offensive coordinator. He coached in the Atlantic Coast Conference for 12 seasons, including tenures at Maryland (2003–04), Georgia Tech (1995–2002) and Duke (2005–06). He served as the offensive coordinator and quarterbacks coach at Duke University for two seasons prior to leaving Maryland.  He served as Ralph Friedgen's running backs coach from 2003 to 2004. In his first season with the Terrapins in 2003, Maryland finished second in the ACC in rushing and defeated West Virginia in the Gator Bowl.

==Personal life==
Friedgen has been married to his wife Gloria (née Spina) since 1973. They have three daughters.

==Head coaching record==
{{CFB Yearly Record Start | type = coach | team = | conf = | bowl = | poll = both}}
{{CFB Yearly Record Subhead
 | name      = [[Maryland Terrapins football|Maryland Terrapins]]
 | startyear = 2001
 | conf      = [[Atlantic Coast Conference]]
 | endyear   = Present
}}
{{CFB Yearly Record Entry
 | championship = conference
 | year         = [[2001 NCAA Division I-A football season|2001]]
 | name         = [[2001 Maryland Terrapins football team|Maryland]]
 | overall      = 10–2
 | conference   = 7–1
 | confstanding = 1st
 | bowlname     = [[2002 Orange Bowl|Orange]]
 | bowloutcome  = L
 | bcsbowl      = yes
 | ranking      = 10
 | ranking2     = 11
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2002 NCAA Division I-A football season|2002]]
 | name         = [[2002 Maryland Terrapins football team|Maryland]]
 | overall      = 11–3
 | conference   = 6–2
 | confstanding = T–2nd
 | bowlname     = [[2002 Peach Bowl|Peach]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 13 
 | ranking2     = 13
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2003 NCAA Division I-A football season|2003]]
 | name         = [[2003 Maryland Terrapins football team|Maryland]]
 | overall      = 10–3
 | conference   = 6–2
 | confstanding = 2nd
 | bowlname     = [[2004 Gator Bowl|Gator]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 20
 | ranking2     = 17
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2004 NCAA Division I-A football season|2004]]
 | name         = [[2004 Maryland Terrapins football team|Maryland]]
 | overall      = 5–6
 | conference   = 3–5
 | confstanding = T–8th
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      = 
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship =
 | year         = [[2005 NCAA Division I-A football season|2005]]
 | name         = [[2005 Maryland Terrapins football team|Maryland]]
 | overall      = 5–6
 | conference   = 3–5
 | confstanding = T–4th <small>(Atlantic)<small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2006 NCAA Division I FBS football season|2006]]
 | name         = [[2006 Maryland Terrapins football team|Maryland]]
 | overall      = 9–4
 | conference   = 5–3
 | confstanding = T–2nd <small>(Atlantic)<small>
 | bowlname     = [[2006 Champs Sports Bowl|Champs Sports]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2007 NCAA Division I FBS football season|2007]]
 | name         = [[2007 Maryland Terrapins football team|Maryland]]
 | overall      = 6–7
 | conference   = 3–5
 | confstanding = T–5th <small>(Atlantic)<small>
 | bowlname     = [[2007 Emerald Bowl|Emerald]]
 | bowloutcome  = L
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2008 NCAA Division I FBS football season|2008]]
 | name         = [[2008 Maryland Terrapins football team|Maryland]]
 | overall      = 8–5
 | conference   = 4–4
 | confstanding = T–3rd <small>(Atlantic)<small>
 | bowlname     = [[2008 Humanitarian Bowl|Humanitarian]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2009 NCAA Division I FBS football season|2009]]
 | name         = [[2009 Maryland Terrapins football team|Maryland]]
 | overall      = 2–10
 | conference   = 1–7
 | confstanding = 6th <small>(Atlantic)<small>
 | bowlname     = 
 | bowloutcome  = 
 | bcsbowl      =
 | ranking      = 
 | ranking2     = 
}}
{{CFB Yearly Record Entry
 | championship = 
 | year         = [[2010 NCAA Division I FBS football season|2010]]
 | name         = [[2010 Maryland Terrapins football team|Maryland]]
 | overall      = 9–4
 | conference   = 5–3
 | confstanding = T–2nd <small>(Atlantic)<small>
 | bowlname     = [[2010 Military Bowl|Military]]
 | bowloutcome  = W
 | bcsbowl      =
 | ranking      = 24
 | ranking2     = 23
}}
{{CFB Yearly Record Subtotal
 | name       = Maryland
 | overall    = 75–50
 | confrecord = 43–37
}}
{{CFB Yearly Record End
 | overall  = 75–50
 | bcs      = 
 | poll     = two
 | polltype = 
}}

==References==
{{Reflist|2}}

==External links==
{{Portal|Biography|College football}}
* {{CFBCR|815|Ralph Friedgen}}

{{Maryland Terrapins football coach navbox}}
{{Navboxes
| title = Ralph Friedgen—championships, awards, and honors
| list1 =
{{1990 Georgia Tech Yellow Jackets football navbox}}
{{AFCA Coach of the Year}}
{{Associated Press College Football Coach of the Year Award}}
{{Broyles Award}}
{{Bobby Dodd Award winners}}
{{Eddie Robinson Coach of the Year}}
{{George Munger Award}}
{{Home Depot Coach of the Year}}
{{Sporting News College Football Coach of the Year}}
{{Walter Camp Coach of the Year}}
}}

{{DEFAULTSORT:Friedgen, Ralph}}
[[Category:1947 births]]
[[Category:Living people]]
[[Category:The Citadel Bulldogs football coaches]]
[[Category:Georgia Tech Yellow Jackets football coaches]]
[[Category:Maryland Terrapins football players]]
[[Category:Maryland Terrapins football coaches]]
[[Category:Murray State Racers football coaches]]
[[Category:San Diego Chargers coaches]]
[[Category:William & Mary Tribe football coaches]]
[[Category:People from Harrison, New York]]