<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{|{{Infobox Aircraft Begin
|name= Short 330 
|image=MVA Shorts 330 at Minneapolis - 9 Sept 1983.jpg 
|caption=A Short 330 of [[Mississippi Valley Airlines]] at [[Minneapolis-Saint Paul International Airport]] in 1985
}}{{Infobox Aircraft Type
|type = [[Airliner|Transport aircraft]]
|manufacturer=[[Short Brothers]]
|first flight= 22 August 1974
|introduced=1976 
|retired=
|primary user= [[Air Cargo Carriers]]
|more users =  [[Corporate Air]]
|produced=1974–1992
|number built= 330-100: 68<ref name="Barnes">Barnes and James, p. 533-535.</ref><br> 330-200: 57<ref name=Barnes/>
|unit cost=
|developed from = [[Short Skyvan]]
|variants with their own articles = [[Short 360]] <br/>[[C-23 Sherpa|C-23A Sherpa]]
}}
|}

The '''Short 330''' (also '''SD3-30''') is a small transport aircraft produced by [[Short Brothers]]. It seats up to 30 people and was relatively inexpensive and had low maintenance costs at the time of its introduction in 1976. The 330 was based on the [[Short Skyvan|SC.7 Skyvan]].

==Development==
The Short 330 was developed by [[Short Brothers]] of [[Belfast]] from Short's earlier [[Short Skyvan]] STOL utility transport. The 330 had a longer wingspan and fuselage than the Skyvan, while retaining the Skyvan's square-shaped fuselage cross section, allowing it to carry up to 30 passengers while retaining good short field characteristics.<ref name="donald world">Donald, David, ed. ''The Encyclopedia of World Aircraft''. London: Aerospace Publishing, 1997. ISBN 1-85605-375-X.</ref> The first prototype of the 330 flew on 22 August 1974.<ref name="Janes88 p304">Taylor 1988, p. 304.</ref>

The Short 330 is unusual in having all of its fuel contained in tanks located directly above the ceiling of the passenger cabin.<ref name="Janes88 p304"/> There are two separate cockpit doors for pilot and co-pilot for access from inside the cabin.<ref>{{cite web|url=http://www.airliners.net/photo/Muk-Air/Short-330-200/0890366/L/&sid=0b86b0cc7185997d97d0f26567e44912|title=Airliners.net - Aviation Photography, Discussion Forums & News|publisher=}}</ref>

While Short concentrated on producing airliners, the design also spawned two freight versions. The first of these, the '''Short 330-UTT''' (standing for ''Utility Tactical Transport''), was a military transport version fitted with a strengthened cabin floor and paratroop doors,<ref name="Janes88 p306">Taylor 1988, p. 306</ref> which was sold in small numbers, primarily to Thailand, which purchased four. The '''Short Sherpa''' was a freighter fitted with a full-width rear cargo door/ramp. This version first flew on 23 December 1982,<ref name="Janes88 p306"/> with the first order, for 18 aircraft, being placed by the [[United States Air Force]] (USAF) in March 1983, for the European Distribution System Aircraft (EDSA) role, to fly spare parts between USAF bases within Europe.<ref name="Janes88 p306"/>
[[File:Short 330 (DLT) at FRA 1983-02-28 (1).jpg|thumb|Short 330 (DLT) at [[Frankfurt Airport|FRA]] Feb. 28, 1983]]

==Operational history==
[[File:Henson Airlines Shorts 330 at Baltimore - 11 September 1983.jpg|thumb|Short 330 of [[Henson Airlines]] at [[Baltimore-Washington International Airport]] on 11 September 1983]]
The basic Short 330 was a passenger aircraft intended as a short-range regional and commuter airliner, and had been designed to take advantage of US regulations which allowed commuter airlines to use aircraft carrying up to 30 passengers,<ref name="donald civil">Donald 1999, p. 709–714.</ref> thereby replacing smaller types such as the [[Beechcraft Model 99]] and the [[de Havilland Canada DHC-6 Twin Otter]]. The Short 330 entered service with [[Time Air]] (a Canadian airline) in [[1976 in aviation|1976]]. Despite its somewhat portly looks (one regional airline affectionately dubbed it the "Shed" <ref>[http://www.airliners.net/info/stats.main?id=353 Shorts 330]</ref>), it soon proved to be an inexpensive and reliable 30-seat airliner.

The 330 was somewhat slower than most of its pressurised competition, but it built up a reputation as a comfortable, quiet and rugged airliner.<ref name="Smith p.2">Smith 1986, p. 2.</ref> The quiet running of the Pratt & Whitney PT6A-45R was largely due to an efficient reduction gearbox.<ref name="Smith p.2"/> The cabin was the result of a collaboration with Boeing engineers who modelled the interior space, fittings and decor after larger airliners. The use of a sturdy structure complete with the traditional Short braced-wing-and-boxy-fuselage configuration also led to an ease of maintenance and serviceability.<ref name="Smith p.2"/>

Production ended in [[1992 in aviation|1992]] with a total of approximately 136 being built (including freighter and military versions).<ref>[http://www.airliners.net/info/stats.main?id=353 Airliners.net: Short 330] Access date: 18 June 2007</ref> As of 1998, approximately 35 were still in service. The 330's design was refined and heavily modified, resulting in the [[Short 360]].

==Variants==
* '''330-100''' was the original production model with Pratt & Whitney Canada PT6A-45A and -45B turboprop engines.<ref name="Frawley_2003">Frawley 2003, p. 193.</ref>
* '''330-200''' included minor improvements and more powerful PT6A-45R engine.<ref name="Frawley_2003" />
* '''330-UTT''' was the Utility Tactical Transport version of the 330-200, with a strengthened cabin floor and inward-opening paratroop doors.
* '''Sherpa''' was a freighter version of the 330-200 with a full width rear cargo ramp.
* '''[[C-23 Sherpa]]''' A, and B variants are military configured Short Sherpas.

==Operators==

===Civilian operators===
A total of 24 Short 330 aircraft (all variants) were in airline service as of August 2008, with [[Air Cargo Carriers]] (13), [[Corporate Air]] (3), [[Arctic Circle Air Service]] (2), [[Deraya Air Taxi]] (2), [[Mountain Air Cargo]] (2), [[Freedom Air (Guam)|Freedom Air]] (1), and [[McNeely Charter Service]] (1).<ref name="FI2008">"World Airliner Census", ''[[Flight International]]'', 19–25 August 2008.</ref> As of July 2011 the number in commercial service had decreased to 15 with the same seven operators; Air Cargo Carriers operating seven and the remaining eight aircraft in service with the other six.<ref>[http://www.flightglobal.com/airspace/media/reports_pdf/emptys/87145/world-airliner-census-2011.pdf ''Flight International'' 2011 World Airliner Census], p.22; retrieved 31 August 2011</ref>

===Military Operators===
;{{USA}} :
* [[United States Air Force]] (C-23)
* [[United States Army]] (C-23)

===Former Military Operators===
;{{TAN}}
* [[Military of Tanzania|Tanzanian Air Force]] (former UAEAF 330-UTT)
;{{THA}} :
* [[Royal Thai Army]]
** [[Army Aviation Center]] (Short 330-UTT)
* [[Royal Thai Police]]
** [[Police Air Division]] (Short 330-UTT)<ref>[http://www.scramble.nl/mil/7/rtaf/orbat.htm Scramble on the Web: Thai Armed Forces - Aircraft Order of Battle] Access date: 18 June 2007</ref>
;{{UAE}} :
* [[United Arab Emirates Air Force]] (Short 330-UTT)<ref>[http://www.scramble.nl/ae.htm Scramble on the Web: United Arab Emirates Air Force Order of Battle] Access date: 18 June 2007</ref>
;{{VEN}}
*[[Venezuelan Air Force]] (Short 330)

==Accidents and incidents==
As of December 2015 the aircraft type have suffered two fatal accidents in civilian use:
* 3 August 1989: an [[Olympic Aviation]] Short 330, operating as [[Olympic Aviation Flight 545]], crashed on a hillside in [[Samos]] island, [[Greece]], while attempting a landing approach in thick fog. All 3 crew members and 31 passengers were killed.
*25 May 2000: a Streamline Aviation Short 330 was hit by a departing aircraft when it entered an active runway at Paris-Charles de Gaulle Airport. The wingtip of the departing plane slashed through the Shorts cockpit and killed one of its pilots.

In addition to these two accidents there have been at least 16 hull-loss occurrences, i.e. non fatal accidents where the plane have been damaged beyond repair.<ref>{{cite web|url=http://aviation-safety.net/database/types/Shorts-330/losses|title=Aviation Safety Network > ASN Aviation Safety Database > Type index > ASN Aviation Safety Database results|first=Harro|last=Ranter|publisher=}}</ref>

==Specifications (330-200)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's All the World's Aircraft, 1988-1989<ref name="Janes88 p305-6">Taylor 1988, pp. 305–306.</ref> 
|crew=Three (two [[aviator|pilot]]s plus one [[flight attendant|cabin crew]])
|capacity=30 passengers
|length alt= 17.69 m
|length main= 58 ft 0½ in
|span alt= 22.76 m
|span main= 74 ft 8 in
|height alt= 4.95 m
|height main= 16 ft 3 in
|area alt= 42.1 m²
|area main= 453 ft²
|airfoil= NACA 63A series (modified)
|empty weight alt= 6,680 kg
|empty weight main= 14,727 lb
|loaded weight main=  
|loaded weight alt=  
|useful load main=  
|useful load alt=  
|max takeoff weight alt= 10,387 kg
|max takeoff weight main= 22,900 lb
|more general=
|engine (prop)= [[Pratt & Whitney Canada PT6]]A-45-R
|type of prop= turboprop
|number of props=2
|power alt= 893 kW
|power main= 1,198 hp
|max speed alt= 245 knots, 455 km/h
|max speed main= 293 mph 
|max speed more=
|cruise speed alt= 190 knots, 352 km/h
|cruise speed main= 218 mph
|cruise speed more= max cruise at 22.000 ft (4.167 m)
|stall speed alt= 73 knots, 136 km/h
|stall speed main= 85 mph
|stall speed more=(flaps and landing gear down)
|never exceed speed main=  
|never exceed speed alt=  
|range alt= 915 nmi 1,695 km
|range main= 1,053 mi
|range more= (no reserves, passenger version, 1,966 kg payload)
|ceiling alt= 6,400 m
|ceiling main= 26.000 ft
|climb rate alt= 6 m/s
|climb rate main= 1,180 ft/min
|loading alt= 247 kg/m²
|loading main= 50.6 lb/ft²
|power/mass alt= 0.17 kW/kg
|power/mass main= 0.052 hp/lb
|more performance=
|armament=
|avionics=
}}

==See also==
{{aircontent
|related=
* [[Short 360]]
* [[Short C-23 Sherpa]]
* [[Short SC.7 Skyvan]]
|similar aircraft=
|lists=
* [[List of civil aircraft]]
|see also=
}}

==References==

===Notes===
{{reflist|2}}

===Bibliography===
{{refbegin}}
* Barnes C.H. and James Derek N. ''Shorts Aircraft since 1900''. London: Putnam, 1989. ISBN 0-85177-819-4.
* Donald, David, ed. ''The Encyclopedia of Civil Aircraft''. London: Aurum, 1999. ISBN 1-85410-642-2.
* Frawley, Gerard. ''The International Directory of Civil Aircraft, 2003/2004''. London: Aerospace Publications Pty Ltd., 2003. ISBN 1-875671-58-7.
* Smith, P.R. ''Shorts 330 and 360'' (Air Portfolios 2) London: Jane's Publishing Company Limited, 1986. ISBN 0-7106-0425-4.
* Taylor, John W.R., ed. ''Jane's All the World's Aircraft, 1988-1989''. London: Jane's Information Group, 1988. ISBN 0-7106-0867-5.
{{refend}}

==External links==
{{Commons category|Short 330}}
* [http://www.airliners.net/info/stats.main?id=353 Short 330 page on Airliners.net]
* [http://www.globalsecurity.org/military/systems/aircraft/c-23.htm C-23 page on Global Security.org]

{{Short Brothers aircraft}}

[[Category:British airliners 1970–1979]]
[[Category:Short Brothers aircraft|330]]
[[Category:Twin-engined tractor aircraft]]
[[Category:1976 introductions]]