{{Multiple issues|
{{BLP sources|date=January 2013}}
{{Notability|Academics|date=April 2014}}
}}

'''Denis Mickiewicz''' [pron: {{IPA|mɪtsˈkjɛ.vɪtʃ}}] (born in 1929) is Professor Emeritus of Russian Literature at [[Duke University]]<ref>http://slaviceurasian.duke.edu/people</ref> and the founding conductor of the [[Yale Russian Chorus]].<ref>http://www.yrcalums.org/DenisMickiewicz.html</ref>

==Early life==

Dr. Mickiewicz was born into a Russian family in [[Latvia]], or possibly [[Estonia]],<ref><Lecture, Duke University, September 2003></ref> which emigrated during World War II to  [[Salzburg]], Austria. As a boy, his mother taught him to play the guitar and the piano, and he sang in the children’s choir of the Orthodox Cathedral in [[Riga]]. In Salzburg, he  enrolled in the [[Mozarteum]], and graduated from gymnasium.  He served as assistant to the choir conductor of the Orthodox Archbishop’s Church in Salzburg, and played Viennese music, jazz, and [[Romani people|Gypsy]] music in various bands. In 1952, Mickiewicz and his family emigrated to the United States.

==Yale education and the Yale Russian Chorus==

[[File:YRCSochi58.jpg|thumb|Denis Mickiewicz playing his guitar and leading the Yale Russian Chorus in song in a park at Yalta (USSR) on the Black Sea in 1958]]

Mickiewicz enrolled in the [[Yale School of Music]] in 1953, earning a B.A. in Music at [[Yale University]] in 1957. He then entered the graduate program in Slavic Languages and Literatures at Yale, receiving  an M.A. and then a Ph.D. from Yale.  While still an undergraduate at Yale, he and George Litton of the Yale Russian Club, founded the Yale Russian Chorus,<ref>http://www.yalerussianchorus.com</ref> which began as a small group of Russian language students.  Over time the Chorus developed a broad repertoire of folk, liturgical, and classical music arranged or transcribed by Mickiewicz.  In 1958, with members of the [[New Haven Symphony Orchestra]], they performed a concert version of [[Mikhail Glinka]]’s opera [[A Life for the Tsar]].<ref>http://www.yrcalums.org/history.html</ref>  By 1963, the group had performed all over the United States, as well as in Britain, France, Germany, Switzerland, Luxembourg, and Spain; highlights were performances at [[Salle Pleyel]] in Paris, at [[Carnegie Hall]] in New York, and at [[Wigmore Hall]] in London..  In 1962, the Chorus won first prize at the Radiodiffusion Française Festival International de Chant Choral at [[Lille]]. Shortly afterwards, [[Philips Records]] invited Mickiewicz and the Chorus to record their repertoire in Paris.<ref>http://www.yrcalums.org/discography.html</ref>
<!-- Deleted image removed: [[File:DenisMickiewiczSanders1963.jpg|thumb|Denis Mickiewicz conducting the Yale Russian Chorus in a concert in Sanders Theatre in Boston in 1963]]  -->

Mickiewicz marked out an original, culturally bold role for the Russian Chorus.  The [[Soviet Union]] was hostile to much of the Russian cultural past, including liturgical and pre-Revolutionary songs.  To help preserve and revive that culture, Mickiewicz and the Chorus went to the Soviet Union several times, first in 1958, to present their Russian and American repertoire directly to Russian citizens.  Singing in squares and on streets, they were hailed as effective cultural ambassadors at a time of serious political tensions.<ref>http://www.yrcalums.org/Humph_all.pdf</ref>

==Professional career==
[[File:DenisMickiewiczDuke2005.jpg|thumb|Denis Mickiewicz conducting the Alumni of the Yale Russian Chorus in a concert at Duke University in October 2005]]

Mickiewicz taught Russian and Comparative literature at [[Connecticut College]], [[Michigan State University]], [[Emory University]] and finally  Duke University, acting as Chairman of his department at both Emory and Duke.  He maintained his musical activities, co-founding the Lansing Opera Company in Michigan, and steadfastly retained his links with the [http://www.yrcalums.org Alumni of the Yale Russian Chorus], returning regularly to lead alumni concerts, including the 25th Anniversary concert in Carnegie Hall in 1978, the 50th Anniversary concert in [[Woolsey Hall]] at Yale University in 2003,<ref>http://www.yrcalums.org/2003_Wall_St_Journal.pdf</ref> the 60th Anniversary concert in [[Woolsey Hall]] at Yale University in 2013, as well as concerts in [[Duke Chapel]] at Duke University in 2005 and 2009.

While publishing research on modernist poetics,<ref>http://www.v-ivanov.it/wp-content/uploads/2010/12/mickiewicz_viacheslav_ivanovs_apollini_1992_text.pdf</ref> Mickiewicz also returned to composing music.  His Duo for Clarinet and Piano was premiered in Atlanta in 1989, and he wrote the choral and incidental music for a production of [[Euripides]]’ [[Hecuba (play)|Hecuba]] presented at Emory University.  His setting of The Lord’s Prayer was premiered and recorded by the Chamber Choir of Washington University in St. Louis in 2000.  In 2003, on the occasion of the 50th Anniversary Concert of the Yale Russian Chorus in Woolsey Hall at Yale, Mickiewicz was awarded the Yale Music School's Cultural Leadership Citation for his extraordinary service to Yale and the international music community.<ref name=duke.edu>http://m.today.duke.edu/2004/04/mickiewicz_0404.html</ref> In 2004, He was presented with the Russian Federation's Medal "In Commemoration of the 300th Anniversary of St. Petersburg" at the Russian Embassy in Washington DC for his contribution in the field of American-Russian cultural relations, particularly for teaching, performing and presenting Russian music.<ref name=duke.edu/> Most recently, he set to music for baritone and piano the verse of the [[Russian symbolism|Russian Symbolist]] poet [[Vyacheslav Ivanov (poet)|Vyacheslav Ivanov]].  It was performed in St. Petersburg, Russia, in 2002.   His scholarly research on Ivanov has been published in Rome and St. Petersburg.<ref>http://www.rvb.ru/ivanov/3_bibliography/davidson/01text/1992.htm</ref><ref>http://www.rvb.ru/ivanov/3_bibliography/davidson/01text/1971.htm</ref><ref>http://nissan-auto.org/knigi/12276-bashnya-vyacheslava-ivanova-i-kultura-serebryanogo.html (in Russian)</ref>

==Personal life==
He is married to Ellen Mickiewicz, Professor of Public Policy and Political Science at the [[Sanford School of Public Policy]]<ref>http://fds.duke.edu/db/Sanford/faculty/epm</ref> at Duke University and an expert in mass media and democratization in the [[former Soviet Union]].

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* Alumni of the Yale Russian Chorus [http://www.yrcalums.org]
* The Yale Russian Chorus [http://www.yalerussianchorus.com]

{{DEFAULTSORT:Mickiewicz, Denis}}
[[Category:Articles created via the Article Wizard]]
[[Category:1929 births]]
[[Category:Living people]]
[[Category:Duke University people]]
[[Category:American choral conductors]]