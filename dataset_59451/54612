{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:_Books -->
| name           = Horse-Shoe Robinson
| title_orig     =
| translator     =
| image          = File:Major Butler and Horseshoe Robinson-Southern Life in Southern Literature 091.png
| image_size     = 
| caption  = Major Butler and Horseshoe Robinson
| author         = [[John P. Kennedy]] 
| cover_artist   =
| country        = United States
| language       = English
| series         = 
| genre          = 
| pages          = 2 vol. (1835 U.S.); 3 vol. (1835 U.K.)
| publisher      = Philadelphia: Carey, Lea & Blanchard / New York: Wiley and Long
| release_date   = 1835
| isbn           = <!-- NA -->
| preceded_by    = 
| followed_by    = 
}}

'''''Horse-Shoe Robinson: A Tale of the Tory Ascendency''''' is an 1835 novel by [[John P. Kennedy]] that was a popular seller in its day.<ref name="hart">Hart, James D. [https://books.google.com/books?id=ZHrPPt5rlvsC The Popular Book: A History of America's Literary Taste], p. 305 (1951)</ref><ref name="knickreview">(July 1835) [https://books.google.com/books?id=_awRAAAAYAAJ&pg=PA71&lpg=PA71&dq=#v=onepage&q&f=false Literary Notices (book review)], ''[[The Knickerbocker]]'', Vol. VI, No. 1, p. 71</ref>

The novel was Kennedy's second, and proved to be his most popular.  It is a work of historical romance of the [[American Revolution]], set in the western mountain areas of the [[The Carolinas|Carolinas]] and [[Virginia]],<ref name="docsouth">Lemon, Armistead. [http://docsouth.unc.edu/southlit/kennedyhorseshoe/summary.html Summary], in ''Documenting the American South'' website, Retrieved 8 December 2014</ref> culminating at the [[Battle of Kings Mountain]].<ref name="western">(November 1835). [https://books.google.com/books?id=1i44AQAAMAAJ&pg=RA1-PA350#v=onepage&q&f=false Critical Notices (book review)], ''The Western Monthly Magazine'', p. 350</ref><ref name="third35">(September 1835). [https://books.google.com/books?id=HBEAAAAAYAAJ&pg=PA240#v=onepage&q&f=false Miscellaneous Notices (book review)], ''The American Quarterly Review'', Vol. 18, pp. 240-42</ref>

The primary characters of the novel include [[Francis Marion]], [[Banastre Tarleton]], General [[Charles Cornwallis, 1st Marquess Cornwallis|Charles Cornwallis]], Horseshoe Robinson (so named because he was originally a blacksmith), Mary Musgrove and her lover John Ramsay, Henry and Mildred Lyndsay (patriots), Mildred's lover Arthur Butler (who she secretly marries), and Habershaw with his gang of rogues and Indians.<ref name="library">[[Charles Dudley Warner|Warner, Charles Dudley]], ed. [https://books.google.com/books?id=Kk0mAQAAMAAJ&pg=PA269#v=onepage&q&f=false Library of the World's Best Literature, Vol. XXX], p.269 (1898)</ref>

==Play==

The novel was adapted for the stage a number of times, but the best known were by Charles Dance in 1836, which starred actor [[James Henry Hackett]], and a version created in 1856 by [[Clifton W. Tayleure]] titled ''Horseshoe Robinson, or the Battle of King's Mountain'', which included William Ellis as Robinson and [[George C. Boniface]] as Major Arthur Butler.<ref name="drama1">Burt, Daniel S. [https://books.google.com/books?id=VQ0fgo5v6e0C&pg=PA205#v=onepage&q&f=false The Chronology of American Literature], p. 205 (2004)</ref><ref name="dramas">Bank, Rosemarie. [https://books.google.com/books?id=96lCI145-38C&pg=PA152#v=onepage&q&f=false Frontier Melodrama], in Ogden, Dunbar H. et al., ''Theatre West: Image and Impact'', pp. 151-52 (1990)</ref><ref name="hisc">Hischak, Thomas S. [https://books.google.com/books?id=DiI1wIyatvUC&pg=PA317&lpg=PA317&#v=onepage&q&f=false The Oxford Companion to American Theatre], p. 317 (2004)</ref>

==References==
{{reflist}}

==External links==
* [https://books.google.com/books?id=-q1EAAAAYAAJ&pg= Horse Shoe Robinson: A Tale of the Tory Ascendency] ("Revised Edition", G.P. Putnam and Sons, New York, 1872) (via Google books)
* [https://books.google.com/books?id=MOuapxk4BakC 1835 English edition, Vol. 1 of 3] (London: Richard Bentley) (1835), [https://books.google.com/books?id=qafdB3onvzEC Vol. 2], [https://books.google.com/books?id=ZHwuAAAAYAAJ Vol. 3.]

[[Category:1835 novels]]
[[Category:19th-century American novels]]
[[Category:American Revolutionary War novels]]
[[Category:Novels set in South Carolina]]
{{1830s-novel-stub}}