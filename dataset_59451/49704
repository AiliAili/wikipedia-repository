{{about|the novel|the film|La Bête Humaine (film)}}
{{Infobox book
| italic title   = 
| name           = La Bête humaine
| image          = Image-La Bete Humaine Cover.jpg
| image_size     = 
| border         = yes
| alt            = 
| caption        = [[Lithograph]] advertisement for La Bête humaine, 1889
| author         = [[Émile Zola]]
| title_orig     = 
| orig_lang_code = fr
| title_working  = 
| translator     = 
| illustrator    = 
| cover_artist   = 
| country        = France
| language       = French
| series         = [[Les Rougon-Macquart]]
| release_number = 
| subject        = 
| genre          = [[psychological thriller]]
| set_in         = France
| published      = {{plainlist|
* 1890 ([[Bibliothèque-Charpentier]], French)
* 1968 ([[Signet Classic]], English)
}}
| media_type     = Print ([[Serial (literature)|Serial]], [[Hardcover]], [[Paperback]])
| pages          = 
| awards         = 
| isbn           = 
| isbn_note      = 
| oclc           = 
| dewey          = 
| congress       = 
| preceded_by    = [[Le Rêve (novel)|Le Rêve]]
| followed_by    = [[L'Argent]]
| native_wikisource = 
| wikisource     = 
| notes          =
| exclude_cover  = 
| website        =
}}
'''''La Bête humaine''''' ({{lang-en|'''''The Beast Within''''' or '''''The Beast in Man'''''}}) is an 1890 novel by [[Émile Zola]]. The story has been adapted for the cinema on several occasions.  The seventeenth book in Zola's ''[[Les Rougon-Macquart]]'' series, it is based upon the railway between [[Paris]] and [[Le Havre]] in the 19th century and is a tense, [[psychological thriller]].

==Characters==
The main characters are Roubaud, the deputy station master at Le Havre, his wife Séverine, and Jacques Lantier. Lantier is an engine driver on the line and the family link with the rest of ''Les Rougon-Macquart'' series. He is the son of Gervaise (''[[L'Assommoir]]''), the brother of Étienne Lantier (''[[Germinal (novel)|Germinal]]'') and Claude Lantier (''[[L'Œuvre]]''), and the half-brother of the eponymous [[Nana (novel)|Nana]].

==Plot==
Lantier, the "human beast" of the title, has a hereditary madness and has several times in his life wanted to murder women. At the beginning of the story he is an engine driver, in control of his engine "La Lison". His relationship with "La Lison" is almost sexual and provides some degree of control over his mania.

As a result of a chance remark, Roubaud suspects that Séverine has had an affair some years earlier, with Grandmorin  one of the directors of the railway company, who had acted as her patron and who had helped Roubaud get his job. He forces a confession out of her and  makes her write a letter to Grandmorin telling him to take a particular train that evening, the same train Roubaud and Séverine are taking back to Le Havre.

Meanwhile, Lantier who is not working while his engine is being repaired goes to visit his Aunt Phasie who lives in an isolated house by the railway. On leaving he meets his cousin Flore, with whom he has had a longstanding mutual attraction. After a brief conversation with her his passions become inflamed and he is on the verge of raping her but this in turn brings on his homicidal mania. He has a desire to stab her but just about controls himself and rushes away. Finding himself beside the railway track as the train from Paris passes, he sees, in a split second, a figure on the train holding a knife, bent over another person. Shortly after, he finds the body of Grandmorin beside the track with his throat cut. It was also discovered that he had been robbed of his watch and some money.

An investigation is launched and Roubaud and Séverine are prime suspects as they were on the train at the time and were due to inherit some property from Grandmorin. The authorities never suspect their true motive. Lantier sees Roubaud while waiting to be interviewed and identifies him as the murderer on the train, but when questioned says he cannot be sure. The investigating magistrate —  believing the killer was Cabuche, a carter who lived nearby — dismisses Roubaud and Séverine. The murder remains unsolved.

Despite being cleared of suspicion, the marriage of Roubaud and Séverine declines. Zola casually tosses in a remark that the money and watch stolen from Grandmorin was hidden behind the skirting  board in their apartment, thus confirming the reader’s suspicion that Roubaud was the murderer all along. Séverine and Lantier begin an affair, at first clandestinely but then more blatantly until they are caught ''in flagrante delicto'' by Roubaud. Despite his previous jealousy, Roubaud seems unmoved and spends less and less time at home and turns to gambling and drink.

Séverine admits to Lantier that Roubaud committed the murder and that together they disposed of the body. Lantier feels the return of his desire to kill and one morning leaves the apartment to kill the first woman he meets. After having picked a victim he is seen by someone he knows and so abandons the idea. He then realizes that he has the desire no longer. It is his relationship with Séverine and her association with the murder that has abated his desire.

The relationship between Roubaud and his wife deteriorates when she realizes that he has taken the last of the hidden money. Lantier has the opportunity to invest money in a friend’s business venture in New York. Séverine suggests they use the money from the sale of the property they inherited from Grandmorin. Roubaud is now the only obstacle to this new life and they decide to kill him. They approach him one night when he is working as a watchman at the station, hoping that the murder will be attributed to robbers. At the last moment however, Lantier loses his nerve.

Cousin Flore, meanwhile, sees Lantier pass her house every day on the train and noticing Séverine with him, realizes they are having an affair and becomes insanely jealous, wishing to kill them both. She hatches a plot to remove a rail from the line in order to cause a derailment of his train. One morning she seizes the opportunity when Cabuche leaves his wagon and horses unattended by the railway. She drags the horses onto the line shortly before the train arrives. In the resulting crash, numerous people are killed and Lantier is seriously injured. Séverine, however, remains unhurt. Wracked by guilt, Flore commits suicide by walking in front of a train.

Séverine nurses Lantier back to health but, in the absence of "la Lison", his mania returns and he murders her. The unfortunate Cabuche is the first to find her body and is accused of killing her at the behest of Roubaud. Both are put on trial for this and the murder of Grandmorin. They are both convicted and sentenced to life imprisonment.

Lantier begins driving again but his new engine is just a number to him. He begins an affair with his fireman's girlfriend.

The novel ends as Lantier is driving a train carrying troops towards the front at the outbreak of the [[Franco-Prussian War]]. The resentment between Lantier and his fireman breaks out as the train is travelling at full steam. Both fall to their deaths as the train full of happy, drunken, patriotic and doomed soldiers hurtles driverless through the night.

==Publication history==

* 1890, ''La Bête humaine'' (French), Paris: [[Bibliothèque-Charpentier]]
* 1968, ''The Beast in Man'' (translated by [[Robert Godfrey Goodyear]] and [[P.J.R. Wright]]), London: [[Signet Classic]]
* 1977, ''La Bête humaine'' (translated by [[Leonard Tancock]]), London: [[Penguin Books|Penguin]], ISBN 978-0-140-44327-1
* 1996, ''La Bête humaine'' (translated by [[Roger Pearson (linguist)|Roger Pearson]]), Oxford: [[Oxford University Press]], ISBN 978-0-199-53866-9
* 2007, ''The Beast Within'' (translated by [[Roger Whitehouse]]), London: [[Penguin Books|Penguin]], ISBN 978-0-140-44963-1

==Film adaptations==
* ''[[Die Bestie im Menschen]]'', a 1920 German silent film, directed by [[Ludwig Wolff (writer)|Ludwig Wolff]]
* ''[[La Bête humaine (film)|La Bête humaine]]'', a 1938 movie directed by [[Jean Renoir]]
* ''[[Human Desire]]'', 1954 movie based on the novel, and directed by [[Fritz Lang]], starring [[Glenn Ford]]
* ''[[La Bestia humana]]'', a 1957 Argentine movie, directed by [[Daniel Tinayre]]
* ''[[Cruel Train]]'', a 1995 British TV movie, directed by [[Malcolm McKay]]

==External links==
{{commons category}}
{{Gutenberg|no=5154|name=La Bête Humaine}} (French)

{{Les Rougon-Macquart}}

{{Authority control}}

{{DEFAULTSORT:Bete Humaine, La}}
[[Category:1890 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:French novels adapted into films]]
[[Category:Novels about rail transport]]