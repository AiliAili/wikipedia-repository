{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Brahma Lodge
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 3,192
| pop_year = {{CensusAU|2011}}
| pop_footnotes       = <ref name=Census2011>{{Census 2011 AUS | id = SSC40066 | name = Brahma Lodge (State Suburb)|quick = on | accessdate=16 February 2015}}</ref>
| pop2                = 2,983
| pop2_year = {{CensusAU|2006}}
| pop2_footnotes = <ref name="ABS">{{Census 2006 AUS|id =SSC41166|name=Brahma Lodge (State Suburb)|accessdate=18 March 2011| quick=on}}</ref>
| est                 = 
| postcode            = 5109
| area                = 
| dist1               = 
| dir1                = N
| location1           = Adelaide CBD
| lga                 = City of Salisbury
| stategov            = [[Electoral district of Ramsay|Ramsay]]
| fedgov              = [[Division of Wakefield|Wakefield]]
| near-n              = [[Salisbury, South Australia|Salisbury]]
| near-ne             = [[Salisbury Plain, South Australia|Salisbury Plain]]
| near-e              = [[Salisbury East, South Australia|Salisbury East]]
| near-se             = [[Salisbury East, South Australia|Salisbury East]]
| near-s              = [[Salisbury East, South Australia|Salisbury East]]
| near-sw             = [[Salisbury South, South Australia|Salisbury South]]
| near-w              = [[Salisbury South, South Australia|Salisbury South]]
| near-nw             = [[Salisbury, South Australia|Salisbury]]
}}

'''Brahma Lodge''' is a northern suburb of [[Adelaide]], [[South Australia]]. It is located in the [[City of Salisbury]].

==History==
The land on which the current suburb lies was owned by Frank Reiss who, in 1960, became the first to sell his land for subdivision. The suburb was named for the trotting horse stud of the same name that had been located on the land.<ref>{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/b/b24.htm#bramahL |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=18 March 2011}}</ref> Brahma Lodge Post Office opened on 14 May 1962.<ref name = "Post Office">{{Cite web  | last = Premier Postal History  | title = Post Office List  | publisher = Premier Postal Auctions | url = https://www.premierpostal.com/cgi-bin/wsProd.sh/Viewpocdwrapper.p?SortBy=SA&country= | accessdate = 26 May 2011}}</ref>

==Geography==
The suburb lies southeast of Salisbury town centre.

==Demographics==
The 2006 Census by the [[Australian Bureau of Statistics]] counted 2,983 persons in Brahma Lodge on census night. Of these, 49.9% were male and 50.1% were female.<ref name="ABS"/>

The majority of residents (68.6%) are of Australian birth, with other common census responses being [[England]] (9.4%), [[Italy]] (2.3%) and [[Vietnam]] (2.1%).<ref name="ABS"/>

The age distribution of Brahma Lodge residents is similar to that of the greater Australian population. 66.5% of residents were over 25 years in 2006, which was the same as the Australian average; and 33.5% were younger than 25 years.<ref name="ABS"/>

==Community==
The local newspaper is the [[News Review Messenger]]. Other regional and national newspapers such as [[The Advertiser (Adelaide)|The Advertiser]] and [[The Australian]] are also available.

===Schools===
Brahma Lodge Primary School is located near the centre of the suburb.

==Attractions==

===Parks===
The main greenspace in Brahma Lodge is '''Cockburn Green''', between Hammond Avenue and Mortess Street, and '''Brahma Lodge Oval''' on Francis Road.

==Transportation==

===Roads===
Brahma Lodge is serviced by [[Main North Road]] and Park Terrace, the latter linking the suburb to Salisbury town centre.

===Public transport===
Brahma Lodge is serviced by buses run by the [[Adelaide Metro]].

==See also==
*[[List of Adelaide suburbs]]

==References==
{{reflist}}

==External links==
*{{cite web |url=http://www.salisbury.sa.gov.au |title=City of Salisbury |author= |date= |work=Official website |publisher= |accessdate=18 March 2011}}
*{{cite web |url=http://www.lga.sa.gov.au/site/page.cfm?c=4148 |title=City of Salisbury |author= |date= |work= |publisher=Local Government Association of SA |accessdate=18 March 2011}}
*{{cite web |url=http://www.censusdata.abs.gov.au/ABSNavigation/prenav/ProductSelect?textversion=false&areacode=4&subaction=-1&action=201&period=2006&collection=census&navmapdisplayed=true&breadcrumb=L& |title=2006 Census Data by Location |author= |date= |work= |publisher=Australian Bureau of Statistics |accessdate=18 March 2011}}

{{Coord|-34.775|138.655|format=dms|type:city_region:AU-SA|display=title}}
{{City of Salisbury suburbs}}

[[Category:Suburbs of Adelaide]]