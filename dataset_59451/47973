{{more footnotes|date=June 2014}}
'''Raoul Heinrich Francé''' (born May 20, 1874 in Vienna, Austria; died October 3, 1943 in Budapest, Hungary) was an Austro-Hungarian [[botanist]], [[microbiologist]], natural and cultural philosopher. His botanical author abbreviation is "Francé".

== Life ==
[[File:Raoul Heinrich Francé Poppy and Pepperpot from Die Pflanze als erfinder 1920.jpeg|thumb|upright=1.7<!--size for low image-->|Poppy and Pepperpot ([[biomimetics]]) image from ''Die Pflanze als Erfinder'', 1920]]

Raoul Heinrich Francé (birth name: Rudolf Heinrich Franze<ref>{{citation/core|Surname1=René von Romain Roth|Title=Raoul H. Francé And The Doctrine Of Life |language=German}}</ref>) was born on 20 May 1874 in Altlerchenfeld ([[Vienna]]) and studied as a self-taught very early in analytical chemistry and "Mikrotechnik" ([[microscopy]]).
At 16 he was the youngest member of the Royal Hungarian scientific society, as its deputy magazine editor, he worked from 1893 to 1898.
From 1897 Francé studied medicine for eight semesters and became a student under the Hungarian protozoa scientist (Protozoenforscher) Geza Entz.
During this time he undertook fourteen botanical expeditions.
In 1898 he was appointed Deputy Head of the Institute of Plant Protection of Agricultural Academy in Hungarian-Altenburg.
Here he published his first natural philosophical work.
Thereupon Francé received in 1902 the invitation to come to Munich.
In 1906 he founded the "Deutsche Mikrologische Gesellschaft" (German micrological Company) and its institution over which he presided as director.
He was editor of this company and co-founder of the journal ''Mikrokosmos'' (1907).
He was editor of other periodicals, such as ''Jahrbuch für Mikroskopiker'' and ''Mikrologische Bibliothek''.

In 1906 Francé initiated the eight-volume monumental work "The Life of Plants", whose first four volumes (1906-1910) are from his own pen.
The publishing company advertised this book as "Pflanzen-Brehm" ("Plant-Brehm", after the famous book "[[Brehms Tierleben]]").

In 1922, he published a popular version of the scientific evidence on the soil biota in a booklet "Das Leben im Ackerboden" (Life in the soil).

Throughout his busy life he wrote 60 books and a variety of popular science articles and writings. 
In the "Walter Seifert Verlag" he was the editor of the journal "Telos - fortnightly magazine for work and success" (Telos – Halbmonatsschrift für Arbeit und Erfolg). 
As a renowned graphic artist Francé developed the technique of "feather stitch" (Federstich), which is rooted in the copper engraving (Kupferstich).

Stages of his life are Dinkelsbühl, Wroclaw, Salzburg, Munich and Dubrovnik-Ragusa.
In his life he wrote many books in which he anticipated many modern [[ecology|ecological]] ideas. 
Francé died of leukemia in 1943 in Budapest. 
He is buried together with his wife in Oberalm, Austria.

== Legacy ==

[[Organic farming]] is based in part on the findings by Francé, which were published in his books "Das Edaphon" [http://en.wiktionary.org/wiki/edaphon], 1913, and "Das Leben im Ackerboden", 1922, (Life in the soil) and a series of articles in the journal ''Mikrokosmos'' made available for a broad public. But this scientific source is usually concealed.

His wife ''Annie Francé-Harrar'' [[Annie Francé-Harrar]], a well-known biologist and author, worked many years with him and continued after his death in 1943 parts of his scientific work.

Today Raoul H. Francé is rediscovered as the founder of "Biotechnik" (Now called "[[Bionics]]"). His books "Das Edaphon" and "Das Leben im Ackerboden" were again published as reprints since 1959.<ref>http://stiftung-france.de/forum/viewtopic.php?f=44&t=299</ref>
Many of his then and now progressive ideas only gained its assessment at the end of the 20th century.

In [[Munich]] and [[Dinkelsbühl]] a street bears his name.

== Works ==
[[File:Die Pflanze als Erfinder (1920) (20938976585).jpg|thumb|[[Protozoa]] from ''Die Pflanze als Erfinder'', 1920]]

* '''''Germs of Mind in Plants'''''. 1905. [http://stiftung-france.de/forum/viewtopic.php?f=36&t=48&p=181&sid=8a9fcee096f9e9d316fb111930a29ecd#p181]
* ''Das Sinnesleben der Pflanzen''. 1905. [http://stiftung-france.de/forum/viewtopic.php?f=66&t=512&p=2071&sid=8a9fcee096f9e9d316fb111930a29ecd#p2071]
* ''Les sens de la plante''. 2013. [http://stiftung-france.de/forum/viewtopic.php?f=66&t=497&p=2031&sid=8a9fcee096f9e9d316fb111930a29ecd#p2031]. 
* ''Die Natur in den Alpen''. Leipzig 1910
* ''Die Alpen gemeinverständlich dargestellt''. Leipzig 1912
* ''Die silbernen Berge''. Stuttgart 1912
* ''Die Welt der Pflanze''. Berlin 1912
* ''Das Edaphon. Untersuchungen zur Oekologie der bodenbewohnenden Mikroorganismen''. München 1913. [http://stiftung-france.de/forum/viewtopic.php?f=44&t=299&sid=edbbbbe4b963bda3537cabeb023a0d65] [http://stiftung-france.de/forum/viewforum.php?f=44&sid=edbbbbe4b963bda3537cabeb023a0d65]
* ''Die Gewalten der Erde''. Berlin 1913
* ''Spaziergänge im Hausgarten''. Leipzig 1914
* ''Die technischen Leistungen der Pflanzen''. (Grundlagen einer objektiven Philosophie II). Leipzig 1919
* ''Einführung in die wissenschaftliche Photographie''. Gemeinsam mit Gambera. Stuttgart 1920
* ''Das Gesetz des Lebens''. Leipzig 1920
* ''Der Weg der Kultur''. Leipzig 1920
* ''Die Pflanze als Erfinder''. Stuttgart 1920
* ''München. Die Lebensgesetze einer Stadt''. (Grundlagen einer objektiven Philosophie III. Teil). München 1920
* ''Zoesis. Eine Einführung in die Gesetze der Welt''. München 1920
* ''Bios. Die Gesetze der Welt''. (Grundlagen einer objektiven Philosophie IV-V. Teil). Stuttgart und Heidelberg 1921
* ''Das Leben der Pflanze''. Stuttgart 1921
* ''Das Leben im Ackerboden. Stuttgart 1922. [http://stiftung-france.de/forum/viewtopic.php?f=43&t=307&sid=f6a0d839327f0ea05002308668b9e6ed]
* ''Die Kultur von morgen. Ein Buch der Erkenntnis und der Gesundung''. Dresden 1922
* ''Ewiger Wald. Ein Buch für Wanderer''. Leipzig 1922
* ''Süd-Bayern''. Berlin 1922
* ''Das wirkliche Naturbild''. Dresden 1923
* ''Der unbekannte Mensch''. Stuttgart und Heilbronn 1923
* ''Die Entdeckung der Heimat. Stuttgart 1923
* ''Die Welt als Erleben. Grundriß einer objektiven Philosophie''. (Grundlagen einer objektiven Philosophie VI). Dresden 1923
* ''Plasmatik. Die Wissenschaft der Zukunft''. Stuttgart und Heilbronn 1923
* ''Das Buch des Lebens. Ein Weltbild der Gegenwart''. Berlin 1924
* ''Die Seele der Pflanze''. Berlin 1924
* ''Grundriß einer vergleichenden Biologie''. (Grundlagen einer objektiven Philosophie I). Leipzig 1924
* ''Richtiges Leben. Ein Buch für jedermann''. Leipzig 1924
* ''Telos, die Gesetze des Schaffens''. Dresden 1924
* ''Das Land der Sehnsucht''. Berlin 1925
* ''Der Dauerwald''. 1925
* ''Der Ursprung des Menschen''. Stuttgart und Heilbronn 1926
* ''Bios - Die Gesetze der Welt. Taschenbuchausgabe'' Leipzig: Kröner 1926
* ''Der Weg zu mir. Der Lebenserinnerungen erster Teil''. Leipzig 1927
* ''Phoebus''. München 1927
* ''Vom deutschen Walde''. Berlin 1927
* ''Der Organismus''. München 1928
* ''Naturgesetze der Heimat''. Wien und Leipzig 1928
* ''Urwald''. Stuttgart 1928
* ''Welt, Erde und Menschheit''. Berlin 1928
* ''Die Waage des Lebens. Eine Bilanz der Kultur''. Leipzig 1929
* ''So musst du leben! Eine Anleitung zum richtigen Leben''. Dresden 1929
* ''Das Leben vor der Sintflut''. Berlin 1930
* ''Korallenwelt. Der siebente Erdteil''. Stuttgart 1930
* ''Lebender Braunkohlenwald. Eine Reise durch die heutige Urwelt''. Stuttgart 1932
* ''Naturbilder''. Wien 1932
* ''Braunkohle - Sonnenkraft''. Berlin 1934
* ''Von der Arbeit zum Erfolg. Ein Schlüssel zum besseren Leben''. Dresden 1934
* ''Sehnsucht nach dem Süden''. Gemeinsam mit Annie Francé-Harrar. Leipzig 1938
* ''Luft als Rohstoff''. 1939
* ''Lebenswunder der Tierwelt. Eine Tierkunde für Jedermann. Berlin 1940
* ''Leben und Wunder des deutschen Waldes''. Berlin 1943

== New editions ==
* ''Die Entdeckung der Heimat''. Reprint (1923). Mit einer Einführung von Gerhard Tenschert. Asendorf 1982
* ''Das Leben im Boden. Das Edaphon''. Reprint (''Das Edaphon'' 1913 and ''Das Leben im Ackerboden'' 1922). Einführung von René Roth, Ontario/Kanada, Edition Siebeneicher, Deukalion Verlag 1995

{{botanist|Francé}}

== Literature ==

*  René Romain Roth: '''''Raoul Heinrich Francé And The Doctrine Of Life'''''. 2000. Biography. [http://www.stiftung-france.de/forum/viewtopic.php?f=34&t=45&sid=af6e38aed677b94fcf38e1b38f2c62a2]
* Dr. Stephen Sokoloff: '''''Raoul Francé's Heritage - A Nightmare for Specialists'''''.  1993. [http://stiftung-france.de/forum/viewtopic.php?f=78&t=319&sid=112c2df1e401dcea52e285cae04289c1]
* Dr. Oliver Botar: '''''Raoul Heinrich France and Early 20th Century Central European Biocentrism'''''. 2010 [http://www.stiftung-france.de/forum/viewtopic.php?f=15&t=160]
* {{ÖBL|1|341||Francé Raoul H.|}}
* {{NDB|5|313|314|Francé, Raoul|Martin Müllerott|118692453}}
* Klaus Henkel: ''Die Renaissance des Raoul Heinrich Francé.'' Mikrokosmos, 86 (1): 3-16, 1997
* Werner Nachtigall: ''Der Bildungswert der Kleinwelt. Was bedeuten R. H. Francés „Gedanken über mikroskopische Studien“ für unsere Zeit?'' Mikrokosmos, 86 (6): 321-328, 1997

==See also==
* [[D'Arcy Thompson]] (''[[On Growth and Form]]'', 1917)

=== References ===
{{Reflist}}

== External links ==
* {{DNB-Portal|118692453}}
* {{IPNI|Francé}}
* [http://www.thomas-caspari.de/bodenkunde/france/index.htm In memoriam: Raoul Heinrich Francé (1874–1943)]
* [http://www.stiftung-france.de Stiftung Raoul Heinrich Francé and Annie Francé-Harrar]
* ''[[:de:Mikrokosmos (Zeitschrift)|Mikrokosmos]]''. Journal founded by Raoul Heinrich Francé. 

{{Authority control}}
{{DEFAULTSORT:France, Raoul Heinrich}}
[[Category:1874 births]]
[[Category:1943 deaths]]
[[Category:Austrian botanists]]