{{Use dmy dates|date=May 2011}}
{{Use Australian English|date=May 2011}}
{{Coord|-34.92577|138.58661|display=title}}
{{Infobox school
| name = Adelaide High School
| image=[[File:Adelaide High School COA.svg|125px]]
| motto = ''Non scholae sed vitae''<br>''(Not only for school, but for life)''
| city = [[Adelaide]]
| state = [[South Australia]]
| country = [[Australia]]
| type = [[Public school (government funded)|Public]]
| established = 1908
| principal = Anita Zocchi
| enrolment = 1216<ref>http://www.decs.sa.gov.au/locs/a8_publish/modules/locations/school_detail.asp?id=0768&type=Search{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
| colours = {{Color box|black|border=darkgray}} {{Color box|#999999|border=darkgray}} {{Color box|white|border=darkgray}}
| campus=Urban
| homepage = http://www.adelaidehs.sa.edu.au/
}}
[[File:Adelaide High School.JPG|thumb|Adelaide High School, viewed from near the corner of West Terrace and Glover Avenue]]

'''Adelaide High School''' is a coeducational state [[high school]] situated on the corner of [[West Terrace, Adelaide|West Terrace]] and Glover Avenue in the [[Adelaide]] Parklands. It was the first government highschool in [[South Australia]].<ref>Peddie, Clare. (29 July 2008). "History lesson", ''[[The Advertiser (Adelaide)|The Advertiser]]'', Adelaide, South Australia. p29.</ref> It currently has an enrollment of approximately 1,200 students.

==History==
In 1879, [[John Anderson Hartley]], [[Catherine Helen Spence]] and others created the [[Advanced School for Girls]] in Grote Street, Adelaide. It was the first public secondary school in South Australia. The school was combined with the Adelaide Continuation School, and on April 8, 1908 the name was changed to Adelaide High School. This was the same year the South Australian state high school system was launched.<ref>

{{cite book |last= Kwan|first= Elizabeth |title= Living in South Australia, a social history|edition= Volume 1, from before 1836 to 1914 |year= 1987|publisher= South Australian Government Printer|location= Adelaide|isbn= 0-7243-6493-5|pages= 145–175}}</ref> Adelaide High School was officially opened on the 24th of September, 1908 by the [[premier of South Australia]], [[Thomas Price (South Australia politician)|Thomas Price]]. It was the first secondary school in the Commonwealth of Australia.<ref>Government of South Australia (2008). [https://web.archive.org/web/20080725140129/http://www.premier.sa.gov.au/news.php?id=2963 ''Adelaide High turns 100'']. Archived from the [http://www.premier.sa.gov.au/news.php?id=2963 original] on 25 July 2005. Retrieved on 12 January 2013.</ref>

In 1927, it had an enrolment of 1,067 students, making it the largest school of its kind in the Commonwealth. By 1929, due to increasing enrollments, the school occupied two sites; one site was at Grote Street and the other was at Currie Street (now part of the Remand Centre). Due to the increasing enrolments, it was decided that a new building was required for Adelaide High School. The current site of the school on West Terrace was originally set aside for an army barracks in 1849, but in 1859 an observatory was built instead, which then became the Bureau of Meteorology in 1939.

Based on an award winning 1940 design, a new building was erected on the site from 1947 to 1951. This became Adelaide Boys High School while Adelaide Girls High School remained in the buildings in Grote Street. An application was made to have the building listed as a Historic Building on the Australian Register of the National Estate. The nomination was on the basis of the building's "Art Modern" style and significance in Adelaide education. It had not led to the building's listing on the register as of 2007.<ref>{{cite AHD|16566|Adelaide High School, West Tce, Adelaide, SA| accessdate=2007-11-23}}</ref> The original Grote Street school buildings were listed on the register as a ''Historic'' site in 1980. As of 2007, the buildings were used as a centre for the [[performing arts]].<ref>{{cite AHD|6430|Adelaide Girls High School (Advanced School for Girls) (former), 101 Grote St, Adelaide, SA, Australia | accessdate=2007-11-23}}</ref> The buildings were considered to be among the [[List of Nationally Significant 20th-Century Architecture in South Australia|Nationally Significant 20th-Century Buildings in South Australia]].<ref>[http://www.architecture.com.au/docs/default-source/act-notable-buildings/120-notable-buildings.pdf 120 notable buildings - Australian Institute of Architects] Accessed 8 May 2014.</ref>

In 1977, due to decreasing enrollments at both the Boys and the Girls schools, amalgamation began with Adelaide High School operating on two campuses - one on Grote Street and one on West Terrace. This arrangement ended in 1979, when all students were on the West Terrace site. In 1979, Adelaide High School became South Australia's Special Interest Language School, with students able to study up to seven languages: French, German, Latin, Modern Greek, Chinese, Spanish and Italian. Latin ceased to be offered in 2004 and was replaced by Japanese.

Adelaide is part of the longest-running sporting exchanges with [[Melbourne High School (Victoria)|Melbourne High School]] and [[Mac.Robertson Girls' High School]], both in [[Melbourne]].

==Curriculum==
{{refimprove section|date=September 2016}}
Adelaide High School is especially known for being a Special Interest Language School. It offers its students seven different languages to study: Modern Greek, Mandarin Chinese, Japanese, Italian, French, Spanish and German.<ref>{{cite web|url=http://www.adelaidehs.sa.edu.au/8-10CurriculumBook2010.pdf |title=Adelaide High School 2010, year 8 curriculum |accessdate=9 May 2010 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> The school is also a Centre for the Hearing Impaired and an Associate School for Students of High Intellectual Potential. It was a selected entry for students in its special interest [[cricket]] and [[Rowing (sport)|rowing]] programs.

===Languages===
Adelaide High School is a Special Interest Language School and currently a sister school to Asahi High School in [[Osaka]], Japan; IIS Quintino Sella in Biella, Italy, Heriburg Gymnasium in Coesfeld, Nord Rhein Westfalen, Germany, Lycée Saint Sauveur in Redon, Brittany, France, IES La Sisla in Sonsecas, Spain, [[Jinan Foreign Language School]] in Jinan, China, the 7th High School of Corfu, Greece and Greensward College in [[Essex]], UK. Sister school visits, both inbound and outbound, take place regularly.

===Facilities===
Facilities that students have access to include a resource centre, gymnasium, weights room, tennis, basketball and netball courts, performing arts centre, science labs and lecture theatres. The school also has a boatshed on the bank of the [[Torrens River|River Torrens]] which, holds the school's many rowing boats and where the school’s rowing crews train. It also has a shared rowing facility with Unley High School and Norwood Morialta High School, both at West Lakes.

==Sport==
The school has four "house" teams which students represent in sporting events within the school. The houses took their names from past principals of the school.The house names are: Adey (Red), Macghey (Blue), Morriss (Green), and West (Yellow). Sporting events include swimming, carnival, and sports day.

Adelaide High School has a range of girls and boys sporting teams and offers special interest sporting programmes including cricket and rowing. They participate in games and regattas throughout the year which lead up to the main events. The 5 Highs Cricket Carnival and Rowing event head to the River. Other sporting trips have the volleyball teams travelling to the Australian Volleyball Schools Cup in Melbourne, in December.

Since 1913, Adelaide High has taken part in a sporting exchange with [[Mac.Robertson Girls' High School]] and [[Melbourne High School (Victoria)|since 1910, Melbourne High School]] is the longest-running sporting exchange in the state. It is known as the Prefects Cup.<ref>Adelaide High School (2007). [http://www.adelaidehs.sa.edu.au/co-curricular/sport/girls-sport/macrob-exchange-program Melbourne / Mac.Rob Exchange Program]. Retrieved on 25 September 2015.</ref> It is held in early Term 3 and each year they swap, as the boys go to Melbourne and the girls go to Adelaide. There are competitions in many sports such as: [[Australian Rules football|Australian rules foctball]], [[soccer]], [[tennis]], [[rowing (sport)|rowing]], [[basketball]], [[netball]], [[softball]], [[chess]], [[debating]], [[theatre sports]], [[volleyball]], cross country athletics, badminton, table tennis and hockey. Sports previously played against Melbourne included [[lacrosse]], [[baseball]] and [[field hockey]]. Melbourne High School currently holds the Prefect Cup as its most recent win was in 2014.

==Head Master / Principal==

{| class="wikitable" style="background: #fffaef; border: 1px dotted gray;"
|-
!style="background:#ffdead;" |Name
!style="background:#ffdead;" |Years
!style="background:#ffdead;" |Ref(s)
|-
|[[William Adey]] || 1908–1920 || <ref>Condon, Brian (2006). [http://adbonline.anu.edu.au/biogs/A070016b.htm Adey, William James (1874 - 1956)]. Australian Dictionary of Biography. Online Edition. Retrieved on 16 November 2008.</ref>
|-
|Reginald A. West || 1920–1948 || <ref>Pash, J.H. (2006). [http://adbonline.anu.edu.au/biogs/A160622b.htm West, Reginald Arthur (1883 - 1964)]. Australian Dictionary of Biography. Online Edition. Retrieved on 16 November 2008.</ref>
|-
|C.M. Ward || 1948 || –
|-
|A. E. Dinning || 1949–1954 || –
|-
|Wybert M. C. Symonds || 1954–1962 || –
|-
|A. H. Campbell || 1963–1968 || –
|-
|W. J. Bentley || 1969–1977 || –
|-
|Colin H. Brideson [[Order of Australia|OAM]] || 1978–1987 || <ref>Governor General of the Commonwealth of Australia (2008). [http://www.gg.gov.au/res/File/PDFs/honours/ad07/medianotesOAM(A-E).pdf Medal of the Order of Australia: Mr Colin Herbert Brideson] {{webarchive |url=https://web.archive.org/web/20080720205356/http://www.gg.gov.au/res/File/PDFs/honours/ad07/medianotesOAM(A-E).pdf |date=20 July 2008 }}. Page 21. Retrieved on 16 November 2008.</ref>
|-
|Peter Sanderson || 1988–1997 || –
|-
|Stephen Dowdy|| 1998–2011 || –
|-
|Anita Zocchi|| 2011–current || <ref>{{cite web|url=http://www.adelaidenow.com.au/news/south-australia/international-womens-day--leading-south-australian-women-to-watch/news-story/a6570027ba564e49e193210cf03af4f5|title=International Women’s Day - leading South Australian women to watch|access-date=1 October 2016|website=[[Adelaide Now]]|publisher=[[The Advertiser (Adelaide)|The Advertiser]]|date=6 March 2015}}</ref>
|- 
|}

==Notable staff and students==
{{refimprove section|date=September 2016}}
*[[Don Anderson|Sir Don Anderson]] (1917–1975) - Director-General of the Department of Civil Aviation<ref>{{citation|url=http://adb.anu.edu.au/biography/anderson-sir-donald-george-don-9352|archiveurl=https://web.archive.org/web/20150202093213/http://adb.anu.edu.au/biography/anderson-sir-donald-george-don-9352|archivedate=2 February 2015 |title=Anderson, Sir Donald George (Don) (1917–1975)|first=John|last=Gunn|publisher=Australian National University|work=Australian Dictionary of Biography}}</ref>
*[[Lynn Arnold]] (b. 1949) - South Australian Premier{{citation needed|date=March 2016}}
*[[Nick Bolkus]] (b. 1950) - South Australian Senator and Cabinet Minister<ref>{{cite interview |last=Bolkus |first=Nick |subject-link=Nick Bolkus |interviewer=George Lewkowicz |title=Don Dunstan Foundation: Don Dunstan Oral History Project: Nick Bolkus |url=https://dspace.flinders.edu.au/xmlui/bitstream/handle/2328/3217/BOLKUS_Nick_Cleared.pdf?sequence=2|archiveurl=https://web.archive.org/web/20150923015221/http://dspace.flinders.edu.au/xmlui/bitstream/handle/2328/3217/BOLKUS_Nick_Cleared.pdf?sequence=2|archivedate=23 September 2015 |date=14 November 2007 }}</ref>
*[[Shaun Burgoyne]] (b. 1982) - AFL footballer{{citation needed|date=December 2010}}
*[[Ralph Clarke (Australian politician)|Ralph Clarke]] (b. 1951) - South Australian Deputy Opposition Leader{{citation needed|date=March 2016}}
*[[Hugh Cairns (surgeon)|Hugh Cairns]] (1896–1952) - First Nuffield Professor of Surgery, Oxford University.<ref>{{cite web|url=http://www.adelaide.edu.au/graduatecentre/scholarships/postgrad/pdf/sarhodesscholars.pdf|title=The Rhodes Scholarship, South Australia|publisher=The University of Adelaide|accessdate=4 December 2010}}</ref>
*[[John Stuart Dowie]] (1915–2008) - Artist<ref>{{cite book|page=82|title=John Dowie: A Life in the Round|editor-first1=Tracey|editor-last1=Lock-Weir|first1=John|last1=Dowie|publisher=Wakefield Press|date=2001|isbn=1862545448}}</ref>
*[[Sia Furler]] (b. 1975) - Pop singer and songwriter<ref>{{cite journal|url=http://www.mjmharry.com/docs/Sia-Furler.pdf|archiveurl=https://web.archive.org/web/20101105010101/http://www.mjmharry.com/docs/Sia-Furler.pdf|title=Sia Sensation|first=Michael|last=Harry|journal=[[The Advertiser (Adelaide)|The Adelaide Advertiser]]|pages=24–26|format=PDF|archivedate=5 November 2010}}</ref>
*[[Anne Haddy]] (1930–1999) - Actress (best known for her role as [[Helen Daniels]] in [[Neighbours]])<ref>{{cite news|url=http://www.independent.co.uk/arts-entertainment/obituary-anne-haddy-1098826.html|last=Hayward|first=Anthony|title=Obituary: Anne Haddy|work=The Independent|date=8 June 1999}}</ref>
*Barbara Hall (c. 1931 – ) - Physicist<ref>{{cite news |url=http://nla.gov.au/nla.news-article47512835 |title=S.A. Woman Brilliant Scientist |newspaper=[[The Advertiser (Adelaide)]] |volume=95, |issue=29,386 |location=South Australia |date=17 December 1952 |accessdate=21 May 2016 |page=1 |via=National Library of Australia}}</ref><ref name=oldscholars/>
*[[Bob Hank]] (1923–2012) - Dual [[Magarey Medal]]list<ref>{{citation|url=http://adelaidehigholdscholars.org/wp-content/uploads/2013/05/Old-Scholars-Newsletter-2012-June.pdf|archiveurl=https://web.archive.org/web/20160323065451/http://adelaidehigholdscholars.org/wp-content/uploads/2013/05/Old-Scholars-Newsletter-2012-June.pdf|archivedate=23 March 2016|date=June 2012|first=Bob|last=Henschke|title=Letter to the Editor: Adelaide High School Old Scholars Association}}</ref>
*Margaret Hubbard (1924 – ) - First woman to win the Hentford Scholarship for Latin at Oxford<ref name=oldscholars/><ref>{{cite news |url=http://nla.gov.au/nla.news-article35660170 |title=Girl Wins Tennyson Medal |newspaper=[[The Advertiser (Adelaide)]] |location=South Australia |date=13 January 1940 |accessdate=21 May 2016 |page=22 |via=National Library of Australia}}</ref> 
*[[Tom Koutsantonis]] (b. 1971) - South Australian Treasurer<ref>{{citation|url=http://www.premier.sa.gov.au/index.php/ministers/tom-koutsantonis-mp|
archiveurl=https://web.archive.org/web/20160305210409/http://www.premier.sa.gov.au/index.php/ministers/tom-koutsantonis-mp|archivedate=5 March 2016|title=Tom Koutsantonis MP|publisher=SA Government}}</ref>
*[[Simon Lewicki]] aka Groove Terminator - Electronic music artist{{citation needed|date=December 2010}}
*[[Brian Ross Martin]] (b. 1947) - [[Chief Justice of the Northern Territory]]<ref>{{citation|quote=Your Honour was educated at Adelaide High School|url=http://www.supremecourt.nt.gov.au/doc/news/2004/ceremonial_br_martin_02022004.pdf|title=WELCOME CEREMONIAL SITTING FOR THE HONOURABLE THE CHIEF JUSTICE B.R. MARTIN: Transcript of proceedings|date=2 February 2004|archiveurl=https://web.archive.org/web/20120722013121/http://www.supremecourt.nt.gov.au/doc/news/2004/ceremonial_br_martin_02022004.pdf|archivedate=22 July 2012|publisher=Supreme Court of the Northern Territory}}</ref>
*Susie May McGillicuddy MBE, née Halbert (1887 – ) - matron of Church of England Boys' Home, Walkerville<ref name=oldscholars>{{cite news |url=http://nla.gov.au/nla.news-article47613544 |title="Old Girls" Will Honor Their Headmaster |newspaper=[[The Advertiser (Adelaide)]] |volume=97, |issue=29,900 |location=South Australia |date=13 August 1954 |accessdate=21 May 2016 |page=15 |via=National Library of Australia}}</ref>
*[[Mark Oliphant]] (1901–2000) - South Australian Governor <ref>{{cite book| last = Cockburn| first = Stewart| first2 = David| last2 = Ellyard| title = Oliphant, the Life and Times of Sir Mark Oliphant| location = Adelaide| year = 1981| publisher = Axiom Books| isbn = 978-0-9594164-0-4| ref = harv|page=19}}</ref>
*[[Neil Page]] (b. 1944) - Australian baseball representative/player{{citation needed|date=December 2010}}
*Dr. (Hannah) June Pash (1923 – ) - Medical Superintendent of Queen Victoria Memorial Hospital, Melbourne<ref name=oldscholars/>
*[[Greig Pickhaver]] - aka H.G. Nelson, actor, comedian and writer{{citation needed|date=December 2010}}
*[[Chris Sumner]] (b. 1943) - South Australian Attorney-General{{citation needed|date=March 2016}}
*[[David Vigor]] (1939–1998) - South Australian Senator{{citation needed|date=March 2016}}
*[[Lou Vincent]] (b. 1978) - New Zealand Test cricketer{{citation needed|date=December 2010}}

==Further reading==

* Adelaide High School Council (1983). ''Adelaide High School: 75th anniversary, 1908-1983 souvenir book''. ISBN 0-9593880-2-8. {{OCLC|220259206}}

==References==
{{Reflist}}

==External links==
*[http://www.adelaidehs.sa.edu.au/Gallery Adelaide High School Virtual Tour]

{{ISSA Schools}}

[[Category:Educational institutions established in 1908]]
[[Category:High schools in South Australia]]
[[Category:Public schools in South Australia]]
[[Category:Special interest high schools in South Australia]]
[[Category:Schools in Adelaide]]
[[Category:1908 establishments in Australia]]