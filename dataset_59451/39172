{{For|the original Sign Route 195|California State Route 195 (1934)}}
{{Good article}}
{{Infobox road
|state=CA
|type=SR
|route=195
|section=495
|maint=[[Caltrans]]
|map_notes=The 1972 routing of SR 195 highlighted in red
|length_mi=7.420
|length_round=3
|length_ref=<ref name=trucklist/>
|direction_a=West
|terminus_a=Harrison Street near [[Oasis, Riverside County, California|Oasis]]
|direction_b=East
|terminus_b={{jct|state=CA|SR|111}} in [[Mecca, California|Mecca]]
|previous_type=SR
|previous_route=193
|next_type=SR
|next_route=197
|formed=1963<ref name="law1964" />
|deleted=2014<ref name="CTC" />
|counties=[[Riverside County, California|Riverside]]
}}
'''State Route 195''' ('''SR 195''') was a [[state highway]] in the [[U.S. state]] of [[California]], branching westward from [[State Route 111 (California)|SR 111]] to [[SR 86 (CA)|SR 86]] near the town of [[Mecca, California|Mecca]] and the [[Salton Sea]]. The route formerly extended east to [[Interstate 10 (California)|Interstate 10]] (I-10) near [[Joshua Tree National Park]] as a longer route extending to [[Blythe, California|Blythe]] and points further east. After the main route was shifted north, the older route remained as an alternate known as Box Canyon Road. The route was designated in the [[1964 state highway renumbering (California)|1964 state highway renumbering]], although the Box Canyon Road portion was removed as a state highway in 1972. Following the construction of the SR 86 expressway, SR 195 was curtailed in 2009, and removed entirely in 2014.

==Route description==
Before the route was mostly removed in 2009, it began at Harrison Street, the old routing of [[California State Route 86|SR 86]], in [[Riverside County, California|Riverside County]]. It then headed north as Pierce Street until intersecting 66th Street, where SR 195 turned east. The highway intersected [[California State Route 86|SR 86]] and continued to the town of [[Mecca, California|Mecca]], where it met its north end at [[California State Route 111|SR 111]]. The route loosely paralleled the northern end of the [[Salton Sea]], passing through farmland for its entire length.<ref>{{cite map|title=Riverside County Street Atlas|year=2009|publisher=Thomas Brothers}}</ref><ref name="googlemaps">{{Google maps | url = https://maps.google.com/?ie=UTF8&ll=33.528231,-116.048584&spn=0.122493,0.308647&z=12 | title = California State Route 195 | accessdate = January 2, 2011}}</ref>

In 2013, SR&nbsp;195 had an [[annual average daily traffic]] (AADT) of 4,500&nbsp;vehicles at Buchanan Street, and 6,000&nbsp;vehicles at the eastern terminus with SR 111, the latter of which was the highest AADT for the highway.<ref name=traffic>{{Caltrans traffic|year=2013|start=180|end=197}}</ref>

==History==
Route 64, a highway from Mecca to [[Blythe, California|Blythe]], was added to the state highway system in 1919.<ref>{{cite CAstat|year=1919|ch=46|res=true}}</ref> In 1935, Pierce Street from Route 26 near [[Oasis, Riverside County, California|Oasis]] to Avenue 66, and Avenue 66 from Route 26 to Mecca were added to the state highway system.<ref>{{cite CAstat|year=1935|ch=429}}</ref> Two years later, Pierce Street was designated as Route 203, and Avenue 66 was designated as Route 204.<ref>{{cite CAstat|year=1937|ch=841}}</ref> By 1926, the road existed east of Mecca to Blythe, but was unpaved;<ref>{{Cite Caltrans map|year=1926|access-date=June 20, 2015}}</ref> by 1930, the road connected from Mecca to the road along the western side of the Salton Sea to [[Indio, California|Indio]] and points further west.<ref>{{Cite Caltrans map|year=1930|access-date=June 20, 2015}}</ref>

Between 1932 and 1934, the road east of Mecca had been paved.<ref>{{Cite Caltrans map|year=1932|access-date=June 20, 2015}}</ref><ref name="1934 map"/> The western part of the road, known as the Box Canyon road, from Mecca to Blythe served as part of [[US 60 (CA)|US 60]] and [[US 70 (CA)|US 70]] until it was eventually bypassed in favor of a more direct route to Indio, diverging at Shavers' Summit. Between 1934 and 1936, US 60 and US 70 had made the shift north towards Indio, and the portion between [[US 99 (CA)|US 99]] and Mecca was paved. Initial opposition was later overcome after the road was washed out during a storm and forced motorists to take refuge in the nearby foothills.<ref name="1934 map">{{Cite Caltrans map|year=1934|access-date=June 20, 2015}}</ref><ref>{{Cite Caltrans map|year=1936|access-date=June 20, 2015}}</ref><ref>{{cite news | url=https://www.newspapers.com/clip/2655845/the_san_bernardino_county_sun/ | title=Cloudburst Brought Major Road Change | work=The San Bernardino County Sun | date=December 23, 1962 | accessdate=June 20, 2015 | author=Belden, L. Burr | page=44 |via=Newspapers.com}} {{open access}}</ref> By 1940, the SR 195 designation was signed.<ref>{{Cite Caltrans map|year=1940|access-date=June 20, 2015}}</ref> In the 1940s, the highway continued due west of Mecca to end at an intersection with US 99, rather than turning south.<ref>{{cite map|url=http://www.lib.utexas.edu/maps/topo/250k/txu-pclmaps-topo-us-santa_ana-1947.jpg|title=Santa Ana|publisher=United States Geological Survey|scale=1:250,000|year=1947}}</ref>

In 1953, efforts to remove the road from Mecca to the highway from Blythe to Indio, from the state highway system were met with community opposition, since it served as an alternate route for the other highway.<ref>{{cite news | title=Box Canyon Road Bill Protested | work=Los Angeles Times | date=February 4, 1953 | page=A2}}</ref> State Senator Nelson Dilworth proposed legislation to require the road from [[Banning, California|Banning]] through [[Idyllwild, California|Idyllwild]] to [[Mountain Center, California|Mountain Center]] to be added to the state highway system if SR 195 was removed, as the two were of roughly the same length, but the latter remained in the system.<ref name="jacinto">{{cite news | url=https://www.newspapers.com/clip/2655894/the_san_bernardino_county_sun/ | title=Road in San Jacinto Mts. May Become State Highway | work=San Bernardino County Sun | date=July 13, 1969 | accessdate=June 20, 2015 | page=18 | via=Newspapers.com}} {{open access}}</ref>

SR 195 was officially designated in the [[1964 state highway renumbering (California)|1964 state highway renumbering]].<ref name="law1964">{{cite CAstat|year=1963|ch=385}}</ref> The original alignment continued past SR 111 and the Salton Sea before ending at US 60, which later became [[I-10 (CA)|I-10]], at the southern end of [[Joshua Tree National Park]].<ref>{{Cite Caltrans map|year=1965}}</ref><ref>{{Cite Caltrans map|year=1969}}</ref> The Division of Highways proposed deleting this part of the state highway in 1971,<ref>{{cite news | url=https://www.newspapers.com/clip/2655946/the_san_bernardino_county_sun/ | title=State Officials Unveil Their Proposal For Classifying Roads By Function | work=San Bernardino County Sun | date=September 17, 1971 | accessdate=June 20, 2015 | author=Long, Ken | page=16 | via=Newspapers.com}} {{open access}}</ref> though similar plans had been revived in 1969.<ref name="jacinto" /> This portion was removed in 1972.<ref name="cite1972" />

In 1988, the [[California Transportation Commission]] (CTC) approved shifting SR 111 onto the new SR 86 expressway after it was completed, using SR 195 to make the connection.<ref name="CTC" /> When the SR 86 expressway was fully constructed, SR 195 was to be removed from the state highway system according to state law;<ref name="cite1972">{{cite CAstat|year=1972|ch=1216}}</ref> that expressway was finished in 2001. However, this removed the connection from the highway portion of SR 111 to the new expressway carrying both SR 111 and SR 86 north from the state highway system. Caltrans officially deleted most of SR 195 in 2009, leaving a gap in SR 111 following deletions of the old routing that was now bypassed by the expressway. In December 2014, with Riverside County and Caltrans both supporting, the CTC transferred the remaining portion of SR 195, from the new SR 86 expressway to SR 111, to become part of SR 111.<ref name="CTC">{{cite web|url=http://www.catc.ca.gov/meetings/agenda/2014Agenda/2014_12/46_2.3a.pdf|title=Route Adoption – State Highway, 08-RIV-111 PM R18.5/R19.4 Resolution HRA-14-02|date=December 10, 2014|author=Craggs, Timothy|accessdate=June 20, 2015}}<br />{{cite web|url=http://www.catc.ca.gov/meetings/agenda/2015Agenda/2015_01/02_1.2.pdf|date=December 10, 2014|title=Minutes|publisher=California Transportation Commission|accessdate=June 20, 2015}}</ref> SR 195 still appears in Caltrans documents dated from 2014.<ref name=trucklist /><ref name=bridgelog />

==Major intersections==
{{CAinttop|county=Riverside|post-1964=yes|based_on=the 1972 routing|former=yes|post_ref=<br><ref name=trucklist /><ref name=traffic /><ref name=bridgelog>{{Caltrans bridgelog}}</ref>
}}
{{CAint
|location=none
|postmile=R0.00
|road=Harrison Street
|notes=South end of SR 195; former [[U.S. Route 99|US 99]]; former [[California State Route 86|SR 86]]
}}
{{CAint
|location=none
|postmile=none
|road={{jct|state=CA|SR|86}}
|notes=
}}
{{CAint
|location=Mecca
|postmile=7.42
|road={{jct|state=CA|SR|111}}
|notes=North end of SR 195
}}
{{Jctbtm}}

==See also==
*{{portal-inline|California Roads}}

==References==
{{Reflist|30em}}

==External links==
{{Attached KML|display=title,inline}}
{{commonscat}}
*[http://www.aaroads.com/california/ca-195.html California @ AARoads.com - State Route 195]
*[http://www.dot.ca.gov/hq/roadinfo/sr195 Caltrans: Route 195 highway conditions]
*[http://www.cahighways.org/193-200.html#195 California Highways: SR 195]

[[Category:Former state highways in California|195]]
[[Category:Roads in Riverside County, California|State Route 195]]