'''Erik Patterson''' is an American screenwriter, television writer, and playwright.

==Life and career==

Erik is a graduate of [[Occidental College]] and the [[British American Drama Academy]].

His plays have been produced by Playwrights' Arena, the [[Los Angeles Theatre]] Center,<ref>Jenelle RIley, [http://thelatc.org/2010/news/video-erik-patterson-gets-interviewed-about-sick/ "Back Stage Interviews Erik Patterson"], Back Stage, 2010-04-18</ref> [[Theatre of NOTE]], Evidence Room, and The Actors' Gang, and developed through the [[Lark Play Development Center]], Moving Arts, Black Dahlia, [[Naked Angels (theater company)|Naked Angels]], and the [[Mark Taper Forum]]. He has had monologues published by [[Heinemann (publisher)|Heinemann]] and Smith & Kraus.

Along with writing partner Jessica Scott, he has written screenplays for [[Warner Premiere]], [[ABC Family]], the [[Disney Channel]], [[The Hatchery (company)|The Hatchery]], and [[Universal Channel|Universal]].

Erik currently lives in [[Los Angeles]], [[California]].

==Awards==

He won a 2012 Humanitas Prize for "Radio Rebel". 

He won a 2010 [[Writers Guild of America Award]] for Best Children's Script (Long Form) for ''Another Cinderella Story''.<ref>Jeff Leins, [http://www.newsinfilm.com/2010/02/21/hurt-locker-up-in-the-air-win-at-wgas/ "Hurt Locker, Up in the Air Win at WGAs"], News In Film, 2010-02-21.</ref>

He was nominated in the [[40th Daytime Emmy Awards]] for Outstanding Writing in a Children's Series for "R.L. Stine's The Haunting Hour".<ref>Nellie Andreeva, [http://www.deadline.com/2013/05/cbs-the-young-and-the-restless-top-daytime-emmy-nominations/ "Daytime Emmy Nominations"], Deadline Hollywood, 2013-05-01.</ref> He was nominated in the same category for the 41st Daytime Emmy Awards. 

His play ''He Asked For It''<ref>Alek & Steph, [http://www.ohlalamag.com/en/2009/07/he-asked-for-it-interview-with-writer-erik-patterson.html "OhLaLa Mag Interviews Erik Patterson"], Oh La La Mag, 2009-07-13</ref> was nominated for a 2009 [[GLAAD Media Award]] for Outstanding Los Angeles Theater.<ref>GLAAD, [http://www.glaad.org/page.aspx?pid=522 "20th Annual GLAAD Media Awards"], GLAAD.org</ref> It was nominated for a 2008 [[Ovation Award]] for World Premiere Playwriting, as well as being named one of the Top 10 Plays of 2008 by Frontiers Magazine.<ref>Les Spindle, [http://www.backstage.com/bso/content_display/news-and-features/e3i68d0e189b7e43eaf6cd1914fabc75014 "Ask and You Shall Receive"], Backstage West, 2009-06-10</ref> In 2003, his play ''Yellow Flesh Alabaster Rose'' won a Backstage West Garland Award for Best Playwriting and was a finalist for the [[PEN Center USA|PEN USA]] Literary Award. In 2004, his play ''Red Light Green Light'' was nominated for an Ovation Award for World Premiere Playwriting.

His play "One of the Nice Ones" was nominated for a 2016 [[Ovation Award]] for World Premiere Playwriting. 

==Work==

===Plays===

*''One of the Nice Ones'' (2016)
*"The Sex Lives of Strangers" (unproduced)<ref>Kathleen Dennehy, [http://www.connotationpress.com/drama/1746-erik-patterson-drama "Erik Patterson interview with Kathleen Dennehy"], Connotation Press, 2013-05-01.</ref>
*''Sick'' (2010)<ref>Joshua Fardon, [http://connotationpress.com/drama/271-erik-patterson-sick "Erik Patterson interview with Connotation Press"], Connotation Press, 2011-08-08.</ref>
*''He Asked For It'' (2008)<ref>Amita Parashar, [http://www.advocate.com/Arts_and_Entertainment/Theater/_He_Asked_for_It_/ "He Asked For It"], The Advocate.</ref>
*''Red Light Green Light'' (2004)<ref>Slim, [http://kotorimagazine.com/none-this-is-a-feature/1127.html "Mother and Child Union"], Kotori Magazine, 2004-05-13.</ref>
*''Yellow Flesh Alabaster Rose'' (2003)<ref>Slim, [http://kotorimagazine.com/permalink/1648.html "Yellow Flesh Alabaster Rose"], Kotori Magazine, 2003-02-15.</ref>
*''Tonseisha'' (2001)

===Film & TV===

*''Love on a Limb'' (2016)
*''The Christmas Note'' (2015)
*''Love Under the Stars'' (2015)
*''Sophia Grace & Rosie's Royal Adventure'' (2014)
*''[[Radio Rebel]]'' (2012)
*''A Cinderella Story: Once Upon A Song'' (2011)
*''[[Another Cinderella Story]]'' (2008)

He has written several episodes of the television series ''[[R.L. Stine's The Haunting Hour]]'' including:

* "Best Friend Forever" (1.12)
* "Wrong Number" (1.16)
* "The Perfect Brother" (1.20)
* "Pumpkinhead" (2.5)
* "Brush With Madness" (2.6)
* "The Hole" (2.10)
* "Stage Fright" (2.15)
* "Grampires: Part 1" (3.1)
* "Grampires: Part 2" (3.2)
* "Poof de Fromage" (3.15)
* "Funhouse" 
* "Bad Egg" 
* "Near Mint Condition"
* "Return of the Pumpkinheads"

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* {{IMDb name|0666203}}

{{DEFAULTSORT:Patterson, Erik}}
<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Occidental College alumni]]
[[Category:American male screenwriters]]
[[Category:American television writers]]
[[Category:American dramatists and playwrights]]
[[Category:Male television writers]]
[[Category:American male dramatists and playwrights]]