{{Infobox journal
| title = Studies in Intelligence
| cover = [[File:Studies in Intelligence.jpg|180px]]
| editor = [[Andres Vaart]]
| discipline = [[Intelligence (information gathering)|Intelligence gathering]]
| formernames =
| abbreviation =
| publisher = [[Center for the Study of Intelligence]]
| country = United States
| frequency = Quarterly
| history = 1955&ndash;present
| openaccess =
| license =
| impact =
| impact-year =
| website = https://www.cia.gov/library/center-for-the-study-of-intelligence/csi-publications/csi-studies/index.html
| link1 =
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 30965384
| LCCN =
| CODEN =
| ISSN = 1527-0874
| eISSN =
}}
'''''Studies in Intelligence''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] on [[Intelligence (information gathering)|intelligence]] that is [[Organizational structure of the Central Intelligence Agency#General Publications|published]] by the [[Center for the Study of Intelligence]], a group within the United States [[Central Intelligence Agency]]. It contains both [[Classified information|classified]] and unclassified articles on the methodology and history of the field of intelligence gathering.<ref name="namebase">{{cite web |url=http://www.namebase.org/sources/XG.html |title=CIA. Studies in Intelligence: Index 1955-1992 |author= |date= |work=[[NameBase]] |publisher= |archiveurl=http://www.webcitation.org/64k8EbEsx |archivedate=2012-01-16 |accessdate=2010-08-07}}</ref><ref>{{cite web| title=Studies in Intelligence Archives | url=http://onlinebooks.library.upenn.edu/webbin/serial?id=sinint | accessdate=2010-08-07}}</ref><ref>{{cite web| title=About CSI | publisher=Center for the Study of Intelligence | url=https://www.cia.gov/library/center-for-the-study-of-intelligence/about-csi.html | accessdate=2010-08-07}}</ref>

The journal was established by [[Sherman Kent]] in 1955. According to Kent, intelligence "has developed a recognized methodology; it has developed a vocabulary; it has developed a body of theory and doctrine; it has elaborate and refined techniques. It now has a large professional following. What it lacks is a literature.... The most important service that such a literature performs is the permanent recording of our new ideas and experiences." ''Studies in Intelligence'' was seen by Kent as a "rudimentary step towards making ... findings cumulative".<ref name="namebase" />

Copies of unclassified and declassified articles from ''Studies in Intelligence'' are held at the [[National Archives and Records Administration|National Archives]]', [[College Park, Maryland]] location as part of the Records of the Central Intelligence Agency (Record Group 263).<ref>{{cite web| title=Index of Declassified Articles | publisher=Center for the Study of Intelligence | url=https://www.cia.gov/library/center-for-the-study-of-intelligence/csi-publications/index-of-declassified-articles/index.html | accessdate=2010-08-07}}</ref> Some extracts from 1992 and 1994&ndash;2007 are also available on-line.

== References ==
{{reflist}}

== External links ==
* {{Official website|https://www.cia.gov/library/center-for-the-study-of-intelligence/csi-publications/csi-studies/index.html}}


[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1955]]
[[Category:Central Intelligence Agency publications]]
[[Category:Non-fiction works about espionage]]