<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name=VCP-1
|image=Verville Vcp-1.jpg
|caption=
}}{{Infobox Aircraft Type
|type=[[United States Army Air Service|USAAS]] Pursuit prototype
|manufacturer=[[Engineering Division]]
|designer=[[Alfred V. Verville]]
|first flight=11 June [[1920 in aviation|1920]]
|introduced=
|retired=
|status=
|primary user=
|more users=
|produced=
|number built=2
||variants with their own articles =[[Verville-Packard R-1 Racer]]
}}
|}

The '''Verville VCP''' was an American single-engined [[biplane]] [[fighter aircraft]] of the 1920s. A single example of the VCP-1 was built by the [[United States Army Air Service]]'s [[Engineering Division]], which was later rebuilt into a successful racing aircraft, while a second, modified fighter was built as the '''PW-1'''.

==Design and development==

===VCP-1===
In 1918, Virginius E. Clark, in charge of the Plane Design section of the U.S. Army Air Service's Engineering Division and [[Alfred V. Verville]], who had recently joined the Engineering Division from private industry, started design of a single-seat fighter (known as "pursuit" aircraft to the U.S. Army), the VCP-1 (Verville-Clark Pursuit).<ref name="Amfight p197">Angelucci and Bowers 1987, p. 197.</ref>

Drawing from the experience of the French and their [[SPAD S.XIII]], the desire was to make a sleeker and more maneuverable fighter.<ref name="Wingsp8">Boyne 2001, p. 8.</ref> The VCP-1 was powered by a Wright-built {{convert|300|hp|kW|lk=on|abbr=on}} [[Hispano-Suiza 8]] [[V-8 engine]] and had tapered single-[[interplane struts|bay]] biplane wings. The fuselage was a [[monocoque]] structure constructed of [[plywood]], while the wings were of wood and fabric construction. The engine was cooled with an unusual [[Annulus (mathematics)|annular]] radiator.<ref name="Amfight p197"/><ref name="complete p193">Green and Swanborough 1994, p. 193.</ref><ref name="Wingsp9">Boyne 2001, p. 9.</ref>

Two were built, but only one was flown, making its maiden flight on June 11, 1920.<ref name= "Amfight p197"/> The aircraft demonstrated good performance, reaching {{convert|156|mph|km/h|abbr=on}}, but the radical annular radiator was unsuccessful, having to be replaced to a more conventional unit. Because of its performance, it was decided to modify the VCP-1 to a racing aircraft, replacing the Wright-Hispano engine with a {{convert|660|hp|kW|abbr=on}} Packard 1A-2025 [[V12 engine]], becoming the '''VCP-R''' (later again rebuilt as the Verville R-1 Racer).<ref name="complete p193"/><ref name="Amfight p197-8">Angelucci and Bowers 1987, pp. 197–198.</ref>

===PW-1===
In 1920, work commenced on two new fighter aircraft based on the VCP-1, featuring an easier to build fabric covered steel-tube fuselage instead of the plywood monocoque of the VCP-1. The aircraft retained the tapered wings of the VCP-1 and was powered by a {{convert|350|hp|kW|abbr=on}} Packard 1A-1237 engine, cooled by a tunnel-style radiator located under the engine.<ref name="Amfight p198">Angelucci and Bowers 1987, p. 198.</ref><ref name="Wingsp11">Boyne 2001, p. 11.</ref>
 
The new design was initially known as the VCP-2, but was soon resignated as PW-1 (Pursuit, Water-Cooled<ref name="Dorr p23">Dorr and Donald 1990, p. 23.</ref>) in the U.S. Army Air Service's new designation system. The first aircraft was used for static testing, while the second prototype flew in November 1921,<ref name="Amfight p198"/> reaching a speed of {{convert|146|mph|km/h|abbr=on}}. It was rebuilt later that year with a new untapered set of wings using a [[Anthony Fokker|Fokker]] style thick [[airfoil]], becoming the PW-1A, but performance was reduced, and the aircraft was refitted with its original wings, reverting to the designation PW-1.<ref name="complete p193"/><ref name="Dorr p24-5">Dorr and Donald 1990, pp. 24–25.</ref>

While plans were prepared for more powerful versions fitted with revised wings, no production ensued.<ref name="complete p193"/><ref name="Dorr p24-5"/>

==Operational History==
The VCP-R made its racing debut at the 1920 Gordon Bennett Cup race held at [[Étampes]] near [[Paris]] on 28 September. Its radiator proved to be insufficient to deal with the powerful, high-compression Packard engine, however, and it retired after the first lap.<ref name="Wingsp9"/><ref>''Flight'' 7 October 1920, pp. 1055–1056.</ref> It was taken back to the U.S. and quickly modified with a larger radiator for entry in the [[Pulitzer Trophy]] air-race, held at [[Mineola, New York]] on 25 November that year. This time it was successful, winning the race at a speed of {{convert|156.5|mph|km/h|1|abbr=on}}.<ref name="Wingsp9"/><ref>''Flight'' 2 December 1920, p. 1244.</ref>

The U.S. Armed Forces did not compete in the 1921 Pulitzer Trophy race,<ref>''Flight'' 1 December 1921, p. 805.</ref> but for the 1922 race the VCP-R was fitted with a revised tail, becoming the R-1. It finished in sixth place at a speed of {{convert|179|mph|km/h|abbr=on}}.<ref name= "Wingsp9"/><ref name="Amfight p197-8"/><ref>''Flight'' 19 October 1922, pp. 603–605.</ref>

==Variants==
;VCP-1
:Single-seat biplane fighter, powered by 300&nbsp;hp Wright-Hispano engine. Two built, one flown.<ref name="Amfight p197-8"/>
;VCP-R
:Modification of VCP-1 for air-racing, with 660&nbsp;hp Packard 1A-2025 engine, and capable of reaching a speed of {{convert|177|mph|km/h|abbr=on}}.<ref name="Amfight p198"/>
;[[Verville-Packard R-1 Racer|R-1]]
:Modification of VCP-R for 1922 Pulitzer Trophy race. Maximum speed {{convert|186|mph|km/h|abbr=on}}.<ref name="Amfight p198"/>
;PW-1
:Modified fighter aircraft with steel-tube fuselage and 350&nbsp;hp Packard 1A-1237 engine. Two built but only one flown.<ref name="Amfight p198-9"/>
;PW-1A
:PW-1 fitted with new, Fokker-style wings. Speed reduced to {{convert|134|mph|km/h|abbr=on}}.<ref name="Amfight p199">Angelucci and Bowers 1987, p. 199.</ref>

==Specifications (PW-1)==
{{Aircraft specs
|ref=The American Fighter<ref name="Amfight p198-9">Angelucci and Bowers 1987, pp. 198–199.</ref>
|prime units?=imp
|crew=1
|capacity=
|length m=
|length ft=22
|length in=6
|span m=
|span ft=32
|span in=0
|height m=
|height ft=8
|height in=4
|wing area sqm=
|wing area sqft=269
|airfoil=
|empty weight kg=
|empty weight lb=2069
|gross weight kg=
|gross weight lb=3005
|fuel capacity=
|eng1 number=1
|eng1 name=Packard 1A-1237
|eng1 type=
|eng1 kw=
|eng1 hp=350
|max speed kmh=
|max speed mph=146
|max speed kts=
|max speed note=at sea level
|cruise speed kmh=
|cruise speed mph=132
|cruise speed kts=
|range km=
|range miles=
|range nmi=
|combat range km=
|combat range miles=
|combat range nmi=
|endurance=2 hr 30 min
|ceiling m=
|ceiling ft=19300
|climb rate ms=
|climb rate ftmin=
|time to altitude=11 minutes to 10,000 ft (3,050 m)
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|guns= 
|bombs= 
|avionics=
}}

==See also==
{{Portal|aviation}}

==References==
;Notes
{{reflist|2}}
;Bibliography
{{Refbegin}}
*Angelucci, Enzo and [[Peter M. Bowers]]. ''The American Fighter''. Sparkford, UK:Haynes Publishing Group, 1987. ISBN 0-85429-635-2.
*[[Walter J. Boyne|Boyne, Walter J.]] "The Treasures of McCook Field: America's First Aero Engineering and Testing Centre, Part 1". ''The Best of Wings Magazine''. Washington DC:Brassey's, 2001. pp.&nbsp;1–9. ISBN 1-57488-368-2.
*Boyne, Walter J. "The Treasures of McCook Field: America's First Aero Engineering and Testing Centre, Part 2". ''The Best of Wings Magazine''. Washington DC:Brassey's, 2001. pp.&nbsp;10–18. ISBN 1-57488-368-2.
*Dorr, Robert F. and David Donald. ''Fighters of the United States Air Force''. London:Temple Press/Aerospace, 1990. ISBN 0-600-55094-X.
*[http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%201053.html "Gordon-Bennett 1920: The Cup Goes to France"]. ''[[Flight International|Flight]]'', 7 October 1920. pp.&nbsp;1055–1059.
*Green, William and Gordon Swanborough. ''The Complete Book of Fighters''. New York: Smithmark, 1994. ISBN 0-8317-3939-8.
*[http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%201242.html "Reports and Memoranda: The Pulitzer Trophy Race"]. ''Flight'', 2 December 1920, p.&nbsp;1244.
*[http://www.flightglobal.com/pdfarchive/view/1922/1922%20-%200603.html "The 1922 Pulitzer Trophy Race"]. ''Flight'', 19 October 1922, pp.&nbsp;603–605.
*[http://www.flightglobal.com/pdfarchive/view/1921/1921%20-%200805.html "The Second Annual Pulitzer Race at Omaha"]. ''Flight'', 1 December 1921, p.&nbsp;805.

{{Refend}}

==External links==
{{commons category|Verville aircraft}} 
* http://www.americancombatplanes.com/biplane_army_1.html

{{Verville Air}}
{{Engineering Division aircraft}}
{{USAAS fighters}}

{{DEFAULTSORT:VCP, Verville}}
[[Category:United States fighter aircraft 1920–1929]]
[[Category:Verville aircraft|VCP]]
[[Category:Engineering Division aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]