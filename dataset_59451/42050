{{Use dmy dates|date=October 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Bugatti Model 100
 | image=Bugattii100display.jpg
 | caption=Bugatti Model 100 on display
}}{{Infobox Aircraft Type
 | type=[[Air racing|Unlimited Racer]]
 | national origin=[[France]]
 | manufacturer=[[Bugatti]]
 | designer=[[Louis de Monge]]
 | first flight=
 | introduced=
 | retired=
 | status=On Display
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced=1939<!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from=
 | variants with their own articles=
}}
|}
The '''Bugatti Model 100''' was a purpose built [[Air racing|air racer]] designed to compete in the 1939 [[Henri Deutsch de la Meurthe|Deutsch de la Meurthe Cup Race]]. The aircraft was not completed by the September 1939 deadline and was put in storage prior to the [[Battle of France|German invasion of France]].

==Development==
[[Ettore Bugatti]] started work in 1938 to design a racer to compete in the Deutsch de la Meurthe Cup Race, using engines sold in his automotive line for co-marketing.

Bugatti's chief engineer was [[Louis de Monge]], with whom Bugatti had worked before. Bugatti was also approached by the French Government to use the technology of the racing aircraft to develop a fighter variant for mass production. The aircraft was the source of five modern patents including the inline engines, V tail mixer controls, and the automatic flap system.<ref>{{cite web|title=Resurrecting the Bugatti Racer|url=http://www.eaa.org/news/2009/2009-10-23_bugatti.asp|accessdate=26 June 2011}}</ref>

==Design==
The Model 100 had an unusual inboard mounted twin engine arrangement driving forward mounted [[contra-rotating propellers]] through driveshafts.

The aircraft also featured a 120 degree [[V-tail]] arrangement and [[retractable landing gear]]. The construction was mostly of wood, with sandwiched layers of balsa and hardwoods, including [[tulipwood]] stringers covered with [[aircraft fabric covering|doped fabric]].

==Operational history==
With the outbreak of WWII and the imminent fall of Paris, Bugatti had the aircraft disassembled and hidden on his estate. Bugatti died in 1947, having never resumed work on it.<ref name=":0">{{Cite web|url=http://www.airspacemag.com/history-of-flight/Aviations-Sexiest-Racer-180952406/?all|title=Aviation’s Sexiest Racer|last=Lerner|first=Preston|access-date=2016-08-12}}</ref>

The aircraft remained in storage throughout [[World War II]]. It was sold several times, and its twin Bugatti 50P engines were removed for automotive restorations. In 1971 a restoration effort was started. The aircraft was stored by the [[National Museum of the United States Air Force]], then transferred to the [[EAA Airventure Museum]] collection where restoration was completed, and it remains on static display.<ref>{{cite web|title=Bugatti Model 100|url=http://museum.eaa.org/collection/aircraft/Bugatti%20Model%20100%20Racer.asp#TopOfPage|accessdate=28 March 2011}}</ref>

==Blue Dream reproduction aircraft==
[[File:Bugatti 100P replica.jpg|thumb|right|Bugatti 100P replication progress in 2011]]
A full scale flying reproduction was constructed by a team of enthusiasts, most notably Scotty Wilson and John Lawson.<ref>{{cite web|title=Update: Bugatti 100p Team |url=http://www.bugatti100p.com/index.php?p=1_4_Team|accessdate=8 February 2013}}</ref><ref>{{cite web|url=http://www.avweb.com/avwebflash/news/bugatti_100P_project_first_flight_207376-1.html|title=Bugatti Aircraft To Fly, Soon|publisher=AVweb|first1=Glenn|last1=Pew|date=20 September 2012|accessdate=20 August 2015}}</ref><ref>{{cite web|url=http://www.avweb.com/podcast/AudioPodcast_Bugatti100P_ScottyWilson_EttoreBugattiAirplane_LuisDeMonge_207378-1.html?kw=RelatedStory|title=Podcast: The Bugatti Project|publisher=AVweb|first1=Glenn|last1=Pew|date=21 September 2012|accessdate=20 August 2015}}</ref> Modern materials were used within reason, allowing for cost and safety. The use of [[magnesium]], named in the original design to save weight, was rejected due to its flammability and cost. A composite wood ([[DuraKore]]) was used in place of the original tulipwood, and glued in place with modern [[epoxy]]. The doped fabric was replaced with [[fiberglass]].<ref name=":0" />

The partially completed aircraft was displayed at the 2011 EAA Airventure airshow.<ref>{{cite web|title=Bugatti 100P Project|url=http://www.airventure.org/news/2011/110623_bugatti.html|accessdate=25 June 2011}}</ref>

On July 4, 2015 the reproduction aircraft, named ''Blue Dream'' taxied under the power of its two [[Suzuki Hayabusa]] engines at Tulsa, Oklahoma.<ref>{{cite journal|journal=[[FlyPast]]|title=Bugatti 'Blue Dream' makes first powered run|date=September 2015|publisher=Key Publishing Ltd|issn=0262-6950}}</ref><ref>{{cite web|url=http://www.avweb.com/avwebflash/news/Bugatti-Project-Starts-Taxi-Tests-224686-1.html|title=Bugatti Project Starts Taxi Tests|publisher=AVweb|first1=Mary|last1=Grady|date=12 August 2015|accessdate=20 August 2015}}</ref>

On August 19, 2015 the team announced that they had completed their first successful test flight of the replica aircraft. Its handling characteristics were "as expected" by the team, and it achieved a maximum altitude of 100 feet (30 meters) AGL at a maximum speed of 110 knots (200&nbsp;km/h). On landing though the plane "floated much more than anticipated" and landed significantly farther down the runway than intended. Because of this the wheel brakes needed to be applied to keep from overrunning the end of the runway. Subsequently the right brake failed, sending the aircraft into the muddy soil adjacent to the runway, tipping it up in its nose and generating a prop and spinner strike.<ref>{{cite web|title=Bugatti Flies, But Damaged On Landing|url=http://www.avweb.com/avwebflash/news/Bugatti-Flies-But-Damaged-On-Landing-224726-1.html|accessdate=20 Aug 2015}}</ref> In October it made a successful flight.<ref>{{cite web|url=http://www.flyingmag.com/news/video-bugatti-100p-back-air|title=Video: Bugatti 100P Back in the Air|work=flyingmag.com|accessdate=8 August 2016}}</ref>

===Crash of reproduction aircraft===

On August 6, 2016 the reproduction aircraft crashed during its third test flight near [[Clinton-Sherman Air Force Base]] in [[Oklahoma]], killing the pilot [[Scotty Wilson]].<ref>{{cite web|title=Aircraft Safety Network, Accident Report 189123|url=https://aviation-safety.net/wikibase/wiki.php?id=189123}}</ref><ref>{{cite web|url=http://www.avweb.com/avwebflash/news/Bugatti-Builder-Killed-In-Crash-226754-1.html|title=Bugatti Builder Killed In Crash|work=AVweb|accessdate=8 August 2016}}</ref>

Less than a minute into its third, and last, flight, the aircraft banked sharply to the left and dived into a nearby field.<ref>{{cite web|title=Historic replica airplane, the Bugatti 100p, crashes near Burns Flat, pilot and designer Scotty Wilson dies|url=http://kfor.com/2016/08/06/historic-replica-airplane-the-bugatti-100p-crashes-near-burns-flat-pilot-and-designer-scotty-wilson-dies/|accessdate=11 August 2016}}</ref> The plane went in nose-first, instantly killing the pilot. The aircraft was destroyed in the crash and subsequent fire. A chase helicopter landed at the site of the crash, but was unable to render assistance.{{Citation needed|date=August 2016}}

It had been planned to retire the aircraft after this flight to an unnamed museum in the [[United Kingdom]].<ref>{{cite web|title=Pilot Killed in Crash of Bugatti 100P Replica|url=http://www.flyingmag.com/pilot-killed-in-crash-bugatti-100p-replica|accessdate=11 August 2016}}</ref> The crash is being investigated by the [[NTSB]] and the [[FAA]].<ref>{{cite web|title=Broken Arrow Pilot Killed In Crash Of Bugatti 100P Replica|url=http://www.newson6.com/story/32700117/broken-arrow-pilot-killed-in-crash-of-bugatti-100p-replica|accessdate=11 August 2016}}</ref>

==Variants==
The Bugatti Model 110P was a proposed militarized pursuit version of the model 100 racer. It never materialised.
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Bugatti Model 100) ==
{{Aircraft specs
|ref=EAA<!-- for giving the reference for the data -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=
|capacity=1
|length m=
|length ft=25
|length in=5
|length note=
|span m=
|span ft=27
|span in=
|span note=
|height m=
|height ft=7
|height in=4
|height note=
|wing area sqm=
|wing area sqft=222.7
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=3086
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=Bugatti Type 50P
|eng1 type=Straight 8 4.9L
|eng1 kw=<!-- prop engines -->
|eng1 hp=450<!-- prop engines -->
|prop number=2
|prop blade number=2<!-- propeller aircraft -->
|prop name=Ratier
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=Counter Rotating

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=13.9
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==References==
{{reflist|30em}}

==External links==
{{Commons category}}
*[http://www.bugattipage.com/bugatti-100p-record-plane.htm Bugatti 100p Airplane book]
*[http://bugatti100p.aero/ Bugatti Reproduction website]
*[https://www.youtube.com/watch?v=78xPUBO034g News Story on loss of the reproduction aircraft (YouTube)]

[[Category:Bugatti aircraft]]
[[Category:Aircraft with contra-rotating propellers]]