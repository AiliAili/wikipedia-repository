{{italic title}}
{{Infobox journal
 | title = Korean Journal of Sociology
 | cover = [[File:KoreanJofSoc img1.jpg]]
 | discipline = [[Sociology]]
 | publisher = [[Seoul National University|Seoul National University Press]] on behalf of the [[Korean Sociological Association]]
 | country = [[South Korea]]
 | language = [[Korean language|Korean]], [[English language|English]]
 | license =
 | editor = In Hee Hahm
 | frequency = Bimonthly
 | history = 1964-present
 | website         = http://www.kjs.re.kr/aboutkjs.htm
 | link1         =
 | link1-name    =
 | impact =
 | impactyear =
 | eISSN =
 | ISSN = 1225-0120
 | OCLC  =
}}
{{Infobox Korean name|
img=|
hangul=한국사회학|
hanja={{linktext|韓|國|社|會|學}}|
rr=Hanguksahoehak|
mr=Hankuksahoehak|
}}

The '''''Korean Journal of Sociology''''' (Korean: Han'guk Sahoehak, 한국사회학) is a [[peer review]]ed [[academic journal]] on [[sociology]] that was established in 1964. It is the official journal of the [[Korean Sociological Association]] and covers theoretical developments, results of qualitative or quantitative research that advance our understanding of [[Korea]]n society, and related subjects.<ref name =about>{{cite web |url=http://www.kjs.re.kr/aboutkjs.htm |title=About the ''Korean Journal of Sociology'' |work=Korean Sociological Association |accessdate=2010-04-08}}</ref> The aim of the journal is to promote academic interaction and communication among sociologists in Korea and abroad.<ref name =about/> The [[editor in chief]] is In Hee Hahm ([[Ewha Womans University]]). The journal was published quarterly between 1964 and 2000, but since 2001 is published bimonthly, with issues in [[Korean language|Korean]] published in February, April, August and October (issues 1, 2, 4, 5) and issues in [[English language|English]] published biannually in June and December (issues 3 and 6).<ref name="about"/> The journal is abstracted and indexed by [[CSA (database company)|CSA Illumina]]'s ''Sociological Abstracts'' as a "core" journal.<ref>{{cite web |url=http://www.csa.com/factsheets/supplements/sociossl.php#H |title=Sociological Abstracts - Current Serials Source List |format= |work= |accessdate=2010-04-08}}</ref>

== References ==
{{Reflist}}

== External links ==

* {{Official website|http://www.kjs.re.kr/}}
* [http://www.ksa.re.kr/ENGLISH Korean Sociological Association]

[[Category:Sociology journals]]
[[Category:Asian studies journals]]
[[Category:Multilingual journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1964]]