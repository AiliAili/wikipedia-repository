<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Tipsy Junior
 |image=Tipsy Junior G-AMVP 1953.jpg
 |caption=Tipsy Junior G-AMVP at a UK airshow in 1953
}}{{Infobox Aircraft Type
 |type=Sports plane
 |manufacturer=[[Avions Fairey]]
 |designer=[[Ernest Tips]]
 |first flight=30 June [[1947 in aviation|1947]]
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=2
 |variants with their own articles=
}}
|}

The '''Avions Fairey Junior''', also known as the '''Tipsy Junior''' was a single-seat light aircraft built in [[Belgium]] following [[World War II]].

==Development==
The Junior was one of a series of light aircraft<ref>{{Harvnb|Taylor|1974|pages=39–44}}</ref> designed by and named after [[Ernest Oscar Tips|E.O.Tips]] of [[Fairey Aviation]]'s Belgian subsidiary, [[Avions Fairey]]. Of wood and fabric construction, it was a conventional, low-wing [[monoplane]] with a [[Conventional landing gear|tailwheel undercarriage]] and a single seat, open [[cockpit]],<ref name="AJJ">{{Harvnb|Jackson|1960|pages=448}}</ref> though there was the option of a bubble hood.<ref name="Flight">[http://www.flightglobal.com/pdfarchive/view/1947/1947%20-%201261.html ''Flight'' Tipsy Junior]</ref> The constant chord wings were almost square ended and the tailplane, fin and rudder also angular. Both completed aircraft were initially powered by the 36&nbsp;hp (27&nbsp;kW) [[Aeronca E-113|Aeronca JAP J-99]]  engine, later replaced by the more powerful, 62&nbsp;hp (46&nbsp;kW) [[Walter Mikron]] 2.<ref name="AJJ"/><ref name="Taylor">{{Harvnb|Taylor|1974|pages=44}}</ref>

The Junior, registered ''OO-TIT'', flew for the first time on 30 June 1947 from Gosselies in Belgium.<ref name="Flight"/>

==Operational history==

The first Junior was written off after a hard landing in 1948.<ref name="Tip"/>

The second example (construction number J.111, registration ''OO-ULA'') was bought by Fairey and taken to [[England]] in 1953, where it was registered as ''G-AMVP''.<ref name="AJJ"/><ref name="Taylor"/> In 1957, it was used in a publicity stunt when Fairey test pilot [[Peter Twiss]] landed it on the [[aircraft carrier]] [[HMS Ark Royal (R09)|HMS ''Ark Royal'']].  For part of its time it had the bubble canopy.  Rebuilt after a long time in storage following a forced landing in 1993,<ref name="Tip"/> it flew again late in 2006.<ref name="Tip">[http://www.tipsy-histories.com/site/junior.htm The Ultimate Tipsy Site] {{webarchive |url=https://web.archive.org/web/20070929084617/http://www.tipsy-histories.com/site/junior.htm |date=September 29, 2007 }}</ref>  It had a minor landing accident in 2008<ref>[http://www.aaib.gov.uk/publications/bulletins/february_2009/avions_fairey_sa_tipsy_junior__g_amvp.cfm G-AMVP accident]</ref> but had a permit to fly until May 2009.<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=AMVP CAA G-AMVP]</ref>

The Junior did not sell, and the third airframe was cancelled before completion.
It was purchased incomplete by Fairey in 1961 and has been under construction in the hands of a number of owners in the intervening years, but never finished.<ref name="Tip"/>

==Specifications==
[[File:Fj4.jpg|thumb|right|Flying scale model of Avions Fairey Tipsy Junior, built by Donald Granlund]]
{{aerospecs
|ref=<ref name="AJJ"/>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met
|crew=one pilot
|capacity=
|length m=5.65
|length ft=18
|length in=6
|span m=6.90
|span ft=22
|span in=8
|dia m=<!-- helicopters -->
|dia ft=<!-- helicopters -->
|dia in=<!-- helicopters -->
|height m=1.48
|height ft=4
|height in=10
|wing area sqm=10.5<ref name="Tip"/>
|wing area sqft=113
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=220
|empty weight lb=486
|gross weight kg=350
|gross weight lb=770
|eng1 number=1
|eng1 type=[[Walter Mikron]] II inverted [[straight engine|inline engine]]
|eng1 kw=<!-- prop engines -->46
|eng1 hp=<!-- prop engines -->62
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=174
|max speed mph=108
|max speed mach=<!-- for supersonic aircraft -->
|cruise speed kmh=158<!-- if max speed unknown -->
|cruise speed mph=98<!-- if max speed unknown -->
|range km=690<ref name="Tip"/>
|range miles=430
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=1,800<ref name="Tip"/>
|ceiling ft=6,000
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

==References==
{{commons category|Tipsy Junior}}

===Notes===
 {{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book |title=Fairey Aircraft since 1915 |last=Taylor |first=H.O. |year=1974 |publisher=Putnam Publishing  |location=London |isbn= 0-370-00065-X |ref=harv}}
*{{cite book |title= British Civil Aircraft 1919-59 vol 2 |last=Jackson |first=A.J. |year=1960 |publisher=Putnam Publishing  |location=London |ref=harv}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages=90 }}
* {{cite book |last= Simpson |first= R. W. |title=Airlife's General Aviation |year=1995 |publisher=Airlife Publishing |location=Shrewsbury |pages=387 }}
{{refend}}

{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
|see also=<!-- other relevant information -->

}}
{{Fairey aircraft}}

[[Category:Belgian sport aircraft 1940–1949]]
[[Category:Tipsy aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Single-engined tractor aircraft]]