{{Accounting}}
[[File:Wachovia National Bank 1906 statement.jpg|right|thumb|250px|Historical financial statements]]

'''Financial statements''' (or '''financial report''') is a formal record of the financial activities and position of a business, person, or other entity.

Relevant financial information is presented in a structured manner and in a form easy to understand. They typically include basic financial statements, accompanied by a [[management discussion and analysis]]:<ref>[http://www.iasplus.com/standard/ias01.htm "Presentation of Financial Statements"] Standard IAS 1, International Accounting Standards Board. Accessed 24 June 2007.</ref>

# A '''[[balance sheet]]''', also referred to as a '''statement of financial position''', reports on a company's [[asset]]s, [[Liability (financial accounting)|liabilities]], and [[Equity (finance)|owners equity]] at a given point in time.
# An '''[[income statement]]''', also known as a '''statement of comprehensive income''', '''statement of revenue & expense''', '''P&L''' or '''profit and loss report''', reports on a company's [[income]], [[expense]]s, and [[profit (accounting)|profits]] over a period of time. A profit and loss statement provides information on the operation of the enterprise. These include sales and the various expenses incurred during the stated period. 
# A '''[[Statement of changes in equity]]''', also known as '''equity statement''' or '''statement of retained earnings''', reports on the changes in [[Equity (finance)|equity]] of the company  during the stated period.
# A '''[[cash flow statement]]''' reports on a company's cash flow activities, particularly its operating, [[investing]] and [[financing]] activities.

For large corporations, these statements may be complex and may include an extensive set of '''footnotes to the financial statements''' and '''management discussion and analysis'''. The notes typically describe each item on the balance sheet, income statement and cash flow statement in further detail. Notes to financial statements are considered an integral part of the financial statements.

==Purpose for business entities==
"The objective of financial statements is to provide information about the financial position, performance and changes in financial position of an enterprise that is useful to a wide range of users in making economic decisions."<ref name="iasplus.com">[http://www.iasplus.com/standard/framewk.htm "The Framework for the Preparation and Presentation of Financial Statements"] International Accounting Standards Board. Accessed 24 June 2007.</ref> 
Financial statements should be understandable, relevant, reliable and comparable. Reported assets, liabilities, equity, income and expenses are directly related to an organization's financial position. 

Financial statements are intended to be understandable by readers who have "a reasonable knowledge of business and economic activities and accounting and who are willing to study the information diligently."<ref name="iasplus.com"/>  Financial statements may be used by users for different purposes:

*Owners and managers require financial statements to make important business decisions that affect its continued operations. [[Financial analysis]] is then performed on these statements to provide management with a more detailed understanding of the figures. These statements are also used as part of management's annual report to the stockholders.
*Employees also need these reports in making [[collective bargaining]] agreements (CBA) with the management, in the case of [[labor unions]] or for individuals in discussing their compensation, promotion and rankings.
*Prospective [[investor]]s make use of financial statements to assess the viability of investing in a business. Financial analyses are often used by investors and are prepared by professionals (financial analysts), thus providing them with the basis for making investment decisions.
*Financial institutions (banks and other lending companies) use them to decide whether to grant a company with fresh [[working capital]] or extend debt [[Security (finance)|securities]] (such as a long-term [[bank loan]] or [[debentures]]) to finance expansion and other significant expenditures.

==Consolidated==
{{main|Consolidated financial statement}}

Consolidated financial statements are defined as "[[Financial statements]]  of a group in which the [[asset]]s, [[liability (financial accounting)|liabilities]], [[equity (finance)|equity]], [[income]], [[expense]]s and [[cash flow]]s of the parent (company) and its [[subsidiaries]] are presented as those of a single [[economic entity]]", according to [[International Accounting Standard]] 27 "Consolidated and separate [[financial]] statements", and [[International Financial Reporting Standard]] 10 "Consolidated financial statements".<ref>{{cite web |url=http://www.iasplus.com/en/standards/ias/ias27-2011 |title=IAS 27 — Separate Financial Statements (2011) |publisher=IAS Plus (This material is provided by Deloitte Touche Tohmatsu Limited (“DTTL”), or a member firm of DTTL, or one of their related entities. This material is provided “AS IS” and without warranty of any kind, express or implied.  Without limiting the foregoing, neither Deloitte Touche Tohmatsu Limited (“DTTL”), nor any member firm of DTTL (a “DTTL Member Firm”), nor any of their related entities (collectively, the “Deloitte Network”) warrants that this material will be error-free or will meet any particular criteria of performance or quality, and each entity of the Deloitte Network expressly disclaims all implied warranties, including without limitation warranties of merchantability, title, fitness for a particular purpose, non-infringement, compatibility and accuracy.) |website= www.iasplus.com |date= |accessdate=2013-11-29}}</ref><ref>{{cite web |url=http://www.iasplus.com/en/standards/ifrs/ifrs10 |title=IFRS 10 — Consolidated Financial Statements |publisher=IAS Plus (This material is provided by Deloitte Touche Tohmatsu Limited (“DTTL”), or a member firm of DTTL, or one of their related entities. This material is provided “AS IS” and without warranty of any kind, express or implied.  Without limiting the foregoing, neither Deloitte Touche Tohmatsu Limited (“DTTL”), nor any member firm of DTTL (a “DTTL Member Firm”), nor any of their related entities (collectively, the “Deloitte Network”) warrants that this material will be error-free or will meet any particular criteria of performance or quality, and each entity of the Deloitte Network expressly disclaims all implied warranties, including without limitation warranties of merchantability, title, fitness for a particular purpose, non-infringement, compatibility and accuracy.) |website= www.iasplus.com |date= |accessdate=2013-11-29}}</ref>

==Government==
{{see also|Fund accounting}}

The rules for the recording, measurement and presentation of [[government financial statements]] may be different from those required for business and even for non-profit organizations.  They may use either of two [[Basis of accounting|accounting methods]]: accrual [[accounting]], or cost accounting, or a combination of the two ([[OCBOA]]). A complete set of [[chart of accounts]] is also used that is substantially different from the chart of a profit-oriented business.

==Personal==<!-- [[Personal financial statement]] redirects here -->
Personal financial statements may be required from persons applying for a [[personal loan]] or [[Welfare|financial aid]]. Typically, a personal financial statement consists of a single form for reporting personally held assets and liabilities (debts), or personal sources of income and expenses, or both. The form to be filled out is determined by the organization supplying the loan or aid.

==Audit and legal implications==
Although laws differ from country to country, an [[audit]] of the financial statements of a public company is usually required for investment, financing, and tax purposes. These are usually performed by independent accountants or auditing firms. Results of the audit are summarized in an [[auditor's report|audit report]] that either provide an unqualified opinion on the financial statements or qualifications as to its fairness and accuracy. The audit opinion on the financial statements is usually included in the annual report.

There has been much legal debate over who an auditor is liable to. Since audit reports tend to be addressed to the current shareholders, it is commonly thought that they owe a legal duty of care to them. But this may not be the case as determined by common law precedent.  In Canada, auditors are liable only to investors using a prospectus to buy shares in the primary market.  In the [[United Kingdom]], they have been held liable to potential investors when the auditor was aware of the potential investor and how they would use the information in the financial statements. Nowadays auditors tend to include in their report liability restricting language, discouraging anyone other than the addressees of their report from relying on it. Liability is an important issue: in the UK, for example, auditors have [[unlimited liability]].

In the [[United States]], especially in the post-[[Enron]] era there has been substantial concern about the accuracy of financial statements. Corporate officers (the [[chief executive officer]] (CEO) and [[chief financial officer]] (CFO)) are personally responsible for fair financial reporting allowing those reading the report to have a good sense of the organization.

==Standards and regulations==
Different countries have developed their own accounting principles over time, making international comparisons of companies difficult. To ensure uniformity and comparability between financial statements prepared by different companies, a set of guidelines and rules are used. Commonly referred to as [[Generally Accepted Accounting Principles]] (GAAP), these set of guidelines provide the basis in the preparation of financial statements, although many companies [[voluntary disclosure|voluntarily disclose]] information beyond the scope of such requirements.<ref name=FASB2001>FASB, 2001. [http://www.fasb.org/brrp/brrp2.shtml Improving Business Reporting: Insights into Enhancing Voluntary Disclosures]. Retrieved on April 20, 2012.</ref>

Recently there has been a push towards standardizing accounting rules made by the [[International Accounting Standards Board]] ("IASB"). IASB develops [[International Financial Reporting Standards]] that have been adopted by [[Australia]], Canada and the [[European Union]] (for publicly quoted companies only), are under consideration in [[South Africa]] and [[International Financial Reporting Standards#Adaptation and convergence|other countries]]. The [[United States]] [[Financial Accounting Standards Board]] has made a commitment to converge the U.S. GAAP and IFRS over time.

==Inclusion in annual reports==
To entice new investors, public companies assemble their financial statements on fine paper with pleasing graphics and photos in an [[Annual report|annual report to shareholders]], attempting to capture the excitement and culture of the organization in a "marketing [[brochure]]" of sorts. Usually the company's chief executive will write a letter to shareholders, describing management's performance and the company's financial highlights.

In the United States, prior to the advent of the internet, the annual report was considered the most effective way for corporations to communicate with individual shareholders. [[Blue chip (stock market)|Blue chip]] companies went to great expense to produce and mail out attractive annual reports to every shareholder. The annual report was often prepared in the style of a [[coffee table book]].

==Notes==
Notes to financial statements (notes) are additional information added to the end of financial statements that help explain specific items in the statements as well as provide a more comprehensive assessment of a company's financial condition. Notes to financial statements can include information on [[debt]], [[going concern]] criteria, [[Account (accountancy)|accounts]], [[contingent liabilities]] or contextual information explaining the financial numbers (e.g. to indicate a lawsuit).

The notes clarify individual statement line-items. For example, if a company lists a loss on a [[fixed asset]] impairment line in their income statement, notes could state the reason for the impairment by describing how the asset became impaired. Notes are also used to explain the accounting methods used to prepare the statements and they support valuations for how particular accounts have been computed.

In [[consolidated financial statement]]s, all [[subsidiaries]] are listed as well as the amount of ownership ([[controlling interest]]) that the [[parent company]] has in the subsidiaries. Any items within the financial statements that are valuated by estimation are part of the notes if a substantial difference exists between the amount of the estimate previously reported and the actual result. Full disclosure of the effects of the differences between the estimate and actual results should be included.

==Management discussion and analysis==
Management discussion and analysis or MD&A is an integrated part of a company's annual financial statements. The purpose of the MD&A is to provide a narrative explanation, through the eyes of management, of how an entity has performed in the past, its financial condition, and its future prospects. In so doing, the MD&A attempt to provide investors with complete, fair, and balanced information to help them decide whether to invest or continue to invest in an entity.<ref>[http://www.cica.ca/research-and-guidance/mda-and-business-reporting/index.aspx MD&A & Other Performance Reporting]</ref>

The section contains a description of the year gone by and some of the key factors that influenced the business of the company in that year, as well as a fair and unbiased overview of the company's past, present, and future.

MD&A typically describes the corporation's [[Accounting liquidity|liquidity position]], capital resources,<ref>[http://www.nikoresources.com/2002manage.html Nico Resources Management's Discussion and Analysis]</ref> results of its operations, underlying causes of material changes in financial statement items (such as asset impairment and restructuring charges), events of unusual or infrequent nature (such as [[mergers and acquisitions]] or [[share buyback]]s), positive and negative trends, effects of [[inflation]], domestic and international market risks,<ref>[http://www.pepsico.com/Annual-Reports/1998/financial/analysis.html PepsiCo Management's Discussion and Analysis]</ref> and significant uncertainties.

==Moving to electronic==
Financial statements have been created on paper for hundreds of years. The growth of the Web has seen more and more financial statements created in an electronic form which is exchangeable over the Web. Common forms of electronic financial statements are PDF and HTML. These types of electronic financial statements have their drawbacks in that it still takes a human to read the information in order to reuse the information contained in a financial statement.

More recently a market driven global standard, [[XBRL]] (Extensible Business Reporting Language), which can be used for creating financial statements in a structured and computer readable format, has become more popular as a format for creating financial statements.  Many regulators around the world such as the [[U.S. Securities and Exchange Commission]] have mandated XBRL for the submission of financial information.

The [[UN/CEFACT]] created, with respect to Generally Accepted Accounting Principles, ([[Generally accepted accounting principles|GAAP]]), internal or external [[financial reporting]] [[XML]] messages to be used between enterprises and their partners, such as private interested parties (e.g. bank) and public collecting bodies (e.g. taxation authorities). Many regulators use such messages to collect financial and economic information.

==See also==
*[[Accountable Fundraising]]
*[[Center for Audit Quality]] (CAQ)
*[[Corporate finance]]
*[[Financial statement analysis]]
*[[Comprehensive annual financial report]]
*[[Model audit]]

==References==
{{Reflist}}

==Further reading==
* Alexander, D., Britton, A., Jorissen, A., "International Financial Reporting and Analysis", Second Edition, 2005, ISBN 978-1-84480-201-2

==External links==
{{Library resources box 
|by=no 
|onlinebooks=no 
|others=no 
|about=yes 
|label=Financial statement }}
*[http://www.ifrs.org IFRS Foundation & International Accounting Standards Board]
*[http://www.fasb.org Financial Accounting Standards Board] (U.S.)
*[http://www.unece.org/trade/untdid/welcome.htm UN/CEFACT]
*[http://www1.unece.org/cefact/platform/display/TBG/TBG12 UN/CEFACT Trade And Business Group Accounting And Audit]
*{{cite web|last=Mańko|first=Rafał|title=New legal framework for financial statements|url=http://www.europarl.europa.eu/RegData/bibliotheque/briefing/2013/130552/LDM_BRI(2013)130552_REV1_EN.pdf|work=Library Briefing|publisher=Library of the European Parliament|accessdate=6 June 2013}}
* [http://www.investopedia.com/university/fundamentalanalysis/notes.asp Fundamental Analysis: Notes To The Financial Statements] by [[Investopedia.com]]
* [http://www.googobits.com/articles/1409-the-notes-to-the-financial-statements-may-be-worth-noting.html The Notes to the Financial Statements May Be Worth Noting] by Googobits.com

{{Authority control}}

[[Category:Financial statements]]