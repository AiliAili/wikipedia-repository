{{Use mdy dates|date=June 2012}}
{{good article}}
{{Infobox monarch
| name            =Artaxerxes III<br>''(Artaxšaçā)''
| title           = [[List of kings of Persia|King of Persia]]<br>[[Pharaoh]] of Egypt
| image           = Artaxerxes III of Persia.jpg
| image_size      = 230px
| caption         =
| reign           =358–338 BC
| coronation      =[[Pasargadae]], February or March 358 BC<ref name="Ochus">{{cite web |url=http://www.livius.org/arl-arz/artaxerxes/artaxerxes_iii_ochus.html |title=Artaxerxes III Ochus |accessdate=March 1, 2008 |last=Lendering  |first=Jona |authorlink=Jona Lendering |work=Livius.Org  | archiveurl= https://web.archive.org/web/20080222120843/http://www.livius.org/arl-arz/artaxerxes/artaxerxes_iii_ochus.html| archivedate= February 22, 2008 <!--DASHBot-->| deadurl= no}}</ref>
| full name       = Artaxerxes III Ochus
| succession1     = [[List of kings of Persia|King of Persia]]
| predecessor1    = [[Artaxerxes II]] 
| successor1      = [[Artaxerxes IV]]
| succession2     = [[List of Pharaohs|Pharaoh of Egypt]]
| predecessor2    = [[Nectanebo II]]
| successor2      = [[Artaxerxes IV]]
| spouse          =
| issue           = [[Arses of Persia|Arses]]<br/>[[Parysatis II|Parysatis]]
| royal house     = [[Achaemenid dynasty|Achaemenid]]
| dynasty         =
| father          = [[Artaxerxes II]]
| mother          = [[Stateira (wife of Artaxerxes II)|Stateira]]
| birth_date      =
| birth_place     =
| death_date      = August 26,/September 25, 338 BC<ref name="Ochus"/>(87 years old)
| death_place     =
| date of burial  =
| place of burial =[[Persepolis]]
|}}

'''Artaxerxes III Ochus of Persia''' {{IPAc-en|ˌ|ɑr|t|ə|ˈ|z|ɜr|k|s|iː|z}} {{nowrap|c. 425 BC –}} 338 BC; ({{lang-peo|{{OldPers|A|12}}{{OldPers|RA|12}}{{OldPers|TA|12}}{{OldPers|XA|12}}{{OldPers|SHA|12}}{{OldPers|SSA|12}}}} ''Artaxšaçā'')<ref>{{cite book|last=Ghias Abadi|first=R. M.|title=Achaemenid Inscriptions (کتیبه‌های هخامنشی)&lrm;|edition=2nd|publisher=Shiraz Navid Publications|year=2004|location=Tehran|isbn=964-358-015-6|pages=144|language=Persian}}</ref> was the [[List of kings of Persia|Great King (Shah) of Persia]] and the eleventh king of the [[Achaemenid Empire]], as well as the first [[Pharaoh]] of the [[Thirty-first Dynasty of Egypt|31st dynasty]] of [[Egypt]]. He was the son and successor of [[Artaxerxes II]] and was succeeded by his son, [[Arses of Persia]] (also known as Artaxerxes IV). His reign coincided with the reign of [[Philip II of Macedon|Philip II]] in [[Macedon]] and [[Nectanebo II]] in Egypt.

Before ascending the throne Artaxerxes was a [[satrap]] and commander of his father's army. Artaxerxes came to power after one of his brothers was executed, another committed suicide, the last murdered and his father, [[Artaxerxes II]] died. Soon after becoming king, Artaxerxes murdered all of the royal family to secure his place as king. He started two major campaigns against Egypt. The first campaign failed, and was followed up by rebellions throughout the western part of his empire. In 343&nbsp;BC, Artaxerxes defeated [[Nectanebo II]], the [[Pharaoh]] of Egypt, driving him from [[History of ancient Egypt|Egypt]], stopping a revolt in Phoenicia on the way.

In Artaxerxes' later years, [[Philip II of Macedon]]'s power was increasing in Greece, where he tried to convince the Greeks to revolt against [[Achaemenid Empire|Achaemenid Persia]]. His activities were opposed by Artaxerxes, and with his support, the city of [[Perinthus]] resisted a Macedonian siege.

There is evidence for a renewed building policy at [[Persepolis]] in his later life, where Artaxerxes erected a new palace and built his own tomb, and began long-term projects such as the Unfinished Gate.

According to a Greek source, [[Diodorus of Sicily]], [[Bagoas]] poisoned Artaxerxes, but a cuneiform tablet (now in the [[British Museum]]) suggests that the king died from natural causes.<ref>{{cite web |url=http://www.livius.org/arl-arz/artaxerxes/artaxerxes_iv.html |title=Artaxerxes IV Arses |accessdate=June 8, 2008 |last=Lendering |first=Jona |authorlink=Jona Lendering| archiveurl= https://web.archive.org/web/20080513084133/http://www.livius.org/arl-arz/artaxerxes/artaxerxes_iv.html| archivedate= May 13, 2008 <!--DASHBot-->| deadurl= no}}</ref>

==Name==
Artaxerxes III ({{lang-peo|{{OldPers|A|12}}{{OldPers|RA|12}}{{OldPers|TA|12}}{{OldPers|XA|12}}{{OldPers|SHA|12}}{{OldPers|SSA|12}}}}, Artaxšaçrā, ''"he whose empire is well-fitted"'' or ''"perfected", or Arta:"honoured"+Xerxes:"a king" ("the honoured king"), according to [[Herodotus]] "the great warrior"''<ref name="Name">{{cite web |url=
http://encyclopedia.jrank.org/ARN_AUD/ARTAXERXES.html |title=Artaxerxes |accessdate=March 11, 2008| archiveurl= https://web.archive.org/web/20080409145002/http://encyclopedia.jrank.org/ARN_AUD/ARTAXERXES.html| archivedate= April 9, 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="name2">{{cite book |last=Smith |first=William |authorlink=William Smith (lexicographer) |editor=William Smith |title=[[Dictionary of Greek and Roman Biography and Mythology]] |origyear=1842 |accessdate=2008-03-25 |volume=1 |year=1867 |publisher=Boston:Little, Brown, and Company |pages=371 |chapter=Artaxerxes |quote=Arta:"honoured"+Xerxes:"a king" ("the honoured king"), according to [[Herodotus]] "the great warrior"''}}</ref>) was the throne name adopted by Ochus when he succeeded [[Artaxerxes II|his father]] in 358&nbsp;BC. He is generally referred to as Ochus, but in [[Iran]] he is known as Ardeshir III (اردشیر سوم [[Persian language|Modern Persian]] form of Artaxerxes). In [[Babylon]]ian inscriptions he is called ''"Umasu, who is called Artakshatsu"''. The same form of the name (probably pronounced Uvasu) occurs in the Syrian version of the [[Canon of Kings]] by Elias of Nisibis.<ref name="Name"/>

==Early life and accession==
Before ascending the throne Artaxerxes had been a [[satrap]] and commander of his father's army.<ref name="pop">{{cite book |title=A History of the Jews and Judaism in the Second Temple Period |last=Grabbe |first=Lester L. |year=2004 |publisher=Continuum International Publishing Group |isbn=0-567-08998-3 |pages=323 }}</ref> In 359 BC, just before ascending the throne, he attacked Egypt as a reaction to Egypt's failed attacks on coastal regions of [[Phoenicia]].<ref name=Judah>{{cite book |title=Judah and the Judeans in the Fourth Century B.C.E. |last=Lipschits |first=Oded |editor=Garry N. Knoppers, Rainer Albertz |year=2007 |publisher=EISENBRAUNS |isbn=1-57506-130-9 |pages=87 }}</ref> In 358&nbsp;BC his father, [[Artaxerxes II]], died--it was said to be because of a broken heart caused by his children's behaviour--and, since his other sons, Darius, Ariaspes and Tiribazus had already been eliminated by plots, Artaxerxes III succeeded him as king.<ref>{{cite book |last=Rhodes |first=Peter J.  |title=A History of the Classical Greek World: 478–323 BC |year= 2006|url=https://books.google.com/books?id=dKhOwYzZTxsC |accessdate= September 24, 2016  |publisher=Blackwell Publishing |isbn=0-631-22564-1 |pages=224}}</ref> His first order was the execution of over 80 of his nearest relations to secure his place as king.<ref name="Lemprière">{{cite book |last=Lemprière |first=John |authorlink=John Lemprière |author2=R. Willets  |title=[[Lemprière's Bibliotheca Classica|Classical Dictionary containing a full Account of all the Proper Names mentioned in Ancient Authors]] |origyear=1788 |year=1984 |publisher=Routledge |isbn=0-7102-0068-4 |pages=82}}<!--|accessdate= June 2008  --></ref>

In 355 BC, Artaxerxes forced [[Athens]] to conclude a peace which required the city's forces to leave [[Asia Minor]] and to acknowledge the independence of its rebellious allies.<ref>{{cite web |url=http://lexicorient.com/e.o/artaxerxes3.htm |title=Artaxerxes 3 |accessdate=March 5, 2008 |last=Kjeilen |first=Tore| archiveurl= https://web.archive.org/web/20080225213722/http://lexicorient.com/e.o/artaxerxes3.htm| archivedate= February 25, 2008 <!--DASHBot-->| deadurl= no}}</ref> Artaxerxes started a campaign against the rebellious [[Cadusii|Cadusians]], but he managed to appease both of the Cadusian kings. One individual who successfully emerged from this campaign was Darius Codomannus, who later occupied the Persian throne as [[Darius III]].

Artaxerxes then ordered the disbanding of all the satrapal armies of Asia Minor, as he felt that they could no longer guarantee peace in the west and was concerned that these armies equipped the western satraps with the means to revolt.<ref name="PersianArmy">{{cite book |title=The Persian Army 560–330 BC: 560–330 BC |last=Sekunda |first=Nick |author2=Nicholas V. Sekunda|author3=Simon Chew|year=1992 |publisher=Osprey Publishing |isbn=1-85532-250-1 |pages=28 }}</ref> The order was however ignored by Artabazus of [[Lydia]], who asked for the help of Athens in a rebellion against the king. Athens sent assistance to [[Sardis]]. [[Orontid dynasty#Orontids Kings and satraps of Armenia|Orontes of Mysia]] also supported Artabazus and the combined forces managed to defeat the forces sent by Artaxerxes in 354&nbsp;BC. However, in 353&nbsp;BC, they were defeated by Artaxerxes’ army and were disbanded. Orontes was pardoned by the king, while Artabazus fled to the safety of the court of [[Philip II of Macedon]].

==First Egyptian Campaign==
In around 351 BC, Artaxerxes embarked on a campaign to recover Egypt, which had revolted under his father, Artaxerxes II. At the same time a rebellion had broken out in Asia Minor, which, being supported by [[Thebes, Greece|Thebes]], threatened to become serious.<ref name="Ochus2">[http://persianempire.info/ArtaxerxesIII.htm Artaxerxes III PersianEmpire.info History of the Persian Empire<!-- Bot generated title -->]</ref> Levying a vast army, Artaxerxes marched into Egypt, and engaged [[Nectanebo II]]. After a year of fighting the Egyptian [[Pharaoh]], Nectanebo inflicted a crushing defeat on the Persians with the support of mercenaries led by the Greek generals Diophantus and Lamius.<ref>{{cite book |title=A History of Ancient Israel and Judah |last=Miller |first=James M. |others=John Haralson Hayes (photographer) |year=1986 |publisher=Westminster John Knox Press |isbn=0-664-21262-X |pages=465}}</ref> Artaxerxes was compelled to retreat and postpone his plans to reconquer Egypt.

==Rebellion of Cyprus and Sidon==
Soon after this defeat, [[Phoenicia]], [[Asia Minor]] and [[Cyprus]] declared their independence from Persian rule. In 343&nbsp;BC, Artaxerxes committed responsibility for the suppression of the Cyprian rebels to [[Idrieus]], prince of [[Caria]], who employed 8,000 Greek mercenaries and forty [[trireme]]s, commanded by [[Phocion]] the Athenian, and Evagoras, son of the elder [[Evagoras I|Evagoras]], the Cypriot monarch.<ref>{{cite book |title=A History of Discoveries at Halicarnassus, Cnidus & Branchidæ |last=Newton |first=Sir Charles Thomas |author2=R.P. Pullan |year=1862 |publisher=Day & son |pages=57 }}</ref><ref name="Persianempire">{{cite web |url=http://persianempire.info/ArtaxerxesIII.htm |title=Artaxerxes III Ochus ( 358 BC to 338 BC )|accessdate=March 2, 2008}}</ref> Idrieus succeeded in reducing Cyprus. Artaxerxes initiated a counter-offensive against [[Sidon]] by commanding the satrap of Syria and Mezseus, and the [[Cilicia (satrapy)|satrap of Cilicia]] to invade the city and to keep the [[Phoenicia]]ns in check. Both satraps suffered crushing defeats at the hands of Tennes, the Sidonese king, who was aided by 40,000 Greek mercenaries sent to him by [[Nectanebo II]] and commanded by [[Mentor of Rhodes]]. As a result, the Persian forces were driven out of [[Phoenicia]].<ref name="Persianempire"/>

After this, Artaxerxes personally led an army of 330,000 men against [[Sidon]]. Artaxerxes' army comprised 300,000 foot soldiers, 30,000 [[cavalry]], 300 triremes, and 500 transports or provision ships. After gathering this army, he sought assistance from the Greeks. Though refused aid by [[Athens]] and [[Sparta]], he succeeded in obtaining a thousand Theban heavy-armed hoplites under Lacrates, three thousand Argives under Nicostratus, and six thousand Æolians, [[Ionians]], and Dorians from the Greek cities of Asia Minor. This Greek support was numerically small, amounting to no more than 10,000 men, but it formed, together with the Greek mercenaries from Egypt who went over to him afterwards, the force on which he placed his chief reliance, and to which the ultimate success of his expedition was mainly due.

The approach of Artaxerxes sufficiently weakened the resolution of Tennes that he endeavoured to purchase his own pardon by delivering up 100 principal citizens of Sidon into the hands of the Persian king, and then admitting Artaxerxes within the defences of the town. Artaxerxes had the 100 citizens transfixed with javelins, and when 500 more came out as supplicants to seek his mercy, Artaxerxes consigned them to the same fate. Sidon was then burnt to the ground, either by Artaxerxes or by the Sidonian citizens. Forty thousand people died in the conflagration.<ref name=Persianempire/> Artaxerxes sold the ruins at a high price to speculators, who calculated on reimbursing themselves by the treasures which they hoped to dig out from among the ashes.<ref>{{cite web|url=http://www.tomrawlinson.com/Personal/Links/RawlinsonGeorge.htm |title=Phœnicia under the Persians |accessdate=March 10, 2008 |last=Rawlinson |first=George |authorlink=George Rawlinson |year=1889 |work=History of Phoenicia |publisher=Longmans, Green |deadurl=yes |archiveurl=https://web.archive.org/web/20060720031359/http://www.tomrawlinson.com/Personal/Links/RawlinsonGeorge.htm |archivedate=July 20, 2006 |df=mdy-all }}</ref> Tennes was later put to death by Artaxerxes.<ref name=Lovetoknow>{{cite web|url=http://www.1911encyclopedia.org/Artaxerxes |title=Artaxerxes |accessdate=March 4, 2008 |archiveurl=https://web.archive.org/web/20080312114204/http://www.1911encyclopedia.org/Artaxerxes |archivedate=March 12, 2008 |deadurl=yes |df=mdy-all }}</ref> Artaxerxes later sent Jews who supported the revolt to [[Hyrcania]], on the south coast of the [[Caspian Sea]].<ref>{{cite web|url=http://www.iras.ucalgary.ca/~volk/sylvia/GogAndMagog.htm |title=The Legend Of Gog And Magog |accessdate=March 10, 2008 |archiveurl=https://web.archive.org/web/20080315084037/http://www.iras.ucalgary.ca/~volk/sylvia/GogAndMagog.htm |archivedate=March 15, 2008 |deadurl=yes |df=mdy-all }}</ref><ref>{{cite book |title=The Acts of the Apostles: The Greek Text with Introduction and Commentary |last=Bruce |first=Frederick Fyvie |year=1990 |publisher=Wm. B. Eerdmans Publishing|isbn=0-8028-0966-9 |pages=117 }}</ref>

==Second Egyptian Campaign==
{{see also|History of Achaemenid Egypt#The second Egyptian satrapy}}
[[File:NectaneboII-StatueHead MuseumOfFineArtsBoston.png|thumb|Head of [[Nectanebo II]]]]

The reduction of Sidon was followed closely by the invasion of Egypt. In 343&nbsp;BC, Artaxerxes, in addition to his 330,000 Persians, had now a force of 14,000 Greeks furnished by the Greek cities of Asia Minor: 4,000 under [[Mentor of Rhodes|Mentor]], consisting of the troops which he had brought to the aid of Tennes from Egypt; 3,000 sent by Argos; and 1000 from Thebes. He divided these troops into three bodies, and placed at the head of each a Persian and a Greek. The Greek commanders were Lacrates of Thebes, [[Mentor of Rhodes]] and Nicostratus of Argos while the Persians were led by Rhossaces, Aristazanes, and [[Bagoas]], the chief of the eunuchs. [[Nectanebo II]] resisted with an army of 100,000 of whom 20,000 were Greek mercenaries. Nectanebo II occupied the [[Nile]] and its various branches with his large navy. The character of the country, intersected by numerous canals, and full of strongly fortified towns, was in his favour and Nectanebo II might have been expected to offer a prolonged, if not even a successful, resistance.  But he lacked good generals, and over-confident in his own powers of command, he was able to be out-manoeuvred by the Greek mercenary generals and his forces eventually defeated by the combined Persian armies.<ref name="Persianempire"/>

After his defeat, Nectanebo hastily fled to [[Memphis, Egypt|Memphis]], leaving the fortified towns to be defended by their garrisons. These garrisons consisted of partly [[Greeks|Greek]] and partly Egyptian troops; between whom jealousies and suspicions were easily sown by the Persian leaders. As a result, the Persians were able to rapidly reduce numerous towns across Lower Egypt and were advancing upon Memphis when Nectanebo decided to quit the country and flee southwards to Ethiopia.<ref name="Persianempire"/> The Persian army completely routed the Egyptians and occupied the Lower Delta of the Nile. Following Nectanebo fleeing to Ethiopia, all of Egypt submitted to Artaxerxes. The Jews in Egypt were sent either to [[Babylon]] or to the south coast of the [[Caspian Sea]], the same location that the Jews of [[Phoenicia]] had earlier been sent.

After this victory over the Egyptians, Artaxerxes had the city walls destroyed, started a reign of terror, and set about looting all the temples. [[Persia]] gained a significant amount of wealth from this looting. Artaxerxes also raised high taxes and attempted to weaken [[Ancient Egypt|Egypt]] enough that it could never revolt against Persia. For the 10&nbsp;years that Persia controlled Egypt, believers in the native religion were persecuted and sacred books were stolen.<ref name="Egyptloot">{{cite web|url=http://www.mnsu.edu/emuseum/prehistory/egypt/history/periods/persianii.html |title=Persian Period II |accessdate=March 6, 2008 |archiveurl=https://web.archive.org/web/20080217023749/http://www.mnsu.edu/emuseum/prehistory/egypt/history/periods/persianii.html |archivedate=February 17, 2008 |deadurl=yes |df=mdy }}</ref> Before he returned to Persia, he appointed Pherendares as [[History of Achaemenid Egypt|satrap of Egypt]]. With the wealth gained from his reconquering Egypt, Artaxerxes was able to amply reward his mercenaries.  He then returned to his capital having successfully completed his invasion of Egypt.

==Later years==
After his success in Egypt, Artaxerxes returned to Persia and spent the next few years effectively quelling insurrections in various parts of the Empire so that a few years after his conquest of Egypt, the [[Achaemenid Empire|Persian Empire]] was firmly under his control. Egypt remained a part of the Persian Empire until [[Alexander the Great]]'s conquest of Egypt.

[[File:Achaemenid Empire-ArtaxerxesIII conquest.png|thumb|[[Achaemenid Empire|Persian Empire]] at the beginning of Artaxerxes III's rule (green), and his conquests and suppressed rebellions(Dark grey)]]

After the conquest of Egypt, there were no more revolts or rebellions against Artaxerxes. Mentor and [[Bagoas]], the two generals who had most distinguished themselves in the Egyptian campaign, were advanced to posts of the highest importance. Mentor, who was governor of the entire Asiatic seaboard, was successful in reducing to subjection many of the chiefs who during the recent troubles had rebelled against Persian rule. In the course of a few years Mentor and his forces were able to bring the whole Asian Mediterranean coast into complete submission and dependence.

Bagoas went back to the Persian capital with Artaxerxes, where he took a leading role in the internal administration of the Empire and maintained tranquility throughout the rest of the Empire. During the last six years of the reign of Artaxerxes III, the Persian Empire was governed by a vigorous and successful government.<ref name="Persianempire" />

[[File:Persepolis Artaxerxes III tomb.jpg|thumb|left|Tomb of Artaxerxes III at [[Persepolis]]]]

The Persian forces in [[Ionia]] and [[Lycia]] regained control of the [[Aegean Sea|Aegean]] and the [[Mediterranean Sea]] and took over much of [[Athens]]’ former island empire. In response, [[Isocrates]] of Athens started giving speeches calling for a ‘crusade against the barbarians’ but there was not enough strength left in any of the Greek city-states to answer his call.<ref name="conspiracy">{{cite web|url=http://www.iranologie.com/history/Achaemenid/chapter%20V.html |title=Chapter V: Temporary Relief |accessdate=March 1, 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20080619124220/http://www.iranologie.com/history/Achaemenid/chapter%20V.html |archivedate=June 19, 2008 |df=mdy-all }}</ref>

Although there weren't any rebellions in the Persian Empire itself, the growing power and territory of [[Philip II of Macedon]] in [[Macedon]] (against which [[Demosthenes]] was in vain warning the Athenians) attracted the attention of Artaxerxes. In response, he ordered that Persian influence was to be used to check and constrain the rising power and influence of the Macedonian kingdom. In 340&nbsp;BC, a Persian force was dispatched to assist the [[Thrace|Thracian prince]], [[Cersobleptes]], to maintain his independence. Sufficient effective aid was given to the city of [[Perinthus]] that the numerous and well-appointed army with which Philip had commenced his siege of the city was compelled to give up the attempt.<ref name="Persianempire"/> By the last year of Artaxerxes' rule, Philip II already had plans in place for an invasion of the Persian Empire, which would crown his career,  but the Greeks would not unite with him.<ref>{{cite web |url=http://historyofmacedonia.org/AncientMacedonia/PhilipofMacedon.html |title= Philip of Macedon Philip II of Macedon Biography |accessdate=March 7, 2008| archiveurl= https://web.archive.org/web/20080314165325/http://www.historyofmacedonia.org/AncientMacedonia/PhilipofMacedon.html| archivedate= March 14, 2008 <!--DASHBot-->| deadurl= no}}</ref>

In 338&nbsp;BC Artaxerxes was poisoned by Bagoas with the assistance of a physician.<ref>{{cite book |title=From Cyrus to Alexander: A history of the Persian Empire |last=Briant |first=Pierre |authorlink=Pierre Briant |year=2002 |publisher=Eienbrauns |isbn=1-57506-120-1 |pages=769}}</ref>

==Legacy==
{{see also|Zoroastrianism|Mithra|Anahita}}
[[File:Faravahar.svg|thumb|Historically, kings of the [[Achaemenid Empire]] were followers of [[Zoroaster]] or heavily influenced by [[Zoroastrianism|Zoroastrian]] ideology.]]

Historically, kings of the [[Achaemenid Empire]] were followers of [[Zoroaster]] or heavily influenced by Zoroastrian ideology. The reign of [[Artaxerxes II]] saw a revival of the cult of Anahita and Mithra, when in his building inscriptions he invoked [[Ahuramazda]], Anahita and Mithra and even set up statues of his gods.<ref>{{cite web|url=http://www.vohuman.org/Article/The%20Achaemenians,%20Zoroastrians%20in%20Transition.htm |title=The Achaemenians, Zoroastrians in Transition |accessdate=March 5, 2008 |author=J. Varza |author2=Dr. M. Soroushian |archiveurl=https://web.archive.org/web/20080326220621/http://www.vohuman.org/Article/The%20Achaemenians%2C%20Zoroastrians%20in%20Transition.htm |archivedate=March 26, 2008 |deadurl=yes |df=mdy-all }}</ref> Mithra and Anahita had until then been neglected by true Zoroastrians--they defied Zoroaster’s command that God was to be represented only by the flames of a sacred fire.<ref name="Lovetoknow"/><ref>{{cite web |url=http://www.livius.org/ag-ai/ahuramazda/ahuramazda.html |title=Ahuramazda and Zoroastranism |accessdate=March 5, 2008 |last=Lendering |first=Jona |authorlink=Jona Lendering| archiveurl= https://web.archive.org/web/20080315083623/http://www.livius.org/ag-ai/ahuramazda/ahuramazda.html| archivedate= March 15, 2008 <!--DASHBot-->| deadurl= no}}</ref> Artaxerxes III is thought to have rejected Anahita and worshipped only Ahuramazda and Mithra.<ref>{{cite web |url=http://www.iranica.com/newsite/articles/ot_grp10/ot_mithra_i_20060114.html |title=i. Mithra In Old Indian And Mithra In Old Iranian |accessdate=March 5, 2008 |author=Hans-Peter Schmidt |date=14 January 2006
|archiveurl = https://web.archive.org/web/20080303175737/http://www.iranica.com/newsite/articles/ot_grp10/ot_mithra_i_20060114.html |archivedate = March 3, 2008}}</ref> An ambiguity in the cuneiform script of an inscription of Artaxerxes III at [[Persepolis]] suggests that he regarded the father and the son as one person, suggesting that the attributes of Ahuramazda were being transferred to Mithra. Strangely, Artaxerxes had ordered that statues of the goddess Anâhita be erected at [[Babylon]], [[Damascus]] and [[Sardis]], as well as at [[Susa]], [[Ecbatana]] and Persepolis.<ref>{{cite web |url=http://www.sacred-texts.com/cla/mom/mom04.htm |title=The Origins Of Mithraism |accessdate=March 5, 2008| archiveurl= https://web.archive.org/web/20080207080309/http://www.sacred-texts.com/cla/mom/mom04.htm| archivedate= February 7, 2008 <!--DASHBot-->| deadurl= no}}</ref>

Artaxerxes' name appears on silver coins (modeled on Athenian ones) issued while he was in Egypt. The reverse bears an inscription in an Egyptian script, saying "Artaxerxes Pharaoh. Life, Prosperity, Wealth".<ref>{{cite web |url=https://www.britishmuseum.org/explore/highlights/highlight_objects/cm/s/silver_tetradrachm_of_artaxerx.aspx  |title=
Silver tetradrachm of Artaxerxes III |accessdate=March 6, 2008}}</ref>

===In literature===
It is thought by some that the ''[[Book of Judith]]'' could have been originally based on Artaxerxes' campaign in [[Phoenicia]], as [[Holofernes]] was the name of the brother of the [[Cappadocia (satrapy)|Cappadocian satrap]] Ariarathes, the vassal of Artaxerxes. Bagoas, the general that finds Holofernes dead, was one of the generals of Artaxerxes during his campaign against Phoenicia and Egypt.<ref>{{cite web |url=http://www.infidels.org/library/modern/gerald_larue/otll/chap29.html |title=The Period of Jewish Independence |accessdate=March 10, 2008 |last=Lare |first=Gerald A. | archiveurl= https://web.archive.org/web/20080225022008/http://www.infidels.org/library/modern/gerald_larue/otll/chap29.html| archivedate= February 25, 2008 <!--DASHBot-->| deadurl= no}}</ref><ref>{{cite web |url=http://www.katapi.org.uk/OTApoc/Judith.htm |title=The Book of Judith |accessdate=March 10, 2008 |author=Paul Ingram }}</ref>

===Construction===
[[File:Persepolis Taureaux.jpg|thumb|right|The Unfinished Gate at [[Persepolis]] gave archaeologists an insight into the construction of Persepolis.]]
{{see also|Persepolis}}

There is evidence for a renewed building policy at Persepolis, but some of the buildings were unfinished at the time of his death. Two of his buildings at Persepolis were the Hall of Thirty-Two Columns, the purpose of which is unknown, and the palace of Artaxerxes III. The unfinished Army Road and Unfinished Gate, which connected the Gate of All Nations and the One-hundred Column Hall, gave archaeologists an insight into the construction of Persepolis.<ref name="Ochus2"/> In 341&nbsp;BC, after Artaxerxes returned to [[Babylon]] from Egypt, he apparently proceeded to build a great Apadana whose description is present in the works of [[Diodorus Siculus]].

The [[Nebuchadnezzar II]] palace in Babylon was expanded during the reign of Artaxerxes III.<ref name=Babylonpalace>{{cite book |title=The Illustrated Dictionary & Concordance of the Bible |last=Wigoder |first=Geoffrey |year=2006 |publisher=Sterling Publishing Company |isbn=1-4027-2820-4 |pages=131 }}</ref>  Artaxerxes' tomb was cut into the mountain behind the Persepolis platform, next to his [[Artaxerxes II|father's]] tomb.

==Family==
{{further|Achaemenid family tree}}
Artaxerxes III was the son of [[Artaxerxes II]] and Statira. Artaxerxes II had more than 115 sons by many wives, most of them however were illegitimate. Some of Ochus' more significant siblings were Rodogune, Apama, Sisygambis, Ocha, Darius and Ariaspes, most of whom were murdered soon after his ascension.<ref name="conspiracy"/> Artaxerxes married his niece and the daughter of Oxathres, brother of the future king [[Darius III]].<ref>{{cite book |title=Women in Ancient Persia, 559–331 BC |last=Brosius |first=Maria |year=1996 |publisher=Oxford University Press |isbn=0-19-815255-8 |pages=67 }}</ref> His children were [[Arses of Persia|Arses]], the future king of Persia, Bisthanes and [[Parysatis II]], wife of [[Alexander the Great]].<ref name=Ochus2/>

==See also==
*[[List of kings of Persia]]
*[[Achaemenid Empire]]
*[[Achaemenid Empire|Persian Empire]]
*[[History of Achaemenid Egypt]]
*[[List of Pharaohs#Thirty-first Dynasty|List of Pharaohs]]
*[[Second Persian conquest of Egypt]]
*[[History of Achaemenid Egypt#The second Egyptian satrapy|History of Achaemenid Egypt]]

==References==
{{Reflist|2}}

==External links==
{{Commons category|Artaxerxes III}}{{Wikibooks|Iranian History/Contents/The Era of the Three Dariuses}}{{Wikisource|Ancient Egypt (Rawlinson)/The Light Goes Out in Darkness}}
*[[Wikibooks:Iranian History/Contents/The Era of the Three Dariuses#Artaxerxes I 465 - 424 BC|Wikibooks "Era of three Dariuses"]]{{dead|date=March 2017}}
*[https://web.archive.org/web/20080619124220/http://www.iranologie.com/history/Achaemenid/chapter%20V.html Achaemenid Empire:Temporary Relief]
*[https://web.archive.org/web/20160303193045/http://persianempire.info/ArtaxerxesIII.htm Persianempire.info]

{{s-start}}
{{s-hou|[[Achaemenid dynasty]]||''Ca.'' 425 BC||338 BC}}
{{s-bef|rows=1|before=[[Artaxerxes II of Persia|Artaxerxes II]]}}
{{s-ttl|title=[[List of kings of Persia|Great King (Shah) of Persia]]|years=358 BC – 338 BC}}
{{s-aft|rows=2|after=[[Arses of Persia|Artaxerxes IV Arses]]}}
{{s-bef|before=[[Nectanebo II]]}}
{{s-ttl|title=[[List of Pharaohs|Pharaoh of Egypt]]|years=343 BC – 338 BC|dynasty=XXXI Dynasty}}
{{end}}

{{Median and Achaemenid kings}}
{{Pharaohs}}

{{Authority control}}

{{DEFAULTSORT:Artaxerxes 03 Of Persia}}
[[Category:Achaemenid kings]]
[[Category:Monarchs of Persia]]
[[Category:Pharaohs of the Achaemenid dynasty of Egypt]]
[[Category:Thirty-first Dynasty of Egypt]]
[[Category:420s BC births]]
[[Category:338 BC deaths]]
[[Category:Deaths by poisoning]]
[[Category:Murdered Persian monarchs]]
[[Category:4th-century BC crimes]]
[[Category:Achaemenid Egypt]]
[[Category:4th century BC in Egypt]]
[[Category:4th-century BC Iranian people]]
[[Category:4th-century BC rulers]]