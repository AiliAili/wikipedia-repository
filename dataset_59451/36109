{{good article}}
{{Use mdy dates|date=May 2016}}
{{Infobox album <!-- See Wikipedia:WikiProject Albums -->
| Name        = All I Want Is You
| Type        = studio
| Artist      = [[Miguel (singer)|Miguel]]
| Cover       = Miguel All-I-Want-Is-You.jpg
| Alt         = An off-center image of the artist wearing oversized red sunglasses and a leather jacket whose outsized popped collar brushes the tips of his ears; the foreground bears his name and the album title, while the background depicts three layers of color: blue, white, and red
| Border      = yes
| Released    = November 30, 2010
| Recorded    = <!-- per [[WP:V]], all content must be attributed to a reliable source; otherwise, it is challengeable/removable -->
| Genre       = [[Contemporary R&B|R&B]], [[hip hop music|hip hop]], [[neo soul]]
| Length      = 42:47
| Label       = [[ByStorm Entertainment|ByStorm]], [[Jive Records|Jive]]
| Producer    = [[Dre & Vidal]], Fisticuffs, Happy Perez, [[Salaam Remi]], State of Emergency
| This album  = '''''All I Want Is You'''''<br />(2010)
| Next album  = ''[[Kaleidoscope Dream]]''<br />(2012)
| Misc = {{Singles
  | Name           = All I Want Is You
  | Type           = studio
  | Single 1       = [[All I Want Is You (Miguel song)|All I Want Is You]]
  | Single 1 date  = May 25, 2010
  | Single 2       = [[Sure Thing (song)|Sure Thing]]
  | Single 2 date  = January 17, 2011
  | Single 3       = [[Quickie (song)|Quickie]]
  | Single 3 date  = August 2, 2011
  | Single 4       = Girls Like You
  | Single 4 date  = November 8, 2011
}}
}}
'''''All I Want Is You''''' is the debut [[studio album]] by American [[Contemporary R&B|R&B]] recording artist [[Miguel (singer)|Miguel]]. After signing a record deal with the [[Jive Records]] label [[ByStorm Entertainment]] in 2007, Miguel recorded the album with producers [[Dre & Vidal]], Fisticuffs, Happy Perez, State of Emergency, and [[Salaam Remi]]. It was shelved by Jive for two years after legal issues with Miguel's former production company. 

''All I Want Is You'' was eventually released on November 30, 2010, selling poorly at first. With the help of its subsequent singles, the album became [[sleeper hit]] on the [[Billboard 200|''Billboard'' 200]] and sold 404,000 copies by September 2012. It also received positive reviews; critics found some of the music inconsistent but praised Miguel's singing/songwriting abilities while drawing comparisons to American recording artist [[Prince (musician)|Prince]]. Miguel toured in promotion of the album as a supporting act for the singers [[Usher (entertainer)|Usher]] and [[Trey Songz]].

== Background and music ==
In 2007, Miguel was signed [[Jive Records]]-imprint label [[ByStorm Entertainment]].<ref name="Lipshutz">{{cite journal|last=Lipshutz|first=Jason|date=September 21, 2012|url=http://www.billboard.com/articles/news/474985/miguels-kaleidoscope-dream-inside-the-rb-dynamos-fresh-start|title=Miguel's 'Kaleidoscope Dream': Inside The R&B Dynamo's Fresh Start|journal=[[Billboard (magazine)|Billboard]]|accessdate=October 20, 2012}}</ref> He subsequently recorded ''All I Want Is You'', but legal issues with his former production company prevented the album from being released for two years.<ref name="Lipshutz"/> Miguel continued working with various underground acts and writing songs for mainstream recording artists, including [[Johnson&Jonson]], [[Asher Roth]], [[Jaheim]], and [[Usher (entertainer)|Usher]].<ref name="Jeffries">{{cite web|last=Jeffries|first=David|url=http://www.allmusic.com/artist/miguel-mn0002570457|title=Miguel|publisher=[[Allmusic]]. [[Rovi Corporation]]|accessdate=October 8, 2012}}</ref> Recording sessions for the album took place at Black Mango Studios in Van Nuys, California, Germano Studios in New York City, Glenwood Studios and Instrument Zoo in Miami, Florida, Studio 609 in Philadelphia, Pennsylvania, and The Gym in Los Angeles, California.<ref>[http://www.cduniverse.com/productinfo.asp?pid=8356801 Miguel - All I Want Is You CD Album]. [[Muze]]. [[CD Universe]]. Retrieved on June 8, 2011.</ref>

According to Jason Newman from ''[[MTV Buzzworthy]]'', ''All I Want Is You'' is "a diverse album rooted in R&B and hip-hop, thoughtfully laced with elements of classic rock, funk and electro".<ref>{{cite web|last=Newman|first=Jason|url=http://buzzworthy.mtv.com/2011/05/16/mtv-push-miguel/|title=MTV PUSH Artist Of The Week: Miguel|work=MTV Buzzworthy Blog|publisher=[[MTV Networks]]|date=May 16, 2011|accessdate=November 9, 2011}}</ref> Marc Hogan from ''[[Spin (magazine)|Spin]]'' said it featured [[neo soul]] music,<ref>{{cite journal|last=Hogan|first=Marc|date=July 26, 2012|url=http://www.spin.com/#articles/stream-miguels-three-song-kaleidoscope-dream-water-preview|title=Stream Miguel's Three-Song 'Kaleidoscope Dream: Water Preview'|journal=[[Spin (magazine)|Spin]]|location=New York|accessdate=October 22, 2012}}</ref> while [[AllMusic]] critic David Jeffries described it as a "slick" and "sexy" synthesis of influences from [[Prince (musician)|Prince]], [[Kanye West]], and electro.<ref name="Jeffries2">{{cite web|last=Jeffries|first=David|url=http://www.allmusic.com/album/all-i-want-is-you-mw0002066164|title=All I Want Is You - Miguel|publisher=[[Allmusic]]. [[Rovi Corporation]]|accessdate=August 8, 2012}}</ref> "Girl Like You" and "Hard Way" feature aggressive hip hop beats, while "Pay Me" and "To the Moon" explore European [[electronic music]] and [[Electronic dance music|EDM]], respectively.<ref name="Nero"/> The album's first half features two romantic songs, an interlude, a song about a prostitute, and another about a [[Quickie (sex)|quickie]]. The closing track "My Piece" uses a "piece"-"peace" [[homonym]].<ref name="Christgau"/> Jeffries said the album featured Miguel's "sly sense of humor".<ref name="Jeffries2"/> On "Sure Thing", Miguel sang about loyalty in a passionate [[committed relationship]].<ref name="Nero"/>

== Release and reception ==
{{quote box|quote="Though its poor initial showing on the charts seemed to affirm [Jive]'s lack of faith in it, the record gradually discovered an audience over the next year thanks to a trickle of ingratiating singles that established Miguel as one of radio’s rarest commodities: a new R&B star. With its splatters of off-kilter funk and mesmeric electro, ''All I Want Is You'' teased a unique vision without coloring too far outside the boundaries of popular R&B."|source=—Evan Rytlewski, ''[[The A.V. Club]]''<ref name="Rytlewski">{{cite news|last=Rytlewski|first=Evan|date=October 9, 2012|url=http://www.avclub.com/articles/miguel-kaleidoscope-dream,86389/|title=Miguel: Kaleidoscope Dream|journal=[[The A.V. Club]]|location=Chicago|accessdate=October 19, 2012}}</ref>|width=20%|align=left|style=padding:8px;|border=2px}}

''All I Want Is You'' was released by Bystorm Entertainment and Jive,<ref name="JivePR">[[RCA/Jive Label Group|Jive Label Group]] (May 9, 2011). [http://assets.sonymusic.com/zomba/jivepress/files/1305586680_sure_thing_no.1_song_on_r_b_singles_chart.pdf Singer/Songwriter Miguel's Hit Single "Sure Thing" No. 1 on the R&B Singles Chart]. Press release. Retrieved on July 22, 2011.</ref> on November 30, 2010.<ref>[http://www.rap-up.com/2010/11/08/album-cover-tracklisting-miguel-all-i-want-is-you/ Album Cover + Tracklisting: Miguel&nbsp;– ‘All I Want Is You’]. ''[[Rap-Up]]''. November 8, 2010. Retrieved on July 22, 2011.</ref> It sold poorly at first;<ref name="Rytlewski"/> in its first week, the album charted at number 109 on the [[Billboard 200|''Billboard'' 200]] and sold 11,000 copies in the United States.<ref>Concepcion, Mariel (December 8, 2010). [http://www.billboard.com/articles/columns/the-juice/949538/soulja-boy-flo-rida-flop-on-billboard-album-chart Soulja Boy, Flo Rida Flop On Billboard Album Chart]. ''Billboard''. Retrieved on July 22, 2011.</ref> After falling off the chart for three weeks, it re-entered and climbed the chart for 22 weeks, before peaking at number 37 on May 14, 2011.<ref>{{cite web|last=Ramirez|first=Erika|url=http://www.billboard.com/articles/columns/the-juice/470613/chart-juice-miguel-describes-his-sound-announces-next-singles|title=Chart Juice: Miguel Describes His Sound & Announces Next Singles|work=[[Billboard (magazine)|Billboard]]|publisher=[[Nielsen Company|Nielsen Business Media]]|date=June 10, 2011|accessdate=November 9, 2011}}</ref> The album became a [[sleeper hit]],<ref>{{cite web|date=March 24, 2011|url=http://soultrain.com/2011/03/24/qa-miguel/|title=Q&A: Miguel|publisher=[[Soul Train]]|accessdate=October 17, 2012}}</ref> and by September 2012, it had sold 404,000 copies, according to [[Nielsen SoundScan]].<ref name="Lipshutz"/>

According to Miguel, Jive marketed him as a "typical R&B artist" during their promotion of ''All I Want Is You'': "That album was a huge learning experience. I left the marketing of my album and me as an artist up to the discretion of the label ... I can't really blame them for [that], because that's what they know. But that's not what my lifestyle was."<ref name="Lipshutz"/> Four singles were released from the album: the title track "[[All I Want Is You (Miguel song)|All I Want Is You]]",<ref>[http://www.amazon.com/All-I-Want-Is-You/dp/B003JR09LG/ref=sr_shvl_album_3?ie=UTF8&qid=1311360371&sr=301-3 All I Want Is You: Miguel featuring J.Cole: MP3 Downloads]. [[Amazon.com]]. Retrieved on July 22, 2011.</ref> "[[Sure Thing (song)|Sure Thing]]",<ref name="JivePR"/> "[[Quickie (song)|Quickie]]", and "Girls Like You".<ref>Muhammad, Latifah (July 1, 2011). [http://www.theboombox.com/2011/07/01/miguel-to-release-two-new-singles-simultaneously/ Miguel to Release Two New Singles Simultaneously]. [[AOL|TheBoombox]]. Retrieved on July 22, 2011.</ref> As the title track gradually received [[radio airplay]], Miguel began touring as a supporting act for Usher and [[Trey Songz]].<ref name="Lipshutz"/> By May 2011, "All I Want Is You" and "Sure Thing" had reached a combined digital/mobile sales of over 825,000 units.<ref name="JivePR"/>

In a review for [[About.com]], Mark Edward Nero gave ''All I Want Is You'' three-and-a-half out of five stars and said Miguel "somehow managed to vocally glide across each" of the different styles he explored in music that was nonetheless rooted in R&B. He deemed the record imperfect but devoid of a poor song.<ref name="Nero">Nero, Mark Edward (November 30, 2010). [https://web.archive.org/web/20111020124742/http://randb.about.com/od/reviews/fr/Miguel-All-I-Want-Is-You.htm Miguel - All I Want Is You]. [[About.com]]. Retrieved on March 17, 2011.</ref> B. Wright from ''[[Vibe (magazine)|Vibe]]'' found the music inconsistent and "schizophrenic" but praised Miguel's singing and songwriting abilities while determining the record was "worth the purchase price".<ref name="Wright">Wright, B. (December 7, 2010). [http://www.vibe.com/posts/review-michael-jacksons-michael-miguels-all-i-want-you Review: Miguel's 'All I Want Is You']. ''[[Vibe (magazine)|Vibe]]''. Retrieved on March 17, 2011.</ref> In a three-and-a-half star review, ''[[Slant Magazine]]''{{'}}s Matthew Cole said the second half of songs was "less stimulating" on an album that "blends slick, radio-friendly R&B with Prince-aping theatrics, both refracted around a sense of humor that, surreal and sexually unsubtle, would have to make [Prince] proud". Cole also compared Miguel to singer [[Kelis]], "whose work has an undeniably commercial cunning to it, but who never fails to imbue her pop confections with real personality".<ref name="Cole">Cole, Matthew (December 3, 2010). [http://www.slantmagazine.com/music/review/miguel-all-i-want-is-you/2335 Miguel: All I Want Is You | Music Review]. ''[[Slant Magazine]]''. Retrieved on March 17, 2011.</ref>

Reviewing ''All I Want Is You'' for ''[[MSN Music]]'' in 2012, Christgau gave it a "B+" and believed Miguel "front-loaded his Prince-channeling debut" with "five [[hook (music)|hooky]] tracks" that were "followed by six pleasant tracks and capped by two hooky novelties". He viewed the song "Teach Me" as "a treasure hidden in the middle" and "[[wikt:supplicant#Noun|supplicant]]'s" song, "unprecedented" in "a genre that makes its nut promising untold pleasures". He credited Miguel for "laying out the truth that, as [[Norman Mailer]] put it in one of the few useful sex tips in his orgasm-mad canon, 'the man as lover is dependent upon the bounty of the woman.' Who knows what pleases her? She does, she alone, and Miguel craves to be let in on that shifting and enthralling secret." He nonetheless critiqued that the song lacked a first-rate hook that would have made it an R&B classic in the vein of [[Marvin Gaye]]'s "[[Sexual Healing]]" and "[[Use Me (Bill Withers song)|Use Me]]" by [[Bill Withers]].<ref name="Christgau">{{cite web|last=Christgau |first=Robert |authorlink=Robert Christgau |date=December 28, 2012 |url=http://social.entertainment.msn.com/music/blogs/expert-witness-blogpost.aspx?post=4ae4a71f-e229-44a9-ae0d-5f3bf53cc93b |title=Miguel |publisher=[[MSN Music]]. [[Microsoft]] |accessdate=December 28, 2012 |archiveurl=http://www.webcitation.org/6DFDgjiLr?url=http%3A%2F%2Fsocial.entertainment.msn.com%2Fmusic%2Fblogs%2Fexpert-witness-blogpost.aspx%3Fpost%3D4ae4a71f-e229-44a9-ae0d-5f3bf53cc93b |archivedate=December 28, 2012 |deadurl=no |df=mdy }}</ref>

== Track listing ==
Credits are adapted from the album's liner notes.<ref>{{cite AV media notes|author=Anon.|year=2010|title=All I Want Is You|type=CD liner notes|others=[[Miguel (singer)|Miguel]]|publisher=[[Jive Records]], [[ByStorm Entertainment]]|id=88697-75487-2}}</ref>
{{Track listing
| writing_credits = yes
| extra_column    = Producer(s)
| title1          = [[Sure Thing (song)|Sure Thing]]
| length1         = 3:14
| writer1         = [[Miguel (singer)|Miguel Pimentel]], Jawan Pimentel
| extra1          = Happy Perez
| title2          = [[All I Want Is You (Miguel song)|All I Want Is You]]
| note2           = featuring [[J. Cole]]
| length2         = 4:55
| writer2         = [[Salaam Remi]], Miguel Pimentel, [[J. Cole|Jermaine Cole]]  
| extra2          = Salaam Remi
| title3          = Girl with the Tattoo (Enter.Lewd)
| writer3         = Miguel Pimentel
| length3         = 1:42
| extra3          = 
| title4          = Pay Me
| writer4         = Miguel Pimentel, Mac Robinson, Brian Warfield
| length4         = 2:57
| extra4          = Fisticuffs
| title5          = [[Quickie (song)|Quickie]]
| length5         = 3:46
| writer5         = Miguel Pimentel, Mac Robinson, Brian Warfield
| extra5          = Fisticuffs
| title6          = [[All I Want Is You (Miguel song)|Girls Like You]]
| length6         = 3:23
| writer6         = Miguel Pimentel, Mac Robinson, Brian Warfield
| extra6          = Fisticuffs
| title7          = Overload (Enter.Lewd)
| length7         = 0:31
| writer7         = Miguel Pimentel 
| extra7          =
| title8          = Hard Way
| length8         = 3:49
| writer8         = Salaam Remi, Miguel Pimentel
| extra8          = Salaam Remi
| title9          = Teach Me
| length9         = 5:22
| writer9         = Miguel Pimentel, [[Darnley Scantlebury]]
| extra9          = State Of Emergency
| title10         = Hero
| length10        = 3:47
| writer10        = Miguel Pimentel, [[Andre Harris]], [[Vidal Davis]]
| extra10         = [[Dre & Vidal]]
| title11         = Vixen
| length11        = 3:01
| writer11        = Miguel Pimentel, Mac Robinson, Brian Warfield
| extra11         = Fisticuffs
| title12         = To the Moon
| length12        = 3:23
| writer12        = Miguel Pimentel, Harry Zelnick, Alex Chiegger, Andre Harris, Vidal Davis
| extra12         = Dre & Vidal
| title13         = My Piece
| length13        = 2:55
| writer13        = Miguel Pimentel, Nathan Perez
| extra13         = Happy Perez
}}

== Personnel ==
{{col-start}}
{{col-2}}
* Raymond Angry&nbsp;– keyboards, strings
* Wayne Barrow&nbsp;– executive producer
* Anita Marisa Boriboon&nbsp;– art direction, design
* Alex Chiegger&nbsp;– instrumentation
* Tom Coyne&nbsp;– mastering
* Kumi Craig&nbsp;– grooming
* Vidal Davis&nbsp;– instrumentation, mixing, producer
* Daniel DeLuna&nbsp;– 12-string bass guitar
* Gleyder "Gee" Disla&nbsp;– engineer
* Vince Dlorinzo&nbsp;– engineer
* Ryan Evans&nbsp;– engineer
* Andre Harris&nbsp;– instrumentation, producer
* Kasey Phillips&nbsp;– engineer
* Erik Madrid&nbsp;– assistant
* Manny Marroquin&nbsp;– mixing
{{col-2}}
* Miguel&nbsp;– engineer, vocals
* Happy Perez&nbsp;– drums, keyboards, producer, programming
* Miguel Pimentel&nbsp;– executive producer
* Mark Pitts&nbsp;– A&R, executive producer
* Christian Plata&nbsp;– assistant
* Kai Regan&nbsp;– photography
* Salaam Remi&nbsp;– bass, drums, guitar
* Anthony Saleh&nbsp;– production coordination
* Wendell "Pops" Sewell&nbsp;– guitar
* State of Emergency&nbsp;– producer
* Steve Bruner&nbsp;– bass
* Tim Stewart&nbsp;– guitar
* Jahi Sundance&nbsp;– programming
* Mike Tsarfati&nbsp;– engineer
* Harry Zelnick&nbsp;– instrumentation
{{col-end}}

== Charts ==
{|class="wikitable sortable"
|-
!Chart (2011)
!Peak<br />position
|-
|U.S. [[Billboard 200|''Billboard'' 200]]<ref>[{{BillboardURLbyName|artist=miguel|chart=Billboard 200}} Miguel Album & Song Chart History&nbsp;– Billboard 200]. ''Billboard''. Retrieved on July 22, 2011.</ref>
| style="text-align:center;"|37
|-
|U.S. [[Top R&B/Hip-Hop Albums]]<ref>[{{BillboardURLbyName|artist=miguel|chart=R&B/Hip-Hop Albums B}} Miguel Album & Song Chart History&nbsp;– R&B/Hip-Hop Albums]. ''Billboard''. Retrieved on July 22, 2011.</ref>
| style="text-align:center;"|9
|}

== References ==
{{reflist|2}}

== External links ==
* {{Official website|http://www.officialmiguel.com/}}

{{Miguel}}

[[Category:2010 debut albums]]
[[Category:Albums produced by Dre & Vidal]]
[[Category:Albums produced by Happy Perez]]
[[Category:Albums produced by Salaam Remi]]
[[Category:English-language albums]]
[[Category:Jive Records albums]]
[[Category:Miguel (singer) albums]]