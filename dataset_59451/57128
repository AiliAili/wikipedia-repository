{{for|GEOREF|World Geographic Reference System}}
{{ infobox bibliographic database
| title       = GeoRef
| image       = 
| caption     = 
| producer    = [[American Geosciences Institute]]
| country     = United States
| history     = 1966 to present
| languages   = 
| providers   = 
| cost        = Subscription
| disciplines = [[geoscience]]s
| depth       = Index & abstract 
| formats     = articles, books, maps, conference papers, reports and theses
| temporal    = 1669 to present
| geospatial  = Worldwide
| number      = 2.8 million
| updates     = 
| p_title     = 
| p_dates     = 
| ISSN        = 
| web         = http://www.americangeosciences.org/georef/georef-information-services
| titles      =  
}}

The '''GeoRef''' database is a [[bibliographic database]] of scientific literature in the [[geoscience]]s, including [[geology]]. Coverage ranges from 1669 to the present for North American literature, and 1933 to the present for the rest of the world. It currently contains more than 2.8 million references. It is widely considered one of the preeminent literature databases for those studying the earth sciences.<ref>[http://www.agiweb.org/georef/about/index.html About the GeoRef database], from the [[American Geosciences Institute]].</ref>

It is produced by the [[American Geosciences Institute]], known as the American Geological Institute until October 2011.

"To maintain the database, GeoRef editor/indexers regularly scan more than 3,500 journals in 40 languages as well as new books, maps, and reports. They record the bibliographic data for each document and assign index terms to describe it. Each month between 6,000 and 9,000 new references are added to the database."<ref>[http://www.proquest.com/en-US/catalogs/databases/detail/georef-set-c.shtml "GeoRef"] ProQuest Products Page.</ref>

Major areas of coverage by GeoRef include: 
•[[Areal geology]]
•[[Economic geology]]
•[[Engineering geology]]
•[[Environmental geology]]
•[[Extraterrestrial geology]]
•[[Geochemistry]]
•[[Geochronology]]
•[[Geophysics]]
•[[Hydrogeology]] and [[hydrology]]
•[[Marine geology]] and [[oceanography]]
•[[Mathematical geology]]
•[[Mineralogy]] and [[Crystallography]]
•[[Paleontology]]
•[[Petrology]]
•[[Seismology]]
•[[Stratigraphy]]
•[[Structural geology]]
•[[Surficial geology]]<ref>[http://www.proquest.com/en-US/catalogs/databases/detail/georef-set-c.shtml "GeoRef"] ProQuest Products Page.</ref>

Print publications that correspond to GeoRef are Bibliography and Index of North American Geology; Bibliography of Theses in Geology; and the Geophysical Abstracts, Bibliography and Index of Geology Exclusive of North America.<ref>Dialog. Blue Sheets. "[http://library.dialog.com/bluesheets/html/bl0089.html GeoRef (89)]". Last Update To Bluesheet: December 15, 2011. Accessdate 2012-09-10</ref>

==References==
{{reflist}}

==Bibliography==
{{Refbegin}}
*{{cite journal|last=Bichteler|first=Julie|title=Geologists and Gray Literature|journal=Science & Technology Libraries|date=1991|volume=11|issue=3|pages=39–50|doi=10.1300/J122v11n03_04}}
*{{cite journal|last=Masahiro|first=Ichiki|author2=Modeki Riko |author3=Nakamura Yasuyuki |author4=Shibao Mikio |title=Leading Edge of Earth Science Technology. An Introduction of the Comprehensive Earth Science Data Base, GeoRef.|journal=Journal of Geography|date=2000|volume=109|issue=6|pages=993&ndash;1005 |doi=10.5026/jgeography.109.6_993}}
{{Refend}}

==External links==
*{{Official website|http://www.agiweb.org/georef/}} of the GeoRef database
*{{Official website|http://www.agiweb.org/}} of the American Geosciences Institute

[[Category:Earth sciences]]
[[Category:Bibliographic databases and indexes]]
[[Category:Scientific databases]]
[[Category:Geographical databases]]


{{geology-stub}}
{{library-stub}}