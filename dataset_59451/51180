{{Italic title}}
'''''Eighteenth-Century Ireland''''' or '''''Iris an dá chultúr''''', is an annual, [[peer-reviewed]] [[academic journal]] of [[History of Ireland (1691–1801)|eighteenth century Ireland]] published on behalf of the [[Eighteenth-Century Ireland Society]].<ref name=soc>[http://www.ecis.ie/journal/ Journal.] Eighteenth-Century Ireland Society. Retrieved 20 May 2015.</ref> The journal was established in 1986.<ref>[https://dbh.nsd.uib.no/publiseringskanaler/erihplus/periodical/info.action?id=478750 Eighteenth-Century Ireland.] ERIH PLUS. Retrieved 20 May 2015.</ref> Articles are in English, Irish, or French.

==Indexing and abstracting==
The journal is indexed in the [[European Reference Index for the Humanities]] (ERIH) where it has a rating of INT1. Abstracts of volumes 1-13 are additionally available on the Eighteenth-Century Ireland Society website.<ref>[http://www.ecis.ie/journal/abstracts/ Abstracts.] Eighteenth-Century Ireland Society. Retrieved 20 May 2015.</ref> The journal forms part of the [[JSTOR]] Ireland Collection.<ref>[http://www.jstor.org/action/showPublication?journalCode=eighcentirel Eighteenth-Century Ireland / Iris an dá chultúr.] JSTOR. Retrieved 20 May 2015.</ref>

==Reception==
In reviewing the first edition in ''The Linen Hall Review'' in 1986, John C. Greene and Michelle O'Riordan argued that the journal came at a time when modern research was "leading to the history of eighteenth-century Ireland being rewritten on almost every front".<ref>[http://www.jstor.org/stable/20533865 "Literary History"], John C. Greene & Michelle O'Riordan, ''The Linen Hall Review'', Vol. 3, No. 4 (Winter, 1986), pp. 36-37.</ref> Toby Barnard, in the ''[[Irish University Review]]'' also noted the timeliness of publication, seeing it as "a fresh sign of rising interest in that century".<ref>[http://www.jstor.org/stable/25477694 "Books Reviews"], Toby Barnard, ''Irish University Review'', Vol. 17, No. 2 (Autumn, 1987), pp. 325-326.</ref> A.P.W. Malcomson, in ''[[Irish Historical Studies]]'', described ''Eighteenth-Century Ireland'' as "filling an obvious void".<ref>[http://www.jstor.org/stable/30008573 "Reviews and short notices"], A.P.W. Malcomson, ''Irish Historical Studies'', Vol. 25, No. 100 (Nov., 1987), pp. 447-448.</ref>

==Later volumes==
Reviewing volumes four and five in ''Irish Historical Studies'', Thomas Bartlett described ''Eighteenth-Century Ireland'' as "highly regarded" but noted its uneven quality and persistent problems with proof reading.<ref>[http://www.jstor.org/stable/30008011 "Reviews and short notices"], Thomas Bartlett, ''Irish Historical Studies'', Vol. 28, No. 109 (May, 1992), pp. 101-102.</ref> The journal was also reviewed in Irish language publications such as ''[[Comhar]]''.<ref>[http://www.jstor.org/stable/25572206 "Review"], Cormac Ó Gráda, ''Comhar'', Iml. 52, Uimh 9 (Sep., 1993), p. 27.</ref>

==References==
{{Reflist|30em}}

== External links ==
* {{Official website|http://www.ecis.ie/journal}}
* Print: {{ISSN|0790-7915}}

[[Category:English-language journals]]
[[Category:Annual journals]]
[[Category:History journals]]
[[Category:18th century in Ireland]]
[[Category:Multilingual journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Historiography of Ireland]]