{{Infobox journal
| title = Geoscientific Model Development
| cover = [[File:gmd cover.jpg|150px]]
| abbreviation = Geosci. Model Dev.
| discipline = [[Geosciences]]
| editor = Julia Hargreaves, Astrid Kerkweg, Robert Marsh, Andy Ridgwell, Didier Roche, Rolf Sander
| publisher = [[Copernicus Publications]] on behalf of the [[European Geosciences Union]]
| country =
| frequency = Bimonthly
| history = 2008-present
| impact = 3.549
| impact-year = 2015
| openaccess = Yes
| license = [[Creative Commons License]]
| website = http://geoscientific-model-development.net/
| link1 = http://www.geosci-model-dev.net/recent_papers.html
| link1-name = Online access
| link2 = http://www.geosci-model-dev.net/volumes_and_issues.html
| link2-name = Online archive
| ISSN = 1991-959X
| eISSN = 1991-9603
| OCLC = 309012016
| LCCN = 2009205845
}}
'''''Geoscientific Model Development''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] published by [[Copernicus Publications]] on behalf of the [[European Geosciences Union]]. It covers the description, development, and evaluation of numerical models of the Earth system and its components.

The journal has a two-stage publication process. In the first stage, papers that pass a rapid access peer-review are immediately published on the ''Geoscientific Model Development Discussions'' website. They are then subject to interactive public discussion, during which the referees' comments (anonymous or attributed), additional short comments by other members of the scientific community (attributed), and the authors' replies are published. In the second stage, the peer-review process is completed and, if accepted, the final revised papers are published in ''Geoscientific Model Development''.

== Abstracting and indexing ==
The journal is abstracted and indexed in the [[Science Citation Index Expanded]], [[Scopus]], [[Astrophysics Data System]], and [[Current Contents]]/Physical, Chemical & Earth Sciences. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.654.<ref name=WoS>{{cite book |year=2015 |chapter=Geoscientific Model Development |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://geoscientific-model-development.net/}}

[[Category:Earth and atmospheric sciences journals]]
[[Category:European Geosciences Union academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 2008]]
[[Category:English-language journals]]
[[Category:Copernicus Publications academic journals]]