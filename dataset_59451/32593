{{featured article}}

{{Use dmy dates|date=October 2013}}
{|{{Infobox ship begin |infobox caption=}} <!-- submarines --><!-- caption: yes, nodab, or <caption text> -->
|+Yugoslav submarine ''Nebojša''
{{Infobox ship image
|Ship image= [[File:Yugoslav submarine Hrabri.jpg|330px|alt=a black and white photograph of a submarine underway on the surface]]
|Ship caption= ''Nebojša'''s sister submarine ''Hrabri'' underway in 1934
}}
{{Infobox ship career
|Hide header=
|Ship country=Kingdom of Yugoslavia
|Ship flag={{shipboxflag|Kingdom of Yugoslavia|naval}}
|Ship name=''Nebojša''
|Ship namesake= Fearless
|Ship renamed=
|Ship ordered=
|Ship builder=[[Vickers-Armstrongs|Vickers-Armstrong Naval Yard]], [[River Tyne]], [[United Kingdom]]
|Ship laid down=
|Ship launched=1927
|Ship acquired=
|Ship commissioned=
|Ship decommissioned=
|Ship in service=1927–45
|Ship out of service=1945
|Ship struck=
|Ship reinstated=
|Ship fate=
|Ship status=
}}
{{Infobox ship career
|Hide header=title
|Ship country=[[Socialist Federal Republic of Yugoslavia]]
|Ship flag={{shipboxflag|Socialist Federal Republic of Yugoslavia|naval}}
|Ship name=''Tara''
|Ship acquired=1945
|Ship out of service=1954
|Ship struck=
|Ship reinstated=
|Ship honours=
|Ship fate=Scrapped in 1958
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class= [[Hrabri-class submarine|''Hrabri''-class]] [[diesel-electric]] [[submarine]]
|Ship displacement=*{{convert|975|LT}} (surfaced)
*{{convert|1164|LT}} (submerged)
|Ship length={{convert|72.05|m|ftin|abbr=on}}
|Ship beam={{convert|7.32|m|ft|abbr=on|sigfig=2}}
|Ship draught={{convert|3.96|m|ft|abbr=on|sigfig=2}}
|Ship propulsion=*2 × shafts
*2 × diesel engines {{convert|2400|bhp|lk=in|abbr=on}}
*2 × electric motors {{convert|1600|shp|lk=in|abbr=on}}

|Ship speed=*{{convert|15.7|kn}} (diesel)
*{{convert|10|kn}} (electric)
|Ship range={{convert|3800|nmi}} at {{convert|10|kn}}
|Ship endurance=
|Ship test depth= {{convert|60|m|ft}}
|Ship complement=45
|Ship sensors=
|Ship EW=
|Ship armament=*6 × {{convert|533|mm|in|abbr=on|sigfig=2}} [[torpedo tubes]] (bow)
*12 × torpedoes
*2 × {{convert|102|mm|in|abbr=on|sigfig=1}} guns
*1 × [[machine gun]]

|Ship notes=
}}
|}

The '''Yugoslav submarine ''Nebojša''''' was the second of the [[Hrabri-class submarine|''Hrabri''-class]] [[diesel-electric]] [[submarine]]s built by the [[Vickers-Armstrongs|Vickers-Armstrong Naval Yard]] on the [[River Tyne]] in the [[United Kingdom]], for the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (later Yugoslavia) and was [[Ceremonial ship launching|launched]] in 1927. Her design was based on that of the [[British L-class submarine]] of [[World War I]], and she was built using parts originally assembled for a [[Royal Navy]] L-class submarine that was never built. She was armed with six bow-mounted {{convert|533|mm|in|abbr=on|sigfig=2}} [[torpedo tubes]], two {{convert|102|mm|in|abbr=on|sigfig=1}} guns and one [[machine gun]], and could dive to {{convert|60|m|ft}}.

Prior to [[World War II]] ''Nebojša'' participated in cruises to several [[Mediterranean Sea|Mediterranean]] ports. During the [[Nazi Germany|German]]-led [[Axis Powers|Axis]] [[invasion of Yugoslavia]] in April 1941, she evaded capture by [[Kingdom of Italy|Italian forces]], and joined British naval forces in the Mediterranean where she performed a training role. After the war she was taken over by the new Yugoslav government and renamed '''''Tara'''''. She was eventually [[Ship commissioning#Ship decommissioning|stricken]] in 1954, and [[Ship breaking|scrapped]] in 1958.

==Description and construction==
Yugoslav naval policy in the [[interwar period]] lacked direction until the mid-1920s,{{sfn|Jarman|1997a|p=732}} although it was generally accepted that the [[Adriatic Sea|Adriatic]] coastline was effectively a sea frontier that the naval arm was responsible for securing with the limited resources made available to it. In 1926, a modest ten-year construction program was initiated to build up a force of [[submarine]]s, coastal [[torpedo boat]]s, [[torpedo bomber]]s and conventional [[bomber]] aircraft to perform this role. The [[Hrabri-class submarine|''Hrabri''-class]] submarines were one of the first new acquisitions aimed at developing a naval force capable of meeting this challenge.{{sfn|Jarman|1997a|p=779}}

''Nebojša'' (Fearless) was built in 1927 for the [[Kingdom of Yugoslavia|Kingdom of Serbs, Croats and Slovenes]] (later Yugoslavia), by the [[Vickers-Armstrongs|Vickers-Armstrong Naval Yard]] on the [[River Tyne]] in the United Kingdom.{{sfn|Chesneau|1980|p=358}} Her design was based on that of the [[British L-class submarine]] of [[World War I]], and she was built using parts originally assembled for HMS ''L-68'', which was never completed.{{sfn|Akermann|2002|p=168}} Along with her [[sister ship|sister submarine]] ''[[Yugoslav submarine Hrabri|Hrabri]]'', she had an [[Length overall|overall length]] of {{convert|72.05|m|ftin}}, a [[Beam (nautical)|beam]] of {{convert|7.32|m|ftin|abbr=on|sigfig=2}}, and a surfaced [[Draft (hull)|draught]] of {{convert|3.96|m|ftin|abbr=on|sigfig=2}}. Her surfaced [[Displacement (ship)|displacement]] was {{convert|975|LT}} or {{convert|1164|LT}} submerged, and her crew consisted of 45 officers and enlisted men.{{sfn|Chesneau|1980|p=358}} She had an operational depth of {{convert|60|m|ft|abbr=on}}.{{sfn|Bagnasco|1977|p=171}}

The ''Hrabri''-class had two shafts driven by two [[diesel engine]]s (when surfaced) or two electric motors (when submerged). Their diesel engines were rated at {{convert|2400|bhp|lk=in}} and the electric motors at {{convert|1600|shp|lk=in}}, and they were designed to reach a top speed of {{convert|15.7|kn|lk=in}} under diesel power while surfaced, and {{convert|10|kn}} on their electric motors when submerged. They were armed with six bow-mounted {{convert|533|mm|in|abbr=on|sigfig=2}} [[torpedo tubes]], and carried twelve torpedoes.{{sfn|Fontenoy|2007|p=148}} They were also equipped with two {{convert|102|mm|in|abbr=on|sigfig=1}} [[deck gun]]s (one forward and one [[aft]] of the [[conning tower]]), and one [[machine gun]].{{sfn|Chesneau|1980|p=358}} Their radius of action was {{convert|3800|nmi}} at {{convert|10|kn}}.{{sfn|Akermann|2002|p=166}}

==Service career==
''Nebojša'' was [[Ceremonial ship launching|launched]] in 1927 as the second submarine of the navy of the Kingdom of Serbs, Croats and Slovenes, which later became the [[Royal Yugoslav Navy]].{{sfn|Chesneau|1980|p=358}} Along with her sister submarine ''Hrabri'', she left the Tyne in late January 1928.{{sfn|Hood|1928|p=154}} In company with the Yugoslav [[submarine tender]] {{ship|Yugoslav submarine tender|Hvar||2}}, the two submarines arrived in the [[Bay of Kotor]]  on the southern [[Adriatic Sea|Adriatic]] coast on 8 April 1928.{{sfn|Luković|6 April 2013}} In May and June 1929, ''Hrabri'', ''Nebojša'', ''Hvar'' and six torpedo boats accompanied the light cruiser ''[[SMS Niobe|Dalmacija]]'' on a cruise to [[Crown Colony of Malta|Malta]], the Greek island of [[Corfu]] in the [[Ionian Sea]], and [[Bizerte]] in the [[French protectorate of Tunisia]]. The British naval [[attaché]] observed that the ships and crews made a very good impression while visiting Malta.{{sfn|Jarman|1997b|p=183}} On 16 May 1930, ''Nebojša'' was exercising her crew at periscope depth near the entrance to the Bay of Kotor when she collided with a Yugoslav [[steamship]]. The damage was not serious and there were no injuries, but her forward 102&nbsp;mm gun was lost overboard. The necessary repairs were carried out at the dockyard in the Bay of Kotor.{{sfn|Jarman|1997b|p=247}}

In June and July 1930, ''Hrabri'', ''Nebojša'' and the fleet auxiliary ''Sitnica'' again cruised the [[Mediterranean Sea|Mediterranean]], visiting [[Alexandria]] and [[Beirut]].{{sfn|Radio Tivat|9 July 2014}} In 1932, the British naval attaché reported that Yugoslav ships engaged in few exercises, manoeuvres or gunnery training due to reduced budgets.{{sfn|Jarman|1997b|p=451}} In September 1933, ''Nebojša'' and the submarine [[Yugoslav submarine Osvetnik|''Osvetnik'']] cruised the southern part of the central Mediterranean.{{sfn|Jarman|1997b|p=453}} In August 1936, ''Nebojša'' and ''Osvetnik'' visited the Greek island of Corfu.{{sfn|Jarman|1997b|p=738}}

During the [[Nazi Germany|German]]-led [[Axis Powers|Axis]] [[invasion of Yugoslavia]] in April 1941, she and two [[Orjen-class torpedo boat|''Orjen''-class]] [[motor torpedo boat]]s evaded capture by [[Kingdom of Italy|Italian forces]] at the Bay of Kotor, arriving at [[Souda Bay|Suda Bay]], [[Crete]], on 23 April,{{sfn|Willmott|2010|p=311}} after eight days at sea.{{sfn|Shores|Cull|Malizia|1987|p=295}} Despite this, the Italians claimed that they had sunk all the Yugoslav vessels.{{sfn|The Ottawa Journal|1941|p=17}} ''Nebojša'' then sailed to Alexandria, but the [[Royal Navy]] considered her unfit for combat duties. [[Prime Minister of the United Kingdom|British Prime Minister]] [[Winston Churchill]] suggested her crew might be retrained and used to operate the recently captured German [[German Type VII submarine#Type VIIC|Type VIIC U-boat]] ''[[HMS Graph|U-570]]'', but this idea was soon abandoned.{{sfn|Blair|1996|p=233}} She was based at [[Valletta]] in Malta as an [[anti-submarine warfare]] training vessel,{{sfn|Večernje Novosti|21 March 2011}} serving with the [[British 2nd Submarine Flotilla]] in 1942 and the [[British 3rd Submarine Flotilla]] in 1943.{{sfn|Chesneau|1980|p=358}} She continued working in the Mediterranean until the end of the war,{{sfn|Bagnasco|1977|p=251}} but her service with the Royal Navy appears to have been limited to a training role.{{sfn|Thomas|1991|p=35}}

After the war she was towed first to [[Bari]] in Italy, then in August 1945 to the port of [[Split, Croatia|Split]] where she was overhauled, renamed ''Tara'' and given the [[pennant number]] 801. She was then transferred to [[Pula]] on the [[Istria]]n peninsula in the northern Adriatic. Used to train the fledgling [[Yugoslav Navy]] submarine arm,{{sfn|Večernje Novosti|21 March 2011}} she was [[Ship commissioning#Ship decommissioning|stricken]] in 1954.{{sfn|Chesneau|1980|p=358}}{{sfn|Fontenoy|2007|p=148}} One of her guns was removed at the end of her career,{{sfn|Gardiner|1983|p=388}} and she was eventually [[Ship breaking|scrapped]] in 1958.{{sfn|Jane's Publishing|1963|p=444}}

==Legacy==
In 2011, to mark the 70th anniversary of the invasion of Yugoslavia, the [[Military Museum (Belgrade)|Military Museum]] in [[Belgrade]], [[Serbia]] hosted an exhibit which included a flag from the ''Nebojša''.{{sfn|Blic online|6 April 2011}} In April 2013, the 85th anniversary of the arrival of the first Yugoslav submarines at the Bay of Kotor was marked by an event in [[Tivat]], [[Montenegro]], attended by dozens of former Yugoslav submariners.{{sfn|Luković|6 April 2013}}

== See also ==
*[[List of ships of the Royal Yugoslav Navy]]
*[[List of ships of the Yugoslav Navy]]

==Footnotes==
{{reflist|20em}}

==References==

===Books===
{{refbegin|40em}}
* {{cite book
  | last = Akermann
  | first = Paul
  | title = Encyclopedia of British Submarines 1901–1955
  | year = 2002
  | publisher = Periscope Publishing
  | location = Penzance, Cornwall
  | isbn = 978-0-907771-42-5
  | url = https://books.google.com/books?id=boO7WGL21EQC
  | ref = harv
  }}
* {{cite book
  | last = Bagnasco
  | first = Erminio
  | year = 1977
  | title = Submarines of World War Two
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | isbn = 978-0-87021-962-7
  | ref = harv
  }}
* {{cite book
  | last = Blair
  | first = Clay
  | year = 1996
  | title = Hitler's U-Boat War: The Hunters, 1939–1942
  | publisher = Random House
  | location = New York, New York
  | isbn = 978-0-297-86621-3
  | url = https://books.google.ca/books?id=E8GDrZJEAGIC
  | ref = harv
  }}
* {{cite book
  | editor-last = Chesneau
  | editor-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Conway Maritime Press
  | location = London, England
  | isbn = 978-0-85177-146-5
  | ref = harv
  }}
* {{cite book
  | last = Fontenoy
  | first = Paul E.
  | year = 2007
  | title = Submarines: An Illustrated History of Their Impact
  | publisher = ABC-CLIO
  | location = Santa Barbara, California
  | isbn = 978-1-85109-563-6
  | url = https://books.google.ca/books?id=yD3eSRfUIesC
  | ref = harv
  }}
* {{cite book
  | editor-last = Gardiner
  | editor-first = Robert
  | year = 1983
  | title = Conway's All the World's Fighting Ships, 1947–1982: The Warsaw Pact and Non-Aligned Nations
  | publisher = Naval Institute Press
  | location = Annapolis, Maryland
  | oclc = 165766596
  | ref = harv
  }}
* {{cite book
  | last = Jane's Publishing
  | year = 1963
  | title = Jane's Fighting Ships 1963–64
  | publisher = Jane's Publishing
  | location = London, England
  | oclc = 35864977
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997a
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 1
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | editor-last = Jarman
  | editor-first = Robert L.
  | year = 1997b
  | title = Yugoslavia Political Diaries 1918–1965
  | volume = 2
  | publisher = Archives Edition
  | location = Slough, Berkshire
  | isbn = 978-1-85207-950-5
  | ref = harv
  }}
* {{cite book
  | last1 = Shores
  | first1 = Christopher F.
  | last2 = Cull
  | first2 = Brian
  | last3 = Malizia
  | first3 = Nicola
  | title = Air War for Yugoslavia, Greece, and Crete, 1940–41
  | year = 1987
  | publisher = Grub Street
  | location = London, England
  | isbn = 978-0-948817-07-6
  | url = https://books.google.ca/books?id=2pSPBQAAQBAJ
  | ref = harv
  }}
* {{cite book
  | last = Thomas
  | first = Nigel
  | year = 1991
  | title = Foreign Volunteers of the Allied Forces, 1939–45
  | publisher = Osprey
  | location = London, England
  | isbn = 978-1-85532-136-6
  | url = https://books.google.ca/books?id=PsSZSQt8VLcC
  | ref = harv
  }}
* {{cite book
  | last = Willmott
  | first = H.P.
  | year = 2010
  | title = The Last Century of Sea Power: From Washington to Tokyo, 1922–1945
  | publisher = Indiana University Press
  | location = Bloomington, Indiana
  | isbn = 978-0-253-35214-9
  | url = https://books.google.com/books?id=2AiIL5icXqMC
  | ref = harv
  }}
{{refend}}

===Periodicals===
{{refbegin|40em}}
* {{cite news
  | last         = Hood
  | first        = A.G.
  | title        = The Jugo-Slavian Submarines ''Hrabri'' and ''Nebojsa''
  | url          = 
  | newspaper    = The Shipbuilder and Marine Engine-builder
  | volume       = 35
  | issue        = 
  | location     = London, England
  | publisher    = Shipbuilder Press
  | date         = 
  | year         = 1928
  | accessdate   = 
  | oclc = 2450525
  | ref          = harv
  }}
* {{cite news
  | author       = The Ottawa Journal
  | title        = Official Reports
  | url          = https://www.newspapers.com/newspage/44434513/
  | subscription = yes
  | newspaper    = The Ottawa Journal
  | volume       = 56
  | issue        = 121
  | location     = Ottawa, Ontario
  | publisher    = The Ottawa Evening Journal
  | date         = 1 May 1941
  | accessdate   = 18 April 2014
  | issn = 0841-4572
  | ref          = harv
  }}
{{refend}}

===Websites===
{{refbegin|40em}}
* {{cite news|author=Blic online |date=6 April 2011 |accessdate=8 October 2015 |language=Serbo-Croatian |title=Deo "štuke" i zastava sa podmornice "Nebojša" u Vojnom muzeju |trans_title=Part of a Stuka and the flag of the submarine Nebojsa in the Military Museum |newspaper=Blic online |url=http://www.blic.rs/Vesti/Beograd/246267/Deo-stuke-i-zastava-sa-podmornice-Nebojsa-u-Vojnom-muzeju |ref={{harvid|Blic online|6 April 2011}} |deadurl=yes |archiveurl=https://web.archive.org/web/20151008030511/http://www.blic.rs/Vesti/Beograd/246267/Deo-stuke-i-zastava-sa-podmornice-Nebojsa-u-Vojnom-muzeju |archivedate=8 October 2015 }}
* {{cite news|last=Luković |first=Siniša |date=6 April 2013 |accessdate=8 October 2015 |language=Serbo-Croatian |title=85 godina od dolaska prvih jugoslovenskih podmornica |trans_title=85 years since the arrival of the first Yugoslav submarines |newspaper=Vijesti online |url=http://www.vijesti.me/vijesti/85-godina-dolaska-prvih-jugoslovenskih-podmornica-clanak-122013{{cbignore}} |ref={{harvid|Luković|6 April 2013}} |deadurl= |archiveurl= |archivedate= }}
* {{cite news|date=21 March 2011 |accessdate=3 October 2015 |language=Serbo-Croatian |title=Preminuo poslednji čuvar srpske podmornice |trans_title=The Last Guardian of a Serbian Submarine Has Died |newspaper=Večernje Novosti |url=http://www.novosti.rs/vesti/naslovna/aktuelno.69.html:323768-Preminuo-poslednji-cuvar-srpske-podmornice |ref={{harvid|Večernje Novosti|21 March 2011}} |deadurl=yes |archiveurl=https://web.archive.org/web/20151008030857/http://www.novosti.rs/vesti/naslovna/aktuelno.69.html:323768-Preminuo-poslednji-cuvar-srpske-podmornice |archivedate=8 October 2015 }}
* {{cite news|author=Radio Tivat |date=9 July 2014 |accessdate=3 October 2015 |language=Serbo-Croatian |title=Tivat kroz novinsku građu - 9.jul |trans_title=Tivat in newspapers – 9 July |newspaper=Radio Tivat |url=http://radiotivat.com/index.php/zdravlje/33-tivat-kroz-novinsku-gradju/9439-tivat-kroz-novinsku-gradju-9-jul.html |ref={{harvid|Radio Tivat|9 July 2014}} |deadurl=yes |archiveurl=https://web.archive.org/web/20151008031358/http://radiotivat.com/index.php/zdravlje/33-tivat-kroz-novinsku-gradju/9439-tivat-kroz-novinsku-gradju-9-jul.html |archivedate=8 October 2015 }}
{{refend}}
{{Hrabri-class submarine}}

{{DEFAULTSORT:Nebojsa}}
[[Category:1927 ships]]
[[Category:World War II submarines of Yugoslavia]]
[[Category:Submarines of the Royal Yugoslav Navy]]
[[Category:Tyne-built ships]]
[[Category:Ships of the Yugoslav Navy]]
[[Category:Hrabri-class submarine]]