{{Infobox journal
| title = Feminist Review
| cover = [[File:Feminist_Review_cover.gif|150px]]
| caption = Cover of the first issue.
| discipline = [[Women's studies]]
| abbreviation = Feminist Rev.
| editor = A collective of editors
| publisher = [[Palgrave Macmillan]]
| country = [[United Kingdom]]
| frequency = Triannually
| history = 1979–present
| impact = 1.156
| impact-year = 2015
| website = http://www.palgrave-journals.com/fr/index.html
| link1 = http://www.palgrave-journals.com/fr/archive/index.html
| link1-name = Online archive
| ISSN = 0141-7789
| eISSN = 1466-4380
| OCLC = 40348469
| LCCN = 80647745
| JSTOR = 01417789
| CODEN = 
}}
'''''Feminist Review''''' is a triannual [[peer-reviewed]] interdisciplinary journal with a focus on exploring [[gender]] in its multiple forms and interrelationships.<ref>{{cite web|title= Feminist Review: About the journal: Aims and scope of journal|url= http://www.palgrave-journals.com/fr/about.html#Aims-and-scope-of-journal|publisher=[[Palgrave Macmillan]]|accessdate=31 August 2015 }}</ref> The journal was established in 1979.<ref>{{cite web|title=Feminist Magazines|url=http://www.feminist.org/research/zines.html|work=Feminist Majority Foundation|accessdate=27 October 2015}}</ref> It is published by [[Palgrave Macmillan]] and is edited by a collective.<ref>{{cite web |title=Feminist Review: About the journal: Editors|url= http://www.palgrave-journals.com/fr/about.html#Editors|publisher=Palgrave Macmillan|accessdate= 31 August 2015 }}</ref> 

== Abstracting and indexing ==
{{columns-list|2|
* ABES Annotated Bibliography for English Studies
* [[Applied Social Sciences Index and Abstracts|Applied Social Science Index and Abstracts (ASSIA)]]
* Australian Citizenship Database
* [[British Humanities Index]]
* [[Current Contents]]/Social & Behavioural Sciences
* Ex Libris / Primo Central
* Expanded Academic Review
* Family Studies database
* Gay and Lesbian Abstracts
* [[International Bibliography of Book Reviews of Scholarly Literature and Social Sciences|International Bibliography of Book Reviews (IBR)]]
* [[International Bibliography of Periodical Literature|International Bibliography of Periodical Literature (IBZ)]]
* [[International Bibliography of the Social Sciences|International Bibliography of the Social Sciences (IBSS)]]
* [[International Political Science Abstracts]]
* [[Social Sciences Citation Index|ISI: Social Sciences Citation Index]]
* Multicultural Education Abstracts
* [[OCLC]] FirstSearch Database
* Periodica Islamica
* Political Science and Government Abstracts
* Research Alert
* [[SCOPUS]]
* Social SciSearch
* Social Services Abstracts
* [[Sociological Abstracts]]
* Social Sciences Index (H.W. Wilson)
* [[Studies on Women and Gender Abstracts]]
* [[Worldwide Political Science Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.156, ranking it 15th out of 40 journals in the category "Women's Studies".<ref>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Women's Studies |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of women's studies journals]]

== References ==
{{Reflist|30em}}

{{DEFAULTSORT:Feminist Review }}
[[Category:English-language journals]]
[[Category:Feminist journals]]
[[Category:Palgrave Macmillan academic journals]]
[[Category:Publications established in 1979]]
[[Category:Triannual journals]]
[[Category:Women's studies journals]]


{{gender-journal-stub}}