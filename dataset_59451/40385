{{Infobox film
| name           = EM Embalming
| image          = Enbamingu-88c89030.jpg
| image_size     = 
| border         = 
| alt            = 
| caption        = 
| director       = [[Shinji Aoyama]]
| producer       = Katsuaki Takemoto<br/>Satoru Ogura
| writer         = [[Izo Hashimoto]]<br/>Shinji Aoyama
| screenplay     = 
| story          = 
| based on       = {{based on|EM|Saki Amemiya}}
| narrator       = 
| starring       = [[Reiko Takashima]]<br/>[[Yutaka Matsushige]]<br/>[[Seijun Suzuki]]<br/>Toshio Shiba
| music          = Isao Yamada<br/>Shinji Aoyama
| cinematography = Ihiro Nishikubo
| editing        = Soichi Ueno<br/>Shinji Aoyama
| studio         = 
| distributor    = 
| released       = {{Film date|1999|07|31|Japan}}
| runtime        = 96 minutes
| country        = Japan
| language       = Japanese
| budget         = 
| gross          = 
}}
{{Nihongo|'''''EM Embalming'''''|EM エンバーミング|EM Enbāmingu}} is a 1999 Japanese [[horror film]] directed by [[Shinji Aoyama]], starring [[Reiko Takashima]].

==Cast==
* [[Reiko Takashima]] as Miyako Murakami
* [[Yutaka Matsushige]] as Detective Hiraoka
* [[Seijun Suzuki]] as Kurume
* Toshio Shiba as Dr. Fuji
* Kojiro Hongo as Jion
* Hitomi Miwa as Rika Shinohara
* Masatoshi Matsuo as Yoshiki Shingo

==Reception==
Todd Brown of [[Twitch Film]] commented that Shinji Aoyama is creating a subtle parody of the [[Japanese horror]] film industry.<ref>{{cite web|url=http://twitchfilm.com/2005/06/em-embalming-dvd-review.html|title=EM Embalming DVD Review|publisher=[[Twitch Film]]|author=Todd Brown|date=June 6, 2005}}</ref> Stina Chyn of [[Film Threat]] noted that ''EM Embalming'' is one of the few Japanese horror films that contains actual non-creepy segments.<ref>{{cite web|url=http://www.filmthreat.com/features/1605/|title=Film Phonics: "em Embalming"|publisher=[[Film Threat]]|author=Stina Chyn|date=October 20, 2005}}</ref> Andy McKeague of [[Monsters and Critics]] felt that the film is genuinely unsettling and morbidly fascinating at the same time.<ref>{{cite web|url=http://www.monstersandcritics.com/dvd/reviews/article_8343.php/DVD_Review_EM_-_Embalming|title=DVD Review: EM - Embalming|publisher=[[Monsters and Critics]]|author=Andy McKeague|date=June 5, 2005}}</ref>

Mike Bracken of [[IGN]] criticized the film, saying that ''EM Embalming'''s greatest failure is that it often tries to be too many things at once and the film itself is almost as schizophrenic as its antagonist. However, he felt that several sequences of the embalming process are gruesome and extended conversations between Miyako and the black market organ harvester are eerily intriguing.<ref>{{cite web|url=http://www.ign.com/articles/2005/06/17/the-horror-geek-speaks-em-embalming|title=The Horror Geek Speaks: EM Embalming|publisher=[[IGN]]|author=Mike Bracken|date=June 16, 2005}}</ref>

Meanwhile, [[Mark Schilling]] of [[The Japan Times]] said: "While verging on a black comic turn, Toshio Shiba's performance as Dr. Fuji is the film's strongest; his dark night of the soul is not just another fashionably blank attitude, but the genuine article, forged in the satanic mills of anger, loathing and despair."<ref>{{cite web|url=http://www.japantimes.co.jp/text/ff19990817a1.html|title=Aoyama offers a stiff little mystery|publisher=[[The Japan Times]]|author=Mark Schilling|date=August 17, 1999|authorlink=Mark Schilling}}</ref>

==References==
{{Reflist}}

==External links==
* {{IMDb title|0205907}}
* {{jmdb title|1999|dw002050}}

{{Shinji Aoyama}}

[[Category:Films directed by Shinji Aoyama]]
[[Category:Psychological horror films]]
[[Category:Japanese horror films]]
[[Category:Japanese films]]
[[Category:1999 horror films]]
[[Category:1999 films]]