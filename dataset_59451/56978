The '''Federation of the European Biochemical Societies''', frequently abbreviated '''FEBS''' is an international scientific society promoting activities in [[biochemistry]], [[molecular biology]] and molecular [[biophysics]] in [[Europe]]. Since it was founded in 1964 it has grown to include almost 40,000 members from 36 member societies and 7 associated societies from 43 countries [http://www.febs.org/index.php?id=64].

== Present activities ==
FEBS sponsors advanced courses for [[Ph.D.]]-students and [[postdoc]]s, arranges [[scientific conference|conferences]] and awards [[scholarship|fellowship]]s, awards and medals. FEBS distributes surplus scientific equipment in the poorer member countries as a part of the Scientific Apparatus Recycling Scheme (SARS). In addition, FEBS gives young scientists from [[Eastern Europe|Eastern]] and [[Central Europe]] a possibility to visit and work in labs in [[Western Europe]]. FEBS collaborates with related scientific societies such as the [[European Molecular Biology Organization]] (EMBO), [[European Life Scientist Organisation]] (ELSO), and the [[European Molecular Biology Laboratory]] (EMBL). FEBS is also founding member of the [[Initiative for Science in Europe]].

==Sir Hans Krebs Lecture and Medal==
The [[Sir Hans Krebs Medal]] is awarded annually for outstanding achievements in Biochemistry and Molecular Biology or related sciences. It was endowed by the Lord Rank Centre for Research and named after the German-born British biochemist [[Hans Adolf Krebs]], well known for identifying the [[urea cycle|urea]] and [[citric acid cycle]]s. The awardee receives a silver medal and presents one of the plenary lectures at the FEBS Congress.<ref name = FEBSA>{{cite web|url =http://www.febs.org/our-activities/awards/medals|title= FEBS Medals|publisher= Federation of European Biochemical Societies|accessdate= 16 December 2014}}</ref>

==Datta Lectureship and Medal==
The Datta Lectureship award is awarded for outstanding achievements in the field of Biochemistry and Molecular Biology or related sciences. The award is endowed by capital gifts from [[Elsevier Science Publishers]] and is named after S. Prakash Datta, the first Managing Editor of FEBS Letters (1968–1985) and Treasurer of FEBS. The awardee, who should normally be from a FEBS country, receives a medal provided by Elsevier Science Publishers and presents one of the plenary lectures at the FEBS Congress.<ref name = FEBSA/>

==Theodor Bücher Lecture and Medal==
The Theodor Bücher Lecture and Medal is awarded for outstanding achievements in Biochemistry and Molecular Biology or related sciences. It was endowed by a capital gift from Frau Ingrid Bücher to the Gesellschaft für Biochemie und Molekularbiologie (GBM) and is named after the German biochemist, [[Theodor Bücher]]. The awardee receives a silver medal and presents one of the plenary lectures at the FEBS Congress.<ref name = FEBSA/>

== Journals ==
FEBS publishes four scientific journals ''[[FEBS Journal]], [[FEBS Letters]]'', and ''[[Molecular Oncology (journal)|Molecular Oncology]]'' and ''FEBS Open Bio''. ''FEBS Journal'' was previously known as the ''European Journal of Biochemistry''. ''FEBS Journal'' publishes full length research reports, whereas ''FEBS Letters'' is dedicated to brief and rapid reports. ''FEBS Open Bio'' is an [[open access journal]].

== See also ==
* [[Life Sciences Switzerland]]

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.febs.org/|FEBS official website}}

{{Portal bar|Science|Europe}}
{{Chemistry societies}}

[[Category:Organizations established in 1964]]
[[Category:Biology societies]]
[[Category:Chemistry societies]]
[[Category:International scientific societies of Europe]]
[[Category:European medical and health organizations]]
[[Category:Science and technology in Europe]]