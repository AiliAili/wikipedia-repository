<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=F.E.3
 | image=Royal Aircraft Factory FE3 front view.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Experimental armed aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=[[Royal Aircraft Factory]]
 | designer=
 | first flight=1913
 | introduced=
 | retired=
 | status=Prototype
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Royal Aircraft Factory F.E.3''' (also known as the A.E.1 ("Armed Experimental") was a [[United Kingdom|British]] experimental single-engined [[pusher configuration|pusher]] [[biplane]] built prior to the [[First World War]]. It was intended to be fitted with a shell-firing gun, but was quickly abandoned, being found to be structurally unsound.

==Design and development==
[[File:Royal Aircraft Factory FE3 side view.jpg|thumb|right]]
In 1913, the [[Royal Aircraft Factory]] designed an experimental armed pusher biplane, the '''F.E.3''' ("Farman" or "Fighting" Experimental), with the alternative designation '''A.E.1''' ("Armed Experimental"). The F.E.3 was to carry a [[Coventry Ordnance Works]] 1½ lb shell-firing quick loading gun.  In order to reduce the drag associated with the pusher layout favoured for gun-carrying aircraft, the tail was carried on a single tubular tailboom connected by bearings to the four-bladed propeller, with bracing wires running to the wings and undercarriage. The crew of two, gunner and pilot, sat in tandem in a wood and metal nacelle. The aircraft was powered by a single water-cooled Chenu [[Inline engine (aviation)|inline engine]] mounted in the nose of the nacelle, connected to a long shaft running under the cockpit which drove the propeller using a chain drive.  The gun was to fire through the cooling intake for the engine.<ref name="Hare p219">Hare 1990, p. 219.</ref><ref name="Mason Fighter p14-5">Mason 1992, pp. 14–15.</ref>

It flew in the summer of 1913, but testing was stopped after the aircraft's propeller broke in flight resulting in a forced landing.  Flight testing did not resume, as it was realised that the tailboom was not strong enough to allow safe flying.<ref name="Hare p64-5">Hare 1990, pp. 64–65.</ref>  Although the F.E.3 did not fly again, the gun installation was test fired with the aircraft suspended from a hangar roof, showing that recoil loads were not excessive.<ref name="Hare p221"/>

==Specifications==
{{Aircraft specs
|ref=The Royal Aircraft Factory<ref name="Hare p221">Hare 1990, p. 221.</ref>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=29
|length in=3
|length note=
|span m=
|span ft=40
|span in=0
|height m=
|height ft=11
|height in=3
|height note=
|wing area sqm=
|wing area sqft=436.5
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=1400
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=2080
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=Chenu
|eng1 type=water-cooled inline engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=4<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=75
|max speed kts=
|max speed note=at sea level
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=5000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=350
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Royal Aircraft Factory F.E.3}}
{{refbegin}}
* Hare, Paul R. ''The Royal Aircraft Factory''. London:Putnam, 1990. ISBN 0-85177-843-7.
* Mason, Francis K. ''The British Fighter Since 1912''. Annapolis, MD: Naval Institute Press, 1992. ISBN 1-55750-082-7.
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->

{{Royal Aircraft Factory aircraft}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Pusher aircraft]]
[[Category:Royal Aircraft Factory aircraft|FE03]]