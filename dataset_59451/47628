{{citation style|date=October 2012}}
A '''Dynamic Manufacturing Network''' (DMN) is a coalition, either permanent or temporal, comprising production systems of geographically dispersed [[Small and medium enterprises]] and/or [[Original equipment manufacturers]] that collaborate in a shared value-chain to conduct joint manufacturing.<ref>Viswanadham, N. (2003). Partner Selection and Synchronized Planning in Dynamic Manufacturing Networks, IEEE Transactions on Robotics and Automation, Vol. 19, No. 1 [http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=01177169]</ref><ref>Papakostas, N. et al. (2012). On the configuration and planning of dynamic manufacturing networks, Logistics Research Journal, ISSN 1865-035X, September 2012 [http://link.springer.com/article/10.1007/s12159-012-0086-9]</ref><ref>IMAGINE Project: Innovative End-to-end Management of Dynamic Manufacturing Networks. Description of Work [http://www.imagine-futurefactory.eu www.imagine-futurefactory.eu]</ref>

Manufacturing networks have become increasingly common in [[applied research]] on [[manufacturing]], since several manufacturing enterprises have recently shown great interest for creating such networks and take advantage of them both for [[Collaborative product development]] <ref>IndustryWeek (2009). Product Development Assistance from Manufacturing Networks [http://www.industryweek.com/public-policy/product-development-assistance-manufacturing-networks]</ref> and for [[Supply chain optimization]].<ref>Deflorin, P, Scherrer-Rathje, M, Dietl, H. (2009). The competitive advantage of the lead factory concept in geographically distributed R&D and production networks. European Operations Management Association (EUROMA). Göteborg, Sweden</ref>

During the last decade the effort is mainly focused on the Dynamic Management of the Manufacturing networks, as proven by several studies published by Accenture,<ref>Accenture (2012). Developing Dynamic and Efficient Operations for Profitable Growth - Research Findings from North American Manufacturers [http://www.accenture.com/SiteCollectionDocuments/PDF/Accenture-Developing-Dynamic-Efficient-Operations.pdf]</ref> MIT
<ref>Williams, G.P. (2011). Dynamic order allocation for make-to-order manufacturing networks: an industrial case study of optimization under uncertainty, Massachusetts Institute of Technology, 2011 [http://dspace.mit.edu/handle/1721.1/67770]</ref> and University of St.Gallen <ref>[http://www.globalmanufacturingnetworks.com/Home.html University of St.Gallen (2012). Global Manufacturing Networks]</ref>

== References ==
{{Reflist}}
*
*
*

[[Category:Production and manufacturing]]