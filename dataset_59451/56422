'''Copac''' is a [[union catalogue]] which provides free access to the merged online catalogues of many major [[research library|research libraries]] and specialist libraries in the United Kingdom and Ireland, plus the [[British Library]], the [[National Library of Scotland]] and the [[National Library of Wales]].  It has over 40 million records<ref name="copac_about"/> from around 90 libraries, representing a wide range of materials across all subject areas. Copac is freely available to all,<ref name="copac_about">[http://copac.ac.uk/about/ Copac.ac.uk]</ref> and is widely used, with users mainly coming from UK Higher Education, but also worldwide.<ref name="copac_survey">[http://blog.copac.ac.uk/wp-content/uploads/sites/12/2015/02/Copac-User-Survey-November-2014.pdf Copac blog]</ref> Users value Copac highly as a research tool.<ref name="copac_survey"/>

Copac is searchable through an easy to use web interface as well as via a [[Z39.50]] client.  It is also accessible through [[OpenURL]] and [[Search/Retrieve via URL|SRU]] interfaces.<ref>[http://copac.ac.uk/developers Copac.ac.uk]</ref> These interfaces can be used to provide links to items on Copac from external sites, such as this [http://www.history.ac.uk/history-online/book/isbn/0195131010 isbn search example].

Copac is a [[Jisc]] service provided for the UK community on the basis of an agreement with RLUK ([[Research Libraries UK]]).<ref>[http://copac.ac.uk/ Copac.ac.uk]</ref> The service uses records supplied by RLUK members, as well as an increasing range of specialist libraries with collections of national research interest. A full list of contributors is available.<ref name="copac_libraries">[http://copac.ac.uk/about/libraries Copac.ac.uk]</ref>

==See also==
* [[Library and Archives Canada]]
* [[Research Libraries UK]]
* [[SUNCAT]]
* [[WorldCat]]

==References==
{{reflist}}

==External links==
*[http://copac.ac.uk/ Copac]
*[http://copac.ac.uk/developers Copac interfaces for developers]
*[http://copac.ac.uk/about/libraries Copac contributing libraries]
*[http://www.jisc.ac.uk/ Jisc]
*[http://www.rluk.ac.uk/ Research Libraries UK (RLUK)]


[[Category:Bibliographic databases and indexes]]
[[Category:Databases in the United Kingdom]]
[[Category:Higher education in the United Kingdom]]
[[Category:Library cataloging and classification]]
[[Category:Online databases]]