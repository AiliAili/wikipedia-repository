{{merge|Oligodendrocyte progenitor cell|date=January 2016}}
'''Polydendrocytes''' (also known as NG2 cells, [[NG2 glia]], or [[oligodendrocyte progenitor]] cells) are process-bearing glial cells ([[neuroglia]]) in the mammalian [[central nervous system]] (CNS) that are identified by the expression of the NG2 chondroitin sulfate proteoglycan ([[CSPG4]]) <ref>[http://www.ensembl.org/Homo_sapiens/Search/Details?species=Homo_sapiens;idx=Gene;end=1;q=CSPG4 Ensembl genome browser 68: Homo sapiens - Result in Detail - Ensembl Lucene search<!-- Bot generated title -->]</ref> and the alpha receptor for platelet-derived growth factor ([[PDGFRA]]).<ref>[http://www.ensembl.org/Homo_sapiens/Search/Details?species=Homo_sapiens;idx=Gene;end=1;q=PDGFRA Ensembl genome browser 68: Homo sapiens - Result in Detail - Ensembl Lucene search<!-- Bot generated title -->]</ref>  They are distinct from other cell populations such as [[neurons]], [[astrocytes]], [[oligodendrocytes]], [[microglia]], and [[neural stem cells]] and are recognized as the fourth major glial cell type in the mammalian CNS.{{Citation needed|date=February 2013}}  Studies have implicated polydendrocytes in many cellular and physiological processes.  Polydendrocytes in the postnatal mouse CNS and those grown in culture generate [[oligodendrocytes]], and thus they are often equated with [[oligodendrocyte progenitor]] cells (OPCs).  Under some culture conditions, polydendrocytes give rise to [[astrocytes]].  A subpopulation of polydendrocytes in the [[gray matter]] of the embryonic CNS also generates [[protoplasmic astrocytes]].  In addition, polydendrocytes express receptors for various [[neurotransmitters]] and undergo membrane [[depolarization]] when they receive synaptic inputs from neurons.

== History ==
It had been known since the early 1900s that astrocytes, oligodendrocytes, and microglia make up the major glial cell populations in the mammalian CNS.  The presence of another glial cell population had escaped recognition because of the lack of a suitable marker to identify them in tissue sections.  The notion that there exists a population of glial progenitor cells in the developing and mature CNS began to be entertained in the late 1980s by several independent groups.  In one series of studies on the development and origin of oligodendrocytes in the rodent CNS, a population of immature cells that appeared to be precursors to oligodendrocytes was identified by the expression of the GD3 [[ganglioside]].<ref>{{Cite journal 
| last1 = Hirano | first1 = M. 
| last2 = Goldman | first2 = J. E. 
| doi = 10.1002/jnr.490210208 
| title = Gliogenesis in rat spinal cord: Evidence for origin of astrocytes and oligodendrocytes from radial precursors 
| journal = Journal of Neuroscience Research 
| volume = 21 
| issue = 2–4 
| pages = 155–167 
| year = 1988 
| pmid = 3216418 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Bankir | first1 = L. 
| last2 = Bouby | first2 = N. 
| last3 = Trinh-Trang-Tan | first3 = M. M. 
| title = Heterogeneity of nephron anatomy 
| journal = Kidney International. Supplement 
| volume = 20 
| pages = S25–S39 
| year = 1987 
| pmid = 3298801
}}</ref>

In a separate series of studies, cells from perinatal rat [[optic nerves]] that expressed the A2B5 [[ganglioside]] were shown to differentiate into [[oligodendrocytes]] in culture.<ref>{{Cite journal 
| doi = 10.1038/303390a0 
| last1 = Raff | first1 = M. C. 
| last2 = Miller | first2 = R. H. 
| last3 = Noble | first3 = M. 
| title = A glial progenitor cell that develops in vitro into an astrocyte or an oligodendrocyte depending on culture medium 
| journal = Nature 
| volume = 303 
| issue = 5916 
| pages = 390–396 
| year = 1983 
| pmid = 6304520
}}</ref>  Subsequently, A2B5+ cells from other CNS regions and from adult CNS were also shown to generate oligodendrocytes.  Based on the observation that these cells require [[PDGF]] for their proliferation and expansion, the expression of the alpha receptor for platelet-derived growth factor (Pdgfra) was used to search for the in vivo correlates of the A2B5+ cells, which led to the discovery of a unique population of Pdgfra+ cells in the CNS whose appearance and distribution were consistent with those of developing oligodendrocytes.<ref>{{Cite journal 
| last1 = Pringle | first1 = N. P. 
| last2 = Mudhar | first2 = H. S. 
| last3 = Collarini | first3 = E. J. 
| last4 = Richardson | first4 = W. D. 
| title = PDGF receptors in the rat CNS: During late neurogenesis, PDGF alpha-receptor expression appears to be restricted to glial cells of the oligodendrocyte lineage 
| journal = Development (Cambridge, England) 
| volume = 115 
| issue = 2 
| pages = 535–551 
| year = 1992 
| pmid = 1425339
}}</ref>

Independently, Stallcup and colleagues generated an antiserum that recognized a group of rat brain tumor cell lines, which exhibited properties that were intermediate between those of typical neurons and glial cells.  Biochemical studies showed that the antiserum recognized a chondroitin sulfate proteoglycan with a core glycoprotein of 300 kDa,<ref name=Stallcup1983>{{Cite journal 
| doi = 10.1101/SQB.1983.048.01.078 
| last1 = Stallcup | first1 = W. B. 
| last2 = Beasley | first2 = L. 
| last3 = Levine | first3 = J. 
| title = Cell-surface molecules that characterize different stages in the development of cerebellar interneurons 
| journal = Cold Spring Harbor symposia on quantitative biology 
| volume = 48 
| pages = 761–774 
| year = 1983 
| pmid = 6373111
 | issue=2
}}</ref> and the antigen was named [[Neuron-glial antigen 2|NG2]] (nerve/glial antigen 2).<ref>{{Cite journal 
| doi = 10.1016/0014-4827(76)90439-0 
| last1 = Stallcup | first1 = W. B. 
| last2 = Cohn | first2 = M. 
| title = Electrical properties of a clonal cell line as determined by measurement of ion fluxes 
| journal = Experimental Cell Research 
| volume = 98 
| issue = 2 
| pages = 277–284 
| year = 1976 
| pmid = 943300
}}</ref><ref>{{Cite journal 
| doi = 10.1016/S0012-1606(81)80017-6 
| last1 = Wilson | first1 = S. S. 
| last2 = Baetge | first2 = E. E. 
| last3 = Stallcup | first3 = W. B. 
| title = Antisera specific for cell lines with mixed neuronal and glial properties 
| journal = Developmental Biology 
| volume = 83 
| issue = 1 
| pages = 146–153 
| year = 1981 
| pmid = 6263737
}}</ref>  NG2 was found to be expressed on A2B5+ oligodendrocyte precursor cells isolated from the perinatal rat CNS tissues and on process-bearing cells in the CNS in vivo.<ref name="Stallcup1983" /><ref>{{Cite journal 
| last1 = Shaĭtan | first1 = K. V. 
| last2 = Ermolaeva | first2 = M. D. 
| last3 = Saraĭkin | first3 = S. S. 
| title = Molecular dynamics of oligopeptides. 3. Maps of levels of free energy of modified dipeptides and dynamic correlation in amino acid residues 
| journal = Biofizika 
| volume = 44 
| issue = 1 
| pages = 18–21 
| year = 1999 
| pmid = 10330580
}}</ref>  Comparison of NG2 and Pdgfra expression revealed that NG2 and Pdgfra are expressed on the same population of cells in the CNS.<ref>{{Cite journal 
| last1 = Nishiyama | first1 = A. 
| last2 = Lin | first2 = X. -H. 
| last3 = Giese | first3 = N. 
| last4 = Heldin | first4 = C. -H. 
| last5 = Stallcup | first5 = W. B. 
| title = Co-localization of NG2 proteoglycan and PDGF ?-receptor on O2A progenitor cells in the developing rat brain 
| doi = 10.1002/(SICI)1097-4547(19960201)43:3<299::AID-JNR5>3.0.CO;2-E 
| journal = Journal of Neuroscience Research 
| volume = 43 
| issue = 3 
| pages = 299–314 
| year = 1996 
| pmid = 8714519 
| pmc = 
}}</ref>  These cells represent 2-9% of all the cells and remain proliferative in the mature CNS.<ref name="Dawson2003">{{Cite journal 
| doi = 10.1016/S1044-7431(03)00210-0 
| last1 = Dawson | first1 = M. R. 
| last2 = Polito | first2 = A. 
| last3 = Levine | first3 = J. M. 
| last4 = Reynolds | first4 = R. 
| title = NG2-expressing glial progenitor cells: An abundant and widespread population of cycling cells in the adult rat CNS 
| journal = Molecular and cellular neurosciences 
| volume = 24 
| issue = 2 
| pages = 476–488 
| year = 2003 
| pmid = 14572468
}}</ref>

== Origin and fate of polydendrocytes ==

=== Origin of polydendrocytes ===
In the embryonic [[spinal cord]], a major source of polydendrocytes is the ventral ventricular zone of the pMN domain marked by the expression of the transcription factors Olig1 and Olig2 and the p3 domain that expresses Nkx2.2, which are induced by the morphogen Shh ([[sonic hedgehog]]).  Some polydendrocytes also arise from the dorsal ventricular zone.  In the [[forebrain]], three regionally distinct sources have been shown to generate polydendrocytes sequentially: an early ventral source from the [[medial ganglionic eminence]] marked by Nkx2.1, followed by progenitor cells in the [[lateral ganglionic eminence]] marked by Gsh2, and finally the dorsal neocortical germinal zone marked by Emx1.<ref>{{Cite journal 
| last1 = Kessaris | first1 = N. 
| last2 = Fogarty | first2 = M. 
| last3 = Iannarelli | first3 = P. 
| last4 = Grist | first4 = M. 
| last5 = Wegner | first5 = M. 
| last6 = Richardson | first6 = W. D. 
| doi = 10.1038/nn1620 
| title = Competing waves of oligodendrocytes in the forebrain and postnatal elimination of an embryonic lineage 
| journal = Nature Neuroscience 
| volume = 9 
| issue = 2 
| pages = 173–179 
| year = 2005 
| pmid = 16388308 
| pmc = 
}}</ref>  After the committed progenitor cells exit the germinal zones, they begin to express NG2 and Pdgfra and expand by local proliferation and migration and eventually occupy the entire CNS parenchyma.  Polydendrocytes continue to exist in the adult CNS and retain their proliferative ability throughout life.

=== Fate of polydendrocytes ===
The fate of polydendrocytes has been highly debated.<ref>{{Cite journal 
| last1 = Nishiyama | first1 = A. 
| last2 = Komitova | first2 = M. 
| last3 = Suzuki | first3 = R. 
| last4 = Zhu | first4 = X. 
| title = Polydendrocytes (NG2 cells): Multifunctional cells with lineage plasticity 
| doi = 10.1038/nrn2495 
| journal = Nature Reviews Neuroscience 
| volume = 10 
| issue = 1 
| pages = 9–22 
| year = 2009 
| pmid = 19096367 
| pmc = 
}}</ref>  Using [[Cre-Lox recombination]]-mediated genetic fate mapping, several labs have reported the fate of polydendrocytes using different Cre driver and reporter mouse lines;<ref>{{Cite journal 
| last1 = Zhu | first1 = X. 
| last2 = Bergles | first2 = D. E. 
| last3 = Nishiyama | first3 = A. 
| doi = 10.1242/dev.004895 
| title = NG2 cells generate both oligodendrocytes and gray matter astrocytes 
| journal = Development 
| volume = 135 
| issue = 1 
| pages = 145–157 
| year = 2007 
| pmid = 18045844 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Rivers | first1 = L. E. 
| last2 = Young | first2 = K. M. 
| last3 = Rizzi | first3 = M. 
| last4 = Jamen | first4 = F. O. 
| last5 = Psachoulia | first5 = K. 
| last6 = Wade | first6 = A. 
| last7 = Kessaris | first7 = N. 
| last8 = Richardson | first8 = W. D. 
| doi = 10.1038/nn.2220 
| title = PDGFRA/NG2 glia generate myelinating oligodendrocytes and piriform projection neurons in adult mice 
| journal = Nature Neuroscience 
| volume = 11 
| issue = 12 
| pages = 1392–1401 
| year = 2008 
| pmid = 18849983 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Dimou | first1 = L. 
| last2 = Simon | first2 = C. 
| last3 = Kirchhoff | first3 = F. 
| last4 = Takebayashi | first4 = H. 
| last5 = Gotz | first5 = M. 
| title = Progeny of Olig2-Expressing Progenitors in the Gray and White Matter of the Adult Mouse Cerebral Cortex 
| doi = 10.1523/JNEUROSCI.2831-08.2008 
| journal = Journal of Neuroscience 
| volume = 28 
| issue = 41 
| pages = 10434–10442 
| year = 2008 
| pmid = 18842903 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Kang | first1 = S. H. 
| last2 = Fukaya | first2 = M. 
| last3 = Yang | first3 = J. K. 
| last4 = Rothstein | first4 = J. D. 
| last5 = Bergles | first5 = D. E. 
| title = NG2+ CNS Glial Progenitors Remain Committed to the Oligodendrocyte Lineage in Postnatal Life and following Neurodegeneration 
| doi = 10.1016/j.neuron.2010.09.009 
| journal = Neuron 
| volume = 68 
| issue = 4 
| pages = 668–681 
| year = 2010 
| pmid = 21092857 
| pmc =2989827 
}}</ref> reviewed in reference.<ref>{{Cite journal 
| last1 = Richardson | first1 = W. D. 
| last2 = Young | first2 = K. M. 
| last3 = Tripathi | first3 = R. B. 
| last4 = McKenzie | first4 = I. 
| title = NG2-glia as Multipotent Neural Stem Cells: Fact or Fantasy? 
| doi = 10.1016/j.neuron.2011.05.013 
| journal = Neuron 
| volume = 70 
| issue = 4 
| pages = 661–673 
| year = 2011 
| pmid = 21609823 
| pmc =3119948 
}}</ref>  The consensus of these studies is that polydendrocytes generate predominantly oligodendrocytes in both gray and white matter.  The rate at which they generate oligodendrocytes declines with age and is greater in [[white matter]] than in [[gray matter]].  These studies revealed that up to 30% of the oligodendrocytes that exist in the adult [[corpus callosum]] are generated de novo from polydendrocytes over a period of 2 months.  It is not known whether all polydendrocytes eventually generate oligodendrocytes while self-renewing its population or whether some remain as polydendrocytes throughout the life of the animal and never differentiate into oligodendrocytes.

Using NG2cre mice, it was shown that polydendrocytes in the prenatal and perinatal [[gray matter]] of the ventral forebrain and spinal cord generate protoplasmic astrocytes in addition to oligodendrocytes.  However, contrary to the prediction from optic nerve cultures, polydendrocytes in [[white matter]] do not generate astrocytes.  When the oligodendrocyte transcription factor Olig2 is deleted specifically in polydendrocytes, there is a region- and age-dependent switch in the fate of polydendrocytes from oligodendrocytes to astrocytes.<ref>{{Cite journal 
| last1 = Zhu | first1 = X. 
| last2 = Zuo | first2 = H. 
| last3 = Maher | first3 = B. J. 
| last4 = Serwanski | first4 = D. R. 
| last5 = Loturco | first5 = J. J. 
| last6 = Lu | first6 = Q. R. 
| last7 = Nishiyama | first7 = A. 
| doi = 10.1242/dev.078873 
| title = Olig2-dependent developmental fate switch of NG2 cells 
| journal = Development 
| volume = 139 
| issue = 13 
| pages = 2299–2307 
| year = 2012 
| pmid = 22627280 
| pmc =3367441 
}}</ref>

Although controversy still continues about the neuronal fate of polydendrocytes, the consensus from a number of recent genetic fate mapping studies described above seems to be that polydendrocytes do not generate a significant number of neurons under normal conditions, and that they are distinct from [[neural stem cells]] that reside in the [[subventricular zone]].<ref>{{Cite journal 
| last1 = Komitova | first1 = M. 
| last2 = Zhu | first2 = X. 
| last3 = Serwanski | first3 = D. R. 
| last4 = Nishiyama | first4 = A. 
| title = NG2 cells are distinct from neurogenic cells in the postnatal mouse subventricular zone 
| doi = 10.1002/cne.21917 
| journal = The Journal of Comparative Neurology 
| volume = 512 
| issue = 5 
| pages = 702–716 
| year = 2009 
| pmid = 19058188 
| pmc =2614367 
}}</ref>

== Functions of polydendrocytes ==

=== Polydendrocytes in remyelination ===
Polydendrocytes retain the ability to proliferate in adulthood and comprise 70-90% of the proliferating cell population in the mature CNS.<ref name="Dawson2003" /><ref>{{Cite journal 
| last1 = Horner | first1 = P. J. 
| last2 = Power | first2 = A. E. 
| last3 = Kempermann | first3 = G. 
| last4 = Kuhn | first4 = H. G. 
| last5 = Palmer | first5 = T. D. 
| last6 = Winkler | first6 = J. 
| last7 = Thal | first7 = L. J. 
| last8 = Gage | first8 = F. H. 
| title = Proliferation and differentiation of progenitor cells throughout the intact adult rat spinal cord 
| journal = The Journal of neuroscience : the official journal of the Society for Neuroscience 
| volume = 20 
| issue = 6 
| pages = 2218–2228 
| year = 2000 
| pmid = 10704497
}}</ref>  Under conditions in the developing and mature CNS where a reduction in the normal number of [[oligodendrocytes]] or [[myelin]] occurs, polydendrocytes react promptly by undergoing increased [[Cell proliferation|proliferation]].  In acute or chronic demyelinated lesions created in the rodent CNS by chemical agents such as [[lysolecithin]] or cuprizone, polydendrocytes proliferate in response to demyelination, and the proliferated cells differentiate into remyelinating oligodendrocytes.<ref>{{Cite journal 
| doi = 10.1016/S0896-6273(00)80359-1 
| last1 = Gensert | first1 = J. M. 
| last2 = Goldman | first2 = J. E. 
| title = Endogenous progenitors remyelinate demyelinated axons in the adult CNS 
| journal = Neuron 
| volume = 19 
| issue = 1 
| pages = 197–203 
| year = 1997 
| pmid = 9247275
}}</ref><ref>{{Cite journal 
| last1 = Zawadzka | first1 = M. 
| last2 = Rivers | first2 = L. E. 
| last3 = Fancy | first3 = S. P. J. 
| last4 = Zhao | first4 = C. 
| last5 = Tripathi | first5 = R. 
| last6 = Jamen | first6 = F. O. 
| last7 = Young | first7 = K. 
| last8 = Goncharevich | first8 = A. 
| last9 = Pohl | first9 = H. 
| doi = 10.1016/j.stem.2010.04.002 
| last10 = Rizzi | first10 = M. 
| last11 = Rowitch | first11 = D. H. 
| last12 = Kessaris | first12 = N. 
| last13 = Suter | first13 = U. 
| last14 = Richardson | first14 = W. D. 
| last15 = Franklin | first15 = R. J. M. 
| title = CNS-Resident Glial Progenitor/Stem Cells Produce Schwann Cells as well as Oligodendrocytes during Repair of CNS Demyelination 
| journal = Cell Stem Cell 
| volume = 6 
| issue = 6 
| pages = 578–590 
| year = 2010 
| pmid = 20569695 
| pmc = 
}}</ref>  Similarly, polydendrocyte proliferation occurs in other types of injury that are accompanied by loss of myelin, such as spinal cord injury.<ref name="McTigue2001">{{Cite journal 
| last1 = McTigue | first1 = D. M. 
| last2 = Wei | first2 = P. 
| last3 = Stokes | first3 = B. T. 
| title = Proliferation of NG2-positive cells and altered oligodendrocyte numbers in the contused rat spinal cord 
| journal = The Journal of neuroscience : the official journal of the Society for Neuroscience 
| volume = 21 
| issue = 10 
| pages = 3392–3400 
| year = 2001 
| pmid = 11331369
}}</ref>

If polydendrocytes were capable of giving rise to myelinating oligodendrocytes, one would expect complete remyelination of pathologically demyelinated lesions such as those seen in [[multiple sclerosis]] (MS).  However, complete myelin regeneration is usually not observed clinically or in chronic experimental models.  Possible explanations for remyelination failure include depletion of polydendrocytes over time, failure to recruit polydendrocytes to the demyelinated lesion, and failure of recruited polydendrocytes to differentiate into mature oligodendrocytes.<ref name="McTigue2001" />

Numerous factors have been shown to regulate polydendrocyte proliferation, migration, and differentiation <ref name="McTigue2001" /><ref name="McTigue2001" /> (reviewed in <ref>{{Cite journal 
| last1 = Franklin | first1 = R. J. M. 
| title = Why does remyelination fail in multiple sclerosis? 
| doi = 10.1038/nrn917 
| journal = Nature Reviews Neuroscience 
| volume = 3 
| issue = 9 
| pages = 705–714 
| year = 2002 
| pmid = 12209119 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Peru | first1 = R. L. 
| last2 = Mandrycky | first2 = N. 
| last3 = Nait-Oumesmar | first3 = B. 
| last4 = Lu | first4 = Q. R. 
| title = Paving the Axonal Highway: From Stem Cells to Myelin Repair 
| doi = 10.1007/s12015-008-9043-z 
| journal = Stem Cell Reviews 
| volume = 4 
| issue = 4 
| pages = 304–318 
| year = 2008 
| pmid = 18759012 
| pmc = 
}}</ref><ref>{{Cite journal 
| last1 = Chong | first1 = S. Y. C. 
| last2 = Chan | first2 = J. R. 
| doi = 10.1083/jcb.200905111 
| title = Tapping into the glial reservoir: Cells committed to remaining uncommitted 
| journal = The Journal of Cell Biology 
| volume = 188 
| issue = 3 
| pages = 305–312 
| year = 2010 
| pmid = 20142420 
| pmc =2819683 
}}</ref>).  In fresh MS lesions, clusters of HNK-1+ oligodendrocytes have been observed,<ref>{{Cite journal 
| last1 = Prineas | first1 = J. W. 
| last2 = Kwon | first2 = E. E. 
| last3 = Goldenberg | first3 = P. Z. 
| last4 = Ilyas | first4 = A. A. 
| last5 = Quarles | first5 = R. H. 
| last6 = Benjamins | first6 = J. A. 
| last7 = Sprinkle | first7 = T. J. 
| title = Multiple sclerosis. Oligodendrocyte proliferation and differentiation in fresh lesions 
| journal = Laboratory Investigation 
| volume = 61 
| issue = 5 
| pages = 489–503 
| year = 1989 
| pmid = 2811298
}}</ref> which suggests that under favorable conditions polydendrocytes expand around demyelinated lesions and generate new oligodendrocytes.  In chronic MS lesions where remyelination is incomplete, there is evidence that there are oligodendrocytes with processes extending toward demyelinated axons, but they do not seem to be able to generate new myelin.<ref>{{Cite journal 
| last1 = Chang | first1 = A. 
| last2 = Tourtellotte | first2 = W. W. 
| last3 = Rudick | first3 = R. 
| last4 = Trapp | first4 = B. D. 
| title = Premyelinating Oligodendrocytes in Chronic Lesions of Multiple Sclerosis 
| doi = 10.1056/NEJMoa010994 
| journal = New England Journal of Medicine 
| volume = 346 
| issue = 3 
| pages = 165–173 
| year = 2002 
| pmid = 11796850 
| pmc = 
}}</ref>  The mechanisms that regulate differentiation of polydendrocytes into myelinating oligodendrocytes are an actively investigated area of research.

Another unanswered question is whether the polydendrocyte pool eventually becomes depleted after they are used to generate remyelinating cells.  Clonal analysis of isolated polydendrocytes in the normal mouse forebrain suggests that in the adult, most clones originating from single polydendrocytes consist of either a heterogeneous population containing both oligodendrocytes and polydendrocytes or consist exclusively of polydendrocytes, suggesting that polydendrocytes in the adult CNS are able to self-renew and are not depleted under normal conditions.<ref>{{Cite journal 
| last1 = Zhu | first1 = X. 
| last2 = Hill | first2 = R. A. 
| last3 = Dietrich | first3 = D. 
| last4 = Komitova | first4 = M. 
| last5 = Suzuki | first5 = R. 
| last6 = Nishiyama | first6 = A. 
| doi = 10.1242/dev.047951 
| title = Age-dependent fate and lineage restriction of single NG2 cells 
| journal = Development 
| volume = 138 
| issue = 4 
| pages = 745–753 
| year = 2011 
| pmid = 21266410 
| pmc =3026417 
}}</ref>  However, it is not known whether this dynamic is altered in response to demyelinating lesions.

=== Neuron-polydendrocyte interactions ===
There is substantial evidence that indicates a functional interaction between polydendrocytes and neurons.

=== Node of Ranvier ===
Polydendrocytes extend their processes to the [[nodes of Ranvier]] <ref name="Butt1999">{{Cite journal 
| last1 = Butt | first1 = A. M. 
| last2 = Duncan | first2 = A. 
| last3 = Hornby | first3 = M. F. 
| last4 = Kirvell | first4 = S. L. 
| last5 = Hunter | first5 = A. 
| last6 = Levine | first6 = J. M. 
| last7 = Berry | first7 = M. 
| doi = 10.1002/(SICI)1098-1136(199903)26:1<84::AID-GLIA9>3.0.CO;2-L 
| title = Cells expressing the NG2 antigen contact nodes of Ranvier in adult CNS white matter 
| journal = Glia 
| volume = 26 
| issue = 1 
| pages = 84–91 
| year = 1999 
| pmid = 10088675 
| pmc = 
}}</ref> and together with astrocyte processes make up the nodal glial complex.  Since the nodes of Ranvier contain a high density of voltage-dependent [[sodium channels]] and allow regenerative [[action potentials]] to be generated, it is speculated that this location allows polydendrocytes to sense and possibly respond to neuronal activity

=== Neuron-polydendrocyte synapse ===
Studies have shown that neurons form [[synapses]] with polydendrocytes in both [[gray matter]] <ref>{{Cite journal 
| last1 = Bergles | first1 = D. E. 
| last2 = Roberts | first2 = J. D. B. 
| last3 = Somogyi | first3 = P. 
| last4 = Jahr | first4 = C. E. 
| title = Glutamatergic synapses on oligodendrocyte precursor cells in the hippocampus 
| doi = 10.1038/35012083 
| journal = Nature 
| volume = 405 
| issue = 6783 
| pages = 187–191 
| year = 2000 
| pmid = 10821275 
| pmc = 
}}</ref> and [[white matter]].<ref name="Butt1999" /><ref>{{Cite journal 
| last1 = Kukley | first1 = M. 
| last2 = Capetillo-Zarate | first2 = E. 
| last3 = Dietrich | first3 = D. 
| doi = 10.1038/nn1850 
| title = Vesicular glutamate release from axons in white matter 
| journal = Nature Neuroscience 
| volume = 10 
| issue = 3 
| pages = 311–320 
| year = 2007 
| pmid = 17293860 
| pmc = 
}}</ref>  Polydendrocytes express the [[AMPA]] type [[glutamate receptors]] and GABA<sub>A</sub> receptors and undergo small membrane depolarizations when stimulated by glutamate or GABA that is vesicularly released from presynaptic terminals.  [[Electron microscopy]] revealed polydendrocyte membranes apposed to neuronal presynaptic terminals filled with [[synaptic vesicles]].  Polydendrocytes lose their ability to respond to synaptic inputs from neurons as they differentiate into mature oligodendrocytes.<ref>{{Cite journal 
| last1 = De Biase | first1 = L. M. 
| last2 = Nishiyama | first2 = A. 
| last3 = Bergles | first3 = D. E. 
| doi = 10.1523/JNEUROSCI.6000-09.2010 
| title = Excitability and Synaptic Communication within the Oligodendrocyte Lineage 
| journal = Journal of Neuroscience 
| volume = 30 
| issue = 10 
| pages = 3600–3611 
| year = 2010 
| pmid = 20219994 
| pmc =2838193 
}}</ref><ref>{{Cite journal 
| last1 = Kukley | first1 = M. 
| last2 = Nishiyama | first2 = A. 
| last3 = Dietrich | first3 = D. 
| doi = 10.1523/JNEUROSCI.0854-10.2010 
| title = The Fate of Synaptic Input to NG2 Glial Cells: Neurons Specifically Downregulate Transmitter Release onto Differentiating Oligodendroglial Cells 
| journal = Journal of Neuroscience 
| volume = 30 
| issue = 24 
| pages = 8320–8331 
| year = 2010 
| pmid = 20554883 
| pmc = 
}}</ref>

Polydendrocytes can undergo cell division while maintaining synaptic inputs from neurons.<ref>{{Cite journal 
| last1 = Kukley | first1 = M. 
| last2 = Kiladze | first2 = M. 
| last3 = Tognatta | first3 = R. 
| last4 = Hans | first4 = M. 
| last5 = Swandulla | first5 = D. 
| last6 = Schramm | first6 = J. 
| last7 = Dietrich | first7 = D. 
| doi = 10.1096/fj.07-090985 
| title = Glial cells are born with synapses 
| journal = The FASEB Journal 
| volume = 22 
| issue = 8 
| pages = 2957–2969 
| year = 2008 
| pmid = 18467596 
| pmc = 
}}</ref>  These observations suggest that cells that receive neuronal synaptic inputs and those that differentiate into oligodendrocytes are not mutually exclusive cell populations but that the same population of polydendrocytes can receive synaptic inputs and generate myelinating oligodendrocytes.  The functional significance of the neuron-polydendrocyte synapses remains to be elucidated.

==See also==
* {{portal-inline|Neuroscience}}

== References ==
{{Reflist}}

[[Category:Glial cells]]