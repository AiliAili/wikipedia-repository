<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name= CF-103
  |image= File:CF 103 Mockup.jpg
  |caption= CF-103 mock-up at the Avro company
}}{{Infobox Aircraft Type
  |type= [[Interceptor aircraft|Interceptor]]
  |manufacturer= [[Avro Canada|Avro Aircraft Limited (Canada)]]
  |designer=
  |first flight=
  |introduction=
  |retired=
  |status= Cancelled December 1951
  |primary user= [[Royal Canadian Air Force]] (intended)
  |more users=
  |produced=
  |number built= 0
  |unit cost=
  |developed from=[[Avro Canada CF-100]]
  |variants with their own articles=
}}
|}

The '''Avro Canada CF-103''' was a proposed Canadian [[Interceptor aircraft|interceptor]], designed by [[Avro Canada]] in the early 1950s as a development, and possible replacement of the company's [[CF-100 Canuck]], that was entering service at the time with the [[Royal Canadian Air Force]] (RCAF).<ref name="60 Years">Milberry 1984, p. 317.</ref> Although intended to be capable of flying at [[transonic]] speeds, the CF-103 only proffered a moderate increase in performance and capability over the CF-100; subsequently, the aircraft never progressed beyond the mock-up stage.<ref name="Milberry p. 48"/>

==Design and development==
Even before the prototype of the CF-100 had flown, Avro Canada was conducting studies of potential advanced variations of the aircraft,<ref name="Arrow"/> as the RCAF was seeking an interceptor with greater high-speed performance.<ref name="Giant">Campagna 2003, p. 55.</ref> Due to the perceived limitations of the CF-100's original "thick", straight wing, Chief Designer [[John Carver Meadows Frost|John Frost]] proposed a series of refinements that included a thinner [[swept wing]].<ref name="Giant"/> In December 1950, the [[Avro Canada|Avro Aircraft]] Design Office decided to proceed with a redesign, primarily incorporating the early series CF-100 fuselage structure with a new swept wing and [[empennage|tail]] surfaces as part of the '''C-100S''' design study.<ref name="Arrow"/>{{#tag:ref|After being brought into the [[CF-100 Canuck|XC-100]] project at a late stage, Frost considerably "cleaned up" the original design but realized the "rudimentary" wing/engine [[planform (aeronautics)|planform]] limited the design to [[subsonic aircraft|subsonic]] performance levels. Consequently, he began a further redesign based on his independent study of more advanced swept-wing designs.<ref name="Zuk p. 28">Zuk 2001, p. 28.</ref>|group=N}}

Frost considered the new design as an interim aircraft between the CF-100 and the more advanced '''C-104''' project.<ref name="Arrow">Page et al. 2004, p. 11.</ref> The salient changes to the basic wing planform were in decreasing its [[Chord (aircraft)|chord]] and thickness, and adding a 42° sweep to the leading edge, creating a near-[[delta wing]] configuration. The tail surfaces were also swept back.<ref name="Milberry p. 49">Milberry 1981, p. 49.</ref>  One version that was considered featured two streamlined fuel tanks blended into the leading edge of the wings near the three/quarter position.<ref name="Valiquette p. 74">Valiquette 2009, p. 74.</ref> {{#tag:ref|Scale models of the proposed CF-103 indicated a more streamlined wing/engine section that incorporated a thick wing root blending the engines into the top of the fuselage. Pinion (outboard wing) fuel tanks and a mildly swept tail were also featured.<ref name="Valiquette p. 74"/>|group=N}} Despite the use of more powerful engines, the redesign had very modest performance specifications, with a planned maximum diving speed of [[Mach number|Mach]] 0.95, scarcely better than the [[placard]]ed Mach 0.85 speed limit of the production CF-100 Mk 2 and Mk 3.<ref name="Milberry p. 48">Milberry 1981, p. 48.</ref> Avro executives, recognizing that the company had already suffered due to the protracted development of the CF-100, determined that Frost's revised design would provide a "hedge" against the CF-100's failure to secure long-term contracts.<ref name="Valiquette p. 74"/>

In 1951, the [[Industry Canada|Canadian Department of Trade and Commerce]] issued an order for two prototypes and a static test airframe, under the CF-103 project designation. Jigs, tools and detailed engineering drawings were in place by June 1951, with wind tunnel testing, conducted at [[Cornell University]], completed by November 1951.<ref name="Arrow"/> Although a wooden [[mock-up]] of the CF-103 was built, along with a separate cockpit area and engine section that was partially framed in, the mock-up did not feature an undercarriage unit nor any interior fittings.<ref name="Page p. 70">Page 1981, p. 70.</ref> Two different tail designs were fitted with the initial effort only having a swept leading edge of the tail, while the definitive version had a much more raked appearance.<ref name="Valiquette p. 74"/> The engineering and installation requirements for the CF-103's proposed [[Avro Canada Orenda|Orenda 17]] jet engines were not finalized, as the experimental "hybrid" using an Orenda 8 compressor unit and Orenda 11 two-stage turbine, matched to a "[[afterburner|reheat]]" unit, had not been fully developed.{{#tag:ref|Although an Orenda TR7 was first contemplated, the more powerful Orenda 17 encountered recurrent problems in test flying with surging and asymmetrical thrust that were never resolved.<ref name="Page p. 68">Page 1981, p. 68.</ref>|group=N}}

==Cancellation==
During 1951, flight tests carried out by Chief Development Test Pilot S/L [[Janusz Żurakowski]] and other members of the Flight Test unit,  revealed the development potential of the CF-100 had outstripped the intended performance envelope of the CF-103, while Frost and the Design Office became preoccupied with more sophisticated designs as potential replacements for the CF-100.<ref name="Zuk p. 28"/>  {{#tag:ref|Although part of the "chain" from the CF-100S/'''CF-100D''' leading to the [[supersonic]] C-104 Advanced Fighter study, the CF-103 was obsolescent and proved to be a "dead end."<ref name="Arrow1"/> The C-104 as the '''C-104/2''' was destined to morph into the later '''[[CF-105 Arrow|Avro CF-105 Arrow]]'''.<ref name="Arrow"/>|group=N}} Work on the CF-103 stalled, with the maiden flight originally scheduled for the summer of 1952, postponed to mid-1953.<ref name="Arrow"/> With [[Cold War]] pressures mounting, the Canadian government demanded that production of the latest CF-100 fighter, as well as developing more advanced variants of the Canuck should predominate, leading the Avro company to curtail the moribund CF-103 project in December 1951.<ref name="60 Years">Milberry 1984, p. 317.</ref> {{#tag:ref|Canada had made a military commitment to enter the [[Korean War]] with the [[Department of National Defence (Canada)|Department of National Defence]] mobilizing to provide a credible RCAF presence, including fielding a front line fighter squadron; ultimately, only an [[Royal Canadian Air Force|Air Transport Command]] unit was deployed. With Avro struggling with the CF-100's wing spar problems that had led to the RCAF rejecting the first series of pre-production aircraft, the Canadian government was justifiably concerned that the company was diverting its energies. Along with the cancellation of the CF-103, Avro's other high-profile project, the [[Avro Canada C102 Jetliner]] was similarly affected, with production of the airliner being halted.<ref name>Milberry 1984, p. 258.</ref>|group=N}}

Although the mock-up languished in the experimental bay at the factory, a dramatic event served to preclude any attempt to restart the project.<ref>Milberry 1981, pp. 48–49.</ref> On 18 December 1952, from a height of 33,000&nbsp;ft (10,000 m), Żurakowski dived the CF-100 Mk 4 prototype (RCAF Serial No. 18112) to Mach 1.06.<ref name="Milberry p. 49"/> His "unauthorized" test flight{{#tag:ref|Żurakowski had permission to explore the maximum speed of the aircraft, although along with his direct superiors in the Flight Test office, all were unaware that the Special Projects Group of the Design Office had been concurrently promoting the CF-103 project to the RCAF.<ref>Stewart 1991, p. 228.</ref>|group=N}} resulted in the final scrapping of the mock-up.<ref>Zuk 2004, p. 184.</ref>

==Specifications==
[[File:Avro Canada CF-103 drawing.jpg|thumb|CF-103: original concept, c. 1950]]
{{aircraft specifications
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet

<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses).
If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Airtemp]]. -->
|ref=''Avro Arrow: The Story of the Avro Arrow from its Evolution to its Extinction'' <ref name="Arrow1">Page et al. 2004, p. 12.</ref>
|crew=2
|capacity=
|length main=59 ft 9 in
|length alt=18.2 m
|span main=43 ft
|span alt=13.1 m
|height main=16 ft
|height alt=4.87 m
|area main=
|area alt=
|airfoil=
|empty weight main=
|empty weight alt=
|loaded weight main=
|loaded weight alt=
|max takeoff weight main=
|max takeoff weight alt=
|engine (jet)=[[Avro Canada Orenda|Orenda 17]]
|type of jet=[[turbojet]]s
|number of jets=2
|thrust main=7,275 [[pound-force|lbf]]
|thrust alt=34.36 kN
|afterburning thrust main=8,490 [[pound-force|lbf]]
|afterburning thrust alt=37.8 kN
|max speed main=[[mach number|Mach]] 0.85 or 647 mph (1,039 km/h) (Mach 0.95 in dive)
|max speed alt=
|max speed more=
|cruise speed main=
|cruise speed alt=
|cruise speed more=
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|combat radius main=
|combat radius alt=
|ceiling main=
|climb rate main=
|climb rate alt=
|loading main=
|loading alt=
|thrust/weight=
|armament= Proposed
* Forward-firing ventral gun pack containing eight [[M2 Browning machine gun#AN/M2 and AN/M3|.5-inch Browning M3]] [[machine guns]] (200 rounds per machine gun)
|rockets=
|missiles=
|avionics=
}}

==See also==
{{Portal|Aviation|Canadian Armed Forces}}
{{aircontent
|see also=
* [[CF-105 Arrow]]
|related=
* [[CF-100 Canuck]]
|similar aircraft=   <!--twin-engine, swept-wing, all-weather interceptors -->
* [[Gloster Javelin]]
* [[Mikoyan-Gurevich MiG-19]]PF
* [[Sud-Ouest Vautour]]
* [[Yakovlev Yak-25]]
* [[Yakovlev Yak-28|Yakovlev Yak-28P/PM]]
|lists=
* [[List of aircraft of the Canadian Air Force]]
 }}

==References==
;Notes
{{reflist|group=N}}
;Citations
{{reflist|2}}
;Bibliography
{{refbegin}}
* Campagna, Palmiro. ''Requiem for a Giant: A.V. Roe Canada and the Avro Arrow''. Toronto: Dundurn Press, 2003. ISBN 1-55002-438-8.
* [[Larry Milberry|Milberry, Larry]]. ''The Avro CF-100''. Toronto: CANAV Books, 1981. ISBN 0-9690703-0-6.
* Milberry, Larry. ''Sixty Years: The RCAF and CF Air Command 1924-1984''. Toronto: CANAV Books, 1984. ISBN 0-9690703-4-9.
* Page, Ron. ''Canuck: CF-100 All Weather Fighter''. Erin, Ontario: Boston Mills Press, 1981. ISBN 0-919822-39-8.
* Page, Ron, Richard Organ, Don Watson and Les Wilkinson ("The Arrowheads"). ''Avro Arrow: The Story of the Avro Arrow from its Evolution to its Extinction''. Erin, Ontario: Boston Mills Press, 1979, reprinted Stoddart, 2004. ISBN 1-55046-047-1.
* Stewart, Greig. ''Shutting Down the National Dream: A.V. Roe and the Tragedy of the Avro Arrow''. Toronto: McGraw-Hill-Ryerson, 1991. ISBN 0-07-551119-3.
* Valiquette, Marc-Andre. ''Destruction of a Dream: The Tragedy of Avro Canada and the CF-105 Arrow, Volume 1''. Montreal: Marc-Andre Valiquette (self-published), 2009. ISBN 978-2-9811239-0-9.
* Zuk, Bill. ''Avrocar, Canada's Flying Saucer: The Story Of Avro Canada's Secret Projects''. Erin, Ontario: Boston Mills Press, 2001. ISBN 1-55046-359-4.
* Zuk, Bill. ''Janusz Zurakowski: Legends in the Sky.'' St. Catharine's, Ontario: Vanwell, 2004. ISBN 1-55125-083-7.
{{refend}}

==External links==
{{commons category|Avro Canada}}
* [http://www.globalsecurity.org/military/world/canada/cf-103.htm CF-103]
* [http://canuck.purpleglen.com/cf103.html "CF-103 Canuck II" (sic)]

{{Avro Canada}}

{{Good article}}

{{DEFAULTSORT:Avro Canada Cf-103}}
[[Category:Abandoned military aircraft projects of Canada]]
[[Category:Canadian fighter aircraft 1950–1959]]
[[Category:Avro Canada aircraft|CF-103]]
[[Category:Twinjets]]
[[Category:Canadian inventions]]