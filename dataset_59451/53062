{{refimprove|date=November 2009}}
{{Infobox journal
| title = Trinity College Law Review
| cover =
| editor = Senior Editorial Board
| language = English, Irish, French, German
| discipline = [[Law review]]
| abbreviation =
| publisher = Dublin University Law Society
| country = Ireland
| frequency = Annually
| history = 1998-present
| openaccess =
| license =
| impact = 
| impact-year = 
| website = http://www.trinitycollegelawreview.org/
| link1 =  http://www.trinitycollegelawreview.org/volumes/current.htm
| link1-name = Online tables of contents
| link2 =
| link2-name =
| JSTOR = 
| OCLC  = 41254523
| LCCN  = 2003250062
| CODEN = 
| ISSN  = 1393-5941
| eISSN = 
}}
The '''''Trinity College Law Review''''' (TCLR) is a student-run [[law review]] affiliated with [[Trinity College, Dublin|Trinity College Dublin]] School of Law. It is the oldest Irish student edited law journal, published annually every March since 1998. The review publishes selected submissions in English, French, or German dealing with any area of law. Its [[Bluebook]] abbreviation is ''Trinity C.L. Rev.''

Previous contributors to the review have included [[Mary McAleese]], Brian Walsh, [[Susan Denham]], [[Pat Cox]], Gerard Hogan, Fergus Ryan, Oran Doyle, Desmond Ryan, and Raymond Ryan.

The TCLR is available on two online legal databases: [[HeinOnline]] and [[Westlaw]].

== TCLR events ==
A "Distinguished Speaker Series" was established in 2006. The 2009 discussion was on "The Law's Response to Gangland Crime in Ireland". Panellists included James Hamilton ([[Director of public prosecutions#Ireland|Director of Public Prosecutions]]), Carol Coulter ([[Irish Times]]), Senator [[Ivana Bacik]], and criminal law solicitors Michael Finucane and Petter Mullan.<ref>{{cite news |url=http://www.irishtimes.com/newspaper/ireland/2009/0113/1231738221238.html |title=DPP says no 'quick fixes' to gangland crime |journal=[[Irish Times]] |date=2009-01-13 |accessdate=2009-10-28}}</ref>
More recently topics of discussion have included "Is the Seanad Worth Saving?" (2011), "The Fusion of the Legal Professions" (2012) and "The Future of the Irish Corporate Tax Regime" (2014).

The TCLR also hosts an annual "Authors' Night", where speakers such as law professors and editors of prominent law journals discuss legal writing strategies and provide advice to students who are considering entering submissions for the publication.

== Awards ==
A number of awards are available for those who enter submissions to the TCLR. 
* In November 2009 "The Inaugural Gernot Biehler Memorial Competition" was launched. This is a casenote competition in honour of [[Gernot Biehler]], lecturer in International Law and Conflicts of Law and Fellow of Trinity College Dublin, who died in September 2009. This competition is open to all first and second year law students in any university. The prize for the best casenote is €250.<ref>http://trinitycollegelawreview.wordpress.com/submissions/</ref>
* The "Reddy Charlton Prize" is awarded to the best article published in the review each year.
* The "Norton Rose Fulbright Internship Prize" is awarded to the best article on a topic of commercial law.
* The "FLAC Award" is awarded by Trinity FLAC to the best article on a topic of human rights or social justice.
* The "Conseil d'État Prize" is awarded to the best French article each year. This comes in the form of a summer internship at the [[Council of State (France)|Conseil d'État]].
* The "German Language Prize" is awarded to the best German article and this also comes in the form of an internship with a leading law firm in Germany.

== Editorial board ==
The [[editorial board]] of the ''Trinity College Law Review'' consists of undergraduate and postgraduate law students of [[Trinity College Dublin]]. Selection to the board is merit-based and highly competitive. Responsible for the selection and editing of the articles published in the review, the board also works to promote legal writing in Trinity College through holding workshops and guest lectures throughout the year. The current [[editor-in-chief]] is Feidhlim Mac Róibín.

==Notable Alumni==
Prominent past editorial board members of the ''Trinity College Law Review'' include: Oran Doyle, constitutional law scholar and Head of Trinity College Dublin Law School<ref>https://www.tcd.ie/Law/orandoyle/</ref> and James Nix, Director of Policy & Operations with An Taisce: The National Trust for Ireland.<ref>{{cite web|title=An Taisce The National Trust for Ireland Annual Report 2012|url=http://www.antaisce.org/sites/antaisce.org/files/2013-05-an_taisce-2012_annaul_report.pdf|website=An Taisce|accessdate=3 March 2015|page=4|date=2013}}</ref>

==Highly Cited Articles==
* Stephen Ranalow, "Bearing a Constitutional Cross: Examining Blasphemy and the Judicial Role in ''Corway v Independent Newspapers''" [2000] 3 ''Trinity CL Rev'' 95.
* Michael Kearney, "Extraterritorial Jurisdiction of the European Convention on Human Rights" [2002] 5 ''Trinity CL Rev'' 158.

==References==
{{Reflist}}

==External links==
* {{Official website|http://www.trinitycollegelawreview.org/}}

{{Trinity College, Dublin}}

[[Category:Trinity College, Dublin]]
[[Category:Irish law journals]]
[[Category:Publications established in 1998]]
[[Category:Annual journals]]
[[Category:Multilingual journals]]