{|{{Infobox Aircraft Begin
  |name = Deux-Ponts / Provence / Sahara / Universale
  |image = File:Air France Breguet 763 Provence Manteufel-1.jpg
  |caption =[[Air France]] Provence at [[Tempelhof Airport]] in 1965
}}{{Infobox Aircraft Type
  |type = Airliner and freighter
  |manufacturer = [[Breguet Aviation]]
  |designer = 
  |first flight = 15 February 1949
  |introduced = 10 March 1953
  |retired = 31 March 1971
  |status = 
  |primary user = [[Air France]]
  |more users = [[French Air Force]]
  |produced = 
  |number built = 20
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}

The '''Breguet 761/763/765''' were a family of 1940s and 1950s French [[double-deck aircraft|double-deck]] transport aircraft produced by [[Breguet Aviation]]. The aircraft were normally called the '''Deux-Ponts''' (Double-Decker) but it was not an official name.

==Design and development==
[[File:Breguet BR761S No. 03 LeB 29.05.57 edited-2.jpg|thumb|right|The third Breguet Br.761S at the 1957 [[Paris Air Show]] fitted with the early central fin shape]]
[[File:Breguet 765 Sahara (MAA).jpg|thumb|alt=A Breguet 765 Sahara being restored by the Musée des Ailes Anciennes in Toulouse, France|Breguet 765 Sahara under restoration.]]

Breguet began design work on the '''Breguet 761''' double-deck [[airliner]] even before the end of the [[World War II|Second World War]], in [[1944 in aviation|1944]]. It was decided that a medium-range airliner with seating for over 100 passengers would be built. The design envisaged using readily available engines with the aim of ease of manufacture and an early first-flight date. The design was known as ''Project 76-1''. The aircraft was destined not to be the first French postwar design to fly, an honour which instead fell to the [[Sud-Est Languedoc]], a civilianised Bloch MB161. The [[prototype]] Br.761, F-WASK, first flew at [[Villacoublay]] on 15 February 1949.<ref name=Aeroplane>{{cite magazine |last1=Hirst |first1=Mike |year=2009 |title=Double-Decker Déja-vu |magazine=Aeroplane |volume= |issue=December 2009 |pages=72–76 |url= |doi= }}</ref>

The 761 featured a [[cantilever]] wing set at mid-height on the bulky [[fuselage]]. The retractable tricycle landing gear featured dual-wheel main units. The [[empennage]] had twin [[fin]]s and [[rudder]]s on a vestigial central fin.<ref name="JAWA53 p128-9"/>  The prototype was powered by four 1,580&nbsp;hp (1180&nbsp;kW) [[SNECMA 14R-24]] [[radial engine]]s.<ref name="Flight53p458">Stroud ''Flight'' 10 April 1953, p. 458.</ref> The Breguets serving with Air France had up to 107 seats and an elevator between the two floors.{{cn|date=August 2016}}

The prototype was followed by three '''Br.761S''' pre-production aircraft powered by 2,020&nbsp;hp (1506&nbsp;kW) [[Pratt & Whitney R-2800|Pratt & Whitney R-2800-B31]] radial engines. These were fitted with 12&nbsp;ft 1½in (3.70 m) diameter [[Hamilton Standard]] propellers. The aircraft successfully completed their trials incident-free. Their first flights were in 1951 and 1952.<ref name=Aeroplane/>

The [[Cabinet of France|French Government]] ordered 12 production aircraft, the Breguet 76-3, which was later redesignated Br.763. Six aircraft were to be operated by [[Air France]] and the other six by the [[Minister of Transport (France)|Ministry of Transport]]. The 763 had more powerful engines, a {{convert|1.20|m|ftin}} larger wingspan, strengthened wings and a three-crew [[Cockpit|flight deck]] (earlier aircraft had four crew).  The 763 first flew on 20 July 1951 and entered service with Air France during autumn 1952.<ref name=Aeroplane/>

The Air France aircraft had accommodation for 59 passengers on the top deck, and 48 on the lower deck, although the aircraft was capable of carrying 135 passengers in a high-density layout.<ref name=Aeroplane/> During 1964 Air France transferred six Br.763s to the [[French Air Force]].  The air force also acquired the three pre-production Br.761S aircraft and four new '''Br.765 Sahara''' freighter aircraft with removable cargo doors.

Projects to build versions powered with British engines (for possible United Kingdom buyers) did not come to fruition.  The projects would have been the '''766''' (with the [[Bristol Hercules]] [[radial engine]]), and the '''767''' with British [[turboprop]] engines.

==Operational history==

===Civil===
The prototype Br.761 entered service with [[Air Algérie]] in 1952 as a cargo aircraft. It was withdrawn early the next year. [[Silver City Airways]] leased a Br.761 for three months in the summer of [[1953 in aviation|1953]] for use on the [[Hamburg Airport|Hamburg]] - [[Berlin Tempelhof Airport|Berlin]] route. A total of 127 round trips carried {{convert|4000000|lb|t}} of freight with up to three round trips being made in a day, each leg taking 52 minutes flight time. It was rumoured that Silver City would purchase three aircraft at £770,000 but this did not materialise into a sale.<ref name=Aeroplane/>

The Breguet Br.763 ''Provence'' entered service with Air France on 10 March 1953. The inaugural route was [[Lyon-Saint Exupéry Airport|Lyon]] - [[Houari Boumediene Airport|Algiers]]. The type was used on European routes from [[Paris-Charles de Gaulle Airport|Paris]], mainly to the Mediterranean area, but occasionally to [[London Heathrow Airport|London]]. Domestic routes included Paris to Lyon, [[Marseille Provence Airport|Marseille]] and [[Nice Côte d'Azur Airport|Nice]].<ref name=Aeroplane/>

Six aircraft were used in response to a serious incident at [[Salat, Algeria]] where French oil rig engineers were in need of assistance. A total of 60 tonnes of heavy equipment and 200 personnel were moved to and from Algiers in four days.<ref name=Aeroplane/>

The introduction of the [[Sud Aviation Caravelle]] rendered the ''Provence'' obsolete as a passenger aircraft. The Caravelle was faster, more comfortable and had a greater range. In [[1958 in aviation|1958]], Breguet borrowed F-BASQ from Air France for a sales tour to North and South America. This was the aircraft which had force-landed at [[Pont-Évêque]] in 1955. The tour covered {{convert|25000|mi|km}}, and took in the cities of [[New York City|New York]], Washington, D.C. and [[Miami]] in the United States, [[Bogotá]] in [[Colombia]], [[Santiago, Chile|Santiago]] in [[Chile]], [[Rio de Janeiro]] and [[Brasilia]] in Brazil. The tour failed to generate any orders. In North America, the [[jet age]] had begun, while the aircraft had too great a capacity for operators in South America, despite being cheaper on a cost-per-seat to operate than a [[Douglas DC-4]]. The ''Provence'' was used on fewer and fewer passenger services, being replaced by the Caravelle and [[Vickers Viscount]]. Six aircraft were transferred to the Armée de l'Air. Air France converted the six remaining Br.763s into freighters with the name ''Universale''. These remained in service on European freight services until the early 1970s. The final flight was on 31 March 1971 from [[Heathrow]] to [[Paris-Orly Airport|Paris-Orly]]. A double-deck [[AEC Routemaster]] bus was parked alongside the aircraft to mark the retirement of the Br.763 from service.<ref name=Aeroplane/>

===Military===
[[File:Breguet BR.765 No. 1 64-PE LEB 11.06.77 edited-2.jpg|thumb|right|Breguet BR.765 of the French Air Force]]
In October 1955 an order for 30 Breguet Br.765 ''Sahara'' aircraft for the [[Armée de l'Air]] was announced. This order was cancelled by the end of the year, but construction on four aircraft was so far advanced that they were completed. These entered service with 64 ''Escadre de Transport''.<ref name=Aeroplane/>

The French Air Force acquired the three pre-production Br.761S aircraft, these and the six Sahara aircraft acquired from Air France provided the French Air Force with a valuable transport fleet for moving personnel and materials to the [[France and nuclear weapons|Pacific nuclear testing areas]].  The Sahara fleet was retired in 1972.

==Accidents and incidents==
The Breguet Deux-Ponts had an excellent safety record.
*On 10 May 1955, F-BASQ of Air France made a forced landing in a field at [[Pont-Évêque]], [[Isère]] following directional control problems in flight. The four crew and 46 passengers were unharmed. The aircraft was repaired on site and flown out from an improvised airstrip a few weeks later.<ref name=Aeroplane/>

==Variants==
;Breguet 761
:Prototype with four {{convert|1590|hp|kW|abbr=on|disp=flip}} [[SNECMA 14R-24]] radial engines, one built.

;Breguet 761S
:Pre-production aircraft, powered by four {{convert|2100|hp|kW|abbr=on|disp=flip}} [[Pratt & Whitney R-2800-B31]] engines; three built.

;Breguet 763 Provence
:Production aircraft for Air France, powered by four {{convert|2400|hp|kW|abbr=on|disp=flip}} [[Pratt & Whitney R-2800-CA18]] engines; 12 built.

;Breguet 764
:Proposed anti-submarine naval version, prototype 761 was to be converted but project was abandoned.

;Breguet 765 Sahara
:Freighter version for the French Air Force, powered by four {{convert|2500|hp|kW|abbr=on|disp=flip}} [[Pratt & Whitney R-2800-CB17]] engines; four built.

==Operators==

;{{FRA}}
:[[Air Algérie]] (loaned for trials 1952 _ French flag - Algeria being at this time a french territory)

;{{FRA}}
:[[French Air Force]]
:[[Air France]]
:[[French Navy]]

;{{UK}}
:[[Silver City (airline)|Silver City]] (leased 1953)

==Specifications (Br.763)==
[[File:Br-673.svg|thumb|3 line drawing]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref= Jane's All The World's Aircraft 1953–54<ref name="JAWA53 p128-9">Bridgman 1953, pp. 128–129.</ref>
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|crew=three
|capacity=107 passengers
|length main= 28.94 m
|length alt= 94 ft 11 in
|span main=42.96 m
|span alt=140 ft 11 in
|height main=9.56 m
|height alt= 31 ft 4 in
|area main= 185.4 m²
|area alt= 1,996 sq ft
|aspect ratio=9.95:1
|empty weight main= 32,535 kg
|empty weight alt= 71,577 lb
|loaded weight main= 
|loaded weight alt= 
|max takeoff weight main= 50,000 kg
|max takeoff weight alt= 111,000 lb
|engine (prop)=[[Pratt & Whitney R-2800-CA18]]
|type of prop= radial piston
|number of props=4
|power main= 2,400 hp
|power alt=1790 kW
|max speed main= 390 km/h
|max speed alt=210 knots, 242 mph
|max speed more=at 3,000 m (10,000 ft) (max cruise)
|cruise speed main=336 km/h
|cruise speed alt=183 knots, 210 mph
|cruise speed more=(econ cruise)
|range main= 2,290 km
|range alt= 1,243 [[nautical mile|nmi]], 1,430 mi
|ceiling main= 
|ceiling alt= 
|climb rate main=5.8 m/s
|climb rate alt=1,140 ft/min
|climb rate more= at sea level
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament= 
}}

==Survivors==
[[File:Breguet 763 Fontenay.jpg|thumb|F-BASS at Fontenay-Trésigny]]
* Br-763 ''Provence'' c/n 6, F-BASS of Air France is used as restaurant at [[Fontenay-Trésigny]], France
* Br.765 ''Sahara'' c/n 501, 64-PE of the Armée de l'Air is preserved as a gate guard at [[Évreux-Fauville Air Base]], France.<ref name=Guard>[http://www.airliners.net/photo/France-Air-Force/Breguet-765-Sahara/1796202 Photo]</ref>
* Br.765 ''Sahara'' c/n 504, 64-PH of the Armée de l'Air is under restoration at [[Ailes Anciennes Toulouse]], France.<ref name=restore>[http://www.airliners.net/photo/Untitled/Breguet-765-Sahara/2694051 Photo]</ref>

==See also==
{{aircontent
|related=
|similar aircraft=
*[[C-97 Stratofreighter]]
|sequence=
|lists=
|see also=
}}

==References==
;Notes
{{Reflist}}
;Bibliography

*{{cite book |last= Bridgman|first= Leonard|authorlink=|title=Jane's All The World's Aircraft 1953–54|year=1953 |publisher=Jane's All The World's Aircraft Publishing Company|location=London }}
*{{cite magazine |last= Stroud|first=John |title=The Double-Decker Provence|magazine= [[Flight International|Flight]]|volume= |issue=10 April 1953 |pages=458–461   |url=http://www.flightglobal.com/pdfarchive/view/1953/1953%20-%200462.html |accessdate= }}
*{{cite book |last= |first= |authorlink= |coauthors= |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=}}
*{{cite book |last= |first= |authorlink= |coauthors= |title= French Post-War Transport Aircraft|year= 1980|publisher= Air-Britain|location= Tonbridge, England|isbn=0-85130-078-2}}

==External links==
{{Commons category|Breguet Deux-Ponts}}
*[http://www.aeroweb-fr.net/appareils/fiche,8,breguet-765-deux-ponts.php Technical data and photos (French)]

{{Breguet aircraft}}

[[Category:Breguet aircraft|Deux-Ponts]]
[[Category:French airliners 1940–1949]]
[[Category:French military transport aircraft 1940–1949]]
[[Category:Four-engined tractor aircraft]]
[[Category:Monoplanes]]