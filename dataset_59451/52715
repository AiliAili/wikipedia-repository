{{Infobox journal
| title = Radiophysics and Quantum Electronics
| cover = 
| editor = Vladimir V. Zheleznyakov
| discipline = [[Radiophysics]], [[quantum electronics]]
| former_names =
| abbreviation = Radiophys. Quantum Electron.
| publisher = [[Springer Science+Business Media]]
| country = 
| frequency = Monthly
| history = 1958-present
| openaccess = 
| license = 
| impact = 0.955
| impact-year = 2012
| website = http://www.springer.com/astronomy/astronomy,+observations+and+techniques/journal/11141
| link1 = http://link.springer.com/journal/volumesAndIssues/11141
| link1-name = Online archive
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 47189961
| LCCN = 75643512
| CODEN = RPQEAC
| ISSN = 0033-8443
| eISSN = 1573-9120
}}
'''''Radiophysics and Quantum Electronics''''' is a monthly [[peer-reviewed]] [[scientific journal]] covering all aspects of [[radiophysics]] and [[quantum electronics]]. It is the English translation of the Russian journal ''IIzvestiya VUZ. Radiofizika''. It is published by [[Springer Science+Business Media]] on behalf of the [[Radiophysical Research Institute]] ([[Ministry of Education and Science (Russia)|Russian Ministry of Education and Science]]) and the [[N. I. Lobachevsky State University of Nizhny Novgorod]]. The journal was established in 1958 and the [[editor-in-chief]] is Vladimir V. Zheleznyakov.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index Expanded]]
* [[Scopus]]
* [[Inspec]]
* [[Astrophysics Data System]]
* [[EBSCO Information Services|EBSCO databases]]
* [[Academic OneFile]]
* [[Academic Search]]
* [[EI-Compendex]]
* [[INIS Atomindex]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.955.<ref name=WoS>{{cite book |year=2013 |chapter=Radiophysics and Quantum Electronics |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://www.springer.com/astronomy/astronomy,+observations+and+techniques/journal/11141}}

[[Category:Springer Science+Business Media academic journals]]
[[Category:Physics journals]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1958]]