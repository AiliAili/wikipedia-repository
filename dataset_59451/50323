{{Infobox journal
| title         = Academic Emergency Medicine 
| cover         = 
| editor        = 
| discipline    = [[Emergency medicine]]
| peer-reviewed = 
| language      = 
| former_names  = 
| abbreviation  = Acad Emerg Med
| publisher     = [[John Wiley & Sons|Wiley]]
| country       = United States
| frequency     = Monthly
| history       = 1994-present
| openaccess    = 
| license       = 
| impact        = 2.5
| impact-year   = 2015
| website       = http://www.saem.org/publications/aem-journal
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 45268302 
| LCCN          = sn93006121
| CODEN         = AEMEF5
| ISSN          = 1069-6563 
| eISSN         = 1553-2712
| boxwidth      = 
}}
'''''Academic Emergency Medicine''''' is a monthly [[peer reviewed]] [[medical journal]] published by [[John Wiley & Sons|Wiley]] on behalf of the [[Society for Academic Emergency Medicine]]. The [[editor in chief]] is Jeffrey A. Kline, MD. Coverage includes [[basic science]], [[clinical research]], education information, and [[clinical practice]] related to [[emergency medicine]]. 

== Abstracting and indexing ==
This journal is indexed by the following services:<ref>
{{cite web
 | last = Catalog entry
 | title = Academic emergency medicine
 | publisher = NLM Catalog
 | url =http://www.ncbi.nlm.nih.gov/nlmcatalog/?term=1553-2712
 | accessdate = 8 April 2014}} NLM ID 9418450</ref>

* [[Current Contents]]/ Clinical Medicine
* [[Journal Citation Reports]]/Science Edition 
*  Research Alert (Thomson Reuters) 
* [[Science Citation Index]]
* [[Abstracts in Anthropology]]
* [[Embase]] 
* [[MEDLINE]]/[[Index Medicus]]

According to [[ResearchGate]], the journal's 2013 impact factor is 1.76.<ref>
{{cite web
 | last = 2013 Impact Factor
 | title =Academic Emergency Medicine
 | publisher =ResearchGate
 | url = http://www.researchgate.net/journal/1553-2712_Academic_Emergency_Medicine
 | format = Online
 | accessdate =8 April 2014}}</ref>

== External links ==
* {{Official website|http://www.saem.org/publications/aem-journal}}

== References ==
{{reflist}}

[[Category:Emergency medicine journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1994]]
[[Category:English-language journals]]