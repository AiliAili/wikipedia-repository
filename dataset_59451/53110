{{Infobox company
| name             = Walter de Gruyter GmbH
| logo             = Verlag Walter de Gruyter Logo.svg
| caption          =
| native_name      = <!-- Company's name in home country language -->
| native_name_lang = de
| type             = [[Gesellschaft mit beschränkter Haftung|Limited liability company]] <br />(''Gesellschaft mit beschränkter Haftung'')
| traded_as        = 
| ISIN             = 
| industry         = 
| fate             = 
| predecessor      = 
| successor        = 
| foundation       = {{start date and age|1749}}
| founder          = 
| defunct          = 
| location_city    = [[Germany]], [[Berlin]]
| location_country = 
| locations        = <!-- Number of locations, stores, offices &c. -->
| area_served      = 
| key_people       = Dr. Anke&nbsp;Beck, Carsten&nbsp;Buhr
| products         = 
| services         = 
| revenue          = €60,6 million (2016)
| operating_income = 
| net_income       = 
| assets           = 
| equity           = 
| num_employees    = 350-500
| homepage         = [http://www.degruyter.com www.degruyter.com]
}}
'''Walter de Gruyter GmbH''' ({{IPA-de|ˈɡʁɔʏ̯tɐ|lang}} or {{IPA-de|ˈxʁɔʏ̯tɐ|}}; brand name: '''De Gruyter''') is a [[academic publishing|scholarly publishing house]] specializing in academic literature. The company has its roots in the bookstore of the ''Königliche Realschule'' in [[Berlin]], which had been granted the [[royal privilege]] to print books by King [[Frederick II of Prussia]] in 1749.<ref name=gr>{{cite web |url=http://www.degruyter.com/dg/page/79/eine-kurze-geschichte-des-verlags |title=A Short History of the Publishing House |publisher=Walter de Gruyter |date= |accessdate=2013-11-30}}</ref> In 1801 the store was taken over by Georg Reimer. In 1919, Walter de Gruyter (1862–1923) merged it with 4 other publishing houses into the company that became ''Verlag Walter de Gruyter & Co'' in 1923, and ''Walter de Gruyter GmbH'' in 2012.<ref name=gr/>

== Imprints and partnerships ==
Several former publishing houses have become [[imprint (trade name)|imprint]]s of De Gruyter.
* "De Gruyter Mouton/De Gruyter Saur" (formerly "Mouton de Gruyter") was purchased by de Gruyter in 1977. It was originally known as Mouton Publishers and based in [[The Hague]]. The imprint specializes in the field of [[linguistics]] and publishes [[academic journal]]s, research [[monograph]]s, reference works, multimedia publications, and [[bibliography|bibliographies]].
* [[K. G. Saur Verlag]], based in [[Munich]], was acquired in 2006 and retains the imprint "De Gruyter Saur". It specializes in reference information for libraries.
* De Gruyter acquired the journals of [[Berkeley Electronic Press]] in 2011.
* After filing for [[Bankruptcy|bankruptcy protection]] in 2012, publisher [[Birkhäuser]] was acquired by De Gruyter.<ref name="Birk">{{cite web |url=http://www.degruyter.com/dg/page/344/ |title=Birkhäuser |publisher=Walter de Gruyter |date=2012-04-26 |accessdate=2013-01-11}}</ref>
* In 2012 De Gruyter also acquired the [[open access]] publisher Versita.<ref>{{cite web |url=http://www.libraries.wright.edu/noshelfrequired/2012/01/09/degruyter-acquires-versita-increasing-their-open-access-publishing-business/ |title=DeGruyter acquires Versita, increasing their open-access publishing business}}</ref> Since 2014 Versita is fully integrated into the imprint "'''De Gruyter Open'''", which also hosts several so-called [[mega journals]]<ref>{{cite web|url=http://degruyteropen.com/de-gruyter-open-converts-eight-subscription-journals-open-access-megajournals/|title=De Gruyter Open converts eight subscription journals to Open Access megajournals|work=De Gruyter Open}}</ref> and a blog<ref>{{cite web|url=http://openscience.com/|title=OpenScience|work=De Gruyter Open}}</ref> on [[open access]] in academia, in reflection of the growing global popularity of [[open access]] among researchers and academic institutions.<ref>{{cite web|url=http://ir.inflibnet.ac.in/handle/1944/2014/|title=Global Shift Towards Open Access Publishing: Key Challenges for Research Community|work=Visakhi, P.}}</ref>
* In 2013 De Gruyter acquired two academic publishers from [[Cornelsen Verlag]]: ''Oldenbourg Wissenschaftsverlag'' and ''[[Akademie Verlag]]''.<ref>{{cite web |url=http://www.degruyter.com/applib/newsitem/60/de-gruyter-kauft-die-wissenschaftsverlage-oldenbourg-und-akademie |title=De Gruyter kauft die Wissenschaftsverlage Oldenbourg und Akademie |publisher=Walter de Gruyter |work=Press release}}</ref>

De Gruyter is one of thirteen publishers to participate in the [[Knowledge Unlatched]] pilot, a global library consortium approach to funding [[open access book]]s.<ref>{{cite web|url=http://www.knowledgeunlatched.org/good-for/publishers/|title=Good for publishers|work=knowledgeunlatched.org}}</ref>

== See also ==
* [[Berkeley Electronic Press]]
* [[:Category:Walter de Gruyter academic journals|Journals published by Walter de Gruyter]]

==References==
{{Reflist}}

== Further reading ==
* Fouquet-Plümscher, Doris: ''Aus dem Archiv des Verlages Walter de Gruyter: Briefe, Urkunden, Dokumente.'' Berlin, New York: Walter de Gruyter, 1980.

==External links==
{{commons category|Verlag Walter de Gruyter}}
* {{Official website|http://www.degruyter.com/}}

{{Authority control}}

{{DEFAULTSORT:Walter De Gruyter}}
[[Category:Walter de Gruyter| ]]
[[Category:Academic publishing companies]]
[[Category:Book publishing companies of Germany]]
[[Category:Publishing companies of Germany]]
[[Category:Publishing companies established in the 1740s]]
[[Category:1749 establishments in Prussia]]
[[Category:1749 establishments in Germany]]