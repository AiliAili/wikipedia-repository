{{Infobox journal
| title = Health Education & Behavior
| cover = [[File:Health Education & Behavior.jpg]]
| discipline = [[Public health]], [[education]]
| editor = John P. Allegrante
| publisher = [[SAGE Publications]]
| country =
| abbreviation = Health Educ. Behav.
| formernames =  Health Education Quarterly, Health Education Monographs
| history = 1973-present
| frequency = Bimonthly
| impact = 2.312
| impact-year = 2015
| website = http://www.sagepub.com/journals/Journal200851/title
| link1 = http://heb.sagepub.com/current.dtl
| link1-name = Online access
| link2 = http://journals.sagepub.com/loi/heb
| link2-name = Online archive
| JSTOR =
| ISSN = 1090-1981
| eISSN = 1552-6127
| CODEN = HEDBFS
| OCLC = 35233880
| LCCN = sn96-2571
}}
'''''Health Education & Behavior''''' is a bimonthly [[peer-reviewed]] [[healthcare journal]] covering applied [[Behavioral science|behavioral]] and [[social science]] in [[public health]] published by [[SAGE Publications]]. It is an official journal of the [[Society for Public Health Education]] (SOPHE).

==History==
The journal was established in 1957 as ''Health Education Monographs'' which was produced on an occasional basis with no set publishing schedule until 1974, when it became a quarterly publication with standard volume numbering and pagination.<ref name=":0">{{Cite journal |last=Zimmerman |first=M. A. |date=2016-07-01 |title=Message From the Editor |journal=Health Education & Behavior |volume=25 |issue=1 |pages=5–7 |doi=10.1177/109019819802500101}}</ref> The first issue of 1974 became volume 2, issue 1. All previous issues covering issues 1 to 36, from 1957 to 1973, were designated to constitute volume 1. During this period, ''Health Education Monographs'' was printed by various small publishing companies or cooperatively by SOPHE and the [[World Health Organization]]. No volume was published in 1979 due to extensive ongoing changes, including a new publisher, editor, and format.<ref name=":0" />

In 1980, the journal was renamed ''Health Education Quarterly'' and a publication agreement was negotiated with Human Sciences Press.<ref name=":0" />  [[John Wiley & Sons]] became the publisher with volume 10 in 1983 and the journal changed to its present publisher, SAGE Publications, in 1995. In 1997, the journal expanded to six issues annually to accommodate the growing volume of submissions and was renamed ''Health Education & Behavior''.<ref name=":0" />

==Abstracting and indexing==
The journal is abstracted and indexed by [[CAB Abstracts]], [[CINAHL]], [[Current Contents]]/Social & Behavioral Sciences, [[Global Health]], [[Index Medicus]]/[[MEDLINE]]/[[PubMed]], [[ProQuest|ProQuest databases]], [[Psychological Abstracts]], [[PsycINFO]], [[PsycLIT]], [[SafetyLit]], [[Scopus]], [[Social Sciences Citation Index]], and [[Sociological Abstracts]]. According to the ''[[Journal Citation Reports]]'', the journal's 2015 [[impact factor]] is 2.312.<ref name=WoS>{{cite book |year=2016 |chapter=Health Education & Behavior|title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.sagepub.com/journals/Journal200851/title}}

{{DEFAULTSORT:Health Education and Behavior}}
[[Category:Publications established in 1957]]
[[Category:Education journals]]
[[Category:Public health journals]]
[[Category:SAGE Publications academic journals]]
[[Category:Bimonthly journals]]
[[Category:Health education]]
[[Category:English-language journals]]