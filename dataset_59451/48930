{{Infobox company
| name             = Joseph
| foundation       = [[London]], [[UK]]
| industry         = [[Retailing|Retail]]
| products         = [[Clothing]], [[Fashion accessory|accessories]]   
| area_served      = Worldwide
| homepage         = {{URL|1=http://www.joseph-fashion.com}}
}}
'''Joseph''' is a fashion brand and retail chain that was established in [[London]] by [[Moroccan people|Moroccan]] entrepreneur [[Joseph Ettedgui]] and his family in 1972.<ref name="fashionreview">{{cite web|url=http://www.fashionreview.co.uk/joseph-fashion/ |title=Joseph Fashion Label - Designer London Fashion &#124; London Fashion Review - British Fashion Designers, Labels and Brands |publisher=Fashionreview.co.uk |date= |accessdate=2013-02-24}}</ref>

== History of the brand ==
The Joseph brand grew from a small shop attached to a hairdressing salon in [[King's Road]], [[Chelsea, London|Chelsea]], owned by [[Casablanca]]-born Joseph Ettedgui and his brothers Maurice and Franklin, to more than 20 London stores, with eight additional outlets in [[New York City]] and [[Paris]] plus stores in [[Leeds]] and [[Manchester]].<ref name="telegraph">{{cite news|url=http://www.telegraph.co.uk/news/obituaries/culture-obituaries/7483180/Joseph-Ettedgui.html |title=Joseph Ettedgui |publisher=Telegraph |date= 2010-03-19|accessdate=2013-02-24 |location=London}}</ref>

Joseph Ettedgui’s love of fashion meant the brothers began displaying designer clothes in their hair salon in the 1960s, including the work of pioneering [[Japanese people|Japanese]] designer [[Kenzo Takada]].<ref name="independent">{{cite news|url=http://www.independent.co.uk/news/obituaries/joseph-ettedgui-fashion-designer-and-entrepreneur-who-made-his-name-selling-cleancut-styles-at-affordable-prices-1925444.html |title=Joseph Ettedgui: Fashion designer and entrepreneur who made his name selling clean-cut styles at affordable prices - Obituaries - News |publisher=The Independent |date=2010-03-23 |accessdate=2013-02-24 |location=London}}</ref> The success of this early collaboration led to a move into fashion retailing, with the first store opening below the Chelsea hair salon in the early 1970s and the first large-scale retail outlet opening on [[Sloane Street]] in 1979.<ref name="fashionreview" /><ref>{{cite web|author=Kiran Randhawa |url=http://www.standard.co.uk/news/fashion-designer-joseph-ettedgui-paragon-of-good-taste-dies-at-71-6721527.html |title=Fashion designer Joseph Ettedgui, paragon of good taste, dies at 71 - News - London Evening Standard |publisher=Standard.co.uk |date=2010-03-19 |accessdate=2013-02-24}}</ref>

==Designer collaborations==
Joseph stores championed the work of many up-and-coming designers, including [[Margaret Howell]], [[Katharine Hamnett]], [[John Galliano]] and [[Azzedine Alaïa]].<ref>{{cite web|author=&#8212;Tim Blanks |url=http://www.style.com/stylefile/2010/03/joseph-ettedgui-rip/ |title=Joseph Ettedgui, R.I.P.: style file: daily fashion, party, and model news |publisher=Style.com |date=2010-03-19 |accessdate=2013-02-24}}</ref><ref name="guardian">{{cite news|author=Valerie Wade |url=https://www.theguardian.com/lifeandstyle/2010/mar/23/joseph-ettedgui-obituary |title=Joseph Ettedgui obituary &#124; Fashion |publisher=The Guardian |date= 2010-03-23|accessdate=2013-02-24 |location=London}}</ref> Own brand clothing began with distinctive knitwear (Joseph Tricot) and went on to include women’s clothing, perfume, homewares (Joseph Pour La Maison), and Joe's restaurants.<ref name="telegraph" /><ref>{{cite web|url=http://collections.vam.ac.uk/item/O1135164/jumper-ettedgui-joseph/ |title=Jumper &#124; Ettedgui, Joseph &#124; V&A Search the Collections |publisher=Collections.vam.ac.uk |date=2013-02-05 |accessdate=2013-02-24}}</ref> Joseph has been the recipient of a number of [[British Fashion Awards]], including Knitwear Designer of the Year four times between 1990 and 1994 and a British Fashion Award in 2000 presented by [[Cherie Blair]].<ref name="independent" />

The stores were as distinctive and influential to the UK retail scene as the clothes in them. Joseph was among the first to combine a restaurant and shop in one space. The Sloane Street store has a Joe's Restaurant on site and the Brompton Cross store has a Joe's Restaurant across the road. The Sloane Street, [[Knightsbridge]] flagship store was opened in 1979, and was designed by acclaimed architect [[Norman Foster]]. Joseph also collaborated with other architects and designers – including [[Eva Jiricná]], [[Andrée Putman]] and [[Raëd Abillama Architects]] – on its retail and restaurant projects.<ref name="guardian" />

==Further development of the brand==
In 1999 the Ettedgui brothers sold a majority share of the business to Belgian financier [[Albert Frère]], and a minority interest to [[LVMH]].<ref>{{cite web|url=http://www.fashionencyclopedia.com/Es-Fo/Ettedgui-Joseph.html |title=Joseph Ettedgui - Fashion Designer Encyclopedia - clothing, women, suits, men, style, new, body, collection, dresses, designs, world |publisher=Fashionencyclopedia.com |date= |accessdate=2013-02-24}}</ref>

In 2005 the brand was sold to its licensee in Asia, [[Onward Kashiyama]].<ref>{{cite news|author=Finance |url=http://www.telegraph.co.uk/finance/2915798/Japanese-eye-for-fashion-nets-Joseph-founder-20m.html |title=Japanese eye for fashion nets Joseph founder £20m |publisher=Telegraph |date=2005-05-17 |accessdate=2013-02-24 |location=London}}</ref> There are currently more than 80 branches and department store concessions around the world, including in [[Taiwan]], [[South Korea]] and [[Russia]].<ref>{{cite web|url=http://www.joseph.co.uk/en/ecomuk/scat/storelocator/ |title=store locator |publisher=Joseph |date= |accessdate=2013-02-24}}</ref>

Between 2009 and 2012 Joseph renovated a number of its stores both in the UK - including Brook Street, Old Bond Street, Sloane Street and Westbourne Grove - and in France, including St Germain and Avenue Montaigne.

In 2011 the company opened its first store in Moscow, and in 2012 opened a store in Los Angeles. In 2013 the company opened stores in Beirut and New York City.

In February 2016, Joseph opened its first stand-alone menswear store at No.2 Savile Row in London.<ref name="Founding Spirit Morris">{{cite web|last1=Morris|first1=Ali|title=Founding spirit: Joseph opens a new menswear flagship on Savile Row|url=http://www.wallpaper.com/fashion/joseph-channels-the-spirit-of-its-founder-in-its-first-menswear-flagship-on-savile-row|website=wallpaper.com|publisher=Wallpaper|accessdate=24 March 2016}}</ref>

==Fashion shows==

In 2013 Joseph announced that it would be appearing at [[London Fashion Week]] in February 2014, with five of its designers creating product for the catwalk show.<ref>{{cite news|url=http://fashion.telegraph.co.uk/article/TMG10446279/Joseph-to-debut-at-London-Fashion-Week.html |title=Joseph to debut at London Fashion Week - Telegraph |publisher=Fashion.telegraph.co.uk}}</ref> It also appeared at London Fashion Week in autumn 2014.<ref>{{cite news|url=http://www.redonline.co.uk/fashion/fashion_news/lfw-joseph |title=London Fashion Week: Joseph |publisher=Redonline.co.uk}}</ref>

The autumn/winter 2015 Menswear Collection received press coverage from fashion trade paper [[Women's Wear Daily|WWD]].<ref>{{cite news|url=http://www.wwd.com/runway/mens-fall-collections-2015/review/joseph |title=Joseph Men's RTW Fall 2015 |publisher=wwd.com}}</ref>

==References==
{{reflist|2}}

== External links ==
* [http://www.joseph.co.uk Joseph UK]
* [http://www.fashionencyclopedia.com/Es-Fo/Ettedgui-Joseph.html List of published reference material about Joseph Ettedgui and Joseph brand from Fashion Encyclopedia]
* [http://www.fashionreview.co.uk/joseph-fashion/ Timeline of the brand from Fashion Review]
* [http://collections.vam.ac.uk/item/O1135164/jumper-ettedgui-joseph/ Example of 1980s Joseph Tricot sweater in V&A fashion archive collection]

[[Category:Clothing brands]]
[[Category:Clothing retailers]]
[[Category:Clothing companies of the United Kingdom]]
[[Category:Clothing retailers of the United Kingdom]]
[[Category:High fashion brands]]

[[de:Joseph (Modemarke)]]