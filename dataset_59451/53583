{{multiple issues|
{{Unreferenced|date=November 2012}}
{{POV|date=November 2012}}
}}

'''Indian Audit and Accounts Service (IA&AS)''' is an Indian Central Government service, free of control from any executive authority, under the [[Comptroller and Auditor General of India]]. The officers of the Indian Audit and Accounts Service serve in an audit managerial capacity, in the Indian Audit and Accounts Department. IA&AS is responsible for auditing the accounts of the Union and State governments and public sector organizations, and for maintaining the accounts of State governments. It role is somewhat similar to the US [[GAO]] and [[National Audit Office (United Kingdom)]].

The service can be divided into officers looking after accounting and audit issues pertaining to the Union government and the State governments, and those manning the Department's Headquarters. The state accounts and audit offices are headed by Accountants General or Principal Accountants General. They are functionally equivalent; only the designations vary. Major states have three Principal Accountants General (PAsG) or Accountants General (AsG), each heading Accounts and Entitlement (i.e., compiling state accounts, maintaining pension accounts, loan accounts, etc.), General and Social Sector Audit (GSSA) or Economic and Revenue Sector Audit (ERSA).

The equivalent officers at the Central level are Principal Directors (PDs) or Directors General (DsG). The PDs, DsG, AsG and PAsG report to Additional Deputy CAG  (also called ADAI, for historical reasons) or Deputy CAG (called DAI, again for historical reasons). The Deputy CAGs are the highest-ranked officers in the service.

After training, the Officer Trainees are posted as Deputy Accountants General (DAsG) or Deputy Directors (DDs). Subsequent to their promotion, they become Senior Deputy Accountants General (Sr.DAsG) or Directors. All officers below the rank of AG/PD are also called Group Officers as they are generally in charge of a group in the office.

==Recruitment and training==

Recruitment to the IA&AS is through the joint competitive examinations (the [[Civil Services Examination]]) and through promotion from the subordinate cadre. Once recruited to the IA&AS, the directly recruited officers are trained mainly at the [http://www.naaa.gov.in National Academy of Audit and Accounts], [[Shimla]], Himachal Pradesh, India. The training is split into two phases. Phase-1 involves giving a theoretical background to the students on concepts of Government and commercial auditing and accounting. Phase-2 gives emphasis on practical training. The training involves modules where Officer Trainees are attached to the Reserve Bank of India,TISS-Mumbai,SEBI the National Institute of Public Finance and Policy, NIFM Faridabad, the Bureau of Parliamentary Studies and the Indian Institute of Management, Ahmedabad. The Officer Trainees are also given an international exposure through attachment with London School of Economics and Political Science and [[National Audit Office (United Kingdom)]].

== Sanctioned strength ==
Sanctioned strength of IA&AS Cadre (As on 01.09.2016)
Deputy Comptroller and Auditor General: 6
Additional Deputy Comptroller and Auditor General: 7
Principal Accountant General: 56
Senior Administrative Grade: 128
Junior Administrative Grade: 147 (Selection Grade 74 and Ordinary Grade 73)
Senior Time Scale: 263
Junior Time Scale: 88

==Job content==

For carrying out diverse functions, the Comptroller & Auditor General of India is assisted by one of India's oldest services – the Indian Audit & Accounts Service, whose officers are deployed in offices spread throughout the country besides three overseas offices located at London, Kuala Lumpur and Washington.

==IA&AS overseas==

IA&AS officers mainly go abroad to conduct embassy audit i.e. audit of Embassies and High Commissions of India situated all over the world. They are also deputed regularly to conduct audit of international institutions like UN. Some of the officers are doing long term foreign assignments in United Nations, UNOPS, Organisation for the Prohibition of Chemical Weapons, UNRWA, etc.

gov.nic.in

{{Public Services of India}}
{{Civil service}}


[[Category:Accounting in India]]
[[Category:Central Civil Services (India)]]