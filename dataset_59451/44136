{{Infobox airport 
| nativename          = 
| nativename-a        = 
| nativename-r        = 
| image2-width        = 250
| location            = [[Belp]]
| hub                 = 
*[[SkyWork Airlines]]
*[[Helvetic Airways]]
| elevation-m        = 510
| coordinates         = {{coord|46|54|44|N|07|29|57|E|region:CH-BE|display=inline,title}}
| pushpin_label       = BRN
| r1-length-f        = 5,676
| r1-surface         = [[Asphalt]]
| metric-rwy         = Yes
| name                = Bern Airport
| image               = <!--airport logo-->
| image-width         = 200
| image2              = Bern Airport Overview in Winter.jpg
| IATA                = BRN
| ICAO                = LSZB / LSMB
| pushpin_map         = Switzerland
| pushpin_mapsize     = 200
| pushpin_mark        = Airplane_silhouette.svg
| pushpin_map_caption = Location of the airport in Switzerland
| type                = Public
| owner               = 
| operator            = Flughafen Bern AG
| city-served         = [[Bern]], [[Switzerland]]
| elevation-f        = 1,673
| website            = {{URL|http://www.bernairport.ch/}}
| r1-number          = 14/32
| r1-length-m        = 1,730
| r2-number          = 14L/32R
| r2-length-f        = 2,133
| r2-length-m        = 650
| r2-surface         = [[Grass]]
| stat-year          = 2016
| stat1-header       = Passengers
| stat1-data         = 183,319
| stat2-header       = 
| stat2-data         = 
| footnotes          = Source: Swiss [[Aeronautical Information Publication|AIP]] at [[European Organisation for the Safety of Air Navigation|EUROCONTROL]]
}}

'''Bern Airport''' {{airport codes|BRN|LSZB / LSMB}}, formerly ''Regionalflugplatz Bern-Belp'' in German, is an [[airport]] serving [[Bern]], the capital of [[Switzerland]]. The airport is located within the town limits of [[Belp]],<ref>"[http://www.geomedia.ch/belportsplan/home/frame.asp?B=1024 Ortsplan] {{webarchive |url=https://web.archive.org/web/20090813104940/http://www.geomedia.ch/belportsplan/home/frame.asp?B=1024 |date=August 13, 2009 }}." Town of Belp. Retrieved on 8 December 2010.</ref> and features flights to some European metropolitan and several leisure destinations. It handled 183,319 passengers in 2016, a decrease of 3.5 percent over 2015,<ref>http://flughafenbern.ch/de/unternehmen/medien-news/medienmitteilungen</ref> and serves as the homebase for [[SkyWork Airlines]] and a base for [[Helvetic Airways]].

==History==
The airport was established in 1929 by Alpar, a private airline that operated within Switzerland until the outbreak of World War II. After the war, Alpar remained in business as the airport's operator, supported by subsidies of the cantonal and city government.<ref>Benedikt Meyer: ''Im Flug. Schweizer Airlines und ihre Passagiere, 1919-2002.'' Chronos, Zürich 2014, ISBN 978-3-0340-1238-6</ref> A planned expansion of the airport in 1947 did not pass in a popular referendum, and it was not until 1950 that the first concrete airstrip was built. In 2014, Alpar was renamed to ''Flughafen Bern AG - Bern Airport''.

Multiple attempts to build an international airport in or around Bern instead of the small regional airport at Belpmoos failed. In 1945, the national parliament decided to build the first international airport, now [[Zürich Airport]], at [[Kloten]] near Zürich instead of in Utzensdorf near Bern, though plans for development there were retained as an inter-urban airport which would require less space and thus placate local opposition by farming interests.<ref name="fae">{{cite journal |last=Bell |first=E. A. |date=10 May 1945 |title=Swiss Planning |url=https://www.flightglobal.com/pdfarchive/view/1945/1945%20-%200907.html |journal=Flight and Aircraft Engineer |publisher=Royal Aero Club |volume=XLVII |issue=1898 |pages=501 |access-date=5 July 2016}}</ref>  A 1963 airport project near Herrenschwanden was abandoned because of strong popular opposition, notably by farmers, as was a 1966 project in Rosshäusern and a 1970 project [[Kallnach]].<ref>{{cite news|title=Das Grounding aller Berner Flughafenpläne|url=http://www.bernerzeitung.ch/region/emmental/als-in-utzenstorf-flugzeuge-landen-sollten/story/12443208|accessdate=22 November 2015|work=[[Berner Zeitung]]|date=22 November 2015}}</ref>

In December 2016, [[bmi regional]] ceased its flights from [[Munich Airport]] to Bern after two years which it served in direct competition with [[SkyWork Airlines]].<ref>http://www.aerotelegraph.com/bmi-regional-kehrt-bern-wieder-den-ruecken</ref>

==Facilities==
[[File:Bern Airport Aerial.JPG|thumb|Aerial view of Bern Airport]]
The airport has multiple touchdown areas, a paved runway (14/32 of 1,730 metres (5,676&nbsp;ft), a grass runway (32L/14R of 650 metres (2,133&nbsp;ft), a heli-square, and a glider area.  Runway 14 has an [[Instrument landing system|ILS]] approach and an [[NDB approach|NDB]] approach. The existing terminal was expanded to better accommodate flights to the non-[[Schengen area]] in 2011. Planned development includes new taxiways and a new parking area.

The ''Biderhangar'', one of the airport's [[hangar]]s built by Swiss aviation pioneer [[Oskar Bider]], is listed as a [[Swiss Inventory of Cultural Property of National and Regional Significance|heritage site of national significance]]. The airport also houses the head office of [[Heliswiss]].<ref>"[http://www.heliswiss.com/en/belp/belp.htm Bern-Belp base]." [[Heliswiss]]. Retrieved on 25 February 2011. "Heliswiss is an international helicopter company based at Bern-Belp Airport."</ref> Previously the North Terminal housed the head office of [[SkyWork Airlines]].<ref>"[https://web.archive.org/web/20101102133929/http://www.flyskywork.com/73-1-Contact.html Contact]." SkyWork Airlines. Retrieved on 8 December 2010. "SkyWork Flughafen Bern-Belp Terminal Nord CH 3123 Bern-Belp."</ref><ref>"[http://www.flyskywork.com/34-1-Terms-and-conditions.html Terms and conditions]." [[SkyWork Airlines]]. Retrieved on 8 December 2010. ""We", "our" "ourselves" and "us" means Sky Work Airlines Ltd., domiciled in Bern-Belp, Switzerland."</ref>

==Other usage==
The aircraft of the air transport service of the [[Swiss Air Force]] are stationed at Bern Airport. These are two jets and two turboprops. The former ones are mainly used for VIP transport and particularly the transport of members of the Bundesrat. They are also used for other purposes, for example deportations or to support international peacekeeping measures. The two turboprop [[DHC-6 Twin Otter]] and [[Beechcraft King Air]] are not usually used for VIP flights, but for the passenger transport as well as for the country's topography service.<ref>{{cite web |title=Lufttransportdienst des Bundes (LTDB) auf admin.ch|url=http://www.lw.admin.ch/internet/luftwaffe/de/home/verbaende/einsatz_lw/ltdb.html |access-date=29 October 2010}}</ref> The [[Beechcraft 1900]] is also used by the country's topography service for the same tasks. The two jets are a [[Dassault Falcon 900]]<ref name="airassets">{{cite web |title=Mittel: Flugzeuge, Helikopter, Flab |url=http://www.lw.admin.ch/internet/luftwaffe/de/home/dokumentation/assets.html |date= |language=German |publisher=Swiss Air Force |accessdate=14 July 2009}}</ref> and a [[Cessna Citation Excel]], which offer up to seats for VIPs.

Additionally, Bern Airport serves as the homebase of the [[Federal Office of Civil Aviation]]. A base of the air rescue organization [[Rega (air rescue)|Rega]] is also located at the Airport, using a [[Eurocopter EC 145]].

==Airlines and destinations==
The following airlines offer regular scheduled and charter flights at Bern Airport:<ref>http://www.flughafenbern.ch/en/passengers/timetable</ref>
<!--PLEASE DO NOT ADD OR REMOVE ROUTES WITHOUT GIVING A VALID SOURCE. EXACT DATES ARE MANDATORY FOR NEW ROUTES TO BE ADDED HERE. ALSO ADD INLINE CITATIONS IF POSSIBLE.-->
{{Airport destination list
<!-- -->
| [[Etihad Regional]] <br>{{nowrap|operated by [[Darwin Airline]]}} | '''Seasonal charter:''' [[Palma de Mallorca Airport|Palma de Mallorca]]
<!-- -->
| [[Germania Flug]] | '''Seasonal:''' [[Calvi Airport|Calvi]] (begins {{date|2017-06-04}})<ref>[https://www.germania.ch/en/flight-information/flight-schedule/ "Flight schedule - Germania Flug AG | Flüge ab CHF 69"]. www.germania.ch. Retrieved 2017-01-31.</ref>
<!-- -->
| [[Helvetic Airways]] | '''Seasonal:''' [[Olbia Costa Smeralda Airport|Olbia]], [[Palma de Mallorca Airport|Palma de Mallorca]] <br>'''Seasonal charter:''' [[Heraklion Airport|Heraklion]], [[Kos Airport|Kos]], [[Larnaca International Airport|Larnaca]], [[Aktion National Airport|Preveza]], [[Rhodes International Airport|Rhodes]]
<!-- -->
| {{nowrap|[[SkyWork Airlines]]}}{{ref|1|1}} | [[Amsterdam Airport Schiphol|Amsterdam]], [[Berlin Tegel Airport|Berlin-Tegel]], [[Cologne Bonn Airport|Cologne/Bonn]], [[Hamburg Airport|Hamburg]], [[London City Airport|London-City]], [[Munich Airport|Munich]], [[Palma de Mallorca Airport|Palma de Mallorca]], [[Vienna International Airport|Vienna]] <br>'''Seasonal:''' [[Cagliari-Elmas Airport|Cagliari]], [[Marina di Campo Airport|Elba]], [[Figari Sud-Corse Airport|Figari]], [[Heringsdorf Airport|Heringsdorf]], [[Ibiza Airport|Ibiza]], [[Jersey Airport|Jersey]], [[Menorca Airport|Menorca]], [[Olbia – Costa Smeralda Airport|Olbia]], [[Sylt Airport|Sylt]], [[Zadar Airport|Zadar]]
<!-- -->
}}
<small>{{note|1|1}}SkyWork Airlines flights to Heringsdorf, Jersey and Sylt are partly operated with an intermediate stop in Basel/Mulhouse. However, SkyWork does not sell tickets on the Swiss domestic sector.<ref>http://www.austrianaviation.net/news-international/news-detail/datum/2015/06/01/skywork-uebernimmt-basel-london-city.html</ref></small>

==Statistics==

{| class="wikitable"
|-
! Year
! Passengers
! Change 
|-
| 2010
| 85,981
|{{decrease}}{{0}}9.6%
|-
| 2011
| 169,288
|{{increase}}{{0}}96.9%
|-
| 2012
| 258,543
|{{increase}}{{0}}52.7%
|-
| 2013
| 244,699
|{{decrease}}{{0}}5.4%
|-
| 2014
| 177,539
|{{decrease}}{{0}}27.5%
|-
| 2015
| 190,032
|{{increase}}{{0}}7.0%
|-
| 2016
| 183,319
|{{decrease}}{{0}}3.5%
|}

==Ground transportation==
Two bus lines serve the airport: the ''AirportBus Bern'' (line 334) connects the terminal every half-hour with Belp railway station where passengers can connect to frequent S-Bahn trains S3, S4, S31 and S44 to Bern main station. The journey time to Bern city center is 30 minutes. The bus line 160 connects the airport with Belp, [[Rubigen]] and [[Münsingen]] (connection to S-Bahn trains S1).

==See also==
* [[Transport in Switzerland]]
* [[List of the busiest airports in Switzerland]]

==References==
{{Reflist}}

==External links==
{{Commonscat-inline|Bern-Belp Airport}}
* [http://www.flughafenbern.ch/?setLang=2/ Official website]
* {{ASN|BRN}}
* [https://web.archive.org/web/20110706221632/http://www.bazl.admin.ch/themen/lupo/00293/00360/index.html?download=M3wBUQCu/8ulmKDu36WenojQ1NTTjaXZnqWfVpzLhmfhnapmmc7Zi6rZnqCkkIN0fX+CbKbXrZ2lhtTN34al3p6YrY7P1oah162apo3X1cjYh2+hoJVn6w==.pdf&lang=de/ Classification of airports and airfields in Switzerland]
* [http://www.biderhangar.ch Biderhangar website]

{{Portalbar|Switzerland|Aviation}}
{{Airports in Switzerland}}

{{Authority control}}

[[Category:Buildings and structures in the Canton of Bern|Airport Bern-Belp]]
[[Category:Airports in Switzerland]]
[[Category:Transport in Bern]]
[[Category:Cultural property of national significance in the canton of Bern]]
[[Category:Military airbases in Switzerland]]