{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox rugby biography
| name = David Bedell-Sivright
| image = Bedellsivright rugby.jpeg
| caption =
| birth_name = David Revell Bedell-Sivright
| nickname = Darkie
| birth_date = {{birth date|1880|12|8|df=y}}
| birth_place = [[Edinburgh]], [[Scotland]]<ref name=Scrum>{{cite web| url = http://www.scrum.com/statsguru/rugby/player/1417.html | title = David Bedell-Sivright | publisher = ESPN | accessdate = 1 June 2014}}</ref>
| death_date = {{Death date and age|1915|9|5|1880|12|8|df=yes}}
| death_place = [[Gallipoli]], Turkey
| height =
| weight = 
| ru_position = [[Rugby union positions#Forwards|Forward]]
| ru_amateuryears = {{nowrap|1899–1903<br /><br />1901<br />1903–07<br />1904–}}
| ru_amateurclubs = {{nowrap|[[Cambridge University R.U.F.C.]]<br />[[Edinburgh University RFC]]<br />[[Fettesian-Lorettonian Club]]<br />[[Barbarian F.C.|Barbarians]]<ref name=Starmer-Smith218>Starmer-Smith (1977), p. 218.</ref><br />[[West of Scotland F.C.]]<br/>[[Murrayfield Wanderers RFC|Edinburgh Wanderers RFC]]}}
| ru_amclubcaps =
| ru_amclubpoints =
| ru_nationalteam = {{nowrap|[[Scotland national rugby union team|Scotland]]<br />[[British and Irish Lions|British Isles]]}}
| ru_nationalyears = {{nowrap|1900–08<br />1903–04}}
| ru_nationalcaps = 22<br />1
| ru_nationalpoints = (9)<br />(0)
| ru_coachclubs =
| ru_coachyears =
| other =
| occupation = Surgeon
| spouse = 
| children =
| relatives = 
| school = [[Fettes College]]
| university = [[Trinity College, Cambridge]]
| module = 
----{{infobox military person|embed=yes 
|allegiance = {{UK}} 
|branch = {{Navy|United Kingdom}}
|serviceyears = 
|rank = 
|unit = 
|commands = 
|battles = 
|placeofburial = 
|placeofburial_coordinates = 
|mawards = <!-- for military awards - appears as "Awards" if |awards= is not set --> 
|memorials = }}
}}

'''David Revell "Darkie" Bedell-Sivright''' (8 December 1880&nbsp;– 5 September 1915) was a Scottish international [[rugby union]] forward who captained both [[Scotland national rugby union team|Scotland]] and the [[British and Irish Lions|British Isles]]. Born in Edinburgh, and educated at [[Fettes College]] where he learned to play rugby, he studied at [[Cambridge University]] and earned four [[Blue (university sport)|Blues]] playing for them in the [[The Varsity Match|Varsity Match]]. He was first selected for Scotland in 1900 in a match against [[Wales national rugby union team|Wales]]. After playing in all of Scotland's [[Six Nations Championship|Home Nations Championship]] matches in 1901, 1902 and 1903, Bedell-Sivright toured with the British Isles{{snd}}now known as the British and Irish Lions{{snd}}side that [[1903 British Lions tour to South Africa|toured South Africa]] in 1903. After playing the first 12 matches of the tour, he was injured and so did not play in any of the [[Test match (rugby union)|Test matches]] against [[South Africa national rugby union team|South Africa]].

The next year Bedell-Sivright was appointed captain for the British Isles team that [[1904 British Lions tour to Australia and New Zealand|toured Australia and New Zealand]]. Due to a broken leg he played only one Test match during the tour{{snd}}against [[Australia national rugby union team|Australia]]{{snd}}but was involved in a notable incident during a non-Test match. Despite not playing, Bedell-Sivright pulled the British team from the field for 20 minutes after disputing the decision by a local referee to send-off one of their players. Bedell-Sivright eventually allowed his side to resume play, but without their ejected teammate.

Following the tour Bedell-Sivright briefly settled in Australia, before returning to Scotland to study medicine. He captained Scotland against the touring New Zealanders in 1905, and in 1906 helped his country defeat the visiting South Africans 6–0. After retiring from international rugby in 1908 he went on to become Scotland's amateur boxing champion. A surgeon by profession, he joined the [[Royal Navy]] during the First World War, and died on active service during the [[Gallipoli Campaign]].

Bedell-Sivright had a reputation as an aggressive and hard rugby player, as well as a ferocious competitor. He was an inaugural inductee into the Scottish Rugby Hall of Fame, and in 2013 was inducted into the [[IRB Hall of Fame|International Rugby Board (IRB) Hall of Fame]].

==Personal history==
David Bedell-Sivright was born in Edinburgh in 1880 to William Henry Revell Bedell-Sivright of [[North Queensferry]]. Bedell-Sivright was educated at [[Fettes College]].<ref |name=Royle>{{cite news | url = http://www.highbeam.com/doc/1P2-9993350.html |title = Remembering the fallen heroes of the rugby community | newspaper = The Sunday Herald| first = Trevor | last = Royle| date = 10 November 2002}}{{Subscription required}}</ref> before going to [[Trinity College, Cambridge]] in 1899 to read medicine. He later completed his medical training at the [[University of Edinburgh]].<ref name=Cambridge>{{acad|id=BDL899DR|name=Bedell-Sivright, David}}</ref> His brother [[John Bedell-Sivright|John]] played for [[Cambridge University RFC]], and gained a single international cap in 1902.<ref name=Bath137>Bath (2007), p. 137.</ref>

There are many tales surrounding Bedell-Sivright, and it is difficult to separate fact from fiction.<ref name="RugbysWarDead">{{cite web| url = http://www.cwgc.org/admin/files/Rugby%20leaflet.pdf |publisher = Commonwealth War Graves Commission | archiveurl=https://web.archive.org/web/20100618084637/http://www.cwgc.org/admin/files/Rugby%20leaflet.pdf | title = Rugby's War Dead | archivedate = 18 June 2010}}</ref> He had a reputation for aggression, and in 1909 became Scottish amateur [[boxing]] champion. After one international he rugby tackled a cart horse in [[Princes Street]] in Edinburgh after apparently laying down on a city tram track – this held up the traffic for an hour as no policeman would approach him.<ref name="RugbysWarDead"/><ref name=Walmsley>{{cite news | url = http://www.telegraph.co.uk/sport/rugbyunion/2361780/1904-Bedell-Sivright-pulls-no-punches.html |title = 1904: Bedell-Sivright pulls no punches | date = 30 June 2005 | publisher = The Telegraph |first =David | last=Walmsely | accessdate = 1 June 2014}}</ref> It is not clear exactly where Bedell-Sivright picked up the nickname "Darkie". One explanation is that it was due to "cynical" tactics he employed as captain, the other is that it was due to the dark rings around his eyes.<ref name=Elliott116>Elliot (2012), p. 116.</ref>

On 25 January 1915 Bedell-Sivright was commissioned as a surgeon in the Royal Navy.<ref>{{London Gazette|issue=29055|startpage=1016|date=2 February 1915|accessdate=14 December 2009}}</ref> He was posted to the Hawke Battalion of the [[63rd (Royal Naval) Division|Royal Naval Division]] stationed at [[Gallipoli]] during the [[Gallipoli Campaign|Dardanelles Campaign]] in May 1915.<ref name=ServiceRecord>{{cite web| url = http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7550186 Royal Naval Division—Image details—Sivright | title = David Revill Bedill | publisher = [[The National Archives]] | accessdate= 14 December 2009}}{{closed access}}</ref> He was loaned to the [[Royal Scots Fusiliers]] from 8 to 20 June, and was then posted to the Portsmouth Battalion of [[Royal Marine Light Infantry]].  After a period onshore in the trenches while serving at an advanced dressing station, he was bitten by an unidentified insect. He complained of being fatigued and was taken offshore and transferred to the hospital ship HMHS Dunluce Castle.<ref name=ServiceRecord /><ref name = navalhistory>{{cite web| url = http://www.naval-history.net/xDKCasAlpha1914-18B.htm | publisher = naval-history.net | first = Don  | last = Kindell |title = Naval History | date = 17 August 2013 | accessdate = 1 June 2014 }}</ref> Two days later, on 5 September, he died of [[Sepsis|septicaemia]] and was buried at sea off [[Cape Helles]].<ref name=ServiceRecord /> He is commemorated on the Portsmouth Naval Memorial.<ref name=Commonwealth>{{cite web| url = http://www.cwgc.org/search/casualty_details.aspx?casualty=3033353 |title =  Casualty details: Bedell-Sivright, David Revell | publisher=  [[Commonwealth War Graves Commission]] | accessdate = 14 December 2009}}</ref>

==Rugby career==
Bedell-Sivright learned his rugby while a student at Fettes College,<ref name=IRBProfile>{{cite web | url = http://www.irb.com/history/halloffame/newsid=2069277.html | publisher = International Rugby Board | title = 2013 Inductee: Dr David Bedell-Sivright | date = 13 November 2013 | accessdate = 1 June 2014}}</ref> but first came to note as a player when he represented Cambridge University in the [[The Varsity Match|Varsity matches]]{{snd}}contested against Oxford University{{snd}}between 1899 and 1902;<ref name=Cambridge/> this won him four sporting [[Blue (university sport)|Blues]].<ref name=IRBProfile/> He was first [[Cap (sport)|capped]] for Scotland in 1900 in a match against [[Wales national rugby union team|Wales]] at [[St Helens Rugby and Cricket Ground|St Helen's]], Swansea.<ref name=HallofFame>{{cite web | url = http://www.scottishrugby.org/about-us/hall-of-fame/2010 | title = Scottish Rugby Hall of Fame Inductees 2010 | publisher = Scottish Rugby Union | accessdate = 1 June 2014 | archiveurl = https://web.archive.org/web/20140328012017/http://scottishrugby.org/about-us/hall-of-fame/2010 | archivedate = 28 March 2014}}</ref> The match was a turning point for Welsh rugby, who won 12–3,<ref name="WelshIndigenous">{{cite journal| title=Welsh Indigenous! and British Imperial?–Welsh Rugby, Culture, and Society 1890–1914 |journal=Journal of Sport History |volume=18 |year=1991 |last=Andrews |first=David |issue=3}}</ref>{{efn|Wales won the Home Nations Championship in 1900, and went on to endure a "Golden Age" until 1911.<ref name="WelshIndigenous"/>}} but the selectors stuck with Bedell-Sivright who won another 21 caps for his country.<ref name=Scrum/> He is the only Scottish player to have won three [[Triple Crown (rugby union)|Triple Crowns]]{{snd}}wins over Ireland, Wales and England within the same [[Six Nations Championship|Home Nations Championship]] (now the Six Nations){{snd}}in 1901, 1903 and 1907.<ref name="STV"/>

In 1903 Bedell-Sivright was selected for his first match with invitational touring side the [[Barbarian F.C.|Barbarians]]. He played a total of five games for the side between 1903 and 1907, and captained them against [[Cardiff RFC]] in 1907.<ref name="Barbarian">{{cite web|url=http://www.barbarianfc.co.uk/player-archive/profile/3505/d-r-bedell-sivright|title=Barbarian player profile: D. R. Bedell-Sivright|accessdate=15 December 2013|publisher=barbarianfc.co.uk | archiveurl= https://web.archive.org/web/20131215135625/http://www.barbarianfc.co.uk/player-archive/profile/3505/d-r-bedell-sivright | archivedate = 15 December 2013}}</ref>

Bedell-Sivright was later chosen to tour with two different British Isles teams. The first was the [[1903 British Lions tour to South Africa|1903 tour of South Africa]] under the captaincy of fellow Scottish international [[Mark Coxon Morrison|Mark Morrison]].<ref name="1903SouthAfrica">{{cite web| url = http://www.lionsrugby.com/history/timeline/1903_south_africa.php | title = 1903 – South Africa | publisher = British Lions Ltd | accessdate = 1 June 2014}}</ref> Although at the centre of the British Isles pack, Beddel-Sivright did not play in any of the [[Test match (rugby union)|test matches]]. He played in the first 12 tour matches, where the team won six and lost six, but was injured thereafter.<ref name="1903Lions">{{cite web| url = http://www.lionsrugby.com/10699.php | publisher = British Lions Ltd | title = The Lions Down Under – 1904 | date = 21 December 2009 | accessdate = 1 June 2014}}</ref><ref name="LionsHallofFame">{{cite web | url = http://www.lionsrugby.com/10606.php | title = Hall of Fame Lions | last = Westgate | first = Rob Cole | date = 9 November 2010 | accessdate = 3 June 2014 | publisher = British Lions Ltd }}</ref>

{{Quote box|quote=  A captain of a team, like a general of an army, has an important part to play, and with every point he must be acquainted, or else disaster will almost invariably befall his side. The British team now on its way to Australasia will, judging from what one can learn, be well served in the way of leadership. D. R. Bedell-Sivright, who has been chosen as the skipper, has had vast experience as a leader. |source = G. W. McArthur, selector of the 1904 British Isles team, on Bedell-Sivright<ref name="CaptainQuote">{{cite news | url = http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&d=OW19040601.2.119.7&e=-------10--1----0-- | newspaper = Otago Witness | date = 1 June 1904 | title = The British Rugby Team | page = 58 | accessdate = 3 June 2014}}</ref>|width  = 33%
 |align  = right}}In 1904, at the request of the England Rugby Board (known as the [[Rugby Football Union]]), Bedell-Sivright was selected to lead a British Lions team on a tour of Australia and New Zealand.<ref name="Lions1904">{{cite news | title = Battling the Lions: 1904 | url = http://www.nzherald.co.nz/rugby/news/article.cfm?c_id=80&objectid=10123624 | publisher = New Zealand Herald | date = 1 May 2005 | accessdate = 1 June 2014}}</ref> He only played in one Test, against Australia, because of a broken leg, but the team did go undefeated in Australia{{snd}}winning all their matches there, and the three Test matches 17–0, 17–3 and 16–0.<ref name="1904Tour">{{cite web | url = http://www.lionsrugby.com/history/timeline/1904_australia_and_new_zealand.php | title = 1904 – Australia & New Zealand | publisher = British Lions Ltd | accessdate = 1 June 2014}}</ref> A notable incident on the Australian leg of the tour was a dispute between the British side and one of the Australian referees. The referee{{snd}}Hugh Dolan{{snd}} had ordered off British Isles player [[Denys Dobson]] after claiming Dobson had directed a personal insult at him. Bedell-Sivright was not playing, but pulled his team from the field for 20 minutes while disputing the decision with Dolan and the other officials. Eventually the British side returned to play, but without Dobson.<ref name=Walmsley/> Following the match Bedell-Sivright accused the referee of incompetence, and an inquiry eventually cleared Dobson of using indecent language, but he became the first British Isles player ever ordered off.<ref name=Walmsley/>

[[File:David Bedell-Sivright.jpg|thumb|right|A sketch of Bedell-Sivright published prior to his tour of Australia with the 1904 British Isles team |alt=Cartoon drawing of the head and shoulders of a young man]]The five-match New Zealand leg of the tour was far less successful for the British team.<ref name=McCarthy29>McCarthy (1968), pp. 29–30.</ref> The tourists won their first two matches, but their third{{snd}}the sole Test against [[New Zealand national rugby union team|New Zealand]]{{snd}} was lost 9–3. Bedell-Sivright did not play in the Test after injuring himself in the teams' first New Zealand tour match,<ref name="Elliott118">Elliott (2012), p. 118.</ref> but blamed that loss, and their subsequent draw and loss (the final loss was 13–0 to [[Auckland rugby union team|Auckland]]) on fatigue after their tour of Australia.<ref name=McCarthy29/> According to a New Zealand newspaper, Bedell-Sivright only ever claimed his side was "stale" after a loss, and never before.<ref name="Elliott125">Elliott (2012), p. 125.</ref> A New Zealand representative side was scheduled to tour the British Isles in 1905, and Bedell-Sivright did not report favourably on their chances of success. This may have contributed to the [[Home Nations]] underestimating the All Blacks{{snd}}as [[The Original All Blacks|the 1905 team]] become known{{snd}}who ended up winning all but one of their 35 matches.<ref name=McCarthy29/><ref name="Elliott125"/>

Bedell-Sivright was so impressed with Australia that he decided to settle there. After a year he became bored of [[Jackaroo (trainee)|jackarooing]] (stock-rearing), and decided to leave and head back to Scotland to study medicine.<ref name=Walmsley/> While studying in Edinburgh he joined the [[Edinburgh University RFC]]{{snd}}captaining them for two seasons in 1906–07 and 1908–09.<ref name="TheCynic">{{cite news |url=http://nla.gov.au/nla.news-article129355371 |title=D. R. Bedell Sivright's Career in Rugby Football |newspaper=[[The Referee (newspaper)|The Referee]] |location=Sydney |date=3 November 1915 |accessdate=6 June 2014 |page=16 |publisher=National Library of Australia}}</ref> He had returned to Scotland in time to face the touring New Zealanders, and so he captained his country against them at [[Inverleith]].<ref name="ABGame">{{cite web| url = http://stats.allblacks.com/asp/teamsheet.asp?MT_ID=1075 | title = 4th All Black Test – 75th All Black Game | publisher = allblacks.com | accessdate = 3 June 2014}}</ref> The All Blacks had not been troubled in any of their previous matches on tour, scoring 612 points, and conceding only 15.<ref name=McCarthy45>McCarthy (1968), pp. 45–46.</ref> Rugby writer Winston McCarthy described the Scottish forwards as "fast, vigorous and good dribblers", and they led 7–6 at half-time.<ref name=McCarthy45/> However the New Zealanders were the better team, and scored six unanswered points in the second half to win 12–6.<ref name="ABGame"/><ref name=McCarthy45/>

In 1906 the South Africans were [[1906–07 South Africa rugby union tour|touring]] the British Isles, and Bedell-Sivright was selected for the Scotland side that defeated them 6–0.<ref name="1906Africa">{{cite web | url = http://www.espnscrum.com/statsguru/rugby/match/19091.html | title = Scotland 6–0 South Africa (FT) | publisher= ESPN | accessdate = 3 June 2014}}</ref> This was the last Home Nations team to defeat South Africa in nearly 60 years.<ref name="MuseumofRugby">{{cite web | url = https://www.rfu.com/images/museum/pdfs/exhibition/gbnf/GBNF1.pdf | format = pdf | title = Gone but not Forgotten – Rugby's War Dead | publisher = Museum of Rugby Twickenham | accessdate = 4 June 2014}}</ref> Hence he became the first Home Nations' player to contest a Test match against each of Australia, New Zealand, and South Africa.<ref name="STV">{{cite news | url = http://news.stv.tv/east-central/223379-david-bedell-sivright-the-british-lion-who-became-a-boxer/ | title = David Bedell-Sivright: British Lion, champion boxer and war surgeon | publisher = STV | last = Fraser | first = Graham | date = 30 April 2013 | accessdate = 1 June 2014}}</ref>

Bedell-Sivright was one of the inaugural inductees into the Scottish Rugby Hall of Fame in 2010,<ref name=HallofFame/> and in 2013 was inducted into the [[IRB Hall of Fame]] in a ceremony that honoured players from the British and Irish Lions (as the British Isles team is now known) and Australia during that year's [[2013 British and Irish Lions tour to Australia|Lions tour of Australia]].<ref>{{cite press release|url=http://www.irb.com/history/halloffame/newsid=2069489.html#legends+inducted+into+irb+hall+fame |title=Legends inducted into IRB Hall of Fame |publisher=International Rugby Board |date=18 November 2013 |accessdate=1 December 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131203011455/http://www.irb.com/history/halloffame/newsid=2069489.html |archivedate=3 December 2013 |df=dmy }}</ref> Edinburgh University RFC have a scholarship in Bedell-Sivright's honour; the scholarships of between £500 and £2000 per year are intended "to attract the best young talent in the UK [United Kingdom] to play and study at The University of Edinburgh".<ref name=EdinburghScholar>{{cite web | url = http://www.edunirugby.co.uk/#!rugby-scholarships/cb7k |title = The 'Bedell-Sivright' Scholarship Fund | publisher = Edinburgh University RFC | accessdate = 4 June 2014 }}</ref>

Writing in 1919, rugby journalist and author [[E. H. D. Sewell]] said of Bedell-Sivright "If a plebiscite was taken on the question: "Who was the hardest forward who ever played International football?" Sivright would get most votes if the voting was confined to players, and probably so in any event."<ref name="Sewell">{{cite web | url = http://www.edinburghs-war.ed.ac.uk/system/files/PDF_scottish_rugby_international_casualties.pdf | title = Scottish Rugby International Casualties | format = pdf | date = December 2012 | accessdate = 4 June 2014 | publisher = The University of Edinburgh | archiveurl = https://web.archive.org/web/20140604093246/http://www.edinburghs-war.ed.ac.uk/system/files/PDF_scottish_rugby_international_casualties.pdf | archivedate = 4 June 2014}}</ref> Bedell-Sivright is described as one of the "hardest" men to ever play for Scotland,<ref name=HallofFame/> with author Nick Oswald{{snd}}who wrote a book on the history of Scottish rugby{{snd}} describing him as "a very aggressive forward. He didn't excel in any one aspect of the game, but he was an absolutely ferocious competitor."<ref name="STV"/> On hearing of his death, it was reported that:<ref name="RefereeDeath">{{cite news |url=http://nla.gov.au/nla.news-article129353461 |title=Death of D. R. Bedell-Sivright |newspaper=[[The Referee (newspaper)|The Referee]] |location=Sydney |date=15 September 1915 |accessdate=6 June 2014 |page=12 |publisher=National Library of Australia}}</ref>
<!--quote is in the public domain as published anonymously in Australia before 1955-->{{Quote box|quote=It is cabled that Dr. D. R. Bedell-Sivright, who captained the British team in Australia and New Zealand in 1904, has died at the Dardanelles. He was a surgeon in the navy, and it is likely that he died on one of the warships engaged there against the Turks. Sivright was a Scottish forward of the most brilliant type, a hard player, but a clever one. He was one of the finest all-round forwards ever seen in Australia from over the seas. Among the forwards of the teams since 1899 he divided honours with [[Arthur Harding|A. F. Harding]], the famous Welshman, and, at his best, was fit for a world's team. Sivright as captain was somewhat' dour, but as [a] player he was magnificent. A man of superb physique, it is hard to think that he has died an ordinary death at his age, and not to a bullet from the enemy.|salign=right|source = ''The Referee'' (Sydney), 15 September 1915}}

== International record ==

{|class="wikitable" style="font-size: 90%; margin-left: 20px;"
|+ |Home Nations Championship appearances for Scotland<ref name="Scrum"/>
|-
!scope="col"|Country
!scope="col"|[[1900 Home Nations Championship|1900]]
!scope="col"|[[1901 Home Nations Championship|1901]]
!scope="col"|[[1902 Home Nations Championship|1902]]
!scope="col"|[[1903 Home Nations Championship|1903]]
!scope="col"|[[1904 Home Nations Championship|1904]]
!scope="col"|[[1905 Home Nations Championship|1905]]
!scope="col"|[[1906 Home Nations Championship|1906]]
!scope="col"|[[1907 Home Nations Championship|1907]]
!scope="col"|[[1908 Home Nations Championship|1908]]
|-
! scope="row" |[[England national rugby union team|England]] 
| align=center| 
| align=center|X 
| align=center|X 
| align=center|X   
| align=center|  
| align=center|
| align=center|X  
| align=center|X  
| align=center| 
|-
! scope="row" |[[Ireland national rugby union team|Ireland]]  
| align=center|  
| align=center|X 
| align=center|X 
| align=center|X  
| align=center|X  
| align=center| 
| align=center|X  
| align=center|X  
| align=center|X 
|-
! scope="row" |[[Wales national rugby union team|Wales]] 
| align=center|X 
| align=center|X 
| align=center|X 
| align=center|X   
| align=center|X 
| align=center|
| align=center|X
| align=center|X  
| align=center|X 
|}

{|class="wikitable" style="font-size: 90%; margin-left: 20px;"
|+ |Other Test appearances<ref name="Scrum"/>
|-
!scope="col"|Team
!scope="col"|Opponent
!scope="col"|Date
!scope="col"|Location
!scope="col"|Score
!scope="col"|Winner
|-
| align=left|[[British and Irish Lions|British Isles]]
||[[Australia national rugby union team|Australia]]
||2 July 1904
||Sydney
||0–17
||British Isles
|-
| align=left|[[Scotland national rugby union team|Scotland]]
||[[New Zealand national rugby union team|New Zealand]]
||18 November 1905
||Inverleith
||7–12
||New Zealand
|-
| align=left|[[Scotland national rugby union team|Scotland]]
||[[South Africa national rugby union team|South Africa]]
||17 November 1906
||Glasgow
||6–0
||Scotland
|}

==See also==
* [[List of international rugby union players killed in action during the First World War]]

==Footnotes==

===Notes===
{{Reflist|group=lower-alpha}}

===References===
{{reflist|2}}

==Bibliography==
* {{cite book| editor-last = Bath | editor-first = Richard |title = The Scotland Rugby Miscellany | publisher = Vision Sports Publishing Ltd | year = | isbn = 1-905326-24-6}}
* {{cite book| title = The Barbarians| first = Nigel | last = Starmer-Smith| publisher = MacDonald & Jane Publishers | year = 1977| isbn= 0-86007-552-4}}
* {{cite book | last = McCarthy | first = Winston | year = 1968 | title = Haka! The All Blacks Story | publisher = Pelham Books | location = London}}
* {{cite book | last = Elliott | first = Matt | year = 2012 | title = Dave Gallaher—The Original All Black Captain | publisher = HarperCollinsPublishers | location = Auckland, New Zealand | isbn = 978-1-86950-968-2}}

==Further reading==
* {{cite news | url = http://thescotsman.scotsman.com/sport/An-entire-team-wiped-out.5804230.jp |title = An entire team wiped out by the Great War | publisher=The Scotsman | date = 6 November 2009 | accessdate = 1 June 2014}}

{{British and Irish Lions 1903}}
{{British and Irish Lions 1904}}
{{WWI Scottish rugby fatalities}}

{{DEFAULTSORT:Bedell-Sivright, David Revell}}
[[Category:1880 births]]
[[Category:1915 deaths]]
[[Category:Scottish rugby union players]]
[[Category:Rugby union forwards]]
[[Category:Sportspeople from Edinburgh]]
[[Category:People educated at Fettes College]]
[[Category:British and Irish Lions rugby union players from Scotland]]
[[Category:Cambridge University R.U.F.C. players]]
[[Category:Barbarian F.C. players]]
[[Category:Edinburgh University RFC players]]
[[Category:Edinburgh Wanderers RFC players]]
[[Category:Fettesian-Lorretonian rugby union players]]
[[Category:World Rugby Hall of Fame inductees]]
[[Category:London Scottish F.C. players]]
[[Category:West of Scotland FC players]]
[[Category:Scotland international rugby union players]]
[[Category:Royal Navy officers]]
[[Category:Deaths from sepsis]]
[[Category:Burials at sea]]
[[Category:Infectious disease deaths in the Ottoman Empire]]
[[Category:Alumni of Trinity College, Cambridge]]
[[Category:Alumni of the University of Edinburgh]]
[[Category:British military personnel killed in World War I]]
[[Category:Royal Navy officers of World War I]]