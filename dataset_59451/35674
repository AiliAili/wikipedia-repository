{{Use dmy dates|date=March 2017}}
{{Use Australian English|date=March 2017}}
{{good article}}
{{Infobox roller coaster
| name                 = Abyss
| logo                 = Abyss logo.jpg
| logodimensions       = 150px
| image                = 
| imagedimensions      = 
| caption              = 
| previousnames        = 
| location             = Adventure World
| locationarticle      = Adventure World (amusement park)
| section              = <!--Must not be linked.-->
| subsection           = <!--Should be linked.-->
| coordinates          = {{Coord|-32.095482|115.817001|type:landmark|display=inline,title}}
| status               = Operating
| opened               = {{Start date|2013|11|01|df=yes}}
| soft_opened          = <!--Use {{Start date|df=yes|YYYY|MM|DD}} -->
| year                 = 2013
| closed               = <!--Use {{End date|df=yes|YYYY|MM|DD}} -->
| cost                 = A$12 million
| previousattraction   = 
| replacement          = 
| type                 = Steel
| type2                = Euro-Fighter
| type3                = <!--Must not be linked, will auto-categorize the coaster.-->
| manufacturer         = Gerstlauer
| designer             = 
| model                = Euro-Fighter &ndash; Custom
| track                = Custom
| lift                 = [[Chain lift hill]]
| height_m             = 30
| drop_m               = 30.5
| length_m             = 630
| speed_km/h           = 85
| inversions           = 3
| duration             = 2 minutes
| angle                = 100
| capacity             = 
| acceleration         = <!--Expression in full form e.g. "X to Y mph (χ to ψ km/h) in Z seconds". -->
| acceleration_from    = <!--Initial speed in mph or km/h defaults to zero, only numeric characters-->
| acceleration_mph     = <!--Final speed in mph may contain only numeric characters-->
| acceleration_km/h    = <!--Final speed in km/h may contain only numeric characters-->
| acceleration_in      = <!--Number of seconds may contain words -->
| gforce               = 4.5
| restriction_cm       = 125
| trains               = 4
| carspertrain         = 1
| rowspercar           = 2
| ridersperrow         = 4
| virtual_queue_name   = 
| virtual_queue_image  = <!--Use ONLY the filename, not a full [[File:]] link-->
| virtual_queue_status = <!--should be available if above fields are used-->
| single_rider         = <!--Must be "available" if available.-->
| accessible           = <!--Must be "available" if available.-->
| transfer_accessible  = <!--Must be "available" if available.-->
| custom_label_1       = 
| custom_value_1       = 
| custom_label_2       = 
| custom_value_2       = 
| custom_label_3       = 
| custom_value_3       = 
| custom_label_4       = 
| custom_value_4       = 
| rcdb_number          = 7444
}}

'''Abyss''' is a [[steel roller coaster]] located at the [[Adventure World (amusement park)|Adventure World]] amusement park in [[Perth, Western Australia]]. The $12-million attraction was announced in April 2013, and construction began the following month. Six months later, the ride opened to the general public on 1 November 2013.

The Abyss is a [[Gerstlauer Euro-Fighter|Euro-Fighter]], a roller coaster model from [[Gerstlauer]] that features a "beyond-vertical" first drop which exceeds 90 degrees. In addition to several [[Roller coaster inversion|inversions]], the Abyss reaches a top speed of {{Convert|85|km/h}} along the two-minute, {{Convert|630|m|ft|adj=mid|-long}} ride. The roller coaster has been well received by the public.

==History==
In mid-2012, the 15-month phase of planning and construction of ''Abyss'' began.<ref name=PW>{{cite journal|title=Abyss - Putting Western Australia on the theme park map|journal=Park World Magazine|date=November 2013|pages=55–56}}</ref> In April 2013, Adventure World announced on their [[Facebook]] page that they would be adding a $12 million, world-class attraction in 2013.<ref>{{cite web|title=It's time to share a $12m secret...|url=https://www.facebook.com/AdventureWorldOfficialPage/posts/10151576902304564|publisher=Facebook|accessdate=28 July 2013|author=Adventure World|date=24 April 2013}}</ref> By May 2013 construction had begun.<ref name="New attraction 2013 (code name Mi1)">{{cite web|title=New attraction 2013 (code name Mi1)|url=https://www.facebook.com/media/set/?set=a.10151586950779564.1073741829.92438924563&type=1|publisher=Facebook|accessdate=28 July 2013|author=Adventure World|date=1 May 2013}}</ref> In June 2013, a [[concrete slab]] was poured prior to the construction of a [[show building]].<ref name="New attraction 2013 (code name Mi1)" /> Shortly after track arrived in July 2013, Adventure World officially announced that the ride would be a Gerstlauer Euro-Fighter, similar to [[Saw – The Ride]].<ref name="Adventure World Perth Ride Announcement">{{cite news|last=Bonjolo|first=Natalie|title=Adventure World Perth Ride Announcement|url=https://www.youtube.com/watch?v=HqC-MM_LodE|accessdate=28 July 2013|newspaper=[[Today Tonight]]|date=23 July 2013|publisher=[[Seven West Media]]}}</ref> The ride was manufactured at the company's facilities in [[Germany]] before being shipped to Australia in forty-five {{Convert|40|ft|m|adj=mid|-long}} [[Intermodal container|shipping containers]].<ref name="Daily Mail">{{cite news|last=Driver|first=Carol|title=Hold tight! £7m rollercoaster with  shuttle launch G-force opens in Australia|url=http://www.dailymail.co.uk/travel/article-2483335/Hold-tight--7m-rollercoaster-G-force-stronger-shuttle-launch-opens-Australia.html|accessdate=8 November 2013|newspaper=Daily Mail|date=1 November 2013}}</ref> Construction of the ride was completed by Gerstlauer crews over seven weeks.<ref name="Edge of abyss for thrill of a ride">{{cite news|last=Tyrell|first=Claire|title=Edge of abyss for thrill of a ride|url=http://au.news.yahoo.com/thewest/a/-/breaking/19395857/edge-of-abyss-for-thrill-of-a-ride/|accessdate=8 November 2013|newspaper=The West Australian|date=15 October 2013}}</ref> The name of the attraction was revealed on the [[Friday the 13th]] of September 2013 as ''Abyss''. The announcement was made on Friday the 13th due to the ride's horror theme.<ref name="Adventure World announces $12m Eurofighter roller coaster">{{cite web|last=Wilson|first=Richard|title=Adventure World announces $12m Eurofighter roller coaster|url=http://www.parkz.com.au/article/2013/07/24/315-Adventure_World_announces_12m_Eurofighter_roller_coaster.html|publisher=Parkz|accessdate=28 July 2013|date=24 July 2013}}</ref><ref name="Adventure World coaster named Abyss">{{cite web|last=Wilson|first=Richard|title=Adventure World coaster named Abyss|url=http://www.parkz.com.au/article/2013/09/12/317-Adventure_World_coaster_named_Abyss.html|publisher=Parkz|accessdate=12 September 2013|date=12 September 2013}}</ref> ''Abyss'' officially opened to the public on 1 November 2013 as the single biggest investment in the park's history.<ref name="Abyss ride opens at WA's Adventure World">{{cite news|title=Abyss ride opens at WA's Adventure World|url=http://news.ninemsn.com.au/entertainment/2013/11/01/02/57/abyss-ride-opens-at-wa-s-adventure-world|accessdate=1 November 2013|newspaper=Ninemsn|date=1 November 2013|agency=Australian Associated Press}}</ref>

Adventure World's CEO Mark Shaw stated the addition of the ''Abyss'' was an attempt to elevate the park "from adventure park to theme park".<ref name=Opodo>{{cite news|title=Aussie theme park unveils new coaster|url=http://news.opodo.co.uk/NewsDetails/2013-11-04/Aussie_theme_park_unveils_new_coaster|accessdate=8 November 2013|newspaper=Opodo|date=4 November 2013}}</ref> The park expected double-digit attendance growth following the addition of the ride.<ref name=PW /> Prior to the 2013 opening of ''Abyss'', Adventure World's only roller coaster was ''[[Dragon Express]]'', a small junior roller coaster by [[Zamperla]].<ref>{{cite RCDB|coaster_name=Dragon Express|location=Adventure World|rcdb_number=2465|accessdate=8 November 2013}}</ref><ref>{{cite RCDB|coaster_name=Adventure World|location=Bibra Lake, Western Australia, Australia|rcdb_number=4937|accessdate=8 November 2013}}</ref> Adventure World previously operated the [[Anton Schwarzkopf]]-designed ''[[Turbo Mountain]]'', before its closure and removal in 2009.<ref>{{cite RCDB|coaster_name=Turbo Mountain|location=Adventure World|rcdb_number=1126|accessdate=8 November 2013}}</ref>

==Characteristics==
''Abyss'' is a custom Gerstlauer Euro-Fighter with {{Convert|630|m}} of track. The ride's vertical lift hill takes riders to a height of {{Convert|30|m}} before dropping {{Convert|30.5|m}} in a 100°, beyond-vertical drop.<ref name="Adventure World announces $12m Eurofighter roller coaster" /><ref name=RCDB /> The two-minute ride features a top speed of {{Convert|85|km/h}} and exerts up to 4.5 times [[G-force|the force of gravity]] on its riders.<ref name=RCDB>{{cite RCDB|coaster_name=Abyss|location=Adventure World|rcdb_number=7444|accessdate=1 November 2013}}</ref> ''Abyss'' contains three [[Roller coaster inversion|inversions]] including an [[inline twist]] enclosed in the ride's show building, as well as an [[Immelmann loop]] and a [[dive loop]] outdoors.<ref name="Adventure World announces $12m Eurofighter roller coaster" /><ref name=RCDB /><ref name="Perth to get $12m roller-coaster">{{cite news|last=Acott|first=Kent|title=Perth to get $12m roller-coaster|url=http://au.news.yahoo.com/thewest/a/-/newshome/18123558/perth-to-get-12m-roller-coaster/|accessdate=9 August 2013|newspaper=[[The West Australian]]|date=24 July 2013|agency=[[Seven West Media Limited]]}}</ref> The ride features four [[Train (roller coaster)|trains]] which each seat riders four-abreast in two rows.<ref name=RCDB /> The theming around the ride was manufactured in the [[Philippines]] and includes thirty {{Convert|3|m|ft|adj=mid|-tall}} ancient druid guardians, exposed tree roots, upturned trees, and an entrance archway.<ref name="Perth Now">{{cite news|title=Adventure World launches new spooky attraction the Abyss roller-coaster|url=http://www.perthnow.com.au/news/western-australia/adventure-world-launches-new-spooky-attraction-the-abyss-rollercoaster/story-fnhocxo3-1226751490223|accessdate=8 November 2013|newspaper=Perth Now|date=1 November 2013|agency=AAP}}</ref>

==Ride experience==
The ride begins at the [[station (roller coaster)|station]] inside the show building. Once the train departs the station, it makes a slow turn to the right. A quick drop follows, before the train rounds a series of left turns into the first inversion, an inline twist. The train emerges from the show building and makes a slight left towards the {{Convert|30|m|adj=on}} [[chain lift hill]]. Once at the top of the hill, riders drop down {{Convert|30.5|m}} at an angle of 100°. The second inversion of the ride, an Immelmann loop, is followed by an overbanked turn around. An [[air-time]] hill, where riders experience the feeling of [[weightlessness]], and a right turn leads into the [[mid-course brake run]]. The train exits the brake run with a left turn into a [[dive loop]]. A left turn leads the train into the final [[brake run]], before returning to the enclosed station.<ref name=RCDB /><ref name=POV>{{cite web|title=Abyss Roller Coaster POV Adventure World Perth Australia 2013 On-Ride|url=https://www.youtube.com/watch?v=rCfoMj4gdZE|work=Theme Park Review|publisher=YouTube|accessdate=8 November 2013|author=Adventure World|date=8 November 2013}}</ref>

==Reception==
''Abyss'' has been well received. Edwin Dickinson of the Australian Coaster Club praised ''Abyss'' for its smoothness, intensity and pacing. He described the ride as better than ''[[Superman Escape]]'' at [[Warner Bros. Movie World]], stating Adventure World "certainly lifted the bar for roller coasters in Australia".<ref name=ACC>{{cite web|last=Dickinson|first=Edwin|title=The Abyss|url=http://australiancoasterclub.com.au/wp-content/uploads/2013/12/The-ABYSS-verion-51.pdf|publisher=Australian Coaster Club|accessdate=1 January 2014|date=1 November 2013}}</ref><ref name=Praise>{{cite web|title=Praise for Abyss|url=http://adventureworld.net.au/#/news-events/praise-for-abyss!/|publisher=Adventure World|accessdate=1 January 2014}}</ref> The [[Australian Associated Press]] commended the ride's ability to continue to be thrilling after multiple rides.<ref name="Perth Now" /> Natalie Bonjolo of ''[[Today Tonight]]'' stated she didn't "know whether to laugh or cry" at the end of ''Abyss'', describing the ride as unbelievable.<ref name="TT launch">{{cite news|last=Bonjolo|first=Natalie|title=Today Tonight ride Abyss for the first time!|url=https://www.youtube.com/watch?v=1Pu-Yphxtk0|accessdate=1 January 2014|newspaper=Today Tonight|date=31 October 2013|publisher=YouTube}}</ref> Local [[Member of Parliament#Australia|MP]] Joe Francis described the ride as an "amazing piece of mechanical engineering".<ref name="Abyss ride opens at WA's Adventure World" />

==See also==
* [[2013 in amusement parks]]

==References==
{{Reflist|33em}}

==External links==
* {{Official website|http://www.abyss-the-ride.com.au/}}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* [http://adventureworld.net.au/#/rides-attractions/abyss/ ''Abyss'' on the official Adventure World website]
* {{RCDB|7444|Abyss}}

[[Category:Enclosed roller coasters]]
[[Category:Euro-Fighter roller coasters]]
[[Category:Roller coasters in Australia]]
[[Category:Roller coasters introduced in 2013]]
[[Category:Roller coasters manufactured by Gerstlauer]]
[[Category:Steel roller coasters]]