{{Orphan|date=August 2016}}

{{Infobox person
| name        = Marty Bax
| image       = 
| alt         = 
| caption     = 
| birth_name  = Martine Theodora
| birth_date  = {{Birth date|1956|11|10|df=y}}
| birth_place = [[Montreal, Quebec, Canada]]
| death_date  = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} or {{Death-date and age|death date†|birth date†}} -->
| death_place = 
|citizenship  = {{flag|Canada}}, {{flag|Netherlands}}
| other_names = 
|alma_mater   = [[Vrije Universiteit]], Amsterdam
| occupation  = Art historian
| known_for   = Leading scholar in the work of [[Piet Mondrian]]; [[modern art]] and [[Western Esotericism]]
|website      = {{url|http://baxart.com/}}}}

'''Marty (Martine Theodora) Bax''' (born 1956)  is a Dutch-Canadian art historian and art critic in modern art. Her specializations are the work of [[Piet Mondrian]] (Dutch: [[Piet Mondriaan]]) and the relationship between art and [[Western Esotericism]], especially Modern [[Theosophy]] and [[Anthroposophy]].

== Biography ==

Bax was born on 10 November 1956 in [[Montreal]] ([[Quebec]], [[Canada]]). Her parents were both journalists for the newspapers [[Nieuwe Rotterdamsche Courant]] and [[Algemeen Dagblad]] in [[Rotterdam]] [[Netherlands]]). In Canada her father Jack was a radio reporter for the [[Canadian Broadcasting Corporation]]. After remigration to the Netherlands he became Chief of Public Relations of the City and Port of Rotterdam. He was the first in the Netherlands to implement a public information center for inhabitants, in which city developments were openly discussed. In the 1960s he was one of the first who envisioned local radio and television as public information channels.<ref>{{cite book |author=Schaaf, Ben van der|title=De stad aan de man gebracht. 50 jaar gemeentevoorlichting in Rotterdam|publisher= Voorlichting Bestuursdienst Rotterdam |location=Rotterdam|year=1996|pages=53–67}}</ref>
Bax is the sister of the human rights activist [[Robert van Voren]] and of Jacky Bax, Programme Manager and Deputy Director at NRPO SIA / Taskforce for Applied Research, formerly Programme Manager Innovation Universities at [[Ministry of Education, Culture and Science]].

== Profession ==
Bax studied art history at the [[Vrije Universiteit]], [[Amsterdam]]. In 1987 she graduated on a thesis on the profound but still scholarly unrecognized influence of Japanese calligraphy on the work of the American Abstract Expressionist [[Franz Kline]]. In 2004 she received her PhD on the influence of Modern [[Theosophy]] on Dutch art between 1880 and 1920.

Bax' scholarly approach to art is interdisciplinary, combining art history and art analysis with (socioeconomic) history, sociology, philosophy, history of religion and genealogy. She works as an independent (co-)curator of and scholarly adviser to many international institutions on modern art from 1850. She has published many books and essays and wrote entries on Dutch architects for the [[Oxford Art Online]]. She has been editor of the university art historical magazine ''Kunstlicht'' and founder of its foundation, and editor-in-chief of the scholarly magazine ''Jong Holland''. As an art critic for [[Het Financieele Dagblad]] she has written approximately 500 articles on art, architecture, design, institutional and private collecting, and the art market. She organized various conferences, e.g. on [[Nazi plunder]] and [[cultural heritage]].

== Mondrian ==
''Piet Mondrian. The Amsterdam years 1892-1912'' (1994) contains the first extensive analysis of the extensive social and artistic network of [[Piet Mondrian]], based on genealogy and research in primary archival sources. In 1996 she was appointed editor of Volume I of the ''Catalogue Raisonné'' of Mondrian’s work.<ref>{{cite book|author1=Joosten, Joop |author2=Welsh, Robert |title=Piet Mondrian Catalogue Raisonné|publisher= V+K Publishing|location=Blaricum|year=1998 |isbn=9789066116214}}</ref> The book ''Mondrian Complete'' received the Choice Outstanding Academic Title Award in 2002. Ever since she publishes and lectures regularly on aspects of Mondrian’s life and art and serves as an authentication expert of his work.<ref>{{cite book|author=Wismer, B.|title=Ferdinand Hodler– Piet Mondrian. Eine Begegnung|publisher=Aargauer Kunsthaus|date=1998|isbn=9783907044780|pages=121&ndash;147}}</ref><ref>{{cite book|author1=Anfam, D. |author2=Adrichem, J. van |author3=Bax, M. |author4=Blotkamp, C. |title=Reflections on the Collection of the Stedelijk Museum Amsterdam|publisher=Stedelijk Museum Amsterdam|location=Amsterdam|date=2012|pages=157&ndash;168|isbn=9789462080027}}</ref><ref>{{cite web|url=http://www.mondriaan.nl/artikel/karakter_is_lot_mondriaans_horoscoop|title=Karakter is lot. Mondriaans horoscoop. (Lecture at the commemoration of Mondrian’s birthday on 7 March 2013)}}</ref><ref>{{cite book|title=Mondrian. Farbe|publisher=Bucerius Kunst Forum|location=Hamburg|date=2014|pages=22&ndash;30|isbn=9783777422046}}</ref><ref>{{cite book|author=Almqvist, K; Belfrage, L. (eds.)|title=Hilma af Klint. The Art of Seeing the Invisible|publisher=Axel and Margaret Ax:son Johnson Foundation|location=Stockholm|date=2015|pages=129&ndash;141|isbn=9789189672710}}</ref> In 2014 she launched the first 3D virtual multi-user Mondrian Museum together with ActiveWorlds Europe and DX Media.<ref>{{cite web|url=http://museum3d.eu|title=Museum3D}}</ref>

== Western Esotericism ==
Bax started her research into art and Western Esotericism after the exhibition ''The Spiritual in Art: Abstract Painting 1890-1985'' (1986-1987), of which Bax was assistant-curator at the [[Gemeentemuseum Den Haag]] in The Hague.
 
In 1991 she published ''Bauhaus Lecture Notes 1930-1933'', in which she describes the continuing influence of Western Esotericism  on the theory and practice of the Bauhaus, right until its closing in 1933.

The exhibition ''Okkultismus und Avantgarde'' (1995), of which Bax was member of the scholarly board and organizer of the Dutch section, was the first exhibition to focus exclusively on the influence of Western Esotericism on European art, thus opening a new field of art historical research.

In 1996 she joined the study group ARIES, founded by [[Wouter Hanegraaff]] and precursor of the [[European Society for the Study of Western Esotericism]]. As a member of ESSWE she contributes to international conferences, lectures and scholarly discussion groups and sharing research data.

In 2001 she was co-founder of the Stichting ter bevordering van wetenschappelijk Onderzoek naar de geschiedenis van de Vrijmetselarij en verwante stromingen in Nederland (OVN; Foundation for the advancement of academic research into the history of freemasonry and related currents in the Netherlands). Main focus points of this foundation are research funding, the preservation of archives, and architectural heritage.

Her dissertation ''Het web der schepping. Theosofie en kunst in Nederland van Lauweriks tot Mondriaan'' (The Web of Creation. Theosophy and art in The Netherlands from Lauweriks to Mondrian, Vrije Universiteit 2004), published in 2006 by Uitgeverij SUN, is the first systematic and interdisciplinary analysis of the relationship between art and Modern Theosophy. It contains a [[prosopography]] of the members of the Dutch branch of the Theosophical Society, which gives insight into the social and religious structure of the Society. Although the book focuses on The Netherlands between 1880 and 1920, it has set an empirical-methodological standard for any research in this complex field of art history.

The exhibition ''Holy Inspiration. Religion and Spirituality on Modern Art'' (2008) was the first exhibition in the history of the strictly modernist [[Stedelijk Museum Amsterdam]] to focus on the religious, spiritual and Western Esoteric sources of inspiration of modern artists in the collection, based on the views of [[Jürgen Habermas]]. Parallel she contributed to [[Traces du Sacré]] held at the [[Centre Pompidou]].
In 2010 Bax made the full membership list of the Theosophical Society available online as a primary source for scholarly and family research.<ref>{{cite web |url=http://tsmembers.org}}</ref>

In 2010 she became interested in the life of [[Grete Trakl]], musical prodigy and sister of the Austrian poet [[Georg Trakl]], because of her notes on lectures by [[Rudolf Steiner]]. Research resulted in the first comprehensive biography of Grete Trakl, published in 2014. This book contains several chapters on her brother’s position within the tradition of Western Esotericism.

==Selected bibliography==
*{{cite book|title=Bauhaus Lecture Notes 1930-1933. Theory and practice of archi¬tectural training at the Bauhaus, based on the lecture notes made by the Dutch ex-Bauhaus stu¬dent and architect J.J. van der Linden of the Mies van der Rohe curriculum|publisher=Architectura & Natura; Brill|location=Amsterdam & Leiden|date=1991|isbn=9789071570049}}
*{{cite book|title=Een verzameling &ndash; Verstilde momenten|publisher=Caldic|location=Rotterdam|year=1992}}
*{{cite book|title=Organic architecture. Rudolf Steiner's building impulse|publisher=Iona Stichting|location=Amsterdam|date=1993}}
*{{cite book|title=Mondriaan aan de Amstel 1892-1912|publisher=Gemeentearchief Amsterdam|location=Amsterdam|date=1994|isbn=9789080175020}}; {{cite book|title=Mondrian. The Amsterdam Years 1892-1912|publisher=Thoth|location=Bussum|date=1994|isbn=9789080175013}}; {{cite book|title=Il primo Mondrian: Gli anni di Amsterdam|publisher=Galeria Nazionale d'Arte Moderna|location=Rome|date=1995|isbn=9788880161370}}
*{{cite book|title=La beauté exacte. De Van Gogh à Mondrian. L'Art aux Pays-Bas au XXième siècle|publisher=Musée d'Art Moderne de la Ville de Paris|location=Paris|date=1994|pages=40&ndash;45|isbn=9782879001722}}
*{{cite book|title=Bloeiende symbolen. Bloemen in de kunst van het fin de siècle|publisher=Noordbrabants Museum|location=’s Hertogenbosch|date=1999|isbn=9789071349140}}; {{cite book|title=Innocence and decadence. Flowers in Northern European art 1880-1914|publisher=Pallant House Gallery|location=Chichester|date=1999|isbn=1869827252}}; {{cite book|title=Symboles en fleurs. Les fleurs dans l'art autour de 1900|publisher=Institut Néerlandais|location=Paris|date=1999|isbn=9789053493045}}
*{{cite book|title=Mondriaan compleet|publisher=Atrium|location=Alphen aan den Rijn|date=2001|isbn=9789066139299}};{{cite book|title=Complete Mondrian|publisher=Ashgate (Lund Humphries)|date=2001|isbn=9780853318033}}; {{cite book|title= Mondrian complet|publisher=Hazan|location=Paris|date=2002|isbn=9782850258039}}
*{{cite book|author=Kroon (ed.), Andréa; Bax (ed.), Marty|title=Masonic and Esoteric Heritage. New Perspectives for Art and Heritage Policies|publisher=Stichting OVN|location=’s Gravenhage|date=2005|isbn=908077782X}}
*{{cite book|title=Het web der schepping. Theosofie en kunst in Nederland van Lauweriks tot Mondriaan|publisher=Uitgeverij SUN|location=Amsterdam|date=2006|isbn=9789085061922}}
*{{cite book|author=Berg, H; Frishman,J. (eds.)|title=Dutch Jewry in a Cultural Maelstrom, 1850-1940|publisher=Amsterdam University Press|location=Amsterdam|date=2007|isbn= 9789052602684|pages=127&ndash;134.}}
*{{cite book|title=Karel de Bazel (1869-1923). Vormgever van een nieuwe wereld|publisher=Bekking&Blitz|location=Amersfoort|date=2007|isbn=9789061095972}}
*{{cite book|title=Stedelijk Museum in de Nieuwe Kerk. Heilig Vuur. Religie en spiritualiteit in de moderne kunst &ndash; Stedelijk Museum in De Nieuwe Kerk &ndash; Holy Inspiration. Religion and Spirituality in Modern Art|publisher=De Nieuwe Kerk &ndash; Stedelijk Museum|location=Amsterdam|date=2008|isbn=9789078653080}}
*{{cite book|title=Van privé naar publiek. Kunst verzamelen in Nederland &ndash; From private walls to public halls. Collecting Art in the Netherlands|publisher=Accenture – Stichting Lieve|location=Amsterdam|date=2008}}
*{{cite book|title=Albrecht Dürer in de Nederlanden|publisher=Bekking&Blitz|location=Amersfoort|date=2010|isbn=9789061091158}}; {{cite book|title=Albrecht Dürer aux anciens Pays-Bas|publisher=Bekking&Blitz|location=Amersfoort|date=2011|isbn=9789061093312}}
*{{cite book|title=Ben Joppe. Schilder van het onmogelijke|publisher=Stichting Ben Joppe|location=Zierikzee|date=2011}}
*{{cite book|title=Immer zu wenig Liebe. Grete Trakl. Ihr feinster Kuppler. Ihre Familie|publisher=DX Media|location=Amsterdam|date=2014|isbn=9789082362404}}

==References==
{{reflist}}

==External links==
* Website: [http://baxart.com]
* Virtual Museum: [http://museum3d.com]
* Membership list Theosophical Society 1875-1942: [http://tsmembers.org]
* ESSWE: [http://esswe.org]

{{Authority control|LCCN=n/91/014030}}

{{DEFAULTSORT:Bax, Marty}}
[[Category:1956 births]]
[[Category:Living people]]
[[Category:20th-century Dutch writers]]
[[Category:21st-century Dutch writers]]
[[Category:Canadian emigrants to the Netherlands]]
[[Category:Canadian people of Dutch descent]]
[[Category:Dutch non-fiction writers]]
[[Category:VU University Amsterdam alumni]]
[[Category:Writers from Montreal]]