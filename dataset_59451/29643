{{Taxobox
| image = Agaricus texensis 65791.jpg
| image_width = 234px
| image_caption =
| regnum = [[Fungus|Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Agaricales]]
| familia = [[Agaricaceae]]
| genus = ''[[Agaricus]]''
| species = '''''A. deserticola'''''
| binomial = ''Agaricus deserticola''
| binomial_authority = G.Moreno, Esqueda & Lizárraga (2010)
| synonyms_ref = <ref name="url Fungorum synonymy: Agaricus texensis"/><ref name=Zeller1943a/>
| synonyms = 
''Secotium texense'' <small>[[Miles Joseph Berkeley|Berk.]] & [[Moses Ashley Curtis|M.A.Curtis]] (1873)</small><br>
''Gyrophragmium texense'' <small>(Berk. & M.A.Curtis) [[George Edward Massee|Massee]] (1891)</small><br>
''Secotium decipiens'' <small>[[Charles Horton Peck|Peck]] (1895)</small><br>
''Podaxon strobilaceus'' <small>[[Edwin Bingham Copeland|Copeland]] (1904)</small><br>
''Gymnopus texensis'' <small>(Berk. & M.A.Curtis) [[William Alphonso Murrill|Murrill]] (1916)</small><br>
''Longia texensis'' <small>(Berk. & M.A.Curtis) [[Sanford Myron Zeller|Zeller]] (1943)</small><br>
''Longula texensis'' <small>(Berk. & M.A.Curtis) Zeller (1945)</small><br>
''Agaricus texensis'' <small>(Berk. & M.A.Curtis) Geml, Geiser & Royse (2004)</small>
}}
'''''Agaricus deserticola''''', commonly known as the '''gasteroid agaricus''', is a species of [[fungus]] in the family [[Agaricaceae]]. Found only in southwestern and western North America, ''A.&nbsp;deserticola'' is adapted for growth in dry or [[semi-arid]] habitats. The [[basidiocarp|fruit bodies]] are [[secotioid]], meaning the [[spore]]s are not forcibly discharged, and the [[pileus (mycology)|cap]] does not fully expand. Unlike other ''[[Agaricus]]'' species, ''A.&nbsp;deserticola'' does not develop true [[lamella (mycology)|gills]], but rather a convoluted and [[anastomosis|networked]] system of [[spore]]-producing tissue called a [[gleba]]. When the [[partial veil]] breaks or pulls away from the stem or the cap splits radially, the blackish-brown gleba is exposed, which allows the spores to be dispersed.

The fruit bodies can reach heights of {{convert|18|cm|in|abbr=on}} tall with caps that are up to {{convert|7.5|cm|in|1|abbr=on}} wide. The tough woody [[stipe (mycology)|stems]] are {{convert|1|–|2|cm|in|1|abbr=on}} wide, thickening towards the base. Fruit bodies grow singly or scattered on the ground in fields, grasslands, or arid ecosystems. Other mushrooms with which ''A.&nbsp;deserticola'' might be confused include the [[desert fungi|desert fungus]] species ''[[Podaxis pistillaris]]'' and ''[[Montagnea arenaria]]''. The [[edible mushroom|edibility]] of ''Agaricus deserticola'' mushrooms is not known definitively. Formerly named ''Longula texensis'' (among several other [[synonym (taxonomy)|synonyms]]), the fungus was transferred to the genus ''Agaricus'' in 2004 after [[molecular phylogenetics|molecular]] analysis showed it to be closely related to species in that genus. In 2010, its [[specific name (botany)|specific epithet]] was changed to ''deserticola'' after it was discovered that the name ''Agaricus texensis'' was [[Illegitimate name|illegitimate]], having been previously published for a different species.

{{mycomorphbox
| name = ''Agaricus deserticola''
| whichGills = free
| capShape = convex
| hymeniumType=gills
| stipeCharacter=ring
| stipeCharacter2=bare
| ecologicalType=saprotrophic
| sporePrintColor=purple-black
| howEdible=edible
| howEdible2=unknown}}

==Taxonomic history==
The species was first [[species description|described]] scientifically as ''Secotium texense'' by [[Miles Joseph Berkeley]] and [[Moses Ashley Curtis]] in 1873, based on specimens sent to them from western Texas.<ref name=Berkeley1873/> [[George Edward Massee]] transferred it to the genus ''Gyrophragmium'' in 1891, because of its resemblance to the species ''[[Gyrophragmium delilei]]'', and because he felt that the structure of the volva as well as the internal [[morphology (biology)|morphology]] of the gleba excluded it from ''Secotium''.<ref name=Massee1891/> In 1916, [[William Alphonso Murrill|William Murrill]] listed the species in ''[[Gymnopus]]'', but did not explain the reason for the generic transfer.<ref name=Murrill1916/> In a 1943 publication, [[Sanford Myron Zeller|Sanford Zeller]] compared a number of similar [[secotioid]] genera: ''[[Galeropsis]]'', ''[[Gyrophragmium]]'' and ''[[Montagnea]]''. He concluded that the species did not fit in the limits set for the genus ''Gyrophragmium'' and so created the new genus ''Longia'' with ''Longia texensis'' as the [[type species]]. The generic name was to honor [[William Henry Long]], an American mycologist noted for his work in describing [[Gasteromycetes]]. Zeller also mentioned two additional [[synonym (taxonomy)|synonyms]]:<ref name=Zeller1943a/> ''Secotium decipiens'' ([[Charles Horton Peck|Peck]], 1895),<ref name=Peck1895/> and ''Podaxon strobilaceous'' ([[Edwin Bingham Copeland|Copeland]], 1904).<ref name=Copeland1904/>

Two years later in 1945, Zeller pointed out that the use of the name ''Longia'' was untenable, as it had already been used for a genus of [[rust (fungus)|rusts]] described by [[Hans Sydow]] in 1921,<ref>''Longia'' as described by Sydow in 1921 is currently known as ''[[Haploravenelia]]''. {{cite book |vauthors =Kirk PM, Cannon PF, Minter DW, Stalpers JA |title=Dictionary of the Fungi |edition=10th |publisher=CAB International |location=Wallingford, UK |year=2008 |page=392 |isbn=0-85199-826-7}}</ref> so he proposed the name ''Longula'' and introduced the new combination ''Longula texensis'' in addition to ''L.&nbsp;texensis'' var. ''major''.<ref name=Zeller1945/> The species was known by this name for about 60 years, until a 2004 [[phylogenetic]] study revealed the taxon's close evolutionary relationship with ''[[Agaricus]]'',<ref name=Geml2004/><ref name=Geml2004b/> a possibility insinuated by [[Curtis Gates Lloyd]] a century before.<ref name=Lloyd1904/> This resulted in a new name in that genus, but it soon came to light that the name ''Agaricus texensis'' had already been used, ironically enough, by Berkeley and Curtis themselves in 1853,<ref name=Berkeley1853/> for a taxon now treated as a synonym of ''[[Flammulina velutipes]]''.<ref name=NYBG/> Since this made the new ''Agaricus texensis'' an [[homonym (biology)|unusable homonym]], Gabriel Moreno and colleagues published the [[nomen novum|new name]] ''Agaricus deserticola'' in 2010.<ref name=Moreno2010/> The mushroom is [[common name|commonly]] known as the gasteroid Agaricus.<ref name=Arora1986/>

==Classification and phylogeny==
{{cladogram|align=left|title=
|clade=
{{clade
|style=font-size:75%;line-height:75%
|label1=
|1={{clade
     |1={{clade
        |1={{clade
           |1={{clade
              |1=''[[Agaricus lanipies|A. lanipes]]''
              |2=''[[Agaricus maskae|A. maskae]]''
              |3=''[[Agaricus subrutilescens|A. subrutilescens]]''
              }}
           |2=''[[Agaricus silvaticus|A. silvaticus]]'' var. ''pallidus''
           }}
        |2=''[[Agaricus aridicola|A. aridicola]]''
        }}
     |2='''''A. deserticola'''''
   }}
}}
|caption=Cladogram showing the phylogeny and relationships of ''Agaricus deserticola'' and related ''Agaricus'' species based on [[ribosomal DNA]] sequences.<ref name=Geml2004/>
}}
The [[classification (biology)|classification]] of ''Agaricus deserticola'' has been under debate since the taxon was first described. It was thought by some mycologists to be a member of the [[Gasteromycetes]], a grouping of fungi in the basidiomycota that do not [[ballistospore|actively discharge]] their spores. The Gasteromycetes are now known to be an artificial assemblage of [[morphology (biology)|morphologically]] similar fungi without any unifying [[monophyly|evolutionary relationship]]. When the species was known as a ''Gyrophragmium'', Fischer thought it to be close to ''Montagnites'', a genus he considered a member of the  family [[Agaricaceae]].<ref name=Fischer1900/> Conrad suggested a relationship with ''Secotium'', which he believed to be close to ''Agaricus''.<ref name=Conrad1915/> [[Curtis Gates Lloyd]] said of ''Gyrophragmium'': "[it] has no place in the Gasteromycetes. Its relations are more close to the Agarics. It is the connecting link between the two passing on one hand through ''Secotium'' to the true Gasteromycetes."<ref name=Lloyd1904/> Morse believed that ''Gyrophragmium'' and the [[secotioid]] genus ''[[Endoptychum]]'' formed a transition between the Gasteromycetes and the [[Hymenomycetes]] (the gilled fungi).<ref name=Morse1933/>

The species is now thought to have evolved from an ''Agaricus'' ancestor, and adapted for survival in dry habitats.<ref name=Geml2004/><ref name=Geml2004b/> These adaptations include: a cap that does not expand (thus conserving moisture); dark-colored gills that do not forcibly eject spores (a mechanism known to depend on [[turgor pressure]] achievable only in sufficiently hydrated environments); and a [[partial veil]] that remains on the fruit body long after it has matured.<ref name=Mycoweb/> This form of growth is called [[secotioid]] development, and is typical of other desert-dwelling fungi like ''[[Battarrea phalloides]]'', ''[[Podaxis pistillaris]]'', and ''[[Montagnea arenaria]]''. [[Molecular phylogenetics|Molecular]] analysis based on the sequences of the partial [[60S|large subunit]] of [[ribosomal DNA]] and of the [[internal transcribed spacer]]s shows that ''A.&nbsp;deserticola'' is closely related to but distinct from ''[[Agaricus aridicola|A.&nbsp;aridicola]]''.<ref name=Geml2004/> A separate analysis showed ''A.&nbsp;deserticola'' to be closely related to ''[[Agaricus arvensis|A.&nbsp;arvensis]]'' and ''[[Agaricus abruptibulbus|A.&nbsp;abruptibulbus]]''.<ref name=Capelari2006/>

==Description==
{{Multiple image|direction=vertical|align=left|image1=Agaricus texensis 65794.jpg|image2=Agaricus texensis 65797.jpg|width=200|caption1=These young fruit bodies have not yet formed scales, and have a [[peridium]] enveloping the cap.|caption2=In mature mushrooms, the peridium rips to reveal the dark brown, lamellate [[gleba]] underneath.}}
The [[basidiocarp|fruit body]] of ''Agaricus deserticola'' can grow up to {{convert|5|to|18|cm|in|1|abbr=on}} in height. Fresh specimens are usually white, but will age to a pale [[tan (color)|tan]]; dried fruit bodies are light gray or tan mixed with some yellow.<ref name=Barnett1943/> The [[pileus (mycology)|cap]] is {{convert|4|to|7.5|cm|in|1|abbr=on}} in diameter, initially conic, later becoming convex to broadly convex as it matures.<ref name=Miller2006/> The cap is composed of three distinct tissue layers: an outer [[volva (mycology)|volval]] layer, a middle cuticular layer ([[pileipellis|cutis]]), and an inner ([[trama (mycology)|tramal]]) layer which supports the [[gleba]]. The surface of the cap is white with yellow-brown to brown-tipped raised small scales; these scales result from the breakup of the volva and the cutis.<ref name=Harding1957/>

Initially, the caps are covered by a [[peridium]]—an outer covering layer of tissue. After the fruit body matures and begins to dry out, the lower part of the peridium starts to rip, usually starting from small longitudinal slits near where the peridium attaches to the top of the stem. However, the pattern of tearing is variable; in some instances, the slits may appear higher up on the peridium, in others, the peridium rips more irregularly.<ref name=Lloyd1904/><ref name=Barnett1943/> The peridium may also rip in such a way that it appears as if there is a [[annulus (mycology)|ring]] at the top of the stem. The torn peridium exposes the internal gleba. The gleba is divided into wavy plates or [[lamella (mycology)|lamellae]], some of which are fused together to form irregular chambers. The gleba is a drab brown to blackish-brown color, and it becomes tough and brittle as it dries out. The [[trama (mycology)|flesh]] is firm when young, white, and will stain light to bright yellow when it is bruised.<ref name=Arora1986/>

The [[stipe (mycology)|stem]] is cylindrical, {{convert|4|to|15|cm|in|1|abbr=on}} long and {{convert|1|to|2|cm|in|1|abbr=on}} thick.  It is shaped like a narrow club, and the base may reach widths up to {{convert|4.5|cm|in|1|abbr=on}}. It is typically white, staining yellow to orange-yellow or pink when bruised, and becomes woody with age.<ref name=Arora1986/><ref name=Harding1957/> Mature specimens develop longitudinal grooves in maturity.<ref name=Miller1988/> Numerous white [[rhizoid]]s are present at the base of the stem; these root-like outgrowths of fungal [[mycelium]] help the mushroom attach to its [[substrate (biology)|substrate]].<ref name=Harding1957/> The apex of the stem extends into the gleba to form a ''columella'' that reaches the top of the cap. The internal gills are free from attachment to the stem,<ref name=Zeller1943a/> but are attached full-length to the inside of the cap.<ref name=Harding1957/> The [[partial veil]] is thick, white, and often sloughs off as the cap expands.<ref name=Miller2006/>

A larger [[variety (botany)|variety]] of the mushroom has been described by Zeller,<ref name=Zeller1943a/> ''A.&nbsp;deserticola'' var. ''major'' (originally ''Longula texensis'' var. ''major''), whose range overlaps that of the typical variety. Its caps are scalier than the typical variety, and range from {{convert|6|to|12|cm|in|1|abbr=on}} or more in diameter, with a stem {{convert|10|to|25|cm|in|1|abbr=on}} and up to {{convert|4.5|cm|in|1|abbr=on}} thick.<ref name=Arora1986/><ref name=Harding1957/>

===Microscopic characteristics===
In deposit, such as with a [[spore print]], the [[spore]]s appear almost black, tinged with purple.<ref name=Barnett1943/> The spores are spherical in shape or nearly so, smooth, thick-walled, and lack a [[germ pore]]. They are [[amyloid (mycology)|nonamyloid]] (not absorbing [[iodine]] when [[staining|stained]] with [[Melzer's reagent]]), black-brown, and have dimensions of 4.5–7.5 by 5.5–6.5&nbsp;[[micrometre|µm]].<ref name=Miller2006/> There is a prominent scar where the spore was once attached to the [[basidium]] (the spore-bearing cell) through the [[sterigma]]. The basidia are broadly club-shaped, and mostly four-spored, with long, slender sterigmata. Unlike other ''Agaricus'' species, the spores of ''A.&nbsp;deserticola'' are not shot off, but are instead dispersed when they sift out of the dried, mature fruit bodies after the peridium breaks open.<ref name=Zeller1943a/>

Schaeffer's [[Chemical tests in mushroom identification|chemical test]] is often used to help identify and differentiate ''Agaricus'' species. In this test, [[aniline]] plus [[nitric acid]] are applied to the surface of the fruit body, and if positive, a red or orange color forms.<ref name=Rumach1994/> ''Agaricus deserticola'' has a positive Schaeffer's reaction, similar to species in [[section (biology)|section]] ''Arvensis'' in the genus ''Agaricus''.<ref name=Geml2004/>

===Similar species===
{{multiple image
| align = right
| image1 = Montagnea arenaria 82232.jpg
| width1 = {{#expr: (150 * 2441 / 2760) round 0}}
| caption1 = ''Montagnea arenaria''
| image2 = Podaxis pistillaris.jpg
| width2 = {{#expr: (150 * 1130 / 1296) round 0}}
| caption2 = ''Podaxis pistillaris''
}}
Species that resemble ''A.&nbsp;deserticola'' include the [[desert fungi]] ''[[Montagnea arenaria]]'' and ''[[Podaxis pistillaris]]''.<ref name=Mycoweb/> ''Montagnea arenaria'' is a whitish stalked [[puffball]] with a hollow, woody stalk and a loose sac-like volva at the base of the stem. It is topped by a thin disc-like cap with blackish gill plates suspended around the margin. ''Podaxis pistillaris'' has a cylindrical to oval white to brownish cap with a paper-thin wall atop a slender stem. When mature, the cap contains powdery, dark brown spores.<ref name=Arora1986/>

===Edibility===
The [[edible mushroom|edibility]] of the fruit bodies of ''Agaricus deserticola'' is not known definitively, and there are conflicting opinions in the literature, with some sources claiming that the edibility is unknown, and consumption should be avoided.<ref name=Mycoweb/><ref name="urlRogers Mushrooms"/> However, one popular [[field guide]] to North American mushrooms suggests they are edible when they are young, and have a pleasant odor and mild taste.<ref name=Miller2006/>

==Fruit body development==
[[image:Agaricus texensis 65799.jpg|thumb|left|Young and old specimens cut lengthwise to reveal internal tissues]]
In one early study of the mushroom's development, the fruit bodies appeared above the surface of the ground two or three days after rainfall or an irrigation, and required between five and eight days to mature. Slender and fragile [[rhizomorph]]s—dense masses of [[hypha]]e that form root-like structures—grow horizontally {{convert|2.5|to|5|cm|in|1|abbr=on}} below the soil surface. Fruit bodies start as enlarged tips on the rhizomorphs, and manifest as numerous small, almost-spherical protuberances just beneath the surface of the soil. When the fruit bodies reach a diameter of about {{convert|4|to|6|mm|in|abbr=on}}, the stem and peridial regions begin to be distinguishable; the peridial region first appears as a small swelling at the apex of the much larger stem regions.<ref name=Barnett1943/>

The fruit bodies push upward through the soil when they are about {{convert|2|cm|in|1|abbr=on}} tall. As growth progresses, the stem elongates and the peridium becomes more rounded, increasing in size until maturity. At about the time the peridium reaches {{convert|1|cm|in|1|abbr=on}} or slightly more in diameter, the columella exerts an upward tension on the tissue of the [[partial veil]], and it begins to pull away from the stem. Typically, the veil tissue is weakest near the attachment to the stem, rather than to the attachment at the edge of the peridium, and the veil separates from the stem. The lower edge of the peridium is further stretched as it is pulled upward and outward. Usually, the arid environment causes the gleba to dry out rapidly. If the veil tissue at the base of the stem is  stronger than that attached to the edge of the peridium, the veil can rip so it remains attached to the stem as a [[annulus (mycology)|ring]]. Scales begin to appear on the surface of the peridium of some specimens at about this time.<ref name=Barnett1943/>
{{Clear}}

==Habitat and distribution==
Like other ''Agaricus'' species, ''A.&nbsp;deserticola'' is [[saprobic]]—feeding off dead or decaying organic matter. The fruit bodies are found growing singly to sometimes more numerous, at low elevations, and typically in sandy soil. 
The species' usual habitats include drylands, [[coastal sage scrub]], and desert ecosystems.<ref name=Miller2006/> It also grows in lawns and fields.<ref name="urlRogers Mushrooms"/> The range of the fungus is restricted to southwestern and western North America, where it fruits throughout the year, typically during or following cold, wet weather.<ref name=Miller2006/> Zeller gives a range that includes as its eastern border central Texas, and extends westward to [[San Diego County, California]], and north to [[Josephine County, Oregon]].<ref name=Zeller1943a/> The mushroom used to be common in the [[San Francisco Bay area]] before land development reduced its preferred habitats.<ref name=Arora1986/> ''A.&nbsp;deserticola'' has been collected in several states in northwestern Mexico, including [[Sonora]],<ref name=Moreno2007/> [[Chihuahua (state)|Chihuahua]],<ref name=Moreno2010/> and [[Baja California]].<ref name=Ochoa2006/>

==See also==
{{Portal|Fungi}}
*[[List of Agaricus species|List of ''Agaricus'' species]]

==References==

{{Reflist|colwidth=30em|refs=

<ref name=Arora1986>{{cite book |author =Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |pages=725–30 |isbn=0-89815-169-4 |url=https://books.google.com/books?id=S-RmabYsjI4C&pg=PA730}}</ref>

<ref name=Barnett1943>{{cite journal |author =Barnett HL. |year=1943 |title=The development and structure of ''Longia texensis'' |journal=Mycologia |volume=35 |issue=4 |pages=399–408 |jstor=3754592 |doi=10.2307/3754592 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0035/004/0399.htm}}</ref>

<ref name=Berkeley1853>{{cite journal |vauthors =Berkeley MJ, Curtis MA |year=1853 |title=Centuries of North American fungi |journal=Annals and Magazine of Natural History |volume=12 |series=II |pages=417–35 |url=http://biodiversitylibrary.org/page/22191853}}</ref>

<ref name=Berkeley1873>{{cite journal |author =Berkeley MJ. |year=1873 |title=Notices of North American fungi (cont.) |journal=Grevillea |volume=2 |issue=15 |pages=33–5 |url=http://www.cybertruffle.org.uk/cyberliber/59649/0002/015/0033.htm}}</ref>

<ref name=Capelari2006>{{cite journal |title=Description and affinities of ''Agaricus martineziensis'', a rare species |journal=Fungal Diversity |vauthors =Capelari M, Rosa LH, ((Lachance M-A)) |year=2006 |volume=21 |pages=11–8 |url=http://www.fungaldiversity.org/fdp/sfdp/21-2.pdf |format=PDF}}</ref>

<ref name=Conrad1915>{{cite journal |author =Conrad HS. |year=1915 |title=The structure and development of ''Secotium agaricoides'' |jstor=3753132 |journal=Mycologia |volume=7 |pages=94–104 |doi=10.2307/3753132 |issue=2 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0007/002/0094.htm}}</ref>

<ref name=Copeland1904>{{cite journal |author =Copeland EB. |title=New and interesting California fungi |journal=Annales Mycologici |year=1904 |volume=2 |issue=1 |pages=1–7 |url=http://www.cybertruffle.org.uk/cyberliber/59685/0002/001/0001.htm}}</ref>

<ref name=Fischer1900>{{cite book |author =Fischer E. |year=1900 |chapter=Hymenogastrineae |title=Natürlichen Pflanzenfamilien I |volume=1 |veditors=Engler A, Krause K, ((Pilger RKF)), ((Prantl KAE)) |pages=296–313 |location=Leipzig, Germany |publisher=W. Engelmann |url=http://biodiversitylibrary.org/page/3622404 |language=German}}</ref>

<ref name=Geml2004>{{cite journal |vauthors =Geml J, Geiser DM, Royse DJ |year=2004 |title=Molecular evolution of ''Agaricus'' species based on ITS and LSU rDNA sequences |journal=Mycological Progress |volume=3 |issue=2 |pages=157–76 |doi=10.1007/s11557-006-0086-8}}</ref>

<ref name=Geml2004b>{{cite journal |author =Geml J. |year=2004 |title=Evolution in action: Molecular evidence for recent emergence of secotioid genera ''Endoptychum'', ''Gyrophragmium'' and ''Longula'' from ''Agaricus'' ancestors |journal=Acta Microbiologica et Immunologica Hungarica |volume=51 |issue=1–2 |pages=97–108 |pmid=15362291 |doi=10.1556/AMicr.51.2004.1-2.7}}</ref>

<ref name=Harding1957>{{cite journal |author =Harding PR Jr. |year=1957 |title=Notes on ''Longula texensis'' var. ''major'' |journal=Mycologia |volume=49 |issue=2 |pages=273–6 |jstor=3755637 |doi=10.2307/3755637 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0049/002/0273.htm}}</ref>

<ref name=Lloyd1904>{{cite journal |author =Lloyd CG. |year=1904 |title=''Gyrophragmium decipiens'' |journal=Mycological Notes |volume=18 |page=196}}</ref>

<ref name=Massee1891>{{cite journal |author =Massee G. |year=1891 |title=New or imperfectly known Gasteromycetes |journal=Grevillea |volume=19 |issue=92 |pages=94–8 |url=http://www.cybertruffle.org.uk/cyberliber/59649/0019/092/0094.htm}}</ref>

<ref name=Miller1988>{{cite book |vauthors =Miller HR, ((Miller OK Jr)) |title=Gasteromycetes: Morphological and Developmental Features, with Keys to the Orders, Families, and Genera |publisher=Mad River Press |location=Eureka, California |year=1988 |page=120 |isbn=0-916422-74-7}}</ref>

<ref name=Miller2006>{{cite book |vauthors =Miller HR, ((Miller OK Jr)) |title=North American Mushrooms: A Field Guide to Edible and Inedible Fungi |publisher=Falcon Guide |location=Guilford, Connecticut |year=2006 |page=489 |isbn=0-7627-3109-5|url=https://books.google.com/books?id=zjvXkLpqsEgC&pg=PA489}}</ref>

<ref name=Moreno2007>{{cite journal |vauthors =Moreno G, Esqueda M, Perez-Silva E, Herrera T, Altes A |year=2007 |title=Some interesting gasteroid and secotioid fungi from Sonora, Mexico |journal=Persoonia |volume=19 |issue=2 |pages=265–80 |url=http://www.ingentaconnect.com/content/nhn/pimj/2007/00000019/00000002/art00007}}</ref>

<ref name=Moreno2010>{{cite journal |vauthors =Moreno G, Marcos Lizárraga ME, Coronado ML |title=Gasteroids and secotioids of Chihuahua (Mexico) |journal=Mycotaxon |year=2010 |volume=112 |pages=291–315 |doi=10.5248/112.291}}</ref>

<ref name=Morse1933>{{cite journal |author =Morse EE. |year=1933 |title=A study of the genus ''Podaxis'' |journal=Mycologia |volume=25 |issue=1 |pages=1–33 |jstor=3754126 |doi=10.2307/3754126 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0025/001/0001.htm}}</ref>

<ref name=Mycoweb>{{cite web |url=http://www.mykoweb.com/CAF/species/Longula_texensis.html |title=''Longula texensis''|vauthors =Wood M, Stevens F |work=California Fungi |publisher=MykoWeb |accessdate=2011-03-18}}</ref>

<ref name=Murrill1916>{{cite journal |author =Murrill WA. |year=1916 |title=Agaricaceae tribe Agariceae |journal=North American Flora |volume=9 |issue=5 |pages=297–374 |url=http://biodiversitylibrary.org/page/751986}} (see p.&nbsp;356)</ref>

<ref name=NYBG>{{cite web |url=http://www.nybg.org/bsci/res/col/exclsp.html |title=Extralimited, excluded and doubtful species |publisher=[[New York Botanical Garden]]|author =Halling RE. |work=A revision of ''Collybia'' s.l. in the northeastern United States & adjacent Canada |year=2004 |accessdate=2011-03-25}}</ref>

<ref name=Ochoa2006>{{cite journal |vauthors =Ochoa C, Moreno G |title=Hongos gasteroides y secotioides de Baja California, México |journal=Boletín de la Sociedad Micológica de Madrid |trans_title=Gasteroid and secotioid fungi of Baja California, Mexico |year=2006 |volume=30 |pages=121–166 |language=Spanish}}</ref>

<ref name=Peck1895>{{cite journal |author =Peck CH. |title=New species of fungi |journal=Bulletin of the Torrey Botanical Club |year=1895 |volume=22 |issue=12 |pages=485–93 |url=http://biodiversitylibrary.org/page/709376 |doi=10.2307/2477972}}</ref>

<ref name=Rumach1994>{{cite book |vauthors =Rumack BH, Spoerke DG |title=Handbook of Mushroom Poisoning: Diagnosis and Treatment |publisher=CRC Press |location=Boca Raton, Florida |year=1994 |page=146 |isbn=0-8493-0194-7 |url=https://books.google.com/books?id=WPWsZNvOqVAC&pg=PA146}}</ref>

<ref name="urlRogers Mushrooms">{{cite web |url=http://www.rogersmushrooms.com/gallery/DisplayBlock~bid~6343~gid~~source~gallerydefault.asp |title=''Longula texensis'' |author =Phillips R |work=Rogers Mushrooms |accessdate=2011-03-18}}</ref>

<ref name="url Fungorum synonymy: Agaricus texensis">{{cite web |url=http://www.speciesfungorum.org/Names/SynSpecies.asp?RecordID=487436 |title=Species synonymy: ''Agaricus texensis'' (Berk. & M.A. Curtis) Geml, Geiser & Royse |publisher=[[CAB International]] |work=Species Fungorum |accessdate=2011-03-21}}</ref>

<ref name=Zeller1943a>{{cite journal |author =Zeller SM. |year=1943 |title=North American species of ''Galeropsis'', ''Gyrophagmium'', ''Longia'', and ''Montagnea'' |jstor=3754593 |journal=Mycologia |volume=35 |issue=4 |pages=409–21 |doi=10.2307/3754593 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0035/004/0409.htm}}</ref>

<ref name=Zeller1945>{{cite journal |author =Zeller SM. |year=1945 |title=Notes and brief articles: A new name |journal=Mycologia |volume=37 |issue=5 |page=636 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0037/005/0636.htm |jstor=3754700}}</ref>

}}

==External links==
* {{Commons category-inline|Agaricus deserticola}}

{{featured article}}

{{DEFAULTSORT:Agaricus Deserticola}}
[[Category:Agaricus|deserticola]]
[[Category:Edible fungi]]
[[Category:Fungi described in 1873]]
[[Category:Fungi of North America]]
[[Category:Secotioid fungi]]