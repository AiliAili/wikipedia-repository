{{Styles}}

The '''Institute of Electrical and Electronics Engineers (IEEE) style''' is a widely accepted [[format]] for [[writing]] [[research paper]]s,{{citation needed|date=December 2014}} commonly used in technical fields, particularly in [[computer science]].  [[IEEE]] [[style]] is based on the [[The Chicago Manual of Style|Chicago Style]].<ref>[http://www.ieee.org/documents/ieeecitationref.pdf IEEE Citation Reference]</ref>  In [[IEEE]] [[style]], [[citation]]s are [[number]]ed, but [[citation number]]s are included in the text in square brackets rather than as [[superscript]]s.  All [[bibliographical]] information is exclusively included in the list of [[references]] at the end of the document, next to the respective citation number.

== External links ==
*[http://www.ieee.org/publications_standards/publications/authors/authors_journals.html Article Preparation and Submission] – IEEE author resources
*[http://www.ieee.org/documents/trans_jour.docx IEEE Template] – Templates for Transactions: Template and Instructions on How to Create Your Paper (DOC, 506 KB)
*[https://www.ieee.org/documents/style_manual.pdf IEEE Editorial Style Manual] – Editing guidelines for Transactions, Journals, and Letters (PDF, 434 KB)
*[http://standards.ieee.org/guides/style/ IEEE Standards Style Manual] – Style and structure for IEEE standards (PDF, 904 KB)
*[http://www.ieee.org/documents/ieeecitationref.pdf IEEE Citation Reference] – official (PDF, 440KB)

== References ==
{{reflist}}
{{Institute of Electrical and Electronics Engineers}}
[[Category:Institute of Electrical and Electronics Engineers|Style]]
[[Category:Bibliography]]
[[Category:Style guides for technical and scientific writing]]
[[Category:Academic style guides]]


{{engineering-journal-stub}}
{{science-journal-stub}}