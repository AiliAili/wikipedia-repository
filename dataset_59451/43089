<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = LaGG-3
 |image = File:LaGG-3 Moscow.jpg
 |caption = 
}}{{Infobox Aircraft Type
 |type = Fighter
 |manufacturer = 21 (Gorky), 31 (Taganrog/Tbilisi), 23/153 (Leningrad/Novosibirsk)
 |designer = V. P. Gorbunov
 |first flight = [[1940 in aviation#First flights|30 March]] [[1940 in aviation|1940]]
 |introduced = early [[1941 in aviation|1941]]
 |retired =
 |status =
 |primary user = Soviet Union
 |more users = <!--up to three more. please separate with <br />.-->
 |produced = 1941–1944
 |number built = 6,528
 |unit cost =
 |variants with their own articles = [[Lavochkin La-5]]<br />[[Lavochkin La-7]]
}}
|}

The '''Lavochkin-Gorbunov-Gudkov LaGG-3''' (Лавочкин-Горбунов-Гудков ЛаГГ-3) was a [[Soviet Union|Soviet]] [[fighter aircraft]] of [[World War II]]. It was a refinement of the earlier [[Lavochkin-Gorbunov-Gudkov LaGG-1|LaGG-1]], and was one of the most modern aircraft available to the [[Soviet Air Force]] at the time of [[Germany]]'s invasion in 1941.

Overweight despite its wooden construction, at one stage 12 LaGG-3s were being completed daily and 6,528 had been built when  factory 31 in Tbilisi switched to [[Yakovlev Yak-3|Yak-3]] production in 1944.<ref name="The Hamlyn Guide to Military Aircraft Markings">Wheeler 1992, p. 73.</ref>
{{TOC limit|limit=2}}

==Design and development==
The prototype of the LaGG-3, I-301, was designed by [[Semyon Lavochkin|Semyon A. Lavochkin]], [[Vladimir Petrovich Gorbunov|Vladimir P. Gorbunov]] and [[Mikhail Gudkov|Mikhail I. Gudkov]]. It was designated LaGG-3 in serial production. Its airframe was almost completely made of wood ''delta-veneer'' (a [[resin-wood multi-ply veneer]] composed of very thin, 0.35 to 0.55&nbsp;mm, [[wood veneer]] and [[phenol formaldehyde resin]], baked at high temperature and pressure) used for the crucial parts. This novel construction material had [[tensile strength]] comparable to that of non-hardened aluminum alloys and only 30% lower than that of [[Precipitation hardening|precipitation hardened]] D-1A grade  [[duralumin]]. It was also incombustible and completely invulnerable to rot, with service life measured in decades in adverse conditions. The full wooden wing (with plywood surfaces) was analogous to that of the [[Yak-1]]. The only difference was that the LaGG's wings were built in two sections. The fuselage was of similar construction to the [[MiG-3]]'s.<ref name= "Drabkin,  pp. 146-147.">Drabkin 2007, pp. 146–147.</ref> The LaGG-3's armament consisted of a 20&nbsp;mm [[ShVAK]] cannon, with 150 rounds, which was installed in the ''motornaya pushka'' in the cilinder block split- between the "V" of the engine cylinders and firing through a hollow propeller shaft, and two synchronized 12.7&nbsp;mm [[Berezin UBS]] machine guns with 170 rpg. Consequently, the combined weight of rounds fired per second was 2.65&nbsp;kg/s, making the LaGG-3 superior in burst mass to all contemporary Russian fighters, particularly to the MiG-3.<ref name= "Drabkin,  p. 147.">Drabkin 2007, p. 147.</ref> Most other Russian fighters of that era were considered under-gunned{{citation needed|date=February 2014}} in relation to western contemporary fighters. This is somewhat true even for the Yak-1, which had a 20&nbsp;mm cannon and two 7.62&nbsp;mm machine guns, but not the later versions of the [[Polikarpov I-16]], which had two cannons and two machine guns.

==Operational history==
The LaGG-3 rapidly replaced the [[LaGG-1]], although the new fighter was too heavy for its engine. In fact, Lavochkin, Gorbunov and Gudkov had originally designed their prototype for the powerful [[Klimov M-106]] engine, but it proved to be unreliable, so they were obliged to install the relatively weak [[Klimov M-105]]P. As a result, the LaGG was slow; its top speed was just 575&nbsp;km/h, while its rate of climb, at ground level, was as slow as 8.5 meters/second. The LaGG-3 proved to be somewhat hard to control as it reacted sluggishly to stick forces. In particular, it was difficult to pull out of a dive, and if the stick was pulled too hard, it tended to fall into a spin. As a consequence, sharp turns were difficult to perform.<ref name= "Drabkin,  p. 147."/> A more powerful version of the engine was installed, but the improvement was small, so the only solution was to lighten the airframe. The LaGG team re-examined the design and pared down the structure as much as possible. Fixed [[Leading edge slats|slats]] were added to the wings to improve climb and maneuverability and further weight was saved by installing lighter armament (most versions used a 20&nbsp;mm [[ShVAK]] cannon and a single synchronized 12.7&nbsp;mm [[Berezin UBS]] machine gun). But the improvement was slight and, thus, without an alternative powerplant, when the LaGG-3 was first committed to combat in July 1941, it was completely outclassed by the [[Messerschmitt Bf 109]].<ref name= "Drabkin,  p. 147."/>

Later in 1941, the LaGG-3 appeared with new armament options, an internally balanced rudder, retractable ski landing gear for the winter, retractable tailwheel and wing pipes for drop tanks.<ref name= "Gunston  p. 132.">Gunston 1980, p. 132.</ref> The result was still not good enough. Even with the lighter airframe and revised [[supercharger|supercharged]] engine, the LaGG-3 was underpowered.

The LaGG-3 proved immensely unpopular with pilots. Some aircraft supplied to the front line were up to 40&nbsp;km/h (25&nbsp;mph) slower than they should have been and some were not airworthy. This happened less because of the added weight with full fuel and weapon loads in combat conditions, but specifically to the poor finishing in rushed industrial production, due to the German invasion. In combat, the LaGG-3's main advantage was its strong airframe. Although the laminated wood did not burn, it shattered when hit by high explosive rounds.

The LaGG-3 was improved during production, resulting in 66 minor variants in the 6,528 that were built. Experiments with fitting a [[Shvetsov ASh-82|Shvetsov M-82]] [[radial engine]] to the LaGG-3 airframe finally solved the power problem, and led to the [[Lavochkin La-5]].<ref>Gordon 2003, p. 37.</ref> The major LaGG-3 construction plant in Gorky switched over to the La-5 in 1942, after having completed 3,583 LaGG-3. All further LaGG-3 development and production was done by factory 31 in Taganrog as the sole LaGG-3 manufacturer.

Soviet pilots generally disliked this aircraft. Pilot Viktor M. Sinaisky recalled:
:It was an unpleasant client! Preparing the LaGG-3 for flight demanded more time in comparison with other planes. All cylinders were supposed to be synchronized: God forbid you from shifting the gas distribution! We were strictly forbidden to touch the engine! ... [T]here were constant problems with [the] water-cooled engines in winter.... [T]here was no anti-freeze liquid [and y]ou couldn't keep the engine running all night long, so you had to pour hot water into the cooling system ... in the morning. ... [P]ilots didn't like flying the LaGG-3 – a heavy beast with a weak ... engine... [T]hey got used to it ... [but] we had higher losses on LaGG-3 than on [[Polikarpov I-16|I-16]]s.<ref name= "Drabkin p. 73.">Drabkin 2007, p. 73.</ref>

Even with its limitations, some Soviet pilots managed to reach the status of ace flying the LaGG-3. G. I. Grigor'yev, from 178th IAP, was credited of at least 11 air victories plus two shared. But pictures of his LaGG-3 "Yellow 6", in November–December 1941, show 15 "stars", so his score was probably higher.<ref name= "Morgan  p. 28.">Morgan  1999, p. 28.</ref>

==Variants==
* Gudkov 82 (M-82) – fitted with the 1540&nbsp;hp Shvetsov M-82 14-cylinder radial air-cooled engine and propeller from a [[Sukhoi Su-2]]
* Gudkov 37 (K-37) – an anti-armour version with a Shpital'ny-Komaritsky 37mm center-mount cannon; only 3 prototypes were built in the summer of 1942
* Gorbunov 105 – a lightened LaGG-3 with improved performance and improved rear vision with cut down rear decking, overtaken by newer aircraft such as the [[La-5]]
* LaGG-3IT – LaGG-3 66 series with a NS-37 cannon

==Operators==
;{{FIN}}
[[File:Finnish Air Force LaGG-3.jpg|thumb|right| Finnish Air Force LaGG-3]]
* The [[Finnish Air Force]] operated three captured examples, mainly as bomber interceptors.<ref>Keskinen et al. 1977, pp. 74–87, 126.</ref> WO Eino Koskinen scored the sole kill achieved by a LaGG-3 in Finnish colors, when he downed a Soviet LaGG-3 on 16 February 1944 in the plane marked as LG-1.<ref>Mellinger et all. 2012, p.28</ref>
;{{flag|Germany|Nazi}}
* The [[Luftwaffe]] operated captured examples for tests. One captured example was used for a propaganda movie in 1943.<ref>Stapfer 1996, p. 16.</ref>
;{{JPN}}
* The [[Imperial Japanese Army Air Service]] operated one flown into Manchuria by a defector. It was used for testing.<ref>Green and Swanborough 1977, p. 13.</ref>
;{{USSR}}
* [[Soviet Air Force]]

==Specifications LaGG-3 (data for LaGG-3 series 66)==
[[File:LaGG-3.svg|thumb|400px|LaGG-3]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's Fighting Aircraft of World War II<ref>Bridgeman 1946, pp. 194–195.</ref>
|crew=One
|length main=8.81 m
|length alt=28 ft 11 in
|span main=9.80 m
|span alt=32 ft 1.75 in
|height main=2.54 m
|height alt=8 ft 4 in
|area main=17.4 m²
|area alt=188 ft²
|empty weight main=2,205 kg
|empty weight alt=4,851 lb
|loaded weight main=2,620 kg
|loaded weight alt=5,764 lb
|max takeoff weight main=3,190 kg
|max takeoff weight alt=7,018 lb
|engine (prop)=[[Klimov M-105]]PF
|type of prop=liquid-cooled V-12
|number of props=1
|power main=924 kW
|power alt=1,260 hp
|max speed main=575 km/h
|max speed alt=357 mph
|range main=1000 km
|range alt=621 mi
|ceiling main=9,700 m
|ceiling alt=31,825 ft
|climb rate main=14.9 m/s
|climb rate alt=2,926 ft/min
|loading main=150 kg/m²
|loading alt=31 lb/ft²
|power/mass main=350 W/kg
|power/mass alt=0.21 hp/lb
|armament=
* 2× 12.7 mm (0.50 in) [[Berezin UB|Berezin BS]] machine guns
* 1× 20 mm [[ShVAK cannon]]
* 6× [[RS-82 rocket|RS-82]] or RS-132 rockets up a total of 200 kg (441 lb)
}}

==See also==
{{aircontent
|related=
* [[Lavochkin-Gorbunov-Gudkov LaGG-1|LaGG-1]]
* [[Lavochkin La-5]]
* [[Lavochkin La-7]]
|similar aircraft=
* [[Curtiss P-40]]
* [[Kawasaki Ki-61]]
* [[Macchi C.202]]
* [[Messerschmitt Bf 109]]
* [[Supermarine Spitfire]]
|lists=
* [[List of fighter aircraft]]
* [[List of aircraft of World War II]]
* [[List of military aircraft of the Soviet Union and the CIS]]
}}

==References==
{{commons category|Lavochkin LaGG-3}}

===Notes===
{{Reflist|2}}

===Bibliography===
{{refbegin}}
* Abanshin, Michael E. and Nina Gut. ''Fighting Lavochkin, Eagles of the East No.1''. Lynnwood, WA: Aviation International, 1993. ISBN unknown.
* Bridgeman, Leonard, ed. "The LaGG-3". ''Jane's Fighting Aircraft of World War II''. London: Studio, 1946. ISBN 1-85170-493-0.
* Drabkin, Artem. ''The Red Air Force at War: Barbarossa & The Retreat to Moscow – Recollections of Fighter Pilots on the Eastern Front''. Barnsley, South Yorkshire, UK: Pen & Sword Military, 2007. ISBN 1-84415-563-3.
* Gordon, Yefim. ''Lavochkin's Piston-Engined Fighters (Red Star Volume 10)''. Earl Shilton, Leicester, UK: Midland Publishing Ltd., 2003. ISBN 1-85780-151-2.
* Gordon, Yefim and Dmitri Khazanov. ''Soviet Combat Aircraft of the Second World War, Volume One: Single-Engined Fighters''. Earl Shilton, Leicester, UK: Midland Publishing Ltd., 1998. ISBN 1-85780-083-4.
* Gunston, Bill. ''Aircraft of World War two''. London, Octopus Books Limited, 1980. ISBN 0-7064-1287-7.
* Gunston, Bill. ''The Osprey Encyclopaedia of Russian Aircraft 1875–1995''. London: Osprey, 1995. ISBN 1-85532-405-9.
* Green, William. ''Warplanes of the Second World War, Volume Three: Fighters''. London: Macdonald & Co. (Publishers) Ltd., 1961 (seventh impression 1973). ISBN 0-356-01447-9.
* Green, William and Gordon Swanborough. ''WW2 Aircraft Fact Files: Soviet Air Force Fighters, Part 1''. London: Macdonald and Jane's Publishers Ltd., 1977. ISBN 0-354-01026-3.
* Keskinen, Kalevi, Kari Stenman and Klaus Niska. ''Venäläiset Hävittäjät (Suomen Ilmavoimien Historia 7)'' (in Finnish with English Summary). Espoo, Finland: Tietoteos, 1977. ISBN 951-9035-25-7.
* Kotelnikov, Vladimir, Mikhail Orlov and Nikolay Yakubovich. ''LaGG-3 (Wydawnictwo Militaria 249)'' (in Polish). Warszawa, Poland: Wydawnictwo Militaria, 2006. ISBN 83-7219-249-9.
* Morgan, Hugh. ''Gli assi Sovietici della Seconda guerra mondiale''(in Italian). Edizioni del Prado/Osprey Aviation, 1999. ISBN 84-8372-203-8.
* Stapfer, Hans-Heiri. ''LaGG Fighters in Action'' (Aircraft in Action Number 163). Carrollton, Texas: Squadron/Signal Publications, Inc., 1996. ISBN 0-89747-364-7.
* Wheeler, Barry C. ''The Hamlyn Guide to Military Aircraft Markings.'' London: Chancellor Press, 1992. ISBN 1-85152-582-3.
* Mellinger, George ''LaGG & Lavochkin Aces of World War 2 (Osprey aircraft of the aces)'' London: Osprey Publishing 2012.  ISBN 978-1841766096.
{{refend}}

{{Lavochkin aircraft}}
{{WWIIUSSRAF}}

[[Category:Lavochkin aircraft]]
[[Category:Soviet fighter aircraft 1940–1949|La-03]]
[[Category:World War II Soviet fighter aircraft]]
[[Category:Single-engine aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Propeller aircraft]]