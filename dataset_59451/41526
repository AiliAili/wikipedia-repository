{{Infobox musical artist
| image               = 
| name                = Thomasina Winslow
| background          = solo_singer
| alias               = Sina Winslow, Thomasina, Tunes
| origin              =
| instrument          = Singer, guitar, electric bass
| genre               = Blues, Soul, R & B, Gospel, Funk, Folk, [[African American music]]
| occupation          =
| years_active        = 1969 to present
| label               = Biograph
| associated_acts     = Tom Winslow, The Winslows, Two Story Tuesday, Bojembe, Nick Katzman
| website             =
| notable_instruments = ''Piece of Ash'' electric guitar
}}
'''Thomasina Winslow''' is an [[United States|American]] [[blues]] musician from the [[Albany, New York]] area, and the daughter of [[folk music]]ian [[Tom Winslow]].  As a toddler, she sang back-up on her father's [[folk music]] classic ''Hey Looka Yonder (It's The Clearwater''); also singing a solo version of ''One-Two-Three'', another version of which she produced on her own 30 years later.  In addition to her solo career, Winslow has been a member of four bands, including a [[Duet|duo]] with [[Nick Katzman]].  Furthermore, she has been a teacher in that [[genre of music]], and has significantly influenced other aspiring musicians.  Winslow primarily performs covers of Blues [[Standard (music)|standards]] and has written a number of blues and [[Gospel music|gospel tunes]] in her own right.  She is also one of a handful of [[African-American]] women producers in the "indy" music industry today.

While she has been teaching a full load of school and private students for over a decade, her primary notability has been from touring in the eastern United States and Europe with '''Nick Katzman''', starting in 2006.

Winslow continues to tour on and off.  On May 6, 2011, she again played with Nick Katzman at Katzman's "favorite U.S. venue," The Good Coffeehouse Music Parlor (GCMP), located in  [[Brooklyn, New York]]. This marks the third time that the famed [[coffee house]] has hosted Winslow.  She took another tour of Europe in the summer of 2011, marking her [[France|French]] debut.

==Biography==

===Early life===
Thomasina Winslow grew up in [[New Baltimore, New York]], south of Albany.  Something of a [[child prodigy]], she was nicknamed "Tunes" by her brother.<ref>[http://melo.pl/licencjainfo/aid/34984 Melo.pl Polish musical Web site]</ref> She first performed on her father's album ''Tom Winslow'',<ref>[[Biograph Records]], catalog no. u/k (1969)</ref> singing ''1-2-3'' (''One-Two-Three'') as a toddler.<ref>[http://www.groupietunes.com/artists/thomasinawinslow Groupie Tunes web site]</ref><ref>[http://www.thomasinawinslow.com/ Official web site]</ref>  She also sang back-up that year on the [[folk music]] classic ''Hey Looka Yonder (It's the Clear Water) (It's the Clearwater)'', about the ''[[Sloop Clearwater]]''.<ref>[http://www.ibiblio.org/keefer/wp11.htm Folk Music Performer Index web page for Wilm to Wir]</ref><ref>Winslow, Tom / ''It's the Clear Water'', [[Biograph Records]], BLP 12018 (1969).</ref>
As part of her family's group, '''The Winslows''', she toured throughout the [[folk music]] [[Music festival|festival circuit]], including [[Clearwater Festival|The Great Hudson River Revival]].<ref name="Auto">[http://winslow-productions.com/meetthomasina.htm Autobiography at Productions Page].  Accessed April 28, 2008.</ref><ref name="Official">[http://www.thomasinawinslow.com/ Official web site].  Accessed April 28, 2008</ref>

===Education===
Winslow attended [[Ravena-Coeymans-Selkirk Central Schools]].{{Citation needed|date=July 2008}}  She received her [[associate's degree|A.A.]] in [[Performing Arts]] from [[Schenectady County Community College]] and her B.A. in Music Performance from the [[University at Albany]], where she majored in [[Classical Guitar]].<ref name="Auto" /> <ref name="Official" /.

===Solo music and band work===
She has been playing covers of [[Blues]] and [[gospel music]] at solo gigs, which have included the Cambridge Inn in [[Cambridge, New York]], and the Red Lion Inn in [[Stockbridge, Massachusetts]].  She also played in the 1990s in concerts with '''Two Story Tuesday''' and '''Bojembe''' around [[upstate]] [[New York]], including [[Glens Falls, New York|Glens Falls]], [[Albany, New York|Albany]], and [[Colonie, New York|Colonie]].   {{Citation needed|date=August 2007}}

Winslow's ''[[day job]]'' is that of a [[Music education|music]] and [[performing arts]] teacher at '''Saint Anne Institute''', a [[reform school]] for girls,<ref name="St Anne">[http://www.stanneinstitute.org/index.cfm Saint Anne Institute web site].  Accessed April 28, 2008.</ref> where she administers the "Expressive Arts" program.<ref name="Expressive">[http://www.stanneinstitute.org/News/details.cfm?newsID=10 St. Anne's Institute web site]. Accessed April 28, 2008.</ref>  She was quoted as saying
"Teenagers have a lot to express, especially children-at-risk. But they haven't always been directed as to how to do that. We try to help them." <ref name="Expressive" /><ref name="Gazette">Kathy Ricetts, ''Youth Advancement Through Music and Art Program Offers Students at St. Anne Institute Renewed Confidence'', ''[[Daily Gazette]]'', December 6, 2005, online at [http://www.emtah.org/story.php?RECORD_KEY(News)=ID&ID(News)=8 Daily Gazette article]</ref>  Funding for this program was a Youth Advancement Through Music and Art (YATMA) grant.<ref name="Gazette" /><ref>[http://www.zoominfo.com/people/level2page43475.aspx YATMA at Zoom Info]</ref><ref>[http://www.linkedin.com/find/w/w76/w76_48.html YATMA at LinkedIn]</ref>  YATMA, since 2006 called '''Educational Mentoring Through the Arts & Humanities''' (EMTAH),<ref>[http://www.emtah.org/history.html History of EMTAH web page]</ref> "incorporates strength-based mentoring with progressive fine arts instruction, taps into and develops student potential by promoting creativity and providing early success. Stimulated by their progress, students learn to apply their new skills to other areas of their lives." <ref>[http://www.idealist.org/en/org/87637-54 Idealist.org listing]</ref>  EMTAH was reviewed in December 2006 at [[WGBH (FM)|WGBH]]'s ''[[Open Source (radio show)|Open Source]]'' with [[Christopher Lydon]], before the show's cancellation in July 2007.<ref>[http://www.radioopensource.org/music-on-the-brain/ Radio Open Source show ''Music on the Brain]</ref>

This program has won kudos in the community:
{{quote|This is an innovative approach to reaching our community’s children who are most at risk for educational and behavioral problems. Between the mentoring quality of the teacher-student relationship and the freedom of expression allowed the students, this approach provides youth with the support and encouragement necessary to make them productive, independent members of our community.|Judith N. Lyons, Executive Director of The Community Foundation for the [[Capital Region]], quoted at http://www.emtah.org/history.html}}

In addition to the Expressive Arts award, she has won numerous other grants, including a [[New York State Education Department]] grant.<ref name="Auto" />

==Solo tours==

===Winter 2007 to Spring 2008 tour===
Winslow made a short [[Upstate New York]] solo tour during December 2007 and early 2008.  She had her first headlining show at [[Caffe Lena]] in [[Saratoga Springs]],<ref>[http://www.caffelena.org/cal-2007-12.htm Caffe Lena official web site Calendar page for December 2007]. Accessed December 10, 2007.</ref><ref name="Past Calendar">[http://www.thomasinawinslow.com/calendar.html?showcal=past Past Calendar from official website].   Retrieved December 9, 2008.</ref> in addition to featured guest spots on [[WRPI]] in [[Troy, New York]] and [[WAMC]] in [[Albany, New York]], as well as a gigs at the Java Jazz Cafe on January 3, 2008,<ref>[http://javajazzcafe.net/javajazzcafe/ Java Jazz Cafe web site]. Accessed January 6, 2008.</ref> in [[Delmar, New York]].<ref name="Past Calendar" />

In March 2007, she was featured in Caffe Lena's "3rd Annual Blues Fest" alongside '''Mark Tolstrup''' and '''Beaucoup Blue'''.<ref name="Past Calendar" /><ref>[http://www.caffelena.org/cal-2008-03.htm Caffe Lena official web site Calendar page for March 2008]. [http://www.caffelena.org/cal-2008-03.htm#22 Caffe Lena official web site Blues Festival page]. Both accessed March 31, 2008.</ref>  In May 2008, Winslow returned to Java Jazz for a second show.<ref name="Past Calendar" /><ref name="Arts show">[http://www.albanyalive.com/gallery/events/?album=1&gallery=137 AlbanyAlive.com Events web site gallery page 137].  Accessed May 13, 2008.</ref>

===Summer 2008===
In the Summer of 2008, Winslow played gigs further out in [[Upstate New York]], including three gigs each at [[Colonie Center]] and the Barnsider BBQ Restaurant in [[Lake George (town), New York|Lake George]].<ref name="Past Calendar" />

===WRPI live show===
On December 8, 2008, Winslow performed a one-hour live show on [[WRPI]], the [[college radio]] station of [[Rensselaer Polytechnic Institute]], for its "Stormy Monday Blues" show.<ref name="Past Calendar" /><ref>[http://www.wrpi.org/schedule.php WRPI schedule].  Accessed December 13, 2008.</ref>

===2009 Tour===
Winslow had a significant tour in Spring 2009.<ref name=calendar>[http://www.thomasinawinslow.com/calendar.html official website calendar page].  Accessed December 9, 2008.</ref> This included a performance at [[Marietta, Ohio]]'s [[River City Blues Festival]] in March 2009,<ref name=calendar /><ref>[http://www.bjfm.org/Templates/festival.htm River City Blues Festival official website].  Accessed December 9, 2008.</ref> a show in [[Easton, New York]], blues festivals in [[Denmark]] and [[Germany]], and return shows at Caffe Lena and the Good Coffeehouse with Nick Katzman.<ref name=calendar />

===2012 Tour===
Winslow made her [[Australia]] debut at the [[Cairns Blues Festival]] on May 12, 2012.<ref>{{cite web|url=http://www.soundandscene.com/cairnsbluesfestival-music-festival-australia-2012.html|title=Cairns Blues Festival:Lineup|publisher=Sound and Scene|date=May 12, 2012|accessdate=July 10, 2012}}</ref><ref>{{cite web|url=http://www.travelwireasia.com/2012/05/top-festivals-and-events-in-asia-this-week-may-7-13/|title=Top festivals and events in Asia this week – May 7–13|publisher=Travel Wire Asia|last=Lane|first=Jo|date=May 8, 2012|accessdate=July 10, 2012}}</ref>

===2014===
Winslow performed at the 2014 Clearwater Festival.<ref>{{cite web|url=http://www.clearwaterfestival.org/all-performers/|work=Clearwater Great Hudson River Revival|title=All Performers|date=June 21, 2014|accessdate=June 23, 2014}}</ref>

==Bojembe==
Winslow was a guitarist, co-lead singer, and musician in the [[Rhythm and blues|R & B]] band '''Bojembe'''.  The other members were John Zumbo, Sean Mack, and Terry Plunkett.  They produced one album in 2005–2006 before the band became inactive, but is still sold online.<ref>[https://www.cdbaby.com/cd/bojembe CD Baby]</ref>  Most of the songs from the CD can be purchased in MP3 format.<ref>[http://www.tradebit.com/searchfiles.php/search/0/Bojembe/AND/3 TradeBit web site]</ref>  Bojembe's most recent public gig was at the River Street Beat Shop in [[Troy, New York]], for which a blogger wrote that "Winslow's roots are in the deep [[Folk blues|folk-blues tradition]], (while) her work with Bojembe leans toward a decidedly [[Funk|funky groove]] with a [[world music]] spin." <ref>[http://users.boardnation.com/~pitchcontrolmusic/Printpage.php?board=1;threadid=2981 Board Nation web site]</ref>

==Management and production==
As a business woman, Winslow manages and produces music for herself and other artists in a variety of music.  She is one of the few African-American woman today producing records for indie artists, including herself.

She is the owner of a production company, Winslow Productions.<ref>[http://winslow-productions.com/ Winslow Productions web page]</ref>  As part of her production work, Winslow also mentors up-and-coming performing artists.<ref>[http://winslow-productions.com/channelconnections.htm Channel Connections web page]</ref>

Winslow was, for a while, an artistic agent.<ref>[http://www.absolutearts.com/portfolios/s/stephenmead/artist_exhibitions.html Stephen Mead artistic web page]</ref>  She has also collaborated with a local visual artist, Kim Morris, for a concert and art show.<ref name="Arts show" />

Winslow's music is sold on Amazon.com <ref>[http://www.amazon.de/s?search-type=ss&index=music-de&field-artist=Thomasina&page=1 Amazon.com web site]</ref> and other online sales web sites, such as CDBaby.  Due to the nature of the Blues market today, as a musical tradition outside the [[mass market]], Winslow's albums have not "charted".  In fact, sales of music she has performed or produced has sold more ''outside'' the [[United States]].  This has been especially in [[England]] and [[Germany]],<ref>[http://www.turbo-music.de/shop/catalog/product_info.php/products_id/32596 German music web site]</ref> where she has performed with Nick Katzman, as well as in [[Poland]],<ref>[http://melo.pl/artysta/aid/121043 Polish music web site]</ref>  [[Denmark]],<ref>[http://www.imusic.dk/page/tracksearch?caption=Send+Your+Word+Lord Danish web site]</ref> and [[Latin America]].<ref>[http://mx.preciomania.com/search_getprod.php/masterid=567139001// Preciomania.com a Spanish-language web site for selling CDs]</ref>

Interestingly, Winslow figured out that [[music download]]s, such as [[MP3]], could actually ''increase'' her sales, especially overseas; such a [[file sharing]] [[business model]] has been documented by [[Harvard University]] researchers,<ref>[http://hbswk.hbs.edu/item/4206.html Harvard Business School web site]</ref> as well as in ''[[Wikinomics]]'', whose authors call "MP3 b-web."<ref>Dan Tapscott and Anthony D. Williams, Wikinomics, pp. 57–58 (Portfolio/Penguin Books 2006) (ISBN 978-1-59184-138-8.</ref>

==Collaboration with Nick Katzman==

One of Winslow's most fruitful and notable collaborations has been with Bluesman [[Nick Katzman]].  As related in that article, Katzman has mentored a number of younger Blues artists, including Winslow, taking her "under his wing."  The duo has performed recently from [[Brooklyn, New York]] to [[Stamford, England]] and [[Berlin, Germany]].

===2006–2007 Brooklyn gigs===
In April 2006, Katzman was slated to perform at the Good Coffeehouse Music Parlor in Brooklyn, his favored venue in "the [[United States|States]]".<ref>[http://bsec.org/events/coffeehouse/index.php Brooklyn Society for Ethical Culture official web site]</ref>  He had previously performed with Ruby Green ([[Joy of Cooking (band)|Terry Garthwaite]]),<ref>[http://www.guitarseminars.com/ubb/Forum1/HTML/002480.html IGS Guitar Forum blog of April 2003]</ref> and had recorded ''Sparking Ragtime & Hardbitten Blues'', a now-classic LP, with Green, since released as a CD,<ref>[http://home.att.net/~sparklex/srp.html Sparking Ragtime & Hardbitten Blues CD web page]</ref> so their fans expected they would perform again together.<ref>[http://www.guitarseminars.com/ubb/Forum1/HTML/013527.html IGS Guitar Forum blog of April 2006]</ref> Instead, Winslow substituted for Green; in Katzman's own words:
{{quotation|I was getting my e-mail and was about to zap what I thought was ... spam.... I decided to open it anyway and it turned out to be a letter from a singer/guitarist named Thomasina Winslow who said she was covering tunes from my Kicking Mule LPs on her new CD. Now, mind you, this is NOT an everyday occurrence for me, so I went immediately to CDbaby and listened to her recording. She had a version of "I'm Goin' Away" where she played guitar at least as well as I did and sang wonderfully. I was of course flattered that someone was so involved with my music and very impressed with how well she handled the material. It turns out that she is the daughter of a Gary Davis disciple named Tom Winslow and apparently she grew up around the Davis' scene and was pretty much brought up in a folk-blues household.
<br />
She said that I was a primary musical influence
for her, so I've asked her to come and do a guest appearance at the gig,
and she said she'd come all the way down from Albany to do it, so it looks
like we'll be hosting Thomasina Winslow on that night, too. I'm not sure how much
exposure she's had or even how much she's done professionally. The
idea of my music somehow finding its way back to young black musicians is
very exciting for me.
<br />
I'll be meeting her for the first time at the gig, so it should be an
interesting night.|email from Nick Katzman, quoted at [http://www.guitarseminars.com/ubb/Forum1/HTML/013527.html IGS Guitar Forum blog of April 2006]}}

Fans of Katzman were once again on notice about a second show in April 2007.<ref>[http://www.guitarseminars.com/ubb/Forum1/HTML/017206.html IGS Guitar Forum blog of April 2007]</ref><ref>[http://www.brooklyneagle.com/categories/category.php?category_id=13&id=12541 The ''[[Brooklyn Eagle]]'' web site listings]</ref>  A fan posted a review on his blog that it was a "Great show! Nick and his cohorts were wonderful!"

===2007 tour with Katzman===

Winslow flew out to [[Germany]] to tour with Katzman in the summer of 2007, when they performed a few gigs in [[Berlin, Germany]] and recorded music videos.<ref>[https://www.youtube.com/watch?v=-WHU4jHJrVY Music Video on YouTube with Nick Katzman]</ref>

The duo played at the [[Stamford, England|Stamford Arts Centre]] [[Stamford Blues Festival|
Blues and Folk Festival]], and were the second-billed act for "Guitar Day" ("8.30pm Nick Katzman & Thomasina Winslow 10.00pm Jeff Lang").<ref>[http://www.stamfordartscentre.com Stamford Arts Centre official web site]</ref><ref>[http://weeniecampbell.com/yabbse/index.php?topic=3495.msg26581 Weenie Campbell's reviews]</ref> It was favorably reviewed in ''Monty's Music News'', an [[on-line]] Music newsletter, as a "great evening Concert in The Ballroom." <ref>[http://www.montysmusic.com/news.php Monty's Music web page]</ref>  [[Blog]]ger Ginger Mayerson called their performances "cute" and "Oh, I just love these trips down memory lane, don’t you?", adding "I like the way this singer, Thomasina Winslow, sings."<ref>Ginger Mayerson, "Nick Katzman: Filed under: amused", found at [http://hackenblog.hackenbush.org/ The Hackenblog].  Retrieved November 2, 2008.</ref>

===2008 work with Katzman===

In April 2008, Winslow and Katzman recorded a song for a future CD.{{Citation needed|date=April 2008}}  They also performed together again at the Good Coffeehouse in Brooklyn on April 25, 2008.<ref name="Past Calendar">[http://www.thomasinawinslow.com/calendar.html?showcal=past Official web site Past Calendar page].  Accessed April 28, 2008.</ref><ref>[http://www.bsec.org/6706/8722.html Brooklyn Society for Ethical Culture official web site The Good Coffeehouse Music Parlor page].  Accessed April 28, 2008.</ref>

They received the "Artist of the Day" from the oneartistaday.com website, which celebrates "Independent artists", on October 1, 2008.<ref>[http://oneartistaday.com/documents/archives.html Thomasina Winslow (Featuring Nick Katzman) on One Artist a Day website].  Retrieved October 2, 2008.</ref>

===2009 tour===

Winslow and Katzman toured both America and Europe in mid-2009.  They had a gigs in Brooklyn, Saratoga Springs,<ref>{{cite news|url=http://www.piratecny.com/BACKISSUES/2009/PDF%2016/16%20LOUDENVILLE%2009.pdf|publisher=Loudonville Spotlight|date=April 22, 2009|page=17|title=Arts & Entertainment|accessdate=July 10, 2012}}</ref> and Easton, New York from April to May 2009.  They performed at the [[Mississippi Blues Nacht]] Festival in [[Köpenick]], [[Germany]] on July 18 and 19, 2009.

===2010 tour===
Katzman and Winslow took a short tour of Germany and [[Denmark]] in July and August 2010.  They performed in [[Bad Sobernheim]] and [[Skarrild, Denmark]] at the Danish Folk, Blues, and Ragtime Guitar Festival.<ref>[http://www.s-e-s.dk/Festival.htm Danish Folk, Blues, and Ragtime Guitar Festival website].  Accessed September 14, 2010.</ref>  She also performed a solo gig in [[Frankfurt, Germany]].

===2011 tour===

On Friday, May 6, 2011, Winslow once again played with Nick Katzman at Katzman's "favorite U.S. venue," The Good Coffeehouse Music Parlor (GCMP). This marked the fourth time that the Good Coffeehouse has hosted Katzman, and the third time for Winslow. The GCMP is housed in a 1900 Gothic Revival mansion's "great room"—noted for excellent line of sight and superb acoustics.{{citation needed|date=March 2012}}

==Other collaborations==
Winslow recorded a [[music video]] with '''Marco Haber''' of '''Mudfunk''' in December 2007, covering ''[[Fly Like An Eagle]]''.<ref>[https://www.youtube.com/watch?v=iWBVKePmh7I YouTube music video]. Accessed January 6, 2008.</ref>  She performed in the ''Old Songs Sampler Concert'' in [[Voorheesville]], New York in January 2010.<ref>''Chronogram'', January 2010 issue, p. , found at [http://issuu.com/chronogram/docs/chronogram_0110_issuu ''Chronogram'' archives]. Accessed May 11, 2010.</ref>  She appeared as a part of a group of female musicians at a Caffe Lena show called ''Ladies Sing the Blues'' in February 2010.<ref>[http://www.nyfolklore.org/about/febevents.html New York Folklore Society website]. Accessed May 11, 2010.</ref>

==Discography==
*''Bojembe''
*''Essential Tunes''

==See also==
{{Portal|African American}}
* [[Biograph Records]]
* [[Nick Katzman]]
* [[Tom Winslow]]
* [[Reverend Gary Davis]]

==References==
{{Reflist|colwidth=30em}}

==External links==
* [http://www.thomasinawinslow.com Official web site]
* [http://winslow-productions.com/ Winslow Productions web site]
* [http://new.music.yahoo.com/thomasina-winslow/ Yahoo music web page]
* [http://pteropusfnq.blogspot.com/2012/05/cairns-blues-festival.html Cairns Blues festival videos at Petropus FNQ website]
* [https://www.youtube.com/watch?NR=1&feature=endscreen&v=mkn9-FjjOEI YouTube video with Nick Katzman] and [https://www.youtube.com/watch?v=iWBVKePmh7I Marco Haber]

{{Americanrootsmusic}}

{{DEFAULTSORT:Winslow, Thomasina}}
[[Category:Living people]]
[[Category:African-American female singers]]
[[Category:American blues guitarists]]
[[Category:American blues singers]]
[[Category:American child musicians]]
[[Category:American female guitarists]]
[[Category:American folk guitarists]]
[[Category:American folk singers]]
[[Category:American funk guitarists]]
[[Category:American funk singers]]
[[Category:American women in business]]
[[Category:Country blues musicians]]
[[Category:People from Albany, New York]]
[[Category:People from New Baltimore, New York]]
[[Category:Year of birth missing (living people)]]