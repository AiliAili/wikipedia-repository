{{coord|41.884531|N|12.494212|E|type:landmark|display=title}}

{{Ancient monuments in Rome
|name=Castra Peregrina
|label_name=
|image_name=
|caption=
|lat_deg=41.884531
|lon_deg=12.494212
|location=Caelian Hill
|date=1st Century AD
|builder=Augustus
|type=[[Castra|Roman Barracks]]
}}

The ''Castra Peregrina'' (“camp of the strangers”) was an ancient military barracks located in [[Rome]] upon the [[Caelian Hill]].

== Location and archaeology ==
The barracks are situated on the [[Caelian Hill]] between the [[Temple of the Deified Claudius]] and Neros’ [[Macellum Magnum]]. It is centrally located on a hill known for its housing of wealthy Romans. The fourth-century Regionaries list the ''Castra Peregrina'' in ''regio II''.<ref>Thayer, Bill (2000-03-20): “[http://penelope.uchicago.edu/Thayer/E/Gazetteer/Places/Europe/Italy/Lazio/Roma/Rome/_Texts/Regionaries/home.html The Regionaries: A Fourth-Century Description of the XIV Regions of Rome]” in [http://penelope.uchicago.edu/Thayer/E/Roman/home.html ''LacusCurtius: Into the Roman World'']. Retrieved 2014-08-01</ref> The camp was discovered during digging for the foundations of a convent and hospital and partially excavated from 1904-1909.<ref name=richardson>Richardson jr., L: ''A New Topographical Dictionary of Ancient Rome'', JHU Press, 1992, p. 78</ref> It can now be found just south-east of the well preserved church [[Santo Stefano Rotondo|S. Stefano Rotondo]].

== Function ==
As the name suggests, the barracks housed a garrison of ''[[Peregrinus (Roman)|peregrini]]''; soldiers from the provincial armies detached for special services in Rome. These consisted of a majority of ''[[frumentarii]]''<ref name=richardson /> and inscriptions suggest that the ''Castra Peregrina'' acted as a central base for the distribution of these men throughout the unarmed provinces (''inermes'').<ref>Rankov, N.B.: “Frumentarii, the Castra Peregrina and the Provincial Officia” in ''Zeitschrift für Papyrologie und Epigraphik'', vol. 80, 1990, p. 176</ref>
The ''frumentarii'', who were likely based, and not only housed, at the ''Castra Peregrina'', were initially involved in the provision supply service of Rome,<ref>Mann, J.C.: “The Organization of the Frumentarii” in ZPE, vol. 74, 1988, p. 149-150</ref> but were later employed as military couriers and members of the secret service.<ref>Historia Augusta (S.H.A): ''Life of Claudius''; ''Life of Hadrian''; ''Life of Commodus''; ''Life of Macrinus''</ref>

== History ==
Richardson suggests that the brickwork demonstrates an Augustan origin with a second century A.D. rebuilding, probably under Severus,<ref name=richardson />
and another rebuilding in third century A.D.,<ref>Martin, Archer et al.: “A Third Century Context from S. Stefano Rotondo” in ''Memoirs of the American Academy in Rome'', vol. 53, 2008, p. 216</ref> however the earliest mention of the ''princeps peregrinorum'' (camp-commandant) in Rome was during the reign of Trajan.<ref>Baillie Reynolds, P.K.: “The Troops Quartered in the Castra Peregrinorum” in ''The Journal of Roman Studies'', vol. 13, 1923, p. 168-189</ref> It is also mentioned by Ammianus Marcellinus, showing that it was in use as late as the fourth century A.D.<ref>Thayer, Bill (2008-02-10): “[http://penelope.uchicago.edu/Thayer/E/Roman/Texts/Ammian/home.html Ammian: The History]” in [http://penelope.uchicago.edu/Thayer/E/Roman/home.html ''LacusCurtius: Into the Roman World'']. Retrieved 2014-08-01</ref>

== More information ==
Within the ''castra'' was a shrine (''templum'') of Jupiter Redux erected in honour of Severus and Mammaea by a ''centurio frumentarius''.<ref>CIL VI.428 (inscription)</ref> A carving of the construction of the barracks was found at Ostia; on a column in the Square of the Corporations. In the initial excavation of the barracks, stone ships were also found; these were made by soldiers thanking the gods for protecting them from shipwrecks (see [[:it:Fontana della Navicella|Fontana della Navicella]]) and provides further evidence of the spread of the barracks' ''frumentarii'' across the provinces.

== See also ==
* [[Castra Praetoria]]
* [[Castra Nova equitum singularium]]
* [http://romereborn.frischerconsulting.com/ge/CB-014.html Castra Priora equitum singularium]
* [[Frumentarii]]

== References ==
{{Reflist}}

[[Category:Military of ancient Rome]]