{{Infobox person
| name   = Theodore Theodorsen
| image     = Theodore theodorsen.jpg
| image_size     = 140px
| birth_date  = {{birth date|1897|01|8}}
| birth_place = [[Sandefjord]], [[Vestfold]] <br>[[Norway]]
| death_date  =  {{death date and age|1978|11|5|1897|1|8}}
| death_place = [[Long Island]], [[New York (state)|New York]],<br>[[United States]]
| occupation     = [[Aerodynamicist]]
| spouse         = Johanne Magdelene Theodorsen
| parents        = Ole Christian Theodorsen<br>Andrea Larsen
| children       = Muriel Gerd-Preutz<br>Theodore Elliott Theodorsen<br>John Willman Theodorsen
}}

'''Theodore Theodorsen''' (January&nbsp;8, 1897&nbsp;&ndash; November&nbsp;5, 1978) was a Norwegian-American theoretical [[Aerodynamics|aerodynamicist]] noted for his work at [[NACA]] (the forerunner of [[NASA]]) and for his contributions to the study of [[turbulence]].

==Early years==
Dr. Theodore Theodorsen was born in [[Sandefjord]], Norway in 1897 to parents Ole Christian Theodorsen, a chief [[engineer]] in the [[Norway|Norwegian]] merchant marine, and his wife Andrea Larsen. He was the oldest of six children. At one point Theodore’s father had taken examinations for a [[merchant marine]] engineer's license. He was the only applicant who correctly answered a particularly difficult question. To his father’s surprise his then twelve-year-old son was also able to solve the problem.

At age sixteen, after finishing compulsory schooling, Theodorsen  attended gymnasium (equivalent in the USA to senior high school-junior college) in the nearby town of [[Larvik]]. If certain grade levels were attained one would gain a scholarship to university. Norway was then far ahead of most nations in the liberalization of  education. Theodorsen's grades were so outstanding that he was admitted to the leading engineering university in Norway, the [[Norwegian Institute of Technology]] in [[Trondheim]]. In those days there were no dormitories; students rented rooms in private homes. Many of the professors in those days were [[Germany|German]] or [[England|English]], therefore a student at the university had to be familiar with all three languages. Only in mechanical drawing was his grade less than perfect. In those days anyone who achieved perfect grades would receive the honor of an introduction to the [[King of Norway]].

==Emigration==
In 1922 Theodorsen graduated with a master's degree in mechanical engineering and was offered a position at the university as an instructor. It was during that year that one of his students was [[Lars Onsager]] who became a lifelong friend. Onsager also emigrated to the United States and eventually went on to win a [[Nobel Prize in Chemistry]]. Theodorsen, like many Norwegian engineers, decided after some years as an instructor in Norway to emigrate. Jobs for engineers were few and far between in Norway at the time. His wife's family knew a retired Norwegian sea captain who lived in [[Baltimore]], so that became their American destination. They arrived in the United States on board the {{SS|Stavangerfjord||6}} on August 25, 1924.<ref>[http://www.ellisisland.org/search/passRecord.asp?MID=16227418720156843072&LNM=THEODORSEN&PLNM=THEODORSEN&first_kind=1&last_kind=0&TOWN=null&SHIP=null&RF=104&pID=600991050084 ''Theodor Theodorsen. Trondhjem, Norway'' (The Statue of Liberty-Ellis Island Foundation, Inc.)]</ref>

==Johns Hopkins University==
For some time there were few job prospects in [[Baltimore]]. Theodorsen took a job working on the third shift as an oiler at the Sparrows Point electrical generating plant located twenty miles from Baltimore. [[Johns Hopkins University]] advertised for an instructor in mechanical engineering. He obtained the position. His English was even then grammatically perfect and only slightly accented. He taught at Johns Hopkins for five years.  In 1928, his university friend from Norway, Lars Onsager, came to teach at Johns Hopkins for one semester. It was at that time that Onsager suggested to Theodorsen that he obtain a [[doctorate]] in [[Physics]].

Theodorsen's thesis dealt with [[thermodynamic]] and [[aerodynamic]] themes that were to permeate much of his later work, which was developed in two parts: 1) shock waves and explosions and 2) [[combustion]] and [[detonation]].  Through the urging of Dr. [[Joseph Sweetman Ames|Joseph Ames]], president of Johns Hopkins University and Chairman of the Executive Committee of the [[National Advisory Committee for Aeronautics]] ([[NASA]]),  Theodorsen came to NACA in 1929  as an associate [[physicist]].

==National Advisory Committee for Aeronautics==
The NACA facility was located adjoining the [[Langley Air Force Base]] near [[Hampton, Virginia]]. This was then the only in-house research arm of NACA and had a highly motivated young staff. The work atmosphere was very informal though competitive, with much open stimulating discussion. However, conditions were rather primitive. For example the library consisted of one small shelf of books. Theodorsen used as his mainstays ''Hutte Mechanical Engineering Handbook'' and a set of the 1929 edition of the ''Handbuch der Physik''.

Within a short time Theodorsen was made head of the Physical Research Division, the other research divisions being Engine Research and [[Aerodynamics]]. Langley NACA was then in the process of expanding its experimental facilities to include a Full Scale Wind Tunnel and a [[Hydrodynamic]] Towing Basin for testing flying boat hulls. It happened that the proposed location of the towing basin had formerly  been a bombing range. One of Theodorsen's  first activities was the invention of an instrument for detecting buried metals and on its very first use it located a live bomb.

The ensuing years were highly productive ones for Theodorsen in a great variety of experimental and theoretical areas. As an overview Theodorsen improved thin airfoil theory by introducing the angle of best streamlining, went on to develop the now classical and elegant  theory of arbitrary wing sections, performed the first in-house noise research, worked on fire prevention in aircraft and on means of icing removal and prevention, contributed to the theory of open, closed and partially open wind-tunnel test sections, developed the basic theory of aircraft [[Wing flutter|flutter]] and its verification, made early measurements of skin friction at [[transonic]] and [[supersonic]] speeds, developed the use of [[freon]] for experimental aeroelastic work, gave damping properties of structures and expanded general propeller theory. During [[World War II]] Theodorsen was called on for the analysis and troubleshooting  of many aircraft problems and to help devise necessary modifications.<ref>[http://www.hq.nasa.gov/pao/History/SP-4305/ch5.htm#136\ ''The Cowling Program: Experimental Impasse and Beyond'' (Aerospace Historian. Fall 1985)]</ref>

==Expanding on Significant Themes==
Theodorsen was an innovative practical engineer at a time in which most of his contemporary  theoretical aerodynamists were  located at educational institutions and thus were not involved in practical engineering solutions. Theodorsen’s work is especially significant in that it still plays an important role in current research and technology.<ref>[http://www.nxtbook.com/nxtbooks/naylor/ISTS0508/index.php?startid=30#/30/OnePage ''The Piston Engine – Part 2'' (Jetrader - September/October 2008 – page 30)]</ref>

The theory of arbitrary airfoils based on conformal mapping developed by Theodorsen, is a model of classical applied mathematics.  It should be pointed out that there are two key concepts that made Theodorsen’s approach different from and a clear improvement on the methods that preceded it such as that of [[von Mises]] and [[von Karman]]. One was the important use of the complex variable not in the usual form of a [[polynomial]] or power series but in the form of an exponential to power series. The equation led directly to the basic boundary value equation which, as an integral equation, represents an exact solution of the problem in terms of the given [[airfoil]] data. This solution gave the exact pressure distribution around an airfoil of arbitrary shape. Seldom in [[aeronautics]] are solutions “exact”. This is one of the very few.  The method has been automated so that complete pressure distributions for a given airfoil section can be obtained in a matter of seconds. The philosophy in Theodorsen’s approach was that an exact formulation is often simpler and  preferable  to an approximate one and that while approximations  are essential in applied mathematics they should be delayed as far as possible.<ref>[http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19760005929_1976005929.pdf ''Adaptation of the Theodorsen Theory to the Representation of an Airfoil as a Combination of a Lifting Line and a Thickness Distribution'' (NASA TN D-8117, Raymond L. Barger. Langley Research Center)]</ref>

Another topic that merits discussion is Theodorsen’s  work on [[Aeroelasticity|flutter]]. The approach here is again direct and clean, leading to an explicit  exact solution as contrasted with previous implicit and approximate results. This exact flutter solution including results for control surfaces has had a keystone role in the development of flutter methods in the United States. It has enabled an engineering feel for the effects of variables and parameters in complex situations and has been available as a model against which approximate solutions can be compared.

Although Theodorsen leaned strongly toward basic theoretical analysis, he usually accompanied his work with experimental verification. He was highly innovative in [[engineering]] and experimental activities where he always sought a theoretical framework or was guided by physical intuition. He was responsible for proposing a wind tunnel for flutter work which employed a mixture of air and freon with variable pressure to greatly increase the scope of research with aeroelastic models throughout the [[Mach number|Mach]] range and with lower horsepower requirements. The [[Transonic]] Dynamics Wind Tunnel now used exclusively for aeroelastic research is based on the same principles.

Another unique facility due to Theodorsen was the helicopter rotor tower for [[aerodynamic]] and noise research. Ideal propeller dynamics was given a definitive treatment in
several reports and a book. Theodorsen was the earliest to obtain reliable skin-friction drag data at [[Subsonic flight|subsonic]], [[transonic]] and [[supersonic]] speeds.<ref>[http://naca.central.cranfield.ac.uk/reports/1949/naca-report-924.pdf ''Application of Theodorsen’s Theory to Propeller Design'' (John L. Crigler. Langley Aeronautical Laboratory. 1948)]</ref>

==Later years==
After leaving [[NACA]]  in 1946 Theodorsen  helped  to organize and administer the [[Instituto Tecnológico de Aeronáutica]] (Aeronautical Institute of Technology) (1947–1950) in Brazil. Then he served as Chief Scientist  for the [[U.S. Air Force]] (1950–1954) during which time he did important work on the structure of [[turbulence]]. Theodorsen then became the Chief of Research  for the [[Republic Aviation Corporation]]  (manufacturer of the famous [[P-47 Thunderbolt]]  fighter plane of World War II, and after the war the [[F-84 Thunderjet]] and the [[F-105 Thunderchief]]) a post from which he retired in 1962 when he became an active consultant  to the [[Sikorsky Aircraft|Sikorsky]] Helicopter Corporation where he specialized in ducted propeller work and helicopter rotors.

==Turbulence Theory==
A significant development was his contribution to the structure of [[turbulence]] in a paper honoring [[Ludwig Prandtl]]’s 75th birthday. The universality of turbulence from microphenomena to [[astrophysics]] is well known as for example, the hypothesis that the planets have condensed from a gaseous cloud and that the angular momentum of the solar system is a result of the action of [[viscosity]] in the [[nebula]]. [[Turbulence]] remains as the major unsolved domain of [[fluid mechanics]]. Theodorsen identified  the main turbulence-creating terms in the equations of motion as (q x curl q . curl curl q); he showed that two-dimensional turbulence cannot exist; that [[vortex]] lines [[vortex stretching|stretching]] and bending is the important mechanism and ingredient of [[turbulence]]. He also  discussed the hierarchy of [[vortices]] ([[Kolmogorov]]).

==Theory of Relativity==
Although Theodorsen's life work was in aerodynamics, and he published numerous books and papers in that field, he had other interests. In particular, he wrote a paper, ''Relativity and Classical Physics'' which sought to show that the results of [[Einstein]]'s theory of [[general relativity]] could be obtained without resorting to curved space-time by a modification of [[Newton's law of universal gravitation]]. The paper presents "a successful transformation of the theory of relativity into classical physics... The mathematical entities of the Einstein development have been redefined into rational physical quantities and rearranged in an organized classical framework. Einstein's space-time has been eliminated and replaced by cognitive time." It was published in the ''Proceedings of the [[DKNVS]] Theodorsen Colloquium'' and on two later occasions.<ref>[http://ask.bibsys.no/ask/action/show?pid=822191024&kid=biblio ''Proceedings of the Theodorsen Colloquium 1976'' (Det Kongelige norske videnskabers selskab. special editor: Leif N. Persen, Universitetsforlaget. Trondheim:  1977)] ISBN 82-00-23091-0</ref> See also: ''Relativity and Classical Physics" <ref>[http://gsjournal.net/Science-Journals/Research%20Papers-Relativity%20Theory/Download/4343 ]</ref>

==Personal life==
In 1922 Theodorsen married Johanne Magdelene Hoem. Her family was a well known respected family in [[Trondheim]]. They were married in the famous [[Nidaros Cathedral]], the largest existing medieval church in [[Scandinavia]]. The wedding party was an all-night celebration at the [[Britannia Hotel (Trondheim)|Britannia Hotel]] with one hundred guests. Their daughter Muriel Gerd-Preutz and sons Theodore Elliott and John Willman were all born in the United States.

In 1976, Dr. Theodorsen was awarded an honorary doctorate degree from the [[Norwegian University of Science and Technology]] in Trondheim. The [[Royal Norwegian Society of Sciences and Letters]], also held a colloquium in his honor at Trondheim. After a short illness, Dr. Theodorsen died in 1978 at the age of eighty one at his home in [[Centerport, New York|Centerport]], [[Long Island, New York]].<ref>[http://www.ntnu.no/forskning/aeresdoktorer ''Æresdoktorer'' (Norwegian University of Science and Technology)]</ref>

==Selected works==
*''The Theory of Wind-Tunnel Wall Interference'' (1931) 
*''A new principle of sound frequency analysis'' (1931) 
*''The prevention of ice formation on gasoline tank vents'' (1931)
*''General Potential Theory of Arbitrary Wing Sections'' (1932)
*''Experimental Verification of the Theory of Wind-Tunnel Boundary Interference'' (1934) 
*''General theory of aerodynamic instability and the mechanism of flutter'' (1935) 
*''Characteristics of six propellers including the high-speed range''  (1937) 
*''Nonstationary flow about a wing-aileron-tab combination including aerodynamic balance''  (1942) 
*''Extension of The Chaplygin Proofs on the Existence of Compressible Flow Solutions to the Supersonic Region'' (1946) 
*''Theory of Propellers'' (1948)
*''The Structure of Turbulence''   (1954) 
*''Theory of Static Propellers and Helicopter Rotors'' (1968)

==References==
{{Reflist}}

==Sources==
*Dowell, Earl H.(editor) ''A Modern view and appreciation of the works of Theodore Theodorsen, physicist and engineer''   (American Institute of Aeronautics and Astronautics: 1992) ISBN 0-930403-85-1
*Anderson, John David ''A History of Aerodynamics and its impact on Flying Machines'' (Cambridge University Press. 1999)  ISBN 0-521-66955-3 
*Hansen, James R.  (editor) ''The Wind and Beyond: A Documentary Journey into the History of Aerodynamics in America'' (National Aeronautics and Space Administration. 2003) ISBN 0-7567-4314-1
*Jones, R. T. (compiler) ''Classical Aerodynamic Theory''  (University Press of the Pacific.  2005) ISBN 1-4102-2489-9

==External links==
*[http://ntrs.nasa.gov/search.jsp?N=0&Ntk=Author-Name&Ntt=Theodorsen&Ntx=mode%20matchall List of Publications at NASA]
*[https://scholar.google.com/scholar?as_q=&num=100&btnG=Search+Scholar&as_epq=&as_oq=&as_eq=&as_occt=any&as_sauthors=t-theodorsen&as_publication=&as_ylo=1897&as_yhi=1978&as_allsubj=all&hl=en&lr=&safe=off&as_eq=polyurethane+wood&client=firefox-a List of Publications from Google Scholar]

{{Authority control}}

{{DEFAULTSORT:Theodorsen, Theodore}}
[[Category:1897 births]]
[[Category:1978 deaths]]
[[Category:People from Sandefjord]]
[[Category:Norwegian emigrants to the United States]]
[[Category:Aerodynamicists]]
[[Category:Norwegian engineers]]
[[Category:Norwegian Institute of Technology alumni]]
[[Category:Johns Hopkins University faculty]]
[[Category:American aerospace engineers]]
[[Category:People from Centerport, New York]]