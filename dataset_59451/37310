{{good article}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Bat Out of Hell III: The Monster is Loose
| Type        = studio
| Artist      = [[Meat Loaf]]
| Cover       = The Monster is Loose Bat Out of Hell 3 album cover.jpg
| Alt         = Cover shows a muscular long-haired, blond man wielding a sword, while a skimpily clad woman lies by fallen pillars.
| Released    = October 20, 2006 (Ireland)<br/>October 23, 2006 (UK)<br/>October 31, 2006 (US)
| Recorded    = 2005–2006
| Genre       = [[Hard rock]], [[Wagnerian rock]], [[Heavy metal music|heavy metal]]
| Length      = 77:28
| Label       = [[Mercury Records|Mercury]] (UK)<br/>[[Virgin Records|Virgin]] (US)
| Producer    = [[Desmond Child]]
| Last album  = ''[[Bat out of Hell: Live with the Melbourne Symphony Orchestra|Live with the Melbourne Symphony Orchestra]]''<br/>(2004)
| This album  = '''''Bat Out of Hell III: The Monster is Loose'''''<br/>(2006)
| Next album  = ''[[Hang Cool Teddy Bear]]''<br/>(2010)
}}

'''''Bat Out of Hell III: The Monster is Loose''''' is the tenth studio album by [[Meat Loaf]], and the third and final one in the ''Bat Out of Hell'' trilogy. It was released in October 2006, 29 years after ''[[Bat Out of Hell]]'' (1977), and 13 years after ''[[Bat Out of Hell II: Back into Hell]]'' (1993).

Produced by [[Desmond Child]], it is the only ''Bat'' album not involving [[Jim Steinman]] in its production. The album was subject to a legal dispute between Meat Loaf and Steinman, who had registered the phrase "Bat Out of Hell" as a [[trademark]] and attempted to prevent the album using the phrase. In the end, seven songs that Steinman wrote for various other projects were included.

As with its predecessors, the album received mixed reviews. A tour, named [[The Seize the Night Tour]], followed the release, concentrating upon songs from the ''Bat'' albums.<ref>{{cite web |first=Jonathan |last=Cohen |work=Billboard |title=Meat Loaf Awakens The Beast For 'Bat III' |url=http://www.billboard.com/articles/news/57666/meat-loaf-awakens-the-beast-for-bat-iii |archiveurl=https://web.archive.org/web/20070929155620/http://www.billboard.com/articles/news/57666/meat-loaf-awakens-the-beast-for-bat-iii |archivedate=2007-09-29 |accessdate=2006-09-25}}</ref>

==History==
According to a [[Reuters]] report, Meat Loaf and Steinman started working on an album in 2001.<ref name="graff">{{cite news |first=Gary |last=Graff |title=Meat Loaf unleashes "Bat" for third flight |date=2006-10-28 |work=Reuters |url=http://uk.reuters.com/article/peopleNews/idUKN2716092720061030 |accessdate=2007-09-11| archiveurl= https://web.archive.org/web/20071017143837/http://uk.reuters.com/article/peopleNews/idUKN2716092720061030| archivedate= 17 October 2007 <!--DASHBot-->| deadurl= no}}</ref> During the concerts in his ''Hair of the Dog'' tour, Meat Loaf made a point to mention that he and Steinman were putting out a new album.

The composer suffered some health setbacks around 2004. Meat Loaf says that "lawyers worked for over a year putting together a contract for Steinman to do ''Bat Out of Hell III''. It was one of the best producer's contracts in the history of the record business." Ultimately, according to the singer, Steinman was not well enough to work on such an intense project and made what he calls the "selfish" decision to go ahead without him. In promotional interviews he said that he did not want to wait a year and a half just to find out that Steinman was still unfit.<ref name="thismorning">{{cite interview |title=Meat Loaf |interviewer=[[Ruth Langsford]], [[Phillip Schofield]] |publisher=ITV1 |location= London |date=May 2007 |work=[[This Morning (TV series)|This Morning]]}}</ref> Describing himself as a "really loyal  person," Meat Loaf says that "the decision not to use Steinman has taken its toll on me."<ref name="graff"/>

However, in 2006, [[David Sonenberg]], Steinman's manager, said:
<blockquote>Jim's health is excellent. That's not the reason he didn't participate in (''Bat III''). He had some meaningful health problems about four years ago, but he's been totally healthy the last couple of years. His health in no way impacted on his involvement in the ''Bat Out of Hell'' project.<ref name="graff"/></blockquote>

The development problems and confusion over Steinman's involvement is a result of a dispute of the [[trademark]] "Bat Out of Hell", which Steinman registered in 1995.<ref name="butler">{{cite web |first=Susan |last=Butler |work= Billboard |title=Meat Loaf Sues Over 'Bat Out Of Hell' |url=http://www.neverlandhotel.dk/news/index.php?nr=155 |accessdate=2006-09-25}}</ref> Meat Loaf sued Steinman and his manager, in a complaint filed May 28, 2006 in federal District Court in [[Los Angeles]], [[California]], for $50 million and to prevent further use by the writer/producer.<ref>{{cite news |title=MEAT LOAF BATTLES FOR BAT OUT OF HELL TRADEMARK |date=2006-06-06 |work=contactmusic.com |url=http://www.contactmusic.com/new/xmlfeed.nsf/mndwebpages/meat%20loaf%20battles%20for%20bat%20out%20of%20hell%20trademark_06_06_2006 |accessdate=2006-11-14}}</ref>

Meat Loaf has stated that he contributed lyrics to "Bat Out of Hell". He had used the phrase extensively for tours, to which Steinman had never objected "until a recent falling out."<ref name="butler"/> Steinman and his representatives approached Meat Loaf's labels, Universal and Virgin, asserting trademark ownership and threatening litigation<ref name="butler"/> to prevent the album's release.<ref name="cnasia">{{cite news |first=Zul |last=Othman |title=Man out of hell |date=2006-10-26 |work=ChannelNews Asia |url=http://www.channelnewsasia.com/stories/entertainment/view/237668/1/.html |archive-url=http://archive.is/20061107091041/http://www.channelnewsasia.com/stories/entertainment/view/237668/1/.html |dead-url=yes |archive-date=2006-11-07 |accessdate=2006-11-14}}</ref> An agreement was reached in Summer 2006. According to Virgin, "the two came to an amicable agreement that ensured that Jim Steinman's music would be a continuing part of the 'Bat Out of Hell' legacy."<ref>{{cite news |title=MEATLOAF REACHES AGREEMENT OVER BAT OUT OF HELL |date=2006-08-01 |work=contactmusic.com |url=http://www.contactmusic.com/news.nsf/article/meatloaf%20reaches%20agreement%20over%20bat%20out%20of%20hell_1004104 |accessdate=2006-11-14}}</ref> In promotional interviews, Meat Loaf has played down the dispute with Steinman, pointing out that it was over in three weeks and was purely for the sake of business.

<blockquote>I consider him to be one of my best friends but the real thing is about managers: I think Steinman's manager is the devil and Steinman feels the same way about my manager. So, we had to communicate through managers and he refused to sign some papers that would have allowed for the recording of ''Bat Out of Hell III'' without a hitch. So, really, I didn't sue Jim Steinman. I sued his manager.<ref name="cnasia"/></blockquote>

Despite the fact that Steinman was not involved in the recording or production, the album does include seven of his songs, five of which are covers of previously released songs. The other two were covers of Steinman's demos intended for musical theater projects, at the time unreleased. The agreement enabled Steinman to work on a musical theatre project based on all of the songs from ''Bat Out of Hell''. Describing the project as "''[[Cirque du Soleil]]'' on [[Lysergic acid diethylamide|acid]]", in September 2007, he expected it to open in [[London]] in 2010.<ref name="cour">{{cite news|first=Maryellen |last=Fillo |title=A Bat Out Of Hell Who Whistles |work=Hartford Courant |url=http://www.courant.com/features/lifestyle/hc-nujavaspilling0928.artsep28,0,6539032.story |date=2007-09-28 |accessdate=2007-09-29 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

==Production==
Meat Loaf had announced that [[Michael Beinhorn]] was producing the record, but [[Desmond Child]] took the helm. The singer reports that Child would say things that made him think he was sitting next to Steinman.<ref name="radio2">{{cite interview |title=Meat Loaf |interviewer=[[Steve Wright (DJ)|Steve Wright]] |publisher=BBC Radio 2 |location= London |date= 2006-08-04 |work=Steve Wright in the Afternoon}}</ref> Child began recording sessions by playing [[Slipknot (band)|Slipknot]] CDs to get the assembled musicians in the mood.<ref name="graff"/> In addition to musicians from his touring band, the [[Neverland Express]], several guest players contributed to the album. Meat Loaf says, "I didn't just want to bring in rock players — I wanted to go to extreme rock people" resulting in an album that "has all the touches of the other two ''Bats'', but it's much more of a rock album."<ref name="graff"/> Child says, that one of the most memorable experiences working on the album was working with [[Brian May]], who played on "Bad for Good".<ref name="mob2">{{cite video |date=2006 |title=The Making of Bat Out of Hell III | medium=DVD |publisher=Mercury Records}}</ref> The album also featured guest performances by [[John 5 (guitarist)|John 5]], [[Steve Vai]] and [[John Shanks]].

Meat Loaf says that he does not enjoy recording in studios. He compares the process to "going to the dentist and having [[root canal]] everyday". Whereas you ''really'' have to be flat for people to notice it in a live show, in a studio, every "nuance is under a microscope... and I'm a perfectionist who knows that there's no such thing as perfection but I try to get as close as I can."<ref name="thismorning"/>

[[Todd Rundgren]], who produced the first album and arranged all of the background vocals for ''Bat II'', arranged the background vocals for three tracks. In a promotional interview for the album Rundgren has said "continuity is an important thing."<ref name="mob2"/> However, in another interview he said that his contribution was considerably less than on some earlier Meat Loaf albums.

<blockquote>And by the time they got it all organized and figured out, there were really only a couple of songs left for me to do anything on. So I came into L.A. for a couple of days and [[Kasim Sulton|Kasim]]... came in, as was the routine, and we did maybe three songs... just so that I’d have a few fingerprints on the record. I think someone, maybe Meat Loaf, said that to keep everything covered, I had to be in there somewhere... but not necessarily running the whole thing. So my involvement was pretty much peripheral.<ref>{{cite web |first=Will |last=Harris |title=A Chat with Todd Rundgren |work=bullz-eye.com |url=http://www.bullz-eye.com/music/interviews/2007/todd_rundgren.htm |date=2006-12-01 |accessdate=2007-09-11| archiveurl= https://web.archive.org/web/20070826231725/http://www.bullz-eye.com/music/interviews/2007/todd_rundgren.htm| archivedate= 26 August 2007 <!--DASHBot-->| deadurl= no}}</ref></blockquote>

The album features three duets. Norwegian vocalist [[Marion Raven]] duets on "It's All Coming Back to Me Now". "What About Love?" features [[Patti Russo]], who has toured with Meat Loaf for over thirteen years. Finally, [[Jennifer Hudson]] performs on "The Future Ain't What It Used to Be".

==Compositions==
The album opens with the title track. "The Monster Is Loose" is highly influenced by the Gothic style of [[John 5 (guitarist)|John 5]], who plays the main guitar parts. The song is very heavy and continues Meat Loaf's association with [[Major League Baseball]] established with [[Phil Rizzuto]]'s commentary on "[[Paradise by the Dashboard Light]]."<ref name="graff"/> "Blind as a Bat", written by Desmond Child, tells about how one is thankful for the love another has given him, even after he has done deeds to suggest that he does not deserve such love.<ref>"Blind as a Bat", written by Desmond Child</ref>

{{Listen
|filename     = It's All Coming Back to Me Now - Meat Loaf.ogg
|title        = "It's All Coming Back to Me Now"
|description  = This clip features the beginning of the final chorus, displaying Raven's harmonies.
}}

According to Steinman, "[[It's All Coming Back to Me Now]]" was inspired by ''[[Wuthering Heights]]'', and was an attempt to write "the most passionate, romantic song" he could ever create.<ref>{{cite web |work=JimSteinman.com |title=Jim Steinman on "It's All Coming Back To Me Now" |url=http://www.jimsteinman.com/backtome.htm |accessdate=2006-09-04| archiveurl= https://web.archive.org/web/20060903072121/http://www.jimsteinman.com/backtome.htm| archivedate= 3 September 2006 <!--DASHBot-->| deadurl= no}}</ref> In interviews, Meat Loaf has said that, in his mind, the song was always meant to be a duet.<ref name="Asia">{{cite news |first=Zul |last=Othman |title=Man out of hell |work=Channel NewsAsia |date=2006-10-26 |url=http://www.channelnewsasia.com/stories/entertainment/view/237668/1/.html |archive-url=http://archive.is/20061107091041/http://www.channelnewsasia.com/stories/entertainment/view/237668/1/.html |dead-url=yes |archive-date=2006-11-07 |accessdate=2006-10-26}}</ref> Norwegian artist [[Marion Raven]], who had been working on her solo album with Child, was chosen because the [[timbre]] of her voice starkly contrasts to Meat Loaf's.<ref name="xmr">{{cite episode |title=Bat Out of Hell III |series=Liner Notes |network =[[XM Satellite Radio]] |airdate=2006-09-11}}</ref>

"Bad for Good" was one of the many songs written by Steinman under the inspiration of ''[[Peter Pan]]'' and [[Peter Pan's Lost Boys|lost boys]] who never grow up.<ref name="bbc">{{cite web |title=BBC Rock Hour Special: Jim Steinman; Transcription of BBC Radio Broadcast, 1981 |url=http://www.jimsteinman.com/bbc.htm |accessdate=2007-08-28| archiveurl= https://web.archive.org/web/20070814030841/http://www.jimsteinman.com/bbc.htm| archivedate= 14 August 2007 <!--DASHBot-->| deadurl= no}}</ref> This is reflected in lyrics such as "You know I'm gonna be like this forever/I'm never gonna be what I should." The song was written to appear on the follow-up to ''Bat Out of Hell'', but which Steinman recorded himself. Because of this, Meat Loaf was aware that there is a "core of fans that know that song," so he "had that under the microscope more than any other on the album."<ref name="mob2"/>

"[[Cry Over Me]]" is, according to Meat Loaf, a timeless song dealing with relationships of all kinds. In a 2007 interview, he says that it can be about your first or last loves, or dealing with your boss at work. Partially quoting the lyrics, the singer posits that there are times when "you want him to feel exactly like I felt when he said that to me."<ref name="thismorning"/>

''[[The Guardian]]'' says "In the Land of the Pig (The Butcher Is King)" is "five Olympian minutes crying out for a full production at [[Glyndebourne Festival Opera|Glyndebourne]]."<ref>{{cite web |first=Caroline |last=Sullivan |title=Meat Loaf, Bat Out of Hell III |url=http://arts.guardian.co.uk/reviews/story/0,,1926073,00.html |work=The Guardian |date=2006-10-20 |accessdate=2007-09-11}}</ref> Guitarist [[Steve Vai]] describes it as "very Gothic; almost terrifying."<ref name="mob2"/> It is about the intense power over subordinates:

:''Can't you hear the choir now?''
:''Listen to the animals sing.''
:''Can't you hear the [[slaughterhouse]] bells?''
:''In the land of the pigs the butcher is king.''

"Monstro" is a bombastic orchestral piece layered with chorals that lead into the piano introduction to "Alive." Meat Loaf decided to hire Desmond Child when he revealed that he had written "Alive" especially for the album. The song refers to how the singer has overcome difficult periods in his life.<ref name="xmr"/>

"What About Love", a piano-based duet with [[Patti Russo]], is a sexually charged song that echoes "[[Paradise by the Dashboard Light]]" from the 1977 album. Here, though, the singers are singing about love throughout, not bitterness. The final verse contains the most explicit lyrics about their first sexual encounter.

:''[Boy:]''
:''I can't forget the feeling of your sweat upon my skin.''
:''And the tremble of your body on the day you let me in...''

:''[Girl:]''
:''On a summernight's surrender with nothing to lose.''
:''You were scared and so was I when I gave myself to you...''

"Seize the Night" has a strong orchestral foundation underneath the lead vocals and a choir. A duet with [[Jennifer Hudson]], "The Future Ain't What It Used to Be" is a pessimistic song based upon the myth that [[Pandora]] closed her jar before allowing "hope" to escape (the song first appeared on the only album performed by female group [[Pandora's Box (band)|Pandora's Box]]). The lyrics reveal the hopelessness of the past (''Were there ever any stars in the sky?'') and the future (''There's nothing so sad as a tomorrow gone bad'').

The final song of the ''Bat'' trilogy is a short one written by Steinman. A few lyrics of ''Cry to Heaven'' begin rather sweet, but turning rather bitter: (''Cry baby cry/Cry, cry to heaven/If that doesn't do it for you/Go ahead and cry like hell''.) The two parts are bridged by an instrumental dominated by an [[Irish flute]].

==Cover and booklet==
The cover follows the style of the previous two albums called ''Bat Out of Hell''. [[Julie Bell]] designed the cover and the artwork that appears alongside the lyrics in the booklet. She also supplied the art for the "It's All Coming Back to Me Now" single. The cover features the biker from the first two covers about to slay the giant bat from the second cover, while the angel from the same cover takes cover behind a destroyed pillar.<ref>{{cite web |first=Randall |last=Larson |work=Cinescape |title= New Bat Out Of Hell Album from Meat Loaf |url=http://www.cinescape.com/0/editorial.asp?aff_id=0&this_cat=Music+%26+Audio&action=page&type_id=&cat_id=270466&obj_id=51370# |accessdate=2006-09-24}}</ref> Like the first two ''Bat'' albums, Steinman's songwriting is credited on the cover, this time shared with Desmond Child.

The booklet contains all of the lyrics to the songs, each page featuring a small illustration. The CD liner contains a dedication "For thirty years of friendship and inspiration, ''Bat Out of Hell III'' is dedicated to Jim Steinman."

==Reception==
{{Album reviews
|rev1 = [[AllMusic]]
|rev1score = {{Rating|2|5}} [{{Allmusic|class=album|id=r929716|pure_url=yes}} AllMusic]
|rev2 = ''[[The Guardian]]''
|rev2score = {{Rating|4|5}} [http://arts.guardian.co.uk/reviews/story/0,,1926073,00.html The Guardian]
|rev3 = ''[[The Independent]]''
|rev3score = {{Rating|2|5}} [http://enjoyment.independent.co.uk/music/reviews/article1902898.ece The Independent]
|rev4 = ''NeuFutur''
|rev4score = {{Rating|6.5|10}} [http://www.neufutur.com/Reviews/meatloaf2.html NeuFutur]
|rev5 = ''[[Q (magazine)|Q]]''
|rev5score = {{Rating|4|5}}
|rev6 = ''[[Rolling Stone]]''
|rev6score = {{Rating|2|5}} [http://www.rollingstone.com/artists/meatloaf/albums/album/12015098/review/12053896/bat_out_of_hell_iii Rolling Stone] <!--Andy Greene, Oct 17, 2006-->
|noprose = yes
}}

The album debuted at #8 on the [[Billboard 200|''Billboard'' 200]] and sold about 81,000 copies in its opening week, his best since ''Bat Out of Hell II: Back Into Hell''.<ref>{{cite news |first=Katie |last=Hasty |title='Montana' Zooms By Manilow For Second Week At No. 1 |url=http://www.billboard.com/articles/news/56725/montana-zooms-by-manilow-for-second-week-at-no-1 |archiveurl=https://web.archive.org/web/20070929133257/http://www.billboard.com/articles/news/56725/montana-zooms-by-manilow-for-second-week-at-no-1 |archivedate=2007-09-29 |work=Billboard.com |date=2006-11-08 |accessdate=2007-09-11}}</ref> However, it slipped to #60 after three weeks. The album also reached #3 in the UK charts, but quickly fell off.

''[[Q (magazine)|Q]]'' gave the album a positive review, calling it "the second-best album to bear the 'Bat' name", and saying that Child did an "impressive recreation of Steinman's [[Andrew Lloyd Webber]]-on-steroids approach", while the album was "overblown, frequently ridiculous and largely devoid of irony." They were unimpressed with the title track, suggesting that "whoever decided it would be a good idea for Meat Loaf to tackle [[nu metal]]…should be tarred and feathered." ''Q'' did, however, praise the "operatic" vocals and May's "fabulously hysterical guitar" on the track "Bad for Good".<ref>''[[Q (magazine)|Q]]'', October 2006</ref> ''[[The Village Voice]]'' named it as 'Album of the Year This Week', calling it "absurdist, righteous majesty".<ref>{{cite web |first=Rob |last=Harvilla |title=Stuff You Need to Know This Week to Avoid Ostracism |work=The Village Voice |url=http://www.villagevoice.com/music/0641,harvilla,74677,22.html |accessdate=2006-10-12| archiveurl= https://web.archive.org/web/20061016035941/http://www.villagevoice.com/music/0641,harvilla,74677,22.html| archivedate= 16 October 2006 <!--DASHBot-->| deadurl= no}}</ref>

Some reviews lamented Steinman's absence. According to [[Allmusic]], "this ''Bat'' is quite obviously a patchwork, pieced together from things borrowed and recreated, never quite gelling the way either of the previous ''Bats'' did." They criticized "The Monster Is Loose" as a "disarming, a grindingly metallic riff-rocker that sits very uncomfortably next to Steinman's "It's All Coming Back to Me Now", and Child as "a professional who is playing a game without bothering to learn the rules." On the other hand, the review commended Meat Loaf's voice: he sings "his heart out as he valiantly tries to make this ''Bat'' a worthy successor to the originals."<ref>{{cite web |first=Stephen Thomas |last=Erlewine |title= Bat Out of Hell III: The Monster Is Loose |publisher=[[AllMusic]] |url={{Allmusic|class=album|id=r929716|pure_url=yes}} |accessdate=2006-10-17}}</ref>

==Singles and music videos==
[[Image:Its All Coming Back To Me Now - Meat Loaf video.JPG|alt=Head and shoulders photo of a short haired, stock man with a frown on his face. A woman is wailing in the background.|thumb|300px|right|Meat Loaf's character mourning that of Marion Raven, in the 2006 video directed by P. R. Brown.]]

"It's All Coming Back to Me Now" was the first song released as a single. It reached number one in Raven's native [[Norway]], and the top ten in both the United Kingdom and Germany.<ref>{{cite web |title=Meat Loaf & Marion Raven: It's All Coming Back to Me Now |work=top40-charts.com |url=http://top40-charts.com/song.php?sid=17797&sort=chartid&string=meat |accessdate=2007-06-13}}</ref> "Blind As a Bat" was scheduled to be released in the UK on December 18, but was then put back to February 26, as two CDs.<ref>{{cite web|title=Blind As A Bat *Multi-Buy* (1xCD1 + CD2) |work=Townsend Records |url=http://www.townsend-records.co.uk/product.php?pId=10002221&pType=music |accessdate=2006-11-28 |deadurl=yes |archiveurl=https://web.archive.org/web/20070928140955/http://www.townsend-records.co.uk/product.php?pId=10002221&pType=music |archivedate=2007-09-28 |df= }}</ref> In turn, this single was pulled at the last minute, in favor of "[[Cry Over Me]]", which was released on May 7, 2007.<ref>{{cite news |author=Deb |title=Cry Over me Released In The UK May 7th 2007! |work=The Naked Wire |url=http://thenakedwire.com/index.php?option=com_content&task=view&id=108&Itemid=1 |date=2007-03-24 |accessdate=2007-04-19}}</ref>

[[P. R. Brown]] directed the videos for "It's All Coming Back to Me Now" and "Cry over Me."<ref name="thismorning"/><ref>{{cite web |work= videos.antville.org |title= Meat Loaf and Marion Raven "It's All Coming Back to Me Now" dir. P.R. Brown |url=http://videos.antville.org/stories/1455654/| accessdate=2006-08-29}}</ref> "It's All Coming Back to Me Now" is an elaborate production, in which Meat Loaf is being haunted by the memory of his dead lover. Told in flashback, Raven's character crashes her car to avoid a man standing in the road. She sings as an unseen spirit following Meat Loaf. It echoes Steinman's comments that the song is about the "dark side of love" and the "ability to be resurrected by it."<ref name="jsopb">{{cite video |people=Jim Steinman |date=1989 |title=Jim Steinman Opens Pandora's Box | medium=DVD |publisher=Virgin Records}}</ref></blockquote>

The video for "Cry over Me" had the lowest budget since those for the original album. A simple video, Meat Loaf has said that it only took four hours to film.<ref name="thismorning"/> The music video for "The Monster Is Loose" consists of animation. Its storyline is about a biker who rescues a girl from an enormous bat-like creature.

The videos for "It's All Coming Back to Me Now", "Cry over Me" and "The Monster Is Loose" are included as bonus features on the ''[[3 Bats Live]]'' DVD, released in October 2007.

==Track listing==
{{Track listing
| writing_credits = yes
| title1          = The Monster is Loose
| writer1         = [[John 5 (guitarist)|John 5]], [[Desmond Child]], [[Nikki Sixx]]
| length1         = 7:12
| title2          = Blind as a Bat
| writer2         = Child, [[James Michael]]
| length2         = 5:51
| title3          = [[It's All Coming Back to Me Now#Meat Loaf and Marion Raven|It's All Coming Back to Me Now]]
| note3           = Duet with [[Marion Raven]]
| writer3         = [[Jim Steinman]]
| length3         = 6:05
| title4          = Bad for Good
| note4           = Featuring [[Brian May]]
| writer4         = Steinman
| length4         = 7:33
| title5          = [[Cry Over Me]]
| writer5         = [[Diane Warren]]
| length5         = 4:40
| title6          = In the Land of the Pig, the Butcher Is King
| writer6         = Steinman
| length6         = 5:38
| title7          = Monstro
| writer7         = Elena Casals, Child, [[Holly Knight]]
| length7         = 1:39
| title8          = Alive
| writer8         = Child, Knight, Michael, Andrea Remanda
| length8         = 4:22
| title9          = If God Could Talk
| writer9         = Child, [[Marti Frederiksen]]
| length9         = 3:46
| title10         = If It Ain't Broke, Break It
| writer10        = Steinman
| length10        = 4:50
| title11         = What About Love?
| note11          = Duet with [[Patti Russo]]
| writer11        = Child, Frederiksen, John Gregory, [[Russ Irwin]]
| length11        = 6:03
| title12         = Seize the Night
| writer12        = Steinman
| length12        = 9:46
| title13         = The Future Ain't What It Used to Be
| note13          = Duet with [[Jennifer Hudson]]
| writer13        = Steinman
| length13        = 7:54
| title14         = Cry to Heaven
| writer14        = Steinman
| length14        = 2:22
}}

All of the [[Jim Steinman]] songs were written for other projects. Like ''Bat II'',<ref>"It Just Won't Quit" and "[[Good Girls Go to Heaven (Bad Girls Go Everywhere)]]"</ref> the album contains two songs ("It's All Coming Back..." and "The Future Ain't What It Used to Be") that originally appeared on ''[[Original Sin (Pandora's Box album)|Original Sin]]'', Steinman's 1989 [[concept album]] with [[Pandora's Box (band)|Pandora's Box]]. Steinman's demo of "In the Land of the Pig, the Butcher Is King" was part of the preparations for the unrealized ''Batman: The Musical'' project. Steinman's "Cry to Heaven" demo was intended for the possibility that Steinman would provide songs for a musical based on the film ''[[Cry-Baby]]''. ''[[Cry-Baby (musical)|Cry-Baby]]'' has since been staged, but without any work from Steinman. "Bad for Good" was featured as the title track on Steinman's [[Bad for Good|1981 album]]. Some of the songs on the ''[[Bad for Good]]'' album were once intended to be a Meat Loaf solo album. "Seize the Night" first appeared as "Carpe Noctem" in the German language musical ''[[Tanz der Vampire]]''. It was also performed in English in the 2002 Broadway show called ''[[Dance of the Vampires]]'', and demos of the song in English became widely available around that time. "If It Ain't Broke, Break It" first appeared in the MTV film ''[[Wuthering Heights (2003 film)|Wuthering Heights]]'', and a recording of the song was released on the soundtrack CD for the film.

==Alternative releases==
A limited edition was released with an accompanying DVD, containing a short "making of" featurette, the animated trailer, and "The Monster is Loose" career montage video. The US version also includes a photo gallery and the "It's All Coming Back to Me Now" video.

[[Best Buy]]'s version of the album came with an exclusive bonus track: a live version of "Testify" (from ''[[Couldn't Have Said It Better]]''). This track was originally announced to be on the CD itself, but Best Buy opted to include an insert in the packaging giving customers a code to download the song. [[Circuit City (1949–2009 company)|Circuit City]]'s version came with an exclusive downloadable track: a live version of "Life Is a Lemon and I Want My Money Back". [[Target Corporation|Target]] released a "Limited Tour Edition" with a concert ticket pre-sale offer. [[Wal Mart]] released the album as part of an exclusive 2-pack with the ''Meat Loaf Bat Out of Hell [[Classic Albums]]'' DVD.

The version of the album available from [[Apple Computer|Apple]]'s [[iTunes Store]] includes two bonus tracks, a live version of "I'd Do Anything for Love", recorded at a concert in Australia, and "Heads Will Roll", sung by Marion Raven from her [[Heads Will Roll|EP of the same name]].

==Tour==
Meat Loaf embarked on a 112 date world tour to promote the album. He performed a concert performing some songs from all three ''Bat Out of Hell'' albums on October 16 at [[London]]'s [[Royal Albert Hall]], and performed many of those songs on the rest of his tour as well. He also performed a "Bat on Broadway" performance on November 2, 2006 at [[New York City|New York]]'s [[Palace Theatre, New York|Palace Theater]], as well as shows in [[Toronto]], [[Atlantic City, New Jersey|Atlantic City]], [[Uncasville, Connecticut|Uncasville]], and [[Mexico City]].

Marion Raven joined Meat Loaf for the first leg of his 2007 tour, [[Seize the Night tour]]. She was the supporting act, promoting her ''[[Set Me Free (Marion Raven album)|Set Me Free]]'' album. Meat Loaf introduced her on stage at the latter stages of the concerts to duet on "It's All Coming Back to Me Now".<ref>{{cite web |title=Norwegian singer tours UK with Meat Loaf |work=norway.org.uk |url=http://www.norway.org.uk/culture/music/marion-raven.htm |archiveurl=https://web.archive.org/web/20070927203846/http://www.norway.org.uk/culture/music/marion-raven.htm |archivedate=2007-09-27 |accessdate=2007-05-25}}</ref>

A DVD of the tour was released in October 2007, entitled ''[[3 Bats Live]]''. It also contains a bonus disc featuring the promotional videos and animations from ''Bat III''.

Portions of the tour were captured on film in the [[Theatrical]] [[Documentary film|Documentary]] ''[[Meat Loaf: In Search of Paradise]]'', directed by Bruce David Klein, which was released in 2008.

==Personnel==
* Lead vocals &ndash; '''Meat Loaf'''
<br>
{{col-start}}
{{col-3}}
;Regular Meat Loaf Studio Sidemen
* [[Kenny Aronoff]] &ndash; [[percussion instrument|percussion]], drums (tracks 2, 6-8, 11, 12)
* [[Brett Cullen]] &ndash; backing vocals  (track 8)
* [[James Michael]] &ndash; backing vocals  (track 2)
* [[Todd Rundgren]] &ndash; backing vocals (tracks 1, 3, 4)
* [[Eric Troyer]] &ndash; backing vocals
{{col-3}}
;The [[Neverland Express]]
* [[Mark Alexander (keyboardist)|Mark Alexander]] &ndash; [[organ (music)|organ]], [[piano]] (tracks 10, 13)
* Carolyn "C.C." Coletti-Jablonski &ndash; backing vocals (tracks 10-13)
* [[Paul Crook]] &ndash; [[guitar]] (tracks 2, 4, 6, 10, 13)
* Randy Flowers &ndash; guitar (tracks 2, 3, 10, 13)
* [[John Miceli]] &ndash; [[drum kit|drums]] (tracks 10, 13)
* [[Patti Russo]] &ndash; female lead vocals (track 11), backing vocals (tracks 10-13)
* [[Kasim Sulton]] &ndash; [[bass guitar]], [[backing vocals]]
{{col-3}}
;Guest performers
* [[Eric Bazilian]] &ndash; guitar (tracks 2-5, 7-9, 11-13)
* [[Jennifer Hudson]] &ndash; guest vocals (track 13)
* [[John Lowery|John 5]] &ndash; guitar (track 1)
* [[Brian May]] &ndash; guitar solo (track 4)
* [[Marion Raven]] &ndash; guest vocals (track 3)
* [[John Shanks]] &ndash; guitar (tracks 8, 11)
* [[Steve Vai]] &ndash; guitar (track 6)
{{col-end}}

;Session Musicians
{{col-start}}
{{col-2}}
* [[Rusty Anderson]] &ndash; guitar (tracks 8, 11-13)
* [[Stephanie Bennett (harpist)|Stephanie Bennett]] &ndash; [[harp]] (tracks 6, 14)
* [[Victor Indrizzo]] &ndash; drums (tracks 1, 3-5, 9)
* [[Corky James]] &ndash; guitar
* Lee Levin &ndash; percussion (track 14)
* David Levita &ndash; guitar (track 3)
* Don Marchese &ndash; [[baritone saxophone]] (track 10)
{{col-2}}
* [[Graham Phillips (actor)|Graham Phillips]] &ndash; boy [[soprano]] (tracks 7, 13, 14)
* [[Eric Rigler]] &ndash; [[Irish flute]] (track 14)
* [[Matt Rollings]] &ndash; piano, organ (tracks 2, 6, 10, 13)
* Bettie Ross &ndash; pipe organ (tracks 7, 8, 12)
* [[Eric Sardinas]] &ndash; electric slide guitar solo (track 10)
* Tom Saviano &ndash; [[tenor saxophone]] (track 10)
* [[Clint Walsh]] &ndash; guitar (track 6)
* Dan Warner &ndash; guitar (tracks 3, 14)
{{col-end}}
* '''backing vocals''' &ndash; [[Becky Baeling]] (track 11), [[Andreas Carlsson]] (track 8), [[Desmond Child]], [[Marti Frederiksen]] (track 11), Diana Grasselli (tracks 2, 7, 13, 14), John Gregory (tracks 3, 11), [[Storm Lee]], [[Jeanette Olsson]], [[Jason Paige]], Keely Pressly (track 3), Camile Saviola (track 10), [[Maria Vidal]] (track 13) 
* '''programming''' &ndash; Randy Canto (track 10), Doug Emery (track 14), Harry Sommerdahl (tracks 2, 7, 9, 10), Chris Vrenna (track 6)
*  '''gospel choir''' (tracks 8, 13) &ndash; Barbara Allen, Vernon Allen, Esther Austin, Bonita Brisco, Cheryl Brown, Sonya Byous, Jessica Jones, Joseph Powell, Sandra Stokes, Roshuan Stovall 
* '''[[trumpet]]''' (track 10) &ndash; Gary Grant, Steve Madaio

;Orchestra
* [[David Campbell (composer)|David Campbell]] &ndash; Conductor and arranger
{{col-start}}
{{col-2}}
* '''[[Violin]]''' &ndash; [[Roberto Cani]], Darius Campo, Mario de Leon, [[Joel Derouin]], Bruce Dukov, Armen Garabedian, Berj Garabedian, [[Endre Granat]], Gerardo Hilera, Sharon Jackson, Peter Kent, Songa Lee-Kito, Natalie Legget, [[Sid Page]], Alyssa Park, Michelle Richards, Haim Shitrum, Tereza Stanislav, Sarah Thornblade, Philip Vaiman, Josefina Vergara, John Witenberg, Ken Yerke
* '''[[Viola]]''' &ndash; Denyse Buffum, Brian Dembow, Andrew Duckles, Matt Funes, Marda Todd, Evan Wilson
* '''[[Cello]]''' &ndash; Larry Corbet, Suzi Katayama, Steve Richards, Daniel Smith
* '''[[Double bass]]''' &ndash; Nico Abondolo, Michael Valerio
{{col-2}}
* '''Trumpet''' &ndash; Rick Baptist, [[Wayne Bergeron]], [[Chuck Findley|Charles Findley]], [[John Fumo]], Jon Lewis
* '''[[Trombone]]''' &ndash; Steven Holtman, Alan Kaplan, [[Bill Reichenbach Jr.]]
* '''[[Bass trombone]]''' &ndash; William Reichenbach, Douglas Tornquist
* '''[[French horn]]''' &ndash; Steven Becknell, Joe Meyer, John Reynolds, Brad Warnaar
* '''[[Tuba]]''' &ndash; Douglas Tornquist
* '''[[Oboe]]''' &ndash; Earle Dumbler
* '''Orchestral percussion''' &ndash; MB Gordy
{{col-end}}

==Charts==
{| class="wikitable sortable plainrowheaders" style="text-align:center"
|-
! scope="col"| Chart (2006)
! scope="col"| Peak<br /> position
|-
{{album chart|Australia|9|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Austria|5|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Flanders|32|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Wallonia|94|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|BillboardCanada|3|artist=Meat Loaf|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Denmark|5|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Netherlands|7|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Germany4|2|id=41191|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Ireland|16|year=2006|week=44|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|New Zealand|5|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Norway|6|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Sweden|10|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Switzerland|3|artist=Meat Loaf|album=Bat Out of Hell III: The Monster Is Loose|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|UK|3|artist=Meat Loaf|rowheader=true|accessdate=October 1, 2016}}
|-
{{album chart|Billboard200|8|artist=Meat Loaf|rowheader=true|accessdate=October 1, 2016}}
|}

==Certifications==
{| class="wikitable"
|-
!align="left"|Country
!align="left"|Certification
!align="left"|Sales/shipments
|-
| [[Canadian Recording Industry Association|Canada]]
| Gold<ref>{{cite web|title=CRIA Gold & Platinum certifications for December 2006 |url=http://www.cria.ca/gold/1206_g.php |work=cria.ca |accessdate=2007-10-02 |archiveurl=https://web.archive.org/web/20071001143307/http://www.cria.ca/gold/1206_g.php |archivedate=1 October 2007 |deadurl=yes |df= }}</ref>
| 50,000
|-
| [[Recording Industry Association of America|United States]]
| Gold
| 500,000
|}

==References==
{{reflist|30em}}

==External links==
* {{metacritic album|id=meatloaf/batoutofhell3}}

{{Meat Loaf}}

{{DEFAULTSORT:Bat Out Of Hell Iii: The Monster is Loose}}
[[Category:2006 albums]]
[[Category:Meat Loaf albums]]
[[Category:Albums produced by Desmond Child]]
[[Category:Sequel albums]]