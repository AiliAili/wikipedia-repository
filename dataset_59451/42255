{|{{Infobox Aircraft Begin
  |name = Auster AOP.9
  |image = Auster AOP.9 XK417 Farnborough 09.56.jpg
  |caption = AOP.9 XK417 at the [[Farnborough Airshow]] in 1956, this aircraft served No. 652 Squadron RAF
}}{{Infobox Aircraft Type
  |type = military observation aircraft
  |manufacturer = [[Auster|Auster Aircraft Limited]]
  |designer = <!--only appropriate for single designers, not project leaders-->
  |first flight = 19 March 1954
  |introduced = 1955
  |retired = <!--date the aircraft left military or revenue service. if vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly -->
  |primary user = [[Army Air Corps (United Kingdom)|Army Air Corps]]
  |more users = [[Royal Air Force]], [[Indian Air Force]]
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 182<ref name="Simp"/>
  |unit cost =
  |developed from =
  |variants with their own articles =
}}
|}

The '''Auster AOP.9''' was a [[United Kingdom|British]] military air observation aircraft ("[[Air Observation Post]]") produced by [[Auster|Auster Aircraft Limited]] to replace the [[Auster AOP.6]].

==Design and development==
The Auster AOP.9 was designed as a successor to the [[Auster AOP.6]]. Like its predecessor, it was a braced high-wing single engined monoplane with a fixed tailwheel undercarriage.<ref name ="JAWA1956">{{Harvnb|Bridgman|1956|pages=46–7}}</ref> Although having the same general appearance, the AOP.9 was a new design, with larger wing area and a more powerful engine. The wing and tail were metal-skinned, but the fuselage and ailerons were fabric-covered.<ref name="JAWA1956"/>  The fin and rudder assembly were more angular in the new aircraft with a noticeable dorsal fillet.<ref>{{Harvnb|Thetford|1957|pages= 37 and 39}}</ref> A combination of the more powerful 180&nbsp;hp (134&nbsp;kW) [[Blackburn Cirrus Bombardier]] engine, larger wings and large flaps gave it an improved take-off and landing performance compared with the AOP.6. It could operate from ploughed fields and muddy surfaces using low pressure tyres and strengthened undercarriage.<ref name="Thet">{{Harvnb|Thetford|1957|page=38}}</ref>

The cabin held three seats, pilot and passenger side-by-side and the observer behind, facing either forwards or rearwards.<ref name="JAWA1956"/> The aircraft was also designed to be convertible into a two-seat light transport with an interchangeable rear floor.<ref name="Thet"/>  In this configuration the observer sat alongside the pilot.

The prototype ''WZ662'' first flew 19 March 1954.<ref name="JAWA1956"/> Auster Aircraft allotted its model designation '''B5''' to the AOP.9 design.<ref name="Ketley, 2005, p. 83">Ketley, 2005, p. 83</ref>

==Operational history==
Deliveries started to the [[Royal Air Force]] in February 1955,<ref name="JAWA1956"/> replacing AOP.6s in the regular AOP squadrons, the auxiliary squadrons disbanding in March 1957 before receiving AOP.9s. Until the formation of the [[Army Air Corps (United Kingdom)|Army Air Corps]] (AAC) in September 1957, Army personnel flew RAF aircraft based in RAF squadrons.

The aircraft were in action with [[No. 656 Squadron RAF|No. 656 Squadron]] from September 1955,<ref>Halley, 1988, p. 447</ref> flying an average of 1,200 sorties per month.<ref>Ketley, 2005, p. 50</ref> By the end of [[Operation Firedog]] in [[Federation of Malaya|Malaya]] on 31 July 1960, 656 Squadron's AOP.6 and AOP.9s had carried out 143,000 sorties.<ref>Thetford, 1976, p.42</ref>

The AOP.9s were involved in several of Britain's other end of Empire conflicts; [[No. 653 Squadron AAC|653 Squadron AAC]] used them in Aden in the early 1960s, flying from Falaise, Little Aden.<ref name="MAF">[http://www.flying-museum.org.uk/the_army_air_corps_today.htm Museum of Army Flying]</ref><ref name="Aden">[http://www.adenairways.com/AAC.htm AOP.9 in Aden]</ref> They stayed in service until 1966 and were the last fixed wing AOP aircraft used by the AAC,<ref name="MAF"/> though their light transport role was taken over by [[de Havilland Canada DHC-2 Beaver|Beavers]].

[[File:AOP9-G-bdfh.jpg|thumb|right|Formerly XR240, this aircraft now (2008) flies as ''G-BDFH'']]

The South African Air Force operated its AOP.9s from 1957 to 1967.

The Army Historic Aircraft Flight maintain an AOP.9<ref>[http://www.army.mod.uk/7354.aspx British Army AOP.9] {{webarchive |url=https://web.archive.org/web/20110605031314/http://www.army.mod.uk/7354.aspx |date=June 5, 2011 }}</ref> in flying condition at [[Middle Wallop]].

In the 1970s, 19 AOP.9s joined the UK civil register, and in 2008 14 remained, though only about three of these had a current certificate of airworthiness.<ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=summary&aircrafttype=Auster%20AOP.9 UK Civil Aviation Authority Aircraft Register Auster AOP.9]</ref> The sole Beagle E3/Auster
AOP.11 ''G-ASCC'' was flying<ref name="CAA">[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=ASCC UK Civil Aviation Authority Aircraft Register G-ASCC]</ref> until an accident in 2007.<ref>[http://www.aaib.gov.uk/publications/bulletins/october_2007/beagle_aircraft_e3__auster_aop_ii___g_ascc.cfm  G-ASCC crash]</ref>

==Variants==
;Auster AOP.9
:Only production version, 182 built.<ref name="Simp"/>
[[File:Auster AOP.11 XP254 Farnborough 09.61.jpg|thumb|right|The Auster AOP.11 exhibited at the [[Farnborough Air Show]] in September 1961]]
;Auster AOP.11
:Three-seat AOP machine with a 260 hp [[Continental O-470|Continental IO-470-D]] 6-cylinder horizontally opposed more powerful engine, that raised the maximum speed to 142 mph (228 km/h) and the empty weight to 1,806 lb (816 kg).<ref name="JAWA66"/> Apart from the engine, the AOP.11 was almost identical to its predecessor. Early in its career (photo, right), the undercarriage had spats, though these were later removed.<ref name ="JAWA66">{{Harvnb|Taylor|1966|pages=140–1}}</ref>  Only one, a converted AOP.9<ref name="Simp"/> was produced, making its first flight on 18 August 1961 with serial ''XP254''.<ref>{{Harvnb|Taylor|1966|page=140}}</ref> A year later it was registered to Beagle aircraft, that had taken over Auster in 1960, as ''G-ASCC'' where it was known as the Beagle Mk 11, the E.3 or as the A.115.<ref name="Simp">{{Harvnb|Simpson|2001|page=52}}</ref>  It was sold into private hands in 1971.<ref name="CAA"/>

;Auster 9M
:A number of army surplus aircraft were bought by Captain Mike Somerton-Rayner in 1967. One was converted as an Auster 9M with a 180 hp (134 kW) Avco [[Lycoming O-360]]-A1D piston engine.<ref name="Jackson1974">{{Harvnb|Jackson|1974|page=325}}</ref> The 9M first flew on 4 January 1968, and gained a Certificate of Airworthiness on 30 April 1968<ref name="Jackson1974" /> The aircraft was still airworthy in 2009.<ref>[http://www.caa.co.uk/docs/HistoricalMaterial/G-AVHT.pdf UK Civil Aviation Authority Aircraft Register G-AVHT (historic)] {{webarchive |url=https://web.archive.org/web/20110606160132/http://www.caa.co.uk/docs/HistoricalMaterial/G-AVHT.pdf |date=June 6, 2011 }}</ref><ref>[http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=AVHT UK Civil Aviation Authority Aircraft Register G-AVHT (current)]</ref>

[[File:auster aop9 xr246 g-azbu arp.jpg|thumb|Privately owned 1961-built AOP.9 ''G-AZBU'' takes off in 2009]]

==Operators==

===Military operators===
;{{flag|Hong Kong|colonial}}
*[[Royal Hong Kong Auxiliary Air Force]] about 4 ex-British AAC aircraft
*various ACC AOP and Independent Flights stationed in [[Hong Kong]] in the 1950s and 1960s
;{{IND}}: About 35 aircraft
*[[Indian Air Force]]
*[[Indian Army]]
;{{flag|South Africa|1928}}:
*[[South African Air Force]] about 2 aircraft
;{{UK}}: about 145 aircraft
*[[Army Air Corps (United Kingdom)|Army Air Corps]]<ref name="Ketley, 2005, p. 83"/>
**[[No. 653 Squadron AAC|653 Squadron]]<ref name="MAF"/>
**[[No. 656 Squadron AAC|656 Squadron]]
**Advanced Fixed Wing Flight
**Army Flights: 2, 3, 5, 6, 7, 8, 10, 11, 12, 13, 14, 16, 18, 20, 21.
**Various Army Regiments
*[[Royal Air Force]]<ref>Halley, 2001, p. 51</ref>
**[[No. 651 Squadron RAF|651 Squadron]]
**[[No. 652 Squadron RAF|652 Squadron]] (1956–1957) <ref name="Jefford102">Jefford 1988, p. 102</ref>
**[[No. 656 Squadron RAF|656 Squadron]] (1955–1957) <ref name="Jefford103">Jefford 1988, p. 103</ref>
**[[No. 657 Squadron RAF|657 Squadron]]
**Light Liaison Flight, South Korea
**Christmas Island Flight, 160 Wing
**1900 Flight (Hong Kong)
**38 Group Communications Flight [[RAF Upavon|Upavon]]
**Light Aircraft School [[RAF Middle Wallop|Middle Wallop]]

==Specifications (AOP.9)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref= <ref name="JAWA1956"/>
|crew=2/3
|length main= 23 ft 8½ in
|length alt=  7.24 m
|span main= 36 ft 5 in
|span alt= 11.10 m
|height main= 8 ft 11 in
|height alt= 2.72 m
|area main= 197.6 ft²
|area alt= 18.36 m²
|empty weight main= 1,460 lb
|empty weight alt=  663 kg
|loaded weight main= 2,100
|loaded weight alt= 953 kg
|max takeoff weight main= 2,330 lb
|max takeoff weight alt= 1,057 kg
|engine (prop)=[[Blackburn Cirrus Bombardier|Blackburn Cirrus Bombardier 203]]
|type of prop= 4-cylinder inverted inline piston
|number of props=1
|power main= 173 hp
|power alt= 129 kw
|max speed main= 127 mph
|max speed alt= 204 km/h
|cruise speed main=110 mph
|cruise speed alt= 178 km/h
|range main= 242 miles
|range alt= 389 km
|ceiling main= 18,500 ft
|ceiling alt= 5640 m
|climb rate main= 920 ft/min
|climb rate alt= 280 m/min
|loading main=10.4 lb/ft<sup>2</sup>
|loading alt=50.4 kg/m<sup>2</sup>
|power/mass main= 11.9 lb/hp
|power/mass alt= 7.25 kg/kW
|armament=
}}

==See also==
{{Portal|Aviation}}
{{aircontent
|related=
|similar aircraft=
|sequence=
|lists=
*[[List of aircraft of the Royal Air Force]]
*[[List of aircraft of the Army Air Corps]]
|see also=
}}

==References==

===Notes===
{{reflist|colwidth=20em}}

===Bibliography===
{{refbegin}}
*{{cite book |last=Bridgman |first=Leonard |title=Jane's All the World's Aircraft 1956-7 |year=1956 |publisher=Jane's All the World's Publishing Co. Ltd |ref=harv}}
*{{cite book |last=Halley |first=J.J. |title=The Squadrons of the Royal Air Force 1918-1988 |year=1988 |publisher=Air-Britain (Historians) Ltd |isbn=0-85130-164-9}}
*{{cite book |last=Halley |first=J.J. |title=Royal Air Force Aircraft |year=2001 |publisher=Air-Britain (Historians) Ltd |isbn=0-85130-311-0}}
*{{cite book |last= Jackson |first= A.J. |title= British Civil Aircraft since 1919 Volume 1 |year= 1974 |publisher= Putnam |location= London |isbn= 0-370-10006-9 |ref= harv}}
*{{cite book |last= Jefford |first= C.G. |title= RAF Squadrons |year=1988 |publisher= Airlife Publishing Ltd |isbn= 1-85310-053-6}}
*{{cite book |last=Ketley |first=Barry |title=Auster - A brief history of the Auster aircraft in British military service |year=2005 |publisher=Flight Recorder Publications |isbn=0-9545605-6-6}}
*{{cite book |last=Simpson |first=Rod |title=Airlife's World Aircraft |year=2001 |publisher=Airlife Publishing Ltd |location=Shrewsbury |isbn=1-84037-115-3 |ref=harv}}
*{{cite book |last=Taylor |first=J.W.R. |title=Jane's All the World's Aircraft 1966-7 |year=1966 |location=Great Missenden |publisher= Sampson Low, Marsden & Co. Ltd. |ref=harv}}
*{{cite book |last= Thetford |first=Owen |title= Aircraft of the Royal Air Force 1919-57|year=1957|publisher= Putnam|location= London |ref=harv}}
*{{cite book |last=Thetford |first=Owen |title=Aircraft of the Royal Air Force since 1918 |year=1976 |publisher=Putnam & Company Ltd |isbn=0-370-10056-5}}
*{{cite book |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985) |publisher= Orbis Publishing}}
{{refend}}

==External links==
{{commons category-inline|Auster AOP.9}}

{{Auster aircraft}}

{{DEFAULTSORT:Auster Aop.9}}
[[Category:High-wing aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:British military reconnaissance aircraft 1950–1959]]
[[Category:Auster aircraft|A.O.P.9]]