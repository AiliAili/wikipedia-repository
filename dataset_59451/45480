{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = William Edward George Mann
| image         =
| caption       =
| birth_date    = {{Birth date|1899|04|20|df=yes}}
| death_date    = {{Death date and age|1966|05|04|1899|04|20|df=yes}}
| placeofburial_label = 
| placeofburial = 
| birth_place  = [[Brentford]], England
| death_place  =
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      = "Pedro" 
| allegiance    = {{flag|United Kingdom}}
| branch        = {{air force|United Kingdom}}
| serviceyears  = 1917&ndash;1945
| rank          = [[Air Commodore]]
| unit          = [[No. 208 Squadron RAF]]<br/>[[No. 25 Squadron RAF]]<br/>[[No. 6 Squadron RAF]]
| commands      = [[RAF Ismailia]]
| battles       = [[First World War]]<br/>[[Second World War]]
| awards        = [[Companion of the Order of the Bath]]<br/>[[Commander of the Order of the British Empire]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]<br/>[[Mentioned in Despatches]] (2)<br/>[[Legion of Merit]] (United States)
| relations     =
| laterwork     = 
}}

[[Air Commodore]] '''William Edward George Mann''' [[Order of the Bath|CB]], [[Order of the British Empire|CBE]], [[Distinguished Flying Cross (United Kingdom)|DFC]] (20 April 1899 &ndash; 4 May 1966) was a senior officer in the [[Royal Air Force]] and a [[flying ace]] of the [[First World War]] credited with thirteen confirmed aerial victories.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/england/mann.php |title=William Edward George Mann |work=The Aerodrome |year=2015 |accessdate=27 May 2015 }}</ref> In later years, he specialized in signals and communications work, and was instrumental in developing mobile radars and signal units for the RAF in the [[Second World War]].<ref name="rafweb">{{cite web |url= http://www.rafweb.org/Biographies/Mann.htm |title=William Edward George Mann |first=M. B.  |last=Barrass |work=Air of Authority - A History of RAF Organisation |year=2015 |accessdate=27 May 2015}}</ref>

==First World War==
Mann began his military career as a [[Sopwith Camel]] pilot in the [[Royal Naval Air Service]] in 1917. It took him several months before he was successful, but from 8 May through 26 September 1918, he scored thirteen aerial victories while with 208 Squadron (formerly 8 Naval). His final tally was six German planes destroyed (including two shared victories), and seven more driven down out of control.<ref name="theaerodrome"/><ref name="ATT">Shores ''et.al.'' (1990), p.254.</ref>

==Interbellum==
Mann spent a period of unemployment for some months as the new Royal Air Force downsized and reorganized. He spent 1920 in various instructor training courses. In 1921, he participated in the second [[Hendon Air Pageant]]; he also served on the [[Central Flying School]]'s five man aerobatic team flying [[Sopwith Snipe]]s along with [[Arthur Coningham (RAF officer)|Arthur Coningham]]. Mann would return to this team in 1924. They were the first to fly an inverted formation at Hendon.<ref name="rafweb"/>

Beginning 10 January 1926, he attended Electrical and Wireless School. Signals would become his specialty for the remainder of his career. He attended the [[RAF Staff College, Andover]], beginning the course on 21 January 1936, before shipping out to the [[Middle East]].<ref name="rafweb"/>

==Second World War==
Mann continued to serve in the Mid East and Mediterranean; he helped develop mobile radar and signals units that served as models for the entire RAF. He served through the war, retiring on 18 April 1945.<ref name="rafweb"/>

==Later life==
Mann's expertise in signals took him into civil service on familiar ground. He became the Civil Aviation Signals representative in Cairo, starting in 1945. He moved on to become the Director of Telecommunications of the [[Ministry of Civil Aviation (United Kingdom)|Ministry of Civil Aviation]] from 1948 to 1950. He then became [[Director-General]] of Civil Aviation Navigational Services until his second retirement in 1959. He would spend the next two years representing the [[Decca Navigator System|Decca Navigator Company]] before returning to England.<ref name="rafweb"/>

Mann died on 4 May 1966.<ref name="rafweb"/>

==Promotions in rank==
* Temporary [[Sub-Lieutenant]]: 12 August 1917
* Second lieutenant: 1 April 1918
* Lieutenant: Unknown date (seniority from 1 April 1918)
* Temporary Captain and [[Flight commander]]: 27 August 1918
* [[Flying officer]]: 1 August 1919 (seniority of 1 April 1918)
&ndash;Transferred to Unemployed List on 27 August 1919&ndash;
* Flying officer: 2 January 1920 (seniority of 1 April 1918)
* [[Flight lieutenant]]: 1 July 1924
* [[Squadron leader]]: 1 December 1934
* [[Wing Commander (rank)|Wing Commander]]: 1 July 1938
* Temporary [[Group Captain]]: 1 December 1940
* Temporary [[Air Commodore]]: 1 December 1943<ref name="rafweb"/>

==Honours and awards==
:[[Companion of the Order of the Bath]]: 2 January 1956
:[[Commander of the Order of the British Empire]]: 1942
:[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]]: 3 December 1918
:[[Mentioned in Dispatches]]: 14 January 1944; 24 September 1941
:[[Legion of Merit|Officer of the Legion of Merit]]: 10 March 1944
:[[Institution of Electrical Engineers|Member of the Institution of Electrical Engineers]]

==References==
{{reflist}}

==Bibliography==
* {{cite book |first1=Christopher F. |last1=Shores |first2=Norman |last2=Franks |authorlink2=Norman Franks |first3=Russell F. |last3=Guest |title=Above the Trenches: a Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920 |location=London, UK |publisher=Grub Street |year=1990 |isbn=978-0-948817-19-9 |lastauthoramp=yes}}

{{DEFAULTSORT:Mann, William}}
[[Category:1899 births]]
[[Category:1966 deaths]]
[[Category:British World War I flying aces]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Companions of the Order of the Bath]]
[[Category:Recipients of the Distinguished Flying Cross (United Kingdom)]]
[[Category:Officers of the Legion of Merit]]
[[Category:Royal Air Force officers]]
[[Category:Royal Air Force personnel of World War I]]
[[Category:Royal Air Force personnel of World War II]]