{{featured article}}
{{About|the British film|the Indian film|David Uncle}}

{{Use British English|date=April 2013}}
{{Use dmy dates|date=April 2013}}
{{Infobox film
| name           = Uncle David
| image          = Uncle David Poster.jpg
| alt            = 
| caption        = Poster advertising film's premiere at the British Film Institute.
| director       = [[David Hoyle (performance artist)|David Hoyle]]<br>Gary Reich<br>Mike Nicholls
| producer       = Gary Reich
| starring       = David Hoyle<br>Ashley Ryder
| music          = [[Richard Thomas (musician)|Richard Thomas]]<br>Jon Opstad
| released       = {{Film date|df=yes|2010|3|25|London Lesbian and Gay Film Festival}}
| runtime        = 95 minutes
| country        = United Kingdom
| budget         = £4000<ref name="Willis 2011"/>
| gross          = 
}}
'''''Uncle David''''' is a 2010 British [[black comedy]] film directed by [[David Hoyle (performance artist)|David Hoyle]], Gary Reich, and Mike Nicholls. It was produced by Reich and stars Hoyle, an English [[performance art]]ist, in the titular role alongside English pornographic actor Ashley Ryder. Developed [[artist collective|collectively]] under the banner of the Avant-Garde Alliance, it was filmed in October 2009. Created without a script, every scene was improvised and filmed in a single [[take]].

Set in a [[trailer park|caravan park]] on the [[Isle of Sheppey]] in [[Kent]], [[South East England]], a young man with a childlike mind named Ashley (Ryder) arrives to stay with his Uncle David (Hoyle). Escaping from his abusive mother, Ashley enters into a sexual relationship with his uncle who offers his insights into the world and the nature of reality. Eventually Ashley tells David that he wants to die, and David agrees to carry out the killing.

The film premiered on 25 March 2010 at the [[London Lesbian and Gay Film Festival]] held in the [[BFI Southbank]] in central London. Reviews were mixed, but the film won several awards at international film festivals. It was released on [[DVD]] by [[Peccadillo Pictures]] in 2011.

==Plot==
Late one night, Ashley (Ashley Ryder)&nbsp;– a young man with a childlike mentality&nbsp;– arrives at the [[Mobile home|caravan home]] of his Uncle David ([[David Hoyle (performance artist)|David Hoyle]]) on the [[Isle of Sheppey]]. Ashley confides in his uncle that his mother has been physically abusing him by stubbing cigarettes out on his hand. David lambasts the mother and society in general; the two embrace in a kiss before going to bed together. The next morning, they awake and have breakfast in bed before David bathes Ashley in the bathtub, telling him a story about an unhappy [[nuclear family]]. Going to a local café and then a park bench, David continues to explain his anti-establishment opinions about society. Ashley reveals that he wants to die, and the two express their love for one another.

The pair snort [[cocaine]] before playing in the local entertainment arcade. Returning to the caravan, they engage in [[Erotic asphyxiation|sexual strangulation]] and [[anal sex]]. Putting on make-up, they then drink wine and dance to a remix of the [[Muse (band)|Muse]] song "[[Uprising (song)|Uprising]]", while same-sex pornography starring Ashley plays on the television set. While Ashley puts on [[Drag (clothing)|drag]], David encourages him to kill himself, telling him that he is "too nice for this Earth" and needs to escape the world's problems.

The next morning, Ashley plays on the beach and builds [[sand castles]]. When Ashley's drunken mother phones, demanding to know where her son is, Uncle David feigns ignorance, telling her that he himself is not in Sheppey but in [[Manchester]]. Taking Ashley to an abandoned military bunker, he injects an unknown substance into his nephew's vein as they kiss. Ashley enters an [[altered state of consciousness]], and David guides him back to the caravan as they smoke [[marijuana]]. Once there, David gives Ashley a second injection, as they sit and look out at the shore. After dark, David places his near-catatonic nephew in a dog bed, showing him a copy of a picture book titled ''Is Britain Great?''. He proceeds to give him a third and final injection, which he remarks will make Ashley lose consciousness and suffer organ failure. As Ashley dies, David professes that he has never loved anyone else as much before. The next morning, he goes onto the beach to place his nephew's body in a shallow grave, tearfully kissing the body goodbye before it is swept away by the sea.

==Production==

===Background===
[[File:David Hoyle 2012.jpg|thumb|right|upright|Hoyle performing at the Royal Vauxhall Tavern]]
According to ''[[The Guardian]]'', Hoyle became "something of a legend" on the British cabaret circuit during the 1990s, initially under the alter-ego of "The Divine David".<ref name="Walters 2010">[[#Wal10|Walters 2010]].</ref> The Divine David was an "anti-[[drag queen]]" who combined "lacerating social commentary" with "breathtaking instances of self-recrimination and even self-harm."<ref name="Walters 2010"/> Taking this character to television, Hoyle produced two shows for [[Channel 4]], ''The Divine David Presents'' (1999) and ''The Divine David Heals'' (2000), before killing off the character at a farewell show at [[Streatham Ice Arena]], [[South London]], titled ''The Divine David on Ice'' (2000). Independently, Hoyle appeared in the film ''[[Velvet Goldmine]]'' (1998) and the television series ''[[Nathan Barley]]'' (2005).<ref name="Walters 2010"/>

Ryder had hoped to be a fashion designer, undertaking a course at [[Central Saint Martins]] which he did not complete; from there he worked in retail, first for Prowler and then for the designer [[Vivienne Westwood]]. Subsequently appearing in gay pornography as a [[twink (gay slang)|twink]] [[Top, bottom and versatile|bottom]] for production companies like [[Eurocreme]] and UK Naked Men, he starred in such films as ''Drunk on Spunk'' and ''Stretch that Hole'', winning numerous awards for his work.<ref name="Willis 2011"/><ref name="Gordon 2011"/> By the time he made ''Uncle David'', he was also running his own weekly gay wrestling night in London.<ref name="Matt 2011" />

Hoyle and Ryder met at a [[performance art]] event, after which the former asked Ryder if he would appear in one of his stage shows. Ryder was busy at the time, but agreed to the request several years later.<ref name="Gordon 2011"/> Staged at the [[Royal Vauxhall Tavern]] (RVT), a [[gay bar]] in [[Vauxhall]], South London, the duo constructed a piece in which Ryder played the role of Hoyle's nephew in an ''[[ad-lib]]'' performance.<ref name="Gordon 2011"/><ref name="Matt 2011" /> Hoyle played a department store [[Santa Claus]] while Ryder portrayed a misbehaving  nine-year-old. The performance ended in Hoyle's Santa character pulling Christmas [[tinsel]] from the child's anus before strangling him.<ref name="Walters 2010"/><ref name="Matt 2011" />

Reich&nbsp;– who had known Hoyle since the late 1990s&nbsp;– was in the audience, and enjoyed the chemistry between the two actors, suggesting that they appear in a feature film together.<ref name="Gordon 2011"/><ref name="Matt 2011" /> Over the course of a day, he filmed three shorts starring Hoyle and Ryder, in which they explored some of the scenarios and themes that made it into ''Uncle David''.<ref name="Matt 2011" /> In these shorts Hoyle wore a wig and makeup, which were omitted from the later feature.<ref name="Matt 2011" /> The two actors continued to develop their characters through a series of text messages and phone conversations.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=20:13–21:13}}

===Development===
''Uncle David'' was filmed over five days in October 2009 on the Isle of Sheppey,<ref name="Walters 2010"/> a location chosen for its proximity to London.<ref name="Urquhart 2011">[[#Urq11|Urquhart 2011]].</ref> Hoyle commented that the profusion of barbed wire fences there made him wonder if they were "keeping people out or keeping people in?".<ref name="Urquhart 2011"/> Reich said that he had been "obsessed" with Sheppey, a place he had considered "a truly Godforsaken wasteland" since working on a television show there ten years previously.<ref name="Matt 2011" /> He remarked that the "trailer trash setting" of the film was influenced by [[Harmony Korine]]'s ''[[Gummo]]'' (1997) and [[John Waters (director born 1946)|John Waters]]' ''[[Female Trouble]]'' (1974).<ref name="Matt 2011" /> He also said that the filming budget of under £1000 was a homage to "the spirit of John Waters."<ref name="Matt 2011" /> All the extras featured in the film were real inhabitants of Sheppey,<ref name="Matt 2011" /> and to film a scene in a local cafe, they walked in and asked the proprietors if they could shoot there.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=24:17–24:39}} Although they had planned to tell any enquirers who approached them that they were shooting a [[horror film]],{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=49:28–49:32}} during the shoot none of the locals expressed any interest in the crew's activities.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=11:33–12:16}}

[[File:Boy George Here and Now Tour 2011 452 v3.jpg|thumb|left|150px|Boy George contributed a song to the film.]]
The caravan park was largely empty at the time, and closed for winter several days after filming ended.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=2:00–2:30}} Both actors and crew slept in the same caravan in which the film was set.<ref name="Matt 2011" /><ref name="Urquhart 2011"/> Ryder later commented on the extreme cold and the crowded nature of the caravan, which had eight people inside it during filming; he felt that it was "quite hectic but [had] a great atmosphere".<ref name="Gordon 2011"/> Throughout filming, Hoyle and Ryder remained in character, improvising their scenes around a basic narrative structure rather than using a script.<ref name="Walters 2010"/><ref name="Urquhart 2011"/> Hoyle told a reporter that they "wanted everything to feel like real time... As soon as you woke up, the cameras would be on you. I look knackered."<ref name="Walters 2010"/> During filming, he contracted [[swine flu]], but insisted that all he needed was a 30-minute break before filming, attributing his ability to continue acting to [[adrenaline]]. He noted that "I just said, 'No, no, no. We'll carry on. I'll just stop eating.' I was completely purged during the making of that film. Afterwards, during the edit, I started to feel a lot calmer."<ref name="Walters 2010"/> Hoyle felt that the process of making the film had been cathartic, noting that "When I was 14, I had&nbsp;– let's call it an existential crisis. I wondered if [''Uncle David''] was a case of me destroying that part of me. If you do have a breakdown that young, for the rest of your life you're very aware of it. It can make you feel your foundations are a little bit shaky. It's a devastating experience but we revisited it. I just went with my instincts and intuition."<ref name="Walters 2010"/>

The footage was captured on two [[Sony HVR-Z1#Sony HVR-Z1|Sony HVR-Z1]] camcorders.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=22:53–23:10}} Costumes had been purchased at an [[Oxfam]] [[charity shop]] in [[Dalston]], [[East London]],{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=4:58–5:36}} while a wig worn by Ashley in one scene was purchased from a Dalston wig store.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=48:50–49:17}} Knowing that he would appear nude in several scenes, Ryder went to the gym regularly in the weeks proceeding filming to gain greater muscle tone.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=1:2:05–1:2:07}} He decided to include footage from one of his pornographic videos in ''Uncle David'', inspired to do so after learning that serial killers [[Fred West|Fred]] and [[Rosemary West]] screened pornography throughout the day in their home.{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=45:50–46:40}}

The composer [[Richard Thomas (musician)|Richard Thomas]], co-creator of the controversial ''[[Jerry Springer: The Opera]]'', agreed to produce a musical score for the film, having long been a fan of Hoyle's work.<ref name="Logan 2012">[[#Log12|Logan 2012]].</ref> After the cast and crew listened to a Muse album while driving between locations, they decided to include the band's work as they felt it was "empowering".{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=40:27–41:07}} They had also planned to include a song by [[Antony and the Johnsons]] in the film, but this was scrapped,{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=1:2:05–1:1:08}} to be replaced by a track by [[Boy George]]. After the film was screened for Boy George, the first person outside the Avant-Garde Alliance to see it, he commented "well it ain't ''[[The Sound of Music]]''".{{sfn|Hoyle|Ryder|Reich|Nicholls|2011|loc=1:30:59–1:31:56}}

==Release==
[[File:London National Film Theatre.jpg|thumb|right|The film premiered at the BFI Southbank (pictured).]]
''Uncle David'' premiered on 25 March 2010 at the [[London Lesbian and Gay Film Festival]] held in the [[BFI Southbank]] in central London.<ref name="Walters 2010"/> Attending the screening was Steve Marmion, the boss of [[Soho Theatre]], who suggested to Reich and Hoyle that they produce a theatrical adaptation of the film. They initially showed an interest in the project; Reich commented that "It was a very mischievous concept, doing an upbeat, life-affirming song-and-dance musical with that storyline and subject matter."<ref name="Logan 2012"/> However, the theatrical adaptation never surfaced, something Thomas attributed to the difficulty in obtaining funding for a musical on such a "dark subject."<ref name="Logan 2012"/> Instead, Hoyle and Thomas teamed up to produce ''Merrie Hell'', an "alternative Christmas celebration" that appeared at the Soho Theatre in December 2012.<ref name="Logan 2012"/> A two-man cabaret that differed every night, it featured Thomas on a piano, accompanying Hoyle's rendition of various songs about Christmas and Britain's societal problems; Thomas described it as "David Hoyle at Christmas - the Musical".<ref name="Logan 2012"/> One of the songs featured in ''Merrie Hell'', "It's Alright to Want to Die", carried a pro-suicide theme influenced by ''Uncle David''.<ref name="Logan 2012"/>

===Home media===
In December 2011, ''Uncle David'' was released as a [[DVD region code|Region Free]] DVD by the company [[Peccadillo Pictures]], a UK based film distributor of art house, gay and lesbian, independent and world cinema.<ref name="Walters 2011"/> The release contained several extras, including the three preparatory shorts detailing the relationship between the characters of Ashley and Uncle David and a cast commentary track voiced by Hoyle, Ryder, Reich and Nicholls.<ref name="Walters 2011"/> Reviewing the DVD release for the ''Sex Gore Mutants'' website, Stuart Willis noted that the disc contained periods of "murkiness or softness in the picture" and a few "(infrequent) instances of muffling", all of which he put down to the original production values of the film.<ref name="Willis 2011"/> British gay magazine ''[[Attitude (magazine)|Attitude]]'' celebrated the DVD's release with a competition to win one of three copies; they described the film as "a bit surreal, but totally amazing."<ref>[[#Att11|''Attitude'' 2011]].</ref>

==Reception==

===Awards===
The film received a number of awards at international film festivals.  At the 2010 Paris International LGBT Film Festival it won the best film award and Hoyle and Ryder shared the best actor award.<ref name="Gordon 2011">[[#Gor11|Gordon 2011]].</ref>  It also won best film at the 2011 Berlin Porn Film Festival,<ref>[[#Ber11|Berlin Porn Film Festival 2011]].</ref> and best film at the 2011 [[Erotic Awards]].<ref>[[#Ero11|Erotic Awards 2011]].</ref> Reich expressed surprise at the Erotic Award, because he did not consider the film to be a work of erotica, but discovered that they'd won because they had explored "an extreme fetish that to be honest we'd not actually heard of nor knew we were exploring."<ref name="Matt 2011">[[#Mat11|Matt 2011]].</ref>

===Reviews===
[[File:Uncle David screenshot.jpg|thumb|right|250px|Ashley playing on the beach; the distinctive muted [[landscape photography]] helped to create the atmosphere of the film.]]
Reviews of ''Uncle David'' were mixed. Writing in ''[[The Guardian]]'', Ben Walters noted that "suspense and dread accumulate as the low-key naturalism and the characters' obvious affection for one another play off against the enormity of what looms ahead." Comparing ''Uncle David'' to Hoyle's earlier stage work, he felt that the two were "consistent", but that in the film Hoyle abandoned the "fireworks of his performance persona", opting for a "controlled, beguiling style".<ref name="Walters 2010"/> He believed that Ryder's familiarity with being filmed meant that he carried a "disarmingly ingenuous presence, by turns determined and naive."<ref name="Walters 2010"/> Walters opined that the film's "menacing" use of British seaside locations was similar to that in ''[[Brighton Rock (1947 film)|Brighton Rock]]'' and ''[[London to Brighton]]'', while he also drew comparisons to ''[[Badlands (film)|Badlands]]'', ''[[Bonnie and Clyde (film)|Bonnie and Clyde]]'' and ''[[The Talented Mr. Ripley (film)|The Talented Mr. Ripley]]'', in that in all of them, the viewers' sympathy for the characters eclipses moral judgement of their actions.<ref name="Walters 2010"/> Walters would follow this with another review published in ''[[Time Out (magazine)|Time Out]]'', in which he awarded the film five stars out of five.<ref name="Walters 2011">[[#Wal11|Walters 2011]].</ref>

"Not to everyone's taste, granted, [''Uncle David''] is nevertheless an original and disturbing take on modern love stories" was the view of Stuart Willis writing for the ''Sex-Gore-Mutants'' website.<ref name="Willis 2011"/> Believing that the film was a "pleasant surprise", he praised the improvised nature of the work, believing that it created believable scenes and allowed the viewer to get to know the characters "almost without realising."<ref name="Willis 2011"/> He considered the acting to be very good, and particularly praised Hoyle's ability to portray a paedophile grooming a young man in a way that was both "pathetic and truly sinister all at once".<ref name="Willis 2011"/> Rather than falling into "tasteless exploitation", Willis thought that the film was saved by a lack of sensationalism.<ref name="Willis 2011"/> Praising the exterior scenes as "beautifully atmospheric" and the music as "fitting the laconic mood and sense of ill-foreboding well", he considered the film to be a good example of low-key filmmaking, akin to that produced in Britain during the early 1980s.<ref name="Willis 2011">[[#Wil11|Willis 2011]].</ref>

Other reviewers were far more critical. Writing for the ''So So Gay'' website, Damien Ryan gave ''Uncle David'' a rating of 0.5 out of 5, describing it as "terrible".<ref name="Ryan 2011"/> Believing that the film simply intended to shock viewers with its taboo subject matter, he felt offended by the story's "sheer inelegance".<ref name="Ryan 2011"/> Opining that it reminded him of "the very worst attempt of a first-year film student", he questioned the description of ''Uncle David'' as a "black comedy", arguing that it wasn't at all funny, instead labelling it "the most vapid, transparent attempt at shock this side of a [[Lady Gaga]] performance."<ref name="Ryan 2011"/> Criticising the acting, he said that Ryder "shouldn't quit his porn day job any time soon" and that Hoyle's character was simply a "marriage of wannabe [[Holden Caulfield]]-style outsider and mincing paedophile."<ref name="Ryan 2011">[[#Rya11|Ryan 2011]].</ref>

Cleaver Patterson was similarly critical in his review on the ''Cine-Vue'' website, awarding the film one out of five stars. He questioned the purpose of the film, arguing that it will only serve to alienate the gay population by portraying them as "sad, perverted and ultimately lonely individuals."<ref name="Patterson 2011"/> More positively, he praised the appearance of the film with its "muted colours of the shingle beaches and piercing blue skies".<ref name="Patterson 2011">[[#Pat11|Patterson 2011]].</ref>

==References==

===Footnotes===
{{reflist|30em}}

===Works cited===
{{refbegin|30em}}
* {{cite news
  | last1 = 
  | year=2011
  | title =Attention! Uncle David Comes to DVD
  | work = Attitude
  | url = http://www.attitude.co.uk/viewers/viewcontent.aspx?contentid=2158&catid=competitions&subcatid=comp&longtitle=ATTENTION!+UNCLE+DAVID+COMES+TO+DVD
  | accessdate = 13 January 2013
  | ref =Att11
  |archiveurl=http://www.webcitation.org/6E8ck1Dug
  |archivedate= 2 February 2013
  }}
* {{cite news
  | last1 = 
  | year=2011
  | title =And the Winners Are...
  | work = Berlin Porn Film Festival
  | url = http://www.pornfilmfestivalberlin.de/pffb_2011/en/?p=4682
  | accessdate = 24 December 2012
  | ref =Ber11
  |archiveurl=http://www.webcitation.org/6D9cYYiL7
  |archivedate=24 December 2012
  }}
* {{cite news
  | last1 = 
  | year=2011
  | title = Film: Feature
  | work = Erotic Awards
  | url = http://www.erotic-awards.co.uk/film.shtml
  | accessdate = 24 December 2012
  | ref =Ero11
  |archiveurl=http://www.webcitation.org/6D9brDE3U
  |archivedate=24 December 2012
  }}
* {{cite news
  | last1 = Gordon
  | date=29 November 2011
  | title = Everyone's Favourite Nephew
  | work = Bent
  | url = http://mag.bent.com/2011/11/everyone%E2%80%99s-favourite-nephew/
  | accessdate = 24 December 2012
  | ref =Gor11
  |archiveurl=http://www.webcitation.org/6D9ZsUqwP
  |archivedate=24 December 2012
  }}
*{{cite video
 | last1 =Hoyle
 | first1=David
 | last2=Ryder
 | first2=Ashley
 | last3=Reich
 | first3=Gary
 | last4=Nicholls
 | first4=Mike
 | year =2011
 | title =Cast & Crew Commentary
 | format =
 | medium =
 | publisher =Peccadillo Pictures
 | location =London
 | oclc =
 | ref =harv
}}
* {{cite news
  | last1 = Logan
  | first1 = Brian
  | date= 3 December 2012
  | title = Merrie Hell: David Hoyle and Richard Thomas do Christmas the Weird Way
  | work = The Guardian
  | url = https://www.theguardian.com/stage/2012/dec/03/merrie-hell-david-hoyle-richard-thomas
  | accessdate = 30 July 2013
  | ref =Log12
  |archiveurl = http://www.webcitation.org/6IWzSBzCs
  |archivedate = 31 July 2013
  }}
* {{cite news
  | last1 = Matt
  | date=10 May 2011
  | title =Say "Uncle" with Director Gary Reich
  | work = Honolulu Gay & Lesbian Cultural Foundation
  | url = http://www.hglcf.org/2011/05/say-uncle-with-director-gary-reich/
  | accessdate = 2 August 2012
  | ref =Mat11
  |archiveurl=http://www.webcitation.org/6MVQD9nuL
  |archivedate=9 January 2014
  }}
* {{cite news
  | last1 = Patterson
  | first1 = Cleaver
  | year=2011
  | title = DVD Review: 'Uncle David'
  | work = CineVue
  | url = http://www.cine-vue.com/2011/12/dvd-review-uncle-david.html
  | accessdate = 1 November 2012
  | ref =Pat11
  |archiveurl=http://www.webcitation.org/6BqwSzS81
  |archivedate=1 November 2012
  }}
* {{cite news
  | last1 = Ryan
  | first1 = Damien
  | date=27 July 2011
  | title = Film Review – Uncle David
  | work = So So Gay
  | url = http://sosogay.org/movie-review/film-review-uncle-david/
  | accessdate = 1 November 2012
  | ref =Rya11
  |archiveurl=http://www.webcitation.org/6Bqx5SujC
  |archivedate=1 November 2012
  }}
* {{cite news
  | last1 = Urquhart
  | first1 = Donald
  | date=December 2011
  | title = David Hoyle
  | work = Beige Magazine
  | url = http://www.beigeuk.com/2011/11/david-hoyle/
  | accessdate = 30 July 2013
  | ref =Urq11
  | archiveurl=http://www.webcitation.org/6IWztJd7R
  | archivedate=31 July 2013
  }}
* {{cite news
  | last1 = Walters
  | first1 = Ben
  | date=24 March 2010
  | title = Welcome Back David Hoyle: You're a Divine Director
  | work = The Guardian
  | url = https://www.theguardian.com/film/filmblog/2010/mar/24/david-hoyle-london-gay-lesbian-film
  | accessdate = 1 November 2012
  | ref =Wal10
  |archiveurl=http://www.webcitation.org/6Bqz15Opa
  |archivedate=1 November 2012
  }}
* {{cite web |url=http://www.timeout.com/film/reviews/91189/uncle_david.html |title=Uncle David |last=Walters, Ben |date=8–14 December 2011 |work=[[Time Out (company)|Time Out]] |publisher= |location=London |accessdate=8 July 2011 |ref=Wal11 |archiveurl=http://www.webcitation.org/6BqzS2YAp |archivedate=1 November 2012}}
* {{cite web |url=http://www.sexgoremutants.co.uk/reviews/uncldpecc.html |title=Uncle David |last=Willis, Stuart |year=2011 |work=Sex Gore Mutants |publisher= |location= |accessdate=9 April 2012 |ref=Wil11 |archiveurl=http://www.webcitation.org/6Br05tOzl |archivedate=1 November 2012}}
{{Refend}}

==External links==
* {{IMDb title|1625884}}

[[Category:2010 films]]
[[Category:2010s comedy-drama films]]
[[Category:British avant-garde and experimental films]]
[[Category:British comedy-drama films]]
[[Category:British independent films]]
[[Category:British LGBT-related films]]
[[Category:Films set in Kent]]
[[Category:Incest in film]]
[[Category:Isle of Sheppey]]
[[Category:LGBT-related comedy films]]
[[Category:LGBT-related drama films]]
[[Category:Films about suicide]]