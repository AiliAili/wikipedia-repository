{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
[[File:Phillip Glasier.jpg|thumb|right|250px|Glasier (right) with colleagues on a trip to the [[Middle East]] in 1963]]
'''Phillip Edward Brougham Glasier''' (22 Dec 1915 – 11 September 2000) was Britain's leading expert on hawking and [[falconry]]. Glasier initiated a new interest in falconry in both the UK and the United States of America. He spent much of his life involved with the conservation and breeding of [[Bird of prey|raptors]] and bringing them to public attention through the foundation of the Falconry Centre (now the [[International Centre for Birds of Prey]]), [[Newent]], Gloucestershire, as well as through books, lectures and public demonstrations. He founded the Hawk Trust (now the Hawk and Owl Trust) and the Raptor Breeders' Association.<ref name="telegraph">[http://www.telegraph.co.uk/news/obituaries/1355462/Phillip-Glasier.html  Daily Telegraph |]</ref><ref name="falcon">As The Falcon Her Bells, Phillip Glasier, Futura Publications Ltd., London. 1978, ISBN 0-7088-1331-3.</ref>

==Early life==
Glasier was born in [[Southfields]] in south-west [[London]], where his father was a land agent. Around 1920 the family moved to [[Kent]] and later to [[Suffolk]] where he spent his childhood. A great influence on his life at that time was a step-uncle, Captain Charles Knight, a respected ornithologist and falconer who encouraged the young Glasier's interest in nature and wildlife. Charles Knight lived a short distance from Glasier's home outside Sevenoaks.  As a child and teenager Glasier spent much time with his uncle learning about wildlife and in particular birds of prey. With his cousin, the actor [[Esmond Knight]], he took part in a number of amateur films made by Charles Knight. Such was his skill at handling birds that when his uncle went away on an expedition he left his young nephew in charge of an [[African hawk-eagle]].

On leaving school Glasier went to work in his father's land agency business.<ref name="falcon" />
<ref name="nyt">[https://www.nytimes.com/2000/09/23/us/phillip-glasier-who-made-falconry-modern-dies-at-84.html?n=Top%2fNews%2fScience%2fTopics%2fAnimals New York Times]</ref>

==Second World War==

During WWII Glasier was an instructor in the Royal Armoured Corp. He was stationed initially at [[Bovington Camp]] in Dorset, and while waiting for a commission he became an instructor for arms and then tanks. He finally went to the [[Royal Military Academy Sandhurst]] as a gunnery instructor. Glasier was demobilised in 1950 after 6 years' service.<ref name="falcon" />

==Middle years==

His father had died during his period of military service and he was unable to return to his former employment. He set up as a press photographer in London but found it unsatisfactory and moved to Salisbury, where he set up a photography shop specialising in bird photographs.<ref name="telegraph" /> At this time [[Peter Scott]] was starting the Severn Wildfowl Trust (now the [[Wildfowl and Wetlands Trust]]) and Glasier sent him a photograph of a [[mallard]], hoping that Scott might buy it. He not only bought it but commissioned Glasier to take more photographs at the Wildfowl Trust, something he did on annual visits.<ref name="falcon" />

In 1953 Glasier took a minor part as the 'Royal Falconer' in a film called ''[[The Sword and the Rose]]''. His role involved flying falcons to add authenticity to the drama.<ref name="falcon" /><ref name="imdb">[http://www.imdb.com/name/nm0321885/ IMDB]</ref> This film starred [[James Robertson Justice]] who was later to invite Glasier to be his personal falconer. He was also responsible for the flying of falcons for the film ''[[Knights of the Round Table (film)|Knights of the Round Table]]'' starring [[Ava Gardner]] and [[Robert Taylor (actor)|Robert Taylor]].<ref name="imdb" />

Shortly after this Glasier and his family moved to [[Black Isle]], [[Inverness]], Scotland to take up James Robertson Justice's offer of employment. Further falconry work was provided for an episode of a 1959 TV series called ''Three Golden Nobles''.<ref name="imdb" /> Later he returned to [[Melbury Osmond]] in Dorset, south-west England, returning annually to Scotland for the shooting season to fly his birds for, among others, [[Prince Philip]] and Charles, [[Prince of Wales]]. In 1963 he undertook an overland trek to the Middle East and India in search of Hodgson's hawk eagles (''Nisaetus nipalense''). The trip was undertaken with his friend Peter Combe in an ancient [[Austin Gypsy]].<ref name="hawk">A Hawk in the Hand, Phillip Glasier, Robinson Publishing, London, 1990, ISBN 1-85487-074-2</ref>

In 1963 Glasier wrote the autobiographical ''As The Falcon Her Bells''  (title from Shakespeare's ''As You Like It, act III, scene iii.'') recalling his early life and his influential uncle.
In 1965 Glasier went to the Royal Navy Air Station in [[Lossiemouth]], Scotland to demonstrate how trained raptors could be used to prevent bird strikes on planes by clearing the runways of seagulls and other birds. This was the first time that falcons had been used in this way.<ref name="hawk" />

==Later years==

In 1966 Glasier moved from Dorset to Gloucester, opening the Falconry Centre in May 1967<ref name="icbp">[http://icbp.org/about/history.html/ International Centre for Birds of Prey]</ref> His intention was that the public would have close access to raptors, witness their handling through flying displays and be more aware of their ecological value. At that time there was a general feeling that many raptors were destructive pests.<ref name="falcon" /> The centre grew and developed a successful breeding program of many birds that had not previously been bred in captivity.<ref name="nyt" />
In 1982 Glasier retired to Scotland to escape the newly passed [[Wildlife and Countryside Act 1981]]: he continued to fly his birds and also wrote another autobiographical book, '' A Hawk in the Hand''.
He later returned to [[Gorsley]] in Gloucestershire.

==Family==

Phillip Glasier's first marriage to Valerie Pedler, by whom he had two sons and a daughter, was dissolved. His second marriage was in 1949, to "Bill" Lees. She died in 1998. They had three daughters and a son, [[Jemima Parry-Jones|Jemima]], [[Anna Glasier|Anna]], Dinah and Nicholas.<ref>[http://www.icbp.org/index/index.php/about-6/history  International Centre for Birds of Prey – history]</ref>

==Bibliography==

* '' As the Falcon Her Bells'', Phillip Glasier, Heinemann, 1963. ISBN 1-4995-5778-7
* ''Guide to Falconry and Hawking'', Phillip Glasier, B.T. Batsford, London, 1986, ISBN 0-7134-5555-1
* ''A Hawk in the Hand'', Phillip Glasier, Robinson Publishing, London, 1990, ISBN 1-85487-074-2

==References==
{{Reflist}}

==External links==
* [http://icbp.org/ International Centre for Birds of Prey]
* [http://www.hawkandowl.org/  Hawk and Owl Trust]

{{DEFAULTSORT:Glasier, Phillip}}
[[Category:English conservationists]]
[[Category:Zookeepers]]
[[Category:English non-fiction writers]]
[[Category:1915 births]]
[[Category:2000 deaths]]
[[Category:Academics of the Royal Military Academy Sandhurst]]
[[Category:Falconry]]
[[Category:English male writers]]