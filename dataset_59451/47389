
{{Infobox company
| name             = Data Management Inc.
| logo             = [[File:TimeClock Plus full colorTM.png|200px]]
| caption          =
| type             = [[Privately held company|Private]]
| genre            = <!-- Only used with media and publishing companies -->
| fate             =
| predecessor      =
| successor        =
| foundation       = {{Start date|1988|09|12}}
| founder          = Jorge Ellis
| defunct          = <!-- {{End date|YYYY|MM|DD}} -->
| location_city    =
| location_country =
| location         = [[San Angelo, Texas|San Angelo]], [[Texas]], [[United States|USA]]
| locations        =
| area_served      = Worldwide
| key_people       =
| industry         = [[Computer software]]
[[Digital distribution]]
| products         =
| services         =
| revenue          =
| operating_income =
| net_income       =
| aum              = <!-- Only used with financial services companies -->
| assets           =
| equity           =
| owner            =
| num_employees    =
| parent           =
| divisions        =
| subsid           =
| traded_as        =
| homepage         = {{URL|http://www.timeclockplus.com}}
| footnotes        =
| intl             =
}}

[[File:DMI-bldg.jpg|thumb|260px|Data Management Inc.]]

'''Data Management Inc.''' (DMI) is an American corporation in [[San Angelo, Texas]]. It was founded by Jorge Ellis in 1988 and specializes in business software development, particularly for time and labor management. Its flagship product is [[TimeClock Plus]], a computerized [[time and attendance]] system.

The company originally developed the "One Number Delivery System" software for food delivery services.<ref>''San Angelo Standard Times'', August 7, 2008, Jayna Boyle, [http://www.gosanangelo.com/news/2008/aug/07/local-software-company-thriving "Local software company thriving"]</ref> Through his business with retailers, Ellis discovered a growing need to track time and attendance: "'No one could tell me at any given minute what the [labor] costs were. I decided to develop a software package that would give a 24-hour picture.'"<ref>''The Daily Record'', December 5, 1991, "TimeClock Plus Keeps Labor Costs in Synch"</ref> The system developed by DMI, called [[TimeClock Plus]], was written for companies as small as five employees or as large as 750 and larger.<ref>''Small Business News for Philadelphia/South Jersey'' (currently Philadelphia Business Journal), April 1997, "New software automates time, attendance"</ref>

In April 2011, the ''San Angelo Standard Times'' reported that about 50,000 companies were using the system including [[Boeing]], [[Harley Davidson]], [[Ford Motor Company]], [[Dial Corporation|Dial]], and [[Sara Lee Corporation|Sara Lee]].<ref name="gosanangelo">''San Angelo Standard Times'', April 23, 2011, Justin Zamudio, [http://www.gosanangelo.com/news/2011/apr/23/time-is-right-for-san-angelo-company "Time is right for San Angelo company"].</ref>

==Awards==

Data Management Inc. was listed among the Deloitte Technology Fast 500 in 2004, 2005, and 2006.<ref>[http://www.deloitte.com/assets/Dcom-UnitedStates/Local%20Assets/Documents/TMT_us_tmt/us_tmt_2006TechnologyFast500WinnersBrochure.pdf Deloitte Technology Fast 500 list, 2006]</ref>

DMI was also selected by CIO Review for the 50 most promising education technology solution providers in 2015.<ref>[http://www.streetinsider.com/Press+Releases/TimeClock+Plus+has+been+Selected+by+CIO+Review+for+the+50+most+Promising+Education+Technology+Solution+Providers+2015/11087420.html Street Insider]. November 18, 2015, "TimeClock Plus has been Selected by CIO Review for the 50 most Promising Education Technology Solution Providers 2015"</ref>

== References ==
{{reflist}}

[[Category:Companies based in San Angelo, Texas]]
[[Category:Business software companies]]