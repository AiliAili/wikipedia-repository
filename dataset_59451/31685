{{bots|deny=Citation bot}}{{Use mdy dates|date=December 2015}}
{{Infobox road
|state=MI
|type=I
|route=496
|alternate_name=R.E. Olds Freeway
|map_notes=Lansing area with I-496 highlighted in red
|map_alt= I-496 connects downtown Lansing with the I-96 and US 127 freeways
|spur_type=I
|spur_of=96
|history=Initial section opened in December 1963,<ref name=IDG63/> completed on December 18, 1970<ref name=paving/>
|length_mi= 11.907
|length_ref=<ref name=PRFA/>
|direction_a=West
|terminus_a={{Jct|state=MI|I|69|I|96}} in [[Delta Charter Township, Michigan|Delta Township]]
|junction={{jct|state=MI|Capitol Loop||M|99}} in [[Lansing, Michigan|Lansing]]<br/>
{{Jct|state=MI|US|127}} near [[East Lansing, Michigan|East Lansing]]
|direction_b=East
|terminus_b={{Jct|state=MI|I|96|US|127}} in [[Delhi Charter Township, Michigan|Delhi Township]]
|counties=[[Eaton County, Michigan|Eaton]], [[Ingham County, Michigan|Ingham]]
|previous_type=I
|previous_route=475
|next_type=Capitol Loop
|next_route=496
}}
'''Interstate 496''' ('''I-496''') is an [[List of auxiliary Interstate Highways|auxiliary Interstate Highway]] that passes through downtown [[Lansing, Michigan|Lansing]] in the US state of Michigan. Also a component of the [[Michigan State Trunkline Highway System|State Trunkline Highway System]], the freeway connects [[Interstate 96|I-96]] to the downtown area. It has been named the '''R.E. Olds Freeway''' (sometimes just '''Olds Freeway''') for [[Ransom E. Olds]], the founder of [[Oldsmobile]] and the [[REO Motor Car Company]]. I-496 runs east–west from I-96/[[Interstate 69 in Michigan|I-69]] near the downtown area and north–south along a section that [[concurrency (road)|runs concurrently]] with [[U.S. Route 127 in Michigan|US Highway&nbsp;127]] (US&nbsp;127). The trunkline also passes a former assembly plant used by Oldsmobile and runs along or crosses parts of the [[Grand River (Michigan)|Grand]] and [[Red Cedar River (Michigan)|Red Cedar]] rivers.

Construction of I-496 started in 1963, and the freeway opened on December 18, 1970. Segments of the freeway south of [[downtown Lansing]] were built in the location of a historically black neighborhood. This neighborhood was formed based on the segregationist practices of the early 20th century. Community leaders worked for different housing opportunities for the black residents displaced by I-496 rather than fight the freeway. As the trunkline neared completion, competing proposals to name it resulted in two similar, but separate designations applied to I-496. The city originally approved one name in honor of a former mayor. The local historical society proposed that the state name it as a memorial to Olds after the demolition of the Olds Mansion. The city renamed it the '''Oldsmobile Expressway''', the name under which it opened in December 1970. Two years later, the [[Michigan Legislature]] restored its preferred name and it has been the Olds Freeway since.

==Route description==
I-496 starts at an interchange with I-96/[[Interstate 69 in Michigan|I-69]] at that freeway's exit&nbsp;95 in [[Delta Charter Township, Michigan|Delta Township]] in [[Eaton County, Michigan|Eaton County]]. The freeway runs eastward through suburban areas of the township adjacent to some residential subdivisions. Continuing eastward, there is an interchange for Creyts Road before I-496 angles to the northeast. At the interchange with Waverly Road, I-496 crosses into [[Ingham County, Michigan|Ingham County]]. The freeway then runs parallel to the [[Grand River (Michigan)|Grand River]]. Near a partial interchange with Lansing Road (old [[U.S. Route 27 in Michigan|US&nbsp;27]]<ref name=MSHD63/>), the freeway gains a pair of [[service drive]]s: St. Joseph Street runs [[one-way traffic|one-way]] westbound on the north side, and Malcolm X Street runs eastbound to the south. The next interchange is for the connection to the [[Capitol Loop]] and [[M-99 (Michigan highway)|M-99]], both of which run along [[List of streets named after Martin Luther King, Jr.#Lansing, Michigan|Martin Luther King Boulevard]]. The Capitol Loop,<ref name=MDOT12/><ref name=google/> also internally numbered Connector 496,<ref name=MGF8C>{{cite web|author=[[Michigan Department of Information Technology]] |url=http://www.michigan.gov/documents/Appendix_C_Connector_19295_7.pdf |format=PDF |title=Appendix C: State Trunkline Connector Routes |date=May 1, 2008 |access-date=October 15, 2008 |work=Michigan Geographic Framework |publisher=Michigan Department of Information Technology |archive-url=http://www.webcitation.org/5va0tTuj2?url=http://www.michigan.gov/documents/Appendix_C_Connector_19295_7.pdf |archive-date=January 8, 2011 |dead-url=no |df=mdy-all }}</ref> is a signed connector that provides access to various state government buildings like the [[Michigan State Capitol]]. South of this interchange, M-99 connects to the [[Lansing Car Assembly]] plant,<ref name=MDOT12/><ref name=google/> a former facility for [[Oldsmobile]].<ref name=wieland>{{cite news |last= Wieland |first= Barbara |date= May 1, 2005 |title= Lansing Car Assembly: 1901–2005 |work= Lansing State Journal |page= A1 |issn= 0274-9742 |oclc= 6678181 |url= http://pqasb.pqarchiver.com/lansingstatejournal/access/1787056071.html?FMT=ABS&FMTS=ABS:FT&type=current&date=May+01%2C+2005&author=Wieland+Barbara&pub=Lansing+State+Journal&desc=Lansing+Car+Assembly+%22+1901-2005 |access-date= July 12, 2012 |via= ProQuest Archiver |subscription= yes}}</ref>

[[File:I-496 at MLK Boulevard.jpg|thumb|left|upright|Looking east from the Martin Luther King, Jr. Boulevard overpass|alt=Photograph of]]
Continuing eastward, I-496 passes north of the assembly plant complex and south of the central business district. East of a partial interchange with Walnut Street, the freeway passes the [[Michigan Women's Hall of Fame]], which is located on I-496's southern service drive. The south side of the freeway is adjacent to Cooley Gardens near the confluence of the Grand and [[Red Cedar River (Michigan)|Red Cedar]] rivers. I-496 crosses the Grand River downstream from the confluence and meets the eastern terminus of the Capitol Loop. This interchange with Cedar and Larch streets is also a connection to [[Interstate 96 Business (Lansing, Michigan)|Business Loop I-96]] (BL I-96) and Pennsylvania Avenue. St. Joseph Street ends after the connection to Pennsylvania Avenue.<ref name=MDOT12/><ref name=google/> The main freeway crosses a rail line owned by [[CSX Transportation]].<ref name=MDOT-RR>{{cite map |author= Michigan Department of Transportation |url= http://www.michigan.gov/documents/MDOT_Official_Rail_130897_7.pdf |title= Michigan's Railroad System |scale= Scale not given |location= Lansing |publisher= Michigan Department of Transportation |date= January 2011 |access-date= February 1, 2011 |format= PDF}}</ref> I-496 runs parallel to the north side of the rail line while Malcolm X Street follows to the south as far as the Clemens Avenue overpass. The freeway then crosses into East Lansing near the Red Cedar Natural Area.<ref name=MDOT12/><ref name=google/>

After crossing the city line, I-496 turns southward and merges with US&nbsp;127. The two highways run concurrently,<ref name=MDOT12/><ref name=google/> and they cross a line of the [[Canadian National Railway]].<ref name=MDOT-RR/> The freeway runs along the western edge of the campus of [[Michigan State University]]. South of campus, I-496/US&nbsp;127 crosses back into Lansing and has an interchange with Jolly Road before entering [[Delhi Charter Township, Michigan|Delhi Township]]. About two-thirds of a mile (1.1&nbsp;km) south of Jolly Road, I-496 meets I-96 and terminates; US&nbsp;127 continues southward as a freeway toward [[Jackson, Michigan|Jackson]].<ref name="MDOT12">{{cite MDOT map |year= 2012 |inset= Lansing  }}</ref><ref name=google>{{google maps |url= //maps.google.com/maps?saddr=I-496+E&daddr=US-127+S&hl=en&sll=42.676946,-84.499626&sspn=0.030415,0.02914&geocode=FWbPiwId6iH0-g%3BFfQtiwIdLLD2-g&t=h&mra=ls&z=12 |title= Overview Map of I-496 |access-date= July 12, 2012}}</ref>

[[File:I-496 US 127 SB.jpg|thumb|Southbound I-496/US&nbsp;127 in East Lansing|alt=Photograph of]]
Like other state highways in Michigan, I-496 is maintained by the [[Michigan Department of Transportation]] (MDOT). In 2011, the department's traffic surveys showed that on [[average annual daily traffic|average]], 61,082&nbsp;vehicles used the freeway between BL I-96 and the Trowbridge Road interchange south of US&nbsp;127, the highest traffic count along I-496. West of Creyts Road, 17,600&nbsp;vehicles did so each day, which was the lowest count along the trunkline.<ref name=TMIS>{{cite web |author= Bureau of Transportation Planning |url= http://mdotnetpublic.state.mi.us/tmispublic/ |title= Traffic Monitoring Information System |publisher= Michigan Department of Transportation |year= 2008 |access-date= June 30, 2012}}</ref> As an Interstate Highway, all of I-496 is listed on the [[National Highway System (United States)|National Highway System]],<ref name=NHS-MI>{{cite map|author=Michigan Department of Transportation |title=National Highway System, Michigan |date=April 23, 2006 |scale=Scale not given |location=Lansing |publisher=Michigan Department of Transportation |url=http://www.michigan.gov/documents/MDOT_NHS_Statewide_150626_7.pdf |archive-url=http://www.webcitation.org/6AqxSurw0?url=http://www.michigan.gov/documents/MDOT_NHS_Statewide_150626_7.pdf |archive-date=September 21, 2012 |format=PDF |access-date=October 7, 2008 |deadurl=yes |df=mdy-all }}</ref> a network of roads deemed important to the country's economy, defense, and mobility.<ref name=NHS>{{cite web |first1= Stefan |last1= Natzke |first2= Mike |last2= Neathery |first3= Kevin |last3= Adderly |last-author-amp= yes |url= http://www.fhwa.dot.gov/planning/national_highway_system/ |title= What is the National Highway System? |work= National Highway System |publisher= [[Federal Highway Administration]] |date= June 20, 2012 |access-date= July 1, 2012 }}</ref>

==History==
[[File:Lansing, Michigan 1955 Yellow Book.jpg|thumb|left|1955 planning map for Lansing's Interstates|alt=Black and white map]]
An east–west freeway was originally planned as an Interstate Highway allowing traffic to access downtown Lansing in the 1955 ''[[General Location of National System of Interstate Highways]]'' (''Yellow Book''), an early proposal for what would become the Interstate Highway System.<ref name="YBD">{{cite map |map-url=http://commons.wikimedia.org/wiki/File:Lansing,_Michigan_1955_Yellow_Book.jpg |title= General Location of National System of Interstate Highways Including All Additional Routes at Urban Areas Designated in September 1955 |map= Lansing |scale= Scale not given |page= 44 |author= [[Bureau of Public Roads]] |location= Washington, DC |publisher= [[Government Printing Office]] |year= 1955 |oclc= 4165975 |via= [[Wikimedia Commons]] }}</ref> As originally proposed by the [[Michigan State Highway Department]] in 1958, the freeway was to be called I-296.<ref name=RIRNM58>{{cite web|author=Michigan State Highway Department |title=Recommended Interstate Route Numbering for Michigan |date=April 25, 1958 |location=Lansing |publisher=Michigan State Highway Department |url=http://nwindianahwys.homestead.com/michiplan.html |archive-url=https://web.archive.org/web/20040805182658/http://nwindianahwys.homestead.com/michiplan.html |archive-date=August 5, 2004 |deadurl=yes |df=mdy-all }}</ref> The department was waiting on approval of a final numbering scheme the next year,<ref name=blade1959-06-04>{{cite news |title= Michigan Delays Road Number System |url= https://news.google.com/newspapers?id=mb1OAAAAIBAJ&sjid=9AAEAAAAIBAJ&pg=7401,5582043&dq=interstate+opening+michigan&hl=en |work= [[Toledo Blade]] |agency= Associated Press |date= June 4, 1959 |page= 11 |oclc= 12962635 |access-date= November 21, 2010 |via= [[Google News]] }}</ref> before the first Interstates were signed in the state in 1959.<ref name=HP1959-10-13>{{cite news |url= https://www.newspapers.com/clip/3823487// |title= New Signs Mark Interstate&nbsp;75 |work= [[Escanaba Daily Press]] |agency= Associated Press |date= October 13, 1959 |page= 2 |access-date= December 14, 2015  |via= Newspapers.com |oclc= 9670912}} {{open access}}</ref> By the time construction started on the Lansing freeway, it was numbered I-496.<ref name=HS62>{{cite news |url= https://www.newspapers.com/clip/3823504// |title= Release Bids for Freeway to Holland |work= [[Holland Evening Sentinel]] |agency= [[United Press International]] |date= July 20, 1962 |page= 6 |oclc= 13440201 |access-date= December 14, 2015 |via= Newspapers.com }} {{open access}}</ref>

The section near downtown was to be built through a historically African-American neighborhood. The neighborhood was formed through "unwritten rules of segregation" as real estate agents and mortgage brokers guided black residents to the area when they were looking to buy homes.<ref name=miller/> When the state and federal governments were planning the freeway, the area was chosen for the path of I-496. The neighborhood boasted a community center and several businesses that catered to the black population of Lansing, including the only record store that sold rhythm and blues music. Community leaders did not fight the freeway and instead lobbied for affordable housing and relocation assistance. The construction spurred integration of blacks into the wider community; some were able to move into neighborhoods previously closed to them, purchasing "newer houses near better schools."<ref name=miller>{{cite news |last= Miller |first= Matthew |date= February 22, 2009 |title= A Complicated Legacy: I-496 Slashed through Key Black Neighborhood in the '60s |work= Lansing State Journal |pages= 1A, 8A |issn= 0274-9742 |oclc= 6678181 |url= http://www.lansingstatejournal.com/article/20090222/NEWS01/108180003/Looking-back-496-construction-complicated-legacy?nclick_check=1 |access-date= July 13, 2012}}</ref> In total, the construction of the freeway required the demolition or removal of nearly 600&nbsp;homes, 60&nbsp;businesses, and 15&nbsp;farms.<ref name=ingalls>{{cite news |title= City's East–West Traffic Speeded |first= Norris |last= Ingells |work= Lansing State Journal |date= February 14, 1965 |issn= 0274-9742 |oclc= 6678181 |page=}}</ref>

The first section of I-496 was opened in December 1963,<ref name=IDG63>{{cite news |url= https://www.newspapers.com/clip/3823514// |title= New Highway Opened |work= [[Ironwood Daily Globe]] |agency= [[Associated Press]] |date= December 21, 1963 |page= 9 |oclc= 10890811 |access-date= December 14, 2015 |via= [[Newspapers.com]] }} {{open access}}</ref> and ran from I-96 northerly to [[M-43 (Michigan highway)|M-43]]/[[M-78 (Michigan highway)|M-78]] (Saginaw and Kalamazoo streets) between Lansing and East Lansing. The freeway, comprising the southern two-thirds, was designated I-496/M-78/[[Interstate 96 Business (Lansing, Michigan)|BL I-96]] while the northern portion was on city streets as M-78/BL I-96.<ref name=MSHD63>{{cite MDOT map |year= 1963 |link= yes |inset= Lansing }}</ref><ref name=MSHD64>{{cite MDOT map |year= 1964 |inset= Lansing }}</ref> Some 50&nbsp;men completed the work by year's end; they went entirely without vacation time to accomplish the feat.<ref name=IDG63/> Another section of freeway was opened in 1966, and US&nbsp;127 was rerouted to follow I-496/M-78. BL I-96 was removed from I-496/US&nbsp;127/M-78 and routed along the former US&nbsp;127.<ref name=MDSH66>{{cite MDOT map |year= 1966 |link= yes |inset= Lansing }}</ref><ref name=MDSH67>{{cite MDOT map |year= 1967 |inset= Lansing }}</ref> The freeway segment north of the Trowbridge Road interchange continuing northward as part of US&nbsp;127 was opened in 1969. Another section opened at the same time was the western section from I-96 to Lansing Road (then US&nbsp;27) in 1969.<ref name=MDSH69>{{cite MDOT map |year= 1969 |c-link= yes |inset= Lansing }}</ref><ref name=MDSH70>{{cite MDOT map |year= 1970 |inset= Lansing }}</ref> The remaining section between M-99 (then Logan Street, now Martin Luther King, Jr. Boulevard) and I-496/US&nbsp;127 opened on December 18, 1970, completing construction.<ref name=paving>{{cite news |last= Rook |first= Christine |date= July 23, 2006 |title= Paving the Way: Interstate Roads Have Shaped the Future for Many Mid-Michigan Communities |url= http://pqasb.pqarchiver.com/lansingstatejournal/access/1764838201.html?FMT=ABS&FMTS=ABS:FT&date=Jul+23%2C+2006&author=Rook+Christine&pub=Lansing+State+Journal&edition=&startpage=D.1&desc=PAVING+THE+WAY |work= [[Lansing State Journal]] |pages= 1D, 5D |issn= 0274-9742 |oclc= 6678181 |access-date= July 13, 2012 |via= [[ProQuest]] Archiver |subscription=yes}}</ref><ref name=barnett>{{cite book |last= Barnett |first= LeRoy |year= 2004 |title= A Drive Down Memory Lane: The Named State and Federal Highways of Michigan |location= Allegan Forest, MI |publisher= Priscilla Press |pages= 140–141, 165 |isbn= 1-886167-24-9 |oclc= 57425393}}</ref>

The freeway underwent a $42.4 million reconstruction (equivalent to ${{formatprice|{{inflation|US-NGDPPC|42400000|2001|r=-5}}}} in {{inflation-year|US-NGDPPC}}{{inflation-fn|US-NGDPPC|lastauthoramp=yes}}) between April and November 2001 which included the rehabilitation or reconstruction of 35&nbsp;bridges, {{convert|8.5|mi|km}} of freeway, and the addition of a [[auxiliary lane|weave-merge lane]] between Pennsylvania Avenue and US&nbsp;127.<ref>{{cite news |last= Gantert |first= Tom |date= April 4, 2001 |title= The Big Fix on Interstate 496 |work= Lansing State Journal |page= A1 |issn= 0274-9742 |oclc= 6678181 |url= http://pqasb.pqarchiver.com/lansingstatejournal/access/2528972841.html?FMT=ABS&FMTS=ABS:FT&date=Apr+4%2C+2001&author=Gantert+Tom&pub=Lansing+State+Journal&edition=&startpage=A.1&desc=THE+BIG+FIX+ON+INTERSTATE+496 |access-date= July 13, 2012 |via= ProQuest Archiver |subscription= yes}}</ref><ref name=hugh>{{cite news |last= Hugh |first= Leach |date= November 5, 2001 |title= Most Area Road Work Complete |work= Lansing State Journal |page= B3 |issn= 0274-9742 |oclc= 6678181 |url= http://pqasb.pqarchiver.com/lansingstatejournal/access/2529033211.html?FMT=ABS&FMTS=ABS:FT&date=Nov+5%2C+2001&author=Leach+Hugh&pub=Lansing+State+Journal&edition=&startpage=B.3&desc=Most+area+road+work+complete |access-date= July 13, 2012 |via= ProQuest Archiver}}</ref> Speed limits were raised along I-496 from {{convert|55|to|70|mph|km/h|adbbr=on}} in 2007 to reflect the speeds motorists were driving during studies conducted by MDOT and the [[Michigan State Police]].<ref name=wallbank>{{cite news |last= Wallbank |first= Derek |date= April 3, 2007 |title= Drivers on I-496 Get the Green Light to Go 70: Stretch of US&nbsp;127 in Frandor Area Seed Higher Speed Limits as Well |work= Lansing State Journal |pages= 1A, 7A |issn= 0274-9742 |oclc= 6678181 |url= http://pqasb.pqarchiver.com/lansingstatejournal/access/1731912811.html?FMT=ABS&FMTS=ABS:FT&date=Apr+3%2C+2007&author=Derek+Wallbank&pub=Lansing+State+Journal&edition=&startpage=A.1&desc=Drivers+on+I-496+get+the+green+light+to+go+70 |access-date= July 13, 2012 |via= ProQuest Archiver |subscription= yes}}</ref>

[[File:Olds Mansion Exterior, East Front.png|thumb|right|Olds Mansion|alt=Black and white photograph]]
The name applied to the freeway was not without controversy. The Lansing City Council named it in September 1966 after Ralph W. Crego, a former city council member and the longest-serving mayor in the city's history. The Historical Society of Greater Lansing wanted it named the "R.E. Olds Expressway", in part because the new road brought about the demolition of the [[Ransom E. Olds#Residence|Olds Mansion]],<ref name=barnett/> which was listed on the [[National Register of Historic Places]],<ref name=mtt>{{cite web |first= Patricia |last= O'Hearn |date= n.d. |url= http://www.michigan.gov/documents/hal_mhc_mhm_carcapital_01-08-2003_92059_7.pdf |title= Michigan Time Traveler |publisher= Lansing Newspapers in Education, Michigan Historical Center |format= PDF |access-date= July 13, 2012}}</ref> and to "recogniz[e] the contributions of R.E. Olds to the industries of the city."<ref name=barnett/> The society approached the [[Michigan Legislature]], which introduced House Resolution&nbsp;48 in February 1970 using the historical society's preferred name. The city council realized that they had been bypassed and conveniently discovered that their original resolution was not "formally adopted".<ref name=barnett/> They named a park for Crego instead in October 1970 and adopted a resolution to name I-496 the "Oldsmobile Expressway". The Legislature approved its resolution resulting in two names, one for the founder of the car company, and one for the company itself. The council member who introduced the city's resolution criticized the Legislature for taking action without consultation. The state resolution was intercepted before it could be sent to the Michigan Department of State Highways, and the freeway opened on December 18, 1970, with the "Oldsmobile Expressway" name. On August 21, 1972, during the celebrations for the 75th anniversary of Oldsmobile, Senate Concurrent Resolution&nbsp;345 renamed I-496 the "R.E. Olds Freeway".<ref name=barnett/>

==Exit list==
{{MIinttop|exit|length_ref=<ref name=PRFA>{{cite MDOT PRFA |link= yes |access-date= July 11, 2012}}</ref>}}
{{MIint|exit
|county=Eaton
|cspan=2
|location=Delta Township
|lspan=2
|mile=0.000
|exit=—
|road={{jct|state=MI|I|96|I|69|city1=Grand Rapids|city2=Flint|location3=[[Detroit]]|location4=[[Ft. Wayne, Indiana|Ft. Wayne]]}}
|notes=Exit&nbsp;95 on I-96/I-69}}
{{MIint|exit
|mile=1.637
|exit=1
|road=Creyts Road
|notes=Signed as exits 1A (south) and 1B (north) westbound}}
{{MIint|exit
|county1=Eaton
|county2=Ingham
|location_special=[[Delta Charter Township, Michigan|Delta&nbsp;Township]]–<br>[[Lansing, Michigan|Lansing]] city line
|mile=3.561
|exit=3
|road=Waverly Road
|notes=}}
{{MIint|exit
|county=Ingham
|cspan=9
|location=Lansing
|lspan=6
|mile=4.545
|exit=4
|type=incomplete
|road=Lansing Road
|notes=Westbound exit and eastbound entrance}}
{{MIint|exit
|mile=5.306
|mile2=5.403
|exit=5
|road={{jct|state=MI|M|99|Capitol Loop||dir1=south|dir2=east|name2=[[List of streets named after Martin Luther King, Jr.|Martin Luther King, Jr. Boulevard]]}}
|notes=}}
{{MIint|exit
|mile=5.802
|mile2=5.950
|exit=6
|road=Pine Street, Walnut Street&nbsp;– [[Downtown Lansing]]
|notes=}}
{{MIint|exit
|mile=6.273
|exit=7A
|type=incomplete
|road=Grand Avenue&nbsp;– [[Downtown Lansing]]
|notes=Westbound exit and eastbound entrance}}
{{MIint|exit
|mile=6.567
|mile2=6.921
|exit=7
|road={{jct|state=MI|BL|96|Capitol Loop ||dab1=Lansing|dir2=west|name2=Pennsylvania Avenue, Cedar Street, Larch Street}}
|notes=Separate exits for Cedar Street and Pennsylvania Avenue, connected by [[collector-distributor road]]s eastbound only; exit 7A is also attached to collector-distributor roads westbound only}}
{{MIint|exit
|mile=8.576
|exit=8
|type=concur
|road={{jct|state=MI|US|127|dir1=north|city1=Flint|city2=East Lansing}}
|notes=Northern end of US&nbsp;127 concurrency}}
{{MIint|exit
|location=East Lansing
|mile=8.748
|exit=9
|road=Trowbridge Road
|notes=}}
{{MIint|exit
|location=Lansing
|mile=10.912
|exit=11
|road=Dunckel Road, Jolly Road
|notes=}}
{{MIint|exit
|location=Delhi Township
|mile=11.907
|exit=—
|type=concur
|road={{nowrap|{{jct|state=MI|I|96|city1=Detroit|city2=Grand Rapids}}}}<br>{{jct|state=MI|US|127|dir1=south|city1=Jackson}}
|notes=Exit 106 on I-96; exit 73 on US&nbsp;127; freeway continues south as US&nbsp;127}}
{{jctbtm|keys=concur,incomplete|exit}}

==Related trunkline==
{{main|Capitol Loop}}
{{Infobox road small
|state= MI
|type= BL
|route= 496
|length_mi= 2.381
|length_ref= <ref name="PRFA"/>
|location=[[Lansing, Michigan|Lansing]]
|formed= October 13, 1989<ref name=ROW180>{{cite map |author= Michigan Department of Transportation |location= Lansing |publisher= Michigan Department of Transportation |author2= City of Lansing |last-author-amp= yes |mapurl= http://mdotwas1.mdot.state.mi.us/public/ROWFiles/files/Ingham/sheet180.pdf |title= Right-of-Way File Application |map= Ingham County |sheet= 180 |access-date= October 15, 2008 |date= August 29, 2007 |scale= Scale not given |map-format= PDF |archive-url= http://www.webcitation.org/5va0llY6P |archive-date= January 8, 2011 |dead-url= no |oclc= 12843916}}</ref>
}}
The Capitol Loop is a state trunkline highway running through Lansing that was commissioned on October 13, 1989.<ref name=ROW180/> It forms a [[loop route]] off I-496 through downtown near the [[Michigan State Capitol]] complex, home of the [[Michigan State Legislature|state legislature]] and several state departments. However, unlike other business loops in Michigan, it has unique [[reassurance marker]]s—the signs that serve as regular reminders of the name and number of the highway. It is known internally at MDOT as Connector&nbsp;496 for inventory purposes.<ref name=MGF8C/> The highway follows a series of one-way and two-way streets through downtown Lansing, directing traffic downtown to the State Capitol and other government buildings.<ref name=googleCL>{{google maps |url= https://maps.google.com/maps?saddr=S+Martin+Luther+King+Jr+Blvd&daddr=42.7326018,-84.55619+to:42.7335723,-84.5492377+to:S+Cedar+St&hl=en&ll=42.733554,-84.557347&spn=0.030387,0.029998&sll=42.726713,-84.555545&sspn=0.030391,0.029998&geocode=FSHziwId4Jr1-g%3BFTkMjAIdYsb1-ikdsaSs18EiiDH0HJhJRWoyyA%3BFQQQjAIdi-H1-inBYoC72MEiiDEtrL8yqDBdGg%3BFT_yiwIdS_L1-g&mra=dpe&mrsp=2&sz=15&via=1,2&t=m&z=15 |title= Overview Map of the Capitol Loop |access-date= October 14, 2008 |link=no}}</ref><ref name=UMMCA>{{cite map |first= David M. |last= Brown |author2= [[Universal Map]] |last-author-amp= yes |scale= [c. 1:24,000] |location= Blue Bell, PA |publisher= Universal Map |title= Michigan County Atlas: Back Roads & Forgotten Places |edition= 2nd |year= 2010 |page= 66 |map= Lansing |isbn= 978-0-7625-6505-4 |oclc= 624374092}}</ref> Unlike the other streets downtown, the seven streets composing the Capitol Loop are under state maintenance and jurisdiction.<ref name=TOP08>{{cite MDOT map |year= 2008T |inset= Lansing}}</ref>

The loop was originally proposed in 1986 as part of a downtown revitalization effort.<ref name=Andrews2003-05-22>{{cite news |first= Chris |last= Andrews |title= Work Set for Capitol Loop |work= Lansing State Journal |date= May 22, 2003 |pages= 1A, 6A |issn= 0274-9742 |oclc= 6678181}}</ref> Almost from the beginning before the highway was commissioned in 1989, it was affected by controversial proposals. The first was related to suggestions by community leaders to rename city streets in honor of [[Martin Luther King, Jr.]]<ref name=barnett/> Another controversy dealt with rebuilding the streets as part of a downtown beautification project; the downtown business community protested the original scope of construction,<ref name=Sturm2003-10-29>{{cite news |first= Daniel |last= Sturm |title= The 'Big Dig' Causing a Big Flap in Downtown Lansing |work= [[City Pulse]] |location= Lansing, MI |date= October 29, 2003 |oclc= 48427464}}</ref> and the Lansing City Council threatened to cancel the project in response to the controversy.<ref name=Murphy2003-11-04>{{cite news |first= Shannon |last= Murphy |title= City to Seek Options for Capitol-Area Road Work |work= Lansing State Journal |date= November 4, 2003 |pages= 1–2B |issn= 0274-9742 |oclc= 6678181}}</ref> In 2010, additional controversies surfaced regarding the posting and enforcement of speed limits on city streets in Michigan, including the streets that make up the Capitol Loop.<ref name=Kolp2010-06-02>{{cite news|first=Stephanie |last=Kolp |title=Some Speeding Tickets Being Waived |url=http://www.wlns.com/Global/story.asp?S=12583883 |publisher=[[WLNS-TV]] |location=Lansing, MI |date=June 2, 2010 |access-date=July 21, 2010 |archive-url=http://www.webcitation.org/5va06KyVV?url=http://www.wlns.com/Global/story.asp?S=12583883 |archive-date=January 8, 2011 |dead-url=no |df=mdy-all }}</ref>

==See also==
*{{portal-inline|Michigan Highways}}

==References==
{{reflist|30em}}

==External links==
{{commons category|Interstate 496}}
{{Attached KML|display=inline,title}}
*{{osmrelation-inline|184734|I-496}}
*[http://www.michiganhighways.org/listings/MichHwys250-696.html#I-496 I-496] at Michigan Highways
*[http://www.state-ends.com/michigan/i496/ I-496] at Michigan Highway Ends
*[http://www.kurumi.com/roads/3di/ix96.html#496mi I-496] at kurumi.com
*[http://www.interstate-guide.com/i-496_mi.html I-496] at AA Roads Interstate Guide
*[http://beta.greenandwhite.com/article/99999999/NEWS01/90219004/-1/special I-496 and the changing landscape of Lansing], multimedia content based on the ''Lansing State Journal'' feature on I-496's construction

{{3di|96}}
{{featured article}}

{{DEFAULTSORT:I496}}
[[Category:Interstate Highways in Michigan|96-4]]
[[Category:Auxiliary Interstate Highways|96-4]]
[[Category:Interstate 96]]
[[Category:Transportation in Eaton County, Michigan]]
[[Category:Transportation in Ingham County, Michigan]]