{{About|the South Australian state electorate|the Australian federal electorate|Division of Adelaide|the historical South Australian state electorate|Electoral district of City of Adelaide}}
{{Infobox Australian Electorate
| name               = Adelaide
| state              = sa
| image              = Adelaide Electoral District SA 2014.png
| imagesize          = 
| image_alt          = Map of Adelaide, South Australia with electoral district of Adelaide highlighted
| caption            = Electoral district of Adelaide (green) in the Greater Adelaide area
| created            = 1902
| mp                 = [[Rachel Sanderson]]
| mp-party           = [[Liberal Party of Australia (South Australian Division)|Liberal Party of Australia (SA)]]
| namesake           = [[Adelaide of Saxe-Meiningen]]
| electors           = 24,779
| electors_year      = 2014
| electors_footnotes = 
| area               = 23.4
| class              = Metropolitan
| latd               = 34
| latm               = 54
| lats               = 28
| longd              = 138
| longm              = 36
| longs              = 5
| coord_type         = 
| coordinates        = 
| footnotes          = 
}}
'''Adelaide''' is an electorate for the [[South Australian House of Assembly]]. The 23.4&nbsp;km² state seat of Adelaide currently consists of the [[Adelaide city centre]] including [[North Adelaide]] and suburbs to the inner north and inner north east: [[Walkerville, South Australia|Walkerville]], [[Gilberton, South Australia|Gilberton]], [[Medindie, South Australia|Medindie]], [[Medindie Gardens, South Australia|Medindie Gardens]], [[Thorngate, South Australia|Thorngate]], [[Fitzroy, South Australia|Fitzroy]], [[Ovingham, South Australia|Ovingham]], most of [[Prospect, South Australia|Prospect]] up to [[Regency Road, Adelaide|Regency Road]], and parts of [[Collinswood, South Australia|Collinswood]] and [[Nailsworth, South Australia|Nailsworth]]. The boundaries have been the same for the past three elections. The federal [[division of Adelaide]] covers the state seat of Adelaide and additional suburbs in each direction.

The electorate's name comes from the city which it encompasses, which is named after [[Adelaide of Saxe-Meiningen|Princess Adelaide of Saxe-Meiningen]], the German born [[Queen consort]] of the King of England, [[William IV of the United Kingdom|King William IV]]. Originally the seat of [[Electoral district of City of Adelaide|City of Adelaide]] from 1857 to 1862, Adelaide was created from the seats of [[Electoral district of East Adelaide|East Adelaide]] and [[Electoral district of West Adelaide|West Adelaide]] as a four-seat [[multi-member]] district, surrounded by the other two metropolitan districts of 13 seats total being [[Electoral district of Torrens|Torrens]] and [[Electoral district of Port Adelaide|Port Adelaide]], from the [[South Australian state election, 1902|1902 election]].<ref>[http://trove.nla.gov.au/ndp/del/article/87816882 The 13 electorates from 1902 to 1915: The Adelaide Chronicle]</ref> Adelaide became a three-member district from the [[South Australian state election, 1915|1915 election]], and then changed from a multi-member to [[Single-member districts|single-member]] seat upon the introduction of the [[Playmander]] from the [[South Australian state election, 1938|1938 election]].<ref name=parl>{{cite web |url=http://www.parliament.sa.gov.au/AboutParliament/From1836/Documents/StatisticalRecordoftheLegislature1836to20093.pdf |title=Statistical Record of the Legislature 1836 to 2009 |publisher= Parliament of South Australia}}</ref> 

For most of the next half-century, the seat was comfortably safe for the [[Australian Labor Party]].  A significant redistribution in 1983 saw the [[Australian Labor Party|Labor]] [[Two-party-preferred vote|two-party]] vote reduced from 66 percent to 47 percent, transforming into a notional marginal [[Liberal Party of Australia|Liberal]] seat in one stroke. However, Labor retained the seat at the [[South Australian state election, 1985|1985 election]], albeit as the most marginal seat in parliament. Liberal [[Michael Armitage (politician)|Michael Armitage]] narrowly took the seat at the [[South Australian state election, 1989|1989 election]]--the first time that they or their predecessors, the [[Liberal and Country League]], had won it in its single-member incarnation. The Liberal high-tide in Adelaide occurred at the landslide [[South Australian state election, 1993|1993 election]], with the Liberal two-party vote swelling to a safe 64.1 percent.  However, it once again became a marginal Liberal seat at the [[South Australian state election, 1997|1997 election]].

After a redistribution ahead of the [[South Australian state election, 2002|2002 election]] made the seat even more marginal, Armitage tried to transfer to the much friendlier seat of [[Electoral district of Bragg|Bragg]], but lost a preselection battle to [[Vickie Chapman]].  Labor candidate [[Jane Lomax-Smith]] regained the seat for Labor at the [[South Australian state election, 2002|2002 election]] as a marginal seat, one of two gains that assisted Labor in forming government.  It became a safe Labor seat at the landslide [[South Australian state election, 2006|2006 election]] on a 60.2 percent two-party vote, before the Liberals won Adelaide for the second time at the [[South Australian state election, 2010|2010 election]] on a swing of over 14 percent, turning it from safe Labor to marginal Liberal in one stroke. The Liberals retained Adelaide at the [[South Australian state election, 2014|2014 election]] on a marginal 52.4 (−1.8) percent two-party vote.

Upon the release of the [[South Australian state election, 2018#Post-redistribution pendulum|2016 draft electoral redistribution]], Liberal MP [[Rachel Sanderson]] organised the mass distribution of a [[pro forma]] document in the two inner metropolitan suburbs of [[Walkerville, South Australia|Walkerville]] and [[Gilberton, South Australia|Gilberton]], which aimed for residents to use the pro forma document to submit their objection to the commission in support of Sanderson's campaign to keep the two suburbs in her seat of Adelaide, which in the draft would have been transferred to neighbouring [[Electoral district of Torrens|Torrens]]. Sanderson's position however was at odds with her own party's submission which in fact agreed with the commission that Walkerville should be transferred to Torrens. Under the commission's draft proposal, the Liberal margin in Adelaide would have been reduced from 2.4 percent to 0.6 percent, but would have also resulted in the Labor margin in Torrens reduced from 3.5 percent to 1.1 percent. Of a record 130 total submissions received in response to the draft redistribution, about 100 (over three quarters of all submissions) were from Walkerville and Gilberton.<ref>[http://www.abc.net.au/news/2016-09-22/adelaide-residents-compared-to-hyacinth-bucket/7868082 Adelaide residents compared to 'Hyacinth Bucket' for lashing out at proposed electoral shift - ABC 22 September 2016]</ref><ref>[http://indaily.com.au/news/local/2016/09/22/patrician-burghers-of-adelaide-lament-wont-someone-think-of-the-rotary-clubs/ Patrician burghers of Adelaide lament: 'Won't someone think of the rotary clubs?' - InDaily 22 September 2016]</ref><ref>[http://indaily.com.au/news/politics/2016/09/28/libs-last-ditch-bid-for-electoral-fairness/ Libs' last-ditch bid for "electoral fairness" - InDaily 28 September 2016]</ref><ref>[http://www.adelaidenow.com.au/news/south-australia/mps-make-submissions-into-south-australian-boundary-changes/news-story/9c7c647d6483a86fb2af6d4a25f73a68 MPs make submissions into South Australian boundary changes - The Advertiser 22 September 2016]</ref><ref>[http://edbc.sa.gov.au/redistributions/2016/draft-redistribution-report.html Draft Report (PDF): Electoral Districts Boundaries Commission 15 August 2016]</ref><ref>[http://edbc.sa.gov.au/redistributions/2016/2016-submissions.html Detail and download of all 130 submissions submitted: Electoral Districts Boundaries Commission]</ref> As a result, the commission reversed the draft decision in the final publication.<ref name=finalredist>[http://edbc.sa.gov.au/redistributions/2016/2016-final-redistribution-report.html Final Report (PDF): Electoral Districts Boundaries Commission 8 December 2016]</ref>

==Members for Adelaide==
{| class="wikitable"
|-
!colspan="16" | Four-member electorate (1902–1915)
|-
!colspan="2"|Member!!Party!!Term!!colspan="2"|Member!!Party!!Term!!colspan="2"|Member!!Party!!Term!!colspan="2"|Member!!Party!!Term
|-
| rowspan=2 {{Australian party style|National Defence League}}|&nbsp;
| rowspan=2 | [[Lewis Cohen (mayor)|Lewis Cohen]]
| rowspan=2 | [[Australasian National League|National League]]
| rowspan=2 | 1902–1906
| {{Australian party style|Other}}|&nbsp;
| [[Bill Denny (Australian politician)|Bill Denny]]
| [[Independent politician|Independent Liberal]]
| 1902–1905
| {{Australian party style|Other}}|&nbsp;
| [[Hugh Denison|Hugh Dixson]]
| 
| 1902–1905
| {{Australian party style|Other}}|&nbsp;
| [[Johann Scherk]]
| 
| 1902–1905
|-
|rowspan="4" {{Australian party style|Labor}}|&nbsp;
|rowspan="4"| [[William David Ponder]]
|rowspan="4"| [[Australian Labor Party|Labor]]
|rowspan="4"| 1905–1915
|rowspan="3" {{Australian party style|Labor}}|&nbsp;
|rowspan="3"| [[Ernest Roberts (Australian politician)|Ernest Roberts]]
|rowspan="3"| [[Australian Labor Party|Labor]]
|rowspan="3"| 1905–1908
|rowspan="2" {{Australian party style|Labor}}|&nbsp;
|rowspan="2"|[[James Zimri Sellar]]
|rowspan="2"|[[Australian Labor Party|Labor]]
|rowspan="2"|1905–1906
|-
|{{Australian party style|Labor}}|&nbsp;
|rowspan="3"| [[Bill Denny (Australian politician)|Bill Denny]]
|rowspan="3"| [[Australian Labor Party|Labor]]
|rowspan="3"| 1906–1915
|-
|{{Australian party style|Labor}}|&nbsp;
|rowspan="2" {{Australian party style|Labor}}|&nbsp;
|rowspan="2"| [[Reginald Blundell]]
|rowspan="2"| [[Australian Labor Party|Labor]]
|rowspan="2"| 1907–1915
|-
|{{Australian party style|Labor}}|&nbsp;
| {{Australian party style|Labor}}|&nbsp;
| [[Edward Alfred Anstey]]
| [[Australian Labor Party|Labor]]
| 1908–1915
|}

{| class="wikitable"
|-
!colspan="15" | Three-member electorate (1915–1938)
|-
!colspan="2"|Member!!Party!!Term!!colspan="2"|Member!!Party!!Term!!colspan="2"|Member!!Party!!Term
|-
| rowspan=4 {{Australian party style|Labor}}|&nbsp;
| rowspan=6 | [[Bill Denny (Australian politician)|Bill Denny]]
| rowspan=4 | [[Australian Labor Party|Labor]]
| rowspan=4 | 1915–1933
| rowspan=1 {{Australian party style|Labor}}|&nbsp;
| rowspan=2 | [[Reginald Blundell]]
| rowspan=1 | [[Australian Labor Party|Labor]]
| rowspan=1 | 1915–1917
| rowspan=1 {{Australian party style|Labor}}|&nbsp;
| rowspan=1 | [[John Gunn (Australian politician)|John Gunn]]
| rowspan=1 | [[Australian Labor Party|Labor]]
| rowspan=1 | 1915–1917
|-
| {{Australian party style|Nationalist}}|&nbsp;
| [[National Party (South Australia)|National]]
| 1917–1918
| rowspan=3 {{Australian party style|Labor}}|&nbsp;
| rowspan=3 | [[Bert Edwards (politician)|Bert Edwards]]
| rowspan=3 |[[Australian Labor Party|Labor]]
| rowspan=3 |1917–1931
|-
| {{Australian party style|Labor}}|&nbsp;
| [[John Gunn (Australian politician)|John Gunn]]
| [[Australian Labor Party|Labor]]
| 1918–1926
|-
| {{Australian party style|Labor}}|&nbsp;
| rowspan=3 |[[Herbert George (politician)|Herbert George]]
| rowspan=3 |[[Australian Labor Party|Labor]]
| rowspan=3 |1926–1933
|-
| rowspan=2 {{Australian party style|Socialist}}|&nbsp;
| rowspan=2|[[Parliamentary Labor Party|Parliamentary Labor]]
| rowspan=2|1931–1933
| {{Australian party style|Labor}}|&nbsp;
| {{Australian party style|Lang Labor}}|&nbsp;
| rowspan="2"|[[Martin Collaton]]
| [[Lang Labor Party (South Australia)|Lang Labor]]
| 1931–1932
|-
| {{Australian party style|Labor}}|&nbsp;
| {{Australian party style|Labor}}|&nbsp;
| [[Australian Labor Party|Labor]]
| 1932–1933
|-
| rowspan="2" {{Australian party style|Lang Labor}}|&nbsp;
| rowspan="4"|[[Doug Bardolph]]
| rowspan="2"|[[Lang Labor Party (South Australia)|Lang Labor]]
| rowspan="2"|1933–1934
| {{Australian party style|Lang Labor}}|&nbsp;
| rowspan="4"|[[Bob Dale (politician)|Bob Dale]]
| [[Lang Labor Party (South Australia)|Lang Labor]]
| 1933–1933
| {{Australian party style|Lang Labor}}|&nbsp;
| rowspan="4"|[[Tom Howard (politician)|Tom Howard]]
| [[Lang Labor Party (South Australia)|Lang Labor]]
| 1933–1933
|-
| {{Australian party style|Lang Labor}}|&nbsp;
| [[Lang Labor Party (South Australia)#South Australian Lang Labor Party (SALLP)|SA Lang Labor]]
| 1933–1934
| {{Australian party style|Lang Labor}}|&nbsp;
| [[Lang Labor Party (South Australia)#South Australian Lang Labor Party (SALLP)|SA Lang Labor]]
| 1933–1934
|-
| {{Australian party style|Labor}}|&nbsp;
| [[Australian Labor Party|Labor]]
| 1934–1935
| rowspan="2" {{Australian party style|Labor}}|&nbsp;
| rowspan="2"|[[Australian Labor Party|Labor]]
| rowspan="2"|1934–1938
| rowspan="2" {{Australian party style|Labor}}|&nbsp;
| rowspan="2"|[[Australian Labor Party|Labor]]
| rowspan="2"|1934–1938
|-
| {{Australian party style|Independent}}|&nbsp;
| [[Independent politician|Independent]]
| 1935–1938
|}

{| class="wikitable"
|-
! colspan="2"|Member
! Party
! Term
|-
| {{Australian party style|Independent}}|&nbsp; 
| [[Doug Bardolph]] 
| [[Independent (politician)|Independent]] 
| 1938–1944
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Bob Dale (politician)|Bob Dale]] 
| [[Australian Labor Party|Labor]] 
| 1944–1947
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Herbert George (Australian politician)|Herbert George]] 
| [[Australian Labor Party|Labor]] 
| 1947–1950
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Sam Lawn]] 
| [[Australian Labor Party|Labor]] 
| 1950–1971
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Jack Wright (politician)|Jack Wright]] 
| [[Australian Labor Party|Labor]] 
| 1971–1985
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Michael Duigan]] 
| [[Australian Labor Party|Labor]] 
| 1985–1989
|-
| {{Australian party style|Liberal}}|&nbsp; 
| [[Michael Armitage (politician)|Michael Armitage]] 
| [[Liberal Party of Australia|Liberal]] 
| 1989–2002
|-
| {{Australian party style|Labor}}|&nbsp; 
| [[Jane Lomax-Smith]] 
| [[Australian Labor Party|Labor]] 
| 2002–2010
|-
| {{Australian party style|Liberal}}|&nbsp; 
| [[Rachel Sanderson]] 
| [[Liberal Party of Australia|Liberal]] 
| 2010–present
|-
|}

==Election results==
{{see also|Electoral results for the district of Adelaide}}

{{Election box begin |
|title=[[South Australian state election, 2014]]: Adelaide<ref>[http://www.ecsa.sa.gov.au/elections/state-elections/past-state-election-results/7651?view=result 2014 State Election Results – Adelaide], ECSA.</ref><ref>[http://www.abc.net.au/news/sa-election-2014/guide/adel/ 2014 State Election Results – Adelaide], ABC.</ref>
}}
{{Election box candidate AU party|
|candidate = [[Rachel Sanderson]]
|party = Liberal
|votes = 10,543
|percentage = 48.7
|change = +4.2
}}
{{Election box candidate AU party|
|candidate = [[David O'Loughlin (Australian politician)|David O'Loughlin]]
|party = Labor
|votes = 7,812
|percentage = 36.1
|change = +2.7
}}
{{Election box candidate AU party|
|candidate = [[Robert Simms (Australian politician)|Robert Simms]]
|party = Greens
|votes = 2,551
|percentage = 11.8
|change = +0.1
}}
{{Election box candidate AU party|
|candidate = Anna Tree
|party = Dignity for Disability
|votes = 748
|percentage = 3.5
|change = +1.6
}}
{{Election box formal|
|votes = 21,654
|percentage = 98.0
|change = +1.8
}}
{{Election box informal|
|votes = 433
|percentage = 2.0
|change = −1.8
}}
{{Election box turnout|
|votes = 22,087
|percentage = 89.1
|change = −0.9
}}
{{Election box 2pp}}
{{Election box candidate AU party|
|candidate = [[Rachel Sanderson]]
|party = Liberal
|votes = 11,341
|percentage = 52.4
|change = −1.8
}}
{{Election box candidate AU party|
|candidate = [[David O'Loughlin (Australian politician)|David O'Loughlin]]
|party = Labor
|votes = 10,313
|percentage = 47.6
|change = +1.8
}}
{{Election box hold AU party|
|winner = Liberal
|swing = −1.8
}}
{{Election box end}}

==Notes==
{{Reflist}}

==References==
* [http://www.abc.net.au/news/sa-election-2014/guide/adel/ ABC profile for Adelaide: 2014]
* [http://www.ecsa.sa.gov.au/component/edocman/?view=document&id=536 ECSA profile for Adelaide: 2014]
* [http://blogs.crikey.com.au/pollbludger/sa2014-adelaide Poll Bludger profile for Adelaide: 2014]

==External links==
*[http://trove.nla.gov.au/ndp/del/article/87816882 The 13 electorates from 1902 to 1915: The Adelaide Chronicle]

{{Electoral districts of South Australia       |state=expanded}}
{{Former electoral districts of South Australia |state=collapsed}}

{{DEFAULTSORT:Adelaide}}
[[Category:Electoral districts of South Australia]]
[[Category:1938 establishments in Australia]]