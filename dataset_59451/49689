{{for|the song|In the Cage (song)}}
<!-- FAIR USE of In the Cage.JPG: see image description page at http://en.wikipedia.org/wiki/Image:In the Cage.JPG for rationale -->
{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = In the Cage
| image          = File:In The Cage (London, Duckworth, 1898) binding cover.jpg
| caption        = First UK edition
| author         = [[Henry James]]
| country        = United Kingdom, United States
| language       = English
| genre          = 
| publisher      = [[Gerald Duckworth and Company|Duckworth]], London<br>Herbert S. Stone & Company, Chicago
| release_date   = Duckworth: 8-Aug-1898<br>Stone: 26-Sept-1898
| media_type     = Print
| pages          = Duckworth: 187<br>Stone: 229
}}

'''''In the Cage''''' is a [[novella]] by [[Henry James]], first published as a book in 1898. This long story centers on an unnamed [[London]] [[telegraphy|telegraphist]]. She deciphers clues to her clients' personal lives from the often cryptic telegrams they submit to her as she sits in the "cage" at the post office. Sensitive and intelligent, the telegraphist eventually finds out more than she may want to know.

== Plot summary ==

An unnamed telegraphist works in the branch post office at Cocker's, a grocer in a fashionable London neighborhood. Her fiancé, a decent if unpolished man named Mr. Mudge, wants her to move to a less expensive neighborhood to save money and to be near him at all times. She refuses because she likes the glimpses of society life she gets from the telegrams at her current location.

Through those telegrams, she gets "involved" with a pair of lovers named Captain Everard and Lady Bradeen. By remembering certain code numbers in the telegrams, she manages to reassure Everard at a particular crisis that their secrets are safe from detection. Later she learns from her friend Mrs. Jordan that Lady Bradeen and Everard are getting married after the recent death of Lord Bradeen. The unnamed telegraphist also learns that Everard is heavily in debt and that Lady Bradeen is forcing him to marry her, as Everard is really not interested in her. The telegraphist finally decides to marry Mudge and reflects on the unusual events of which she was a part.

== Key themes ==
James frequently sent telegrams (over a hundred are still extant) and he got the idea for this clever tale from his experiences at the telegraphist's office. The unnamed protagonist of ''In the Cage'' is actually a typical Jamesian artist, constructing a complex finished work from the slightest hints. Her knack of deducing the details of her customers' lives from their brief, cryptic telegrams is similar to James' ability to invent stories from the tiniest suggestions - an ability he often discussed in the ''[[New York Edition]]'' prefaces.

Though the telegraphist lives vicariously through her customers to some extent, she is not presented as voyeuristic or abnormal. She never exposes any of her customers' secrets, and her final decision to marry her unexciting but reliable fiancé shows maturity and common sense. She keeps her active imagination under reasonable control, as James himself did. The story does a fine job of filling in the details of the telegraphist's workaday world. Her sometimes difficult family life is also described well.

== Critical evaluation ==
Critics have generally been very kind to this relatively little-known story. The detailed and convincing portrait of the telegraphist has garnered much praise. More politically-inclined critics have appreciated James's ability to present a working-class woman with sympathy and accuracy.

Some have compared the story to ''[[The Turn of the Screw]]'', published just before ''In the Cage''. The unnamed protagonists of both stories do display active imaginations, but the telegraphist seems much better grounded in reality. At least critics do not ask whether she has imagined Captain Everard and Lady Bradeen. A few critics have amused themselves by trying to guess exactly what the telegraphist deciphered from the telegrams between Captain Everard and Lady Bradeen. James himself said that he didn't know and he didn't want to know.

The young lady has read perhaps a few too many ha'penny novels, has a lively imagination and a nearly photographic memory. Her decision to marry her ordinary young  man—as soon as possible—is a revolt against her discovery that the necessary "hero" and "heroine" she has created from their telegrams-aren't that at all. Although James sees the telegraphist as a member of her class, surely, the story is not one of class conflict. It is not that she believes all young, wealthy men and women are good, only that, well, ''her'' wealthy young man and women must surely be. Unfortunately, they prove to be more real than wonderful.

== References ==
* ''Tales of Henry James: The Texts of the Tales, the Author on His Craft, Criticism'' edited by Christof Wegelin and Henry Wonham (New York: W.W. Norton & Company, 2003) ISBN 0-393-97710-2
* ''The Tales of Henry James'' by [[Edward Wagenknecht]] (New York: Frederick Ungar Publishing Co., 1984) ISBN 0-8044-2957-X

==External links==
*{{Wikisource-inline}}
*{{Commonscat-inline}}
* [http://www2.newpaltz.edu/~hathawar/inthcage.html The ''New York Edition'' text of ''In the Cage'', including the author's preface (1908)]
* [http://www.loa.org/volume.jsp?RequestID=63&section=notes Library of America note on the various texts of ''In the Cage'']
* {{librivox book | title=In the Cage | author=Henry JAMES}}

[[Category:1898 novels]]
[[Category:Novels by Henry James]]
[[Category:American novellas]]
[[Category:Gerald Duckworth and Company books]]
[[Category:19th-century American novels]]
[[Category:19th-century British novels]]