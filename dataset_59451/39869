{{EngvarB|date=October 2013}}
{{Use dmy dates|date=October 2013}}
{{Infobox military person
|name= Geoffrey Forrest Hughes
|image= File:Geoffrey Hughes Duke of York.jpg
|size= 250px
|alt= Photograph of two men in formal military uniform. The man on the left is a naval officer's uniform, and has a cap, white gloves, a ceremonial sword and has medal ribbons on his chest. The man on the right is also in a military officer's uniform, and is wearing a cap, leather gloves and medals. He is carrying a ceremonial sword in his right hand. Other men in military uniform can be seen in the background.
|caption= Geoffrey Hughes (right) speaking with [[George VI of the United Kingdom|Prince Albert]] during the latter's tour of Australia in 1927.
|birth_date= 12 July 1895
|death_date= {{Death date and age|1951|09|13|1895|07|12|df=yes}}
|birth_place= [[Darling Point, New South Wales]]
|death_place= [[Lewisham, New South Wales]]
|placeofburial= [[Waverley Cemetery]]
|nickname= 
|allegiance= {{flagu|Australia}}<br />{{flagu|United Kingdom}}
|branch= [[Australian Army]]<br />[[Royal Flying Corps]]<br />[[Royal Australian Air Force]]
|serviceyears= 1914–1919<br />1940–1943
|rank= [[Group Captain]]
|unit= 
|commands= 
|battles= [[First World War]]<br />[[Second World War]]
|awards= [[Military Cross]]<br />[[Air Force Cross (United Kingdom)|Air Force Cross]]<br />[[Mentioned in Despatches]] (2)
|relations= [[Tom Hughes (Australian politician)|Tom Hughes]] (son)<br />[[Robert Hughes (critic)|Robert Hughes]] (son)
|laterwork= 
}}

'''Geoffrey Forrest Hughes''' {{postnominals|country=AUS|size=100%|sep=,|MC|AFC}} (12 July 1895 – 13 September 1951) was an Australian aviator and [[flying ace]] of the [[First World War]]. He was credited with 11 aerial victories, and won a [[Military Cross]] for his valour. After a postwar award of the [[Air Force Cross (United Kingdom)|Air Force Cross]], he returned to Australia and completed university. He became a businessman and a solicitor in the family law firm while retaining his interests in aviation. From 1925 through 1934, he was president of the Royal Australian Aero Club, and largely responsible for government support of the club. Despite his business concerns, he returned to military duty during the Second World War. He commanded an aviation training school and rose to the rank of [[group captain]] before surrendering his commission in April 1943. After the war ended, he moved into public life and the political realm.

==Early life==
Geoffrey Forrest Hughes was born in the [[Sydney]] suburb of [[Darling Point, New South Wales|Darling Point]] on 12 July 1895.<ref name="Aces">{{Harvnb|Garrisson|1999|p=91}}</ref> He was the second son of [[Thomas Hughes (Sydney mayor)|Thomas Hughes]], a solicitor and future [[List of Mayors and Lord Mayors of Sydney|Lord Mayor of Sydney]], and Louisa (née Gilhooley); he was of Irish descent on both sides, with roots in [[County Roscommon]].<ref name="ADB">{{Australian Dictionary of Biography|last=Spearritt|first=Peter|year=1983|id=A090393b|title=Hughes, Geoffrey Forrest (1895–1951)|accessdate=30 March 2013}}</ref> Hughes received his secondary education at [[Saint Ignatius' College, Riverview]], before undertaking a Bachelor of Arts degree at the [[University of Sydney]] from 1914. In June that year, he was [[Officer (armed forces)|commissioned]] as an officer in the 26th Infantry Regiment, [[Australian Army Reserve|Citizens Military Force]]. In his youth, Hughes had acquired a keen interest in aviation, which led him to apply for the [[Royal Australian Air Force|Australian Flying Corps]]; his application was unsuccessful.<ref name="ADB"/>

==First World War==
In March 1916, Hughes suspended his studies and travelled to the United Kingdom, where he enlisted in the [[Royal Flying Corps]] and was commissioned a [[second lieutenant]] on 3 June. He was posted for flight instruction, and on graduating as a pilot was made a flying officer on 28 July. Later that year, Hughes was posted to [[No. 10 Squadron RAF|No.&nbsp;10 Squadron RFC]] in France.<ref name="ADB"/> Piloting [[Armstrong Whitworth F.K.8]] biplanes, the unit carried out co-operation duties with the [[Allies of World War I|Allied]] ground troops over the [[Western Front (World War I)|Western Front]].<ref>{{cite web|url=http://www.raf.mod.uk/bombercommand/h10.html |title=No.&nbsp;10 Squadron |accessdate=30 March 2013 |work=Bomber Command |publisher=Royal Air Force |deadurl=yes |archiveurl=https://web.archive.org/web/20130118042939/http://www.raf.mod.uk:80/bombercommand/h10.html |archivedate=18 January 2013 |df=dmy }}</ref> Around the same time Hughes had enlisted in the Royal Flying Corps, his elder brother, Roger Forrest Hughes, was granted a commission in the [[Australian Army Medical Corps]] of the [[First Australian Imperial Force|Australian Imperial Force]]. Posted for service on the Western Front, he was mortally wounded by an artillery shell in the morning of 11 December 1916; Geoffrey was with Roger when the latter died of his wounds later that day.<ref>{{cite web|url=http://www.aif.adfa.edu.au:8080/showPerson?pid=146225 |title=Roger Forrest Hughes |accessdate=2010 |work=The AIF Project |publisher=[[Australian Defence Force Academy]] |deadurl=yes |archiveurl=https://web.archive.org/web/20120227063502/http://www.aif.adfa.edu.au:8080/showPerson?pid=146225 |archivedate=27 February 2012 |df=dmy }}</ref><ref>{{cite web|url=http://cas.awm.gov.au/item/P08188.001|title=Captain Roger Forrest Hughes (P08188.001)|accessdate=30 March 2013|work=Collections search|publisher=[[Australian War Memorial]]}}</ref>

On completing his tour with No.&nbsp;10 Squadron, Hughes returned to the United Kingdom in February 1917. For the following ten months, he was posted for duties at flight training installations and units in England. He was promoted to temporary [[Captain (British Army and Royal Marines)|captain]] on 29 July.<ref name="FACrecord">{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=8230863&queryType=1&resultcount=2|title=RAF officers' service records 1918–1919—Image details|work=DocumentsOnline|publisher=[[The National Archives (United Kingdom)|The National Archives]]|format=fee usually required to view pdf of full original service record|accessdate=2010}}</ref> During this time, his letters home to his parents were critical of anti-conscription efforts by Australian Roman Catholics, led by Archbishop Mannix.<ref name="ADB"/>

In 1918, he returned to France in an assignment to fly a [[Bristol F.2 Fighter]] for [[No. 62 Squadron RAF|No. 62 Squadron RFC]]. On 17 February, he and [[Hugh Claye]] were first into combat for the new squadron; four days later, they scored the unit's first victory. The team of Hughes and Claye continued to accrue victories; they became aces in a notable air battle on 13 March 1918. They were leading a formation of 11 Bristol Fighters when they were sucked into a lopsided battle to save a squadronmate. Despite bucking combat odds of about four to one, Hughes and Claye were credited with two of 62 Squadron's six victories that day.<ref>''Bristol F.2 Fighter Aces'', p. 59.</ref> Hughes was promoted to captain on 1 April 1918. He was [[mentioned in despatches]] twice, and awarded the [[Military Cross]],<ref name="ADB"/> which was [[gazetted]] on 13 May 1918:

{{quote|...While leading his formation over the enemy's lines he was attacked by twelve enemy machines, two of which he shot down. On the following day, when in charge of a patrol, he attacked seven enemy triplanes, drove down one out of control, and forced three others to land. On another occasion, while in charge of a patrol, he was attacked by a large number of enemy scouts; owing to his skilful flying his observer succeeded in shooting down one of the enemy machines, which broke up in the air. He always showed the greatest coolness and courage in action, and, as a flight commander, led his formation with splendid courage and determination.<ref>''Supplement to the London Gazette'', 13 May 1918, pp. 5696, 5700.</ref>}}

Hughes was again withdrawn to training duties in England.<ref name="ADB"/>

==List of aerial victories==
See also [[Aerial victory standards of World War I]]

Unconfirmed victories denoted "u/c".
{| class="wikitable" border="1" style="margin: 1em auto 1em auto"
|-
!No.
!Date/time
!Aircraft
!Foe
!Result
!Location
!Notes
|-
| align="center"| 1
| align="center"| 21 February 1918
| [[Bristol F.2 Fighter|Bristol F.2b Fighter]]
| German two-seater
| Destroyed
| Vicinity of [[Armentières]], France
| Observer/gunner: [[Hugh Claye]]
|-
| align="center"| 2
| align="center"| 10 March 1918
| Bristol F.2b Fighter
| [[Albatros D.V]]
| Destroyed
|
| Observer/gunner: Hugh Claye
|-
| align="center"| 3
| align="center"| 10 March 1918
| Bristol F.2b Fighter
| Albatros D.V
| Driven down out of control
|
| Observer/gunner: Hugh Claye
|-
| align="center"| 4
| align="center"| 11 March 1918
| Bristol F.2b Fighter
| [[Fokker Dr.I|Fokker Triplane]]
| Driven down out of control
|
| Observer/gunner: Hugh Claye
|-
| align="center"| u/c
| align="center"| 11 March 1918
| Bristol F.2b Fighter
| Fokker Triplane
| Forced to land
|
| Observer/gunner: Hugh Claye
|-
| align="center"| u/c
| align="center"| 11 March 1918
| Bristol F.2b Fighter
| Fokker Triplane
| Forced to land
|
| Observer/gunner: Hugh Claye
|-
| align="center"| u/c
| align="center"| 11 March 1918
| Bristol F.2b Fighter
| Fokker Triplane
| Forced to land
|
| Observer/gunner: Hugh Claye
|-
| align="center"| 5
| align="center"| 13 March 1918 @ 1030 hours
| Bristol F.2b Fighter
| [[Pfalz D.III]]
| Driven down out of control; pilot hit
| East of [[Cambrai]], France
| Observer/gunner: Hugh Claye
|-
| align="center"| 6
| align="center"| 13 March 1918 @ 1030 hours
| Bristol F.2b Fighter
| Fokker Triplane
| Destroyed
| East of Cambrai, France
| Observer/gunner: Hugh Claye; victory over [[Lothar von Richthofen]] shared with Augustus Orlebar in a [[Sopwith Camel]]
|-
| align="center"| 7
| align="center"| 12 April 1918 @ 1420 hours
| Bristol F.2b Fighter
| Albatros D.V
| Driven down out of control
| South of Bois du Biez
| Observer/gunner: Hugh Claye
|-
| align="center"| 8
| align="center"| 12 April 1918 @ 1500 hours
| Bristol F.2b Fighter
| [[LVG]] two-seater
| Set afire in midair; destroyed
| [[Auchy-les-Mines]], France
| Observer/gunner: Hugh Claye
|-
| align="center"| 9
| align="center"| 22 April 1918 @ 1030 hours
| Bristol F.2b Fighter
| [[Fokker D.VII]]
| Driven down out of control
| [[Nieppe]] Forest
| Observer/gunner: Hugh Claye
|-
| align="center"| 10
| align="center"| 22 April 1918 @ 1030 hours
| Bristol F.2b Fighter
| Fokker D.VII
| Driven down out of control
| Nieppe Forest
| Observer/gunner: Hugh Claye
|-
| align="center"| 11
| align="center"| 10 May 1918 @ 1835 hours
| Bristol F.2b Fighter
| [[Rumpler]] two-seater
| Driven down out of control
| Between [[Combles]] and [[Péronne, Somme|Péronne]], France
| Observer/gunner: Hugh Claye<ref>''Above the Trenches'', p. 204.</ref>
|-
|}

==Between the World Wars==
Hughes remained in England for some time following war's end, training recruits.<ref name="ADB"/> On 3 June 1919, his award of the [[Air Force Cross (United Kingdom)|Air Force Cross]] was gazetted.<ref>''Supplement to the London Gazette'', 3 June 1919, pp. 7032, 7033.</ref>

After discharge from military duty, Hughes completed his Bachelor of Arts at the University of Sydney, graduating in 1920.<ref name="ADB"/> He married Margaret Sealy Vidal, an Anglican and the daughter of an English cleric, at Saint Canice's Church, [[Darlinghurst]] on 8 January 1923.<ref>''The Sydney Morning Herald'', 9 January 1923, p. 5.</ref> Hughes went on to receive a [[Bachelor of Laws]] degree with Second Class honours on 17 May 1923.<ref>''The Sydney Morning Herald'', 18 May 1923, p. 5.</ref> He became a solicitor and joined the family legal firm.<ref name="ADB"/> His son, [[Robert Hughes (critic)|Robert]] (1938–2012), was an influential art critic.<ref>''The Australian'', ''Roads to Rome'' by [[Peter Craven]] on 2 July 2011.</ref>

Geoffrey Hughes maintained his interest in aviation, becoming president of the Royal Aero Club of New South Wales from 1925 through to 1934. He was instrumental in gaining government support for the club, on grounds that it would supply pilots for military as well as civil use.<ref name="ADB"/> He was one of three members of a Committee of Inquiry into the forced landing of the [[Kookaburra (aeroplane)|Kookaburra]] during a long distance flight on 29 March 1929. The committee's report, issued on 25 June 1929, besides exploring causes of the accident, also contained recommendations for better radio communications and sufficient onboard emergency rations for crew survival in future mishaps.<ref>''The Mercury (Hobart, Tasmania)'', 25 June 1929, p. 7.</ref> On 5 September 1936, it was reported by ''[[The Sydney Morning Herald]]'' that Hughes became a director of the United Insurance Co Limited.<ref>''The Sydney Morning Herald,'' 5 September 1936, p. 19.</ref>

==Second World War and beyond==
In July 1940, Hughes returned to military service, being appointed as a flying officer in the [[Royal Australian Air Force]].<ref name="ADB"/> On 13 August 1940, he was appointed to fill a vacant seat on the board of directors of [[Commercial Banking Company of Sydney]].<ref>''The Courier-Mail (Brisbane Queensland)'', 2 August 1940, p. 4.</ref> Ranked as a temporary [[wing commander (rank)|wing commander]] in 1941, he commanded the flying school at [[Narrandera]]. By the time he gave up his commission in April 1943 to end his military career, he had risen to acting [[group captain]].<ref name="ADB"/>

==Business and public career==
Hughes became a prominent businessman, with connections to companies in which his father had an interest. The younger Hughes became a director of United Insurance Company, Commercial Banking Company of Sydney, and Australia Hotel Company Ltd, as well as chairman of Tooheys Ltd and Tooheys Standard Securities Ltd.<ref name="ADB"/> He was chairman of directors for Toohey's Brewery, as well as [[senior partner]] in the family law firm of Hughes, Hughes, and Garvin.<ref>''The Sydney Morning Herald'', 14 September 1951, p. 5.</ref> His business interests led him into political life as an opponent of the bank nationalisation polices of postwar Prime Minister [[Ben Chifley]]. Hughes refused to serve on the board of [[Qantas|Qantas Empire Airways Ltd]].<ref name="ADB"/> Elsewhere in the sphere of public life, he became a council member of [[Sancta Sophia College]]. He became a member of the socially prominent [[Australian Club]], as well as the Royal Sydney Golf Club.<ref name="ADB"/>

==Death==
Geoffrey Forrest Hughes died of lung cancer and [[pneumonia]] in [[Lewisham, New South Wales#Notable residents|Lewisham Hospital]] on 13 September 1951, at age 56. His wife, daughter, and three sons survived him. He was buried in the Catholic section of [[Waverley Cemetery]].<ref name="ADB"/>

==Notes==
{{reflist|30em}}

==References==
* {{cite book|last=Dempsey|first=Harry|last2=Guttman|first2=Jon|title=Bristol F2 Fighter Aces of World War I|series=Osprey Aircraft of the Aces|year=2007|publisher=Osprey Publishing|location=Oxford, England|isbn=9781846032011}}
* {{cite book|last=Garrisson|first=A.D.|title=Australian Fighter Aces 1914–1953|url=http://airpower.airforce.gov.au/Publications/Details/220/Australian-Fighter-Aces-1914–1953.aspx|publisher=Air Power Studies Centre|location=Fairbairn, Australia|year=1999|format=PDF|accessdate=30 March 2013|isbn=0 642 26540 2|ref=harv}}
* {{cite book|last=Shores|first=Christopher|last2=Franks|first2=Norman|last3=Guest|first3=Russell|title=Above the Trenches: A Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920|year=1990|publisher=Grub Street|location=|isbn=0-948817-19-4}}

{{DEFAULTSORT:Hughes, Geoffrey Forrest}}
[[Category:1895 births]]
[[Category:1951 deaths]]
[[Category:Australian Army officers]]
[[Category:Australian aviators]]
[[Category:Australian military personnel of World War II]]
[[Category:Australian people of Irish descent]]
[[Category:Australian Roman Catholics]]
[[Category:Australian World War I flying aces]]
[[Category:British military personnel of World War I]]
[[Category:Deaths from cancer in New South Wales]]
[[Category:Deaths from lung cancer]]
[[Category:Deaths from pneumonia]]
[[Category:Infectious disease deaths in New South Wales]]
[[Category:People from Sydney]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Recipients of the Military Cross]]
[[Category:Royal Air Force officers]]
[[Category:Royal Australian Air Force officers]]
[[Category:Royal Flying Corps officers]]
[[Category:University of Sydney alumni]]