{{About|the ocean sunfish, ''Mola mola''|other fishes known as "sunfish"|Sunfish (disambiguation)}}
{{pp-move-indef}}
{{Taxobox
| name = Ocean sunfish
| image = Sunfish2.jpg
| image_width = 250px
| image_caption =
<!-- | status = [[Not Evaluated]] -->
| status = VU
| status_system = IUCN3.1
| status_ref= <ref>{{cite web| url= http://www.iucnredlist.org/details/190422/0 |title= mola mola | publisher =ICUN  |accessdate=2015-11-29}}</ref><ref>{{cite news| url= http://www.japantimes.co.jp/news/2015/11/19/national/iucn-red-list-of-threatened-species-includes-ocean-sunfish/ |title= IUCN Red List of threatened species includes ocean sunfish |author= Kyodo |publisher= The Japan Times |date=19 November 2015 |accessdate=2015-11-29}}</ref>
| regnum = [[Animalia]]
| phylum = [[Chordata]]
| classis = [[Actinopterygii]]
| ordo = [[Tetraodontiformes]]
| familia = [[Molidae]]
| genus = ''[[Mola (genus)|Mola]]''
| species = '''''M. mola'''''
| binomial = ''Mola mola''
| binomial_authority = ([[Carl Linnaeus|Linnaeus]], [[10th edition of Systema Naturae|1758]])
}}

The '''ocean sunfish''' or '''common mola''' (''Mola mola'') is the heaviest known [[Osteichthyes|bony fish]] in the world. Adults typically weigh between {{convert|247|and(-)|1000|kg|lb|abbr=on}}. The [[species]] is native to [[Tropical fish|tropical]] and [[temperate]] waters around the globe. It resembles a fish head with a tail, and its main body is flattened laterally. Sunfish can be as tall as they are long when their [[Dorsal fin|dorsal]] and ventral [[Pelvic fins|fins]] are extended.

Sunfish live on a diet consisting mainly of [[jellyfish]], but because this diet is nutritionally poor, they consume large amounts to develop and maintain their great bulk. Females of the species can produce more [[Egg (biology)|eggs]] than any other known [[vertebrate]],<ref name="OceanSunfishLifeHistory"/> up to 300,000,000 at a time.<ref>Freedman, J. A., & Noakes, D. L. G. (2002) Why are there no really big bony fishes? A point-of-view on maximum body size in teleosts and elasmobranchs. ''Reviews in Fish Biology and Fisheries.'' '''12'''(4): 403-416.</ref> Sunfish [[Fry (biology)|fry]] resemble miniature [[pufferfish]], with large pectoral fins, a tail fin, and body spines uncharacteristic of adult sunfish.

Adult sunfish are vulnerable to few natural predators, but [[sea lion]]s, [[killer whale]]s, and [[shark]]s will consume them. Among humans, sunfish are considered a delicacy in some parts of the world, including [[Japan]], [[Korea]], and [[Taiwan]]. In the [[EU]], regulations ban the sale of fish and fishery products derived from the family [[Molidae]].<ref name="BannedinEEC">{{cite web|url=http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:32004R0853R(01):EN:NOT |title=Regulation (EC) No 853/2004 of the European Parliament and of the Council of 29 April 2004 laying down specific hygiene rules for food of animal origin |publisher=Eur-lex.europa.eu |accessdate=2010-11-16}}</ref> Sunfish are frequently caught in [[gillnet]]s.

A member of the [[order (biology)|order]] [[Tetraodontiformes]], which also includes [[pufferfish]], [[porcupinefish]], and [[filefish]], the sunfish shares many traits common to members of this order. The ocean sunfish, ''Mola mola'', is the [[type species]] of the [[genus]].
__TOC__
{{clear}}

==Naming and taxonomy==
[[File:Mola mola2.jpg|thumb|255px|right|The ocean sunfish is the heaviest of all [[bony fish]]es. It has a flattened body and is as tall as it is long. It feeds mainly on [[jellyfish]].]]

Many of the sunfish's various names allude to its flattened shape. Its specific name, ''mola'', is Latin for "millstone", which the fish resembles because of its gray color, rough texture, and rounded body. Its common English name, sunfish, refers to the animal's habit of sunbathing at the surface of the water. The Dutch-, Portuguese-, French-, Catalan-, Spanish-, Italian-, Russian-, Greek- and German-language names, respectively ''maanvis'', ''peixe lua'', ''poisson lune'', ''peix lluna'', ''pez luna'', ''pesce luna'', ''рыба-луна'', ''φεγγαρόψαρο'' and ''Mondfisch'', mean "moon fish", in reference to its rounded shape. In German, the fish is also known as ''Schwimmender Kopf'', or "swimming head". In Polish, it is named ''samogłów'', meaning "head alone", because it has no true tail. The Chinese translation of its academic name is ''fan-che yu'' 翻車魚, meaning "toppled wheel fish".
The ocean sunfish has various superseded [[binomial nomenclature|binomial]] synonyms, and was originally classified in the pufferfish genus, as ''[[Tetraodon]] mola''.<ref name="CatalogOfFishes">{{FishBase genus
|genus=Mola
|month=June
|year=2007}}</ref><ref name="AnnotatedChecklist">{{cite journal
|last=Parenti
|first=Paolo
|date=September 2003
|title=Family Molidae Bonaparte 1832: molas or ocean sunfishes
|journal=Annotated Checklist of Fishes (electronic journal)
|volume=18
|issue=
|pages=
|issn=1545-150X
|url=http://research.calacademy.org/sites/research.calacademy.org/files/Departments/ichthyology/Molidae.pdf
|format=PDF
|accessdate=2012-02-06
}}</ref>
It is now placed in its own [[genus]], ''Mola'', with two [[species]]: ''Mola mola'' and ''[[Mola ramsayi]]''. The ocean sunfish, ''Mola mola'', is the [[type species]] of the genus.<ref name="EvolutionSunfishPaper">{{cite journal
|last=Bass
|first=L. Anna
|author2=Heidi Dewar|author3= [[Tierney Thys]]|author4= J. Todd. Streelman|author5= Stephen A. Karl
|date=July 2005
|title=Evolutionary divergence among lineages of the ocean sunfish family, Molidae (Tetraodontiformes)
|journal=Marine Biology
|volume=148
|issue=2
|pages=405–414
|url=http://OceanSunfish.org/MolageneticsMarBio05.pdf
|format=PDF
|accessdate=2007-06-26
|doi=10.1007/s00227-005-0089-z
}}</ref>

The genus ''Mola'' belongs to the [[family (biology)|family]] [[Molidae]]. This family comprises three genera: ''[[Masturus]]'', ''[[Mola (genus)|Mola]]'' and ''[[Slender sunfish|Ranzania]]''. The common name "sunfish" without qualifier is used to describe the [[seawater|marine]] family Molidae as well as the [[freshwater]] sunfishes in the family [[Centrarchidae]] which are unrelated to Molidae. On the other hand, the name "ocean sunfish" and "mola" refer only to the family Molidae.<ref name="OceanSunfishLifeHistory"/>

The family Molidae belongs to the [[order (biology)|order]] [[Tetraodontiformes]], which includes [[pufferfish]], [[porcupinefish]], and [[filefish]]. It shares many traits common to members of this order, including the four fused teeth that form the characteristic beak and give the order its name (''tetra''=four, ''odous''=tooth, and ''forma''=shape). Indeed, sunfish fry resemble spiky pufferfish more than they resemble adult molas.<ref name="OceanSunfishEvolution">{{cite web |url=http://OceanSunfish.org/evolution.html
|title=Molidae information and research (Evolution)
|first=Tierney
|last=Thys
|publisher=OceanSunfish.org
|accessdate=2007-06-26}}</ref>

==Description==
{{multiple image
 | align = right
 | direction = vertical
 | width     = 200
 | image1    = Molalavdj.jpg
 | alt1      = 
 | caption1  = A sunfish fry, which still possesses spines that will later disappear
 | image2    = Mola mola-Skelett, Naturhistorisches Museum Wien.jpg
 | alt2      = 
 | caption2  = A skeleton, showing the structure of the fins
}}

The [[caudal fin]] of the ocean sunfish is replaced by a rounded clavus, creating the body's distinct truncated shape. The body is flattened laterally, giving it a long oval shape when seen head-on. The pectoral fins are small and fan-shaped, while the dorsal fin and the anal fin are lengthened, often making the fish as tall as it is long. Specimens up to {{convert|3.2|m|ft|1|abbr=on}} in height have been recorded.<ref name="NZHeraldVisitor"/>

The mature ocean sunfish has an average length of {{convert|1.8|m|ft|1|abbr=on}} and a fin-to-fin length of {{convert|2.5|m|ft|1|abbr=on}}. The weight of mature specimens can range from {{convert|247|to|1000|kg|lb|abbr=on}},<ref name="OceanSunfishLifeHistory">{{cite web
|url=http://www.oceansunfish.org/lifehistory.html
|title=Molidae Descriptions and Life History
|first=Tierney
|last=Thys
|publisher=OceanSunfish.org
|accessdate=2007-05-08}}</ref><ref>Watanabe, Y., & Sato, K. (2008). ''Functional dorsoventral symmetry in relation to lift-based swimming in the ocean sunfish Mola mola''. PLoS One, 3(10), e3446.</ref><ref>Nakatsubo, T., Kawachi, M., Mano, N., & Hirose, H. (2007). ''Estimation of maturation in wild and captive ocean sunfish Mola mola''. Aquaculture Science, 55.</ref> but even larger individuals are not unheard of. The maximum size is up to {{Convert|3.3|m|ft|1|abbr=on}} in length<ref name="NZHeraldVisitor">{{cite web
|url=http://www.nzherald.co.nz/section/story.cfm?c_id=1&objectid=10412167
|title=Tropical sunfish visitor as big as a car
|date=November 24, 2006
|author=Juliet Rowan
|publisher=The New Zealand Herald
|accessdate=2007-05-08}}</ref>
{{convert|4.2|m|ft|abbr=on}} across the fins<ref name= Wood>Wood, The Guinness Book of Animal Facts and Feats. Sterling Pub Co Inc (1983), ISBN 978-0-85112-235-9</ref> and up to {{convert|2300|kg|lb|abbr=on}} in mass.<ref name="fishbase">{{FishBase species
|genus=Mola
|species=mola
|year=2006
|month=March}}</ref>

The spinal column of ''M. mola'' contains fewer [[vertebra]]e and is shorter in relation to the body than that of any other fish.<ref name="LargePelagicsLifeHistory">{{cite web
|url=http://www.tunalab.unh.edu/molalifehistory.htm
|title=Mola mola program - Life History
|publisher=Large Pelagics Research Lab
|archiveurl=https://web.archive.org/web/20110819181804/http://www.tunalab.unh.edu/molalifehistory.htm
|archivedate=2011-08-19}}</ref>
Although the sunfish descended from [[Osteichthyes|bony ancestors]], its skeleton contains largely [[cartilage|cartilaginous]] tissues, which are lighter than [[bone]], allowing it to grow to sizes impractical for other bony fishes.<ref name="LargePelagicsLifeHistory"/><ref>{{cite web
|url=http://www.naturalhistorymag.com/0307/0307_biomechanics.html
|title=No Bones About ’Em
|publisher=Natural History Magazine
|author=Adam Summers
|accessdate=2007-06-30
|archiveurl=https://web.archive.org/web/20070927174327/http://www.naturalhistorymag.com/0307/0307_biomechanics.html
|archivedate=September 27, 2007}}</ref> Its teeth are fused into a beak-like structure,<ref name="fishbase"/> and it has [[pharyngeal teeth]] located in the throat.<ref>{{cite book|author1=Bone, Quentin |author2=Moore, Richard |year=2008|title=Biology of Fishes|publisher=Taylor & Francis US|page=210|isbn=0203885228}}</ref>

The sunfish lacks a [[swim bladder]].<ref name="LargePelagicsLifeHistory"/> Some sources indicate the internal organs contain a concentrated [[neurotoxin]], [[tetrodotoxin]], like the organs of other poisonous tetraodontiformes,<ref name="fishbase"/> while others dispute this claim.<ref name="OceanSunfishResearch"/>

===Fins===
In the course of its evolution, the caudal fin (tail) of the sunfish disappeared, to be replaced by a lumpy pseudotail, the clavus. This structure is formed by the convergence of the dorsal and anal fins,<ref name="StrangeTail">{{cite web
|url=http://www.nhm.ac.uk/about-us/news/2006/feb/news_7758.html
|title=Strange tail of the sunfish
|publisher=The Natural History Museum
|accessdate=2007-05-11}}</ref><ref name="Leis'Conundrum">{{cite journal
|last=Johnson
|first=G. David
|author2=Ralf Britz
|date=October 2005
|title=Leis' Conundrum: Homology of the Clavus of the Ocean Sunfishes. 2. Ontogeny of the Median Fins and Axial Skeleton of ''Ranzania laevis'' (Teleostei, Tetraodontiformes, Molidae)
|journal=Journal of Morphology
|volume=266
|issue=1
|pages=11–21 |url=http://www3.interscience.wiley.com/user/accessdenied?ID=109795665&Act=2138&Code=4717&Page=/cgi-bin/fulltext/109795665/PDFSTART |format=PDF (fee required)
|accessdate=2007-06-11
|quote=We thus conclude that the molid clavus is unequivocally formed by modified elements of the dorsal and anal fin and that the caudal fin is lost in molids.
|doi=10.1002/jmor.10242
|pmid=15549687}}</ref> and is used by the fish as a rudder.<ref name="OnlineFieldGuide">{{cite web
|url=http://www.montereybayaquarium.org/animal-guide/fishes/ocean-sunfish
|title=Ocean sunfish
|publisher=Monterey Bay Aquarium
|accessdate=2015-11-26}}</ref> The smooth-denticled clavus retains 12 [[fin ray]]s, and terminates in a number of rounded ossicles.<ref name="AustMuseumAdult">{{cite web |url=http://AustralianMuseum.net.au/Ocean-Sunfish-Mola-mola/
|title=Ocean Sunfish, Mola mola
|first=Mark
|last=McGrouther
|date=6 April 2011
|publisher=Australian Museum Online
|accessdate=2012-02-06}}</ref>

Ocean sunfish often swim near the surface, and their protruding dorsal fins are sometimes mistaken for those of [[shark]]s.<ref name="NationalGeo"/> However, the two can be distinguished by the motion of the fin. Sharks, like most fish, swim by moving the tail sideways while keeping the dorsal fin stationary. The sunfish, though, swings its dorsal fin and anal fin in a characteristic [[sculling]] motion.<ref name="UnderwaterPioneer"/>

===Skin===
Adult sunfish range from brown to silvery-grey or white, with a variety of mottled skin patterns; some of these patterns may be region-specific.<ref name="OceanSunfishLifeHistory"/> Coloration is often darker on the dorsal surface, fading to a lighter shade ventrally as a form of countershading camouflage. ''M. mola'' also exhibits the ability to vary skin coloration from light to dark, especially when under attack.<ref name="OceanSunfishLifeHistory"/> The skin, which contains large amounts of reticulated collagen, can be up to {{Convert|7.3|cm|abbr=on}} thick on the ventral surface, and is covered by denticles and a layer of [[mucus]] instead of [[Scale (zoology)|scales]]. The skin on the clavus is smoother than that on the body, where it can be as rough as sandpaper.<ref name="LargePelagicsLifeHistory"/>

More than 40 species of [[parasite]]s may reside on the skin and internally, motivating the fish to seek relief in a number of ways.<ref name="OceanSunfishLifeHistory"/><ref name="AustMuseumAdult"/> One of the most frequent ocean sunfish parasites is the flatworm, ''[[Accacoelium contortum]]''.<ref>{{Cite journal|title = Accacoelium contortum (Trematoda: Accacoeliidae) a trematode living as a monogenean: morphological and pathological implications|journal = Parasites & Vectors|date = 15 October 2015|issn = 1756-3305|pmc = 4608113|pmid = 26471059|volume = 8|doi = 10.1186/s13071-015-1162-1|first = Ana Elena|last = Ahuir-Baraja|first2 = Francesc|last2 = Padrós|first3 = Jose Francisco|last3 = Palacios-Abella|first4 = Juan Antonio|last4 = Raga|first5 = Francisco Esteban|last5 = Montero}}</ref>

In temperate regions, drifting kelp fields harbor [[cleaner fish|cleaner wrasse]]s and other fish which remove parasites from the skin of visiting sunfish. In the tropics, ''M. mola'' solicits cleaning help from reef fishes. By basking on its side at the surface, the sunfish also allows seabirds to feed on parasites from its skin. Sunfish have been reported to [[Cetacean surfacing behaviour#Breaching and lunging|breach]], clearing the surface by approximately {{Convert|3|m|ft|0|abbr=on}}, in an effort to dislodge embedded parasites.<ref name="NationalGeo">{{cite web
|url=http://www3.nationalgeographic.com/animals/fish/mola.html
|title=Mola (Sunfish)
|publisher=National Geographic
|accessdate=2007-05-08}}</ref><ref>{{cite web
|url=http://oceansunfish.org/sightings07.html
|title=Help Unravel the Mystery of the Ocean Sunfish
|first=Tierney
|last=Thys
|publisher=OceanSunfish.org
|year=2007
|accessdate=2013-05-17}}</ref>

==Range and behavior==
{{multiple image
 | align = right
 | direction = vertical
 | width     = 220
 | image1    = Mola mola.jpg
 | alt1      = 
 | caption1  = {{center|Typical swimming position}}
 | image2    = SurfacedMolaMola.jpg
 | alt2      = 
 | caption2  = {{center|Characteristic horizontal basking behavior}}
}}

Ocean sunfish are native to the [[temperate]] and [[tropical fish|tropical]] waters of every ocean in the world.<ref name="LargePelagicsLifeHistory"/> ''Mola'' [[genotype]]s appear to vary widely between the [[Atlantic Ocean|Atlantic]] and [[Pacific Ocean|Pacific]], but genetic differences between individuals in the [[Northern Hemisphere|Northern]] and [[Southern Hemisphere|Southern hemispheres]] are minimal.<ref name="TrackingSunfish"/>

Although early research suggested sunfish moved around mainly by drifting with ocean currents, individuals have been recorded swimming {{Convert|26|km|abbr=on}} in a day, at a top speed of {{Convert|3.2|km/h|abbr=on}}.<ref name="OnlineFieldGuide"/>
Sunfish are [[pelagic]] and swim at depths to {{Convert|600|m|abbr=on}}. Contrary to the perception that sunfish spend much of their time basking at the surface,  ''M. mola'' adults actually spend a large portion of their lives submerged at depths greater than {{convert|200|m|abbr=on}}, occupying both the [[epipelagic]] and [[mesopelagic]] zones.<ref name="LargePelagicsPreliminary">{{cite web
|url=http://www.tunalab.unh.edu/molaresearchresults.htm
|title=Mola mola program - Preliminary results
|date=January 2006
|publisher=Large Pelagics Research Lab
|archiveurl=https://web.archive.org/web/20110720094050/http://www.tunalab.unh.edu/molaresearchresults.htm
|archivedate=2011-07-20}}</ref>

Sunfish are most often found in water warmer than {{Convert|10|C}};<ref name="LargePelagicsPreliminary"/> prolonged periods spent in water at temperatures of {{Convert|12|C}} or lower can lead to disorientation and eventual death.<ref name="UnderwaterPioneer"/> Surface basking behavior, in which a sunfish swims on its side, presenting its largest profile to the sun, may be a method of "thermally recharging" following dives into deeper, colder water.<ref name="TrackingSunfish">
{{cite web
|url=http://www.oceansunfish.org/MontereySanctuaryv11.html
|title=Tracking Ocean Sunfish, ''Mola mola'' with Pop-Up Satellite Archival Tags in California Waters
|first=Tierney
|last=Thys
|date=30 November 2003
|publisher=OceanSunfish.org
|accessdate=2007-06-14}}</ref><ref name="Biogeography">
{{cite web
|url=http://online.sfsu.edu/bholzman/courses/Fall00Projects/Mola.html
|title= The Biogeography of Ocean Sunfish (Mola mola)
|date=Fall 2000
|publisher= San Francisco State University Department of Geography |accessdate=2008-04-25}}</ref> Sightings of the fish in colder waters outside of its usual habitat, such as those southwest of England, may be evidence of increasing marine temperatures,<ref>{{cite web
 | first      = Mark
 | last       = Oliver
 | author2    = and agencies
 | title      = Warm Cornish waters attract new marine life
 | url        = https://www.theguardian.com/conservation/story/0,,1828580,00.html
 | date       = 25 July 2006
 | publisher  = [[Guardian.co.uk]]
 | accessdate = 2007-05-08}}</ref><ref>{{cite web
 | title      = Giant sunfish washed up on Overstrand beach in Norfolk
 | url        = http://www.bbc.co.uk/news/uk-england-norfolk-20663332
 | publisher  = [[BBC News Online]]
 | date       = 2012-12-10
 | accessdate = 2012-12-10}}</ref> although the proximity of England's southwestern coast to the [[Gulf Stream]] means that many of these sightings may also be the result of the fish being carried to Europe by the current.<ref>''[http://www.glaucus.org.uk/Sunfish.htm Ocean Sunfish]'', Glaucus, British Marine Life Study Society</ref> Sunfish have been found as far north as off the northern coast of Scotland, and have been caught from areas around the United Kingdom in smaller numbers. Especially large specimens have occasionally been found in the North Atlantic, with a fish caught in 1976 off the coast of [[Wales]] being over {{Convert|108|lb|abbr=on}} in weight.{{cn|date=April 2017}}{{contradict-inline|date=April 2017}}<!-- the rest of the article states that mature adults are in the 250–1000 kg range; a 50 kg fish would be the opposite of "especially large" -->

Sunfish are usually found alone, but occasionally in pairs or in large groups while being cleaned.<ref name="LargePelagicsLifeHistory"/> They swim primarily in open waters, but are sometimes seen near [[kelp bed]]s, taking advantage of resident populations of smaller fish which remove [[ectoparasite]]s from their skin. Because sunfish must consume a large volume of prey, their presence in a given area may be used as an indicator of nutrient-rich waters where endangered species may be found.<ref name="LargePelagicsLifeHistory"/>{{clarify|date=April 2017}}<!-- What does this really mean?? suggest to take away last paragraph -->

===Feeding===
The diet of the ocean sunfish consists primarily of various [[jellyfish]]. It also consumes [[salp]]s, [[squid]], [[crustacean]]s, small fish, fish larvae, and [[Zostera|eel grass]].<ref name="OceanSunfishLifeHistory"/> This range of food items indicates that the sunfish feeds at many levels, from the surface to deep water, and occasionally down to the seafloor in some areas.<ref name="OceanSunfishLifeHistory"/> The diet is nutritionally poor, forcing the sunfish to consume a large amount of food to maintain its size.<ref name="UnderwaterPioneer"/>

===Lifecycle===
Ocean sunfish may live up to ten years in captivity, but their lifespan in a natural habitat has not yet been determined.<ref name="NationalGeo"/> Their growth rate is also undetermined. However, a young specimen at the [[Monterey Bay Aquarium]] increased in weight from {{convert|26|to|399|kg|lb|abbr=on}} and reached a height of nearly {{convert|1.8|m|ft|abbr=on}} in 15 months.<ref name="UnderwaterPioneer"/>

The sheer size and thick skin of an adult of the species deters many smaller predators, but younger fish are vulnerable to predation by [[bluefin tuna]] and [[mahi mahi]]. Adults are consumed by sea lions, orca, and sharks.<ref name="LargePelagicsLifeHistory"/> Sea lions appear to hunt sunfish for sport, tearing the fins off, tossing the body around, and then simply abandoning the still-living but helpless fish to die on the seafloor.<ref name="OceanSunfishLifeHistory"/><ref name="UnderwaterPioneer">{{cite book |url=http://ark.cdlib.org/ark:/13030/kt2g502035/ |title=A Fascination for Fish: Adventures of an Underwater Pioneer |first=David C. |last=Powell |year=2001 |accessdate=2007-06-13 |publisher=University of California Press, Monterey Bay Aquarium |location=Berkeley |isbn=0-520-22366-7 |oclc=44425533 |pages=270–275 |chapter=21. Pelagic Fishes |chapterurl=http://content.cdlib.org/view?docId=kt2g502035&chunk.id=ch21}}</ref>

The mating practices of the ocean sunfish are poorly understood, but spawning areas have been suggested in the North Atlantic, South Atlantic, North Pacific, South Pacific, and Indian oceans.<ref name="LargePelagicsLifeHistory"/> Females can produce as many as 300 million eggs at a time, more than any other known vertebrate.<ref name="OceanSunfishLifeHistory"/> Sunfish eggs are released into the water and [[spawn (biology)|externally fertilized]] by sperm.<ref name="AustMuseumAdult"/>

Newly hatched sunfish larvae are only {{convert|2.5|mm|in|1|abbr=on}} long and weigh a fraction of a gram. They grow to become [[Fry (biology)|fry]], and those which survive grow many millions of times their original size before reaching adult proportions.<ref name="OnlineFieldGuide"/> Sunfish fry, with large pectoral fins, a tail fin, and body spines uncharacteristic of adult sunfish, resemble miniature pufferfish, their close relatives.<ref name="AustMuseumAdult"/><ref name="FOGM_Mola">{{cite web
|url=http://www.gma.org/fogm/Mola_mola.htm
|title=The Ocean Sunfishes or Headfishes
|author=Fishes of the Gulf of Maine
|publisher=Fishes of the Gulf of Maine
|accessdate=2007-06-30}}</ref>
Young sunfish school for protection, but this behavior is abandoned as they grow.<ref name="SwimWithSunfish"/> By adulthood, they have the potential to grow more than 60 million times their birth size, arguably the most extreme size growth of any vertebrate animal.<ref name= Wood/><ref>Kooijman, S. A. L. M., & Lika, K. (2013). Resource allocation to reproduction in animals. ''Am. Nat.'' subm, 2(06).</ref>

==Genome==
In 2016, researchers from China National Genebank and [[Agency for Science, Technology and Research|A*STAR]] Singapore, including Nobel laureate [[Sydney Brenner]], [[Whole genome sequencing|sequenced the genome]] of the ocean sunfish and discovered several genes which might explain its fast growth rate and large body size. As member of the order Tetraodontiformes, like Fugu the sunfish has quite a compact genome, at 730 Mb in size. Analysis from this data suggests that sunfish and pufferfishes separated approximately 68 million years ago, which corroborates the results of other recent studies based on smaller datasets. <ref>{{cite journal|vauthors=Pan H, Yu H, Ravi V, et al.|date=9 September 2016|title=The genome of the largest bony fish, ocean sunfish (''Mola mola''), provides insights into its fast growth rate|url= http://gigascience.biomedcentral.com/articles/10.1186/s13742-016-0144-3|journal=GigaScience|doi=10.1186/s13742-016-0144-3|access-date=11 September 2016}}</ref>

==Human interaction==
[[File:Enormous Sunfish.jpg|thumb|left|A sunfish caught in 1910, with an estimated weight of 1600 kg (3500 lb)]]

Despite their size, ocean sunfish are docile, and pose no threat to human divers.<ref name="AustMuseumAdult"/> Injuries from sunfish are rare, although a slight danger exists from large sunfish leaping out of the water onto boats; in one instance, a sunfish landed on a 4-year-old boy when the fish leaped onto the boy's family's boat.<ref>
{{cite news |
url = http://news.bbc.co.uk/1/hi/wales/4192566.stm |
title = Boy struck by giant tropical fish |
publisher = BBC
| date = 2005-08-28
| accessdate = 2008-04-25}}
</ref> Areas where they are commonly found are popular destinations for sport dives, and sunfish at some locations have reportedly become familiar with divers.<ref name="fishbase"/> The fish is more of a problem to boaters than to swimmers, as it can pose a hazard to watercraft due to its large size and weight. Collisions with sunfish are common in some parts of the world and can cause damage to the hull of a boat,<ref name="BoatAlarm">{{cite news |url=http://www.theaustralian.news.com.au/story/0,20867,20965343-23218,00.html
|title=Giant sunfish alarm crews
|first=Amanda
|last=Lulham
|date=23 December 2006
|publisher=The Daily Telegraph
|accessdate=2007-06-12
}}</ref>
or to the propellers of larger ships.<ref name="AustMuseumAdult"/>

The flesh of the ocean sunfish is considered a delicacy in some regions, the largest markets being Taiwan and Japan. All parts of the sunfish are [[fish (food)|used in cuisine]], from the fins to the internal organs.<ref name="OceanSunfishResearch"/>
Some parts of the fish are used in some areas of traditional medicine.<ref name="fishbase"/> Fishery products derived from sunfish are forbidden in the [[European Union]] according to Regulation (EC) No 853/2004 of the European Parliament and of the Council as they contain toxins that are harmful to human health.<ref name="BannedinEEC" />

[[File:SunfishDish1 2.jpg|thumb|right|A dish made from the ocean sunfish]]

Sunfish are accidentally but frequently caught in [[Gillnetting#Drift nets|drift gillnet]] fisheries, making up nearly 30% of the total catch of the swordfish fishery employing drift gillnets in California.<ref name="OnlineFieldGuide"/> The [[bycatch]] rate is even higher for the Mediterranean swordfish industry, with 71% to 90% of the total catch being sunfish.<ref name="OceanSunfishResearch"/><ref name="SwimWithSunfish">{{cite video
|people=[[Tierney Thys]]
|date=February 2003
|title=Swim with giant sunfish in the open ocean
|url=https://www.ted.com/talks/tierney_thys_swims_with_the_giant_sunfish
|medium=Professional conference
|publisher=Technology Entertainment Design
|location=Monterey, California, United States
|accessdate =2007-05-30}}</ref>

The fishery bycatch and destruction of ocean sunfish are unregulated worldwide. In some areas, the fish are "finned" by fishermen who regard them as worthless bait thieves; this process, in which the fins are cut off, results in the eventual death of the fish, because it can no longer propel itself without its dorsal and anal fins.<ref name="MolaFishery">{{cite web
|url=http://www.tunalab.unh.edu/molafishery.htm
|title=Present Fishery/Conservation
|first=Tierney
|last=Thys
|publisher=Large Pelagics Lab
|archiveurl=https://web.archive.org/web/20110720094025/http://www.tunalab.unh.edu/molafishery.htm
|archivedate=2011-07-20}}</ref>
The species is also threatened by floating litter such as plastic bags which resemble jellyfish, its main food. Bags can choke and suffocate a fish or fill its stomach to the extent that it starves.<ref name="NationalGeo"/>

Many areas of sunfish biology remain poorly understood, and various research efforts are underway, including aerial surveys of populations,<ref name="LargePelagicsResearch">{{cite web |url=http://www.tunalab.unh.edu/molaresearch.htm
|title=Current Research
|publisher=Large Pelagics Research Lab
|archiveurl=https://web.archive.org/web/20110720094017/http://www.tunalab.unh.edu/molaresearch.htm
|archivedate=2011-07-20}}</ref> satellite surveillance using pop-off satellite tags,<ref name="OceanSunfishResearch">{{cite web |url=http://oceansunfish.org/research.html
|title=Ongoing Research
|first=Tierney
|last=Thys
|publisher=OceanSunfish.org
|accessdate=2007-06-11}}</ref><ref name="LargePelagicsResearch"/> genetic analysis of tissue samples,<ref name="OceanSunfishResearch"/> and collection of amateur sighting data.<ref>{{cite web |url=http://www.tunalab.unh.edu/haveyouseenamola.htm
|title=Have you seen a ''Mola''??
|publisher=Large Pelagics Research Lab
|archiveurl=https://web.archive.org/web/20110901151544/http://www.tunalab.unh.edu/haveyouseenamola.htm
|archivedate=2011-09-01}}</ref> A decrease in sunfish populations may be caused by more frequent bycatch and the increasing popularity of sunfish in human diet.<ref name="LargePelagicsLifeHistory"/>

In 2015, a [[viral video|video went viral]] of a [[Boston]] man profusely expressing his amazement when encountering a basking sunfish for the first time.<ref>{{cite web|url=http://www.boston.com/news/local/massachusetts/2015/09/24/might-not-sea-monstah-but-the-sunfish-that-video-ocean-oddball/7DZzEFY8sVrqBejs37dgGI/story.html|title=It might not be a 'sea monstah' but the sunfish in that video is an 'ocean oddball'|last=Wilder|first=Charlotte|publisher=Boston.com}}</ref><ref>{{cite web|url=http://www.buzzfeed.com/katienotopoulos/this-man-yelling-at-a-fish-is-a-massachusetts-hero#.mrqV2jJwE|title=This Boston Man Yelling At A "Sea Monstah" Is The Best Thing You'll See Today|publisher=Buzzfeed|last=Notopoulos|first=Katie}}</ref>
{{clear}}

===In captivity===
[[File:Mola mola ocean sunfish Monterey Bay Aquarium 2.jpg|thumb|left|A tank at the Monterey Bay Aquarium provides a size comparison between an ocean sunfish and humans.]]

Sunfish are not widely held in aquarium exhibits, due to the unique and demanding requirements of their care. Some Asian aquaria display them, particularly in Japan.<ref name="UnderwaterPioneer"/> The [[Osaka Aquarium Kaiyukan|Kaiyukan Aquarium]] in [[Osaka]] is one of few aquariums with ''M. mola'' on display, where it is reportedly as popular an attraction as the larger [[whale shark]]s.<ref name="OsakaAquarium">{{cite web
|url=http://www.kaiyukan.com/eng/life/index.htm
|title=Main Creature in Kaiyukan
|publisher=Osaka Kaiyukan Aquarium
|accessdate=2007-06-13| archiveurl = https://web.archive.org/web/20070528161126/http://www.kaiyukan.com/eng/life/index.htm| archivedate = May 28, 2007}}</ref>
The [[Lisbon Oceanarium]] in Portugal has sunfish showcased in the main tank,<ref name="Oceanario_Lisbon">{{cite web
|url=http://www.oceanario.pt/en/exhibitions/permanent-exhibition/fishes/sunfish
|title=Sunfish
|publisher=Oceanario
|accessdate=2009-04-05}}</ref> and in Spain, the [[L'Oceanogràfic|Valencia Oceanogràfic]]<ref>{{cite web
|url=http://www.cac.es/oceanografic/animals/ficha/?contentId=109672
|title=Sunfish at Oceanogràfic
|publisher=Oceanogràfic
|accessdate=2010-08-05}}</ref> has specimens of sunfish.
The [[Nordsøen Oceanarium]] in the northern town of [[Hirtshals]] in Denmark is also famous for its sunfish.<ref>{{cite web
|url=http://en.nordsoenoceanarium.dk/the-open-sea
|title=The Open Sea
|publisher=Nordsøen Oceanarium
|accessdate=2015-09-17}}</ref>

While the first ocean sunfish to be held in an aquarium in the United States is claimed to have arrived at the [[Monterey Bay Aquarium]] in August 1986,<ref>{{cite web
|url=http://www.mbayaq.org/aa/timelineBrowser.asp?tf=32
|title=Aquarium Timeline
|publisher=Monterey Bay Aquarium
|accessdate=2007-06-11|archiveurl=https://web.archive.org/web/20080511143956/http://www.mbayaq.org/aa/timelineBrowser.asp?tf=32|archivedate=2008-05-11}}</ref> other specimens have previously been held at other locations. [[Marineland of the Pacific]], closed since 1998 and located on the Palos Verdes Peninsula in Los Angeles County, California, held an ocean sunfish in its warm-water tank as early as 1957,<ref>Brochure: Marineland of the Pacific, 1957</ref> and in 1964 held a {{convert|650|lb|kg|adj=on}} specimen, claimed as the largest ever captured at that time.<ref>Los Angeles Times - Jun 15, 1964. p.3</ref> However, another {{convert|1000|lb|kg|adj=on}} specimen was brought alive to Marineland Studios Aquarium, near St. Augustine, Florida, in 1941.<ref>The Miami News, March 16, 1941, p. 5-C</ref>

[[File:Mondfisch Ozenarium Lissabon 20090228.ogv|thumb|right|Video of an ocean sunfish at the [[Lisbon Oceanarium]]]]

Because sunfish had not been kept in captivity on a large scale before, the staff at Monterey Bay was forced to innovate and create their own methods for capture, feeding, and parasite control. By 1998, these issues were overcome, and the aquarium was able to hold a specimen for more than a year, later releasing it after its weight increased by more than 14 times.<ref name="UnderwaterPioneer"/> ''Mola mola'' has since become a permanent feature of the Open Sea exhibit.<ref name="OnlineFieldGuide"/> Monterey Bay Aquarium's largest sunfish specimen was euthanized on February 14, 2008, after an extended period of poor health.<ref>{{cite web
|date=14 February 2008
|url=http://www.ksbw.com/news/15306004/detail.html
|title=Aquarium Euthanizes Its Largest Ocean Sunfish
|publisher=KSBW
|accessdate=2008-12-20|archiveurl=https://web.archive.org/web/20110713173709/http://www.ksbw.com/news/15306004/detail.html|archivedate=2011-07-13}}</ref>

A major concern to curators is preventive measures taken to keep specimens in captivity from injuring themselves by rubbing against the walls of a tank, since ocean sunfish cannot easily maneuver their bodies.<ref name="OsakaAquarium"/> In a smaller tank, hanging a vinyl curtain has been used as a stopgap measure to convert a cuboid tank to a rounded shape and prevent the fish from scraping against the sides. A more effective solution is simply to provide enough room for the sunfish to swim in wide circles.<ref name="UnderwaterPioneer"/> The tank must also be sufficiently deep to accommodate the vertical height of the sunfish, which may reach {{convert|3.2|m|ft|abbr=on}}.<ref name="NZHeraldVisitor"/>

Feeding captive sunfish in a tank with other faster-moving, more aggressive fish can also present a challenge. Eventually, the fish can be taught to respond to a floating target to be fed,<ref name="Life in the slow lane">{{cite video
|title=Life in the slow lane
|url=http://www.montereybayaquarium.com/animals/AnimalDetails.aspx?enc=C53nR+hhcrXgfKW+bt/MWA
|publisher=Monterey Bay Aquarium
|accessdate=October 24, 2010}}</ref> and to take food from the end of a pole or from human hands.<ref name="UnderwaterPioneer"/>

{{clear}}

==References==
{{Reflist|30em}}

==External links==
{{Commons category|Mola mola}}
{{wikispecies|Mola mola}}

===Research and info===
* [http://FishBase.org/Summary/SpeciesSummary.php?id=1732 FishBase reference]
* [http://AustralianMuseum.net.au/Ocean-Sunfish-Mola-mola/ Australian Museum]
* [http://OceanSunfish.org/ OceanSunfish.org]

===Images and videos===
* [http://www.earthwindow.com/mola.html Mike Johnson Natural History Photography]
* [http://www.oceanlight.com/html/mola_mola.html Phillip Colla Photography/Oceanlight.com]
* [http://www.ted.com/talks/tierney_thys_swims_with_the_giant_sunfish Video lecture (16:53): ''Swim with giant sunfish in the open ocean'' - [[Tierney Thys]]]
* Skaphandrus.com [http://skaphandrus.com/en/marine-species/info/species/Mola-mola Mola mola photos]
{{Featured article}}
* [http://www.liveleak.com/view?i=ba4_1410374726 Giant sunfish filmed off Malta]
* [https://www.youtube.com/watch?v=3z8LC_gP364 Sunfish filmed off the coast of Massachusetts in 2015]

{{DEFAULTSORT:Ocean Sunfish}}
[[Category:Fish of Hawaii]]
[[Category:Molidae]]
[[Category:Articles containing video clips]]
[[Category:Saltwater fish of Florida]]
[[Category:Poisonous fish]]
[[Category:Animals described in 1758]]
[[Category:Fish of Australia]]