{{italic title}}
[[File:Elsie Venner title page.jpg|thumb|Title page of ''Elsie Venner'']]
'''''Elsie Venner: A Romance of Destiny''''' is an 1861 novel by American author and physician [[Oliver Wendell Holmes, Sr.]] Later dubbed the first of his "medicated novels", it tells the story of a neurotic young woman whose mother was bitten by a rattlesnake while pregnant, which imbued the child with some characteristics of the reptile.

==Plot summary==
The novel is narrated by an unnamed medical professor. One of his students, Bernard Langdon, had to interrupt his medical studies to earn money as a teacher, first at a public school, then at the Apollinean Female Institute. The schoolmistress of the institute is Miss Helen Darley, who is literally working herself to death. One of Langdon's students is the 17-year-old Elsie Venner, who purposely sits apart from the other students. She is known for being strange and quick to anger. She is only close to her father Dudley Venner, whom she calls "Dudley"; to her governess, Old Sophy; and to the town physician, Dr. Kittredge.

Elsie's half-Spanish cousin Richard "Dick" Venner pays a visit at the Venner estate. Like Elsie, his mother died when he was a child and the two cousins were playmates in their childhood. Elsie, however, was rough on her cousin and once bit him hard enough to leave a permanent scar. Dick has become a skilled horse-rider and a bit of a trouble-maker, though stories of his escapades are unclear. Rumors abound that Dick has come to town to ask his cousin Elsie to marry him; in fact, he intends to marry her so that he can inherit his uncle's estate.

Langdon is surprised to find a gift stuck in the pages of a book by [[Virgil]] on his desk at school. Pressed inside is an exotic-looking flower, known to be the type Elsie collects. Frightened yet intrigued that the girl has taken an interest in him, he resolves to climb the mountain and find her secret hiding-place. After climbing several precipitous rock formations, Langdon finds the source of the exotic flower. Investigating a cavern where he thinks Elsie hides out, Langdon is instead encounters a [[rattlesnake]] poised to strike. However, at that moment Elsie appears and calms the snake merely by looking at it.

Intrigued, Langdon researches snakes, poisons, and the "evil eye". He captures some snakes and contacts his old professor for information. Doctor Kittredge recognizes the mutual interest between Langdon and Elsie, and recommends the former begin practicing with a pistol. In the meantime, Dick Venner continues cultivating a relationship with Elsie, and is jealous of Langdon. He also worries that Elsie's father might marry Miss Darley (and thus interrupt the expected pattern of inheritance). One night, Dick attacks Langdon with his lasso. Langdon shoots his pistol and kills Dick's horse but is injured in the melee. Dr. Kittredge's assistant appears, having been ordered to follow Dick and, after he publicises the incident, Dick is run out of town.

Elsie soon admits her romantic interest in Langdon. Though he admits he is concerned about her as a friend, she is devastated by his rejection, and falls sick. During her illness, she calls for Miss Darley to attend to her. Miss Darley finally asks Old Sophy how Elsie's mother died, and it is implied that she was poisoned by a snake bite shortly before Elsie was born. During her illenss, Elsie slowly loses her mysterious nature and softens enough to tell her father that she loves him. She dies shortly after.

==Composition and publication==
Holmes's medical background&mdash;he had studied medicine in [[Paris]] when he was young and was a professor at [[Harvard Medical School]] when the novel was published&mdash;was an important influence on the novel's storyline, as well as its overriding themes.

''Elsie Venner'' was Holmes's first novel, originally published serially in ''[[The Atlantic|The Atlantic Monthly]]'' beginning in December 1859 as "The Professor's Story".<ref>Broaddus, Dorothy C. ''Genteel Rhetoric: Writing High Culture in Nineteenth-Century Boston''. Columbia, South Carolina: University of South Carolina, 1999: 62. ISBN 1-57003-244-0.</ref> It was first published as a stand-alone novel in 1861. It was republished in 1883 and 1891.

==Analysis==
[[File:Holmes photo 1910.jpg|thumb|Holmes called ''Elsie Venner'' one of his "medicated novels".]]
Holmes wrote in the second preface to ''Elsie Venner'' that his aim was "to test the doctrine of '[[original sin]]' and human responsibility for the distorted violation coming under that technical denomination".<ref>Weinstein, Michael A. ''The Imaginative Prose of Oliver Wendell Holmes''. Columbia: University of Missouri Press, 2006: 94</ref> The novel explores medical complexities, psychology, the origins of behavior and the certainties of religion.<ref>Ruland, Richard and Malcolm Bradbury. ''From Puritanism to Postmodernism: A History of American Literature''. New York: Viking, 1991: 112. ISBN 0-670-83592-7</ref> The novel bears a resemblance to [[Nathaniel Hawthorne]]'s short story "[[Rappaccini's Daughter]]", a tale which explored original sin several years before ''Elsie Venner'' was published.<ref>Fryer, Judith. ''The Faces of Eve: Women in the Nineteenth-Century American Novel''. Oxford: Oxford University Press, 1976: 41. ISBN 0-19-502431-1</ref> It is also often compared to Hawthorne's ''[[The Marble Faun]]'' (1860) in that both works discuss moral-theological questions in terms of psychology.<ref>Gibian, Peter. ''Oliver Wendell Holmes and the Culture of Conversation''. Cambridge: Cambridge University Press, 2001: 294. ISBN 0-511-01763-4</ref> Holmes himself referred to ''Elsie Venner'' as one of his "medicated novels", along with ''The Guardian Angel'' (1867) and ''A Moral Antipathy'' (1885), because of the characters' health or mental problems which are "diagnosed" in the text.<ref>Gibian, Peter. ''Oliver Wendell Holmes and the Culture of Conversation''. Cambridge: Cambridge University Press, 2001: 4–5. ISBN 0-511-01763-4</ref>

The character of Elsie Venner may have been modeled on the famous feminist pioneer [[Margaret Fuller]],<ref>Von Mehren, Joan. ''The Minerva and the Muse: A Life of Margaret Fuller''. Amherst: [[University of Massachusetts Press]], 1994: 18. ISBN 1-55849-015-9</ref> whom Holmes knew from his school days in Cambridge.

==Influence==
It was in ''Elsie Venner'' that Holmes first coined the term [[Boston Brahmin]], originally referred to as "the Brahmin caste of New England... the harmless, inoffensive, untitled aristocracy".<ref>O'Connor, Thomas H. ''The Hub: Boston Past and Present''. Boston: Northeastern University Press, 2001: 87. ISBN 1-55553-474-0</ref>

==References==
{{reflist}}

==External links==
{{wikisource|Elsie Venner|''Elsie Venner''}}
*[http://www.gutenberg.org/etext/2696 ''Elsie Venner'' at Project Gutenberg]
*{{imdb company|1258694}} (1914 film adaptation)

[[Category:1861 novels]]
[[Category:Medical novels]]
[[Category:Works by Oliver Wendell Holmes Sr.]]