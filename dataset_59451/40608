'''Kristóf Nyíri''' (born 1944), is a member of [[Hungarian Academy of Sciences|HAS]],<ref>{{cite web|title=Hungarian Academy of Sciences|url=http://mta.hu/oldmta_en/?pid=859&LANG=e&TID=558|accessdate=June 25, 2012}}</ref> was a guest at the [[University of Leipzig]] in the Winter Semester of 2006-2007 as Leibniz Professor, <ref name="June 25, 2012">{{cite web|title=Universität Leipzig|url=http://www.uni-leipzig.de/zhs/de/leibniz/56-200607-wintersemester-prof-dr-kristof-nyiri-hungarian-academy-of-sciences/|accessdate=June 25, 2012}}</ref> directed Communications in the 21st Century: The Mobile Information Society<ref>[http://www.socialscience.t-mobile.hu/index.html Communications in the 21st Century: The Mobile Information Society]</ref> from 2001 to 2010,<ref>{{cite web|title=Század kommunikációja Mobil információs társadalom|url=http://www.socialscience.t-mobile.hu/index.html|accessdate=June 25, 2012}}</ref> and is a Professor of [[Budapest University of Technology and Economics|Philosophy]], in the Department of Technical Education, at [[Budapest University of Technology and Economics]].<ref>{{cite web|title=BME Filozófia és Tudománytörténet Tanszék|url=http://www.filozofia.bme.hu/people/Nyiri-Kristof|accessdate=June 25, 2012}}</ref> He has written and edited more than 200 articles, chapters, reports and books<ref name="hunfi_b">Nyíri, Kristóf. List of Publications. [http://www.hunfi.hu/nyiri/PUBLLweb.htm] Retrieved 13 June 2012.</ref> (his work from the late 1960s to 2001, was published under the name "J. C. Nyíri").

His initial areas of interest were Austrian intellectual history,<ref>[http://www.hunfi.hu/nyiri/oe.htm Austrian intellectual history]</ref> and the theory of [[conservatism]]. In the 1980s, he began to study the philosophical question of tradition as a cognitive issue and as an issue in social philosophy. He later became interested in the cultural history of communication technologies, and more recently he studies the philosophy of images and time.<ref name="hunfi_a">Nyíri, Kristóf. Personal Website. [http://www.hunfi.hu/nyiri/] Retrieved 13 June 2012.</ref>

He is well known for his work on mental imagery,<ref>{{cite web|last=Nigel|first=Thomas|title="Mental Imagery", The Stanford Encyclopedia of Philosophy|url=http://plato.stanford.edu/archives/win2011/entries/mental-imagery/|publisher=The Stanford Encyclopedia of Philosophy|accessdate=18 July 2012}}</ref> his interpretations of [[Ludwig Wittgenstein|Wittgenstein]]’s later work,<ref>Hrachovec, Herbert., Pichler, Alois. (2007). ''Wittgenstein and the Philosophy of Information: Proceedings of the 30th International Ludwig Wittgenstein-Symposium in Kirchberg”, p. 157. Ontos Verlag, Frankfurt. ISBN 3868380019.</ref><ref>Stern, David G. 2005 “How Many Wittgensteins?” In Wittgenstein: The Philosopher and his Works, edited by Alois Pichler and Simo Säätelä, p. 221. Working Papers from the Wittgenstein Archives at the University of Bergen no. 17. Bergen: Wittgenstein Archives at the University of Bergen</ref> and the application of his philosophy to mobile communication.<ref>{{cite web|last=Peters|first=Michael|title=Open Works, Open Cultures and Open Learning Systems|url=http://www.ffst.hr/ENCYCLOPAEDIA/doku.php?id=open_culture#fnt__1|publisher=The Encyclopedia of Educational Philosophy and Theory|accessdate=18 July 2012}}</ref><ref>Thomas, Michael. “Book Review, Thumb Culture: The Meaning of Mobile Phones for Society”, ''Australian Journal of Emerging Technologies and Society'', Vol. 5, No. 1, 2007: pp. 60-62.</ref>

==Biography==

Kristóf Nyíri was born and raised in a Budapest suburb and now lives in the village of [[Dunabogdány]] in the Danube Bend, with his wife Ilona Tibay (married since 1973) and many pet dogs and cats.
He received his MA in philosophy and mathematics from [[Eötvös Loránd University]], Budapest, and his PhD in Philosophy and DSc in Philosophy from the [http://mta.hu/english/ Hungarian Academy of Sciences] in 1978 and 1985 respectively.

==Career==

Prior to his current position as Professor of [[Budapest University of Technology and Economics|Philosophy]], in the Department of Technical Education, at Budapest University of Technology and Economics, Nyíri held the following positions: From 1995–2005 he was Director of the [http://www.phil-inst.hu/english.html Institute for Philosophical Research of the Hungarian Academy of Sciences],<ref>{{cite web|title=Hungarian Academy of Sciences-Institute for Philosophical Research|url=http://www.phil-inst.hu/en/fellows/fellows.html|accessdate=25 June 2012}}</ref> and later, from 2005 to 2007 became a research professor there. From 1971- 2004 Nyíri held roles at [[Eötvös Loránd University]], as Assistant Professor (1971–78), Associate Professor (1978–86), and as full Professor of Philosophy (1986–2004).

==Distinctions==
 
In 2009, Nyíri was awarded the [[Széchenyi Prize]], a prize given in Hungary by the state, in recognition of those who have made an outstanding contribution to academic life in Hungary, and received an honorary degree from the [[University of Pécs]], Hungary. 

He was elected as member of the [http://www.i-i-p.org/index.php Institut International de Philosophie] in 2006,<ref>{{cite web|title=L'Institut international de philosophie|url=http://www.i-i-p.org/index.php|accessdate=June 25, 2012}}</ref> that same year (2006/07) he was elected as Leibniz Professor at the [[University of Leipzig]].<ref name="June 25, 2012" /> In 1994 Nyíri was a visiting Fellow at the IFK-International Research Center for Cultural Studies, Vienna and in 1993 he was a European visiting Research Fellow of the [[Royal Society of Edinburgh]] at the [[University of St. Andrews]], Centre for Philosophy and Public Affairs.<ref>{{cite web|title=University of St Andrews Centre for Ethics, Philosophy and Public Affairs|url=http://www.st-andrews.ac.uk/ceppa/fellows.html|accessdate=25 June 2012}}</ref>

Nyíri also has held and holds various board memberships: From 1995 to present he is the Honorary President of the [http://www.phil-inst.hu/~wittgenstein/ Hungarian Wittgenstein Society],<ref>{{cite web|title=Magyar Wittgenstein Tarsasag|url=http://www.phil-inst.hu/~wittgenstein/cel.htm|accessdate=25 June 2012}}</ref> Budapest, Hungary. From 1993 to 1998 he was President of the Hungarian Philosophical Association, Budapest, Hungary, and since 1987 he has been an Advisory Board Member at Forschungsstelle für österreichische Philosophie in Graz, Austria.

==Philosophy==

From the works of [[Plato]], [[John Dewey|Dewey]], [[Heidegger]] and [[Wittgenstein]], Nyíri reads that social communication has always been a decisive philosophical question. Nyíri claims that today's dominant communication media, the cell phone, with its ability to transmit multimedia messages that convey far more than speech alone, is a unique device satisfying primordial human needs.<ref name="hunfi_a" /> In Wittgenstein’s work Nyíri finds the theoretical background for modern mobile communication. A main thesis in Wittgenstein is that pictures are an important component in communication; rather than explicit speech, which has a clear temporal direction, pictures also have a spatial dimension, and in particular a sequence of images is a natural carrier of meaning and can convey complex conceptual messages unambiguously. Devices that transmit multimedia messages can contain speech but also images and sounds that describe states of affairs with a richness not found before. Nyíri presents the mobile phone as the instrument which makes possible a new integration of imagery, orality and literacy, a new art of collective thought, and one that defines a new sense of space and time.<ref name="hunfi_a" /> Nyíri was also heavily influenced by the works of [[Wilfrid Sellars]], [[Eric Havelock]] and [[Walter J. Ong]].

==Criticism==

Nyíri’s work has been criticized on two main points, namely his position that Wittgenstein was a philosopher of conservatism and his interpretation that images can convey meaning without the accompaniment of words.

Nigel Pleasants opposes Nyiri’s beliefs that Wittgenstein was a philosopher of conservatism.<ref>[http://socialsciences.exeter.ac.uk/sociology/staff/pleasants Nigel Pleasants opposition]</ref> Pleasants contends that “the extent to which Wittgenstein can be said to be conservative lies in his rigorous skepticism regarding the power of philosophical theory to yield representation of the ‘essential nature’ of mental and epistemic phenomena,”<ref>Koller, Peter. “Current issues in political philosophy: justice and Welfare in Society and World Order; papers of the 19th International Wittgenstein Symposium”, p.292. Austrian Ludwig Wittgenstein Soc.</ref> (Current Issues in Political Philosophy, Justice and Welfare in Society and World Order, Papers of the 19th International Wittgenstein Symposium, Kirchberg am Wechsel, 1996, p. 292).

In the compendium, Wittgenstein and Political Philosophy, Nyiri is referenced repeatedly. In the introduction, [[Cressida Heyes]] mentions Nyiri’s interpretation of the later Wittgenstein’s writings as supporting a conservative worldview, in alignment with his social circle and their intellectual predilection in Vienna at the time; she goes on to cite similar views (e.g. Gellner, Bloor and Winch) as well as opposing views (e.g. Eagleton)<ref>Heyes, Cressida.“The Grammar of Politics: Wittgenstein and Political Philosophy”, p. 4. Cornell University Press. ISBN 0801488389.</ref> (p. 4,5). In a further article, the Limits of Conservatism, David Cerborne opposes Nyiri’s views as he claims “the pictures at the heart of the conservative-mind reading must be shown to be a target of Wittgenstein’s criticism rather than a cornerstone of it.” (p 48, 51).

In the volume [http://www.amazon.com/exec/obidos/tg/detail/-/9042008881/qid=1126703722/sr=8-2/ref=sr_8_xs_ap_i2_xgl14/002-5438527-9632004?v=glance&s=books&n=507846 Essays on Wittgenstein and Austrian Philosophy: In Honour of J.C. Nyíri], [[:de:Herbert Hrachovec|Herbert Hrachovec]] debates the interpretation of the later Wittgenstein that images can convey meaning without the accompaniment of words. As Hrachovec sees the matter, "Nyíri's tendency to paint Wittgenstein as an, albeit hesitant, pictorialist" (p.&nbsp;205), is definitely an exaggeration.<ref>Demeter, Tamás. “Essays on Wittgenstein and Austrian Philosophy: In Honour of J.C. Nyíri”, p. 205. Rodopi, Amsterdam . ISBN 9042008881.</ref>

==Publications==

=== Online ===
Nyíri is the author of more than 200 articles, below is a list of selected publications:
* "Image and Metaphor in the Philosophy of Wittgenstein", in: R. Heinrich et al., Image and Imaging in Philosophy, Science and the Arts, Proceedings of the 33rd International Ludwig Wittgenstein Symposium, vol. 1, Heusenstamm bei Frankfurt: ontos Verlag, 2011, pp.&nbsp;109–129.
* "Images in Natural Theology," in: Russell Re Manning, ed., The Oxford Handbook of Natural Theology, Oxford: Oxford University Press, 2012.
* "Gombrich on Image and Time", in Journal of Art Historiography<ref>, no. 1 (December 2009).[http://www.gla.ac.uk/departments/arthistoriography "Gombrich on Image and Time", in Journal of Art Historiography]</ref> Published in hardcopy in Klaus Sachs-Hombach and Rainer Totzke, eds., Bilder – Sehen – Denken: Zum Verhältnis von begrifflich-philosophischen und empirisch-psychologischen Ansätzen in der bildwissenschaftlichen Forschung, Köln: Herbert von Halem Verlag, 2011, pp.&nbsp;9–32.
* "Hundred Years After: How McTaggart Became a Thing of the Past", in: T. Czarnecki et al., eds., The Analytical Way: Proceedings of the 6th European Congress of Analytic Philosophy, London: College Publications, pp.&nbsp;47–64.
* "Film, Metaphor, and the Reality of Time", New Review of Film and Television Studies, vol. 7, no. 2 (June 2009), pp.&nbsp;109–118.<ref>[http://dx.doi.org/10.1080/17400300902816796. "Film, Metaphor, and the Reality of Time", New Review of Film and Television Studies, vol. 7, no. 2]</ref>
* "Visualization and the Limits of Scientific Realism" Nyíri. (2008)
* "Pictorial Meaning and Mobile Communication" [English translation of (2002f)], in: Kristóf Nyíri, ed., Mobile Communication: Essays on Cognition and Community, Vienna: Passagen Verlag, 2003, pp.&nbsp;157–184.
* "The Picture Theory of Reason”, in: Berit Brogaard - Barry Smith, eds., Rationality and Irrationality, Wien: öbv-hpt, 2001, pp.&nbsp;242–266.
* "In a State of Flux". Review (abridged) of Manuel Castells, The Information Age. -- Budapest Review of Books, Summer/Fall 1999, pp.&nbsp;55–64.
* "Thinking with a Word Processor". In: R. Casati, ed., Philosophy and the Cognitive Sciences, Vienna: Hölder-Pichler-Tempsky, 1994, pp.&nbsp;63–74.
* "Kant and the New Way of Words". Review of W. Sellars, Science and Metaphysics. - Inquiry 1970/3, pp.&nbsp;321–331.

===Books===
Nyíri is also author and editor of numerous books, below is a list of selected publications.<ref name="hunfi_b" />

==== As author ====

* Zeit und Bild: Philosophische Studien zur Wirklichkeit des Werdens, Bielefeld: Transcript Verlag, 2012, pp.&nbsp;203.
* Tradition and Individuality. Essays. Dordrecht: Kluwer, 1992, xi + 180 pp. Reviewed in BUKSZ, Autumn 1993, in Magyar Filozófiai Szemle, 1995/3-4, and in Studies in East European Thought 51 (1999), pp.&nbsp;329–345.
* Am Rande Europas: Studien zur österreichisch-ungarischen Philosophiegeschichte [On the fringe of Europe. Studies in the history of Austro-Hungarian philosophy]. Wien: Böhlau, 1988, pp.&nbsp;232. Reviewed in Review of Metaphysics, June 1989, in Deutsche Zeitschrift für Philosophie 1989, in Austrian Studies 1990, and in Wissenschaftliche Zeitschrift der Universität Halle 1991/5. Hungarian translation reviewed in Népszabadság, May 21, 1987, and in Magyar Nemzet, February 23, 1987.

==== As editor ====

* Mobile Studies: Paradigms and Perspectives, Vienna: Passagen Verlag, 2007.
* Mobile Communication: Essays on Cognition and Community, Vienna: Passagen Verlag, 2003.
* Practical Knowledge: Outlines of a Theory of Traditions and Skills, with B. Smith. Beckenham: Croom Helm, 1988.

==References==
{{Reflist|2}}

{{Authority control}}
{{DEFAULTSORT:Nyiri, Kristof}}
[[Category:1944 births]]
[[Category:Leipzig University faculty]]
[[Category:Living people]]