The ''''Two Layer' Hypothesis''', or '''immigration hypothesis''', is an [[Archaeology|archaeological]] theory that suggests the human occupation of mainland [[Southeast Asia]] occurred over two distinct periods by two separate racial groups, hence the term 'layer'.<ref name="reich">Reich, D., Patterson, N., Kircher, M., Delfin, F., Nandineni, M.R., Pugach, I.,... Stoneking, M. (2011). Denisova admixture and the first modern human dispersals into Southeast Asia and Oceania. The American Journal of Human Genetics, 89(4), 516-528.</ref>  According to the Two Layer Hypothesis, early indigenous [[Australoid race|Australo-Melanesian]] peoples comprised the first population of Southeast Asia before their genetic integration with a second wave of inhabitants from East Asia, including Southern China, during the [[Neolithic Revolution|agricultural expansion]] of the [[Neolithic]].<ref name="matsu1">Matsumura, H., Oxenham, M.F., Dodo, Y., Domett, K. Thuy, N.K., Cuong, N.L.,... Yamagata, M. (2008). Morphometric affinity of the late Neolithic human remains from Man Bac, Ninh Binh Province, Vietnam: key skeletons with which to debate the 'two layer' hypothesis. Anthropological Science, 116(2), 135-148</ref><ref name="matsu2">Matsumura, H., Hudson, M.J. (2005). Dental perspectives on the population history of Southeast Asia. American Journal of Physical Anthropology, 127(2), 182-209.</ref>
The majority of evidence for the Two Layer Hypothesis consists of dental and [[Morphometrics|morphometric]] analyses from archaeological sites throughout Southeast Asia, most prominently [[Thailand]] and [[Vietnam]]. The credibility of the Two Layer Hypothesis has been criticized due mainly to similarities between Southeast Asian and Chinese cranial and dental characteristics, excluding Australo-Melanesians.

== History ==

The first fossilized skeletal remains and indication of early 'Proto-Australian' Southeast Asian inhabitants surfaced in 1920 during an excavation by Dubois on the island of [[Java]].<ref name="dubois">Dubois, E. (1921). The proto-Australian fossil man of Wadjak, Java. Koninklijke Nederlandse Akademie van Weteschappen Proceedings Series B Physical Sciences, 23, 1013-1051.</ref>  Despite this, a formal connection to mainland Southeast Asia and the suggestion of an initial population of Australomelanesoids was not suggested until 1952 by Koenigswald in his response to Hooijer,<ref name="koen">Koenigswald, G.H.R. (1952). Evidence of a prehistoric Australomelanesoid population in Malaya and Indonesia. Southwestern Journal of Anthropology, 8(1), 92-96.</ref>  who sharply criticized the attribution of 'big toothed' dental remains to early Australo-Melanesians.<ref name="hooijer">Hooijer, D.A. (1950). Fossil Evidence of Austromelanesian Migrations in Malaysia? Southwestern Journal of Anthropology, 6(4), 416-422.</ref>
The immigration hypothesis proposed by Koenigswald was formally termed the 'Two Layer' model by Jacob Teuku. In 1967, Teuku analyzed the cranial and dental proportions of 152 adult skeletal samples recovered from prehistoric sites in [[Malaysia]] and [[Indonesia]], the majority displaying robust jaws and teeth, prominent [[glabella]]e, and slender, elongated limbs. Teuku argued these characteristics correspond to the Australo-Melanesian population proposed by Koenigswald that predated the East Asian immigrants of the Neolithic; also suggesting the initial inhabitants were likely forced south of Southeast Asia's mainland by the second wave of migrants, due to resource competition or conflict.<ref name="matsu2" /><ref name="jacob">Jacob, T. (1967). Some problems pertaining to the racial history of the Indonesian region: a study of human skeletal and dental remains from several prehistoric sites in Indonesia and Malaysia. Drukkerij Neerlandia.</ref>

== Archaeological evidence ==

=== Thailand ===

Excavations at Moh Khiew Cave resulted in the discovery of a [[Late Pleistocene]] female human skeleton, an [[Accelerator mass spectrometry|AMS]] radiocarbon date on a charcoal sample that was gathered from the burial grave gave an age of 25,800 +/- 600 BP.<ref name="matsu2" />  Measurements such as the bimaxillary breadth, upper facial height, bicondylar breadth, mandibular length, nasal breadth and height, palatal height, and mandibular angle were compared from the cranial and dental area to other female human specimens from [[East Asia]] and the Southwest Pacific regions. Twelve measurements were used in total for the statistical comparison in addition to 14 buccolingual crown diameters of the maxillary and mandibular teeth.<ref name="matsu3">Matsumura, H., & Pookajorn, S. (2005). A morphometric analysis of the Late Pleistocene human skeleton from the Moh Khiew Cave in Thailand. HOMO-Journal of Comparative Human Biology, 56(2), 93-118.</ref>  After a comparison was completed the closest sample to the Moh Khiew Cave sample is the Late Pleistocene Coobool Creek from [[Australia]].<ref name="matsu2" /> The next closest sample is the modern [[Australian Aborigines]].  However, those from [[Flores]], Vietnam, and Thailand are distant samples in comparison.  This discovery suggests that the Moh Khiew Cave skeleton may have shared a common ancestor with Australian Aborigines and [[Melanesians]] and also supports the ‘Two Layer’ hypothesis.

=== Vietnam ===

A study conducted in 2005, examined and compared permanent teeth from 4,002 individuals found in 42 prehistoric and historic samples throughout [[East Asia]], Southeast Asia, Australia, and [[Melanesia]].<ref name="matsu2" /> Metric dental traits were denoted by [[Anatomical terms of location#Teeth|mesiodistal]] and buccolingual crown diameters.  Measurements were taken as maximum diameters and male individuals were primarily measured because males generally have larger teeth and therefore show more differences.  One culture that was prevalent during this time was the [[Đa Bút culture]] which occurred during 6000-5000 BP.<ref name="matsu2" /> The metric and nonmetric dental analyses of the Đa Bút resulted in one culture that are highly differentiated from the East Asians.  A cluster analysis of the transformed [[Probability plot correlation coefficient|Q-mode correlation coefficients]] of the dental crown measurements, showed a major cluster with Australo-Melanesians.  Da But also has similarities with Australo-Melanesians in tooth size proportions and nonmetric traits.<ref name="matsu2" /> These close traits indicates a likeness with Australo-Melanesians.

== Modern debates and controversies ==

The main controversy concerning the 'Two Layer' hypothesis is whether or not the evolutionary process truly involved the Australo-Melanesians. Archaeologists such as Matsumura suggest Southern Chinese people comprised the initial population of Southeast Asia, rather than Australo-Melanesians<ref name="matsu1" /> while researchers such as Turner argue that prehistoric Southeast Asians did not mix with either racial group.<ref name="turner">Turner, C. (1992). The Dental Bridge between Australia and Asia: Following Macintosh into the East Asian Hearth of Humanity. Archaeology in Oceania, 27(3), 143-152.</ref>
Though the early prehistoric Vietnamese and Malaysians both resembled the Australo-Melanesian samples the most, the Màn Bạc people had a greater resemblance to the [[Dong Son culture|Đông Sơn]] samples dating back to the [[Iron Age]]. Analyzing cranial and dental remains, Matsumura concluded based on chronological differences that the Man Bac people were immigrants affiliated with peoples near the [[Yangtze|Yangtze River]] region in Southern China.<ref name="matsu1" /> Molecular anthropologists have used classical genetic markers and [[mitochondrial DNA|mtDNA]] to analyze the similarities between early Chinese and Southeast Asians. Such genetic markers suggest the genetic layout of Southern Chinese peoples is quite similar to that of Southeast Asians.

Other controversies completely reject the 'Two Layer' hypothesis. Using dental evidence, Turner’s [[Sinodonty and Sundadonty|Sundadont/Sinodont]] hypothesis suggests the “Sundadont” trait seen in present-day Southeast Asians is a result of long-standing continuity. Turner created a cluster analysis of MMD values in order to test existing hypotheses of origins,<ref name="turner" /> concluding that all Southeast Asians, Micronesians, [[Polynesians]], and Jomonese form their own branch and descend from a common ancestor. The Australians and Melanesians, however, are scattered over the African and European branch along with a side branch of [[Aboriginal Tasmanian|Tasmanians]] and [[Solomon Islands#Early history|Solomon Islanders]]. Howell analyzed crania of major racial branches worldwide, and linked Australian and Melanesian cranial morphology most closely with African cranials. Howell discovered, however, that the size and features of present-day Asian cranial morphology differed significantly from that of Australians, Melanesians, and Africans.<ref name="turner" />

== See also ==

*[[Indochina|Mainland Southeast Asia]]
*[[Southeast Asian History]]
*[[Early human migrations]]

== References ==

{{reflist}}

== Further reading ==

*Antón, S. C. (2002). Evolutionary significance of cranial variation in Asian Homo erectus. American Journal of Physical Anthropology, 118(4), 301-323.
*Bellwood, P. (2007). Prehistory of the Indo-Malaysian archipelago. ANU E Press.
*Hill, C., Soares, P., Mormina, M., Macaulay, V., Clarke, D., Blumbach, P. B., ... & Richards, M. (2007). A mitochondrial stratigraphy for island southeast Asia. The American Journal of Human Genetics, 80(1), 29-43.
*Nguyen, V. (2005). The Da But culture: Evidence for cultural development in Vietnam during the middle Holocene. Bulletin of the Indo-Pacific Prehistory Association, 25, 89-94.
*Oota, H., Kurosaki, K., Pookajorn, S., Ishida, T., & Ueda, S. (2001). "Genetic study of the Paleolithic and Neolithic Southeast Asians. Human biology", 73(2), 225-231.
*Pookajorn, S., Sinsakul, S., & Chaimanee, Y. (1994). "Final report of excavations at Moh-Khiew Cave, Krabi Province; Sakai Cave, Trang Province and ethnoarchaeological research of hunter-gatherer group, so-called Mani or Sakai or Orang Asli at Trang Province (the Hoabinhian Research Project in Thailand)". Bangkok: Silpakorn University.
*Storm, P. (2001). The evolution of humans in Australasia from an environmental perspective. Palaeogeography, Palaeoclimatology, Palaeoecology, 171(3), 363-383.

== External links ==
*[http://s1.zetaboards.com/anthroscape/topic/4742474/1/ Maps of early Austronesian settlement routes]
*[http://www.bradshawfoundation.com/journey/kota.html Kota Tampan tools and settlement of Southeast Asia]

{{Archaeology |state=collapsed}}
{{Southeast Asia |state=collapsed}}

[[Category:Human migrations]]
[[Category:Archaeological theory]]
[[Category:Archaeology]]
[[Category:Asian archaeology]]
[[Category:Pleistocene]]
[[Category:Holocene]]