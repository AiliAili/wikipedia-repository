{{Orphan|date=March 2016}}
{{Infobox college volleyball team
|name = Hawaii–Hilo Vulcans
|logo = Hawaii–Hilo Vulcans logo.png
|logo_size = 175px
|university = University of Hawai'i Hilo 
|conference = Pacific West Conference
|conference_short = PacWest
|city = Hilo
|stateabb = HI
|state = Hawai'i
|coach = Gene Krieger
|tenure = 1st
|arena = Hilo Civic Auditorium 
|capacity = 3,000
|nickname = [[Hawaii–Hilo Vulcans|Vulcans]]
|NCAAchampion = 1979, 1981
|NCAArunnerup = 1978, 1980
|NCAAfinalfour = 1978, 1979, 1980, 1981
|NCAAeliteeight = NAIA: 1981, 1982, 1983, 1984, 1985, 1987, 1988, 1989, 1991, 1992, 1993<ref>{{cite web|url=http://www.naiahonors.com/records/Volleyball_ChampionshipRecrods.pdf|title=Novel NAIA Spaced Pertinent Info.}}</ref>
|NCAAsweetsixteen = 
|NCAAtourneys = 1978, 1979, 1980, 1981, 1994, 1995, 1997, 2009, 2011
|conference_tournament = 
|conference_season = NAIA: 1981, 1982, 1983, 1984, 1985, 1988, 1993 NCAA: 1994, 1995, 1997, 2007, 2009
}}
[[File:VulcansBanner.jpg|thumb|Hui Kaua~ "Contend" (Present Progressive Tense)]]
The '''Hawaii–Hilo Vulcans women's volleyball''' team is the intercollegiate women's volleyball team of the [[University of Hawaii at Hilo]]. One of the original "traditionals" in the world of small schools volleyball, the Vulcans started out as National Runners Up right out of the gates in 1978. The 1978 [[AIAW]] Div.II team was led by Cheryl Ching, Kawehi Ka'a'a, Vetoann Baker and Lyndell Lindsey.<ref>{{cite web|url=http://fs.ncaa.org/Docs/stats/w_volleyball_RB/2009/AIAW.pdf|title=AIAW Champions' Results}}</ref> Coach Sharon Peterson (a inaugural 1988 NAIA Hall of Fame honoree) was in fact coach back then, but she had been preceded by Coach [[Mike Wilton]] for partial foundations in some of 1977. On 6 March 2017, a UHH press release named Mr. Gene Krieger as the new Vulcans' Head Coach.<ref>{{cite web|url=http://hiloathletics.com/news/2017/3/6/womens-volleyball-krieger-named-uh-hilo-volleyball-leader.aspx|title=New 2017 Coach}}</ref>

In the first week of December, in 1979 (Orlando, FL), UHH won Hawai'i its first ever National Championship of volleyball.  Later that evening, [[Hawaii Rainbow Wahine volleyball]] would also win a national title for large national colleges (at Central Standard Time). All-American Cheryl Ching would go on to win the [[Honda-Broderick Cup]] in 1980, moreover, the first in the 50th State to do so. The program as a matter of circumstance would go from AIAW Division II to NAIA powers in 1981.  [[Hilo]] is still the only multi-Champion, multi-Divisional program to win simultaneous collegiate championships in a single year (1981).<ref>{{cite web|url=http://hiloathletics.com/sports/2013/7/22/GEN_0722134119.aspx?path=general|title=Vulcans: The University of Hawaii at Hilo Championships}}</ref>

==Head coaching==
* 1978–2002: Sharon Peterson (511 W's – 251 L's)
* 2003–2005: Julie Morgan
* 2006: Carla Carpenter-Kabalis (Interim)
* 2007–10: Bruce Atkinson (Responsible for two AVCA Division II HM All-Americans from Brazil (C/O 2008–09))
* 2010–2016: Tino Reyes

Throughout Coach Peterson's tenure, the Vuls as a sampling of series records would go: 2–0 against the [[Washington Huskies women's volleyball|Washington Huskies]], 3–1 against the [[BYU Cougars women's volleyball|BYU Cougars]], 0–1 against the [[Stanford Cardinal women's volleyball|Stanford Cardinal]] and, further, were solidly able to compete with a 1978, #4 ranked [[Pepperdine Waves women's volleyball|Pepperdine Waves]] WVB team touring the Hawaiian Islands.

==History==
From 1981–1984, UH Hilo was the first USA college team to win four consecutive national titles, regardless of divisional league.<ref>{{cite web|url=http://www.hawaiisportshalloffame.com/cms/index.php?page=paterson-sharon|title=Pioneer Sharon Peterson}}</ref>

In 1988 the program's zenith topped out with a dominant [[National Association of Intercollegiate Athletics|NAIA]] National Championship season.  Hawai'i small schools' glory was epitomized with a starting six, of seven, being awarded All-American certificates.  Florence Alo (MVP / 2x 1st Team AA (1987–88)), Jessica Strickland (1st Team), Debra Namohala (2nd Team (1987 AA)), Edna Togiai (1st Team), Hae Ja Kim (2nd Team) and Sheila Scott (1st Team) who would, the latter, complete this list.<ref>{{cite web|url=https://books.google.com/books?id=s4QWvle-CR8C&pg=PA389&lpg=PA389&dq=Flo+Alo+Volleyball&source=bl&ots=mBLHdQpH6q&sig=ekr16VbHwaDXQpkffyTeCBP8k0g&hl=en&sa=X&ved=0ahUKEwj35NbNo4jLAhVW2WMKHdJPC-MQ6AEIKzAC#v=onepage&q=Flo%20Alo%20Volleyball&f=false|title=List of Hawai'i small schools all-Americans}}</ref>

In 1993, runners-up UHH would lose its final match of the NAIA in the National Championship game.   
In 1994, UH Hilo would once again return to the NCAA Division II league.

They went a winning 15-7 in the 2011-12 school's calendar year; a, one, Marley Strand-Nicolaisen, the Kau High School junior, recruited from Naalehu, HI also.   
==Additional notable players==
*Kawehi Ka'a'a: 1978–79 All-American, multi-AIAW Final Four participant (1978–79 & 1980–81 at UH Manoa) 
*Sweetie Lindsey-Osorio: An original 1977 recruit. A National Champion All-American (1979). A Haili Congregational (church) elder, who's been in on organized international tournaments her entire life. The Osorio's have further been hosts to multi-USAV events.  
*Edie Manzano: 4x National Champion (1979, 1981, 1982). Thrice 1st Team All-American (AIAW, NAIA) as program's second [[Honda-Broderick Cup]] winner. '88-NAIA Hall of Fame awardee at setter.   
*Carla Carpenter-Kabalis: Three-time 1st Team All-American (AIAW-1980-81 & NAIA-1981), 5x National Championships finalist, mother to Kuulei (member of the USAV's Premier VB League pro-Florida Wave Team at libero).   
*Alofa Tagataese (1980–1983): Three-time All-American who holds the UHH efficiency, Highest Kill Percentage record at .447 for a career.
*Nalani Spencer (1981–84): 5x National Champion. Three-time (NAIA-83/84, USAV-Berkeley Nationals) 1st Team All-American.
*Laurie Kemp: Towering Cosmopolitan, dime'd-doz., into Vulcans Hall of Fame (2003). O'ahu Volleyball Association quasi-Pro Beach VB star. 
*Tanya Fuamatu (1992–1995): 1993 NAIA P.O.Y., 1994–95 PacWest P.O.Y./AVCA Div.II 1st Team All-American, UHH career digs leader at 1,559. 
*Fabiane Seben and Josimara Pinheiro: 2008 HM AVCA Div.II All-Americans
*Hillary Hurley (2007–2010): 2007 PacWest/AVCA Pacific Region F.O.Y., 2010 PacWest P.O.Y., Professional Puerto Rican League athlete (mid 2000-10s)

==References==
{{reflist}}

==Further reading==
* ''Hawai'i Sports: History, Facts, and Statistics'' by Dan Cisco

==External links==
* {{Official website|1=http://hiloathletics.com/index.aspx?path=wvball&}}

{{DEFAULTSORT:Hawaii-Hilo Vulcans women's volleyball}}
[[Category:Hawaii–Hilo Vulcans women's volleyball|*]]