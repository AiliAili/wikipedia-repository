{{Infobox stadium
|stadium_name      = Russ Chandler Stadium<br/>at Rose Bowl Field
|nickname          = "The Rusty C"
|former_names      = Rose Bowl Field (1930-1985)
|image             = [[File:Russ Chandler Stadium.jpg|300px]]
|location          = 255 Ferst Drive, N.W.<br/>[[Atlanta, Georgia]] 30318
|opened            = 1930
|renovated         = 1985, 2002
|owner             = [[Georgia Institute of Technology|Georgia Tech]]
|operator          = Georgia Tech Athletic Association
|surface           = Grass
|construction_cost = $9,700,000 [[United States dollar|USD]] (2002 renovation)
|architect         = Hellmuth, Obata & Kassabaum (HOK)
|tenants           = [[Georgia Tech Yellow Jackets baseball]] ([[Atlantic Coast Conference]])<br />[[Atlantic Coast Conference Baseball Tournament|ACC Tournament]] ([[1985 Atlantic Coast Conference Baseball Tournament|1985]])
|seating_capacity  = Chairback Seats: 1,100<br/>Bench Seats: 3,057<br/>Total Seats: 4,157
|dimensions        =  Left Field - 328 ft (100 m)<br/>Left Center Field - 391 ft (119 m)<br/>Center Field - 400 ft (122 m)<br/>Right Center Field - 353 ft (108 m)<br/>Right Field - 334 ft (102 m)
}}

'''Russ Chandler Stadium''' is a college [[baseball]] [[stadium]] in [[Atlanta, Georgia]].  It has been the home field of the [[Georgia Institute of Technology|Georgia Tech]] [[Georgia Tech Yellow Jackets|Yellow Jackets]] [[college baseball]] [[Georgia Tech Yellow Jackets baseball|team]] since 1930.<ref name="Guide">The Official 2006 Georgia Tech Baseball Media Guide</ref> The current stadium opened in 2002.<ref name="Guide"/>

== History ==
=== Rose Bowl Field ===
The original stadium was built in 1930, using the payoff from the football team's participation in the [[1929 Rose Bowl]].  The entire complex, which included three football practice fields, was named '''Rose Bowl Field.'''  The complex stood behind a stone wall along 5th and Fowler streets.

In 1971, the permanent grandstand was torn down to make way for the extension of 5th Street.  Lights were added in 1983.
=== Original stadium ===
The stadium existed with only bleacher seats until 1985, when A. Russell Chandler, III (BSIE '67) funded construction of a new grandstand that opened in time for Tech's centennial year.<ref name="Guide"/> Fans of Georgia Tech baseball affectionately called it "The Rusty C" due to its extensive use of aluminum as a construction material.

=== Current stadium ===
The stadium was completely rebuilt in 2002. The new stadium features more brick and less aluminum in its construction materials than the previous one, but "Beesball" fans still affectionately refer to it as "The Rusty C." The stadium is located on the Georgia Tech campus in the heart of Atlanta's midtown area and offers fantastic views of the Atlanta skyline.<ref>{{cite news|first=Al|last=Przygocki|url=http://www.nique.net/issues/2002-02-08/sports/1|title=The New Russ Chandler Stadium: Flashy beginning for baseball|work=[[The Technique]]|date=2002-02-08|accessdate=2007-09-15}} {{Dead link|date=October 2010|bot=H3llBot}}</ref> On April 9, 2008, the stadium set an attendance record of 4,609 for the Yellow Jackets versus the Georgia Bulldogs game held that night. The crowd totally eclipsed previous marks for both post-season (4,468 vs. Southern California on June 2, 2000) and regular-season games (4,264 vs. Georgia on March 27, 2002). One ranking of college baseball stadium experiences ranks it three <ref>{{cite web|url=http://www.stadiumjourney.com/stadiums/russ-chandler-stadium-s1967 |title=Archived copy |accessdate=2014-07-15 |deadurl=no |archiveurl=https://web.archive.org/web/20160529095629/http://www.stadiumjourney.com/stadiums/russ-chandler-stadium-s1967/ |archivedate=2016-05-29 |df= }}</ref>

== Description ==
[[File:Russ Chandler vs OSU.JPG|thumb|right|250 px|Russ Chandler Stadium during a game against [[Ohio State Buckeyes baseball|Ohio State]].]]
=== Field ===
'''Field Dimensions:'''
*Left Field: either 328 or 329 feet (~100m), depending on whether you believe the official media guide or the foul line post.
*Left Center: 391 feet (119 m)
*Center Field: 400 feet (122 m)
*Right Center: 353 feet (108 m)
*Right Field: 334 feet (102 m)

Due to the asymmetric shape of the field, the deepest point is actually 409 feet and lies just to the left of center field.

The fence height is a uniform 10 feet.

=== Seating ===
'''[[Seating capacity]]: 4,157'''

This figure includes 1,100 chairback seats (mostly behind home plate, but also the front rows along the first and third baselines and several "open-air" skybox areas) and over 3,000 bleacher seats. There is room for additional expansion of the bleacher sections along the third base line that will bring the total [[seating capacity]] to over 5,000 seats, and there is ample spectator room adjacent to the bullpen along the first base line.<ref name="Guide"/>

In 2013, the Yellow Jackets ranked 42nd among [[List of NCAA Division I baseball programs|Division I baseball programs]] in attendance, averaging 1,525 per home game.<ref name=13att>{{cite web|last=Cutler|first=Tami|title=2013 Division I Baseball Attendance - Final Report|url=http://www.sportswriters.net/ncbwa/news/2013/attendance130611.pdf|work=Sportswriters.net|publisher=NCBWA|accessdate=July 20, 2013|archiveurl=http://www.webcitation.org/6IGG6kpVy?url=http://www.sportswriters.net/ncbwa/news/2013/attendance130611.pdf|archivedate=July 20, 2013|date=June 11, 2013}}</ref>

=== Facilities and features ===
{{Empty section|date=June 2008}}

=== Parking ===
There are no dedicated parking facilities for Russ Chandler Stadium. Recommended surface lots on the Georgia Tech campus in the vicinity of the stadium include the adjacent Klaus College of Computing Parking Deck, Peters Parking Deck, and the Alexander Memorial Colliseum lot. Limited street parking may be available on campus, but those parking along Fowler Street past the right field fence and wall could be subject to damage from long home runs. The O'Keefe lot and others nearby (Architecture and Van Leer Electrical Engineering lots) are no longer available due to construction or other campus projects. Consult The Georgia Tech Athletic Association, Rusty C, or Beesball.com website links provided in the "External links" section below for maps and more detailed information on recommended parking.

==See also==
* [[List of NCAA Division I baseball venues]]

== References ==
{{reflist}}

== External links ==
{{commons category}}
*[http://www.therustyc.com The Rusty C], a fansite maintained by the fans in Section 10 of Russ Chandler Stadium.
*[http://www.beesball.com Beesball.com], a very popular fansite with extensive information on the Georgia Tech Baseball program.
*[http://ramblinwreck.cstv.com/sports/m-basebl/geot-m-basebl-body.html Official Georgia Tech Baseball website]
*[http://www.parking.gatech.edu/info/1_maps__schedules/1_parking_areas.php Georgia Tech Campus Parking Map]
{{coord|33.777491|-84.394771|region:US_type:landmark|display=title}}

{{Georgia Tech Navbox}}
{{Atlantic Coast Conference baseball venue navbox}}
{{Georgia NCAA Division I college baseball venue navbox}}

[[Category:College baseball venues in the United States]]
[[Category:Sports venues in Atlanta]]
[[Category:Georgia Institute of Technology buildings and structures]]
[[Category:Georgia Tech Yellow Jackets baseball]]
[[Category:Baseball venues in Georgia (U.S. state)]]
[[Category:1930 establishments in Georgia (U.S. state)]]
[[Category:Sports venues completed in 1930]]