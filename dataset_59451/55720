{{Infobox journal
| title = Arthritis & Rheumatology
| formernames = Arthritis & Rheumatism
| cover =
| editor = [[Joan M. Bathon]]
| discipline = [[Rheumatology]]
| abbreviation = Arthritis Rheumatol.
| publisher = [[John Wiley & Sons]]
| country = 
| frequency = Monthly
| history = 1958–present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 7.764
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2326-5205
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2326-5205/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2326-5205/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 01514332
| LCCN = 59037291
| CODEN = ARHEAW
| ISSN = 0004-3591
| eISSN = 1529-0131
}}
'''''Arthritis & Rheumatology''''' is a monthly [[peer-reviewed]] [[medical journal]] covering the natural history, [[pathophysiology]], treatment, and outcome of the [[rheumatism|rheumatic diseases]]. It is an official journal of the [[American College of Rheumatology]].

It was established in 1958 as ''Arthritis & Rheumatism'' and obtained its current name in 2014.<ref>{{cite web  | title = Arthritis & Rheumatism (A&R) Gets a New Name | work = THE RHEUMATOLOGIST | publisher = American College of Rheumatology  | date = August 2013 | url = http://www.the-rheumatologist.org/details/article/5072361/Arthritis__Rheumatism_AR_Gets_a_New_Name.html | accessdate = 2015-02-27 }}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 7.764.<ref name=WoS>{{cite book |year=2015 |chapter=Arthritis & Rheumatology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2326-5205}}
* [http://www.rheumatology.org/ American College of Rheumatology]

{{DEFAULTSORT:Arthritis and Rheumatology}}
[[Category:Publications established in 1958]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Rheumatology journals]]

{{med-journal-stub}}