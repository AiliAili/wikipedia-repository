{{Good article}}
{{Infobox album
| Name        = Brothers and Sisters
| Type        = [[Album]]
| Artist      = [[The Allman Brothers Band]]
| Cover       = Brothersandsistersallmanbrother.jpg
| Released    = August 1973
| Recorded    = October–December 1972<br/>Capricorn Sound Studios<br/><small>([[Macon, Georgia|Macon]])</small>
| Genre       = {{hlist|[[Country rock]]|[[Southern rock]]|[[blues rock]]}}
| Length      = 38:21
| Label       = [[Capricorn Records|Capricorn]]
| Producer    = Johnny Sandlin, The Allman Brothers Band
| Last album  = ''[[Beginnings (The Allman Brothers Band album)|Beginnings]]''<br />(1973)
| This album  = '''''Brothers and Sisters'''''<br />(1973) |
| Next album  = ''[[Win, Lose or Draw (album)|Win, Lose or Draw]]''<br />(1975)
  | Misc        = {{Singles
  | Name           = Brothers and Sisters
  | Type           = studio
  | single 1       = [[Ramblin' Man (The Allman Brothers Band song)|Ramblin' Man]]" / "Pony Boy
  | single 2       = [[Jessica (instrumental)|Jessica]]" / "Come and Go Blues
  | single 1 date  = August 1973
  | single 2 date  = December 1973
  }}
}}

'''''Brothers and Sisters''''' is the fourth [[studio album]] by American [[Rock music|rock]] band [[the Allman Brothers Band]]. [[Record producer|Produced]] by Johnny Sandlin and the band themselves, the album was released in August 1973 in the United States by [[Capricorn Records]]. Following the death of group leader [[Duane Allman]] in 1971, the Allman Brothers Band released ''[[Eat a Peach]]'' (1972), a hybrid studio/live album that became their biggest yet. Afterwards, the group purchased a farm in [[Juliette, Georgia]], to become a "group hangout". However, bassist [[Berry Oakley]] was visibly suffering from the death of Duane: he excessively drank and consumed drugs. After nearly a year of severe [[depression (mood)|depression]], Oakley was killed in a motorcycle accident not dissimilar from his friend's in November 1972 making it the last album to feature bassist [[Berry Oakley]], the first to feature bassist [[Lamar Williams]] and pianist [[Chuck Leavell]]. 

The band carried on, adding new members [[Chuck Leavell]] on [[piano]] and [[Lamar Williams]] on [[bass guitar|bass]]. ''Brothers and Sisters'' was largely recorded over a period of three months at Capricorn Sound Studios in [[Macon, Georgia|Macon]], Georgia. Lead guitarist [[Dickey Betts]] assumed the role of band leader, and many of his compositions reflected a more [[country music|country]]-inspired sound. Session musicians [[Les Dudek]] and Tommy Talton sat in to play guitar on several songs. The album was being produced at the same time as vocalist/organist [[Gregg Allman]]'s solo debut, ''[[Laid Back (album)|Laid Back]]'', and features many of the same musicians and engineers. The front album cover features a photograph of Vaylor Trucks, the son of drummer [[Butch Trucks]] and his wife Linda. The back cover features a photograph of Oakley's daughter Brittany and his wife Linda.

The album represented the Allmans' commercial peak: it has sold over seven million copies worldwide, landing it at the time atop of the [[Billboard 200|Top 200 Pop Albums]] for five weeks. "[[Ramblin' Man (The Allman Brothers Band song)|Ramblin' Man]]" became the band's first and only [[hit single]], peaking at number two on the [[Billboard Hot 100|''Billboard'' Hot 100]] in 1973. The album was followed by a tour of arenas and stadiums, but marred by drug problems, strained friendships and miscommunication between the group members.

==Background==
[[File:Allman Brothers at the Farm.png|thumb|right|The Allman Brothers Band at "the Farm" in [[Juliette, Georgia|Juliette]], [[Georgia (U.S. state)|Georgia]], 1973.]]
Shortly following their leader [[Duane Allman]]'s death in 1971, the Allman Brothers Band released ''[[Eat a Peach]]'', a hybrid studio/live album that became their biggest release yet; it peaked at number four on the ''[[Billboard (magazine)|Billboard]]'' charts. The band performed nearly 90 shows in the following year, touring as a five-piece.{{sfn|Paul|2014|p=173}} The band also purchased 432 acres of land in [[Juliette, Georgia|Juliette]], [[Georgia (U.S. state)|Georgia]] for $160,000 and nicknamed it "the Farm"; it soon became a "group hangout" and fulfilled bassist [[Berry Oakley]]'s communal dreams.{{sfn|Paul|2014|p=175}} Oakley, however, was visibly suffering from the death of Duane: he excessively drank and consumed drugs, and was losing weight quickly.{{sfn|Paul|2014|p=185}} According to friends and family, he appeared to have lost "all hope, his heart, his drive, his ambition, [and] his direction" following Duane's death.{{sfn|Paul|2014|p=187}} "Everything Berry had envisioned for everybody—including the crew, the women and children—was shattered on the day Duane died, and he didn't care after that," said [[roadie]] Kim Payne.{{sfn|Paul|2014|p=186}}

During recording sessions for their upcoming album, vocalist [[Gregg Allman]] was also working on his solo album, ''[[Laid Back (album)|Laid Back]]'', and the sessions occasionally overlapped.{{sfn|Paul|2014|p=179}} [[Chuck Leavell]] was asked to play [[piano]] for Allman's solo album, and gradually found himself contributing to the Allman Brothers as well.{{sfn|Paul|2014|p=179}} Allman and Betts took turns caring for Oakley, taking him outdoors to places like the zoo to keep him from drinking as much.{{sfn|Allman|Light|2012|p=208}} "Time and time again, I have sat and wondered, 'God, what in the hell could I have done, what could have anybody have done, to help him?'" said Allman.{{sfn|Allman|Light|2012|p=209}} Upon Leavell's entry into the group, Oakley went out of his way to make the new member comfortable.{{sfn|Paul|2014|p=188}} On November 11, 1972, overjoyed at the prospect of leading a jam session later that night, Oakley crashed his motorcycle into the side of a bus, just three blocks from where Duane had been killed in a bike accident.{{sfn|Paul|2014|p=189}} He declined hospital treatment and went home, but gradually grew delirious. He was taken to the hospital shortly thereafter and died of [[Traumatic brain injury|cerebral swelling]] caused by a fractured skull.{{sfn|Paul|2014|p=190}} Oakley was buried directly beside Duane at [[Rose Hill Cemetery (Macon, Georgia)|Rose Hill Cemetery]] in Macon, Georgia.{{sfn|Paul|2014|p=194}}

The band unanimously decided to carry on and arrange auditions for new bassists, with a renewed fervor and determination. Several bassists auditioned, but the band picked [[Lamar Williams]], an old friend of drummer [[Jai Johanny Johanson]]'s from [[Gulfport, Mississippi]].{{sfn|Paul|2014|p=199}} The band immediately recognized that Williams represented the best of both Oakley's style and his own style, and they were pleased with his easygoing demeanor.{{sfn|Paul|2014|p=200}} The addition of Leavell and Williams to the band brought about renewed passion and uplifted spirits within the group.{{sfn|Paul|2014|p=201}}

==Recording and production==
The band began recording ''Brothers and Sisters'' in the autumn of 1972 at Capricorn Studios in Macon, Georgia, prior to Oakley's death.{{sfn|Paul|2014|p=179}} The band had previously started rehearsals for the album in the summer. Allman brought a song he had worked on for a year, titled "Queen of Hearts", but he was drunk and none of the members of the band would listen to him.{{sfn|Allman|Light|2012|p=212-213}} This was the catalyst that led to Allman beginning work on ''Laid Back'', his debut solo album.{{sfn|Allman|Light|2012|p=213}} After Williams helped record the album's third track, "Come and Go Blues", the rest of the recording process was smooth.{{sfn|Allman|Light|2012|p=218}}

Betts became the group's ''[[de facto]]'' leader during the recording process. "It's not like Dickey came in and said, 'I'm taking over. I'm the boss. Do this and that.' It wasn't overt; it was still supposedly a democracy but Dickey started doing more and more of the songwriting," said road manager Willie Perkins.{{sfn|Paul|2014|p=182}} The band first recorded "Wasted Words" and "Ramblin' Man," the latter showcasing a more [[country music|country]]-infused sound.{{sfn|Paul|2014|p=181}} The entire group were initially reluctant to record "Ramblin' Man" — "We knew it was a good song but it didn't sound like us," said drummer [[Butch Trucks]] — but the band gradually headed in a more country direction, as that was Betts' background.{{sfn|Paul|2014|p=183}} Betts was very serious about his songwriting, sitting down each afternoon to write.{{sfn|Allman|Light|2012|p=220}}

The band were adamant about not replacing Duane's position in the band. [[Les Dudek]], the guitarist who would eventually record "Jessica" and "Ramblin' Man," had entered the sessions when he was asked to jam with Betts through mutual friends.{{sfn|Paul|2014|p=180}} The band enjoyed how Dudek played and [[Phil Walden]], the band's manager, seemed set on Dudek being in the band.{{sfn|Paul|2014|p=180}} Word of mouth, propagated by Dudek himself, was that he "got the gig" and had become the Allman Brothers' new co-lead guitarist. "We went looking for this dude to kick his ass. Nobody was going to replace Duane and the very thought of it was infuriating to us," said Trucks.{{sfn|Paul|2014|p=180}} After recording completed, Dudek went on the road with [[Boz Scaggs]] and the [[Steve Miller Band]].{{sfn|Paul|2014|p=209}} He wanted to end his contract with Phil Walden at Capricorn, and part of his release granted him no part of the publishing deal for ''Brothers and Sisters'', including a songwriting credit for "Jessica", as he had no written contract.{{sfn|Paul|2014|p=210}}

==Composition==
{{Listen
 |filename     = Ramblin Man.ogg
 |title       = "Ramblin' Man"
 |description  = "[[Ramblin' Man (The Allman Brothers Band song)|Ramblin' Man]]", written by [[Dickey Betts]], was considerably more [[country music|country]]-inspired than other songs.
 |filename2     = Jessica ABB.ogg
 |title2       = "Jessica"
 |description2  = "[[Jessica (instrumental)|Jessica]]" is an [[instrumental]] with an upbeat opening inspired by Betts's daughter, Jessica. It was later used as the theme for ''[[Top Gear (1977 TV series)|Top Gear]]'' in the UK.
}}
"Ramblin' Man" pre-dates the album considerably, and was first created during songwriting sessions for ''[[Eat a Peach]]''. An embryonic version, referring to a "ramblin' country man," can be heard on the bootleg ''The Gatlinburg Tapes'', featuring the band jamming on an off-day in April 1971 in [[Gatlinburg, Tennessee|Gatlinburg]], [[Tennessee]].{{sfn|Paul|2014|p=181}}  A version more similar to the finished recording was broadcast on ABC's ''[[In Concert (TV series)|In Concert]]'' TV program in December 1972, several months in advance of ''Brothers and Sisters'''s release.

"Jessica" was co-written by Betts and Dudek, although only Betts receives credit. Betts first created the song as an experiment, to test whether or not he could write a song that could be played with just two fingers, in honor of [[Gypsy jazz]] guitar virtuoso [[Django Reinhardt]], who played with two left fingers due to severe burns.{{sfn|Paul|2014|p=202}} When his baby daughter Jessica entered the room and began bouncing around to the melody, Betts attempted to capture her mood with the song.{{sfn|Paul|2014|p=202}} Dudek created the song's bridge when Betts became frustrated with the piece. Dudek was disappointed when he was told he would only be recording the acoustic guitar opening, as Betts felt Dudek performing the harmonies to both "Ramblin' Man" and "Jessica" would lead critics to assume he was a member of the band.{{sfn|Paul|2014|p=204}} Leavell also contributed heavily to "Jessica", mostly on the arrangement.{{sfn|Paul|2014|p=205}}

"Jelly Jelly" was the final song recorded for ''Brothers and Sisters'', credited to Gregg Allman. The song actually contained lyrics from [[Bobby Bland]]'s song of the same name, albeit with a very different melody and arrangement.{{sfn|Paul|2014|p=209}} ''Brothers and Sisters'' concludes with another country-inspired track, "Pony Boy," which showcases Betts' acoustic slide playing.{{sfn|Paul|2014|p=207}} The song was heavily inspired by [[Robert Johnson]] in its building rhythm, and [[Blind Willie McTell]] influenced its humor.{{sfn|Paul|2014|p=208}} The song was based on a true story involving his uncle, who would take his horse out to avoid [[driving under the influence]] (DUI) charges, as the horse knew the direction home.{{sfn|Paul|2014|p=208}} Williams played upright bass on the track to keep it an all-acoustic affair, and Trucks played percussion by banging a piece of plywood on the floor.{{sfn|Paul|2014|p=208}}

==Artwork==
The album's artwork was taken at "the Farm" in [[Juliette, Georgia]]. The cover art features Trucks' son Vaylor, while the back cover featured Oakley's daughter Brittany.{{sfn|Paul|2014|p=225}} The gatefold spread reveals a photo of the band and their extended families.{{sfn|Paul|2014|p=225}}<ref name="billboard3"/> "I have an almost dreamlike memory of the way things were—parties, people giving the horses beer, various people in and out," said Brittany Oakley in 1996.<ref name="people"/> She noted that despite the good memories, "it was painful" following her father's death, which is when the photo was taken.<ref name="people"/> Vaylor Trucks later went on to study at [[Florida State University]], where he formed a band. To promote a concert, they printed covers of the ''Brothers and Sisters'' album cover with the caption "Have you seen me lately?", which led to a sold-out crowd.<ref name=people>{{cite journal| date = June 17, 1996 | title =Face the Music: The Folks Who Graced Some of Rock's Most Memorable Album Jackets Share Their Cover Stories| journal =[[People (magazine)|People]]| volume =45| issue = 24| page =81 | publisher =[[Time Inc.]]| location =[[New York City]]| issn =0093-7673| url =http://www.people.com/people/archive/article/0,,20141545,00.html}}</ref>

==Release==

===Commercial performance===
''Brothers and Sisters'' was an enormous success with near-immediacy; the record went gold in retail sales within 48 hours after shipping began. Capricorn estimated that these early sales were due to hardcore fans of the group.<ref name="billboard1"/> The album sold 760,000 copies in its first three weeks, making it one of the fastest-starting albums in Warner/Elektra/Atlantic Records' history.<ref name=billboard1>{{cite journal| last =Freedland| first =Nat| date =September 15, 1973| title =Allman's 5th Instant Click Based on Built-In Demand| journal =[[Billboard (magazine)|Billboard]]| volume =85| issue = 37| pages =1, 48 | publisher =[[Prometheus Global Media]]| location =[[New York City]]| issn =0006-2510| url =https://books.google.com/books?id=CQkEAAAAMBAJ&pg=RA1-PA48&dq=billboard+allman+brothers+sisters&hl=en&sa=X&ei=NFbAU47KGY-vyAS4-oDYDw&ved=0CCAQ6AEwAQ#v=onepage&q=allman&f=false }}</ref> Billboard called it the "success story of the summer," noting that there was no "sustained merchandising promotion effort needed" on the LP.<ref name="billboard1"/> Record stores credited the album with bringing business back to their stores in a lagging season.<ref name="billboard1"/> ''Brothers and Sisters'' was the band's best-performing album on the charts: it logged five weeks as the number one album in the country on the [[Billboard 200|''Billboard'' Top LP's & Tape]] chart.<ref name=billboard>{{cite journal| last =Mayfield| first =Geoff| date =December 18, 1999| title =ABB on the Charts| journal =[[Billboard (magazine)|Billboard]]| volume =111| issue = 51| page =26 | publisher =[[Prometheus Global Media]]| location =[[New York City]]| issn =0006-2510| url =https://books.google.com/books?id=iggEAAAAMBAJ&pg=PA26&dq=billboard+allman&hl=en&sa=X&ei=vVXAU9etO8WGyASnxYHABg&ved=0CB4Q6AEwAA#v=onepage&q=billboard%20allman&f=false }}</ref>

Capricorn executives were split between issuing "Wasted Words" or "Ramblin' Man" as the lead single. National promotion director Dick Wooley sent advance tapes of "Ramblin' Man" to [[Atlanta]] and [[Boston]] radio stations and "listener phone-in reaction was near-phenomenal."<ref name="billboard1"/> "Ramblin' Man" became a rare [[Rock music|rock]] hit on [[AM radio|AM stations]] nationwide, and it rose to number two on the [[Billboard Hot 100|''Billboard'' Hot 100]].{{sfn|Paul|2014|p=225}}<ref name="billboard1"/> Although "Jessica" rose no higher than number 65 on the Hot 100, it later became a staple of [[classic rock]] radio.<ref name="billboard"/> It was later employed as the [[theme song]] to the television program [[Top Gear (1977 TV series)|Top Gear]] in the UK.<ref>{{citeweb|title=Top Gear saw Guns N’ Roses guitarist Slash take to the track|date=March 11, 2012|accessdate=October 2, 2014|url=http://metro.co.uk/2012/03/11/top-gear-series-18-episode-7-tv-review-348613/|journal=[[Metro (British newspaper)|Metro]]}}</ref>

''Brothers and Sisters'' has since sold over seven million copies worldwide.<ref name="people"/>

===Critical reception===
Reviews of ''Brothers and Sisters'' in 1973 were mostly positive. Bud Scoppa of ''[[Rolling Stone]]'' deemed the album "no masterpiece, but the new band has shown that it can carry on the work of the old, and add the appropriate new twists when necessary. They've finally discovered a form that feels as natural in the studio as it does in front of their people. It's heartening to see a group of this commercial and critical stature still working so hard at getting even better."<ref name=rs>{{cite journal|author=Bud Scoppa| date =September 27, 1973| title = ''Brothers and Sisters'' Review | journal =[[Rolling Stone (magazine)|Rolling Stone]]| volume = | issue = | page = | publisher =[[Jann Wenner|Wenner Media]] [[Limited liability company|LLC]]| location =[[New York City]] | issn =0035-791X | url =}}<!--| accessdate =July 11, 2014--></ref> Janis Schacht of ''[[Circus (magazine)|Circus]]'' was very positive, writing, "Never, even in the face of adversity, do the Allman Brothers quit making strong, hard-driving rock/blues albums.&nbsp;... This is another in the continuing line of quality products from The Allman Brothers Band."<ref name=circus>{{cite journal|author=Janis Schacht| date =November 1973| title = ''Brothers and Sisters'' Review | journal =[[Circus (magazine)|Circus]]| accessdate =July 11, 2014}}</ref> ''Billboard'' called it "A fine blues/rock set from this fine band, featuring top lead vocals from Gregg Allman and Dickey Betts, and the excellent instrumental fusion for which they are particularly well known."<ref name=billboard3>{{cite journal| date =August 25, 1973| title =''Billboard''{{'s}} Top Album Picks| journal =[[Billboard (magazine)|Billboard]]| volume =85| issue = 34| page =52 | publisher =[[Prometheus Global Media]]| location =[[New York City]]| issn =0006-2510| url =https://books.google.com/books?id=KAkEAAAAMBAJ&pg=PA52&dq=billboard+a+fine+set+dickey+betts&hl=en&sa=X&ei=fWbAU5TTE4yGyASqiYHwBg&ved=0CCAQ6AEwAQ#v=onepage&q=billboard%20a%20fine%20set%20dickey%20betts&f=false}}</ref>

Subsequent reviews have remained positive. Bruce Eder of [[Allmusic]] called ''Brothers and Sisters'' "not quite a classic album, especially in the wake of the four that had appeared previously, but it served as a template for some killer stage performances, and it proved that the band could survive the deaths of two key members."<ref>{{cite web|author=Bruce Eder |url=http://www.allmusic.com/album/brothers-and-sisters-mw0000188946|title=Review: ''Brothers and Sisters''|publisher=[[Allmusic]]|date=|accessdate=July 11, 2014}}</ref> Andrew Mueller of ''[[Uncut (magazine)|Uncut]]'' deemed it their "indisputable commercial peak and arguable creative apogee."<ref name="am">{{cite journal|author=Andrew Mueller |url=http://www.uncut.co.uk/the-allman-brothers-band-brothers-sisters-review|title=The Allman Brothers Band - Brothers & Sisters|journal=[[Uncut (magazine)|Uncut]]|date=June 2013|accessdate=July 11, 2014}}</ref> [[Robert Christgau]], in ''Christgau's Record Guide'', wrote that "Gregg Allman is a predictable singer who never has an unpredictable lyric to work with anyway, and the jams do roll on, but at their best — "Ramblin' Man," a miraculous revitalization of rock's earliest conceit — they just may be the best, and on this album Dickey Betts's melodious spirituality provides unity and renewal."<ref>{{cite news|last=Christgau|first=Robert|authorlink=Robert Christgau|date=|url=http://www.robertchristgau.com/get_artist.php?name=The+Allman+Brothers+Band|title=Consumer Guide: The Allman Brothers Band|accessdate=July 11, 2014}}</ref>

A 2013 four-disc reissue of the album received very positive reviews as well. [[David Fricke]] of ''[[Rolling Stone]]'' gave it four stars, writing, "The road to that symmetry is caught in this four-CD set by a disc of rehearsals and outtakes that sounds like the work of a more brawny, Southern [[Grateful Dead]]."<ref name=rs1>{{cite journal|author=[[David Fricke]]| date =October 7, 2013| title = ''Brothers and Sisters'': 40th Anniversary Super Deluxe Edition | journal =[[Rolling Stone (magazine)|Rolling Stone]]| volume = | issue = | page = | publisher =[[Jann Wenner|Wenner Media]] [[Limited liability company|LLC]]| location =[[New York City]] | issn =0035-791X | url =http://www.rollingstone.com/music/albumreviews/brothers-and-sisters-40th-anniversary-super-deluxe-edition-20131007| accessdate =January 7, 2013}}</ref>
Walter Tunis of the ''[[Lexington Herald-Leader]]'' wrote that "The larger set is costlier, about $65, but the edition's two live discs chronicle the Chuck Leavell/Lamar Williams-era Allmans as exquisitely as ''[[At Fillmore East|Fillmore East]]'' did the groundbreaking Duane Allman/Berry Oakley lineup."<ref name=k>{{cite journal|author=Walter Tunis| date =August 5, 2013| title =Critic's pick: Allman Brothers Band, 'Brothers and Sisters — Super Deluxe Edition' | journal =[[Lexington Herald-Leader]]| volume = | issue = | page = | publisher =[[The McClatchy Company]]| location =[[Lexington, Kentucky]] | issn = 0745-4260 | url =http://www.kentucky.com/2013/08/05/2750735/critics-pick-allman-brothers-band.html| accessdate =January 7, 2013}}</ref>

==Touring==
{{Quote box
 |quote  = Now leaderless and demoralized, the band was influenced mostly by the road crew that surrounded them. By industry standards the road crew was a cluster-fuck of drugged-out hangers-on that seemingly kept their job by keeping the ABB supplied with drugs and high most of the time. It was crazy and out of control.
 |source = Dick Wooley, Capricorn Records{{sfn|Paul|2014|p=217}}
 |quoted = 1
 |width  = 25%
 |align  = right
}}

After completing ''Brothers and Sisters'', the Allman Brothers Band returned to touring, playing larger venues, receiving more profit and dealing with less friendship, miscommunication and spiraling drug problems.{{sfn|Paul|2014|p=211}} This culminated in a backstage brawl when the band played with [[the Grateful Dead]] at [[Washington, D.C.|Washington]]'s [[RFK Stadium]] in June 1973. The roadies of the Dead dosed the food and drinks of as many people as possible with [[LSD]], holding no compunction about the practice as they felt "evangelical" about the substance.{{sfn|Paul|2014|p=213}} The stage was very crowded with the Dead's entourage, and roadie Kim Payne instructed driver Tuffy Phillips to let no one onstage.{{sfn|Paul|2014|p=214}} When Capricorn promotional chief Dick Wooley attempted to get onstage, Phillips punched Wooley in the nose. Wooley competed in [[martial arts]] and began punching back.{{sfn|Paul|2014|p=214}} When all three roadies engaged in the fight, the Dead's security (the [[Hell's Angels]]) assumed Wooley a "bad guy" and they joined. Soon, the Grateful Dead's roadies realized the situation at hand and they pulled Wooley from the pile.{{sfn|Paul|2014|p=215}}

Walden demanded that whoever was responsible be terminated; as a result, Kim Payne, Mike Callahan, and Tuffy Phillips all were fired. "That was a culmination of everything falling apart.&nbsp;... They were a symptom of the problem—not the problem itself," said Perkins.{{sfn|Paul|2014|p=215}} The band again joined the Grateful Dead and [[the Band]] in July at [[Watkins Glen International|Watkins Glen Speedway]] in [[New York (state)|New York]]'s [[Finger Lakes]] region, for what was then deemed the largest rock concert ever. 150,000 tickets were sold, but the crowd expanded to nearly 600,000, causing it to be declared a disaster area.{{sfn|Paul|2014|p=221}} People abandoned their cars and walked up to ten miles to attend the concert. The jam between the three bands was later called "garbage" by Trucks, as all involved were under the influence of various substances, including alcohol, [[cocaine]], and [[LSD]].{{sfn|Paul|2014|p=224}} To promote the release of the album, ''[[Don Kirshner's Rock Concert]]'' program staged an episode set in Macon, taping performances from the Allman Brothers Band and [[the Marshall Tucker Band]]. Betts walked offstage midway through the performance, with Walden chasing him down the street and urging him to return.{{sfn|Paul|2014|p=225}} "Walking off the stage was a first for me. It was like, 'Dude, what are you doing? We're all brothers here. If there's a problem, let's talk about it,'" said Leavell. {{sfn|Paul|2014|p=227}}

The band concentrated on playing arenas and stadiums as their drug use escalated. In 1974, the band were regularly making $100,000 per show, and were renting [[the Starship]], a customized [[Boeing 720]]B used by [[Led Zeppelin]] and [[the Rolling Stones]].{{sfn|Paul|2014|p=230}}

==Track listing==
;Side one
# "Wasted Words" ([[Gregg Allman]]) - 4:20
#* Gregg Allman - [[lead vocals]], [[rhythm guitar]]
#* Dickey Betts - [[slide guitar]]
#* Chuck Leavell - [[piano]]
#* Berry Oakley - [[bass guitar|bass]]
#* Butch Trucks - [[drums]]
#* Jaimoe - drums
# "[[Ramblin' Man (The Allman Brothers Band song)|Ramblin' Man]]" ([[Dickey Betts]]) - 4:48
#* Dickey Betts - [[lead guitar]], lead vocals
#* Les Dudek - lead guitar
#* Gregg Allman - [[organ (music)|organ]], [[backing vocals]]
#* Chuck Leavell - piano, backing vocals
#* Berry Oakley - bass
#* Butch Trucks - drums, [[percussion]]
#* Jaimoe - drums, [[congas]]
# "Come and Go Blues" (Allman) - 4:54
#* Dickey Betts - lead guitar
#* Gregg Allman - organ, lead vocals
#* Chuck Leavell - [[electric piano|electric]] and acoustic pianos
#* Lamar Williams - bass
#* Butch Trucks - drums, congas, percussion
#* Jaimoe - drums
# "Jelly Jelly" ([[Billy Eckstine]], [[Earl Hines]]) - 5:46
#* Dickey Betts - lead guitar
#* Gregg Allman - organ, lead vocals
#* Chuck Leavell - piano
#* Lamar Williams - bass
#* Butch Trucks - drums, congas, percussion
#* Jaimoe - drums
;Side two
#<li value="5">"Southbound" (Betts) - 5:11
#* Dickey Betts - lead guitar
#* Gregg Allman - organ, lead vocals
#* Chuck Leavell - piano
#* Lamar Williams - bass
#* Butch Trucks - drums, percussion
#* Jaimoe - drums
# "[[Jessica (instrumental)|Jessica]]" (Betts) - 7:31
#* Dickey Betts - lead guitar
#* Les Dudek - [[acoustic guitar]]
#* Gregg Allman - organ
#* Chuck Leavell - electric and acoustic pianos
#* Lamar Williams - bass
#* Butch Trucks - drums, [[timpani]], percussion
#* Jaimoe - drums, congas
# "Pony Boy" (Betts) - 5:51
#* Dickey Betts - [[Dobro]], lead vocals
#* Tommy Talton - acoustic guitar
#* Chuck Leavell - piano
#* Lamar Williams - [[acoustic bass]]
#* Butch Trucks - drums <ref> http://www.discogs.com/The-Allman-Brothers-Band-Brothers-And-Sisters/release/3020121 </ref>

==Personnel==
All credits adapted from liner notes.<ref name="linernotes">{{cite AV media notes | title=Brothers and Sisters| year=1973 | others=[[The Allman Brothers Band]] | type=liner notes | publisher=[[Capricorn Records|Capricorn]] | location=[[United States|US]] | id=CP 0111}}</ref>
{{col-start}}
{{col-2}}
;The Allman Brothers Band
*[[Gregg Allman]]&nbsp;– [[lead vocalist|vocals]], [[organ (music)|organ]], [[rhythm guitar]] on "Wasted Words", [[backing vocals]] on "Ramblin' Man"
*[[Dickey Betts]]&nbsp;– [[lead guitar]], vocals on "Ramblin' Man" and "Pony Boy", [[slide guitar]] on "Wasted Words"
*[[Berry Oakley]]&nbsp;– [[bass guitar]] on "Wasted Words" and "Ramblin' Man"
*[[Jai Johanny Johanson]]&nbsp;– [[drum kit|drums]], congas on "Ramblin' Man" and "Jessica"
*[[Butch Trucks]]&nbsp;– drums, percussion, tympani on "Jessica", congas on "Come and Go Blues" and "Jelly Jelly"
*[[Chuck Leavell]]&nbsp;– [[piano]], backing vocals on "Ramblin' Man", [[electric piano]] on "Jessica" and "Come and Go Blues"
*[[Lamar Williams]]&nbsp;– bass guitar
{{col-2}}
;Additional musicians
*[[Les Dudek]] – co-lead guitar on "Ramblin' Man", acoustic guitar on "Jessica"
*Tommy Talton - acoustic guitar on "Pony Boy"

;Production
*Johnny Sandlin&nbsp;– [[Sound recording and reproduction|production]], [[Audio engineer|engineer]], [[remix]]
*Ovie Sparks&nbsp;– engineer, remix
*Buddy Thornton&nbsp;– engineer
*[[George Marino]]&nbsp;– [[mastering engineer]]
*Judi Reeve&nbsp;– graphic content
*Dan Hudson, Jr.&nbsp;– [[photography]]
*Bo Meriwether&nbsp;– photography
*Barry Feinstein&nbsp;– layout, design
*Vicki Hodgett&nbsp;– layout, design
{{col-end}}

==Charts==
{{col-begin}}
{{col-2}}

===Weekly charts===
{|class="wikitable"
|-
! Chart (1973)
! Peak<br />position
|-
{{albumchart|Germany|52|artist=Allman Brothers|album=Brothers and Sisters|accessdate=July 30, 2013}}
|-
{{albumchart|Netherlands|10|artist=Allman Brothers|album=Brothers and Sisters|accessdate=July 30, 2013}}
|-
| [[Billboard 200|''Billboard'' Top LP's & Tape]]
| style="text-align:center;"| 1
|}

{{col-2}}

===Certifications===
{{Certification Table Top}}
{{Certification Table Entry|region=United States|title=Brothers and Sisters|artist=Allman Brothers|type=album|award=Platinum|relyear=1973|certyear=1995|autocat=yes}}
{{Certification Table Bottom}}
{{col-end}}
{{s-start}}
{{succession box
  | before = ''[[A Passion Play]]'' by [[Jethro Tull (band)|Jethro Tull]]
  | title  = [[Billboard 200|''Billboard'' 200]] [[Number-one albums of 1973 (USA)|number one album]]
  | years  = September 8, 1973 - October 12, 1973
  | after  = ''[[Goats Head Soup]]'' by [[The Rolling Stones]]}}
{{end}}

==Notes==
{{Reflist|colwidth=20em}}

==References==
* {{cite book
|ref       = harv
|last      = Paul
|first     = Alan
|title     = One Way Out: The Inside History of the Allman Brothers Band
|year      = 2014
|publisher = St. Martin's Press
|isbn      = 978-1-250-04049-7}}
* {{cite book
|ref       = harv
|last      = Freeman
|first     = Scott
|title     = Midnight Riders: The Story of the Allman Brothers Band
|year      = 1996
|publisher = Little, Brown and Company
|isbn      = 978-0-316-29452-2}}
* {{cite book
|ref       = harv
|last      = Poe
|first     = Randy
|title     = Skydog: The Duane Allman Story
|year      = 2008
|publisher = Backbeat Books
|isbn      = 978-0-87930-939-8}}
* {{cite book
|ref       = harv
|last1      = Allman
|first1     = Gregg
|last2      = Light
|first2     = Alan
|title     = My Cross to Bear
|year      = 2012
|publisher = William Morrow
|isbn      = 978-0-06-211203-3}}

==External links==
* {{Official website|http://www.allmanbrothersband.com/}}

{{Allman Brothers Band}}

[[Category:The Allman Brothers Band albums]]
[[Category:1973 albums]]
[[Category:Capricorn Records albums]]
[[Category:English-language albums]]