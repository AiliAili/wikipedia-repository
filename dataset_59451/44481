{{Use dmy dates|date=October 2013}}
{{Use British English|date=October 2013}}
{{Infobox book 
| name          = Dariel
| image         = Dariel by R D Blackmore - 1897 frontispiece.png
| caption       = Frontispiece to the 1897 edition
| author        = [[R. D. Blackmore]]
| country       = United Kingdom
| language      = English
| genre         = 
| publisher     = 
| pub_date      = 1897
| pages         = 
| isbn          = 
}}
'''''Dariel: a romance of Surrey''''' is a novel by [[R. D. Blackmore]] published in 1897. It is an adventure story set initially in [[Surrey]] before the action moves to the [[Caucasus Mountains|Caucasian mountains]]. The story is narrated by George Cranleigh, a farmer who falls in love with Dariel, the daughter of a Caucasian prince. ''Dariel'' was the last of Blackmore's novels, published just over two years before his death.<ref name="lonquart">The London Quarterly Review, (1926), page 53</ref>

==Plot==
The story is narrated by George Cranleigh,<ref name="pub">''The Publisher'', (1897), Volume 14, Issue 67, page 689</ref> a younger son of Lord Harold Cranleigh, a destitute landowner in Surrey,<ref name="revofrev">''The Review of Reviews'', Volume 17, page 88</ref> 
who has been ruined, according to Blackmore, by the "farce of Free-trade."<ref name="spec">[http://archive.spectator.co.uk/article/25th-december-1897/22/recent-novels-the-dimensions-of-john-oliver-hobbes Recent Novels], ''The Spectator'', page 22, 25 December 1897</ref>

In the opening chapter George, riding home from market, surprises a maiden of surpassing beauty upon her knees in a ruined chapel.<ref name="athen">''The Athenaeum'', (1897), Vol. 2., page 782</ref> She proves to be Dariel, the daughter of Sur Imar, a prince of the [[Lezgian people|Lesghians]], a wild tribe of the [[Caucasus]].<ref name="revofrev"/> A blood feud has arisen between Imar and his sister, and so he has, with his daughter, his foster-brother Stepan, and a body of retainers, come to England and settled peaceably in a deserted house in Surrey.<ref name="athen"/>

Imar resolves to returns to his native land to educate his tribesmen in the lessons of civilisation.<ref name="revofrev"/> George, who has fallen in love with Dariel, follows her to the East.<ref name="vicweb">[http://www.victorianweb.org/authors/blackmore/dariel.html Dariel (1896)], www.victorianweb.org, retrieved 17 September 2013</ref> But Imar's twin-sister Marva, Queen of the [[Ossets]], who is appropriately called by the natives "the Bride of the Devil," plans to kill Prince Imar and wed his daughter Dariel to her son.<ref name="revofrev"/> After weeks of travelling and days full of desperate adventure, George, with the help of miners and Lesghians, rescues Dariel and her father and kills the wicked Princess and her fiendish son.<ref name="revofrev"/>

==Publication==
''Dariel'' was first serialised in ''[[Blackwood's Magazine]]'' from October 1896 to October 1897, and then published in one volume in 1897.<ref name="cambio">"Richard Doddridge Blackmore" entry in ''The Cambridge Bibliography of English Literature: 1800-1900'', (1999), Cambridge University Press. ISBN 0521391008</ref> It was the only one of his novels which was first published as one volume.<ref name="lonquart"/> It was published once more in 1900.<ref name="cambio"/> The novel included 14 illustrations by Miss Chris Hammond.<ref name="pub"/>

==Reception==
''Dariel'' received mixed reviews. ''[[The Spectator]]'' complained that "Mr. Blackmore's method is too leisurely, and his canvas is crowded with characters who, though very engaging in themselves, retard the march of the story,"<ref name="spec"/> and similarly ''[[Athenaeum (British magazine)|The Athenaeum]]'' said that "the length is quite disproportionate either to the number of characters introduced or the complication of the history."<ref name="athen"/> ''The Publisher'', on the other hand, loved the novel, stating that "the book is unquestionably the most important contribution made to fiction this year ... the love element is singularly fresh and delightful, ... the characters are alive in every fibre, and there are scores of those wonderful descriptions of nature in which Mr. Blackmore has no existing peer save [[Thomas Hardy|Mr. Hardy]] or [[George Meredith|Mr. Meredith]]."<ref name="pub"/>

==References==
{{reflist}}

==External links==
{{Gutenberg|no=42529|name=Dariel: A Romance of Surrey}}

{{R. D. Blackmore}}

[[Category:1897 novels]]
[[Category:Novels by Richard Doddridge Blackmore]]
[[Category:Novels set in Surrey]]