{{good article}}
{{Nutritional value
| name = Baconnaise (Regular)
| image = [[File:Baconnaise.jpg|85 px]]
| caption = Plastic jar of Baconnaise
| serving_size = 13 g
| kcal = 80
| fat = 9 g
| satfat = 1.5 g
| transfat = 0 g
| sodium_mg = 85
| carbs = 1 g
| fiber = 0 g
| sugars = 0 g  
| protein = 0 g
| source = [https://web.archive.org/web/20120406091131/http://www.jdfoods.net/images/shop/nutrition-bnaise.gif Baconnaise] on company website
}}
'''Baconnaise''' is a [[bacon]]-flavored [[mayonnaise]]-based product that is [[Ovo vegetarianism|ovo vegetarian]] and [[kashrut|kosher]] certified. It was created by Justin Esch and Dave Lefkow, founders of [[J&D's Down Home Enterprises|J&D's Foods]], in November 2008, and sold 40,000 jars within six months.<ref name="Seattle">{{cite news |title=It's mayo, it's bacon, it's Baconnaise — and sales are sizzling |author=Mark Rahner |url=http://seattletimes.nwsource.com/html/entertainment/2009010469_baconnaise09m.html |newspaper=[[The Seattle Times]] |date=April 28, 2009 |accessdate=June 19, 2011}}</ref> It was [[Test market|test-marketed]] in [[Grand Forks, North Dakota]],  to determine consumer interest.<ref>{{cite news |title=Groceries, entertainment meet with Baconnaise product, Product is being test-marketed in Grand Forks |author= |url=http://nl.newsbank.com/nl-search/we/Archives?p_product=GF&s_site=grandforks&p_multi=GF&p_theme=realcities&p_action=search&p_maxdocs=200&p_topdoc=1&p_text_direct-0=12B579C2422A3AD0&p_field_direct-0=document_id&p_perpage=10&p_sort=YMD_date:D&s_trackval=GooglePM |newspaper=[[Grand Forks Herald]] |date=October 14, 2009 |accessdate=June 21, 2011}}</ref> After being featured on both ''[[The Daily Show]]'' and ''[[The Oprah Winfrey Show]]'', sales of Baconnaise increased significantly, with more than a million jars sold. 

==History==
In an interview with [[ABC News]], Esch and Lefkow stated that they came up with the original idea for bacon products and their first invention, [[Bacon Salt]], while making a joke about the subject over a meal at a diner.<ref name="ABC"/> The money for their [[startup company|startup]] came from the $5000 that Lefkow had obtained while on ''[[America's Funniest Home Videos]]''.<ref name="King">{{cite news|title=Oprah helped put Baconnaise on the map |author=Lori Matsukawa |url=http://www.king5.com/video/featured-videos/Oprah-helped-put-Baconnaise-on-the-map-122477694.html |newspaper=[[KING-TV|KING5]] |date=May 23, 2011 |accessdate=June 21, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20120402203243/http://www.king5.com/video/featured-videos/Oprah-helped-put-Baconnaise-on-the-map-122477694.html |archivedate=April 2, 2012 |df= }}</ref> The idea to make bacon spreadable came from a customer's request.<ref>{{cite web | url=http://www.jdfoods.net/products/tastyspreads | title=Tasty Spreads | publisher=JDFoods.net | accessdate=14 August 2014}}</ref> Together, they created interest in their products by going to numerous sporting events dressed in bacon costumes and used [[social networking]] sites to raise awareness of their company.<ref name="ABC">{{cite video |people=Neal Karlinsky (reporter) |date=April 21, 2009 |title=Craze Over Baconnaise and Bacon Salt |url=http://abcnews.go.com/video/playerIndex?id=7395637 |medium= |language= |trans_title= |publisher=[[ABC News]] |location= |archiveurl= |archivedate= |accessdate=June 21, 2011 |time= |id= |isbn= |oclc= |quote= |ref= }}</ref>

Baconnaise has been featured on the ''[[The Daily Show with Jon Stewart]]'' several times as a repeat joke. In 2009, [[Jon Stewart]] used it in a skit that drew negative attention from ''[[Seattle Post-Intelligencer]]''{{'}}s Leslie Kelly.<ref>{{cite news |title=Sticking up for Baconnaise |author=Leslie Kelly |url=http://www.seattlepi.com/default/article/On-Dining-Go-green-with-our-virtual-pub-crawl-1302062.php#page-2 |newspaper=[[Seattle Post-Intelligencer]] |date=March 10, 2009 |accessdate=June 21, 2011}}</ref> Stewart commented, "Baconnaise, for people who want to get heart disease but, you know, too lazy to actually make bacon."<ref>{{cite news |title=Baconnaise on The Daily Show |author=Ronald Holden |url=http://seattlest.com/2009/02/26/baconnaise_on_the_daily_show.php |newspaper=[[Seattlest]] |date=February 26, 2009 |accessdate=June 21, 2011}}</ref> In 2010, Jon Stewart again lampooned Baconnaise with a fake clip of the billboard in Times Square that drew a response from J&D's Foods. According to J&D's Foods, a plan to run an actual billboard ad was made, but it was declined by [[Comedy Central]].<ref>{{cite web | url=http://www.baconsaltblog.com/2010/05/jon-stewart-baconnaise-billboards.html | title=Jon Stewart Baconnaise Billboards in Times Square | publisher=Bacon Salt Blog | date=5 May 2010 | accessdate=17 January 2014}}</ref> Using it as a prop in a later episode, Stewart referred to Baconnaise as "capitalism's greatest triumph".<ref name="Xconomy">{{cite news |title=Oprah Grabs Some Bacon Salt; Seattle Startup Is Freaking Out |author=Gregory T. Huang |url=http://www.xconomy.com/seattle/2009/04/24/oprah-grabs-some-bacon-salt-seattle-startup-is-freaking-out/ |newspaper=Xconomy |date=April 24, 2009 |accessdate=June 21, 2011}}</ref>

Baconnaise was discussed on ''[[The Oprah Winfrey Show]]'' on April 24, 2009, when Esch and Lefkow were interviewed by [[Oprah Winfrey|Winfrey]] via [[Skype]]. After her guests and she ate sandwiches that used the product, she commented, "Vegetarian and kosher! Thanks Justin and Dave! Get your own Baconnaise!"<ref name="Xconomy"/> After her endorsement, the traffic on the company website and telephones overwhelmed its systems.<ref name="Xconomy"/> Lefkow stated that a year after the show aired over a million jars of Bacconaise were sold.<ref name="King"/>

==Production==
Esch and Lefkow have stated that they started with just mayonnaise and bacon and worked with chefs to determine how to have mayonnaise that tastes like bacon without having bacon as an ingredient. Lefkow stated that developing and refining the taste of Baconnaise resulted in him having "nothing but bacon and mayonnaise for breakfast for the next six months."<ref name=press>{{cite web | url=http://www.issaquahpress.com/2008/10/15/spread-the-news-%E2%80%94-baconnaise-is-delicious/ | title=Spread the news — Baconnaise is delicious | publisher=Issaquah Press | date=15 October 2008 | accessdate=17 January 2014 | author=Hayes, David}}</ref> Lefkow refined the taste of Baconnaise by comparing real bacon to the developing product.<ref name=press />

Baconnaise contains no bacon, [[artificial flavor]]s, or [[Monosodium glutamate|MSG]], but the actual process and ingredients in the product are a [[trade secret]].<ref name="Seattle"/> A complete list of the ingredients includes: soybean oil, water, egg yolk, [[gluconic acid]], yeast extract, stabilizer ([[microcrystalline cellulose]], [[modified food starch]], xanthan gum, guar gum, [[gum arabic]]), [[cultured dextrose]], salt, sugar, dehydrated [[garlic]], [[paprika]], dehydrated [[onion]], spice, natural smoke flavor, natural flavors, [[tocopherols]], [[calcium disodium EDTA]], and autolyzed yeast extract.<ref name=jar />

J&D's Foods also released a light version of Baconnaise. Marketed as Baconnaise Lite, it contains 30 Cal per serving and less fat than the original Baconnaise.<ref>{{cite web | url=http://www.jdfoods.net/products/nutrition | title=Compare the Facts – Baconnaise Nutritional Information | publisher=JD Foods | accessdate=17 January 2014|archiveurl=https://web.archive.org/web/20120413163307/http://www.jdfoods.net/products/nutrition |archivedate=13 April 2012}}</ref> Baconnaise is sold in 15-oz (443-ml) plastic jars.<ref name=jar>{{cite web | url=http://www.baconaddicts.com/products/baconnaise-bacon-flavored-mayonnaise-mayo | title=J&D's Baconnaise Bacon Flavor Mayonnaise Spread | publisher=Bacon Addicts | accessdate=17 January 2014}}</ref>

== Release ==
The product's marketing test was at [[Pike Place Market]] in [[Seattle]], where it sold up to 120 jars a week.<ref name=press /> The product debuted in October 2008, at the Seattle Semi Pro Wrestling League in the Heaven's Night Club. The event featured a costumed fight between mayonnaise and a slab of bacon.<ref>{{cite news |title=Baconnaise Release Party Fights the Fat |author=Keith Axline |url=https://www.wired.com/rawfile/2008/11/baconnaise-release-party-fights-the-fat/#more-39 |newspaper=[[Wired (magazine)|Wired]] |date=November 4, 2008 |accessdate=June 21, 2011}}</ref>

== Reception ==
Will Goldfarb of the website [[Serious Eats]] reviewed Baconnaise, stating, "[it] works fairly well as a sandwich condiment, but the assertive smokiness can overpower mild ingredients."<ref name=will>{{cite web | url=http://aht.seriouseats.com/archives/2011/01/baconnaise-bacon-flavored-mayo-condiment-review.html | title=Baconnaise: Bacon-Flavored Mayo for the Masses | publisher=Serious Eats | date=19 January 2011 | accessdate=16 January 2014 | author=Goldfarb, Will}}</ref> Goldfarb recommended it as a sandwich condiment, but cautioned against using it in dips, salad dressings, and fish dishes.<ref name=will /> The "Baconnaise Lite" was met with a positive review from "Hungry Girl", though the reviewer noted its name was "a bit of an oxymoron".<ref>{{cite web | url=http://www.hungry-girl.com/newsletters/raw/1194-hg-bargains-new-products-cool-finds-and-more | title=Hungry Girl - Monday Newsletter | publisher=Hungry Girl | date=9 February  2009 | accessdate=16 January 2014}}</ref> Baconnaise, while being both vegetarian and kosher-friendly, does not taste like mayonnaise.<ref>{{cite web | url=http://www.yumsugar.com/Taste-Test-Baconnaise-2494409 | title=Taste Test: Baconnaise | publisher=Yum Sugar | date=12 November 2008 | accessdate=17 January 2014}}</ref>

=== Original recipes ===
Original recipes featuring [[animal fat]]-infused mayonnaise, including Baconnaise, were covered on the Serious Eats website.<ref>{{cite web | url=http://www.seriouseats.com/2009/10/the-food-lab-meatonnaise-mayonnaises-mayos-bacon-lamb-duck-beef-fats-science.html | title=The Food Lab: Animal Fat Mayonnaise | publisher=Serious Eats | date=23 October 2009 | accessdate=16 January 2014 | author=Kenji Lopez-Alt, J.}}</ref> The recipe includes crumbled bacon strips, liquid rendered bacon fat, canola oil, egg yolks, and [[Dijon mustard]] with water and lemon juice.<ref>{{cite web | url=http://www.seriouseats.com/recipes/2009/10/baconnaise-meat-mayos-mayonnaise-recipe.html | title=Baconnaise | publisher=Serious Eats | date=23 October 2009 | accessdate=16 January 2014 | author=Kenji López-Alt, J.}}</ref>

==See also==
* [[List of bacon substitutes]]
* [[Mayonnaise]]
{{Portalbar|Food|Bacon}}

==References==
{{reflist}}

==Further reading==
* {{cite news |title=Baconnaisebeviset |author= |url=http://www.aftonbladet.se/nojesbladet/article4975921.ab? |newspaper=[[Aftonbladet]] |date=April 23, 2009 |accessdate=June 21, 2011 |language=Swedish}}
* {{cite news |title=What’s next for the makers of ‘Baconnaise’ |author=Amy Rolph |url=http://blog.seattlepi.com/thebigblog/2010/10/06/whats-next-for-the-makers-of-baconnaise/ |newspaper=[[Seattle Post-Intelligencer]] |date=October 6, 2010 |accessdate=June 21, 2011}}
* {{cite news |title=Taste Test: Baconnaise |author=Tasha Robinson |url=http://www.avclub.com/articles/taste-test-baconnaise,2557/ |newspaper=[[The A.V. Club]] |date=December 9, 2008 |accessdate=June 21, 2011}}

==External links==
* [http://www.jdfoods.net/ Official website]

{{Bacon}}
{{Mayonnaise-based sauces}}
{{Condiments}}

[[Category:American cuisine]]
[[Category:Bacon]]
[[Category:Condiments]]
[[Category:Sauces]]
[[Category:Sauces of the mayonnaise family]]
[[Category:Kosher food]]