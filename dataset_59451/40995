'''Tawkify''' is a dating website that uses human [[Matchmaking|matchmaker]]s instead of computer [[algorithms]] to select matches.<ref>Biddle, Sam. [http://gizmodo.com/5922398/the-best-dating-site-youve-never-heard-of-is-too-smart-for-its-own-good "The Best Dating Site You've Never Heard of Is Too Smart for Its Own Good"], ''[[Gizmodo]]'', 29 June 2012. Retrieved on 4 August 2012.</ref>  Tawkify is a matchmaking company, not a "dating site". That means humans – not machines –  pick your matches, and they use your profile information and photo in that process.<ref name="nytimes">Rosenbloom, Stephanie. [https://www.nytimes.com/2012/04/15/fashion/no-scrolling-required-at-new-dating-sites.html?pagewanted=all "No Scrolling Required at New Dating Sites"], ''[[New York Times]]'', New York, 13 April 2012. Retrieved on 3 August 2012.</ref> 

==Overview==
Tawkify facilitates [[Heterosexuality|heterosexual]] and [[Same-sex relationship|same-sex]] relationships in the [[United States]] and [[Canada]].<ref>Papamarko, Sofi. [http://www.theglobeandmail.com/life/relationships/tawkify-online-dating-with-a-traditional-twist/article4458684/ "Online dating with a traditional twist"], ''[[The Globe and Mail]]'', Toronto, 2 August 2012. Retrieved on August 4, 2012.</ref>

The service's most notable matchmaker is [[E. Jean Carroll]] (co-founder). No user sees any photographs or personal information about any other user.<ref name="theglobeandmail">Papamarko, Sofi. [http://www.theglobeandmail.com/life/relationships/tawkify-online-dating-with-a-traditional-twist/article4458684/ "Tawkify: Online dating with a traditional twist"], ''[[The Globe and Mail]]'', Toronto, 2 October 2012.  Retrieved on 3 August 2012.</ref>Tawkify is a matchmaking service, not a dating site. In a nutshell--actual humans ( Matchmakers) pick all of the matches. They use private profiles to aid that process--along with a 80K and growing member-strong network. For clients, they also recruit externally to find the best possible matches with a fully staffed Recruitment Team. The Matchmakers handle the date planning and also collect date feedback from both parties to recalibrate after each match.<ref name="theglobeandmail" />

Tawkify is in over 30 US cities, have 70+ matchmakers, and have an almost 80% success rate after working with clients 6-12 months.

==Controversy: human versus algorithm==
In the panel discussion ''[[Algorithm]]s and [[Matchmaking|MatchMaking]]: Dating in the Age of Digital'' held during ''[[Internet]] Week [[New York City|New York]]'', disagreement arose between panelists Sam Yagan (co-founder of [[OkCupid]]) and Brian Schecter (co-founder of HowAboutWe.com) over the usefulness of algorithms in online dating.<ref name="bare_url_a">[http://new.livestream.com/iwny/thursdaystage1/videos/1091367 "Algorithms and MatchMaking: Dating in the Age of Digital", [[Livestream]], New York, May 14–21, 2012. Retrieved on 4 August 2012.</ref>  Panelist Amarnath Thombre ([[Match.com]]<ref>{{cite web| url=https://hookup-sites.info/match-com-review/| title=Match.com Review}} Retrieved 20 October 2016</ref> [[Vice President]] of Strategy) said: "We've seen algorithms that are four to ten times better than random".<ref name="bare_url_a" />  Panelist [[E. Jean Carroll]] (co-founder of Tawkify.com) explained Tawkify's position: "We take them [users] off the internet like that.  They're not even allowed to sit around on the internet because that is time-wasting. ...Time well-spent is when you're with somebody or talking."<ref name="bare_url_a" />
In an article on Tawkify in [[Gizmodo]], Sam Biddle writes, "Is it so surprising that smart humans are at least as good at pairing up other humans as a computer?"<ref>Biddle, Sam. [http://gizmodo.com/5922398/the-best-dating-site-youve-never-heard-of-is-too-smart-for-its-own-good "The Best Dating Site You've Never Heard of Is Too Smart for Its Own Good"], ''[[Gizmodo]]'', 29 June 2012. Retrieved 4 August 2012.</ref>

==Matching personalities==
Users cannot see profiles or pictures of other users, which are reserved for matchmakers.  In an article in ''The Globe and Mail'', Tawkify co-founder Kenneth Shaw is reported saying, "We want people to meet by personality first," and "We will never match my machine."<ref name="theglobeandmail" />

==History==
USA Today says "If you wish someone else would do all the hard work for you, Tawkify may be your ideal dating companion: http://www.usatoday.com/story/tech/columnist/2014/02/14/tech-now-online-dating-apps/5434555/.

Recent press by Swagger New York features Tawkify as a "Love Fixer"... “Essentially, we're trying to harness tech to facilitate real, in person-human connection,” certified matchmaker, Michele Presley told me. Instead of using tech to make the matches, a la Tinder, Hinge, etc., it just helps Tawkify’s team of matchmakers “narrow down the options and reach out beyond our own database to find the best candidates wherever they are…because we've found that intuitive, empathetic humans just are - and probably always will be - better at doing that for other humans than the cleverest of machines.” http://swagger.nyc/posts/sto/its-not-the-dating-app-its-you

Tawkify co-founders are [[E. Jean Carroll]] and Kenneth Shaw.<ref>http://www.crunchbase.com/person/kenneth-shaw</ref>  E. Jean Carroll is the writer of ''[[Elle (magazine)|Elle]]'' magazine's "Ask E. Jean" column,<ref>http://www.elle.com/life-love/ask-e-jean/</ref> [[Emmy Award]] nominee for writing for ''[[Saturday Night Live]]'', television [[talk show]] host and author of four books.<ref>http://www.amazon.com/E.-Jean-Carroll/e/B000AP7CJM</ref>  According to ''[[The New York Times]]'', "The two met years ago, when Ms. Carroll sought out Mr. Shaw...to help her design her own app".<ref name="nytimes" />
The Tawkify website launched in January 2012.
In January 2012 Tawkify was featured in ''[[The New York Observer]]''{{'}}s [[Betabeat.com|Betabeat]] blog.<ref>Tiku, Natasha. [http://betabeat.com/2012/01/elle-advice-columnist-e-jean-carroll-dating-startup-tawkify-01262012/ "Elle Advice Columnist E. Jean Carroll Has a New Startup For All You Special Ladies"], ''[[Betabeat]]'', New York, 26 January 2012. Retrieved 3 August 2012.</ref>
In April 2012 Tawkify appeared in ''The New York Times''.<ref name="nytimes" />
In April 2012 Tawkify appeared on [[CNN|CNN.com]].<ref>Bartz, Andrea, and Ehrlich, Brenna. [http://www.cnn.com/2012/04/25/tech/web/niche-dating-sites-netiquette/index.html "Three niche dating sites for targeted romance"], [[CNN.com]], 25 April 2012.</ref>
In May 2012 E. Jean Carroll appeared in Internet Week New York, presented by [[Yahoo!]]<ref>[http://new.livestream.com/iwny/thursdaystage1/videos/1091367]. Retrieved on 3 August 2012.</ref> 
In June 2012 Tawkify launched Mystery Date and Walkify.<ref name="theglobeandmail" />
In June 2012 Tawkify was featured in [[Gizmodo]].<ref name="bare_url">Biddle, Sam. [http://gizmodo.com/5922398/the-best-dating-site-youve-never-heard-of-is-too-smart-for-its-own-good "The Best Dating Site You've Never Heard of Is Too Smart for Its Own Good"], [[Gizmodo]], 29 June 2012. Retrieved on 3 August 2012.</ref>
In July 2012 Tawkify Matchmaker Charlee Ziegler was interviewed by Michelle Skeen, [[Doctor of Psychology|PsyD]], on her radio show ''Relationships 2.0''.<ref>http://www.michelleskeen.com/media-center/relationships-2-0/</ref>
In August 2012 Tawkify was featured in ''The Globe and Mail''.<ref name="theglobeandmail" />
In August 2012 Tawkify appeared in ''[[The New York Post]]''.<ref>Lieberman, Sara. [http://www.nypost.com/p/entertainment/dating/love_at_first_byte_TqYmoWiI3daWvKLC3RAXLN "Love at first byte", ''[[New York Post]]", 5 August 2012. Retrieved on 6 August 2012.''</ref>

==See also==
* [[Comparison of online dating websites]]

==References==
{{reflist}}

==External links==
* [http://www.tawkify.com Tawkify.com]
* [http://betabeat.com/2012/01/elle-advice-columnist-e-jean-carroll-dating-startup-tawkify-01262012/ Betabeat.com]
* [http://new.livestream.com/iwny/thursdaystage1/videos/1091367 Video of E. Jean Carroll, Sam Yagan, Amarnath Thombre, and Brian Schechter at Internet Week New York]
* [http://www.michelleskeen.com/media-center/relationships-2-0-podcast-archive/ Matchmaker Charlee Ziegler interviewed by Michelle Skeen PsyD]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->



[[Category:Online dating services]]