{{Infobox airport
| name         = Renton Municipal Airport
| image        = Aerial Renton Airport during Resurfacing Aug 2009.jpg
| image-width  = 200
| caption      = Airport during a runway resurfacing project, August 2009
| IATA         = RNT
| ICAO         = KRNT
| FAA          = RNT
| type         = Public
| owner        = City of Renton
| operator     = 
| city-served  = [[Renton, Washington]]
| location     = <!--if different than above-->
| elevation-f  = 32
| elevation-m  = 10
| coordinates  = {{coord|47|29|35|N|122|12|57|W|region:US-WA_type:airport_scale:10000}}
| website      = 
| image_map    = KRNT Airport Diagram November 2009.pdf
| image_mapsize = 200
| image_map_caption = FAA airport diagram
| pushpin_map            = USA Washington#USA
| pushpin_relief         = yes
| pushpin_map_caption    = Location of airport in Washington / United States
| pushpin_label          = '''RNT'''
| pushpin_label_position = right
| r1-number    = 16/34
| r1-length-f  = 5,382
| r1-length-m  = 1,640
| r1-surface   = Asphalt/Concrete
| stat-year    = 2010
| stat1-header = Aircraft operations
| stat1-data   = 80,688
| stat2-header = Based aircraft
| stat2-data   = 336
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=RNT|use=PU|own=PU|site=26381.*A}}. Federal Aviation Administration. Effective April 5, 2012.</ref>
}}

'''Renton Municipal Airport''' {{airport codes|RNT|KRNT|RNT}} is a public use [[airport]] located in [[Renton, Washington|Renton]], a city in [[King County, Washington]], [[United States]].<ref name="FAA" /> The airport was renamed '''Clayton Scott Field''' in 2005 to celebrate the 100th birthday of Clayton Scott.<ref>{{cite news
  | url = http://www.seattlepi.com/business/286923_scottobit29.html
  | title = Clayton Scott, 1905-2006: Longtime aviator, Bill Boeing's personal pilot
  | date = September 29, 2006
  | first = James | last = Wallace
  | work = Seattle Post-Intelligencer
}}</ref> The airport's northern boundary is [[Lake Washington]] and the [[Will Rogers - Wiley Post Memorial Seaplane Base]]. Renton Airport has a floating dock and a launching ramp for conversion from wheeled landings to water takeoffs and landings.

The airport is owned by the City of Renton and is a [[general aviation]] airport which serves Renton and other nearby communities. It provides regional aviation services for [[air charter]], [[air taxi]], corporate, business and recreational flyers. This airport is included in the [[National Plan of Integrated Airport Systems]] for 2011–2015, which [[FAA airport categories|categorized]] it as a ''[[reliever airport]]'',<ref>
 {{cite web
 | url = http://www.faa.gov/airports/planning_capacity/npias/reports/media/2011/npias_2011_appA.pdf
 | title = 2011–2015 NPIAS Report, Appendix A (PDF, 2.03 MB)
 | work = National Plan of Integrated Airport Systems
 | publisher = Federal Aviation Administration
 | date = October 4, 2010
 }}
</ref> diverting general aviation aircraft traffic from [[Sea-Tac International Airport]].

The airport is located approximately 12 miles southeast of downtown [[Seattle]] near the south end of [[Lake Washington]]. US Customs service available for both floatplane and wheeled aircraft arriving by water or by land.<ref>{{cite web | url=http://rentonwa.gov/living/default.aspx?id=212 | title=Renton Municipal Airport, Clayton Scott Field | work=City of Renton | accessdate=2016-01-04 }}</ref>

Renton Airport is adjacent to the [[Boeing Renton Factory]] that manufactures [[Boeing 737|737s]] and formerly [[Boeing 757|757s]], and is the initial point of departure for airplanes produced in that facility.
[[File:2009-0603-14a-Air-RentonAirBoeing.jpg|right|thumb|Aerial view showing airport and adjacent Boeing Renton Factory (large structures)]]

== Facilities and aircraft ==
Renton Municipal Airport covers an area of 170 [[acre]]s (69 [[hectare|ha]]) at an [[elevation]] of 32 feet (10 m) above [[mean sea level]]. It has one [[runway]] designated 16/34 with an [[asphalt]] and [[concrete]] surface measuring 5,382 by 200 feet (1,640 x 61 m).<ref name="FAA" /> The runway was resurfaced and realigned in August 2009; prior to this time, it was designated 15/33.<ref>
  {{cite news
  | url = http://www.rentonreporter.com/news/52088722.html
  | title = Runway repaving to ground most flights at Renton Municipal Airport
  | date = July 30, 2009
  | first = Dean | last = Radford
  | work = Renton Reporter
  }}
</ref>

For the 12-month period ending December 31, 2010, the airport had 80,688 aircraft operations, an average of 221 per day: 98% [[general aviation]], 1% [[air taxi]], <1% [[airline|scheduled commercial]], and <1% [[military aviation|military]]. At that time there were 336 aircraft based at this airport: 90% single-[[aircraft engine|engine]], 5% multi-engine, 1% [[jet aircraft|jet]], and 5% [[helicopter]].<ref name="FAA" />

== References ==
{{Reflist}}

== External links ==
* {{Official website|url=http://rentonwa.gov/living/default.aspx?id=212}}
* [http://www.befa.org Boeing Employee Flying Association]
* [http://www.flightcentral.net/airport/KRNT KRNT listing at FlightCentral.Net]
* [http://msrmaps.com/map.aspx?t=1&s=11&lat=47.4931&lon=-122.2157&w=800&h=1200&lp=---+None+--- Aerial image as of July 1990] from [[USGS]] ''[[The National Map]]''
* {{FAA-diagram|05396}}
* {{FAA-procedures|RNT}}
* {{US-airport-ga|RNT}}

<!--Navigation box--><br />
{{Renton, Washington}}
{{Airports in Washington}}

[[Category:Airports in King County, Washington]]
[[Category:Renton, Washington|Airport]]
[[Category:Transportation in King County, Washington]]
[[Category:Year of establishment missing]]