{{Infobox Airport
| name         = Sedona Airport
| image        = Sedona airport.jpg
| IATA         = SDX<!--not SEZ-->
| ICAO         = KSEZ
| FAA          = SEZ
| type         = Public
| owner        = [[Yavapai County, Arizona|Yavapai County]]
| operator     = 
| city-served  = 
| location     = [[Sedona, Arizona]]
| elevation-f  = 4,830
| elevation-m  = 1,472
| coordinates  = {{coord|34|51|00|N|111|47|24|W|type:airport|display=inline,title}}
| website      = http://sedonaairport.org/
| r1-number    = 3/21
| r1-length-f  = 5,129
| r1-length-m  = 1,563
| r1-surface   = [[Asphalt]]
| h1-number    = H1
| h1-length-f  = 50
| h1-length-m  = 15
| h1-surface   = [[Concrete]]
| stat-year    = 2006
| stat1-header = Aircraft operations
| stat1-data   = 50,000
| stat2-header = Based aircraft
| stat2-data   = 102
| footnotes    = Source: [[Federal Aviation Administration]]<ref name=FAA>{{FAA-airport|ID=SEZ|use=PU|own=PU|site=00788.3*A}}, effective 2007-07-05</ref>
}}
'''Sedona Airport''' {{Airport codes|SDX|KSEZ|SEZ}} is a small [[Non-towered airport|non-towered]] [[airport]] located two miles (3&nbsp;km) southwest of the [[central business district]] of [[Sedona, Arizona|Sedona]], a city in [[Yavapai County, Arizona|Yavapai County]], [[Arizona]], [[United States]]. The airport covers {{convert|220|acre|ha|0}} and has one [[runway]] and one [[helipad]].<ref name="FAA" />

Although most U.S. airports use the same three-letter [[location identifier]] for the [[Federal Aviation Administration|FAA]] and [[International Air Transport Association|IATA]], Sedona Airport is assigned '''SEZ''' by the FAA and '''SDX''' by the IATA<ref>[http://gc.kls2.com/airport/SDX Great Circle Mapper: SDX / KSEZ - Sedona, Arizona]</ref> (which assigned '''SEZ''' to [[Seychelles International Airport]] in [[Mahé, Seychelles]]<ref>[http://gc.kls2.com/airport/SEZ Great Circle Mapper: SEZ / FSIA - Mahé, Seychelles (Seychelles International Airport)]</ref>).

Sedona is a very popular destination among Arizona tourists, especially with those who are interested in the [[New Age]] movement or those seeking to be close to nature. The airport is located on top of a high [[mesa]] overlooking a major portion of the city; it is not uncommon for tourists or locals driving around downtown Sedona to see an approaching airplane fly overhead and then suddenly disappear into the mountains without ever appearing to land. The airport is also located very close to the [[Red Rocks of Sedona]].

== History ==
[[Image:SedonaAirport.JPG|thumb|left|500px|Sedona Airport from the south, showing its location atop a mesa]]
The airport was inaugurated in 1955. At that time it had no paved runway, and animals such as [[coyote]]s could be seen walking around the air-strip. This proved dangerous to pilots arriving at Sedona. By 1960, a small, paved runway had been built, practically eliminating the animal problem. 

By 1990, the airport's runway had been improved and it had begun to receive service from some small scheduled airlines. It was the hub of Air Sedona, which served it from such places as [[Sky Harbor International Airport]] in [[Phoenix, Arizona|Phoenix]], [[Las Vegas, Nevada|Las Vegas]], the nearby [[Grand Canyon]] airport and others. Sedona's airport is not able to accommodate [[Jet aircraft|commercial jets]] of the size of the [[Boeing 727]] or larger. It does, however, attract a large number of smaller business jets and aircraft such as [[Cessna]] and [[Beechcraft|Beech]] airplanes and [[helicopter]]s.

[[Scenic Airlines]] discontinued service at Sedona in April 1997.<ref>{{cite web
| url = http://www.reviewjournal.com/lvrj_home/1997/Jul-19-Sat-1997/photos/tapia-romero.html
| title = Inside business: Scenic Airlines to leave Sedona
| publisher = Las Vegas Review-Journal
| date = 1997-04-07
}}</ref>

== Accidents and Incidents ==
Former two time [[Olympic Games|Olympic]] distance runner [[Pat Porter]], his 15-year-old son Connor and a friend of his son, 14-year-old Connor Mantsch, died when their airplane, a [[Beechcraft Duke]] piloted by Porter, crashed after takeoff from the airport, on Thursday, July 26, 2012.<ref name="CBS 5 Arizona">{{cite web|url=http://www.kpho.com/story/19122087/3-dead-in-sedona-airport-plane-crash|title=3 Dead In Sedona Airport Plane Crash|publisher=CBS 5 Arizona|date=2012-07-27}}</ref><ref>http://www.kathrynsreport.com/2012/07/3-killed-when-plane-crashes-bursts-into.html</ref>

== References ==
{{Reflist}}

== External links ==
* [http://sedonaairport.org Sedona Airport], official web site
* [http://www.azdot.gov/MPD/Airport_Development/airports/airports_list.asp?FAA=SEZ Sedona Airport (SEZ)] at [[Arizona DOT]] airport directory
* {{cite web|url= http://www.azdot.gov/aviation/library/MP_PDF/SEZ_MP_02.pdf |title=Sedona Airport Master Plan: 1997-2017 }}&nbsp;{{small|(2.61&nbsp;[[Mebibyte|MiB]])}}
* [http://www.opennav.com/airport/KSEZ openNav: SDX / KSEZ charts]
* {{US-airport-minor|SEZ|SDX}}

[[Category:Sedona, Arizona]]
[[Category:Airports in Yavapai County, Arizona]]