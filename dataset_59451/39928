{{for|the American industrialist|Thomas P. Pike}}
{{Use dmy dates|date=November 2012}}
{{Infobox military person
|name= Sir Thomas Geoffrey Pike
|image= Kammhuber and Pike.jpg
|image_size= 300px
|alt= 
|caption= Sir Thomas Pike (right) with the ''Luftwaffe'' General [[Josef Kammhuber]] in 1956.
|nickname= 
|birth_date= {{birth date|1906|06|29|df=y}}
|birth_place= [[Lewisham]], [[London]]
|death_date={{death date and age|1983|06|01|1906|06|29|df=y}}
|death_place  [[RAF Halton]], [[Buckinghamshire]]
|placeofburial= 
|allegiance= United Kingdom
|branch= [[Royal Air Force]]
|serviceyears= 1924–1967
|rank= [[Marshal of the Royal Air Force]]
|unit= 
|commands= [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] (1960–63)<br/>[[RAF Fighter Command]] (1956–59)<br/>[[No. 11 Group RAF|No. 11 Group]] (1950–51)<br/>Officers' Advanced Training School (1945–46)<br/>No. 1 Mobile Operations Room Unit (1943–44)<br/>[[RAF North Weald]] (1942)<br/>[[No. 219 Squadron RAF|No. 219 Squadron]] (1941)
|battles= [[Second World War]]
|awards= [[Knight Grand Cross of the Order of the Bath]]<br/>[[Commander of the Order of the British Empire]]<br/>[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] & [[Medal bar|Bar]]<br/>[[Mentioned in Despatches]]<br/>[[Legion of Merit|Officer of the Legion of Merit]] (United States)
|relations= Lieutenant General [[William Pike (British Army officer)|Sir William Pike]] (brother)<br/>Lieutenant General [[Hew Pike|Sir Hew Pike]] (nephew)
|laterwork=
}}
[[Marshal of the Royal Air Force]] '''Sir Thomas Geoffrey Pike''', {{postnominals|country=GBR|size=100%|sep=,|GCB|CBE|DFC1|DL}} (29 June 1906 – 1 June 1983) was a senior officer in the [[Royal Air Force]]. He served in the [[Second World War]] as a night fighter squadron commander and then as a station commander. He was [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] in the early 1960s and, in that role, deployed British air power as part of the British response to the [[Brunei Revolt]]. Also, in the face of escalating costs, he implemented the cancellation of the British [[Blue Streak (missile)|Blue Streak]] [[ballistic missile]] system but then found the RAF was without any such capability when the Americans cancelled their own [[GAM-87 Skybolt|Skybolt]] ballistic missile system. He went on to be Deputy Supreme Commander [[Supreme Headquarters Allied Powers Europe]] in the mid-1960s.

==RAF career==
Born the son of [[Captain (British Army and Royal Marines)|Captain]] Sydney Royston Pike and Sarah Elizabeth Pike (née Huddleston),<ref name=odnb>{{cite web|url=http://www.oxforddnb.com/view/article/31548 |title=Sir Thomas Geoffrey Pike|publisher= Oxford Dictionary of National Biography|accessdate =14 July 2012}}</ref> Pike was educated at [[Bedford School]] between 1915 and 1923<ref name=prob56>Probert, p. 56</ref> before joining the [[Royal Air Force]] as a flight cadet on 17 January 1924.<ref name=air>{{cite web|url=http://www.rafweb.org/Biographies/Pike_TG.htm |publisher=Air of Authority – A History of RAF Organisation |title= Marshal of the Royal Air Force Sir Thomas Pike|accessdate =14 July 2012}}</ref> On successfully passing through the  [[Royal Air Force College Cranwell]], he was commissioned as a [[pilot officer]] on 16 December 1925<ref>{{LondonGazette|issue=33123|supp=|startpage=300|date=12 January 1926|accessdate=14 July 2012}}</ref> and immediately posted to [[No. 56 Squadron RAF|No. 56 Squadron]] at [[London Biggin Hill Airport|RAF Biggin Hill]] where he flew [[Gloster Grebe]]s and then [[Armstrong Whitworth Siskin]]s.<ref name=air/> Promoted to [[flying officer]] on 16 June 1927,<ref>{{LondonGazette|issue=33293|supp=|startpage=4497|date=12 July 1927|accessdate=14 July 2012}}</ref> he attended the instructors' course at the [[Central Flying School]] in Autumn 1928 and then became an instructor first with No. 5 Flying Training School at [[RAF Sealand]] and then, from May 1929, at the Central Flying School, where he was a member of the aerobatic team.<ref name=air/>
Promoted to [[flight lieutenant]] on 9 July 1930,<ref>{{LondonGazette|issue=33623|supp=|startpage=4274|date=8 July 1930|accessdate=14 July 2012}}</ref> Pike attended the Long Aircraft Engineering Course at the Home Aircraft Depot at [[RAF Henlow]] from August 1930 and then joined the engineering Staff at the RAF Depot in the Middle East in October 1932.<ref name=air/> He became an instructor at No. 4 Flying Training School at [[Abu Suwayr Air Base|RAF Abu Suwayr]] in November 1934<ref name=air/> and, after attending the [[RAF Staff College, Andover|RAF Staff College]] from January 1937, he was promoted to [[squadron leader]] on 1 February 1937.<ref>{{LondonGazette|issue=34366|supp=|startpage=717|date=2 February 1937|accessdate=14 July 2012}}</ref> He was posted to No. 10 Flying Training School at [[RAF Ternhill]] as Chief Flying Instructor in January 1938 and then became a staff officer in the Deputy Directorate of Peace Organisation within the [[Air Ministry]] in February 1939.<ref name=air/>

Pike served in the [[Second World War]], initially on the air staff within the Directorate of Organisation at the Air Ministry,<ref name=air/> and was promoted to the temporary rank of [[wing commander (rank)|wing commander]] on 1 March 1940<ref>{{LondonGazette|issue=34810|supp=|startpage=1473|date=12 March 1940|accessdate=14 July 2012}}</ref> (made permanent in April 1942).<ref>{{LondonGazette|issue=35525|supp=|startpage=1649|date=14 April 1942|accessdate=14 July 2012}}</ref> He was appointed Officer Commanding [[No. 219 Squadron RAF|No. 219 Squadron]] flying [[Bristol Beaufighter]]s from [[RAF Tangmere]] in February 1941<ref name=air/> and was awarded the [[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] on 13 May 1941 for showing great skill in intercepting enemy aircraft at night, destroying a raiding aircraft on his first night patrol.<ref>{{LondonGazette|issue=35161|supp=|startpage=2744|date=13 May 1941|accessdate=14 July 2012}}</ref> He was awarded a [[medal bar|bar]] to the Distinguished Flying Cross on 30 May 1941 for engaging attackers at night when the aerodrome was illuminated by the glare from a large number of incendiary bombs.<ref>{{LondonGazette|issue=35176|supp=|startpage=3095|date=30 May 1941|accessdate=14 July 2012}}</ref>

Pike was given command of the Night Fighters of [[No. 11 Group RAF|No. 11 Group]] in September 1941 and then went on to be Station Commander at [[RAF North Weald]] in February 1942.<ref name=air/> Promoted to the temporary rank of [[group captain]] on 27 March 1942,<ref>{{LondonGazette|issue=35503|supp=|startpage=1386|date=27 March 1942|accessdate=14 July 2012}}</ref> he became Officer Commanding No. 1 Mobile Operations Room Unit during the Allied Landings in [[Italy]]<ref name=prob57>Probert, p. 57</ref> in May 1943 for which role he was [[mentioned in despatches]] in June 1943.<ref name=air/> He went on to be Senior Air Staff Officer at HQ Desert Air Force in February 1944.<ref name=air/> Appointed a [[Order of the British Empire|Commander of the Order of the British Empire]] in the 1944 [[Birthday Honours]],<ref>{{LondonGazette|issue=36544|supp=yes|startpage=2582|date=2 June 1944|accessdate=14 July 2012}}</ref> he became Commandant of the Officers' Advanced Training School in June 1945.<ref name=air/> He was also awarded the American [[Legion of Merit|Officer of the Legion of Merit]] on 16 October 1945.<ref name=prob57/>

Pike was appointed a [[Companion of the Order of the Bath]] in the [[1946 New Year Honours]].<ref>{{LondonGazette|issue=37407|supp=|startpage=6|date=28 December 1945|accessdate=14 July 2012}}</ref> After the war he stayed in the RAF and became Director of Operational Requirements at the Air Ministry in October 1946 being promoted to [[air commodore]] on 1 July 1947.<ref>{{LondonGazette|issue=38015|supp=yes|startpage=3255|date=11 July 1947|accessdate=14 July 2012}}</ref> Then, after attending the [[Royal College of Defence Studies|Imperial Defence College]] in 1949, he was made Air Officer Commanding [[No. 11 Group RAF|No. 11 Group]] in January 1950.<ref name=air/> He was given the acting rank of [[air vice marshal]] on 9 January 1950.<ref>{{LondonGazette|issue=38838|supp=yes|startpage=768|date=14 February 1950|accessdate=14 July 2012}}</ref> He became Deputy Chief of Staff (Operations) at HQ [[Allied Air Forces Central Europe]] in July 1951, Assistant Chief of Air Staff (Policy) in June 1953 and [[Deputy Chief of the Air Staff]] with the acting rank of [[air marshal]] on 9 November 1953.<ref>{{LondonGazette|issue=40037|supp=yes|startpage=6655|date=4 December 1953|accessdate=14 July 2012}}</ref> He was confirmed in the rank of air marshal on 1 January 1955.<ref>{{LondonGazette|issue=40363|supp=yes|startpage=7361|date=28 December 1954|accessdate=14 July 2012}}</ref> Advanced to [[Knight Commander of the Order of the Bath]] in the 1955 [[Birthday Honours]],<ref>{{LondonGazette|issue=40497|supp=yes|startpage=3259|date=3 June 1955|accessdate=14 July 2012}}</ref> he went on to be Air Officer Commanding-in-Chief at [[RAF Fighter Command]] in August 1956.<ref name=air/> He was promoted to [[air chief marshal]] on 1 November 1957.<ref>{{LondonGazette|issue=41217|supp=yes|startpage=6405|date=1 November 1957|accessdate=14 July 2012}}</ref>

Pike became [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] on 1 January 1960.<ref>{{LondonGazette|issue=41664|supp=yes|startpage=1979|date=20 March 1959|accessdate=14 July 2012}}</ref> In that role he deployed British air power as part of the British response to the [[Brunei Revolt]].<ref name=prob58>Probert, p. 58</ref> Also, in the face of escalating costs, he implemented the cancellation of the British [[Blue Streak (missile)|Blue Streak]] [[ballistic missile]] system but then found the RAF was without any such capability when the Americans cancelled their own [[GAM-87 Skybolt|Skybolt]] ballistic missile system.<ref name=prob58/> He was advanced to [[Knight Grand Cross of the Order of the Bath]] in the 1961 [[New Year Honours]]<ref>{{LondonGazette|issue=42231|supp=yes|startpage=8891|date=27 December 1960|accessdate=14 July 2012}}</ref> and promoted to [[Marshal of the Royal Air Force]] on 6 April 1962.<ref>{{LondonGazette|issue=42644|supp=yes|startpage=2929|date=6 April 1962 |accessdate=14 July 2012}}</ref> Pike was then Deputy Supreme Commander [[Supreme Headquarters Allied Powers Europe]] from January 1964 until his retirement in March 1967.<ref name=air/>

==Later life==
Following his retirement, Pike lived in [[Hastingwood]] in [[Essex]] and was made a [[Deputy Lieutenant]] of Essex in February 1973:<ref>{{LondonGazette|issue=45905|supp=|startpage=2032|date=13 February 1973|accessdate=14 July 2012}}</ref> he continued in the post until December 1981.<ref>{{LondonGazette|issue=48832|supp=|startpage=16274|date=23 December 1981|accessdate=14 July 2012}}</ref> He was President of the [[Royal Air Forces Association]] from 1969 to 1979 and his interests included local history and arranging engineering [[apprenticeship]]s for local teenagers in Essex.<ref name=prob59>Probert, p. 59</ref> He died at [[RAF Halton]] on 1 June 1983 and, due to his time spent at North Weald, he was buried in the military section of St. Andrew’s churchyard, [[North Weald Bassett]].<ref>{{cite web|url=http://www.raf.mod.uk/downloads/RAFpublications/sota_vol1_no1_pt2.pdf|title=Night Fighter|page=47|publisher=Spirit of the Air Volume 1 Number 1|year=2006|accessdate=14 July 2012}}</ref>

==Family==
In 1930 Pike married Kathleen Althea Elwell; they had a son and two daughters.<ref name=odnb/> Sir Thomas's brother was Lieutenant General [[William Pike (British Army officer)|Sir William Pike]] and his nephew (Sir William's son) is Lieutenant General [[Hew Pike|Sir Hew Pike]], who commanded the [[3rd Battalion, The Parachute Regiment]] in the [[Falklands War]].<ref>{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=7953030 |title=Lt. Col. Hew Pike|publisher= National Archives|accessdate =14 July 2012}}</ref>

==References==
{{reflist|30em}}

==Sources==
{{commons category}}
*{{cite book|last=Probert|first=Henry|year=1991|title=High Commanders of the Royal Air Force|publisher=HMSO|isbn= 0-11-772635-4}}

{{s-start}}
{{s-mil}}
|-
{{s-bef|before=[[Stanley Vincent]]}}
{{s-ttl|title=Air Officer Commanding [[No. 11 Group RAF|No. 11 Group]]|years=1950–1951}}
{{s-aft|after=[[Percy Bernard, 5th Earl of Bandon|The Earl of Bandon]]}}
|-
{{s-bef|before=[[Ronald Ivelaw-Chapman|Sir Ronald Ivelaw-Chapman]]}}
{{s-ttl|title=[[Deputy Chief of the Air Staff]]|years=1953–1956}}
{{s-aft|after=[[Geoffrey Tuttle|Sir Geoffrey Tuttle]]}}
|-
{{succession box|title=Commander-in-Chief [[RAF Fighter Command|Fighter Command]]|before=[[Hubert Patch|Sir Hubert Patch]]|after=[[Hector McGregor|Sir Hector McGregor]]|years=1956–1959}}
{{succession box |title=[[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] |before=[[Dermot Boyle|Sir Dermot Boyle]] |after=[[Charles Elworthy, Baron Elworthy|Sir Charles Elworthy]] |years=1960–1963}}
{{succession box|before=[[Hugh Stockwell|Sir Hugh Stockwell]]|title=[[Deputy Supreme Allied Commander Europe]]|after=[[Robert Bray (general)|Sir Robert Bray]]|years=1964–1967}}
{{s-end}}

{{Chief of the Air Staff}}

{{Authority control}}
{{DEFAULTSORT:Pike, Thomas}}
[[Category:1906 births]]
[[Category:1983 deaths]]
[[Category:Chiefs of the Air Staff (United Kingdom)]]
[[Category:Marshals of the Royal Air Force]]
[[Category:People educated at Bedford School]]
[[Category:Graduates of the Royal Air Force College Cranwell]]
[[Category:Deputy Lieutenants of Essex]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Recipients of the Distinguished Flying Cross and Bar (United Kingdom)]]
[[Category:Knights Grand Cross of the Order of the Bath]]
[[Category:Commanders of the Order of the British Empire]]
[[Category:Officers of the Legion of Merit]]
[[Category:People from Lewisham (London borough)]]
[[Category:NATO military personnel]]