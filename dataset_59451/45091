{{Infobox musical
| name         = The Kid
| subtitle     = 
| image        = Dan and Terry from The Kid musical.jpg
| image_size   = 200px
| caption      = Characters Dan and Terry,<br /> in scene from ''The Kid'' (2010)
| music        = Andy Monroe
| lyrics       = Jack Lechner
| book         = Michael Zam
| basis        = ''[[The Kid: What Happened After My Boyfriend and I Decided to Go Get Pregnant]]'' by [[Dan Savage]]
| productions  = 2010 [[Off-Broadway]], [[Theatre Row (New York City)|Theatre Row, New York City]], [[The New Group]]
| awards       = 2009 [[BMI Foundation]] [[Jerry Bock]] Award for Excellence in Musical Theatre<ref name="seattleaward" /><ref name="bmiaward" /><ref name="playbillaward" /> <br />2011 [[Outer Critics Circle Award]] for Outstanding New Off-Broadway Musical<ref name="playbilloutercritics" /><ref name="upioutercritics" /><ref name="warhorsenamed" />
}}

'''''The Kid''''' is a [[musical theatre|musical]] with a book by Michael Zam, music composed by Andy Monroe and lyrics by Jack Lechner.  The comic story concerns an [[open adoption]] process by a [[Homosexuality|same-sex]] couple.<ref>{{cite news|url=http://artsbeat.blogs.nytimes.com/2010/03/22/theater-suggestions-for-the-first-family/|accessdate=June 5, 2011|title=Arts Beat - The Culture at Large - Theater Suggestions for the First Family|date=March 22, 2010|first=Patrick|last=Healy|work=[[The New York Times]]|publisher=[[The New York Times Company]]}}</ref><ref name="familyvalues" /> It is based on the 1999 [[non-fiction]] book by [[Dan Savage]], ''[[The Kid: What Happened After My Boyfriend and I Decided to Go Get Pregnant]]''.<ref name="familyvalues">{{cite news|url=http://www.newyorker.com/arts/events/theatre/2010/03/29/100329goth_GOAT_theatre_preview|accessdate=June 5, 2011|title=Spring Preview - The Theatre - Family Values|date=March 29, 2010|work=[[The New Yorker]]|publisher=Condé Nast}}</ref><ref>{{cite news|title=The Family Room: A Preview of the 2010 Off-Broadway Season|first=Robert|last=Simonson|date=January 11, 2010|accessdate=June 5, 2011|url=http://www.playbill.com/news/article/135815-The-Family-Room-A-Preview-of-the-2010-Off-Broadway-Season-|work=[[Playbill]]|publisher=Playbill, Inc}}</ref>  The protagonist, Dan, is a sex [[advice columnist]] who decides to adopt a child with his partner Terry. Throughout the musical the couple encounter difficulties including making the decision to adopt, finding a birth mother, and overcoming apprehension about the adoption process.

The musical premiered on May 10, 2010 [[Off-Broadway]], starring [[Christopher Sieber]] as Dan.  It received a generally favorable reception and received five [[Drama Desk Award]] nominations in 2011, including [[Drama Desk Award for Outstanding Musical|Outstanding Musical]], [[Drama Desk Award for Outstanding Lyrics|Outstanding Lyrics]] and [[Drama Desk Award for Outstanding Book of a Musical|Outstanding Book of a Musical]];<ref name="backstageaward" /><ref name="dramadeskawards" /><ref name="galanominations" />

==Plot==
;Act I
Dan is a [[gay]] sex [[advice columnist]] ("I'm Asking You"),<ref>{{cite news|work=[[The New York Observer]]|first=Jesse|last=Oxfeld|date=May 12, 2010|title=Remember Shmomosexuals?}}</ref> and his partner is Terry.<ref name="familyvalues" /><ref name="tdf" />  The two initially become involved in a relationship with each other after bonding over a discussion of [[Gore Vidal]] ("Gore Vidal").<ref name="becomesamusical" /> In a later scene, Dan attempts to convince Terry to leave a bathroom in a hotel where he locked himself inside after the two have an argument ("Terry...").<ref name="becomesamusical" /> The couple meet with other individuals who are looking to become parents and adopt children ("They Hate Us").<ref name="bacalzo" /><ref name="dailynews">{{cite news|url=http://articles.nydailynews.com/2010-05-11/entertainment/27064028_1_dan-savage-bacchus-social-worker|accessdate=May 27, 2011|title=Dan Savage's ''The Kid'' musical is boring baby that needs to grow up|first=Joe|last=Dziemianowicz|date=May 11, 2010|work=[[New York Daily News]]}}</ref> Dan composes a [[satire|satirical]] letter to potential birth mothers listing entries which should obviously not be included ("If You Give Us Your Baby").<ref name="bacalzo" /> Dan and Terry meet with an adoption counselor named Anne to go over their worries about becoming parents.<ref name="becomesamusical" /> The adoption counselor tells them, "Man or woman, gay or straight, what you're facing is a long, long wait."<ref name="brantley" /> The couple decide upon an [[open adoption]] process, where they will be able to meet the baby's birth mother.<ref name="bacalzo" /><ref name="feldberg" />  Melissa, a [[teenager]] who is [[pregnant]] and [[homeless]], selects them as the couple to care for her child ("Her Name Is Melissa").<ref name="bacalzo" /><ref name="dailynews" /> She describes her life to Dan and Terry ("Spare Changin'").<ref name="brantley" />

;Act II
Dan's mother informs him she knew he was gay as a child but did not meddle in his process of becoming a gay adult ("I Knew").<ref name="tdf" /><ref name="becomesamusical" /> She reassures him during a period of nervousness, telling Dan, "If you think this is scary, wait. Do you know how terrified I was when you first had measles?"<ref name="brantley" />  Bacchus, the biological father of Melissa's child, appears and becomes involved in the decision regarding the outcome for the baby ("Behind the Wheel").<ref name="bacalzo" /> Dan and Terry worry over whether Melissa will change her mind during the adoption process ("42 Hours").<ref name="dailynews" /> After the child is born, the couple share the experience of holding their new baby together ("Beautiful"), and all ends happily ("My Kid").<ref name="bacalzo" /><ref name="dailynews" />

==Musical numbers==
{{col-begin}}
{{col-2}}
;Act I
*I'm Asking You	&ndash; Company
*The Kid &ndash; Dan, Dan's Mother
*Terry... &ndash; Dan, Terry
*They Hate Us &ndash; Company
*The Kid (Reprise) &ndash; Ruth, Dan, Terry
*Nice &ndash; Anne, Dan, Terry
*Gore Vidal &ndash; Terry, Dan
*If You Give Us Your Baby &ndash; Dan, Terry, Susan
*Seize the Day &ndash; Company
*Her Name is Melissa &ndash; Dan, Terry, Anne
*Spare Changin'	&ndash; Melissa
*What Do You Say? &ndash; Company

{{col-break}}
;Act II
*We're Asking You &ndash; Ensemble
*It's Not Your Baby &ndash; Anne, Dan, Terry
*When They Put Him In Your Arms &ndash; Dan, Terry
*It Gets Better &ndash; Company
*Behind the Wheel &ndash; Bacchus
*I Knew	&ndash; Dan's Mother
*Beautiful &ndash; Terry, Dan
*42 Hours &ndash; Dan, Terry, Melissa
*My Kid	&ndash; Dan
{{col-end}}

==Background==
[[File:Dan Savage Provided.jpg|thumb|right|upright|[[Dan Savage]] (2005)]]
Production on the musical began in 2005.<ref name="becomesamusical">{{cite news|accessdate=June 5, 2011|url=https://www.nytimes.com/2010/05/07/theater/07kid.html|title=A Gay Adoption Becomes a Musical|date=May 7, 2010|first=Patrick|last=Healy|work=[[The New York Times]]|publisher=[[The New York Times Company]]|page=C1}}</ref><ref name="kachka" />  Zam and Monroe had both previously read Savage's book.  Monroe approached Zam and told him he thought the material would make a good musical. Zam and Monroe contacted Lechner, as they wished to have him involved in the musical's production process.<ref name="tdf">{{cite news|url=http://wp.tdf.org/index.php/2010/04/raising-the-kid/|accessdate=June 5, 2011|title=Raising The Kid|first=Ashley|last=Van Buren|date=April 2010|work=tdf STAGES|publisher=tdf; Theatre Development Fund}}</ref>  In an interview with ''[[The New York Times]]'', Zam later commented, "We also wanted to create a musical that was true to the experiences of Dan Savage and his partner, Terry, capture the essence of who they were, without just imposing our own plot and music on their lives. But it was also thrilling to create a show around a person who is not your standard role-model character, who is insightful about his foibles and admits to them and who has all these powerful emotions that we knew – somehow – could be turned into powerful songs."<ref name="becomesamusical"/>

After Zam, Monroe and Lechner determined an initial plan for developing the musical, Lechner contacted his friend [[Kate Clinton]], who was also a friend of Savage and could make an introduction.<ref name="greenfield">{{cite news|url=http://newyork.timeout.com/arts-culture/gay-lesbian/70961/the-kid-hits-off-broadway|accessdate=June 5, 2011|title=The Kid hits Off-Broadway: A sex-columnist's memoir about adoption gets the drama-queen treatment|first=Beth|last=Greenfield|work=[[Time Out New York]]|date=May 3, 2010|publisher=newyork.timeout.com}}</ref> The three-member production team met with Savage and convinced him of the merits of developing his book into a musical.<ref name="greenfield" /> Monroe told ''[[The Advocate]]'', "Dan has a really good way of framing [adoption by gays] with his own brand of humor. It becomes less of a political piece and more of a human story."<ref name="savagejazzhands">{{cite news|title=Savage jazz hands|date=October 9, 2007|work=[[The Advocate]]|first=Japhy|last=Grant|issn=0001-8996}}</ref> Savage gave feedback to the production team, during the adaptation process, providing them with "thousands of words of notes".<ref name="kachka">{{cite news|work=[[New York Magazine]]|accessdate=June 5, 2011|date=May 16, 2010|first=Boris|last=Kachka|title=The Kid Stays in the Picture: Sex columnist Dan Savage takes his husband and son to a musical about themselves|url=http://nymag.com/arts/theater/features/66008/|publisher=New York Media LLC}}</ref>  The work takes the form of a "presentational musical", wherein the character of Dan speaks directly to the audience about his experiences.<ref name="tdf" /><ref name="feldberg" />

==Production==
The original production was organized by [[The New Group]] and directed by Scott Elliott.<ref name="familyvalues" /> Dan was portrayed by [[Christopher Sieber]].<ref name="peikert" /><ref>{{cite news|title=TM Theater News: New Group's The Kid Musical Delays Previews; Complete Casting Announced|work=TheaterMania|date=March 12, 2010|location=[[New York City]]|first=Dan|last=Bacalzo|accessdate=June 5, 2011|url=http://www.theatermania.com/off-broadway/news/03-2010/new-groups-the-kid-musical-delays-previews-complet_25747.html|publisher=www.theatermania.com}}</ref> Elliott served as the artistic director for The New Group.<ref name="becomesamusical" /> Elliott observed to ''The New York Times'', "One of the great parts of the book was that it was political without being political, that it tells a story of two guys creating a family without having to shove the politics of that experience in America down audience members' throats. But we were also conscious of the delicacy of turning a popular book, particularly one man's deeply felt memoir, into a piece of theater."<ref name="becomesamusical" /> [[Rosie O'Donnell]] promoted the musical through a discussion titled "The Modern Family: A Discussion of Gay Adoption" that was held by The New Group on April 18, 2010.<ref name="rosie">{{cite news|title=Rosie O'Donnell Talks Gay Adoption for The New Group's ''The Kid'' Panel| first=Ernio| last=Hernandez|date=April 7, 2010|work=[[Playbill]]|publisher=Playbill, Inc|accessdate=June 5, 2010|url=http://www.playbill.com/news/article/138549-Rosie-ODonnell-Talks-Gay-Adoption-for-The-New-Groups-The-Kid-Panel}}</ref> O'Donnell commented, "I was blown away: here was the first show that really showed what it's like for a gay person who wants to be a parent – but also what it's like for anyone going through this crazy but thrilling process."<ref name="rosie" />

The musical premiered on May 10, 2010,<ref name="kachka" /> [[Off-Broadway]] in [[Theatre Row (New York City)|Theatre Row, New York City]] at the Acorn Theatre.<ref name="familyvalues" />  The production ran through May 29, 2010.<ref name="lortelaward" /> Dan, Terry and their 12-year-old son DJ attended the premiere.<ref name="kachka" /> Terry described the experience of watching himself portrayed onstage as "surreal".<ref>{{cite news|first=Harry|last=Haun|work=[[Playbill]]|date=May 26, 2010|accessdate=June 5, 2011|url=http://www.playbill.com/playblog/2010/05/the-kid-himself-watches-his-story/|title=The Kid Himself Watches His Story|publisher=Playbill, Inc}}</ref>

==Principal roles and notable performers==
<!--this list is for stage performers only, please do not add movie cast-->
{|class="wikitable"
!width="100"|'''Character'''
!width="300"|'''Description'''
!width="400"|'''Notable stage performers'''
 |-
 |Dan ||A journalist and author, who maintains a sex [[advice column]] ||[[Christopher Sieber]]°
 |-
 |Terry ||Dan's partner and husband||Lucas Steele°
|-
 |Dan's mother ||  ||[[Jill Eikenberry]]°
 |-
 |Ruth ||  ||[[Ann Harada]]°
 |-
 |Anne ||An adoption counselor who advises Dan and Terry ||[[Susan Blackwell]]°
 |-
 |Susan || ||Brooke Sunny Moriber°
 |-
 |Melissa ||Biological mother of adopted child  ||Jeannine Frumess°
 |-
 |Bacchus ||Biological father of adopted child ||Michael Wartella°
 |-
 |Chad ||  ||[[Tyler Maynard]]°
 |-
 |}
° denotes original [[Off-Broadway]] cast<ref name="lortelaward" /><ref>{{cite news|url=http://thekidthemusical.com/cast.html|publisher=thekidthemusical.com|title=The Kid: A New Musical|year=2010|accessdate=June 9, 2011}}</ref>

==Reception==
''The Kid'' received a generally favorable reception and was recognized with the 2009 [[BMI Foundation]] Jerry Bock Award for Excellence in Musical Theatre,<ref name="seattleaward">{{cite news|work=[[Seattle Post-Intelligencer]]|title=Musical based on Dan Savage memoir opens Monday|date=May 9, 2010|page=Web Edition; Big Blog}}</ref><ref name="bmiaward" /> in the category of Best New Musical.<ref name="playbillaward">{{cite journal|journal=[[Playbill]]|last=Hernandez|first=Ernio|date=November 6, 2009|accessdate=June 3, 2011|title=Blackwell and Reichard Will Offer Sneak Peek of The Kid Musical|issn=0032-146X|publisher=Playbill, Inc.|url=http://www.playbill.com/news/article/134408-Blackwell-and-Reichard-Will-Offer-Sneak-Peek-of-The-Kid-Musical}}</ref> Composer [[Jerry Bock]], a [[Pulitzer Prize]] and [[Tony Award]] recipient, chose the [[BMI Lehman Engel Musical Theater Workshop]]-developed musical to receive the award.<ref name="bmiaward">{{cite news| url=http://www.bmifoundation.org/news/bmi_foundation_presents_jerry_bock_award_to_a_talented_musical_trifecta|accessdate=June 4, 2011| date=August 13, 2009|title=BMI Foundation Presents Jerry Bock Award To A Talented Musical Trifecta|work=News, [[BMI Foundation|The BMI Foundation, Inc.]]|publisher=www.bmifoundation.org}}</ref>

''The Kid'' received nominations in 2011 for five [[Drama Desk Award]]s including [[Drama Desk Award for Outstanding Musical|Outstanding Musical]], [[Drama Desk Award for Outstanding Lyrics|Outstanding Lyrics]], and [[Drama Desk Award for Outstanding Book of a Musical|Outstanding Book of a Musical]];<ref name="backstageaward" /><ref name="galanominations">{{cite news|url=http://www.dramadeskawardsgala.com/nominations_01.php|accessdate=June 5, 2011|title=Nominations: Drama Desk Nominations for the 2010-2011 Season|author=Drama Desk| publisher=www.dramadeskawardsgala.com| year=2011| work=DramaDeskAwardsGala.com}}</ref> in addition to nominations for a [[Lucille Lortel Award]] and a [[GLAAD Media Award]].<ref name="lortelaward" /> It won the 2011 [[Outer Critics Circle Award]] in the category of Outstanding New Off-Broadway Musical.<ref name="playbilloutercritics" /><ref name="upioutercritics" /><ref name="warhorsenamed">{{cite news|accessdate=June 5, 2011| title=ArtsBeat - The Culture at Large - 'War Horse' Named Best New Broadway Play by Outer Critics Circle|date=May 16, 2011| url=http://artsbeat.blogs.nytimes.com/2011/05/16/war-horse-named-best-new-broadway-play-by-outer-critics-circle|work=[[The New York Times]]|publisher=[[The New York Times Company]]|first=Patrick|last=Healy}}</ref>

Ben Brantley reviewed the musical for ''[[The New York Times]]'', writing that it informs viewers that same-sex couples are similar to everyone else: "This message is transmitted with a consistency and a thoroughness that are rare in contemporary musicals."<ref name="brantley">{{cite news|first=Ben|last=Brantley|work=[[The New York Times]]|publisher=[[The New York Times Company]]|title=Just Like Other Dads (Well, Almost)|date=May 11, 2010|accessdate=June 5, 2011|url=http://theater.nytimes.com/2010/05/11/theater/reviews/11kid.html}}</ref> A review in ''TheaterMania'' praised the musical composition, stating that the musical "does a good job capturing the tone of Savage's book, which is filled with irreverent humor, frank talk about sex, and the understandable fears and anxieties that go hand in hand with the anticipation of becoming a parent."<ref name="bacalzo">{{cite news|accessdate=June 5, 2011|url=http://www.theatermania.com/off-broadway/reviews/05-2010/the-kid_27193.html|title=TM Reviews: The Kid|date=May 11, 2010|first=Dan|last=Bacalzo|location=[[New York City]]|work=TheaterMania|publisher=www.theatermania.com}}</ref> Roma Torre reviewed the musical for ''[[NY1]]'', observing, "''The Kid'' is aimed at anyone who's ever had a family, lost a family or craved one &mdash; which actually means just about all of us. The fact that it's about two gay men attempting to adopt a baby from a homeless teenager doesn't make it any less universal. In fact, it's the details based on true events that give this sweetly clever, somewhat sentimental love story its disarming appeal."<ref name="torre">{{cite news|accessdate=June 5, 2011|date=May 12, 2010|title=NY1 Theater Review: "The Kid"|first=Roma|last=Torre|work=[[NY1]]|url=http://www.ny1.com/content/ny1_living/118517/ny1-theater-review---the-kid-|publisher=www.ny1.com}}</ref>

''[[The Star-Ledger]]'' noted, "A lot of the jokes are based on the would-be adoptive couple being two men. But the sharp humor is balanced with a sweetness and humanity that makes their emotional experience relatable for everyone."<ref name="feldberg">{{cite news|work=[[The Star-Ledger]]|title='The Kid' is all right Gay couple try to adopt in charming new musical|date=May 12, 2010|first=Robert|last=Feldberg|page=035}}</ref> Writing for ''[[New York Press]]'', reviewer Mark Peikert was less enthusiastic: "Buttery, salty and ultimately unmemorable, the song-strewn adaptation of sex columnist Dan Savage's book about adopting a child with his boyfriend is a funny, smart and enjoyable musical – as long as it's being performed right in front of you. But once you leave the theater, you'll be hard pressed to remember just what about it you enjoyed."<ref name="peikert">{{cite news|first=Mark|last=Peikert|date=May 11, 2010|title=Grown-Ups Time: 'The Kid' and 'Family Week' have very different takes on modern families|work=[[New York Press]]|accessdate=June 5, 2011|url=http://www.nypress.com/article-21211-grown-ups-time.html|publisher=www.nypress.com}}</ref>

==Awards==
{|class="wikitable" style="font-size: 90%;" border="2" cellpadding="4" background: #f9f9f9;
|- align="center"
! style="background:#B0C4DE;" | Year
! style="background:#B0C4DE;" | Award
! style="background:#B0C4DE;" | Organization
! style="background:#B0C4DE;" | Category
! style="background:#B0C4DE;" | Result
|-
|2009
|[[Jerry Bock]] Award for Excellence in Musical Theatre
|[[BMI Foundation]]
|Best New Musical
|bgcolor="#ddffdd"|Won<ref name="seattleaward" /><ref name="bmiaward" />
|-
|rowspan="8"|2011
|rowspan="5"|[[Drama Desk Award]]
|rowspan="5"|[[Drama Desk]]
|[[Drama Desk Award for Outstanding Musical|Outstanding Musical]]
|style="background: #ffdddd"| Nominated<ref name="backstageaward">{{cite news|url=http://www.backstage.com/bso/news-and-features-news/2011-drama-desk-award-nominations-announced-1005162322.story|accessdate=June 5, 2011|title=2011 Drama Desk Award Nominations Announced|first=Frank|last=Nestor|date=April 29, 2011|work=Backstage: The Actor's Resource|publisher=www.backstage.com}}</ref><ref name="dramadeskawards">{{cite news|url=http://dramadesk.com/press111.html|accessdate=June 5, 2011|publisher=dramadesk.com|date=April 29, 2011|title=56th Annual Drama Desk Awards announced at NY Friars Club by Audra McDonald and Liev Schreiber|author=Drama Desk|work=News & Views from the Drama Desk}}</ref>
|-
|[[Drama Desk Award for Outstanding Actor in a Musical|Outstanding Actor in a Musical]] ([[Christopher Sieber]])
|style="background: #ffdddd"| Nominated<ref name="backstageaward" /><ref name="dramadeskawards" />
|-
|[[Drama Desk Award for Outstanding Featured Actress in a Musical|Outstanding Featured Actress in a Musical]] ([[Jill Eikenberry]])
|style="background: #ffdddd"| Nominated<ref name="backstageaward" /><ref name="dramadeskawards" />
|-
|[[Drama Desk Award for Outstanding Lyrics|Outstanding Lyrics]] (Jack Lechner)
|style="background: #ffdddd"| Nominated<ref name="backstageaward" /><ref name="dramadeskawards" />
|-
|[[Drama Desk Award for Outstanding Book of a Musical|Outstanding Book of a Musical]] (Michael Zam)
|style="background: #ffdddd"| Nominated<ref name="backstageaward" /><ref name="dramadeskawards" />
|-
|[[Lucille Lortel Awards]]
|The League of Off-Broadway Theatres and Producers
|Outstanding Musical
|style="background: #ffdddd"| Nominated<ref name="lortelaward">{{cite news|accessdate=June 5, 2011|url=http://www.lortel.org/lla_archive/index.cfm?search_by=show&id=5512|publisher=Lucille Lortel Foundation; www.lortel.org|work=Lortel Archives: The Internet Off-Broadway Database|title=The Kid; New Group|year=2011|location=[[New York, New York]]}}</ref>
|-
|[[GLAAD Media Awards]]
|[[Gay & Lesbian Alliance Against Defamation]]
|Outstanding New York Theater: Broadway and Off-Broadway
|style="background: #ffdddd"| Nominated<ref name="lortelaward" />
|-
|[[Outer Critics Circle Award]]
|Outer Critics Circle 
|Outstanding New Off-Broadway Musical
|bgcolor="#ddffdd"|Won<ref name="playbilloutercritics">{{cite news|url=http://www.playbill.com/news/article/151247-PHOTO-CALL-The-2011-Outer-Critics-Circle-Awards|accessdate=June 5, 2011|publisher=Playbill, Inc|work=[[Playbill]]|title=Photo Call: The 2011 Outer Critics Circle Awards|date=May 27, 2011|first=Lucas |last=Lilieholm}}</ref><ref name="upioutercritics">{{cite news|accessdate=June 5, 2011|url=http://www.upi.com/Entertainment_News/Music/2011/05/17/Goes-Mormon-win-Critics-Circle-Awards/UPI-42171305654982/|title='Goes,' 'Mormon' win Critics Circle Awards|date=May 17, 2011|work=[[United Press International]]|publisher=United Press International, Inc.}}</ref>
|-
|}

==See also==
{{Portal|LGBT|Musical Theatre|Theatre}}
*[[Heterosexism]]
*[[LGBT adoption]]
*[[LGBT parenting]]
*[[LGBT rights]]
*[[Same-sex marriage]]s and [[civil union]]s
*''[[Preacher's Sons]]'', documentary about a gay adoptive couple
*''[[Mommy Mommy]]'', documentary about a lesbian adoptive couple

==References==
{{Reflist|2}}

==Further reading==
*{{cite book|title=Lesbian and Gay Fostering and Adoption: Extraordinary Yet Ordinary|first=Stephen|last= Hicks|author2=Janet McDermott  |publisher=Jessica Kingsley Pub|year=1998|isbn=978-1-85302-600-3}}
*{{cite book|title=Lesbian and Gay Foster and Adoptive Parents: Recruiting, Assessing, and Supporting an Untapped Resource for Children and Youth|first=Gerald P. |last=Mallon |publisher=CWLA Press; [[Child Welfare League of America]]| year=2006| isbn=978-1-58760-104-0}}
*{{cite book|title=Families Like Mine: Children of Gay Parents Tell It Like It Is|first=Abigail|last=Garner| publisher=Harper| year=2005| isbn=978-0-06-052758-7}}

==External links==
*{{Official website|http://thekidthemusical.com/}}, at thekidthemusical.com
*{{Official website|http://www.thenewgroup.org/0608.htm#thekid}}, at [[The New Group]]
*{{Official website|http://www.bmifoundation.org/news/bmi_foundation_presents_jerry_bock_award_to_a_talented_musical_trifecta|name= 2009 BMI Foundation Jerry Bock Award for Excellence in Musical Theatre}}, [[BMI Foundation|BMI Foundation, Inc.]] 
*{{cite news|url=http://www.lortel.org/lla_archive/index.cfm?search_by=show&id=5512|publisher=www.lortel.org|work=Lortel Archives: The Internet Off-Broadway Database|title=The Kid; New Group|year=2011}}

{{Adopt}}
{{Dan Savage}}
{{Drama Desk Awards}}
{{LGBT}}

{{DEFAULTSORT:Kid}}
[[Category:2010 musicals]]
[[Category:Off-Broadway musicals]]
[[Category:LGBT adoption]]
[[Category:LGBT theatre in the United States]]
[[Category:Dan Savage]]
[[Category:LGBT-related musicals]]
[[Category:Musicals based on novels]]
[[Category:American plays]]