{{Infobox journal
| title = Journal of Contemporary History
| cover = [[File:Journal of Contemporary History.jpg]]
| editor = [[Stanley G. Payne]], [[Richard J. Evans]]
| discipline = [[History]]
| former_names = 
| abbreviation = J. Contemp. Hist.
| publisher = [[Sage Publications]]
| country = 
| frequency = Quarterly
| history = 1966–present
| openaccess = 
| license = 
| impact = 0.296
| impact-year = 2012
| website = http://www.uk.sagepub.com/journalsProdDesc.nav?prodId=Journal200983&crossRegion=eur
| link1 = 
| link1-name = 
| link2 = 
| link2-name = 
| JSTOR = 00220094
| OCLC = 1783199
| LCCN = 
| CODEN = 
| ISSN = 0022-0094
| eISSN = 1461-7250
}}

The '''''Journal of Contemporary History''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] covering the study of history in all parts of the world since the end of the [[First World War]]. It was established in 1966 by [[Walter Laqueur]] and [[George L. Mosse]] and is now published quarterly by [[Sage Publications]] and edited by [[Richard J. Evans]] ([[University of Cambridge]]) and [[Stanley Payne]] ([[University of Wisconsin–Madison]]).

== Content and scope ==
The journal publishes scholarly articles, review articles and book reviews, covering a broad range of historical approaches including social, economic, political, diplomatic, intellectual and cultural, on every country and region of the world within living memory, from 1918 to the present day.

Each issue is approximately 216 pages and comprises 8 or 9 research articles, 1 or 2 review articles and up to 40 book reviews. The Journal normally publishes at least 1 special issue per volume, either arising from a supported conference or from an externally submitted proposal. Each volume runs through one calendar year. Issues are published in January, April, July and October.

Since 2008 JCH has included reviews of individual books, in addition to review articles covering a range of books within the compass of a single critical essay. [[Kristina Spohr]], of the [[London School of Economics and Political Science]], and [[Jim Bjork]], of [[King's College London]] are Joint Review Editors.

== History ==
The Journal was founded in 1966 by [[Walter Laqueur]] and [[George L. Mosse]]. The Journal's editorial office (until 2005) was at the [[Wiener Library|Institute of Contemporary History and Wiener Library]], Devonshire Street, London, at which Laqueur was Director from 1965 to 1994. Originally published by [[Weidenfeld & Nicolson]] it was purchased by [[Sage Publications]] in 1972 and continues to be published by SAGE.

== Abstracting and indexing ==
The ''Journal of Contemporary History'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]].

== Reception and influence ==
The journal has a high reputation internationally. ''[[International Herald Tribune]]'' columnist [[William Pfaff]] has described it as "an excellent international publication".<ref>{{cite news|last=Pfaff|first=William|title=Violent Talk From Leaders, Violent Acts in Schoolyards|url=http://www.highbeam.com/doc/1P1-6408630.html|newspaper=[[International Herald Tribune]]|date=16 April 1998}}</ref> The ''[[Times Literary Supplement]]'' has described it as "one of the outstanding learned journals of history in the English-speaking world."

According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 0.296 (5-year impact factor 0.406), ranking it 28 out of 69 journals in the category "History".<ref name=WoS>{{cite book |year=2011 |chapter=Journals Ranked by Impact: History |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref>

== Conference grants ==
The Journal funds conferences in any field of contemporary history as defined in the ''Journal of Contemporary History'', that is, history within the memory of some persons now living. Other than this, there is no restriction of field, area or period. Up to £4000 is awarded to fund each conference. Normally only one conference is funded each year. The papers are normally published in a special issue of the journal, unless rejected by the editors.

== Prizes ==
The ''Journal of Contemporary History'' has since 2006 awarded two annual prizes in honour of its founding co-editors [[George L. Mosse]] and [[Walter Laqueur]]. The author of the best article published in the Journal is awarded the Walter Laqueur Prize of US$2,500.<ref>The prize winners are [http://jch.sagepub.com/cgi/collection/walter_laqueur_prize listed here].</ref> The George L. Mosse Prize is awarded to the best article by a previously unpublished author, and is US$2,000.<ref>The prize winners are [http://jch.sagepub.com/cgi/collection/george_l_mosse_prize listed here].</ref> The winner of the first George L. Mosse Prize in 2006 was the British historian of Nazi Germany [[Alex J. Kay]], who won for his article ''Germany’s Staatssekretäre, Mass Starvation and the Meeting of 2 May 1941''.<ref>''Journal of Contemporary History''. Vol. 42, 2007, No. 3, p. 420. The prize winners are [http://jch.sagepub.com/cgi/collection/george_l_mosse_prize listed here].</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://jch.sagepub.com/}}

{{Authority control}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1966]]
[[Category:Quarterly journals]]
[[Category:History journals]]