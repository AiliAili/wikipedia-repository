{{Expand Chinese|中誠信集團|date=March 2013}}
{{Infobox company
| name = China Chengxin Credit Rating Group
| logo = China Chengxin Credit Rating Group Logo.gif
| type = 
| foundation = 1992<ref name="Gup2006">{{cite book|author=Benton E. Gup|title=Capital Markets, Globalization, and Economic Development|url=https://books.google.com/books?id=W7M_CdnX-hUC&pg=PA200|accessdate=2 August 2013|date=20 July 2006|publisher=Springer|isbn=978-0-387-24563-8|pages=200–}}</ref>
| founder = 
| location_city = [[Beijing China Merchants International Financial Center]]<br>[[Beijing]]
| location_country = [[China]]
| location = 
| origins = 
| key_people = [[Dr. MAO Zhenhua]] (Chairman)<ref name=Directors>{{cite web|url=http://www.ccxap.com/About.aspx?Id=3|title=China Chengxin Corp Website}}</ref>
| area_served = 
| industry = [[Bond credit rating]]s
| products = 
| services =
| revenue = 
| operating_income = 
| net_income = 
| num_employees = 
| parent = 
| divisions =
| subsid = 
| owner = 
| company_slogan = 
| homepage = [http://www.ccx.com.cn/ China Chengxin Group]<br>[http://www.ccxi.com.cn/ China Chengxin International]<br>[http://www.ccxr.com.cn/ China Chengxin Securities Rating ]<br>[http://www.ccxap.com/ China Chengxin Asia Pacific]
| intl = yes
| dissolved = 
| footnotes = 
}}

'''China Chengxin Credit Rating Group''' was founded in Beijing on 8 October 1992 through the incorporation of China Chengxin Credit Management Co Ltd (renamed as China Chengxin Credit Management Co. Ltd. in 2002),<ref name="Gup2006" /> which is the first nationwide credit rating company of China.<ref name=Hunt>{{cite web |url=http://www.bbc.co.uk/news/business-20078837 |author=Katie Hunt |title=China ratings firms challenge US dominance |date=31 October 2012 |work=BBC}}</ref> Subsequently, it formed subsidiaries and established branches across China, including China Chengxin International Credit Rating Company Limited (the joint venture credit rating company among China Chengxin Securities Rating Company Limited, [[Fitch Ratings]]  and [[International Finance Corporation]] established in 1999).<ref name="Fitch IBCA Announces Rating Joint Venture in Beijing">{{cite web|title=Fitch IBCA Announces Rating Joint Venture in Beijing|url=http://www.thefreelibrary.com/Fitch+IBCA+Announces+Rating+Joint+Venture+in+Beijing+-+Fitch+IBCA+--a021117106|publisher=Fitch IBCA|accessdate=11 April 2013}}</ref> The share-holder structure of the joint venture company was changed in 2006 when Moody’s came in to take over the equity positions of Fitch and the supranational institution.<ref>{{cite web|url=https://www.moodys.com/Pages/atc002001002.aspx|author=Moody's Corp |title=CCXI}}</ref>
The company is one of the few major credit rating agencies currently operating in China.<ref name="Liaw2011">{{cite book|author=K. Thomas Liaw|title=The Business of Investment Banking: A Comprehensive Overview|url=https://books.google.com/books?id=4w3oVc4ZCMsC&pg=PA352|accessdate=2 August 2013|date=1 November 2011|publisher=John Wiley & Sons|isbn=978-1-118-00449-4|page=352}}</ref>

China Chengxin Credit Management Co. Ltd. went to Hong Kong to set up its subsidiary, China Chengxin (Asia Pacific) Credit Ratings Company Limited, which received the Type Ten License from [[Hong Kong Securities and Futures Commission]] on 28 June 2012, thus became the first Chinese credit rating company going out of the Chinese mainland to do credit rating business in the international capital markets.<ref name=InvestHK>>{{cite web |url=http://www1.investhk.gov.hk/news-item/hong-kongs-role-as-chinas-global-financial-centre-attracts-china-chengxin-credit-rating-company-to-set-up/ |title=Hong Kong’s role as China’s global financial centre attracts China Chengxin credit rating company to set up |date=31 July 2012 |work=InvestHK}}</ref>

China Chengxin Credit Rating Group has branches and subsidiaries operating in Beijing, Shanghai, Hong Kong, Shenzhen, Fujian, Wuhan, Shandong, Liaoning, Tianjin, Jiangsu, Zhejiang, Shanxi and Shaanxi.<ref name="分支机构">{{cite web|title=分支机构
|url=http://www.ccx.com.cn/templates/second/index.aspx?nodeid=9|publisher=CCX|accessdate=2 Aug 2013}}</ref>

== History ==

'''1992-1999: Founding Period'''

In 1992, encouraged by the talks of the then Chinese leader Deng Xiaoping during his tour to the southern China, MAO Zhenhua, like many young people of China, started his private venture to form a credit rating company.<ref name="Fitch Ratings Announces Divestment of China Chengxin International Credit Rating Co. ">{{cite web|title=92派官員下海20年盤點:有人成富豪有人被判刑 |url=http://news.takungpao.com.hk/mainland/focus/2013-08/1831310.html|publisher=Takungpag|accessdate=15 Aug 2013}}</ref> Upon the approval given by People’s Bank of China, MAO was successful in establishing the first Chinese credit rating company, China Chengxin Securities Credit Rating Company Limited under the Chinese corporation rules and regulations.<ref name="Gup2006" />

The company worked out the first set of credit rating system and methodology in China, which was assessed and accredited by the group of experts from the Finance and Economic Committee and Law Committee under the People’s Congress, People’s Bank of China, National Planning Commission, banks and securities house. {{Citation needed|date=January 2014}}

'''1999-2005 Take-off Period'''

As the Chinese capital markets had just started growing, IFC, an agencies of [[World Bank Group]], made efforts to help the Chinese capital markets to develop, including the establishment of an effective and efficient credit rating industry. After several years’ endeavour, this supranational institution succeeded in pulling together China Chengxin Securities Credit Rating Company, Fitch IBCA (now [[Fitch Ratings]]) and itself to form China’s first credit rating joint venture company, China Chengxin International Credit Rating Company Limited in August 1999.<ref name="Fitch IBCA Announces Rating Joint Venture in Beijing"/>  In July 2004, Fitch divested from the joint venture company to align with its global strategy of securing majority control over the overseas subsidiaries.<ref name="Fitch Ratings Announces Divestment of China Chengxin International Credit Rating Co. 2">{{cite web|title=Fitch Ratings Announces Divestment of China Chengxin International Credit Rating Co |url=http://www.prnewswire.com/news-releases/fitch-ratings-announces-divestment-of-china-chengxin-international-credit-rating-co-71322817.html|publisher=Fitch IBCA|accessdate=2 Aug 2013}}</ref>

'''2006-2011 Advancing Period'''

The Chinese capital markets were further reformed in 2005, leading to the fast development of the debt market.  Moody’s moved in to buy up to the regulatory cap of 49% share of China Chengxin International Credit Rating Company Limited from the local share-holders in September 2006. Moody's added additional management and technical support on rating methodologies and training of analysts to the joint venture company.<ref name="Moody's acquires 49 pct stake in Chinese rating agency">{{cite web|title=Moody's acquires 49 pct stake in Chinese rating agency|url=http://english.peopledaily.com.cn/200604/15/eng20060415_258515.html|publisher=People Daily|accessdate=11 April 2013}}</ref>

China Chengxin Securities Credit Rating Company Limited was renamed as China Chengxin Credit Management Company Limited to turn it into a holding company of all subsidiaries and branches formed or to be formed.Subsequently, China Chengxin Credit Rating Management Company Limited has subsidiaries and branches providing credit rating services in Beijing, Shanghai, Hong Kong, Shenzhen, Fujian, Wuhan, Shandong, Liaoning, Tianjin, Jiangsu, Zhejiang and Shanxi.

Owing to its first comer status and expertise acquired from the two international credit rating agencies, respectively. The China Chengxin Credit Rating Group can always capture the largest overall market share in the domestic market.{{Citation needed|date=January 2014}}

'''2012- Going out'''

Following the trend of internationalization of the Chinese currency, China Chengxin Credit Management Company Limited established its subsidiary in Hong Kong, China Chengxin (Asia Pacific) Credit Ratings Company Limited, which received the Type Ten License from Hong Kong Securities and Futures Commission on 28 June 2012, thus became the first Chinese credit rating company going out of the Chinese mainland to do credit rating business in the international capital markets.<ref name="Invest Hong Kong">{{cite web|title=Hong Kong's role as China's global financial centre attracts China Chengxin credit rating company to set up (with photo)|url=http://www.info.gov.hk/gia/general/201207/31/P201207310299.htm|publisher=Hong Kong Government News|accessdate=3 April 2013}}</ref>

== China Chengxin (Asia Pacific) ==

On 28 June 2012, China Chengxin (Asia Pacific) Credit Ratings Company Limited (“CCXAP”) was successful in applying for the type ten operating licence from Hong Kong Securities and Futures Commission, thus, became the first licensed Chinese credit rating agency to operate in the international credit rating market.

== Regulatory Approval & Membership ==

China Chengxin Credit Rating Groups' subsidies and branches obtained license from all the regulators in China such as [[People's Bank of China]], [[China Insurance Regulatory Commission]], [[National Development and Reform Commission]] and [[China Securities Regulatory Commission]].

'''China'''
* 1997 [[People's Bank of China|PBOC]] Nationally Recognized Bond Credit Rating Agency<ref name=FinanceWorld_Jan2012>{{cite news|title=脆弱的评级|url=http://fw.xinhua08.com/a/20120131/894560.shtml|accessdate=19 April 2013|newspaper=Finance World|date=Jan 2012}}</ref>
* 2003 [[China Insurance Regulatory Commission|CIRC]] Recognized Credit Rating Agency<ref name=FinanceWorld_Jan2012/>
* 2003 [[National Development and Reform Commission|NDRC]] Approved Credit Rating Agency<ref name=FinanceWorld_Jan2012/>
* 2005 [[People's Bank of China|PBOC]] Recognized Credit Rating Agency in Interbank Market<ref name=FinanceWorld_Jan2012/>
* 2007 [[China Securities Regulatory Commission|CSRC]] Approved Credit Rating Agency<ref name="中国证券监督管理委员会关于核准中诚信证券评估有限公司从事证券市场资信评级业务的批复">{{cite web|title=中国证券监督管理委员会关于核准中诚信证券评估有限公司从事证券市场资信评级业务的批复|url=http://www.lawyee.org/Act/Act_Display.asp?RID=917186|publisher=教育频道|accessdate=19 April 2013}}</ref>
* Member of [[Securities Association of China]]<ref name="证券评级机构信息公示">{{cite web|title=证券评级机构信息公示|url=http://cx.sac.net.cn/huiyuan/g/cn/cx/pjjg_home.jsp?tab=1&id=3867&pageName=&cn=%D6%D0%B3%CF%D0%C5%D6%A4%C8%AF%C6%C0%B9%C0%D3%D0%CF%DE%B9%AB%CB%BE%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20&code=431002|publisher=SAC|accessdate=19 April 2013}}</ref>
* Member of [[National Association of Financial Market Institutional Investors]](NAFMII)<ref name="会员分类">中介机构>评级机构>{{cite web|title=会员分类>中介机构>评级机构|url=http://www.nafmii.org.cn/hygl/hyfl/zjjg/201202/t20120227_2624.html|publisher=NAFMII|accessdate=19 April 2013}}</ref>
'''Hong Kong'''
* 2012 [[Hong Kong Securities and Futures Commission|SFC]] Type 10: Providing credit rating services<ref name="Public Register of Licensed Persons and Registered Institutions">{{cite web|title=Public Register of Licensed Persons and Registered Institutions|url=http://www.sfc.hk/publicregWeb/corp/AZE668/details|publisher=SFC|accessdate=19 April 2013}}</ref>

== Sovereign Credit Rating ==
<!-- key for sorting, China Chengxin: Cg=01; CCg=02; CCCg=03; Bg-=04; Bg=05; Bg+=06; BBg-=07; BBg=08; BBg+=09; BBBg-=10; BBBg=11; BBBg+=12; Ag-=13; Ag=14; Ag+=15; AAg-=16; AAg=17; AAg+=18; AAAg=19; -->
China Chengxin Credit Rating Group through China Chengxin International Credit Rating Company Limited released its sovereign credit rating report for 30 countries in July 2012, as follows:.<ref name="Mainland credit rating agency opens HK arm">{{cite news|last=Jia|first=Chen|title=Mainland credit rating agency opens HK arm|url=http://www.chinadaily.com.cn/china/2012-08/01/content_15636429.htm|accessdate=19 April 2013|newspaper=China Daily|date=2012-08-01}}</ref>
{| class="sortable wikitable"
|-  style="background:#ececec; vertical-align:top;"
! Country !! Rating !! Outlook !! Date !! Ref.
|-
| {{flag|Argentina}} || <span style="display:none;">05</span>CCCg || Negative || 2014-07 ||<ref name= chengxin>{{cite web|url=http://www.ccxi.com.cn/245/311/YjfxList.html |title=中诚信国际信用评级有限责任公司 |publisher=Ccxi.com.cn |date=2012-08-15 |accessdate=2014-01-05}}</ref>
|-
| {{flag|Australia}} || <span style="display:none;">19</span>AAAg || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Brazil}} || <span style="display:none;">12</span>BBBg+ || Stable || 2012-07 ||<ref name= chengxin/>
|-
| {{flag|Canada}} || <span style="display:none;">19</span>AAAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|China}} || <span style="display:none;">17</span>AAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Denmark}} || <span style=" display:none;">17</span>AAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|France }} || <span style="display:none;">16</span>AAg || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Germany}} || <span style="display:none;">17</span>AAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Greece}} || <span style="display:none;">01</span>CCCg || Stable || 2014-04 ||<ref name= chengxin/>
|-
| {{flag|India}} || <span style="display:none;">11</span>BBBg || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Indonesia}} || <span style="display:none;">10</span>BBBg- || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Ireland }} || <span style="display:none;">10</span>BBBg- || Negative || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Italy}} || <span style="display:none;">11</span>BBBg || Stable || 2013-03 ||<ref name="中诚信国际：意大利政治不确定性对其主权信用带来负面影响">{{cite web|title=中诚信国际：意大利政治不确定性对其主权信用带来负面影响|url=http://www.ccxi.com.cn/272/486/NewsInfo.html|publisher=中诚信国际|accessdate=19 April 2013}}</ref>
|-
| {{flag|Japan}} || <span style="display:none;">18</span>AAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Mexico}} || <span style="display:none;">12</span>BBBg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag| Netherlands}} || <span style="display:none;">18</span>AAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Norway}} || <span style="display:none;">19</span>AAAg || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Portugal}} || <span style="display:none;">07</span>BBg- || Negative || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Russia}} || <span style="display:none;">14</span>Ag || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Saudi Arabia}} || <span style="display:none;">18</span>AAg+ || Stable || 2013-07 ||<ref name=chengxin/>
|-
| {{flag|Singapore}} || <span style="display:none;">19</span>AAAg || Stable || 2013-07 ||<ref name=chengxin/>
|-
| {{flag|South Africa}} || <span style="display:none;">14</span>Ag || Stable || 2013-07 ||<ref name=chengxin/>
|-
|{{flag|South Korea}} || <span style="display:none;">16</span>AAg- || Stable || 2013-07 ||<ref name=chengxin/>
|-
| {{flag|Spain}} || <span style="display:none;">09</span>BBg+ || Negative || 2013-03 ||<ref name="中诚信国际：西班牙主权评级维持BBg+，评级展望由稳定调为负面">{{cite web|title=中诚信国际：西班牙主权评级维持BBg+，评级展望由稳定调为负面|url=http://www.ccxi.com.cn/272/485/NewsInfo.html|publisher=中诚信国际|accessdate=19 April 2013}}</ref>
|-
| {{flag|Switzerland}} || <span style="display:none;">19</span>AAAg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Thailand}} || <span style="display:none;">12</span>BBBg+ || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|Turkey}} || <span style="display:none;">08</span>BBg || Stable || 2012-07 ||<ref name=chengxin/>
|-
| {{flag|United Kingdom}} || <span style="display:none;">18</span>AAg+ || Stable || 2013-03 ||<ref name="中诚信国际：下调英国主权评级至AAg+，评级展望为稳定">{{cite web|title=中诚信国际：下调英国主权评级至AAg+，评级展望为稳定|url=http://www.ccxi.com.cn/272/488/NewsInfo.html|publisher=中诚信国际|accessdate=19 April 2013}}</ref>
|-
| {{flag|United States of America}} || <span style="display:none;">19</span>AAAg || Stable || 2012-07 ||<ref name=chengxin/>
|}

== References ==

{{reflist}}

[[Category:Credit rating agencies]]
[[Category:Financial services companies of China]]
[[Category:Companies based in Beijing]]