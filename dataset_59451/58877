{{italictitle}}
{{Infobox journal
| title        = Medical Physics
| cover        = 
| editor       = Jeffrey F. Williamson 
| discipline   = [[Physics]], [[medicine]]
| language     = [[English language|English]]
| abbreviation = Med. Phys.
| publisher    = [[American Institute of Physics]]
| country      = US
| frequency    = 12 per year
| history      = 1974–present   
| openaccess   = [[Hybrid open access journal|Hybrid]]
| impact       = 2.635
| impact-year  = 2014
| website      = http://www.medphys.org
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         = 
| LCCN         = 74645246
| CODEN        = MPHYA6
| ISSN         = 0094-2405
| eISSN        = 
}}

'''''Medical Physics''''' is a monthly [[peer review]]ed [[scientific journal]] covering research on [[medical physics]].<ref name="APESM">{{Cite web
  | title = Medical Physics
  | publisher = [[American Institute of Physics]]
  | url = http://www.medphys.org
  | accessdate = 2015-11-25}}
</ref> The first issue was published in January 1974. Medical Physics is an official journal of the [[American Association of Physicists in Medicine]], the Canadian Organization of Medical Physicists, the Canadian College of Physicists in Medicine and the [[International Organization for Medical Physics]].

==Abstracting and indexing==
''Medical Physics'' is indexed in:
*  [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-11-25}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[Pubmed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/425746 |title=Medical physics |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-11-25}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2015-11-25}}</ref>

==References==
{{reflist}}

==External links==
*[http://www.medphys.org Official website]

[[Category:Medical physics journals]]
[[Category:American Institute of Physics academic journals]]
[[Category:Publications established in 1974]]
[[Category:English-language journals]]
[[Category:Monthly journals]]