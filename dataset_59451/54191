{{Infobox Australian Electorate
| name               = Dunstan
| state              = sa
| image              = Dunstan Electoral District SA 2014.png
| imagesize          = 
| image_alt          = Map of Adelaide, South Australia with electoral district of Dunstan highlighted
| caption            = Electoral district of Dunstan (green) in the Greater Adelaide area
| created            = 2014
| mp                 = [[Steven Marshall]]
| mp-party           = [[Liberal Party of Australia (South Australian Division)|Liberal Party of Australia (SA)]]
| namesake           = [[Don Dunstan]]
| electors           = 24,581
| electors_year      = 2014
| electors_footnotes = 
| area               = 14.6
| class              = Metropolitan
| latd               = 34
| latm               = 54
| lats               = 5
| longd              = 138
| longm              = 38
| longs              = 8
}}
'''Dunstan''' is an [[Electoral districts of South Australia|electoral district]] of the [[South Australian House of Assembly|House of Assembly]] in the Australian state of [[South Australia]], covering the inner eastern suburbs of [[Beulah Park, South Australia|Beulah Park]], [[College Park, South Australia|College Park]], [[Evandale, South Australia|Evandale]], [[Firle, South Australia|Firle]], [[Hackney, South Australia|Hackney]], [[Joslin, South Australia|Joslin]], [[Kensington, South Australia|Kensington]], [[Kent Town, South Australia|Kent Town]], [[Marden, South Australia|Marden]], [[Maylands, South Australia|Maylands]], [[Norwood, South Australia|Norwood]], [[Payneham South, South Australia|Payneham South]], [[Royston Park, South Australia|Royston Park]], [[St Morris, South Australia|St Morris]], [[St Peters, South Australia|St Peters]], [[Stepney, South Australia|Stepney]], [[Trinity Gardens, South Australia|Trinity Gardens]] and [[Vale Park, South Australia|Vale Park]] and parts of [[Klemzig, South Australia|Klemzig]] and [[Payneham, South Australia|Payneham]]. 

The electorate was created in a 2012 redistribution of electoral boundaries.  It was essentially a reconfigured version of [[electoral district of Norwood|Norwood]], with electoral boundaries remaining unchanged.  It is named after the 35th [[Premier of South Australia]] [[Don Dunstan]], who represented Norwood for [[Australian Labor Party|Labor]] from 1953 to 1979. As of the [[South Australian state election, 2010|2010 election]] it was the first time that Labor was in government without holding Norwood.

The seat is held by [[Liberal Party of Australia|Liberal]] [[Leader of the Opposition (South Australia)|Opposition Leader]] [[Steven Marshall]].

==Members for Dunstan==
{| class="wikitable"
|-
! colspan="2"|Member
! Party
! Term
|-
| {{Australian party style|Liberal}}|&nbsp;
| [[Steven Marshall]]
| [[Liberal Party of Australia|Liberal]]
| 2014–present
|-
|}

==Election results==
{{main|Electoral results for the district of Dunstan}}
{{Election box begin |
|title=[[South Australian state election, 2014]]: Dunstan<ref>[http://www.ecsa.sa.gov.au/elections/state-elections/past-state-election-results/7666?view=result 2014 State Election Results – Dunstan], ECSA.</ref><ref>[http://www.abc.net.au/news/sa-election-2014/guide/duns/ 2014 State Election Results – Dunstan], ABC.</ref>
}}
{{Election box candidate AU party|
|candidate = [[Steven Marshall]]
|party = Liberal
|votes = 10,978
|percentage = 50.0
|change = +3.7
}}
{{Election box candidate AU party|
|candidate = Jo Chapley
|party = Labor
|votes = 7,881
|percentage = 35.9
|change = +2.1
}}
{{Election box candidate AU party|
|candidate = Michael Donato
|party = Greens
|votes = 2,465
|percentage = 11.2
|change = −0.5
}}
{{Election box candidate AU party|
|candidate = Rick Neagle
|party = Dignity for Disability
|votes = 624
|percentage = 2.8
|change = +1.0
}}
{{Election box formal|
|votes = 21,948
|percentage = 97.8
|change = +1.4
}}
{{Election box informal|
|votes = 494
|percentage = 2.2
|change = −1.4
}}
{{Election box turnout|
|votes = 22,442
|percentage = 91.3
|change = −0.4
}}
{{Election box 2pp}}
{{Election box candidate AU party|
|candidate = [[Steven Marshall]]
|party = Liberal
|votes = 11,656
|percentage = 53.1
|change = −1.7
}}
{{Election box candidate AU party|
|candidate = Jo Chapley
|party = Labor
|votes = 10,292
|percentage = 46.9
|change = +1.7
}}
{{Election box hold AU party|
|winner = Liberal
|swing = −1.7
}}
{{Election box end}}

==See also==
*[[Electoral results for the district of Norwood]]

==Notes==
{{reflist}}

==References==
* [http://www.abc.net.au/news/sa-election-2014/guide/duns/ ABC profile for Dunstan: 2014]
* [http://www.ecsa.sa.gov.au/component/edocman/?view=document&id=572 ECSA profile for Dunstan: 2014]
* [http://blogs.crikey.com.au/pollbludger/sa2014-dunstan Poll Bludger profile for Dunstan: 2014]

{{Electoral districts of South Australia |state=expanded}}
{{Former electoral districts of South Australia |state=collapsed}}

{{DEFAULTSORT:Dunstan, Electoral district of}}
[[Category:Electoral districts of South Australia]]
[[Category:2014 establishments in Australia]]