{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = René
| title_orig    = 
| translator    = 
| image         = Chateabriand Rene.jpg 
| caption       = René tells his story to Chactas and Father Souel 
| author        = [[François-René de Chateaubriand]]
| illustrator   =  
| cover_artist  = 
| country       = France
| language      = French
| series        = 
| genre         = [[Romanticism]], [[novella]]
| publisher     = 
| pub_date      = 1802
| english_pub_date =
| media_type    = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| preceded_by   = 
| followed_by   = 
}}
'''''René''''' is a short [[novella]] by [[François-René de Chateaubriand]], which first appeared in 1802. The work had an immense impact on early [[Romanticism]], comparable to that of [[Goethe]]'s ''[[The Sorrows of Young Werther]]''. Like the German novel, it deals with a sensitive and passionate young man who finds himself at odds with contemporary society.  ''René'' was first published as part of Chateaubriand's ''[[Génie du christianisme]]'' along with another novella, ''[[Atala (novella)|Atala]]'', although it was in fact an excerpt from a long prose epic the author had composed between 1793 and 1799  called ''[[Les Natchez]]'', which would not be made public until 1826. ''René'' enjoyed such immediate popularity that it was republished separately in 1805 along with ''Atala''.

==Plot summary==

René, a desperately unhappy young Frenchman, seeks refuge among the [[Natchez people]] of [[New France|Louisiana]]. It is a long time before he is persuaded to reveal the cause of his melancholy. He tells of his lonely childhood in his father's castle in Brittany. His mother died giving birth to him and since his father is a remote, forbidding figure, René takes refuge in an intense friendship with his sister Amélie and in long, solitary walks in the countryside around the castle.

When René's father dies and his brother inherits the family home, he decides to travel. He visits the ruins of ancient Greece and Rome which inspire him with melancholy reflections. He travels to Scotland to view the places mentioned by the bard [[Ossian]] and to the famous sights of Italy. Nothing satisfies him: "The ancient world had no certainty, the modern world had no beauty." He returns to France and finds society corrupt and irreligious. His sister Amélie inexplicably seems to avoid him too. As René explains: 
:''"I soon found myself more isolated in my own land, than I had been in a foreign country. For a while I wanted to fling myself into a world which said nothing to me and which did not understand me. My soul, not yet worn out by any passion, sought an object to which it might be attached; but I realised I was giving more than I received. It was not elevated language or deep feelings that were asked of me. My only task was to shrink my soul and bring it down to society's level."''

Disgusted, René withdraws from society and lives in an obscure part of the city. But this reclusive life soon bores him too. He decides to move to the countryside but he finds no happiness there: "Alas, I was alone, alone on the earth. A secret languor was taking hold of my body. The disgust for life I had felt since childhood came back with renewed force. Soon my heart no longer provided food for my mind, and the only thing I felt in my existence was a deep ennui."

René decides to kill himself, but when his sister learns of his plan, the two are joyfully reunited. But there is no happy ending. Amélie seems to be pining for something. One day, René finds she has gone, leaving a letter saying she wants to become a nun but giving no explanation why. René goes to witness her initiation ceremony where she reveals she has joined the convent because she wants to overcome her incestuous love for him. Devastated by this confession, René decides to leave Europe forever and travel to America. After spending some time with the Indians, he receives a letter announcing his sister's death. The novella concludes by revealing shortly after René told his tale, he was killed in a battle between the Natchez and the French.

(Note: according to the version in ''Les Natchez'', the action of the story takes place in the 1720s).

==Literary significance & criticism==
[[File:Rene chateaubriand image.jpg|thumb|René walking with Amélie]]
As the title suggests, there are numerous autobiographical elements in the book, which draws extensively on Chateaubriand's memories of his own childhood in [[Brittany]] and his travels in [[North America]] in 1791. Chateaubriand was criticised for his use of the theme of incest and there is no evidence that his sister Lucile had any such passion for him in real life.

''René'' proved an immense inspiration to young Romantics who felt it was the perfect expression of the ''mal du siècle'' their generation  experienced. Notable admirers included [[Berlioz]] and [[Alfred de Musset]]. Its fame reached abroad; René's travels through Europe were imitated by [[Lord Byron]] in ''[[Childe Harold's Pilgrimage]]''. Both René and Harold are restless outsiders with an aristocratic contempt for the banality of the world. Like Goethe with ''Werther'', in later years, Chateaubriand came to resent the popularity of his early work. As he wrote in his memoirs:
*"If ''René'' did not exist, I would not write it again; if it were possible for me to destroy it, I would destroy it. It spawned a whole family of René poets and René prose-mongers; all we hear nowadays are pitiful and disjointed phrases; the only subject is gales and storms, and unknown ills moaned out to the clouds and to the night. There's not a fop who has just left college who hasn't dreamt he was the most unfortunate of men; there's not a milksop who hasn't exhausted all life has to offer by the age of sixteen; who hasn't believed himself tormented by his own genius; who, in the abyss of his thoughts, hasn't given himself over to the "wave of passions"; who hasn't struck his pale and dishevelled brow and astonished mankind with a sorrow whose name neither he, nor it, knows".

==References==
*Chateaubriand: ''Atala, René, Les aventures du dernier Abencérage'', ed. Jean-Claude Berchet, Flammarion (1996)

==External links==
*[http://hypo.ge.ch/athena/chateaubriand/chat_ren.html ''René''] in French at [http://hypo.ge.ch/athena/html/athome.html ATHENA]
* {{fr}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/chateaubriand-francois-rene-de-rene-texte-integral.html/ ''René'', audio version] [[Image:Speaker Icon.svg|20px]]
*[http://www.poetryintranslation.com/PITBR/Chateaubriand/ChateaubriandRene.htm English translation]

{{Authority control}}

{{DEFAULTSORT:Rene (novella)}}
[[Category:1802 novels]]
[[Category:Works by François-René de Chateaubriand]]
[[Category:French novellas]]
[[Category:19th-century French novels]]