{{Use mdy dates|date=October 2014}}
[[Image:Johnwise.jpg|thumb|right|John Wise]]

'''John Wise''' (February 24, 1808 – September 28, 1879?) was a pioneer in the field of [[Balloon (aircraft)|ballooning]]. He made over 400 flights during his lifetime and was responsible for several innovations in balloon design. His balloon, ''The Jupiter'' carried 123 letters to make the first official airmail delivery run for the US Post Office in 1859.

== Early career ==
John Wise was born on February 24, 1808 in [[Lancaster, Pennsylvania]], United States, to William and Mary Trey Weiss who anglicized his surname to Wise. He was the fourth of eight children. He worked as an apprentice cabinetmaker from the time he was 16; after the age of 21 he briefly became a piano maker. He had been interested in ballooning since reading an article in the newspaper when he was 14, and in 1835, at the age of 27, he decided to construct his own balloon.

Wise made his first ascent in [[Philadelphia]] on May 2, 1835. As the construction had been self-financed the materials of his home-made balloon were not of the highest quality. He used [[muslin]] sheet coated with a mixture of [[birdlime]] suspended in [[linseed oil]] to make the sheet impermeable. Unlike most balloonists of the day, Wise was not ballooning as a commercial venture, but rather for his own interest and scientific curiosity. The ascent was short and uneventful.

He took a second flight in [[Lebanon County, Pennsylvania|Lebanon County]] on [[Independence Day (United States)|Independence Day]] 1835. He attempted to open the valve on the top of the balloon, but lost control, and it burst, compelling him to descend. On October 1, 1835, he attempted an ascension from [[Lancaster, Pennsylvania]], but was thrown from the car and became unconscious while the balloon ascended alone.  On May 7, 1836, he ascended again from Lancaster, and landed in [[Harford County, Maryland]], about 75 miles distant. While he was emptying the car of its cargo an explosion of the gas occurred and burned him severely.

He made a voyage from Philadelphia on September 18, 1837, alighting in [[Delaware River]], where he was rescued. On this trip he set loose two [[parachute]]s for the purpose of demonstrating the superiority of the inverted parachute. In October 1837, he ascended again from Philadelphia, and alighted in New Jersey, 40 miles from his starting-point.

In his early flights in Pennsylvania, he conducted various experiments on [[atmospheric pressure]], [[pneumatics]] and [[Fluid statics|hydrostatics]], and while his primary interest remained scientific, he joined the ranks of commercial balloonists performing at shows and county [[fair]]s.

== Projects and innovations ==

[[Image:Jwise airmail.jpg|thumb|right|Wise starts the first airmail delivery in the United States on August 17, 1859 from [[Lafayette, Indiana]].]]
In 1838 he developed a balloon that if ruptured or deflated when aloft would collapse to form a parachute (the bottom half would fold upwards into the top half to form the classic parachute shape) which would allow the occupants of the basket to descend without injury or loss of life. Although the idea was not original, Wise was the first to build a working version and the first to demonstrate its use. On a flight from [[Easton, Pennsylvania]], on August 11, 1838, in bad weather, the design was put to an impromptu test when Wise's balloon was punctured at 13,000 feet. In less than ten seconds all the gas had escaped. The balloon descended rapidly with an oscillating motion, and, on reaching the earth, rebounded, throwing Wise ten feet from the car. Wise survived without injury. He later advertised that on October 1, 1838 he would ascend and in the air would convert his balloon into a parachute, which feat he successfully accomplished.

After the death of [[Robert Cocking]] in the first modern parachuting accident, questions were raised over which of the two competing parachute designs was superior: the cone-shaped parachute proposed by [[Sir George Cayley]] and used by Cocking, or the umbrella-shaped design used by [[André-Jacques Garnerin]] in his successful jump of 1797. Wise conducted numerous experiments comparing the two designs and found that Cayley's design always made a more stable descent. Cocking's failure was put down to poor calculations and substandard construction. (The oscillation problem inherent in the Garnerin parachute was later solved by the introduction of a vent in the top of the canopy).

Another of Wise's innovations was the rip panel for controlled deflation on landing. Prior to Wise's use of the rip panel, balloons would drag along the ground when landing and had to be secured by anchors and lines. Balloonists wishing to deflate their balloons would climb out of their baskets onto the netting surrounding the balloon, and having scaled to the top of the balloon would open the valve to allow the gas to escape. The weight of the balloonist would cause the balloon to collapse inwards and there had been a number of accidents where the balloonists had been killed after becoming entangled in the rigging. Wise also recognised that the heat from the sun played a valuable role in warming the gas in the balloon, and built a black balloon to utilize the effects. He was the first to observe the [[jet stream]], noting there was a "great river of air which always blows from west to east". On August 17, 1859 he made the first flight of local [[airmail]] in the U.S. from [[Lafayette, Indiana]] to [[Crawfordsville, Indiana]] in a balloon named ''Jupiter,'' <ref>[http://arago.si.edu/index.asp?con=1&cmd=1&tid=2033764 Smithsonian National Postal Museum]</ref> carrying 123 letters and 23 circulars of which one [[Cover (philately)|cover]] was discovered in 1957.<ref>
{{cite book  | last = Mackay  | first = James A.  | authorlink = James A. Mackay  | title = Airmails 1870–1970  | publisher = B.T. Batsford Ltd.  | year = 1971  | location = London  | pages = 17–18  | isbn = 0-7134-0380-2 }}</ref>
His trip of 25 miles ended when he was forced to land by lack of buoyancy.

=== Transatlantic aspirations ===
As one who recognized the possibilities of balloon flight by use of the high wind yet to be named the [[Jet Stream]], Wise had made plans for a transatlantic flight in a large aerostat he had built and named ''Atlantic''. Unfortunately his test flights for just such a trip were less than successful. Where he had enjoined company with another yet younger prominent balloonist, a Mr. [[John LaMountain]], an 1857 pre-flight of theirs had ended up caught by a windstorm over Lake Ontario forcing a crash landing in [[Henderson, New York]], which damaged the balloon and ended their partnership. La Mountain took over ownership of the ''Atlantic'' but any further talk of a transatlantic flight was never heard.

=== American Civil War ===
Wise was one of several top American balloonists who made a bid for Chief Aeronaut of a yet-to-be-established balloon corps for the Union Army during the opening months of the [[American Civil War]] (see [[Union Army Balloon Corps]]). Against major competition which included [[Thaddeus S. C. Lowe]] and John La Mountain, he lacked either the endorsements of the science community, like those of Prof. Lowe, or the insidious propaganda ploys, like those of La Mountain. However, he did attract enough attention from topographical engineers to be recommended for building a balloon for the purposes of demonstrating aerial surveillance for [[map making]] and undercut the bids of the others by $200.

By July 19, 1861 General [[Irvin McDowell]]'s army was prepared to face the [[First Battle of Bull Run]]. McDowell had called for a balloon to be sent to the front, but the Engineers awaited the belated arrival of John Wise. Thaddeus Lowe was at the ready and had been called up to inflate his balloon in Wise's stead. At the last minute Wise appeared with papers in hand demanding that Lowe step aside and allow him to inflate his balloon, which was rightfully commissioned into action. Major [[Albert J. Myer]] and 20 men from the 26th Pennsylvania Volunteers secured the inflated balloon to a wagon and proceeded toward the battlefield at Centerville, Virginia. In his haste to move forward, Myer entangled the balloon in the trees, which disabled the craft and permanently removed Wise from involvement in the Civil War.<ref>Lowe's Official Report Part I.</ref>

In January 1918, during World War I, the Army established Camp John Wise Aerostation at [[San Antonio]], in what is now [[Olmos Park]], Texas, as a war balloon training center.

== Disappearance ==

On September 28, 1879, aged 71, he [[List of aerial disappearances|disappeared]] with a passenger George Burr on a trip in high speed winds from [[East St. Louis, Illinois]] over [[Lake Michigan]]. It is reported the balloon was seen over [[Carlinville, Illinois]]<ref>[https://books.google.com/books?id=3hgXAAAAYAAJ&pg=PA201&dq=John+Wise+in+Ballon+Pathfinder&lr=&cd=12#v=onepage&q&f=false .p.202]</ref> or was seen 35 miles from Chicago drifting northeastward toward Lake Michigan at "[[Mount Prospect, Illinois|Miller's Station]]".<ref>[https://books.google.com/books?id=SBMLAAAAYAAJ&pg=PA448&dq=John+Wise+in+Ballon+Pathfinder&lr=&cd=9#v=onepage&q&f=false .p.448]</ref> No trace of Wise or the balloon "Pathfinder" have ever been found. The body of George Burr, the passenger, was found in Lake Michigan, and left little doubt as to the fate of John Wise.<ref>[https://books.google.com/books?id=SBMLAAAAYAAJ&pg=PA448&dq=John+Wise+in+Ballon+Pathfinder&lr=&cd=9#v=onepage&q&f=false .p.448]</ref>

In 44 years, Wise had made 463 ascents. Wise published a ''System of Aeronautics'' (Philadelphia, 1850), and ''Through the Air: A Narrative of Forty Years' Experience as an Aeronaut'' (Philadelphia, 1873).

==See also==
*[[List of accidents and incidents involving military aircraft (pre-1925)]]

== Notes ==
{{reflist}}

== References ==
* {{cite encyclopedia|encyclopedia =Encyclopædia Britannica|title = Parachute| publisher = Cambridge University Press| location = London| edition = 11th| year = 1911}}
* {{cite web|url=http://www.johnwise.net/jw.html|title=John Wise, A Pioneer|publisher=John Wise Balloon Society of Central Pennsylvania|author=Nick Moehlmann|date=|accessdate=July 2, 2007}}
* {{cite web|url=http://www.ctie.monash.edu.au/hargrave/wise.html|title=The Pioneers, John Wise|publisher=Monash University Engineering Department|author=Russell Naughton|year=2002|accessdate=July 2, 2007}}
* {{cite web|url=https://engineering.purdue.edu/AAE/AboutUs/History|title=History of the Purdue School of Aeronautics and Astronautics|publisher=Purdue University|author=|year=2007|accessdate=July 2, 2007}}
* {{cite Appletons'|wstitle=Wise, John (aëronaut)|year=1889}}

{{Authority control}}

{{DEFAULTSORT:Wise, John}}
[[Category:1808 births]]
[[Category:1879 deaths]]
[[Category:American balloonists]]
[[Category:Aviation pioneers]]
[[Category:Missing people]]
[[Category:Missing aviators]]
[[Category:People from Lancaster, Pennsylvania]]