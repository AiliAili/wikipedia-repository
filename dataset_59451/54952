{{Infobox district
<!-- See Template:Infobox district for additional fields and descriptions -->
| name                    = Gagra District
| native_name             = {{lang|ka|გაგრის რაიონი}}<br>{{lang|ab|Гагра араион}}<br />{{lang|ru|Гагрский район}}
| type                    = [[Districts of Abkhazia|District]]
| image_skyline           = Gegsky1.jpg
| image_alt               = 
| image_caption           = Gega waterfall
| image_map               = Abkhazia_Gagra_map.svg
| map_alt                 = 
| map_caption             = Location of Gagra District in Abkhazia (de facto bordering)
| latd  =  |latm  =  |lats  =  |latNS  = 
| longd =  |longm =  |longs =  |longEW = 
| coordinates_type        = region:GE-AB_type:adm1st
| coordinates_display     = inline,title
| coordinates_footnotes   = 
| subdivision_type        = [[List of sovereign states|Country]]
| subdivision_name        = {{flag|Georgia}}
| subdivision_type1       = [[List of sovereign states#Other states|''De facto'' state]]
| subdivision_name1       = {{flag|Abkhazia}}<ref>{{Abkhazia-note}}</ref>
| established_title       = 
| established_date        = 
| seat_type               = [[Capital (political)|Capital]]
| seat                    = [[Gagra]]
| government_footnotes    = 
| leader_party            = 
| leader_title            = Governor
| leader_name             = [[Zaur Bganba]]
| unit_pref               = Metric<!-- or US or UK -->
| area_footnotes          = 
| area_total_km2          = 772
| area_note               = 
| elevation_footnotes     = 
| elevation_m             = 
| population_footnotes    = 
| population_total        = 37002
| population_as_of        = 2003
| population_density_km2  = auto
| population_demonym      = 
| population_note         = 
| timezone1               = [[Moscow Time|MSK]]
| utc_offset1             = +3
| postal_code_type        = 
| postal_code             = 
| area_code_type          = 
| area_code               = 
| website                 = <!-- [http://www.example.com example.com] -->
| footnotes               = 
}}
[[File:GEO-AB-GG.svg|thumbnail|Location of Gagra District on the map of Georgia|280px]]
'''Gagra District''' is a district of [[Abkhazia]].<ref group="notes">{{Abkhazia-note}}</ref> It corresponds to the [[Administrative divisions of Georgia (country)|Georgian district]] by the same name. In medieval times, it was known as the southern part of [[Sadzen]]. It is located in the western part of Abkhazia, and the river [[Psou]] serves as a border with [[Krasnodar Krai]] of [[Russia]]. Its capital is [[Gagra]], the town by the same name. The population of the ''Gagra town zone'' in 1989 was 77,079,<ref name=censuses/> but this number dropped dramatically following the [[collapse of the Soviet Union]] and the [[War in Abkhazia (1992-1993)|1992-1993 war in Abkhazia]], (including the [[Ethnic cleansing of Georgians in Abkhazia|ethnic cleansing of Georgians]]), to 37,002 at the time of the 2003 census. Ethnic Armenians now constitute a plurality in the district.

==Administration==
[[Grigori Enik]] was reappointed as Administration Head on 10 May 2001 following the March 2001 local elections.<ref name="press2001-92">{{cite news|title=Выпуск № 92|url=http://abkhazia.narod.ru/gb/1579|accessdate=24 April 2016|agency=[[Apsnypress]]|date=10 May 2001}}</ref>

In December 2002, Enik was appointed Head of the State Customs Committee,<ref name=uzel32238>{{cite news|script-title=ru:Новые назначения в правительстве Абхазии|url=http://www.kavkaz-uzel.ru/articles/32238/|accessdate=18 April 2011|newspaper=[[Caucasian Knot]]|language=Russian|date=18 December 2002}}</ref> he was succeeded by [[Valeri Bganba]].<ref name=uzel36972>{{cite news|title=Бганба Валерий Рамшухович|url=http://www.kavkaz-uzel.ru/articles/36972/|accessdate=13 January 2012|newspaper=[[Caucasian Knot]]|date=24 April 2003}}</ref>
On 25 May 2006, Bganba was released from office by President Bagapsh upon his own request, and succeeded by [[Astamur Ketsba]].<ref name=regnum647250>Regnum.ru [http://www.regnum.ru/news/647250.htmlГлавой администрации Гагрского района Абхазии назначен Астамур Кецба], 26.05.2006</ref> In turn, after the [[Abkhazian presidential election, 2011|election of Alexander Ankvab]], on 6 September 2011 Ketsba was dismissed upon his own request and temporarily replaced by his deputy [[Teimuraz Kapba]].<ref name=apress4172>{{cite news|title=Астамур Кецба освобожден от должности главы Администрации Гагрского района на основании личного заявления|url=http://apsnypress.info/news/4172.html|accessdate=23 September 2011|newspaper=[[Apsnypress]]|date=6 September 2011}}</ref> On 15 November, [[Grigori Enik]], who had previously headed the Presidential Administration, was appointed Acting Head of Gagra District.<ref name=apress4721>{{cite news|title=Григорий Еник назначен исполняющим обязанности главы администрации Гагрского района|url=http://apsnypress.info/news/4721.html|accessdate=30 December 2011|newspaper=[[Apsnypress]]|date=15 November 2011}}</ref> On 28 May 2012, Enik was permanently appointed.<ref name=gcity86>{{cite news|title=Григорий Еник назначен главой администрации Гагрского района |url=http://gagra.biz/2-new/86-grigorij-enik-naznachen-glavoj-administratsii-gagrskogo-rajona |archive-url=https://archive.is/20130115055421/http://gagra.biz/2-new/86-grigorij-enik-naznachen-glavoj-administratsii-gagrskogo-rajona |dead-url=yes |archive-date=15 January 2013 |accessdate=28 May 2012 |newspaper=Gagra District Administration |date=28 May 2012 }}</ref>

Following the [[Abkhazian Revolution|May 2014 Revolution]] and the election of [[Raul Khajimba]] as President, he dismissed Enik and replaced him with MP Beslan Bartsits (as acting Head) on 22 October.<ref name=apress13329>{{cite news|title=Беслан Барциц назначен исполняющим обязанности главы администрации Гагрского района|url=http://apsnypress.info/news/13329.html|accessdate=22 October 2014|newspaper=[[Apsnypress]]|date=22 October 2014}}</ref> Bartsits was confirmed in his post the following year.<ref name="inform3771">{{cite news|title=БЕСЛАН БАРЦИЦ НАЗНАЧЕН РУКОВОДИТЕЛЕМ АДМИНИСТРАЦИИ ПРЕЗИДЕНТА РЕСПУБЛИКИ АБХАЗИЯ|url=http://abkhazinform.com/item/3771-beslan-bartsits-naznachen-rukovoditelem-administratsii-prezidenta-respubliki-abkhaziya|accessdate=31 July 2016|agency=[[Abkhazia Inform]]|date=16 May 2016}}</ref>

On 16 May 2016, Bartsits became Head of the Presidential Administration.<ref name="press_beslan">{{cite news|title=Беслан Барциц назначен Руководителем Администрации Президента РА|url=http://apsnypress.info/news/beslan-bartsits-naznachen-rukovoditelem-administratsii-prezidenta-ra/|accessdate=18 May 2016|agency=[[Apsnypress]]|date=16 May 2016}}</ref> That same day, Gagra Forestry Director [[Zaur Bganba]] was appointed acting District Head.<ref name="press_zaur">{{cite news|title=Заур Бганба назначен исполняющим обязанности главы Администрации Гагрского района|url=http://apsnypress.info/news/zaur-bganba-naznachen-ispolnyayushchim-obyazannosti-glavy-administratsii-gagrskogo-rayona/|accessdate=18 May 2016|agency=[[Apsnypress]]|date=16 May 2016}}</ref> Bganba was confirmed in his post on 2 June.<ref name="press_prezident">{{cite news|title=Президент подписал указы о назначении глав районных и городских администраций|url=http://apsnypress.info/news/prezident-podpisal-ukazy-o-naznachenii-glav-rayonnykh-i-gorodskikh-administratsiy/|accessdate=31 July 2016|agency=[[Apsnypress]]|date=3 June 2016}}</ref><ref name="inform3894">{{cite news|title=ПРЕЗИДЕНТ РАУЛЬ ХАДЖИМБА ПОДПИСАЛ УКАЗЫ О НАЗНАЧЕНИИ ГЛАВ РАЙОННЫХ И ГОРОДСКИХ АДМИНИСТРАЦИЙ|url=http://abkhazinform.com/item/3894-prezident-raul-khadzhimba-podpisal-ukazy-o-naznachenii-glav-rajonnykh-i-gorodskikh-administratsij|accessdate=16 November 2016|agency=[[Abkhazia Inform]]|date=2 June 2016}}</ref>

===List of Governors===

{| class="wikitable" width=70% cellpadding="2"
|-style="background-color:#E9E9E9; font-weight:bold" align=left
| #
| width=150|Name
| width=150|From
|
| width=150|Until
|
| width=150|President
| width=50|Comments
|-
|colspan=8 align=center|'''Chairmen of the (executive committee of the) City Soviet:'''
|-
|
|[[Enver Kapba]]
|1967
|<ref name=lakoba>{{cite web|last=Lakoba|first=Stanislav|authorlink=Stanislav Lakoba|title=Кто есть кто в Абхазии|url=http://apsnyteka.narod2.ru/l/abhaziya_posle_dvuh_imperii_xix-xxi_vv/kto_est_kto_v_abhazii_ukazatel_imen/index.html|accessdate=20 January 2012}}</ref>
|1970
|<ref name=lakoba/>
|
|
|-
|colspan=8 align=center|'''Heads of the City Administration:'''
|-
| 
| [[Ruslan Iazychba]]
|≤1994
|
|≥1997
|
| rowspan=3|[[Vladislav Ardzinba]]
|
|-
| 
|[[Grigori Enik]]
|≤2000
|
| December 2002
|
|First time
|-
|rowspan=2|
|rowspan=2|[[Valeri Bganba]]
|December 2002
|<ref name=uzel36972/>
|12 February 2005
|
|
|-
|12 February 2005
|
|25 May 2006
|<ref name=regnum647250/>
|rowspan=2|[[Sergei Bagapsh]]
|
|-
|rowspan=2|
|rowspan=2|[[Astamur Ketsba]] 
|25 May 2006
|<ref name=regnum647250/>
|29 May 2011
|
|
|-
|29 May 2011
|
|6 September 2011
|<ref name=apress4172/>
|rowspan=3|[[Alexander Ankvab]]
|
|-
|
| ''[[Teimuraz Kapba]]''
| 6 September 2011
|<ref name=apress4172/>
| 15 November 2011
|
| Acting
|-
|rowspan=2|
|rowspan=2|[[Grigori Enik]]
|15 November 2011
| <ref name=apress4721/>
|1 June 2014
|
|Second time
|-
|1 June 2014
|
|22 October 2014
|
|''[[Valeri Bganba]]''
|
|-
|
|[[Beslan Bartsits]]
|22 October 2014
|<ref name=apress13329/>
|16 May 2016
|<ref name="press_beslan"/>
|rowspan=2|[[Raul Khajimba]]
|
|-
|
|[[Zaur Bganba]]
|16 May 2016
|<ref name="press_zaur"/>
|''Present''
|
|
|}

==Demographics==
According to 2003 census, the population of the district included:<ref name=censuses>[http://www.ethno-kavkaz.narod.ru/rnabkhazia.html Population censuses in Abkhazia: 1886, 1926, 1939, 1959, 1970, 1979, 1989, 2003] {{ru icon}}</ref>
*Armenians (44.1%)
*Abkhaz (27.7%)
*Russians (20.0%)
*Georgians (3.3%)
*Greeks (0.7%)

==Settlements==
The district's main settlements are:
*[[Gagra]]
*[[Pitsunda]]
*[[Leselidze (town)|Leselidze]]
*[[Tsandripsh]]
*[[Bzyb (town)|Bzyb]]

The district is mostly mountainous except for the Bzyb lowland in the southern part of it (where Pitsunda is located) and is crossed by several ranges (Gagra, Arabika and others).

==See also==
*[[Administrative divisions of Abkhazia]]

==Notes==

{{Reflist|group="notes"}}

==References==

{{Reflist}}

{{Administrative divisions of Abkhazia}}
{{Districts of Georgia}}

{{coord|43|25|55|N|40|15|53|E|region:GE-AB_type:adm1st_source:kolossus-huwiki|display=title}}

[[Category:Districts of Abkhazia]]
[[Category:Gagra District| ]]