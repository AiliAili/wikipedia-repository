{{about|the mountain in South Australia|other similarly named features|Mount Lofty (disambiguation)}}
{{refimprove|date=May 2013}}
{{Infobox mountain
| name              = Mount Lofty
| other_name        = 
| photo             = 
| photo_size        = 
| photo_alt         = 
| photo_caption     = 
| elevation         = 
| elevation_m       = 727
| elevation_ft      = 
| elevation_ref     = 
| prominence        = 
| prominence_m      = 
| prominence_ft     = 
| prominence_ref    = 
| listing           = 
| range             = [[Mount Lofty Ranges]]
| parent_peak       = 
| location          = [[Cleland, South Australia]], [[Australia]]<ref name=PLB>{{cite web|title=Search result for "Mount Lofty (Mountain)" (Record no. SA0041096) with the following layers selected - "Suburbs and Localities" and " Place names (gazetteer)"  |url=http://maps.sa.gov.au/plb/# |work=Property Location Browser|publisher=Government of South Australia |accessdate=19 July 2016}}</ref>
| map               = South Australia
| map_alt           = 
| map_caption       = 
| map_relief        = 
| map_size          = 
| label             = 
| label_position    = 
| coordinates       = {{coord|34|58|S|138|42|E|region:AU-SA|display=inline,title}}
| grid_ref_UK       = 
| grid_ref_Ireland  = 
| topo              = 
| type              = 
| volcanic_arc/belt = 
| age               = 
| last_eruption     = 
| first_ascent      = April 1831 <br> [[Collet Barker]]
| easiest_route     = 
| child             = 
| embedded          = 
}}
[[File:Eurilla1905-B33591.jpg|thumb|250px|Snow at Eurilla in 1905]]
[[File:Mount lofty from south.jpg|250px|thumb|North view of the Summit and Flinders Column from the Fire Tower]]
[[File:Piccadilly Valley.JPG|thumb|250px|View SE across the [[Piccadilly, South Australia|Piccadilly Valley]] from the Mount Lofty Scenic Route. The summit of [[Mount Barker (South Australia)|Mount Barker]], 22 km away, is visible on the horizon.]]
[[File:Flinders Column at Mount Lofty.JPG|thumb|Flinders Column and viewing platform]]
[[File:Flinders Column dedication plaque.JPG|thumb|Flinders Column dedication plaque, from 1902]]

'''Mount Lofty''' ({{coord|34|58|S|138|42|E|region:AU-SA_type:mountain}}, elevation 727 metres [[Australian Height Datum|AHD]]) is the highest point in the southern [[Mount Lofty Ranges]]. It is located about 15&nbsp;km east of the [[Adelaide]] [[Adelaide city centre|city centre]] in [[South Australia]], and has panoramic views of the city and the Adelaide plains to the west, and of the [[Piccadilly, South Australia|Picadilly Valley]] to the east.

The summit can be accessed by road from the [[South Eastern Freeway]] at [[Crafers, South Australia|Crafers]], and from the eastern suburbs via [[Greenhill Road]] and the Mount Lofty Scenic Route. The more enthusiastic can walk up the gully from [[Waterfall Gully, South Australia|Waterfall Gully]], through the [[Cleland Conservation Park]] and from [[Chambers Gully]]. The track from Waterfall Gully to the summit is a 4&nbsp;km uphill trek and one of Adelaide's most popular exercise circuits so the park at the bottom is often busy. The summit provides panoramic views across Adelaide, and a cafe-restaurant and gift shop. These are relatively new due to protracted disputes over appropriate development following the destruction of the old cafe in the 1983 [[Ash Wednesday fires]].

On the ridge near the summit are three television transmission towers (the northernmost being that of the [[Australian Broadcasting Corporation|ABC]]), and the [[Mount Lofty Fire Tower]] operated by the [[Country Fire Service]].

The summit has become a popular spot for tourists to Adelaide, and also for cyclists coming up the old [[Mount Barker Road]] through [[Eagle on the Hill, South Australia|Eagle on the Hill]]; this former section of National Highway No.1 has been superseded by the [[Heysen Tunnels]].

==History==

===European discovery and use===
Mount Lofty was named by [[Matthew Flinders]] on 23 March 1802 during his circumnavigation of the [[Australia (continent)|Australian continent]].<ref>
{{cite book| last = Flinders| first = Matthew| authorlink = Matthew Flinders| title = A Voyage to Terra Australis : undertaken for the purpose of completing the discovery of that vast country, and prosecuted in the years 1801, 1802, and 1803 in His Majesty's ship the Investigator, and subsequently in the armed vessel Porpoise and Cumberland Schooner; with an account of the shipwreck of the Porpoise, arrival of the Cumberland at Mauritius, and imprisonment of the commander during six years and a half in that island.| url = http://www.gutenberg.org/catalog/world/readfile?pageno=229&fk_files=1486723| accessdate= 4 January 2014| edition= Facsimile| year= 1966| origyear= 1814| publisher= Libraries Board of South Australia| location= Adelaide;  Facsimile reprint of: London : G. and W. Nicol, 1814 ed. In two volumes, with an Atlas (3 volumes)
| page= 251}}</ref>
It was first climbed by a European when the explorer [[Collet Barker]] climbed it in April 1831, almost six years before Adelaide was settled.

A stone cairn at the summit was originally used to mark the [[Triangulation station|trig point]], and in 1885 this was replaced by an obelisk which served as the central reference point for surveying purposes across Adelaide. In 1902 the obelisk was rededicated and renamed as the "Flinders Column".<ref>{{cite book|last1=Smith|first1=Pam|last2=Pate|first2=F. Donald|last3=Martin|first3=Robert|title=Valleys of Stone: The Archaeology and History of Adelaide's Hills Face|date=2006|publisher=Kōpi Books|location=Belair, South Australia|isbn=0 975 7359-6-9|page=232}}</ref>

The Summit was closed to the public during the [[Second World War]], when the obelisk was considered an indispensable [[navigation aid]]. A flashing strobe was fitted to the top to improve visibility at night. This strobe was removed after the war, but then re-installed in the 1990s, when the obelisk was repainted and restored during construction of the new kiosk.

==Historic houses==

Summit Road, Mt Lofty, was previously one of the most well-known addresses in South Australia, with the [[summer house]]s of several prominent families being located there. These were all destroyed or severely damaged by the Ash Wednesday bushfires in 1983, but have subsequently been restored.<ref>{{cite web |author= Wall, Barbara |title= Mount Lofty Summit Road: A survey 1841-2008 (research paper) |year= 2008 |url= http://www.catalog.slsa.sa.gov.au/record=b2231465~S1 }}</ref>  They include:

* Mt Lofty House (1858) - [[Arthur Hardy (businessman)|Arthur Hardy]]<ref>{{cite web | url = http://www.mtloftyhouse.com.au/adelaide-hills-hotel.html | title = Explore Mt Lofty House | publisher = Mt Lofty House - Adelaide Hills - Grand Mercure | accessdate = 6 May 2013 }} [http://www.mtloftyhouse.com.au/explorethearea/history-en.html History]</ref>
* Eurilla (1884) - [[William Milne (politician)|William Milne]], 1917 [[Lavington Bonython]], 1972 [[Kym Bonython]], 1998<ref>{{ cite web |title= Eurilla rises again  |url= http://www.salife.com.au/megazine/bookhtml.aspx?BasePath=/magazine/homes/hills/Eurilla-Rises-Again |work= SA Life Magazine, vol. 5, no. 2 |date=February 2008 |author= Amanda Ward |accessdate=17 June 2012 |pages=32–43 }}<br>{{cite web |author=Keelan, Michael |title= Eurilla : for the love of trees |work=SA Life Magazine, vol. 5, no. 2 |date=February 2008 |pages=44–51 |accessdate=17 June 2012 |url= http://www.salife.com.au/megazine/bookhtml.aspx?BasePath=/magazine/gardens/featuregardens/eurilla-feb08 }}<br>Photos: [http://images.slsa.sa.gov.au/mpcimg/47750/B47676.htm 1890],[http://images.slsa.sa.gov.au/mpcimg/15750/B15676.htm 1890],[http://images.slsa.sa.gov.au/mpcimg/33750/B33591.htm 1905],[http://recordsearch.naa.gov.au/scripts/PhotoSearchItemDetail.asp?M=0&B=11855511&SE=1 1983]. See also: [http://www.catalog.slsa.sa.gov.au:80/record=b2231465~S1],[http://www.burnside.sa.gov.au/webdata/resources/files/snames.doc],[http://portrait.gov.au/UserFiles/file/Portrait25.pdf]</ref>
* Carminow (1885) - [[Thomas Elder]],<ref>{{cite web | author = Fayette Gosse | title = Elder, Sir Thomas (1818–1897) | publisher = Australian Dictionary of Biography, National Centre of Biography, Australian National University | url = http://adb.anu.edu.au/biography/elder-sir-thomas-347/text5319 | accessdate = 6 May 2013}}</ref> 1905 [[Langdon Bonython]]<ref>[http://trove.nla.gov.au/ndp/del/article/5019086 A Trip to Mount Lofty], 31 March 1906, ''The Advertiser'' pg.6</ref>

Other buildings, such as "St. Michael's Monastery" (an Anglican Church retreat) and "Arthur's Seat", for a time known as [[Stawell School]], a private school for girls, were never rebuilt.<ref>Barbara Wall ''A Short History of Stawell School: The forgotten school on Mount Lofty'' published for Mount Lofty Districts Historical Society by Peacock Publications 2012 ISBN 978--1-921601-69-9</ref> Part of this property was excised for the ABC-TV transmitter building and mast.

==Snow==

Due to Adelaide's mild winters, temperatures cold enough to produce snow in the Adelaide metropolitan area never occur, and the nearest snowfields to Adelaide are in eastern Victoria, over 700&nbsp;km away.  However, light snowfalls (rarely lasting for more than a day) are not uncommon on the summit (although it is possible for Mount Lofty to go two or three years without any snowfall.)  This is a huge novelty for the approximately 1 million residents of the Adelaide Plains, (particularly for the children), and a photograph of the event has made the front page of [[The Advertiser (Adelaide)|the local newspaper]] multiple times in the past.<ref>Advertiser coverage of "Snow at Mt Lofty": [http://www.adelaidenow.com.au/news/south-australia/mother-nature-delivers-snow-to-the-adelaide-hills-and-wild-wind-and-rain-to-sa/story-fni6uo1m-1227009593714 1 August 2014]; [http://www.adelaidenow.com.au/news/south-australia/steady-rain-lashes-south-australia-but-warmer-weather-in-sight/story-fni6uo1m-1226682340603 22 July 2013]; [http://www.news.com.au/national/south-australia/snow-at-mt-lofty-more-wild-weather-coming/story-fndo4dzn-1226493307107 11 October 2012].</ref>   Mount Lofty is the coldest location in the Adelaide area; during winter months the temperature may not exceed 3-4&nbsp;°C on some days. The summit is the most common location for snow in South Australia; rare snowfalls sometimes occur in other parts of the Mount Lofty Ranges, and in Northern South Australia.

==See also==
*[[List of mountains in Australia]]

==References==
{{reflist|2}}
==External links==
*[http://www.environment.sa.gov.au/parks/Find_a_Park/Browse_by_region/Adelaide_Hills/mount-lofty-summit Official Cleland Conservation Park webpage ]

[[Image:Mount Lofty View Night.jpg|700px|thumb|left|View of [[Adelaide]] Plains at night from the summit.]]
[[File:Adelaide sunset.jpg|700px|thumb|left|View of the eastern suburbs, the [[Adelaide city centre]] and the [[Gulf St Vincent]] at sunset from the summit.]]

{{Adelaide Hills Council suburbs}}
{{Adelaide Hills}}

{{DEFAULTSORT:Mount Lofty}}
[[Category:Mountains of South Australia|Lofty, Mount]]