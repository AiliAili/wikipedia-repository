{{Refimprove|date=January 2010}}
[[Image:Ivanhoe (opera).jpg|right|thumb|300px|Scenes from the ''[[Illustrated London News]]'' of [[Arthur Sullivan]]'s operatic adaptation of [[Ivanhoe (opera)|Ivanhoe]].]]

The '''Waverley Novels''' is a long series of novels by Sir [[Walter Scott]] (1771–1832). For nearly a century, they were among the most popular and widely read novels in all of Europe.

Because Scott did not publicly acknowledge authorship until 1827, the series takes its name from ''[[Waverley (novel)|Waverley]]'', the first novel of the series released in 1814. The later books bore the words "by the author of [[Waverley (novel)|''Waverley'']]" on their title pages.

The ''[[Tales of my Landlord]]'' sub-series was not advertised as "by the author of ''Waverley''" and thus is not always included as part of the Waverley Novels series.

==Order of publication==
{|class="wikitable sortable"
!Title !! Published !! Main setting !! Period
|-
|''[[Waverley (novel)|Waverley]]'', or, ''Tis Sixty Years Since'' || 1814 || [[Perthshire]] (Scotland)|| 1745–1746
|-
|''[[Guy Mannering]]'', or, ''The Astrologer'' || 1815 || [[Galloway]] (Scotland)|| 1760-5, 1781–2
|-
|''[[The Antiquary]]'' || 1816 || [[Angus, Scotland|Angus]] (Scotland)|| 1790s
|-
|colspan=4|''Tales of My Landlord'', 1st series:
|-
|&nbsp;&nbsp;&nbsp;''[[The Black Dwarf (novel)|The Black Dwarf]]'' || 1816 || [[Scottish Borders]] || 1707
|-
|&nbsp;&nbsp;&nbsp;''[[The Tale of Old Mortality]]'' || 1816 || Southern Scotland || 1679–89
|-
|''[[Rob Roy (novel)|Rob Roy]]'' || 1818 || [[Northumberland]] (England), and the environs of [[Loch Lomond]] (Scotland) || 1715–16
|-
|colspan=4|''Tales of My Landlord'', 2nd series:
|-
|&nbsp;&nbsp;&nbsp;''[[The Heart of Midlothian]]'' || 1818 || [[Edinburgh]] and [[Richmond, London]] || 1736
|-
|colspan=4|''Tales of My Landlord'', 3rd series:
|-
|&nbsp;&nbsp;&nbsp;''[[The Bride of Lammermoor]]'' || 1819 || [[East Lothian]] (Scotland) || 1709–11
|-
|&nbsp;&nbsp;&nbsp;''[[A Legend of Montrose]]'' || 1819 || [[Scottish Highlands]] || 1644-5
|-
|''[[Ivanhoe]]'' || 1819 || [[Yorkshire]], [[Nottinghamshire]] and [[Leicestershire]] (England)|| 1194
|-
|''[[The Monastery]]'' || 1820 || [[Scottish Borders]] || 1547–57
|-
|''[[The Abbot]]'' || 1820 || Various in Scotland || 1567-8
|-
|''[[Kenilworth (novel)|Kenilworth]]'' || 1821 || [[Berkshire]] and [[Warwickshire]] (England) || 1575
|-
|''[[The Pirate (novel)|The Pirate]]'' || 1822 || [[Shetland]] and [[Orkney]] || 1690s
|-
|''[[The Fortunes of Nigel]]'' || 1822 || [[London]] and [[Greenwich]] (England)|| 1616–18
|-
|''[[Peveril of the Peak]]'' || 1822 || [[Derbyshire]], the [[Isle of Man]], and [[London]] || 1658–80
|-
|''[[Quentin Durward]]'' || 1823 || [[Tours]] and [[Péronne, Somme|Péronne]] (France)<br>[[Liège]] ([[Wallonia]]/Belgium) || 1468
|-
|''[[St. Ronan's Well]]'' || 1824 || Southern Scotland || 19th century
|-
|''[[Redgauntlet]]'' || 1824 || Southern Scotland, and [[Cumberland]] (England)|| 1766
|-
|colspan=4|''Tales of the Crusaders'':
|-
|&nbsp;&nbsp;&nbsp;''[[The Betrothed (1825)|The Betrothed]]'' || 1825 || Wales, and [[Gloucester]] (England)|| 1187–92
|-
|&nbsp;&nbsp;&nbsp;''[[The Talisman (Scott novel)|The Talisman]]'' || 1825 || [[Syria]] || 1191
|-
|''[[Woodstock (novel)|Woodstock]]'', or, ''The Cavalier'' || 1826 || [[Woodstock, Oxfordshire|Woodstock]] and [[Windsor, Berkshire|Windsor]] (England)<br>[[Brussels]], in the [[Spanish Netherlands]] || 1652
|-
|colspan=4|''Chronicles of the Canongate'', 2nd series:<ref>The first series of ''Chronicles of the Canongate'' comprised several short stories.</ref>
|-
|&nbsp;&nbsp;&nbsp;''St Valentine's Day'', or, ''[[The Fair Maid of Perth]]'' || 1828 || [[Perthshire]] (Scotland) || 1396
|-
|''[[Anne of Geierstein]]'', or, ''The Maiden in the Mist'' || 1829 || Switzerland and Eastern France || 1474–77
|-
|colspan=4|''Tales of my Landlord'', 4th series:
|-
|&nbsp;&nbsp;&nbsp;''[[Count Robert of Paris]]'' || 1831 || [[Constantinople]] and [[Üsküdar|Scutari]] (now in [[Turkey]]) || 1097
|-
|&nbsp;&nbsp;&nbsp;''[[Castle Dangerous]]'' || 1831 || [[Kirkcudbrightshire]] (Scotland) || 1307
|-
|''[[The Siege of Malta (novel)|The Siege of Malta]]'' || 2008 || [[Malta]] and Southern Spain || 1565
|}

==Chronological order, by setting==
*1097: ''Count Robert of Paris''
*1187–94: ''The Betrothed, The Talisman, Ivanhoe''
*1307: ''Castle Dangerous''
*1396: ''The Fair Maid of Perth''
*1468–77: ''Quentin Durward, Anne of Geierstein''
*1547–75: ''The Monastery, The Abbot, Kenilworth, The Siege of Malta''
*1616–18: ''The Fortunes of Nigel''
*1644–89: ''A Legend of Montrose, Woodstock, Peveril of the Peak, The Tale of Old Mortality, The Pirate''
*1700–99: ''The Black Dwarf, The Bride of Lammermoor, Rob Roy, Heart of Midlothian, Waverley, Guy Mannering, Redgauntlet, The Antiquary''
*19th century: ''St. Ronan's Well''

==Editions==
Originally printed by James Ballantyne on the Canongate in Edinburgh, brother of one of Scott's close friends, John Ballantyne ("Printed by James Ballantyne and Co. For Archibald Constable and Co., Edinburgh"). Some of the early editions were lavishly illustrated by [[George Cattermole]].

The two definitive editions are the 48-volume set published between 1829 and 1833 by [[Robert Cadell]] (the "Magnum Opus"), based on previous editions, with new introductions and appendices by Scott, and the 30-volume set, based on manuscripts, published by the Edinburgh University Press and Columbia University Press in the 1990s.

==Placenames==
[[File:Edinburgh-scottm.600px.jpg|right|thumb|View from the [[Scott Monument]] of the [[Edinburgh Waverley railway station|Waverley Station]] roof, in [[Edinburgh]], with [[Arthur's Seat]] in the background]]

The towns of [[Waverly, Nebraska]]; [[Waverly, Tioga County, New York|Waverley, New York]]; [[Waverley, Nova Scotia]]; [[Waverly, Ohio]]; and [[Waverly, Tennessee]],<ref name="waverly.net">{{cite web |url=http://www.waverly.net/hcchamber/history.htm |title=History of Humphreys County Tennessee |work=Humphreys County Chamber of Commerce |archiveurl=https://web.archive.org/web/20070516154432/http://www.waverly.net/hcchamber/history.htm |archivedate=May 16, 2007}}</ref> take their names from these novels, as does [[Edinburgh Waverley railway station|Waverley Station]] and [[Waverley Bridge]] in [[Edinburgh]]. Waverley School in Louisville, Kentucky, which later became the [[Waverly Hills Sanatorium]], was named after the novels as well.<ref>http://freepages.history.rootsweb.ancestry.com/~waverlymemorial/newspapers/waverly_herald/Misc/how_waverly_got_its_name_wh_p9.jpg</ref>

==Other uses of names==
Many [[Naming of British railway rolling stock|British railway locomotives]] were given names from the novels.

Over two thousand streets in Britain have names from titles of individual novels, with 650 from ''Waverley'' alone.

==See also==
{{portal|Novels}}
* ''[[Tales of my Landlord]]''
* ''[[Tales of the Crusaders]]''
* ''[[Chronicles of the Canongate]]''

==Notes and references==
<references/>

==External links==
* A typically enthusiastic [http://www.oldandsold.com/articles28/books-4.shtml essay] on the Waverley Novels, published in 1912

{{Walter Scott}}

[[Category:Walter Scott novel series]]