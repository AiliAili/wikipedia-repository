{{about|the emotional steel inverted roller coasters|the 4D spin roller coaster| Batman: The Ride (Six Flags Fiesta Texas)}}
{{Distinguish|Batman Adventure – The Ride|Batman The Escape}}
{{Good article}}
{{Use mdy dates|date=January 2013}}
{{Infobox roller coaster
| name                 = Batman: The Ride
| logo                 = <!--Use ONLY the filename, not a full [[File:]] link-->
| logodimensions       = <!--Must be expressed in pixels (px); default is 250px-->
| image                = Batman signage.JPG
| imagedimensions      = 250px
| caption              = Entrance to Batman: The Ride at Six Flags Magic Mountain
| previousnames        = 
| location             = Six Flags Great America
| section              = Yankee Harbor
| subsection           = <!--Should be linked.-->
| coordinates          = {{Coord|42|22|5.79|N|87|56|1.42|W||display=inline}}
| status               = Operating
| opened               = {{Start date and age|1992|5|9}}
| soft_opened          = {{Start date and age|1992|5|2}}<ref name=ACE />
| year                 = 1992
| closed               = <!--Use {{End date|YYYY|MM|DD}} -->
| cost                 = 
| previousattraction   = 
| replacement          = 
| location_website   = {{Official website|https://www.sixflags.com/greatamerica/attractions/batman-ride}}
| extend               = {{Infobox roller coaster extend
| location           = Six Flags Great Adventure
| coordinates        = {{Coord|40|8|8.73|N|74|26|38.05|W|display=inline}}
| section            = Movie Town
| subsection         = <!--Should be linked.-->
| status             = Operating
| opened             = {{Start date and age|1993|5|1}}
| year               = 1993
| closed             = <!--Use {{End date}} -->
| cost               = 
| previousattraction = 
| replacement        = 
| website            = {{Official website|https://www.sixflags.com/greatadventure/attractions/batman-ride}}
}}{{Infobox roller coaster extend
| location           = Six Flags Magic Mountain
| coordinates        = {{Coord|34.425854|-118.600778|display=inline}}
| section            = DC Universe
| subsection         = <!--Should be linked.-->
| status             = Operating
| opened             = {{Start date and age|1994|3|26}}
| year               = 1994
| closed             = <!--Use {{End date}} -->
| cost               = 
| previousattraction = 
| replacement        = 
| website            = {{Official website|https://www.sixflags.com/magicmountain/attractions/batman-ride}}
}}{{Infobox roller coaster extend
| location           = Six Flags St. Louis
| coordinates        = {{Coord|38.513535|-90.673207|display=inline}}
| section            = Studio Backlot
| subsection         = <!--Should be linked.-->
| status             = Operating
| opened             = {{Start date and age|1995|4|22}}
| year               = 1995
| closed             = <!--Use {{End date}} -->
| cost               = 
| previousattraction = 
| replacement        = 
| website            = {{Official website|https://www.sixflags.com/stlouis/attractions/batman-ride}}
}}{{Infobox roller coaster extend
| location           = Six Flags Over Georgia
| coordinates        = {{Coord|33.769495|-84.546366|display=inline}}
| section            = Gotham City
| subsection         = <!--Should be linked.-->
| status             = Operating
| opened             = {{Start date and age|1997|5|3}}
| year               = 1997
| closed             = <!--Use {{End date}} -->
| cost               = 
| previousattraction = 
| replacement        = 
| website            = {{Official website|https://www.sixflags.com/overgeorgia/attractions/batman-the-ride}}
}}{{Infobox roller coaster extend
| location           = Six Flags Over Texas
| coordinates        = {{Coord|32|45|30.31|N|97|3|59.93|W|display=inline}}
| section            = Gotham City
| subsection         = <!--Should be linked.-->
| status             = Operating
| opened             = {{Start date and age|1999|5|26}}
| year               = 1999
| closed             = <!--Use {{End date}} -->
| cost               = 
| previousattraction = 
| replacement        = 
| website            = {{Official website|https://www.sixflags.com/fiestatexas/attractions/batman-the-ride/overview}}
}}{{Infobox roller coaster extend
| location           = Six Flags New Orleans
| coordinates        = {{Coord|30.049732| -89.934758|display=inline}}
| section            = DC Comics Super Heroes Adventures
| subsection         = <!--Should be linked.-->
| status             = Relocated to Six Flags Fiesta Texas where it operates as [[Goliath (Six Flags Fiesta Texas)|Goliath]]
| opened             = {{Start date and age|2003|04|12}}
| year               = 2003
| closed             = {{End date|2005|08}}
| cost               = 
| previousattraction = 
| replacement        = 
}}
| type                 = Steel
| type2                = Inverted
| type3                = <!--Must not be linked, will auto-categorize the coaster.-->
| manufacturer         = Bolliger & Mabillard
| designer             = [[Werner Stengel]]
| model                = Inverted Coaster &ndash; Batman
| track                = 
| lift                 = [[Chain lift hill]]
| inversions           = 5
| duration             = 1:45
| angle                = <!--Do not include "degrees", it is added automatically.-->
| capacity             = 1280&ndash;1400<ref name=SFGA /><ref>{{Parkz|ride_name=Batman The Ride|location=Six Flags Magic Mountain|parkz_number=302|accessdate=January 6, 2013}}</ref>
| gforce               = 4
| restriction_in       = 54
| trains               = 2
| carspertrain         = 8
| rowspercar           = 1
| ridersperrow         = 4
| restraint           = Over-the-shoulder
| virtual_queue_name   = [[Flash Pass]]
| virtual_queue_image  = Fastpass availability icon.svg
| virtual_queue_status = available
| single_rider         = <!--Must be "available" if available.-->
| accessible           = <!--Must be "available" if available.-->
| transfer_accessible  = <!--Must be "available" if available.-->
| custom_label_1       = Height
| custom_value_1       = {{Convert|100|or|105|ft|m|abbr=on}}
| custom_label_2       = Length
| custom_value_2       = {{Convert|2693|or|2700|ft|m|abbr=on}} 
| custom_label_3       = Speed
| custom_value_3       = {{Convert|50|mph|km/h|abbr=on}}
| custom_label_4       = Drop
| custom_value_4       = 84.5 ft<ref>{{cite web|title=Batman: The Ride|url=http://beamer3k.deviantart.com/art/Batman-The-Ride-ACE-Coaster-Landmark-Plaque-441037350|website=Beamer3K}}</ref>
| rcdb_number          = 8310
}}

'''Batman: The Ride''' is a [[steel roller coaster|steel]] [[inverted roller coaster|inverted]] [[roller coaster]] based thematically off of the 1989 film [[Batman]] and found at seven [[Six Flags]] [[theme parks]] in the [[United States]]. Built by consulting engineers [[Bolliger & Mabillard]], it rises to a height of between {{Convert|100|and|105|ft}} and reaches top speeds of {{Convert|50|mph}}. The original roller coaster at [[Six Flags Great America]] was partially devised by the park's [[general manager]] Jim Wintrode. Batman: The Ride was the world's first [[inverted roller coaster]] when it opened in 1992,<ref>{{cite web|url=http://www.themeparkinsider.com/flume/201405/4014/|title=Coaster Tech: An Insider's look at inverted coasters
|last=Meyer|first=Russell|work=themeparkinsider.com|date=May 12, 2014|accessdate=May 21, 2015}}</ref> and has since been awarded [[ACE Coaster Landmark|Coaster Landmark]] status by the [[American Coaster Enthusiasts]]. Clones of the ride exist at [[amusement park]]s around the world.

==History==
The concept of an inverted roller coaster with [[Roller coaster inversion|inversions]] was developed by Jim Wintrode, the [[general manager]] of [[Six Flags Great America]], in the 1990s.<ref name=ACE>{{cite web|title=Coaster Landmark Award - Batman: The Ride|url=http://www.aceonline.org/CoasterAwards/details.aspx?id=45|publisher=American Coaster Enthusiasts|accessdate=January 6, 2013|date=June 20, 2005}}</ref><ref name="In my office: Jim Wintrode">{{cite journal|last=O'Brien|first=Tim|title=In my office: Jim Wintrode|journal=Amusement Business|date=March 24, 2003|volume=115|issue=12|accessdate=January 6, 2013}}</ref> To develop the idea, Wintrode worked with Walter Bolliger and Claude Mabillard &ndash; from Swiss roller coaster manufacturer Bolliger & Mabillard &ndash; and engineer Robert Mampe.<ref name=ACE /> The ride [[Soft opening|soft opened]] to the public on May 2, 1992, with an official opening one week later.<ref name=ACE /><ref name=SFGAm>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags Great America|rcdb_number=5|accessdate=January 6, 2013}}</ref> Although the full cost of the ride was never disclosed, it was the single biggest investment made by Six Flags Great America on one attraction.<ref name="New attraction takes Six Flags into world of virtual reality">{{cite journal|last=Muret|first=Don|title=New attraction takes Six Flags into world of virtual reality|journal=Amusement Business|date=March 1994|volume=106|issue=10|accessdate=January 6, 2013}}</ref>

Clones of the original ride, at Six Flags Great America in [[Gurnee, Illinois]], became a staple at Six Flags theme parks around the world, and were opened in [[Six Flags Great Adventure]] (1993), [[Six Flags Magic Mountain]] (1994), and [[Six Flags St. Louis]] (1995).<ref name=SFGA>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags Great Adventure|rcdb_number=32|accessdate=January 6, 2013}}</ref><ref name=SFMM>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags Magic Mountain|rcdb_number=24|accessdate=January 6, 2013}}</ref><ref name=SFSL>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags St. Louis|rcdb_number=43|accessdate=January 6, 2013}}</ref> Installation of the ride followed at [[Six Flags Over Georgia]] (1997), and [[Six Flags Over Texas]] (1999).<ref name=SFOG>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags Over Georgia|rcdb_number=421|accessdate=January 6, 2013}}</ref><ref name=SFOT>{{cite RCDB|coaster_name=Batman The Ride|location=Six Flags Over Texas|rcdb_number=552|accessdate=January 6, 2013}}</ref>

In 2002 [[La Ronde (amusement park)|La Ronde]] [[amusement park]], [[Montreal]], opened a clone of the ride under the name [[Le Vampire (roller coaster)|Le Vampire]] (The Vampire), and [[Warner Bros. Movie World Madrid]], under the name [[Batman: La Fuga]] (Batman: The Escape).<ref name="Vampire LR">{{cite RCDB|coaster_name=Vampire|location=La Ronde|rcdb_number=1567|accessdate=January 6, 2013}}</ref><ref name="La Fuga WBMWM">{{cite RCDB|coaster_name=Batman la Fuga|location=Parque Warner Madrid|rcdb_number=1365|accessdate=January 6, 2013}}</ref> As La Ronde is not a branded Six Flags park, the licensing agreement with [[Warner Bros.]] and [[DC Comics]] for the name Batman: The Ride was not allowed.<ref>{{cite journal|date=March 3, 2003|title=Montreal-based theme park|journal=Amusement Business|publisher=BPI Communications, Inc.|volume=115|issue=9|page=6|issn=0003-2344}}</ref> The La Ronde ride was expected to become Batman: The Ride when the park was scheduled to be converted to a Six Flags-branded park in the mid-2000s, however, these changes were never initiated.<ref name="New Coasters Ready To Roll">{{cite journal|last=O'Brien|first=Tim|title=New Coasters Ready To Roll|journal=Amusement Business|date=May 13, 2002|volume=114|issue=19|pages=23–25}}</ref>

Other non-Six Flags parks that installed Batman: The Ride clones are [[Diavlo]] at [[Himeji Central Park]] in [[Japan]] (1994),<ref>{{cite RCDB|coaster_name=Diavlo|location=Himeji Central Park|rcdb_number=1208|accessdate=January 6, 2013}}</ref> [[The Great White (SeaWorld San Antonio)|The Great White]] at [[SeaWorld San Antonio|SeaWorld]] in [[San Antonio, Texas]] (1997),<ref>{{cite RCDB|coaster_name=Great White|location=SeaWorld San Antonio|rcdb_number=278|accessdate=January 6, 2013}}</ref> and [[Lightning (Entertainment City)|Lightning]] at [[Entertainment City]] in [[Kuwait]] (2004).<ref>{{cite RCDB|coaster_name=Lightning|location=Kuwait Entertainment City|rcdb_number=2872|accessdate=January 6, 2013}}</ref>

One of the last installations of the ride was at [[Six Flags New Orleans]] in 2003,<ref name=SFNO>{{cite RCDB|coaster_name=Batman: The Ride|location=Six Flags New Orleans|rcdb_number=1886|accessdate=January 6, 2013}}</ref> having been relocated from the Japanese park Thrill Valley where it operated as [[Gambit (roller coaster)|Gambit]] from 1995 to 2002.<ref name=TV>{{cite RCDB|coaster_name=Gambit|location=Thrill Valley|rcdb_number=1521|accessdate=January 6, 2013}}</ref> In 2005 [[Effects of Hurricane Katrina in New Orleans|the effects of]] [[Hurricane Katrina]] caused Six Flags to abandon its [[New Orleans]] park,<ref>{{cite web|title=New Orleans: Six Flags New Orleans|url=http://www.sixflags.com/national/alert/neworleans.aspx|publisher=Six Flags|accessdate=November 13, 2010|archiveurl=https://web.archive.org/web/20070327195321/http://www.sixflags.com/national/alert/neworleans.aspx|archivedate=March 27, 2007}}</ref> and after [[standing but not operating]] for two years, the ride there was relocated to [[Six Flags Fiesta Texas]], where it was refurbished and repainted. The ride reopened as [[Goliath (Six Flags Fiesta Texas)|Goliath]] on April 18, 2008.<ref name="Six Flags' Flashback coaster will live on (somewhere else)">{{cite web|url=http://www.kvue.com/news/state/171544501.html |title=Six Flags' Flashback coaster will live on (somewhere else) |publisher=kvue.com |date=September 27, 2012 |accessdate=January 6, 2013 |deadurl=yes |archiveurl=http://www.webcitation.org/6DrzvSg4V?url=http%3A%2F%2Fwww.kvue.com%2Fnews%2Fstate%2F171544501.html |archivedate=January 22, 2013 |df=mdy }}</ref><ref name="Goliath SFFT">{{cite RCDB|coaster_name=Goliath|location=Six Flags Fiesta Texas|rcdb_number=3976|accessdate=January 6, 2013}}</ref>

On February 21, 2013, Six Flags Great America announced that their Batman: The Ride roller coaster would run backwards for a limited time during the 2013 season.<ref name=Backwards>{{cite press release|title=Six Flags Great America Announces BATMAN: The Ride Backwards|url=http://www.sixflags.com/greatAmerica/info/news_2013BatmanBackwards.aspx|publisher=Six Flags Great America|accessdate=February 21, 2013|date=February 21, 2013}}</ref><ref name="Season of backwards">{{cite web|url=http://www.sixflags.com/greatAmerica/info/news_SeasonOfBackwards.aspx|title=Six Flags Announces a Full Season of Backwards|publisher=Six Flags |date= May 24, 2013 |accessdate=July 2, 2013}}</ref> Six Flags Magic Mountain and Six Flags Over Texas did the same during the 2014 season.<ref>{{cite web|title=New for 2014 |url=http://content.sixflags.com/comingin2014/ |date=August 29, 2013 |accessdate=August 29, 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130901054320/http://content.sixflags.com:80/comingin2014/ |archivedate=September 1, 2013 |df=mdy }}</ref><ref>{{cite web|title=SFOT BB|url=https://www.sixflags.com/overtexas/newsroom/batman-the-ride-backwards|date=March 25, 2014|accessdate=March 26, 2014}}</ref> [[Six Flags Over Georgia]] and [[Six Flags Great Adventure]] followed suit running theirs backwards for a limited time in 2015.<ref>{{cite news|url=http://www.myfoxdc.com/story/27947234/batman-ride-at-six-flags-over-georgia-to-run-backward |title=Batman ride at Six Flags Over Georgia to run backward |date=January 27, 2015 |publisher=Associated Press |accessdate=August 3, 2015 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref><ref>{{cite news|url=http://www.nydailynews.com/new-york/flags-great-adventure-batman-ride-article-1.2160071|title=SEE IT: Batman ride at Six Flags Great Adventure to run backward for the first time|last=Silverstein|first=Jason|date=March 24, 2015|work=New York Daily News|accessdate=July 30, 2015}}</ref>
==Movies That Rides Opened For==
[[Six Flags Great America]] with [[Batman Returns]]

[[Six Flags St. Louis]] with [[Batman Forever]]

[[Six Flags Over Georgia]] with [[Batman and Robin]]

==Characteristics==
[[File:Batman station.JPG|thumb|left|A train waiting to depart the station at Six Flags Magic Mountain]]

===Structure===
The original installation of the ride at Six Flags Great America featured a maximum height of {{Convert|100|ft}} while the installations to follow reached {{Convert|105|ft}}. Each installation of Batman: The Ride has a track length of approximately {{Convert|2700|ft}}. The rides reach a top speed of {{Convert|50|mph}} and exert up to four times [[G-force|the force of gravity]].<ref name=SFGA /><ref name=SFGAm /><ref name=SFMM /><ref name=SFSL /><ref name=SFOG /><ref name=SFOT /><ref name=SFNO />

Batman: The Ride clones operate with two steel and fiberglass trains, each containing eight cars. Each car seats four riders in a single row for a total of 32 riders per train.<ref name=SFGA /><ref name=SFGAm /><ref name=SFMM /><ref name=SFSL /><ref name=SFOG /><ref name=SFOT /><ref name=SFNO />
[[File:Batman The Ride at Six Flags Great America 5.jpg|thumb|left|Theming around the ride at Six Flags Great America]]

===Ride layout===
[[File:Batman The Ride at Six Flags Great America 1.jpg|thumb|A vertical loop and [[Roller coaster elements#Zero-gravity roll|zero-G roll]] on the ride at Six Flags Great America]]
The ride's layout was specifically designed to fit in the [[Six Flags Great America#Yankee Harbor|Yankee Harbor]] themed area at Six Flags Great America, although the layout for each successive attraction is identical or a mirror image of the original.<ref name="Model installation list">{{cite RCDB|coaster_name=Roller Coaster Search Results|location=Model = Batman|rcdb_number=http://rcdb.com/r.htm?ot=2&mo=8310&page=1&order=10|accessdate=January 6, 2013}}</ref>

Batman: The Ride begins with the track floor descending. The [[Train (roller coaster)|train]] moves out of the [[Station (roller coaster)|station]] and up a [[chain lift hill]]. At the top of the hill the train dips down through a Bolliger & Mabillard pre-drop, coasts down a 190-degree swoop to the left, and drops into the first 360-degree [[vertical loop]]. It then flips through a [[Roller coaster elements#Zero-gravity roll|zero-G roll]], followed by another vertical loop. The train then travels upward around a tight spiral to the left, then through a wider turn to the right, drops slightly, and quickly turns through the first [[Roller coaster elements#Corkscrew|corkscrew]] (referred to as a "flatspin" by the manufacturer). Following this is a tight right turn and another flatspin, then a tight left turnaround before the train enters the final [[brake run]].<ref name=SFGA /><ref name=SFGAm /><ref name=SFMM /><ref name=SFSL /><ref name=SFOG /><ref name=SFOT /><ref name=SFNO /><ref name="POV SFOG">{{cite web|last=Alvey|first=Robb|authorlink=Robb Alvey|title=Batman The Ride POV Roller Coaster Front Seat Onride Six Flags Over Georgia|url=https://www.youtube.com/watch?v=RVTnuRJ96jA|work=[[Theme Park Review]]|publisher=YouTube|accessdate=January 6, 2013}}</ref>

===Color scheme===
While some Batman: The Ride clones opened with dark blue track and supports, others featured gray and yellow. Over the years there have been modifications in Batman: The Ride color schemes, with more incorporating yellows, blues, and purples. The original ride at Six Flags Great America retained the original black color scheme until 2004, when the track was painted yellow, and supports dark purple.<ref name=SFGAm /> Six Flags Great Adventure's originally featured a black color scheme with yellow rails until 2004, when the track was repainted yellow.<ref name=SFGA /> For the 2010 season, the clone at Six Flags Magic Mountain was repainted medium blue with black supports.<ref>{{cite web|title=Batman Blue Track (HD POV) Six Flags Magic Mountain|url=https://www.youtube.com/watch?v=QU_6kcZXfbo|work=The Coaster Views|publisher=YouTube|accessdate=21 May 2013}}</ref>
{{clear left}}

===Experience===
Six Flags designers' decorative theme attempts to capture the spirit of [[Batman]]'s world for those queuing to board the ride. As the queue moves through [[Gotham City]] Park,<ref name="Holy Thrills! Six Flags Over Texas Gets Batman Ride">{{cite journal|last=O'Brien|first=Tim|title=Holy Thrills! Six Flags Over Texas Gets Batman Ride|journal=Amusement Business|date=November 16, 1998|volume=110|issue=46|pages=3, 48|accessdate=January 6, 2013}}</ref> the theme becomes more ominous.<ref name="Ultimate Rollercoaster" /> Modeled after [[Anton Furst]]'s award-winning set design for the original ''[[Batman (1989 film)|Batman]]'' film, the atmosphere indicates a crime-ridden and dirty environment, with wrecked cars, discarded pieces of equipment, crumbling concrete, and a Gotham City Police car riddled with bullet holes.<ref name="Ultimate Rollercoaster">{{cite web|title=Batman The Ride|url=http://www.ultimaterollercoaster.com/coasters/reviews/batman/|publisher=Ultimate Rollercoaster|accessdate=April 26, 2013}}</ref> The queue then enters the ride structure.<ref name="Ultimate Rollercoaster" /> The ride passenger loading area is modeled on Batman's [[Batcave]], and features a replica of the Batsuit from the 1989 film.<ref name="Holy Thrills! Six Flags Over Texas Gets Batman Ride" />

==Incidents==
[[File:Batman The Ride at Six Flags Great Adventure 06.jpg|thumb|An overview of the ride's first drop and vertical loop at Six Flags Great Adventure]]
{{see also|Incidents at Six Flags parks}}
On May 26, 2002, a 58-year-old park employee working in the roller coaster's restricted area at Six Flags Over Georgia was killed after being struck in the head by the dangling leg of a 14-year-old girl riding in the front. The girl was hospitalized with a leg injury.<ref name="Six Flags' worker is killed in inverted coaster's path">{{cite news|title=Six Flags' worker is killed in inverted coaster's path|url=https://news.google.com/newspapers?id=eFhPAAAAIBAJ&sjid=2AMEAAAAIBAJ&pg=5299%2C4021952|accessdate=January 6, 2013|newspaper=[[Toledo Blade]]|date=May 28, 2002}}</ref>

On June 28, 2008, a 17-year-old South Carolina teenager was [[decapitation|decapitated]] after being struck by the Batman roller coaster at Six Flags Over Georgia. The teen, who was on a trip to the park with his church's youth group, scaled two fences with a friend into a restricted area and walked into the ride's path. Although witnesses stated he was trying to retrieve his hat, a Cobb County police spokesman reported the teens were attempting to take a shortcut into the park.<ref>{{cite web|author= |url=http://www.cbsnews.com/stories/2008/06/28/national/main4216930.shtml |title=Teen Decapitated in Six Flags Accident |publisher=CBS News |date=June 28, 2008 |accessdate=January 1, 2011}}</ref><ref>{{cite web|last=Cook |first=Rhonda |url=http://www.ajc.com/fayette/content/metro/cobb/stories/2008/06/28/six_flags_death.html |title=Boy Decapitated by Roller Coaster at Six Flags over Georgia is ID'd |publisher=Atlanta Journal-Constitution |date=June 28, 2008 |accessdate=January 1, 2011|archiveurl=https://web.archive.org/web/20080730063724/http://www.ajc.com/fayette/content/metro/cobb/stories/2008/06/28/six_flags_death.html|archivedate=July 30, 2008}}</ref>

==Reception==
Batman: The Ride has generally received positive reviews. ''[[The Dallas Morning News]]'' stated the ride "is proof that new thrills on the cutting edge of technology generate excitement". They also praise the theme of the "smooth-riding coaster" stating "the mysterious crime-fighter is a proven crowd-pleaser".<ref>{{cite news|title=Wild ride - Six Flags should keep thrills coming|accessdate=April 26, 2013|newspaper=[[The Dallas Morning News]]|date=May 28, 1999|publisher=[[A. H. Belo Corporation]]}}</ref> [[American Coaster Enthusiasts]] have also praised the ride, awarding it [[ACE Coaster Landmark|Coaster Landmark]] status in 2005. They describe the ride as a "revolutionary design" which offers "unprecedented intensity, while maintaining remarkable smoothness, comfort, and pacing".<ref name=ACE /><ref name="ACE2">{{cite web|url=http://www.aceonline.org/CoasterAwards/?type=3 |title=ACE Coaster Landmark Awards |publisher=American Coaster Enthusiasts |date= |accessdate=January 1, 2011| archiveurl= https://web.archive.org/web/20101212021831/http://aceonline.org/CoasterAwards/?type=3| archivedate=December 12, 2010}}</ref> Ultimate Rollercoaster describes Batman: The Ride as "the ride of your life". They state "the sensation created by an inverted coaster is very different from that of traditional roller coasters. It is a sensation that every coaster fan must experience".<ref name="Ultimate Rollercoaster" />

===Awards===

In ''[[Amusement Today]]''{{'}}s [[Golden Ticket Awards]] for Best Steel Roller Coasters, Batman: The Ride ranked reasonably in the late 1990s before dropping off the poll, and returning once in 2005. The original installation at Six Flags Great America was ranked 23 and 25 in 1998 and 1999, respectively, before returning in 2005 at position 45. In 1998, the Six Flags Great Adventure and Six Flags St. Louis installations ranked 19 and 21, respectively.<ref>{{cite journal|title=Top 25 steel roller coasters |url=http://www.goldenticketawards.com/issuearchive/1998gta/1998gta.pdf |journal=[[Amusement Today]] |date=September 1998 |deadurl=yes |archiveurl=https://web.archive.org/web/20131019222136/http://www.goldenticketawards.com/issuearchive/1998gta/1998gta.pdf |archivedate=October 19, 2013 |df=mdy }}</ref><ref>{{cite journal|title=Top 25 steel roller coasters|url=http://www.goldenticketawards.com/issuearchive/1999gta/1999gta.pdf|journal=[[Amusement Today]]|date=September 1999}}</ref><ref>{{cite journal|title=Top 50 steel roller coasters|url=http://www.goldenticketawards.com/issuearchive/2005gta/2005gta.pdf|journal=[[Amusement Today]]|date=September 2005}}</ref>

In Mitch Hawker's worldwide Best Roller Coaster Poll, Batman: The Ride peaked at position 21 in 1999 (the first year of the poll).<ref name="Poll 1999" /> The ride's ranking dropped in subsequent polls and is summarised in the table below.

{| class="wikitable"
!  style="text-align:center; background:white;" colspan="500"|Mitch Hawker's Best Roller Coaster Poll: Best Steel-Tracked Roller Coaster
|- style="background:#white;"
! style="text-align:center;"|Year !!1999!!2000!!2001!!2002!!2003!!2004!!2005!!2006!!2007!!2008!!2009!!2010!!2011!!2012
|-
!  style="text-align:center; background:#white;"|Ranking
|<center>21<ref name="Poll 1999">{{cite web|url=http://www.ushsho.com/scprst99.txt |title=FINAL RESULTS: 1999 Internet Steel Tracked Roller Coaster Poll|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center><ref group="nb" name=nopoll>No steel roller coaster poll was held in 2000 or 2011.</ref></center>
|<center>30<ref>{{cite web|url=http://www.ushsho.com/scprs01.htm |title=FINAL RESULTS: 2001 Internet Steel Tracked Roller Coaster Poll|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>39<ref>{{cite web|url=http://www.ushsho.com/scprs02.htm |title=FINAL RESULTS: 2002 Internet Steel Tracked Roller Coaster Poll|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>35<ref>{{cite web|url=http://www.ushsho.com/scprs03.htm |title=FINAL RESULTS: 2003 Internet Steel Tracked Roller Coaster Poll|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>40<ref>{{cite web|url=http://www.ushsho.com/steelrollercoasterpollresults2004.htm |title=FINAL RESULTS: 2004 Internet Steel Tracked Roller Coaster Poll|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>39<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2005.htm |title=Detailed Steel Roller Coaster Poll Results 2005|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>59<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2006.htm |title=Detailed Steel Roller Coaster Poll Results 2006|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>49<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2007.htm |title=Detailed Steel Roller Coaster Poll Results 2007|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>56<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2008.htm |title=Detailed Steel Roller Coaster Poll Results 2008|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>61<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2009.htm |title=Detailed Steel Roller Coaster Poll Results 2009|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center>76<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2010.htm |title=Detailed Steel Roller Coaster Poll Results 2010|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|<center><ref group="nb" name=nopoll/></center>
|<center>77<ref>{{cite web|url=http://www.ushsho.com/detailedsteelrollercoasterpollresults2012.htm |title=Detailed Steel Roller Coaster Poll Results 2012|publisher=Best Roller Coaster Poll |first=Mitch |last=Hawker |accessdate=February 9, 2013}}</ref></center>
|}

Notes:
{{Reflist|group=nb}}

==References==
{{Commons category|Batman: The Ride}}
{{Reflist|30em}}

{{Six Flags Great America rides}}
{{Six Flags Great Adventure rides}}
{{Six Flags Magic Mountain rides}}
{{Six Flags New Orleans}}
{{Six Flags St. Louis}}
{{Six Flags Over Georgia rides}}
{{Six Flags Over Texas rides}}
{{Batman amusement rides and stunt shows}}
{{ACE Coaster Landmarks}}

[[Category:Steel roller coasters]]
[[Category:Inverted roller coasters]]
[[Category:Mass-produced roller coasters]]
[[Category:Roller coasters in Illinois]]
[[Category:Roller coasters in New Jersey]]
[[Category:Roller coasters in California]]
[[Category:Roller coasters in Missouri]]
[[Category:Roller coasters in Georgia (U.S. state)]]
[[Category:Roller coasters in Texas]]
[[Category:Former roller coasters in Louisiana]]
[[Category:Roller coasters introduced in 1992]] <!-- Great America -->
[[Category:Roller coasters introduced in 1993]] <!-- Great Adventure -->
[[Category:Roller coasters introduced in 1994]] <!-- Magic Mountain -->
[[Category:Roller coasters introduced in 1995]] <!-- St. Louis -->
[[Category:Roller coasters introduced in 1997]] <!-- Over Georgia -->
[[Category:Roller coasters introduced in 1999]] <!-- Over Texas -->
[[Category:Roller coasters introduced in 2003]] <!-- New Orleans -->
[[Category:Amusement rides that closed in 2005]] <!-- New Orleans -->
[[Category:Roller coasters operated by Six Flags]]
[[Category:Six Flags Great America]]
[[Category:Six Flags Great Adventure]]
[[Category:Six Flags Magic Mountain]]
[[Category:Six Flags New Orleans]]
[[Category:Six Flags Over Georgia]]
[[Category:Six Flags Over Texas]]
[[Category:Six Flags St. Louis]]
[[Category:Batman in amusement parks]]