{{third-party|date=May 2015}}
{{refimprove|date=November 2013}}
{{Infobox organization
| name = Conference on College Composition and Communication (CCCC)
| founded_date = 1949
| location = [[Illinois]], [[United States]] 
| key_people = Joyce Locke Carter, CCCC Chair 2015
| focus = [[Teaching]], [[Composition (language)|composition]], [[rhetoric]], [[writing]]
| num_members = 7000
| homepage = {{URL|http://www.ncte.org/cccc/}}
}}
The '''Conference on College Composition and Communication''' ('''CCCC''', occasionally referred to as "'''Four Cs'''") is a national professional association of [[college]] and [[university]] writing instructors in the United States. Formed in 1949 as an organization within the National Council of Teachers of English, CCCC currently has about 7000 members.{{cn|date=May 2015}} CCCC is the largest organization dedicated to writing research, theory, and teaching worldwide.

== Publications and conferences ==
=== Publications ===
CCCC publishes a quarterly journal, ''College Composition and Communication'' (CCC), that seeks to promote scholarship, research, and the teaching of writing at the collegiate level. CCCC also co-publishes ''The Studies in Writing and Rhetoric'' book series with [[Southern Illinois University Press]] and, between, 1984 and 1999, an annual ''Bibliography of Composition and Rhetoric''.

=== Annual convention ===
CCCC holds an annual convention which usually has over 3000 members in attendance.{{cn|date=May 2015}} The location of the convention and convention chair changes from year to year. The convention is primarily made up of scholarly panels, featured speakers, committee meetings, special interest group meetings, and workshops. An additional part of the convention is the Research Network Forum, a venue where researchers gather to present works-in-progress, discuss methodologies, and share possible future projects.
The convention is also the time when CCCC presents several yearly awards, including the Exemplar Award, Outstanding Book Award, Richard Braddock Award (for the most outstanding article in CCC), the James Berlin Memorial Outstanding Dissertation Award, and Chair's Memorial Scholarship (for graduate students presenting at the convention), in addition to several others.<ref>http://www.ncte.org/cccc/awards</ref> In addition, the opening meeting usually features the CCCC Chair's Address during which the convention chair addresses the entire assembly of participants, often articulating a vision of the field of rhetoric and composition.<ref>Duane Roen's collection Views From the Center: The CCCC Chair's Addresses 1977-2005, Bedford-St. Martin's 2006</ref>

== Mission ==
The organization has the four following aims:
# sponsoring meetings and publishing scholarly materials for the exchange of knowledge about composition, composition pedagogy, and rhetoric
# supporting a wide range of research on composition, communication, and rhetoric
# working to enhance the conditions for learning and teaching college composition and to promote professional development
# acting as an advocate for [[language]] and [[literacy]] education nationally and internationally

== Position statements ==
CCCC has published a number of [http://www.ncte.org/cccc/resources/positions position statements on writing, teaching of writing, and related issues]. Emerging from committees within CCCC, the position statements seek to promote the CCCC goals and encourage best practices in writing [[pedagogy]].

== Committees ==
The permanent CCCC executive committee oversees a number of temporarily constituted special interest committees. These committees are constituted for a 3-year period, after which the executive committee can reconstitute the committee for another term.

== Initiatives ==
The organization sponsors the CCCC Research Initiative, which provides funds to researchers working on datasets collected by the organization and its affiliates. Begun in 2004, the grant has provided means for various research projects, including the "Composition, Rhetoric, and Literacy—What We Know, What We Need to Know" project that ran from 2004–2007. In addition to providing grant support to individual and collective projects and promoting inter-institutional collaboration, the project is designed to "create a sustained research initiative to advance scholarship in composition and rhetoric".<ref>http://www.ncte.org/cccc/awards/researchinitiative</ref>

CCCC, along with its parent organization, the [[National Council of Teachers of English]], sponsors a number of initiatives on writing, including the [[National Day on Writing]] held on October 20, 2009.

== See also ==
* [[Composition studies]]

==References==
{{reflist}}

== External links ==
* {{Official website|http://www.ncte.org/cccc}}

{{DEFAULTSORT:Conference On College Composition And Communication}}
[[Category:Professional associations based in the United States]]
[[Category:1949 establishments in the United States]]