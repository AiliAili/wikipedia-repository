{{refimprove|date=July 2006}}

'''Risk-adjusted return on capital''' ('''RAROC''') is a [[risk]]-based profitability measurement framework for analysing risk-adjusted financial performance and providing a consistent view of [[Profit (accounting)|profitability]] across businesses. The concept was developed by [[Bankers Trust]] and principal designer Dan Borge in the late 1970s.<ref name="isbn0-691-12883-9">{{cite book |author1=Herring, Richard |author2=Diebold, Francis X. |author3=Doherty, Neil A. |title=The Known, the Unknown, and the Unknowable in Financial Risk Management: Measurement and Theory Advancing Practice |publisher=Princeton University Press |location=Princeton, N.J |year=2010 |page=347}}</ref> Note, however, that more and more '''return on risk adjusted capital''' (RORAC) is used as a measure, whereby the risk adjustment of Capital is based on the [[capital adequacy guidelines]] as outlined by the [[Basel Committee on Banking Supervision|Basel Committee]], currently [[Basel III]].{{citation needed|date=August 2012}}

==Basic formulae==
<br />
:<math>\mbox{RAROC} = {\mbox{Expected return} \over \mbox{Economic capital}}</math> <ref name="pstat.ucsb.edu">[http://www.pstat.ucsb.edu/research/papers/report10_2004%5B1%5D.pdf Quantifying Risk in the Electricity Business: A RAROC-based Approach]</ref>   or   <math>\mbox{RAROC} = {\mbox{Expected return} \over \mbox{Value at risk}}</math><ref name="pstat.ucsb.edu"/> 
<br />
Broadly speaking, in business enterprises, risk is traded off against benefit. RAROC is defined as the ratio of risk adjusted return to [[economic capital]]. The economic capital is the amount of money which is needed to secure the survival in a worst-case scenario, it is a buffer against unexpected shocks in market values. Economic capital is a function of [[market risk]], [[credit risk]], and [[operational risk]], and is often calculated by [[VaR]]. This use of capital based on risk improves the capital allocation across different functional areas of banks, insurance companies, or any business in which capital is placed at risk for an expected return above the [[risk-free rate]].

RAROC system allocates capital for two basic reasons:
#Risk management
#Performance evaluation

For risk management purposes, the main goal of allocating capital to individual business units is to determine the bank's optimal [[capital structure]]—that is economic capital allocation is closely correlated with individual business risk. As a performance evaluation tool, it allows banks to assign capital to business units based on the [[economic value added]] of each unit.

==See also==
*[[Enterprise risk management]]
*[[Financial risk management]]
*[[Omega ratio]]
*[[Risk-return spectrum]]
*[[Sharpe ratio]]
*[[Sortino ratio]]
* [[Risk return ratio|Risk Return Ratio]]

==Notes==
{{reflist}}

==References==
* {{cite book|last1=Glantz|first1=Morton|title=Managing Bank Risk: An Introduction to Broad-Base Credit Engineering|date=2003|publisher=Academic Press|location=Amsterdam|isbn=0-12-285785-2}}

==External links==
*[http://www.teradata.com/tdmo/v07n01/pdf/AR5210.pdf RAROC & Economic Capital]
*[http://www.erisk.com/ResourceCenter/Features/raroc.pdf Between RAROC and a hard place]

{{Financial risk}}
{{Financial ratios}}

[[Category:Actuarial science]]
[[Category:Financial ratios]]
[[Category:Financial risk]]
[[Category:Capital requirement]]