{{Taxobox
| name = Muscovy duck
| image = MuscovyDuck.jpg 
| image_caption = A Piebald Muscovy drake 
| status = LC
| status_system = IUCN3.1
| status_ref = <ref name=IUCN>{{IUCN|id=22680061 |title=''Cairina moschata'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2013.2 |year=2012 |accessdate=26 November 2013}}</ref>
| regnum = [[Animalia]]
| phylum = [[Chordata]]
| classis = [[Aves]]
| ordo = [[Anseriformes]]
| familia = [[Anatidae]]
| genus = ''[[Cairina]]''
| species = '''''C. moschata'''''
| binomial = ''Cairina moschata''
| binomial_authority = ([[Carl Linnaeus|Linnaeus]], 1758)
| subdivision_ranks = [[Subspecies]]
| subdivision = 
*''C. moschata sylvestris'' <small>(Stephens 1824)</small><!--CHECK PARENTHESES-->
*''[[Domestic muscovy duck|C. moschata domestica]]'' <small>Donkin, 1989</small>{{sfn|Donkin|1988}}
| synonyms =
''Anas moschata'' <small>(Linnaeus, 1758)</small>
}}

The '''Muscovy duck''' (''Cairina moschata'') is a large [[duck]] native to [[Mexico]], [[Central America|Central]], and [[South America]]. Small wild and [[feral]] breeding populations have established themselves in the [[United States]], particularly in [[Florida]] and the lower [[Rio Grande Valley]] of [[Texas]] as well as in many other parts of North America, including southern Canada. Feral Muscovy ducks are found in New Zealand, Australia, and in parts of Europe.

They are large ducks, with the males about {{convert|76|cm|in|abbr=on}} long, and weighing up to {{convert|7|kg|lb|abbr=on}}. Females are considerably smaller, and only grow to {{convert|3|kg|lb|abbr=on}}, roughly half the males' size. The bird is predominantly black and white, with the back feathers being iridescent and glossy in males, while the females are more drab. The amount of white on the neck and head is variable, as well as the bill, which can be yellow, pink, black, or any mixture of these. They may have white patches or bars on the wings, which become more noticeable during flight. Both sexes have pink or red wattles around the bill, those of the male being larger and more brightly colored.

Although the Muscovy duck is a tropical bird, it adapts well to cooler climates, thriving in weather as cold as {{convert|−12|C|F|abbr=on}} and able to survive even colder conditions.<ref>{{harvnb|Holderread|2001|p=17}}</ref><ref name="nis.gsmfc.org"/> In general, '''Barbary duck''' is the term used for ''C. moschata'' in a culinary context.

The domestic breed, ''[[Cairina moschata domestica]]'', is commonly known in Spanish as the ''pato criollo'' ("creole duck"). They have been bred since [[Pre-Colombian era|pre-Columbian]] times by [[Indigenous peoples of the Americas|Native Americans]] and are heavier and less able to fly long distances than the wild subspecies. Their plumage color is also more variable. Other names for the domestic breed in Spanish are ''pato casero'' ("backyard duck") and ''pato mudo'' ("mute duck").

==Description==
[[File:Muscovy Duck SMTC.jpg|left|thumb|"True wild" Muscovy duck]]
[[File:Cairina moschata momelanotus head.jpg|right|thumb|''Cairina moschata momelanotus'' head details]]

All Muscovy ducks have long [[claw]]s on their feet and a wide flat tail. In the domestic drake (male), length is about {{convert|86|cm|in|abbr=on}} and weight is {{convert|4.6|-|6.8|kg|lb|abbr=on}}, while the domestic hen (female) is much smaller, at {{convert|64|cm|in|abbr=on}} in length and {{convert|2.7|-|3.6|kg|lb|abbr=on}} in weight. Large domesticated males often weigh up to {{convert|8|kg|lb|abbr=on}}, and large domesticated females up to {{convert|5|kg|lb|abbr=on}}.

The true wild Muscovy duck, from which all domesticated Muscovys originated, is blackish, with large white wing patches. Length can range from {{convert|66|to|84|cm|in|abbr=on}}, wingspan from {{convert|137|to|152|cm|in|abbr=on}} and weight from {{convert|1.1|-|4.1|kg|lb|abbr=on}} in wild Muscovys. On the head, the wild male has short crest on the nape. The bill is black with a speckling of pale pink. A blackish or dark red knob can be seen at the bill base, and the bare skin of the face is similar to that in color. The eyes are yellowish-brown. The legs and webbed feet are blackish. The wild female is similar in plumage, but is also much smaller, and she has feathered face and lacks the prominent knob. The juvenile is duller overall, with little or no white on the upperwing.<ref name=oiseaux/> Domesticated birds may look similar; most are dark brown or black mixed with white, particularly on the head.{{sfn|Cisneros-Heredia|2006}} Other colors such as lavender or all-white are also seen. Both sexes have a nude black-and-red or all-red face; the drake also has pronounced [[Caruncle (bird anatomy)|caruncles]] at the base of the bill and a low erectile [[Crest (feathers)|crest]] of feathers.<ref name="nis.gsmfc.org"/>

''C. moschata'' ducklings are mostly yellow with buff-brown markings on the tail and wings. For a while after hatching, juveniles lack the distinctive wattles associated with adult individuals, and resemble the offspring of various other ducks such as [[Mallard]]s. Some domesticated ducklings have a dark head and blue eyes, others a light brown crown and dark markings on their nape. They are agile and speedy [[precocial]] birds.

The drake has a low breathy call, and the hen a quiet trilling coo.

The [[karyotype]] of the Muscovy duck is 2n=80, consisting of three pairs of [[macrochromosome]]s, 36 pairs of [[microchromosome]]s, and a pair of [[sex chromosome]]s. The two largest macrochromosome pairs are [[submetacentric]], while all other [[chromosomes]] are [[acrocentric]] or (for the smallest microchromosomes) probably [[telocentric]]. The submetacentric chromosomes and the [[Z chromosome|Z (female) chromosome]] show rather little [[constitutive heterochromatin]] (C bands), while the [[W chromosome]]s are at least two-thirds heterochromatin.{{sfn|Wójcik|Smalec|2008}}
 
Male Muscovy ducks have spiralled penises which can become erect to {{convert|20|cm|in|abbr=on}} in one third of a second. Females have [[cloaca]]s that spiral in the opposite direction that appear to have evolved to limit [[Sexual coercion|forced copulation]] by males.<ref name=Sample2009/>

==Etymology, taxonomy and systematics==
[[File:Black Duck (1).jpg|thumb|right|A male and several females]]
The term "Muscovy" means "from the [[Moscow]] region", but these ducks are neither native there nor were they introduced there before they became known in Western Europe. It is not quite clear how the term came about; it very likely originated between 1550 and 1600, but did not become widespread until somewhat later.

In one suggestion, it has been claimed that the [[Company of Merchant Adventurers to New Lands]] traded these ducks to Europe occasionally after 1550;<ref name=holderread2001-7374>{{harvnb|Holderread|2001|pp=73–74}}</ref> this [[chartered company]] became eventually known as the [[Muscovy Company]] or "Muscovite Company" so the ducks might thus have come to be called "Muscovite ducks" or "Muscovy ducks" in keeping with the common practice of attaching the importer's name to the products they sold.<ref name=holderread2001-7374 /> But while the Muscovite Company initiated vigorous trade with [[Russia]], they hardly, if at all, traded produce from the [[Americas]]; thus they are unlikely to have traded ''C. moschata'' to a significant extent.

Alternatively—just as in the "[[turkey (bird)|turkey]]" (which is also from America), or the "[[guineafowl]]" (which are not limited to [[Guinea]])—"Muscovy" might be simply a generic term for a hard-to-reach and exotic place, in reference to the singular appearance of these birds. This is evidenced by other names suggesting the species came from lands where it is not actually native, but from where much "outlandish" produce was imported at that time (see below).

Yet another view—not incompatible with either of those discussed above—connects the species with the [[Muisca people|Muisca]], a [[Native Americans in the United States|Native American]] nation in today's Colombia. The duck is native to these lands too, and it is likely that it was kept by the Muisca as a domestic animal to some extent. It is conceivable that a term like "Muisca duck", hard to comprehend for the average European of those times, would be corrupted into something more familiar.

The [[Miskito people|Miskito Indians]] of the [[Mosquito Coast|Miskito Coast]] in Nicaragua and Honduras relied heavily on this domestic species. The ducks may have been named after this region.

[[File:Muscovy drake Graniteville SC USA.JPG|thumb|left|Muscovy drake]]
The species was first scientifically described by [[Carl Linnaeus]] in his 1758 edition of ''[[Systema Naturae]]'' as ''Anas moschata'',<ref name=Linnaeus1758/> literally meaning "[[musk]] duck". His description only consists of a curt but entirely unequivocal ''[Anas] facie nuda papillosa'' ("A duck with a naked and carunculated face"), and his primary reference is his earlier work ''Fauna Svecica''.<ref name=Linnaeus1746/> But Linnaeus refers also to older sources, and therein much information on the origin of the common name is found.

[[Conrad Gessner]] is given by Linnaeus as a source, but the ''[[Historiae animalium (Gesner)|Historiae animalium]]'' mentions the Muscovy duck only in passing.<ref>{{harvnb|Gessner|1555|p=118}} ; not p. 122 as per Linnaeus (1741, 1758): see {{harvnb|Aldrovandi|1637|p=192}} and {{harvnb|Willughby|1676|p=295}}</ref> [[Ulisse Aldrovandi]]<ref>{{harvnb|Aldrovandi|1637|pp=192–201}}</ref> discusses the species in detail, referring to the wild birds and its domestic breeds variously as ''anas cairina'', ''anas indica'' or ''anas libyca'' – "Duck from [[Cairo]]", "Indian duck" (in reference to the [[West Indies]]) or "[[Libya]]n duck". But his ''anas indica'' (based, like Gessner's brief discussion, ultimately on the reports of [[Christopher Columbus]]'s travels) also seems to have included another [[species]],<ref>{{harvnb|Aldrovandi|1637|pp=192, 194}}: ''Anas indica alia''</ref> perhaps a [[whistling-duck]] (''Dendrocygna''). Already however the species is tied to some more or less nondescript "exotic" locality – "Libya" could still refer to [[Ancient Libya|any place in Northern Africa]] at that time – where it did not natively occur. [[Francis Willughby]] discusses "The Muscovy duck" as ''anas moschata'' and expresses his belief that Aldrovandi's and Gessner's ''anas cairina'', ''anas indica'' and ''anas libyca'' (which he calls "The Guiny duck", adding another mistaken place of origin to the list) refer to the very same species.<ref>{{harvnb|Willughby|1676|pp=294–295}}</ref> Finally, [[John Ray]] clears up much of the misunderstanding by providing a contemporary explanation for the bird's [[etymology]]:
<blockquote>"In English, it is called ''The Muscovy-Duck'', though this is not transferred from Muscovia [the [[New Latin]] name of Muscovy], but from the rather strong musk odour it exudes."<ref name=Ray/></blockquote>

Linnaeus came to witness the birds' "[[game (food)|game]]y" aroma first-hand, as he attests in the ''Fauna Svecica'' and again in the [[Travel literature|travelogue]] of this 1746 [[Västergötland]] excursion.<ref name=Linnaeus1746/><ref name=Linnaeus1747/> Similarly, the [[Russian (language)|Russian]] name of this species, ''muskusnaya utka'' (Мускусная утка), means "musk duck" – without any reference to Moscow – as do the [[Bokmål]] and [[Danish (language)|Danish]] ''moskusand'', [[Dutch (language)|Dutch]] ''muskuseend'', [[Finnish (language)|Finnish]] ''myskisorsa'', [[French (language)|French]] ''canard musqué'', [[German (language)|German]] ''Moschusente'', [[Italian (language)|Italian]] ''anatra muschiata'', [[Spanish (language)|Spanish]] ''pato almizclado'' and [[Swedish (language)|Swedish]] ''myskand''. In English however, [[musk duck]] refers to the [[Australia]]n species ''Biziura lobata''.

[[File:Duck wings outstretched.jpg|right|thumb|A domestic Muscovy duck with wings outstretched]]
In some regions the name '''Barbary duck''' is used for domesticated and "Muscovy duck" for wild birds; in other places "Barbary duck" refers specifically to the dressed carcass, while "Muscovy duck" applies to living ''C. moschata'', regardless of whether they are wild or domesticated. In general, "Barbary duck" is the usual term for ''C. moschata'' in a [[culinary]] context.

This [[species]] was formerly placed into the [[paraphyletic]] "[[perching duck]]" assemblage, but subsequently moved to the [[dabbling duck]] [[subfamily]] (Anatinae). Analysis of the [[mtDNA]] [[DNA sequence|sequence]]s of the [[cytochrome b|cytochrome ''b'']] and [[NADH dehydrogenase]] [[Protein subunit|subunit]] 2 [[gene]]s,{{sfn|Johnson|Sorenson|1999}} however, indicates that it might be closer to the genus ''[[Aix (genus)|Aix]]'' and better placed in the [[shelduck]] subfamily [[Tadorninae]]. In addition, the other species of ''Cairina'', the rare [[white-winged duck]] (''C. scutulata''), seems to belong to a distinct genus. The [[name of a biological genus|generic name]] '''''Cairina''''', meanwhile, traces its origin to Aldrovandi, and ultimately to the mistaken belief that the birds came from [[Egypt]]: translated, the current scientific name of the Muscovy duck means "the musky one from Cairo".

==Ecology==
This [[bird migration|non-migratory]] species normally inhabits [[forest]]ed [[swamp]]s, [[lake]]s, [[stream]]s and nearby [[grassland]] and farm crops,{{sfn|Accordi|Barcellos|2006}} and often roosts in trees at night. The Muscovy duck's diet consists of plant material obtained by grazing or dabbling in shallow water, and small fish, [[amphibian]]s, [[reptile]]s, [[crustacean]]s, insects, and [[millipede]]s.<ref name=fws/> This is a somewhat aggressive duck; males often fight over food, territory or mates. The females fight with each other less often. Some adults will peck at the ducklings if they are eating at the same food source.

The Muscovy duck has benefited from nest boxes in [[Mexico]], but is somewhat uncommon in much of the east of its range due to excessive hunting. It is not considered a globally threatened species by the [[IUCN]] however, as it is widely distributed.<ref name=IUCN/>

===Reproduction===
[[File:Cairina moschata MWNH 1056.JPG|thumb|Egg, Collection [[Museum Wiesbaden]]]]
This species, like the [[mallard]], does not form stable pairs. They will mate on land or in water (note the submerged female in the image below). Domesticated Muscovy ducks can breed up to three times each year.

The hen lays a clutch of 8–16 white eggs, usually in a tree hole or hollow, which are [[Avian incubation|incubate]]d for 35 days. The sitting hen will leave the nest once a day from 20 minutes to one and a half hours, and will then defecate, drink water, eat and sometimes bathe. Once the eggs begin to hatch it may take 24 hours for all the chicks to break through their shells. When feral chicks are born they usually stay with their mother for about 10–12 weeks. Their bodies cannot produce all the heat they need, especially in temperate regions, so they will stay close to the mother especially at night.

Often, the drake will stay in close contact with the brood for several weeks. The male will walk with the young during their normal travels in search for food, providing protection. Anecdotal evidence from [[East Anglia]], UK suggests that, in response to different environmental conditions, other adults assist in protecting chicks and providing warmth at night. It has been suggested that this is in response to local efforts to cull the eggs, which has led to an atypical distribution of males and females as well as young and mature birds.

For the first few weeks of their lives, Muscovy ducklings feed on grains, corn, grass, insects, and almost anything that moves. Their mother instructs them at an early age how to feed.

<gallery mode=packed heights=136px>
File:Bariken kid.JPG|Hatchling
File:Lucky George Muscovy duckling.JPG|Young duckling
File:Muscovy Duckling Graniteville SC USA.JPG|Older duckling
File:Bill the duckling.jpg|Fledgling
File:Fledgling Muscovy Duck.jpg|One-year-old (still immature)
File:Cairina moschata reproduction.jpg|Mating pair
File:Cairina moschata 001.jpg|White Muscovy duck
File:Muscovy Duck's Mating Graniteville, SC.JPG|Mating in water; the large drake entirely submerges the female
File:Muscovy Duck Baton Rouge 1 April 2016 3.jpg|Black Muscovy Duck in [[Baton Rouge]]
</gallery>

===Feral bird===
[[File:Muscovy duck at Lake Union.JPG|thumb|right|Feral chocolate/white Muscovy duck hen at [[Lake Union]], [[Seattle]] (USA)]]
[[Feral]] Muscovy ducks can breed near urban and suburban lakes and on farms, nesting in tree cavities or on the ground, under shrubs in yards, on apartment balconies, or under roof overhangs. Some feral populations, such as that in [[Florida]], have a reputation of becoming nuisance [[pest (organism)|pest]]s on occasion.{{sfn|Johnson|Hawk|2009}} At night they often sleep at water, if there is a water source available, to flee quickly from predators if awoken. A small population of Muscovy ducks can also be found in [[Ely, Cambridgeshire]], and in [[Lincoln, Lincolnshire]] UK. Muscovy ducks have also been spotted in [[Walsall Arboretum]]. There has been a small population in the Pavilion Gardens public park in [[Buxton]], Derbyshire for many years.<ref>{{cite web| url=http://www.ispotnature.org/node/325007 | title="muscovy duck" | website="http://www.ispotnature.org" | access-date=29 August 2016}}</ref>

In the US, Muscovy ducks are considered an invasive species. An owner may raise them for food production only (not for hunting). Similarly, if the ducks have no owner, 50CFR Part 21 allows the removal or destruction of the Muscovy ducks, their eggs and nests anywhere in the United States outside of [[Hidalgo County, Texas|Hidalgo]], [[Starr County, Texas|Starr]], and [[Zapata County, Texas|Zapata]] counties in Texas where they are considered indigenous. The population in southern Florida is considered, with a population in the several thousands, to be established enough to be considered "countable" for bird watchers.<ref name=countable/>

Legal methods to restrict breeding include not feeding these ducks, deterring them with noise or by chasing, and finding nests and vigorously shaking the eggs to render them non-viable. Returning the eggs to the nest will avoid re-laying as the female would if the clutch were removed.

Recent legislation in the USA prohibits trading of Muscovy ducks and plans for eradication are in order to solve nuisance problems.<ref name=fws/>

==Domestication==<!-- This section is linked from [[Anatidae]] -->
Muscovy ducks had been domesticated by various [[Indigenous peoples of the Americas|Native American]] cultures in the [[Americas]] when [[Christopher Columbus|Columbus]] arrived in the Bahamas. The first few were brought onto the Columbus ship Santa Maria they then sailed back to Europe by the 16th century.

The Muscovy duck has been domesticated for centuries, and is widely traded as "Barbary duck". Muscovy breeds are popular because they have stronger-tasting meat—sometimes compared to roasted [[beef]]—than the usual [[domestic duck]]s which are descendants of the [[mallard]] (''Anas platyrhynchos''). The meat is lean when compared to the fatty meat of mallard-derived ducks, its leanness and tenderness being often compared to [[veal]]. Muscovy ducks are also less noisy, and sometimes marketed as a "[[Quack (sound)|quackless]]" duck; even though they are not completely silent, they don't actually quack (except in cases of extreme stress). The carcass of a Muscovy duck is also much heavier than most other domestic ducks, which makes it ideal for the dinner table. 
[[File:Cairina moschata -Graniteville SC USA.JPG|Piebald drake|thumb|left]]

Domesticated Muscovy ducks, like those pictured, often have plumage features differing from other wild birds. White breeds are preferred for meat production, as darker ones can have much [[melanin]] in the skin, which some people find unappealing.

The Muscovy duck can be crossed with mallards in captivity to produce [[Hybrid (biology)|hybrids]], known as '''[[mulard duck]]''' ("[[mule]] duck") because they are [[sterility (physiology)|sterile]]. Muscovy drakes are commercially crossed with mallard-derived hens either naturally or by [[artificial insemination]]. The 40–60% of eggs that are fertile result in birds raised only for their meat or for production of [[foie gras]]: they grow fast like mallard-derived breeds but to a large size like Muscovy ducks. Conversely, though crossing Mallard drakes with Muscovy hens is possible, the offspring are neither desirable for meat nor for egg production.<ref>{{harvnb|Holderread|2001|p=97}}</ref><ref name=Zivotofsky/>

In addition, Muscovy ducks are reportedly cross-bred in [[Israel]] with Mallards to produce kosher duck products. The [[kashrut]] status of the Muscovy duck has been a matter of [[rabbinic]] discussion for over 150 years.<ref name=Zivotofsky/>

{{stack|
[[File:Female Muscovy Duck wen.jpg|Lavender hen|thumb|right]]
{{stack|
[[File:Cairina moschata -Graniteville SC USA=Cinderfella.jpg|thumb|Lavender Drake]]
}}}}
[[Oscillococcinum]] is a [[homeopathy|homeopathic]] preparation made from Muscovy duck [[liver]] and [[heart]] manufactured by the French company [[Boiron]]; similar products are also available from other manufacturers. Typically diluted with [[lactose]] and [[sucrose]] to 1:10<sup>400</sup> (far less than one in one [[googol]]), they are advertised to relieve [[influenza]]-like symptoms, but no evidence has been found of its efficacy.<ref name=Wouden/><ref name=Vickers/>

A study examining birds in northwestern [[Colombia]] for blood [[parasitism|parasite]]s found the Muscovy duck to be more frequently infected with ''[[Haemoproteus]]'' and [[malaria]] (''[[Plasmodium]]'') parasites than [[chicken]]s, [[domestic pigeon]]s, [[domestic turkey]]s and in fact almost all wild bird species studied also. It was noted that in other parts of the world, [[chicken]]s were more susceptible to such infections than in the study area, but it may well be that Muscovy ducks are generally more often infected with such parasites (which might not cause pronounced disease though, and are harmless to humans).{{sfn|Londoño|Pulgarin-R|Blair|2007}}

==See also==
*[[List of duck breeds]]
*[[Domestic duck]]

==Notes==
{{reflist|group=note}}

==References==

{{reflist|30em|refs=
<ref name=countable>{{cite web |last=Pranty |first=Bill |url=http://lists.ufl.edu/cgi-bin/wa?A2=ind0105&L=FLORIDABIRDS-L&D=0&T=0&P=27143 |title=Re: Red-crowned Parrot |date=24 May 2001}}</ref>

<ref name=fws>{{cite journal |title=Migratory Bird Permits; Control of Muscovy Ducks, Revisions to the Waterfowl Permit Exceptions and Waterfowl Sale and Disposal Permits Regulations |author=US Fish and Wildlife Service |url=http://www.fws.gov/policy/library/2009/E9-7650.pdf |page=9316 |journal=Federal Register |volume=75 |issue=39 |date=1 March 2010}}</ref>

<ref name=Linnaeus1746>{{cite book |authorlink=Carl Linnaeus |last=Linnaeus |first=Carl |year=1746 |chapter=98 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/img/?IDDOC=255608 |title=Anas facie nuda papillosa |work=Fauna Svecica Sistens Animalia Sveciæ Regni, etc. |edition=1st: 35 |language=Latin |publisher=Conrad & Georg Jacob Wishoff |location=Leiden ("Lugdunum Batavorum")}}</ref>

<ref name=Linnaeus1747>{{cite book |authorlink=Carl Linnaeus |last=Linnaeus |first=Carl |year=1747 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/toc/?IDDOC=233372 |title=Anas facie nuda papillosa |work=Wästgöta-Resa, etc. 134 |language=Swedish |publisher=Lars Salvius |location=Stockholm ("Holmius")}}</ref>

<ref name=Linnaeus1758>{{cite book |authorlink=Carl Linnaeus |last=Linnaeus |first=Carl |year=1758 |chapter=61.13 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/toc/?IDDOC=265100 |title=Anas moschata |work=Systema naturae per regna tria naturae, secundum classes, ordines, genera, species, cum characteribus, differentiis, synonymis, locis |edition=10th |volume=vol. 1 |page=124 |language=Latin |publisher=Lars Salvius |location=Stockholm ("Holmius")}}</ref>
<ref name="nis.gsmfc.org">{{cite web |url=http://nis.gsmfc.org/nis_factsheet.php?toc_id=210 |title=Non-Native Aquatic Species in the Gulf of Mexico and South Atlantic Regions |publisher=Gulf States Marine Fisheries Commission |accessdate=6 February 2012 |archiveurl=https://web.archive.org/web/20080412105702/http://nis.gsmfc.org/nis_factsheet.php?toc_id=210 |archivedate=12 April 2008 |deadurl=yes}}</ref>

<ref name=oiseaux>{{cite web |url=http://www.oiseaux-birds.com/card-muscovy-duck.html |title=Muscovy Duck ''Cairina moschata''}}</ref>

<ref name=Ray>[[John Ray|Ray, John]] (Joannis Raii) (1713): [http://gdz.sub.uni-goettingen.de/dms/load/toc/?IDDOC=269292 ''Synopsis methodica avium & piscium: opus posthumum''], etc. (vol. 1) [in Latin]. William Innys, London, p. 150: ''Anglicē, ''the Muscovy-Duck'' dicitur, non quōd ē Muscovia huc translata esset, sed quōd satis validum moschi odorem spiret.''</ref>

<ref name=Sample2009>{{cite news |url=https://www.theguardian.com/science/2009/dec/23/video-genital-warfare-ducks |title=Video reveals twists and turns of genital warfare in ducks |last=Sample |first=Ian |date=23 December 2009 |newspaper=The Guardian |accessdate=23 December 2009}}</ref>

<ref name=Vickers>{{cite journal |last1=Vickers |first1=Andrew J. |last2=Smith |first2=Claire |year=2012 |title=Homoeopathic Oscillococcinum for preventing and treating influenza and influenza-like syndromes |journal=[[Cochrane Database of Systematic Reviews]] |volume=12 |pages=CD001957 |doi=10.1002/14651858.CD001957.pub5 |pmid=23235586 |editor1-last=Mathie |editor1-first=Robert T. }}</ref>

<ref name=Wouden>{{cite journal |last1=van der Wouden |first1=J.C. |last2=Bueving |first2=H.J. |last3=Poole |first3=P. |year=2005 |title=Preventing influenza: an overview of systematic reviews |journal=Respiratory Medicine |volume=99 |issue=11 |pages=1341–1349 |pmid=16112852 |doi=10.1016/j.rmed.2005.07.001}}</ref>

<ref name=Zivotofsky>{{cite journal |last1=Zivotofsky |first1=Rabbi Ari Z. |last2=Amar |first2=Zohar |year=2003 |title=The Halachic Tale of Three American Birds: Turkey, Prairie Chicken, and Muscovy Duck |journal=Journal of Halacha and Contemporary Society |volume=6 |pages=81–104 |url=http://www.kashrut.com/articles/ThreeBirds/}}</ref>
}}

==Bibliography==

{{refbegin|30em}}

* {{cite journal
| title      = Composição da avifauna em oito áreas úmidas da Bacia Hidrográfica do Lago Guaíba, Rio Grande do Sul
| trans_title= Composition of the avifauna in eight wetlands of the Basin Lake Guaiba, Rio Grande do Sul
| first1     = Iury Almeida
| last1      = Accordi
| first2     = André
| last2      = Barcellos
| journal    = Revista Brasileira de Ornitologia - Brazilian Journal of Ornithology
| date       = 2006
| volume     = 14
| issue      = 2
| pages      = 101–115
| language   = Portuguese
| url        = http://www4.museu-goeldi.br/revistabrornito/revista/index.php/BJO/article/view/2402
| ref        = harv
}}
* {{cite book |authorlink=Ulisse Aldrovandi |last=Aldrovandi |first=Ulisse (Ulyssis Aldrovandus) |year=1637 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/toc/?IDDOC=273391 |title=Ornithologia |edition=2nd |volume=vol. 3 (''Tomus tertius ac postremus'')  |language=Latin |publisher=Nicolò Tebaldini |location=Bologna ("Bononia") |ref=harv}}
* {{cite journal
| last1       = Cisneros-Heredia
| first1      = Diego F.
| date        = 2006
| title       = Información sobre la distribución de algunas especies de aves de Ecuador
| trans_title = Information on the distribution of some species of birds of Ecuador
| journal     = Boletín de la Sociedad Antioqueña de Ornitología (SAO)
| volume      = 16
| issue       = 1
| pages       = 7–16
| language    = Spanish
| url         = http://www.sao.org.co/publicaciones/boletinsao/02CisnerosEcuador.pdf
| archiveurl  = https://web.archive.org/web/20070927094336/http://www.sao.org.co/publicaciones/boletinsao/02CisnerosEcuador.pdf
| archivedate = {{date| 27 sep 2007}}
| deadurl     = no
| ref         = harv
}}
* {{cite book
| title      = The Muscovy Duck: Cairina Moschata Domestica: Origins, Dispersal and Associated Aspects of Geography of Domestication
| date       = 1988 
| first1     = R. A.
| last1      = Donkin
| publisher  = Routledge
| isbn       = 978-9061915447
| ref        = harv
}}
* {{cite book |authorlink=Conrad Gessner |last=Gessner |first=Conrad |year=1555 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/toc/?IDDOC=279472 |title=Historiae animalium |volume=vol. 3 |language=Latin |publisher=Christoph Froschauer |location=Zürich ("Tigurium") |ref=harv}} 
* {{cite book |last=Holderread |first=David |year=2001 |title=Storey's Guide to Raising Ducks |publisher=Storey Publishing |location=North Adams, MA |isbn=1-58017-258-X |ref=harv}}
* {{cite journal
| first1     = Steve A.
| last1      = Johnson
| first2     = Michelle
| last2      = Hawk
| date       = 2009 
| url        = http://edis.ifas.ufl.edu/uw299
| title      = Florida's Introduced Birds: Muscovy Duck (''Cairina moschata'')
| publisher  = University of Florida
| ref        = harv
}}
* {{cite journal
| last1      = Johnson
| first1     = Kevin P.
| last2      = Sorenson
| first2     = Michael D.
| date       = 1999
| title      = Phylogeny and Biogeography of Dabbling Ducks (Genus: Anas): A Comparison of Molecular and Morphological Evidence
| journal    = [[Auk (journal)|Auk]]
| volume     = 116
| issue      = 3
| pages      = 792–805
| publisher  = American Ornithologists' Union
| doi        = 10.2307/4089339
| ref        = harv
}}
* {{cite journal
| first1     = Aurora
| last1      = Londoño
| first2     = Paulo C.
| last2      = Pulgarin-R
| first3     = Silvia
| last3      = Blair
| date       = 2007
| title      = Blood Parasites in Birds From the Lowlands of Northern Colombia
| journal    = [[Caribbean Journal of Science]]
| publisher  = University of Puerto Rico at Mayagüez
| volume     = 43
| issue      = 1
| pages      = 87–93
| doi        = 10.18475/cjos.v43i1.a8
| url        = http://caribjsci.org/June07/43_87-93.pdf
| ref        = harv
| subscription = yes
}}
* {{cite book |authorlink=Francis Willughby |last=Willughby |first=Francis |year=1676 |url=http://gdz.sub.uni-goettingen.de/no_cache/dms/load/toc/?IDDOC=234289 |title=Ornithologiae libri tres |language=Latin |publisher=John Martyn |location=London |ref=harv}}
* {{cite journal
| last1      = Wójcik
| first1     = Ewa
| last2      = Smalec
| first2     = Elżbieta
| date       = 2008
| title      = Description of the Muscovy Duck (''Cairina moschata'') Karyotype
| journal    = Folia Biologica
| volume     = 56
| issue      = 3–4
| pages      = 243–248
| doi        = 10.3409/fb.56_3-4.243-248
| url        = http://www.ingentaconnect.com/content/isez/fb/2008/00000056/F0020003/art00019?token=00531c43c76f7884953b7e2a46762c6b355d23666c7050234a6d53673f7b2f267738703375686f49db7
| publisher  = Institute of Systematics and Evolution of Animals, Polish Academy of Sciences
| ref        = harv
}}
{{refend}}

==Further reading==

{{refbegin|30em}}
* {{cite web |publisher=Florida Fish and Wildlife Conservation Commission (FFWCC) |year=1999 |url=http://myfwc.com/wildlifehabitats/managed/waterfowl/nuisance/nuisance-muscovies/ |title=Nuisance Muscovy Ducks |accessdate=18 November 2008}}
* {{cite journal |authorlink=John Maddox |last=Maddox |first=John |year=1988 |title=When to believe the unbelievable |journal=[[Nature (journal)|Nature]] |volume=333 |number=6176 |page=787 |doi=10.1038/333787a0 |pmid=<!--none-->}}
* {{cite book |last=Hilty |first=Steven L. |date=2003 |title=Birds of Venezuela |publisher=Princeton University Press |isbn=0-7136-6418-5 }}
* {{cite journal |last1=Shang |first1=Aijing |last2=Huwiler-Müntener |first2=Karin |last3=Nartey |first3=Linda |last4=Jüni |first4=Peter |last5=Dörig |first5=Stephan |last6=Sterne |first6=Jonathan A.C. |last7=Pewsner |first7=Daniel |last8=Egger |first8=Matthias |year=2005 |title=Are the clinical effects of homoeopathy placebo effects? Comparative study of placebo-controlled trials of homoeopathy and allopathy |journal=[[Lancet (journal)|Lancet]] |volume=366 |issue=9487 |pages=726–732 |doi=10.1016/S0140-6736(05)67177-2 |pmid=16125589 |url=http://www.sld.cu/galerias/pdf/sitios/revsalud/are_the_clinical-effects-of-homoeopathy_placebo-effects.pdf}}
* {{cite book |last=Stiles |first=F. Gary |last2=Skutch |first2=Alexander F. |date=1989 |title=A Guide to the Birds of Costa Rica |publisher=Comstock Publishing Associates |isbn=0-8014-9600-4 }}
{{refend}}

==External links==
{{commons category|Cairina moschata}}
{{wikispecies|Cairina moschata}}
* {{BirdLife|22680061|Cairina moschata}}
* {{Avibase|name=Cairina moschata}}
* {{InternetBirdCollection|muscovy-duck-cairina-moschata|Muscovy duck}}
* {{VIREO|Muscovy+duck}}
* {{IUCN_Map|22680061|Cairina moschata}}
* {{Xeno-canto species|Cairina|moschata|Muscovy duck}}

{{DEFAULTSORT:duck, Muscovy}}
[[Category:Cairina|Muscovy]]
[[Category:Ducks|Muscovy]]
[[Category:Duck breeds|Muscovy]]
[[Category:Domesticated birds]]
[[Category:Birds of the Americas]]
[[Category:Birds of the U.S. Rio Grande Valleys]]
[[Category:Animal breeds on the GEH Red List]]
[[Category:Birds described in 1758]]