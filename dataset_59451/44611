{{Infobox person
| image            = D. B. Weiss by Gage Skidmore 2.jpg
| caption          =Weiss in 2016
| birth_name       = Daniel Brett Weiss
| birth_date       = {{Birth date and age|1971|04|23}} 
| birth_place      = [[Chicago]], [[Illinois]], U.S.
| occupation       = Author, screenwriter, producer, director
| years_active     =
| spouse           = Andrea Troyer
| children         = 2
}}
'''Daniel Brett Weiss''' (born April 23, 1971)<ref>{{cite web|title=D.B. Weiss Biography|url=http://www.starpulse.com/Celebrity/D.B._Weiss-spb4270322/Biography/|work=StarPulse|accessdate=11 October 2013}}</ref> is an American author, screenwriter, producer, and director. Along with his collaborator [[David Benioff]], he is best known as screenwriter, [[executive producer]], and sometimes director of ''[[Game of Thrones]]'', the [[HBO]] adaptation of [[George R. R. Martin]]'s series of books ''[[A Song of Ice and Fire]]''.

His 2003 [[debut novel]], ''[[Lucky Wander Boy]]'', is themed around video games.

== Early life ==
Weiss was born and raised in [[Chicago]], [[Illinois]]. His family is [[Jewish]].<ref name=jew>[http://www.timesofisrael.com/the-jewish-legacy-behind-game-of-thrones/ The Jewish legacy behind Game of Thrones], ''Times of Israel''</ref> He is a graduate of [[Wesleyan University]]. He earned a [[Master of Philosophy]] in Irish literature from [[Trinity College, Dublin]] with a thesis on [[James Joyce]]'s ''[[Finnegans Wake]]'',<ref name="vanity fair">{{cite web |url=http://www.vanityfair.com/hollywood/2014/03/game-of-thrones-benioff-weiss-interview |title=The Surprising Connection Between Game of Thrones and Monty Python |date=March 24, 2014 |work=Vanity Fair }}</ref> and a [[Master of Fine Arts]] in creative writing from the [[Iowa Writers' Workshop]].<ref>{{cite web |url=http://www.luckywanderboy.com/bio.html |title=Lucky Wander Boy |accessdate=February 22, 2011 }}</ref>

== Career==
Weiss worked as personal assistant on films such as ''[[The Viking Sagas]]'' for [[New Line Cinema]].  For a brief period, Weiss also worked as a personal assistant for [[Glenn Frey]].<ref name="vanity fair"/> Weiss then went to Dublin in 1995 to study Anglo-Irish literature and met [[David Benioff]], and three years later they met again in [[Santa Monica, California]] around 1998.<ref name="vanity fair"/>

Weiss and Benioff wrote a screenplay for a film titled ''The Headmaster'' together, but it was never made.<ref name="vanity fair"/>  In 2003, Weiss and Benioff were hired to collaborate on a new script of [[Orson Scott Card]]'s book ''[[Ender's Game]]'' in consultation with the then-designated director [[Wolfgang Petersen]].<ref name="HBO GoT 1x10">{{cite web |url=http://www.hbo.com/game-of-thrones/episodes/1/10-fire-and-blood/interview.html |title=''Game of Thrones'': Interview with David Benioff and D.B. Weiss |publisher=[[HBO]] |accessdate=March 23, 2013 }}</ref><ref>{{cite web|url=http://www.writerswrite.com/wblog.php?wblog=719061 |title=DB Weiss talks ''Halo'' |publisher=Writerswrite.com |date=July 19, 2006 |accessdate=February 22, 2011 }}</ref> It was not used.<ref>{{cite web |url=http://movies.ign.com/articles/781/781573p1.html |title=Card talks ''Ender's Game'' movie |date=April 18, 2007 |publisher=IGN Entertainment, Inc. |accessdate=January 1, 2009 }}</ref>

In 2006, Weiss said he has a second novel finished that "needs a second draft".<ref>{{cite web |url=http://www.gamesetwatch.com/2006/07/gamesetinterview_db_weiss.php |title=GameSetInterview: Halo Screenwriter DB Weiss |publisher=GameSetWatch |date=July 13, 2006 |accessdate=February 22, 2011 }}</ref> That same year, Weiss completed a screenplay for a film adaptation of the video game series ''[[Halo (series)|Halo]]'', based on a script written by [[Alex Garland]].<ref>{{cite web |last=Miller |first=Ross |url=http://www.joystiq.com/2006/07/14/db-weiss-takes-on-halo-script/ |title=DB Weiss takes on ''Halo'' script |publisher=Joystiq |date=July 14, 2006 |accessdate=February 22, 2011 }}</ref><ref>{{cite web |last=Fritz |first=Ben |date=2006-10-31 |url=http://www.variety.com/VR1117953031.html |title= No home for ''Halo'' pic |work=[[Variety (magazine)|Variety]] |accessdate=October 20, 2007 }}</ref> However, director [[Neill Blomkamp]] declared the project dead in late 2007.<ref>{{cite web|last=Farrell |first=Nick |date=October 9, 2007 |url=http://www.theinquirer.net/gb/inquirer/news/2007/10/09/halo-canned |title=''Halo'' movie canned |work=[[The Inquirer]] |accessdate=May 30, 2008 |deadurl=yes |archiveurl=https://web.archive.org/web/20081018135623/http://www.theinquirer.net:80/gb/inquirer/news/2007/10/09/halo-canned |archivedate=October 18, 2008 |df= }}</ref>

Weiss also worked on a script for a prequel to ''[[I Am Legend (film)|I Am Legend]].''<ref>{{cite web |url=http://www.upi.com/Entertainment_News/2008/09/26/I-Am-Legend-prequel-in-the-works/UPI-45231222443469/ |title=''I Am Legend'' prequel in the works |publisher=UPI.com |date=September 26, 2008 |accessdate=February 22, 2011 }}</ref> However, in May 2011, director [[Francis Lawrence]] stated that he did not think the prequel was ever going to happen.<ref>{{cite web |url=http://moviesblog.mtv.com/2011/05/03/i-am-legend-prequel-will-smith-francis-lawrence |title=Exclusive: 'I Am Legend Prequel' is Dead, Says Francis Lawrence |work=[[MTV]] Movies blog |date=May 3, 2011 |accessdate=August 11, 2011 }}</ref>

Weiss currently works with David Benioff, the writer of ''[[Troy (film)|Troy]]'', on the television series ''[[Game of Thrones]]'', based on [[George R. R. Martin]]'s book series ''[[A Song of Ice and Fire]]''.<ref>{{cite web |last=Fleming |first=Michael |url=http://www.variety.com/article/VR1117957532.html?categoryid=14&cs=1&query=hbo+fuels+fantasy+fire |title=HBO turns ''Fire'' into fantasy series |work=[[Variety (magazine)|Variety]] |date=January 16, 2007 |accessdate=February 22, 2011 }}</ref> Benioff and Weiss both directed a couple of episodes together, but used a coin-flip to decide who would get the credit on the show. Weiss received the credit for [[Game of Thrones (season 4)|season 4]], episode 1 "[[Two Swords (Game of Thrones)|Two Swords]]" , while Benioff was credited for "[[Walk of Punishment]]" in [[Game of Thrones (season 3)|season 3]].<ref name="vanity fair"/>

==Personal life==
Weiss and his wife Andrea have two children, Leo and Hugo.<ref>{{cite web|url=http://www.chicagomag.com/Chicago-Magazine/March-2013/How-D-B-Weiss-Went-From-Highland-Park-Geek-to-Game-of-Thrones-Writer/|title=How This Highland Park Geek Became a Game of Thrones Writer|work=Chicago magazine}}</ref>

== Bibliography ==

=== Author ===
{| class="wikitable sortable"
|-
! Title !! Year !! Type !! Note
|-
| ''[[Lucky Wander Boy]]'' || 2003 || [[Novel]] || 
|-
|}

==Television==
{| class="wikitable sortable" style="text-align:center"
|-
! width="56" | Year !!  Title !! width=65 | [[Film director|Director]] !! width=65 | [[Screenwriter|Writer]] !! width=65 | [[film producer|Producer]] !! | Notes
|-
| 2011–present
| style="text-align:left"| ''[[Game of Thrones]]''
| {{yes}}
| {{yes}}
| {{yes}}
| style="text-align:left"| Co-creator<br>Directed and wrote episode:"[[Two Swords (Game of Thrones)|Two Swords]]"<br>Wrote: 42 episodes
|-
| 2013–2017
| style="text-align:left"| ''[[It's Always Sunny in Philadelphia]]''
| 
| {{yes}}
| 
| style="text-align:left"| Wrote episodes: "Flowers for Charlie"<br>Bored Lifeguard #2 <small>([[Cameo appearance|cameo]])</small>
|-
| 2014
| style="text-align:left"| ''[[The Specials (internet TV series)|The Specials]]''
| 
| 
| {{yes}}
| style="text-align:left"| Executive Producer<ref name="Entertainment Weekly">{{cite web |url=http://insidetv.ew.com/2014/09/05/the-specials-own-tv/|title=Oprah-approved 'Specials' stars intellectually disabled 20-somethings|author=Ariana Bacle|date=2014-09-05|accessdate=2014-09-09}}</ref>
|}

== Awards and nominations ==
{{update|section|date=August 2016}}
{{see also|List of awards and nominations received by Game of Thrones}}
{| class="wikitable sortable"
! Year
! Award
! Category
! Work
! Result
|-
| 2011
| [[Primetime Emmy Award]]
| [[Primetime Emmy Award for Outstanding Drama Series|Outstanding Drama Series]]<ref name="IMDb:Awards">{{cite web |url=http://www.imdb.com/name/nm1888967/awards |title=D.B. Weiss: Awards |work=[[Internet Movie Database]] |accessdate=March 23, 2013 }}</ref><br /><small>(shared with eight other producers and executive producers)</small>
| ''[[Game of Thrones]]''
| {{nom}}
|-
| 2011
| Primetime Emmy Award
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]<ref name="IMDb:Awards"/><br /><small>(shared with [[David Benioff]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2012
| [[Hugo Award]]
| Best Dramatic Presentation — Long Form<ref name="IMDb:Awards"/><br /><small>(shared with eight other producers and executive producers)</small>
| ''Game of Thrones''
| {{won}}
|-
| 2012
| Primetime Emmy Award
| Outstanding Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with nine other producers and executive producers)</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2012
| [[Producers Guild of America Award|PGA Award]]
| Outstanding Producer of Episodic Television, Drama<ref name="IMDb:Awards"/><br /><small>(shared with [[Carolyn Strauss]], [[Frank Doelger]], [[David Benioff]], and [[Mark Huffam]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2012
| [[Writers Guild of America|WGA Award]]
| Best New Series<ref name="IMDb:Awards"/><br /><small>(shared with [[Bryan Cogman]], [[Jane Espenson]], [[George R. R. Martin]], and [[David Benioff]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2012
| [[SFX (magazine)|''SFX'' Award]]
| Best New Show<ref name="IMDb:Awards"/><ref>{{cite web |url=http://www.sfx.co.uk/2012/02/04/sfx-awards-2012-the-winners/ |title=SFX awards 2012: The Winners |work=[[SFX (magazine)|SFX]] |date=February 4, 2012 |accessdate=March 23, 2013 }}</ref><br /><small>(shared with cast and crew)</small>
| ''Game of Thrones''
| {{won}}
|-
| 2012
| ''SFX'' Award
| Best TV Show<ref name="IMDb:Awards"/><br /><small>(shared with cast and crew)</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2012
| [[Monte-Carlo Television Festival|Monte-Carlo TV Festival Award]]
| Drama TV series: Outstanding International Producer<ref name="IMDb:Awards"/><ref>{{cite web |url=http://www.tvfestival.com/2013/2440-palmares_2012 |title=Golden Nymphs Awards Listing, 2012 |publisher=[[Monte-Carlo Television Festival]] |date=June 14, 2012 |accessdate=March 23, 2013 }}</ref><ref>{{cite web |url=http://www.hollywoodreporter.com/news/game-of-thrones-monte-carlo-tv-festival-winners-337899 |title=HBO's ''Game of Thrones'', ''Game Change'' Win Top Prizes at Monte Carlo TV Festival |work=[[The Hollywood Reporter]] |date=June 14, 2012 |accessdate=March 23, 2013 }}</ref><br /><small>(shared with [[David Benioff]], [[Frank Doelger]], and [[Carolyn Strauss]])</small>
| ''Game of Thrones''
| {{won}}
|-
| 2013
| PGA Award
| Outstanding Producer of Episodic Television, Drama<ref name="IMDb:Awards"/><br /><small>(shared with [[Frank Doelger]], [[Carolyn Strauss]], [[Bernadette Caulfield]], and [[David Benioff]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2013
| WGA Award
| Best Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with [[David Benioff]], [[George R. R. Martin]], [[Bryan Cogman]], and [[Vanessa Taylor]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2013
| Primetime Emmy Award
| Outstanding Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with eleven other producers and executive producers)</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2013
| Primetime Emmy Award
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]<ref name="IMDb:Awards"/><br /><small>(shared with [[David Benioff]])</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2014
| Primetime Emmy Award
| Outstanding Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with ten other producers and executive producers)</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2014
| Primetime Emmy Award
| [[Primetime Emmy Award for Outstanding Writing for a Drama Series|Outstanding Writing for a Drama Series]]<ref name="IMDb:Awards"/><br /><small>(shared with D. B. Weiss)</small>
| ''Game of Thrones''
| {{nom}}
|-
| 2015
| Primetime Emmy Award
| Outstanding Writing for a Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with [[David Benioff]])</small>
| ''Game of Thrones''
| {{Won}}
|-
| 2015
| [[Primetime Emmy Award]]
| Outstanding Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with eleven other producers and executive producers)</small>
| ''[[Game of Thrones]]''
| {{Won}}
|-
| 2016
| Primetime Emmy Award
| Outstanding Writing for a Drama Series<ref name="IMDb:Awards"/><br /><small>(shared with [[David Benioff]])</small>
| ''Game of Thrones''
| {{Won}}
|-
| 2016
| [[Primetime Emmy Award]]
| Outstanding Drama Series <ref name="IMDB:">{{Cite web |url=http://www.imdb.com/name/nm1125275/awards       |title=David benioff: awards |work=lnternet Movie Database}}</ref><br /><small>
(shared with eleven other producers and executive producers)</small>
| ''[[Game of Thrones]]''
| {{Won}}
|}

== References ==
{{reflist|30em}}

== External links ==
{{commons category}}
* {{IMDb name|1888967}}

{{EmmyAward DramaWriting}}
{{Authority control}}

{{DEFAULTSORT:Weiss, D.B.}}
[[Category:Living people]]
[[Category:1971 births]]
[[Category:Jewish American screenwriters]]
[[Category:American male novelists]]
[[Category:American male screenwriters]]
[[Category:American television directors]]
[[Category:American television producers]]
[[Category:American television writers]]
[[Category:Male television writers]]
[[Category:Writers from Chicago]]
[[Category:Showrunners]]
[[Category:Game of Thrones]]
[[Category:Primetime Emmy Award winners]]
[[Category:Wesleyan University alumni]]
[[Category:Alumni of Trinity College, Dublin]]
[[Category:21st-century American writers]]