{{Infobox journal
| cover = [[File:AqBcover.gif]]
| editor = Jan E. Vermaat, E.M. Gross
| discipline = [[Botany]], [[ecology]]
| abbreviation = Aquat. Bot.
| publisher = [[Elsevier]]
| country = 
| frequency = 8/year
| history = 1975–present
| openaccess =
| impact = 1.471
| impact-year = 2013
| website = http://www.journals.elsevier.com/aquatic-botany/
| link1 = http://www.sciencedirect.com/science/journal/03043770
| link1-name = Online access
| OCLC = 2254712
| LCCN =
| CODEN =
| ISSN = 0304-3770
| eISSN =
}}
'''''Aquatic Botany''''' ("An International Scientific Journal dealing with Applied and Fundamental Research on Submerged, Floating and Emergent Plants in Marine and Freshwater Ecosystems") is a [[peer-reviewed]] [[scientific journal]] dedicated to research on structure, function, dynamics, and classification of plant-dominated aquatic communities and [[ecosystem]]s, as well as molecular, [[Biochemistry|biochemical]], and [[Physiology|physiological]] aspects of [[aquatic plant]]s. It publishes [[Basic research|fundamental]] as well as [[applied research]]. The journal was established in 1975 by Cees den Hartog,<ref name="editorial">{{cite journal |last1=den Hartog |first1=C. |title=Aquatic botany — Aims and scope of a new journal |journal=Aquatic Botany |volume=1 |issue=1 |pages=1–2 |year=1975 |doi=10.1016/0304-3770(75)90002-9}}</ref> who still serves as consulting editor.<ref name="edboard">{{cite web |url=http://www.elsevier.com/wps/find/journaleditorialboard.cws_home/503303/editorialboard |title=Editorial Board |work=Aquatic Botany |publisher=Elsevier |accessdate=2009-02-24}}</ref> It is published by [[Elsevier]] and the [[editors-in-chief]] are J.E. Vermaat ([[Norwegian University of Life Sciences]]) and E.M. Gross ([[University of Lorraine]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by [[Aquatic Sciences and Fisheries Abstracts]], [[BIOSIS Previews]], [[Current Contents]]/Agriculture, Biology & Environmental Sciences, [[EMBiology]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.471.<ref name=WoS>{{cite book |year=2014 |chapter=Aquatic Botany |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/aquatic-botany/}}

[[Category:Botany journals]]
[[Category:Ecology journals]]
[[Category:Aquatic plants]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1975]]
[[Category:English-language journals]]
[[Category:Marine botany]]