{{Infobox military person
|name=Mihr-Mihroe
|birth_date=Unknown
|death_date=555
|birth_place=[[Sasanian Empire|Eranshahr]] 
|death_place=[[Mtskheta]], [[Caucasian Iberia|Iberia]]
|placeofburial=
|image=
|caption=
|nickname=
|allegiance=[[File:Derafsh Kaviani.png|20px]] [[Sasanian Empire]]
|branch= [[Sasanian army]]
|serviceyears=
|rank=
|commands = 
|unit=
|battles= [[Iberian War]]<br>[[Lazic War]]
|laterwork=
}}
'''Mihr-Mihroe''', in [[Byzantine]] sources '''Mermeroes''' ({{lang-el|Μερμερόης}}), was a 6th-century [[Sassanid Persia]]n general, and one of the leading commanders of the [[Byzantine–Sassanid Wars]] of the time.

==Biography==
Nothing is known of his early life, but Mihr-Mihroe is recorded as an old man by 555. He first appears in summer 530, during the [[Iberian War]], when he led an army of 30,000 in an invasion of Byzantium's [[Roman Armenia|Armenian provinces]]. However, he was [[Battle of Satala (530)|defeated]] near [[Satala]] by the Byzantine generals [[Sittas]] and Dorotheus and had to withdraw.<ref name="PLRE884">{{harvnb|Martindale|Jones|Morris|1992|p=884}}.</ref><ref>{{harvnb|Greatrex|Lieu|2002|p=91}}.</ref> In summer 531, following the narrow Persian victory at [[Battle of Callinicum|Callinicum]] and a series of minor reversals in [[Armenia]] and northern [[Mesopotamia]], the Persian shah, [[Kavadh I]] (r. 488–531), sent Mihr-Mihroe along with [[Bawi]] and [[Chanaranges]] to capture the Byzantine stronghold of [[Silvan, Diyarbakır|Martyropolis]]. The two commanders laid siege to the city, but after receiving news of Kavadh's death, and with their troops suffering from the cold winter, they concluded a truce and withdrew to Persian territory.<ref name="PLRE884"/><ref>{{harvnb|Greatrex|Lieu|2002|pp=94–95}}.</ref>
[[File:The kingdom of Lazica in Late Antiquity - EN.svg|thumb|247x247px|Kingdom of [[Lazica]]]]
In 542, after the renewal of hostilities in 540, Mihr-Mihroe was dispatched by [[Khosrau I]] (r. 531–579) against the Byzantine fortress of [[Dara (Mesopotamia)|Dara]], but, according to [[Corippus]], he was defeated and captured by the fort's commander, [[John Troglita]].<ref name="PLRE884"/><ref>{{harvnb|Greatrex|Lieu|2002|pp=111–112}}.</ref> Mihr-Mihroe reappears in 548, when he was sent at the head of a large army to relieve the fortress of [[Petra, Lazica|Petra]] in [[Lazica]], which was under siege by a combined Byzantine-Lazic force. As the Byzantine commander, [[Dagisthaeus]], had neglected to safeguard the mountain passes with sufficient men, Mihr-Mihroe was able to move into Lazica, brushing aside the Byzantine detachments. He relieved the siege of Petra and reinforced its garrison, but lacking supplies for his army, he was forced to withdraw to [[Dvin (ancient city)|Dvin]] in [[Persian Armenia]], leaving behind some 3,000 men garrisoning Petra and further 5,000 under [[Phabrizus]] to keep the supply route open.<ref name="PLRE884"/><ref>{{harvnb|Greatrex|Lieu|2002|p=117}}.</ref>

These forces were defeated in the next year by the Lazi and the [[Byzantine Greeks|Byzantines]], and the new Byzantine commander, [[Bessas (general)|Bessas]], laid siege to Petra. In spring 551, Mihr-Mihroe marched to relieve the fortress once again, but before he could do this, it fell to Bessas's troops. He then turned towards the Lazic capital, [[Archaeopolis]], seizing the forts of [[Sarapanis]] and [[Scanda]] in the process. He laid siege to Archaeopolis, but his attacks were repulsed. As his army suffered from lack of supplies, he was forced to abandon the siege and head west, to the fertile province of Mocheresis, which he made his base of operations.<ref name="PLRE884"/><ref>{{harvnb|Greatrex|Lieu|2002|p=119}}.</ref> Over the subsequent winter of 551/552, he strengthened his control over eastern Lazica (including the region of [[Suania]]), while his peace overtures to the Lazic king [[Gubazes II of Lazica|Gubazes II]] (r. 541–555) failed. Reinforced with [[mercenary|mercenaries]] recruited among the [[Sabirs]], in 552 he attacked the Byzantine-Lazic strongholds of Archaeopolis, [[Tzibile]], and a third unnamed fort, but was again repulsed and withdrew to Mocheresis.<ref name="PLRE884"/><ref>{{harvnb|Greatrex|Lieu|2002|p=120}}.</ref>

In 554, through a ruse he succeeded in dislodging the Byzantines from Telephis, their most forward position, causing a general retreat along the [[Phasis (river)|Phasis river]]. He did not pursue them, however, or otherwise press his advantage, due to his own army's lack of supplies. After strengthening his own forts, he returned to Mocheresis. There he fell ill, and withdrew to [[Caucasian Iberia|Iberia]]; he died of his illness at [[Mtskheta]] in the summer of 555.<ref>{{harvnb|Martindale|Jones|Morris|1992|pp=884–885}}; {{harvnb|Greatrex|Lieu|2002|p=91}}.</ref>

==References==
{{reflist|2}}

==Sources==
{{refbegin|2}}
*{{cite book|last=Greatrex|first=Geoffrey|last2=Lieu|first2=Samuel N. C.|title=The Roman Eastern Frontier and the Persian Wars (Part II, 363–630 AD)|location=New York, New York and London, United Kingdom|publisher=Routledge (Taylor & Francis)|year=2002|isbn=0-415-14687-9|url=https://books.google.com/books?id=zc8iAQAAIAAJ|ref=harv}}
*{{cite book|editor1-last=Martindale|editor1-first=John Robert|editor2-last=Jones|editor2-first=Arnold Hugh Martin|editor3-last=Morris|editor3-first=J.|title=The Prosopography of the Later Roman Empire, Volume III: A.D. 527–641|year=1992|location=Cambridge, United Kingdom|publisher=Cambridge University Press|isbn=978-0-521-20160-5|url=https://books.google.com/books?id=ElkwedRWCXkC|ref=harv}}
{{refend|2}}

[[Category:5th-century births]]
[[Category:555 deaths]]
[[Category:6th-century Iranian people]]
[[Category:Lazica]]
[[Category:People of the Roman–Persian Wars]]
[[Category:Generals of Khosrow I]]