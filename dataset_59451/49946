{{redirect|The Cavalier|other uses|Cavalier (disambiguation)}}
{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name         = Woodstock
| orig title   = 
| translator   =
| image        = Frontispiece 1863 Woodstock-neat.png <!-- First edition cover preferred -->
| caption      = "Bevis", on the frontispiece of the 1863 edition by [[A & C Black]]
| author       = [[Walter Scott|Sir Walter Scott]]
| cover_artist = 
| country      = Scotland & England
| language     = English
| series       = [[Waverley Novels]]
| genre        = [[Historical novel|Historical]]
| publisher    = [[Archibald Constable|Archibald Constable and Co.]] (Edinburgh)<br> [[Longman|Longman, Rees, Orme, Brown and Green]] (London)
| release_date = 28 April 1826
| media_type   = Print
| pages        = 
| isbn         = 
| preceded_by   = <!-- Preceding novel in series -->
| followed_by   = <!-- Following novel in series -->
}}

'''''Woodstock, or The Cavalier. A Tale of the Year Sixteen Hundred and Fifty-one''''' (1826) is a historical novel by [[Walter Scott]]. Set just after the [[English Civil War]], it was inspired by the legend of the Good Devil of Woodstock, which in 1649 supposedly tormented parliamentary commissioners who had taken possession of a royal residence at [[Woodstock, Oxfordshire]]. The story deals with the escape of [[Charles II of England|Charles II]] in 1652, during the Commonwealth, and his final triumphant entry into [[London]] on 29 May 1660.

==Plot summary==
At a thanksgiving service in Woodstock church for the victory at [[Battle of Worcester|Worcester]] (3 September 1651), the Rev. Nehemiah Holdenough was compelled to cede the pulpit, which he had usurped from the late rector (Dr Rochecliffe), to Joseph Tomkins, who, in military attire, declaimed against monarchy and prelacy, and announced the sequestration of the royal lodge and park by Cromwell and his followers. Proceeding thither, he encountered Sir Henry Lee, accompanied by his daughter Alice, prepared to surrender his charge, and was conducted through the principal apartments by the forester Joliffe, who managed to send his sweetheart Phoebe and dog Bevis with some provisions to his hut, in which the knight and his daughter had arranged to sleep. On arriving there they found Colonel Everard, a [[Roundhead]] who had come to offer them his own and his father's protection; but Sir Henry abused and spurned his nephew as a rebel, and at Alice's entreaty he bade them farewell, as he feared, for ever. On his way to the lodge he met his Royalist friend, Captain Wildrake, whom he was sheltering in spite of his politics, and determined to send him with an appeal to Cromwell to reinstate his uncle at Woodstock. On reaching Windsor, the captain, disguised as a Roundhead, obtained an interview with Oliver Cromwell, and a compliance with Everard's request, on condition that he would aid in securing the murdered king's son, in the event of his seeking refuge with the Lees.

Armed with the warrant of ejectment, the colonel and Wildrake, accompanied by the mayor and the minister, visited the Commissioners during their evening carouse, and took part in endeavouring to ascertain the cause of some startling occurrences by which they had been disturbed. Everard made his way alone to a dark gallery, in which he fancied he heard his cousin's voice, and suddenly felt a sword at his throat. Meeting Wildrake as he regained the hall, they hurried off to the hut where they found Dr Rochecliffe reading the Church service to Sir Henry and his daughter; and, after a reconciliation between uncle and nephew, the cousins were allowed a private interview, during which Alice warned her lover against betraying the king. Returning to the lodge they were told of other unaccountable events; and during the night Everard was ordered by an apparition to change his quarters. The sentinels also declared that they had heard strange sounds, and the Commissioners decided to retire to the village inn. Master Holdenough, too, confessed that he had been terribly shocked by the reflection in a mirror of the figure of a college friend whom he had seen drowned.

[[Image:Woodstock Palace.jpg|250px|thumb|right|Woodstock Palace]]
The following day Sir Henry Lee was induced to resume his post, and his son Albert arrived with one "Louis Kerneguy", whom he introduced as his Scotch page. Sir Henry having no suspicion who his guest really was treated him without ceremony; and while Dr Rochecliffe and the colonel were planning for his escape to Holland, the disguised Charles amused himself by endeavouring to gain Alice's love; but, in spite of a declaration of his rank, she made him ashamed of his suit. A quarrel, however, having arisen between him and Everard, she evinced her loyalty by preventing a duel they had arranged, at the risk of her reputation and the loss of her cousin's affection. A similar attempt by Tomkins to trifle with Phoebe was punished by a death-blow from Joliffe. The next evening Everard and his friend, and Holdenough, were unexpectedly made prisoners by Cromwell, who, having received intelligence of their knowledge of the king's sojourn at Woodstock, had brought a large force to secure him. Wildrake, however, managed to send his page Spitfire to the lodge to warn them, and while Alice acted as Charles's guide, Albert, in his dress, concealed himself in Rosamond's tower. Cromwell and his soldiers arrived soon afterwards with Dr Rochecliffe and Joliffe, whom they had seized as they were burying Tomkins, and, having searched all the rooms and passages in vain, they proceeded to blow up the tower. Albert, however, leapt from it just before the explosion, and Cromwell was furious when he discovered the deception. In his rage he ordered the execution of the old knight and all his abettors, including his dog; but afterwards released them, with the exception of Albert, who was imprisoned, and subsequently fell in the [[Battle of the Dunes (1658)|battle of Dunkirk]] (1658). Alice returned in safety, with the news that the king had effected his escape, and a letter from him to Sir Henry, approving of her marriage with Everard, whose political opinions had been considerably influenced by recent events.

Eight years later Wildrake arrived at [[Brussels]] with news for Charles. After Cromwell's son Richard abdicated, the Protectorate was abolished and the country descended into chaos. Order was restored when [[George Monck, 1st Duke of Albemarle|George Monck]], the Governor of Scotland, marched into the City of London with his army and forced the Rump Parliament to re-admit members of the Long Parliament excluded during [[Pride's Purge]]. The Long Parliament dissolved itself and for the first time in almost 20 years, there was a general election. The Convention Parliament assembled and voted for Charles' [[English Restoration|restoration]]. In his progress to London, Charles, escorted by a brilliant retinue, amidst shouts of welcome from his assembled subjects, dismounted to salute a family group in which the central figure was the old knight of Ditchley, whose venerable features expressed his appreciation of the happiness of once more pressing his sovereign's hand, and whose contented death almost immediately followed the realisation of his anxious and long-cherished hopes.

==List of characters==
*'''Sir Henry Lee''', of Ditchley, keeper of Woodstock Park
*'''Albert Lee''', his son, a Royalist colonel
*'''Alice Lee''', his daughter
*Dr Anthony Rochecliffe, late rector of Woodstock
*Rev Nehemiah Holdenough, a Presbyterian minister
*Colonel '''Markham Everard''', a Roundhead, Sir Henry's nephew
*Joceline Joliffe, a Royalist forester, and Sir Henry's servant
*Phoebe Mayflower, his sweetheart
*''Commissioners of the Council of State''
**Colonel Desborough
**General Harrison
**Joshua Bletson
*Joseph Tomkins, their steward
*Captain '''Roger Wildrake''', of Squattlesea-mere
*Spitfire, his page
*[[Oliver Cromwell]]
*Captain Pearson, his aide-de-camp
*"Louis Kerneguy", a page, the future [[Charles II of England]]
*Bevis, a wolf-dog

==External links==
{{Gutenberg|no=9785|name=Woodstock}}
*[http://www.walterscott.lib.ed.ac.uk/works/novels/cavalier.html ''Woodstock''] at [http://www.walterscott.lib.ed.ac.uk/ Walter Scott Digital Archive], the [[University of Edinburgh]] library
{{Waverley Grey}}

{{Walter Scott}}

{{DEFAULTSORT:Woodstock (Novel)}}
[[Category:1826 novels]]
[[Category:19th-century British novels]]
[[Category:Novels by Walter Scott]]
[[Category:Historical novels]]
[[Category:1651 in fiction]]
[[Category:Novels set in Oxfordshire]]
[[Category:Constable & Co. books]]
[[Category:English Civil War novels]]