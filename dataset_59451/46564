{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Blackburn-Walker biplane
 | image= Blackburn-Walker biplane, 1909.jpg
 | caption= [[Harold Blackburn (pioneer aviator)|Harold Blackburn]] (left) at the controls of the Blackburn-Walker biplane in its original configuration.
}}{{Infobox Aircraft Type
 | type=Experimental, pioneer aeroplane
 | national origin=United Kingdom
 | manufacturer=
 | designer=[[Harold Blackburn (pioneer aviator)|Harold Blackburn]] and Albert Walker
 | first flight=Unknown
 | introduced=1909
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from=
 | variants with their own articles=
}}
|}

The '''Blackburn-Walker biplane''' was a pioneer aeroplane built in Great Britain in 1909 by [[Harold Blackburn (pioneer aviator)|Harold Blackburn]]<ref>Harold Blackburn was unrelated to aviation pioneer [[Robert Blackburn (aviation pioneer)|Robert Blackburn]]. However Harold did work as a test and demonstration pilot for Robert Blackburn during the period 1912-1914.</ref> and Albert Walker. It is not known whether it actually flew.<ref>Philip Jarrett, Lost and Found, ''[[Aeroplane Magazine|The Aeroplane]]'', June 2011, p. 50.</ref>

==Development==
The Blackburn-Walker biplane was a tailless three-bay tandem pusher craft with a canard elevator and the engine in front of its two occupants, which was designed and built by Harold Blackburn and Albert Walker in 1909. The placement of the engine ahead of the wing leading edge was unusual, with a long driveshaft passing between the two occupants to the two-bladed pusher propeller. The flywheel was extensively drilled for lightness. A precarious step below the engine allowed access to the crank handle and radiator. Lateral control was provided by twin narrow-chord rudders between the upper and lower planes.

[[File:Blackburn-Walker biplane, 1910.jpg|left|thumb|The Blackburn-Walker biplane in its modified configuration with increased wing area.]]

The two extant photographs of the aircraft are marked 'Dagenham, Barking', though it is not clear whether this refers to the short-lived Experimental Flying Ground at Dagenham<ref>Philip Jarrett, The Dagenham Experimental Flying Ground, ''Air-Britain Digest'', November–December 1984, pp. 135-139.</ref> established by the [[Royal Aeronautical Society]]<ref>Founded in 1866 as the Aeronautical Society of Great Britain, it became the Royal Aeronautical Society in 1918.</ref> or the nearby [[Handley Page]] field at Barking. Given that Handley Page supplied components to the nascent aviation industry the latter seems more likely.

It would appear that the original configuration was not successful as by 1910 the machine sported large leading-edge extensions on both upper and lower wings to provide increased lift.

The ultimate fate of the Blackburn-Walker biplane is unknown.

==References==
{{Commons category|Blackburn-Walker biplane}}
{{reflist}}
{{refbegin}}
{{refend}}
<!-- ==External links== -->
{{Blackburn aircraft}}

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

[[Category:British sport aircraft 1910–1919]]