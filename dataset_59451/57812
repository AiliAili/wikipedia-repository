{{Infobox journal
| title = JAMA Dermatology
| cover = 
| caption = 
| former_name = Archives of Dermatology
| abbreviation = JAMA Dermatol.
| discipline = [[Dermatology]]
| editor = June K. Robinson
| publisher = [[American Medical Association]]
| country = 
| history = 1960-present
| frequency = Monthly
| openaccess = 
| license = 
| impact = 5.097
| impact-year = 2015
| ISSN = 2168-6068
| eISSN = 2168-6084
| CODEN =
| JSTOR = 
| LCCN = 2012200145
| OCLC =
| website = http://archderm.jamanetwork.com/journal.aspx
| link1 = http://archderm.jamanetwork.com/issue.aspx
| link1-name = Online access
| link2 = http://archderm.jamanetwork.com/issues.aspx
| link2-name = Online archive
}}
'''''JAMA Dermatology''''' is a monthly [[peer-reviewed]] [[medical journal]] published by the [[American Medical Association]]. It covers the effectiveness of [[diagnosis]] and treatment in medical and surgical [[dermatology]], [[pediatrics|pediatric]] and [[geriatrics|geriatric]] dermatology, and [[Oncology|oncologic]] and [[Aesthetics|aesthetic]] dermatologic [[surgery]].

The journal was established in 1960 as the '''''Archives of Dermatology''''', obtaining its current name in January 2013. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 5.097, ranking it 4th out of 61 journals in the category "Clinical Dermatology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Clinical Dermatology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>  The [[editor-in-chief]] is June K. Robinson ([[Northwestern University]]).


== See also ==
* [[List of American Medical Association journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://archderm.ama-assn.org/}}

[[Category:Dermatology journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:American Medical Association academic journals]]
[[Category:Publications established in 1960]]


{{med-journal-stub}}