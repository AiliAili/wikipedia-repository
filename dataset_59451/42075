{|{{Infobox Aircraft Begin
  |name = Type 5 Racer
  |image = Napier-Heston racer.jpg
  |caption = Napier-Heston Racer at Heston airfield, c. 1940
}}{{Infobox Aircraft Type
  |type = Single seat racing monoplane
  |manufacturer = [[Heston Aircraft Company]] Ltd
  |designer =<!--- note reserved for a single designer, not a team--->
  |first flight = 12 June 1940
  |introduced = 
  |retired = 1940
  |status = 
  |primary user =
  |more users = 
  |produced = 
  |number built =  1
  |unit cost = 
  |developed from = 
  |variants with their own articles = 
}}
|}

The '''Napier-Heston Racer''', also referred to as the '''Heston Type 5 Racer''' or '''Heston High Speed Aircraft J.5''',<ref name="Clare p. 426">[[#refClare1976|Clare 1976]], p. 426.</ref> was a 1940s [[United Kingdom|British]] single-seat [[air racing|racing]] monoplane first conceived by [[Napier & Son|D. Napier and Son Ltd.]], and built by the [[Heston Aircraft Company]] Ltd, for an attempt on the [[Flight airspeed record|World Air Speed Record]]. The private venture was financed by William Morris, [[Lord Nuffield]].<ref name="Gunston p. 321"/>

==Design and development==
The Napier-Heston Racer design team was led by [[Arthur Ernest Hagg]] of [[Napier & Son]], and George Cornwall of [[Heston Aircraft Company]] Ltd. It was a single-engined, low-wing cantilever [[monoplane]], purpose-built as a contender for the [[Flight airspeed record|World Speed Record]].<ref name="Gunston p. 321">[[#refGunston1976|Gunston 1976]], p. 321.</ref> It was built almost entirely of wood, that served to ensure rapid construction, a "superfine" finish, and streamlined, "beautiful" lines. The use of a multi-ducted belly scoop and clear, low profile perspex canopy, along with a reputed 20 coats of hand-rubbed lacquer also contributed to the sleek aerodynamic finish. [[Saunders-Roe]] provided wing spars made of "Compregnated wood", a system that involved multiple laminations bonded with resin under high pressure.<ref name="Gunston p. 322">[[#refGunston1976|Gunston 1976]], p. 322.</ref>

Diminutive, thin-sectioned symmetrical wing airfoils were designed for high-speed flight.<ref name="Cowin p. 75">[[#refCowin1999|Cowin 1999]], p. 75.</ref> The elevator control circuit was designed by [[Heston Aircraft Company]]'s Chief Draughtsman, C.G.W Ebbutt, with a variable ratio- with the stick near the neutral position, large movements could be made with small resulting pitch movements. This was needed for accurate handling at low level and high speed (the 3&nbsp;km airspeed record course had to be flown under 100&nbsp;ft above sea level). Towards the ends of the control column movement, the ratio increased to allow utilization of the full range of elevator travel.<ref name="Clare p. 427">[[#refClare1976|Clare 1976]], p. 427.</ref>

The aircraft’s design parameters were purposely designed around a top secret, untested, 24-cylinder, 2,450&nbsp;hp liquid-cooled [[Napier Sabre]] engine.<ref name="Lewis p. 441">[[#refLewis1972|Lewis 1972]], p. 441.</ref> Although originally proposed to the Air Ministry and receiving approval as primarily an engine programme, the Napier-Heston Racer was ultimately not officially sanctioned and had to proceed as a private venture with Lord Nuffield entirely underwriting the project.<ref name="Gunston p. 321"/>

==Operational history==
The first aircraft of two planned for the record attempt, registered G-AFOK (call sign ''Fox Oboe King''),<ref name="Gunston p. 324"/> had its [[maiden flight]] at [[Heston Aerodrome]] on 12 June 1940, piloted by Squadron Leader G.L.G. Richmond, Chief Test Pilot of Heston Aircraft.<ref name="Cowin p. 75"/>  The takeoff was not without drama and a heavy bump during the high speed run in takeoff configuration (with the constricted canopy removed for the test flight), launched the Racer prematurely into the air.<ref name="Gunston p. 323">[[#refGunston1976|Gunston 1976]], p. 323.</ref>

Recovering from the abrupt takeoff, Richmond carried out a preliminary test flight with gear extended throughout but after only five minutes airborne, while encountering inadequate elevator control, the engine overheated. According to some accounts, Richmond was being scalded by steam from the radiator mounted below the cockpit, and in haste to carry out a forced landing, inadvertently stalled the aircraft at approximately 30&nbsp;ft above the airfield.<ref name="Gunston p. 323"/> Other sources state that the coolant leak only occurred after impact.<ref name="Clare p. 428">[[#refClare1976|Clare 1976]], p. 428.</ref> The aircraft impacted heavily, with the undercarriage driven through the wings, and the tail broken off.<ref name="Gunston p. 324">[[#refGunston1976|Gunston 1976]], p. 324.</ref> Richmond survived with minor injuries, chiefly burns.<ref>[http://www.air-racing-history.com/aircraft/Napier-Heston%20Racer.htm Napier-Heston Racer: Air Racing History.com]</ref>

Napier had ordered two examples in 1938, but with the destruction of the first prototype, the Napier-Heston programme was discontinued despite 80% completion of the second aircraft, G-AFOL, the No. 2 (as it was commonly known).<ref name="Lewis p. 444">[[#refLewis1972|Lewis 1972]], p. 444.</ref>

==Specifications==
{{aerospecs
|ref= "Heston Aircraft"<ref name="Lewis p. 444"/>''British Civil Aircraft since 1919, Vol. 3''<ref name="Jackson253">[[#refJackson1974|Jackson 1974]], p. 253.</ref>
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->eng
|crew=One
|capacity=
|length m=7.50
|length ft=24
|length in=7¼
|span m=9.766
|span ft=32
|span in=0½
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.61
|height ft=11
|height in=10
|wing area sqm=15.57 
|wing area sqft=167.6 
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|empty weight kg=
|empty weight lb=
|gross weight kg=3,266
|gross weight lb=7,200
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Napier Sabre]] H-24 cylinder liquid-cooled piston engine
|eng1 kw=<!-- prop engines -->1,828
|eng1 hp=<!-- prop engines -->2,450
|max speed kmh=772
|max speed mph=480 (estimated)
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=
|range miles=
|endurance h =0<!-- if range unknown -->
|endurance min=<!-- if range unknown -->18
|ceiling m=
|ceiling ft=
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent|
|related=
|similar aircraft=
|sequence=
|lists=
|see also=
}}

==Comparable aircraft==
*[[Hughes H-1 Racer]]: 352&nbsp;mph in 1935 (unofficial world record).
*[[Messerschmitt Bf 109#Flight records|Messerschmitt Bf 109 V-13, D-IPKY]]: 379&nbsp;mph on 11 November 1937 (unofficial world record)
*[[Heinkel He 100#World speed record|Heinkel He 100]] V-2: 394.6&nbsp;mph in June 1938; V-8: 463.9&nbsp;mph on 30 March 1939 (both unofficial world records)
*[[Supermarine Spitfire (early Merlin powered variants)#Speed Spitfire (Type 323)|Supermarine Speed Spitfire]]: 408&nbsp;mph in February 1939 (not during a record attempt)
*[[Messerschmitt Me 209|Messerschmitt Me 209 V-1]]: 469&nbsp;mph on 26 April 1939 ([[Fédération Aéronautique Internationale|FAI]] [[Flight airspeed record|landplane airspeed world record]])

==References==

===Notes===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* <cite id=refClare1976>Clare, R.A. "Napier-Heston Racer Postscript". ''Aeroplane Monthly'', August 1976. IPC Media.</cite>
* <cite id=refCowin1999>Cowin, Hugh W. ''The Risk Takers, A Unique Pictorial Record 1908-1972: Racing & Record-setting Aircraft'' (Aviation Pioneer 2). London: Osprey Aviation, 1999. ISBN 978-1-85532-904-1.</cite>
* <cite id=refGunston1976>Gunston, Bill. "The Napier-Heston Racer". ''Aeroplane Monthly'', June 1976. IPC Media.</cite>
*''The [[Illustrated Encyclopedia of Aircraft]]'' (Part Work 1982-1985). London: Orbis Publishing, 1985, p.&nbsp;2158.
* <cite id=refJackson1974>Jackson, A.J. ''British Civil Aircraft since 1919, Vol. 3''. London: Putnam, 1974. ISBN 0-370-10014-X.</cite>
* <cite id=refLewis1972>Lewis, Peter. "Heston Aircraft." ''Air Pictorial'', Volume 34, no. 11, November 1972. </cite>
* Meaden, Jack. "The Heston-Napier Racer and World Records". ''Air-Britain Archive'' (quarterly), Summer 2006. 
{{Refend}}

==External links==
{{Commons category|Napier-Heston Racer}}
* [http://www.airracinghistory.freeola.com/aircraft/Napier-Heston%20Racer.htm Air Racing History website: Napier-Heston Racer]
* [http://www.fliegerweb.com/geschichte/flugzeuge/lexikon.php?show=lexikon-641 Heston J-5 Napier Racer, technical data, in German only]

{{Heston aircraft}}

[[Category:British experimental aircraft 1940–1949]]
[[Category:British sport aircraft 1940–1949]]
[[Category:Heston aircraft]]