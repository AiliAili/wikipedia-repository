<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->{{Infobox aviator
|name= Herbert O. Fisher
|image=Herb Fisher.jpg
|image_size=200px
|caption= Herbert O. Fisher, c. 1944
|full_name= Herbert Owen Fisher
|nickname=
|birth_date={{birth date|mf=yes|1909|03|06}}
|birth_place= [[Tonawanda (town), New York|Tonawanda]], [[Erie County, New York|New York]]<ref name="1910 Census"/>
|death_date={{death date and age|mf=yes|1990|07|29|1909|03|06}}
|death_place= [[Chilton Memorial Hospital]], [[Kinnelon, New Jersey]]
|death_cause= congestive heart failure
|nationality= [[United States of America]]
|spouse= Emily Fisher (née Yucknat)<ref name="1930 Census">[http://search.ancestry.ca/cgi-bin/sse.dll?db=1930usfedcen&rank=1&new=1&so=3&MSAV=0&msT=1&gss=ms_db&gsfn=Herbert+O&gsln=Fisher&msbdy=1909&dbOnly=_83004006|_83004006_x&dbOnly=_83004005|_83004005_x&uidh=000"1930 United States Federal Census for Herbert O. Fisher".] ''ancestry.ca.'' Retrieved: June 15, 2013.</ref>|parents= Harold O. Fisher<br> Emma Rose Fisher (née Wortley)<ref name="1910 Census"/>
|children= Herbert O. Fisher, Jr.
|known_for= Test pilot, Administrator
|air_force= [[United States Army Air Corps]]
|battles= World War II
|rank= 
|awards=[[Air Medal]]
}}

'''Herbert O. Fisher''' (March 6, 1909 – July 29, 1990) was an American test pilot and an aviation executive, overseeing aviation projects at the [[Port Authority of New York and New Jersey]]. He worked for the [[Curtiss-Wright|Curtiss-Wright Corporation]]. Fisher flew as a pilot for over 50 years, racking up 19,351 accident and violation free hours.

During World War II Fisher was sent to the [[China Burma India Theater|China-Burma-India theater]] to train the [[Flying Tigers]] as a civilian. He flew many combat missions and was awarded the [[Air Medal]] by [[Franklin D. Roosevelt]].

Disproving the public perception of the heroic test pilot, among his peers, the portly Fisher was considered one of the premiere pilots of his time. "Herbert O. Fisher is one of those people who generally went unnoticed. That is until he climbed into the cockpit of an airplane."<ref name="Jordan"/>

==Early years==
Fisher was born on March 6, 1909 at [[Tonawanda (town), New York|Tonawanda]], [[Erie County, New York|New York]], to Harold O. Fisher and Emma Rose Fisher (née Wortley).<ref name="1910 Census">[http://search.ancestry.ca/cgi-bin/sse.dll?db=1910USCenIndex&rank=1&new=1&so=3&MSAV=0&msT=1&gss=ms_db&gsfn=Herbert+O&gsln=Fisher&msbdy=1909&dbOnly=_83004006%7C_83004006_x&dbOnly=_83004005%7C_83004005_x&uidh=000"1910 United States Federal Census for Herbert O. Fisher".] ''ancestry.ca.'' Retrieved: June 15, 2013.</ref> At the age of 16, he had his first flying experience at the hands of a barnstormer, and was "hooked" on flying.<ref name="Hansen"/> After completing two years at college, Fisher began to explore aviation as a career.<ref name="1930 Census"/>

==Aviation career==
[[File:Herbert O. Fisher.jpg|thumb|left|Curtiss test pilots, Fisher in center, c. 1940]]

Fisher's aviation career began with his signing up with the [[United States Army Air Corps]] in 1927, becoming a member of the 309 Observation Reserve Squadron, Schoen Field, [[Fort Benjamin Harrison]], Indiana. He began flight training in 1928.<ref name="P-47"/>{{#tag:ref|In 1931, 309 Observation Reserve Squadron consisted of 18 National Guard personnel and 10 aircraft.<ref name="Bulletin "/>|group=N}} In 1931, as the Secretary for the Aviation Committee of the [[Indianapolis]] Chamber of Commerce, Fisher was actively involved in the aviation interests in the area, including the operation of the municipal airport.<ref name="Bulletin ">[http://digitallibrary.imcpl.org/cdm/fullbrowser/collection/icc/id/3827/rv/compoundobject/cpd/3832 "City aviation grows: Report for 1931 reveals great record."] ''Bulletin of the Indianapolis Chamber of Commerce'', January 1932, Vol. 47, No. 1, p. 4. Retrieved: June 19, 2013.</ref>

===Curtiss-Wright===
[[File:FMA CURTISS HAWK 75-0.jpg|thumb|Curtiss Hawk 75-O, developed for [[Argentine Army Aviation]], as an export Hawk 75 variant.]]

After leaving the military in 1933, Fisher joined Curtiss-Wright, and was assigned to test pilot duties. In checking out aircraft off the production lines at the [[Buffalo, New York]] plant, on his first day, he flew 10 aircraft.{{#tag:ref|The standard procedure for an acceptance flight was a one-hour check of all major systems. Fisher had a specialty, he would take an aircraft up to "20,000, strain the engine upward, then go inverted, throttle full forward, and nose it over, losing 10,000 feet or more at 500mph+. He knew it was almost impossible to tear a P-40 apart in a dive, which was a HUGE advantage over the Zero. He knew the operating parameters of that airplane and almost every rivet."<ref name= "Fisher Jr.">Fisher, Herbert O., Jr.[http://www.forums.flyingtigersavg.com/index.php?tid=28&tpg=1 "Herbert O Fisher, Chief Production Test Pilot, Curtiss Wright, 1939 forward."] ''flyingtigersavg.com'',  February 20, 2009. Retrieved: June 15, 2013.</ref>|group=N}} During his 15 years with Curtiss-Wright as Chief Production Test Pilot, Fisher recorded thousands of test flight hours in the [[Curtiss C-46 Commando]], [[Curtiss P-36 Hawk|Curtiss P-36/Model 75 Hawk]], [[Curtiss SB2C Helldiver]]s, [[Curtiss P-40 Warhawk]]s and [[Republic P-47 Thunderbolt]] fighters.{{#tag:ref|During World War II, along with its own transports, dive bombers, trainers, seaplanes and fighters, Curtiss-Wright manufactured 354 P-47Gs at its [[Buffalo, New York]] plant.<ref>O'Leary 1986, p. 228.</ref>|group=N}} He flew 2,498 P-40s in his role as a production test pilot.<ref name="Hansen"/>{{#tag:ref|In 1976, Fisher flew a P-40 again to make it an even 2,500 flights in the type.<ref name="Bartlett"/>|group=N}}

[[File:Flying Tigers Fisher.jpg|thumb|left|Fisher in P-40E "70" leads Curtiss-Wright test pilots flying in''Flying Tigers'', c. 1942.]]
[[File:Curtiss Execs.jpg|thumb|Curtiss company executives, with Fisher (at center), indicating the highly-decorated P-40N,{{#tag:ref|The Curtiss P-40N, representing the 15,000th Curtiss fighter built, was emblazoned with the insignia of all 28 air forces with which Curtiss fighters had served during World War II.<ref>[http://www.planetgiant.com/details/1291148914VBGXUE/1944-Curtiss-P-40-Warhawk?redirecturl=http%3A%2F%2Fwww.planetgiant.com%2Ftext%2Fflying%2Baviation%2Bairplane%2Baircraft%2Bnavigator%2Bpropeller%3Fpage%3D10 "1944 Curtiss P-40 Warhawk."] ''planetgiant.com.'' Retrieved: June 14, 2013.</ref>|group=N}} the firm's 15,000th fighter, November 1944.]]

In 1942, due to wartime priorities that prohibited the use of military aircraft for [[Cinema of the United States|Hollywood]] productions, [[Republic Studios]] approached Curtiss-Wright in [[Buffalo, New York|Buffalo]] to recreate the aerial battle sequences required for ''[[Flying Tigers (film)|Flying Tigers]]'', starring [[John Wayne]].<ref>Orriss 1984, p. 60.</ref> A number of P-40E fighter aircraft waiting for USAAC delivery were repainted in [[American Volunteer Group|AVG]] markings, and with the aid of Curtiss test pilots, flew in the film. Fisher's screen role was in subbing for John Wayne.<ref>Maurer, Neil L. [http://www.ex-cbi-roundup.com/documents/1979_october.pdf "Letters from the Editors."] ''CBI Roundup'', Vol.34, No. 8, October 1979.</ref>

At the request of the commander of the Flying Tigers, [[Claire Chennault]], Chief Engineer [[Don R. Berlin]] sent Fisher abroad as the best way to have "imparted his experience on those courageous young fellows over there with knowledge they could not possibility have".<ref name= "Fisher Jr."/> During 13 months overseas from 1943 on, despite his being a civilian test pilot, while in the China-Burma-India [[China Burma India Theater|(CBI)]] theater, Fisher flew as many as 50 missions to prove the P-40 under combat conditions. He also lectured and conducted P-40 flight demonstrations in almost every fighter base in the CBI, Middle East, North and Central Africa.<ref name="P-47"/>
[[File:C-46 Commando.jpg|thumb|left|Curtiss C-46 Commando]]
[[File:Herb and fire chief.jpg|thumb|Fisher with Curtiss-Wright Fire Chief John L. Ward, note Caldwell in background, August 6, 1942.]]

Fisher was also instrumental in supporting the introduction of the [[Curtiss C-46 Commando|Curtiss C-46]] at the Engineering and Operation section of the Air Transport Command. On August 6, 1942, first-hand experience with a faulty landing gear on an early production C-46, led to an example of his coolness in critical situations.<ref>Davis et al. 1978, p. 55.</ref> While on an acceptance re-flight, the aircraft was loaded with Curtiss executives, and a special guest, fresh from combat in North Africa, [[Australia]]n P-40 [[Flying ace|ace]], [[Group Captain]] [[Clive Caldwell|Clive "Killer" Caldwell]]. With the landing gear stuck in a three-quarters down position, and after an extended eight-hour attempt to release the gear, Fisher calmly belly-landed the C-46. With the weight of the aircraft gently pushing the gear back into the wheel wells, a minimum of damage resulted. Caldwell had taken over as the co-pilot on the eight hours of circling over Buffalo, receiving certification that he was checked out on the C-46, under the tutelage of Fisher.<ref>Alexander 2006, pp. 165–166.</ref>
Recognizing that the new transport was subject to teething problems common to any new type, Fisher was able to assist operational units in the technique of flying and maintaining C-46 transports. He would eventually fly 96 "research missions" over "[[The Hump]]" as a means of testing all the critical systems and troubleshooting a dangerous tendency for the engine fires and explosions that had plagued the C-46.<ref>Ethell and Downie 1996, pp. 86–87.</ref> CBI C-46 pilot Don Downie recalled, "Herb Fisher's detailed reports were some of the best pilot briefings we had."<ref>Ethell and Downie 1996, p. 87.</ref>

Through his hard work and persistence, Fisher also convinced American General [[Joseph Stilwell]], the Vice Commander of the CBI theater, that the new operational procedures would save the lives of hundreds of pilots and passengers. In 1944, at a special ceremony in Washington, D.C., President Franklin D. Roosevelt awarded the Air Medal to Fisher in recognition of his service; he was the first living civilian to receive the honor.<ref name="Hansen">Hansen, Lee. [https://news.google.com/newspapers?nid=1873&dat=19750821&id=VkUfAAAAIBAJ&sjid=ddEEAAAAIBAJ&pg=685,1841192 "Herb Fisher: Career of Derring-do"]''Daytona Beach Morning Journal'', August 21, 1975.</ref>

==Postwar==
At the end of World War II, Fisher transferred to the Propeller Division of Curtiss-Wright, in [[Caldwell, New Jersey]], where he served as the Chief Pilot. One of his important assignments was to assess the full potential of propeller-driven aircraft.<ref name="Bartlett">Bartlett, Kay. [https://news.google.com/newspapers?nid=1499&dat=19761116&id=400aAAAAIBAJ&sjid=XCkEAAAAIBAJ&pg=5252,4120584 "Daring old men in their flying machines."] ''Milwaukee Journal'', November 16, 1976.</ref>
[[File:Herb Fisher-P47.jpg|thumb|left|Fisher with USAAF P-47D-30-RE "Test PE-219" in postwar testing; note scimitar propeller.]]

===Compressibility effects===
After test flights of a [[Republic P-47 Thunderbolt|P-47C]] on November 13, 1942, [[Republic Aviation]] issued a press release on December 1, 1942, claiming that Lts. [[Harold E. Comstock]] and fellow test pilot Roger Dyar had exceeded the [[speed of sound]].<ref>Hammel 1983, pp. 275–280.</ref> In response, Fisher later observed, “We knew about Mach 1 going clear back to the P-36 and the P-40 ... Nothing could go {{convert|600|mph|abbr=on}} mph in level flight, but pilots were beginning to dive fighters. We ran into compressibility back in ’38.”<ref>Wilkinson, Stephan. [http://www.airspacemag.com/history-of-flight/Mach_1.html "Mach 1: Assaulting the Barrier."] ''Air & Space magazine'', December 1990.</ref>

The desire to develop a propeller that maintained its efficiency at [[transonic]] and [[supersonic]] speeds led the Curtiss-Wright Propeller Division to design and test several different concepts, including a thin, cuffed four-blade and a three-bladed "scimitar" design. Utilizing a specially modified P-47D-30-RE on loan from the [[United States Army Air Forces|USAAF]],{{#tag:ref|P-47D-30-RE (44-90219) was marked as "Test PE-219".<ref>[http://p-47.database.pagesperso-orange.fr/Database/44-xxxxx.htm "P-47 Serials: 44-xxxx."] ''pagesperso-orange''. Retrieved: June 16, 2013.</ref>|group=N}} Fisher undertook over 100 high Mach number precision dives from {{convert|38000|ft|m|abbr=on|0}} at speeds from {{convert|500|mph|abbr=on}} to {{convert|590|mph|abbr=on}}.{{#tag:ref|In a bit of grim humor, British test pilot [[Eric Brown (pilot)|Eric Brown]] said diving the P-47 at anything over Mach 0.74 was called "The Graveyard Dive".<ref>Brown 1994, p. 145.</ref>|group=N}} The typical flight began above {{convert|35000|ft|m|abbr=on|0}} when Fisher would push over into a steep dive, allowing his airspeed to build beyond {{convert|560|mph|abbr=on}} (true airspeed).<ref>Hallion 1981, pp. 205–206.</ref> He would then execute a pullout at {{convert|18000|ft|m|abbr=on|0}}, having to maintain an exacting set altitude within plus or minus five ft.<ref name="Bartlett"/>

Some of the tests proved hazardous with flexing of the thin blades on the ground run-ups. Test flights also had to be carefully flown as engine power could only be fed in gradually for the same reason.  The most serious incident, though one unconnected with compressibility, occurred in August 1948 when a rupture of a high pressure oil line at {{convert|590|mph|abbr=on}} mph over [[Allentown, Pennsylvania]] led to an emergency "blind" landing with the entire aircraft coated in black oil.<ref>Hallion 1981, p. 206.</ref> Several of these dives resulted in speeds of Mach 0.83; one on 27 October 1949 reached the fastest speed a P-47 could attain.<ref>McKinney, Kevin. [http://www.aflyer.com/1001_f_adventuresinflying.html "Review: Adventures in Flying by Jack Elliott."]''Atlantic Flyer'', January 2010, pp. 5–6. Retrieved: June 14, 2013.</ref>

During the test program, Fisher brought his son, Herbert O. Fisher, Jr., along for a Mach 0.80 dive. The youngster was touted as "the world's fastest toddler".<ref name="Jordan">Jordan, Corey C. [http://planesandpilotsofww2.webs.com/Fisher.html "Pushing the envelope with test pilot Herb Fisher".] ''Planes and Pilots of WW2'', 2000. Retrieved: June 14, 2013.</ref>
[[File:An USAF C-54 Skymaster.jpg|thumb|Douglas C-54 Skymaster]]

===Reversible pitch experiments===
Fisher also conducted a program designed to allow aircraft to descend rapidly. It involved reversing all four propellers simultaneously in flight. On a [[Douglas C-54 Skymaster]] transport, the extremely high sink rates of up to 15,000 feet per minute in four seconds after reversal, produced a safe method of rapid descent for airliners. The flight would start at 15,000&nbsp;ft., three miles from the airfield before push over, landing and coming to a full stop in one minute and 50 seconds. Fisher wrote that forward airspeed was well within normal parameters ({{convert|200|mph|abbr=on}}), with no decrease in controllability.<ref name="Jordan"/>

Fisher conducted nearly 200 of these high rate/low speed descents and demonstrated the technique for Generals[[Henry H. Arnold|"Hap" Arnold]] and [[Dwight D. Eisenhower|Dwight Eisenhower]] in 1948. For the latest pressurized airliners and combat aircraft that operated at high altitudes, this was an effective method of safely dumping altitude in the event of an emergency, and was adopted worldwide as a safe procedure.<ref>Nilsson, Eric. [http://economics.csusb.edu/facultystaff/nilsson/personal/Professional/By_Doing.pdf "Innovating-by-doing: Skill, Innovation as a Source of Technological Advance."] ''Journal of Economic Issues, Volume XXIX, No. 1, March 1995, p. 42.</ref> Fisher was also instrumental in developing the use of reversing pitch to rapidly slow an aircraft, which allowed them to land safely on shorter runways, and, in general, greatly reduce the incidence of runway overruns.<ref name="P-47"/>
[[File:F8F Bearcat (flying).jpg|thumb|left|Grumman F8F Bearcat]]

Later, flying a U.S. Navy [[Grumman F8F Bearcat]] fighter, Fisher developed a way to fly "zero-[[g-force|g]]", vertical dives. From 20,000&nbsp;ft, the F8F would be nosed down into a vertical dive, while he simultaneously reversed the propeller pitch. This technique allowed a controlled vertical dive at rates of descent that varied between 30,000 and 37,000 fpm.<ref name="Jordan"/>
[[File:Herb Fisher- 2,500 flight.jpg|thumb|Fisher with the P-40 in which he had his 2,500th flight in the type, c. 1976.]]

==Later career and associations==
After leaving Curtiss-Wright in 1952, Fisher worked for 23 years as a Special Assistant for Aviation to Executive Director [[Austin Joseph Tobin]] at the Port Authority of New York and New Jersey.<ref>Griffin 1959, p. 90.</ref><ref>Glanz and Lipton 2004, p. 42.</ref> As head of aviation-industry affairs, his work included evaluating requests for fixed-wing and rotary aircraft to use airports in the New York metropolitan region. The airports he supervised included [[John F. Kennedy International Airport|John F. Kennedy]], [[LaGuardia Airport|LaGuardia]], [[Newark Liberty International Airport|Newark]] and [[Teterboro Airport|Teterboro]]. During his tenure, Fisher checked out numerous aircraft from wide-body airliners to the executive jets.{{#tag:ref|During his administration, the one aircraft that was not cleared for use in the New York area was the [[Concorde|Aérospatiale-BAC Concorde]]. Fisher was instrumental, however, in setting up the approach corridor for the Concorde that would later be used when the supersonic airliner began regular service to JFK in 1977.<ref>Henriques, Dudley. [http://warbirdinformationexchange.org/phpBB3/viewtopic.php?f=3&t=26283&start=0 "Re: Curtiss Wright Chief test pilot Lloyd Child".] ''Warbirdinformationexchange.org'', December 9, 2008. Retrieved: June 17, 2013.</ref>|group=N}} He retired in 1975, but continued to be active in aviation and other interests.<ref name="obit"/> Over the years, in his role as Special Assistant at the Port Authority of New York and New Jersey, Fisher had gained a reputation as a knowledgeable and skilled pilot, as well as being friendly and affable. Likewise, his dapper figure, often in well-cut clothes, led to a painting given to him on retirement, titled, "The man in the flying tuxedo," that highlighted his many connections to aviation.<ref>[https://www.flickr.com/photos/sdasmarchives/4728448611/sizes/o/in/photolist-8cQx34-8cTRcU-8cTRcy-8cQwZp-8cTRdG-8DMiXy-8DMiXf-eejvjT/ "The Man in the flying tuxedo."] ''San Diego Air & Space Museum Library and Archives'', May 7, 2010. Retrieved: June 18, 2013.</ref>

Other positions Fisher filled included Director of Aeronautics for the Indianapolis Chamber of Commerce and instructor at the [[Embry–Riddle Aeronautical University]] where he held a Doctorate of Aeronautical Science (''honoris causa'') and was a member of their International Advisory Council.<ref>[http://www.coursehero.com/file/2013534/UG0607Faculty/ "Elements 0607, Fall 2009."] ''Embry-Riddle FL/AZ'' via ''coursehero.com''. Retrieved: June 17, 2013.</ref> The university also has an endowment fund in his name, the Herbert O. Fisher Scholarship.<ref>[http://givingto.erau.edu/pdfs/lift/winter07/liftw07.pdf "Endowment Accounts."] ''Lift, the alumni magazine of the Embry-Riddle Aeronautical University'', Winter 2007. Retrieved: June 15, 2013.</ref>

In later years, Fisher was founder and first President of the P-40 Warhawk Association, a charter member and Past President of the P-47 Thunderbolt Pilots Association,  Past President of the CBI Hump Pilots Association, and Past President of the [[Yankee Air Museum|Yankee Air Force]], Northeast Division. Among the many other associations in which he took an active role was as a charter member of the International Fighter Pilots Fellowship, member of the [[Society of Experimental Test Pilots]],  the [[Wings Club]] of New York and the China-Burma-India Veterans Association.<ref name="Wasp"/> In Caldwell, New Jersey, Fisher was on the New Jersey Civil Air Defense Advisory Council and also served as a councilman and police commissioner.<ref name="obit"/>
[[File:Herb Fisher CBI.jpg|thumb|Fisher receiving the CBI Americanism Award, his wife Emily by his side, c. 1976.]]

==Awards and honors==
In 1976, Fisher was inducted into the OX5 Aviation Pioneers Hall of Fame, at the [[Glenn H. Curtiss Museum]] in [[Hammondsport, New York]], at the same time as [[Charles Lindbergh]]; he also received the China-Burma-India Veterans Association's Americanism Award. In the same year, Fisher was named a General "[[Jimmy Doolittle]] Educational Fellow" at the Air Force Association, with the award presented by [[Barry Goldwater]].<ref name="P-47">[http://www.p47pilots.com/P47-Pilots.cfm?c=incP47BiographyHome.cfm&vm=BIO&pilotid=157&p=Herbert%20O.%20Fisher "Herbert O. Fisher biography."] ''P-47 Pilots Association''. Retrieved: June 14, 2013.</ref> Joining the ranks of many famous aviators from New Jersey, in 1983, Fisher was inducted into the [[Aviation Hall of Fame and Museum of New Jersey]].<ref>[http://www.njahof.org/halloffame.htm "Hall of Fame Inductees."] ''Aviation Hall of Fame and Museum of New Jersey''. Retrieved: June 15, 2013.</ref> In 1984, the Yankee Air Force honoured Fisher as the "Airman of the Year" as well as inducting him in the association's Hall of Fame.<ref name="Wasp">[http://www.twu.edu/library/wasp/newsletters/1984_June.pdf "WWII Pilots Gatherings Scheduled."] ''Wasp Newsletter'' June 1984. Retrieved: June 15, 2013.</ref> On May 13, 2011, Fisher was also inducted into the [[Ira G. Ross Aerospace Museum|Niagara Frontier Aviation & Space]] Hall of Fame. Accepting on behalf of his father, was Herbert O. Fisher, Jr.<ref>[https://www.facebook.com/media/set/?set=a.10150180480445997.304067.87126225996&type=3 "Herbert O. Fisher."] ''Niagara Aerospace Museum''. Retrieved: June 15, 2013.</ref>

==Death==
At age 81, Fisher died on July 29, 1990, after suffering congestive heart failure.<ref name="obit">[https://www.nytimes.com/1990/08/03/obituaries/herbert-o-fisher-81-test-pilot-and-official.html "Obituary: Herbert O. Fisher, 81, Test Pilot and Official."] ''[[The New York Times]]'', August 3, 1990. Retrieved: June 14, 2013.</ref> At the time of his death, he lived in the [[Smoke Rise (community)|Smoke Rise]] gated community in [[Kinnelon, New Jersey]], his family home since 1955.<ref name="obit"/><ref>Fisher, Herbert O., Jr. [http://smokerise-nj.blogspot.ca/2010/02/early-days-of-smoke-rise.html "Tales from the Early Rise."] ''The Smoke Rise and Kinnelon Blog: The Early Days of Smoke Rise'', February 7, 2010. Retrieved: June 17, 2013.</ref>

==See also==
* [[Test pilot]]
* [[Flight test]]

==References==

===Notes===
{{Reflist|group=N}}

===Citations===
{{Reflist|30em}}

===Bibliography===
{{Refbegin}}
* Alexander, Kristen. ''Clive Caldwell: Air Ace''. Crows Nest, Australia: Allen & Unwin, 2006. ISBN 1-74114-705-0.
* Brown, Eric. ''Testing for Combat: Testing Experimental and Prototype Aircraft, 1930-45.'' London: Airlife Publishing Ltd., 1994. ISBN 978-1-85310-319-3.
* Davis, John M., Harold G. Martin and John A. Whittle. ''The Curtiss C-46 Commando''. Tonbridge, Kent, UK: Air-Britain (Historians) Ltd., 1978. ISBN 0-85130-065-0.
* Ethell, Jeffrey L. and Don Downie. ''Flying the Hump: In Original World War II Color''. St. Paul, Minnesota: Motorbooks, 1996. ISBN 978-0-76030-113-5.
* Glanz, James and Lipton, Eric. ''City in the Sky: The Rise and Fall of the World Trade Center''. New York: Times Books, 2004. ISBN 978-0-80507-691-2.
* Griffin, John Ignatius. ''Port of New York''. New York: Arco Press, 1959.
* Hallion, Richard P. ''Test Pilots: The Frontiersmen of Flight''. New York: Doubleday & Company, 1981. ISBN 0-385-15469-0.
* Hammel, Eric.''Aces Against Germany: The American Aces Speak''. Novato, California: Presidio Press, 1993.ISBN 978-0-93555-361-1.
* O'Leary, Michael. ''USAAF Fighters of World War Two in Action''. London: Blandford Press, 1986. ISBN 0-7137-1839-0.
* Orriss, Bruce. ''When Hollywood Ruled the Skies: The Aviation Film Classics of World War II''. Hawthorne, California: Aero Associates Inc., 1984. ISBN 0-9613088-0-X.
{{Refend}}

==External links==
* [http://thetartanterror.blogspot.co.uk/2011/04/curtiss-test-pilots.html Curtiss test pilots]
* [http://thetartanterror.blogspot.ca/2008/05/herbert-o-fisher-1909-1990.html Test & Research Pilots, Flight Engineers: Herbert O Fisher 1909–1990]
* [http://wnyaerospace.org/?p=500 Herbert O. Fisher, Curtiss-Wright test pilot, Buffalo, NY, 1941: Ira G. Ross/Niagara Aerospace Museum Hall of Fame]

{{DEFAULTSORT:Fisher, Herbert O.}}
[[Category:1909 births]]
[[Category:1990 deaths]]
[[Category:American test pilots]]
[[Category:Aviation history of the United States]]
[[Category:People from Buffalo, New York]]
[[Category:People from Caldwell, New Jersey]]
[[Category:People from Kinnelon, New Jersey]]
[[Category:Recipients of the Air Medal]]
[[Category:People from Tonawanda, New York]]