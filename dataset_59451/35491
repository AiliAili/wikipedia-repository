{{Use mdy dates|date=October 2013}}
{{Infobox baseball game
|year       = 2013
|game       = American League Wild Card tie-breaker game
|image      = 
|caption    = Rangers Ballpark in Arlington played host to the 2013 American League Wild Card tie-breaker game
|date       = September 30, 2013
|venue      = [[Rangers Ballpark in Arlington]]
|city       = [[Arlington, Texas]]
|visitor    = [[2013 Tampa Bay Rays season|Tampa Bay Rays]]
|home       = [[2013 Texas Rangers season|Texas Rangers]]
|attendance = 42,796
|umpires    = [[Tim Welke]] (crew chief), [[Jeff Kellogg]], [[Bruce Dreckman]], [[Chris Guccione (umpire)|Chris Guccione]], [[Tom Hallion]], [[Ron Kulpa]]
|television    = [[TBS (U.S. TV channel)|TBS]]
|tv_announcers    = [[Brian Anderson (broadcaster)|Brian Anderson]], [[John Smoltz]] and [[Joe Simpson (baseball)|Joe Simpson]]
}}
The '''2013 American League Wild Card tie-breaker game''' was a [[one-game playoff|one-game extension]] to [[Major League Baseball]]'s (MLB) [[2013 Major League Baseball season|2013 regular season]], played between the [[Texas Rangers (baseball)|Texas Rangers]] and [[Tampa Bay Rays]] to determine the second participant in the [[2013 American League Wild Card Game|2013 American League (AL) Wild Card Game]]. It was played at the [[Rangers Ballpark in Arlington]] on September 30, 2013. The Rays defeated the Rangers, 5–2, and advanced to the AL Wild Card Game against the [[Cleveland Indians]] at [[Progressive Field]], which they won 4–0; the Rangers failed to qualify for the postseason.

The tie-breaker game was necessary after both teams finished the season with [[Winning percentage|win–loss records]] of 91–71 and thus tied for the second [[Major League Baseball wild card|Wild Card]] position in the AL. The Rangers were awarded [[home advantage|home field]] for the game, as they won the regular season series against the Rays, four-games-to-three. The game was televised on [[Major League Baseball on TBS|TBS]].<ref>{{cite web |url=http://mlb.mlb.com/news/article/mlb/pulse-of-the-postseason-cleveland-indians-in-tampa-bay-rays-texas-rangers-head-to-tiebreaker?ymd=20130929&content_id=62125574&vkey=news_mlb |title=Pulse: Tribe in as Rays, Rangers head to tiebreaker |last1=Berry |first1=Adam |date=September 29, 2013 |publisher=[[MLB.com]] |accessdate=September 30, 2013}}</ref> It was the [[List of Major League Baseball tie-breakers|fourth tie-breaker in MLB history]] for a Wild Card spot, although it was the first since MLB adopted its current format of two Wild Card teams playing in a [[Major League Baseball Wild Card game|Wild Card Game]] in 2012. The tie-breaker counted as the 163rd regular season game for both teams, with all events in the game added to regular season statistics.

==Background==
{{main article|2013 Tampa Bay Rays season|2013 Texas Rangers season}}
In Major League Baseball the two teams with the best record in each league who do not win a [[Major League Baseball division winners|division]] play in the [[Major League Baseball Wild Card Game|Wild Card Game]]. A number of teams were in competition for these Wild Card spots, along with their divisional competition. The Rangers spent over 80 days leading the [[American League West]] and shared the lead as late as September 4.<ref name="rangersseason">{{cite web|url=http://www.baseball-reference.com/teams/TEX/2013-schedule-scores.shtml|title=2013 Texas Rangers Schedule, Box Scores, and Splits|work=[[Baseball-Reference.com]]|accessdate=September 30, 2013}}</ref> The Rays spent only a few days leading the [[American League East]], but held a share of the lead as late as August 24.<ref name="raysseason">{{cite web|url=http://www.baseball-reference.com/teams/TBR/2013-schedule-scores.shtml|title=2013 Tampa Bay Rays Schedule, Box Scores, and Splits|work=[[Baseball-Reference.com]]|accessdate=September 30, 2013}}</ref> The Cleveland Indians did not lead the [[American League Central]] after July 2 but remained close throughout the season and ultimately finished just a single game back of the Central champion [[Detroit Tigers]].<ref name="indiansseason">{{cite web|url=http://www.baseball-reference.com/teams/CLE/2013-schedule-scores.shtml|title=2013 Cleveland Indians Schedule, Box Scores, and Splits|work=[[Baseball-Reference.com]]|accessdate=September 30, 2013}}</ref>

Although other teams including the [[Kansas City Royals]], [[Baltimore Orioles]], and [[New York Yankees]] had vied for a Wild Card spot, the Indians, Rangers, and Rays all remained in contention until the end of the season. Entering the final day of the scheduled regular season, on which all three teams played, the Indians had a 91–70 record while both the Rangers and Rays had 90–71 records.<ref name="lastday">{{cite web|url=http://www.baseball-reference.com/games/standings.cgi?date=2013-09-28|title=Standings on Saturday, September 28, 2013|work=[[Baseball-Reference.com]]|accessdate=September 30, 2013}}</ref> These were the best non-division-leading records in the American League.<ref name="lastday"/> Thus, the possibility existed (had the Indians lost and the Rays and Rangers won) for a three-way tie for the two Wild Card spots, which would have required several tie-breaker games to settle.<ref>{{cite web|url=http://mlb.si.com/2013/09/29/crunching-the-numbers-al-wild-card/|title=Crunching the numbers on the AL three-way tie scenario|work=[[Sports Illustrated]]|date=September 29, 2013|first=Jay|last=Jaffe|accessdate=September 30, 2013}}</ref> However, all three teams won, leaving the Indians definitively in the Wild Card Game at 92–70 and the Rays and Rangers tied at 91–71 for the second spot.<ref>{{cite web|url=http://www.latimes.com/sports/sportsnow/la-sp-sn-mlb-playoffs-dodgers-rays-rangers-tiebreaker-20130929,0,3065784.story|title=MLB playoffs: Rays, Rangers to play Monday tiebreaker|work=[[Los Angeles Times]]|date=September 29, 2013|first=Bill|last=Shaikin|accessdate=September 30, 2013}}</ref>

The Indians finished the season strong, winning their last 10 games to clinch their Wild Card berth.<ref name="indiansseason"/> The Rays were 16–12 in September, winning 8 of their last 10.<ref name="raysseason"/> The Rangers were just 12–15 in September, although they also won eight of their final 10 games.<ref name="rangersseason"/> Home field advantage for the tie-breaker game was awarded to the Rangers, as they had won the season series against the Rays 4 games to 3.<ref>{{cite web|url=http://www.usatoday.com/story/sports/mlb/rangers/2013/09/29/texas-rangers-one-game-playoff-wild-card/2892719/|title=Rangers finish strong, force one-game playoff vs. Rays|work=[[USA Today]]|date=September 29, 2013|first=Jeff|last=Miller|accessdate=September 30, 2013}}</ref>

==Game summary==
[[File:EvanLongoria.jpg|thumb|upright|right|alt=A man in a gray and blue baseball uniform with the letters "TB" on his cap holds a baseball in his glove, preparing to throw.|Evan Longoria (pictured here in 2008) hit a two-run home run in the third inning]]
{{Linescore
| Date = September 30, 2013
| Time = 8:07 p.m.
| Location = [[Rangers Ballpark in Arlington]]
| Road = '''Tampa Bay Rays''' | RoadAbr = TB
| R1=1|R2=0|R3=2|R4=0|R5=0|R6=1|R7=0|R8=0|R9=1|RR=5|RH=7|RE=0
| Home = Texas Rangers | HomeAbr = TEX
| H1=0|H2=0|H3=1|H4=0|H5=0|H6=1|H7=0|H8=0|H9=0|HR=2|HH=7|HE=1
| WP = [[David Price (baseball)|David Price]] (10–8)
| LP = [[Martín Pérez (baseball)|Martín Pérez]] (10–6)
| SV = 
| RoadHR = [[Evan Longoria]] (32)
| HomeHR = 
}}
	
[[Desmond Jennings]] opened the first inning with a single, but was thrown out at second base trying to [[Baseball terms#stretch a hit|stretch the hit]] into a double. [[Wil Myers]] then walked, advanced to third base on singles by [[Ben Zobrist]] and [[Evan Longoria]], and finally scored on a [[sacrifice fly]] by [[Delmon Young]]. Rays starter [[David Price (baseball)|David Price]] struck out leadoff batter [[Ian Kinsler]], then allowed a walk to [[Elvis Andrus]] but picked him off and finished the inning by retiring [[Alex Ríos]]. The score remained 1–0 until the top of the third inning, when Jennings drew a walk and scored on a home run by [[Evan Longoria|Longoria]] to give the Rays a 3–0 lead. The Rangers struck right back in the bottom half, as [[Craig Gentry]] led off the inning with a single. After advancing to second on a [[Leonys Martín]] groundout, he scored on a single to right field by Kinsler. The Rays scored again in the sixth, as Longoria doubled to lead off the inning, and advanced to third base on a groundout by Young. The next batter, [[David DeJesus]], hit a double to right field that scored Longoria and put the Rays ahead, 4–1. Rangers [[Relief pitcher|reliever]] [[Alexi Ogando]] entered the game with one out and recorded the final two outs to end the inning.<ref name="gamesumm">{{cite web|url=http://www.baseball-reference.com/boxes/TEX/TEX201309300.shtml|title=September 30, 2013 Tampa Bay Rays at Texas Rangers Box Score and Play by Play|work=[[Baseball-Reference.com]]|accessdate=October 1, 2013}}</ref>

After a single and a stolen base from Andrus, Ríos doubled in the bottom half of the sixth to cut the score to 4–2. A small controversy arose in the top of the seventh inning. Longoria and Myers were on first and second base respectively with two outs when Young hit a [[line drive]] to center field. Replays showed that the ball bounced into Leonys Martín's (the Rangers' [[center fielder]]) glove after hitting the ground, making it a [[Glossary of baseball terms#trapped|trap]] and therefore should have been a hit.<ref name="call">{{cite web|url=https://sports.yahoo.com/blogs/mlb-big-league-stew/blown-call-delmon-young-line-drive-costs-rays-025329185--mlb.html|title=Blown call on Delmon Young line drive costs Rays a run in AL tiebreaker game|work=[[Yahoo! Sports]]|date=September 30, 2013|first=Mike|last=Oz|accessdate=September 30, 2013}}</ref> However, the umpires ruled the play an out, ending the inning without a run scoring. Ultimately, the issue did not affect the outcome. The Rays added onto their lead in the ninth inning when [[Sam Fuld]] stole third and a scored on a throwing error from Rangers reliever [[Tanner Scheppers]], extending their lead to 5–2. Price closed the game in the ninth, recording three straight outs and finishing off a [[complete game]].<ref name="gamesumm"/>

==Aftermath==
[[File:David Price on August 19, 2013.jpg|thumb|right|alt=A man in gray pants, a blue baseball jersey with 'Rays' on the chest, and a blue baseball cap is in the process of pitching a baseball with his left hand.|David Price (pictured here in the regular season) threw a complete game, the first in a tie-breaker since 1999.]]
[[David Price (baseball)|David Price]] recorded the first complete game in a tie-breaker game since [[Al Leiter]] in [[1999 National League Wild Card tie-breaker game|1999]].<ref>{{cite web|url=http://www.nydailynews.com/sports/baseball/price-ace-pitches-complete-game-gem-rays-1-game-playoff-article-1.1472270|title=David Price throws complete game as Rays beat Rangers in one-game playoff, face Indians next|work=[[New York Daily News]]|agency=[[Associated Press]]|date=September 30, 2013|accessdate=September 30, 2013}}</ref> The game counted as a regular season game in [[baseball statistics]]. For example, [[Evan Longoria]]'s third-inning home run broke [[Stan Musial]]'s record for the most home runs in the last game of the season, setting the mark at seven.<ref name="longorecord">{{cite web |url=http://mlb.mlb.com/news/article/mlb/rays-evan-longoria-delivers-when-it-matters-most?ymd=20130930&content_id=62258510&vkey=news_mlb |title=Longoria delivers when it matters most |last1=Gonzalez |first1=Alden |date=October 1, 2013 |publisher=[[MLB.com]] |accessdate=October 2, 2013}}</ref> He went 3-for-4 with a double, a home run, and two [[Runs batted in|RBI]] in the game overall. This left him 11-for-19 with seven home runs and ten RBIs in season finales from 2009–2013.<ref name="longorecord"/> Tampa Bay's win clinched the team's fourth post-season berth in franchise history.<ref name="franchise">{{cite web|url=http://www.baseball-reference.com/teams/TBD/ |title=Tampa Bay Rays Team History & Encyclopedia |work=[[Baseball-Reference.com]] |accessdate=September 30, 2013 |deadurl=yes |archiveurl=http://www.webcitation.org/5lgXtbJui?url=http%3A%2F%2Fwww.baseball-reference.com%2Fteams%2FTBD%2F |archivedate=December 1, 2009 |df=mdy }}</ref> The Rays played the [[Cleveland Indians]] in the [[2013 American League Wild Card Game|American League Wild Card Game]] and advanced to the [[2013 American League Division Series|American League Division Series]] (ALDS) with a 4-0 win. However, the Rays lost to the [[Boston Red Sox]] in the ALDS, 3 games to 1.<ref>{{cite news |url=http://scores.espn.go.com/mlb/recap?gameId=331008130 |title=Red Sox rebound, push through Rays late to reach ALCS |publisher=[[ESPN]] |date=October 8, 2013 |accessdate=January 3, 2014}}</ref>

==References==
;General
{{refbegin}}
*{{cite web|url=http://www.baseball-reference.com/boxes/TEX/TEX201309300.shtml|title=September 30, 2013 Tampa Bay Rays at Texas Rangers Box Score and Play by Play|work=[[Baseball-Reference.com]]|accessdate=October 1, 2013}}
{{refend}}
;Specific
{{reflist|30em}}

{{Navboxes|list1=
{{MLBtiebreaker}}
{{MLB on TBS}}
{{MLB on ESPN Radio}}
{{Texas Rangers}}
{{Tampa Bay Rays}}
}}

{{Good article}}

[[Category:2013 Major League Baseball season|American League Wild Card tie-breaker game]]
[[Category:Texas Rangers (baseball)]]
[[Category:Tampa Bay Rays]]
[[Category:Major League Baseball tie-breaker games]]
[[Category:2013 in sports in Texas]]
[[Category:21st century in Arlington, Texas]]
[[Category:Baseball in the Dallas–Fort Worth metroplex]]
[[Category:September 2013 sports events]]