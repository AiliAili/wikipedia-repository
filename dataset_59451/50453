{{Infobox journal
| title = American Journal of Medical Quality 
| cover = [[File:American Journal of Medical Quality.tif]]
| editor = David B. Nash
| discipline = [[Quality control]] in [[medicine]]
| former_names = 
| abbreviation = Am. J. Med. Qual.
| publisher = [[Sage Publications]]
| country =
| frequency = Bimonthly
| history = 1992-present
| openaccess = 
| license = 
| impact = 1.252
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201749/title
| link1 = http://ajm.sagepub.com/content/current
| link1-name = Online access
| link2 = http://ajm.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 25797414
| LCCN = 94660087
| CODEN = 
| ISSN = 1062-8606 
| eISSN = 1555-824X 
}}
The '''''American Journal of Medical Quality''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering [[quality control]] in [[medicine]]. The [[editor-in-chief]] is David B. Nash ([[Jefferson School of Population Health]]). It was established in 1986 and is published by [[Sage Publications]] in association with the [[American College of Medical Quality]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Science Citation Index Expanded]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 1.252.<ref name=WoS>{{cite book |year=2015 |chapter=American Journal of Medical Quality |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201749/title}}
* [http://www.acmq.org/website American College of Medical Quality]

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Healthcare journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1992]]