<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Asso XI.RC
 | image=Isotta Fraschini XI R. 2 C. 15.jpg
 | caption=
}}{{Infobox Aircraft Engine
 |type=V-12 water-cooled piston aircraft engine
 |manufacturer=[[Isotta Fraschini]], [[Milan]]
 |designer=
 |national origin=[[Italy]]
 |first run=early 1930s
 |major applications=[[CANT Z.501]], [[CANT Z.1007]]
 |produced=
 |number built=
 |program cost=
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}
The '''Isotta Fraschini Asso XI''' family of engines were [[Italy|Italian]] [[internal combustion engine cooling|water-cooled]], [[supercharger|supercharged]] [[V12 engine|V12]] piston [[aeroengine]]s produced in the 1930s, powering the [[CANT Z.501]] and [[CANT Z.1007]] and several prototype aircraft.

==Design and development==
Isotta Fraschini produced a long series of engines with the name Asso (Ace, in English). The Asso XI.RC was an upright, liquid-cooled V-12 engine with maximum power output in the range 670-725&nbsp;kW (900-970&nbsp;hp) depending on the degree of supercharging.  There were two variants, differing only in their supercharger speed: the '''R.C.40''' ran at a little over 10 times the crankshaft speed and enabled the engine to maintain a [[rated power]] of 623&nbsp;kW (836&nbsp;hp) to an altitude of 4,000&nbsp;m (13,120&nbsp;ft) (hence the ''40'' in the variant name) whereas the '''R.2C.15''' held 655&nbsp;kW (880&nbsp;hp) to 1,500&nbsp;m (4,920&nbsp;ft) with a supercharger gearing of 7.<ref name=JAWA38/>

All variants of the RC.40 had much in common constructionally with the earlier [[Isotta Fraschini Asso-750.RC|Asso-750.RC]], though they had two, rather than three, banks of 6 cylinders and 4 rather than 2 valves per cylinder.  [[Cylinder (engine)|Cylinder]] barrels were machined from [[carbon steel]] with flat-topped heads and [[valve seat]]ings. Each barrel had a separate sheet steel water jacket.  Cast aluminium head blocks were bolted to each of the two banks of 6 cylinders, providing valve ports, guides, coolant passages and [[camshaft]] supports.  The pistons were also aluminium castings.  The [[crankshaft]] was a 6-throw design with 8 plain bearings and a double row [[ball bearing|ball thrust bearing]] between the front two.  The [[connecting rods]] had bronze bush [[little end]]s and white metal [[big end]]s.  The [[crankcase]] was cast in two parts, the upper one with the housings for the crankshaft bearings.<ref name=JAWA38/>
<!-- ==Operational history== -->

==Variants==
;R
;R.C.
;R.C.15
;R.2C.15
:Supercharger speed 7 times  crankshaft; rated power at 1,500 m.
;R.C.40
:Supercharger speed 75/7 times crankshaft; rated power at 4,000 m.
;L.121 R.C.40:A version of the Asso XI,  671&nbsp;kW (900&nbsp;hp)
;A.120 R.C.40: Inverted version of the L.121

==Applications==
*[[CANT Z.501]]
*[[CANT Z.505]]
*[[CANT Z.508]]
*[[CANT Z.1007]]
*[[CANT Z.1011]]
*[[Caproni Ca.124|Caproni Ca.124 idro]]
*[[Caproni Ca.134]]
*[[Caproni Ca.135]]
*[[Caproni Ca.405]]
*[[IMAM Ro.45]]
*[[Piaggio P.32]]
*[[Piaggio P.50]]
*[[Ambrosini S.S.4|SAI Ambrosini S.S.4]]
<!-- Survivors -->
<!-- Engines on display== -->

==Specifications (RC.40)==
<!-- choose one to copy here -->
{{pistonspecs|
For additional lines, end your alt units with ) and start a new, fully formatted line with * -->
|ref=Jane's All the World's Aircraft 1938<ref name=JAWA38/>
|type=12-cylinder supercharged water-cooled 60° [[V12 engine|V-12]] piston aircraft engine
|bore=146 mm (5.75 in)
|stroke=160 mm (6.30 in)
|displacement=32.65 L (1,992 in<sup>3</sup>)
|length=2,128 mm (83.78 in)
|diameter=
|width=834 mm (32.83 in)
|height=1,106 mm (43.54 in)
|weight=594 kg (1,310 lb)
|valvetrain=Two intake and two exhaust valves per cylinder, drive via cams and T-shaped tappets from inlet and exhaust pairs of overhead camshafts geared together, one for each bank of 6 cylinders, each pair driven by its own shaft connected indirectly to the [[crankshaft]] [[bevel gear]].  
|supercharger=Single speed, single stage centrifugal, running at 10.714 times crankshaft speed.
|turbocharger=
|fuelsystem=4×Isotta Frachini L.70C [[carburetter#Principles|updraft carburreters]], 2 for each bank of 6 cylinders.
|fueltype=
|oilsystem=[[Dry sump]] with single pressure and scavenge pump shaft driven off crankshaft bevel gear.
|coolingsystem=Water-cooled, pump shaft driven off crankshaft bevel gear.
|power=[[Rated power]] 623 kW (836 hp) at 2,250&nbsp;rpm at 4,000 m (13,120 ft); take off power 596&nbsp;kW (880&nbsp;hp) at 2,250&nbsp;rpm
|specpower=Rated 19.1 kW/L (0.42 hp/in<sup>3</sup>)
|compression=6.4:1
|fuelcon=At Rated output, 194&nbsp;kg/h (428 lb/hr)
|specfuelcon= 0.312 kg/(kW·h) (0.512 lb/(hp.hr))
|oilcon=Cruise: 8&nbsp;kg/h (17.6 lb/hr)
|power/weight=1.05 kW/kg (0.64 hp/lb)

|designer=
|reduction_gear=0.743 or 0.512

|general_other=
|components_other=Compressed air starter
|performance_other=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists= 
*[[List of aircraft engines]]
}}

==References==
{{Commons category}}
{{Reflist|refs=

<ref name=JAWA38>{{cite book |title= Jane's All the World's Aircraft 1938|last= Grey |first= C.G. |coauthors= |edition= |year=1972|publisher= David & Charles|location= London|isbn=0-7153-5734-4|page=72d }}</ref>

}}
<!-- ==External links== -->
{{Isotta Fraschini aeroengines}}


[[Category:Isotta Fraschini aircraft engines|Asso XI]]
[[Category:Aircraft piston engines 1930–1939]]