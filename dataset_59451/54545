{{Infobox book
| name          = Clovis Dardentor
| title_orig    = Clovis Dardentor
| translator    =
| image         = 'Clovis Dardentor' by Jules Verne, UK edition book cover.jpg
| caption = Cover from UK Edition
| author        = [[Jules Verne]]
| illustrator   = [[Léon Benett]]
| cover_artist  =
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #43
| subject       =
| genre         = [[Adventure novel]]
| publisher     = [[Pierre-Jules Hetzel]]
| pub_date      = 1896
| english_pub_date = 1897
| media_type    = Print ([[Hardcover|Hardback]])
| pages         =
| oclc          =
| preceded_by   = [[Facing the Flag]]
| followed_by   = [[An Antarctic Mystery]]
}}

'''''Clovis Dardentor''''' is an 1896 fiction novel by French writer [[Jules Verne]], written partly as a travel narrative. Compared to other Verne novels, it is a relatively unknown work.

Very common throughout ''Clovis Dardentor'' is Verne's usage of a comedic, slightly [[burlesque]] tone in the narration and in the characters' [[dialogue]]s (something which the [[narrator]] confirms at a certain point of the novel).

The original illustrations were drawn by designer [[Léon Benett]].

==Plot summary==

The novel tells the story of two cousins, Jean Taconnat and Marcel Lornans, travelling from [[Cette]], France to [[Oran]], [[Algeria]], with the purpose of enlisting in the 5th [[regiment]] of the ''Chasseurs D'Afrique''.

On board the ''Argelès'', the ship to Oran, they meet Clovis Dardentor, a wealthy [[industrialist]], who is the central character of the novel. Jean and Marcel, whose desire to travel to Africa arises from their pursuit of financial independence, find out that Clovis &mdash;an unmarried man, with no family&mdash; has left no heirs to his fortune.

Yet Marcel, well-versed in the Law, knows that any person who were to save Clovis' life either from a fight, from drowning, or from a fire, would forcibly have to be adopted by Clovis. The cousins come to a plan: They will find a way to save Clovis' life, so that he will indeed be legally required to adopt them.

Ironically, it is Clovis who finally saves the cousins' lives: Marcel is saved from a fire, and Jean is saved from drowning.

Eventually, while Jean continues to look for the opportunity to save Clovis' life, Marcel falls in love with Louise Elissane, the prospective daughter-in-law of one of Clovis' acquaintances, the unpleasant Desirandelle family. Louise becomes a key character in the novel, for it is she who saves Clovis Dardentor's life.

Fortunately for the cousins, in the end, Louise is adopted by Clovis, and marries Marcel.

==Publication history==

''Clovis Dardentor'' was first published in France in 1896 and in 1897 the first British edition, fully illustrated, was published by Sampson Low, Marston, and Company. The book was not published in the U.S. until 2008 <ref>
{{cite book |title=Clovis Dardentor |last=Verne |first=Jules |publisher=Choptank Press |location=St. Michaels, MD |year=2008 |pages=320 }}</ref> when the Choptank Press of St Michaels, Maryland re-published the Sampson Low version in a fully illustrated replica edition as a  [[Lulu Press]] book.<ref>http://www.ibiblio.org/pub/docs/books/sherwood/Verne-COLLECTOR-1.htm</ref>

==Film adaptation==

Three British film-makers are currently in the development stages of ''Killing Clovis Dardentor'', an adaptation of the book, funding it through the site [www.buyacredit.com].<ref>{{cite web |first=Helen |last=Pidd |url=https://www.theguardian.com/film/2009/apr/14/clovis-dardentor-film-funding |title=Teenagers' credit note approach to fund £1m film of Clovis Dardentor |work=[[The Guardian]] |date=April 14, 2009 |accessdate=July 26, 2013 }}</ref> Writer [[Lizzie Hopley]] has written the screenplay for the movie.{{citation needed|date=July 2013}}

==References==
{{Reflist}}

==External links==
* {{Internet Archive|clovisdardentor00vernuoft}}
* {{IMDb title|1462032|Killing Clovis Dardentor}}

{{commons category}}

{{Verne}}

{{Authority control}}

[[Category:1896 novels]]
[[Category:Novels by Jules Verne]]


{{1890s-novel-stub}}