{{Distinguish2|the journal of the same name published 1975-1980 by Elsevier<ref>WorldCat listing for {{OCLC|499436755}}</ref>}}
{{Infobox journal
| title = Journal of Molecular Medicine
| cover = [[File:Journal of Molecular Medicine cover.jpg]]
| editor = D. Ganten; G.L. Semenza; W. Rosenthal
| discipline = Human molecular biology and pathophysiology
| formernames = Klinische Wochenschrift, The Clinical Investigator
| abbreviation = J. Mol. Med.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Monthly
| history = 1864-present
| openaccess = 
| license = 
| impact = 5.107
| impact-year = 2014
| website = http://www.springer.com/biomed/molecular/journal/109
| link1 = 
| link1-name = 
| link2 = http://www.springerlink.com/content/100437/
| link2-name = Online access
| JSTOR = 
| OCLC = 32140699
| LCCN = sn95038252
| CODEN = JMLME8
| ISSN = 0946-2716 
| eISSN = 1432-1440
}}
The '''''Journal of Molecular Medicine''''' is a monthly, [[peer-reviewed]], [[scientific journal]] published by [[Springer Science+Business Media]]. It covers all aspects of [[human biology]] and [[pathophysiology]]. The emphasis is on the progress and precision now possible in the understanding, prevention, diagnosis and treatment of [[disease|human diseases]]. The employment of [[molecular biology]] and [[genetic engineering|gene technology]] has enhanced the understanding of [[disease|human diseases]] and has created a new branch of research known as "[[molecular medicine]]".

The journal is the continuation of the ''Klinische Wochenschrift'' (''Journal for Clinical Medicine'') established in 1864 and published by Springer for the "[[Gesellschaft Deutscher Naturforscher und Ärzte]]". From 1992 to 1994, the journal's title was ''The Clinical Investigator''.<ref name=JoMM>{{cite web |url=http://www.springer.com/biomed/molecular/journal/109 |title=springer.com - About this journal |publisher=Springer |work=Journal of Molecular Medicine |accessdate=2010-12-20}}</ref><ref name=SL>{{cite web |url=http://www.springerlink.com/content/0946-2716/1/ |title=SpringerLink - About this journal |publisher=Springer |work=Journal of Molecular Medicine |accessdate=2010-12-20}}</ref> It covers research in the areas of molecular and [[translational medicine]] and publishes original papers, review articles and correspondence pertinent to all aspects of human biology and [[pathophysiology]].

==References==
{{reflist}}

==External links==
* {{Official|http://www.springerlink.com/content/100437/}}

[[Category:General medical journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1994]]
[[Category:Monthly journals]]
[[Category:Springer Science+Business Media academic journals]]