{{Taxobox
| image = Myriostoma coliforme.jpg
| image_width = 240px
| image_caption =
| regnum = [[Fungi]]
| divisio = [[Basidiomycota]]
| classis = [[Agaricomycetes]]
| ordo = [[Geastrales]]
| familia = [[Geastraceae]]
| genus = '''''Myriostoma'''''
| genus_authority = [[Nicaise Auguste Desvaux|Desv.]] (1809)
| type_species = '''''Myriostoma coliforme'''''
| type_species_authority = ([[James Dickson (botanist)|Dicks.]]) [[August Carl Joseph Corda|Corda]] (1842)
| synonyms_ref = <ref name="urlFungorum synonymy: Myriostoma"/>
| synonyms = '''Species'''
*''Lycoperdon coliforme'' <small>Dicks. (1785)</small>
*''Geastrum coliforme'' <small>(Dicks.) [[Christian Hendrik Persoon|Pers.]] (1801)</small>
*''Myriostoma anglicum'' <small>Desv. (1809)</small>
*''Polystoma coliforme'' <small>(Dicks.) [[Samuel Frederick Gray|Gray]] (1821)</small>
*''Myriostoma coliforme'' <small>(Dicks.) Corda (1842)</small>
*''Geastrum columnatum'' <small>[[Joseph-Henri Léveillé|Lév.]] (1846)</small>
*''Bovistoides simplex'' <small>[[Curtis Gates Lloyd|Lloyd]] (1919)</small>
'''Genera'''
*''Bovistoides'' <small>Lloyd (1919)</small>
*''Polystoma'' <small>Gray (1821)</small>
}}
'''''Myriostoma''''' is a fungal [[genus]] in the [[family (biology)|family]] [[Geastraceae]]. The genus is [[monotypic]], containing the single species '''''Myriostoma coliforme'''''. It is an [[Geastrales|earthstar]], so named because the [[spore]]-bearing sack's outer wall splits open into the shape of a star. The inedible fungus has a [[cosmopolitan distribution]], and has been found in Africa, Asia, North and South America, and Europe, where it grows in [[humus]]-rich forests or in [[woodland]]s, especially on well-drained and sandy soils. A somewhat rare fungus, it appears on the [[Regional Red List|Red Lists]] of 12 European countries, and in 2004 it was one of 33 species proposed for [[protected species|protection]] under the [[Convention on the Conservation of European Wildlife and Natural Habitats|Bern Convention]] by the European Council for Conservation of Fungi.

The [[Sporocarp (fungi)|fruit body]], initially shaped like a [[puffball]], is encased within an outer covering that splits open from the top to form rays. These rays curve down to expose an inner papery spore case, which contains the fertile [[spore]]-bearing tissue, the [[gleba]]. The fungus is unique among the earthstars in having a spore case that is supported by multiple stalks, and is perforated by several small holes suggestive of its [[common name]]s '''salt-shaker earthstar''' and '''pepperpot'''. It is the largest of the earthstar fungi, and reaches diameters of up to {{convert|12|cm|in|abbr=on}}. Its spherical spores have elongated warts that create a ridge-like pattern on their surface. The spores are dispersed when falling water hits the outer wall of the spore sac, creating puffs of air that force the spores through the holes.

==Taxonomy and phylogeny==

The species was first mentioned in the scientific literature by [[Samuel Doody]] in the second edition of [[John Ray]]'s ''Synopsis methodica Stirpium Brittanicorum'' in 1696.<ref name=PoncedeLeon1968/> Doody briefly described the mushroom like so: "fungus pulverulentus coli instar perforatus, cum volva stellata" (mushroom dusty, like a perforated colander, volva star-shaped), and went on to explain that he found it in 1695 in [[Kent]].<ref name=Ray1696/>[[File:Coloured Figures of English Fungi or Mushrooms - t. 313.jpg|thumb|left|Illustration from [[James Sowerby]]'s ''Coloured Figures of English Fungi or Mushrooms'' (1803)]]It was first [[species description|described scientifically]] as a new species in 1776 from collections made in England by [[James Dickson (botanist)|James Dickson]], who named it ''Lycoperdon coliforme''. He found it growing in roadside banks and hedgerows among nettles in [[Suffolk]] and [[Norfolk]].<ref name=Dickson1776/> [[Nicaise Auguste Desvaux]] first defined and published the new genus ''Myriostoma'' in 1809, with the species renamed ''Myriostoma anglicum'' (an [[illegitimate name|illegitimate renaming]]).<ref name=Desvaux1809/> [[Christian Hendrik Persoon]] had previously placed the species in ''[[Geastrum]]'' in 1801,<ref name=Persoon1801/> while [[Samuel Frederick Gray]] would in 1821 describe the genus ''Polystoma'' for it.<ref name=Gray1821/> ''Myriostoma coliforme'' received its current and final name when [[August Carl Joseph Corda]] moved Dickson's name to ''Myriostoma'' in 1842, replacing Desvaux's name.<ref name=Corda1842/>

In North America the fungus began to be reported in the late 19th century, first from [[Colorado]] by [[Charles Horton Peck]], and later from [[Florida]], collected by [[Lucien Underwood]] in 1891; both findings were reported by [[Andrew Price Morgan]] in April 1892.<ref name=Morgan1892/> In 1897, Melville Thurston Cook also reported having collected it the year before from "Albino Beach".<ref name=Cook1897/> [[Curtis Gates Lloyd]] described ''Bovistoides simplex'' from a South African specimen in 1919,<ref name=Lloyd1919/> but in 1942, [[William Henry Long]] examined that specimen and concluded that it was a weathered spore sac of ''M.&nbsp;coliforme'' that had become detached from the outer star-shaped exoperidium.<ref name=Long1942/> This conclusion was confirmed in a later study of the material.<ref name=Suarez1999/>

''Myriostoma'' had been [[classification (biology)|classified]] in the [[Geastraceae]] family until 1973, when British mycologist [[Donald Dring]] placed it in the Astraeaceae{{#tag:ref|Neither the family nor the [[order (biology)|order]] (Sclerodermatales) that Dring placed it in are recognized anymore: ''[[Astraeus (fungus)|Astraeus]]'' is a member of [[Diplocystaceae]] and Sclerodermatales is a synonym of [[Boletales]].<ref>Kirk ''et al.'' (2008), pp. 66, 622.</ref>|group=nb}} based on the presence of trabeculae (stout columns that extend from the [[peridium]] to the central core of the fruit body) in the [[gleba]], and the absence of a true [[hymenium]].<ref name=Dring1973/> In his 1989 [[monograph]], Stellan Sunhede returned it to the Geastraceae.<ref name=Sunhede1989/> [[Molecular phylogenetics|Molecular]] analysis of DNA sequences has confirmed the traditional belief that ''Myriostoma'' and ''Geastrum'' are closely related.<ref name=Kruger2001/><ref name=Hosaka2006/>

[[Václav Jan Staněk]] proposed a [[variety (botany)|variety]] ''capillisporum'' in 1958,<ref name=Stanek1958/> which has been sunk back into [[synonym (biology)|synonymy]] with the species.<ref name="urlFungorum synonymy: Myriostoma"/> ''M.&nbsp;coliforme'' is the sole species in ''Myriostoma'', making the genus [[monotypic]].<ref>Kirk ''et al.'' (2008), p. 456.</ref> Because the original [[type (biology)|type]] material has been lost, in 1989 Sunhede suggested that Dickson's illustration in his 1776 publication (tab. III: 4a & b) be used as the [[lectotype]].<ref name=Sunhede1989/>

The [[botanical name|specific epithet]] is derived from the [[Latin]] words ''colum'', meaning "strainer", and ''forma'', meaning "shape"—Berkeley's [[common name|vernacular]] name "Cullenden puff-ball" also refers to a [[colander]].<ref name=Lloyd1902/> Gray called it the "sievelike pill-box".<ref name=Gray1821/> The generic name is from the Greek words ''μνριός'', meaning "countless" and ''στόμα'', meaning "mouth" (the source of the technical term [[stoma]]).<ref name=Rea1922/> The species is commonly known as the "salt-and-pepper shaker earthstar"<ref name=Hemmes2002/> or simply the "pepperpot".<ref name=Dahlberg2006/>

==Description==
[[File:Myriostoma coliforme 46755.jpg|thumb|right|Brown spores can be seen on the surface of this fruit body.]]
The [[sporocarp (fungus)|fruit bodies]] start their development underground or buried in [[leaf litter|leaf debris]], linked to a strand of [[mycelium]] at the base. As they mature, the exoperidium (the outer tissue layer of the peridium) splits open into 7 to 14 rays which curve backward; this pushes the fruit body above the [[substrate (biology)|substrate]]. Fully opened specimens can reach dimensions of {{convert|2|–|12|cm|in|1|abbr=on}} from ray tip to tip. The rays are of unequal size, with tips that often roll back inward. They comprise three distinct layers of tissue. The inner pseudoparenchymatous layer (so named for the resemblance to the tightly packed cells of plant [[parenchyma]]) is fleshy and thick when fresh, and initially pale beige but darkening to yellow or brown as it matures, often cracking and peeling off in the process. The exterior mycelial layer, often matted with fine leaf debris or dirt, usually cracks to reveal a middle fibrous layer, which is made of densely packed [[hypha]]e 1–2.5&nbsp;[[micrometre|μm]] wide. The base of the fruit body is concave to vaulted in shape, and often covered with adhering dirt. The roughly spherical spore sac (endoperidium) measures {{convert|1|–|5|cm|1|in|abbr=on}} in diameter, and is supported by a cluster of short columns shaped like flattened spheres. It is gray-brown in color, and minutely roughened with small, lightly interconnected warts. There are several to many evenly dispersed mouths, the [[ostiole]]s, mainly on the upper half of the endoperidium. They are roughly circular with fimbriate (fringed) edges.<ref name=Long1948/><ref name=Rees2005/> The inedible fruit bodies have no distinct taste, although dried specimens develop an odor resembling [[curry powder]] or [[bouillon cube]]s.<ref name=Jordan2004/>

Like many earthstars, the fungus uses the force of falling raindrops to help disperse the spores, which are ejected in little bursts when objects (such as rain) strike the outer wall of the [[spore]] sac. The gleba is brown to grayish-brown, with a cotton-like texture that, when compressed, allows the endoperidium to flex quickly and create a puff of air that is forced out through the ostioles. This generates a cloud of spores that can then be carried by the wind.<ref name="urlKew: Myriostoma coliforme"/> There are columellae (sterile structures that start at the base of the gleba and extend through it), which are usually not evident in the mature gleba, but apparent at the base of the spore sac. The columellae are not connected to the ostioles, but rather, terminate within the gleba at some distance from them. The [[capillitia]] (sterile strands within the gleba) are long, slender, free, tapering, unbranched, and 2–5&nbsp;μm thick, with thickened walls. The spores are spherical, [[amyloid (mycology)|nonamyloid]], and are ornamented with irregularly shaped flaring protuberances up to 2&nbsp;μm high. They measure 3.9–4.8&nbsp;μm in diameter (without ornamentation), and 5.4–7.0&nbsp;μm including the ornamentation.<ref name=Rees2005/>{{#tag:ref|[[Scanning electron microscopy]] images of the spores can be viewed on [http://www.cybertruffle.org.uk/cyberliber/59575/0071/0256.htm page 256] of Suarez and Wright (1999).<ref name=Suarez1999/>|group=nb}}

===Similar species===
''Myriostoma coliforme'' is a distinctive species easily characterized by its size—being the largest earthstar fungus—as well as the multiple openings on its spore sack and stalk supporting the sack.<ref name="urlKew: Myriostoma coliforme"/> Historically, it was thought that the holes might have been a result of insects. This was discussed and rejected by [[Thomas Jenkinson Woodward]] in 1797: <blockquote>It has been doubted whether these mouths might not be accidental, and formed by insects after the expansion of the plant. But this (not to mention their regularity, and that each is furrowed by its border of ciliae) is clearly disproved, from the marks of the projections formed by the mouths being seen on the expanded rays, when freshly opened ... I have likewise found an abortive plant, in which the seed did not ripen; but which had numerous projecting papillae on the head, where the mouths should have been formed.<ref name=Woodward1794/></blockquote>

==Habitat and distribution==
[[File:Myriostoma coliforme 9945.jpg|thumb|right|''Myriostoma'' is a [[saprobic]] species.]]
''Myriostoma'' is [[saprobic]], deriving nutrients from decomposing organic matter. Fruit bodies grow grouped in well-drained or sandy soil, often in the partial shade of trees.<ref name=Long1948/> The species occurs in deciduous forests and [[mixed forest]]s, gardens, along hedges and grassy road banks, and grazed grasslands. In the Northern Hemisphere, it tends to grow on well-drained south-facing slopes, while it prefers a similar habitat on north-facing slopes in Australia.<ref name=Sunhede1989/> In Europe, its major habitat is [[Riparian zone|riparian]] mixed forests dominated by ''[[Salix alba]]'' and ''[[Populus alba]]'' along the great rivers.<ref name=Dahlberg2006/> In [[Hawaii]], it has been collected at elevations above {{convert|2000|m|ft|abbr=on}} where it appears to favor the mamame (''[[Sophora chrysophylla]]'') forest.<ref name=Smith1982/>

The species is widespread,{{#tag:ref|European countries and territories in which ''M. coliforme'' has been recorded include Bulgaria,<ref name=Alexov2012/> the [[Channel Islands]],<ref name=Jordan2004/> Czechoslovakia,<ref name=Kotlaba1971/> France,<ref name=Guinberteau2005/> Germany,<ref name=Benkert2003/> Italy,<ref name=Monti2001/> the Netherlands,<ref name=Remijn2007/> Poland,<ref name=Pawlowski2008/> Portugal, Spain,<ref name=SIMIL2006/> Turkey,<ref name=Asan2002/> and Ukraine.<ref name="urlUkraine fungi"/> It has also been found in the [[Canary Islands]],<ref name=Beltran1998/> Morocco (in the [[Mamora forest]]),<ref name=Outcoumit2009/> and in South Africa.<ref name=Sunhede1989/> In South America the fungus has been collected in the [[Galapagos Islands]],<ref name=Reid1980/> Argentina,<ref name=Spegazzini1908/> Brazil,<ref name=Baseia2002/> and Chile.<ref name=Lazo1972/> The North American distribution ranges from Canada to Mexico ([[Los Ajos-Bavispe National Forest Reserve]]).<ref name=Esqueda2009/> In the Middle East, it has been found in Afghanistan,<ref name=Lange1953/> Israel,<ref name=Binyamini1984/> and Iran. It is also known from Australia (where it may be an [[introduced species]]),<ref name=Rees2005/> China,<ref name=Zhou2002/> and India.<ref name=Sunhede1989/>|group=nb}} being known in its natural habitat from all five continents,<ref name=Baseia2002/><ref name=Lange1953/><ref name=Binyamini1984/> but is not found in abundance. ''Myriostoma coliforme'' is rare in Europe, where it appears on the [[Regional Red List]]s of 12 countries, and is one of 33 candidate species for listing in Appendix I of the [[Convention on the Conservation of European Wildlife and Natural Habitats]] (the "Bern Convention").<ref name=Dahlberg2006/> Although originally described from England, it was considered extinct in mainland Britain until it was found again in [[Suffolk]] in 2006 near [[Ipswich]], one of its original localities<ref name="urlKew: Myriostoma coliforme"/>—it had been last reported in the country in 1880.<ref name=Pegler1995/> The fungus is considered extinct in Switzerland.<ref name=ECCF2001/> Its most northerly location is southern Sweden,<ref name=Andersson2010/> although it is generally rare in northern Europe.<ref name="urlKew: Myriostoma coliforme"/> It is similarly widespread but rarely encountered in North America, although there may be isolated localities, like [[New Mexico]], where it is more abundant.<ref name=Arora1986/> In Australia, where its range is limited to the central [[New South Wales]] coast, it may have been [[introduced species|introduced]] from exotic plant material.<ref name=Rees2005/>

==Notes==
<references group="nb" />

==References==
{{Reflist|colwidth=30em|refs=

<ref name=Alexov2012>{{cite journal |vauthors=Alexov R, Vassilev D, Nedelev P, Traikov I |title=New records of seven rare and noteworthy Basidiomycetes from Bulgaria |journal=Trakia Journal of Sciences |year=2012 |volume=10 |issue=2 |pages=10–6 |url=http://tru.uni-sz.bg/tsj/Vol10N2_%202012/R.Alexov.pdf |format=PDF}}</ref>

<ref name=Andersson2010>{{cite journal |author=Andersson U-B. |year=2010 |title=Jordstjärnor i Sverige 5. Fyrflikig jordstjärna, hög jordstjärna, hårig jordstjärna, sålljordstjärna|trans_title=Swedish earthstars 5. Four-footed earthstar, arched earthstar, hairy earthball, salt-shaker earthstar|journal=Svensk Botanisk Tidskrift |volume=104 |issue=1 |pages=39–43 |language=Swedish}}</ref>

<ref name=Arora1986>{{cite book |author=Arora D. |title=Mushrooms Demystified: A Comprehensive Guide to the Fleshy Fungi |publisher=Ten Speed Press |location=Berkeley, California |year=1986 |page=704 |isbn=978-0-89815-169-5}}</ref>

<ref name=Asan2002>{{cite journal |vauthors=Asan A, Sesli E, Gücin F, Stojchev G |title=''Myriostoma coliforme'' and ''Phylloporia ribis'' (Basidiomycetes): First reports from European Turkey |journal=Botanika Chronika |year=2002 |volume=15 |pages=45–9 |issn=0253-6064}}</ref>

<ref name=Baseia2002>{{cite journal |author=Baseia IG. |year=2002 |title=Some interesting gasteromycetes (Basidiomycota) in dry areas from northeastern Brazil |journal=Acta Botanica Brasilica |volume=16 |issue=1 |doi=10.1590/S0102-33062002000100002}} {{Open access}}</ref>

<ref name=Beltran1998>{{cite journal |vauthors=Beltrán Tejera E, Bañares Baudet A, Rodríguez-Armas JL |year=1998 |title=Gasteromycetes on the Canary Islands. Some noteworthy new records |journal=Mycotaxon |volume=67 |pages=439–53 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0067/0439.htm}}</ref>

<ref name=Benkert2003>{{cite journal |author=Benkert D. |year=2003 |title=Berlin und die Mark Brandenburg&nbsp;– ein Paradies für Erdsterne (Geastrales) |trans_title=Berlin and the Mark Brandenburg: a paradise for earthstars (Geastrales) |journal=Verhandlungen des Botanischen Vereins von Berlin und Brandenburg |volume=136 |pages=231–68 |language=German |issn=0724-3111}}</ref>

<ref name=Binyamini1984>{{cite journal |author=Binyamini N. |title=New records of Geastraceae from Israel |journal=Mycologia Helvetica |year=1984 |volume=1 |issue=3 |pages=169–76 |issn=0256-310X}}</ref>

<ref name=Cook1897>{{cite journal |author=Cook MT. |year=1897 |title=''Myriostoma Coliforme'' |journal=Botanical Gazette |volume=23 |issue=1 |pages=43–4 |jstor=2995721 |doi=10.1086/327460}} {{Open access}}</ref>

<ref name=Corda1842>{{cite book |author=Corda ACJ. |year=1842  |title=Anleitung zum Studium der Mykologie |trans_title=Manual for the study of mycology |publisher=Friedrich Ehrlich |location=Prague, Czech Republic |page=lxxxi |language=German |url=https://archive.org/stream/anleitungzumstud00cord#page/n89/mode/2up}}</ref>

<ref name=Dahlberg2006>{{cite book |vauthors=Dahlberg A, Croneborg H |title=The 33 Threatened Fungi in Europe (Nature and Environment) |publisher=Council of Europe |location=Strasbourg, France |year=2006 |page=88 |isbn=978-9-28715-928-1 |url=https://books.google.com/books?id=6mAfxSiGB9MC&pg=PA88}}</ref>

<ref name=Desvaux1809>{{cite journal |author=Desvaux NA. |year=1809 |title=Observations sur quelques genres à établir dans la famille des champignons |trans_title=Comments of a few genera needing creation in the fungus family |journal=Journal de Botanique, Rédigé par une Société di Botanistes |volume=2 |pages=88–105 |language=French |url=https://books.google.com/books?id=Vb0XAAAAYAAJ&pg=PA88}}</ref>

<ref name=Dickson1776>{{cite book |author=Dickson J.|title=Fasciculus Plantarum Cryptogamicarum Britanniae  |year=1785 |publisher=G. Nicol |location=London, UK |volume=1 |page=24 |url=https://books.google.com/books?id=Fc0VAAAAYAAJ&pg=PA24 |language=Latin}}</ref>

<ref name=Dring1973>{{cite book |chapter=Gasteromycetes |title=The Fungi: An Advanced Treatise |author=Dring DM. |year=1973 |volume=IVb |veditors=Ainsworth GC, Sparrow FK, Sussman AS |publisher=Academic Press |location=New York, New York |pages=451–78}}</ref>

<ref name=ECCF2001>{{cite report |author=The European Council for Conservation of Fungi |title=Datasheets of threatened mushrooms of Europe, candidates for listing in Appendix I of the Convention |year=2001 |publisher=Council of Europe |url=https://wcd.coe.int/com.instranet.InstraServlet?command=com.instranet.CmdBlobGet&InstranetImage=1338439&SecMode=1&DocId=1464288&Usage=2 |format=PDF}}</ref>

<ref name=Esqueda2009>{{cite journal |author=Esqueda M. |year=2009 |title=Primeros registros de hongos gasteroides en la Reserva Forestal Nacional y Refugio de Fauna Silvestre Ajos-Bavispe, Sonora, México |trans_title=First records of gasteroid fungi from the Ajos-Bavispe National Forest Reserve and Wildlife Refuge, Sonora, Mexico |journal=Revista Mexicana de Micologia |volume=30 |pages=19–29 |language=Spanish |url=http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S0187-31802009000200003}}</ref>

<ref name=Gray1821>{{cite book |author=Gray SF. |year=1821 |title=A Natural Arrangement of British Plants |volume=1 |publisher=Baldwin, Craddock, and Joy |location=London, UK |page=586 |url=http://biodiversitylibrary.org/page/30087288}}</ref>

<ref name=Guinberteau2005>{{cite journal|author=Guinberteau J. |title=Découverte d'une nouvelle station en Gironde d'un champignon très rare en Aquitaine: ''Myriostoma coliforme'' (With.:Pers.) Corda |journal=Miscellanea Mycologia |year=2005 |volume=84 |pages=6–12 |language=French |issn=0777-7213}}</ref>

<ref name=Hemmes2002>{{cite book |vauthors=Hemmes DE, Desjardin D|title=Mushrooms of Hawai'i: An Identification Guide |publisher=Ten Speed Press |location=Berkeley, California |year=2002 |page=143 |isbn=978-1-58008-339-3 |url=https://books.google.com/books?id=xbEC2PrmZZkC&pg=PA100}}</ref>

<ref name=Hosaka2006>{{cite journal |vauthors=Hosaka K, Bates ST, Beever RE, Castellano MA, Colgan III W, Domingues LS, Nouhra ER, Geml J, Giachini AJ, Kenny SR, Simpson NB, Spatafora JW, Trappe JM |year=2006 |title=Molecular phylogenetics of the gomphoid-phalloid fungi with an establishment of the new subclass Phallomycetidae and two new orders |journal=Mycologia |volume=98 |issue=6 |pages=949–59 |doi=10.3852/mycologia.98.6.949 |jstor=20444784 |pmid=17486971 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0098/006/0949.htm}}</ref>

<ref name=Jordan2004>{{cite book |author=Jordan M. |title=The Encyclopedia of Fungi of Britain and Europe |publisher=Frances Lincoln |location=London, UK |year=2004 |page=362 |isbn=978-0-7112-2378-3 |url=https://books.google.com/books?id=bFMfytLn3bEC&pg=PA362}}</ref>

<ref name=Kotlaba1971>{{cite journal |author=Kotlaba F. |authorlink=František Kotlaba |year=1971 |title=Nová lokalita vzácné břichatky mnohokrčky dírkované-''Myriostoma coliforme'' (With. ex Pers.) Corda-v Československu |trans_title=A new locality for the rare gasteromycete ''Myriostoma coliforme'' in Czechoslovakia |journal=Česká Mykologie |volume=25 |issue=3 |pages=161–4 |language=Czech |url=http://web.natur.cuni.cz/cvsm/CM2535.pdf |format=PDF abstract}}</ref>

<ref name=Kruger2001>{{cite journal |vauthors=Krüger D, Binder M, Fischer M, Kreisel H |year=2001 |title=The Lycoperdales. A molecular approach to the systematics of some gasteroid mushrooms  |journal=Mycologia |volume=93 |issue=5 |pages=947–57 |jstor=3761759 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0093/005/0947.htm |doi=10.2307/3761759}}</ref>

<ref name=Lange1953>{{cite journal |author=Lange M. |year=1953 |title=Some Gasteromycetes from Afghanistan |journal=Svensk Botanisk Tidskrift |volume=50 |pages=79–80}}</ref>

<ref name=Lazo1972>{{cite journal |author=Lazo W. |year=1982 |title=Fungi from Chile I. Some Gasteromycetes and Agaricales |journal=Mycologia |volume=64 |issue=4 |pages=786–98 |jstor=3757933 |url= http://www.cybertruffle.org.uk/cyberliber/59350/0064/004/0786.htm | doi = 10.2307/3757933}}</ref>

<ref name=Long1942>{{cite journal |author=Long WH. |year=1942 |title=Studies in the Gasteromycetes: VI |journal=Mycologia |volume=34 |issue=5 |pages=532–5 |jstor=3754666 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0034/005/0532.htm |doi=10.2307/3754666}}</ref>

<ref name=Long1948>{{cite journal |vauthors=Long WH, Stouffer DJ |year=1948 |title=Studies in the Gasteromycetes XVI. The Geastraceae of the south-western United States |journal=Mycologia |volume=40 |issue=5 |pages=547–85;582 |jstor=3755257 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0040/005/0582.htm |doi=10.2307/3755257}}</ref>

<ref name=Lloyd1902>{{cite journal |author=Lloyd CG. |year=1902 |title=The Geastrae |journal=Bulletin of the Lloyd Library of Botany, Pharmacy and Materia Medica |volume=5 |issue=2 |page=7 |url=https://archive.org/stream/bulletinlloydli00medigoog#page/n36/mode/2up}}</ref>

<ref name=Lloyd1919>{{cite journal |author=Lloyd CG. |year=1919 |title=The genus ''Bovistoides'' |journal=Mycological Notes |volume=6 |issue=61 |page=883}}</ref>

<ref name=Monti2001>{{cite journal |vauthors=Monti G, Tommasi S, Maccioni S |year=2001 |title=Macromiceti rari o nuovi nella Tenuta di San Rossore (pisa): descrizione e osservazioni critiche |trans_title=Rare or new macromycetes in San Rossore (Pisa): Description and critical notes |journal=Micologia Italiana |volume=30 |issue=1 |pages=19–34 |issn=0390-0460 |language=Italian}}</ref>

<ref name=Morgan1892>{{cite journal |author=Morgan AP. |year=1892 |title=''Myriostoma coliformes'' Dicks. in Florida |journal=American Naturalist |volume=26 |issue=304 |pages=341–3 |jstor=2452432 | doi = 10.1086/275521 }} {{Open access}}</ref>

<ref name=Outcoumit2009>{{cite journal |vauthors=Outcoumit A, Touhami AO, Douira A |year=2009 |title=''Myriostoma coliforme'', nouvelle espèce pour la forêt de la Mamora (nord-ouest du Maroc) |trans_title=''Myriostoma coliforme'', a new species for the Mamora forest (northwestern Morocco) |journal=Bulletin Mycologique et Botanique Dauphiné-Savoie |volume=49 |issue=192 |pages=41–6 |language=French |issn=1771-754X}}</ref>

<ref name=Pawlowski2008>{{cite journal |vauthors=Pawłowski B, Adamska E |year=2008 |title=Nowe, najliczniejsze w Polsce stanowisko gwiazdy wieloporowatej Myriostoma coliforme (Dicks.) Corda w Toruniu |trans_title=A new locality of ''Myriostoma coliforme'' (Dicks.) Corda in Toruń with the most abundant occurrence of this species in Poland |journal=Chrońmy Przyrodę Ojczysta |volume=64 |issue=2 |pages=70–6 |issn=0009-6172 |url=http://www.iop.krakow.pl/images/Files/ChPO%20Rocznik%20LXIV%20%2864%29%202008/Zeszyt%202/040302-200802-08.pdf |format=pdf (English abstract) |language=Polish}}</ref>

<ref name=Pegler1995>{{cite book |vauthors=Laessøe T, Pegler DN, Spooner B |title=British Puffballs, Earthstars and Stinkhorns: An Account of the British Gasteroid Fungi |publisher=Royal Botanic Gardens |location=Kew, UK |year=1995 |page=112 |isbn=978-0-947643-81-2}}</ref>

<ref name=Persoon1801>{{cite book |author=Persoon CH. |title=Synopsis Methodica Fungorum |volume=1 |publisher=Dieterich |location=Göttingen, Germany |page=131 |url=https://books.google.com/books?id=zko-AAAAcAAJ&pg=PA131 |language=Latin}}</ref>

<ref name=PoncedeLeon1968>{{cite journal |author=Ponce de León P. |year=1968 |title=A revision of the family Geastraceae |journal=Fieldiana |volume=31 |issue=14 |pages=301–49 |url=http://biodiversitylibrary.org/page/2452205}}</ref>

<ref name=Ray1696>{{cite book |title=Synopsis Methodica Stirpium Britannicarum |edition=2nd |author=Doody S. |editor-last=Ray J |year=1696 |publisher=Impensis Gulielmi & Joannis Innys |location=London, UK |page=340 |url=https://books.google.com/books?id=o-4TAAAAQAAJ&pg=PA340#v=onepage&q&f=false |language=Latin, English}}</ref>

<ref name=Rea1922>{{cite book |title=British Basidiomycetae: A Handbook to the Larger British Fungi |author=Rea C. |year=1922 |publisher=University Press |location=Cambridge, UK |page=39 |url=http://biodiversitylibrary.org/page/17127874}}</ref>

<ref name=Rees2005>{{cite journal |vauthors=Rees BJ, Taker F, Coveny RG |year=2005 |title=''Myriostoma coliforme'' in Australia |journal=Australasian Mycologist |volume=24 |issue=2 |pages=25–8 |url=http://www.australasianmycology.com/pages/pdf/24/2/25.pdf |format=PDF}}</ref>

<ref name=Reid1980>{{cite journal |vauthors=Reid DA, Pegler DN, Spooner BM |year=1980 |title=An annotated list of the fungi of the Galapagos Islands |journal=Kew Bulletin |volume=35 |issue=4 |pages=847–92 |jstor=4110185 |doi=10.2307/4110185}}</ref>

<ref name=Remijn2007>{{cite journal |author=Remijn H. |year=2007 |title=Sterrenparadijs: Hier aan de kust, de Zeeuwse kust |trans_title=A paradise for earthstars |journal=Coolia |volume=50 |issue=4 |pages=177–80 |language=Dutch |url=http://www.mycologen.nl/Coolia/PDF-files/Coolia_50(4).pdf |format=PDF}}</ref>

<ref name=SIMIL2006>{{cite web |author=Hernández-Crespo JC. |title=Distribution map: ''Myriostoma coliforme'' (With.:Pers.) Corda |work=S I M I L. An on line Information System of the Iberian Fungal Diversity |url=http://www.rjb.csic.es/sim/php/Paginas/mapadistribucion.php?busca=Myriostoma&por=gensi&num=267&fichero=censogas&version=english |publisher=Royal Botanic Garden Madrid, C.S.I.C. Research Project Flora Micológica Ibérica I-VI (1990–2008). Spanish Ministry of Education and Science |date=14 February 2007 |accessdate=2012-05-02}}</ref>

<ref name=Smith1982>{{cite journal |vauthors=Smith CW, Ponce de Leon P |year=1982 |title=Hawaiian Geastroid Fungi |journal=Mycologia |volume=74 |issue=5 |pages=712–7 |jstor=3792856 |url=http://www.cybertruffle.org.uk/cyberliber/59350/0074/005/0712.htm |doi=10.2307/3792856}}</ref>

<ref name=Spegazzini1908>{{cite journal |author=Spegazzini C. |year=1908 |title=Mycetes argentinenses (Serie IV) |trans_title=Argentinian mushrooms (5th Series) |journal=Anales del Museo Nacional de Buenos Aires, series 3 |volume=9 |pages=25–34 |url=http://biodiversitylibrary.org/page/14582999 |language=Spanish}}</ref>

<ref name=Stanek1958>{{cite book |title= Flora ČSR, B1: Gasteromycetes, Houby-Břichatky.|trans_title=Flora of the Czechoslovak Republic, B1: Gasteromycetes, Puffballs |language=Czech |author=Pilát A. |year=1958 |publisher=Nakladatelstvi Československé Akademie Vĕd |location=Prague, Czechoslovakia |pages=402, 779}}</ref>

<ref name=Suarez1999>{{cite journal |vauthors=Suárez VL, Wright JE |year=1999 |title=The status of genus ''Bovistoides'' (Gasteromycetes) |journal=Mycotaxon |volume=71 |pages=251–8 |url=http://www.cybertruffle.org.uk/cyberliber/59575/0071/0251.htm}}</ref>

<ref name=Sunhede1989>{{cite book |author=Sunhede S. |title=Geastraceae (Basidiomycotina): Morphology, Ecology, and Systematics with Special Emphasis on the North European Species |year=1989 |series=Synopsis Fungorum, '''1''' |publisher=Fungiflora |location=Oslo, Norway |isbn=978-8290724059}}</ref>

<ref name="urlUkraine fungi">{{cite web |title=Ukrainian fungi recorded more recently than the end of 1990 |url=http://www.cybertruffle.org.uk/redlists/red_1991.htm |work=Lists of Potentially Rare, Endangered or Under-Recorded Fungi in Ukraine |publisher=Cyberliber |veditors=Minter DW, Hayova VP, Minter TJ, Tykhonenko YY}}</ref>

<ref name="urlKew: Myriostoma coliforme">{{cite web |url=http://www.kew.org/plants-fungi/Myriostoma-coliforme.htm |title=Plants & Fungi: ''Myriostoma coliforme'' (pepperpot earthstar) |vauthors=Spooner B, Döring H, Aguirre-Hudson B |work=Species profile from Kew |publisher=Kew Botanical Gardens |accessdate=2010-09-29}}</ref>

<ref name="urlFungorum synonymy: Myriostoma">{{cite web |url=http://www.indexfungorum.org/Names/SynSpecies.asp?RecordID=122233 |title=''Myriostoma coliforme'' (Dicks.) Corda |publisher=CAB International. [[Index Fungorum|Species Fungorum]] |accessdate=2012-05-17}}</ref>

<ref name=Woodward1794>{{cite journal |title=As essay towards an History of the British stellated Lycoperdons: being an account of such species as have been found in the neighbourhood of Bungay, in Suffolk |author=Woodward TJ. |year=1794 |journal=Transactions of the Linnean Society of London |volume=2 |pages=32–68 |url=http://biodiversitylibrary.org/page/12866111 |doi=10.1111/j.1096-3642.1794.tb00241.x}}</ref>

<ref name=Zhou2002>{{cite journal |vauthors=Zhou TX, Yan YH |year=2002 |script-title=zh:地星科的中国新记录属种 |trans_title=New records of Geastraceae in China |journal=Mycosystema |volume=21 |issue=4 |pages=485–92 |language=Chinese |url=http://en.cnki.com.cn/Article_en/CJFDTotal-JWXT200204007.htm}} {{closed access}}</ref>
}}

===Cited texts===
*{{cite book |vauthors=Kirk PM, Cannon PF, Minter DW, Stalpers JA |title=Dictionary of the Fungi |edition=10th |publisher=CAB International |location=Wallingford, UK |year=2008 |isbn=978-0-85199-826-8}}

== External links ==
*{{IndexFungorum|122233}}
*[https://books.google.com/books?id=Fc0VAAAAYAAJ&pg=PA143 Lectotype illustration] from Tab. III of James Dickson's 1776 ''Fasciculus plantarum cryptogamicarum Britanniae''

{{featured article}}

[[Category:Fungi described in 1785]]
[[Category:Fungi of Africa]]
[[Category:Fungi of Asia]]
[[Category:Fungi of Europe]]
[[Category:Fungi of North America]]
[[Category:Fungi of South America]]
[[Category:Geastraceae]]
[[Category:Inedible fungi]]
[[Category:Monotypic Basidiomycota genera]]