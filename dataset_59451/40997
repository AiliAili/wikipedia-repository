{{Liberalism sidebar}}

'''Technoliberalism''' [ˈtɛknəʊ lɪb(ə)r(ə)lism’] is a [[political philosophy]] founded on ideas of liberty, individuality, responsibility, decentralization, and self-awareness. It also highlights an idea that technology should be available to everyone without controls.<ref name="Digital Anthroplogy">Horst, Heather and Miller, Daniel (eds.) [https://books.google.com/books?id=FVcDAAAAQBAJ&pg=RA10-PA2010&lpg=RA10-PA2010&dq=technoliberalism&source=bl&ots=9nFV_nri5M&sig=eVe1BYFq1NxmNxoPfecfzQfq9CI&hl=en&sa=X&ei=Ktr0UtTUD-yS7Abtw4DgDg&ved=0CEoQ6AEwBQ#v=onepage&q=technoliberalism&f=false "Digital Anthropology"] 2012. Accessed 7 February 2014.</ref> Its core beliefs fit under five main interests that include Construction of the Government, Economics, [[Civil Liberties]], Education and Science, and Environment. Technoliberals support such ideas as balance of powers in the government, [[decentralization]], affordable education, the protection of our planet, Fine Arts, and the [[freedom of speech]] and communication technologies.

== Philosophy ==
In his book titled ''Technoliberalism'', Adam Fish describes technoliberalism as a belief that networked technologies ameliorate the contradictions of a society that cherishes both the free market of economic liberalism and the social welfare of social liberalism<ref>Fish, Adam. 2017. Technoliberalism and the End of Participatory Culture. Palgrave Macmillan. https://www.palgrave.com/de/book/9783319312552</ref>. In this manner, technoliberalism has some links to [[neo-liberalism]], yet with some core differences; "While [[Adam Smith]] conceived of a market that was in a way a natural and ineradicable part of the landscape (based on the human propensity 'to truck, barter and exchange'), and neoliberal thought continues to see the market in this way, technoliberalism holds up the idea that such complex systems can be contrived in their entirety"<ref>Malaby, Thomas. [https://books.google.com/books?id=0PWpblV3bWMC&pg=PA33&lpg=PA33&dq=technoliberalism&source=bl&ots=XLlZSeqLOD&sig=PYsTrHwQNWGpA8Dh7U5Czvclmfs&hl=en&sa=X&ei=Ktr0UtTUD-yS7Abtw4DgDg&ved=0CEwQ6AEwBg#v=onepage&q=technoliberalism&f=false "Making Virtual Worlds: Linden Lab and Second Life"] 2009. Accessed 7 February 2014.</ref>
At the centre of the philosophy of Technoliberalism as a belief and a movement is "an overriding faith in technology, a suspicion of conventional [[modernist]] (top-down) institutions and a conviction that the aggregate effects of individual engagement of technology will generate social goods"<ref name="Digital Anthroplogy"/>
Technoliberalism is about the combining of decentralism, [[individualism]], responsibility and self-awareness, nothing in excess, sustainability, and engineering style regulation and governance. Its core beliefs fit under five main interests; Construction of the Government, Education and Science, Economics, the Environment, and Civil Liberties.
They include:
*The protection of the individuals freedom, whilst maintaining that of others.
*Free markets with strongly enforced rules.
*Fair taxation, especially of big companies.
*The protection of our planet through strong regulation on damaging the environment.
*The power of small and medium-sized businesses.
*The freedom of speech and communication technologies. 
*The emphasis on technological advancements instead of the status quo.

== Networked technology ==
* High-speed development of networked technology provides the platform for spreading information which encourages the [[freedom of speech]] and communication technologies. 
* Networks of [[distributed intelligence]]{{Disambiguation needed|date=March 2014}} increase the capacity of information technology. 
* Free access to the Internet deeply reflects the idea of technoliberalism.
* New forms of networked technology appear such as [[Current TV]] and mobile network which increase the opportunity in pursuing [[democratization]]. But one of the civil liberty is net neutrality for landline Internet and no net neutrality for mobile and satellites. 
* Cultural myths impact the success of [[digital democracy]] as much or more than technology.<ref>Fish, Adam. [http://prezi.com/jfrircplshv1/technoliberalism-and-current/ "Technoliberalism and Current"], 19 September 2013. Retrieved 11 February 2014.</ref>

== Economic freedom ==

=== Businesses ===
[[Economic freedom]] in terms of technoliberalism involves small scale [[capitalism]], that is capitalism for small and medium-sized businesses, rather than corporate organizations created by major interest groups. Ideally, localized systems and community ties will pave the way for a new capitalist economy, undoing the power of global capitalism.<ref>Ware, Michael. [http://climateandcapitalism.com/2013/04/02/why-small-scale-alternatives-wont-change-the-world/ "Why small-scale alternatives won't change the world"] 2 April 2013. Retrieved on 6 February 2014.</ref> Implemented trust regulations will compliment this, meaning more rules for big companies to create better competition, whereas smaller companies will be enforced with fewer rules. Technoliberalism places an emphasis on these small and medium-sized businesses because it can help boost economic growth. Money spent by local authorities with small firms is re-spent into the local economy, compared to that spent with large businesses in the same area. Doing business this way then, is better value for money.<ref>FSB. [http://www.fsb.org.uk/News.aspx?loc=pressroom&rec=8116 "FSB report reveals the power of small businesses in the local economy"] 8 July 2013. Retrieved 6 February 2014.</ref>

=== Rules ===
[[Decentralization]] is also a key ideological idea to technoliberalism, sought to work though the deregulation of rules that stop businesses competing with the government services.<ref>Litvack, Jennie. [http://www.ciesin.org/decentralization/English/General/Different_forms.html "What is Decentralization?"] Retrieved 6 February 2014.</ref> Decentralization means distributing the power away from the center of an organization, diffusing authority outwards to workers in the field. The rapid growth of [[information technology]] has aided this concept as the likes of the [[internet]] have made the distribution of information accessible and cheap.<ref>The Economist. [http://www.economist.com/node/14298890 "Decentralisation"] 5 October 2009. Retrieved 6 February 2014.</ref> On the other hand, is the ideological idea of free markets. Strongly enforced rules would be needed here as this type of market would be based on supply and demand with little government control.<ref name="free">Investopedia.[http://www.investopedia.com/terms/f/freemarket.asp "Definition of 'Free Market'"] Retrieved 6 February 2014.</ref> Technoliberalist's believe that knowledge and technology can be geographically transferred without much difficulty or state action,<ref>Archibugi, Daniele and Iammorino, Simona (1999). ''The policy implications of the globalisation of innovation'', p. 10. Elsevier, Rome.</ref> envisaging a completely [[free market]] where buyers and sellers are allowed to transact unreservedly, based on a mutual agreement on price without state intervention in the form of [[taxes]], [[subsidies]] or [[regulation]]. Whilst this is an idealized view, it would be hard to implement.<ref name="free" />

=== Taxation ===
Technoliberalist's believe in [[negative income tax]]. This is the idea that people earning below a certain amount receive supplemental pay from the government, instead of paying taxes to the government. This ensures that there is a minimum level of income for all.<ref name="negative">Allen, Jodie. [http://www.econlib.org/library/Enc1/NegativeIncomeTax.html "Negative Income Tax"] Retrieved 6 February 2014.</ref> Whilst common criticisms revolve around the fact that negative income tax could reduce the incentive to work, Technoliberalist's want to ensure there is a basic level of income available to everyone.<ref name="negative" /> Equally, technoliberalism wants fair taxation of big companies. Controversies involving [[multinational companies]] abusing tax rules,<ref>Walker, Andrew. [http://www.bbc.co.uk/news/business-23371564 "OECD launches plan to stop firms 'abusing' tax rules"], ''[[BBC News]]'', July 19, 2013. Retrieved 6 February 2014.</ref> means Technoliberalist's want to see fair tax being paid by big businesses. Ideas such as the [[Fair Tax Mark]] are already in progress<ref>Murphy, Richard. [http://www.taxresearch.org.uk/Blog/2013/06/13/the-fair-tax-mark-launched-today/ "The Fair Tax Mark – launched today"] 13 June 2013. Retrieved 6 February 2014.</ref>

== Free speech ==
Technoliberalism is seen as 21st century [[liberalism]]. New technologies and social networking sites allow for the free speech of citizens to voice their views. The discussions surrounding technoliberalism involve:{{citation needed|date=June 2016}}

* [[Decentralism]]
* [[Individualism]], Responsibility and Self-Awareness.
* Nothing In Excess.
* Sustainability.
* Engineering-Style Regulation and Governance.

== Citizen responsibility ==
Citizen responsibility in ‘Technoliberalism’ refers to the protection of the [[Autonomy|individuals' freedom]] while maintaining that of others.
Techno-liberals look for change. By their nature, they're not satisfied with the way things are and want to find new ways to do things. Liberals in technology arena move a society forward as the opportunists. Technoliberalism is actually more about civil liberties and individual rights such as Free Internet, Net neutrality for landline Internet not for mobile or [[satellites]], [[gun control]] and Pro abortion, etc. Techno-liberalism represents socio-cultural perspectives that imply all human endeavors. This includes how we develop and use technology, especially computer technology. In the technology arena, liberalism normally points to innovation and risk-taking. Furthermore, if you're a techno-liberal in information technology, the future can't come soon enough. For those who see the true promise of the web for [[multi-media]] and as a general platform for application software, the Internet is still far too slow and primitive.<ref>Feldman, Michael, [http://archive.hpcwire.com/hpcwire/2006-11-03/how_to_talk_to_a_techno-liberal_and_you_must.html 'How to Talk to a Techno-Liberal (and you must)'], 3 November 2006. Retrieved 5 February 2014</ref>

== Worldwide examples ==
Technoliberalism is a worldwide issue and it can be found in different aspects, the following are some worldwide examples:

=== Construction of the government ===
To construct a government, power will be balance and peer-reviewing everything is a core principle. Moreover, there will be separation of concerns and convention over configuration. For example, in [[United Kingdom]], the [[Prime Minister]] leads the government with the support of the [[Cabinet (government)|Cabinet]] and ministers. While departments and their agencies are responsible for putting government policy into practice and the public can engage with government through consultations and petitions to inform and influence the decisions it makes.<ref>{{cite web|last=GOV.UK|title=How government works|url=https://www.gov.uk/government/how-government-works#how-governement-is-run|accessdate=8 February 2014}}</ref>

=== Economics ===
Small scale [[capitalism]], which means, capitalism for small and medium-sized business instead of corporate socialisms; [[decentralization]]; [[Negative income tax]] and Trust regulation; are the examples of technoloberalism in economics aspects. For example, in [[European Union]], there were [[European Union competition law|Competition law]] illustrated to control big companies for better competition by adding more rules to them, while less rules for smaller companies. The main goal for this is that only consumer welfare considerations are relevant there.<ref>''See, for example, the [http://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=CELEX:52010XC0330(02):EN:NOT Commission's Article 101(3) Guidelines], the Court of First Instance's recent Glaxo Case and certain academic works, such as Okeoghene Odudu, The boundaries of EC competition law: the scope of article 81.'' Oxford: Oxford University Press, 2006.</ref>

=== Civil liberties ===
In today society, free access to Internet with the freedom to discuss different issues was a well-known example of technoliberalism. Moreover, gun control policy was another example, in other words, it means no one needs weapons and peace in technoliberalism.

=== Education and science ===
Technoliberalism can be found in examples relating to education and scientific fields. Within science some examples include more engineers and scientists within the political industry and free science on genetical engineering.
Examples included in Education can be the following;
*Tuition fee for Bachelor / Master for every student -> 3% of GDPpA per capita,
*Secondary education for every student -> 0.5% of GDPpA per capita,
*Primary education for every student: -> 0.1% of GDPpA per capita.

=== Environment ===
Examples of how technoliberalism can be applied to the environment are the following:
*Higher taxes on fossil fuels and anything that is damaging the environment
*Emissions trading / [[cap and trade]]
*Car-free cities.
*Genetical engineering (also for food) must be allowed

== References ==
{{Reflist|2}}

{{Technology}}

[[Category:Political theories]]
[[Category:Political ideologies]]
[[Category:Liberalism]]
[[Category:Politics and technology]]