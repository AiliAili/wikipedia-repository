{{Infobox scientist
| name              = Immanuel Bloch
| image             = 
| birth_date        = {{Birth date and age|1972|11|16|df=yes}}
| birth_place       = [[Fulda]], Germany
| residence         = {{flagcountry|Germany}}
| nationality       = {{flagcountry|Germany}} 
| field             = [[Physicist]]
| work_institution  = [[Ludwig-Maximilians University]] <br /> [[Max Planck Institute of Quantum Optics]]
| doctoral_advisor  = [[Theodor W. Hänsch]]
| doctoral_students = 
| known_for         = [[ultracold atom]]s, [[optical lattice]]s, Mott insulator
| societies         = Leopoldina,
| prizes	    = [[Otto Hahn Medal]] (2002)<br/>[[Gottfried Wilhelm Leibniz Prize]] (2004)<br/>
[http://qeod.epsdivisions.org/qeod-prizes EPS Quantum Electronics Prize] (2011)<br/>
[[Körber European Science Prize]] (2013)
[[Harvey Prize]] (2015)
}}

{{Tone|date=December 2016}}
{{likeresume|date=December 2016}}

'''Immanuel Bloch''' (born 16 November 1972, [[Fulda]]) is a German  [[experimental physicist]]. His research is focused on the investigation of quantum many-body systems using ultracold atomic and molecular quantum gases. Bloch is known for his work on ultracold atoms in artificial crystals of light, so called [[optical lattice]]s and especially the first realization of a quantum phase transition from a weakly interacting superfluid to a strongly interacting Mott insulating state of matter.

==Career==
{{unsourced|section|date=December 2016}}
Bloch studied physics at the University of Bonn in 1995, followed by a one-year research visit to [[Stanford University]]. He obtained his PhD in 2000 working under [[Theodor W. Hänsch]] at the Ludwig-Maximilians University in Munich. As a junior group leader, he continued in Munich starting his work on ultracold quantum gases in optical lattices. In 2003 he moved to a full professor position in experimental physics at the [[University of Mainz]], where he stayed until 2009. 

In 2008 he was appointed scientific director of the newly founded division on ''Quantum Many-Body Systems'' at the [[Max Planck Institute of Quantum Optics]], in Garching. Since 2012 he has been vice-dean at the [http://www.physik.uni-muenchen.de department of physics of LMU] and, since 2012, managing director of the [[Max Planck Institute of Quantum Optics]].

==Research==
The work of the physicist is concentrated on the investigation of quantum many-body system using ultracold atoms<ref>I. Bloch, J. Dalibard & W. Zwerger, Many-Body Physics with Ultracold Gases, [http://rmp.aps.org/abstract/RMP/v80/i3/p885_1 Rev. Mod. Phys. 80, 885 (2008)]</ref> stored in [[optical lattice]] potentials. Among other things, he is known for the realization of a [[quantum phase transition]] from a [[superfluid]] to a [[Mott insulator]],<ref>M. Greiner, O. Mandel, T. Esslinger, T.W. Hänsch & I. Bloch, Quantum Phase Transition from a Superfluid to a Mott Insulator [http://www.nature.com/nature/journal/v415/n6867/full/415039a.html Nature 415, 39-44 (2002)]</ref> in which ultracold atoms were for the first time brought into the regime of strong correlations thereby allowing one to mimic the behaviour [[strongly correlated material]]s. The experimental ideas were based on a theoretical proposal by [[Peter Zoller]] and [[Ignacio Cirac]]. His further work includes the observation of a [[Tonks-Girardeau gas]]<ref>B. Paredes, A. Widera, V. Murg, O. Mandel, S. Fölling, I. Cirac, G. Shlyapnikov, T.W. Hänsch & I. Bloch, Tonks-Girardeau gas of ultracold atoms in an optical lattice, [http://www.nature.com/nature/journal/v429/n6989/abs/nature02530.html Nature 429, 277 (2004)]</ref> of strongly interacting bosons in one dimensions, the detection of collapses and revivals<ref>M. Greiner, O. Mandel, T.W. Hänsch & I. Bloch, Collapse and revival of the matter-wave field of a Bose–Einstein condensate, [http://www.nature.com/nature/journal/v419/n6902/full/nature00968.html Nature 419, 51 (2002)]</ref> of the wavefunction of a [[Bose–Einstein condensate]] because of interactions, and the use of quantum noise correlations to observe [[Hanbury Brown and Twiss effect|Hanbury-Brown and Twiss]] bunching<ref>S. Fölling, F. Gerbier, A. Widera, O. Mandel, T. Gericke & I. Bloch, Spatial quantum noise interferometry in expanding ultracold atom clouds, [http://www.nature.com/nature/journal/v434/n7032/abs/nature03500.html Nature 434, 481 (2005)]</ref> and antibunching<ref>T. Rom, Th. Best, D. Van Oosten, U. Schneider, S. Fölling, B. Paredes, I. Bloch, Free fermion antibunching in a degenerate atomic Fermi gas released from an optical lattice, [http://www.nature.com/nature/journal/v434/n7032/abs/nature03500.html Nature 434, 481 (2005)]</ref> for bosonic and fermionic atoms (simultaneously with the group of  [[Alain Aspect]]). More recently, his research team was able to realize single-atom resolved imaging<ref>J.F. Sherson, C. Weitenberg, M. Endres, M. Cheneau, I. Bloch & S. Kuhr, Single-atom-resolved fluorescence imaging of an atomic Mott insulator, [http://www.nature.com/nature/journal/vaop/ncurrent/full/nature09378.html Nature 467, 68 (2010)]</ref> and addressing<ref>C. Weitenberg, M. Endres, J.F. Sherson, M. Cheneau, P. Schauß, T. Fukuhara, I. Bloch & S. Kuhr, Single-spin addressing in an atomic Mott insulator, [http://www.nature.com/nature/journal/v471/n7338/abs/nature09827.html Nature 471, 319 (2011)]</ref> of ultracold atoms held in an optical lattice. Related work was carried out in the group of [[Markus Greiner]].

==Awards==
In 2005 he was presented with the '''International Commission of Optics Prize'''. In 2011, he received the [http://qeod.epsdivisions.org/qeod-prizes EPS Prize for Fundamental Aspects of Quantum Electronics and Optics]<ref>[http://qeod.epsdivisions.org/qeod-prizes/the-eps-quantum-electronics-prize EPS Quantum Electronics Prize 2011], qeod.epsdivisions.org; accessed 3 December 2016.</ref> of the [[European Physical Society]]. 

In 2013, Bloch was awarded the [[Körber European Science Prize]] and the International Senior BEC Award. For the year 2015 he received the [[Harvey Prize]] from Israel's Technion Institute.<ref>[http://www.technion.ac.il/en/# Harvey Prize 2015]</ref> He is a member of the [[German Academy of Sciences Leopoldina]] and an external member of the [[Canadian Institute for Advanced Research]].

==References==
<references/>

==External links==
* [http://www.quantum-munich.de/fileadmin/media/people/ImmanuelBlochCV.pdf CV]
* [http://www.quantum-munich.de/people/person-details/pers/1/ Immanuel Bloch at LMU and MPQ]
* [http://www.quantum-munich.de/ Group Homepage]
* [http://www.mpq.mpg.de/ Max-Planck Institute of Quantum Optics]
* [http://www.physik.uni-muenchen.de Fakultät für Physik, LMU Munich]
* [https://scholar.google.com/citations?user=kX5_lc8AAAAJ&hl=en Google Scholar Entries for Immanuel Bloch]

{{Authority control}}

{{DEFAULTSORT:Bloch, Immanuel}}
[[Category:Quantum physicists]]
[[Category:German physicists]]
[[Category:Gottfried Wilhelm Leibniz Prize winners]]
[[Category:1972 births]]
[[Category:Living people]]
[[Category:People from Fulda]]
[[Category:University of Bonn alumni]]
[[Category:Ludwig Maximilian University of Munich alumni]]