{{Infobox journal
| title         = Algebra i Logika
| cover         = 
| editor        = [[Yury Leonidovich Ershov]]
| discipline    = Mathematics
| peer-reviewed = 
| language      = Russian, English
| former_names  = 
| abbreviation  = 
| publisher     = [[Siberian Fund for Algebra and Logic]]
| country       = Russia
| frequency     = Bimonthly
| history       = 1962–present
| openaccess    = 
| license       = 
| impact        = 0.493
| impact-year   = 2012
| website       = http://www.mathnet.ru/php/journal.phtml?jrnid=al&option_lang=eng
| link1         = http://www.mathnet.ru/php/archive.phtml?jrnid=al&wshow=contents&option_lang=eng
| link1-name    = Online access
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| ISSN          = 0373-9252
| eISSN         = 
| boxwidth      = 
}}

'''''Algebra i Logika''''' (English: ''Algebra and Logic'') is a peer-reviewed [[russian language|Russian]] [[mathematical journal]] founded in 1962 by [[Anatoly Ivanovich Malcev]], published by the [[Siberian Fund for Algebra and Logic]] at [[Novosibirsk State University]].<ref>
{{cite book
 |editor=Yakov Sinai
 |year=2003
 |url=https://books.google.com/books?id=BFsnWXkqZaMC&pg=PA560&dq=%22algebra+i+logika%22+malcev&lr=&as_brr=3&ei=ArItS4fjOYiWNZGPycoI&cd=11#v=onepage&q=%22algebra%20i%20logika%22%20malcev&f=false
 |title=Russian Mathematicians in the 20th Century
 |page=560
 |publisher=[[World Scientific]]
 |isbn=978-981-238-385-3
}}</ref> An English translation of the journal is published by [[Springer-Verlag]] as '''''Algebra and Logic''''' since 1968.<ref>
{{cite book
 |author=Anita Burdman Feferman, Solomon Feferman
 |year=2004
 |title=Alfred Tarski: Life and Logic
 |url=https://books.google.com/books?id=wqktlxHo9wkC&pg=PA306&dq=%22algebra+i+logika%22+malcev&lr=&as_brr=3&ei=lrEtS-nqMYHMM_GlifEC&cd=4#v=onepage&q=&f=false
 |page=306
 |publisher=[[Cambridge University Press]]
 |isbn=978-0-521-80240-6
}}</ref><ref>
{{cite book
 |author=Krishna Subramanyam
 |year=1981
 |title=Scientific and Technical Information Resources
 |url=https://books.google.com/books?id=51C-Uwykb38C&pg=PA274&dq=%22Algebra+i+Logika%22+important+Russian+journal&as_brr=3&ei=I2E-S6eiDI3aNbq3lLEB&cd=9#v=onepage&q=Algebra%20i%20Logika&f=false
 |page=274
 |publisher=[[Taylor & Francis]]
 |isbn=0-8247-8297-6
}}</ref><ref>
{{cite book
 |author=Diana F. Liang
 |year=1992
 |title=Mathematical Journals: An Annotated Guide
 |page=8
 |publisher=[[Scarecrow Press]]
 |isbn=0-8108-2585-6
}}</ref> It published papers presented at the meetings of the "Algebra and Logic" seminar at the [[Novosibirsk State University]]. The journal is edited by academician [[Yury Yershov]].

The journal is reviewed cover-to-cover in ''[[Mathematical Reviews]]'' and ''[[Zentralblatt MATH]]''. The 2012 impact factor is 0.493.

==Abstracting and Indexing==
''Algebra i Logika'' is indexed and abstracted in the following databases:
{{columns-list|3|
*[[Academic OneFile]]
*[[Academic Search]]
*[[EBSCO]]
*[[Inspec]]
*[[International Bibliography of Book Reviews]]
*[[International Bibliography of Periodical Literature]]
*[[Journal Citation Reports]]/Science Edition
*[[Mathematical Reviews]]
*[[Science Citation Index]]
*[[SCImago]]
*[[Scopus]]
*[[STMA-Z]]
*[[Summon by Serial Solutions]]
*[[Zentralblatt Math]]
}}

==References==
{{Reflist}}

==External links==
*[http://www.mathnet.ru/php/journal.phtml?jrnid=al&option_lang=eng ''Algebra i Logika'' website]
*[http://www.springer.com/math/algebra/journal/10469 ''Algebra and Logic'' website]

[[Category:Mathematics journals]]
[[Category:Publications established in 1962]]
[[Category:Novosibirsk State University]]
[[Category:Magazines published in Novosibirsk]]