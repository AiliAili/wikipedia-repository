[[File:Turner 07 FGLecture.JPG|thumb|right|At the 2007 Turner Prize ceremony at the [[National Building Museum]], [[Frank Gehry]] gives a presentation on the work of Gehry Partners and Gehry Technologies driving construction innovation.]]
The '''Henry C. Turner Prize for Innovation in Construction Technology''' is awarded annually by the [[National Building Museum]] to recognize outstanding leadership and innovation in the field of [[construction]] methods and processes, including engineering design and construction techniques and practices. Created in 2002 by an endowment established by the [[Turner Construction Company]] and named after the company's founder, the prize carries a cash award of $25,000.<ref name="ArchMag">{{cite web|title=Engineers Without Borders Selected for 2010 Turner Prize|date=2010-07-13|author=Peter James|publisher=Architect Magazine|url=http://www.architectmagazine.com/award-winners/engineers-without-borders-selected-for-2010-turner-prize.aspx}}</ref>
<br>
Past honorees include individuals and organizations such as architect [[I. M. Pei]], for encouraging construction and engineering innovation with his designs;<ref name="Pei">{{cite web|title=I. M. Pei's Construction Innovation|publisher=Architecture Week|date=2003-04-23|url=http://www.architectureweek.com/2003/0423/news_1-1.html}}</ref> [[Stanford University]] civil engineering professor Paul Teicholz, for his work in paving the way for [[Building Information Modeling]];<ref name="Teicholz">{{cite web|title=Stanford's Teicholz Wins Turner Prize|date=2007-02-19|publisher=Engineering Record|url=http://enr.construction.com/news/buildings/archives/070219a.asp}}</ref> and [[Engineers Without Borders (USA)|Engineers Without Borders–USA]] for efforts in creating sustainable infrastructure in poverty-stricken world communities and for "instilling a sense of global responsibility in the next generation of engineers," according to the award jury.<ref name="EWB">{{cite web|title=Engineers Without Borders-USA Selected for 2010 Turner Prize|publisher=Engineering News Record|date=2010-07-14|url=http://enr.construction.com/business_management/workforce/2010/0714-turnerprizewinner.asp}}</ref> When [[Frank Gehry]] accepted the prize on behalf of Gehry Partners in 2007, he stated, "I've gotten a lot of awards from the artsy side of the profession, but this one's from the meat-and-potatoes side, and that's pretty special."<ref name="Gehry">{{cite web|author=Stephani Miller|title=Gehry Companies Receive Henry C. Turner Prize|publisher=Architect Magazine|date=10-12-2007|url=http://www.architectmagazine.com/construction/gehry-companies-receive-henry-c-turner-prize.aspx}}</ref><br>
In addition to the Turner Prize, the National Building Museum also awards the [[Vincent Scully Prize]], which honors exemplary practice, scholarship, or criticism in architecture, historic preservation, and urban design, and the [[Honor Award]] for individuals and organizations who have made important contributions to the U.S.'s building heritage.

== List of Turner Prize winners ==

{| class="wikitable sortable"
|-
! Year !! Recipient
|-
| 2014 || [[Penn_State_College_of_Engineering|Department of Architectural Engineering of the Pennsylvania State University]]
|-
| 2011 || [[Caterpillar Inc.|Caterpillar, Inc.]]
|-
| 2010 || [[Engineers Without Borders (USA)|Engineers Without Borders–USA]]
|-
| 2008 || Charles H. Thornton, co-founder of [[Thornton Tomasetti]]<ref name="Thornton">{{cite web|url=http://newyork.construction.com/people/names/archive/2009/01.asp|title=Names in the News - January 2009|publisher=McGraw Hill Companies}}</ref>
|-
| 2007 || [[Gehry Partners|Gehry Partners and Gehry Technologies]]
|-
| 2007 || Dr. Paul Teicholz
|-
| 2005 || [[U.S. Green Building Council]]
|-
| 2004 || Charles A. DeBenedittis, senior managing director of design and construction, [[Tishman Speyer|Tishman Speyer Properties]]<ref name="DeBenedittis">{{cite news|title=Lectures, Conferences, Symposia|publisher=Architectural Record (192.9)|date=09-01-2004|page=58}}</ref>
|-
| 2003 || [[I. M. Pei]]
|-
| 2002 || [[Leslie E. Robertson]]
|}<ref name="NBM List">{{cite web|publisher=National Building Museum|title=Henry C. Turner Prize|url=http://www.nbm.org/support-us/awards_honors/turner-prize/}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.nbm.org/support-us/awards_honors/turner-prize/ National Building Museum's information on the Turner Prize]
* [http://www.turnerconstruction.com/corporate/content.asp?d=5743 Turner Construction's information on the Turner Prize]

[[Category:Architecture awards]]