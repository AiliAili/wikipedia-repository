{{About|the journal|the field of study|Contemporary literature}}
{{Infobox journal
| title = Contemporary Literature
| cover =
| discipline = [[Contemporary literature]]
| formernames = Wisconsin Studies in Contemporary Literature
| editor = Thomas Schaub
| abbreviation = Contemp. Lit.
| publisher = [[University of Wisconsin Press]]
| country = United States
| frequency = Quarterly
| history = 1960-present
| openaccess =
| website = http://uwpress.wisc.edu/journals/journals/cl.html
| link1 = http://cl.uwpress.org/content/current
| link2 = http://cl.uwpress.org/content/by/year
| link2-name = Online archive
| link1-name = Online access
| ISSN = 0010-7484
| eISSN = 1548-9949
| OCLC = 2244753
| LCCN = 64006922
| JSTOR = 00107484
}}
'''''Contemporary Literature''''' is a quarterly [[peer-reviewed]] [[academic journal]] which publishes interviews with notable and developing authors, scholarly essays, and reviews of recent books critiquing the [[contemporary literature]] field. Genre coverage includes [[poetry]], the novel, drama, creative nonfiction, and new media (including digital literature and the graphic narrative). The [[editor-in-chief]] is [[Thomas Schaub]] ([[University of Wisconsin-Madison]]). It was established in 196à as the ''Wisconsin Studies in Contemporary Literature'', obtaining its current title in 1968.

== Abstracting and indexing ==
The journal is abstracted and indexed by:<ref>{{cite web |url=http://uwpress.wisc.edu/journals/indandab.html#CL |title=Services that Index and Abstract our Journals: ''Contemporary Literature'' |publisher=University of Wisconsin Press |accessdate=2015-01-16}}</ref>
{{columns-list|colwidth=30em|
* [[Arts and Humanities Citation Index]]
* [[Current Contents]]/Arts and Humanities
* Abstracts of English Studies
* [[Academic Search]]
* American Bibliography of Slavic and East European Studies
* [[Annual Bibliography of English Language and Literature]]
* Routledge Annotated Bibliography of English Studies
* [[Humanities Index]]
* [[Expanded Academic]]
* [[Humanities International index]]
* [[Scopus]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://uwpress.wisc.edu/journals/journals/cl.html}}
{{University of Wisconsin&ndash;Madison}}
[[Category:American literary magazines]]
[[Category:University of Wisconsin Press academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1960]]