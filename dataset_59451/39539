{{Use dmy dates|date=May 2014}}
{{Infobox Election
|election_name = Abkhazian presidential election, 2011
|country = Abkhazia
|type = presidential
|ongoing = no
|party_colour = yes
|party_name = no
|previous_election = Abkhazian presidential election, 2009
|previous_year = 2009
|next_election = Abkhazian presidential election, 2014
|next_year =  2014
|election_date = 26 August 2011
|candidate1=[[Alexander Ankvab]]
|colour1 = FF0000
|image1 = [[File:No image.png|100px]]
|party1 = [[Aitaira]]
| running_mate1 = [[Mikhail Logua]]
| popular_vote1 = 58,657
| percentage1 = 54.90
|candidate2=[[Sergei Shamba]]
|colour2 = 000000
|image2 = [[File:No image.png|100px]]
|party2 = 
| running_mate2 = [[Shamil Adzynba]]
| popular_vote2 = 22,456
| percentage2 = 21.02
|candidate3=[[Raul Khajimba]]
|colour3 = 000000
|image3 = 
|party3 = 
| running_mate3 = [[Svetlana Jergenia]]
| popular_vote3 = 21,177
| percentage3 = 19.82
|title= President
|before_election= [[Alexander Ankvab]] (acting)
|before_party= [[Aitaira]]
|after_election= [[Alexander Ankvab]]
|after_party = [[Aitaira]]
}}

{{Politics of Abkhazia}}

A '''presidential election''' was held in the [[Abkhazia|Republic of Abkhazia]] on 26 August 2011. This was the fifth such election since the post of [[President of Abkhazia|President of the Republic of Abkhazia]] was created in 1994. The election was held to elect the successor of president [[Sergei Bagapsh]] who died in office on 29 May 2011.

==Background==
The election was originally scheduled to take place in 2014, five years after the [[Abkhazian presidential election, 2009|previous election]]; however, the [[Constitution of Abkhazia]] required an election to be held within three months after the unexpected death of incumbent president [[Sergei Bagapsh]] on 29 May. On 8 June the [[People's Assembly of Abkhazia|People's Assembly]] set the election date for 26 August.<ref name=civil23592>{{cite news|title=Abkhaz Early Elections Set for August 26|url=http://www.civil.ge/eng/article.php?id=23592|accessdate=8 June 2011|newspaper=[[Civil Georgia]]|date=8 June 2011}}</ref><!--Is this notable? If so WHY? exactly three years after Abkhazia's independence was first recognised by Russia.--> The MPs decided against earlier dates such as 20 August, which is immediately after the summer holidays, as the election is largely organised by teachers and to allow for repairs to school buildings to be completed, where many polling stations are located.<ref name=ekho24229002>{{cite news|last=Galustyan|first=Levon|title=Выборы состоятся 26 августа|url=http://www.ekhokavkaza.com/content/article/24229002.html|accessdate=2 July 2011|newspaper=Echo of the Caucasus|date=8 June 2011}}</ref>

===Requirements and registration procedure===
According to the Law on the Election of the President of the Republic of Abkhazia, candidates for the Presidency have to:
* possess Abkhazian citizenship;
* be of Abkhaz ethnicity;
* be fluent in the Abkhaz language;
* be no younger than 35 and no older than 65 years of age;
* be eligible to vote;
* have permanently lived in Abkhazia for the last five years up to the election day.<ref name=uzel188095>{{cite news|last=Kuchuberia|first=Anzhela|title=Кандидаты на пост главы Абхазии будут сдавать экзамен по абхазскому языку|url=http://abkhasia.kavkaz-uzel.ru/articles/188095/|accessdate=2 July 2011|newspaper=[[Caucasian Knot]]|date=29 June 2011}}</ref>

Prospective candidates have to be nominated between 27 June and 17 July.<ref name=apress3453/> This can be done either by an initiative group of at least 10 people with a list of between 2000 and 2500 signatures, or by a political party registered with the Central Election Commission.<ref name=apress3541>{{cite news|title=С 27 июня начинается выдвижение кандидатов в президенты Республики Абхазия|url=http://apsnypress.info/news/3541.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=27 June 2011}}</ref> The only parties registered in this way are [[United Abkhazia]], the [[Forum of the National Unity of Abkhazia]], the [[Party of the Economic Development of Abkhazia]], the [[People's Party of Abkhazia|People's Party]] and the [[Communist Party of Abkhazia|Communist Party]]. After the nomination period ends, the Central Election Commission will verify the signature lists and whether candidates satisfy the set requirements. To test the nominees' proficiency in Abkhaz, it has established a language commission.<ref name=apress3550>{{cite news|title=Батал Табагуа проинформировал депутатов о подготовке Центризбиркома к президентским выборам|url=http://apsnypress.info/news/3550.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=28 June 2011}}</ref> Registration of the candidates has to be completed before 27 July and two days after their registration the Central Election Commission has to make public the list of nominees whose candidacy had been approved.<ref name=apress3453>{{cite news|title=Центризбирком утвердил календарный план мероприятий по подготовке к досрочным выборам президента|url=http://apsnypress.info/news/3453.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=10 June 2011}}</ref>

==Candidates==
All three candidates who were nominated for the election successfully completed their registration: Acting President Alexander Ankvab, Prime Minister Sergei Shamba and opposition leader Raul Khajimba. Following their nomination, Shamba and his Vice Presidential candidate, Shamil Adzynba, as well as Khajimba and his running mate Svetlana Jergenia applied for registration on 16 July.<ref name=apress3690>{{cite news|title=Сергей Шамба и Шамиль Адзынба подали заявление в Центральную избирательную комиссию о согласии баллотироваться на пост президента и вице-президента Республики Абхазия.|url=http://apsnypress.info/news/3690.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=16 July 2011}}</ref><ref name=apress3691>{{cite news|title=Рауль Хаджимба и Светлана Джергения дали согласие баллотироваться на пост президента и вице-президента Абхазии|url=http://apsnypress.info/news/3691.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=16 July 2011}}</ref> Ankvab and his Vice Presidential candidate Mikhail Logua filed their application on 17 July.<ref name=apress3692>{{cite news|title=Александр Анкваб и Михаил Логуа дали согласие баллотироваться на пост президента и вице-президента Республики Абхазия|url=http://apsnypress.info/news/3692.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=17 July 2011}}</ref> The three Presidential candidates passed their Abkhaz language test on 20 July 2011.<ref name=apress3724>{{cite news|title=Александр Анкваб, Рауль Хаджимба и Сергей Шамба сдали экзамен на знание Государственного абхазского языка|url=http://apsnypress.info/news/3724.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=20 July 2011}}</ref> All candidates were registered by the CEC on 25 July<ref name=apress3754>{{cite news|title=Центральная избирательная комиссия зарегистрировала кандидатами в президенты Александра Анкваба, Рауля Хаджимба и Сергея Шамба|url=http://apsnypress.info/news/3754.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=25 July 2011}}</ref> and received their certificates on 26 July.

{|class="wikitable"
|-
!Presidential candidate
!Career
!Vice Presidential candidate
!Career
!Political support
|-
| width="160px" nowrap align=center | 
| rowspan=2 width="160px" |
*Vice President (2005–2009)
*Prime Minister (2003–2004)
*Minister of Defence (2002–2003)
| width="160px" nowrap align=center |
| rowspan=2 width="160px" |
*Widow of first President [[Vladislav Ardzinba]]
| rowspan=2 width="320px" |
*Nominated by an initiative group (28 June)<ref name=apress3547>{{cite news|title=ЦИК Абхазии зарегистрировал инициативную группу по выдвижению кандидатом в президенты Рауля Хаджимба|url=http://apsnypress.info/news/3547.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=28 June 2011}}</ref>
*Nominated by the [[Forum of the National Unity of Abkhazia|Forum for National Unity]] (16 July)<ref name=apress3691/>
*[[Akhatsa]] (5 July)<ref name=aiaaira2178>{{cite news|title="Ахьаца" поддерживает тандем Рауля Хаджимба и Светланы Джергения|url=http://aiaaira.com/index.php?option=com_content&view=article&id=2178:qq-------&catid=69:politics&Itemid=112|accessdate=6 July 2011|newspaper=Aiaaira|date=5 July 2011}}</ref>
*[[Aruaa]] (7 July)<ref name=aiaaira2183>{{cite news|title=Заявление Высшего Совета "АРУАА"|url=http://aiaaira.com/index.php?option=com_content&view=article&id=2183:---lr&catid=73:society&Itemid=116|accessdate=11 July 2011|newspaper=Aiaaira|date=7 July 2011}}</ref>
*Former Prime Minister and 2004 Presidential candidate [[Anri Jergenia]] (27 July)<ref name=nuzhnaya29>{{cite news|title=Анри Джергения о выборах, санаторном вопросе и о прочем... |url=http://aiaaira.com/component/content/article/65-headline/2261-2011-07-30-14-12-35 |accessdate=5 August 2011 |newspaper=Nuzhnaya/Aiaaira |date=27–30 July 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20110825031037/http://aiaaira.com:80/component/content/article/65-headline/2261-2011-07-30-14-12-35 |archivedate=25 August 2011 |df=dmy }}</ref>
*[[Union of the Cossacks of Abkhazia|Union of Cossacks]] (5 August)<ref name=apress3865>{{cite news|title=Союз Казаков Абхазии поддерживает кандидатуру Рауля Хаджимба|url=http://apsnypress.info/news/3865.html|accessdate=5 August 2011|newspaper=[[Apsnypress]]|date=5 August 2011}}</ref>
|-
![[Raul Khajimba]]
![[Svetlana Jergenia]]
|-
| width="160px" nowrap align=center |
| rowspan=2 width="160px" |
*Acting President (since 29 May 2011)
*Vice President (2010–2011)
*Prime Minister (2005–2010)
*Businessman
| width="160px" nowrap align=center |
| rowspan=2 width="160px" |
*Governor of [[Gulripsh district]]
| rowspan=2 width="320px" |
*Nominated by an initiative group (29 June)<ref name=apress3560>{{cite news|title=Инициативная группа из 19 человек выдвинула кандидатом в президенты Александра Анкваб|url=http://apsnypress.info/news/3560.html|accessdate=2 July 2011|newspaper=[[Apsnypress]]|date=29 June 2011}}</ref>
*[[United Abkhazia]] (22 July)<ref name=apress3748>{{cite news|title=Политсовет "Единой Абхазии" поддержал выдвижение кандидатами в президенты и вице-президенты Александра Анкваба и Михаила Логуа|url=http://apsnypress.info/news/3748.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=22 July 2011}}</ref>
*[[Amtsakhara]] (2 August)<ref name=apress3852>{{cite news|title=Союз ветеранов отечественной войны народа Абхазии "Амцахара" поддерживает Александра Анкваба|url=http://apsnypress.info/news/3852.html|accessdate=5 August 2011|newspaper=[[Apsnypress]]|date=3 August 2011}}</ref>
|-
![[Alexander Ankvab]]
![[Mikhail Logua]]
|-
| width="160px" nowrap align=center |
| rowspan=2 width="160px" |
*Prime Minister (since 13 February 2010)
*Minister for Foreign Affairs (1997–2004 and 2004–2010)
| width="160px" nowrap align=center |
| rowspan=2 width="160px" |
*Deputy Chairman of the committee for Youth Affairs and Sport
| rowspan=2 width="320px" |
*Nominated by an initiative group (8 July)<ref name=apress3620>{{cite news|title=ЦИК зарегистрировал инициативную группу по выдвижению Сергея Шамба кандидатом в президенты Абхазии|url=http://apsnypress.info/news/3620.html|accessdate=11 July 2011|newspaper=[[Apsnypress]]|date=8 July 2011}}</ref>
*[[Party of the Economic Development of Abkhazia|Party for Economic Development]] (21 July)<ref name=apress3737>{{cite news|title=Партия Экономического развития Абхазии поддержала кандидатуру Сергея Шамба на пост президента республики|url=http://apsnypress.info/news/3737.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=21 July 2011}}</ref>
*10 (out of 12) members of the [[Association of Youth Organisations of Abkhazia|Association of Youth Organisations]] (28 July)<ref name=apress3796>{{cite news|title=10 из 12 организаций, входящих в Ассоциацию молодежных организаций Абхазии, заявили о поддержке кандидата в президенты Сергея Шамба|url=http://apsnypress.info/news/3796.html|accessdate=5 August 2011|newspaper=[[Apsnypress]]|date=28 July 2011}}</ref>
*[[Communist Party of Abkhazia|Communist Party]] (9 August)<ref name=apress3899>{{cite news|title=Политсовет компартии Абхазии поддержал кандидатуру Сергея Шамба.|url=http://apsnypress.info/news/3899.html|accessdate=9 August 2011|newspaper=[[Apsnypress]]|date=9 August 2011}}</ref>
|-
![[Sergei Shamba]]
![[Shamil Adzynba]]
|}

==Election campaign==
The election campaign official began when the candidates received their registration certificates on 26 July. According to election law, Alexander Ankvab and Sergei Shamba had to take leave from their offices.<ref name=apress3756>{{cite news|title=Кандидаты в президенты Республики Абхазия должны уйти в отпуск|url=http://apsnypress.info/news/3756.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=25 July 2011}}</ref> Correspondingly, Parliament Speaker [[Nugzar Ashuba]] temporarily took over as Acting President and Vice-Premier [[Beslan Kubrava]] as Prime Minister.

On 19 July, the League of Voters "For Fair Elections" held its first press conference to announce that it would monitor the upcoming election as in previous years.<ref name=apress3713>{{cite news|title=Лига избирателей "За честные выборы" будет наблюдать за ходом голосования на выборах президента Абхазии 26 августа|url=http://apsnypress.info/news/3713.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=19 July 2011}}</ref>

Shamba's campaign was led by [[Beslan Eshba]].<ref name=ekho27195737>{{cite news|last1=Sharia|first1=Vitali|title=«Родина» и «Народный фронт» по-абхазски|url=http://www.ekhokavkaza.org/content/article/27195737.html|accessdate=19 October 2015|agency=[[Echo of the Caucasus]]|date=18 August 2015}}</ref>

===Media use===

====TV broadcasts====
Each Presidential Candidate received three hours of free air time on national state television in the four weeks running up to election day, and each Vice Presential candidate one hour. During the first week, candidates had the choice between an hour of live interaction with voters and sending in a pre-recorded DVD. In the second week, the candidates, alone or assisted by no more than four associates, received one hour of live interaction with voters. In the third and fourth weeks, first the Vice-Presidential candidate and then the Presidential candidates had the opportunity to answer alternatingly questions from voters and from officially registered media.

Apart from these broadcasts, each candidate could send in commercials up to five minutes, which were broadcast on weekdays between 1 and 25 August, three times daily (8:00, 18:00 and 20:00).

The order in which the broadcasts of candidates appeared was determined by draw, and was as follows:<ref name=apress3768>{{cite news|title=Центризбирком жеребьевкой определил очередность выступления кандидатов в президенты и вице-президенты в эфире Абхазской Гостелерадиокомпании|url=http://apsnypress.info/news/3768.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=26 July 2011}}</ref>

{| class="wikitable" style="text-align:right;background:none"
|-
|style="width: 200px;"| 
|style="width: 100px;"|'''Monday'''
|style="width: 100px;"|'''Tuesday'''
|style="width: 100px;"|'''Wednesday'''
|style="width: 100px;"|'''Thursday'''
|style="width: 100px;"|'''Friday'''
|style="width: 200px;"|'''Order of commercials'''
|-
|style="text-align:left" |Week 1 (1–5 August)
|Khajimba
|
|Shamba
|
|Ankvab
|Khajimba – Ankvab – Shamba
|-
|style="text-align:left" |Week 2 (8–12 August)
|Khajimba
|
|Shamba
|
|Ankvab
|Khajimba – Shamba – Ankvab
|-
|style="text-align:left" |Week 3 (15–19 August)
|Logua
|
|Jergenia
|
|Adzynba
|Ankvab – Khajimba – Shamba
|-
|style="text-align:left" |Week 4 (22–26 August)
|Shamba
|Khajimba
|Ankvab
|
|
|Ankvab – Shamba – Khajimba
|-
|}

==Election==
The CEC decided that in order to reduce costs and simplify the organisation of the election, there would be no separate polling stations and precinct election commissions for military units. Instead, it sent a letter to the Ministry of Defence requesting that soldiers should receive leave ten days before the election, allowing them to collect absentee ballots and thus vote in a polling station of their choosing.<ref name=apress3757>{{cite news|title=В войсковых частях министерства обороны РА не будет участковых избирательных комиссий|url=http://apsnypress.info/news/3757.html|accessdate=31 July 2011|newspaper=[[Apsnypress]]|date=25 July 2011}}</ref>

===Results===
According to the Abkhazian electoral commission, preliminary results showed a 54.86% first round victory for Ankvab over Shamba (21.04%) and Khajimba (19.83%).<ref name=apress4103>{{cite news|title=Александр Анкваб набрал 54,86% голосов избирателей|url=http://apsnypress.info/news/4103.html|accessdate=27 August 2011|newspaper=[[Apsnypress]]|date=27 August 2011}}</ref>

The official results, released on 27 August, showed only small differences, and Alexander Ankvab was elected with 54.90% of the vote.<ref>{{cite news|url=http://www.mfaabkhazia.net/en/node/1054|title=Final results of the presidential elections in Abkhazia|date=27 August 2011|publisher=Abkhazian Ministry of Foreign Affairs|accessdate=4 September 2011}}</ref> He is to be sworn in on 26 September.<ref>{{cite news|url=http://apsnypress.info/news/4149.html|title=26 сентября состоится инаугурация вновь избранного президента Республики Абхазия|date=2 September 2011|publisher=Apsnypress|language=Russian|accessdate=4 September 2011}}</ref>

{{Abkhazian presidential election, 2011}}

==Opinion polling==

{| class="wikitable sortable"
|-
! Date
! class=unsortable|Source
! style="background-color:#F7230C; width: 50px;" | Sergei Shamba
! style="background-color:#2C75FF; width: 50px;" | Alexander Ankvab
! style="background-color:Steelblue; width: 50px;" | Raul Khajimba
|-
! 5–12 July 2011
! class=unsortable|Dobrososedstvo
| align="center" | 30.1%
| align="center" | 26.1%
| align="center" | 8.0%
|}
* [https://web.archive.org/web/20120424225952/http://www.kavkaz-news.info/portal/cnid_181385/alias__Caucasus-Info/lang__en/tabid__2434/default.aspx Opinion polling]

==References==
{{reflist}}

{{Abkhazian elections}}

[[Category:2011 elections in Europe]]
[[Category:Abkhazian presidential election, 2011| ]]