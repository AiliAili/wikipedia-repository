{{More footnotes|date=July 2013}}


'''Geoffrey Key''' (born 13 May,1941, [[Rusholme]], [[Manchester]], [[England]]) is a British painter and sculptor. A number of public art collections have examples of his work.<ref name="BBC Your Art">{{cite web|url=http://www.bbc.co.uk/arts/yourpaintings/artists/geoffrey-key/paintings/slideshow#/1 |title=Geoffrey Key (b.1941) &#124; Art UK Art UK &#124; Discover Artists Geoffrey Key (b.1941) |website=Bbc.co.uk |date= |accessdate=2016-09-15}}</ref>

==Early life and education==
Key's mother, Marion, worked as an [[illustrator]], and encouraged him to draw.<ref name="GKey">{{Cite book|title=G Key |first=Judith M |last=O'Leary |year=2011 |publisher=JMOL Publishing | ISBN=978-0-9559117-4-3}}</ref>{{rp|3–4}}<ref name="messums2">{{cite web|url=http://www.messums.com/artist/955/Geoffrey-Key/ |title=Archived copy |accessdate=2013-06-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20130615064223/http://www.messums.com:80/artist/955/Geoffrey-Key |archivedate=2013-06-15 |df= }}</ref>

Key was educated at the Manchester High School of Art, whose headmaster, Ernest Goodman, established the Salford Art Club. After Goodman's death, its members chose Key as the Honorary President.<ref>Salford Art Club - Salford City Councilwww.salford.gov.uk › ... › Salford Museum › About us › Links</ref>

In 1958, Key enrolled at the [[Manchester School of Art|Manchester Regional College of Art]].<ref name="GKey"/>{{rp|11}} At the college, Key was tutored by sculptor Ted Roocroft and painter [[Harry Rutherford]].

After gaining the National Diploma of Design and the Diploma of Associateship of Manchester, the latter with distinction, Key took up a postgraduate scholarship in [[sculpture]]. His academic awards include the Heywood Medal in Fine Art and the Guthrie Bond Travelling Scholarship.<ref name="GKey"/>

==Career==
[[File:Office Workers 2012.jpg|thumb|right|Office Workers by Geoffrey Key]]
Key’s early work included an important period of development during which he concentrated on painting and drawing a specific area of the [[Derbyshire]] landscape, the Whiteley Nab hill, south of [[Glossop]]. Key created hundreds of images  of this one landscape. Key later revealed that the purpose of this dedicated period of study was to build upon the firm foundation established by his academic training, whilst divesting himself of the influences he had absorbed in order to arrive at his own personal artistic language.<ref name="GKey"/>{{rp|13}} 

During this time, Key also worked as an art teacher at [[Broughton High School, Salford, Greater Manchester|Broughton High Secondary School]] in [[Salford, Greater Manchester|Salford]].  Key left this job as his reputation grew and galleries such as Salford Art Gallery, The Rutherston Loan Collection and North West Arts began acquiring his works.<ref name="GKey"/>{{rp|33}}

Key was elected to membership of the [[Manchester Academy of Fine Arts]] in 1968 and was a prize winner in 1971.<ref name="GKey"/>{{rp|36}} During this time, he was also commissioned to produce artworks by three companies in [[North West England]] – [[Mather & Platt]], the former Richard Johnson & Nephew company, and the former Wilson’s Brewery.  The Richard Johnson & Nephew pictures are now held by the [[Museum of Science and Industry (Manchester)|Museum of Science and Industry (MOSI)]] in Manchester.<ref name="GKey"/>{{rp|38}} Of the [[Manchester Academy]] Exhibition in 1979, Jane Clifford, writing in ''[[The Daily Telegraph]]'' commented "Perhaps the artist that stands out most is Geoffrey Key" "His female forms show a self confidence which is compelling".<ref name="messums1">{{cite web|url=http://www.messums.com/artist/955/Geoffrey-Key/ |title=Archived copy |accessdate=2013-06-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20130615064223/http://www.messums.com:80/artist/955/Geoffrey-Key |archivedate=2013-06-15 |df= }}</ref>

In the 1980s the French company [[Lactalis|Société des Caves de Roquefort]] also commissioned work.<ref name="GKey"/>{{rp|38}} Further successful exhibitions were held in both the United Kingdom and abroad. In 1987, the [[Manchester Evening News]] asked Key to write an article on [[L. S. Lowry]], who Key had known well, for a feature marking the centenary of the artist's birth. This article was accompanied by a contemporary drawing of Lowry by Key.<ref name="GKey"/>{{rp|22}}

In the early 1990s Key visited [[Hong Kong]] to exhibit his work in a gallery at the [[Mandarin Oriental, Hong Kong|Mandarin Oriental Hotel]].<ref name="GKey"/>{{rp|185}} Key has stated that this, his first visit to Asia, was to prove a catalyst in a change in the use of colour in his work. An earlier predominance of muted tones was replaced with a fuller and more vibrant [[Color scheme|palette]], which has remained an enduring aspect of his art. Of the exhibition, Asian Art News said "It is the emotion and pleasure of making art that comes through so clearly in Key's work and makes it sparkle".<ref name="messums2"/>  
[[File:Pennine Canal 2011.jpg|thumb|right|Pennine Canal by Geoffrey Key]]
Key’s career as an artist now spans five decades and his work and exhibitions have been widely appraised and reviewed.<ref>[https://web.archive.org/web/20140415041410/http://www.everysite.co.uk/sources/4000587/4001884/4032229//press.htm?id=4032229 ]</ref> His paintings and sculpture feature in several public art collections in North West England, including Salford and Manchester Art Galleries.<ref name="BBC Your Art" /> His work is also held in private and corporate collections including the [[National Westminster Bank]], Mandarin Oriental Hong Kong, the [[Hong Kong Jockey Club]], the Chateau de St Ouen and [[Perrier]]. Key has represented the UK in invited exhibitions in Europe.<ref>{{cite web|url=http://www.artdecorgallery.co.uk/geoffrey-key-paintings.htm |title=Archived copy |accessdate=2013-06-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20130527173200/http://www.artdecorgallery.co.uk:80/geoffrey-key-paintings.htm |archivedate=2013-05-27 |df= }}</ref> Shows have taken place across the UK, Europe, Asia, Australia and the USA <ref>[http://www.debretts.com/people/biographies/browse/k/8082/Geoffrey+George.aspx]{{dead link|date=September 2016}}</ref> and his work is exhibited and sold in galleries in the UK, Ireland, the US and Hong Kong.<ref>{{cite web|url=http://www.messums.com/artist/955/Geoffrey-Key/ |title=Archived copy |accessdate=2013-06-23 |deadurl=yes |archiveurl=https://web.archive.org/web/20130615064223/http://www.messums.com:80/artist/955/Geoffrey-Key |archivedate=2013-06-15 |df= }}</ref><ref>{{cite web|author=Geoffrey Key |url=http://www.geoffreykey.com/gkey/exhibitions.aspx |title=Exhibitions |publisher=Geoffrey Key |date= |accessdate=2016-09-15}}</ref>{{better source|date=July 2013}}

In May 2013 Key's work was exhibited at [[Messum's]] Art Gallery in London.<ref name="platform1">{{cite web|url=http://www.platform505.com/geoffrey-key/ |title=Geoffrey Key |publisher=Platform505 |date= |accessdate=2016-09-15}}</ref> Decscribed as one of the most important living painters, prices for Key's work have risen steadily over the last decade.<ref>{{cite news|title=The Bigger Picture |url=http://www.enforbusiness.com/feature/bigger-picture |work=En Magazine |date=September 3, 2009 |accessdate=December 21, 2014}}</ref>

Key has also published several books.<ref>{{cite web|author=Geoffrey Key |url=http://www.geoffreykey.com/gkey/publications.aspx |title=Publications |publisher=Geoffrey Key |date= |accessdate=2016-09-15}}</ref> ''G Key - A Book of Drawings and Interview'' (1975), ''Daydreams'' (1981), ''Clowns'' (2001), ''Geoffrey Key Twentieth Century Drawings'' (2002), ''Images'' (2004), ''Geoffrey Key Paintings'' (2008), ''Birds'' (2010). ''Signature Book'' (2011).<ref>[http://www.debretts.com/people/biographies/.../k/.../Geoffrey+George.aspx]{{dead link|date=September 2016}}</ref>

In 2011 Key was filmed for a documentary about his work and life by [[Andy pacino|Andy Pacino]], a filmmaker and author from Manchester. It had the working title ''Saltglaze Pot''.<ref name="artdecorgallery1">{{cite web|url=http://www.artdecorgallery.co.uk/news-details.htm?news_id%3D6 |title=Archived copy |accessdate=2013-12-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20131213122138/http://www.artdecorgallery.co.uk/news-details.htm?news_id=6 |archivedate=2013-12-13 |df= }}</ref>

Speaking with the Manchester Evening News in October 2009 Key said of his style and manner of working "If I'm doing a still life. I'll set it up, look at it for an hour or so, put it away and then paint it. I find the mind's great at distilling, at breaking it down into essentials.  If I'm looking at it I find myself copying every detail, and that isn't necessary. I couldn't sit in a field painting a landscape because I would just feel I'm copying nature when photography can do a better job. Painting it from memory, recreating in on canvas can do quite another job".<ref name="messums1"/>

==References==
{{Reflist}}

==Other news coverage==
* [http://www.cheshire-today.co.uk/32270/geoffrey-key-an-artist-of-significance/ Geoffrey Key an artist of significance], Cheshire Today 2015
* [http://www.platform505.com/geoffrey-key/ The Figurative Art of Everyday Life by Geoffrey Key], Platform505.com, 10 June 2013
* [http://www.messums.com/clippings/Geoffrey_Key_GeoffreyKey_LancashireLife.jpg Key Unlocks the Past], ''Lancashire Life'', October 2009 Edition page 60
* [http://www.lancashirelife.co.uk/out-about/manchester_artist_geoffrey_key_shows_individuality_1_1633429 Manchester artist Geoffrey Key shows individuality], ''Lancashire Life'', 28 December 2009 
* [http://www.messums.com/clippings/Geoffrey_Key_GeoffreyKey_ManchesterEveningNews_2009.jpg Light is a key to a great leap forward], ''Manchester Evening News Edition'', Friday October 16, 2009
* Jane Clifford [http://www.messums.com/clippings/Geoffrey_Key_GeoffreyKey_TheTelegraph_1979.jpg Humour and colour in new-look academy], ''The Telegraph'', February 2, 1979
* [http://www.salfordonline.com/salfordvideos_page/22454-video:_geoffrey_key,_salford's_best_kept_secret_.html Video: Geoffrey Key, Salford's best kept secret], Salfordonline, 25 August 2010
* {{cite web|author= |url=http://www.cheshire-today.co.uk/14696/geoffrey-key-at-messums/ |title=Geoffrey Key at Messums &#124; News |publisher=Cheshire Today |date= |accessdate=2016-09-15}}
* {{cite web|url=http://www.incheshiremagazine.co.uk/the-art-of-investment/ |title=The art of investment |publisher=INCheshire Magazine |date=2013-03-27 |accessdate=2016-09-15}}

==Bibliography==
* The Northern School: A Reappraisal, Martin Regan, ISBN 978-1-5272-0320-4
* The Northern School Revisited, Peter Davies, ISBN 978-0-9552591-4-2
* Geoffrey Key Paintings, Judith M O'Leary ISBN 978-0955911705
* Infinite Jest, Judith M O'Leary, ISBN 978-0955911767

==External links==
*[https://www.youtube.com/watch?v=8h4qR2Yb64k In the footsteps of Lowry]How Manchester artists sought to escape the overwhelming influence of L.S.Lowry. Featuring Geoffrey Key and Reg Gardner with commentary by John Robert-Blunn.
*{{cite web|url=https://www.youtube.com/watch?v=TzCdsyMIMGU |title=Geoffrey Key Paintings For Sale :: Geoffrey Key Artist |publisher=[[YouTube]] |date=2010-12-26 |accessdate=2016-09-15}}
*{{cite web|url=http://www.andypacino.com/ |title=Andy Pacino – Artist – Writer |website=Andypacino.com |date= |accessdate=2016-09-15}}
* [http://www.gateway-gallery.co.uk/artists/geoffrey-key/ Geoffrey Key's work at Gateway Gallery]

{{Authority control}}
{{DEFAULTSORT:Key, Geoffrey}}
[[Category:1941 births]]
[[Category:Living people]]
[[Category:20th-century British painters]]
[[Category:British male painters]]
[[Category:21st-century British painters]]
[[Category:20th-century British sculptors]]
[[Category:British male sculptors]]