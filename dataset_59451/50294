{{Infobox person 
| name        = Mana Saeed Al Otaiba
| residence   = 
| other_names = Mani Said Al Utaybah, الدكتور مانع سعيد العتيبة 
| image       =
| imagesize   = 
| caption     = 
| birth_date  = 15 May 1946
| birth_place = [[Abu Dhabi]], [[United Arab Emirates|UAE]]
| death_date  =
| death_place = 
| death_cause = 
| known       = 
| occupation  = [[politician]], [[economist]], [[businessman]], [[poet]], [[novelist]]
| title       = [[Excellency|His Excellency]]
| salary      = 
| term        = 
| predecessor = 
| successor   = 
| party       = 
| boards      = 
| religion    = Liberal [[Muslim]]
| spouse      = 
| partner     = 
| relations   = [[Moza Saeed Al Otaiba]] (sister)<br>[[Yousef Al Otaiba]] (son) Khalid AlOtaiba (son)
| website     = 
| footnotes   = 
| employer    = 
| height      = 
| weight      = 
}}
'''Mana Al Otaiba''' ({{lang-ar|الدكتور مانع العتيبه}}) was born on 15 May 1946 to [[Saeed Al Otaiba]] in [[Abu Dhabi]] in the [[United Arab Emirates]]. Little else is known about Al Otaiba's personal life. Al Otaiba is the former Minister of Petroleum and Mineral Resources of the United Arab Emirates under the Presidency of [[Sheikh]] [[Zayed bin Sultan Al Nahyan]].<ref> http://query.nytimes.com/gst/fullpage.html?res=9C01E3D7123BF931A35751C0A967948260</ref> Al Otaiba then became his Personal Adviser until the president's death, after which he became the Private Advisor to Sheikh [[Khalifa bin Zayed Al Nahyan]] ,<ref>http://www.ameinfo.com/94172.html</ref> as well as a member of the Royal Moroccan Academy under [[King Hassan II]].

==Politics==
Al Otaiba is an [[Arab]] [[diplomat]] who is sought for advice by Arab and international leaders because of his economic experience and his high levels of experience in the diplomatic circuit. His son [[Yousef Al Otaiba]] became the UAE ambassador to the United States.

Mana Al Otaiba served as President of [[OPEC]] a record six times, for its 26th, 52nd, 53rd, 54th, 62nd and 63th conferences, held during 1971–1983.<ref>http://www.opec.org/library/General%20Information/pdf/geninfo.pdf</ref> He caused controversy in the 1970s by disregarding OPEC agreements that he considered harmful to the economy of the UAE, his most famous quote being "I always sign" whenever asked about whether he would comply with an agreement.<ref>https://query.nytimes.com/gst/fullpage.html?res=950DE0DA113DF93BA35755C0A96F948260&sec=&spon=&pagewanted=all</ref>

==Business==
Al Otaiba is the former chairman of [[Noor Capital]],<ref>http://www.noorcapitaluae.com/?s=section&sectionID=5</ref> a firm dealing in asset management, private equity, investment banking, investment placement and direct equity. He is also a major shareholder in [[Abu Dhabi Group]],<ref>http://www.zawya.com/cm/profile.cfm/cid1000838/</ref> a company with holdings in real estate, banking, Islamic banking, telecommunication, ISP, manufacturing, pharmaceuticals, hotels and tourism, as well as being a major shareholder in [[Dana Gas]],<ref>http://www.danagas.ae/uae.html</ref> which is the first regional private-sector natural gas company in the Middle East. Al Otaiba is the owner of the Royal Mirage Hotels in [[Morocco]] (formerly part of [[Sheraton Hotels and Resorts]]), which include the Royal Mirage Marrakech, The Royal Mirage Marrakech Deluxe, the Royal Mirage Fes, and the Royal Mirage Agadir. He also owns [[Maissoune]], a business venture that has a significant presence in Morocco.<ref>http://www.galileo.co.ae/Morepress.html</ref> His sister [[Moza Saeed Al Otaiba]] is a prominent businesswoman and philanthropist. The rest of his business activity is unknown due to his very private nature.<ref>https://wikileaks.org/plusd/cables/1975ABUDH02265_b.html</ref>

==Culture==
Al Otaiba has published more than 42 books of poetry, written in colloquial [[Arabic]], formal Arabic, and [[English language|English]]. His writing was renowned even before the unification of the Emirates in 1972.<ref>http://archive.gulfnews.com/uae/sharjah/more_stories/34543.html</ref> Al Otaiba has written novels (including ''Karima'', which became the basis for a television series)<ref>http://www.foldedup.com/ps-media-exclusive-media-representative/karima</ref> and several non-fiction books including ''Essays on Petroleum'',<ref>{{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1982|title=Essays on  Petroleum |publisher=Routledge Kegan & Paul |language=En|isbn=978-0709919216}}</ref> ''The Petroleum Concession Agreements'',<ref>{{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1982|title=The Petroleum Concession Agreements of the United Arab Emirates |publisher=Routledge Kegan & Paul |language=En|isbn=978-0709919155}}</ref> and ''OPEC and the Petroleum Industry''.<ref>{{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1975 |title=OPEC and the Petroleum Industry |publisher= Croom Helm|language=En |isbn=978-0856642623}}</ref> He has been awarded several honorary doctorates, including a Doctorate of Law from [[Keio University]] in [[Japan]],<ref>http://www.keio.ac.jp/english/keio_in_depth/keio_view/004.html</ref> a Doctorate of Law from the [[University of Manila]] in the Philippines,<ref>http://furat.alwehda.gov.sy/_archive.asp?FileName=98906555420070826012120</ref> and a Doctorate of Economics from the [[University of São Paulo]] in [[Brazil]].

==Books==
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1971 |title=The Economy of Abu Dhabi, Ancient and Modern |publisher=Beirut, Commercial and Industrial Press |language= |isbn=}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year= |title=The Abu Dhabi Planning Board |publisher= |language= |isbn=}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1975 |title=OPEC and the Petroleum Industry |publisher= Croom Helm|language=En |isbn=978-0856642623}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed|year=1977 |title=Petroleum and the Economy of the United Arab Emirates |publisher= Croom Helm |language=En |isbn= 978-0856645198}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1982|title=The Petroleum Concession Agreements of the United Arab Emirates |publisher=Routledge Kegan & Paul |language=En|isbn=978-0709919155}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year=1982|title=Essays on Petroleum |publisher=Routledge Kegan & Paul |language=En|isbn=978-0709919216}}
* {{Cite book |last=Al-Otaiba |first=Mana Saeed |year=2008|title=Dialogue of Civilizations: The Self and the Other |publisher=Red Sea Press,U.S.|language=En|isbn=978-1569022948}}

==See also==
*[[Otaibah]]
*[[List of Arabic language poets]]

==References==
{{Reflist}}

{{DEFAULTSORT:Otaiba, Mana}}
[[Category:1946 births]]
[[Category:Living people]]
[[Category:People from Abu Dhabi]]
[[Category:Emirati businesspeople]]
[[Category:Emirati economists]]
[[Category:Emirati novelists]]
[[Category:Emirati poets]]
[[Category:Government ministers of the United Arab Emirates]]
[[Category:OPEC people]]