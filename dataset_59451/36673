{{about|the Bristol arts centre|the portrait of [[Giovanni Arnolfini]] by [[Jan van Eyck]]|Arnolfini Portrait}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{good article}}
{{Infobox museum
| name             = Arnolfini
| image            = arnolfini from across the harbour arp.jpg
| caption          = The Arnolfini seen from across the harbour
| alt              =
| map_type         = Bristol
| map dot label            = Arnolfini
| map_caption      = Location map for Arnolfini
| coordinates      = {{coord|51.4492|-2.5972|display=inline,title}}
| established      = 1961
| location         = [[Bristol]], England, UK
| type             = Art Gallery, Performance Arts, Cinema
| website          = http://www.arnolfini.org.uk/
| visitors         = 398,000 (2008/2009)<ref name="visitors">{{cite web
 |url= http://www.charity-commission.gov.uk/SIR/ENDS04%5C0000311504_SIR_09_E.PDF
 |title=Summary Information Return 2009: Arnolfini Gallery Limited|format=PDF|date=26 January 2010|page=2
 |publisher=The Charity Commission|last=Dauncey|first=Sara |accessdate=25 April 2010}}</ref><ref>{{cite web
 |url= http://www.e-architect.co.uk/bristol/arnolfini_icon.htm
 |title=Arnolfini Bristol : Architecture Information + Images
 |work=e-architect |accessdate=25 April 2010}}</ref>
}}

The '''Arnolfini''' is an international [[arts centre]] and gallery in [[Bristol]], England. It has a programme of [[contemporary art]] exhibitions, artist's performance, music and dance events, poetry and book readings, talks, lectures and cinema. There is also a specialist art bookshop and a café bar. Educational activities are undertaken and experimental digital media work supported by online resources. A number of festivals are regularly hosted by the gallery. Arnolfini is funded by [[Bristol City Council]] and [[Arts Council England]], with some corporate and individual supporters.

The gallery was founded in 1961 by [[Jeremy Rees]], and was originally located in [[Clifton, Bristol|Clifton]]. In the 1970s it moved to [[Queen Square, Bristol|Queen Square]], before moving to its present location, Bush House on Bristol's waterfront, in 1975. The name of the gallery is taken from [[Jan van Eyck]]'s 15th-century painting, ''[[The Arnolfini Portrait]]''. Arnolfini has since been refurbished and redeveloped in 1989 and 2005. Artists whose work has been exhibited include [[Bridget Riley]], [[Rachel Whiteread]], [[Richard Long (artist)|Richard Long]] and [[Jack Yeats]]. Performers have included [[Goat Island (performance group)|Goat Island Performance Group]], the [[Philip Glass Ensemble]], and [[Shobana Jeyasingh Dance Company]]. The gallery reached a new audience in April 2010, when it was chosen to host one of the three [[United Kingdom general election debates, 2010|2010 general election debates]].

==History==
Jeremy Rees started Arnolfini with the assistance of his wife Annabel, and the painter John Orsborn in 1961. The original location was above a bookshop in the Triangle in Clifton, Bristol. In 1968, Rees was able to give up his teaching job and with the aid of private funding and Arts Council funding relocated the gallery to Queen Square, then to E Shed, the current home of the [[Watershed Media Centre]]. In 1975, Arnolfini moved to its present home in Bush House, occupying two floors of a 19th-century Grade II* [[listed building|listed]] tea warehouse situated on the side of the [[Floating Harbour]] in [[Bristol city centre]]. The remainder of the building was office space leased out by developers JT Group.<ref name="Rees-ONDB">{{cite web |url= http://www.oxforddnb.com/view/article/93024?docPos=14 |title=Rees, Jeremy Martin|subscription=yes |date=May 2007|last=Hedley|first=Gill
 |work=Oxford Dictionary of National Biography |accessdate=25 April 2010}}</ref><ref>{{cite web
 |url=http://www.imagesofengland.org.uk/details/default.aspx?id=380204 
 |archivedate=12 October 2008 
 |title=Bush House 
 |work=Images of England 
 |accessdate=25 April 2010 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20081012131632/http://www.imagesofengland.org.uk:80/details/default.aspx?id=380204 
 |df=dmy 
}}</ref><ref>{{cite web
 |url=http://www.lookingatbuildings.org.uk/default.asp?document=3.C.1.1|archiveurl= https://web.archive.org/web/20050501162119/www.lookingatbuildings.org.uk/default.asp?document=3.C.1.1|archivedate=1 May 2005
 |title=Arnolfini Arts Centre Conversion
 |work=Looking at Buildings|publisher=Pevsner Architectural Guides
 |accessdate=26 April 2010
}}</ref>

The architect of Bush House was [[Richard Shackleton Pope]], who constructed first the south part of the warehouse (1831) then extended it to the north in 1835–1836. Its original use was as a warehouse for local iron foundry D., E. & A. Acraman. The building has a [[Pennant Sandstone]] exterior with arched ground level entrances and arched windows above. This style of architecture is the first example of the [[Bristol Byzantine]] style which became popular in the 1850s. Later conversion to a tea warehouse added interstitial floors.<ref>{{cite web | title=Bush House | work=Looking at Buildings |publisher=Pevsner Architectural Guides|url=http://www.lookingatbuildings.org.uk/default.asp?document=3.C.1|archiveurl=https://web.archive.org/web/20070927235425/http://www.lookingatbuildings.org.uk/default.asp?document=3.C.1|archivedate=27 September 2007 | accessdate=19 May 2007}}</ref>

Originally dedicated to exhibiting the work of artists from the West of England, under the directorship of [[Barry Barker]] (1986–1991)<ref>{{cite web
 |url=http://artsresearch.brighton.ac.uk/research/academic/barker 
 |title=Barry Barker: Biography 
 |work=Centre for Research and Development 
 |publisher=University of Brighton, Faculty of Arts 
 |accessdate=25 April 2010 
 |archiveurl=https://web.archive.org/web/20100508064105/http://artsresearch.brighton.ac.uk/research/academic/barker 
 |archivedate=8 May 2010 
 |deadurl=yes 
 |df=dmy 
}}</ref> the gallery moved towards a more general spread of [[contemporary art]]. Barker supervised a successful refurbishment of the gallery spaces and café bar by [[David Chipperfield]]. Before development work began, the Arnolfini was attracting over 285,000 visitors per year.<ref name="Rees-ONDB"/> Subsequent Directors have been [[Tessa Jackson]] (1991–1999),<ref>{{cite web
 |url= http://www.iniva.org/library/archive/people/j/jackson_tessa
 |title=Tessa Jackson
 |work=Iniva |accessdate=25 April 2010}}</ref> Caroline Collier (1999–2005)<ref>{{cite web
 |url= http://www.tate.org.uk/about/theorganisation/seniorstaff/
 |archiveurl= https://web.archive.org/web/20100322232030/http://www.tate.org.uk/about/theorganisation/seniorstaff/
 |archivedate= 22 March 2010
 |title=Senior Tate Staff
 |work=Tate
 |accessdate=25 April 2010
}}</ref> and [[Tom Trevor]] (2005-2013).

As part of a two-year development project that finished in September 2005,<ref>{{cite web
 |url= http://www.building.co.uk/story.asp?storycode=3048040
 |title=The Arnolfini wedding
 |work=Building|year=2005|issue=10|last=Lane|first=Thomas
 |accessdate=25 April 2010
}}</ref><ref>{{cite web|url=http://www.arup.com/unitedkingdom/project.cfm?pageid=9195|archiveurl=https://web.archive.org/web/20080607002656/http://www.arup.com/unitedkingdom/project.cfm?pageid=9195|archivedate=7 June 2008|title=Arnolfini Gallery|work=Arup|accessdate=31 December 2008}}</ref>
the old warehouse has been fully redeveloped, adding another attic storey. Arnolfini now occupies the lower three floors and basement, and the upper floors are leased to help pay for the running costs.<ref>{{cite journal|last=Gregory|first=Rob|date=August 2005|title=New Order|journal=The Architectural Review|volume=218|pages=20–21|issn=0003-861X|url=http://web.ebscohost.com/ehost/viewarticle?data=dGJyMPPp44rp2%2fdV0%2bnjisfk5Ie46a9JsKmwSLak63nn5Kx95uXxjL6qrUm1pbBIr6ieT7intVKvrp5oy5zyit%2fk8Xnh6ueH7N%2fiVa%2bvrk63q7RNsq2khN%2fk5VXj5KR84LPffvKc8nnls79mpNfsVbOms0myq7ZPtJzkh%2fDj34y73POE6urjkPIA&hid=111|accessdate=25 April 2010|subscription=yes }}</ref> One tenant is the School of Creative Arts, part of the [[University of the West of England]].<ref>{{cite web |url= http://info.uwe.ac.uk/news/uwenews/article.asp?item=1436
 |title=UWE launches new space for start-up businesses |work=University of the West of England|date=23 February 2009
 |accessdate=25 April 2010| archiveurl= https://web.archive.org/web/20100311155902/http://info.uwe.ac.uk/news/UWENews/article.asp?item=1436| archivedate= 11 March 2010 <!--DASHBot-->| deadurl= no}}</ref> Funding for this development was received from the [[National Lottery (UK)|National Lottery]] and the Barker-Mill Trust, set up by long term Arnolfini patron Peter Barker-Mill.<ref>{{cite news |url= http://www.lexisnexis.com/uk/nexis/results/docview/docview.do?docLinkInd=true&risb=21_T9179568165&format=GNBFI&sort=BOOLEAN&startDocNo=1&resultsUrlKey=29_T9179549031&cisb=22_T9179568168&treeMax=true&treeWidth=0&csi=169745&docNo=18
 |title=Arnolfini's lottery win aids Euro culture bid|work=Bristol Evening Post, archived at [[LexisNexis]]|publisher=Bristol News and Media|subscription=yes |date=28 September 2001|last=Marsh|first=Suzannah
 |accessdate=25 April 2010}}</ref><ref>{{cite news
 |url= http://www.lexisnexis.com/uk/nexis/results/docview/docview.do?docLinkInd=true&risb=21_T9179568165&format=GNBFI&sort=BOOLEAN&startDocNo=1&resultsUrlKey=29_T9179549031&cisb=22_T9179568168&treeMax=true&treeWidth=0&csi=8200&docNo=7 |title=Obituary: Peter Barker-Mill|date=25 June 1994|last=Rees|first=Jeremy|subscription=yes |work=The Independent, archived at [[LexisNexis]]|publisher=Newspaper Publishing|location=London
 |accessdate=25 April 2010}}</ref> The original committee to support Arnolfini included Peter Barker-Mill, Ann Hewer, and [[Lawrence Ogilvie]].<ref>Arnolfini archives</ref>

==Archives==
Records of Arnolfini, including a collection of artist books, are held at [[Bristol Archives]] (Ref. 44371) ([http://archives.bristol.gov.uk/Record.aspx?src=CalmView.Catalog&id=43371 online catalogue]).

==Name==
[[File:ArnolfiniCabot.jpg|thumb|right|[[John Cabot]]'s statue outside the Arnolfini]]
The Arnolfini is named after [[Jan van Eyck]]'s masterpiece ''[[The Arnolfini Portrait]]'' (1434) depicting the merchant and arts patron [[Giovanni Arnolfini]]. ''The Arnolfini Portrait'' is one of the earliest paintings to assert the presence of the artist within its depiction (an inscription in the middle of the work and a reflection in a mirror on the back wall) and one of Arnolfini's consistent concerns: to explore the role of artist as a witness and recorder of what is around them – contemporary society. The painting is in the [[National Gallery, London]] and it was one of the founder's favourite paintings.<ref name="Rees-ONDB"/><ref name="Debate">{{cite web
 |url= http://www.arnolfini.org.uk/news/index/53
 |title=Sky News Debate at Arnolfini
 |work=Arnolfini
 |accessdate=25 April 2010
| archiveurl= https://web.archive.org/web/20100426010430/http://www.arnolfini.org.uk/news/index/53| archivedate= 26 April 2010 <!--DASHBot-->| deadurl= no}}</ref>

==Today==
Arnolfini has three floors of galleries, a specialist arts bookshop, a cinema which can also be used as a performance space for theatre, live art, dance and music, a reading room that provides reference material for all past exhibitions and wide range of books and catalogues, and a café bar. Entrance to the galleries is free of charge. Notable exhibitions have included works by [[Bridget Riley]], [[Richard Long (artist)|Richard Long]], [[Rachel Whiteread]], [[Paul McCartney]], [[Angus Fairhurst]] and [[Louise Bourgeois]].<ref>{{cite web
 |url= http://dev.arnolfini.org.uk/about/history.php
 |title=History |work=Arnolfini |accessdate=25 April 2010}}</ref> Regular events include poetry and film festivals, live art and dance performances, lectures and jazz and experimental music concerts, including Bodies in Flight, [[Goat Island (performance group)|Goat Island Performance Group]], [[Akram Khan (dancer)|Akram Khan]], the [[London Sinfonietta]], the [[Philip Glass Ensemble]], [[Wayne McGregor Random Dance|Random Dance]], and [[Shobana Jeyasingh Dance Company]].<ref name="Debate"/>

There is an active access and education programme, hosting visits from students, workshops with artists, presenting interpretative information and offering some work experience placements within the gallery.<ref>{{cite web
 |url= http://www.arnolfini.org.uk/pages/learning/
 |title= Interaction
 |work=Arnolfini
 |accessdate=26 April 2010
| archiveurl= https://web.archive.org/web/20100406163909/http://www.arnolfini.org.uk/pages/learning/| archivedate= 6 April 2010 <!--DASHBot-->| deadurl= no}}</ref> ''project.ARNOLFINI'' is an online experimental web site with dumps of digital media related to Arnolfini exhibitions and events, past and present, which may be reorganised by any online user, utilising resources on the web site to create new works and projects under a [[copyleft]] license.<ref>{{cite web |url= http://project.arnolfini.org.uk/?t=1 |title=project.ARNOLFINI |work=Arnolfini |accessdate=25 April 2010}}</ref>
<ref>{{cite web |url= http://www.intute.ac.uk/cgi-bin/fullrecord.pl?handle=artifact2511
 |title=Arnolfini |work=Intute |publisher=Intute consortium|accessdate=26 April 2010}}</ref>
Arnolfini also hosts events from outside organisations, including the ''Encounters Short Film Festival'' (along with the [[Watershed Media Centre]]),<ref>{{cite web |url= http://www.encounters-festival.org.uk/blog/call-for-submissions-the-uk-film-councils-short-film-completion-fund-2010.html |archiveurl= https://web.archive.org/web/20091227150549/http://www.encounters-festival.org.uk/blog/call-for-submissions-the-uk-film-councils-short-film-completion-fund-2010.html |archivedate= 27 December 2009
 |title=encounters short film festival
 |work=encounters-festival.org.uk
 |accessdate=25 April 2010
}}</ref> [[Mayfest (Bristol)|Mayfest]],<ref>{{cite web
 |url= http://issuu.com/mayfestbristol/docs/mayfest_2010_brochure
 |title=Mayfest 2010 Brochure
 |work=issuu.com
 |accessdate=25 April 2010
| archiveurl= https://web.archive.org/web/20100602031948/http://issuu.com/mayfestbristol/docs/mayfest_2010_brochure| archivedate= 2 June 2010 <!--DASHBot-->| deadurl= no}}</ref> the first Festival of British Independent Cinema,<ref>{{cite web
 |url= http://www.studycollection.co.uk/postershow/2/intro.html
 |title=British Avant Garde Film Poster Art 1966–85
 |work=studycollection.co.uk
 |accessdate=25 April 2010
}}</ref> the biennial {{sic|hide=y|In|between}} Time Festival of Live Art and Intrigue<ref>{{cite news
 |url= http://www.realtimearts.net/article/72/8413
 |title=The Australia-UK connection|date=April–May 2006
 |work=RealTime Arts
 |accessdate=25 April 2010
}}</ref> and the Bristol Artists Book Events.<ref>{{cite web
 |url= http://www.bookarts.uwe.ac.uk/babe09.htm
 |title=BABE: Bristol Artist’s Book Event at Arnolfini
 |work=CFPR Book Arts
 |accessdate=25 April 2010
}}</ref> In April 2010, [[British Sky Broadcasting]] chose Arnolfini to host the second of the three [[United Kingdom general election debates, 2010|2010 general election debates]].<ref name="Debate"/>

Arnolfini receives funding from [[Arts Council England]],<ref>{{cite web
 |url= http://www.artscouncil.org.uk/rfo/arnolfini/
 |archiveurl= https://web.archive.org/web/20110613181155/http://www.artscouncil.org.uk/rfo/arnolfini/
 |archivedate= 13 June 2011
 |title=Arnolfini |work=Arts Council England
 |accessdate=25 April 2010
}}</ref> and Bristol City Council.<ref>{{cite web
 |url= http://www.bristol.gov.uk/ccm/content/Leisure-Culture/Arts-Entertainment/key-arts-providers.en
 |title=Arts – information and advice: Key Arts Providers
 |work=Bristol City Council
 |accessdate=25 April 2010
}}</ref> According to returns lodged with the [[Charity Commission]] for the year ending in March 2009, Arnolfini had 398,000 visitors in 2008/2009.<ref name="visitors"/> Income was £2.08 million and expenditure was £2.4 million<ref>{{cite web
 |url= http://www.charity-commission.gov.uk/Accounts/Ends04%5C0000311504_AC_20090331_E_C.PDF
 |title=Annual Report and Financial Statements for the year ended 31 March 2009|format=PDF|page=14
 |publisher=The Charity Commission|last=Grant Thornton Accountants
 |accessdate=25 April 2010
}}</ref> and the gallery employed 44 people.<ref>{{cite web
 |url= http://www.charity-commission.gov.uk/SHOWCHARITY/RegisterOfCharities/CharityWithPartB.aspx?RegisteredCharityNumber=311504&SubsidiaryNumber=0
 |title=Charity overview: Arnolfini Gallery Limited
 |work=The Charity Commission
 |accessdate=25 April 2010
}}</ref>

==See also==
*[[Royal West of England Academy]]

==References==
{{Reflist|30em}}

==External links==
{{commons category|The Arnolfini}}
* {{Official website|http://www.arnolfini.org.uk}}
* [https://web.archive.org/web/20070927235425/http://www.lookingatbuildings.org.uk/default.asp?document=3.C.1 Bush House], [[Nikolaus Pevsner|Pevsner Architectural Guides]]

{{Culture in Bristol}}

[[Category:Commercial buildings completed in 1836]]
[[Category:1836 establishments in England]]
[[Category:Grade II* listed buildings in Bristol]]
[[Category:Art museums and galleries in Bristol]]
[[Category:Museums in Bristol]]
[[Category:Bristol Harbourside]]
[[Category:Music venues in Bristol]]
[[Category:Art museums established in 1961]]
[[Category:1961 establishments in England]]
[[Category:Grade II* listed museum buildings]]