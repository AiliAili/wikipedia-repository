{{Refimprove|date=November 2010}}
{{Infobox journal
| title         = Journal of Applied Psychology
| cover         = [[File:Journal of Applied Psychology.gif]]
| editor        = Gilad Chen (Incoming)
| discipline    = [[Applied psychology]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = J. Appl. Psychol.
| publisher     = [[American Psychological Association]]
| country       = United States
| frequency     = Bimonthly
| history       = 1917-present
| openaccess    = 
| license       = 
| impact        = 3.810
| impact-year   = 2015
| website       = http://www.apa.org/pubs/journals/apl/index.aspx
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 476130184
| LCCN          = 19012586
| CODEN         = 
| ISSN          = 0021-9010
| eISSN         = 1939-1854
| boxwidth      = 
}}
The '''''Journal of Applied Psychology''''' is a bimonthly, [[Peer review|peer-reviewed]] [[academic journal]] published by the [[American Psychological Association]]. The journal emphasizes the publication of original investigations that contribute new knowledge and understanding to fields of applied psychology (other than clinical and applied experimental or human factors, which are more appropriate for other American Psychological Association journals). The journal primarily considers empirical and theoretical investigations that enhance understanding of cognitive, motivational, affective, and behavioral psychological phenomena."<ref name=home>{{cite web |url=http://www.apa.org/pubs/journals/apl/ |title=Journal of Applied Psychology|publisher=[[American Psychological Association]] |date= |accessdate=2012-07-30}}</ref> The incoming [[editor-in-chief]] is Gilad Chen ([[University of Maryland]]).

==Abstracting and indexing==
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 3.810, ranking it 5th out of 79 journals in the category "Psychology, Applied"<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Psychology, Applied |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |accessdate=2016-06-27 |series=Web of Science |postscript=.}}</ref> and 17th out of 192 journals in the category "Management".<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Management |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2016-06-27 |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
*[http://www.apa.org/pubs/journals/apl/index.aspx Journal of Applied Psychology.]

{{Psychology}}

{{DEFAULTSORT:Journal Of Applied Psychology}}
[[Category:American Psychological Association academic journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1917]]
[[Category:Applied psychology journals]]


{{psych-journal-stub}}