{|{{Infobox aircraft begin
 | name= Rs.I
 | image=Dornier RS1 Giant Flying Boat.JPG
 | caption=
}}{{Infobox aircraft type
 | type=[[Maritime patrol aircraft|Patrol]] [[flying boat]]
 | national origin=[[Germany]]
 | manufacturer=[[Zeppelin-Lindau]]<ref name="Haddow">{{cite book|last=Haddow|first=G.W.|author2=Peter M. Grosz|title=The German Giants - The German R-Planes 1914-1918|publisher=Putnam|location=London|date=1988|edition=3rd|isbn=0-85177-812-7}}</ref>
 | designer=[[Claudius Dornier]]<ref name="Haddow"/>
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built= 1<ref name="Haddow"/>
 | developed from=
 | variants with their own articles=
}}
|}

The '''Zeppelin-Lindau Rs.I''' (known incorrectly postwar as the Dornier Rs.I)  was a large three-engined biplane flying boat, designed by [[Claudius Dornier]] and built during 1914–15 on the German side of [[Lake Constance]]. It never progressed beyond taxi-ing trials as it was destroyed in a storm.<ref name="Haddow"/>

==Design and development==
Claudius Dornier made a distinct impression with Count [[Ferdinand von Zeppelin]] while working on the still-born giant civil airship in 1913, who promptly set him up as chief designer of the Zeppelin-Werke at [[Lindau]], responsible for building large patrol flying boats. Dornier's first design to be built was the Rs.I. This was a large aircraft (''[[Riesenflugzeug]]'' in the German classification) constructed largely of high-strength steel for highly stressed parts, and [[Duralumin]] (aluminium alloy) for low stress parts.
The large biplane wings sat atop the fuselage and were braced with four complete sets per side of [[Warren girder|Warren strut]] style interplane structures comprising 'V' struts, which obviated the need for drag inducing wire bracing. The wing structure was formed with built-up steel spars, four in the top wing and three in the lower wing, and duralumin ribs riveted to the spars and braced internally. The fuselage was also made up from formed steel members built up into a framework which was then covered with fabric or [[duralumin|dural]] sheeting.
The powerplant arrangements were unorthodox, with the two outboard engines housed inside the bulky fuselage  each driving a pusher propeller  via shafts and bevel gearboxes, and a central pusher engine in a nacelle between the wings.<ref name="Haddow"/>

==History==
The Rs.I was completed by October 1915 and rolled out at [[Seemos]] for trials. On 23 October, during a taxi test, the port propeller and/or gearbox parted company with the aircraft causing damage to the gearbox mountings and the upper wing. The opportunity was taken to move the outboard engines into nacelles identical to the centre engine and mount them between the wings, on an independent structure with cat-walks for engineers to attend to engines in flight. This gave much better clearance for the propellers from spray, which was very likely to have been the cause of the port gearbox/propeller incident. Taxi trials recommenced but with little success till on 21 December 1915 a [[Foehn wind]] blew up during trials. Unable to beach the giant flying boat, attempts were made to ride out the storm on the lake, but the moorings gave and the Rs.I was dashed to pieces on the lakeside rocks.
This giant aircraft is noteworthy for the construction materials used, let alone the sheer size of it, being the largest aeroplane in the world at the time of its launch.<ref name="Haddow"/>

==Specifications (Dornier Rs.I)==
{{Aircraft specs
|ref=The German Giants<ref name="Haddow"/>
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->

<!--
        General characteristics
-->
|genhide=

|crew=at least 7
|capacity=
|length m=29
|length ft=
|length in=
|length note=
|span m=43.5
|span ft=
|span in=
|span note=
|height m=7.2
|height ft=
|height in=
|height note=
|wing area sqm=328.8
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=7,500
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=10,500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=3
|eng1 name=[[Maybach HS]] (Mb.IV)
|eng1 type=
|eng1 kw=<!-- prop engines -->179
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=<!-- for different engine types -->
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=<!-- for different engine types -->
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=Y

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|guns= 
|bombs=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[AD Seaplane Type 1000]]
*[[Felixstowe F.2|Felixstowe F.2A]]
|lists=<!-- related lists -->
* [[List of seaplanes and amphibious aircraft]]
* [[List of aircraft]]
}}

==References==
;Notes
{{Reflist}}
;Bibliography
*Haddow G.W., Grosz,P.M. ''The German Giants''. Putnam, 3rd Ed. 1988  ISBN 0-85177-812-7

==External links==
{{Commons category|Dornier Rs.I}}
*http://www.iren-dornier.com/en/aircraft.html

{{Dornier aircraft}}
{{Zeppelin aircraft}}

[[Category:German patrol aircraft 1910–1919]]
[[Category:Flying boats]]
[[Category:Three-engined pusher aircraft]]
[[Category:Mid-engined aircraft]]
[[Category:Biplanes]]
[[Category:Dornier aircraft|RS.1]]