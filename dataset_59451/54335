{{Use British English|date=October 2011}}
{{Use dmy dates|date=August 2015}}
{{Infobox Governor  
| image = Marjorie Jackson.jpg 
| caption = Marjorie Jackson-Nelson in 2007
| honorific-prefix    = The Honourable   
|name                 = Marjorie Jackson-Nelson  
| honorific-suffix    = {{post-nominals|country=AUS|size=100%|AC,|CVO,|MBE}} 
| order               = 33rd 
| office              = Governor of South Australia
| term_start          = 3 November 2001
| term_end            = 8 August 2007
| lieutenant          = 
| premier             = [[Rob Kerin]] <small>(2001–02)</small><br/>[[Mike Rann]] <small>(2002–07)</small>
| monarch             = [[Elizabeth II|Queen Elizabeth II]]
| predecessor         = Sir [[Eric Neal]] 
| successor           = [[Kevin Scarce]] 
| birth_date          = {{Birth date and age|1931|9|13|df=y}}
| birth_place         = [[Coffs Harbour]], New South Wales
| death_date          =
| death_place         =
| nationality         =  Australian
| spouse              = [[Peter Nelson (cyclist)|Peter Nelson]] <small>(1953–77; his death)</small> 
| relations           =
| children            = 
| residence           = [[Marion, South Australia]]
| alma_mater          = 
| occupation          =
| profession          = 
| religion            =
| signature           =
| website             = 
| footnotes           =
}}
{{Infobox sportsperson
|name=
|nickname=
|image=Marjorie Jackson-Nelson 1952.jpg
| image_size     =
|caption=Jackson at a club meeting in Sydney on 12 January 1952 
|birth_date=
|birth_place=
| death_date =
| death_place = 
| height = {{convert|1.72|m|ftin|abbr=on}}
| weight = {{convert|66|kg|lb|abbr=on}}
|sport=Athletics
|event= 
|club=
|pb= 100 m – 11.4 (1952)<br>200 m – 23.59 (1952)<ref name=r1/><ref name=r4/> 
|alma_mater=
| show-medals    = yes
| medaltemplates = 
{{Medal|Country|{{AUS}}}}
{{Medal|Comp|[[Athletics at the Summer Olympics|Olympic Games]]}}
{{Medal|Gold|[[1952 Summer Olympics|1952 Helsinki]]|[[Athletics at the 1952 Summer Olympics|100 m]]}}
{{Medal|Gold|[[1952 Summer Olympics|1952 Helsinki]]|[[Athletics at the 1952 Summer Olympics|200 m]]}}
{{Medal|Competition|[[Commonwealth Games|British Empire and<br> Commonwealth Games]]}}
{{Medal|Gold| [[1950 British Empire Games|1950 Auckland]] | 100 yards}}
{{Medal|Gold| [[1950 British Empire Games|1950 Auckland]] | 220 yards}}
{{Medal|Gold| [[1950 British Empire Games|1950 Auckland]] | 3×110/220 yd}}
{{Medal|Gold| [[1950 British Empire Games|1950 Auckland]] | 4×110/220 yd}}
{{Medal|Gold| [[1954 British Empire and Commonwealth Games|1954 Vancouver]] | 100 yards}}
{{Medal|Gold| [[1954 British Empire and Commonwealth Games|1954 Vancouver]] | 220 yards}}
{{Medal|Gold| [[1954 British Empire and Commonwealth Games|1954 Vancouver]] | 4×110 yards}}
}}

'''Marjorie Jackson-Nelson''' {{post-nominals|country=AUS|size=100%|AC,|CVO,|MBE}} (born 13 September 1931) is a former [[Governor of South Australia]] and a former Australian [[Track and field athletics|athlete]]. She finished her sporting career with two Olympic and seven Commonwealth Games Gold Medals, six individual world records<ref name=r1/> and every Australian State and National title she contested from 1950–1954.<ref name=CT>{{cite news|title=Olympic Order for Lithgow Flash|newspaper=[[The Canberra Times]]|date=16 July 2007|page=4|accessdate=|url=}}</ref>

== Biography ==
Marjorie Jackson was born in [[Coffs Harbour]], New South Wales, and first gained fame when she defeated reigning Olympic 100 and 200 metres champion [[Fanny Blankers-Koen]] a number of times in 1949, thus earning the nickname "the Lithgow Flash", after the New South Wales town of [[Lithgow, New South Wales|Lithgow]] where she lived and had grown up.<ref>{{cite interview|url=http://www.abc.net.au/gnt/history/Transcripts/s1119665.htm|title=GNT History|subjectlink=Marjorie Jackson-Nelson|type=transcript|last=Jackson Nelson|first=Marjorie|interviewer=[[George Negus]]|work=[[George Negus Tonight]]|publisher=[[ABC1]]|date=31 May 2004|accessdate=21 October 2013|quote=}}</ref>

Having won four titles at the 1950 [[Commonwealth Games|British Empire Games]], Jackson came as a favourite to the [[Helsinki]] [[1952 Summer Olympics]].  She won both the 100 m, in a then-world-record-equalling time of 11.5, and the 200 m, winning the first Olympic athletics track titles for Australia since [[Edwin Flack]] in 1896. Having more strong runners in the team, the Australian 4 × 100 m relay team was also a favourite for the gold, but a faulty exchange meant Jackson's chances for third gold medal were gone. The Americans, anchored by [[Catherine Hardy]] (later Lavender), won in an upset, setting a new world record time of 45.9 seconds.<ref name=r1/> Later in 1952, Jackson lowered the 100 m world record time to 11.4, running this new record in a meet at Gifu, Japan on 4 October 1952.<ref name=r4/>

In 1953 Jackson married Olympic cyclist [[Peter Nelson (cyclist)|Peter Nelson]].<ref name=r1/> After his death from [[leukaemia]] in 1977, she launched the Peter Nelson Leukaemia Research Fellowship. Now named Jackson-Nelson, she was one of the eight flag-bearers of the [[Olympic symbols#Flag|Olympic Flag]] at the opening ceremony of the [[2000 Summer Olympics]] in Sydney. She also has a road named in honour of her at the [[Sydney Olympic Park]], beside the [[Sydney Superdome]] (now Allphones Arena). 

==Governor==
In late 2001, Jackson-Nelson was appointed [[Governor of South Australia]]. She relinquished the office on 31 July 2007.<ref name=r1/> On 15 March 2006, Jackson-Nelson was one of the final four runners who carried the [[Queen's Baton Relay|Queen's Baton]] around the [[Melbourne Cricket Ground|MCG]] stadium during the [[2006 Commonwealth Games]] Opening Ceremony in Melbourne. On 6 June 2007, it was announced that a new medical facility to be built in Adelaide will be named the "Marjorie Jackson-Nelson Hospital". On 18 February 2009, Premier Mike Rann agreed to remove her name from the planned hospital.

== Honours ==

* 1953: [[Order of the British Empire|Member of the Order of the British Empire (MBE)]] in the Coronation Honours for her service to women's athletics.<ref>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=1084288&search_type=quick&showInd=true It's an Honour] – Member of the Order of the British Empire</ref>
* 1985: Induction into the [[Sport Australia Hall of Fame]]<ref>{{cite web|url=http://www.sahof.org.au/hall-of-fame/member-profile/?memberID=156&memberType=legends|title=Marjorie Jackson Nelson AC CVO MBE|publisher=Sport Australia Hall of Fame|accessdate=14 December 2013}}</ref>
* 2001: [[Order of Australia|Companion of the Order of Australia (AC)]] upon appointment as governor.<ref>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=885473&search_type=quick&showInd=true It's an Honour] – Companion of the Order of Australia</ref>
* 2002: [[Royal Victorian Order|Commander of the Royal Victorian Order (CVO)]] in February 2002 during [[Elizabeth II of the United Kingdom|Queen Elizabeth II]]'s visit to South Australia.<ref>[http://www.itsanhonour.gov.au/honours/honour_roll/search.cfm?aus_award_id=1130960&search_type=quick&showInd=true It's an Honour] – Commander of the Royal Victorian Order</ref>
* 2007: [[Olympic Order]], the highest order bestowed by the [[International Olympic Committee]].  The citation from the IOC stated that the award was made for her "having illustrated the Olympic ideal through her actions, having achieved remarkable merit in the sporting world and having rendered outstanding service to the Olympic movement through her community work and as Governor of South Australia".<ref name=CT/>

She is also a Dame of the [[Venerable Order of Saint John|Order of St John of Jerusalem]] and a [[Freedom of the City#Freedom of the City of London|Freeman]] of the [[City of London]].

In 1993, the [[State Transit Authority]] of New South Wales named a Sydney [[Sydney RiverCats|RiverCat ferry]] after Jackson-Nelson.

==References==
{{reflist|refs=
<ref name=r1>[http://www.sports-reference.com/olympics/athletes/ja/marjorie-jackson-1.html Marjorie Jackson]. sports-reference.com</ref>
<ref name=r4>[http://trackfield.brinkster.net/Profile.asp?ID=1165&Gender=W Marjorie Jackson (neé Nelson)]. trackfield.brinkster.net</ref>
}}

==Bibliography==
*{{cite book|first=Peter|last=FitzSimons|authorlink=Peter FitzSimons|year=2006|publisher=Harper Collins Publishers|isbn=0-7322-8517-8|title=Great Australian Sports Champions}}
* {{cite web|last=Prentis|first=Malcolm|url=http://nsw.uca.org.au/presbyterian100/greataussies.htm|title=Great Australian Presbyterians: The Game|publisher=Uniting Church in Australia|accessdate=7 March 2007|archiveurl=https://web.archive.org/web/20061211164227/http://nsw.uca.org.au/presbyterian100/greataussies.htm|archivedate=11 December 2006}}

==External links==
{{Commons category}}
* [http://www.governor.sa.gov.au/html/governor.html Biography at the Governor of South Australia website]
* [http://www.cancersa.org.au/aspx/peter_nelson_leukaemia_research_fellowship_fund.aspx Peter Nelson Leukaemia Research Fellowship Fund]

{{s-start}}
{{s-gov}}
{{s-bef|before=[[Eric Neal|Sir Eric Neal]]}}
{{s-ttl|title= [[Governor of South Australia]]|years= 2001–2007}}
{{s-aft|after= [[Kevin Scarce]]}}
{{s-end}}
{{Governors of South Australia}}
{{Footer_Olympic_Champions_100_m_Women}}
{{Footer_Olympic_Champions_200_m_Women}}
{{Footer Commonwealth Champions 100 metres Women}}
{{Footer Commonwealth Champions 200m Women}}
{{IAAF Hall of Fame}}
{{Authority control}}

{{DEFAULTSORT:Jackson-Nelson, Marjorie}}
[[Category:1931 births]]
[[Category:Living people]]
[[Category:Australian female sprinters]]
[[Category:Olympic athletes of Australia]]
[[Category:Athletes (track and field) at the 1952 Summer Olympics]]
[[Category:Australian Commanders of the Royal Victorian Order]]
[[Category:Governors of South Australia]]
[[Category:Australian Members of the Order of the British Empire]]
[[Category:Companions of the Order of Australia]]
[[Category:Sport Australia Hall of Fame inductees]]
[[Category:Dames of Justice of the Order of St John]]
[[Category:People from Coffs Harbour]]
[[Category:Australian Presbyterians]]
[[Category:Olympic gold medalists for Australia]]
[[Category:Commonwealth Games competitors for Australia]]
[[Category:Athletes (track and field) at the 1950 British Empire Games]]
[[Category:Athletes (track and field) at the 1954 British Empire and Commonwealth Games]]
[[Category:Commonwealth Games gold medallists for Australia]]
[[Category:Commonwealth Games medallists in athletics]]
[[Category:Medalists at the 1952 Summer Olympics]]
[[Category:Freemen of the City of London]]
[[Category:Olympic gold medalists in athletics (track and field)]]