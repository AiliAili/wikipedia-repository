{{distinguish|Journal of Luminescence}}
{{Infobox journal
| cover = [[Image:Bio cover new.gif]]
| discipline = [[Biochemistry]]
| abbreviation = Luminescence
| formernames = Journal of Bioluminescence and Chemiluminescence
| editor = L.J. Kricka
| publisher = [[John Wiley & Sons]]
| country =
| history = 1986-present
| frequency = Bimonthly
| impact = 1.675
| impact-year = 2013
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-7243
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-7243/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-7243/issues
| link2-name = Online archive
| ISSN = 1522-7235
| eISSN = 1522-7243
| OCLC = 488597914
| LCCN = sn99047451
| CODEN = LUMIFC
}}
'''''Luminescence: The Journal of Biological and Chemical Luminescence''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]] publishing original scientific papers, short communications, technical notes, and reviews on fundamental and applied aspects of all forms of [[luminescence]], including [[bioluminescence]], [[chemiluminescence]], [[electrochemiluminescence]], [[sonoluminescence]], [[triboluminescence]], [[fluorescence]], time-resolved fluorescence, and [[phosphorescence]]. The current [[editor-in-chief]] is [[L.J. Kricka]] ([[University of Pennsylvania]]). It was established in 1986 by [[John Wiley & Sons]] as the ''Journal of Bioluminescence and Chemiluminescence'' and obtained its current title in 1999.

== Abstracting and indexing ==
''Luminescence''  is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Elsevier BIOBASE]]
* [[Biochemistry & Biophysics Citation Index]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
* [[CAB Abstracts]]
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Life Sciences
* [[Index Medicus]]/[[MEDLINE]]
* [[Inspec]]
* [[METADEX]]
* [[Science Citation Index]]
* [[Scopus]]
* [[VINITI Database RAS]]
* [[The Zoological Record]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.675, ranking it 234th out of 291 journals in the category "Biochemistry & Molecular Biology".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Biochemistry & Molecular Biology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

== Highest cited papers ==
According to the [[Web of Science]], the most-cited papers published in ''Luminescence''  are:
# "The red-edge effects: 30 years of exploration", Volume 17, Issue 1, Jan-Feb 2002, Pages: 19-42, Demchenko AP.
# "Imaging of light emission from the expression of luciferases in living cells and organisms: a review", Volume 17, Issue 1, Jan-Feb 2002, Pages: 43-74, Greer LF, Szalay AA.
# "Analytical applications of flow injection with chemiluminescence detection - a review", Volume 16, Issue 1, Jan-Feb 2001, Pages: 1-23, Fletcher P, Andrew KN, Calokerinos AC, et al.
# "Reporter cell lines are useful tools for monitoring biological activity of nuclear receptor ligands", Volume 16, Issue 2, Mar-Apr 2001, Pages: 153-158, Balaguer P, Boussioux AM, Demirpence E, et al.

== References ==
<references />

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1522-7243}}

[[Category:Biochemistry journals]]
[[Category:Publications established in 1986]]
[[Category:John Wiley & Sons academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]