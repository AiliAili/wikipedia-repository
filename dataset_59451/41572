{{Warbox
|conflict = Bailundo Revolt of 1902
|partof = 
|campaign =
| image = Angola Ethnic map 1970.svg
| caption =  {{legend|#64809d|[[Ovimbundu]] area}} within modern day [[Angola]]
|date =15 May 1902 - 22 March 1904<ref name=OvI/>
|place = Benguela Highlands, central [[Portuguese Angola]]
|result = Portuguese victory
|combatant1 = [[Image:Flag Portugal (1830).svg|25px|border]] [[Portuguese Empire]]
|combatant2 =* [[Ovimbundu|Ovimbundu Kingdoms]]
**Bailundo
**Bié
**Huambo
**Civula
**Kasongi
**Civanda
**Ngalanga
<br/>Kisanji <br/>Luimbi<ref name=OvI/>
|commander1 =[[Image:Flag Portugal (1830).svg|25px|border]] [[Carlos I of Portugal|Carlos I]]<br /> [[Image:Flag Portugal (1830).svg|25px|border]] [[Cabral de Moncada]] <br />[[Image:Flag Portugal (1830).svg|25px|border]] Pais Brandão <br /> [[Image:Flag Portugal (1830).svg|25px|border]] Joaquim Teixeira Moutinho <br /> [[Image:Flag Portugal (1830).svg|25px|border]] Pedro Massano de Amorim<ref name=OvI/>
|commander2 =Kalandula {{POW}}<br /> [[Mutu ya Kevela]] {{KIA}}<br /> Samakaka <br /> Livongue {{KIA}} <br /> Civava {{KIA}}<ref name=OvI/>
|strength1 =750 troops<br />1,000 [[Auxiliaries]]<ref name=OvI>{{cite web|url=http://run.edu.ng/media/1834181203243.pdf|title=The Bailundo Revolt of 1902|work=Redeemer's University|author=Douglas Wheeler|date=|accessdate=9 May 2015}}</ref> 
|strength2 = 10,000<ref name=OvI/>
|casualties1 = Minimal<ref name=OvI/>
|casualties2 = +2,000 casualties<ref name=OvI/>
|casualties3 = +120 civilians<ref name=OvI/>
}}
{{Portuguese colonial campaigns}}
{{History of Angola}}

The '''Bailundo Revolt''' was an insurrection launched by the [[Ovimbundu]] kingdom of Bailundo and its allies against the [[Portuguese Empire]]. The revolt was prompted by the sudden decline of the price of root rubber, pitting the indigenous population against European immigrants and aboriginal loyalists. The revolt lasted between 1902 - 1904, ending in a Portuguese victory.

==Background==
The fertile Benguela highlands also known as ''Nano'' were traditionally settled by other [[Bantu people]]. The early 17th century invasion of the [[Imbangala]] tribesmen led to a merger of the two populations and the subsequent creation of the [[Ovimbundu]] kingdoms. The Benguela region was first explored by Portuguese merchants in middle of the 17th century, initiating the trade of slaves, ivory, beeswax and rubber. Occasional clashes between the Portuguese and Ovimbudu occurred in the 18th and 19th century, taking place in the two major regional trading arteries of [[Benguela]] - [[Caconda Nova]] and [[Catumbella]] - Northern Huambo. Private armies belonging to merchants and tribal chiefs fought each other over the control of the southern trading route. Following the defeat of the Bailundo and Bié in the 1774 war against the Portuguese, the two kingdoms remained at peace sharing control over the northern trading route.<ref name=OvI/> 

In the middle of the 19th century, Portuguese colonists began building forts in Ovimbundu controlled areas. Despite the fact, power largely remained in the hands of local kings and powerful merchants. Trading continued to flourish, as army deserters, Portuguese convicts, vagrants, mulattoes and locals became engaged in the long distance caravan trade.<ref name=OvI/> 

By the beginning of the 20th, the Benguela region was controlled by 22 [[Ovimbundu]] kingdoms, the Bailundo, Bié and Huambo being the most powerful of them. The 1874 invention of root rubber, quickly established it as a [[substitute good]] for traditional rubber. Bailundo citizens became increasingly involved in the peaking rubber trade between the period of 1886-1901, abandoning traditional raiding activities.<ref name=OvI/><ref name=WuH>{{cite web|url=http://www.ics.ul.pt/rdonweb-docs/ROQUE%20Razors%20edge_pdf.pdf|title=The Razor's Edge|work=Journal of African Historical Studies|author=Ricardo Roque|date=2003|accessdate=12 May 2015}}</ref>  

In 1890, a Portuguese punitive expedition overthrew the king of the Bié kingdom turning it into a [[puppet state]], while accelerating the settlement of Portuguese and Boer settlers and reinforcing the area's garrison.  The fall of Bié in combination with the shift in allegiance of a number of smaller puppet kingdoms separating Bailundo from areas under Portuguese control brought Bailundo into the Portuguese sphere of interest. However the Portuguese administrative control of the area remained weak, the colonial authorities were only represented by a small number of low level officials and soldiers. Portugal's lack of coherent political control in the region lead to the rise of [[Anti-imperialism|anti colonial]] attitudes. In 1902, the artificially high price of rubber suddenly plummeted due to the immoderate credit practices, causing an economic depression. Immediately before the rebellion, the Ovimbundu were hit by a smallpox epidemic and a famine.<ref name=OvI/><ref name=WuH/><ref>{{cite book|last=Stearns|first=Peter N.|author2=William Leonard Langer|year=2001|title=The Encyclopedia of World History: Ancient, Medieval, and Modern|pages=595}}</ref>

==Conflict==
On 7 April 1902 Kalandula was crowned king of the Bailundo. For the celebrations, Kalandula's leading councilor [[Mutu ya Kevela]] purchased several bottles of rum from a Portuguese merchant. The trader accused Kavela of not paying for the rum, prompting the commander of a nearby fort to summon Kavela. Mutu ya Kavela refused, claiming that he no longer recognised the authority of the commander. On 9 April 1902, representatives of the neighboring Ovimbudu kingdoms gathered in Bailundo preparing for war. On 15 May 1902, a delegation led by Bailundu king Kalandula attempted to hold a [[parley]] at the Bailundo fort, the Portuguese authorities detained and imprisoned the Bailundans provoking the war.<ref name=OvI/> 

Mutu ya Kavela organised further assemblies and traveled through the region, inciting others to join the revolt. The Ovimbudu kingdoms of Bié, Huambo, Civula, Kasongi, Civanda and Ngalanga answered Kavela's call to arms. The non Ovimbudu tribes of Kisanji and Luimbi also took part in the rebellion, possibly inspired by the shared [[Kandundu Cult]].<ref name=OvI/>

Between May - July 1902, as many as 6,000 Ovimbundu troops besieged the Bailundo fort several times, also launching raids on Portuguese trading posts and houses. A total of 20 Portuguese and 100 indigenous loyalists were killed as the rebels set up roadblocks on the roads leading to the Bailundo plateau. The rebels expected Portuguese resistance to collapse, while planning to destroy the Bailundo fort and Caconda settlement, later expelling all Europeans from the region. Indigenous merchants became the main target of the insurgents, who aimed to disrupt the trade of slaves and rum in particular. American missionaries present in the area were left unharmed, acting as mediators in a June prisoner exchange.<ref name=OvI/>

Amidst rising tensions, colonial authorities formed three columns composed of Portuguese, African and [[Boer]] soldiers, as well as auxiliaries. The colonial troops were able to fully exploit the favorable conditions created by the May - September dry season, quickly deploying their troops. The Northern column consisting of fewer than 100 soldiers was formed at the banks of the [[Cuanza River|Cuanza river]], arriving at Bailundo on July 17 and effectively breaking the siege a week later. The Cacenda column was assembled in Banguela, numbering 215 men it reached Bailundo on September 23. The third column set off from [[Luanda]], totaling 452 soldiers, over 1,000 porters and 4 mountain guns, entered the Bailundo fort on September 24. Pãis Brandao, Joacim Texeira Mutinho and Pedro Massano de Amorim commanded the columns respectively.<ref name=OvI/>

Utilizing modern repeating rifles and artillery pieces the reinforcements were able to counter rebel infantry charges, and destroy their fortified villages. On 4 August 1902, Portuguese troops killed [[Mutu ya Kavela]] and a small group of his companions in the area of [[Tchipindo]]. On 6 September 1902, insurgents unsuccessfully ambushed a band of Portuguese soldiers on the banks of river Congo, fleeing after coming under a [[salvo]]. By the end of September, Ovimbudu kings and their vassal began to surrender en masse, a month later the three columns were disbanded returning to their bases. A number of acts of reprisal took place, notably the execution of Cicende king Civava on 4 November 1902.<ref name=OvI/><ref>{{cite book|last=Walker|first=John Frederick|year=2004|title=A Certain Curve of Horn: The Hundred-Year Quest for the Giant Sable Antelope of Angola|pages=40–41}}</ref><ref>{{cite book|last=Rotberg|first=Robert I.|year=1965|title=A Political History of Tropical Africa|pages=302}}</ref>

Despite suffering at least 2,000 casualties, the Ovimbudu continued their resistance in remote mountainous areas until 1904. 1903 was marked by the pacification of the Selles area. Angola governor general [[Cabral de Moncada]] praised the bravery of Mutu ya Kavela describing him as being "great with courage", a number of Ovimbudu [[Last stand|last stands]] were also recorded. On 22 March 1904, a force of 230 soldiers was dispatched to the area of Bimbe, following reports of insurgent raids and sabotage. The expedition killed or captured the last cell of indigenous resistance, ending the revolt.<ref name=OvI/>

==See also==
*[[Slavery in Angola]]
*[[Battle of Mufilo]]
*[[Kingdom of Kongo]]
*[[Portuguese Colonial War]]

==References==
{{Reflist}}

[[Category:1903 in Angola]]
[[Category:1904 in Angola]]
[[Category:1902 in Angola]]
[[Category:1902 in Portugal]]
[[Category:Conflicts in 1904]]
[[Category:Conflicts in 1903]]
[[Category:Conflicts in 1902]]
[[Category:Wars involving Portugal]]
[[Category:Wars involving the states and peoples of Africa]]