{{refimprove|date=November 2012}}
{{Use dmy dates|date=September 2011}}
{{infobox book | 
| name          = Cranford
| author=[[Elizabeth Gaskell]]
| image         = Miss Matty and Peter.jpg
| image_size = 220px
| caption = Miss Matty and Peter
| country       = United Kingdom
| language      = English
| genre         = [[Novel]]
| publisher=Serial:[[Household Words]]
| pub_date      = 1851–3 (serial), 1853 (book)
| media_type    = Print ()
}}

'''''Cranford''''' is one of the better-known novels of the 19th-century English writer [[Elizabeth Gaskell]]. It was first published, irregularly, in eight instalments, between December 1851 and May 1853, in the magazine ''[[Household Words]]'', which was edited by [[Charles Dickens]]. It was then published, with minor revision, in book form in 1853.<ref>Peter Keating, "Introduction" to the Penguin edition of ''Cranford'' (1976). (London, 1986).</ref>

In the years following Elizabeth Gaskell's death the novel became immensely popular.<ref>Peter Keating, "Introduction", p.9.</ref>

{{wikisourcepar|Cranford}}

==Background==
The first instalment (in ''Household Words''), which became the novel's first two chapters, was originally published "as a self-contained sketch",<ref>Peter Keating, "Introduction", p.7.</ref> and the "irregular way" the further seven instalments were published suggests that it took Mrs Gaskell time to think of making this into a book.<ref>Peter Keating, "Introduction", p.8.</ref> She was during this period busy writing the three volume novel ''[[Ruth (novel)|Ruth]]'', which was published January 1853.<ref>Peter Keating, "Introduction", p.8.</ref>

''Cranford'' has been described as "practically structurelesss", and given the irregular nature of how it was first published, it is not surprising that it lacks unity.<ref>Peter Keating, "Introduction", p.10</ref> A. W. Ward describes the novel, as a "brief series of sketches, strung together with easy grace".<ref>Peter Keating, "Introduction", p.10.</ref>

The small country town of Cranford corresponds to Knutsford, Cheshire, where Elizabeth Gaskell had spent much of her childhood and where she returned after she married. However, the story's narrator comes from the nearby industrial city of Drumble, which corresponds to Manchester, where the author lived when writing the novel.<ref>Michell, Sheila (1985). "Introduction" to ''The Manchester Marriage''. UK: Alan Sutton. pp. iv–viii.</ref>

==Synopsis==

There is no real plot, but rather a collection of satirical sketches, which sympathetically portray changing small town customs and values in mid [[Victorian England]].<ref>{{cite book|last1=Denisoff|first1=Dennis|authorlink=Dennis Denisoff|title=The Broadview Anthology of Victorian Short Stories|date=2004|publisher=Broadview Press|location=Canada|page=123}}</ref> Harkening back to memories of her childhood in the small [[Cheshire]] town of [[Knutsford]], ''Cranford'' is Elizabeth Gaskell's affectionate portrait of people and customs that were already becoming anachronisms.<ref>{{cite web|last1=Wright|first1=Edgar|title=Elizabeth Cleghorn Gaskell|url=http://galenet.galegroup.com/servlet/DLBC_Online?vrsn=1.0&dd=0&srs=ALL&locID=loyolau&b1=KE&srchtp=b&c=2&ste=10&stp=DateDescend&dc=tiPG&d4=0.25&n=10&docNum=BK1497825014&b0=cranford&tiPG=0|website=Dictionary of Literary Biography}}</ref>

===Chapter I – Our Society===

The book is narrated by Mary Smith, a young woman who frequently visits the town and, when away, remains abreast of events through correspondence with the other characters. The first chapter introduces the leading women of Cranford, idiosyncratic yet endearing characters who hope to preserve their lifestyles (and all-important social customs) from change. Rowena Fowler, possessor of a red silk umbrella, conservatively considers an heir while her infirm body has outlived her kin. Miss Betty Barker is also determined to preserve the past, but in the form of her cow, which she sews pyjamas for, as it lost all of its hair after falling into a lime-pit. As for Miss Deborah Jenkyns, she establishes the norms and customs by which the town must abide.

However, when Captain Brown moves to town, he challenges the women's rules of politeness. First, he openly admits his own poverty. This is particularly awful to Miss Deborah Jenkyns, whom Brown also offends by finding [[Charles Dickens]] a better writer than Jenkyns' preferred "Dr. Johnson" ([[Samuel Johnson]]). Nevertheless, Brown’s warm manner subdues his detractors’ contention of his supposed social awkwardness; therefore, they allow him to bypass custom and visit before noon. Brown also has two daughters: Miss Brown, an ill-tempered woman with hardened features, and Miss Jessie, who has an innocent face and, like her father, is naive to Cranford’s rules. For instance, Miss Jessie boasts that her uncle can provide her with large amounts of Shetland wool. When aristocratic Miss Jamieson overhears, she takes exception to Miss Jessie putting on airs.

===Chapter II – The Captain===

As in any other small town, all is known by all, and the Browns' affairs shortly are no exception. While their economic distress is evident from day one, the townspeople soon discover something new: Captain Brown’s kindness. This is most notable in his treatment of his elder daughter, who has a debilitating illness (which contributes to her bad temper). Captain Brown and his younger daughter endure abject poverty in order to afford small luxuries to comfort the elder daughter. Captain Brown also shows his kindness by hand-crafting a wood-fire shovel for Miss Jenkyns.

After leaving Cranford for a time, our narrator, Mary Smith, returns to find nothing changed, not even Miss Jenkyns' animosity for Captain Brown. Captain Brown is still indigent as his daughter’s condition worsens. However, the town soon becomes hectic when Captain Brown dies. The women of the town then expect to have to support Brown’s daughters. With the final pages focusing on funeral arrangements, consoling the daughters, and Miss Jenkyns' attempts to take Miss Jessie in, the chapter concludes with Major Gordon, a man who served with Captain Brown, arriving in Cranford and trying to connect with Brown’s children.

===Chapter III – A Love Affair of Long Ago===

Miss Jenkyns' younger sister, Miss Matty, is overwhelmed by the upcoming visit of her cousin, Major Jenkyns. This anxiety is due to the fact that Miss Matty’s sister always ruled over her. Therefore, Miss Matty is ill-prepared to receive visitors, a job Miss Jenkyns would have overseen herself. In the meantime, Miss Matty takes on a new servant, Martha, whom Mary Smith trains. Fortunately, the visit goes without a hitch, aside from one small mistake: the servant nudges Major Jenkyns when he does not help himself fast enough at the dinner table.

The chapter concludes at Johnson’s store, when Miss Matty and Mary notice the arrival of Thomas Holbrook, who proposed to Miss Matty when they were young. Recalling the incident, Miss Matty says that her elder sister did not feel Thomas Holbrook would be a suitable husband, so the two separated in spite of mutual interest. Nevertheless, Holbrook invites the women to spend a day with him at his home.

===Chapter IV – A Visit to an Old Bachelor===

Arriving at Mr. Holbrook’s house, Miss Matty, Miss Pole, and Mary Smith are received well, and Miss Matty is flush with ideas of what may have been. Allowing Miss Pole and Miss Matty to get acclimated, Holbrook then shows Mary around the grounds, which is a fine home indeed. Although a hard-working man, Holbrook is content with a meagre social status, as his passion is books, not climbing social ladders.

After an enjoyable dinner, Holbrook offers Miss Matty the honor of filling his pipe. Then he reads poetry aloud, during which Miss Matty falls asleep. As the women are departing, Mr. Holbrook says he will call upon them. Miss Matty begins to hope that her girlhood dream may come true after all. Unfortunately, Mr. Holbrook dies later. As a result, Miss Matty resigns herself to allowing Martha to date, for she does not wish to prevent love in the same way Miss Jenkyns did to her.

===Chapter V – Old Letters===

In chapter five, the story shifts back in time to focus on Molly and John, the future parents of the Jenkyns sisters. John is very much in love and eager to be married, but Molly is not as enthusiastic. She expresses annoyance at John’s constant professions of love in their correspondence. However, she finally accepts his advances, and they are married when Molly is eighteen.

The chapter then includes letters from the Jenkyns sisters' grandfather, who writes at the news of his granddaughters being born. He believes they will be great beauties of Cranford. When a son is born later, the grandfather hopes that the boy will not fall into the proverbial “snares of the world.”

Finally, John, now Reverend Jenkyns, receives some acclaim in having one of his sermons published. Now Molly’s love finally matures, and is illustrated by her now addressing him as “my honored husband” in their letters.

===Chapter VI – Poor Peter===

Chapter six focuses on Miss Matty’s brother, Peter. Close with Matty but not with Miss Jenkyns, Peter has a life of prestige ahead of him, destined for an education of distinction at Cambridge. Preferring a life of mischief, Peter dresses up as his sister Miss Jenkyns and carried what would be construed as an illegitimate child, to upset his father, the reverend. The reverend then beats his son with his walking stick. After kissing his mother, Peter goes upstairs, and then leaves without telling anyone. His mother cries for his return.

A few days later a letter from Peter arrives. He has signed up to work on a ship. Unfortunately, Peter’s letter arrives late, and his parents’ attempts to persuade him otherwise go to no avail. Shortly thereafter, Peter’s mother dies from grief over her son, and Miss Jenkyns promises to take care of her father. After some time, Peter returns with some military accomplishment, and his father shows him off to Cranford. Peter then leaves for a war in India, and he is never heard from again.

===Chapter VII – Visiting===

A woman named Miss Betty Barker visits Miss Matty before the socially acceptable time. Thus, this visit once again overwhelms Matty, who accidentally puts on two caps to receive Miss Betty. Mary Smith is highly amused with the mistake. Miss Betty, a modest clerk’s daughter, once was employed as a maid. She saved enough to open a millinery shop with her sister. After working with a well-connected woman, Lady Arley, on clothing patterns, they focused on selling to aristocrats.

Miss Betty invites all the Cranford women to tea, even her former employer, Mrs. Jamieson. Class consciousness begins to inflate, and a Mrs. Fitz Adam receives no invitation due to her background. During the tea party, Miss Betty’s tea tray arrives with extravagant goodies, which is considered vulgar in Cranford. However, the rules begin to recede as the women play cards, order another tray, and consume a little too much brandy.

===Chapter VIII – Your Ladyship===

The widow of Mr. Jamieson’s eldest brother, Lady Glenmire, is arriving to visit the small town, and staying with Mrs. Jamieson. In turn, Cranford is buzzing to have such a prestigious woman in their midst. That is, all are excited but Mrs. Jamieson, who slightly insults Lady Glenmire. Nevertheless, while Miss Pole specifically wishes to be assured that Queen Victoria is well; however, confusion abounds as to how one should address Lady Glenmire, especially Miss Matty.

Yet in still, Mrs. Jamieson’s butler, Mr. Mulliner, sends out invitations for a small party. Most women wish to decline until they are persuaded by Miss Pole to accept. Uncomfortably quiet in the beginning, no one knows how to begin a conversation with someone of Lady Glenmire’s stature. This awkwardness is concluded when Miss Pole finally asks Lady Glenmire whether she has been to court, to which she states she had not. A mutual friendship is soon formed, while Mr. Mulliner takes too long to appease the hungry guests.

===Chapter IX – Signor Brunoni===

Mary Smith decides to return home to nurse her ailing father, leaving her interest in Cranford behind. Yet in November, while her father is returning to health, Mary receives an odd letter from Miss Matty. Asking about Turbans, styles and fashions, Miss Matty inevitably put the confusion to rest by saying a conjurer is coming: Signor Brunoni.

Mary decides to return, but mostly to see her friends, to which Miss Pole has a story just aching to be told. In the meantime, preparations are made for Signor Brunoni’s arrival. Miss Pole then meets a man, but this man mysteriously turns into Signor Brunoni, who is apparently demonstrating his magic. The next day Miss Matty is so excited to attend the show that she prepares early and rushes Mary to do so as well. The performance begins, and as the ladies are impressed, they aren’t quite sure what to think about the spectacle.

===Chapter X – The Panic===

Shortly after Signor Brunoni’s show, there is a spike in robberies. The ladies begin to believe Brunoni is responsible, even though he is no longer in town, and perhaps even a spy for the French. Miss Matty is also concerned, and she, Mary Smith, and Martha check the house every evening before bed. Soon after, Miss Pole also believes there is some trouble brewing when odd things happen, such as a beggar coming to the house and two men lurking around the premises as well. Similar things happen at Mrs. Jamiesons’ home, and Mr. Mulliner attempted to confront the evildoers, scaring off the assailants. In the fracas, Mrs. Jamieson’s dog, Carlo, died.

Miss Pole then sees the gang, 2 men and a woman, which came with their appearance including glowing auras. Later, Mrs. Forrester invites everyone to come to celebrate her wedding anniversary. Everyone attends in spite of difficult traveling conditions to support Mrs. Forrester’s night reminiscing as a widow. Showing their bravery the women confront their fears by discussing them. Miss Matty was fearful there may be a man hidden under her bed, while Miss Jenkyns thought it would be terrible to find a man staring at her from underneath a bed. Mrs. Forrester resigns to hiring boy from the cottages to keep an eye on her home. This comes with instructions that the boy should attack any strange noises with the Major’s sword.

===Chapter XI – Samuel Brown===

Miss Pole and Lady Glenmire go for a walk to visit a lady famous for knitting socks. Unfortunately, they become lost on the way, so they inquire for directions. Meeting two men and a woman, Miss Pole ponders as to whether these are the burglars. Mrs. Roberts, owner of the lodging place where Miss Pole and Lady Glenmire ask for directions, is insulted as if she stands accused. She goes to look for one of the apparent members of the gang, who is coincidentally Signor Brunoni, but now discovered to be an Englishman named Samuel Brown. Lady Glenmire offers to have a Dr. Hoggins look him over.

As Mary Smith is talking to Mrs. Brown, she learns of their time in India. Signor had been in the 31st regiment, but during that time she lost six children. Finally having a daughter named Phoebe, Mrs. Brown cannot bear to lose another child, and asks to go back to England. They save up, and when Phoebe is born they set off for the journey back to England. And as they near the end of their journey to leave India, Phoebe becomes ill. Meeting an Englishman named Aga Jenkyns, he nursed the baby back to health. Also, considering Peter vanished in India, curiosity abounds.

===Chapter XII – Engaged to be Married===

As Cranford, particularly Mrs. Jamieson and Lady Glenmire, took in Mr. and Mrs. Brown, the extra time spent between Lady Glenmire and Dr. Hoggins leads to Mrs. Fitz-Adam, Dr. Hoggins’ sister, confirming the two are to be married. This time was also lengthened when one of Mrs. Jamieson’s servants became ill, which require more interaction between the Dr. and Lady Glenmire.

This is quite the development for Cranford, the town struggles for an appropriate response, and this momentarily overshadows any discussion of Peter. However, the new spring fashion have arrived at the Johnson shop, and this may be the diversion Cranford needs.

===Chapter XIII – Stopped Payment===

The post-woman brings two letters to Miss Matty’s house, one for herself and one for Mary Smith. Mary’s letter was from her father, informing her of his continued good health and mentions the Town and County Bank, which Miss Matty has stock in. Miss Matty’s letter comes directly from the bank, informing her of an important meeting.

The ladies travel to Johnson’s to inspect the latest clothing arrivals. Miss Matty considers a new silk gown, a great departure from Miss Jenkyns's style. The shop man has also received a letter about the bank and informs the ladies that there are reports out that it is likely to break. This concerns the ladies. The following morning news comes that the bank has stopped payment, and Miss Matty is now financially ruined.

===Chapter XIV – Friends in Need===

Miss Matty attempts to deal with her financial ruin, and is supported by Martha, who refuses to leave. However, Miss Matty’s finances made this a necessity, as she considers taking a single room and working as a teacher, a profession that Mary consider to be ill-suited to her. Mary Smith asks her father for advice, and a magnificent meal is made by Martha to console Miss Matty. Martha suggests that she will marry Jem Hearn, and asks Miss Matty to lodge with them.

The next morning, Miss Pole sends a letter suggesting a meeting with Mary and others. The women of Cranford discuss Miss Matty’s plight, and resolve to help her. Mary gives Miss Matty the news when Mary’s father comes to discuss Miss Matty’s affairs. Miss Matty speaks of her own plan, which was that Martha and Jem were to be married without delay, and that they could live at Miss Matty’s house after a sale.

===Chapter XV – A Happy Return===

Miss Matty’s sale goes well, and she is able to retain some of her belongings. Mary leaves Miss Matty making a living by selling tea out of her parlor. Mr. and Mrs. Hoggins return to Cranford. Mrs. Hoggins casts aside her aristocratic roots (as Lady Glenmire), and is genuinely thrilled with Mr. Hoggins’ more humble status.

Not long after, Martha is about to have her first lying-in. She summons Mary back to Cranford to break the news to Miss Matty. Even though she is married, Martha is terrified of what the monastic Miss Matty will think when she finds out that she is pregnant. Indeed, Miss Matty is not aware that Martha is 9 months pregnant, but the shock subsides as soon as the baby comes.

One day soon after, a man walks into the shop. Mary quickly realizes that it is Peter Jenkyns himself, responding to her letter. Indeed, he sold his land in India and came back to be with his sister, who is overpowered with joy at his return. He is wealthy enough that they can live together comfortably, and she can give up the tea shop.

To increase his use to Cranford, Peter hires Signor Brunoni to perform at a brunch given by the long-absent Jessie Gordon (née Brown), and he inexplicably mends the Jamieson-Hoggins feud. This is all in the service of making Miss Matty happy.

===Chapter XVI – Peace to Cranford===

Peter wonders how Miss Matty never married Mr. Holbrook, distressing her somewhat. He becomes a favourite of the ladies of Cranford. Although the ladies worry that he is interested in Mrs. Jamieson, he turns out to be "pranking" her, telling her tall tales about his travels in India. His ultimate goal is to please Miss Matty, and the book ends with a panegyric on her goodness, and prognostics of a peaceful future for her.

==Characters==
* '''Mary Smith''' – The narrator. We are only given limited information about her. Mary is a young woman who once lived in Cranford but now lives with her business man father in the nearby city of Drumble. She frequently stays with Miss Matty.
* '''Miss Matty Jenkyns''' – An amiable, good-natured, though rather timid, elderly spinster.
* '''Miss Deborah Jenkyns''' – Miss Matty's imperious older sister, who dies early in the novel.
* '''Miss Pole''' – Allegedly the most "reasonable" and "enlightened" of the Cranford ladies.
* '''The Honourable Mrs Jamieson''' – A widow with aristocratic connections and the owner of Carlo, her beloved dog. On the social scale she is the most important person in Cranford.
* '''Mrs Forrester''' – Another widow.
* '''Betty Barker''' – A former milliner, who owns a cow whom she loves like a daughter.
* '''Peter Jenkyns''' – Matty Jenkyns' brother, who disappears early in the novel, after he was beaten by his father. Subsequently Mary Smith finds out that he may still be alive and writes to him. At the end of the novel Peter returns from India and is reunited with his sister.
* '''Thomas Holbrook''' – A lively and curious-minded farmer, who was Miss Matty's former admirer. He dies a year after a trip to Paris, France.   
* '''Captain Brown''' – An impoverished army captain, who comes to live at Cranford with his two daughters.
* '''Miss Brown''' – Captain Brown's elder daughter.
* '''Miss Jessie Brown''' – Captain Brown's younger daughter. After her father and sister's deaths, she marries and leaves Cranford.
* '''Major Gordon''' – A friend of Captain Brown who has been in love with Jessie Brown for years. 
* '''Lady Glenmire''' – Mrs. Jamieson's poor, widowed sister-in-law. She marries Dr. Hoggins to the initial dismay of the ladies of Cranford.
* '''Dr Hoggins''' – The Cranford surgeon. A rough but friendly and well-meaning man. Both he and his surname is regarded as vulgar by the ladies of Cranford.
* '''Martha''' – Miss Matty's maid, who is deeply attached to her mistress. Later, after Matty loses most of her money, Martha is her landlady and companion, with whom Matty lives on equal terms.
* '''Jem Hearn''' – The young man Martha marries.  
* '''Mr Mulliner''' – Mrs. Jamieson's butler. 
* '''Signor Brunoni''' – A travelling magician.
* '''Signora Brunoni''' – The wife of the travelling magician, who travelled on foot across India to save the life of her baby daughter.

==Adaptations==
The novel has been thrice adapted for television by the [[British Broadcasting Company|BBC]]. The first version was broadcast in 1951, the second in 1972, with Gabrielle Hamilton as Miss Matty, and [[Cranford (TV series)|the third version]] in 2007. The 2007 version added material from other writings by Gaskell: ''[[My Lady Ludlow]]'', ''[[Mr. Harrison's Confessions]]'' and ''[[The Last Generation in England]]''. [[Judi Dench]] and [[Eileen Atkins]] took the leading roles as Miss Matty and Miss Deborah Jenkyns, with [[Imelda Staunton]] cast as the town's gossip, Miss Pole, and [[Michael Gambon]] as Miss Matty's former admirer, Mr. Holbrook. The BBC sequel, ''[[Return to Cranford]]'', was broadcast in 2009 in the UK and 2010 in the USA.

== References ==
<references />

==External links==
{{Portal|Cheshire}}
* {{IMDb title|id=1145518|title=Cranford (1951) (TV)}}
* {{IMDb title|id=0437943|title=Cranford (1972) (TV)}}
* {{IMDb title|id=0974077|title=Cranford (2007)}}
* The [http://www.amazon.com/dp/0980921023 ''2008 release of Cranford''] by [http://www.engagethebook.com ''AD Classic Books''] includes artwork by George Du Maurier, commissioned by Smith, Elder & Co. in 1864, and artwork by Hugh Thomson for Macmillan and Co. in 1891.
* {{cite news|first=Jenny|last=Uglow|work=The Guardian |location=UK|title= Band of women|url=http://books.guardian.co.uk/review/story/0,,2204287,00.html|date=3 November 2007|accessdate=2007-11-06 }}
{{Gutenberg|no=394|name=Cranford}}
* {{librivox book | title=Cranford | author=Elizabeth Cleghorn GASKELL}}

{{Elizabeth Gaskell}}

{{DEFAULTSORT:Cranford}}
[[Category:1853 novels]]
[[Category:Novels by Elizabeth Gaskell]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in Household Words]]
[[Category:Victorian novels]]
[[Category:Novels set in Cheshire]]
[[Category:Novels adapted into television programs]]