{{TRseries}}
[[Image:Tr-bigstick-cartoon.JPG|thumb|[[William Allen Rogers]]'s 1904 cartoon recreates an episode in ''[[Gulliver's Travels]]'']]
[[Image:SpeakSoftly.jpg|thumb|The letter in which [[Theodore Roosevelt|Roosevelt]] first used his now famous phrase]]

'''Big stick ideology''', '''Big stick diplomacy''', or '''Big stick policy''' refers to [[President of the United States|U.S. President]] [[Theodore Roosevelt]]’s foreign policy: "speak softly, and carry a big stick." Roosevelt described his style of foreign policy as "the exercise of intelligent forethought and of decisive action sufficiently far in advance of any likely crisis".<ref name="Roosevelt">{{Harvnb|Roosevelt|1913|p=522}}</ref>

The idea of negotiating peacefully, simultaneously threatening with the "big stick", or the military, ties in heavily with the idea of ''[[Realpolitik]]'', which implies a pursuit of political power that resembles [[Machiavellianism|Machiavellian]] ideals.<ref name="High Beam">{{cite web| last = | first = | authorlink = | coauthors = | title = Big Stick and Dollar Diplomacy| work = | publisher = High Beam Encyclopedia| year = 2001| url = http://www.encyclopedia.com/doc/1G2-3468300113.html| doi = | accessdate = 2008-07-16}}</ref> It is comparable to [[gunboat diplomacy]], as used in international politics by [[Imperialism|imperial]] powers.

== Background ==
The first known use of the phrase occurs in a private letter from Roosevelt (then [[Governor of New York]]) to Henry L. Sprague, dated January 26, 1900.<ref>{{cite web |url=http://www.loc.gov/exhibits/treasures/trm139.html |title=Speak Softly. . . |accessdate=2016-03-24}}
</ref> Roosevelt wrote, in a bout of happiness after forcing [[New York (state)|New York]]'s [[Republican Party (United States)|Republican]] committee to pull support away from a corrupt financial adviser:{{quote|I have always been fond of the West African proverb: "Speak softly and carry a big stick; you will go far."}}

In an article about an interview with Governor Roosevelt, published in the ''Brooklyn Daily Eagle'' on April 1, 1900, a reporter noted that "His motto, he says, he has taken from the South African people: 'Speak softly— carry a big stick— and you will go far.{{Single double}}<ref>[https://www.newspapers.com/image/50386416/?terms=%22speak%2Bsoftly%22%2B%22big%2Bstick%22 "Gambling and Vice in the State Capital"], ''The Brooklyn Daily Eagle'', April 1, 1900, p39</ref>

Roosevelt would go on to be [[United States presidential election, 1900|elected Vice President]] later that year, and subsequently used the aphorism publicly in an address to the [[Minnesota State Fair]], entitled "National Duties", on September 2, 1901:<ref>{{cite book |url=http://hdl.handle.net/2027/uva.x000144517?urlappend=%3Bseq=306 |page=288 |title=The Strenuous Life: Essays and Addresses |first=Theodore |last=Roosevelt |location=New York |publisher=Century |year=1902}}</ref><ref>http://www.theodore-roosevelt.com/images/research/txtspeeches/678.pdf</ref>

{{quote|A good many of you are probably acquainted with the old proverb: "Speak softly and carry a big stick -- you will go far."}}

Four days later, President [[William McKinley]] was [[Assassination of William McKinley|shot by an assassin]]; his death a further eight days later elevated Roosevelt to the presidency.

Roosevelt's attribution of the phrase to "a West African proverb" was seen at the time as evidence of Roosevelt’s "prolific" reading habits,<ref name="LoC Exhibit">{{cite web| last = | first = | authorlink = The [[Library of Congress]]| coauthors = | title = Speak Softly. . .| work = Library of Congress Exhibit| publisher = [[Library of Congress]]| date = 2007-10-31| url = http://www.loc.gov/exhibits/treasures/trm139.html| doi = | accessdate = 2008-07-14| archiveurl= https://web.archive.org/web/20080710190036/http://www.loc.gov/exhibits/treasures/trm139.html| archivedate= 10 July 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name="Davis">{{Harvnb|Davis|1990|p=229}}</ref> but the claim that it originated in West Africa has been disputed.<ref name="PhraseFinder">{{cite web| last = Martin| first = Gary| authorlink = | coauthors = | title = Speak Softly And Carry a Big Stick| work = | publisher = | year = | url = http://www.phrases.org.uk/meanings/speak-softly-and-carry-a-big-stick.html| doi = | accessdate = }}</ref> No earlier citation for the phrase has been found, and there is no record of the phrase being used in West Africa before Roosevelt's time. It has been therefore suggested that he might have coined the phrase himself.<ref name="PhraseFinder" />

== Usage ==
Although used before his presidency, Roosevelt used military muscle several times throughout his two terms with a more subtle touch to complement his diplomatic policies and enforcing the [[Monroe Doctrine]] throughout multiple interventions in [[Latin America]]. This included the [[Great White Fleet]], 16 battleships which peacefully circumnavigated the globe as an illustration of United States' rising yet neutral prestige under Roosevelt's direction.

=== In the U.S. ===

====Anthracite coal strike====
In 1902, 140,000 miners went [[Coal strike of 1902|on strike]], wanting higher pay, [[Eight-hour day|shorter work hours]], and better housing.<ref name="Davis" /> They were led by [[John Mitchell (United Mine Workers)|John Mitchell]], a fellow miner who formed the [[United Mine Workers]] (UMW). The mining companies refused to meet the demands of the UMW and contacted the Federal Government for support.<ref name="Davis" /> Before Roosevelt, the government would send in [[Armed forces|military support]] to forcefully end the strike, but during Roosevelt's terms, this strategy was not used. After the companies called for assistance, Roosevelt, fearful of the effects a coal shortage would have on the [[Economic history of the United States#Progressive Era: 1890–1920|economy]] of the time, decided to host a meeting in the [[White House]] involving representatives or delegates, of the miners and the leaders of the mining companies.<ref name="Davis" /><ref name="StFrancis.edu">{{cite web| last = Marks| first = Ratchell| authorlink = | coauthors = | title = Anthracite Coal Strike of 1902| work = | publisher = | date = 2005-10-31| url = http://www.stfrancis.edu/content/ba/ghkickul/stuwebs/btopics/works/anthracitestrike.htm| doi = | accessdate = 2008-07-14| archiveurl= https://web.archive.org/web/20080621111119/http://www.stfrancis.edu/ba/ghkickul/stuwebs/btopics/works/anthracitestrike.htm| archivedate= 21 June 2008 <!--DASHBot-->| deadurl= no}}</ref> Mitchell, after returning from the White House meeting, met with the miners, and drew a Consensus. The miners decided not to submit to political pressure, and continue on with the strike. Roosevelt then decided to bring in the military, but instead of forcefully ending the strike and restoring power to the mining companies, he would use the military to ''run'' the mines in the "public interest".<ref name="Davis" /> The mining companies, upset that they were no longer directly making a [[Profit (accounting)|profit]], then accepted the demands of the UMW. This policy was later referred to as the "Square Deal".<ref name="JSTOR1">{{cite journal| last = Williams| first = Talcott| authorlink = | coauthors = | title = The Anthracite Strike of 1902: A Record of Confusion| journal = The American Monthly Review of Reviews| volume = 48| issue = | pages = 229–251| publisher = | year = 1905| jstor = 1902513| doi =  }}</ref>

=== Latin America ===

==== Venezuelan Affair (1902) and the Roosevelt Corollary ====
[[File:BigStickinLAmerica.jpg|thumb|right|A map of [[Middle America (Americas)|Middle America]], showing the places affected by the proverbial "big stick"<ref name="Bailey">{{Harvnb|Bailey|1980|p=500}}</ref>]]
In the early 20th century, [[Venezuela]] was receiving messages from [[United Kingdom|Britain]] and Germany about "Acts of violence against the liberty of British subjects and the massive capture of British vessels" who were from the UK and the acts of Venezuelan initiative to pay off long-standing debts.<ref name="Hershey">{{Harvnb|Hershey|1903|p=251}}</ref><ref name="Barck">{{Harvnb|Barck, Jr.|1974|p=99}}</ref> After British and German forces [[Venezuelan crisis of 1902–03|took naval action with a blockade on Venezuela]] (1902–1903), Roosevelt denounced the blockade. The blockade began the basis of the [[Roosevelt Corollary]] to the [[Monroe doctrine]].<ref name="MSN Foreign">{{cite encyclopedia|title=Theodore Roosevelt: Foreign Policy |work=Encarta |publisher=MSN |year=2008 |url=http://encarta.msn.com/encyclopedia_761558578_5/Theodore_Roosevelt.html |accessdate=2008-07-24 |archiveurl=http://www.webcitation.org/5kwQ7FiMt?url=http%3A%2F%2Fencarta.msn.com%2Fencyclopedia_761558578_5%2FTheodore_Roosevelt.html |archivedate=2009-10-31 |deadurl=yes |df= }}</ref><ref name="LaFeber">{{Harvnb|LaFeber|1993|p=198}}</ref> Though he had mentioned the basis of his idea beforehand in private letters, he officially announced the corollary in 1904, stating that he only wanted the "other republics on this continent" to be "happy and prosperous". For that goal to be met, the corollary required that they "maintain order within their borders and behave with a just obligation toward outsiders".<ref name="LaFeber" />

Most historians, such as one of Roosevelt's many biographers [[Howard K. Beale]] have summarized that the corollary was influenced by Roosevelt's personal beliefs as well as his connections to foreign bondholders.<ref name="LaFeber" /><ref name="Fagan">{{cite web| last=Fagan | first=Patrick| title=On Historians' Changing Perceptions of Theodore Roosevelt Pre-1950s and Post-1940s| publisher=WorkingPapers.org| date=2005-05-18| url=http://workingpapers.org/writings/roosevelt.htm| accessdate=2008-08-27}}</ref><ref name="Gould">{{Harvnb|Gould|1991|p=380}}</ref> The U.S. public was very "tense" during the two-month blockade, and Roosevelt requested that Britain and Germany pull out their forces from the area. During the requests for the blockade’s end, Roosevelt stationed naval forces in [[Cuba]], to ensure "the respect of Monroe doctrine" and the compliance of the parties in question.<ref name="Barck" /> The doctrine was never ratified by the senate or brought up for a vote to the American public. Roosevelt's declaration was the first of many presidential decrees in the twentieth century that were never ratified.<ref>{{cite book|last1=Burns|first1=James MacGregor|last2=Dunn|first2=Susan|title=The Three Roosevelts|date=2001|publisher=Atlantic Monthly Press|isbn=0871137801|pages=76–77|edition=1st}}</ref>

==== Canal diplomacy ====
The U.S. used the "big stick" during "Canal Diplomacy", the questionable diplomatic actions of the U.S. during the pursuit of a canal across [[Central America]]. Both [[Nicaragua]] and [[Panama]] featured canal related incidents of Big Stick Diplomacy.<ref name="Conniff">{{Harvnb|Conniff|2001|p=63}}</ref>

===== Proposed construction of the Nicaragua Canal =====
{{Main|Nicaragua Canal}}

In 1901, [[United States Secretary of State|Secretary of State]] [[John Hay]] pressed the Nicaraguan Government for approval of a canal. Nicaragua would receive $1.5 million in ratification, $100,000 annually, and the U.S. would "provide sovereignty, independence, and territorial integrity".<ref name="Berman">{{Harvnb|Berman|1986|p=149}}</ref> Nicaragua then returned the contract draft with a change; they wished to receive, instead of an annual $100,000, $6 million in ratification. The U.S. accepted the deal, but after [[United States Congress|Congress]] approved the contract a problem of court jurisdiction came up. The U.S. did not have legal jurisdiction in the land of the future canal. An important note is that this problem was on the verge of correction, until Pro-Panama representatives posed problems for Nicaragua; the current leader ([[José Santos Zelaya|General José Santos Zelaya]]) did not cause problems, from the outlook of U.S. interests.<ref name="Berman" />

===== Construction of the Panama Canal =====
{{Main|History of the Panama Canal}}
'''
In 1899, the [[Isthmian Canal Commission of 1899|Isthmian Canal Commission]] was set up to determine which site would be best for the canal (Nicaragua or Panama) and then to oversee construction of the canal.<ref name="WebDocPAN-HISTORY">{{cite web| last = | first = | authorlink = | coauthors = | title = The Panama Canal: History| work = |website=eclipse.co.uk | date = 1999-12-30| url = http://www.eclipse.co.uk/~sl5763/panama.htm| doi = | accessdate = 2008-08-20}}</ref> After Nicaragua was ruled out, Panama was the obvious choice. A few problems had arisen, however. With the U.S.' solidified interests in Panama (then a small portion of [[Colombia]]), both Colombia and the French company that was to provide the construction materials raised their prices. The U.S., refusing to pay the higher-than-expected fees, "engineered a revolution" in Colombia.<ref name="Zinn1">{{Harvnb|Zinn|1999|p=408}}</ref><ref name="Davis2">{{Harvnb|Davis|1990|pp=224–227}}</ref><ref name="UncleSam1">{{Harvnb|Bishop|1913|p=23}}</ref> On November 3, 1903, Panama (with the support of the [[United States Navy]]) revolted against Colombia. Panama became a new republic, receiving $10 million from the U.S. alone. Panama also gained an annual payment of $250,000, and guarantees of independence.<ref name="Davis2" /> The U.S. gained the rights to the [[Panama Canal Zone|canal strip]] "in perpetuity". Roosevelt later said that he "took the Canal, and let Congress debate".<ref name="Davis2" /> After Colombia lost Panama, they tried to appeal to the U.S. by the [[Separation of Panama from Colombia#Reactions|reconsidering of treaties]] and even naming [[Panama City]] the capital of Colombia.<ref name="Reaction1">{{cite web| last = Vargas| first = Diego Uribe| authorlink = | coauthors = | title = CAPITULO XIV: Memorial de Agravios | work = | publisher = Biblioteca Luis Ángel Arango| date = 2007-01-12| url = http://www.banrepcultural.org/blaavirtual/historia/canal/can13.htm | doi = | accessdate = 2008-08-21|language=es}}</ref>

==== Cuba ====
{{Main|Teller Amendment|Platt Amendment}}
The U.S., after the [[Spanish–American War]], had many [[expansionism|expansionists]] who wanted to [[annexation|annex]] [[Cuba]]. Many people felt that a foreign power (outside of the U.S.) would control a portion of Cuba, thus the U.S. could not continue with its interests in Cuba.<ref name="Bailey" /> Although many advocated annexation, this was prevented by the [[Teller Amendment]], which states "hereby disclaims any disposition of intention to exercise sovereignty, jurisdiction, or control over said island except for pacification thereof, and asserts its determination, when that is accomplished, to leave the government and control of the island to its people." When summarized, this could mean that the U.S. would not interfere with Cuba and its peoples. The expansionists argued though, that the Teller Amendment was created "ignorant of actual conditions" and that this released the U.S. from its obligation.<ref name="Bailey" /> Following the debate surrounding the Teller Amendment, the [[Platt Amendment]] took effect. The Platt Amendment (the name is a misnomer; the Platt Amendment is actually a [[Rider (legislation)|rider]] to the Army Appropriation Act of 1901) was accepted by Cuba in late 1901, after "strong pressure" from [[Washington, D.C.|Washington]].<ref name="Bailey" /> The Platt Amendment, summarized by [[Thomas A. Bailey]] in "Diplomatic History of the American People":

# Cuba was not to make decisions impairing her independence or to permit a foreign power [e.g., Germany] to secure lodgment in control over the island.
# Cuba pledged herself not to incur an indebtedness beyond her means [It might result in foreign intervention].
# The United States was at liberty to intervene for the purpose of preserving order and maintaining Cuban independence.
# Cuba would agree to an American-sponsored sanitation program [Aimed largely at yellow fever].
# Cuba would agree to sell or lease to the United States sites for naval or coaling stations [Guantánamo became the principal base].<ref name="Bailey" />

With Platt Amendment in place, Roosevelt pulled the troops out of Cuba. This action was met with public unrest and outcries for annexation, with reasons ranging from "U.S. interests" to "dominant white race". The ''Indianapolis News'' said, "It is [[manifest destiny]] for a nation to own the islands which border its shores." A year later, Roosevelt wrote,

{{quote|Just at the moment I am so angry with that infernal little Cuban republic that I would like to wipe its people off the face of the earth. All that we wanted from them was that they would behave themselves and be prosperous and happy so that we would not have to interfere.|Theodore Roosevelt|Roosevelt to White<ref>Roosevelt to White, Sept. 13, 1906, Roosevelt Papers, Library of Congress</ref>}}

====Japan====
At the conclusion of the [[Russo-Japanese War]] in September 1905, President Roosevelt leveraged his position as a strong but impartial leader in order to negotiate a peace treaty between the two nations.  "''Speaking softly''" earned the President enough prestige to even merit a [[Nobel Peace Prize]] the following year for his efforts.

== See also ==
*[[Peace through strength]]
*{{USS|Theodore Roosevelt|CVN-71}} (nicknamed the Big Stick)

== Notes ==
{{Reflist|2}}

== References ==
{{Refbegin}}
*{{Citation |last=Bailey|first=Thomas A. |title= A Diplomatic History of the American People 10th ed.|year= 1980 |publisher= Prentice Hall |isbn= 0-13-214726-2}}
*{{Citation |last= Barck, Jr. |first= Oscar Theodore|title= Since 1900 |year= 1974 |publisher= MacMilliam Publishing Co., Inc. |isbn= 0-02-305930-3}}
*{{Citation |last= Beale|first= Howard K. |title =Theodore Roosevelt and the Rise of America to World Power |year= 1957 |publisher= [[Johns Hopkins Press]] |isbn= }}
*{{Citation |last= Berman |first= Karl |title= Under the Big Stick: Nicaragua and the United States Since 1848 |publisher= South End Press |year= 1986 |isbn=}}
*{{Citation |last= Bishop |first= Joseph Bucklin |title= Uncle Sam's Panama Canal and World History, Accompanying the Panama Canal Flat-globe: Its Achievement an Honor to the United States and a Blessing to the World |publisher= Pub. by J. Wanamaker expressly for the World Syndicate Company | year= 1913 |isbn=}}
*{{Citation |last=Conniff|first= Michael L. |title= Panama and the United States: The Forced Alliance|year= 2001 |publisher= [[University of Georgia Press]] |isbn= 0-8203-2348-9}}
*{{Citation |last= Davis|first= Kenneth C. |title =Don't Know Much About History |year= 1990 |publisher= Avon Books |isbn= 0-380-71252-0}}
*{{Citation |last= Gould |first= Lewis L. |title= The Presidency of Theodore Roosevelt |publisher= [[University Press of Kansas]] |year= 1991 |isbn= 978-0-7006-0565-1}}
*{{Citation |last= Hershey|first= A.S. |title= The Venezuelan Affair in the Light of International Law|year= 1903 |publisher= [[University of Michigan Press]] |isbn= }}
*{{Citation |last= LaFeber|first= Walter |title= A Cambridge History of American Foreign Relations: The American Search for Opportunity. 1865 - 1913 |year= 1993 |publisher= [[Cambridge University Press]] |isbn= 0-521-38185-1}}
*{{Citation |last=Perkins|first=Dexter |title= The Monroe Doctrine, 1867-1907 |year= 1937 |publisher= Baltimore Press |isbn= }}
*{{Citation |last= Roosevelt |first= Theodore |title= Theodore Roosevelt: An Autobiography |publisher= The Macmillan Press Company |year= 1913 |isbn=}}
*{{Citation | last = Zinn | first = Howard| title = A People's History of the United States| publisher = Harper Perennial | year = 1999 |isbn = 0-06-083865-5}}
{{Refend}}

== External links ==
* [http://www.mtholyoke.edu/~jlgarner/classweb/worldpolitics/bigstick.html A site about Big Stick Ideology]
* [http://www.theodoreroosevelt.org/life/RooseveltCorollary.htm Information about the political aspects of the Big Stick]

{{United States intervention in Latin America}}
{{Theodore Roosevelt}}
{{US history}}

{{Good article}}

{{DEFAULTSORT:Big Stick Ideology}}
[[Category:Hegemony]]
[[Category:American political catch phrases]]
[[Category:Banana Wars]]
[[Category:Presidency of Theodore Roosevelt]]
[[Category:Military diplomacy]]