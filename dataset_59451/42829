<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = T.V
 |image = Fokker T.V.jpg
 |caption = Fokker T.V
}}{{Infobox Aircraft Type
 |type = Bomber
 |manufacturer = [[Fokker]]
 |designer =
 |first flight =16 October [[1937 in aviation|1937]]
 |introduced =[[1938 in aviation|1938]]
 |retired =[[1940 in aviation|1940]]
 |produced =
 |number built =16
 |status =
 |unit cost =
 |primary user = [[Netherlands Air Force]]
 |more users = 
 |developed from =
 |variants with their own articles = 
}}
|}

The '''Fokker T.V''' was a twin-engine bomber, described as an "aerial [[cruiser]]",<ref>Hooftman 1979, p. 16.</ref> built by Fokker for the [[Netherlands Air Force]].

It was modern for its time, but by the [[Battle of the Netherlands|German invasion]] of 1940, it was outclassed by the airplanes of the ''[[Luftwaffe]]''. Nevertheless, the T.V was used with some success against the German onslaught.

==Development and design==
In the early 1930s, the ''[[Luchtvaartafdeling]]'' (i.e. the Netherlands Army Air Force) became interested in the ''luchtkruiser'' (aerial cruiser) concept multipurpose aircraft, which was to have a primary role of intercepting and destroying enemy bomber formations, with a secondary role as a long-range bomber, with Colonel P.W. Best, commander of the ''Luchtvaartafdeling'' stating on 28 March 1935 that aircraft of the ''luchtkruiser'' should be purchased in as large numbers as possible, proposing to cancel procurement of the [[Fokker D.XXI]] fighter to release funds.<ref name="Frust p241-2">Van der Klaauw 1986, pp. 241—242.</ref>

To meet this requirement, Fokker developed the T.V, a five-seat, twin-engined [[monoplane]]. It featured a wooden wing, while the slab-sided [[fuselage]] was of mixed construction, with a wooden [[monocoque]] centre fuselage, a fabric covered steel tube rear fuselage and a [[duralumin]] forward fuselage. While this construction method was typical for Fokker aircraft, it was obsolete compared with contemporary aircraft of its size, which were normally of all-metal construction.<ref name="Frust p243">Van der Klaauw 1986, p.243.</ref>  It was fitted with a 20&nbsp;mm [[autocannon]] in the nose to meet the bomber destroyer part of the requirement, and four defensive Browning machine guns, one each in dorsal, ventral and tail positions, with one capable of being switched between two waist positions. It had a bomb-bay under the centre fuselage capable of carrying up to 1,000&nbsp;kg (2,200&nbsp;lb) of bombs.<ref name="Frust p244">Van der Klaauw 1986, p.244.</ref>

A contract was signed for 16 T.Vs on 7 December 1936,<ref name="Frust p243"/> with the first aircraft (not a prototype as such) flying on 16 October 1937 from [[Amsterdam Airport Schiphol|Schiphol]] airfield.<ref name="Frust p244"/>

==Operational history==
The first 11 T.Vs, by now considered medium bombers, were delivered in 1938, with the last 4 following in 1939.<ref name="Frust p245">Van der Klaauw 1986, p.245.</ref> Although it had good handling characteristics, its suffered from reliability problems with its engines and propellers, and by the summer of 1939, the Netherlands was planning to purchase 24 [[Dornier Do 215]]s to replace them.<ref name="Frust p245-6">Van der Klaauw 1986, pp. 245—246.</ref>

On 10 May 1940, [[Nazi Germany|Germany]] [[Battle of the Netherlands|invaded the Netherlands]], [[Belgium]] and [[Luxembourg]]. The T.V saw its first combat, when taking off from [[Amsterdam Airport Schiphol|Schiphol]] to avoid air attack, eight T.Vs encountered a formation of German bombers, shooting down two.<ref name="Frust p246-7">Van der Klaauw 1986, pp. 246—247.</ref> After this, the T.V reverted to its primary bomber role, being used in attacks against German airborne troops landing at [[The Hague]] and [[Rotterdam]]. By the end of the first day of fighting only two T.Vs were servicible, being sent against bridges over the [[Meuse (river)|River Maas]] at Rotterdam on 11 May, where a further aircraft was shot down, with the final T.V being shot down during attacks on bridges at [[Moerdijk]] on 13 May.<ref name="Frust p248-9">Van der Klaauw 1986, pp. 248—249.</ref>

As the T.V lacked [[self-sealing fuel tanks]], they gained a reputation for rapidly catching fire when hit by enemy fire.<ref>Hooftman 1979, p. 18.</ref>

==Operators==
;{{flag|Netherlands}}
*[[Royal Netherlands Air Force]]

==Specifications==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->  
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. -->
|ref=Frustrated Fokker <ref name="Frust p247">Van de Klaauw 1986, p.247.</ref> 
|crew= Five (Pilot, co-pilot, bombardier, radioman/gunner, gunner)
|capacity=
|length main= 16.00 m
|length alt= 60 ft 6 in
|span main= 21.00 m
|span alt= 68 ft 10¾ in
|height main= 4.20 m <ref>tail down</ref>
|height alt= 13 ft 9 in
|area main= 66.2 m²
|area alt= 712.6 ft²
|airfoil=
|empty weight main= 4,650 kg
|empty weight alt= 10,251 lb
|loaded weight main= 7,250 kg  
|loaded weight alt= 15,983 lb  
|useful load main= <!--kg-->  
|useful load alt= <!--lb-->  
|max takeoff weight main= 7,650 kg
|max takeoff weight alt= 16,865 lb
|more general=
|engine (prop)=[[Bristol Pegasus]] XXVI
|type of prop= [[air-cooled]] [[radial engine]]s
|number of props=2
|power main= 690 kW 
|power alt= 925 hp
|power original= at 9,500 ft
|max speed main= 417 km/h
|max speed alt= 225 knots, 259 mph
|cruise speed main= 335 km/h
|cruise speed alt= 181 kn, 208 mph
|never exceed speed main= <!--km/h-->  
|never exceed speed alt= <!--knots,mph--> 
|stall speed main= <!--km/h-->  
|stall speed alt= <!--knots,mph--> 
|range main= 1,550 km 
|range alt= 837 nmi, 963 mi
|ceiling main= 8,550 m 
|ceiling alt= 28,050 ft
|climb rate main= <!--m/s-->  
|climb rate alt= <!--ft/min-->  
|loading main= <!--kg/m²-->  
|loading alt= <!--lb/ft²-->  
|thrust/weight=<!--a unitless ratio-->  
|power/mass main= <!--W/kg-->  
|power/mass alt= <!--hp/lb-->  
|more performance=*'''Climb to 5,000 m (16,400 ft):''' 13.1 min
|guns=<br />
** 1 × [[20 mm caliber|20 mm]] [[Solothurn S-18/100]] cannon in nose cupola
** 5 × 7.9 mm (.31 in) [[machine-gun]]s on dorsal, ventral, and lateral positions, and tail cupola
|bombs= 1,000 kg (2,205 lb) of bombs
|avionics=
}}

==See also==
{{aircontent
|related=
*[[Fokker T.IX]]
|similar aircraft=
*[[Bristol Blenheim]]
*[[Bell YFM-1 Airacuda]]
*[[Kawasaki Ki-48]] ''Sokei''
*[[Martin Maryland]]
*[[PZL.37 Łoś]]
|sequence=
|lists=
* [[List of aircraft of World War II]]
* [[List of bomber aircraft]]
|see also=
}}

==References==

===Notes===
{{reflist|2}}

===Bibliography===
{{refbegin}}
* Gerdessen, Frits and Luuk Boerman. ''Fokker T.V 'Luchtkruiser':  History, Camouflage and Markings'' (Bilingual English-Dutch). Zwammerdam, the Netherlands: Dutch Profile Publications, 2009. ISBN 978-94-90092-01-6.
* Hooftman, Hugo. ''Van Brik tot Freedom Fighter: 1. Met Stofbril en Leren Vliegkap'' (In Dutch). Zwolle, the Netherlands: La Rivière & Voorhoeve N.V., 1963.
* Hooftman, Hugo. ''Fokker T-V en T-IX (Nederlandse Vliegtuigencyclopedie 8)'' (In Dutch). Bennekom, the Netherlands: Cockpit UItgeverij, 1979.
* Van der Klaauw, Bart. "Frustrated Fokker". ''[[Air International]]'', November 1986, Vol 31 No 6, Bromley, UK:Fine Scroll. ISSN 0306-5634. pp.&nbsp;241–249.
* Van der Klaauw, Bart. ''Bommenwerpers Wereldoorlog II, deel 2'' (In Dutch). Alkmaar, the Netherlands: Uitgeverij de Alk bv.
{{refend}}

==External links==
{{commons category|Fokker T.V}}
* [http://www.waroverholland.nl/woh/index.php?page=photo&pid=3958 War over Holland]

{{Fokker aircraft}}

[[Category:Dutch bomber aircraft 1930–1939]]
[[Category:Fokker aircraft|T 05]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]