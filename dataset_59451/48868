{{Infobox person
| name                      = Jeffrey A. Hutchings
| image                     = Jeffrey_Hutchings.jpg
| birth_date                = {{Birth date and age|1958|09|11|}}
| birth_place               = [[Orillia]], [[Ontario]]
| nationality               = [[Canada|Canadian]]
| occupation                = Fisheries Scientist
| organization              = [[Dalhousie University]]
}}
'''Jeffrey A. Hutchings''' (born 11 September 1958) is a Canadian-born [[fisheries scientist]], a Faculty of Science Killam Professor of Biology, and former Canada Research Chair in Marine Conservation and Biodiversity (2002-2011) at [[Dalhousie University]]. He is well known for his work on the [[evolution of fish]] life histories and on the collapse, recovery, and sustainable harvesting of marine fishes. In addition to being Chair of a 2012 Royal Society of Canada Expert Panel on Marine Biodiversity (and member of a 2001 Expert Panel on genetically modified foods), he chaired Canada's national science body (Committee on the Status of Endangered Wildlife in Canada) responsible, by law, for advising the Canadian federal Minister of the Environment on species at risk of extinction. He is Past-President of the Canadian Society for Ecology and Evolution (2012-2013). He was elected Fellow of the Royal Society of Canada (Academy of Science) in 2015.

==Early life and education==

Jeffrey A. Hutchings was born on September 11, 1958, in [[Orillia]], [[Ontario]] to Wendy Simpson (born [[Toronto]], [[Ontario]]) and Alexander Hutchings (born [[Sunnyside, Newfoundland and Labrador|Sunnyside]], [[Newfoundland and Labrador|Newfoundland]]). His mother studied physiotherapy and his father was a business high school teacher at Orillia District Collegiate and Vocational Institute.  

Hutchings attended Hillcrest Public School until grade 8 and then the Orillia District Collegiate & Vocational Institute for grades 9-13. He was very [[music]] oriented, successfully auditioning three years in a row for the Ontario Youth Orchestra, where he played the [[trumpet]]. The group would get together annually for a week in London, ON,  perform concerts, and even produce record albums. He thoroughly enjoyed music and came close to enrolling in a Bachelor of Music programme. He decided instead to pursue a science degree in [[biology]], his interest in this subject having been kindled by ten summers spent at the family cottage near [[Gravenhurst, Ontario|Gravenhurst]], a love of the outdoors, and a realization that biology provided opportunities to be engaged in field work.

In 1980 he graduated from the [[University of Toronto]] (U of T) with a [[Bachelor of Science]] in [[Zoology]]. During his first three years at U of T, his marks were somewhat mediocre primarily because of a lack of inspiration. The summer after his 3rd year he obtained a job with Harold H. Harvey, who studied the impact of [[Freshwater acidification|acidification]] on the [[abundance (ecology)|abundance]] and [[Biodiversity|diversity]] of [[species]] in the lakes of the  [[La Cloche Mountains]] area near Killarney, Ontario.<ref>http://www.eeb.utoronto.ca/about-us/support_us/gradscholarships/schol_harvey.htm</ref> His summer spent canoeing and collecting fish in several acid rain affected lakes sparked an interest in fish biology. In his final year as an undergraduate, he took an [[Ichthyology]] class taught by Edwin J. Crossman, someone who was to become somewhat of a mentor to Hutchings. Crossman co-authored ''Freshwater Fishes of Canada'' with W.B. Scott and worked as a [[curator]] at the [[Royal Ontario Museum]] (ROM) in the Department of Ichthyology and [[Herpetology]] as well as being a Professor at the U of T. He worked mainly with [[freshwater]] fish and was actively involved in the study of [[biodiversity]] of the fish found in the [[Great Lakes]].<ref>http://www.eeb.utoronto.ca/about-us/support_us/ugradschol/sucrossman.htm</ref>  Hutchings got a job with him during his last year at U of T and even worked for him for a year after completing his undergraduate degree on [[Muskellunge]] (''Esox masquinongy'') and [[White sucker]] (''Catostomus commersoni'') at Nogies Creek, near [[Bobcaygeon]]. 

After working at the ROM, tree-planting in northern British Columbia, and filling beer bottles at Molson’s Brewery, Hutchings decided to return to academic studies.  From 1982 to 1985, he attended the [[Memorial University of Newfoundland]] (MUN) for a [[Master of Science]] in [[Biology]], studying [[Atlantic salmon]] (''Salmo salar'') migration, life history, and their use of lakes. Supervised by Richard L. Haedrich, he thoroughly enjoyed his 12 months of field work in [[Terra Nova National Park]]. In 1983 he met [[Ransom A. Myers]], a scientist with similar interests with whom he remained close, co-authored 20 papers until Myers’ death in 2007. In 1987, under the supervision of Douglas W. Morris, he began his [[Doctor of Philosophy]] at MUN, studying life history variability of the [[Brook trout]] (''Salvelinus fontinalis''), graduating in 1991. 

In 1991, he moved to [[Scotland]] to complete [[Postdoctoral research]] at the [[University of Edinburgh]]. He was interested in the work of Dr. [[Linda Partridge]], a British [[geneticist]] who studies the biology of ageing and age-related diseases. Partridge uses the [[Common fruit fly]] (''Drosophila melanogaster'') to study questions on the cost of living and life history evolution.<ref>http://www.ucl.ac.uk/iha/linda-partridge</ref> Hutchings, who was interested in similar questions, wanted to get a sense of the strengths and weaknesses of her genetical, experimental-manipulation approach  as opposed to his field-based studies in evolutionary ecology. His first experiment was unsuccessful, but his second on male costs of reproduction resulted in his lone publication on fruit flies. Importantly, his proximity to Northern Europe allowed him to meet several researchers and teach week-long courses in 1992 in Bergen [[Norway]] and Umeå [[Sweden]] on the subject of [[Life history theory|Life History Theory]]. 

He returned to Canada in late 1992 to undertake Post Doctoral research at the Department of [[Fisheries and Oceans Canada]] (DFO) in [[St. John's, Newfoundland and Labrador|St. John’s]] [[Newfoundland and Labrador|Newfoundland]]. There he worked on [[Atlantic cod]] (''Gadus morhua'') for the first time, analyzing several lengthy time series databases on reproduction related characteristics. This was at the time of the [[Collapse of the Atlantic northwest cod fishery|collapse of Newfoundland’s northern cod fishery]], and Hutchings became interested in the different hypotheses on the cause of the collapse. On his own initiative, he used the databases to test several different hypotheses. His work at the time started a set of research initiatives on ecologically, socially, and economically important fish in which he is still interested today.

==Teaching career==

In 1995, he began an Assistant Professor position at [[Dalhousie University]] in Halifax. Hutchings had always thought he would enjoy teaching, and had even considered becoming a high school teacher like his father. He enjoys the independence that comes with being a professor, as well as the chance to discuss and communicate science with the public and decision-makers through lectures, expert panels, media, and government. He is very enthusiastic while lecturing, and enjoys seeing that enthusiasm being transferred to his students. He was appointed Canada Research Chair in Marine Conservation and Biodiversity in 2002, and in 2004 was promoted to Professor at Dalhousie University. He is Professor II at the Centre for Ecological and Evolutionary Synthesis at the [[University of Oslo]] and in the Department of Natural Sciences at the University of Agder. Over the years he has taught several classes including:
*Ecology and Evolution of Fishes (1995–present)
*Introductory Ecology (1996-2001)
*Resource Ecology (1998-2000)
*Assessment of Species at Risk in Canada (2007-2008)
*Foundations of Ecology (2000, 2004)
In addition, he has taught several special topics courses and several graduate student modules over the years. His responsibilities as a professor, other than teaching undergraduate and graduate classes, include the supervision of postgraduate students and postdoctoral fellows, and basic and applied research work.<ref name="myweb">http://myweb.dal.ca/jhutch/</ref>  One of his most notable trainees was Dylan Fraser, a former Postdoctoral Fellow now a faculty member of [[Concordia University]], who had a terrific attitude towards research and with the students.<ref>http://www.dylanfraser.com/</ref> In 2012, Hutchings became a Killam Professor (Faculty of Science) at Dalhousie University, a position of recognition.

==External organisations==

Over the years, Hutchings has contributed to several organisations in his field, the publication of other scientists’ research in journals, the protection of Canadian species at risk through [[COSEWIC]], the distribution of research grants through [[NSERC]], and the undertaking of expert panels intended to provide public awareness, scientific advice, and recommendations to decision-makers on matters of science of relevance to Canadian society.

==Professional Timeline==

*1995-1996 – Chair of Ecology, Ethology & Evolution Section, Canadian Society of Zoologists
*1997-2002 – Associate Editor, Canadian Journal of Fisheries & Aquatic Sciences
*1999-2001 – Associate Editor, Transactions of the American Fisheries Society
*1999-2002 – Councillor, Canadian Society of Zoologists
*1999-2007 – Member, Marine Fishes Species Specialist Subcommittee, COSEWIC
*2000-2001 – Member, Royal Society of Canada expert panel on Future of Food Biotechnology
*2001-2012 – Member of COSEWIC 
*2002 – Canada Research Chair in Marine Conservation & Biodiversity
*2002-2007 – Editor, Canadian Journal of Fisheries & Aquatic Sciences
*2003-2006 – Member, Evolution & Ecology Grant Selection Committee, (NSERC)
*2005-2006 – Co-Chair, Evolution & Ecology Grant Selection Committee, (NSERC)
*2006-2007 – Founding Member and Councillor, Canadian Society for Ecology and Evolution
*2006-2010 – Chair, Committee on the Status of Endangered Wildlife in Canada (COSEWIC)
*2008-2009 – Chair (inaugural), Vanier Canada Graduate Scholarships selection Committee (NSERC)
*2009-2012 – Chair, Royal Society of Canada expert panel on Sustaining Canada’s Marine Biodiversity
*2007–present – Member, Editorial Board, Evolutionary Application
*2008–present – Member, Editorial Board, Environmental Reviews
*2009–2015 – Member, Editorial Board, Proceedings of the Royal Society B
*2009–present – Scientific Advisor, Loblaw Companies
*2010–present – Member, Research Advisory Panel, Science Media Centre of Canada
*2012-2013 – President, Canadian Society for Ecology and Evolution
*2012–present – Member, Board of Directors, WWF Canada
*2012 – Fellow, Royal Canadian Geographical Society
*2015 – Fellow, Royal Society of Canada
<ref>http://myweb.dal.ca/jhutch/index/CV%20March%202011.pdf</ref>

==Scientific contributions==

===Areas of research===
*Life History Evolution in Fishes
*Phenotypic Plasticity and Norms of Reaction
*Recovery and Extinction Probabilities of Species at Risk
*Demographic and Evolutionary Consequences of Over-Exploitation
*Farmed-Wild Atlantic Salmon Interactions
*Mating Systems in Atlantic Salmon and Atlantic Cod
*Biodiversity of Arctic and Marine Fishes
*Communication of Science to Society

===Research===
Hutchings’ research focuses on the life history evolution, behavioural ecology, population dynamics, and conservation biology of marine and anadromous fishes.<ref name="myweb" /> In particular, his research focuses on 3 species: Atlantic cod (''Gadus morhua''), Atlantic salmon (''Salmo salar''), and Brook trout (''Salvelinus fontinalis''). He has explored various questions on the interactions between wild and farmed Atlantic salmon, the biodiversity of Arctic and sub-Arctic fishes (Baffin and Ellesmere Islands), and population consequences of fisheries-induced evolution. Hutchings’ best known scientific contributions  pertain to his work on the collapse, recovery, and sustainable harvesting of marine fishes. He was one of the first researchers to examine the correlates of recovery of depleted marine fish populations,  work that has now extended to studying factors that affect the recovery of depleted species in general.

Over the years his research interests, although varied and broad, have remained generally the same. As of 2012, Hutchings is still following up on several of his research areas of interest, including life-history evolution, alternative reproductive strategies, salmon aquaculture, conservation biology, communication of science to society, phenotypic plasticity, and the fitness consequences of organismal responses to directional temperature change.

==Views==
Hutchings believes that an integral part of being a research scientist is giving back to society and communicating with society as effectively as possible. He does this through his teaching, but also by delivering plenary and keynote lectures, appearing before parliamentary committees, publishing his research findings, and also by interacting with the media. As one example, on the 10th of July, 2012, Hutchings was a speaker at a rally on Parliament Hill in Ottawa for The Death of Evidence. The rally was in protest of the closure of federal scientific programmes, scientifically unsupported changes to national legislation, and the “muzzling” of government scientists.<ref>http://www.deathofevidence.ca/</ref> (The idea behind the rally was that democracy depends on informed opinions, which rely on evidence provided by scientific research.)

==Publications==

===Books===
Hutchings co-authored a book entitled ''Ecology: A Canadian Context'' which was published in 2011 (second edition 2015). The book, which covers the core concepts of ecology, is the first resource that integrates evolution and sustainable development. In addition, it profiles the extensive ecological research being conducted in Canada to provide a more relevant text for Canadian students and instructors.

Freedman, B., Hutchings, J.A., Gwynne, D.T., Smol, J.P., Suffling, R., Turkington, R., Walker, R.L., and D. Bazely. 2015. Ecology: A Canadian Context. Nelson Education, Toronto.

===Journal Publications (Selected publications)===

*Hutchings, J.A., and N.C. Stenseth. 2016. Communication of science advice to government. Trends in Ecology and Evolution 31: 7-11.
*Hutchings, J.A. 2015. Thresholds for impaired species recovery. Proceedings of the Royal Society B 282: 20150654.
*Neubauer, P., Jensen, O.P., Hutchings, J.A., and J.K. Baum. 2013. Resilience and recovery of overexploited marine populations. Science 340: 347-349. 
*Hutchings, J.A., Myers, R.A., García, V.B., Lucifora, L.O., and A. Kuparinen. 2012. Life-history correlates of extinction risk and recovery potential. Ecological Applications 22: 1061-1067.
*Hutchings, J.A. 2011. Old wine in new bottles: reaction norms in salmonid fishes. Heredity 106: 421-437.
*Hutchings, J.A., and R.W. Rangeley. 2011. Correlates of recovery for Canadian Atlantic cod. Canadian Journal of Zoology 89: 386-400.
*Hutchings, J.A., Minto, C., Ricard, D., Baum, J.K., and O.P Jensen. 2010. Trends in the abundance of marine fishes. Can. J. Fish. Aquat. Sci. 67: 1205-1210.
*Hutchings, J.A. 2009. Avoidance of fisheries-induced evolution: management implications for catch selectivity and limit reference points. Evol. Applic. 2: 324-334.
*Hutchings, J.A. 2008. Ransom Aldrich Myers (1952-2007): in memoriam. Can. J. Fish. Aquat. Sci. 65: vii-xix.
*Hutchings, J.A., and D.J. Fraser. 2008. The nature of fishing- and farming-induced evolution. Molecular Ecology 17: 294-313.
*Hutchings, J.A., Swain, D.P., Rowe, S., Eddington, J.D., Puvanendran, V., and J.A. Brown. 2007. Genetic variation in life-history reaction norms in a marine fish. Proc. R. Soc. B 274: 1693-1699.
*Hutchings, J.A. 2005. Life history consequences of overexploitation to population recovery in Northwest Atlantic cod (Gadus morhua). Can. J. Fish. Aquat. Sci. 62: 824-832.
*Hutchings, J.A., and J.K. Baum. 2005. Measuring marine fish biodiversity: temporal changes in abundance, life history, and demography. Phil. Trans. R. Soc. Lond. 360: 315-338.
*Hutchings, J.A. 2004. The cod that got away. Nature 428: 899-900.
*Hutchings, J.A. 2004. Norms of reaction and phenotypic plasticity in salmonid life histories. In: A.P. Hendry and S.C. Stearns [eds], Evolution Illuminated: Salmon and Their Relatives, Oxford University Press, Oxford, pp.&nbsp;154–174.
*Hutchings, J.A., and J.D. Reynolds. 2004. Marine fish population collapses: consequences for recovery and extinction risk. BioScience 54: 297-309.
*Hutchings, J.A. 2002. Life histories of fish. In: P.J.B. Hart and J.D. Reynolds [eds], Handbook of Fish Biology and Fisheries Vol. 1. Fish Biology, Blackwell Science, Oxford, pp.&nbsp;149–174.
*Hutchings, J.A. 2000. Collapse and recovery of marine fishes. Nature (Lond.) 406: 882-885.
*Hutchings, J.A., Bishop, T.D., and C.R. McGregor-Shaw. 1999. Spawning behaviour of Atlantic cod, Gadus morhua: evidence of mate competition and mate choice in a broadcast spawner. Can. J. Fish. Aquat. Sci. 56: 97-104.
*Hutchings, J.A., and M.E.B. Jones. 1998. Life history variation and growth rate thresholds for maturity in Atlantic salmon, Salmo salar. Can. J. Fish. Aquat. Sci. 55 (Suppl. 1): 22-47.
*Hutchings, J.A., Walters, C., and R.L. Haedrich. 1997. Is scientific inquiry incompatible with government information control? Can. J. Fish. Aquat. Sci. 54: 1198-1210.
*Hutchings, J.A., and R.A. Myers. 1995. The biological collapse of Atlantic cod off Newfoundland and Labrador: an exploration of historical changes in exploitation, harvesting technology, and management. In Arnason, R., and L.F. Felt [eds], The North Atlantic Fishery: Strengths, Weaknesses, and Challenges, Inst. Island Stud., Univ. Prince Edward Island, Charlottetown, PEI, pp.&nbsp;37–93.
*Hutchings, J.A., and R.A. Myers. 1994. What can be learned from the collapse of a renewable resource? Atlantic cod, Gadus morhua, of Newfoundland and Labrador. Can. J. Fish. Aquat. Sci. 51: 2126-2146.
*Hutchings, J.A., and R.A. Myers. 1994. The evolution of alternative mating strategies in variable environments. Evolutionary Ecology 8: 256-268.
*Hutchings, J.A. 1993. Adaptive life histories effected by age-specific survival and growth rate. Ecology 74: 673-684.
*Hutchings, J.A. 1991. Fitness consequences of variation in egg size and food abundance in brook trout, Salvelinus fontinalis. Evolution 45: 1162-1168.
*Hutchings, J.A. 1986. Lakeward migrations by juvenile Atlantic salmon, Salmo salar. Can. J. Fish. Aquat. Sci. 43: 732-741.
*Hutchings, J.A., and D.W. Morris. 1985. The influence of phylogeny, size and behaviour on patterns of covariation in salmonid life histories. Oikos 45: 118-124.
*Hutchings, J.A., and R.A. Myers. 1985. Mating between anadromous and nonanadromous Atlantic salmon, Salmo salar. Can. J. Zool. 63: 2219-2221.
*Hutchings, J.A., and R.L. Haedrich. 1984. Growth and population structure in two species of bivalves (Nuculanidae) from the deep sea. Mar. Ecol. Prog. Ser. 17: 135-142.

==References==
{{reflist|2}}

==External links==
{{external links|date=January 2013}}
*http://myweb.dal.ca/jhutch/
*http://biology.dal.ca/People/faculty/hutchings/hutchings.htm
*http://www.aquatic.uoguelph.ca/Human/Research/gh/hutchingsj.htm
*http://www.killamtrusts.ca/Dalhousie.asp
*http://www.rsc.ca
*http://www.ecoevo.ca
*http://www.nserc.ca
*http://www.cosewic.gc.ca
*http://www.dylanfraser.com/
*http://www.dfo-mpo.gc.ca/index-eng.htm
*http://www.eeb.utoronto.ca/about-us/support_us/gradscholarships/schol_harvey.htm
*http://www.phys.ocean.dal.ca/ccffr/TributetoEdCrossman.html
*http://www.rom.on.ca/
*http://old.richarddawkins.net/articles/646470-death-of-scientific-evidence-mourned-on-parliament-hill
*http://www.deathofevidence.ca/
*http://rspb.royalsocietypublishing.org/
*http://www.nrcresearchpress.com/journal/er
*http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1752-4571

{{DEFAULTSORT:Hutchings, Jeffrey A.}}
[[Category:1958 births]]
[[Category:Canadian biologists]]
[[Category:Dalhousie University faculty]]
[[Category:Living people]]
[[Category:People from Orillia]]