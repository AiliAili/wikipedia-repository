'''MobiCom''', the '''International Conference on Mobile Computing and Networking''', is a series of annual conferences sponsored by [[Association for Computing Machinery|ACM]] [[SIGMOBILE]] dedicated to addressing the challenges in the areas of [[mobile computing]] and [[wireless networking|wireless]] and [[mobile phone|mobile networking]]. Although no rating system for computer networking conferences exists, MobiCom is generally considered to be the best conference in these areas, and it is the fifth highest-impact venue in all of Computer Science.<ref>Estimated impact of publication venues in Computer Science, http://citeseer.ist.psu.edu/impact.html</ref> The quality of papers published in this conference is very high. The acceptance rate of MobiCom typically around 10%, meaning that only one tenth of all submitted papers make it through the tough [[peer review]] filter.<ref>Kevin Almeroth's Conference Statistics, http://www.cs.ucsb.edu/~almeroth/conf/stats/#mobicom</ref>

According to [[SIGMOBILE]], "the MobiCom conference series serves as the premier international forum addressing networks, systems, algorithms, and applications that support the symbiosis of mobile computers and wireless networks.  MobiCom is a highly selective conference focusing on all issues in mobile computing and wireless and mobile networking at the link layer and above."

MobiCom Conferences have been held at the following locations:

* MobiCom 2015, [[Paris]], France, 7–11 September 2015
* MobiCom 2014, [[Maui]], [[Hawaii]], United States, 7–11 September 2014
* MobiCom 2013, [[Miami]], [[Florida]], United States, 30 September-4 October 2013
* MobiCom 2012, [[Istanbul]], [[Turkey]], 22–26 August 2012
* MobiCom 2011, [[Las Vegas Valley|Las Vegas]], [[Nevada]], United States, 19–23 September 2011
* MobiCom 2010, [[Chicago]], [[Illinois]], United States, 20–24 September 2010
* MobiCom 2009, [[Beijing]], China, 20–25 September 2009
* MobiCom 2008, [[San Francisco]], [[California]], United States, 13–19 September 2008
* MobiCom 2007, [[Montreal]], [[Quebec]], Canada, 9–14 September 2007
* MobiCom 2006, [[Los Angeles]], [[California]], United States, 23–29 September 2006
* MobiCom 2005, [[Cologne]], Germany, 28 August-2 September 2005
* MobiCom 2004, [[Philadelphia]], [[Pennsylvania]], United States, 26 September-1 October 2004
* MobiCom 2003, [[San Diego]], [[California]], United States, 14–19 September 2003
* MobiCom 2002, [[Atlanta]], [[Georgia (U.S. state)|Georgia]], United States, 23–26 September 2002
* MobiCom 2001, [[Rome]], Italy, 16–21 July 2001
* MobiCom 2000, [[Boston]], [[Massachusetts]], United States, 6–11 August 2000
* MobiCom '99, [[Seattle]], [[Washington (U.S. state)|Washington]], United States, 15–20 August 1999
* MobiCom '98, [[Dallas]], [[Texas]], United States, 25–30 October 1998
* MobiCom '97, [[Budapest]], Hungary, 26–30 September 1997
* MobiCom '96, [[Rye (city), New York|Rye]], [[New York (state)|New York]], United States, 10–12 November 1996
* MobiCom '95, [[Berkeley, California|Berkeley]], [[California]], United States, 13–15 November 1995

==References==
{{Reflist}}

==External links==
* [http://www.sigmobile.org/ SIGMOBILE]
* [http://www.sigmobile.org/mobicom/ MobiCom]

{{DEFAULTSORT:International Conference On Mobile Computing And Networking}}
[[Category:Computer networking conferences]]


{{compu-conference-stub}}