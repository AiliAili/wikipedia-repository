<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Profi
 | image=AEROS 2 Trike.JPG
 | caption=A Profi TL wing mounted on an [[Aeros-2]] trike
}}{{Infobox Aircraft Type
 | type=[[Ultralight trike]] [[wing]]
 | national origin=[[Ukraine]]
 | manufacturer=[[Aeros]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production (2013)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!-- [[US$]] [[Euro|€]] [[Pound sterling|£]] (date) -->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Aeros Profi''' is a [[Ukraine|Ukrainian]] high-performance [[ultralight trike]] [[wing]], designed and produced by [[Aeros]] of [[Kiev]]. The wing is widely used on Aeros trikes as well as by other [[ultralight aircraft]] manufacturers.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 198. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The base model Profi wing is a [[Flying wires|cable-braced]], [[king post]]-equipped [[hang glider]]-style wing for two-place trikes, while the Profi TL is a "topless" design, lacking the king post and using streamlined lower [[lift strut]]s instead of wires. The topless design lowers the overall height of the wing and allows storage in lower-roofed hangars. A [[carbon fibre]] [[leading edge]] insert is optional, while [[winglets]] are standard on the TL model.<ref name="WDLA11" /><ref name="ProfiTL">{{cite web|url = http://www.aeros.com.ua/structure/trike/prfTL_en.php|title = The Aeros PROFI TL - another step forward! |accessdate = 19 February 2013|last = [[Aeros]]|year = 2005}}</ref>

The wing is made from bolted-together [[aluminum]] tubing, with its double surface wing covered in [[Dacron]] sailcloth. Its {{convert|10|m|ft|1|abbr=on}} span wing has an area of  {{convert|14.5|m2|sqft|abbr=on}}, a nose angle of 128° and uses an "A" frame weight-shift control bar. An electro-mechanical trim is optional.<ref name="WDLA11" /><ref name="ProfiTLSpecs">{{cite web|url = http://www.aeros.com.ua/structure/trike/tkprfTL_en.php|title = Profi TL Performance|accessdate = 19 February 2013|last = [[Aeros]]|year = 2005}}</ref><ref name="ProfiSpecs">{{cite web|url = http://www.aeros.com.ua/structure/trike/tkprf_en.php|title = Profi Performance|accessdate = 19 February 2013|last = [[Aeros]]|year = 2005}}</ref>

==Variants==
;Profi
:Base model with a king post and cable bracing.<ref name="Profi">{{cite web|url = http://www.aeros.com.ua/structure/trike/prf_en.php|title = PROFI - Wing for Professionals |accessdate = 19 February 2013|last = [[Aeros]]|year = 2005}}</ref>
;Profi TL
:Topless model, lacking the king post, using streamlined lift struts and with standard winglets.<ref name="WDLA11" /><ref name="ProfiTL" />

==Applications==
*[[Aeros-2]]
*[[Aeros Cross Country]]
*[[Aeros del Sur Manta]]
*[[Apollo Delta Jet]]
*[[Apollo Jet Star]]
*[[Apollo Monsoon]]
*[[Exkluziv Joker]]

==Specifications (Profi TL) ==
{{Aircraft specs
|ref=Bayerl and Aeros<ref name="WDLA11" /><ref name="ProfiTLSpecs" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=10
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=14.5
|wing area sqft=
|wing area note=
|aspect ratio=6.9:1
|airfoil=
|empty weight kg=52
|empty weight lb=
|empty weight note=wing weight
|gross weight kg=450
|gross weight lb=
|gross weight note=maximum aircraft gross weight allowed
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=125
|cruise speed mph=
|cruise speed kts=
|cruise speed note=with trim installed
|stall speed kmh=52
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=140
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+4/-2
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=
|sink rate ftmin=
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
}}

==References==
{{reflist}}

==External links==
*[http://www.aeros.com.ua/structure/trike/prf_en.php Official Profi webpage]
*[http://www.aeros.com.ua/structure/trike/prfTL_en.php Official Profi TL webpage]
{{Aeros aircraft}}

[[Category:Ultralight trike wings]]