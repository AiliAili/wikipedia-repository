{{distinguish|The Double (Saramago novel)}}
{{italic title}}
[[File:ThedoubleDOSTOYEVSKY.jpg.w180h281.jpg|thumb|right|Cover of The Double: A Petersburg Poem]]
'''''The Double''''' ({{lang-ru|Двойник}}, ''Dvoynik'') is a novella written by [[Fyodor Dostoyevsky]]. It was first published on January 30, 1846 in the ''[[Fatherland Notes]]''.<ref>{{cite book
|title=Dostoevsky: His Life and Work
|last=Mochulsky
|first=Konstantin
|others=Trans. Minihan, Michael A
|year=1973
|origyear=First published 1967
|publisher=[[Princeton University Press]]
|location=[[Princeton, New Jersey|Princeton]]
|isbn=0-691-01299-7
|page=46
|url=https://books.google.com/books?id=mDKphT8_XLsC
|ref = harv}}</ref> It was subsequently revised and republished by Dostoevsky in 1866.<ref>Harden, Evelyn J. "Translator's Introduction." Introduction. The Double: Two Versions. Ann Arbor, MI: Ardis, 1985. Ix-Xxxvi. Print.</ref>  It is narrated in the first person.

==Plot summary==

''The Double'' centers on a government clerk who goes mad. It deals with the internal psychological struggle of its main character, Yakov Petrovich Golyadkin, who repeatedly encounters someone who is his exact double in appearance but confident, aggressive, and extroverted, characteristics that are the polar opposites to those of the toadying "pushover" protagonist.  The motif of the novella is a [[doppelgänger]] (''dvoynik'').

Golyadkin is a [[Table of Ranks|titular councillor]]. This is rank 9 in the [[Table of Ranks]] established by [[Peter the Great]]. As rank eight led to hereditary nobility,<ref>Peace, Richard. Introduction. In Plays and Petersburg Tales, Vii-Xxx. Oxford: Oxford Paperbacks, 1998, xii.</ref> being a titular councillor is symbolic of a low-level bureaucrat still struggling to succeed. Golyadkin has a formative discussion with his Doctor Rutenspitz, who fears for his sanity and tells him that his behavior is dangerously antisocial. He prescribes "cheerful company" as the remedy. Golyadkin resolves to try this, and leaves the office. He proceeds to a birthday party for Klara Olsufyevna, the daughter of his office manager. He was uninvited, and a series of faux pas lead to his expulsion from the party. On his way home through a [[snowstorm]], he encounters his double, who looks exactly like him. The following two thirds of the novel then deals with their evolving relationship.

At first, Golyadkin Sr. (the original main character) and Golyadkin Jr. (his double) are friends, but Golyadkin Jr. proceeds to attempt to take over Sr.'s life, and they become bitter enemies. Because Golyadkin Jr. has all the charm, unctuousness and social skills that Golyadkin Sr. lacks, he is very well-liked among the office colleagues. At the story's conclusion, Golyadkin Sr. begins to see many replicas of himself, has a psychotic break, and is dragged off to an asylum by Doctor Rutenspitz.

==Influences==

''The Double'' is the most [[Gogol]]esque of Dostoyevsky's works; its subtitle "A Petersburg Poem" echoes that of Gogol's ''[[Dead Souls]]''. [[Vladimir Vladimirovich Nabokov|Vladimir Nabokov]] called it a [[parody]] of "[[The Overcoat]]". Many others have emphasized the relationship between ''The Double'' and other of [[Gogol]]'s Petersburg Tales. One contemporary critic, Konstantin Aksakov, remarked that "Dostoevsky alters and wholly repeats Gogol’s phrases."<ref>Konstantin Aksakov, qtd. in Mochulsky, Dostoevsky: zhizn I tvorchestvo (Paris, 1947): 59, qtd in. Fanger, Donald. Dostoevsky and Romantic Realism: A Study of Dostoevsky in Relation to Balzac, Dickens, and Gogol. Chicago: University of Chicago Press, 1965, 159.</ref> Most scholars, however, recognize The Double as Dostoevsky’s response to or innovation on Gogol’s work. For example, A.L. Bem called ''The Double'' "a unique literary rebuttal" to [[The Nose (Gogol)]].<ref>A.L. Bem's essay "The Double and 'The Nose'" in Meyer, Priscilla, and Stephen Rudy, eds. Dostoevsky & Gogol: Texts and Criticism. Ann Arbor: Ardis, 1979, 248.</ref>

This immediate relationship is the obvious manifestation of Dostoevsky's entry into the deeper tradition of [[German Romanticism]], particularly the writings of [[E. T. A. Hoffmann]].

==Critical reception==

'' Double'' has been interpreted in a number of ways. Looking backwards, it is viewed as Dostoevsky's innovation on [[Gogol]]. Looking forwards, it is often read as a psychosocial version of his later ethical-psychological works.<ref>Chizhevsky, Dmitri. "The Theme of the Double in Dostoevsky." In Dostoevsky: A Collection of Critical Essays, edited by Rene Wellek, 112-29. Englewood Cliffs, NJ: Prentice-Hall, 1965, 124.</ref> These two readings, together, position ''The Double'' at a critical juncture in Dostoevsky's writing at which he was still synthesizing what preceded him but also adding in elements of his own. One such element was that Dostoevsky switched the focus from [[Gogol]]'s social perspective in which the main characters are viewed and interpreted socially to a psychological context that gives the characters more emotional depth and internal motivation.<ref>Valerian Maykov in Terras, Victor. "The Young Dostoevsky: An Assessment in the Light of Recent Scholarship." In New Essays on Dostoevsky, edited by Malcolm V. Jones and Garth M. Terry, 21-40. Bristol, Great Britain: Cambridge University Press, 1983, 36.</ref>

As to the interpretation of the work itself, there are three major trends in scholarship. First, many have said that Golyadkin simply goes insane, probably with [[schizophrenia]].<ref>Rosenthal, Richard J. "Dostoevsky's Experiment with Projective Mechanisms and the Theft of Identity in The Double." In Russian Literature and Psychoanalysis, 59-88. Vol. 31. Linguistic & Literary Studies in Eastern Europe. Amsterdam/Philadelphia: John Benjamins Publishing Company, 1989, 87.</ref> This view is supported by much of the text, particularly Golyadkin's innumerable hallucinations.

Second, many have focused on Golyadkin's search for identity. One critic wrote that ''The Double'''s main idea is that "'the human will in its search for total freedom of expression becomes a self-destructive impulse.’"<ref>Terras, Victor. "The Young Dostoevsky: An Assessment in the Light of Recent Scholarship." In New Essays on Dostoevsky, edited by Malcolm V. Jones and Garth M. Terry, 21-40. Bristol, Great Britain: Cambridge University Press, 1983, 35.</ref>

This individualistic focus is often contextualized by scholars, such as Joseph Frank, who emphasize that Golyadkin's identity is crushed by the bureaucracy and stifling society he lives in.<ref>Frank, Joseph. "The Double." In The Seeds of Revolt: 1821-1849., 295-312. Princeton: Princeton University Press, 1979, 300.</ref>

The final context of understanding for ''The Double'' that transcends all three categories is the ongoing debate about its literary quality. While the majority of scholars have regarded it as somewhere from "too fragile to bear its significance"<ref>Frank, The Seeds of Revolt, 295.</ref> to utterly unreadable, there have been two notable exceptions. Dostoevsky wrote in [[A Writer's Diary]] that "Most decidedly, I did not succeed with that novel; however, its idea was rather lucid, and I have never expressed in my writings anything more serious. Still, as far as form was concerned, I failed utterly."<ref>Dostoevsky, Fyodor. "The History of the Verb "Stushevatsia"" In Diary of a Writer, translated by Boris Brasol, 882-85. Vol. II. New York, N.Y.: Octagon Books, 1973, 883.</ref> [[Vladimir Nabokov]], who generally regarded [[Fyodor Dostoyevsky|Dostoyevsky]] as a "rather mediocre" writer called ''The Double'' "the best thing he ever wrote," saying that it is "a perfect work of art."<ref>Nabokov, Vladimir Vladimirovich. "Fyodor Dostoevski." In Lectures on Russian Literature, compiled by Fredson Bowers, 97-136. New York: Harcourt Brace Jovanovich/Bruccoli Clark, 1981, 68.</ref>

== Adaptation ==
The story was adapted into an English play of the same name, which was later adapted into [[The Double (2013 film)|a 2013 film of the same name]], starring [[Jesse Eisenberg]].

== References ==
{{mirsky}}
<references />

== External links ==
* [http://ebooks.adelaide.edu.au/d/dostoyevsky/d72d/ Full text of ''The Double'' in English]
* [http://ilibrary.ru/text/18/p.1/ Full text of ''The Double'' in the original Russian]
* {{librivox book | title=The Double | author=Fyodor Dostoyevsky}}

{{Fyodor Dostoevsky}}

{{Authority control}}

{{DEFAULTSORT:Double: A Petersburg Poem}}
[[Category:1848 novels]]
[[Category:Novels by Fyodor Dostoyevsky]]
[[Category:Existentialist novels]]
[[Category:Novels set in Saint Petersburg]]
[[Category:Russian novellas]]