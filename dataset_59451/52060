{{Italic title}}
The '''''Journal of Shoulder and Elbow Surgery''''' is a [[Peer review|peer-reviewed]] [[medical journal]] covering [[orthopedic surgery]] related to the shoulder, elbow, and upper extremities. It is the official journal of multiple shoulder and elbow societies, including the [[American Shoulder and Elbow Surgeons]], the [[European Society for Surgery of Shoulder and Elbow]], the [[Japan Shoulder Society]], the [[Shoulder and Elbow Society of Australia]], the [[South American Shoulder and Elbow Society]], the [[South African Shoulder and Elbow Surgeons]], the [[Asian Shoulder Association]], the [[Korean Shoulder and Elbow Society]], the [[International Congress of Shoulder and Elbow Surgery]], and the [[American Society of Shoulder and Elbow Therapists]].

== History ==
The journal was established in January 1991, with [[Mosby (publisher)|Mosby]], now an imprint of [[Elsevier]], as the publisher. The founding [[editor-in-chief]] was Robert Cofield ([[Mayo Clinic]]). In 1997, Robert Neviaser ([[George Washington University Medical Center]]) took over as editor-in-chief. In 2008, [[Bill Mallon]] (Triangle Orthopaedic Associates, [[Durham, North Carolina]]) was named as the third editor-in-chief.<ref>{{cite web |first=Stephen |last=Copeland |year=2007 |url=http://www.icses.org/ibses/history.html |title=History |accessdate=2013-02-24}}</ref> Originally the journal was published bimonthly. In 2010, it began publishing 8 issues per year, increased in 2012 to monthly.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[PubMed]], [[MEDLINE]], [[EMBASE]], and [[Scopus]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.289, ranking it 18th out of 72 journals in the category "Orthopedics",<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Orthopedics |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 17th out of 81 journals in the category "Sport Science",<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Sport Science |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science |postscript=.}}</ref> and 60th out of 198 journals in the category "Surgery".<ref name=WoS3>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Surgery |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |series=Web of Science |postscript=.}}</ref>

== References ==
{{reflist|30em}}

==External links==
* {{Official website|http://www.jshoulderelbow.org/}}
* [http://www.journals.elsevier.com/journal-of-shoulder-and-elbow-surgery/ Journal page at publisher's website]
* {{ISSN|1058-2746}}

[[Category:Orthopedics journals]]
[[Category:Surgery journals]]
[[Category:Monthly journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1991]]
[[Category:English-language journals]]