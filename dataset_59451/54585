{{infobox book | 
| name          = Fanshawe
| image         = 
| image_size = 
| caption =  
| author        = [[Nathaniel Hawthorne]]
| country       = United States
| language      = English
| genre         = [[Romantic novel]]
| publisher     = Marsh and Capen
| release_date  = 1828 
| english_release_date = 
| media_type    = Print
 
}}
'''''Fanshawe''''' is a novel written by American author [[Nathaniel Hawthorne]]. It was his first published work, which he published anonymously in 1828.

==Background==
Hawthorne had worked on a novel as early as his time as an undergraduate at [[Bowdoin College]]. ''Fanshawe'', his first published novel, may or may not have been that book.<ref name=Mellow41>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 41. ISBN 0-395-27602-0</ref> ''Fanshawe: A Tale'' appeared anonymously in October 1828 from the Boston publishers Marsh and Capen. Its printing was paid for at the author's own expense, costing him $100.<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 41–42. ISBN 0-395-27602-0</ref> The book was based on Hawthorne's experiences as a Bowdoin College student in the early 1820s.

''Fanshawe'' generally received positive reviews. [[Sarah Josepha Hale]], then editor of the ''Ladies' Magazine'', advised potential readers buy the book rather than rely on finding it at a circulating library.<ref>Okker, Patricia. ''Our Sister Editors: Sarah J. Hale and the Tradition of Nineteenth-Century American Women Editors''. Atlanta: University of Georgia Press, 1995: 89. ISBN 0-8203-1686-5</ref> As she wrote, "Purchase it, reader. There is but one volume, and trust me that it is worth placing in your library."<ref name=Mellow43>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 43. ISBN 0-395-27602-0</ref> [[William Leggett (writer)|William Leggett]] saw further potential in the young author: "The mind that produced this little, interesting volume, is capable of making great and rich additions to our native literature."<ref name=Mellow43/>

The book, however, did not sell well. After its commercial failure, Hawthorne burned the unsold copies: "Later all the copies that could be obtained were destroyed. A dozen years after his death a copy was found and the tale reissued by James o & co."  (quote cf. N.E. Brown, Bibl. of Nathaniel Hawthorne, Boston and New York, 1905) The novel was so rare and Hawthorne was so secretive about his early attempt at a novel that after his death his wife [[Sophia Hawthorne|Sophia]] insisted her husband had never written a novel with that title, despite being shown a copy.<ref>Nelson, Randy F. ''The Almanac of American Letters''. Los Altos, California: William Kaufmann, Inc., 1981: 73. ISBN 0-86576-008-X</ref>

==Summary==

Dr. Melmoth, the President of fictional Harley College, takes into his care Ellen Langton, the daughter of his friend, Mr. Langton, who is at sea. Ellen is a young, beautiful girl and attracts the attentions of the college boys, especially Edward Walcott, a strapping though immature student, and Fanshawe, a reclusive, meek intellectual. While out walking, the three young people meet a nameless character called “the angler,” a name he gets for appearing an expert fisherman. The angler asks for a word with Ellen, tells her something in secret, and apparently flusters her. Walcott and Fanshawe become suspicious of his intentions.

We learn that the angler is an old friend of the reformed Inn owner, Hugh Crombie. The two had been at sea together, where Mr. Langton had been the angler's mentor and caretaker. Langton and the angler had a falling out, however, and, thinking that Langton has been killed at sea, the angler undertakes to marry Ellen in order to inherit her father's considerable wealth. Thus in his secret meeting with Ellen, the angler instructs her to sneak out of Melmoth's home and follow him, telling her he has information about her father’s whereabouts. His real aim, though, is to kidnap her, to tell her of her father’s death, and to manipulate her into marrying him.

When the various men (Melmoth, Edward, Fanshawe) learn that she is not in her chamber, they go searching for her. The search reveals the nature of each: Melmoth, an aged scholar unused to physical labor, enlists the help of Walcott, who is the most skilled rider and the most likely to be able to contend with the angler in a fight. Fanshawe, who lags behind the search because of his weak constitution and his slow horse, is given information by an old woman in a cabin (where another old woman, Widow Butler, who turns out to be the angler's mother, has just died) that allows him to reach the angler and Ellen first. The angler has taken Ellen to a craggy cliff and cave, where he intends to hold her captive. Ellen has finally realized the angler's intentions. When Fanshawe arrives, he stands above them, looking over the edge of the cliff. The angler begins to climb up the cliff to fight Fanshawe but grabs a twig too weak to support him and tumbles to his death. Fanshawe awakens Ellen from a faint, and they travel back to town together.

Fanshawe loves Ellen but knows that he will die young because of his shut-in lifestyle. When Langton offers Ellen's hand in marriage to Fanshawe in exchange for rescuing her, he refuses, sacrificing his happiness so as not to subject her to a life of widowhood. He also knows that Ellen has affections for Walcott. Fanshawe dies at 20. Ellen and Walcott marry four years later. The narrator states that Walcott grows out of his childish ways (drunkenness, impulsiveness, the suggestion of teenage affairs) and becomes content with Ellen. They are, according to the narrator, happy, but the book ends on an ambivalent note, stating that the couple did not produce children.

==References==
{{reflist}}

==External links==
{{wikisource|Fanshawe}}
{{Gutenberg|no=7085|name=Fanshawe}}

{{Nathaniel Hawthorne}}

{{DEFAULTSORT:Fanshawe (Novel)}}
[[Category:1828 novels]]
[[Category:Novels by Nathaniel Hawthorne]]
[[Category:19th-century American novels]]
[[Category:Works published anonymously]]
[[Category:Debut novels]]