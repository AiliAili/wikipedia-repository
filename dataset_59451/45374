{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox person
|name        = Cecil Stanley Grace
|image       = 
|caption     = 
|birth_date  = 1880
|birth_place = Chile
|death_date  = 22 December 1910, (Age 30)
|death_place = English Channel
|other_names = 
|known_for   = Pioneer aviator
|occupation  = Pilot
|nationality = British
}}
'''Cecil Stanley Grace''' (1880–1910) was a pioneer aviator who [[List of aerial disappearances|went missing on a flight]] across the English Channel in 1910.

==Family==
Grace was born in Chile, the son of John William Grace of New York. His uncle was [[W. R. Grace]], a former mayor of New York City. Grace was naturalised as a British citizen in October 1910.<ref name="naturalisation" />

==Eastchurch==
In 1909 members of the [[Aero Club of Great Britain]] established a flying ground at [[RAF Eastchurch|Eastchurch]] on the [[Isle of Sheppey]]. Grace was one of this early group of pioneering aviators, and in 1910 he was awarded only the fourth [[Royal Aero Club]] [[List of pilots awarded an Aviator's Certificate by the Royal Aero Club in 1910|Aviator's Certificate]].<ref>[http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200293.html ''Flight'' 16 April 1910]</ref><ref>Dallas Brett, R. ''History of British Aviation 1908–1914.'' John Hamilton Ltd, 1933.</ref>

==Final flight==
[[File:Short S.27.jpg|thumb|left| Short S.27 aircraft]]
[[File:LondonCalaisParis.png|thumb|right|300px|Map showing Calais in relation to London and Paris]]
[[File:Be-map.png|thumb|right|Map of Belgium with Ostend at upper left hand corner]]

In 1910, a number of early aviators were competing for the [[Baron de Forest]] Prize of £4,000 for the longest flight from England into continental Europe.<ref>Roberts, Priscilla Mary, ed. ''World War I: A Student Encyclopedia''. p. 1706. ABC-CLIO, 2005. ISBN 1-85109-879-8.</ref> [[Thomas Sopwith|Tom Sopwith]] was the first to try with a 170-mile flight into Belgium on 17 December 1910. [[Claude Grahame-White]] crashed his aircraft before he could make an attempt. Grace departed Swingate Downs on 22 December 1910 flying a [[Short S.27]] in an attempt at the prize.<ref name="missing1" /> The sea was covered in mist, but a telegram was received that Grace had landed due to the strong winds near the village of [[Les Baraques]] near [[Calais]].<ref name="missing1" /> 

He eventually made it to Calais, but with strong winds he decided to return to Eastchurch via Dover and attempt a prize flight on another day.<ref name="missing1" /> After lunch in Calais at about ten past two in the afternoon Grace left Calais to return to England. The journey to Dover was expected to last no longer than 40 minutes, but by 3:30 he had not arrived.<ref name="missing1" /> An aeroplane had been sighted by the Coastguard from [[Ramsgate]] at about 3 o'clock about six miles out to sea near the [[Goodwin Sands]] heading north.<ref name="missing1" />

For a few days it was hoped that Grace had managed to land somewhere, but on 6 January 1911<ref>{{cite news | url=http://chroniclingamerica.loc.gov/lccn/sn83030214/1911-03-15/ed-1/seq-1/;words=CECIL+GRACE?date1=1911&sort=relevance&sort=relevance&sort=relevance&sort=relevance&sort=relevance&rows=20&searchType=basic&state=&date2=1911&proxtext=Cecil+grace&y=13&x=17&dateFilterType=yearRange&page=3&page=2&page=1&page=2&page=3&index=5 | newspaper=New York Tribune | date=15 March 1911 | title=Fate of Cecil Grace | accessdate=3 January 2015 }}</ref> a pilot's goggles and cap washed ashore at [[Mariakerke (West Flanders)|Mariakerke]] {west of Ostend Belgium} were later identified as Grace's.<ref name="missing2" /><ref name="missing3" /> Reportedly his aircraft wreckage was found near the same location.<ref>{{cite news | url=http://chroniclingamerica.loc.gov/lccn/sn83045396/1911-03-15/ed-1/seq-3/;words=Cecil+Grace?date1=1911&rows=20&searchType=basic&state=&date2=1911&proxtext=Cecil+grace&y=10&x=14&dateFilterType=yearRange&index=13 | newspaper=Salt Lake Tribune | date=15 March 1911 | title=Believe Body is that of Missing Aviator | accessdate=3 January 2015 }}</ref> A body resembling Grace's was found in [[Ostend]] harbour on 14 March 1911, but it was too badly disfigured to be identifiable.<ref name=NYT19110315/> <ref>[http://chroniclingamerica.loc.gov/lccn/sn83045462/1911-03-15/ed-1/seq-11/#date1=03%2F11%2F1911&index=2&rows=20&searchType=advanced&language=&sequence=0&words=CECIL+Cecil+GRACE+Grace&proxdistance=5&date2=03%2F31%2F1911&ortext=&proxtext=Cecil+Grace&phrasetext=&andtext=&dateFilterType=range&page=1  Evening star., March 15, 1911, Page 11, Image 11]</ref>

In March 1911 he was formally declared to have died.<ref name="death" /> There is a stained glass window in the south wall of All Saints' Church, [[Eastchurch]], dedicated jointly to Grace and to [[Charles Rolls]] who died the previous July.<ref>[http://www.eastchurchpc.kentparishes.gov.uk/default.cfm?pid=545 Eastchurch Parish Council]. Accessed 21 May 2010.</ref> Grace's name also appeared on a monument celebrating the earliest cross-channel flights, erected at Calais by the Aero Club de France in about July 1911.<ref>[http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200927.html ''Flight'' 1 April 1911  p.299]</ref> He was posthumously awarded the Gold Medal of the Royal Aero Club "for his achievements as a pilot and competitor".<ref>{{cite web
  | last =
  | first =
  | authorlink =
  | author2 =
  | title = Awards & Trophies: Gold Medal of the Royal Aero Club
  | work =
  | publisher =Royal Aero Club
  | year =2009
  | url =http://www.royalaeroclub.org/awardGold.htm
  | doi =
  | accessdate = 19 June 2010 }}</ref>

==References==
{{reflist|2|refs=
<ref name="missing1">
{{Cite newspaper The Times
|articlename=British Airman Missing. Mr. Grace Lost Over The Sea After A Channel Flight. 
|author=
|section=News
|day_of_week=Friday
|date=23 December 1910
|page_number=8
|page_numbers=
|issue=39463
|column=C
}}</ref>

<ref name="missing2">
{{Cite newspaper The Times
|articlename=The Disappearance of Mr. Grace. Discovery of CAP And Spectacles
|author=
|section=News
|day_of_week=Friday
|date=6 January 1911
|page_number=8
|page_numbers=
|issue=39475
|column=F
}}</ref>

<ref name="missing3">
{{Cite newspaper The Times
|articlename=The Disappearance of Mr. Grace. 
|author=
|section=News
|day_of_week=Saturday
|date=17 January 1911
|page_number=8
|page_numbers=
|issue=39476
|column=F
}}</ref>

<ref name=NYT19110315>
{{Cite news
|newspaper=The New York Times
|title=Grace's Body Found?; One Much Disfigured, Resembling Lost Aviator, Picked Up at Ostend. 
|author=
|section=News
|date=15 March 1911
|page_number= 
|page_numbers=
|issue=
|column=
}}</ref>

<ref name="death">
{{Cite newspaper The Times
|articlename=Airman's Death in the Goods of Cecil Stanley Grace. 
|author=
|section=Law
|day_of_week=Tuesday
|date=28 March 1911
|page_number=3
|page_numbers=
|issue=39544
|column=E
}}</ref>

<ref name="naturalisation">
{{Cite newspaper The Times
|articlename=News in Brief
|author=
|section=News in Brief
|day_of_week=Wednesday
|date=2 November 1910
|page_number=20
|page_numbers=
|issue=39419
|column=F
}}</ref>

}}

==External links==
* [http://earlyaviators.com/egrace.htm Cecil Grace 1880–1910]

{{DEFAULTSORT:Grace, Cecil Stanley}}
[[Category:1880 births]]
[[Category:1910 deaths]]
[[Category:British aviators]]
[[Category:Aviation pioneers]]
[[Category:Missing aviators]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]
[[Category:Chilean emigrants to England]]
[[Category:Chilean people of American descent]]
[[Category:Chilean people of Irish descent]]
[[Category:Naturalised citizens of the United Kingdom]]