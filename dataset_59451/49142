{{italic title}}
[[File:Pietro Metastasio.jpg|thumb|Pietro Metastasio]]
'''''{{Lang|it|L'impresario delle Isole Canarie}}''''' (The impresario from the [[Canary Islands]]), also known as ''L'impresario delle Canarie'' or ''Dorina e Nibbio'', is a satirical opera [[intermezzo]] [[libretto]] by [[Metastasio]] (Pietro Antonio Domenico Trapassi), written in 1724 to be performed between the acts of his [[opera seria]] ''[[Didone abbandonata]]''. The first performance of the work was on February 1, 1724 in [[Naples]], Italy, at [[Teatro San Bartolomeo]]. The composer was [[Domenico Sarro]], also known by the name ''Sarri'', who also revised the work in 1730.<ref>Don Neville, ''[[Grove Dictionary of Music and Musicians]]'', "Impresario delle Canarie, L'" (The Impresario from the Canary Islands).</ref> The role of Dorina was first sung by the [[contralto]] Santa Marchesini, and Nibbio by the [[basso buffo]] singer Gioacchino Corrado.<ref>C. E. Troy, ''The Comic Intermezzo: a Study in the History of Eighteenth-Century Italian Opera'', (Ann Arbor, 1979) ISBN 0835709922</ref><ref>Stefano Capone, ''L'opera comica Napoletana'', Liguori editore, 2007, ISBN 9788820740566</ref>

== Plot ==

Persons: Dorina, an opera singer; Nibbio, an impresario from the Canary Islands; maids, wardrobe assistants

Part I (Dorina's House) to be performed between the first and second act of the opera ''Didone abbandonata''.

Dorina waits impatiently for the visit of a foreign [[impresario]]. She vents her frustration at her maid at not finding the appropriate audition piece which is modern enough and embellishes every word with [[ornament (music)|ornament]]ation. Nibbio enters, and proves to be interested more in a conquest than in art. He tries to convince the reluctant Dorina to sing for him. Nibbio forces Dorina to listen to a self-composed [[Cantata]], proving Nibbio to be a dilettante. Dorina, feigning another appointment, escapes.
[[File:Dorina at the Semperoper.jpg|thumb|Sarro's ''Dorina e Nibbio'' at the [[Semperoper]] in Dresden with Pavol Kuban (Nibbio) and Gala El Hadidi (Dorina), Felice Venanzoni (conductor)]]
Part II (A dressing room of a theatre) to be performed between the second and third act of the opera ''Didone abbandonata''.

Dorina is trying on a costume and upbraids her sloppy wardrobe assistants. Nibbio arrives. Dorina laments the hardships of performing before a harsh audience. Nibbio convinces Dorina to sing an excerpt from ''Cleopatra''. While he applauds her performance, Nibbio is disappointed that the dramatic recitative sung by Dorina is not followed by an 'exit aria', such as the typically flashy 'butterfly' or 'ship' arias of the time. He promptly provides a live example of such an aria. Dorina hopes to get rid of Nibbio by listing preposterous demands for her contract (always leading roles of [[Prima Donna]], librettos written by friends, a permanent supply of ice cream, coffee, chocolate and at least two presents weekly). She is even more suspicious of Nibbio's intentions when he accepts all her demands readily. The intermezzo ends without Nibbio having achieved his goal.

== Reception ==

[[File:Sarro - Il sagrifizio di Jefte - title page.jpg|thumb|Sarro – ''Il sagrifizio di Jefte'' – title page]]
''L'impresario'' is Metastasio's single venture in this comic genre. Inspired possibly by Girolamo Gigli's ''La Dirindina'' (1715) and certainly by the composer [[Benedetto Marcello]]'s tirade against the failing standards of operatic art in his ''Teatro alla Moda'' (1720),<ref>Gordana Lazarevich, ''Das neapolitanische Intermezzo'', original essay, for programme book for the Stuttgart opera performances, pp. 12–40</ref><ref>Franco Vazzoler, Metastasio "alla moda": ''L'impresario delle Canarie''</ref> Metastasio takes the theme of opera performance one step further. In his letters he always complained of the sloppy and unartistic habits of singers, musicians, theatre-impresarios and others involved with opera production.<ref>Roger Savage, ''Staging an Opera: Letters from the Cesarian Poet'', Early Music, Vol. 26, No. 4, Nov. 1998, pp. 583–595</ref> In his intermezzo, Metastasio gives Dorina (representing the art as it should be) most of his biting criticism, while Nibbio gets to utter most of the foolish text (art as it is). Dorina complains about awful 'modern' composers that are only interested in ornamentation; Nibbio utters his belief that texts are not important, because an audience 'has lots to catch up on' during the performance, and could not care less about meaning.<ref>Kurt Ringger, ''Die Italienische Oper als Gegenstand von Parodie und Satire'', in: Maehder, Stenzl, Perspektiven der Opernforschung, Bd. 1, ISBN 3631419171 Zwischen Operra buffa und melodrama</ref> Metastasio's text became wildly popular and was also composed by [[Tomaso Albinoni]] in 1725,<ref>Don Neville, "Metastasio, Pietro." ''[[The New Grove Dictionary of Opera]]''. Ed. [[Stanley Sadie]]. Oxford University Press, See under: Writings/Operas/''L'impresario delle Isole Canarie''</ref><ref>Printed libretto for a performance in the San Cassiano theater, 1725, Code IT\ICCU\MUS\0002663, VE0239,VEAFC, Biblioteca della Fondazione Giorgio Cini – Venezia – VE – [fondo/collocazione] Rolandi Musica 2B RAC</ref> Chiocchetti in 1726, [[Giuseppe Maria Orlandini]], [[Leonardo Leo]] in 1741 and [[Giovanni Battista Martini]] in 1744.<ref>Piero Mioli, "Padre Martini", ''Libreria musicale Italiana'', 2006, p. 42</ref>

The intermezzo was heard in different versions all over Europe. Orlandini's version was performed in Copenhagen, Sarro's in London in 1737.<ref>Printed libretto: ''The master of the opera. An interlude. Performed by Sig. Anna Maria Faini, and Signor Antonio Lottini, at the King's Theatre in the Hay-Market. The musick is composed by Sig. Domenico Sarri'', London, printed by J. Chrichley, 1737</ref> Leonardo Leo's version was performed in Vienna (1747), Potsdam (1748).<ref>Catalogue of opera librettos printed before 1800 (volume 2), Library of Congress, p. 166</ref> ''L'impresario delle Isole Canarie'' may have been Metastasio's single venture into comic opera, but it had far reaching influence. [[Carlo Goldoni]] quotes many instances of this intermezzo in his ''Impresario from Smyrna'' as well as his ''La bella Veritá'' (1762). Ranieri de' Calzabigi, (who wrote the libretto to Gluck's ''Orfeo''), wrote ''L'opera seria'' in 1769. Giovanni Bertati's ''L'opera nuova'' (1781). Giambattista Casti's ''Prima la musica poi le parole'' (1786) and Donizetti's ''Le convenienze ed inconvenienze teatrali'' followed in 1827.<ref>Francesco Savoia, Ed., ''La Cantante e l'Impresario e altri metamelodrammi'', Costa&Nolan, 1988 ISBN 8876480722</ref>

There are currently only two versions of this intermezzo that have survived to modern day as musical manuscripts: Sarro's version, which can be found in Naples, at the Conservatorio [[San Pietro a Majella]], and Giambattista Martini's version, which is located in the library of the conservatory in Bologna.<ref>Manuscript published by Arnoldo Forni, ''L'impresario delle Canarie (rist. anast. 1744'') (Bibliotheca musica Bononiensis), ISBN 978-8827125014</ref><ref>Claudio Toscani, L'impresario delle canarie due intonazioni a confronto, Studi musicali, rivista semestriale di studi musicologici / Accademia Nazionale di Santa Cecilia, Roma, 01.2010, pp. 369–388</ref>

== Discography ==

* ''L'impresario delle Canarie'' di Domenico Sarro (Nibbio, 1992) Bongiovanni Gatti, Mingardo, Catalucci
* ''L'impresario delle Canarie'' di Giambattista Martini  (Piero Santi, 1959) Maria Luisa Gavioli, soprano (Dorina), Otello Borgonovo, baryton (Nibbio), I Commendianti in musica della Cetra (Compagnie du Théâtre musical de chambre de Villa Olmo), Piero Santi (1959)
* La Dirindina- Il Maestro di Cappella- ''L'impresario delle canarie'' Bongiovanni, B00A8N9RR6

== Modern performances ==

* Barga 1979 (Domenico Sarro)
* [[Staatsoper Stuttgart]] 1993 (Domenico Sarro)
* Teatro Leal (Canary Islands) 1989 (Domenico Sarro)
* Teatro Messina 1997 (Padre Martini)
* Bochumer Symphoniker 2004 (Domenico Sarro, Padre Martini)
* Venice Biennale, 2009 (Domenico Sarro)
* Austin, Texas, 2011 (Domenico Sarro)
* [[Semperoper]] Dresden 2012/2013 (Domenico Sarro)
* Semperoper Dresden 2014 (Padre Martini)

== References ==

{{Reflist}}

{{DEFAULTSORT:impresario delle isole canarie, L'}}
[[Category:Libretti by Metastasio]]
[[Category:Italian-language operas]]
[[Category:Opera buffa]]
[[Category:Intermezzi]]
[[Category:Operas]]
[[Category:One-act operas]]
[[Category:1724 operas]]
[[Category:Operas by Domenico Sarro]]