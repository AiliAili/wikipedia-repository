The '''NASA/IPAC Extragalactic Database''' ('''NED''') is an on-line astronomical [[database]] for  astronomers that collates and cross-correlates [[astronomy|astronomical]] information on extragalactic objects (galaxies, quasars, radio, x-ray and infrared  sources, etc.).  NED was created in the late 1980s by two Pasadena astronomers, George Helou and Barry F. Madore. NED is funded by NASA and is operated by the [[Infrared Processing and Analysis Center (IPAC)]] on the campus of the [[California Institute of Technology]], under contract with [[NASA]].

NED is built around a master list of extragalactic objects for which cross-identifications of names have been established, accurate positions and redshifts entered to the extent possible, and some basic data collected. Bibliographic references relevant to individual objects have been compiled, and abstracts of extragalactic interest are kept on line. Detailed and referenced photometry, position, and redshift data, have been taken from large compilations and from the literature.<ref>http://www.ipac.caltech.edu/project/24</ref>

NED also includes images from 2MASS, from the literature, and from the Digitized Sky Survey.

As of March 2014, NED contains 206 million distinct [[astronomical object]]s with 232 million cross-identifications across multiple [[wavelength]]s, with [[redshift]] measurements for 5 million objects, 1.9 billion [[photometry (astronomy)|photometric]] data points, 609 million diameter measurements, 71 thousand redshift-independent distances for over 15 thousand galaxies, 310 thousand detailed classifications for 230 thousand objects, and 2.6 million images, maps and external links, together with links to 65 thousand journal articles, notes and abstracts.<ref>http://ned.ipac.caltech.edu/help/whats_new.html</ref>

==See also==
*[[SIMBAD]] - a database of information on Galactic objects, maintained by the [[Centre de Données astronomiques de Strasbourg]], [[France]]
* NASA's [[Planetary Data System]] (PDS) - a database of information on solar system objects, also maintained by [[JPL]]
* NASA [[Astrophysics Data System]] (ADS)
* [[Bibcode]]

== References ==
{{Reflist}}

==External links==
*[http://nedwww.ipac.caltech.edu/ NED]

{{NASA navbox}}

{{DEFAULTSORT:NASA IPAC Extragalactic Database}}
[[Category:Astronomical databases]]
[[Category:Government databases in the United States]]