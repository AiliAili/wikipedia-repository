{{Infobox scientist
| name              = Joanne Johnson
| image             = Joanne Johnson portrait.jpg
| image_size        = 250px
| caption           = 
| birth_date        = {{Birth year and age|1977}}
| nationality       = British
| fields            = Polar [[Geology|Geologist]]
| workplaces        = [[British Antarctic Survey]]
| alma_mater = BSc [[University of Durham]]<br>PhD [[University of Cambridge]]
| awards            = BAS [[Laws Prize]],<br>[[Marie Tharp Fellowship]]
| education         =  
}}

'''Joanne Johnson''' (born 1977, [[Birmingham]], UK) is a British geologist and prominent [[Antarctic]] scientist, currently working for [[British Antarctic Survey]] in the 'Palaeoenvironments, Ice Sheets and Climate Change' team.<ref>{{cite web|last1=British Antarctic Survey|title=Joanne Johnson|url=http://www.bas.ac.uk/profile/jsj|website=British Antarctic Survey|accessdate=27 July 2016}}</ref> Johnson joined British Antarctic Survey in 2002. She is best known for her work on [[Glacial motion|glacial retreat]],<ref name=":0" /> and is the namesake of the Johnson Mesa in James Ross Island, Antarctica.<ref>{{Cite web|url=http://mapcarta.com/25605960|title=Johnson Mesa|website=Mapcarta|access-date=2016-08-22}}</ref>

==Early life and education==
Johnson decided to follow a science career after particularly enjoying studying science subjects at King Edward VI High School in Birmingham during her teenage years. In 1998, Johnson obtained a BSc. Hons. Geology (1st class) at the [[University of Durham]] ([[Hatfield College]]). She then went on to complete a PhD in 2002, supervised by  Sally A Gibson at the [[University of Cambridge]] ([[Clare College]]), with a thesis  "Magmatism of the Vitim Volcanic Field, Baikal Rift Zone, Siberia". The research consisted of using [[geochemical]] characteristics of lavas to study the composition and thickness of the [[lithosphere]] in the [[Baikal Rift Zone]] of Siberia, and to improve  understanding of the melting regime beneath the region during the [[Cenozoic]].<ref>{{cite journal|last1=Johnson, J.S., Gibson, S.A., Thompson, R.N., Nowell, G.M|title=Volcanism in the Vitim Volcanic Field, Siberia: Geochemical Evidence for a Mantle Plume Beneath the Baikal Rift Zone|journal=Journal of Petrology|date=2005|volume=46|issue=7|pages=1309–1344|doi=10.1093/petrology/egi016}}</ref>

==Career and impact==
After   her PhD, Johnson began work at British Antarctic Survey. The first project she worked on (2002–2005) was an analysis of the origin and implications of [[authigenic]] alteration minerals in [[volcaniclastic]] rocks from [[James Ross Island]].<ref>{{cite journal|last1=Johnson & Smellie|title=Zeolite compositions as proxies for eruptive palaeoenvironment|journal=Geochemistry, Geophysics, Geosystems|date=2007|volume=8|issue=3|pages=n/a|doi=10.1029/2006GC001450|bibcode=2007GGG.....8.3009J}}</ref> In 2005–2009, she worked in the QWAD (Quaternary West Antarctic Deglaciation project), within the GRADES (Glacial Retreat in Antarctica and Deglaciation of the Earth System) programme at BAS, reconstructing Quaternary thinning history of [[Pine Island Glacier]].<ref name=":0">{{Cite journal|title=Research highlights|url=http://www.nature.com/doifinder/10.1038/452130a|journal=Nature|volume=452|issue=7184|pages=130–131|doi=10.1038/452130a}}</ref> Her work showed that Pine Island Glacier thinned as rapidly 8000 years ago as it is at the present-day.<ref name=":3">{{cite journal|last1=Johnson et al.|title=Rapid thinning of Pine Island Glacier in the early Holocene|journal=Science|date=2014|volume=343|issue=6174|pages=999–1001|doi=10.1126/science.1247385|bibcode=2014Sci...343..999J|pmid=24557837}}</ref><ref>{{Cite web|url=http://www.spiegel.de/international/world/millennia-of-melting-new-research-confirms-antarctic-thaw-fears-a-540059.html|title=Millennia of Melting: New Research Confirms Antarctic Thaw Fears|date=2014|website=spiegel.de|publisher=Der Spiegel|access-date=2016-08-22}}</ref>

For  2015–2020, Johnson is working on a NERC funded project  "Reconstructing millennial-scale ice sheet change in the western [[Amundsen Sea Embayment]], Antarctica, using high-precision exposure dating", with a team from BAS, [[Imperial College]] ([[London]]), [[Durham University]], [[Columbia University]] (USA) and [[Pennsylvania State University]] (USA).<ref name=":2">{{cite web|author1=Joanne Johnson  |author2=British Antarctic Survey|title=ANiSEED – AmuNdsen Sea Embayment Exposure Dating|url=https://www.bas.ac.uk/project/reconstructing-millennial-scale-ice-sheet-change/|website=British Antarctic Survey|accessdate=19 August 2016}}</ref> She is also working on  other projects including "Exploring feedbacks between glaciation, volcanism and climate in Antarctica: studying CO<sub>2</sub> outgassing from the James Ross Island lavas using melt inclusions in olivines", "Determining Quaternary glacial history of the Lassiter Coast, Antarctica", "Comminution dating boundary conditions: A study of (<sup>234</sup>U/<sup>238</sup>U) disequilibrium along the Antarctic Peninsula", and "Antarctic Peninsula exhumation and landscape development investigated by low-temperature detrital thermochronometry".

==Awards and honours==
Johnson was awarded the British Antarctic Survey Laws Prize in 2008<ref>{{cite web|last1=British Antarctic Survey Club|title=The Laws Prize|url=http://basclub.org/about/the-laws-prize/|website=British Antarctic Survey Club|accessdate=28 July 2016}}</ref> and the Columbia University, [[Marie Tharp Fellowship]] for 2010–2011.<ref name=":1">{{cite web|last1=Joanne Johnson And Lamont-Doherty|title=Collaborating On Glacial Research|url=http://www.ldeo.columbia.edu/news-events/joanne-johnson-and-lamont-doherty-collaborating-glacial-research|website=Lamont-Doherty Earth Observatory|accessdate=27 July 2016}}</ref> The three-month fellowship allowed Johnson to collaborate with scientists at [[Lamont–Doherty Earth Observatory|Lamont-Doherty Earth Observatory]] and yielded work published in the journal, ''Science''.<ref>{{cite journal|last1=Johnson et al|title=Rapid thinning of Pine Island Glacier in the early Holocene. Science|journal=Science|date=2014|volume=343|issue=6174|pages=999–1001|doi=10.1126/science.1247385|bibcode=2014Sci...343..999J|pmid=24557837}}</ref> She has spoken about the challenges of doubling as a scietist and mother: "The hardest thing is being torn between your personal and professional ambitions…Wanting to go to conferences, but not wanting to leave your children. Having to leave work early or drop everything if you get a phone call that she's sick. You could be in the middle of a complicated thought process and you have to start again."<ref name=":1" />

In January 2007, due to the  impact of Johnson's work (which also led to a completely novel proxy for recognising past ice sheets using alteration mineral chemistry), the UK Antarctic Place-Names Committee named a feature on James Ross Island, Antarctica after her. Johnson Mesa, James Ross Island, Antarctica (63° 49'40"S, 57° 55'22"W) is a large flat-topped volcanic mountain north of [[Abernethy Flats]], between [[Crame Col]] and [[Bibby Point]] on [[Ulu Peninsula]], James Ross Island.<ref>{{Cite web|url=http://apc.antarctica.ac.uk/wp-content/uploads/sites/9/sites/9/2013/03/APC0602_minutes.pdf|title=Meeting of the Antarctic Place Names Committee|date=2006|website=apc.antarctica.ac.uk|publisher=British Antarctic Survey|access-date=}}</ref><ref>{{Cite web|url=http://www.antarcticglaciers.org/glacial-geology/glacial-landforms/glacial-landsystems-on-james-ross-island/|title=Glacial landsystems on James Ross Island|language=en-US|access-date=2016-08-22}}</ref>

==Selected works==
*Joanne Johnson – British Antarctic Survey". bas.ac.uk. N.p., 2016. Web. 25 July 2016. Available from: https://www.bas.ac.uk/profile/jsj/. 
*Joanne Johnson And Lamont-Doherty, Collaborating On Glacial Research | Lamont-Doherty Earth Observatory". ldeo.columbia.edu. N.p., 2016. Web. 25 July 2016. Available from: http://www.ldeo.columbia.edu/news-events/joanne-johnson-and-lamont-doherty-collaborating-glacial-research.
*{{cite journal | last1 = Johnson | first1 = J.S. | last2 = Bentley | first2 = M.B. | last3 = Smith | first3 = J.A. | last4 = Finkel | first4 = R.C. | last5 = Rood | first5 = D.H. | last6 = Gohl | first6 = K. | last7 = Balco | first7 = G. | last8 = Larter | first8 = R.D. | last9 = Schaefer | first9 = J.M. | year = 2014 | title = Rapid thinning of Pine Island Glacier in the early Holocene | url = | journal = Science | volume =  343| issue = 6174| pages = 999–1001 | doi = 10.1126/science.1247385 | pmid=24557837}}
*{{cite journal | last1 = Johnson | first1 = J.S. | last2 = Smellie | first2 = J.L. | year = 2007 | title = Zeolite compositions as proxies for eruptive palaeoenvironment | url = | journal = Geochemistry, Geophysics, Geosystems | volume = 8 | issue = 3| page = Q03009 | doi = 10.1029/2006GC001450 | bibcode=2007GGG.....8.3009J}}
*{{cite journal | last1 = Johnson | first1 = J.S. | last2 = Gibson | first2 = S.A. | last3 = Thompson | first3 = R.N. | last4 = Nowell | first4 = G.M. | year = 2005 | title = Volcanism in the Vitim Volcanic Field, Siberia: Geochemical Evidence for a Mantle Plume Beneath the Baikal Rift Zone | url = | journal = Journal of Petrology | volume = 46 | issue = 7| pages = 1309–1344 | doi = 10.1093/petrology/egi016 }}

==References==
{{Reflist|30em}}

{{Authority control}}

{{DEFAULTSORT:Johnson, Joanne}}
[[Category:Created via preloaddraft]]
[[Category:British women scientists]]
[[Category:British geologists]]
[[Category:1977 births]]
[[Category:People from Birmingham, West Midlands]]
[[Category:Living people]]
[[Category:Women Antarctic scientists]]
[[Category:Alumni of the University of Cambridge]]
[[Category:Alumni of Durham University]]