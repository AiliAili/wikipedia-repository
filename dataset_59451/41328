{{BLP sources|date=July 2016}}
{{infobox musical artist <!-- See Wikipedia:WikiProject Musicians -->
|name = Chubby Checker
|image = WIKI CHUBBY CHECKER 1.jpg
|caption = Chubby Checker in 2005
|image_size = <!-- Only for images narrower than 220 pixels -->
|background = solo_singer
|birth_name = Ernest Evans
|alias =
|birth_date = {{birth date and age|1941|10|3|}}
|birth_place = [[Andrews, South Carolina|Spring Gully]], [[South Carolina]], US
|origin = [[Philadelphia]], [[Pennsylvania]], US
|instrument = [[singing|Vocals]]
|genre = [[Rock and roll]], [[rhythm and blues]]
|occupation = Singer
|years_active = 1959–present
|label = [[Cameo-Parkway Records|Parkway]], [[MCA Records|MCA]]
|associated_acts =
|website = [http://chubbychecker.com/ ChubbyChecker.com]
|notable_instruments =
}}
'''Chubby Checker''' (born '''Ernest Evans'''; October 3, 1941) is an American singer. He is widely known for popularizing the [[Twist (dance)|twist dance style]], with his 1960 [[hit record|hit]] [[cover version|cover]] of [[Hank Ballard]]'s [[Rhythm and blues|R&B]] hit "[[The Twist (song)|The Twist]]". In September 2008 "The Twist" topped ''[[Billboard (magazine)|Billboard's]]'' list of the most popular [[single (music)|singles]] to have appeared in the [[Billboard Hot 100|Hot 100]] since its debut in 1958, an honor it maintained for an August 2013 update of the list.<ref>[http://www.billboard.com/articles/list/2155531/the-hot-100-all-time-top-songs]</ref> He also popularized the [[Limbo Rock]] and its trademark limbo dance, as well as various dance styles such as the fly.

Checker is the only recording artist to place five albums in the Top 12 all at once. The performer has often claimed to have personally changed the way we dance to the beat of music, as when he told ''Billboard'', "Anyplace on the planet, when someone has a song that has a beat, they're on the floor dancing apart to the beat. And before Chubby Checker, it wasn't here." [[Clay Cole]] agreed: "Chubby Checker has never been properly acknowledged for one major contribution to pop culture—Chubby and the Twist got adults out and onto the dance floor for the first time. Before the Twist dance phenomenon, grown ups did not dance to teenage music."

==Early life==
Chubby Checker was born Ernest Evans in [[Andrews, South Carolina|Spring Gully]], [[South Carolina]]. He was raised in the projects of [[South Philadelphia]], where he lived with his parents, Raymond and Mrs. Eartle Evans,<ref>UPI Telephoto (NAP 122601), December 1963)</ref> and two brothers.<ref name= Chubby>{{cite web|url=http://www.chubbychecker.com/bio.asp |title=The Official Site | website= ChubbyChecker.com |date= |accessdate=2015-08-18}}</ref> By age eight Evans formed a street-corner harmony group, and by the time he entered high school, took piano lessons at [[Settlement Music School]]. He would entertain his classmates by performing vocal impressions of popular entertainers of the day, such as [[Jerry Lee Lewis]], [[Elvis Presley]] and [[Fats Domino]].<ref>[http://www.biography.com/search/article.do?id=954233 Chubby Checker],{{dead link|date=August 2015}} ''Biography.com''</ref> One of his classmates and friends at South Philadelphia High School was Fabiano Forte, who would become a popular performer of the late 1950s and early 1960s as [[Fabian Forte]].<ref name= Chubby />

After school Evans would entertain customers at his various jobs, including Fresh Farm Poultry in the [[Italian Market, Philadelphia|Italian Market]] on Ninth Street and at the Produce Market, with songs and jokes. It was his boss at the Produce Market, Tony A., who gave Evans the nickname "Chubby". The store owner of Fresh Farm Poultry, Henry Colt, was so impressed by Ernest's performances for the customers that he, along with his colleague and friend Kal Mann, who worked as a songwriter for [[Cameo-Parkway Records]],<ref>{{cite web| author= |url=http://www.classicbands.com/ChubbyCheckerInterview.html |title=Interview With Chubby Checker |website= Classicbands.com |date=1961-02-01 |accessdate=2015-08-18}}</ref> arranged for young Chubby to do a private recording for ''[[American Bandstand]]'' host [[Dick Clark]]. It was at this recording session that Evans got his stage name from Clark's wife, who asked Evans what his name was. "Well", he replied, "my friends call me 'Chubby'". As he had just completed a Fats Domino impression, she smiled and said, "As in Checker?" That little play on words ('chubby' meaning 'fat', and '[[checkers]]', like '[[dominoes]]', being a game) got an instant laugh and stuck, and from then on, Evans would use the name Chubby Checker.<ref>{{cite web|author= |url=http://www.classicbands.com/checker.html |title= Chubby Checker |website=Classicbands.com |date=1958-11-11 |accessdate=2015-08-18}}</ref>

==Career==

===1960s===
Checker privately recorded a novelty single for Clark in which the singer portrayed a school teacher with an unruly classroom of musical performers. The premise allowed Checker to imitate such acts as [[Fats Domino]], [[The Coasters]], [[Elvis Presley]], [[Cozy Cole]], and [[Ricky Nelson]], [[Frankie Avalon]], and [[Fabian Forte]] as [[The Chipmunks]], each singing "[[Mary Had a Little Lamb]]". Clark sent the song out as his Christmas greeting, and it received such good response that Cameo-Parkway signed Checker to a recording contract. Titled "The Class", the single became Checker's first release, charting at #38 in the spring of 1959.

Checker introduced his version of "The Twist" at the age of 18 in July 1960 in [[Wildwood, New Jersey]] at the Rainbow Club, "The Twist" went on to become the only single to top the [[Billboard Hot 100|''Billboard'' Hot 100]] twice, in two separate chart runs. ([[Bing Crosby]]'s "[[White Christmas (song)|White Christmas]]" had done so on ''Billboard'''s earlier chart.)

"[[The Twist (song)|The Twist]]" had previously peaked at #16 on the ''[[Billboard (magazine)|Billboard]]'' [[rhythm and blues]] chart, in the 1959 version recorded by its author, [[Hank Ballard]], whose band The Midnighters first performed the dance on stage. Checker's "Twist", however, was a nationwide smash, aided by his many appearances on Dick Clark's [[American Bandstand]], the Top 10 American Bandstand ranking of the song, and the teenagers on the show who enjoyed dancing the Twist. The song was so ubiquitous that Checker felt that his critics thought that he could only succeed with dance records typecasting him as a dance artist. Checker later lamented:"...in a way, "The Twist" really ruined my life. I was on my way to becoming a big nightclub performer, and "The Twist" just wiped it out... It got so out of proportion. No one ever believes I have talent."{{citation needed|date=August 2015}}<ref>{{Cite book|title = The twist: The story of the song and dance that changed the world|last = Dawson|first = Jim|publisher = Faber & Faber|year = 1995|isbn = 9780571198528|location = Boston, Massachusetts|pages = 26}}</ref> By 1965 alone, "The Twist" had sold over 15 million copies, and was awarded multiple [[music recording sales certification|gold discs]] by the [[Recording Industry Association of America|RIAA]].<ref name="The Book of Golden Discs">{{cite book
| first= Joseph
| last= Murrells
| year= 1978
| title= The Book of Golden Discs
| edition= 2nd
| publisher= Barrie and Jenkins Ltd 
| location= London
| pages= 122–3
| isbn= 0-214-20512-6}}</ref>

Despite Checker's initial disapproval, he found follow-up success with a succession of up-tempo dance tracks and produced a series of successful dance-related singles, including "The Hucklebuck" (#14), "The Fly" (#7), "Dance the Mess Around" (#24), and "[[Pony Time]]", which became his second #1 single. Checker's follow-up "twist" single, "[[Let's Twist Again]]", won the 1961 Grammy Award for Best Rock and Roll Solo Vocal Performance. A 1962 duet with [[Dee Dee Sharp]], "Slow Twistin'", reached #3 on the national charts. "[[Limbo Rock]]" reached #2 in the fall of 1962, becoming Checker's last Top Ten Hit. Checker continued to have Top 40 singles until 1965, but [[British Invasion|changes in public taste]] ended his hit-making career. He spent much of the rest of the 1960s touring and recording in [[Europe]].

==1970s-1990s==
“The Twist” was recorded for [[Cameo Parkway Records]] and along with the label's other material, became unavailable after the early 1970s because of the company's internal legal disputes. For decades, almost all compilations of Checker's hits consisted of re-recordings. The 1970s saw him become a staple on the [[oldies]] circuit, including a temporary stint as a [[disco]] artist. Chubby Checker continued to be a superstar in Europe with television and records{{citation needed|date=September 2015}}. A dance-floor cover version of [[The Beatles]]' "[[Back in the U.S.S.R.]]" was released in 1969 on [[Buddah Records]], but only charted at #82. It was Checker's last chart appearance until 1982 when he hit #92 with "Running".
[[File:ChubbyCheckerByPhilKonstantin.jpg|thumb|Chubby Checker during a TV interview in 2008]]

In 1971, at Checker's insistence, he recorded a [[psychedelic music|psychedelic]] album filled with music he felt was "current" that was initially only released in [[Europe]]. Originally named ''Chequered!'', it was renamed over the years in subsequent re-releases as ''New Revelation'', ''The Other Side Of Chubby Checker'', and sometimes as ''Chubby Checker''. The songs were all written by Checker and produced by longtime [[Jimi Hendrix]] producer Ed Chaplin{{citation needed|date=September 2015}}, but the studio musicians' names are unknown. The album flopped.

==2000s and 2010s==
Checker had a single at #1 on ''Billboard's'' dance [[record chart|chart]] in July 2008 with "Knock Down the Walls". The single also made the top 30 on the Adult Contemporary chart. [[Roger Filgate]] of [[Wishbone Ash]] is featured on lead guitar. Chubby continues to perform on a regular basis.{{citation needed|date= November 2016}}

In 2009, Checker recorded a [[public service announcement]] (PSA) for the [[Social Security Administration]] to help launch a new campaign to promote recent changes in [[Medicare (United States)|Medicare]] law.<ref>{{cite web|url=http://www.socialsecurity.gov/pgm/flash/chubbychecker.htm |title=Social Security Public Service Announcement| publisher= Social Security Administration |date= |accessdate=2015-08-18}}</ref> In the PSA, Checker encourages Americans on Medicare to apply for Extra Help, "A new 'twist' in the law makes it easier than ever to save on your prescription drug plan costs."<ref name= SSA_PSA>{{cite web|accessdate=January 8, 2010|url=http://www.socialsecurity.gov/pressoffice/psa-video.html|title=Public Service Announcements for Television|website= SocialSecurity.gov| publisher= Social Security Administration}}</ref><ref name= SSA_videos>{{cite web|accessdate=January 8, 2010|url=http://www.socialsecurity.gov/pressoffice/psa-video.html|title=Social Security Videos| publisher= Social Security Administration}}</ref>

On February 25, 2013, Checker released a new single, the ballad "Changes," via [[iTunes]]; it was posted on [[YouTube]] and amassed over 100,000 views.{{citation needed|date=July 2016}} "Changes" was produced by the hill & hifi and reached 43 on the [[Mediabase]] Top 100 AC Chart and 41 on the Gospel Chart. Checker performed it on July 5, 2013, on [[NBC]]'s ''[[Today (U.S. TV program)|Today]]'' show.{{citation needed|date=July 2016}}

==Controversies==
In 2002, Chubby Checker protested outside of the [[Rock and Roll Hall of Fame]] induction ceremony, over the lack of radio airplay of his hit "[[The Twist (song)|The Twist]]" and his perception that the Hall of Fame had snubbed him. Seymour Stein, president of the Rock Hall's New York chapter and member of the nomination committee, claimed "I think that Chubby is someone who will be considered. He has in certain years."<ref>{{cite news|url=http://www.newsday.com/entertainment/music/wire/sns-ap-music-chubby-checker,0,1104288.story|archiveurl=https://web.archive.org/web/20080209131626/http://www.newsday.com/entertainment/music/wire/sns-ap-music-chubby-checker,0,1104288.story|archivedate=2008-02-09|title=Chubby Checker Stages RockHall Fame Protest|author=Mumby Moody, Nekesa|date=March 16, 2004|publisher=Newsday}}</ref>

In 2013, Checker sued [[Hewlett Packard|HP]] over a [[WebOS]] application with the same name. The application, before being pulled in September 2012, was used to unscientifically estimate penis size from shoe size.<ref>{{cite web|last=Gallagher |first=Sean |url=http://arstechnica.com/tech-policy/2013/02/hp-sued-by-chubby-checker-over-webos-penis-size-app/ |title=HP sued by Chubby Checker over webOS penis size app |publisher=Ars Technica |date=2013-02-14 |accessdate= 2015-08-18}}</ref><ref>{{cite press release| url= http://www.marketwatch.com/story/famed-attorney-willie-gary-files-half-billion-dollar-lawsuit-on-behalf-of-music-legend-chubby-checker-against-hewlett-packard-and-palm-inc-for-copyright-infringement-2013-02-12 | website= marketwatch.com | archiveurl=https://web.archive.org/web/20130602012846/http://www.marketwatch.com/story/famed-attorney-willie-gary-files-half-billion-dollar-lawsuit-on-behalf-of-music-legend-chubby-checker-against-hewlett-packard-and-palm-inc-for-copyright-infringement-2013-02-12 |archivedate=June 2, 2013 |title = Famed Attorney Willie Gary Files Half-Billion Dollar Lawsuit on behalf of Music Legend Chubby Checker against Hewlett Packard and Palm, Inc. for Copyright Infringement | agency= PR Newswire | location= Ft. Pierce, Florida| accessdate= November 20, 2016}}</ref><ref>{{cite web|url=http://www.webosnation.com/chubby-checker-lawsuit-filed-against-hp-over-endowment-size-estimator |title='Chubby Checker' lawsuit filed against HP over endowment size estimator |publisher=webOS Nation |date= |accessdate=2015-08-18}}</ref><ref>{{cite web|url=http://www.theweek.co.uk/technology/51519/chubby-checker-sues-hp-over-app-guesses-penis-size |title=Chubby Checker sues HP over app that guesses penis size |website=TheWeek.co.uk |date=2013-02-14 |accessdate=2015-08-18}}</ref> The district court said that Checker's trademark claim survived Hewlett-Packard's motion to dismiss, but his other claims were dismissed per [[Section 230 of the Communications Decency Act]].<ref>{{cite web|url=http://digitalcommons.law.scu.edu/cgi/viewcontent.cgi?article=1492&amp;amp;context=historical |title=Ernest Evans et al v. Hewlett-Packard Company et al |website= DigitalCommons.law.scu.edu |publisher= [[Santa Clara University]]| accessdate= 2015-08-18}}</ref>

==Film and musical depictions==
Chubby performed as well as appeared as a version of himself in ''[[Twist Around the Clock]]'' (1961) and ''[[Don't Knock the Twist]]'' (1962).  In both films he provided advice and crucial breaks for the protagonist.

He later appeared as himself in the 1989 ''[[Quantum Leap]]'' episode entitled "Good Morning, Peoria" where he walks into a radio station in 1959 hoping to have his demo record played on the air. The show's main character, Dr. [[Sam Beckett]] ([[Scott Bakula]]), convinces the station owner to play the song "The Twist," inadvertently teaching Chubby himself how to do The Twist.

In 2001 he again guest-starred as himself singing "The Twist" in [[Ally McBeal (season 4)|season 4]] of ''[[Ally McBeal]]''.

==Awards==
In 2008, Chubby Checker's "The Twist" was named the biggest chart hit of all time by ''Billboard'' magazine. ''Billboard'' looked at all singles that made the charts between 1958 and 2008. He was also honored by [[Settlement Music School]] as part of the school's centennial celebration and named to the Settlement 100, a list of notable people connected to the school.<ref>{{cite web|url=http://www.smsmusic.org/people/settlement_100.php?t=4/ |title=Settlement Music School : Settlement 100 |publisher=Smsmusic.org |date= |accessdate=2015-08-18}}</ref>

Checker received the prestigious Sandy Hosey Lifetime Achievement Award on November 9, 2013 from the [[Artists Music Guild]]. Checker was the host of the 2013 AMG Heritage Awards and was given the honor during the television broadcast. The award was presented to him by longtime friend [[Dee Dee Sharp]].<ref name="Checker receives Lifetime Achievement Award">{{cite web|last=Checker|first=Chubby|title=Checker receives the 2013 Sandy Hosey Lifetime Achievement Award|url=http://artistsmusicguild.com/content/2013-amg-heritage-award-recipients|publisher=[[Artists Music Guild]]|accessdate=9 December 2013}}</ref>

==Personal life==
On 12 December 1963 he, by then a millionaire at 22 years old, proposed marriage to [[Catharina Lodders]], a 21-year-old [[Dutch (ethnic group)|Dutch]] model and [[Miss World 1962]] from Haarlem, the [[Netherlands]].<ref>{{cite news|url=https://news.google.com/newspapers?id=zBMrAAAAIBAJ&sjid=Z5wFAAAAIBAJ&pg=5252,7340884&dq=catharina-lodders&hl=en|title=Chubby Checker To Wed Beauty|date=12 December 1963|work=[[Reading Eagle]]|publisher=[[United Press International|UPI]]|page=46|accessdate=12 August 2010}}</ref> Checker said he met Miss Lodders in [[Manila]] the prior January.<ref>UPI Telephoto (NAP 1404993…New York Bureau)</ref> They were married on 12 April 1964 at Temple Lutheran Church in [[Pennsauken Township, New Jersey|Pennsauken]], NJ.<ref>AP Wire Photo 1964</ref> Their first child, Bianca Johanna Evans, was born in a Philadelphia hospital on 8 December 1966.<ref>AP wire photo (jfu 5-2200-jfu-stass) 1966</ref>

Checker is the father of [[Women's National Basketball Association|WNBA]] player [[Mistie Bass]].<ref>[http://www.wnba.com/playerfile/mistie_bass/bio.html ] {{webarchive |url=https://web.archive.org/web/20150109165230/http://www.wnba.com/playerfile/mistie_bass/bio.html |date=January 9, 2015 }}</ref>

Checker is also father to musician Shan Egan, lead singer of Funk Church, a band in the Philadelphia area.<ref>[https://jumpphilly.com/2011/04/16/musictown-chubby-checkers-son-is-a-funk-master/]</ref>

==Singles discography==

{| class="wikitable" 
!rowspan="2"|Year
!rowspan="2"|Titles (A-side, B-side)<br><small>Both sides from same album except where indicated
!colspan="3"|Chart positions
!rowspan="2"|Album
|-
!width="40"|<small>[[Billboard Hot 100|US]]</small>
!width="40"|<small>[[Hot R&B/Hip-Hop Songs|US R&B]] </small>
!width="40"|<small>[[UK Singles Chart|UK]]</small>
|-
| rowspan="2"|1959
| align="left"|"The Class"<br><small>b/w "Schooldays, Oh, Schooldays" (Non-album track)
| align="center"|38
| align="center"|—
| align="center"|—
| align="left"|''Greatest Hits -- 15 Original Hits''
|-
| align="left"|"Whole Lotta Laughin'"<br><small>b/w "Samson and Delilah"
| align="center"|—
| align="center"|—
| align="center"|—
| align="left" rowspan="2"|Non-album tracks
|-
| rowspan="4"|1960
| align="left"|"Dancing Dinosaur"<br><small>b/w "Those Private Eyes"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| align="left"|"[[The Twist (song)|"The Twist"]]"<br><small>b/w "Toot" (from ''The Chubby Checker Discotheque'')
| align="center"|1
| align="center"|2
| align="center"|44
| align="left" rowspan="2"|''Twist With Chubby Checker''
|-
| align="left"|"[[The Hucklebuck]]" /
| align="center"|14
| align="center"|15
| align="center"|—
|-
| align="left"|"Whole Lotta Shakin' Goin' On"
| align="center"|42
| align="center"|—
| align="center"|—
| align="left"|''For Twisters Only''
|-
| rowspan="6"|1961
| align="left"|"[[Pony Time]]"<br><small>b/w "Oh, Susannah" (Non-album track)
| align="center"|1
| align="center"|1
| align="center"|27
| align="left"|''It's Pony Time''
|-
| align="left"|"Dance The Mess Around" /
| align="center"|24
| align="center"|—
| align="center"|—
| align="left" rowspan="2"|''Chubby Checker's Biggest Hits''
|-
| align="left"|"Good, Good Lovin'"
| align="center"|43
| align="center"|—
| align="center"|—
|-
| align="left"|"[[Let's Twist Again]]"<br><small>b/w "Everything's Gonna Be All Right" (from ''Chubby Checker'')
| align="center"|8
| align="center"|26
| align="center"|2
| align="left"|''Let's Twist Again''
|-
| align="left"|"The Fly"<br><small>b/w "That's The Way It Goes" (Non-album track)
| align="center"|7
| align="center"|11
| align="center"|—
| align="left"|''For Teen Twisters Only''
|-
| align="left"|"[[Jingle Bell Rock]]"<br><small>b/w "Jingle Bell Rock Imitations"<br>Both sides with [[Bobby Rydell]]
| align="center"|21
| align="center"|—
| align="center"|—
| align="left"|''Bobby Rydell/Chubby Checker''
|-
| rowspan="9"|1962
| align="left"|"The Twist" /<br><small>Chart reentry; the only song of the rock era<br>to reach #1 twice in two different years
| align="center"|1
| align="center"|4
| align="center"|14
| align="left" rowspan="2"|''Twist With Chubby Checker''
|-
| align="left"|"Twistin' U.S.A."
| align="center"|68
| align="center"|—
| align="center"|—
|-
| align="left"|"Slow Twistin'"<small> (With [[Dee Dee Sharp]])</small> /
| align="center"|3
| align="center"|3
| align="center"|23
| align="left"|''For Teen Twisters Only''
|-
| align="left"|"La Paloma Twist"
| align="center"|72
| align="center"|—
| align="center"|—
| align="left"|''Twistin' Round The World''
|-
| align="left"|"Teach Me To Twist"<br><small>b/w "Swingin' Together"<br>Both sides with Bobby Rydell
| align="center"|109
| align="center"|—
| align="center"|—
| align="left"|''Bobby Rydell/Chubby Checker''
|-
| align="left"|"Dancin' Party"<br><small>b/w "Gotta Get Myself Together" (Non-album track)
| align="center"|12
| align="center"|—
| align="center"|19
| align="left"|''Chubby Checker's Biggest Hits''
|-
| align="left"|"[[Limbo Rock]]" /
| align="center"|2
| align="center"|3
| align="center"|32
| align="left" rowspan="2"|''All The Hits (For Your Dancin' Party)''
|-
| align="left"|"Popeye The Hitchhiker"
| align="center"|10
| align="center"|13
| align="center"|—
|-
| align="left"|"Jingle Bell Rock"<br><small>b/w "Jingle Bell Imitations"<br>Chart reentry, both sides with Bobby Rydell
| align="center"|92
| align="center"|—
| align="center"|40
| align="left"|''Bobby Rydell/Chubby Checker''
|-
|rowspan="9"|1963
| align="left"|"Let's Limbo Some More" /
| align="center"|20
| align="center"|16
| align="center"|—
| align="left" rowspan="2"|''Let's Limbo Some More''
|-
| align="left"|"Twenty Miles"
| align="center"|15
| align="center"|15
| align="center"|—
|-
| align="left"|"Birdland" / 
| align="center"|12
| align="center"|18
| align="center"|—
| align="left"|''Beach Party''
|-
| align="left"|"Black Cloud"
| align="center"|98
| align="center"|—
| align="center"|—
| align="left"|Non-album track
|-
| align="left"|"Twist It Up" /
| align="center"|25
| align="center"|—
| align="center"|—
| align="left" rowspan="2"|''Beach Party''
|-
| align="left"|"Surf Party"
| align="center"|55
| align="center"|—
| align="center"|—
|-
| align="left"|"What Do Ya Say!"<br><small>b/w "Something To Shout About"<br>Released in UK only
| align="center"|—
| align="center"|—
| align="center"|37
| align="left"|Non-album tracks
|-
| align="left"|"Loddy Lo" /
| align="center"|12
| align="center"|4
| align="center"|—
| align="left" rowspan="3"|''Chubby's Folk Album''
|-
| align="left"|"Hooka Tooka"
| align="center"|17
| align="center"|20
| align="center"|—
|-
| rowspan="5"|1964
| align="left"|"Hey, Bobba Needle"<br><small>b/w "Spread Joy" (Non-album track)
| align="center"|23
| align="center"|5
| align="center"|—
|-
| align="left"|"Rosie" /
| align="center"|116
| align="center"|43
| align="center"|—
| align="left"|Non-album track
|-
| align="left"|"Lazy Elsie Molly"
| align="center"|40
| align="center"|7
| align="center"|—
| align="left"|''18 Golden Hits''
|-
| align="left"|"She Wants T'Swim"<br><small>b/w "You Better Believe It Baby" (Non-album track)
| align="center"|50
| align="center"|—
| align="center"|—
| align="left"|''The Chubby Checker Discotheque''
|-
| align="left"|"Lovely, Lovely (Loverly, Loverly)<br><small>b/w "The Weekend's Here"
| align="center"|70
| align="center"|—
| align="center"|—
| align="left"|Non-album tracks
|-
| rowspan="3"|1965
| align="left"|"Let's Do The Freddie"<br><small>b/w "(At The) Discotheque" (Non-album track)
| align="center"|40
| align="center"|—
| align="center"|—
| align="left"|''18 Golden Hits''
|-
| align="left"|"Everything's Wrong"<br><small>b/w "Cu Ma La Be-Stay"
| align="center"|—
| align="center"|—
| align="center"|—
| align="left" rowspan="8"|Non-album tracks
|-
| align="left"|"You Just Don't Know<small> (What To Do With Me)<br>b/w "Two Hearts"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| rowspan="3"|1966
| align="left"|"Hey You! Little Boo-Ga-Loo"<br><small>b/w "Pussy Cat"
| align="center"|76
| align="center"|—
| align="center"|—
|-
| align="left"|"Looking At Tomorrow"<br><small>b/w "You Got The Power"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| align="left"|"Karate Monkey"<br><small>b/w "Her Heart"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| 1969
| align="left"|"Back In The U.S.S.R"<br><small>b/w "Windy Cream"
| align="center"|82
| align="center"|—
| align="center"|—
|-
| 1973
| align="left"|"Reggae My Way"<br><small>b/w "Gypsy"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| 1974
| align="left"|"She's A Bad Woman"<br><small>b/w "Happiness Is A Girl Like You"
| align="center"|—
| align="center"|—
| align="center"|—
|-
| 1975
| align="left"|"Let's Twist Again" /<br>"The Twist"<br><small>Double A-side chart reentry in UK
| align="center"|—
| align="center"|—
| align="center"|5
| align="left"|A: ''Let's Twist Again''<br>B: ''Twist With Chubby Checker''
|-
| 1976
| align="left"|"The Rub"<br><small>b/w "Move It"
| align="center"|—
| align="center"|—
| align="center"|—
| align="left"|Non-album tracks
|-
| rowspan="2"|1982
| align="left"|"Running"<br><small>b/w "Is Tonight The Night" (Non-album track)
| align="center"|91
| align="center"|—
| align="center"|—
| align="left" rowspan="2"|''The Change Has Come''
|-
| align="left"|"Harder Than Diamond"<br><small>b/w "Your Love"
| align="center"|104
| align="center"|—
| align="center"|—
|-
| 1989
| align="left"|"The Twist"<small> ("Yo, Twist!" version)</small><br><small>b/w "The Twist" (Buffapella)<br>Both sides with The [[Fat Boys]]
| align="center"|16
| align="center"|40
| align="center"|2
| align="left"|Non-album tracks
|-
| 2008
| align="left"|"Knock Down The Walls"<br><small>#1 US Dance, #29 US AC
| align="center"|—
| align="center"|—
| align="center"|—
| align="left"|''All The Best -- Knock Down The Walls''<br><small>Featuring 8 different mixes
|-
| 2013
| align="left"|"Changes"<small> (Pop version)<br>c/w "Changes" (Alt version)<br>CD single</small>
| align="center"|—
| align="center"|—
| align="center"|—
| align="left"|Non-album tracks
|}

==References==
{{Reflist|colwidth=30em}}

==Bibliography==
* ''[[Joel Whitburn]]'s Top Pop Singles 1955–1990,'' Record Research Inc., P.O. Box 200, Menomonee Falls WI, 1991 (ISBN 0-89820-089-X)
* ''Joel Whitburn's Top R&B Singles 1942–1988,'' Record Research Inc., P.O. Box 200, Menomonee Falls WI, 1988 (ISBN 0-89820-069-5)

==External links==
{{Wikipedia books|Chubby Checker}}
* {{IMDb name|154830}}
* {{YouTube|kqYBSodZpSw|The Chubby Checker Show 1963}}
* [http://chubbychecker.com/ Official website] <!--also used as reference for this article-->
* [http://www.jeffosretromusic.com/chubby.html Chubby Checker – King Of The Twist – by Dr. Frank Hoffmann]
* [https://web.archive.org/web/20100111111154/http://www.ssa.gov/pgm/flash/chubbychecker.htm Chubby Checker says "There's a New 'Twist' in the Law!"] (Social Security Public Service Announcements)
* [https://web.archive.org/web/20100326171309/http://www.wnyc.org:80/shows/soundcheck/episodes/2010/03/22 Interviewed March 22, 2010 on WNYC SoundCheck with John Schaefer; Discusses this Wikipedia entry and career]

{{Chubby Checker}}
{{Authority control}}

{{DEFAULTSORT:Checker, Chubby}}
[[Category:1941 births]]
[[Category:Teen idols]]
[[Category:Living people]]
[[Category:African-American rock singers]]
[[Category:African-American male singers]]
[[Category:Singers from South Carolina]]
[[Category:Musicians from Philadelphia]]
[[Category:Grammy Award winners]]
[[Category:People from Georgetown County, South Carolina]]
[[Category:People from Williamsburg County, South Carolina]]
[[Category:Cameo Records artists]]
[[Category:Singers from Pennsylvania]]
[[Category:20th-century American singers]]
[[Category:21st-century American singers]]
[[Category:Rock and roll musicians]]