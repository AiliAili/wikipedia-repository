{{Infobox journal
| title = Progress in Electromagnetics Research
| cover =
| editor = Weng Cho Chew, Sailing He
| discipline = [[Maxwell's equations|Electromagnetic theory and applications]]
| formernames = Electromagnetic Waves
| abbreviation =
| publisher = [[EMW Publishing]]
| country =
| frequency = 
| history = 1989-present
| openaccess = Yes
| license =
| impact = 5.298
| impact-year = 2011
| website = http://www.jpier.org/PIER/
| link1 = 
| link1-name = 
| link2 =
| link2-name =
| JSTOR =
| OCLC = 63284447
| LCCN =
| CODEN =
| ISSN = 1070-4698
| ISSN2 = 1070-4698
| ISSN2label = ''Electromagnetic Waves'':
| eISSN = 1559-8985
}}
'''''Progress in Electromagnetics Research''''' is a [[Peer review|peer-reviewed]] [[open access]] [[scientific journal]] covering all aspects of [[Maxwell's equations|electromagnetic theory and applications]]. It was established in 1989 as ''Electromagnetic Waves''. The [[editors-in-chief]] are Weng Cho Chew ([[University of Hong Kong]]) and Sailing He ([[Royal Institute of Technology]]). [[Jin Au Kong]] was the founding editor-in-chief.

== Abstracting and indexing ==
The journal is abstracted and indexed by the [[Science Citation Index Expanded]], [[Current Contents]], [[Inspec]], [[Scopus]], and [[Compendex]]. It is also a member of [[CrossRef]]. According to the ''[[Journal Citation Reports]]'', the journal had a 2011 [[impact factor]] of 5.298.<ref name=WoS>{{cite book |year=2012 |chapter=Progress in Electromagnetics Research |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> However, it was not listed in 2012 because of "anomalous citation patterns resulting in a significant distortion of the Journal Impact Factor, so that the rank does not reflect the journal's citation performance in the literature".<ref>{{Cite web |title=Journal Citation Reports Notices  |work=Journal Cition Reports |publisher=Thomson Reuters  |year=2012 |url=http://admin-apps.webofknowledge.com/JCR/static_html/notices/notices.htm |archiveurl=https://web.archive.org/web/20130908231319/http://admin-apps.webofknowledge.com/JCR/static_html/notices/notices.htm |archivedate=September 8, 2013 |accessdate=September 14, 2013}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.jpier.org/PIER/}}

[[Category:Engineering journals]]
[[Category:Publications established in 1989]]
[[Category:English-language journals]]