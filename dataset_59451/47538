{{Use dmy dates|date=July 2013}}
{{Disability}}
'''Disability art''' or '''disability arts''' is any art, theatre, fine arts, film, writing, music that takes disability as its theme or whose context relates to disability.<ref name="Shape" />

==Meaning==

Disability arts is an area of art where the context of the art takes on disability as its theme. Disability art is about exploring the conceptual ideas and physical realities of what is to be disabled or concepts relating to the word.

==Context==

Disability art is different from [[Disability in the arts]] which refers more to the active participation or representation of disabled people in the arts rather than the context of the work being about disability. Disability art does not require the maker of the art to be disabled (see Disability Arts in the Disability Arts Movement for the exception) nor does art made by a disabled person automatically become disability art just because it was a disabled person that made it. 

An example of disability art by a non-disabled person: ''Alison Lapper Pregnant'', 2005, [[Marc Quinn]]<ref name="clearup" /> is disability art<ref name="Bragg" /> because of its context as he reveals the concept of the work was to make "the ultimate statement about disability"<ref name="quinn" /> 

An example of disability art by a disabled person: ''effective, defective, creative'', 2000, [[Yinka Shonibare]], shows photos of foetuses from women deemed to be at risk of delivering a defective baby,<ref name="science" /> therefore looking at the relationship of defectiveness and disability.  

An example of art made by a disabled person that is not disability art: ''Dorothea'', 1995, [[Chuck Close]];  relates to his "strict adherence to the self-imposed rules that have guided his art" and "formal analysis and methodological reconfiguration of the human face" <ref name="moma" /> therefore conceptually has nothing to do with disability therefore is not disability art.

==Context of disability art in the disability arts movement==

Disability art is a concept which was developed out of the disability arts movement.<ref name="DAC" /> In the disability arts movement disability art stood for "art made by disabled people which reflects the experience of disability."<ref name="DAO" /> To be making disability art in the disability arts movement it is conditional on being a disabled person.

==Development of the concept of disability art in Britain==

The development of disability art began in the 1970s / 80s as a result of the new political activism of the disabled peoples' movement.<ref name="DAC" /> The exact date the term came into use is currently unverified, although the first use of the term in the Disability Arts Chronology is 1986.<ref name="Chronology" /> During this period the term "disability art" in the disability arts movement has been retrospectively agreed to mean "art made by disabled people which reflects the experience of disability".<ref name="Lear" /> 

As the movement and term developed, the disability arts movement began to expand from what mainly started out as disabled people's cabaret to all art forms. The disability arts movement began to grow year on year and was at its height during the late 1990s.<ref name="Chronology" /> Key exhibitions which looked at disability art happened like ''Barriers'', which was an exhibition considering physical, sensory and intellectual limitation and its effect on personal art practice. (8 Feb - 16 Mar 2007: Aspex Gallery, Portsmouth)<ref name="Chronology" /> and the creation of the Disability Film Festival in London in 1999,<ref name="BBC" />  – both of which looked at work by disabled people as well as disability arts.  

Disabled people's politics in Britain was changed by the Disability Discrimination Act of 1995.<ref name="DDA" /> In the subsequent years as people adapted to the protection of legislation a new wave of politics entered in the disability arts movement. In 2004 the revised Disability Discrimination Act signified the end of the domination of art based on discrimination politics in the disability arts movement. A new generation of disabled people were less political<ref name=" DAC" /> and carried an agenda of integration. This combined with the carers movement highlighted a change in attitude that acknowledged the work of the disability arts movement to claim the term "disability art" but showed a movement away from the idea that only disabled people could make disability art. It began to be recognised that disability art needs to be "supported by society itself and not just by disabled people".<ref name="Williamson" />

In 2007 the London Disability Arts Forum held a debate at the [[Tate Modern]] on the motion 'Should disability and Deaf art be dead and buried in the 21st Century?' produced in response to arts cuts from the [[Arts Council]] faced by disabled-led arts organisations at the time. This debate has become significant in the way Melvin Bragg's article highlighted how disability art like Marc Quinn's sculpture Alison Lapper Pregnant raise the profile of disability in the arts.<ref name="Bragg" /> This debate and subsequent article set in motion a change for many people to recognise that the new generation of disabled people and artists did not feel it necessary to control the term disability arts but to open it out for a wider view on disability. 

Very few people are aware of disability as a topic of art,<ref name="Davis" /> mainly due to the lack of accessible and cohesive academic work and publications around the subjects of disability art and the disability arts movement. It has yet to enter into the art curriculum or establish itself as a strongly recognised concept in the arts – so development of the subject needs much more work for it to justify its place as a relevant term long term in the arts. On the other hand, in some instances, artists, curators or theorists who identify as disabled and make, curate or write about disability in their creative practices feel ambivalent about this category.

In some circles disability art is still promoted as "art made by disabled people that reflects the experience of disability." This is most notably the line taken by NDACA Co-op,<ref name=" NDACACO" /> which is predominantly made up of members who were key to the development of the disability arts movement. Although it is more commonly accepted that non-disabled people can make valid disability art, even by people that strongly align themselves with the disability arts movement.<ref name=" Gosling" />

==Development of the concept of disability arts in the United States==

VSA, (previously Very Special Arts), the international organization on arts and disability in the United States, was founded more than 35 years ago{{when?|date=December 2015}} by Ambassador [[Jean Kennedy Smith]] to provide arts and education opportunities for people with disabilities and increase access to the arts for all. With 52 international affiliates and a network of nationwide affiliates, VSA is providing arts and education programming for youth and adults with disabilities around the world.<ref name="vsa"/>

The development of disability arts in the USA is also tied to several non-profit organizations such as Creative Growth in Oakland, CA, that serves adult artists with developmental, mental and physical disabilities, providing a professional studio environment for artistic development, gallery exhibition and representation and a social atmosphere among peers.<ref name="creative growth"/> Organizations with similar mandates in the [[Bay Area]] include Creativity Explored in San Francisco, and NIAD Art Center in Richmond http://www.niadart.org. NIAD Art Center - Nurturing Independence through Artistic Development (formerly registered as National Institute of Art & Disabilities) - was established in 1982 by the late Florence Ludins-Katz and the late Elias Katz, PhD. Many other organizations with similar visions and mandates can be found across the country.

Currently, the leading scholars in disability arts in the USA include Michael Davidson, Lennard Davis, [[Rosemarie Garland-Thomson]], Ann Fox, Jessica Cooley, Joseph Grigely, Georgina Kleege, Petra Kuppers, Simi Linton, Ann Millett-Gallant, Amanda Cachia, David. T Mitchell, Carrie Sandahl, Susan Schweik, Tobin Siebers and Sharon L. Snyder, who write about a range of topics within disability arts, such as performance, literature, aesthetics, visual art, music, art history, theatre, film, dance, curatorial studies, and more. 

[[Bodies of Work: Network of Disability Arts & Culture]] (including artists and organizations) is one of the leading disability arts festivals occurring in Chicago every few years, whose art illuminates the disability experience. From the local to the international, "bodies of work" explores innovative forms of artistic expression, derived from unique bodies and minds, that explore the disability experience, advance the rights of disabled people, and widen society's understanding of what it means to be human.<ref name="bodies of work"/>

Artists who identify as disabled and make work about disability are growing in numbers, as are curators who identify as disabled and curate exhibitions on disability. Katherine Ott is a curator at the [[National Museum of American History]] at the [[Smithsonian Institution]] who has curated a number of exhibits on the history of the body, disability, ethnic and folk medicine, integrative and alternative medicine, ophthalmology, plastic surgery and dermatology, medical technology, prosthetics and rehabilitation, sexuality, visual and material culture and other ephemera.<ref name="katherine ott"/>

==Development of the concept of disability arts in other countries==

Non-profit, government-funded organizations dedicated to providing resources and support towards activities in disability art are numerous in countries like Australia and Canada. In Australia, such organizations include Arts Access Australia (peak body), Accessible Arts (NSW), DADAA (WA), Arts Access Victoria (VIC) and more.<ref name="arts access australia"/> In Canada, organizations include Arts & Disability Network Manitoba, Kickstart Arts - disability arts and culture and Tangled Art + Disability.<ref name="kickstart arts"/> These organizations work to increase opportunities and access for people with disability as artists, arts-workers, participants and audiences. They offer services to their members, such as representation and advocacy, facilitation and development, information and advice, grants and more. Many of these organizations use the [[Social model of disability]], thus they use the term ‘disability’ to refer to barriers, rather than medical conditions or impairments. They might categorize ‘people with disability’, to mean anyone with sensory or physical impairments, hidden impairments, intellectual impairments, learning difficulties or mental health conditions. These organizations recognise and value the culture and language of the Deaf community, and include them within this definition in regognition of the similar barriers that many deaf people face accessing the arts.<ref name="arts access australia"/>

==References==
{{div col|2}}
{{Reflist|refs=
<ref name="Shape">{{Cite web
 |title=Disability Arts. A Brief History 
 |publisher=Shape 
 |url=http://www.shapearts.org.uk/abriefhistory.aspx 
 |accessdate=9 February 2012 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20120222122227/http://www.shapearts.org.uk:80/abriefhistory.aspx 
 |archivedate=22 February 2012 
 |df=dmy 
}}</ref>
<ref name="DAC">{{Cite web
 |title= What is Disability Arts?
 |publisher= Disability Arts Cymru
 |url= http://www.dacymru.com/dwl/InfoWhat.html
 |accessdate=9 February 2012
}}</ref>
<ref name="DAO">{{Cite web
 |last=Allan |first=Sutherland
 |title= What is Disability Arts?
 |publisher= Disability Arts Online
 |url= http://www.disabilityartsonline.org.uk/what-is-disability-arts 
 |date=1 July 2005
 |accessdate=9 February 2012
}}</ref>
<ref name="Bragg">{{Cite web
 |last=Bragg |first=Melvyn
 |title=The last remaining avant-garde movement
 |url=https://www.theguardian.com/society/2007/dec/11/disability.arts
 |work=Society Guardian |date=11 December 2007 |accessdate=18 September 2010
}}</ref>
<ref name="quinn">{{Cite web
 |last=Marc |first=Quinn
 |title=Marc Quinn's Alison Lapper Pregnant unveiled in Trafalgar Square
 |url=http://www.london.gov.uk/media/press_releases_mayoral/marc-quinns-alison-lapper-pregnant-unveiled-trafalgar-square
 |publisher= Greater London Authority
 |work=Press Release |date=15 September 2005 |accessdate=9 February 2012
}}</ref>
<ref name="clearup">{{Cite web
 | title=This is based on an assumption that nothing is written about Marc Quinn being disabled at the point of make this work. 
}}</ref>
<ref name="science">{{Cite web
 |title=effective, defective, creative
 |url=http://www.sciencemuseum.org.uk/smap/collection_index/yinka_shonibare_effective_defective_creative.aspx
 |publisher= Science Museum 
 |accessdate=9 February 2012
}}</ref>
<ref name="moma">{{Cite web
 |last=Storr|first=Robert
 |title=Chuck Close
 |url=http://www.moma.org/interactives/exhibitions/1998/close/
 |publisher= MOMA
 |year=2008 |accessdate=9 February 2012
}}</ref>
<ref name="Chronology">{{Cite web
 |last=Allan |first=Sutherland
 |title= Disability Arts Chronology: 1977 - 2003
 |publisher= Disability Arts Online
 |url=  http://www.disabilityartsonline.org.uk/chronology_disability_arts_1976_1989
 |date=22 July 2008
 |accessdate=27 February 2012
}}</ref>
<ref name="Lear">{{Cite web
 |title= What is Disability Arts?
 |publisher= Edward Lear Foundation
 |url= http://www.learfoundation.org.uk/4555/9020.html
|accessdate=27 February 2012
}}</ref>
<ref name="Davis">{{Cite web
 |last=Lennard J. |first=Davis
 |title= The Disability Paradox: Ghettoisation of the Visual
 |publisher= Serpentine Gallery; Edited Aaron Williamson
 |url= http://www.parallellinesjournal.com/article-in-the-ghetto.html
 |year= 2011
 |accessdate=27 February 2012
}}</ref>
<ref name="NDACACO">{{Cite web
 |title= National Disability Arts Collection and Archive
 |publisher= National Disability Arts Collection and Archive Co-op
 |url=  http://www.ndaca.org/
 |accessdate=27 February 2012
}}</ref>
<ref name=" Gosling">{{Cite web
 |last=Ju |first=Gosling
 |title= What is Disability Arts?
 |publisher= Holton Lee
 |url=  http://www.ju90.co.uk/blog/what.htm 
 |year=2006
 |accessdate=27 February 2012
}}</ref>
<ref name="BBC">{{Cite web
 |last=Ollie |first=Chase
 |title=  LDAF: So much more than a charity case
 |publisher= BBC
 |url=  http://www.bbc.co.uk/london/content/articles/2008/12/12/yourstories_ldaf_video_feature.shtml
 |date=15 December 2008
 |accessdate=27 February 2012
}}</ref>
<ref name="DDA">{{Cite web
 |title= Disability Discrimination Act 1995
 |publisher= The National Archives
 |url=  http://www.legislation.gov.uk/ukpga/1995/50/contents
 |accessdate=27 February 2012
}}</ref>
<ref name="Williamson">{{Cite web
 |last=Aaron|first=Williamson
 |title= In the Ghetto? A Polemic in Place of an Editorial
 |publisher= Serpentine Gallery; Edited Aaron Williamson
 |url= http://www.parallellinesjournal.com/article-in-the-ghetto.html
 |year= 2011
 |accessdate=27 February 2012
}}</ref>
<ref name="vsa">{{Cite web
 |title= VSA The International Organization on Art and Disability
 |publisher= The Kennedy Center
 |url=http://www.kennedy-center.org/education/vsa/
 |year= 2013
 |accessdate=April 30, 2013
}}</ref>
<ref name="creative growth">{{Cite web
 |title= Creative Growth
 |url=http://creativegrowth.org/about/
 |year= 2013
 |accessdate=April 30, 2013
}}</ref>
<ref name="bodies of work">{{Cite web
 |title= Bodies of Work
 |url=http://www.bodiesofworkchicago.org/
 |year= 2013
 |accessdate=April 30, 2013
}}</ref>
<ref name="katherine ott">{{Cite web
 |title= Smithsonian Institution Profile page, National Museum of American History
 |url=http://americanhistory.si.edu/profile/475
 |year= 2013
 |accessdate=April 30, 2013
}}</ref>
<ref name="arts access australia">{{Cite web
 |title= Arts Access Australia
 |url=http://www.artsaccessaustralia.org/
 |year= 2013
 |accessdate=May 1, 2013
}}</ref>
<ref name="kickstart arts">{{Cite web
 |title= Kickstart arts
 |url=http://www.kickstart-arts.ca/
 |year= 2013
 |accessdate=May 1, 2013
}}</ref>
<ref name="arts access australia">{{Cite web
 |title= Arts Access Australia
 |url=http://www.artsaccessaustralia.org/
 |year= 2013
 |accessdate=May 1, 2013
}}</ref>}}
{{div col end}}

==External links==
* http://www.disabilityartsonline.org.uk
* http://exhibits.haverford.edu/whatcanabodydo/
* http://www.creativityexplored.org/
* http://www.blindatthemuseum.com/
* http://www.aarts.net.au/
* http://www.davidson.edu/reformations
{{Disability artists}}
[[Category:Disability in the arts|Art]]