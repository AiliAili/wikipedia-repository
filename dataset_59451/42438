{{Infobox company
| name = Brantly International
| logo = [[File:Brantly logo.png]]
| type =
| foundation = 1945
| location   = [[Coppell, Texas]]
| key_people = Cheng Shenzong <small>(President)</small>
| industry   = [[Manufacturer]]
| products   = [[Helicopters]]
| revenue          =
| operating_income =
| net_income       =
| num_employees = 20
| parent = 
| subsid =
| homepage = {{URL|http://www.brantly.com/}}
| footnotes =
}}

'''Brantly International Inc.''' is a [[helicopter]] company with its engineering and administrative offices based [[Coppell, Texas]], [[United States]]. Manufacturing of Brantly-designed helicopters is now carried out by [[Qingdao Haili Helicopters]] of China.<ref>{{cite journal|magazine=Aviation Week and Space Technology|date=14 October 2013|page=60}}</ref><ref>[http://www.qingdaohaili.com Qingdao Haili Helicopter Co.]  {{webarchive |url=https://web.archive.org/web/20120301231902/http://www.qingdaohaili.com |date=1 March 2012 }}</ref>

==History==
[[File:Flugausstellung Hermeskeil Brantly B2 - Flickr - KlausNahr.jpg|right|thumb|A Brantly B.2 on display at the [[Flugausstellung Hermeskeil]]]]

===Brantly Helicopter===
The company started out 1945 as '''Brantly Helicopter Corporation''' in [[Philadelphia]], [[Pennsylvania]], founded by [[Newby O. Brantly]].  Brantly was so impressed with the [[Sikorsky VS-300]] that he decided to design his own helicopter.<ref>''The Brantly Helicopter'' by Kristen Hynes</ref> In 1946 his first helicopter, the [[Brantly B-1]] with [[Helicopter rotor#Coaxial|coaxial]] rotors made its [[first flight]]. The B-1 was not put into production.

Using lessons learned from the B-1, he decided to build a two-seater with a simple rotor design. This helicopter, the [[Brantly B-2]], made its first flight 1953. In 1957 the company moved to [[Frederick, Oklahoma]], where the B-2 was certified in 1959. He later designed the [[Brantly 305]], a five-seater which made its first flight 1964. It was certified by the [[FAA]] in 1965, the same year it entered production.
[[File:Brantly 305 G-ASXF Kidlington 29.10.66 edited-2.jpg|right|thumb|A Brantly 305 at Kidlington Airport]]
[[Lear Jet]] acquired the Brantly Helicopter Corporation in 1966; at this time the {{convert|180000|sqft|m2|abbr=on}} factory in [[Frederick, Oklahoma]] had 100 employees.<ref>Flight International, 02 June 1966</ref> The factory moved to [[Wichita, Kansas]] in 1969. Aeronautical Research & Development Corporation (ARDC) bought all the rights to Brantly helicopters from Lear Jet in 1969, but they ended operations in early 1970.

===Brantly-Hynes===
In 1972, the rights were acquired as Brantly Operators Inc. by [[Michael K. Hynes]]. He renamed the company in 1975 as Brantly-Hynes Helicopter Inc. Later that year, the Franklin Capital Corp, headed by [[F. Lee Bailey]] who also owned [[Enstrom Helicopter Corporation]] at that time, purchased the company.<ref>Flight International, 13 November 1975</ref> Brantly-Hynes originally were just providing product support but later placed the B-2 and 305 back into production.<ref name="orbis" />

===Brantly International===
The new factory in Vernon was built 1989 by Japanese-American businessman James T. Kimura, who renamed the company as Brantly International. In 1994, the ownership was transferred to a Beijing-based company, FESCO. In 1996, they achieved an [[FAA]] production certificate. In 2007, Cheng Shenzong, referred to as the "helicopter king" in China, acquired a major interest in the company, and a [[joint venture]] between Brantly International Inc, Qingdao Wenquan International Aviation Investment Co., Ltd, and Qingdao Brantly Investment Consultation Co., Ltd. was established.<ref>http://www.wantchinatimes.com/news-subclass-cnt.aspx?id=20120717000045&cid=1206</ref><ref>{{cite web|url=http://www.businessinsider.com/the-helicopter-king-of-china-is-quietly-building-an-empire-2012-7|title=The 'Helicopter King Of China' Is Quietly Building An Empire|author=AP|date=13 July 2012|work=Business Insider|accessdate=4 February 2016}}</ref>

The factory at the Wilbarger County Airport closed at the end of 2010, and engineering and administrative offices of Brantly moved to [[Coppell, Texas]].<ref>{{cite web|url=http://www.brantly.com/|title=Brantly B-2B Helicopter - Home Page|publisher=|accessdate=4 February 2016}}</ref> Not many helicopters were sold in the last years of manufacturing in Texas. The [[Aerospace Industries Association]] (AIA) statistic for US Civil Helicopter Shipments between 1981 and 2007 showed 12 delivered B-2Bs.

Qingdao Haili Helicopters Co. Ltd. is now the only manufacturer of the B-2B helicopter.<ref>Times Record News, 21 November 2009</ref><ref>{{cite web|url=http://www.brantly.com/profile.htm|title=Brantly B-2B Helicopter - Company Profile|publisher=|accessdate=4 February 2016}}</ref><ref>[http://www.qingdaohaili.com/English/index.asp Qingdao Haili Helicopter Co.,ltd<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20120321152737/http://www.qingdaohaili.com/English/index.asp |date=21 March 2012 }}</ref>

==Products==
* 1946 - [[Brantly B-1]]
* 1953 - [[Brantly B-2]]: pre-production version.
** [[Brantly B-2|Brantly B-2A]]: basic production version.
** [[Brantly B-2|Brantly B-2B]]: improved version of the B-2, fitted with new metal rotor blades and an uprated fuel-injected 180&nbsp;hp Lycoming piston engine. This is the only version currently available.
** [[Brantly B-2|Brantly B-2J10]]: projected tandem-rotor version with longer and wider fuselage for carrying passengers and/or cargo. None built.
* 1964 – [[Brantly 305]]: an enlarged B-2.

==See also==
:{{portal|Aviation}}

==References==
{{reflist|refs=
<ref name="orbis">Orbis 1985, p. 838</ref>
}}
{{refbegin}}
*{{cite book |last= |first= |authorlink= |coauthors= |title= The Illustrated Encyclopedia of Aircraft (Part Work 1982-1985)|year= |publisher= Orbis Publishing|location= |issn=|pages=}}
{{refend}}

==External links==
{{Commons category|Brantly International}}
* [http://www.brantly.com/ Brantly International website]

{{Brantly aircraft}}
{{Use dmy dates|date=March 2011}}

[[Category:Helicopter manufacturers of the United States]]
[[Category:Companies based in Coppell, Texas]]