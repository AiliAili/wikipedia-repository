{{good article}}
{{Infobox monarch
| name               = Ælfwald
| title              = King of the East Angles  
| image              = East Anglian tally (Textus Roffensis).png
| imagesize          = 50px
| caption            = 'Alfƿald Aldƿulfing', as recorded (at the top of this list) in the ''[[Anglian collection]]''
| reign              = 713 to 749
| death_date         = 749
| predecessor        = [[Ealdwulf of East Anglia]] 
| successor          = [[Beonna of East Anglia|Beonna]] and [[Alberht of East Anglia|Alberht]] and possibly Hun
| dynasty            = [[Wuffingas]]
| father             = 
| religion           = Christian
}}

'''Ælfwald''' ([[Old English]]: ''Alfƿold'', "elf-ruler," reigned from 713 to 749) was an 8th-century king of [[Kingdom of East Anglia|East Anglia]], an [[Anglo-Saxon]] kingdom that today includes the English counties of [[Norfolk]] and [[Suffolk]]. The last king of the [[Wuffingas]] dynasty, Ælfwald succeeded his father [[Ealdwulf of East Anglia|Ealdwulf]], who had ruled for forty-nine years. Ælfwald himself ruled for thirty-six years. Their combined reigns, with barely any record of external military action or internal dynastic strife, represent a long period of peaceful stability for the East Angles. In Ælfwald's time, this was probably owing to a number of factors, including the settled nature of East Anglian ecclesiastical affairs and the prosperity brought through [[Rhineland]] commerce with the East Anglian port of Gipeswic (modern [[Ipswich]]). The coinage of Anglo-Saxon [[sceatta]]s expanded in Ælfwald's time: evidence of East Anglian mints, markets, and industry are suggested where concentrations of such coins have been discovered.

After returning from exile, [[Æthelbald of Mercia]] succeeded [[Ceolred of Mercia|Coelred]] and afterwards endowed the church at [[Crowland]]. Ælfwald's friendly stance towards Æthelbald helped to maintain peaceful relations with his more powerful neighbour. The ''Life of Guthlac'', which includes information about Æthelbald during his period of exile at Crowland, is dedicated to Ælfwald. Later versions of the ''Life'' reveal the high quality of written Old English produced in East Anglia during Ælfwald's reign. He was a literate and devoutly Christian king: his letter written to [[Saint Boniface|Boniface]] in around 747 reveals his diplomatic skills and gives a rare glimpse into the life of a ruler who is otherwise shrouded in obscurity.

== Pedigree ==

The East Anglian pedigree in the ''[[Anglian collection]]'' brings the descent down to Ælfwald, indicating that it was compiled during his reign, possibly by around 726. Showing Ælfwald as son of Ealdwulf, the pedigree continues back through Ethelric, [[Eni of East Anglia|Eni]], [[Tytila of East Anglia|Tytla]], [[Wuffa of East Anglia|Wuffa]], [[Wehha of East Anglia|Wehha]], Wilhelm, Hryp, Hrothmund, Trygil, Tyttman and Caser ([[Caesar (title)|Caesar]]) to  [[Woden]].<ref>Dumville, ''The Anglian Collection'', pp. 23-50.</ref> The ''[[Historia Brittonum]]'', which was probably compiled in the early 9th century, also has a version (the ''de ortu regum Estanglorum'') in descending order, showing: "Woden ''genuit'' ('begat') Casser, who begat Titinon, who begat Trigil, who begat Rodmunt, who begat Rippan, who begat Guillem Guechan. He first ruled in Britain over the race of East Angles. Guecha begat Guffa, who begat Tydil, who begat Ecni, who begat Edric, who begat Aldul, who begat Elric".<ref>Nennius, in Giles (ed.), ''Old English Chronicles'', [https://archive.org/stream/oldenglishchroni00gileuoft#page/412/mode/2up/search/nennius p. 412].</ref> It is not certain whether the last name, Elric, is a mistake for Ælfwald or is referring to a different individual.<ref>Royal Historical Society, ''Guides and Handbooks'', Issue 2, [https://books.google.com/books?ei=krKZTs6zFITB8QP84dnZBQ&ct=result&sqi=2&id=tCtnAAAAMAAJ&dq=Elric+mistake+%C3%86lfwald&q=Elric+ p. 20.]</ref>

== Reign ==

=== Accession ===

At Ælfwald's accession in 713,<ref>Yorke, ''Kings'', p, 63.</ref> Ceolred of [[Mercia]] had dominion over both [[Kingdom of Lindsey|Lindsey]] and [[Kingdom of Essex|Essex]].<ref>Fyrde et al, ''Handbook of British Chronology'', pp. 1-25.</ref> Ælfwald's sister Ecgburgh was abbess at [[Repton]] in [[Derbyshire]]<ref>Fryde et al, ''Handbook of British Chronology'', p. 9.</ref><ref>Brown and Farr, ''Mercia'', p. 70.</ref> and Ælfwald's upbringing was undoubtedly Christian in nature.

The following family tree shows the descendants of Eni, who was the paternal grandfather of Ælfwald. The kings of East Anglia, Kent and Mercia are coloured green, blue and red respectively:

{{familytree/start |summary=Ælfwald's family tree}}
{{familytree |border=1| | | | | | | | | | | | | | | | | | | | | | | | | | | ENI |     ENI=Eni of East Anglia}}
{{familytree | | | | | | | | | | | | | | | | |,|-|-|-|-|-|-|-|v|-|-|^|v|-|-|-|.| }}
{{familytree |border=1| | | | | | | | | | | | | | | |ANN|y|SAE| |AEE| |AED| |AEC|v|HER|RGH=Rægenhere|EOR=Eorpwald|SIG=Sigeberht |ANN=Anna|SAE=Saewara|AEE=Æthelhere|AED=Æthelwold|AEC=Æthelric|HER=Hereswith|boxstyle_ANN  =background-color: #afa;|boxstyle_AEE =background-color: #afa;|boxstyle_AED =background-color: #afa;}}
{{familytree | | | | | | | | | | | ||,|-|-|-|v|-|^|-|v|-|-|-|.| | | | | | | | | |!||}}
{{familytree |border=1| | | | | | | |EOR|v|SEA| |AET| |AEB| |JUR| | | | | | | |EAL|  SEA=Seaxburh|EOR=Eorcenberht of Kent|AET=Æthelthryth|AEB=Æthelburh|JUR=Jurmin|EAL=Ealdwulf|boxstyle_EOR=background-color: #aaf;|boxstyle_EAL =background-color: #afa; }}
{{familytree | | | | | | |,|-|-|-|^|-|-|-|v|-|-|-|v|-|-|-|.| | | | | | | | | |,|-|^|-|.||}}
{{familytree |border=1| | | | | |ERM|v|WUL| |ERC| |ECG| |HLO| | | | | | | |AEL| | |ECH|  ECG= Ecgberht|HLO= Hlothhere|ERM=Ermenilda|WUL=Wulfhere of Mercia|ERC=Ercongota|AEL=Ælfwald|ECH=Ecgburgh|boxstyle_WUL =background-color: #faa;|boxstyle_AEL =background-color: #afa; }}
{{familytree | | |,|-|-|-|-|-|^|-|.| }}
{{familytree |border=1| |CED| |COE|-|WER|  COE=Coelred of Mercia|WER= Werburh|CED=Coenred|boxstyle_COE =background-color: #faa;|boxstyle_CED =background-color: #faa; }}
{{familytree/end}}

=== Felix's'' 'Life of Guthlac' '' ===

[[File:Guthlac.jpg|thumb|125px|right|[[Saint Guthlac]]]]
Ceolred of Mercia's appropriation of monastic assets during his reign created disaffection amongst the Mercians.<ref>Colgrave, ''Life of Guthlac'', p. 5.</ref> His persecuted a distant cousin, Æthelbald, the grandson of [[Penda of Mercia|Penda]]'s brother [[Eowa of Mercia|Eowa]]. Æthelbald was driven to take refuge deep in [[the Fens]] at Crowland, where [[Saint Guthlac|Guthlac]], another descendant of the Mercian royal house, was living as a hermit.<ref name="Colgrave-6"/> When Guthlac died in 714, Ælfwald's sister Ecgburgh provided a stone coffin for his burial.<ref>Colgrave, ''Life of Guthlac'', p. 147.</ref>  Ceolred died in 716, blaspheming and insane, according to his chroniclers.<ref name="Colgrave-6">Colgrave, ''Life of Guthlac'', p. 6.</ref> Penda's line became extinct (or disempowered) and Æthelbald emerged as king of Mercia.

Æthelbald lived until 757 and carried Mercian power to a new height.<ref>Colgrave, ''Life of Guthlac'', p. 7.</ref><ref>Hunter Blair, Roman Britain, p. 168.</ref> His debt to Crowland was not forgotten: soon after his accession he richly endowed a new church on the site where Guthlac had lived as a hermit.<ref>Plunkett, ''Suffolk'', p. 144.</ref> The first ''Life of Guthlac'', written by the monk Felix, appeared soon after Guthlac's death. Nothing is known about Felix, although [[Bertram Colgrave]] has observed that he was a good scholar who evidently had access to works by Bede and [[Saint Aldhelm|Aldhelm]], to a ''Life'' of [[Saint Fursey]] and Latin works by [[Saint Jerome]], [[Saint Athanasius]] and [[Gregory the Great]]. Felix was either an East Anglian or was living in the kingdom when he wrote the book, which was written at the request of Ælfwald.<ref>Colgrave, ''Life of Saint Guthlac'', pp. 15-16.</ref>{{#tag:ref|For a detailed discussion of Felix's writing style and the works that he would have been familiar with and that would have influenced him, see the introductory chapter in ''Felix's Life of Saint Guthlac'', by Bertram Colgrave.<ref>Colgrave, ''Life of Saint Guthlac'', pp. 16-18.</ref>|group=note}} In the ''Life'', Felix portrays Æthelbald's exile at Crowland and asserts Ælfwald's right to rule in East Anglia.<ref>Colgrave, ''Felix's Life of Saint Guthlac'', p. 61.</ref> Two Old English [[Guthlac poems A and B|verse versions]] of the ''Life'' drawn on the work of Felix were written, which show the vigour of vernacular heroic and elegiac [[Mode (literature)|modes]] in Ælfwald's kingdom.

Sam Newton has proposed that the Old English heroic poem ''[[Beowulf]]'' has its origins in Ælfwald's East Anglia.<ref>Newton, ''The Origins of Beowulf'', p. 133.</ref>

=== The king's bishops ===

[[File:Williamson p16 3.svg|300px|thumb|The kingdom of East Anglia during the reign of Ælfwald]]
[[Acca of Dunwich|Æcci]] held the East Anglian see of Dommoc, following its division of in about 673, and during Ealdwulf's reign [[Ascwulf|Æscwulf]] succeeded Æcci.<ref>Fyrde et al, ''Handbook of British Chronology'', p. 216.</ref> At the [[Council of Clofeshoh]] in 716, [[Heardred of Dunwich|Heardred]] attended as Bishop of Dommoc, while [[Northbertus|Nothberht]] was present as [[Bishop of Elmham]], having succeeded [[Bedwinus|Baduwine]].<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 144.</ref>

During the 720s, [[Cuthwine of Dunwich|Cuthwine]] became bishop of Dommoc. Cuthwine was known to Bede and is known to have travelled to [[Rome]], returning with a number of [[illuminated manuscript]]s, including ''Life and Labours of Saint Paul'': his library also included [[Prosper Tiro]]'s ''Epigrammata'' and [[Coelius Sedulius|Sedulius]]' ''Carmen Pachale''.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 146.</ref> According to Bede, [[Ealdbeorht I]] was Bishop of Dommoc and [[Headulacus]] Bishop of Elmham in 731, but by 746 or 747, [[Heardred of Dunwich|Heardred (II)]] had replaced Aldberct.<ref name=HBOBC>Fryde et al, ''Handbook of British Chronology'', p. 216.</ref>

:''For more information on the episcopal sees and bishops of Dommoc and Elmham, see [[Bishop of Dunwich (ancient)|Bishop of Dunwich]] and [[Bishop of Elmham]]

=== The development of the port at Gipeswic ===

Ipswich was the first East Anglian town to be created by the Anglo-Saxons, predating other new towns such as [[Norwich]] by a century.<ref name="W-1">Wade, ''Ipswich'', p. 1,</ref> Excavation work at Ipswich has revealed that the town expanded out to become {{convert|50|ha}} in size during Ælfwald's reign,<ref>Wade, ''Ipswich'', p. 2.</ref> when it was known as Gipeswic.<ref>Allen, ''Ipswich Borough Archives'', p. xvii.</ref> It is generally considered that Gipeswic, as the trade capital of Ælfwald's kingdom, developed under the king's patronage.<ref>Yorke, ''Kings'', pp. 65-66</ref>

A rectangular grid of streets linked the earlier [[quayside]] town northwards to an ancient trackway that ran eastwards.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 149.</ref> The [[Ipswich dock#Early period|quay at Gipeswic]] also continued to develop in a form that was similar to the quayside at [[Dorestad]], south of the continental town of [[Utrecht (city)|Utrecht]], which was perhaps its principal trading partner.<ref>Russo, ''Town Origins and Development in Early England, c.400-950 A.D.'', p. 172.</ref>  Gipeswic's street grid, parts of which have survived, was subdivided into rectangular plots or ''insulae'' and new houses were built directly adjacent to metalled roads.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', pp. 149-150.</ref> The town's pottery industry, producing what has been known since the 1950s as 'Ipswich ware',<ref name="W-1"/> gained its full importance at around this time.

The former church dedication to [[Saint Mildred]] is one that can be dated to the 740s, when Mildred's relics were translated at [[Minster-in-Thanet]] by her successor abbess [[Eadburh]].<ref>Wade, ''Ipswich'', p. 3.</ref>

=== Coinage ===

The coins of Ælfwald's reign are amongst the earliest that were minted in East Anglia.<ref>Yorke, ''Kings'', p. 66.</ref> The coinage of silver pennies known as [[sceatta]]s expanded in his time and several types are attributed to East Anglian production. Most of them fall into two main groups, known as the 'Q' and 'R' series. Neither group bears a royal name or title and the authority by which they were issued cannot not established.<ref name="P-148">Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 148.</ref> The 'Q' series, which has some [[Northumbrian]] affinities, is most densely distributed in western East Anglia, along the Fen edge between [[the Wash]] and [[Cambridge]].<ref>Newman, in ''Two Decades of Discovery'', p. 18.</ref> The R series, with bust and standard, derived from earlier Kentish types, is more densely distributed in central and eastern East Anglia, including the Ipswich area.<ref name=P-148 /> According to Michael Metcalf, the 'R' series was also East Anglian, being minted at Gipeswic.<ref>Metcalf, ''Two Decades of Discovery'', p. 10.</ref>

=== Letter to Boniface ===

{{quote box
| width = 50%
| title = Ælfwald's letter to Boniface
| title_bg = BlanchedAlmond
| title_fnt = SaddleBrown
| bgcolor = Cornsilk
| align = right
| halign = left
|source = Edward Kylie, ''English Correspondence''.<ref>Kylie, ''English Correspondence'', pp. 152-153.</ref>
| quote = "To the most glorious lord, deserving of every honour and reverence. Archbishop Boniface, Ælfwald, by God's gift endowed with kingly sway over the Angles, and the whole abbey with all the brotherhood of the servants of God in our province who invoke Him, throned on high, with prayers night and day for the safety of the churches, greetings in God who rewards all.

First of all we would have thee know, beloved, how gratefully we learn that our weakness has been commended to your holy prayers; so that, whatever your benignity by the inspiration of God commanded concerning the offering of masses and the continuous prayers, we may attempt with devoted mind to fulfil. Your name will be remembered perpetually in the seven offices of our monasteries; by the number seven perfection is often designated. Wherefore, since this has been well ordered and by God's help the rules for the soul have been duly determined and the state of the inner man is provided for, the external aids of earthly substance, which by the bounty of God have been placed in our power, we wish to be at your will and command, on condition, however, that through your loving kindness you have the assistance of your prayers given to us without ceasing in the churches of God. And just as the purpose of God willed thee to become a shepherd over His people, so we long to feel in thee our patron. The names of the dead and of those who enter upon the way of all flesh, will be brought forward on both sides, as the season of the year demands, that the God of Gods and the Lord of Lords, who willed to place you in authority over bishops, may deign to bring His people through you to a knowledge of the One in Three, the Three in One.

Farewell, until you pass the happy goal.

Besides, holy father, we would have thee know that we have sent across the bearer of the present letter with a devout intention; just as we have found him faithful to you, so wilt thou find that he speaks the truth in anything relating to us."}}

A letter from Ælfwald to Boniface, the leader of the English continental mission, has survived. It was written in at some time between 742 and 749 and is one of the few surviving documents from the period that relate the ecclesiastical history of East Anglia.<ref name="Hoggett-34">Hoggett, ''The Archaeology of the East Anglian Conversion'', p. 34.</ref>

The letter, which is a response to Boniface who had requested his support, reveals Ælfwald's sound understanding of [[Latin]].<ref name="Hoggett-34"/> Ælfwald's letter reassures Boniface that his name was being remembered by the East Angles: it contains an offer to exchange the names of their dead, so that mutual prayers could be read for them. According to Richard Hoggett, a phrase in the letter,'' 'in septenis monasteriorum nostorum sinaxis', ''has been interpreted incorrectly by historians to imply that there were at the time seven monasteries in Ælfwald's kingdom in which prayers were being read, a theory which has proved difficult for scholars to explain.<ref name="Hoggett-34"/> Hoggett argues that the words in the phrase refer to the number of times that the monks offered praise during the monastic day and not to the number of monasteries then in existence. He points out that this interpretation was published by Haddan and Stubbs as long ago as 1869.<ref>Hoggett, ''East Anglian Conversion'', pp. 34-35.</ref><ref>Haddan, Stubbs, ''Ecclesiastical Documents'', pp. 152-153.</ref>

== Death ==

<!-- A living link with the age of Anna of East Anglia was finally broken in 743, when his youngest daughter [[Wihtburh]] died at her monastery of [[Dereham]], in Norfolk. --> 
Ælfwald died in 749. It is not known whether he left an immediate heir. After his death, according to mediaeval sources, East Anglia was divided between three kings,<ref>Plunkett, ''Suffolk'', p. 155.</ref> under circumstances that are not clear.<ref>Kirby, ''The Earliest English Kings'', p. 115.</ref>

== Notes ==
{{Reflist|group=note}}

== References ==
{{Reflist|2}}

== Sources ==
*{{cite book
  |editor=Colgrave, B.
  |title=Felix's Life of Guthlac
  |year=2007
  |publisher=Cambridge University Press
  |isbn=978-0-521-31386-5}}
*{{cite journal
  |last=Dumville
  |first=D. N.
  |year=1976
  |title=The Anglian Collection of Royal Genealogies and Regnal Lists
  |journal=Anglo-Saxon England
  |publisher=Cambridge Journals Online
  | doi       = 10.1017/S0263675100000764 
  |volume=5
  |pages=23–50
  |url=http://journals.cambridge.org/action/displayAbstract;jsessionid=041BDFD550FB456C2564389A9BD50299.tomcat1?fromPage=online&aid=1997004
  |accessdate=21 August 2011}}
*{{cite book
|last1=Brown
|first1=Michelle P. 
|authorlink1=Michelle P. Brown
|last2=Farr
|first2=Carol Ann
|title=Mercia: an Anglo-Saxon Kingdom in Europe
|year=2001 
|publisher=Leicester University Press
|isbn=0-8264-7765-8}}
*{{cite book
  |author1=Fryde, E. B. 
  |author2=Greenway, D. E. 
  |author3=Porter, S. 
  |author4=Roy, I. 
  |title=Handbook of British Chronology 
  |edition=3rd 
  |publisher=Cambridge University Press 
  |location=Cambridge 
  |year=1986 
  |isbn=0-521-56350-X 
  |page=}}
*{{Cite book
  | last = Hoggett
  | first = Richard
  | authorlink =
  | title = The Archaeology of the East Anglian Conversion
  | url= 
  | publisher = The Boydell Press
  | location = Woodbridge, UK
  | year = 2010
  | page = 
  | doi =
  | isbn = 978-1-84383-595-0}}
*{{cite book
  |last1=Haddan
  |first1=Arthur West
  |last2=Stubbs
  |first2=William
  |title= Councils and Ecclesiastical Documents relating to Great Britain and Ireland
  |url=https://archive.org/details/councilsecclesia03hadduoft
  |accessdate=23 August 2011
  |volume=3 
  |year=1869
  |publisher=Clarendon Press
  |location=Oxford 
  | oclc      = 1317490
  |pages=387–388}}
*{{cite book
  |last= Hunter Blair
  |first= Peter
  |title= Roman Britain and Early England: 55 B.C. - A.D. 871
  |year= 1966
  |publisher= W.W. Norton & Company
  |isbn=0-393-00361-2}}
*{{Cite book
  | last = Kirby
  | first = D. P.
  | authorlink = 
  | title = The Earliest English Kings
  | publisher = Routledge
  | location = London and New York
  | year = 2000
  | page = 
  | doi = 
  | isbn = 0-4152-4211-8
}}
*{{cite book
|last=Kylie
|first=Edward 
|title=English Correspondence, Being for the Most Part Letters Exchanged Between the Apostle of the Germans and his English Friends
|url= https://archive.org/stream/englishcorrespon00boniuoft#page/n13/mode/2up
|year=1911
|publisher=Chatto & Windus
|location=London
|pages=}}
*{{cite book|last=Metcalf|first=Michael|title=Two Decades of Discovery|year=2008|publisher=Boydell Press|location=Woodbridge|isbn=978-1-84383-371-0|url=https://books.google.com/books?id=G7p69a0Rk24C&lpg=PA58&dq=porcupine%20sceatta&pg=PA18#v=onepage&q&f=false|editor=Abramson, Tony|chapter=Sceattas: Twenty-One Years of Progress}}
*{{cite book
  | last       =Nennius
  | editor        = [[John Allen Giles|Giles, J. A.]]
  | title         = Old English Chronicles
  | trans_title   = 
  | url           = https://archive.org/details/oldenglishchroni00gileuoft
  | accessdate    = 15 October 2011
  | year      = 1906
  | origyear  =9th century
  | chapter   = Historia Brittonum
  | publisher = George Bell
  | location  = London
}}
*{{cite book|last=Newman|first=John|title=Two Decades of Discovery|year=2008|publisher=Boydell Press|location=Woodbridge|isbn=978-1-84383-371-0|url=https://books.google.com/books?id=G7p69a0Rk24C&lpg=PA58&dq=porcupine%20sceatta&pg=PA18#v=onepage&q&f=false|editor=Abramson, Tony|chapter=Sceattas in East Anglia: An Archaeological Perspective}}
*{{Cite book
  | last = Plunkett
  | first = Steven 
  | authorlink = 
  | title = Suffolk in Anglo-Saxon Times
  | publisher = Tempus
  | location = Stroud
  | year = 2005
  | page = 
  | doi = 
  | isbn = 0-7524-3139-0 }}
*{{Cite book
  | last = Russo
  | first = Daniel G.
  | authorlink =
  | title = Town Origins and Development in Early England, c.400-950 A.D.
  | publisher = Greenwood
  | location = Westport, U.S.A.
  | year = 1998
  | page =
  | doi =
  | isbn = 0-313-30079-8 }}
*{{Cite book
  | last = Wade
  | first = Keith
  | authorlink =
  | title = Ipswich from the First to the Third Millennium
  | publisher = Wolsey Press
  | location = Ipswich
  | year = 2001
  | page =
  | chapter =Gipeswic: East Anglia's first economic capital, 600-1066
  | doi =
  | isbn = 0-9507328-1-8}}
*{{Cite book
  | last = Yorke
  | first = Barbara
  | authorlink = Barbara Yorke
  | title = Kings and Kingdoms of Early Anglo-Saxon England
  | publisher = Routledge
  | location = London and New York
  | year = 2002
  | page = 
  | doi = 
  | isbn = 0-415-16639-X}}
<!-- *''[[Anglo-Saxon Chronicle]]''.
*{{Cite Bede HE}}.
*M. Anderton (editor). ''Anglo-Saxon Trading Centres: Beyond the Emporia'' (Cruithne Press, Glasgow, 1999) ISBN 978-1-873448-04-5.
*J. Hines, K. Hoyland Nielsen and F. Siegmund (editors). "The Pace of Change". ''Studies in Early Mediaeval Chronology 4'' (Oxford, 1985).
*R. Hodges. ''Dark Age Economics: The Origins of Towns and Trade AD 600-1000'' ISBN 978-0-7156-1666-6 (Gerald Duckworth & Co Ltd, London, 1989).
*D.M. Metcalf. ''Thrymsas and Sceattas in the Ashmolean Museum'' (3 volumes), [http://www.ashmolean.org/ash/publications/numis.html OUP], ISBN 1-85444-066-7; ISBN 1-85444-067-5. Oxford.
*D.M. Metcalf. "Determining the mint-attribution of East Anglian sceattas through regression analysis". ''British Numismatic Journal'' 70, 1-11 (2000).
*S. Newton. ''The Origins of Beowulf and the Pre-Viking Kingdom of East Anglia''  ISBN 0-85991-361-9 (D.S. Brewer, Cambridge 1993).
*S. J. Plunkett. "Some recent metalwork discoveries from the area of the Gipping valley". ''New Offerings, Ancient Treasures'', 61-87 (Sutton, Stroud, 2001).
*D. Whitelock, 1972. "The Pre-Viking age church in East Anglia". ''Anglo-Saxon England'' 1, 1-22 (Cambridge, 1972). -->

== External links ==
* {{PASE|522|Ælfwald 6}}
* A translation into both modern and Old English of Felix's ''Vita Sancti Guthlaci'' ''('Life of St Guthlac')'' by Charles Goodwin (1848), from the [https://archive.org/details/anglosaxonversi02goodgoog Internet Archive].

{{s-start}}
{{s-roy|en}}
{{succession box |
 before=[[Aldwulf of East Anglia|Aldwulf]]| 
 title=[[Kings of East Anglia|King of East Anglia]] |
 years= 713–749|
 after=[[Beonna of East Anglia|Beonna]], [[Alberht of East Anglia|Alberht]] and possibly Hun
}}
{{s-end}}

{{Kings of East Anglia}}

{{DEFAULTSORT:Aelfwald of East Anglia}}
[[Category:East Anglian monarchs]]
[[Category:8th-century English monarchs]]
[[Category:House of Wuffingas]]

[[de:Aelfwald]]