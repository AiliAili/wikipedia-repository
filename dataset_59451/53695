{{Cleanup|date=February 2008}}
The '''PEG ratio''' ([[P/E ratio|price/earnings]] to [[Earnings growth|growth]] ratio) is a valuation metric for determining the relative trade-off between the price of a stock, the earnings generated per share ([[Earnings per share|EPS]]), and the company's expected growth.

In general, the [[P/E ratio]] is higher for a company with a higher growth rate. Thus using just the P/E ratio would make high-growth companies appear overvalued relative to others. It is assumed that by dividing the P/E ratio by the earnings growth rate, the resulting ratio is better for comparing companies with different growth rates.<ref>{{cite web |last=Easton |first=Peter D. |title=Does The Peg Ratio Rank Stocks According To The Market's Expected Rate Of Return On Equity Capital? |date=January 2002 |ssrn=301837}}</ref>

The PEG ratio is considered to be a convenient approximation. It was originally developed by Mario Farina who wrote about it in his 1969 Book, ''A Beginner's Guide To Successful Investing In The Stock Market''.<ref>{{citation |last=Farina |first=Mario V. |title=A Beginner's Guide to Successful Investing in the Stock Market|url=https://books.google.com/books/about/A_beginner_s_guide_to_successful_investi.html?id=A7-MGwAACAAJ|year=1969}}</ref> It was later popularized by [[Peter Lynch]], who wrote in his 1989 book ''One Up on Wall Street'' that "The P/E ratio of any company that's fairly priced will equal its growth rate", i.e., a fairly valued company will have its PEG equal to 1.

==Basic formula==
<math>\mbox{PEG Ratio} \,=\,\frac{\mbox{Price/Earnings}}{\mbox{Annual EPS Growth}} </math>

The growth rate is expressed as a percent value, and should use real growth only, to correct for [[inflation]]. E.g. if a company is growing at 30% a year, in real terms, and has a [[P/E ratio|P/E]] of 30, it would have a PEG of 1.

A lower ratio is "better" (cheaper) and a higher ratio is "worse" (expensive).

The P/E ratio used in the calculation may be projected or trailing, and the annual growth rate may be the expected growth rate for the next year or the next five years.

Examples:
* [[Yahoo! Finance]] uses 5-year expected growth rate and a P/E based on the EPS estimate for the current fiscal year for calculating PEG (PEG for IBM is 1.26 on Aug 9, 2008 [https://finance.yahoo.com/q/ks?s=IBM]).
* The NASDAQ web-site uses the forecast growth rate (based on the consensus of professional analysts) and forecast earnings over the next 12 months. (PEG for IBM is 1.148 on Aug 9, 2008 [http://www.nasdaq.com/earnings/peg_ratio.asp?symbol=QCOM&selected=ibm]).

==PEG ratio as an indicator==
PEG is a widely employed indicator of a stock's possible true value. Similar to PE ratios, a lower PEG means that the stock is undervalued more. It is favored by many over the price/earnings ratio because it also accounts for growth.

The PEG ratio of 1 is sometimes said to represent a fair trade-off between the values of cost and the values of growth, indicating that a stock is reasonably valued given the expected growth. A crude analysis suggests that companies with PEG values between 0 and 1 may provide higher returns.<ref>{{citation |last=Khattab |first=Joseph |url=http://www.fool.com/investing/value/2006/04/06/how-useful-is-the-peg-ratio.aspx |title=How Useful Is the PEG Ratio? |publisher=The Motley Fool |date=April 6, 2006 |accessdate=November 21, 2010}}</ref>

A PEG Ratio can also be a negative number if a stock's present income figure is negative, (negative earnings) or if future earnings are expected to drop (negative growth).  PEG ratios calculated from negative present earnings are viewed with skepticism as almost meaningless, other than as an indication of high investment risk.<ref>[https://www.zacks.com/commentary/28731/beware-the-peg-ratio ''Beware the PEG Ratio''] Jeremy Mullin, in [[Zacks]] August 26, 2013, accessed February 25, 2017</ref>

==Criticism of the PEG ratio ==
When the PEG is quoted in public sources it makes a great deal of difference whether the earnings used in calculating the PEG is the past year's EPS, the estimated future year's EPS, or even selected analysts' speculative estimates of growth over the next five years. Use of the coming year's expected growth rate is considered preferable as the most reliable of the future-looking estimates.  Yet which growth rate was selected for calculating a particular published PEG ratio may not be clear, or may require a close reading of the footnotes for the given figure.

The PEG ratio is commonly used and provided by numerous sources of financial and stock information. Despite its wide use, the PEG ratio is only a rough rule of thumb.  [[T-Model#Relationship to other valuation models|Criticisms]] of the PEG ratio include that it is an oversimplified ratio that fails to usefully relate the price/earnings ratio to growth because it fails to factor in [[Return on equity|return on equity (ROE)]] or the required return factor (T).

The PEG ratio's validity is particularly questionable when used to compare companies expecting high growth with those expecting low-growth, or to compare companies with high P/E with those with a low P/E.  It is more apt to be considered when comparing so-called growth companies (those growing earnings significantly faster than the market).

Growth rate numbers are expected to come from an impartial source. This may be from an analyst, whose job it is to be objective, or the investor's own analysis. Management is not impartial and it is assumed that their statements have a bit of puffery, going from a bit optimistic to completely implausible. This is not always true, since some managers tend to predict modest results only to have things come out better than claimed. An investor prudent to investigate for himself whether the estimates are reasonable, and what should be used to compare the stock price.

PEG calculations based on five-year growth estimates are especially subject to over-optimistic growth projections by analysts, which on average are not achieved, and to discounting the risk of outright loss of invested capital.<ref>[https://www.zacks.com/commentary/28731/beware-the-peg-ratio ''Beware the PEG Ratio''] Jeremy Mullin, in [[Zacks]] August 26, 2013, accessed February 25, 2017</ref>

==Advantages==
Investors may prefer the PEG ratio because it explicitly puts a value on the expected growth in earnings of a company. The PEG ratio can offer a suggestion of whether a company's high [[P/E ratio]] reflects an excessively high stock price or is a reflection of promising growth prospects for the company.

==Disadvantages==
The PEG ratio is less appropriate for measuring companies without high growth. Large, well-established companies, for instance, may offer dependable [[dividend]] income, but little opportunity for growth.

A company's growth rate is an estimate. It is subject to the limitations of projecting future events. Future growth of a company can change due to any number of factors: market conditions, expansion setbacks, and hype of investors. Also, the convention that "PEG=1" is appropriate is somewhat arbitrary and considered a rule-of-thumb metric.

The simplicity and convenience of calculating PEG leaves out several important variables. First, the absolute company growth rate used in the PEG does not account for the overall growth rate of the economy, and hence an investor must compare a stock's PEG to average PEG's across its industry and the entire economy to get any accurate sense of how competitive a stock is for investment. A low (attractive) PEG in times of high growth in the entire economy may not be particularly impressive when compared to other stocks, and vice versa for high PEG's in periods of slow growth or recession.

In addition, company growth rates that are much higher than the economy's growth rate are unstable and vulnerable to any problems the company may face that would prevent it from keeping its current rate. Therefore, a higher-PEG stock with a steady, sustainable growth rate (compared to the economy's growth) can often be a more attractive investment than a low-PEG stock that may happen to just be on a short-term growth "streak". A sustained higher-than-economy growth rate over the years usually indicates a highly profitable company, but can also indicate a scam, especially if the growth is a ''flat'' percentage no matter how the rest of the economy fluctuates (as was the case for several years for returns in [[Madoff investment scandal|Bernie Madoff's Ponzi scheme]]).

Finally, the volatility of [[penny stock|highly speculative and risky stocks]], which have low price/earnings ratios due to their very low price, is also not corrected for in PEG calculations. These stocks may have low PEG's due to a very low short-term (~1 year) PE ratio (e.g. 100% growth rate from $1 to $2 /stock) that does not indicate any guarantee of maintaining future growth or even solvency.

==References==
{{Reflist}}

==External links==
* [http://www.investopedia.com/terms/p/pegratio.asp Investopedia - PEG Ratio]

{{Financial ratios}}

[[Category:Financial ratios]]