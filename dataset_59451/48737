{{infobox Organization
|name = Institute for the Study of War
|image = ISW Web OnClear small.png
|abbreviation = ISW
|motto = On the Front Lines of Military Thinking.
|formation = 2007
|type = Public Policy [[Think Tank]]
|status = U.S. [[501(c) organization|501(c)(3)]]
|headquarters = 1400 16th Street NW
|location = [[Washington, D.C.]]
|leader_title = President
|leader_name  = [[Kimberly Kagan]]
|website = [http://www.understandingwar.org www.understandingwar.org]
}}

The '''Institute for the Study of War''' ('''ISW''') is a United States-based [[think tank]] founded in 2007 by [[Kimberly Kagan]]. ISW describes itself as a non-partisan think tank providing research and analysis regarding issues of defense and [[foreign affairs]]. Others have described ISW as "a hawkish Washington" group<ref>http://swampland.time.com/2013/09/17/the-rise-and-fall-of-elizabeth-obagy/</ref> favoring an "aggressive foreign policy".<ref name="washingtonpost.com">[http://www.washingtonpost.com/world/national-security/civilian-analysts-gained-petraeuss-ear-while-he-was-commander-in-afghanistan/2012/12/18/290c0b50-446a-11e2-8061-253bccfc7532_print.html Civilian analysts gained Petraeus’s ear while he was commander in Afghanistan], By Rajiv Chandrasekaran, The [[Washington Post]], December 19, 2012</ref>   Though it had produced reports on the Afghanistan and Iraq wars, "focusing on military operations, enemy threats, and political trends in diverse conflict zones".<ref name="About Us">{{cite web | url = http://www.understandingwar.org/background| title = About Us | publisher = Institute for the Study of War | accessdate = 2010-10-12 |date=December 2010 }}</ref> The [[non-profit]] organization is supported by grants and contributions from large defense contractors,<ref name="washingtonpost.com"/> including [[Raytheon]], [[General Dynamics]], [[DynCorp]] and others.<ref>http://www.understandingwar.org/our-supporters</ref> It is headquartered in [[Washington, D.C.]]<ref name="About">{{cite web | url = http://www.rightweb.irc-online.org/profile/institute_for_the_study_of_war| title = About | publisher = Right Web | accessdate = 2011-01-31 |date=June 2010 }}</ref>

==Political stance and influence==
[[File:PetraeusISW.jpg|thumb|left|General [[David Petraeus]] speaking at an ISW event discussing progress of the [[Iraq war]], January 2010.]]
[[File:GEN RaymondOdierno.jpg|thumb|ISW hosts General [[Raymond Odierno]] to discuss the future of Iraq and the [[2010 Iraqi elections]], February 2010]]

The Institute for the Study of War and its President, [[Kimberly Kagan]], were some of the first and strongest supporters of the controversial '[[The surge|surge]]' strategy in Iraq. On May 25, 2010 Kagan participated in a briefing on [[Capitol Hill]] focusing on Iraq's political crisis that included remarks from Iraq's Ambassador [[Samir Sumaidaie]] and [[Kenneth Pollack]], Senior Fellow at the [[Brookings Institution]].<ref name=" Iraq’s Political Crisis with Kimberly Kagan and Samir Sumaidaie ">{{cite web |url=http://www.understandingwar.org/press-media/event/event-iraqs-political-crisis |title=Iraq’s Political Crisis with Kimberly Kagan and Samir Sumaidaie |publisher=Institute for the Study of War |accessdate=2010-11-12 |date=May 25, 2010}}</ref> Kagan also participated in a Brookings Institution event entitled "Prospects for Afghanistan's Future: Assessing the Outcome of the Afghan Presidential Election" alongside Michael O'Hanlon.<ref name=" Prospects for Afghanistan's Future: Assessing the Outcome of the Afghan Presidential Election">{{cite web |url=http://www.brookings.edu/events/2009/0825_afghanistan_election.aspx |title=Prospects for Afghanistan's Future: Assessing the Outcome of the Afghan Presidential Election |publisher=Brookings Institution |accessdate=2010-11-11 |date=August 25, 2009}}</ref> Kagan also helped produce the documentary [http://www.understandingwar.org/press-media/event/premier-event-surge-untold-story-never-seen-interviews The Surge: the Untold Story] with ISW Chairman, U.S Army General [[Jack Keane]] (ret.) and LTG James Dubik (ret.) describing the battle of Iraq and how the United States won the war.<ref name="Events">{{cite web |url=http://www.understandingwar.org/press-media/event/premier-event-surge-untold-story-never-seen-interviews |title="The Surge: the Untold Story" (never-before-seen interviews) |publisher=Institute for the Study of War |accessdate=2010-10-12 |date=November 9, 2010}}</ref>

ISW President Kagan has conducted eight battlefield circulations of Iraq since starting ISW for the MNF-I Commanding General, three of which were in [[Afghanistan]] for CENTCOM [[United States Central Command]] and ISAF [[International Security Assistance Force]]. She participated formally on the Joint Campaign Plan Assessment Team for Multi-National Force – Iraq  U.S. Mission – Iraq in October 2008, and as part of the Civilian Advisory Team for the CENTCOM strategic review in January 2009.<ref name="Staff Bios">{{cite web |url=http://www.understandingwar.org/press-media/staff-bios |title=Staff Bios |publisher=Institute for the Study of War |accessdate=2010-11-12 |date=November 2010}}</ref> Kagan served in Kabul as a member of General [[Stanley McChrystal]]'s strategic assessment team, composed of civilian experts, during his strategic review in June and July 2009. She returned to Afghanistan in the summer of 2010 to assist General [[David Petraeus]] with key transition tasks following his assumption of command in Afghanistan. Kagan also serves on the Academic Advisory Board at the Afghanistan- [[Pakistan]] Center of Excellence at CENTCOM.

==Research==
ISW research is divided into three main categories: the Iraq Project, the Afghanistan Project, and the Middle East Security Project.

===Afghanistan Project===
The ISW's Afghanistan Project monitors and analyzes the effectiveness of Afghan and Coalition operations to disrupt enemy networks and secure the population, while also evaluating the results of Afghanistan’s 2010 Presidential election.<ref name="Afghanistan Project">{{cite web |url=http://www.understandingwar.org/afghanistan-project|title=Afghanistan Project |publisher=Institute for the Study of War |accessdate=2011-01-12 |date=November 2010}}</ref>

The Afghanistan Project remains focused on the main enemy groups in Afghanistan, specifically: the [[Quetta Shura]] Taliban, the [[Haqqani network]], and [[Hizb-i Islami Gulbuddin]].<ref name="Afghanistan Project"/> Specific attention is paid to understanding the ethnic, tribal, and political dynamics within these areas and how these factors are manipulated by the enemy and misunderstood by the Coalition.

In 2010, ISW researchers testified before the [[United States Congress]] in regards to understanding the problems of corruption and use of local powerbrokers in ISAF’s Afghanistan strategy.<ref name="Military Supply Chain in Afghanistan">{{cite web |url=http://www.c-spanvideo.org/program/294188-2|title=Testimony |publisher=CSPAN |accessdate=2010-10-12 |date=June 22, 2010}}</ref>

===Iraq Project===
[[File:Kimberly Kagan meeting Iraqi National Police commander.JPG|thumb|Kimberly Kagan in Iraq, 2008]]
The Iraq Project at the ISW produces fully documented reports that monitor and analyze the changing security and political dynamics within Iraq.

====The "Surge"====
Institute for the Study of War President Kagan is noted for her support of [[the Surge]] strategy in Iraq and has argued for a restructured American military strategy more generally. ''[http://www.understandingwar.org/press-media/event/premier-event-surge-untold-story-never-seen-interviews The Surge: the Untold Story]'', co-produced by ISW provides a historical account of U.S. military operations in Iraq during the Surge of forces during 2007 and 2008. As a [[documentary]], it offers audiences a look into the story of the Surge in Iraq, as told by [[U.S. military]] commanders and diplomats as well as Iraqis.

The video documents the Iraq Surge as part of a population-centric [[counterinsurgency]] approach and features many of the top commanders and others responsible for its implementation—including Gen. Jack Keane (Ret.), Gen. [[David Petraeus]], Amb. [[Ryan Crocker]], Gen. [[Raymond Odierno]], Gen. Nasier Abadi (Iraq), Col. [[Peter Mansoor]] (Ret.), Col. J.B. Burton, Col. Ricky Gibbs, Col. Bryan Roberts, Col. [[Sean MacFarland]], Col. James Hickey, Col. David Sutherland, Col. Steven Townsend, Lt.-Col. James Crider, and Lt. James Danly (Ret.) <ref name="Small Wars Journal">{{cite web |url=http://smallwarsjournal.com/blog/2009/11/the-surge-the-untold-story/ |title=The Surge: The Untold Story |publisher=Small Wars Journal |accessdate=2010-01-12 |date=November 2009}}</ref>

''The Surge: The Untold Story'' was nominated for several awards and in 2010 was a winner of a Special Jury Award at the WorldFest film festival in [[Houston]].<ref name="World Fest">{{cite web|url=http://www.worldfest.org/PAGES/winners.htm |title=World Fest |publisher=WorldFest |accessdate=2009-01-12 |date=June 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20070225115818/http://www.worldfest.org/PAGES/winners.htm |archivedate=2007-02-25 |df= }}</ref> It also won honors as the best documentary part of the [[Military Channel]]'s Documentary Series at the GI Film Festival in [[Washington, D.C.]]<ref name="GI Film Festival">{{cite web |url=http://www.gifilmfestival.com/News/GIFF-2010-Announces-Winning-Films.htm|title=GI Film Festival |publisher=GI Film Fest |accessdate=2010-11-12 |date=May 2010}}</ref>

====Iraq in 2011====
Since the end of military operations in Iraq and after a general withdrawal of US forces there, ISW now focuses its research on the security and political dynamic now taking place there. ISW takes the view point that both Iraqi and U.S. military personnel believe that the Iraqi Security Forces will need additional training beyond 2011, but the mechanism for security this continued partnership is still uncertain.

===Middle East Security Project===
The Institute for the Study of War launched its Middle East Security Project in November 2011. The project seeks: to study the national security challenges and opportunities emerging from the Persian Gulf and wider Arab World; to identify ways the United States and Gulf States can check Iran’s growing influence and contain the threat posed by its nuclear ambitions; to explain the shifting balance of power within the Middle East caused by recent upheaval, and assess the responses of the United States and Arab States to address these changes as they emerge. The Project currently is focused on Syria and Iran and also produced a series of reports during the Libyan Revolution.

====Syria====
ISW has chronicled the resistance to President [[Bashar al-Assad]] through a number of reports including:
* [http://understandingwar.org/report/struggle-syria-2011 The Struggle for Syria in 2011]
* [http://understandingwar.org/report/syrias-armed-opposition Syria's Armed Opposition]
* [http://understandingwar.org/report/syrias-political-opposition Syria's Political Opposition] 
* [http://understandingwar.org/report/syrias-maturing-insurgency Syria's Maturing Insurgency]

Notable among these reports, ''Syria's Political Opposition'' was authored by disgraced former Institute for the Study of War staff member [[Elizabeth O'Bagy]].

====Libya====
ISW released four reports on the conflict that overthrew [[Muammar Gaddafi]] between September 19, 2011 and December 6, 2011. The series was entitled [http://understandingwar.org/publications?type%5B%5D=report&tid%5B%5D=292&field_lastname_value=&sort_by=created&sort_order=DESC "The Libyan Revolution"] with each of the four parts focused on different stages in the struggle in order to chronicle the revolution from start to finish.

====Iran====
The Middle East Security Project has released reports on the status of the Iranian military as well as the influence that Iran has on its neighbors in the region. These reports include [http://understandingwar.org/report/irans-two-navies "Iran's Two Navies"] and [http://understandingwar.org/report/iranian-influence-levant-egypt-iraq-and-afghanistan "Iranian Influence in the Levant, Egypt, Iraq, and Afghanistan"] which was co-written with the [[American Enterprise Institute]].

==References==
{{reflist}}

==External links==
*{{official website|http://www.understandingwar.org}}

{{DEFAULTSORT:Institute For The Study Of War}}
[[Category:Foreign policy and strategy think tanks in the United States]]
[[Category:Organizations based in Washington, D.C.]]