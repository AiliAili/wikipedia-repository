{{Infobox journal
| cover        = [[File:In Vitro Cellular & Developmental Biology – Plant.jpg]]
| editor       = Dwight Tomes & Nigel Taylor
| discipline   = [[Life Sciences]] & [[Plant Sciences]]
| abbreviation =
| publisher    = [[Springer Science+Business Media|Springer]]
| country      = [[United States]]
| frequency    = Bimonthly
| history      = 1965-present
| openaccess   =
| impact       = 0.853
| impact-year  = 2009
| website      = http://www.springer.com/journal/11627/
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| RSS          =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1054-5476
| eISSN        =
}}
{{italic title}}
'''''In Vitro Cellular & Developmental Biology – Plant''''' is a [[scientific journal]] devoted to in vitro biology in plants. Its original research and reviews cover developments and research in plant cell and tissue culture and biotechnology from around the world. Its animal counterpart is ''[[In Vitro Cellular & Developmental Biology – Animal]]''.

== Societies ==
''In Vitro Cellular & Developmental Biology - Plant'' is published on behalf of the [[Society for In Vitro Biology]] (SIVB) and in association with the [[International Association for Plant Biotechnology]] (IAPB).

===Society for In Vitro Biology===
The Society for In Vitro Biology was founded in 1946 as the Tissue Culture Association to foster the exchange of knowledge of [[in vitro]] biology of cells, tissues, and organs from both plants and animals (including humans). The focus is on biological research, development, and applications of significance to science and society. The mission is accomplished through the Society's publications, national and local conferences, meetings and workshops, and through the support of teaching initiatives in cooperation with [[educational institutions]].

===International Association for Plant Biotechnology===
The International Association of Plant Biotechnology (IAPB) has nearly 2,000 members spanning 85 countries and, as such, is the largest society of its kind.{{Citation needed|date=January 2010}}

== Subjects covered ==
''In Vitro Cellular & Developmental Biology - Plant'' covers cellular, molecular, and developmental biology research using in vitro grown or maintained organs, tissues, or cells derived from plants. Two special IAPTC&B issues deal with plant tissue culture, and molecular and cellular aspects of plant biotechnology. The IAPTC&B and SIVB maintain completely separate and independent International Editorial Review boards for their issues.

Topics covered include:

*[[Biotechnology]]/genetic transformation
*[[Developmental biology]]/ morphogenesis
*Micropropagation
*Functional [[genomics]]
*Molecular farming
*Metabolic engineering
*[[Plant physiology]]
*[[Cell biology]]
*Somatic cell genetics
*Secondary [[metabolism]]

== External links ==
*[http://www.sivb.org/ Society for In Vitro Biology]
*[http://www.iapb-stl.org International Association of Plant Biotechnology]

{{DEFAULTSORT:In Vitro Cellular and Developmental Biology - Plant}}
[[Category:English-language journals]]
[[Category:Publications established in 1965]]
[[Category:Molecular and cellular biology journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Bimonthly journals]]