{{Refimprove|date=May 2010}}
{{Infobox military person
| name          = Sir Peter Vanneck
| image         =
| caption       =
| birth_name          = Peter Beckford Rutgers Vanneck
| birth_date          = {{Birth date|df=yes|1922|01|07}}
| death_date          = {{Death date and age|df=yes|1999|08|02|1922|01|07}}
| placeofburial_label =
| placeofburial =
| birth_place  = [[London]]
| death_place  = London
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = {{UK}}
| branch        = [[Royal Navy]], [[Royal Air Force Volunteer Reserve]], [[Royal Auxiliary Air Force]]
| serviceyears  = 1940-1949 (RN)<br /> 1949-1950 (RAFVR)<br /> 1950-after 1967 (RAuxAF)
| rank          = [[Air Commodore]]<ref name="LG 10 December 1974" /> 
| servicenumber = 205378 (RAuxAF)
| unit          =
| commands      =
| battles       = '''[[World War II]]'''<br>- [[German battleship Bismarck|Bismarck]]
| battles_label =
| awards        = [[Knight Grand Cross of the Order of the British Empire]] (1977)<br>[[Companion of the Order of the Bath]] (1973)<br>[[Air Force Cross (United Kingdom)|Air Force Cross]] (1955)<br>[[Air Efficiency Award]] (1954)<br>[[Venerable Order of Saint John|Knight of Justice of the Order of St John]] (1959)<br>[[Legion of Honour|Commander, Legion of Honour]] (France) (1981)<br>[[Order of the Crown (Belgium)|Grand Officer, Order of the Crown]] (Belgium) (1983)
| relations     = [[William Vanneck, 5th Baron Huntingfield|Lord Huntingfield]] (father)<br>[[Dennis Stevenson, Baron Stevenson of Coddenham|Lord (Dennis) Stevenson]] (son-in-law)
| laterwork     =
}}

[[Air Commodore]] '''Sir Peter Beckford Rutgers Vanneck''' {{postnominals|country=GBR|GBE|CB|AFC|AE|DL}} (7 January 1922 &ndash; 2 August 1999) was a British [[Royal Navy]] officer, [[Fighter aircraft|fighter]] pilot, engineer, stockbroker and politician. He made notable contributions to [[Anglo-French relations]] as [[Lord Mayor of London]] and as a [[Member of the European Parliament]].

==Early life==
Vanneck was born on 7 January 1922 in London,<ref name="UH entry">{{cite web|title=Royal Navy (RN) Officers 1939-1945|url=http://www.unithistories.com/officers/RN_officersV.html|publisher=UnitHistories.com|accessdate=10 July 2012}}</ref> the youngest son of [[William Vanneck, 5th Baron Huntingfield|Lord Huntingfield]] and American born Margaret Eleanor Crosby.<ref name="UH entry" />

He spent his early years in Australia during his father's tenure as [[Governor of Victoria]] in the 1930s. He attended [[Geelong Grammar School]] and was sent back to Britain to study at [[Stowe School]], having won a [[scholarship]].<ref name="UH entry" />

==War service and Royal Navy career==
Vanneck joined the [[Royal Navy]] during World War II. He studied at the [[Britannia Royal Naval College|Royal Naval College]] as an [[Officer Cadet|officer cadet]] from 1 January 1940 to 1 September 1940, when he passed out as a [[Midshipman#Royal Navy|midshipman]]. He served on {{HMS|King George V|41|6}} during the operation to sink the ''[[German battleship Bismarck|Bismarck]]'', and on [[HMS Eskimo (F75)|HMS ''Eskimo'']]. He commanded a [[Landing Craft Assault|LCA]] during service off [[North Africa]]n coast.<ref name="UH entry" />

Having attended a promotion course in [[Portsmouth]] from May to August 1941,<ref name="UH entry" /> he was promoted to [[sub lieutenant]] on 10 October 1942 with seniority from 1 August 1941.<ref name="LG 16 October 1942">{{London Gazette |issue=35748 |date=16 October 1942 |startpage=4499 |endpage= |supp= |accessdate=2012-07-10}}</ref> He joined the crew of the [[sloop]] [[HMS Wren|HMS ''Wren'']] in December 1942.<ref name="UH entry" /> He was promoted to [[Lieutenant (navy)|lieutenant]] on 25 May 1943 with seniority from 1 December 1942.<ref name="LG 28 May 1943">{{London Gazette |issue=36032 |date=28 May 1943 |startpage=2386 |endpage= |supp= |accessdate=2012-07-10}}</ref> HMS ''Wren'' was part of the [[2nd Escort Group (Royal Navy)|2nd Escort Group]] under the command of Captain [[Frederic John Walker|Johnnie Walker]], the most successful [[Anti-submarine warfare|anti-submarine]] unit of World War II. He commanded a [[Motor Torpedo Boat]] from August 1944 to the end of the war.<ref name="UH entry" />

After the war, he trained as a pilot. On 30 September 1945, he transferred to [[771 Naval Air Squadron]] of the [[Fleet Air Arm]]. The squadron was based at [[RNAS Yeovilton (HMS Heron)|RNAS Yeovilton]]. He transferred to [[807 Naval Air Squadron]] on 18 August 1947. He retired from the Royal Navy on 24 May 1949, when he resigned his commission.<ref name="UH entry" />

==University and Air Force service==
After leaving the Royal Navy in 1949, Vanneck [[Matriculation#United Kingdom|matriculated]] at [[Trinity College, Cambridge]].<ref name="UH entry" /> He joined the [[Cambridge University Air Squadron]] to further pursue his interest in flying, and was commissioned into the [[Royal Air Force Volunteer Reserve]] as a [[flying officer]] on 17 November 1949.<ref name="LG 25 April 1950">{{London Gazette |issue=38892 |date=25 April 1950 |startpage=2027 |endpage= |supp=yes |accessdate=2012-07-11}}</ref>

He transferred to [[No. 601 Squadron RAF|No. 601 (County of London) Squadron]] of the [[Royal Auxiliary Air Force]] in 1950<ref name="UH entry" /> and was commissioned into the RAuxAF as a flying officer on 18 December 1950 with seniority from 9 October 1950.<ref name="LG 20 April 1951">{{London Gazette |issue=39209 |date=20 April 1951 |startpage=2314 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He was promoted to [[flight lieutenant]] on 16 July 1951 with seniority from 9 October 1950.<ref name="LG 31 August 1951">{{London Gazette |issue=39325 |date=31 August 1951 |startpage=4668 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He graduated from the University of Cambridge with a [[Master of Arts (Oxbridge and Dublin)|Master of Arts]] in engineering.<ref name="UH entry" /> He studied at [[Harvard University]], US in 1953. While there, he was seconded to 101 Squadron [[Massachusetts Air National Guard]], flying the [[P-51 Mustang]].<ref name="UH entry" />

In 1958, he transferred to No. 3619 (County of Suffolk) Fighter Control Unit<ref name="UH entry" /> based at [[Ipswich Airport|RAF Nacton]]<ref>{{cite web|title=RAF Ipswich aerodrome|url=http://www.controltowers.co.uk/H-K/Ipswich.htm|publisher=Control Towers.co.uk|accessdate=11 July 2012|author=Geoff Foster}}</ref> He was promoted to [[squadron leader]] on 1 July 1958.<ref name="LG 15 April 1960">{{London Gazette |issue=42010 |date=15 April 1960 |startpage=2786 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He commanded the unit from 28 June 1959<ref name="LG 3 July 1959">{{London Gazette |issue=41758 |date=3 July 1959 |startpage=4351 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> to 1 February 1961.<ref name="UH entry" /><ref name="LG 13 June 1961">{{London Gazette |issue=42383 |date=13 June 1961 |startpage=4374 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He then moved No 1 (County of Hertford) Maritime Headquarters Unit based at [[Northwood Headquarters|RAF Northwood]].<ref name="UH entry" /> On 1 July 1962, he was promoted to [[group captain]] and appointed Inspector of the Royal Auxiliary Air Force.<ref name="LG 29 June 1962">{{London Gazette |issue=42721 |date=29 June 1962 |startpage=5303 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He resigned that appointment and retired from the Royal Auxiliary Air Force on 1 September 1973.<ref name="LG 24 September 1973">{{London Gazette |issue=46085 |date=24 September 1973 |startpage=11405 |endpage= |supp=yes |accessdate=2012-07-11}}</ref>

==Business career==
Vanneck then went into business with the engineering company [[Ransomes, Sims & Jefferies|Ransome]]'s in [[Ipswich]], followed by [[Rowe and Pitman]] ([[brokerage firm|stockbrokers]]) in the [[City of London]]. He was appointed to the Council of the [[London Stock Exchange]] in 1968. He was Deputy Chairman from 1973 to 1975. He retired from the Council in 1979.<ref name="UH entry" />

==Political career==
Vanneck became involved in Municipal affairs through the [[City of London Corporation]] (as [[Alderman]] for [[Cordwainer (ward)|Cordwainer]] [[Wards of the City of London|Ward]] and a member of many [[Livery Company|Livery Companies]]).

After a year serving as a [[Sheriff of London]] in 1974, he was elected the 650th [[Lord Mayor of London]] in November 1977, towards the end of the [[Silver Jubilee of Elizabeth II|Queen's Silver Jubilee year]].<ref name="UH entry" /> He held an eloquent speech at the [[Guildhall, London|Guildhall]] in which he recalled the first time he had met [[Elizabeth II of the United Kingdom|The Queen]], who accompanied her father during a visit to the Royal Naval College when Vanneck was a young cadet there.

A popular Lord Mayor, Vanneck declared that despite his interesting careers, he had missed out on the one he would most like, which was to be a tug-boat skipper on the Thames. He made excellent contacts with his [[Paris]] counterpart [[Jacques Chirac]] and arranged an official visit to visit (one of only two that had taken place since the French revolution). Vanneck was a Francophile who was fluent in [[French language|French]].

At the end of his Lord Mayoral term, Vanneck was adopted as [[Conservative Party (UK)|Conservative]] candidate for the [[European Parliament]] for [[Cleveland (European Parliament constituency)|Cleveland]]. He won the seat at the [[European Parliament election, 1979 (United Kingdom)|1979 election]], enjoying his time in the European institutions. He was vice-chairman of the Political Affairs committee and served on Energy Resources and Technology. After keeping his seat by only 2,625 votes in 1984, he lost in the [[European Parliament election, 1989 (United Kingdom)|1989 election]], and then retired from public life.

==Later life==
[[High Sheriff]] of Suffolk from 1974 until his death, Vanneck continued to enjoy yacht racing (a member of the [[Royal Yacht Squadron]], he was a regular at [[Cowes Week]]).

He died on 2 August 1999 in London.<ref name="UH entry" />

==Honours and decorations==
As part of the 1955 [[Queen's Birthday Honours]], Vanneck was awarded the [[Air Force Cross (United Kingdom)|Air Force Cross]] (AFC).<ref name="LG 3 June 1955">{{London Gazette |issue=40497 |date=3 June 1955 |startpage=3292 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> In the 1963 [[New Year Honours]], he was appointed [[Officer of the Order of the British Empire]] (OBE).<ref name="LG 28 December 1962">{{London Gazette |issue=42870 |date=28 December 1962 |startpage=8 |endpage=9 |supp=yes |accessdate=2012-07-11}}</ref> In January 1965, he was appointed a Knight of the [[Venerable Order of Saint John]].<ref name="LG 11 January 1965">{{London Gazette |issue=43871 |date=11 January 1965 |startpage=331 |endpage= |supp= |accessdate=2012-07-11}}</ref> As part of the 1973 Queen's Birthday Honours, he was appointed [[Companion of the Order of the Bath]] (CB).<ref name="LG 22 May 1973">{{London Gazette |issue=45984 |date=22 May 1973 |startpage=6474 |endpage=6475 |supp=yes |accessdate=2012-07-11}}</ref> He was promoted to [[Knight Grand Cross of the Order of the British Empire]](GBE) as [[Lord Mayor of London]] [[elect]].<ref name="LG 25 October 1977">{{London Gazette |issue=47360 |date=25 October 1977 |startpage=13431 |endpage= |supp= |accessdate=2012-07-13}}</ref>

He was appointed [[Aide-de-camp#United Kingdom|aide-de-camp]] to [[Queen Elizabeth II]] on 26 February 1963.<ref name="LG 22 February 1963">{{London Gazette |issue=42929 |date=22 February 1963 |startpage=1811 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He relinquish the appointment on 1 September 1973, when he retired from the [[Royal Auxiliary Air Force]].<ref name="LG 29 October 1973">{{London Gazette |issue=46115 |date=29 October 1973 |startpage=12875 |endpage= |supp=yes |accessdate=2012-07-11}}</ref> He was a [[Deputy Lieutenant]] of the [[County of London]] from 1970. He was appointed to the [[Title of honor|honorary]] position of [[Inspector-General]] of the Royal Auxiliary Air Force on 16 September 1974,<ref name="LG 10 December 1974">{{London Gazette |issue=46427 |date=10 December 1974 |startpage=12555 |endpage= |supp=yes |accessdate=2012-07-13}}</ref> and held it until 1 January 1984.<ref name="LG 23 January 1984">{{London Gazette |issue=49625 |date=23 January 1984 |startpage=1052 |endpage= |supp=yes |accessdate=2012-07-13}}</ref>

==References==
{{Reflist|2}}

*Obituary, ''[[The Times]]'', 26 August 1999
*"Who Was Who", A&C Black

==External links==
*[http://www.europarl.europa.eu/members/archive/alphaOrder/view.do?language=EN&id=1494 European Parliament] site

{{S-start}}
{{s-par|eu}}
{{Succession box| title=[[Member of the European Parliament]] for [[Cleveland (European Parliament constituency)|Cleveland]] | years=[[European Parliament election, 1979 (United Kingdom)|1979]]&ndash;[[European Parliament election, 1984 (United Kingdom)|1984]] | before=''Direct elections started'' | after=''Constituency altered''}}
{{Succession box| title=[[Member of the European Parliament]] for [[Cleveland and Yorkshire North (European Parliament constituency)|Cleveland and Yorkshire North]] | years=[[European Parliament election, 1984 (United Kingdom)|1984]]&ndash;[[European Parliament election, 1989 (United Kingdom)|1989]] | before=''Constituency created'' | after=[[Dave Bowe]]}}
{{s-hon}}
{{Succession box| title=[[Lord Mayor of London]] | before=Sir [[Robin Gillett]] | after=Sir [[Kenneth Cork]] | years=1977&ndash;1978}}
{{S-end}}
{{Use dmy dates|date=April 2012}}

{{DEFAULTSORT:Vanneck, Peter}}
[[Category:1922 births]]
[[Category:1999 deaths]]
[[Category:People from Eye, Suffolk]]
[[Category:Alumni of Trinity College, Cambridge]]
[[Category:Harvard University people]]
[[Category:Sheriffs of the City of London]]
[[Category:Lord Mayors of London]]
[[Category:Royal Navy officers of World War II]]
[[Category:Fleet Air Arm aviators]]
[[Category:People educated at Geelong Grammar School]]
[[Category:People educated at Stowe School]]
[[Category:Conservative Party (UK) MEPs]]
[[Category:Members of the European Parliament for English constituencies]]
[[Category:Councilmen and Aldermen of the City of London]]
[[Category:Gentlemen Ushers]]
[[Category:Younger sons of barons]]
[[Category:MEPs for the United Kingdom 1979–84]]
[[Category:High Sheriffs of Suffolk]]
<!-- Orders and honours : United Kingdom -->
[[Category:Knights Grand Cross of the Order of the British Empire]]
[[Category:Recipients of the Air Force Cross (United Kingdom)]]
[[Category:Companions of the Order of the Bath]]
[[Category:Knights of the Order of St John]]
<!-- Orders and honours -->
[[Category:Commandeurs of the Légion d'honneur]]
[[Category:Grand Officers of the Order of the Crown (Belgium)]]
[[Category:British stockbrokers]]
[[Category:British people of American descent]]
[[Category:British people of Dutch descent]]
[[Category:British people of French descent]]
[[Category:British people of French-Canadian descent]]
[[Category:British people of Scandinavian descent]]