[[File:ATP Flight School Logo.gif|thumb|right|ATP Flight School]]

'''ATP Flight School''', headquartered in [[Jacksonville, Florida]], is the largest [[flight training]] company in the [[United States]]. The privately held company was founded in [[Atlanta, GA]] by its current management, a small group of airline [[Aviator|pilots]], in 1984. ATP’s initial focus was providing flight training to [[United States Armed Forces|U.S. military]] [[Aviator|pilots]] who were seeking their [[Airline Transport Pilot Licence|Airline Transport Pilot]] Certificates to transition over to commercial [[Airline|air carrier]] operations. Maintaining a focus on professional flight training, ATP later expanded its course offerings to include ab-initio training programs for both domestic and international students, seeking a career in commercial aviation.

==Schools==
ATP Flight School has 39 (as of August 2015) locations throughout the United States.<ref>{{cite web|url=http://www.atpflightschool.com/flight_schools/index.html |title=Directory of Flight Training Center Locations |publisher=ATP Flight School |date= |accessdate=2012-02-02}}</ref>
* [[Atlanta Regional Airport|Atlanta (FFC - Atlanta Regional Airport)]]
* [[Cobb County Airport|Atlanta (RYY - Cobb County Airport)]] 
* [[DeKalb-Peachtree Airport|Atlanta (PDK - Peachtree Dekalb Airport)]]
* [[Gwinnett County Airport|Atlanta (LZU - Gwinnett County Airport, Briscoe Field)]]
* [[Bowling Green-Warren County Regional Airport|Bowling Green (BWG - Warren County Airport)]]
* [[McClellan-Palomar Airport|Carlsbad (CRQ - McClellan-Palomar Airport)]]
* [[Concord Regional Airport|Charlotte (JQF - Concord Regional Airport)]]
* [[DuPage Airport|Chicago (DPA - Dupage Airport)]]
* [[Addison Airport|Dallas - Addison (ADS - Addison)]]
* [[Arlington Municipal Airport (Texas)|Dallas - Arlington (GKY - Arlington Municipal Airport)]]
* [[McKinney National Airport|Dallas - (TKI - McKinney National Airport)]]
* [[Daytona Beach Airport|Daytona Beach (DAB - Daytona Beach International Airport)]]
* [[Centennial Airport|Denver (APA - Centennial Airport)]]
* [[Ft. Lauderdale Airport|Ft. Lauderdale (FXE - Ft. Lauderdale Executive Airport)]]
* [[David Wayne Hooks Memorial Airport|Houston (DWH - Hooks Airport)]]
* [[Houston Southwest Airport|Houston (AXH - Houston Southwest Airport)]]
* [[Craig Airport|Jacksonville (CRG - Craig Airport)]]
* [[North Las Vegas Airport|Las Vegas (VGT - North Las Vegas Airport)]]
* [[Long Beach Airport|Long Beach (LGB - Long Beach/Daugherty Field)]]
* [[Long Island MacArthur Airport|Long Island (ISP - MacArthur Airport)]]
* [[Clark Regional Airport|Louisville (JVY - Clark Regional Airport)]]
* [[Morristown Municipal Airport|Morristown (MMU - Morristown Municipal Airport)]]
* [[Hayward Executive Airport|Oakland (HWD - Hayward Executive Airport)]]
* [[Ogden-Hinckley Airport|Ogden (OGD - Ogden-Hinckley Airport)]]
* [[Phoenix-Mesa Gateway Airport|Mesa (IWA - Phoenix Gateway Airport)]]
* [[Smyrna Airport (Tennessee)|Nashville (MQY - Smyrna Airport)]]
* [[Raleigh-Durham International Airport|Raleigh-Durham (RDU - Raleigh-Durham International Airport)]]
* [[Richmond International Airport|Richmond (RIC - Richmond International Airport)]]
* [[Riverside Municipal Airport|Riverside (RAL - Riverside Airport)]]
* [[Sacramento Executive Airport|Sacramento (SAC - Sacramento Executive Airport)]]
* [[McClellan–Palomar Airport|San Diego (CRQ - McClellan–Palomar Airport)]]
* [[Montgomery Field|San Diego (MYF - Montgomery Field)]]
* [[Scottsdale Airport|Scottsdale (SDL - Scottsdale Airport)]]
* [[St. Petersburg-Clearwater International Airport|Tampa (PIE - St Petersburg-Clearwater International Airport)]]
* [[Tacoma Narrows Airport|Tacoma (TIW - Tacoma Narrows Airport)]]
* [[Trenton-Mercer County Airport|Trenton (TTN - Trenton Mercer County Airport)]]
* [[Richard Lloyd Jones Jr. Airport|Tulsa (RVS - Richard Lloyd Jones Jr. Airport)]]

==Training==
ATP operates their programs with a fixed-cost, fixed timeframe training model, with an emphasis on multi-engine flight experience. ATP offers training programs for most all [[Fixed-wing aircraft|fixed-wing]] [[Federal Aviation Administration|FAA]] [[Pilot certification in the United States|pilot certificates]] and ratings. Other programs offered include [[regional jet]] training, which utilize [[Bombardier CRJ200|CRJ-200]] [[Flight simulator|flight-training devices (FTD)]]. This Regional Jet Standards Certification program was endorsed by [[Envoy Air|American Eagle Airlines]] in 2010.<ref>{{cite web|url=http://www.atpflightschool.com/airlines/american-eagle-hiring.html |title=American Eagle Airline Pilot Hiring Partnership |publisher=ATP Flight School |date= |accessdate=2012-02-02}}</ref><ref>{{cite web|url=http://pilotjobs.com/2011/06/22/american-eagle-airlines-announces-new-hiring-minimums/ |title=American Eagle Taps into the ATP Flight Training Pipeline with PilotPool.com |publisher=PilotJobs |date=2011-06-22 |accessdate=2012-02-02}}</ref>

ATP Flight School is also partnered with [[Arizona State University]], and is the flight provider for its Professional Flight bachelor's degree program, based out of the [[Phoenix-Mesa Gateway Airport]] facility. Aircraft used for the contract are painted in a special livery featuring the ASU logo and pitchfork.

[[File:Arizona State University and ATP Piper Seminole.jpeg|thumb|right|ASU and ATP's Piper Seminole]]

==Fleet==
ATP Flight School has the largest multi-engine training fleet in the world,<ref>{{cite web|url=http://www.piper.com/pages/PiperAtpReachDealatNbaafor30AirplanesWorth18MillionRetail_01242012.cfm |title=Piper Aircraft, Inc. - Piper, ATP Reach Deal At NBAA For 30 Airplanes, Worth $18 Million Retail |publisher=Piper.com |date=2011-10-11 |accessdate=2012-02-02}}</ref> consisting exclusively of [[Piper PA-44 Seminole|Piper Seminoles]], with the exception of one [[Cessna Citation|Cessna CE-525 CitationJet]]. A mix of [[Diamond DA40|Diamond DA40-180 Diamond Stars]] and [[Cessna 172|Cessna CE-172 Skyhawks]] make up their single-engine fleet, the majority going to the latter.

In 2006, ATP Flight School placed an order for a fleet of 20 [[Diamond D-Jet|Diamond D-JETs]] and 5 Diamond flight-training devices (FTD). Under this same agreement, [[Diamond Aircraft]] reached a partnership with ATP, in which the flight school will provide factory-approved initial [[type rating]]s and recurrent training for the [[very light jet]].<ref>{{cite web|url=http://www.diamondaircraft.com/news/news-article.php?id=30 |title=selects ATP for factory-authorized D-JET Flight Training |publisher=Diamond Aircraft |date= |accessdate=2012-02-02}}</ref>

[[Piper Aircraft|Piper Aircraft Inc.]] and ATP jointly made an announcement at the 2011 [[National Business Aviation Association|National Business Aviation Association (NBAA)]] Annual Meeting and Convention for the sale of 30 new [[Piper PA-44 Seminole|Piper Seminoles]] – a total retail value of $18 million.<ref>{{cite web|url=http://www.piper.com/pages/PiperAtpReachDealatNbaafor30AirplanesWorth18MillionRetail_01242012.cfm |title=Piper Aircraft, Inc. - Piper, ATP Reach Deal At NBAA For 30 Airplanes, Worth $18 Million Retail |publisher=Piper.com |date=2011-10-11 |accessdate=2012-02-02}}</ref> All of the new airplanes under the agreement are to be equipped with [[Garmin|Garmin’s]] G500 [[glass cockpit]] avionics suite.

==Airline Relationships==
ATP Flight School has hiring relationships with 10 U.S. based [[regional airline]]s, including [[ExpressJet]], [[Endeavor Air]], and [[PSA Airlines]].<ref>{{cite web|last=Wallace |first=Lane |url=http://www.flyingmag.com/safety/training/airline-transport-professionals |title=Airline Transport Professionals &#124; Flying Magazine &#124; The World’s Most Widely Read Aviation Magazine |publisher=Flyingmag.com |date=2009-06-03 |accessdate=2012-02-02}}</ref><ref>{{cite web|url=http://www.atpflightschool.com/airlines/ |title=Airline Pilot Hiring Partnerships |publisher=ATP Flight School |date=2010-09-16 |accessdate=2012-02-02}}</ref> These relationships and alliances are based on [[Letter of understanding|letters of understanding]], or letters of agreement. While these letters vary between [[Airline|air carriers]], in general they bestow reduced hiring minimums for graduates of ATP.<ref>{{cite web|url=http://www.aero-news.net/index.cfm?do=main.textpost&id=54451932-65ee-4033-b7ae-74a1741bc22f |title=ATP Signs Two New Pilot Hiring Agreements &#124; Aero-News Network |publisher=Aero-news.net |date=2006-12-27 |accessdate=2012-02-02}}</ref>

==References==
{{Reflist}}
*
*
*
*

== External links ==
* [http://www.atpflightschool.com Official Site]

{{coord missing|Jacksonville}}

[[Category:Organizations based in Jacksonville, Florida]]
[[Category:Privately held companies based in Florida]]
[[Category:Educational institutions established in 1984]]
[[Category:Aviation schools in the United States]]