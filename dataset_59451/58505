{{Infobox journal
| title = Journal of Software: Evolution and Process
| formernames = Journal of Software Maintenance: Research and Practice, Journal of Software Maintenance and Evolution: Research and Practice
| editor = Gerardo Canfora, Darren Dalcher, David Raffo
| discipline = [[Computer science]], [[software maintenance]], [[software evolution]]
| publisher = [[John Wiley & Sons]]
| country =
| frequency = Monthly
| history = 1989-present
| impact = 1.273
| impact-year = 2012
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2047-7481
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2047-7481/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2047-7481/issues
| link2-name = Online archive
| ISSN= 1532-060X
| eISSN = 2047-7481
| OCLC = 488593641
| LCCN = 2012205429
}}
The '''''Journal of Software: Evolution and Process''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[software development]] and [[software evolution|evolution]]. It is published by [[John Wiley & Sons]]. The journal was established in 1989 as the ''Journal of Software Maintenance: Research and Practice'', renamed in 2001 to ''Journal of Software Maintenance and Evolution: Research and Practice'', and obtained its current title in 2012. The [[editors-in-chief]] are Gerardo Canfora ([[University of Sannio]]), Darren Dalcher ([[University of Hertfordshire]]), and David Raffo ([[Portland State University]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref>{{cite web |url=http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2047-7481/homepage/ProductInformation.html |title=Journal of Software: Evolution and Process - Overview  |publisher=[[John Wiley & Sons]] |work=Wiley Online Library |accessdate=2013-06-29}}</ref>
{{columns-list|colwidth=30em|
* [[Academic Search]]
* [[Advanced Polymers Abstracts]]
* [[Ceramic Abstracts]]/World Ceramic Abstracts
* [[Compendex]]
* [[CompuMath Citation Index]]
* [[Computer & Information Systems Abstracts]]
* [[ACM Computing Reviews]]
* [[CSA Civil Engineering Abstracts]]
* [[CSA Mechanical & Transportation Engineering Abstracts]]
* [[CSA Technology Research Database]]
* [[Current Contents]]/Engineering, Computing & Technology
* [[Inspec]]
* [[METADEX]]
* [[Science Citation Index Expanded]]
* [[Scopus]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 1.273, ranking it 30th out of 105 journals in the category "Computer Science, Software Engineering".<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Computer Science, Software Engineering |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref>

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)2047-7481}}

[[Category:Computer science journals]]
[[Category:Software engineering publications]]
[[Category:Software maintenance]]
[[Category:Publications established in 1989]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]


{{compu-journal-stub}}