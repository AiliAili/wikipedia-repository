{{Infobox journal
| title = Autonomic Neuroscience
| cover = 
| editor = [[Geoffrey Burnstock]]
| discipline = [[Autonomic neuroscience]]
| former_names = Journal of the Autonomic Nervous System
| abbreviation = Auton. Neurosci.
| publisher = [[Elsevier]]
| country = 
| frequency = 8/year
| history = 1978-present
| openaccess = [[Hybrid open access journal|Hybrid]] 
| license = 
| impact = 1.562
| impact-year = 2014
| website = http://www.journals.elsevier.com/autonomic-neuroscience-basic-and-clinical/
| link1 = http://www.autonomicneuroscience.com/
| link1-name = Online access
| link2 = http://www.autonomicneuroscience.com/issues
| link2-name = Online archive
| link3 =http://www.sciencedirect.com/science/journal/01651838
| link3-name = ''Journal of the Autonomic Nervous System'' archives
| JSTOR = 
| OCLC = 838647867
| LCCN = 
| CODEN = ANUEB2
| ISSN = 1566-0702
| eISSN = 1872-7484
}}
'''''Autonomic Neuroscience: Basic and Clinical''''' is a [[peer-reviewed]] [[scientific journal]] covering research on the [[autonomic nervous system]]. It is published by [[Elsevier]] and is the official journal of the [[International Society for Autonomic Neuroscience]]. It was established by Chandler McCluskey Brooks in 1978<ref>{{cite journal |last1=Koizumi |first1=K. |last2=Vassalle |first2=M. |title=Chandler McCluskey Brooks |journal=Biographical Memoirs |volume=91 |series=Office of the Home Secretary, National Academy of Sciences, National Academies Press |page=67 |url=http://www.nasonline.org/publications/biographical-memoirs/memoir-pdfs/brooks-chandler-mcc.pdf}}</ref> as the ''Journal of the Autonomic Nervous System'' and obtained its current title in 2000. The [[editor-in-chief]] is [[Geoffrey Burnstock]] ([[UCL Medical School]]).

The journal is abstracted and indexed by [[MEDLINE]]/[[Pubmed]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.562.<ref name=WoS>{{cite book |year=2015 |chapter=Autonomic Neuroscience |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.journals.elsevier.com/autonomic-neuroscience-basic-and-clinical/}}

[[Category:Neuroscience journals]]
[[Category:Elsevier academic journals]]
[[Category:Publications established in 1978]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]