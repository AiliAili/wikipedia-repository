{{speciesbox
| name = Mexican beaded lizard<ref name="itis">{{ITIS |id=585899 |taxon=Heloderma horridum |accessdate=20 September 2008}}</ref>
| status = LC
| status_system = iucn3.1
| status_ref = <ref name=iucn>{{IUCN2010.1 | assessor=Canseco Marquez, L. | assessor2=Muñoz, A. | last-assessor-amp=yes |year=2007 | id=9864 | title=Heloderma horridum | downloaded=19 June 2010}} Database entry includes a brief justification of why this species is of least concern</ref>
| image = BeadedLizard-AHPExotics.jpg
| image_width = 240px
| genus = Heloderma
| species = horridum
| authority = ([[Arend Friedrich August Wiegmann|Wiegmann]], 1829)
| synonyms = ''Trachyderma horridum'' [[Arend Friedrich August Wiegmann|Wiegmann]], 1829
}}
The '''Mexican beaded lizard''' (''Heloderma horridum'') is the most famous of the four species of [[venomous]] beaded [[lizard]]s found principally in [[Mexico]] and southern [[Guatemala]]. They and their [[Genus|congener]] the [[Gila monster]] (''Heloderma suspectum'') are the only lizards known to have evolved an overt venom delivery system.  The Mexican beaded lizard is larger than the Gila monster, but has duller coloration, black with yellowish bands. As it is a specialized predator that feeds primarily upon eggs, the primary use of its venom is still a source of debate among scientists. However, this venom has been found to contain several [[enzyme]]s useful for manufacturing drugs in the treatment of [[diabetes]], and research on the pharmacological use of its venom is ongoing.

Threatened throughout its range by overcollection and [[Habitat destruction|habitat loss]], it is a [[CITES]] protected species.  The [[Heloderma charlesbogerti|Guatemalan beaded lizard]] (''H. charlesborgeti'') is one of the rarest lizards in the world, with a wild population of fewer than 200.

== Taxonomy ==
The beaded lizards have one close living relative, the [[Gila monster]] (''H. suspectum''), as well as many extinct relatives in the Helodermatidae, whose genetic history may be traced back to the [[Cretaceous]] period. The genus ''[[Heloderma]]'' has existed since the [[Miocene]], when ''H. texana'' ranged over most of North America.<ref name="Cogger"/>  Because the helodermatids have remained relatively unchanged morphologically, they are occasionally regarded as [[living fossil]]s.<ref name="varanoid">{{cite book |author1=King, Ruth Allen |author2=Pianka, Eric R. |author3=King, Dennis |title=Varanoid Lizards of the World |publisher=Indiana University Press |location=Bloomington |year=2004 |pages= |isbn=0-253-34366-6}}</ref> Although the beaded lizard appears closely related to the [[monitor lizard]]s (varanids) of Africa, Asia, and Australia, the wide geographical separation and unique features not found in the varanids indicate the beaded lizard is better placed in a separate family.<ref name="mattison">{{cite book |author=Mattison, Chris |title=Lizards of the World |publisher=Blandford |location=London |year=1998 |pages= |isbn=0-7137-2357-2 }}</ref>

The species was first described in 1829 by [[Arend Friedrich August Wiegmann|Arend Wiegmann]] as ''Trachyderma horridum'', but he renamed it ''Heloderma horridum'' six months later.<ref>{{cite journal | last = Wiegmann| first = A.F.A.| authorlink = Arend Friedrich August Wiegmann|  title = Über die Gesetzlichkeit in der geographischen Verbreitung der Saurier| journal = Isis| volume =22 | issue =3-4 | pages = 418–428| publisher =Oken | year = 1829 }}</ref>  Its [[genus|generic]] name ''Heloderma'' means "studded skin", from the [[Ancient Greek]] words ''hêlos'' (ἧλος)—the head of a nail or stud—and ''dérma'' (δέρμα), meaning skin. Its [[species|specific]] name, ''horrĭdum'', is the [[Latin]] word meaning rough or rude.

==Species==
The four [[subspecies]] of beaded lizard, elevated to full species in 2013, are:<ref>http://www.redlist-arc.org/Article-PDFs/Special%20Mexico%20Issue_ARC_7(1)_74-96_low_res.pdf Reiserer & al., 2013, ''Taxonomic reassessment and conservation status 
of the beaded lizard, ''Heloderma horridum'' 
(Squamata: Helodermatidae)''</ref>
{| cellspacing=0 cellpadding=2 border=1 style="border-collapse: collapse;"
!bgcolor="#f0f0f0"|Species
!bgcolor="#f0f0f0"|Taxon author
!bgcolor="#f0f0f0"|Subsp.*
!bgcolor="#f0f0f0"|Common name
!bgcolor="#f0f0f0"|Geographic range
|-
|''[[Heloderma alvarezi|H. alvarezi]]''
|[[Charles Mitchill Bogert|Bogert]] and Martin del Campo, 1956
|align="center"|0
|Chiapan beaded lizard
|style="width:40%"|[[Mexico]]: Chiapas to extreme northwestern [[Guatemala]].
|-
|''[[Heloderma charlesbogerti|H. charlesbogerti]]''<span style="font-size:100%;"><sup>T</sup></span>
|[[Jonathan Atwood Campbell|Campbell]] and Vannini, 1988
|align="center"|0
|Guatemalan beaded lizard
|[[Guatemala]]: Motagua Valley.
|-
|''[[Heloderma exasperatum|H. exasperatum]]''
|[[Charles Mitchill Bogert|Bogert]] and Martin del Campo, 1956
|align="center"|0
|Rio Fuerte beaded lizard
|[[Mexico]]: Rio Fuerte, Rio Mayo, southern Sonora, northern Sinaloa. Sierra Madre Occidental, Rio Mayo, and Rio Fuerte, western Chihuahua, and northern Sinaloa.
|-
|''H. horridum''<span style="font-size:100%;"><sup>T</sup></span>
|(Wiegmann, 1829)
|align="center"|0
|Mexican beaded lizard
|[[Mexico]]: southern Sinaloa to Oaxaca.
|-
|}

== Description ==
[[File:Gila fg01.jpg|thumb|right|Close-up of a helodermatid's skin, composed of beadlike scales]]
Adult beaded lizards range from {{convert|57|to|91|cm|in|abbr=on}} in length. They are substantially larger than the Gila monster, which only reaches lengths of {{convert|30|to|56|cm|in|abbr=on}}. The snout-to-vent length of a beaded lizard averages {{convert|33|to|48|cm|in|abbr=on}}. The average body mass of an adult beaded lizard is {{convert|800|g|lb|abbr=on}}, about 45% heavier than the average mass of a gila monster, with large specimens exceeding {{convert|2000|g|lb|abbr=on}}. Maximum weight known is {{convert|4000|g|lb|abbr=on}}<ref>Beck, D. D. (2005). ''Biology of Gila monsters and beaded lizards (Vol. 9)''. Univ of California Press.</ref><ref>Grzimek, Dr. H.C. Bernhard. 1975. ''Animal Life Encyclopedia. Volume 6'' Pgs. 321-322 and 151-152. Van Nostrand Reinhold Company.</ref> Although males are slightly larger than females, the lizards are not [[sexually dimorphic]]. Both males and females are stocky with broad heads, although the males' tend to be broader.<ref name="SA"/> The beaded lizard's [[Scale (zoology)|scales]] are small, beadlike, and not overlapping. Except for the lizard's underside, the majority of its scales are underlaid with bony [[osteoderms]].<ref name="SA"/>

Their base color is black and marked with varying amounts of yellow spots or bands, with the exception of ''H. alvarezi'', which tends to be all [[melanistic|black]] in color. The beaded lizard has a short tail which is used to store fat so it can survive during months of [[estivation]]. Unlike many other lizards, this tail does not [[autotomy|autotomize]] and cannot grow back if broken. The beaded lizard has a forked, black tongue which it uses to smell, with the help of a [[Jacobson's organ]]; it sticks its tongue out to gather scents and touches it to the opening of the organ when the tongue is retracted.<ref name="Cogger">Cogger (1992) p. 156</ref>
[[File:Heloderma_alvarezi.jpg|thumb|left|The Chiapan beaded lizard tends to be all black, brown, or gray in color.]]

== Habitat and range ==
Beaded lizards are found in the Pacific drainages from southern [[Sonora]] to southwestern [[Guatemala]] and two Atlantic drainages, from central [[Chiapas]] to southeastern Guatemala.<ref name="SA"/> Their habitats are primarily in the desert, tropical [[deciduous forest]]s and [[Deserts and xeric shrublands|thorn scrub]] forests, but are found in pine-oak forests, with elevations from [[sea level]] to 1500 m. In the wild, the lizards are only active from April to mid-November, spending about an hour per day above the ground.<ref name="Beck">{{harvnb|Beck|2005|pp=35–36}}</ref>

The Mexican beaded lizard ''H. horridum'' is found in Mexico, from [[Sonora]] to [[Oaxaca]]. The [[Rio Fuerte beaded lizard]] (''H. exasperatum'') is found from southern Sonora to northern [[Sinaloa]].  The [[Chiapan beaded lizard]] (''H. alvarezi'') is found in the northern Chiapas and the depression of the [[Río Lagartero]] in [[Huehuetenango]] to northwestern Guatemala.<ref name="Beck"/> The ranges of these three species overlap, making them [[sympatric]].<ref name ="Campbell"/> The [[Guatemalan beaded lizard]] (''H. charlesbogerti'') is the only [[allopatric]] one, separated from the nearest population (''H. alvarezi'') by 250&nbsp;km of unsuitable habitat.<ref name="Campbell">{{cite journal|last=Campbell|first=J.|author2=J. Vannini|year=1988|title=A new subspecies of beaded lizard, Heloderma horridum, from the Motagua Valley of Guatemala|jstor=1564340|journal=Journal of Herpetology|volume=22|pages=457–468|doi=10.2307/1564340|issue=4}}</ref> The Guatemalan beaded lizard is the most endangered of the species, if not of all lizards; it is found only in the [[dry valley]] of the [[Río Motagua]] in northeastern Guatemala; less than 200 are believed to exist in the wild.<ref name="Beck"/>

== Ecology ==

=== Diet ===
[[File:Heloderma horridum pair (Buffalo Zoo).jpg|thumb|right|A pair of Mexican beaded lizards at the [[Buffalo Zoo]]:  The specimen on the right is in the process of shedding.]]
The beaded lizard is a specialized [[vertebrate]] nest predator, feeding primarily on bird and reptile [[Egg (biology)|eggs]]. A semi-arboreal species, it is found climbing [[deciduous]] trees in search of prey when encountered above ground.<ref name="Pianka">{{cite journal | last = Pianka| first = Eric| title =Convexity, desert lizards and spatial heterogeneity | jstor = 1935656 | journal = Ecology | volume =47 | pages =1055–1059 | year = 1966 | doi = 10.2307/1935656 | issue = 6}}</ref>  It occasionally preys upon small birds, mammals, frogs, lizards, and insects.  Steve Angeli and Robert Applegate, noted captive breeders of the beaded lizard, have remarked that captive specimens do best on a diet of small vertebrates primarily mice and rats. Confiscated wild-caught specimens can be made to feed by using egg on the prey item.<ref name="SA">{{cite journal | last = Angeli| first = Steven| title = Beaded Dragon| journal = Reptile Care| volume = 9| issue = 1| pages = 36–39| publisher = | year = 2005| url = http://www.helodermahorridum.com/article.pdf|  accessdate = 2008-09-22}}</ref><ref name="Applegate">{{Cite conference  | first = Robert  | last = Applegate  | title = Northern California Herpetological Society's Conference on Captive Propagation and Husbandry of Reptiles and Amphibians  | year = 1991  | pages = 39–44  | location = Sacramento, California  | publisher = Northern California Herpetological Society }}</ref>

=== Venom ===
The [[venom]] glands of the beaded lizard are modified [[salivary gland]]s located in the reptile's lower jaw. Each gland has a separate duct leading to the base of its grooved teeth.  When biting, the beaded lizard hangs on its victim and chews to get its venomous saliva into the wound.  Although its jaw grip is strong, its unsocketed teeth are easily broken off at their bases. The beaded lizard's venom is a weak [[hemotoxin]], and although human deaths are rare, it can cause [[respiratory failure]]. It consists of a number of components, including L-amino acid oxidase, hyaluronidase, phospholipase A, serotonin, and highly active [[kallikrein]]s that release vasoactive kinins. The venom contains no enzymes that significantly affect coagulation.  Almost all documented human bites (eight in the past century) have resulted from prodding captive lizards with a finger or bare foot.<ref name="Freiberg">{{Harvnb|Freiberg|1984|pp=116–120}}</ref>

While invertebrates are essentially immune to the effects of this venom, effects on vertebrates are more severe and varied. In mammals such as rats, major effects include a rapid reduction in carotid blood flow followed by a marked fall in blood pressure, respiratory irregularities, tachycardia, and other cardiac anomalies, as well as [[hypothermia]], [[edema]], and internal hemorrhage in the [[gastrointestinal tract]], lungs, eyes, liver, and kidneys. In humans, the effects of bites are associated with excruciating pain that may extend well beyond the area bitten and persist up to 24 hours. Other common effects of bites on humans include local edema (swelling), weakness, sweating, and a rapid fall in blood pressure. Beaded lizards are immune to the effects of their own venom.<ref name="Beck44">{{Harvnb|Beck|2005|p=44}}</ref>

[[File:Heloderma horridum.jpg|thumb|The Mexican beaded lizard's base color is black, with yellow bands or patches.]]

The compounds in its venom have been studied have pharmacological properties relating to [[diabetes]], [[Alzheimer's disease]], and even [[HIV]].<ref name="Beck4161">{{Harvnb|Beck|2005|pp=41–61}}</ref>  This hormone was named exendin-3 and is marketed by [[Amylin Pharmaceuticals]] as the drug [[exenatide]].<ref name="Eng">{{cite journal | last = Eng| first = John |author2=Wayne A. Kleinman |author3=Latika Singh |author4=Gurchar Singh |author5=Jean-Pierre Raufman| title =Isolation and Characterization of Exendin-4, an Exendin-3 Analogue,from Heloderma suspectum Venom | journal =The Journal of Biological Chemistry | volume =267 | issue = 11| pages =7402–7406 | publisher = | year =1992 | url = http://www.jbc.org/cgi/reprint/267/11/7402.pdf| accessdate =2008-09-21 | pmid=1313797}}</ref>  One study reported in 1996 revealed that it binds to cell receptors from breast [[cancer cells]] and may stop the growth of lung cancer cells.<ref>{{cite journal|last=Raufman|first=J.P.|year=1996|title=Bioactive peptides from lizard venoms|journal=Regulatory Peptides|volume=61|pages=1–18|doi=10.1016/0167-0115(96)00135-8|pmid=8701022|issue=1}}</ref>

=== Reproduction ===
The beaded lizard becomes sexually mature at six to eight years and mates between September and October. Males engage in [[Agonistic behaviour|ritual combat]] that often lasts several hours; the victor [[mating|mates]] with the female.<ref name="Cogger"/> The female lays her clutch of two to 30&nbsp;[[egg (biology)|eggs]] between October and December, the clutch hatching the following June or July.<ref name="SA"/>

Young lizards are seldom seen. They are believed to spend much of their early lives underground, emerging at two to three years of age after gaining considerable size.<ref name="nature"/>

== Conservation ==
[[File:Mexican-Beaded-Lizard.jpg|thumb|right|Mexican beaded lizard at the [[Louisville Zoo]], [[Kentucky]]]]
The beaded lizard is surrounded by myth and superstition in much of its [[Range (biology)|native range]].  It is incorrectly believed, for example, to be more venomous than a [[rattlesnake]], can cause [[lightning|lightning strikes]] with its tail, or make a [[pregnant]] woman miscarry by merely looking at her.  As a result of this superstition, locals often kill the lizard on sight.<ref name="nature">{{cite web|title=Protecting the Guatemalan Beaded Lizard |work=The Nature Conservancy in Guatemala |publisher=[[The Nature Conservancy]] |year=2007 |url=http://www.nature.org/wherewework/centralamerica/guatemala/features/index.html |accessdate=2010-09-14 |deadurl=yes |archiveurl=https://web.archive.org/web/20110105154521/http://www.nature.org/wherewework/centralamerica/guatemala/features/index.html |archivedate=January 5, 2011 }}</ref>

The seldom-seen lizard is [[poaching|poached]] and sold into the illegal exotic animal trade. It does not reproduce well in captivity, and its scarcity means a high price for collectors. As a direct result, the beaded lizard is protected by Mexican law under the category A (Threatened), and it dwells within the range of several protected areas.<ref name="cites"/> In Guatemala, it is protected by national legislation, and part of its range is within protected areas. It is listed on Appendix II of CITES.<ref name="nature"/>

Fewer than 200 lizards remain in the [[Tropical and subtropical dry broadleaf forests|dry forest]] habitat of the [[Motagua River|Motagua Valley]], and this species of beaded lizard (''[[Heloderma charlesbogerti|H. charlesbogerti]]'') was facing extinction due to local extermination and loss of habitat for agricultural purposes.<ref name="nature"/>  A conservation effort has been launched known as Project Heloderma to preserve the [[semiarid]] habitat of the Motagua Valley by [[the Nature Conservancy]] and partners such as ZOOTROPIC, CONAP, the International Reptile Conservation Foundation, [[Lincoln Park Zoo]], [[Zoo Atlanta]], and the [[San Diego Zoo]]. This effort has been successful in getting the Guatemalan government to list the beaded lizard under the Convention on International Trade in Endangered Species as an Appendix I animal, making it illegal to export the species.<ref name="cites">{{cite web | title = CONSIDERATION OF PROPOSALS FOR AMENDMENT OF APPENDICES I AND II| work = Fourteenth meeting of the Conference of the Parties| publisher =Convention on International Trade in Endangered Species of Wild Flora and Fauna | year = 2007| url =http://www.cites.org/eng/cop/14/prop/E14-P14.pdf | format=pdf|accessdate = 2008-09-22 }}</ref>

==References==<!-- BiodiversConserv17:2037. -->
{{Reflist|colwidth=35em}}

==Further reading==
* Ariano, D. 2008. Envenomation by a wild Guatemalan beaded lizard ''Heloderma horridum charlesbogerti''. Clinical toxicology 46 (9): 897-899. [http://www.informapharmascience.com/doi/pdf/10.1080/15563650701733031]
* Ariano, D. y G. Salazar. 2007. Notes on the Distribution of the Endangered Lizard, ''Heloderma horridum charlesbogerti'', in the Dry Forests of Eastern Guatemala: An Application of Multi-criteria Evaluation to Conservation. Iguana 14(3): 152-158.[http://www.scribd.com/doc/11116925/Ariano-D-y-G-Salazar-2007-Notes-on-the-Distribution-of-the-Endangered-Lizard-Heloderma-horridum-charlesbogerti-in-the-Dry-Forests-of-Eastern-Gu]
* Ariano, D. 2006. The Guatemalan beaded lizard: Endangered inhabitant of a unique ecosystem. Iguana 13(3): 178-183. [http://www.scribd.com/doc/11117893/Ariano-D-2006-The-Guatemalan-beaded-lizard-Endangered-inhabitant-of-a-unique-ecosystem-Iguana-133-178183]
* {{IUCN2006|assessor=Beaman|year=1996|id=9864|title=Heloderma horridum|downloaded=11 May 2006}} Listed as Vulnerable (VU A2cd v2.3)
* {{cite book | last = Beck | first = Daniel D.| title =Biology of Gila Monsters and Beaded Lizards (Organisms and Environments) | publisher = University of California Press| year = 2005 | isbn =0-520-24357-9| page =247 }}
* {{cite book | author=Berkow, Robert | title=The Merck Manual | edition=16th| publisher=Merck Research Laboratories| year=1992 | isbn=0-911910-16-6}}
* {{cite book | last = Cogger  | first = Harold  |authorlink = Harold Cogger  | last2 = Zweifel  | first2 = Richard  | title = Reptiles & Amphibians  | publisher = Weldon Owen  | location = [[Sydney]]  |year = 1992  | isbn = 0-8317-2786-1}}
* {{cite book |last= Freiberg|first= Dr. Marcos|last2= Walls|first2=Jerry |title= The World of Venomous Animals|year= 1984
|publisher= TFH Publications|location=[[New Jersey]] |isbn= 0-87666-567-9}}

==  External ==
{{wikispecies|Heloderma horridum}}
{{commons|Heloderma horridum}}
* [https://web.archive.org/web/20081120122926/http://www.ircf.org/projectheloderma/downloads/GBL-ConservationProj-2Web.pdf IRCF's Guatemalan Beaded Lizard Conservation Project]
* [https://web.archive.org/web/20120807033258/http://www.lazoo.org:80/animals/reptiles/lizard_mexicanbeaded/index.html Mexican Beaded Lizard at the LA Zoo]
* [https://web.archive.org/web/20081008005848/http://www.stlzoo.org/animals/abouttheanimals/reptiles/lizards/mexicanbeadedlizard.htm Mexican Beaded Lizard at the Saint Louis Zoo]
* [http://mobile.zooatlanta.org/home/conservation_efforts/guatemalan_beaded_lizards_conservation_project?utm_referrer=http%3A%2F%2Fwww.zooatlanta.org%2Fhome%2Fconservation_efforts Protecting Guatemalan beaded lizards: Conservation Heloderma]
{{Varanoidea}}
{{good article}}

{{DEFAULTSORT:Beaded Lizard}}
[[Category:Helodermatidae]]
[[Category:Venomous lizards]]
[[Category:Megafauna of North America|Lizard Beaded]]
[[Category:Reptiles of Guatemala]]
[[Category:Reptiles of Mexico]]
[[Category:Animals described in 1829]]