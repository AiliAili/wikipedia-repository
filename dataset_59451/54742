{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = The American Claimant
| title_orig    = 
| translator    = 
| image         = Image:TheAmericanClaimant.jpg
| caption       = First edition
| author        = [[Mark Twain]]
| illustrator   = [[Dan Beard]]<ref>[[:File:1892. The American Claimant.djvu|Facsimile of the original 1st edition]].</ref>
| cover_artist  = 
| country       = United States
| language      = English
| series        = 
| genre         = [[Humor]], [[Satire]], [[Alternate history (fiction)|alternate history]], [[science fiction]], [[fantasy]]
| publisher     = Charles L. Webster
| release_date  = 1892
| media_type    = Print ([[Hardcover]], [[Paperback]])
| pages         = 291 pp
| isbn          = 
| preceded_by   =
| followed_by   =
}}

'''''The American Claimant''''' is an [[1892 in literature|1892]] [[novel]] by American [[humorist]] and [[writer]] [[Mark Twain]].  Twain wrote the novel with the help of [[Phonograph cylinder|phonographic]] [[Dictation machine|dictation]],<ref>[https://query.nytimes.com/gst/fullpage.html?res=9804EFDE1630F936A15750C0A96F958260 In Love With Technology, as Long as It's Dusty], a March 25, 1999 article from ''[[The New York Times]]''</ref> the first author (according to Twain himself) to do so.<ref>[https://query.nytimes.com/gst/fullpage.html?res=9904E0D71131F936A25754C0A96E958260&pagewanted=all Twain Rolls On To New Heights], a July 15, 1998 article from ''[[The New York Times]]''</ref> This was also (according to Twain) an attempt to write a book without mention of the weather, the first of its kind in fictitious literature. Indeed, all the weather is contained in an appendix, at the back of the book, which the reader is encouraged to turn to from time to time.

==Plot==
The American Claimant is a comedy of mistaken identities and multiple role switches. Its cast of characters include an American enamored of British hereditary aristocracy and a British earl entranced by American democracy.

==Characters==
'''Colonel Mulberry Sellers:''' An eccentric white-headed old man who becomes the rightful heir to the Earl of Rossmore after the death of his relative, Simon Lathers. According to his wife, Sellers is a "scheming, generous, good-hearted, moonshiny, hopeful, no-account failure" who is well beloved for his generosity and approachability. Although many of his eccentric money-making schemes are failures, he occasionally "makes a strike," as he calls it, and makes quite a bit of money. One such strike is the exceedingly popular "Pigs in the Clover" toy which he invented and patented.

According to the tin signs by his door, Sellers is an attorney at law and claim agent, a materializer, a hypnotizer, and a mind-cure dabbler. He has also been named "Perpetual Member of the Diplomatic Body representing the multifarious sovereignties and civilizations of the globe near the republican court of the United States of America."

The explanatory note at the beginning of the novel indicates that Colonel Sellers is the same character as Eschol Sellers in the first edition of Twain's earlier novel [[The Gilded Age: A Tale of Today|Gilded Age]](1873) and Beriah Sellers in later editions. The note also identifies Colonel Sellers as the same character as Mulberry Sellers in John T. Raymond's dramatization of ''Gilded Age''.

'''Washington Hawkins:''' The Congressional Delegate from Cherokee Strip who partners with Colonel Sellers in several of his schemes. Hawkins is described as a "stoutish, discouraged-looking man whose general aspect suggested that he was fifty years old, but whose hair swore to a hundred." At the beginning of the novel he has been living with his wife, Louise, and their children in the far west for the last fifteen years.

'''Sally (Gwendolen) Sellers:''' Daughter of Colonel Mulberry Sellers and Polly Sellers. She attends Rowena-Ivanhoe College, "the selectest and most aristocratic seat of learning for young ladies" in the US. Like her father, Sally is given to Romantic aspirations and delusions of grandeur. She happily takes the name Gwendolen after her father becomes the rightful heir of the Earl of Rossmore. However, the narrative describes Sally as having a "double personality": She is both Sally Sellers, who is "practical and democratic,"  and Lady Gwendolen, who is "romantic and aristocratic." During the day she works hard designing and sewing dresses to help financially support her family, and in the evening she upholds the shadowy fantasy of the family's nobility. She falls in love with Howard Tracy (Viscount Berkeley) at first sight and later renounces her aspirations of aristocracy in order to be with him.

'''Berkeley Rossmore (Howard Tracy):''' The only son and heir of the Earl of Rossmore. According to the narrative, his full name is the Honourable Kirkcudbright Llanover Marjoribanks Sellers Viscount-Berkeley, of Cholmondeley Castle, Warwickshire(which the narrative tells us is pronounced "K'koobry Thlanover Marshbanks Sellers Vycount Barkly, of Chumly Castle, Warrikshr). At the beginning of the novel, Berkeley announces his intention to go to America and "change places" with Simon Lathers, the man he considers the rightful heir. He wishes to "retire...from a false existence, a false position, and begin [his] life over again, begin it right--begin it on the level of mere manhood, unassisted by factitious aids, and succeed or fail by pure merit or the want of it. I will go to America, where all men are equal and all have an equal chance; I will live or die, sink or swim, win or lose as just a man—that alone, and not a single helping gaud or fiction back of it." Shortly after his arrival to the United States, the hotel in which he is staying catches fire, and during his escape, he snatches up and dresses himself in the clothes and hat of one-armed Pete, leading everyone to believe that he is a cowboy. After the newspapers announce that Berkeley has died in the fire, he decides to renounce his former identity and calls himself Howard Tracy, determining to work for his living according to democratic principles.

==Gallery==
<gallery>
File:Mark Twain-The American Claimant-1896-2.png|"He was constructing what seemed to be some kind of frail mechanical toy". Illustration to 1896 edition.
</gallery>

==See also==
*[[Mark Twain bibliography]]

==References==
<references/>

==External links==
{{Gutenberg|no=3179|name=The American Claimant}}
*[http://www.lib.berkeley.edu/give/historyroom/panel10.html Special Collections] from the [[University of California, Berkeley Library System|UC Berkeley Library]] website, depicting Twain's preface to ''The American Claimant'' from the original manuscript
* {{librivox book | title=The American Claimant | author=Mark Twain}}

{{Twain}}

{{DEFAULTSORT:American Claimant, The}}
[[Category:1892 novels]]
[[Category:Novels by Mark Twain]]
[[Category:19th-century American novels]]