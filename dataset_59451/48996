'''Kathleen M. Adams''' (born 1957, San Francisco, California) is a cultural anthropologist and Professor of [[Anthropology]] and [[Asian studies|Asian Studies]] at [[Loyola University Chicago]]. She is also an adjunct curator of Southeast Asian Ethnology at the [[Field Museum of Natural History]]. Her research focuses on island [[Southeast Asia]], [[critical tourism studies]], [[heritage studies]], museums, [[material culture]] and ethnic arts, [[globalization]], ethnicity and nationalism. She first began anthropological fieldwork in [[Indonesia]] in 1984<ref>{{cite web|url=http://www.voaindonesia.com/content/kenangan-antropolog-kathleen-adams-tentang-toraja/1702542.html|title=Kathleen Adams, Antropolog AS yang Jatuh Hati pada Indonesia|publisher=Voice of America|accessdate=24 November 2013}}</ref> and received her Ph.D. from the [[University of Washington]] in 1988.  

Adams is author of several books, including the award-winning [http://www.uhpress.hawaii.edu/p-4431-9780824830724.aspx Art as Politics: Re-crafting Identities, Tourism and Power in Tana Toraja, Indonesia], co-editor of [http://www.iupress.indiana.edu/product_info.php?products_id=672626 Everyday Life in Southeast Asia], and [https://g.co/kgs/I2lbZK Home and Hegemony: Domestic Service and Identity Politics in South and Southeast Asia], as well as over fifty articles. Her research has been featured on [[National Public Radio]], [[Voice of America]], [[CBC Television|Canadian Broadcasting Corporation]] and in the [[New york Times|New York Times]]. A former [[Fulbright Program|Fulbright]] recipient, Adams has consulted for [[Ministry of Education and Culture (Indonesia)|Indonesia's Ministry of Education and Culture]]/[[UNESCO]] and served as academic adviser for several [[BBC Films|BBC]] and [[National Geographic (U.S. TV channel)|National Geographic]] documentary film and television projects.     

== Education and teaching ==

As an undergraduate, Adams studied anthropology at the [[University of California, Santa Cruz]] (1979), and completed her Ph.D. in 1988 in socio-cultural anthropology at the [[University of Washington]] in Seattle,<ref name=faculty/> with additional graduate training in Indonesian Studies at [[Cornell University]] and [[Universitas Kristen Satya Wacana]] (Salatiga, [[Indonesia]]), and [[Museology|Museum Studies]] at the [[Univ. of Washington]].

Currently Professor of Anthropology at [[Loyola University Chicago]] and Adjunct Curator at the [[Field Museum of Natural History]].<ref name=faculty>{{cite web|title=Dr. Kathleen M. Adams: Loyola University Chicago|url=http://www.luc.edu/anthropology/faculty/adams.shtml|accessdate=12 November 2013}}</ref>
Adams previously taught at [[Beloit College]] in Wisconsin (1988-1993), where she held the Mouat Family Endowed Chair for Junior faculty.<ref>{{cite web|title=Author Biography|url=http://www.amazon.com/Kathleen-M.-Adams/e/B001HP465W/ref=ntt_athr_dp_pel_pop_1|accessdate=12 November 2013}}</ref> Adams has been a visiting professor at [[Loyola University Chicago]]'s [[John Felice Rome Center]], [[Ateneo de Manila University]] in the [[Philippines]], [[Al-Farabi University]] in Kazakhstan, the [[National University of Singapore]] and has taught on several of [[University of Virginia]]'s [[Semester at Sea]] voyages.<ref>{{cite web|title=Faculty and Staff {{ndash}} Kathleen Adams|work=[[Semester at Sea]]|url=http://www.semesteratsea.org/faculty-and-staff/kathleen-adams/}}</ref>

== Awards and honors ==

Adams' book on the politics of art and tourism in upland Sulawesi (Indonesia)<ref>{{cite book|last=Adams|first=Kathleen|title=Art as Politics: Re-Crafting Identities, Tourism and Power in Tana Toraja, Indonesia|year=2006|publisher=University of Hawaii Press|location=Honolulu|isbn=978-0-8248-3072-4|pages=304 pp.|url=http://www.uhpress.hawaii.edu/p-9780824830724.aspx}}</ref> won the [[Alpha Sigma Nu]] award as the best social science book published in 2007–2009 by faculty at Jesuit institutions.<ref>{{cite web|title=Past Winners of Alpha Sigma Nu Awards|url=http://www.alphasigmanu.org/images/uploads/documents/Past_Winners_of_Book_Awards_2013.pdf|accessdate=12 November 2013|page=3}}</ref> A past Fulbright awardee (1984–85) and [http://www.fas.nus.edu.sg/visit/vss.html Isaac Manasseh Meyer Fellowship] (1999) recipient, Adams' research has also been supported by various fellowships from scholarly organizations such as the [[American Philosophical Society]]. In 2016 she received Loyola University's 2016 Sujack Master Researcher Award.<ref>{{cite web|url=http://www.luc.edu/cas/thesujackawards/pastrecipients/|title=PAst REcipients Loyola University Chicago Sujack Award|accessdate=12 November 2013}}</ref> Adams' has also received teaching awards, including Loyola University Chicago's 2007 Sujack Award for Teaching Excellence, and in 2012 she was recognized by ''[[Princeton Review]]'' as one of the "300 best professors" in the US and Canada.<ref>{{cite web|title=Best Professors Name|url=http://www.princetonreview.com/uploadedFiles/Sitemap/Home_Page/Rankings/Best_Professors/BestProfessors_Name.pdf|work=THe Princeton Review Best 300 Professors|publisher=Princeton Review|accessdate=12 November 2013|author=The Princeton Review|page=1|year=2012}}</ref>

== Significant publications ==

Adams has published several books and over fifty articles on island Southeast Asia, the politics of art, domestic work, identity dynamics, museums, and the anthropology of tourism. Some of her most significant works include the following:

* 2015 "[https://www.cambridge.org/core/journals/trans-trans-regional-and-national-studies-of-southeast-asia/article/families-funerals-and-facebook-reimagining-and-curating-toraja-kin-in-trans-local-times/7250B2E3885649AFEC38B784DD7FCC8D Families, Funerals and Facebook: Reimag(in)ing and Curating Toraja Kin in Translocal Times].” ''TRaNS: Trans –Regional and –National Studies of Southeast Asia'', 3(2).
* 2015 Guest Editor of [http://onlinelibrary.wiley.com/doi/10.1111/muan.12085/abstract Special Issue "Back to the Future? Emergent Visions for Object-Based Teaching in and Beyond the Classroom.]” ''Museum Anthropology'', Vol 38(2).
* 2015  "Identity, Heritage, and Memorialization: The Toraja Tongkonan of Indonesia.” In G. Riello and A. Gerritsen (eds) ''Writing Material Culture History'', London: Bloomsbury.
* 2012  "[http://www.tandfonline.com/doi/abs/10.1080/02560046.2012.684436 Love American Style and Divorce Toraja Style: Lessons from a Tale of Mutual Reflexivity in Indonesia],” ''Critical Arts'' 26(2).
* 2012	“Ethnographic Methods” for ''Handbook of Research Methods in Tourism: Qualitative and Quantitative Methods'', eds. Larry Dwyer, Alison Gill and Neelu Seertaram.  Northamton, MA & Edward Elgar/Ashgate. Pp. 339-351.
* 2011 ''[http://www.iupress.indiana.edu/product_info.php?products_id=672626 Everyday Life in Southeast Asia]''. [[Indiana University Press]]. (With Kathleen Gillogly).  
* 2011 "[http://www.tandfonline.com/doi/abs/10.1080/19407963.2011.555457 Public Interest Anthropology, Political Market Squares, and Re-scripting Dominance: From Swallows to ‘Race]’ in San Juan Capistrano, CA." ''Journal of Policy Research in Tourism, Leisure and Events'', 3(2):147-169. 
* 2010 "Courting and Consorting with the Global: The Local Politics of an Emerging World Heritage Site in Sulawesi, Indonesia.” In V.T. King, M. Parnwell & M. Hitchcock (eds.) ''Heritage Tourism in Southeast Asia'', NIAS Press & Univ. of Hawai'i Press.
* 2008 "Indonesian Souvenirs as Micro-Monuments to Modernity: Hybridization, Deterritorialization and Commoditization." In ''Tourism in South-East Asia'', Univ. of Hawai'i Press. 
* 2008 “The Janus-Faced Character of Tourism in Cuba: Ideological Continuity and Change.” ''Annals of Tourism Research'', 35(1):27-46. Co-authored with Peter Sanchez.
* 2006	''[http://www.uhpress.hawaii.edu/p-4431-9780824830724.aspx Art as Politics: Re-crafting Identities, Tourism, and Power in Tana Toraja, Indonesia].'' [[University of Hawaii Press]]. (Winner of the 2009 Association of Jesuit Colleges and Universities Alpha Sigma Nu National Book Award, best in Social Sciences 2007-2009)<ref>{{cite journal|journal=Alpha Sigma Nu|date=Summer 2010|title=Alpha Sigma Nu Week {{ndash}} Loyola University Chicago|page=1|url=http://www.alphasigmanu.org/images/uploads/newsletters/summer2010.pdf}}</ref> 
* 2005	“Public Interest Anthropology in Heritage Sites: Writing Culture and Righting Wrongs.” ''International Journal of Heritage Studies'', 11(5):433-439.
* 2005 “Generating Theory, Tourism & “World Heritage” in Indonesia: Ethical Quandaries for Practicing Anthropologists.” ''National Association for the Practice of Anthropology Bulletin'', 23:45-59. Part of a Special Issue on “Anthropological Contributions to Travel & Tourism: Linking Theory with Practice.”
* 2004  “The Genesis of Touristic Imagery: Politics and Poetics in the Creation of a Remote Indonesian Island Destination.”  ''Tourist Studies'', 4(2):115-135.
* 2000 ''[https://www.press.umich.edu/23067/home_and_hegemony Home and Hegemony: Domestic Service and Identity Politics in South and Southeast Asia]''. [[University of Michigan Press]]. (With Sara Dickey).   
* 2000  ''A Changing Indonesia''. Special issue of ''Southeast Asian Journal of Social Science''. Vol. 28, No. 2. (Guest edited, with Maribeth Erb).

==References==
{{Reflist}}

[[University of California, Santa Cruz faculty and alumni]]==External links==
*[http://luc.academia.edu/KathleenAdams Kathleen M. Adams] at Academia.edu

{{DEFAULTSORT:Adams, Kathleen M.}}
[[Category:Living people]]
[[Category:1957 births]]
[[Category:American anthropologists]]
[[Category:American women anthropologists]]
[[Category:University of California, Santa Cruz alumni]]
[[Category:University of Washington alumni]]
[[Category:Beloit College faculty]]
[[Category:Tradition]]
[[Category:Tourism researchers]]
[[Category:People from San Francisco]]
[[Category:Writers about globalization]]
[[Category:Social anthropologists]]
[[Category:Fulbright Scholars]][[Toraja]]