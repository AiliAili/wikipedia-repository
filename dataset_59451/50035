{{DISPLAYTITLE:Gilly (''A Song of Ice and Fire'')}}
{{Infobox character
| name        = Gilly
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| portrayer   = [[Hannah Murray]]<br />(''[[Game of Thrones]]'')
| creator     = [[George R. R. Martin]]
| image       = Gilly-Hannah Murray.jpg
| caption     = [[Hannah Murray]] as Gilly
| first       = '''Novel''': <br />''[[A Clash of Kings]]'' (1998) <br />'''Television''': <br />"[[The North Remembers]]" (2012)
| last        = 
| occupation  =
| title       = 
| alias       = The Rabbit Keeper
| gender      = Female
| family      = 
| spouse      = [[List of A Song of Ice and Fire characters#Craster|Craster]]
| significantother = [[Samwell Tarly]] {{small|(lover)}}
| children    = One son with Craster<br />{{small|(named Sam in the TV series)}}
| relatives   = Craster {{small|(father)}}
| nationality = [[Westeros]]i
}}

'''Gilly''' is a [[fictional character]] in the ''[[A Song of Ice and Fire]]'' series of [[fantasy]] novels by American author [[George R. R. Martin]], and its television adaptation ''[[Game of Thrones]]''.

Introduced in 1998's ''[[A Clash of Kings]]'', she is a [[wildling (character)|wildling]] from the wild lands north of the [[The Wall (A Song of Ice and Fire)|Wall]]. She subsequently appeared in Martin's ''[[A Storm of Swords]],'' ''[[A Feast for Crows]]'', ''[[A Dance with Dragons]]'' and will appear in the upcoming novel ''[[The Winds of Winter]]''.

Gilly is portrayed by [[Hannah Murray]] in the [[HBO]] television adaptation.<ref name=HBOCast>{{cite web | url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/gilly/bio/gilly.html |title=''Game of Thrones'' Cast and Crew: Gilly played by Hannah Murray |publisher=[[HBO]] | accessdate=December 8, 2016}}</ref><ref>{{cite web|url=http://www.hbo.com/game-of-thrones/index.html#/game-of-thrones/cast-and-crew/index.html|title=The Official Website for the HBO Series Game of Thrones - Season 4|work=HBO|accessdate=December 8, 2016}}</ref><ref>{{cite web|url=http://grrm.livejournal.com/164794.html|title=From HBO|publisher=|accessdate=December 8, 2016}}</ref><ref>{{cite web|url=https://www.buzzfeed.com/samstryker/gilly-from-got-is-actually-totally-hot|title=The Actress Who Plays Gilly On “Game Of Thrones” Is Actually A Total Babe|work=Buzzfeed|accessdate=December 8, 2016}}</ref>

==Character description==
Gilly is a wildling girl, daughter and wife of Craster. She is in her late teens, has brown eyes and is estimated to be around 15 or 16 years-old when she first appears in the novel.{{Sfn|''A Clash of Kings''|loc=Chapter 23: Jon III}} Gilly is not a [[Narration#Third-person|point of view]] character in the novels, so her actions are witnessed and interpreted through the eyes of other people, such as [[Samwell Tarly]] and [[Jon Snow (character)|Jon Snow]].<ref>{{cite web|url=http://viewers-guide.hbo.com/map/special/areas-of-control/4/|title=Game of Thrones Viewer's Guide|publisher=HBO|accessdate=December 8, 2016}}</ref>

==Storylines==

===In the books===
On [[Jeor Mormont]]'s ranging, the Night's Watch stops at Craster's Keep, where Gilly encounters and befriends [[Samwell Tarly]].{{Sfn|''A Clash of Kings''|loc=Chapter 23: Jon III}} After the Night's Watch regroups at Craster's Keep, Gilly gives birth to a son. Craster is killed before he can sacrifice the child, and in the confusion Gilly flees south with Samwell. In A Feast for Crows she is sent South aboard a ship to Oldtown with Samwell, ostensibly with her child; in truth Jon Snow swapped her child with that of Mance Rayder, to spare the innocent child from Melisandre's flames on account of his king's blood.{{Sfn|''A Feast for Crows''|loc=Chapter 5: Samwell I}}

===In the show===
====Season 2====
A young wildling girl who lives north of the Wall, Gilly is one of many daughters of Craster, a wildling who takes all his daughters as wives once they grow up into women. She has a son with her father Craster. Samwell falls for her and becomes protective of her.<ref>{{cite web|url=http://www.ign.com/articles/2012/04/02/game-of-thrones-the-north-remembers-review |title=Game of Thrones: 'The North Remembers' Review |publisher=[[IGN]] |date=April 2, 2012 |accessdate=December 8, 2016}}</ref>

====Season 3====
After Craster is killed and Commander Mormont's rangers turn on each other, Samwell runs with Gilly and her son to Castle Black. Along their journey, Gilly becomes fascinated with Samwell over his knowledge and his bravery of defending her son from a White Walker. After the three of them manage to reach Castle Black, Maester Aemon allows Gilly and her son to stay with them. In gratitude for Samwell helping them, Gilly names her son after Sam.<ref>{{cite web|url=http://ca.ign.com/articles/2013/04/22/game-of-thrones-and-now-his-watch-is-ended-review-2|title=Game of Thrones: "And Now His Watch is Ended" Review|last=Fowler|first=Matt|publisher=[[IGN]]|date=April 21, 2013|accessdate=December 8, 2016}}</ref>

====Season 4====
Gilly settles in a nearby inn close to Castle Black, with Sam's assistance. The Inn is later attacked by wildlings, but Gilly hides with her son. They are found by Ygritte, who spares them. They make it back to Castle Black safely, where Sam hides them in the food storage, and kisses Gilly for the first time in case he dies. She is also visibly surprised when Janos Slynt hides in the food storage as well. Gilly remains unharmed throughout the battle, and reunites with Sam in the aftermath.<ref>{{cite web |url=http://tvrecaps.ew.com/recap/game-of-thrones-watchers-wall/ |title=Game of Thrones recap: Where the Wildlings Are |work=Entertainment Weekly |last=Hibberd |first=James |date=June 8, 2014 |accessdate=December 8, 2016}}</ref>

====Season 5====
She expresses concern of being sent away or executed if Ser Alliser Thorne becomes the new Lord Commander. She is later present at Mance Rayder's execution. Gilly has begun to learn letters from Princess Shireen Baratheon, with Samwell watching over the two. Gilly also reveals to Shireen that she had sisters that also had Greyscale, but they were quarantined away from the other women at Craster's Keep and eventually succumbed to the disease. After Maester Aemon's death, during Jon's absence in Hardhome, she is almost sexually assaulted by two members of the Watch, but is saved by Sam and Ghost, after which she willingly makes love to him. She and her baby later leave Castle Black for Oldtown with Sam on Jon Snow's orders.<ref>{{cite web |url=http://www.ign.com/articles/2015/06/15/game-of-thrones-mothers-mercy-review |title=Game of Thrones: "Mother's Mercy" Review |work=IGN |last=Fowler |first=Matt |date=June 15, 2015 |accessdate=December 8, 2016}}</ref>

====Season 6====
Along the way, they stop at Horn Hill, where Sam initially intends to leave Gilly and the baby with his family, but despite Randyll Tarly allowing them to stay, Sam decides to take them with him to the Citadel in Oldtown. In the sixth-season finale, they arrive.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ign.com/articles/2016/06/27/game-of-thrones-the-winds-of-winter-review |archivedate=August 17, 2016 |url=http://www.ign.com/articles/2016/06/27/game-of-thrones-the-winds-of-winter-review |title=Game of Thrones: "The Winds of Winter" Review |publisher=[[IGN]] |accessdate=December 8, 2016 |date=June 27, 2016 |author=Fowler, Matt|deadurl=no}}</ref>

==TV adaptation==
[[File:Hannah Murray - IFFR 2015-1.jpg|thumb|right|upright|[[Hannah Murray]] plays the role of Gilly in the [[Game of Thrones|television series]].]]
Gilly is played by the British actress [[Hannah Murray]] in the television adaption of the series of books.<ref name="Time">{{cite web|url=http://time.com/3827082/hannah-murray-bridgend-game-of-thrones-gilly/|title=Hannah Murray on Bridgend and Her Wish for Gilly on Game of Thrones|work=Time|first=Sarah |last=Begley|accessdate=December 8, 2016 |date=April 20, 2015}}</ref>

In an interview, Murray spoke about Gilly's relationship with Sam. Murray stated, "A big thing that connects them is having horrible fathers. We haven't seen Sam's father, but we know about that in a different way from Gilly, Sam was abused as well."<ref name="makingofgameofthrones">{{cite web |url=http://www.makinggameofthrones.com/production-diary/2014/6/8/hannah-murray-discusses-gilly-and-sams-big-moment |title=Hannah Murray Discusses Gilly and Sam’s Big Moment |publisher=[[IGN]] |accessdate=December 8, 2016 |date=June 8, 2014 |first=Katie |last=M. Lucas}}</ref> She continued, "She knows that he loves her and she definitely loves him, but she's not someone who can process her emotions very well or has a sophisticated language for them. I've thought for a long time that they are like this little, unconventional family. He loves the baby as much as she does. John has said, 'Sam couldn't love it any more if it was his own.'"<ref name="makingofgameofthrones"/>

In another interview, Murray spoke about her fellow castmember John, who plays her lover Sam and also about other castmembers on the show, saying "I’m really good friends with John, and I also have friends who I was friends with before we started the show. I did [[Skins (UK TV series)|Skins]] with [[Joe Dempsie]]. My friend [[Jacob Anderson]] plays Grey Worm — we used to live together. We shared a flat with another actor. I think we were still living together when Jacob got the part, and I was so happy he was going to join."<ref name="Time"/>

===Recognition and awards===
{| class="wikitable sortable"
|-
! scope="col" |Year
! scope="col" |Award
! scope="col" |Category
! scope="col" |Result
! scope="col" |{{abbr|Ref.|References}}
|-
| 2014
|[[Screen Actors Guild Award]]
|[[Screen Actors Guild Award for Outstanding Performance by an Ensemble in a Drama Series|Outstanding Performance by an Ensemble in a Drama Series]]
|{{nom}}
|<ref>{{cite web|url=http://www.deadline.com/2013/12/sag-awards-2013-nominations-full-list/ | title=SAG Awards Nominations: ''12 Years A Slave'' And ''Breaking Bad'' Lead Way |publisher=Deadline.com | date=December 11, 2013| accessdate=December 8, 2016}}</ref><ref>{{cite web |url=http://www.deadline.com/2014/01/sag-awards-2014-stunt-winners-lone-survivor-game-of-thrones/ | title=SAG Awards: ''Lone Survivor'', ''Game Of Thrones'' Win Stunt Honors |publisher=Deadline.com | date=January 18, 2014 |accessdate=December 8, 2016}}</ref>
|-
| 2016
|Screen Actors Guild Award 
|Outstanding Performance by an Ensemble in a Drama Series
|{{nom}}
|<ref>{{cite web |url=http://variety.com/2015/film/awards/sag-award-nominations-2016-nominees-full-list-1201657169/ |title=SAG Awards Nominations: Complete List |work=[[Variety (magazine)|Variety]] |date=December 9, 2015 |accessdate=December 8, 2016}}</ref>
|}

== References ==
{{Reflist|30em}}

{{ASOIAF}}

[[Category:A Song of Ice and Fire characters]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional characters introduced in 1998]]