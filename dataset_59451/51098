{{Italic title}}
{{Infobox journal
| title         = Deutsche Mathematik
| cover         = DeuMath1936_1.jpg
| editor        = Theodor Vahlen
| discipline    = Mathematics, Nazi propaganda
| peer-reviewed = 
| language      = German
| former_names  = 
| abbreviation  = 
| publisher     = Leipzig: S. Hirzel
| country       = Germany
| frequency     = bimonthly (delays in vol.6-7)
| history       = 1.1936 &mdash; 3.1942<ref>[http://portal.ub.tu-berlin.de/primo_library/libweb/action/dlDisplay.do?vid=TUB&docId=tub_aleph001828958&fn=permalink Record] at [[Technical University Berlin]] library</ref>
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = 
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 
| LCCN          = 
| CODEN         = 
| boxwidth      = 
}}

'''''Deutsche Mathematik''''' (German mathematics) was a mathematics journal founded by [[Ludwig Bieberbach]] and [[Theodor Vahlen]] in 1936. Vahlen was publisher on behalf of the [[German Research Foundation]] (DFG), Bieberbach was chief editor. Permanent editors were {{Interlanguage link multi|Fritz Kubach|de}}, [[Erich Schönhardt]], [[Werner Weber (mathematician)|Werner Weber]]<sup>([[:de:Werner Weber (Mathematiker)|de]])</sup>  (all volumes), [[Ernst August Wei&szlig;]] (vol. 1-6), {{Interlanguage link multi|Karl Dörge|de}}, [[Wilhelm Süß]] (1-5), [[Günther Schulz]] Berlin, {{Interlanguage link multi|Erhard Tornier|de}} (1-4), [[Georg Feigl]], [[Gerhard Kowalewski]] (2-6), {{Interlanguage link multi|Maximilian Krafft|de}}, [[Willi Rinow]], {{Interlanguage link multi|Max Zacharias|de}} (2-5), and [[Oswald Teichmüller]] (vol. 3-7).<ref>[[commons:Category:Deutsche Mathematik (journal)|Volume tables of contents]]</ref> 
In Feb 1936, the journal was declared the official organ of the [[German Student Union]] (DSt) by its ''Reichsf&uuml;hrer''; all local DSt mathematics departments were requested to subscribe and to actively contribute.<ref>Vol.1, issue 2, p.122-123, Communication of the DSt's ''Reichsfachabteilung'' mathematics by Fritz Kubach</ref>
 
'''''Deutsche Mathematik''''' is also the name of a movement closely associated with the journal whose aim was to promote "German mathematics" and eliminate "Jewish influence", similar to the [[Deutsche Physik]] movement. As well as articles on mathematics, the journal also published propaganda articles giving the Nazi viewpoint on the relation between mathematics and race (though these political articles mostly disappeared after the first two volumes). As a result of this many mathematics libraries outside Germany did not subscribe to it, so copies of the journal can be hard to find. This caused some problems in [[Teichmüller theory]], as [[Oswald Teichmüller]] published several of his foundational papers in the journal.

==References==
{{Reflist}}

*{{Citation | last1=Mehrtens | first1=Herbert | editor1-last=Phillips | editor1-first=Esther R. | title=Studies in the history of mathematics | url=https://books.google.com/books?id=jlYPAQAAMAAJ | publisher=Math. Assoc. America | location=Washington, DC | series=MAA Stud. Math. | isbn=978-0-88385-128-9 | mr=913104 | year=1987 | volume=26 | chapter=Ludwig Bieberbach and "Deutsche Mathematik" | pages=195–241}}
*{{Citation|last=M. A. H. N.|url=http://www.nature.com/nature/journal/v137/n3467/pdf/137596a0.pdf|year=1936|title=Deutsche Mathematik|journal=Nature|pages=596–597|volume=137|issue=3467|id=Book review|doi=10.1038/137596a0}}
*{{Citation | last1=Segal | first1=Sanford L. | title=Mathematicians under the Nazis | url=https://books.google.com/books?id=Xa8CNCyohBQC | publisher=[[Princeton University Press]] | isbn=978-0-691-00451-8 | mr=1991149 | year=2003 | chapter=Chapter seven: Ludwig Bieberbach and Deutsche Mathematik | pages=334–418}}
* The title page, the table of contents, and some article pages of the journal's volume 1, issue 2 (1936) are linked from the blog [http://guestblog.scientopia.org/2011/09/19/mathematicians-are-human-beings ''Mathematicians are human beings''] (scientopia.org, 19 Sep 2011).
* {{cite report | editor=Moritz Epple and Volker Remmert and Norbert Schappacher | title=History of Mathematics in Germany, 1920&mdash;1960 | institution=Mathematisches Forschungsinstitut Oberwolfach | type=Report | number=03/2010 | pages=109&mdash;140 | url=http://www.mfo.de/document/1003a/OWR_2010_03.pdf | year=2010 }} In particular: Philipp Kranz, ''The journal "Deutsche Mathematik" (1936-1942/44)'', p.&nbsp;132&mdash;134.

{{commons|Category:Deutsche Mathematik (journal)|Deutsche Mathematik|position=left}}

[[Category:Mathematics journals]]
[[Category:Politics of science]]
[[Category:Science in Nazi Germany]]