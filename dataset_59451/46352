{{advert|date=March 2014}}
{{Infobox company
| name             = BackOffice Associates, LLC
| logo             = [[File:BackOffice Associates Logo, Copyright Permission to Use.png|200px|BackOffice Associates logo]]
| type             = Private
| industry         = [[Software industry|Software Industry]]
| founded         = 1996
| location_city        = Hyannis, MA
| location_country        = US
| area_served      = Worldwide
| key_people       = David Booth, CEO<br/>Krish Datta, CEO APJ-ME<br/>Clive Bellmore, CEO EUAF<br/>Alex Berry, COO<br/>Rex Ahlstrom, CSO<br/>Matthew Rowles, CFO
| products   = Data Stewardship Platform, dspCloud, Data Quality as a Service, Data Audit, SAP Accelerators, ERP<sup>2</sup>, DBMoto 
| services         = Data Quality, Data Migration, Information Governance, Enterprise Performance Management, Mass Data Uploads, Change Data Capture, Real-time Data Replication, Mobile Data Governance
| num_employees      = 650+
| homepage         = [http://www.boaweb.com www.boaweb.com]
}}

'''BackOffice Associates''', LLC is a Hyannis, MA based software and services company focused on [[data migration]], [[information governance]], [[master data management]] (MDM) and [[data quality]] for enterprise systems. The company has global offices in Canada, Mexico, United Kingdom, Singapore, Dubai, India and Australia with development offices and corporate headquarters based in the United States.

== History ==

BackOffice Associates, LLC was founded in 1996 by [[Thomas R. Kennedy, Jr.|Tom Kennedy]] and Trish Kennedy,<ref>[http://www.bizjournals.com/boston/blog/mass-high-tech/2008/03/backoffice-associates-kennedys-are-a-cape.html?page=all “BackOffice Associates' Kennedy's are a Cape coding couple,”] By: Jay Rizoli, Boston Business Journal, March 7, 2008</ref> and delivers data migration and data governance [[solution]]s with a focus on data quality for enterprise systems, partnering with large software companies like [[Oracle Corporation|Oracle]] and [[SAP AG|SAP]].<ref name="Boston Business Journal">{{cite web|last=Alspach|first=Kyle|title=BackOffice Associates steps up partnership with SAP|url=http://www.bizjournals.com/boston/blog/startups/2012/12/backoffice-associates-sap-partnership.html|publisher=Boston Business Journal|date=December 2012}}</ref>  Over the years, BackOffice Associates released several [[solution]]s and services, including the Data Stewardship Platform (DSP),<ref name="Enterprise Apps Today">{{cite web|last=Sarrel|first=Matt|title=Review: Data Stewardship Platform Eases ERP Migration|url=http://www.enterpriseappstoday.com/data-management/review-data-stewardship-platform-eases-erp-migration.html|publisher=Enterprise Apps Today|date=October 2013}}</ref> allowing customers to see the business relevance of their data. Employing hundreds of on-site consultants, BackOffice Associates works with Fortune ranked customers worldwide, with locations in Canada, Mexico, United Kingdom, Singapore, Dubai, India and Australia, and ranked in [[Inc. 5000]] list of growing private companies.<ref>{{cite web|last=Inc. 5000|title=Company Profile|url=http://www.inc.com/profile/backoffice-associates|publisher=Inc.|year=2011}}</ref>
In expansion of their product and [[solution]] offerings to [[Enterprise resource planning|ERP]] customers, BackOffice Associates acquired Quadrate, a software development company with tools for data loading, extraction and reporting for [[SAP ERP]] systems in February 2010. In March 2010, they acquired CPM Consulting, a [[SAP AG|SAP]] partner with business consulting and technology projects in [[Enterprise project management|Enterprise Project Management]]. Shortly thereafter, they acquired HiT Software, a California-based development company focused on data integration, change data capture, data replication, XML transformation and [[IBM]] DB2 connectivity software.<ref name="HiT Acquisition">{{cite web|last=Woodie |first=Alex |title=HiT Software Acquired by BackOffice Associates |url=http://www.itjungle.com/fhs/fhs051110-story09.html |publisher=IT Jungle |date=May 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20130309004343/http://www.itjungle.com/fhs/fhs051110-story09.html |archivedate=2013-03-09 |df= }}</ref>  BackOffice Associates acquired Headwall Software, Inc., a Canada-based software development company in July 2012<ref name="Headwall Acquisition">{{cite web|last=Gold |first=Robert |title=BackOffice buys Canadian business |url=http://www.capecodonline.com/apps/pbcs.dll/article?AID=/20120712/BIZ/207120306 |publisher=Cape Cod Times |date=July 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20140306220036/http://www.capecodonline.com/apps/pbcs.dll/article?AID=/20120712/BIZ/207120306 |archivedate=2014-03-06 |df= }}</ref> and in December 2013 they acquired ENTOTA, the UK-based SAP data services consultancy company further strengthening their SAP partnership.<ref name="ENTOTA Acquisition">{{cite web|last=Savvas|first=Antony|title=BackOffice buys UK-based SAP specialist to grow big data business|url=http://www.computerworlduk.com/news/it-business/3494135/backoffice-buys-uk-based-sap-specialist-to-grow-big-data-business/|publisher=Computerworld UK|date=December 2013}}</ref>
Backed by [[Goldman Sachs]], the company bought out its founders in 2012.<ref name=IPO>{{cite web|last=Kharif|first=Olga|title=BackOffice Associates in Talks With Goldman Sachs for an IPO|url=https://www.bloomberg.com/news/2012-05-14/backoffice-associates-in-talks-with-goldman-sachs-for-an-ipo.html|publisher=Bloomberg|date=May 2012}}</ref>

== Acquisitions ==

* In December 2013, BackOffice Associates acquired ENTOTA, UK-based SAP data services consultancy.<ref name="ENTOTA Acquisition" /> 
* In July 2012, BackOffice Associates acquired Headwall Software Inc., a Canada-based custom mobile [[solution]]s company.<ref name="Headwall Acquisition" /> 
* In April 2010, BackOffice Associates acquired HiT Software, a California-based company specializing in data integration, change data capture, data replication, XML transformation and [[IBM]] DB2 connectivity software.<ref name="HiT Acquisition" /> 
* In March 2010, BackOffice Associates acquired CPM Consulting, a [[SAP AG|SAP]] partner with business consulting and technology projects in [[Enterprise Performance Management|Enterprise Project Management]].
* In February 2010, BackOffice Associates acquired Quadrate, a software development company for their ERP<sup>2</sup> and Qubed products.

== Global Expansion ==

In 2014, BackOffice Associates made several expansions by opening new offices in Richmond, Virginia and over seas in Switzerland, Dubai, Bangalore, Hyderabad and Melbourne, Australia. BackOffice Associates is still headquartered in South Harwich, MA. In 2015, BackOffice continued the expansion by opening a new office at Barcelona, Spain.
== Partnerships ==

BackOffice Associates' largest partnership is with [[SAP SE]]. Through a global reseller agreement in 2014, SAP now resells two BackOffice solutions as SAP-branded offerings for data migration and data governance projects.<ref name=DBTA>{{cite web|title=BackOffice Associates Enters Global Reseller Agreement with SAP|url=http://www.dbta.com/Editorial/News-Flashes/BackOffice-Associates-Enters-Global-Reseller-Agreement-with-SAP-94621.aspx|publisher=Database Trends and Applications}}</ref> BackOffice Associates also has strategic partnerships with [[Oracle Corporation]], [[Microsoft]], [[IBM]], [[Google Cloud Platform]], [[Dassault Systèmes]] and more.

== References ==
{{reflist}}

[[Category:1996 establishments in Massachusetts]]
[[Category:Companies based in Harwich, Massachusetts]]
[[Category:Development software companies]]
[[Category:ERP software companies]]
[[Category:Software companies based in Massachusetts]]
[[Category:Software companies established in 1996]]
[[Category:Data companies]]
[[Category:Data quality companies]]