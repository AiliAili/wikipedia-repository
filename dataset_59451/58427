{{Infobox Journal
| cover =
| discipline = [[Political science]]
| abbreviation = J. Polit.
| publisher = [[University of Chicago Press]]
| country = [[United States]]
| editor = Jeffery A. Jenkins
| frequency = Quarterly
| history = 1939-present
| impact = 1.840
| impact-year = 2015
| openaccess =
| website = http://www.thejournalofpolitics.org/
| link1 = http://journals.uchicago.edu/jop
| link1-name = Online access
| link2 = http://www.journals.uchicago.edu/loi/jop
| link2-name = Online archive
| ISSN = 0022-3816
| eISSN = 1468-2508
| OCLC = 38309773
| JSTOR = 00223816
| LCCN = 41016606
| RSS = http://journals.cambridge.org/data/rss/feed_JOP_rss_2.0.xml
}}
'''''The Journal of Politics''''' is a [[Peer review|peer-reviewed]] [[academic journal]] of [[political science]] established in 1939 and published quarterly (February, May, August and November) by [[University of Chicago Press]] on behalf of the [[Southern Political Science Association]].

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.840, ranking it 24th out of 163 journals in the category "Political Science".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.thejournalofpolitics.org/}}
* [http://www.journals.uchicago.edu/jop Online Access]

{{DEFAULTSORT:Journal Of Politics, The}}
[[Category:Political science journals]]
[[Category:Publications established in 1939]]
[[Category:University of Chicago Press academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]


{{poli-journal-stub}}