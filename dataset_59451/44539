{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Ralph the Heir
| title_orig    = 
| translator    = 
| image         = Ralph the Heir big loaf.jpg
| caption = [[F. A. Fraser]] illustration: Sir Thomas campaigning in Percycross
| author        = [[Anthony Trollope]]
| illustrator   = 
| cover_artist  = 
| country       = United Kingdom
| language      = English
| series        = 
| genre         = 
| publisher     = [[Hurst and Blackett]]
| pub_date      = January 1870 to July 1871; 3 volumes, 1871
| english_pub_date =
| media_type    = Serialized; Print
| pages         = 
| isbn = 0-486-23642-0
| isbn_note = (Dover paperback edition, 1978)
| preceded_by   =
| followed_by   =
}}
'''''Ralph the Heir''''' is a novel by [[Anthony Trollope]], originally published in 1871.  Although Trollope described it as "one of the worst novels I have written",<ref name=autobio19>Trollope, Anthony (1883).  [http://ebooks.adelaide.edu.au/t/trollope/anthony/autobiography/chapter19.html ''An Autobiography'', chapter 19.]  Retrieved 2010-05-19.</ref>
it was well received by contemporary critics.<ref>[https://books.google.com/books?id=ZvV8GXvDcToC&lpg=PA346&ots=8Z26A2iieS&dq=smalley%20trollope%20%22critical%20heritage%22%20%22ralph%20the%20heir%22&pg=PP1#v=onepage&q&f=false ''Trollope: The Critical Heritage''.]  Donald Smalley, ed.  Chatham: W & J Mackay & Co. Ltd. 1969. [https://books.google.com/books?id=ZvV8GXvDcToC&lpg=PA346&ots=8Z26A2iieS&dq=smalley%20trollope%20%22critical%20heritage%22%20%22ralph%20the%20heir%22&pg=PA346#v=onepage&q&f=false p. 346.]  Retrieved 2010-05-19.</ref>
More recently, readers have found it noteworthy for its account of a corrupt Parliamentary election,<ref>Briggs, Asa (1955).  [https://books.google.com/books?id=GesH9eVJcecC&lpg=PA104&ots=sHx1D9_zf2&dq=percycross%20eatanswill&pg=PP1#v=onepage&q&f=false ''Victorian People: A Reassessment of Persons and Themes, 1851-67''.]  University of Chicago Press.  1972 paperback edition.  [https://books.google.com/books?id=GesH9eVJcecC&lpg=PA104&ots=sHx1D9_zf2&dq=percycross%20eatanswill&pg=PA104#v=onepage&q&f=false p. 104.]  Retrieved 2010-05-19.</ref>
an account based closely on Trollope's own experience as a candidate.<ref name=autobio19 />

==Plot summary==

The title character is Ralph Newton, the nephew of [[Squire#Village leader|Squire]] Gregory Newton of Newton Priory.  The squire has never married; he has an [[Legitimacy (law)|illegitimate]] son, also named Ralph Newton, whom he loves dearly.  However, the estate is [[Fee tail|entailed]], and after his death will go to his nephew Ralph; he cannot leave it to his natural son.

Ralph the heir is a spendthrift, and has run himself deep into debt.  There are two ways in which he can extricate himself: by raising money on his future interest in the Newton estate, or by marrying Polly Neefit, the daughter of a wealthy breeches-maker who is one of his major creditors.  Neither choice is a good one for him: the first might lead to the estate's being seized by his creditors upon the old squire's death; the second would mean allying himself to a family of a much lower social class, thus putting his own social standing at risk.

The squire, anxious to obtain full possession of the estate so that he can pass it to his son, offers to buy the heir's [[Reversion (law)|reversion]].  Ralph vacillates, hesitatingly proposes to and is rejected twice by Polly Neefit, and eventually accepts his uncle's offer.  However, before the transaction can be completed, the squire is killed in a [[Fox hunting|hunting]] accident and his nephew comes into full possession of the property and its large income.

Now safe from his creditors, the new squire is nevertheless harassed by Polly Neefit's father, who threatens him with legal action and embarrassing publicity if he does not continue seeking his daughter's hand.  The matter is eventually resolved by Polly, who accepts the oft-repeated proposals of Ontario Moggs, son of a prosperous bootmaker, and induces her father to consent to the marriage despite his preference for the squire.  In the meantime, Ralph the squire has proposed to and been rejected by Mary Bonner, the beautiful niece and ward of Sir Thomas Underwood; soon after this, she accepts an offer of marriage from the illegitimate Ralph.

The novel also describes a [[House of Commons of the United Kingdom|Parliamentary]] election in the fictional [[Parliamentary borough|borough]] of Percycross, in which Sir Thomas, a [[Conservative Party (UK)|Conservative]], and Moggs, a [[Radicals (UK)|Radical]], are two of the four candidates for the two available seats.  Both are eager that the election be conducted fairly and honestly.  The other two candidates, one a Conservative and one a [[Liberal Party (UK)|Liberal]], are the incumbents; they see nothing wrong with the buying and selling of votes that has been traditional at Percycross.  Sir Thomas and his fellow Conservative win the election, but it is annulled on petition, and the borough is disfranchised by Parliament because of its pervasive corruption.

==Development history==

===Beverley campaign===

Trollope had long dreamt of taking a seat in the House of Commons.<ref name=autobeverley>Trollope (1883), [http://ebooks.adelaide.edu.au/t/trollope/anthony/autobiography/chapter16.html chapter 16.]  Retrieved 2010-05-21.</ref>
As a civil servant, however, he was ineligible for such a position.  His resignation from the Post Office in 1867
removed this disability, and he almost immediately began seeking a seat for which he might run.<ref name=super>Super, R. H. (1988).  [https://books.google.com/books?id=KysOhYFtAEcC&lpg=PA253&ots=KiwQBxpNb1&dq=trollope%20beverley&pg=PP1#v=onepage&q&f=false ''The Chronicler of Barsetshire''.]  University of Michigan Press.  [https://books.google.com/books?id=KysOhYFtAEcC&lpg=PA253&ots=KiwQBxpNb1&dq=trollope%20beverley&pg=PA251#v=onepage&q&f=false pp. 251-5.]  Retrieved 2010-05-19.</ref>
In 1868, he agreed to stand as a Liberal candidate in the borough of [[Beverley (UK Parliament constituency)|Beverley]], in the [[East Riding of Yorkshire]].<ref name=autobeverley />

Party leaders apparently took advantage of Trollope's eagerness to run and willingness to spend money on a campaign.<ref name=autobeverley />
Beverley had a long history of vote-buying and of intimidation by employers and others.  Every election since 1857 had been followed by a petition alleging corruption, and it was estimated that 300 of the 1100 voters in 1868 would sell their votes.<ref name=bevhist>[http://www.british-history.ac.uk/report.aspx?compid=36433 Modern Beverley: Political and Social History, 1835-1918.]  [http://www.british-history.ac.uk/Default.aspx British History Online.]  Retrieved 2010-05-20.</ref>
The task of a Liberal candidate was not to win the election, but to give the Conservative candidates an opportunity to display overt corruption, which could then be used to disqualify them.<ref name=super />

Trollope described his period of campaigning in Beverley as "the most wretched fortnight of my manhood".<ref name=autobeverley />
He spent a total of £400 on his campaign.<ref name=autobeverley />
The election was held 17 November 1868; the novelist finished last of four candidates, with the victory going to the two Conservatives.<ref name=super />
A petition was filed, and a [[Royal Commission]] investigated the circumstances of the election; its findings of extensive and widespread corruption drew nationwide attention, and led to the disfranchisement of the borough in 1870.<ref name=bevhist />

Trollope wrote ''Ralph the Heir'' between 4 April and 7 August 1869,<ref name=moody>Moody, Ellen.  [http://www.jimandellen.org/trollope/trollope.writing.chron.html "A Chronology of Anthony Trollope's Writing Life".]  [http://www.jimandellen.org/ellen/emhome.htm Ellen Moody's Website: Mostly on English and Continental and Womens' Literature].  Retrieved 2010-05-17.</ref>
less than a year after the Beverley campaign.  Although there are differences of detail between the fictional election at Percycross and the historical one at Beverley, the one was clearly based on the other.  In his 1883 autobiography, Trollope wrote: "Percycross and Beverley were, of course, one and the same place."<ref name=autobeverley />

===Publication history===

''Ralph the Heir'' was initially published as a supplement to ''[[St. Paul's Magazine]]'' in monthly numbers from January 1870 to July 1871.  In April 1871, it was published in three volumes by [[Hurst and Blackett]].<ref name=moody />
Also in 1871, a one-volume edition was published by Strahan and Co.;<ref>Super (1988). [https://books.google.com/books?id=KysOhYFtAEcC&lpg=PP1&dq=chronicler%20of%20barsetshire&pg=PA277#v=onepage&q&f=false p. 277.]  Retrieved 2010-05-20.</ref>
an English-language edition was released by [[Tauchnitz]] of [[Leipzig]]; an American edition was issued by [[Harper (publisher)|Harper]]; and a Russian translation, ''Naslednik Ralph'', was published in [[Saint Petersburg|St. Petersburg]].  In 1872, the novel was published in Danish as ''Arvingden Ralph''; in 1874, a Swedish translation, ''Ralph'', was released in [[Stockholm]]<ref name=tingay>Tingay, Lance O (1985).  ''The Trollope Collector''.  London: The Silverbridge Press.  p. 33.</ref>

More recently, editions of the novel have been released by [[Dover Publications|Dover]] in 1978, by [[Oxford University Press]] in 1990, and by the Trollope Society in 1996.<ref>Moody, Ellen.  [http://www.jimandellen.org/trollope/general.biblio.html "Trollope's Singletons".]  [http://www.jimandellen.org/ellen/emhome.htm Ellen Moody's Website: Mostly on English and Continental and Womens' Literature].  Retrieved 2010-05-21.</ref>

Trollope received a total of £2500 for the novel: the same amount that he had received for ''[[The Vicar of Bullhampton]]'' the previous year, and that he received for ''[[The Eustace Diamonds]]'' two years later.<ref>Trollope (1883), [http://ebooks.adelaide.edu.au/t/trollope/anthony/autobiography/chapter20.html chapter 20].  Retrieved 2010-05-18.</ref>

==Adaptations==

[[Charles Reade]] adapted the plot of ''Ralph the Heir'' for the stage under the name ''Shilly-Shally''.  The play ran for a month in 1872 at the Gaiety Theatre in London, with Trollope and Reade listed as the authors.<ref>Super (1988).  [https://books.google.com/books?id=KysOhYFtAEcC&lpg=PP1&dq=chronicler%20of%20barsetshire&pg=PA309#v=onepage&q&f=false pp. 308-9.]  Retrieved 2010-05-20.</ref>
Trollope, who at that time was travelling in Australia, complained that his name and his plot had been used without his knowledge or consent; however, copyright law at the time gave an author no recourse in such a case.<ref>Letter to the editor of the ''Daily Telegraph'', 6 August 1872.  ''The Letters of Anthony Trollope''.  N. John Hall, ed.  Stanford University Press: 1983.  [https://books.google.com/books?id=CYCaAAAAIAAJ&lpg=PA563&ots=rsVzjKJiVE&dq=%22ralph%20the%20heir%22%20reade%20%22shilly%20shally%22&pg=PA563#v=onepage&q&f=false vol. II, p. 563.]  Retrieved 2010-05-20.</ref>

==References==
{{reflist}}

==External links==
*[https://books.google.com/books?id=z20JAAAAQAAJ&ots=fyK1SiiulA&dq=%22ralph%20the%20heir%22&pg=PP7#v=onepage&q&f=false ''Ralph the Heir''] at Google Books
*[http://www.gutenberg.org/etext/25579 ''Ralph the Heir''] at [http://www.gutenberg.org/wiki/Main_Page Project Gutenberg]
* {{librivox book | title=Ralph the Heir | author=Anthony Trollope}}

{{Anthony Trollope}}

[[Category:1871 novels]]
[[Category:Novels adapted into television programs]]
[[Category:Novels by Anthony Trollope]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in British magazines]]
[[Category:Works originally published in literary magazines]]