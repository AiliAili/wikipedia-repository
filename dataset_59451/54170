{{For|other people of the same name|Samuel Davenport (disambiguation)}}
{{Use Australian English|date=April 2011}}
{{Use dmy dates|date=November 2015}}
{{Infobox officeholder
|honorific-prefix   = Sir
|name               = Samuel Davenport
|honorific-suffix   = {{post-nominals|country=AUS|KCMG}}
|image              = File:Samuel davenport.jpg
|order1             = 
|office1            = South Australian<br>[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]]
|term_start1        = 20 March 1857
|term_end1          = 21 August 1857
|premier1           = [[B. T. Finniss|Boyle Finniss]]
|predecessor1       = [[Arthur Henry Freeling|Arthur Freeling]]
|successor1         = [[Arthur Blyth]]
|term_start2        = 1 September 1857
|term_end2          = 30 September 1857
|premier2           = [[Robert Torrens]]
|predecessor2       = [[Arthur Blyth]]
|successor2         = [[Thomas Reynolds (Australian politician)|Thomas Reynolds]]
|birth_date   = {{birth date|1818|03|05|df=y}} 
|birth_place  = [[Shirburn]], [[Oxfordshire]], England
|death_date   = {{death date and age|df=yes|1906|9|3|1818|3|5}}
|death_place  = [[Beaumont, South Australia]]
|restingplacecoordinates = 
|nationality  = 
| spouse      = Margaret Fraser
| parents     = George Davenport and Jane Devereux, ''née'' Davies
|religion     = 
}}

'''Sir Samuel Davenport'''  {{post-nominals|country=AUS|KCMG}} (5 March 1818 – 3 September 1906) was one of the early settlers of Australia and became a [[landowner]] and [[Parliament of South Australia|parliamentarian]] in [[South Australia]].

Davenport was fourth son of George Davenport, a wealthy English [[banker]], and his wife Jane Devereux, ''née'' Davies, and was educated at [[Mill Hill School]] in North London. His father, had become an agent of the [[South Australia Company]] in England and together with partners Frederick Luck (quarter share) and Roger Cunliffe (one-eighth share) paid [[Pound stirling|£]]4416 for a special survey of {{convert|4416|acre|km2}} in [[South Australia]]. George Davenport sent his eldest son Francis to select the land, and Francis arrived in [[Adelaide]] in February 1840. After initially considering land near [[Port Lincoln, South Australia|Port Lincoln]], he selected land on the upper reaches of the [[River Angas]], including what is now the town of [[Macclesfield, South Australia|Macclesfield]]. Francis returned to England in 1841, leaving Henry Giles to manage his affairs.

Samuel married Margaret Fraser Cleland (1821 &ndash; 6 February 1902) on 1 June 1842. She was to become a noted philanthropist, closely associated with [[Emily Clark]], Lady Colton and [[Catherine Helen Spence]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article56233231 |title=Concerning People. |newspaper=[[The Register (Adelaide)|The Register]] |location=Adelaide |date=7 February 1902 |accessdate=13 February 2012 |page=5 |publisher=National Library of Australia}}</ref>

Samuel, his wife and another brother [[Robert Davenport (Australian politician)|Robert Davenport]] went to Australia in February 1843 accompanying Francis and his wife Sarah on their return. Francis died on 8 April 1843, and the remaining brothers lived at Macclesfield and managed the survey. Samuel continued to receive an annual allowance from his father.

Davenport's first ventures after moving to Australia from England were in mixed farming, almonds and vines, which had sparked his interest when he was in the south of France as a youth. He then tried sheep-farming with approximately 6000 sheep, but disease killed half of them. In 1860 he bought land near [[Port Augusta, South Australia|Port Augusta]], and turned to ranching horses and cattle. He realised from his success that large-stock holdings made healthy profits in South Australia.

[[File:Davenport beaumont.jpg|200px|thumb|left|Davenport at Beaumont House during 1880.]]
From 1849 he lived mostly at his home in [[Beaumont, South Australia|Beaumont]], in his residence of [[Beaumont House]], which he owned from 1851 onwards. He continued to care for the welfare of tenants at Macclesfield, providing attractive rental terms.

Between 5 May 1846 and 1 July 1848, Davenport was a non-official nominated member of the [[South Australian Legislative Council]].<ref name=ParlSD>
{{cite SA-parl| pid=4076 |name=Sir Samuel Davenport}}
</ref>
<ref>
{{cite web
 |url=http://www.parliament.sa.gov.au/AboutParliament/From1836/Documents/StatisticalRecordoftheLegislature1836to20093.pdf
 |title=Statistical Record of the Legislature 1836 – 2007
 |publisher=Parliament of South Australia
}}</ref> 
Between 1849 and 1852 he served as a city commissioner. He contested the Legislative Council seat of Hindmarsh without success in a by-election during 1854, but on 25 October 1855<ref name=ParlSD/> was nominated to the part-elective Legislative Council. He was eventually elected to the first Legislative Council under [[responsible government]] in 1857 and administered the [[oath of allegiance]] to the councillors on 22 April 1857. He served a number of ministries; however he resigned from the council on 25 September 1866.<ref name=ParlSD/>

Davenport strongly promoted agriculture and other new industries in South Australia. Between 1864 and 1872 he published a number of pamphlets, three of them dealing with the cultivation of olives and manufacture of [[olive oil]], [[silk]] and [[tobacco]]. Davenport grew both olives and silk on his [[Beaumont House]] estate. He was a member of the [[Royal Agricultural and Horticultural Society of South Australia|Royal Agricultural and Horticultural Society]] and its president from 1873 to 1879 and 1890 to 1891. He was a member of the [[South Australian Institute]]'s Board of Governors.{{when|date=May 2014}}

He was elected to a number of positions in the agricultural, [[Horticulture|horticultural]] and [[Geography|geographical]] societies. He was also a successful banker like his English father.

Davenport was [[knight]]ed during 1884 and in 1886 appointed [[Order of St Michael and St George|KCMG]] and given an [[honorary doctorate]] by the [[University of Cambridge]].<ref>{{acad|id=DVNT886S|name=Davenport, Samuel}}</ref> After his death in 1906, obituarists praised his 'honourable record both in public and private life'<ref>
{{cite news
 |url=http://nla.gov.au/nla.news-article5097235
 |title=Death of Sir Samuel Davenport
 |newspaper=[[The Advertiser (Adelaide)|The Advertiser]]
 |location=Adelaide |date=4 September 1906
 |accessdate=13 February 2012 |page=7
 |publisher=National Library of Australia
}}</ref> and both [[Parliament of South Australia|Houses of Parliament]] were [[adjournment|adjourned]] for his funeral.

The South Australian Assembly seat of [[electoral district of Davenport|Davenport]] was later named after him.

{{s-start}}
{{s-off}}
{{s-bef|before=[[Arthur Henry Freeling|Arthur Freeling]]}}
{{s-ttl|title=[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]]|years=<small>20 Mar{{spaced ndash}}21 Aug </small>1857}}
{{s-aft|after=[[Arthur Blyth]]}}
|-
{{s-bef|before=[[Arthur Blyth]]}}
{{s-ttl|title=[[Commissioner of Public Works (South Australia)|Commissioner of Public Works]]|years=<small>1 Sep{{spaced ndash}}30 Sep </small>1857}}
{{s-aft|after=[[Thomas Reynolds (Australian politician)|Thomas Reynolds]]}}
{{s-end}}

==References==
{{reflist}}

*{{cite web
| url = http://adb.anu.edu.au/biography/davenport-sir-samuel-3371
| title = Davenport, Sir Samuel (1818–1906)
| author = Nicks, Beverley A.
| publisher = Australian Dictionary of Biography
| accessdate = 26 September 2014
}}
*{{cite book
| editor = Jim Faull 
| title = Macclesfield Reflections along the Angas
|date=December 1980
| publisher = Macclesfield historical Book Committee
| location = Adelaide
| isbn = 0-9594595-0-2
| pages = 23–24
}}
*{{Dictionary of Australian Biography
| First = Sir Samuel
| Last = Davenport
| shortlink=0-dict-biogD.html#davenport1
}}
*{{cite web
| url = http://www.science.org.au/academy/basser/lists/ms064.doc
| title = Australian Botanists
| author = 
| publisher = Australian Academy of Science
| accessdate = 2007-01-11
}} {{Dead link|date=October 2010|bot=H3llBot}}
*{{cite web
| url = http://www.postcards.sa.com.au/features/davenport_olives.html
| title = Features: Davenport Olives
| work = Postcards SA (TV show)
| accessdate = 2007-01-11
}}
*{{cite web
| url = http://www.abc.net.au/elections/sa/2006/guide/dave.htm
| title = 2006 South Australian election profiles: Seat of Davenport
| publisher = ABC
| accessdate = 2007-01-11
}}

{{Authority control}}

{{DEFAULTSORT:Davenport, Samuel}}
[[Category:Knights Bachelor]]
[[Category:Knights Commander of the Order of St Michael and St George]]
[[Category:Settlers of South Australia]]
[[Category:Settlers of Australia]]
[[Category:1818 births]]
[[Category:1906 deaths]]
[[Category:People educated at Mill Hill School]]
[[Category:Members of the South Australian Legislative Council]]
[[Category:19th-century Australian politicians]]