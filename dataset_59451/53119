{{Infobox journal
| title = Western Journal of Nursing Research
| cover = [[File:WJN cover.gif|200px]]
| abbreviation = West. J. Nurs. Res.
| editor = Vicki Conn
| discipline = [[Nursing]]
| publisher = [[SAGE Publications]]
| country =
| frequency = 10/year
| history = 1979-present
| openaccess =
| license =
| impact = 1.217
| impact-year = 2012
| website = http://wjn.sagepub.com/
| link1 = http://wjn.sagepub.com/content/current
| link1-name = Online access
| link2 = http://wjn.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR =
| OCLC = 475164874
| LCCN = 
| CODEN =
| ISSN = 0193-9459
| eISSN = 1552-8456
}}
The '''''Western Journal of Nursing Research''''' is a [[Peer review|peer-reviewed]] [[nursing journal]] that covers clinical research in the field of [[nursing]]. It was established as a quarterly in 1979 by founding [[editor-in-chief]] Pamela Brink, working with the Western Council on Higher Education in Nursing and KNI Publishing, which was acquired by [[Sage Publications]] in 1984.<ref name=Brink>{{cite journal |last=Brink |first=P |title=The Western Journal of Nursing Research |journal=Western Journal of Nursing Research |year=2003 |volume=25 |pages=901–908 |doi=10.1177/0193945903258551 |pmid=14678618 |issue=8}}</ref> In 2003, it became the official journal of Midwest Nursing Research Society. Brink was succeeded by Vicki Conn in 2007.

== Abstracting and indexing ==
The ''Western Journal of Nursing Research'' is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 1.217.<ref name=WoS>{{cite book |year=2013 |chapter=Western Journal of Nursing Research |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://wjn.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:General nursing journals]]
[[Category:Publications established in 1979]]