{{good article}}
{{Use dmy dates|date=March 2015}}
{{Use British English|date=March 2015}}
{{Infobox military unit
|unit_name= 77th Infantry Division<br>77th Infantry (Reserve) Division<br>77th (Holding) Division
|image=[[File:77 inf div -vector.svg|150px|alt=]]
|caption=The shoulder insignia of the division
|dates=1 December 1941&nbsp;–1 September 1944{{sfn|Joslen|2003|p=100}}
|country={{flag|United Kingdom}}
|branch={{army|United Kingdom|23px}}
|type=[[Infantry]]
|role=Home defence, and training.
|size=
}}
The '''77th Infantry Division''' of the [[British Army]] was formed during the [[World War II|Second World War]], from the re-organisation of the [[Devon and Cornwall County Division]]. During its existence the [[Division (military)|division]] changed roles several times. On 20 December 1942, it became the 77th Infantry (Reserve) Division, training recruits in [[infantry]] and armoured warfare. New recruits to the army were assigned to the 77th to complete their training.

On 1 December 1943, the division was once again renamed. Now known as the 77th (Holding) Division, it was responsible for retraining soldiers who had been on medical leave. Once recruits were fully trained, and men returning from injury retrained, they were allocated to formations fighting overseas. Notably, the formation was used as a source of reinforcements for the [[21st Army Group]], which was fighting in [[Operation Overlord|Normandy]]. After all available British army troops left the United Kingdom for France, the division was disbanded and re-formed as a [[Military deception|deception unit]] to give [[Nazi Germany|Germany]] the impression that the British army had more divisions than it did. The notional 77th Division was held in reserve within the United Kingdom.

==Background==

[[File:Cornwall County Division Insignia vector.svg|thumb|left|140px|alt=|The divisional badge of the Devon and Cornwall County Division]]

In 1940, following the [[World War II|Second World War's]] [[Battle of France]], the [[United Kingdom]] was under [[Operation Sea Lion|threat of invasion]] from [[Nazi Germany|Germany]].{{sfn|Fraser|1999|p=83}} During the summer, the [[Battle of Britain]] dampened this threat.{{sfn|Fraser|1999|p=83}} As the year progressed, the size of the [[British Army]] increased dramatically as 140 new infantry [[battalion]]s were raised.{{sfn|Perry|1988|p=53}} During October, with the possibility of a German invasion during 1941, these new battalions were formed into independent [[infantry]] [[brigade]]s that were then assigned to newly created [[British County Divisions|County Divisions]].{{sfn|Perry|1988|p=53}}{{sfn|Forty|2013|loc=County Divisions}}

The County Divisions, including the [[Devon and Cornwall County Division]], were around 10,000 men strong and assigned to defend the coastlines of threatened sections of the country, including the manning of coastal artillery.{{sfn|Perry|1988|p=53}}{{sfn|Churchill|Gilbert|2001|p=1321}}{{sfn|Joslen|2003|p=108}} These divisions were largely static, lacking mobility and also divisional assets such as [[artillery]], [[Sapper|engineers]], and [[reconnaissance]] forces.{{sfn|Joslen|2003|p=108}} Using the recruits in this manner allowed the regular infantry divisions to be freed up from such duties, undertake training, and form an all-important reserve that could be used to counterattack any possible German landing.{{sfn|Messenger|1994|p=61}}

On 22 June, Germany [[Operation Barbarossa|launched a massive attack]] upon the [[Soviet Union]]; this attack all but removed the threat of a German invasion of the United Kingdom. However, the British still had to consider the threat of a German invasion due the possibility that the Soviet Union could collapse under the German onslaught and the ease in which Germany could transfer troops back to the west. In late 1941, the arrival of autumn and winter weather meant that the threat of invasion subsided. This, coupled with the production of new equipment for the British army, allowed the [[War Office]] to begin steps to better balance the army due to the large number of infantry units formed during the preceding year and a half.{{sfn|Goldstein|McKercher|2003|p=274}}{{sfn|Perry|1988|p=65}} As part of this reform, the County Divisions were disbanded.{{sfn|Goldstein|McKercher|2003|p=274}}{{efn|The large intake of men into the army had considerably increased the infantry arm. The reforms intended to address this, with many of the newly raised battalions being "converted to other arms, particularly artillery and armour".{{sfn|Perry|1988|pp=53–54}} In addition to this, historian F.W. Perry comments, there was considerable pressure "to increase the armoured component [of the army] and build up [[British Commandos|raiding]] and special forces". These pressures, and the re-balancing of the military, resulted in seven of the nine County Divisions being disbanded and only two being reformed as infantry divisions{{sfn|Perry|1988|p=65}}}}

==History==

===Home defence===
During the war, the divisions of the British Army were divided between "Higher Establishment" and "Lower Establishment" formations. The former were intended for deployment overseas and combat, whereas the latter were strictly for home defence in a static role.{{sfn|Perry|1988|p=65}}{{sfn|French|2001|p=188}} On 1 December 1941, the Devon and Cornwall County Division was abolished and reformed as the 77th Infantry Division, a "Lower Establishment" division.{{sfn|Joslen|2003|p=100}} The division, like its predecessor, comprised the [[203rd Infantry Brigade (United Kingdom)|203rd]], the [[209th Infantry Brigade (United Kingdom)|209th]], and the [[211th Infantry Brigade (United Kingdom)|211th Infantry Brigades]].{{sfn|Joslen|2003|pp=100 and 108}}  That day, the division was assigned artillery, engineers, and [[signaller]]s. An [[Anti-tank warfare|anti-tank regiment]] and reconnaissance troops joined the following month.{{sfn|Joslen|2003|p=100}} The war-establishment, the on-paper strength, of an infantry division at this time was 17,298 men.{{sfn|Joslen|2003|pp=130–131}} [[Major General (United Kingdom)|Major-General]] [[Godwin Michelmore]], who had commanded the Devon and Cornwall County Division since 30 October, retained command of the division.{{sfn|Joslen|2003|pp=100 and 108}} The 77th was assigned to [[VIII Corps (United Kingdom)|VIII Corps]], and remained based in the [[Devon]] area.{{sfn|Joslen|2003|p=100}}{{sfn|Cunliffe|1956|p=69}}{{sfn|Collier|1957|p=293}} The [[Imperial War Museum]] comments that the division insignia references "[[King Arthur|Arthur's]] sword [[Excalibur]], acknowledging the [division's] connections with the [[West Country]]" and its predecessor division.<ref name="IWM">{{cite web |url=http://www.iwm.org.uk/collections/item/object/30071846 |title=Badge, formation, 77th Infantry Division |website=[[Imperial War Museum]] |accessdate=15 May 2015}}</ref> After the division became a training formation, the insignia was only worn by the permanent division members.{{sfn|Davis|1983|p=107}}

===Training formation===
[[File:British army training 22 January 1943.jpg|thumb|right|alt=|Infantry training, January 1943.]]
During the winter of 1942–43, the British Army overhauled how it would train new recruits. The 77th was one of three divisions that were changed from "Lower Establishment" units to "Reserve Divisions".{{sfn|Perry|1988|p=66}}{{efn|The other two divisions were the [[48th (South Midland) Division|48th]] and [[76th Infantry Division (United Kingdom)|76th]].{{sfn|Joslen|2003|pp=77, and 99}}}} On 20 December, the division was renamed the 77th Infantry (Reserve) Division, becoming a training formation in the process.{{sfn|Joslen|2003|p=100}}{{sfn|Perry|1988|p=66}} These three divisions were supplemented by a fourth training formation, which was raised on 1 January 1943.{{efn|The [[80th Infantry (Reserve) Division (United Kingdom)|80th Infantry (Reserve) Division]].{{sfn|Joslen|2003|p=103}}}} The 77th Infantry (Reserve) Division was assigned to [[Northern Command (United Kingdom)|Northern Command]].{{sfn|Forty|2013|loc=Reserve Divisions}} Soldiers who had completed their corps training were sent to these training divisions.{{sfn|French|2001|p=68}}{{efn|Having entered military service, a recruit was assigned to the [[General Service Corps]]. They would then undertake six weeks training at a Primary Training Centre and take aptitude and intelligence tests. The recruit would then be posted to a Corps Training Centre that specialized in the arm of the service they were joining. For those who would be joining the infantry, Corps training involved a further sixteen week course. For more specialized roles, such as signallers, it could be up to thirty weeks.{{sfn|French|2001|p=68}}}} The soldiers were given five weeks of additional training at the [[Section (military unit)|section]], [[platoon]] and [[Company (military unit)|company]] level, before undertaking a final three-day [[Military exercise|exercise]]. Troops would then be ready to be sent overseas to join other formations.{{sfn|French|2001|p=68}} Training was handled in this manner to relieve the "Higher Establishment" divisions from being milked for replacements for other units and to allow them to intensively train without the interruption of having to handle new recruits.{{sfn|Forty|2013|loc=Reserve Divisions}} For example, the 12th Battalion, [[Devonshire Regiment]] provided the additional training to the regiment's new recruits before assigning them to other battalions within the regiment.<ref>{{cite web |url=http://www.keepmilitarymuseum.org/history/second+world+war/the+devonshire+regiment/the+twelfth+and+fiftieth+battalions|title=The 12th and 50th Battalions The Devonshire Regiment in World War Two|website=The Keep Military Museum: Home of the Regiments of Devon and Dorset|accessdate=15 May 2015}}</ref>

As part of the restructuring, the 211th Infantry Brigade was transferred to the [[80th Infantry (Reserve) Division (United Kingdom)|80th Infantry (Reserve) Division]]. The infantry brigade was replaced with the [[11th Armoured Brigade (United Kingdom)|11th Army Tank Brigade]], in order to provide training facilities for the [[Royal Armoured Corps]] and retain reinforcements until they were ready to be deployed.<ref name="IWM"/>{{sfn|Joslen|2003|pp=199 and 374}} On 1 December 1943, the division was again re-organised, and became the 77th Holding Division. As part of this restructuring, the 11th Army Tank Brigade was withdrawn. [[Lieutenant colonel (United Kingdom)|Lieutenant-Colonel]] H.F. Joslen wrote that the division's role was now "for sorting, retraining and holding personnel temporarily&nbsp;– due to disbandments, medical and other causes."{{sfn|Joslen|2003|p=100}} For example, as part of the change from a reserve to holding division, the 14th Battalion, [[Durham Light Infantry]] was converted from a regular infantry unit into a Rehabilitation Centre. Ex-[[prisoner of war|prisoners of war]], [[Repatriation (humans)|repatriates]], troops who were suffering from morale issues or of low physique were sent to the battalion where they underwent medical, physical, and military tests. These tests were designed to establish what medical category the soldiers should be assigned, and what job or military capability would best suit them.{{sfn|Rissik|2012|p=315}} Likewise, soldiers returning from long periods of overseas service were sent to the 11th Battalion, [[York and Lancaster Regiment]] for retraining.{{sfn|Sheffield|1930–1956|p=269}}

On 30 June 1944, the 77th Holding, and the training divisions, had a combined total of 22,355 men. Of these, only 1,100 were immediately available as replacements for the [[21st Army Group]].{{sfn|Hart|2007|p=52}}{{efn|The war establishment—the paper strength—of a "Higher Establishment" infantry division in 1944 was 18,347 men.{{sfn|Joslen|2003|pp=130–131}}}} The remaining 21,255 men were considered ineligible for service abroad due to a variety of reasons, ranging from medical, not being considered fully fit, or not yet fully trained. Over the following six months, up to 75 per cent of these men would be deployed to reinforce the 21st Army Group, following the completion of their training and/or having come up to the required fitness levels.{{sfn|Hart|2007|pp=48–51}} Stephen Hart comments that, by September, the 21st Army Group "had bled [[Commander-in-Chief, Home Forces|Home Forces]] dry of draftable riflemen" due to the losses suffered during the [[Operation Overlord|Battle of Normandy]], leaving the army in Britain (with the exception of the [[52nd (Lowland) Infantry Division]]) with just "young lads, old men, and the unfit".{{sfn|Hart|2007|pp=49–50}} On 1 September 1944, the division was disbanded.{{sfn|Joslen|2003|p=100}} The [[45th Infantry Division (United Kingdom)|45th (Holding) Division]] was then formed (the 45th Infantry Division having been disbanded on 15 August) by Michelmore and his headquarter staff. Michelmore assumed command, and the 45th Holding Division took over the role of the 77th Division.{{sfn|Forty|2013|loc=Reserve Divisions}}{{sfn|Joslen|2003|p=73}} Roger Hesketh states the reason behind this renumbering was due to the 45th Division being a "well-known territorial [formation from] before the war whose [number was] familiar to the public and [was] therefore of recruiting value".{{sfn|Hesketh|2000|p=246}}

===Deception===

During 1944, the British army was facing a manpower crisis. The army did not have enough men to replace the losses to front line infantry. While efforts were made to address this (such as transferring men from the [[Royal Artillery]] and [[Royal Air Force]] to be retrained as infantry), the War Office began disbanding divisions to downsize the army so as to transfer men to other units to help keep those as close to full strength as possible.{{sfn|Messenger|1994|p=122}}{{sfn|Allport|2015|p=216}} The 77th Infantry (Reserve) Division was one of several "Lower Establishment" divisions, within the United Kingdom, chosen to be disbanded.{{sfn|Hesketh|2000|p=246}}

[[R Force]], a British deception unit, seized upon this opportunity to retain the division as a phantom unit to inflate the army's [[order of battle]]. A cover story was established to explain the change in the division's status. It was claimed that, with the war nearing an end, several [[Army Reserve (United Kingdom)|Territorial Army]] divisions would revert to their peacetime recruiting role and release their equipment and resources to other units. For the 77th, this equipment would be notionally transferred from the 45th Division. With the transfer of equipment, the 77th Division was notionally raised to the "Higher Establishment". The notional division was held in reserve, within the United Kingdom, pending a future use elsewhere.{{sfn|Hesketh|2000|p=246}}

==General officer commanding==
{{see also|General officer commanding}}
Commanders included:
{|
!Appointed
!General officer commanding
|-
|From formation
|[[Major-general (United Kingdom)|Major-General]] [[Godwin Michelmore]]{{sfn|Joslen|2003|p=100}}
|}

==Order of Battle==
{| class="toccolours collapsible collapsed" style="width:100%; background:transparent;"
!colspan= | 77th Infantry Division
|-
|colspan="2" |
203rd Infantry Brigade{{sfn|Joslen|2003|p=366}}
{{main |203rd Infantry Brigade (United Kingdom) | l1 = 203rd Infantry Brigade }}
* 12th Battalion, [[Devonshire Regiment]] (until September 1942)
* 10th Battalion, [[Loyal Regiment (North Lancashire)]] (until May 1942)
* 9th Battalion, [[Essex Regiment]] (until September 1942)
* 11th Battalion, [[Hampshire Regiment]] (September 1942)
* 2nd Battalion, Loyal Regiment (North Lancashire) (May 1942, until September 1942)
* 2nd Battalion, [[East Surrey Regiment]] (September 1942, until January 1943)
* 10th Battalion, [[Royal Sussex Regiment]] (September 1942, until October 1943)
* 15th Battalion, [[Queen's Royal Regiment]] (September 1943, until October 1943)
* 8th Battalion, Devonshire Regiment (January 1943, until November 1943)
* 11th Battalion, [[South Staffordshire Regiment]] (October 1943, until division disbanded)
* 11th Battalion, [[York and Lancaster Regiment]] (November 1943, until July 1944)
* 9th Battalion, [[Seaforth Highlanders]] (November 1943, until July 1944)
* 7th Battalion, [[Royal Ulster Rifles]] (November 1943, until division disbanded)
* 11th Battalion, [[Argyll and Sutherland Highlanders]] (July 1944, until division disbanded)
* 2/6th Battalion, [[Lancashire Fusiliers]] (July 1944, until division disbanded)

209th Infantry Brigade{{sfn|Joslen|2003|p=372}}
{{main |209th Infantry Brigade (United Kingdom) | l1 = 209th Infantry Brigade }}
* 8th Battalion, [[Buffs (Royal East Kent Regiment)]] (until September 1942)
* 9th Battalion, Buffs (Royal East Kent Regiment) (until October 1943)
* 10th Battalion, Buffs (Royal East Kent Regiment) (until October 1943)
* 11th Battalion, Hampshire Regiment (from September 1942, until division disbanded)
* 14th Battalion, [[Durham Light Infantry]] (from September 1942, until division disbanded)
* 18th Battalion, [[Welch Regiment]] (November 1943, until division disbanded)
* 6th Battalion, [[Northamptonshire Regiment]] (November 1943, until division disbanded)

211th Infantry Brigade (until January 1943) {{sfn|Joslen|2003|p=374}}
{{main |211th Infantry Brigade (United Kingdom) | l1 = 211th Infantry Brigade }}
* 10th Battalion, East Surrey Regiment
* 11th Battalion, Devonshire Regiment (until September 1942)
* 14th Battalion, [[Royal Warwickshire Regiment]] (until October 1942)
* 13th Battalion, [[Queen's Royal Regiment]] (from September 1942)
* 2/6th Battalion, Lancashire Fusiliers (from October 1942)

11th Army Tank Brigade (1943 only){{sfn|Joslen|2003|p=199}}
{{main |11th Armoured Brigade (United Kingdom) | l1 = 11th Army Tank Brigade }}
* [[107th Regiment Royal Armoured Corps]]
* [[110th Regiment Royal Armoured Corps]]
* [[111th Regiment Royal Armoured Corps]]

Divisional Troops
* 23rd Field Regiment, [[Royal Artillery]] (until December 1942){{sfn|Joslen|2003|p=100}}
* 175th Field Regiment, Royal Artillery (from July 1942 until December 1942, then August 1943 until the division disbanded){{sfn|Joslen|2003|p=100}}
* 176th Field Regiment, Royal Artillery (from December 1942, until the division disbanded){{sfn|Joslen|2003|p=100}}
* 87th Anti-tank Regiment, Royal Artillery (from January 1942, until October 1942){{sfn|Joslen|2003|p=100}}
* 77th Infantry Divisional Engineers, [[Royal Engineers]]{{sfn|Joslen|2003|p=100}}
** 100th Field Company (until December 1942){{sfn|Joslen|2003|p=100}}
** 101st Field Company (until December 1942){{sfn|Joslen|2003|p=100}}
** 556th Field Company (from January 1943, until October 1943){{sfn|Joslen|2003|p=100}}
** Divisional Field Stores Section{{sfn|Joslen|2003|p=100}}
* 77th Infantry Divisional Signals, [[Royal Corps of Signals]] (later known as the 77th Infantry (Reserve) Divisional Signals){{sfn|Joslen|2003|p=100}}
* 77th Independent Company, [[Reconnaissance Corps]] (from January 1942, until June 1942){{sfn|Joslen|2003|p=100}}
* 77th Independent Squadron, Reconnaissance Corps (from June 1942, until January 1943){{sfn|Joslen|2003|p=100}}
* 10th Battalion, [[King's Royal Rifle Corps]] ([[Motorized infantry|Motor Battalion]]. From January 1943, until November 1943){{sfn|Joslen|2003|p=100}}
|}

==Notes==
{{portal|British Army|World War II}}
===Footnotes===
{{Notelist}}
===Citations===
{{Reflist|3}}

==References==
{{Refbegin}}
* {{cite book |ref=harv |first=Alan|last=Allport|author1-link=Alan Allport|title=Browned Off and Bloody-minded: The British Soldier Goes to War 1939–1945|location=New Haven |publisher=[[Yale University Press]]|year=2015|isbn=978-0-300-17075-7|lastauthoramp=y}}
* {{cite book|ref=CITEREFChurchillGilbert2001|last=Churchill|first=Winston|author-link=Winston Churchill|editor-last=Gilbert|editor-first=Martin|editor-link=Martin Gilbert|title=The Churchill War Papers: The Ever-Widening War|volume=3|location=New York|publisher=[[W. W. Norton & Company]]|year=2001|isbn=978-0-393-01959-9}}
* {{cite book |ref=harv |first=Basil|last=Collier|author1-link=Basil Collier|editor-last=Butler |editor-first=J. R. M. |editor-link=James Ramsay Montagu Butler |series=[[History of the Second World War]], United Kingdom Military Series |title=The Defence of the United Kingdom|location=London |publisher=[[Office of Public Sector Information|Her Majesty's Stationery Office]]|year=1957|oclc=375046|lastauthoramp=y}}
* {{Cite book|ref=harv|first=Marcus|last=Cunliffe|author-link=Marcus Cunliffe|title=History of the Royal Warwickshire Regiment, 1919–1955|location=London|publisher=[[William Clowes Ltd.]]|year=1956|oclc=10627036}}
* {{Cite book|ref=harv|first=Brian Leigh|last=Davis|title=British Army Uniforms & Insignia of World War Two|location=London|publisher=[[Lionel Leventhal|Arms and Armour Press]]|year=1983|isbn=978-0-85368-609-5}}
* {{cite book|ref=harv|last=French|first=David|title=Raising Churchill's Army: The British Army and the War Against Germany 1919–1945|publisher=[[Oxford University Press]]|year=2001|origyear=2000|location=Oxford|isbn=978-0-19-924630-4}}
* {{cite book|ref=harv|last=Forty|first=George|title=Companion to the British Army 1939–1945|publisher=[[The History Press|Spellmount]]|year=2013|origyear=1998|edition=ePub|location=New York|isbn=978-0-7509-5139-5}}
* {{cite book|ref=harv|last=Fraser|first=David|author-link=David Fraser (British Army officer)|title=And We Shall Shock Them: The British Army in the Second World War|location=London|publisher=[[Orion Publishing Group|Cassell Military]]|year=1999|origyear=1983|isbn=978-0-304-35233-3}}
* {{cite book|ref=harv|editor-last1=Goldstein|editor-first1=Erik|editor-last2=McKercher|editor-first2=Brian|title=Power and Stability: British Foreign Policy, 1865–1965|series=Diplomacy & Statecraft|location=London|publisher=[[Routledge]]|year=2003|isbn=978-0-7146-8442-0}}
* {{Cite book|ref=harv|first=Stephen Ashley|last=Hart|title=Colossal Cracks: Montgomery's 21st Army Group in Northwest Europe, 1944–45|location=Mechanicsburg|publisher=[[Stackpole Books]]|year=2007|origyear=2000|isbn=978-0-8117-3383-0}}
* {{Cite book|ref=harv|first=Roger|last=Hesketh|authorlink=Roger Fleetwood-Hesketh|title=Fortitude: The D-Day Deception Campaign|location=Woodstock|publisher=[[The Overlook Press|Overlook Hardcover]]|year=2000|isbn=978-1-58567-075-8}}
* {{Joslen-OOB}}
* {{cite book |ref=harv |last=Messenger |first=Charles |year=1994 |series=A History of British Infantry |title=For Love of Regiment 1915–1994 |volume=2 |location=London|publisher=[[Pen and Sword Books|Pen & Sword Books]] |isbn=978-0-85052-422-2}}
* {{Cite book|ref=harv|first=Frederick William|last=Perry|title=The Commonwealth Armies: Manpower and Organisation in Two World Wars|series=War, Armed Forces and Society|location=Manchester|publisher=[[Manchester University Press]]|year=1988|isbn=978-0-7190-2595-2}}
* {{cite book |ref=harv |last=Rissik |first=David |year=2012 |orig-year=1953 |title=The D. L. I. at War: The History of the Durham Light Infantry 1939–1945|location=Luton|publisher=Andrews UK |edition=ePub|isbn=978-1-78151-535-8}}
* {{cite book |ref=harv |last=Sheffield |first=O. F. |year=1930–1956 |title=The York and Lancaster Regiment, 1758–1953 |volume=III |location=London|publisher=Butler & Tanner |oclc=39831761}}
{{refend}}

==External links==
* {{cite web|url=http://www.kentphotoarchive.com/kpa/warpeace/thumblarge.php?keywordname=0&selection=1&category_id=1&startnumber=202&pagenumber=11|title=Kent Photo Archive: The War & Peace Collection |accessdate=5 July 2015}} A member of the 77th Infantry Division poses for a public relations photo on an armed trawler, Devon c.1942.

{{British Army Divisions}}

{{DEFAULTSORT:77 Infantry Division}}
[[Category:Infantry divisions of the British Army in World War II]]
[[Category:Military units and formations established in 1941]]
[[Category:Military units and formations disestablished in 1944]]