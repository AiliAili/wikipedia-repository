{{redirect|Jupiter V|the story by Arthur C. Clarke|Jupiter Five|the aeroplane|Bristol Jupiter}}
{{use dmy dates|date=March 2016}}
{{Good article}}
{{Infobox planet
| name = Amalthea
| adjectives = Amalthean
| image = [[File:Amalthea PIA02532.png|295px]]
| caption = Greyscale ''[[Galileo probe|Galileo]]'' images of Amalthea
| discovery = yes
| discoverer = [[Edward Emerson Barnard|E. E. Barnard]]
| discovered = 9 September 1892
| mean_orbit_radius = {{val|181365.84|0.02|u=km}} ({{Jupiter radius|2.54}}){{sfn|Cooper Murray et al.|2006}}
| eccentricity = {{val|0.00319|0.00004}}{{sfn|Cooper Murray et al.|2006}}
| periapsis = {{val|181150|u=km}}{{efn|name=stub}}
| apoapsis = {{val|182840|u=km}}{{efn|name=stub}}
| period = {{val|0.49817943|0.00000007|u=d}} {{nowrap|(11 h, 57 min, 23 s)}}{{sfn|Cooper Murray et al.|2006}}
| avg_speed = {{val|26.57|u=km/s}}{{efn|name=stub}}
| inclination = {{val|0.374|0.002|u=°}} (to Jupiter's equator){{sfn|Cooper Murray et al.|2006}}
| satellite_of = [[Jupiter]]
| physical_characteristics = yes
| dimensions = 250&thinsp;×&thinsp;146&thinsp;×&thinsp;128&nbsp;km{{sfn|Thomas Burns et al.|1998}}
| mean_radius = {{val|83.5|2.0|u=km}}{{sfn|Thomas Burns et al.|1998}}
| volume = {{val|2.43|0.22|e=6|u=km<sup>3</sup>}}{{sfn|Anderson Johnson et al.|2005}}
| mass = {{val|2.08|0.15|e=18|u=kg}}{{sfn|Anderson Johnson et al.|2005}}
| density = {{val|0.857|0.099|u=g/cm<sup>3</sup>}}{{sfn|Anderson Johnson et al.|2005}}
| surface_grav = {{val|p=≈&thinsp;|0.020|ul=m/s2}} (≈&thinsp;0.002&nbsp;g){{efn|name=stub}}
| escape_velocity = {{val|p=≈&thinsp;|0.058|u=km/s}}{{efn|name=stub}}
| rotation = [[synchronous rotation|synchronous]]{{sfn|Thomas Burns et al.|1998}}
| axial_tilt = zero{{sfn|Thomas Burns et al.|1998}}
| albedo = {{val|0.090|0.005}}{{sfn|Simonelli Rossier et al.|2000}}
| magnitude = 14.1{{sfn|Observatorio ARVAL}}
| temperatures = yes
| temp_name1 = {{sfn|Simonelli|1983}}
| max_temp_1 = 165&nbsp;K
| mean_temp_1 = 120&nbsp;K
}}

'''Amalthea''' ({{IPAc-en|æ|m|əl|ˈ|θ|iː|ə}} {{respell|am-əl|THEE|ə}}; {{lang-el|Ἀμάλθεια}}) is the third [[natural satellite|moon]] of [[Jupiter]] in order of distance from the planet. It was discovered on 9 September 1892, by [[Edward Emerson Barnard]] and named after [[Amalthea (mythology)|Amalthea]], a [[nymph]] in [[Greek mythology]].{{sfn|Barnard|1892}} It is also known as '''{{nowrap|Jupiter V}}'''.

Amalthea is in a close orbit around Jupiter and is within the outer edge of the [[Rings of Jupiter#Amalthea gossamer ring|Amalthea Gossamer Ring]], which is formed from dust ejected from its surface.{{sfn|Burns Simonelli et al.|2004}} From its surface, Jupiter would appear 46.5 [[Degree (angle)|degrees]] in diameter.{{efn|name=stub2}} Amalthea is the largest of the [[inner satellites of Jupiter]]. Irregularly shaped and reddish in color, it is thought to consist of porous water ice with unknown amounts of other materials. Its surface features include large craters and ridges.{{sfn|Thomas Burns et al.|1998}}

Amalthea was photographed in 1979 by the ''[[Voyager program|Voyager 1 and 2]]'' spacecraft, and later, in more detail, by the [[Galileo (spacecraft)|''Galileo'' orbiter]] in the 1990s.{{sfn|Thomas Burns et al.|1998}}

==History==

=== Discovery ===
[[File:Amalthea Voyager-1.png|thumb|upright|left|''Voyager 1'' color image of Amalthea (1979)]]

Amalthea was discovered on 9 September 1892, by [[Edward Emerson Barnard]] using the [[James Lick telescope|36 inch (91 cm) refractor telescope]] at [[Lick Observatory]].{{sfn|Barnard|1892}} It was the last planetary satellite to be discovered by direct visual observation (as opposed to photographically) and was the first new satellite of Jupiter since [[Galileo Galilei]]'s discovery of the [[Galilean satellites]] in 1610.<ref name=Bakich_2000/>

===Name===
Amalthea is named after the [[nymph]] [[Amalthea (mythology)|Amalthea]] from [[Greek mythology]], who nursed the infant [[Zeus]] (the Greek equivalent of Jupiter) with [[goat's milk]].<ref name=gazetteer_names/> Its [[Roman numeral]] designation is {{nowrap|Jupiter V}}. The name "Amalthea" was not formally adopted by the [[International Astronomical Union|IAU]] until 1976,<ref name=Blunck_2010/><ref name=IAU_1975/> although it had been in informal use for many decades. The name was initially suggested by [[Camille Flammarion]].{{sfn|Flammarion|1893}} Before 1976, Amalthea was most commonly known simply as {{nowrap|Jupiter V}}.{{sfn|Simonelli|1983}}

== Orbit ==
Amalthea orbits [[Jupiter]] at a distance of 181&nbsp;000&nbsp;km (2.54&nbsp;Jupiter radii). The orbit of Amalthea has an [[orbital eccentricity|eccentricity]] of 0.003 and an [[inclination]] of 0.37° relative to the equator of Jupiter.{{sfn|Cooper Murray et al.|2006}} Such appreciably nonzero values of inclination and eccentricity, though still small, are unusual for an [[inner satellite]] and can be explained by the influence of the innermost [[Galilean satellites|Galilean satellite]], [[Io (moon)|Io]]: in the past Amalthea has passed through several [[orbital resonance|mean-motion resonances]] with Io that have excited its inclination and eccentricity (in a mean-motion resonance the ratio of orbital periods of two bodies is a rational number like ''m'':''n'').{{sfn|Burns Simonelli et al.|2004}}

Amalthea's orbit lies near the outer edge of the [[Rings of Jupiter#Amalthea gossamer ring|Amalthea Gossamer Ring]], which is composed of the dust ejected from the satellite.{{sfn|Burns Showalter et al.|1999}}

== Physical characteristics ==
The surface of Amalthea is very [[red]].{{sfn|Thomas Burns et al.|1998}} The reddish color may be due to [[sulfur]] originating from [[Io (moon)|Io]] or some other non-ice material.{{sfn|Thomas Burns et al.|1998}}  Bright patches of less red tint appear on the major slopes of Amalthea, but the nature of this color is currently unknown.{{sfn|Thomas Burns et al.|1998}} The surface of Amalthea is slightly brighter than surfaces of other [[inner satellites of Jupiter]].{{sfn|Simonelli Rossier et al.|2000}} There is also a substantial asymmetry between leading and trailing [[Sphere|hemispheres]]: the leading hemisphere is 1.3&nbsp;times brighter than the trailing one. The asymmetry is probably caused by the higher velocity and frequency of [[impact crater|impacts]] on the leading hemisphere, which excavate a bright material—presumably ice—from the interior of the moon.{{sfn|Simonelli Rossier et al.|2000}}

[[File:Jupiter's moon Amalthea photographed by Galileo.jpg|thumb|left|''[[Galileo probe|Galileo]]'' images showing Amalthea's irregular shape]]
[[File:Amalthea.gif|thumb|170px|right|The most detailed existing image of Amalthea (2.4&nbsp;km/pix).{{sfn|Burns Simonelli et al.|2004}} Anti-Jupiter side. [[Ida Facula]] and [[Lyctos Facula]] are on the left side (on the [[terminator (solar)|terminator]]). Bright spot underside is associated with crater [[Gaea (crater)|Gaea]]. Photo by ''Galileo'' (2000)]]

Amalthea is irregularly shaped, with the best [[ellipsoid]]al approximation being {{nowrap|250 × 146 × 128&nbsp;km}}.{{sfn|Thomas Burns et al.|1998}} From this, Amalthea's surface area is likely between 88,000 and 170,000 square kilometers, or somewhere near 130,000. Like all other inner [[moons of Jupiter]] it is [[tidally locked]] with the planet, the long axis pointing towards Jupiter at all times.{{sfn|Burns Simonelli et al.|2004}} Its surface is heavily scarred by [[Impact crater|crater]]s, some of which are extremely large relative to the size of the moon: [[Pan crater|Pan]], the largest crater, measures 100&nbsp;km across and is at least 8&nbsp;km deep.{{sfn|Thomas Burns et al.|1998}} Another crater, [[Gaea (crater)|Gaea]], measures 80&nbsp;km across and is likely twice as deep as Pan.{{sfn|Thomas Burns et al.|1998}} Amalthea has several prominent bright spots, two of which are named. They are [[Lyctos Facula]] and [[Ida Facula]], with width reaching up to 25&nbsp;km. They are located on the edge of ridges.{{sfn|Thomas Burns et al.|1998}}

Amalthea's irregular shape and large size led in the past to a conclusion that it is a fairly strong, rigid body,{{sfn|Burns Simonelli et al.|2004}} where it was argued that a body composed of ices or other weak materials would have been pulled into a more [[sphere|spherical]] shape by its own gravity. However, on 5 November 2002, the [[Galileo spacecraft|''Galileo'' orbiter]] made a targeted flyby that came within 160&nbsp;km of Amalthea and the deflection of its orbit was used to compute the moon's mass (its volume had been calculated previously—to within 10% or so—from a careful analysis of all extant images).{{sfn|Thomas Burns et al.|1998}} In the end, Amalthea's density was found to be as low as {{nowrap|0.86 g/cm<sup>3</sup>}},{{sfn|Anderson Johnson et al.|2005}}{{sfn|Swiss Cheese Moon}} so it must be either a relatively icy body or very porous "[[rubble pile]]" or, more likely, something in between. Recent measurements of infrared spectra from the [[Subaru (telescope)|Subaru telescope]] suggest that the moon indeed contains [[hydrous]] minerals (or organic materials), indicating that it cannot have formed in its current position, since the hot primordial Jupiter would have melted it.{{sfn|Takato Bus et al.|2004}} It is therefore likely to have formed farther from the planet or to be a captured [[Solar System]] body.{{sfn|Anderson Johnson et al.|2005}} No images were taken during this flyby (''Galileo''{{'}}s cameras had been deactivated due to radiation damage in January 2002), and the resolution of other available images is generally low.

Amalthea radiates slightly more heat than it receives from the [[Sun]], which is probably due to the influence of Jovian heat flux (<9 [[kelvin]]), sunlight reflected from the planet (<5&nbsp;K), and charged particle bombardment (<2&nbsp;K).{{sfn|Simonelli|1983}} This is a trait shared with [[Io (moon)|Io]], although for different reasons.

=== Named geological features ===
There are four named geological features on Amalthea: two craters and two [[facula]]e (bright spots).{{sfn|USGS: Jupiter: Amalthea}} The faculae are located on the edge of a ridge on the anti-Jupiter side of Amalthea.{{sfn|Thomas Burns et al.|1998}}

[[File:Amalthea (moon).png|thumb|170px|right|Leading side of Amalthea. North is up, and Jupiter is beyond the right side. Crater Pan is seen on the upper right edge, and Gaea on the lower. Ida Facula and Lyctos Facula are on the left end (upper and lower brightenings respectively)]]

{| class="wikitable plainrowheaders"
|-
! scope="col" | Feature
! scope="col" | Named after
|-
! scope="row" | [[Pan (crater)]]
| [[Pan (mythology)|Pan]], Greek god
|-
! scope="row" | [[Gaea (crater)]]
| [[Gaia (mythology)|Gaia]], Greek goddess
|-
! scope="row" | [[Lyctos Facula]]
| [[Lyctus]], [[Crete]]
|-
! scope="row" | [[Ida Facula]]
| [[Mount Ida]], [[Crete]]
|}

== Relationship with Jupiter's rings ==
Due to [[tidal force]] from Jupiter and Amalthea's low density and irregular shape, the [[escape velocity]] at its surface points closest to and furthest from [[Jupiter]] is no more than 1&nbsp;m/s and dust can easily escape from it after, e.g. micrometeorite impacts; this dust forms the [[Rings of Jupiter#Amalthea gossamer ring|Amalthea Gossamer Ring]].{{sfn|Burns Simonelli et al.|2004}}

During its flyby of Amalthea, the ''Galileo'' orbiter's [[Galileo (spacecraft)#The Galileo star scanner|star scanner]] detected nine flashes that appear to be small [[moonlet]]s near the orbit of Amalthea. Because they were sighted only from one location, their true distances could not be measured. These moonlets may be anywhere in size from gravel to stadium-sized. Their origins are unknown, but they may be gravitationally captured into current orbit or they may be ejecta from [[impact event|meteor impact]]s on Amalthea. On the next and final orbit (just an hour before destruction), ''Galileo'' detected one more such moonlet. However, this time Amalthea was on the other side of the planet, so it is probable that the particles form a [[planetary ring|ring]] around the planet near Amalthea's orbit.<ref name=Fieseler_2004/><ref name=JPL_2003/><ref name=IAU_2003/><ref name=Lakdawalla_2013/>

== Views to and from Amalthea ==
{{see also|Extraterrestrial skies}}

From Jupiter's surface—or rather, from just above its cloudtops—Amalthea would appear very bright, shining with a [[Apparent magnitude|magnitude]] of −4.7,{{efn|name=stub2}} similar to that of [[Venus]] from Earth. At only 8&nbsp;[[arcminutes]] across,{{efn|name=sizecalc}} its disc would be barely discernible. Amalthea's orbital period is only slightly longer than its parent planet's day (about 20% in this case), which means it would cross Jupiter's sky very slowly. The time between moonrise and moonset would be over 29&nbsp;hours.{{efn|name=stub2}}
[[File:Jupiter from Amalthea.jpg|thumb|Simulated view of Jupiter from Amalthea.]]
From the surface of Amalthea, Jupiter would look enormous: 46&nbsp;[[degree (angle)|degree]]s across,{{efn|name=sizecalc}} it would appear roughly 92 times larger than the [[full moon]]. Because Amalthea is in [[synchronous rotation]], Jupiter would not appear to move, and would not be visible from one side of Amalthea. The Sun would disappear behind Jupiter's bulk for an hour and a half each revolution, and Amalthea's short rotation period gives it just under six hours of [[wiktionary:daylight|daylight]]. Though Jupiter would appear 900&nbsp;times brighter than the full moon, its light would be spread over an area some 8500&nbsp;times greater and it would not look as bright per surface unit.{{efn|name=stub2}}

== Exploration ==
During 1979, the unmanned ''[[Voyager 1]]'' and ''[[Voyager 2]]'' space probes obtained the first images of Amalthea to resolve its surface features.{{sfn|Thomas Burns et al.|1998}} They also measured the visible and infrared [[Spectrum|spectra]] and surface temperature.{{sfn|Simonelli|1983}} Later, the [[Galileo spacecraft|''Galileo'' orbiter]] completed the imaging of Amalthea's surface. ''Galileo'' made its final satellite fly-by at a distance of approximately {{convert|244|km|mi|abbr=on}} from Amalthea's center (at a height of about 160–170&nbsp;km) on 5 November 2002, permitting the moon's mass to be accurately determined, while changing ''Galileo's'' trajectory so that it would plunge into Jupiter in September 2003 at the end of its mission.{{sfn|Anderson Johnson et al.|2005}} In 2006, Amalthea's orbit was refined with measurements from ''[[New Horizons]]''.

== In fiction ==
{{main|Jupiter's moons in fiction#Amalthea|l1=Amalthea in fiction}}

Amalthea is the setting of several works of [[science fiction]], including stories by [[Arthur C. Clarke]] and [[James Blish]].

== Notes ==
{{notes
| notes =
{{efn
| name = stub
| Calculated on the basis of other parameters.
}}
{{efn
| name = stub2
| 1= Calculated on the basis of known distances, sizes, periods and visual magnitudes as visible from the Earth. Visual magnitudes as seen from Jupiter m<sub>j</sub> are calculated from visual magnitudes on Earth m<sub>v</sub> using the formula m<sub>j</sub>=m<sub>v</sub>−log<sub>2.512</sub>(I<sub>j</sub>/I<sub>v</sub>), where I<sub>j</sub> and I<sub>v</sub> are respective brightnesses (see [[visual magnitude]]), which scale according to the inverse square law. For visual magnitudes see http://www.oarval.org/ClasSaten.htm and [[Jupiter (planet)]].
}}
{{efn
| name = sizecalc
| Calculated from the known sizes and distances of the bodies, using the formula 2*arcsin(R<sub>b</sub>/R<sub>o</sub>), where R<sub>b</sub> is the radius of the body and R<sub>o</sub> is the radius of Amalthea's orbit or distance from the Jovian surface to Amalthea.
}}
}}

== References ==
{{reflist
| colwidth = 20em
| refs =
<ref name=Bakich_2000>{{cite book
 |author    =Bakich M. E.
 |title     =The Cambridge Planetary Handbook
 |publisher =Cambridge University Press
 |date      =2000
 |pages     =220–221
 |isbn      =9780521632805
 |url       =https://books.google.com/books?id=PE99nOKjbXAC&pg=PA221&dq=Amalthea
}}</ref>
<ref name=Blunck_2010>{{cite book
 |author    =Blunck J.
 |title     =Solar System Moons: Discovery and Mythology
 |date      =2010
 |publisher =Springer
 |pages     =9–15
 |bibcode   =2010ssm..book.....B
 |isbn      =978-3-540-68852-5
 |doi       =10.1007/978-3-540-68853-2
 |url       =http://jdsweb.jinr.ru/record/52129/files/Solar%20System%20Moons.pdf
}}</ref>
<ref name=Fieseler_2004>{{cite journal
 |author  =Fieseler P. D.
 |author2  =Adams O. W.
 |author3  =Vandermey N.
 |author4  =Theilig E. E.
 |author5  =Schimmels K. A.
 |author6  =Lewis G. D.
 |author7  =Ardalan S. M.
 |author8  =Alexander C. J.
 |title   =The Galileo star scanner observations at Amalthea
 |date    =2004
 |journal =Icarus
 |volume  =169
 |issue   =2
 |pages   =390–401
 |doi     =10.1016/j.icarus.2004.01.012
 |bibcode =2004Icar..169..390F
}}</ref>
<ref name=gazetteer_names>{{cite web
 |url         = http://planetarynames.wr.usgs.gov/Page/Planets
 |title       = Planet and Satellite Names and Discoverers
 |publisher   = International Astronomical Union (IAU) Working Group for Planetary System Nomenclature (WGPSN)
 |work        = Gazetteer of Planetary Nomenclature
 |accessdate  = 2014-10-08
 |archiveurl  = https://web.archive.org/web/20140821014052/http://planetarynames.wr.usgs.gov/Page/Planets
 |archivedate = 2014-08-21
}}</ref>
<ref name=IAU_1975>{{cite web
 |url         = http://www.cbat.eps.harvard.edu/iauc/02800/02846.html#Item6
 |title       = Satellites of Jupiter
 |work        = International Astronomical Union Circular 2846
 |author      = Flammarion C.
 |author2      = Kowal C.
 |author3      = Blunck J.
 |publisher   = Central Bureau for Astronomical Telegrams
 |accessdate  = 2014-10-17
 |date        = 1975-10-07
 |archiveurl  = https://web.archive.org/web/20140222215122/http://www.cbat.eps.harvard.edu/iauc/02800/02846.html
 |archivedate = 2014-02-22
}} ({{bibcode|1975IAUC.2846....6F}})</ref>
<ref name=IAU_2003>{{cite web
 |url         = http://www.cbat.eps.harvard.edu/iauc/08100/08107.html#Item2
 |title       = Objects near Jupiter V (Amalthea)
 |author      = Fieseler P. D.
 |author2      = Ardalan S. M.
 |work        = International Astronomical Union Circular 8107
 |publisher   = Central Bureau for Astronomical Telegrams
 |accessdate  = 2014-10-12
 |date        = 2003-04-04
 |archiveurl  = https://web.archive.org/web/20140302034543/http://www.cbat.eps.harvard.edu/iauc/08100/08107.html
 |archivedate = 2014-03-02
}} ({{bibcode|2003IAUC.8107....2F}})</ref>
<ref name=JPL_2003>{{Cite journal
 |publisher   = Jet Propulsion Laboratory
 |date        = 9 April 2003
 |title       = Another Find for Galileo
 |url         = http://solarsystem.nasa.gov/galileo/news/display.cfm?News_ID=4939
 |accessdate  = 2012-03-27
 |archiveurl  = https://web.archive.org/web/20041104163021/http://www.solarsystem.nasa.gov/galileo/news/display.cfm?News_ID=4939
 |archivedate = 2004-11-04
}}</ref>
<ref name=Lakdawalla_2013>{{cite web
 |url        =http://www.planetary.org/blogs/emily-lakdawalla/2013/05170800-serendipitous-observation-amalthea-rocks.html
 |title      =A serendipitous observation of tiny rocks in Jupiter's orbit by Galileo
 |author     =Emily Lakdawalla
 |publisher  =The Planetary Society
 |date       =2013-05-17
 |accessdate =2014-10-14
 |archiveurl =https://web.archive.org/web/20140814150855/http://www.planetary.org/blogs/emily-lakdawalla/2013/05170800-serendipitous-observation-amalthea-rocks.html
 |archivedate=2014-08-14
}}</ref>
}}

'''Cited sources'''

* {{cite journal| doi = 10.1126/science.1110422| last1 = Anderson | first1 = J. D.| last2 = Johnson | first2 = T. V.| last3 = Schubert | first3 = G.| last4 = Asmar | first4 = S.| last5 = Jacobson | first5 = R. A.| last6 = Johnston | first6 = D.| last7 = Lau | first7 = E. L.| last8 = Lewis | first8 = G.| last9 = Moore | first9 = W. B.| date = 27 May 2005| last10 = Taylor | first10 = A.| last11 = Thomas | first11 = P. C.| last12 = Weinwurm | first12 = G.| title = Amalthea's Density is Less Than That of Water| journal = Science| volume = 308| issue = 5726| pages = 1291–1293| pmid = 15919987| bibcode = 2005Sci...308.1291A| ref = {{sfnRef|Anderson Johnson et al.|2005}}}}
* {{cite journal| doi = 10.1086/101715| last = Barnard| first = E. E.| date = 12 October 1892| title = Discovery and observations of a fifth satellite to Jupiter| journal = The Astronomical Journal| volume = 12| issue = 11| pages = 81–85| bibcode = 1892AJ.....12...81B| ref = harv}}
* {{cite journal| doi = 10.1126/science.284.5417.1146| last1 = Burns| first1 = Joseph A.| last2 = Showalter| first2 = Mark R.| last3 = Hamilton| first3 = Douglas P.| last4 = Nicholson| first4 = Philip D.| last5 = de Pater| first5 = Imke| last6 = Ockert-Bell| first6 = Maureen E.| last7 = Thomas| first7 = Peter C.| date = 14 May 1999| title = The Formation of Jupiter's Faint Rings| journal = Science| volume = 284| issue = 5417| pages = 1146–1150| pmid = 10325220| bibcode = 1999Sci...284.1146B| ref = {{sfnRef|Burns Showalter et al.|1999}}}}
* {{cite encyclopedia
  | last1 = Burns
  | first1 = Joseph A.
  | last2 = Simonelli
  | first2 = Damon P.
  | last3 = Showalter
  | first3 = Mark R.
  | last4 = Hamilton
  | first4 = Douglas P.
  | last5 = Porco
  | first5 = Carolyn C.
  | last6 = Throop
  | first6 = Henry
  | last7 = Esposito
  | first7 = Larry W.
  | date = 2004
  | pages = 241–262
  | title = Jupiter's Ring-Moon System
  | publisher = Cambridge University Press
  | editor1-last = Bagenal
  | editor1-first = Fran
  | editor2-last = Dowling
  | editor2-first = Timothy E.
  | editor3-last = McKinnon
  | editor3-first = William B.
  | url = http://www.astro.umd.edu/~hamilton/research/preprints/BurSimSho03.pdf
  | bibcode = 2004jpsm.book..241B
  | isbn = 978-0-521-81808-7
  | ref = {{sfnRef|Burns Simonelli et al.|2004}}
  | journal = Jupiter: the Planet, Satellites and Magnetosphere
  }}
* {{cite journal| doi = 10.1016/j.icarus.2005.11.007| last1 = Cooper | first1 = N. J.| last2 = Murray | first2 = C. D.| last3 = Porco | first3 = C. C.| last4 = Spitale | first4 = J. N.| date=March 2006 | title = Cassini ISS astrometric observations of the inner jovian satellites, Amalthea and Thebe| journal = Icarus| volume = 181| issue = 1| pages = 223–234| bibcode = 2006Icar..181..223C| ref = {{sfnRef|Cooper Murray et al.|2006}}}}
* {{cite journal
  | last = Flammarion
  | first = Camille
  | title = Le Nouveau Satellite de Jupiter
  | journal = L'Astronomie
  | volume = 12
  | date = 1893
  | pages = 91–94
  | bibcode = 1893LAstr..12...91F
  | ref = {{sfnRef|Flammarion|1893}}
  }}
* {{cite web
  | author = Observatorio ARVAL
  | title = Classic Satellites of the Solar System
  | publisher = Observatorio ARVAL
  | date = April 15, 2007
  | url = http://www.oarval.org/ClasSaten.htm
  | accessdate = 2011-12-17
  | ref = {{sfnRef|Observatorio ARVAL}}
  }}
* {{cite journal| doi = 10.1016/0019-1035(83)90244-0| last = Simonelli | first = D. P.| date=June 1983 | title = Amalthea: Implications of the temperature observed by Voyager| journal = Icarus| issn = 0019-1035| volume = 54| issue = 3| pages = 524–538| bibcode = 1983Icar...54..524S| ref = harv}}
* {{cite journal| doi = 10.1006/icar.2000.6474| last1 = Simonelli | first1 = D. P.| last2 = Rossier | first2 = L.| last3 = Thomas | first3 = P. C.| last4 = Veverka | first4 = J.| last5 = Burns | first5 = J. A.| last6 = Belton | first6 = M. J. S.| date=October 2000 | title = Leading/Trailing Albedo Asymmetries of Thebe, Amalthea, and Metis| journal = Icarus| volume = 147| issue = 2| pages = 353–365| bibcode = 2000Icar..147..353S| ref = {{sfnRef|Simonelli Rossier et al.|2000}}}}
* {{cite web|publisher=Space.com |date=9 December 2002 |title=Swiss Cheese Moon: Jovian Satellite Full of Holes |url=http://www.space.com/scienceastronomy/almathea_update_021209.html |ref={{sfnRef|Swiss Cheese Moon}} |deadurl=yes |archiveurl=https://web.archive.org/web/20080828222245/http://www.space.com/scienceastronomy/almathea_update_021209.html |archivedate=28 August 2008 }} 
* {{cite journal| doi = 10.1126/science.1105427| last1 = Takato| first1 = Naruhisa| last2 = Bus| first2 = Schelte J.| last3 = Terada| first3 = H.| last4 = Pyo| first4 = Tae-Soo| last5 = Kobayashi| first5 = Naoto| date = 24 December 2004| title = Detection of a Deep 3-μm Absorption Feature in the Spectrum of Amalthea (JV)| journal = Science| volume = 306| issue = 5705| pages = 2224–2227| pmid = 15618511| bibcode = 2004Sci...306.2224T| ref = {{sfnRef|Takato Bus et al.|2004}}}}
* {{cite journal| doi = 10.1006/icar.1998.5976| last1 = Thomas | first1 = P. C.| last2 = Burns | first2 = J. A.| last3 = Rossier | first3 = L.| last4 = Simonelli | first4 = D.| last5 = Veverka | first5 = J.| last6 = Chapman | first6 = C. R.| last7 = Klaasen | first7 = K.| last8 = Johnson | first8 = T. V.| last9 = Belton | first9 = M. J. S.| author10 = Galileo Solid State Imaging Team| date =September 1998| title = The Small Inner Satellites of Jupiter| journal = Icarus| volume = 135| issue = 1| pages = 360–371| bibcode = 1998Icar..135..360T| ref = {{sfnRef|Thomas Burns et al.|1998}}}}
* {{cite web
  | author = [[USGS]]/[[IAU]]
  | title = Amalthea Nomenclature
  | publisher = USGS Astrogeology
  | work = Gazetteer of Planetary Nomenclature
  | url = http://planetarynames.wr.usgs.gov/SearchResults?target=AMALTHEA
  | accessdate = 2012-03-27
  | ref = {{sfnRef|USGS: Jupiter: Amalthea}}
  }}

== External links ==
{{Commons category|Amalthea}}
* [http://solarsystem.nasa.gov/planets/profile.cfm?Object=Jup_Amalthea Amalthea Profile] by [http://solarsystem.nasa.gov NASA's Solar System Exploration]
* [http://planetarynames.wr.usgs.gov/Page/AMALTHEA/target Amalthea nomenclature] from the [http://planetarynames.wr.usgs.gov USGS planetary nomenclature page]
* [http://solarsystem.nasa.gov/galileo/news/display.cfm?News_ID=3919 Jupiter's Amalthea Surprisingly Jumbled]&nbsp;– JPL press release (2002-12-09)
* [https://web.archive.org/web/20130313165916/http://www.skyhighgallery.com/gallery-page-14.htm ''Jupiter From Amalthea''], a painting by Frank Hettick, 2002.
* [http://space.frieger.com/asteroids/asteroids.php?id=J5 Animated 3D shape model of Amalthea]

==See also==
*[[Galilean moons]] (the four biggest moons of Jupiter)

{{Moons of Jupiter}}

{{Authority control}}

{{DEFAULTSORT:Amalthea (Moon)}}
[[Category:Amalthea (moon)| ]]
[[Category:Moons of Jupiter]]
[[Category:Discoveries by Edward Emerson Barnard]]
[[Category:Astronomical objects discovered in 1892|18920909]]