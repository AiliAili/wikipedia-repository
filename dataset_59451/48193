{{More footnotes|date=January 2012}}

{{Infobox artist
 | name          = Berta Rosenbaum Golahny 
 | image         = File:Self Portrait with Parents, by Berta Rosenbaum Golahny, intaglio (etching and softground), 1949, 18" x 13".jpg
 | image_size    =
 | alt           =
 | caption       = Self Portrait with Parents, 1949
 | birth_name    =  
 | birth_date    = {{Birth date|df=yes|1925|02|07}}
 | birth_place   = 
 | death_date    = {{Death date and age|df=yes|2005|11|04|1925|02|07}}
 | death_place   =
 | nationality   = American
 | spouse        = Yehuda Golahny
 | field         = Painting, printmaking, sculpture
 | training      = [[Cass Technical High School]]<br>[[Art Students League of New York]]<br>[[Art Institute of Chicago]]<br>[[University of Iowa]] 
 | movement      =
 | works         =
 | patrons       = 
 | influenced by =
 | influenced    =
 | awards        =
 | elected       =
}}
'''Berta Rosenbaum Golahny''' (February 7, 1925 – November 4, 2005) was an [[United States|American]] painter, printmaker, and sculptor.

==Biography==
Golahny was born into a Jewish immigrant family in [[Detroit, Michigan]]. Her parents were Fannie Henkin Rosenbaum (1889–1953, b. in Belarus) and Gedaliah Rosenbaum (1889–1985, b. in Wlodova, Poland). As a child, Golahny began to draw while watching her father design wrought-iron pieces for the company he founded, Liberty Ironworks, some of whose ornamental gates and railings remain standing in Detroit.

She was educated at Detroit’s [[Cass Technical High School]] and then in 1943-4 the [[Art Students League of New York]], having received a National Scholarship. There, [[George Grosz]] encouraged her drawing and French artist [[Ossip Zadkine]] introduced her to sculpture.

{|style=""float:margin: 0 auto;"
| [[File:Golahny standing on stool, by high school classmates with whom she painted mural of the Four Freedoms for a Detroit storefront, 1943.jpg|thumb|upright|Golahny (standing on stool) with high school classmates with whom she designed and painted a mural of the Four Freedoms for a Detroit storefront, 1943]]
| [[File:Berta Golahny and her sculpture "Sheba".jpg|thumb|upright|Golahny and her sculpture ''Sheba'']]
|}

Golahny continued her studies at the [[Art Institute of Chicago]]. After receiving her Bachelor's in Fine Art from the Art Institute in 1947, Golahny completed her studies at the [[University of Iowa]], from which she received a Master's in Fine Art in 1950. At Iowa, she studied printmaking under [[Mauricio Lasansky]], art history under [[William S. Heckscher]], and painting under [[Eugene Ludins]]. Her thesis painting, ''The Resurrection'', was awarded the Painting Prize by juror [[Ben Shahn]]. In 1951, she was awarded a fellowship from [[The Louis Comfort Tiffany Foundation]]. While at Iowa, she married Yehuda Golahny, an engineering student in Detroit. When Yehuda began to pursue a Master’s of Science in Electrical Engineering at [[MIT]] (class of 1954), the couple moved to [[Cambridge, Massachusetts]]. After two years, they moved to [[Newton, Massachusetts]], where they settled.

{|style="margin: 0 auto;"
| [[File:Sinai Summit, by Berta Rosenbaum Golahny, woodcut, 1970s, 36" x 20".jpg|thumb|left|upright| center|''Sinai Summit'', by Golahny, woodcut, 1970s, 36" x 20"]]
| [[File:Joy, by Berta Rosenbaum Golahny, oil on canvas, 12" x 12", 1990.jpg|thumb|upright|center|''Joy'' by Golahny, oil on canvas, 1990, 12" x 12"]]
| [[File:Purple and Ash, by Berta Rosenbaum Golahny, wood engraving, 15" x 14".jpg|thumb|''Purple and Ash'', by Golahny, wood engraving, 15" x 14"]]
|}

From 1959 to 2001, she taught at the [[Cambridge Center for Adult Education]].<ref>Ackerman, Jerry. "Suddenly, Monday Nights Without Berta as a Teacher Steps Aside." ''The Boston Globe'' 19 August 2001.</ref> Her students took her painting classes on Monday nights, often continually for years. She exhibited in the USA and other countries in several hundred juried and invitational shows. Today her work is held in private collections in America, France, and Israel, and in museums including the [[Fogg Art Museum]] at Harvard University, the Wichita Art Museum, and the E. J. Pratt Library at the University of Toronto.

Golahny used the traditional media of etching, wood engraving, and woodcut. She experimented with monotype, with different ways of biting the plate, and with electric tools to incise lines upon zinc and copper plates.

[[File:Street Car Scene, by Berta Rosenbaum Golahny, 1940s.jpg|thumb|''Street Car Scene'', by Golahny, oil on canvas, 1940s]]

Artists with whom Golahny found affinity include [[Max Beckmann]], [[Paul Cézanne]], [[Wassily Kandinsky]], [[Paul Klee]], [[Franz Marc]], and [[Nicolas de Staël]].

Golahny’s earliest work uses a darker palette and rougher line than her later work. This early work, such as ''Street Car Scene'', reflects city life in Detroit, New York City, and Chicago: people on buses or trains, workers in factories, and children at play.

One series of works, begun around 1964 with a multi-block color woodcut, was titled ''Landscape of Man in the Nuclear Age''. The series continued in intaglio, painting, wood engraving, and copper engraving, and was completed in 1988. Golahny repeatedly portrayed human suffering, as in a series of works on the Holocaust.

{|style="margin: 0 auto;"
| [[File:Landscape of Man in the Nuclear Age 2, by Berta Rosenbaum Golahny, oil on canvas, 48" x 30".jpg|thumb|center|upright|''Landscape of Man in the Nuclear Age #2'', by Golahny, oil on canvas, 1984, 48" x 30"]] 
| [[File:Concentration Camp 1, by Berta Rosenbaum Golahny, oil on canvas, 30" x 24".jpg|thumb|left|center|upright|''Concentration Camp #1'', by Golahny, oil on canvas, 1985, 30" x 24"]]
|}

A visit to a Midwest state fair inspired an intaglio print of 1949 titled ''Children at the Fair: The Ride''. Golahny reprised this composition of a whirligig (a central pole with carts swinging from it) in a 1987 woodcut and in several subsequent large paintings.

Inspired by early publications on nebulae and black holes, Golahny began the ''Space'' series in 1980. In dozens of paintings she modeled her images on photographs of cosmic exploration in the archives of the [[Harvard College Observatory]]. She also painted evocative layerings of an imagined passage through space and time, and fantastic semi-formed creatures that exist in a life outside our experience.<ref>Golahny, Berta. "How I Came To Paint The Crab Nebula: The Development Of Cosmic Themes In My Oil Paintings." ''Leonardo: International Quarterly of the Arts and Sciences'' 23.4 (1990).</ref>

{|style="margin: 0 auto;"
| [[File:Children At The Fair 2, 1990, 60x40.jpg|thumb|left|upright|center|''Children at the Fair #2'', by Golahny, oil painting, 1990, 60" x 40". The text is a quotation from [[Niels Bohr]]: "We are at the same time actors and spectators in the great drama of nature."]]
| [[File:Crab Nebula 1, by Berta Rosenbaum Golahny, oil on canvas, 1980s, 30" x 36".jpg|thumb|center|''Crab Nebula #1'', by Golahny, oil on canvas, 1980s, 30" x 36"]]
| [[File:Being and Becoming 2, by Berta Rosenbaum Golahny, oil on canvas, 60" x 38".jpg|thumb|left|upright|''Being and Becoming #2'', by Golahny, oil on canvas, 1990, 60" x 38"]]
|}

The ''Being and Becoming'' series concerns the expansion of the universe since the Big Bang. Inspired by this series and other paintings, Boston-based musicians Paul and Rosalie DiCrescenzo wrote a four-movement score to accompany a slide-show of the images, titled ''The Watchers and the Watched''. This was performed with support from the Massachusetts Council on the Arts in 1995. It was performed again at the Newton Free Library in December 2006 in commemoration of Golahny’s life and work.<ref>'"Art/music presentation 'The Watchers and the Watched' at library." ''Newton Tab'' 13 December 2006.</ref> Golahny died in November 2005.<ref>Stickgold, Emma. "Berta Rosenbaum Golahny; taught art and also created it; at 80" [obituary]. ''The Boston Globe'' 7 November 2005.</ref>

Her response to the World Trade Center attacks of September 11, 2001 was to paint two large canvases in a series called ''The Striving''.

[[File:The Striving - 9 11 and History, by Berta Rosenbaum Golahny, oil on canvas, 2003, 60" x 32".jpg|thumb|right|''The Striving - 9/11 and History #2'', by Golahny, oil on canvas, 2003, 60" x 32"]]

==References==
{{reflist}}

*Jerry Ackerman, "Suddenly, Monday Nights Without Berta as a Teacher Steps Aside," ''The Boston Globe'', Aug. 19, 2001.
*Dr. Alicia Faxon, "Commentary on 'How I Came To Paint The Crab Nebula,'" ''Leonardo'', 24.1, 1991.
*Berta Golahny, "How I Came To Paint The Crab Nebula: The Development Of Cosmic Themes In My Oil Paintings," ''Leonardo:  International Quarterly of the Arts and Sciences'', 23.4, 1990.
*Donald Judd, ''Complete Writings, 1959-1975: Gallery Reviews, Book reviews, Articles, Letters to the editor, Reports, Statements, Complaints'', Press of the Nova Scotia College of Art and Design, 1975, p.&nbsp;4.
*Stephanie Rauschenbusch, "Three Visions: Juliani Gallery, Massachusetts Bay Community College Wellesley Hills, MA, July- August 1987 (Selma Bromberg, Berta Golahny and Ellen Milan)" ''Women Artists News'', Vol. 12, Midmarch Associates, 1987.
*Emma Stickgold, "Berta Rosenbaum Golahny; taught art and also created it; at 80" [obituary], ''The Boston Globe'', November 7, 2005.
*'"Art/music presentation 'The Watchers and the Watched' at library," ''Newton Tab'', Dec. 13, 2006.

{{DEFAULTSORT:Golahny, Berta Rosenbaum}}
[[Category:1925 births]]
[[Category:2005 deaths]]
[[Category:Artists from Detroit]]
[[Category:American people of Belarusian-Jewish descent]]
[[Category:American people of Polish-Jewish descent]]
[[Category:20th-century American painters]]
[[Category:21st-century American painters]]
[[Category:20th-century American sculptors]]
[[Category:20th-century American printmakers]]
[[Category:American women painters]]
[[Category:20th-century women artists]]
[[Category:21st-century women artists]]