{{About|the book|the film|Twice-Told Tales (film)}}
{{Infobox book <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name         = Twice-Told Tales
| title_orig   =
| translator   =
| image        = Twice told tales.jpg
| caption      = Cover of the first edition
| author       = [[Nathaniel Hawthorne]]
| illustrator  =
| cover_artist =
| country      = [[United States]]
| language     = [[English language|English]]
| series       =
| genre        = [[Short stories]]
| publisher    = [[American Stationers Co.]]
| release_date = 1837
| media_type   = Print ([[Hardcover|Hardback]])
| pages        = 334 pp
| preceded_by  =
| followed_by  =
}}

'''''Twice-Told Tales''''' is a [[short story]] collection in two volumes by [[Nathaniel Hawthorne]]. The first was published in the spring of 1837, and the second in 1842.<ref>Roy Harvey Pearce, "Introduction" in Nathaniel Hawthorne, ''Twice-Told Tales'', New York: Dutton, 1967, pp. v-vi.</ref> The stories had all been previously published in magazines and annuals, hence the name.

==Publication==
Hawthorne was encouraged by friend [[Horatio Bridge]] to collect these previously anonymous stories; Bridge offered $250 to cover the risk of the publication.<ref>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2005: 22. ISBN 0-8021-1776-7</ref> Many had been published in ''[[The Token and Atlantic Souvenir|The Token]]'', edited by [[Samuel Griswold Goodrich]]. When the works became popular, Bridge revealed Hawthorne as the author in a review he published in the ''Boston Post''.<ref>[[Brenda Wineapple|Wineapple, Brenda]]. ''Hawthorne: A Life''. New York: Random House, 2004: 90. ISBN 0-8129-7291-0</ref>

The title, ''Twice-Told Tales'', was based on a line from [[William Shakespeare]]'s ''[[The Life and Death of King John]]'' (Act 3, scene 4): "Life is as tedious as a twice-told tale, / Vexing the dull ear of a drowsy man."<ref>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 92–93. ISBN 0-8129-7291-0</ref> The quote referenced may also be Hawthorne's way of acknowledging a belief that many of his stories were ironic retellings of familiar tropes.<ref>Howe, Susan. ''The Birth-mark: Unsettling the Wilderness in American Literary History''. Middletown, CT: Wesleyan University Press, 1993: 5. ISBN 0819562637</ref> The book was published by the American Stationers' Company on March 6, 1837; its cover price was one dollar.<ref>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 92. ISBN 0-8129-7291-0</ref> Hawthorne had help in promoting the book from [[Elizabeth Peabody]]. She sent copies of the collection to [[William Wordsworth]] as well as to [[Horace Mann]], hoping that Mann could get Hawthorne a job writing stories for schoolchildren.<ref>Marshall, Megan. ''The Peabody Sisters: Three Women Who Ignited American Romanticism''. Boston: Mariner Books, 2006: 356. ISBN 978-0-618-71169-7</ref>

After publication, Hawthorne asked a friend to check with the local bookstore to see how it was selling. After noting the initial expenses for publishing had not been met, he complained: "Surely the book was puffed enough to meet with sale. What the devil's the matter?"<ref>Schreiner, Samuel A., Jr. ''The Concord Quartet: Alcott, Emerson, Hawthorne, Thoreau, and the Friendship that Freed the American Mind''. Hoboken, NJ: John Wiley and Sons, 2006: 120. ISBN 978-0-471-64663-1.</ref> By June, between 600 and 700 copies were sold but sales were soon halted by the [[Panic of 1837]] and the publisher went out of business within a year.<ref name=Wineapple93>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 93. ISBN 0-8129-7291-0</ref>

On October 11, 1841, Hawthorne signed a contract with publisher James Munroe to issue a new, two-volume edition of ''Twice-Told Tales'' with 21 more works than the previous edition. 1,000 copies were published in December of that year with a cover price of $2.25; Hawthorne was paid 10 percent per copy.<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Baltimore: The Johns Hopkins University Press, 1980: 192. ISBN 0-8018-5900-X</ref> Hawthorne complained that he still struggled financially. Editor [[John L. O'Sullivan]] suggested Hawthorne buy back unsold copies of ''Twice-Told Tales'' so that they could be reissued through a different publisher. At the time of this suggestion, 1844, there were 600 unsold copies of the book. Hawthorne lamented, "I wish Heaven would make me rich enough to buy the copies for the purpose of burning them."<ref>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 183. ISBN 0-8129-7291-0</ref>

After the success of ''[[The Scarlet Letter]]'' in 1850, ''Twice-Told Tales'' was reissued with the help of publisher [[James Thomas Fields]]. In a new preface, Hawthorne wrote that the stories "may be understood and felt by anybody, who will give himself the trouble to read it, and will take up the book in a proper mood."<ref>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 229–231. ISBN 0-8129-7291-0</ref>

==Critical response==
[[File:Nathaniel Hawthorne.jpg|thumb|Nathaniel Hawthorne portrait by Charles Osgood, 1840]]
About a week after the publication of the book, Hawthorne sent a copy to his classmate from [[Bowdoin College]], the poet [[Henry Wadsworth Longfellow]].<ref>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City, University of Iowa Press, 1991: 120. ISBN 0-87745-381-0</ref> Longfellow had given a speech at their [[Graduation|commencement]] calling for notable contributions to American literature. By this time, Longfellow was working at [[Harvard University]] and was becoming popular as a poet. Hawthorne wrote to him, "We were not, it is true, so well acquainted at college, that I can plead an absolute right to inflict my 'twice-told' tediousness upon you; but I have often regretted that we were not better known." In his 14-page critique in the April issue of the ''[[North American Review]]'', Longfellow praised the book as a work of genius.<ref>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2004: 58–59. ISBN 0-8021-1776-7</ref> "To this little book", Longfellow wrote, "we would say, 'Live ever, sweet, sweet book.' It comes from the hand of a man of genius."<ref>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City, University of Iowa Press, 1991: 121. ISBN 0-87745-381-0</ref> For his review of the second edition, Longfellow noted that Hawthorne's writing "is characterized by a large proportion of feminine elements, depth and tenderness of feeling, exceeding purity of mind."<ref name=Mellow193>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Baltimore: The Johns Hopkins University Press, 1980: 193. ISBN 0-8018-5900-X</ref> He referred to the collection's "The Gentle Boy" as "on the whole, the finest thing he ever wrote".<ref>Miller, Edwin Haviland. ''Salem Is My Dwelling Place: A Life of Nathaniel Hawthorne''. Iowa City: University of Iowa Press, 1991: 43. ISBN 0-87745-332-2</ref> The two authors would eventually build a strong friendship.<ref>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2005: 19. ISBN 0-8021-1776-7</ref>

Generally, reviews were positive. [[Park Benjamin, Sr.]] said that the author was "a rose baptized in dew".<ref name=Wineapple93/> For the ''Boston Quarterly Review'', [[Orestes Brownson]] noted Hawthorne's writings as "a pure and living stream of manly thought and feeling, which characterizes always the true man, the Christian, the republican and the patriot."<ref name=Mellow193/> After reading ''Twice-Told Tales'', [[Herman Melville]] wrote to [[Evert Augustus Duyckinck]] that the stories weren't meaty enough. "Their deeper meanings are worthy of a [[Boston Brahmin|Brahmin]]. Still there is something lacking—a good deal lacking to the plump sphericity of the man. What is that?—He does'nt patronise the butcher—he needs roast-beef, done rare."<ref>Wineapple, Brenda. ''Hawthorne: A Life''. New York: Random House, 2004: 228–229. ISBN 0-8129-7291-0</ref>

[[Edgar Allan Poe]] wrote a well-known two-part review of the second edition of ''Twice-Told Tales'', published in the April and May 1842 issues of ''[[Graham's Magazine]]''. Poe particularly praised Hawthorne's originality as "remarkable".<ref name=Mellow193/> He nonetheless criticized Hawthorne's reliance on [[allegory]] and the didactic, something he called a "[[heresy]]" to American literature. He did, however, express praise at the use of short stories (Poe was a tale-writer himself) and said they "rivet the attention" of the reader.<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001: 233. ISBN 978-0-8160-4161-9</ref> Poe admitted, "The style of Hawthorne is purity itself. His tone is singularly effective--wild, plaintive, thoughtful, and in full accordance with his themes." He concluded that, "we look upon him as one of the few men of indisputable genius to whom our country has as yet given birth."<ref>McFarland, Philip. ''Hawthorne in Concord''. New York: Grove Press, 2005: 88–89. ISBN 0-8021-1776-7</ref>

The [[Grolier Club]] later named ''Twice-Told Tales'' the most influential book of 1837.<ref>Nelson, Randy F. ''The Almanac of American Letters''. Los Altos, California: William Kaufmann, Inc., 1981: 19. ISBN 0-86576-008-X</ref>

==Contents==
{{colbegin|colwidth=22em}}
*"The Gray Champion"
*"[[Sunday at Home]]"
*"[[The Wedding-Knell]]"
*"[[The Minister's Black Veil]]"
*"[[The Maypole of Merry Mount|The May-Pole of Merry Mount]]"
*"[[The Gentle Boy]]"
*"[[Mr. Higginbotham's Catastrophe]]"
*"[[Little Annie's Ramble]]"
*"[[Wakefield (short story)|Wakefield]]"
*"[[A Rill from the Town-Pump]]"
*"[[The Great Carbuncle]]"
*"[[The Prophetic Pictures]]"
*"[[David Swan]]"
*"[[Sights from a Steeple]]"
*"[[The Hollow of the Three Hills]]"
*"[[The Toll-Gatherer's Day]]"
*"[[The Vision of the Fountain]]"
*"[[Fancy's Show Box]]"
*"[[Dr. Heidegger's Experiment]]"
*"[[Legends of the Province-House]]"
::I. "[[Howe's Masquerade]]"
::II. "[[Edward Randolph's Portrait]]"
::III. "[[Lady Eleanore's Mantle]]"
::IV. "[[Old Esther Dudley]]"
*"[[The Haunted Mind]]"
*"[[The Village Uncle]]"
*"[[The Ambitious Guest]]"
*"[[The Sister Years]]"
*"[[Snow-Flakes]]"
*"[[The Seven Vagabonds]]"
*"[[The White Old Maid]]"
*"[[Peter Goldthwaite's Treasure]]"
*"[[Chippings with a Chisel]]"
*"[[The Shaker Bridal]]"
*"[[Night Sketches]]"
*"[[Endicott and the Red Cross]]"
*"[[The Lily's Quest]]"
*"[[Foot-prints on the Sea-shore]]"
*"[[Edward Fane's Rosebud]]"
*"[[The Threefold Destiny]]"
{{colend}}

==Adaptations==
In 1963, [[United Artists]] released a horror trilogy film titled ''[[Twice-Told Tales (film)|Twice-Told Tales]]'', with content very loosely adapted from three Hawthorne stories. The three stories were: "Dr. Heidegger's Experiment," which actually was one of the "Twice-Told Tales"; the Hawthorne novel ''[[The House of the Seven Gables]]''; and another short story, "Rappaccini's Daughter." The film is regarded as a classic of sorts in the field of low-budget Hollywood horror, with [[Vincent Price]], [[Sebastian Cabot (actor)|Sebastian Cabot]], and [[Beverly Garland]] delivering good performances.

==Footnotes==
{{reflist|2}}

==References==
*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=145}}

==External links==
*{{wikisource-inline|single=true}}
*{{Commonscat-inline}}
*[https://archive.org/search.php?query=title%3Atwice%20told%20tales%20AND%20mediatype%3Atexts ''Twice-Told Tales''], available at [[Internet Archive]] (scanned books original editions illustrated)
* {{librivox book | title=Twice Told Tales | author=Nathaniel Hawthorne}}

{{Nathaniel Hawthorne}}

[[Category:1837 short story collections]]
[[Category:Short story collections by Nathaniel Hawthorne]]