{{Use dmy dates|date=May 2016}}
{{Use British English|date=May 2016}}
{{Infobox military installation
| name = RAF Annan
| ensign = [[File:Ensign of the Royal Air Force.svg|90px]]
| partof = <!-- for elements within a larger site -->
| location = [[Annan, Dumfries and Galloway|Annan]], [[Dumfriesshire]]
| nearest_town = <!-- used in military test site infobox -->
| country = Scotland
| image = Hurricane Mk.X 55 OTU in flight c1941.jpg
| alt =
| caption = Hurricane Mk. X (AG162 "EH-W") of No. 55 OTU
| image2 = <!--secondary image, major command emblems for airfields -->
| alt2 =
| caption2 =
| type = [[Royal Air Force station]]
| coordinates = {{coord|55|01|00|N|3|13|45|W|type:landmark_region:GB-DGY|display=inline}}
| pushpin_map = Scotland Dumfries and Galloway
| pushpin_mapsize =
| pushpin_map_alt =
| pushpin_map_caption = RAF Annan in Dumfries and Galloway
| pushpin_relief =
| pushpin_image =
| pushpin_label =
| pushpin_label_position = right
| pushpin_mark =
| pushpin_marksize =
| ownership = [[Air Ministry]]  
| operator = [[Royal Air Force]]
| controlledby = <!-- such as RAF Bomber Command or the Eighth Air Force -->
| site_other_label = <!-- for renaming "Other facilities" in infobox -->
| site_other = <!-- for other sorts of facilities - radar types etc -->  
| site_area = {{convert|154.10|ha}}
| built = {{Start date|1942}} 
| used = 1942–{{End date|1944}} 
| builder =
| materials =
| height = <!-- height of tallest part, not above sea level -->
| length = <!-- for border fences or other DMZs -->
| fate = Closed, now [[Chapelcross nuclear power station]]
| condition =
| battles = World War II
| events =
| current_commander = <!-- current commander -->
| past_commanders =
| occupants = <!-- squadrons only -->
| website =
| footnotes = <!-- catchall in case it's needed to preserve something in infobox that doesn't work in new code -->
}}
'''RAF Annan''' is a former [[Royal Air Force]] [[Royal Air Force station|station]] located about {{convert|2|mi|}} north-east of the town of [[Annan, Dumfries and Galloway|Annan]], [[Dumfries and Galloway]], Scotland, which was operational during the [[Second World War]].

==Station history==
Initially serving as a sub-site of [[No. 18 Maintenance Unit RAF|No. 18 Maintenance Unit]] in 1940–1941, RAF Annan was opened as an air station in April 1942 as the base for No. 55 [[Operational Training Unit]] (OTU), to train fighter pilots.<ref name="rafweb">{{cite web |url=http://www.rafweb.org/Stations/Stations-A.htm#Annan |title=RAF Stations (A) |first=M. B. |last=Barrass |work=Air of Authority – A History of RAF Organisation |accessdate=14 May 2016}}</ref> As part of [[No. 81 Group RAF|No. 81 Group]] 55 OTU pilots flew at low level over the [[Solway Firth]] training to fly "Rhubarb" missions, crossing the English Channel to attack [[Target of opportunity|targets of opportunity]] in France and the [[Low Countries]]. Initially they flew [[Hawker Hurricane]]s;<ref>{{cite web |url=http://www.aircrew-saltire.org/lib218.htm |title=Wartime Airfields in Dumfries and Galloway |first=Dennis |last=Sawden |work=Scottish Saltire Aircrew Association |year=2015 |accessdate=14 May 2016}}</ref> and later, [[Miles Master]] trainers and [[Hawker Typhoon]] fighter-bombers. In June 1943 55 OTU was transferred to [[No. 9 Group RAF|No. 9 Group]]; and on 26 January 1944 was redesignated No. 4 Tactical Exercise Unit (TEU), then No. 3 TEU on 28 March 1944.<ref>{{cite web |url=http://www.rafcommands.com/Ross/Fighter/55otuF.html |title=No. 55 OTU RAF |first=Ross |last=McNeill |work=RAF Commands |date=May 1999 |accessdate=14 May 2016}}</ref><ref>{{cite web |url=http://www.rafcommands.com/Ross/Fighter/4teuF.html |title=No. 4 TEU RAF |first=Ross |last=McNeill |work=RAF Commands |date=May 1999 |accessdate=14 May 2016}}</ref> No. 3 TEU moved to [[RAF Aston Down]] in July 1944,<ref>{{cite web |url= http://www.rafcommands.com/Ross/Fighter/3teuF.html |title=No. 3 TEU RAF |first=Ross |last=McNeill |work=RAF Commands |accessdate=14 May 2016}}</ref> and Annan then served as a sub-site of [[No. 14 Maintenance Unit RAF|No. 14 Maintenance Unit]] from August 1944 until August 1952.<ref name="rafweb"/>

==Units==
The following units were based at RAF Annan.<ref name="rafweb"/>
* Sub-site, No 18 Maintenance Unit (1940–1941)
* No. 55 Operational Training Unit (April 1942–January 1944)
**No. 4 Tactical Exercise Unit (January–March 1944)
**No. 3 Tactical Exercise Unit (March–May 1944)
* Detachment, No. 10 (Observer) Advanced Flying Unit
* [[No. 107 Squadron RAF]] (August 1942)
* Night Conversion Unit (March–April 1944)
* Sub-site, No 14 Maintenance Unit (August 1944–August 1952)

==Station facilities==
The air station had two concrete runways at right angles, running north-east/south-west ({{Convert|1476|m|abbr=on}}), and north-west/south-east ({{Convert|1454|m|abbr=on}}). The main technical area was in the north-eastern sector, and there were at least seven [[blister hangar]]s around the perimeter.<ref>{{cite web |url=https://canmore.org.uk/event/845445 |title=Annan Airfield: Archaeology Notes |work=[[Canmore (database)|Canmore]] |year=2016 |accessdate=14 May 2016}}</ref> There was a camp about {{convert|600|m}} north-east of the airfield near the village of Creca which contained various accommodation buildings, huts, and air raid shelters.<ref>{{cite web |url=http://www.scotlandsplaces.gov.uk/record/rcahms/280532/annan-airfield-creca-camp-domestic-site/rcahms?inline=true |title=Annan Airfield, Creca Camp |work=ScotlandsPlaces |year=2016 |accessdate=14 May 2016}}</ref> Another camp existed about {{convert|500|m}} south of the airfield.<ref>{{cite web |url=http://www.secretscotland.org.uk/index.php/Secrets/RAFAnnan |title=RAF Annan |work=Secret Scotland |year=2016 |accessdate=14 May 2016}}</ref>

==Fate==
On 1 July 1955 the site was taken over for the construction of the [[Chapelcross nuclear power station]].<ref>{{cite web |url=http://www.abct.org.uk/airfields/airfield-finder/annan/#tab2 |title=Annan |work=Airfields of Britain Conservation Trust UK |year=2016 |accessdate=14 May 2016}}</ref>

==See also==
* [[List of former Royal Air Force stations]]

==References==
{{reflist}}

{{Royal Air Force}}

{{DEFAULTSORT:Annan}}
[[Category:Royal Air Force stations in Scotland|A]]
[[Category:1942 establishments in Scotland]]
[[Category:1944 disestablishments in Scotland]]