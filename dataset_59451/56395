The '''''Connecticut Law Review''''' is a [[law review]] produced by [[University of Connecticut School of Law]] students. The ''Review'' publishes more than 1,000 pages of critical legal discussion each year and is managed entirely by a student board of editors, who solicit, edit, and publish articles and [[book review]]s written by scholars, [[judge]]s, and practicing [[Attorney at law|attorneys]]. Much of the content of the ''Review'' is written by students.

Subscribers to the ''Review'' include law offices and [[Law library|law libraries]] throughout the country and abroad, and the ''Review'' is often cited in briefs, [[court opinion]]s, and legal texts. Membership on the ''Connecticut Law Review'' provides many opportunities and benefits. Established in 1968, the ''Review'' is the oldest and largest student-run organization at the University of Connecticut School of Law.

==Notable alumni==
* [[Christopher F. Droney]], Circuit Judge of the United States Court of Appeals for the Second Circuit<ref>{{cite web|title=Christopher F. Droney - '79|url=http://connecticutlawreview.org/2012/04/09/christopher-f-droney-alumni/|website=Connecticut Law Review|accessdate=2014-10-28}}</ref>
* [[Dennis G. Eveleigh]], Justice of the Connecticut Supreme Court<ref>{{cite web|title=Biographies of Supreme Court Justices, Justice Dennis G. Eveligh|url=http://www.jud.ct.gov/external/supapp/justice_eveleigh.html|website=State of Connecticut Judicial Branch|accessdate=2014-10-28}}</ref>
* [[Joan G. Margolis]], United State Magistrate Judge for the District of Connecticut<ref>{{cite web|title=Joan G. Margolis - '79|url=http://ctd.uscourts.gov/content/joan-g-margolis}}</ref>
* [[Thomas P. Smith]], United States Magistrate Judge for the District of Connecticut<ref>{{cite web|title=Thomas P. Smith - '79|url=http://ctd.uscourts.gov/content/thomas-p-smith}}</ref>
* [[Shauhin Talesh]], professor, University of California, Irvine School of Law<ref>{{cite web|title=Shauhin Talesh - '79|url=http://www.law.uci.edu/faculty/full-time/talesh/}}</ref>

==References==
{{reflist}}

==External links==
*[http://connecticutlawreview.org/ Official website]

{{University of Connecticut}}

[[Category:American law journals]]
[[Category:General law journals]]
[[Category:University of Connecticut]]


{{law-journal-stub}}