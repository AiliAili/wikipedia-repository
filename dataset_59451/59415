{{Infobox journal
| title = Philosophy and Theology
| formernames = Philosophy, Theology
| cover = [[File:philtheocover2.gif]]
| editor = James B. South
| discipline = [[Philosophy]], [[religious studies]]
| abbreviation = Philos. Theol.
| publisher = [[Philosophy Documentation Center]]
| country = United States
| frequency = Biannual
| history = 1986–present
| website = https://www.pdcnet.org/philtheol
| link2 = https://www.pdcnet.org/philtheol/toc
| link2-name = Online access
| OCLC  = 14228352
| JSTOR =
| LCCN = 
| CODEN =
| ISSN = 0890-2461
| eISSN = 2153-828X
}}
'''''Philosophy and Theology''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes articles and reviews exploring connections between [[philosophy]] and [[theology]]. It was established in 1986 by Andrew Tallon at [[Marquette University]] and is the journal of the [[Karl Rahner Society]]. One issue of each volume is dedicated to [[Karl Rahner|Rahner]]'s thought. Since 1997 the journal has been published on behalf of Marquette University by the [[Philosophy Documentation Center]]. All issues are available in electronic format.

==Indexing==
''Philosophy and Theology'' is abstracted and indexed [[Academic Search]], Current Abstracts, [[Expanded Academic ASAP]], [[Index Philosophicus]], [[InfoTrac OneFile]], [[International Bibliography of Book Reviews of Scholarly Literature]], [[International Bibliography of Periodical Literature]], [[MLA International Bibliography]], Periodicals Index Online, [[Philosopher's Index]], [[Philosophy Research Index]], [[PhilPapers]], Religion Index, [[Religious and Theological Abstracts]], and [[TOC Premier]].

== See also ==
* [[List of philosophy journals]]
* [[List of theology journals]]

== External links ==
* {{Official|https://www.pdcnet.org/philtheol}}
* [http://www.krs.stjohnsem.edu/ Karl Rahner Society]

[[Category:English-language journals]]
[[Category:Philosophy journals]]
[[Category:Publications established in 1986]]
[[Category:Biannual journals]]
[[Category:Catholic studies journals]]
[[Category:Philosophy Documentation Center academic journals]]
[[Category:Contemporary philosophical literature]]