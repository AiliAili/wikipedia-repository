{{cleanup|reason=does not adhere to [[WP:MOSFLAGS]]|date=March 2017}}
{{Infobox Academic Conference
 | history = 1975–
 | discipline = [[software engineering]]
 | abbreviation = ICSE
 | publisher = ACM and IEEE Computer Society
 | country= International
 | frequency = annual
}}

The '''International Conference on Software Engineering''' ('''ICSE'''), is one of the largest annual [[software engineering]] conferences. It has an 'A*' rating in the [http://www.core.edu.au/index.php/conference-rankings|Conference Rankings] of the [http://www.core.edu.au/index.php/ Computing Research and Education Association of Australasia (CORE)] and an 'A1' rating from the Brazilian ministry of education.<ref>[http://www.conferenceranks.com/index.html?searchall=ICSE Conference Ranks]</ref> Furthermore, it is the software engineering conference with the highest Microsoft Academic field rating.<ref>[http://www.conferenceranks.com/visualization/msar2014.html?field=Software+Engineering MSAR field ratings]</ref> The first ICSE conference was in 1975 in [[Washington DC]].<ref name="ICSEhistory">[http://www.icse-conferences.org/history.html History of ICSE Conferences]</ref>

==List of Conferences==
Past and future ICSE conferences include:<ref name=ICSEhistory/>
{| class="wikitable"
|-
! Year
! Conference
! City
! Country
! General Chair(s)
! Notes
|-
|2018
|[http://www.icse2018.org/ ICSE 40]
|[[Gothenburg]]
|{{Flagicon|Sweden}} Sweden
|[http://www.ivica-crnkovic.net/ Ivica Crnkovic]
|
|-
|2017
|ICSE 39
|[[Buenos Aires]]
|{{Flagicon|Argentina}} Argentina
|[http://lafhis.dc.uba.ar/en/~suchitel Sebastián Uchitel]
|
|-
|2016
|[http://2016.icse-conferences.org ICSE 38]
|[[Austin]]
|{{Flagicon|USA}} USA
|[http://www.cse.msu.edu/~ldillon/ Laura K. Dillon]
|
|-
|2015
|[http://2015.icse-conferences.org/ ICSE 37]
|[[Florence]]
|{{Flagicon|Italy}} Italy
|[http://bertolino.isti.cnr.it/ Antonia Bertolino]
|
|-
|2014
|[http://icse2014.acm.org/ ICSE 36]
|[[Hyderabad]]
|{{Flagicon|India}} India
|[[Pankaj Jalote]]
|
|-
|2013
|[http://2013.icse-conferences.org/ ICSE 35]
|[[San Francisco]]
|{{Flagicon|USA}} USA
|[http://www.cs.washington.edu/people/faculty/notkin/ David Notkin]
|
|-
|2012
|[http://www.ifi.uzh.ch/arvo/req/events/ICSE2012/ ICSE 34]
|[[Zurich]]
|{{Flagicon|Switzerland}} Switzerland
|[http://www.ifi.uzh.ch/rerg/people/glinz.html Martin Glinz]
|
|-
|2011
|[http://2011.icse-conferences.org/ ICSE 33]
|[[Honolulu]]
|{{Flagicon|USA}} USA
|[http://www.ics.uci.edu/~taylor/ Richard Taylor]
|
|-
|2010
|[http://www.sbs.co.za/ICSE2010/ ICSE 32]
|[[Cape Town]]
|{{Flagicon|South Africa}} South Africa
|Judith Bishop & Jeff Kramer
|-
|2009
|[http://www.cs.uoregon.edu/events/icse2009/home/ ICSE 31]
|[[Vancouver]]
|{{Flagicon|Canada}} Canada
|Stephen Fickas
|-
|2008
|[http://www.icse-conferences.org/2008/index.html ICSE 30]
|[[Leipzig]] 
|{{Flagicon|Germany}} Germany
|Wilhelm Schäfer
|-
|2007
|[http://www.icse-conferences.org/2007/index.html ICSE 29]
|[[Minneapolis]] 
|{{Flagicon|USA}} USA
|John Knight
|-
|2006
|[http://isr.uci.edu/icse-06/ ICSE 28]
|[[Shanghai]] 
|{{Flagicon|China}} China
|Leon Osterweil
|-
|2005
|[http://www.icse-conferences.org/2005/Home/index.shtml ICSE 27]
|[[St. Louis]] 
|{{Flagicon|USA}} USA
|Gruia-Catalin Roman
|-
|2004
|[http://www.icse-conferences.org/2004/index.html ICSE 26]
|[[Edinburgh]] 
|{{Flagicon|UK}} UK
|Anthony Finkelstein
|-
|2003
|[http://www.icse-conferences.org/2003/index.html ICSE 25]
|[[Portland, Oregon|Portland]] 
|{{Flagicon|USA}} USA
|Lori Clarke
|-
|2002
|[http://www.icse-conferences.org/2002/index.html ICSE 24]
|[[Orlando]] 
|{{Flagicon|USA}} USA
|Will Tracz
|-
|2001
|[http://www.icse-conferences.org/2001/index.html ICSE 23]
|[[Toronto]] 
|{{Flagicon|Canada}} Canada
|[[Hausi A. Muller|Hausi A. Müller]]
|-
|2000
|[http://www3.ul.ie/~icse2000/ ICSE 22]
|[[Limerick]] 
|{{Flagicon|Ireland}} Ireland
|Carlo Ghezzi
|-
|1999
|ICSE 21
|[[Los Angeles]] 
|{{Flagicon|USA}} USA
|Barry Boehm
|
|-
|1998
|ICSE 20
|[[Kyoto]] 
|{{Flagicon|Japan}} Japan
|Koji Torii
|-
|1997
|[http://www.icse-conferences.org/1997/ ICSE 19]
|[[Boston]] 
|{{Flagicon|USA}} USA
|W. Richards Adrion
|-
|1996
|ICSE 18
|[[Berlin]] 
|{{Flagicon|Germany}} Germany
|{{Interlanguage link multi|Dieter Rombach|de}}
|-
|1995
|[http://www.cs.washington.edu/research/se/icse17/ ICSE 17]
|[[Seattle]] 
|{{Flagicon|USA}} USA
|Dewayne Perry
|-
|1994
|ICSE 16
|[[Sorrento]] 
|{{Flagicon|Italy}} Italy
|Bruno Fadini
|-
|1993
|ICSE 15
|[[Baltimore]] 
|{{Flagicon|USA}} USA
|Victor R. Basili
|-
|1992
|ICSE 14
|[[Melbourne]]
|{{Flagicon|Australia}} Australia
|Tony Montgomery
|-
|1991
|ICSE 13
|[[Austin]]
|{{Flagicon|USA}} USA
|Les Belady
|-
|1990
|ICSE 12
|[[Nice]]
|{{Flagicon|France}} France
|François-Regis Valette
|-
|1989
|ICSE 11
|[[Pittsburgh]] 
|{{Flagicon|USA}} USA
|Larry Druffel
|-
|1988
|ICSE 10
|[[Raffles City]] 
|{{Flagicon|Singapore}} Singapore
|Tan Chin Nam
|-
|1987
|ICSE 9
|[[Monterey]] 
|{{Flagicon|USA}} USA
|William E. Riddle
|-
|1985
|ICSE 8
|[[London]] 
|{{Flagicon|UK}} UK
|Manny M. Lehman
|-
|1984
|ICSE 7
|[[Orlando]] 
|{{Flagicon|USA}} USA
|T. Straeter
|-
|1982
|ICSE 6
|[[Tokyo]] 
|{{Flagicon|Japan}} Japan
|Yutaka Ohno, Koji Kobayashi, Raymond T. Yeh
|-
|1981
|ICSE 5
|[[San Diego]] 
|{{Flagicon|USA}} USA
|Seymour Jeffrey
|-
|1979
|ICSE 4
|[[Munich]] 
|{{Flagicon|Germany}} Germany
|Friedrich (Fritz) L. Bauer
|Sponsors: [[ACM SIGSOFT]], [[European Research Office]] (ERO), [[Gesellschaft für Informatik]], and [[IEEE Computer Society]].
|-
|1978
|ICSE 3
|[[Atlanta]]
|{{Flagicon|USA}} USA
|Maurice V. Wilkes
|Sponsors: [[Association for Computing Machinery]] (ACM), [[IEEE Computer Society]], and [[National Bureau of Standards]].
|-
|1976
|ICSE 2
|[[San Francisco]]
|{{Flagicon|USA}} USA
|Raymond T. Yeh
|The conference was renamed to '''2nd International Conference on Software Engineering''' and was sponsored by [[Association for Computing Machinery]] (ACM), [[IEEE Computer Society]], and [[National Bureau of Standards]].
|-
|1975
|NCSE 1
|[[Washington, D.C.|Washington]]
|{{Flagicon|USA}} USA
|[[Harlan Mills]], Dennis Fife
|The first conference was called '''1st National Conference on Software Engineering''' and was sponsored by [[National Bureau of Standards]] and [[IEEE Computer Society]].
|}

==References==
{{Reflist|30em}}

==External links==
* [http://www.icse-conferences.org Pointers to ICSE conference websites]
* [http://www.icse-conferences.org/sc/index.html ICSE Steering Committee Information]

{{DEFAULTSORT:International Conference On Software Engineering}}
[[Category:Software engineering conferences]]