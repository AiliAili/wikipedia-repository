{{Infobox Journal
| cover 	= [[File:Nature Materials Nov 2008.jpg|250px]]
| discipline 	= [[material science|materials science]]
| abbreviation = Nat. Mater.
| publisher 	= [[Nature Publishing Group]]
| country 	= United Kingdom
| frequency 	= Monthly
| history 	= 2002–present
| openaccess = 
| license 	= 
| impact 	= 38.891
| impact-year = 2015
| website 	= http://www.nature.com/nmat/index.html
| link1 	= 
| link1-name 	= 
| link2 	= 
| link2-name 	= 
| RSS 		= http://feeds.nature.com/nmat/rss/current 
| atom	 	= 
| JSTOR 	= 
| OCLC 		= 51211555 
| LCCN 		= 2003260101 
| CODEN 	= NMAACR
| ISSN 		= 1476-1122
| eISSN         = 1476-4660
| boxwidth 	= 
}}
'''''Nature Materials''''', is a [[peer-review]]ed [[scientific journal]] published by [[Nature Publishing Group]]. It was launched in September 2002. Vincent Dusastre is the launching and current chief editor.<ref name=aim-scope/><ref name=editor>{{Cite web
  | title =Editorial Team
  | work =Biographical paragraphs about the editorial staff 
  | publisher =Nature Publishing group 
  | date =July 2010 
  | url =http://www.nature.com/nmat/authors/about_eds/index.html 
  | accessdate =2010-07-29}}</ref><ref name=full-time>{{Cite web
  | title =Editors and contact information
  | work ="...all editorial decisions are made by a team of full-time professional editors, who are PhD-level scientists."
  | publisher =Nature Publishing group 
  | date =July 2010 
  | url =http://www.nature.com/nmat/authors/index.html#editors-contact 
  | accessdate =2010-07-29}}</ref><ref name=LCC>{{Cite web
  | title =Nature materials. 
  | work =Bibliographic information for this journal. 
  | publisher =Library of Congress 
  | date =January 2009 
  | url =http://lccn.loc.gov/2003260101 
  | accessdate =2010-07-29}}</ref> 

==Aims and scope==
''Nature Materials'' is focused on all topics within the combined disciplines of ''[[materials science]]'' and ''[[engineering]]''. Topics published in the journal are presented from the view of the impact that materials research has on other scientific disciplines such as (for example) [[physics]], [[chemistry]], and [[biology]]. Coverage in this journal encompasses [[fundamental research]] and applications from synthesis to processing, and from structure to composition. Coverage also includes [[basic research]] and applications of properties and performance of materials. Materials are specifically described as "substances in the [[Condensed matter physics|condensed states]] (liquid, solid, colloidal)", and which are "designed or manipulated for [[technology|technological]] ends."<ref name=aim-scope/> 

Furthermore, ''Nature Materials'' functions as a forum for the [[materials scientist]] community.  [[Interdisciplinary]] research results are published, obtained from across all areas of materials research, and between scientists involved in the different disciplines. The readership for this journal are scientists, in both [[academia]] and industry involved in either developing materials or working with materials-related concepts. Finally, ''Nature Materials'' perceives materials research as significantly influential on the development of society.<ref name=aim-scope>{{Cite web
  | title =Aims and scope of the journal 
  | publisher =Nature Publishing group 
  | date =July 2010 
  | url =http://www.nature.com/nmat/authors/index.html#aims-scope 
  | accessdate =2010-07-29}}</ref>

==Coverage==
Research areas covered in the journal include:<ref name=aim-scope/> 

* Engineering and structural materials (metals, [[alloys]], ceramics, [[Composite material|composites]])
* Organic and soft materials (glasses, colloids, [[liquid crystals]], [[polymers]])
* Bio-inspired, biomedical and biomolecular materials
* Optical, photonic and optoelectronic materials
* Magnetic materials
* Materials for electronics
* [[Superconducting]] materials
* Catalytic and separation materials
* Materials for [[energy]]
* [[Nanoscale]] materials and processes
* Computation, modelling and materials theory
* Surfaces and [[thin films]]
* Design, synthesis, processing and characterization techniques

In addition to primary research, ''Nature Materials'' also publishes review articles, news and views, research highlights about important papers published in other journals, commentaries, correspondence, interviews and analysis of the broad field of materials science.

==Abstracting and indexing==
''Nature Materials'' is indexed in the following databases:<ref name=masterList>{{Cite web
  | title =Master Journal search 
  | work =Coverage  
  | publisher =[[Thomson Reuters]]
  | date =July 2010 
  | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1476-1122
  | accessdate =2010-07-29}}</ref><ref name=cassi>{{Cite web
  | title =''Nature Materials'' 
  | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication) 
  | publisher =[[American Chemical Society]]
  | date =July 2010 
  | url =http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfB1hjAUbAURScQxM7r04NXzLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfrQZxl5GI9PD3gLWXjjhuVA
  | accessdate =2010-07-29}}</ref>
*[[Chemical Abstracts Service]] – [[CASSI]]
*[[Science Citation Index]] 
*[[Science Citation Index Expanded]] 
*[[Current Contents]] – Physical, Chemical & Earth Sciences 
*[[BIOSIS Previews]]

==See also==
* ''[[ACS Applied Materials & Interfaces]]''
* ''[[Advanced Materials]]''
* ''[[Advanced Composite Materials]]
* ''[[Advanced Functional Materials]]''
* ''[[JOM]]''
* ''[[Journal of Electronic Materials]]''
* ''[[Materials (journal)]]''
* ''[[Materials and Structures]]''
* ''[[Materials Today]]''
* ''[[Metallurgical and Materials Transactions]]''
* ''[[Metamaterials (journal)]]''
* ''[[Science and Technology of Advanced Materials]]''
* ''[[Computational Materials Science (journal)|Computational Materials Science]]
* ''[[Nature Photonics]]''

==References==
{{reflist}}

==External links==
* [http://www.nature.com/nmat/index.html Nature Materials]
* [http://www.nature.com/nmat/authors/about_eds//index.html Nature Materials editors]

{{Georg von Holtzbrinck Publishing Group}}

[[Category:Nature Publishing Group academic journals]]
[[Category:Materials science journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 2002]]