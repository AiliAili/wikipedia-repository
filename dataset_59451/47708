{{Infobox ethnic group

|group      = Emberá
|image      = Mujeres de la etnia Emberá.jpg
|population = ~83,000
|region1    = {{COL}}
|pop1       = ~50,000
|ref1       =
|region2    = {{PAN}}
|pop2       = ~33,000
|ref2       =<ref name=censo>{{cite web|title=Instituto Nacionál de Estadistica y Censo - Panamá |url=http://www.contraloria.gob.pa/inec/Publicaciones/subcategoria.aspx?ID_CATEGORIA=13&ID_SUBCATEGORIA=59&ID_IDIOMA=1 |accessdate=20 September 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130918173704/http://www.contraloria.gob.pa/inec/Publicaciones/subcategoria.aspx?ID_CATEGORIA=13&ID_SUBCATEGORIA=59&ID_IDIOMA=1 |archivedate=18 September 2013 |df= }}</ref>
|languages  = [[Northern Embera language|Northern Emberá]], [[Emberá-Catío language|Emberá-Catío]], [[Emberá-Chamí language|Emberá-Chamí]], [[Emberá-Baudó language|Emberá-Baudó]], [[Eperara language|Eperara]], [[Spanish language|Spanish]]
|religions  = ''jaí'', [[Evangelicalism|Evangelicism]]
|related    = [[Wounaan]]
|footnotes  =
}}
The '''Emberá people''' {{audio|emberá_people.ogg|listen}}, also known in the historical literature as the ''Chocó'' or ''Katío'' Indians are an indigenous people of [[Panama]] and [[Colombia]]. In the [[Emberá languages|Emberá language]], the word ''ẽberá'' can be used to mean person, man, or indigenous person, depending on the context in which it is used. There are approximately 33,000 people living in Panama and 50,000 in Colombia who identify as Emberá.

== Language ==
{{main article|Embera language}}

The Emberá language, or ''ẽberá bedéa'', is not a single language but a group of mutually-intelligible languages spoken throughout Panamá and Colombia. Along with [[Wounaan language|Wounmeu]], they are the only extant members of the [[Choco languages|Chocó language family]] and not known to be related to any other language family of [[Central America|Central]] or [[South America|South]] America, although in the past relationships have been proposed with the [[Carib languages|Carib]], [[Arawakan languages|Arawak]], and [[Chibchan languages|Chibchan]] language families.<ref name="Choco Languages" /><ref name=mortensen>{{cite book|last=Mortensen|first=Charles A.|title=A Reference Grammar of the Northern Embera Languages|year=1999|publisher=SIL International and The University of Texas at Arlington}}</ref>

An established Emberá alphabet has been officially recognized by the government of Panama, consisting of:
* 6 oral vowels (a, e, i, o, u, ʌ)
* 6 nasal vowels (ã, ẽ, ĩ, õ, ũ, ʌ̃)
* 21 consonants (b, b̶, ch, d, d̶, dy, g, j, k, l, m, n, p, r, rr, s, t, v, w, y, z).<ref>{{cite web|last=Asemblea Nacional de Panamá |title=Por el cual se oficializan los idiomas alfabetos de los pueblos indígenas de Panamá, se establecen bases lingüisticas de la educación bilingüe intercultural y se adoptan otras disposiciones. |url=http://www.asamblea.gob.pa/apps/seg_legis/PDF_SEG/PDF_SEG_2000/PDF_SEG_2009/2009_P_055.pdf |accessdate=21 September 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20131213214407/http://www.asamblea.gob.pa/apps/seg_legis/PDF_SEG/PDF_SEG_2000/PDF_SEG_2009/2009_P_055.pdf |archivedate=13 December 2013 |df= }}</ref>

To date, there have been very few books published in the Emberá language. These are mostly educational materials produced by [[Education in Panama|Panama's education ministry (MEDUCA)]] or by Christian missionaries. The most significant of these to date is a Bible translation containing the [[New Testament]] and parts of the [[Old Testament]] titled Ãcõrẽ Be<s>d</s>ea, "the word of God."<ref name=akore>{{cite book|title=Ãcõrẽ Bedea|year=2011|publisher=Wycliffe}}</ref><ref name="akore web">
{{cite web
 |title=The Bible in Emberá, Northern 
 |url=http://www.scriptureearth
 |accessdate=17 November 2013 
}}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> The following is an excerpt from the book of Matthew:

:<small>18 Jesucrito tod̶ara nãwã b̶asia: dji papa Mariara Jose ume dji edad̶i carea bed̶ea bia panasid̶aa. Baribʌrʌ dji edad̶i naẽna Mariara Ãcõrẽ Jaured̶eba b̶iogoa b̶esia.  19 Dji quima Josera ẽberã jipa b̶asia. Maʌ̃ bẽrã Mariara jũmarã quĩrãpita biẽ́ jara amaaba chupea igara quĩrĩã b̶asia.  20 Mãwã crĩcha b̶ʌd̶e bajãnebema nezocada cãĩmocarad̶eba Josemaa zeped̶a nãwã jarasia: –Jose, David̶eba zed̶a, Maria edaira wayarãdua, idjira Ãcõrẽ Jaured̶ebabʌrʌ b̶iogoa b̶ʌ bẽrã.  21 Mariaba warrada toya. Maʌ̃ warraba idji purura ãdji cadjiruad̶ebemada ẽdrʌ edaya. Maʌ̃ bẽrã idjira trʌ̃ b̶ʌdua Jesu. -Matthew 1:18-21<ref name=akore /></small>
Words from the Emberá language are marked in this article in parentheses and italicized. For example, woman ''(wẽra)'' or shaman ''(jaibaná)''.

== Geography ==
The Emberá people mostly live in the [[Chocó department]] of western [[Colombia]] and in eastern Panama. For the Panamanian Emberá, the [[Chocó department|Chocó]] and its river systems remain their ancestral homelands, and the different dialects of the Emberá language still mostly correspond to different parts of that area and its river systems.<ref name="Choco Languages">{{cite book|last=Aguirre Licht|first=D|title=Concise Encyclopedia of Languages of the World|year=2008|publisher=Elsevier Science|isbn=978-0080877747|pages=224–238|editor=Keith Brown|chapter=Choco Languages}}</ref>

== History ==

=== History of the Emberá in Panama ===
In the late 1700s, the Emberá began migrating from the Choco region of modern-day Colombia to what is currently the [[Darién Province|Darién]] province of Panama, which had been home to the Tule or [[Guna people]] at the time of European contact.<ref name="herlihy diss">{{cite book|last=Herlihy|first=Peter H.|title=A Cultural Geography of the Embera and Wounan (Choco) Indians of Darien, Panama: With Emphasis on Recent Village Formation and Economic Diversification.|year=1986|publisher=Louisiana State University and Agricultural and Mechanical College}}</ref>  This migration was prompted by the Spanish, and took the form of a conflict between the Emberá and the Guna, moving the latter into the lands along the Caribbean coast which now form the [[Guna Yala|Comarca Guna Yala]] and [[San Blas Islands]]. The Darién has subsequently become recognized as the homelands of the Emberá in Panama, though they had also settled as far west as [[Lake Gatún]] and the riverine areas of what would become the [[Panama Canal Zone|Canal Zone]].

[[File:New Caledonia in Darien2.jpg|thumb|left|A map of the Darién isthmus, 1699.]]During the 19th century, during which it was part of the Gran Republica de Colombia, the Darién was inhabited almost exclusively by the indigenous Emberá and Guna peoples along with the descendants of [[Cimarron people (Panama)|escaped African slaves]], known as ''[[Afro-Panamanians|darienitas]]'' or ''libres''. Typically, ''darienitas'' settled in port towns at the mouths of rivers while the Emberá would live along the rivers downstream. This historical trend is still reflected in the current regional demographics; many port towns have retained their ''darienita'' identity, such as [[La Palma, Darién|La Palma]], [[Yaviza]] and [[Garachiné|Garachine]], while many contemporary Emberá towns are found downstream along the rivers.<ref name = herlihy_participatory>{{cite journal|last=Herlihy|first=Peter H.|title=Participatory Research Mapping of Indigenous Lands in Darién, Panama|journal=Human Organization|year=2003|volume=62|issue=4|pages=315–331}}</ref>
[[Separation of Panama from Colombia|Panama seceded from the Republic of Colombia]], achieving its independence in 1903. However, Eastern Panama still remained isolated and undeveloped during this time. While trade with the ''darienitas'' increased the access that the Emberá had to resources such as off-board motors, kerosene lamps, hunting rifles, and other modern commodities which would change their daily lives, major changes started taking place in the 1950s and 1960s. This time period observed the influence of missionaries and the involvement of the revolutionary government of [[Omar Torrijos]], who actively sought to integrate the indigenous population of Darién into Panamanian society by encouraging them to settle into communities for access to government-sponsored services such as schools and health centers. This initiated the transition of the Emberá people, who until that time had lived dispersed in small family units throughout the rainforest, into settled communities.

The [[Darién Province|Darién province]] changed dramatically with the completion of the [[Pan-American Highway|Pan-American highway]] into the province as far as Yaviza. Before the creation of the highway, the dominant if not exclusive mode of trade and transportation between Panama City and the Darién was by boat. The construction of this highway through a previously untraversable terrain had a number of significant effects. Latino farmers from the interior of the country, often called ''interioranos,'' began to migrate into the Darién in large numbers, effecting demographic shifts and an unprecedented scale of logging and deforestation for the purpose of establishing cattle ranches.<ref name = "Colin">{{cite book|last=Colin|first=France-Lise|title="Nosotros no solamente podemos vivir de cultura": Identity, Nature, and Power in the Comarca Emberá of Eastern Panama (PhD Dissertation)|year=2010|publisher=Department of Geography and Environmental Studies Carleton University|location=Ottawa, Canada}}</ref> It also shifted the economic and political centers of the province from the coastal ''darienita'' cities to the new towns being established along the highway by these Latino settlers.

== Social organization ==

=== Transition from dispersed settlement to villages, towns, and urban neighborhoods ===
Historically, the Emberá lived in a dispersed settlement pattern along the river systems of Panamá and Colombia. Since the 1960s, the majority of Emberá have transitioned to settled communities and urban areas. Herlihy describes the pre-1950's settlement pattern:
:<small>"Traditional Chocó settlement consists of dispersed household units in a distinctive riverine pattern&nbsp;... The extended family serv[ed] as the settlement unit. No villages or large agglomerations of dwellings exist[ed]. Thatched-roof, pile-dwellings are scattered along the river margin, usually constructed on levee tops and high alluvial terraces. Population densities vary from one river to another, but houses are generally situated at least several hundred meters from one another, with intervening forests and river bends blocking the view of a neighbor's house. Settlement density is usually greatest where occupation periods have been longest."<ref name = herlihy>{{cite journal|last=Herlihy|first=Peter|title=Settlement and subsistence change among the Choco Indians of the Darien Province, eastern Panama: An overview|journal=Proceedings of the Conference of Latin Americanist Geographers|year=1985|volume=1|url=http://sites.maxwell.syr.edu/clag/yearbook1985/herlihy.pdf|accessdate=20 September 2013}}</ref></small>
The Emberá began forming the first small villages in the 1950s, in what has been described as a "slow, almost evolutionary process."<ref name="herlihy settlement">{{cite journal|last=Herlihy|first=Peter|title=Settlement and Subsistence Change Among the Chocó Indians of the Darién Province, Eastern Panama: An Overview|journal=Yearbook, Conference of Latin Americanist Geographers|year=1985|volume=11|pages=11–16}}</ref> Economic considerations, Western influence, and the presence of religious missionaries were influential factors in the first settlements of Emberá communities. Accounts also exist of a foreigner known by his nickname "Perú," a mysterious and legendary figure who tried to convince the Emberá to settle into colonies.

:<small>He instructed them that... through the formation of villages they could solicit government officials to provide teachers, schools, and medical supplies. He told them that through more effective occupation of their traditional lands, they could obtain a [semi-autonomous territory] like the one of the Cuna Indians of Panama's San Blas Islands, guaranteeing them legal rights to land and resources. As a result, among the Darién Chocó Perú is a larger-than-life romantic folk figure.</small> <ref name = "herlihy settlement" />

By the late 1960s, the government of Omar Torrijos was promoting the settlement of the Darién Emberá into communities; the first formal establishment of an Emberá town was in 1963 along the Rio Balsas river. In 1985, it was estimated that 25 percent of Emberá people in Panamá still lived according to the traditional dispersed settlement pattern.<ref name = herlihy />  However, life in settled communities is now considered the norm, or "typical" of most Emberá.<ref name  = "authenticity">{{cite journal|last=Theodossopoulos|first=Dimitrios|title=Emberá Indigenous Tourism and the Trap of Authenticity: Beyond Inauthenticity and Invention|journal=Anthropological Quarterly|year=2013|volume=86|issue=2|pages=397–426|doi=10.1353/anq.2013.0023}}</ref>

In addition to these settled communities, many Emberá now also live in urban areas. According to the 2010 Panamanian National Census, over one third of Panamanian Emberá people live in the central province of Panamá, and over 25% of the total Panamanian Emberá population reside in urban districts of Panama City.<ref name = censo />

=== Riverine lifestyle and housing ===
The Emberá are a [[wikt:riverine|riverine]] people, historically building their houses along the banks of rivers. Although now most all Emberá people live in villages, towns, or urban centers, many established Emberá communities are still found along riverbanks. The designated autonomous region, the [[Comarca Emberá-Wounaan]], is split up into two territories surrounding two of the Darién's major river systems, the [[Sambú River|Sambú]] and [[Chucunaque River|Chucunaque]].<ref name = herlihy_participatory /> The word for river in both the Emberá and Wounaan languages is ''dó'', noticeable in the names of many of the rivers and towns in the Chocó department of Colombia, such as the Baudó river, as well as the capital of the department itself, [[Quibdó]].

Fish ''(bedá)'' are an important staple of the Emberá diet along with plantains ''(patá)'', and rivers play a central role in daily life for fishing, bathing, transport, and many domestic chores. Boats have also played important roles in Emberá tradition and cosmology. The craft of constructing dugout canoes ''(hampá)'' was historically a very significant skill for Emberá men, at times serving as a rite of passage or prerequisite for marriage according to oral history.{{Citation needed|date=December 2013}} Ethnographic records detail the ways in which boats take on an anthropomorphic character in the Emberá language and philosophy, and traditionally people were even buried in canoes.<ref name="Transformations of eternity">{{cite book|last=Isacsson|first=Sven-Erik|title=Transformations of Eternity: On Man and Cosmos in Emberá Thought|year=1993|publisher=University of Göteborg|location=Göteborg}}</ref>  Anthropologists have written about how central rivers are to the worldview of both the Emberá and the Wounaan, a closely related group of people who, while having a distinct history and belief system from the Emberá, share much with them linguistically, historically, and culturally.<ref name="gringo boat">{{cite book|last=Kane|first=Stephanie|title=The phantom gringo boat: shamanic discourse and development in Panama|year=1994|publisher=Smithsonian|location=Washington, D.C.}}</ref><ref name="runk rhizomic">{{cite journal|last=Velasquez Runk|first=Julie|title=Social and River Networks for the Trees:  Wounaan's Riverine Rhizomic Cosmos  and Arboreal Conservation|journal=American Anthropologist|year=2009|volume=111|issue=4|pages=456–457|doi=10.1111/j.1548-1433.2009.01155.x}}</ref>
In addition to changes in settlement patterns, the form of typical Emberá housing is actively going through a period of change. A traditional Emberá house can be described as an open-air dwelling raised 6–12 feet off the ground on stilts with thatched roofing made from palm leaves (often, but not exclusively, ''[[Sabal mauritiiformis]]'') and flooring made from the bark of the [[Socratea exorrhiza|''jira'']] palm ''(épa)''. These houses were typically round in shape and large enough to hold members of an extended family group. Logs with notches cut in them (''domé'') were used as ladders to enter/exit the house, and could be turned with the notches facing inwards to signal that people were busy, not home, or to keep animals from climbing into the house. The space under the house would be used as a dry space to carry out domestic chores or keep animals.

Contemporary Emberá housing style often employs many of the traditional materials and styles. However, they may often be smaller due to a comparatively smaller number of family members per house. Wooden boards often replace the ''jira'' bark as flooring, and durable aluminum roofing in place of palm leaves. Due to living in settled communities with other unrelated people, walls have become more common for added privacy, whereas walls were historically very uncommon. Propane stoves often replace or complement the traditional cooking fire. Some contemporary Emberá houses have both a larger structure of wooden floors and walls with metal roofing on a cement foundation, with an attached, more traditional thatched-roof structure for use as a kitchen. Many now live in cinder-block houses in the typical Panamanian style, if they have the access to the resources and infrastructure to rent or build one.

== Political organization ==
Historically, the Emberá people were described as having a fundamentally egalitarian social and political organization:

:<small>Ethnographic accounts and oral history indicate that since colonial times, [Emberá] social structure has been egalitarian, with no formal tribal leaders, chiefs, councils, or a structure of elders. Certain religious beliefs and ceremonial activities center on the shaman who, with an intimate knowledge of the medicinal, toxicologic and hallucinogenic properties of the surrounding plant and animal world, cures through exorcising malignant spirits. Yet, in terms of political, economic, or interpersonal relationships, no individual holds special leadership status.</small><ref name ="herlihy cultural survival">{{cite journal|last=Herlihy|first=Peter|title=Chocó Indian Relocation in Darién, Panama|journal=Cultural Survival Quarterly|year=1985|issue=9.2}}</ref>

===Political Organization of the Panamanian Emberá===

Part of [[Omar Torrijos]]'s efforts to organize the indigenous people of the Darién was through the establishment of the first National Indian Congress in 1968. The Emberá were encouraged to self-organize and form political leadership in the same way the [[Guna people]] had done, and a Guna chief was even appointed to aid them in the process. Throughout the 1970s, more and more Emberá families continued settling into communities and towns. By 1980, discussions were taking place about the establishment of a ''[[comarca indigena]]'', or autonomous territory, for the Emberá within the Darién province.

On November 8, 1983, the [[National Assembly of Panama]] ratified the ''ley 22'', establishing the [[Comarca Emberá-Wounaan]], a territory of 4383.5&nbsp;km² encompassing two [[non-contiguous]] districts, Sambú and Cemaco, whose capital is the town of [[Unión Chocó]]. The law established the territory under a [[Customary land|collective land title]] of the Emberá and Wounaan peoples. The leadership structure, officially known as the ''Congreso General Emberá Wounaan de la Comarca,'' has a ''casique general'' as its head. In addition, each district has a regional ''casique,'' and individuals communities also elect local ''casiques'' as their representatives at the annual General Congress of the Comarcá. The establishment of the Comarca conferred on the indigenous population legal [[Indigenous land rights|protection of their land]] from the encroachment of Latino cattle-ranchers, as well as a certain degree of self-governance.{{Citation needed|date=December 2013}}

The Comarca encompasses around 27% of the total land mass of the Darién province (not including the autonomous territories of the Guna people). However, since many Emberá communities both inside and outside of the Darién exist outside of that territory, a political organization was created in December 2008 under the name ''Tierras Colectivas Emberá y Wounaan'', or "Collective lands of the Emberá and Wounaan." This organization is currently working to secure collective [[Land rights of indigenous people|land rights]] for all the Emberá communities outside of the Comarca who become members of the organization.{{Citation needed|date=December 2013}}

== See also ==
* [[Indigenous peoples of Panama]]
* [[Indigenous peoples of Colombia]]
* [[Embera languages]]
* [[Chocó department]]
* [[Darién Province]]

== References ==
{{reflist}}

== External links ==
* [http://www.ojewp.org/ ''Organización de Jovenes Emberá y Wounaan de Panamá'' - Organization of Emberá and Wounaan Youth of Panama]
* [http://cms.onic.org.co/ ONIC - ''Organización Nacional Indígena de Colombia'' - National Indigenous Organization of Colombia]
* [http://ifnotusthenwho.me/films/indigenous-culture-needed-to-protect-forests/ - ''Being Embera'' - Short film on how indigenous culture needed to protect forests -If Not Us Then Who?]

{{Navboxes|list =
{{Indigenous peoples of the Americas}}

{{Indigenous peoples by continent}}
{{Colombian people}}
{{Ethnic Groups in Panama}}

}}

{{Authority control}}

{{DEFAULTSORT:Embera people}}
[[Category:Ethnic groups in Panama]]
[[Category:Indigenous peoples in Panama]]
[[Category:Indigenous peoples in Colombia]]
[[Category:Indigenous peoples of Central America]]