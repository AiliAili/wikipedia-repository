{{Infobox Dogbreed 
<!-- Put article text AFTER this infobox markup. See: -->
<!-- Wikipedia:WikiProject Dog breeds/Templates for more info.-->
|name= Blue Picardy Spaniel
|image= Epagneul bleu de picardie 868.jpg
|image_caption= A Blue Picardy Spaniel
|altname= Épagneul Bleu de Picardie
|nickname=  Bleu Picard 
|country= [[France]]
|fcigroup= [[FCI Pointer Group|7]]
|fcisection= 1.2 Continental Pointing Dogs: Spaniel type
|fcinum= 106
|fcistd= http://www.fci.be/Nomenclature/Standards/106g07-en.pdf
|akcgroup= 
|akcstd= 
|ankcgroup=
|ankcstd=
|ckcgroup= Group 1 - Sporting
|ckcstd= http://www.ckc.ca/en/Default.aspx?tabid=99&BreedCode=SBP
|kcukgroup=
|kcukstd= 
|nzkcgroup=
|nzkcstd= 
|ukcgroup = Gun Dog
|ukcstd = http://www.ukcdogs.com/WebSite.nsf/Breeds/EpagneulBleudePicardie
|akcfss= 
|akcmisc = 
|note= The UKC does not currently have an official standard. It currently uses the FCI standard.
|}}  <!-- End Infobox -->

The '''Blue Picardy Spaniel''' (or '''Épagneul Bleu de Picardie''') is a [[dog breed|breed]] of [[Spaniel]] originating in [[France]], from the area around the mouth of the [[River Somme]], around the start of the 20th century. It is descended from [[Picardy Spaniel]]s and [[English Setter]]s, and is described as a quiet breed that requires much exercise due to its stamina. It is especially good with children. Similar to the Picardy Spaniel, it has a distinctive coloured coat. Recognised by only a handful of kennel associations, the breed is predominantly known in France and Canada.

== Description ==

=== Appearance ===
A Blue Picardy Spaniel on average is around {{convert|22|-|24|in|cm}} high at the [[withers]] and weighs {{convert|43|-|45|lb|kg}}.<ref name="cunliffe">{{cite book |title=The Encyclopedia of Dog Breeds |last=Cunliffe |first=Juliette |year=1999 |publisher=Parragon |isbn=978-0-7525-8018-0 |page=262 }}<!--|accessdate=2009-11-28--></ref> Its coat is speckled grey black forming a bluish shade, with some black patches.<ref name=coat>{{cite web |url=http://www.fci.be/Nomenclature/Standards/106g07-en.pdf |title=FCI-Standard No 106 / 07. 09. 1998 / GB |date=1998-09-07 |work=Fédération Cynologique Internationale |accessdate=2010-01-08 }}</ref> The coat is flat or a little wavy with feathering on the ears, legs, underside and tail.<ref name=hair>{{cite web |url=http://www.furrycritter.com/resources/dogs/Blue_Picardy_Spaniel.htm |title=Blue Picardy Spaniel |publisher=The Furry Critter Network |accessdate=2010-01-08}}</ref> It has long legs with some [[setter]] characteristics.<ref name="h/weight">{{cite web |url=http://www.canadasguidetodogs.com/spanielblue.htm |title=Blue Picardy Spaniel |publisher=Canada's Guide to Dogs |accessdate=2010-01-08}}</ref>

It has a long broad nose and [[Snout|muzzle]], with thick ears covered in silky hair that usually end around the tip of the muzzle. Its chest is of medium size that descends down to the same level as the elbows. Both the forequarters and the hindquarters are well muscled. Its tail typically does not extend beyond the [[Hock (zoology)|hock]] and is normally straight.<ref name=sarah>{{cite web |url=http://www.sarahsdogs.com/breeds/blue_picardy_spaniel/ |title=Blue Picardy Spaniel Information |publisher=Sarah's Dogs |accessdate=2010-01-08}}</ref>

The breed has many similarities with the [[Picardy Spaniel]] due to the two breeds' recent history. The Blue Spaniel is described as being softer, as well as the obvious difference in coat color. The Picardy has a brown coat whereas the Blue Picardy has a black and grey coat, which was brought into the breed by the introduction of [[English Setter]] blood. Similar in the modern era due to the close similarities of the two different breed standards.<ref name=sarah2>{{cite web |url=http://www.sarahsdogs.com/breeds/picardy_spaniel/ |title=Picardy Spaniel Information |publisher=Sarah's Dogs |accessdate=2010-01-09}}</ref> In addition, the Blue Picardy is a little faster, and has a slightly finer nose.<ref name=rasinfo>{{cite web |url=http://www.epagneulbleudepicardie.nl/cms/?page_id=8 |title=Rasinfo |language=Dutch |publisher=Nederlandse Vereniging Epagneul Bleu de Picardie |accessdate=2010-01-09}}</ref>

=== Temperament ===

It is a versatile hunting dog, used for its ability to locate and retrieve game in harsh and adverse terrain and conditions.<ref>{{cite web |url=http://www.completedogsguide.com/dog-breeds/rare/Blue-Picardy-Spaniel/ |title=Blue Picardy Spaniel |year=2007 |publisher=Complete Guide to Dogs |accessdate=2010-01-09}}</ref> It is not specialised to any one type of terrain, and tends to score well in field trials.<ref name=rasinfo/> The Blue Picardy is considered to be a quiet breed, but requires a great deal of exercise as it has a high level of stamina. It loves to play, and is a responsive and obedient breed which thrives on human companionship. It is especially good with children.<ref name="cunliffe"/>

== Health ==
{{details|topic=general dog related health issues|Dog health}}
The breed has no known genetic health issues.<ref>{{cite web|url=http://www.completedogsguide.com/dog-breeds/rare/Blue-Picardy-Spaniel/|title=Blue Picardy Spaniel |publisher=Complete Dogs Guide|accessdate=2010-03-25}}</ref> Blue Picardy Spaniels can be prone to ear infections,<ref name=sarah/> which are common among dogs with pendulous ears, including [[Basset Hound]]s and other breeds of [[Spaniel]].<ref name="earinfect">{{cite web|url=http://www.healthypet.com/PetCare/PetCareArticle.aspx?art_key=11b39b10-4735-4ac9-ba43-0b348cab2c6b|title=Pet Care: Ear infections|publisher=Healthy Pet.com|accessdate=2010-02-09}}</ref> It has an average life expectancy of thirteen years.<ref>{{cite web |url=http://www.dogsindepth.com/sporting_dog_breeds/blue_picardy_spaniel.html |title=Blue Picardy Spaniel (Epagneul Bleu de Picardie) |publisher=dogsindepth.com |accessdate=2010-01-10}}</ref>

== History ==
[[File:Epagneul bleu de picardie głowa 098.jpg|thumb|right|alt="The head and shoulders of a black Spaniel with brown eyes."|A close-up of the facial features of a Blue Picardy Spaniel.]]
The first [[French Spaniel]] is speculated to have appeared following the Crusades of the 11th century,<ref name="sarah"/> and it was these breeds of dogs that were described in [[Gaston III of Foix-Béarn]]'s 14th-century work ''Livre de Chasse''.<ref name="master">{{cite book |title=[[The Master of Game]] |last=[[Edward of Norwich, 2nd Duke of York]] |year=1909 |publisher=Ballantyne, Hanson & Co |page=195 |accessdate=2009-12-18 }}</ref> Following the [[French Revolution]] the commoners in [[France]] were allowed to raise and keep their own hunting dogs. This in turn meant that the pre-existing [[French Spaniel]] split into several types specific to their own regions, which were classified according to physical appearance and hunting abilities.<ref name="sarah"/>

At the turn of the 20th century the area around the mouth of the [[River Somme]] was considered a paradise for hunters interested in [[wildfowl]]. Because of quarantine restrictions in the [[United Kingdom]], [[British people|British]] shooters would board their dogs in the Picardy area, near the mouth of the Somme. This caused the infusion of [[English Setter]] blood into the local Spaniel population and developed the Blue Picardy Spaniel.<ref name=bred>{{cite web |url=http://www.petstyle.com/dogs/breeds/blue-picardy-spaniel |title=Blue Picardy Spaniel  |publisher=Pet Style |accessdate=2010-01-08 }}</ref><ref>{{cite web |url=http://www.irishfieldsports.com/hpr/frenchspaniels.htm |title=European Gundog Breeds: French Spaniels |publisher=Irish Field Sports.com |accessdate=2010-01-08 |archiveurl = https://web.archive.org/web/20080507052845/http://www.irishfieldsports.com/hpr/frenchspaniels.htm |archivedate = May 7, 2008|deadurl=yes}}</ref>

While the first black, blue-grey Spaniel was recorded in 1875, it was not until 1904 when the [[Picardy Spaniel]] was first shown. This Spaniel was officially classified as a French Spaniel, and was shown at the Paris Canine Exposition. When the Picard Spaniel and Blue Picardy Spaniel Club was formed in 1907 the two different breeds of Picardy Spaniel were categorised.<ref name="alex"/>

In France, the Blue Picardy was recognised as a separate breed in 1938,<ref name="alex">{{cite web |url=http://www.bluepicardy.com/spaniel/bps_hist.htm |title=A Brief History of the Blue Picardy Spaniel |publisher=Alex Cyrill Sporting Dogs |accessdate=2010-01-08}}</ref> and there are about 1000 puppies born in France each year.<ref name=rasinfo/> The first person to import the Blue Picardy Spaniel into [[Canada]] was Ronald Meunier of [[Saint-Julien, Quebec]], around 1987, and the breed was then recognised by the [[Canadian Kennel Club]] effective 1 June 1995.<ref>{{cite web |url=http://www.canadasguidetodogs.com/spaniel/bluearticle2.htm |title=The Blue Picardy Spaniel in Canada |author=Fath, Don |publisher=Canada's Guide to Dogs |accessdate=2010-01-09}}</ref> The breed is recognised by the [[American Rare Breed Association]], which uses the same standard as the [[Fédération Cynologique Internationale]].<ref>{{cite web |url=http://www.fci.be/Nomenclature/Standards/106g07-en.pdf |title=FCI-Standard N° 106 / 07. 09. 1998 / GB |publisher=American Rare Breeds Association |accessdate=2010-01-08}}</ref>

== References ==
{{reflist|2}}

== External links ==
{{commons category|Epagneul bleu de Picardie}}
* [http://www.club-epi-ebp-epa.com/ Espagneuls Picards, Bleus de Picardie & Pont Audemer Club (In French)]
* [http://www.epagneulbleudepicardie.nl/cms/ Nederlandse Vereniging Epagneul Bleu de Picardie (In Dutch)]

{{Gundogs}}
{{French dogs}}
{{good article}}

{{DEFAULTSORT:Blue Picardy Spaniel}}
[[Category:Dog breeds originating in France]]
[[Category:Sporting dogs]]
[[Category:Spaniels]]
[[Category:Rare dog breeds]]