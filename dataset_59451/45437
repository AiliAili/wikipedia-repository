{{Other people|William Knight}}
{{Infobox astronaut
|name          =William J. Knight
|other_names   =William John Knight
|image         =Pete_Knight.jpg
|image_size    = 
|caption       = 
|type          =[[United States Air Force|USAF]] [[Astronaut]]
|nationality   =American
|birth_date    ={{Birth date|1929|11|18}}
|death_date    ={{Death date and age|2004|05|07|1929|11|18}}
|birth_place   =[[Noblesville, Indiana]], U.S.
|death_place   =[[Los Angeles]], California, U.S.
|occupation    =[[Test pilot]]
|alma_mater    =[[Butler University]]<br>[[Purdue University]]<br>[[Air Force Institute of Technology]], B.S. 1958
|rank          =[[Colonel (United States)|Colonel]], [[United States Air Force|USAF]]
|selection     =[[List of astronauts by selection#1960|1960 Dyna-Soar Group 1]]
|time          =
|mission       =[[X-15#Highest flights|X-15 Flight 190]]
|awards        =[[File:Dfc-usa.jpg|20px|link=Distinguished Flying Cross (United States)]]
}}
'''William John "Pete" Knight''' (November 18, 1929&nbsp;– May 7, 2004), ([[Colonel (United States)|Col]], [[United States Air Force|USAF]]), was an American [[aeronautical engineer]], [[politician]], [[Vietnam War]] combat [[aviator|pilot]], [[test pilot]], and [[astronaut]] in the [[Boeing X-20 Dyna-Soar|X-20 Dyna-Soar]] and [[North American X-15]] programs. Knight holds the world's speed record for flight in a winged, powered aircraft.

As a politician, he is noted as the author of [[California Proposition 22 (2000)|California Proposition 22]], which forbade the state from performing or recognizing [[same-sex marriage]].

==Early life and education==
Knight was born November 18, 1929 in [[Noblesville, Indiana]], to parents William T. Knight (1906–1968) and Mary Emma Knight (1909–1959).<ref>[http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=8749323 Knight's parents]</ref> Following high school, Knight attended [[Butler University]] and [[Purdue University]]. He graduated with a [[Bachelor of Science]] degree in [[Aeronautical Engineering]] from the U.S. [[Air Force Institute of Technology]] in 1958.

==Personal==
Knight was married to Helena Stone and they had three sons, [[Steve Knight (politician)|Steve]], Peter, and David. Helena preceded Knight in death. Knight remarried and at his death in 2004 he was survived by his widow Gail, a brother, three sons, four stepchildren and 15 grandchildren.

==Air Force career==
Knight joined the [[United States Air Force]] in 1951. While only a [[Second Lieutenant#United States|Second Lieutenant]], he flew an [[F-89 Scorpion|F-89]] at the National Air Show in 1954 and won the [[Allison Jet Trophy]].

Starting in 1958, following his graduation from both U.S. Air Force Institute of Technology and the [[U.S. Air Force Test Pilot School|Air Force Experimental Flight Test Pilot School]], Knight served as a test pilot at [[Edwards Air Force Base]], [[California]]. He was a project test pilot for the [[F-100 Super Sabre|F-100]], [[F-101 Voodoo]], [[F-104 Starfighter]] and later, [[T-38 Talon|T-38]] and [[F-5 Freedom Fighter|F-5]] test programs. In 1960, he was one of six test pilots selected to fly the [[X-20 Dyna-Soar]], which was slated to become the first winged orbital space vehicle capable of lifting reentries and conventional landings. After the X-20 program was canceled in 1963, he completed the astronaut training curriculum through the [[Aerospace Research Pilot School]] at Edwards AFB and was selected to fly the [[North American X-15]].

He had more than his share of eventful flights in the X-15. While climbing through {{Convert|107000|ft}} at [[Mach number|Mach]] 4.17 on June 29, 1967, he suffered a total electrical failure and all onboard systems shut down. After reaching a maximum altitude of {{Convert|173000|ft}}, he calmly set up a visual approach and, resorting to old-fashioned "seat-of-the-pants" flying, he glided down to a safe emergency landing at Mud Lake, [[Nevada]].<ref>Mud Lake, Nye County, Nevada, {{Coord|37|52|10|N|117|04|17|W}}.  Located at the northern edge of the [[Tonopah Test Range]], this is the southernmost Mud Lake of several dry lakes bearing the same name in Nevada.</ref>  For his remarkable feat of airmanship that day, he earned a [[Distinguished Flying Cross (United States)|Distinguished Flying Cross]].

[[Image:Pete Knight by X15 19671003.jpg|left|thumb|Maj. William "Pete" Knight with the X-15]]
On October 3, 1967, Knight set a world aircraft speed record for manned aircraft by piloting the X-15A-2 to {{Convert|4520|mph|km/h|0|sp=us}} ([[Mach number|Mach]] 6.72), a record that still stands today. During 16 flights in the aircraft, Knight also became one of only five pilots to earn their [[Astronaut Badge|Astronaut Wings]] by flying an airplane in space, reaching an altitude of {{Convert|280500|ft}}.

After nearly ten years of test flying at Edwards AFB, he went to [[Southeast Asia]] in 1968 where he completed a total of 253 [[Aerial warfare|combat missions]] in the [[F-100 Super Sabre|F-100]]. Following his combat tour, he served as test director during development of the [[F-15 Eagle]] at [[Wright Patterson Air Force Base]] in [[Dayton, Ohio]]. He also was the Program Director for the International Fighter (F-5) Program at Wright-Patterson. In 1979, he returned to Edwards AFB, and served as a test pilot for the [[F-16 Fighting Falcon]].

After 32 years of service and more than 6,000 hours in the cockpits of more than 100 different aircraft, he retired from the U.S. Air Force as a [[Colonel (United States)|Colonel]] in 1982.

==Political career==
In 1984, he was elected to the city council of [[Palmdale, California]], and four years later became the city's first elected mayor. During his time as mayor of Palmdale, it was the fastest growing city in the United States.{{citation needed|date=October 2014}}  In 1992, he was elected to serve in the [[California State Assembly]] representing the 36th District. He served in the [[California State Senate|State Senate]] representing the 17th District from 1996 until his death on May 7, 2004. Knight's youngest son, [[Steve Knight (politician)|Steve Knight]] served as Assemblyman for the 36th Assembly District from 2008 to 2012, the seat previously held by his father.

===Proposition 22===
{{Main| California Proposition 22 (2000)}}
During his term in the Senate, Knight gained statewide attention in 2000 as the author of [[California Proposition 22 (2000)|Proposition 22]], a.k.a. the "Knight Initiative," the purpose of which was to ban same-sex marriage: "Only marriage between a man and a woman is valid or recognized in California." The proposition passed with 61.4% approval and 38.6% against. On March 9, 2004, Knight's son, David Knight, who is gay, married his partner during the period when [[Same-sex marriage in California#2004 San Francisco marriages|San Francisco performed same-sex marriages in defiance of state law]]. These marriages were later nullified by the [[California Supreme Court]] in 2004. The Court later found Proposition 22 to be unconstitutional in ''[[In re Marriage Cases]]'' (2008).<ref>{{cite news| url=http://articles.sfgate.com/2010-08-05/news/22206069_1_ban-on-same-sex-marriage-gay-rights-proponents-gays-and-lesbians | work=The San Francisco Chronicle | first1=Joe | last1=Garofoli | first2=John | last2=Wildermuth | first3=Demian | last3=Bulwa | title=Mormons urged to back ban on same-sex marriage | date=August 5, 2010}}</ref>

==Watch==
*[http://www.scvtv.com/html/sg042504-nm.html ''Pete Knight's Final Television Interview''] (30 min., free, taped 4-1-2004)

==Awards and honors==
*[[Legion of Merit]]
*[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]
*[[Air Medal]]
*[[Harmon Trophy]], 1967
*[[AIAA]] [[Octave Chanute Award]], 1968
*Enshrined in the [[National Aviation Hall of Fame]] in 1988.<ref>{{cite web |url=http://www.nationalaviation.org/enshrinees/ | title=National Aviation Hall of fame: Our Enshrinees |publisher=[[National Aviation Hall of Fame]] |accessdate=February 10, 2011}}</ref>
*Inducted into the [[Aerospace Walk of Honor]] in 1990.<ref>[http://cityoflancasterca.org/index.aspx?page=210 Knight inducted into the Aerospace Walk of Honor]</ref>
*Inducted into the [[International Space Hall of Fame]] in 1998.<ref>[http://www.nmspacemuseum.org/halloffame/detail.php?id=140 Knight inducted into the International Space Hall of Fame]</ref>

In the city of Palmdale, [[Pete Knight High School]] was opened in his memory.{{citation needed|date=October 2014}} The school began its first year in the school year of 2003-2004 and celebrated its first graduating class in 2007.

==References==
{{Reflist}}

==Bibliography==
*Thompson, Milton O. (1992) ''At The Edge Of Space: The X-15 Flight Program'', Smithsonian Institution Press, Washington and London. ISBN 1-56098-107-5
*{{cite web|title=Maj. William "Pete" Knight|author=Dr. James Young|accessdate=2008-10-22|url=http://www.af.mil/news/story.asp?id=123120593}}

==External links==
{{Portal|Biography|United States Air Force}}
*[http://history.nasa.gov/x15/knight.html Knight's official NASA biography]
*[http://www.astronautix.com/astros/knight.htm Astronautix biography of Pete Knight]
*[http://www.spacefacts.de/bios/astronauts/english/knight_william.htm Spacefacts biography of Pete Knight]
*[http://www.nationalaviation.org/knight-william/ Pete Knight] at the [[National Aviation Hall of Fame]]
*[http://www.nmspacemuseum.org/halloffame/detail.php?id=140 Knight at International Space Hall of Fame]
*{{Find a Grave|8749323|Pete Knight}}

{{S-start}}
{{S-off}}
{{S-bef| before = [[Cathie Wright]] }}
{{S-ttl| title  = [[California State Assembly]]man <br/>'''36th District'''
       | years  = December 7, 1992 – November 30, 1996 }}
{{S-aft| rows   = 2 | after  = [[George Runner]] }}
{{S-bef| before = [[Don Rogers (California politician)|Don Rogers]] }}
{{S-ttl| title  = [[California State Senate|California State Senator]] <br/>'''17th District'''
       | years  = December 2, 1996 – May 7, 2004 }}
{{S-end}}

{{DEFAULTSORT:Knight, William J.}}
[[Category:1929 births]]
[[Category:2004 deaths]]
[[Category:United States Air Force astronauts]]
[[Category:American test pilots]]
[[Category:American engineers]]
[[Category:American aerospace engineers]]
[[Category:People from Noblesville, Indiana]]
[[Category:California Republicans]]
[[Category:California city council members]]
[[Category:Members of the California State Assembly]]
[[Category:California State Senators]]
[[Category:Deaths from leukemia]]
[[Category:Air Force Institute of Technology alumni]]
[[Category:U.S. Air Force Test Pilot School alumni]]
[[Category:United States Air Force officers]]
[[Category:American military personnel of the Vietnam War]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Harmon Trophy winners]]
[[Category:X-15 program]]
[[Category:American astronaut-politicians]]
[[Category:20th-century American politicians]]
[[Category:21st-century American politicians]]
[[Category:People from Palmdale, California]]