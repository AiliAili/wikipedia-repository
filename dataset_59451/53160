{{Infobox journal
| title = Zeitschrift für deutsches Altertum und deutsche Literatur
| cover =
| editor = Jürgen Wolf
| discipline = [[German studies]]
| language = German
| former_names = Zeitschrift für deutsches Alterthum
| abbreviation = Z. dtsch. Altert. dtsch. Lit.
| publisher = [[S. Hirzel Verlag]]
| country = Germany
| frequency = Quarterly
| history = 1841–present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://www.zfda.de/
| link1 = http://www.zfda.de/inhalt.php?mode=jahrgang
| link1-name = Online tables of content
| link2 =
| link2-name =
| JSTOR = 00442518
| OCLC = 645345094
| LCCN = sn85004980
| CODEN =
| ISSN = 0044-2518
| eISSN =
}}
The '''''Zeitschrift für deutsches Altertum und deutsche Literatur''''' (commonly abbreviated '''''ZfdA''''') is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[German studies]] with emphasis on the older periods. It was established in 1841 and is the oldest periodical in early [[Germanic studies]] still publishing.

== History ==
The journal was established in 1841 by [[Moriz Haupt]] as the ''Zeitschrift für deutsches Alterthum'' (older spelling) with the objective of applying the same rigour to the [[philology]] and textual criticism of medieval German texts as was already current with Greek and Latin.<ref name=Meves/>

With volume 13 (1867) the ''Zeitschrift für deutsches Alterthum'' began a new series ({{lang-de|Neue Folge}}).<ref name=Chron163>[[Kurt Ruh]], [http://www.jstor.org/stable/20655699 "Kleine Chronik der Zeitschrift für deutsches Altertum und deutsche Literatur"], ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 100 (1971) 163–65, p. 163 {{de icon}}</ref> In 1876, with volume 19 (New Series 7) its name was changed to the present ''Zeitschrift für deutsches Altertum und deutsche Literatur'' and a supplement, the ''Anzeiger'', began publication as a journal of reviews;<ref>[[Franz Josef Worstbrock]], [http://www.jstor.org/stable/20657952 "Mitteilung des Herausgebers"], ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 119 (1990) 1&ndash;3, p. 1 {{de icon}}</ref> this sometimes overshadowed the parent journal.<ref name=Chron164>Ruh, "Chronik", p. 164.</ref>

The journal has appeared in quarterly issues since 1931. It was originally published by the [[Weidmannsche Buchhandlung]], first in [[Leipzig]], from 1856 in [[Berlin]]. With the third issue of volume 82 (1948/50), [[Franz Steiner Verlag]] took over publication.<ref name=Chron165>Ruh, "Chronik", p. 165.</ref> The current publisher is [[S. Hirzel Verlag]].

Since volume 140 (2011) the [[editor-in-chief]] has been [[Jürgen Wolf]] ([[University of Marburg]]).<ref name=Heinzle>[[Joachim Heinzle]], "Mitteilung des Herausgebers", ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 139 (2010), p. 417 {{de icon}} ([http://www.zfda.de/download/mitteilung.pdf pdf])</ref>

=== Editors ===
* 1841 &ndash; 1874: [[Moriz Haupt]] (only 16 volumes appeared)<ref name=Chron163/><ref>According to Mirko Nottscheid and Hans H. Müller with Myriam Richter, eds., Wilhelm Scherer, ''Briefe und Dokumente aus den Jahren 1853 bis 1886'', Marbacher Wissenschaftsgeschichte 5, Göttingen: Wallstein, 2005, ISBN 9783892448266, [https://books.google.com/books?id=6UowAR7jqjIC&pg=PA177&dq=Zeitschrift+f%C3%BCr+deutsches+Altertum&hl=en&sa=X&ei=ZoZZUuW_Cc2ZjAKYmIHYCA&ved=0CFQQ6AEwCDge#v=onepage&q=Zeitschrift%20f%C3%BCr%20deutsches%20Altertum&f=false p. 177] {{de icon}}, [[Wilhelm Scherer]] became editor in 1872.</ref>
* 1874 &ndash; 1883: [[Karl Müllenhoff]] and [[Elias von Steinmeyer]]
* 1874 &ndash; 1890: Elias von Steinmeyer<ref name=Chron164/>
* 1891 &ndash; 1926: [[Gustav Roethe]] and [[Edward Schröder]]
* 1926 &ndash; 1931: Edward Schröder<ref>Ruh, "Chronik", pp. 164&ndash;65.</ref>
* 1932 &ndash; 1938: Edward Schröder and [[Arthur Hübner]]
* 1939 &ndash; 1955: [[Julius Schwietering]]
* 1956 &ndash; 1963: Julius Schwietering and [[Friedrich Ohly]]
* 1963 &ndash; 1969: Friedrich Ohly
* 1969 &ndash; 1985: [[Kurt Ruh]]<ref name=Chron165/><ref>Kurt Ruh, [http://www.jstor.org/stable/20657668 "Mitteilung des Herausgebers"], ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 114 (1985) 289 {{de icon}}</ref>
* 1986 &ndash; 1997: [[Franz Josef Worstbrock]]<ref>Franz Josef Worstbrock, [http://www.jstor.org/stable/20658954 "Mitteilung des Herausgebers], ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 126 (1997) 494 {{de icon}}</ref>
* 1998 &ndash; 2010: [[Joachim Heinzle]]
* 2011 &ndash; present: [[Jürgen Wolf]]<ref name=Heinzle/>

== Editorial philosophy ==
Haupt wrote a statement of purpose in the first issue in which he set out the journal's range: "the literature, language, customs, legal history [and] belief of German antiquity".<ref>"die literatur, die sprache, die sitten, die rechtsalterthümer, der glauben der deutschen vorzeit": cited in Kurt Ruh, [http://www.jstor.org/stable/20655669 "Zum hundertsten Jahrgang der Zeitschrift für deutsches Altertum und deutsche Literatur"], ''Zeitschrift für deutsches Altertum und deutsche Literatur'' 100 (1971) 1&ndash;3, p. 1 {{de icon}}</ref> The major focus from the beginning was publishing editions of [[Old High German|Old]] and [[Middle High German]] works, which were presented for the most part for an academic readership, without explanatory material.<ref name=Meves>Uwe Meves, "Die Entstehung und frühe Entwicklung der Germanischen Philologie", in ''History of the language sciences: an international handbook on the evolution of the study of language from the beginning to the present/Geschichte der Sprachwissenschaften: ein internationales Handbuch zur Entwicklung der Sprachforschung von den Anfängen bis zur Gegenwart/Histoire des sciences du langage: manuel international sur l'évolution de l'étude du langage des origines à nos jours'', ed. Sylvain Auroux ''et al''., Volume 2, Handbücher zur Sprache- und Kommunikationswissenschaft 18.2, Berlin: de Gruyter, 2001, ISBN 9783110167351, pp.&nbsp;1286&ndash;93, [https://books.google.com/books?id=DI6hb3x_eY4C&pg=PA1260&dq=Zeitschrift+f%C3%BCr+deutsches+Altertum&hl=en&sa=X&ei=OMdWUoGoNYS-igL2_4CYBA&ved=0CEsQ6AEwBA#v=onepage&q=Zeitschrift%20f%C3%BCr%20deutsches%20Altertum&f=false p.&nbsp;1260]{{de icon}}</ref> The journal "quickly became the most important academic forum for the 'Berlin school'".<ref>Nottscheid, Müller and Richter: "... die schnell zum wichtigsten wissenschaftlichen Forum der 'Berliner Schule' avancierte".</ref> In reaction to the narrowness of its approach, a quarterly named ''[[Germania: Vierteljahrsschrift für deutsche Altertumskunde|Germania]]'' was founded by [[Franz Pfeiffer]] in 1856, with the objective of reflecting the broader approach of [[Jacob Grimm]] and of classical philology and of therefore laying a greater emphasis on articles than editions. The foundation of the ''[[Zeitschrift für deutsche Philologie]]'' in 1868 was also a reaction to the ''ZfdA'''s restricted focus, in its case intended to supplement it.<ref name=Meves/> For at least a century, this emphasis on philology and relatively few changes of editor left the ''Zeitschrift für deutsches Altertum und deutsche Literatur'' largely untouched by changes in opinion in the field. Until the end of World War II, the editor was traditionally the Chairman of the Faculty of ''Germanistik'' at the [[Humboldt University of Berlin|University of Berlin]]. It was a relatively impersonal publication, sometimes accused of arrogance, and did not even include death notices for many decades.<ref>Ruh, "Zum hundertsten Jahrgang", p. 3.</ref>

== References ==
{{Reflist|30em}}

==External links==
* {{Official website|http://www.zfda.de/}}
* [http://www.digizeitschriften.de/dms/toc/?IDDOC=141814 ''Zeitschrift für deutsches Alterthum'', volume 1 (1841) &ndash; New Series volume 6 (volume 18, 1875)] online at DigiZeitschriften
* [http://www.digizeitschriften.de/dms/toc/?PPN=PPN345204123 ''Zeitschrift für deutsches Altertum und deutsche Literatur'', New Series volume 7 (volume 19, 1876) &ndash; volume 136 (2007)] online at DigiZeitschriften

{{DEFAULTSORT:Zeitschrift fur deutsches Altertum und deutsche Literatur}}
[[Category:Quarterly journals]]
[[Category:Cultural journals]]
[[Category:Publications established in 1841]]
[[Category:German-language journals]]
[[Category:Germanic philology journals]]