<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=SNCAC NC.510
 | image=NC.510a.tif
 | caption=NC.510
}}{{Infobox Aircraft Type
 | type=[[army co-operation aircraft|army co-operation]] or advanced [[training aircraft]]
 | national origin=[[France]]
 | manufacturer=Société Nationale de Constructions Aéronautique du Centre ([[SNCAC]])
 | designer=
 | first flight=20 June 1938
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''SNCAC NC.510''' was a twin-engine [[France|French]] [[reconnaissance aircraft|reconnaissance]], [[army co-operation aircraft|army co-operation]] or advanced [[training aircraft]], built in the late 1930s. Three were built and refined but production orders were not forthcoming.

==Design and development==

[[File:NC.510.tif|thumb|right|NC.510M at the December 1938 Paris Aero Show]]
The Société Nationale de Constructions Aéronautique du Centre (SNCAC) was formed via 1936 legislation in February 1937 as a nationalised merging of the [[Farman]] and [[Hanriot]] companies, in whose design offices several SNCAC types began.<ref name=JAWA38a/>  Thus the NC.510 was often referred to as the '''Hanriot 510'''<ref name=JAWA38b/> or '''Hanriot NC.510'''.<ref name=Flight/>

The NC.510 was designed for [[army co-operation aircraft|army co-operation]] work and as an advanced [[training aircraft]]. It was a twin-engined [[monoplane#Types of monoplane|mid-wing monoplane]].  Between the engines and fuselage the wing was rectangular in plan and externally braced; the outboard panels were tapered [[cantilever]]s.  The wings were built around two metal [[spar (aviation)|spars]] and the centre section was all metal, with metal [[rib]]s and skinning.  Outboard their construction was mixed, with wooden ribs and a mixture of metal and [[aircraft fabric covering|fabric covering]]. [[flap (aircraft)#Split flap|Split flaps]] were fitted.<ref name=JAWA38b/>

The fuselage consisted of two sections, both rounded in cross-section.  The forward part, including the enclosed cockpit where the pilot was seated forward of the wing [[leading edge]] and an observer, provided with dual controls, behind him, was [[spruce]] framed and [[plywood]] covered. The observer could also access a long, rectangularly framed, largely transparent, ventral observation structure. The rear fuselage section was also spruce-framed but internally wire-braced and fabric-covered. A rear-facing glazed enclosure over the wing [[trailing edge]] held the rear gunner/radio operator.  A transverse metal structure within the fuselage connected it to the two pairs of wing struts.  There was another metal frame aft for mounting the [[empennage]], a braced wooden structure mounted on top of the fuselage.  The tailplane had significant [[dihedral (aircraft)|dihedral]] and carried twin oval [[fin]]s and [[rudder]]s. Its fixed surfaces were plywood-covered; the [[elevator (aircraft)|elevator]]s and [[rudder]]s were fabric-covered with mass and aerodynamically [[balanced rudder|balances]].  The rudders had [[trim tab]]s.<ref name=JAWA38b/>  The NC.510 had a fixed, [[conventional undercarriage]] with vertical, [[oleo strut|oleo]] [[shock absorber]] legs attached to the forward wing spar just inside the engines, braced with a [[strut]] to the rear spar.  Legs and wheels were enclosed in [[aircraft fairing|fairings]], and there was a sprung tail skid. The aircraft carried three [[machine gun]]s, one fixed in the nose and one moveable in each of the rear [[dorsal (anatomy)|dorsal]] and the ventral positions.  There were racks for flares and for phosphorus [[bombs]] in addition to a mixture of handheld and remotely operated cameras for reconnaissance.<ref name=JAWA38b/>

The NC.510 first flew on 20 June 1938,<ref name=af510/> powered by two {{convert|770|hp|kW|abbr=on|0|disp=flip}} [[Gnome-Rhône 9K]]fr 9-cylinder [[Radial engine|air-cooled radials]] driving two-blade, wooden, fixed-pitch [[propeller (aircraft)|propeller]]s.  By December that year it was on display at the Paris Aero Salon with 14-cylinder {{convert|680|hp|kW|abbr=on|0}} [[Gnome-Rhône 14M]] double-row [[radial engine]]s and three-blade propellers.<ref name=Flight/>  Though these engines had a smaller displacement than the earlier 9Ks and consequently a lower power output, they were more compact with a diameter of {{convert|0.950|m|in|abbr=on|1}} compared with {{convert|1.306|m|in|abbr=on|1}}, reducing the engine frontal area by 47%.<ref name=JAWA38c/> The cleaned-up version first flew with its new engines on 14 January 1939 and was designated the '''NC.510M'''.<ref name=af510M/>

A final version, the '''NC.530''', was further aerodynamically cleaned and speeded, chiefly by the removal of the ventral gondola. It first flew on 28 June 1939. Two were completed but no production order was won.<ref name=af510M/>
<!-- ==Operational history== -->

==Variants==
;NC.510: 9-cylinder, {{convert|770|hp|kW|abbr=on|0}} [[Gnome-Rhône 9K]]fr [[radial engine]]s. One only.
;NC.510M: 14-cylinder {{convert|680|hp|kW|abbr=on|0}} [[Gnome-Rhône 14M]] [[radial engine]]s. NC.510 modified, first flew 14 January 1939.<ref name=af510M/>
;NC.530: Modified fuselage without ventral pod; engines as NC.510M. First flew 28 June 1939. Two built.<ref name=af530/>
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (NC.510)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 1938 p.113-4c<ref name=JAWA38b/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=Three
|length m=10.12
|length note=
|span m=15.00
|height m=2.91
|height note=
|wing area sqm=13.5
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=2757
|empty weight note=
|gross weight kg=3720
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity={{convert|480|kg|0|abbr=on}} 
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Gnome-Rhône 9K]]fr
|eng1 type=9-cylinder [[Radial engine|air-cooled radials]] 
|eng1 hp=770
|eng1 note=, rated at 3,000 m (9,840 ft)
|power original=
|more power=

|prop blade number=2
|prop name=fixed pitch, wooden
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=350
|max speed note=at 3,000 m (9,840 ft)
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1350
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=7000
|ceiling note=service
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=6 min to 3,000 m (9,840 ft)
|lift to drag=
|wing loading kg/m2=118
|wing loading note=
|power/mass=300 W/kg (0.182 hp/lb)

|more performance=
<!--
        Armament
-->
|guns= 3 [[machine gun]]s, one in each of nose, dorsal and ventral positions
|bombs= racks for bombs and flares
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{commons category|SNCAC}}
{{reflist|refs=

<ref name=JAWA38a>{{cite book|title=Jane's All the World's Aircraft 1938|last=Grey |first=C.G.|year=1972|publisher= David & Charles|location= London|isbn=0715 35734 4|pages=102c}}</ref>

<ref name=JAWA38b>{{cite book|title=Jane's All the World's Aircraft 1938|last=Grey|pages=113–4c}}</ref>

<ref name=JAWA38c>{{cite book|title=Jane's All the World's Aircraft 1938|last=Grey|pages=43–4d}}</ref>

<ref name=Flight>{{cite magazine|date=30 March 1939 |title=Army Co-operation Types|magazine=[[Flight International|Flight]]|volume=XXXV|issue=1579|page=1138|url=http://www.flightglobal.com/pdfarchive/view/1939/1939%20-%200932.html}}</ref>

<ref name=af510>{{cite web |url=http://www.aviafrance.com/s-n-c-a-c-nc-510-aviation-france-5680.htm|title=S.N.C.A.C. NC-510|accessdate=26 May 2013}}</ref>

<ref name=af510M>{{cite web |url=http://www.aviafrance.com/s-n-c-a-c-nc-510m-aviation-france-5681.htm|title=S.N.C.A.C. NC-510M|accessdate=26 May 2013}}</ref>

<ref name=af530>{{cite web |url=http://www.aviafrance.com/s-n-c-a-c-nc-530-aviation-france-1325.htm|title=S.N.C.A.C. NC-530|accessdate=26 May 2013}}</ref>
}}

{{SNCAC aircraft}}
{{Hanriot aircraft}}

[[Category:SNCAC aircraft|050]]
[[Category:French military aircraft 1930–1939]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Monoplanes]]
[[Category:French patrol aircraft]]