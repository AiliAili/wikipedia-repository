{{for|the soundtrack album|Space Ghost Coast to Coast (album)}}
{{Use mdy dates|date=June 2013}}
{{Infobox television
|image = [[File:Space Ghost Coast to Coast.png|250px|alt=Space Ghost, Moltar, and Zorak sit around a coffee table]]
|caption = From the left: [[Space Ghost]], Moltar and [[Zorak]].
|genre = [[Comedy]]<ref>{{cite web|url=http://www.allmovie.com/movie/space-ghost-coast-to-coast-animated-tv-series-v309268
|title=Space Ghost Coast to Coast [Animated TV Series&#93; (1994) - Trailers, Reviews, Synopsis, Showtimes and Cast|publisher=AllMovie|accessdate=March 4, 2015}}</ref>
|executive_producer = [[Mike Lazzo]]<br>[[Keith Crofford]]<br>[[Matt Harrigan]]
|runtime = 15–30 minutes
|company = [[Williams Street|Ghost Planet Industries]] {{small|(1994–98)}}<br>[[Williams Street]] {{small|(1999–2008)}}<br>[[Williams Street West]] {{small|(2003–04)}}<br>[[Cartoon Network]] {{small|(1994–2001)}}
|distributor = [[Warner Bros. Television]]
|creator = [[Mike Lazzo]]
|voices = [[George Lowe]]<br>[[C. Martin Croker]]<br>[[Andy Merrill]]<br>[[Don Kennedy]]
|composer = [[Sonny Sharrock]]<br>Eddie Horst<br>[[Man or Astro-man?]]
|country = United States
|network = [[TBS (U.S. TV channel)|TBS]] <small>(1994–96)</small><br>[[TNT (TV channel)|TNT]] <small>(1994–95)</small><br>[[Cartoon Network]] <small>(1994–2001)</small><br>[[Adult Swim]] <small>(2001–04)</small><br>[[GameTap]] <small>(2006–08)</small>
|picture_format = [[4:3]] [[SDTV]]
|first_aired = <small>'''Cartoon Network series:'''</small><br />April 15, 1994 – <br />December 17, 1999<br> <small>'''Adult Swim series:'''</small><br />May 7, 2001 – <br />April 12, 2004<br> <small>'''GameTap series:'''</small><br />May 30, 2006
|last_aired = May 31, 2008
|preceded_by = ''[[Space Ghost (TV series)|Space Ghost]]''
|followed_by = ''[[Cartoon Planet]]''<br>''[[The Brak Show]]''<br>''[[Aqua Teen Hunger Force]]''<br>''[[Sealab 2021]]''<br>''[[Harvey Birdman: Attorney at Law]]''<br>''[[Perfect Hair Forever]]''
|num_episodes = 110 (and 2 unaired pilots and 3 specials)
|list_episodes  = List of Space Ghost Coast to Coast episodes
|num_seasons = 10
|website = http://www.adultswim.com/shows/space-ghost-coast-to-coast/
 }}
'''''Space Ghost Coast to Coast''''' is an American animated parody talk show, created by [[Mike Lazzo]] and hosted by the 1960s [[Hanna-Barbera]] cartoon character [[Space Ghost]]. Though the original 1960s series aired as a standard Hanna-Barbera [[Saturday-morning cartoon|Saturday-morning superhero cartoon]], ''Space Ghost Coast to Coast'' was a total [[Reboot (fiction)|reboot]] of the series intended for adults, now reinterpreted as a [[Surreal humour|surreal]] spoof [[talk show]] and produced using the original artwork.

''Space Ghost Coast to Coast'' was considered to be the first fully produced [[Cartoon Network]] original series, and premiered on April 15, 1994. The first two seasons were presented as a serious talk show with subdued jokes, while the later seasons relied more on [[surrealism]], [[non sequitur (literary device)|non-sequiturs]], and parodies. In 2001, it moved to the network's [[late night television]] block [[Adult Swim]].

''Space Ghost Coast to Coast'' was known for inspiring the series ''[[Cartoon Planet]], [[The Brak Show]]'', ''[[Sealab 2021]]'', ''[[Aqua Teen Hunger Force]]'', ''[[Harvey Birdman, Attorney At Law]]'',  ''[[Perfect Hair Forever]]'' and ''[[The Eric Andre Show]]''.

==Format==
''Space Ghost Coast to Coast'' used the talk show format as its template, but subverted it regularly. Various celebrities appeared on the show as guests. They were shown on a TV screen next to Space Ghost's desk, and unlike the characters, they were not animated. In early episodes of the show, Space Ghost apparently believed his guests were other superheroes and usually opened the interview by asking them about their superpowers. His interactions with guests were almost always painfully awkward, and sometimes hostile. It was sometimes hard to tell if guests were aware of the nature of the program on which they were appearing. Their answers often did not match the questions coming from Space Ghost, because the questions were changed after the interview.

Space Ghost's relationship with his co-workers was even worse. His [[bandleader]], an evil talking [[mantis]] named [[Zorak]], and his director/producer, a red-helmeted lava man named Moltar, worked for Space Ghost as punishment for their crimes. They frequently disrupted the show and made no secret of the fact that they hated him.

Most episodes of ''Space Ghost Coast to Coast'' were about 15 minutes in length, although there were a few 30-minute episodes. Cartoon Network often aired two episodes back-to-back to make a 30-minute [[Block programming|programming block]]. In the first few years of the show, Cartoon Network would show episodes of the original 1960s and 1980s ''[[Space Ghost (TV series)|Space Ghost]]'' cartoons (sometimes with an unusual added [[laugh track]]) after the 11-minute episode of ''Space Ghost Coast to Coast''.

===Music===
In early seasons of the show, music was played by Zorak and his band "The Original Way-Outs". The original theme song, "Hit Single", was composed by [[free jazz]] guitarist [[Sonny Sharrock]], performed by Sharrock on guitar, Lance Carter on drums, Eddie Horst on bass and Alfreda Gerald on vocals. Sharrock and Carter recorded a number of songs for the show, several of which were later compiled on the [[music album|album]] ''[[Space Ghost Coast to Coast (album)|Space Ghost Coast to Coast]]''. As a tribute to Sharrock, who died in May 1994 shortly after the show first aired, the episode "Sharrock" featured nearly fifteen minutes of unedited takes of music recorded for the show.

Seasons 4–6 featured a new closing theme by [[Man or Astro-man?]] and in later seasons the opening theme and titles were almost completely abandoned. Various other music was sometimes used as the theme song, including the ''[[CHiPs]]'' theme song for the episode titled "CHiPs". An hour-long musical season finale for the 1998 season was planned, featuring the bands [[Yo La Tengo]] and [[Cornershop]], but was never produced.<ref>{{cite news|url=https://books.google.com/books?id=-w0EAAAAMBAJ&pg=PA22&lpg=PA22&dq=combustible+edison+space+ghost&source=bl&ots=b9S22CKQHK&sig=PCDnraD0kVmglAiJjXkkoEusgZE&hl=en&sa=X&ei=hruVULnLMIrhiwLJ6oGoBQ&ved=0CFIQ6AEwCQ#v=onepage&q=yo%20la%20tengo&f=false|title=On The Tube|date=June 20, 1996|publisher=''[[Billboard Magazine]]''|last=McCormick|first=Moira|accessdate=November 3, 2012|page=22}}</ref>

==Production==
''Space Ghost Coast to Coast'' was created by [[Mike Lazzo]] after he was asked to create a cartoon to appeal to adults.<ref>{{cite web|url=http://www.allmovie.com/movie/space-ghost-coast-to-coast-animated-tv-series-v309268|title=Space Ghost Coast to Coast [Animated TV Series&#93; (1994) - Trailers, Reviews, Synopsis, Showtimes and Cast |publisher=AllMovie |accessdate=2015-08-26}}</ref> The original name of the show stemmed from early 1993, while Andy Merrill and Jay Edwards were coming up with names for a marathon of the original ''Space Ghost'' TV show to air on [[Cartoon Network]], trying to find things that rhyme with "Ghost". The series premiered on April 15, 1994,<ref>{{cite web|url=http://snard.com/sg/shoot.html |title=Late Night with Space Ghost |publisher=Snard.com |date=April 15, 1994 |accessdate=2009-08-01}}</ref><ref>{{cite news |url=https://www.nytimes.com/1994/11/20/arts/television-and-now-here-s-ummm-space-ghost.html?scp=2&sq=cartoon+network&st=nyt |title=And Now, Here's . . . Ummm . . . Space Ghost |page=1 |publisher=The New York Times |date=November 20, 1994 |accessdate=2010-01-07 | first=Andy | last=Meisler}}</ref><ref>{{cite news |url=https://www.nytimes.com/1994/11/20/arts/television-and-now-here-s-ummm-space-ghost.html?pagewanted=2 |page=2 |title=And Now, Here's . . . Ummm . . . Space Ghost |publisher=The New York Times |date=November 20, 1994 |accessdate=2010-01-07 | first=Andy | last=Meisler}}</ref> having aired initially at 11:00 pm ET on Friday nights, with an encore showing of the episode on Saturday night. Later, the program was moved to various late-night time slots, having usually been on weekends.

In February 1995, an episode of ''Space Ghost Coast to Coast'' was simulcast on Cartoon Network, TBS, and [[Turner Network Television|TNT]] for the "World Premiere Toon-In" special debut of Cartoon Network's ''[[What a Cartoon!|World Premiere Toons]]'' series. In the special, Space Ghost interviewed a few of the new directors, while the Council of Doom were the judges of the cartoon clips.

On September 2, 2001, new episodes of the series, along with re-runs of the existing episodes, moved to [[Adult Swim]], a late night programming block, launched by Cartoon Network that night. The series ended its television run in 2004 with "Live at the Fillmore".

In 2006, the series returned as a five-minute [[web series]] on [[Turner Broadcasting]]'s [[GameTap]] online pay service in which Space Ghost interviewed celebrities from the video game industry and GameTap's artist of the month. The series officially ended with the final webisode on May 31, 2008.

On [[April Fools' Day]] 2014, Adult Swim had an unannounced ''Space Ghost Coast to Coast'' marathon with new material in the form of commercials featuring Space Ghost, Zorak and Moltar in a voice-recording booth ad-libbing lines from episodes.

===Cast===
{{Main article|List of Space Ghost Coast to Coast characters}}

[[File:GeorgeLowe-Painting crop.jpg|thumb|right|200px|George Lowe provides the voice of the lead role, Space Ghost.]]
*[[George Lowe]] - [[Space Ghost]], Announcer, Salesman, and various other characters
*[[C. Martin Croker]] - [[Zorak]], Moltar, and various other characters
*[[Andy Merrill]] - [[Brak (character)|Brak]], Lokar, and various other characters
*[[Don Kennedy]] - Tansit
*[[Judy Tenuta]] - Black Widow (also appeared as herself, as a guest)
*Scott Finnel - [[Harvey Birdman]]
*[[Dave Willis]] - various characters
*Brad Abrell - Chad Ghostal
*[[Bobcat Goldthwait]] - Himself

==Episodes==
{{main article|List of Space Ghost Coast to Coast episodes}}

==Home media==
{| class="wikitable"
|-
!DVD Name!!Release Date!!Ep #!!Additional Information
|-
| Volume One || November 18, 2003 || style="text-align:center;"|16 || This two disc boxset collected 16 episodes from the show's first three seasons, 1994 to 1996.
"Elevator", "Spanish Translation", "Gilligan", "CHiPs", "Bobcat", "Punch", "Banjo", "Batmantis", "Story Book House", "Girlie Show", "Hungry", "Fire Drill", "Sleeper", "Jerk", "Urges", and "Explode" and had 2 Easter eggs.
|-
| Volume Two || November 16, 2004 || style="text-align:center;"|14 || This two disc boxset collected 14 episodes from the third season, 1996.
"$20.01", "Lovesick", "Transcript", "Sharrock", "Boo", "Freak Show", "Switcheroo", "Surprise", "Glen Campbell", "Jacksonville", "Late Show", "Cookout", "Art Show", and "Woody Allen's Fall Project"
Special features included "Andy's Pilot", a performance by [[Thurston Moore]], the unedited version of [[Matt Groening]]'s interview from "Glen Campbell", [[pencil test (animation)|pencil test]] footage, bonus footage and Easter eggs, as well as commentary on every episode.
|-
| Volume Three: <br><br>This is 1997 || April 12, 2005 || style="text-align:center;"|24 || This two disc boxset collected all 24 episodes from the show's 1997 season, the fourth season, some of which were the originally aired extended versions.
"Rehearsal", "Gallagher", "Edelweiss", "Anniversary", "Zoltran", "Pilot", "Speck", "Zorak", "Switcheroo", "Mayonnaise", "Brilliant Number One", "Boo Boo Kitty", "Needledrop", "Sphinx", "Pavement", "Untitled", "Hipster", "Piledriver", "Suckup", "Dam", "Boatshow", "Telethon", "Dimethylpyrimidinol Bisulfite" and "Joshua". It also featured commentaries by cast members, new footage, deleted scenes, the 1995 [[World Premiere Toons|World Premiere Toon-In]], "President's Day Nightmare" (without any footage from the cartoons featured) and Easter eggs.
|-
| Volume Four: <br><br>The 1998 Episodes || December 7, 2007 || style="text-align:center;"|11 || This single-disc set collected all 11 episodes from the show's 1998 season, the fifth season, one of which was the originally aired extended version.
The fourth DVD was released exclusively through the Adult Swim website, and was titled "The 1998 Episodes" rather than "Volume Four".<ref>{{cite web|url=http://www.williamsstreet.com/cat/Space-Ghost-Coast-to-Coast/Space-Ghost-Coast-to-Coast-Volume-Four-The-1998-Episodes.html|archiveurl=//web.archive.org/web/20090605163331/http://www.williamsstreet.com/cat/Space-Ghost-Coast-to-Coast/Space-Ghost-Coast-to-Coast-Volume-Four-The-1998-Episodes.html|archivedate=June 5, 2009|title=Space Ghost Coast to Coast: The 1998 Episodes |publisher=Williamsstreet.com|accessdate=August 1, 2009}}</ref>  It included the episodes "Terminal", "Toast", "Lawsuit", "[[Cahill (Space Ghost Coast To Coast episode)|Cahill]]", "Warren" (36-minute cut), "Chinatown", "Rio Ghosto", "Pal Joey", "Curses", "Intense Patriotism" and "Waiting for Edward". It also featured an unfinished episode guest-starring Steven Wright titled "Dinner with Steven" and one Easter egg.
|-
| Volume Five: <br><br>From the Kentucky Nightmare DVD || September 11, 2008 || style="text-align:center;"|16 || This two disc boxset included all 14 episodes from 1999 to 2001, seasons six and seven, one of which was the originally aired extended version.
The fifth DVD was released exclusively through the Adult Swim website, and was titled "From the Kentucky Nightmare DVD" rather than "Volume Five". It included the episodes "Snatch", "Sequel", "Girl Hair", "Chambraigne", "Table Read", "King Dead", "Fire Ant" (22-minute cut), "Curling Flower Space", "[[Knifin' Around]]", "The Justice Hole", "Kentucky Nightmare", "Sweet for Brak", "Flipmode", "Mommentary". Extras included Snatch Alt Ending, Table Read Extra, Conan Raw Interview, [[George Lowe]] Record, [[C. Martin Croker|Clay Croker]] Record, Promos, Busta Raw Interview, Mommentary w/ Creators Commentary and Mommentary: Jelly Bean and 2 Easter eggs).
|}

The fourth and fifth volumes were only available for purchase through the now-defunct Adult Swim online store. The final six episodes of the television run have never had an official release on DVD. Nearly every episode was available to buy through a "build your own DVD" feature Adult Swim had on its website, thus the final season, early episodes that were left off of the first volume and unedited shows that had been altered on the official releases were now available to own. The shop was taken down in 2012 for unknown reasons.

In 2006, episodes were made available on the [[Xbox Live]] Marketplace.<ref>{{cite news|url=http://www.wired.com/gamelife/2006/11/xbox_hdtv_downl/|title=Xbox HDTV Downloads: Best Space Ghost Ever &#124; Game &#124; Life from Wired.com |publisher=Blog.wired.com|date=November 22, 2006|accessdate=August 1, 2009|first=Chris|last=Kohler}}</ref><ref>{{cite web|url=http://www.xbox.com/en-US/live/marketplace/moviestv/adultswim.htm |title=Xbox.com &#124; Movies & TV Shows&nbsp;— adult swim|archiveurl=//web.archive.org/web/20080212051741/http://www.xbox.com/en-US/live/marketplace/moviestv/adultswim.htm |archivedate=February 12, 2008}}</ref>

==Legacy==
In 1995, a [[Spin-off (media)|spin-off]] show called ''[[Cartoon Planet]]'' premiered on Cartoon Network as well as one of Cartoon Network's sister networks, [[TBS (TV network)|TBS]]. This show featured Space Ghost, Zorak, and Brak hosting a variety show on the Cartoon Planet. In 2000, the show either spun off or directly inspired<ref>{{cite web|url=http://www.amog.com/visual-history-adult-swim/ |title=From Space Ghost to Robot Chicken: A Visual History of Adult Swim|author=Albert Costill|publisher=AMOG|date=March 23, 2010|accessdate=April 16, 2016}}</ref> the four original cartoons that constituted Adult Swim's comedy block. ''[[Sealab 2021]]'', ''[[The Brak Show]]'', ''[[Harvey Birdman, Attorney at Law]]'', and ''[[Aqua Teen Hunger Force]]'' (also known by various [[List of alternative titles for Aqua Teen Hunger Force|alternative titles]]). All four shows originally used the same limited animation style as ''Space Ghost Coast to Coast''. ''The Brak Show'' included the characters Brak and Zorak, recurring characters on ''Space Ghost Coast to Coast''.

The original writers and staff of ''Space Ghost Coast to Coast'' now continue to make up the backbone of Adult Swim. Show creator [[Mike Lazzo]] currently serves as senior executive vice president of Adult Swim. Writer [[Dave Willis]] now continues to write and voice characters for ''Aqua Teen Hunger Force'', as well as characters for ''[[12 oz. Mouse]]'', ''[[Squidbillies]]'' and ''[[Perfect Hair Forever]]''. [[Matt Harrigan]] became the head writer of the popular MTV series ''[[Celebrity Deathmatch]]'' from 1998 to 2002, and had written and voiced characters on the shows ''12 oz. Mouse'' and ''Aqua Teen Hunger Force''. He also created the show ''[[Assy McGee]]''. Likewise, Matt Maiellaro currently writes and provides voices for several Adult Swim series including ''Aqua Teen Hunger Force'', ''Perfect Hair Forever'', ''Squidbillies'' and ''12 oz. Mouse'', which he created.

Various ''Space Ghost Coast to Coast'' clips and shorts have been made after the series final episode. On December 13, 2009, Adult Swim aired a new Space Ghost interview with [[Zoe Saldana]] to promote [[James Cameron]]'s ''[[Avatar (2009 film)|Avatar]]''. In 2010, Adult Swim aired a new Space Ghost interview with return guest [[Jack Black]]. It was created to promote Black's film ''[[Gulliver's Travels (2010 film)|Gulliver's Travels]]''. A short interviewing NBA star [[Steve Nash]] and promoting Vitamin Water was made available online.<ref>{{cite web|url=http://www.adultswim.com/promos/201005_vitaminwater/index.html |title=Adult Swim / Steve Nash Connects with Vitamin Water|publisher=Adultswim.com|accessdate=January 24, 2011}}</ref> In 2011, on April Fools' Day, Adult Swim aired ''[[The Room (film)|The Room]]'' again. Space Ghost interviewed [[Tommy Wiseau]] during commercial breaks. In 2012, Adult Swim aired a new Space Ghost interview with [[Will Ferrell]] and [[Zach Galifianakis]] to promote their new film ''[[The Campaign (film)|The Campaign]]''.

In a 2012 interview, [[Eric André]] mentioned being a big fan of the show, stating it was a major influence on him while developing his own series for Adult Swim, ''[[The Eric Andre Show]]''. Before shooting André would re-watch several episodes of ''Space Ghost Coast to Coast'' in a row in order to "absorb as much ''Space Ghost''" as he could. André would also ask executive producer and Adult Swim president [[Mike Lazzo]] several questions about the series, as he was an executive during its production run. To André's surprise Lazzo had no interest in ''Space Ghost Coast to Coast'', saying "Space Ghost is dead to me".<ref name="huffpoabc">{{cite web|last=Luippold|first=Ross|url=http://www.huffingtonpost.com/2012/05/09/eric-andre-talks-his-adult-swim-talk-show_n_1506832.html|title=Eric Andre Talks His New Adult Swim Show That ABC Isn't 'Thrilled' About|work=The Huffington Post|date=May 10, 2012|accessdate=September 7, 2012}}</ref>

''Space Ghost'' is mentioned in the opening lyrics of [[cello rock]] band [[Rasputina (band)|Rasputina]]'s song "The Olde Headboard", which was featured on their 1998 sophomore album ''[[How We Quit the Forest]]''.

Four rocks found on the planet [[Mars]] were named after Space Ghost, Zorak, Moltar, and Brak.<ref>{{cite journal|authorlink=K. Self|title=Technically Speaking|journal=Spectrum|volume=35|issue=2|page=17|publisher=IEEE |date=February 1998}}</ref><ref>{{cite web|url=http://mars.jpl.nasa.gov/MPF/imp/names/names.html|title=Named rocks on Mars|website=Mars.jpl.nasa.gov|accessdate=January 24, 2011}}</ref> In January 2009, IGN named ''Space Ghost Coast to Coast'' as their 37th favorite animated TV show in their Top 100 Best Animated TV Shows article.<ref>{{cite web|title=37, Space Ghost Coast to Coast|url=http://www.ign.com/top/animated-tv-series/37.html|publisher=IGN|accessdate=April 16, 2016|date=January 23, 2009}}</ref> In 2013 IGN placed ''Space Ghost Coast to Coast'' as number 18 on their list of Top 25 [[Adult animation|animated series for adults]].<ref name=Top25IGN>{{cite web|last=Fowler|first=Matt|title=The Top 25 Animated Series for Adults|url=http://www.ign.com/articles/2013/07/15/top-25-animated-shows-for-adults|publisher=IGN|date=July 15, 2013 |accessdate=July 23, 2013|archiveurl=//web.archive.org/web/20130722082915/http://www.ign.com/articles/2013/07/15/top-25-animated-shows-for-adults?page=2|archivedate=July 22, 2013|deadurl=no}}</ref>

Rapper [[SpaceGhostPurrp]] is named after the titular character, as well as lending his image for the cover of his ''NASA'' mixtape.

==See also==
{{Portal|Cartoon Network|United States|Animation}}
*[[List of Space Ghost Coast to Coast characters|List of ''Space Ghost Coast to Coast'' characters]]
*[[List of Space Ghost Coast to Coast episodes|List of ''Space Ghost Coast to Coast'' episodes]]
*[[Space Ghost]]
*''[[Cartoon Planet]]''
*''[[Space Ghost (TV series)]]''
*''[[The Brak Show]]''
*''[[Coast to Coast AM]]''

==References==
{{Reflist|2}}

==External links==
*{{Official website|http://www.adultswim.com/shows/space-ghost-coast-to-coast/}}
*[//web.archive.org/web/*/cartoonnetwork.com/spaceghost/index.html Space Ghost Coast to Coast] at [[Cartoon Network]] <small>(Archive)</small>
*{{IMDb title|0108937}}
*{{tv.com show|space-ghost-coast-to-coast|Space Ghost Coast to Coast}}

{{Navboxes|list=
{{Space Ghost Coast to Coast}}
{{Cartoon Network original programming}}
{{Adult Swim original programming}}
{{Aqua Teen Hunger Force}}
{{Williams Street}}
{{Harvey Birdman}}
}}

{{DEFAULTSORT:Space Ghost Coast To Coast}}
[[Category:Space Ghost Coast to Coast| ]]
[[Category:1994 American television series debuts]]
[[Category:1999 American television series endings]]
[[Category:2001 American television series debuts]]
[[Category:2004 American television series endings]]
[[Category:2006 web series debuts]]
[[Category:2008 web series endings]]
[[Category:1990s American animated television series]]
[[Category:2000s American animated television series]]
[[Category:Cartoons animated with Adobe After Effects]]
[[Category:TBS (U.S. TV channel) programs]]
[[Category:TNT (TV channel) shows]]
[[Category:American web series]]
[[Category:Television series with live action and animation]]
[[Category:American television talk shows]]
[[Category:Comic science fiction]]
[[Category:English-language television programming]]
[[Category:Flash cartoons]]
[[Category:Television series created by Mike Lazzo]]
[[Category:Space Ghost television series|Coast to Coast]]
[[Category:Television spin-offs]]
[[Category:Hanna-Barbera series and characters]]
[[Category:Television series revived after cancellation]]
[[Category:Web talk shows]]
[[Category:Television series by Williams Street]]
[[Category:Animated internet series]]
[[Category:Comedy web series]]
[[Category:Animated comedy television series]]