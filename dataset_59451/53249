{{Infobox NCAA team season
|Mode = football
|Year = 2006
|Team = North Carolina Tar Heels
|Image = University of North Carolina Tarheels Interlocking NC logo.svg
|ImageSize = 100
|Conference = Atlantic Coast Conference
|Division = Coastal
|ShortConference = ACC
|BCSRank =
|CoachRank = 
|APRank = 
|Record = 3&ndash;9
|ConfRecord = 2&ndash;6
|HeadCoach = [[John Bunting (coach)|John Bunting]]
|OffCoach = [[Frank Cignetti (quarterbacks coach)|Frank Cignetti]]
|DefCoach = [[Marvin Sanders]]
|OScheme = 
|DScheme = 
|StadiumArena = [[Kenan Memorial Stadium]]<br>(Capacity: 60,000)
|Champion = 
|BowlTourney = 
|BowlTourneyResult =
}}
{{2006 ACC football standings}}
The '''2006 North Carolina Tar Heels football team''' represented the [[University of North Carolina at Chapel Hill]] during the [[2006 NCAA Division I FBS football season]]. The Tar Heels  played their home games at [[Kenan Memorial Stadium]] in [[Chapel Hill, North Carolina]] competed in the [[Atlantic Coast Conference]], and was led by head coach [[John Bunting (coach)|John Bunting]]. The Tar Heels finished the season with a disappointing 3&ndash;9 record.

==Coaching staff==
The 2006 season was the last for [[John Bunting (coach)|John Bunting]] as [[head coach]]. He was to be replaced by [[Butch Davis]] in the postseason.<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/102206aaa.html Carolina To Make Football Coaching Change For 2007]." ''tarheelblue.com.'' Retrieved on February 6, 2008.</ref><ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/111306aaa.html Carolina Names Butch Davis New Football Coach]." ''tarheelblue.com.'' Retrieved on February 6, 2008.</ref>
{| class="wikitable"
! '''Name'''
! '''Position''' <ref>"[http://tarheelblue.cstv.com/sports/m-footbl/spec-rel/081406aae.html 2006 North Carolina Football Media Guide]." ''tarheelblue.com.'' Retrieved on February 6, 2008.</ref>
! '''Seasons in Position'''
|- align="center"
| [[John Bunting (coach)|John Bunting]]
| Head Coach
| 6th
|- align="center"
| Dave Brock
| Assistant Head Coach / Wide Receivers / Recruiting Coordinator
| 2nd
|- align="center" 
| Ken Browning
| Defensive Tackles
| 13th
|- align="center" 
|  [[Frank Cignetti (quarterbacks coach)|Frank Cignetti]]
| Offensive Coordinator / Quarterbacks
| 1st
|- align="center"
| Jeff Connors
| Strength and Conditioning Coordinator
| 6th
|- align="center"
| [[John Gutekunst]]
| Assistant Head Coach / Tight Ends
| 3rd
|- align="center"
| Danny Pearman
| Defensive Ends
| 1st
|- align="center"
| Andre' Powell
| Special Teams Coordinator / Running Backs
| 6th
|- align="center"
| [[Marvin Sanders]]
| Defensive Coordinator / Defensive Backs
| 3rd
|- align="center"
| Tommy Thigpen
| Linebackers
| 2nd
|- align="center"
| Mark Weber
| Offensive Line
| 1st
|- 
|}

==Schedule==
{{CFB Schedule Start|time=|rank=no|ranklink=|rankyear=2006|tv=|attend=yes}}
{{CFB Schedule Entry
| date         = September 2
| time         = 3:30 pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         =
| neutral      = 
| rank         = no
| opponent     = [[2006 Rutgers Scarlet Knights football team|Rutgers]]
| opprank      = 
| site_stadium = [[Kenan Memorial Stadium]]
| site_cityst  = [[Chapel Hill, North Carolina|Chapel Hill, NC]]
| gamename     = 
| tv           = [[ESPN on ABC|ABC]]
| score        = 16&ndash;21
| overtime     =
| attend       = 50,000
}}
{{CFB Schedule Entry
| date         = September 9
| time         = 12:00 pm
| w/l          = l
| nonconf      =
| homecoming   =
| away         = 
| neutral      = 
| rank         = no
| opponent     = [[2006 Virginia Tech Hokies football team|Virginia Tech]]
| opprank      = 16
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = 
| tv           = [[ESPN]]
| score        = 10&ndash;35
| overtime     =
| attend       = 57,000
}}
{{CFB Schedule Entry
| date         = September 16
| time         = 7:00 pm
| w/l          = w
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = 
| rank         = no
| opponent     = {{cfb link|year=2006|team=Furman Paladins|title=Furman}}
| opprank      = 
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = 
| tv           = [[ESPNU]]
| score        = 45&ndash;42
| overtime     =
| attend       = 47,000
}}
{{CFB Schedule Entry
| date         = September 23
| time         = 12:00 pm
| w/l          = l
| nonconf      =
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = no
| opponent     = [[2006 Clemson Tigers football team|Clemson]]
| opprank      = 19
| site_stadium = [[Memorial Stadium, Clemson|Memorial Stadium]]
| site_cityst  = [[Clemson, South Carolina|Clemson, SC]]
| gamename     = 
| tv           = [[ESPN3|ESPN360]]
| score        = 7&ndash;52
| overtime     =
| attend       = 81,886
}}
{{CFB Schedule Entry
| date         = October 7
| time         = 12:00 pm
| w/l          = l
| nonconf      =
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = no
| opponent     = [[2006 Miami Hurricanes football team|Miami]]
| opprank      = 
| site_stadium = [[Miami Orange Bowl]]
| site_cityst  = [[Miami|Miami, FL]]
| gamename     = 
| tv           = [[Raycom Sports|LFS]]
| score        = 7&ndash;27
| overtime     =
| attend       = 29,621
}}
{{CFB Schedule Entry
| date         = October 14 
| time         = 12:00 pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         = 
| neutral      = 
| rank         = no
| opponent     = [[2006 South Florida Bulls football team|South Florida]]
| opprank      = 
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = 
| tv           = ESPNU
| score        = 20&ndash;37
| overtime     =
| attend       = 44,000
}}
{{CFB Schedule Entry
| date         = October 19
| time         = 7:30 pm
| w/l          = l
| nonconf      =
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = no
| opponent     = [[2006 Virginia Cavaliers football team|Virginia]]
| opprank      = 
| site_stadium = [[Scott Stadium]]
| site_cityst  = [[Charlottesville, Virginia|Charlottesville, VA]]
| gamename     = [[South's Oldest Rivalry]]
| tv           = ESPN
| score        = 0&ndash;23
| overtime     =
| attend       = 56,632
}}
{{CFB Schedule Entry
| date         = October 28
| time         = 3:30 pm
| w/l          = l
| nonconf      =
| homecoming   = 
| away         = 
| neutral      = 
| rank         = no
| opponent     = [[2006 Wake Forest Demon Deacons football team|Wake Forest]]
| opprank      = 24
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = 
| tv           = ESPNU
| score        = 17&ndash;24
| overtime     =
| attend       = 49,000
}}
{{CFB Schedule Entry
| date         = November 4 
| time         = 2:30 pm
| w/l          = l
| nonconf      = yes
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = no
| opponent     = [[2006 Notre Dame Fighting Irish football team|Notre Dame]]
| opprank      = 11
| site_stadium = [[Notre Dame Stadium]]
| site_cityst  = [[Notre Dame, Indiana|Notre Dame, IN]]
| gamename     = 
| tv           = [[Notre Dame Football on NBC|NBC]]
| score        = 26&ndash;45
| overtime     =
| attend       = 80,795
}}
{{CFB Schedule Entry
| date         = November 11
| time         = 12:00 pm
| w/l          = l
| nonconf      =
| homecoming   = yes
| away         = 
| neutral      = 
| rank         = no
| opponent     = [[2006 Georgia Tech Yellow Jackets football team|Georgia Tech]]
| opprank      = 19
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = 
| tv           = LFS
| score        = 0&ndash;7
| overtime     =
| attend       = 41,000
}}
{{CFB Schedule Entry
| date         = November 18
| time         = 12:00 pm
| w/l          = w
| nonconf      =
| homecoming   = 
| away         = 
| neutral      = 
| rank         = no
| opponent     = [[2006 NC State Wolfpack football team|NC State]]
| opprank      = 
| site_stadium = Kenan Memorial Stadium
| site_cityst  = Chapel Hill, NC
| gamename     = [[North Carolina–NC State football rivalry|Carolina–State Game]]
| tv           = LFS
| score        = 23&ndash;9
| overtime     =
| attend       = 54,000
}}
{{CFB Schedule Entry
| date         = November 25
| time         = 12:00 pm
| w/l          = w
| nonconf      =
| homecoming   = 
| away         = yes
| neutral      = 
| rank         = no
| opponent     = [[2006 Duke Blue Devils football team|Duke]]
| opprank      = 
| site_stadium = [[Wallace Wade Stadium]]
| site_cityst  = [[Durham, North Carolina|Durham, NC]]
| gamename     = [[Victory Bell (Carolina-Duke)|Victory Bell Game]]
| tv           = ESPN360
| score        = 45&ndash;44
| overtime     =
| attend       = 24,478
}}
{{CFB Schedule End|rank=|poll=|timezone=[[Eastern Time Zone|Eastern Time]]}}

'''Did not play:''' [[2006 Boston College Eagles football team|Boston College]], [[2006 Florida State Seminoles football team|Florida State]], and [[2006 Maryland Terrapins football team|Maryland]].<ref>"[http://tarheelblue.cstv.com/sports/m-footbl/archive/unc-m-footbl-sched-2006.html 2006-2007 Schedule]." ''tarheelblue.com.'' Retrieved on February 6, 2008.</ref>

==Team Statistics==
{| class="wikitable"
! &nbsp; &nbsp; Passing Leaders  &nbsp; &nbsp;
! &nbsp; Cmp &nbsp;
! &nbsp; Att &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; TD &nbsp;
! &nbsp; Int &nbsp;
|-
| Joe Dailey
| align="center" | 112
| align="center" | 195
| align="center" | 1316
| align="center" | 7
| align="center" | 10
|-
| Cameron Sexton
| align="center" | 57
| align="center" | 136
| align="center" | 840
| align="center" | 4
| align="center" | 8
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Rushing Leaders &nbsp; &nbsp;
! &nbsp; Car &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; TD &nbsp;
|-
| Ronnie McGill
| align="center" | 192
| align="center" | 790
| align="center" | 48
| align="center" | 7
|-
| Barrington Edwards
| align="center" | 91
| align="center" | 330
| align="center" | 34
| align="center" | 2
|-
| Justin Warren
| align="center" | 7
| align="center" | 77
| align="center" | 23
| align="center" | 1
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Receiving Leaders &nbsp; &nbsp;
! &nbsp; Rec &nbsp;
! &nbsp; Yds &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; TD &nbsp;
|-
| Hakeem Nicks
| align="center" | 39
| align="center" | 660
| align="center" | 83
| align="center" | 4
|-
| Brooks Foster
| align="center" | 38
| align="center" | 486
| align="center" | 39
| align="center" | 2
|-
| Jesse Holley
| align="center" | 37
| align="center" | 466
| align="center" | 50
| align="center" | 2
|-
|}

{| class="wikitable"
! &nbsp; &nbsp; Kicking &nbsp; &nbsp;
! &nbsp; XPM &nbsp;
! &nbsp; XPA &nbsp;
! &nbsp; FGM &nbsp;
! &nbsp; FGM &nbsp;
! &nbsp; Long &nbsp;
! &nbsp; Pts &nbsp;
|-
| Connor Barth
| align="center" | 24
| align="center" | 24
| align="center" | 10
| align="center" | 10
| align="center" | 54
| align="center" | 54
|-
|}

==References==
{{Reflist}}

{{North Carolina Tar Heels football navbox}}

{{DEFAULTSORT:2006 North Carolina Tar Heels Football Team}}
[[Category:2006 Atlantic Coast Conference football season|North Carolina Tar Heels]]
[[Category:North Carolina Tar Heels football seasons]]
[[Category:2006 in North Carolina|Tar Heels]]