<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{{lowercase|de Havilland Hound}}
{|{{Infobox Aircraft Begin
 |name = DH.65 Hound 
 |image = dHHound.jpg
 |caption =The modified DH.65A 
}}{{Infobox Aircraft Type
 |type = Day Bomber
 |manufacturer = [[de Havilland]]
 |designer =  
 |first flight = 17 November 1926
 |introduced = 1928
 |retired = 
 |produced = 
 |number built = 1 
 |status = 
 |unit cost =
 |primary user = [[Royal Air Force]]
 |more users = 
 |developed from = 
 |variants with their own articles = 
}}
|}

The '''de Havilland DH.65 Hound''' was a 1920s [[United Kingdom|British]] two-seat day bomber built by [[de Havilland]] at [[Stag Lane Aerodrome]].

==History==
The '''Hound''' was designed as a two-seat general purpose biplane, a private venture to meet [[List of Air Ministry Specifications|Air Ministry Specification 12/26]]. The prototype ''G-EBNJ'' first flew on 17 November 1926.<ref name ="mason bomber">{{Cite book |author=Mason, Francis K |authorlink =|title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books|location=London |year=1994 |isbn= 0-85177-861-5}}</ref> It was of all-wooden construction, powered by a [[Napier Lion]] engine. In 1927, the nose and rudder were modified, it was fitted with a geared engine and received the modified designation '''DH.65A'''. It was delivered to the [[Royal Air Force]] in January 1928 receiving serial number ''J9127'' for evaluation. While it showed superior performance to the other competitors for the specification, it was rejected because of its wooden construction and the order was placed with the [[Hawker Hart]].<ref name ="mason bomber"/>

Despite its rejection by the RAF, on 26 April 1928 the aircraft set a world record for carrying a load of 2,205&nbsp;lb (1,000&nbsp;kg) over 62&nbsp;mi (100&nbsp;km) at 160&nbsp;mph (257&nbsp;km/h) piloted by [[Hubert Broad|H.S. Broad]].<ref name="jacksonv2">{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 2|year= 1973|publisher= Putnam|location= London|isbn=0-370-10010-7 }}</ref>

A project to further develop the Hound as a four-seat passenger transport under the designation '''DH.74''' was left unrealised.<ref name ="mason bomber"/>

The design being otherwise unsuccessful, the second aircraft ''G-EBNK'' was not completed.<ref name ="mason bomber"/>

==Operators==
;{{UK}}
*[[Royal Air Force]]

==Specifications (DH.65A)==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|ref=British Civil Aircraft since 1919 Volume 2<ref name="jacksonv2"/>
|plane or copter?=plane
|jet or prop?=prop
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|crew=three
|length main= 31 ft 0 in 
|length alt= 9.45 m 
|span main= 45 ft 0 in
|span alt= 13.72m
|height main= 11 ft 6 in
|height alt= 3.51 m
|area main= 462 ft²
|area alt= 42.9 m²
|empty weight main= 2,981 lb
|empty weight alt= 1,355 kg 
|loaded weight main= 
|loaded weight alt= 
|max takeoff weight main= 4,934 lb
|max takeoff weight alt= 2,243 kg
|engine (prop)=[[Napier Lion XI]] 
|type of prop= W-12 water-cooled inline piston engine
|number of props=1
|power main= 550 hp
|power alt= 410 kW
|max speed main= 133 kn
|max speed alt= 153 mph, 246 km/h
|range main= 870 nmi
|range alt= 1,000 mi, 1,610 km
|ceiling main= 25,500 ft
|ceiling alt= 7,770 m
|climb rate main= 1,490 ft/min 
|climb rate alt=7.57 m/s
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament=''Provision for'': <br />
*1 × forward firing .303 in (7.7 mm) [[Vickers machine gun]] and 1 × .303 in (7.7 mm) [[Lewis gun]] on [[Scarff ring]] in rear cockpit
*Up to 2 × 230 lb (104 kg) bombs carried under wings
}}

==See also==
{{aircontent
|related=
|similar aircraft=
|lists=
*[[List of aircraft of the RAF]]
|see also=
}}

==References==
{{reflist}}

==Bibliography==
*{{cite book |last= Jackson|first= A.J.|authorlink= |coauthors= |title= British Civil Aircraft since 1919 Volume 2|year= 1973|publisher= Putnam|location= London|isbn=0-370-10010-7 }}
*{{Cite book |author=Mason, Francis K |authorlink =|title=The British Bomber since 1914 |publisher=Putnam Aeronautical Books|location=London |year=1994 |isbn= 0-85177-861-5}}

{{commons category|De Havilland DH.65 Hound}}

{{de Havilland aircraft}}

[[Category:Biplanes]]
[[Category:Single-engined tractor aircraft]]
[[Category:British bomber aircraft 1920–1929]]
[[Category:De Havilland aircraft|Hound]]