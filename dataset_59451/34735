{{Good article}}
{{Infobox military unit
|unit_name=172d Infantry Brigade
|image= [[File:172nd Infantry Brigade SSI.svg|160px]]
|caption=172d Infantry Brigade shoulder sleeve insignia
|nickname="Blackhawk Brigade" <br/> formerly ''Snow Hawks'' ([[Special Designation]])<ref name = SUD>{{cite web| title = Special Unit Designations| url = http://www.history.army.mil/html/forcestruc/spdes-123-ra_ar.html| publisher=[[United States Army Center of Military History]]| date = 21 April 2010| accessdate =12 July 2010| archiveurl= https://web.archive.org/web/20100709200756/http://www.history.army.mil/html/forcestruc/spdes-123-ra_ar.html| archivedate= 9 July 2010 <!--Added by DASHBot-->}}</ref>
|motto=''Caveat'' – "Let Him Beware"
|colors=Black and Bronze
|march=
|ceremonial_chief=
|type=[[Infantry]] [[Brigade]]
|branch=[[United States Army|Army]]
|dates= 5 August 1917 – 15 April 1986<br/>17 April 1998 – 15 December 2006<br/>17 March 2008 – 31 May 2013
|country={{USA}}
|allegiance=[[Active duty]]
|command_structure=[[V Corps (United States)|V Corps]]
|size= [[Brigade]]
|specialization=[[Mechanized infantry|Mechanized]] [[Infantry]]
|last_commander=COL Edward T. Bohnemann
|garrison=[[Grafenwöhr]], Germany
|battles='''[[World War II]]'''<br/>'''[[Operation Iraqi Freedom]]'''<br/>'''[[Operation Enduring Freedom]]'''
|notable_commanders=
|anniversaries=
|identification_symbol_2=[[File:172InfantryBdeCSIB.jpg|100px]]
|identification_symbol_2_label=[[Combat Service Identification Badge]]
}}

The '''172nd Infantry Brigade''' was an [[infantry]] [[brigade]] of the [[United States Army]] headquartered at [[Grafenwöhr]], Germany. An [[active duty]] separate brigade, it was part of [[V Corps (United States)|V Corps]] and was one of five active-duty, separate, combat brigades in the U.S. Army before its most recent inactivation on 31 May 2013.

First activated in 1917, the brigade was deployed to France during [[World War I]] and used to reinforce front-line units. The brigade's actions in France during that time are not completely clear. It would later be converted to a reconnaissance unit that was deployed during [[World War II]] and saw several months of combat in the European Theater. The brigade has multiple tours of duty in [[Operation Iraqi Freedom]] from 2005 until 2006 and from 2008 until 2010 and in [[Operation Enduring Freedom]] from 2011 until 2012. Its infamous 16-month deployment was one of the longest deployments for a unit serving in the OIF campaign. Most recently the brigade served a 12-month tour in Afghanistan from 2011 until 2012.

The unit has been activated and inactivated numerous times, and has also seen several redesignations. The 172nd was one of the first [[brigade combat team]]s before it was deactivated in 2006. Reactivated in 2008 from another reflagged unit, it immediately prepared for another tour of duty in [[Iraq]]. Following a series of budget cuts and force structure reductions, the unit formally inactivated on 31 May 2013 in Grafenwöhr, Germany.

==Organization==
[[File:172nd US Infantry Brigade.png|thumb|left|150px|Structure 172d Infantry Brigade]]
The brigade was a separate unit and did not report to a higher [[division (military)|division]]-level headquarters, but instead reported directly to the [[V Corps (United States)|V Corps]] of [[United States Army Europe]]. It consisted of six [[battalion]]s and four [[companies]].<ref name="blackorg">[http://www.2bct.1id.army.mil/Primary%20Sites/index.htm 172nd Infantry "Blackhawk Brigade": Blackhawk Organization]. 172d Infantry Brigade Staff. Retrieved 27 June 2008.</ref> These components included two [[infantry]] battalions, one [[armor]] battalion, one [[field artillery]] battalion, a [[combat engineer]] battalion and a support services battalion. It also contained a headquarters company, a Signal company, a [[military intelligence]] company, and an armored [[cavalry]] company for reconnaissance.<ref name="blackorg"/>

The [[Headquarters and Headquarters Company]] of the unit was located at [[Grafenwöhr]], Germany.<ref name="172infam">[http://www.172infantry.army.mil/#history 172nd Infantry.army.mil], 172d Infantry Brigade Staff. Retrieved 27 June 2008.</ref> The Unit also contained the 1st Battalion, 2d Infantry,<ref>[http://www.1-18in.2bct.1id.army.mil/ 1–2 Infantry Homepage], 1–2 Infantry Staff. Retrieved 27 June 2008.</ref> the 2nd Battalion, 28th Infantry,<ref>[http://www.1-26in.2bct.1id.army.mil/ 2–28 Infantry Homepage], 2–28 Infantry Staff. Retrieved 27 June 2008.</ref> the [[77th Field Artillery Regiment (United States)|1st Battalion, 77th Field Artillery]],<ref>[http://www.schweinfurt.army.mil/sites/17/ 1–77 Field Artillery Homepage], 1–77 Field Artillery Staff. Retrieved 27 June 2008.</ref> the [[9th Engineer Battalion]],<ref>[http://www.9en.2bct.1id.army.mil/index.htm 9th Engineer Battalion Homepage], 9th Engineer Battalion Staff. Retrieved 27 June 2008</ref> the 3rd Battalion, [[66th Armor Regiment (United States)|66th Armor]],<ref>[http://www.1-77ar.2bct.1id.army.mil/ 3–66 Armor Homepage], 3–66 Armor Staff. Retrieved 27 June 2008.</ref> and the 172nd Forward Support Battalion.<ref>[https://portal.eur.army.mil/sites/172ndSB/default.aspx 172nd Support Battalion Homepage], 172nd Support Battalion Staff. Retrieved 27 June 2008.</ref> In addition, the brigade contained three independent companies; 504th Military Intelligence Company,<ref>[http://www.172infantry.army.mil/publications/BlackhawkVoice/BlackhawkVoice_Vol01-Iss01_2008-12.pdf 172D IN BDE Newsletter], 172ND IN BDE Staff. Retrieved 11 June 2009. [http://www.webcitation.org/5hXJe4EfO Archived] 14 June 2009.</ref> and Echo Troop, [[5th Cavalry Regiment]],<ref name="blackorg"/> the [[57th Signal Company (United States)|57th Signal Company]].<ref>[http://www.57sig.2bct.1id.army.mil/ 57th Signal Company Homepage], [[57th Signal Company (United States)|57th Signal Company]] Staff. Retrieved 27 June 2008.</ref> All of these subordinate units were last located in Grafenwöhr.<ref name="standto">[http://www4.army.mil/news/standto.php?dte=17 March 2008 ''Today's Focus'' 17 Mar 2008 Edition], ''Stand To!'' Magazine. Retrieved 27 June 2008.</ref>

==History==

===World War I===
[[File:172InfantryBdeDUI.PNG|thumb|right|172nd Infantry Brigade distinctive unit insignia.]]
The 172nd Infantry Brigade (Separate), officially titled the "172d Infantry Brigade",<ref name="bdehist"/> was first constituted on 5 August 1917 in the [[National Army (USA)|National Army]] as the 172nd Infantry Brigade.<ref name="bdehist">[http://www.2bct.1id.army.mil/Primary%20Sites/index.htm 172nd Blackhawk Brigade History], United States Army. Retrieved 7 June 2008.</ref><ref name="stripes"/> It was organized on the 25th of that month at [[Camp Grant (Illinois)|Camp Grant]], in [[Rockford, Illinois]] and assigned to the [[U.S. 86th Infantry Division|86th Infantry Division]].<ref name="Little">Little, John G., Jr. ''The Official History of the 86th Division''. Chicago: States Publications Society, 1921.</ref>  The brigade was assigned to the 86th Division and deployed to Europe for duty during [[World War I]]. It arrived in [[Bordeaux]], France, in September 1918<ref name="bdehist"/> The combat record of the unit during its World War I service is not clear, but it is known that the 86th Division was depleted when much of its force was used to reinforce other units already on the front lines.<ref name="globalsecurity">[http://www.globalsecurity.org/military/agency/army/172inbde.htm 172nd Infantry Brigade (Separate) "Snow Hawks"], GlobalSecurity.org. Retrieved 28 June 2008.</ref> Thus, the brigade received a World War I [[campaign streamer]] without an inscription, as it was not known to have fought in any engagements.<ref name="globalsecurity"/> After a cease fire was signed in 1918, the Brigade returned to the United States. It was demobilized in January 1919 at Camp Grant, and the camp itself was abandoned in 1921.<ref name="bdehist"/><ref name="Little"/>

On 24 June 1921 the unit was reconstituted in the [[United States Army Reserve|Organized Reserves]] as Headquarters and Headquarters Company, 172nd Infantry Brigade,<ref name="bdehist"/> and again assigned to the 86th Division.<ref name="Little"/>  It was organized in January 1922 at [[Springfield, Illinois]] and went through several redesignations, including Headquarters and Headquarters Company, 172nd Brigade,<ref name="bdehist"/> on 23 March 1925 and Headquarters and Headquarters Company, 172nd Infantry Brigade<ref name="bdehist"/> on 24 August 1936.

===World War II===

The Headquarters and Headquarters Company, 172nd Infantry Brigade, was converted and redesignated the 3rd [[Platoon]], [[86th Reconnaissance Troop]], and assigned to the [[86th Infantry Division (United States)|86th Infantry Division]] on 31 March 1942,<ref name="bdehist"/> while the Headquarters and Headquarters Company, [[171st Infantry Brigade]], became the remainder of the 86th Reconnaissance Troop.<ref name="bdehist"/>  On 15 December 1942 the troop was mobilized and reorganized at [[Camp Howze, Texas|Camp Howze]], in [[Gainesville, Texas]], as the [[86th Cavalry Reconnaissance Troop]], only to be reorganized and redesignated again on 5 August 1943 as the ''86th Reconnaissance Troop, Mechanized''.<ref name="bdehist"/><ref name="globalsecurity"/> For the majority of the US involvement in [[World War II]] it remained stateside, participating in the [[Third United States Army|Third Army]] #5 Louisiana Maneuvers in 1943, among other exercises, until finally staging at [[Camp Myles Standish]], at [[Boston, Massachusetts]] on 5 February 1945 and shipping out from [[Boston]] on 19 February 1945.<ref name="bdehist"/><ref name="globalsecurity"/>

The 86th Reconnaissance Troop arrived in France on 1 March 1945, acclimated and trained, and then moved to [[Cologne|Köln]], Germany, and participated in the relief of the [[U.S. 8th Infantry Division|8th Infantry Division]] in defensive positions near Weiden which is now part of [[Lindenthal (Cologne district)|Lindenthal]] on 28–29 March 1945. During its few months of combat duty in Europe, the troop participated in [[amphibious assault]]s across was [[Danube]], [[Bigge (river)|Bigge]], [[Altmuhl]], [[Isar]], [[Inn River|Inn]], [[Mittel-Isar]] and [[Salzach river]]s in Germany and Austria.<ref name="globalsecurity"/> It was assigned to [[First United States Army|First]], [[Third United States Army|Third]], [[Seventh United States Army|Seventh]], and [[Fifteenth United States Army|Fifteenth]] US Armies.<ref name="globalsecurity"/> The unit was at [[Salzburg]] on 7 May 1945 ([[V-E Day]]).<ref name="bdehist"/>  It was then sent back stateside to prepare for operation in the Pacific. Arriving back in New York City on 17 June 1945, the unit proceeded to [[Fort Gruber]] in [[Braggs, Oklahoma]] before staging at [[Camp Stoneman]] in [[Pittsburg, California]] on 14 August 1945.<ref name="bdehist"/>
The unit shipped out from [[San Francisco]] on 21 August 1945 and arrived in the Philippines on 7 September 1945, five days after the Japanese surrender.<ref name="bdehist"/>

===The Cold War===
On 10 October 1945 the 86th Reconnaissance Troop (Mechanized) was again redesignated the ''86th Mechanized Reconnaissance Troop'' before finally being inactivated on 30 December 1946 while still stationed in the Philippines.<ref name="bdehist"/>  However the 86th Mechanized Reconnaissance Troop was reactivated again on 9 July 1952 as part of the Army Reserve.<ref name="bdehist"/> It continued serving within the Army Reserve for some years. Activation of the brigade with its new structure took place on 1 July 1963 at [[Fort Richardson (Alaska)|Fort Richardson]], Alaska.<ref name="McBrig238">McGrath, p. 238.</ref>  The Army set up an experimental Airborne Company "Company F(FoxTrot) Airborne in 1962 in Fort Richardson.  The Company Commander was Capt Lawrence.  Under the new structure, the unit was renamed Company C Airborne. The Airborne Company was used to see how best to use Airborne Troops in Arctic conditions throughout the vast area of Alaska.

The new structure included one [[Light Infantry]] [[Battalion]]; one [[Mechanized Infantry]] Battalion; and one [[Tank]] Company.<ref>Paternostro, Anthony. "The Alaska Brigade: Arctic Intelligence and Some Strategic Considerations." ''Military Intelligence'' 6 (October–December 1980):47–50.</ref>  Its [[shoulder sleeve insignia]] was authorized for use on 28 August 1963<ref name="TIOH">[http://www.172infantry.army.mil/Info/History/FactFile_History-172Infantry_2008-05.pdf US Army official page 172nd Infantry Brigade], The US Army. Retrieved 15 March 2013.</ref> and its [[distinctive unit insignia]] was authorized on 8 June 1966.<ref name="TIOH" /><ref>Meiners, Theodore J. "They Climb the Crags." ''Army Digest'' 22 (April 1967):36–38.</ref>  The Brigade was reorganized from Mechanized Infantry to Light Infantry on 30 June 1969, with a reduction to two mechanized infantry [[battalion]]s.<ref>Bender, John A. "Dynamic Training Arctic Style: A Report from Alaska." ''Infantry'' 62 (November–December 1972):36–37.</ref>  In 1974 the 172nd Infantry Brigade was reorganized again to include three light infantry battalions,<ref>Boatner, James G. "Rugged Training on the 'Last Frontier.' Supersoldiers of the North." ''Army'' 26 (November 1976):27–30.</ref>

US Army Alaska was known as USARAL through the 60s and 70s, whereas after the activation of the 6th Infantry Division it was known as USARAK.
The two Arctic brigades, the 171st (4-9th Infantry, 1-47th Infantry [which was subsequently deactivated], and other components at Fort Wainwright) and the 172d (4-23d Infantry, 1-60th Infantry, 1-37th Artillery, 561st Combat Engineer Company, and other components at Fort Richardson) were consolidated in 1973 with the drawdown after Viet Nam. There was an administrative split between the "LIB" (Light Infantry Brigade) and the "Brigade Alaska", with the 1-43d Air Defense, 222d Aviation, 56th MP Company, 23d Construction Engineer Company, Northern Warfare Training Center- then at Fort Greely,' being assigned.

It was again reorganized in 1978  to a structure that included one infantry battalion,<ref>Kiernan, David R. "Winter Training in Alaska." ''Infantry'' 70 (November–December 1980):10–12.</ref> one mechanized infantry battalion, and one tank battalion<ref>Simone, Michael R. "Where 'Teary Eyes Freeze Shut."' ''Army'' 31 (February 1981):32–33.</ref> The brigade was again inactivated on 15 April 1986 at Fort Richardson, Alaska,<ref name="McBrig238"/> being reflagged as part of the newly reformed [[6th Infantry Division (United States)|6th Infantry Division]].<ref name="bdehist"/>

===Transformation===
In the late 1990s, Army leaders including [[General (United States)|General]] [[Eric Shinseki]] began shifting the Army force toward brigade centered operations. All separate brigades had been deactivated in the 1990s as part of the US Army's drawdown following the end of the [[Cold War]].<ref name="McBrigxi">McGrath, p. xi.</ref> These inactivations, along with subsequent reorganization of US Army divisions, saw several divisional brigades stationed in bases that were far from the division's headquarters and support units. These brigades had difficulty operating without support from higher headquarters.<ref name="McBrigxi"/>

It was Shinseki's idea to reactivate a few separate brigades and assign them their own support and sustainment units, which would allow them to function independently of division-level headquarters. These formations were termed "[[Brigade Combat Team]]s".<ref>McGrath, p. xii.</ref> Such units could be stationed in bases far from major commands, not requiring division-level unit support, an advantage in places like Alaska and Europe, where stationing entire divisions was unnecessary or impractical. The first of the separate brigades was to be the 172nd Infantry Brigade. On 17 April 1998, the U.S. Army reactivated the "172nd Infantry Brigade (Separate)" and reflagged the 1st Brigade, [[6th Infantry Division (United States)|6th Infantry Division]]<ref name="McBrigxi"/> as that unit was headquartered at [[Fort Wainwright]], Alaska.<ref name="bdehist"/> Two years later, the [[173rd Airborne Brigade]] was reactivated on 12 June 2000 at [[Caserma Ederle]] in Vicenza, Italy>.<ref name="McBrigxi"/> The 172nd was given an [[airborne forces|airborne]] infantry battalion, one of only three existing outside of the [[82nd Airborne Division]]<ref name="McBrig90">McGrath, p. 90.</ref> (the other two battalions were part of the [[2nd Infantry Division (United States)|2nd Infantry Division]] based in Korea).<ref name="McBrig90"/> The 172nd Infantry was designed as a "pacific theater contingency brigade". Located in Alaska, the 172nd would be able to deploy to any contingencies in Alaska, Europe (over the north pole) or the Pacific.<ref>McGrath, p. 110.</ref>

In July 2001 the [[United States Army|US Army]] announced that the 172nd Infantry Brigade was to become one of the Army's new Interim Brigade Combat Teams, later to be known as [[Stryker|Stryker Brigade Combat Team]]s (SBCTs).<ref name="defenselink">
{{cite news
| url=http://www.defenselink.mil/news/NewsArticle.aspx?ID=2428
| title=172nd Stryker Brigade Legacy to Live on as Unit ‘Reflags,’ Gets New Commanders
| publisher=[[American Forces Press Service]]
| author=[[Donna Miles]]
| date=28 June 2008
| accessdate=23 November 2008
| quote=
| archiveurl= https://web.archive.org/web/20081113224543/http://www.defenselink.mil/news/NewsArticle.aspx?ID=2428| archivedate= 13 November 2008 <!--Added by DASHBot-->}}
</ref>
Changes to the brigade included the addition of some 300 [[Stryker]] vehicles, and several [[Unmanned Aerial Vehicle]]s.<ref>[http://www.epa.gov/EPA-IMPACT/2002/March/Day-04/i5085.htm Preparation of an Environmental Impact Statement (EIS) for Force Transformation of the 172nd Infantry Brigade (Separate) and Mission Sustainment in Alaska], Federal Register Environmental Documents, [[US Environmental Protection Agency]]. Retrieved 28 June 2008.</ref> The transformation was intended to increase the brigade's mobility in operations as well as reduce its logistical footprint.<ref>[http://www.cemml.colostate.edu/AlaskaEIS/ Transformation EIS], Calvin Bagley, [[Colorado State University]]. Retrieved 13 August 2008.</ref> The project entailed around $1.2&nbsp;billion in construction costs for training facilities, motor pools, and other buildings.<ref name="ABM">[http://goliath.ecnext.com/coms2/gi_0199-816910/A-stryking-endeavor-preparation-for.html A Stryking endeavour: preparation for third Stryker brigade underway in Alaska], ''Alaska Business Monthly''. Retrieved 13 August 2008.</ref> This transformation was completed when the unit was formally redesignated on 16 October 2003.<ref name="stripes"/><ref>[http://www.afcea.org/signal/articles/templates/SIGNAL_Article_Template.asp?articleid=1214&zoneid=30 Pacific Army Forces Push Readiness] Robert K. Ackerman, ''Signal Online [[AFCEA]] Magazine''. Retrieved 14 August 2008.</ref> After the transformation was complete, the 172nd became the third Stryker brigade in the US Army, with a force of 3,500 soldiers.<ref name="ABM"/> In 2005, the new Brigade Commander changed the motto of the infantry brigade from "Snow Hawks" to "Arctic Wolves".<ref name="stripes"/> In early 2005, the brigade was alerted that it would be deployed to [[Operation Iraqi Freedom]] for the first time. To prepare, it participated in several large exercises at the [[Fort Polk#JRTC moves to Polk|Joint Readiness Training Center]] at [[Fort Polk]], [[Louisiana]]. The [[220th Military Police Brigade]], a reserve unit, provided additional soldiers to assist the brigade in the exercises during their final preparations for deployment.<ref>[http://www.wood.army.mil/mpbulletin/pdfs/Oct%2005/Arnold.pdf Military Police Support for the 172nd Stryker Brigade Combat Team], Robert Arnold, Jr.. Retrieved 13 August 2008.</ref>

===Operation Iraqi Freedom===
[[File:CSA-2006-09-22-092516.jpg|thumb|Soldiers from the 172nd Stryker Brigade Combat Team prepare to cross an open field during a cordon and search mission in Ur, Iraq in 2006.]]
In August 2005, the 172nd Infantry Brigade deployed to [[Iraq]] in support of [[Iraq War|Operation Iraqi Freedom]]. The unit deployed to [[Mosul]], Iraq. 4-14 CAV and a Stryker infantry company (A/4-23 IN and later, B/2-1 IN) were attached to 2nd Marine Expeditionary Force, and stationed at COP Rawah; away from the rest of the BDE.  Duties of the unit during deployment included numerous patrol operations, searches for weapons caches, and counterinsurgency operations.<ref>[http://www4.army.mil/armyimages/armyimage.php?photo=11489 Army.mil Featured Image], United States Army Homepage. Retrieved 10 August 2008.</ref> Its tour was to have ended on 27 July 2006, but the U.S. Army unexpectedly extended the deployment until the end of November 2006. During the extension, the unit was sent to [[Baghdad]] to quell growing sectarian violence concerns.<ref name="defenselink"/> The infamous extension of the deployment had happened after some of the units of the Brigade were already touched down at their home base of Fort Wainwright, AK, forcing them to fly back to staging areas in Iraq.<ref name="stripes"/> The extension occurred after the unit's regular 12-month tour was complete, making the deployment last for a total of 16 months. As a result of the unit's action in Iraq, the brigade was awarded the [[Valorous Unit Award]].<ref>[http://www.army.mil/-newsreleases/2008/05/19/9262-army-prepares-for-fall-2008-active-duty-rotations-in-iraq/ Army Prepares for Fall 2008 Active-duty Rotations in Iraq], United States Army. Retrieved 28 June 2008.</ref>

During this action, 26 soldiers of the brigade were [[killed in action]], and another 350 were wounded.<ref name="defenselink2">
{{cite news
| url=http://www.defenselink.mil/News/NewsArticle.aspx?id=2402
| title=Stryker Brigade Ceremony Focuses on Accomplishments, Sacrifices
| publisher=[[American Forces Press Service]]
| author=[[Donna Miles]]
| accessdate=28 June 2008
| quote=
| archiveurl= https://web.archive.org/web/20080616091736/http://www.defenselink.mil/news/NewsArticle.aspx?ID=2402| archivedate= 16 June 2008 <!--Added by DASHBot-->}}
</ref>
Ten additional soldiers in units attached to the brigade were killed.<ref>
{{cite news
| url=http://www.defenselink.mil/News/NewsArticle.aspx?ID=2407&42407=20061213
| title=‘Arctic Wolves’ Dedicate Wall Honoring Fallen Comrades
| publisher=[[American Forces Press Service]]
| author=[[Donna Miles]]
| accessdate=28 June 2008
| quote=
| archiveurl= https://web.archive.org/web/20080616091703/http://www.defenselink.mil/News/NewsArticle.aspx?ID=2407&42407=20061213| archivedate= 16 June 2008 <!--Added by DASHBot-->}}
</ref>

===Inactivation 2006===
Having returned from its extended tour in Baghdad, Iraq, the 172nd Stryker Brigade Combat Team was officially deactivated and the [[1st Brigade, 25th Infantry Division (United States)|1st Stryker Brigade Combat Team, 25th Infantry Division]] was activated in its place on 14 December 2006.<ref>[http://www.army.mil/-links/2006/12/19/1032-172nd-reflagged/ 172nd Reflagged], SRTV, United States Army. Retrieved 27 June 2008.</ref> <br/> The brigade's six battalions and four separate companies were likewise reflagged as part of the change.<ref name="25thID">[http://www.wainwright.army.mil/1_25_SBCT/units.htm 1st Brigade, 25th Infantry Division Homepage: Units]. 25th Infantry Division Staff. Retrieved 27 June 2008.</ref> The reflagged units were:
* 1st Battalion, [[17th Infantry Regiment (United States)|17th Infantry Regiment]] to 1st Battalion, [[5th Infantry Regiment (United States)|5th Infantry Regiment]].<ref name="25thID"/>
* 2nd Battalion, [[1st Infantry Regiment (United States)|1st Infantry Regiment]] to 1st Battalion, [[24th Infantry Regiment]].<ref name="25thID"/>
* 4th Battalion, [[23rd Infantry Regiment (United States)|23rd Infantry Regiment]] to 3rd Battalion, [[21st Infantry Regiment (United States)|21st Infantry Regiment]].<ref name="25thID"/>
* 4th Squadron, [[14th Cavalry Regiment]] to 5th Squadron, [[1st Cavalry Regiment (United States)|1st Cavalry Regiment]].<ref name="25thID"/>
* 4th Battalion, [[11th Field Artillery Regiment (United States)|11th Field Artillery Regiment]] to 2nd Battalion, [[8th Field Artillery Regiment (United States)|8th Field Artillery Regiment]].<ref name="25thID"/>
* [[172nd Brigade Support Battalion]] to [[25th Brigade Support Battalion]].<ref name="25thID"/>
* A Company, 52nd Infantry Regiment to D Company, [[52nd Infantry Regiment]].<ref name="25thID"/>
* 572nd Military Intelligence Company to 184th Military Intelligence Company.<ref name="25thID"/>
* 562nd Engineer Company to 73rd Engineer Company.<ref name="25thID"/>
* [[21st Signal Company]] to [[176th Signal Company]].<ref name="25thID"/>

===Reactivation in Germany===
[[File:Army mil-2008-03-19-115842.jpg|thumb|right|200px|Formal reactivation ceremony]]
As part of the [[Grow the Army]] Plan announced 19 December 2007, the Army will activate and retain two Infantry Brigades in Germany until 2012 and 2013.<ref name="standto"/> On 6 March 2008, it was announced that the 172nd Infantry Brigade would be activated as the first of these brigades, with the other being the [[170th Infantry Brigade (United States)|170th Infantry Brigade]].<ref name="USAREUR"/> On 17 March, the 172nd Infantry Brigade was formally activated in Schweinfurt, Germany by reflagging the [[1st Infantry Division (United States)|1st Infantry Division]]'s [[2nd (Dagger) Brigade]], which relocated to Ft. Riley, KS.<ref name="stripes">[http://www.stripes.com/article.asp?section=104&article=60777&archive=true Big Red One relocating to Grafenwöhr with new name], Matt Millham, ''Stars and Stripes''. Retrieved 28 June 2008.</ref> Colonel [[Jeffrey Allen Sinclair|Jeffrey Sinclair]] was commanding the brigade at the time. The 172nd Infantry Brigade relocated to Grafenwöhr, Germany, The unit it was activated using the assets of the 2nd Brigade, 1st Infantry Division, which had recently completed its own tour of duty in Iraq. The 172nd Infantry Brigade was activated with the following unit redesignations:<ref name="USAREUR">[http://www.hqusareur.army.mil/news/releases/2008-03-06_02_RELEASE20080202%20_2_.pdf Army Announces Next Steps in USAREUR Transformation] {{webarchive |url=https://web.archive.org/web/20080910072603/http://www.hqusareur.army.mil/news/releases/2008-03-06_02_RELEASE20080202%20_2_.pdf |date=10 September 2008 }}, US Army Europs Office of Public Affairs. Retrieved 27 June 2008</ref>
* Headquarters and Headquarters Company, 172nd Infantry Brigade (formed from HHC, 2-1 ID)
* 2nd Battalion, 28th Infantry (reflagged from [[26th Infantry Regiment (United States)|1–26 Infantry]])
* 1st Battalion, 2nd Infantry (reflagged from [[18th Infantry Regiment (United States)|1–18 Infantry]])
* 3rd Battalion, [[66th Armor Regiment|66th Armor]] (reflagged from [[77th Armor Regiment (United States)|1–77 Armor]])
* Troop E, 5th Cavalry (reflagged from Troop E, [[4th Cavalry Regiment (United States)|4th Cavalry]])
* 1st Battalion, 77th Field Artillery (reflagged from [[7th Field Artillery Regiment (United States)|1–7 Field Artillery]])
* 172nd Support Battalion (reflagged from 299th Forward Support Battalion)
* 57th Signal Company, [[9th Engineer Battalion]] and 504th Military Intelligence Company remain attached to 172nd but were not reflagged.
When the brigade converts to a modular design, the Brigade Special Troops Battalion will be given organic, unnumbered signal, engineer and military intelligence companies along with a chemical and military police platoons.

After its activation, the brigade began moving its components from Schweinfurt to Grafenwöhr, Germany, as part of the [[Grow the Army]] plan.<ref>[http://www.2bct.1id.army.mil/Primary%20Sites/messages/09JAN08_1630.jpg Official 2BCT Message], United States Army. Retrieved 10 August 2008.</ref> Simultaneously, the brigade converted to a modular structure to become a [[Brigade Combat Team]] upon completion.<ref>[http://www.2bct.1id.army.mil/Primary%20Sites/index.htm Dagger 6 Note 03-08], COL Jeffry Sinclair, United States Army. Retrieved 10 August 2008.</ref> In May 2008, the brigade was alerted that it would be returning to Iraq in the fall of that year.<ref name="Armytimes">[http://www.armytimes.com/news/2008/05/army_deployments_051908w/ 25,000 headed to war later this year], Michelle Tan, ''Army Times'' News. Retrieved 28 June 2008.</ref><ref>[http://www.army.mil/-news/2008/05/20/9271-usareur-unit-tapped-for-deployment-to-iraq/ USAREUR unit tapped for deployment to Iraq], Seventh Army Public Affairs Office. Retrieved 28 June 2008.</ref> The deployment was set to last 12 months,<ref name="Armytimes"/> and was set to start after the unit's 12-month out-of-action cycle ended on November 2008.<ref>[http://www.172infantry.army.mil/Blackhawk6Notes/BlackHawk6Notes_2008-08.pdf Blackhawk 6 Note 08-08], COL Jeffry Sinclair, United States Army. Retrieved 28 June 2008.</ref><ref>[http://www.post-gazette.com/pg/08141/883224-84.stm W.Pa. Guard brigade headed for Iraq] Nancy A. Youssef, ''The Pittsburgh Post-Gazette''. Retrieved 14 August 2008.</ref> This would be the brigade's third tour to Iraq,<ref>[http://democrats.senate.gov/journal/entry.cfm?id=298057 With Troops Strained from Multiple Extended Deployments, They Deserve a GI Bill Worthy of Their Sacrifice], ''[[Democratic Party (United States)|Democratic Party]]'' Caucus Senate Journal. Retrieved 13 August 2008.</ref> as it completed a tour of duty in Iraq shortly before being redesignated from the 2nd Brigade, 1st Infantry Division. The brigade began training for its deployment to the country as soon as it received orders for deployment. German military officers trained with the brigade during this preparation.<ref>[http://www.army.mil/-images/2008/07/22/19677/ Army.mil Image], United States Army Homepage. Retrieved 10 August 2008.</ref> The soldiers of the brigade were part of a 40,000-soldier troop rotation into Iraq and Afghanistan, intended to maintain previous troop levels in both countries until late 2009.<ref>[http://www.baltimoresun.com/news/world/iraq/bal-te.troops20may20,0,6573372.story 40,000 troops told of fall deployment], David Wood, ''[[Baltimore Sun]]''. Retrieved 13 August 2008.</ref> In fall of 2008, the brigade completed its transition to a brigade combat team, and was redesignated as the 172nd Infantry Brigade Combat Team.<ref>[http://www.armytimes.com/news/2008/09/army_usareur_090808w/ New commander among big changes in Europe], ArmyTimes.com. Retrieved 10 December 2008.</ref>

[[File:172nd Infantry Brigade in Iraq.jpg|thumb|right|transfer of authority ceremony on [[FOB Kalsu]], 18 December 2008]]
In late October 2008 the brigade began moving equipment and vehicles by train from Germany in preparation for their tour in Iraq. 385 containers full of gear, as well as 75 [[M1A1 Abrams]] Tanks, [[M2 Bradley]] Infantry Fighting Vehicles, and [[HMMWV]]s were sent by train on 28 October. the brigade picked up additional [[MRAP]] and uparmored HMMWVs in Kuwait.<ref>Robson, Seth. [http://www.stripes.com/article.asp?article=58434&section=104 172nd Infantry Brigade ships tanks, gear for deployment], Stars and Stripes. Retrieved 1 December 2008.</ref> The brigade deployed into theater by December 2008, replacing the [[3rd Infantry Division (United States)|4th Brigade Combat Team, 3rd Infantry Division]].<ref>{{cite web|url=http://www.mnf-iraq.com/index.php?option=com_content&task=view&id=24577&Itemid=128 |title=Vanguard Bde transfers authority to 172nd Infantry Bde |publisher=Multi National Corps Iraq Public Affairs Office |accessdate=28 August 2009|archiveurl=http://www.webcitation.org/5jWKWAqQ9|archivedate=3 September 2009|deadurl=no}}</ref>

A proposal was made to relocate the unit to White Sands Missile Range, New Mexico in 2012 as the 7th Brigade Combat Team, [[1st Armored Division (United States)|1st Armored Division]], pending discussions to leave two heavy brigades in Europe.<ref name="standto"/>

===Afghanistan===

The 172 IBCT deployed to Afghanistan in the summer of 2011. The brigade left behind its "heavy" vehicles, Bradley fighting vehicles and Abrams tanks, for [[MRAP]]s. Soldiers will spend some of their time during the deployment patrolling on foot, as their normal heavy tracked vehicles are incompatible with rugged terrain of Afghanistan.
.<ref>{{cite web|author=Seth Robson |url=http://www.stripes.com/news/europe/two-europe-based-brigades-will-deploy-to-afghanistan-in-2011-1.125993 |title=Two Europe-based brigades will deploy to Afghanistan in 2011 – Europe |publisher=Stripes |accessdate=20 May 2011}}</ref> During this deployment the Brigade was responsible for Paktika province along the Pakistani border. One of the more controversial aspects of the deployment was the formation of the first US/Afghan Joint firing base with Afghan National Army Artillery firing in support of U.S. forces in the Urgun district.

Following a number of budget cuts and force structure reductions, the brigade deactivated in Germany on 31 May 2013.<ref>{{cite web| title =Final flourish as 172nd inactivates in Grafenwöhr | url = http://www.stripes.com/news/europe/final-flourish-as-172nd-inactivates-in-grafenw%C3%B6hr-1.223736| accessdate =31 May 2013}}</ref>

==Honors==

===Unit decorations===
{| class="wikitable"
|- style="background:#efefef;"
! Ribbon
! Award
! Year
! Notes
|-
||[[File:Valorous Unit Award ribbon.svg|50px]]
||[[Valorous Unit Award]]
||2006
||for service in Operation Iraqi Freedom, Permanent Order 241-05
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]]
||[[Meritorious Unit Commendation]]
||2009
||Operation Iraqi Freedom, Permanent Order 351-07
|-
||[[File:Meritorious Unit Commendation ribbon.svg|50px]]
||[[Meritorious Unit Commendation]]
||2012
||Operation Enduring Freedom, Permanent Order 226-03
|}

===Campaign streamers===
{| class=wikitable
! Conflict
! Streamer
! Year(s)
|-
|| [[World War I]]
|| (no inscription)
|| 1918
|-
|| [[World War II]]
|| [[Central Europe Campaign|Central Europe]]
|| 1944–1945
|-
|| [[Operation Iraqi Freedom]]
|| National Resolution
Iraqi Surge
|| 2005–2006
|-
|| [[Operation Iraqi Freedom]]
|| Iraqi Sovereignty
|| 2008-2009
|-
|| [[Operation Enduring Freedom]]
|| Transition I
|| 2011–2012
|}

==Notes==
{{Portal|United States Army}}
{{Reflist|30em}}

==References==
*{{Cite book|first=John J. |last=McGrath |title= The Brigade: A History: Its Organization and Employment in the US Army |year=2004 |publisher=Combat Studies Institute Press |isbn=978-1-4404-4915-4}}

==External links==
{{Commons category}}
* [http://www.172infantry.army.mil/#history 172nd Infantry.army.mil]
* [http://www.history.army.mil/books/Lineage/ACDSB/172Bde.htm Lineage and Heraldic information] at the [[United States Army Center of Military History]]

{{Use dmy dates|date=November 2012}}

[[Category:Infantry brigades of the United States Army|172]]
[[Category:Military units and formations in Alaska]]
[[Category:United States military in Germany]]