"'''A Virtuoso's Collection'''" is the final [[short story]] in ''[[Mosses from an Old Manse]]'' by [[Nathaniel Hawthorne]].  It was first published in ''[[Boston Miscellany of Literature and Fashion]]'', I (May 1842), 193-200.  
The story references a number of historical and mythical figures, items, beasts, books, etc. as part of a museum collection. Some scholars regard the real-life museum of the [[East India Marine Society]] in Salem, Massachusetts, as a model for Hawthorne's fictional museum.<ref>{{citation |title=Nathaniel Hawthorne and the Museum of the Salem East India Marine Society |author= Charles E. Goodspeed |location= Salem, Mass.|publisher= [[Peabody Museum of Salem|Peabody Museum]] |year= 1946 |url=http://catalog.hathitrust.org/Record/000479068 }} (fulltext via HathiTrust)</ref> The narrator is led through the collection by the virtuoso himself who turns out to be the [[Wandering Jew]].

== The collection ==

* Opportunity, by the ancient sculptor [[Lysippus]]
* The [[wolf]] that devoured [[Little Red Riding Hood]]
* The she-wolf that suckled [[Romulus and Remus]]
* [[Edmund Spenser]]'s 'milk-white lamb' which Una led in ''[[The Faerie Queene]]''
* [[Alexander the Great]]'s [[Bucephalus]]
* [[Don Quixote]]'s horse [[Rosinante]]
* The [[donkey]] from [[William Wordsworth]]'s [[Peter Bell (Wordsworth)|Peter Bell: A Tale]]
* The donkey from [[Book of Numbers]] chapter 22 that was beaten by [[Balaam]]
* [[Argos (dog)|Argus]], [[Odysseus|Ulysses]]' dog
* [[Cerberus]]
* The fox from [[Aesop]]'s fable [[The Fox Who Lost Its Tail]]
* [[Dr. Samuel Johnson]]'s cat [[Hodge (cat)|Hodge]]
* The cat who saved [[Muhammad]] from a snake, or [[Muezza]], the Prophet's pet.  Perhaps both cats are in the collection.
* [[Thomas Gray]]'s inspiration for the poem "Ode on the Death of a Favourite Cat Drowned in a Tub of Goldfishes".  The cat, [[Selima (cat)|Selima]], belonged to [[Horace Walpole]]
* [[Sir Walter Scott]]'s cat [[Hinse]]
* [[Puss in Boots (fairy tale)|Puss in Boots]]
* [[Bastet|Bast]], the Egyptian sun and war goddess, in her cat form
* [[George Gordon Byron]]'s pet bear
* The [[Erymanthian Boar|Erymanthean Boar]]
* St. George's Dragon. ''See'' [[Saint George and the Dragon]]
* [[Python (mythology)|Python]]
* The [[Serpent (Bible)|serpent]] which tempted [[Eve]]
* The horn's of the [[stag]] poached by [[Shakespeare]]<ref>http://shakespeare.palomar.edu/rowe.htm</ref>
* The shell of the [[tortoise]] that supposedly killed [[Aeschylus]]
* [[Apis (Egyptian mythology)|Apis]], an Egyptian bull-deity
* "The cow with the crumpled horn" from the nursery rhyme "[[This Is The House That Jack Built]]"
* The cow that jumped over the moon from the nursery rhyme "[[Hey Diddle Diddle]]"
* A [[griffin]]
* The dove that brought the olive branch to [[Noah]] to signify that the flood was receding
* Grip, the raven that belonged to [[Barnaby Rudge]] and later inspired [[Edgar Allan Poe]]'s "[[The Raven]]"
* The raven in which the soul of [[George I of Great Britain]] revisited his love, [[Melusine von der Schulenburg, Duchess of Kendal]] after his death
* [[Minerva]]'s owl
* The vulture (or eagle) that daily ate [[Prometheus]]'s liver
* The [[sacred ibis]] of Egypt
* One of the [[Stymphalian birds]] shot by [[Hercules]]. ''See'' [[Labours of Hercules]]
* [[Percy Bysshe Shelley]]'s skylark from "[[To a Skylark]]"
* [[William Cullen Bryant]]'s water-fowl from "[[To a Waterfowl]]"
* A pigeon, preserved by [[Nathaniel Parker Willis]], from the belfry of [[Old South Church]] in Boston
* The albatross from [[Samuel Taylor Coleridge]]'s ''[[The Rime of the Ancient Mariner]]''
* A [[domestic goose]] from the temple of Juno on the [[Capitoline Hill]].  [[Livy]] claimed these geese saved [[Rome]] from the [[Gauls]] around 390 BC.
* [[Robinson Crusoe]]'s parrot
* A live [[Phoenix (mythology)|phoenix]]
* A footless bird of paradise or [[Huma bird]]
* The peacock that once contained the soul of [[Pythagoras]]

== References ==
<references />

==Further reading==
* {{cite journal |url=http://hdl.handle.net/2027/nyp.33433081753711?urlappend=%3Bseq=237 |author=Nathaniel Hawthorne |title=A Virtuoso's Collection |journal=[[The Boston Miscellany|Boston Miscellany]] |date=May 1842 |volume=1 }}

==Bibliography==
*Goluboff, Benjamin (1995). "'A Virtuoso's Collection': Hawthorne, History, and the Wandering Jew".  ''Nathaniel Hawthorne Review'' 1995 Spring; 21 (1): 14-25. ISSN 0890-4197.
*McMurray, Price (2002). "'I Would Write on the Lintels of the Door-Post, Whim': History and Idealism in '''A Virtuoso's Collection'''. ''Conference of College Teachers of English Studies'' 2002 September; 67: 32-42. ISSN 0092-8151.

{{DEFAULTSORT:Virtuoso's Collection}}
[[Category:Short stories by Nathaniel Hawthorne]]
[[Category:1842 short stories]]
[[Category:Works originally published in The Boston Miscellany]]