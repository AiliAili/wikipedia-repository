{{Infobox basketball biography
| name         = Danny Green
| image        = Danny Green warms up 20090930-6130.jpg
| image_size   = 
| caption      = Green with the Cleveland Cavaliers in November 2009
| position     = [[Shooting guard]] / [[Small forward]]
| height_ft    = 6
| height_in    = 6
| weight_lb    = 215
| league       = [[National Basketball Association|NBA]]
| team         = San Antonio Spurs
| number       = 14
| birth_date   = {{Birth date and age|1987|06|22|mf=y}}
| birth_place  = [[North Babylon, New York]]
| nationality  = American
| high_school  = 
*{{nowrap|North Babylon}} {{nowrap|(North Babylon, New York)}}
*[[St. Mary's High School (Manhasset, New York)|St. Mary's]] ([[Manhasset, New York]])
| college      = [[North Carolina Tar Heels men's basketball|North Carolina]] ([[2005–06 NCAA Division I men's basketball season|2005]]–[[2008–09 NCAA Division I men's basketball season|2009]])
| draft_year   = 2009
| draft_round  = 2
| draft_pick   = 46
| draft_team   = [[Cleveland Cavaliers]]
| career_start = 2009
| career_end   = 
| years1       = {{nbay|2009|full=y}}
| team1        = [[Cleveland Cavaliers]]
| years2       = [[2009–10 NBA Development League season|2010]]
| team2        = →[[Erie BayHawks]]
| years3       = {{nbay|2010|start}}
| team3        = [[San Antonio Spurs]]
| years4       = [[2010–11 NBA Development League season|2011]]
| team4        = [[Reno Bighorns]]
| years5       = {{nbay|2010|end}}–present
| team5        = San Antonio Spurs
| years6       = [[2010–11 NBA Development League season|2011]]
| team6        = →[[Austin Toros]]
| years7       = [[2011–12 Telemach League|2011]]
| team7        = [[KK Union Olimpija|Union Olimpija]]
| highlights   =
* [[List of NBA champions|NBA champion]] ([[2014 NBA Finals|2014]])
* [[List of NCAA Men's Division I Basketball Champions|NCAA champion]] ([[2009 NCAA Men's Division I Basketball Tournament|2009]])
* Third-team All-[[Atlantic Coast Conference|ACC]] ([[2008–09 Atlantic Coast Conference men's basketball season|2009]])
* ACC All-Defensive Team (2009)
* [[McDonald's All-American Game|McDonald's All-American]] ([[2005 McDonald's All-American Boys Game|2005]])
* Second-team [[Parade All-America Boys Basketball Team|''Parade'' All-American]] (2005)
| bbr         = greenda02
| profile     = danny_green
}}
'''Daniel Richard "Danny" Green, Jr.''' (born June 22, 1987) is an American professional [[basketball]] player for the [[San Antonio Spurs]] of the [[National Basketball Association]] (NBA). He is a [[swingman]] from the [[University of North Carolina at Chapel Hill|University of North Carolina]], where he played in more games (145) and had more wins (123) than any [[North Carolina Tar Heels|Tar Heel]] before him. Green is also the only player in the history of the [[Atlantic Coast Conference]] (ACC) with at least 1,000 points, 500 rebounds, 250 assists, 150 three-pointers, 150 blocks and 150 steals.<ref>{{cite web|url=http://sportsillustrated.cnn.com/2009/writers/joe_lemire/04/03/danny.green/1.html |title=Doing the little things, UNC's Green makes his dad, teammates proud |publisher=Sportsillustrated.cnn.com |date=2009-04-03 |accessdate=2013-06-23}}</ref> He won an [[NCAA Men's Division I Basketball Championship|NCAA championship]] his senior year and was subsequently drafted by the [[Cleveland Cavaliers]] with the 46th overall pick in the [[2009 NBA draft]]. During the [[2013 NBA Finals]], Green set an NBA record for most [[three-point field goals]] made in a Finals series. He then won an NBA championship with the San Antonio Spurs the following season, and became just the third player from UNC to win an NCAA championship and an NBA championship, the two others being [[James Worthy]], and [[Michael Jordan]].

==High school career==
As a high school freshman, Green attended [[North Babylon Union Free School District|North Babylon High School]] in North Babylon, New York on Long Island and in addition to basketball, he played [[quarterback]] on the [[American Football|football]] team. From his sophomore onwards, Green attended [[St. Mary's High School (Manhasset, New York)|St. Mary's High School]], a private school, in [[Manhasset, New York]].<ref>{{cite web |last=Chan |first=Lorne |url=http://www.nba.com/spurs/persistence-danny-green |title=The Persistence of Danny Green |work=NBA.com |date=July 14, 2015 |accessdate=February 28, 2016}}</ref> He averaged 20 points, 10 rebounds, 4 assists and 4 blocks as a senior.

Considered a four-star recruit by [[Rivals.com]], Green was listed as the No. 8 shooting guard and the No. 31 player in the nation in 2005.<ref>[https://n.rivals.com/content/prospects/maple/21525 Danny Green – Rivals.com]</ref>

==College career==
Green came off the bench as the [[sixth man]] during his freshman year at [[University of North Carolina|UNC]]. He averaged 5.2 points and 2.8 rebounds in his sophomore season.<ref name="espn.com college profile">[http://sports.espn.go.com/ncb/player/profile?playerId=27017 "Danny Green Stats, News, Photos,"] ESPN Men's College Basketball, North Carolina Tar Heels, April 20, 2009.</ref> After his second year at North Carolina Green considered transferring, but would ultimately finish his college career there.<ref name=":0" /> Green improved his scoring average in each of the next two seasons.

Green's junior year, he averaged 11.5 points, 4.9 rebounds, 2.0 assists, 1.9 turnovers, 1.2 steals, 1.2 blocks in 22.3 minutes per game.<ref name="espn.com college profile"/> He also improved his true shooting percentage (TS%) significantly, increasing his field goal percentage to 46.9% and his free throw percentage to 87.3%. He shot 37.3% from the three-point line.<ref name="espn.com college profile"/>

Approaching his [[2008–09 North Carolina Tar Heels men's basketball team|senior]] season, he declared himself eligible for the 2008 NBA Draft, but did not sign with an agent so that he had the option to return to school, which he decided to do on June 16.<ref name=":0">[http://tarheelblue.cstv.com/sports/m-baskbl/mtt/green_danny00.html "Player Profile, Danny Green,"] tarheelblue.cstv.com, April 20, 2009.</ref> He went on to average 13.1 points, 4.7 rebounds, 2.7 assists, 1.7 turnovers, 1.8 steals and 1.3 blocks in 27.4 minutes per game.<ref name="espn.com college profile"/> He again improved his shooting percentages, averaging 47.1% and 41.8% from the field and three-point line respectively.<ref name="espn.com college profile"/>
In his senior year, Green was selected to be a member of the ACC's All-Defensive Team. He was also named as a team captain along with [[Bobby Frasor]] and [[Tyler Hansbrough]].<ref>[http://northcarolina.scout.com/2/857249.html "No Decision Yet for Lawson, Ellington,"] InsideCarolina.com, The Independent Voice of UNC sports, April 16, 2009.</ref>

===Achievements at UNC===
*Part of the 2009 national championship team's starting five. Green logged a solid overall Final Four performance with 18 total points including six made three-pointers, six rebounds, five assists, three blocks, and one steal.
*Green is the only Tar Heel ever to have 1,000 points (1,368), 500 rebounds (590), 200 assists (256), 100 blocks (155) and 100 steals (160)
*One of four players in ACC history with 100 blocked shots and 100 three-point field goals (with Duke's [[Shane Battier]], Maryland's [[Terence Morris]] and Wake Forest's [[Josh Howard]])
*Played in 145 games and been a part of 123 wins setting a new UNC record (record of 115 previously held by [[Sam Perkins]])
*Scored 1,368 career points (9.4 per game)
*Passed [[Vince Carter]] in scoring at Virginia Tech on March 4, 2009
*Blocked a career-high 7 shots in the win at Duke on March 8, 2008
*Only Tar Heel ever to block 100 or more shots and make 50 or more three-pointers
*Scored in double figures 63 times (nine times in 2005–2006, four times in 2006–2007, 24 times in 2007–08 and 26 times in 2008–2009)
*Scored 20 or more points seven times in his career, including five times in 2009
*Led Carolina in blocked shots as a freshman with 32 and was second the next three seasons
*UNC’s defensive player of the game 15 times (twice as a sophomore, six times as a junior and seven times as a senior)
*Played in four wins at Duke, joining [[Tyler Hansbrough]] and Wake Forest's [[Tim Duncan]] and [[Rusty LaRue]] as the only players to do that against [[Mike Krzyzewski]]-coached teams.

===College career highs===
*Points: 26 at Chaminade (11/24/08)
*Field Goals: 11 at Chaminade (11/24/08)
*Three-Pointers: 6 vs. UNC Asheville (11/30/08)
*Free Throws: 7 at Wake Forest (1/11/09)
*Offensive Rebounds: 6 vs. NC State (1/12/08)
*Rebounds: 14 vs. NC State (1/12/08)
*Assists: 7 vs. Dayton (12/31/06), UNC Asheville (1/9/08), Gonzaga (3/27/09)
*Turnovers: 6 at Florida State (1/28/09)
*Blocks: 7 at Duke (3/8/08)
*Steals: 6 at Florida State (1/28/09)

==Professional career==

===Cleveland Cavaliers (2009–2010)===
The [[Cleveland Cavaliers]] selected Green as the 46th overall pick of the [[2009 NBA draft]]. After he played in 20 games in his rookie year with the Cavaliers, the team waived Green at the beginning of the next season.

===San Antonio Spurs (2010–present)===
Green was subsequently picked up by the [[San Antonio Spurs]] on November 17, 2010.<ref>{{cite web|url=http://www.nba.com/spurs/news/spurs_sign_danny_green_101117.html |title=Spurs Sign Danny Green |publisher=Nba.com |date=2010-10-19 |accessdate=2013-06-23}}</ref> The Spurs waived him six days later after he appeared in two games.<ref>{{cite news |title=Spurs Waive Danny Green |url=http://www.nba.com/spurs/news/spurs_waive_danny_green_101123.html |work=NBA.com |publisher=Turner Sports Interactive, Inc. |date=November 23, 2010 |accessdate=2010-11-23}}</ref> In January 2011, Green was acquired by the [[Reno Bighorns]] of the NBA D-League. He averaged 20 points, a team high, and 7.5 rebounds in 16 games with the Bighorns.<ref>{{cite web|author=|url=http://blog.mysanantonio.com/spursnation/2011/03/15/spurs-sign-swing-man-danny-green/ |title=Spurs Nation » Spurs sign swing man Danny Green |publisher=Blog.mysanantonio.com |date=2011-03-15 |accessdate=2013-06-23}}</ref> The Spurs signed Green again in March 2011,<ref>{{cite web|author=|url=http://blog.mysanantonio.com/spursnation/2011/03/15/spurs-sign-swing-man-danny-green/ |title=Spurs sign swing man Danny Green |publisher=Blog.mysanantonio.com |date=2011-03-15 |accessdate=2013-06-23}}</ref> assigned him to the [[Austin Toros]] of the NBA Development League on April 2, and then recalled him on April 3.<ref>{{cite web|url=http://www.nba.com/dleague/austin/green_110402.html |title=Spurs Assign Danny Green To Austin Toros |publisher=Nba.com |date=2011-04-02 |accessdate=2013-06-23}}</ref>

In August 2011, Green signed a one-year contract with [[KK Union Olimpija]], which included an NBA-out clause option when the [[2011 NBA lockout]] ended.<ref>{{cite web|url=http://www.sportando.net/eng/europe/adriatic-league/30414/danny_green_moves_to_union_olimpija.html |title=Danny Green moves to Union Olimpija |publisher=Sportando.net |date= |accessdate=2013-06-23}}</ref> Green returned to the Spurs after the lockout ended. Green had a breakout season, as he started 38 of his 66 games played, averaging 9.1 ppg. Green eventually became the starting shooting guard for the Spurs when [[Manu Ginobili]] returned to being the sixth man in the rotation. Green finished ninth in the league in voting for the [[NBA Most Improved Player Award]].<ref>{{cite news |title=Magic's Ryan Anderson wins Kia Most Improved Player Award |url=http://www.nba.com/2012/news/05/04/most-improved-player/index.html|work=nba.com |publisher=[[NBA]] |date= 4 May 2012|accessdate=2012-05-20}}</ref>

On July 11, 2012, Green re-signed with the Spurs for $12&nbsp;million over three years.<ref>{{cite web|url=http://www.nba.com/spurs/news/120711_green |title=Spurs Re-Sign Danny Green |publisher=Nba.com |date= |accessdate=2013-06-23}}</ref> In his first game of the season, he scored 9 points and added 2 blocks in San Antonio's win over New Orleans. On November 1, 2012, he scored 13 points in a win over the Thunder. Then on November 3, Danny Green scored 21 points to help the Spurs beat the [[Utah Jazz]] in a 110–100 win.<ref>{{cite web|url=http://www.nba.com/games/20121103/UTASAS/gameinfo.html |title=Notebook: Spurs 110, Jazz 100 |publisher=Nba.com |date= |accessdate=2013-06-23}}</ref> On November 13, 2012, he hit a game-winner against the [[Los Angeles Lakers]], finishing the game with 11 points. On February 6, 2013, Green recorded career-highs of 28 points and 8 three-pointers made in a win over the [[Minnesota Timberwolves]]. He was one three-pointer shy of [[Chuck Person]]'s record for most three-pointers made in a single game as a Spur.<ref>{{cite web|url=http://www.nba.com/games/20130206/SASMIN/gameinfo.html |title=Notebook: Spurs 104, Timberwolves 94 |publisher=Nba.com |date= |accessdate=2013-06-23}}</ref>

In Game 2 of the [[2013 NBA Finals]], Green was perfect from the field, including 5–5 from the three-point line. However, the Spurs lost in a blowout to the [[Miami Heat]], 103–84.<ref>{{cite web|url=http://espn.go.com/nba/recap?gameId=400467334|title=Heat ride Mario Chalmers in second half, cruise by Spurs in Game 2|date=June 9, 2013|accessdate=2013-06-17|work=ESPN.com}}</ref> In Game 3 of the NBA Finals, Green hit 7–9 from three-point range, including the one to set a Finals record for most three-pointers in a game by a team. He notched 27 total points in the Spurs' blowout 113–77 victory as the team took a 2–1 series lead in the process.<ref>{{cite web|url=http://espn.go.com/nba/recap?gameId=400467335|title=Spurs hit NBA Finals-record 16 3-pointers in rout of Heat|date=June 11, 2013|accessdate=2013-06-17|work=ESPN.com}}</ref> On June 16 in Game 5, Green made six three pointers for a total of 25 in the series to that point, breaking the record for an NBA Finals series previously held by [[Ray Allen]], who made 22 in six games with the [[Boston Celtics]] in [[2008 NBA Finals|2008]].<ref>{{cite web|url=http://espn.go.com/nba/recap?gameId=400467337|title=Manu Ginobili's surprise start sparks Spurs, pushes Heat to brink|date=June 16, 2013|accessdate=2013-06-17|work=ESPN.com}}</ref> By the end of the series, Green had made 27 three-pointers, but the Spurs lost the series in seven games. The record was later broken by [[Stephen Curry]] in [[2016 NBA Finals|2016]].<ref>{{cite web|last1=Young|first1=Royce|title=Danny Green breaks NBA Finals 3-point record|url=http://www.cbssports.com/nba/news/danny-green-breaks-nba-finals-3-point-record/|website=CBS Sports|accessdate=September 18, 2016|date=June 16, 2013}}</ref><ref>{{cite web|last1=Garcia|first1=Paul|title=Green’s 3-Point Finals Record broken by Curry in Game 6 of 2016 Finals|url=http://projectspurs.com/2016-articles/greens-3-point-finals-record-broken-by-curry-in-game-6-of-2016-finals.html|website=Project Spurs|accessdate=September 18, 2016|date=June 16, 2016}}</ref>

On April 11, 2014, Green scored a career high 33 points in a 112-104 win over the Phoenix Suns.<ref>[http://www.nba.com/games/20140411/PHXSAS/gameinfo.html Notebook: Spurs 110, Suns 104]</ref> On June 15, 2014, Green won his first NBA championship after the Spurs defeated the [[Miami Heat]] 4 games to 1 in the [[2014 NBA Finals]]. In doing so, Green joined [[Michael Jordan]] and [[James Worthy]] as the third Tar Heel to win both the NCAA and NBA championship.

On December 19, 2014, Green scored a season-high 27 points in the 119-129 triple overtime loss to the Portland Trail Blazers.<ref>[http://www.nba.com/games/20141219/PORSAS/gameinfo.html Lillard scores 43 as Blazers beat Spurs in 3 OT]</ref> On April 12, 2015, Green recorded 3 three-pointers against the Phoenix Suns to set a franchise record for the most three-pointers in a season at 191.<ref>[http://www.nba.com/games/20150412/PHXSAS/gameinfo.html Duncan leads Spurs to 11th straight win, 107-91 over Suns]</ref>

On July 14, 2015, Green re-signed with the Spurs<ref>{{cite news|title=Spurs Re-Sign Danny Green|url=http://www.nba.com/spurs/spurs-re-sign-danny-green|accessdate=July 14, 2015|work=NBA.com|date=July 14, 2015}}</ref> to a reported four-year, $45 million contract.<ref>{{cite news|url=http://www.usatoday.com/story/sports/nba/spurs/2015/07/01/danny-green-4-year-deal-spurs/29563997/|title=Danny Green agrees to 4-year contract with Spurs|publisher=osatoday.com|author=Sam Amick|date=2015-07-01|accessdate=2015-07-01}}</ref> On January 6, 2016, Green hit two three-pointers against the Utah Jazz, giving him 662 with San Antonio to surpass [[Bruce Bowen]] (661) for second in franchise history.<ref>[http://www.nba.com/games/20160106/UTASAS/gameinfo.html Duncan scores 18, Spurs rout Jazz to move to 21-0 at home]</ref>

On November 9, 2016, Green made his season debut for San Antonio, scoring eight points against the Houston Rockets in his return from a strained left quadriceps.<ref>[http://www.espn.com/nba/recap?gameId=400899564 Harden's triple-double leads Rockets past Spurs, 101-99]</ref>

==NBA career statistics==

{{NBA player statistics legend}}

{| class="wikitable"
|-
| style="background:#afe6ba; width:3em;"|†
|Denotes season in which Green won an [[List of NBA champions|NBA Championship]]
|}

===Regular season===

{{NBA player statistics start}}
|-
| style="text-align:left;"| {{nbay|2009}}

| style="text-align:left;"| [[2009–10 Cleveland Cavaliers season|Cleveland]]
| 20 || 0 || 5.8 || .385 || .273 || .667 || .9 || .3 || .3 || .2 || 2.0
|-
| style="text-align:left;"| {{nbay|2010}}
| style="text-align:left;"| [[2010–11 San Antonio Spurs season|San Antonio]]
| 8 || 0 || 11.5 || '''.486''' || .368 || .000 || 1.9 || .3 || .3 || .1 || 5.1
|-
| style="text-align:left;"| {{nbay|2011}}
| style="text-align:left;"| [[2011–12 San Antonio Spurs season|San Antonio]]
| 66 || 38 || 23.1 || .442 || '''.436''' || .790 || 3.5 || 1.3 || .9 || .7 || 9.1
|-
| style="text-align:left;"| {{nbay|2012}}
| style="text-align:left;"| [[2012–13 San Antonio Spurs season|San Antonio]]
| 80 || '''80''' || 27.5 || .448 || .429 || .848 || 3.1 || 1.8 || '''1.2''' || .7 || 10.5
|-
| style="text-align:left;background:#afe6ba;"| {{nbay|2013}}†
| style="text-align:left;"| [[2013–14 San Antonio Spurs season|San Antonio]]
| 68 || 59 || 24.3 || .432 || .415 || .794 || 3.4 || 1.5 || 1.0 || .9 || 9.1
|-
| style="text-align:left;"| {{nbay|2014}}
| style="text-align:left;"| [[2014–15 San Antonio Spurs season|San Antonio]]
| '''81''' || '''80''' || '''28.5''' || .436 || .418 || '''.874''' || '''4.2''' || '''2.0''' || '''1.2''' || '''1.1''' || '''11.7'''
|-
| style="text-align:left;"| {{nbay|2015}}
| style="text-align:left;"| [[2015–16 San Antonio Spurs season|San Antonio]]
| 79 || 79 || 26.1 || .376 || .332 || .739 || 3.8 || 1.8 || 1.0 || .8 || 7.2 
|- class="sortbottom"
| style="text-align:center;" colspan="2" | Career
| 402 || 336 || 24.8 || .428 || .403 || .819 || 3.4 || 1.6 || 1.0 || .8 || 9.1
{{s-end}}

===Playoffs===
{{NBA player statistics start}}
|-
| style="text-align:left;"| [[2011 NBA Playoffs|2011]]
| style="text-align:left;"| [[2010–11 San Antonio Spurs season|San Antonio]]
| 4 || 0 || 1.8 || .333 || .250 || .000 || .3 || .5 || .3 || .3 || 1.3
|-
| style="text-align:left;"| [[2012 NBA Playoffs|2012]]
| style="text-align:left;"| [[2011–12 San Antonio Spurs season|San Antonio]]
| 14 || 12 || 20.6 || .418 || .345 || .700 || 3.2 || 1.1 || .5 || .7 || 7.4
|-
| style="text-align:left;"| [[2013 NBA Playoffs|2013]]
| style="text-align:left;"| [[2012–13 San Antonio Spurs season|San Antonio]]
| 21 || 21 || '''31.9''' || .446 || .482 || .800 || '''4.1''' || 1.5 || 1.0 || '''1.1''' || '''11.1'''
|-
| style="text-align:left;background:#afe6ba;"| [[2014 NBA Playoffs|2014]]†
| align="left" | [[2013–14 San Antonio Spurs season|San Antonio]]
| '''23''' || '''23''' || 23.0 || '''.491''' || .475 || '''.818''' || 3.0 || .9 || 1.4 || .7 || 9.3
|-
| style="text-align:left;"| [[2015 NBA Playoffs|2015]]
| align="left" | [[2014–15 San Antonio Spurs season|San Antonio]]
| 7 || 7 || 29.1 || .344 || .300 || .667 || 3.1 || '''2.1''' || 1.0 || 1.0 || 8.3
|-
| style="text-align:left;"| [[2016 NBA Playoffs|2016]]
| align="left" | [[2015–16 San Antonio Spurs season|San Antonio]]
| 10 || 10 || 26.7 || .462 || '''.500''' || .667 || 3.1 || .7 || '''2.1''' || .8 || 8.6
|- class="sortbottom"
| style="text-align:center;" colspan="2" | Career
| 79 || 73 || 24.9 || .444 || .438 || .756 || 3.2 || 1.1 || 1.0 || .8 || 8.8
{{s-end}}

==Personal==
Green's brother, Rashad, played for [[Manhattan College]] in 2007–08 and the [[San Francisco Dons|University of San Francisco]] from 2009 to 2012.<ref>[http://espn.go.com/mens-college-basketball/player/_/id/37416/rashad-green Rashas Green]. ESPN.</ref> His younger brother, Devonte, plays for [[Indiana University]].<ref>[http://www.newsday.com/sports/high-school/boys-basketball/danny-green-confident-brother-devonte-will-thrive-at-indiana-1.11315608 Danny Green confident brother Devonte will thrive at Indiana]</ref> His second cousin is NBA player [[Gerald Green]].<ref>[http://usfdons.cstv.com/sports/m-baskbl/spec-rel/081408aaa.html "USF Men's Basketball Announces Signing of Rashad Green,"] USF official website, August 14, 2008.</ref> A first cousin, Jordan Green, went to Texas A&M.<ref>[http://www.12thman.com/ViewArticle.dbml?ATCLID=205237426 Jordan Green Bio]</ref>

After Green's freshman season at UNC in 2006 his father, Danny Green Sr., was arrested on drug trafficking charges along with 13 other people.<ref name="nydailynews1">{{cite web|url=http://www.nydailynews.com/sports/basketball/north-babylon-green-spur-moment-article-1.1366966|title=Persistence of North Babylon’s Danny Green pays off for San Antonio Spurs|date=|publisher=NY Daily News|accessdate=2013-06-23}}</ref> Police confiscated 462 pounds of cocaine worth an estimated street value of $40&nbsp;million, as well as $5&nbsp;million in cash, marijuana and assorted firearms. Green Sr., a gym teacher, said he had no connection to the drug trafficking ring. He pleaded not guilty and bail was set at $7.5&nbsp;million cash. After Green Sr. was in jail for 18 months, officials offered him a lesser charge of conspiracy that would include time served and get him out of jail within six months. He was paroled on January 28, 2008 after serving 22 months in prison.<ref>[http://www.nydailynews.com/sports/college/2008/0$/04/2008-04-04_green_family_has_heel_of_a_story.html "Green family has Heel of a story,"] New York Daily News website, April 20, 2009.</ref>

Green was well known for dancing to the song [[Jump Around]] to pump up the crowd before most of the University of North Carolina home games.<ref>Lemire, Joe. "[http://sportsillustrated.cnn.com/2009/writers/joe_lemire/04/03/danny.green/index.html Doing the little things, UNC's Green makes his dad, teammates proud]", ''[[Sports Illustrated]]'' April 3, 2009. Image caption.</ref>

==See also==
{{Portal|National Basketball Association}}
* [[List of NCAA Division I men's basketball players with 145 games played]]

==References==
{{reflist|30em}}

==External links==
{{commons category|Danny Green (basketball player)}}
{{Basketballstats|nba=daniel_green|bbr=g/greenda02}}
*[http://www.goheels.com/ViewArticle.dbml?ATCLID=205674001 UNC bio]
*[http://northcarolina.scout.com/a.z?s=78&p=8&c=1&cfg=bb&yr=2005&nid=3890711 Scout.com Profile]
*[http://espn.go.com/nba/player/_/id/3988/danny-green ESPN.com Profile]

{{San Antonio Spurs current roster}}
{{Navboxes|list1=
{{San Antonio Spurs 2013–14 NBA champions}}
{{2009 North Carolina Tar Heels men's basketball navbox}}
{{2009 NBA Draft}}
}}

{{DEFAULTSORT:Green, Danny}}
[[Category:1987 births]]
[[Category:Living people]]
[[Category:African-American basketball players]]
[[Category:American expatriate basketball people in Slovenia]]
[[Category:American men's basketball players]]
[[Category:Austin Toros players]]
[[Category:Basketball players at the 2008 NCAA Men's Division I Final Four]]
[[Category:Basketball players at the 2009 NCAA Men's Division I Final Four]]
[[Category:Basketball players from New York]]
[[Category:Cleveland Cavaliers draft picks]]
[[Category:Cleveland Cavaliers players]]
[[Category:Erie BayHawks players]]
[[Category:KK Union Olimpija players]]
[[Category:McDonald's High School All-Americans]]
[[Category:North Carolina Tar Heels men's basketball players]]
[[Category:Parade High School All-Americans (boys' basketball)]]
[[Category:People from Manhasset, New York]]
[[Category:People from North Babylon, New York]]
[[Category:Reno Bighorns players]]
[[Category:San Antonio Spurs players]]
[[Category:Shooting guards]]
[[Category:Small forwards]]
[[Category:Sportspeople from Suffolk County, New York]]