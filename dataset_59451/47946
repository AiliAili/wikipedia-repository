'''Kenneth William Ford''' (born May 1, 1926) is an [[United States|American]] [[theoretical physicist]], teacher, and writer, currently residing in [[Philadelphia]], [[Pennsylvania]]. He was the first chair of the physics department at the [[University of California, Irvine]], and later served as president of the [[New Mexico Institute of Mining and Technology]] (New Mexico Tech) and as Executive Director and [[CEO]] of the [[American Institute of Physics]].

==Biography==
Ken was born on May 1, 1926 in [[West Palm Beach, Florida]], to parents Paul Hammond Ford (1892-1961) and Edith Timblin Ford (1892-1992) and was the second of their three children. He spent most of his childhood in Kentucky, living one year in Georgia when he was eight and nine.

===Education===
Ken attended Highlands High School in [[Fort Thomas, Kentucky]] from 1940 to 1942, then [[Phillips Exeter Academy]] in [[Exeter, NH]], graduating in 1944. In 1948, he received an [[A.B.]] from [[Harvard University]], graduating ''[[summa cum laude]]''. He received his [[Ph.D.]] in [[physics]] from [[Princeton University]] in 1953, studying under [[John Archibald Wheeler]]. During 1950-1952 he interrupted his graduate studies to join the H-bomb design team at [[Los Alamos National Laboratory]] (then Los Alamos Scientific Laboratory) and at [[Princeton University]]'s [[Project Matterhorn]].

===US Navy===
In April 1944, just before his 18th birthday, while still at Exeter, Ken enlisted in the [[US Navy]]. After summer employment at the  [[Woods Hole Oceanographic Institution|Marine Biological Laboratory]] in [[Woods Hole, Massachusetts]], Ken was called to active service and began the Navy's [[Electronics Technician (US Navy)|Electronic Technician Training]]. In June 1945, he transferred into the [[V-12 Navy College Training Program]], studying at [[John Carroll University]] in [[Cleveland, Ohio]],and at the [[University of Michigan]] in [[Ann Arbor, MI|Ann Arbor]]. In three four-month semesters completed in one year, he was able to secure credit for two years of college work and entered [[Harvard University]] as a junior in the fall of 1946 following his discharge from the Navy in June of that year.<ref name="aip1">[http://www.aip.org/history/ohilist/24701_1.html Interview of Kenneth W. Ford by Alexei Kojevnikov on November 15, 1997, Niels Bohr Library & Archives, American Institute of Physics, College Park, MD USA]</ref>

===Graduate Work and the H-Bomb===
In the fall of 1948, Ken began graduate studies in physics at [[Princeton University]]. In 1950, he took a leave of absence from graduate work to work on the [[H-bomb]] at [[Los Alamos National Laboratory]] (then called the Los Alamos Scientific Laboratory) with his mentor [[John Archibald Wheeler|John Wheeler]].<ref name="aip1"></ref> Others in Wheeler's immediate group were John Toll and Burton Freeman. They worked closely with other lab staff members such as [[Carson Mark]], [[Conrad Longmire]], [[Edward Teller]], and [[Stanislaw Ulam]], and with lab consultants such as [[John von Neumann]], [[Enrico Fermi]], and [[Hans Bethe]]. Following the radiation implosion idea offered by Teller and Ulam early in 1951, the focus of the work was on this new design idea. In June 1951, Ken returned to Princeton to continue the H-bomb work there at Wheeler's [[Project Matterhorn]].

Much of Ken's work was concerned with writing programs and doing calculations related to [[Nuclear fusion|thermonuclear burning]], first using human "computers" with desk calculators, then [[IBM CPC|IBM card-programmed calculators]] (CPCs), and later, with Project Matterhorn, the [[SEAC (computer)|SEAC]] computer at the [[National Bureau of Standards]] in [[Washington, DC]]. A SEAC calculation, programmed by Ken with the assistance of John Toll and the guidance of John Wheeler, provided the final predicted yield of 7 megatons for the [[Ivy Mike|Mike test]] on November 1, 1952. The actual yield was approximately 10 megatons.
<blockquote>
"I was aware of the fact that Oppenheimer was expressing opposition to the development of the H-bomb, for various reasons, from technical to moral. But I was also an accidental participant in what may have been the critical moment in changing his point of view. In June of '51 there was a meeting of the General Advisory Committee of the Atomic Energy Commission [actually a broader gathering that included the GAC] meeting at the Institute for Advanced Study. They were meeting in a first floor room on, if I remember correctly, a Sunday morning. Maybe it was a Saturday morning. It was a weekend. [It was Sunday, June 17.] Wheeler was scheduled to make a presentation on the latest results of our calculations on the CPC in New York. I was working every night, all night, that week prior to that meeting trying to get the latest and best results that we could.
</blockquote><blockquote>
"The night prior to this meeting at the Institute for Advanced Study, I took a morning train down from New York with the latest results, went over to the Matterhorn building, got out a very large piece of paper, maybe about two feet by three feet in size, a large rectangle of paper, and sketched out a graph showing our latest calculation of thermonuclear burning, still very crude of course, because of those calculations, yet extremely encouraging. I then rolled up this large graph and drove over to the Institute and walked up to the first-floor window of the room where the meeting was in progress and either rapped gently on the window or signaled through the window to catch Wheeler's attention. As it happened, Wheeler had just taken the floor. It had just become his turn to speak. He interrupted his speech, walked over to the window, opened it, took from me this large graph, carried it back and pasted it on the blackboard for all to see. And then, at that moment, according to the reports of those who were there, Oppenheimer suddenly decided, 'This does look encouraging. I think they've really got something. It looks like it's going to work after all.'"<ref name="aip1"></ref>
</blockquote>
In the fall of 1952, Ken left project Matterhorn and spent the next six months completing his graduate dissertatation on the collective model of the nucleus. After defending his dissertation in the spring of 1953, and after spending that summer working in Los Alamos, he took up a post-doctoral research appointment at [[Indiana University]], beginning that fall.

Ken furthered his professional education in two subsequent leaves of absence: in 1955-56 at the [[Max Planck Institut]] in [[Göttingen, Germany]], mentored by [[Werner Heisenberg]] and supported by a [[Fulbright Fellowship]];<ref name="aip1"></ref> and in 1961-62 at [[Imperial College, London]] and [[MIT]] in [[Cambridge, Massachusetts]], mentored by [[Abdus Salam]], [[Herman Feshbach]], and [[Victor Weisskopf]] and supported by a [[National Science Foundation]] Senior Postdoctoral Fellowship. It was during this second leave that Ken wrote his first book, ''The World of Elementary Particles''.

In 1964, Ken took a job as professor and department chair at the newly opened [[University of California, Irvine]], setting up the Physics Department there for its opening in the fall of 1965.<ref>{{cite web|url=http://sunsite.berkeley.edu/~ucalhist/general_history/campuses/uci/colleges.html |title=University of California History Digital Archives |publisher=Sunsite.berkeley.edu |date=1965-07-01 |accessdate=2013-07-19}}</ref>

===Cessation of weapons work===
In the summer of 1968, influenced by his opposition to the Vietnam War, Ken announced at a talk in [[Cloudcroft, NM]] that he would no longer do weapons work or other secret work.<ref>http://www.spsnational.org/governance/ethics/ford.pdf</ref>

===Research, teaching, administration, and writing===
Ken's principal research was in the theory of [[nuclear structure]], with some work in [[particle physics|particle and mathematical physics]]. He exploited the [[nuclear shell]] model and the [[Unified field theory|collective, or unified, model]], and also worked extensively on [[muonic atoms]]. His first paper, co-authored with [[David Bohm]] in 1950, used data from low-energy neutron scattering to give evidence for the transparency of nuclei to neutrons. A 1953 paper showed how regularites in the energies of the first excited states of even-even nuclei can be interpreted in terms of the deformations of these nuclei. Later papers analyzed muonic-atom data to give evidence on the distribution of electric charge within nuclei. Ken's 1959 papers with [[John Archibald Wheeler|John Wheeler]] provided a semiclassical theory of scattering. In 1963, he participated, with [[Henry Kolm]] and [[Eiichi Goto]], in a search for [[magnetic monopoles]].

Although Ken's initial appointment at [[Indiana University]] in 1953 was as a postdoctoral researcher, he was given the opportunity to teach a graduate course. In later appointments at Indiana and other universities, he continued to teach both graduate students and undergraduate. Subsequent to retirement, he taught high-school physics at [[Germantown Academy]] (1995–98) and at [[Germantown Friends School]] (2000-2001).

In 1958, after a year's leave from Indiana University in Los Alamos, Ken took up new faculty duties at [[Brandeis University]], where he continued research, supervision of graduate students, and, for the first time, taught introductory physics. He also served as department chair at Brandeis, 1963-64. In 1964 he was recruited by the soon-to-open new campus of the [[University of California at Irvine]] as its first physics chair.

In 1970, for family reasons, Ken left Irvine for the [[University of Massachusetts Boston]], where he was a professor. In 1975, he accepted the job of President of the [[New Mexico Institute of Mining and Technology]] (NM Tech). He spent 7 years at New Mexico Tech, resigning after receiving a vote of no confidence from the faculty. Ken then became [[Executive Vice President]] of the [[University of Maryland System]]. That job lasted for slightly more than a year, during 1982-83, before Ken took his first non-academic job as President of Molecular Biophysics Technology, Inc. (MBT) in [[Philadelphia]].<ref name="aip2">[http://www.aip.org/history/ohilist/24701_2.html Interview of Kenneth W. Ford by Alexei Kojevnikov on November 22, 1997, Niels Bohr Library & Archives, American Institute of Physics, College Park, MD USA]</ref>

When MBT's research results failed to measure up to expectations and the company had to shut down, Ken took a position as Education Officer of the [[American Physical Society]]. Then, in 1987, he became the Director of the [[American Institute of Physics]] (AIP) and later helped to shepard its move from [[New York City]] (to which he'd been commuting from Philadelphia) to [[College Park, MD]]. Ken's retirement from AIP in 1993, at age 67, coincided with the organization's move (along with other physics organizations) to College Park.<ref name="aip2"></ref>

Ken has written ten books (counting one three-volume work as three books), one of them with a co-author—five before his retirement and five after. His first book, ''The World of Elementary Particles'', written in 1961-62 and published in 1963, did well enough and was satisfying enough to encourage him to write more. The thick textbook ''Basic Physics'' followed in 1968 and the three volumes of ''Classical and Modern Physics'' in 1972-74. Following his retirement and while teaching at [[Germantown Academy]], he joined with [[John Archibald Wheeler|John Wheeler]] to write Wheeler's autobiography, ''Geons, Black Holes, and Quantum Foam'', published in 1998. In 1999, This book won an [[American Institute of Physics]] [[Science Writing Award#Past Winners: Scientist|Science Writing Prize]]. There followed ''The Quantum World'' in 2004, ''In Love with Flying'' (a memoir) in 2007, ''101 Quantum Questions'' in 2011, and ''Building the H Bomb'' in 2015.

===Post-retirement===
Since officially retiring, Ken has done some consulting work, worked for the [[David and Lucile Packard Foundation]], tutored students, both in person and online, taught high-school physics, and, as noted above, written five books. He lives with his wife Joanne in Philadelphia.

==Personal==
Ken married Karin Stehnike on August 27, 1953 and fathered two children; Paul Thomas Ford (1957) and Sarah Elizabeth Ford (1958). They divorced in 1961.
Ken married Joanne Baumunk on June 9, 1962, gained one stepdaughter, Nina Tannenwald (1959), and fathered four more children: Caroline Amanda Ford (now Caroline Richards) (1963), Adam Baumunk Ford (1964), Jason Lawrence Ford (1966), and Lucas Wheeler (now Star Lucia) Ford (1968). He has 13 grandchildren and three step-grandchildren.

==Selected honors==
* 1944 [[Westinghouse Science Talent Search]] winner
* 1944 Prize for General Excellence, [[Phillips Exeter Academy]]
* 1948 [[Phi Beta Kappa]], [[Sigma Xi]], [[Harvard College]]
* 1955-56 [[Fulbright Fellow]], [[Max Planck Institut]], [[Göttingen, Germany]]
* 1961-62 [[National Science Foundation]] (NSF) Senior Postdoctoral Fellow, [[Imperial College]], [[London]], and [[M.I.T.]]
* 1976 Distinguished Service Citation, [[American Association of Physics Teachers]]
* 2006 [[Oersted Medal]], [[American Association of Physics Teachers]]
* Fellow, [[American Physical Society]], [[American Association for the Advancement of Science]], and [[American Association of Physics Teachers]]

==Selected bibliography==
* ''The World of Elementary Particles'' (Blaisdell Publishing Co., 1963). Translated into Italian, German, and Russian. ([[Public Library of Science|Library of Science]] selection in the United States, science writing prize in Italy.)
* ''Basic Physics'' (Blaisdell, 1968). Introductory text for nonscience students.
* ''Classical and Modern Physics'' (Xerox College Publishing, 1972–74). Introductory text for students of science and engineering. 3 volumes.
* ''Geons, Black Holes, and Quantum Foam: A Life in Physics'', with [[John Archibald Wheeler]] (W. W. Norton, 1998). Wheeler’s autobiography. Translated into Chinese and Greek. (1999 [[American Institute of Physics]] [[Science Writing Award#Past Winners: Scientist|Science Writing Prize]].)
* ''The Quantum World: Quantum Physics for Everyone'' (Harvard University Press, 2004). Translated into several languages. Paperback with “Quantum Questions,” 2005. Answer Manual available.
* ''In Love With Flying'' (H Bar Press, 2007). Memoir on 50 years of flying [[light aircraft|small planes]] and [[glider (sailplane)|gliders]].
* ''101 Quantum Questions: What You Need to Know About the World You Can’t See'' (Harvard University Press, 2011). Translated into several languages.
* ''Building the H Bomb: A Personal History'' (World Scientific, 2015).

==External links==
* [http://www.kennethwford.com/ Personal Website]
* Kenneth W. Ford at [http://www.aip.org/history/historymatters/KenFord.htm aip.org]
* [http://student.societyforscience.org/science-talent-search-1944 Westinghouse Science Talent Search Winners from 1944]
* [http://www.scientificamerican.com/podcast/episode.cfm?id=58AD0DA4-F12F-B68C-02506A9B55CA95B6 Scientific American interview with Kenneth Ford on John Wheeler's passing]
* Kenneth W. Ford's Author Page on [http://www.amazon.com/-/e/B001HCZQMM/ Amazon.com]

==References==
{{Reflist}}

{{Authority control}}
{{DEFAULTSORT:Ford, Kenneth W.}}
[[Category:1926 births]]
[[Category:People from West Palm Beach, Florida]]
[[Category:American physicists]]
[[Category:Harvard University alumni]]
[[Category:Princeton University alumni]]
[[Category:Living people]]
[[Category:University of Michigan alumni]]
[[Category:Fulbright Scholars]]