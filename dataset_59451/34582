{{good article}}
{{Infobox planet
 | minorplanet     = yes
 | background      = #FFFFC0
 | name            = 10 Hygiea
 | symbol          = [[File:10 Hygiea Astronomical Symbol.svg|12px|The traditional astronomical symbol for 10 Hygiea|alt=A Rod of Asclepius (an ancient Greek symbol associated with medicine)]]
 | image           = [[File:10 Hygiea (Lightcurve Inversion).png|275px]]
 | caption         = {{longitem|[[Light curve]]-based 3D-model of 10 Hygiea|style=padding: 5px 0; line-height: 1.3em;}}
 | discovery_ref   = &thinsp;<ref name="jpldata" />
 | discoverer      = [[Annibale de Gasparis|A. de Gasparis]]
 | discovered      = 12 April 1849
 | pronounced      = {{IPAc-en|h|aɪ|ˈ|dʒ|iː|ə}}
 | adjectives      = Hygiean
 | mp_name       = (10) Hygiea
 | alt_names       = A900 GA
 | named_after     = [[Hygieia]]&thinsp;<ref name="springer" />
 | mp_category     = [[Asteroid belt|Main belt]] ([[Hygiea family]])
 | orbit_ref       = &thinsp;<ref name="jpldata" />
 | epoch           = 27 June 2015 ([[Julian day|JD]] 2457200.5)
 | uncertainty     = 0
 | observation_arc = {{nowrap|163.54 yr (59,732 days)}}
 | aphelion        = 3.5024 [[Astronomical unit|AU]]
 | perihelion      = 2.7817 AU
 | semimajor       = 3.1421 AU
 | eccentricity    = 0.1146
 | period          = 5.57 [[Julian year (astronomy)|yr]] (2034.3 days)
 | mean_anomaly    = 264.46[[degree (angle)|°]]
 | inclination     = 3.8377°
 | asc_node        = 283.41°
 | arg_peri        = 312.10°
 | surface_area    = {{convert|837080.744|km2|sqmi|abbr=on}}
 | avg_speed       = 16.76 km/s
 | p_orbit_ref = &thinsp;<ref name=Hygiea-POE>{{cite web
  |title=AstDyS-2 Hygiea Synthetic Proper Orbital Elements
  |publisher=Department of Mathematics, University of Pisa, Italy
  |url=http://hamilton.dm.unipi.it/astdys/index.php?pc=1.1.6&n=10
  |accessdate=2011-10-01}}</ref>
| p_semimajor = 3.1417827
| p_eccentricity = 0.1356
| p_inclination = 5.1039°
| p_mean_motion = 64.621
| perihelion_rate = 128.543
| node_rate = −96.902
| dimensions=530×407×370 ±7 km<ref name=Baer/><br>431 km ([[Geometric mean|mean]])
| mass=(8.67 ±0.15){{e|19}} kg<ref name=Baer>
{{cite web
  |date=2010
  |title=Recent Asteroid Mass Determinations
  |publisher=Personal Website
  |author=Jim Baer
  |url=http://home.earthlink.net/~jimbaer1/astmass.txt
  |accessdate=2011-09-02
}}</ref>
| density=2.08 ± 0.10 g/cm³<ref name=Baer/>
| surface_grav={{convert|0.091|m/s2|g0}}
| escape_velocity={{convert|0.21|km/s|km/h}}
| rotation=27.623 h (1.15 d)<ref name=jpldata/>
| spectral_type=[[C-type asteroid|C-type]]<ref name=jpldata/>
| magnitude=9.0<ref name=AstDys-Hygiea>{{cite web |title=AstDys (10) Hygiea Ephemerides |publisher=Department of Mathematics, University of Pisa, Italy |url=http://hamilton.dm.unipi.it/astdys/index.php?pc=1.1.3.1&n=10&oc=500&y0=2095&m0=6&d0=27&h0=00&mi0=00&y1=2095&m1=6&d1=28&h1=00&mi1=00&ti=1.0&tiu=days |accessdate=2010-06-26}}</ref><!-- Horizons 2011-May-14 --> to 11.97
| abs_magnitude=5.43<ref name=jpldata/>
| albedo=0.0717&nbsp;<ref name=jpldata/>
| angular_size = 0.321″ <!-- Horizons 2078-May-20 --> to 0.133″
| single_temperature=≈164 [[kelvin|K]]<br>''max:'' 247 K (−26°C) <ref name="Lim2005">{{cite journal| author=L.F. Lim | title=Thermal infrared (8–13 µm) spectra of 29 asteroids: the Cornell Mid-Infrared Asteroid Spectroscopy (MIDAS) Survey| journal= Icarus|volume= 173| issue=2|page= 385|date=2005| doi=10.1016/j.icarus.2004.08.005| last2=McConnochie| first2=T| last3=Belliii| first3=J| last4=Hayward| first4=T| bibcode=2005Icar..173..385L}}</ref>
}}

'''10 Hygiea''' is the [[List of notable asteroids|fourth-largest asteroid]] in the [[Solar System]] by volume and mass, and it is located in the [[asteroid belt]]. With somewhat oblong diameters of {{convert|350|–|500|km}} (217–310&nbsp;miles) and a mass estimated to be 2.9% of the total mass of the belt,<ref>"Mass of 10 Hygiea" 0.445 / [http://iau-comm4.jpl.nasa.gov/EPM2004.pdf "Mass of Mbelt"] 15 = 0.0296</ref> it is the largest of the class of dark [[C-type asteroid]]s with a [[carbon]]aceous surface.{{citation needed|date=July 2016}}

==Observation==
Despite its size, Hygiea appears very dim when observed from [[Earth]]. This is due to its dark surface and larger-than-average distance from the [[Sun]]. For this reason, many smaller asteroids were observed before [[Annibale de Gasparis]] discovered Hygiea on 12 April 1849. At most oppositions, Hygiea has a [[apparent magnitude|magnitude]] that is four magnitudes dimmer than [[4 Vesta|Vesta's]], and observing it typically requires at least a {{convert|100|mm|in|adj=on|0}} [[telescope]]. However, while at a [[perihelion|perihelic opposition]], it can often be observed just with 10x50 [[binoculars]].{{citation needed|date=July 2016}}

==Discovery and name==
On 12 April 1849, in [[Naples]], Italy, astronomer [[Annibale de Gasparis]] (age 29) discovered Hygiea.<ref>{{cite journal| title=Comparison of Theory with Observation for the Minor planets 10 Hygiea and 175 Andromache with Respect to Perturbations by Jupiter|author= A. O. Leuschner|journal=Proceedings of the National Academy of Sciences of the United States of America|volume= 8|issue= 7 |date=1922-07-15| pages= 170–173|publisher= National Academy of Sciences| doi=10.1073/pnas.8.7.170| pmid=16586868| pmc=1085085 |bibcode = 1922PNAS....8..170L }}</ref> It was the first of his nine asteroid discoveries. The director of the Naples observatory, [[Ernesto Capocci]], named the asteroid. He chose to call it ''Igea Borbonica'' ("[[House of Bourbon|Bourbon]] Hygieia") in honor of the ruling family of the [[Kingdom of the Two Sicilies]] where Naples was located.<ref name=name/>

In 1852, [[John Russell Hind]] wrote that "it is universally termed ''Hygiea'', the unnecessary appendage 'Borbonica' being dropped" (as well as the final "ia" in favor of just "a").<ref name=name>{{cite book| author=John Russell Hind| title=The Solar System:  Descriptive Treatise Upon the Sun, Moon, and Planets, Including an Account of All the Recent Discoveries |page= 126 |date= 1852| publisher=G. P. Putnam}}</ref> The name comes from [[Hygieia]], the [[Greek mythology|Greek goddess]] of health, daughter of [[Asclepius]] ([[Aesculapius]] for the Romans).<ref name=Barucci>{{cite journal|doi=10.1006/icar.2001.6775|title=10 Hygiea: ISO Infrared Observations|date=2002|author=Barucci, M|journal=Icarus|volume=156|issue=1|page=202|bibcode=2002Icar..156..202B}}</ref> The name was occasionally misspelled Hygeia in the 19th century, for example in the ''[[Monthly Notices of the Royal Astronomical Society]]''.<ref>{{cite journal | bibcode=1850MNRAS..10..162H | title=Observations of Hygiea | author=Hartnup, J. | journal=Monthly Notices of the Royal Astronomical Society| volume= 10 |page=162|date=June 1850 | doi=10.1093/mnras/10.8.162b}}</ref>

==Physical characteristics==
Based on spectral evidence, Hygiea's surface is thought to consist of primitive [[carbonaceous]] materials similar to those found in [[carbonaceous chondrite]] [[meteorite]]s. [[Aqueous]] alteration products have been detected on its surface, which could indicate the presence of water ice in the past which was heated sufficiently to melt.<ref name=Barucci/> The primitive present surface composition would indicate that Hygiea had not been melted during the early period of Solar System formation,<ref name=Barucci/> in contrast to other large [[planetesimal]]s like [[4 Vesta]].{{citation needed|date=July 2016}}

Hygiea is the main member of the [[Hygiea family]] and contains almost all the mass  (well over 90%) in this family. It is the largest of the class of dark [[C-type asteroid]]s that are dominant in the outer [[asteroid belt]]—which lie beyond the [[Kirkwood gap]] at 2.82 AU. Hygiea appears to have a noticeably [[oblate spheroid]] shape, with an average diameter of 444 ± 35&nbsp;km and a semimajor axis ratio of 1.11.<ref name=Barucci/> This is much more than for the other objects in the "[[List of exceptional asteroids#Largest by mass|big four]]"—[[2 Pallas]], [[4 Vesta]] and the [[dwarf planet]] [[Ceres (dwarf planet)|Ceres]]. Aside from being the smallest of the four, Hygiea, like Ceres, has a relatively low density, which is more comparable to the icy satellites of [[Jupiter]] or [[Saturn]] than to the [[terrestrial planet]]s or the stony asteroids.{{citation needed|date=July 2016}}

Although it is the largest body in its region, due to its dark surface and larger-than-average distance from the Sun, Hygiea appears very dim when observed from Earth. In fact, it is the third dimmest of the first twenty-three asteroids discovered, with only [[13 Egeria]] and [[17 Thetis]] having lower mean opposition [[Magnitude (astronomy)|magnitude]]s.<ref name=brightestasteroids>{{cite journal|title=The Brightest Asteroids|journal=The Jordanian Astronomical Society|author=Moh'd Odeh|url=http://jas.org.jo/ast.html|accessdate=2008-05-21|archiveurl=https://web.archive.org/web/20080511115437/http://www.jas.org.jo/ast.html|archivedate=11 May 2008<!--Added by DASHBot-->}}</ref> At most oppositions, Hygiea has a [[apparent magnitude|magnitude]] of around +10.2,<ref name=brightestasteroids/> which is as much as four orders fainter than Vesta, and observation calls for at least a {{convert|4|in|mm|sing=on}} [[telescope]] to resolve.<ref name=telescope>{{cite web|date=2004|title=What Can I See Through My Scope?|publisher=Ballauer Observatory|url=http://www.allaboutastro.com/Articlepages/whatcanisee.html|accessdate=2008-07-20|archiveurl=https://web.archive.org/web/20130622002417/http://www.allaboutastro.com/Articlepages/whatcanisee.html|archivedate=2013-06-22}}</ref> However, at a perihelic opposition, Hygiea can reach +9.1 and may just be resolvable with 10x50 [[binoculars]], unlike the next two largest asteroids in the asteroid belt, [[704 Interamnia]] and [[511 Davida]], which are always beyond binocular visibility.{{citation needed|date=July 2016}}

At least 5 [[star|stellar]] [[occultation]]s by Hygiea have been tracked by Earth-based astronomers,<ref>{{cite web|url=http://www.lpi.usra.edu/books/AsteroidsIII/pdf/3008.pdf|title=Asteroid Masses and Densities|author=James L. Hilton|work=U.S. Naval Observatory|publisher=Lunar and Planetar Institute|format=PDF|accessdate=2008-08-26|archiveurl=https://web.archive.org/web/20080819191809/http://www.lpi.usra.edu/books/AsteroidsIII/pdf/3008.pdf|archivedate=19 August 2008<!--Added by DASHBot-->}}</ref> but all with few independent observers so that little was learned of its shape. The [[Hubble Space Telescope]] has resolved the asteroid and ruled out the presence of any orbiting companions larger than about {{convert|16|km|mi}} in diameter.<ref name="Storrs1999">{{cite journal|author=A. Storrs|title=Imaging Observations of Asteroids with HST|journal=Bulletin of the American Astronomical Society|volume=31|page=1089|date=1999|bibcode=1999DPS....31.1103S}}</ref>

==Orbit and rotation==
Generally, Hygiea's properties are the most poorly known out of the "big four" objects in the asteroid belt. Its orbit is much closer to the plane of the [[ecliptic]] than those of Ceres, Pallas or Interamnia,<ref name=Barucci/> but is less circular than Ceres or Vesta with an eccentricity of around 12%.<ref name=jpldata /> Its [[Apsis|perihelion]] is at a quite similar longitude to those of Vesta and Ceres, though its ascending and descending nodes are opposite to the corresponding ones for those objects. Although its perihelion is extremely close to the mean distance of Ceres and Pallas, a collision between Hygiea and its larger companions is impossible because at that distance they are always on opposite sides of the ecliptic.  In 2056, Hygiea will pass 0.025AU from Ceres, and then in 2063, Hygiea will pass 0.020AU from Pallas.<ref name=jpl-close>{{cite web |date=2009-11-27 |title=JPL Close-Approach Data: 10 Hygiea |url=http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=10;cad=1#cad |accessdate=2010-03-02}}</ref> At [[Apsis|aphelion]] Hygiea reaches out to the extreme edge of the asteroid belt at the perihelia of the [[Hilda family]] which is in 3:2 resonance with Jupiter.<ref>{{cite web|url=http://neopage.nm.ru/ENG/GENERAL/DATA/hil-a.pdf|title=Some Peculiarities in the Hildas' Motion| publisher=Central (Pulkovo) Astronomical Observatory of Russian Academy of Sciences|author=L’vov V.N.|author2=Smekhacheva R.I.|author3=Smirnov S.S.|author4=Tsekmejster S.D.|format=PDF|accessdate=2008-09-07}}</ref> Hygiea is used by the [[Minor Planet Center]] to calculate [[Perturbation (astronomy)|perturbations]].<ref name="Perturbing"/>

Hygiea is an unusually slow rotator, taking 27 hours and 37 minutes for a revolution,<ref name=jpldata/> whereas 6 to 12 hours are more typical for large asteroids. Its direction of rotation is not certain at present, due to a twofold ambiguity in lightcurve data that is exacerbated by its long rotation period—which makes single-night telescope observations span at best only a fraction of a full rotation—but it is believed to be [[Retrograde motion|retrograde]].<ref name=Barucci/>  Lightcurve analysis indicates that Hygiea's pole points towards either [[ecliptic coordinate system|ecliptic coordinates]] (β, λ) = (30°, 115°) or (30°, 300°) with a 10° uncertainty.<ref name="Kaasalainen2002">{{cite journal| author=M. Kaasalainen |title=Models of Twenty Asteroids from Photometric Data| journal=Icarus| volume= 159| issue=2| page= 369| date=2002| format= PDF| url= http://www.rni.helsinki.fi/~mjk/IcarPIII.pdf| doi=10.1006/icar.2002.6907| accessdate=2009-06-23| bibcode=2002Icar..159..369K}}</ref>  This gives an [[axial tilt]] of about 60° in both cases.{{citation needed|date=July 2016}}

==Gallery==
{{Gallery
|title=Hygiea Characteristics
|width=160 | height=170
|align=center
|File:Moon and Asteroids 1 to 10.svg|Sizes of the first 10 asteroids discovered profiled against [[Earth]]'s [[Moon]]. Hygiea is furthest right.|alt1=Sizes of the first 10 asteroids discovered profiled against [[Earth]]'s [[Moon]]. Hygiea is furthest right.
|File:Iau dozen.jpg|The [[IAU definition of planet#Draft proposal|IAU 2006 draft proposal]] listed Hygiea as a candidate planet.<ref name=IAU-draft2006>{{cite web
 |title=The Path to Defining Planets
 |author=O. Gingerich
 |work=Harvard-Smithsonian Center for Astrophysics and IAU EC Planet Definition Committee chair
 |date=2006
 |url=http://astro.cas.cz/nuncius/nsiii_03.pdf
 |accessdate=2007-03-13
 |format=PDF|archiveurl=https://web.archive.org/web/20070306093519/http://astro.cas.cz/nuncius/nsiii_03.pdf|archivedate=6 March 2007<!--Added by DASHBot-->}}</ref>|alt2=The [[IAU definition of planet#Draft proposal|IAU 2006 draft proposal]] listed Hygiea as a candidate planet.
|File:Hygeia.GIF|A [[rotating frame]] depiction of asteroid Hygiea's orbital motion relative to Jupiter; the latter (purple loop at upper right) is held nearly stationary.|alt3=A [[rotating frame]] depiction of asteroid Hygiea's orbital motion relative to Jupiter; the latter (purple loop at upper right) is held nearly stationary.
|File:AnimatedOrbitOf10Hygiea.gif|Animated orbit of Hygiea relative to the orbits of the terrestrial planets and Jupiter.|alt4=Animated orbit of Hygiea relative to the orbits of the terrestrial planets and Jupiter.
}}

==See also==
* [[Planet#Objects formerly considered planets|Former classification of planets]]

== References ==
{{reflist|2
| refs =

<ref name="Perturbing">{{cite web
  |title=Perturbing Bodies
  |publisher=[[Minor Planet Center]]
  |url=http://www.minorplanetcenter.net/iau/info/Perturbers.html
  |accessdate=2013-04-18}}</ref>

<ref name="jpldata">{{cite web
  |type=2012-12-09 last obs.
  |title=JPL Small-Body Database Browser: 10 Hygiea
  |url=http://ssd.jpl.nasa.gov/sbdb.cgi?sstr=2000010
  |publisher=Jet Propulsion Laboratory
  |accessdate=2015-10-01}}</ref>

<ref name="springer">{{cite book
  |url=http://link.springer.com/referenceworkentry/10.1007/978-3-540-29925-7_11
  |title=Dictionary of Minor Planet Names – (10) Hygiea
  |last=Schmadel |first=Lutz D.
  |publisher=Springer Berlin Heidelberg
  |page=16
  |date=2007
  |isbn=978-3-540-29925-7
  |accessdate=2015-10-01}}</ref>

}} <!-- end of reflist -->

==Bibliography==

{{Empty section|date=April 2017}}

== External links ==
* [http://www.rni.helsinki.fi/~mjk/IcarPIII.pdf Shape model deduced from lightcurve]
* [http://orbitsimulator.com/gravity/articles/ceres.html A simulation of the orbit of Hygiea]
* [http://ssd.jpl.nasa.gov/horizons.cgi?find_body=1&body_group=sb&sstr=10 JPL Ephemeris]
* {{cite web |title=Elements and Ephemeris for (10) Hygiea |publisher=Minor Planet Center |url=http://scully.cfa.harvard.edu/cgi-bin/returnprepeph.cgi?d=b2011&o=00010 | accessdate= 26 May 2011 <!--Added by DASHBot-->}} (displays [[Elongation (astronomy)|Elong]] from Sun and [[apparent magnitude|V mag]] for 2011)
* {{JPL small body}}

{{Minor planets navigator|9 Metis|number=10|11 Parthenope}}
{{Small Solar System bodies}}
{{Large asteroids}}

{{DEFAULTSORT:000010}}
[[Category:Hygiea asteroids]]
[[Category:Numbered minor planets]]
[[Category:Minor planets named from Greek mythology|Hygiea]]
[[Category:Discoveries by Annibale de Gasparis|Hygiea]]
[[Category:Named minor planets|Hygiea]]
[[Category:Former dwarf planet candidates]]
[[Category:C-type asteroids (Tholen)]]
[[Category:C-type asteroids (SMASS)]]
[[Category:1840s in science|18490412]]
[[Category:Astronomical objects discovered in 1849|18490412]]