{{italictitle}}
{{Infobox Journal
| title        = ACS Medicinal Chemistry Letters
| cover        = 
| editor       = Dennis C. Liotta
| discipline   = [[Medicinal Chemistry]]
| language     = English
| abbreviation = ACS Med. Chem. Lett.
| publisher    = [[American Chemical Society]]
| country      = USA
| frequency    = Monthly
| history      = 2009–present
| openaccess   = Via author payment only
| impact       = 3.355
| impact-year  = 2015
| website      = http://pubs.acs.org/journal/amclct
| link1        = 
| link1-name   = 
| link2        = 
| link2-name   = 
| RSS          = 
| atom         = 
| JSTOR        = 
| OCLC         = 
| LCCN         = 
| CODEN        = amclct
| ISSN         = 1948-5875
| eISSN        =  
}}

'''''ACS Medicinal Chemistry Letters''''' is a [[Peer review|peer-reviewed]] [[academic journal]] in the field of [[medicinal chemistry]]. Founded in 2009, this online journal is published monthly by the [[American Chemical Society]]. 

The Editor-in-Chief is [[Dennis C. Liotta]] ([[Emory University]], Atlanta, Georgia, USA).<ref>[http://pubs.acs.org/page/amclct/profile.html ''ACS Medicinal Chemistry Letters'': Editor Profile] (accessed 12 December 2011)</ref> 

The journal publishes short articles ("letters") on experimental or theoretical results of exceptional timeliness in all aspects of pure and applied medicinal chemistry and its extension into pharmacology. The journal publishes studies that range from compound design to optimization, biological evaluation, drug delivery, and pharmacology. Specific areas include, but are not limited to:

* Identification, synthesis, and optimization of lead biologically active compounds and drugs
* Biological characterization of new chemical entities in the context of drug discovery
* Computational, cheminformatics, and structural studies for the identification or SAR analysis of bioactive molecules, ligands and their targets, etc.
* Novel and improved methodologies, including radiation biochemistry, with broad application to medicinal chemistry
* Discovery technologies for biologically active compounds from both synthetic and natural (plant and other) sources
* Pharmacokinetic/pharmacodynamic studies that address mechanisms underlying drug disposition and response
* Pharmacogenetic and pharmacogenomic studies used to enhance drug design and the translation of medicinal chemistry into the clinic
* Mechanistic drug metabolism and regulation of metabolic enzyme gene expression<ref>[http://pubs.acs.org/page/amclct/profile.html ''ACS Medicinal Chemistry Letters'': About the Journal] (accessed 12 December 2011)</ref>

==References==
{{reflist}}

==External links==
*[http://pubs.acs.org/journal/amclct ''ACS Medicinal Chemistry Letters'' website]

[[Category:American Chemical Society academic journals]]
[[Category:Biochemistry journals]]
[[Category:English-language journals]]
[[Category:Medicinal chemistry journals]]
[[Category:Publications established in 2009]]