{{Infobox person
|name = Paul Shoup
|image = Paul Shoup.jpg
|image_size = 200 px
|caption = Paul Shoup in Los Altos circa 1920
|birth_name = Paul Shoup<ref name="oac.cdlib">{{cite web|url=http://www.oac.cdlib.org/findaid/ark:/13030/tf5c6004f9/ |title=Guide to the Paul Shoup Papers, 1928-1946 |publisher=Oac.cdlib.org |date= |accessdate=2011-10-05}}</ref>
|birth_date = {{Birth-date|January 8, 1874}}<ref name="Stateof">State of California. California Death Index, 1940-1997. Sacramento, CA, USA: State of California Department of Health Services, Center for Health Statistics</ref>
|birth_place = [[San Bernardino, California]]<ref name="oac.cdlib" />
|death_date = {{death-date and age|July 30, 1946|January 8, 1874}}<ref name="Stateof" /><ref name="bare_url">{{cite web|url=http://historyengine.richmond.edu/episodes/view/5106 |title=History Engine: Tools for Collaborative Education and Research &#124; Episodes |publisher=Historyengine.richmond.edu |date= |accessdate=2011-10-05}}</ref>
|death_place = [[Los Angeles, California]]<ref name="bare_url" />
|death_cause =
|resting_place = [[Alta Mesa Memorial Park]], [[Palo Alto, California]]<ref>{{cite web|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GSln=shoup&GSfn=paul&GSbyrel=all&GSdyrel=all&GSst=6&GScnty=225&GScntry=4&GSob=n&GRid=63460306&df=all& |title=Paul Shoup (1874 - 1946) - Find A Grave Memorial |publisher=Findagrave.com |date= |accessdate=2011-10-05}}</ref>
|resting_place_coordinates = Latitude: 37.39830, Longitude: -122.12750<ref>{{cite web|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=cr&CRid=7837 |title=Alta Mesa Memorial Park |publisher=Find A Grave |date= |accessdate=2011-10-05}}</ref>
|residence = [[Los Altos, California]] 
|nationality = [[United States|American]] 
|other_names =
|known_for = [[Southern Pacific Transportation Company|Southern Pacific Railroad]] Executive,<ref name="TimeMagazine">Time Magazine, July 25, 1932</ref><ref name="time">{{cite news|author=Monday, July 25, 1932 |url=http://www.time.com/time/magazine/article/0,9171,744040-1,00.html |title=Business: Great Shoes Shuffled |publisher=TIME |date=1932-07-25 |accessdate=2011-10-05}}</ref> Founder of [[Los Altos, California|Los Altos]],<ref>[http://www.mercurynews.com/ci_9735047?source=pkg Los Altos: 'The loveliest place' that never grew up - San Jose Mercury News]. Mercurynews.com. Retrieved on 2013-07-21.</ref> [[Stanford University]] Trustee<ref name="bare_url" />
|education = [[San Bernardino High School]] graduate 1891<ref name="oac.cdlib" />
|alma_mater =
|employer = [[Southern Pacific Transportation Company|Southern Pacific Railroad]]<ref name="TimeMagazine" /><ref name="time" />
|occupation = [[Rail transport|Railroad]] [[Senior management|Executive]]<ref name="TimeMagazine" /><ref name="time" />
|home_town =
|title = Vice-Chairman<ref name="TimeMagazine" /><ref name="time" />
|salary = $90,000 per year before promotion to Vice-Chairman in 1932<ref name="TimeMagazine" /><ref name="time" />(${{Formatprice|{{Inflation|US|90000|1932|{{CURRENTYEAR}}|r=0}}}} as of {{CURRENTYEAR}}) {{Inflation-fn|US}}
|networth =
|height =
|weight =
|term =
|predecessor =
|successor =
|party = Republican<ref name="Guideto">Guide to the Paul Shoup Papers [1928-1946], 3.</ref>
|boards =
|religion =
|spouse = Rose Wilson Shoup<ref>Who's Who on the Pacific Coast, 1913</ref>
|partner = 
|children = Carl Sumner, Jack Wilson, and Louise<ref name="bare_url" />
|parents = Timothy and Sarah Sumner Shoup
|relatives = Siblings: Twins Carl and Guy Shoup, Brother Fred, Sister Faith
|signature = Paul_Shoup_Signature.tiff
|website =
|footnotes =
}}

'''Paul Shoup''' (January 8, 1874 – July 30, 1946)<ref name="Stateof" /> was an [[United States|American]] businessman, president and later vice-chairman of the [[Southern Pacific Transportation Company|Southern Pacific Railroad]] in the 1920s and 1930s,<ref name="TimeMagazine" /><ref name="time" /> a founding board member of the [[Stanford Graduate School of Business|Stanford University School of Business]],<ref>''Stanford University 1916-1941'', pg 79. J. Pearce Mitchell, 1958</ref> and founder of the community of [[Los Altos, California]].<ref>{{cite web|url=http://www.losaltosonline.com/index.php?option=com_content&task=view&id=53&Itemid=51 |title=''Los Altos Town Crier'' |publisher=Losaltosonline.com |date= |accessdate=2011-10-06}}</ref>

==Family==
He was the third of five children of Timothy and Sarah Sumner Shoup. His siblings included two older twin brothers, Carl and Guy, a younger brother, Fred, and a younger sister, Faith. Paul’s father was a well-respected attorney in San Bernardino who relocated the family from Iowa in 1872. After Timothy’s death in 1877, Sarah moved back to Iowa with her children. Carl died in 1898 while still in his early 20s.<ref>Robert J. Burdette, American Biography and Genealogy (Chicago: The Lewis Publishing Company, 1914), 829-830.</ref> Guy and Fred joined Paul in careers with Southern Pacific, with Guy becoming an influential company attorney and Fred working for the Pacific Electric Railway Company in Los Angeles. Paul married Rose Wilson in 1900 in San Francisco<ref name="HarperFranklin">Harper, Franklin, editor. Who’s Who on the Pacific Coast. Los Angeles, California: Harper Publishing Co., 1913.</ref> and eventually settled in Los Altos with their three children – [[Carl Shoup|Carl Sumner Shoup]], Jack Wilson Shoup, and Louise Shoup.<ref name="Guideto" /> Son Carl went on to become an economist, responsible for drafting the post-World War II [[Japan]]ese tax structure, forming the modern [[Value Added Tax]]; he also taught economics as a professor at Columbia University.<ref>David Cay, “Carl S. Shoup, 97; Shaped Japan’s Tax Code,” ''New York Times'', 31 March 2000, Obituary.</ref> Brother Guy became a business partner with Paul in various Los Altos-focused businesses.

==Southern Pacific Career==
While he wrote for various magazines during his early life, as a contributing writer to the ''[[The Sun (New York)|New York Sun]]'', ''[[Overland Monthly]]'', ''[[Black Cat (magazine)|Black Cat]]'', ''[[Illustrated Monthly]]'', and ''[[Sunset Magazine]]'',<ref>Don McDonald and Los Altos History Museum, Images of American Early Los Altos and Los Altos Hills (South Carolina: Arcadia, 2010), 7-8,21-24.</ref> he turned from it after high school to start a career in the railroad industry by becoming a clerk in the mechanical department of the [[Atchison, Topeka & Santa Fe Railroad|Santa Fe Railroad]] in San Bernardino.<ref name="HarperFranklin" /> This job was short lived, as he moved to the Southern Pacific Railroad in 1891 as a ticket clerk in San Bernardino.<ref name="SanFrancisco">San Francisco Call, “Paul Shoup Wins High Preferment,” 20 December 1908.</ref> While there, he began his training in managing a railroad. In between shifts, he tutored individuals in mathematics, and learned telegraphy and stenography, and he continued to write, submitting short pieces to eastern magazines.<ref name="SanFrancisco" />

From San Bernardino, Shoup was given a position in San Francisco in the Passenger Department, which began his personal relationship with the San Francisco Bay area. It is there that he supposedly began creating promotional materials for local fruit and agricultural products that were distributed by Southern Pacific on the east coast.<ref name="SanFrancisco" />

On April 11, 1906, just one week before the [[1906 San Francisco earthquake]], it was announced that Shoup was being transferred back to San Francisco as part of his promotion to Assistant General Freight and Passenger Agent for the region.<ref>Los Angeles Herald, “Railway World: Paul Shoup is Transferred,” 11 April 1906.</ref> This placement put him at the forefront of the rebuilding of Southern Pacific’s northern California interests in the aftermath of the earthquake and fire. 

As part of this promotion, Paul was unofficially in charge of the development of the electric lines in the City of Los Angeles. Today, a street in Los Angeles today is named for Paul Shoup in recognition of his contributions. Building on Paul’s experience with electric interurban service in San Jose, Southern Pacific purchased Pacific Electric Railway Company in 1910 and made Paul Vice President of the new acquisition. He was given the responsibility for overseeing the complete integration and conversion of the newly combined interurban system from steam to electric trains.<ref>Los Angeles Herald, “Pacific Electric to Connect with Los Angeles Line,” 13 November 1910.</ref> In 1920 he was made Vice President of Southern Pacific and assistant to the President. In 1925, he was again promoted, this time to Executive Vice President of the company.<ref name="Guideto" /> Four years later he was made President of Southern Pacific, a post he held until his retirement in 1938. Other executive positions included Vice Chairman of the Board (1932)<ref name="TimeMagazine" /> and President of the Pacific Electric Railway Company (1933).

==Activities outside of Southern Pacific Railroad==
After Shoup retired in 1938, he became President of Southern Californians,<ref name="bare_url" /> later renamed the [[Employers Group|Merchants and Manufacturers Association]], a pro-business, anti-labor political group that was highly influential in Los Angeles and Southern California.<ref>Guy Finney, Angel City in Turmoil (American State Press, 1945), 184.</ref> His involvement in politics continued as well, including a strong supportive role in the presidential campaign of Republican [[Alf Landon|Alfred Landon]] against [[Franklin D. Roosevelt]] in 1935.<ref name="Guideto" />

Paul Shoup was one of the first to ascend [[Table Mountain (Tulare County, California)|Table Mountain]] in California with his brother Fred Shoup and Gilbert Hassell on August 25, 1908.<ref>Farquhar, Francis P. (1926). Place Names of the High Sierra. San Francisco: Sierra Club.</ref>

Beyond Southern Pacific, Paul Shoup was very active in other social and business interests. Early in his career these interests focused on the development of Los Altos as a residential commuter community. He became a founding member of the San Jose Chamber of Commerce and worked to promote the Santa Clara Valley as an ideal place to live and establish business interests. He joined all the influential social clubs of the time, including the [[Pacific-Union Club|Pacific Union Club]] and the [[Bohemian Club]] in San Francisco, as well as the Bankers Club in New York. In 1924, during a Bohemian Club meeting, [[Herbert Hoover]] urged
the leading western businessmen of the club to consider starting a first-rate business school on the west coast to limit the siphoning of talent to the eastern seaboard. As a result, in 1925, Paul became a founding board member of the [[Stanford Graduate School of Business]].<ref name="Guideto" />

Both as a representative of Southern Pacific and as a private citizen, he corresponded with Congress regarding labor and management issues.<ref name="Guideto" />

==Los Altos==
Only months after the 1906 San Francisco earthquake and fire, the Interurban Electrical Railroad purchased a 160-acre tract of ranch land in the Santa Clara Valley owned by [[Sarah Winchester]]. The Interurban Electrical Railroad was a subsidiary of Southern Pacific. In 1906, Paul Shoup was named Assistant General Manager of Southern Pacific’s local municipal and interurban lines.<ref>Trains Mean Business: The Growth of Los Altos, Exhibit Notes, Los Altos History Museum, 2009.</ref> When Southern Pacific wanted to purchase a right of way through the Winchester property, Sarah realized that the section the railroad wanted would cut her property in two, separating her cattle barn from her grazing lands. It would render her ranch ineffective, therefore she demanded that the railroad purchase the entire property. This left the railroad with an excessive amount of land. Shoup gathered his business associates and purchased the excess property from Southern Pacific. They incorporated as the Altos Land Company for the purposes of developing the area as a residential community marketed to executives and businessmen working in San Francisco. Clark marketed Los Altos as the “Crown of the Peninsula.” <ref>San Francisco Call, “Los Altos, Crown of the Peninsula,” 28 May 1909.</ref>

Paul Shoup is considered the founder of Los Altos because of his personal and professional contributions to the ongoing economic health of the fledgling settlement. Beginning in 1907, Paul and several business associates formed the Altos Land Company to develop the former Winchester and Merriman ranches<ref>[http://www.losaltosonline.com/index.php?option=com_content&task=view&id=23890&Itemid=47 Los Altos Town Crier - Downtown history shrouded in myth: Rotary Club presenter probes Los Altos' roots]. Losaltosonline.com (2011-04-19). Retrieved on 2013-07-21.</ref> as a residential enclave along the Southern Pacific Railroad’s Los Gatos cutoff, then under construction. This rail line would allow residents to have a direct rail connection to both San Jose (30 minutes) and San Francisco (60 minutes). Paul sold his interest in the company only to buy back the company in 1913 to prevent it from going into bankruptcy. Beyond this direct involvement, he played a large role in bringing businesses and people to the town. It was his business connections through his roles at Southern Pacific and his involvement in the influential social clubs of the time that gave legitimacy to the enterprise, drawing in an influential and well-heeled group of early residents to bring his vision of a bucolic residential commuter community to light.

In January 2000, Paul Shoup was chosen by the Los Altos Town Crier as its “Los Altan of the Century.”<ref>{{cite web|url=http://www.losaltosonline.com/index.php?option=com_content&task=view&id=53&Itemid=51 |title=Los Altos Town Crier - A Brief History of Los Altos |publisher=Losaltosonline.com |date=2008-03-01 |accessdate=2011-10-05}}</ref>

==Paul Shoup House in Los Altos==
The [[Paul Shoup House]] was the first property in Los Altos to be listed on the [[National Register of Historic Places]] by the Department of the Interior on September 23, 2011<ref>Marino, Pam. (2011-10-04) [http://losaltos.patch.com/articles/shoup-house-first-los-altos-property-on-national-register-of-historic-places Shoup House First Los Altos Property on National Register of Historic Places - Around Town - Los Altos, CA Patch]. Losaltos.patch.com. Retrieved on 2013-07-21.</ref><ref>{{cite web|url=http://www.nps.gov/history/nr/listings/20110930.htm |title=National Register of Historic Places Listings |publisher=Nps.gov |date=2011-09-30 |accessdate=2011-10-06}}</ref> based on the significance of the original owner, Paul Shoup.<ref>[https://web.archive.org/web/20110930192827/http://www.parks.ca.gov/pages/1067/files/shoup%20house%20nr%20nomination%20draft.pdf ]</ref>

==External links==
* [http://www.time.com/time/covers/0,16641,19290812,00.html Paul Shoup on the Cover of Time Magazine August 12, 1929]

==References==
{{reflist|2}}

{{Southern Pacific Presidents}}

{{DEFAULTSORT:Shoup, Paul}}
[[Category:American railroad executives of the 20th century]]
[[Category:American city founders]]
[[Category:1874 births]]
[[Category:1946 deaths]]
[[Category:Southern Pacific Railroad people]]
[[Category:Stanford University trustees]]
[[Category:Los Altos, California]]<!--city founder-->
[[Category:People from San Bernardino, California]]
[[Category:People from Los Altos, California]]
[[Category:Businesspeople from California]]