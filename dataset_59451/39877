{{Infobox military person
| name = Vladimir Yegorovich Kabanov
| native_name = Владимир Егорович Кабанов
| image = Vladimir Kabanov.jpg
| birth_date = 22 August 1918
| death_date = 17 August 1977
| birth_place = [[Bolshaya Zhuravka]], [[Balashovsky Uyezd]], [[Saratov Governorate]], [[RSFSR]]
| death_place = [[Krasnodar]]
| allegiance = {{flag|Soviet Union}}
| branch = [[Soviet Air Force]]
| serviceyears = 1939–58
| rank = [[Captain]]
| unit = [[7th Guards Attack Aviation Regiment]], [[230th Assault Aviation Division]]
| battles = [[World War II]]
*[[Kerch-Eltigen Operation]]
*[[Crimean Offensive]]
*[[Operation Bagration]]
*[[East Pomeranian Offensive]]
*[[Battle of Berlin]]
| awards = {{Hero of the Soviet Union}}<br>
{{Order of Lenin}}<br>
[[Order of the Red Banner]] (2)<br>
[[Order of the Patriotic War]] 1st class<br>
[[Order of the Red Star]] (2)
}}'''Vladimir Yegorovich Kabanov''' (Russian: Владимир Егорович Кабанов; 22 August 1918 – 17 August 1977) was a [[Soviet Air Forces|Soviet Air Force]] [[Captain (armed forces)|captain]] and [[Hero of the Soviet Union]]. Kabanov was awarded the title for flying 114 attack sorties during [[World War II]]. Kabanov continued to serve in the Soviet Air Force until his retirement in 1958. He worked as a goods manager in [[Krasnodar]].<ref name=":0">{{Ruheroes|name=Vladimir Kabanov|id=85}}</ref>

== Early life ==
Kabanov was born on 22 August 1918 in the village of Bolshaya Zhuravka in [[Saratov Governorate]]. He graduated from seventh grade and then worked at an aluminum plant in [[Zaporizhia]]. Kabanov was drafted into the [[Red Army]] in 1939. He graduated from the [[Voroshilovgrad]] Military Pilots Aviation School in 1941.<ref name=":0" /><ref name=":1">{{Cite web|url=http://allaces.ru/p/people.php?id=00000002611|title=Кабанов Владимир Егорович|website=allaces.ru|language=Russian|trans-title=Kabanov Vladimir Yegorovich|access-date=2016-04-21}}</ref><ref name=":2">{{Cite web|url=http://pobeda.poklonnayagora.ru/heroes/6932.htm|title=Кабанов Владимир Егорович|website=pobeda.poklonnayagora.ru|language=Russian|trans-title=Kabanov Vladimir Yegorovich|access-date=2016-04-22}}</ref>

== World War II ==
Kabanov fought in combat from October 1943.<ref name=":1" /> He became an [[Ilyushin Il-2]] pilot in the [[7th Guards Attack Aviation Regiment]] of the [[230th Assault Aviation Division]]. In November 1943, he flew sorties during the [[Kerch–Eltigen Operation]]. On 14 November he was wounded in the leg. On 22 January 1944 he flew in a raid on trains on the [[Vladislavovka railway station|Vladislavovka]]-[[Kolodez]] section of the railway in the [[Kerch]] area. During the mission, three trains were attacked and 21 cars were set on fire, of which one contained ammunition. For his actions Kabanov received thanks from the commander of the [[4th Air Army]]. On 27 January, Kabanov flew in a sortie with three other Il-2s to attack German tanks in the area of Skosiyev-Fontan. During the mission a shell fragment hit the oil cooler of Kabanov's plane but he reportedly did not turn back until the mission was accomplished. <ref>{{Cite book|title=Герои боёв за Крым|last=Ghazaryan|first=Andranik|publisher=Tavria Publishing House|year=1972|location=Simferopol|page=119|language=Russian|trans-title=Heroes of the Battle for Crimea}}</ref> On 30 January he was awarded the [[Order of the Red Star]].<ref>Order No. 2, 230th Assault Aviation Division, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie20023970/ pamyat-naroda.ru]</ref> In April and May 1944, Kabanov fought in the [[Crimean Offensive]]. On 15 June he was awarded the [[Order of the Patriotic War]] 1st class.<ref>Order No. 24, 4th Air Army, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie30985099/ pamyat-naroda.ru]</ref> After the end of the Crimean Offensive the 230th Division was transferred to fight in [[Operation Bagration]]. During the June and July  1944, Kabanov participated in the [[Mogilev Offensive]] and the [[Bialystok Offensive]]. On 20 July he was awarded the [[Order of the Red Banner]].<ref>Order No. 34, 4th Air Army, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie36816024/ pamyat-naroda.ru]</ref> In August he fought in the [[Osovets Offensive]]. On 20 September he received a second Order of the Red Banner.<ref name=":0" /><ref>Order No. 41, 4th Air Army, available online at  [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie34101315/ pamyat-naroda.ru]</ref>

In 1945, Kabanov joined the [[Communist Party of the Soviet Union]].<ref name=":2" /> From February 1945, Kabanov fought in the [[East Pomeranian Offensive]]. On 23 February he was awarded the title Hero of the Soviet Union and the [[Order of Lenin]] for completing 114 sorties by December 1944. In these sorties, Kabanov reportedly destroyed 19 tanks, 12 artillery batteries, 17 AA positions, 58 vehicles, 2 trains, 3 bridges, 60 carts, 5 warehouses. His sorties also reportedly killed up to 100 German troops.<ref name=":1" /><ref>Hero of the Soviet Union citation, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie46444080/ pamyat-naroda.ru]</ref> From April, he fought in the [[Berlin Offensive]].<ref name=":0" />

== Postwar ==
Kabanov graduated from the Krasnodar Higher Air Force Navigator Officers School in 1949. In 1954, he received a second Order of the Red Star. He retired in 1958 and graduated from the Krasnodar Institute of Soviet Trade. He worked as a goods manager at the Krasnodar branch of the Russian Shoe Trade Association. He lived in Krasnodar and died on 17 August 1977. Kabanov was buried in Krasnodar.<ref name=":0" /><ref name=":1" /><ref name=":2" />

== Personal life ==
Kabanov married Yulia Mikhailovna. He had at least one child. <ref>{{Cite web|url=http://www.warheroes.ru/hero/hero.asp?photo_id=2641|title=Герой Советского Союза Кабанов Владимир Егорович :: Фотодокументы|website=www.warheroes.ru|language=Russian|trans-title=Hero of the Soviet Union Kabanov Vladimir Yegorovich : Photo document|access-date=2016-04-22}}</ref>

== References ==
<references />
[[Category:1918 births]]
[[Category:1977 deaths]]
[[Category:People from Saratov Governorate]]
[[Category:Soviet military personnel of World War II]]
[[Category:Soviet Air Force officers]]
[[Category:Heroes of the Soviet Union]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Recipients of the Order of the Red Banner]]
[[Category:Recipients of the Order of the Patriotic War, 1st class]]
[[Category:Recipients of the Order of the Red Star]]
{{DEFAULTSORT:Kabanov, Vladimir}}