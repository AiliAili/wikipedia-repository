{{Infobox person
|name        = Ebenezer Doan
|image       = Ebenezer Doan.jpg
|alt         = 
|caption     = Ebenezer Doan, Master Builder
|birth_date  = {{Birth date|1772|09|09|df=y}}
|birth_place = Bucks County, PA
|death_date  = {{Death date and age|1866|02|03|1778|06|07|df=y}}
|death_place = Sharon, Ontario
|other_names = 
|known_for   = Sharon Temple
|occupation  = Master Builder
}}
'''Ebenezer Doan, Jr.''' (1772–1866) was the Master Builder or architect-contractor in charge of designing and building the [[Sharon Temple]], a National Historic Site of Canada. Doan was a highly accomplished builder, as evidenced by the creative techniques used in the Temple structure. Doan was an early [[Quakers|Quaker]] immigrant from [[Bucks County, Pennsylvania|Bucks County]], [[Pennsylvania]] who joined the [[The Children of Peace|Children of Peace]] in 1812. His first house (1819), drive shed and granary have now been relocated on the Temple grounds and restored.<ref>{{cite journal|last=McIntyre|first=John|title=Tradition and Innovation: Ebenezer Doan and the Buildings of the Children of Peace|journal=Canadian Quaker History Journal|year=1989|volume=46|pages=6–16|url=http://www.cfha.info/journal46.pdf}}</ref>

==Life==
Ebenezer Doan, Jr. was born 9 September 1772 in Bucks County of a large Quaker family. He apprenticed at a young age to his elder brother Jonathan, a prominent Master Builder in the mid-Atlantic states, he having built and designed the first New Jersey State House (1791-2) and the New Jersey State Prison (1797–99).<ref>{{cite journal|last=McIntyre|first=John|title=Tradition and Innovation: Ebenezer Doan and the Buildings of the Children of Peace|journal=Canadian Quaker History Journal|year=1989|volume=46|pages=7}}</ref>

After a short, tragic first marriage, Ebenezer Doan married Elizabeth Paxon in 1801; they had six children.<ref>{{cite book|last=Schrauwers|first=Albert|title=Awaiting the Millennium: The Children of Peace and the Village of Hope 1812-1889|year=1993|publisher=University of Toronto Press|location=Toronto|pages=221}}</ref> In 1808, the extended Doan clan moved to the new Quaker settlement on [[Yonge Street]] in what is now [[Newmarket, Ontario|Newmarket]], Ontario. In 1813, most members of the family, including Ebenezer and Elizabeth, joined the [[The Children of Peace|Children of Peace]], led by another Yonge Street Quaker, [[David Willson (1778–1866)|David Willson]]. Ebenezer's fame as a builder is linked to the extraordinary "Meeting Houses" (churches) that he built for this group in Hope (now Sharon), Ontario.

Doan remained an active member of the group until 1840, when he resigned for unknown reasons. His wife and children remained members. Doan died 3 February 1866.<ref>{{cite book|last=Schrauwers|first=Albert|title=Awaiting the Millennium: The Children of Peace and the Village of Hope 1812-1889|year=1993|publisher=University of Toronto Press|location=Toronto|pages=221–2}}</ref>

==Buildings==
[[Image:Sharon Temple.jpg|thumb|right|Sharon Temple located in Sharon, Ontario, Canada]]
[[File:Second Meeting House.jpg|thumb|left|Second Meeting House, Sharon, Ontario]]
[[File:David Willsons study.jpg|thumb|left|David Willson's Study, Sharon Temple Museum]]
[[File:Sharon Temple Jacobs ladder.jpg|thumb|left|Jacob's Ladder, Sharon Temple]]

The Children of Peace rejected the "plain style" of Quaker architecture and built a series of ornate meeting places designed to "ornament the Christian Church with all the glory of Israel".<ref>{{cite book|last=Schrauwers|first=Albert|title=Awaiting the Millennium: The Children of Peace and the Village of Hope 1812-1889|year=1993|publisher=University of Toronto Press|location=Toronto|pages=115}}</ref> The building most clearly associated with this imagery is the Temple, built over a seven-year period in imitation of [[Solomon's Temple]], and the [[New Jerusalem]] described in Revelations 21. The Temple is three storeys tall and measures 60 feet square by 75 feet high. The building is of [[Timber framing|timber frame]] construction, held together with [[mortice and tenon]] joints. It is square, and each side symmetrical, with tall centred double doors on each side to "allow all to enter on an equal footing".<ref>{{cite book|last=McArthur|first=Emily|title=Children of Peace|year=1898|publisher=Era & Express|location=Newmarket|pages=6}}</ref>  On either side of the door are three tall, sliding [[sash window]]s; there are 3 more per side on the second storey, and one on the third. At each of the building's twelve corners is a square [[Roof lantern|lantern]] (carved out of a single block of wood) surmounted by four green [[finial]]s. From the top four lanterns hangs a golden ball, with the word Peace inscribed on it. A reeded [[frieze]] caps each storey, and the four corners are tall, reeded quarter columns.

The temple's main auditorium is open and spacious, filled with light from 24 windows and from the central well that reaches up through the second floor musician's gallery to the third storey. Sixteen columns in all support the upper storeys; in the centre of which is an altar, or arc. The space was filled with individual chairs, not pews, which sit on two raised levels of floor to allow better sight-lines. The pillars are surmounted by curved arches and a coved ceiling. The second floor musician's gallery is reached by a 20 foot high curved staircase called "Jacob's Ladder."<ref>{{cite book|last=McIntyre|first=John|title=Children of Peace|year=1994|publisher=McGill-Queen's University Press|location=Montreal & Kingston|pages=58–64}}</ref>
[[File:Doan house.jpg|thumb|right|The Doan Farm House, now relocated to the Sharon Temple site]]
[[File:Doan barn.jpg|thumb|right|The Doan Barn, now demolished]]
Ebenezer Doan finished David Willson's Study in September 1829. Its exterior colonnade and arches can be seen as the Temple "turned inside out". Many of its architectural details such as the reeding, lantern, quarter pillars, coved ceiling and arches match the Temple. It was also referred to as "the counting house." It was 16 by 8 feet wide. It appears to have been a model for the Second Meeting House, constructed  1834-1842. The Second Meeting House was 100 feet long and 50 feet wide, and two storeys high. Its exterior was similar to the study. The interior auditorium was an open space supported by 20 pillars and a 20 foot high ceiling. The second floor was reached by a circular staircase leading to a 20 by 20 foot room used as a school. The Second Meeting House was demolished in 1912, leading the York Pioneer & Historical Society to preserve the Temple as a museum in 1917.<ref>{{cite book|last=McArthur|first=Emily|title=Children of Peace|year=1898|publisher=Era & Express|location=Newmarket|pages=5}}</ref>

==Doan Farm at the Sharon Temple Museum==

In 1960, the Doan farm house was moved from the original farmstead to the [[Sharon Temple]] National Historic Site. The House, built in 1819, is reminiscent of the Delaware Valley houses Doan would have remembered from his youth. It has a three bay facade, and three rooms on the main floor, one of which is a large open beamed kitchen. The house has been restored to illustrate life in the period. More recently, the house's original Drive shed and granary were moved to the site, and are currently being restored.

==References==
{{reflist}}

==Sources==
*McIntyre, John "Tradition and Innovation: Ebenezer Doan and the Buildings of the Children of Peace" Canadian Quaker History Journal (1989) 46-6-17.
*McIntyre, John "Children of Peace" (McGill-Queen's University Press: Montreal & Kingston, 1994).
* Schrauwers, Albert. ''Awaiting the Millennium: The Children of Peace and the Village of Hope 1812-1889''. (Toronto: University of Toronto Press, 1993). pp.&nbsp;221–2.
{{Prominent 19th-Century Canadian Quakers}}

{{DEFAULTSORT:Doan, Ebenezer}}
[[Category:1772 births]]
[[Category:1866 deaths]]
[[Category:Persons of National Historic Significance (Canada)]]
[[Category:Canadian Quakers]]
[[Category:19th-century Quakers]]
[[Category:Canadian architects]]
[[Category:18th-century Quakers]]
[[Category:American Quakers]]
[[Category:American emigrants to Canada]]
[[Category:People from Bucks County, Pennsylvania]]