{{Infobox military person
|name= Quentin C. Aanenson
|birth_date= {{birth date|1921|4|21}}
|death_date= {{death date and age|2008|12|28|1921|4|21}}
|birth_place= [[Luverne, Minnesota]],<br />[[United States]]
|death_place= [[Bethesda, Maryland]],<br />United States
|image=Quentin Aanenson Training Photo.JPG
|caption=Quentin C. Aanenson at [[Thunderbird Field]], [[Phoenix, Arizona]] during Primary flight training.
|nickname=
|allegiance= [[U.S. Army Air Forces]]
|serviceyears= 1942–1945
|rank= [[Captain (United States)|Captain]]
|commands=
|unit= 391st Fighter Squadron<br />366th Fighter Group<br />[[9th Air Force]]
|battles= [[World War II]]
|awards= 
|laterwork=
}}
'''Quentin C. Aanenson''' (April 21, 1921 – December 28, 2008) was a [[World War II]] veteran [[fighter pilot]] and former captain of the 391st Fighter Squadron, [[366th Fighter Group]], 9th [[United States Air Force|Air Force]], [[U.S. Army Air Corps]]. He flew the [[P-47 Thunderbolt]] in the [[Normandy]] [[D-Day]] invasion and subsequent [[European Theatre of World War II|European campaign]].<ref name="Wpost">{{cite web|url=http://www.washingtonpost.com/wp-dyn/content/article/2008/12/29/AR2008122902407.html|title=WWII Fighter Pilot Shared Haunting Story With the World|last=Sullivan|first=Patricia|date=2008-12-30|publisher=Washington Post|accessdate=2009-01-04}}</ref>

Aanenson enlisted in the [[United States Army Air Corps]] in 1942 but was not called up to active duty until February 1943. He left for [[Santa Ana Army Air Base|Santa Ana Air Force Base]] for pre-flight training and then to Primary Flight School at [[Thunderbird Field]] near [[Phoenix, Arizona]]. In September 1943, he attended Basic Flight School at Gardner Field near [[Bakersfield, California]]. Aanenson then received Advanced Flight Training at [[Luke AFB|Luke Field]], [[Phoenix, Arizona]] where he was commissioned a [[second lieutenant]] on January 7, 1944. From January to May 1944, he trained at Harding Field in [[Baton Rouge]], [[Louisiana]] where he met his wife Jackie.<ref name="Wpost"/><ref name="Review by The Times-Picaynne">{{cite web
| url = http://blog.nola.com/davewalker/2007/09/ken_burns_new_world_war_ii_min.html | title = Ken Burns' new World War II miniseries is a masterpiece | accessdate = 2009-01-04 | publisher = The Times-Picaynne}}</ref>

Aanenson demonstrated exceptional courage and ability as a fighter pilot, amassing tens of kills and beating all odds to survive the early months of his tour of duty.<ref>Burns, Ken: "The War". Documentary, 2007</ref> Later in the war, Aanenson was taken out of the cockpit and embedded with advance troops, with his skills put to good use as a quick-response aircraft attack coordinator. He eventually documented his experiences for his family.<ref name="Wpost"/> This was later turned into a documentary video, ''A Fighter Pilot's Story'', which Aanenson wrote, produced and narrated. The film was first televised in late 1993<!--11/12/93-->, then broadcast on over 300 public television stations in June 1994. Until August 2007, it was available for purchase on DVD. The three-hour documentary, tells of an enthusiastic and cheery boy very rapidly aged by too much death. It also tells of a remarkably wide range of combat duties and details many harrowing individual missions. In one such mission, Aanenson and his wingman came upon and destroyed a German convoy, but the wingman's gun had jammed. Aanenson fired upon roadside ditches where German soldiers had hidden, making multiple passes and "walking" his rudder to spread his fire more effectively and leave as few survivors as possible.<ref name="doc1993">Aanenson, Quentin C.: "A Fighter Pilot's Story". Documentary, 1993</ref>

The documentary also tells of a remarkable coincidence, in which Aanenson's [[P-47]] was called down to assist some American troops under attack by a tank. He surveyed the scene, then reported to the troops that the [[tank]] was too close to them for him to fire upon it without risking injury to the Americans. However, since the soldiers were sure to be killed if the tank wasn't stopped, Aanenson decided to attack, and he managed to destroy the tank cleanly. About two years after the war, Aanenson met a new neighbor who started to recount the story.  About halfway through, Aanenson finished the memorable event for him, and for a time they both shared in the emotion of the event.<ref name="doc1993"/>

Aanenson was a Commander of the [[France|French]] [[Legion of Honor]], representing all Americans who served in [[France]]. He was also featured in the documentary ''[[The War (documentary)|The War]]'' by [[Ken Burns]], recounting his experiences during [[World War II]] as a fighter pilot. At the conclusion of Episode Five of the series, Aanenson narrated a poignant and ominous letter he had written to his future wife but had never sent, considered by some critics to be of similar style to the [[Sullivan Ballou#The letter|Sullivan Ballou letter]] in Burns' ''[[The Civil War (documentary)|The Civil War]]''. Written December 5, 1944, the letter reads:<ref name="Wpost"/><ref name="Review by The Times-Picaynne"/>

{{cquote|

''Dear Jackie,''

''For the past two hours, I've been sitting here alone in my tent, trying to figure out just what I should do and what I should say in this letter in response to your letters and some questions you have asked. I have purposely not told you much about my world over here, because I thought it might upset you. Perhaps that has been a mistake, so let me correct that right now. I still doubt if you will be able to comprehend it. I don’t think anyone can who has not been through it.''

''I live in a world of death. I have watched my friends die in a variety of violent ways...''

''Sometimes it's just an engine failure on takeoff resulting in a violent explosion. There's not enough left to bury. Other times, it's the deadly [[Anti-aircraft warfare|flak]] that tears into a plane. If the pilot is lucky, the flak kills him. But usually he isn't, and he burns to death as his plane spins in. Fire is the worst. In early September one of my good friends crashed on the edge of our field. As he was pulled from the burning plane, the skin came off his arms. His face was almost burned away. He was still conscious and trying to talk. You can't imagine the horror.  ''

''So far, I have done my duty in this war. I have never aborted a mission or failed to dive on a target no matter how intense the flak. I have lived for my dreams for the future. But like everything else around me, my dreams are dying, too. In spite of everything, I may live through this war and return to Baton Rouge. But I am not the same person you said goodbye to on May 3. No one can go through this and not change. We are all casualties. In the meantime, we just go on. Some way, somehow, this will all have an ending. Whatever it is, I am ready for it.''

:''Quentin''}}

According to the PBS website, Quentin and Jackie married after the war and had three children and eight grandchildren, with Aanenson working in the insurance field after graduating from [[Louisiana State University]].

Aanenson died from the effects of cancer at his home in [[Bethesda, Maryland]] on December 28, 2008.<ref name="Wpost"/>

==Tributes==

The painting ''Thunderbolt Patriot'' by William R. Farrell, now in the permanent collection of the [[Smithsonian Institution]] of the [[National Air and Space Museum]], depicts Aanenson having just returned from a combat mission over Germany during [[World War II]].

The airfield at [[Luverne Municipal Airport]] (KLYV) was named Quentin Aanenson Field in his honor.<ref name="NWS">{{cite web|url=http://weather.noaa.gov/weather/current/KLYV.html|title=Current Weather Conditions: Luverne, Quentin Aanenson Field Airport, MN, United States |publisher=National Weather Service - Telecommunication Operations Center|accessdate=2009-01-04}}</ref>

==References==
{{reflist}}
* [http://quentinaanenson.com/ Official website of Quentin C. Aanenson] DVD still available as of Jan 2016.
*[http://www.startribune.com/local/36853279.html?elr=KArks7PYDiaK7DU2EkP7K_V_GD7EaPc:iLP8iUiD3aPc:_Yyc:aUU Star Tribune obituary]
* [https://web.archive.org/web/20060910214443/http://www.drury.edu/multinl/story.cfm?ID=2901&NLID=162 Drury University: A Fighter Pilot's Story]
* [http://www.arlingtoncemetery.net/qcaanenson.htm Biography]

==External links==
* [https://web.archive.org/web/20070903094126/http://www.folklife.si.edu/explore/Features/WorldWarII/Wartime%20Stories.html Smithsonian Center for Folklife and Cultural Heritage, Wartime Stories]
* [http://quentinaanenson.com/transition.html World War II photos of Aanenson]

{{DEFAULTSORT:Aanenson, Quentin C.}}
[[Category:1921 births]]
[[Category:2008 deaths]]
[[Category:People from Rock County, Minnesota]]
[[Category:United States Army Air Forces officers]]
[[Category:United States Army Air Forces pilots of World War II]]