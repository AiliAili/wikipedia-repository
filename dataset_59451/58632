{{Infobox journal
| title = Journal of Travel Research
| cover = [[File:Journal of Travel Research.tif]]
| editor = Geoffrey I. Crouch
| discipline = [[Tourism]]
| former_names = 
| abbreviation = J. Travel Res.
| publisher = [[Sage Publications]]
| country = 
| frequency = Bimonthly
| history = 1968-present
| openaccess = 
| license = 
| impact = 2.442
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal200788/title
| link1 = http://jtr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jtr.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 0047-2875
| eISSN = 1552-6763
| OCLC = 647703441
| LCCN =77647048
}}
The '''''Journal of Travel Research''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering [[tourism]]. The [[editor-in-chief]] is [[Geoffrey I. Crouch]] ([[La Trobe University, Australia]]). It was established in 1968 and is published by [[Sage Publications]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.442.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Travel Research |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal200788/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Tourism geography]]
[[Category:Economics journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1968]]