{{Infobox journal
| title = Gut
| cover = [[File:Gutjournalcover.gif]]
| editor = [[Emad El-Omar]]
| discipline = [[Gastroenterology]], [[hepatology]]
| abbreviation = Gut
| publisher = [[BMJ Group]]
| country =
| frequency = Monthly
| history = 1960–present
| openaccess = [[Delayed open access|Delayed]], after 12 months
| license =
| impact = 14.66
| impact-year = 2014
| website = http://gut.bmj.com/
| link1 = http://gut.bmj.com/content/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 01641120
| LCCN =
| CODEN = GUTTAK
| ISSN = 0017-5749
| eISSN = 1468-3288
}}
'''''Gut''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] on [[gastroenterology]] and [[hepatology]]. It is the journal of the [[British Society of Gastroenterology]] and is published by the [[BMJ Group]]. {{As of|2013}}, the [[editor-in-chief]] is [[Emad El-Omar]].

''Gut'' was established in 1960 and covers [[original research]] on the [[gastrointestinal tract]], [[liver]], [[pancreas]], and [[biliary tract]]. The journal has annual supplements covering the presentations from the British Society of Gastroenterology Annual General Meeting. British Society of Gastroenterology [[clinical practice guidelines]] are also published as supplements to the journal. As of March 2010 subscribers to ''Gut'' also receive a copy of ''Frontline Gastroenterology''.

== Abstracting and indexing ==
''Gut'' is abstracted and indexed by [[Medline]], [[Science Citation Index]], [[Current Contents]]/Clinical Medicine, Current Contents/Life Sciences, [[Excerpta Medica]], and [[BIOSIS Previews]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 14.666, ranking it second out of 76 journals in the category "Gastroenterology and Hepatology".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Gastroenterology and Hepatology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

===Highly cited articles===
According to the [[Web of Science]], some highly cited articles in ''Gut'' are:
# {{cite journal |vauthors=Eaden JA, Abrams KR, Mayberry JF |year= 2001 |title= The risk of colorectal cancer in ulcerative colitis: a meta-analysis |journal= Gut |publisher= BMJ Group |volume= 48 |issue= 4 |pages= 526–535 |url= http://gut.bmj.com/content/48/4/526.abstract?ck=nck|doi= 10.1136/gut.48.4.526 |pmid= 11247898 |pmc=1728259}}
# {{cite journal |vauthors=Schlemper RJ, Riddell RH, Kato Y, Borchard F, Cooper HS, Dawsey SM, Dixon MF, Fenoglio-Preiser CM, Flejou JF, Geboes K, Hattori T, Hirota T, Itabashi M, Iwafuchi M, Iwashita A, Kim YI, Kirchner T, Klimpfinger M, Koike M, Lauwers GY, Lewin KJ, Oberhuber G, Offner F, Price AB, Rubio CA, Shimizu M, Shimoda T, Sipponen P, Solcia E, Stolte M, Watanabe H, Yamabe H |year= 2000 |title= The Vienna classification of gastrointestinal epithelial neoplasia |journal= Gut |publisher= BMJ Group |volume= 47 |issue= 2 |pages= 251–255 |url= http://gut.bmj.com/content/47/2/251.full|doi= 10.1136/gut.47.2.251 |pmid= 10896917 |pmc=1728018}}
# {{cite journal |vauthors=Ono H, Kondo H, Gotoda T, Shirao K, Yamaguchi H, Saito D, Hosokawa K, Shimoda T, Yoshida S |year= 2001 |title= Endoscopic mucosal resection for treatment of early gastric cancer |journal= Gut |publisher= BMJ Group |volume= 48 |issue= 2 |pages= 225–229 |url= http://gut.bmj.com/content/48/2/225.abstract|doi= 10.1136/gut.48.2.225 |pmid= 11156645 |pmc=1728193}}

==References==
{{reflist}}

== External links ==
* {{Official website|http://gut.bmj.com/}}

[[Category:Publications established in 1960]]
[[Category:Monthly journals]]
[[Category:BMJ Group academic journals]]
[[Category:English-language journals]]
[[Category:Gastroenterology and hepatology journals]]