{{Infobox journal
| title = Infection and Immunity
| cover = [[Image:Low resolution infection&immunity cover image.jpg|200px]]
| editor = [[Ferric C. Fang]]
| discipline = [[Infectious disease]]
| abbreviation = Infect. Immun.
| publisher = [[American Society for Microbiology]]
| country = United States
| frequency = Monthly
| history = 1967–present
| openaccess = [[Delayed open access|Delayed]]
| license = 
| impact = 3.731
| impact-year = 2014
| website = http://iai.asm.org/
| link1 = http://iai.asm.org/content/current
| link1-name = Online access
| link2 = http://iai.asm.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 01753126
| LCCN = 70234261
| CODEN = INFIBR
| ISSN = 0019-9567
| eISSN = 1098-5522
}}
'''''Infection and Immunity''''' is a [[peer review|peer-reviewed]] [[medical journal]] published by the [[American Society for Microbiology]]. It focuses on interactions between bacterial, fungal, or parasitic [[pathogen]]s and their hosts. Areas covered include molecular [[pathogenesis]], cellular [[microbiology]], [[bacterial infection]], host responses and [[inflammation]], fungal and parasitic infections, microbial [[Immunity (medical)|immunity]] and [[vaccine]]s, and molecular [[genomics]]. The journal publishes primary research articles, [[editorial]]s, commentaries, minireviews, and a spotlight report highlighting articles of particular interest selected by the editors. Articles are freely accessible after 6 months ([[delayed open-access journal|delayed open access]]). Through its "Global Outreach Program", free online access is available to qualified microbiologists in eligible developing countries.

== History ==
The journal was established in 1970. Prior to that time, original research articles covering topics in infection and immunity were published in a section of the ''[[Journal of Bacteriology]]''. As the size of this section grew, the need for a separate journal publishing peer-reviewed research in this area became apparent.<ref>Campbell, L.L. “Growth—and more fission.” J. Bacteriol. 1969;100(2):555-556.</ref> The first [[editor-in-chief]] was Erwin Neter ([[University at Buffalo, The State University of New York|SUNY Buffalo]]).

=== Editors-in-chief ===
The following persons have been editor-in-chief of ''Infection and Immunity'':
* 1970-1979: Erwin Neter
* 1980-1989: Joseph W. Shands, Jr.
* 1990-1999: Vincent A. Fischetti
* 2000-2007: Alison D. O’Brien
* 2007–present: Ferric C. Fang

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[AGRICOLA]]
* [[BIOSIS Previews]]
* [[CAB Abstracts]]
* [[Cambridge Scientific Abstracts]]
* [[Chemical Abstracts Service]]
* [[Current Contents]]/Life Sciences
* [[EMBASE]]
* [[Food Science and Technology Abstracts]]
* [[Illustrata]]
* [[Index Medicus]]
* [[MEDLINE]]
* [[Science Citation Index]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.731, ranking it 23rd out of 78 journals in the category "Infectious Diseases"<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Infectious Diseases |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref> and 47th out of 148 journals in the category "Immunology".<ref name=WoS2>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Immunology |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |accessdate= |series=Web of Science |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://iai.asm.org/}}

[[Category:Microbiology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1970]]
[[Category:English-language journals]]
[[Category:Monthly journals]]