{|{{Infobox ship begin}}
{{Infobox ship image
| Ship image=
| Ship caption=
}}
{{Infobox ship career
| Hide header=
| Ship country=
| Ship flag= [[File:Naval Ensign of the United Kingdom.svg|60px|Royal Navy Ensign]]
| Ship name=HMS ''Witherington''
| Ship ordered=April 1918
| Ship builder= [[J. Samuel White|James Samuel White & Co Ltd]]
| Ship laid down=27 September 1918
| Ship launched=16 January 1919
| Ship acquired=
| Ship commissioned=10 October 1919
| Ship decommissioned=
| Ship in service=
| Ship out of service= To reserve after May 1945
| Ship struck= On disposal list after September 1945
| Ship reinstated=
| Ship fate=*Sold for scrap 20 March 1947
*Wrecked 29 April 1947
| Ship honours=*Atlantic 1939-44
*Norway 1940
*English Channel 1940
| Ship badge= On a Field Black, A sinister Leg Gold couped at the Knee Red
| Ship motto=”I Will Not Fail”
| Ship identification=[[Pennant number]]: D76 and I76
| Ship notes=
}}
{{Infobox ship characteristics
| Hide header=
| Header caption=
| Ship class=[[V and W class destroyer|Admiralty modified W-class]] [[destroyer]]
| Ship displacement=1,140&nbsp;tons standard, 1,550&nbsp;tons full
| Ship length= 300&nbsp;ft [[Length overall|o/a]], 312&nbsp;ft [[Length between perpendiculars|p/p]]
| Ship beam= {{convert|29.5|ft|m}}
| Ship draught= {{convert|9|ft|m}}, {{convert|11.25|ft|m}} under full load
| Ship power=27,000&nbsp;shp
| Ship propulsion=
*White-Foster water-tube [[boilers]]
*Brown-Curtis geared [[steam turbines]]
*2 shafts
| Ship speed= 34&nbsp;[[knot (unit)|kn]]
| Ship range=*320-370&nbsp;tons oil
*3,500&nbsp;[[nautical mile|nmi]] at 15&nbsp;kn
*900&nbsp;nmi at 32&nbsp;kn
| Ship complement= 127
| Ship sensors=
| Ship EW=
| Ship armament=
* 4 × [[BL 4.7 inch /45 naval gun|BL 4.7 in (120-mm) Mk.I guns]], mount P Mk.I
* 2 × [[QF 2 pounder naval gun|QF 2 pdr Mk.II "pom-pom" (40 mm L/39)]]
* 6 × [[British 21 inch torpedo|21-inch torpedo tubes]]
| Ship armour=
| Ship aircraft=
| Ship aircraft facilities=
| Ship notes=
}}{{Infobox ship characteristics
| Hide header=
| Header caption= SRE conversion
| Ship complement= 134
| Ship sensors=
*Type 271 target indication radar
| Ship EW=
| Ship armament=
* 3 × BL 4.7 in (120mm) Mk.I L/45 guns
* 1 × [[QF 12 pounder 12 cwt naval gun]]
* 2 × QF 2 pdr Mk.II "pom-pom" (40 mm L/39)
* 3 × 21-inch torpedo tubes (one triple mount)
* 2 × depth charge racks
| Ship armour=
| Ship aircraft=
| Ship aircraft facilities=
| Ship notes=
}}{{Infobox service record
|is_ship=yes
|label=
|partof=*3rd Destroyer Flotilla - 1919
*Reserve at Rosyth - 1930
*15th Destroyer Flotilla - 1939
|codes=
|commanders=
|operations=*[[Nanking Incident]] 1927
*[[World War II]] 1939 to 1945
|victories=[[German submarine U-340|''U-340'']] - 1 Nov 1943
|awards=
}}
|}
'''HMS ''Witherington''''' was an [[V and W class destroyer|Admiralty modified W-class]] [[destroyer]] built for the [[Royal Navy]].  She was one of four destroyers ordered in April 1918 from [[J. Samuel White|James Samuel White & Co Ltd.]] under the 14th Order for Destroyers of the Emergency War Program of 1917-18. She was the first Royal Navy ship to carry this name.<ref name="naval-history.net">{{cite web|title=Service Histories of Royal Navy Warships in World War 2|url=http://www.naval-history.net/xGM-Chrono-10DD-09VW-Witherington.htm}}</ref>

The City of Durham adopted HMS ''Witherington'' following a successful Warship Week National Savings campaign in February 1942.<ref name="naval-history.net"/>

== Construction ==
''Witherington''’s keel was laid on 27 September 1918 at the James Samuel White & Co. Ltd. Shipyard in [[Cowes]], [[Isle of Wight]].  She was launched on 16 January 1919.  She was 300 feet overall (312&nbsp;ft between the perpendiculars) in length with a beam of 29.5 feet.  Her mean draught was 9 feet, and would reach 11.25 feet under full load.  She had a displacement of 1,140 tons standard and up to 1,550 full load.<ref name="pbenyon1.plus">{{cite web|title=Jane's Fighting Ships © for 1919|url=http://www.pbenyon1.plus.com/Janes_1919/Destroyers/Admiralty-V_Post_War.html}}</ref>

She was propelled by three White-Foster type water tube [[boilers]] powering Curtis-Brown geared [[steam turbines]] developing 27,000 SHP driving two screws for a maximum designed speed of 34 knots.  She was oil-fired and had a bunkerage of 320 to 370 tons.  This gave a range of between 3500 nautical miles at 15 knots and 900 nautical miles at 32 knots.<ref name="pbenyon1.plus">{{cite web|title=Jane's Fighting Ships © for 1919|url=http://www.pbenyon1.plus.com/Janes_1919/Destroyers/Admiralty-V_Post_War.html}}</ref>

She shipped four [[BL 4.7 inch /45 naval gun|BL 4.7 in (120-mm) Mk.I guns]], mount P Mk.I naval guns in four single centre-line turrets.  The turrets were disposed as two forward and two aft in super imposed firing positions.  She also carried two [[QF 2 pounder naval gun|QF 2 pdr Mk.II "pom-pom" (40 mm L/39)]] mounted abeam between funnels.  Abaft of the second funnel, she carried six [[British 21 inch torpedo|21-inch torpedo tubes]] in two triple mounts on the centre-line.<ref name="pbenyon1.plus">{{cite web|title=Jane's Fighting Ships © for 1919|url=http://www.pbenyon1.plus.com/Janes_1919/Destroyers/Admiralty-V_Post_War.html}}</ref>

== Inter-War years ==
''Witherington'' was commissioned into the Royal Navy on the 10th of October, 1919 with [[pennant number]] D76.  After commissioning she was assigned to the 3rd Destroyer Flotilla of the [[Atlantic Fleet (United Kingdom)|Atlantic Fleet]].  The flotilla served in Home waters from May 1920 to July 1923 when the Flotilla was transferred to the [[Mediterranean Fleet (United Kingdom)|Mediterranean Fleet]].  She was transferred to [[China Station]] in 1926.

Also, in 1926 she carried the last Shah of Persia, [[Ahmad Shah Qajar]], into exile, as the old country of Persia was replaced by the country of Iran.  During the [[Nanking Incident]] in March 1927, she helped rescue foreign nationals from the Nanking region of China.<ref name="naval-history.net"/>

In the early 1930s she underwent a refit and was laid-up in Maintenance Reserve at [[Rosyth]] as more modern destroyers came into service.  She was reactivated manned by [[Royal Naval Reserve|Reservists]] for a [[Fleet review (Commonwealth realms)|Royal Review]] at [[Weymouth Harbour, Dorset|Weymouth]] in August 1939.  With war looming she was retained in service and brought to war readiness.<ref name="naval-history.net"/>

== Second World War ==

=== Early Operations ===
In September 1939 the ship was allocated to the 15th Destroyer Flotilla based at Rosyth (changed to Liverpool in 1940) in [[Western Approaches Command]] for convoy defence. Up to April 1940 she was employed in the North West Approaches area providing local escort for convoys leaving Liverpool (OB series) to a dispersal point in the Atlantic approximately 750 nautical miles west of Lands End.<ref name="naval-history.net"/><ref name="www.convoyweb.org.uk/ob2">{{cite web|title=Convoy Web OB Convoys|url=http://www.convoyweb.org.uk/ob2/index.html}}</ref>  Periodically an OA (sailing from Southend)series convoy would sail and join up with the OB series.  The merged convoy would change to an OG series (UK to Gibraltar).<ref name="www.convoyweb.org.uk/ob2">{{cite web|title=Convoy Web OB Convoys|url=http://www.convoyweb.org.uk/ob2/index.html}}</ref>  During this period she escorted 20 convoys, for a total of 436 ships with total losses of 3 ships (2 sunk by U-boats and 1 due to collision).<ref name="www.convoyweb.org.uk">{{cite web|title=Convoy Web|url=http://www.convoyweb.org.uk/hague/index.html}}</ref>

In April 1940 she was detached to [[Scapa Flow]] after the [[German invasion of Norway]].  From 11 April to 15 April she escorted military convoy NP001 to Narvik then on 24 April she escorted military TM001/1. She provided local escort for the arrival at Clyde for TC004 with two troopships carrying 2,591 troops.<ref name="www.convoyweb.org.uk/tc">{{cite web|title=Convoy Web TC Series|url=http://www.convoyweb.org.uk/tc/index.html}}</ref> At the end of May her pennant number was changed to I76 for visual signalling purposes.  In June she escorted Group 1 (named Hebrew) of the evacuation of Norway from Scapa to the Clyde.<ref name="naval-history.net"/><ref name="www.convoyweb.org.uk/misc">{{cite web|title=Convoy Web Shorter Convoy Series|url=http://www.convoyweb.org.uk/misc/index.html}}</ref>

In July 1940 she was returned to the Western Approaches for convoy defence and was mainly employed in the North-West Approach sector as a local escort until February 1942.<ref name="naval-history.net"/>  During this time she escorted 13 mercantile convoys.<ref name="www.convoyweb.org.uk">{{cite web|title=Convoy Web|url=http://www.convoyweb.org.uk/hague/index.html}}</ref> On March 11, 1941, [[List of shipwrecks in March 1941|she was beached]] in [[Portsmouth]] after sustaining damage from a [[Luftwaffe]] air raid, to be later repaired and returned to service.

=== SRE Conversion ===
In late February 1942 she was withdrawn for conversion to a short-range escort (SRE). To augment the earlier changes, the replacement of the after bank of torpedo tubes with a single [[QF 12 pounder 12 cwt naval gun]] and the landing of 'Y' gun for additional space for depth charge gear and stowage, a Type 271 centimetric target indication radar was added on the bridge and a Type 286P air warning radar was installed on the main mast.<ref name="naval-history.net"/>

=== Western Approaches Command ===
Upon completion of the conversion, ''Witherington'' was redeployed in the Western Approaches.  During March she was part of the escort force of Convoy Halifax Inbound (HX) 229.  The convoy was under sustained attack during the night of 16–17 March by 5 U-Boats of the Raubgraf Group, 2 U-Boats of the Sturmer Group and one U-Boat transiting to home port.  There was no rescue ship assigned to HX229, therefore the escorts were rescuing the survivors of the ten merchant ships that were sunk.  Only two escorts were constantly with the convoy further exposing the convoy to attack.<ref name="Hitler's U-Boat War The Hunted 1942 - 1945">{{cite book|last=Blair|first=Clay|title=Hitler's U-Boat War The Hunted 1942 - 1945|year=1998|publisher=Random House Inc|location=New York|isbn=0-679-45742-9|pages=261–262}}</ref>

At the end of June 1943 she was transferred to the Mediterranean-based out of [[Alexandria]] in support of follow on convoys for the [[Allied invasion of Sicily]].  In November she was deployed to Gibraltar for Atlantic Convoy Defence.

On 1 November she took part in the sinking of [[German submarine U-340|''U-340'']] with {{HMS|Active|H14|6}}, {{HMS|Fleetwood|U73|6}} and two [[Vickers Wellington]] aircraft of [[No. 179 Squadron RAF]]<ref name="Hitler's U-Boat War The Hunted 1942 - 1945"/>{{rp|456}} at position 35<sup>o</sup>33'N, 06<sup>o</sup>37'W.<ref>{{cite web|title=U-Boat.net|url=http://uboat.net/allies/warships/ship/5450.html}}</ref>  She was deployed in the South-West Approaches out of Gibraltar throughout 1944.<ref name="naval-history.net"/>

She was deployed in the South-West Approaches out of Gibraltar throughout 1944.<ref name="naval-history.net"/>

In 1945 she was deployed to the English Channel area to counter the threat of snorkel equipped U-Boats being concentrated or convoy formation areas.  She remained in this deployment until [[VE-Day]].<ref name="naval-history.net"/>

== Post war ==

=== Disposition ===
''Witherington'' was paid off into reserve after VE-Day.  She was placed on the disposal list after [[VJ-Day]].  On 20 March 1947 she was sold to Metal Industries for [[ship breaking|breaking up]].  On 29 April while under tow to the breakers yard at [[Charlestown, Fife|Charlestown]] near Rosyth she parted the tow and was wrecked off the mouth of the [[River Tyne|Tyne]] in a gale.<ref name="naval-history.net"/>

=== Place of honour ===
After the ship was sold for scrap, her ship’s bell was retrieved and presented to the City of Durham.  The bell, along with a plaque displaying the ship’s crest, were mounted in the City Council Chambers.

== Notes ==
{{more footnotes|date=September 2012}}
{{Reflist}}

==Bibliography==
*{{cite book|last=Campbell|first=John|title=Naval Weapons of World War II|year=1985|publisher=Naval Institute Press|location=Annapolis, Maryland|isbn=0-87021-459-4}}
*{{cite book|title=Conway's All the World's Fighting Ships 1922–1946|editor1-last=Chesneau|editor1-first=Roger|publisher=Conway Maritime Press|location=Greenwich, UK|year=1980|isbn=0-85177-146-7}}
*{{Colledge}}
* {{cite book |first1=Maurice |last1=Cocker |first2=Ian |last2=Allan |title=Destroyers of the Royal Navy, 1893-1981 |isbn=0-7110-1075-7}}
* {{cite book|last=Friedman|first=Norman|title=British Destroyers From Earliest Days to the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2009|isbn=978-1-59114-081-8}}
* {{cite book |editor1-last=Gardiner|editor1-first=Robert|editor2-last=Gray|editor2-first=Randal|title=Conway's All the World's Fighting Ships: 1906–1921|year=1984|location=Annapolis, Maryland|publisher=Naval Institute Press|isbn=0-85177-245-5|lastauthoramp=y}}
* {{cite book|last=Lenton|first=H. T.|authorlink=Henry Trevor Lenton|title=British & Empire Warships of the Second World War|publisher=Naval Institute Press|location=Annapolis, Maryland|year=1998|isbn=1-55750-048-7}}
*{{cite book|last=March|first=Edgar J.|title=British Destroyers: A History of Development, 1892-1953; Drawn by Admiralty Permission From Official Records & Returns, Ships' Covers & Building Plans|year=1966|publisher=Seeley Service|location=London |OCLC=164893555}}
* {{cite book |last=Preston |first=Antony |title='V & W' Class Destroyers 1917-1945 |publisher=Macdonald |location=London |year=1971 |oclc=464542895}}
* {{cite book |last=Raven |first=Alan |last2=Roberts|first2=John  |title='V' and 'W' Class Destroyers |publisher=Arms & Armour |location=London |year=1979 |series=Man o'War |volume=2 |isbn=0-85368-233-X|lastauthoramp=y }}
* {{cite book|last=Rohwer|first=Jürgen|title=Chronology of the War at Sea 1939-1945: The Naval History of World War Two|publisher=Naval Institute Press|location=Annapolis, Maryland|year=2005|edition=Third Revised|isbn=1-59114-119-2}}
* {{cite book |last=Whinney |first=Bob |title=The U-boat Peril: A Fight for Survival |publisher=Cassell |year=2000 |isbn=0-304-35132-6}}
* {{cite book|last=Whitley|first=M. J.|title=Destroyers of World War 2|publisher=Naval Institute Press|date=1988|isbn=0-87021-326-1|location=Annapolis, Maryland}}
* {{cite book|last=Winser|first=John de D.|title=B.E.F. Ships Before, At and After Dunkirk|publisher=World Ship Society|location=Gravesend, Kent|year=1999|isbn=0-905617-91-6}}
* ''Hitler's U-Boat War The Hunters 1939-1942''
* ''Hitler's U-Boat War The Hunted 1942-1945''
* ''Jane's Fighting Ships'' for 1919

== External links ==
Service History of HMS ''Witherington'' was compiled by the late Lt Cdr Geoffry B. Mason RN and can be found at [http://www.naval-history.net/xGM-Chrono-10DD-09VW-Witherington.htm Naval History Web Site]
* [http://uboat.net/allies/warships/ship/5450.html U-Boat.net]

{{V and W class destroyer}}
{{March 1941 shipwrecks}}
{{1947 shipwrecks}}

{{DEFAULTSORT:Witherington}}
[[Category:V and W-class destroyers of the Royal Navy]]
[[Category:Ships built on the Isle of Wight]]
[[Category:1919 ships]]
[[Category:World War II destroyers of the United Kingdom]]
[[Category:Shipwrecks in the North Sea]]
[[Category:Maritime incidents in March 1941]]
[[Category:Maritime incidents in 1947]]