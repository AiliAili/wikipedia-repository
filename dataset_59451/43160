{{DISPLAYTITLE:''LZ 18'' (''L 2'')}}
{|{{Infobox aircraft begin
| name           = ''LZ 18'' (''L 2'')
| image          = File:LZ 18.jpg
| caption        = 
| alt            = <!-- Alt text for main image -->
}}{{Infobox aircraft
|type           = Reconnaissance/bomber airship
|national origin = Germany 
|manufacturer   =  	[[Luftschiffbau Zeppelin]]
|designer       = 
|first flight   = 9 September 1913
|primary user   = [[Imperial German Navy]]
|number built   = 1
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|developed from = <!--The aircraft which formed the basis for this aircraft-->
|variants with their own articles = <!--Variants OF this aircraft-->
}}
|}
'''''LZ 18''''' (Navy designation '''''L 2''''') was the second [[Zeppelin]] airship to be bought by the [[Imperial German Navy]]. It [[Johannisthal air disaster|caught fire and crashed]] with the loss of all aboard on 17 October 1913 before entering service.

==Design==
On 18 January 1913 Admiral [[Alfred von Tirpitz]], Secretary of State of the German Imperial Naval Office, obtained the agreement of [[Kaiser Wilhelm II]] to a five-year program of expansion of the German  naval airship strength.  A contract was placed for the first ship on 30 January, one requirement being that the craft should be capable of bombing England.  The design was heavily influenced by the [[naval architect]] Felix Pietzker, who was an advisor to the German Admiralty Aviation Department.  Construction started in May.  The length and overall height of the ship were limited by the size of the Navy's airship shed at [[Fuhlsbüttel]],<ref name=Rob23>Robinson 1971, p. 23</ref> but Pietzker's proposals enabled the diameter of the airship to be increased without increasing  overall height firstly by changing the position of the keel, moving it from outside the main hull structure to within it, and secondly by placing the engine cars closer to the hull. It was powered by four {{convert|165|hp|kW|disp=flip}} [[Maybach]] engines in two engine cars, each car driving a pair of four-bladed propellers which were mounted either side of the envelope via driveshafts and [[bevel gears]]. The ship was controlled from a third gondola mounted in front of the forward engine car.<ref name=Rob23/>

==Operational history==
''LZ 18'' was first flown on 6 September at [[Friedrichshafen]], and following a number of trial flights was flown to [[Johannisthal Air Field|Johannisthal]] on 20 September for naval acceptance trial to begin, the flight of about 700&nbsp;km (438&nbsp;mi) taking twelve hours.<ref>[http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201045.html Twelve-hour voyage by ''L 2''  ]. [[Flight International|''Flight'']]: 27 September 1913, p. 1071</ref>  The ship's tenth flight was to be an altitude trial and was scheduled for 17 October.<ref name=Rob26>Robinson 1971, p. 26</ref>  The airship was removed from its shed in the morning but takeoff was delayed because one of the engines would not start.  The delay of two hours while the engine was repaired allowed the morning sun to heat the hydrogen, causing it to expand.  This caused the airship to ascend rapidly to {{convert|2000|ft|m|disp=flip|abbr=on}}, when horrified observers on the ground saw a flame leap out of the forward engine car, causing the explosion of some of the gasbags.  Halfway to the ground there was a second explosion, and as the wreckage hit the ground further explosions followed as the fuel tanks ignited.  Three survivors were pulled from the blazing wreckage, but two died shortly afterwards and the third died that night in hospital.  In all 28 men died, including  Pietzker, and the new chief of the Admiralty Aviation Department, ''Korvettenkapitän'' Behnisch, the successor to ''Korvettenkapitän'' Metzing who had been [[Helgoland Island air disaster|killed in the ''L&nbsp;1'']] on 9 September.  The accident was agreed to have been caused by the rapid ascent leading to venting of hydrogen through the relief valves, which in Zeppelins of the period were placed at the bottom of the bags, without vent trunks to convey any hydrogen let off to the top of the ship.  Some of the vented gas was then sucked into the forward engine car, where it was ignited, the fire then spreading to the gasbags.<ref name=Rob28>Robinson 1971, p. 28</ref>

==Aftermath==
The loss of the ''L&nbsp;2'' occurred six weeks after the loss of the ''L&nbsp;1'' with most of its crew.<ref>Robinson 1971, p. 25</ref>  The two disasters deprived the Navy of most of its experienced personnel and led to the suspension of the planned expansion program.  The death of ''Korvettenkapitän'' Behnisch led to the appointment of [[Peter Strasser]] as the new head of the Admiralty Aviation Department.<ref name=Rob28/>

==Specifications==
{{Aircraft specs
|ref=Robinson 1971, p. 378
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=15
|length m=
|length ft=518
|length in=2
|length note=
|dia m=
|dia ft=54
|dia in=6
|dia note=

|width note=
|height m=
|height ft=
|height in=
|height note=
|volume m3=
|volume ft3=953000
|volume note=

|empty weight kg=
|empty weight lb=44500
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=

|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=24500
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=4
|eng1 name=Maybach 
|eng1 type=C-X six cylinder inline water-cooled piston engine.
|eng1 kw=<!-- prop engines -->
|eng1 hp=165
|eng1 shp=<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=47
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=

|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following 
specific parameters, remove this parameter-->
|guns= 
|bombs=

}}

==Notes==
{{reflist}}

==References==
*Robinson. Douglas. ''The Zeppelin in Combat'': Henley-on Thames, Foulis, 1973 (3rd ed.) ISBN 0 85429 130 X

{{Zeppelin aircraft}}

[[Category:Zeppelins]]
[[Category:Hydrogen airships]]