{{Infobox artist
| bgcolour      =
| name         = Nehemia Azaz ( נחמיה עזז)
| image        = Harsa003.jpg
| imagesize  = 
| caption      = Azaz, pictured in late 1950's at Harsa in Beersheba
| birth_name   =
| birth_date   = {{birth date|df=yes|1923|10|9}}
| birth_place  = Berlin, Germany
| death_date   = {{death date and age|df=yes|2008|10|28|1923|10|9}}
| death_place  = Oxfordshire, England, UK
| field        = Sculpture, stained glass, ceramics}}

{{moresources|date=October 2016}}


'''Nehemia Azaz''' ({{lang-he|נחמיה עזז}}), also known as Nechemia, Nechemiah, Nehemiah, Henri or N H Azaz (9 October 1923 – 28 October 2008), was an Israeli sculptor, ceramicist and architectural artist,<ref>[http://www.imj.org.il/artcenter/newsite/en/?artist=Azaz,%20Nehemiah "The Israel Museum" Information Center for Israeli Art catalogue, retrieved November 23, 2015]</ref> who spent half of his working life in the UK. Best known in Israel as founder of the Department of Artistic Ceramics at the Harsa<ref>{{cite book|last1=Ofrat|first1=Gideon|last2=Magdalena|first2=Hefez|last3=Agudat omane ḳeramiḳah be-Yiśraʼel|title=ראשית הקרמיקה הישראלית, 1932־1962 = The beginnings of Israeli ceramics / ]אוצרת התערוכה, גדעון עפרת ומגדלמה חפץ].|date=1991|publisher=[Israel] : Agudat omane ha-ḳeramiḳah be-Yiśraʼel, [1991]|page=51-52}}</ref> factory in Beersheba, Azaz made his studio base in Oxfordshire, England from the late 1960s onwards, working in stained glass, wood, concrete, bronze, brass, copper and aluminium.

==Chronology==

{| class="wikitable"
|-
! Date!! !! Location
|-
| 1923 || Born Berlin. October 9, taken to Palestine at the age of 3 months. || Germany, Palestine
|-
| 1923- || Childhood and Education. || [[Zichron Ya'akov]], Palestine
|-
| 1946 || Enrolled at University of Bologna, and apprenticed to stonemason. || Bologna
|-
| 1947 || Apprenticed to sculptor & stained-glass maker Cephas Stauthamer.<ref>[[:nl:Cephas Stauthamer|"Cephas Stauthamer", retrieved November 23, 2015]]</ref>{{Better source|reason=per WP:CIRCULAR|date=December 2015}} || Amsterdam
|-
| 1949-50 || Studied part-time at [[Académie de la Grande Chaumière|Grand Chaumiere]], and under [[Ossip Zadkine]]|| Paris
|-
| 1953-4 || Studied Industrial Ceramics Engineering, Institute of Technology, The Hague || Holland
|-
| 1954-5 || Worked in Paddington, London-based pottery || London
|-
| 1956-60 || Founded and managed art department of Harsa pottery and ceramic art factory || [[Beersheba|Beersheva]], Israel
|-
| 1963-65 || Artist in residence, [[Carmel College (Oxfordshire)|Carmel College School]], Berkshire || UK
|-
| 1965-2008 || Lived and worked in Oxfordshire || UK
|-
| 2008 || Died 28 October || UK
|}

== Background ==
Born in 1923 and taken to Palestine at an early age, Azaz's early career developed alongside, and in relation to, the formation of the state of Israel. After founding Harsa ceramic art pottery in the country in [[Beersheba|Beersheva]], and designing the first generation of ceramics (and jewellery) as cultural export through the Maskit initiative of [[Ruth Dayan]] and others, he began to explore the idea of ceramics as sculpture. His work featured in an exhibition at Wakefield Gallery in 1955, alongside [[L S Lowry]] and [[Josef Herman]]; his ceramic sculpture is included in London's Victoria & Albert Museum collection<ref>[http://collections.vam.ac.uk/item/O153285/form-azaz-nehemiah/ "V&A Collections", Stoneware form with creamy glaze, by Nehemiah Azaz, ca. 1954, Last update November 4, 2015]</ref>

He was the founder and first director of the Department of Artistic Ceramics at the Harsa factory in Beersheba in 1955 where he designed the first generation of ceramics as cultural export through the Maskit initiative of Ruth Dayan. In 1960 he left Harsa to concentrate on large-scale architectural sculpture and stained glass. Influenced by the art of 1950s/60s, he was just as much inspired by the natural forms of the desert, mythology and the human condition. 

After a highly successful and high profile architectural art commission at the Sheraton Hotel in Tel Aviv in 1965,<ref>{{cite journal|last1=Spencer|first1=Charles S.|title=Azaz: Sculpture as Architecture|journal=Studio International|date=1965|issue=170|pages=198–201|url=https://books.google.com/books?id=0RRHAQAAIAAJ&dq=1965+azaz+%22studio+international%22&focus=searchwithinvolume&q=azaz|accessdate=29 November 2015}}</ref> he became one of the first Israeli sculptors of his generation to be commissioned internationally. Later, at the invitation of [[Yitzhak Rabin]], then Israeli Ambassador in Washington, DC,  Azaz -  living in UK as artist in residence at Carmel College, UK, carved a 30 square metre walnut wood wall for the Israel Lounge (along with a ceiling mural by artist [[Shraga Weil]] and painted fabric by Ezekiel Kimche) at the [[John F. Kennedy Center for the Performing Arts]]. 

His career as an architectural and stained glass artist then continued largely in US and UK, where he made his studio base and home in Oxfordshire from late 1960's onwards. There are a number of large scale public art pieces still in existence, including at the Loop Synagogue in Chicago;<ref>[http://www.chicagoloopsynagogue.org/ "Chicago Loop Synagogue" homepage, retrieved November 23, 2015]</ref> Belfast Synagogue, Pace University in Manhattan NY and in the Israel Lounge at the John F Kennedy Centre in Washington, DC<ref>{{cite news|title=Kennedy Center Israel Room Dedicated|agency=Jewish Telegraphic Agency|issue=XXXVIII No.228|date=December 2, 1971|url=http://www.jta.org/1971/12/02/archive/kennedy-center-israel-room-dedicated}}</ref>

==Artistic expression==
===Studio pottery and design at Harsa===
After his time in Europe, Azaz returned to Israel to take up the position of head of the ceramic art department at Harsa<ref>{{cite book|author1=Eretz Israel Museum|title=Third Biennale of Israeli Ceramics: Design, Art, Design|date=2005|publisher=Eretz Israel Museum|url=https://books.google.com/books?id=nOcmAQAAIAAJ&dq=azaz+harsa&q=azaz|accessdate=26 November 2015}}</ref><ref>{{cite journal|last1=Israeli|first1=Amnon|title=Harsa, The Art Ceramic Department 1956-1966|journal=1280°C (Journal of the Ceramic Artists Association of Israel)|date=Summer 2013|issue=27|pages=24–28}}[In Hebrew]</ref> in Beersheba. Joined by young and enthusiastic artists such as [[Dan Arbeid]],<ref>{{cite journal|last1=Christian|first1=Andy|title=Dan Arbeid - A Retrospective|journal=Ceramic Art and Perception|date=June 2012|issue=88|pages=107}}</ref> he designed and made sculptural pottery artworks influenced by the contours,  shapes and colours of the desert, and also designed pieces that were made and decorated by others as his contribution to the Maskit programme set up by Ruth Dayan. The idea of Maskit was to enable émigré  craftspeople to adapt their indigenous skills and Azaz was instrumental in their training and encouragement.

===Stained glass windows===
Azaz's first commercial stained glass was a commission onboard the Zim Lines cruise liner S/S Moledet, which routinely sailed between New York and Haifa in the early 1960s. His technique of using pieces of intensely coloured glass, became a hallmark in his other glasswork commissions. Large stained glass window projects include synagogue windows at Carmel College, praised by art and architecture scholar [[Nikolaus Pevsner]] as using "extraordinary technique with rough pieces of coloured glass like crystals"<ref>{{cite book|last1=Sherwood|first1=Jennifer|last2=Pevsner|first2=Nikolaus|title=Buildings of England - Oxfordshire - Pevsner Architectural Guides|date=1974|publisher=Penguin|pages=712|url=https://books.google.com/books?id=2yUNAQAAIAAJ&dq=synagogue+azaz+-%22a-z%22&q=azaz}}</ref> and by Historic England as "brilliant and innovative artistic glass";<ref>[http://www.historicengland.org.uk/listing/the-list/list-entry/1379943 "Jewish Synagogue at Carmel College" Historic England List Entry, retrieved November 24, 2015]</ref> at Marble Arch Synagogue (over a period of 17 years); at [[Headington Hill Hall]], once the former HQ of [[Pergamon Press]],<ref>[http://www.historicengland.org.uk/listing/the-list/list-entry/1047044 "Headington Hill Hall" Historic England List Entry, retrieved November 25, 2015]</ref> (now part of [[Oxford Brookes University]] campus) in Oxford, UK and at Temple Sholom<ref>{{cite news|last1=Achilles|first1=Rolf|title=God is in the Details|url=http://articles.chicagotribune.com/1997-03-30/features/9703300290_1_inspiring-churches-pews|accessdate=26 November 2015|publisher=Chicago Tribune|date=March 30, 1997}}</ref> in [[Chicago]].

===Carved wood sculpture===
Carving large wood sculpture has been a feature of Azaz's career from his earliest sculpting period, and also as the one of the preferred materials for small maquettes of his large-scale bronze work. The largest wooden piece, entitled Old Testament Musical Instruments,<ref>[http://www.kennedy-center.org/about/virtual_tour/israeli_lounge.html "John F Kennedy Center for the Performing Arts" Israeli Lounge, retrieved November 23, 2015]</ref> a 30 square metre carved walnut wall gifted by the State of Israel, is located in the Israeli Lounge<ref>[http://viewer.zmags.com/publication/7709daa8#/7709daa8/16 "Artistic Gifts to the Kennedy Center and the Artists Who Created Them" in-house publication of the John F Kennedy Center for the Performing Arts, retrieved November 24, 2015]</ref> at the John F Kennedy Center for the Performing Arts in Washington DC. Several other carved wooden pieces still in existence are located in public institutions in Israel including the Chaim Sheba Medical Centre at Tel-Hashomer.

=== Large scale architectural commissions in concrete ===

Azaz's architectural work appeared in early on his career, as features of a rapidly growing Tel-Aviv in the 1960's & 70's featuring on civic buildings and incorporated into the fabric of new constructions such as the Sheraton Hotel,<ref>{{cite journal|last1=Spencer|first1=Charles S.|title=Azaz|journal=Studio International Art|date=1961|volume=161-162|page=114|url=https://books.google.com/books?id=0RRHAQAAIAAJ&dq=studio+international+sheraton&focus=searchwithinvolume&q=sheraton|publisher=National Magazine Company}}</ref> Plaza Hotel, Hilton Hotel, Architects and Engineering Associates Building and Hennig House Diamond Center, Ramat-Gan. In the United States, several large concrete commissions followed, including 'THE FORM MAKERS' at Sherman House Hotel<ref>{{cite news|last1=Edwards|first1=Carole|title=Who left a 75-ton sculpture in the Sherman House lobby?|url=http://archives.chicagotribune.com/1969/08/03/page/246/article/who-left-a-75-ton-sculpture-in-the-sherman-house-lobby|accessdate=24 November 2015|issue=123d Year - No.215|publisher=Chicago Tribune|date=August 3, 1969}}</ref> in Chicago (then rededicated to Dominican University, IL but subsequently retired in 2008).<ref>[http://domininet.blogspot.com/2008/11/what-became-of-wall.html "What became of the Wall, "DominiNet", November 17, 2008]</ref>

===Metallic sculpture===
Azaz was self taught in respect of working and finishing different metals. His commissions span iron, copper, bronze, aluminum, brass and silver with several listed on the [[Smithsonian American Art Museum]], Art Inventories Catalog.<ref>[http://collections.si.edu/search/results.htm?q=Azaz+Henri&view=&date.slider=&dsort=title "Smithsonian American Art Museums Art Inventories Catalog", Henri Azaz List entries"]</ref>  'HANDS OF PEACE' at the Loop Synagogue in Chicago,<ref>{{cite book|last1=Cutler|first1=Irving|title=The Jews of Chicago: From Shtetl to Suburb|date=1976|publisher=University of Illinois Press|location=Urbana & Chicago|isbn=9780252021855|pages=176}}</ref><ref>{{cite journal|author1=Inland Architecture Press and Inland Architect Corporation and American Institute of Architects. Chicago Chapter|title=Inland Architect|date=1969|volume=13-14|pages=20|url=https://books.google.com/books?id=v-FUAAAAMAAJ&q=azaz|accessdate=26 November 2015|publisher=Inland Architect Press|issn=0020-1472}}</ref> and 'BROTHERHOOD OF MAN'<ref>{{cite book|last1=Gayle|first1=Margot|last2=Koch|first2=Michele Cohen ; foreword by Edward I.|last3=Art Commission of New York|first3=|last4=Municipal Art Society of New York|first4=|title=The Art Commission and the Municipal Art Society guide to Manhattan's outdoor sculpture|date=1988|publisher=Prentice Hall Press|location=New York|isbn=0136202535|pages=46|url=https://books.google.com/books?id=LHpQAAAAMAAJ&q=%22henri+azaz%22}}</ref> at [[Pace University]]<ref>{{cite journal|last1=Lucki|first1=Monet|last2=Daugherty|first2=Greg|title=The Art of Pace|journal=Pace University Magazine|date=Fall 2014|volume=XXXI|issue=2|pages=26|url=http://issuu.com/urmarketing/docs/pace_magazine_fall_2014|accessdate=24 November 2015}}</ref> in New York. Also, [[Belfast Synagogue]],<ref>{{cite book|last1=Harwood|first1=Elain|title=England : a guide to post-war listed buildings|date=2000|publisher=Ellipsis|location=London|isbn=184166037X|pages=5:42|url=https://books.google.com/books?id=2_NUAAAAMAAJ&dq=azaz+rosenberg+belfast&focus=searchwithinvolume&q=azaz+}}</ref><ref>{{cite book|last1=Meek|first1=Harold Alan|title=The Synagogue|date=1995|publisher=Phaidon Press|isbn=0714829323|pages=225–226|url=https://books.google.com/books?id=IjVUAAAAMAAJ&dq=azaz+rosenberg+belfast+meek&focus=searchwithinvolume&q=azaz|accessdate=29 November 2015}}</ref> St Barbara Church in Essen, Germany<ref>{{cite book|last1=Sichau|first1=Frank|title=Der Emscherbrücher, 2000: Sakralgebäude und religiöse Kunst in Wanne-Eickel und Herne|date=1 January 2000|publisher=Gesellschaft f. Heimatkunde Wanne-Eickel|isbn=978-3936452051|pages=53-56|}}</ref> and the Kol Ami Museum at the North Suburban Synagogue Beth El<ref>[https://nssbethel.org/sites/default/files/uploaded_files/site/Life_Long_Learning/Rissman_Family_Kol_Ami_Museum/GatesBrochure2010.pdf "The Gates by Henri Nehemiah Azaz" Exhibition of Kol Ami Museum curated by Ilana Segal, Curator of Collections at Spertus Institute for Jewish Learning and Leadership, retrieved November 24, 2015]</ref> in Illinois.

Smaller bronze sculpture also feature strongly throughout Azaz's career. Using lost styrofoam (polystyrene) or [[lost-wax casting]], he realised a large collection of bronze work evolving from industrial style pieces in the 1960's to figurative and mythical based pieces in the 1980's making up part of the portfolio displayed at the International Contemporary Art Fair in 1986<ref>{{cite book|title=Works Displayed at ICAF/London, 3rd International Contemporary Art Fair|date=1986|publisher=Marlborough Fine Art (London) and International Contemporary Art Fair, 3rd (London)}}</ref>

===Optical art===
Azaz created his own optical art variants, using painted canvas, plate metal or acrylic and aluminium, rods and tubes. Some of the earliest pieces are still held at the [[Warwick Arts Centre]]<ref>[http://www2.warwick.ac.uk/services/art/artist/nechemiaazaz/wu0075/ "University of Warwick Art Collection", Op Mobile No.10 by Nechemia Azaz at Warwick Arts Centre, retrieved November 23, 2015]</ref>  in the UK and in collection of Guy's and St Thomas' Charity Fine Art and Heritage Collection.

===Exhibitions===
Exhibitions included: Wakefield Gallery (with [[LS Lowry]] and [[Josef Herman]])<ref>{{cite book|author1=Lowry, L.S|author2=Herman, J|author3=Azaz, N|author4=Wakefield Art Gallery|title=Paintings and Drawings [by] L.S. Lowry, Josef Herman. Pottery [by] Nehemiah Azaz|date=1955|publisher=City Art Gallery|url=https://books.google.com/books?id=VgidPQAACAAJ}}</ref> 1955; Grosvenor Gallery, London 1965; Camden Arts Centre, London 1966, Galerie Semiha Huber, Zurich 1966; Galerie Artek, Helsinki 1967; Israel Pavilion, [[Expo 67]]; International Contemporary Art Fair, London 1986; Mabat Art Gallery, Tel-Aviv 1987.

==References==
<references/>

==External links==
* [http://www.nehemiaazaz.com "Nehemia Azaz", Artist's website]
* [[:he:נחמיה עזז|"Nehemia Azaz on Wikipedia Hebrew edition]]

[http://www.online-instagram.com/tag/iaanazaz "Israel Architecture Archive", Works by NH Azaz, retrieved November 2015]

{{DEFAULTSORT:Azaz, Nehemia}}
[[Category:1923 births]]
[[Category:2008 deaths]]
[[Category:Israeli artists]]
[[Category:20th-century Israeli sculptors]]
[[Category:Israeli ceramists]]
[[Category:Israeli sculptors]]
[[Category:Israeli stained glass artists and manufacturers]]
[[Category:Israeli jewellery designers]]