<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=GA-7 Cougar & TB 320 Tangara
 |image=GulfstreamAmericanGA-7Cougar03.jpg
 |caption= 
}}{{Infobox Aircraft Type
 |type= Personal and [[trainer aircraft]]
 |manufacturer=[[Gulfstream Aerospace|Gulfstream American Aviation]]
 |designer=
 |first flight= 20 December 1974<ref name="simpson25">Simpson 1991, p. 25</ref>
 |introduced=February 1978
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=1978-1979
 |number built=115
 |unit cost=
 |developed from=
 |variants with their own articles=
}}
|}
[[File:GulfstreamAmericanGA-7Cougar01.jpg|thumb|A GA-7 Cougar on the ramp at Les Cedres Quebec, May 2005]]

The '''Gulfstream American GA-7 Cougar''' is an [[United States|American]] [[Aluminium|all-metal]], 4-seat, twin-engined [[light aircraft]]. The Cougar was a twin-engine development of the [[Grumman American AA-5|Gulfstream American AA-5B Tiger]] and traces its lineage to the [[Grumman American AA-1|AA-1 Yankee Clipper]] and the [[Bede BD-1]].

== Development ==
As a development of the company's single-engined designs, Grumman American developed a twin-engined version, designated the GA-7 which it named the Cougar, in keeping with the existing Lynx, Cheetah and Tiger names for aircraft in the company's line. The prototype Cougar with two {{convert|160|hp|kW|0|abbr=on}} Lycoming O-320 engines first flew on the 29 December 1974. The prototype had a sliding canopy but this was soon changed to a starboard side door on the production aircraft. With other rework required the production prototype did not fly until 14 January 1977.<ref name="Wood">Wood, Derek: ''Jane's World Aircraft Recognition Handbook'', page 233. Jane's Publishing Company, 1982. ISBN 0-7106-0202-2</ref>

Before production started the company was taken over on 1 September 1978 by American Jet Industries, who changed the company name to Gulfstream American.<ref name="simpson25" /> Production of the Cougar ran for only two model years, 1978 and 1979, before production was halted. Just 115 Cougars were delivered.<ref name="A17SO" /><ref>{{cite web|url=http://www.aopa.org/News-and-Video/All-News/1996/August/1/Return-of-the-Night-Fighter|title=Return of the Night Fighter?: From predator to exotic bird,|last=Marsh|first=Alton K|date=Aug 1, 1996|publisher=[[Aircraft Owners and Pilots Association]]}}</ref>

In 1995 the type certificate for the GA-7 was sold to [[SOCATA]] of [[France]] who intended to produce the aircraft as the TB 320 Tangara for the training market.<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/1995/1995%20-%201853.htm|title=Socata to manufacture Cougars|journal=[[Flight International]]|date=27 June 1995|page=24}}</ref> It was also to develop a variant with two [[Lycoming O-360|Lycoming O-360-A1G6]] engines of {{convert|180|hp|kW|0|abbr=on}} each and a re-designed cockpit, it was designated the TB 360. The first Tangara was a modified Cougar, had {{convert|160|hp|kW|0|abbr=on}} engines and first flew in mid-1996. The complete Tangara prototype was also a converted Cougar and had the {{convert|180|hp|kW|0|abbr=on}} engines. It first flew in February 1997. After delays in getting the type certified SOCATA announced at the end of 1999 that it had delayed indefinitely plans to certify the type.<ref>{{cite journal|url=http://www.flightglobal.com/pdfarchive/view/2000/2000%20-%200022.htm|title=Tangara delayed 'indefinitely'|journal=[[Flight International]]|date=3 January 2000|last=Legros|first=Francois}}</ref>

== Design ==
The Cougar is a twin-engined low-wing cantilever monoplane using a honeycomb and bonded metal construction that is the hallmark of the line since the BD-1. The prototype's single spar wing was upgraded to a double-spar configuration and this allowed a [[wet wing]].

The Cougar is powered by a pair of wing-mounted [[Lycoming O-320|Lycoming O-320-D1D]] engines of {{convert|160|hp|kW|0|abbr=on}}. It carries four people at maximum cruise speed of {{convert|160|kn|km/h|0|abbr=on}} and a typical cruise speed of {{convert|140|kn|km/h|0|abbr=on}}. It was certified under US [[Federal Aviation Regulations#Regulations of Interest#Part 23|FAR Part 23]] on 22 September 1977.<ref name="A17SO">{{cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/B2CEC2BAFD278D88862572A00056C584?OpenDocument&Highlight=ga-7|title = TYPE CERTIFICATE DATA SHEET NO. A17SO|accessdate = 2008-07-02|last = [[Federal Aviation Administration]]|authorlink = |date=March 2007}}</ref>

==Variants==
;GA7 Cougar
:{{convert|160|hp|kW|0|abbr=on}} version designed by Grumman American and produced by Gulfstream American 1978-79. 115 built.<ref name="simpson25" />
;TB 320 Tangara
:Re-started production of the {{convert|160|hp|kW|0|abbr=on}} version by SOCATA, two modified Cougars were converted to Tangara prototypes 1996-97, production was never started.
;TB 360 Tangara
:Re-designed variant from SOCATA with {{convert|180|hp|kW|0|abbr=on}} O-360 engines, one prototype, a modified Cougar, was first flown in 1997, never entered production.

==Specifications (Gulfstream American GA-7 Cougar)==
{{Aircraft specs
|ref=FAA Type Certificate,<ref name="A17SO" /> The Incomplete Guide to Airfoil Usage,<ref name="Lednicer">{{cite web|url = http://www.ae.uiuc.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage|accessdate = 2009-12-30|last = Lednicer |first = David|authorlink = |date=October 2007}}</ref> Pilot's Operating Handbook<ref name="POH">{{cite web|url = http://www.88thservices.com/pdf/cougarflightmanual.pdf|title = Pilot's Operating Handbook|accessdate = 2009-12-31|last = [[Gulfstream Aerospace]]|authorlink = |date=October 1978}}</ref> and Pilot Friend<ref name="PilotFriend">{{cite web|url = http://www.pilotfriend.com/aircraft%20performance/Grumman/8.htm|title = Grumman GA-7 Cougar performance and specifications |accessdate = 2009-12-30|last = Pilot Friend|authorlink = |year = n.d.}}</ref>
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=three passengers
|length m=
|length ft=28
|length in=8
|length note=
|span m=
|span ft=36
|span in=10
|span note=
|height m=
|height ft=10
|height in=4
|height note=
|wing area sqm=
|wing area sqft=184
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=NACA 63A415
|empty weight kg=
|empty weight lb=2569
|empty weight note=
|gross weight kg=
|gross weight lb=3800
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Lycoming O-320|Lycoming O-320-D1D]]
|eng1 type=four cylinder, [[horizontally-opposed]] aircraft engines
|eng1 kw=<!-- prop engines -->
|eng1 hp=160<!-- prop engines -->
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=168
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=160
|cruise speed note=true airspeed
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=63
|stall speed note=calibrated airspeed, flaps down
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=188
|never exceed speed note=indicated airspeed
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=61
|minimum control speed note= indicated airspeed
|range km=
|range miles=
|range nmi=1170
|range note= maximum economy with no reserves
|combat range km=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=17400
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1150
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=20.65
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=11.9 lb/hp (0.14 kg/kW)
|thrust/weight=

|more performance=

|avionics=
}}

==See also==
{{aircontent
|related=
* [[Grumman American AA-5]]
|similar aircraft=
* [[Beechcraft Duchess]]
* [[Piper Seminole]]
|sequence=
}}

==References==
{{reflist|30em}}

*{{cite book |last=Simpson |first= R.W.|authorlink= |coauthors= |title= Airlife's General Aviation|year=1991 |publisher= Airlife Publishing|location= England |isbn=1 85310 194 X|pages=}}

==External links==
* {{commons category-inline|Gulfstream American GA-7 Cougar}}

{{Grumman aircraft}}
{{American Aviation}}
{{SOCATA aircraft}}

[[Category:Gulfstream American aircraft|Cougar]]
[[Category:SOCATA aircraft|TB 320]]
[[Category:United States civil utility aircraft 1970–1979]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]