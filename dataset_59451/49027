{{Infobox writer <!-- For more information see [[:Template:Infobox Writer/doc]]. --> 
| name          = Kristine Kershul
| image         = Kristine_Kershul,_Rwanda_2011.jpg
| image_size    = 
| alt           = 
| caption       = Kristine Kershul, Rwanda 2011
| pseudonym     = 
| birth_name    = Kristine K. Kershul
| birth_date    = <!-- {{Birth date and age|YYYY|MM|DD}} -->
| birth_place   = [[Oregon]], U.S.<ref name=Wilhelm>{{cite news|last=Wilhelm | first=Steve | title=Kristine Kershul uses her background as a linguist to develop Bilingual Books' language training for travelers | work=[[Puget Sound Business Journal]]| date=29 August 2003 | url=http://www.bizjournals.com/seattle/stories/2003/09/01/smallb1.html | accessdate = 8 October 2013}}</ref>
| death_date    = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->
| death_place   = 
| resting_place = 
| occupation    = Author, Publisher<br />Linguist, Teacher<br />Creator of the 10 Minutes a Day Series and the Language Map Series<br />Founder and President of [[Bilingual Books|Bilingual Books, Inc.]]
| language      = 
| nationality   = 
| ethnicity     = 
| citizenship   = 
| education     = Undergraduate and Graduate Degrees in [[German language and literature|German Languages and Literature]]<ref name=Butterworth>Butterworth, Beverly H.  "Fast Accent", ''[[The Oregonian]]'', 10 May 1989.</ref>
| alma_mater    = 
| period        = 
| genre         = Foreign Language Learning
| subject       = 
| movement      = 
| notableworks  = 10 Minutes a Day Book Series<br />10 Minutes a Day Audio CD Series<br />Language Map Series
| spouse        = 
| partner       = 
| children      = 
| relatives     = 
| awards        = 
| signature     = 
| signature_alt = 
| module        =
| website       = {{URL|www.kershul.com}}
| portaldisp    = 
}}
'''Kristine K. Kershul''' is an American author, publisher, [[linguist]] and teacher.  Kershul founded [[Bilingual Books|Bilingual Books, Inc]]. in 1981 with the publication of her first book, ''German in 10 Minutes a Day'' and the development of the ''10 Minutes a Day'' Series.<ref name=Wilhelm/><ref name=Luce>Luce, Beth. "Do you speak...?  Language books translate in to success", ''[[Costco Connection]]'', June 2004.</ref>  She has authored books, audio CDs, phrase guides and interactive computer software for 20 languages.

== Background ==

Kershul attributes her lifelong passion for languages to her family.<ref name=Butterworth/><ref name=Rumley>Rumley, Larry.  "Linguists have quick method to improve Americans' image", ''[[Seattle Times]]'', January 1982.</ref>  She grew up in a trilingual home<ref name=Luce/><ref name=Dhyan>Dhyan, Lee. "Language for Travelers Earns Praise and Profits for Ex-Teacher Kris Kershul", ''Northwest Women in Business'', Nov/Dec 1987.</ref> where in addition to English, her father spoke [[Croatian language|Croatian]] and her mother spoke [[Danish language|Danish]].<ref name=Wilhelm/><ref name=Butterworth/>

Kershul studied in the U.S.,<ref name=Dhyan/> then took her academic endeavors to [[Heidelberg]], [[Germany]] where she earned her undergraduate and graduate degrees.  She received a second graduate degree from the [[University of California at Santa Barbara]] and then went on to do her doctoral studies in [[German language and literature|German Languages and Literature]].<ref name=Butterworth/>

=== Early Career, 1971 - 1981 ===

Kershul worked her way through school as a bilingual tour guide, traveling all over Europe and parts of [[Asia]] and [[Africa]].<ref name=Luce/><ref name=Rumley/><ref name=Dhyan/>

She also served as a translator for the [[US Embassy|U.S. Embassy]] in [[Bonn]], West Germany and for Berlitz in Europe and the U.S.<ref name=Seaside>"Seaside Native Starts Book Company", ''[[Seaside Signal]]'', 14 January 1988.</ref>
She spent ten years teaching at universities in [[Germany]] and the U.S. where she noticed students experiencing the same problems.  They were self-conscious speaking and Kershul wanted to help them progress to the level where they were laughing and using the language comfortably.<ref name=Butterworth/>

The idea for her first book grew out of what she observed as a teacher and a traveler.<ref name=Luce/><ref name=Rumley/><ref name=Dhyan/>

== Bilingual Books, 1981 - 1988 and 1995 - Present ==

Kershul saw the need for a language instruction method that would not intimidate or frustrate beginning students.<ref name=Seaside/>  She wanted to fill the gap between text books and traditional phrase books.  She developed a fresh approach to learning a foreign language, which remained academically solid while geared to the traveler.<ref name=Wilhelm/><ref name=Butterworth/><ref name=Rumley/><ref name=Stainsby>Stainsby, Mia. "The Quick Way to Survive in a Foreign Language", ''[[Vancouver Sun]]'', 2 April 1988.</ref>
In 1981 she designed and wrote her first book, ''German in 10 Minutes a Day'', which was the beginning of a series of language-learning books that launched [[Bilingual Books|Bilingual Books, Inc.]]<ref name=Seaside/>  By the end of the first ten months, Kershul had authored a total of five books, adding [[French language|French]], [[Spanish language|Spanish]], [[Italian language|Italian]] and [[Chinese language|Chinese]] to create the ''10 Minutes a Day'' Series.<ref name=Luce/>  In the next three years, she would add Inglés, [[Norwegian language|Norwegian]], [[Japanese language|Japanese]], [[Russian language|Russian]] and [[Hebrew language|Hebrew]] languages to the Series.<ref name=Seaside/>

In 1988 Kershul sold her company to Sunset Books and Magazine.  She moved to [[Cape Town]], [[South Africa]] and spent the next six years traveling around the world.<ref name=Wilhelm/>

In 1995, Kershul reacquired [[Bilingual Books]] and moved back to the Pacific Northwest to base the company out of [[Seattle]], [[Washington (state)|Washington]].<ref name=Wilhelm/>  In the following years, she expanded the breadth of languages to 20 and added new product lines with the creation of the ''Language Map'' Series, the ''10 Minutes a Day'' Audio CD Series, and the ''10 Minutes a Day'' software.<ref name=Catalog>{{cite web|url=http://www.bbks.com/products.aspx|title=Catalog|publisher=Bilingual Books}} Retrieved 14 October 2013.</ref>

== Publications ==

=== 10 Minutes a Day Book Series ===

# Arabic in 10 Minutes a Day
# Chinese in 10 Minutes a Day
# French in 10 Minutes a Day
# German in 10 Minutes a Day
# Hebrew in 10 Minutes a Day
# Inglés en 10 Minutos al Día
# Italian in 10 Minutes a Day
# Japanese in 10 Minutes a Day
# Norwegian in 10 Minutes a Day
# Portuguese in 10 Minutes a Day
# Russian in 10 Minutes a Day
# Spanish in 10 Minutes a Day

=== 10 Minutes a Day Audio Series ===

# French in 10 Minutes a Day Audio CD
# German in 10 Minutes a Day Audio CD
# Italian in 10 Minutes a Day Audio CD
# Spanish in 10 Minutes a Day Audio CD

=== Language Map Series ===

# Arabic a Language Map
# Chinese a Language Map
# Dari a Language Map
# Farsi a Language Map
# French a Language Map
# German a Language Map
# Greek a Language Map
# Hawaiian a Language Map
# Hebrew a Language Map
# Inglés un Mapa del Lenguaje
# Italian a Language Map
# Japanese a Language Map
# Norwegian a Language Map
# Pashto a Language Map
# Polish a Language Map
# Portuguese a Language Map
# Russian a Language Map
# Spanish a Language Map
# Swahili a Language Map
# Vietnamese a Language Map

== References ==

{{reflist}}

{{DEFAULTSORT:Kershul, Kristine}}
[[Category:Living people]]
[[Category:American linguists]]
[[Category:Publishers (people)]]
[[Category:American education writers]]
[[Category:Writers from Seattle]]
[[Category:Year of birth missing (living people)]]