{{italic title}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox writer <!-- for more information see [[:Template:Infobox writer/doc]] -->
| name        = Thomas Nashe
| image       = Thomas-Nashe.jpg
| alt         = A crudely printed, full-length picture of a standing man. He is in Elizabethan-style clothing and chains are around his ankles
| caption     = A woodcut showing Nashe with chains on his ankles. From 
[[Richard Lichfield]]'s ''The Trimming of Thomas Nashe, Gentleman'' (1597)
| birth_date   = Baptised November 1567
| birth_place  = [[Lowestoft]], Suffolk
| death_date   = [[Circa|c.]] {{Death year and age|1601|1567}}
| death_place  =
| alma_mater  = [[St John's College, Cambridge]]
| occupation  = Playwright, poet, satirist
| nationality = English
| period      = ''circa'' 1589–1599
| movement    = 
| notableworks  = Summer's Last Will and Testament (1592)
| debut_work  = 
| influences  =
| influenced  = 
| relatives   ={{Plainlist |
* William Nashe, father
* Margaret Nashe (née Witchingham), mother}}
}}'''''Pierce Penniless, His Supplication to the Divell''''' is a tall tale, or a prose satire, written by [[Thomas Nashe]] and published in London in 1592.<ref>Drabble, Margaret, ed. ''The Oxford Companion to English Literature.''  Oxford University Press. 1985</ref> It was among the most popular of the Elizabethan pamphlets.  It was reprinted in 1593 and 1595,<ref name="Harrison">Harrison, G. B.  '‘Thomas Nashe, Pierce Penilesse, His Supplication to the Divell.'’ Corwen Press. 1924</ref> and in 1594 was translated into French.<ref>Gossse, Edmund.  ''The Unfortunate Traveller or the Life of Jack Wilton: With an Essay on the Life and Writings of Thomas Nash.''  The Chiswick Press. 1892</ref><ref>Stapleton, Michael, editor.The Cambridge Guide to English Literature.  Cambridge University Press. 1983</ref>
It is written from the point of view of Pierce, a man who has not met with good fortune, who now bitterly complains of the world’s wickedness, and addresses his complaints to the devil.  At times the identity of Pierce seems to conflate with Nashe's own.  But Nashe also portrays Pierce as something of an arrogant and prodigal fool.  The story is told in a style that is complex, witty, fulminating, extemporaneous, digressive, anecdotal, filled with wicked descriptions, and peppered with newly minted words and Latin phrases.  The satire can be mocking and bitingly sharp, and at times Nashe’s style seems to relish its own obscurity.<ref>’’The Columbia Encyclopedia.’’ Columbia University Press. Ed. William Bridgwater. 1963</ref><ref>Greenblatt, Stephen.  ‘’Will in the World; How Shakespeare Became Shakespeare’’.  Pimlico. 2005.  p. 202</ref>

== Elizabethan pamphlets ==

''Pierce Penniless'' was printed and published as one of the many pamphlets or short quarto books that provided lively material to the reading public.  Printed pamphlets had been a popular and longstanding tradition, but in London in the late 16th century, with the urban population booming, and literacy becoming widespread, they flourished.<ref>Greenblatt, Stephen.  ‘’Will in the World; How Shakespeare Became Shakespeare’’.  Pimlico. 2005.  p. 199</ref>  The content of these pamphlets often tended to be scandalous or scurrilous, but they contained a variety of material: Satires, war-of-words, anonymous attacks, topical issues, poetry, fiction, etc.<ref>’’Pamphlets and Pamphleteering in Early Modern Britain.’’  Joad Raymond. Cambridge University Press.  2003</ref>  [[Shakespeare]], in the dedication to his poem ''[[The Rape of Lucrece]]'' refers to its quarto publication as a pamphlet.<ref>[http://www.bartleby.com/70/49.html Shakespeare, William. ’’The Rape of Lucrece.’’  Full text online]</ref>

== The plague ==

Pamphlets also offered playwrights an opportunity to write and be published in those times when the plague had closed the theatres.  Such was the case with Shakespeare's ''Venus and Adonis'' and Nashe's ''Pierce Penniless'', which were both written and published when the theatres were closed from 1592–1593.<ref>Duncan-Jones, Katherine.  '’Shakespeare an Ungentle Life.'’  Methuen Drama 2001 p. 64</ref>
As Nashe says in his introductory "Private Epistle of the Author to the Printer", the first edition of ''Pierce Penniless'' was published in London, while Nashe was out of town, because "the fear of infection detained me with my Lord [his patron [[Ferdinando Stanley, 5th Earl of Derby|Lord Strange]]] in the country."  The plague is an underlying motive for the story itself—the seven deadly sins that Nashe's tale describes were said at the time to be the cause of the disease.<ref>Duncan-Jones, Katherine.  '’Shakespeare an Ungentle Life'’. Methuen Drama 2001 p. 66</ref> Indeed, the last words that Pierce addresses to the Devil in his supplication express the wish that certain souls will be accepted into Hell, and thus will "not let our air be contaminated with their sixpenny damnation any longer".

== Pamphlet war ==

The text of ''Pierce Penniless'' contains an attack on both Richard Harvey, the astrologer and the Marinists, who, as part of the [[Marprelate Controversy]], had been waging a pamphlet war attacking the episcopacy of the Anglican Church.<ref>The Oxford Companion to English Literature.  Edited by Margaret Drabble. 1985 Oxford University Press</ref> Near the midpoint of the story, Nashe's salvo begins:
::Gentlemen, I am sure you have heard of a ridiculous ass that ... wrote an absurd Astrological Discourse ... I have read over thy sheepish discourse of The Lamb Of God And His Enemies, and ... I could not refrain, but bequeath it to the privy, leaf by leaf as I read it, it was so ugly, dorbellical, and lumpish.

== The preface ==

In the “Private Epistle of the Author to the Printer”, which prefaces the second edition of ''Pierce Penniless'', Nashe refers to another pamphlet entitled ''[[Greene's Groats-Worth of Wit]]'' (1592), which contains this well known attack on William Shakespeare: 
::... there is an upstart crow, beautified with our feathers, that with his tiger's heart wrapped in a player's hide supposes he is as well able to bombast out a blank verse as the best of you, and being an absolute Johannes factotum is in his own conceit the only Shake-scene in a country<ref name="oxford-shakespeare">[http://www.oxford-shakespeare.com/Greene/Greenes_Groatsworth.pdf "Greene’s Groatsworth of Wit" Full text online]</ref>
From the moment ''Greene's Groats-Worth of Wit'' was published, people disbelieved that Robert Greene actually wrote it, let alone wrote it from his deathbed, as is purported in the pamphlet.<ref>Duncan-Jones, Katherine.  '’Shakespeare an Ungentle Life'’. Methuen Drama 2001 p. 48</ref> But the two who are then suspected of writing it, Thomas Nashe and Henry Chettle, each denied being the author.  
Among other reasons for considering that Nashe may be the author is Nashe's denial, which at first glance seems adamant, but is immediately followed by a kind of rueful confession that seems to have left a door open for interpretation:<ref>Duncan-Jones, Katherine.  ‘’Shakespeare an Ungentle Life’’. Methuen Drama 2001 p. 51</ref>
::... a scald trivial lying pamphlet called Greene's Groatsworth of Wit is given out to be of my doing. God never have care of my soul, but utterly renounce me, if the least word or syllable in it proceeded from my pen, or if I were any way privy to the writing or printing of it.  I am grown at length to see into the vanity of the world more than ever I did, and now I condemn myself for nothing so much as playing the dolt in print.<ref name="oxford-shakespeare" />

Nashe's character, "Pierce", occasionally has some harsh words for his unnamed fellow authors, which has led to speculation about who they might be.  Nashe refers to them variously as sonnet-composing “upstarts”, who are not as well-born or as well educated as himself, who have little Latin, but who are enjoying popular success in the theatre.<ref>Sams, Eric.  ‘’The Real Shakespeare; Retrieving the Early Years, 1564–1594.’’  Yale University Press. 1995. P. 75</ref>
Nashe then, in the “Private Epistle of the Author to the Printer”,  threatens anyone who might suggest his satire has particular victims:  “Let the interpreter beware,” he says, “...they shall know that I live as their evil angel to haunt them world without end.”<ref name="Harrison" />  However, Nashe’s satire would have no point unless it could be seen to be striking an actual target.<ref>Sams, Eric.  The Real Shakespeare; Retrieving the Early Years, 1564–1594.  Yale University Press.  1995.  p. 68</ref>
Nashe ends his epistle to his printer by mentioning that he would be available “... before the third impression [to] come and alter whatsoever may be offensive to any man."<ref name="Harrison" />

== The story ==

The story of ''Pierce Penniless'' is told by Pierce himself, who is a scholar, author and poet.  He begins his story by bewailing his own lack of good fortune, saying " ... have I more wit than all these (thought I to myself)? Am I better born, am I better brought up, yea, and better favoured, and yet am I a beggar?"  He sees no solution and finds that wickedness prevails.  "Divines and dying men may talk of hell," he says, "but in my heart her several torments dwell."

Having heard that a person might pawn his soul to the Devil for a thousand pounds, Pierce decides to seek a solution in that direction and appeal to the Devil, reasoning that if the Devil were to remove certain souls from the land of the living and recruit them into his domain where they belong, it would liberate and make available the wealth that they've been hoarding:  Gold—that “mighty controller of fortune and imperious subverter of destiny, delicious gold, the poor man's god, and idol of princes.” <ref name="Harrison" /><ref name="oxford-shakespeare_a" />

Pierce searches for the Devil, first in Westminster, then in the Exchange, and then in St. Paul’s, where he finds a Knight of the Post, (i.e. whipping post) – a term that refers to one who is a professional perjurer.<ref name="Harrison_a">Harrison, G. B. ‘‘Thomas Nashe, Pierce Penilesse, His Supplication to the Divell.’’ Corwen Press. 1924 P.vi</ref>  This man claims he can get a message to the devil.  Pierce hands him what he has written, a supplication addressed to “The Prince of Darkness” from Pierce Penniless who “wisheth increase of damnation and increase of malediction eternal.”

The supplication is based on the medieval theme of the Seven Deadly Sins,<ref name="Harrison_a" /> and enumerates each vice one after the other:  Greed, and his wife Dame Niggardise; Pride and his mistress, Lady Swine-Snout;  gluttony; sloth; etc.

Each vice is personified in the manner of a [[prosopopoeia]], and provides an opportunity for the story to introduce various sinners, who are described with rich detail—as though they are costumed to appear onstage.

The section devoted to the sin of "sloth" contains Nashe's "defense of Playes", which finds that plays are not slothful, but virtuous.  Pierce especially appreciates historical plays and gives Shakespeare's ''[[Henry VI, Part 1]]'' as an example.  As Pierce says:
::How would it have joyed brave [[John Talbot, 1st Earl of Shrewsbury|Talbot]] (the terror of the French) to think that after he had lien two hundred years in his tomb, he should triumph again on the stage, and have his bones new embalmed with the tears of ten thousand spectators at least (at several times), who, in the tragedian that represents his person, imagine they behold him fresh bleeding.<ref name="oxford-shakespeare_a">[http://www.oxford-shakespeare.com/Nashe/Pierce_Penilesse.pdf Nashe, Thomas.  ‘’Pierce Peniless’’.  1592.  full text online]</ref>
A notable passage occurs when Nashe describes the various types of drunkards one encounters in pubs and taverns.

Pierce signs this supplication:  “Your devilship's bounden execrator, Pierce Penilesse”.  He then asks the Knight of the Post a question that was of interest to Elizabethan Londoners: what is the nature of Hell and the Devil?<ref name="Harrison" />

The Knight begins to answer, but digresses into a story: the allegory of the wickedness of the bear, "a right earthly devil", which is seen as a reference to the [[Earl of Leicester]].<ref>Harrison, G. B. '‘Thomas Nashe, Pierce Penilesse, His Supplication to the Divell.'’ Corwen Press. 1924 P.vii</ref> Nashe's tale ends abruptly with the praises of a nobleman named Amuntas, who is believed to be the [[Earl of Southampton]].<ref name="Harrison" /><ref name="oxford-shakespeare_a" />

== References ==

{{reflist}}

{{DEFAULTSORT:Penniless, Pierce}}
[[Category:1592 books]]
[[Category:English-language books]]
[[Category:Satirical books]]
[[Category:Early Modern English literature]]
[[Category:William Shakespeare]]
[[Category:Works by Thomas Nashe]]