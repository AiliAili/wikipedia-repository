
<!-- Just press the "Save page" button below without changing anything! Doing so will submit your article submission for review. Once you have saved this page you will find a new yellow 'Review waiting' box at the bottom of your submission page. If you have submitted your page previously, either the old pink 'Submission declined' template or the old grey 'Draft' template will still appear at the top of your submission page, but you should ignore it. Again, please don't change anything in this text box. Just press the "Save page" button below. -->

{{Infobox person
|name                  = Martin Felsen
|image                 = Martin Felsen of UrbanLab.jpg
|image_size            = 220
|caption               = Martin Felsen, UrbanLab
|nationality           = [[United States|American]]
|birth_date            = {{birth year and age|1968}}
|birth_place           = [[Silver Spring, Maryland]], [[United States|U.S.]]
|death_date            = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
|death_place           = 
|alma_mater            = [[Virginia Tech]] (B.Arch., 1991), [[Columbia University]] (M.S., 1994)
|influences            = 
|influenced            = 
|practice              = 
|significant_buildings = [[Hennepin, Illinois]] Residence, Morgan Street Live/Work, Upton’s Naturals Headquarters, [[Smart Museum of Art]] Courtyard, Mohawk Residence, [[Echo Park, Los Angeles]] Residence, Hannah’s Bretzel. 
|significant_projects  =   
|significant_design    = 
|awards                = 2009 Latrobe Prize
}}

'''Martin Felsen''' (born 1968) is an [[United States|American]] [[architect]] and [[Fellow of the American Institute of Architects]] (FAIA). He directs [[UrbanLab]], a [[Chicago]]-based architecture and urban design firm. Felsen's projects range in scale from houses such as the [[Hennepin, Illinois]] Residence,<ref>{{cite web| url= http://www.dwell.com/houses-we-love/slideshow/new-grass-roots | date=November 2009|  work=[[Dwell magazine]] | title= New Grass Roots | author= Ryan Blitstein | accessdate=2014-04-25}}</ref><ref>{{cite web |url=http://www.urbanlab.com/live/hennepin.html |title= Hennepin House | publisher= [[UrbanLab]]| accessdate=2014-04-25}}</ref> mixed-use residential and commercial buildings such as Upton’s Naturals Headquarters,<ref>{{cite web| url=https://www.dwell.com/article/in-just-48-hours-a-chicago-livework-space-is-built-from-the-ground-up-using-concrete-panels-3be00429 | date=June 2015|  work= Dwell Magazine | title= In just 48 hours | author= Luke Hopping | accessdate=2016-09-18}}</ref><ref>{{cite web |url=http://www.urbanlab.com/work/uptons.html |title= Upton’s Naturals | publisher= [[UrbanLab]]| accessdate=2014-04-25}}</ref> public open spaces such as the [[Smart Museum of Art]] Courtyard <ref>{{cite web| url=http://smartmuseum.uchicago.edu/exhibitions/sculpture-garden/ | work= [[University of Chicago]] | title= Sculpture Garden and Reception Hall | accessdate=2014-04-25}}</ref><ref>{{cite web |url=http://www.urbanlab.com/culture/smartmuseum.html|title= Courtyard at the Smart Museum | publisher= [[UrbanLab]]| accessdate=2014-04-25}}</ref> at the [[University of Chicago]], and large scale, urban design projects such as Growing Water in Chicago<ref>{{cite web| url= http://archrecord.construction.com/news/daily/archives/080929future.asp | date=2008-09-29|  work= [[Architectural Record]] | title= Momentum Grows for Futuristic Scheme | author= David Sokol | accessdate=2014-04-25}}</ref><ref>{{cite web |url=http://www.urbanlab.com/urban/growingwater.html |title= Growing Water | publisher= [[UrbanLab]]| accessdate=2014-04-25}}</ref> and a masterplan (13 square kilometers / 5 square miles) for the Yangming Lake region of [[Changde]], China. Felsen was awarded the 2009 Latrobe Prize <ref>{{cite web| url= http://www.bdcnetwork.com/aia-college-fellows-announces-winner-2009-latrobe-prize | date=August 11, 2010|  work=[[Building Design+Construction]] | title= AIA College of Fellows announces winner of 2009 Latrobe Prize| author= Matt Tinder | accessdate=2014-04-25}}</ref> by the [[American Institute of Architects]], College of Fellows.

==Biography==
Felsen earned his [[Bachelor of Architecture]] from [[Virginia Tech College of Architecture and Urban Studies]] in 1991 and a [[Master of Science]] in Advanced Architectural Design from [[Columbia Graduate School of Architecture, Planning and Preservation]] in 1994.<ref>{{cite web| url= http://www.metropolismag.com/December-1969/Tschumi-Steps-Down/ | date=December 1999|  work=[[Metropolis (architecture magazine)]] | title= Tschumi Steps Down| author= Paul Makovsky | accessdate=2014-04-25}}</ref> Prior to founding his own firm, he worked for [[Peter Eisenman]],<ref>{{cite web |url= http://architecture.yale.edu/faculty/peter-eisenman |publisher=Yale School of Architecture |title=Profile of Peter Eisenman |accessdate=2014-04-24}}</ref> [[Stan Allen]],<ref>Princeton University: [http://soa.princeton.edu/content/stanley-t-allen Stan Allen profile], retrieved 19 February 2014</ref> and [[Office for Metropolitan Architecture|OMA]]/[[Rem Koolhaas]] in [[Rotterdam]].<ref>{{cite web |url=http://www.gsd.harvard.edu/cgi-bin/faculty/details.cgi?faculty_id=1137   |title= Faculty Bio | work= [[Harvard University Graduate School of Design]]}}</ref>
Felsen’s work has been exhibited at the International [[Venice Biennale of Architecture]], the [[Museum of Modern Art]], the [[National Building Museum]], and the [[Art Institute of Chicago]]. Felsen has been featured in publications such as [[Architect magazine]] <ref>{{cite web| url= http://www.architectmagazine.com/architects/urbanlab.aspx | date=January 17, 2012|  work=[[Architect magazine]] | title= UrbanLab| author= Ernest Beck | accessdate=2014-04-25}}</ref> and [[Dwell magazine]]. He has received many honors for his work, including several design awards from the [[American Institute of Architects]].

Since 1996, Felsen has taught architecture and urban design as an Associate Professor at the [[Illinois Institute of Technology]].<ref>{{cite web| url= http://www.iit.edu/arch/faculty/felsen_martin.shtml | title= IIT Faculty: Martin Felsen| accessdate=2014-04-25}}</ref> He has been a visiting professor at [[Columbia Graduate School of Architecture, Planning and Preservation]] and [[Washington University in St. Louis]].<ref>{{cite web| url= http://www.samfoxschool.wustl.edu/node/6479 | title= Sam Fox School, Master’s of Urban Design Studio | accessdate=2014-04-25}}</ref> Felsen served as Director of Archeworks,<ref>{{cite web| url= http://www.archeworks.org/ | title=Archeworks | accessdate=2014-04-25}}</ref> an alternative design school in Chicago from 2008-11.<ref>{{cite web| url= http://articles.chicagotribune.com/2008-01-17/features/0801150601_1_design-school-panel-discussion-school-year | date=January 17, 2008|  work=[[Chicago Tribune]] | title= Alternative design school | author= Blair Kamin | accessdate=2014-04-25}}</ref> Under his leadership, Archeworks completed several significant public interest design projects,<ref>{{cite web| url= http://www.archeworks.org/proj0910_mobilefoodcollective.cfm| title=Archeworks Project Archive | accessdate=2014-04-25}}</ref> and developed and organized two influential urban design workshops titled ''Infrastructures for Change''.<ref>{{cite web| url= http://www.metroplanning.org/news-events/event/59| title=Infrastructures for Change Workshop 2010 – Great Lakes Model | author= Metropolitan Planning Council |accessdate=2014-04-25}}</ref> During Felsen’s tenure, Archeworks’ projects were exhibited internationally at the [[Venice Biennale of Architecture]] <ref>{{cite web| url= http://www.workshopping.us/?p=23 | title= Workshopping at the 2010 Venice Biennale| accessdate=2014-04-25}}</ref> and [[The Architecture Foundation]] in London.<ref>{{cite web| url= http://www.architecturefoundation.org.uk/programme/2011/critical-infrastructures | title= Critical Infrastructures at The Architecture Foundation, London| accessdate=2014-04-25}}</ref> Felsen founded and served as editor of a new Archeworks publication titled ''Works''.<ref>{{cite web| url= http://issuu.com/archeworks/docs/works| title= Works 01| accessdate=2014-04-25}}</ref>

==Awards and honors==
*2017 named [[Fellow of the American Institute of Architects]] (FAIA) <ref>{{cite web| url= https://network.aia.org/cof/home| date=2014 | title= American Institute of Architects College of Fellows| accessdate=2017-01-28}}</ref>
*2016 AIA Regional & Urban Design Honor Award <ref>{{cite web| url= http://www.aiachicago.org/dea_archive/2016 | date=2014| title= AIA Awards | accessdate=2016-09-18}}</ref> – ''Masterplan for Changde, China''
*2015 [[Chicago Architecture Biennial]],<ref>{{cite web| url= http://archpaper.com/2015/10/bold-new-visions-for-the-future-city/#more-109399 | date=October 2015 | work=[[The Architect's Newspaper]] | title= Bold New Visions | author= Matt Shaw | accessdate=2016-09-18}}</ref> ''The State of the Art of Architecture''
*2014/15 MCHAP: Mies Crown Hall Architecture Prize, Nominated Work<ref>{{cite web| url= http://www.arch.iit.edu/prize/mchap/selected-works/2016/all/nominated | date=2015| title= MCHAP Awards | accessdate=2016-09-18}}</ref> – ''Mohawk Residence''
*2014 AIA Distinguished Building Honor Award <ref>{{cite web| url= http://www.aiachicago.org/dea/2014/morgan-street-live-work | date=2014| title= AIA Awards | accessdate=2014-11-05}}</ref> – ''Morgan Street Live/Work''
*2014 AIA Unbuilt Design Award, Special Recognition <ref>{{cite web| url= http://www.aiachicago.org/dea/2014/virtual-water | date=2014| title= AIA Awards | accessdate=2014-11-05}}</ref> – ''[[MoMA PS1]]''
*2014 [[Graham Foundation for Advanced Studies in the Fine Arts]] Publication Grant
*2013 AIA Sustainability Leadership Special Recognition Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2013dea/awards.asp?appId=1005 | date=2013| title= AIA Awards | accessdate=2014-04-25}}</ref> – ''[[MoMA PS1]]''
*2012 [[Venice Biennale of Architecture]],<ref>{{cite web| url= http://archrecord.construction.com/news/2012/06/120619-Preview-Americans-in-Venice.asp | date=June 19, 2012 | work=[[Architectural Record]] | title= Preview: Americans in Venice | author= Jayne Merkel | accessdate=2014-04-25}}</ref> the 13th International Exhibition
*2012 AIA Small Project Award - ''Video Arcade, [[Merchandise Mart]]''
*2011 AIA Regional & Urban Design Honor Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2011dea/awards.asp?appId=185 | date=2011| title= AIA Awards | accessdate=2014-04-25}}</ref> - ''Vertical Farming the [[Union Stock Yards]]''
*2010 AIA Interior Architecture Honor Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2010DEA/awards.asp?catID=2 | date=2010| title= AIA Awards | accessdate=2014-04-25}}</ref> - ''Hennepin House, [[Hennepin, Illinois]]''
*2010 [[Buckminster Fuller Challenge]], Honorable Mention<ref>{{cite web| url= http://www.bustler.net/index.php/article/the_2010_buckminster_fuller_challenge_finalists/| date=April 29, 2010| work= Bustler.net | title= 2010 Buckminster Fuller Challenge Finalists | accessdate=2014-04-25}}</ref>
*2010 Emerging Voices,<ref>{{cite web| url= http://archpaper.com/news/articles.asp?id=4332 | date=2010-03-03 | work=[[The Architect's Newspaper]] | title= UrbanLab | author= Julie Iovine | accessdate=2014-04-25}}</ref> [[Architectural League of New York]] - ''lecture delivered at the [[New Museum]]''
*2010 [[Venice Biennale of Architecture]],<ref>{{cite web| url= http://www.workshopping.us/?p=23 | title= Workshopping at the 2010 Venice Biennale| accessdate=2014-04-25}}</ref> Archeworks at the 12th International Exhibition
*2009 AIA Distinguished Building Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2009dea/awards.asp?catID=1 | date=2009| title= AIA Awards | accessdate=2014-04-25}}</ref> - ''Hennepin House, [[Hennepin, Illinois]]''
*2009 AIA Urban Design Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2009dea/awards.asp?catID=6 | date=2009| title= AIA Awards | accessdate=2014-04-25}}</ref> - ''Growing Water, Chicago''
*2009 Latrobe Prize,<ref>{{cite web| url= http://www.bdcnetwork.com/aia-college-fellows-announces-winner-2009-latrobe-prize | date=August 11, 2010|  work=[[Building Design+Construction]] | title= AIA College of Fellows announces winner of 2009 Latrobe Prize| author= Matt Tinder | accessdate=2014-04-25}}</ref> from the [[American Institute of Architects]] (AIA) College of Fellows. 
*2009 Global Visionary <ref>{{cite web| url= http://www.evl.uic.edu/files/events/ChicagoGlobalVisionaries.pdf | date=March 2, 2009| title=Global Visionaries WBEZ Press Release | accessdate=2014-04-25}}</ref> by [[WBEZ]], Chicago Public Radio 
*2008 AIA Unbuilt Design Award,<ref>{{cite web| url= http://www.aiachicago.org/special_features/2008dea/awards.asp?subID=1179| date=2008| title= AIA Awards | accessdate=2014-04-25}}</ref> Growing Water, Chicago
*2008 Excellence in Education Award, [[American Institute of Architecture Students]]
*2007 Dubin Family Young Architect Award,<ref>{{cite web| url= http://articles.chicagotribune.com/2007-12-12/features/0712110115_1_pritzker-architecture-prize-firm-award-howard-roark | date=December 12, 2007|  work=[[Chicago Tribune]] | title= Pitt making celebrity work for homes| author= Blair Kamin | accessdate=2014-04-25}}</ref> AIA Chicago
*2007 AIA Divine Detail Award <ref>{{cite web| url= http://www.aiachicago.org/special_features/2007DEA/awards.asp?subID=714| date=2007 | title= AIA Awards | accessdate=2014-04-25}}</ref> - ''Hannah’s Bretzel''
*2007 National Grand Prize, The City of the Future: A Design and Engineering Challenge,<ref>{{cite web| url= https://query.nytimes.com/gst/fullpage.html?res=9907EED6113FF93AA35751C0A9619C8B63 | date=February 9, 2007|  work=[[New York Times]] | title= Chicago Firm Urban Design Winner| author= [[Robin Pogrebin]] | accessdate=2014-04-25}}</ref> [[The History Channel]]
*2003 Emerging Visions Competition Prize, Chicago Architecture Club
*1995 Lloyd Warren Fellowship: 82nd Paris Prize, [[Van Alen Institute]]

==References==
{{Reflist}}

==External links==
*[http://www.urbanlab.com] UrbanLab’s official website

{{DEFAULTSORT:Felsen, Martin}}
[[Category:1968 births]]
[[Category:Living people]]
[[Category:American architects]]
[[Category:Virginia Tech alumni]]
[[Category:Columbia Graduate School of Architecture, Planning and Preservation alumni]]
[[Category:Illinois Institute of Technology faculty]]
[[Category:Artists from Chicago]]
[[Category:Architects from Illinois]]
[[Category:Jewish architects]]
[[Category:Architectural theoreticians]]