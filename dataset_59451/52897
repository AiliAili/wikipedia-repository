{{Infobox journal
| title = Spiritus
| cover = [[File:Spiritus.gif|Image:Spiritus.gif]]
| editor = Douglas E. Christie
| discipline = [[Religious studies]]
| abbreviation = Spiritus
| formernames = Christian Spirituality Bulletin
| publisher = [[Johns Hopkins University Press]]
| country =
| frequency = Biannually
| history = 1993-present
| openaccess = 
| impact = 
| impact-year = 
| website = http://www.press.jhu.edu/journals/spiritus/
| link1 = http://muse.jhu.edu/journals/spiritus/
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 45664645
| LCCN = 
| CODEN = 
| ISSN = 1533-1709
| eISSN = 1535-3117
| ISSNlabel = Spiritus
| ISSN2 = 1082-9008
| ISSN2label = Christian Spirituality Bulletin
}}
'''''Spiritus: A Journal of Christian Spirituality''''' is a biannual [[academic journal]] published by the [[Johns Hopkins University Press]]. It was established in 1993 as the ''Christian Spirituality Bulletin: Journal of the Society for the Study of Christian Spirituality'' and obtained its current title in 2001.<ref>[http://sscs.press.jhu.edu/publications/index.html Publications (page at website of The Society for the Study of Christian Spirituality] (accessed 2 Sep 2011)</ref> It is the official publication of the [[Society for the Study of Christian Spirituality]] and covers research on [[Christianity|Christian]] [[spirituality]] while fostering creative dialogue with non-Christian traditions. As such, it explores the relationship between spirituality and [[Cultural studies|cultural analysis]] using the disciplines of [[history]], [[philosophy]], [[theology]], and [[psychology]], among others. The journal includes original articles, reviews, and translations. Readership includes academics as well as a general audience. The current and founding [[editor-in-chief]] is [[Douglas Burton-Christie]] ([[Loyola Marymount University]]).

== Abstracting and indexing ==
''Spiritus'' is abstracted and indexed in the [[Christian Periodical Index]], [[Dietrich's Index Philosophicus]], [[International Bibliography of Book Reviews of Scholarly Literature in the Humanities and Social Sciences]], and the [[International Bibliography of Periodical Literature]].<ref name=indexingpage>{{cite web |url=http://www.press.jhu.edu/journals/spiritus/indexing.html |title=Indexing and Abstracting |publisher=The Johns Hopkins University Press |work=Spiritus |accessdate=2011-09-02}}</ref>

== See also ==
* [[List of theology journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.press.jhu.edu/journals/spiritus/}}
* [http://muse.jhu.edu/journals/spiritus/ ''Spiritus''] at [[Project MUSE]]
* [http://sscs.press.jhu.edu/ Society for the Study of Christian Spirituality]

{{DEFAULTSORT:Spiritus (Journal)}}
[[Category:Christianity studies journals]]
[[Category:Publications established in 2001]]
[[Category:Johns Hopkins University Press academic journals]]
[[Category:Biannual journals]]
[[Category:English-language journals]]