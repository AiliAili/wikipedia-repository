{{good article}}
{{For|the EP|Another One Rides the Bus (EP)}}
{{Infobox single
| Name         = Another One Rides the Bus
| Cover        = Another One Rides the Bus US Single.jpg
| Border       = yes
| Artist       = [["Weird Al" Yankovic]]
| from Album   = [[Another One Rides the Bus (EP)|Another One Rides The Bus]] and [["Weird Al" Yankovic (album)|"Weird Al" Yankovic]]
| Released     = February 1981 <small>(Initial release and TK re-release)</small><br/>May 3, 1983 <small>(Rock 'n Roll Records)</small>
| B-side       = "Gotta Boogie"
| Format       = 7" single
| Recorded     = September 14, 1980
| Genre        = [[Comedy music|Comedy]], [[polka]]
| Length       = 2:36
| Label        = Placebo Records <small>(Initial release)</small><br/>[[TK Records]] <small>(re-release)</small><br/>[[Rock 'n Roll Records]] <small>(1983 re-release)</small>
| Producer     = "Weird Al" Yankovic
| Writer = [[John Deacon]], "Weird Al" Yankovic{{#tag:ref|While Yankovic wrote new lyrics for the song, on February 2, 1981, he transferred copyright of the parody to John Deacon.<ref>{{cite web|title=Another One Rides The Bus|url=http://cocatalog.loc.gov/cgi-bin/Pwebrecon.cgi?v1=7&ti=1,7&Search%5FArg=Another%20One%20Rides%20The%20Bus&Search%5FCode=TALL&CNT=25&PID=zWgAFWLEmJWXk6x1Ox6hHvg26i&SEQ=20151117150118&SID=1|publisher=[[United States Copyright Office]]|accessdate=December 18, 2016}}</ref> This is reflected in the liner notes for the ''[[Another One Rides the Bus (EP)|Another One Rides the Bus]]'' EP, as well as the 1983 album ''[["Weird Al" Yankovic (album)|"Weird Al" Yankovic]]'', both of which list only Deacon as the parody’s writer.<ref name=linernotes/><ref>{{cite AV media notes|title = "Weird Al" Yankovic|others = [["Weird Al" Yankovic]]|year = 1983|url = https://www.discogs.com/Weird-Al-Yankovic-Weird-Al-Yankovic/release/3320346#images/6179495|type = liner|publisher = Rock 'n Roll Records|location = [[California]], [[United States]]}}</ref>|group="nb"}}
| Last single  = "[[My Bologna]]"<br/>(1979)
| This single  = "'''Another One Rides the Bus'''"<br/>(1981)
| Next single  = "[[Ricky (song)|Ricky]]"<br/>(1983)
| Misc         = {{Extra album cover
| Upper caption = 
| Type          = single
| Cover         = Another One Rides the Bus (Weird Al Yankovic single - cover art).jpg
| Alt           = A cartoon image of a crowded bus.
| Lower caption = The cover of the Dutch release of the single
| Border        = yes
}}
}}

"'''Another One Rides the Bus'''" is a 1981 [[parody]] of [[Queen (band)|Queen's]] "[[Another One Bites the Dust]]" by American comedy musician [["Weird Al" Yankovic]]. The song describes a person riding in a crowded public bus. It was recorded live on September 14, 1980, on the ''[[Dr. Demento|Dr. Demento Show]]'', hosted by Barret "Dr. Demento" Hansen. Accompanying Yankovic was [[Jon Schwartz (drummer)|Jon "Bermuda" Schwartz]], who would go on to be the parody artist's long-time drummer. 

The song became a hit on the ''Dr. Demento Show'', as well as an underground success. Hoping to capitalize on the success of the song, Yankovic originally released "Another One Rides the Bus" on an [[Another One Rides the Bus (EP)|EP of the same name]]. Later, the song was issued as a commercial single by [[TK Records]], which peaked at 104 on the U.S. ''Billboard'' [[Bubbling Under Hot 100 Singles]]. However, it quickly fell off the chart when TK subsequently closed. In 1983, Yankovic re-released the song on his [["Weird Al" Yankovic (album)|debut album]]. The song has been well received by critics, and [[Brian May]]{{emdash}}the guitarist of Queen{{emdash}}has expressed his amusement with Yankovic's parody.

==Background==
[[File:Bass player queen.jpg|thumb|left|"[[Another One Bites the Dust]]", the target of Yankovic's parody, was written by [[Queen (band)|Queen]] bassist [[John Deacon]].]]
In 1979, while he was still a student at [[California Polytechnic State University]] (Cal Poly) in [[San Luis Obispo, California]], [["Weird Al" Yankovic|Alfred “Weird Al” Yankovic]] recorded a rough parody of "[[My Sharona]]" by [[The Knack]] entitled "[[My Bologna]]". Fortuitous circumstances led to the song being released by [[Capitol Records]] on December 25, 1979.<ref name="praitb"/> Although the single managed to sell 10,000 copies a month after its release,<ref>Rabin and Yankovic 2012, p. 24.</ref> Yankovic soon learned that Capitol had no interest in promoting the record, or releasing a follow-up single. After graduating in 1980 with a Bachelor of Science degree in architecture, Yankovic still had an interest in releasing parody music, and soon turned his attention to "[[Another One Bites the Dust]]" by rock band [[Queen (band)|Queen]].<ref name="praitb"/> The song had been written by Queen bassist [[John Deacon]] and released on their 1980 album ''[[The Game (Queen album)|The Game]]''.<ref>{{cite AV media notes|title = The Game |others = Queen |year = 1980|type = liner|publisher = EMI Records|location = [[United Kingdom]]}}</ref>

==Writing and recording==

Lyrically, "Another One Rides the Bus" describe a person riding in a crowded public bus. In the first verse, the bus proceeds to pick up more people. The second verse discusses the various things that are touching the person (such as a suitcase and an elbow), and about how several of his personal items are missing (like a contact lens and a wallet). In the third verse, the speaker is trying to get fresh air but the bus' fan is broken and his window does not open. This causes him to exclaim that he "hasn't been in a crowd like this since I went to see [[The Who concert disaster|The Who]]." Finally, he laments about not getting off the bus sooner.<ref>Rabin and Yankovic, p. 26.</ref>

Yankovic debuted the song live on September 14, 1980, on the ''[[Dr. Demento|Dr. Demento Show]]'', hosted by Barret "Dr. Demento" Hansen. While practicing the song outside the sound booth, Yankovic met [[Jon Schwartz (drummer)|Jon "Bermuda" Schwartz]], who offered to provide percussion for his performance. Because Yankovic did not have a drum kit, Schwartz rhythmically struck Yankovic's accordion case as a way of keeping the beat.<ref>Rabin and Yankovic, p. 28.</ref> The version of "Another One Rides the Bus" that was recorded in 1980 and released in 1981 was later re-released in its original form on [["Weird Al" Yankovic (album)|Yankovic's eponymous debut album]] (1983).<ref name="praitb"/> 

The single's [[b-side]] is "Gotta Boogie", which was co-written by Joe Earley. The song is a play on words discussing a man with a "[[Dried nasal mucus|boogie]]" on his finger and his failure to get rid of it.<ref name=linernotes>{{cite AV media notes|title = Another One Rides the Bus|others = [["Weird Al" Yankovic]]|year = 1981|url = http://www.discogs.com/viewimages?release=2578636|type = liner|publisher = Placebo Records|location = [[California]], [[United States]]}}</ref> The version of "Gotta Boogie" included on this single was recorded in April 1980; this song also appeared on Yankovic's eponymous debut album, although in a re-recorded form.<ref name="recordingdates">{{Cite web|url=http://www.weirdal.com/rcdgdate.htm |title=Recording Dates |accessdate=June 26, 2010 |last=Yankovic |first=Alfred M. |authorlink="Weird Al" Yankovic |date=December 2007 |work=The Official "Weird Al" Yankovic Web Site |deadurl=yes |archiveurl=https://web.archive.org/web/20100709121948/http://www.weirdal.com/rcdgdate.htm |archivedate=July 9, 2010 |df= }}</ref>

==Release==

Much like "[[My Bologna]]", "Another One Rides the Bus" was a hit on the ''Dr. Demento Show'', and Dr. Demento himself said:

<blockquote>For the next few weeks we got twice as many requests for "Another One Rides the Bus" as for everything else put together. Thank goodness I had a tape rolling! We even got it in stereo. Over the next couple of months that tape was duplicated and re-duplicated all over the world, as the song took on a life of its own. [...] The Dr. Demento Show gained a couple of dozen new station affiliates just because of that song.<ref name="praitb"/></blockquote>

Hoping to capitalize on the parody's success, Yankovic attempted to get a record contract. He and Schwartz quickly began duplicating the original "Another One Rides the Bus" recording on cassettes, and the two then sent them to anyone who might be interested in the spoof. Schwartz later joked, "Some were cleverly targeted to record executives, others were randomly addressed. I think one went to Santa Claus, [[wikt:care of|care of]] the North Pole."<ref>Klamm 2015, p. 97.</ref>

Eventually, Yankovic borrowed some money from Dr. Demento and pressed up one thousand copies of [[Another One Rides the Bus (EP)|a four-track EP]] by himself.<ref name=page33>Rabin and Yankovic, p. 33.</ref> Yankovic then distributed this EP to various record stores, selling them through consignment deals.<ref>Rabin and Yankovic, p. 31.</ref> Yankovic released the record under Placebo Records, a one-off label founded by Yankovic for the sole purpose of distributing the EPs.<ref name="praitb"/><ref name="rate">{{Cite web| url = http://weirdal.com/archives/miscellaneous/rare-items/gallery/rare-items/singles | title = The Placebo EP 1981, Placebo (7") | accessdate = June 26, 2010 | last = Yankovic | first = Alfred M. | authorlink = "Weird Al" Yankovic |date=December 2007 | work = The Official "Weird Al" Yankovic Web Site}}</ref><ref name=page33/> Due to the underground success of the record, Yankovic secured a short-lived record deal with [[TK Records]], and in February 1981, the label released "Another One Rides the Bus" as a single.<ref name="praitb">{{cite AV media notes|title = Permanent Record: Al in the Box|others = [["Weird Al" Yankovic]]|year = 1994|url = http://dmdb.org/al/booklet.html|first = Barret|last = Hansen|authorlink = Dr. Demento|type = liner|publisher = [[Scotti Brothers Records]]|location = [[California]], [[United States]]}}</ref> The record was rush-released, but managed to chart on the [[Bubbling Under Hot 100 singles]]. However, TK abruptly closed due to financial troubles and the single fell off the charts.<ref name="praitb"/>

Domestically, the single was released in a generic TK Records sleeve. International versions of the single, however, featured different artwork. The [[Netherlands|Dutch]] release, for instance, featured artwork depicting a crowded bus.<ref>Rabin and Yankovic, p. 27.</ref> According to Yankovic's official site, this version of the "single used artwork similar to what would appear on Al's first album."<ref>{{Cite web|url=http://weirdal.com/archives/miscellaneous/rare-items/gallery/rare-items/singles |title=Rare Items: Another One Rides the Bus 1981 |accessdate=December 18, 2016 |last=Yankovic |first=Alfred M. |authorlink="Weird Al" Yankovic |date=December 2007 |work=The Official "Weird Al" Yankovic Web Site |df= }}</ref>

==Reception==
[[File:Brian-May with red special.jpg|thumb|right|[[Brian May]], the guitarist of [[Queen (band)|Queen]], found the song to be "extremely funny".<ref name=brianquote/>]]
The song was well-received by music critics. In a later review of Yankovic's debut album, Eugene Chadbourne of [[Allmusic]] called the parody "a classic piece of musical humor" that showcased Yankovic's ability to knock "the wind out of any pretentious, overblown rock anthem by slightly adjusting the lyrical content."<ref name="allmusic">{{cite web | url         = {{Allmusic|class=album|id=r82658|pure_url=yes}} | title       = 'Weird Al' Yankovic – Overview | accessdate  = December 17, 2016 | work        = [[Allmusic]] | publisher   = [[Macrovision]] | location    = [[Ann Arbor, Michigan]], [[United States|USA]]}}</ref> [[Nathan Rabin]], in the book ''Weird Al, The Book'' (2012), praised the song, writing:

<blockquote>Though the modest Yankovic himself laughs off the notion, we can all agree that "Another One Rides the Bus" embodies the anarchic spirit of punk rock just as much as anything [[Johnny Rotten]] or [[The Clash]] ever recorded. It's the essence of punk: an enraged, defiant malcontent with a long list of grievances screaming his pain to an indifferent world. [...] Yankovic came to symbolize a curiously ubiquitous fixture of [[new wave]]: the enraged geek.<ref>Rabin and Yankovic, pp. 29–30.</ref></blockquote>

[[Brian May]], the guitarist of Queen, found the song amusing, and said in an interview, "There's been a few cover versions [of 'Another One Bites the Dust'] of various kinds, notably 'Another One Rides the Bus', which is an extremely funny record by a bloke called 'Mad Al' or something in the [United] States—it's hilarious."<ref name=brianquote>Purvis 2012.</ref>

==Live video==

Although no official music video was created for this single, Yankovic and Schwartz performed "Another One Rides the Bus" on ''[[The Tomorrow Show]]'' with [[Tom Snyder]]. During the performance, Yankovic played his accordion and Schwartz banged on Yankovic's accordion case and played bulb horns.<ref name=page30>Rabin and Yankovic, p. 30.</ref> ''The Tomorrow Show'' performance was later included on the ''[["Weird Al" Yankovic: The Ultimate Video Collection]]'' (2003) DVD as a bonus feature.<ref>{{Citation | year       = 2003 | title      = 'Weird Al' Yankovic: The Ultimate Video Collection | publisher  = [[Volcano Entertainment]] | publication-place = [[Los Angeles]], [[New York City]] | type = DVD }}</ref>

==Track listing==

# "Another One Rides the Bus" &ndash; 2:36
# "Gotta Boogie" &ndash; 2:21

==Chart positions==

{| class="wikitable"
!Chart (1981)
!Peak<br/>Position
|-
|align="left"|U.S. ''Billboard'' [[Bubbling Under Hot 100 Singles]]            
|align="center"|104<ref>{{Cite web|url=http://weirdal.com/archives/awards/ |title=Awards |accessdate=December 18, 2016 |last=Yankovic |first=Alfred M. |authorlink="Weird Al" Yankovic |work=The Official "Weird Al" Yankovic Web Site }}</ref>
|-
|}

==See also==
*[[List of singles by "Weird Al" Yankovic]]
*[[List of songs by "Weird Al" Yankovic]]

==Notes==
{{reflist | group="nb"}}

==References==

===Footnotes===

{{reflist|2}}

===Bibliography===

*{{cite book|last=Rabin|first=Nathan|title=Weird Al: The Book|publisher=[[Abrams Image]]|isbn=9781419704352|author2=Yankovic, Alfred M.|date=September 25, 2012}}
*{{cite book|author=Klamm, Jason|title=Post-Modem: The Interwebs Explained|year=2015|publisher=[[Lulu.com]]|isbn=9781329037465}}
*{{cite book|last1=Purvis|first1=Georg|title=Queen: The Complete Works|date=March 13, 2012|publisher=[[Titan Books]]|isbn=9780857685513}}

{{"Weird Al" Yankovic}}

{{DEFAULTSORT:Another One Rides The Bus}}
[[Category:"Weird Al" Yankovic songs]]
[[Category:1981 singles]]
[[Category:Queen (band)]]
[[Category:Songs with lyrics by "Weird Al" Yankovic]]
[[Category:Songs about buses]]
[[Category:1980 songs]]
[[Category:Songs written by John Deacon]]

[[pt:"Weird Al" Yankovic (álbum)#Another One Rides the Bus (EP)]]