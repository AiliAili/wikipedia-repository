{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=P.10
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Experimental aircraft|Experimental]] two seat [[biplane]]
 | national origin=[[United Kingdom]]
 | manufacturer=[[Boulton & Paul Ltd]]
 | designer=[[John Dudley North]]
 | first flight= no record of flight
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Boulton & Paul P.10''' was a two-seat, single-engined [[biplane]] built just after [[World War I]] to develop techniques for the construction of all steel aircraft.  It is also notable for its first use of  plastic as a structural material.<ref name="Brew2">{{Harvnb|Brew|1993|pages=170–3}}</ref><ref name="Flight55">[http://www.flightglobal.com/pdfarchive/view/1955/1955%20-%200934.html ''Flight''  8 July 1955 p.44]</ref> Only one P.10 was built and it attracted much attention; but it probably never flew.<ref name="Brew2"/>

==Design and development==
In the first decade of the 20th century, Boulton and Paul besides their wood construction shops also had plants for iron making, wire fencing and structural steel for buildings.<ref name="Brew1">{{Harvnb|Brew|1993|pages=3}}</ref>  It is not surprising therefore that when they became involved with aircraft production in [[World War I]] and then began their own designs they soon looked to the use of steel airframes. The Boulton & Paul P.10 was the first example and only their third design.

The P.10 was <ref name="Brew2"/> a single-engined two-seat biplane with an airframe of high tensile steel, zinc treated and varnished against corrosion. It had single bay wings with no stagger or sweep.  Both wings had the same span and the same constant chord.  Rather like the earlier [[Boulton Paul P.6|P.6]], the interwing gap was large and equal to the chord, putting the upper wing high above the fuselage. Either side of the centre section were a pair of vee struts to front and rear spars, assisted by another strut from front spar to the engine bulkhead  The wings were built around two I section spars, each a box section constructed from rolled strips.<ref name="Brew2"/>  The forward spar had four of these strips, the rear two.<ref name="Flight20">[http://www.flightglobal.com/pdfarchive/view/1920/1920%20-%200011.html ''Flight''  1 January 1920 p.11-4]</ref>  The P.10 was displayed at the Paris ''Salon d'Aeronautique'' in 1919 without its fabric covering, the wing construction was in plain view.<ref name="Flight20"/> There were interconnected ailerons on both wings.  The P.10 had a small fin, which with its horn balanced rudder formed a teardrop shape.  The tail, with unbalanced elevators was strut braced to the fin.

The front fuselage was built on four tubular longerons, but from leading edge rearwards it consisted of a set of oval formers with stringers.<ref name="Brew2"/>  The greatest novelty of the P.10 was that this part of the fuselage was not only a monocoque structure (still fairly unusual at the time), but a monocoque of steel with a load-bearing plastic skin riveted between the formers and stringers.<ref name="Flight55"/>  Specifically, the plastic was [[Bakelite]]-Dilecto, a hard, synthetic cellulose-formaldehyde product.<ref name="Brew2"/>  The company claimed it was proof against fire, heat, humidity and insects.  This was the first use of structural plastic in an airframe<ref name="Brew2"/> and perhaps the last for another sixty years.<ref name="Brew3">{{Harvnb|Brew|1993|pages=29}}</ref>

This construction gave the fuselage a smooth, rounded "torpedo-like"<ref name="Brew2"/> look, which was enhanced by the close cowling of the 100&nbsp;hp (75&nbsp;kW) [[Cosmos Lucifer]] radial engine, though its three cylinders projected out a long way for cooling.  This engine drove a four bladed wooden propeller, the only wooden part of the airframe. Boulton Paul had made their own propellers during the war. The front cockpit was at the wing leading edge with a rather distant second cockpit under the trailing edge, about 5&nbsp;ft 6 in (1.68 m) aft.  Dual control was fitted.  As exhibited in Paris,<ref name="Flight20"/> the P.10 had a tall single axle undercarriage mounted on a pair of vee-struts on either side.  These were [[Bungee cord|bungee]] sprung, though there were plans for oleo dampers.<ref name="Brew2"/>

The P.10 was not the first metal British aircraft, for the unlikely looking [[Seddon Mayfly]] holds that priority; but it never had a realistic hope of flight.<ref name="Brew2"/> One lesser novelty, which was to become a standard Boulton & Paul feature was the mounting of the Lucifer on a hinge so that it could be swung sideways for servicing without disconnecting pipework etc.<ref name="Brew2"/>

The P.10 made a big impact at the 1919 Paris show, with ''Flight'' describing it as "the machine of the show".<ref name="Flight20"/>  Nonetheless, after the show the P.10 disappeared from sight.  There is no record of it flying, though there seems no reason why it should not have been capable.<ref name="Brew2"/>  There are reports of it being damaged when the engine failed.<ref name="Brew2"/> It was not at the Paris show of 1920. Surprisingly, the delicate structures of one wing and the tail unit have survived.  These parts are now at the [[Bridewell Museum]]<ref>[http://www.museums.norfolk.gov.uk/default.asp?Document=200.22 Bridewell Museum] {{webarchive |url=https://web.archive.org/web/20091009172125/http://www.museums.norfolk.gov.uk/default.asp?Document=200.22 |date=9 October 2009 }}</ref>  in Boulton & Paul's home town of Norwich, though this museum is closed for refurbishment as of 2009. It intends to re-open in 2011.

==Specifications==
{{aerospecs
|ref={{harvnb|Brew|1993|page=173}}  The performance figures are estimates only.<!-- reference -->
|met or eng?=eng<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->
|crew=1
|capacity=1 passenger
|length m=7.93
|length ft=26
|length in=0
|span m=9.14
|span ft=30
|span in=0
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=3.66
|height ft=12
|height in=0
|wing area sqm=28.7
|wing area sqft=309
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=501
|empty weight lb=1,104
|gross weight kg=771
|gross weight lb=1,700
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|eng1 number=1
|eng1 type=[[Cosmos Lucifer]] 3-cylinder air-cooled radial engine
|eng1 kw=75<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|max speed kmh=167
|max speed mph=at 1,000 ft (305 m) 104
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=at 90 mph (145 km/h) and 3,000 ft (914 m) 5<!-- if range unknown -->
|endurance min=0<!-- if range unknown -->
|ceiling m=4,270
|ceiling ft=14,000
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=3.18
|climb rate ftmin=to 5,000 ft (1524 m) 625
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Boulton Paul}}

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
*{{cite book |title= Boulton Paul Aircraft since 1915 |last= Brew |first= Alec |coauthors= |edition= |year= 1993|publisher= Putnam|location= London|isbn= 0-85177-860-7|ref=harv}}
{{refend}}
*[http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%201610.html ''Flight'' 18 December 1919]

==External links==
*[https://shinealightproject.wordpress.com/2015/03/29/a-real-showstopper/ A Real Showstopper] - description of P.10 and image of surviving part
{{Boulton Paul aircraft}}

[[Category:Boulton Paul aircraft|P.010]]
[[Category:British experimental aircraft 1910–1919]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]