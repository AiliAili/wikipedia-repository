{{Use British (Oxford) English|date=December 2011}}
'''ISO 4''' ('''Information and documentation – Rules for the abbreviation of title words and titles of publications''') is an [[international standard]], which defines a uniform system for the abbreviation of serial titles, i.e., titles of publications such as scientific journals that are published in regular installments.<ref>{{cite web |url= http://www.issn.org/services/online-services/access-to-the-ltwa/ |title=Online services: Access to the LTWA| publisher=International Standard Serial Number International Centre|accessdate=22 October 2014}}</ref> The [[International Standard Serial Number#Code assignment|ISSN International Centre]], which the [[International Organization for Standardization]] (ISO) has appointed as the [[registration authority]] for ISO 4, maintains the "List of Title Word Abbreviations", which are standard abbreviations for words commonly found in serial titles.

One major use of ISO&nbsp;4 is to abbreviate the names of scientific journals using the '''List of Title Word Abbreviations''' (LTWA). For instance, when citing a paper from the ''[[European Physical Journal]]'', the ISO&nbsp;4 standard prescribes "''Eur. Phys. J.''" to be used as the abbreviation. ISO&nbsp;4 is particularly efficient for abbreviating journal titles: for example, the letter "J." is the standard abbreviation for the word "journal".

== See also ==
* [[ISSN]]
* [[CODEN]]

== References ==
{{Reflist}}

== External links ==
* [http://www.issn.org/services/online-services/access-to-the-ltwa/ List of Title Word Abbreviations]

{{ISO standards}}

{{Use DMY dates|date=February 2015}}
[[Category:ISO standards|#00004]]

{{Standard-stub}}