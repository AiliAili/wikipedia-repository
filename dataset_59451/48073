{{Other people|John Garvey|John Garvey (disambiguation){{!}}John Garvey}}
{{Infobox musical artist
| name                = John Garvey
| image               = 
| image_size          = 
| caption             =
| birth_name          = John Charles Garvey
| native_name         = 
| native_name_lang    = 
| alias               = 
| background          = non_vocal_instrumentalist
| birth_date          = {{birth date|1921|3|17|mf=y}}
| birth_place         = [[Canonsburg, Pennsylvania]], U.S.A.
| origin              = 
| death_date          = {{death date and age|2006|7|18|1921|3|17|mf=y}}
| death_place         = [[Silver Spring, Maryland]],<ref>{{cite journal|title=John Garvey Memorial Celebration Held on Campus|journal=Sonorities|date=Spring 2007|page=4|url=http://illinois.edu/cms/3337/sonorities_07.pdf|accessdate=2016-10-01}}</ref> U.S.A.
| genre               = [[Chamber Music]], [[Jazz]], [[Russian traditional music|Russian Folk Music]]
| occupation          = Musician, academic
| instrument          = Viola
| years_active        = 1940s–1990s
}}

'''John Garvey''' (March 17, 1921<ref name="GarveyUIArchives">{{cite web
|title=John Garvey Music and Papers, The Sousa Archives and Center for American Music
|url=http://archives.library.illinois.edu/archon/index.php?p=collections/controlcard&id=2203
|website=University of Illinois Archive Holdings Database
|publisher=University of Illinois
|accessdate=2016-09-13
}}</ref>
- July 18, 2006<ref name="NewsGazette2006-08-03">{{cite news
| title=Former music professor championed jazz studies at UI
| url=http://www.news-gazette.com/news/local/2006-08-03/former-music-professor-championed-jazz-studies-ui.html
| newspaper=[[The News-Gazette (Champaign-Urbana)|The News-Gazette]]
| accessdate=2016-09-13
| place=[[Champaign, Illinois]] 
| date=August 3, 2006
}}</ref>) was an American musician, orchestra leader, and academic who played [[viola]] in the Walden String Quartet for 23 seasons, introduced a jazz curriculum at the [[University of Illinois at Urbana–Champaign|University of Illinois]], and created its Jazz Big Band which he led until his retirement from the university in 1991. The jazz band dominated collegiate jazz festival awards in its early days and in 1969 was chosen by the state department to tour the [[USSR]] and [[Eastern Europe]]. Many members of Garvey's jazz bands went on to successful careers as professional musicians and academics.

==Early Years==

Inspired by a talk given by the violinist of the Chautauqua Trio, Garvey began studying violin at age 7. By 14 he was commuting from his home in Reading, PA every other week to Temple University in Philadelphia to study violin with Alfred Lorenz,
<ref name="Eagle1936-12-23">{{cite news
| title=Musical Talent Plentiful In Two Reading Families
| url=https://news.google.com/newspapers?id=YPRlAAAAIBAJ&sjid=0WQNAAAAIBAJ&pg=512,5030353
| work=[[Reading Eagle]] 
| place=[[Reading, Pennsylvania]] 
| at=pg 2, cols 6-8 
| date=December 23, 1936 
| accessdate=2016-09-13
}}</ref>
a violist with the Philadelphia Orchestra
<ref name="StowkowskiLorenzEntry">{{cite web
|last1=Huffman
|first1=Larry
|title=Principal Musicians of the Philadelphia Orchestra – Alfred Lorenz
|url=http://www.stokowski.org/Philadelphia_Orchestra_Musicians.htm#Alfred_Lorenz
|website=The Stokowski Legacy (www.stokowski.org)
|accessdate=2016-09-13
}}</ref>
He went on to major in music at Temple.
<ref name="DailyIllini1948-09-28">{{cite news
| title=Beethoven, Haydn, Season Starters for Waldens
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19480928.2.66&srpos=1&e=--1921----194-en-50--1--txt-txIN-John+Garvey------
| accessdate=2016-09-13
| newspaper=[[Daily Illini|The Daily Illini]]
| place=[[Champaign, Illinois]] 
| date=September 28, 1948
| at=pg 5, col 4 
}}</ref>

==As a Jobbing Musician==
After college Garvey played with the Philadelphia Symphony and Columbus Philharmonic. In March 1943, Garvey joined a dance band that played pop-styled classical music led by a classically trained violinist named [[Jan Savitt]], "The Stokowski of Swing".
<ref name="Eagle1943-08-19">{{cite news
| title=David Garvey Presents Recital in Virginia
| url=https://news.google.com/newspapers?id=hI0hAAAAIBAJ&sjid=EpgFAAAAIBAJ&pg=2288%2C839140
| accessdate=2016-09-13
| work=[[Reading Eagle]] 
| place=[[Reading, Pennsylvania]] 
| date=August 19, 1943
| at=pg 3, col 2 
}}</ref> 
<ref name="Billboard1944-07-08">{{cite journal
| title=ON THE STAND
| journal=The Billboard
| date=July 8, 1944
| volume=56
| issue=28
| at= p.17, col 2
| url=http://search.proquest.com/docview/1032338326/
| accessdate=2016-09-13
}}</ref>
In the summer of 1948 he was director of the summer chamber music program at Ball State University.<ref name="DailyIllini1948-09-28" />

==Academia==
In 1948, Garvey joined the Walden String Quartet<ref name=DailyIllini1948-09-28 /> in an until-then frequently changing viola chair. He was to remain with the quartet until 1971. The group had recently become quartet-in-residence at the [[University of Illinois at Urbana–Champaign|University of Illinois]].<ref name=DailyIllini1947-05-28>{{cite news
| title=Ridenour Named New Dean of Graduate School, Kuypers Named as Director of UI Music Department By Board of Trustees
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19470528.2.1&srpos=5&e=-------en-20--1-byDA-txt-txIN-John+Kuypers------
| newspaper=[[Daily Illini|The Daily Illini]] 
| place=[[Champaign, Illinois]] 
| at=pg 1, cols 1-2
| date=May 28, 1947 
| accessdate=2016-09-13
}}</ref><ref name=OxfordWSQEntry>
{{cite encyclopedia
 | title = Walden String Quartet
 | encyclopedia = Oxford Music Online
 | accessdate = 2016-09-13
 | publisher = Oxford University Press
 | url = http://www.oxfordmusiconline.com/subscriber/article/grove/music/44222
}}</ref>
In addition to touring and recording, the Quartet were members of the music faculty. Garvey was hired as Instructor in the department<ref name=1948-49UofIDirectory>
{{cite book
|title=Directory, Faculty and Students, 1948–1949
|date=October 1948
|publisher=The University of Illinois Press
|location=Urbana, Illinois
|page=106
|url=https://archive.org/stream/studentstaffdir4849univ#page/n109/mode/2up|accessdate=2016-09-13
}}</ref> and within a few years was promoted to Assistant Professor.<ref name=1951-52UofIDirectory>{{cite book
|title=Directory, Staff and Students, 1951–1952
|publisher=The University of Illinois Press
|location=Urbana, Illinois
|page=132
|url=https://archive.org/stream/studentstaffdir5152univ#page/n135
|accessdate=2016-09-13
}}</ref> In May 1952, Garvey became head of the music committee of the university’s Festival of Contemporary Arts.<ref name=DailyIllini1952-05-27>{{cite news
| title=GARVEY HEADS COMMITTEE
|url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19520527.2.77&srpos=10&e=-------en-20--1--txt-txIN-Garvey+Contemporary+Arts+Festival------| place=[[Champaign, Illinois]] 
| newspaper=[[Daily Illini|The Daily Illini]] 
| at=pg 6, col 3
| date=May 27, 1952 
| accessdate=2016-09-30
}}</ref><ref name=HarrisonThesis>{{cite book
 | last1=Harrison
 | first1=Albert Dale
 | title=A History of the University of Illinois School of Music, 1940–1970
 | date=1986
 | publisher=Ed.D. Thesis, University of Illinois
 | pages=281–285
 | url=http://search.proquest.com/docview/303485936| accessdate=2016-10-12
}}
</ref>
In 1959, he was made a full professor.
<ref name=BoardOfTrusteesMinutes1958-1960>{{cite book
 | title=Transactions of the Board of Trustees, July 1, 1958 to June 30, 1960 
 | date=1960
 | publisher=University of Illinois Board of Trustees
 | page=696
 | url=http://www.trustees.uillinois.edu/trustees/minutes/1960/1958-1960-uibot.pdf
}}</ref>

==The University Jazz Program==

Work with [[Harry Partch]] on "The Bewitched", a music and dance piece for the 1957 festival, rekindled Garvey's interest in jazz,<ref name=HarrisonThesis/>{{rp|281}} 
<ref name=DailyIllini1957-03-19>{{cite news
| title=The "Boo", or Bamboo
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19570319.2.4&srpos=1&e=--1955---1958--en-20--1--txt-txIN-Partch+Garvey------#
| place=[[Champaign, IL]] 
| newspaper=[[The Daily Illini]] 
| at=pg 1, col 2
| date=March 19, 1957
| accessdate=2016-10-09
}}</ref>
and for the 1959 festival he invited the [[Modern Jazz Quartet]] to play with a student jazz band and a string ensemble.<ref name=HarrisonThesis/>{{rp|281}}<ref name=ChicagoDailyTribune1959-02-22>{{cite news
| title=U. OF I. TO GIVE JAZZ PLACE IN ARTS FESTIVAL
| url=http://search.proquest.com/docview/182219047/
| place=[[Chicago, IL]] 
| newspaper=[[Chicago Tribune|The Chicago Daily Tribune]] 
| at=pg 32, col 4
| date=February 22, 1959 
| accessdate=2016-09-30
}}</ref>

Because of student interest in continuing the jazz band, Garvey sought funding from the School of Music, but faced strong opposition.<ref name=HarrisonThesis /> By October 1960 he was able to get $150 from the school, and obtained additional money and administrative support from the student union.
<ref name=DailyIllini1960-10-19>{{cite news
| title=Union Board Agrees to Cosponsor Jazz Program with School of Music
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19601020.2.60&srpos=2&e=-------en-20--1--txt-txIN-Union+Board+to+Cosponsor+Jazz------
| place=[[Champaign, IL]] 
| newspaper=[[The Daily Illini]] 
| at=pg 12, col 4-5
| date=October 19, 1960 
| accessdate=2016-09-30
}}</ref>

Garvey and the band made their debut at a routine Thursday morning School of Music function on December 8, 1960.<ref name=HarrisonThesis/>{{rp|283}} In April 1964, they entered the [[Notre Dame Collegiate Jazz Festival|Collegiate Jazz Festival]] at [[Notre Dame University]]
<ref name=CJFprogram1964>{{cite web
| title=Collegiate Jazz Festival 1964 Program
| url=http://archives.nd.edu/ndcjf/dcjf1964.pdf
| place=[[South Bend, IN]] 
| date=April 1964 
| at=pg 8
| accessdate=2016-09-30
}}</ref>
for the first time and took the prize for Best Big Band.

In 1965 and 1966, they were finalists at the festival.
<ref name=CJFprogram1967>{{cite web
| title=Collegiate Jazz Festival 1967 Program
| url=http://archives.nd.edu/ndcjf/dcjf1967.pdf
| place=[[South Bend, IN]] 
| date=March 1967 
| at=pg 12
| accessdate=2016-09-30
}}</ref>
and in March 1967, the band won Best Overall Jazz Group. 
<ref name=CJFprogram1968>{{cite web
| title=Collegiate Jazz Festival 1968 Program
| url=http://archives.nd.edu/ndcjf/dcjf1968.pdf
| place=[[South Bend, IN]] 
| date=March 1968 
| at=pg 16
| accessdate=2016-09-30
}}</ref>
In December 1967, Garvey's band was officially sanctioned by the School of Music.<ref name=HarrisonThesis/>{{rp|285}}

In 1968, and 1969 they again won "Best Overall Jazz Group", the first ensemble to do so three years in a row. The prize at the 1968 and 1969 festivals was an invitation to perform at the [[Newport Jazz Festival]] ,
<ref name=DailyIllini1969-03-19>{{cite news
| title=University Band Captures Ten Awards at Notre Dame Fest
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19690319.2.13&srpos=2&e=-------en-20--1--txt-txIN-%22Collegiate+Jazz+Festival%22+Illinois------
| place=[[Champaign, IL]] 
| newspaper=[[The Daily Illini]] 
| at=pg 3 col 4
| date=March 19, 1969 
| accessdate=2016-09-30
}}</ref>
for which they received praise from critics [[Leonard Feather]]<ref name=Feather1968-07-13>{{cite news
| author=Leonard Feather
| title=Jazz Fest Amateurs Top Pros
| url=http://search.proquest.com/docview/1515741321/
| place=[[Austin, Texas|Austin,TX]] 
| newspaper=[[Austin American-Statesman|The Austin Statesman]] 
| at=pg 18, col 1
| date=July 13, 1968 
| accessdate=2016-09-30
}}</ref> and [[John S. Wilson (music critic)|John S. Wilson]].
<ref name=NYTimes1968-07-08>{{cite news
| author=John S. Wilson
| title=It's Newcomers Day at Newport
| url=http://search.proquest.com/docview/118282617/
| place=[[New York, NY]] 
| newspaper=[[The New York Times]] 
| at=pg 47, col 1
| date=July 8, 1968 
| accessdate=2016-09-30
}}</ref>
 
The 1969 Downbeat Readers Poll ranked the band 16th place in the jazz Big Band category, the only college band in the entire poll.<ref name=HarrisonThesis/>{{rp|285}}

The accolades led to sponsorship by the US State Department for an eight-week tour of Eastern Europe and Scandinavia in 1968, and a six-week tour of the Soviet Union in 1969.
<ref name=ChicagoTribune1969-11-01>{{cite news
| title=U. of I. Jazz Group to Tour Soviet Union
| url=http://search.proquest.com/docview/169797941/
| place=[[Chicago, IL]] 
| newspaper=[[Chicago Tribune|The Chicago Tribune]] 
| at=pg 6 col 3-4
| date=November 1, 1969 
| accessdate=2016-09-30
}}</ref>

By 1970 the School of Music was sponsor of four jazz bands.<ref name=HarrisonThesis/>{{rp|285}}

Many alumni of Garvey's bands went on to successful careers as musicians and teachers, including [[Cecil Bridgewater]], [[Dee Dee Bridgewater]],<ref name="NYTimes1998-09-22">{{cite news
 | title = A Singer Is Returning to a Stage Where It All Began
 | url = https://www.nytimes.com/1998/09/22/arts/arts-in-america-a-singer-is-returning-to-a-stage-where-it-all-began.html
 | last =Thomas | first =Jo | authorlink= | newspaper = [[The New York Times]] | date = September 22, 1998
 | access-date  = 2016-10-06
}}</ref> [[Jim McNeely]], [[Ron Dewar]], [[Joel Helleny]], and [[Howie Smith]].

==The Russian Folk Orchestra==

During the tour of the U.S.S.R., Garvey became interested in Russian folk music. Upon returning he found funds through the university to purchase 25 balalaikas, and in 1973 formed the Russian Balalaika Orchestra
,<ref name=DailyIllini1973-12-06>{{cite news
| title=Garvey, Inspired by Trip to Russia , Forms Balalaika Band
| url=http://idnc.library.illinois.edu/cgi-bin/illinois?a=d&d=DIL19731206.2.57&srpos=4&e=-------en-20--1-byDA-txt-txIN-Garvey+Russian+Folk+Orchestra------#
| place=[[Champaign, IL]] 
| newspaper=[[The Daily Illini]] 
| at=pg 18, col 1-3, p20 col 1-2
| date=December 6, 1973 
| accessdate=2016-10-01
}}</ref> soon to be renamed the Russian Folk Orchestra. Garvey led this ensemble for over a decade, touring the U.S and abroad.
<ref name=archives>{{cite web
|title=Garvey, John (1921–2006)
|url=http://archives.library.illinois.edu/archon/?p=creators/creator&id=1758
|publisher=University of Illinois Archives
|accessdate=2016-10-01}}</ref>

== Sound Recordings ==

Zoltan Kodaly Quartet No 2, Op. 10/Karol Syzmanowski Quartet in C Major, Op 37. The Walden Quartet of the University of Illinois. Lyrichord LP LL 22, 1951. [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/quartet-no-2-op-10/oclc/28007974 28007974]

William Bergsma: String Quartet No. 2. Arthur Shepherd: Triptych for Soprano and String Quartet. Performed By the Walden String Quartet of the University of Illinois. With Marie Kraft, Soprano. American Recording Society ARS-18, 1952.<ref>https://www.discogs.com/William-Bergsma-Walden-String-Quartet-Of-The-University-Of-Illinois-Arthur-Shepherd-2-Marie-Kraft-Wa/release/3156789</ref>

Ernest Bloch: Quintet for Piano and Strings. Johana Harris and The Walden String Quartet. MGM Records E3239, 1955. [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/quintet-for-piano-and-strings/oclc/3528792 3528792]<ref>https://www.discogs.com/Ernest-Bloch-Johana-Harris-And-The-Walden-String-Quartet-Quintet-For-Piano-And-Strings/master/638754</ref>

The Walden Quartet of the University of Illinois, [[String Quartet No. 1 (Carter)|''Elliott Carter String Quartet No. 1'']], [[Columbia Masterworks Records|Columbia Masterworks]] ML 5104, 1956, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/string-quartet/oclc/2100491 2100491].

Harry Partch: The Bewitched. Recording of first performance at Festival of Contemporary Arts March 26, 1957. Originally released on Partch's own label: Gate 5 Records HP-101 5701.<ref>https://www.discogs.com/Harry-Partch-The-Bewitched/release/2183039</ref>

Andrew Imbrie, The California String Quartet, The Walden String Quartet – String Quartets 2 And 3, Contemporary Records C6003, 1958.<ref>https://www.discogs.com/Andrew-ImbrieCalifornia-String-QuartetWalden-String-Quartet-String-Quartets-2-And-3/release/8305098</ref>

The Walden String Quartet, "Charles Ives: Second String Quartet", Folkways FM 3369, 1966.<ref name="Folkways3369">{{cite AV media
| title=Charles Ives & Alan Hovhaness
| url=http://media.smithsonianfolkways.org/liner_notes/folkways/FW03369.pdf
| publisher=Folkways Records | id=FM 3369 | year=1966 | access-date=2016-09-14
}}</ref>

Walter Piston / Alan Hovhaness - Earl Wild, The Walden String Quartet / William Masselos, Izler Solomon – Quintet For Piano & Strings / Khaldis: Concerto For Piano, Four Trumpets & Percussion, Heliodor HS-25027, 1966.<ref>https://www.discogs.com/Walter-Piston-Alan-Hovhaness-Earl-Wild-Walden-String-Quartet-William-Masselos-Chamber-Ensemble-Izler/release/932651</ref>

''Orchestra De Jazz A Universitatii Din Illinois'', Electrecords EDD 1224, 1968, [[LP record|LP]].

''The University Of Illinois Jazz Band In Champaign-Urbana'', Century Records, 1968, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-in-champaign-urbana/oclc/11069493 11069493].

''The University Of Illinois Jazz Band In Champaign-Urbana'', Century Records, 1968, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-in-champaign-urbana/oclc/11069493 11069493].

''University of Illinois Jazz Band - T-Bird 1968-12-17'', amateur live recording, 1968, multiple formats.
<ref name="1968-12-17-T-Bird">
{{cite AV media
| title=University of Illinois Jazz Band - T-Bird 1968-12-17
| url=https://archive.org/details/1968-12-17_UIJazzBand
| publisher=[[Internet Archive]] 
| year=1968 
| access-date=2017-01-16
}}</ref>

''The University of Illinois Jazz Band in Stockholm, Sweden'', Century Records, 1969, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-in-stockholm-sweden/oclc/24483234 24483234].

''The University of Illinois Jazz Band and Dixie Band'', Century Records, 1969, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-and-dixie-band/oclc/11069451 11069451].

''The University of Illinois Jazz Band with Don Smith'', Mark Custom Recording Service MC 2944, 1970, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-with-don-smith/oclc/17211237 17211237].

''The University of Illinois Jazz Band and the Hot 7 at CJF'', Mark Custom Recording Service, 1970, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/university-of-illinois-jazz-band-and-the-hot-7-at-cjf/oclc/7440049 7440049].

''The University of Illinois Jazz Band, John Garvey, conductor, presents the Big Wide World of Jazz'', Golden Crest Records CRS 4161, 1977, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/wide-world-of-jazz/oclc/405375 405375].

''Closeout'', Dancing Bear Records, 1982, [[LP record|LP]], [[OCLC]] [http://www.worldcat.org/title/closeout/oclc/19115091 19115091].

== References ==
{{Reflist|30em}}

== External links ==
{{cite web
|last1=JH
|url=http://garveyband.blogspot.com/
|title=John Garvey and the University of Illinois Jazz Band
|accessdate=2016-09-13
}}  A very well-researched history of the band from 1958 to 1991, with articles and photographs.

{{cite web
|title=John Garvey/UIJB Scrapbook
|url=https://uk.pinterest.com/jazztrpt/john-garveyuijb-scrapbook/
|website=pinterest
|accessdate=2016-10-01
}} A collection of photographs of Garvey and the jazz band.
*{{discogs artist | John Garvey | John Garvey at discogs}} 
*{{allmusic|artist/john-garvey-mn0001533234}}
{{authority control}}

{{DEFAULTSORT:Garvey, John}}
[[Category:1921 births]]
[[Category:2006 deaths]]
[[Category:Articles created via the Article Wizard]]
[[Category:People from Canonsburg, Pennsylvania]]
[[Category:American jazz bandleaders]]
[[Category:Temple University alumni]]
[[Category:University of Illinois at Urbana–Champaign faculty]]
[[Category:American music educators]]