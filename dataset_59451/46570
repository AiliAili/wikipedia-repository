{{Use dmy dates|date=September 2016}}
{{Use Australian English|date=September 2016}}
{{Infobox person
| name                  = Ron Blanchard
| image                 = <!-- [[Freely licenced]] images only. NO SCREEN CAPTURES. Please do not put a fair-use image here, it will be deleted - see [[WP:NONFREE]] -->
| imagesize             = 
| caption               = 
| birth_name             = 
| birth_date             = <!-- {{Birth date and age|df=yes|YYYY|MM|DD}} -->
| birth_place            = 
| death_date             = 
| death_place            = 
| other_names             = 
| occupation            = [[Film actor|Film]] and [[television actor]]
| years_active           = 1969-1997
| spouse                = 
| partner       = 
| website               = 
}}

'''Ron Blanchard''' is an Australian [[Film actor|film]] and [[television actor]]. He is best known for his starring roles in five popular children's television series ''[[Good Morning!!! (Australian show)|Breakfast-a-Go-Go]]'', ''[[The Lost Islands]]'', ''[[Alexander Bunyip's Billabong]]'', ''[[Watch This Space]]'' and ''[[Professor Poopsnagle's Steam Zeppelin]]''. A well-known [[character actor]], Blanchard had numerous appearances in film and on television series from the late 1960s up until the late 1990s, most especially, his recurring role as Lenny Sawyer on ''[[A Country Practice]]''.

==Biography==
Blanchard got his first big break when he appeared on the children's variety show ''[[Good Morning!!! (Australian show)|Breakfast-a-Go-Go]]'' in 1969 as "Witless Wonder", the comedic sidekick of new host Sue Smith. In 1972, he made his television acting debut as a guest star on the soap opera ''[[Number 96 (TV series)|Number 96]]'' and on the comedy series ''[[Snake Gully with Dad and Dave]]''. He continued working on-and-off for the next several years and, in 1976, Blanchard landed the part of the villainous Quell on the children's adventure series ''[[The Lost Islands]]''. As one of the show's main [[antagonist]]s, the series provided his breakout role.

As well as appearing on ''[[The Outsiders (Australian TV series)|The Outsiders]]'', in the same year he also made his film debut with a supporting role in ''[[Caddie (film)|Caddie]]''.<ref name="NYT Filmography">{{cite web |first= |last= |author=All Movie Guide |authorlink=All Movie Guide |coauthors= |title=Ron Blanchard filmography |url=https://movies.nytimes.com/person/6659/Ron-Blanchard/filmography |archiveurl= |work=Movies & TV |publisher=[[New York Times]] |location= |page= |pages= |language= |format= |doi= |date= |month= |year=2009 |archivedate= |accessdate= |quote= }}</ref><ref>{{cite web |first= |last= |author=Canby, Vincent |authorlink= |coauthors= |title='Caddie,' An Australian Woman On Her Own |url=https://www.nytimes.com/1981/02/08/movies/caddie-an-australian-woman-on-her-own.html |archiveurl= |work=Cultural Desk |publisher=[[New York Times]] |location= |page= |pages= |language= |doi= |date=1981-02-08 |month= |year= |archivedate= |accessdate= |quote= }}</ref><ref>Reade, Eric. ''History and Heartburn: The Saga of Australian Film, 1896-1978''. Sydney: Harper & Rowe, 1981. (pg. 248) ISBN 0-8386-3082-0</ref><ref>[[Daily Variety]]. ''Variety's Film Reviews: 1975-1977''. New York: R.R. Bowker, 1983. ISBN 0-8352-2794-4</ref> He followed this with appearances on ''[[Garry McDonald|The Garry McDonald Show]]'' (1977), ''[[Alexander Bunyip's Billabong]]'' (1978),<ref>[[Library Association of Australia]]. "Item notes." Orca. Vol. 15-16. (1979): 44+.</ref> ''[[Doctor Down Under]]'' (1979) and, most notably, co-starring with [[Paul Chubb]] in the children's science fiction series ''[[Watch This Space]]'' (1982). He began to work more regularly during the early 1980s and had parts in the [[television movie]] ''[[Chase Through the Night]]'' (1983), [[television miniseries]] ''[[The Last Bastion]]'' (1984) <ref>[[John A. Willis|Willis, John]]. ''Screen World 1986 Film Annual''. Vol. 37. New York: Random House, 1986. (pg. 171) ISBN 0-517-56257-X</ref> and films ''[[Silver City (1984 film)|Silver City]]'' (1984),<ref>{{cite web |first= |last= |author=Maslin, Janet |authorlink= |coauthors= |title=Screen: Australia's 'Silver City' |url=https://www.nytimes.com/1985/05/17/movies/screen-australia-s-silver-city.html |archiveurl= |work=Weekend Desk |publisher=[[New York Times]] |location= |page= |pages= |language= |doi= |date=1985-05-17 |month= |year= |archivedate= |accessdate= |quote= }}</ref> ''[[Warming Up]]'' (1985) and ''[[Burke & Wills]]'' (1985).<ref name="NYT Filmography"/><ref>Murray, Scott and Raffaele Caputo. ''Australian Film, 1978-1992: A Survey of Theatrical Features''. Melbourne: Oxford University Press, 1993. (pg. 153, 165, 179) ISBN 0-19-553584-7</ref>

In January 1986, he joined former co-star Paul Chubb and Perry Quinton in a [[pantomime|children's pantomime]] called ''Humpty Dumpty''. It featured the fictional characters of the ''[[Harry and Ralph Show]]'' at the Footbridge Theatre.<ref>"Kids' Theatre". [[Sydney Morning Herald]]. 3 Jan 1986.</ref> In the same year, he starred in yet another children's television series, the popular but short-lived ''[[Professor Poopsnagle's Steam Zeppelin]]''. After a supporting role in the thriller film ''[[Dark Age]]'' (1987),<ref>Lentz, Harris M. ''Science Fiction, Horror & Fantasy Film and Television Credits''. Jefferson, North Carolina: McFarland, 1994. (pg. 396) ISBN 0-89950-927-4</ref> he returned to television acting with guest appearances in ''[[Mother and Son]]'' (1988), ''[[True Believers (TV series)|True Believers]]'' (1988) and ''[[G.P.]]'' (1991). That same year, he also had a minor role in the action film ''[[Gotcha (1991 film)|Gotcha]]'' (1991).

Beginning in 1992, Blanchard had a recurring role as Lenny Sawyer on the television series ''[[A Country Practice]]''. His last regular acting role was in the film ''[[Country Life (film)|Country Life]]'' (1994),<ref>[[Roger Ebert|Ebert, Roger]]. ''Roger Ebert's Video Companion''. 13th ed. Kansas City: Andrews & McMeel, 1997. (pg. 173) ISBN 0-8362-3688-2</ref> although he made appearances in the television movie ''[[Hart to Hart#TV movies|Hart to Hart: Harts in High Season]]'' (1996) and the [[romance film]] ''[[Oscar and Lucinda]]'' (1997).<ref name="NYT Filmography"/><ref>Willis, John. ''Screen World 1998 Film Annual''. Vol. 49. Milwaukee: Hal Leonard Corporation, 1999. (pg. 257) ISBN 1-55783-342-7</ref>

For the past 12 years he's been traveling the world playing the role of an English butler for the famous Swedish illusionist [[Joe Labero]].

==References==
{{Reflist}}

==External links==
*{{IMDb name|id=0087101|name=Ron Blanchard}}
*{{amg name|6659|Ron Blanchard}}

{{DEFAULTSORT:Blanchard, Ron}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Australian male television actors]]