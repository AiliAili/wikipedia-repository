{{Infobox airport
| name         = Mandal Airfield
| nativename   = 
| image        =
| image-width  =
| caption      =
| IATA         =
| ICAO         =
| type         = Military
| owner        =
| operator     = [[Luftwaffe]]
| city-served  = [[Mandal]], [[Norway]]
| location     = Vestnes, Mandal
| metric-elev  = y
| elevation-m  =
| elevation-f  =
| coordinates  = {{coord|58.0263|N|007.4353|E|region:NO|display=inline,title}}
| pushpin_map            = Norway
| pushpin_relief         = yes
| pushpin_label_position =
| pushpin_label          = Mandal
| pushpin_map_alt        =
| pushpin_mapsize        = 300
| pushpin_image          =
| pushpin_map_caption    = Location in Norway
| website      = 
| metric-rwy   = y
| r1-number    = 
| r1-length-f  = <s>4,990</s>
| r1-length-m  = <s>1,520</s>
| r1-surface   = <s>Wood</s>
}}
'''Mandal Airfield''' ({{lang-no|Mandal flyplass}}) was a [[military air base]] situated at Vestnes in [[Mandal]], [[Norway]]. It featured a wooden runway measuring {{convert|1520|by|80|m|sp=us}}. Built by the German [[Luftwaffe]] in 1940 after Nazi Germany occupied Norway, it remained in use for the rest of the [[Second World War]]. [[Lista Air Station]] opened in April 1941, after which Mandal remained only a reserve airfield. A 1950 proposal to rejuvenate it as a civilian airport was turned down by the municipal council.

==History==
Following the [[Operation Weserübung|German invasion of Norway]] on 9 April 1940, the Luftwaffe secured [[Kristiansand Airport, Kjevik]] and [[Sola Air Station]], the two land airports along the south coast of Norway. The Luftwaffe quickly assessed that there would be need for an additional airfield located between [[Kristiansand]] and [[Stavanger]]. Work therefore commenced on building Mandal Airfield, which was completed in August 1940.<ref name=ettrup>{{cite book |last=Ettrup |first=Erik |last2=Schellenbergen |first2=Daniel |last3=Ritterbach |first3=Erik |title=Festung Lista |publisher=Commentum Forlag |location=Sandnes |isbn=978-82-92309-71-1 |language=Norwegian, German |pages=64–65}}</ref>

[[File:Mandal. Strøkene Vestnes og litt av Støkkan.jpg|thumb|left|Vestnes is today a residential area]]
The airfield remained an important site for [[fighter aircraft]] which participated in the [[Norwegian Campaign]].<ref name=hafsten>{{cite book |last=Hafsten |first=Bjørn |last2=Larsstuvold |first2=Ulf |last3=Olsen |first3=Bjørn |last4=Stenersen |first4=Sten |title=Flyalarm: Luftkrigen over Norge 1939–1945 |year=1991 |publisher=Sem & Stenersen |location=Oslo |language=Norwegian |isbn=82-7046-058-3 |url=http://www.nb.no/nbsok/nb/824921f23d8278cf9650ee046c3209f8 |page=317}}</ref> However, by Mandal's completion the Luftwaffe had concluded that [[Lista]] would be a more suitable site for a major air base. Planning started in August 1940 and Mandal remained in use until April 1941. It was then closed down and operations moved to Lista Air Station.<ref name=ettrup /> A further expansion of Mandal took place in 1944, with more taxiways and aircraft stands, although these were never used. No squadrons were ever stationed at Mandal and it was closed at the end of the war in 1945.<ref name=hafsten />

Civilian interest in the airfield arose in 1950. The struggling Kristiansand-based airline [[Sørfly]] proposed that it could use Mandal as a base for [[air charter|charter flights]]. The issue was discussed by Mandal Municipal Council, but it decided not to allow the reopening of the airfield.<ref name=bakken>{{cite book |last=Bakken |first=Rolf |last2=Jæger |first2=Ingrid W. |last3=Krageboen |first3=Sven |title=Kr.sand lufthavn, Kjevik 50 år 1939–1989 |year=1989 |location=Kristiansand |isbn=82-991887-0-9 |language=Norwegian |page=56}}</ref>

==Facilities==
Mandal Airfield was situated at Vestnes,<ref name=bakken /> located just west of the town center of Mandal. It featured a wooden runway which measured {{convert|1520|by|80|m|sp=us}}.<ref name=hafsten />

==References==
{{reflist}}

{{Airports in Norway}}
{{Portal bar|Aviation|World War II|Norway}}

[[Category:Airports in Vest-Agder]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Mandal, Norway]]
[[Category:1940 establishments in Norway]]
[[Category:1945 disestablishments in Norway]]
[[Category:Airports established in 1940]]
[[Category:Military installations in Vest-Agder]]