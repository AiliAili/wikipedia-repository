{{Use dmy dates|date=December 2016}}
{{Infobox person
| name          = Finn Varde Jespersen
| image         = Finn varde jespersen.jpg
| alt           = 
| caption       = 
| birth_name    = <!-- only use if different from name -->
| birth_date    = {{Birth date|df=yes|1914|04|04}}
| birth_place   = Kristiania
| death_date    = {{Death date and age|df=yes|1944|06|06|1914|04|04}} 
| death_place   = Cherbourg
| nationality   = Norwegian
| other_names   = 
| occupation    = Orienteer
| years_active  = 
| known_for     = Pilot
| notable_works = 
}}

'''Finn Varde Jespersen''' (4 April 1914 &ndash; 6 June 1944) was among Norway's leading [[orienteering|orienteers]] in the late 1930s. During [[World War II]] he was a pilot with the rank of Lieutenant; he perished during the [[Operation Overlord|invasion of Normandy]].

==Orienteering career==
He was born in [[Oslo|Kristiania]]. His parents were [[Per Mathias Jespersen]] and Anna Jespersen née Johnsen, both from [[Skien]].<ref name=vf>{{cite encyclopedia |title=Jespersen, Finn Varde |page=497 |last=Ording|first=Arne |authorlink=Arne Ording |author2=Johnson, Gudrun |author3=Garder, Johan|encyclopedia=[[Våre falne 1939-1945]] |publisher=The State of Norway |location=Oslo |year=1950 |volume=2 |language=Norwegian |url=http://da2.uib.no/cgi-win/WebBok.exe?slag=lesside&bokid=vaarefalne2&sideid=498&storleik= |accessdate=19 July 2010}}</ref> In the 1930s he was an active sportsman competing in [[orienteering]] for the club [[IL Heming]]. He represented the Norwegian national team in the third Scandinavian cup in September 1939, where only Sweden and Norway participated because of travel restrictions for the Finnish athletes due to the recent [[World War II|outbreak of war]], and Jespersen placed third in the individual contest.<ref name=o90>{{cite book |title=Orienteringsidretten i Norge gjennom 90 år |editor1=Berglia, Knut |editor2=Brohaug, Tom-Erik |editor3=Staver, Kristoffer |editor4=Thuesen, Kaare |editor5=Strandhagen, Torgeir |pages=71–76 |year=1987 |language=Norwegian |publisher=[[Norges Orienteringsforbund]] |location=Oslo }}</ref> In the 1940 season he won a contest in Finland ahead of the Finnish orienteers Börje Malmström, Jan Gripenberg and Birger Lönnberg. This result has been described as the first noted Norwegian orienteering victory in Finland.<ref>Berglia 1987: p. 77</ref>

==World War II==
Jespersen participated in the fighting during the [[Operation Weserübung|German invasion of Norway]] in 1940. He then travelled the long journey via Finland, Russia, Japan, crossing the Pacific Ocean and finally reaching the American continent. He received his pilot training at the [[Little Norway]] training camp in [[Toronto|Toronto, Ontario]], Canada. Fellow orienteer [[Per Bergsland]] from IL Heming was also trained as a pilot in Toronto.<ref>Berglia 1987: p. 74</ref><ref name=henriksen-pb>{{cite book |title=Luftforsvarets historie |volume=2 |first=Vera |last=Henriksen |authorlink=Vera Henriksen |pages=212–223, 657–661 |year=1996|language=Norwegian |publisher=Aschehoug |location=Oslo}}</ref> 
Jespersen served as an instructor at the training school, and later as a pilot flying transport planes. After having been trained on combat aircraft he joined [[RAF Bomber Command]].<ref name=vf/>  Jespersen's [[Avro Lancaster|Lancaster]] of [[No. 97 Squadron RAF|97 Squadron]] was shot down over [[Battle of Cherbourg|Cherbourg]] the [[Operation Overlord|night between 5 and 6 June]] 1944.<ref name=vf/><ref name=meyer>{{cite book |title=Hærens og marinens flyvåpen 1912-1945 |first=Fredrik |last=Meyer |page=399 |year=1977 |language=Norwegian |publisher=Gyldendal |location=Oslo}}</ref><ref name=henriksen>{{cite book |title=Luftforsvarets historie |volume=2 |first=Vera |last=Henriksen |authorlink=Vera Henriksen |page=372 |year=1996 |language=Norwegian |publisher=Aschehoug |location=Oslo}}</ref>

Jespersen's military rank was Lieutenant. He was awarded the [[Haakon VII 70th Anniversary Medal]] post mortem.<ref name=vf/> His diaries from 1940, edited by and with comments by John Berg, were published in 1983 as ''9. april kommer jeg aldri til å glemme.... ''.<ref>{{cite book|first=Finn |last=Jespersen|title=9. april kommer jeg aldri til å glemme... |origyear=1940|year=1983|language=Norwegian |publisher=Cappelen|location=Oslo |editor=John Berg |isbn=82-02-09790-8}}</ref>

==References==
{{Reflist}}

{{Authority control}}

{{DEFAULTSORT:Jespersen, Finn Varde}}
[[Category:1914 births]]
[[Category:1944 deaths]]
[[Category:Sportspeople from Oslo]]
[[Category:Writers from Oslo]]
[[Category:Norwegian orienteers]]
[[Category:Male orienteers]]
[[Category:Foot orienteers]]
[[Category:Norwegian Army personnel of World War II]]
[[Category:Norwegian expatriates in Canada]]
[[Category:Norwegian World War II pilots]]
[[Category:Norwegian military personnel killed in World War II]]
[[Category:Royal Air Force personnel of World War II]]
[[Category:Aviators killed by being shot down]]
[[Category:Aviators killed in aviation accidents or incidents in France]]
[[Category:Norwegian diarists]]
[[Category:20th-century Norwegian writers]]
[[Category:Recipients of the Haakon VII 70th Anniversary Medal]]