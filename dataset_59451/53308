{{Infobox college coach
| name = Howell Peacock
| image = Howell Peacock - UNC.jpg
| alt = 
| caption = Peacock pictured in ''Yackety yak 1918'', UNC yearbook
| sport = [[College basketball]]
| current_title = 
| current_team = 
| current_conference = 
| current_record = 
| contract = 
| birth_date = 1889
| birth_place = 
| death_date = 1962
| death_place = 
| alma_mater = 
| player_years1 = 1909–1912
| player_team1 = [[Georgia Bulldogs men's basketball|Georgia]]
| coach_years1 = 1912–1916
| coach_team1 = [[Georgia Bulldogs men's basketball|Georgia]]
| coach_years2 = 1916–1919
| coach_team2 = [[North Carolina Tar Heels men's basketball|North Carolina]]
| overall_record = 
| bowl_record = 
| tournament_record = 
| championships = 
| awards = 
| coaching_records = 
}}
'''Howell Peacock''' (1889–1962) was an American basketball coach, best known for being the [[head coach]] of men's [[college basketball]] at the [[University of Georgia]] and at the [[University of North Carolina at Chapel Hill|University of North Carolina]].

==University of Georgia==
Peacock played for the [[Georgia Bulldogs men's basketball|Georgia men's basketball team]] and was team captain for the 1909–10 and 1911–12 seasons.<ref name="Georgia Bulldogs Media Guide p=174">{{harvnb|Georgia Bulldogs Media Guide|2008|p=174}}</ref>  After playing on the team, Peacock became the head coach for Georgia in 1912 and coached the bulldogs for the next four seasons.<ref name="Georgia Bulldogs Media Guide p=174">{{harvnb|Georgia Bulldogs Media Guide|2008|p=174}}</ref>  Peacock amassed a 30–7 record while coaching for the University of Georgia.<ref name="Georgia Bulldogs Media Guide p=174">{{harvnb|Georgia Bulldogs Media Guide|2008|p=174}}</ref>

==Coaching at North Carolina==
After leaving the University of Georgia, Peacock became head coach at [[North Carolina Tar Heels men's basketball|North Carolina]] while being simultaneously enrolled as a medical student there.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref> Peacock took over after the departure of [[Charles Doak]] as head coach.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref> When Doak left, many of his players also graduated, leaving Peacock to build the team mostly from scratch.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref> In order to field a full team, Peacock recruited players from all over campus by posting signs up, asking men to come and try out for the team.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref>  Ten individuals showed up for try-outs and three made it onto the team.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref> The 1916–17 team barely managed to earn a winning record, but did manage to beat [[Virginia Cavaliers men's basketball|Virginia]], which was considered a moral victory.<ref name="Rappoport 2002 p=2">{{harvnb|Rappoport|2002|p=2}}</ref> The 1916–17 team also included a future [[Governor of North Carolina]] [[Luther H. Hodges]] and General F. Carlylel Shepard.<ref name="Rappoport 2002 p=6">{{harvnb|Rappoport|2002|p=6}}</ref>

The 1917–18 team managed to win all of its home games and became one of the best teams in the South.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref> Peacock's third and final season with the Tar Heels was largely a disappointment, however, as the Tar Heels went 9–7 in the 1918–19 season.<ref name="Powell 2005 p=12">{{harvnb|Powell|2005|p=12}}</ref>

==Head coaching record==
{{CBB Yearly Record Start
|type=coach
|conference=
|postseason=
|poll=no
}}
{{CBB Yearly Record Subhead
|name=[[Georgia Bulldogs basketball|Georgia Bulldogs]]
|startyear=1912
|conference=Independent
|endyear=1916
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1912–13
 | name         = Georgia
 | overall      = 10–1
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1913–14
 | name         = Georgia
 | overall      = 9–1
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1914–15
 | name         = Georgia
 | overall      = 4–3
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1915–16
 | name         = Georgia
 | overall      = 7–2
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Subtotal
 | name       = Georgia
 | overall    = 30–7
 | confrecord = 
}}
{{CBB Yearly Record Subhead
|name=[[North Carolina Tar Heels men's basketball|North Carolina Tar Heels]]
|startyear=1916
|conference=Independent
|endyear=1919
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1916–17
 | name         = North Carolina
 | overall      = 5–4
 | conference   = 
 | confstanding = 
 | postseason   =
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1917–18
 | name         = North Carolina
 | overall      = 9–3
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = 
 | ranking2     = 
}}
{{CBB Yearly Record Entry
 | championship = 
 | season       = 1918–19
 | name         = North Carolina
 | overall      = 9–7
 | conference   = 
 | confstanding = 
 | postseason   = 
 | ranking      = no
 | ranking2     = no
}}
{{CBB Yearly Record Subtotal
 | name       = North Carolina
 | overall    = 23–14
 | confrecord = 
}}
{{CBB Yearly Record End
|overall= 53–21
|poll=no
}}

==Sources==
# {{Cite book|title=University of North Carolina Basketball|first=Adam|last=Powell|publisher=Arcadia Publishing|location= |year=2005|isbn=0-7385-4150-8|url=https://books.google.com/books?id=4YhGeDCjS7wC&output=html|ref=harv}}
# {{Cite book|title=Tales from the Tar Heel Locker Room|first=Ken|last=Rappoport|publisher=Sports Publishing LLC|location= |year=2002|isbn=1-58261-489-X|url=https://books.google.com/books?id=WHcMGVwNzgkC&dq=Charles+Doak+basketball&source=gbs_summary_s&cad=0|ref=harv}}
# {{Cite web|title=2008-09 North Carolina men's basketball media guide|first=|last=|publisher=UNC Athletic Communications|location= |year=|isbn=|url=http://tarheelblue.cstv.com/sports/m-baskbl/spec-rel/100708aab.html|ref=harv}}
# {{Cite web|title=2008-09 Georgia men's basketball media guide|first=|last=|publisher=|location= |year=|isbn=|url=http://www.georgiadogs.com/ViewArticle.dbml?DB_OEM_ID=8800&ATCLID=1281297&KEY=&DB_OEM_ID=8800&DB_LANG=C&IN_SUBSCRIBER_CONTENT= Georgia media guide|ref=harv}}
# {{Cite web|title=Orange County, NC - Old Chapel Hill Cemetery, Part 2 |first= |last= |publisher= usgwarchives.net |location= |year= |isbn= |url=http://files.usgwarchives.net/nc/orange/cemeteries/chap002.txt|ref=harv}}

==References==
{{Reflist}}

{{Georgia Bulldogs basketball coach navbox}}
{{North Carolina Tar Heels men's basketball coach navbox}}

{{DEFAULTSORT:Peacock, Howard}}
[[Category:1889 births]]
[[Category:1962 deaths]]
[[Category:American basketball coaches]]
[[Category:Georgia Bulldogs basketball coaches]]
[[Category:Georgia Bulldogs basketball players]]
[[Category:North Carolina Tar Heels men's basketball coaches]]