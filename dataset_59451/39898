{{EngvarB|date=May 2015}}
{{Use dmy dates|date=May 2015}}
{{Infobox aviator
|name= Judy Leden
|image= Judy Leden MBE.jpg
|image_size=
|alt=
|caption=
|full_name=
|birth_date= {{Birth year and age|1959}}
|birth_place=London
|death_date= <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} -->
|death_place=
|death_cause=
|resting_place=
|resting_place_coordinates= <!-- {{Coord|LATITUDE|LONGITUDE|type:landmark}} -->
|monuments=
|nationality=
|spouse=Chris Dawes
|relatives=Yasmin (daughter)<br>Cameron (son)
|known_for=
|first_flight_aircraft=
|first_flight_date=
|famous_flights=Crossing English Channel in hang glider. Dropped from a balloon from 40,000ft over Jordan.
|license_date=
|license_place=
|air_force=
|battles=
|rank=
|awards=MBE. Royal Aero Club Gold Medal. Hussein Medal for Excellence. Star of the First Order.
|first_race = <!-- For racing aviators -->
|racing_best = <!-- For racing aviators -->
|racing_aircraft = <!-- For racing aviators -->
| website    = [http://www.judyleden.com// Judyleden.com]
}}
'''Judy Leden''', [[MBE]] (born 1959) is a British [[hang gliding|hang glider]] and [[paragliding|paraglider]] pilot. She has held three [[world championship]]s,<ref name=fwc>Flying with Condors by Judy Leden ISBN 0-7528-0874-5</ref> twice in hang gliding, once in paragliding.

==Biography==
Judy Leden's flying career began while she was at university in [[Cardiff]] in 1979. She started competing in 1982 and broke many records in 1983. She currently holds [[world record]]s for both hang gliding and paragliding. Leden turned professional in 1988, becoming the only woman to earn a living as a hang glider and paraglider pilot. Her work includes films, stunts, flying displays, teaching and writing. She is a friend and supporter of [[Flyability]] - the [[British Hang Gliding and Paragliding Association | BHPA]] charity to help disabled people to fly hang gliders and paragliders.

As a display pilot, Judy has been asked to take part in a range of airshows, most recently she was asked to fly at the [[Imperial War Museum]] display at [[Duxford Aerodrome|Duxford]] as part of their 2011 Spring Air Show themed Celebrating Women in Aviation.<ref>{{cite news
  | publisher = Flightline UK
  | title = Celebrating Women in Aviation
  | date = 23 March 2011
  | url = http://www.airshows.org.uk/news/2011/03/fantastic-female-pilots-are-the-stars-of-duxford%E2%80%99s-spring-air-show
  | accessdate = 16 February 2012 }}</ref> Unfortunately the strong winds made conditions unsafe for flying hang gliders<ref>{{citation
  | publisher = SIMON FENWICK PHOTOGRAPHY
  | title = duxford-spring-air-show-22nd-may
  | url = http://www.simonsfotos.co.uk/duxford-spring-air-show-22nd-may.html
  | accessdate = 16 February 2012 }}</ref>

Judy has landed many sponsorship deals including [[Citroën]]. Competitive success includes winning the Women's World Hang Gliding Championships in 1987 and again in 1991.  She has been British Women's Champion six times and in 1995 she won the Women's World Paragliding Championship.<ref name=fwc /> Involved at [[Airways Airsports]] from the start, Judy is the senior instructor proficient in every form of free flying<ref>{{cite news
  | publisher = Airways Airsports
  | title = Instructors
  | date = 15 October 2002
  | url = http://www.airways-airsports.com/showkb.php?org_id=516&kb_header_id=304&kb_id=456
  | accessdate = 28 November 2011 }}</ref>

Judy's father was one of the children saved by [[Sir Nicholas Winton]] prior to the outbreak of [[World War II]]. She has taken him flying in a microlight on a number of occasions including a flight on his 98th and 100th birthdays.<ref>{{cite news
  | publisher = BBC News
  | title = 'UK Schindler' in birthday flight 
  | date = 29 June 2009
  | url = http://news.bbc.co.uk/1/hi/england/berkshire/8125546.stm
  | accessdate = 16 February 2012 }}</ref><ref>{{cite news
  | publisher = Maidenhead Advertiser
  | title = Sir Nicky celebrates 98th year with record flight 
  | date = 24 May 2007
  | url = http://www.powerofgood.net/pdfs/Nickys98th.pdf
  | accessdate = 16 February 2012 }}</ref>

==Achievements==

In 1989 Judy became the first woman to fly over the English Channel by hang glider following a launch from a hot air balloon at 13,000 feet. The flight received substantial press attention and was even featured in an episode of [[Spitting Image]].<ref name=fwc /><ref>{{YouTube|hf2pKLa86UY}}</ref>

For "Cotopaxi Dream" [[Channel 4]] documentary she climbed and flew from the top of [[Cotapaxi]] at 19,600 feet the worlds tallest active volcano.<ref name=fwc /><ref name=indy /><ref name=wh1 />

Early 1992 saw Judy taking part in the Flight of the Dacron Eagles a 1000-mile, three-week microlight and hang glider expedition down the [[rift valley]] in Kenya filming for the [[BBC 1]] ''[[Classic Adventure]]'' series with cameramen [[Sid Perou]], microlight pilots [[Richard Meredith-Hardy]] and [[Ben Ashman]] and hang glider pilots [[Mark Dale (pilot)|Mark Dale]], [[Tim Hudson (pilot)|Tim Hudson]] and [[Louise Anderton]].<ref name=fwc /><ref name=dacron>{{cite news
  | publisher = BMAA Records
  | title = Flight of the Dacron Eagles
  | date = 5 January 1992
  | url = http://www.flymicro.com/records/index.cfm?record=remarkable_feats
  | accessdate = 16 February 2012 }}</ref><ref name=dacronvid>{{YouTube|juEmPDa-WLc}}</ref>

The "Flight for life"  flight from London to [[Amman]], Jordan for Cancer research campaign with [[Ben Ashman]]. Inspired by and in memory of hang glider pilot Yasmin Saudi who died of Cancer at the age of 24. The flight succeeded in raising over £100,000 and was filmed en route and made into a video.<ref name=fwc /><ref>{{cite news
  | publisher = Flylight Airsports
  | title = The Flylight Team
  | date = 5 January 1992
  | url = http://www.flylight.co.uk/tuition/team.htm
  | accessdate = 23 February 2012 }}</ref><ref name=ffl>{{YouTube|-7evZdHhkQI}}</ref>

In 1996 Judy's autobiography "Flying with Condors" was published.<ref name=fwc /> This title was also used for a [[BBC 1]] [[Natural World]] documentary where Judy and husband [[Chris Dawes PhD|Chris Dawes]] travelled to [[Patagonia]] to fly with [[Andean condors]] using hang gliders, paragliders and paramotors.<ref name=fwctv>{{cite news
  | publisher = OVGuide
  | title = Watch Natural World Season 24 Episode 7 Flying with Condors
  | date = 30 November 2005
  | url = http://www.ovguide.com/tv_episode/natural-world-season-24-episode-7-flying-with-condors-1669901
  | accessdate = 23 February 2012 }}</ref><ref>{{YouTube|ZZOq0hDuIyU}}</ref><ref name=fwcvid>{{YouTube|pGfchi71OUU}}</ref>

Judy's wide experience with various aircraft, adventurous nature and low body weight has resulted in her being asked to test fly a number of unique aircraft. In 2003 she was asked to test fly a replica of a [[Leonardo da Vinci]] glider<ref>{{cite news
  | publisher = IMDB
  | title = Leonardo Dream machine
  | date = 4 May 2003
  | url = http://www.channel4.com/programmes/leonardos-machines/4od
  | accessdate = 16 February 2012 }}</ref><ref>Leonardo's Legacy by Stefan Klein (Author) ISBN 978-0-306-81825-7 http://www.amazon.co.uk/Leonardos-Legacy-Stefan-Klein/dp/0306818256/ref=sr_1_8?ie=UTF8&qid=1310456773&sr=8-8</ref><ref>{{cite news
  | publisher = Judy Leden
  | title = Leonardo da Vinci replica glider
  | date = 4 May 2003
  | url = https://www.facebook.com/media/set/?set=a.174720555891850.36904.100000617190744&type=1
  | accessdate = 16 February 2012 }}</ref> and in 2009 the makers of a replica of the first aircraft to fly in the UK recruited Judy in an attempt to get their replica [[Roe I Triplane|Roe Triplane]] airborne.<ref>{{cite news
  | publisher = Airways Airsports
  | title = Judy Leden Flying A.V Roe's Tri-Plane
  | date = 4 November 2009
  | url = https://www.youtube.com/watch?v=q65GguBHBd0
  | accessdate = 16 February 2012 }}</ref><ref>{{cite news
  | publisher = Daily Mail
  | title = Health and safety grounds magnificent girl in her flying machine
  | date = 27 October 2009
  | url = http://www.dailymail.co.uk/news/article-1223157/Elf-n-safety-ground-magnificent-girl-flying-machine.html
  | accessdate = 23 February 2012 }}</ref>

===Awards===
Judy was awarded an [[MBE]] in 1989 for her services to hang gliding and has received many other awards, including the [[Royal Aero Club]] Gold Medal, Sportswoman of the year from [[Cosmopolitan magazine]] and [[Middlesex|Middlesex county]].<ref name=indy >{{cite web
   | publisher = Independent
   | title = Hang-gliding: Thermal hermit's obsession with the superlative: 'I would be too scared to look over Beachy Head, but ask me to jump off with a hang-glider and it's no problem'
   | date = 25 June 1993
   | url=http://www.independent.co.uk/sport/hanggliding-thermal-hermits-obsession-with-the-superlative-i-would-be-too-scared-to-look-over-beachy-head-but-ask-me-to-jump-off-with-a-hangglider-and-its-no-problem-1493775.html
   | accessdate = 16 February 2012}}</ref> She also received the [[Hussein Medal for Excellence]] in recognition of her flight to Jordan by microlight and the [[Star of the First Order]], by [[King Hussein]]<ref name=fwc />

===Records===
{| class="wikitable"
!style="text-align:left"| Date
!style="text-align:left"| Class
!style="text-align:left"| UK/Worldwide
!style="text-align:left"| Record
!style="text-align:left"| Notes
|-
| 15 June 1983
| HG
| W
| out and return distance
| 82.04&nbsp;km at Owens Valley, USA. (Feminine World, National)<ref name=FAI>{{cite news
  | publisher = FAI
  | title = Hang gliding records
  | date = 4 May 2003
  | url = http://www.fai.org/records
  | accessdate = 16 February 2012 }}</ref><ref name=BritHG>{{cite news
  | publisher = British Hang Gliding Competitions
  | title = Notable Achievements
  | url = http://bhgc.wikidot.com/notable-achievements
  | accessdate = 16 February 2012 }}</ref>
|-
| 13 July 1983
| HG
| W
| Straight Distance
| 233&nbsp;km. (Feminine World, National)<ref name=FAI/><ref name=BritHG/><ref>{{Citation
 | last = Leden
 | title = In search of world records
 | newspaper = Wings
 | pages = 11
 | date = September 1983}}</ref>
|-
| 28 April 1989
| HG
| UK
| Open distance 
| 114 miles from Wether Fell to Lincolnshire.<ref name=BritHG/><ref>{{Citation
 | last = Williams
 | title = Judy Leden sets new hang gliding distance record
 | newspaper = Skywings
 | pages = 11
 | date = June 1989}}</ref><ref>{{Citation
 | last = Leden
 | title = Beating the Women's distance record
 | newspaper = Skywalker
 | pages = 21
 | date = June 1989}}</ref>
|-
| 14 January 1990
| HG
| W
| Distance with one turn point
| 81.72&nbsp;km at Forbes, Australia. (Feminine World, National)<ref name=FAI/><ref name=BritHG/>
|-
| 22 January 1990
| HG
| W
| Distance with one turn point
| 170&nbsp;km at Forbes, Australia. (All Feminine World, National)<ref name=FAI/><ref name=BritHG/>
|-
| 24 January 1990
| HG
| W
| speed over triangular course of 25&nbsp;km
| 16.67&nbsp;km/h at Forbes, Australia.<ref name=FAI/><ref name=BritHG/>
|-
| 22 June 1991
| HG
| W
| triangular course
| 114.107&nbsp;km at Kossen, Austria. (Feminine World, National)<ref name=FAI/><ref name=BritHG/>
|-
| 1 January 1992
| HG
| W
| height gain
| 3970metres at Kuruman, South Africa. (Feminine World, National)<ref name=FAI/><ref name=BritHG/>
|-
| 25 October 1994
| HG
| W
| Altitude record for a [[Hang gliding#Launch|hang glider release from a balloon]]
| Taking off from Wadi Rum, Jordan, Judy was flown to an altitude of 41,300 feet, (12390m)by world class balloonist [[Per Lindstrand]].<ref>{{Citation
 | last = Dougherty
 | first = Sean 
 | title = News From Europe
 | newspaper = National Newsletter THE OFFICIAL PUBLICATION OF THE HANG GLIDING AND PARAGLIDING ASSOCIATION OF CANADA
 | url=http://www.hpac.ca/Air/Air_1994-12.pdf
 | pages = 21
 | date = December 1994}}</ref> This flight was filmed for the Discovery documentary Stratosfear<ref name=bfi >http://ftvdb.bfi.org.uk/sift/title/525676 documentary ref</ref><ref name=strat1>{{YouTube|-N_5Y2iTx0M}}</ref><ref name=strat2>{{YouTube|HvqErXflO94}}</ref> by [[Matt Dickinson]]<ref>http://literati.net/Dickinson/DickinsonFilmCV.htm</ref>
|-
| 12 January 1997
| HG
| W
| Tandem flight to goal
| Steve Varden and Judy Leden make flight to goal of 36.3&nbsp;km from Kuruman, South Africa (Multiplace World, National)<ref name=BritHG/><ref>{{Citation
 | last = Varden
 | first = Steve
 | title = We had it all…except the weather
 | newspaper = Skywings
 | pages = 46
 | date = May 1997}}</ref>
|-
| 15 October 2002
| HG
| UK
| Tandem hang glider tow record
| Aerotow to over 11,000 feet. Her partner for the flight was ''[[Blue Peter]]'' presenter [[Matt Baker (presenter)|Matt Baker]].<ref name="Hang-gliders break world record">{{cite news
  | publisher = BBC
  | title = Hang-gliders break world record 
  | date = 15 October 2002
  | url = http://news.bbc.co.uk/1/hi/england/2331253.stm
  | accessdate = 18 April 2011 }}</ref><ref name=bp>{{YouTube|PKtXao6qp3E}}</ref>
|-
| 11 April 2003
| PG
| UK
| Straight Distance (F)
| 94.4&nbsp;km, Bradwell Edge to Grasby, Ozone Vulcan<ref name=BritPG>{{cite news
  | publisher = XC League UK cross-country flying
  | title = Official UK Paragliding records
  | url = http://www.xcleague.com/xc/info/records.html
  | accessdate = 16 February 2012 }}</ref>
|-
| 17 March 2004
| PG
| UK
| Straight Distance (F)
| 125.0&nbsp;km, Bradwell Edge to Spilsby, Ozone Vibe<ref name=BritPG/>
|-
| 9 December 1992
| PG
| W
| Straight Distance (F)
| 128.5&nbsp;km, Vryburg (South Africa) to, Firebird Apache<ref name=FAI/><ref name=BritPG/>
|-
| 20 July 1994
| PG
| W
| Distance around a Triangular Course (F)
| Sarah Fenwick, Judy Leden, 50.3&nbsp;km, Piedrahita, Nova Sphinx & Edel Rainbow<ref name=FAI/><ref name=BritPG/>
|-
| 20 July 1994
| PG
| W
| Speed over a Triangular Course of 50&nbsp;km (F)
| Piedrahita, 15.8&nbsp;km/h, 20 July 1994, Edel Rainbow<ref name=FAI/><ref name=BritPG/>
|}

==Media appearances==
(1989) ''[[ITN]]'' news reports on crossing the English Channel landing on the beach at Calais.<ref name=fwc />

(1991) ''Cotopaxi Dream'' [[Channel 4]] documentary. The first ever hang glider flight from one of the world's highest active volcanoes – Cotopaxi in [[Ecuador]] which rises 19,600 feet.<ref name=fwc /><ref name=indy /><ref name=wh1 />

(1992) [[Flight of Dacron Eagles]] [[BBC 1]] ''[[Classic Adventure]]'' series<ref name=dacron /><ref name=dacronvid/>

(1994) ''[[Stratosfear]]'' [[Discovery Channel]] documentary<ref name=bfi /> on breaking the world altitude record for a [[Hang gliding#Launch|hang glider release from a balloon]] taking off from Wadi Rum, Jordan to an altitude of 41,300 feet<ref name=fwc /><ref name=strat1/><ref name=strat2/>

[[Don't Try This at Home (TV series)|Don't Try This at Home]] Tandem hang glider flight from a mountain<ref>{{YouTube|ZSEyjZcMpe0}}</ref>

(2003) ''[[Woman's Hour]]'' two interviews with presenter [[Jenni Murray]] discussing Leonardo's Dream Machine and flying with Sir Nicholas Winton<ref name=wh1 >{{cite web
   | publisher = BBC
   | title = Woman's Hour March 4, 2003
   | date = 4 March 2003
   | url=http://www.bbc.co.uk/radio4/womanshour/2003_04_wed_03.shtml
   | accessdate = 17 May 2011}}</ref><ref>{{cite web
   | publisher = BBC
   | title = Womans Hour April 20, 2003
   | date = 20 April 2003
   | url=http://www.bbc.co.uk/radio4/womanshour/2003_20_fri_04.shtml
   | accessdate = 17 May 2011}}</ref>

(2003) ''[[Blue Peter]]'' presenter [[Matt Baker (television personality)|Matt Baker]] taught to fly & makes successful tandem world record breaking flight with Airways instructor Judy Leden.<ref name="Hang-gliders break world record"/><ref name=bp/>

(2005) [[BBC 1]] ''[[Natural World]]'' documentary [[Flying with Condors]] using hang gliders, paragliders and paramotors.<ref name=fwctv /><ref name=fwcvid/>

(2006) An extract from [[BBC]]'s ''Great British Summer''. Airways instructor Judy Leden flying Tandem with [[Cloud Appreciation Society]] founder [[Gavin Pretor-Pinney]], showing him what its like to fly through the clouds on a hang glider.
<ref>{{cite news
  | publisher = BBC
  | title = Great British Summer (Audio sync slightly out as it has come from VHS.)
  | date = 5 November 2006
  | url = https://www.facebook.com/video/video.php?v=10150268749815135&oid=195724634104&comments
  | accessdate = 17 May 2011 }}</ref>

(2010) ''[[Blue Peter]]'' presenter [[Joel Defries]] learnt to fly with Airways instructor Judy Leden with the aim of flying with Lucy the [[peregrine falcon]].<ref>{{cite web
  | publisher = BBC
  | title = Blue Peter
  | date = 4 October 2010
  | url = http://www.bbc.co.uk/programmes/b006md2v/episodes/2010
  | accessdate = 18 April 2011 }}</ref>

(2010) ''[[BBC Breakfast]]'' sports presenter [[Mike Bushell]] and ''[[BBC Disability Sport]]'' reporter [[Tony Garrett]] flew tandem with Airways instructor Judy Leden to experience hang gliding and to demonstrate the benefits of aerotowing for disabled students.<ref>{{cite news
  | publisher = BBC
  | title = Mike Bushell flies high with aerotow hang-gliding
  | date = 8 October 2010
  | url = http://news.bbc.co.uk/sport1/hi/other_sports/disability_sport/9075500.stm
  | accessdate = 18 April 2011 }}</ref>

(2011) ''[[The Gadget Show]]'' presenter, [[Jon Bentley (TV presenter)|Jon Bentley]], flew with Judy Leden, competing with a [[peregrine falcon]] to track a lure<ref>{{cite web
  | publisher = Gadget Show
  | title = Episode 11 (Man Vs Beast Challenge)
  | date = 25 April 2011
  | url = http://www.channel5.com/shows/the-gadget-show/episodes/episode-11-158
  | accessdate = 26 April 2011 }}</ref>

(2011) Judy Leden takes [[BBC Breakfast]] Weather reporter [[Carol Kirkwood]] flying in a hang glider to see a cloud close up and weight it, for BBC's ''[[The Great British Weather Show]]''.<ref>{{cite web
  | publisher = BBC
  | title = A clip from episode 2 'Carol Kirkwood flies through clouds'
  | date = 20 June 2011
  | url = http://www.bbc.co.uk/programmes/p00jc083
  | accessdate = 8 September 2011 }}</ref>

(2014) TEDx Talk: Putting the F into Future: Judy Leden at TEDxHurstpierpointCollege<ref>{{cite web
  | publisher = TEDx Youth
  | title = Putting the F into Future
  | date = 15 April 2014
  | url = https://www.youtube.com/watch?v=7V0tWxpKOWw
  | accessdate = 29 September 2014 }}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Leden, Judy}}
[[Category:Hang gliding]]
[[Category:Paraglider pilots]]
[[Category:Female aviators]]
[[Category:Flight altitude record holders]]
[[Category:English sportswomen]]
[[Category:Members of the Order of the British Empire]]
[[Category:1959 births]]
[[Category:Living people]]
[[Category:Aviation writers]]
[[Category:Aviation pioneers]]
[[Category:English autobiographers]]
[[Category:English people of Jewish descent]]
[[Category:British female aviators]]