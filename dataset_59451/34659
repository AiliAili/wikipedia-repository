{{Use dmy dates|date=January 2017}}
{{Use Australian English|date=January 2017}}
{{Good Article}}
{{Infobox military unit
|unit_name=31st/51st Battalion
|image=[[File:31st 51st Bn patrol at Porton Plantation June 1945.jpg|250px|alt=Soldiers sitting on stores boxes at a military camp surrounded by thick jungle]]
|caption=Men from the 31st/51st Battalion about to go out on patrol around Porton Plantation, 9 June 1945
|dates= 1943–1946
|country= {{flag|Australia|23px}}
|allegiance=
|branch=[[Australian Army]]
|type=[[Infantry]]
|role=
|size=~800–900 men{{#tag:ref|At the start of the war, the authorised strength of an Australian infantry battalion was 910 men all ranks, however, following the reorganisation of the 3rd Division along the [[Jungle division|jungle establishment]], it dropped to 803 men all ranks.<ref>{{harvnb|Palazzo|2004|p= 94}}.</ref>|group=Note}}
|command_structure=[[11th Brigade (Australia)|11th Brigade]], [[3rd Division (Australia)|3rd Division]]
|garrison=
|garrison_label=
|nickname=Kennedy/Far North Queensland Regiment
|motto=
|colors=Chocolate and Gold
|colors_label=Colours
|march=
|mascot=
|equipment=
|equipment_label=
|battles=[[World War II]]
* [[New Guinea campaign]]
* [[Bougainville campaign]]
|anniversaries=
|decorations=
|battle_honours=
|disbanded=
<!-- Commanders -->
|commander1=
|commander1_label=
|commander2=
|commander2_label=
|commander3=
|commander3_label=
|notable_commanders=
<!-- Insignia -->
|identification_symbol=[[File:31st Battalion AIF Unit Colour Patch.PNG|50px|alt=A two-toned rectangular shape, one half of which is brown and the other half gold]]
|identification_symbol_label=[[Unit Colour Patch]]
}}
The '''31st/51st Battalion''' was an [[infantry]] [[battalion]] of the [[Australian Army]], which served during [[World War II]]. Raised for service as part of the [[Australian Army Reserve|Militia]] in 1943 through the amalgamation of two previously existing battalions, the 31st/51st Battalion undertook garrison duties in [[Dutch New Guinea]] in 1943–44 before taking part in the [[Bougainville Campaign]] in 1944–45. Following the end of the war, the battalion served in the Pacific overseeing the transfer of Japanese prisoners of war and re-establishing law and order until mid-1946 when it returned to Australia and was disbanded.
{{TOC limit|2}}

==History==

===Formation===
The 31st/51st Battalion was formed on 12 April 1943 at Yatlee, near [[Cairns, Queensland|Cairns]], [[Queensland]],<ref name="Burla131"/> by the amalgamation of two previously existing Queensland-based [[Australian Army Reserve|Militia]] infantry battalions: the [[31st Battalion (Australia)|31st]] and [[51st Battalion (Australia)|51st Battalions]].<ref name=Burla595>{{harvnb|Burla|2005|p=595}}.</ref> Upon establishment, the battalion adopted the territorial title of the "Kennedy/Far North Queensland Regiment", in order to perpetuate the designations of its two predecessor units.<ref name=AWM>{{cite web|url=http://www.awm.gov.au/units/unit_11316.asp |title=31st/51st Battalion (Kennedy/Far North Queensland Regiment)|work=Second World War, 1939–1945 units |publisher=Australian War Memorial |accessdate= 20 February 2011}}</ref> Although the battalion officially adopted the brown and yellow rectangular [[Unit Colour Patch]] of the 31st Battalion, members of the new battalion that had previously served with the 51st were authorised to wear its brown over light blue circular colour patch.<ref name=Burla131>{{harvnb|Burla|2005|p=131}}.</ref><ref>{{cite web|url= http://www.diggerhistory.info/pages-badges/patches/infantry.htm|archiveurl=https://web.archive.org/web/20101115085809/http://www.diggerhistory.info/pages-badges/patches/infantry.htm |title=Australian Infantry Unit Colour Patches, 1921–1949 |publisher=Digger History |archivedate=15 November 2010 |accessdate=20 February 2011}}</ref> Reinforcements to the unit were allocated the colour patch based upon the territorial area from which they had been recruited.<ref name=Burla131/>

The amalgamation came about due to a shortage of manpower in the Australian economy which had occurred as a result of an over mobilisation of Australia's military forces in the early years of the war.<ref>{{harvnb|Grey|2008|p=184}}.</ref> In an attempt to rectify this situation, the Australian government made the decision to release Militia personnel who had previously been employed in "essential industries" back into the civilian workforce. As a result of this, both the 31st and 51st Battalions, which had large numbers of personnel drawn from the agricultural sector, were well below their authorised establishments, and so it was decided that they would be merged to form a complete battalion.<ref name=AWM/>

===Merauke===
Assigned to the [[11th Brigade (Australia)|11th Brigade]], [[3rd Division (Australia)|3rd Division]],<ref name=AWM/> in May the newly formed battalion undertook amphibious training and intensive physical activities such as route marches around [[Yorkeys Knob, Queensland|Yorkeys Knob]], Queensland, in preparation for deployment overseas.<ref name=Burla132>{{harvnb|Burla|2005|p=132}}.</ref> In June–July 1943, under the command of Lieutenant Colonel Geoffrey Brock, the battalion was deployed to [[Merauke]], in [[Dutch New Guinea]]<ref name=Burla595/> as part of [[Merauke Force]].<ref name=AWM/> They departed from Cairns on board the TSS ''Canberra'' on 20 June, arriving at [[Thursday Island]] on 24 June. From there they were transferred to [[Horn Island, Queensland|Horn Island]] before boarding the MV ''Van de Ljin'' and arriving at Merauke on 16 July 1943.<ref name=Burla132/>

From July 1943 until August 1944 the battalion undertook long range patrols in the surrounding area and established outposts along the coast to the north-west. During this time the patrols from the 31st/51st were involved in two major actions against the Japanese. The first came on 22 December 1943 near Japero,<ref>{{harvnb|Burla|2005|p=139}}.</ref> when a small patrol of nine men on board the ''Rosemary'', a small diesel trawler, surprised a Japanese patrol consisting of two {{convert|40|ft|m|adj=on}} barges. In the fighting that followed, between 20 and 30 Japanese were killed for the loss of one Australian killed and five wounded.<ref>{{harvnb|Burla|2005|p=140}}.</ref> The second action came on 30 January 1944 when a waterborne force of about 200 Japanese was contacted and engaged by a 14-man detachment at an outpost near the start of the Eilanden River, about {{convert|250|mi|km}} from Merauke. Anchoring about {{convert|150|yd|m}} from the outpost, the Japanese were engaged with fire from [[Bren light machine gun|Bren light machine-guns]], small arms and anti-tank rifles. Initially taken by surprise, the Japanese lost about men 60 killed before withdrawing about {{convert|400|yd|m}} out to sea and engaging the outpost with heavy machine-guns and mortars. Nevertheless, only three Australians were slightly injured as a result of the contact, receiving burns to their hands from their overheated Bren guns.<ref>{{harvnb|Burla|2005|p=141}}.</ref>

On 24 July 1944, the battalion was gazetted as an [[Second Australian Imperial Force|Australian Imperial Force]] unit, meaning that it could be sent outside of the bounds imposed upon Militia units as set out in the ''Defence Act (1903)''.<ref name=Burla145>{{harvnb|Burla|2005|p=145}}.</ref> Shortly afterwards, they received orders that they would be withdrawn back to Australia in preparation for employment in combat elsewhere in the Pacific theatre.<ref name=Burla145/> In early August 1944, Lieutenant Colonel Philip Parbury took over command of the battalion, relieving Major Bernard Callinan who had been acting in the role after Brock was taken ill and later died in December 1943.<ref>{{harvnb|Burla|2005|p=138}}.</ref><ref>{{cite web|url=http://www.ww2roll.gov.au/Veteran.aspx?serviceId=A&veteranId=416665 |work=World War II Nominal Roll |title=Brock, Geoffrey Hutton |publisher=Commonwealth of Australia |accessdate=21 February 2011}}</ref> On 8 August the battalion embarked to return to Australia.<ref name=Burla595/> Upon arrival, the men were given a brief period of leave before the battalion concentrated at [[Strathpine, Queensland]], where they received reinforcements and undertook further training. They remained there for four months before departing for [[Bougainville Island|Bougainville]] on 6 December 1944, under the command of Lieutenant Colonel Joseph Kelly.<ref name=Burla595/>

===Bougainville===
[[File:Australian casualties at Tsimba Ridge 6 February 1945.png|thumb|right|alt=Black and white image of wounded men lying on canvas stretchers, while other soldiers attend to them|Australian casualties being treated at the 31st/51st Battalion regimental aid post, following the fighting on Tsimba Ridge, 6 February 1945]]

The 31st/51st Battalion arrived at [[Torokina]] on Bougainville four days later and, along with the rest of the 11th Brigade, they began an offensive [[Bougainville Campaign|campaign]] which saw them involved in heavy combat with the Japanese. They were committed to the fighting in three periods. The first, between December 1944 and February 1945, saw the battalion take [[Battle of Tsimba Ridge|Tsimba Ridge]], cross the Genga River and capture Downs Ridge.<ref name=Burla595/> Later, in March and April 1945, they undertook long range patrols in the central sector of the island in the vicinity of the [[Numa Numa Trail]]. Their final commitment came between May and July 1945 when the battalion was switched to the northern sector, where they took part in the fighting around the Bonis Peninsula. As a part of this phase, the battalion fought along the [[Battle of Ratsua|Ratsua front]] and undertook a disastrous amphibious landing at [[Battle of Porton Plantation|Porton Plantation]].<ref name=Burla596>{{harvnb|Burla|2005|p=596}}.</ref><ref>{{harvnb|Davidson|2005|p=vii}}.</ref>

Conceived as a [[Company (military unit)|company]]-level effort to outflank the significant Japanese defences that were holding up the advance from Ratsua,<ref>{{harvnb|James|2005|p=175}}.</ref> the operation was plagued by poor planning, inadequate resources, intelligence failures and strategic and tactical errors at all levels.<ref>{{harvnb|James|2005|pp=191–192}}.</ref><ref>{{harvnb|Davidson|2005|pp=31–39}}.</ref> After landing at the wrong beach on 8 June,<ref>{{harvnb|Burke|2006|p=6}}.</ref> and finding themselves caught in a tight box of Japanese defensive positions, the 190 men from 'A' and 'C' Companies, under Captain Henry Downs, were unable to advance inland.<ref>{{harvnb|James|2005|p=180}}.</ref> One of the landing craft ran aground and supplies began to dwindle.<ref name=Long213>{{harvnb|Long|1963|p=213}}.</ref> In an effort to link up with the beleaguered company, 'D' Company, which was holding the line along the Ratsua front, attempted to breakthrough the Japanese lines. Although some patrols were able to penetrate to within {{convert|500|yd|m}} of Porton, they were unable to get any closer.<ref>{{harvnb|Hughes|1993|pp=253–254}}.</ref>

Eventually the order to withdraw was given from 11th Brigade headquarters. During the subsequent evacuation, two more landing craft ran aground and although one managed to float free, the other remained stuck hard on a reef.<ref name=Long213/> Over the course of the next couple of days, the men in the landing craft were subjected to heavy Japanese attack and it was not until 11 June that rescue efforts were completed. The battalion lost 23 men killed or missing, presumed dead, while a further 106 were wounded in the operation.<ref>{{harvnb|Long|1963|p=215}}.</ref>

Following the failure of the landing at Porton Plantation, the planned Australian advance into the Bonis Peninsula was called off, and the Australians focused their main effort on Bougainville towards the capture of Buin in the south. The effort in the north was reduced to that of a holding action as further resources were transferred out of the sector.<ref>{{harvnb|Long|1963|pp=234–235}}.</ref> Nevertheless, the 31st/51st Battalion continued to undertake patrols along the Ratsua front, during which they suffered further casualties, until they were withdrawn from combat operations on 28 June.<ref name=AWM/> They were subsequently moved back to Torokina.<ref>{{harvnb|Hughes|1993|p=255}}.</ref>

===Disbandment===
[[File:AWM 117313 31st 51st Battalion Nauru.jpg|thumb|right|Members of the 31st/51st Battalion inspect captured Japanese Type 97 tanks on Nauru in September 1945|alt=Soldiers inspect disused armoured vehicles]]
Following the cessation of hostilities on 15 August 1945, the battalion undertook garrison duties on [[Nauru]] and [[Banaba Island|Ocean Island]].<ref name=AWM/> In this role they helped to maintain law and order and investigate war crimes, oversaw the transfer of over 4,000 Japanese prisoners of war to Bougainville and maintained a military administration until 1 November 1945 when civil control was re-established. A short time later, the detachment on Ocean Island was withdrawn back to Nauru.<ref name=Burla596/>

In December 1945, most of the battalion (except one platoon) was withdrawn to [[New Britain]] to rejoin the 11th Brigade. In February 1946, the platoon that had remained on Nauru arrived in [[Rabaul]]. As the [[Demobilisation of the Australian military after World War II|demobilisation process]] began, the 31st/51st stayed on New Britain until May when they received orders to return to Australia. On 15 April Lieutenant Colonel Donald Lamb took over as battalion commander.<ref>{{harvnb|Burla|2005|p=182}}.</ref> As personnel were discharged or transferred to other units for further service, the battalion's numbers declined until eventually, on 4 July 1946, the battalion was disbanded.<ref name=AWM/>

Throughout the course of the war, the 31st/51st Battalion lost 61 men killed and 168 wounded.<ref name=AWM/> The majority of these came during the fighting on Bougainville where 41 men were killed in action, seven were posted as missing in action (presumed killed) and 12 died of wounds.<ref name=Burla596/> Members of the battalion received the following decorations: one [[Distinguished Service Order]], one [[Distinguished Conduct Medal]], three [[Military Cross]]es, 10 [[Military Medal]]s, one [[British Empire Medal]] and 27 [[Mentioned in Despatches|Mentions in Despatches]].<ref name=AWM/>

Both the 31st and 51st Battalions were later re-raised as separate units when Australia's part-time military was re-raised in 1948 under the guise of the [[Citizen Military Forces]]. Later, they became company-level formations of the [[Pentropic organisation|Pentropic]] 2nd Battalion, [[Royal Queensland Regiment]], in 1960.<ref name=Festberg91_109>{{harvnb|Festberg|1972|pp=91 & 109}}.</ref> A further re-organisation saw the units split again in 1965 and re-raised as battalion-level formations under their old numerical designations.<ref>{{harvnb|Grey|2008|p=239}}.</ref> They have remained separate since then.<ref name=Digger>{{cite web|archiveurl=https://web.archive.org/web/20071121012109/http://www.diggerhistory.info/pages-army-today/state-regts/51fnqr.htm |url=http://www.diggerhistory.info/pages-army-today/state-regts/51fnqr.htm |publisher=Digger History |accessdate=1 November 2011 |title=51 Far North Queensland Regiment |archivedate=21 November 2007}}</ref>

==Battle honours==
For their involvement in World War II, the 31st/51st Battalion received the following [[battle honour]]s:
* [[Battle of Tsimba Ridge|Tsimba Ridge]], [[Battle of Porton Plantation|Bonis–Porton]], [[South West Pacific Area|South-West Pacific 1943–45]], [[New Guinea campaign|Liberation of Australian New Guinea]].<ref name=AWM/><ref>{{harvnb|Burla|2005|p=x}}.</ref>{{#tag:ref|These battle honours were later inherited by the battalion's successor units.<ref name=Festberg91_109/>|group=Note}}

==Commanding officers==
The following officers served as [[commanding officer]] of the 31st/51st Battalion:<ref>{{harvnb|Burla|2005|p=573}}.</ref>
* Lieutenant Colonel Geoffrey Brock (1943);
* Lieutenant Colonel Philip Parbury (1944);
* Lieutenant Colonel Joseph Kelly (1944–1946);
* Lieutenant Colonel Donald Lamb (1946).

==Notes==
;Footnotes
{{reflist|group=Note}}

;Citations
{{Reflist|3}}

==References==
{{Refbegin}}
* {{cite journal|last=Burke|first=Arthur|year=2006|title=Porton, One Beach Too Far&nbsp;– Bougainville 1945|journal=Sabretache|publisher=Military Historical Society of Australia |location=Garran, Australian Capital Territory |volume=47|issue=1|pages=5–8|issn=0048-8933 |ref=CITEREFBurke2006}}
* {{cite book | last=Burla | first=Robert | authorlink= | year=2005 | title=Crossed Boomerangs: The History of All the 31 Battalions | edition= | publisher=Australian Military Publications | location=Loftus, New South Wales | isbn=978-1-876439-67-5 |ref=CITEREFBurla2005}}
* {{cite book|last=Davidson|first=Audrey|title=Porton: A Deadly Trap|year=2005|publisher=Boolarong Press|location=Brisbane, Queensland|isbn=0-646-44766-1 |ref=CITEREFDavidson2005}}
* {{cite book|last=Festberg |first=Alfred |title=The Lineage of the Australian Army |year=1972 |publisher=Allara Publishing |location= Melbourne, Victoria |isbn= 978-0-85887-024-6 | ref=harv}}
* {{cite book | last=Grey | first=Jeffrey | authorlink= | year=2008 | title=A Military History of Australia | edition=3rd | publisher=Cambridge University Press | location=Melbourne, Victoria | isbn=978-0-521-69791-0 |ref=CITEREFGrey2008}}
* {{cite book |last=Hughes |first=William |title=At War With the 51st Infantry Battalion and 31/51st Infantry Battalion (AIF) from 1940 to 1946 |year=1993 |publisher=Church Archivist Press |location=Brisbane, Queensland |isbn= 978-0-949122-29-2 |ref=CITEREFHughes1993}}
* {{cite thesis |degree=PhD |title=The Final Campaigns: Bougainville 1944–1945  |url=http://ro.uow.edu.au/theses/467 |last=James |first=Karl |year=2005 |publisher=School of History and Politics, University of Wollongong |oclc=225536344 |ref=CITEREFJames2005}}
* {{cite book|last=Long|first=Gavin|title=The Final Campaigns|series=Australia in the War of 1939–1945, Series 1—Army. Volume VII|year=1963|edition=1st|publisher=Australian War Memorial|location=Canberra, Australian Capital Territory|url=https://www.awm.gov.au/collection/RCDIG1070206/|oclc=1297619 |ref=CITEREFLong1963}}
* {{cite book|last=Palazzo|first=Albert|chapter=Organising for Jungle Warfare|title=The Foundations of Victory: The Pacific War 1943–1944|year=2004|editor=Dennis, Peter |editor2=Grey, Jeffrey|publisher=Army History Unit|location=Canberra, Australian Capital Territory|url=http://www.army.gov.au/Our-history/Army-History-Unit/Chief-of-Army-History-Conference/Previous-Conference-Proceedings/~/media/Files/Our%20history/AAHU/Conference%20Papers%20and%20Images/2003/2003-The_Pacific_War_1943-1944_Part_1.ashx|isbn=978-0-646-43590-9 |ref=CITEREFPalazzo2004}}
{{Refend}}

{{DEFAULTSORT:31st 51st Battalion (Australia)}}
[[Category:Australian World War II battalions]]
[[Category:Military units and formations established in 1943]]
[[Category:Military units and formations disestablished in 1946]]