{{Infobox artist
| name        = Robert Qualters
| image       = Robert Qualters in Homestead Studio - Mark Perrot, photographer.jpg
| caption     = Photograph by Mark Perrott of artist Robert Qualters in his Homstead, PA studio.
| birth_name  = Robert L. Qualters, Jr.
| birth_date    = {{Birth date and age|1934|03|13}}
| birth_place   = [[McKeesport, Pennsylvania]]
| death_date  = 
| death_place = 
| nationality = American
| religion    = 
| field       = Painting, Printmaking, Installation
| training    = Carnegie Institute of Technology (now [[Carnegie Mellon University]]), [[California College of the Arts|California College of Arts and Crafts]], [[Syracuse University]]
| movement    = [[Bay Area Figurative Movement|Bay Area Figurative Movement of Representational Painting]]
| works       = 
| spouse      = 
| patrons     =
| awards      =
}}

'''Robert L. Qualters, Jr.''' (born March 13, 1934)<ref name="whatdoiknow">Sewald. Jeff. [http://www.pittsburghquarterly.com/index.php/What-Do-I-Know/robert-qualters.html "What Do I Know: A life's recounting in the subject's own words"], "[[Pittsburgh Quarterly]]", Pittsburgh, Winter 2010. Retrieved on 24 June 2013.</ref> is an American painter, installation artist and printmaker based in [[Pittsburgh|Pittsburgh, Pennsylvania]]. His work encompasses traditional painting, as well as murals, and collaborations with other Pittsburgh-based artists across several disciplines. He is associated with the [[Bay Area Figurative Movement|Bay Area Figurative Movement of Representational Painting]].<ref name="whatdoiknow" />

== Early life ==

Qualters was born in [[McKeesport, Pennsylvania]] and grew up in [[Clairton, Pennsylvania]].<ref name="intheframe">Thomas, Mary. Guidry, Nate. [http://www.post-gazette.com/stories/ae/art-architecture/in-the-frame-artists-in-their-own-words-243403 "In The Frame: Artists In Their Own Words"], "[[Pittsburgh Post-Gazette]]', Pittsburgh, 21 April 2010. Retrieved on 20 June 2013</ref>  His mother was a schoolteacher<ref name="whatdoiknow" /> and his father a county assessor.  He has two sisters, Priscilla and Margaret.

== Education, military, and teaching ==

Qualters graduated from Clairton High School in 1951 and enrolled in Carnegie Institute of Technology (now [[Carnegie Mellon University]]) as an art major.<ref name="popstar">Mendelson, Abby. [http://www.popcitymedia.com/features/qualters0402.aspx "Pop Star: Robert Qualters"], "[[Pop City]]", Pittsburgh, 02 April 2008. Retrieved on 24 June 2013.</ref><ref name="paintingsrecall">Shaw. kurt. [http://triblive.com/x/pittsburghtrib/ae/museums/s_567952.html#axzz2Y2c1s9sG "Paintings recall city's historic, personal moments"], "[[TribeLive]]", Pittsburgh, 18 May 2008. Retrieved on 03 July 2013.</ref>  At Carnegie Tech, he studied with [[Robert Lepper]] who once taught fellow Pittsburghers [[Philip Pearlstein]] and [[Andy Warhol]].  In 1953, Qualters joined the army, serving the following two years as an artilleryman in England.<ref name="whatdoiknow" />  He returned to Pittsburgh in 1955 and spent one more year at Carnegie Tech.

Qualters moved to California in 1956 on the [[G.I. Bill]] and enrolled in the [[California College of the Arts|California College of Arts and Crafts]] in Oakland.<ref name="whatdoiknow" />  He earned a B.F.A. there, while becoming immersed in the fledgling [[Bay Area Figurative Movement|Bay Area Figurative Movement of Representational Painting]].<ref name="whatdoiknow" /><ref name="paintingsrecall" />  At California College, his mentors included the painters [[Nathan Oliveira]] and [[Richard Diebenkorn]]<ref name="intheframe" /><ref name="paintingsrecall" />—founding members of the [[Bay Area Figurative Movement]]—and the calligrapher Sabro Hasegawa.  It was Diebenkorn who, in 1957, invited Qualters to be in the first Bay Area Figurative show.

Qualters returned to Pittsburgh in 1959.<ref name="whatdoiknow" /><ref name="paintingsrecall" /> From 1962–1968, he taught at the [[State University of New York at Oswego]],<ref name="whatdoiknow" /> while completing an M.F.A. at [[Syracuse University]] in 1965.  He settled in Pittsburgh permanently in 1968.<ref name="whatdoiknow" /><ref name="paintingsrecall" />  Since then, he has taught at the [[University of Pittsburgh]], [[Carlow University|Carlow College]], [[Slippery Rock University of Pennsylvania|Slippery Rock State University]], [[Indiana University of Pennsylvania]], [[West Virginia University]], and [[Carnegie Mellon University]].<ref name="intheframe" />  He has also been an Artist-in-Residence at the [[Pittsburgh High School for the Creative and Performing Arts]].

[[File:Edward Hopper Was Driving from "Lessons of the Masters," 1997.jpg|thumb|''Edward Hopper Was Driving'' from ''Lessons of the Masters'', 1997]]

== Work ==
“A quintessential Pittsburgh artist,”<ref name="intheframe" />  Qualters is known for his vivid, color-saturated depictions of Pittsburgh's neighborhoods, bridges, and steel mills as well as self-portraits.  A “master of the collaborative process,”<ref>Thomas, Mary. "Robert Qualters and Mark Perrott: Combining Talents", "[[Pittsburgh Post-Gazette Weekend Magazine]]", pp. 21 and 23, Pittsburgh, 21 February 2013.</ref>   Qualters has also created original works with poets Jan McReery and Gail Ghai; mixed media and tattoo artist Nick Bubash; photographers Charlee Brodksy and Mark Perrott, and other Pittsburgh-based artists.  With scenic designer Tony Ferrieri, Qualters designed and painted the set for the [[City Theatre (Pittsburgh)|City Theatre]] of Pittsburgh's 1996–97 production of “Below the Belt.”

Qualters has exhibited in Pennsylvania, as well as in New York, California, Maine, and West Virginia. In Pittsburgh, he has had solo-artist shows at Borelli Edwards, Concept Art Gallery, Carson Street Gallery, the [[Carnegie Museum of Art]], the [[Pittsburgh Center for the Arts]], and many other galleries and museums.<ref name="popstar" />  A 2008 retrospective at the [[Rivers of Steel National Heritage Area]] in [[Homestead, Pennsylvania|Homestead]] featured works from a 20-year span, 1980–2000, highlighting the end of the industrial age in the Monongahela Valley through firsthand observation and through what Rivers of Steel Director Ronald Baraff calls "the lens of the people".<ref>{{cite web|last=Goga|first=Jennifer|date=10 April 2008|title=Exhibit of paintings documents decline of steel industry in Western Pennsylvania|url=http://www.post-gazette.com/stories/local/neighborhoods-south/exhibit-of-paintings-documents-decline-of-steel-industry-in-western-pennsylvania-388739/|work=Pittsburgh Post-Gazette|publisher=PG Publishing Co., Inc.|accessdate=20 June 2013}}</ref>   His work is represented in the permanent collections of the [[Carnegie Museum of Art]]; the [[Oakland Museum of California]]; and the [[Westmoreland Museum of American Art|Westmoreland County Museum of American Art]] in [[Greensburg, Pennsylvania]]; as well as in the main offices of [[PPG Industries]], [[Alcoa]], Hillman Company, The Pittsburgh Foundation, [[Heinz Endowments|The Heinz Endowments]], and many other corporations, schools, and government agencies, and private collections throughout Southwestern Pennsylvania.

Qualters' work has also been seen in more than two-dozen public murals and site-specific installations around the city. Besides commissions from Mercy Hospital of Pittsburgh, the [[Western Pennsylvania School for Blind Children]], Pittsburgh's [[Three Rivers Arts Festival]], Oxford Development Company and others, he has created more than a dozen murals with high school students between 1986 and 1988 and as recently as 2005. His work with these students, like his numerous collaborations with prominent Pittsburgh artists, reflects a passion to create, as well as to teach, learn, and discover.

Besides Diebenkorn and other Bay Area colorists, his influences include [[Pieter Bruegel the Elder|Brueghel]], [[Rembrandt]], [[Matisse]], and fellow Pittsburgh artist [[John Kane]]. Many of his works include references or homages to these artists.<ref name="paintingsrecall" />

[[File:Mural at Grant Street and Boulevard of the Allies 1986.jpg|thumb|Mural at Grant Street and Boulevard of the Allies, 1986]]

== Awards and recognition ==

A past president of both the [[Associated Artists of Pittsburgh]] and Artists Equity of Pittsburgh, Qualters is the [[Pittsburgh Center for the Arts]]' 1985 Artist of the Year, one of many honors bestowed on him over six decades of work.<ref name="intheframe" />  In addition to local grants and commissions, he has received funding to work in France and was a visiting artist at the [[American Academy in Rome]].  Along with countless reviews and feature stories in local press, his work has been published in ''American Artist'', ''The Pittsburgh That Starts Within You'', ''Carnegie Magazine'', ''Pittsburgh Quarterly'', and the Pratt Institute's ''Artists Proof''.

On March 11, 2014, [[Pittsburgh City Council]] presented Robert Qualters with an official City Proclamation.

On July 24, 2014, Pennsylvania First Lady Susan Corbett announced Robert Qualters as the Pennsylvania Artist of the Year at the Pennsylvania Governor's Arts Awards 2014.<ref>{{cite web|last1=Klenotic|first1=Deborah|title=First Lady Susan Corbett Announces Governor’s Awards for the Arts Recipients|url=http://blog.iup.edu/pagovernorsartsawards2014/2014/07/first-lady-susan-corbett-announces-governors-awards-for-the-arts-recipients.html|website=www.iup.edu|accessdate=11 August 2014}}</ref>

[[File:Penn Station Rotunda, 2006.jpg|thumb|''Penn Station Rotunda'', 2006]]

== Personal life==

Qualters lives in the [[Squirrel Hill]] neighborhood of Pittsburgh and works in [[Homestead, Pennsylvania|Homestead]].<ref name="intheframe" /><ref name="popstar" /><ref name="paintingsrecall" /> His wife of 46 years, the former Joanne Ricchi of Oswego, New York, died in 2010.<ref name="jrq">[http://obituaries.triblive.com/listing/170185/Joanne-R-Qualters/ "Qualters, Joanne R."], "[[TribLive]]", Pittsburgh. Retrieved on 24 June 2013.</ref> The Qualters have two sons: Robert Qualters III of Florida and Brett Qualters of Montana.<ref name="jrq" />

== Other ==

In 2014, three concurrent projects--a biography, a documentary film, and a 40-year retrospective--celebrate Qualters' life and work. [[University of Pittsburgh Press|The University of Pittsburgh Press]] published [http://www.amazon.com/Robert-Qualters-Autobiographical-Vicky-Clark/dp/0822962926/ref=sr_1_1?s=books&ie=UTF8&qid=1407770937&sr=1-1&keywords=robert+qualters ''Robert Qualters: Autobiographical Mythologies''] by author, historian, and curator Vicky A. Clark, PhD of [[Clarion University of Pennsylvania|Clarion University]] in January; With [[Pittsburgh Filmmakers]], filmmakers Joe and Elizabeth Seamans released ''Bob Qualters: The Artist in Action''; and from February 7 through April 20, the [[Pittsburgh Center for the Arts]] in [[Shadyside (Pittsburgh)|Shadyside]] ran ''Robert Qualters: 40 Years Paintings, Drawings, and Prints''.

== References ==
{{reflist}}

== External links ==
* {{official website|http://www.qualtersart.com}}

 {{Authority control}}

{{DEFAULTSORT:Qualters, Robert L}}
[[Category:20th-century American painters]]
[[Category:American male painters]]
[[Category:21st-century American painters]]
[[Category:1934 births]]
[[Category:Living people]]