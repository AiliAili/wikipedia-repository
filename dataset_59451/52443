The '''OECC''', established in 1996, is an annual conference which publishes proceedings and scientific research articles as a result of its conferences. ''OECC'' stands for the '''OptoElectronics and Communications Conference''', which has conducted annual meetings since its establishment up to the present year. With an international scope, the areas of focus for the OECC are annual meetings in the Asia Pacific region, centered on the [[optoelectronics]] and
[[optical communications]] profession. The function of the meetings are to report, discuss, exchange, and generate ideas which advance the disciplines of optoelectronics and optical communications. Communicating current and future applications related to these disciplines are also a function of these meetings.<ref name=2009annouce>{{Cite web
  | last =Ping-kong Alexander Wai
  | title =The 14th OptoElectronics and Communications Conference Hong Kong
  | work =Conference Chair person
  | publisher =OECC 2009 Organizing Committee
  | date =July 2009
  | url =http://www.oecc2009.org/download/fullprog.pdf
  | format =Free PDF download
  | accessdate =2010-09-01}}</ref><ref name=CommMag2009>{{Cite journal
  | last1 =Wai
  | first1 =Ping-Kong
  | last2 =Chiang
  | first2 =Kin
  | title =The 14th OptoElectronics and Communications Conference
  | journal =IEEE Communications Magazine
  | volume =47
  | pages =20
  | year =2009
  | doi =10.1109/MCOM.2009.5307459
  | issue =11}}</ref><ref name=titlepage2009>{{Cite journal|title=Title page |pages=1 |year=2009 |doi=10.1109/OECC.2009.5214090}}</ref><ref name=onlineiexp>{{Cite web
  | last =
  | first =
  | authorlink =
  | coauthors =
  | title =OptoElectronics and Communications Conference, 2009. OECC 2009. 14th
  | work =Published scientific articles
  | publisher =IEEE Explore
  | date =13–17 July 2009
  | url =http://www.ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=5204038
  | format =
  | accessdate =2010-09-01}}</ref>

==Scope==
Topical coverage for this annual conference includes [[optical fiber]] and communication networks (architecture, performance, [[routing]], [[Wavelength-division multiplexing|WDM]] systems, WDM networks, solitons, OTDM, [[CDMA]], and fiber nonlinearities),  [[computer network]]s (protocols, security, design, algorithms, management, and  modules) applications in [[photonics]], commercial technologies (including [[wireless]], [[multimedia]], [[virtual reality]], communications, speech, and software), optoelectronic devices, [[semiconductor lasers]], and other related topics.<ref name=1999meet>{{Cite conference
  | title =Communications, 1999,  APCC/OECC
  | format =2 volumes at 1718 pages
  | publisher =IEEE Explore
  | date =October 18–22, 1999
  | location =Beijing , China
  | url =http://www.ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=824454
  | doi =10.1109/APCC.1999.824454
  | isbn =7-5635-0402-8
  | accessdate =2010-09-01}} [[INSPEC]] Accession Number: 6588742</ref>

==Past OECCs==
{| class="wikitable sortable"
|-
! Year!! Country/Region !! City !! Date !! Papers !! Attendance
|-
| [http://www.oecc2010.org 2010] || Japan || [[Sapporo]] || 5–9 July || 470 || 545
|-
| [http://www.oecc2009.org 2009] || Hong Kong SAR || [[Hong Kong]] || 13–17 July || 451 || 491
|-
| [http://www.iceaustralia.com/ico2008/ 2008] || Australia || [[Sydney]] || 7–11 July || 464 || 463
|-
| 2007 || Japan || [[Yokohama]] || 9–13 July || 398 || 568
|-
| [http://www.oecc2006.org 2006] || Taiwan || [[Kaohsiung]] || 3–7 July || 415 || 452
|-
| 2005 || Korea (South) || [[Seoul]] || 4–8 July || 448 || 573
|-
| 2004 || Japan || Yokohama || 12–16 July || 461 || 634
|-
| 2003 || China || [[Shanghai]] || 13–16 October || 381 || 410
|-
| 2002 || Japan || Yokohama || 8–12 July || 327 || 607
|-
| 2001 || Australia || Sydney || 2–5 July || 274 || 457
|-
| 2000 || Japan || [[Chiba, Chiba|Chiba]] || 11–14 July || 309 || 717
|-
| 1999 || China || [[Beijing]] || 18–22 October || 479 || 500
|-
| 1998 || Japan || Chiba || 13–16 July || 290 || 640
|-
| 1997 || Korea (South) || Seoul || 8–11 July || 338 || 696
|-
| 1996 || Japan || Chiba || 16–19 July || 305 || 642
|}

==References==
{{Reflist}}

==External links==
*[http://www.oeccconference.org OECC Homepage]
*[http://www.ieeexplore.ieee.org/xpl/tocresult.jsp?isnumber=17837 1999 Proceedings online]. IEEE Explore. 18–22 October 1999.
*[http://www.worldcat.org/title/proceedings-apccoecc99-fifth-asia-pacific-conference-on-communications-and-fourth-optoelectronics-and-communications-conference-joint-conference-held-october-18-22-1999-friendship-hotel-beijing-china/oclc/43439039&referer=brief_results 1999 Proceedings] WorldCat. September 2010. OCLC Number: 43439039 ISBN 7-5635-0402-8 or 9787563504022.
*[http://www.ieeexplore.ieee.org/xpl/mostRecentIssue.jsp?punumber=5204038 2009 Proceedings online]. IEEE Explore. 13–17 July 2009.
*[http://lccn.loc.gov/00708848 Bibliographic information]. 1999 proceedings. Library of Congress. 2010.

{{DEFAULTSORT:Oecc}}
[[Category:Optoelectronics]]
[[Category:Conferences]]