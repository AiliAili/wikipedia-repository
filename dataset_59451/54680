{{For|the mysterious art collective of the same name|Netochka Nezvanova (disambiguation){{!}}Netochka Nezvanova (author)}}
{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Netochka Nezvanova
| title_orig    = Неточка Незванова
| translator    = [[Jane Kentish]]
| image         = Netochkacover.jpg
| caption = 
| author        = [[Fyodor Dostoyevsky]] 
| illustrator   = 
| cover_artist  = 
| country       = Russia
| language      = Russian
| series        = 
| genre         = [[Novel]] 
| publisher     = 
| release_date  = 1849
| media_type    = Print ([[Hardcover|Hardback]] & [[Paperback]])
| pages         = 173 pp
| preceded_by   = [[The Double: A Petersburg Poem]]
| followed_by   = [[The Village of Stepanchikovo]]
}}

'''''Netochka Nezvanova''''' ({{lang-ru|Неточка Незванова}}, "Nameless Nobody") is [[Fyodor Dostoyevsky]]'s first but unfinished attempt at writing a novel. He started writing it in 1848 and the first completed section of the book was published in the end of 1849. According to translator [[Jane Kentish]], this first publication was intended as "no more than a prologue to the novel".<ref name="NN">Fyodor Dostoyevsky: ''Netochka Nezvanova''. Translated with an introduction by Jane Kentish. Penguin Books. 1985. ISBN 0-14-044455-6</ref> Further work on the novel was prevented by Dostoyevsky's arrest and exile to a [[Siberian]] detention camp for alleged participation in revolutionary activities {{citation needed|date=May 2014}}. After his return in 1859, Dostoyevsky never resumed work on ''Netochka Nezvanova'', leaving this fragment forever incomplete.

[[Ann Dunnigan]]'s American English translation of the novel was first published in 1972.

==Plot==
The story is about the sorrowful life of a young girl living in extreme poverty in [[Saint Petersburg]], who ends up an orphan and is adopted by a wealthy upper-class family. When she meets her new stepsister, Katya, she instantly becomes infatuated with her and the two soon become inseparable. However, one day Katya is forced to leave to [[Moscow]] with her parents, and for the next eight years Netochka lives with Katya's older sister, who becomes her new mother figure. The story ends abruptly before the two girls reunite.

== Footnotes ==
<!--See http://en.wikipedia.org/wiki/Wikipedia:Footnotes for an explanation of how to generate footnotes using the <ref> and </ref> tags and the tag below -->
<references/>

== External links ==
*[http://ilibrary.ru/text/30/p.1/ Full text of ''Netochka Nezvanova'' in the original Russian]

{{Fyodor Dostoevsky|Fyodor Dostoyevsky}}

[[Category:1849 novels]]
[[Category:Novels by Fyodor Dostoyevsky]]
[[Category:Novels set in Saint Petersburg]]


{{1840s-novel-stub}}