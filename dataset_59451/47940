'''Food defense''' is the protection of food products from intentional contamination or adulteration by biological, chemical, physical, or radiological agents. It addresses additional concerns including physical, personnel and operational security.<ref>{{cite web|url=http://www.fsis.usda.gov/wps/portal/fsis/topics/food-defense-defense-and-emergency-response|title=Food Defense and Emergency Response|work=United States Department of Agriculture}}</ref> This is in contrast to [[food safety]], which is based on accidental or environmental contamination, and [[food security]], which deals with individuals having access to enough food for an active, healthy life. These three terms are often conflated. Food protection is the umbrella term encompassing both food defense and food safety.

Along with protecting the food system, food defense also deals with prevention, protection, mitigation, response and recovery from intentional acts of adulteration.<ref>{{cite web|url=https://www.ncfpd.umn.edu/about/overview/|title=What Is Food Defense?|work=National Center for Food Protection and Defense}}</ref>

==History in the USA==
*1906: The [[Federal Meat Inspection Act]] places requirements on the slaughter, processing and labeling of meat and meat products, domestic and imported.<ref>{{cite web|url=http://www.fsis.usda.gov/wps/portal/fsis/topics/rulemaking/federal-meat-inspection-act|title=Federal Meat Inspection Act|work=United States Department of Agriculture}}</ref>
* 1938: The [[Federal Food, Drug and Cosmetic Act]] establishes definitions and regulation for the safety of food, drugs, and cosmetics.<ref>{{cite web|url=http://www.fda.gov/RegulatoryInformation/Legislation/FederalFoodDrugandCosmeticActFDCAct/default.htm|title=Federal Food, Drug and Cosmetic Act|work=U.S. Food and Drug Administration}}</ref>
*1957: The [[Poultry Products Inspection Act]] requires the [[Food Safety and Inspection Service]] (FSIS) to inspect all domesticated birds meant for human consumption.<ref>{{cite web|url=http://www.fsis.usda.gov/wps/portal/fsis/topics/rulemaking/poultry-products-inspection-acts|title=Poultry Products Inspection Act|work=United States Department of Agriculture}}</ref>
* November 2002: The [[Homeland Security Act]] passed by congress creates the [[Department of Homeland Security]] in response to the [[September 11 attacks]].<ref>{{cite web|url=https://www.dhs.gov/homeland-security-act-2002|title=Homeland Security Act|work=Department of Homeland Security}}</ref>
* December 2003: [[Homeland Security Presidential Directive 7]] establishes a policy to identify and prioritize critical infrastructures.<ref>{{cite web|url=https://www.dhs.gov/homeland-security-presidential-directive-7|title=Homeland Security Presidential Directive 7|work=Department of Homeland Security}}</ref> Food and Agriculture is identified as one of these infrastructures<ref>{{cite web|url=https://www.dhs.gov/critical-infrastructure-sectors|title=Critical Infrastructure Sectors|work=Department of Homeland Security}}</ref>
* January 2004: The [[Homeland Security Presidential Directive 9]] establishes policy to protect agriculture and food systems against terrorist attacks.<ref>{{cite web|url=http://www.epa.gov/homelandsecurityportal/laws-hspd.htm#hspd9|title=Homeland Security Presidential Directive 9|work=Department of Homeland Security}}</ref>
*January 2004: [[DHS]] launches [[Homeland Security Centers of Excellence]] in order to conduct research to address homeland security challenges.<ref>{{cite web|url=https://www.dhs.gov/science-and-technology/centers-excellence|title=Homeland Security Centers For Excellence|work=Department of Homeland Security}}</ref>
*July 2004: The [[National Center for Food Protection and Defense]] now Food Protection and Defense Institute (FPDI) is formally launched with the vision to "defend the safety of the food system through research and education" at the [[University of Minnesota, Twin Cities]].<ref>{{cite web|url=https://www.ncfpd.umn.edu/about/overview/|title= NCFPD: Overview|work=National Center for Food Protection and Defense}}</ref>
* January 2011: [[FDA Food Safety Modernization Act|The Food Safety Modernization Act]] grants the [[FDA]] new authorities and powers, as well as directly providing for protections against intentional adulteration.<ref name="FSMA">{{cite web|url=http://www.fda.gov/Food/GuidanceRegulation/FSMA/ucm247548.htm#SEC106|title=Protection against Intentional Adulteration, Section 106 of the Food Safety Modernization Act|work=Food and Drug Administration}}</ref>

==Food defense event types==
Food defense events can generally be categorized into three types. These could be carried out by a disgruntled employee, sophisticated insider, or intelligent adversary with a specific goal in mind. This goal may be to impact the public, brand, company or the psycho-social stability of a group of people depending on the type. However an event may contain aspects of more than one category.

===Industrial sabotage===
These events include intentional contamination by a disgruntled employee, insider or competitor with the intention of damaging the brand of the company, causing financial problems from a widespread recall or sabotage,<ref>{{cite web|url=http://www.foodqualityandsafety.com/article/food-defense-and-protection/|title=Food Defense and Protection|work=Food Quality and Safety|last=Agres|first=Ted|date= August 6, 2013}}</ref> but not necessarily with the goal of causing widespread illness or public harm. These internal actors often know what procedures are followed in the plant, and how to bypass checkpoints and security controls.<ref>{{cite web|url=http://www.fsis.usda.gov/shared/PDF/Food_Defense_Plan.pdf|title=Developing a Food Defense Plan for Meat and Poultry Slaughter and Processing Plants|work=U.S. Department of Agriculture Food Safety and Inspection Service|date=June 2008}}</ref>

An example of a disgruntled employee is the contamination of frozen foods produced by a subsidiary of Maruha Nichiro Holdings with malathion, a pesticide. The contamination is speculated to have occurred because the employee was dissatisfied with pay and benefits.<ref>{{cite web|url=http://rt.com/news/japan-poison-food-scandal-217/|title=Poisoning Japanese food: Contract worker arrested after 6 mn products recalled|work=RT|date=January 27, 2014}}</ref> The contamination resulted in a recall of 6.4 million potentially tainted products.<ref>{{cite web|url=http://www.news.com.au/world/toshiki-abe-arrested-for-poisoning-frozen-in-japan/story-fndir2ev-1226810502913|title=Toshiki Abe arrested for poisoning frozen food in Japan|work=news.com.au|date=January 25, 2014}}</ref> Nearly 1,800 people are estimated to have been affected, and public confidence in food quality was shaken.<ref>{{cite web|url=http://www.huffingtonpost.com/2014/01/08/japan-tainted-frozen-food_n_4560804.html|title=In Japan, Frozen Food Tainted With Pesticides Leaves Hundreds Sick|work=Huffington Post| first=Elaine|last=Kurtenbach|date=January 23, 2014}}</ref>

===Terrorism===
The reach and complexity of the food system has caused concern for its potential as a terrorist target.<ref>{{cite web|url=http://www.ncbi.nlm.nih.gov/books/NBK57094/|title=Addressing Foodborne Threats to Health|work=National Center for Biotechnology Information|first=Michael|last=Osterholm|date=2006}}</ref>

The first and largest food attack in the US is the [[1984 Rajneeshee bioterror attack]]. 751 individuals were poisoned in [[The Dalles, Oregon]] through the contamination of salad bars with [[Salmonella]] with the intention of affecting the 1984 [[Wasco County]] elections.

===Economically motivated adulteration (EMA)===
The [[FDA]]'s working definition of EMA is
{{Quote|the fraudulent, intentional substitution or addition of a substance in a product for the purpose of increasing the apparent value of the product or reducing the cost of its production, i.e., for economic gain. EMA includes dilution of products with increased quantities of an already-present substance (e.g., increasing inactive ingredients of a drug with a resulting reduction in strength of the finished product, or watering down of juice) to the extent that such dilution poses a known or possible health risk to consumers, as well as the addition or substitution of substances in order to mask dilution.<ref name="EMAdef">{{cite web|url=http://www.fda.gov/ohrms/dockets/98fr/e9-7843.htm|title=Economically motivated adulteration; Public meeting; Request for comment, Docket No. FDA-2009-N-0166|work=Federal Register}}</ref>||}}

EMA, also known as Food Fraud, commonly occurs for financial advantage through the undeclared substitution with alternative ingredients. This poses a health concern due to allergen labeling requirements. In 2016 a restaurateur was jailed for manslaughter after a customer died because cheaper ground nut powder (containing peanut allergen) was used instead of almond powder in preparing a takeaway curry, three weeks after another customer suffered an allergic reaction which required hospital treatment.<ref>{{cite web|title=Peanut curry death: Restaurant owner Mohammed Zaman jailed|url=http://www.bbc.co.uk/news/uk-england-36360111|website=BBC News|publisher=British Broadcasting Corporation|accessdate=1 April 2017|ref=BBCZaman}}</ref> These deliberate acts are intended to evade detection, posing a challenge to regulating bodies and quality assurance methodologies.<ref>Everstine, Karen, John Spink, and Shaun Kennedy. "Economically Motivated Adulteration (EMA) of Food: Common Characteristics of EMA Incidents." J Food Prot Journal of Food Protection 76.4 (2013): 723-35.</ref><ref>{{cite web|url=http://www.foodsafetymagazine.com/magazine-archive1/octobernovember-2013/economically-motivated-adulteration-another-dimension-of-the-e2809cexpanding-umbrella-of-food-defensee2809d/|last=Spink|first=John|work=Food Safety Magazine| title=Economically Motivated Adulteration: Another Dimension of the "Expanding Umbrella of Food Defense"|date=November 2013}}</ref><ref>{{cite web|url=http://www.foodsafetymagazine.com/magazine-archive1/augustseptember-2014/economically-motivated-adulteration-broadening-the-focus-to-food-fraud/|title=Economically Motivated Adulteration: Broadening the Focus to Food Fraud|work=Food Safety Magazine|first=John|last=Spink|date=September 2014}}</ref>

Cases of EMA have been seen in the [[2013 meat adulteration scandal|horse-meat scandal]], [[Protein adulteration in China|melamine contamination scandal]] and the ''[[Salmonella]]'' outbreak involving the [[Peanut Corporation of America]]. The most commonly counterfeited product is extra-virgin [[olive oil]]. Other products commonly associated with food fraud include fish and seafood, honey, meat and grain-based foods, fruit juices, organic foods, coffee, some highly processed foods, tea and spices.<ref>{{cite web|url=https://fas.org/sgp/crs/misc/R43358.pdf|work=U.S. Congressional Research Service|title=Food Fraud and "Economically Motivated Adulteration" of Food and Food Ingredients| date=January 10, 2014| first=Renee |last=Johnson}}</ref> Experts estimate that up to 10% of food products in retail stores contain some degree of adulteration, and EMA events cost the US food industry between $10 billion and $15 billion a year.<ref>{{cite web|url=https://fas.org/biosecurity/education/dualuse-agriculture/1.-agroterrorism-and-foodsafety/economically-motivated-adulteration.html|title=Economically Motivated Adulteration|work=Federation of American Scientists}}</ref>

==Protection strategies==
Regulatory bodies and industry can implement strategies and use tools in order to protect their supply chains and processing facilities from intentional contamination or adulteration. Defined as protection or mitigation, this process involves both assessing the risk and vulnerabilities of a single supply chain or facility and working to mitigate these risks in order to prevent an event, and reducing the severity of an event.

===Tools===
The [[FDA]] has developed several tools for the food industry, including among others:<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/ToolsEducationalMaterials/ucm20027049.htm|title=Tools and Educational Materials|work=US Food and Drug Administration}}</ref>

* '''Mitigation Strategies Database'''<ref>{{cite web|url=http://www.accessdata.fda.gov/scripts/fooddefensemitigationstrategies/|title=Food Defense Mitigation Strategies Database|work=US Food and Drug Administration}}</ref> which includes a range of preventative measures and suggestions for companies
* '''Food Defense 101''',<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/ToolsEducationalMaterials/ucm353774.htm|title= Food Defense 101|work=US Food and Drug Administration}}</ref> focused on training for preparedness against an intentional attack
* '''FREE-B''',<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/ToolsEducationalMaterials/ucm295902.htm|title=Food Related Emergency Exercise Bundle|work=US Food and Drug Administration}}</ref> a compilation of both intentional and unintentional food contamination scenarios
* '''Food Defense Plan Builder''',<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/ToolsEducationalMaterials/ucm349888.htm|title=Food Defense Plan Builder|work=US Food and Drug Administration}}</ref> designed to assist owners and operators of food facilities with developing personalized food defense plans

===Risk assessments===
It is difficult to quantify the risk in a system, due to the stochastic nature of the events. However, it is possible to use other sources of information, such as gathered intelligence, economic and social drivers and data mining to assess the potential weaknesses and entry points of a system, along with the scale of consequences related to a breach in that system. Tools being developed for this purpose by the National Center for Food Protection and Defense include Focused Integration of Data for Early Signals (FIDES) and Criticality Spatial Analysis (CRISTAL).<ref>{{cite web|url=https://www.ncfpd.umn.edu/tools/tools/|title=Tools|work=National Center for Food Protection and Defense}}</ref>

Food industry stakeholders can perform a vulnerability assessment to understand the vulnerabilities of their system, the consequences of an event and the potential threats and agents. This allows companies  to assess and prioritize vulnerabilities within their facility and system. A software tool has been developed by the FDA to assist with this process.<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/ToolsEducationalMaterials/ucm295900.htm|title=Vulnerability Assessment Software|work=US Food and Drug Administration}}</ref> Companies are encouraged to create a Food Defense Plan based on the vulnerability and risk assessments performed, detailing their plan of action in the case of an intentional or unintentional contamination event.

===Vulnerability assessment tools===
The FDA has identified four key activities, or common vulnerabilities within the food system: bulk liquid receiving and loading, liquid storage and handling, secondary ingredient handling, and mixing or similar activities. Knowledge of these key activities can direct action plans.

'''CARVER + Shock''' is used to consider the factors involved in an intentional contamination event<ref>{{cite web|url=http://www.fda.gov/Food/FoodDefense/FoodDefensePrograms/ucm376791.htm|title=CARVER+Shock Primer|work=US Food and Drug Administration}}</ref>
*Criticality - measure of public health and economic impacts of an attack
*Accessibility – ability to physically access and egress from target
*Recuperability – ability of system to recover from an attack
*Vulnerability – ease of accomplishing attack
*Effect – amount of direct loss from an attack as measured by loss in production
*Recognizability – ease of identifying target
*Shock – the combined health, economic, and psychological impacts of an attack

===Supply chain control===
Understanding the supply chains involved in a food system is difficult due to their complex and often obscured nature, but having a good understanding of where incoming ingredients come from can help to mitigate contamination and adulteration. Good supply chain management coupled with regular audits and quality assurance analyses can help to safeguard companies from contamination originating outside the facility.

In addition, companies can take advantage of existing scenario based tools and should follow [[Good Manufacturing Practice]] guidelines.

===Mitigation strategies===

====Physical measures====
*Secure the facility perimeter and perform periodic checks
*Use controlled-access procedures for people or vehicles entering the plant or parking area
*Install an alarm system, cameras and sufficient lighting
*Designate restricted areas for authorized employees, restrict non-employees to non-production areas
*Limit access to control systems
*Use [[tamper-evident technology|tamper-evident]] or [[tamper resistance|tamper-resistant]] packaging

====Policy measures====
*Use a system to identify personnel by their specific functions
*Conduct background checks on all employees and contractors who will be working in sensitive operations
*Train employees on food defense and security awareness, including recognition of suspicious behavior or individuals

====Management====
*Maintain records to allow easy trace-back and trace-forward of materials and products
*Have available a list of contacts for local, state and federal agencies
*Implement an inventory control system

More strategies for various categories and nodes of the food system can be found in the various mitigation strategies databases available through the FDA and USDA.

==Stakeholders involved in food defense==

===Global===
*[[Food and Agriculture Organization]] (FAO)
*[[World Health Organization]] (WHO)
*[[World Trade Organization]] (WTO)
* [[Interpol]]

===United States===

====Federal level====
*[[Food and Drug Administration]] (FDA)
**[[Center for Food Safety and Applied Nutrition]] (CFSAN)
*[[United States Department of Agriculture]] (USDA)
**[[Food Safety and Inspection Service]] (FSIS)
**[[Foreign Agricultural Service]] (FAS)
*[[Centers for Disease Control and Prevention]] (CDC)
*[[Federal Bureau of Investigation]] (FBI)
*[[Department of Homeland Security]] (DHS)
**[[U.S. Customs and Border Protection|Customs and Border Protection]] (CBP)
*[[Department of State]]
*National Center for Food Protection and Defense (NFCPD) now Food Protection and Defense Institute (FPDI)

====State level====
*State health departments
*State departments of agriculture
*Local law enforcement

===Other groups===
*Industry representatives
*Academic researchers

==References==
{{reflist}}

[[Category:Food safety]]