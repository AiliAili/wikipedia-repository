{{Infobox fashion designer
|image = 
|image_size = 220px
|name = Sherri Hill
|nationality = American
|birth_date = {{bya|1949}}
|birth_place = [[Minco, Oklahoma|Minco]], [[Oklahoma]]
|residence = [[Austin, Texas]]<ref>[http://www.fashiontimes.com/articles/27656/20160911/10-questions-for-designer-sherri-hill-inside-the-atelier.htm]</ref>
|spouse = Charles Hill
|children = 4
|label_name = Sherri Hill
|website = http://www.sherrihill.com
}}
 
'''Sherri Hill''' (born 1949 in [[Minco, Oklahoma]], USA) is an [[United States|American]] [[Fashion design|fashion designer]] and [[businesswoman]] whose specialty is formal evening wear.

== Early years ==
Sherri Hill was born Sherri Branum,<ref name="People 88">{{cite web|last=Neill|first=Michael|title=Doing Everything in the Correct Sequins, Two Oklahoma Sisters Design Hits for Misses|url=http://www.people.com/people/archive/article/0,,20099899,00.html|date=September 12, 1988|publisher=People magazine|accessdate=July 9, 2013}}</ref> one of five siblings of James and Roberta Branum and was born and raised in a small town in Oklahoma.<ref>{{cite web|title=MTV Style {{!}} Sherri Hill Exclusive Interview|url=http://www.mtv.com/videos/interview/652361/sherri-hill-talks-working-with-snooki-new-collection.jhtml#id=1663824|accessdate=July 9, 2013}}</ref>  Hill’s parents owned local general stores selling everything from fabric and clothing to shampoo, candy and school supplies; she was given the opportunity at age eight to join her older sisters in part-time work at the family store. The colorful fabric that was sold in the store sparked Hill's interest in fashion and eventually she was allowed to “dress” the four mannequins in the front windows by draping yards of fabric around each.<ref name="People 88" /> 

While Hills was studying fashion design at the [[University of Oklahoma]], her family opened stores in [[Norman, Oklahoma|Norman]] and [[Oklahoma City]] where they focused on ladies’ clothing and evening wear.<ref>{{cite web|last=McDonnell|first=Brandy|title=Oklahoma designer Sherri Hill dresses Carrie Underwood, Miranda Lambert, "Snooki" Polizzi and more|url=http://newsok.com/oklahoma-designer-sherri-hill-dresses-carrie-underwood-miranda-lambert-snooki-polizzi-and-more/article/3826307|publisher=newsok.com|accessdate=July 9, 2013}}</ref>  It was during these years that Hill began designing her own line of evening gowns.

== Career ==
After university, Hill worked for various manufacturers in the evening wear business. She started gaining recognition in the pageant world when various [[Miss America]], [[Miss USA]] and [[Miss Universe]] contestants won in the gowns she had designed.<ref>{{cite web|title=2012 MISS USA: opening number-Sherri Hill Fashion|url=http://www.missuniverse.com/missusa/YouVideos/index?key=Sherri+Hill|publisher=Miss USA|accessdate=July 9, 2013}}</ref> Along with designing pageant gowns, Hill worked for 12 years as designer for [[Jovani Fashions]], helping launch the brand. Hill eventually left Jovani in 2008 to begin her own evening gown line under the name Sherri Hill, Inc. which now has a network of over 800 stores.<ref>http://www.sherrihill.com/prom-dress-shops.php|publisher=Sherri Hill, Inc.|accessdate=5 July 2012</ref>

Various celebrities including [[Selena Gomez]], [[Carrie Underwood]], [[Miranda Lambert]] , [[Zendaya]], [[Ariana Grande]], [[Gabby Douglas]], [[Fifth Harmony]], [[Bella Thorne]] and [[Ryan Newman (actress)|Ryan Newman]]<ref>{{cite news|last=Pearson|first=Jennifer|title=Dreams do come true! Bella Thorne is a princess for the day as she celebrates her 15th birthday with a fairytale bash   |url=http://www.dailymail.co.uk/tvshowbiz/article-2221170/Dreams-come-true-Bella-Thorne-looks-like-princess-fairy-tale-birthday-bash.html|accessdate=July 8, 2013|location=London|work=Daily Mail|date=October 22, 2012}}</ref> have worn Hill’s designs for various red carpet, charity and social events. One of the dresses of the Sherri Hill brand, style 1403, was worn by [[Selena Gomez]] while performing in a 2011 concert.<ref name="MTV Gomez">{{cite web|last=Hochberg|first=Emily|title=Selena Gomez Or Kendall Jenner: Who Wore Sherri Hill Best?|url=http://style.mtv.com/2011/03/21/selena-gomez-kendall-jenner-sherri-hill/|accessdate=July 8, 2013}}</ref> [[Kendall Jenner]] and [[Nicole Polizzi]] are also fans.<ref name="MTV Gomez" />

Hill was on Google's annual "Year in Search" results for the top-trending fashion designers in 2014.<ref>{{cite web|url=http://wwd.com/fashion-news/fashion-scoops/top-10-fashion-queries-8077732/ |title=Google Publishes Annual "Year in Search" Results |publisher=Women's Wear Daily |accessdate=2015-04-15|first=Lisa|last=Lockwood|date=December 16, 2014}}</ref> <ref>{{cite web|url=https://www.google.com/trends/topcharts?hl=en-US&geo=US&date=2014#vm=trendingchart&cid=a2ffc043-fdb4-4679-b547-3e31fe5fd3a2&geo=US&date=2014&cat=lifestyle |title=Google Year in Search 2014 |publisher=Google |accessdate=2015-04-15}}</ref>

== New York Fashion Week ==
In 2011, Sherri Hill debuted at [[New York Fashion Week]] in the [[Trump Tower]]. This was [[Kendall Jenner]]<ref>{{cite web|last=Watts|first=Jenisha|title=Another Day, Another Jenner: Kendall Hits the Catwalk for Sherri Hill|url=http://stylenews.peoplestylewatch.com/2011/09/15/kendall-jenner-models-sherri-hill-show/|accessdate=July 8, 2013}}</ref>'s first runway show launching her New York Fashion Week runway career. Kendall's celebrity sisters Kim Kardashian and Kourtney Kardashian sat front row in support of Kendall's runway debut.<ref>{{cite news|title=See Kendall Jenner's Runway Modeling Debut!|url=http://www.eonline.com/news/263973/see-kendall-jenner-s-runway-modeling-debut|accessdate=April 15, 2015 |work=Eonline|first=Cristina|last=Gibson|date=September 15, 2011}}</ref> Following the runway show Kendall expressed her excitement by tweeting "OMGGGG just finished my first ever runway show for @sherrihill!!! #amazing #imaddictedtorunway!"<ref>{{cite web|title=Kendall Jenner|url=https://twitter.com/KendallJenner/status/114125525550772224 =April 15, 2015 |work=Twitter|first=Kendall|last=Jenner|date=September 14, 2011}}</ref>

World-famous model, [[Carmen Dell’Orefice]] also walked the 2011 Sherri Hill runway as well as Miss China 2011 [[Luo Zilin]], and Miss USA 2011 [[Alyssa Campanella]].

In September 2012, Hill debuted her Spring 2013 collection at [[New York Fashion Week]] in the Trump Tower with Kendall and [[Kylie Jenner]]<ref>{{cite news|title=Kendall And Kylie Jenner Model In Sherri Hill Spring 2013 Runway Show|url=http://www.huffingtonpost.com/2012/09/08/kendall-and-kylie-jenner-model-runway-show-sherri-hill-spring-2013_n_1866906.html|accessdate=July 8, 2013 | work=Huffington Post|first=Rebecca|last=Adams|date=September 8, 2012}}</ref> and Miss Universe 2008 [[Dayana Mendoza]] on the runway.

At the 2013 New York Fashion Week, Sherri Hill held an 'Evening By Sherri Hill' show at the Trump Tower.<ref name="eonline1">{{cite web|last=Finn|first=Natalie|title=Duck Dynasty's Sadie Robertson Makes New York Fashion Week Debut—Catch Up With the Family Backstage!|url=http://www.eonline.com/news/457509/duck-dynasty-s-sadie-robertson-makes-new-york-fashion-week-debut-catch-up-with-the-family-backstage|publisher=E! Online|accessdate=November 7, 2013}}</ref> Hill debuted her new prom and couture collections in front of several celebrities,<ref>{{cite web|last=Shipley|first=Kelci|title=Emmy Rossum, Kat Graham, Nick Jonas And More Celebs At Fashion Week|url=http://style.mtv.com/2013/09/10/emmy-rossum-kat-graham-nick-jonas-nyfw/|publisher=MTV Style|accessdate=November 7, 2013}}</ref> including the [[Duck Dynasty]] family.<ref name="eonline1"/> Sadie Robertson, the 16-year-old daughter of [[Willie Robertson]] of [[Duck Commander]], walked in the runway show modelling the 'Sadie Robertson Live Original' range.<ref>{{cite news|last=Sieczkowski|first=Cavan|title='Duck Dynasty' Daughter Sadie Robertson Walks The Runway At New York Fashion Week|url=http://www.huffingtonpost.com/2013/09/11/duck-dynasty-sadie-robertson_n_3906356.html|publisher=Huffington Post|accessdate=November 7, 2013|date=September 11, 2013}}</ref>

At the February, 2016 NYFW show, Sherri Hill's show was held at Gotham Hall and in addition to Sadie Robertson, she had Miss Universe, Pia Alonzo Wurtzbach walk the runway.

==Television appearances==
[[Keeping Up with the Kardashians]] (2011) as herself (Season 6, Episode 2)<ref>{{cite web|url=https://www.youtube.com/watch?feature=endscreen&NR=1&v=4rTowkxJRKs |title=Kardashians: Modeling Mayhem |publisher=YouTube |date= |accessdate=2013-12-24}}</ref> Kendall Jenner is hired to walk her first New York Fashion Week runway show by Sherri Hill.

== Anti-counterfeit work ==

In June 20, 2012, Hill made public her success in a lawsuit against online counterfeiters. She was awarded $5,000,000 in the case against the two defendants that were accused of selling hundreds of fake gowns under the Sherri Hill pretense.<ref>{{cite web|last=Gioconda Law Group PLLC|title=Dress Designer Sherri Hill Wins $5M Judgment Against Online Counterfeiters in Lawsuit Brought by The Gioconda Law Group PLLC (press release)|url=http://globenewswire.com/news-release/2012/06/20/479982/259894/en/Dress-Designer-Sherri-Hill-Wins-5M-Judgment-Against-Online-Counterfeiters-in-Lawsuit-Brought-by-The-Gioconda-Law-Group-PLLC.html|accessdate=July 8, 2013}}</ref>  After winning the legal battle in 2012, the company has hired staff devoted to reporting violations of copyright laws via the Internet.<ref>{{cite web|last=Davis|first=Josh|title=Scammers target promgoers with fake designer dresses|url=http://www.today.com/news/scammers-target-promgoers-fake-designer-dresses-1C9255117|accessdate=July 8, 2013}}</ref> 

In March 2013, Sherri Hill, Inc. was featured on ''[[The Today Show]]''<ref>{{cite web|last=Rossen|first=Jeff|title=Rossen Reports: Scammers target promgoers with fake designer dresses|url=http://www.today.com/video/today/51503000#51503000|accessdate=July 8, 2013}}</ref>  and discussed their work with fighting knock off designer goods and their experience with individuals that have received counterfeit Sherri Hill dresses from fraudulent online retailers.

In May 2015, Sherri Hill, Inc. was featured on "[[Inside Edition]]"<ref>{{cite web|title=Prom Dress Knock Offs Break Teen Hearts with Shoddy Quality|url=http://www.insideedition.com/investigative/10431-prom-dress-knock-offs-break-teen-hearts-with-shoddy-quality|accessdate=May 12, 2015}}</ref>  and discussed continuing challenges faced with knock off designer goods.

== References ==
{{reflist}}

== External links ==
*[http://www.sherrihill.com Sherri Hill official web site]
*http://style.mtv.com/2011/03/21/selena-gomez-kendall-jenner-sherri-hill/



{{DEFAULTSORT:Hill, Sherri}}
[[Category:1949 births]]
[[Category:Living people]]
[[Category:American fashion designers]]
[[Category:High fashion brands]]