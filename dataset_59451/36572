{{good article}}
{{Use dmy dates|date=August 2016}}
{{Infobox Olympic event
| event       = Archery
| games       = 2004 Summer
| image       = [[File:Archery, Athens 2004.png|150px]]
| caption     = 
| venue       = [[Panathinaiko Stadium]]
| dates       = 15 - 21 August 2004
| competitors = 128
| nations     = 43
| prev        = [[Archery at the 2000 Summer Olympics|2000]]
| next        = [[Archery at the 2008 Summer Olympics|2008]]
}}
{{ArcheryAt2004SummerOlympics}}

'''[[Archery]] at the [[2004 Summer Olympics]]''' was held at [[Panathinaiko Stadium]] in [[Athens]], [[Greece]] with ranking rounds on 12 August and regular competition held from 15 August to 21 August. One hundred twenty-eight archers from forty-three nations competed in the four [[gold medal]] events—individual and team events for men and for women—that were contested at these games.<ref name="or2004b">{{cite encyclopedia |url=http://www.la84foundation.org/6oic/OfficialReports/2004/or2004b.pdf |format=pdf |title=Official Report of the XXVIII Olympiad |volume=2 |publisher=Athens Organising Committee for the Olympic Games |isbn=960-88101-8-3 |pages=237–238 |date=November 2005 |accessdate=2008-08-02| archiveurl= https://web.archive.org/web/20080819195306/http://www.la84foundation.org/6oic/OfficialReports/2004/or2004b.pdf| archivedate= 19 August 2008 <!--DASHBot-->| deadurl= no}}</ref>

The stadium, often called Kallimarmaro, is notable as the site of the [[1896 Summer Olympics|first Olympic Games]] and even earlier, where the Ancient Greeks' [[Panathenean Games]] were hosted.<ref name="or2004b" /> At the behest of James Easton, president of the [[International Archery Federation]], archery events were held in the historic stadium, hoping that its history and natural beauty would attract the public to the sport.<ref name="Wind">{{cite news |publisher=[[New York Times]] |accessdate=2008-08-03 |url=https://query.nytimes.com/gst/fullpage.html?res=9C0CE3DE103FF934A2575BC0A9629C8B63# |title=It's Ready, Aim, Wait for the Mighty Wind Gusts to Die Down, and Fire |date=2004-08-17 |author=Drape, Joe}}</ref> [[Laurence Godfrey (archer)|Laurence Godfrey]], the fourth-place finisher in the men's individual event, remarked that the stadium inspired pride, while American [[Vic Wunderle]] spoke for most of the archers in saying, "It's a great honor and a privilege to be able to compete inside the 1896 Olympic Stadium."<ref name="stadium">{{cite news |url=http://www.accessmylibrary.com/coms2/summary_0286-8520756_ITM |title=Athens' 1896 Stadium is a Classic |date=2004-08-19 |publisher=South Florida Sun-Sentinel |author=Berardino, Mike |accessdate=2008-08-03}}</ref>

The [[South Korea at the 2004 Summer Olympics|Korean team]] won three out of the four gold medals contested. Four [[Olympic record]]s and several other [[world record]]s were [[Olympic records at the 2004 Summer Olympics|broken at these games]], despite poor weather conditions during the preliminary rounds of competition.

==Qualification and format==
[[Image:Athens archery.jpg|right|thumb|Matches in progress during the women's round of 64 at Panathinaiko Stadium]]
There were four ways for National Olympic Committees (NOCs) to qualify individual archers for the Olympics in archery. For each gender, the host nation ([[Greece]]) was guaranteed three spots.  The 2003 World Target Competition's top 8 teams (not including the host nation) each received three spots, and the 19 highest ranked archers after the team qualifiers were removed also received spots.  Fifteen of the remaining eighteen spots were divided equally among the five [[Olympic symbols|Olympic continents]] for allocation in continental tournaments. The last three spots in each gender were determined by the Tripartite Commission. Sixty-four archers of each sex took part in the Olympics, with each NOC being able to enter a maximum of three archers.<ref name="or2004b" />

For all archery events at the Olympics, archers stand 70 metres from their target. The target consists of concentric circles, and has a total diameter of 122 cm. Archers earn points based on which circle their arrow landed in, with ten points awarded for hitting the center circle, and one point awarded for hitting the outermost circle.<ref name="or2004b" /> During the ranking rounds, each archer shot twelve ends, or groups, of six arrows per end. The score from that round determined the match-ups in the elimination rounds, with high-ranking archers facing low-ranking archers. The first three rounds of elimination used six ends of three arrows, narrowing the field of archers from 64 to 8. The three final rounds (quarterfinals, semifinals, and medal matches) each used four ends of three arrows.<ref name="Guide">{{cite web |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/sport_guide/default.stm |title=Beginner's Guide to Archery |date=2004-04-23 |accessdate=2008-08-02 |publisher=[[BBC Sport]]}}</ref> 

Thirteen men's and fifteen women's teams competed in the team competitions. The teams consisted of the country's three archers from the individual round, and the team's initial ranking was determined by summing the three members' scores in the individual ranking round. Each round of eliminations consisted of each team firing 27 arrows (9 by each archer).<ref name="Guide" />

==Medal summary==
{| {{MedalistTable|type=Event}}
|-
|Men's individual<br/>{{DetailsLink|Archery at the 2004 Summer Olympics – Men's individual}}
| {{flagIOCmedalist|[[Marco Galiazzo]]|ITA|2004 Summer}}
| {{flagIOCmedalist|[[Hiroshi Yamamoto (archer)|Hiroshi Yamamoto]]|JPN|2004 Summer}}
| {{flagIOCmedalist|[[Tim Cuddihy]]|AUS|2004 Summer}}
|-
|Men's team<br/>{{DetailsLink|Archery at the 2004 Summer Olympics – Men's team}}
| {{flagIOCteam|KOR|2004 Summer}}<br>[[Im Dong-Hyun]]<br>[[Jang Yong-Ho]]<br>[[Park Kyung-Mo]]
| {{flagIOCteam|TPE|2004 Summer}}<br>[[Chen Szu-yuan]]<br>[[Liu Ming-huang]]<br>[[Wang Cheng-pang]]
| {{flagIOCteam|UKR|2004 Summer}}<br>[[Dmytro Hrachov]]<br>[[Viktor Ruban]]<br>[[Oleksandr Serdyuk]]
|-
|Women's individual<br/>{{DetailsLink|Archery at the 2004 Summer Olympics – Women's individual}}
| {{flagIOCmedalist|[[Park Sung-Hyun]]|KOR|2004 Summer}}
| {{flagIOCmedalist|[[Lee Sung-Jin]]|KOR|2004 Summer}}
| {{flagIOCmedalist|[[Alison Williamson]]|GBR|2004 Summer}}
|-
|Women's team<br/>{{DetailsLink|Archery at the 2004 Summer Olympics – Women's team}}
| {{flagIOCteam|KOR|2004 Summer}}<br>[[Lee Sung-Jin]] <br> [[Park Sung-Hyun]] <br> [[Yun Mi-Jin]]
| {{flagIOCteam|CHN|2004 Summer}}<br>[[He Ying]] <br> [[Lin Sang]] <br> [[Zhang Juanjuan]]
| {{flagIOCteam|TPE|2004 Summer}}<br>[[Chen Li-Ju]] <br> [[Wu Hui-ju]] <br> [[Yuan Shu-chi]]
|}

==Event summary==
For the sixth Olympics in a row, the South Korean team came out as the clear victor, taking three out of the four gold medals in Athens.<ref name="or2004b" /> Korean archers set new world records in the women's individual ([[Park Sung-Hyun]]) and team (Park, [[Yun Mi-Jin]], and [[Lee Sung-Jin]]) ranking rounds and the men's individual ranking round ([[Im Dong Hyun]]),<ref name="records">{{cite news |url=http://www.usatoday.com/sports/olympics/athens/skill/2004-08-12-qualifying-record_x.htm |title=South Korean archers break three records in qualifying |publisher=[[Associated Press]] |date=2004-08-12 |accessdate=2008-08-03}}</ref> though none of those scores counted as Olympic records because the ranking round was held before the opening ceremony.<ref name=ResultBook>{{cite book |author=Organizing Committee for the Olympic Games Athens 2004 |title=Archery Official Results Book |url=http://www.la84foundation.org/6oic/OfficialReports/2004/Results/Archery.pdf |format=PDF |date=2004 |language=Greek, French}}</ref> [[List of Olympic records in archery|Olympic records]] were broken in both the men's and women's 36-arrow 1/16 and 1/8 rounds combined (by [[Chen Szu Yuan]] of Chinese Taipei and Yun of Korea), as well as in the men's 18-arrow match (by [[Park Kyung Mo]] of Korea) and 36-arrow finals rounds combined (by [[Tim Cuddihy]] of Australia).<ref name=ResultBook/>

{{Rquote|right|[[Panathinaiko Stadium|This historic stadium]] has given me strength, because it is a great feeling to see the [[Acropolis of Athens|Acropolis]] next to you.|[[Wietse van Alten]]<ref name="MenIndi">{{cite news |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/3569920.stm |title=Fairweather Crashes Out |date=2004-08-16 |publisher=[[BBC Sport]] |accessdate=2008-08-03}}</ref>}}
In the men's events, the Korean team shot 12 maximum scores of 10 to win the gold medal against Chinese Taipei 251-245. Losing by two points, the United States failed to fend of the Ukraine team to capture the bronze.<ref name="MenTeam">{{cite news |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/3586756.stm |title=South Koreans Retain Title |date=2004-08-21 |publisher=[[BBC Sport]] |accessdate=2008-08-03}}</ref> The event causing the most upset however was the men's individual, the only event that the Korean team has never won and yet again failed to clinch. Defending champion [[Simon Fairweather]] was ousted from the competition in a first round loss due to blustery weather conditions.<ref name="MenIndi" /> The wind caused some archers like Fairweather to make one-point shots, and its strength even caused others to miss their targets completely.<ref name="Wind" /> The final matches of this event also saw competitors coming close in score, with Italian [[Marco Galiazzo]] beating the Japanese [[Hiroshi Yamamoto (archer)|Hiroshi Yamamoto]] by only two points to win gold. Even closer still was the bronze medal match, in which Britain's [[Laurence Godfrey (archer)|Laurence Godfrey]] was outshot 112-113 by seventeen-year-old Australian [[Tim Cuddihy]],<ref name="MenIndi2">{{cite news |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/3578886.stm |title=Godfrey Loses Out on Bronze |date=2004-08-19 |publisher=[[BBC Sport]] |accessdate=2008-08-03}}</ref> who himself only managed to get into the semifinals by one point.<ref name="Cuddihy">{{cite news |url=http://www.theage.com.au/olympics/articles/2004/08/18/1092508491642.html |title=Cuddihy Scrapes Through on Final Arrow |date=2004-08-18 |accessdate=2008-08-03 |author=Labi, Sharon |publisher=[[The Age]]}}</ref>

The woman's individual event fell easily to the Koreans; they have won this event continuously since the [[1984 Summer Olympics]] in Los Angeles<ref>{{cite news|title=South Koreans Set Olympic, World Archery Records |url=http://www.pe.com/sharedcontent/sports/olympics/LatestNews/news-cw/081204ccwcSportsOLYarcheryrecord.6ca9161b.html |date=2004-08-12 |accessdate=2008-08-03 |publisher=[[Associated Press]] |deadurl=yes |archiveurl=https://web.archive.org/web/20110520022520/http://www.pe.com/sharedcontent/sports/olympics/LatestNews/news-cw/081204ccwcSportsOLYarcheryrecord.6ca9161b.html |archivedate=May 20, 2011 }}</ref> and swept all three medals at the [[2000 Summer Olympics|2000 Olympics]].<ref name="Williamson">{{cite news |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/3578092.stm |title=Williamson hints at rethink |author=Gordos, Phil |publisher=[[BBC Sport]] |date=2004-08-18 |accessdate=2008-08-03}}</ref> Though both gold and silver fell to Korean archers, [[Alison Williamson]] captured the bronze medal, giving Britain its first medal in archery since 1992.<ref name="Williamson" /> In the team event, the Korean women beat the Chinese team 241-240 to win the gold medal, making this their eleventh straight women's team championship win. Taiwan easily took the bronze medal over France.<ref name="WomenTeam">{{cite news |publisher=[[BBC Sport]] |accessdate=2008-08-03 |url=http://news.bbc.co.uk/sport2/hi/olympics_2004/archery/3582848.stm |title=Koreans Take 11th Title |date=2004-08-20}}</ref>

== Participating nations ==
Forty-three nations contributed archers to compete in the events.  Below is a list of the competing nations; in parentheses are the number of national competitors.<ref name=ResultBook />

{{col-begin|width=100%}}
{{col-1-of-4}}
*{{flagIOC|AUS|2004 Summer|6}}
*{{flagIOC|BLR|2004 Summer|2}}
*{{flagIOC|BHU|2004 Summer|2}}
*{{flagIOC|BUL|2004 Summer|1}}
*{{flagIOC|CAN|2004 Summer|2}}
*{{flagIOC|CHN|2004 Summer|5}}
*{{flagIOC|TPE|2004 Summer|6}}
*{{flagIOC|CUB|2004 Summer|1}}
*{{flagIOC|DEN|2004 Summer|1}}
*{{flagIOC|EGY|2004 Summer|4}}
*{{flagIOC|ESA|2004 Summer|1}}
{{col-2-of-4}}
*{{flagIOC|FIJ|2004 Summer|1}}
*{{flagIOC|FIN|2004 Summer|1}}
*{{flagIOC|FRA|2004 Summer|6}}
*{{flagIOC|GEO|2004 Summer|2}}
*{{flagIOC|GER|2004 Summer|4}}
*{{flagIOC|GBR|2004 Summer|4}}
*{{flagIOC|GRE|2004 Summer|6}}
*{{flagIOC|IND|2004 Summer|6}}
*{{flagIOC|INA|2004 Summer|2}}
*{{flagIOC|ITA|2004 Summer|4}}
*{{flagIOC|JPN|2004 Summer|6}}
{{col-3-of-4}}
*{{flagIOC|KAZ|2004 Summer|3}}
*{{flagIOC|KOR|2004 Summer|6}}
*{{flagIOC|LAO|2004 Summer|1}}
*{{flagIOC|LUX|2004 Summer|1}}
*{{flagIOC|MAS|2004 Summer|1}}
*{{flagIOC|MRI|2004 Summer|1}}
*{{flagIOC|MEX|2004 Summer|3}}
*{{flagIOC|MYA|2004 Summer|1}}
*{{flagIOC|NED|2004 Summer|3}}
*{{flagIOC|NZL|2004 Summer|1}}
*{{flagIOC|PHI|2004 Summer|1}}
{{col-4-of-4}}
*{{flagIOC|POL|2004 Summer|4}}
*{{flagIOC|RUS|2004 Summer|5}}
*{{flagIOC|RSA|2004 Summer|1}}
*{{flagIOC|ESP|2004 Summer|2}}
*{{flagIOC|SWE|2004 Summer|3}}
*{{flagIOC|TJK|2004 Summer|1}}
*{{flagIOC|TGA|2004 Summer|1}}
*{{flagIOC|TUR|2004 Summer|4}}
*{{flagIOC|UKR|2004 Summer|6}}
*{{flagIOC|USA|2004 Summer|6}}
{{col-end}}

==Medal table==
[[South Korea|Korea]] continued its domination of the sport, winning three of the four [[gold medal]]s as well as a silver.  [[Marco Galiazzo]] won the men's individual competition, earning Italy the nation's first gold medal in Olympic archery, blocking [[Hiroshi Yamamoto (archer)|Hiroshi Yamamoto]]'s attempt to win Japan's first gold medal.  [[Chinese Taipei]], which had never before won a medal in archery, won a silver and a bronze.

{| {{RankedMedalTable}}
|-
| 1 ||align=left| {{flagIOCteam|KOR|2004 Summer}} || 3 || 1 || 0 || 4
|-
| 2 ||align=left| {{flagIOCteam|ITA|2004 Summer}} || 1 || 0 || 0 || 1
|-
| 3 ||align=left| {{flagIOCteam|TPE|2004 Summer}} || 0 || 1 || 1 || 2
|- 
|rowspan=2| 4 ||align=left| {{flagIOCteam|CHN|2004 Summer}} || 0 || 1 || 0 || 1
|- 
|align=left| {{flagIOCteam|JPN|2004 Summer}} || 0 || 1 || 0 || 1
|-
|rowspan=3| 6 ||align=left| {{flagIOCteam|AUS|2004 Summer}} || 0 || 0 || 1 || 1
|- 
|align=left| {{flagIOCteam|GBR|2004 Summer}} || 0 || 0 || 1 || 1
|-
|align=left| {{flagIOCteam|UKR|2004 Summer}} || 0 || 0 || 1 || 1
|}

==References==
{{reflist|2}}

{{EventsAt2004SummerOlympics}}
{{Archery at the Summer Olympics}}

[[Category:Archery at the Summer Olympics|2004]]
[[Category:2004 Summer Olympics events]]
[[Category:2004 in archery|Olympics]]
[[Category:Archery in Greece]]