{{Infobox book  | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = Kéraban the Inflexible
| title_orig     = Kéraban-le-têtu
| translator     = 
| image          = 'Kéraban the Inflexible' by Léon Benett 005.jpg
| caption  =  An illustration from ''Kéraban the Inflexible'' by painted by [[Léon Benett]]
| author         = [[Jules Verne]]
| illustrator    = [[Léon Benett]]
| cover_artist   = 
| country        = France
| language       = French
| series         = [[Voyages Extraordinaires|The Extraordinary Voyages]] #24
| subject        = 
| genre          = [[Adventure novel]]
| publisher      = [[Pierre-Jules Hetzel]]
| pub_date       = 1883
| english_pub_date = 1883-1884
| media_type     = Print ([[Hardcover|Hardback]])
| pages          = 
| oclc           = 
| preceded_by    = [[The Green Ray]]
| followed_by    = [[The Vanished Diamond]]
}}
'''''Kéraban the Inflexible''''' ({{lang-fr|Kéraban-le-têtu}}, [[1883 in literature|1883]]) is an [[adventure novel]] written by [[Jules Verne]].

==Synopsis==
Jan van Mitten and his valet Bruno (both of Rotterdam, Holland) are in Istanbul, Turkey. The pair are going to meet with Van Mitten’s tobacco business associate, a headstrong man named Kéraban. At Van Mitten’s meeting Kéraban decides to take them to dinner at his home in Scutari on the other side of the Bosphorus Strait. Just before they are going to cross the Strait a tax is imposed on all vessels that can be used to cross the strait. Enraged by this new tax, Kéraban decides to take his associates to Scutari by traveling seven hundred leagues around the perimeter of the Black Sea so that he won’t have to pay the paltry 10 paras tax. Kéraban, this man of principle, and his reluctant traveling companions begin the journey; the only deadline for Kéraban is that he must be back in 6 weeks time so that he may depart in time to arrange for his nephew’s wedding to a young woman who must be married before she turns seventeen. If she doesn’t meet that deadline, she won’t inherit 100,000 Turkish pounds. Unfortunately for Kéraban and friends the villains Yarhud, Scarpante and the man they work for, Seigneur Saffar, have plans to ensure that the young woman gets married to Saffar before the deadline.

==Publication history==
*1883-1884 USA: New York, G. Munro, published as 'The Headstrong Turk''
*1887, UK, London: Sampson Low, Marston, Searle, & Rivington, 1887. First UK edition. Published in two volumes: ''The Captain of the Guidara'' and ''Scarpante the Spy''

==External links==
* {{gutenberg|no=8174|name=Kéraban-le-têtu, vol.I}} {{fr}}
* {{gutenberg|no=8175|name=Kéraban-le-têtu, vol.II}} {{fr}}
* [http://epguides.com/djk/JulesVerne/works.shtml epguides.com]
* [http://www.julesverne.ca/vernebooks/jvbkkeraban.html julesverne.ca]
{{commonscatinline}}

{{Verne}}

{{Authority control}}

{{DEFAULTSORT:Keraban The Inflexible}}
[[Category:1883 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in Turkey]]


{{1880s-novel-stub}}