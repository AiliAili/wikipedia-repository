{{Insolvency}}
[[Bankruptcy]] is a legally declared inability or impairment of ability of an individual or organization to pay their [[creditor]]s. In most cases [[personal bankruptcy]] is initiated by the bankrupt individual.  Bankruptcy is a legal process that discharges most debts, but has the disadvantage of making it more difficult for an individual to borrow in the future.  To avoid the negative impacts of personal bankruptcy, individuals in debt have a number of '''bankruptcy alternatives'''.

== Take no action ==

Bankruptcy prevents a person's creditors from obtaining a [[Legal judgment|judgment]] against them.  With a judgment a [[creditor]] can attempt to [[garnishment|garnish]] wages or seize certain types of property.  However, if a [[debtor]] has no wages (because they are [[Unemployment|unemployed]] or [[Retirement|retired]]) and has no property, they are "[[judgment proof]]", meaning a judgment would have no impact on their financial situation.  Creditors typically do not initiate legal action against a debtor with no assets, because it is unlikely they could collect the judgment.

If enough time passes, seven years in most jurisdictions, the debt is removed from the debtor's [[credit history]].

A [[debtor]] with no assets or income cannot be garnished by a [[creditor]], and therefore the "Take No Action" approach may be the correct option, particularly if the debtor does not expect to have a steady income or property a creditor could attempt to seize.

==Self money management==

[[Debt]] is a result of spending more than one's income in a given period.  To reduce debt, the most obvious solution is to reduce monthly spending to allow extra cash flow to service debt.  This can be done by creating a [[personal budget]] and analyzing expenses to find areas to reduce expenses.

Most people, when reviewing a written list of their monthly expenses, can find ways to reduce expenses.  Common areas for expense reduction would include reducing food expenses by eating out less often, taking public transportation instead of driving a car, and eliminating enhanced telephone and cable television services.

== Negotiate with creditors ==

Creditors understand that [[bankruptcy]] is an option for debtors with excessive debt, so most creditors are willing to negotiate a settlement so that they receive a portion of their money, instead of risking losing everything in a bankruptcy.

Negotiation is a viable alternative if the debtor has sufficient income, or has assets that can be liquidated so that the proceeds can be applied against the debt.  Negotiation may also buy the debtor some time to rebuild their finances.

For a business, a restructuring agreed with the creditors is a common approach but this requires the agreement of all creditors.<ref name="EkvallSmiley2007">{{citation|author1=Lei Lei Ekvall|author2=Evan Smiley|title=Bankruptcy for Businesses: Benefits, Pitfalls and Alternatives|url=https://books.google.com/books?id=74KzPHVvcc8C|date=17 April 2007|publisher=Entrepreneur Press|isbn=978-1-59918-096-0}}</ref>

== Debt restructuring ==
[[Debt restructuring]] is a process that allows a private or public company - or a sovereign entity - facing cash flow problems and [[financial distress]], to reduce and renegotiate its delinquent debts in order to improve or restore liquidity and rehabilitate so that it can continue its operations.

Out-of court restructurings, also known as workouts, are increasingly becoming a global reality. A debt restructuring is usually less expensive and a preferable alternative to bankruptcy. The main costs associated with a business debt restructuring are the time and effort to negotiate with bankers, creditors, vendors and tax authorities. Debt restructurings typically involve a reduction of debt and an extension of payment terms.

== Debt consolidation ==

Debt is a problem if the interest payments are greater than the debtor can afford. [[Debt consolidation]] typically involves borrowing from one lender (typically a bank), at a low rate of interest, sufficient funds to repay a number of higher interest rate debts (such as [[credit cards]]).  By consolidating debts, the debtor replaces many payments to many different creditors with one monthly payment to one creditor, thereby simplifying their monthly budget. In addition, the lower interest rate means that more of the debtor's monthly payment is applied against the principal of the loan, resulting in faster debt repayment.  It may be necessary to have a co-signor or other security, such as a car, if the borrower's credit is not sufficient on their own.

== Formal proposal to creditors ==

If the debtor cannot deal with their debt problems through personal budgeting, negotiation with creditors, or debt consolidation, the final bankruptcy alternative is a formal proposal or deal with the creditors.

Different countries have different legal procedures for compromising debts.  In the United States, a debtor can file a [[Chapter 13]] Wager Earner Plan.  The plan will typically last for up to five years, during which time the debtor makes payments that are distributed to their creditors.

In Canada, a Consumer Proposal can be filed with the assistance of a government-licensed proposal administrator.  Forty-five days after filing the proposal the creditors vote on the proposal, which is considered accepted if more than half of the creditors, by dollar value, vote to approve the proposal.

== Individual Voluntary Arrangement ==

In the UK the [[individual voluntary arrangement]] (IVA) represents the main formal alternative to a debtors bankruptcy petition. The IVA is part of the [[Insolvency Act 1986]] and essentially allows a debtor to reach a formal repayment arrangement with their creditors usually over a 5-year period. In most cases the debtor does not repay their debts in full to their creditors however the IVA proposal essentially allows for any remaining debt to be written off by the creditors at the end of the 5 year repayment period. As with bankruptcy petitions the number of IVA proposals has been increasing rapidly in the UK in recent years.

==References==
{{Reflist}}

==External links==
* [https://web.archive.org/web/20081217025425/http://www.uscourts.gov/bankruptcycourts.html U.S. Federal Bankruptcy Courts]
* [http://www.usdoj.gov/ust/index.htm Executive Office for United States Bankruptcy Trustees]
* [http://www.law.cornell.edu/topics/bankruptcy.html Cornell Bankruptcy Laws]
* [http://nacba.org  National Association of Consumer Bankruptcy Attorneys]
* [https://web.archive.org/web/20090429115235/http://www.insolvency.gov.uk/ Website of the Insolvency Service in the UK]
* [http://www.oro.gov.hk/cgi-bin/oro/stat.cgi Bankruptcy Statistics in Hong Kong]

[[Category:Bankruptcy]]

[[bg:Банкрут]]
[[cs:Úpadek]]
[[da:Konkurs]]
[[de:Bankrott]]