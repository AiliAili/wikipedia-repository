{{Infobox person
| name                = Pura Lopez 
| image                 = PURA_LOPEZ.jpg
| birth_name          = Pura Lopez Segarra
| father            = Antonio López Moreno
| mother            = Josefina Segarra García
| birth_date          = {{Birth date and age|1962|9|29}}
| birth_place         = [[Elche]], [[Spain]]
| occupation          = Shoe designer
| years_active        = 1986-present
| website             = [http://www.puralopez.com/ Official website]
}}

'''Pura Lopez''' (born September 29, 1962, in [[Elche]], [[Spain]]) is a [[Spain|Spanish]] shoe designer, director of the design department of the Pura Lopez brand.<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1559872_5539612_5563983_232715_0,00.htm |title=Pura López (Fashion from Spain) |archivedate=January 25, 2013 |deadurl=unfit |archiveurl=http://www.webcitation.org/6DviZ7GSx }}</ref>
She is a shoe designer for Royal Family members and Spanish cinema.<ref>{{cite web|url=http://www.modamarcas.com/pura-lopez/ |title=Online Fashion magazine ModaMarcas |archivedate=January 25, 2013 |deadurl=unfit |archiveurl=http://www.webcitation.org/6DvidRN4L }}</ref>

== Early life ==
Pura Lopez was born in Elche, [[Alicante]], [[Spain]], the daughter of Antonio Lopez Moreno, a shoe manufacturer who founded his Company as a family business in 1964 during the rapid growth of the shoe industry in [Spain].<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1549487_5857768_5852720_233085_0,00.html|title=Fashion from Spain, Pura Lopez dossier}}</ref>

Lopez was growing surrounded by shoes and everything related to shoe production. Her observations and attention to the techniques gave her deep knowledge of the shoe construction and design concepts. Later, in the interview for the Spanish Magazine ''[[Surrealista]]'',<ref>{{cite web|url=http://www.revistasurrealista.es|title=''Surrealista'' fashion magazine}}</ref> Lopez admitted that she was always attracted by art and creative process but she never really thought to work neither in the shoe industry nor in [[fashion]].<ref>{{cite web|url=http://www.revistasurrealista.es/index.php?s=215&a=432|title=Interview with Pura Lopez, Surrealista magazine, issue nr. 55}}</ref>
 
In 1979 Lopez entered the School of Applied Arts and Creative Careers of Alicante to study the Interior Design.<ref>{{cite web|url=http://www.modaes.es/perfiles/pura-lopez-es.html|title=Dossier Pura Lopez on Fashion Spain}}</ref>

In 1981 she proceeds with her education entering the School of Arts and Design of Barreira<ref>{{cite web|url=http://barreira.edu.es|title=School of Arts and Design of Barreira official site}}</ref> in Valencia, where she studies Fashion Design.

In the summer of 1984 Lopez went to [[New York City]], where she started attending a specialized course on Clothing Fashion Design in the [[Fashion Institute of Technology]].<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1559872_5539612_5563983_232715_0,00.html|title=Fashion From Spain}}</ref>
A year later she went to [[Milan]], to study footwear design and construction in [[ARSUTORIA School]].<ref>{{cite web|url = http://www.arssutoria.com|title = ARSUTORIA School official website|website = ARSUTORIA School {{!}} The university for shoes and bags design}}</ref>
That became a significant period in Lopez’s life. She stayed in every-day contact with the fashion world, often attended fashion shows and [[pret-a-porter]] runways to learn as much as possible about the trends and fashion world.

In 1986 Lopez joined her family business. She started working in the Department of Design, Development and Production for two significant collections: "Josephine" and "Academia".<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1549487_5857812_5857549_303383_0,00.html|title=Pura Lopez, a tribute to femminility, article on Fashion From Spain}}</ref> Soon after her arrival the Lopez family changed their strategy and started a new ladies shoe brand.

In 1987 the concept of Pura Lopez brand was developed. The main figure is a woman of wide culture, fashion-oriented, very feminine whilst strong.<ref>{{cite web|url=http://zapatosmarca.net/pura-lopez/|title=Pura Lopez style - article on Zapatosmarca.net}}</ref>

After a family council, the new brand was named after its creator. Currently the second generation of Lopez family conducts all the affairs of the Company where Lopez is in charge of the Design Department and her brother, Antonio Lopez Segarra is CEO.<ref>{{cite web|url=https://www.puralopez.com/en/company|title=About the company}}</ref>

The distinctive particularity of Pura Lopez shoes is the color spectrum of pastels, which vary depending on the season.<ref>{{cite web|url=http://bagsaaa.footblog.fr/840021/Pura-Lopez-shoes-offers-pure-pleasure/|title=Pura Lopez shoes offers pure pleasure, article in footblog}}</ref> Thanks to accurate calculations of the form construction Lopez manages to create the models with high heels and platforms without compromising the stability.<ref>{{cite web|url=http://fashionvictimsbcn.blogspot.it/2012/06/pura-lopez-sandals.html|title=Fashion Victims - fashion blog}}</ref>
Lopez chose her moto: "Your shoes should inspire you to walk" and her concept: "a shoe is a main accessory in the woman’s wardrobe" considering it to be an indicator between the platitude and elegance. According to Lopez "The shoes are communication."<ref>{{cite web|url=http://olivemds.wordpress.com/2012/04/25/shoes-in-spain-are-anything-but-plain/|title=Shoes in Spain are anything but Plain}}</ref>

Today the Pura Lopez brand is the basis of family business Dalph International founded in 1956.<ref>{{cite web|url=http://www.informacion-empresas.com/Empresa_DALP-INTERNACIONAL.html|title=Catalogue of Spanish Enterprises}}</ref>

== Recognition ==
Admission to the runway became Lopez's big victory. Her models are being used for shows and catwalks by such designers as Cibeles y Gaudí, Juanjo Oliva, Andrés Sardá, Javier Larraínzar and Guillermina Baeza.<ref>{{cite web|url=http://www.embelezzia.com/moda/pura-lopez-zapatos-puro-lujo|title=Pura Lopez-pure luxury shoes, article in Embelezzia magazine}}</ref>

Moreover, she collaborates with such houses as La Perla, Devota & Lomba, Joaquim Verdu, Roberto Verino, Roberto Torretta and participates in [[Madrid]] Fashion Week.

In 1998 Pura Lopez represented the Spanish shoe industry in [[Expo '98|1998 Lisbon World Exposition]] in [[Lisbon]], [[Portugal]].

Lopez works with [[Haute couture]], creating special occasion shoes for the members of the European Monarchies such as Mary Donaldson, [[Mary, Crown Princess of Denmark, Countess of Monpezat|Crown Princess of Denmark]] and [[Letizia, Princess of Asturias|Crown Princess Letizia of Spain]], who prefers to wear Pura Lopez shoes in different occasions of celebrations.<ref>{{cite web|url=http://www.ideasparatuboda.es/2012/03/zapatos-pura-lopez/|title=Pura Lopez shoes on Ideasparatuboda.es}}</ref>

Howbeit the most significant moment in Lopez's career was May 22, 2004  - the wedding day of [[Letizia, Princess of Asturias|Letizia Ortiz Rocasolano]] and [[Felipe, Prince of Asturias|Prince Philipe]], when the bride was wearing the dress of Spanish designer [[Manuel Pertegas]] and Pura Lopez shoes.<ref>{{cite web|url=http://mujer.hispavista.com/noticias/e-20040521162631/la-disenadora-pura-lopez-senala-que-los-zapatos|title=Spanish shoe designer Pura Lopez proclaims the wedding shoes for Letizia to be her greatest creation}}</ref>
Other guests followed the bride's lead and wore Pura Lopez shoes as well: [[Infanta Cristina of Spain, Duchess of Palma de Mallorca]] and her cousin Alessandra of Greece.<ref>{{cite web|url=http://personales.ya.com/fororeal/vestido.htm|title=Prince Philippe-s wedding to Princess Letizia}}</ref>

The wedding was watched by more than 25 million television viewers in Spain alone, and was broadcast throughout the world. After that Lopez was adored and requested as wedding shoes designer.<ref>{{cite web|url=http://top1212.blogspot.it/2012/01/pura-lopez-shoes-best-choice-for-your.html|title=Pura Lopez, the best choice for your wedding}}</ref>

In 2009 Lopez received Fuenso International Shoe Award.<ref>{{cite web|url=http://www.centromodaonline.com/pura-l-pez-la-zapatera-prodigiosa|title=Interview with Pura Lopez, prodigy shoemaker}}</ref>

On March 30, 2012 the Association of businessmen, professionals and managers recognized Lopez as the best businesswoman of the year.<ref>{{cite web|url= http://www.diarioinformacion.com/alicante/2012/03/08/pura-lopez-elegida-empresaria-2012-aepa/1231564.html|title=Pura Lopez, businesswoman of 2012}}</ref>

At the moment{{when|date=April 2013}} Pura Lopez is present in more than 1000 places worldwide.<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1549487_5857712_5857556_329890,00.html|title=Article about Pura Lopez on fashion from Spain}}</ref> Those are fashion boutiques, monobrand store and department stores of the whole world.<ref>{{cite web|url = http://coolturemag.com/index.php?option=com_content&view=article&id=709&Itemid=2| title = "Couture" magazine: Pura Lopez opens her first boutique in Rome}}</ref>
Pura Lopez has its own stores in Piazze de Spagna in [[Rome's|Rome]] Fashion district,<ref>{{cite web
 | url         = http://es.paperblog.com/pura-lopez-roma-800428/
 | title       = Pura Lopez in Rome}}</ref> in [[Galeries Lafayette]] on [[Boulevard Haussmann]] in [[Paris]], [[France]] and Galleries Lafayette in Casablanca, [[Morocco]].

The average retail price of Pura Lopez shoes is about 250—350 euro.

In February 2012 Pura Lopez opens their first show room in the center of [[Milan]].<ref>{{cite web|url=http://www.sportswearnet.com/businessnews/pages/protected/Pura-Lpez-showroom-opens-in-Milan_5785.html|title="Sportwear International" magazine: Show room Pura Lopez open in Milan}}</ref>

== Shoes ==
Lopez creates two collections a year that embrace several fashion concepts. As a result, each collection goes up to 200 styles per season.<ref>{{cite web|url=http://www.realeventos.tv/canales-para-bodas-eventos-empresa/moda-complementos-tiendas-ropa/pura-lopez/imagenes-fiesta.html|title=RealEventos.tv}}</ref>

That large amount is due to the fact that Lopez always has in mind all the types of women that she is designing for. She considers the bone structure, the height and time of the day. First she creates a group of High Fashion - Runway shoes. Then the same process is repeated for classic evergreen style; then for chic and elegant style; then for conservative target etc.
But not all the models are then exposed. After the development Lopez chooses only the most interesting and innovative ones and sends them to the production.

Pura Lopez shoes are produced in eight separate factories that belong to the Company in Elche, [[Spain]]. Only Italian leather is used for the shoes, as well as modern and antique technologies of shoe-making.<ref>{{cite web|url=http://zapatos.hispamoda.com/379/pura-lopez-botas-para-este-otono-invierno|title=Article about Pura Lopez on HispaModa.com}}</ref>
The designer uses a lot of texture and "crust" nappa and is not afraid to apply a "vintage effect", i.e. artificial leather aging that makes the product more attractive when it is being used a lot.

The production capacity of the company is divided into five lines up to 1800 pairs a week each.

From 2009 Lopez began to expand the bag line and to introduce them to the main collections.<ref>{{cite web|url=http://blog.es.privalia.com/pura-lopez-pura-tendencia.html|title="Pura Lopez - a pure trend", article on 09.10.2009}}</ref>

== Cinema ==
The designs of Pura Lopez models were used in such movies as ''[[La Dama Boba (film)|La Dama Boba]]'' of Manolo Iborra Martinez based on the play by [[Lope de Vega]], ''Insomnia'' by Chuz Guttieres, ''[[Kika (1993 film)|Kika]]'' by [[Pedro Almodóvar|Pedro Almodóvar Caballero]], as well as in the French production of ''Asterix and Obelix''.<ref>{{cite web|url=http://www.ofertasropa.com/2011/09/pura-lopez-elegancia-a-buen-precio/|title=Article about Pura Lopez on Ofertasropa.com}}</ref> <!--all the films are nominated in the paragraph: Otras películas que han optado por encargar unos zapatos de Pura López son Insomnio de Chuz Gutierrez, Kika de Pedro Almodóvar, La Dama Boba de Manolo Iborra e incluso la producción francesa Asterix y Obelix-->

But in all their glory the shoes appeared in the movie ''[[Manolete (film)|Manolete]]'' with [[Adrien Brody]].<ref>{{cite web|url=http://allfashion4u.blogspot.it/2007/01/ole-ole-y-ole-manolete.html|title="Ole!Ole!Ole!Manolete" - article on AllFashion4U}}</ref> Lopez was invited to create the design of the shoes worn by [[Penélope Cruz]].<ref>{{cite web|url=http://www.trendencias.com/noticias-de-la-industria/pura-lopez-calza-a-penelope-cruz-para-manolete|title=Pura Lopez puts the shoes on Penelope Cruz in "Manolete"}}</ref> The styles chosen for the movie reflect all the femininity and glamour of the 40's women — the time when platforms, round pumps and high heels were especially relevant.<ref>{{cite web|url=http://www.fashionfromspain.com/icex/cda/controller/pageGen/0,3346,1549487_5857712_5857493_514228_-1,00.html?pageIdPrint=5857556&tipoVista=Contenidos&esPopup=true|title=Pura Lopez shoes for Manolete}}</ref>

Lopez admitted that she liked her experience in the movie industry because it gave her a great possibility to express her creative potential without respecting any limits.

Pura Lopez shoes are worn by such celebrities as: [[Kylie Minogue]], [[Monica (singer)|Monica]], [[Penélope Cruz]], [[Isabelle Fuhrman]],<ref>{{cite web|url=http://coolspotters.com/shoes/pura-lopez-wedge-ankle-boots/spots|title=Celebrities that wear Pura Lopez}}</ref> [[Lea Michele]],<ref>{{cite web|url=http://coolspotters.com/actresses/lea-michele/and/brands/pura-lopez#medium-1911710|title=Lea Michele and Pura Lopez}}</ref> [[Kim Kardashian]],<ref>{{cite web|url=http://shoes.stylosophy.it/articolo/celebrities-kim-kardashian-con-decolletes-pura-lopez/4815/|title=Celebrities: Kim Kardashian con decolletes Pura Lopez}}</ref> and others.

== Interesting facts ==
* Lopez does not like to repeat the models so she never uses the same design without changing it at least a little bit in two collection in a row. That productivity requires a lot of time. Lopez says that she and her designers team could often stay in the studio for days choosing the colors and materials that match.
* Once Lopez decided to create an exclusive design just for her. But in the end she couldn't resist and included it in the main collection. 
* The Spanish princess Letizia who is constantly called «Princess of Fashion»,<ref>{{cite web|url=http://www.infozapatos.com/letizia-ortiz-la-princesa-de-la-moda|title=Letizia ortiz, princess of fashion}}</ref> she wears Pura Lopez shoes.<ref>{{cite web|url=http://www.modacoqueta.es/moda/tag/pura-lopez/|title="Fashion and Beautyа" magazine, Princess turns 39}}</ref>
* Lopez doesn't like to give interviews. She can easily be seen in the Fairs and Presentations where she comes incognito.

==External links==
* [http://www.puralopez.com/ Official website]

==References==
{{reflist}}

{{high heels}}

{{DEFAULTSORT:Lopez, Pura}}
[[Category:1962 births]]
[[Category:Living people]]
[[Category:Spanish fashion designers]]
[[Category:Shoe designers]]
[[Category:Shoe companies of Spain]]
[[Category:People from Elche]]