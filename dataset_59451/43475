<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=S-7 Courier
 | image=Rans-S7.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Kit aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Rans Inc]]
 | designer=Randy Schlitter
 | first flight=November 1985
 | introduced=
 | retired=
 | status=In production (2017)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=600 (2011)
 | program cost= <!--Total program cost-->
 | unit cost= [[United States dollar|US$]]25,500 (base price 2010) S model, plus engine and instruments<ref name="Price">{{Cite web|url = http://www.rans.com/_KITS/ModelsPages/S-7SoptionsPricing.htm|title = S-7 Courier Options and Pricing|accessdate = 21 November 2010|last = Rans|authorlink = |year = n.d.}}</ref>
 | developed from= [[Rans S-5 Coyote]]<br />[[Rans S-4 Coyote]]
 | variants with their own articles=
}}
|}
[[File:RANSS7Floats.jpg|thumb|S-7 on floats]]
The '''Rans S-7 Courier''' is an [[United States|American]] single-engined, [[tractor configuration]], two-seats in [[tandem]], high-wing [[monoplane]] designed by Randy Schlitter and manufactured by [[Rans Inc]]. The Courier is available in [[Homebuilt aircraft|kit form]] for amateur construction or as a completed [[light-sport aircraft]].<ref name="KitplanesDec1998">Downey, Julia: ''1999 Kit Aircraft Directory'', Kitplanes, Volume 15, Number 12, December 1998, page 65. Primedia Publications. ISSN 0891-1851</ref><ref name="KitplanesDec2007">Downey, Julia: ''2008 Kit Aircraft Directory'', Kitplanes, Volume 24, Number 12, December 2007, page 69. Primedia Publications. ISSN 0891-1851</ref><ref name="Courier">{{Cite web|url = http://www.rans.com/_KITS/ModelsPages/S-7S.htm|title = S-7S Courier|accessdate = 22 November 2010|last = Rans|authorlink = |year = n.d.}}</ref><ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook'', page 239. BAI Communications. ISBN 0-9636409-4-1</ref><ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 157. Pagefast Ltd, Lancaster OK, 2003. ISSN 1368-485X</ref><ref name="JAWA8687">Taylor, John (ed): ''Jane's All The World's Aircraft 1986-1987'', pages 701-702. Jane's Publishing Company, 1986. ISBN 0-7106-0835-7</ref><ref name="JAWA8788">Taylor, John (ed): ''Jane's All The World's Aircraft 1987-1988'', pages 698-699. Jane's Publishing Company, 1987. ISBN 0-7106-0850-0</ref><ref name="JAWA8889">Taylor, John (ed): ''Jane's All The World's Aircraft 1988-1989'', pages 592-594. Jane's Publishing Company, 1988. ISBN 0-7106-0867-5</ref><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2011 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 68. Belvoir Publications. ISSN 0891-1851</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 72. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

==Design and development==
The S-7 was originally conceived of as a trainer for the single seat [[Rans S-4 Coyote|S-4 Coyote]]. First flown in November 1985 the Courier was named for an aircraft that Schlitter admired, the [[Helio Courier]].<ref name="Courier" /><ref name="Aerocrafter" /><ref name="JAWA8687" /><ref name="JAWA8889" /><ref name="WDLA11" />

The S-7 features a welded [[4130 steel]] tube cockpit, with a bolted [[aluminum]] tube rear fuselage, wing and tail surfaces all covered in  [[Aircraft fabric covering|dope and fabric]]. The reported construction times for the Courier are 500-700 man-hours.<ref name="KitplanesDec1998" /><ref name="KitplanesDec2007" /><ref name="Aerocrafter" />

The Courier is available only with [[conventional landing gear]] but can be equipped with [[Floatplane|floats]] and [[ski]]s. The original basic engine was the [[Rotax 503]] of {{convert|50|hp|kW|0|abbr=on}}, with the [[Rotax 582]] of {{convert|64|hp|kW|0|abbr=on}} being available as an option. Today the standard engine is the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]].<ref name="KitplanesDec1998" /><ref name="KitplanesDec2007" /><ref name="Courier" /><ref name="Aerocrafter" />  At least one S7 has been fitted with a [[Jabiru 2200]] flat-four, four-stroke direct-drive engine.<ref>[[Civil Aviation Authority (United Kingdom)|Civil Aviation Authority]], [http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detail&aircrafttype=Rans_S7&dataindex=5 GINFO Search Results], retrieved 1 October 2013</ref>

==Operational history==
325 examples of the Courier had been completed by December 2007.<ref name="KitplanesDec2007" />  In November 2010 74 were on the registers of European countries west of Russia.<ref name="Ereg">{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= Coulsdon, Surrey|isbn= 978-0-7106-2916-6 |pages=}}</ref>

Reviewer Marino Boric said in a 2015 review, that, "this refined little fun flyer...continues to prove itself deservedly popular."<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 76. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Variants==
[[File:Rans S-7 Courier ZK-OAC, Taieri Aerodrome, NZ.jpg|thumb|right|S-7 Courier]]
;S-7
:Initial version, standard engine {{convert|50|hp|kW|0|abbr=on}} [[Rotax 503]], {{convert|64|hp|kW|0|abbr=on}} [[Rotax 582]] engine optional.<ref name="Courier"/>
[[File:RANS S-7C Courier N2506A 01.JPG|thumb|right|Rans S-7C Courier]]
;S-7C
:Refined version introduced in 2001, certified under the US primary category. Certification in the category took seven years of effort by the manufacturer.<ref name="Courier" /><ref name="WDLA11" /><ref name="WDLA15"/>
;S-7S
:Kit version of the S-7C, introduced in 2003. Qualifies as a US Experimental [[light-sport aircraft]] (ELSA). Standard engine is the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]].<ref name="Courier" /><ref name="WDLA04" /><ref name="WDLA11"/><ref name="WDLA15"/>
;S-7LS
:Sold as a factory-assembled ready-to-fly US Special [[light-sport aircraft]], the S-7LS is a factory-assembled version of the S-7S. Standard engine is the {{convert|100|hp|kW|0|abbr=on}} [[Rotax 912ULS]].<ref name="Courier" /><ref name="WDLA11"/><ref name="CourierFactoryBuiltOverview">{{Cite web|url = http://www.rans.com/_RTF/S-7LSmain.htm|title = S-7LS Coyote II|accessdate = 30 November 2010|last = Rans|authorlink = |year = n.d.}}</ref>

==Specifications (S-7S)==
[[File:RANS S-7C Courier Primary category N2506A 03 instrument panel.jpg|thumb|right|S-7C instrument panel]]
[[File:Rans S-7 Courier with damaged propeller at Brioude aerodrome.jpg|right|thumb|An S-7 at Brioude in France]]
{{Aircraft specs
|ref=Kitplanes<ref name="KitplanesDec1998" /><ref name="KitplanesDec2007" />
|prime units?=imp
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=One passenger
|length m=
|length ft=23
|length in=3
|length note=
|span m=
|span ft=29
|span in=3
|span note=
|height m=
|height ft=6
|height in=4
|height note=
|wing area sqm=
|wing area sqft=147.1
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=700
|empty weight note=
|gross weight kg=
|gross weight lb=1232
|gross weight note=
|fuel capacity=18 US Gallons (68 litres)
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912ULS]]
|eng1 type=
|eng1 kw=
|eng1 hp=100
|eng1 shp=

|prop blade number=2
|prop name=adjustable pitch
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=118
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=41
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=390
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=1000
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=8.37
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
|avionics=
}}
==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
* [[Bellanca Citabria]]
* [[Cessna 162]]
* [[Denney Kitfox]]
* [[Fisher Dakota Hawk]]
* [[Fisher Horizon]]
* [[Murphy Maverick]]
* [[Murphy Rebel]]
* [[Piper PA-18]]
|sequence=
|lists=
}}

==References==
{{reflist|30em}}

==External links==
{{commons category|RANS S-7 Courier}}
*{{Official website|https://www.rans.com/s-7s-courier}}

{{Rans aircraft}}

[[Category:United States civil utility aircraft 1980–1989]]
[[Category:Homebuilt aircraft]]
[[Category:Light-sport aircraft]]
[[Category:Rans aircraft|S-007 Courier]]