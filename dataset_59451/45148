{{unreliable sources|date=November 2016}}{{Infobox military person
|name=Horst Ademeit
|birth_date={{birth date|1912|2|8|df=y}}
|death_date={{death date and age|1944|8|7|1912|2|8|df=y}}
|birth_place=[[Breslau]]
|death_place=near [[Daugavpils|Dünaburg]]
|image=File:Horst Ademeit.jpg
|caption=
|nickname=
|allegiance={{flag|Nazi Germany}}
|serviceyears=1936–44
|rank=[[Major (Germany)|Major]]
|branch={{Luftwaffe}}
|commands=I./[[JG 54]]
|unit=[[JG 54]]
|battles=[[World War II]]
*[[Battle of Britain]]
*[[Operation Barbarossa]]
*[[Eastern Front (World War II)|Eastern Front]]{{KIA}}
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]]
|laterwork=}}

'''Horst Ademeit (Adomaitis)<ref>[http://www.antraspasaulinis.net/e107_plugins/content/content.php?content.939 Turinys / Ginkluotosios pajėgos / Lietuviai Vermachte - Antrasis pasaulinis karas<!-- Bot generated title -->]</ref>''' (8 February 1912 – 7 August 1944) was a Lithuanian descent [[Germans|German]] former [[Luftwaffe]] [[fighter ace]] and recipient of the [[Knight's Cross of the Iron Cross with Oak Leaves]] ({{lang-de|Ritterkreuz des Eisernen Kreuzes mit Eichenlaub}}) during [[World War II]]. The Knight's Cross of the Iron Cross and its higher grade Oak Leaves was awarded to recognise extreme battlefield bravery or successful military leadership. A flying ace or fighter ace is a [[military aviation|military aviator]] credited with shooting down five or more enemy [[aircraft]] during aerial combat.<ref>Spick 1996, pp. 3–4.</ref>

==Early life==
Ademeit, the son of a ''Regierungsbaurat'' (government building officer), was born on 8 February 1912 in [[Breslau]] in the [[Kingdom of Prussia]] of the [[German Empire]], present-day Wrocław in western Poland. He studied at the [[Königsberg Albertina University]], a member of the [[Corps Masovia Königsberg (Potsdam)|Corps Masovia Königsberg]]. He then studied [[chemistry]] at the [[Technical University of Berlin]] and the [[Technical University of Braunschweig]] graduating as ''[[Diplom|Diplom Ingenieur]]''. He joined the military service of the ''[[Luftwaffe]]'' on 1 August 1936.<ref name="Stockert p29">Stockert 2007, p. 29.</ref>

On 9 December 1938, Ademeit was made an [[officer cadet]] of the [[Military reserve force|reserves]] and received flight training.<ref name="Stockert p29"/>

==World War II==
In the spring of 1940, Unteroffizier Ademeit was transferred to 3./[[Jagdgeschwader 54]] (JG 54—54th Fighter Wing) and participated in the [[Battle of Britain]].<ref group="Note">For an explanation of the meaning of Luftwaffe unit designation see [[Luftwaffe Organization]]</ref> He claimed his first victory on 18 September 1940 shortly afterwards he was shot down over the Channel. He bailed out and was rescued by the ''[[Seenotdienst]]'' unharmed.

In June 1941, after the attack on the [[Soviet Union]], he accompanied I./JG 54 to the [[Eastern Front (World War II)|Eastern Front]]. In quick succession he achieved aerial victories, promotions and awards. On 15 January 1944, Ademeit was credited with his 100th aerial victory. He was the 61st ''Luftwaffe'' pilot to achieve the century mark.<ref>Obermaier 1989, p. 243.</ref> In the beginning of August 1944, Ademeit was appointed acting ''[[Geschwaderkommodore]]'' (wing commander) of JG 54.

On 7 August 1944, Ademeit, flying a [[Focke Wulf Fw 190|Focke Wulf Fw 190 A-5]] (''Werksnummer'' 5960 — factory number) pursued a Russian [[Ilyushin Il-2|Il-2 Sturmovik]] ground-attack aircraft eastwards over Russian lines near [[Daugavpils|Dünaburg]], however he failed to return from this mission and is considered [[Missing in action]] since.<ref name="Obermaier p60">Obermaier 1989, p. 60.</ref>

Horst Ademeit was credited with 166 victories in over 600 missions. He recorded 164 of his victories over the Eastern Front. He was posthumously promoted to ''[[Major (Germany)|Major]]''.<ref name="Obermaier p60"/>

==Awards==
* [[Iron Cross]] (1939)
**2nd Class (7 September 1940)<ref name="Thomas p2">Thomas 1997, p. 2.</ref>
**1st Class (5 September 1941)<ref name="Thomas p2"/>
* [[Front Flying Clasp of the Luftwaffe]] for fighter pilots in Gold and Penant
* [[Ehrenpokal]] der Luftwaffe on 8 December 1941 as ''[[Leutnant]]'' and pilot<ref name="Obermaier p60"/><ref>Patzwall 2008, p. 40.</ref>
* [[German Cross]] in Gold on 25 February 1942 as ''Leutnant'' in the 1./JG 54<ref>Patzwall & Scherzer 2001, p. 11.</ref>
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 16 April 1943 as ''Leutnant'' (war officer) and pilot in the I./JG 54<ref name="Scherzer p188">Scherzer 2007, p. 188.</ref><ref>Fellgiebel 2000, p. 113.</ref>
** 414th Oak Leaves on 2 March 1944 as ''[[Hauptmann]]'' (war officer) and ''[[Gruppenkommandeur]]'' of the I./JG 54<ref name="Scherzer p188"/><ref>Fellgiebel 2000, p. 79.</ref>

==Notes==
{{Reflist|group="Note"}}

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{Refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
* {{Cite book
  |last=Spick
  |first=Mike
  |year=1996
  |title=Luftwaffe Fighter Aces
  |location=New York
  |publisher=[[Ivy Books]]
  |isbn=978-0-8041-1696-1
}}
* {{Cite book
  |last=Stockert
  |first=Peter
  |year=2007
  |title=Die Eichenlaubträger 1939–1945 Band 5
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 5
  |language=German
  |location=Bad Friedrichshall, Germany
  |publisher=Friedrichshaller Rundblick
  |oclc=76072662
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
}}
{{Refend}}

{{Knight's Cross recipients of JG 54}}
{{Top German World War II Aces}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Ademeit, Horst}}
[[Category:1912 births]]
[[Category:1944 deaths]]
[[Category:People from Wrocław]]
[[Category:People from the Province of Silesia]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:German military personnel killed in World War II]]
[[Category:Missing in action of World War II]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]
[[Category:University of Königsberg alumni]]