{{use mdy dates|date=October 2014}}
{{Infobox company
|name= Bell Helicopter
|logo= [[File:Bell Textron logo.svg]]
|type= [[Subsidiary]]<ref>{{cite web|url=http://www.textron.com/about/our-businesses/|title=About Textron: Our Businesses|date=October 21, 2015|publisher=}}</ref>
|foundation= 1935
|location_city= [[Fort Worth, Texas]]
|location_country= United States
|key_people= Mitch Snyder, President & CEO
|industry= [[Aerospace]]
|products= [[Helicopter]]s, [[tiltrotor]]s
|revenue= 
|operating_income=
|net_income= 
|num_employees= 
|parent= [[Textron]]
|subsid= 
|homepage= http://bellhelicopter.com
|footnotes= 
}}

'''Bell Helicopter''' is an [[United States|American]] [[aerospace manufacturer]] headquartered in [[Fort Worth, Texas]].  A division of [[Textron]], Bell manufactures military [[rotorcraft]] in and around Fort Worth, as well as in [[Amarillo, Texas]], and commercial [[helicopter]]s in [[Mirabel, Quebec]], [[Canada]].  Bell provides training and support services worldwide.

==History==
===Bell Aircraft===
The company was founded on July 10, 1935 as [[Bell Aircraft|Bell Aircraft Corporation]] by [[Lawrence Dale Bell]] in [[Buffalo, New York]].  The company focused on the designing and building of fighter aircraft.  Their first fighters were the [[Bell YFM-1 Airacuda|XFM-1 Airacuda]], a twin-engine fighter for attacking bombers, and the [[P-39 Airacobra]].  The [[P-59 Airacomet]], the first American jet fighter, the [[P-63 Kingcobra]], the successor to the P-39, and the [[Bell X-1]] were also Bell products.<ref name="Bell_hist">[http://www.bellhelicopter.com/en/company/history.cfm History of Bell Helicopter] {{webarchive |url=https://web.archive.org/web/20070603084523/http://www.bellhelicopter.com/en/company/history.cfm |date=June 3, 2007 }}. bellhelicopter.com</ref>

[[File:Bellhelicopter.MOMA.JPG|right|thumb||The [[Bell 47]] is displayed at the [[Museum of Modern Art|MoMA]]]]
In 1941, Bell hired [[Arthur M. Young]], a talented inventor, to provide expertise for helicopter research and development. It was the foundation for what Bell hoped would be a broader economic base for his company that was not dependent on government contracts. The [[Bell 30]] was their first full-size helicopter (first flight December 29, 1942) and the [[Bell 47]] became the first helicopter in the world rated by a civil aviation authority, becoming a civilian and military success.<ref name="Bell_hist" />

===Bell Helicopter===
[[Textron]] purchased Bell Aerospace in 1960. Bell Aerospace was composed of three divisions of Bell Aircraft Corporation, including its helicopter division, which had become its only division still producing complete aircraft. The helicopter division was renamed '''Bell Helicopter Company''' and in a few years, with the success of the [[UH-1 Iroquois|UH-1]] during the Vietnam War, it had established itself as the largest division of Textron. In January 1976, Textron changed the name of the company again to '''Bell Helicopter Textron'''.<ref>{{cite web|url=http://www.bellhelicopter.com/en/training/index.cfm?content=about/history.cfm&g_folder=header_4|title= Our History|publisher=Bell Training Academy}}</ref>

Bell Helicopter has a close association with [[AgustaWestland]]. The partnership dates back to separate manufacturing and technology agreements with [[Agusta]] ([[Bell 47]] and [[Bell 206]]) and as a sublicence via Agusta with [[Westland Aircraft|Westland]] ([[Bell 47]]).<ref>{{cite web|url=http://www.helis.com/timeline/westland4.php|title=Westland History - Part 4|publisher=}}</ref> When the two European firms merged, the partnerships were retained, with the exception of the AB139, which is now known as the [[AgustaWestland AW139|AW139]]. {{asof|2014}}, Bell and AW cooperate on the [[AW609]] [[tiltrotor]].<ref name=ojeh>Oliver Johnson & Elan Head. "[http://www.verticalmag.com/news/article/BellCEOoutlinesEuropeangrowthplan Bell CEO outlines European growth plan]" ''Vertical'', October 15, 2014. Accessed: October 21 ,2014.</ref>

Bell intends to reduce employment by 760 in 2014 as fewer [[Bell Boeing V-22 Osprey|V-22s]] are made.<ref name=ojeh/> A [[rapid prototyping]] center called XworX assists Bell's other divisions in improving development speed.<ref>{{cite web|url=http://www.ainonline.com/aviation-news/aviation-international-news/2009-12-29/bells-xworx-studying-improved-rotor-blades|title=Bell’s XworX studying improved rotor blades|work=Aviation International News}}</ref>

==Product list==
[[File:heli.g-code.750pix.jpg|thumb|Bell 206B JetRanger III]]
[[File:Navy-hh1n-158256-070327-09cr-10.jpg|thumb|Comparison of the Bell 212 (U.S. Navy [[UH-1N|HH-1N]]) and 412 (Mercy Air) at the Mojave Airport]]
[[File:Bell 412EP Griffin HT1 of the RAF at RIAT 2010 arp.jpg|thumb|[[Bell 412]]EP Griffin HT1 helicopter of the UK  [[Defence Helicopter Flying School]]]]

===Commercial helicopters===
* [[Bell 30]]
* [[Bell 47]]
** [[Bell 47J Ranger]]
* [[Bell 204/205|Bell 204]] – civilian version of UH-1
* [[Bell 204/205|Bell 205]] – civilian version of UH-1
* [[Bell 206]] – in production
* [[Bell UH-1 Iroquois|Bell 210]] – remanufactured, civilian version of UH-1H
* [[Bell 212]]
* [[Bell 214]]
* [[Bell 214ST]]
* [[Bell 222]]
* [[Bell 222|Bell 230]]
* [[Bell 400]]
* [[Bell D-292]]
* [[Bell 407]] – in production
* [[Bell 412]] – in production
* [[Bell 407|Bell 417]] – model canceled in 2007
* [[Bell 427]]
* [[Bell 429 GlobalRanger]] – in production
* [[Bell 430]]
* [[Bell 525 Relentless]] – under development
* [[Bell 505 Jet Ranger X]] – certified and in production.
* [[Bell FCX-001]] – concept, presented March 2017

===Military helicopters===
* [[Bell H-12]]
* [[Bell H-13 Sioux]]
** [[Bell 201|Bell XH-13F]]
* [[Bell XH-15]]
* [[Bell HSL]]
* [[Bell UH-1 Iroquois]] (or Huey)
** [[Bell Huey family]]
** [[Bell UH-1 Iroquois variants]]
* [[Bell UH-1N Twin Huey]]
* [[Bell YHO-4]]
* [[Bell 207 Sioux Scout]] - experimental attack helicopter
* [[Bell 533]] - experimental Huey variant with increased performance
* [[Bell AH-1 Cobra]]
* [[Bell AH-1 SuperCobra|Bell AH-1 SeaCobra/SuperCobra]]
* [[Bell 309 KingCobra]]
* [[Bell YAH-63|YAH-63/Model 409]] - competitor with the [[Boeing AH-64 Apache|YAH-64]] for [[Advanced Attack Helicopter]] program
* [[Bell OH-58 Kiowa]]
* [[H-1 upgrade program]]
** [[Bell UH-1Y Venom]]
** [[Bell AH-1Z Viper]]
* [[Bell CH-146 Griffon]]
* [[Bell ARH-70 Arapaho]]

===Tiltrotors===
[[File:CV-22 Osprey in flight.jpg|thumb|right|V-22 in flight]]

* [[Bell XV-3]]
* [[Bell XV-15]]
* [[Bell V-247 Vigilant]] - currently in development
* [[Bell V-280 Valor]] - currently in development, estimated first flight 2017
* [[V-22 Osprey]] - with [[Boeing Defense, Space & Security|Boeing BDS]]
* [[Bell Eagle Eye|TR918 Eagle Eye UAV]]
* [[Bell Boeing Quad TiltRotor|Quad TiltRotor]] - with Boeing BDS

===Projects produced by other companies===
* [[AgustaWestland AW139]] helicopter (formerly 50/50 as the Bell/Agusta AB139, now 100% [[AgustaWestland]])
* [[AgustaWestland AW609]] tiltrotor (formerly 50/50 as the Bell/Agusta BA609, now 100% [[AgustaWestland]])
* [[Lockheed Martin VH-71 Kestrel]]

==See also==
:{{portal|Aviation}}

==References==
{{Reflist}}

==External links==
{{commons category|Bell Helicopter Textron|Bell Helicopter}}
*{{official website|http://bellhelicopter.com}}
*[http://www.helis.com/timeline/bell.php Bell timeline at the Helicopter History Site]
*[http://www.educatedearth.net/video.php?id=5148 Video history of Bell Helicopter]
*{{cite web | title=Patents owned by Bell Helicopter Textron | work=US Patent and Trademark Office |url=http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&u=%2Fnetahtml%2Fsearch-adv.htm&r=0&p=1&f=S&l=50&Query=an%2F%22Bell+Helicopter+Textron%22&d=ptxt | accessdate=December 5, 2005}}

{{-}}
{{Bell Aircraft}}
{{Textron}}

[[Category:Helicopter manufacturers of the United States]]
[[Category:Defense companies of the United States]]
[[Category:Helicopter manufacturers of Canada]]
[[Category:Companies based in Fort Worth, Texas]]
[[Category:Textron]]
[[Category:Companies established in 1935]]
[[Category:1935 establishments in New York]]