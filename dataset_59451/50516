{{Infobox journal
| title = Annals of Clinical Biochemistry
| cover =
| editor = Edmund J. Lamb
| discipline = [[Clinical chemistry|Clinical biochemistry]]
| former_names = 
| abbreviation = Ann. Clin. Biochem.
| publisher = [[Sage Publications]] on behalf of [[The Association for Clinical Biochemistry and Laboratory Medicine]]
| country = United Kingdom
| frequency = Bimonthly
| history = 1960-present
| openaccess = 
| license = 
| impact = 1.922
| impact-year = 2012
| link1 = http://www.sagepub.com/journals/Journal202177/title
| link1-name = Journal website
| link2 = http://acb.sagepub.com/content/current
| link2-name = Online access
| link3 = http://acb.sagepub.com/content/by/year
| link3-name = Online archive
| ISSN = 0004-5632
| eISSN = 1758-1001
| OCLC = 848282046
}}
'''''Annals of Clinical Biochemistry''''' is a bimonthly [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[Clinical chemistry|clinical biochemistry]]. The [[editor-in-chief]] is Edmund J. Lamb ([[East Kent Hospitals University NHS Foundation Trust]]). It was established 1960 and is published by [[Sage Publications]] on behalf of [[The Association for Clinical Biochemistry and Laboratory Medicine]].

== Abstracting and indexing ==

The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Scopus]]
* [[Science Citation Index Expanded]]
* [[Academic Search Premier]]
* [[AIDS and Cancer Research Abstracts]]
* [[Analytical Abstracts]]
* [[Elsevier BIOBASE]]
* [[Biochemistry & Biophysics Citation Index]]
* [[Biotechnology & Bioengineering Abstracts]]
* [[Biotechnology Research Abstracts]]
* [[CINAHL]] Plus with Full Text
* [[Neurosciences Abstracts]]
* [[Current Contents]]/Life Sciences
* [[EMBASE]]
* [[Immunology Abstracts]]
* [[PubMed]]/[[MEDLINE]]
* [[ProQuest|ProQuest databases]]
}}
According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 1.922, ranking it 12th out of 31 journals in the category "Medical Laboratory Technology".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Medical Laboratory Technology |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |work=Web of Science |postscript=.}}</ref>

== References ==

{{reflist}}

== External links ==

* {{Official website|http://www.sagepub.com/journals/Journal202177/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Medicinal chemistry journals]]
[[Category:Publications established in 1960]]