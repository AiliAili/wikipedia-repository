<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=CP.60 Diamant
 | image=Piel C.P. 605 DIAMANT C-GUMM 01.JPG
 | caption=Piel C.P. 605 Diamant
}}{{Infobox Aircraft Type
 | type=civil utility aircraft
 | national origin=France
 | manufacturer=[[Homebuilt aircraft|Homebuilt]]
 | designer=[[Claude Piel]]
 | first flight=
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Piel CP.60 Diamant''' is a single-engine light aircraft designed in France in the 1960s and marketed for [[homebuilt aircraft|home building]].<ref name="JAE">Taylor 1989, p.725</ref>

==Design and development==
The Diamant is a conventional, low-wing cantilever monoplane and essentially an enlarged version of Piel's successful [[Piel Emeraude|Emeraude]] designed to accommodate more passengers.<ref name="JAE" /><ref name="JAWA77 495">''Jane's All the World's Aircraft 1977–78'', p.495</ref> This extra capacity is provided by a redesigned fuselage which is higher in the rear section and longer overall.<ref name="JAWA77">''Jane's All the World's Aircraft 1977–78'', p.495–96</ref> While the Emeraude has only two seats, side-by-side, the Diamant has an extra bench seat at the rear of the cabin to seat one or two more people.<ref name="JAWA77 495"/> The wingspan is also greater than that of the Emeraude, allowing for a greater maximum take-off weight.<ref name="JAWA77" /> The 110 page set of plans included modifications for retractable gear, tricycle gear, and wing mounted fuel tanks.<ref>{{cite journal|magazine=Air Progress Sport Aircraft|date=Winter 1969|page=74}}</ref>

As with the Emeraude, Piel obtained type certification for the Diamant, allowing it to be manufactured on a commercial basis.<ref name="JAWA77 495"/> However, unlike the Emeraude, this did not actually transpire, and the Diamant was only ever built as a homebuilt.<ref name="Simpson">Simpson 1995, p.283</ref>

By the late 1970s, the original versions of the Diamant were no longer offered and had been supplanted by the '''Super Diamant''', designed for more powerful engines and featuring revised tail surfaces.<ref name="JAWA77 495"/>

<!-- ==Development== -->
<!-- ==Operational history== -->

==Variants==
* '''CP.60 Diamant''' - version with 90&nbsp;hp Continental engine
* '''CP.601 Diamant''' - version with 100&nbsp;hp Continental engine
* '''CP.602 Diamant''' - version with 105&nbsp;hp Potez engine
* '''CP.603 Diamant''' - version with 115&nbsp;hp Lycoming engine
* '''CP.604 Super Diamant''' - version with 145&nbsp;hp Continental engine
* '''CP.605 Super Diamant''' - version with [[Lycoming O-320|Lycoming O-320-E2A]] engine
* '''CP.606 Super Diamant''' - version with 140&nbsp;hp Lycoming engine
* '''CP.607 Super Diamant''' - version with 130&nbsp;hp Continental engine
* '''CP.608 Super Diamant''' - version with 180&nbsp;hp Lycoming engine
* '''CP.615 Super Diamant''' - version with 160&nbsp;hp engine

<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (CP.605B) ==
{{aerospecs
|ref=<!-- reference -->''Jane's All the World's Aircraft 1977–78'', p.496
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=One pilot
|capacity=2-3 passengers
|length m=7.00
|length ft=23
|length in=0
|span m=9.20
|span ft=30
|span in=2
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=2.00
|height ft=6
|height in=7
|wing area sqm=13.3
|wing area sqft=143
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=520
|empty weight lb=1,146
|gross weight kg=850
|gross weight lb=1,873
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=[[Lycoming O-320|Lycoming O-320_E2A]]
|eng1 kw=<!-- prop engines -->112
|eng1 hp=<!-- prop engines -->150
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=260
|max speed mph=160
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=1,150
|range miles=710
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=5,000
|ceiling ft=16,400
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=5.5
|climb rate ftmin=1,080
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{Commons category|Piel CP 605 Diamant}}
* {{cite book |title=Jane's All the World's Aircraft 1977–78 |publisher=Jane's Publishing |location=London }}
* {{cite book |last= Simpson |first= R. W. |title=Airlife's General Aviation |year=1995 |publisher=Airlife Publishing |location=Shrewsbury |pages= }}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London }}
<!-- ==External links== -->

{{Piel aircraft}}

[[Category:French civil utility aircraft 1960–1969]]
[[Category:Piel aircraft]]
[[Category:Homebuilt aircraft]]