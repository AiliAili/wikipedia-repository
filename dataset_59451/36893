{{good article}}
{{Use dmy dates|date=April 2014}}
{{Use Australian English|date=April 2014}}
{{infobox country at games
| NPC = AUS
| NPCname = [[Australian Paralympic Committee]]
| games = Winter Paralympics
| year = 2014
| flagcaption = 
| oldcode = 
| website = {{url|www.paralympic.org.au }}
| location = [[Sochi]]
| competitors = 9
| sports = 1
| flagbearer = [[Cameron Rahles-Rahbula]] (Opening) [[Ben Tudhope]] (Closing)
| rank = 19
| gold = 0
| silver = 0
| bronze = 2
| officials = 15
| appearances = auto
| app_begin_year = 
| app_end_year = 
| summerappearances = 
| winterappearances = 
| seealso = 
}}
'''Australia''' sent nine competitors to the  '''[[2014 Winter Paralympics]]''' in [[Sochi]], Russia. The delegation also consisted of two [[sighted guide]]s and 15 support staff. The team won two bronze medals. [[Toby Kane]] won a bronze medal in the [[Alpine skiing at the 2014 Winter Paralympics – Men's combined|men's Super combined standing]], and [[Jessica Gallagher]] and guide [[Christian Geiger]] won one in the [[Alpine skiing at the 2014 Winter Paralympics – Women's giant slalom|women's giant slalom visually impaired]] event.

The Australian Team's performance was affected by the death of para-snowboarder [[Matthew Robinson (snowboarder)|Matthew Robinson]] just prior to the Games, and injuries to [[Cameron Rahles-Rahbula]] and [[Joany Badenhorst]] forcing them to withdraw from their events.

==Team Preparation==
Australian Paralympic Winter Program members competed in competitions in Europe and the United States in the lead up to the [[2014 Winter Paralympics]] in [[Sochi]], Russia.<ref name=apcnews>{{cite news|title=Countdown to Sochi begins|url=http://london2012.paralympic.org.au/news/countdown-sochi-2014-begins|newspaper=Australian Paralympic Committee News |date=22 October 2012|accessdate=21 January 2013}}</ref> In September 2013, Australia hosted the [[IPC Alpine Skiing World Cup]] at [[Thredbo, New South Wales]]. Australia finished the competition with three gold, three silver and one bronze medal to finish third on the medal tally behind the United States (eight gold, six silver, eight bronze medals) and Slovakia (eight gold, two silver, three bronze medals). Australia's [[Mitchell Gourley]] and [[Cameron Rahles-Rahbula]] won gold medals.<ref name=ipcworldcup>{{cite news|title=Racing cancelled on the final day of the World up|url=http://www.paralympic.org.au/news/racing-cancelled-final-day-world-cup|newspaper=Australian Paralympic Committee News |date=5 September 2013|accessdate=8 February 2014}}</ref>

Three team members – [[Toby Kane]], [[Cameron Rahles-Rahbula]] and [[Mitchell Gourley]] undertook [[wind tunnel]] testing at [[Monash University]] in 2013 to assist them in determining their optimal [[aerodynamics|aerodynamic]] position whilst skiing.
<ref>{{cite news|last=Homfray|first=Reece|title=Australia's Winter Paralympic skiers go high-tech in search for glory|url=http://www.couriermail.com.au/sport/more-sports/australias-winter-paralympic-skiers-go-hightech-in-search-for-glory/story-fnii0hmo-1226843808208|accessdate=10 March 2014|newspaper=Courier Mail|date=3 March 2014}}</ref>

===Pre Games Skiing Accidents===
The Australian Paralympic Team suffered two major skiing accidents just prior to the games.  [[Matthew Robinson (Paralympic skier)|Matthew Robinson]] died after a skiing accident while competing at the [[IPC Alpine Skiing World Cup]],<ref>{{cite news|title=Australian Paralympian Matthew Robinson dies on way home from Spain World Cup|url=http://www.couriermail.com.au/sport/more-sports/australian-paralympian-matthew-robinson-dies-on-way-home-from-spain-world-cup/story-fnii0hmo-1226834405912|accessdate=8 March 2014|newspaper=Courier Mail|date=22 February 2014}}</ref> Whilst Robinson's event was not included in the Games, he was an integral member of the Australian Paralympic snowboard team. Less than a day after being named as Australia's flagbearer for the Sochi Paralympics, [[Cameron Rahles-Rahbula]] was injured while training for the 2014 Sochi Winter Paralympic Games. Rahles-Rahbula suffered a knee injury that forced him to miss his opening races, but he still hoped to be able to compete in his pet event, the slalom, and in the giant slalom.<ref name=Paxinos>{{cite news|last=Paxinos|first=Stathi|title=Cameron Rahles-Rahbula will remain Australian flagbearer despite injury at Sochi Paralympics|url=http://www.smh.com.au/sport/winter-olympics/cameron-rahlesrahbula-will-remain-australian-flagbearer-despite-injury-at-sochi-paralympics-20140306-hvgaz.html|accessdate=8 March 2014|newspaper=Sydney Morning Herald|date=6 March 2014}}</ref> On 10 March 2014, it was revealed that Rahles-Rahbula has a knee fracture and would not be able to compete in any of his Paralympic events.<ref name=newcastle>{{cite news|last=Paxinos|first=Stathi|title=Cameron Rahles-Rahbula out of Paralympics|url=http://www.theherald.com.au/story/2137391/cameron-rahles-rahbula-out-of-paralympics/?cs=12|accessdate=23 March 2014|newspaper=Newcastle Herald|date=10 March 2014}}</ref>

==Administration==
[[File:160314 - Closing flag Tudhope Sochi 14 - 3b.jpg|thumb|right|Ben Tudhope was given the honour of carrying the flag for Australia at the Closing Ceremony]]
Kate McLoughlin was appointed the [[Chef de Mission]] in March 2013. This was the first time that a woman had been appointed a Chef de Mission.<ref>{{cite news|title=Paralympic team breaks new ground with female chef de mission Kate McLoughlin|url=http://www.theaustralian.com.au/sport/paralympic-team-breaks-new-ground-with-female-chef-de-mission-kate-mcloughlin/story-e6frg7mf-1226591856936|accessdate=8 March 2014|newspaper=The Australian|date=8 March 2013}}</ref> However, she stepped down due to family reasons, and was replaced by [[Chris Nunn]] in November 2013.<ref>{{cite news|title=Nunn in new Chef de Mission for Sochi 2014 |newspaper=Australian Paralympic Committee News |date=26 November 2013 |url=http://www.paralympic.org.au/news/nunn-new-chef-de-mission-sochi-2014 |accessdate=1 January 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140103035747/http://www.paralympic.org.au/news/nunn-new-chef-de-mission-sochi-2014 |archivedate=3 January 2014 |df=dmy }}</ref> The [[Australian Paralympic Committee]] announced that it cost AUD $1.5 million to send the Team to Sochi, with $200,000 of the budget still outstanding. To raise additional funds, it launched the "Believe" campaign.<ref>{{cite news|title=Australian Winter Paralympic Team announced|url=http://www.paralympic.org.au/news/australian-paralympic-winter-team-announced|newspaper=Australian Paralympic Committee News |date=4 February 2014|accessdate=8 February 2014}}</ref><ref name=media>{{cite web|url=http://www.paralympic.org.au/sites/default/files/APC_Media%20Guide_Sochi%202014%20Winter%20Paralympic%20Games_WEB.pdf|title=Media Guide : 2014 Sochi Winter Paralympic Games|publisher=Australian Paralympic Committee|accessdate=10 March 2014}}</ref>

{| class="wikitable" border="1"
|+ Team Support
! Position !! Person !! Reference
|-
| Chef de Mission || Chris Nunn || <ref name=media/>
|-
| Manager, Team Operations|| Caroline Walker || <ref name=media/>
|-
| Team Attaché || Adam Cormack ||<ref name=media/>
|-
| Manager, Media and Communications || Tim Mannion || <ref name=media/>
|-
| Manager, Multimedia || Sean Giles || <ref name=media/>
|-
| Head Coach, Alpine|| [[Steve Graham]] || <ref name=media/>
|-
| Assistant Coach, Alpine || [[Michael Milton (skier)|Michael Milton]] || <ref name=media/>
|-
| Coach, Snowboard || Peter Higgins || <ref name=media/>
|-
| Ski Technician || Alan Dean  || <ref name=media/>
|-
| Ski Technician || Francis "Spike" Kullas || <ref name=media/>
|-
| Chief Medical Officer || Dr Geoff Thompson || <ref name=media/>
|-
| Physiotherapist || Jonathon Davis || <ref name=media/>
|-
| Physiotherapist || Joel Cook || <ref name=media/>
|-
| Winter Sport Scientist || Markus Klusemann || <ref name=media/>
|-
| Psychologist || Sarah Jack || <ref name=media/>
|-
|}

==Team==
[[File:2014 Opening ceremony.jpg|thumb|260x260px|The Australian Team marches at the Opening Ceremony of the Sochi 2014 Paralympic Winter Games, led by flagbearer Cameron Rahles-Rahbula.]]
On 4 February 2014, [[Australian Paralympic Committee]] announced a team of nine athletes, as well as two sighted guides, to attend the 2014 Sochi Winter Paralympics. Their head coach is [[Steve Graham]].<ref name=apcnews/> Athletes selected were: 
;Alpine skiing
*[[Jessica Gallagher]]
*[[Christian Geiger]] (guide for Gallagher) 
*[[Mitchell Gourley]]
*[[Toby Kane]]
*[[Victoria Pendergast]]
*[[Melissa Perrine]]
*[[Andrew Bor]] (guide for Perrine)
*[[Cameron Rahles-Rahbula]]

;Snowboard cross
*[[Joany Badenhorst]]
*[[Trent Milton]]
*[[Ben Tudhope]].<ref name=abcnews>{{cite news|title=Australia names Winter Paralympics team for Sochi including 14-year-old Para-snowboarder Ben Tudhope|url=http://www.abc.net.au/news/2014-02-04/australia-names-winter-paralympics-team-for-sochi/5236974|newspaper=ABC News |date=5 February 2014|accessdate=8 February 2014}}</ref>

Victoria Pendergast became Australia's first female sit-skier to compete at a Winter Paralympics, and [[Ben Tudhope]], who turned 14 in December 2013, became Australia's youngest competitor at the Winter Paralympics.<ref name=abcnews/> The 14-year-old Tudhope was the youngest competitor at the Games from any nation.<ref name=closing/> The team included three medallists from previous Winter Paralympics: Jessica Gallagher, Toby Kane and Cameron-Rahles-Rahbula. Four athletes and one guide made their Games debut.<ref name=abcnews/>

==Ceremonies==
[[Cameron Rahles-Rahbula]], competing at his fourth [[Winter Paralympics]], was named as the Australian flag bearer for the [[2014 Winter Paralympics opening ceremony|Opening Ceremony]].<ref name=Paxinos/><ref name=flagbearer>{{cite news|last=Paxinos|first=Stathi|title=Cameron Rahles-Rahbula named Australian Paralympic flagbearer|url=http://www.smh.com.au/sport/winter-olympics/cameron-rahlesrahbula-named-australian-paralympic-flagbearer-20140305-345wq.html|accessdate=5 March 2014|newspaper=Sydney Morning Herald|date=5 March 2014}}</ref> After being named flag bearer, Rahles-Rahbula had a skiing training accident that made his participation doubtful. He went on to carry the flag but withdrew from the Games on medical advice.<ref name=Paxinos/> At Opening Ceremony, the entire Australian team of nine athletes, coaches, medical staff and administration officials wore the black arm bands to remember [[Matthew Robinson (snowboarder)|Matthew Robinson]], who had recently died as a result of a skiing accident.<ref name=memorial>{{cite news|last=McDonald|first=Margie|title=Emotional tribute to fallen Paralympian Matthew Robinson|url=http://www.theaustralian.com.au/sport/emotional-tribute-to-fallen-paralympian-matthew-robinson/story-e6frg7mf-1226847640435|accessdate=8 March 2014|newspaper=The Australian|date=7 March 2014}}</ref> [[Ben Tudhope]], the youngest competitor at the Games, carried the Australian flag at the [[2014 Winter Paralympics closing ceremony|Closing Ceremony]].<ref name=closing>{{cite news|last=Paxinos|first=Stathi|title=Sochi Winter Paralympics end in style|url=http://www.smh.com.au/sport/winter-olympics/sochi-winter-paralympics-end-in-style-20140317-hvjgj.html|accessdate=16 March 2014|newspaper=Sydney Morning Herald|date=17 March 2014}}</ref>

==Medallists==
{| class="wikitable sortable" style="font-size:90%"
|-
!Medal
!Name
!Sport
!Event
!Date
|-
|{{bronze03}}
|[[Toby Kane]]
|[[Alpine skiing at the 2014 Winter Paralympics|Alpine Skiing]]
|[[Alpine skiing at the 2014 Winter Paralympics – Men's combined|Men's Super combined standing]]
|14 March
|-
|{{bronze03}}
|[[Jessica Gallagher]] /[[Christian Geiger]] (guide)
|[[Alpine skiing at the 2014 Winter Paralympics|Alpine Skiing]]
|[[Alpine skiing at the 2014 Winter Paralympics – Women's giant slalom|Women's giant slalom visually impaired]]
|15 March
|}
<ref name=results>{{cite web|title=Latest results|url=http://www.paralympic.org.au/sochi-2014/latest-results/latest-results|work=Australian Paralympic Committee Sochi 2014|accessdate=23 March 2014}}</ref>

==Events==
[[File:Sochi2014 D1 DH M Perrine 01.JPG|300px|thumb|right|float|Melissa Perrine and her guide Andy Bor in the Women's Downhill at the 2014 Winter Paralympics]]

===Alpine skiing===
{{Main|Alpine skiing at the 2014 Winter Paralympics}}
The [[International Paralympic Committee]] introduced [[Para-snowboarding classification|para-snowboard]] as an alpine skiing discipline for the 2014 Games. These events are for standing athletes with a lower body disability.<ref name=apcnews/>

;Women
{|class="wikitable" style="font-size:100%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="4"|Final
!rowspan="2"|Date
|-
!Run 1
!Run 2
!Total Time
!Rank
|- align="center"
|align="left" rowspan=2|[[Jessica Gallagher]] / <br/> [[Christian Geiger]] (guide) [[B3 (classification)|B3]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's slalom|Slalom B & VI]]
|1:04.09
|1:38.46
|2:42.55
|7
|14 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's giant slalom|Giant slalom B & VI]]
|1:36.69
|1:25.42
|3:02.11
||{{bronze03}}
|16 March
|- align="center"
|align="left" rowspan=5|[[Melissa Perrine]] / <br/> [[Andy Bor]] (guide) [[B2 (classification)|B2]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's downhill|Downhill B & VI]]
|colspan="2" bgcolor="wheat"|
|1:36.15
|4
|8 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's Super-G|Super G B & VI]]
|colspan="2" bgcolor="wheat"|
|colspan="2"|DNF
|10 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's combined|Super combined B & VI]]
|DSQ
|colspan="3"|
|11 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's slalom|Slalom B & VI]]
|DNF
|colspan="3"|
|14 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's giant slalom|Giant slalom B & VI]]
|colspan="4"|DNF
|16 March
|- align="center"
|align="left" rowspan=2|[[Victoria Pendergast]] [[LW12|LW12-1]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's slalom|Slalom sit-ski]]
|1:21.53
|1:21.82
|2:43.35
|7
|14 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's giant slalom|Giant slalom sit-ski]]
|1:54.37
|1:40.22
|3:34.59
|10
|16 March
|- align="center"
|align="left" |[[Joany Badenhorst]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Women's snowboard cross|Cross Lower Limb]]
|colspan="4"|DNS
|14 March
|}
<ref name=results/> <br/>
DNF – did not finish<br/>
DSQ – disqualified. Melissa Perrine was disqualified after the slalom leg of the Super combined for wearing a visor, which was taped to her helmet, to keep rain from her goggles. This was a breach of the IPC Alpine Skiing rules. [[Jason Hellwig]], CEO of the Australian Paralympic Committee described it as "mindnumbingly-dumb mistake" as it was not picked up by relevant team officials. He indicated it was an honest mistake and there was no intention to cheat.<ref name=theage>{{cite news|last=Paxinos|first=Stathi|title=Sochi Winter Paralympics: Australian team owns up to 'dumb' mistake|url=http://www.theage.com.au/sport/winter-olympics/sochi-winter-paralympics-australian-team-owns-up-to-dumb-mistake-20140312-hvhmi.html?skin=text-only|accessdate=12 March 2014|newspaper=The Age|date=12 March 2014}}</ref> <br/>
DNS – Badenhorst was forced to withdraw from the event after suffering an injury to her left knee whilst training on the morning of the event.<ref name=results/>
<ref>{{cite news|title=Badenhorst out of Sochi para-snowboard |url=http://au.news.yahoo.com/thewest/sport/a/21983200/badenhorst-out-of-sochi-para-snowboard/ |accessdate=14 March 2014 |newspaper=Yahoo News |date=14 March 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20140314234903/http://au.news.yahoo.com/thewest/sport/a/21983200/badenhorst-out-of-sochi-para-snowboard/ |archivedate=14 March 2014 |df=dmy }}</ref>
;Men
{|class="wikitable" style="font-size:100%"
|-
!rowspan="2"|Athlete
!rowspan="2"|Event
!colspan="4"|Final
!rowspan="2"|Date
|-
!Run 1
!Run 2
!Total Time
!Rank
|- align="center"
|align="left" rowspan=5|[[Mitchell Gourley]] [[LW6/8|LW6/8-2]]
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's downhill|Downhill Standing]]
|colspan="2" bgcolor="wheat"|
|1:26.71
|7
|8 March
|- align="center"
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's slalom|Slalom Standing]]
|50.75
|colspan="3"|DNF
|13 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's giant slalom|Giant slalom standing]]
| 1:16.16  
|colspan="3"|DNF
|15 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's Super-G|Super G standing]]
|colspan="2" bgcolor="wheat"|
|colspan="2"|DNF
|9 March
|- align="center"
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's combined|Super combined standing]]
|53.63
|1:20.75
|2:14.38
|5
|11 March
|- align="center"
|align="left" rowspan=5|[[Toby Kane]] [[LW2 (classification)|LW2]]
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's downhill|Downhill Standing]]
|colspan="2" bgcolor="wheat"|
|1:26.25
|6
|8 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's slalom|Slalom Standing]] 
|48.69
|54.56
|1:43.48
|4
|13 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's giant slalom|Giant slalom Standing]]
|colspan="4"|DNF
|15 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's Super-G|Super G Standing]]
|colspan="2" bgcolor="wheat"|
|colspan="2"|DNF
|9 March
|- align="center"
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's combined|Super combined Standing]]
|53.52
|1:20.62
|2:14.14
|{{bronze03}}
|11 March
|- align="center"
|align="left" rowspan=5|[[Cameron Rahles-Rahbula]] [[LW2 (classification)|LW2]]
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's downhill|Downhill Standing]]
|colspan="2" bgcolor="wheat"|
|colspan="2"|DNS
|8 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's slalom|Slalom Standing]] 
|colspan="4"|DNS
|13 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's giant slalom|Giant slalom Standing]]
|colspan="4"|DNS
|15 March
|- align="center"
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's Super-G|Super G Standing]]
|colspan="2" bgcolor="wheat"|
|colspan="2"|DNS
|9 March
|- align="center"
|align="left"|[[Alpine skiing at the 2014 Winter Paralympics – Men's combined|Super combined Standing]]
|colspan="4"|DNS
|11 March
|- align="center"
|align="left" |[[Trent Milton]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's snowboard cross|Cross Lower Limb]]
|1:03.58
|1:14.27
|2:07.95
|20
|14 March
|- align="center"
|align="left" |[[Ben Tudhope]]
|align="left"| [[Alpine skiing at the 2014 Winter Paralympics – Men's snowboard cross|Cross Lower Limb]]
|58.78
|59.31
|1:56.84
|10
|14 March
|}
<ref name=results/> <br/>
DNF – did not finish. <br/>
DNS – did not start. Cameron Rahles-Rahbula withdrew from all competitions after the Games commenced.  This was due to injuries sustained in training just prior to the Games.

==Broadcasting==
The [[Australian Broadcasting Corporation]] showed a daily 30 minutes highlights program twice a day, and also streamed the alpine skiing and ice hockey live on its Grandstand website. The Games' opening ceremony was broadcast live. The presenters for the ABC's coverage were Amanda Shalala and Darren Boyd.<ref>{{cite news|title=Winter Paralympics Sochi 2014|url=http://www.abc.net.au/tv/programs/winter-paralympics-sochi-2014/|newspaper=Australian Broadcasting Corporation website|accessdate=10 March 2014}}</ref>

==Outcome==
Australia finished the Games with two bronze medals. Jason Hellwig stated that the aim was to win between two and five medals. He said: "wo medals absolutely is a pass ... so we're really pleased with that but we're absolutely disappointed we didn't get the mission done to win that gold medal, but I tell you what, we are absolutely determined to get it done in four years' time. It hasn't been easy, we've come in under the most difficult of circumstances that I've ever seen a team  at a major Games."<ref name=closing/> Chris Nunn, Chef de Mission, pointed to the future by stating that  "We really need to focus on having good-quality athletes who are robust and have financial support to get overseas. It's not like living in Austria, Sweden or Germany where you can do this on the weekend and after work."<ref name=aftersochi>{{cite news|last=Paxinos|first=Stathi|title=Australian Winter Paralympics team to rebuild after Sochi|url=http://www.smh.com.au/sport/winter-olympics/australian-winter-paralympics-team-to-rebuild-after-sochi-20140317-hvjsi.html|accessdate=23 March 2014|newspaper=Sydney Morning Herald|date=17 March 2014}}</ref>

Toby Kane and Cameron Rahles-Rahbula indicated that Sochi Games would be their last [[Winter Paralympics]]. Kane and Dutch snowboarder [[Bibian Mentel-Spee]] were named winners of the [[Whang Youn Dai Achievement Award]], which is presented at every Paralympic Games for outstanding performances and overcoming adversity.<ref name=award>{{cite news|last=Paxinos|first=Stathi|title=Sochi Winter Paralympics: Toby Kane becomes first Australian to win Games' top award|url=http://www.smh.com.au/sport/winter-olympics/sochi-winter-paralympics-toby-kane-becomes-first-australian-to-win-games-top-award-20140315-hvj6p.html|accessdate=15 March 2014|newspaper=Sydney Morning Herald|date=15 March 2014}}</ref>

==References==
{{reflist|30em}}

==External links==
*[http://www.sochi2014.com/ Sochi 2014 Paralympic Games official website]
*[http://www.paralympic.org International Paralympic Committee official website]
*[http://www.paralympic.org.au/sites/default/files/APC_Media%20Guide_Sochi%202014%20Winter%20Paralympic%20Games_WEB.pdf Australian Paralympic Committee Media Guide Sochi 2014 Winter Paralympic Games]
{{2014 Australian Paralympic Team}}
{{NPCin2014WinterParalympics}}
{{portal bar|Australia|Paralympics|Winter Paralympics}}

{{DEFAULTSORT:Australia at the 2014 Winter Paralympics}}
[[Category:Nations at the 2014 Winter Paralympics]]
[[Category:Australia at the Paralympics|2014]]
[[Category:2014 in Australian sport|Paralympics]]