{{Infobox journal
| cover = [[File:International Journal of Radiation Biology.jpg|150px]]
| editor = Professor Gayle Woloschak
| former_name = International Journal of Radiation Biology and Related Studies in Physics, Chemistry and Medicine
| discipline = [[Radiobiology]]
| abbreviation = Int. J. Rad. Biol.
| publisher = [[Informa]]
| country =
| frequency = Monthly
| history = 1988-present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 1.687
| impact-year = 2014
| website = http://informahealthcare.com/rab
| link1 = 
| link1-name = 
| link2 =
| link2-name =
| JSTOR =
| OCLC =
| LCCN =
| CODEN = IJRBE7
| ISSN = 0955-3002
| eISSN = 1362-3095
}}
The '''''International Journal of Radiation Biology''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] that covers research into the effects of [[ionizing radiation|ionizing]] and [[non-ionizing radiation]] in biology. The [[editor-in-chief]] is Professor Gayle Woloschak.

The title was formerly known as ''International Journal of Radiation Biology and Related Studies in Physics, Chemistry and Medicine'', having changed its name in 1988.

== Abstracting and indexing ==
The journal is abstracted and indexed in:
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web|url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-31 }}{{dead link|date=April 2017 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[Pubmed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/8809243 |title=International Journal of Radiation Biology |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2014-12-31}}</ref>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-12-31}}</ref>
* [[Current Contents]]/Life Sciences<ref name=ISI/>
* [[BIOSIS Previews]]<ref name=ISI/>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/__data/assets/excel_doc/0005/226742/title_list.xlsx |title=Scopus title list |format=[[Microsoft Excel]] |publisher=[[Elsevier]] |work=[http://www.elsevier.com/online-tools/scopus/content-overview Scopus coverage lists] |accessdate=2014-12-31}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.687.<ref name=WoS>{{cite book |year=2015 |chapter=International Journal of Radiation Biology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
*{{Official|http://informahealthcare.com/rab}}

[[Category:Publications established in 1988]]
[[Category:Radiology and medical imaging journals]]
[[Category:Monthly journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]