{{Infobox person
| name   = Margaret Ringenberg
| image     =Margaret Ringenberg in cockpit.jpg
| image_size     =200px 
| caption  =Margaret at the controls of a fighter during World War II.
| birth_date  =June 17, 1921
| birth_place =[[Fort Wayne, Indiana]] 
| death_date  =July 28, 2008 (aged 87)
| death_place =[[Oshkosh, Wisconsin]]
| occupation     =Aviator<br/>Author<br/>Spokesperson
| spouse         =Morris Ringenberg
| parents        =
| children       =
}}
'''Margaret Ringenberg''' (née '''Ray'''; June 17, 1921, [[Fort Wayne, Indiana]] &ndash; July 28, 2008, [[Oshkosh, Wisconsin]]) was an American [[aviator]], who had logged more than 40,000 hours of flying time during her career.<ref name="ACS">{{cite web|url=http://www.che.uc.edu/acs/dec01.html |title=Featured Speaker: Margaret Ringenberg |date=2001-12-05 |publisher=Cincinnati Section, American Chemical Society |accessdate=2008-07-29 |deadurl=yes |archiveurl=https://web.archive.org/web/20081204113808/http://www.che.uc.edu/acs/dec01.html |archivedate=2008-12-04 |df= }}</ref>

==Career and accomplishments==
[[File:Ringenberg1.jpg|left|thumb|Ringenberg in her flight suit.]]
She became interested in flying as an eight-year-old when she saw a [[barnstorming|barnstormer]] land in a field near her family's farm.<ref name="APObituary">{{cite web|url=http://www.miamiherald.com/news/nation/AP/story/621174.html |title=Obituaries in the news |date=2008-07-28 |publisher=[[Associated Press]] |accessdate=2008-07-29 }}{{dead link|date=June 2016|bot=medic}}{{cbignore|bot=medic}}</ref> She trained at a flight training school and had her first solo flight in 1941 as a 19-year-old.<ref name="APObituary" /> Ringenberg began her aviation career in 1943 during [[World War II]] when she became a ferry pilot with the [[Women Airforce Service Pilots]] (WASP).<ref name="APObituary" /> Although WASP pilots were not allowed to fly combat missions, they served grueling, often dangerous duties, such as ferrying, test flying, and target towing. The WASP corps was disbanded at the end of 1944. Ringenberg went on to become a flight instructor in 1945 and flew as a commercial pilot and instructor for the rest of her life.<ref name="ACS" /> After the war, she answered phones at an airport.<ref name="APObituary" /> 

She began racing airplanes in the 1950s.<ref name="APObituary" /> She raced in every [[Powder Puff Derby (1947)|Powder Puff Derby]] from 1957 to 1977, every Air Race Classic since 1977, the Grand Prix and the Denver Mile High and many others, garnering over 150 trophies for her accomplishments.<ref name="ACS" /> She completed the Round-the-World Air Race in 1994 at age 72,<ref name="APObituary" /> and in March 2001 at the age of 79 she flew in a race from [[London]] to [[Sydney]].<ref name="ACS" />

In 1999 she received the [[National Aeronautic Association|NAA]] Elder Statesman in Aviation Award in a presentation ceremony in Washington, DC.

Margaret Ringenberg was married to banker Morris Ringenberg in 1946.<ref name="APObituary" /> He preceded her in death in 2003.<ref name="Indy500" /> They had two children and five grandchildren.<ref name="Indy500" /> All of her children have flown with her in races and all have been in the winner’s circle with her to receive trophies.<ref name="Indy500">{{cite web|url=http://www.indy500.com/news/2586/Female_Flight_Pioneer_Adds_Speedway_Lap_To_A_Life_Of_Speed|title=Female Flight Pioneer Adds Speedway Lap To A Life Of Speed|last=Mittman|first=Don|date=2004-04-21|publisher=[[Indianapolis Motor Speedway]]|accessdate=2008-07-29}}</ref>

==Books==
[[Tom Brokaw]] devoted a chapter to Margaret Ringenberg in his book ''The Greatest Generation''.<ref name="APObituary" /> During an interview with Brokaw she said, “I started out flying because I wanted to be a stewardess—you call them flight attendants nowadays—and I thought ‘what if the pilot gets sick or needs help? I don’t know the first thing about airplanes’ and that’s where I found my challenge. I never intended to solo or be a pilot. I found it was wonderful.” Following her death, Brokaw said, in a telephone interview "Margaret was one of my favorites".<ref>{{cite web|url=http://www.journalgazette.net/apps/pbcs.dll/article?AID%3D%2F20080729%2FLOCAL%2F807290306 |title=Archived copy |accessdate=2005-11-25 |deadurl=yes |archiveurl=https://web.archive.org/web/20110522122351/http://www.journalgazette.net/apps/pbcs.dll/article?AID=/20080729/LOCAL/807290306 |archivedate=2011-05-22 |df= }}</ref>

Ringenberg's autobiography ''Girls Can’t Be Pilots,'' written with [[Jane L. Roth]] was published by Daedalus Press in 1998. (ISBN 0-9661859-0-0). It was illustrated with several photos from her career both as a WASP and as an air racer.

==Death==
Ringenberg died in her sleep of natural causes on July 28, 2008 while attending the [[Experimental Aircraft Association]] [[EAA AirVenture Oshkosh|annual airshow]].<ref name="APObituary" /> She was representing the WASPs.

==References==
{{reflist}}
*[https://web.archive.org/web/20060721092010/http://www.ww2conference.org:80/speakers_memory_ringenberg.html The International Conference on World War II]
*[https://web.archive.org/web/20081204113808/http://www.che.uc.edu/acs/dec01.html Brief biography]

==Further Information==
*Paluso, Philip. ''Wings for Maggie Ray''. [Fishers, IN]: Medium Cool Pictures, 2012. ISBN 9781618944443

==External links==
*[http://lcweb2.loc.gov/diglib/vhp/story/loc.natlib.afc2001001.02701/transcript?ID=sr0001 Library of Congress interview]
*[https://web.archive.org/web/20070927181948/http://www.indy500.com/news/story.php?story_id=2586 Story about Margaret Ringenberg Indy 500 experience in 2004]
*[http://www.canadian99s.org/articles/rarebirds.htm Story of Margaret Ringenberg’s 1994 around-the-world air race]
*[https://web.archive.org/web/20051216102915/http://www.airraceclassic.org:80/pages/2/page2.html?refresh=1116256106436 Air Race Classic]

{{Authority control}}

{{DEFAULTSORT:Ringenberg, Margaret}}
[[Category:1921 births]]
[[Category:2008 deaths]]
[[Category:American aviators]]
[[Category:Female aviators]]
[[Category:Aviators from Indiana]]
[[Category:People from Fort Wayne, Indiana]]
[[Category:Women in World War II]]
[[Category:Women Airforce Service Pilots]]
[[Category:American female aviators]]