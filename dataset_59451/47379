{{Infobox person
| honorific_prefix          = 
| name                      = Darren Romanelli 
| honorific_suffix          = 
| native_name               =
| native_name_lang          =
| image                     = Darren Romanelli shot by Jason Wong, 2013.jpg
| image_size                = 
| alt                       = 
| caption                   = 
| birth_name                = 
| birth_date                = <!--{{Birth-date|Month DD, YYYY}}-->
| birth_place               = 
| disappeared_date          = <!-- {{Disappeared date and age|YYYY|MM|DD|YYYY|MM|DD}} (disappeared date then birth date) -->
| disappeared_place         = 
| disappeared_status        = 
| death_date                = <!--{{Death-date and age|Month DD, YYYY|Month DD, YYYY}}-->
| death_place               = 
| death_cause               = 
| body_discovered           = 
| resting_place             = 
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 = 
| residence                 = 
| nationality               = American
| other_names               =  Dr. Romanelli, DRx
| ethnicity                 = <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               = 
| education                 = 
| alma_mater                = 
| occupation                = {{flatlist|
*Designer
*Marketer
*Director}}
| years_active              = 
| employer                  = 
| organization              = 
| agent                     = 
| known_for                 = 
| notable_works             = 
| style                     = 
| influences                = 
| influenced                = 
| home_town                 = 
| salary                    = 
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| television                = 
| title                     = 
| term                      = 
| predecessor               = 
| successor                 = 
| party                     = 
| movement                  = 
| opponents                 = 
| boards                    = 
| religion                  = <!-- Religion should be supported with a citation from a reliable source -->
| denomination              = <!-- Denomination should be supported with a citation from a reliable source -->
| criminal_charge           = <!-- Criminality parameters should be supported with citations from reliable sources -->
| criminal_penalty          = 
| criminal_status           = 
| spouse                    = 
| partner                   = 
| children                  = 
| parents                   = 
| relatives                 = 
| callsign                  = 
| awards                    = 
| signature                 = 
| signature_alt             = 
| signature_size            = 
| module                    = 
| module2                   = 
| module3                   = 
| module4                   = 
| module5                   = 
| module6                   = 
| website                   = {{URL|www.drromanelli.com/}} 
| footnotes                 = 
| box_width                 = 
}}
'''Darren Romanelli''' (DRx, Dr. Romanelli) is a [[Los Angeles]] based [[designer]], [[marketer]], and director.<ref>[http://www.realeyesonline.net/portfolio/darren-romanelli/ Darren Romanelli], Real Eyes Magazine, October 2012, Retrieved 8 November 2012</ref> He is most notable for creating series of customized, limited edition clothing, furniture and collectibles. Romanelli adds his signature (DRx) to each project and has been involved with all levels of a project's development, from inception to display to promotion.<ref>[http://articles.latimes.com/2007/dec/23/image/ig-lupe23 Double Dose of Cool: Darren Romanelli Has the Prescription For Lupe Fiasco], LA Times,  December 2007, Retrieved 8 November 2012</ref> He has had collaborations with [[Converse (shoe company)|Converse]],<ref>[http://www.highsnobiety.com/2011/10/10/dr-romanelli-beetle-bailey-vs-popeye-x-converse-chuck-taylor-all-star-sneakers/%20%20 Dr. Romanelli - Beetle Bailey vs. Popeye x Converse Chuck Taylor All Star Sneakers] HighSnobiety.com, October 2011, Retrieved 8, November, 2012</ref> [[Coca-Cola]],<ref>[http://www.highsnobiety.com/2013/10/23/dr-romanelli-coca-cola-2013/ Q&A: Dr. Romanelli on his Coca-Cola DRx Collection, High Snobiety,October 2013, Retrieved Jan. 2014]</ref> [[Jaeger LeCoultre]],<ref>[http://highsnobiety.blogspot.com/2006/01/dr-romanelli-x-jaeger-lecoultre-launch.html Dr. Romanelli x Jaeger LeCoultre Launch, High Snobiety, January 2006, Retrieved Jan. 2014]</ref> and  [[Disney]] among others.<ref>[http://blog.misskl.com/index.php/2012/10/dr-romanelli-joins-up-with-disney-couture/ Dr. Romanelli Joins Up With Disney Coutre, Miss KL Blog, October 2012, Retrieved Jan. 2014]</ref>

== Collaborations ==

=== 2014 ===
'''DRx x [[British Knights]]''' - British Knights is a New York-based footwear label inspired by [[hip-hop culture]]. Romanelli was brought on board as the creative "doctor" to bring a new look to the non-performance shoes. In this collaboration he created four styles that combined performance-level materials with [[streetwear]] aesthetics. Features included earth tone midtops with ribbed brown leather paneling and black and white hi-tops that incorporate the [[British flag]]. The first designs were unveiled on February 17, 2014 at [http://agendashow.com/travel/las-vegas Agenda] in [[Las Vegas]] before officially going on sale March 15, 2014.<ref>[http://hypebeast.com/2014/2/dr-romanelli-x-british-knights Dr. Romanelli x British Knights, HypeBeast.com, Feb. 2014, Retrieved Feb. 2014]</ref><ref>[http://www.billboard.com/biz/articles/news/5908251/scooter-braun-on-relaunching-british-knights-sneakers-artists-are-the-new Scooter Braun on Relaunching British Knights Sneakers: 'Artists Are the New Athletes', Billboard.com, Feb. 2014, Retrieved Feb. 2014]</ref>

=== 2013 ===
[[File:DRx Coca Cola Collaboration.jpg|thumb|right|DRx x Coca-Cola Collaboration, 2013]]
* '''DRx x [[Coca-Cola]]''' - Romanelli – inspired by his own collection of retro [[Coca-Cola Company|Coke]] clothing, hand-picked from flea markets and vintage stores in his hometown of [[Los Angeles]]  to  launch this  200 piece [[Coca-Cola Company|Coca-Cola]] by DRx collection, replete with his signature bomber and biker jackets, vests, and cut-and-sew T-shirts. The collection also included [[Coca-Cola Company|Coke]] workwear uniforms of deliveryman jackets and factory-worker button-ups that still had the employee’s names stitched on the garments.  The collection was available at select retailers, including [http://wikifashion.com/wiki/Opening_Ceremony Opening Ceremony in New York], Juice in Hong Kong,<ref>[http://clotinc.com/juicehk/ Juice HK, clotinc.com, Retrieved Jan. 2014]</ref> Fruition in Las Vegas,<ref>[http://shop.fruitionlv.com/category/mens/dr-romanelli/ Shop Fruition Las Vegas - Coca Cola x DRx Collection, Dec. 2013, Retrieved Jan. 2014]</ref>  [[Colette (boutique)|Colette]] in Paris and United Arrows<ref>[[:ja:ユナイテッドアローズ|United Arrows in Tokyo - Japanese Clothing Store, Wikipedia - Japan, Retrieved Jan. 2014]]{{Better source|date=November 2015|reason=[[WP:CIRCULAR]]}}</ref> in Tokyo.
*'''DRx x [[Converse (shoe company)|Converse]] Boro Chuck Taylor 1970s''' - Romanelli has worked with Converse before and for this collaboration decided to use vintage boro, a collectible Japanese textile. The [[Converse (shoe company)|Converse]] Chuck Taylor 1970s encapsulated both brands’ (Romanelli's and Convers') appreciation for premium materials with olive cotton twill, inspired by a vintage [[World War II]] [[U.S. Army]] jacket from Romanelli’s personal vintage collection, paired with a coarse cotton plaid. Due to the nature of these materials each pair made was slightly different and sold in limited supply.<ref>[http://www.highsnobiety.com/2013/07/19/dr-romanelli-converse-boro-chuck-taylor-1970s/ Dr. Romanelli Converse Boro Chuck Taylor 1970s, HighSnobiety.com, July 2013, Retrieved Feb. 2014]</ref><ref>[http://streething.com/news/11309/dr-romaneli-x-converse-first-string-boro-chuck-taylor-all-star/ THING: Dr Romanelli x Converse First String “BORO” Chuck Taylor All Star, streetThing.com, July 2013, Retrieved Feb. 2014]</ref>
*'''DRx x [[Stussy]] Taipei Capsule Collection''' - Developed over the course of more than a year, Romanelli collaborated with [[Stussy]] for their Taipei Capsule Collection in honor of the Stussy Taipei’s store opening. Visiting Stussy’s warehouses, Romanelli sifted through vintage collections for the most iconic pieces from Stussy’s past. He then fused these items together to formulate new pieces, with some of the elements dating as far back as the 1980s. The result was an amalgamation of Stussy’s history, a collection that was to both celebrate the brand’s past and acknowledges its transition into the future. The collection was available exclusively at the Stussy Taipei chapter store.<ref>[http://hypebeast.com/2013/3/drx-romanelli-for-stussy-taipei-capsule-collection Romanelli for Stussy Taipei Capsule Collection, HypeBeast, April 2013, Retrieved Feb. 2014]</ref>

=== 2012 ===
*'''DRx for [[Disney]] Couture Jewelry Collection''' - In this collaboration with [[Disney]] Couture, Romanelli created a limited-time collection that reconstructed [[Disney characters]] from junkyard materials, while still keeping their iconic designs. The collection boasted unique reconstruction to ensure each piece of jewelry was truly different.<ref>[http://sneakhype.com/accessories/2012/11/dr-romanellis-recycled-mickey-necklace.html Dr. Romanelli's Recycled Mickey Necklace, Sneak Hype, Nov. 2012, Retrieved Jan. 2014]</ref>
* '''Stealth by DRx For UNKNWN [[LeBron James]] Jersey Collection''' - For the UNKNWN Stealth Collection Romanelli used a selection of skins, leathers and vintage military materials to create the reconstructed jerseys. The collection was sold in limited supply at the menswear shop, UNKNWN owned by [[LeBron James]] in [[Miami, FL|Miami, Fl]].<ref>[http://www.djbobbytrends.com/fashion-drx-romanelli-x-lebron-james-snakeskin-miami-heat-jersey-for-unknwn/ DRx x Romanelli x LeBron James Snakeskin Miami Heat Jersey, djbobbytrends.com, Retrieved Feb. 2014]</ref><ref>[http://www.complex.com/style/2012/04/the-complete-stealth-by-drx-for-unknwn-lebron-james-jersey-collection The Complete Stealth By DRx For Unknwn LeBron James jersey Collection, Complex.com, April 2012, Retrieved Feb. 2014]</ref>

=== 2011===
* '''DRx and [[MediCom|Medicom]] - [[Bearbrick|BearBricks Toy]] - [[Popeye]] Vs. [[Beetle Bailey]]''' - As part of one of his holiday collections, Army Vs. Navy, Romanelli collaborated with [[MediCom|Medicom]] Toy to include a collection of [[Bearbrick|BearBricks Toy]]s incorporating the likeness of [[Popeye]] and the popular [[Beetle Bailey]] comic strip character.  One of the toys represented the '[[Navy SEAL|Navy]]' side with [[Popeye]] and the other represented the '[[Army National Guard|Army]]' side with [[Beetle Bailey]]. The toys were sold at select [[MediCom|Medicom]] retailers and were featured in both 100% and 400% versions.<ref>[http://www.highsnobiety.com/2011/09/23/dr-romanelli-x-medicom-toy-beetle-bailey-popeye-bearbricks/ DRx and Medicom BearBricks: Popeye Vs. Beetle Bailey] HighSnobiety.com, Retrieved 11, February 2013</ref><ref>[http://hypebeast.com/2011/9/dr-romanelli-x-medicom-toy-beetle-bailey-popeye-bearbricks|Dr. Romanelli x Medicom Toy Beetle Bailey & Popeye Bearbricks] Hypebeast.com, Retrieved 11, February 2013</ref>
*'''Dr. Romanelli - [[Beetle Bailey]] vs. [[Popeye]] - [[Converse (shoe company)|Converse]] [[Chuck Taylor All-Stars|Chuck Taylor All Star Hi Shoe]]''' - Following up on the Dr. Romanelli and [[MediCom|Medicom]] [[Bearbricks]] Toy: [[Beetle Bailey]] and [[Popeye]] collaboration <ref>[http://www.highsnobiety.com/2011/09/23/dr-romanelli-x-medicom-toy-beetle-bailey-popeye-bearbricks/ DRx and Medicom BearBricks: Popeye Vs. Beetle Bailey] HighSnobiety.com, Retrieved 11, February 2013</ref> Romanelli again joined forces with [[Converse (shoe company)|Converse]] to add shoes to the [[Beetle Bailey]] Vs [[Popeye]] Collection, which launched exclusively at [[Bloomingdales]] on October 15, 2011.  Together with [[Converse (shoe company)|Converse]], Romanelli presented two [[Chuck Taylor All-Stars|Chuck Taylor All Star Hi shoes]]. One of them featured a [[military]] green canvas upper, black leather details, gum sole with rubber toe cap, numerous embroidery and a [[Beetle Bailey]] comic strip print lining. The second one came with a black wool upper, anchor embroidery, red line detail on the ankle and a [[Popeye]] comic strip print lining.<ref>[http://www.highsnobiety.com/2011/10/10/dr-romanelli-beetle-bailey-vs-popeye-x-converse-chuck-taylor-all-star-sneakers/%20 Dr. Romanelli Beetle Bailey vs. Popeye x Converse Chuck Taylor All Star Sneakers] HighSnobiety.com, October 2011, Retrieved 13, February 201</ref>

=== 2010 ===
* '''The [[Chuck Taylor All-Stars|Chuck Taylor]] ''All Star and Stripes'',  Shoe''' - Romanelli was the first artist to collaborate with [[Converse (shoe company)|Converse]] when the [[Chuck Taylor All-Stars|Chuck Taylor All Star]] shoes were first introduced. The ''All Star and Stripes'' shoe, with the [[Chuck Taylor All-Stars|All Star]] silhouette and futuristic touches, debuted in June 2010. The shoes were a combination of classic [[Converse (shoe company)|Converse]] style and modern details as Romanelli regularly fuses vintage [[Americana]] while using a modern approach.  Romanelli added stars and stripes to two versions of the shoe and both versions incorporated clean uppers with 12-ounce [[Cotton duck|duck canvas]] and leather with antiqued details that together make up the stripes of the [[American flag]]. Wool toe caps lent the feeling of a vintage [[peacoat]] and gum soles added to the classic look and feel of the shoe. In addition, Romanelli's abbreviated "Dr. X" moniker appeared on the tongue and a unique rendition of [[the American Eagle]] could be found on the sock liner. The shoe was available in high-top leather and [[Cotton duck|duck canvas]] with wool toe caps and gum soles.<ref>[http://www.offspring.co.uk/perl/go.pl/blog/index.html?currentmonth=09&currentyear=2010 Converse Dr. Romanelli] Offspring.com, September 2010, Retrieved 11 February 2013</ref>
* '''[[Converse (shoe company)|Converse]] - (PRODUCT) Red - [[Chuck Taylor All-Stars|Chuck Taylor]] Pack, Shoe''' - [[Converse (shoe company)|Converse]] teamed up with Romanelli and [[AIDS virus|AIDS]] awareness group [[Product Red|PRODUCT (RED)]] on a special duo of [[Chuck Taylor All-Stars|Chuck Taylor]]'s. Romanelli  kept the [[Converse All Star|All Star 2000]]s look while adding two color ways of [[Americans|American]] flag-inspired [[Chuck Taylor All-Stars|Chuck]]s. The shoe also featured [[Cotton duck|duck canvas]], [[Peacoat|pea-coat]] wool, and a gum sole to make the shoes. These shoes were a limited edition and proceeds supported the [[Product Red|PRODUCT (RED)]] cause.<ref>[http://www.highsnobiety.com/2010/09/07/dr-romanelli-x-converse-productred-born-in-the-usa-chuck-taylor-cups-pack/ Dr. Romanelli x Converse PRODUCT (RED)] HighSnobiety.com, Retrieved 11, February 2013</ref><ref>[http://www.idiomag.com/peek/113765/romanell Romanelli News: Dr. Romanelli x Converse x Product Red] IdioMag.com, September 2010, Retrieved 11, February 2013</ref>

=== 2009 ===
*'''[[Fraggle Rock]] - Dr. Doozer''' - A line of high-fashion T-shirts and toys designed by Romanelli featuring [[Jim Henson]]'s [[Fraggle Rock]] characters.The line was sold at high-end stores such as; [[Kitson (store)|Kitson]] in [[West Hollywood, California|West Hollywood]] and [[Colette (boutique)|Colette]] in [[Paris, France|Paris]].<ref>[http://www.drdoozer.com Fraggle Rock: Dr. Doozer] Dr.Doozer.com, Retrieved 31, January 2013</ref>
* '''[[Pete Wentz]] - [[Fall Out Toy Works|Fall Out Toyworks]]''' -  [[Fall Out Toy Works]] is a limited edition comic book series, created by Romanelli, [[Pete Wentz]] of [[Fall Out Boy]] and Nathan Cabrera.  It was written by Brett Lewis, whose previous work includes [[The Winter Men]], and was illustrated by several members of [[Imaginary Friends Studios]].  The first issue was published by [[Image Comics]] on September 2, 2009.  The story is about androids with artificial intelligence in a futuristic Los Angeles. It has been compared to [[Pinocchio]] and Pygmalion.<ref>[http://www.comicbookresources.com/?page=article&id=21877 Wentz and Romanelli Talk "Fall Out Toyworks"] ComicBookResources.com, Retrieved 5, February, 2013</ref>

=== 2008 ===
*'''DRx and Huf'''- Romanelli collaborated with Huf, an independent manufacturer of premium apparel, footwear, and [[skateboarding]] goods located in both [[Los Angeles]] and [[San Francisco]], to create a collection of jackets, handmade winter boots, fur caps, down vests, skateboard decks and T-shirts.  All pieces reflected an  outdoors/hunting theme and were sold exclusively at Huf in [[Los Angeles]] and Huf in [[San Francisco]].<ref>[http://www.highsnobiety.com/2008/10/29/huf-x-dr-romanelli-the-complete-collection/ Huf x Dr. Romanelli] HighSnobiety.com, Retrieved 13, February 2013</ref>

=== 2007 ===
*'''[[Looney Tunes]] - What's Up DRx'''-"What's Up DRX" depicted the [[Warner Bros. Cartoons|Warner Bros]]. [[Looney Tunes]] characters as mad scientists for a clothing line and limited edition toy designed by Romanelli.<ref>[http://www.whatsupdrx.com Looney Tunes: What's Up DRx] WhatsUpDrx.com, Retrieved 30, January, 2013</ref>
*'''DRx - Surgical Strike [[Installation art|Installation]] - Maharishi, [[London]]''' - (2007) - Romanelli and painter Andrew Brandou collaborated on an [[Installation art|installation]] for London retailer, Maharishi. Working together in a military theme, Romanelli and Brandou each created a body of work incorporating the culture of war from different angles. Inspired by his love of vintage military garments, Romanelli took a wide range of iconic uniform pieces from several countries and reimagined them as high-end luxury apparel far removed from their original [[Utilitarianism|utilitarian]] purposes. Brandou created a new series of works reinvisioning the world of legendary wildlife artist [[John James Audubon]] as populated by a new breed of fashion-conscious and heavily armed animals. The pieces created by Romanelli in this collection told a story that mixed with the artwork of Brandou's exhibition.<ref>[http://www.selectism.com/2007/11/08/dr-romanelli-x-andrew-brandou-maharishi/%20Selectism.com Dr. Romanelli x Andrew Brandou installation At Maharishi In London] Selectism.com, Retrieved 11, February, 2013</ref><ref>[http://www.styleguru.com/entry/surgical-strike-military-apparels-collide-with-paintbrush-at-londons-maharishi/ Surgical Strike: Military Apparels Collide at London's Maharishi] StyleGuru.com, Retrieved 11, February, 2013</ref><ref>[http://slamxhype.com/news/andrew-brandou-dr-romanelli-exhibition-at-maharishi/ Andrew Brandou / Dr. Romanelli Exhibition at Maharishi] SlamxHype.com, Retrieved 11, February, 2013</ref>

=== 2006 ===
*'''[[Jaeger LeCoultre]] - Reverso Romanelli'''-  Romanelli designed a limited edition watch for the luxury watch brand accompanied by a black and white leather jacket which integrated a wrist attachment for the watch.<ref>[http://highsnobiety.blogspot.com/2006/01/dr-romanelli-x-jaeger-lecoultre-launch.html Dr. Romanelli x Jaegar LeCoultre Launch], HighSnobiety.com, Retrieved January 30, 2013</ref>
*'''"[[Black Sabbath]] Resurrection"'''-  A limited edition line of accessories inspired by the [[Heavy metal music|heavy metal]] band [[Black Sabbath]].  The "Black Sabbath Resurrection" collection was created by [[Black Sabbath]] in collaboration with Romanelli and in partnership with [[Live Nation Entertainment|Signatures Network]].  Romanelli wanted to pay tribute to,"one of the most iconic rock bands of all time," and also create products that capture the authenticity and impact of the band that would attract both a new generation and long-time fans.<ref>[http://www.prnewswire.com/news-releases/black-sabbath-resurrection-reveals-their-historic-untold-story-through-retrospective-art-and-new-black-sabbath-inspired-fashion-items-56460977.html 'Black Sabbath Resurrection' Reveals Their Historic Untold Story Through Retrospective Art and New Black Sabbath Inspired Fashion Items]</ref><ref>[http://www.blacksabbathresurrection.com/index_scale.html Black Sabbath Resurrection Website] BlackSabbathResurrection.com, Retrieved 7, February 2013</ref>

=== 2004 ===
* '''[[Nike, Inc.|Nike]] - RECONSTRUCT Project''' - In 2004 [[Nike, Inc.|Nike]] approached various designers throughout [[Los Angeles]] and gave them 24 hours to work with a box of [[Nike, Inc.|Nike]] scraps off the cutting room floor. Romanelli's creations for RECONSTRUCT inspired a DR. Romanelli / Nike collaboration, creating intricate patchwork jackets from vintage [[Nike, Inc.|Nike]] jumpers. This [[Nike, Inc.|Nike]] / DR. Romanelli collaboration is ongoing.<ref>[http://walklikeus.com/wlu/?p=1022 Fashion: DRx.Romanelli Surgical Strike Nike Jacket] WalkLikeUs.com, Retrieved 8, February 2013</ref><ref>[http://citifiable.wordpress.com/tag/drx/ DrX: Dr. Romanelli x Nike Sportswear Destroyer Jacket] Citifiable.com, Retrieved 7, February 2013</ref>

=== 1999 ===
* '''Sophnet - F.C.R.B Collection'''-  The F.C.R.B collection for Sophnet, an Asian [[retailer]], is reconstructed cut and sewn T-shirts, polos, and jackets designed by Romanelli. This collaboration has been ongoing and can only be found at the retailer in [[Japan]].<ref>[http://www.virtualjapan.com/wiki/Sophnet About Sophnet] VirtualJapan.com, Retrieved 7, February 2013</ref><ref>[http://hypebeast.com/2006/12/dr-romanelli-f-c-r-b-collection-sophnet/ Dr. Romanelli FCRB Collection for Sophnet]</ref>

== Marketer ==
[[File:Pancake Epidemic.jpg|thumb|right|The Pancake Epidemic, Est. 2012]]
'''StreetVirus''' is a marketing agency in which Romanelli is co-owner. The agencies client roster has included; [[Disney]], [[FOX]], [[Warner Bros.]], [[Converse (shoe company)|Converse]], and Grenco Science the makers of the G Pen.<ref>[http://agenda.transworld.net/2013/08/12/agenda-las-vegas-grenco-carnival/ AGENDA Las Vegas GRENCO Carnival, Agenda Transworld, Aug. 2013, Retrieved Feb. 2014]</ref> Romanelli and StreetVirus have aligned brands with specialty boutiques in cities around the world such as; Maxfield Department Store in [[Los Angeles]], [[Colette]] in [[Paris]], and [[Barneys]] in [[Tokyo]].<ref>[http://graphicnovelreporter.com/content/beetle-new-generation-feature-stories Interview: "What Is The Difference Between Dr. Romanelli and StreetVirus?]" Graphic Novel Reporter, October 2012, Retrieved 8 November 2012</ref>

'''The Pancake Epidemic''', a division of StreetVirus, is a creative agency which focuses strictly on content creation. The offices are located directly above the iconic [[IHOP]] in the [[Miracle Mile, Los Angeles|Miracle Mile]] district of [[Los Angeles]] and officially opened its doors in September, 2012.<ref>[http://thehundreds.com/blog/2012/09/18/the-pancake-epidemic/%20 The Pancake Epidemic], TheHundreds.com, September 2012, Retrieved 8 November 2012</ref>

'''Blank You Very Much''' is an online crowd-sourcing design platform that consists of nearly 50,000 graphic designers from around the world. Curated by Romanelli and StreetVirus, BYVM offers global brands the opportunity to engage their audience through the lens of art and design. BYVM launched in 2012 and has worked with iconic sport and lifestyle brands including, [[Coca-Cola Company|Coca-Cola]], [[Burton Snowboards]], [http://www.hufworldwide.com/ HUF], and [[SoBe|SOBE]], among others.<ref>[http://hypebeast.com/2013/12/darren-romanelli-discusses-blank-you-very-much Darren Romanelli Discusses Blank You Very Much, HypeBeast.com, Dec. 2013, Retrieved Feb. 2014]</ref>

== Director==
In 2011, Romanelli directed, "Dawned On Me", a music video from American alternative rock band [[Wilco]].  The song came off their [[Grammy]] nominated album, "The Whole Love", which was released in September 2011. This was the first video for the band since 1999. Romanelli conceived the idea of the video - a black-and-white vintage cartoon which features [[Popeye]] and the likeness of  [[Wilco]] frontman, [[Jeff Tweedy]], along with the rest of the band. The video debuted in January 2012 and this collaboration with [[King Features]] whom sydicates the comic strip, presents the first hand-drawn [[Popeye]] cartoon in more than 30 years.<ref>[http://www.washingtonpost.com/blogs/comic-riffs/post/when-popeye-met-wilco/2012/01/26/gIQAcaQGVQ_blog.html When Popeye Met Wilco] Washington Post, January 2012, Retrieved 8 November 2012</ref>

In 2012, Romanelli was called on to direct another music video, this time for [[Los Angeles]] based singer, [[Ryan McDermott]] whom at the time had recently signed to [[Kanye West|Kanye West's]] [[G.O.O.D. Music|G.O.O.D Music]]. The video was for McDermott's first single, "Paradise", and came off his album,  "Ryan vs. The Sandman."

In 2013, Romanelli directed a music video for [[Hip hop music|hip-hop]] artist [[Kendrick Lamar]] featuring artist Eddie Peake. The video was a two-part series for Lamar's song, "Sing About Me (Part 1)" off his album, [[Good Kid, M.A.A.D City]]. Part one was released in December and Romanelli conceptualized the idea between Lamar and Peake, two artists coming together to, "lay ground to their legacy".<ref>[http://www.dubcnn.com/2013/12/22/kendrick-lamar-and-eddie-peake-sing-about-me-part-1/ Kendrick Lamar and Eddie Peake - "Sing About Me" Part 1, Dubcnn.com, Dec. 2013, Retrieved Feb. 2014]</ref>  The video starts with Kendrick driving around his hometown, [[Compton, California|Compton]]. From there, the scene shifts to a warehouse where Peake begins to work on a spraypainting piece to correspond with the song and ends with Lamar entering the warehouse to look at the painting. Satisfied with what he sees, he turns and walks away.<ref>[http://www.complex.com/art-design/2014/02/the-best-artist-directed-music-videos/sing-about-me The Best Artist Directed Music Videos, Complex.com, Feb. 2014, Retrieved Feb. 2014]</ref> Though a simple premise, Romanelli's goal was to  reinforce the ‘stream-of-consciousness’ style of the song itself.<ref>[http://hypebeast.com/2013/12/kendrick-lamar-sing-about-me-pt-1-video Kendrick Lama's "Sing About Me", PT. 1 Video, Hypebeast, Dec. 2013, Retrieved Feb. 2014]</ref>

== References ==
{{Reflist|3}}

== External links ==
* [http://www.drromanelli.com Dr. Romanelli Official Website]
* [http://drromanelli.com/blog Dr. Romanelli Official Blog]
* [http://www.StreetVirus.com StreetVirus Official Website]
* [http://thepancakeepidemic.com/ The Pancake Epidemic Official Website]

<!-- This will add a notice to the bottom of the page and won't blank it! The new template which says that your draft is waiting for a review will appear at the bottom; simply ignore the old (grey) drafted templates and the old (red) decline templates. A bot will update your article submission. Until then, please don't change anything in this text box and press "Save page". -->

{{DEFAULTSORT:Romanelli, Darren}}
[[Category:Living people]]
[[Category:American fashion designers]]
[[Category:American furniture designers]]
[[Category:American music video directors]]
[[Category:American art directors]]