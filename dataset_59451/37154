{{good article}}
{{Taxobox
| name = Banded houndshark
| image = Triakis scyllium himeji.jpg
| status = LC
| status_system = iucn3.1
| status_ref = <ref name="iucn"/>
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Chondrichthyes]]
| subclassis = [[Elasmobranchii]]
| superordo = [[Shark|Selachimorpha]]
| ordo = [[Carcharhiniformes]]
| familia = [[Houndshark|Triakidae]]
| genus = ''[[Triakis]]''
| species = '''''T. scyllium'''''
| binomial = ''Triakis scyllium''
| binomial_authority = [[Johannes Peter Müller|J. P. Müller]] & [[Friedrich Gustav Jakob Henle|Henle]], 1839
| range_map = Triakis scyllium distmap.png
| range_map_caption = Range of the banded houndshark<ref name="compagno"/>
| synonyms = ''Hemigaleus pingi'' <small>Evermann & Shaw, 1927</small>
}}

The '''banded houndshark''' (''Triakis scyllium'') is a [[species]] of [[houndshark]], in the [[family (biology)|family]] [[Houndshark|Triakidae]], common in the northwestern [[Pacific Ocean]] from the southern [[Russian Far East]] to [[Taiwan]]. Found [[benthic fish|on or near the bottom]], it favors shallow coastal [[habitat]]s with sandy or vegetated bottoms, and also enters [[brackish water]]. This shark reaches {{convert|1.5|m|ft|abbr=on}} in length. It has a short, rounded snout and mostly narrow fins; the [[pectoral fin]]s are broad and triangular, and the trailing margin of the first [[dorsal fin]] is almost vertical. It is gray above and lighter below; younger sharks have darker saddles and dots, which fade with age.

[[Nocturnal]] and largely solitary, the banded houndshark preys on [[benthic]] [[invertebrate]]s and [[bony fish]]es. It is [[aplacental viviparous]], with the developing [[embryo]]s sustained by [[yolk]]. After [[mating]] during summer, females bear as many as 42 pups following a [[gestation period]] of 9–12 months. The banded houndshark poses no danger to humans and adapts well to captivity. It is caught as [[bycatch]] off [[Japan]], [[Taiwan]], and likely elsewhere in its range; it may be eaten but is not as well-regarded as related species. Because fishing does not appear to have diminished this shark's population, the [[International Union for Conservation of Nature]] (IUCN) has listed it under [[Least Concern]].

==Taxonomy==
The first scientific description of the banded houndshark was authored by German biologists [[Johannes Peter Müller]] and [[Friedrich Gustav Jakob Henle]], based on a dried specimen from [[Japan]], in their 1838–41 ''Systematische Beschreibung der Plagiostomen''. They gave it the [[specific name (zoology)|specific epithet]] ''scyllium'', derived from the [[Ancient Greek]] ''skylion'' ("dogfish"), and placed it in the genus ''Triakis''.<ref name="muller and henle"/> Within the genus, it is placed in the subgenus ''Triakis'' along with the [[leopard shark]] (''T. (Triakis) semifasciata'').<ref name="compagno"/>

==Distribution and habitat==
Native to the northwestern [[Pacific Ocean]], the banded houndshark occurs from the southern [[Russian Far East]] to [[Taiwan]], including Japan, [[Korea]], and eastern [[China]]; records from the [[Philippines]] are questionable.<ref name="iucn"/> This common, [[benthic fish|benthic]] shark is found over [[continental shelf|continental and insular shelves]], mostly close to shore but also to a depth of {{convert|150|m|ft|abbr=on}}.<ref name="hennemann"/> It frequents sandy flats and beds of [[seaweed]] and [[Zostera|eelgrass]]; additionally it is tolerant of [[brackish water]] and enters [[estuaries]] and [[bay]]s.<ref name="iucn"/>

==Description==
[[File:Banded houndshark osaka.jpg|thumb|left|The markings of the banded houndshark become indistinct with age.]]
The banded houndshark is a moderately slender-bodied species growing up to {{convert|1.5|m|ft|abbr=on}} long. The snout is short, broad, and rounded; the widely separated nostrils are each preceded by a lobe of skin that does not reach the mouth. The horizontally oval eyes are placed high on the head; they are equipped with rudimentary [[nictitating membrane]]s (protective third eyelids) and have prominent ridges underneath. The mouth forms a short, wide arch and bears long furrows at the corners that extend onto both jaws. Each tooth has an upright to oblique knife-like central cusp flanked by strong cusplets. There are five pairs of [[gill slit]]s.<ref name="compagno"/>

Most of the fins are fairly narrow; in adults the [[pectoral fin]]s are broad and roughly triangular. The moderately tall first [[dorsal fin]] is placed about halfway between the pectoral and [[pelvic fin]]s, and its trailing margin is nearly vertical near the apex. The second [[dorsal fin]] is about three-quarters as high as the first and larger than the [[anal fin]]. The [[caudal fin]] has a well-developed lower lobe and a prominent ventral notch near the tip of the upper lobe; in young sharks the lower caudal fin lobe is much less distinct.<ref name="compagno"/> This species is gray above, with darker saddles and scattered black spots that fade with age; the underside is off-white.<ref name="hennemann"/>

==Biology and ecology==
[[File:Echiura in Korea1.jpg|thumb|Spoon worms are an important food source for small banded houndsharks.]]
The banded houndshark is [[nocturnal]] and generally solitary, though several individuals may rest together, sometimes piled atop one another inside a cave.<ref name="hennemann"/><ref name="michael"/> It feeds mainly on [[crustacean]]s (including [[shrimp]]s, [[crab]]s, [[hermit crab]]s, and [[mantis shrimp]]s), [[cephalopod]]s (including [[octopus]]), and [[Echiura|spoon worms]]; [[polychaete worm]]s, [[tunicate]]s, [[Sipunculida|peanut worms]], and small, bottom-living [[bony fish]]es (including [[flatfish]]es, [[Congridae|conger eel]]s, [[herring]], [[Carangidae|jacks]], [[Sciaenidae|drums]], and [[Haemulidae|grunts]]) are occasionally consumed. Shrimp and spoon worms are important prey for sharks up to {{convert|70|cm|in|abbr=on}} long; cephalopods predominate in the diets of larger sharks.<ref name="kamura and hashimoto"/>

[[Mating]] occurs during the summer, and involves the male swimming parallel to the female and gripping her pectoral fin with his teeth; thus secured, he then twists the aft portion of his body to insert a single [[clasper]] into her [[cloaca]] for [[copulation]]. The banded houndshark is [[aplacental viviparous]], in which the developing [[embryo]]s are sustained to birth by [[yolk]]. Females bear litters of 9–26 pups after a [[gestation period]] of 9–12 months, though litters as large as 42 pups have been recorded.<ref name="michael"/><ref name="ni et al"/><ref name="michael2"/> The newborns measure {{convert|18|-|20|cm|in|abbr=on}} long. Males [[sexual maturation|mature sexually]] at 5–6 years old, when they are {{convert|93|-|106|cm|in|abbr=on}} long, and live up to 15 years. Females mature sexually at 6–7 years old, when they are {{convert|106|-|107|cm|abbr=on}} long, and live up to 18 years.<ref name="iucn"/> Known [[parasite]]s of this species include the [[tapeworm]]s ''Callitetrarhynchus gracilis'',<ref name="williams and jones"/> ''Onchobothrium triacis'', and ''Phyllobothrium serratum'',<ref name="yamaguti"/> the [[leech]] ''Stibarobdella macrothela'',<ref name="yamauchi et al"/> and the [[copepod]]s ''Achtheinus impenderus'',<ref name="shen and wang"/> ''Caligus punctatus'',<ref name="boxshall and defaye"/> ''Kroyeria triakos'',<ref name="izawa"/> and ''Pseudopandarus scyllii''.<ref name="yamaguti and yamasu"/>

==Human interactions==
[[File:Banded houndshark china.jpg|thumb|Live banded houndshark on display at a restaurant in China.]]
Harmless to humans,<ref name="fishbase"/> the banded houndshark is commonly displayed in [[public aquarium]]s in China and Japan,<ref name="iucn"/> and has reproduced in captivity.<ref name="michael2"/> Individuals have survived in captivity for over five years.<ref name="michael"/> This species is often [[bycatch|caught incidentally]] off Japan in [[gillnet]]s and [[fishing net|set nets]]; the meat is sometimes sold, but is considered to be of poorer quality than that of other houndsharks in the region. It is caught in lesser numbers off [[Taiwan]], and is probably also fished off Korea and northern China. The [[International Union for Conservation of Nature]] (IUCN) has listed the banded houndshark under [[Least Concern]], as it remains abundant throughout its range. Off Japan, it can be found in rocky areas that provide refuge from fishing pressure.<ref name="iucn"/>

==See also==
{{Portal|Sharks}}

*[[List of sharks]]

==References==
{{reflist|colwidth=30em|refs=

<ref name="boxshall and defaye">{{cite book |title=Pathogens of Wild and Farmed Fish: Sea Lice |editor1=Boxshall, G.A. |editor2=Defaye, D. |publisher=CRC Press |year=1993 |isbn=0-13-015504-7 |page=16}}</ref>

<ref name="compagno">{{cite book |author=Compagno, L.J.V. |year=1984 |title=Sharks of the World: An Annotated and Illustrated Catalogue of Shark Species Known to Date |place=Rome |publisher=Food and Agricultural Organization |isbn=92-5-101384-5 |page=432}}</ref>

<ref name="fishbase">{{fishbase species |genus=Triakis |species=scyllium |month=May |year=2011}}</ref>

<ref name="hennemann">{{cite book |author=Hennemann, R.M. |title=Sharks & Rays: Elasmobranch Guide of the World |publisher=IKAN &ndash; Unterwasserarchiv |edition=second |year=2001 |isbn=3-925919-33-3 |pages=113}}</ref>

<ref name="iucn">{{IUCN2010.4 |assessor=Ebert, D.A. |assessor2=Tanaka, S. |assessor3=Nakaya, K. |title=Triakis scyllium |year=2007 |id=161395 |downloaded=July 16, 2011}}</ref>

<ref name="izawa">{{cite journal |title=Redescription of four species of ''Kroyeria'' and ''Kroeyerina'' (Copepoda, Siphonostomatoida, Kroyeriidae) infecting Japanese sharks |author=Izawa, K. |journal=Crustaceana |volume=81 |issue=6 |year=2008 |pages=695–724 |url=http://www.ingentaconnect.com/content/brill/cr/2008/00000081/00000006/art00006 |doi=10.1163/156854008784513465}}</ref>

<ref name="kamura and hashimoto">{{cite journal |author1=Kamura, S. |author2=Hashimoto, H. |year=2004 |title=The food habits of four species of triakid sharks, ''Triakis scyllium'', ''Hemitriakis japanica'', ''Mustelus griseus'' and ''Mustelus manazo'', in the central Seto Inland Sea, Japan |journal=Fisheries Science |volume=70 |pages=1019–1035 |doi=10.1111/j.1444-2906.2004.00902.x}}</ref>

<ref name="michael">{{cite book |author=Michael, S.W. |year=1993 |title=Reef Sharks & Rays of the World |publisher=Sea Challengers |page=59 |isbn=0-930118-18-9}}</ref>

<ref name="michael2">{{cite book |author=Michael, S.W. |title=Aquarium Sharks & Rays |publisher=T.F.H. Publications |year=2001 |isbn=1-890087-57-2 |page=229}}</ref>

<ref name="muller and henle">{{cite book |author1=Müller, J.  |author2=F.G.J. Henle  |lastauthoramp=yes |year=1838–41 |title=Systematische Beschreibung der Plagiostomen |publisher=Veit und Comp |pages=63–64}}</ref>

<ref name="ni et al">{{cite journal |title=Preliminary observation on the feeding habits and reproduction of ''Triakis scyllium'' |author1=Ni, J. |author2=Li, J. |author3=Xu, Y. |journal=Journal of Oceanography of Huanghai & Bohai Seas |volume=10 |issue=1 |pages=42–46 |year=1992 |url=http://en.cnki.com.cn/Article_en/CJFDTOTAL-HBHH199201005.htm}}</ref>

<ref name="shen and wang">{{cite journal |title=A new parasitic copepod, ''Achtheinus impenderus'' (Coligoida, Pandaridae), from a shark taken at Peitaiho, Hopei Province |author1=Shen, C.J. |author2=Wang, K.N. |journal=Acta Zoologica Sinica |volume=10 |issue=1 |pages=27–31  |year=1959}}</ref>

<ref name="williams and jones">{{cite book |title=Parasitic Worms of Fish |author1=Williams, H.H. |author2=Jones, A. |publisher=CRC Press |year=1994 |isbn=0-85066-425-X |page=390}}</ref>

<ref name="yamaguti">{{cite journal |author=Yamaguti, S. |author-link=Satyu Yamaguti |year=1952 |title=Studies on the helminth fauna of Japan. Part 49. Cestodes of fishes, II |journal=Acta Medicinae Okayama |volume=8 |pages=1–76 |url=http://ousar.lib.okayama-u.ac.jp/Detail.e?id=3188220080109182233}}</ref>

<ref name="yamaguti and yamasu">{{cite journal |title=Parasitic copepods from fishes of Japan with descriptions of 26 new species and remarks on two known species |author=Yamaguti, S. |author-link=Satyu Yamaguti |author2=Yamasu, T. |journal=Biological Journal of Okayama University |volume=5 |issue=3/4 |pages=89–165 |year=1959}}</ref>

<ref name="yamauchi et al">{{cite journal |title=''Stibarobdella macrothela'' (Annelida, Hirudinida, Piscicolidae) from Elasmobranchs in Japanese Waters, with New Host Records |author1=Yamauchi, T. |author2=Ota, Y. |author3=Nagasawa, K. |journal=Biogeography |volume=10 |pages=53–57 |date=August 20, 2008 |url=http://ci.nii.ac.jp/naid/10024482948/en}}</ref>
}}

{{Triakidae}}

[[Category:Triakidae]]
[[Category:Animals described in 1839]]