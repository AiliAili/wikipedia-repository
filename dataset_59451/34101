{{Distinguish | Ulysses (novel)}}
{{Use dmy dates|date=March 2013}}
[[File:Alfred Tennyson, 1st Baron Tennyson by George Frederic Watts.jpg|thumb|right|[[Alfred, Lord Tennyson]], author of "Ulysses", portrayed by [[George Frederic Watts]]]]

"'''Ulysses'''" is a poem in [[blank verse]] by the [[Victorian era|Victorian]] poet [[Alfred, Lord Tennyson]] (1809–1892), written in 1833 and published in 1842 in his well-received [[Poems (Tennyson, 1842)|second volume of poetry]]. An oft-quoted poem, it is popularly used to illustrate the [[dramatic monologue]] form. Facing old age, mythical hero [[Odysseus|Ulysses]] describes his discontent and restlessness upon returning to his kingdom, [[Ithaca]], after his far-ranging travels. Despite his reunion with his wife [[Penelope]] and son [[Telemachus]], Ulysses yearns to explore again.

The character of Ulysses (in [[Greek language|Greek]], [[Odysseus]]) has been explored widely in literature. The adventures of Odysseus were first recorded in [[Homer]]'s ''[[Iliad]]'' and ''[[Odyssey]]'' (c. 800–700 BC), and Tennyson draws on Homer's narrative in the poem. Most critics, however, find that Tennyson's Ulysses recalls [[Dante Alighieri|Dante's]] Ulisse in his ''[[The Divine Comedy#Inferno|Inferno]]'' (c. 1320). In Dante's re-telling, Ulisse is condemned to hell among the false counsellors, both for his pursuit of knowledge beyond human bounds and for his adventures in disregard of his family.

For much of this poem's history, readers viewed Ulysses as resolute and heroic, admiring him for his determination "To strive, to seek, to find, and not to yield".<ref>Pettigrew, p. 28.</ref> The view that Tennyson intended a heroic character is supported by his statements about the poem, and by the events in his life—the death of his closest friend—that prompted him to write it. In the twentieth century, some new interpretations of "Ulysses" highlighted potential [[irony|ironies]] in the poem. They argued, for example, that Ulysses wishes to selfishly abandon his kingdom and family, and they questioned more positive assessments of Ulysses' character by demonstrating how he resembles flawed protagonists in earlier literature.

==Synopsis and structure==
As the poem begins, Ulysses has returned to his kingdom, Ithaca, having made a long journey home after fighting in the [[Trojan War]]. Confronted again by domestic life, Ulysses expresses his lack of contentment, including his indifference toward the "savage race" (line 4) whom he governs. Ulysses contrasts his present restlessness with his heroic past, and contemplates his old age and eventual death—"Life piled on life / Were all too little, and of one to me / Little remains" (24–26)—and longs for further experience and knowledge. His son Telemachus will inherit the throne that Ulysses finds burdensome. While Ulysses thinks that Telemachus will be a good king—"Most blameless is he, centred in the sphere / Of common duties" (39)—he seems to have lost any connection to his son—"He works his work, I mine" (43)—and the conventional methods of governing—"by slow prudence" and "through soft degrees" (36, 37). In the final section, Ulysses turns to his fellow mariners and calls on them to join him on another quest, making no guarantees as to their fate but attempting to conjure their heroic past:

<poem>
                  … Come, my friends,
     'Tis not too late to seek a newer world.
     Push off, and sitting well in order smite
     The sounding furrows; for my purpose holds
     To sail beyond the sunset, and the baths
     Of all the western stars, until I die.
     It may be that the gulfs will wash us down;
     It may be we shall touch the Happy Isles,
     And see the great Achilles, whom we knew. <small>(56–64)</small>
</poem>

=== Prosody ===

The speaker's language is unadorned but forceful, and it expresses Ulysses' conflicting moods as he searches for continuity between his past and future.  There is often a marked contrast between the sentiment of Ulysses' words and the sounds that express them.<ref>{{cite book|title=Victorian Poetry: An Annotated Anthology|author=O'Gorman, Francis|publisher=Blackwell Publishing|year=2004|isbn=0-631-23436-5|pages=85}}</ref> For example, the poem's insistent [[iambic pentameter]] is often interrupted by [[spondee]]s ([[metrical feet]] that consist of two long syllables); such laboured language slows the poem (and in other places may cast doubt upon the [[unreliable narrator|reliability]] of Ulysses' utterances):

<poem>
     Yet all experience is an arch wherethro'
     Gleams that untravell'd world, whose margin fades
     For ever and for ever when I move. <small>(19–21)</small>
</poem>

Observing their burdensome [[prosodic]] effect, the poet [[Matthew Arnold]] remarked, "these three lines by themselves take up nearly as much time as a whole book of the ''Iliad''."<ref>Quoted in Markley, p. 125.</ref>
Many of the poem's clauses carry over into the following line; these [[enjambment]]s emphasize Ulysses' restlessness and dissatisfaction.<ref>{{cite journal|title=The Three Modes in Tennyson's Prosody|author=Ostriker, Alicia | journal=PMLA|volume=82|issue=2|date=May 1967|pages=273–284|doi=10.2307/461298|jstor=461298|publisher=Modern Language Association}}</ref>

=== Form ===

The poem's seventy lines of [[blank verse]] are presented as a [[dramatic monologue]]. Scholars disagree on how Ulysses' speech functions in this format; it is not necessarily clear to whom Ulysses is speaking, if anyone, and from what location. Some see the verse turning from a [[soliloquy]] to a public address, as Ulysses seems to speak to himself in the first movement, then to turn to an audience as he introduces his son, and then to relocate to the seashore where he addresses his mariners.<ref>Hughes, p. 201.</ref> In this interpretation, the comparatively direct and honest language of the first movement is set against the more politically minded tone of the last two movements. For example, the second paragraph (33–43) about Telemachus, in which Ulysses muses again about domestic life, is a "revised version [of lines 1–5] for public consumption":<ref name="p41"/> a "savage race" is revised to a "rugged people".

The ironic interpretations of "Ulysses" may be the result of the modern tendency to consider the narrator of a dramatic monologue as necessarily "[[unreliable narrator|unreliable]]". According to critic Dwight Culler, the poem has been a victim of revisionist readings in which the reader expects to reconstruct the truth from a misleading narrator's accidental revelations.<ref>Culler, p. 368.</ref> (Compare the more obvious use of this approach in Robert Browning's "[[My Last Duchess]]".) Culler himself views "Ulysses" as a [[dialectic]] in which the speaker weighs the virtues of a contemplative and an active approach to life;<ref>Markley, p. 126.</ref> Ulysses moves through four emotional stages that are self-revelatory, not ironic: beginning with his rejection of the barren life to which he has returned in Ithaca, he then fondly recalls his heroic past, recognizes the validity of Telemachus' method of governing, and with these thoughts plans another journey.<ref>Culler, p. 383.</ref>

=== Publication history ===

Tennyson completed the poem on 20 October 1833,<ref>A.T. Tennyson and A . Day, ''Alfred Lord Tennyson: selected poems'' (1991), 360.</ref> but it was not published until 1842, in his second collection of ''[[Poems (Tennyson, 1842)|Poems]]''. Unlike many of Tennyson's other important poems, "Ulysses" was not revised after its publication.<ref>{{cite journal|title=The Classical Poems of Tennyson|author=Cressman, Edmund D.|journal=The Classical Journal|volume=24|issue=2|date=Nov 1928|pages=98–111}}</ref>

Tennyson originally blocked out the poem in four paragraphs, broken before lines 6, 33 and 44. In this structure, the first and third paragraphs are thematically parallel, but may be read as interior and exterior [[monologue]]s, respectively. However, the poem is often printed with the first paragraph break omitted.<ref name="p41">Pettigrew, p. 41.</ref>

==Interpretations==

===Autobiographical elements===

Tennyson penned "Ulysses" after the death of his close [[University of Cambridge|Cambridge]] friend, the poet [[Arthur Henry Hallam]] (1811–1833), with whom Tennyson had a strong emotional bond. The two friends had spent much time discussing poetry and philosophy, writing verse, and travelling in [[southern France]], the [[Pyrenees]], and Germany. Tennyson considered Hallam destined for greatness, perhaps as a statesman.<ref>Hughes, p. 197.</ref>

When Tennyson heard on 1 October 1833 of his friend's death, he was living in [[Somersby, Lincolnshire|Somersby]], Lincolnshire, in cramped quarters with his mother and nine of his ten siblings. His father had died in 1831, requiring Tennyson to return home and take responsibility for the family. Tennyson's friends were becoming increasingly concerned about his mental and physical health during this time. The family had little income, and three of Tennyson's brothers were mentally ill. Just as Tennyson's outlook was improving—he was adjusting to his new domestic duties, regaining contact with friends, and had published his 1832 book of poems—the news of Hallam's death arrived. Tennyson shared his grief with his sister, [[Emilia Tennyson|Emily]], who had been engaged to Hallam.

According to Victorian scholar Linda Hughes, the emotional gulf between the state of his domestic affairs and the loss of his special friendship informs the reading of "Ulysses"—particularly its treatment of domesticity.<ref name="hughes">Hughes.</ref> At one moment, Ulysses' discontent seems to mirror that of Tennyson, who would have been frustrated with managing the house in such a state of grief. At the next, Ulysses is determined to transcend his age and his environment by travelling again. It may be that Ulysses' determination to defy circumstance attracted Tennyson to the myth;<ref>Hughes, p. 199.</ref> he said that the poem "gave my feeling about the need of going forward and braving the struggle of life".<ref>Widely quoted; see for example, A. T. Tennyson and A. Roberts (2000). ''Alfred Tennyson. The Oxford authors.'' Oxford: Oxford University Press. ISBN 0-19-210016-5. p. 567.</ref> On another occasion, the poet stated, "There is more about myself in ''Ulysses'', which was written under the sense of loss and that all had gone by, but that still life must be fought out to the end. It was more written with the feeling of his loss upon me than many poems in ''In Memoriam''."<ref>Quoted in Hughes, p. 195.</ref> Hallam's death influenced much of Tennyson's poetry, including perhaps his most highly regarded work, ''[[In Memoriam A.H.H.]]'', begun in 1833 and completed seventeen years later.

Other critics find stylistic incongruities between the poem and its author that make "Ulysses" exceptional. W. W. Robson writes, "Tennyson, the responsible social being, the admirably serious and 'committed' individual, is uttering strenuous sentiments in the accent of Tennyson the most un-strenuous, lonely and poignant of poets."<ref name="robson">Robson, W. W. (1957). "The Dilemma of Tennyson". In Killham (1960), p. 159.</ref> He finds that Tennyson's two widely noted personae, the "responsible social being" and the melancholic poet, meet uniquely in "Ulysses", yet seem not to recognize each other within the text.

===Literary context===

Tennyson adopts aspects of the Ulysses character and narrative from many sources; his treatment of Ulysses is the first modern account.<ref name="Stanford202">Stanford, p. 202.</ref> The [[ancient Greek]] poet [[Homer]] introduced Ulysses ([[Odysseus]] in Greek<ref>The word "Ulysses" (more correctly "Ulixes") is the Latin form of the Greek "[[Odysseus]]", source of the word "odyssey".</ref>), and many later poets took up the character, including [[Euripides]],<ref>Tennyson wrote in the margin of [[Euripides]]' ''Hecabe'', "Ulysses is, as usual, crafty [and] unfeeling." (Markley, p. 123).</ref> [[Horace]], [[Dante]], [[William Shakespeare]], and [[Alexander Pope]].  Homer's ''Odyssey'' provides the poem's narrative background: in its eleventh book the prophet [[Tiresias]] foretells that Ulysses will return to Ithaca after a difficult voyage, then begin a new, mysterious voyage, and later die a peaceful, "unwarlike" death that comes vaguely "from the sea". At the conclusion of Tennyson's poem, his Ulysses is contemplating undertaking this new voyage.

[[File:Blake Dante Hell XXVI Ulisses.jpg|thumb|right|150px|In one of [[William Blake]]'s [[watercolor painting|watercolors]] (1824–1827) illustrating [[Dante]]'s ''[[Inferno (Dante)|Inferno]]'', [[Odysseus|Ulysses]] and [[Diomedes]] are condemned to the [[Inferno (Dante)#Eighth Circle (Fraud)|Eighth Circle]].<ref>"[http://www.ngv.vic.gov.au/collection/international/print/b/blake/ipd00062.html Ulysses and Diomed Swathed in the Same Flame, 1824–1827]". [[National Gallery of Victoria]], 1999. Retrieved on 21 October 2007.</ref>]]

Tennyson's character, however, is not the lover of public affairs seen in Homer's poems. Rather, "Ulisse" from [[Dante]]'s ''[[The Divine Comedy#Inferno|Inferno]]'' is Tennyson's main source for the character,<ref>Pettigrew, p. 31.</ref> which has an important effect on the poem's interpretation. Ulisse recalls his voyage in the ''Inferno'''s 26th [[canto]], in which he is condemned to the Eighth Circle of false counsellors for misusing his gift of reason. Dante treats Ulisse, with his "zeal …/ T'explore the world", as an evil counsellor who lusts for adventure at the expense of his family and his duties in Ithaca.<ref>Rowlinson (1994), p. 241.</ref> Tennyson projects this zeal into Ulysses' unquenched desire for knowledge:<ref>Schwarz, D. R. (1987). ''Reading Joyce's Ulysses.'' New York: St. Martin's Press, p. 39. ISBN 0-312-66458-3.</ref>

<poem>
     And this gray spirit yearning in desire
     To follow knowledge like a sinking star,
     Beyond the utmost bound of human thought. <small>(30–32)</small>
</poem>

The poet's intention to recall the Homeric character remains evident in certain passages. "I am become a name" (11) recalls an episode in the ''Odyssey'' in which [[Demodocus (Homer)|Demodocus]] sings about Odysseus' adventures in the king's presence, acknowledging his fame. With phrases such as "There gloom the dark broad seas" (45) and "The deep / Moans round with many voices" (55–56), Tennyson seems to be consciously invoking Homer.<ref>Markley, p. 124.</ref>

Critics have also noted the influence of Shakespeare in two passages. In the early movement, the savage race "That hoard, and sleep, and feed, and know not me" (5) echoes [[Prince Hamlet|Hamlet's]] [[soliloquy]]: "What is a man, / If his chief good and market of his time / Be but to sleep and feed? A beast, no more."<ref>IV, iv. 32 ff., quoted in Bush, Douglas (Jan 1943). "Tennyson's 'Ulysses' and 'Hamlet'." ''The Modern Language Review'', 38(1), 38.</ref> Tennyson's "How dull it is to pause, to make an end, / To rust unburnish’d, not to shine in use!" (22–23) recalls Shakespeare's Ulysses in ''[[Troilus and Cressida]]'' (c. 1602):<ref>''Troilus and Cressida'', Act III, Scene III, quoted in Stanford, p. 203.</ref>
<poem>
     Perseverance, my dear lord,
     Keeps honour bright: to have done is to hang
     Quite out of fashion, like a rusty mail,
     In monumental mockery.
</poem>

The last movement of "Ulysses", which is among the most familiar passages in nineteenth-century English-language poetry, presents decisive evidence of the influence of Dante.<ref>i) Robson, W. W. (1957). "The Dilemma of Tennyson". In Killham (1960). ii) Stanford.</ref>  Ulysses turns his attention from himself and his kingdom and speaks of ports, seas, and his mariners. The strains of discontent and weakness in old age remain throughout the poem, but Tennyson finally leaves Ulysses "To strive, to seek, to find, and not to yield" (70), recalling the Dantesque damnable desire for knowledge beyond all bounds. The words of Dante's character as he exhorts his men to the journey find parallel in those of Tennyson's Ulysses, who calls his men to join him on one last voyage. Quoting Dante's Ulisse:

<poem>
     'O brothers', said I, 'who are come despite
     Ten thousand perils to the West, let none,
     While still our senses hold the vigil slight
     Remaining to us ere our course is run,
     Be willing to forgo experience
     Of the unpeopled world beyond the sun.
     Regard your origin,—from whom and whence!
     Not to exist like brutes, but made were ye
     To follow virtue and intelligence'.<ref>Dante, ''Inferno'' XXVI; quoted in Robson, W. W. (1957). "The Dilemma of Tennyson". In Killham (1960).</ref>
</poem>

However, critics note that in the Homeric narrative, Ulysses' original mariners are dead. A significant irony therefore develops from Ulysses' speech to his sailors—"Come, my friends, / 'Tis not too late to seek a newer world" (56–57). Since Dante's Ulisse has already undertaken this voyage and recounts it in the ''Inferno'', Ulysses' entire monologue can be envisioned as his recollection while situated in Hell.<ref>Rowlinson (1994), p. 132.</ref>

===From affirmation to irony===

[[File:Paradise Lost 2.jpg|right|thumb|"Satan rises from the burning lake" (1866) by [[Gustave Doré]]; a critical interpretation of the poem compares Ulysses' final sentiments with Satan's "courage never to submit or yield" in [[John Milton]]'s ''[[Paradise Lost]]''.]]

The degree to which Tennyson identifies with Ulysses has provided one of the great debates among scholars of the poem.<ref name="hughes"/> Critics who find that Tennyson identifies with the speaker read Ulysses' speech "affirmatively", or without [[irony]]. Many other interpretations of the poem have developed from the argument that Tennyson does not identify with Ulysses, and further criticism has suggested that the purported inconsistencies in Ulysses' character are the fault of the poet himself.

Key to the affirmative reading of "Ulysses" is the biographical context of the poem.  Such a reading takes into account Tennyson's statements about writing the poem—"the need of going forward"—and considers that he would not undermine Ulysses' determination with irony when he needed a similar stalwartness to face life after Hallam's death. Ulysses is thus seen as an heroic character whose determination to seek "some work of noble note" (52) is courageous in the face of a "still hearth" (2) and old age.<ref>Tennyson, however, discouraged an autobiographical interpretation of his monologues. (Campbell, p. 130.)</ref><ref name="hughes194">Hughes, p. 194.</ref> The passion and conviction of Tennyson's language—and even his own comments on the poem—signify that the poet, as was typical in the Victorian age, admired courage and persistence. Read straightforwardly, "Ulysses" promotes the questing spirit of youth, even in old age, and a refusal to resign and face life passively.

Until the early twentieth century, readers reacted to "Ulysses" sympathetically. The meaning of the poem was increasingly debated as Tennyson's stature rose. After Paull F. Baum criticized Ulysses' inconsistencies and Tennyson's conception of the poem in 1948,<ref>Baum, Paull F. (1948). ''Tennyson Sixty Years After.'' Chapel Hill: Univ. of North Carolina Press.</ref> the ironic interpretation became dominant.<ref>Hughes, p. 200.</ref> Baum finds in Ulysses echoes of [[Byronic hero|Lord Byron's flawed heroes]], who similarly display conflicting emotions, self-critical introspection, and a rejection of social responsibility. Even Ulysses' resolute final utterance—"To strive, to seek, to find, and not to yield"—is undercut by irony, when Baum and later critics compare this line to [[Satan]]'s "courage never to submit or yield" in [[John Milton]]'s ''[[Paradise Lost]]'' (1667).<ref>Baum 1948, cited in Pettigrew (1963).</ref><ref>{{cite book|author=Thomson, Alastair W.|title=The Poetry of Tennyson|location=London|publisher=Routledge|year=1986|isbn=0-7102-0716-6}}</ref>

Ulysses' apparent disdain for those around him is another facet of the ironic perspective. He declares that he is "matched with an aged wife" (3), indicates his weariness in governing a "savage race" (4), and suggests his philosophical distance from his son Telemachus. A skeptical reading of the second paragraph finds it a condescending tribute to Telemachus and a rejection of his "slow prudence" (36). However, the adjectives used to describe Telemachus—"blameless", "discerning", and "decent"—are words with positive connotations in other of Tennyson's poetry and within the classical tradition,<ref name="hughes194"/> where "blameless" is an attribute of gods and heroes.

Critic E. J. Chiasson argued in 1954 that Ulysses is without faith in an afterlife, and that Tennyson uses a "method of indirection" to affirm the need for religious faith by showing how Ulysses' lack of faith leads to his neglect of kingdom and family. Chiasson regards the poem as "intractable" in Tennyson's canon, but finds that the poem's meaning resolves itself when this indirection is understood: it illustrates Tennyson's conviction that "disregarding religious sanctions and 'submitting all things to desire' leads to either a [[wikt:sybaritic|sybaritic]] or a brutal repudiation of responsibility and 'life'."<ref>E. J. Chiasson, "Tennyson's 'Ulysses'—A Re-Interpretation". In Killham (1960), 164–173.</ref>

Other ironic readings have found Ulysses longing for withdrawal, even death, in the form of his proposed quest.  In noting the sense of passivity in the poem, critics highlight Tennyson's tendency toward the melancholic. [[T. S. Eliot]] opines that "Tennyson could not tell a story at all".<ref name="eliot"/> He finds Dante's treatment of Ulysses exciting, while Tennyson's piece is "an elegiac mood".<ref name="eliot">T. S. Eliot, "In Memoriam". In Killham (1960), 210.</ref> "Ulysses" is found lacking in narrative action; the hero's goal is vague, and by the poem's famous last line, it is not clear for what he is "striving", or to what he refuses to yield. According to Victorian scholar Herbert Tucker, Tennyson's characters "move" through time and space to be moved inwardly.<ref name="tucker11">Tucker, p. 11.</ref> 
To Ulysses, experience is "somewhere out there",<ref name="tucker11"/>

<poem>
      … an arch wherethro'
     Gleams that untravell'd world whose margin fades
     For ever and for ever when I move. <small>(19–21)</small>
</poem>

==Legacy==

[[File:Alfred Tennyson, 1st Baron Tennyson - Project Gutenberg eText 17768.jpg|thumb|left|Tennyson, as [[Poet Laureate]], used verse to promote [[British Empire|Empire]]: "Ulysses" has been interpreted as anticipating the concept of [[imperialism]].]]

===Contemporary appraisal and canonization===

Contemporary reviews of "Ulysses" were positive and found no irony in the poem. Author [[John Sterling (author)|John Sterling]]—like Tennyson a member of the [[Cambridge Apostles]]—wrote in the ''[[Quarterly Review]]'' in 1842, "How superior is 'Ulysses'! There is in this work a delightful epic tone, and a clear impassioned wisdom quietly carving its sage words and graceful figures on pale but lasting marble."<ref name="MarkleyQ124">Quoted in Markley, p. 124.</ref> Tennyson's 1842 volume of poetry impressed Scottish writer [[Thomas Carlyle]]. Quoting three lines of "Ulysses" in an 1842 letter to Tennyson—

<poem>
     It may be that the gulfs will wash us down,
     It may be we shall touch the happy Isles
     And see the great Achilles whom we knew! [sic] <small>(62–64)</small>
</poem>

—Carlyle remarked, "These lines do not make me weep, but there is in me what would fill whole Lachrymatories as I read."<ref>{{cite journal|title=Carlyle and Tennyson|author=Sanders, Charles Richard|journal=PMLA|volume=76|issue=1|date=March 1961|pages=82–97|doi=10.2307/460317|jstor=460317|publisher=Modern Language Association}}</ref>

English theologian [[Richard Holt Hutton]] summarized the poem as Tennyson's "friendly picture of the insatiable craving for new experience, enterprise, and adventure, when under the control of a luminous reason and a self-controlled will."<ref name="qstorch">Quoted in Storch, p. 283.</ref> The contemporary poet [[Matthew Arnold]] was early in observing the narrative irony of the poem: he found Ulysses' speech "the least ''plain'', the most ''un-Homeric'', which can possibly be conceived. Homer presents his thought to you just as it wells from the source of his mind: Mr. Tennyson carefully distils his thought before he will part with it. Hence comes ... a heightened and elaborate air."<ref name="MarkleyQ124"/> A bit later, journalist and apologist [[Gilbert Keith Chesterton]] saw Ulysses as a "tribute to Christianity" as getting to the heart of the remarkable difference between the outlook of Antiquity and the Christian-shaped modern one, remarking: "The poet reads into the story of Ulysses the conception of an incurable desire to wander. But the real Ulysses does not desire to wander at all. He desires to get home."<ref>G. K. Chesterton, ''Heretics'', Chapter XII [http://www.cse.dmu.ac.uk/~mward/gkc/books/heretics/ch12.html]</ref>

Despite the critical acclaim "Ulysses" received, its rise within the Tennyson canon took decades. Tennyson did not usually select it for publication in poetry anthologies; in teaching anthologies, however, the poem was usually included—and it remains a popular teaching poem today. Its current prominence in Tennyson's oeuvre is the result of two trends, according to Tennyson scholar Matthew Rowlinson: the rise of formal English poetry studies in the late nineteenth century, and the [[Victorian era|Victorian]] effort to articulate a British culture that could be exported.<ref name="r1992">Rowlinson (1992).</ref> He argues that "Ulysses" forms part of the prehistory of [[imperialism]]—a term that only appeared in the language in 1851. The protagonist sounds like a "colonial administrator", and his reference to seeking a newer world (57) echoes the phrase "[[New World]]", which became common during the [[Renaissance]]. While "Ulysses" cannot be read as overtly imperialistic, Tennyson's later work as [[Poet Laureate]] sometimes argues for the value of Britain's colonies, or was accused of jingoism. Rowlinson invokes the [[Marxism|Marxist]] theorist [[Louis Althusser]]'s extension of the argument that ideology is ahistorical, finding that Tennyson's poem "comes before an ideological construction for which it nonetheless makes people nostalgic".<ref name="r1992"/>

===Literary and cultural legacy===

In a 1929 essay, [[T. S. Eliot]] called "Ulysses" a "perfect poem".<ref name="eliot2">T. S. Eliot (1950). ''Selected essays, 1917–1932.'' New York: Harcourt Brace Jovanovich, 210. ISBN 0-15-180387-0.</ref> An analogue of Ulysses is found in Eliot's "[[Gerontion]]" (1920). Both poems are narrated by an aged man contemplating life's end. An excerpt from "Gerontion" reads as an ironic comment on the introductory lines of "Ulysses":<ref>Fulweiler, p. 170.</ref>

<poem>
     Rocks, moss, stonecrop, iron, merds.
     The woman keeps the kitchen, makes tea,
     Sneezes at evening, poking the peevish gutter.

                 I am an old man,
     A dull head among windy places. <small>(13–17)</small>
</poem>

The Italian poet [[Giovanni Pascoli]] (1855–1912) stated that his long lyric poem ''L'ultimo viaggio'' was an attempt to reconcile the portrayals of Ulysses in Dante and Tennyson with [[Tiresias]]'s prophecy that Ulysses would die "a mild death off the sea".<ref>Stanford, p. 205. "Off the sea" is Stanford's attempt to cover both angles of the Greek phrase (''ex alos''), which has been taken to mean "from the sea" or, conversely, "away from the sea".</ref> Pascoli's Ulysses leaves Ithaca to retrace his epic voyage rather than begin another.

"Ulysses" remains much admired, even as the twentieth century brought new interpretations of the poem.<ref>Pettigrew, p. 27.</ref> Professor of literature [[Basil Willey]] commented in 1956, "In 'Ulysses' the sense that he must press on and not moulder in idleness is expressed objectively, through the classical story, and not subjectively as his own experience. [Tennyson] comes here as near perfection in the grand manner as he ever did; the poem is flawless in tone from beginning to end; spare, grave, free from excessive decoration, and full of firmly controlled feeling."<ref name="qstorch"/>
In the fifteenth edition of ''[[Bartlett's Familiar Quotations]]'' (1980), nine sections of "Ulysses", comprising 36 of the poem's 70 lines, are quoted,<ref>{{cite book|title=[[Bartlett's Familiar Quotations]]|edition=Fifteenth|author=[[John Bartlett (publisher)|John Bartlett]] (edited by Emily Morison Beck)|publisher=[[Little, Brown and Company]]|year=1980|isbn=0-316-08275-9|pages=529}}</ref> compared to only six in the ninth edition (1891).

Many readers have accepted the acclaimed last lines of "Ulysses" as inspirational. The poem's ending line has also been used as a [[motto]] by schools and other organisations. The final line is inscribed on a cross at [[Observation Hill (McMurdo Station)|Observation Hill]], Antarctica, to commemorate explorer [[Robert Falcon Scott]] and his party, who died on their [[Terra Nova Expedition|return trek]] from the [[South Pole]] in 1912:<ref>{{cite journal|author=Maher, Patrick|title=A Ulysses Connection: Examining Experiences in the Ross Sea Region, Antarctica|journal=Journal of Experiential Education|year=2005|issn=1053-8259}}</ref>

<poem>
     One equal temper of heroic hearts,
     Made weak by time and fate, but strong in will
     To strive, to seek, to find, and not to yield. <small>(68–70)</small>
</poem>

==Notes==

{{reflist|30em}}

==References==

''Excerpts from "Ulysses" are given line numbers in parentheses, and are sourced from Tennyson, A. T., & Day, A. (1991). References to paragraph (stanza) numbers correspond to the more common, three-paragraph printing of the poem.''
*{{cite book|author=Campbell, Matthew|title=Rhythm & Will in Victorian Poetry|publisher=Cambridge University Press|year=1999|isbn=0-521-64295-7}}
*{{cite journal|author=Culler, A. Dwight|title=Monodrama and the Dramatic Monologue|journal=PMLA|date=May 1975|volume=90|issue=3|pages=366–385|doi=10.2307/461625|jstor=461625|publisher=Modern Language Association}}
*Fulweiler, H. W. (1993). ''"Here a captive heart busted": studies in the sentimental journey of modern literature.'' New York: Fordham University Press. ISBN 0-8232-1496-6.
*{{cite journal|author=Hughes, Linda K.|title=Dramatis and private personae: 'Ulysses' revisited|journal=Victorian Poetry|volume=17|issue=3|year=1979|pages=192–203}}
*{{cite book|author=Killham, John (ed.)|title=Critical essays on the poetry of Tennyson|year=1960|publisher=Routledge & Kegan Paul|location=London}} {{LCC|PR5588 .K5}}
*{{cite book|author=Markley, A. A.|title=Stateliest Measures: Tennyson and the Literature of Greece and Rome|year=2004|publisher=University of Toronto Press|isbn=0-8020-8937-2}}
*{{cite journal|author=Pettigrew, John|title=Tennyson's 'Ulysses': a reconciliation of opposites|journal=Victorian Poetry|volume=1|pages=27–45|year=1963}}
*{{cite journal|author=Rowlinson, Matthew|title=The Ideological Moment of Tennyson's 'Ulysses'|journal=Victorian Poetry|volume=30|issue=3/4|pages=265–276|year=1992}}
*Rowlinson, M. C. (1994). ''Tennyson's fixations: psychoanalysis and the topics of the early poetry.'' Victorian literature and culture series. Charlottesville: University Press of Virginia. ISBN 0-8139-1478-7.
*Stanford, W. B. (1993) [1955]. ''The Ulysses theme: a study in the adaptability of a traditional hero.'' Dallas, TX: Spring Publications. ISBN 0-88214-355-7.
*{{cite journal|author=Storch, R. F.|title=The fugitive from the ancestral hearth: Tennyson's 'Ulysses'|journal=Texas Studies in Literature and Language|volume=13|issue=2|year=1971|pages=281–297}}
*Tennyson, A. T., & Day, A. (1991). ''Alfred Lord Tennyson: selected poems.'' Penguin classics. London, England: Penguin Books. ISBN 0-14-044545-5.
*{{cite journal|author=Tucker, Jr., Herbert F.| title=Tennyson and the Measure of Doom|journal=PMLA|volume=98|issue=1|date=January 1983|pages=8–20|doi=10.2307/462069|jstor=462069|publisher=Modern Language Association}}

==External links==
{{wikisource|Ulysses (Tennyson)|"Ulysses"}}
*[http://rpo.library.utoronto.ca/poems/ulysses Text of "Ulysses"] with annotations at Representative Poetry Online.
*[https://www.youtube.com/watch?v=DCO3xgiL1EU A reading of "Ulysses"] by Robert Nichol
*[http://charon.sfsu.edu/TENNYSON/ULYSSES.HTML A reading of "Ulysses"] by Sir [[Lewis Casson]] (1875–1969). [BAD LINK]
*Landow, George P. "[http://www.victorianweb.org/authors/tennyson/ulysses.html Alfred Tennyson's "Ulysses"]". ''The Victorian Web''.
{{Spoken Wikipedia|En-Ulysses (poem)-article.ogg|2016-008-21}}

{{featured article}}

{{Alfred Tennyson}}
{{Odyssey navbox}}

{{DEFAULTSORT:Ulysses (Poem)}}
[[Category:1833 poems]]
[[Category:1842 poems]]
[[Category:Poetry by Alfred Tennyson]]
[[Category:Odyssey]]
[[Category:Works based on the Odyssey]]