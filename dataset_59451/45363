{{Use dmy dates|date=October 2012}}
{{Infobox military person
|name          = Harold Huston George
|image         = File:Harold H. George.JPG
| image_size        = 250
|caption       = Harold Huston George
|birth_date          = 14 September 1892
|death_date          = {{Death-date and age|29 April 1942|14 September 1892}}
|placeofburial_label =
|placeofburial = [[Arlington National Cemetery]]
|birth_place  = [[Lockport (city), New York|Lockport]], [[New York (state)|New York]]
|death_place  = [[Darwin, Northern Territory|Darwin]], [[Australia]]
|placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
|nickname      = "Pursuit" George
| branch        = [[Air Service, United States Army]]<BR>[[United States Army Air Corps]]<BR>[[United States Army Air Forces]]
|serviceyears  = 1916-1942
|rank          = [[File:US-O7 insignia.svg|18px]] [[Brigadier general (United States)|Brigadier general]]
|servicenumber = 0-9605
| unit          = [[Air Service, United States Army]]
* [[139th Aero Squadron]]
|commands      = [[31st Pursuit Group]]<br>[[Far East Air Force (United States)|Far East Air Force]]
| battles       = [[File:World War I Victory Medal ribbon.svg|50px]]&nbsp;[[File:World War II Victory Medal ribbon.svg|50px]]<BR>[[World War I]]<BR>[[World War II]]
*[[Philippines Campaign (1941–42)]]
|awards        = [[Distinguished Service Cross (United States)|Distinguished Service Cross]]<br/>[[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]]<br/>[[Silver Star]]
|relations     =
|laterwork     =
}}
'''Harold Huston George''' (14 September 1892 – 29 April 1942) was a general officer in the [[United States Army Air Forces]] during [[World War II]]. He began his military career before [[World War I]] when he enlisted as a private in the [[New York Army National Guard|3rd New York Infantry Regiment]]. Joining the [[United States Army Air Service|Air Service]], he became an [[flying ace|ace]] in France in 1918, credited with five aerial victories.

George, known as "Pursuit" George to distinguish him from [[Harold L. George]] ("Bomber" George), commanded the 5th Interceptor Command (Provisional) on [[Luzon]] following the attack by Japan on the Philippine Islands, then directed the remnants of the Army's air forces in the Philippine Islands after [[Far East Air Force (United States)|Far East Air Force]] commander Maj. Gen. [[Lewis H. Brereton]] evacuated to Australia on 24 December 1941.<ref name="aerod">{{cite web | last =| first =| authorlink = | year =| url= http://www.theaerodrome.com/aces/usa/george.php|title = Harold George| work = | publisher = theaerodrome.com| accessdate= 2010-06-17}}</ref>

==World War I service==
George joined the [[New York National Guard]] on 5 July 1916, during the crisis caused by [[Pancho Villa]]'s [[Battle of Columbus (1916)|raid on Columbus, New Mexico]]. His unit was federalized and deployed to the Mexican border, where he served as a sergeant until 5 October.

George enlisted as an aviation cadet on 15 April 1917, in the [[Aviation Section, U.S. Signal Corps]].<ref name="aerod"/> He completed flying training on the Curtiss biplane at [[Hazelhurst Field]], [[Mineola, New York]], received a [[U.S. Air Force Aeronautical Ratings|rating]] of Reserve Military Aviator, and was commissioned a [[first lieutenant]] in the Signal Officers Reserve Corps (SORC) on 15 September 1917. He went to [[Kelly Field]], [[Texas]], for additional training before going to [[Tours]], [[France]], as commanding officer of the [[201st Aero Squadron]] in October 1917.

For six months he instructed other pilots at the [[United States Army Air Service#Air Service of the AEF|Air Service AEF]]'s training center at [[Issoudun]]. He then took pursuit pilot and gunnery courses himself and went into combat in August 1918 with the [[185th Aero Squadron]], and later duty with the [[139th Aero Squadron]].

George arrived at the [[139th Aero Squadron]] on 18 September 1918.<ref>{{cite book |title= ''American Aces of World War I'' |page= 78 }}</ref> He scored his first two victories on 27 October, near Bantheville, France, he struck a formation of four enemy Fokkers, destroying two and driving the other two away. George shot down a [[Fokker D.VII]] single-handed and shared a second win with [[Robert Opie Lindsay]]. Two days later, he doubled again, sharing the wins over Fokker D.VIIs with [[Edward Haight (aviator)|Edward Haight]] and [[Karl Schoen]]. On 5 November, he shared his fifth victory over a D.VII with two other pilots, and became an ace. His Distinguished Service Cross came through after war's end, in 1919<ref name="aerod"/>

After his return to the United States, George married Vera McKenna, whom he had met in Tours where she was working, on 5 April 1919, in [[New York City]]. They had two children, Robert (b. 1920) and Peggy (b. 1922).

==Between wars==
After the war he returned to the United States for duty at [[Langley Field]], [[Virginia]]. In November 1919 he became flight commander and commanding officer of the 19th Pursuit Squadron at [[March Field]], [[California]], serving until March 1922. During that time he applied for and received a regular Army commission as captain, [[United States Army Air Service|Air Service]], 0n 1 July 1920, when the Air Service became a combat arm of the line. He went to [[Fort Douglas, Utah]], for three years as Air Service Officer to the [[104th Infantry Division (United States)|104th Division]]. He was transferred to the Advanced Flying School at Kelly Field in March 1925, as an instructor for two years, commanding officer of the 43d School Squadron, and Chief of Pursuit Instruction to July 1929.

He was then assigned to [[France Field]], [[Panama]] for a two-year tour, as operations officer and commanding officer of the [[24th Fighter Squadron|24th Pursuit]] and [[397th Bombardment Squadron|7th Observation Squadron]]s, respectively. He returned to Langley Field on 25 June 1932, as commanding officer of the [[33d Special Operations Squadron|33d Pursuit Squadron]], followed by assignments as intelligence and operations officer of the [[8th Operations Group|8th Pursuit Group]]. In 1934 he commanded a sector of the Eastern Zone of [[Air Mail Scandal|Army Air Corps Mail Operation]], responsible for [[Airmail|Air mail]] operations on routes between [[Newark, New Jersey]]; [[Cleveland, Ohio]]; [[St. Louis, Missouri]]; and [[Memphis, Tennessee]].

From September 1936 to June 1937, he attended the [[Air Corps Tactical School]] at [[Maxwell Field]], [[Alabama]], followed in 1937-1938 by the [[Command and General Staff School]] at [[Fort Leavenworth]], [[Kansas]]. His next assignment was to [[Selfridge Field]], Michigan with the [[1st Operations Group|1st Pursuit Group]], where he commanded its [[94th Fighter Squadron|94th Pursuit Squadron]]. On 1 February 1940, he was promoted to lieutenant colonel and named commander of the [[31st Operations Group|31st Pursuit Group]], newly activated at Selfridge.

George commanded an aircraft-ferrying flight to Panama via Mexico and Central America, exactly 20 years after he had flown a DeHaviland plane to the Pacific Coast and return to New York in an early-day transcontinental reliability test flight.
[[File:CampMurphy.jpg|right|220px|thumb|Ceremony at [[Camp Aguinaldo|Camp Murphy]] in Rizal marking the induction of the Philippine Army Air Corps into the U.S. Army on 15 August 1941. George is second from left in the rank behind Gen. [[Douglas MacArthur]].]]

On 4 May 1941, Brig. Gen. Henry B. Clagett arrived at [[Manila]] in the [[Philippines]] to command the newly created Philippine Department Air Force. George, promoted to colonel on 9 October 1940, accompanied him as his chief of staff. As the [[United States Department of War|War Department]] belatedly attempted to expand the defenses in the Philippines, the air forces evolved first into the Air Force, [[United States Army Forces Far East]] in August, and then into the [[Far East Air Force (United States)|Far East Air Force]] in November 1941. Clagett was named commander of the 5th Interceptor Command (Provisional) and George remained as his chief of staff after a short stint as head of supply for FEAF. When Clagett was sent to Australia in mid-December, George inherited the pursuit command, and when Brereton and a small staff also left on 24 December, George became ''de facto'' commander of the remaining Army aviation units and personnel.

He was promoted to brigadier general on 25 January 1942, and made his command post at [[Mariveles, Bataan|Mariveles]] on the [[Bataan]] peninsula. On 11 March 1942, he was [[Douglas MacArthur's escape from the Philippines|evacuated from Corregidor by PT boat]] along with Gen. [[Douglas MacArthur]]. General George was awarded the [[Silver Star]] for his actions on Corregidor.

==Death on duty==
George was killed in a ground accident at [[Batchelor Field]], southeast of [[Darwin, Australia]] on 29 April 1942 when a [[Curtiss P-40]] of the [[49th Fighter Group]] lost directional control on take off and struck the parked [[Lockheed C-40]] in which he had just arrived at the base. George was struck in the head, possibly by one of the fighter's wheels, and killed instantly as were two others standing with him next to the Lockheed after disembarking.<ref>[http://home.st.net.au/~dunn/ozcrashes/nt105.htm GENERAL HAL GEORGE, 2ND LT. ROBERT D. JASPER, & WAR CORRESPONDENT MEL JACOBY KILLED IN A KITTYHAWK GROUND ACCIDENT AT BATCHELOR AIRFIELD ON 29 APRIL 1942]</ref>

The AAF opened [[Lawrenceville-Vincennes International Airport|George Army Airfield]], a flying training base at [[Lawrenceville, Illinois]], in August 1942. The base was declared surplus in September 1945 and sold. Victorville Air Force Base, California, was renamed [[George Air Force Base]] in George's honor in June 1950.<ref name="aerod"/>

General George was not killed instantly.  He lingered on for several days treated by a Lieutenant Lawrence Braslow MC., USA.

==See also==
{{Portal|United States Air Force|Military of the United States|World War I|World War II|Biography}}
* [[List of World War I flying aces from the United States]]

==References==
{{Reflist}}
{{Refbegin}}
{{Refend}}

==Bibliography==
* {{webarchive |date=2012-12-12 |url=http://archive.is/20121212203214/http://www.af.mil/information/bios/bio.asp?bioID=5515 |title=Harold H. George}} USAF biography
* [http://www.arlingtoncemetery.net/hhgeorge.htm Harold Huston George], Arlington Cemetery website
* ''American Aces of World War I.'' Norman Franks, Harry Dempsey. Osprey Publishing, 2001. ISBN 1-84176-375-6, ISBN 978-1-84176-375-0.

==External links==

{{DEFAULTSORT:George, Harold H.}}
[[Category:1892 births]]
[[Category:1942 deaths]]
[[Category:People from Lockport, New York]]
[[Category:American generals]]
[[Category:American World War I flying aces]]
[[Category:American military personnel of World War I]]
[[Category:American military personnel of World War II]]
[[Category:Air Corps Tactical School alumni]]
[[Category:United States Army Command and General Staff College alumni]]
[[Category:Recipients of the Distinguished Service Cross (United States)]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Silver Star]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:Aviators killed in aviation accidents or incidents in Australia]]