{{Infobox non-profit
| name              = Boys Hope Girls Hope
| image             = Boys Hope Girls Hope.png
| type              = [[Charitable organization|Charitable]]
| founded_date      = 1977 <!-- {{Start date|YYYY|MM|DD}} -->
| tax_id            = 
| registration_id   =
| founder           = Paul Sheridan
| location          = [[St. Louis, Missouri|St. Louis]], [[Missouri]], U.S.
| coordinates       = <!-- {{Coord|LAT|LON|display=inline,title}} -->
| origins           = 
| key_people        = 
| area_served       = 
| product           = 
| mission           = 
| focus             = 
| method            = 
| revenue           = 
| endowment         = 
| num_volunteers    = 
| num_employees     = 
| num_members       = 
| subsid            = 
| opponents         = 
| owner             = 
| non-profit_slogan = 
| former name       =
| homepage          ={{URL|http://www.boyshopegirlshope.org/}}
| dissolved         = 
| footnotes         = 
}}
'''Boys Hope Girls Hope (BHGH)''' is a [[charitable organization]] based in the United States that provides [[group home]]s for poor, academically capable children. Children are often enrolled in private schools and provided additional academic support. BHGH has been very successful in helping at risk children attend college and become successful professionals.<ref>{{cite journal |first=Nancy |last=DeCesare |title=Meeting the Academic Potential of Youth in Foster Care |journal=reclaiming children and youth |volume=12 |issue=4 |year=2004 |url=https://reclaimingjournal.com/sites/default/files/journal-article-pdfs/12_4_DeCesare.pdf}}</ref> BHGH has fifteen sites in the US and [[Latin America]]. Six to twelve children live in each home.<ref>{{cite web |title=Boys Hope Girls Hope |url=http://www.boyshopegirlshope.org/}}</ref> BHGH also has a Community Based Program (CBP), where scholars live with parents, but receive BHGH benefits.<ref>[http://www.boyshopegirlshope.org/About.aspx "About"], ''Boys Hope Girls Hope''</ref>  Services are continued after high school graduation, including job placement and financial assistance through college.<ref name="times">[https://www.nytimes.com/2007/02/04/nyregion/04neediest.html?scp=3&sq=boys%20hope%20girls%20hope&st=cse "Her Parents Couldn’t Take Care of Her. This Home Could."], ''The New York Times, February 2007''</ref> Participants are referred to as scholars. It was [[List of Jesuit development centres|founded]] by a [[Jesuit]] priest, Paul Sheridan, but is not based on any religion or spirituality.<ref>''Nun wins award for helping young people overcome obstacles''. Cincinnati Enquirer, September 1, 2005, p. C3.</ref>

The New York Times Neediest Cases Fund aids the New York BHGH affiliate.<ref name="times"/bgh

== References ==
{{Reflist}}

== External links ==
* [http://www.boyshopegirlshope.org Boys Hope Girls Hope (BHGH) national page]

<!--- Categories --->
[[Category:Jesuit development centres]]
[[Category:Articles created via the Article Wizard]]
[[Category:Charities based in Missouri]]
[[Category:1977 establishments in Missouri]]
[[Category:Children's charities based in the United States]]
[[Category:Organizations established in 1977]]
[[Category:Development charities based in the United States]] 
[[Category:Social welfare charities based in the United States]] 
[[Category:International educational charities]]