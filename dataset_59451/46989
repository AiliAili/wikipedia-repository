{{EngvarB|date=June 2013}}
{{Use dmy dates|date=December 2014}}
{{Infobox settlement
| name                       = Cheragi Pahar
| native_name                = চেরাগী পাহাড়
| native_name_lang           = [[Bengali language|Bengali]] 
| settlement_type            = Historical place
| image_skyline              = Cheragi Pahar Circle chittagong (4).JPG
| imagesize                  = 250px
| image_caption              = Cheragi Pahar Circle
| nickname                   = Cheragi More, Cheragi Square
| image_map                  = 
| map_caption                = Location map of Cherai Pahar
| pushpin_map                = Bangladesh
| pushpin_label_position     = 
| pushpin_map_alt            = 
| pushpin_map_caption        = Location of Cherai Pahar in [[Bangladesh]]
| coordinates                = {{coord|22|20|37.24|N|91|50|1.33|E|type:landmark|display=inline,title}}
| coor_pinpoint              = 
| coordinates_footnotes      = 
| subdivision_type           = Country
| subdivision_name           = [[Bangladesh]]
| subdivision_type1          = District
| subdivision_name1          = [[Chittagong]]
| subdivision_type2          = Place
| subdivision_name2          = Cheragi Pahar
| subdivision_type3          = 
| subdivision_name3          = 
| established_title          = Settled
| established_date           = 
| founder                    = 
| seat_type                  = 
| seat                       = 
| government_type            = [[Mayor–council government|Mayor–council]]
| leader_title               = Mayor
| leader_name                = A J M Nasir Uddin
| leader_title1              = [[Mayor|City Mayor]]
| leader_name1               = [[Chittagong City Corporation]]
| unit_pref                  = Imperial
| area_footnotes             =
| area_magnitude             = 
| area_total_sq_mi           = 
| area_land_sq_mi            = 
| area_water_sq_mi           = 
| area_water_percent         = 
| area_urban_sq_mi           = 
| area_metro_sq_mi           = 
| elevation_footnotes        = 
| elevation_ft               = 
| elevation_m                = 
| population_footnotes       = 
| population_as_of           = [[Demographics of Bangladesh]]
| population_total           =
| pop_est_as_of              = 
| population_est             =
| population_rank            = 
| population_density_sq_mi   = 
| population_urban           = 
| population_density_urban_sq_mi = auto
| population_metro           = 
| population_density_metro_sq_mi = auto
| population_demonym         = Chittagonian
| timezone                   = [[Bangladesh Standard Time|BST]]
| utc_offset                 = +06:00
| timezone_DST               = 
| utc_offset_DST             = 
| area_code_type             = BTCL
| area_code                  = 031
| website                    = 
}}

'''Cheragi Pahar''' (sometimes written '''Cheragee Pahar''')<ref>{{cite web |url=http://wikimapia.org/10293136/Cheragi-Pahar-Jamal-khan |title=Cheragi Pahar, Jamal khan |publisher=wikimapia.org |accessdate=2013-06-21}}</ref> is a cultural and historical place located at [[Chittagong]], Bangladesh.<ref>{{cite web |url=http://en.wikigogo.org/en/124960/ |title=Cheragee Pahar Circle Locations |publisher=en.wikigogo.org |accessdate=2013-06-21}}</ref><ref name="geoview">{{cite web |url=http://bd.geoview.info/cheragi_pahar,156280077w |title=Cheragi Pahar |website=bd.geoview.info |publisher=bd.geoview.info |access-date=June 9, 2016}}</ref> Most of the cultural activities of Chittagong and related business are establishes in the place. Some parts of the region Momin Road are the part of Cheragee Pahar Circle.

== Cultural activities ==
[[File:Cheragee Art Show 2 (5).jpg|thumb|left|''Cheragee Art Show 2'', is a site specific art exhibition at Cherai Pahar in 2013]]

The poets, [[authors]], [[little magazine|little magazine activist]], artists, musicians, journalists, dramatist, model and cultural workers gather at Cheragee Pahar Circle every day.<ref>{{cite web|url=http://www.daily-sun.com/details_yes_01-01-2011_Addaru-gathering-at-Cheragi_84_1_7_1_0.html |title=Addaru gathering at Cheragi |publisher=daily-sun.com |date=11 January 2011 |accessdate=2013-06-21}}</ref> [[Pohela Boishakh]], [[Pohela Falgun]] is celebrated in every year at the place.<ref>{{cite web|url=http://www1.bssnews.net/newsDetails.php?cat=4&id=313159$date=2013-02-13&dateCurrent=2013-02-22 |title=Pahela Falgun celebrated in Chittagong |publisher=bssnews.net |date=13 February 2013 |accessdate=2013-06-21}}</ref>

In 2012, a site specific art exhibition held at the place, where young artists and curator experiments with new art forms and media in his exhibitions.<ref name="Art World">{{cite news |author=Fayeka Zabeen Siddiqua |date= January 9, 2015 |title=Taking on the Art World |url=http://www.thedailystar.net/taking-on-the-art-world-58935 |newspaper=[[The Daily Star (Bangladesh)]] |access-date=June 9, 2016}}</ref> 

==Media and communications==
There are several newspapers, including daily newspapers, opposition newspaper, business newspapers based in Chittagong are published from the place. Various [[little magazines]] also published and distributed. There are some newspapers office and cultural-book shops.

=== Newspaper publishes ===
* [[The Daily Azadi]]
* [[Bhorer Kagoj|The Daily Bhorer Kagoj]]

==Gallery==
<gallery>
Ceragi Pahar.jpg|Cheragi Square
Cheragee Art Show 2 (7).jpg|Cheragee Art Show 2, in 2013
</gallery>

== References ==
{{Reflist}}

==External links==
{{Commons category|Cheragi Pahar}}

[[Category:Chittagong]]
[[Category:Neighbourhoods in Bangladesh]]
[[Category:Buildings and structures in Chittagong]]

{{Chittagong-geo-stub}}