{{redirect|X-55|the Soviet/Russian cruise missile|Kh-55 (missile family)}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout, and guidelines. -->
{|{{Infobox aircraft begin
 |name= X-55 ACCA
 |image= Lockheed Martin X-55 ACCA 001.jpg
 |caption =
}}{{Infobox aircraft type
 |type= Technology demonstrator
 |manufacturer= [[Lockheed Martin]] [[Skunk Works]]
 |designer =
 |first flight= 2 June 2009
 |introduction =
 |retired =
 |status= Development and testing
 |primary user= [[United States Air Force]]
 |produced= <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
 |number built= 1
 |unit cost =
 |developed from= [[Fairchild Dornier 328JET]]
 |variants with their own articles =
}}
|}

The '''Lockheed Martin X-55 Advanced Composite Cargo Aircraft (ACCA)''' is an experimental [[twinjet]] [[Military transport aircraft|transport aircraft]].  It is intended to demonstrate new [[air cargo]]-carrier capabilities using advanced [[composite material]]. A project of the [[United States Air Force]]'s [[Air Force Research Laboratory]], it was built by the international aerospace company [[Lockheed Martin]], at its Advanced Development Programs (''[[Skunk Works]]'') facility in [[Palmdale, California]].

==Design and development==
The X-55 is a one-off aircraft intended to demonstrate the use of advanced composite materials in the fuselage of an otherwise conventional [[high-wing]] transport aircraft. There are no plans to place the X-55 into production.

Lockheed Martin's design for Advanced Composite Cargo Aircraft (ACCA) was chosen over [[Aurora Flight Sciences]]' design based on the [[Antonov An-72]] in 2007.<ref>{{cite web|url=http://www.flightglobal.com/blogs/the-dewline/2010/08/why-aurora-flight-sciences-is.html|title=Why Aurora Flight Sciences is still bitter about X-55|work=The DEW Line|accessdate=3 July 2015}}</ref>  The aircraft is powered by 2 [[Pratt & Whitney]] PW306B turbofans.<ref name="Parsch 2011">{{cite web|last=Parsch|first=Andreas|title=DOD 4120.15-L - Addendum|url=http://www.designation-systems.net/usmilav/412015-L%28addendum%29.html|accessdate=13 September 2012}}</ref> The X-55 design is based on the existing [[Fairchild Dornier 328JET]]. The fuselage of that aircraft, which is constructed of [[aluminium alloy]]s, was replaced aft of the entrance door with a newly designed fuselage. The new design makes extensive use of advanced composite materials, selected to allow out-of-[[autoclave (industrial)]] curing at lower temperatures and pressures than previous materials. The new widened fuselage allows the loading of cargo through a rear ramp.

The new fuselage section is constructed as a single large component, including the [[vertical stabilizer]]. When attached to the existing nose section, the fuselage is 55 feet (16.8 m) long and 9 feet (2.74 m) diameter. The fuselage has upper and lower halves, each with a roughly-oval shape similar to a canoe. The halves are bonded to circular frames. The fuselage section ahead of the entrance door consists of the existing (metal) 328J component, with fasteners used to bring the forward and new aft sections together.

As of April 2008, the fuselage was being fabricated. The first flight of the modified aircraft was expected during the winter of 2008/2009.<ref>{{cite web|url=http://www.aero-online.org|title=Aerospace Articles - SAE International|work=aero-online.org|accessdate=3 July 2015}}</ref>  However, due to a "glitch" during fabricating the composite fuselage, that schedule slipped.<ref>"Taking Shape". [[Aviation Week & Space Technology]], '''70''', 10 (9 March 2009), p. 32.</ref>  The delay was caused by an unsatisfactory bond of the skin on the lower fuselage, which required a second fuselage to be fabricated.

The first flight was completed at Lockheed Martin's Advanced Development Programs facility (Air Force Plant 42) in Palmdale, California on June 2, 2009 by the Air Force Research Laboratory in conjunction with Lockheed Martin.<ref>{{cite web|url=http://archive.is/20120718111814/http://www.af.mil/news/story.asp?id=123152339|title=Advanced Composite Cargo Aircraft makes first flight|work=archive.is|accessdate=3 July 2015}}</ref>  In October 2009, the ACCA demonstrator was designated ''X-55A'' by the USAF. Over the course of the program, 15 to 20 flights were expected. Furthermore, it was estimated that the vehicle was built for half the cost of a conventional design for a similar aircraft.<ref>{{cite web|url=http://archive.is/20120630081008/http://www.af.mil/news/story.asp?id=123173711|title=Advanced Composite Cargo Aircraft gets X-plane designation|work=archive.is|accessdate=3 July 2015}}</ref><ref>{{cite web|url=http://www.airforcesmonthly.com/view_news.asp?ID=931|title=AirForces Monthly: ACCA becomes X-55|work=airforcesmonthly.com|accessdate=3 July 2015}}</ref>

==Aircraft on display==
As of September 12, 2014, the X-55 aircraft is on display at Joe Davies Heritage Airpark in Palmdale, California.<ref>{{cite web|url=http://globalaviationreport.com/2014/09/17/nasas-shuttle-carrier-aircraft-settles-down-in-palmdale/|title=NASA’s shuttle carrier aircraft settles down in Palmdale - global aviation report|work=global aviation report|accessdate=3 July 2015}}</ref>

==See also==
{{Portal|United States Air Force}}
{{aircontent
|see also=
|related=
* [[Fairchild Dornier 328JET]]
|similar aircraft=
|lists=
}}

==References==
{{Reflist}}

==External links==
{{Commons category-inline|Lockheed Martin X-55}}

{{Lockheed aircraft}}
{{X-planes}}

[[Category:Lockheed Martin aircraft|X-055]]
[[Category:United States experimental aircraft 2000–2009]]
[[Category:Twinjets]]
[[Category:High-wing aircraft]]
[[Category:T-tail aircraft]]