{{Infobox journal
| title = Molecular Neurobiology
| cover = [[File:2013 cover Mol Neurobiol.jpg]]
| editor = Nicolas G. Bazan
| discipline = [[Molecular neuroscience]]
| former_names =
| abbreviation = Mol. Neurobiol.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Bimonthly
| history = 1987-present
| openaccess =
| license =
| impact = 5.137
| impact-year = 2014
| website = http://www.springer.com/biomed/neuroscience/journal/12035
| link1 =
| link1-name =
| link2 = http://link.springer.com/journal/volumesAndIssues/12035
| link2-name = Online archive
| JSTOR =
| OCLC = 15640289
| LCCN = 87659194
| CODEN = MONBEW
| ISSN = 0893-7648
| eISSN = 1559-1182
}}
'''''Molecular Neurobiology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering all aspects of [[molecular neuroscience]]. It was established in 1987 and is published by [[Springer Science+Business Media]]. The [[editor-in-chief]] is Nicolas G. Bazan ([[LSU Health Sciences Center New Orleans]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]
* [[PubMed]]/[[MEDLINE]]
* [[Scopus]]
* [[PsycINFO]]
* [[EMBASE]]
* [[Chemical Abstracts Service]]
* [[Academic OneFile]]
* [[AGRICOLA]]
* [[Aquatic Sciences and Fisheries Abstracts]]
* [[Biological Abstracts]]
* [[BIOSIS Previews]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 5.137.<ref name=WoS>{{cite book |year=2014 |chapter=Molecular Neurobiology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/biomed/neuroscience/journal/12035}}

[[Category:Neuroscience journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1987]]