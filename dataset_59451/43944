<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = 8 hp
 |image = File:ABC8HP.JPG
 |caption = ABC 8 hp installed in the [[English Electric Wren]] at the [[Shuttleworth Collection]]
}}{{Infobox Aircraft Engine
 |type = [[Flat-twin]] [[Aircraft engine|aero engine]]
 |national origin = United Kingdom
 |manufacturer = [[ABC Motors|ABC Motors Limited]]
 |designer = [[Granville Bradshaw]]
 |first run = {{avyear|1923}}
 |major applications = 
 |produced =  
 |number built = 
 |program cost =
 |unit cost =
 |developed from =
 |variants with their own articles =
}}
|}

The '''ABC 8&nbsp;hp''' is an 8&nbsp;hp (6&nbsp;kW) two-cylinder [[Aircraft engine|aero engine]] designed by the noted British engineer [[Granville Bradshaw]] for use in ultralight aircraft. The engine was derived from a specially tuned [[motorcycle]] unit and was built by [[ABC Motors]], first running in 1923.<ref>Lumsden 2003, p.51.</ref>

==Applications==
*[[English Electric Wren]]
*[[Handley Page H.P.22]]
*[[Pegna-Bonmartini Rondine]]

==Survivors==
The sole surviving English Electric Wren ([[Aircraft registration|''G-EBNV'']]) flies occasionally at the [[Shuttleworth Collection]] at [[Old Warden]] and is powered by an ABC 8&nbsp;hp engine. With barely enough power to maintain flight the aircraft is [[Gliding#Bungee launch|assisted into the air]] by a team of volunteers using [[bungee cord]], after which the Wren lands straight ahead following a short 'hop'.<ref>[http://www.shuttleworth.org/shuttleworth_aircraft_details.asp?ID=13 The Shuttleworth Collection - English Electric Wren] Retrieved: 7 February 2009</ref>

==Specifications (ABC 8 hp)==
{{pistonspecs
|ref=Lumsden<ref>Lumsden 2003, p.54.</ref> 
|type=2-cylinder air-cooled flat-twin
|bore=2.72 in (69&nbsp;mm)
|stroke=2.13 in (54&nbsp;mm) 
|displacement=24.76 cu in (404&nbsp;cc)
|length=
|diameter=
|width=
|height=
|weight=41 lb (18.6&nbsp;kg)
|valvetrain= [[Overhead valve]], two valves per cylinder
|supercharger=
|turbocharger=
|fuelsystem= Carburettor
|fueltype=Petrol
|oilsystem=
|coolingsystem=Air-cooled
|power=8&nbsp;hp (6 kW) at 4,500&nbsp;rpm  
|specpower=
|compression=
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight= 0.2&nbsp;hp/lb (0.32 kW/kg)
}}

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=
*[[ABC motorcycles]]
<!-- designs which were developed into or from this aircraft: -->
|related=
*[[ABC Scorpion]]
<!-- aircraft that are of similar role, era, and capability this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]

<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Armstrong Siddeley Ounce]]
*[[Bristol Cherub]]
*[[Walter Atom]]
<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==

===Notes===
{{reflist}}

===Bibliography===
{{refbegin}}
* Guttery, T.E. ''The Shuttleworth Collection of historic aeroplanes, cars, cycles, carriages and fire engines''. Biggleswade, Bedfordshire: The Collection, 1969. ISBN 978-0-901319-01-2
* Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
{{refend}}

==External links==
{{Commons category|ABC aircraft engines}}

{{ABC aero engines}}

{{DEFAULTSORT:Abc 8 Hp}}
[[Category:Aircraft piston engines 1920–1929]]
[[Category:ABC aircraft engines|8]]