{{Use dmy dates|date=August 2014}}
{{Use British English|date=August 2014}}
<!-- Unsourced image removed: [[File:Makingwaves.jpg|thumb|right|300px|[[Commanding Officer|CO]], Commander Martin Brooke ([[Alex Ferns]]) and [[Executive Officer|XO]], Lieutenant Commander Jenny Howard ([[Emily Hamilton]]).]] -->
{{Infobox television
| show_name            = Making Waves
| image                = Making Waves title card.jpg
| caption              = Title card
| genre                = [[Drama]]
| runtime              = Approx. 49 minutes per episode
| creator              = [[Ted Childs]]<br />[[Richard Maher]]
| producer             = Stephen Smallwood<br />Philip Shelley
| editor               = Tim Murrell
| executive_producer   = Ted Childs
| starring             = [[Alex Ferns]]<br />[[Emily Hamilton]]<br />[[Ian Bartholomew]]<br />[[Steve Speirs]]<br />[[Making Waves (TV series)#Characters|''Others'']]
| theme_music_composer = [[Simon Rogers]]
| country              = United Kingdom
| location             = United Kingdom
| language             = English
| network              = [[ITV (TV network)|ITV]]
| first_aired          = {{Start date|2004|7|7|df=yes}}
| last_aired           = {{Start date|2004|7|21|df=yes}}
| num_series           = 1
| num_episodes         = 6 (3 unaired)
| list_episodes        = Making Waves (TV series)#Episodes
}}

'''''Making Waves''''' is a British television drama series produced      by [[Carlton Television]] for [[ITV (TV network)|ITV]]. It was created by [[Ted Childs]] and chronicles the professional and personal lives of the crew of the [[Royal Navy]] [[frigate]] {{HMS|Suffolk||}}. The series remained in [[development hell]] for several years and was first broadcast on 7 July 2004. However, due to low ratings it was removed from the schedules after only three episodes, the remainder of the series going unaired on television in the United Kingdom.

The series starred [[Alex Ferns]] as Commander Martin Brooke and [[Emily Hamilton]] as Lieutenant Commander Jenny Howard. The frigate [[HMS Grafton (F80)|HMS ''Grafton'']] stood in for ''Suffolk'' and additional filming took place around [[HMNB Portsmouth]] with the full co-operation of the Royal Navy. A limited-edition DVD of all six episodes was released in December 2004.

== Production ==

=== Development ===
Following the success of his previous series ''[[Soldier Soldier]]'', Ted Childs, [[Richard Maher]] and Carlton began planning the series in the late 1990s, with Carlton's controller of drama and continuing series expecting something "in the ''[[London's Burning (TV series)|London's Burning]]'', ''[[Peak Practice]]'' vein".<ref name="astonbroadcast">{{cite news |first= Steve|last= Aston|title= Navy drama will be ITV flagship|url= http://www.broadcastnow.co.uk/broadcastnowArticle.aspx?intStoryID=123887|work= Broadcast Now (subscription)|publisher= |date= 2002-07-19|accessdate= 2007-03-14 |archiveurl = https://web.archive.org/web/20070927205323/http://www.broadcastnow.co.uk/broadcastnowArticle.aspx?intStoryID=123887 |archivedate = 27 September 2007|deadurl=yes}}</ref> It was turned down by ITV because it was "old-fashioned" and did not fit in with the network's existing portfolio of dramas.<ref name="liddimentguardian">{{cite news |first= David|last= Liddiment|title= The story of a sunken drama|url= http://media.guardian.co.uk/mediaguardian/story/0,,1273913,00.html|work= [[The Guardian]]|publisher= |date= 2004-08-02|accessdate= 2007-03-14 }}</ref> Despite this, Childs and Carlton continued to develop the series and they brought it to the commissioning editors of [[BBC One]], who negotiated with Carlton to broadcast it. By that time, the top levels of the ITV drama department had changed and due to Carlton's links with ITV, the BBC was unable to take the series.<ref name="childsguardian">{{cite news |first= Ted|last= Childs|title= Lost with all hands|url= http://media.guardian.co.uk/mediaguardian/story/0,,1283722,00.html|work= The Guardian|publisher= |date= 2004-08-16|accessdate= 2007-03-14 }}</ref> A six-episode series was commissioned in July 2002 by Nick Elliott at ITV.<ref name="astonbroadcast"/>

The project was initially managed by the MoD's Directorate of Corporate Communications (Navy), headed by Commodore [[Richard Leaman]].  Lieutenant Commander Steve Tatham undertook much of the initial scoping and planning work before, in August 2002, Commander Kevin Fincher was appointed as the specialist project officer for the series; he would acquire the necessary ships, locations and personnel as well as advising the production team on and off set. Throughout pre-production Fincher negotiated a legal agreement with Carlton, whereby a financial recovery was made for anything they used that was taxpayer-funded. This included use of ships, fuel, and personnel. Another clause gave the Royal Navy a share of any royalties from the series, including advertising revenue and sales.<ref name="fincheraccount">{{cite web |first= Kevin|last= Fincher|title= The Making of Making Waves|url= http://www.royal-navy.mod.uk/static/pages/7030.html|publisher= Royal Navy|date= 2004-06-27|accessdate= 2007-05-02 |archiveurl=https://web.archive.org/web/20040818185425/http://www.royal-navy.mod.uk/static/pages/7030.html |archivedate=2004-08-18}}</ref>

=== Writing ===
There were four credited writers on the Carlton staff, with writers from the ITV network centre involved in the development of the scripts.<ref name="childsguardian"/> The first episode was written in 2002 by Terry Cafolla, who later wrote ''[[Messiah (TV series)|Messiah IV: The Harrowing]]''.<ref name="jonesprofile">{{cite web |url= http://www.culturenorthernireland.org/article.aspx?county=0&articleID=1300&cultID=0&townID=0&cultSubID=0&page=0&navID=5|title= Profile of the award winning writer of ''Holy Cross''|accessdate= 2007-03-24 |last= Jones|first= Francis|date= 2006-04-26|publisher= Culture Northern Ireland }}</ref> The second episode was written by Damian Wayling of ''[[The Bill]]'', and the third by Niall Leonard. Matthew Bardsley was the credited writer of the three unaired episodes. Although a second series was not made, storylines were planned for a potential commission.<ref>{{cite web |url= http://www.pfd.co.uk/clients/bardslem/f-ftw.html|title= Matthew Bardsley CV|date=n.d.|accessdate=2007-03-24|work=PFD |archiveurl = https://web.archive.org/web/20071111012310/http://www.pfd.co.uk/clients/bardslem/f-ftw.html |archivedate = 11 November 2007|deadurl=yes}}</ref> Warrant Officer Dave Allport and Leading Seaman Sarah Worthy joined Fincher as advisers in January 2003.<ref name="fincheraccount"/>

=== Casting ===
Actors were auditioned and hired in late 2002, and included [[Alex Ferns]], well-known to British audiences for his role as [[Trevor Morgan (EastEnders)|Trevor Morgan]] in the BBC [[soap opera]] ''[[EastEnders]]''.<ref name="astonbroadcast"/> ''Making Waves'' was the first lead role for Ferns, who had taken time off from television acting since leaving ''EastEnders'' to avoid being typecast in a soap role, and was pleased not to be playing "a psychotic rapist".<ref name="mottesun">{{cite news |first= Adrian|last= Motte|title= Big splash for Alex|url=http://www.thesun.co.uk/sol/homepage/showbiz/tv/209093/Big-splash-for-Alex.html|work= [[The Sun (newspaper)|The Sun]]|publisher= |date= 2004-07-03 |accessdate=2007-03-14}}</ref><ref name="eviltrevor">{{cite news |last= Staff writer|title= Evil Trevor doesn't scare Pompey kids|url= http://www.portsmouthtoday.co.uk/ViewArticle.aspx?SectionID=455&ArticleID=532818|work= Portsmouth Today|publisher= |date= 2003-06-13|accessdate=2007-03-24 }}</ref> [[Emily Hamilton]], cast as [[executive officer]] Jenny Howard, was largely unknown to British television audiences at the time; her only notable role was in [[Russell T Davies]]'s ''The Grand'' in the late 1990s. [[Lee Boardman]] (who later appeared in ''[[Rome (TV series)|Rome]]'') took the role of the chef Art Francis to distance himself from his most well-known role, drug-dealer [[Jez Quigley]] in the soap opera ''[[Coronation Street]]''.<ref name="wyliemen1">{{cite news |first= Ian|last= Wylie|title= Plain sailing for Lee|url= http://www.manchestereveningnews.co.uk/entertainment/film_and_tv/s/122/122248_telly_talk_plain_sailing_for_lee.html|work= [[Manchester Evening News]]|publisher= |date=2004-06-25|accessdate=2007-03-14 }}</ref> 
[[Stephen Kennedy]] was already known for his role as Ian Craig in the [[BBC Radio 4]] soap opera ''[[The Archers]]''. He mentioned in an interview the differences between the two roles, before quipping that they were not that dissimilar.<ref>{{cite web |url= http://www.bbc.co.uk/radio4/archers/backstage/stephen_kennedy.shtml|title= BBC – Radio 4 – The Archers – Chef!|work= [[bbc.co.uk]]|date= 2004-02-27|accessdate= 2007-05-02 }}</ref> The crew of ''Grafton'' appeared as extras throughout the series and schoolchildren from St. Jude's School, [[Southsea]] appeared in the families day scenes in episode six.<ref name="eviltrevor"/><ref name="officialnavy">{{cite web |title= Navy's TV star heads for Gulf|url= http://www.royal-navy.mod.uk/server/show/conWebDoc.3783/changeNav/3533|publisher= Royal Navy|date=n.d.|accessdate=2007-03-24 | archiveurl = https://web.archive.org/web/20070930043604/http://www.royal-navy.mod.uk/server/show/conWebDoc.3783/changeNav/3533| archivedate = 30 September 2007}}</ref>

=== Filming ===
[[File:HMS Sutherland F81.jpg|thumb|300px|{{HMS|Sutherland|F81|6}} appeared in the opening scenes of episode one as HMS ''Wessex'', sister ship of HMS ''Suffolk''.<ref name="fincheraccount"/>]]
The series producers scouted [[Portsmouth]] in 2002 for ship locations. Potential main settings {{HMS|Marlborough|F233|6}} and {{HMS|Dryad|establishment|6}} were put aside in favour of HMS ''Grafton'' and filming commenced on 24 March 2003 with 30 actors and 60 crew moving onto the ship for the shoot.<ref name="fincheraccount"/><ref name="officialnavy"/><ref name="portsnews1">{{cite news |last= Staff writer|title= Navy is set to rule airwaves|url= http://www.portsmouthtoday.co.uk/ViewArticle.aspx?SectionID=455&ArticleID=249001|work= Portsmouth News|publisher= |date= 2002-08-02|accessdate=2007-03-24 }}</ref> Alex Ferns arrived two days earlier than the rest of the cast to settle in, and made a trip to [[RNAS Yeovilton (HMS Heron)|Yeovilton]] to meet [[BAE Sea Harrier|Harrier]] pilots, while Emily Hamilton prepared for her role by shadowing Vanessa Spiller, XO of {{HMS|Kent|F78|6}}.<ref name="wyliemen1"/><ref name="wyliemen2">{{cite news |first= Ian|last= Wylie|title= Why my rear got a cheer|url= http://www.manchestereveningnews.co.uk/entertainment/film_and_tv/s/124/124645_telly_talk_why_my_rear_got_a_cheer.html|work=  Manchester Evening News|publisher= |date= 2004-07-21|accessdate=2007-03-24 }}</ref> The series was directed by Matthew Evans and Nigel Douglas and was shot on digital [[DV]] cameras. The production staff filmed approximately three hours of footage on every 12-hour day, editing it using [[Avid Technology|Avid]] systems in the production offices at the naval base.<ref>{{cite news|last=Staff writer |title=Naval drama breaks new ground |url=http://www.navynews.co.uk/articles/2003/0306/0003060301.asp |date=2003-06-03 |work=Navy News |accessdate=2007-06-06 |deadurl=yes |archiveurl=https://web.archive.org/web/20051112183148/http://www.navynews.co.uk/articles/2003/0306/0003060301.asp |archivedate=12 November 2005 }}</ref>

Other vessels made cameos in the series; filming took place around and aboard {{HMS|Victory}} for scenes in episode two, and aerial footage of {{HMS|Invincible|R05|6}} and {{HMS|Gloucester|D96|6}} was done for the war games scenes in episode four.<ref name="portsnews2">{{cite news |last= Staff writer|title= Fleet's finest are small-screen stars|url= http://www.portsmouthtoday.co.uk/template/ViewArticle.aspx?SectionID=1080&ArticleID=821128|work= Portsmouth Today|publisher= |date= 2004-07-12|accessdate=2007-03-24 }}</ref> [[BNS Turag|HMS ''Lindisfarne'']] appeared for the funeral scenes in episode five.<ref name="portsnews2"/> Health and safety regulations required that ''Grafton'' was shadowed at all times by a support vessel during filming at sea, in the event of a member of the production team falling overboard.<ref name="fincheraccount"/>

Location filming lasted until 26 June 2003 before post-production was completed in [[London]], and the series was delivered to ITV in August 2003 for broadcast in the autumn schedules, though it would be held back for several months.<ref name="childsguardian"/><ref name="fincheraccount"/> A special preview screening of episode one was held aboard {{HMS|Richmond|F239|6}} on 13 February 2004, while the ship was in [[Aberdeen]] on a recruitment and promotional tour of the UK.  It was also premièred aboard {{HMS|Northumberland|F238|6}} during the same month whilst at the London Boat Show, and was attended by much of the cast and crew.<ref>{{cite news |last= Staff writer|title= FRIGATE'S CREW GETS PREVIEW OF DRAMA|work= Aberdeen Press & Journal|publisher= |date= 2004-02-14}}</ref><ref>{{cite news |last= Staff writer|title= Navy makes waves in city as warship draws crowds|work= Aberdeen Press & Journal|publisher= |date= 2004-02-16}}</ref>

== Setting ==

=== Plot ===
HMS ''Suffolk'' is due for Flag Officer Sea Training in four weeks but an accident during training results in the dismissal of the executive officer and the resignation of the captain. The series follows his replacement, Commander Martin Brooke, in his attempts to get his vessel and crew ship-shape for final assessment. Other storylines follow Leading Marine Engineer Artificer (LMEA) Dave Finnan's relationship with his Charge Chief's daughter Teresa, the emotional state of Mickey Sobanski after a blundered rescue operation, new rating Rosie Bowen settling into life on her first ship and the budding cross-ranks relationship between officer Sam Quartermaine and medic Anita Cook. Comic relief is provided in the characters of "Scouse" Phillips and Leading Chef Art Francis. ''Suffolk'' is mainly based at Portsmouth but engages in exercises such as war games throughout the series, as well as undertaking hazardous rescues of other vessels in the [[English Channel]].

=== Episodes ===
{| class="wikitable plainrowheaders" style="width:99%; margin:auto;"
|-
! width="20"|# !! Title !! width="120"| Writer !! width="120"| Director !! width="120"|Viewers !! width="120"| Airdate
{{Episode list
 |EpisodeNumber=1
 |Title=Episode 1
 |Aux1=Terry Cafolla
 |Aux2=Matthew Evans
 |Aux3=5.83 million<ref name="liddimentguardian"/>
 |OriginalAirDate={{Start date|2004|7|7|df=y}}
 |ShortSummary= After a joint exercise results in the death of an officer from another ship, Commander Martin Brooke takes command of HMS ''Suffolk'' to get the ship and her crew ready for final assessment. The new XO, Jenny Howard, contemplates her future in the Navy. "Scouse" Philips celebrates his birthday with Mickey Sobanski, but is trapped at sea the next day. Andy Fellows becomes a grandfather, oblivious that the father is his shipmate.
 |LineColor=
}}
{{Episode list
 |EpisodeNumber=2
 |Title=Episode 2
 |Aux1=Damian Wayling
 |Aux2=Matthew Evans
 |Aux3=4.39 million<ref name="liddimentguardian"/>
 |OriginalAirDate={{Start date|2004|7|14|df=y}}
 |ShortSummary=A visit from an [[Argentina|Argentine]] naval delegation prompts Brooke to step up training. Fellows worries about failing his fitness test and being transferred off the ship. Howard accepts Brooke's offer to be made permanent XO. Sobanski struggles over his inaction that led to a young boy's death. He receives a disciplinary hearing and is later landed from the ship by Brooke. Finnan vows to see Teresa and baby Janey every day.
 |LineColor=
}}
{{Episode list
 |EpisodeNumber=3
 |Title=Episode 3
 |Aux1=Niall Leonard
 |Aux2=Matthew Evans
 |Aux3=3.96 million<ref name="liddimentguardian"/>
 |OriginalAirDate={{Start date|2004|7|21|df=y}}
 |ShortSummary=Buffer is accused of assaulting a female crewmember on a stricken protest ship aided by ''Suffolk'', and his career is endangered. Sobanski attends counselling and later confesses his secret to Bowen. Leading Medical Assistant (LMA) Anita Cook tends to a minor injury Sam Quartermaine received. Buffer's career is safe when the alleged victim of his attack reveals her captain made the story up. Finnan asks Teresa to marry him.
 |LineColor=
}}
{{Episode list
 |EpisodeNumber=4
 |Title=Episode 4
 |Aux1=Matthew Bardsley
 |Aux2=Nigel Douglas
 |Aux3=—
 |OriginalAirDate={{Start date|Unaired<ref>Scheduled to air at 9 p.m. on 28 July 2004 but pulled that morning.</ref>}}
 |ShortSummary=''Suffolk'''s assessment begins and Brooke learns a man from his past will be leading it. Finnan discovers Teresa has been running up a massive credit debt and in anger she calls off the wedding. ''Suffolk'' is assigned to lead the task force in a mock war and a fire breaks out below decks. Fellows remains to fight it and he is sealed in when the compartment is flooded with [[bromotrifluoromethane|BTM]]. He is airlifted off the ship but later dies in hospital.
 |LineColor=
}}
{{Episode list
 |EpisodeNumber=5
 |Title=Episode 5
 |Aux1=Matthew Bardsley
 |Aux2=Nigel Douglas
 |Aux3=—
 |OriginalAirDate={{Start date|Unaired}}
 |ShortSummary=Fellows's ashes are [[burial at sea|scattered at sea]] while Finnan is [[Court martial#Courts-martial in the United Kingdom|court martialled]] for striking Lewis and sentenced to 90 days imprisonment. A board of inquiry finds no fault in the events that led to Fellows's death, but Brooke discovers negligent maintenance by Lewis led to the fire. Lewis is escorted from the ship by Scouse and Finnan's sentence is reduced to 28 days after an appeal by Brooke. Before he is taken away, he and Teresa marry.
 |LineColor=
}}
{{Episode list
 |EpisodeNumber=6
 |Title=Episode 6
 |Aux1=Matthew Bardsley
 |Aux2=Nigel Douglas
 |Aux3=—
 |OriginalAirDate={{Start date|Unaired}}
 |ShortSummary=Brooke learns ''Suffolk'' has passed FOST and will soon be deployed. Finnan returns from prison meets his new Charge Chief, who shows him and Teresa a memorial to Andy. Sam and Anita's affair is revealed and Brooke has Sam landed from the ship when Sam appears to lie to him about wanting to marry her. The crew is elated when Brooke informs them that their first deployment will be in the [[Caribbean]].
 |LineColor=
}}
|}

== Characters ==
''Making Waves'' featured an ensemble of actors but followed a core cast, with supporting players appearing in only a few episodes or having secondary storylines.

Commander Martin Brooke (played by [[Alex Ferns]]) is the son of a car mechanic and his naval background is based on piloting, rather than commanding a ship. He is assisted by Lt Cdr Jenny Howard (played by [[Emily Hamilton]]), who is initially his temporary XO, but eventually accepts Brooke's offer to stay on the ship. Lieutenant Commander William Lewis, the Marine Engineering Officer (played by [[Ian Bartholomew]]), is the superior of Charge Chief Marine Engineering Artificer (CCMEA) Andy Fellows (played by [[Steve Speirs]]) and Lewis's refusal to give the engines full maintenance regularly infuriates him, though not as much as LMEA Dave Finnan (played by [[Paul Chequer]]) who has just had a baby with his daughter Teresa (played by [[Chloe Howman]]). New Operator Mechanic Rosie Bowen (played by [[Joanna Page]]) settles into her first posting and attracts the attention of OM Mickey Sobanski (played by [[Lee Turnbull (actor)|Lee Turnbull]]), who is contemplating his future in the Navy after an incident in the first episode.

The second episode introduces the new navigating officer Lieutenant Sam Quartermaine (played by [[Adam Rayner]]) and a subplot involving his relationship with LMA Anita Cook (played by [[Angel Coulby]]) runs through the series and is eventually discovered by Lieutenant James Maguire, the Principal Warfare Officer (played by [[Stephen Kennedy]]). Terry "Buffer" Duncan's (played by [[Geoff Bell (actor)|Geoff Bell]]) career is in jeopardy when an accusation of assault is thrown at him in the third episode, while Leading Regulator Liz Wilson (played by [[Diane Beck]]) develops an unreciprocated crush on Bowen. Leading Chef Art Francis (played by [[Lee Boardman]]) must successfully prepare dinner for the captain and crew before it is stolen or ruined by Steward Tim "Scouse" Phillips (played by [[Darren Morfitt]]).

== Series information ==

=== Broadcast history ===
Carlton delivered the series to ITV in August 2003 for broadcast in the autumn schedules, but it was then rescheduled four times over the next several months, before ITV eventually set a premiere date of 11 July 2004.<ref name="childsguardian"/> It was then rescheduled to the preceding Wednesday, in the 9 pm slot.<ref name="childsguardian"/> The series lost two million viewers over two weeks and ITV pulled episode four from the schedules on the morning before it was due for broadcast, replacing it with ''It Shouldn't Happen on a TV Soap'', a [[blooper]]s programme, which returned ITV's ratings to above the five million mark.<ref name="liddimentguardian"/>

Writing in ''[[The Guardian]]'' a fortnight later, ITV head of drama [[David Liddiment]] defended the decision, stating that the network had planned to let ''Making Waves'' profit from ''[[The Bill]]'''s (the lead-in) high ratings at a time when BBC One Wednesday night ratings were suffering, but the series just "wasn't good enough" to hold an audience.<ref name="liddimentguardian"/> Ted Childs later responded that ''Making Waves'' had received little publicity compared to [[Channel 4]]'s ratings smash ''[[Supernanny]]'', which aired opposite his series, and that because that programme had ended its run, the ratings for the last three episodes might have improved. He went on to question why ITV had spent [[Pound sterling|£]]5 million on a series they knew would not be a hit.<ref name="childsguardian"/> ''Making Waves'' had been another in a line of expensive series which had been cancelled because they performed below ITV's expectations in the ratings, following ''[[Sweet Medicine]]'' and ''Family'' the previous year.<ref name="liddimentguardian"/>

The cancellation of the series also drew criticism from the Royal Navy, with a source telling ''[[The Sun (newspaper)|The Sun]]'' that it was "a kick in the teeth to our sailors".<ref name="dunnsun">{{cite news |first= Tom|last= Newton Dunn|title= Fury of ITV chop for Navy|url=http://www.thesun.co.uk/sol/homepage/news/92316/Fury-of-ITV-chop-for-Navy.html|work= The Sun|publisher= |date= 2004-07-28|accessdate=2007-03-24}}</ref> In its end-of-year review, ITV described the series as having "quality and distinctiveness" but failing "to find a mass audience".<ref>{{cite web |url=http://www.itv.com/uploads/files/1109330579125_0.19631474734302934.pdf |title=ITV1 2004 Review/2005 Statement |accessdate=2007-03-24 |format= PDF|publisher= ITV plc }}</ref>

=== Critical reception ===
Due to the series coming from the producer of ''[[Soldier Soldier]]'' and ''[[Kavanagh QC]]'', the close involvement of the Royal Navy and the lead being taken by Alex Ferns, there was a strong media interest in the series, which only intensified as the series went through its many reschedulings. Positive previews were run by the ''[[Manchester Evening News]]''<ref name="wyliemen1"/> and ''The Sun''.<ref name="mottesun"/> ''[[The Times]]'' predicted "ITV has a ratings winner" and called the series a "classic military soap opera".<ref>{{cite news |first= David|last= Chater|title= TV Choice|work= The Times|publisher= |date= 2004-07-03}}</ref> The tabloid press was keen for the series to succeed; ''[[The Daily Mirror]]'' described it as promising and conjectured that it would be the defining role of Alex Ferns's career,<ref>{{cite news |first= Jill|last= Foster|title= Shipshape naval yard|work= [[The Daily Mirror]]|publisher= |date= 2004-07-07}}</ref> although its [[Sunday Mirror|Sunday equivalent]] did not share the sentiment: it described the series as a "seabed-bound disaster".<ref>{{cite news |first= Ian|last= Hyland|title= Critic of the week|url=http://www.highbeam.com/doc/1G1-119196169.html|work= Sunday Mirror |date= 2004-07-11|accessdate=2007-03-24}}</ref> The same writer criticised the script and directing of episode three and suggested that viewers were not interested in a naval series set in peacetime.<ref>{{cite news |first= Ian|last= Hyland|title= Drowned at berth|url=http://www.highbeam.com/doc/1G1-119720076.html|work= Sunday Mirror |date= 2004-07-25|accessdate=2007-03-24}}</ref>

''[[The Scotsman]]'' dismissed it as little more than a six-part recruitment video, comparing scenes of refugees being lifted to real advertisements that showcased the navy's role in humanitarian crises, and concluded that the drama was a "collection of clichés and stilted dialogue".<ref>{{cite news |first= Stephen|last= McGinty|title= All this spin is|work= The Scotsman|publisher= |date= 2004-07-08}}</ref> ''[[The Independent on Sunday]]'' compared the series to the sea-based soap opera ''[[Triangle (1981 TV series)|Triangle]]'' and noted "an overdependence on claustrophobic interiors". However, the series was wryly praised for casting Alex Ferns instead of [[Ross Kemp]] in the lead role, bucking the trend of [[Ultimate Force|recent ITV military series]] and commented on the difficult time slot the series had been given.<ref>{{cite news |first= Charlie|last= Coultauld|title= Fed up with reality? Then try gangland...|work= The Independent on Sunday|date= 2004-07-11|pages=18–19}}</ref> Over two years after the series was pulled, Alex Ferns admitted that it was formulaic, but blamed its failure on the constant rescheduling.<ref>{{cite news |first= Wil|last= Marlow|title= Master of menace Alex Ferns is back|url=http://living.scotsman.com/tv.cfm?id=1330072006|work= The Scotsman|publisher= |date= 2006-09-08|accessdate=2007-04-28 }}</ref>

=== DVD release ===
The [[British Board of Film Classification|BBFC]] passed the series for video release on 3 December 2004, rating episodes three and four as [[British Board of Film Classification#Current certificates|PG]],<ref>British Board of Film Classification (2004-12-03). [http://www.bbfc.co.uk/website/Classified.nsf/0/F62FEE732D50FC7880256F600020FB6F Episode 3], [http://www.bbfc.co.uk/website/Classified.nsf/0/BBB692E322A805AD80256F600020FB73 Episode 4]. Retrieved on 2007-03-24</ref> and the rest as [[British Board of Film Classification#Current certificates|12]].<ref>British Board of Film Classification (2004-12-03). [http://www.bbfc.co.uk/website/Classified.nsf/0/F097A5F9936B73AA80256F600020FB5E Episode 1], [http://www.bbfc.co.uk/website/Classified.nsf/0/B1F1CE290C50A63E80256F600020FB66 Episode 2], [http://www.bbfc.co.uk/website/Classified.nsf/0/870875325B83952780256F600020FB76 Episode 5], [http://www.bbfc.co.uk/website/Classified.nsf/0/624A9A4FC3A2059880256F600020FB79 Episode 6]. Retrieved on 2007-03-24</ref> Granada Ventures pressed approximately 2,500 two-disc sets of the series for sale exclusively on the Navy News website and it went on sale in December 2004, with 2,000 sets being purchased within a month.<ref>{{cite news |last=Staff writer |title= DVD sales mean new wave of success|url= http://www.portsmouthtoday.co.uk/ViewArticle.aspx?SectionID=455&ArticleID=915543|work= Portsmouth Today|publisher= |date= 2005-01-07|accessdate=2007-03-24 }}</ref>

== See also ==
* [[Naval officer ranks]]
* [[Royal Navy officer rank insignia]]
* [[Warship (TV series)|''Warship'' (TV series)]]

== References ==
{{Reflist|30em}}

== External links ==
* [https://web.archive.org/web/20041112175337/www2.itv.com/makingwaves/index.html ''Making Waves''] at [[ITV (TV network)|ITV.com]] (archive)
* [http://ftvdb.bfi.org.uk/sift/series/37671 ''Making Waves''] at the [[British Film Institute]]
* {{IMDb title|id=0390740|title=Making Waves}}
* {{tv.com show|making-waves|Making Waves}}

{{Featured article}}

{{DEFAULTSORT:Making Waves (Tv Series)}}
[[Category:2000s British drama television series]]
[[Category:2004 British television programme debuts]]
[[Category:2004 British television programme endings]]
[[Category:ITV television dramas]]
[[Category:Military television series]]
[[Category:Royal Navy]]
[[Category:Carlton Television]]
[[Category:Television series by ITV Studios]]
[[Category:English-language television programming]]
[[Category:Television shows set in Hampshire]]