{{Infobox journal
| title = European Journal of English Studies
| cover = [[File:EJES cover.jpg]]
| editor = Martin A. Kayman, Angela Locatelli, Ansgar Nünning
| discipline = [[English language]], [[English literature]]
| abbreviation = Eur. J. Engl. Stud.
| publisher = [[Routledge]]
| country =
| frequency = Triannually
| history = 1997–present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.tandf.co.uk/journals/titles/13825577.asp
| link1 = http://www.tandfonline.com/toc/neje20/current
| link1-name = Online access
| link2 = http://www.tandfonline.com/loi/neje20
| link2-name = Online archive
| JSTOR = 
| OCLC = 37135308
| LCCN = 
| CODEN = 
| ISSN = 1382-5577
| eISSN = 1744-4233
}}
The '''''European Journal of English Studies''''' is a [[Peer review|peer-reviewed]] [[academic journal]] focusing on [[English language]], literature and culture, established in 1997 and published by [[Routledge]]. It is the official journal of the [[European Society for the Study of English]]. The current [[editors-in-chief]] are Martin A. Kayman, Angela Locatelli, and Ansgar Nünning. The journal appears three times a year and the individual issues are devoted to specific topics, e.g.:
* Letters and letter writing
* New Englishes
* Intercultural negotiations
The aim of the journal is to reflect the multicultural perspective characterising the current state of English studies research in Europe, while encouraging dialogue and not avoiding controversial topics.<ref>{{Cite journal | last1 = Belsey | first1 = C. | last2 = Grabes | first2 = H. | last3 = Lecercle | first3 = J. J. | title = Cutting EJES: the Editorial Policy of the European Journal of English Studies | journal = European Journal of English Studies | volume = 1 | pages = 3 | year = 1997 | doi = 10.1080/13825579708574374}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://www.tandf.co.uk/journals/titles/13825577.asp}}

[[Category:British literary magazines]]
[[Category:Linguistics journals]]
[[Category:Publications established in 1997]]
[[Category:English-language journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:Triannual journals]]
[[Category:Academic journals associated with international learned and professional societies of Europe]]


{{ling-journal-stub}}