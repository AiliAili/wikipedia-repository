{{Use dmy dates|date=April 2014}}
{{Use British English|date=April 2014}}
<!-- This article is a part of [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name          = Harold Day
| image         =
| caption       =
| birth_date    = {{Birth date|df=yes|1897|4|17}}
| death_date    = {{Death date and age|df=yes|1918|2|5|1897|4|17}}
| birth_place   = [[Abergavenny]], Wales
| death_place   = Vicinity of [[Harnes]], France
| placeofburial_label = 
| placeofburial = St. Mary's A.D.S Cemetery, [[Haisnes]], [[Pas de Calais]], France
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    = United Kingdom
| branch        = Royal Navy
| serviceyears  = 1917–1918
| rank          = [[Flight Sub-Lieutenant]]
| unit          = [[No. 210 Squadron RAF|No. 10 Squadron RNAS]]<br/>[[No. 208 Squadron RAF|No. 8 Squadron RNAS]]
| commands      =
| battles       =
| awards        = [[Distinguished Service Cross (United Kingdom)|Distinguished Service Cross]]
| relations     =
| laterwork     =
}}
Flight Sub-Lieutenant '''Harold Day''' {{post-nominals|country=GBR|size=100%|DSC}} (17 April 1897 – 5 February 1918) was a Welsh World War I [[flying ace]] credited with 11 confirmed aerial victories.<ref name="theaerodrome">{{cite web |url=http://www.theaerodrome.com/aces/wales/day1.php |title=Harold Day |work=The Aerodrome |year=2014 |accessdate=18 November 2014 }}</ref>

==World War I==
Day was the son of William John and Elizabeth Day, of Wernddu, [[Abergavenny]], [[Monmouthshire]].<ref name="CWGC">{{cite web |url= http://www.cwgc.org/find-war-dead/casualty/324940/DAY,%20HAROLD |title=Day, Harold |work=Commonwealth War Graves Commission |year=2014 |accessdate=17 November 2014}}</ref> He joined the RNAS as a probationary temporary flight officer, and was commissioned as a temporary flight sub-lieutenant on 30 June 1917.<ref>{{London Gazette |issue=30261 |date=31 August 1917 |startpage=9033 |nolink=yes }}</ref>

He first served in No. 10 Naval Squadron, flying a [[Sopwith Triplane]]. He scored his first aerial victory with them, sending an [[Albatros D.V]] down out of control on 12 August 1917. He was then posted to No. 8 Naval Squadron and flew the [[Sopwith Camel]] for his remaining ten victories. During December 1917, he teamed with [[Guy William Price]] to drive down [[Deutsche Flugzeug-Werke|DFW]] two-seater reconnaissance aircraft on three different days. Fighting solo, Day destroyed one enemy aircraft and drove down three others during January 1918. On 2 February 1918, he joined [[Robert J. O. Compston]] and three other RNAS pilots in destroying a German reconnaissance aircraft, and in driving down an Albatros D.V later in the day. On 5 February, he joined three other RNAS pilots in destroying a reconnaissance machine; that brought his total to one enemy aircraft destroyed solo, two more shared, and eight driven down out of control. He then dived on another German aircraft.<ref name="theaerodrome"/> Day's Camel came to pieces during the dive, plummeting him to his death.<ref>Franks (2003), p.23.</ref> Günther Schuster of ''[[Jagdstaffel 29|Jasta 29]]'' was credited with the victory.<ref name="theaerodrome"/> 

Day was awarded the Distinguished Service Cross, which was gazetted on 22 February 1918. 

He is buried at St. Mary's ADS (Advanced Dressing Station) Cemetery, [[Haisnes]], France.<ref name="CWGC"/>

==Honours and awards==
;Distinguished Service Cross
:Flt. Sub-Lieut. Harold Day, R.N.A.S.
:In recognition of the skill and determination shown by him in aerial combats, in the course of which he has done much to stop enemy artillery machines from working. On 6 January 1918, he observed a new type enemy aeroplane. He immediately dived to attack, and after a short combat the enemy machine went down very steeply, and was seen to crash. On several other occasions he has brought down enemy machines out of control.<ref>{{London Gazette |issue=30536 |date=19 February 1918 |startpage=2305 |supp=yes |nolink=yes }}</ref>

==References==
;Notes
{{reflist|30em}}
;Bibliography
* {{cite book |first=Norman |last=Franks |authorlink=Norman Franks |title=Sopwith Camel Aces of World War I: Volume 52 of Aircraft of the Aces |location=London, UK |publisher=Osprey Publishing |year=2003 |isbn=978-1-84176-534-1 }}


{{wwi-air}}

{{DEFAULTSORT:Day, Harold}}
[[Category:1897 births]]
[[Category:1918 deaths]]
[[Category:British World War I flying aces]]
[[Category:People from Abergavenny]]
[[Category:Recipients of the Distinguished Service Cross (United Kingdom)]]
[[Category:Royal Naval Air Service aviators]]
[[Category:British military personnel killed in World War I]]