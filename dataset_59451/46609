{{Use dmy dates|date=September 2013}}
{{Use British English|date=September 2013}}
'''Dorothy Bohm''' is a [[photographer]] based in [[London]], known for her portraiture, street photography, early adoption of colour, and photography of London and Paris; she is considered one of the [[doyen]]nes of British photography.<ref>Jennifer Boyd, "[http://www.manchestergalleries.org/dorothybohm/biography/ Biography and life in Manchester]", Manchester Art Gallery. Accessed 1 July 2012.</ref>

==Life and career==
Bohm was born '''Dorothea Israelit''' in 1924 in [[Königsberg]], [[East Prussia]] (now [[Kaliningrad]], Russia), to a German-speaking family of Jewish-Lithuanian origins.<ref>Colin Ford, "Dorothy Bohm: A Life in Photography", David Hawkins, ed., ''A World Observed, 1940&ndash;2010: Photographs by Dorothy Bohm'' (London: Philip Wilson; Manchester: Manchester Art Gallery, 2010), p.11.</ref> From 1932 to 1939 she lived with her family in Lithuania, first in Memel (now [[Klaipėda]]) and later in [[Šiauliai]]. She was sent to [[England]] in 1939 to escape Nazism: first to a boarding school in [[Ditchling]], Sussex, but London soon to Manchester, where her brother was a student, and where she met Louis Bohm (whom she would marry in 1945).<ref name="manmemoir">Dorothy Bohm, "[http://www.manchestergalleries.org/dorothybohm/biography/memoir/ Manchester Memoir]" (2007), Manchester Art Gallery. Accessed 1 July 2012.</ref><ref name="dossier">[http://carnavalet.paris.fr/sites/default/files/dossier_de_presse_dorothy_bohm.pdf Press dossier] (PDF) for the exhibition ''Un Amour de Paris'', Musée Carnavalet, 2005. {{Fr icon}} Accessed 1 July 2012.</ref>

Dorothy Bohm studied photography at the [[Manchester Municipal College of Technology]], from which she received a diploma; she also received a certificate in photography from [[City and Guilds of London Art School|City and Guilds]]. She working under the photographer Samuel Cooper for four years until, using her ''nom de guerre'' of '''Dorothy Alexander''', she set up her own portrait studio, Studio Alexander, in 1946.<ref name="manmemoir" /> (She would sell the studio in 1958.<ref name="dossier" />) Samples from this early portrait work would be exhibited decades later.<ref name="howweare">Val Williams and Susan Bright, eds, ''How We Are: Photographing Britain from the 1840s to the Present'' (London: Tate Publishing, 2007; ISBN 978-1-85437-714-2), pp.&nbsp;106, 107, 229.</ref>

Bohm's husband worked for a petrochemical company that obliged him to move around the world.<ref name="manmemoir" /> In 1947 she made the first of several visits to Paris, where she lived with her husband from 1954 to 1955. In the 1950s she also lived in New York and San Francisco,<ref name="dossier" /> in 1956 travelling to Mexico, where she photographed in [[Color photography|colour]] for the first time.<ref name="manobserved">[http://www.manchestergalleries.org/about-us/press-office/press-releases/index.php?itemID=35 Exhibition notice] for ''A World Observed 1940–2010'', Manchester Art Gallery, 1 March 2010. Accessed 1 July 2012.</ref> She has lived in [[Hampstead]] since 1956.<ref name="smyth" />

By the late 1950s, Dorothy Bohm had abandoned studio portraiture in favour of 'street photography', but was still working predominantly in [[Black-and-white|black and white]]; in 1980 she was persuaded by [[André Kertész]] to experiment with colour, which she did for two years using a [[Polaroid SX-70]] instant camera. She used colour negative film from 1984, and from 1985 worked exclusively in colour.<ref name="dossier" />

This early work of Bohm's has been described by Monica Bohm-Duchen as ‘humanist street photography, capturing the moment in the manner of [[Henri Cartier-Bresson]]’ whilst ‘people are often surprised by the youth and vibrancy of her colour work. She focuses on fragments of the urban landscape .&nbsp;.&nbsp;. that are otherwise overlooked. These photographs have an abstract quality; there’s a deliberate spatial ambiguity and you’re not quite sure what you’re looking at. But nothing is manipulated – she will still only work with film.’<ref>{{webarchive |url=https://web.archive.org/web/20100209063448/http://www.creativetourist.com/city-guides/bohm-photography-exhibition |date=9 February 2010 |title="A World Observed: Dorothy Bohm" }}, Creativetourist.com. Accessed 1 July 2012.</ref>

A major 1969 exhibition in the [[Institute of Contemporary Arts]] of photography by Bohm, [[Don McCullin]], [[Tony Ray-Jones]] and [[Enzo Ragazzini]] drew a response that encouraged one of its organizers, Sue Davies, to embark on a photography gallery for London.<ref>[[Peter Turner (writer and photographer)|Peter Turner]], ''History of Photography'' (Twickenham: Hamlyn, 1987; ISBN 0-600-50270-8), p.208.</ref> In 1971, Bohm co-founded [[The Photographers' Gallery]] in London with Davies; she worked as its Associate Director for the next 15 years, making many friends among the notable photographers who exhibited there.<ref name="dossier" /> She visited South Africa for five weeks in 1974, later exhibiting photographs taken there.<ref name="dossier" /> With Helena Kovac, she also founded the Focus Gallery for Photography in 1998; the gallery closed in 2004.<ref name="dossier" /><ref name="smyth">Diane Smyth, "[http://www.bjp-online.com/british-journal-of-photography/preview/1650061/dorothy-bohm The life and work of Dorothy Bohm] {{webarchive |url=https://web.archive.org/web/20130706094329/http://www.bjp-online.com/british-journal-of-photography/preview/1650061/dorothy-bohm |date=6 July 2013 }}", ''British Journal of Photography,'' 7 April 2010. Accessed 1 July 2012.</ref> She was awarded an honorary fellowship of the Royal Photographic Society in November 2009.<ref>[http://www.rps.org/annual-awards/Honorary-Fellowships List of honorary fellowships], Royal Photographic Society. Accessed 1 July 2012.</ref><ref>[http://www.photographyblog.com/news/royal_photographic_society_annual_awards/ RPS press release], photographyblog.com, 12 October 2009. Accessed 1 July 2012.</ref>

Dorothy Bohm has said about her work:

<blockquote>The photograph fulfils my deep need to stop things from disappearing. It makes transience less painful and retains something of the special magic, which I have looked for and found. I have tried to create order out of chaos, to find stability in flux and beauty in the most unlikely places.<ref name="manobserved" /></blockquote>

== Publications ==
*Dorothy Bohm. ''A World Observed''. London: Hugh Evelyn, 1970. With Introduction by [[Roland Penrose]].
*Dorothy Bohm and Ian Norrie. ''Hampstead: London Hill Town''. London: Wildwood House, 1981. ISBN 0704530600.
*Ian Norrie and Dorothy Bohm. ''A Celebration of London, Walks around the Capital''. London: André Deutsch, 1984. ISBN 0233975012.
**''Walks around London: A Celebration of the Capital.'' London: André Deutsch, 1986. ISBN 0233978534.
*''Dorothy Bohm Photographs''. Jerusalem: The Israel Museum, 1986. Text by Nissan Perez. ISBN 9652780499. Only black and white images of an exhibition that had included both black and white and color images.
*Dorothy Bohm. ''Egypt''. London: Thames & Hudson, 1989. ISBN 0500541507. With Foreword by [[Lawrence Durrell]] and text by [[Ian Jeffrey]].
*Dorothy Bohm. ''Venice''. London: Thames & Hudson, 1992. ISBN 050054171X. Text by [[Ian Jeffrey]].
*Dorothy Bohm. ''Colour Photography 1984&ndash;94''. London: The Photographers’ Gallery, 1994. ISBN 0907879446. Text by [[Ian Jeffrey]].
*Dorothy Bohm. ''Sixties London''. London: Lund Humphries / The Photographers’ Gallery, 1996. ISBN 0853316996. Texts by Amanda Hopkinson and [[Ian Jeffrey]].
*Dorothy Bohm. ''Walls and Windows''. London: Lund Humphries Publishers; Bath: The Royal Photographic Society, 1998. ISBN 0853317186. Texts by [[Mark Haworth-Booth]] and Monica Bohm-Duchen.
*Dorothy Bohm. ''Inside London''. London: Lund Humphries / The Photographers’ Gallery, 2000. ISBN 0853317801. Texts by Martin Harrison and Jessica Duchen.
*Dorothy Bohm. ''Breaks in Communication''. Göttingen: Steidl, 2002. ISBN 3882438134. Text by Martin Harrison.
*Mátyás Sárközy and Dorothy Bohm. ''Albion köd nélkül.'' Pécs: Jelenkor Kiadó, 2004. ISBN 9636763577.
*Dorothy Bohm. ''Un Amour de Paris''. Paris: Paris Musées, 2005. ISBN 2879008964. Texts by [[Mark Haworth-Booth]], Lynne Woolfson and Françoise Reynaud.
*Dorothy Bohm. ''Ambiguous Realities: Colour Photographs by Dorothy Bohm''. London: Ben Uri Gallery, 2007. ISBN 0900157100. Text by Monica Bohm-Duchen.
*David Hawkins, ed. ''A World Observed, 1940&ndash;2010: Photographs by Dorothy Bohm.'' London: Philip Wilson; Manchester: Manchester Art Gallery, 2010. ISBN 0856676888, ISBN 0901673765. Texts by Monica Bohm-Duchen, Colin Ford, and [[Ian Jeffrey]].
*Dorothy Bohm. ''About Women.'' Stockport, Cheshire: Dewi Lewis, 2016. ISBN 978-1-907893-81-0.

== Exhibitions ==

===Solo exhibitions===
*'''1969''' ''People at Peace'', part of ''Four Photographers in Contrast'' (with [[Don McCullin]], [[Tony Ray-Jones]] and [[Enzo Ragazzini]]) at [[Institute of Contemporary Arts]], London.<ref name="dossier" /><ref name="smyth" />
*'''1976''' Exhibition of London photographs at ll Diaframma Gallery, Milan.<ref name="dossier" /> A smaller version of this exhibition was also shown at the Marjorie Neikrug gallery in New York.<ref name="dossier" />
*'''1976''' ''Impressions of South Africa'', at [[The Photographers' Gallery]], London.<ref name="dossier" />
*'''1981''' ''A Sense of Place'', retrospective exhibition at the [[Camden Arts Centre]], London.<ref name="dossier" />
*'''1984''' Exhibition of Polaroid images of London at the Contrast Gallery, London.<ref name="dossier" />
*'''1986''' Retrospective at the [[Israel Museum]], Jerusalem, with a catalogue published by the Museum.<ref name="dossier" />
*'''1994''' ''Dorothy Bohm: Colour Photography 1984–94,'' [[The Photographers' Gallery]], London, with catalogue published by the Gallery.<ref name="dossier" />
*'''1997''' Exhibition of photographs from ''Sixties London'' at [[Museum of London]].<ref>[http://www.museumoflondon.org.uk/NR/rdonlyres/45CA1492-8978-4A2D-8DBE-FE38D632C471/0/MOLPastExhibitions.pdf List of past exhibitions] {{webarchive |url=https://web.archive.org/web/20130622165135/http://www.museumoflondon.org.uk/NR/rdonlyres/45CA1492-8978-4A2D-8DBE-FE38D632C471/0/MOLPastExhibitions.pdf |date=22 June 2013 }} (PDF), Museum of London, 8 July 2009, p.&nbsp;7. Accessed 1 July 2012.</ref>
*'''1998''' ''Walls and Windows'', exhibition of colour photographs 1994–1998 at [[Royal Photographic Society]], Bath and Royal National Theatre, London, with accompanying book published by Lund Humphries.<ref name="dossier" />
*'''1998''' Exhibition of still-life images at Artmonsky Arts, London.<ref name="dossier" />
*'''1999''' Retrospective at Focus Gallery (London).<ref name="dossier" />
*'''1999''' ''Colour Photographs'' at Focus Gallery (London).<ref name="dossier" />
*'''2002''' Exhibition of Hungarian images at Hungarian Cultural Institute, London.<ref name="dossier" />
*'''2002''' Exhibition of large colour photographs from ''Breaks in Communication'' at [[Victoria and Albert Museum]] and Focus Gallery, London.<ref name="dossier" />
*'''2003''' Exhibition of Hampstead images at Hampstead Museum, London.<ref name="dossier" />
*'''2005''' Photographs of Mexico at [[The Photographers' Gallery]], London.<ref name="dossier" />
*'''2005''' ''Un Amour de Paris'', major retrospective of Paris photographs 1947-2003, [[Musée Carnavalet]], Paris,<ref name="dossier" /><ref>[http://carnavalet.paris.fr/fr/expositions/un-amour-de-paris Exhibition notice], Musée Carnavalet. {{fr icon}} Accessed 30 June 2012.</ref><ref>"[http://www.camdennewjournal.co.uk/100605/r100605_04.htm Dorothy's street art] {{webarchive |url=https://web.archive.org/web/20160304194636/http://www.camdennewjournal.co.uk/100605/r100605_04.htm |date=4 March 2016 }}" (interview of Bohm by Ruth Gorb), ''Camden New Journal'', 2005. Accessed 30 June 2012.</ref><ref name="drean">Jeanne Dréan, "[http://www.20minutes.fr/paris/60473-Paris-Dorothy-Bohm-cinquante-ans-de-flamme-declaree-a-la-capitale.php Dorothy Bohm, cinquante ans de flamme déclarée à la capitale]", ''20 Minutes,'' 4 March 2006. {{Fr icon}} Accessed 1 July 2012.</ref> accompanied by catalogue in English and French published by Paris Musées.
*'''2005''' Exhibition of vintage black and white images of London and Paris, [[The Photographers' Gallery]], London.<ref>[http://thephotographersgallery.org.uk/3180/Dorothy-BohmLondon-and-Paris/1058 Exhibition notice], the Photographers' Gallery. Accessed 30 June 2012.</ref>
*'''2006''' Smaller version of Musée Carnavalet Paris exhibition shown at Das Verborgene Museum, Berlin<ref>[http://www.photography-now.com/popup_ausst_5.php?id_ausstellungen=T41494 Exhibition notice]{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}, photography-now.com. {{de icon}} Accessed 30 June 2012.</ref>
*'''2007''' ''Ambiguous Realities: Colour Photographs by Dorothy Bohm'', exhibition at [[Ben Uri Gallery]], London<ref>[http://www.benuri.org.uk/Ambiguous2.htm Notice of the extension of the exhibition] {{webarchive |url=https://web.archive.org/web/20101224045517/http://www.benuri.org.uk/Ambiguous2.htm |date=24 December 2010 }}, Ben Uri Gallery. Accessed 30 June 2012.</ref>
*'''2007''' ''Israel in Black and White: Photographs by Dorothy Bohm'', exhibition at Corman Arts, London<ref>"[http://www.thecnj.com/review/061407/feat061407_03.html Give peace a chance]", ''Camden New Journal'', 14 June 2007. Accessed 30 June 2012.</ref>
*'''2010''' ''A World Observed 1940–2010'', a major retrospective, [[Manchester City Art Gallery]]. 24 April – 30 August.<ref name="manobserved" /><ref name="smyth" /><ref>[http://www.manchestergalleries.org/dorothybohm/exhibition/ Exhibition notice], Manchester City Art Gallery. Accessed 30 June 2012.</ref>
*'''2010''' ''Dorothy Bohm Vintage Photographs'', exhibition at Zoe Bingham Fine Art, London.<ref>[http://www.artrabbit.com/all/events/event&event=19586 Exhibition notice], Artrabbit. Accessed 30 June 2012.</ref>
*'''2011''' ''A World Observed 1940–2010'', a major retrospective, [[Sainsbury Centre for Visual Arts]], Norwich<ref name="smyth" /><ref name="observedsainsbury">[http://www.scva.org.uk/exhibitions/archive/?exhibition=87 Exhibition notice] for "A World Observed", Sainsbury Centre for the Visual Arts, 2011. Accessed 30 June 2012.</ref>
*'''2012''' ''Seeing and Feeling'', Margaret Street Gallery, London<ref>30 July - 29 September {{cite web|url=http://margaretstreetgallery.com/Current-Exhibition |title=Archived copy |accessdate=2012-08-24 |deadurl=yes |archiveurl=https://web.archive.org/web/20130930133346/http://margaretstreetgallery.com/Current-Exhibition |archivedate=30 September 2013 |df=dmy }}</ref>
*'''2016''' ''Sixties London'', Jewish Museum, London

===Selected group exhibitions===
*'''1978''' ''Paris Seen'', [[Graves Art Gallery]], Sheffield.<ref name="dossier" /><!--
*'''1979''' ''Vintage Paris'', Hamilton Gallery<ref name="dossier" /> Source doesn't say where this is. (There are several "Hamilton Galleries" in 2012.) -->
*'''1989''' ''City Lights'', [[Goldsmiths, University of London|Goldsmiths' College]], London<ref name="dossier" />
*'''2003''' ''London Cultural Capital'', [[The Photographers' Gallery]], London<ref name="dossier" /><ref>[http://legacy.london.gov.uk/view_press_release.jsp?releaseid=1741 Press release] {{webarchive |url=https://web.archive.org/web/20120728141217/http://legacy.london.gov.uk/view_press_release.jsp?releaseid=1741 |date=28 July 2012 }}, London website, 2 May 2003. Accessed 1 July 2012.</ref>
*'''2007''' ''How We Are: Photographing Britain'', [[Tate Britain]], London.<ref name="howweare" />
*'''2012''' ''Another London: International Photographers Capture City Life 1930-1980'', [[Tate Britain]], London <ref>http://www.tate.org.uk/whats-on/tate-britain/exhibition/another-london</ref>

==Permanent collections==
*[[Victoria and Albert Museum]] (London)<ref>[http://collections.vam.ac.uk/search/?listing_type=imagetext&offset=0&limit=15&narrow=0&extrasearch=&q=Dorothy+Bohm&commit=Search&quality=0&objectnamesearch=&placesearch=&after=&after-adbc=AD&before=&before-adbc=AD&namesearch=&materialsearch=&mnsearch=&locationsearch= Search results], Victoria and Albert Museum. Accessed 1 July 2012.</ref>
*[[Tate]] (London and elsewhere)<ref>Olivier Laurent, "[http://www.bjp-online.com/british-journal-of-photography/news/2172088/tate-doubles-photography-collection-donation Tate doubles its photography collection after donation]", ''British Journal of Photography,'' 2 May 2012. Accessed 1 July 2012.</ref>
*[[Musée Carnavalet]] (Paris)<ref name="drean" />

== References ==
{{Reflist}}

== External links ==
*[http://www.dorothybohm.com/ Dorothy Bohm's website]
*Jennifer Boyd, "[http://www.manchestergalleries.org/dorothybohm/exhibition/ Dorothy Bohm in Manchester]", Manchester Art Gallery.

{{Authority control}}
{{DEFAULTSORT:Bohm, Dorothy}}
[[Category:Articles created via the Article Wizard]]
[[Category:Living people]]
[[Category:People from Königsberg]]
[[Category:1924 births]]
[[Category:British photographers]]
[[Category:British women photographers]]
[[Category:Portrait photographers]]
[[Category:Photography in Egypt]]
[[Category:Photography in France]]
[[Category:Photography in Italy]]
[[Category:Jews who immigrated to the United Kingdom to escape Nazism]]
[[Category:German emigrants to England]]
[[Category:British people of Lithuanian-Jewish descent]]
[[Category:British people of German-Jewish descent]]
[[Category:Alumni of the University of Manchester Institute of Science and Technology]]
[[Category:German people of Lithuanian-Jewish descent]]
[[Category:People from Klaipėda]]
[[Category:People from Šiauliai]]