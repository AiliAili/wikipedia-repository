{{Orphan|date=April 2015}}
{{Infobox musical artist
| name                = Mark Saint Juste
| image               =
| image_size          = 
| landscape           = <!-- yes, if wide image, otherwise leave blank -->
| alt                 = Mark Saint Juste
| caption             = 
| background          =  non_vocal_instrumentalist
| birth_name          = 
| native_name         = 
| native_name_lang    = 
| alias               = Mark St. Juste
| birth_date          = January 23, 1968
| birth_place         = Boynton, Florida
| origin              = 
| death_date          = <!-- {{death date and age|YYYY|MM|DD|YYYY|MM|DD}} (death date 1st) -->
| death_place         = 
| genre               = R&B, Hip-Hop, Children’s literature
| occupation          = Entrepreneur, TV/Film producer, Author, Songwriter, Record producer, DJ
| instrument          = 
| years_active        = 1987-Present
| label               = MSJ Records, Street Street Communications
| associated_acts     = Sweet MSJ, Cali Cartel, DJ Nasty Knock, A.C. Au Funkster, South Side Pride, Lauren Denise Carter, Gerald Kelly
| website             = {{URL|MarkSaintJuste.com}}
| notable_instruments = 
}}

'''Mark Saint Juste''' (born January 23, 1968) also known as Mark St. Juste is an entrepreneur, television, film and record producer, author and former school teacher. Over the course of his entertainment career he has written, produced and distributed music that has reached the Billboard charts under his independent record label and production company, Street Street Communications.<ref>{{cite journal|title=Billboard Top R&B/Hip-Hop Albums|journal=Billboard|date=Aug 11, 2001|volume=113|issue=32|page=34|url=https://books.google.com/books?id=hRIEAAAAMBAJ&pg=PA34&lpg=PA34&dq=%22black+beach+hits%22+billboard&source=bl&ots=8yg96q5SJD&sig=MAuP5tH1ayoF2uc5DAfcwkfHxHw&hl=en&sa=X&ei=eNwiVejENYPooASvvYGADQ&ved=0CDYQ6AEwBQ#v=onepage&q=%22black%20beach%20hits%22%20billboard&f=false|ref=Black Beach Hits|issn=0006-2510}}</ref>  He also wrote, produced and directed ''Peep Diss Videos'', a prime time syndicated Hip-Hop TV show on broadcast television and three documentary films, ''Shake City 101'', ''Black Beach'' and ''Black Beach Weekend''. Saint Juste has worked with some of hip-hops most notable artists to successfully explore the realities of hip-hop and leverage its positive messages for the advancement of communities.<ref name="SunSentinel">{{cite web|last1=McCabe|first1=Robert|title=Positive Rap: Record Firm Offers A Beat From A Different Drummer.|url=http://articles.sun-sentinel.com/1994-08-26/business/9408250593_1_carrie-borzillo-spec-s-and-peaches-positive-rap|website=Sun Sentinel|accessdate=3 April 2015|date=August 26, 1994}}</ref>

Saint Juste was a teacher for 2 years at Deerfield Park Elementary School in Florida. Since then he has worked as a consultant and principal investor in commodities finance and media content production and distribution. He is the President of the Hong Kong and Los Angeles based company, Zhou Limited.<ref name=ZhouLimited>{{cite web|title=Team|url=http://zhoulimited.com/management/|website=Zhou Limited|accessdate=7 April 2015}}</ref>

== Career ==

===Music===
Mark started his first entertainment company MSJ Records in 1987. Under this label he released his first single "Money" as a solo artist.  In 1992 Mark started production company Street Street Communications. The label independently released 13 albums between 1994 and 2001 culminating with the Billboard chart topping soundtrack, ''Black Beach Hits''.<ref>{{cite journal|title=Top R&B/Hip-Hop Albums|journal=Billboard|date=Jul 21, 2001|volume=113|issue=29|page=28|url=https://books.google.com/books?id=DxUEAAAAMBAJ&pg=PA28&lpg=PA28&dq=%22black+beach+hits%22+billboard&source=bl&ots=_0OH0i7Vwe&sig=kpk6zHWZC3JUUn09RtcOLx6d87g&hl=en&sa=X&ei=eNwiVejENYPooASvvYGADQ&ved=0CDIQ6AEwAw#v=onepage&q=%22black%20beach%20hits%22%20billboard&f=false|accessdate=6 April 2015|ref=Black Beach Hits|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> Other productions by Saint Juste recognized in Billboard Magazine are "Body Love" and "Doo Doo Brown" which reached the top R&B/Hip-Hop Song chart.<ref>{{cite journal|last1=Flick|first1=Larry|title=Single Reviews|journal=Billboard|date=Jul 4, 1992|volume=104|issue=27|page=68|url=https://books.google.com/books?id=QRIEAAAAMBAJ&pg=RA1-PA68&dq=%22body+love%22+billboard&hl=en&sa=X&ei=KMYdVeKbOIztoASplYLYAw&ved=0CDoQ6AEwBg#v=onepage&q=%22body%20love%22%20billboard&f=false|accessdate=3 April 2015|ref=Body Love}}</ref><ref>{{cite journal|last1=Russell|first1=Deborah|title=Grass Route|journal=Billboard|date=Jul 11, 1992|volume=104|issue=28|page=44|url=https://books.google.com/books?id=LRIEAAAAMBAJ&pg=PA44&dq=Billboard+-+Jul+11,+1992+-+page+44&hl=en&sa=X&ei=N-weVae9O8vroASU0YGIBA&ved=0CB8Q6AEwAA#v=snippet&q=%22body%20love%22&f=false|accessdate=3 April 2015|ref=Body Love}}</ref><ref>{{cite web|title=DJ Nasty Knock Chart History|url=http://www.billboard.com/artist/299966/dj-nasty-knock/chart|website=Billboard|accessdate=3 April 2015}}</ref> Street Street's first album, ''Florida Funk Bass, Bass, Bass & Mo Bass'' by A.C. AuFunkster was released Nov 30, 1994.<ref>{{cite journal|author1=J.R. Ryenolds|title=Tupac's Loss May Preserve Awards' Image; New Indies Form Out West and Down South|journal=Billboard|date=Jan 15, 1994|volume=106|issue=3|page=15|url=https://books.google.com/books?id=FwgEAAAAMBAJ&pg=PA15&lpg=PA15&dq=street+street%27s+first+album+florida+funk+bass+billboard&source=bl&ots=Er9I1Sjvhw&sig=0XPpPsqGVS_l8iRWIlp3y0dKXdM&hl=en&sa=X&ei=ZOAiVaSIOo3coATKmYFY&ved=0CB4Q6AEwAA#v=onepage&q=street%20street%27s%20first%20album%20florida%20funk%20bass%20billboard&f=false|accessdate=6 April 2015|ref=Florida Funk Bass, Bass, Bass & Mo Bass|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref>

During Mark's 2 years at Deerfield Park Elementary he used the positive impact of hip hop to engage his students and make learning more fun.<ref name="SunSentinel" /><ref>{{cite journal|last1=Borzillo|first1=Carrie|title=Popular Uprisings|journal=Billboard|date=Jun 18, 1994|volume=106|issue=25|page=20|url=https://books.google.com/books?id=UQgEAAAAMBAJ&pg=PA20&lpg=PA20&dq=sweet+msj+billboard&source=bl&ots=96Qam9MlzE&sig=fyXKFl1-ABdqPr6O9Sog_mxPub4&hl=en&sa=X&ei=8ModVbHYJonfoASDxYGwCQ&ved=0CCIQ6AEwAQ#v=onepage&q=sweet%20msj&f=false|accessdate=3 April 2015|ref=Sweet MSJ|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> Witnessing the impact rap had on his students inspired him to bring the positivity of rap music to kids with the next productions from his record label. As Sweet MSJ, Saint Juste released the singles "We've Got a Lesson to Learn" and "I’m Too Good for Drugs." His students performed "I’m Too Good for Drugs" at the National Drug Abuse Resistance Education (DARE) Convention in Orlando.<ref name="SunSentinel" /> "I’m Too Good for Drugs" is a single from the ''Get Me Me Music'' album that Saint Juste released in 2005.<ref name=GetMeMeMusicCD>{{cite web|title=Mark St Juste - Get Me Me Music CD|url=http://www.cduniverse.com/search/xx/music/pid/6956658/a/get+me+me+music.htm|website=CD Universe|accessdate=6 April 2015|ref=Get Me Me Music}}</ref> The album is composed of songs that combine educational themed lyrics with hip-hop beats.

===TV and film===
In 2000 Mark launched Unlimited Television (UTV), a film and TV production company, and released the reality spring break film, ''Black Beach''.<ref name=BlackBeach>{{cite web|title=Black Beach 2000|url=http://www.amazon.com/Black-Beach-2000/dp/B00002L5YT/ref=sr_1_14?s=movies-tv&ie=UTF8&qid=1428623246&sr=1-14&keywords=%22black+beach%22|website=Amazon|accessdate=9 April 2015}}</ref> ''Black Beach'' was subsequently licensed through the Universal Music Group on DVD as ''Black Beach Spring Bling''.<ref name=SpringBling>{{cite web|title=Black Beach Spring Bling|url=http://www.amazon.com/Black-Beach-Spring-Bling-2002/dp/B00006AUFZ|website=Amazon|accessdate=9 April 2015}}</ref> Mark also independently produced and syndicated the Hip Hop celebrity lifestyle TV series, ''Peep Diss Videos''.<ref name="billboard10212000">{{cite journal|last1=Hay|first1=Carla|title=The Eye|journal=Billboard|date=Oct 21, 2000|volume=112|issue=43|page=94|url=https://books.google.com/books?id=QBIEAAAAMBAJ&pg=PA94&dq=peep+diss+videos&hl=en&sa=X&ei=8qoaVaMrzbahBMuPgLAO&ved=0CC8Q6AEwAg#v=onepage&q=peep%20diss%20videos&f=false|accessdate=3 April 2015|ref=Peep Diss Videos|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> ''Peep Diss Videos'' features one-on-one interviews with Hip Hop’s most popular artists such as [[Jay Z]], [[Snoop Dogg]], [[Pharrell]], [[Ice-T]], [[R.L. Huggard|RL]] of Next, [[Naughty By Nature]], 702, Chinaman of the [[2 Live Crew]], [[Boo Yaa Tribe]], [[The Baka Boyz]], Big Boy, [[Skee-Lo]] and [[Young M.C.]].<ref name=PeepDiss>{{cite web|title=Peep Diss Videos: Season One|url=http://www.amazon.com/PEEP-DISS-VIDEOS-SEASON-ONE/dp/B000OYC7QC|website=Amazon|accessdate=9 April 2015}}</ref> In its inception, ''Peep Diss Videos'' reached approximately 700,000 viewers in South Florida.<ref>{{cite journal|last1=Atwood|first1=Brett|title=The Eye|journal=Billboard|date=Jul 13, 1996|volume=108|issue=28|page=106|url=https://books.google.com/books?id=ogkEAAAAMBAJ&pg=PA106&dq=peep+diss+videos&hl=en&sa=X&ei=8qoaVaMrzbahBMuPgLAO&ved=0CCsQ6AEwAQ|accessdate=3 April 2015|ref=Peep Diss Videos|publisher=Nielsen Business Media, Inc.|issn=0006-2510}}</ref> The program expanded to a prime time slot airing at 7:30PM Sundays on KDOC-TV in Los Angeles.<ref name="billboard10212000" />

Through UTV Mark also produced, directed and released the first documentary film on the inner city dance phenomenon, [[Krumping]].<ref>{{cite web|last1=Paggett|first1=Taisha|title=Getting krumped: the changing race of hip hop.|url=http://www.thefreelibrary.com/Getting+krumped%3A+the+changing+race+of+hip+hop.-a0118675205|website=The Free Library|publisher=2004 Dance Magazine, Inc.|accessdate=3 April 2015}}</ref> This film followed all star Krump dancers Gizmo, Hurricane, Mijo, Tight Eyes, and Tsunami and others through the streets of Venice, Hollywood and South Central, Los Angeles as they battle dance with their best moves.<ref>{{cite book|last1=Rajakumar|first1=Mohanalakshmi|title=Hip Hop Dance|date=Jan 9, 2012|publisher=ABC-CLIO|page=102|url=https://books.google.com/books?id=jGZhfy9UaGIC&pg=PA102&dq=shake+city+101&hl=en&sa=X&ei=ra8aVeGwBojtoASYgoGYDg&ved=0CB0Q6AEwAA#v=onepage&q=shake%20city%20101&f=false|ref=Shake City 101}}</ref><ref>{{cite web|last1=Koslow|first1=Jessica|title=Know Your L.A. Hip-Hop Dances: The Evolution Of Krumping|url=http://www.laweekly.com/music/know-your-la-hip-hop-dances-the-evolution-of-krumping-2409665|website=LA Weekly|accessdate=3 April 2015|date=November 1, 2011}}</ref>

In 2005 Mark launched Saint Juste International to develop, produce and distribute broader based entertainment properties. The production and distribution company first acquired the feature film ''The Definite Maybe'', which was renamed and released on DVD as ''No Money Down''.<ref>{{cite web|title=No Money Down|url=http://www.amazon.com/No-Money-Down-Bob-Balaban/dp/B000EDWLLY/ref=sr_1_1?s=movies-tv&ie=UTF8&qid=1428624203&sr=1-1&keywords=no+money+down|website=Amazon|accessdate=10 April 2015}}</ref> This was followed by the 2007 acquisition and DVD release of the Speed Channel's top rated program, ''Nopi Tunervision: Season One''.<ref>{{cite web|title=Nopi Tunervision Season One Part 1|url=http://www.amazon.com/Nopi-Tunervision-Season-One-Part/dp/B000RGX0GI|website=Amazon|accessdate=6 April 2015}}</ref> That same year the company released the stand up comedy series, ''Laugh Bandits'', and the first season of ''Peep Diss Videos'' on DVD.<ref name="PeepDiss" /><ref name=LaughBandits>{{cite web|title=Gerald Kelly: Laugh Bandits Number One|url=http://www.barnesandnoble.com/w/dvd-gerald-kelly-laugh-bandits-number-one-mark-saint-juste/12384226?ean=745423700228|website=Barnes & Noble|accessdate=9 April 2015}}</ref>

In 2010 Saint Juste International distributed the limited theatrical release of the family comedy, Oy Vey! My Son Is Gay!<ref>{{cite web|title=Oy Vey! My Son is Gay!|url=http://www.boxofficemojo.com/movies/?id=oyveymysonisgay.htm|website=Box Office Mojo|accessdate=7 April 2015}}</ref> The film was the 10th highest per screen average grossing during its second weekend at the box office.<ref>{{cite web|title=December 31-January 2, 2011|url=http://www.boxofficemojo.com/weekend/chart/?view=main&yr=2010&wknd=53&sort=avg&order=DESC&p=.htm|website=Box Office Mojo|accessdate=7 April 2015|ref=Oy Vey! My Son is Gay!}}</ref>

===Social projects===
In 2012 Saint Juste launched an online social activism site to counter the effects of high gas prices www.TwoDollarGas.com.<ref>{{cite web|title=Two Dollar Gas Campaign Aims to Unite 200 Million U.S. Motorists Before Presidential Election|url=http://www.prweb.com/releases/2012/10/prweb10051600.htm|website=PRWeb|accessdate=7 April 2015}}</ref>

==Other business ventures==

===Zhou Limited===
Mark is the President of Zhou Limited, a firm that trades directly in commodities and offers services in security, media and consulting.<ref name="ZhouLimited" />

===Get Me Me===
In 2014, under Zhou Limited, Mark launched the Get Me Me brand as an online destination for kids with a mission to effect positive change in the world using entertainment and media to promote love.<ref name=GetMeMe>{{cite web|title=About Us|url=http://getmemekids.com/about-us/|website=Get Me Me Kids|accessdate=7 April 2015}}</ref>

===Books===
Mark completed and released the first book from the Get Me Me series titled ''My Neighborhood'' in 2014.<ref>{{cite web|title=Mark Saint Juste Releases New Children's Book, "My Neighborhood," as Part of Forthcoming Values-conscious Entertainment, Success Lifestyle, and Education Brand to Implement a "Curriculum of Love" This New School Year|url=http://www.prnewswire.com/news-releases/mark-saint-juste-releases-new-childrens-book-my-neighborhood-as-part-of-forthcoming-values-conscious-entertainment-success-lifestyle-and-education-brand-to-implement-a-curriculum-of-love-this-new-school-year-272551601.html|website=PR Newswire|accessdate=7 April 2015}}</ref>

== Filmography ==
*Laugh Bandits Number One (2007)<ref name="LaughBandits" />
*Peep Diss Videos: Season One (2007)
*DJ Domination: World Domination (2003)<ref>{{cite web|title=DJ Domination: World Domination (2003)|url=https://www.nytimes.com/movies/movie/275167/DJ-Domination-World-Domination/overview|website=New York Times|accessdate=9 April 2015}}</ref>
*Shake City 101 (2002)
*Black Beach Spring Bling (2001)<ref name="SpringBling" />
*Black Beach Weekend (2001)<ref>{{cite web|title=Black Beach Weekend (Unsensored)|url=http://www.amazon.com/Black-Beach-Weekend-Uncensored-VHS/dp/B00005BJS7|website=Amazon|accessdate=10 April 2015}}</ref>
*Black Beach 2000<ref name="BlackBeach" />

==Billboard==
{|class="wikitable sortable"
|-
!Chart (2001)
!Peak<br />position
|-
{{Album chart|BillboardRandBHipHop|70|M|title=Top R&B/Hip-Hop Albums|url=https://books.google.com/books?id=DxUEAAAAMBAJ&pg=PA28&lpg=PA28&dq=%22Black+Beach+Hits%22+billboard&source=bl&ots=_0OH1g02Bc&sig=Dku7_iX2gGELMdHyFZKIj22uRN8&hl=en&sa=X&ei=VP0jVYXzGIayoQTGtYGQBw&ved=0CDMQ6AEwAw#v=onepage&q=%22Black%20Beach%20Hits%22%20billboard&f=false|accessdate=7 April 2015}}
|}

{|class="wikitable sortable"
|-
!Chart (1998)
!Peak<br />position
|-
{{singlechart|Billboardrandbhiphop|69|artist=DJ Nasty Knock|song=Doo Doo Brown}}
|}

==Production Discography==
*Get Me Me Music <ref name="GetMeMeMusicCD" />
*Black Beach Weekend Hits Volume 1<ref name=allmusiccred>{{cite web|title=Mark St. Juste Credits|url=http://www.allmusic.com/artist/mark-st-juste-mn0000334677/credits|website=All Music|accessdate=7 April 2015}}</ref>
*Cali Bumps<ref name="allmusiccred" />
*Black Spring Break<ref name="allmusiccred" />
*Hi De Ho<ref name="allmusiccred" />
*Sex<ref name="allmusiccred" />
*Florida Funk: Bass Bass Bass and Mo Bass<ref name="allmusiccred" />
*NaCl2<ref>{{cite web|title=NaCl2|url=http://www.allmusic.com/album/nacl2-mw0000645533|website=AllMusic|accessdate=7 April 2015}}</ref>

==References==
{{reflist|2}}

==External links==
*[http://saintjuste.com/twodollargas/?page_id=41 Two Dollar Gas]
*[http://getmemekids.com/ Get Me Me Kids]
*[http://myneighborhoodkidsbook.com/ My Neighborhood kids book]
*[http://www.dancelessons.net/dancehistory/HistoryofHipHopDance.html History of Hip Hop Dance]
*[http://zhoulimited.com/ Zhou Limited]
{{DEFAULTSORT:Juste, Mark}}
[[Category:Living people]]
[[Category:1968 births]]