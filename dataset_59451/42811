<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name = Fw 42
 |image = Focke-Wulf Fw 42.jpg
 |caption = Wind tunnel model of the final configuration.
}}{{Infobox Aircraft Type
 |type = [[Bomber]]
 |manufacturer = [[Focke-Wulf]]
 |designer = [[Henrich Focke]]
 |first flight =
 |introduced =
 |retired =
 |produced =
 |number built = None
 |status = 
 |unit cost =
 |primary user = [[Luftwaffe]] (intended)
 |more users = 
 |developed from = [[Focke-Wulf F 19]]
 |variants with their own articles = 
 |developed into =
}}
|}

The '''Focke-Wulf Fw 42''' was a design for a twin-engined medium bomber, of [[canard (aeronautics)|canard]] configuration, that was designed by [[Focke-Wulf Flugzeugbau AG]] in [[Germany]] in the early 1930s. Several air forces expressed interest in the aircraft; however, despite its advanced design being proved sound in [[wind tunnel]] testing, the Fw 42 failed to win a contract for development, and no examples of the type were ever built.

==Development and design==
Designed by [[Heinrich Focke]] in response to a 1929 specification issued by the Air Department of the [[Reichswehr]],<ref name="HR">Herwig and Rode 2000, p. 17.</ref> the design of the Fw 42 was based on that of Focke's earlier [[Focke-Wulf F 19|F 19 ''Ente'']] ([[German language|German: "Duck"]]) light transport.<ref name="L46"/>

The aircraft's design featured a long, slender fuselage with gun positions at each end, an aft-mounted wing with a "tail-first", or canard, configuration, fully retractable tricycle [[landing gear]], and an internal [[bomb bay]].<ref name="AOL">Lepage 2009, p. 144.</ref> The aircraft was planned to be operated by a crew of six.<ref name="L46"/>

Early versions of the Fw 42 design featured [[vertical stabiliser]]s mounted on the end of the wing, with additional fins located just outboard of the engine nacelles, for a four-tail arrangement.<ref name="L46"/> In addition, these early concepts featured the canard being mounted above the fuselage in a parasol arrangement, which had been used on the F 19.<ref name="L46"/> As the design was developed between 1931 and 1933, the canard was moved from the top to the bottom of the fuselage, to improve vision for the pilot and fields of fire for the forwards gunner.<ref name="L46"/> In addition, [[wind tunnel]] tests of the four-fin configuration showed that it did not provide a significant advantage over a single, large fin, and so the latter configuration was adopted for simplicity.<ref name="L46"/>

The Fw 42 was intended to be powered by two {{convert|560|kW|hp|adj=on}} [[BMW VI]] 12-cylinder, liquid-cooled engines, which were expected to provide a top speed of nearly {{convert|300|km/h|mph}},<ref name="AOL"/> and a range of over {{convert|1200|km|mi}}.<ref name="L46"/>

==Evaluation and cancellation==
A full-scale [[mockup]], including working gun turrets, of the Fw 42's final design was constructed.<ref name="L46"/> Focke-Wulf promoted the design for export sales as well as Luftwaffe service, with the Russians and Japanese reported as expressing interest in the type, and one or both nations' representatives examining the mockup.<ref name="L46">{{cite web|url=http://www.luft46.com/fw/fw42.html|title=Focke-Wulf Fw 42|work=Luft '46|accessdate=2010-06-02}}</ref> However, despite the fact that wind tunnel tests of the design's unconventional configuration provided promising results, indicating the concept was sound, no contracts for development were ever issued, and the Fw 42 project was abandoned.<ref name="AOL"/>

===Recovery of data===
At the end of World War II, technical information on the Fw 42 was lost in the destruction of sensitive data by Focke-Wulf. However, in 1969, excavation of the site on which the data had been stored revealed that the folders containing information on the aircraft had survived nearly completely undamaged.<ref name="L46"/>

==Specifications (Fw 42) ==
{{Aircraft specs
|ref=<ref name="L46"/>
|prime units?=met
|genhide=
|crew=Six
|capacity=
|length m=17.7
|length ft=
|length in=
|length note=
|span m=25
|span ft=
|span in=
|span note=
|height m=4.3
|height ft=
|height in=
|height note=
|wing area sqm=108
|wing area sqft=
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=5600
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=9000
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
|eng1 number=2
|eng1 name=[[BMW VI]]
|eng1 type=liquid-cooled [[Inline engine (aviation)|inline piston engines]]
|eng1 kw=
|eng1 hp=750
|eng1 note=
|power original=
|more power=
|prop blade number=4
|prop name=
|prop dia m=3.8
|prop dia ft=
|prop dia in=
|prop note=
|perfhide=
|max speed kmh=310
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=
|cruise speed kmh=260
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1200
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=
|ceiling m=6000
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|more performance=
|guns= Two [[machine gun]]s in nose and tail positions
|bombs= {{convert|1000|kg}} in internal bay
|avionics=
}}

==See also==
{{Portal|Aviation|Germany}}
{{aircontent
|related=
*[[Focke-Wulf F 19]]
|similar aircraft=
*[[Douglas DB-7]]
*[[Miles M.35 Libellula|Miles Libellula]]
|sequence=
|lists =
*[[List of bomber aircraft]]
*[[List of military aircraft of Germany]]
*[[List of RLM aircraft designations]]
|see also=
}}

==References==
{{commons category|Focke-Wulf}}
;Notes
{{reflist}}
;Bibliography
{{refbegin}}
*Herwig, Dieter and Heinz Rode. ''Luftwaffe Secret Projects: Strategic Bombers 1935-1945''. Midland Publishing, 2000. ISBN 1-85780-092-3.
*Lepage, Jean-Denis G.G. ''Aircraft of the Luftwaffe, 1935-1945: An Illustrated Guide''. Jefferson, NC: McFarland, 2009. ISBN 0-7864-3937-8.
{{refend}}

{{Focke-Wulf aircraft}}
{{RLM aircraft designations}}

[[Category:Canard aircraft]]
[[Category:Abandoned military aircraft projects of Germany]]
[[Category:Focke-Wulf aircraft|Fw 042]]
[[Category:German bomber aircraft 1930–1939]]
[[Category:Twin-engined tractor aircraft]]
[[Category:High-wing aircraft]]