'''Thrust-to-weight ratio''' is a [[dimensionless quantity|dimensionless]] ratio of [[thrust]] to [[weight]] of a [[rocket]], [[jet engine]], [[Propeller (aircraft)|propeller]] engine, or a vehicle propelled by such an engine that indicates the performance of the engine or vehicle.

The instantaneous thrust-to-weight ratio of a vehicle varies continually during operation due to progressive consumption of fuel or propellant and in some cases a gravity gradient.  The thrust-to-weight ratio based on initial thrust and weight is often published and used as a [[figure of merit]] for quantitative comparison of the initial performance of vehicles.

==Calculation==
The thrust-to-weight ratio can be calculated by dividing the thrust (in SI units &ndash; in [[newton (unit)|newton]]s) by the weight (in newtons) of the engine or vehicle.  It is a dimensionless quantity. Note that the thrust can also be measured in [[Pound (force)|pound-force]] (lbf) provided the weight is measured in pounds (lb); the division of these two values still gives the numerically correct thrust-to-weight ratio.  For valid comparison of the initial thrust-to-weight ratio of two or more engines or vehicles, thrust must be measured under controlled conditions.

==Aircraft==
The thrust-to-weight ratio and [[wing loading]] are the two most important parameters in determining the performance of an aircraft.<ref>Daniel P. Raymer, ''Aircraft Design: A Conceptual Approach'', Section 5.1</ref>  For example, the thrust-to-weight ratio of a [[combat aircraft]] is a good indicator of the maneuverability of the aircraft.<ref>John P. Fielding, ''Introduction to Aircraft Design'', Section 4.1.1 (p.37)</ref>

The thrust-to-weight ratio varies continually during a flight.  Thrust varies with throttle setting, [[airspeed]], [[Altitude#Altitude in aviation|altitude]] and air temperature.  Weight varies with fuel burn and changes of payload.  For aircraft, the quoted thrust-to-weight ratio is often the maximum static thrust at sea-level divided by the [[maximum takeoff weight]].<ref>John P. Fielding, ''Introduction to Aircraft Design'', Section 3.1 (p.21)</ref>

In cruising flight, the thrust-to-weight ratio of an aircraft is the inverse of the [[lift-to-drag ratio]] because thrust is the inverse of [[Drag (force)|drag]], and weight is the inverse of lift.<ref>Daniel P. Raymer, ''Aircraft Design: A Conceptual Approach'', Equation 5.2</ref>

:<math>\left (\frac{T}{W}\right)_{cruise}=\frac{1}{(\frac{L}{D})_{cruise}}</math>

===Propeller-driven aircraft===
For propeller-driven aircraft, the thrust-to-weight ratio can be calculated as follows:<ref>Daniel P. Raymer, ''Aircraft Design: A Conceptual Approach'', Equation 5.1</ref><br />
:<math>\frac{T}{W}=\left(\frac{\eta_p}{V}\right)\left(\frac{P}{W}\right)</math>
where <math>\eta_p\;</math> is [[propulsive efficiency]] at [[true airspeed]] <math>V\;</math>
:<math>P\;</math> is [[Horsepower#Measurement|engine power]]

==Rockets==
[[File:Thrust to weight ratio vs Isp.png|thumb|Rocket vehicle Thrust-to-weight ratio vs Isp for different propellant technologies]]

The thrust-to-weight ratio of a rocket, or rocket-propelled vehicle, is an indicator of its acceleration expressed in multiples of gravitational acceleration g.<ref name="sutton">George P. Sutton & Oscar Biblarz, ''Rocket Propulsion Elements'' (p. 442, 7th edition) "thrust-to-weight ratio F/W<sub>g</sub> is a dimensionless parameter that is identical to the acceleration of the rocket propulsion system (expressed in multiples of g<sub>0</sub>) if it could fly by itself in a gravity-free vacuum"</ref>

Rockets and rocket-propelled vehicles operate in a wide range of gravitational environments, including the ''weightless'' environment. The thrust-to-weight ratio is usually calculated from initial gross weight at sea-level on earth<ref>George P. Sutton & Oscar Biblarz, ''Rocket Propulsion Elements'' (p. 442, 7th edition) "The loaded weight W<sub>g</sub> is the sea-level initial gross weight of propellant and rocket propulsion system hardware."</ref> and is sometimes called ''Thrust-to-Earth-weight ratio''.<ref>{{cite web
  | publisher = The Internet Encyclopedia of Science
  | title = Thrust-to-Earth-weight ratio
  | url = http://www.daviddarling.info/encyclopedia/T/thrust-to-Earth-weight_ratio.html
  | accessdate =2009-02-22 }}</ref>  The thrust-to-Earth-weight ratio of a rocket or rocket-propelled vehicle is an indicator of its acceleration expressed in multiples of earth’s gravitational acceleration, g<sub>0</sub>.<ref name="sutton"/>

The thrust-to-weight ratio for a rocket varies as the propellant is burned. If the thrust is constant, then the maximum ratio (maximum acceleration of the vehicle) is achieved just before the propellant is fully consumed.  Each rocket has a characteristic thrust-to-weight curve or acceleration curve, not just a scalar quantity.

The thrust-to-weight ratio of an engine exceeds that of the whole launch vehicle but is nonetheless useful because it determines the maximum acceleration that any vehicle using that engine could theoretically achieve with minimum propellant and structure attached.

For a takeoff from the surface of the [[earth]] using thrust and no [[aerodynamic lift]], the thrust-to-weight ratio for the whole vehicle must be more than ''one''. In general, the thrust-to-weight ratio is numerically equal to the ''[[g-force]]'' that the vehicle can generate.<ref name="sutton"/> Take-off can occur when the vehicle's ''g-force'' exceeds local gravity (expressed as a multiple of g<sub>0</sub>).

The thrust to weight ratio of rockets typically greatly exceeds that of [[airbreathing jet engine]]s because the comparatively far greater density of rocket fuel eliminates the need for much engineering materials to pressurize it.

Many factors affect a thrust-to-weight ratio.  The instantaneous value typically varies over the flight with the variations of thrust due to speed and altitude along with the weight due to the remaining propellant and payload mass.  The main factors include freestream air [[temperature]], [[pressure]], [[density]], and composition.  Depending on the engine or vehicle under consideration, the actual performance will often be affected by [[buoyancy]] and local [[Field strength#Gravitational field strength|gravitational field strength]].

==Examples==
The [[Russia]]n-made [[RD-180 (rocket engine)|RD-180]] rocket engine (which powers [[Lockheed Martin]]’s [[Atlas V rocket|Atlas V]]) produces 3,820&nbsp;kN of sea-level thrust and has a [[Dry matter|dry mass]] of 5,307&nbsp;kg.{{citation needed|date=March 2009}}  Using the Earth surface gravitational field strength of 9.807&nbsp;m/s², the sea-level thrust-to-weight ratio is computed as follows: (1&nbsp;kN = 1000&nbsp;N = 100&nbsp;kg⋅m/s²)

:<math>\frac{T}{W}=\frac{3,820\ \mathrm{kN}}{(5,307\ \mathrm{kg})(9.807\ \mathrm{m/s^2})}=0.07340\ \frac{\mathrm{kN}}{\mathrm{N}}=73.40\ \frac{\mathrm{N}}{\mathrm{N}}=73.40</math>

===Aircraft===

{| class="wikitable"
|-
! Vehicle
! T/W
! Scenario
|-
| [[B-2 Spirit]]
| 0.205<ref>[[Northrop Grumman B-2 Spirit]]</ref>
| Max take-off weight, full power
|-
| [[Airbus A380]]
| 0.227
| Max take-off weight, full power
|-
| [[Tupolev Tu-160|Tu-160]]
| 0.363
| Max take-off weight, full afterburners
|-
| [[Concorde]]
| 0.372
| Max take-off weight, full reheat
|-
| [[Rockwell B-1 Lancer|B-1 Lancer]]
| 0.38
| Max take-off weight, full afterburners
|-
| [[BAE Systems Hawk|BAE Hawk]]
| 0.65<ref>[[BAE Systems Hawk]]</ref>
| 
|-
|[[Lockheed Martin F-35 Lightning II|F-35]]
|0.87 with full fuel (1.07 with 50% fuel)
|
|-
| [[Dassault Rafale|Rafale]]
| 0.988<ref>http://www.aviationsmilitaires.net/display/variant/1</ref>
| Version M, 100% fuel, 2 EM A2A missile, 2 IR A2A missiles
|-
| [[Su-30MKM]]
| 1.00<ref name="Wikipedia">[[Sukhoi Su-30MKM#Specifications .28Su-30MKM.29]]</ref>
| Loaded weight with 56% internal fuel
|-
| [[McDonnell Douglas F-15 Eagle|F-15]]
| 1.04<ref>{{cite web
  | publisher = About.com:Inventors
  | title = F-15 Eagle Aircraft
  | url = http://inventors.about.com/library/inventors/blF_15_Eagle.htm
  | accessdate =2009-03-03}}</ref>
| Nominally loaded
|-
| [[Mikoyan MiG-29|MiG-29]]
| 1.09<ref name=militaire>http://www.globalsecurity.org/military/world/russia/mig-29-specs.htm</ref>
| Full internal fuel, 4 AAMs
|-
| [[Lockheed Martin F-22 Raptor|F-22]]
| > 1.09 (1.26 with loaded weight and 50% fuel)<ref name=militaire2>http://www.aviationsmilitaires.net/display/aircraft/87/f_a-22</ref>
| Combat load?
|-
| [[General Dynamics F-16 Fighting Falcon|F-16]]
| 1.096{{Citation needed|date=July 2008}}
|
|-
| [[Hawker Siddeley Harrier|Harrier]]
| 1.1{{Citation needed|date=July 2008}}
| [[VTOL]]
|-
| [[Eurofighter Typhoon|Typhoon]]
| 1.15<ref>[http://eurofighter.airpower.at/vergleich.htm Kampflugzeugvergleichstabelle Mader/Janes]</ref>
| Interceptor configuration 
|-
| [[Space Shuttle]]
| 1.5
| Take-off
|-
| [[Space Shuttle]]
| 3
| Peak (throttled back for astronaut comfort)
|}

===Jet and rocket engines===

{{Engine thrust to weight table}}

=== Fighter aircraft ===

{| class="wikitable" border="1" 
|+ Table b: Thrust-to-weight ratios, fuel weights, and weights of different fighter planes (in metric units)
|-
! In International System
! [[McDonnell Douglas F-15 Eagle|F-15K]]
! F-15C
! MiG-29K
! MiG-29B
! [[CAC/PAC JF-17 Thunder|JF-17]]
! [[Chengdu J-10|J-10]]
! F-35A
! F-35B
! F-35C
! F-22
! [[HAL Tejas|LCA Mk-1]]
|-
| Engine(s) thrust maximum (N)
| 259,420 (2)
| 208,622 (2)
| 176,514 (2)
| 162,805 (2)
| 81,402 (1)
| 122,580 (1)
| 177,484 (1)
| 177,484 (1)
| 177,484 (1)
| 311,376 (2)
| 89,800  (1)
|-
| Aircraft mass, empty (kg)
| 17,010
| 14,379
| 12,723
| 10,900
| 06,586
| 09,250
| 13,290
| 14,515
| 15,785
| 19,673
|  6,560
|-
| Aircraft mass, full fuel (kg)
| 23,143
| 20,671
| 17,963
| 14,405
| 08,886
| 13,044
| 21,672
| 20,867
| 24,403
| 27,836
|  9,500
|-
| Aircraft mass, max take-off load (kg)
| 36,741
| 30,845
| 22,400
| 18,500
| 12,700
| 19,277
| 31,752
| 27,216
| 31,752
| 37,869
| 13,300
|-
| Total fuel mass (kg)
| 06,133
| 06,292
| 05,240
| 03,505
| 02,300
| 03,794
| 08,382
| 06,352
| 08,618
| 08,163
| 02,458
|-
| T/W ratio (full fuel)
| 1.14
| 1.03
| 1.00
| 1.15
| 1.09
| 0.96
| 0.84
| 0.87
| 0.74
| 1.14
| 1.07
|-
|}

{| class="wikitable"
|+ Table a: Thrust-to-weight ratios, fuel weights, and weights of different fighter planes
|-
! Specifications   /   Fighters
! F-15K
! F-15C
! MiG-29K
! MiG-29B
! JF-17
! J-10
! F-35A
! F-35B
! F-35C
! F-22
|-
| Engine(s) thrust maximum (lbf)
| 58,320 (2)
| 46,900 (2)
| 39,682 (2)
| 36,600 (2)
| 18,300 (1)
| 27,557 (1)
| 39,900 (1)
| 39,900 (1)
| 39,900 (1)
| 70,000 (2)
|-
| Aircraft weight empty (lb)
| 37,500
| 31,700
| 28,050
| 24,030
| 14,520
| 20,394
| 29,300
| 32,000
| 34,800<ref name="Lockheed Martin Website">{{cite web|title= Lockheed Martin Website |url= http://www.lockheedmartin.com/products/f35/f-35specifications/f-35c-cv-specifications.html}}</ref>
| 43,340
|-
| Aircraft weight, full fuel (lb)
| 51,023
| 45,574
| 39,602
| 31,757
| 19,650
| 28,760
| 47,780
| 46,003
| 53,800
| 61,340
|-
| Aircraft weight, max take-off load (lb)
| 81,000
| 68,000
| 49,383
| 40,785
| 28,000
| 42,500
| 70,000
| 60,000
| 70,000
| 83,500
|-
| Total fuel weight (lb)
| 13,523
| 13,874
| 11,552
| 07,727
| 05,130
| 08,366
| 18,480
| 14,003
| 19,000<ref name="Lockheed Martin Website"/>
| 18,000
|-
| T/W ratio (full fuel)
| 1.14
| 1.03
| 1.00
| 1.15
| 1.09
| 0.96
| 0.84
| 0.87
| 0.74
| 1.14
|}

* Fuel density used in calculations: 0.803&nbsp;kg/l
* The number inside brackets is the number of engines.
* For the metric table, the T/W ratio is calculated by dividing the thrust by the product of the full fuel aircraft weight and the acceleration of gravity.
* Engines powering F-15K are the Pratt & Whitney engines.
* MiG-29K's empty weight is an estimate.
* JF-17's engine rating is of RD-93.
* JF-17 if mated with its engine WS-13, and if that engine gets its promised 18,969&nbsp;lb then the T/W ratio becomes 1.10
* J-10's empty weight and fuelled weight are estimates.
* J-10's engine rating is of AL-31FN.
* J-10 if mated with its engine WS-10A, and if that engine gets its promised 132&nbsp;kN (29,674&nbsp;lbf) then the T/W ratio becomes 1.08

==See also==
* [[Power-to-weight ratio]]
* [[Factor of safety]]

==References==
* John P. Fielding. ''Introduction to Aircraft Design'', Cambridge University Press, ISBN 978-0-521-65722-8
* Daniel P. Raymer (1989). ''Aircraft Design: A Conceptual Approach'', American Institute of Aeronautics and Astronautics, Inc., Washington, DC. ISBN 0-930403-51-7
* George P. Sutton & Oscar Biblarz. ''Rocket Propulsion Elements'', Wiley, ISBN 978-0-471-32642-7

===Notes===
{{Reflist|30em}}

==External links==
* [http://www.grc.nasa.gov/WWW/K-12/airplane/fwrat.html NASA webpage with overview and explanatory diagram of aircraft thrust to weight ratio]

{{DEFAULTSORT:Thrust-To-Weight Ratio}}
[[Category:Jet engines]]
[[Category:Rocket engines]]
[[Category:Engineering ratios]]