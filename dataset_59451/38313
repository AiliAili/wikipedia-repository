{{good article}}
{{Infobox University Boat Race
| name= 140th Boat Race
| winner =Cambridge
| margin =  6 and 1/2 lengths
| winning_time=  18 minutes 9 seconds
| date= 26 March 1994
| umpire = John Garrett<br>(Cambridge)
| prevseason= [[The Boat Race 1993|1993]]
| nextseason= [[The Boat Race 1995|1995]]
| overall =71&ndash;68
| reserve_winner= Goldie
| women_winner = Cambridge
}}
The 140th [[The Boat Race|Boat Race]] took place on 26 March 1994. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Cambridge won by six-and-a-half lengths.  The race saw the first competitors from Norway in the history of the race, in brothers Snorre and Sverke Lorgen.  It was also the first time that both competing coxes had previously won the event.

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] defeated Oxford's Isis, while Cambridge won the [[Women's Boat Race]].

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 9 July 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 9 July 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=9 July 2014}}</ref><ref>{{Cite web | url = http://theboatrace.org/men/tv-and-radio| archiveurl=https://web.archive.org/web/20141006123036/http://theboatrace.org/men/tv-and-radio | archivedate= 6 October 2014 | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 7 July 2014}}</ref>  Cambridge went into the race as reigning champions, having won the [[The Boat Race 1993|1993 race]] by three-and-a-half lengths,<ref name=results>{{Cite web | url = http://theboatraces.org/results| publisher =The Boat Race Company Limited | title = Boat Race – Results| accessdate = 9 July 2014}}</ref> with Cambridge leading overall with 70 victories to Oxford's 68 (excluding the "dead heat" of 1877).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 9 July 2014}}</ref>  The race was sponsored by [[Beefeater Gin]] for the eighth consecutive year.<ref>{{Cite web | url = http://theboatraces.org/report/professionalism-arrives | publisher = The Boat Race Company Limited | title= Professionalism arrives | accessdate= 7 August 2014 }}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Up until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatrace.org/women/history |archiveurl=https://web.archive.org/web/20140714183555/http://theboatrace.org/women/history|archivedate=14 July 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 9 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

In preparing for the race, Cambridge had defeated crews from both [[Leander Club|Leander]] and [[Molesey Boat Club|Molesey]]. Meanwhile, Oxford lost out to [[London Rowing Club]]'s lightweight crew, but narrowly defeated the [[University of London Boat Club]].  They went on to secure victory in the [[Reading University Head of the River Race|Reading Head of the River]] race before head coach Richard Tinkler and his assistant Tim Bramfitt were removed from their positions.  Fred Smallbone was installed as [[Glossary of rowing terms#The stroke|finishing]] coach for the Dark Blues whose build-up concluded with a loss to Leander.  Cambridge were coached throughout by Harry Mahon, John Wilson and Sean Bowden.<ref name=class/>

==Crews==
The crews weighed-in at [[The Hurlingham Club]] five days prior to the race.<ref name=close>{{Cite news | title = Crews close to record at weigh-in | work = [[The Times]] | first = Mike |last = Rosewell | date = 22 March 1994 | page = 48 | issue = 64908}}</ref>  Oxford weighed an average of 14&nbsp;[[stone (unit)|st]] 7&nbsp;[[pound (mass)|lb]] (91.6&nbsp;kg) per rower, {{convert|2|lb|kg}} more than their opponents.<ref name=close/>  Oxford's crew featured two Norwegian brothers in Snorre and Sverke Lorgen, the first rowers from their country to participate in the Boat Race, both of whom had participated in both world championship and Olympic races.<ref name=class/><ref name=nor>{{Cite web | url = http://www.independent.co.uk/sport/rowing-blues-brothers-with-pulling-power-simon-ohagan-meets-the-norwegians-ready-to-make-history-in-the-boat-race-1430306.html | work = [[The Independent]] | title = Rowing: Blues brothers with pulling power: Simon O'Hagan meets the Norwegians ready to make history in the Boat Race | date = 20 March 1994 | accessdate = 10 July 2014 | first = Simon | last = O'Hagan}}</ref>  Oxford Boat Club president Kingsley Poole referred to them as "Herdy and Gerdy".  He noted: "They've got some kind of kin thing where they can just switch on and go nuts".<ref name=nor/>  Cambridge welcomed back five former [[Blue (university sport)|Blues]] while Oxford saw three return in the form of Michels, Poole and Chick.<ref name=close/>
{| class="wikitable" 
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || H J MacMillan || [[Worcester College, Oxford|Worcester]] || 14 st 5.5 lb || R D Taylor || [[Trinity Hall, Cambridge|Trinity Hall]]  || 14 st 0.5 lb
|-
| 2 ||  C N Mahne || [[St Catherine's College, Oxford|St Catherine's]] || 14 st 10.5 lb || W T M Mason ||  [[Trinity Hall, Cambridge|Trinity Hall]] ||  13 st 6 lb
|-
| 3 || J G Michels || [[St John's College, Oxford|St John's]] || 13 st 4.5 lb || S M Gore|| [[Jesus College, Cambridge|Jesus]] || 13 st 10.5 lb
|-
| 4 || A S Gordon-Brown || [[Keble College, Oxford|Keble]] || 14 st 12.5 lb || R C Phelps || [[St Edmund's College, Cambridge|St Edmund's]] || 14 st 3.5 lb
|-
| 5 || D R H Clegg {{double dagger}} || [[Keble College, Oxford|Keble]] || 13 st 12 lb || J A Bernstein (P)|| [[St Edmund's College, Cambridge|St Edmund's]]  || 15 st 0 lb
|-
| 6 || Sverke Lorgen || [[University College, Oxford|University]] || 16 st 1 lb || M H W Parish || [[St Edmund's College, Cambridge|St Edmund's]]  || 14 st 6 lb
|-
| 7 || Snorre Lorgen || [[Nuffield College, Oxford|Nuffield]]  || 15 st 8.5 lb || P J M Hoeltzenbein || [[Magdalene College, Cambridge|Magdalene]] ||  14 st 2.5 lb
|-
| [[Stroke (rowing)|Stroke]] || K K Poole (P) || [[St John's College, Oxford|St Johns's]] || 13 st 2.5 lb || [[Thorsten Streppelhoff|T Streppelhoff]] || [[St Edmund's College, Cambridge|St Edmund's]] || 14 st 1 lb
|-
| [[Coxswain (rowing)|Cox]] || H E Chick ||[[Christ Church, Oxford|Christ Church]] || 7 st 10 lb || [[Martin Haycock|M N Haycock]] || [[Magdalene College, Cambridge|Magdalene]]  || 7 st 12.5 lb
|-
!colspan="7"|Source:<ref>{{Cite web | url = http://www.independent.co.uk/sport/rowing--boat-race-norwegian-bulk-tips-the-scales-towards-oxford-boat-race-weighin-finds-last-years-winners-cambridge-light-on-weight-but-laden-with-talent-hugh-matheson-reports-1430920.html | work = [[The Independent]] | first = Hugh | last = Matheson | accessdate = 10 July 2014 | date = 22 March 1994 | title = Rowing / Boat Race: Norwegian bulk tips the scales towards Oxford: Boat Race weigh-in finds last year's winners, Cambridge, light on weight but laden with talent}}</ref><br>(P) &ndash; boat club president<br>{{double dagger}} &ndash; Rob Clegg was brought in to replace Adam Pearson two weeks prior to the race.<ref name=class/>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Cambridge were considered to be pre-race favourites.<ref name=class>{{Cite news | title = Cambridge confident of making class count| first = Mike | last=  Rosewell | work = [[The Times]] | date = 26 March 1994 | page = 44 | issue = 64912}}</ref>  Oxford won the toss and elected to start from the Middlesex station.<ref name=standards>{{Cite news | title = Cambridge set new standards | first = Mike |last = Rosewell | work = [[The Times]] | date = 28 March 1994 | page = 31 | issue = 64913}}</ref>  After a poor start, which Oxford's cox Chick referred to as "a bit ropey", Cambridge took an early lead and were five seconds ahead at the Mile Post.  They extended their advantage to eight seconds by [[Hammersmith Bridge]] and sixteen by Chiswick Steps.  Poor steering from Haycock combined with determination from Oxford to stay in touch kept the Cambridge from moving too much further ahead;  the Light Blues passed the finishing post in 18 minutes and 9 seconds, six-and-a-half lengths and 20 seconds ahead of the Dark Blues.<ref name=results/><ref name=standards/> It was Cambridge's first back-to-back victory since 1973.<ref name=rhythm>{{Cite web | url = http://www.independent.co.uk/sport/rowing--boat-race-all-rhythm-and-light-blues-hugh-matheson-sees-cambridge-claim-an-impressive-victory-to-make-it-two-in-a-row-1431860.html | work = [[The Independent]] | first = Hugh |last = Matheson | accessdate = 12 July 2014 | date = 27 March 1994 | title = Rowing / Boat Race: All rhythm and Light Blues: Hugh Matheson sees Cambridge claim an impressive victory to make it two in a row}}</ref>  The race was umpired by former Cambridge Blue John Garrett.<ref>{{Cite web | url = http://theboatraces.org/news-article/12/umpires-announced-for-the-2012-xchanging-boat-race | publisher = The Boat Race Company Limited | accessdate = 12 July 2014 | date = 6 September 2011 | title = Umpires announced for the 2012 Xchanging Boat Race}}</ref>

In the reserve race, Cambridge's [[Goldie (Cambridge University Boat Club)|Goldie]] won by thirteen lengths over Isis, their seventh victory in eight years.<ref name=results/>  Cambridge won the [[Women's Boat Race 1994|49th Women's Boat Race]] a week earlier, a {{convert|2000|m|yd}} race at [[Henley-on-Thames]],<ref>{{Cite web | url = http://www.independent.co.uk/sport/womens-boat-race-cambridge-women-pass-examination-of-rowing-prowess-1430686.html | work = [[The Independent]] | title = Women's Boat Race: Cambridge women pass examination of rowing prowess | date = 21 March 1994 | first = Hugh |last = Matheson | accessdate = 12 July 2014}}</ref> by one length in a time of 6 minutes and 11 seconds, their fifth victory in six years.<ref name=results/>

==Reaction==
Cambridge's boat club president Jon Bernstein said "it does not get any better than that."<ref name=standards/>  The Cambridge number four, Richard Phelps, was more descriptive: "It was a bitch of a race. We never cruised."<ref name=rhythm/>

==References==
{{reflist|30em}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1994}}
[[Category:The Boat Race]]
[[Category:1994 in English sport]]
[[Category:1994 in rowing]]
[[Category:March 1994 sports events]]