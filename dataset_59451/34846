{{Infobox cycling race report
| name               = 1905 Tour de France
| image              = Tour de France 1905 map-fr.svg
| image_caption      = Route of the 1905 Tour de France<br />Followed clockwise, starting in Paris
| image_alt          = Map of France with the route of the 1905 Tour de France on it, showing that the race started in Paris, went clockwise through France and ended in Paris after eleven stages.
| image_size         = 300px
| date               = 9–30 July
| stages             = 11
| distance           = 2994
| unit               = km
| time               = 35 points
| speed              = 27.481
| first              = [[Louis Trousselier]]
| first_nat          = FRA
| second             = [[Hippolyte Aucouturier]]
| second_nat         = FRA
| third              = [[Jean-Baptiste Dortignacq]]
| third_nat          = FRA
| previous           = [[1904 Tour de France|1904]]
| next               = [[1906 Tour de France|1906]]
}}

The '''1905 Tour de France''' was the third edition of the [[Tour de France]], held from 9 to 30 July, organized by the newspaper ''[[L'Auto]]''. Following the disqualifications after the [[1904 Tour de France]], there were changes in the rules, the most important one being the general classification not made by time but by points. The race saw the introduction of mountains in the Tour de France, and [[René Pottier]] excelled in the first mountain, although he could not finish the race.<ref name="letour">{{cite web|url=http://www.letour.fr/HISTO/us/TDF/1905/histoire.html|title=The Tour - Year 1905|publisher=[[Amaury Sport Organisation]]|accessdate=29 December 2009}}</ref>
Due in part to some of the rule changes, the 1905 Tour de France had less cheating and sabotage than in previous years, though they were not completely eliminated. It was won by [[Louis Trousselier]], who also won four of the eleven stages.

==Changes from the previous Tour==

After the [[1904 Tour de France]], some cyclists were disqualified, most notably the top four cyclists of the original overall classification, [[Maurice Garin]], [[Lucien Pothier]], [[César Garin]] and [[Hippolyte Aucouturier]]. Maurice Garin was originally banned for two years and Pothier for life, so they were ineligible to start the 1905 Tour de France. Of these four, only Aucouturier (who had been "warned" and had a "reprimand inflicted" on him), started the 1905 Tour.<ref>{{cite web|url=http://www.veloarchive.com/races/tour/1904.php|title=The Tour is finished&nbsp;... |first=Tom|last=James|date=14 August 2003|accessdate=21 June 2010|publisher=VeloArchive}}</ref> They were disqualified by the [[Union Vélocipédique Française]], based on accusations of cheating when there were no race officials around. In 1904 Tour, it was difficult to observe the cyclists continuously, as significant portions of the race were run overnight, and the long stages made it difficult to have officials everywhere.

Because these disqualifications had almost put an end to the Tour de France, the 1905 event had been changed in important ways, to make the race easier to supervise:<ref name="Gann">{{cite book|last=McGann|first=Bill|author2=McGann, Carol|title=The Story of the Tour de France|publisher=Dog Ear Publishing|year=2006|accessdate=29 December 2009|url=https://books.google.com/books?id=jxq20JskqMUC|isbn=1-59858-180-5|pages=14–16}}</ref>
*The stages were shortened so that no night riding occurred.
*The number of stages increased to 11 stages, almost double from the previous year.
*The winner was selected on points, not time.<ref name="veloarchive">{{cite web|url=http://www.veloarchive.com/races/tour/1905.php|title=1905: A new formula is devised|publisher=VeloArchive|date=14 August 2003|accessdate=21 June 2010}}</ref>
The first cyclist to cross the finish line received 1 point. Other cyclists received one point more than the cyclist who passed the line directly before him, plus an additional point for every five minutes between them, with a maximum of ten points. In this way, a cyclist could not get more than 11 points more than the cyclist that crossed the finish line just before him.<ref name="mdc1905">{{cite web|url=http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905.php|publisher=Mémoire du cyclisme|title=3ème Tour de France 1905|language=French|accessdate=18 March 2009|archiveurl=http://www.webcitation.org/5gVchFd9E|archivedate=3 May 2009|deadurl=yes}}</ref>

As an example for this point system, the result for the first seven cyclists in the first stage is in this table:
{| class="wikitable"
|+ Results of stage 1<ref>{{cite news|url=http://gallica.bnf.fr/ark:/12148/bpt6k561845g.zoom.f5.langFR|title=Cyclisme - Le Tour de France|language=French|work=Le Petit Parisien|publisher=Gallica Bibliothèque Numérique|date=11 July 1905|accessdate=21 June 2010|page=5}}</ref>
! Cyclist || Time || Difference with<br/>previous finisher||Extra points || Points
|-
| Louis Trousselier || style="text-align:right;" | 11h 25'    || style="text-align:right;" |  — || style="text-align:right;" | 1 || style="text-align:right;" | 1
|-
| Jean-Baptiste Dortignacq || style="text-align:right;" | + 3' || style="text-align:right;" | 3'  || style="text-align:right;" |   1 || style="text-align:right;" | 2
|-
| René Pottier || style="text-align:right;" | + 4'             || style="text-align:right;" | 1'  || style="text-align:right;" |   1 || style="text-align:right;" | 3
|-
| Hippolyte Aucouturier || style="text-align:right;" | + 26'   || style="text-align:right;" | 22' || style="text-align:right;" |   5 || style="text-align:right;" | 8
|-
| Henri Cornet || style="text-align:right;" | + 26'            || style="text-align:right;" |  0'  || style="text-align:right;" |   1 || style="text-align:right;" | 9
|-
| Augustin Ringeval || style="text-align:right;" | + 1h 40'    || style="text-align:right;" | 74' || style="text-align:right;" | 11 || style="text-align:right;" | 20
|-
| Emile Georget || style="text-align:right;" | + 2h 40'        || style="text-align:right;" | 60' || style="text-align:right;" | 11 || style="text-align:right;" | 31
|}
<!--Trousselier received one point as the winner, and Dortignac and Pottier received 2 and 3 points, being second and third. The fourth finisher, Aucouturier, was 22 minutes behind number three, so he received 5 additional points, to make 8 in total. Cornet, the next finisher, was within 5 minutes of Aucouturier, so he received only one point more, 9 points. The sixth cyclist, Ringeval, was 74 minutes behind; this would mean 15 extra points. This is more than the maximum of 11 points, so Ringeval had 20 points after the first stage instead of 24. Similar with Georget: although he was 60 minutes behind (13 points), he only received 11 extra points for this.-->

The other important introduction were the mountains. One of Desgrange's staffers, Alphonse Steinès, took Desgrange for a trip over the [[Col Bayard]] at {{convert|1246|m|ft}} and the [[Col du Ballon d'Alsace|Ballon d'Alsace]] at {{convert|1178|m|ft}}, that had an average gradient of 5.2% with 10% at some places,<ref name="Gann"/><ref name="mdc1905"/> to convince Desgrange to use these climbs in the route. Desgrange accepted it, saying that Steinès would take the blame if the mountains would be too hard to climb.<ref name="Gann"/><ref>{{cite book|url=https://books.google.com/books?id=Ggx7H3q29v4C&pg=PA190#v=onepage&q&f=false|title=Cols mythiques du Tour de France|author=Laget, Serge|author2=Bouvet, Philippe|pages=190–191|language=French|publisher=L'Equipe|year=2005|isbn=2-915535-09-4}}</ref> In the two previous editions, the highest point was the [[Col de la République]] at {{convert|1145|m|ft}}. In 1905, Desgrange chose to overlook this, and focused instead on the introduction of the Ballon d'Alsace, because he saw that he had missed the opportunity for publicity previously.<ref>{{cite web|url=http://www.sportgeschiedenis.nl/2007/11/10/col-de-la-republique-was-eerste-berg-in-tour-de-france.aspx|title=Col de la République was eerste berg in Tour de France|author=Van den Bogaart, Ronnie|language=Dutch|date=10 November 2007|accessdate=5 July 2010|publisher=Sportgeschiedenis}}</ref>

There were two categories of riders, the ''coureurs de vitesse'' and the ''coureurs sur machines poinçonnées''.{{sfn|Augendre|2016|p=7}} The riders in the first category were allowed to change bicycles, which could be an advantage in the mountains, where they could use a bicycle with lower gears.<ref name="Gann"/> The riders in the machines poinçonnées category had to use the same bicycle in the entire race, and to verify this, their bicycles were marked.

==Participants==
{{main list|List of cyclists in the 1905 Tour de France}}

Before the race started, 78 riders had signed up for the race. Eighteen of those did not start the race, so the Tour began with 60 riders, including former winner [[Henri Cornet]] and future winners [[René Pottier]] and [[Lucien Petit-Breton]].<ref name="mdc1905" /> The riders were not grouped in teams, but most of them rode with an individual sponsor. Two of the cyclists—[[Alois Catteau|Catteau]] and [[Julien Lootens|Lootens]]—were Belgian, all other cyclists were French. Leading up to the start of the Tour, Wattelier, [[Louis Trousselier|Trousselier]], Pottier and Augereau were all considered the most likely contenders to win the event.<ref>{{cite news|url=http://gallica.bnf.fr/ark:/12148/bpt6k256192d.zoom.f4.langFR|language=French|title=Vélocipedie - Le Tour de France de 1905|work=La Croix|date=9 July 1905|accessdate=21 June 2010|publisher=Gallica Bibliothèque Numérique|page=4}}</ref>

==Race overview==

{{main article|1905 Tour de France, Stage 1 to Stage 6|1905 Tour de France, Stage 7 to Stage 11}}

Despite the rule changes, there were still protesters among the spectators; in the first stage all riders except [[Jean-Baptiste Dortignacq]] punctured due to 125&nbsp;kg of nails spread along the road.<ref name="letour" /><ref name="veloarchive" /> The first stage was won by [[Louis Trousselier]]. Trousselier was serving the army, and had requested his commander leave for the Tour de France; this was allowed for 24 hours.<ref name="Amels">{{cite book|title=De geschiedenis van de Tour de France 1903–1984|first=Wim|last=Amels|year=1984|publisher=Sport-Express|language=Dutch|isbn=90-70763-05-2|pages=8–9}}</ref> After he won the first stage and led the classification, his leave was extended until the end of the Tour.<ref>{{cite web|url=http://www.cyclingwebsite.net/coureurmemofiche.php?coureurmemoid=846&coureurid=7607|title=Memo Louis Trousselier|publisher=CyclingWebsite|accessdate=20 March 2009}}</ref> From 60 starting cyclists, only 15 cyclists reached the finish line within the time limit;<ref name="mdc1905-1">{{cite web|url=http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_1.php|archive-url=http://web.archive.org/web/20120506185506/http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_1.php|archive-date=2012-05-06|access-date=28 October 2016|df=dmy-all|dead-url=yes|publisher=Mémoire du cyclisme|title=3ème Tour de France 1905 - 1ère étape|language=French}}</ref> 15 more reached the finish after the limit and the rest took the train.<ref name="nieuwsdossier">{{cite web|url=http://www.nieuwsdossier.nl/dossier/1905-07-30/Louis+Trousselier+wint+Tour+de+France+1905|title=Louis Trousselier wint Tour de France 1905|language=Dutch|publisher=NieuwsDossier|date=8 January 2008|accessdate=21 September 2009}}</ref>
The Tour organizer Desgrange wanted to stop the race, but was persuaded by the cyclists not to do so, and allowed all cyclists to continue with 75 points.<ref name="mdc1905-1" /><ref name="nieuwsdossier" />

[[File:René Pottier.jpg|thumb|left|upright|[[René Pottier]], the first cyclist to climb a mountain in the Tour de France.|alt=A black-and-white photograph of a man with a mustache.]]
In the second stage, the first major climb, the [[Col du Ballon d'Alsace|Ballon d'Alsace]], made its debut. Four riders were the fastest climbers: Trousselier, [[René Pottier]], Cornet and Aucouturier. Of those four, Trousselier and Aucouturier were the first to be dropped, and Cornet had to drop in the final kilometers.<ref name="Gann"/> The top was therefore reached first by René Pottier, without dismounting, at an average speed of 20&nbsp;km/h.<ref name="veloarchive"/> Cornet, who reached the top second, had to wait 20 minutes for his bicycle with higher gear, because his support car had broken down.<ref name="Gann"/>
Later Aucouturier caught Pottier, and dropped him, and won the stage.<ref name="Gann"/> Pottier became second in the stage and led the classification.<ref name="letour"/> Seven cyclists did not reach the finish in time, but they were again allowed to start the next stage.<ref name="mdc1905-2">{{cite web|url=http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_2.php|archive-url=http://web.archive.org/web/20120506185737/http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_2.php|archive-date=2012-05-06|access-date=28 October 2016|df=dmy-all|dead-url=yes|publisher=Mémoire du cyclisme|title=3ème Tour de France 1905 - 2ème étape|language=French}}</ref>

In the third stage, Pottier had to abandon due to tendinitis.<ref name="convicts">{{cite web|url=http://www.lorrainemace.com/index_files/Page1342.html|title=Convicts of the road|first=Lorraine|last=Mace|year=2004|archiveurl=http://www.webcitation.org/5gVcirhlK|archivedate=3 May 2009|deadurl=no|accessdate=18 March 2009}}</ref> The lead was back with Trousselier, who also won the stage.

In the fourth stage, the [[Rampe de Laffrey|Côte de Laffrey]] and the [[Col Bayard]] were climbed, the second and third mountains of the Tour de France.<ref name="mdc1905"/> [[Julien Maitron]] reached both tops first, but Aucouturier won the stage. Trousselier finished in second place, still leading the overall classification, although with the same number of points as Aucouturier.<ref name="mdc1905-4">{{cite web|url=http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_4.php|archive-url=http://web.archive.org/web/20120506185711/http://memoire-du-cyclisme.net/eta_tdf_1903_1939/tdf1905_4.php|archive-date=2012-05-06|access-date=28 October 2016|df=dmy-all|dead-url=yes|publisher=Mémoire du cyclisme|title=3ème Tour de France 1905 - 4ème étape|language=French}}</ref>

[[File:Louis Trousselier.jpg|thumb|upright|[[Louis Trousselier]], the winner of the 1905 Tour de France|alt=A black-and-white photograph of a man with three-colored sweater and shorts with a mustache sitting on a bicycle.]]In the fifth stage, Trousselier won, and because Aucouturier finished in twelfth place, Trousselier had a big lead in the general classification. After the fifth stage, Aucouturier could no longer challenge Trousselier for the lead.<ref name="revealed">{{cite web|url=http://www.cyclingrevealed.com/timeline/Race%20Snippets/TdF/TdF1905.htm|title=The Restructuring|author=Boyce, Barry|year=2004|publisher=Cycling revealed|accessdate=10 May 2010}}</ref>

In the seventh stage to Bordeaux, Trousselier punctured after only a few kilometers. The rest of the cyclists quickly sped away from him, and Trousselier had to follow them alone for 200&nbsp;km. A few kilometers before Bordeaux, Trousselier caught up with the rest, and even managed to win the sprint.<ref name="Amels"/> Louis Trousselier kept his lead until the end of the race, winning five stages. Trousselier was accused of bad sportsmanship: he reportedly smashed the inkstands of a control post to prevent his opponents from signing.<ref name="convicts"/> Unlike the 1904 Tour de France, no stage winners, nor anyone from the top ten of the general classification, were disqualified.
{{clear left}}

==Results==

===Stage results===

In the first and last stage, the cyclists were allowed to use pacers. All the 11 stages were won by only three cyclists:{{sfn|Augendre|2016|p=7}}

{| class="wikitable"
|+ Stage characteristics and winners<ref name="mdc1905"/>{{sfn|Augendre|2016|p=7}}
|-
! scope="col" | Stage
! scope="col" | Date
! scope="col" | Course
! scope="col" | Distance
! scope="col" colspan="2" | Type{{refn|group=n|In 1905, there was no distinction in the rules between plain stages and mountain stages; the icons shown here indicate which stages included mountains.<ref name="mdc1905"/>}}
! scope="col" | Winner
! scope="col" | Race leader
|-
! scope="row" style="text-align:center;" | 1
| style="text-align:center;" | 9 July || [[Paris]] to [[Nancy, France|Nancy]]
| {{convert|340|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Louis Trousselier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 2
| style="text-align:center;" | 11 July || [[Nancy, France|Nancy]] to [[Besançon]]
| {{convert|299|km|mi|abbr=on}} ||[[File:Mountainstage.svg|20px|alt=|link=]] || Stage with mountain(s)|| {{flagathlete|[[Hippolyte Aucouturier]]|FRA}} || {{flagathlete|[[René Pottier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 3
| style="text-align:center;" | 14 July || [[Besançon]] to [[Grenoble]]
| {{convert|327|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Louis Trousselier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 4
| style="text-align:center;" | 16 July || [[Grenoble]] to [[Toulon]]
| {{convert|348|km|mi|abbr=on}} ||[[File:Mountainstage.svg|20px|alt=|link=]] || Stage with mountain(s)|| {{flagathlete|[[Hippolyte Aucouturier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 5
| style="text-align:center;" | 18 July || [[Toulon]] to [[Nîmes]]
| {{convert|192|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Louis Trousselier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 6
| style="text-align:center;" | 20 July || [[Nîmes]] to [[Toulouse]] ||{{convert|307|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Jean-Baptiste Dortignacq]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 7
| style="text-align:center;" | 22 July || [[Toulouse]] to [[Bordeaux]] ||{{convert|268|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Louis Trousselier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 8
| style="text-align:center;" | 24 July || [[Bordeaux]] to [[La Rochelle]] ||{{convert|257|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Hippolyte Aucouturier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 9
| style="text-align:center;" | 26 July || [[La Rochelle]] to [[Rennes]] ||{{convert|263|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Louis Trousselier]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 10
| style="text-align:center;" | 28 July || [[Rennes]] to [[Caen]] ||{{convert|167|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Jean-Baptiste Dortignacq]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" style="text-align:center;" | 11
| style="text-align:center;" | 29 July || [[Caen]] to [[Paris]] ||{{convert|253|km|mi|abbr=on}} ||[[File:Plainstage.svg|20px|alt=|link=]] || Plain stage|| {{flagathlete|[[Jean-Baptiste Dortignacq]]|FRA}} || {{flagathlete|[[Louis Trousselier]]|FRA}}
|-
! scope="row" |
| colspan="2" style="text-align:center"| Total
| colspan="5" style="text-align:center"| {{convert|2994|km|0|abbr=on}}{{sfn|Augendre|2016|p=108}}
|}

===General classification===
The cyclists officially were not grouped in teams; some cyclists had the same sponsor, even though they were not allowed to work together.<ref name="noteams">{{cite book|title=The Tour de France: a cultural history|first=Christopher S.|last=Thompson|page=36|url=https://books.google.com/books?id=M-vUF6Y_4RUC|publisher=University of California Press|year=2006|isbn= 0-520-24760-4}}</ref>
{| class="wikitable" style="width:40em; margin-bottom:0;"
|+Final general classification (1–10)<ref name="mdc1905"/>
|-
! width=40 | Rank
! width=200 | Rider
! width=150 | Sponsor
! width=40 | Points
|-
| style="text-align:center;" | 1|| {{flagathlete|[[Louis Trousselier]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]]|| style="text-align:right;" | 35
|-
| style="text-align:center;" | 2|| {{flagathlete|[[Hippolyte Aucouturier]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]] || style="text-align:right;" | 61
|-
| style="text-align:center;" | 3|| {{flagathlete|[[Jean-Baptiste Dortignacq]]|FRA}} || Saving || style="text-align:right;" | 64
|-
| style="text-align:center;" | 4|| {{flagathlete|[[Emile Georget]]|FRA}} || JC Cycles || style="text-align:right;" | 123
|-
| style="text-align:center;" | 5|| {{flagathlete|[[Lucien Petit-Breton]]|FRA}} || JC Cycles || style="text-align:right;" | 155
|-
| style="text-align:center;" | 6|| {{flagathlete|[[Augustin Ringeval]]|FRA}} || JC Cycles || style="text-align:right;" | 202
|-
| style="text-align:center;" | 7|| {{flagathlete|[[Paul Chauvet]]|FRA}} || Griffon || style="text-align:right;" | 231
|-
| style="text-align:center;" | 8|| {{flagathlete|[[Philippe Pautrat]]|FRA}} || JC Cycles || style="text-align:right;" | 248
|-
| style="text-align:center;" | 9|| {{flagathlete|[[Julien Maitron]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]]/Griffon || style="text-align:right;" | 255
|-
| style="text-align:center;" | 10|| {{flagathlete|[[Julien Gabory]]|FRA}} || JC Cycles || style="text-align:right;" | 304
|}
{| class="collapsible collapsed wikitable noprint" style="width:40em;margin-top:-1px;"
|-
! colspan=4 | Final general classification (11–24)
|-
! width=40 | Rank
! width=200 | Rider
! width=150 | Sponsor
! width=40 | Points
|-
| style="text-align:center;" | 11|| {{flagathlete|[[Alois Catteau]]|BEL}} || Catteau Cycles || style="text-align:right;" | 355
|-
| style="text-align:center;" | 12|| {{flagathlete|[[Martin Soulie]]|FRA}} || JC Cycles || style="text-align:right;" | 358
|-
| style="text-align:center;" | 13|| {{flagathlete|[[Léon Leygoute]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]] || style="text-align:right;" | 394
|-
| style="text-align:center;" | 14|| {{flagathlete|[[Camille Fily]]|FRA}} || Guerin Cycles || style="text-align:right;" | 415
|-
| style="text-align:center;" | 15|| {{flagathlete|[[Antony Wattelier]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]] || style="text-align:right;" | 441
|-
| style="text-align:center;" | 16|| {{flagathlete|[[Henri Lignon]]|FRA}} || JC Cycles || style="text-align:right;" | 488
|-
| style="text-align:center;" | 17|| {{flagathlete|[[Maurice Decaup]]|FRA}} || Alcyon-Dunlop || style="text-align:right;" | 490
|-
| style="text-align:center;" | 18|| {{flagathlete|[[Maurice Carrere]]|FRA}} || Pirate-Michelin || style="text-align:right;" | 497
|-
| style="text-align:center;" | 19|| {{flagathlete|[[Gustave Guillarme]]|FRA}} || Renault || style="text-align:right;" | 509
|-
| style="text-align:center;" | 20|| {{flagathlete|[[Julien Lootens]]|BEL}} || JC Cycles || style="text-align:right;" | 515
|-
| style="text-align:center;" | 21|| {{flagathlete|[[Pinchau]]|FRA}} || ? || style="text-align:right;" | 707
|-
| style="text-align:center;" | 22|| {{flagathlete|[[Eugène Ventresque]]|FRA}} ||  Saving || style="text-align:right;" | 792
|-
| style="text-align:center;" | 23|| {{flagathlete|[[Fernand Lallement]]|FRA}} || [[Peugeot (cycling team)|Peugeot-Wolber]] || style="text-align:right;" | 797
|-
| style="text-align:center;" | 24|| {{flagathlete|[[Clovis Lacroix]]|FRA}} ||  Saving || style="text-align:right;" | 870
|}

===Other classifications===
Pautrat was the winner of the ''coureurs sur machines poinçonnées'' category, having used the same bicycle through the whole event.<ref>{{cite web|url=http://www.letour.fr/HISTO/fr/TDF/1905/histoire.html|title=l'Historique du Tour - Année 1905|language=French|publisher=[[Amaury Sport Organisation]]|accessdate=29 December 2009}}</ref>

The organising newspaper [[L'Auto]] named [[René Pottier]] the ''meilleur grimpeur''. This unofficial title is the precursor to the [[mountains classification in the Tour de France|mountains classification]].<ref>{{cite book|title=Legends of the Tour|url=https://books.google.nl/books?id=3nCNAwAAQBAJ&pg=PA156&lpg=PA156|first=Jan|last=Cleijne|isbn=978-1-78185-999-5|publisher=Head of Zeus|accessdate=3 December 2015}}</ref>

==Aftermath==
The tour organizers liked the effect of the points system, and it remained active until the [[1912 Tour de France]], after which it was reverted to the time system. In [[1953 Tour de France|1953]], for the 50-years anniversary of the Tour de France, the points system was reintroduced as the [[points classification in the Tour de France|points classification]], and the winner was given a [[green jersey]]. This points classification has been active ever since.

The introduction of mountains in the Tour de France had also been successful. After the introduction of the Vosges in the 1905 Tour de France, in 1906 the [[Massif Central]] were climbed, followed by the [[Pyrenees]] in 1910 and the [[Alps]] in 1911.

The winner Trousselier received 6950 [[French Franc|Francs]] for his victory. The night after he won, he drank and gambled with friends, and lost all the money.<ref name="Gann"/> In later years, Trousselier would not win a Tour de France again, but he still won eight more stages and finished on the podium in the next year.<ref>{{cite web|url=http://www.letour.fr/HISTO/TDF/riders/us/1371.html|title=Past results for Louis Trousselier (FRA)|publisher=[[Amaury Sport Organisation|ASO]]|accessdate=20 May 2009|archiveurl=http://www.webcitation.org/5h5mChqHE|archivedate=27 May 2009|deadurl=no}}</ref>
The unofficial mountain champion of the 1905 Tour de France, Pottier, would be more successful in the next year, when he won the overall classification and five stages.<ref>{{cite web|url=http://www.letour.fr/HISTO/us/TDF/coureur/1364.html|title=Past results for Rene Pottier (FRA)|publisher=[[Amaury Sport Organisation|ASO]]|accessdate=20 May 2009}}</ref>

For [[L'Auto]], the newspaper that organized the Tour de France, the race was a success; the circulation had increased to 100,000.<ref>{{cite web|url=http://www.kb.nl/dossiers/tourdefrance/tourdefrance.html|title=Tour de France|language=Dutch|publisher=Koninklijke Bibliotheek|date=8 August 2008|archiveurl=http://www.webcitation.org/5gVchgK02|archivedate=3 May 2009|deadurl=no|accessdate=18 March 2009}}</ref>

==Notes and references==

===Footnotes===
{{reflist|group=n}}

===References===
{{Reflist|30em}}

===Sources===
{{Refbegin}}
* {{cite book|first=Jacques|last=Augendre|author-link=Jacques Augendre|url=http://netstorage.lequipe.fr/ASO/cyclisme/le-tour/2016/histoire/TDF16_GH_Interactif-PROD.pdf|title=Guide historique|trans-title=Historical guide|year=2016|language=French|access-date=27 October 2016|format=PDF|work=[[Tour de France]]|location=Paris|publisher=[[Amaury Sport Organisation]]|archive-url=https://web.archive.org/web/20160817121602/http://netstorage.lequipe.fr/ASO/cyclisme/le-tour/2016/histoire/TDF16_GH_Interactif-PROD.pdf|archive-date=17 August 2016|dead-url=no|ref={{harvid|Augendre|2016}}}}
{{Refend}}

==External links==
{{commons category-inline|Tour de France 1905|1905 Tour de France}}

{{Cycling stage recaps|1905 Tour de France|1|6|7|11}}
{{Tour de France}}

{{Good article}}

{{Use dmy dates|date=June 2013}}

{{DEFAULTSORT:1905 Tour De France}}
[[Category:1905 Tour de France| ]]
[[Category:1905 in French sport|Tour de France]]
[[Category:Tour de France by year]]
[[Category:1905 in road cycling|Tour de France]]