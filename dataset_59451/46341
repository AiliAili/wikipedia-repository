'''Ayla''' ({{lang-ar|آيلة}}) was a medieval Islamic city established at the site of present-day [[Aqaba]] in [[Jordan]]. It was the first Islamic city founded outside of the [[Arabian Peninsula]]. Its ruins are located northwest of the current city center.

[[File:Ayla01.jpg|thumb|A view of Ayla]]
[[File:Ayla02.jpg|thumb|Ayla with a background view of mountains North of Aqaba]]

==History==
In 630, shortly after the [[Hijra (Islam)|Hijrah]], the Prophet of Islam [[Muhammad]] concluded an agreement with the Bishop of Ailan, the [[Byzantine]] city located about 500 meters northeast of the site of Ayla, which was included peacefully into the Islamic conquest.

The city was founded around the year 650 by the Caliph [[Uthman ibn Affan]].<ref>{{cite web|title=The Oriental Institute of the University of Chicago - Aqaba Project|url=http://oi.uchicago.edu/research/projects/aqa/|work=Aqaba project|publisher=The Oriental Institute of the University of Chicago}}</ref> The city prospered from 661 to 750 under the [[Ummayads]] and beyond under the [[Abbasids]] (750-970) and the [[Fatimids]] (970-1116), but declined in the late twelfth century due to earthquakes and attacks by [[Bedouins]] and [[Crusaders]]. [[Baldwin I of Jerusalem]] took over the city in 1116 without much resistance. The center of the city then moved to 500 meters along the coast to the south, around the [[Mamluk]] fort and the current flagpole of [[Aqaba]].

Ayla took advantage of its key position as an important step on the road to [[India]] and Arab spices (frankincense, myrrh), between the [[Mediterranean Sea]] and the [[Arabian Peninsula]]. The city is also mentioned in several stories of the [[Arabian Nights]].

The work of US-Jordanian excavations were conducted in 1986 by the [[University of Chicago]]. Besides the remains in place, many discoveries are at the [[Aqaba Archaeological Museum]] (one inscription above the door of Egypt) and the [[Jordan Archaeological Museum]], in [[Amman]].

The city was inscribed in a rectangle 170 × 145 fortified meters, with walls 2.6 meters thick and 4.5 meters high. 24 towers defended the city. The city had four gates on all four sides, defining two main lines intersecting at the center: the door of [[Egypt]] (north), the [[Damascus]] Gate (east), the door of the [[Hejaz]] (south ) and the door of the Sea (west). The intersection of these two channels was indicated by a tetra pylon (a four-way arch), which was transformed into a luxury residential building decorated with frescoes of the tenth century.

This type of urban structure, called MSIR, is typical of early Islamic fortified settlements. The city enclosed in a square or rectangle also included fortified residence, mosque northeast, occupying an area of 35 × 55 meters.

The city was notably described by the geographer Shams Eddin Muqaddasi.

There is also a similar in the ancient cities of Al-Fustat Egyptian and Iraqi [[Al Basrah]] and [[Al-Kufa]].

==Accessibility==
Ayla is accessible during the day. The walls of the ruins in some places can reach three meters high.

There is a [[wadi]] (dry river bed) that passes through the site.

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

==External links==
* [http://www.aqaba.jo/en/aqaba/beach-of-history#page Aqaba 'The beach of history (3700 BC till date)']
* [http://www.discoverislamicart.org/database_item.php?id=monument;ISL;jo;Mon01;12;en Museum with No Frontiers : Ayla]
* [http://www.discoverislamicart.org/zoom.php?img=http://www.museumwnf.org/images/lo_res/monuments/isl/jo/1/12/plans/1.jpg Aqaba: town plan]{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}
* [http://www.worldportsource.com/ports/JOR_Port_of_Aqaba_359.php  World Port source-Aqaba]
* [http://oi.uchicago.edu/research/projects/aqa/  The Oriental Institute of the University of Chicago - Aqaba Project]

{{Portal|archaeology|islam|Jordan}}
{{coord|29|31|51|N|34|59|59|E|type:landmark_region:JO|display=title}}
{{Characters and names in the Quran}}

[[Category:Aqaba]]
[[Category:Archaeology of Jordan]]