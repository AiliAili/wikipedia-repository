{{Infobox officeholder
| name        = Rainivoninahitriniony (Raharo)
| image       = Rainivoninahitriniony Prime Minister of Madagascar.jpg
| office      = [[Prime Minister of Madagascar]]
| term_start  = 10 February 1852
| term_end    = 14 July 1864
| predecessor = [[Rainiharo]]
| successor   = [[Rainilaiarivony]]
| birth_date  = 1824
| birth_place = [[Ilafy]], [[Merina Kingdom|Madagascar]]{{citation needed|date=August 2015}}
| death_date  = 5 May 1868
| death_place = [[Ambohimandroso, Ambalavao|Ambohimandroso]], Madagascar
| spouse = [[Rasoherina]]
}}

'''Rainivoninahitriniony'''<ref name="Royal Ark">[http://www.royalark.net/Madagascar/madagascar2.htm Royal Ark]</ref> (1824–1868), also called '''Raharo''', was [[Prime Minister]] of the [[Merina Kingdom|Kingdom of Madagascar]] between 1852 and 1864.  He was the chief engineer of the Aristocratic Revolution initialized upon the attempted assassination of [[Radama II|King Radama II]]. His excesses and participation in the regicide saw him fall from favor, ultimately being relieved of his position and replaced as Prime Minister by his younger brother [[Rainilaiarivony]]. Rainivoninahitriniony died in exile on May 5, 1868, shortly after an attempted coup meant to enable him to regain his position upon the death of [[Rasoherina|Queen Rasoherina]] ended in failure.

==Early years==
{{Unreferenced section|date=August 2015}}
Born in 1824, Rainivoninahitriniony was known in childhood as Raharo. He and his younger brother [[Rainilaiarivony]] were sons of [[Rainiharo]], of the Tsimiamboholahy clan, Prime Minister to [[Ranavalona I|Queen Ranavalona I]] from 1833 to 1852. His mother was Rabodomiarana, a daughter of Ramamonjy. Their family were not ''[[andriana]]'' (nobles) by blood, but rather belonged to the class of Hova (freemen). Their family had gained a prominent position at court when Rainiharo's father was chosen by [[Andrianampoinimerina|King Andrianampoinimerina]] to serve as a favored adviser on account of his keen acumen and unwavering loyalty.

Throughout his childhood, Rainivoninahitriniony had access to the education provided by the Protestant missionaries of the [[London Missionary Society]] and was favorable to European influences and the modernization of Madagascar.

As a young man, Rainivoninahitriniony became a Field Marshal in the army of Queen [[Ranavalona I]], where he was best known for putting down a revolt of the [[Antefasy]] and [[Antesaka]] in the southeast of the island in 1852. He served as Commander-in-Chief of the army from 1852 to 1862, as well as Minister for War (1861–62) and Minister for the Interior (1862), being awarded 14 state honors in 1861 and 15 honors in 1862.

==Rise to Prime Minister==
Upon his father's death in 1852, Rainivoninahitriniony succeeded to the position of Prime Minister. He advised Queen Ranavalona I and conducted the administrative business of the state jointly with another of the Queen's long-standing advisers, a conservative elder named Rainijohary. Rainivoninahitriniony gained notice for his talents as an administrator and for his ability to work in the best interests of the State.{{citation needed|date=August 2015}}

With the death of Ranavalona in 1861, many of the old guard of conservatives who had supported her regressive policies were sidelined by her progressive son and successor [[Radama II]].  The new king favored the counsel of his entourage of young friends, known as the ''menamaso'' ("red-eyes"), to the point of undermining Rainivoninahitriniony's authority as Prime Minister - a position he now held alone after the dismissal of Rainijohary.<ref name = "Camille">De La Vaissière, Camille. ''Histoire de Madagascar: ses habitants et ses missionnaires, Volume 1.''  {{fr icon}}</ref> At the same time, a growing faction of nobles was growing increasingly concerned with the drastic nature of [[Radama II|Radama]]'s reform policies.  These included the [[Lambert Charter]], which had given the [[France|French]] disproportionate rights to the exploitation of the island's resources, and which Rainivoninahitriniony attempted fruitlessly to contest. Rainivoninahitriniony united this group of nobles under his leadership and acted as their spokesperson in communicating their increasing frustrations to the king.{{citation needed|date=August 2015}}

==Aristocratic Revolution==
Tension between the nobles and the king came to a head when [[Radama II|Radama]] refused to reconsider his decision legalizing [[duel]]s.  The nobles, under the leadership of Rainivoninahitriniony, conspired to [[Radama II#Attempted assassination|assassinate the king]]. At the time, the coup was generally believed to have been successful, although 20th century evidence suggests the rumors at the time that Radama had actually survived the attack and went on to live to old age as a regular citizen were in fact true.  The conspirators decided to transfer the crown to Radama's apparent widow Rabodo, on the condition that power would be henceforth shared jointly between the monarchy and the nobles, symbolically represented in Rainivoninahitriniony's marriage in September 1863 to Rabodo, who took the throne name Queen [[Rasoherina]].{{citation needed|date=August 2015}}

Under [[Rasoherina]]'s reign, Rainivoninahitriniony effectively operated unhindered in his running of state affairs. At court, the conservative faction was still present under the leadership of the former co-Prime Minister to [[Ranavalona I]], Rainijohary.  However, Rainivoninahitriniony's progressives formed a majority in the Queen's Council and their advocacy for a moderately pro-European policy of modernization won out over the reversal of the policies of [[Radama II]] that were demanded by Rainijohary's conservatives.<ref>Ade Ajayi, J.F.''Africa in the nineteenth century until the 1880s.'' UNESCO, 1989.</ref>

His contemporary critics accused Rainivoninahitriniony of increasing despotism, habitual drunkenness and frequent violence as his power grew, reportedly threatening the queen at knife point on several occasions.<ref name = "Camille"/>

==Downfall and death==
Rainivoninahitriniony's involvement in the [[regicide]] plot had made him deeply unpopular among the people, and his excesses turned the opinion of many nobles against him. His fate was sealed during a visit to [[Ambohimanga]] when, upon receiving the names of individuals accused of intentionally setting a series of house fires that had ravaged [[Antananarivo]], he sought to ensure the rapid arrest and execution of the accused conspirators.  [[Rasoherina]] opposed these executions, however, sparking Rainivoninahitriniony's anger and prompting him to declare violent threats to her sovereignty.  Immediately afterward, Rasoherina set about to remove Rainivoninahitriniony from power with the support of Rainivoninahitriniony's younger brother [[Rainilaiarivony]], Commander-in-Chief of the army and influential leader among the [[andriana]]. The queen addressed the public at Andohalo, first releasing [[zebu]] into the crowd for public consumption in a bid to ensure popular approval of the transition, then announcing the deposition of Rainivoninahitriniony in favor of [[Rainilaiarivony]], relieving the Prime Minister of his post and stripping him of his ranks on July 14, 1864.<ref name = "Camille"/>  He was consequently sent into exile in [[Ambohimandroso, Ambalavao|Ambohimandroso]], in [[Betsileo]] country, on February 20, 1865.<ref name="Nativel">{{Cite book | last = Nativel | first = Didier | title = Maisons royales, demures des grands à Madagascar | publisher = Karthala Editions| year = 2005 | url = https://books.google.com/books?id=8IOZLvSrspIC&printsec=frontcover#v=onepage&q&f=false | accessdate = December 1, 2010 | isbn = 9782845865396|language=fr}}</ref>

In the final months of his life, Rainivoninahitriniony is believed to have participated in an attempted coup against the administration of his brother and the queen.  On Friday, March 27, 1868, at two in the afternoon, a massive crowd armed with guns and swords attempted to storm the seat of government at the [[Rova of Antananarivo]] in the interest of exerting control over the succession of the queen, who had been growing ever weaker from dysentery since October 1867.  The leaders of the group successfully captured a number of key figures, including the head of the Queen's Guard. A group of guards, however, managed to escape and raced to notify Prime Minister [[Rainilaiarivony]], who was visiting the countryside estate where Queen [[Rasoherina]] was convalescing. The Prime Minister gave the order to arrest the conspirators, several of whom were promptly captured upon the soldiers' return to the city.  At five in the evening, [[Rasoherina]] delivered a public address, asking those favorable to her rule to walk with her through the capital; a massive crowd paraded with her through the city, demonstrating their support for her continued authority. The queen then delivered a speech at Andohalo ordering the public to turn in any known conspirators to bring them to justice, before re-entering her residence at the [[Rova of Antananarivo|Rova]] to reassure the public with her presence. However, the windows along the route to the Rova were reportedly all ordered shut, whether to protect the Queen from projectiles or to keep the nobles of the upper town from viewing the advanced state of her infirmity. The physical and emotional exertion further weakened the queen, who died four days later, on April 1, 1868.<ref name = "Camille"/>

Two main theories about the role of Rainivoninahitriniony in the coup against [[Rasoherina]] were proposed although the truth was never confirmed.  One theory offered that Rainivoninahitriniony had intended to establish a republic in Madagascar with himself as its president. An alternate and more probable theory maintained that Rainivoninahitriniony had formed an alliance with Protestant leaders who wished to see a male prince named Rasata placed on the throne. This prince would have been a puppet to Prime Minister Rainivoninahitriniony, who was committed to serving the interests of his Protestant allies.<ref name = "Camille"/>

Rainivoninahitriniony died in exile in Ambohimandroso on 5 May 1868.<ref name="Royal Ark"/>

==References==
{{reflist}}

{{MadagascarPMs}}

{{DEFAULTSORT:Rainivoninahitriniony}}
[[Category:Year of birth unknown]]
[[Category:1868 deaths]]
[[Category:Merina people]]
[[Category:Prime Ministers of Madagascar]]
[[Category:Malagasy military personnel]]
[[Category:1824 births]]
[[Category:People from Alaotra-Mangoro]]