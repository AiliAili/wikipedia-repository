{{Infobox college coach
| name = Michael Christianson
| image = 
| alt = 
| caption = Christianson in [[Tampa Bay]] in 2002.
| sport = [[American football|Football]]
| birth_date = {{Birth date and age|1965|4|7}}
| birth_place = [[Boise, Idaho]]
| death_date = 
| death_place = 
| alma_mater = [[Western Oregon University]]
| player_years1 = 1987
| player_team1 = [[Portland State University|Portland State]]
| player_years2 = 1988–1991
| player_team2 = [[Western_Oregon_University|Western Oregon]]
| player_positions = [[Tight End]] - [[Offensive Line]]
| coach_years1 = 1992
| coach_team1 = [[Western Oregon Wolves|Western Oregon]] (OL)
| coach_years2 = 1993–1994
| coach_team2 = [[Western Oregon Wolves|Western Oregon]] (TE)
| coach_years3 = 1995
| coach_team3 = [[Lewis & Clark Pioneers football|Lewis & Clark]] (TE/RB)
| coach_years4 = 1996
| coach_team4 = [[Portland State Vikings football|Portland State]] (OL)
| coach_years5 = 1997–1999
| coach_team5 = [[Portland State Vikings football|Portland State]] (RB)
| coach_years6 = 2000-2001
| coach_team6 = [[Montana State Bobcats football|Montana State]] (TE)
| coach_years7 = 2002–2003
| coach_team7 = [[Tampa Bay Buccaneers|Tampa Bay-NFL]] (OL)
| coach_years8 = 2004–2006
| coach_team8 = [[Nebraska Cornhuskers football|Nebraska]] (SA)
| coach_years9 = 2006–2007
| coach_team9 = [[San Diego Toreros football|San Diego]] (TE)
| coach_years10 = 2011–2013
| coach_team10 = [[San Francisco 49ers|San Francisco-NFL]] (RB)
| coach_years11 = 2014-2016
| coach_team11 = [[ESPN Radio|ESPN Radio Boise]] (Host/Analyst/PD)
| awards = * 3x NFC Division Championships
* Buccaneers ([[Super Bowl|Superbowl]]) Championship ([[Super Bowl XXXVII|XXXVII]])
* [[San Diego Toreros football|San Diego Toreros]] D-IAA Mid-Major National Championship
* [[2005 Alamo Bowl]] Champion Nebraska Cornhuskers
* 2x NFC Championships ([[NFC Championship Game|NFC Champions]])
* 2x ([[Super Bowl|Superbowls]]). ([[Super Bowl XXXVII|XXXVII]]) & ([[Super Bowl XLVII|XLVII]])
}}
'''Michael Christianson''' (born April 7, 1965) is an [[American football]] college and professional coach. Christianson most recently served as the assistant running backs coach and the Director of Technology for the [[San Francisco 49ers]] of the [[National Football League]] (NFL) in 2011<ref>{{cite web|title=49ers Name Christianson Offensive Coach|url=http://www.csnbayarea.com/49ers/49ers-name-christianson-offensive-qc-coach|website=CSNBayArea.com|publisher=CSNBayArea.com|accessdate=23 February 2011}}</ref> thru 2013 which featured a Superbowl appearance against the [[Baltimore Ravens]] in [[Superbowl XLVII]] (47). Prior to his tenure with the 49ers, Christianson also served on [[University of Michigan]] Head Coach [[Jim Harbaugh]]'s staff at the [[University of San Diego]] (USD) where the Toreros of the [[Pioneer Football League]] won a Division I-AA Mid-Major National title in 2006. He has worked as a football coach at several other [[National Collegiate Athletic Association]] (NCAA) institutions, including The [[University of Nebraska]], [[Montana State University]] and [[Portland State University]]. Christianson coached the offensive line in 2002-2003 as a member of [[Jon Gruden]]'s staff and the [[Tampa Bay Buccaneers]],<ref>{{cite web|last1=Reynolds|first1=Scott|title=Tampa Bay Adds 7 Assistant Coaches|url=http://www.scout.com/nfl/buccaneers/story/38087-tampa-bay-adds-7-assistant-coaches|website=BucsBlitz|publisher=BucMag.com|accessdate=25 February 2002}}</ref> winning [[Superbowl XXXVII]] (37). Christianson played tight-end and offensive lineman at [[Western Oregon University]].

== Playing career ==

Christianson was a linebacker and tight-end at [[Kuna High School]] (Kuna, ID). He went on to play college football at [[Portland State University]] (NCAA FCS) and then transferred to [[Western Oregon University]] (NAIA DIVII).

== Coaching career ==

Christianson began his coaching career at his alma mater, [[Western Oregon University]] in 1991 and spent 4 years coaching the offensive line and the tight-ends. In 1995 he spent one season with the [[Lewis & Clark College]] Pioneers (NAIA DivII) as the recruiting coordinator and tight-ends/running backs coach.

He then went on to coach in the [[Big Sky Conference]] and [[Portland State University]] (PSU). At PSU he coached one year as the assistant offensive line coach and 3 years as the Vikings running backs coach. While at [[Portland State]] he is largely attributed for helping to develop one of the most prolific running backs in [[Portland State]] school history in Charles 'Chip' Dunn. Dunn, who played at PSU from 1997-2000, rushed for more than 6,000 yards<ref>{{cite web|title=Charles "Chip" Dunn|url=http://www.goviks.com/mobile/hof.aspx?hof=18|website=Official Site of the Portland State Viking|publisher=Football Induction Class of 2006}}</ref> as a Viking, was a first-team All-American and is a member of the Portland State Athletics Hall of Fame. In 2013 Dunn ranked number 35 among Big Sky Conference's Top 50 All-Time Athletes.<ref>{{cite web|last1=Kasper|first1=John|title=Big Sky's "50 Greatest Male Athletes"|url=http://www.bigskyconf.com/news/2013/10/24/FB_1024131316.aspx|website=Big Sky Conference|publisher=Big Sky Conference|accessdate=24 October 2013}}</ref>

Christianson left [[Portland State]] in 2000 to coach the tight-ends for the Bobcats and [[Montana State University]]. 

He made his NFL coaching debut as the assistant offensive line coach for the 2002 [[Superbowl XXXVII]] Champion [[Tampa Bay Buccaneers]] and head coach [[Jon Gruden]]. After a short stint with the Oakland Raiders as a contractor, he followed Raiders head coach, [[Bill Callahan (American football)|Bill Callahan]] to the [[University of Nebraska]] 2004 thru 2006 where he was the senior offensive assistant and helped Callahan with all facets of the program.<ref>{{cite web|title=Coach Info: Position:|url=http://www.huskers.com/ViewArticle.dbml?DB_OEM_ID=100&ATCLID=36203|website=HuskersNside|publisher=Nebraska Huskers}}</ref> He was instrumental in assisting offensive coordinator [[Jay Norvell]] in developing the west coast offense in Lincoln. The Huskers beat Michigan in the 2005 Alamo Bowl during his tenure.<ref>{{cite web|title=Nebraska 32 Michigan 28|url=https://www.huskermax.com/games/2005/michigan05.html|website=huskermax.com|publisher=huskermax.com|accessdate=28 December 2005}}</ref> While at the University of Nebraska, he oversaw the development and execution of one of the largest athletic technology projects in Nebraska history.

Christianson had a couple stops with both Avid Sports and XOS Digital where he worked to develop, implement and train coaches on the use and benefits of technology as it relates to the sport of football. Christianson is considered an expert when it comes to technology and how it relates to the game of football.<ref>{{cite web|last1=Hogan Ketchum|first1=Karen|title=49ers Streamline Coaching Analysis With Panasonic Workflow|url=http://www.sportsvideo.org/2011/11/22/49ers-streamline-coaching-analysis-with-panasonic-workflow/|website=Sports Video Group|publisher=SVG|accessdate=2 November 2011}}</ref><ref>{{cite web|title=Nebraska Bio|url=http://www.huskers.com/ViewArticle.dbml?DB_OEM_ID=100&ATCLID=36203|website=Nebraska Football|accessdate=5 April 2017}}</ref> He coordinated and helped develop large football technology projects at the Oakland Raiders, the University of Nebraska and the San Francisco 49ers to name just a few.

After working with [[Jim Harbaugh]] at the Raiders, he joined the staff at the [[University of San Diego]] (USD) in 2006.<ref>{{cite web|title=Michael Christianson|url=http://www.usdtoreros.com/sports/m-footbl/mtt/christianson_mike00.html|website=Usdtoreros.cstv.com|publisher=usdtoreros.com|accessdate=2 February 2017}}</ref> At USD, Christianson worked as the tight-end coach and assistant offensive Line coach with current Michigan Offensive Coordinator/Offensive Line Coach, [[Tim Drevno]]. During his tenure, USD won the [[Pioneer Football League]] championship and an NCAA Division I-AA Mid-Major National Championship.<ref>{{cite web|title=Toreros 2006 Mid-Major National Champions|url=http://www.usdtoreros.com/sports/m-footbl/recaps/120206aaa.html|website=usdtoreros.com|publisher=usdtoreros.com|accessdate=3 January 2017}}</ref> That year, USD led the Nation in passing offense, total offense, and scoring offense.<ref>{{cite web|last1=Pioneer Football|first1=League|title=2006 Pioneer Football League Team Statistics Through games of Jul 31, 2008|url=http://cdn.streamlinetechnologies.com/pioneer-football/media/stats/2006/confldrs.htm|website=streamlinetechnologies.com|publisher=streamlinetechnologies.com/pioneer-football|accessdate=31 July 2008}}</ref> He also coached USD starting tight end Chris Ramsey to a 2nd Team Mid-Major All-American honors.<ref>{{cite web|title=19 Chris Ramsey|url=http://www.usdtoreros.com/sports/m-footbl/mtt/ramsey_chris00.html|website=usdtoreros.com/sports/m-footbl/mtt/ramsey_chris00.html|publisher=usdtoreros.com|accessdate=1 February 2017}}</ref>

In 2012, Christianson coached in his second Superbowl after joining the [[San Francisco 49ers]] in February of 2011.<ref>{{cite web|last1=49ers Public Relations|title=49ers Add Assistant Coach|url=http://www.49ers.com/news/article-1/49ers-Add-Assistant-Coach/db889a77-70f4-4bb9-8a1e-2081e769a39d|website=49ers.com|accessdate=23 February 2011}}</ref> Working closely with [[Tom Rathman]], Christianson played a major role in 2011 in the development of the 49ers rushing attack<ref>{{cite web|last1=Espinoza|first1=Alex|title=Playing It Forward|url=http://www.49ers.com/news/article-2/Playing-It-Forward/0c0266a6-20c5-43d5-9229-0c7d4f2162c0|website=49ers.com|publisher=San Francisco 49ers|accessdate=28 December 2011}}</ref> and in mentoring veteran player [[Frank Gore]] to his 5th 1,000 yard season. Gore became the 49ers all time leading rusher in 2011.<ref>{{cite web|last1=Lynch|first1=Kevin|title=Frank Gore sets franchise rushing record|url=http://blog.sfgate.com/49ers/2011/12/04/frank-gore-sets-franchise-rushing-record/|website=SFGate|publisher=Hearst Communications, Inc.|accessdate=4 December 2011}}</ref> Christianson coached running back [[Frank Gore]] in 2012 when Gore was named to the [[NFL Pro Bowl]].<ref>{{cite web|title=2012 Pro Bowl Rosters|url=http://www.nfl.com/probowl/story/09000d5d82578253/article/2012-pro-bowl-rosters|website=NFL.com|publisher=NFL.com|accessdate=27 December 2011}}</ref>

== Personal ==

Christianson is a native of [[Boise, Idaho|Boise Idaho]] and is married to his wife Tracy.<ref>{{cite web|last1=Pilato|first1=Gino|title=Same 8 Questions: Michael Christianson (Former NFL and Collegiate Coach)|url=http://thesportsdaily.com/wendell-maxey-consulting/same-8-questions-michael-christianson-former-nfl-and-collegiate-coach/|website=The Sports Daily|publisher=TheSportsDaily.com|accessdate=10 November 2016}}</ref> He received his bachelors and masters degree in secondary education and administration with a minor in computer applications from [[Western Oregon University]].

== References ==

<references />

{{DEFAULTSORT:Christianson, Michael}}
[[Category:1965 births]]
[[Category:Living people]]
[[Category:Montana State Bobcats football coaches]]
[[Category:San Diego Toreros football coaches]]
[[Category:San Francisco 49ers coaches]]
[[Category:Tampa Bay Buccaneers coaches]]
[[Category:Portland State Vikings football coaches]]
[[Category:Nebraska Cornhuskers football coaches]]
[[Category:Portland State Vikings football players]]
[[Category:Western Oregon Wolves football coaches]]
[[Category:Western Oregon Wolves football players]]