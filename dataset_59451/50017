{{Infobox character
| series      = [[A Song of Ice and Fire]] ''character'' <br> [[Game of Thrones]]
| name        = Davos Seaworth
| image       = Davos Seaworth-Liam Cunningham.jpg
| caption     = [[Liam Cunningham]] as Davos Seaworth
| first       = '''Novel''': <br>''[[A Clash of Kings]]'' (1998) <br>'''Television''': <br>"[[The North Remembers]]" (2012)
| last        = 
| creator     = [[George R. R. Martin]]
| portrayer   = [[Liam Cunningham]] <br> (''[[Game of Thrones]]'')
| alias       = The Onion Knight<br>Davos Shorthand
| gender      = Male
| occupation  = [[Smuggler]] (previously)<br>[[Knight]]<br>Lord<br>Hand of the King to Stannis Baratheon
| family      = House Seaworth
| spouse      = Marya Seaworth
| children    = Dale Seaworth<br>Allard Seaworth<br>Matthos Seaworth<br>Maric Seaworth<br>Devan Seaworth<br>Stannis Seaworth<br>Steffon Seaworth
| lbl21       = Kingdom
| data21      = [[World of A Song of Ice and Fire#The Stormlands|The Stormlands]]
}}

'''Davos Seaworth''' ({{aka}} "the Onion Knight") is a fictional character from George R. R. Martin's A Song of Ice and Fire. He is a POV character in ''[[A Clash of Kings]]'', ''[[A Storm of Swords]]'' and ''[[A Dance with Dragons]]'', of 13 chapters, and a main character in the television adaptation [[Game of Thrones]].

==Character==

===Background===
Davos, a smuggler at the time of Robert Baratheon's rebellion, brought onions along with other food supplies to Storm's End, then under siege by Mace Tyrell and the Redwynes, to Stannis Baratheon and his men. As a reward for this service, Stannis had Davos knighted; as a punishment for years of illicit smuggling, however, Stannis personally "shortened" Davos' left hand, cutting the fingers off at the furthest joint. Davos later kept the remains in a pouch about his neck, and refers to them in the books as his "luck." Davos was given lands on Cape Wrath and has a Keep there. He is married and has seven sons.

===Character===
Davos is loyal to Stannis, due to the life and opportunities that Stannis knighting him has presented to Davos and his family. However, he sometimes disagrees with Stannis's methods. Davos is a devout believer in the Seven, which puts him at odds with Melissandre and the Queen's Men, who worship R'hllor. Davos tries to always be honest to Stannis, speaking his mind instead of saying what Stannis wants to hear.

==Storylines==
[[File:A Song of Ice and Fire arms of House Seaworth.gif|thumb|150px|alt=A coat of arms showing a black ship with a white onion on his sail on a field of grey.|Coat of arms of Davos Seaworth]]
===In the books===
====''A Clash of Kings''====
Due to Joffrey Baratheon's illegitimacy, Stannis names himself heir to his brother King Robert Baratheon. Davos supports him, though he dislikes Stannis converting to R'hllor under the red priestess Melisandre.{{Sfn|''A Clash of Kings''|loc=Prologue}} Davos is sent to deliver letters declaring Queen Cersei's children were born of incest, so they cannot claim the Iron Throne. However Stannis receives little assistance.

Davos sails with Stannis to besiege Storm's End. Stannis' younger brother Renly has also declared himself king and tries to defeat Stannis, but is killed by a shadow birthed by Melisandre. The castellan of Storm's End, Ser Cortnay Penrose, refuses to surrender Storm's End. Davos advises attacking King's Landing before taking Storm's End, but Stannis fears that the stormlords will not follow him if he appears defeated. Davos brings Melisandre, about whom he already has misgivings, to the caverns underneath Storm's End, where she births a "shadow" that kills Penrose and allows Stannis to claim Edric Storm, his illegitimate nephew via Robert.

Davos is given command of a ship at the Battle of the Blackwater, though he disagrees with their commander, Stannis' brother-in-law Ser Imry Florent. [[Tyrion Lannister]] uses wildfire to destroy most of the Baratheon fleet and a chain to trap most of the fleet. Davos' four oldest sons are killed by the wildfire.{{Sfn|''A Storm of Swords''|loc=Chapter 5: Davos I}} Davos is washed ashore on the Spears of the Merling King, losing his "luck", but is recovered by Stannis' men.

====''A Storm of Swords''====
Blaming Melisandre for the loss (due to the strategic import of Tyrion's wildfire and Melisandre's religious associations with fire), Davos plots to murder the priestess.{{Sfn|''A Storm of Swords''|loc=Chapter 10: Davos II}}{{Sfn|''A Storm of Swords''|loc=Chapter 25: Davos III}} However, her magic warns her and he is imprisoned by Ser Axell Florent, who also threatens to kill Davos if he does not help Axell become Stannis' Hand of the King.{{Sfn|''A Storm of Swords''|loc=Chapter 36: Davos IV}} Stannis releases Davos to hear his counsel on attacking Claw Isle. Davos claims that it would be unjust to attack Claw Isle despite Lord Celtigar bending the knee to Joffrey, as its people are innocent and only following their Lord. Pleased by his honesty, Stannis names Davos Hand of the King. Davos then watches a ritual where Stannis uses blood leeched from Edric to curse the three rival kings. After two of these kings, Balon Greyjoy and Robb Stark, die, Stannis and Melisandre consider sacrificing Edric to raise a dragon sleeping under Dragonstone, despite Davos' counsel. On hearing of Joffrey's death, Davos sends Edric Storm east to the Free Cities.

Learning to read, Davos comes across a plea for help from the Wall, and convinces Stannis to sail north and aid the Night's Watch against an invasion of wildlings fleeing the Others. After the victory, Davos is sent to White Harbor to persuade Wyman Manderly, one of the most powerful Northern lords, to support Stannis' cause.{{Sfn|''A Storm of Swords''|loc=Chapter 36: Davos IV}}{{Sfn|''A Storm of Swords''|loc=Chapter 63: Davos
VI}}

====''A Feast for Crows'' and ''A Dance With Dragons''====
Davos arrives at White Harbor and discovers that Manderly is hosting three members of House Frey, who betrayed the Starks at the Red Wedding and killed Robb Stark and many other northmen, including Wylis Manderly. Nonetheless, Davos claims the North should support Stannis as he will give them vengeance, but Manderly imprisons Davos. He sends word to King's Landing that he has executed him, but in reality the man executed was another criminal vaguely resembling Davos. Davos is imprisoned in the Wolf's Den, but released and taken to Manderly, who explains that he could not publicly defy the Lannisters while his only surviving son was a captive of the Freys, but he has been returned. Manderly implies he will murder the three Freys and reveals that many of the other northern lords wish to overthrow House Bolton, who has assumed control of the North following their betrayal of House Stark, but dare not openly defy the Boltons unless a Stark heir can be found. He reveals that he has been harboring Wex Pyke, the former squire to Theon Greyjoy who was witness to Ramsay Bolton's sack of Winterfell and followed Rickon Stark after the sack. Manderly tells Davos that if he retrieves Rickon from the cannibal island of Skagos, the northmen will support Stannis' campaign.{{Sfn|''A Dance with Dragons''|loc=Chapter 9: Davos I}}{{Sfn|''A Dance with Dragons''|loc=Chapter 19: Davos III}}

===In the TV series===
[[File:Liam Cunningham by Gage Skidmore.jpg|thumb|right|upright|[[Liam Cunningham]] plays the role of Davos Seaworth in the [[Game of Thrones|television series]].]]
In the [[HBO]] series Davos has the fingers taken from his right hand, as Liam Cunningham is left-handed. He has only one son, Matthos.
====Second season====
When Renly refuses to surrender to Stannis, Davos is ordered to sail with Melisandre beneath Storm's End. When they reach shore, Davos is horrified when Melisandre gives birth to a shadowy demon, who kills Renly. The stormlords bend the knee to Stannis, who plans to launch a naval attack on King's Landing and gives Davos command of the fleet. As the Baratheon fleet arrives in Blackwater Bay, a ship filled with wildfire is detonated in the middle of the fleet; Davos' ship is one of the closest, and he is thrown into the sea when his ship is destroyed.
====Third season====
Davos is revealed to have survived by swimming to a rock, and is rescued by his friend, the pirate Salladhor Saan, who reveals that Matthos died during the Battle of the Blackwater. Davos blames Melisandre for Stannis' defeat and unsuccessfully tries to assassinate her before being thrown into the dungeons. During his captivity, Stannis' daughter Shireen discovers that Davos is illiterate and teaches him to read. Stannis eventually releases Davos, naming him as his hand. Soon after, Stannis and Melisandre plot to sacrifice Robert's bastard Gendry, until Davos frees him and helps him escape Dragonstone. An enraged Stannis resolves to execute Davos, until Davos presents a letter from Castle Black warning of the White Walkers' return. Melisandre corroborates this and counsels Stannis that he will need Davos when the Long Night comes, prompting Stannis to pardon Davos.

====Fourth season====
After Joffrey Baratheon's death, Stannis reprimands Davos for not being able to secure more men. Davos arranges a meeting with Tycho Nestoris, a representative of the Iron Bank of Braavos, and persuades him to give their financial backing to Stannis instead. Davos uses the money to hire ship and sellswords and the Baratheon army travels to the Wall, defeating the wildling army besieging it.
====Fifth season====
Davos accompanies the Baratheon forces on their march to Winterfell. During their march, Ramsay Bolton and his men infiltrate the camp and destroy their supplies. Davos is sent back to the Wall to ask for more supplies, unaware that Stannis plans to sacrifice Shireen to assure his victory. Lord Commander Jon Snow, though sympathetic, is unable to offer help. Soon after, Melisandre arrives at Castle Black, having fled in the aftermath of Shireen's sacrifice. Davos realises that Stannis has been defeated and Shireen is dead, though Melisandre does not reveal her role.

====Sixth season====
Davos is the first to discover Jon Snow's body after his assassination, and barricades his body in a storeroom with the help of Jon's loyalists. After the wildlings imprison the mutineers who killed Jon, Davos persuades Melisandre to attempt a resurrection of Jon, which is ultimately successful.<ref>{{cite web |archiveurl= https://web.archive.org/web/20160817123724/http://www.ign.com/articles/2016/05/02/game-of-thrones-home-review |archivedate=August 19, 2016 |url=http://www.ign.com/articles/2016/05/02/game-of-thrones-home-review |title=Game of Thrones: "Home" Review - IGN |accessdate=May 1, 2016 |date=May 1, 2016 |author=Fowler, Matt |publisher=[[IGN]]|deadurl=no}}</ref> Jon's death releases him from his Night's Watch vows and he decides to gather allies to march with him on Winterfell. Davos accompanies him, and is able to convince the young Lady Lyanna Mormont to offer House Mormont's support, though few other houses offer their support. During the army's march to Winterfell, they briefly made camp where Stannis had before, and whilst walking around the area, Davos found the remnants of the pyres where Shireen had been burnt on the stake,in which he found the burnt remains of the wooden stag he had carved to Shireen as a gift. Davos initially keeps this revelation to himself, instead participating in the Stark loyalists' victory over the Bolton forces. After the battle has been won and Winterfell retaken, Davos confronted Melisandre and Jon, and barely managing to hold back tears, he threw the wooden toy stag to Melisandra, shouting that she own up to what she had down, tearfully proclaiming he had loved Shireen as if she was her own daughter. Quietly, Melisandre defended her actions by stating that Stannis' army was trapped in a Blizzard and starving while their horses froze to death, and that sacrificing Shireen was their only hope. Declaring the Lord of Light to be evil for demanding the burning of children, he simply asked her; their only hope for what? As Stannis, Selyse and what remained of the Baratheon army all died anyway. Melisandre, once again, defended her actions by stating she had been wrong, was questioned by Davos as to how many had died because she was wrong. Looking towards Jon Snow, Davos requested permission to execute Melisandre for murder as she had committed to her crimes, though Jon instead exiled her on pains of death. As Melisandre turned to leave, Davos briefly stepped in her way and coldly stated that if she ever came North again, he himself would personally execute her. Afterwards, as the Northern lords gathered to choose a new King In the North, Davos Seaworth was one amongst them to hail Jon Snow as the White Wolf and the King in the North.

==Reception==
Schmoop.com has claimed Davos Seaworth demonstrates the social stratum of the Seven Kingdoms, by becoming a knight despite his humble origins.<ref>{{cite web|url=http://www.shmoop.com/a-clash-of-kings/davos-seaworth.html|title=Davos Seaworth|work=Shmoop}}</ref> [[David Benioff]] has said that Davos is one of his favorite characters.<ref>{{cite web|url=http://www.westeros.org/GoT/Features/Entry/Interview_with_David_Benioff_and_Dan_Weiss/|title=Interview with David Benioff and Dan Weiss|publisher=Westeros.org}}</ref>

== References ==
{{Reflist|30em}}

{{ASOIAF}}

{{DEFAULTSORT:Seaworth, Davos}}
[[Category:A Song of Ice and Fire characters]]
[[Category:Fictional advisors]]
[[Category:Fictional amputees]]
[[Category:Fictional knights]]
[[Category:Fictional outlaws]]
[[Category:Fictional sailors]]
[[Category:Characters in American novels of the 20th century]]
[[Category:Characters in American novels of the 21st century]]
[[Category:Fictional ship captains]]
[[Category:Fictional smugglers]]
[[Category:Fictional lords and ladies]]