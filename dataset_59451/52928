{{Infobox journal
| title = Surgical Innovation
| cover = [[File:Surgical Innovation.jpg]]
| editor = Adrian E. Park, MD, FRCS, Lee Swanstrom, MD
| discipline = 
| former_names = 
| abbreviation = 
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1994-present
| openaccess = 
| license = 
| impact = 1.537
| impact-year = 2012
| website = http://www.uk.sagepub.com/journals/Journal201793?siteId=sage-uk&prodTypes=any&q=Surgical+Innovation&fs=1
| link1 = http://sri.sagepub.com/content/current
| link1-name = Online access
| link2 = http://sri.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1553-3506
| eISSN =
| OCLC = 240897558
| LCCN =
}}

'''''Surgical Innovation''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Surgery]]. The journal's [[Editor-in-Chief|editors]] are Adrian E. Park, MD, FRCS ([[Dalhousie University]]) and Lee Swanstrom, MD ([[Legacy Health System]]). It has been in publication since 1994 and is currently published by [[SAGE Publications]].

== Scope ==
''Surgical Innovation'' focuses on minimally invasive surgical techniques, new instruments and endoscopes, and new technologies. The journal aims to help surgeons learn new techniques, understand and adapt to new technologies, maintain surgical competencies, and apply surgical outcomes data to their practices. 

== Abstracting and indexing ==
''Surgical Innovation '' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2012 [[impact factor]] is 1.537, ranking it 88 out of 198 journals in the category ‘Surgery’.<ref name=WoS>{{cite book |year=2013 |chapter=Journals Ranked by Impact: Surgery |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2013-12-21|work=Web of Science |postscript=.}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://sri.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]