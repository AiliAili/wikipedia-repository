{{Infobox military conflict
|conflict=Battle of Jenné
|date=April 26, 1599<ref name="Hunwick, page 234">Hunwick, page 234</ref>
|place=[[Djenné]], [[Mali]]
|result=Decisive Moroccan Victory<br/>Collapse of Mali Empire
|combatant1=[[Mali Empire]]
|combatant2=[[Morocco]]<br/>Moroccan Pashalik of Timbuktu 
|commander1=Mansa [[Mahmud IV]]
|commander2=Governor Sayyid Mansur<br/>Qa’id [[al-Mustafa al-Fil]] <br/>Qa’id [[Ali bin Abd Allah al-Tilimsani]] <br/>Jenne-koi [[Muhammad Kinba bin Isma’il]]
|strength1=Mandinka infantry and cavalry armed with lance and bow<br/>Bamana infantry and cavalry armed with lance and bow
|strength2=Moroccan infantry armed with arquebus<br/>Mandinka infantry and cavalry armed with lance and bow
|casualties1=Unknown but substantial
|casualties2=Unknown 
}}

The '''Battle of Jenné''' was a military engagement between forces of the [[Mali Empire]] and the [[Saadi dynasty|Moroccan]] [[Pashalik of Timbuktu]]. The battle marked the effective end of the great Mali Empire and set the stage for a plethora of smaller West African states to emerge.

==Background==
Throughout the 15th and 16th centuries, the Mali Empire had been in near-constant state of decline. All of its periphery vassal territories had become independent states with some even challenging Mali’s sovereignty, notable [[Songhai Empire|Songhai]].<ref>Oliver, page 431</ref> In 1591, the Songhai Empire was defeated at the [[Battle of Tondibi]] by a Moroccan expeditionary force. Thanks to the Moroccan’s use of gunpowder weapons such as the arquebus and cannon, Songhai power was pushed back eastward across the Niger where they formed the smaller but still robust [[Dendi Kingdom]].<ref name="Thornton, page xix">Thornton, page xix</ref> With Songhai out of the way, the ruler of Mali Mansa [[Mahmud IV]] set his sights on rebuilding his moribund empire.<ref name="Ki-Zerbo & Niane, page 75">Ki-Zerbo & Niane, page 75</ref> The first step in this grand plan would be to seize the valuable city-state of [[Jenné]], which controlled trade along the inland Niger valley.

===Mali Forces===
Mansa Mahmud IV set about rallying his remaining provinces along with groups that had formerly been vassals of the Mali Empire.  He sent out an envoy to his last two provinces of Binduku (Bendugu), Kala and Sibiridugu.<ref>Oliver, page 384</ref> Only two minor chiefs responded with a promise of aid in the upcoming battle. These were the kings, called “koi” in the Tarik es-Soudan of Farka or Fadku (part of Kala) and Ama.<ref name="Hunwick, page 234" />  Another minor leader who offered his assistance at the time was [[Hammad Amina]], a Fulbe chief ruling in Masina.<ref name="Hunwick, page 234">Hunwick, page 234</ref> Still, the mansa was able to raise a substantial army and marched on Jenné with the hopes of resuscitating his dying empire.<ref name="Hunwick, page 234" />

===Pashalik Forces===
The trading emporium of Jenné was subject to the Moroccan pashalik of Timbuktu, named for the city where the Moroccan expeditionary force governed from.<ref name="Gray, page 161">Gray, page 161</ref> Previously, the pashalik had taken Jenné without a fight and preserved its king, [[Muhammad Kinba bin Isma’il]] on the throne under a Moroccan resident,<ref name="Gray, page 161" /> Governor Sayyid Mansur.<ref name="Hunwick, page 234">Hunwick, page 234</ref> There are no details on what kind of troops were present when the mansa first began marching toward it, but once the governor was alerted, he sent word to Timbuktu for reinforcements. In response, Pasha Ammar sent a force headed by Qa’id [[al-Mustafa al-Fil]] and Qa’id [[Ali bin Abd Allah al-Tilimsani]], which included arquebusiers.<ref name="Hunwick, page 234" />

==Breakout from Sanuna==
The Moroccan reinforcements arrived via river boats,<ref>Oliver, page 455</ref> making good use of the city’s position on the Niger to ferry troops quickly to the battle.  They arrived on May 26 to find Mansa Mahmud IV and his army encamped over the entire dune of Sanuna,<ref name="Hunwick, page 234">Hunwick, page 234</ref> which reached down into the creek where the boats were to enter the city.<ref name="Hunwick, page 234" /> The reinforcements had to fight their way into the city. Using their guns in what Arab records call a massive bombardment,<ref name="Ki-Zerbo & Niane, page 75" /> the reinforcements were able to repel the mansa’s army.<ref name="Shillington, page 922">Shillington, page 922</ref> The pashalik forces entered the city, but the Mali army was still encamped and far from defeated.

==Noon Attack==
Inside Jenné, Governor Sayyid Mansur was advised to attack the mansa’s forces immediately before anyone else rallied to his banner.<ref name="Hunwick, page 234">Hunwick, page 234</ref> After making counsel with his advisors he is quoted as saying:

<blockquote>
“Our encounter with them shall be after the noon worship on Friday.”<ref name="Hunwick, page 234" />
</blockquote>

True to his word the governor along with the king of Jenne went out and engaged the Mali army again. It was a complete route, with the Mali army suffering many casualties.<ref name="Hunwick, page 234">Hunwick, page 234</ref> At the end of the battle, Mansa Mahmud IV was forced to flee.

==Aftermath==
Mali’s defeat at Jenne destroyed the temporary alliance Mahmud IV was able to pull together, and the Mali Empire ceased to be a political factor in the region. The mansa still received a great deal of respect for even attempting to re-establish the empire, according to Arab records.<ref name="Ki-Zerbo & Niane, page 75" /> The Moroccans’ allies, says the Ta’rikh al-Sudan,

<blockquote>
“Having met him in a safe place, saluted him as sultan and bore their heads to do him honour, as was their custom”.<ref name="Ki-Zerbo & Niane, page 75" />
</blockquote>

Mansa Mahmud IV retired to Niani and died by 1610. His three sons tore the remnants of the country apart, and Mali became a scattering of loosely allied chiefdoms.<ref name="Shillington, page 922"/> These were swallowed up by the [[Bamana Empire]],<ref name="Ki-Zerbo & Niane, page 75" /> which even the pashalik of Timbuktu would eventually have to pay tribute to.<ref>Holt, page 15</ref> Still, none of the emergent powers would come close to the hegemony of Mali or Songhai. West Africa transitioned to a region ruled by smaller and less centralized states until the 19th century.<ref>Gray, page 143</ref>

==Reasons for the Mandinka Defeat==
The defeat of Mahmud IV at Jenné has been attributed to several causes. Details are sparse on the nature of both armies. The overarching theme in why the battle turned out the way it did revolves around the ability of each side to rally its forces. In few other instances are the effects of imperial collapse in the face of a changing political and military environment so well demonstrated. The Mali which faced off against the pashalik forces and its native allies was not the grand military of [[Mansa Musa]]. In fact, it had reversed back to its pre-imperial structure just at the time when leaps forward in technology and organization were essential to the empire’s survival.

===Unreliable Allies===
One of the most highlighted is the mansa’s betrayal by Hammad Amina, the chief of the Fulbe at Masina.<ref name="Thornton, page xix"/> Previously, Amina had promised support in taking Jenné, but went over to the Moroccans with whom he already had a king-vassal type relationship.<ref>Hunwick, page 236</ref> He advised the Moroccans on what to expect from the Mandinka-Bamana force and kept his own forces from joining the battle.<ref name="Ki-Zerbo & Niane, page 75" /> The Fulbe are not mentioned as lending military aid to Jenné’s defenders, but their absence from the battlefield may have had a great effect on the final outcome. The pashalik of Timbuktu would also regret putting faith in Hammad Amina. The Fulbe would eventually thrown off their nominal vassalage to the pashalik and hand the Moroccans their first major defeat in the Sahel.<ref>De Villiers & Hirtle, page 222</ref>

===Unreliable Provinces===
Apart from Amina’s betrayal, another reason for Mali’s defeat may lay in its inability to draw on its remaining provinces for men. Had Mahmud IV been able to draw on the support of his traditional division commanders (the Sanqara-Zuma and Farim-Soura), he would have also had access to the reluctant the governor or Kala-sha of Kala province. Kala-sha Bukar refused to join the mansa without the two commanders, remarking in private:

<blockquote>
"Since his two greatest lieutenants are not accompanying  him, the situation is hopeless." <ref name="Hunwick, page 234" />
</blockquote>

Mali’s disunity, already in full throttle since the mid-16th century had finally caught up with it at Jenné. In fact when Mansa Mahmud IV went to Kala to fetch what he still believed to be a loyal ally, he found that the Kala-sha had left for Jenné to fight alongside the Moroccans.<ref name="Hunwick, page 234" />

===Gunpowder===
Lastly, the Mali Empire, like its Songhai competitor, had failed to modernize its military machine. This was not for lack of trying, however. Previous mansas had tried in vain to purchase firearms or firearm-equipped mercenaries from the Portuguese to no avail.<ref name="Shillington, page 922"/> The Mali Empire went to war with the same methods it had been using since the days of [[Sundjata]] but without the unity or scale of its past armies.

On the other hand, the gunpowder weapons of the pashalik forces were not decisive against the Mali Empire, despite the latter’s reliance on traditional infantry and cavalry forces. The use of guns saved the pashalik reinforcements from annihilation (along with the timely intervention of the king of Jenné), but they didn’t set the mansa’s forces to flight either. Mansa Mahmud IV and his army stayed encamped at the dune of Sanuna awaiting a second engagement. The Moroccans were now faced with the law of diminishing return, as their guns did not have the tactical effect they had held not even a decade earlier. A Mali Empire armed with guns instead of lances and arrows might have stopped Jenné’s reinforcement or taken the city outright.

==See also==
*[[Military history of the Mali Empire]]
*[[Mansa Mahmud IV]]
*[[Djenne]]
*[[Mali Empire]]
*[[History of Mali]]

==References==
<references/>

==Sources==
*{{cite book |author1=De Villiers  |author2=Sheila Hirtle  |lastauthoramp=yes |title=Timbuktu: the sahara’s Fabled City of Gold |publisher=Walker & Company |location=New York |year=2007 |pages=302 pages |isbn=0-8027-1497-8}}
*{{cite book |author=Gray, Richard |title=The Cambridge History of Africa: From c. 1600 to c. 1790 |publisher=Cambridge University Press |location=Cambridge |year=1975 |pages=752 pages |isbn=0-521-20413-5}}
*{{cite book |author1=Holt, P.M. |author2=Ann K. S. Lambton |author3=Bernard Lewis  |lastauthoramp=yes |title=The Cambridge history of Islam |publisher=Cambridge University Press |location=Cambridge |year=1977 |pages=966 pages |isbn=0-521-29137-2}}
*{{cite book |author=Hunwick, John |title=Timbuktu & the Songhay Empire: Al-Sa'dis Ta`rikh al-sudan down to 1613 and other Contemporary Documents |publisher=BRILL |location=Leiden |year=1988 |pages=480 pages |isbn=90-04-12822-0}}
*{{cite book |author1=Ki-Zerbo, J  |author2=D.T. Niane  |lastauthoramp=yes |title=UNESCO General History of Africa, Vol. IV: Africa from the Twelfth to the Sixteenth Century (Abridged Edition) |publisher=University of California Press |location=Berkeley |year=1998 |pages=277 Pages |isbn=0-520-06699-5}}
*{{cite book |author=Oliver, Roland |title=The Cambridge History of Africa Volume 3 1050 – c. 1600 |publisher=Cambridge University Press |location=Cambridge |year=1975 |pages=816 Pages |isbn=0-521-20981-1}}
*{{cite book |author=Shillington, Kevin |title=Encyclopedia of African History, Vol. 1 |publisher=Routledge |location=London |year=2004 |pages=1912 Pages |isbn=1-57958-245-1}}
*{{cite book |author=Thornton, John K. |title=Africa and Africans in the Making of the Atlantic World, 1400-1800 (Second Edition) |publisher=Cambridge University Press |location=Cambridge |year=1998 |pages=378 Pages |isbn=0-521-62724-9}}

{{coord missing|Mali}}

{{DEFAULTSORT:Battle Of Jenne}}
[[Category:Mali Empire]]
[[Category:Conflicts in 1599]]
[[Category:Wars involving Mali]]
[[Category:Battles involving Morocco]]
[[Category:History of Azawad]]
[[Category:16th century in Morocco]]
[[Category:16th century in Mali]]
[[Category:1599 in Africa]]
[[Category:Conflicts in Mali]]