{{Infobox journal
| title = Combustion, Explosion, and Shock Waves
| cover = 
| editor = Vladimir M. Titov
| discipline = [[Physical chemistry]] of [[combustion]]
| former_names = 
| abbreviation = Combust. Explos. Shock Waves
| publisher = [[Nauka (publisher)|MAIK Nauka/Interperiodica]] and [[Springer Science+Business Media]]
| country = 
| frequency = Bimonthly
| history = 1965-present
| openaccess = 
| license = 
| impact = 0.399
| impact-year = 2012
| website = http://www.springer.com/physics/classical+continuum+physics/journal/10573
| link1 = http://link.springer.com/journal/volumesAndIssues/10573
| link1-name = Online archive
| link2 = http://www.maik.ru/cgi-perl/journal.pl?lang=eng&name=cesw
| link2-name = Journal page at MAIK Nauka/Interperiodica
| JSTOR = 
| OCLC = 1564276
| LCCN = 
| CODEN = 
| ISSN = 0010-5082 
| eISSN = 1573-8345
}}
'''''Combustion, Explosion, and Shock Waves''''' (''Russian'': '''''Fizika Goreniya i Vzryva''''', ''[[:ru:Физика горения и взрыва|Физика горения и взрыва]]'') is the [[English-language]] translated version of the Russian [[Peer review|peer-reviewed]] [[scientific journal]], ''Fizika Goreniya i Vzryva''. It covers the [[combustion]] of gases and materials, [[detonation]] processes, dispersal and transformation of substances, and [[Shock wave|shock-wave propagation]]. The [[editor-in-chief]] is [[Vladimir M. Titov]].

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Current Contents]]/Physical, Chemical and Earth Sciences
* [[Science Citation Index Expanded]]
* [[Scopus]] 
* [[Inspec]] 
* [[Chemical Abstracts Service]] 
* [[Compendex|EI-Compendex]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.399.<ref name=WoS>{{cite book |year=2013 |chapter=Combustion, Explosion, and Shock Waves |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/physics/classical+continuum+physics/journal/10573}}


[[Category:Springer Science+Business Media academic journals]]
[[Category:Physical chemistry journals]]
[[Category:English-language journals]]
[[Category:Russian-language journals]]
[[Category:Magazines published in Novosibirsk]]
[[Category:Publications established in 1965]]
[[Category:Nauka academic journals]]
[[Category:Bimonthly journals]]