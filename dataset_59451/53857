[[File:Lindos4.svg|400px|right]]

The '''Fletcher–Munson curves''' are one of many sets of [[equal-loudness contour]]s for the human ear, determined experimentally by [[Harvey Fletcher]] and [[Wilden A. Munson]], and reported in a 1933 paper entitled "Loudness, its definition, measurement and calculation" in the ''Journal of the Acoustic Society of America''.<ref>Fletcher, H. and Munson, W.A. "Loudness, its definition, measurement and calculation", ''Journal of the Acoustic Society of America'' 5, 82-108 (1933).</ref>

== Background ==
The first research on the topic of how the ear hears different frequencies at different levels was conducted by Fletcher and Munson in 1933. Until recently, it was common to see the term ''Fletcher–Munson'' used to refer to equal-loudness contours generally, even though a re-determination was carried out by Robinson and Dadson in 1956, which became the basis for an ISO 226 standard.

It is now better to use the generic term [[equal-loudness contours]], especially as a 2003 survey by ISO redefined the curves in a new standard.<ref>{{citation |url=http://www.nedo.go.jp/itd/grant-e/report/00pdf/is-01e.pdf |title=ISO 226:2003 |archive-url=https://web.archive.org/web/20070927000000/http://www.nedo.go.jp/itd/grant-e/report/00pdf/is-01e.pdf |archive-date=September 27, 2007 }}</ref>

According to the ISO report, the Robinson–Dadson results were the odd one out, differing more from the current standard than did the Fletcher Munson curves. The report states that it is fortunate that the 40-[[phon]] Fletcher–Munson curve on which the [[A-weighting]] standard was based turns out to have been in agreement with modern determinations.

The article also comments on the large differences apparent in the low-frequency region, which remain unexplained. Possible explanations are:

* The equipment used was not properly calibrated.
* The criteria used for judging equal loudness at different frequencies had differed.
* Subjects were not properly rested for days in advance, or were exposed to loud noise in traveling to the tests which tensed the [[tensor tympani]] and [[stapedius muscle]]s controlling low-frequency mechanical coupling.

== See also ==
*[[Robinson-Dadson curves]]
*[[Weighting filter]]
*[[CCIR (ITU) 468 Noise Weighting]]
*[[A-weighting]]
*[[Audio quality measurement]]
*[[Listener fatigue]]
*[[Mel scale]]

== References ==
{{reflist}}

== External links ==
*[http://www.iso.org/iso/en/CatalogueDetailPage.CatalogueDetail?CSNUMBER=34222&scopelist=ALL ISO Standard]
*[http://www.sengpielaudio.com/Fletcher-MunsonIsNotRobinson-Dadson.pdf Fletcher-Munson is not Robinson-Dadson - pdf]
*[http://www.aist.go.jp/aist_e/latest_research/2003/20031114/20031114.html Full Revision of International Standards for Equal-Loudness Level Contours (ISO 226)]
*[http://www.phys.unsw.edu.au/~jw/hearing.html Hearing curves and on-line hearing test]
*[http://www.lindos.co.uk/cgi-bin/FlexiData.cgi?SOURCE=Articles&VIEW=full&id=17 Equal-loudness contours by Fletcher and Munson]

{{Acoustics}}

{{DEFAULTSORT:Fletcher-Munson curves}}
[[Category:Psychoacoustics]]
[[Category:Audio engineering]]

[[fr:Courbe isosonique]]