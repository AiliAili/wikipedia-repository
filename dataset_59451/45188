{{Multiple issues|
{{primary sources|date=October 2014}}
{{one source|date=October 2014}}
{{more footnotes|date=October 2014}}
}}

{{Infobox military person
|name          =Roland John Barnick
|image         =
|caption       =
|birth_date    ={{Birth date|1917|1|9}}
|death_date    ={{Death date and age|1996|1|28|1917|1|9}}
|birth_place   =[[Max, North Dakota]]
|death_place   =
|placeofburial =
|placeofburial_label =
|placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
|nickname      =
|birth_name    =
|allegiance    ={{flag|United States|23px}}
|branch        =[[File:Roundel of the USAF.svg|23px]] [[U.S. Army Air Corps]]<br>[[United States Air Force]]
|serviceyears  =1939 - 1967
|rank          =[[Brigadier General]]
|servicenumber =
|unit          =
|commands      =[[63rd Troop Carrier Wing]]<br>[[438th Military Airlift Wing]]
|battles       =[[World War II]]<br>[[Korean War]]
|battles_label =
|awards        =[[Silver Star]]<br>[[Bronze Star Medal]]<br>[[Order of the Sword (United States)|Order of the Sword]]
|relations     =
|laterwork     =
|signature     =
}}
'''Roland John Barnick''' (January 9, 1917 – January 28, 1996) was [[United States|American]] [[military pilot]] and [[general]]. He served in [[World War II]] and [[Korean War]].

Barnick's last post was commander of the [[438th Military Airlift Wing]], with headquarters at McGuire Air Force Base, N.J. The wing was the major Military Airlift Command operational unit on the East Coast, and was responsible for meeting airlift requirements in the Eastern United States, the Atlantic area and to the West Coast for shipment to Southeast Asia.

== Biography ==

===Education===
General Barnick was born in 1917 in Max, N.D., where his parents, Mr. and Mrs. John Barnick, still reside. In 1934 he graduated from Max High School and entered [[North Dakota State College]]. There he received a bachelor of science degree in mathematics and chemistry in June 1938.

Upon graduation from college, he joined the U.S. Army Air Corps as an aviation cadet. He was awarded [[pilot's wings]] and commissioned a second lieutenant in 1939 at Kelly Field, Texas.

===Early career and World War II===
General Barnick's first assignment after Kelly was the Philippine Islands. In Manila he commanded the Headquarters Squadron, [[Far East Air Service Command]]. After the Japanese attack on Pearl Harbor, he took his squadron to Bataan. When Bataan fell, General Barnick flew General [[Carlos Romulo]] to safety in Mindanao aboard the last plane to leave. For this feat he was awarded the Silver Star.

Following the fall of Bataan, General Barnick continued to serve in the Southwest Pacific and was soon promoted to captain. In May 1942, he returned to the United States to command a B-17 squadron at Boise, Idaho. Shortly thereafter he was assigned as Commander of a B-17 provisional group, training for combat duty. The B-17 group deployed to England under his command. Upon return to the United States he was appointed commander of a B-24 training group at Tonopah, Nev. During this assignment, General Barnick was promoted to lieutenant colonel and became deputy commander of a B-29 group. Later as commander of the B-29 group he went to the Southwest Pacific and led his unit in bombardment activities against the Japanese homeland. General Barnick was awarded the Bronze Star for his service during this period.

===After World War II===
At the end of World War II, he served in Japan with a troop carrier group flying C-54 aircraft. Following duty in Japan, he became professor of air science at the [[University of Wyoming]], where he remained until 1950. On July 3 he was promoted to colonel.

When the Korean war began, General Barnick was assigned to Headquarters [[Continental Air Command]] at [[Mitchel Air Force Base]], N.Y., as chief of the Mobilization Branch. In this capacity, he was responsible for the mobilization of a large number of Air Force reservists who served in the Korean War.

In 1953 he was appointed director of military personnel for [[U.S. Air Forces in Europe]] at Wiesbaden, Germany. After three years in this assignment, General Barnick was selected to attend the Industrial College of the Armed Forces, in Wash., D.C. for a year.

===63rd Troop Carrier Group===
General Barnick was assigned as commander of the 63rd Troop Carrier Group at [[Donaldson Air Force Base]], S.C. in June 1957, after completing studies at the [[Industrial College of the Armed Forces]]. His group executed many successful missions of international significance. History was made at the South Pole when unprecedented paradrops of supplies and equipment were completed to the American Scientific Station. During the Lebanon crisis in 1958, General Barnick's Task Force airlifted military units and equipment to the troubled area to help preserve the peace. His aircraft and crews established records throughout the world by moving tons of cargo and passengers on short notice in support of national policy.

After two and one-half years as commander of the 63rd Troop Carrier Group, General Barnick was assigned to Headquarters [[Military Air Transport Service]] as inspector general. He served in this capacity from June 1960 until July 1961 when he was appointed chief of staff of the Military Air Transport Service.

Reassigned to Donaldson in September 1961, General Barnick became commander of the 63rd Troop Carrier Wing. The organization was moved from Donaldson Air Force Base, S.C., to [[Hunter Air Force Base]], Ga. in April 1963. During the period of this movement, General Barnick's organization maintained normally heavy commitments, even though operating from two separate bases. He assumed this position in June 1964.

Following his assignment to McGuire, General Barnick was, in August 1964, nominated for promotion to brigadier general by President [[Lyndon B. Johnson]]. On Sept. 16, the promotion became effective.

On 1 August 1967 he retired from active service as general and command pilot, having logged more than 5,000 hours.

== Awards and decorations ==
* [[Silver Star]] (1942)
* [[Bronze Star Medal]] (194?)
* [[Order of the Sword (United States)|Order of the Sword]] (26 May 1967)<ref>[http://afehri.maxwell.af.mil/Pages/Sword.htm Members of the Order of the Sword]</ref>

== See also ==
{{Portal|Biography|Military history|United States Air Force}}

== References ==
* {{USGovernment|sourceURL=[http://www.af.mil/information/bios/bio.asp?bioID=4611 Brigadier General Roland John Barnick]}}
{{reflist|2}}

== External links ==
* {{webarchive |date=2012-12-12 |url=http://archive.is/20121212025827/http://www.af.mil/information/bios/bio.asp?bioID=4611 |title=Af.mil - Brigadier General Roland John Barnick}}

{{DEFAULTSORT:Barnick, Roland John}}
[[Category:United States Army officers]]
[[Category:United States Air Force generals]]
[[Category:American military personnel of World War II]]
[[Category:American military personnel of the Korean War]]
[[Category:Recipients of the Silver Star]]
[[Category:Recipients of the Order of the Sword (United States)]]
[[Category:1917 births]]
[[Category:1996 deaths]]
[[Category:American expatriates in Japan]]