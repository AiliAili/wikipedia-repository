{{Infobox journal
| title         = Mathematics & Mechanics of Solids
| cover         = [[File:Mathematics and Mechanics of Solids.jpg]]
| editor        = David J Steigmann             
| discipline    = [[Mathematics]]
| peer-reviewed = 
| language      = 
| former_names  =    
| abbreviation  = Math. & Mech. of Solids
| publisher     = [[SAGE Publications]]
| country       = 
| frequency     = 8 Times/Year  
| history       = 1996-present
| openaccess    = 
| license       = 
| impact        = 1.836   
| impact-year   = 2015
| website       = http://www.sagepub.com/journalsProdDesc.nav?prodId=Journal201478
| link1         = http://mms.sagepub.com/content/current
| link1-name    = Online access
| link2         = http://mms.sagepub.com/content/by/year
| link2-name    = Online archive
| JSTOR         = 
| OCLC          = 40810423
| LCCN          = 96658706 
| CODEN         = MMESFP
| ISSN          = 1081-2865  
| eISSN         = 1741-3028 
| boxwidth      = 
}}

'''''Mathematics & Mechanics of Solids''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the fields of [[Mechanics]] and [[Mathematics]]. The journal's [[editor-in-chief|editor]] is David J Steigmann  ([[University of California]]). It has been in publication since 1996 and is currently published by [[SAGE Publications]].

== Scope ==
''Mathematics and Mechanics of Solids'' is an international journal which publishes original research in solid mechanics and materials science. The journal’s aim is to publish original, self-contained research that focuses on the mechanical behaviour of solids with particular emphasis on mathematical principles.

== Abstracting and indexing ==
''Mathematics & Mechanics of Solids'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2015 [[impact factor]] is 1.836, ranking it 111 out of 271 journals in the category ‘Materials Science, Multidisciplinary’.<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Materials Science, Multidisciplinary |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2014-09-30 |work=Web of Science |postscript=.}}</ref> and 25 out of 101 journals in the category ‘Mathematics, Interdisciplinary Applications’.<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Mathematics|title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> and 42 out of 135 journals in the category ‘Mechanics’.<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Mechanics|title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2014-09-30 |work=Web of Science |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://mms.sagepub.com/}}

{{DEFAULTSORT:Mathematics and Mechanics of Solids}}
[[Category:Mathematics journals]]