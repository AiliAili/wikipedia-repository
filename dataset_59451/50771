{{Infobox journal
| title = British Journal of Dermatology
| cover = [[File:BJD-journal-cover.jpg]]
| editor = Alex Anstey
| discipline = [[Dermatology]]
| abbreviation = Br. J. Dermatol.
| publisher = [[Wiley-Blackwell]]  on behalf of the [[British Association of Dermatologists]]
| country = United Kingdom
| frequency = Monthly
| history = 1888–present
| openaccess =
| license =
| impact = 4.275
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2133
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2133/currentissue
| link1-name = Online archive
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2133/issues
| link2-name = Online archive
| JSTOR =
| OCLC = 614652454
| LCCN = 11014759
| CODEN = BJDEAZ
| ISSN = 0007-0963
| eISSN = 1365-2133
}}
The '''''British Journal of Dermatology''''' is a monthly [[Peer review|peer-reviewed]] [[medical journal]] that covers the field of [[dermatology]]. It is published by [[Wiley-Blackwell]] on behalf of the [[British Association of Dermatologists]]. The journal was established in 1888 and the [[editor-in-chief]] is Alex Anstey. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 4.2.<ref name=WoS>{{cite book |year=2015 |chapter=British Journal of Dermatology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |series=[[Web of Science]] |postscript=.}}</ref>

It was edited from 1896 to 1904 by [[James Galloway (physician)|Sir James Galloway]].

== References ==
{{Reflist}}

== External links ==
* {{Official|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1365-2133}}
* [http://www.bad.org.uk British Association of Dermatologists]

[[Category:Publications established in 1888]]
[[Category:English-language journals]]
[[Category:Monthly journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Dermatology journals]]
[[Category:Health in the London Borough of Camden]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]