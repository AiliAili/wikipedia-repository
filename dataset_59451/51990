{{DISPLAYTITLE:''Journal of Obstetric, Gynecologic, & Neonatal Nursing''}}
{{Infobox Journal
| title = Journal of Obstetric, Gynecologic, & Neonatal Nursing
| cover = [[File:JOGNN Cover.gif]]
| abbreviation = J. Obstet. Gynecol. Neonatal Nurs.
| editor = Nancy K. Lowe, Marilyn Stringer
| discipline = [[Nursing]], [[Obstetrics and gynaecology|Obstetrics and gynecology]], [[Women's health]], and [[Neonatology]]
| publisher = [[Wiley-Blackwell]] on behalf of the [[Association of Women's Health, Obstetric and Neonatal Nurses]]
| country =
| frequency = Bimonthly
| history = 1972-present
| openaccess =
| license =
| impact = 0.952
| impact-year = 2009
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6909
| link1 = 
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 11738525
| LCCN = 85644507
| CODEN = JOGNEY
| ISSN = 0884-2175
| eISSN = 1552-6909
}}
The '''''Journal of Obstetric, Gynecologic, & Neonatal Nursing''''' is a [[Peer review|peer-reviewed]] [[healthcare journal|nursing journal]] in the fields of [[obstetrical nursing]], [[women's health nursing]], and [[neonatal nursing]]. It is the official publication of the [[Association of Women's Health, Obstetric and Neonatal Nurses]].

== Abstracting and indexing ==
The journal is covered by the following abstracting and indexing services: [[CAB Direct (database)|CAB Health/CABDirect]], [[CINAHL]], [[Current Contents]]/Clinical Medicine, Current Contents/Social & Behavioral Sciences, [[Index Medicus]]/[[MEDLINE]], [[PubMed]], [[ProQuest]], [[PsycINFO]], [[Science Citation Index|Science Citation Index Expanded]], [[Scopus]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', the ''Journal of Obstetric, Gynecologic, & Neonatal Nursing'' has a 2009 [[impact factor]] of 0.952, ranking it 31 out of 70 journals in the category "Nursing (Social Science)", 34 out of 72 in "Nursing (Science)", and 52 out of 70 in "Obstetrics & Gynecology".<ref>{{cite web |url=http://www.wiley.com/bw/journal.asp?ref=0884-2175&site=1 |title=Journal of Obstetric, Gynecologic, &amp;Neonatal Nursing: Clinical Scholarship for the Care of Women, Childbearing Families, &Newborns - Journal Information |format= |work=Wiley Online Library |accessdate=2010-11-06}}</ref>

== References ==
{{reflist}}

{{Portal|Nursing}}

== External links ==
* {{Official website|1=http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1552-6909}}
* [http://www.awhonn.org/awhonn/index.do Association of Women's Health, Obstetric and Neonatal Nurses]

{{DEFAULTSORT:Journal of Obstetric, Gynecologic, and Neonatal Nursing}}
[[Category:Obstetrical nursing journals]]
[[Category:Pediatric nursing journals]]
[[Category:English-language journals]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Publications established in 1972]]
[[Category:Bimonthly journals]]
[[Category:Women's health nursing journals]]