{{Use British English|date=August 2016}}
{{Use dmy dates|date=April 2016}}
{{Infobox film
| name = I, Daniel Blake
| image = I, Daniel Blake.png
| alt = 
| caption = British release poster
| director = [[Ken Loach]]
| producer = Rebecca O'Brien
| writer = [[Paul Laverty]]
| starring = {{Plainlist|
* [[Dave Johns]]
* [[Hayley Squires]]
* Dylan McKiernan
* Briana Shann
}} 
| music = [[George Fenton]]
| cinematography = [[Robbie Ryan (cinematographer)|Robbie Ryan]]
| editing = Jonathan Morris
| production companies = {{Plainlist|
* Sixteen Films
* [[Why Not Productions]]
* [[Wild Bunch (company)|Wild Bunch]]
}} 
| distributor    = {{Plainlist|
* [[eOne Films]] {{small|(UK)}}
* Le Pacte {{small|(France)}}
}} 
| released = {{Film date|2016|05|13|[[2016 Cannes Film Festival|Cannes]]|2016|10|21|United Kingdom|df=yes}}
| runtime = 100 minutes<!--Theatrical runtime: 100:19--><ref>{{cite web|url=http://www.bbfc.co.uk/releases/i-daniel-blake-film|title=''I, Daniel Blake'' (15)|work=[[British Board of Film Classification]]|date=18 August 2016|accessdate=19 August 2016}}</ref>
| country = {{Plainlist|
* United Kingdom
* France
* Germany
* Belgium
}} 
| language = English
| budget = 
| gross = $12.45 million<ref>{{cite web|url=http://www.boxofficemojo.com/movies/?page=intl&id=i,danielblake.htm|title=I, Daniel Blake |work=[[Box Office Mojo]]}}</ref> 
}}
'''''I, Daniel Blake''''' is a 2016 British-French-German [[drama film]] directed by [[Ken Loach]] and written by Loach's frequent collaborator [[Paul Laverty]]. It stars [[Dave Johns]] as Daniel Blake, who is denied [[Employment and Support Allowance|employment and support allowance]] despite his doctor finding him unfit to work. [[Hayley Squires]] co-stars as Katie, a struggling [[Single parent|single mother]] whom Daniel befriends.

The film won the [[Palme d'Or]] at the [[2016 Cannes Film Festival]] and the [[Prix du public]] at the 2016 [[Locarno International Film Festival]].<ref>{{cite web|url=https://www.theguardian.com/film/live/2016/may/22/cannes-2016-palme-dor-winners-live|title=Cannes 2016: Ken Loach's I, Daniel Blake wins the Palme d'Or - live!|last1=Lee|first1=Benjamin|website=[[The Guardian]]|date=22 May 2016|access-date=22 May 2016}}</ref><ref name="Deadline2016">{{cite web |url=http://deadline.com/2016/05/cannes-film-festival-winners-2016-palme-dor-ful-list-1201760956/ |title=Cannes Film Festival Winners: Palme d'Or To Ken Loach's 'I, Daniel Blake' |accessdate=22 May 2016 |publisher=[[Deadline.com]]}}</ref><ref name=Locarno />

==Plot==
Widower Daniel Blake, a 59-year-old [[joiner]] living in [[Newcastle upon Tyne|Newcastle]], has had a heart attack at work. Though his [[Cardiology|cardiologist]] has advised him not to return to work, Daniel is deemed fit to do so after a [[Work Capability Assessment|work capability assessment]] and denied [[Employment and Support Allowance|employment and support allowance]]. He is frustrated to learn that his doctor was not contacted about the decision, and applies for an appeal, a process he finds difficult because he is not [[Computer literacy|computer literate]].

Daniel befriends a [[Single parent|single mother]], Katie, after she is sanctioned for arriving late for a [[Jobcentre Plus|jobcentre]] appointment. Katie and her children have moved to Newcastle from a London [[Homeless shelter|homeless persons' hostel]], with no affordable accommodation available in the capital. Daniel helps the family by repairing objects, teaching them how to heat rooms without electricity, and crafting wooden toys for the children.

During a visit to a [[food bank]], Katie is overcome by hunger and breaks down. After she is caught shoplifting, a security guard offers her work as an [[Escort agency|escort]]. Daniel surprises her at the brothel, where he begs her to give up the job, but she tearfully insists she has no choice to feed her children.

As a condition for receiving [[Jobseekers Allowance|jobseeker's allowance]], Daniel searches for work on an industrial estate. He is offered a job in a scrapyard, but has to turn it down for health reasons. When Daniel's work coach tells him he must work harder to find a job, Daniel spraypaints "I, Daniel Blake, demand my appeal date before I starve" on the building. He earns the support of passersby, including other benefits claimants, but is arrested and given a police warning. Daniel sells most of his belongings and becomes withdrawn.

On the day of Daniel's appeal, Katie accompanies him to court. A welfare adviser tells Daniel that his case looks sound. On glimpsing the judge and doctor who will decide his case, Daniel becomes anxious and visits the lavatory, where he suffers a fatal heart attack. At his "[[Public health funeral|pauper's funeral]]", Katie reads the [[eulogy]], including a speech Daniel had intended to read at his appeal. Daniel's speech describes his feelings about how the [[Welfare state in the United Kingdom|welfare system]] failed him, treating him like a number and not a man who was proud to say he had paid his dues to society.

==Cast==
* [[Dave Johns]] as Daniel Blake
* [[Hayley Squires]] as Katie Morgan
* Dylan McKiernan as Dylan Morgan
* Briana Shann as Daisy Morgan
* Mick Laffey as Welfare Benefits Advisor
* Harriet Ghost as Appeal Receptionist
* Helen Dixon as Police Officer
* Bryn Jones as Police Officer
* Laura Jane Barnes-Martin as Call Centre Advisor
* Kema Sikazwe as China
* Steven Richens as Piper
* [[Gavin Webster]] as Joe

==Production==
Principal photography began in October 2015 in [[Newcastle upon Tyne]] and the surrounding area.<ref>{{cite news|last1=Hodgson|first1=Barbara|title=Award-winning director Ken Loach takes to the streets of Newcastle to shoot his latest feature film|url=http://www.chroniclelive.co.uk/news/north-east-news/award-winning-director-ken-loach-10407775|work=[[Evening Chronicle]]|date=8 November 2015|accessdate=25 May 2016}}</ref> The film was produced by [[Rebecca O'Brien]]<ref>{{Cite web|url=http://www.sixteenfilms.co.uk/people/profile/2/rebecca_o_brien/|title=People :: Rebecca O'Brien|last=Solutions|first=Starlight|website=www.sixteenfilms.co.uk|access-date=26 May 2016}}</ref> for Sixteen Films, [[Why Not Productions]] and [[Wild Bunch (company)|Wild Bunch]] with the support of the [[British Film Institute]] and [[BBC Films]].<ref>{{cite web|last1=Gleiberman|first1=Owen|title=Cannes Film Review: ‘I, Daniel Blake’|url=http://variety.com/2016/film/reviews/i-daniel-blake-review-ken-loach-1201772584/|publisher=[[Variety (magazine)|Variety]]|accessdate=25 May 2016|date=12 May 2016}}</ref>

==Reception==

===Critical response===
On [[review aggregator]] website [[Rotten Tomatoes]], the film has an approval rating of 93%, based on 109 reviews, with an average rating of 7.9/10.<ref name="I, Daniel Blake">{{cite web|url=https://www.rottentomatoes.com/m/i_daniel_blake|title=I, Daniel Blake|publisher=Rotten Tomatoes|accessdate=2 February 2017}}</ref> The site's consensus reads: "''I, Daniel Blake'' marks yet another well-told chapter in director Ken Loach's powerfully populist filmography."<ref name="I, Daniel Blake"/> On [[Metacritic]] the film has a score of 78 out of 100 score, based on 23 critics, indicating "generally favourable reviews".<ref>{{cite web | url=http://www.metacritic.com/movie/i-daniel-blake|title=I, Daniel Blake reviews|work=[[Metacritic]]| publisher=[[CBS Interactive]]|accessdate=2 February 2017}}</ref>

===Box office===
''I, Daniel Blake'' became Ken Loach's biggest success at the UK box office, especially as the film sparked debate in the country.<ref>{{cite web|url=http://www.thefilmagazine.com/i-daniel-blake-is-ken-loachs-most-successful-uk-release-ever/|title=‘I, Daniel Blake’ Is Ken Loach’s Most Successful UK Release Ever|date=24 October 2016|publisher=}}</ref><ref>{{cite web|url=https://www.theguardian.com/film/2016/oct/25/i-daniel-blake-ken-loach-uk-box-office-trolls-top-spot|title=I, Daniel Blake scores impressive result at UK box office as Trolls takes top spot|first=Charles|last=Gant|date=25 October 2016|publisher=|via=The Guardian}}</ref>

===Political response===
There has been a wide variety of both praise and criticism of the film from politicians. Former Work and Pensions Secretary [[Iain Duncan Smith]] referred to the film as unfair, aiming particular criticism at its portrayal of [[Jobcentre Plus|Jobcentre]] staff, saying: "This idea that everybody is out to crunch you, I think it has really hurt Jobcentre staff who don’t see themselves as that."<ref>{{cite web|url=http://www.independent.co.uk/news/uk/politics/i-daniel-blake-iain-duncan-smith-ken-loach-response-criticism-benefits-sanctions-film-unfair-a7384306.html|title=Iain Duncan Smith's predictable response after watching I, Daniel Blake|date=28 October 2016|publisher=}}</ref> Producer Rebecca O'Brien responded by stating that Duncan Smith "is living in cloud cuckoo land."<ref>{{cite web|url=https://inews.co.uk/essentials/news/i-daniel-blake-far-scathing-says-producer/|title=I, Daniel Blake producer responds to Toby Young, Iain Duncan Smith|date=28 October 2016|publisher=}}</ref>

Similarly, on an episode of BBC's topical debate programme ''[[Question Time (TV series)|Question Time]]'' broadcast on 27 October 2016, which featured Ken Loach as a panellist, Business Secretary [[Greg Clark]] described the film as "a fictional film",<ref name="telegraph.co.uk">{{cite web|url=http://www.telegraph.co.uk/films/2016/10/28/ken-loach-and-minister-greg-clark-clash-over-fictional-i-daniel/|title=Ken Loach and minister Greg Clark clash over 'fictional' I, Daniel Blake on Question Time|publisher=}}</ref> saying "It’s a difficult job administering a benefits system...[[Department of Work and Pensions]] staff have to make incredibly difficult decisions and I think they should have our support in making those decisions."<ref name="telegraph.co.uk"/> Loach responded to this by criticising the pressure that DWP staff are placed under.<ref name="telegraph.co.uk"/>

Conversely, [[Labour Party (UK)|Labour]] leader [[Jeremy Corbyn]] appeared at the film's London premiere with the director, then subsequently praised the film on his Facebook page.<ref>{{cite web|url=http://www.chroniclelive.co.uk/whats-on/film-news/jeremy-corbyn-tells-film-fans-12049745|title=Jeremy Corbyn urges film fans to go see Tyneside-set I, Daniel Blake|first=Mike|last=Kelly|date=19 October 2016|publisher=}}</ref> In a session of [[Prime Minister's Questions]] on 2 November 2016, he advised [[Prime Minister of the United Kingdom|Prime Minister]] [[Theresa May]] to watch the film, as he criticised the fairness of the welfare system.<ref>{{cite web|url=http://www.bbc.co.uk/news/uk-politics-37851425|title=PMQs: Corbyn tells May to watch I Daniel Blake film|date=2 November 2016|publisher=|via=www.bbc.co.uk}}</ref>

===Accolades===
{| class="wikitable sortable" 
|- style="background:#ccc; text-align:center;"
! colspan="6" style="background: LightSteelBlue;" | List of awards and nominations
|- style="background:#ccc; text-align:center;"
! Award
! Date of ceremony
! Category
! Recipient(s)
! Result
! class="unsortable"| {{nowrap|{{Abbr|Ref(s)|References}}}}
|-
| rowspan="5"| [[British Academy Film Awards]]
| rowspan="5"| [[70th British Academy Film Awards|12 February 2017]]
| [[BAFTA Award for Best Film|Best Film]]
| Rebecca O'Brien
| {{nom}}
| rowspan="5" style="text-align:center;"| <ref>{{Cite web|url=http://www.hollywoodreporter.com/lists/bafta-2017-complete-list-nominations-962769/item/film-bafta-nominees-2017-962744 |title=BAFTA Awards: 'La La Land' Leads Nominations |first=Alex |last=Ritman |date=9 January 2017 |publisher=''[[The Hollywood Reporter]]'' |accessdate=10 January 2017}}</ref><br /><ref>{{cite web|url=http://deadline.com/2017/02/bafta-awards-winners-list-1201909076/|title=BAFTA Awards Winners List (LIVE)|website=[[Deadline.com]]|first=Patrick|last=Hipes|date=12 February 2017|accessdate=12 February 2017}}</ref>
|-
| [[BAFTA Award for Best Actress in a Supporting Role|Best Actress in a Supporting Role]]
| [[Hayley Squires]]
| {{nom}}
|-
| [[BAFTA Award for Best Direction|Best Direction]]
| [[Ken Loach]]
| {{nom}}
|-
| [[BAFTA Award for Best Original Screenplay|Best Original Screenplay]]
| [[Paul Laverty]]
| {{nom}}
|-
| [[BAFTA Award for Best British Film|Best British Film]]
| Paul Laverty, Ken Loach and Rebecca O'Brien
| {{won}}
|-
| rowspan="7"| [[British Independent Film Awards]]
| rowspan="7"| [[British Independent Film Awards 2016|4 December 2016]]
| [[BIFA Award for Best British Independent Film|Best British Independent Film]]
| ''I, Daniel Blake''
| {{nom}}
| rowspan="7" style="text-align:center;"| <ref>{{cite web|url=https://www.theguardian.com/film/2016/nov/01/i-daniel-blake-leads-british-independent-film-award-nominations-bifa-ken-loach |title=I, Daniel Blake leads British independent film award nominations |last=Evans |first=Alan |work=[[The Guardian]] |date=1 November 2016 |accessdate=1 November 2016}}</ref>
|-
| Best Director
| Ken Loach
| {{nom}}
|-
| [[BIFA Award for Best Performance by an Actor in a British Independent Film|Best Actor]]
| [[Dave Johns]]
| {{won}}
|-
| [[BIFA Award for Best Performance by an Actress in a British Independent Film|Best Actress]]
| Hayley Squires
| {{nom}}
|-
| rowspan="2"| Most Promising Newcomer
| Dave Johns
| {{nom}}
|-
| Hayley Squires
| {{won}}
|-
| Best Screenplay
| Paul Laverty
| {{nom}}
|-
| rowspan="2"| [[Cannes Film Festival]]
| rowspan="2"| [[2016 Cannes Film Festival|22 May 2016]]
| [[Palme d'Or]]
| Ken Loach
| {{won}}
| rowspan="2" style="text-align:center;"| <ref name="CFF2016">{{cite web|url=https://www.theguardian.com/film/live/2016/may/22/cannes-2016-palme-dor-winners-live|title=Cannes 2016|accessdate=22 May 2016|work=The Guardian}}</ref>
|-
| [[Palm Dog Award|Palm DogManitarian Award]]
| Ken Loach <small>(showcasing a three-legged dog named Shea)</small>
| {{won}}
|-
| [[César Awards]]
| [[42nd César Awards|24 February 2017]]
| [[César Award for Best Foreign Film|Best Foreign Film]]
| Ken Loach
| {{won}}
| style="text-align:center;"| <ref>{{cite web |url=http://www.academie-cinema.org/ceremonie/palmares.html|title=PALMARÈS 2017 - 42 ÈME CÉRÉMONIE DES CÉSAR |work=[[Académie des Arts et Techniques du Cinéma]] |accessdate=24 February 2017}}</ref>
|-
| [[Denver Film Festival]]
| 14 November 2016
| Special Jury Prize: Best Actress
| Hayley Squires
| {{won}}
| style="text-align:center;"| <ref>{{Cite news|url=http://denverfilmfestival.denverfilm.org/media/press-releases/denver-film-society-announces-winners-39th-denver-film-festival/|title=AWARD WINNERS FOR 39th DENVER FILM FESTIVAL - Denver Film Festival|newspaper=Denver Film Festival|language=en-US|access-date=5 December 2016}}</ref>
|-
| rowspan="4"| [[Empire Awards]]
| rowspan="4"| [[22nd Empire Awards|19 March 2017]]
| [[Empire Award for Best British Film|Best British Film]]
| ''I, Daniel Blake''
| {{won}}
| rowspan="4" style="text-align:center;"| <ref>{{Cite web|url=http://www.empireonline.com/movies/news/three-empire-awards-2017-rogue-one-tom-hiddleston-patrick-stewart-win-big/ |title=Three Empire Awards 2017: Rogue One, Tom Hiddleston And Patrick Stewart Win Big |last=Nugent |first=John |work=[[Empire (magazine)|Empire]]|date=19 March 2017|access-date=19 March 2017}}</ref>
|-
| [[Empire Award for Best Director|Best Director]]
| Ken Loach
| {{nom}}
|-
| [[Empire Award for Best Male Newcomer|Best Male Newcomer]]
| Dave Johns
| {{won}}
|-
| [[Empire Award for Best Female Newcomer|Best Female Newcomer]]
| Hayley Squires
| {{nom}}
|-
| rowspan="4"| [[European Film Awards]]
| rowspan="4"| [[29th European Film Awards|10 December 2016]]
| [[European Film Award for Best Film|Best Film]]
| ''I, Daniel Blake''
| {{nom}}
| rowspan="4" style="text-align:center;"| <ref>{{cite web|url=https://www.theguardian.com/film/2016/nov/07/toni-erdmann-maren-ade-leads-nominations-at-european-film-awards |title=Toni Erdmann leads nominations at European film awards |first=Alan |last=Evans |publisher=''[[The Guardian]]'' |date=7 November 2016 |accessdate=11 December 2016}}</ref>
|-
| [[European Film Award for Best Director|Best Director]]
| Ken Loach
| {{nom}}
|-
| [[European Film Award for Best Actor|Best Actor]]
| Dave Johns
| {{nom}}
|-
| [[European Film Award for Best Screenwriter|Best Screenwriter]]
| Paul Laverty
| {{nom}}
|-
| rowspan="5"| [[Evening Standard British Film Awards]]
| rowspan="5"| 8 December 2016
| Best Film
| ''I, Daniel Blake''
| {{won}}
| rowspan="5" style="text-align:center;"| <ref>{{cite web|url=http://www.standard.co.uk/goingout/film/evening-standard-british-film-awards-the-longlist-a3398101.html |title=Evening Standard British Film Awards - The Longlist |publisher=''[[London Evening Standard]]''|first=William|last=Moore|date=17 November 2016|accessdate=29 November 2016}}</ref><br /><ref>{{cite web|url=http://www.standard.co.uk/goingout/film/evening-standard-british-film-awards-the-winners-a3415651.html |title=Evening Standard British Film Awards: Kate Beckinsale and Hugh Grant Crowned |publisher=''[[London Evening Standard]]''|first=Ben |last=Norum |date=9 December 2016 |accessdate=9 December 2016}}</ref>
|-
| Best Actor
| Dave Johns
| {{nom}}
|-
| Best Supporting Actress
| Hayley Squires
| {{won}}
|-
| Best Screenplay
| Paul Laverty
| {{nom}}
|-
| Most Powerful Scene Award
| ''I, Daniel Blake''
| {{won}}
|-
| [[Rotten Tomatoes|Golden Tomato Awards]]
| 12 January 2017
| Best British Movie 2016
| ''I, Daniel Blake''
| {{draw|3rd Place}}
| style="text-align:center;"| <ref>{{cite web|title=Golden Tomato Awards - Best of 2016|url=https://editorial.rottentomatoes.com/rt-hub/golden-tomato-awards-best-of-2016/|publisher=[[Rotten Tomatoes]]|date=12 January 2017}}</ref>
|-
| [[Locarno International Film Festival]]
| 13 August 2016
| [[Prix du public]]
| Ken Loach
| {{won}}
| style="text-align:center;"| <ref name="Locarno">{{cite web |url=http://www.pardolive.ch/pardo/pardo-live/today-at-festival/2016/day-11/loc-69-palmares/palmares-2016.html|title=Palmarès 2016|work=Locarno}}</ref>
|-
| rowspan="4"| [[London Film Critics' Circle]]
| rowspan="4"| [[London Film Critics Circle Awards 2016|22 January 2017]]
| [[London Film Critics' Circle Award for Film of the Year|Film of the Year]]
| ''I, Daniel Blake''
| {{nom}}
| rowspan="4" style="text-align:center;"| <ref>{{cite web|url=http://variety.com/2016/film/awards/moonlight-love-and-friendship-lead-london-film-critics-circle-nominations-1201945718/|title=‘Moonlight’ and ‘Love and Friendship’ Lead London Film Critics’ Circle Nominations|date=20 December 2016|publisher=''Variety''|accessdate=20 December 2016}}</ref>
|-
| [[London Film Critics' Circle Award for British Film of the Year|British/Irish Film of the Year]]
| ''I, Daniel Blake''
| {{won}}
|-
| [[London Film Critics' Circle Award for British Actor of the Year|British/Irish Actor of the Year]]
| Dave Johns
| {{nom}}
|-
| [[London Film Critics' Circle Award for British Actress of the Year|British/Irish Actress of the Year]]
| Hayley Squires
| {{nom}}
|-
| [[New York Film Critics Online]]
| 11 December 2016
| Top 12 Films
| ''I, Daniel Blake''
| {{won}}
| style="text-align:center;"| <ref name="NYFCO">{{cite web|url=http://www.hollywoodreporter.com/news/moonlight-named-best-picture-by-new-york-film-critics-online-association-955062 |title='Moonlight' Named Best Picture by New York Film Critics Online Association |publisher=''[[The Hollywood Reporter]]''|date=11 December 2016 |accessdate=11 December 2016}}</ref>
|-
| [[San Sebastián International Film Festival]]
| 24 September 2016
| Audience Award: Best Film
| Ken Loach
| {{won}}
| style="text-align:center;"| <ref>{{cite web|url=http://www.screendaily.com/festivals/san-sebastian/san-sebastian-i-daniel-blake-fire-at-sea-in-pearls-line-up/5108580.article|title=San Sebastian: 'I, Daniel Blake', 'Fire At Sea' in Pearls line-up|first=Michael|last=Rosser |publisher=''[[Screen International|Screen Daily]]''|date=19 August 2016|accessdate=11 December 2016}}</ref>
|-
| [[Stockholm International Film Festival]]
| 20 November 2016
| Audience Award: Best Film
| Ken Loach
| {{won}}
| style="text-align:center;"| <ref>{{cite web|url=http://www.vimooz.com/2016/11/21/i-daniel-blake-stockholm-audience-award/|title=Ken Loach’s I, DANIEL BLAKE Wins Stockholm Audience Award at Stockholm Film Fest |first=Angela |last=Ramsay|publisher=''Vimooz.com''|accessdate=11 December 2016}}</ref>
|-
| [[Vancouver International Film Festival]] 
| 14 October 2016
| Most Popular International Feature
| Ken Loach
| {{won}}
| style="text-align:center;"| <ref>{{cite press release |url=http://archive.viff.org/e-blasts/viff/2016/PR/19_VIFFAudienceAward_PR.html |title=Maudie Wins Coveted VIFF Super Channel People's Choice Award |date=14 October 2016 |accessdate=18 October 2016 |publisher=Greater Vancouver International Film Festival Society}}</ref>
|}

==References==
{{Reflist|30em}}

==External links==
* {{IMDb title|5168192}}

{{Ken Loach}}
{{Navboxes|title=Awards for ''I, Daniel Blake''|list=
{{César Award for Best Foreign Film}}
{{BAFTA Best British Film}}
{{London Film Critics Circle Award for British Film of the Year}}
{{Palme d'Or}}
}}

{{DEFAULTSORT:I Daniel Blake}}
[[Category:2016 films]]
[[Category:2010s drama films]]
[[Category:British drama films]]
[[Category:British films]]
[[Category:French drama films]]
[[Category:French films]]
[[Category:Films directed by Ken Loach]]
[[Category:Palme d'Or winners]]
[[Category:Welfare in England]]
[[Category:Entertainment One films]]
[[Category:IFC Films films]]
[[Category:Best British Film BAFTA Award winners]]