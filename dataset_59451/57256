
{{Infobox journal
| title = Histochemistry and Cell Biology
| cover = [[File:Histochemistry and Cell Biology.jpg]]
| formernames = Histochemie, Histochemistry
| editor = Detlev Drenckhahn, Jurgen Roth
| discipline = [[Cell biology]], [[histochemistry]]
| abbreviation = Histochem. Cell Biol.
| publisher = [[Springer Science+Business Media]] on behalf of the [[Society for Histochemistry]]
| country = 
| frequency = Quarterly
| history = 1958-present
| openaccess = 
| impact = 2.78
| impact-year = 2015
| website = http://www.springer.com/journal/418/
| link1 = http://link.springer.com/journal/418/138/6/page/1
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 32074894
| LCCN = 95648296
| CODEN = HCBIFP
| ISSN = 0948-6143
| eISSN = 1432-119X
}}
'''''Histochemistry and Cell Biology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] in the field of molecular [[histology]] and [[cell biology]], publishing original articles dealing with the localization and identification of molecular components, metabolic activities, and cell biological aspects of [[Cell (biology)|cells]] and [[Tissue (biology)|tissues]]. The journal covers the development, application, and evaluation of methods and probes that can be used in the entire area of [[histochemistry]] and cell biology.<ref>{{cite web |url=http://www.springer.com/medicine/anatomie/journal/418?detailsPage=description |title=About the journal |publisher=[[Springer Science+Business Media]] |work=Histochemistry and Cell Biology |accessdate=2012-12-22}}</ref> The journal is published by [[Springer Science+Business Media]] and the official journal of the [[Society for Histochemistry]].<ref>{{cite web |url=http://www.histochemistry.eu/ |title=Society for Histochemistry |publisher=Histochemistry.eu |date= |accessdate=2012-12-22}}</ref> Earlier names of the journal are ''Histochemie'' and ''Histochemistry''. The [[editors-in-chief]] are Detlev Drenckhahn ([[University of Würzburg]]) and Jurgen Roth ([[University of Zurich]]).<ref>{{cite web |url=http://www.springer.com/medicine/anatomie/journal/418?detailsPage=editorialBoard |title=Editorial Board |publisher=[[Springer Science+Business Media]] |work=Histochemistry and Cell Biology |accessdate=2012-12-22}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.springer.com/journal/418/}}

[[Category:English-language journals]]
[[Category:Molecular and cellular biology journals]]
[[Category:Publications established in 1958]]
[[Category:Histochemistry]]
[[Category:Immunohistochemistry]]
[[Category:Quarterly journals]]


{{biochem-journal-stub}}