{{Infobox journal
| title = Proceedings of the Institution of Mechanical Engineers, Part F: Journal of Rail and Rapid Transit
| cover = [[Image:Proceedings of the IMechE - F - journal cover.jpg]]
| editors = Simon S D Iwnicki
| discipline = [[Transportation engineering]]
| peer-reviewed = 
| former_names = 
| abbreviation = Proc. Inst. Mech. Eng. F J. Rail Rapid Transit
| publisher = [[Sage Publications]]
| country =
| frequency = 8/year
| history = 1989-present
| openaccess = 
| license = 
| impact = 0.743
| impact-year = 2013
| website = http://www.uk.sagepub.com/journals/Journal202020
| link1 = http://pif.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pif.sagepub.com/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 19786779
| LCCN = 89643806
| CODEN = 
| ISSN = 0954-4097
| eISSN = 2041-3017
}}
The '''''Proceedings of the Institution of Mechanical Engineers, Part F: Journal of Rail and Rapid Transit''''' is a [[peer-reviewed]] [[scientific journal]] that covers on [[Transportation engineering|engineering applicable to rail]] and [[rapid transit]]. The journal was first published in 1989 as a split-off of'' [[Proceedings of the Institution of Mechanical Engineers]]''. It is published by [[Sage Publications]] on behalf of the [[Institution of Mechanical Engineers]].<ref>[http://www.imeche.org/knowledge/publications Institution of Mechanical Engineers publications page]. IMechE. Retrieved 25 May 2014. </ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=index>
{{Cite web
  | title = Abstracting/Indexing 
  | work = 
  | publisher = Sage Publications  
  | date = June 2014
  | url = http://www.uk.sagepub.com/journals/Journal202020#tabview=abstractIndexing 
  | format = Online web page format}}</ref>

*[[Applied Science & Technology Abstracts]]
*[[Applied Science & Technology Index]]
*[[Civil Engineering Abstracts]]
*[[Current Contents]]/Engineering, Computer, & Technology
*[[Engineered Materials Abstracts]]
*[[SAE International]]/[[Global Mobility Database]] <ref>[http://store.sae.org/gmd/ Global Mobility Database] on the World Wide Web. SAE International. 2014.</ref>
*[[Inspec]]
*[[Mechanical Engineering Abstracts]]
*[[Metals Abstracts]]/[[METADEX]]
*[[Science Citation Index]]
According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 0.743, ranking it 76th out of 124 journals in the category "Engineering, Civil",<ref name=WoS1>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Civil |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> 76th out of 126 journals in the category "Engineering, Mechanical",<ref name=WoS2>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Engineering, Mechanical |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> and 20th out of 30 journals in the category "Transportation Science and Technology".<ref name=WoS3>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Transportation Science and Technology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.uk.sagepub.com/journals/Journal202020}}
{{Institution of Mechanical Engineers}}
[[Category:Bimonthly journals]]
[[Category:Engineering journals]]
[[Category:English-language journals]]
[[Category:Institution of Mechanical Engineers academic journals]]
[[Category:Publications established in 1989]]
[[Category:SAGE Publications academic journals]]