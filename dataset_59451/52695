{{Infobox journal
| title = Public Works Management and Policy 
| cover = [[File:Public Works Management & Policy Journal Front Cover.jpg]]
| editor = Richard G. Little
| discipline = [[Public Administration]]
| former_names = 
| abbreviation = 
| publisher = [[SAGE Publications]]
| country = 
| frequency = Quarterly
| history = 1996-present
| openaccess = 
| license = 
| impact = 
| impact-year = 2010
| website = http://www.uk.sagepub.com/journals/Journal200885?siteId=sage-uk&prodTypes=any&q=Public+Works+Management+%26+Policy&fs=1
| link1 = http://pwm.sagepub.com/content/current
| link1-name = Online access
| link2 = http://pwm.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1087-724X
| eISSN = 
| OCLC = 535496227
| LCCN = 96652944
}}

'''''Public Works Management and Policy''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers four times a year in the field of [[Public Administration]]. The journal's [[Editor-in-Chief|editor]] is Richard G. Little ([[University of Southern California]]). It has been in publication since 1996 and is currently published by [[SAGE Publications]] in association with the Section on Transportation Policy and Administration of the [[Administration of the American Society for Public Administration]].

== Scope ==
''Public Works Management and Policy'' is a resource for academics and practitioners in public works and the public and private infrastructure industries. The journal publishes research results, evaluative management innovations, methods of analysis and evaluation and policy issues. ''Public Works Management and Policy'' aims to address the planning, financing, development and operations of civil infrastructure systems.

== Abstracting and indexing ==
''Public Works Management and Policy'' is abstracted and indexed in the following databases:
:* [[Business Source Complete]]
:* [[Business Source Elite]]
:* [[Business Source Premier]]

== External links ==
* {{Official website|1=http://pwm.sagepub.com/}}

{{DEFAULTSORT:Public Works Management and Policy}}
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Public administration]]