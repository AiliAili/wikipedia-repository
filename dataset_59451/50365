{{About|the scientific journal|the concept of addiction|Addiction}}
{{Infobox journal
| title = Addiction
| editors = Robert West
| discipline = [[Addiction]]
| abbreviation = Addiction
| publisher = [[Wiley-Blackwell]] on behalf of the [[Society for the Study of Addiction]]
| history = 1884-present
| frequency = Monthly
| openaccess = Hybrid
| impact = 4.145
| impact-year = 2010
| website = http://www.addictionjournal.org
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1360-0443/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1360-0443/issues
| link2-name = Online archive
| ISSN = 0965-2140
| eISSN = 1360-0443
| CODEN = ADICE5
| LCCN = 93645978
| OCLC = 27367194
}}
'''''Addiction''''' is a monthly [[peer review|peer-reviewed]] [[scientific journal]] established in 1884 by the [[Society for the Study of Addiction to Alcohol and other Drugs]]. It covers [[original research]] relating to [[alcohol abuse|alcohol]], [[substance abuse|illicit drug]]s, [[tobacco]], and [[behavioural addiction]]s.<ref>{{cite web|url=http://www.addictionjournal.org/aims.asp|title=Aims & scope|accessdate=2008-02-27| archiveurl= https://web.archive.org/web/20080311060028/http://www.addictionjournal.org/aims.asp| archivedate= 11 March 2008 <!--DASHBot-->| deadurl= no}}</ref>

== Impact ==
According to the ''[[Journal Citation Reports]]'', the journal has a 2010 [[impact factor]] of 4.145, ranking it second out of 14 journals in the category "Substance Abuse"<ref name=WoS1>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Substance Abuse |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> and 24th out of 126 journals in the category of "Psychiatry".<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Psychiatry |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref> It is also ranked 1st out of 29 journals in the Social Sciences category "Substance Abuse"<ref name=WoS3>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Substance Abuse|title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref> and 12th out of 107 journals in the Social Sciences category "Psychiatry".<ref name=WoS4>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Psychiatry |title=2010 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.addictionjournal.org/}}

[[Category:English-language journals]]
[[Category:Publications established in 1884]]
[[Category:Addiction medicine journals]]
[[Category:Monthly journals]]
[[Category:Wiley-Blackwell academic journals]]