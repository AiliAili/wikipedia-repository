{{Infobox journal
| title = Arteriosclerosis, Thrombosis, and Vascular Biology
| formernames = Arteriosclerosis; Arteriosclerosis and Thrombosis
| cover = [[File:Arteriosclerosis, Thrombosis, and Vascular Biology Cover.gif]]
| editor = [[Alan Daugherty]]
| discipline = [[Cardiology]], [[cardiovascular|cardiovascular biology]]
| abbreviation = Arterioscler. Thromb. Vasc. Biol.
| publisher = [[Lippincott Williams & Wilkins]] on behalf of the [[American Heart Association]]
| country = United States
| frequency = Monthly
| history = 1981-present
| openaccess =
| license = 
| impact = 6.0
| impact-year = 2014
| website = http://atvb.ahajournals.org
| link1 = http://atvb.ahajournals.org/content/current
| link1-name = Online access
| link2 = http://atvb.ahajournals.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 31441121
| LCCN = 95644718
| CODEN = ATVBFA
| ISSN = 1079-5642
| eISSN = 1524-4636
}}
'''''Arteriosclerosis, Thrombosis, and Vascular Biology''''' is a [[Peer review|peer-reviewed]] [[medical journal]] published by [[Lippincott Williams & Wilkins]] on behalf of the [[American Heart Association]]. It covers [[basic research|basic]] and [[clinical research]] related to [[circulatory system|vascular biology]], [[pathophysiology]] and [[complication (medicine)|complications]] of [[atherosclerosis]], and [[thrombosis|thrombotic]] mechanisms in [[blood vessel]]s.

The journal was established in 1981 as ''Arteriosclerosis'' ({{ISSN|0276-5047}}), which was published bimonthly. From 1991 to 1994 it was published monthly under the title ''Arteriosclerosis and Thrombosis: A Journal of Vascular Biology'' ({{ISSN|1049-8834}}).<ref>[[National Library of Medicine]] online catalog. Accessed 2009-08-31.</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 6.0, ranking it fifth out of 68 journals in the category "Hematology"<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Hematology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |accessdate= |work=Web of Science |postscript=.}}</ref> and fourth out of 60 in the category "Peripheral Vascular Disease".<ref name=WoS1>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Peripheral Vascular Disease |title=2014 Journal Citation Reports |publisher=Thomson Reuters |edition=Science |accessdate= |work=Web of Science |postscript=.}}</ref>

== See also ==
* ''[[Hypertension (journal)|Hypertension]]''

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://atvb.ahajournals.org}}

[[Category:Cardiology journals]]
[[Category:Lippincott Williams & Wilkins academic journals]]
[[Category:Monthly journals]]
[[Category:Publications established in 1981]]
[[Category:American Heart Association academic journals]]