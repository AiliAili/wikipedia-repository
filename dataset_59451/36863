{{good article}}
{{Use mdy dates|date=May 2014}}
{{Infobox park
| name =  Audubon Sharon
| photo = SharonCT Audubon Center.tiff
| photo_width =
| photo_caption = View of the Sharon Audubon Center fields, Bog Meadow Pond and the Taconic Mountain range.
| photo_alt =
| type = [[Nature center]] and [[wildlife sanctuary]]
| location = 325 Cornwall Bridge Road <br />[[Sharon, Connecticut|Sharon]], [[Connecticut|CT]], [[United States|USA]]
| area = {{convert|2,600|acre|ha|abbr=on}}+
| created = 1961<ref name="official">{{cite web|url=http://sharon.audubon.org/history-2|title=Sharon Audubon Center - History|accessdate=May 2, 2014}}</ref>
| designer =
| operator =
| hiking_trails = {{convert|11|mi|abbr=on}} of trails
| other_info =
| website = {{URL|http://sharon.audubon.org}}
| coords= {{coord|41.857|N|73.455|W|region:US-CT|format=dms|display=inline,title}}
}}
'''Audubon Sharon''', which consists of the '''Sharon Audubon Center''' and the '''Emily Winthrop Miles Wildlife Sanctuary''', is a wildlife sanctuary of the [[National Audubon Society]] in [[Sharon, Connecticut]]. The {{convert|1147|acre}} of the Sharon Audubon Center property is primarily forest land with two ponds with {{convert|11|mile}} of trails for visitors to use. Its facilities include a raptor [[aviary]], an herb garden, a garden to attract bird and butterflies, a [[sugarhouse]], a memorial room to [[Hal Borland]], a small museum and store.<ref name="center">{{cite web | url=http://sharon.audubon.org/sharon-audubon-center | title=Sharon Audubon Center | publisher=National Audubon Society, Inc | accessdate=May 1, 2014}}</ref> Sharon Audubon Center is located at 325 Cornwall Bridge Road.

The other part of Audubon Sharon is the Emily Winthrop Miles Wildlife Sanctuary, which currently encompasses {{convert|1500|acre}} of land that is situated in {{convert|5000|acre}} of protected open space. The residential facility within the wildlife sanctuary is used by interns and scientists who are conducting work in the area; none of the buildings are currently open to the public. Parking and access is available at 99 West Cornwall Road.

Audubon Sharon offers [[environmental education]] program for school groups. The Center also has summer and weekend environmental programs for adults and children.

== Sharon Audubon Center ==
Prior to the creation of Audubon Sharon, the land was owned by Clement and Keyo Ford who lived on a property known as Bog Meadow Farm.<ref name="history2">{{cite web | url=http://sharon.audubon.org/history-2 | title=History | publisher=National Audubon Society, Inc | accessdate=May 1, 2014}}</ref> In 1961, the Fords donated the estate to the [[National Audubon Society]] to serve as an educational nature center for future generations.<ref name="history2" /> 

The main building features the [[Hal Borland]] Room, a memorial to the nature writer whose work first appeared in ''[[The New York Times]]'' in 1941. Some of Borland's essays were collected and published as ''Sundial of the Seasons'' in 1964. The room includes photos, his books and typewriter.<ref name="carol">{{cite book | title=Natural Wonders of Connecticut and Rhode Island - A Guide to Parks, Preserves and Wild Places | publisher=Country Roads Press | author=Henshaw, Carol | year=1995 | pages=2–7 | isbn=1566260795}}</ref>

===Trails===
The Sharon Audubon Center has a collection of trails available for visitors to walk, including the wheelchair accessible Lucy Harvey Multiple Use Interpretative area, totaling {{convert|11|mile}}. Hal Borland is also honored with a {{convert|0.75|mile|km}} trail that begins near the "native wildflower garden and continues through brushland and deciduous forest to a streamside hemlock forest." The native wildflower garden includes [[Virginia bluebell]]s, [[Aquilegia]], and white violets. Another trail, the Fern Trail, is a narrow and rocky {{convert|1|mi|km}} woodland trail that follows the northern shore of Ford Pond. Over 70 species of birds have been recorded on the trail and there are many varieties of ferns to be seen. The Ford Trail is a {{convert|1.6|mile|adj=on}} trail through the deciduous and hemlock forest. The Hazelnut Trail is a {{convert|1|mile|adj=on}} loop trail. The Woodchuck Trail is a {{convert|2.35|mile|adj=on}} trail through open fields and the deciduous forest. The Hendrickson Bog Meadow Trail is a {{convert|1.6|mile|adj=on}} loop trail through the deciduous forest and along Bog Meadow Pond's shore.<ref name=carol />

==Emily Winthrop Miles Wildlife Sanctuary==
The Emily Winthrop Miles Wildlife Sanctuary was originally property owned by Emily Winthrop Miles, a poet, writer and artist,<ref name="history2" /> who acquired {{convert|740|acre}} of land in [[Sharon, Connecticut]]. In 1962, as part of her will, Miles donated the property to the National Audubon Society.<ref name="miles2">{{cite book | url=https://books.google.com/books?id=KJEQAwAAQBAJ&pg=PA68&lpg=PA68&dq=Emily+Winthrop+Miles+Wildlife+Sanctuary&source=bl&ots=FumQWcxAHW&sig=yMYIInAp5tgqyH2yBOsEUPeqz1A&hl=en&sa=X&ei=lvpiU6b4CKSayQGA-IG4Bw&ved=0CC8Q6AEwAQ#v=onepage&q=Emily%20Winthrop%20Miles%20Wildlife%20Sanctuary&f=false | title=Wildlife Sanctuaries and the Audubon Society: Places to Hide and Seek | publisher=University of Texas Press | author=Anderson, John | year=2010}}</ref> The property now includes 1,500 acres of land that is situated amidst 5,000 acres of protected open space.<ref name="history2" /> The wildlife sanctuary includes forested land and two miles of Carse Brook Wetlands, home to endangered flora and fauna species.<ref>{{cite web | url=http://sharon.audubon.org/miles-wildlife-sanctuary | title=Miles Wildlife Sanctuary | publisher=National Audubon Society, Inc | accessdate=May 1, 2014}}</ref> 

== See also ==
* [[List of nature centers in Connecticut]]

== References ==
{{reflist}}

==External links==
* [http://sharon.audubon.org/ Sharon Audubon Center] 

{{Nature centers in Connecticut}}
{{Protected Areas of Connecticut}}

[[Category:National Audubon Society]]
[[Category:Nature centers in Connecticut]]
[[Category:Protected areas of Litchfield County, Connecticut]]
[[Category:Sharon, Connecticut]]