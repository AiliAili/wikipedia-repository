{{EngvarB|date=May 2015}}
{{Use dmy dates|date=May 2015}}
{{Good article}}
{{Infobox sportsperson
| headercolor = green
| textcolor = yellow
| name = Luke Cain
| image = File:XXXX15 - Luke Cain - 3b - 2016 Team processing.jpg
| image_size = <!--Only for images narrower than 220 pixels. !-->
| caption = 2016 Australian Paralympic Team Portrait
| birth_name = 
| fullname = 
| nickname = 
| nationality = Australia
| residence =  
| birth_date =  {{birth date and age|df=yes|1980|2|3}}
| birth_place =  
| death_date = 
| death_place = 
| height =  <!--  {{convert| |cm}} (2012) !-->
| weight =  <!-- {{convert|  |kg}} (2012) !-->
| website = <!-- {{URL|www.example.com}} !-->
| country = Australia
| sport = Shooting
| event =  10m air rifle prone<br/>10m air rifle standing
| collegeteam = 
| club = Springvale Range Club
| team = 
| turnedpro = 
| coach = 
| retired = 
| coaching = 
| worlds = 
| regionals = 
| nationals = 
| olympics = 
| paralympics = 
| highestranking = 
| pb = 
|medaltemplates= 
}}

'''Luke Cain''' (born 3 February 1980) is an [[SH2 (classification)|SH2-classified]] Australian shooter who became a [[paraplegic]] after an accident while playing [[Australian rules football]]. He started competing in 2007, as the sport suited his disability, and has been a Victorian Institute of Sport scholarship holder since 2008. He first represented Australia internationally in 2009 at a [[2009 ISSF World Cup|World Cup]] event in [[South Korea]]. He has also represented Australia in two Paralympic Games including the [[2016 Summer Paralympics|2016 Rio Paralympics]].<ref name="apcnews">{{cite web|title=Six Australian shooters to target Paralympic gold in Rio|url=https://www.paralympic.org.au/six-australian-shooters-to-target-paralympic-gold-in-rio/|website=Australian Paralympic Committee News|date=17 May 2016|accessdate=17 May 2016}}</ref>

==Personal==
Cain was born on 3 February 1980 in [[Rosebud, Victoria]].<ref name=apc/><ref name=london2012site/><ref name=bigpoind>{{cite web|url=http://www.bigpondsport.com/olympics/athletebio/tabid/712/athleteid/294/default.aspx |title=Paralympics Athlete Bio |publisher=BigPond Sport |date= |accessdate=16 September 2012}}</ref> He started playing [[Australian rules football]] when he was seven years old for the [[Rye Football Club]].  He played senior football for [[Rosebud Football Club]] as a [[full-forward]].<ref name=whenyoung/> In August 1999, at the age of nineteen, he was playing for Rosebud in a game against [[Hastings Football Club]] when he broke his neck after being sandwiched between a teammate and an opposing player.<ref name=whenyoung/><ref name=needsmillions/><ref name=supportcloke/><ref name=2010oceania/><ref name=thatogttahurt>{{cite web|url=http://www.osabrands.com/expedition/page.php/1/10/55 |title=Luke Cain |publisher=Osa Brands |date= |accessdate=16 September 2012}}</ref> He is a [[paraplegic]],<ref name=whenyoung/> and requires use of a wheelchair because of paralysis that affects him from the chest down.  He has limited use of his fingers and no use of his hands.<ref name=supportcloke/><ref name=2010oceania/> Before his accident, he participated in a range of sports, including basketball, athletics, [[waterskiing]], [[Kneeboarding (towsport)|kneeboarding]] and [[wakeboarding]].<ref name=whenyoung/><ref name=thatogttahurt/> After his accident, he continued in some sports, including [[angling]].<ref name=london2012site>{{cite web|url=http://www.london2012.com/paralympics/athlete/cain-luke-5514607/|accessdate=16 September 2012|publisher=London 2012 Paralympics|title=Luke Cain}}</ref>  {{As of|2012}}, he resides in [[Boneo, Victoria]].<ref name=london2012site/><ref name=bigpoind/>

His cousin is [[Travis Cloke]], an [[Australian Football League|AFL]] [[All-Australian]] full-forward.<ref name=needsmillions>{{cite news|url=http://www.heraldsun.com.au/news/victoria/luke-guns-for-gold-trav-aims-at-flag/story-fn7x8me2-1226142153396 |title=Luke guns for gold, Trav aims at flag |publisher=Herald Sun |location=Melbourne, Victoria |date= |accessdate=25 July 2012}}</ref> Cain has been an inspiration to his cousin on the football field.<ref name=needsmillions/> At the same time, Cloke has supported Cain.<ref name=supportcloke>{{cite web|last=Langmaid |first=Aaron |url=http://www.couriermail.com.au/sport/afl/travis-wont-choke-in-afl-grand-final-replay-says-cloke-family/story-e6frepf6-1225930284677 |title=Travis won't choke in AFL Grand Final replay says Cloke family |publisher=The Courier-Mail |date=28 September 2010 |accessdate=25 July 2012}}</ref> Other cousins include AFL players [[Jason Cloke]] and [[Cameron Cloke]].<ref name=london2012site/>

==Shooting==
[[File:010912 - Luke Cain - 3b - 2012 Summer Paralympics (02).jpg|thumb|Cain shooting at the 2012 London Paralympics|left|300x300px]]
Cain is an [[SH2 (classification)|SH2-classified]] shooter competing in 10m air rifle prone and 10m air rifle standing events.<ref name=apc>{{cite web|url=http://www.paralympic.org.au/team/luke-cain |title=Luke Cain |publisher=Australian Paralympic Committee |location=Australia |year=2012 |accessdate=13 July 2012}}</ref> He has been a [[Victorian Institute of Sport]] scholarship holder since 2008,<ref name=needsmillions/> and is a member of the [[Springvale Range Club]].<ref name=apc/> He is coached by [[Miro Sipek]] as an individual and when on the national team.<ref name=london2012site/>

As a youngster, Cain hunted with his father and cousin.<ref name=whenyoung>{{cite web|url=http://www.lukecainshooting.com/Luke_Cain/About_Me.html |title=A bit about me |publisher=Luke Cain |date= |accessdate=16 September 2012}}</ref> He took up the sport of shooting because it was one of the few available to people with his physical limitations.<ref name=2010oceania/>  {{As of|2012}}, he was sponsored by Miall's Gun Shop of [[Frankston, Victoria]], who provided him with competition gear including cleaning supplies, a rifle case and a rifle.  He holds two world records, one in the individual 600–600 R4 prone event, and another in the R4 10-metre standing event.<ref name=2010oceania/><ref name=sposnors>{{cite web|url=http://www.lukecainshooting.com/Luke_Cain/My_Sponsors.html |title=My Sponsors |publisher=Luke Cain |date= |accessdate=16 September 2012}}</ref>

Cain started competing in 2007, and made the Australian national team the same year.<ref name=apc/><ref name=finsihefirst/> During Australia's 2009 domestic series, the Australia Cup, an invitation-only series for the top shooters in the country,<ref>{{cite web |url=http://www.ausshooting.org/calendar/aisl-competitions.html |accessdate=25 November 2012 |title=AISL Competitions |publisher=Australian International Shooting Limited }}</ref> he earned a bronze, silver and gold medal.<ref name=apc/> That year, he made his national team debut at an international event when he represented Australia at the [[2009 ISSF World Cup]] in South Korea.<ref name=thatogttahurt/> By early 2010, he was looking for a sponsor to assist with costs for his international competitions.<ref name=finsihefirst/> He set a personal best of 598 out of 600 in the SH-2 prone air event at the 2010 World Cup in Germany, and not long after, set a new personal best when he shot 599 at the same event at the 2010 World Cup in France.<ref name=thatogttahurt/><ref name=lukecainresults/> He came in first at the 2010 Oceania Shooting Federation Continental Championship.<ref name=2010oceania>{{cite web|last=Morris |first=Deborah |url=http://mornington-peninsula-leader.whereilive.com.au/sport/story/olympic-gold-in-his-sight-for-boneo-star/ |title=Olympic gold in his sight for Boneo star |publisher=Mornington Peninsula Leader |date= |accessdate=25 July 2012}}</ref><ref name=finsihefirst>{{cite web|last=McEvoy |first=Simon |url=http://mornington-peninsula-leader.whereilive.com.au/sport/story/on-target-for-world-title/ |title=CAN YOU HELP? On target for world title |publisher=Mornington Peninsula Leader |date=27 January 2010 |accessdate=16 September 2012}}</ref> At the 2010 IPC World Shooting Championships in [[Zagreb]],<ref>{{cite web |url=http://www.hpo.hr/ipc_shooting_wc_2010/ |accessdate=25 November 2012 |title=Welcome to the 2010 IPC Shooting World Championships, Zagreb, Croatia |publisher=Croatian Paralympic Committee}}</ref> he came in first in the SH-2 prone air event by setting a world record of 600 points, a perfect score.<ref name=2010oceania/><ref name=lukecainresults/> At the 2011 Great Britain International, he finished second in the 10m air rifle prone event.<ref name=apc/><ref name=greatbritain>{{cite web|url=http://www.weeklytimesnow.com.au/article/2011/09/20/385141_shooting.html |title=Shooting Luke Cain wins world shooting medal |publisher=Weekly Times Now |date=20 September 2011 |accessdate=16 September 2012}}</ref> He competed in the United States-hosted International Paralympic Committee World Cup in 2011,<ref name=needsmillions/> finishing fourth in the R5 Air Rifle Prone event with a score of 599. He earned a gold medal in the team event with teammates [[Jason Maroney]] and [[Bradley Mark]].<ref name=finishusa>{{cite web|url=http://www.ausshooting.org/component/content/article/437-australia-shines-at-the-ipc-world-cup-fort-benning.html |title=Australia shines at the IPC World Cup, Fort Benning |publisher=Ausshooting.org |date=19 November 2011 |accessdate=16 September 2012}}</ref> In 2012, he trained up to six days a week.<ref name=2010oceania/> At the Australian nationals, he finished second in the prone event behind New Zealander Michael Johnson, and third in the standing event behind fellow Australian [[Bradley Marks]] and Johnson.<ref name=michaeljohnson>{{cite web|url=http://www.tra.org.au/nats2012/nats2012.html |title=2012 NATIONALS |publisher=Tra.org.au |date= |accessdate=16 September 2012}}</ref>

Cain was selected to represent [[Australia at the 2012 Summer Paralympics|Australia]] at the [[2012 Summer Paralympics]] in Mixed R5-10m Air Rifle Prone-SH2 and Mixed R4-10m Air Rifle Standing-SH2 shooting events.<ref name=apc/><ref name=london2012site/><ref name=2010oceania/><ref>{{cite web|title=APC|url=https://www.paralympic.org.au/athlete/luke-cain/|website=APC|accessdate=22 September 2016}}</ref><ref name=qualified>{{cite web|author=Niav Owens |url=http://www.abc.net.au/news/2012-07-23/kosmala-shoots-for-her-11th-games/4124320?section=sport |title=Kosmala shoots for her 11th Games – |publisher=ABC Grandstand Sport&nbsp;– ABC News (Australian Broadcasting Corporation) |date= |accessdate=25 July 2012 |location=Australia}}</ref> Competing on 3 September,<ref name=daycompete>{{cite web|url=http://www.abc.net.au/tv/guide/abc1/201209/programs/SP1105H016D2012-09-02T040000.htm |title=Paralympics London 2012 – Day Three – 4:00&nbsp;am Sunday, September 02 2012 |publisher=ABC Television |date=2 September 2012 |accessdate=16 September 2012}}</ref> he did not medal, finishing 27th in the standing event and 28th in the prone event.<ref name=london2012site/><ref name=27thfinish>{{cite web|url=http://www.sportingshootermag.com.au/news/so-close-aussies-edged-out-at-paralympics |title=So Close: Aussies edged out at the Paralympics |publisher=Sport Shooting Magazine |date= |accessdate=16 September 2012}}</ref>

Most recently Cain had represented Australia in the [[2016 Summer Paralympics|2016 Rio Paralympics]]. He competed in the Mixed 10 m air rifle standing SH2 and the Mixed 10 m air rifle prone SH2.<ref>{{cite web|title=Shooting: Day three preview|url=https://www.paralympic.org/news/shooting-day-three-preview|website=IPC Shooting|accessdate=22 September 2016}}</ref><ref>{{cite web|title=Rio Results|url=https://www.rio2016.com/en/paralympics/athlete/luke-cain|website=Rio 2016|accessdate=22 September 2016}}</ref> Cain did not medal, placing 26th in the standing event and 33rd in the prone event.

===Performance===
{| class="wikitable sortable"
|+
|-
!Event
!Competition
!Date
!Score
!Finish
!Notes
!Reference
|-
|Prone Air&nbsp;– SH2
|[[2012 Summer Paralympics]]
|Sept-12
|Shot 592
|27th
|
|<ref name=london2012site/><ref name=27thfinish/>
|-
|Prone Air&nbsp;– SH2
|International Paralympic Committee World Cup
|2011
|Shot 599+105.6
|4th
|
|<ref name=finishusa/>
|-
|Prone Air&nbsp;– SH2
|World Cup GB
|Oct-10
|Shot 598
|4th
|
|<ref name=lukecainresults>{{cite web|url=http://www.lukecainshooting.com/Luke_Cain/Achievements.html |title=Achievements |publisher=Luke Cain |date= |accessdate=16 September 2012}}</ref><ref name=resultsvis>{{cite web|url=http://www.vis.org.au/item/luke-caion.html |title=Luke Cain |publisher=Victorian Institute of Sport (VIS) |date=2 March 1980 |accessdate=16 September 2012}}</ref>
|-
|Prone Air&nbsp;– SH2
|World Championships Croatia
|Jul-10
|Shot 600
|9th
|New personal best
|<ref name=bigpoind/><ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|Aus Cup
|Jul-10
|Shot 599
|2nd
|
|<ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|AISL GP
|Jul-10
|Shot 597
|3rd
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|World Cup France
|May-10
|Shot 599
|9th
|New personal best
|<ref name=thatogttahurt/><ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|World Cup Germany
|May-10
|Shot 598
|10th
|New personal best
|<ref name=thatogttahurt/><ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|Australia Cup
|May-10
|Shot 596
|1st
|
|<ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|Nationals
|Mar-10
|Shot 593
|2nd
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Australia Cup
|Feb-10
|Shot 597
|2nd
|
|<ref name=lukecainresults/><ref name=resultsvis/>
|-
|Prone Air&nbsp;– SH2
|Oceania
|Dec-09
|Shot 597
|1st
|
|<ref name=finsihefirst/><ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Aus Cup Final
|Oct-09
|Shot 597
|3rd
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Aus Cup
|Sep-09
|Shot 597
|1st
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Arafura
|May-09
|Shot 595
|3rd
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Korea Cup
|Apr-09
|Shot 596
|11th
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Nationals
|Apr-09
|Shot 597
|3rd
|
|<ref name=lukecainresults/>
|-
|Prone Air&nbsp;– SH2
|Aus Cup
|Mar-09
|Shot 591
|3rd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|[[2012 Summer Paralympics]]
|Sept-12
|Shot 586
|28th
|
|<ref name=london2012site/><ref name=27thfinish/>
|-
|Standing Air&nbsp;– SH2
|International Paralympic Committee World Cup
|2011
|Shot 596+103.1
|6th
|
|<ref name=finishusa/>
|-
|Standing Air&nbsp;– SH2
|AISL GP
|Jul-10
|Shot 593
|3rd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|World Cup France
|May-10
|Shot 593
|11th
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|World Cup Germany
|May-10
|Shot 592
|13th
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Aus Cup
|May-10
|Shot 586
|1st
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Nationals
|Mar-10
|Shot 588
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Aus Cup
|Feb-10
|Shot 590
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Oceania
|Dec-09
|Shot 597
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Aus Cup Final
|Oct-09
|Shot 592
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Aus Cup
|Sep-09
|Shot 594
|1st
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Arafura
|May-09
|Shot 587
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Korea Cup
|Apr-09
|Shot 587
|10th
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Nationals
|Apr-09
|Shot 592
|2nd
|
|<ref name=lukecainresults/>
|-
|Standing Air&nbsp;– SH2
|Aus Cup
|Mar-09
|Shot 592
|2nd
|
|<ref name=lukecainresults/>
|}

==References==
{{reflist|30em}}

==External links==
{{Portal|Australia|Biography|Paralympics}}
*[https://www.paralympic.org.au/athlete/luke-cain/ Australian Paralympic Committee Profile]
*[https://www.paralympic.org/asp/redirect/ipc.asp?page=athletebio&personid=927706&sportid=540 International Paralympic Committee Profile and Results]
* {{Twitter|lukecain600}}
* {{Facebook|pages/Luke-Cain/199153910149641}}

{{2016 Australian Paralympic Team}}
{{2012 Australian Paralympic Team}}

{{DEFAULTSORT:Cain, Luke}}
[[Category:Living people]]
[[Category:1980 births]]
[[Category:Paralympic shooters of Australia]]
[[Category:Shooters at the 2012 Summer Paralympics]]
[[Category:Shooters at the 2016 Summer Paralympics]]
[[Category:Wheelchair category Paralympic competitors]]
[[Category:Victorian Institute of Sport alumni]]