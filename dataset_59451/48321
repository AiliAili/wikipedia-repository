'''Erwin Anton Gutkind''' (May 20, 1886, [[Berlin]] – 7 August 1968, [[Philadelphia]]), was a German-Jewish architect and city planner, who left Berlin in 1935 for Paris, London and then Philadelphia, where he became a member of the faculty of the University of Pennsylvania.   Of his work in Germany, all but one building remains and now, in the year 2013 most if not all have historical protection orders on them. Some have been restored too.

== Biography ==

Gutkind was born in Berlin on May 20, 1886. He studied from 1905 to 1909 in the  [[Technische Universität Berlin|Technischen Hochschule (Berlin-) Charlottenburg]] and the [[Humboldt-Universität zu Berlin]].<ref>Rudolf Hierl: ''Erwin Gutkind 1886-1968. Architektur als Stadtraumkunst.'' Birkhäuser, Basel / Boston / Berlin 1992, ISBN 3-76432689-1, S. 14.</ref> In 1910 he married Margarete Jaffé, with whom he had two children. In 1914 he was awarded the degree of Doktor-Ingenieur (Dr.-Ing.) by the  Technischen Hochschule Charlottenburg for his thesis ''Raum und Materie''.

In 1933 Gutkind left Berlin for Paris. He then moved to London in 1935, and finally in 1956 to Philadelphia, where he became a member of the faculty of the School of Fine Arts, University of Pennsylvania.  That year he married his partner — the Sinologist [[Anneliese Bulling]] — as his first wife, from whom he had become estranged, died during [[World War II]] in Germany.

== Reputation ==

Gutkind and his contemporaries were commonly referred to as the “circle of friends of the Bauhaus” or “children of the 1880s” as they followed their mentors and the “Fathers” of modern architecture ( or Neues Bauen ) of Peter Behrens and Walter Gropius.

He was a Siedlung architect, post-war reconstructionist, urban planner, historian of urbanization and writer. His buildings were “bold combinations of stucco and exposed brick, with large windows and strikingly individual corners”.<ref>''The Many Faces of Modern Architecture:Building in Germany between the World Wars.''  Edited by John Zukowsky. Munich, New York, Prestel-Verlag, 1994.</ref> He was a ‘Bauhausian’ architect incorporating their philosophy of light, air and sun. Like many others,

The Siedlung, essentially workers (or social) housing though the word means community, was about providing accommodation for working people in a harmonious, attractive and a well provided for shelter and environment. They included gardens, kindergartens, shopping facilities, laundry areas and playgrounds. They were, in part, a response to Berlin’s shortage of housing, as the city grew and grew, developing from garden cities and the harshness of tenements. The building of the German Siedlungen reached its peak between 1926 and 1930.

Gutkind and his contemporary architects participated in the building of the Siedlungen and also in the architectural discussion groups that were an important part of these times. "The Ring of Ten," for example, included [[Hans Poelzig]], [[Eric Mendelsohn]], [[Ludwig Hilberseimer]], the Taut brothers, [[Otto Bartning]], [[Martin Wagner (architect)|Martin Wagner]], [[Walter Gropius]], Erwin Gutkind and they met at [[Mies van der Rohe]]’s office. In 1931, The Ring was denounced by the National Socialists .

Gutkind was one of many architects who engaged in the debates of the time: Bruno Taut, a brilliant architect of the same time (the utopian expressionist) versus Gutkind (the rationalist architect). He was accused by Taut of not being interested in the individual house as a construction of architecture and did not obey a strictly Bauhaus line. Gutkind responded by saying that it was wrong to have as the beginning point the cell of an individual home, but that the whole settlement and its place in the city, was also or more important. (Paraphrasing the words of the late Julius Posener, from an article in the archives of the Akademie der Kunste, Berlin.)

== Awards ==

In 1968, he was awarded the ‘Berliner Kunstpreises für Baukunst’ (Berlin art prize for building) from the City of Berlin. the prize was first awarded inn 1948, the previous two recipients to Gutkind were Mies van der Rohe and Wassili Luckhardt.

== Influence ==

The Italian architectural historian Piergiacomo Bucciarelli has even suggested that [[Bruno Taut]]’s work was influenced by Gutkind’s. There are many examples dotted throughout Berlin; for example , comparet Taut’s work on the Trierer Strasse in Weissensee and Gutkind’s work in Reinickendorf.

Contemporary German architect, Klaus Meier-Hartmann wrote: “the inspiration for this new building [are] the 1920s apartment blocks of Erwin Gutkind. Gutkind’s trademark was his treatment and handling of the corner entrance; the play of brickwork and render; and an emphasis on the horizontal. [ Hartmann ] has reinterpreted these elements and used them in his work. The result is a building that has a clear relationship with the adjoining area and its history and that satisfies the requirements of today’s social housing programme.”<ref>''Berlin. A Guide to Recent Architecture.'' Published by Könemann Verlagsgesellschaft  mbH and Ellipses London Limited, 1997, pages 36 & 37</ref>
Professor Peirogiacomo Bucciarelli wrote “Erwin Anton Gutkind represents a difficult case in the history of contemporary architecture. He lived and worked in Berlin during the “heroic period” of modern architecture in Europe, but today he is practically unknown as an architect, in spite of a certain interest on the part of some recent architectural critics and historians in Germany and Italy. He is certainly most famous as a town planning theorist and urban historian, especially after he emigrated to the U.S.A. during the fifties. It was not until 1968, a few months before his death at the age of eighty-two, that the late German architectural historian Julius Posener dedicated an article to him in the review ''Bauwelt,'' to mark the prize the city of Berlin had just awarded him.”

“Erwin Gutkind belongs to the circle of those most representative German architects of the twenties. His short professional career as an architect, only ten years, includes almost 1,500 residential units, exhibition stands, town planning projects and public building projects. For the “architectural tourist” visiting Berlin, Gutkind’s buildings are still beautiful and intelligent, unlike those built by some of his most famous colleagues, which today may appear outdated or too ambitious, with their strictly rationalist and radically pure architectural language.”

“He devoted more attention to the architectural details, the correct use of materials (horizontal bands alternatively of bricks and plaster are typical of his formal language), and he made an original and diversified use of the corners of his housing blocks. His work reveals close attention to the perceptive and environmental qualities of the modern metropolis. The many solutions of his architectural corners establish mutual relationships between the different fronts of buildings on the other side of the street. The corners of Gutkind’s apartment blocks play a double role: they coordinate and conclude the façades and at the same time communicate monumental values.”

“Seventy years after the challenge of the Modern Movement, everybody can experience how both the “Heimatstil” and the “Neues Bauen” avant garde, the audacity of the “Expressionists” and the objectivity of the “Neue Sachlichkeit” no longer have a place in the German capital. The most renowned models of these movements face each other on the same streets of the city and participate in the same silence. Each of them shares the same tragic memories. All of these buildings, objects of ferocious clashes between minds and bodies, survived the last war, showing above all their architectural being. No ideological critique, however sharp, would prevent Gutkind’s architecture from standing. In spite of everything, they are a beautiful testimony to ideas and techniques as well as useful places to live.”

== Major works ==

* Berlin-Pankow, Wohnblock, Thulestrasse, 1927, a block of flats, close to Berlin’s largest Jewish cemetery.  The building is enclosed, powerful and strong. Its main vertical corner, triangular, of layered glass block and concrete, acts as a beacon — thrusting out and up. Its small windows at the top of the building are Tautian in style. It lacks the intimacy and softness of some of his other work. At the beginning of 1999, this block of flats was bought by a private investor who has restored it. As this building is listed, there are strict regulations so that it will not be changed or spoiled. Christoph Freudenberg was the architect responsible for the restoration: “My employees have made a detailed check-up upon the existing substance of the building. The whole ensemble is in pretty bad shape, since there has been little maintenance of the building during the past forty years. Nevertheless the substance of the main structure shows little damage, so I am hopeful that we are going to be able to repair and renew the building without too much expense. Our restoration efforts will be concentrated upon those parts of the building which were not destroyed in World War Two — including the very expressively designed staircase.”
*Gutkind was the primary architect for the Berlin building firm, Gruppe Nord, still in working existence in 1999 after seventy years. They are the caretakers of two thousand apartments in this Siedlung, nine hundred and fifty of which were designed by Gutkind. In the cellar of Gruppe Nord’s offices are many original drawings by Gutkind.

== Published books ==

* ''Raum und Materie. Ein baugeschichtlicher Darstellungsversuch der Raumentwicklung.'' Berlin 1915.
* ''[[Neues Bauen]]. Grundlagen zur praktischen Siedlungstätigkeit'', Verlag der Bauwelt, Berlin 1919.
* ''Berliner Wohnbauten der letzten Jahre.'' Berlin 1931. (with J. Schallenberger)
* ''Creative demobilisation.'' London 1943.
* ''Revolution of environment.'' London 1946.
* ''Our world from the air. An international survey of man and his environment.'' London 1952.
* ''Community and environment. A discourse on social ecology.'' London 1953.
* ''The expanding environment. The end of cities, the rise of communities.'' London 1953.
* ''The twilight of cities.'' New York 1962.
* ''International History of city development.'' (8 vols.) New York / London 1964-1968.

== References ==

{{Reflist}}

== External links ==

* {{DNB-Portal|119014572}}
* {{archINFORM|arch|3059}}

Some portions of this article are translated from the

{{Authority control}}

{{DEFAULTSORT:Gutkind, Erwin Anton}}
[[Category:Year of death missing]]
[[Category:1886 births]]
[[Category:20th-century German architects]]
[[Category:German Jews]]
[[Category:Humboldt University of Berlin alumni]]
[[Category:Architects from Berlin]]
[[Category:Historians of urban planning]]