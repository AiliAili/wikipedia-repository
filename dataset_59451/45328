[[Image:DuTemplePhotograph.jpg|thumb|200px|'''Félix du Temple de la Croix''' (1823&ndash;1890).]]
'''Félix du Temple de la Croix''' (18 July 1823 &ndash; 4 November 1890) (usually simply called '''Félix du Temple''') was a [[France|French]] naval officer and an inventor, born into an ancient [[Normandy]] family. He developed some of the [[Early flying machines|first flying machines]] and is credited with the first successful flight of a powered aircraft of any sort, a powered model plane, in 1857,<ref name=first-to-fly>"Felix Du Temple and his brother Louis, France, fly a model monoplane whose propellers are driven by a small steam engine. It takes off under its own power, flies a short distance, and glides to a safe landing. It is the first successful flight of a powered aircraft of any sort." [http://www.first-to-fly.com/History/History%20of%20Airplane/powering.htm]</ref> and is sometimes credited with the first manned powered flight in history onboard his [[Du Temple Monoplane|Monoplane]] in 1874,
<ref>Some quotes on the first powered flight in history, achieved by Félix du Temple:
*"The first takeoff of a powered, fixed-wing aircraft with a man aboard took place in 1874 at Brest, France. The steam-engine-powered plane, designed by a French naval officer, Félix du Temple, rose a few feet when it was launched down a hill." [http://oglobo.globo.com/online/blogs/arquivos/2003-12-09_09h28_Santos_Dumont_reconhecido_-_pagina(1).htm The New York Times, 2003]
*"The Wrights were not even the first to leave the ground in a powered plane. That honor apparently went to a French sailor whose name has been lost to history. In 1874, Félix du Temple, a French naval officer, watched the steam-powered plane he devised speed down a ski-jump-like ramp and sputter through the air with the guileless young sailor at the helm." Paul Hoffman, author of Wings of Madness, writing in [http://www.historynewsnetwork.com/articles/866.html#wright12-17-03 The New York Times Dec. 17, 2003 article]
*"In 1874, French naval officer Félix Du Temple successfully launched a bat-like plane with a steam engine. Rising a few feet off the ground, it was the first launch of a manned, powered fixed-wing aircraft." in [http://wright.ccc.edu/FirstFlight/ahistory.asp Wright College]
*"Felix Du Temple built a steam-powered monoplane that managed to rise a few feet off the ground-the first powered fixed-wing aircraft that carried a passenger, albeit down a slope." [http://www.cr.nps.gov/nr/travel/aviation/ideaflight.htm US National Park Service]
*"First powered flight to make even a brief hop", according to the [http://www.centennialofflight.gov/essay/Prehistory/mid-19th_century/PH3G7.htm U.S. Centennial of flight commission]
*"It was more of a "hop" than a real flight, but some historians give du Temple credit for the first powered flight." in [http://www.ctie.monash.edu.au/hargrave/du_temple.html The pioneers, an anthology] and [http://wings.avkids.com/Book/History/instructor/power-01.html The National Business Aviation Association]
*"It is said the craft managed to make a brief hop a few feet off the ground after being rolled down an inclined slope, gliding a short distance, and landing safely. If this account is true, Félix du Temple did succeed in launching the first manned, powered, fixed-wing, heavier-than-air flight, but the propulsion system was too weak to sustain the flight and the control system was ineffective." in [http://www.aerospaceweb.org/question/history/q0172.shtml Aerospaceweb.org]</ref>
twenty-nine years before the 1903 flight of the [[Wright brothers]].
He was a contemporary of [[Jean-Marie Le Bris]], another French flight pioneer who was active in the same region of France.

==Military life==
[[Image:FelixDuTemplePainting.jpg|thumb|left|150px|Félix du Temple as a young Naval officer.]]
Félix du Temple entered the French Navy Academy ([[École Navale]]) in 1838. He participated in most of the conflicts during the [[Second French Empire]], especially the [[Crimean War]], the French intervention in Italy against Austria, and the [[French intervention in Mexico]].

At the age of 41, he returned to France, became a captain (Capitaine de Frégate), and joined the Loire Army ([[Armée de la Loire]]). A partisan of the [[Comte de Chambord]] and a [[legitimist]] (an "Ultra-Royalist"), he was forced to quit the Navy in 1876.

==Flying machine patent==
[[File:Felix de la Croix du Temple airplane mockup.jpg|thumb|300px|Airplane of Félix du Temple de la Croix, 1857. [[Musée de l'Air et de l'Espace]].]]
[[Image:1857DuTemplePatent.jpg|thumb|300px|1857 [[patent drawing]] of Félix du Temple's flying machine, the "Canot planeur".]]
Félix du Temple accomplished the first successful flight of a powered aircraft of any sort, a model plane that was able to take-off under its own power, in 1857.<ref name=first-to-fly /><ref>{{cite journal |last1=Gibbs-Smith |first1=Charles H. |last2= |first2= |date=3 Apr 1959|year= |title= Hops and Flights: A roll call of early powered take-offs |journal=[[Flight International|Flight]] |volume=75 |issue=2619 |page=468|publisher= |url= http://www.flightglobal.com/pdfarchive/view/1959/1959%20-%200937.html|accessdate=24 Aug 2013}}</ref><ref>"In 1857 or 1858 the Du Temple model took off under its own steam",  The Road to Kitty Hawk - Page 54 by Valerie Moolman</ref> There are however competing claims for the first "assisted" powered flight, with [[John Stringfellow]]'s experiments in 1848.<ref name="books.google.com">[https://books.google.com/books?id=zrpb67qFXUIC&pg=PT63&dq=1857+Temple+flight&lr=&sig=ACfU3U0s2bWJVVIn6IEYDXat5SKBPh93UA ''First to Fly'' By Thomas C Parramore, p.46]</ref>

Félix du Temple patented the designs for his aerial machine in 1857,<ref name="books.google.com"/><ref>[https://books.google.com/books?id=IYnl_XPggZYC&pg=PA41&dq=1857+Temple+flight&sig=ACfU3U2YJQELcAf5YCFvjxl3Zd6hdF-c2g ''Inventing Flight'' By John David Anderson p.41]</ref> which incorporated a retractable wheel landing gear, a propeller, a 6&nbsp;hp engine and a [[dihedral (aircraft)|dihedral]] wing design, under the title "Locomotion aérienne par imitation du vol des oiseaux" ("Aerial locomotion by imitation of the flight of birds"). He built several large models together with his brother Luis. One of them, weighing 700 grams, was able to fly, first using a clockwork mechanism as an engine, and then using a miniature [[steam engine]]. The two brothers managed to make the models take off under their own power, fly a short distance and land safely.{{citation needed|date=February 2012}}

As they tried to build a machine capable of carrying a man, they realized that steam engines lacked power and were too heavy.  In 1867 they designed an original "[[stirling engine|hot air]]" engine, which did not prove satisfactory. They also experimented with the new [[Internal combustion engine|internal combustion gas engine]] design developed by [[Etienne Lenoir|Lenoir]], but this also lacked the necessary power.

[[File:Felix de la Croix du Temple circa 1870.jpg|thumb|left|Felix du Temple de la Croix du Temple circa 1870.]]
Du Temple continued his research and finally succeeded in creating a very compact high-speed circulation steam engine, for which he applied for a patent on 28 April 1876. The engine used very small pipes packed together "to obtain the highest possible contact surface for the smallest possible volume" <ref>The patent describes "une demande pour une chaudière à vapeur à circulation rapide donnant la plus grande surface de chauffe possible sous le plus petit volume et le moindre poids."</ref>

:"When he began with the aid of his brother, M. Louis du Temple, to experiment on a large scale, the inadequacy of all motors then known became apparent. They first tried steam at very high pressures, then a hot-air engine, and finally built and patented, in 1876 a very light steam boiler weighing from 39 to 44 lb. to the horse power, which appears to have been the prototype of some of the light boilers which have since been constructed. It consisted in a series of very thin tubes less than 1/8 inches in internal diameter, through which water circulated very rapidly, and was flashed into steam by the surrounding flame." Octave Chanute, Aeroplanes : Part III, August 1892

==The "Monoplane"==
{{main article|Du Temple Monoplane}}
[[Image:1874DuTemple.jpg|thumb|300px|Félix du Temple's 1874 ''[[Monoplane (1874)|Monoplane]]''.]]
In 1874, the two brothers built the ''[[Monoplane (1874)|Monoplane]]'', a large plane made of [[aluminium]] in [[Brest, France|Brest]], [[France]], with a [[wingspan]] of 13 meters and a weight of only 80 kilograms (without the pilot). Several trials were made with the plane, and it is generally recognized that it achieved lift off under its own power after a ski-jump run, glided for a short time and returned safely to the ground, possibly making it the first successful powered flight in history, depending on the definition — since the flight was only a short distance and a short time.<ref>{{cite journal |last1=Gibbs-Smith|first1=Charles H.|date=3 April 1959|title=They Dared First|journal=[[Flight International|Flight]]|volume=102|issue=3308|page=17|publisher=|url= http://www.flightglobal.com/pdfarchive/view/1972/1972%20-%202006.html|accessdate=24 August 2013}}</ref>

The plane was displayed at the 1878 World Fair ([[Exposition Universelle (1878)]]) in Paris.

==High-circulation steam engine==
The plane's engine used a very compact, high-speed circulation steam boiler, the [[Du Temple boiler]], for which Félix du Temple applied for a patent on 28 April 1876. The engine used very small pipes packed together "to obtain the highest possible contact surface for the smallest possible volume".

The original steam engine which had been developed by Félix du Temple was later commercialized by him from a company he established in [[Cherbourg]], "Générateur Du Temple S.A." and became highly successful. The design was adopted by the [[French Navy]] for the propulsion of the first French [[torpedo boat]]s:

: "Officers and engineers have now made up their opinion regarding Du Temple's steam engine. Everybody proclaims the superiority of its qualities… orders are pouring in from our commercial harbours and from the French government."  Revue Maritime 1888 (''L’opinion est faite aujourd’hui sur la chaudière Du Temple parmi les officiers et les ingénieurs. Tout le monde proclame ses qualités supérieures… les commandes affluent de nos ports de commerce et de la part du gouvernement français''.) <ref>[http://www.mairie-tourlaville.fr/fr/tourisme/tourisme_et_patrimoine/dossiers_en_consultation/fichiers/felix_du_temple.pdf) French document]</ref>

==Industrial legacy==
Following his death in 1890, his successors took over the management of the company. "Générateur Du Temple S.A.", acquired the Lesénéchal company in 1905, and by 1918 had several hundred employees when it was absorbed by the shipbuilding company Société Normande de Construction Navale.

== Quote ==
{{quotation|"In general, birds, especially the largest ones, only rise and fly because of an acquired speed: this speed which is necessary to rise is obtained either by running on the ground or on water, or by jumping from a high point.  Once arrived at a certain height that allows him to fly horizontally and move forward with just the flap of the wings, he gains speed, spreads his wings and tail so as to form as flat a surface as possible, and thus moves forward without any visible movement of the wings and without falling  significantly"|Félix du Temple.<ref>French original text: "En général, l'oiseau, surtout de grande taille, ne s'élève et ne vole qu'en raison d'une vitesse acquise : cette vitesse il la prend pour s'élever soit en courant sur la terre ou sur l'eau, soit en se précipitant d'un point culminant. Une fois arrivé à une certaine hauteur qui lui permet de voler horizontalement d'un coup d'aile, il se donne de la vitesse, étend ses ailes et sa queue de manière à former avec elles un plan aussi parfait que possible et avance ainsi sans mouvement d'ailes apparent et sans tomber de manière sensible" Félix du Temple</ref>}}

==Notes==
{{reflist}}

==External links==
*[http://www.flyingmachines.org/dutm.html Flying machines]
*[http://www.cas.astate.edu/draganjac/du_Temple.html Pioneers and innovators]

==See also==
*[[List of early flying machines]]
*[[Timeline of aviation - 19th century]]
*[[History of aviation]]

{{DEFAULTSORT:Du Temple}}
[[Category:French inventors]]
[[Category:History of aviation]]
[[Category:Steam-powered aircraft]]
[[Category:1823 births]]
[[Category:1890 deaths]]