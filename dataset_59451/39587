{{Infobox college sports rivalry
| name = Maryland–Virginia football rivalry
| team1_image = Maryland terrapins logo.png
| team1_image_size = 200
| team2_image = Virginia Cavaliers text logo.svg
| team2_image_size = 125
| team1 = [[Maryland Terrapins football|Maryland Terrapins]]
| team2 = [[Virginia Cavaliers football|Virginia Cavaliers]]
| total_meetings = 78
| series_record = Maryland leads 44–32–2
| first_meeting_date = October 11, 1919
| last_meeting_date = October 12, 2013
| next_meeting_date = September 16, 2023
| trophy = Tydings Trophy (1920s–1945)
}}

The '''Maryland–Virginia football rivalry''' is an American [[college football]] [[college rivalry|rivalry]] between the Maryland Terrapins and Virginia Cavaliers. The Terrapins and Cavaliers first met in 1919 and the series has been played annually without interruption since 1957, although the series' future is in doubt beyond 2013 because of Maryland leaving the [[Atlantic Coast Conference]] (ACC) for the [[Big Ten Conference]] in 2014.<ref>{{cite news|last=Prewitt|first=Alex|title=Maryland moving to Big Ten|url=http://www.washingtonpost.com/blogs/terrapins-insider/wp/2012/11/19/maryland-approves-move-to-big-ten-reports-say/|accessdate=2012-11-19|newspaper=[[The Washington Post]]|date=November 19, 2012}}</ref>On January 12th, 2017, the schools jointly annonced a home-home series would be played during the 2023 and 2024 seasons.<ref>{{Cite news|url=http://www.virginiasports.com/sports/m-footbl/spec-rel/011217aaa.html|title=Virginia Football to Face Maryland in 2023, 2024|access-date=2017-02-24|language=en}}</ref> 

Maryland leads the series 44–32–2, although Virginia is 15–7 since 1991. Maryland possesses the longest winning streak of the series, sixteen games between 1972 and 1987, while Virginia has the second longest streak with nine consecutive wins ending in 2000.

==Background==

===Contributing factors===
[[File:TerpsO.jpg|thumb|right|325px|Maryland lines up against Virginia in Charlottesville in 2008. The Terps' offense went scoreless in the game, losing 31-0.]]
Several factors contribute to the intensity of the rivalry. The two states, and their eponymous flagship universities based in [[Charlottesville, Virginia]] and [[College Park, Maryland]], respectively, share close historical and cultural ties. The schools are located in relatively close geographic proximity, separated by about {{convert|129|mi}}. Due in large part to this proximity, the schools aggressively compete for recruits in the [[Mid-Atlantic States|Mid-Atlantic]] region.<ref name="common">Eric Prisbell, [http://www.encyclopedia.com/doc/1P2-318028.html No Common Ground; They Battle for Position in the ACC. They Compete for Recruits. Most of All, Maryland and Virginia Fight Just to Beat Each Other], ''[[The Washington Post]]'', November 13, 2003.</ref> Former Maryland coach [[Ralph Friedgen]] expressed the importance of the rivalry by stating, "It's a potential rivalry in every sport we play. They're border states. We compete for students, not just athletes."<ref>Rick Snider, [http://goliath.ecnext.com/coms2/gi_0199-3308500/Maryland-Virginia-seeking-KO-punch.html Maryland, Virginia seeking KO punch], ''[[The Washington Times]]'', November 13, 2003.</ref>

The two are both long-time members of the [[Atlantic Coast Conference]], with Maryland becoming a founding member in 1953 and Virginia joining later in that same year. When the [[2005 NCAA football realignment#Atlantic Coast Conference|conference reorganized]] in 2005, Maryland and Virginia were placed in separate divisions, but designated as cross-divisional rivals that continue to meet annually. The intensity of the rivalry is increased by a long history in the series of comebacks, shutouts, and spoilers that prevented one team from securing a conference championship or bowl game appearance. From the 1920s until 1945, the teams competed for the Tydings Trophy, named for former politician and Maryland alum [[Millard Tydings]] who had several friends amongst the professors at Virginia. In 2003, the schools discussed reviving the trophy tradition, but it was ultimately rejected by Virginia, due to concerns over the reorganization of the ACC.<ref name="common"/>

Before and after their meeting in 2010, players from both schools attributed the importance of the game to the negative feelings the programs have for each other. Virginia [[Center (American football)|center]] Anthony Mihota said "I guess it's because we don't like them very much. Something about them that gets under our skin".<ref name="mduvaap">{{cite news|last=Kurtz Jr |first=Hank |title=There's no love lost between Cavaliers and Terps|url=http://www.usatoday.com/sports/college/football/2010-11-12-1482762790_x.htm|agency=Associated Press|publisher=USA Today |accessdate=14 November 2010|date=November 12, 2010}}</ref>  After Maryland's victory, Terp [[linebacker]] Adrian Moten commented, "This was a big win in the rivalry. They hate us. We hate them."<ref name="wapo13Nov2010">{{cite news|last=Prisbell|first=Eric|title=Maryland football defeats Virginia, stays alive in ACC race  |url=http://www.washingtonpost.com/wp-dyn/content/article/2010/11/13/AR2010111303676.html?sid=ST2010111400059|accessdate=14 November 2010|newspaper=The Washington Post|date=13 November 2010}}</ref>

The high academic standing of the University of Virginia in a national publication has added to the competitiveness between the two. In 2003, [[List of Presidents of the University of Maryland, College Park|University of Maryland president]] [[C. Daniel Mote, Jr.|C.D. Mote]] asserted that, in academic terms, Virginia was "highly overrated these days ... ''[[U.S. News & World Report]]'' places them at the top of the pile with [[University of California, Berkeley|Berkeley]], which is ridiculous." Mote further stated that students from the state of [[Maryland]] paying a higher tuition cost to attend the [[University of Virginia]] "don’t know any better."<ref name="textbook">Doug Doughty, [http://www.highbeam.com/doc/1P2-12676308.html Cavs, Terrapins a textbook rivalry], ''[[The Roanoke Times]]'', November 14, 2003.</ref> While the [[List of presidents of the University of Virginia|University of Virginia president]], [[John T. Casteen III|John Casteen]], said such remarks can be taken out of context, Virginia board of visitors member, William H. Goodwin, responded in ''[[The Cavalier Daily]]'', "I certainly think a college president should have more class, but you have to expect that from Maryland."<ref name="textbook"/><ref name="cd-goodwin">{{cite news |url=http://www.cavalierdaily.com/2003/10/27/umd-president-university-overrated/ |last=Manese-lee |first=Angela |title=UMD President: University overrated |work=[[The Cavalier Daily]] |date=2003-10-27}}</ref>

===Relative importance===
Former Virginia head coach [[Al Groh]] stopped short of calling the game with Maryland a 'rivalry', but said that "it is a very important game to our team and it is our annual game. So that certainly designates it as being different from the other games."<ref>Eric Prisbell, [http://voices.washingtonpost.com/terrapins-insider/2008/10/groh_on_acc_teleconference.html Groh on ACC Teleconference], Terrapins Insider, ''[[The Washington Post]]'', October 1, 2008.</ref> Maryland head coach [[Ralph Friedgen]] conceded that "[Virginia has] Virginia Tech as an in-state rivalry, but I think we're their out-of-state rivalry."

Maryland–Virginia is the second most played out-of-state rivalry for Virginia, after the longest standing rivalry in the ACC, the [[South's Oldest Rivalry]] between Virginia and North Carolina. It is the fourth most played for the Cavaliers overall, after North Carolina, Virginia Tech ([[Commonwealth Cup (Virginia)|Commonwealth Cup]]), and Virginia Military Institute. In contrast, Maryland–Virginia is the most-played historical series for Maryland.

==Noteworthy games==

===1945===
'''Maryland 19, Virginia 13'''

In 1945, the two teams met for a neutral site match-up at [[Griffith Stadium]] in [[Washington, D.C.]] Head coach [[Frank Murray]], a future [[College Football Hall of Fame|Hall of Fame]] inductee, had led 13th-ranked Virginia to a perfect 7–0 record. The Cavaliers also held an undefeated streak of fourteen games. [[Bear Bryant]] was in his only year as Maryland's head coach. Maryland's Sam Behr broke away for a 90-yard touchdown run, but Virginia held the lead in the fourth quarter.<ref name="46terp">''The Terrapin'', Class of 1946, University of Maryland yearbook, p. 175–176.</ref> In the final seconds of play, Terrapin freshman tailback Bill "Red" Poling completed a forward pass to end [[Don Gleasner]] for a 50-yard score.<ref>[http://www.themclub.org/about/newsletter/MClub_News_Spring07.pdf Club News] (PDF), The M Club, Spring 2007, retrieved January 31, 2009.</ref> With the win, Maryland tied the all-time series record. The 19–13 loss to Maryland ended Virginia's perfect season and they fell to 20th place in the AP Poll.<ref name="45uvapoll">[http://www.appollarchive.com/football/ap/teams/by_season.cfm?seasonid=1945&teamid=104 Virginia 1945 AP Football Rankings], AP Poll Archive, retrieved February 14, 2009.</ref> After this game, the series went on hiatus for 11 years, and it was the last year the Tydings Trophy was awarded.<ref name="common"/>

===1961===
'''Virginia 28, Maryland 16'''

[[Tom Nugent]]'s Terps entered their last season game boasting a 7–2 record that included a win over seventh-ranked [[Syracuse Orange football|Syracuse]] with [[Heisman Trophy]]-winner [[Ernie Davis]] and the only victory over [[Penn State Nittany Lions football|Penn State]] until 2014. They faced a Virginia team that started off their season by breaking a three-year, 28-game losing streak against [[William & Mary Tribe|William & Mary]]. The Cavaliers had lost the last three meetings against Maryland by an average of 38 points. With a win against Virginia, the Terps would secure the ACC title and a trip to the [[Gator Bowl]]. After Maryland scored for an early lead, Virginia quarterback [[Gary Cuozzo]] tossed three short touchdown passes to third-string receiver John Hepler. Defensive back [[Ted Rzempoluch]] returned an interception 95 yards for a touchdown, and sealed the Terps' fate to go bowl-less. The other three-loss team in the ACC, Duke, won the conference.<ref>Mervin Hyman, [http://vault.sportsillustrated.cnn.com/vault/article/magazine/MAG1073283/index.htm Football's Week], ''Sports Illustrated'', December 4, 1961, retrieved November 14, 2008.</ref>

===1988===
'''Virginia 24, Maryland 23'''

In the final minutes of the game, with the Terps trailing the Cavaliers 24–17, Maryland quarterback [[Neil O'Donnell]] was injured and taken out of the game. He was replaced by a young second-stringer [[Scott Zolak]]. Zolak led a drive which culminated with a three-yard option run for a touchdown narrowing the deficit to a point. With 1:09 left in the game, Maryland went for two. Zolak threw into the endzone to running back Ricky Johnson. Johnson appeared to catch the ball before being hit by defensive back Keith McMeans, knocking the ball loose. An official in the endzone immediately signaled for a catch, before others waved it as incomplete. After the game, the usually level-headed Terps head coach Joe Krivak ran to the official who made the initial call, following him to the locker room, and then ran over to yell at referee Don Safrit. Several witnesses claim he said to the officials that "If it takes every ounce of energy, I'm going to get you out of this league."<ref>[http://pqasb.pqarchiver.com/washingtonpost/access/73645150.html?dids=73645150:73645150&FMT=ABS&FMTS=ABS:FT&type=current&date=Nov+20%2C+1988&author=Dave+Sell&pub=The+Washington+Post+(pre-1997+Fulltext)&desc=USC%2C+Mountineers+Perfect%3B+Terrapins+Cry+Foul%3B+Virginia%27s+24-23+Victory+Controversial&pqatl=google Terrapins Cry Foul; Virginia's 24-23 Victory Controversial], ''The Washington Post'', November 20, 1988.</ref> Virginia (7–4) broke their 16-game losing streak in the series and finished second in the conference. Maryland (5–6) failed to achieve a winning season for the third consecutive year.<ref>[https://www.nytimes.com/1988/11/20/sports/college-football-south-virginia-at-last-defeats-maryland.html College Football: South; Virginia, at Last, Defeats Maryland], ''The New York Times'', November 20, 1988.</ref> However, the Cavaliers failed to secure a bowl game berth, while N.C. State with a worse record went on to the [[Peach Bowl]].

===1990===
'''Maryland 35, Virginia 30'''

Besieged Maryland head coach [[Joe Krivak]], with a 17–25 record, led his Terps (5–5) to a sold-out Scott Stadium in Charlottesville for their final regular season game. [[George Welsh (American football)|George Welsh]]'s eighth-ranked Virginia (8–1) had spent three weeks atop the AP poll at number-one until a loss against [[Georgia Tech Yellow Jackets football|Georgia Tech]]. At halftime, Maryland trailed 21–7. Then freshman reserve running back Mark Mason put the Terps back in the game with a 59-yard touchdown breakaway, and a team best run for the year. Maryland quarterback [[Scott Zolak]] threw for two more touchdowns and Mason scored the go-ahead with an 8-yard rush to put the Terps ahead 35–30. With the win, Maryland secured a bowl berth for the first time since [[Bobby Ross]] left four seasons prior.<ref>[https://query.nytimes.com/gst/fullpage.html?res=9C0CEFD71331F93BA25752C1A966958260&scp=88&sq=maryland%20football&st=cse Virginia upset by Maryland 35-30], ''New York Times'', November 18, 1990, retrieved November 14, 2008.</ref> Virginia took a nose-dive in the AP rankings to 17th, and after another loss to Virginia Tech settled for the [[Sugar Bowl]] where they were edged by Tennessee, 23–22.<ref>Bob Boyles and Paul Guido, ''50 Years of College Football: A Modern History of America's Most Colorful Sport'', p. 1255, Skyhorse Publishing, 2007.</ref>

===1999===
'''Virginia 34, Maryland 30'''

After starting 5–2, Maryland suffered a three-game losing streak before their finale at [[Byrd Stadium]]. Maryland needed to beat Virginia to finish with a winning season and likely secure a berth in the [[Aloha Bowl]] or [[Oahu Bowl]]. At the end of the first quarter, Virginia led 17–0. Maryland's freshman quarterback [[Latrez Harrison]] was pulled and replaced by former safety Randall Jones. Jones along with Heisman prospect running back [[LaMont Jordan]] led the Terps to tie it at 17 before halftime. In the third quarter, Jordan ran 90 yards to make it 24–17, but the Cavaliers tied it soon after. With 5:18 left, Terps kicker Brian Kopka made a field goal to make it 30–27. In the next series, Virginia went for it on fourth down but a Dan Ellis pass was incomplete. The Terps took over on downs with 1:40 left. Victory seemed assured, but Maryland failed to convert a first down and the inexperienced Randall Jones ran out of bounds on third down, which stopped the clock when Virginia had no time-outs. The Cavaliers got the ball on their own 24-yard line. In nine plays and just 46 seconds, Dan Ellis led a 76-yard drive that culminated with a touchdown pass to Billy McMullen to win the game with 26 seconds left. The 34–30 loss dashed the Terrapins' hopes for their first bowl game since 1990.<ref>David Ginsburg, [http://umterps.cstv.com/sports/m-footbl/recaps/112099aaa.html Cavs rally to knock off Terps, 34–30], AP Sports, University of Maryland, November 20, 1999, retrieved November 16, 2008.</ref> After the game, Maryland coach [[Ron Vanderlinden]] remarked "Devastating. The worst loss I've ever been associated with because of what was on the line."<ref>{{cite news| url=http://voices.washingtonpost.com/terrapins-insider/2008/10/most_memorable_maryland-virgin.html | work=The Washington Post | title=Most Memorable Maryland-Virginia Games | accessdate=12 May 2010}}</ref>

===2002===
'''Virginia 48, Maryland 13'''

After Terps head coach Ralph Friedgen stated Maryland expected to "beat teams like Duke and Virginia", the Cavs sought vindication for the comparison to ACC football's perpetual underdog. While Maryland's [[Scott McBrien]] passed for a touchdown in the first series, Virginia went on to score 20 unanswered points in the second quarter with two Matt Schaub passes and two field goals. In the second half, the Cavaliers delivered four more touchdowns: another Schaub pass, a pass by receiver [[Billy McMullen]], and two short rushing scores. The Terps only managed to use [[Nick Novak]] to make two field goals. The loss broke Maryland's eight-game winning streak, and prevented them from sharing a piece of the ACC championship. Instead, Florida State finished with a better conference record (7–1), and secured the title outright.<ref>[http://sports.espn.go.com/ncf/recap?gameId=223270258 Schaub throws three TDs, Lundy scores three in upset], ESPN, November 23, 2002, retrieved November 15, 2008.</ref>

===2003===
'''Maryland 27, Virginia 17'''

With leading rusher [[Bruce Perry]] out due to a sprained ankle, Terps running back Josh Allen adeptly filled in, and racked up 257 yards and two touchdowns. Virginia quarterback [[Matt Schaub]] threw for a touchdown and ran for another, but his second-half comeback attempt ultimately fell short. In the game, Schaub overtook [[Shawn Moore]]'s career passing record at the school. There was also some excitement even before the game started. Maryland assistant coach James Franklin and Virginia head coach [[Al Groh]] had a heated exchange after a Cavaliers player interrupted the Terps' warm up drills. Then, after Virginia won the coin toss, Al Groh infamously elected to kick off to start ''both'' halves. Immediately after the decision, the entire Maryland team rushed the field in celebration and was given a 15-yard [[unsportsmanlike conduct]] penalty before the opening kickoff. The win kept the Terps' ACC title hopes alive, but Florida State narrowly beat N.C. State 50–44 to win the conference outright. Maryland finished second-place in the ACC.<ref>[http://sports.espn.go.com/ncf/recap?gameId=233170120 Allen rushes for 257 yards, two TDs], ESPN, November 13, 2003.</ref>

===2006===
'''Maryland 28, Virginia 26'''

After giving up 255 yards, the Terps trailed 20–0 at halftime. Cavaliers punt returner Emmanuel Byers [[muffed punt|muffed]] a third-quarter punt to give Maryland possession on the Virginia one-yard line. [[Lance Ball]] rushed for a touchdown on the next play, and the Terps went on to score another 21 points in the final quarter to include a 56-yard breakaway by [[Keon Lattimore]] and a 45-yard [[interception#Interception returned for touchdown|pick-six]] by linebacker [[Erin Henderson]]. With 2:37 left on the clock, the Cavaliers responded with a 44-yard touchdown pass to [[Kevin Ogletree]], to pull to 28–26. Virginia attempted a two-point conversion to send the game into overtime. Cornerback [[Josh Wilson (American football)|Josh Wilson]], who was scored on twice earlier, broke up the pass to clinch the win for the Terps.<ref>[http://umterps.cstv.com/sports/m-footbl/recaps/101406aaa.html Maryland overcomes 20-point deficit for 28–26 win at Virginia], Associated Press, University of Maryland, October 14, 2006, retrieved November 14, 2008.</ref>

===2007===
'''Virginia 18, Maryland 17'''

Reserve sophomore running back [[Mikell Simpson]] racked up 119 yards rushing and two TDs to go along with 152 receiving yards, for 271 all-purpose yards, as 19th-ranked Virginia came from behind to defeat Maryland. The game ended when, with 16 seconds left, Simpson dived into the endzone as the ball was knocked out of his hands. It was ruled a touchdown and Virginia defeated Maryland. Terps coach [[Ralph Friedgen]] said "I saw the ball come out on the goal line. I saw it and I don't think he had possession." Simpson himself stated, "It crossed the line. I knew I scored because I looked down and saw the yellow line and I saw the ball cross [before] they hit it out." With the victory, the Cavaliers reached seven wins in a row for the first time since 1990 and for only the fourth time in school history. They also accomplished the feat in 1914 and 1949.<ref>[http://sports.espn.go.com/ncf/recap?gameId=272930120 Simpson's late TD caps all-around day in Virginia's comeback win], ESPN, October 20, 2007, retrieved November 15, 2008.</ref> That season, Virginia won an NCAA record of five games by two points or less, with the other edged teams being [[2007 North Carolina Tar Heels football team|North Carolina]], [[2007 Middle Tennessee Blue Raiders football team|Middle Tennessee State]], [[2007 Connecticut Huskies football team|Connecticut]], and [[2007 Wake Forest Demon Deacons football team|Wake Forest]].<ref>[http://www.ncaapublications.com/Uploads/PDF/2007_d1_football_records_book8205c0e9-8be5-47e9-8f95-ebb50bf991fe.pdf Football Bowl Subdivision Records], National Collegiate Athletic Association, 2008, p. 111.</ref>

==Game results==
{{sports rivalry series table
| no_number = true
| series_summary = yes
| format = compact
| cols = 2
| team1 = Maryland
| team1style = {{CollegePrimaryStyle|Maryland Terrapins|border=0|color=white}}
| team2 = Virginia
| team2style = {{CollegePrimaryStyle|Virginia Cavaliers|border=0|color=white}}

| October 11, 1919 | [[Charlottesville, Virginia|Charlottesville, VA]] | Maryland | 13 | Virginia | 0
| October 24, 1925 | Charlottesville, VA | Virginia | 6 | Maryland | 0
| November 13, 1926 | [[College Park, Maryland|College Park, MD]] | Maryland | 6 | Virginia | 6
| November 12, 1927 | Charlottesville, VA | Virginia | 21 | Maryland | 0
| November 17, 1928 | College Park, MD | Maryland | 18 | Virginia | 2
| November 2, 1929 | College Park, MD | Maryland | 13 | Virginia | 13
| November 1, 1930 | Charlottesville, VA | Maryland | 14 | Virginia | 6
| October 3, 1931 | College Park, MD | Maryland | 7 | Virginia | 6
| October 1, 1932 | Charlottesville, VA | Virginia | 7 | Maryland | 6
| November 4, 1933 | Charlottesville, VA | Virginia | 6 | Maryland | 0
| November 3, 1934 | College Park, MD | Maryland | 20 | Virginia | 0
| November 2, 1935 | Charlottesville, VA | Maryland | 14 | Virginia | 7
| October 17, 1936 | Charlottesville, VA | Maryland | 21 | Virginia | 0
| October 16, 1937 | Charlottesville, VA | Maryland | 3 | Virginia | 0
| October 22, 1938 | College Park, MD | Virginia | 27 | Maryland | 19
| October 14, 1939 | Charlottesville, VA | Virginia | 12 | Maryland | 7
| October 12, 1940 | College Park, MD | Virginia | 19 | Maryland | 6
| November 14, 1942 | Charlottesville, VA | Maryland | 27 | Virginia | 12
| November 6, 1943 | Charlottesville, VA | Virginia | 39 | Maryland | 0
| November 4, 1944 | [[Washington, D.C.]] | Virginia | 18 | Maryland | 7
| November 24, 1945 | Washington, D.C. | Maryland | 19 | #13 Virginia | 13
| November 23, 1957 | College Park, MD | Maryland | 12 | Virginia | 0
| November 22, 1958 | Charlottesville, VA | Maryland | 44 | Virginia | 6
| November 21, 1959 | College Park, MD | Maryland | 55 | Virginia | 12
| November 19, 1960 | Charlottesville, VA | Maryland | 44 | Virginia | 12
| November 25, 1961 | Charlottesville, VA | Virginia | 28 | Maryland | 16
| November 24, 1962 | College Park, MD | Maryland | 40 | Virginia | 18
| November 23, 1963 | College Park, MD | Maryland | 21 | Virginia | 6
| November 21, 1964 | Charlottesville, VA | Maryland | 10 | Virginia | 0
| November 20, 1965 | College Park, MD | Virginia | 33 | Maryland | 27
| November 19, 1966 | Charlottesville, VA | Virginia | 41 | Maryland | 17
| November 25, 1967 | College Park, MD | Virginia | 12 | Maryland | 7
| November 23, 1968 | Charlottesville, VA | Virginia | 28 | Maryland | 23
| November 22, 1969 | College Park, MD | Maryland | 17 | Virginia | 14
| November 21, 1970 | Charlottesville, VA | Maryland | 17 | Virginia | 14
| November 20, 1971 | College Park, MD | Virginia | 29 | Maryland | 27
| October 28, 1972 | Charlottesville, VA | Maryland | 24 | Virginia | 23
| November 10, 1973 | College Park, MD | Maryland | 33 | Virginia | 0
| November 23, 1974 | Charlottesville, VA | Maryland | 10 | Virginia | 0
| November 22, 1975 | College Park, MD | Maryland | 62 | Virginia | 24
| November 20, 1976 | Charlottesville, VA | Maryland | 28 | Virginia | 0
| November 19, 1977 | College Park, MD | Maryland | 28 | Virginia | 0
| November 11, 1978 | Charlottesville, VA | Maryland | 17 | Virginia | 7
| November 24, 1979 | College Park, MD | Maryland | 17 | Virginia | 7
| November 22, 1980 | Charlottesville, VA | Maryland | 31 | Virginia | 0
| November 21, 1981 | College Park, MD | Maryland | 48 | Virginia | 7
| November 20, 1982 | Charlottesville, VA | Maryland | 45 | Virginia | 14
| October 1, 1983 | College Park, MD | Maryland | 23 | Virginia | 3
| November 24, 1984 | Charlottesville, VA | Maryland | 45 | Virginia | 34
| November 29, 1985 | College Park, MD | Maryland | 33 | Virginia | 21
| November 28, 1986 | Charlottesville, VA | Maryland | 42 | Virginia | 10
| September 12, 1987 | College Park, MD | Maryland | 21 | Virginia | 19
| November 19, 1988 | Charlottesville, VA | Virginia | 24 | Maryland | 23
| November 18, 1989 | College Park, MD | Virginia | 48 | Maryland | 21
| November 17, 1990 | Charlottesville, VA | Maryland | 35 | #8 Virginia | 30
| September 7, 1991 | College Park, MD | Maryland | 17 | Virginia | 6
| September 5, 1992 | Charlottesville, VA | Virginia | 28 | Maryland | 15
| September 4, 1993 | College Park, MD | Virginia | 43 | Maryland | 29
| November 12, 1994 | Charlottesville, VA | Virginia | 46 | Maryland | 21
| November 11, 1995 | College Park, MD | Virginia | 21 | Maryland | 18
| September 14, 1996 | Charlottesville, VA | Virginia | 21 | Maryland | 3
| November 1, 1997 | College Park, MD | Virginia | 45 | Maryland | 0
| September 12, 1998 | Charlottesville, VA | Virginia | 31 | Maryland | 19
| November 20, 1999 | College Park, MD | Virginia | 34 | Maryland | 30
| October 7, 2000 | Charlottesville, VA | Virginia | 31 | Maryland | 23
| October 6, 2001 | College Park, MD | #25 Maryland | 41 | Virginia | 21
| November 23, 2002 | Charlottesville, VA | Virginia | 48 | #18 Maryland | 13
| November 13, 2003 | College Park, MD | #24 Maryland | 27 | Virginia | 17
| November 6, 2004 | Charlottesville, VA | #13 Virginia | 16 | Maryland | 0
| October 1, 2005 | College Park, MD | Maryland | 45 | #18 Virginia | 33
| October 14, 2006 | Charlottesville, VA | Maryland | 28 | Virginia | 26
| October 20, 2007 | College Park, MD | #24 Virginia | 18 | Maryland | 17
| October 4, 2008 | Charlottesville, VA | Virginia | 31 | Maryland | 0
| October 17, 2009 | College Park, MD | Virginia | 20 | Maryland | 9
| November 14, 2010 | Charlottesville, VA | Maryland | 42 | Virginia | 23
| November 5, 2011 | College Park, MD | Virginia | 31 | Maryland | 13
| October 13, 2012 | Charlottesville, VA | Maryland | 27 | Virginia | 20
| October 12, 2013 | College Park, MD | Maryland | 27 | Virginia | 26
}}

==See also==
* [[List of NCAA college football rivalry games]]

==References==
{{Reflist|2}}

{{Maryland Terrapins football navbox}}
{{Virginia Cavaliers football navbox}}
{{Atlantic Coast Conference rivalry navbox}}
{{Big Ten Conference football rivalry navbox}}
{{Maryland Terrapins rivalries navbox}}

{{DEFAULTSORT:Maryland-Virginia football rivalry}}
[[Category:College football rivalries in the United States]]
[[Category:Maryland Terrapins football]]
[[Category:Virginia Cavaliers football]]
[[Category:Maryland Terrapins rivalries]]