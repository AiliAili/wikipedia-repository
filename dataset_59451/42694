{|{{Infobox Aircraft Begin
  |name = DC-1
  |image =Douglas DC-1.jpg
  |caption = Douglas DC-1 on its handover to TWA in December 1933
}}{{Infobox Aircraft Type
  |type = Prototype and Testbed
  |manufacturer =[[Douglas Aircraft Company]]
  |designer =
  |first flight = July 1, 1933
  |introduced = December [[1933 in aviation|1933]]
  |retired =
  |status = Destroyed
  |primary user = [[Trans World Airlines|Transcontinental & Western Air]]
  |more users =
  |produced =
  |number built = 1
  |unit cost =
  |variants with their own articles =
  |developed into= [[Douglas DC-2]]
}}
|}

The '''Douglas DC-1''' was the first model of the famous [[United States]] DC (Douglas Commercial) commercial transport aircraft series. Although only one example of the DC-1 was produced, the design was the basis for the [[Douglas DC-2|DC-2]] and [[Douglas DC-3|DC-3]].

==Design and development==
Development of the DC-1 can be traced back to the 1931 crash of [[TWA Flight 599]], a [[Fokker F.10]] Trimotor airliner in which a wing failed, probably because water had seeped between the layers of the wood laminate and dissolved the glue holding the layers together. Following the accident, the [[United States government role in civil aviation|Aeronautics Branch of the U.S. Department of Commerce]] placed stringent restrictions on the use of wooden wings on passenger airliners.<ref name="Friedman">Friedman and Friedman ''Aeroplane Monthly'' May 2001, pp. 34–40.</ref><ref name="databasep71">O'Leary ''Aeroplane Monthly'' February 2007, p. 71.</ref> [[Boeing]] developed an answer, the [[Boeing 247|247]], a twin-engined all-metal [[monoplane]] with a retractable undercarriage, but their production capacity was reserved to meet the needs of  [[United Airlines]], part of [[United Aircraft and Transport Corporation]] which also owned Boeing. [[Trans World Airlines|TWA]] needed a similar aircraft to respond to competition from the Boeing 247 and they asked five manufacturers to bid for construction of a three-engined, 12-seat aircraft of all-metal construction, capable of flying 1,080&nbsp;mi (1,740&nbsp;km) at 150&nbsp;mph (242&nbsp;km/h).  The most demanding part of the specification was that the airliner would have to be capable of safely taking off from any airport on TWA's main routes (and in particular [[Albuquerque]], at high altitude and with severe summer temperatures) with one engine non-functioning.<ref name="Frnc doug p166">Francillon 1979, p. 166.</ref><ref name="AE19 p60">Pearcy ''Air Enthusiast'' 1982, p. 60.</ref>

[[Donald Wills Douglas, Sr.|Donald Douglas]] was initially reluctant to participate in the invitation from TWA. He doubted that there would be a market for 100 aircraft, the number of sales necessary to cover development costs. Nevertheless, he submitted a design consisting of an [[Aluminium|all-metal]], low-wing, twin-engined aircraft seating 12 passengers, a crew of two and a flight attendant. The aircraft exceeded the specifications of TWA even with two engines, principally through the use of [[variable-pitch propeller|controllable pitch propellers]].<ref>Smith (1998), p. 10</ref> It was insulated against noise, heated, and fully capable of both flying and performing a controlled takeoff or landing on one engine.

Don Douglas stated in a 1935 article on the DC-2 that the first DC-1 cost $325,000 to design and build.<ref>[https://books.google.com/books?id=yN8DAAAAMBAJ&pg=PA213&dq=Popular+Science+1935+plane+%22Popular+Mechanics%22&hl=en&ei=t_c0TqiZF-6rsALn6bXtCg&sa=X&oi=book_result&ct=result&resnum=1&ved=0CCgQ6AEwADgK#v=onepage&q&f=true "Douglas Tells Secrets of Speed"], ''Popular Mechanics'', February 1935.</ref>
[[File:TWA DC-1.jpg|thumb|left|DC-1 in TWA markings]]

==Operational history==
Only one aircraft was produced. The prototype made its maiden flight on July 1, 1933,<ref>Gradidge 2006, p. 9.</ref> flown by Carl Cover. It was given the model name '''DC-1''', derived from "'''D'''ouglas '''C'''ommercial". During a half-year of testing, it performed more than 200 test flights and demonstrated its superiority over the most used airliners at that time, the [[Ford Trimotor]] and [[Fokker F.VII|Fokker Trimotor]]. It was flown across the United States on February 19, 1934, making the journey in the record time of 13 hours 5 minutes.<!--nonstop?-->{{citation needed|date = February 2017}}

TWA accepted the aircraft on 15 September 1933 with a few modifications (mainly increasing seating to 14 passengers and adding more powerful engines) and <!--later?-->ordered 20 examples of the developed production model which was named the [[Douglas DC-2]].<ref name="Gradidge 2006, p. 299">Gradidge 2006, p. 299.</ref>

The DC-1 was sold to [[Lord Forbes]] in the United Kingdom in May 1938, who operated it for a few months before selling it in [[France]] in October 1938. It was then sold to ''[[Líneas Aéreas Postales Españolas]]'' (L.A.P.E.) in [[Spain]] in November 1938 and was also used by the [[Spanish Republican Air Force]] as a transport aircraft.<ref>[http://www.zi.ku.dk/personal/drnash/model/spain/did.html "Aircraft that took part in the Spanish Civil War."] ''Aircraft of the Spanish Civil War (zi.ku).'' Retrieved: February 4, 2011.</ref> Later operated by [[Iberia Airlines]] from July 1939 with the name ''Negron'' it force-landed at [[Málaga]], Spain, in December 1940 and was damaged beyond repair.<ref name="Gradidge 2006, p. 299"/>

==Specifications (DC-1)==
{{Aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=McDonnell Douglas Aircraft since 1920<ref name="Frnc doug p173">Francillon 1979, p. 173.</ref>
|crew=2 pilots
|capacity= 12 passengers
|length main=60 ft 0 in	
|length alt=18.29 m
|span main=85 ft 0 in	
|span alt=25.91 m
|height main=16 ft 0 in 
|height alt=4.88 m
|area main=942 sq ft
|area alt=87.5 m²
|empty weight main=11,780 lb 	
|empty weight alt=5,343 kg
|loaded weight main=17,500 lb 	
|loaded weight alt=7,938 kg
|engine (prop)=	[[Wright R-1820|Wright Cyclone SGR-1820F3]]
|type of prop=9-cylinder [[radial engine]] driving variable-pitch propellers
|number of props=2
|power main=690 hp
|power alt=515 kW
|max speed main=210 mph 	
|max speed alt=183 knots, 338 km/h
|cruise speed main=190 mph
|cruise speed alt=165 knots, 306 km/h
|cruise speed more=at 8,000 ft (2,440 m)
|range main=1,000 mi
|range alt=870 [[nautical mile|nmi]], 1,610 km
|ceiling main=23,000 ft 	
|ceiling alt=7,010 m
|loading main=
|loading alt=
|power/mass main=
|power/mass alt=
|armament=
}}

==See also==
{{Aircontent
|related=
* [[Douglas DC-2]]
* [[Douglas DC-3]]
|similar aircraft=
* [[Boeing 247]]
|lists=
* [[List of civil aircraft]]
|see also=
}}

==References==
;Notes
{{Reflist|2}}
;Bibliography
{{refbegin}}
* Francillon, René J. ''McDonnell Douglas Aircraft since 1920''. London: Putnam, 1979. ISBN 0-370-00050-1.
* Freidman, Herbert M. and Ada Kera Friedman. "The Legacy of the Rockne Crash". ''Aeroplane'',  Vol. 29, No. 5, Issue 337, May 2001, pp.&nbsp;34–40. London: IPC. ISSN 0143-7240. 
* Gradidge, Jennifer M., ed. ''DC-1, DC-2, DC-3: The First Seventy Years.'' Tonbridge, Kent, UK: Air-Britain (Historians), Two volumes, 2006. ISBN 0-85130-332-3.
* O'Leary, Michael. "Database: Douglas DC-1 & DC-2". ''[[Aeroplane Monthly|Aeroplane]]'', Vol. 35, No. 2, Issue 406, February 2007, pp.&nbsp;70–89. London: IPC. ISSN 0143-7240. 
* Pearcy, Arthur. "Douglas Commercial Two". ''[[Air Enthusiast]]'', Nineteen, August–November 1982, pp.&nbsp;60–77. Bromley, UK: Fine Scroll. ISSN 0143-5450. 
{{refend}}
*Smith, Richard K. (1998). [http://newpreview.afnews.af.mil/shared/media/document/AFD-100929-015.pdf ''Seventy-Five Years of Inflight Refueling: Highlights 1923-1998''] Air Force History and Museums, Air University, Maxwell AFB

==External links==
{{commons category}}
* {{cite journal |date=February 1934 |title=Inside the Douglas Transport |journal=[[Popular Aviation]] |volume=XIV |issue=2 |pages=86–88 |url=https://books.google.com/books?id=99pjuOJ9aFYC&lpg=PP1&lr&pg=PA85-IA1#v=onepage&q&f=false |accessdate=December 24, 2013 }} A contemporary, somewhat technical article on the Douglas DC-1.
* [http://www.aviation-history.com/douglas/dc3.html Douglas DC-1, 2, and 3]
* [http://www.flyinghigher.net/douglas/NC223Y.html Douglas DC-1]
*[http://www.dc3history.org/donalddouglas.htm Development of the DC-1 story]
* [https://www.flickr.com/photos/auburnuniversitydigitallibrary/3018967837/ Photo: The DC-1 before sale to Howard Hughes]
* [http://www.gettyimages.co.uk/detail/news-photo/lord-forbes-and-his-new-douglas-airliner-on-arrival-from-news-photo/3352218 Photo: Lord Forbes inspecting the DC-1 in London Docks on arrival in 1938]  
* [https://www.flickr.com/photos/iberialineasaereas/5811450762/ Photo: The DC-1 in Spain]

{{Douglas airliners}}

{{DEFAULTSORT:Douglas Dc-02}}
[[Category:Douglas aircraft|DC-01]]
[[Category:United States airliners 1930–1939]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Low-wing aircraft]]