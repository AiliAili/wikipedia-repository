{{Infobox journal
| title = Educational Researcher 
| cover = [[File:Educational_Researcher_Cover_Image.png]]
| editor = Carolyn D. Herrington, Vivian L. Gadsden
| discipline = [[Educational research]]
| former_names =
| abbreviation = Educ. Res.
| publisher = [[Sage Publications]] on behalf of the [[American Educational Research Association]]
| country = United States
| frequency = 9/year
| history =1972-present
| openaccess =
| license =
| impact = 2.527
| impact-year = 2014
| website = http://www.sagepub.com/journals/Journal201856/title
| link1 = http://edr.sagepub.com/content/current
| link1-name = Online access
| link2 = http://edr.sagepub.com/content/by/year
| link2-name = Online archive
| link3 = http://www.aera.net/Publications/Journals/EducationalResearcher/tabid/12609/Default.aspx
| link3-name = Journal page at association's website
| ISSN = 0013-189X
| eISSN = 1935-102X
| OCLC = 56209028
| LCCN = 87657066
}}
'''''Educational Researcher''''' is a [[peer-reviewed]] [[academic journal]] that covers the field of [[education]]. The [[editors-in-chief]] are Carolyn D. Herrington ([[Florida State University]]) and Vivian L. Gadsden ([[University of Pennsylvania]]). It was established in 1972 and is published by [[Sage Publications]] on behalf of the [[American Educational Research Association]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 2.527, ranking it 9th out of 224 journals in the category "Education and Educational Research".<ref name=WoS>{{cite book |year=2015 |chapter=Educational Researcher |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201856/title}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1972]]
[[Category:Education journals]]