{{Use British (Oxford) English|date=October 2012}}
{{Use dmy dates|date=October 2012}}
{{Infobox book
| name          = Bad Pharma
| image         = Bad Pharma.jpg
| author        = [[Ben Goldacre]]
| country       =
| genre         = Science writing, medicine, investigative journalism
| publisher     = [[Fourth Estate (imprint)|Fourth Estate]] (UK)
| publisher2    = [[Faber & Faber]] (US), Signal (Canada)
| pub_date      = 25 September 2012
| pages         = 430 (first edition)
| isbn          = 978-0-00-735074-2
| dewey         =
| congress      =
| oclc          =
| preceded_by   = [[Bad Science (book)|Bad Science]]
| followed_by   =
}}
'''''Bad Pharma: How Drug Companies Mislead Doctors and Harm Patients''''' is a book by the British physician and academic [[Ben Goldacre]] about the [[pharmaceutical industry]], its relationship with the medical profession, and the extent to which it controls academic research into its own products.<ref name=Dillner>Luisa Dillner, [https://www.theguardian.com/books/2012/oct/17/bad-pharma-ben-goldacre-review "Bad Pharma by Ben Goldacre – review"], ''The Guardian'', 17 October 2012.</ref> It was published in the UK in September 2012 by the [[Fourth Estate (imprint)|Fourth Estate]] imprint of [[HarperCollins]], and in the United States in February 2013 by [[Faber and Faber]].

Goldacre argues in the book that "the whole edifice of medicine is broken", because the evidence on which it is based is systematically distorted by the pharmaceutical industry.{{refn|group=n|Goldacre, 2012: "Medicine is broken&nbsp;... We like to imagine that medicine is based on evidence, and the results of fair tests. In reality, those tests are often profoundly flawed. We like to imagine that doctors are familiar with the research literature, when in reality much of it is hidden from them by drug companies. We like to imagine that doctors are well-educated, when in reality much of their education is funded by industry. We like to imagine that regulators only let effective drugs onto the market, when in reality they approve hopeless drugs, with data on side effects casually withheld from doctors and patients.{{pb}}"I'm going to tell you how medicine works&nbsp;... We're going to see that the whole edifice of medicine is broken, because the evidence we use to make decisions is hopelessly and systematically distorted&nbsp;..."<ref>Ben Goldacre, ''Bad Pharma'', Fourth Estate, 2012, ix.</ref>}} He writes that the industry finances most of the [[clinical trial]]s into its own products and much of doctors' continuing education, that clinical trials are often conducted on small groups of unrepresentative subjects and negative data is routinely withheld, and that apparently independent academic papers may be planned and even [[Medical ghostwriter|ghostwritten]] by pharmaceutical companies or their contractors, without disclosure.<ref>''Bad Pharma'', x–xi, 287ff; [http://www.economist.com/node/21563689 "Pick your pill out of a hat"], ''The Economist'', 29 September 2012.</ref> Describing the situation as a "murderous disaster", he makes suggestions for action by patients' groups, physicians, academics and the industry itself.<ref>''Bad Pharma'', xii, 357ff.</ref>

Responding to the book's publication, the [[Association of the British Pharmaceutical Industry]] issued a statement in 2012 arguing that the examples the book offers were historical, that the concerns had been addressed, that the industry is among the most regulated in the world, and that it discloses all data in accordance with international standards.<ref name=ABPI>[http://www.abpi.org.uk/media-centre/newsreleases/2012/Pages/051012.aspx "ABPI statement on Ben Goldacre's book 'Bad Pharma'"], Association of the British Pharmaceutical Industry, 5 October 2012.<p>
Ben Adams, [http://www.pharmatimes.com/Article/12-10-12/Goldacre_takes_ABPI_to_task_over_book_snub.aspx "Goldacre takes ABPI to task over book snub"], ''Pharma Times'', 12 October 2012.</ref>

In January 2013 Goldacre joined the [[Cochrane Collaboration]], ''[[British Medical Journal]]'' and others in setting up [[AllTrials]], a campaign calling for the results of all past and current clinical trials to be reported.<ref name=alltrials>[http://www.alltrials.net/about/ "About"], alltrials.net; Tracey Brown, [http://www.thecochranelibrary.com/details/editorial/4627901/Its-time-for-AllTrials-registered-and-reported.html "It's time for AllTrials registered and reported"], ''The Cochrane Library'', 30 April 2013.</ref> The British House of Commons [[Public Accounts Committee (United Kingdom)|Public Accounts Committee]] expressed concern in January 2014 that drug companies were still only publishing around 50 percent of clinical-trial results.<ref name=Tovey>David Tovey, [http://www.huffingtonpost.co.uk/dr-david-tovey/tamiflu-report_b_4535688.html "Why the Public Accounts Committee Report on Tamiflu Is Important for Us All"], ''Huffington Post'', 3 January 2014.<p>
Rajeev Syal, [https://www.theguardian.com/business/2014/jan/03/drug-firms-information-clinical-trials "Drug companies accused of holding back complete information on clinical trials"], ''The Guardian'', 3 January 2014.</ref>

==Author==
[[File:Ben Goldacre TAM London 2009 (2).JPG|thumb|alt=photograph|[[Ben Goldacre]]]]
After graduating in 1995 with a first-class honours degree in medicine from [[Magdalen College, Oxford]], Goldacre obtained an MA in philosophy from [[King's College London]], then undertook clinical training at [[UCL Medical School]], qualifying as a medical doctor in 2000 and as a psychiatrist in 2005. As of 2014 he was Wellcome Research Fellow in Epidemiology at the [[London School of Hygiene and Tropical Medicine]].<ref>Ben Goldacre, [http://www.badscience.net/about-dr-ben-goldacre/ "About"], badscience.net.</ref>

Goldacre is known for his "Bad Science" column in the ''Guardian'', which he has written since 2003, and for his first book, ''[[Bad Science (book)|Bad Science]]'' (2008). This unpicked the claims of several forms of alternative medicine, and criticized certain physicians and the media for a lack of [[critical thinking]]. It also looked at the [[MMR vaccine controversy]], [[AIDS denialism]], the [[placebo effect]] and the misuse of statistics.<ref>Ben Goldacre, ''Bad Science'', Harper Perennial, 2008.</ref> Goldacre was recognized in June 2013 by ''[[Health Service Journal]]'' as having done "more than any other single individual to shine a light on how science and research gets distorted by the media, politicians, quacks, PR and the pharmaceutical industry."<ref>[http://blogs.lshtm.ac.uk/news/2013/06/12/nick-black-and-ben-goldacre-named-among-clinical-leaders-making-greatest-impact/ "Nick Black and Ben Goldacre named among clinical leaders making greatest impact"], London School of Hygiene and Tropical Medicine, 12 June 2013.<p>
[http://www.hsj.co.uk/leadership/clinical-leaders-2013/5059869.article#.Us6mkvYingo "Clinical Leaders 2013"], ''Health Service Journal'', 14 June 2013.</ref>

==Synopsis==
===Introduction===
Goldacre writes in the introduction of ''Bad Pharma'' that he aims to defend the following:

<blockquote>
Drugs are tested by the people who manufacture them, in poorly designed trials, on hopelessly small numbers of weird, unrepresentative patients, and analysed using techniques which are flawed by design, in such a way that they exaggerate the benefits of treatments. Unsurprisingly, these trials tend to produce results that favour the manufacturer. When trials throw up results that companies don't like, they are perfectly entitled to hide them from doctors and patients, so we only ever see a distorted picture of any drug's true effects. Regulators see most of the trial data, but only from early on in a drug's life, and even then they don't give this data to doctors or patients, or even to other parts of government. This distorted evidence is then communicated and applied in a distorted fashion.

In their forty years of practice after leaving medical school, doctors hear about what works through ad hoc oral traditions, from sales reps, colleagues or journals. But those colleagues can be in the pay of drug companies&nbsp;– often undisclosed&nbsp;– and the journals are too. And so are the patient groups. And finally, academic papers, which everyone thinks of as objective, are often covertly planned and written by people who work directly for the companies, without disclosure. Sometimes whole academic journals are even owned outright by one drug company. Aside from all this, for several of the most important and enduring problems in medicine, we have no idea what the best treatment is, because it's not in anyone's financial interest to conduct any trials at all.<ref>''Bad Pharma'', xi (paragraph break added for ease of reading).</ref>
</blockquote>

===Chapter 1: "Missing Data"===
In "Missing Data," Goldacre argues that the clinical trials undertaken by drug companies routinely reach conclusions favourable to the company. For example, in a 2007 journal article published in [[PLOS Medicine]], researchers studied every published trial on [[statin]]s, drugs prescribed to reduce [[cholesterol]] levels. In the 192 trials they looked at, industry-funded trials were 20 times more likely to produce results that favoured the drug.<ref>''Bad Pharma'', 2; for the study, {{cite journal | last1 = Bero | first1 = L. | display-authors = 1 | last2 = et al | year = 2007| title = Factors Associated with Findings of Published Trials of Drug-Drug Comparisons: Why Some Statins Appear More Efficacious than Others | url = http://www.plosmedicine.org/article/info:doi/10.1371/journal.pmed.0040184 | journal = PLOS Medicine | volume =  4| issue = | pages =  e184| pmid = 17550302 | doi=10.1371/journal.pmed.0040184 | pmc=1885451}}</ref>

He writes that these positive results are achieved in a number of ways. Sometimes the industry-sponsored studies are flawed by design (for example by comparing the new drug to an existing drug at an inadequate dose), and sometimes patients are selected to make a positive result more likely. In addition, the data is analysed as the trial progresses. If the trial seems to be producing negative data it is stopped prematurely and the results are not published, or if it is producing positive data it may be stopped early so that longer-term effects are not examined. He writes that this [[publication bias]], where negative results remain unpublished, is endemic within medicine and academia. As a consequence, he argues, doctors may have no idea what the effects are of the drugs they prescribe.<ref>''Bad Pharma'', 4–7, 12.</ref>

An example he gives of the difficulty of obtaining missing data from drug companies is that of [[oseltamivir]] (Tamiflu), manufactured by [[Hoffmann-La Roche|Roche]] to reduce the complications of [[Influenza A virus subtype H5N1|bird flu]]. Governments spent billions of pounds stockpiling this, based in large part on a meta-analysis that was funded by the industry. ''Bad Pharma'' charts the efforts of independent researchers, particularly [[Tom Jefferson (epidemiologist)|Tom Jefferson]] of the [[Cochrane Collaboration]] Respiratory Group, to gain access to information about the drug.<ref>''Bad Pharma'', 81ff; Tom Jefferson, et al., [http://summaries.cochrane.org/CD008965/a-review-of-unpublished-regulatory-information-from-trials-of-neuraminidase-inhibitors-tamiflu-oseltamivir-and-relenza-zanamivir-for-influenza "A review of unpublished regulatory information from trials of neuraminidase inhibitors (Tamiflu - oseltamivir and Relenza - zanamivir) for influenza"], ''Cochrane Summaries'', 17 October 2012.</ref>

===Chapter 2: "Where Do New Drugs Come From?"===
In the second chapter, the book describes the process as new drugs move from [[animal testing]] through [[Phases of clinical research#Phase I|phase 1]] ([[first-in-man study]]), [[Phases of clinical research#Phase II|phase 2]], and [[Phases of clinical research#Phase III|phase 3]] clinical trials. Phase 1 participants are referred to as volunteers, but in the US are paid $200–$400 per day, and because studies can last several weeks and subjects may volunteer several times a year, earning potential becomes the main reason for participation.<ref>''Bad Pharma'', 104–110.</ref> Participants are usually taken from the poorest groups in society, and outsourcing increasingly means that trials may be conducted in countries with highly competitive wages by [[contract research organization]]s (CROs). The rate of growth for clinical trials in India is 20 percent a year, in Argentina 27 percent, and in China 47 percent, while trials in the UK have fallen by 10 percent a year and in the US by six percent.<ref name=BP113>''Bad Pharma'', 113–114.</ref>

The shift to outsourcing raises issues about data integrity, regulatory oversight, language difficulties, the meaning of [[informed consent]] among a much poorer population, the standards of clinical care, the extent to which corruption may be regarded as routine in certain countries, and the ethical problem of raising a population's expectations for drugs that most of that population cannot afford.<ref name=BP113/> It also raises the question of whether the results of clinical trials using one population can invariably be applied elsewhere. There are both social and physical differences: Goldacre asks whether patients diagnosed with depression in China are really the same as patients diagnosed with depression in California, and notes that people of Asian descent metabolize drugs differently from Westerners.<ref>''Bad Pharma'', 115.</ref>

There have also been cases of available treatment being withheld during clinical trials. [[Abdullahi v. Pfizer, Inc.|In 1996 in Kano, Nigeria]], the drug company [[Pfizer]] compared a new antibiotic during a [[meningitis]] outbreak to a competing antibiotic that was known to be effective at a higher dose than was used during the trial. Goldacre writes that 11 children died, divided almost equally between the two groups. The families taking part in the trial were apparently not told that the competing antibiotic at the effective dose was available from [[Médecins Sans Frontières]] in the next-door building.<ref>''Bad Pharma'', 117.</ref>

===Chapter 3: "Bad Regulators"===
Chapter three describes the concept of "[[regulatory capture]]," whereby a regulator&nbsp;– such as the [[Medicines and Healthcare products Regulatory Agency]] (MHRA) in the UK, or the [[Food and Drug Administration]] (FDA) in the United States&nbsp;– ends up advancing the interests of the drug companies rather than the interests of the public. Goldacre writes that this happens for a number of reasons, including the [[Revolving door (politics)|revolving door]] of employees between the regulator and the companies, and the fact that friendships develop between regulator and company employees simply because they have knowledge and interests in common. The chapter also discusses [[surrogate outcomes]] and [[accelerated approval]], and the difficulty of having ineffective drugs removed from the market once they have been approved.<ref>''Bad Pharma'', 123ff.</ref> He argues that regulators do not require that new drugs offer an improvement over what is already available, or even that they be particularly effective.<ref>''Bad Pharma'', 143ff.</ref>

===Chapter 4: "Bad Trials"===
"Bad Trials" examines the ways in which clinical trials can be flawed. Goldacre writes that this happens by design and by analysis, and that it has the effect of maximizing a drug's benefits and minimizing harm.
There have been instances of fraud, though he says these are rare. More common are what he calls the "wily tricks, close calls, and elegant mischief at the margins of acceptability."<ref>''Bad Pharma'', 171.</ref>

These include testing drugs on unrepresentative, "freakishly ideal" patients; comparing new drugs to something known to be ineffective, or effective at a different dose or if used differently; conducting trials that are too short or too small; and stopping trials early or late.<ref>''Bad Pharma'', 176, 180–187, 191–193.</ref> It also includes measuring uninformative outcomes; packaging the data so that it is misleading; ignoring patients who drop out (i.e. using [[Analysis of clinical trials#Per protocol|per-protocol analysis]], where only patients who complete the trial are counted in the final results, rather than [[intention-to-treat analysis]], where everyone who starts the trial is counted); changing the main outcome of the trial once it has finished; producing [[Subgroup analysis|subgroup analyses]] that show apparently positive outcomes for certain tightly defined groups (such as Chinese men between the ages of 56 and 71), thereby hiding an overall negative outcome; and conducting "[[seeding trial]]s," where the objective is to persuade physicians to use the drug.<ref>''Bad Pharma'', 194–198, 200–212.</ref>

Another criticism is that outcomes are presented in terms of [[relative risk reduction]] to exaggerate the apparent benefits of the treatment. For example, he writes, if four people out of 1,000 will have a heart attack within the year, but on statins only two will, that is a 50 percent reduction if expressed as relative risk reduction. But if expressed as [[absolute risk reduction]], it is a reduction of just 0.2 percent.<ref>''Bad Pharma'', 216–217.</ref>

===Chapter 5: "Bigger, Simpler Trials"===
In chapter five Goldacre suggests using the [[General Practice Research Database]] in the UK, which contains the anonymized records of several million patients, to conduct [[Randomized controlled trial|randomized trial]]s to determine the most effective of competing treatments. For example, to compare two statins, [[atorvastatin]] and [[simvastatin]], doctors would randomly assign patients to one or the other. The patients would be followed up by having data about their cholesterol levels, heart attacks, strokes and deaths taken from their computerized medical records. The trials would not be [[Blind experiment|blind]]&nbsp;– patients would know which statin they had been prescribed&nbsp;– but Goldacre writes that they would be unlikely to hold such firm beliefs about which one is preferable to the extent that it could [[Placebo effect|affect their health]].<ref>''Bad Pharma'', 225–227, 233.<p>
Tjeerd-Pieter van Staa, Ben Goldacre, et al, [http://www.bmj.com/content/344/bmj.e55 "Pragmatic randomised trials using routine electronic health records: putting them to the test"], ''British Medical Journal'', 7 February 2012. {{PMID|22315246}}</ref>

===Chapter 6: "Marketing"===
In the final chapter, Goldacre looks at how doctors are persuaded to prescribe "me-too drugs," brand-name drugs that are no more effective than significantly cheaper off-patent ones. He cites as examples the statins atorvastatin (Lipitor, made by Pfizer) and simvastatin (Zocor), which he writes seem to be equally effective, or at least there is no evidence to suggest otherwise. Simvastatin came off patent several years ago, yet there are still three million prescriptions a year in the UK for atorvastatin, costing the [[National Health Service]] (NHS) an annual £165 million extra.<ref>''Bad Pharma'', 243.</ref>

He addresses the issue of [[medicalization]] of certain conditions (or, as he argues, of personhood), whereby pharmaceutical companies "widen the boundaries of diagnosis" before offering solutions. [[Female sexual dysfunction]] was highlighted in 1999 by a study published in the ''Journal of the American Medical Association'', which alleged that 43 percent of women were suffering from it. After the article appeared, the ''New York Times'' wrote that two of its three authors had worked as consultants for Pfizer, which at the time was preparing to launch [[UK-414,495]], known as female [[Viagra]]. The journal's editor said that the failure to disclose the relationship with Pfizer was the journal's mistake.<ref>''Bad Pharma'', 261–262.<p>
For the 1999 study, E. O. Laumann, et al, [http://www.ncbi.nlm.nih.gov/pubmed/10022110 "Sexual dysfunction in the United States: prevalence and predictors"], ''Journal of the American Medical Association'', 281(6), 10 February 1999, 537–544. {{PMID|10022110}}<p>
Denise Grady, [https://www.nytimes.com/1999/02/14/weekinreview/the-nation-better-loving-through-chemistry-sure-we-ve-got-a-pill-for-that.html "Sure, We've Got a Pill for That"], ''The New York Times'', 14 February 1999.</ref>

The chapter also examines celebrity endorsement of certain drugs, the extent to which claims in advertisements aimed at doctors are appropriately sourced, and whether [[direct-to-consumer advertising]] (currently permitted in the US and New Zealand) ought to be allowed.<ref>''Bad Pharma'', 247, 251, 271.</ref> It discusses how PR firms promote stories from patients who complain in the media that certain drugs are not made available by the funder, which in the UK is the NHS and the [[National Institute for Health and Clinical Excellence]] (NICE). Two breast-cancer patients who campaigned in the UK in 2006 for [[trastuzumab]] (Herceptin) to be available on the NHS were being handled by a law firm working for [[Hoffmann-La Roche|Roche]], the drug's manufacturer. The historian [[Lisa Jardine]], who was suffering from breast cancer, told the ''Guardian'' that she had been approached by a PR firm working for the company.<ref>''Bad Pharma'', 254; Sarah Boseley, [https://www.theguardian.com/science/2006/mar/29/medicineandhealth.health "The selling of a wonder drug"], ''The Guardian'', 29 March 2006.</ref>

The chapter also covers the influence of [[Pharmaceutical sales representative|drug reps]], how ghostwriters are employed by the drug companies to write papers for academics to publish, how independent the academic journals really are, how the drug companies finance doctors' continuing education, and how patients' groups are often funded by industry.<ref>''Bad Pharma'', 274, 287, 303, 311.</ref>

===Afterword: "Better Data"===
In the afterword and throughout the book, Goldacre makes suggestions for action by doctors, medical students, patients, patient groups and the industry. He advises doctors, nurses and managers to stop seeing drug reps, to ban them from clinics, hospitals and medical schools, to declare online and in waiting rooms all gifts and hospitality received from the industry, and to remove all drug company promotional material from offices and waiting rooms. (He praises the website of the [[American Medical Student Association]] – www.amsascorecard.org – which ranks institutions according to their [[Conflict of interest|conflict-of-interest]] policies, writing that it makes him "feel weepy.") He also suggests that regulations be introduced to prevent pharmacists from sharing doctors' prescribing records with drug reps.<ref>''Bad Pharma'', 284–286, 339–340; [http://www.amsascorecard.org/ AMSA Scorecard], American Medical Students Association.</ref>

He asks academics to lobby their universities and academic societies to forbid academics from being involved in ghostwriting, and to lobby for "film credit" contributions at the end of every academic paper, listing everyone involved, including who initiated the idea of publishing the paper.<ref>''Bad Pharma'', 302.</ref> He also asks for full disclosure of all past clinical trial results, and a list of academic papers that were, as he puts it, "rigged" by industry, so that they can be retracted or annotated.<ref>''Bad Pharma'', 350–351.</ref> He asks drug company employees to become whistleblowers, either by writing an anonymous blog, or by contacting him.<ref>''Bad Pharma'', 363–364.</ref>

He advises patients to ask their doctors whether they accept drug-company hospitality or sponsorship, and if so to post details in their waiting rooms, and to make clear whether it is acceptable to the patient for the doctor to discuss his or her medical history with drug reps. Patients who are invited to take part in a trial are advised to ask, among other things, for a written guarantee that the trial has been publicly registered, and that the main outcome of the trial will be published within a year of its completion. He advises patient groups to write to drug companies with the following: "We are living with this disease; is there anything at all that you're withholding? If so, tell us today."<ref name=BP357>''Bad Pharma'', 357–358.</ref>

==Reception==
The book was generally well received. The ''Economist'' described it as "slightly technical, eminently readable, consistently shocking, occasionally hectoring and unapologetically polemical".<ref>[http://www.economist.com/node/21563689 "Pick your pill out of a hat"], ''The Economist'', 29 September 2012.</ref> [[Helen Lewis (journalist)|Helen Lewis]] in the ''New Statesman'' called it an important book,<ref>Helen Lewis, [http://www.newstatesman.com/culture/books/2012/10/lies-damn-lies-and-drug-trials "Lies, damn lies and drug trials"], ''New Statesman'', 4 October 2012.</ref> while Luisa Dillner, writing in the ''Guardian'', described it as a "thorough piece of investigative medical journalism".<ref name=Dillner/>

Andrew Jack wrote in the ''Financial Times'' that Goldacre is "at his best in methodically dissecting poor clinical trials.&nbsp;... He is less strong in explaining the complex background reality, such as the general constraints and individual slips of regulators and pharma companies' employees." Jack also argued that the book failed to reflect how many lives have been improved by the current system, for example with new treatments for HIV, rheumatoid arthritis and cancer.<ref>Andrew Jack, [http://www.ft.com/cms/s/0/ca5bb27c-07ba-11e2-9df2-00144feabdc0.html#axzz29vEV06HZ "Extremes of drug development dissected"], ''Financial Times'', 26 September 2012.</ref>

[[Max Pemberton (doctor)|Max Pemberton]], a psychiatrist, wrote in the ''Daily Telegraph'' that "this is a book to make you enraged&nbsp;... because it's about how big business puts profits over patient welfare, allows people to die because they don't want to disclose damning research evidence, and the tricks they play to make sure doctors do not have all the evidence when it comes to appraising whether a drug really works or not."<ref>[[Max Pemberton (doctor)|Max Pemberton]], [http://www.telegraph.co.uk/culture/books/bookreviews/9617550/Bad-Pharma-by-Ben-Goldacre-review.html "Bad Pharma by Ben Goldacre: review"], ''The Daily Telegraph'', 22 October 2012.</ref>

The [[Association of the British Pharmaceutical Industry]] (ABPI) replied in the ''New Statesman'' that Goldacre was "stuck in a bygone era where pharmaceutical companies wine and dine doctors in exchange for signing on the dotted line".<ref>[http://www.newstatesman.com/sci-tech/sci-tech/2012/10/dr-ben-goldacre-vs-association-british-pharmaceutical-industry "Dr Ben Goldacre vs the Association of the British Pharmaceutical Industry"], ''New Statesman'', 19 October 2012.</ref> The ABPI issued a press release, writing that the pharmaceutical industry is responsible for the discovery of 90 percent of all medicines, and that it takes an average of 10–12 years and £1.1bn to introduce a medicine to the market, with just one in 5,000 new compounds receiving regulatory approval. This makes research and development an expensive and risky business. They wrote that the industry is one of the most heavily regulated in the world, and is committed to ensuring full transparency in the research and development of new medicines. They also maintained that the examples Goldacre offered were "long documented and historical, and the companies concerned have long addressed these issues".<ref name=ABPI/> Goldacre argues in the book that "the most dangerous tactic of all is the industry's enduring claim that these problems are all in the past".<ref>''Bad Pharma'', 343.</ref>

Humphrey Rang of the [[British Pharmacological Society]] wrote that Goldacre had chosen his target well and had produced some shocking examples of secrecy and dishonesty, particularly the nondisclosure of data on the antidepressant [[reboxetine]] (chapter one), in which only one trial out of seven was published (the published study showed positive results, while the unpublished trials suggested otherwise). He argued that Goldacre had gone "over the top" in devoting a whole chapter (chapter five) to recommending large clinical trials using electronic patient data from general practitioners, without fully pointing out how problematic these can be; such trials raise issues, for example, about informed consent and regulatory oversight. Rang also criticized Goldacre's style, describing the book as too long, repetitive, hyperbolic, and in places too conversational. He particularly objected to the line, "medicine is broken", calling it a "foolish remark".<ref>Humphrey Rang, [http://onlinelibrary.wiley.com/doi/10.1111/bcp.12047/abstract "Book review: Bad Pharma"], ''British Journal of Clinical Pharmacology'', 75(5), 17 October 2012, 1377–1379.</ref>

==AllTrials==
Following the book's publication, Goldacre co-founded [[AllTrials]] with David Tovey, editor-in-chief of the [[Cochrane Library]], together with the ''[[British Medical Journal]]'', the Centre for Evidence-based Medicine, and others in the UK, and [[Dartmouth College]]'s [[Geisel School of Medicine]] and the [[Dartmouth Institute for Health Policy and Clinical Practice]] in the US. Set up in January 2013, the group campaigns for all past and current clinical trials to be registered and reported, for all treatments in use.<ref name=alltrials/>

The British House of Commons [[Public Accounts Committee (United Kingdom)|Public Accounts Committee]] produced a report in January 2014, after hearing evidence from Goldacre, [[Fiona Godlee]], editor-in-chief of the ''British Medical Journal'', and others, about the stockpiling of Tamiflu and the withholding of data about the drug by its manufacturer, Roche. The committee said it was "surprised and concerned" to learn that information from clinical trials is routinely withheld from doctors, and recommended that the Department of Health take steps to ensure that all clinical-trial data be made available for currently prescribed treatments.<ref>[http://www.publications.parliament.uk/pa/cm201314/cmselect/cmpubacc/295/29502.htm "Access to clinical trial information and the stockpiling of Tamiflu"], Public Accounts Committee, 3 January 2014.<p>
Charlie Cooper, [http://www.independent.co.uk/life-style/health-and-families/health-news/drugs-firms-routinely-withhold-results-of-medical-trials-from-doctors-researchers-and-patients-9035740.html "Drugs companies 'routinely withhold results of medical trials' from doctors, researchers and patients"], ''The Independent'', 3 January 2014.</ref>

==Publication details==
*''Bad Pharma: How drug companies mislead doctors and harm patients'', Fourth Estate, 2012 (UK). {{ISBN|978-0-00-735074-2}}
*Faber and Faber, 2013 (US). {{ISBN|978-0-86547-800-8}}
*Signal, 2013 (Canada). {{ISBN|978-0-7710-3629-3}}
*As of December 2012 foreign rights had been sold for Brazil, the Czech Republic, Netherlands, Germany, Israel, Italy, Korea, Norway, Poland, Portugal, Spain and Turkey.<ref>[http://uaforeignrights.tumblr.com/post/38305141435/best-books-of-2012 "Best Books of 2012"], ''United Agents Foreign Rights, 19 December 2012.</ref>

==See also==
{{col-begin}}
{{col-break}}
{{Portal|Medicine}}
;Books
* ''[[Big Pharma (book)|Big Pharma]]'' (2006) by Jacky Law
* ''[[Side Effects (Bass book)|Side Effects]]'' (2008) by [[Alison Bass]]
* ''[[Anatomy of an Epidemic]]'' (2010) by [[Robert Whitaker (author)|Robert Whitaker]]

;Lists
* [[Lists about the pharmaceutical industry]]
* [[List of books about the politics of science]]
* [[List of pharmaceutical companies]]
* [[List of largest pharmaceutical settlements]]
{{col-break}}

;Miscellaneous
* [[Ethics in pharmaceutical sales]]
* [[Pharmaceutical fraud]]
* [[Pharmaceutical industry in the United Kingdom|Pharmaceutical industry in the UK]]
* [[GlaxoSmithKline#2012 criminal and civil settlement]]
* [[Rosiglitazone]]
* [[Study 329]]
* [[TGN1412]]
{{col-end}}

==Notes==
{{reflist|group=n}}

==References==
{{reflist|26em}}

==Further reading==
;External links
*[http://www.harpercollins.co.uk/Titles/52872/bad-pharma-ben-goldacre-9780007350742 Bad Pharma], publisher's website.
*[http://www.badscience.net/ badscience.net], Ben Goldacre's website.
*[https://www.theguardian.com/science/series/badscience?INTCMP=SRCH "Bad Science"], Goldacre's column for ''The Guardian''.
*[http://www.abc.net.au/unleashed/4323556.html "Why doctors don't know what they're prescribing"], extract from ''Bad Pharma''.

;Articles and radio
{{refbegin|2|normalfont=yes}}
*BBC Radio 4. [http://news.bbc.co.uk/today/hi/today/newsid_9754000/9754505.stm "Pharmaceutical regulators have been 'unethical'"], ''Today'' programme, 25 September 2012 (radio interview with Ben Goldacre and Stephen Whitehead of the Association of the British Pharmaceutical Industry).
*Brice, Makini. [http://www.medicaldaily.com/articles/12403/20120928/pharmaceutical-companies-cherry-pick-data-drug-approval.htm "Pharmaceutical Companies Cherry Pick Data for Drug Approval, Sweep Bad Results Under the Rug"], ''Medical Daily'', 28 September 2012.
*Burke, Maria. [http://www.rsc.org/chemistryworld/2012/10/gsk-clinical-trials-data-published-transparency "GSK pledge on trials transparency"], ''Chemistry World'', 17 October 2012.
*[[Daniel Carlat|Carlat, Daniel]]. [https://www.nytimes.com/2007/11/25/magazine/25memoir-t.html?_r=1 "Dr. Drug Rep"], ''New York Times'' magazine, 25 November 2007.
*Goldacre, Ben. [http://www.bmj.com/content/339/bmj.b4949?ij "Is the conflict of interest unacceptable when drug companies conduct trials on their own drugs? Yes"], ''British Medical Journal'', 29 November 2009.
*Goldacre, Ben. [http://www.thetimes.co.uk/tto/opinion/columnists/article3576313.ece "Drug companies must publish all trial results"], ''The Times'', 23 October 2012; [http://www.thetimes.co.uk/tto/science/medicine/article3576357.ece "Calls to end ‘national scandal’ of stifled clinical trial results"], ''The Times'' health editor, 23 October 2012.
*Haynes, Laura; Service, Owain; Goldacre, Ben; Torgerson, David. [http://www.cabinetoffice.gov.uk/sites/default/files/resources/TLA-1906126.pdf "Test, Learn, Adapt: Developing Public Policy with Randomised Controlled Trials"], Cabinet Office [[Behavioural Insights Team]] (UK), June 2012.
*Hennessy, Mark. [http://www.irishtimes.com/newspaper/weekend/2012/0929/1224324589266.html "Putting the drug companies' research to the test"], ''The Irish Times'', 29 September 2102.
*McClenaghan, Maeve. [http://www.thebureauinvestigates.com/2012/09/28/why-big-pharma-is-bad-for-your-health/ "Why Big Pharma is bad for your health"], Bureau of Investigative Journalism, 28 September 2012.
*Rehman, Jalees. [http://blogs.scientificamerican.com/guest-blog/2012/09/23/can-the-source-of-funding-for-medical-research-affect-the-results/ "Can the Source of Funding for Medical Research Affect the Results?"], ''Scientific American'', 23 September 2012.
*[[Adam Rutherford|Rutherford, Adam]]. [http://www.nature.com/nature/podcast/index-goldacre-2012-09-28.html "Podcast Extra: Ben Goldacre"], ''Nature'', 28 September 2012.
*Szalavitz, Maia. [http://healthland.time.com/2012/09/24/a-doctors-dilemma-when-crucial-new-drug-data-is-hidden/ "A Doctor’s Dilemma: When Crucial New-Drug Data Is Hidden"], ''Time'' magazine, 24 September 2012.
*Tucker, Ian. [https://www.theguardian.com/business/2012/oct/07/ben-goldacre-bad-pharma-interview "Ben Goldacre: 'It's appalling ... like phone hacking or MPs' expenses'"], ''The Observer'', 7 October 2012.
{{refend}}

{{conflict of interest}}
{{Pharmaceutical and biotechnology industry in the United Kingdom}}
{{Pharmaceutical industry in the United States}}
{{Pharmaceutical companies of the United States}}
{{Pharmaceutical industry by country}}
{{Public health}}
{{Medicine}}
{{Good article}}

[[Category:2012 non-fiction books]]
[[Category:Books about the politics of science]]
[[Category:Books by Ben Goldacre]]
[[Category:British books]]
[[Category:HarperCollins books]]
[[Category:Medical books]]
[[Category:Pharmaceutical industry]]
[[Category:Science books]]