{{underlinked|date=January 2016}}
{{Infobox person
| name       = Stanley K. Bernstein
| image =
| caption    =
| birth_date =
| birth_place = [[Toronto]], [[Ontario]], Canada
| nationality = [[Canadians|Canadian]]
| alma_mater = [[University of Toronto]] <small>(MD)</small>
| occupation = Physician
| spouse     = Judy Bernstein
| children   =
}}

'''Stanley K. Bernstein''' is a [[Canadians|Canadian]] physician who founded and is current owner of 60 [[weight-loss]] clinics in Canada and the United States, known collectively as the Dr. Bernstein Diet & Health Clinics.  Bernstein has owned and operated weight-loss clinics for more than 35 years <ref name=kari>{{cite news|url= http://www.theglobeandmail.com/news/toronto/diet-doctor-bernstein-sues-former-partner/article596118/ |title=Diet doctor Bernstein sues former partner|date=September 30, 2011 |newspaper=[[The Globe and Mail]] |accessdate=May 7, 2014 | first=Shannon | last=Kari}}</ref> and employs a weight-loss regimen involving a low calorie intake, frequent physician visits and injections of a Vitamin B solution.  Bernstein is a member of the American Society of Bariatric Physicians.

== Early life and education ==

Bernstein was raised near the [[Moss Park]] area of Toronto by parents who owned a used clothing outlet called Bernstein’s Second Hand Store on Queen East.<ref name=torontolife>{{cite news |title= The Toronto Diet|date=March 2010 |newspaper=[[Toronto Life]] |accessdate= May 3, 2014 | first=Robert | last=Hough}}</ref> Bernstein graduated from the [[University of Toronto]] in 1966 and followed this by interning at [[Mount Sinai Hospital, Toronto|Mount Sinai Hospital]] in Toronto.<ref name=torontolife />

== Career ==

=== Establishing the Dr. Bernstein Diet & Health Clinics ===

After completing his internship, Bernstein set up a general family practice in 1967 near Gerrard and Greenwood in Toronto,<ref name=torontolife /> and was also involved in geriatric work at Baycrest Centre.

In 1972, Bernstein traveled to Las Vegas to attend a conference hosted by the American Society of Bariatric Physicians.<ref name=torontolife /> It was at this conference where Bernstein learned about a group of physicians who were using what was called the Simeons Approach as a way to help patients achieve weight loss.  In order to achieve weight loss, the Simeons Approach prescribed a very low calorie intake, frequent physician visits on the part of the patient and patient injections of a hormone called Human Chorionic Gonadotropin, or hCG, a hormone taken from a live human body.<ref name=star />

Bernstein returned to Toronto and began using this knowledge to treat patients at his practice.<ref name=torontolife />  Subsequently, Bernstein opened his first weight-loss clinic at the Richmond-Adelaide Centre of Toronto in 1977 and in 1978, his second clinic at Yonge and Eglinton, also in Toronto.<ref name=torontolife />  By the late 1990s, Bernstein had 25 diet clinics in Ontario, along with a clinic in Vancouver, as well as clinics in British Columbia and in the Prairie provinces of Canada. Around 2005, Bernstein opened his first American clinics in Virginia and Florida.<ref name=torontolife />

As of 2014, Bernstein owns 60 clinics across Canada and the United States.<ref name=national />

It is estimated that Bernstein's clinics have treated approximately 450,000 patients over the years, including Canadian Archbishop Marcel Gervais and comedian [[Mike Bullard (comedian)|Mike Bullard]], who served as the clinic's spokesman for a period of time.<ref name=perrin>{{cite news|url= https://groups.yahoo.com/neo/groups/raw1/conversations/topics/25?var=1| title=The Bernstein Diet: How much weight should you give celebrity endorsements?|date=April 3, 2002 |newspaper=CBC Marketplace |accessdate=May 5, 2014 | first=Jacquie | last=Perrin}}</ref>

=== Use of hCG ===

Starting in 1979, the Ontario College of Physicians and Surgeons warned against the use of hCG in the treatment of obesity due to the lack of scientific evidence concerning the effects of its use.<ref name=star />  The Ontario College also warned against a 500-calorie diet used in conjunction with hCG treatment, citing the treatment's risk to a patient's health.<ref name=star>{{cite news|title=Every weekday, across Metro and beyond, hundreds of patients stream into the offices of Dr. Stanley K. Bernstein, drop their drawers and brace themselves for a shot of vitamin B in the backside.|date=May 27, 1990 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Janice | last=Turner}}</ref>
Several doctors, including Bernstein, continued to administer and use hCG in their weight-loss treatments.  In 1987, the Ontario College formally banned hCG as a weight-loss treatment.<ref name=star />

=== Use of vitamin B ===

Following the ban on hCG, Bernstein discontinued using hCG injections as part of his diet program and replaced it with injections of a Vitamin B complex, the exact composition of which is a closely guarded secret.  While on Bernstein's diet program, patients are administered Vitamin B injections three times a week and are instructed to maintain an intake of 800 to 1,500 calories per day on a strict diet regimen.<ref name=perrin />

=== Canadian Medical Association Journal editorial ===

In February 2009, Bernstein publicly complained that an editorial in the [[Canadian Medical Association Journal]] (CMAJ) concerning commercial diets unfairly targeted his clinics.<ref name=canada>{{cite news|url= http://www2.canada.com/story.html?id=1295488 |title=Weight-loss clinics with 'fantastical' claims duping Canadians|date=February 16, 2009 |newspaper= canada.com |accessdate=May 7, 2014 | first=Sharon | last=Kirkey}}</ref>  CMAJ's editorial commented on commercial diet clinics and specifically their pattern of manipulating patients with false hopes and using medically unproven weight-loss techniques. As evidence of the latter, CMAJ's editorial specifically pointed out the use of Vitamin B injections.<ref name=canada />

Bernstein's use of Vitamin B injections as part of his weight-loss program has met with skepticism and a lack of agreement by multiple members of the medical community.<ref name=star />

=== Glenn Duffin controversy ===

In 1988, Glenn Duffin, a thirty-one-year-old Canadian who had been a patient of Bernstein's died of cardiac arrhythmia eight days after beginning Bernstein's diet program.<ref name=perrin />  Following Duffin's death, a jury concluded that his death was caused by stress brought about by family and financial issues and Bernstein's diet program.  The jury also advised that patients using commercial weight-loss programs should be encouraged to lose no more than two pounds a week when considering a diet of 1,200 calories per day.<ref name=turner2>{{cite news|title=Licensing group to review findings of diet-death probe|date=May 30, 1990 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Janice | last=Turner}}</ref>
Duffin's family filed a malpractice suit against Bernstein and was awarded $700,000 in damages in an out-of-court settlement.<ref name=torontolife />

=== Litigation against medical colleagues ===

Bernstein has been a part of a number of litigation suits involving the proprietary secrets of his weight-loss clinics and his Vitamin B-based weight-loss program.  In 2007, Bernstein brought to court Daniela Stoytcheva-Todorova and Vesselin Todorov under allegations that their weight-loss clinic, Veda Healthy Weight Loss Centre Inc., used trade secrets and confidential information belonging to Bernstein.  Bernstein ended up discontinuing all claims against Todorov and Todorova.<ref name=todorova>{{cite court|litigants=Bernstein et al v. Stoytcheva-Todorova et al|date=January 5, 1997 |url=http://www.canlii.org/en/bc/bcsc/doc/2007/2007bcsc14/2007bcsc14.html?searchUrlHash=AAAAAQARc3RhbmxleSBiZXJuc3RlaW4AAAAAAQ |accessdate=May 5, 2014}}</ref>
In 2009, Bernstein sought litigation against Dr. Nadia Brown under similar claims,<ref name=brown>{{cite court|litigants=Bernstein v. Brown|date= September 17, 2009 |url=https://www.canlii.org/en/on/onsc/doc/2009/2009canlii58975/2009canlii58975.html?searchUrlHash=AAAAAQARc3RhbmxleSBiZXJuc3RlaW4AAAAAAQ |accessdate=May 5, 2014}}</ref> and, following this, in 2011, Bernstein filed suit against Dr. Scott Seagrist and his spouse, seeking $10.5 million in damages under allegations that Seagrist had improperly used trade secrets to begin his own weight-loss clinic.  Bernstein's lawsuit against Seagrist was settled in an out-of-court agreement.<ref name=seagrist>{{cite news|url= http://www.thestar.com/news/crime/2011/10/05/diet_doctor_claims_expartner_divulged_trade_secrets.html |title=‘Diet doctor’ claims ex-partner divulged trade secrets|date=October 5, 2011 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Peter | last=Small}}</ref>

=== Litigation against Dr. Pat Poon ===

In September 2009, Bernstein launched a lawsuit against Dr. Pat Poon, a [[Canadians|Canadian]] weight-loss doctor who operates four weight-loss clinics in the Greater Toronto Area.<ref name=star5>{{cite news|url= http://www.thestar.com/news/gta/2015/01/23/bernstein-diet-lawsuit-ends-with-anticlimactic-10000-ruling.html |title= Bernstein Diet lawsuit ends with "anticlimactic" $10,000 ruling|date=January 23, 2015 |newspaper=[[Toronto Star]] |accessdate=January 17, 2016 | first=Alex | last=Ballingall}}</ref> In the lawsuit, Bernstein argued that Poon had defamed Bernstein in Poon’s Dr. Poon’s Metabolic Diet book, as well as in a subsequent television show that Poon was a guest on.

After a six-year lawsuit, the judge ruled in Bernstein’s favor, awarding $10,000 to Bernstein in damages.<ref name=cbc3>{{cite news|url= http://www.cbc.ca/news/canada/toronto/diet-doctor-defamation-suit-more-about-ego-than-injury-court-rules-1.2926839 |title=Diet doctor defamation suit 'more about ego than injury,' court rules|date=January 21, 2015 |newspaper= CBC News |accessdate=January 16, 2016 | first=Diana | last=Mehta}}</ref>   In making the final decision, the judge admitted that the trial was ‘more about ego than injury’, saying, “It is more about turf warfare in the competitive world of diet medicine than about reputation”.<ref name=star5 /> The judge also questioned the ‘substantial’ public resources used in conducting the trial, saying, “It is for others to decide whether the substantial public resources that have been made available to enable this dispute to be adjudicated are proportionate to the rights and interests that were at stake.” <ref name=star5 />
 
=== Litigation against real estate investment partners ===

In 2013, Bernstein filed suit against The Rose and Thistle Group owners and Toronto attorneys, Norma and Ronauld Walton, under claims that the Waltons had diverted assets from real estate investments that Bernstein and the Waltons had jointly invested in.<ref name=dempsey>{{cite news|url= http://www.thestar.com/news/gta/2013/12/14/toronto_diet_doctor_stanley_bernstein_lost_millions_to_theft_judge.html |title=Toronto diet doctor Stanley Bernstein lost millions to ‘theft’: Judge|date=December 14, 2013 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Amy | last=Dempsey}}</ref>  The Waltons have communicated publicly that they "strongly disagree" with the accusations and have appealed the findings of Ontario Superior Court's ordered investigation.<ref name=dempsey />

=== Formal caution by CPSO over advertising ===

In April 2014, the College of Physicians and Surgeons of Ontario ruled that Bernstein had repeatedly breached rules established by the College by advertising his weight-loss clinics through customer testimonials, before and after photos and medically unproven, superlative language.<ref name=national />  The College also ruled that Bernstein had acted against medical standards by connecting himself with the sale of his product, in this case, his diet clinics.  As a result, Bernstein was instructed to appear before the College for a verbal warning.<ref name=national>{{cite news|url= http://news.nationalpost.com/2014/04/27/weight-loss-industry-giant-dr-stanley-bernstein-violating-laws-on-physician-advertising-regulator-says/ |title=Weight-loss industry giant Dr. Stanley Bernstein violating laws on physician advertising, regulator says|date=April 27, 2014 |newspaper=[[National Post]] |accessdate=May 5, 2014 | first=Tom | last=Blackwell}}</ref>

In May, 2014, it was reported that Bernstein was set to contest the formal caution imposed on him by the College of Physicians and Surgeons.<ref name=citynews>{{cite news|url= http://www.citynews.ca/2014/05/08/formal-caution-over-dr-bernstein-weight-loss-advertising-claims-contested/ |title=Formal 'caution' over Dr. Bernstein weight-loss advertising claims contested|date=May 8, 2014 |newspaper=CityNews |accessdate=January 17, 2016 }}</ref>

In February 2015, the Health Professions Appeal and Review Board dismissed a protest from Bernstein and his request that a formal caution on his use of advertising be thrown out.<ref name=review2>{{cite news|url= http://news.nationalpost.com/news/canada/review-board-dismisses-protest-by-dr-stanley-bernstein-key-player-in-weight-loss-industry-over-formal-caution |title=Review board dismisses protest by Dr. Stanley Bernstein, key player in weight loss industry, over formal caution|date=February 28, 2015 |newspaper=[[National Post]] |accessdate=January 16, 2016 | first=Colin | last=Perkel}}</ref> The appeal board found that previous investigations into the complaint that Bernstein had violated advertising rules were “adequate and reasonable”.  The appeal board requested that further investigation be done into whether Bernstein was guilty of “steering” members of the public to visit doctors in his own clinics, which is a violation of rules.<ref name=review2 />

== Personal life ==

Bernstein has been married to his second wife, Judy Bernstein, since 1992.<ref name=torontolife />  Bernstein also has three adult sons, one who is a radiologist in New York City, one a lawyer, and the third son, a business graduate who works at Bernstein's clinics. Bernstein enjoys to ski in his free time.<ref name=torontolife />

=== Professional controversy ===

In 1977, Bernstein was instructed to testify before the Discipline Committee of the College of Physicians and Surgeons of Ontario in allegations that he had inappropriate sexual relations with Jo-Anne Johnston, who was mentally fragile and a patient of his from 1970 to 1973.  The Committee concluded that Bernstein was guilty of the charge and disciplined Bernstein with a suspension from practice for 12 months, as well as a fine of $4,500.<ref name=oleary>{{cite court|litigants= Bernstein and College of Physicians and Surgeons of
Ontario|date= February 11, 1977 
NOTE THAT THIS FINDING WAS APPEALED SUCCESSFULLY AND THE GUILTY CHARGE AND FINE AND SUSPENSION WERE ALL REMOVED.
check the College files to confirm
|url=http://www.canlii.org/en/on/onsc/doc/1977/1977canlii1072/1977canlii1072.html?searchUrlHash=AAAAAQARc3RhbmxleSBiZXJuc3RlaW4AAAAAAQ |accessdate=May 6, 2014}}</ref>

=== Criminal controversies ===

In 1992, Bernstein, along with two other participants, Peter Gassyt and Arnold Markowitz, was arrested and charged in a conspiracy to murder a business colleague and a friend.<ref name=edwards>{{cite news|title=Bail hearing continues for charged doctor|date=July 22, 1992 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Jim | last=Wilkes}}</ref>   At the time of Bernstein's arrest, it was found that he was carrying $18,000 on his person.  In the trial that followed, both Gassyt and Markowitz were charged with a conspiracy to murder, and in 1993, the two were convicted on a sole count of conspiracy to commit murder.<ref name=eyeweekly>{{cite news |title= Toronto Police Farce: One Hot Mess|date=November 2010 |newspaper= Eye Weekly |accessdate= May 3, 2014 | first=Derek | last=Finkle}}</ref>
At the time of Bernstein's arrest, undercover officers undertook an unannounced search of Bernstein's residence, where they found $2 million in stolen goods, including 24 Rolex watches.<ref name=seagrist />  As a result, along with conspiracy to murder, Bernstein was also charged with possession of stolen property.  Both charges which were later dropped.<ref name=seagrist />

In 1998, Bernstein, along with his wife, were robbed at gunpoint in their Toronto residence and bound and forced into the trunk of Bernstein's Mercedes.<ref name=wilkes /> The assailants emptied Bernstein's safe of valuables and fled.  Both Bernstein and his wife were unharmed in the robbery and the assailants were never captured.<ref name=wilkes>{{cite news|title=Doctor stuffed in trunk during home invasion|date=July 9, 1998 |newspaper=[[Toronto Star]] |accessdate=May 5, 2014 | first=Jim | last=Wilkes}}</ref>

== References ==

{{Reflist}}

{{DEFAULTSORT:Bernstein, Stanley K.}}
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]
[[Category:Canadian physicians]]
[[Category:University of Toronto alumni]]