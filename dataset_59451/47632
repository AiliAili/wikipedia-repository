{{Infobox road
|country=LKA
|image=
|type=E
|route=02
|alternate_name =Arthur C. Clarke Expressway
|maint=the [[Road Development Authority]]
|map=
|map_alt=
|map_notes=
|map_custom=
|length_km=29
|length_round=
|length_ref=
|length_notes=
|history=
|decommissioned=
|direction_a=North
|terminus_a= [[Kerawalapitiya]] - connect with [[E03 expressway (Sri Lanka)|{{Expressway code (Sri Lanka)|03}} Colombo - Katunayake Expressway]] (not open yet)
|beltway_city=[[Colombo]] 
|junction= Interchange 2 → [[Mattumagala]] <br> Interchange 3 →[[Kadawatha]]<br> Interchange 4 → [[Kaduwela, Western Province|Kaduwela]]<br> Interchange 5 → [[Athurugiriya]] <ref name="sundayobserver.lk">{{cite news|url=http://www.sundayobserver.lk/2013/03/03/fea05.asp|title=Work on Outer Circular Highway to be completed year-end|newspaper=[[Sunday Observer (Sri Lanka)|Sunday Observer]]|date=3 March 2013|last=Wijayapala|first=Ranil|accessdate=1 November 2013}}</ref>
|direction_b=South
|terminus_b=[[Kottawa]], [[Colombo]] - Start of [[E01 expressway (Sri Lanka)|{{Expressway code (Sri Lanka)|01}} Colombo - Matara Expressway]]
}}

The '''Colombo Outer Circular Expressway''' (also known as the '''Outer Circular Highway'''(OCH),  '''Colombo Inter-provincial Orbital Router''' or the '''Arthur C. Clarke Expressway'''<ref>{{cite news|url=http://www.dailynews.lk/2011/08/16/fea14.asp|title=A Speedy and safe journey to Galle|newspaper=[[Daily News (Sri Lanka)|Daily News]]|date=16 August 2011|last=Guruge|first=Hemanthi|accessdate=1 November 2013}}</ref>) is a highway in [[Colombo]], [[Sri Lanka]]. It was opened in sections, with the latest one between [[Kaduwela, Western Province|Kaduwela]] and [[Kadawatha]] having been opened on 17 September 2015.<ref>{{cite news|url=http://www.lankabusinessonline.com/section-of-colombo-outer-circular-expressway-to-open-sept-17-jica/|title=Section of Colombo Outer Circular Expressway to open Sept 17|newspaper=[[Lanka Business Online (Sri Lanka)|LBO]]|date=15 September 2013|accessdate=17 September 2015}}</ref><ref>{{cite news|title=Outer Circular Highway from Kaduwela to Kadawatha to be opened|url=http://www.news.lk/news/business/item/8000-outer-circular-highway-from-kaduwela-to-kadawatha-to-be-opened}}</ref> The {{convert|29|km|mi|abbr=on}} long outer circular road network links the [[E01 expressway (Sri Lanka)|Colombo - Matara Expressway]] with [[E03 expressway (Sri Lanka)|Colombo - Katunayake Expressway]] and the proposed [[E04 expressway (Sri Lanka)|Colombo - Kandy Expressway]] and will provide an orbital beltway to bypass the city of Colombo and reduce traffic congestion. The project is funded by the [[Japan International Cooperation Agency]] (JICA) <ref>{{cite news|url=http://www.colombopage.com/archive_091/Aug1251387596CH.html|title=Project to build a Beltway around Sri Lanka Capital given to Chinese Company|newspaper=Colombo Page|date=27 August 2009|accessdate=1 November 2013}}</ref>

== History ==
The pre-feasibility was study carried out in 1992, five alternative traces have been studied.In 1999 a Feasibility Study was carried out under grant aid assistance from government of Japan.  After the Environmental Impact Assessment was conducted the project approval was obtained from Central Environmental Authority for the section from Kerawelapitiya to Kottawa for a length of 28&nbsp;km.Detailed Design Study for this 28&nbsp;km stretch was commenced in July 2001 under a grant aid assistance from Government of Japan.  Basic Design of the project has been carried out.<ref name=":0">{{Cite web|title=Spillburg Holdings  Private Limited |url=http://www.spillburg.com/circular.html |website=www.spillburg.com |accessdate=2015-09-18 |deadurl=yes |archiveurl=https://web.archive.org/web/20150805015617/http://www.spillburg.com:80/circular.html |archivedate=2015-08-05 |df= }}</ref>

The project was delayed in November 2001 due to protests by residents in the area and the government decided to resettle them in different lands in the same area away from the planned highway.<ref name=":0" />

Construction of the road commenced in October, 2009 and it is projected that the project will take at least eight years to complete. Access will be provided to all "A" class roads via interchanges. The highway will have an operational speed limit of 100&nbsp;km/h and is to be built with four lanes and provisions to upgrade the road for six lanes of traffic.<ref name="RDA">{{cite web|url=http://www.rda.gov.lk/supported/expressways/och.htm |title=Outer Circular Highway |publisher=Road Development Authority (Sri Lanka)|date= |accessdate=1 November 2013}}</ref>

== Construction ==
Construction of the Colombo Outer Circular road will be carried out in three phases: Phase 1 - an 11.0&nbsp;km section from Kottawa to Kaduwela; Phase 2 - a 8.9&nbsp;km section from Kaduwela to Kadawatha; and Phase 3 - a 9.3&nbsp;km section from Kadawatha to Kerawelapitiya. Construction of the road commenced in October 2009 and is predicted that it will be completed in September 2017. The expressway will be [[Sri Lanka]]'s costliest and most expensive road, estimated at USD$57 million per km<ref>{{cite news|url=http://www.globaltamilnews.net/GTMNEditorial/tabid/71/articleType/ArticleView/articleId/88157/language/en-US/Colombo-Outer-Circular-Highway-given-to-Chinese-company.aspx|title=Colombo Outer Circular Highway given to Chinese Company|newspaper=Global Tamil News|date=1 February 2013|accessdate=1 November 2013}}</ref>

On January 2013, Road Development Authority said that there will be an extra interchange in Athurugiriya with the intention of accommodating projected traffic from a fast developing area.<ref>{{cite web|url=http://www.lbo.lk/fullstory.php?nid=1527881399 |title=Highway circling Sri Lanka's Capital gets extra Interchange|publisher=Lanka Business Online|date= |accessdate=1 November 2013}}</ref>

===Phase one===
The construction of first stage from [[Kottawa]] to [[Kaduwela, Western Province|Kaduwela]], a length of 11&nbsp;km, was completed at a cost of Rs. 27 billion in early 2014.<ref>{{cite news|url=http://colombogazette.com/2014/03/08/new-outer-circular-highway-opens/|title=New outer circular highway opens|newspaper=Colombo Gazette|date=8 March 2014|accessdate=25 July 2014}}</ref> It will be vested with the public on 8 March 2014.<ref name="RDA"/><ref>{{cite news|url=http://www.sundayobserver.lk/2012/06/24/fea08.asp |title=Road network to be strengthened |newspaper=[[Sunday Observer (Sri Lanka)|Sunday Observer]]|last=Sirimane|first=Shirajiv |date=24 June 2012 |accessdate=1 November 2013}}</ref>

===Phase two===
Construction of the second stage of the project from Kaduwela to Kadawatha, a length of 9.8&nbsp;km, began on 18 February 2012.<ref name="RDA"/> Its expected that phase two will be completed by January 2015, at an estimated cost of Rs. 49 billion.<ref name="RDA"/>

The second phase is scheduled to be opened on 20 June 2015 and will form a major connection between Colombo and Kadawatha and a link to the Northern Expressway which will begin construction in July, thereafter connecting the capital with the major tourism cities: [[Kandy]], [[Kurunegala]] and [[Dambulla]].

===Phase three===
On January 2013, the third phase construction contract of this project from Kadawatha to Kerawalapitiya (length of 9.2&nbsp;km) was awarded  to [[China Metallurgical Group Corporation]] Limited by the Cabinet of Sri Lanka for a value of Rs. 66.69 billion.<ref>{{cite news|url=http://www.news360.lk/economy/news-sri-lanka-01-02-2013-awards-a-road-building-contract-to-chinese-firm-113007|title=Sri Lanka awards a Road Building Contract to Chinese Firm|publisher=News 360 Pvt|date=1 February 2013|accessdate=1 November 2013}}</ref>

==Connectivity==
[[File:Southern Expressway (E01) in Sri Lanka.jpg|thumbnail|[[Kottawa]] Interchange]]
At Kottawa interchange the <abbr title="Outer Circular Expressway">OCE</abbr> will be connected to High Level road and the [[E01 expressway (Sri Lanka)|Colombo - Matara Expressway]] and at Athurugiriya it will be connected the Malabe - Athurugiriya road and later it will be connected to the proposed high elevated road to be constructed from Orugodawatta to Pore - Athurugiriya.

At Kaduwela interchange the <abbr title="Outer Circular Expressway">OCE</abbr> will be connected to Low Level road while at Kadawatha it will be connected to present Kandy road and to the proposed Kandy express way and the Northern highway.

There will connecting roads at Mattumagala connect the <abbr title="Outer Circular Expressway">OCE</abbr> to [[E03 expressway (Sri Lanka)|Colombo - Katunayake Expressway]] at Kerawalapitiya to connect the <abbr title="Outer Circular Expressway">OCE</abbr> with the Katunayake express way.<ref name="sundayobserver.lk"/>
{| class="wikitable sortable"
|-
! Interchange
! Connections
|-
|Kerawalapitiya 
|[[E03 expressway (Sri Lanka)|{{Expressway code (Sri Lanka)|03}} Colombo - Katunayake Expressway]]
|-
|Mattumagala 
|[[A3 Highway (Sri Lanka)|Negombo Road]]
|-
|Kadawatha 
|Colombo-Kandy Road<br> Proposed Colombo-Kandy / Northern Expressway
|-
|Kaduwela 
|Low Level Road<br> Peliyagoda-Mudungoda Road (New Kandy Road)
|-
|Athurugiriya 
| Malabe-Athurugiriya Road <br>Proposed elevated way from Orugodawatta to Athurugiriya
|-
|Kottawa 
|High Level road <br>[[E01 expressway (Sri Lanka)|{{Expressway code (Sri Lanka)|01}} Southern Expressway]]
|-
|}

==See also==
* {{portal-inline|Roads}}
* {{portal-inline|Sri Lanka}}

==References==
{{Reflist}}

==External links==
*[http://www.colombopage.com/archive_091/Aug1251387596CH.html Project to build a beltway around Sri Lankan capital given to Chinese company]
*[http://www.rda.gov.lk/supported/expressways/och.htm RDA - Outer Circular Highway]
*[http://www.dailymirror.lk/DM_BLOG/Sections/frmNewsDetailView.aspx?ARTID=59738 Outer Circular Road Network for Colombo]

{{Expressways and Highways in Sri Lanka |state=expanded}}

[[Category:Highways in Sri Lanka]]
[[Category:Proposed roads]]
[[Category:Proposed transport infrastructure in Sri Lanka]]
[[Category:Japan International Cooperation Agency]]