{{other ships|HMS Audacious}}
{{Good Article}}
{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=[[File:HMS Vanguard h52617.jpg|300px]]
|Ship caption=A sister ship, HMS ''Vanguard''
}}
{{Infobox ship career
|Hide header=
|Ship country=United Kingdom
|Ship flag={{shipboxflag|UK|naval}}
|Ship name=HMS ''Audacious''
|Ship ordered=29 April 1867
|Ship original cost=£256,291
|Ship builder=[[Robert Napier (engineer)|Robert Napier]], [[Govan]]
|Ship laid down=26 June 1867
|Ship launched=27 February 1869
|Ship christened=
|Ship completed=10 September 1870
|Ship commissioned=October 1870
|Ship decommissioned=1894
|Ship in service=
|Ship out of service=
|Ship renamed=*''Fisgard'' in 1904
*''Imperieuse'' in 1914
|Ship reclassified=Depot ship in 1902; training hulk 1906; receiving ship in 1914; storeship in 1920.
|Ship refit=
|Ship struck=
|Ship fate=Sold for [[ship breaking|scrap]] 15 March 1927
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship class={{sclass|Audacious|ironclad}}
|Ship sail plan=[[ship rig]]ged
|Ship displacement={{convert|6034|LT|t}}
|Ship length={{convert|280|ft|m|1|abbr=on}} 
|Ship beam={{convert|54|ft|m|1|abbr=on}}
|Ship draught={{convert|23|ft|m|1|abbr=on}}
|Ship power={{convert|4021|ihp|lk=in|abbr=on}}
|Ship propulsion=1 shaft, 1 [[Marine steam engine#Back acting|horizontal return connecting rod steam engine]]
|Ship speed={{convert|12|kn|lk=in}}
|Ship range={{convert|1260|nmi|lk=in|abbr=on}} at {{convert|10|knots|abbr=on}}
|Ship complement=450
|Ship armament=*10 × [[RML 9 inch 12 ton gun|9-inch]] [[Muzzle-loading rifle|rifled muzzle-loading guns]]
*4 × rifled muzzle-loading 64-pounder guns
|Ship armour=*[[Belt armor|Belt]]: {{convert|6|-|8|in|mm|0|abbr=on}}
*[[Bulkhead (partition)|Bulkhead]]s: {{convert|4|-|5|in|mm|0|abbr=on}}
*[[Artillery battery#Naval usage|Battery]]: {{convert|4|-|6|in|mm|0|abbr=on}}
|Ship notes=
}}
|}

'''HMS ''Audacious''''' was the [[lead ship]] of the {{sclass-|Audacious|ironclad}}s built for the [[Royal Navy]] in the late 1860s. They were designed as second-class ironclads suitable for use on foreign stations and the ship spent the bulk of her career on the [[China Station]]. She was decommissioned in 1894 and [[hulk (ship)|hulk]]ed in 1902 for use as a training ship. The ship was towed to [[Scapa Flow]] after the beginning of the First World War to be used as a [[Receiving ship#Receiving hulk|receiving ship]] and then to [[Rosyth]] after the war ended. ''Audacious'' was sold for [[ship breaking|scrap]] in 1929.

==Design and description==
[[File:Audacious3view.png|thumb|300px|left|Right elevation, plan and cross-section of the ''Audacious''-class ironclads]]
The ''Audacious''-class ironclads were laid out as [[Central battery ship|central battery ironclads]] with the armament concentrated amidships. They were the first British ironclads to have a two-deck [[Artillery battery#Naval usage|battery]] with the upper deck guns [[sponson]]ed out over the sides of the hull. The ships were fitted with a short, plough-shaped [[Naval ram|ram]] and their crew numbered 450 officers and men.<ref>Parkes, pp. 151–52</ref>

HMS ''Audacious'' was {{convert|280|ft|m|1}} [[Length between perpendiculars|long between perpendiculars]]. She had a [[Beam (nautical)|beam]] of {{convert|54|ft|m|1}} and a [[Draft (hull)|draught]] of {{convert|23|ft|m|1}}.<ref name=b1>Ballard, p. 241</ref> The ship was first British ironclad to be completed below her designed displacement; this meant that she was top heavy and required {{convert|360|LT|t}} of cement [[Sailing ballast|ballast]] to raise her [[metacentric height]]. ''Audacious'', and her [[sister ship|sisters]], were the steadiest gun platforms among the large British ironclads of their era.<ref>Ballard, p. 174</ref> ''Audacious'' was given an experimental [[zinc]] sheath for her hull in an attempt to reduce [[biofouling]] that proved unsuccessful.<ref name=b9>Brown, p. 39</ref>

===Propulsion===
''Audacious'' had two 2-cylinder [[Marine steam engine#Back acting|horizontal return connecting rod steam engines]] made by Ravenhill, each driving a single {{convert|16|ft|2|in|m|1|adj=on}} propeller. The bronze four-bladed Mangin propellers were not arranged in the usual radial cross shape, but rather in two pairs, one behind the other, on an elongated boss in an attempt to reduce their [[drag (physics)|drag]] when the ship used her sails. They were later replaced by two-bladed Griffiths propellers. Six rectangular [[boiler (steam generator)|boiler]]s provided steam to the engine at a working pressure of {{convert|31|psi|kPa kg/cm2|0|abbr=on|lk=on}}. The engines produced a total of {{convert|4021|ihp|lk=in}} during [[sea trial]]s on 21 October 1870 and ''Audacious'' reached a maximum speed of {{convert|12.83|kn|lk=in}}. The ship carried {{convert|460|LT|t}} of coal,<ref>Ballard, pp. 175, 246–47</ref> enough to steam {{convert|1260|nmi|lk=in}} at {{convert|10|kn}}.<ref>Silverstone, p. 164</ref>

The ''Audacious''-class ironclads were initially [[ship rig]]ged and had a sail area of {{convert|25054|sqft|sqm|0}}. After the loss of {{HMS|Captain|1869|6}} in a storm in 1870, the ships were modified with a [[barque]] rig which reduced their sail area to {{convert|23700|sqft|sqm|0}}.<ref name=p11>Parkes, pp. 151, 155</ref> They were slow under sail, only {{convert|6.5|kn}},<ref>Brown, p. 37</ref> partly due to the drag of the twin screws, and their shallow draft and flat bottom meant that they were [[leeward]]ly when [[close-hauled]].<ref name=p11/> The three ships, ''Audacious'', {{HMS|Vanguard|1870|2}}, and {{HMS|Invincible|1869|2}}, with balanced [[rudder]]s were described as unmanageable under sail alone.<ref name=b9/>

===Armament===
HMS ''Audacious'' was armed with ten [[RML 9 inch 12 ton gun|9-inch]] and four 64-pounder [[Muzzle-loading rifle|rifled muzzle-loading guns]]. Six of the {{convert|9|in|0|adj=on}} guns were mounted on the main deck, three on each side, while the other four guns were fitted above them on the upper deck. Their [[Glossary of nautical terms#G|gun ports]] were in each corner of the upper battery and could be worked in all weathers, unlike like the guns on the main deck below them. The 64-pounder guns were mounted on the upper deck, outside the battery, as [[chase gun]]s. The ship also had six 20-pounder Armstrong guns for use as [[Salute#Heavy arms: gun salutes|saluting guns]].<ref name=p3>Parkes, p. 153</ref>

The shell of the 14-[[caliber (artillery)|calibre]] 9-inch gun weighed {{convert|254|lb|kg|1}} while the gun itself weighed {{convert|12|LT|t}}. It had a [[muzzle velocity]] of {{convert|1420|ft/s|m/s|abbr=on}} and was credited with the ability to penetrate a nominal {{convert|11.3|in|mm|0}} of [[wrought iron]] armour at the [[muzzle (firearms)|muzzle]]. The 16-calibre 64-pounder gun weighed {{convert|3.2|LT|t}} and fired a {{convert|6.3|in|0|adj=on}}, {{convert|64|lb|kg|1|adj=on}} shell that had a muzzle velocity of {{convert|1125|ft/s|m/s|abbr=on}}.<ref>Gardiner, p. 6</ref>

In 1878 ''Audacious'' received four {{convert|14|in|mm|0|adj=on}} [[torpedo]] launchers that were carried on the main deck, outside the armoured battery.<ref name=p3/> When the ship was refitted in 1889–90<ref>Parkes, p. 155</ref> she received eight [[BL 4 inch naval gun Mk I – VI|4-inch]] [[Breech-loading weapon|breech-loading guns]] as well as four [[quick-firing gun|quick-firing]] [[QF 6-pounder Hotchkiss|6-pounder Hotchkiss]] and six [[QF 3-pounder Hotchkiss|3-pounder Hotchkiss guns]] for defence against [[torpedo boat]]s.<ref name=g0>Gardiner, p. 15</ref>

===Armour===
''Audacious'' had a complete waterline [[Belt armor|belt]] of [[wrought iron]] that was {{convert|8|in|mm|0}} thick amidships and tapered to {{convert|6|in|mm|0}} thick at the bow and stern. It only protected the main deck and reached {{convert|3|ft|m|0}} above the [[waterline]] at full load and {{convert|5|ft|m|1}} below. The guns were protected by a section of 8-inch armour, {{convert|59|ft|m|1}} long, with a {{convert|5|in|mm|0|adj=on}} transverse [[Bulkhead (partition)|bulkhead]] forward and a {{convert|8|in|mm|0|adj=on}} bulkhead to the rear. The armour was backed by {{convert|8|-|10|in}} of [[teak]]. The total weight of her armour was {{convert|924|LT|t}}.<ref>Parkes, pp. 151, 153–54</ref>

==Service==
HMS ''Audacious'' was ordered on 29 April 1867 from [[Robert Napier (engineer)|Robert Napier]] in [[Govan]], [[Glasgow]]. She was laid down on 26 June 1867 and launched on 27 February 1869 in a gale. The winds caught the rear of the ship as she was about halfway down the [[slipway]] and twisted her enough that some plates and frames of her bottom were damaged. The ship was completed on 10 September 1870 and commissioned the following month.<ref>Ballard, pp. 176, 241</ref> She cost £256,291<ref group=Note>Adjusted for inflation, the ship cost {{formatnum:{{Inflation|UK|256291|1870}}}} in current pounds.</ref> to build.<ref>Parkes, p. 151</ref>

Upon completion she became [[guard ship]] of the First Reserve at Kingstown, Ireland (modern [[Dún Laoghaire]]), but was transferred the following year to [[Kingston upon Hull|Hull]] where she remained until 1874. The ship was ordered to the Far East that year to serve as the flagship for the China Station under the flag of Vice-Admiral Sir [[Alfred Phillips Ryder]].<ref>Ballard, pp. 176–77</ref> Despite the presence of escorting tugs, ''Audacious'' grounded twice while she was transiting through the [[Suez Canal]].<ref name=b7>Ballard, p. 177</ref> She relieved her sister {{HMS|Iron Duke|1870|2}} in [[Singapore]], and later collided with a merchant ship during a typhoon in [[Yokohama]]. ''Iron Duke'' relieved her in turn in 1878.<ref name=b7/>  ''Audacious'' returned to her previous post in Hull in 1879, relieving {{HMS|Endymion|1865|6}}.<ref name=Times140779>{{Cite newspaper The Times |articlename=Naval and Military Intelligence |day_of_week=Monday |date=14 July 1879 |page_number=8 |issue=29619 |column=E }}</ref> She served there until she began a lengthy refit which included new boilers and the addition of a [[poop deck]].<ref name=b7/>

The ship's refit was complete in March 1883 and she again relieved ''Iron Duke'' as flagship of the China Station later that year. ''Audacious'' remained there until 1889 when she returned to [[Chatham Dockyard|Chatham]] where she was refitted, rearmed and replaced her masts and rigging with simple pole masts fitted with [[Top (sailing ship)|fighting tops]]. Upon the completion of her refit in 1890 she returned to Hull for the third time until the ship was [[Ship decommissioning|decommissioned]] in 1894. ''Audacious'' was relegated to 4th class [[Wikt:mothball|reserve]] until her engines were removed and she was converted to an unpropelled [[depot ship]] in 1902. She was commissioned at [[HMNB Chatham|Chatham]] on 16 July 1902 by Captain Henry Loftus Tottenham as torpedo depot ship at that port.<ref>{{Cite newspaper The Times |articlename=Naval & Military intelligence |day_of_week=Thursday |date=17 July 1902 |page_number=9 |issue=36822| }}</ref> She then acted as depot ship for [[destroyers]] at [[Felixstowe]] until 1905, when she paid off; in April 1904 she had been renamed ''Fisgard'' (after the French translation of the Welsh town [[Fishguard]]). In 1906, she was recomissioned as part of the four-ship [[HMS Fisgard (shore establishment)|Fisgard]] boy artificers training establishment at [[Portsmouth]]. The ship was towed to Scapa Flow in 1914 after the start of the First World War to be used as a receiving ship and was renamed ''Imperieuse''. On 13 January 1915, the auxiliary [[Minesweeper (ship)|minesweeper]] {{HMS|Roedean}} was driven onto ''Imperieuse'' in Scapa Flow off [[Hoy]], [[Orkney Islands]]; ''Rodean'' sank due to damage she suffered in the collision.<ref name=Roedean>{{cite web |url=http://www.scapaflowwrecks.com/wrecks/other/roedean.php |title=HMS Roedean |publisher=Scapa Flow |accessdate=19 February 2013}}</ref>
In 1919, ''Imperieuse'' was to be renamed ''Victorious'', but the renaming was cancelled. She was towed from Scapa to Rosyth on 31 March 1920, where she remained as [[store ship]] until 15 March 1927, when she was sold to [[Thos W Ward]] of [[Inverkeithing]] for scrap.<ref>Warlow, p. 75</ref>

==Notes==
{{reflist|group=Note}}

==Footnotes==
{{Reflist|2}}

==References==
* {{cite book|last=Ballard |first=G. A., Admiral |title=The Black Battlefleet |year=1980 |publisher=Naval Institute Press |location=Annapolis, MD |isbn=0-87021-924-3 |oclc=}}
*{{cite book|last=Brown|first=David K.|title=Warrior to Dreadnought: Warship Development 1860–1905|edition=reprint of the 1997|year=2003|publisher=Caxton Editions|location=London|isbn=1-84067-529-2}}
*{{cite book|title=Conway's All the World's Fighting Ships 1860–1905|editor=Gardiner, Robert |publisher=Conway Maritime Press|location=Greenwich|date=1979|isbn=0-8317-0302-4}}
*{{cite book|last=Parkes|first=Oscar|title=British Battleships|publisher=Naval Institute Press|location=Annapolis, MD|year=1990|edition=reprint of the 1957|isbn=1-55750-075-4}}
*{{cite book|last=Silverstone|first=Paul H.|title=Directory of the World's Capital Ships|year=1984|publisher=Hippocrene Books|location=New York|isbn=0-88254-979-0}}
*{{cite book|last=Warlow|first=B.|title=Shore Establishments of the Royal Navy|year=2000|publisher=Maritime Books|location=Liskeard|isbn=978-0-907771-73-9}}

{{Audacious class battleship}}
{{January 1915 shipwrecks}}

{{DEFAULTSORT:Audacious (1869)}}
[[Category:Audacious-class ironclads]]
[[Category:Victorian-era battleships of the United Kingdom]]
[[Category:1869 ships]]
[[Category:Maritime incidents in 1915]]