The '''African Blood Brotherhood for African Liberation and Redemption''' ('''ABB''') was a radical [[United States|U.S.]] [[African-American Civil Rights Movement (1896–1954)|black liberation]] organization established in 1919 in New York City by journalist [[Cyril Briggs]]. The group was established as a propaganda organization built on the model of the [[secret society]]. The group's [[socialist]] orientation caught the attention of the fledgling American [[Communism|communist]] movement and soon evolved into a propaganda arm of the [[Communist Party of America]]. The group was terminated in the early 1920s.

==Organizational history==

===Background===

During the second decade of the 20th century, a socialist movement for the liberation of American blacks began to develop in the [[Harlem]] section of [[New York City]].<ref name=Solomon4>Mark Solomon, ''The Cry Was Unity: Communists and African Americans, 1917-36.'' Jackson: University Press of Mississippi, 1998; p. 4.</ref> The movement included a substantial number of immigrants from the [[British West Indies]] and other islands from the [[Caribbean]] region, who, having been raised and educated as part of a racial majority population in their homelands, had found themselves thrust into the position of an [[racism|oppressed racial minority]] in America.<ref name=Solomon4 /> As products of the unequal system of [[colonialism]], many of these newcomers to America were predisposed to hostility towards [[capitalism]] and the notion of [[imperialism|empire-building]].<ref name=Solomon4 />

One of these transplants from the Caribbean was [[Cyril Briggs]], born in 1888 on the island of [[Nevis]], who immigrated to Harlem in the summer of 1905.<ref name=Solomon5>Solomon, ''The Cry Was Unity,'' p. 5.</ref> In 1912, Briggs was hired as a [[journalist]] by one of the black community's leading newspapers, the ''[[New York Amsterdam News]].'' He worked there throughout the years of the [[World War I|First World War]].<ref name=Solomon5 /> Inspired by the rhetoric of "national [[self-determination]]" espoused by President [[Woodrow Wilson]], in September 1918 Briggs launched a monthly publication called ''[[The Crusader (journal)|The Crusader]],'' to promote the idea of "repatriation" of blacks to a decolonized [[Africa]], a concept similar to the contemporary notion among some European Jews of [[Zionism]] and "return" to Palestine.<ref name=Solomon6>Solomon, ''The Cry Was Unity,'' p. 6.</ref>

''The Crusader'' was started by [[George Wells Parker]], a black businessman from [[Omaha, Nebraska]], as the official organ of the [[Hamitic League of the World]], a pan-African nationalist group.<ref name=Solomon6 /> Parker published articles in the journal proclaiming that Africa was the [[cradle of civilization]] and arguing the superiority of the black race. He contributed financially to the publication, which was essentially a vehicle for his views.<ref>Solomon, ''The Cry Was Unity,'' pp. 6-7.</ref>

In February 1919, Briggs began to change his ideas, and his new thinking was expressed in articles in the ''Crusader.'' He began to draw parallels between the plight of black workers in the United States and impoverished [[working class]] whites, who were mostly recent immigrants or their descendants from [[Europe]].<ref name=Solomon7>Solomon, ''The Cry Was Unity,'' p. 7.</ref> Over ensuing months, Briggs began to consider the system of [[capitalism]] as the villain, and he argued in favor of a common cause and common action by workers of all races.<ref name=Solomon7 />

The ''Crusader'' eventually reached a total readership of 36,000 persons, mostly in Harlem.<ref name=Letter>Cyril Briggs, [http://www.marxisthistory.org/history/usa/groups/abb/1958/0317-briggs-todraper.pdf "Letter to Theodore Draper in New York from Cyril Briggs in Los Angeles, March 17, 1958 (long extract),"] Corvallis, OR: 1000 Flowers Publishing, 2007.</ref>

===Establishment of the organization===

The summer of 1919 in America was a time of racial rioting and violence, remembered retrospectively by historians as the "[[Red Summer of 1919|Red Summer]]." Returning soldiers from European battlefields, including blacks with heightened expectations of freedom and equality and whites seeking a return to civilian employment and the ''[[status quo ante bellum]],'' and new immigrant black workers from the rural South formed a volatile mixture which erupted in mob violence in [[Chicago]], Omaha, and cities throughout the Midwest and South.

In response to these attacks, ''The Crusader'' advocated armed self-defense. Politically, Briggs drew comparisons between government attacks on white and black radicals. He identified capitalism as the underlying cause of oppression of poor people of all races. While endorsing a [[Marxist]] analysis, ''The Crusader'' advocated a separate organization of African-Americans to defend against racist attacks in the United States, and likened this to Africans' combating colonialism abroad.

In September 1919, ''The Crusader'' announced the formation of a new organization called the African Blood Brotherhood (ABB), to serve as a self-defense organization for blacks threatened by [[race riot]]s and [[lynching]]s.

Not long afterwards, Briggs began to forge connections with pioneer black American Communists such as the [[Surinam]]-born [[Otto Huiswoud]] and [[Jamaican]] poet and writer [[Claude McKay]].<ref name=Solomon9>Solomon, ''The Cry Was Unity,'' p. 9.</ref> These in turn connected Briggs and his publication with native-born white Communists including [[Robert Minor]] and [[Rose Pastor Stokes]], who took a strong interest in the so-called "Negro Question."<ref name=Solomon9 /> Briggs would join the Communist Party himself in 1921.<ref name=Solomon9 />

===Conflicts with Marcus Garvey and the Bureau of Investigation===

The ABB attempted to organize from inside the [[UNIA-ACL]] and advocated a policy of critical support for its leader, [[Marcus Garvey]]. ABB leaders Briggs and [[Claude McKay]] participated in the UNIA's 1920 and 1921 international conferences in New York. At the second conference, McKay arranged for [[Rose Pastor Stokes]], a white leader of the [[Communist Party USA|Communist Party]], to address the assembly.

The ABB became highly critical of Garvey following the apparent failure of the [[Black Star Line]] and Garvey's July 1921 Atlanta meeting with Grand Kleagle Clarke of the [[Ku Klux Klan]]. In June 1922, ''The Crusader'' announced that it had become the official organ of the African Blood Brotherhood. Arguing that the UNIA was doomed unless it developed new leadership, the magazine sought to convert the UNIA's membership to the ABB. In seeking to replace the UNIA, the ABB competed with Randolph's socialist publication ''[[The Messenger Magazine|The Messenger]]'', which had called for Garvey's expulsion from the United States. In return, Garvey called for his followers to disrupt meetings of these oppositional groups.

In addition to the dispute with Garvey, Briggs and the ABB were targeted for investigation by police and federal law enforcement agencies. Historian Theodore Kornweibel reports that the government began manipulating radical organizations in conjunction with legal prosecution under the pretence of disrupting opposition to [[World War I]]. Following the end of the war, a government campaign against communists, anarchists, and other radicals was instituted at the direction of Attorney General [[A. Mitchell Palmer]] (himself the victim of two anarchist bomb attacks) in what came to be called the [[First Red Scare]]. Government agents were secretly planted in the UNIA, ABB and ''[[The Messenger Magazine|The Messenger]]''. These agents provided intelligence to the [[Bureau of Investigation]] while in some case sabotaging meetings, and acting as [[agent provocateur|agents provocateurs]].

The ABB enjoyed a period of notoriety following the [[Tulsa Race Riot|Tulsa Riot]] of 1921. Tulsa had an ABB chapter and news reports credited the organization with inspiring resistance to racist attacks.

===Fusion with the Communist Party===

''The Crusader'' ceased publication in February 1922, following Garvey's indictment for [[mail fraud]]. Briggs continued to operate the Crusader News Service, providing news material to affiliated publications of the American black press. As cooperation with the Communist Party increased, the ABB ceased to recruit separately.

The leadership of the [[Communist International]], while largely ignorant about the particulars of the situation of blacks in the United States, did understand the importance of ethnic and other non-class forms of oppression, and pushed the early CP to pay more attention to blacks in the U.S.{{Citation needed|date=August 2010}} Before this intervention by the Comintern, the party had largely ignored blacks, and thus was not particularly attractive to black radicals like Briggs. Instead, it was the [[Bolshevik Revolution]] that attracted their attention.

Poet and ABB member Claude McKay has previously been active in the [[Left Communist]] [[Workers Socialist Federation]] in [[London]] and subsequently visited the [[Soviet Union]] several times in the mid-1920s, writing about conferences of the Communist International for African-American audiences. McKay's book, ''The Negroes in America'' (published in Russian in 1924 but not in the U.S. until 1979) argued, against the official Communist position of the time, that the oppression of black people in the U.S. was not reducible to economic oppression, but was unique. He argued against the color blindness that the Communists had inherited from the Socialist Party.

McKay made argued vociferously for [[national self-determination]] in support of national independence for oppressed peoples, which to him meant an independent African-American government separate and apart from that of the United States.  Subsequently, in the aftermath of the [[Comintern#From the Fifth to the Seventh World Congress|Sixth Comintern Congress]] in 1928, the CPUSA adopted a policy of national self-determination for African-Americans living in the [[Black Belt (U.S. region)|Black Belt]] of the American South. The policy was neglected after the [[Popular Front]] period began in 1935, but was not formally replaced until 1959.

As the Communist Party developed, it regularized its structure along the lines called for by the [[Communist International]] (Comintern). Semi-independent organizations such as the African Blood Brotherhood with its divergent Afro-Marxist political theories were anathema to the Comintern and its Soviet leaders, who believed all communist and Marxist–Leninist organizations should be unified in a single communist party and platform in each nation under Moscow's overall direction and control. In the early 1920s the African Blood Brotherhood was dissolved, with its members merged into the [[Workers Party of America]] and later into the [[American Negro Labor Congress]]. Many early ABB members, however, went on to be key CP cadres for decades.

===Membership===

The ABB had a total membership of fewer than 3,000 members at its peak.

==Footnotes==
{{Reflist|2}}

==Further reading==

===ABB Publications===

* Cyril V. Briggs (ed.), ''[http://catalog.hathitrust.org/Record/012360295 The Crusader.]'' August 1918-February 1922. New York: Garland Publishing, 1987.
* Cyril V. Briggs, [https://archive.org/details/0600BriggsAfricanbloodbrotherhood "The African Blood Brotherhood,"] ''The Crusader,'' vol. 2, no. 10 (June 1920), pp. 7, 22.
*[http://www.marxists.org/history/international/comintern/sections/britain/periodicals/communist_review/1922/06/african.htm "Programme of the African Blood Brotherhood"], ''The Communist Review,'' vol. 2, no. 6 (April 1922).

===Published material===

* Eric Arnesen, ''Black Protest and the Great Migration : A Brief History with Documents.'' New York: Bedford/St. Martin's, 2002.
* Anthony Dawahare, ''Nationalism, Marxism, and African American Literature Between the Wars: A New Pandora's Box.'' Jackson: University Press of Mississippi, 2002.
* Michael C. Dawson, ''Black Visions: The Roots of Contemporary African-American Political Ideologies.'' Chicago: University Of Chicago Press, 2003.
* [[Theodore Draper]], ''The Roots of American Communism.'' New York: Viking Press, 1957.
* Harry Haywood, ''Black Bolshevik: Autobiography of an Afro-American Communist.'' Chicago: Liberator Press, 1978.
* [[Robert A. Hill (Jamaican historian)|Robert A. Hill]] (ed.), ''The FBI's RACON: Racial Conditions in the United States during World War I.'' Ithaca, NY: Northeastern University Press, 1995.
*Robert A. Hill, "Racial and Radical: Cyril V. Briggs, The Crusader Magazine, and the African Blood Brotherhood, 1918-1922."  Introductory essay to ''The Crusader.'' New York: Garland Publishing, 1987.
* Winston James, ''Holding Aloft the Banner of Ethiopia: Caribbean Radicalism in Early Twentieth Century America.'' London: Verso Books, 1998.
* Shannon King, [https://web.archive.org/web/20051118072528/http://history.binghamton.edu/resources/bjoh/stateviolenceandblackresistance.doc "Enter the New Negro: State Violence and Black Resistance during World War I and the 1920s,"] ''Binghamton Journal of History,'' Spring 2004.
* Theodore Kornweibel, Jr. ''"Investigate Everything": Federal Efforts to Compel Black Loyalty During World War I.'' Bloomington: Indiana University Press, 2002.
* Theodore Kornweibel, Jr. ''Seeing Red: Federal Campaigns Against Black Militancy, 1919-1925.'' Bloomington: Indiana University Press, 1999.
* Ronald A. Kuykendall, [http://findarticles.com/p/articles/mi_go2877/is_1_26/ai_n6808166/ "The African Blood Brotherhood, Independent Marxist During the Harlem Renaissance"], ''The Western Journal of Black Studies,'' vol. 26, no. 1 (2002), pp.&nbsp;16–21.
* Mark I. Solomon, ''The Cry Was Unity: Communists and African Americans, 1917-36.'' Jackson: University Press of Mississippi, 1998.
* Alan Wald, [http://www.hartford-hwp.com/archives/45a/435.html "African Americans, Culture and Communism (Part 1): National Liberation and Socialism"], ''[[Solidarity (U.S.)|Against the Current]],'' vol. 14, no. 6, whole no. 84 (January/February 2000).
*Jacob A. Zumoff, ''The Communist International and US Communism, 1919-1929.'' [2014] Chicago: Haymarket Books, 2015.

===Unpublished material===

* Maria Gertrudis van Enckevort, ''The Life and Work of Otto Huiswoud: Professional Revolutionary and Internationalist (1893–1961).'' Mona, Jamaica: University of the West Indies, 2000. PhD dissertation.
* Minkah Makalani, ''For the LIberation of Black People Everywhere: The African Blood Brotherhood, Black Radicalism, and Pan-African Liberation in the New Negro Movement, 1917-1936.'' Urbana, IL: University of Illinois at Urbana-Champaign, 2004. PhD dissertation.
* Theman Ray Taylor, ''Cyril Briggs and the African Blood Brotherhood: Another Radical View of Race and Class in the 1920s.'' Santa Barbara, CA: University of California at Santa Barbara, 1981. PhD dissertation.
* Maurie I. Warren,  ''Moses and the Messenger: The Crisis of Black Radicalism, 1921-1922.'' Cambridge, MA: Harvard University, 1974. Bachelor's thesis.
* Jacob A. Zumoff,  ''The Communist Party of the United States and the Communist International, 1919-1929.''  London: University of London, 2003. PhD dissertation.

===Archives===
* [http://dlib.nyu.edu:8083/tamwagead/servlet/SaxonServlet?source=/solomon.xml&style=/saxon01t2002.xsl&part=body Research files on African-Americans and communism, 1919-1993, (Bulk 1919-1939).] Created by Mark Solomon. Tamiment Library/Robert F. Wagner Labor Archives. 4.25 linear feet (4 boxes).
* [http://www.newberry.org/general/L3ageneral.html Theodore Kornweibel Research Papers, 1910-1960.] Research materials assembled by Theodore Kornweibel. The collection consists of copies of FBI and other federal agency records, including case files obtained through the Freedom of Information Act, detailed notecards, printed federal documents, and Kornweibel's correspondence with federal agencies. Newberry Library, Chicago.

==See also==
{{Portal|African American|United States}}
*[[The Communist Party and African-Americans]]
*[[American Civil Rights Movement (1896-1954)]]
*[[Black Belt (U.S. region)]]
*[[Harry Haywood]]
*[[Alternative media|Alternative press]]
*[[Black separatism]]
*[[Black nationalism]]

==External links==
* [http://www.marxisthistory.org/subject/usa/eam/abb.html African Blood Brotherhood documents], Early American Marxism website, www.marxisthistory.org/
* [http://www.tcnj.edu/~fisherc/black_and_red.html "Black and Red: A Journey Through Communism in the Black Community,"] www.tcnj.edu/
*[http://www.icl-fi.org/english/wv/1006/jacob.html "The Black Freedom Struggle and the Comintern"], www.icl-fi.org/

[[Category:1919 establishments in New York]]
[[Category:African-American leftism]]
[[Category:African Americans' rights organizations]]
[[Category:Communist Party USA mass organizations]]
[[Category:African-American history in Omaha, Nebraska]]
[[Category:Federal Bureau of Investigation]]
[[Category:History of Omaha, Nebraska]]
[[Category:Organizations established in 1919]]