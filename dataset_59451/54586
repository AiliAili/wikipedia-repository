'''Fashionable novels''', also called '''silver-fork novels''', were a [[19th century in literature|19th-century]] [[genre]] of [[English literature]] that depicted the lives of the [[upper class]] and the aristocracy.

==Era==
The silver-fork novels dominated the English literature market from the mid-1820s to the mid-1840s.<ref name="wu">{{cite book |first=Duncan |last=Wu |title=A Companion to Romanticism |url=https://books.google.com/books?id=kJCHB0tqd1kC&pg=PA338 |date=29 October 1999 |publisher=John Wiley & Sons |isbn=978-0-631-21877-7 |page=338}}</ref> They were often indiscreet, and on occasion "[[roman à clef|keys]]" would circulate that identified the real people on which the principal characters were based.<ref name=wu/> Their emphasis on the relations of the sexes and on marital relationships presaged later development in the novel.<ref name="collection"/>

==Genre and satire of the genre==
[[Theodore Hook]] was a major writer of fashionable novels, and [[Henry Colburn]] was a major publisher.<ref name=wu/> Colburn particularly advertised fashionable novels as providing insight into aristocratic life by insiders.<ref name="Wagner"/> [[Edward Bulwer-Lytton]], [[Benjamin Disraeli]] and [[Catherine Gore]] were other very popular writers of the genre.<ref>[http://www.enotes.com/nineteenth-century-criticism/gore-catherine Catherine Gore 1799(?)-1861] {{webarchive |url=https://web.archive.org/web/20091007144620/http://www.enotes.com/nineteenth-century-criticism/gore-catherine |date=October 7, 2009 }}</ref> Many were advertised as being written by aristocrats, for aristocrats.<ref>{{cite book |first=Claire |last=Harman |title=Jane's Fame: How Jane Austen Conquered the World |page=72 |publisher=Henry Holt and Co |location=New York |year=2010 |isbn=978-0-8050-8258-6}}</ref>

As more women wrote the genre, it became increasingly moralized: "middle-class morality became central, and the novels detailed the demise of the aristocracy, though the characteristically Byronic heroes of the genre remained."<ref name="collection">{{cite web |url=http://www.gla.ac.uk/services/specialcollections/collectionsa-z/novelcollection/silverforknovels/ |title=Silver Fork Novels |publisher=University of Glasgow, Special Collections |accessdate=16 December 2015 }}</ref> The most popular authors of Silver Fork novels were women, including Lady Blessington, Catherine Gore and Lady Bury.<ref name="collection" />

William Hazlitt coined the term "silver fork" in an article on “The Dandy School” in 1827.<ref name="Wagner">{{cite web |first=Tamara S. |last=Wagner |url=http://www.victorianweb.org/genre/silverfork.html |title=The Silver Fork Novel |work=Victorian Web |date=12 December 2002 |accessdate=2 October 2016}}</ref> He characterized them as having "under-bred tone" because while they purported to tell the lives of aristocrats, they were commonly written by the middle-class.<ref name="Wagner"/> [[Thomas Carlyle]] wrote ''[[Sartor Resartus]]'' in critique of their minute detailing of clothing, and [[William Makepeace Thackeray]] satirized them in ''[[Vanity Fair (novel)|Vanity Fair]]'' and ''[[Pendennis]]''.<ref name="Wagner"/>

==In modern culture==
In [[Donna Leon]]'s fourth [[Donna Leon#List of Commissario Guido Brunetti novels|Commissario Guido Brunetti novel]], ''Death and Judgment'' aka ''A Venetian Reckoning'' (1995), chapter 22, English professor Paola Brunetti explains to her husband, Guido Brunetti, [[silver fork novel]]s are "books written in the eighteenth century, when all that money pored into England from the colonies, and the fat wives of Yorkshire weavers had to be taught which fork to use."<ref>{{cite book |last=Leon |first=Donna |title=Death and Judgment |date=June 1995 |publisher=Harpercollins |edition=1st |isbn=978-0060177966}}</ref>

==References==
{{reflist}}

==Further reading==
* {{cite book| author = Richard Cronin| title = Romantic Victorians: English Literature, 1824-1840| date = 2002-03-08| publisher = Palgrave MacMillan| isbn = 978-0-333-96616-7 }}
* {{cite web|url=http://www.victorianweb.org/genre/silverfork.html|title= The Silver Fork Novel |website= [[Victorian Web]]}}

==See also==
*[[Silver spoon]]

[[Category:Literary genres]]
[[Category:English literature]]
[[Category:History of literature]]


{{lit-genre-stub}}