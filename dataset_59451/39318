{{italic title}}
[[File:Captain Future, Wizard of Science.jpg|thumb|right|The ''Captain Future'' logo as it appeared on the cover of the Fall 1940 issue]]
'''''Captain Future''''' was a [[science fiction]] [[pulp magazine]] launched in 1940 by [[Standard Magazines|Better Publications]], and edited initially by [[Mort Weisinger]].  It featured the adventures of [[Captain Future]], a super-scientist whose real name was Curt Newton, in every issue.  All but two of the novels in the magazine were written by [[Edmond Hamilton]]; the other two were by [[Joseph Samachson]].  The magazine also published other stories that had nothing to do with the title character, including [[Fredric Brown]]'s first science fiction sale, "Not Yet the End".  ''Captain Future'' published unabashed [[space opera]], and was, in the words of science fiction historian [[Mike Ashley (writer)|Mike Ashley]], "perhaps the most juvenile"<ref name=":1" /> of the science fiction pulps to appear in the early years of World War II.  Wartime paper shortages eventually led to the magazine's cancellation: the last issue was dated Spring 1944.

== Publication history and contents ==
Although [[science fiction]] (sf) had been published before the 1920s, it did not begin to coalesce into a separately marketed genre until the appearance in 1926 of ''[[Amazing Stories]]'', a [[pulp magazine]] published by [[Hugo Gernsback]].  By the end of the 1930s, the field was booming.<ref>Edwards & Nicholls (1992), pp.&nbsp;1066–1068.</ref>  [[Standard Magazines|Better Publications]], a pulp magazine publisher which had acquired ''[[Thrilling Wonder Stories]]'' in 1936, launched three new magazines as part of this boom.  The first two were ''[[Startling Stories]]'', which appeared in January 1939, and ''[[Strange Stories]]'', which began the following month; both were edited by [[Mort Weisinger]], who was also the editor of ''Thrilling Wonder Stories.''<ref name=":0">Ashley (2000), pp. 250–255.</ref>  [[Edmond Hamilton]], an established science fiction writer,  met with [[Leo Margulies]], Better Publication's editorial director, in early 1939, and they subsequently planned the launch of a new magazine with the lead character of Curt Newton, a super-scientist who lived on the moon and went by the name "[[Captain Future]]".  Margulies announced the new magazine at the [[1st World Science Fiction Convention|first World Science Fiction Convention]], held in New York in July 1939, and the first issue, edited by Weisinger, appeared in January of the following year.<ref>Moskowitz (1974), p. 109.</ref><ref name="SFFWFM_CF" />  Captain Future's companions in the series included an enormously strong robot named Grag, an android named Otho, and the brain of Simon Wright, Newton's mentor.  Joan Randall, Newton's girlfriend, was also a regular character.  Better Publications followed up the magazine launch with a companion comic, ''Startling Comics'', which appeared in May 1940; Captain Future was the protagonist of the lead story.<ref>Ashley (2000), pp. 152–153.</ref>  Weisinger left in 1941 to edit comics following the adventures of [[Superman]], and was replaced by [[Oscar J. Friend]].<ref name="SFFWFM_CF" />

''Captain Future'' was a hero pulp: these were pulps which were built around a central character, with every issue containing a lead story featuring that character.{{#tag:ref|''Captain Future'' was the only hero pulp which was also a science fiction magazine, though a couple of other hero pulps, such as ''Captain Hazzard'' and ''Captain Zero'', did include some science fictional material.  Flash Gordon also appeared in a one off magazine bearing his own name.<ref name="SFFWFM_CF" /><ref>Weinberg (1985a), p. 158.</ref><ref>Weinberg (1985b), p. 159−160.</ref>|group = note}}  Every issue of ''Captain Future'' contained a novel about Curt Newton.  Hamilton was willing to write the lead novel for every issue, but was concerned that he might be [[Conscription in the United States|drafted]], so Margulies made arrangements for other writers to contribute the lead stories.  Hamilton escaped the draft, but Margulies had already made arrangements for other writers to work on the series, and so two of the seventeen lead novels in the magazine were written by [[Joseph Samachson]], instead of by Hamilton.  The house name "Brett Sterling" was invented to conceal the identity of the new writer; it was used for both of Samachson's contributions, as well as some of Hamilton's.<ref name="SFFWFM_CF" />  Hamilton also wrote regular features that provided background material on the stories: "Worlds of Tomorrow" provided information about the planets featured in the stories, and "The Futuremen" covered Newton's companions.<ref name="SFFWFM_CF" /><ref>Gombert (2009), p. 284.</ref>  In addition to the novels about Curt Newton, ''Captain Future'' published both new and reprinted science fiction stories that were unconnected with the lead character.  [[Fredric Brown]]'s first sf sale, "Not Yet the End", appeared in the Winter 1941 issue; and Weisinger reprinted [[David H. Keller]]'s ''The Human Termites'' and [[Laurence Manning]]'s ''[[The Man Who Awoke]]'', both abridged, in the first few issues of the magazine; these had originally appeared in 1929 and 1933, respectively, and were from back issues of ''Wonder Stories'', which Better Publications had acquired the rights to in 1936.  The magazine was unashamedly focused on straightforward space opera: a typical plot saw Captain Future and his friends save the solar system, or perhaps the entire universe, from a villain.<ref name="SFFWFM_CF" /> Sf historian [[Mike Ashley (writer)|Mike Ashley]] describes the magazine as "perhaps the most juvenile" of the World War II crop of science fiction pulps.<ref name=":1">Ashley (1978), p. 57.</ref>  Wartime paper shortages killed the magazine in mid-1944, but more Captain Future novels saw print in ''Startling Stories'', some over the next two years, with more following in 1950 and 1951.<ref name="SFFWFM_CF" />

== Bibliographic details ==
{| class="wikitable" style="font-size: 10pt; line-height: 11pt; margin-left: 2em; text-align: center; float: right"
! !!Winter !! Spring !!Summer !!Fall 
|- 
!1940
| bgcolor=#ccffff|1/1 || bgcolor=#ccffff|1/2 || bgcolor=#ccffff|1/3 || bgcolor=#ccffff|2/1 
|- 
!style="width:10%;" |1941
|style="width:18%;" bgcolor=#ccffff|2/2 || style="width:18%;" bgcolor=#ccffff|2/3 || style="width:18%;" bgcolor=#ccffff|3/1 || style="width:18%;" bgcolor=#ffff99|3/2 
|- 
!1942
| bgcolor=#ffff99|3/3 || bgcolor=#ffff99|4/1 || bgcolor=#ffff99|4/2 || bgcolor=#ffff99|4/3 
|- 
!1943
| bgcolor=#ffff99|5/1 || bgcolor=#ffff99|5/2 || bgcolor=#ffff99|5/3 ||
|- 
!1944
| bgcolor=#ffff99|6/1 || bgcolor=#ffff99|6/2 || ||
|-
|colspan="13" style=" width:100%; font-size: 8pt; text-align:left"|Issues of ''Captain Future'' from 1940 to 1944, showing issue numbers,<br/>and indicating editors: Weisinger (blue, first seven issues), and<br/>Friend (yellow, remaining ten issues).
|}
''Captain Future'' was pulp format, 128 pages, and was priced at 15 cents; the first seven issues were edited by Mort Weisinger, and the remaining ten by Oscar J. Friend.  There were three issues to a volume.  The schedule was quarterly, with one omission: there was no Fall 1943 issue.  The publisher was Better Publications, with offices in Chicago and New York, for all issues.<ref name="SFFWFM_CF">Ashley & Ewald (1985), p. 155−157.</ref>  The magazine was subtitled "Wizard of Science" for the first four issues; after that the subtitle was "Man of Tomorrow", a name that had already been used by the Superman franchise for their hero.<ref>{{Cite web|url = http://www.philsp.com/mags/sf_c.html#captain_future|title = Captain Future|accessdate = 23 December 2014|website = Galactic Central|publisher = Phil Stephenson-Payne|last = Stephenson-Payne|first = Phil}}</ref><ref>Jones (2005), p. 156.</ref>

Thirteen Captain Future novels were reprinted as paperbacks at the end of the 1960s, all by [[Popular Library]].  Ten of these, all printed in 1969, were originally printed in ''Captain Future ''as follows:<ref name="SFFWFM_CF" /><ref>Currey (1979), pp. 217–219.</ref>
{| class="wikitable sortable"
!Serial #
!Title
!Original issue
!class="unsortable"|Notes
|-
|2389
|''Quest Beyond the Stars''
|{{sort|3/3|Winter 1942}}
|
|-
|2399
|''Outlaws of the Moon''
|{{sort|4/1|Spring 1942}}
|
|-
|2407
|{{sort|''Comet Kings''|''The Comet Kings''}}
|{{sort|4/2|Summer 1942}}
|
|-
|2416
|''Planets in Peril''
|{{sort|4/3|Fall 1942}}
|
|-
|2421
|''Calling Captain Future''
|{{sort|1/2|Spring 1940}}
|
|-
|2430
|''Captain Future's Challenge''
|{{sort|1/3|Summer 1940}}
|
|-
|2437
|''Galaxy Mission''
|{{sort|2/1|Fall 1940}}
|Magazine version titled "The Triumph of Captain Future".
|-
|2450
|{{sort|''Magician of Mars''|''The Magician of Mars''}}
|{{sort|3/1|Summer 1941}}
|
|-
|2445
|{{sort|''Tenth Planet''|''The Tenth Planet''}}
|{{Sort|6/2|Spring 1944}}
|By Joseph Samachson.  Magazine version titled "Days of Creation".
|-
|2457
|''Captain Future and the Space Emperor''
|{{sort|1/1|Winter 1940}}
|
|}

==Notes==
{{reflist|group=note}}

==References==
<references />

==Sources==
* {{Cite book|title = Encyclopedia of Science Fiction|publisher = Octopus Books|year = 1978|isbn = 0-7064-0756-3|location = London|editor-last = Holdstock|editor-first = Robert|last = Ashley|first = Michael|chapter = Pulps and magazines}}
* {{Cite book|title = Science Fiction, Fantasy, and Weird Fiction Magazines|publisher = Greenwood Press|year = 1985|isbn = 0-313-21221-X|location = Westport CT|pages = 155–157|editor-last = Tymn|editor-first = Marshall B.|editor2-last = Ashley|editor2-first = Mike|chapter = ''Captain Future''|last = Ashley|first = Michael|last2=Ewald|first2=Robert}}
* {{Cite book|title = Science Fiction and Fantasy Authors: A Bibliography of First Printings of Their Fiction and Selected Nonfiction|last = Currey|first = L.W.|publisher = G.K. Hall & Co.|year = 1979|isbn = 0-8161-8242-6|location = Boston}}
* {{Cite book|title = Encyclopedia of Science Fiction|last = Edwards|first = Malcolm|publisher = St. Martin's Press, Inc.|year = 1992|isbn = 0-312-09618-6|location = New York|last2 = Nicholls|first2 = Peter|chapter = SF Magazines|editor-last = Clute|editor-first = John|editor2-first = Peter|editor2-last = Nicholls}}
* {{Cite book|title = The World Wrecker: An Annotated Bibliography of Edmond Hamilton|last = Gombert|first = Richard W.|publisher = Wildside Press|year = 2009|issn = 0749-470X}}
* {{Cite book|title = Men of Tomorrow|last = Jones|first = Gerard|publisher = Basic Books|year = 2005|isbn = 978-0-465-03657-8|location = Cambridge MA}}
*{{Cite book|title = Seekers of Tomorrow|last = Moskowitz|first = Sam|publisher = Hyperion|year = 1974|isbn = 0-88355-158-6|location = Westport CT|origyear = 1966}}
* {{Cite book|title = Science Fiction, Fantasy, and Weird Fiction Magazines|publisher = Greenwood Press|year = 1985a|isbn = 0-313-21221-X|location = Westport CT|pages = 158|editor-last = Tymn|editor-first = Marshall B.|editor2-last = Ashley|editor2-first = Mike|chapter = ''Captain Hazzard''|last = Weinberg|first = Robert|last2=Ewald|first2=Robert}}
* {{Cite book|title = Science Fiction, Fantasy, and Weird Fiction Magazines|publisher = Greenwood Press|year = 1985b|isbn = 0-313-21221-X|location = Westport CT|pages = 158–159|editor-last = Tymn|editor-first = Marshall B.|editor2-last = Ashley|editor2-first = Mike|chapter = ''Captain Zero''|last = Weinberg|first = Robert|last2=Ewald|first2=Robert}}
{{ScienceFictionPulpMagazines}}
{{Good article}}

== External links ==
*[http://www.isfdb.org/cgi-bin/pe.cgi?11390 Complete index] at [[Internet Speculative Fiction Database]]

[[Category:Defunct science fiction magazines of the United States]]
[[Category:Pulp magazines]]
[[Category:Magazines established in 1940]]
[[Category:Magazines disestablished in 1944]]
[[Category:Science fiction magazines established in the 1940s]]
[[Category:Magazines published in Illinois]]
[[Category:Magazines published in New York]]