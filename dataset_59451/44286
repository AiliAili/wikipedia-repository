{{Infobox airport 
| name = RAF Worthy Down <BR> RNAS Worthy Down (HMS Kestrel) <BR> HMS Ariel
| nativename = [[File:Ensign of the Royal Air Force.svg|90px]][[File:Naval Ensign of the United Kingdom.svg|90px]]
| nativename-a = 
| nativename-r =   
| image = 
| image-width = 
| caption = 
| IATA =    
| ICAO =
| type = Military   
| owner = [[Air Ministry]] 1917-1939<BR> [[Admiralty]] 1939-1960
| operator = [[Royal Air Force]] <BR> [[Royal Navy]]
| city-served =    
| location = [[South Wonston]], [[Hampshire]]
| built = 1917
| used = 1917-1960
| elevation-f = 338
| elevation-m = 100
| coordinates = {{Coord|51|06|37|N|001|19|08|W|region:GB_type:airport|display=inline,title}}
| latd =51  | latm =06  | lats =41  | latNS = N
| longd=001 | longm=19  | longs=10  | longEW= W
| coordinates_type       = airport
| pushpin_map            = Hampshire
| pushpin_label          = RAF Worthy Down
| pushpin_map_caption    = Location in Hampshire
| website =    
| r1-number = N/S
| r1-length-f = 2,310
| r1-length-m = 704
| r1-surface = [[Grass]]
| r2-number = E/W
| r2-length-f = 4,200
| r2-length-m = 1,280
| r2-surface = Grass
| stat-year =    
| stat1-header =    
| stat1-data =    
| stat2-header = 
| stat2-data =    
| footnotes =   
}}
'''RAF Worthy Down''', was  a [[Royal Air Force]] station built located {{Convert|3.5|mi}} north west of [[Winchester]], [[Hampshire]] the airfield was also known as '''RNAS Worthy Down''' (HMS Kestrel) and HMS Ariel. The Royal Navy used the airfield from 1939 until 1942. Being completed in 1918, the airfield was first manned by [[No. 58 Squadron RAF]]. The airfield remained in use throughout the Second World War and was made into an engineering school in 1952. The airfield was in use until 1960. The station is now [[Worthy Down Barracks]].

==History==

The site was first used as a military establishment when the [[War Office]] acquired the site for a Wireless and Observers School in 1917 before changing to the school of Army Co-operation in 1918 on the site of the Winchester Racecourse. In 1918 an airfield was built for the [[Royal Flying Corps]] (RFC), but before it was completed the RFC was amalgamated with the [[Royal Naval Air Service]] to form the [[Royal Air Force]].<ref name="AW">{{cite web|url=http://www.atlantikwall.co.uk/atlantikwall/hampshire/worthey-down01/html/page01.htm |title=RAF/RNAS Worthy Down, Hampshire |publisher=Atlantik Wall|accessdate=24 May 2012}}</ref><ref name="Tripod">{{cite web|url=http://daveg4otu.tripod.com/airfields/wdn.html |title=Worthy Down |publisher=Daveg4otu |accessdate=24 May 2012}}</ref> Nonetheless, the RFC do seem to have operated there to some degree prior to amalgamation, as Lieutenant Harold Percy Dawson, RFC, was killed in an air accident stated to be at Worthy Down on the 9th March 1918. <ref>http://www.rafmuseumstoryvault.org.uk/archive/?7000269264</ref>

===Royal Air Force===
The first squadron to use the airfield was [[No. 58 Squadron RAF]] which was reformed there on 1 April 1924. The squadron flew the [[Vimy]] and the [[Virginia]] before moving to [[RAF Upper Heyford]] on 13 January 1936.<ref name="Jefford2001p43">{{Harvnb|Jefford|2001|p=43.}}</ref> On 7 April 1927 [[No. 7 Squadron RAF]] moved from [[RAF Bircham Newton]] and stayed until 3 September 1936 flying the Virginia IX/X and the Heyford II/III before moving to [[RAF Finningley]].<ref name="Jefford2001p26">{{Harvnb|Jefford|2001|p=26.}}</ref> During this [[No. 102 Squadron RAF]] formed at the airfield flying the Heyford II/III on 1 October 1935 before leaving on 3 September 1936 moving to RAF Finningley.<ref name="Jefford2001p54">{{Harvnb|Jefford|2001|p=54.}}</ref> On 1 October 1939 the same day as when 102 Sqn formed [[No. 215 Squadron RAF]] also reformed at the airfield but they flew the [[Virginia]] X instead and left on 14 March 1936 moving to [[RAF Upper Heyford]].<ref name="Jefford2001p71">{{Harvnb|Jefford|2001|p=71.}}</ref>

On 8 August 1936 [[No. 49 Squadron RAF]] moved from [[RAF Bircham Newton]] flying the [[Hawker Hind]] before leaving on 14 March 1938 moving to [[RAF Scampton]].<ref name="Jefford2001p41">{{Harvnb|Jefford|2001|p=41.}}</ref> The next squadron to join was [[No. 35 Squadron RAF]]. The squadron moved to RAF Worth Down on 26 August 1936 flying the [[Fairey Gordon]], Vickers Wellesley and the [[Fairey Battle]] before moving to [[RAF Cottesmore]] on 20 April 1938.<ref name="Jefford2001p37">{{Harvnb|Jefford|2001|p=37.}}</ref> The last squadron to join RAF Worthy Down was [[No. 207 Squadron RAF]] which had moved to the airfield on 29 August 1936 flying the Gordon and Wellesley before leaving on 20 April 1938 to [[RAF Cottesmore]].<ref name="Jefford2001p69">{{Harvnb|Jefford|2001|p=69.}}</ref><ref name="ABCT">{{cite web|url=http://www.abct.org.uk/airfields/worthy-down |title=Worthy Down |publisher=Airfields of Britain Conservation Trust|accessdate=24 May 2012}}</ref>

The only Royal Air Squadron not to use the airfield when it was under RAF control was the Southampton University Air Squadron which joined in 1945 flying the [[Tiger Moth]] before leaving during 1946.<ref name="Lake1999p187">{{Harvnb|Lake|1999|p=187.}}</ref>

====Station commanders====
{| class="wikitable sortable"
|-
! Commander !! Dates
|-
| 1 Dec 1926 || Gp Capt Hon J D Boyle
|-
| 4 Apr 1929 || Gp Capt F K Haskins
|-
| 6 Aug 1929 || Gp Capt [[Charles Edmonds|C H K Edmonds]]
|-
| 7 Aug 1931 || Gp Capt J R W Smyth-Pigott
|-
| 16 Jan 1934 || Wg Cdr/Gp Capt A A B Thomson
|-
| 6 Nov 1936 || Wg Cdr C H Keith
|-
| 22 Aug 1937 || Wg Cdr W Underhill
|}
*data from <ref>{{cite web|url=http://www.rafweb.org/Stations/Station%20OCs-SE.htm#Worthy |title=RAF Station Commanders - South East England - Worthy Down |publisher=Air of Authority - A History of RAF Organisation|accessdate=24 May 2012}}</ref>

===Royal Navy===
The site was recommissioned by the [[Royal Navy]] in 1939 as HMS Kestrel and used as a flying station by the [[Fleet Air Arm]]. During this time the site was featured on the news when [[Lord Haw-Haw]] (William Joyce) claimed HMS Kestrel was sunk by the [[Kriegsmarine]].<ref name="Helis">{{cite web|url=http://www.helis.com/database/heliport/uk_worthy_down |title=Worthy Down  |publisher=Helis.com |accessdate=24 May 2012}}</ref> In 1950 it was placed in a state of care and maintenance until 1952 when it was re-established as HMS Ariel II and used as an engineering training school.<ref name="Tripod"/>

The following squadrons were based at Worthy Down at some point:

*[[700 Naval Air Squadron]] between June 1945 and 1945
*[[734 Naval Air Squadron]] used the airfield between February 1944 and sometime in 1945 with the [[Armstrong Whitworth Whitley]] GRVII.
*[[739 Naval Air Squadron]] (BADU) between September 1943 and September 1944 with the [[Airspeed Oxford]].
*[[755 Naval Air Squadron]] formed at the airfield during 1939 and flew various aircraft including the [[Blackburn Shark]], [[Hawker Hart#Osprey|Hawker Osprey]], [[Westland Lysander]] and [[Curtiss SO3C Seamew]].
*[[756 Naval Air Squadron]] - 1939 - 1943
*[[757 Naval Air Squadron]] - 1939 - 1943
*[[763 Naval Air Squadron]] - December 1939 - June 1940
*763 (FAA Pool) Naval Air Squadron - February - July 1941
*[[800 Naval Air Squadron]] - 1938 - 1939
*[[803 Naval Air Squadron]] - 21 November 1938
*[[806 Naval Air Squadron]] - May 1940
*[[807 Naval Air Squadron]] - 15 September 1940
*[[808 Naval Air Squadron]] - 1 July 1940 - 5 September 1940
*[[811 Naval Air Squadron]] - October 1939
* [[815 Naval Air Squadron]] - 15 October 1939 - May 1940
*[[822 Naval Air Squadron]] - October 1939
*[[848 Naval Air Squadron]] - November 1959 - March 1960
*Air Electrical School - June 1952 - 1 November 1960

Additionally [[Supermarine]] used the airfield in the development of the [[Supermarine Spitfire|Spitfire]] from December 1940 to March 1944.<ref name="Tripod"/>

==Current use==

The technical site is now [[Worthy Down Barracks]] with the runway area open grassland.<ref name="ABCT"/>

==See also==
* [[Worthy Down railway station]]
* [[Worthy Down Barracks]]
* [[List of former Royal Air Force stations]]

==References==

===Citations===
{{reflist|2}}

===Bibliography===
*{{wikicite|ref={{harvid|Jefford|2001}}|reference=Jefford, C.G, [[Order of the British Empire|MBE]],BA,RAF (Retd). ''RAF Squadrons, a Comprehensive Record of the Movement and Equipment of all RAF Squadrons and their Antecedents since 1912''. Shrewsbury, Shropshire, UK: Airlife Publishing, 2001. ISBN 1-84037-141-2.}}
*{{wikicite|ref={{harvid|Lake|1999}}|reference=Lake, A ''Flying Units of the RAF''. Shrewbury,  Airlife Publishing Ltd., 1999. ISBN 1-84037-086-6.}}

==External links==
{{commons category|RAF Worthy Down}}
* [http://www.airfieldinformationexchange.org/community/showthread.php?2495-Worthy-Down Airfield Information Exchange - RAF Worthy Down]
{{Royal Air Force}}

{{DEFAULTSORT:Worthy Down}}
[[Category:Royal Air Force stations in Hampshire]]
[[Category:Royal Air Force stations of World War II in the United Kingdom]]
[[Category:Buildings and structures in Hampshire]]
[[Category:Airports in Hampshire]]