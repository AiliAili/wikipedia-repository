'''''GIGA Journal Family''''' is a publishing initiative of the [[GIGA German Institute of Global and Area Studies]] that brings together four international area studies journals. It comprises the GIGA journals ''[[Africa Spectrum]]'', ''[[Journal of Current Chinese Affairs]]'', ''[[Journal of Politics in Latin America (JPLA)]]'' and ''[[Journal of Current Southeast Asian Affairs]]'' offering original research and empirical analysis on contemporary politics, society and economy of [[Africa]], [[China]], [[Latin America]] and [[Southeast Asia]].

==Publishing Concept==
The ''GIGA Journal Family'' is funded by the German Research Foundation (Deutsche Forschungsgemeinschaft, DFG) as a pilot project in [[Open access (publishing)|open-access]] publishing. Since the beginning of 2009 all socio-scientific journals of the GIGA German Institute of Global and Area Studies have been transformed into open-access journals which means their full content is freely online accessible without time delay or cost to the reader. In addition, it remains available in traditional printed format.
To ensure the journals' quality, all essays are evaluated in a double-blind peer-review process.

==External links==
* [http://hup.sub.uni-hamburg.de/giga/journal-family/index GIGA Journal Family]
* [http://www.giga-hamburg.de/english/index.php?file=giga.html&folder=giga GIGA German Institute of Global and Area Studies]
* [http://hup.sub.uni-hamburg.de/giga/afsp Africa Spectrum]
* [http://hup.sub.uni-hamburg.de/giga/jcca Journal of Current Chinese Affairs]
* [http://hup.sub.uni-hamburg.de/giga/jpla Journal of Politics in Latin America]
* [http://hup.sub.uni-hamburg.de/giga/jsaa Journal of Current Southeast Asian Affairs]

[[Category:English-language journals]]
[[Category:German-language journals]]
[[Category:Open access journals]]
[[Category:Political science journals]]
[[Category:Organizations established in 2009]]
[[Category:International relations journals]]
[[Category:Area studies journals]]