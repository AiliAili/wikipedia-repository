{{Infobox Bibliographic Database
|title = Energy Citations Database  
|image = 
|caption = 
|producer = [[United States Department of Energy]] - [[Office of Scientific and Technical Information]]
|country = United States
|history = 2001-present 
|languages = English 
|providers = Regional Federal Depository Libraries  
|cost = Free
|disciplines = Multidisciplinary science 
|depth = full text, bibliographic citation, title, creator/author, subject, identifier numbers, publication date, system entry date, resource/document type, research organization, sponsoring organization, and/or combinations thereof
|formats = citations of literature, citations to report literature, conference papers, journal articles, books, dissertations, and patents. 
|temporal = 1943- present
|geospatial = North America 
|number = 2.6 million +
|updates = regular
|p_title =
|p_dates =
|ISSN =
|web = http://www.osti.gov/energycitations/index.jsp 
|titles = http://www.osti.gov/energycitations/availability.jsp  
}}
The '''''Energy Citations Database (ECD)'''''  was created in 2001 in order to make [[scientific literature]] citations, and [[electronic documents]],  publicly accessible from [[U.S. Department of Energy]] (DOE), and its  predecessor agencies, at no cost to the user. This [[database]] also contains all the unclassified materials from '''Energy Research Abstracts'''. Classified materials are not available to the public. ''ECD'' does include the unclassified, unlimited distribution [[scientific]] and [[Technology|technical]] reports from the Department of Energy and its predecessor agencies, the [[United States Atomic Energy Commission|Atomic Energy Commission]] and the [[Energy Research and Development Administration]].  The database is usually updated twice per week.<ref name=ECDhome/><ref name=FAQs/>

''ECD'' provides free access to over 2.6 million [[science research citation]]s with continued growth through regular updates. There are over 221,000 [[electronic document]]s, primarily from 1943 forward, available via the database. Citations and documents are made publicly available by the Regional Federal Depository Libraries. These institutions  maintain and make available DOE [[scientific literature|research literature]], providing access to non‑electronic documents prior to 1994, and electronic access to more recent documents.<ref name=ECDhome/><ref name=FAQs/>

''ECD'' was created and developed by DOE's [[Office of Scientific and Technical Information]] with the science-attentive citizen in mind. It contains [[energy]] and energy‑related [[scientific]] and [[Technology|technical]] information collected by the DOE and its predecessor agencies.<ref name=ECDhome/><ref name=FAQs>
{{Cite web| last =Energy Citations Database| title =ECD Frequently Asked Questions (FAQs) 
  | publisher =U.S. DOE| date =June 11, 2010| url =http://www.osti.gov/energycitations/faq.jsp#content 
  | accessdate =2010-06-20}}</ref>

==Scope==
Topics, or subjects, and Department of Energy disciplines of interest  in ''Energy Citations Database'' (ECD) are wide-ranging. [[Scientific]] and [[research|technical research]] encompass [[chemistry]], [[physics]], materials, [[environmental science]], [[geology]], [[engineering]], [[mathematics]], [[climatology]], [[oceanography]], [[computer science]], and [[interdisciplinary|related disciplines]]. It includes bibliographic citations to report [[scientific literature]], [[academic conference|conference papers]], [[Scientific journal|journal articles]], [[book]]s, [[dissertation]]s, and [[patent]]s. <ref name=ECDhome>
{{Cite web| last =U.S. Dept. of Energy| title =Energy Citations Database - Home page| publisher =DOE Office of Science| date =June 11, 2010| url =http://www.osti.gov/energycitations/index.jsp | accessdate =2010-06-17}}</ref>

===Stated capabilities===
[[Bibliographic]] citations for scientific and technical information dating from 1943 to the present day. Search capabilities include full text, bibliographic citation, title, creator/author, subject, identifier numbers, publication date, system entry date, resource/document type, research organization, sponsoring organization, and/or any combination of these.

Commensurate with the above search capabilities is sorting results by various means. 
Results can be sorted by relevance, publication date, system entry date, resource/document type, title, research organization, sponsoring organization, or the unique Office of Scientific Information  (OSTI) Identifier. Furthermore, acquiring a count of search results, combined with a link to the actual results is available.  

*ability to receive weekly Alerts in topics of interest; 
*information about acquiring a non-electronic document

==Research and database in  predecessor agencies==
Since the late 1940s, the Office of Scientific and Technical Information (OSTI) and its predecessor organizations have been responsible for the management of scientific and technical information (STI) for the Department of Energy (DOE) and its predecessor agencies, the Atomic Energy Commission (AEC) and the Energy Research and Development Administration (ERDA). Growth and development of STI management has incorporated planning, developing, maintaining, and administering all services and facilities required to accomplish the dissemination of scientific and technical information for the encouragement of scientific progress.<ref name=Abtindx/> 

===Atomic Energy Commission===
In 1942, the [[Manhattan Project]] was established by the [[United States Army]] to conduct [[atomic research]] with the goal of ending [[World War II]]. This research was performed in a manner that helped to cement the ongoing bond between basic [[scientific research]] and [[national security]]. After the war, the authority to continue this research was transferred from the Army to the United States Atomic Energy Commission (AEC) through the [[Atomic Energy Act]] of 1946. This Act was signed into law by President [[Harry S. Trumanun]]  on August 1, 1946, and entrusted the AEC with the government monopoly in the field of atomic research and development.<ref name=Abtindx/><ref name=fstexcerpt/> 

===Energy Reorganization Act=== 
The [[Energy Reorganization Act]] of 1974 abolished the Atomic Energy Commission and established the Energy Research and Development Administration (ERDA). ERDA was created to achieve two goals: 

First was to focus the Federal Government’s [[energy research]] and [[energy development|development]] activities within a unified agency whose major function would be to promote the speedy development of various energy technologies. The second, was to separate nuclear licensing and regulatory functions from the development and production of nuclear power and weapons. <ref name=Abtindx/><ref name=fstexcerpt> ''Manhattan Project: Making the Atomic Bomb''; 1999 edition.; F.G. Gosling, January 1, 1999</ref> 

===Department of Energy=== 
To achieve a major Federal energy reorganization, the Department of Energy (DOE) was activated on October 1, 1977. DOE became the twelfth cabinet-level department in the Federal Government and brought together for the first time most of the government's energy programs and defense responsibilities that included the design, construction, and testing of nuclear weapons. Uniting seemingly disparate organizations and programs reflected a common commitment to performing first rate science and technology. The Department of Energy sought–and continues to seek–to be one of the Nation’s premier science and technology organizations. <ref name=Abtindx> [http://www.osti.gov/energycitations/about.jsp  About Energy CitationsDatabase (ECD)]. DOE. Accessed 2010-06-17.</ref><ref name=2ndexcerpt>''Department of Energy 1977–1994: A Summary History''; T.R. Fehner and J.M. Holl; November 1, 1994</ref>

==Features section==
The ''Energy Citations Database'' features noteworthy topics of discussion in the features section.

===Supercomputer Modeling and Visualization===
Supercomputer Modeling and Visualization is a notable featured topic in June 2010. It is discussed in three DOE research areas. Two of these are discussed below in the context of the featured topic.
 
====SciDAC====
SciDAC is a specially designed program within the [[U.S. Department of Energy|Office of Science of the U.S. Department of Energy]]. It enables [[scientific discovery]] through [[supercomputer|advanced computing]] (SciDAC), and is driven by a spirit of collaboration. Discipline scientists, applied [[mathematician]]s, and [[computer scientist]]s are working together to maximize use of the most sophisticated high-power computers for scientific discovery. Research results are promgulated through the ''SciDAC review magazine''. ''Supercomputer Modeling and Visualization'' is covered in the Spring 2010 issue of this magazine.<ref>and ''SciDAC review magazine''. Number 17 Spring 2010</ref>

====VACET program====
A perspective on supercomputer modeling and visualization is also discussed in the DOE's VACET program.

The Visualization and Analytics Center for Enabling Technologies (VACET) focuses on leveraging scientific visualization and [[computational model|analytics software]] technology as an enabling [[technology]] for increasing [[computing science|scientific productivity]] and insight. Advances in computational technology have resulted in an "information big bang," which in turn has created a significant data understanding challenge. This challenge is widely acknowledged to be one of the primary bottlenecks in contemporary [[science]]. Using an organizational model such as VACET, we are well positioned to respond to the needs of a diverse set of scientific stakeholders in a coordinated fashion using a range of [[Information graphics|visualization]], mathematics, [[statistic]]s, computer and [[computational science]] and [[data management]] technologies.

The vision of VACET is to adapt, extend, create when necessary, and deploy visual data analysis solutions that are responsive to the needs of DOE's computational and experimental scientists. Our center is engineered to be directly responsive to those needs and to deliver solutions for use in DOE's large open computing facilities. The research and development directly target data understanding problems provided by our scientific application stakeholders. VACET draws from a diverse set of visualization technology ranging from production quality applications and application frameworks to state-of-the-art algorithms for visualization, analysis, analytics, data manipulation, and data management. Our goal is to respond to the urgent needs of the scientific community by providing significant, production-quality technology to aid in data understanding.<ref name=VACET>[http://www.vacet.org/about.html  About VACET] SciDAC of DOE. 2010.</ref>

==Regional Federal Depository Libraries==
{{main|Federal Depository Library Program}}
[[Image:USA Federal depository library logo.svg|right|thumb|Logo for a Federal Depository Library]]
There are nearly 1,250 depository libraries throughout the United States and its territories.  Access to all documents (hundreds of thousands) is no-fee access. Expert assisted searches are available, on site. 

Federal depository libraries have been established by Congress to ensure that the American public has access to its Government's information.  The Federal Depository Library Program (FDLP) involves the acquisition, format conversion, and distribution of depository materials to libraries throughout the United States and the coordination of Federal depository libraries in the 50 states, the District of Columbia and U.S. territories.

Since 1813, depository libraries have safeguarded the public's right to know by collecting, organizing, maintaining, preserving, and assisting users with information from the Federal Government. Depository libraries provide local, no-fee access to Government information in an impartial environment with professional assistance.

The mission of the FDLP is to disseminate information products from all three branches of the Government to about 1,250 libraries nationwide at no cost.

The U.S. Government Printing Office administers the FDLP and serves to provide this network of libraries with the tools they need to keep America informed.<ref name=FDLP/>

===Federal depository library coverage===
Coverage generally encompasses:
*Health and Nutrition 
*Laws, Statistics, and Presidential Materials 
*Science and Technology 
*Business and Careers 
*Education 
*History 
*World Maps 

Available formats are publications, journals, electronic resources, microfiche, microfilm and various other formats encompassing hundreds of thousands of topics. <ref name=FDLP>
{{Cite web
 |title=About the Federal Depository Library Program 
 |publisher=U.S. Government Printing Office 
 |date=March 24, 2009 
 |url=http://www.gpoaccess.gov/libraries.html 
 |format=A service of the U.S. Government Printing Office. 
 |accessdate=2010-06-29 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20100707044550/http://www.gpoaccess.gov/libraries.html 
 |archivedate=2010-07-07 
 |df= 
}}</ref>

==References==
{{Reflist|35em}}
{{DoE}}<br/>
{{USGovernment}}

[[Category:United States Department of Energy]]
[[Category:Physics journals]]
[[Category:2001 establishments in the United States]]
[[Category:Bibliographic databases and indexes]]
[[Category:Scientific databases]]