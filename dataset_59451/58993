{{Infobox journal
| title = Monthly Weather Review
| cover = [[Image:Monthly Weather Review cover.jpg]]
| discipline = [[Atmospheric sciences]], [[meteorology]]
| abbreviation = Mon. Weather Rev.
| editor = David M. Schultz
| publisher = [[American Meteorological Society]]
| country = United States
| frequency = Monthly
| history = 1872–present
| openaccess = [[Delayed open access journal|Delayed]], after 2 years
| license =
| impact = 2.758
| impact-year = 2012
| website = http://www2.ametsoc.org/ams/index.cfm/publications/journals/monthly-weather-review/
| link1 = http://journals.ametsoc.org/toc/mwre/current
| link1-name = Online access
| link2 = http://ams.allenpress.com/perlserv/?request=get-archive&issn=1520-0493
| link2-name = Online archive
| ISSN = 0027-0644
| eISSN = 1520-0493
| OCLC = 436659104
| LCCN = 74648196
| CODEN = MWREAB
}}
The '''''Monthly Weather Review''''' is a [[peer-reviewed]] [[scientific journal]] published by the [[American Meteorological Society]]. It covers research related to analysis and prediction of observed and modeled circulations of the atmosphere, including technique development, [[data assimilation]], [[model validation]], and relevant case studies. This includes papers on numerical techniques and data assimilation techniques that apply to the atmosphere and/or ocean environment. The [[editor-in-chief]] is David M. Schultz ([[University of Manchester]]).

== History ==
The journal was established in July 1872 by the [[United States Army Signal Corps]]. It was issued by the Office of the Chief Signal Officer from 1872 until 1891. In 1891, the Signal Office's [[meteorology|meteorological]] responsibilities were transferred to the Weather Bureau under the [[United States Department of Agriculture]]. The Weather Bureau published the journal until 1970 when the Bureau became part of the newly formed [[National Oceanic and Atmospheric Administration]], which published it until the end of 1973. Since 1974, it has been published by the [[American Meteorological Society]].<ref name = "NOAA">[http://docs.lib.noaa.gov/rescue/mwr/data_rescue_monthly_weather_review.html Monthly Weather Review], [[National Oceanic and Atmospheric Administration]] Central Library.</ref>

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Current Contents]]/Physical, Chemical & Earth Sciences and the [[Science Citation Index]].<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-03-28}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.758.<ref name=WoS>{{cite book |year=2013 |chapter=Monthly Weather Review |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www2.ametsoc.org/ams/index.cfm/publications/journals/monthly-weather-review/}}
* [http://docs.lib.noaa.gov/rescue/mwr/data_rescue_monthly_weather_review.html NOAA Central Library] - Free access to articles prior to 1974

{{Journals published by the American Meteorological Society}}

[[Category:Meteorology journals]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1872]]
[[Category:1872 establishments in the United States]]
[[Category:American Meteorological Society academic journals]]


{{sci-journal-stub}}
{{climate-stub}}