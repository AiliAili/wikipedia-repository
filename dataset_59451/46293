{{Infobox company
| name = Astronics Max-Viz
| logo = Big Max-Viz-10566.jpg
| logo_size = thumb
| logo_alt =
| logo_caption = Max-Viz product logo
| logo_padding =
| image = San_Diego_Line_Boy.jpg
| image_size = thumb
| image_alt =
| image_caption = San Diego at night on the tarmac
| trading_name = 
| native_name = 
| native_name_lang =    <!-- Use ISO 639-1 code, e.g. "fr" for French. For multiple names in different languages, use {{lang|[code]|[name]}}. -->
| romanized_name = 
| former_name = 
| type = 
| traded_as = 
| ISIN =
| industry = Enhanced Vision Systems 
| genre =               <!-- Only used with media and publishing companies -->
| fate = 
| predecessor =         <!-- or: | predecessors = -->
| successor =           <!-- or: | successors = -->
| foundation =          {{start date|2001|05|31}} in [[Portland, Oregon|Portland]], United States 
| founder =              Dr. J. Richard Kerr 
| defunct =             <!-- {{end date|YYYY|MM|DD}} -->
| location_city = Portland
| location_country = USA
| coordinates =
| locations =           <!-- Number of locations -->
| area_served = USA
| key_people = 
| products = Max-Viz 1000, Max-Viz 1500, Max-Viz 2500 
| brands = 
| production = 
| services = 
| revenue = 
| operating_income = 
| net_income = 
| aum =                 <!-- Only for financial-service companies -->
| assets = 
| equity = 
| owner =               <!-- or: | owners = -->
| members =
| num_employees = 
| parent = 
| divisions = 
| subsid = 
| website =             <!-- {{URL|example.com}} -->
| footnotes = 
| intl =                <!-- "true" or "yes" if company is international, otherwise omit -->
| bodystyle = 
}}

'''Astronics Max-Viz''' is an American company founded in [[Portland, Oregon]] on May 31, 2001 as Max-Viz, Inc.<ref name="Oregon Business License">{{cite web|title=Oregon Business Entity Data|url=http://egov.sos.state.or.us/br/pkg_web_name_srch_inq.show_detl?p_be_rsn=841913&p_srce=BR_INQ&p_print=FALSE|publisher=State of Oregon|accessdate=8 January 2014}}</ref> to design, manufacture and certify Enhanced Vision Systems (“EVS”) primarily for use in the aerospace industry.  Max-Viz EVS devices present real-time images of the external environment on aircraft cockpit monitors to improve pilot situational awareness under circumstances where visibility is impaired by weather or darkness.  The company objective is to help the pilot see clearly and fly safely by providing visual information about where they are, where they are going and what is in their way.    The Max-Viz EVS captures and enhances thermal [[infrared]] signals and can be combined with visible light as well as other electromagnetic energy sources.  The company's systems are designed to be integrated with a variety of displays already in the aircraft cockpit.<ref name="Company Overview">{{cite web|title=KippsDeSanto & Co. Advises Max-Viz, Inc. on its Sale to Astronics Corporation|url=http://www.kippsdesanto.com/kippsdesanto-co-advises-max-viz-inc-on-its-sale-to-astronics-corporation/|publisher=KippsDeSanto}}</ref>

[[File:Sun Valley EVS Comparison.jpg|thumb|Sun Valley, Idaho at night]]

==History==
The company came into being as a result of two inventions that allowed for possibility of a small lightweight design for EVS that could be deployed onto small private and commercial aircraft and helicopters.

The first invention was an uncooled focal plane array licensed from [[Honeywell]] (called a [[microbolometer]]) that could detect thermal infrared radiation, did not require a cryogenic cooling system (as conventional systems required in the 1990s) and could convert the electromagnetic thermal impulses into signals that could be displayed on a monitor.  The second invention was a patented system to fuse video images from multiple sources developed by Dr. J. Richard Kerr (one of the original founders of Max-Viz).<ref name="Richard Kerr Founder">{{cite web|title=Executive Profile J. Richard Kerr Ph.D.|url=http://investing.businessweek.com/research/stocks/private/person.asp?personId=3688969&privcapId=3643754&previousCapId=3643754&previousTitle=Max-Viz,%20Inc.|publisher=Business Week}}</ref>  This invention <ref>{{cite web|title=US Patent 6,232,602|url=http://pdfpiw.uspto.gov/.piw?Docid=6232602&idkey=NONE&homeurl=http%3A%252F%252Fpatft.uspto.gov%252Fnetahtml%252FPTO%252Fpatimg.htm|publisher=US Patent and Trademark Office}}</ref> enabled signals from a visible light source to be combined with the signals from an infrared source (like the Honeywell focal plane array mentioned above) and present a picture which Astronics describe as "turning night into day".<ref>{{cite web|title=New infrared system turns night into day|url=http://www.aopa.org/News-and-Video/All-News/2013/January/10/New-infrared-system-turns-night-into-day.aspx|publisher=AOPA}}</ref>
The Max-Viz start-up was financed with initial investments from a number of Portland area angel investors, strategic investments from [[FLIR Systems]]<ref>{{cite web|title=FLIR Systems|url=http://investors.flir.com/releasedetail.cfm?ReleaseID=297684|publisher=FLIR Systems}}</ref>  and, later, with venture capital funding from OVP Venture Partners,<ref>{{cite web|title=List of OVP Investments|url=http://www.ovp.com/complete-list/|publisher=OVP Venture Partners}}</ref> Alexander Hutton Venture Partners,<ref>{{cite web|title=AHVP Portfolio Investments |url=http://www.ahvp.com/portfolio.html |publisher=AHVP |deadurl=yes |archiveurl=https://web.archive.org/web/20130822104820/http://ahvp.com/portfolio.html |archivedate=2013-08-22 |df= }}</ref> Montlake Capital,<ref>{{cite web|title=Portfolio Company News|url=http://archive.constantcontact.com/fs018/1102192932259/archive/1103516052688.html|publisher=Montlake Capital}}</ref> and Highway 12 Ventures.<ref>{{cite web|title=Highway 12 Portfolio Companies|url=http://www.highway12ventures.com/portfolio/our-companies/|publisher=Highway 12 Ventures}}</ref>  Max-Viz licensed the [[microbolometer]] design from [[Honeywell]] for use in aviation and initially licensed the fusion patent (developed by Dr. Kerr) from ''[[FLIR Systems]]''.  In 2012, the company was acquired by [[Astronics Corporation]] <ref name="Astronics Acquisition of Max-Viz">{{cite web|title=Astronics Corporation Acquires Enhanced Vision Systems Provider Max-Viz, Inc.|url=http://www.businesswire.com/news/home/20120731005386/en/Astronics-Corporation-Acquires-Enhanced-Vision-Systems-Provider#.UxDaCfldV8E|publisher=Business Wire}}</ref> (NASDAQ: ATRO) as a wholly owned subsidiary.<ref name="Max-Viz continues as wholly owned subsidiary">{{cite web|title=Portland's Max-Viz acquired in deal that could reach $18M|url=http://www.bizjournals.com/portland/news/2012/07/31/portlands-max-viz-acquired-in-deal.html|publisher=Portland Business Journal}}</ref>

By May 2011, the company had sold more than 1,000 EVS systems using 50+ aviation certifications on more than 200 different aircraft models including business jets, helicopters and various piston driven aircraft.<ref name="Shipments and Certifications">{{cite web|title=Max-Viz ships 1,000th system|url=http://generalaviationnews.com/2011/05/11/max-viz-ships-1000th-system/|publisher=General Aviation News}}</ref>  Among those companies that offer the Max-Viz EVS at the factory on selected aircraft models are [[AgustaWestland]],<ref name="Agusta EVS Installations">{{cite web|title=A109 Power Deliveries Commence To Saudi Aramco |url=http://www.agustawestland.com/node/3295 |publisher=Agusta Westland |deadurl=yes |archiveurl=https://web.archive.org/web/20140327221253/http://www.agustawestland.com/node/3295 |archivedate=2014-03-27 |df= }}</ref> [[Beechcraft]],<ref name="Beechcraft EVS Installations">{{cite web|title=Enhanced Vision Now Available for King Air 90, 200, 300; Hawker 700, 800 and 900 Available Soon |url=http://www.beechcraft.com/customer_support/hawker_beechcraft_services/Special_Promotions/MaxViz.aspx |publisher=Beechcraft |deadurl=yes |archiveurl=https://web.archive.org/web/20140327234905/http://www.beechcraft.com/customer_support/hawker_beechcraft_services/Special_Promotions/MaxViz.aspx |archivedate=2014-03-27 |df= }}</ref> [[Bell Helicopter]],<ref name="Bell EVS Installations">{{cite web|title=Astronics Max-Viz EVS approved for Bell 429|url=http://www.helicoptersmagazine.com/content/view/4077/38/|publisher=Helicopters Magazine}}</ref> ''[[Cessna]]'',<ref name=Cessna>{{cite web|title=Cessna Max-Viz Enhanced Vision System|url=http://www.flyingmag.com/photo-gallery/photos/cessna-max-viz-enhanced-vision-system|publisher=Flying}}</ref> [[Cirrus Aircraft]]<ref name="Max-Viz EVS on Cirrus">{{cite web|title=Astronics Delivers 600th Max-Viz Enhanced Vision System to Cirrus|url=http://www.dailyfinance.com/2012/10/08/astronics-delivers-600th-max-viz-enhanced-vision-s/|publisher=Daily Finance}}</ref>  and [[Eurocopter]].<ref name="Eurocopter EASA STC">{{cite web|title=Max-Viz Enhanced Vision Systems Approved by EASA for Eurocopters|url=http://www.aviationpros.com/press_release/10714442/max-viz-enhanced-vision-systems-approved-by-easa-for-eurocopters|publisher=Aviation Pros}}</ref>  Max-Viz continues as a standalone business unit of Astronics Corporation and focuses its resources on core competencies involving packaging & design, image fusion and certification/installation of EVS systems.

== Products ==

The company’s first product was the Max-Viz 1000<ref>{{cite web|title=Max-Viz EVS-1000 for Pilatus PC-12 |url=http://www.westair.com/images/pdfs/MAX-VIZ-EVS.pdf |publisher=Western Aviation |deadurl=yes |archiveurl=https://web.archive.org/web/20110109233616/http://westair.com/images/pdfs/MAX-VIZ-EVS.pdf |archivedate=2011-01-09 |df= }}</ref>  and was certified in 2003.<ref>{{cite web|title=Jet Aviation Equips first Falcon 50EX with Max-Viz 1000|url=http://www.flightglobal.com/news/articles/jet-aviation-equips-first-falcon-50ex-with-max-viz-evs-1000-174271/|publisher=Flight Global}}</ref>  It contained a single [[microbolometer]] to detect thermal radiation and was made of an exotic compound called [[Vanadium(V) oxide|Vanadium Oxide]] tuned to detect infrared waves in the 8 to 12 micron bandwidth.  The sensor had a 53-degree field of view (slightly wider than normal 30 degree human vision) and produced a video signal that could be fed to a monitor using a standard RS-170, the standard [[NTSC]] signal used in analog displays.  The Max-Viz 1000 system weighed about 5 pounds and required 10 watts during normal usage at 28 VDC.  This product was produced from 2003 to 2008.  The company’s second product was the Max-Viz 2500 and was certified in 2005.<ref name="EVS-2500 Introduction">{{cite web|title=Max-Viz Demonstrates New Level of Capabilities With Synthetic-Enhanced Vision System; U.S. Air Force Research Laboratories Supported Test Program of SE-Vision Systems Underway|url=http://www.businesswire.com/news/home/20050405005409/en/Max-Viz-Demonstrates-Level-Capabilities-Synthetic-Enhanced-Vision-System#.UxDhaPldV8E|publisher=Business wire}}</ref>  This product contained two sensors; a long wave microbolometer like the one used in the Max-Viz 1000 and a short wave sensor that could pick up the incandescent lights from the airport environment.  This system had a 30-degree field of view (equal to the field of view of human vision) and weighed 10 pounds.  This product was produced from 2005 to 2010.

The Max-Viz 1500 was introduced in 2008 was an enhancement of the Max-Viz 1000.<ref>{{cite web|title=Pilots Embracing Low Cost EVS|url=http://www.ainonline.com/aviation-news/nbaa-convention-news/2008-09-30/pilots-embracing-low-cost-evs|publisher=AIN Online}}</ref>  It offers an optical dual field of view (30° and 53°) and advanced image processing to avoid blooming and washout.<ref name="Max-Viz 1500 product specifications">{{cite web|title=Max-Viz 1500|url=http://max-viz.com/products/evs-1500/|publisher=Max-Viz}}</ref>  Both the Max-Viz 1000 and Max-Viz 1500 use the same housing making it easier for existing Max-Viz 1000 customers to upgrade and preserving the investment in FAA certifications.<ref name="16500 uses same housing as 1000">{{cite web|title=Sikorsky S-76 Gets Max-Viz EVS STC|url=http://www.helicoptersmagazine.com/content/view/1240/60/|publisher=Helicopters Magazine}}</ref>
The Max-Viz 600 was also introduced in 2008.<ref>{{cite web|title=EVS-600 available on Cirrus aircraft|url=http://www.avweb.com/news/aopa/AOPAExpo2008_CirrusAddsEnhancedVision_199136-1.html|publisher=Aviation news}}</ref>  This product was specifically designed for the General Aviation community and is housed in ultra-lightweight high impact plastic weighing less than 1.2 pounds.  The system is dual wavelength (infrared and visible light sensors) and uses a patented process to fuse the images.<ref name="Max-Viz 600 product specifications">{{cite web|title=Max-Viz 600|url=http://max-viz.com/products/evs-600/|publisher=Max-Viz}}</ref>

== References ==

{{reflist}}

[[Category:Avionics]]
[[Category:Avionics companies]]
[[Category:Companies based in Portland, Oregon]]