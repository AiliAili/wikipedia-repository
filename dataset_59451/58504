{{Infobox journal
| title = Journal of Sociology
| cover = [[File:Journal of Sociology.jpg]]
| editor = [[Alphia Possamai-Inesedy (academic)|Alphia Possamai-Inesedy]]
| discipline = [[Sociology]]
| formernames =
| abbreviation = J. Sociol.
| publisher = [[Sage Publications]] on behalf of [[The Australian Sociological Association]]
| country = Australia
| frequency = Quarterly
| history = 1965-present
| openaccess =
| license =
| impact = 1.455
| impact-year = 2013
| website = http://www.sagepub.com/journals/Journal201492/title
| link1 = http://jos.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jos.sagepub.com/content
| link2-name = Online archive
| JSTOR =
| OCLC = 38994786
| LCCN = sn98031957
| CODEN =
| ISSN = 1440-7833
| eISSN = 1741-2978
}}
The '''''Journal of Sociology''''' is a quarterly [[peer-reviewed]] [[academic journal]] covering [[sociology]] with a focus on Australia. The journal's [[editor-in-chief]] is Alphia Possamai-Inesedy ([[University of Western Sydney]]). It was established in 1965 and is published by [[Sage Publications]] on behalf of [[The Australian Sociological Association]].

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Scopus]] and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2013 [[impact factor]] is 1.455.<ref name=WoS>{{cite book |year=2014 |chapter=JOURNALNAME |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.sagepub.com/journals/Journal201492/title}}

[[Category:Sociology journals]]
[[Category:Publications established in 1965]]
[[Category:SAGE Publications academic journals]]
[[Category:Quarterly journals]]
[[Category:English-language journals]]