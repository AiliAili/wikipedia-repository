{{Infobox person
|name          = Patsy Conroy
|image         = PatsyConroy.jpg
|image_size    = 
|caption       = An illustration of Patsy Conroy from "Criminals of America" (1876) by Philip Farley.
|birth_name    = Patrick Conway
|birth_date    = c. 1846
|birth_place   = 
|death_date    = 
|death_place   = 
|death_cause   = 
|resting_place = 
|resting_place_coordinates = 
|residence     = [[Bowery]], [[New York City, New York]]
|nationality   = [[Irish-American]]
|other_names   = Patsey Conroy
|known_for     = New York burglar and river pirate who founded the [[Patsy Conroy Gang]]
|education     =
|alma_mater    = 
|employer      = 
|occupation    = Saloon keeper
|home_town     = 
|title         = 
|salary        = 
|networth      = 
|height        = {{height|ft=5|in=7}}
|weight        = {{convert|150|lb|kg|abbr=on|lk=on}}
|term          = 
|predecessor   = 
|successor     = 
|party         = 
|boards        = 
|religion      = [[Catholic]]
|spouse        = 
|partner       = 
|children      = 
|parents       = 
|relations     = 
|signature     = 
|website       = 
|footnotes     = 
}}

'''Patrick Conway''' (c. 1846&ndash; ?), commonly known by his alias '''Patsy''' or '''Patsy Conroy''', was an American burglar and river pirate. He was the founder and leader of the [[Patsy Conroy Gang]], a gang of river pirates active on the [[New York Harbor|New York waterfront]] in the old Fourth Ward and Corlears' Hook districts during the post-American Civil War era. 

Fellow members of his gang, Denny Brady and Larry Griffin, later assumed control but he participated in their raiding towns in [[Westchester County]]. He and Larry Griffin were eventually convicted of robbing the home of [[Robert Emmet]] in White Plains in 1874, as well as Denny Brady in [[Catskill (town), New York|Catskill]] the same year, resulting in the gang's breakup.

==Biography==
{{main|Patsy Conroy Gang}}
Conroy was described in Philip Farley's ''Criminals of America, Or, Tales of the Lives of Thieves: Enabling Every One to be His Own Detective'' (1876) as being ''"..of Irish origin and a burglar. He is 30 years of age, five feet seven inches high, has black hair, gray eyes, several [[India ink|India-ink]] marks on his hand, and weights 150 lbs.''<ref>Farley, Philip. ''Criminals of America, Or, Tales of the Lives of Thieves: Enabling Every One to be His Own Detective''. New York: Author's Edition, 1876. (pg. 350)</ref> He became known as an experienced river pirate in the New York underworld and "operated with great success" along the old Fourth Ward waterfront.<ref name="Asbury">[[Herbert Asbury|Asbury, Herbert]]. ''The Gangs of New York: An Informal History of the New York Underworld''. New York: Alfred A. Knopf, 1928. (pg. 73-76) ISBN 1-56025-275-8</ref> In or around 1858, Conroy boarded a [[brigantine]] anchored at the foot of Jefferson Street with Bill Cummings and two other men. Capturing the watchman, whom they bound and gagged, Conroy led his companions to the main cabin where they subdued the 16-man crew and successfully looted the ship.<ref name="Moss">[[Frank Moss (lawyer)|Moss, Frank]]. ''The American Metropolis from Knickerbocker Days to the Present Time''. London: The Authors' Syndicate, 1897. (pg. 104-108)</ref> Ten years later, he was implicated with Larry Griffin and Tommy Shea of the murder of a [[first mate]] during the robbery a ship anchored of [[Ryker's Island]].<ref name="Sutton">Sutton, Charles; James B. Mix and Samuel A. Mackeever, ed. ''The New York Tombs: Its Secrets and Its Mysteries. Being a History of Noted Criminals, with Narratives of Their Crimes''. San Francisco: A. Roman & Co., 1874. (pg. 474-476, 479)</ref>

On one occasion, Conroy entered a Bowery saloon one night with Cummings, [[Boiled Oysters Malloy]] and Charley Mosher. Each of the four had been wounded, Conroy having been shot in the arm and Cummings in the chest. Jim McGuire, a Bowery thief, arrived shortly after with a bundle of stolen goods. Upon seeing the gangsters, McGuire ordered them a round of drinks. When Cummings complained about the [[whiskey]] they had been served, saying ''"they ought to be served [[Champagne (wine)|champaigne]]"'', McGuire "good-naturedly" changed their order. Conroy then turned to McGuire a demanded a share of the young thief's merchandise. McGuire offered the men $10 each but Cummings scoffed at what he called "chicken-feed" and "unworthy of being offered to companions in distress". McGuire then walked away but was stopped by the men, received a punch in the stomach, and his goods stolen. Before leaving, Conroy told police ''"Officer, there's a man who has fallen off a car; better take him up"''.<ref name="Moss"/> 

Conroy ran a [[basement]] [[dive bar]] in the Bowery which was advertised as a restaurant but, in actuality, was a front for his gang's headquarters. The building, reputed to be "an arsenal and always garrisoned", was avoided by police. Only one officer, detective Holly Lyons, had "dared to make an arrest there" and police usually waited outside to arrest a suspect instead of entering the basement bar.<ref name="Moss"/>

In the early 1870s, Conroy moved his gang to the Corlears' Hook district. Shortly after his arrival, Conroy started recruiting many of the area'a infamous waterfront thieves and criminals including [[Socco the Bracer]], Scotchy Lavelle, Johnny Dobbs, [[Kid Shanahan]], [[Pugsey Hurley]], [[Wreck Donovan]], Tom The Mick, Beeny Kane, Piggy Noles, Billy Woods, Bum Mahoney, Denny Brady and Larry Griffin. Brady and Griffin would later become joint leaders of the gang.<ref name="Asbury"/><ref name="Moss"/><ref name="Sutton"/>

Under his leadership, the Patsy Conroys dominated the New York waterfront district during the post-American Civil War era and remained one of the last active gangs of river pirates prior to the formation of the [[Steamboat Squad]]. Over time, Denny Brady and Larry Griffin gradually took over running the gang and may not have been actively involved some of the Patsy Conroys' more infamous crimes such as the ''Elizabeth'' or ''Mattan'' robberies during 1873. The failed robbery of the brig ''Elizabeth'' ended in the death of his chief lieutenant Socco the Bracer. Conroy was named as a suspect in the latter robbery, one which resulted in the wrongful imprisonment of fellow river pirates Tommy Dagan and Billy Carroll,<ref name="Walling">Walling, George W. ''Recollections of a New York Chief of Police: An Official Record of Thirty-eight Years as Patrolman, Detective, Captain, Inspector and Chief of the New York Police''. New York: Caxton Book Concern, 1887.</ref><ref>{{cite news |title=River Pirates Caught.; The Exploits Of Saturday Night. Three Of The Thieves Captured By Capt. Williams And His Detectives -- How The Pirates Operated -- The Manner Of Their Detection -- Other Arrests Expected |trans_title= |url=https://query.nytimes.com/mem/archive-free/pdf?res=9C0DEFDA1E3FE73BBC4151DFB466838D669FDE |format= PDF|agency= |newspaper=[[New York Times]] |id= |pages= |page= |date=1876-02-29 |accessdate=31 August 2009 |language= |quote= |archiveurl= |archivedate= }}</ref> and he and the others began relying more heavily on raiding isolated towns in [[Westchester County, New York|Westchester County]] along [[Long Island Sound]] and occasionally [[Long Island|the island itself]]. During the last two years of his criminal career, the Patsy Conroys "kept these hamlets in a chronic condition of terror" until 1874 when he and Griffin were arrested by famed detectives Richard King and Holly Lyons for robbing the home of Robert Emmett in [[White Plains, New York|White Plains]] (or [[New Rochelle, New York|New Rochelle]]).<ref>{{cite news |title=Once Famous Sleuth Dead.; Richard King, Who Caught the $40,000 Express Wagon Robbers |trans_title= |url=https://query.nytimes.com/mem/archive-free/pdf?res=9805EFDE143DE733A25754C2A9669D946497D6CF |format= PDF|agency= |newspaper=[[New York Times]] |location= |id= |pages= |page= |date=1905-10-27 |accessdate=31 August 2009 |language= |quote= }}</ref><ref>{{cite news |first= |last= |authorlink= |author= |coauthors= |title=City And Suburban News. Westchester County.|url=https://query.nytimes.com/mem/archive-free/pdf?res=9D03E6DF1030EF34BC4D53DFB566838F669FDE |format= PDF|agency= |newspaper=[[New York Times]] |publisher= |location= |id= |pages= |page= |date=1874-03-05 |accessdate=31 August 2009 |language= |quote= }}</ref> Brady was also convicted of a similar charge against Abraham Post in [[Catskill (town), New York|Catskill]] that same year.<ref>{{cite news |first= |last= |authorlink= |author= |coauthors= |title=Modern Bandits.; Capture Of Notorious Burglars. The Perpetrators Of The Catskill, New Rochelle, And Staten Island Outrages Arrested With All Their Implements |trans_title= |url=https://query.nytimes.com/mem/archive-free/pdf?res=9F06E1DD153FE63ABC4E53DFB766838F669FDE |format= PDF|agency= |newspaper=[[New York Times]] |id= |pages= |page= |date=1874-01-06 |accessdate=31 August 2009 |language= |quote= }}</ref> Held at the White Plains jail,<ref>{{cite news |first= |last= |authorlink= |author= |coauthors= |title=City And Suburban News. Westchester County.|url=https://query.nytimes.com/mem/archive-free/pdf?res=9C03E2D8173BEF34BC4F52DFB466838F669FDE |format= PDF|agency= |newspaper=[[New York Times]]|id= |pages= |page= |date=1874-02-17 |accessdate=31 August 2009 |language= |quote= |archiveurl= |archivedate= }}</ref> both men were eventually convicted and sentenced to 20 years imprisonment in New York State Prison;<ref name="Asbury"/><ref name="Sutton"/><ref>{{cite news |title=The Doom Of The Masked Burglars |trans_title= |url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=9F02EFD8173BEF34BC4951DFB466838F669FDE |format= PDF|agency= |newspaper=[[New York Times]] |pages= |page= |date=1874-02-21 |accessdate=31 August 2009 |language= |quote= }}</ref> - however a news article published by the ''[[New York Times]]'' five years later claimed he and his gang were in [[Sing Sing]].<ref>{{cite news |first= |last= |authorlink= |author= |coauthors= |title=Johnny Dobbs In Custody.; The Manhattan Bank Burglary. Identification Of The Man Arrested In Philadelphia Dobb's Notorious Career His Connection With Other Robberies |url=https://query.nytimes.com/mem/archive-free/pdf?res=9404E1DE133EE63BBC4F53DFB3668382669FDE |format= PDF|agency= |newspaper=[[New York Times]] |id= |pages= |page= |date=1879-05-07 |accessdate=31 August 2009 |language= |quote= }}</ref>

==References==
{{Reflist}}

==Further reading==
*[[Thomas F. Byrnes|Byrnes, Thomas]]. ''1886 Professional Criminals of America''. New York: Chelsea House Publishers, 1969.
*Costello, Augustine E. ''Our Police Protectors: History of the New York Police from the Earliest Period to the Present Time''. New York: A.E. Costello, 1885.

{{DEFAULTSORT:Conroy, Patsy}}
[[Category:1846 births]]
[[Category:Year of death missing]]
[[Category:American people of Irish descent]]
[[Category:Criminals from New York City]]
[[Category:People from Manhattan]]
[[Category:American pirates]]
[[Category:19th-century pirates]]