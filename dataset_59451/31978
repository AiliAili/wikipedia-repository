
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:SMS Kronprinz Wilhelm in Scapa Flow.jpg|300px|alt=A large, light gray warship sits motionless in a calm sea]]
| Ship caption = ''Kronprinz Wilhelm'' in Scapa Flow, 1919
}}
{{Infobox ship class overview
| Name = 
| Builders = 
| Operators = {{Navy|German Empire}}
| Class before = {{sclass-|Kaiser|battleship|4}}
| Class after = {{sclass-|Bayern|battleship|4}}
| Subclasses = 
| Cost = 
| Built range = 
| In service range = 
| In commission range = 1914–19
| Total ships building = 
| Total ships planned = 
| Total ships completed = 4
| Total ships cancelled = 
| Total ships active = 
| Total ships laid up = 
| Total ships lost = 4
| Total ships retired = 
| Total ships preserved = 
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship type = [[Battleship]]
| Ship displacement =* {{convert|25796|t|LT|abbr=on}} design
* {{convert|28600|t|LT|abbr=on}} full load
  
| Ship length = {{convert|175.4|m|ftin|abbr=on}}
| Ship beam = {{convert|29.5|m|ftin|abbr=on}}
| Ship draft = {{convert|9.19|m|ftin|abbr=on}}
| Ship propulsion =* 3 shafts
* 3 steam turbines
* {{convert|{{convert|31000|PS|shp|0|disp=number}}|shp|kW|abbr=on|lk=on}}
  
| Ship speed = {{convert|21.2|kn}}
| Ship range = {{convert|8000|nmi|lk=in|abbr=on}} at {{convert|12|kn}}
| Ship complement = 1,136
| Ship armament =* 10 × [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} guns]]
* 14 × [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} guns]]
* 10 × [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} guns]]
*  5 × {{convert|50|cm|in|1|abbr=on}} torpedo tubes
  
| Ship armor =* Belt: {{convert|350|mm<!-- nm? -->|in|abbr=on}}
* Turrets and conning tower: {{convert|300|mm|in|abbr=on}}
* Deck: {{convert|30|mm|in|abbr=on}}
  
| Ship notes = 
}}
|}

The '''''König'' class''' was a group of four [[battleship]]s built for the German ''[[Kaiserliche Marine]]'' on the eve of [[World War I]]. The class was composed of {{SMS|König||2}}, {{SMS|Grosser Kurfürst|1913|2}}, {{SMS|Markgraf||2}}, and {{SMS|Kronprinz||2}}. The most powerful warships of the German [[High Seas Fleet]] at the outbreak of war in 1914, the class operated as a unit throughout World War I—the V Division of the III Battle Squadron. The ships took part in a number of fleet operations during the war, including the [[Battle of Jutland]], where they acted as the vanguard of the German [[line of battle|line]]. They survived the war and were interned at [[Scapa Flow]] in November 1918. All four ships were [[Scuttling of the German fleet in Scapa Flow|scuttled on 21 June 1919]] when Rear Admiral [[Ludwig von Reuter]] ordered the sinking of the entire High Seas Fleet.

The ''König''s were an improvement over the preceding {{sclass-|Kaiser|battleship|4}}; one of the primary changes being in the disposition of the main gun battery. The ''Kaiser''-class ships mounted ten 30.5&nbsp;cm (12&nbsp;in) SK L/50 guns in five twin [[gun turret|turrets]]; one turret was mounted fore, two aft in a [[Superfire|superfiring]] arrangement, and the other two as wing turrets in a zig-zag "echelon" configuration amidships. For the ''König'' class, the use of main-gun wing turrets was abandoned. Instead, a second turret was moved forward and placed in a superfiring arrangement, and a single rear-facing turret was mounted [[centerline (ship marking)|centerline]] amidships; with a [[traverse (gunnery)|traverse]] allowing for broadsides but not forward-fire. The two superfiring aft turrets remained. This allowed for a wider angle of fire on the [[broadside]], as all 10 guns could fire on a large area.{{efn|name=Kaiser wing turrets}}  It did, however, reduce the ship's forward-fire capabilities; from six guns with only limited traverse on the two wing turrets, to four guns with full traverse.

== Design ==
The ''König''-class battleships were authorized under the [[German Naval Laws|Second Amendment to the Naval Law]], which had been passed in 1908 as a response to the revolution in naval technology created with the launch of the British {{HMS|Dreadnought|1906|6}} in 1906. [[Dreadnought]]s were significantly larger—and correspondingly more expensive—than the old [[pre-dreadnought]] battleships. As a result, the funds that had been appropriated for the Navy in 1906 were going to be used up before they were scheduled to be replenished in 1911.{{sfn|Herwig|p=62}} Along with the additional funding secured in the 1908 bill, the service life of all large warships was to be reduced from 25&nbsp;years to 20&nbsp;years; this was done in an effort to force the [[Reichstag (German Empire)|Reichstag]] to allocate more funds for additional ships. In his effort to force the Reichstag to pass the bill, Admiral [[Alfred von Tirpitz]] threatened to resign from his post as the State Secretary for the Navy. As a result of von Tirpitz's ultimatum, the bill was passed in March 1908 by a large margin.{{sfn|Herwig|p=63}}

The reduction in service life necessitated the replacement of the [[coastal defense ship]]s of the {{sclass-|Siegfried|coastal defense ship|5}} and {{SMS|Oldenburg|1884|2}} classes as well as the {{sclass-|Brandenburg|battleship|2}}s. In the terms of the First Amendment to the Naval Law of 1906, von Tirpitz had requested but failed to secure funding for new battleships; they had now been approved by the Reichstag. The Naval Law also increased the naval budget by an additional 1 billion [[German gold mark|marks]].{{sfn|Gardiner & Gray|p=135}} After the four {{sclass-|Sachsen|ironclad|2}}s had been replaced by the four ''Nassau''s, and the six ''Siegfried''-class ships had been replaced by the ''Helgoland'' and ''Kaiser'' classes, the next vessels to be replaced were the old {{sclass-|Brandenburg|battleship|2}}s. ''König'' and her three [[sister ship|sisters]] were ordered under the provisional names ''S'', ''Ersatz Kurfürst Friedrich Wilhelm'', ''Ersatz Weissenburg'', and ''Ersatz Brandenburg''.{{efn|name=provisional names}}

=== General characteristics ===
[[File:König class battleship - Jane's Fighting Ships, 1919 - Project Gutenberg etext 24797.png|thumb|Plan and elevation view of a ship of the ''König'' class, from Jane's ''Fighting Ships'' 1919|alt=Schematics for this type of battleship; the ships mount five gun turrets, two forward, one in the center between two smoke stacks, and two aft]]

The ''König''-class ships were 174.7&nbsp;m [[length at the waterline|long at the waterline]], and 175.4&nbsp;m [[length overall|long overall]]. With a beam of 29.5&nbsp;m, a forward [[draft (hull)|draft]] of 9.19&nbsp;m and a rear draft of 9.0&nbsp;m, the ''König''s were designed to displace 25,796&nbsp;[[metric ton]]s normally, but at full combat load, they displaced 28,600&nbsp;tons. The [[hull (watercraft)|hulls]] were constructed with transverse and longitudinal steel frames, over which the outer hull plates were [[rivet]]ed. The ships' hulls each contained 18 [[watertight compartment]]s, each equipped with a [[double bottom]] that ran for 88% of the length of the hull.{{sfn|Gröner|p=27}}

German naval historian Erich Gröner, in his book ''German Warships 1815–1945'', stated that the German navy considered the ships to be "very good sea-boats."{{sfn|Gröner|p=28}} They suffered a slight loss of speed in a [[Swell (ocean)|swell]], and with the rudder hard over, the ships lost up to 66% speed and heeled over 8&nbsp;degrees. The battleships had a transverse [[metacentric height]] of 2.59&nbsp;m. ''König'', ''Grosser Kurfürst'', ''Markgraf'', and ''Kronprinz'' each had a standard crew of 41 officers and 1095 enlisted men; ''König'', the flagship of the III Squadron,{{sfn|Tarrant|p=286}} had an additional crew of 14 officers and another 68 sailors. The ships carried several smaller boats, including one [[picket boat]], three barges, two [[Launch (boat)|launches]], two [[yawl]]s, and two [[dinghy|dinghies]].{{sfn|Gröner|p=28}}

=== Propulsion ===
It was originally intended that the ''König''-class battleships would be powered by two sets of turbines on the outer shafts, while the center shaft would have utilized a [[MAN SE|MAN]] 6-cylinder 2-stroke [[diesel engine]] producing 12,000&nbsp;shp at 150&nbsp;[[revolutions per minute|rpm]].{{sfn|Breyer|p=276}} Development of the diesel was protracted, however, and it was later decided that the diesel would be installed only in ''Grosser Kurfürst'' and ''Markgraf''.{{sfn|Breyer|p=276}} Ultimately, the diesel was never installed in any of the ''König''-class battleships.{{sfn|Breyer|p=276}} They were instead equipped with three sets of Parsons (''König'' and ''Kronprinz''), AEG-Vulcan (''Grosser Kurfürst''), or Bergmann (''Markgraf'') [[Steam turbine|turbines]] driving 3.8&nbsp;m-wide three-bladed [[propeller|screws]].{{sfn|Gardiner & Gray|pp=147–148}} Steam was supplied by three oil-fired and 12 coal-fired Schulz-Thornycroft [[boilers]] operating at up to 16&nbsp;atmospheres of pressure.{{sfn|Gröner|p=28}}

The power plant was rated at {{convert|{{convert|31000|PS|shp|0|disp=number}}|shp|kWlk=on}}. On trials, the ships produced between {{convert|{{convert|41400|–|46200|PS|shp|0|disp=number}}|shp|kW|abbr=on|lk=on}}.{{sfn|Gardiner & Gray|pp=147–148}} The designed top speed was {{convert|21|kn}}. The ships had a designed range of {{convert|8,000|nmi|lk=on}} at a cruising speed of 12&nbsp;knots, which decreased to 4,000&nbsp;nautical miles at 18&nbsp;knots. Maximum bunkerage was 3,000&nbsp;tons of coal and 600&nbsp;tons of oil.{{sfn|Gröner|p=27}} Each ship had two [[rudder]]s. Electrical power was supplied by four turbo-generators and a pair of diesel generators; total electrical output was 2,040&nbsp;kilowatts at 225&nbsp;[[volt]]s.{{sfn|Gröner|p=28}}

=== Armament ===
[[File:Bundesarchiv Bild 146-1971-017-32, Besetzung Insel Ösel, Linienschiff und Zeppelin.jpg|thumb|upright|The rear turrets of ''Grosser Kurfürst''|alt=The gun turrets of a battleship. A gray zeppelin flies overhead]]

The ''König''s were armed with a main battery of ten [[30.5 cm SK L/50 gun|{{convert|30.5|cm|in|abbr=on}} SK L/50 guns]]{{efn|name=gun nomenclature}} in five twin [[gun turret|turrets]].{{sfn|Gröner|p=28}} Two turrets were mounted forward of the main [[superstructure]] in a [[superfire|superfiring pair]], the third was placed on the centerline between the two funnels amidships, and the fourth and fifth turrets were also arranged in a superfiring pair, aft of the rear [[conning tower]]. Each gun turret had a working chamber beneath it that was connected to a revolving ammunition hoist leading down to the magazine below it. The turrets were electrically controlled, though the guns were elevated hydraulically. In an effort to reduce the possibility of a fire, everything in the turret was constructed of steel.{{sfn|Herwig|p=70}} The centerline arrangement was an improvement over the preceding {{sclass-|Kaiser|battleship|4}}, as all ten guns could fire on a wide arc on the [[broadside]], and four guns could fire directly ahead, as opposed to only two on the ''Kaiser''s. The guns were supplied with 900 rounds, or 90 shells per gun.{{sfn|Gröner|p=28}} The 30.5&nbsp;cm gun had a [[rate of fire]] of between 2–3 405.5-kilogram (894-pound) [[Armor-piercing shot and shell|armor-piercing shells]] per minute, and was expected to fire 200 shells before replacement was necessary. The guns were also capable of firing 405.9&nbsp;kg (894.8&nbsp;lb) [[high explosive]] shells. Both types of shells were loaded with two [[propellant]] charges: an RP C/12 main charge in a brass cartridge that weighed 91&nbsp;kg (201&nbsp;lb) and an RP C/12 fore charge in a silk bag that weighed 34.5&nbsp;kg (76&nbsp;lb). This provided a [[muzzle velocity]] of 855&nbsp;meters per second (2,805&nbsp;ft/s). The turrets on the ''König''-class battleships initially allowed elevation up to 13.5&nbsp;degrees; this enabled a maximum range of 16,200&nbsp;m (17,700&nbsp;yd). After modifications, elevation was increased to 16&nbsp;degrees, which correspondingly increased the range of the guns to 20,400&nbsp;m (22,300&nbsp;yd).{{sfn|NavWeaps (30.5 cm/50)}}

Secondary armament consisted of fourteen [[15 cm SK L/45|{{convert|15|cm|in|abbr=on}} SK L/45 quick-firing guns]], each mounted in MPL C/06.11 [[casemate]]s in the side of the top deck.{{sfn|NavWeaps (15 cm/45)}} These guns were intended for defense against torpedo boats, and were supplied with a total of 2,240 shells. These guns could engage targets out to 13,500&nbsp;m, and after improvements in 1915, their range was extended to 16,800&nbsp;m. The guns had a sustained [[rate of fire]] of 5 to 7 rounds per minute. The shells were 45.3&nbsp;kg (99.8&nbsp;lb), and were loaded with a 13.7&nbsp;kg (31.2&nbsp;lb) RPC/12 propellant charge in a brass cartridge. This provided a muzzle velocity of 835&nbsp;meters per second (2,740&nbsp;ft/s). Service life was estimated at approximately 1,400 shells fired before the guns would need to be replaced.{{sfn|NavWeaps (15 cm/45)}}

The ''König''s also carried six [[8.8 cm SK L/45 naval gun|{{convert|8.8|cm|in|abbr=on}} SK L/45 quick-firing guns]], mounted in casemates. The six guns were located on either side of the forward conning tower and were all directed forward. These guns were supplied with a total of 3,200 rounds, or 200 shells per gun,{{sfn|Gröner|p=28}} and could fire at a rate of 15 shells per minute. The high-explosive shells fired by these guns weighed 10&nbsp;kg (22.05&nbsp;lb) and were loaded with a 3&nbsp;kg (6.6&nbsp;lb) RPC/12 propellant charge. These guns had a life expectancy of around 7,000 rounds.{{sfn|NavWeaps (8.8 cm/45)}} These were later removed and replaced with four 8.8&nbsp;cm SK L/45 anti-aircraft guns, which were mounted on either side of the rear conning tower.{{sfn|Gröner|p=28}}

As was customary for capital ships of the [[dreadnought]] era, the ships were armed with five {{convert|50|cm|in|abbr=on}} submerged torpedo tubes. One was mounted in the bow, the other four were placed on the broadside, two on each side of the ship. The tubes were supplied with 16 torpedoes.{{sfn|Gröner|p=28}} The torpedoes were the G7 type; they were 7.02&nbsp;m (23&nbsp;ft) long and were armed with a 195&nbsp;kg (430&nbsp;lb) explosive warhead. At 37&nbsp;knots, the torpedoes had a range of 4,000&nbsp;m (4,370&nbsp;yd); at 27&nbsp;knots the range more than doubled, to 9,300&nbsp;m (10,170&nbsp;yards).{{sfn|NavWeaps (Torpedoes)}}

=== Armor ===
The ''König''-class ships were protected with [[Krupp]] [[Krupp cemented armor|cemented steel armor]], as was the standard for German warships of the period. They had an [[armor belt]] that was {{convert|350|mm|in|abbr=on}} thick in the central citadel of the ship, where the most important parts of the ship were located. This included the ammunition magazines and the machinery spaces. The belt was reduced in less critical areas, to {{convert|180|mm|in|abbr=on}} forward and {{convert|120|mm|in|abbr=on}} aft. The [[Bow (ship)|bow]] and [[stern]] were not protected by armor at all. A {{convert|40|mm|in|abbr=on}}-thick [[torpedo bulkhead]] ran the length of the hull, several meters behind the main belt. The main armored deck was {{convert|60|mm|in|abbr=on}} thick in most places, though the thickness of the sections that covered the more important areas of the ship was increased to {{convert|100|mm|in|abbr=on}}.{{sfn|Gröner|p=28}}

The forward [[conning tower]] was protected with heavy armor: the sides were {{convert|300|mm|in|abbr=on}} thick and the roof was {{convert|150|mm|in|abbr=on}} thick. The rear conning tower was less well armored; its sides were only {{convert|200|mm|in|abbr=on}} thick and the roof was covered with {{convert|50|mm|in|abbr=on}} of armor plate. The main battery gun turrets were also heavily armored: the turret sides were 300&nbsp;mm thick and the roofs were {{convert|110|mm|in|abbr=on}} thick. The casemated 15&nbsp;cm guns had {{convert|170|mm|in|abbr=on}} worth of armor plating in the casemates; the guns themselves had {{convert|80|mm|in|abbr=on}}-thick shields to protect their crews from shell splinters.{{sfn|Gröner|p=28}}

== Construction ==
{{SMS|König||2}}, the [[name ship]] of the class, was built at the Imperial Dockyard in Wilhelmshaven, under construction number 33. She was laid down in 1911, was launched on 1 March 1913, and was commissioned into the fleet on 9 August 1914. {{SMS|Grosser Kurfürst|1913|2}} was the second ship of the class. She was built at the AG Vulcan shipyard in Hamburg, under construction number 4. She too was laid down in 1911; she was launched on 5 May 1913, and was commissioned before ''König'' on 30 July 1914. {{SMS|Markgraf||2}} was built at the AG Weser dockyard in Bremen, under construction number 186. She was laid down in 1911, launched on 4 June 1913, and commissioned on 1 October 1914, shortly after the outbreak of World War I. {{SMS|Kronprinz||2}}, the final ship of the class, was laid down at the [[Germaniawerft]] dockyard in Kiel in 1912. She was launched on 21 February 1914 and commissioned just under 9 months later on 8 November.{{sfn|Gröner|pp=27–28}}
===Ships===
{| class="wikitable"
! align = center|Ship
! align = center|Builder
! align = center|Namesake
! align = center|Laid down
! align = center|Launched
! align = center|Commissioned
! align = center|Fate
|-
|{{SMS|König||2}}
|[[Kaiserliche Werft Wilhelmshaven|Kaiserliche Werft]], [[Wilhelmshaven]]
|''[[King of Württemberg|König]]'' [[William II of Württemberg|Wilhelm II von Württemberg]]
|October 1911
|1 March 1913
|10 August 1914
|rowspan=4|[[Scuttling of the German fleet in Scapa Flow|Scuttled]], 21 June 1919
|-
|{{SMS|Grosser Kurfürst |1913|2}}
|[[Friedrich Krupp Germaniawerft|Germaniawerft]], [[Kiel]]
|''[[Prince-elector|Kurfürst]]'' [[Frederick William, Elector of Brandenburg|Friedrich Wilhelm von Brandenburg]]
|October 1911
|5 May 1913
|30 July 1914
|-
|{{SMS|Markgraf||2}}
|[[AG Weser]], [[Bremen]]
|[[Margraviate of Baden]]
|November 1911
|4 June 1913
|1 October 1914
|-
|{{SMS|Kronprinz||2}}
|[[Friedrich Krupp Germaniawerft|Germaniawerft]], [[Kiel]]
|[[Crown prince|''Kronprinz'']] [[Wilhelm, German Crown Prince|Wilhelm von Preußen]]
|November 1911 
|21 February 1914
|8 November 1914
|}

== Service history ==
[[File:SMS Kronprinz illustration.png|thumb|Illustration of ''Kronprinz'']]

=== Raid on Scarborough, Hartlepool and Whitby ===
{{details|Raid on Scarborough, Hartlepool and Whitby}}

The first major operation of the war in which the ''König''-class ships participated was the raid on Scarborough, Hartlepool and Whitby on 15–16 December 1914. The raid was primarily conducted by the battlecruisers of the [[I Scouting Group]]. The ''König''-class ships, along with the {{sclass-|Nassau|battleship|5}}, {{sclass-|Helgoland|battleship|5}}, and {{sclass-|Kaiser|battleship|4}}es, steamed in distant support of [[Franz von Hipper]]'s battlecruisers. [[Friedrich von Ingenohl]], the commander of the [[High Seas Fleet]], decided to take up station approximately in the center of the North Sea, about 130&nbsp;miles east of [[Scarborough, North Yorkshire|Scarborough]].{{sfn|Tarrant|p=31}}

The [[Royal Navy]], which had recently received the German code books captured from the beached cruiser {{SMS|Magdeburg||2}}, was aware that an operation was taking place, but uncertain as to where the Germans would strike. Therefore, the [[Admiralty]] ordered [[David Beatty, 1st Earl Beatty|David Beatty's]] 1st Battlecruiser Squadron, the six battleships of the 2nd Battle Squadron, and a number of cruisers and destroyers to attempt to intercept the German battlecruisers.{{sfn|Tarrant|p=31}} However, Beatty's task force nearly ran headlong into the entire High Seas Fleet. At 6:20, Beatty's destroyer screen came into contact with the German torpedo boat ''V155''. This began a confused two-hour-long battle between the British destroyers and the German cruiser and destroyer screen, frequently at very close range. At the time of the first encounter, the ''König''-class battleships were less than 10 miles away from the six British dreadnoughts; this was well within firing range, but in the darkness, neither British nor German admiral were aware of the composition of their opponents' fleets. Admiral Ingenohl, loathe to disobey the Kaiser's order to not risk the battlefleet without his express approval, concluded that his forces were engaging the screen of the entire [[Grand Fleet]], and so 10 minutes after the first contact, he ordered a turn to port to a south-east course. Continued attacks delayed the turn, but by 6:42, it had been carried out.{{sfn|Tarrant|p=32}} For about 40&nbsp;minutes, the two fleets were steaming on a parallel course. At 7:20, Ingenohl ordered a further turn to port, which put his ships on a course for German waters.{{sfn|Tarrant|p=33}}

=== Bombardment of Yarmouth and Lowestoft ===
[[File:Konig-class battleship.jpg|thumb|One of the ''König''-class battleships in 1915 or 1916]]
{{details|Bombardment of Yarmouth and Lowestoft}}

The ''König''-class ships took part in another raid on the English coast, again as support for the German battlecruiser force in the I Scouting Group. The battlecruisers left the Jade Estuary at 10:55 on 24 April 1916, and the rest of the [[High Seas Fleet]] followed at 13:40. The battlecruiser {{SMS|Seydlitz||2}} struck a mine while en route to the target, and had to withdraw.{{sfn|Tarrant|p=53}} The other battlecruisers bombarded the town of Lowestoft largely without incident, but during the approach to Yarmouth, they encountered the British cruisers of the [[Harwich Force]]. A short artillery duel ensued before the Harwich Force withdrew. Reports of British submarines in the area prompted the retreat of the I Scouting Group. At this point, Admiral [[Reinhard Scheer]], who had been warned of the sortie of the Grand Fleet from its base in [[Scapa Flow]], also withdrew to safer German waters.{{sfn|Tarrant|p=54}}

=== Battle of Jutland ===
{{details|Battle of Jutland}}

The four ships took part in the fleet sortie that resulted in the battle of [[Jutland]] on 31 May–1 June 1916. The operation again sought to draw out and isolate a portion of the Grand Fleet and destroy it before the main British fleet could retaliate. ''König'', ''Grosser Kurfürst'', ''Markgraf'', and ''Kronprinz'' made up the V Division of the III Battle Squadron, and they were the [[wikt:vanguard|vanguard]] of the fleet. The III Battle Squadron was the first of three battleship units; directly astern were the {{sclass-|Kaiser|battleship|2}}s of the VI Division, III Battle Squadron. Astern of the ''Kaiser''-class ships were the ''Helgoland'' and ''Nassau'' classes of the II Battle Squadron; in the [[rear guard]] were the elderly {{sclass-|Deutschland|battleship|4}} [[pre-dreadnought]]s of the I Battle Squadron.{{sfn|Tarrant|p=286}}

Shortly before 16:00&nbsp;[[Central European Time|CET]],{{efn|name=CET times}} the battlecruisers of [[I Scouting Group]] encountered the British 1st Battlecruiser Squadron, under the command of David Beatty. The opposing ships began an artillery duel that saw the destruction of {{HMS|Indefatigable|1909|2}}, shortly after 17:00,{{sfn|Tarrant|pp=94–95}} and {{HMS|Queen Mary||2}}, less than a half an hour later.{{sfn|Tarrant|pp=100–101}} By this time, the German battlecruisers were steaming south in order to draw the British ships towards the main body of the High Seas Fleet. At 17:30, {{SMS|König||2}}, the leading German battleship, spotted both the I Scouting Group and the 1st Battlecruiser Squadron approaching. The German battlecruisers were steaming down to starboard, while the British ships steamed to port. At 17:45, Scheer ordered a two-point turn to port to bring his ships closer to the British battlecruisers, and a minute later at 17:46, the order to open fire was given.{{sfn|Tarrant|p=110}}

[[File:SMS Konig.jpg|thumb|left|''König'' underway]]

''König'', ''Grosser Kurfürst'', and ''Markgraf'' were the first to reach effective gunnery range; they engaged the battlecruisers {{HMS|Lion|1910|2}}, {{HMS|Princess Royal|1911|2}}, and {{HMS|Tiger|1913|2}}, respectively, at a range of 21,000&nbsp;yards.{{sfn|Tarrant|pp=110–111}} ''König''{{'}}s first salvos fell short of her target, and so she shifted her fire to the nearest British ship, ''Tiger''. Simultaneously, the leading ''König''-class battleships began firing on the destroyers {{HMS|Nestor|1915|2}} and {{HMS|Nicator||2}}.{{sfn|Tarrant|p=111}} The two destroyers closed in on the German line and, having endured a hail of gunfire, maneuvered into a good firing position. Each ship launched two torpedoes apiece at ''König'' and ''Grosser Kurfürst'', though all four weapons missed. In return, a secondary battery shell from one of the battleships hit ''Nestor'' and wrecked her engine room. The ship, along with the destroyer {{HMS|Nomad||2}}, was crippled and lying directly in the path of the advancing German line. Both of the destroyers were sunk, but German torpedo boats stopped to pick up survivors.{{sfn|Tarrant|p=114}} At around 18:00, the four ''König''s shifted their fire to the approaching {{sclass-|Queen Elizabeth|battleship|2}}s of the V Battle Squadron, though the firing lasted only a short time before the range widened too far.{{sfn|Tarrant|p=116}}

Shortly after 19:00, the German cruiser {{SMS|Wiesbaden||2}} had become disabled by a shell from {{HMS|Invincible|1907|2}}; Rear Admiral Behncke in ''König'' attempted to maneuver his ships in order to cover the stricken cruiser.{{sfn|Tarrant|p=137}} Simultaneously, the British III and IV Light Cruiser Squadrons began a torpedo attack on the German line; while advancing to torpedo range, they smothered ''Wiesbaden'' with fire from their main guns. The ''König''s fired heavily on the British cruisers, but even sustained fire from the Germans' main guns failed to drive off the British cruisers.{{sfn|Tarrant|p=138}} In the ensuing melee, the British armored cruiser {{HMS|Defence|1907|2}} was struck by several heavy-caliber shells from the German dreadnoughts. One salvo penetrated the ship's ammunition magazines and, in a massive explosion, destroyed the cruiser.{{sfn|Tarrant|p=140}}

By the time the German fleet returned to the Jade estuary, the ''Nassau''-class battleships {{SMS|Nassau||2}}, {{SMS|Westfalen||2}}, and {{SMS|Posen||2}} and the ''Helgoland''-class battleships {{SMS|Helgoland||2}} and {{SMS|Thüringen||2}} took up guard duties in the outer [[roadstead]]. The ''Kaiser''-class battleships {{SMS|Kaiser|1911|2}}, {{SMS|Kaiserin||2}}, and {{SMS|Prinzregent Luitpold||2}}, took up defensive positions outside the Wilhelmshaven locks. The four ''König''-class ships, along with other capital ships—those that were still in fighting condition—had their fuel and ammunition stocks replenished in the inner harbor.{{sfn|Tarrant|p=263}}

=== Fleet advance of 18–19 August ===
During the fleet advance on 18–19&nbsp;August, the I&nbsp;Scouting Group was to bombard the coastal town of [[Sunderland, Tyne and Wear|Sunderland]] in an attempt to draw out and destroy Beatty's battlecruisers. As {{SMS|Moltke||2}} and {{SMS|Von der Tann||2}} were the only two remaining German battlecruisers still in fighting condition in the Group, three dreadnoughts were assigned to the unit for the operation: ''Markgraf'', ''Grosser Kurfürst'', and the newly commissioned {{SMS|Bayern||2}}. Admiral Scheer and the rest of the High Seas Fleet, with 15 dreadnoughts of its own, would trail behind and provide cover.{{sfn|Massie|p=682}} The British were aware of the German plans and sortied the Grand Fleet to meet them. By 14:35, Scheer had been warned of the Grand Fleet's approach and, unwilling to engage the whole of the Grand Fleet just 11&nbsp;weeks after the decidedly close call at Jutland, turned his forces around and retreated to German ports.{{sfn|Massie|p=683}}

=== Operation Albion ===
{{details|Operation Albion}}

In early September 1917, following the German conquest of the Russian port of [[Riga]], the German navy decided to expunge the Russian naval forces that still held the [[Gulf of Riga]]. To this end, the ''Admiralstab'' (the Navy High Command) planned an operation to seize the Baltic islands of [[Saaremaa|Ösel]], particularly the Russian gun batteries on the [[Sõrve Peninsula|Sworbe peninsula]].{{sfn|Halpern|p=213}} On 18 September, the order was issued for a joint Army-Navy operation to capture Ösel and [[Muhu|Moon]] islands; the primary naval component was to comprise the [[flagship]], ''Moltke'', along with the III Battle Squadron of the High Seas Fleet. The V Division included the four ''König''s, and was by this time augmented with the new battleship {{SMS|Bayern||2}}. The VI Division consisted of the five ''Kaiser''-class battleships. Along with nine light cruisers, three torpedo boat flotillas, and dozens of [[naval mine|mine]] warfare ships, the entire force numbered some 300 ships, and were supported by over 100 aircraft and six zeppelins. The invasion force amounted to approximately 24,600 officers and enlisted men.{{sfn|Halpern|pp=214–215}} Opposing the Germans were the old Russian [[pre-dreadnought]]s {{ship|Russian battleship|Slava||2}} and {{ship|Russian battleship|Tsesarevich||2}}, the [[armored cruiser]]s ''Bayan'', ''Admiral Makarov'', and ''Diana'', 26 destroyers, and several torpedo boats and gunboats. The garrison on Ösel numbered some 14,000 men.{{sfn|Halpern|p=215}}

The operation began on 12 October, when ''Moltke'', ''Bayern'', and the ''König''s began firing on the Russian shore batteries at Tagga Bay. Simultaneously, the ''Kaiser''s engaged the batteries on the Sworbe peninsula; the objective was to secure the channel between Moon and [[Hiiumaa|Dagö]] islands, which would block the only escape route of the Russian ships in the Gulf. Both ''Grosser Kurfürst'' and ''Bayern'' struck mines while maneuvering into their bombardment positions; damage to the former was minimal, but ''Bayern'' was severely wounded; the ship had to be withdrawn to Kiel for repairs.{{sfn|Halpern|p=215}}

On 16 October, it was decided to detach a portion of the invasion flotilla to clear the Russian naval forces in Moon Sound; these included the two Russian pre-dreadnoughts. To this end, ''König'' and ''Kronprinz'', along with the cruisers {{SMS|Strassburg||2}} and {{SMS|Kolberg||2}} and a number of smaller vessels were sent to engage the Russian battleships. They arrived by the morning of 17 October, but a deep Russian minefield kept the ships temporarily at bay. A rude surprise came to the Germans, when they discovered that the 30.5&nbsp;cm guns of the Russian battleships out-ranged their own 30.5&nbsp;cm guns. The Russian ships managed to keep the distance wide enough to prevent the German battleships from being able to return fire, while still firing effectively on the German ships—at several points the Germans had to take evasive maneuvers to avoid the Russian shells. However, by 10:00, the minesweepers had cleared a path through the minefield, and ''König'' and ''Kronprinz'' dashed into the bay; the two big dreadnoughts engaged the Russian battleships, ''König'' dueled with ''Slava'' and ''Kronprinz'' fired on both ''Slava'' and the cruiser ''Bayan''. The Russian vessels were hit dozens of times, until at 10:30 the Russian naval commander, [[Mikhail Bakhirev|Admiral Bakhirev]], ordered their withdrawal. ''Slava'' had taken too much damage, and was unable to make good her escape; instead, she was scuttled and her crew was evacuated on a destroyer.{{sfn|Halpern|p=218}}

By 20 October, the naval operations were effectively over; the Russian ships had been destroyed or forced to withdraw, and the German army held their objectives. However, on 29 October, ''Markgraf'' struck a mine during the German withdrawal from the Gulf of Riga.{{sfn|Halpern|p=219}}

=== Fate ===
{{see also|Scuttling of the German fleet in Scapa Flow}}
[[File:Internment at Scapa Flow.svg|thumb|Location of German vessels before scuttling|alt=A map of about two dozen ships in the western area of Scapa Flow, around the island of Cava. A marker "The Barrel of Butter" is just east of the ships.]]

Following the [[Armistice with Germany|capitulation of Germany in November 1918]], the majority of the High Seas Fleet, under the command of Rear Admiral [[Ludwig von Reuter]], was interned in the British naval base at [[Scapa Flow]].{{sfn|Tarrant|p=282}} The fleet remained in captivity during the negotiations that ultimately produced the [[Versailles Treaty]]. It became apparent to Reuter that the British intended to seize the German ships on 21 June, which was the deadline for Germany to have signed the peace treaty. Unaware that the deadline had been extended to the 23rd, Reuter ordered his ships be sunk. On the morning of 21 June, the British fleet left Scapa Flow to conduct training maneuvers; at 11:20 Reuter transmitted the order to his ships.{{sfn|Herwig|p=256}}

Of the four ships, ''Kronprinz'' was the first to sink. She slipped beneath the waters of Scapa Flow at 13:15. ''Grosser Kurfürst'' followed 15 minutes later at 13:30. ''König'' sank at approximately 14:00, but ''Markgraf'' did not sink until 16:45; she was one of the last capital ships to be successfully scuttled—only the battlecruiser {{SMS|Hindenburg||2}} sank afterwards, at 17:00.{{sfn|Gröner|p=51}} ''Grosser Kurfürst'' was eventually raised, on 29 April 1938. The ship was towed to [[Rosyth]], where she was broken up for scrap metal. The other three ships remain on the sea floor, and were sold to Britain in 1962.{{sfn|Gröner|p=28}}

== Notes ==

{{notes
| notes =

{{efn
| name = Kaiser wing turrets
| The wing turrets on the ''Kaiser''-class ships could fire across the deck, allowing all main guns to engage the enemy, however the [[conning tower]]s, funnels, and other areas of [[superstructure]] greatly restricted the angle of fire in that direction.
}}

{{efn
| name = provisional names
| All German ships were ordered under provisional names; new additions to the fleet were given a letter, while ships that were intended to replace older vessels were ordered as "Ersatz [ship name]" ("Replacement (for) [ship name]).
}}

{{efn
| name = gun nomenclature
| In Imperial German Navy gun nomenclature, "SK" (Schnelladekanone) denotes that the gun quick firing, while the L/50 denotes the length of the gun. In this case, the L/50 gun is 50 [[Caliber (artillery)|caliber]], meaning that the gun barrel is 50 times as long as it is in diameter.
}}

{{efn
| name = CET times
| The times mentioned in this section are in [[Central European Time|CET]], which is congruent with the German perspective. This is one hour ahead of [[Coordinated Universal Time|UTC]], the time zone commonly used in British works.
}}

}}

== Footnotes ==

{{reflist|20em}}

== References ==

{{portal|Battleships}}
{{Commons category|König class battleship}}

* {{cite book
  | last = Breyer
  | first = Siegfried
  | title = Battleships and Battlecruisers 1905–1970: Historical Development of the Capital Ship
  | year = 1973
  | location = Garden City
  | publisher = Doubleday and Company
  | isbn = 978-0-385-07247-2
  | oclc = 
  | ref = {{sfnRef|Breyer}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Gray
  | editor2-first = Randal
  | year = 1985
  | title = Conway's All the World's Fighting Ships: 1906–1921
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-907-8
  | ref = {{sfnRef|Gardiner & Gray}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = [[Annapolis]]
  | isbn = 978-0-87021-790-6
  | oclc = 22101769
  | ref = {{sfnRef|Gröner}}
  }}
* {{cite book
  | last = Halpern
  | first = Paul G.
  | year = 1995
  | title = A Naval History of World War I
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-55750-352-7
  | oclc = 57447525
  | ref = {{sfnRef|Halpern}}
  }}
* {{cite book
  | last = Herwig
  | first = Holger
  | year = 1998
  | origyear = 1980
  | title = "Luxury" Fleet: The Imperial German Navy 1888–1918
  | publisher = Humanity Books
  | location = [[Amherst, New York]]
  | isbn = 978-1-57392-286-9
  | oclc = 57239454
  | ref = {{sfnRef|Herwig}}
  }}
* {{cite book
  | last = Massie
  | first = Robert K.
  | authorlink = Robert K. Massie
  | year = 2003
  | title = [[Castles of Steel]]
  | publisher = Ballantine Books
  | location = [[New York City]]
  | isbn = 978-0-345-40878-5
  | oclc = 57134223
  | ref = {{sfnRef|Massie}}
  }}
* {{cite book
  | last = Tarrant
  | first = V. E.
  | year = 2001
  | origyear = 1995
  | title = Jutland: The German Perspective
  | publisher = Cassell Military Paperbacks
  | location = London
  | isbn = 978-0-304-35848-9
  | oclc = 48131785
  | ref = {{sfnRef|Tarrant}}
  }}

'''Online sources'''

* {{cite web
  | last = DiGiulian
  | first = Tony
  | date = 28 December 2008
  | title = Germany 30.5&nbsp;cm/50 (12") SK L/50
  | publisher = NavWeaps.com
  | url = http://www.navweaps.com/Weapons/WNGER_12-50_skc12.htm
  | accessdate = 17 July 2009
  | ref = {{sfnRef|NavWeaps (30.5 cm/50)}}
  }}
* {{cite web
  | last = DiGiulian
  | first = Tony
  | date = 6 July 2007
  | title = German 15 cm/45 (5.9") SK L/45
  | publisher = NavWeaps.com
  | url = http://www.navweaps.com/Weapons/WNGER_827-45_skc05.htm
  | accessdate = 29 June 2009
  | ref = {{sfnRef|NavWeaps (15 cm/45)}}
  }}
* {{cite web
  | last = DiGiulian
  | first = Tony
  | date = 16 April 2009
  | title = German 8.8&nbsp;cm/45 (3.46") SK L/45, 8.8&nbsp;cm/45 (3.46") Tbts KL/45, 8.8&nbsp;cm/45 (3.46") Flak L/45
  | publisher = NavWeaps.com
  | url = http://www.navweaps.com/Weapons/WNGER_88mm-45_skc13.htm
  | accessdate = 29 June 2009
  | ref = {{sfnRef|NavWeaps (8.8 cm/45)}}
  }}
* {{cite web
  | last = DiGiulian
  | first = Tony
  | date = 21 April 2007
  | title = German Torpedoes Pre-World War II
  | publisher = NavWeaps.com
  | url = http://www.navweaps.com/Weapons/WTGER_PreWWII.htm
  | accessdate = 17 July 2009
  | ref = {{sfnRef|NavWeaps (Torpedoes)}}
  }}

{{König class battleship}}
{{WWIGermanShips}}
{{Featured article}}

{{DEFAULTSORT:Konig-class battleship}}
[[Category:Battleship classes]]
[[Category:König-class battleships| ]]
[[Category:World War I battleships of Germany]]