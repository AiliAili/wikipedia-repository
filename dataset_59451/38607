{{Good article}}
{{Infobox person
| name          = James T. Brady
| image         = James Topham Brady.jpg
| alt           = 
| caption       = Painting of Brady by [[Joseph Alexander Ames]], 1869
| birth_name    = James Topham Brady
| birth_date    = {{Birth date|1815|4|9}}
| birth_place   = [[New York City]], New York, U.S.
| death_date    = {{Death date and age|1869|2|9|1815|4|9}}
| death_place   = New York City, New York, U.S.
| nationality   = 
| other_names   = 
| occupation    = Lawyer
| years_active  = 1835/6–1869
| known_for     = 
| notable_works =
}}

'''James Topham Brady''' (April 9, 1815{{spaced ndash}}February 9, 1869) was an American lawyer. Born in [[New York City]], Brady studied law in his father's [[law practice|practice]] before being admitted to the New York bar himself. He is most notable for his career as a criminal lawyer, being involved in numerous high-profile proceedings. He tried fifty-two criminal cases and only lost one. Brady died at his home after having the left side of his body paralyzed for several days, and was interred at [[Calvary Cemetery (Queens, New York)|Calvary Cemetery]] in New York.

== Early life ==
[[File:James T. Brady.jpg|thumb|right|An engraving of James Topham Brady]]
Brady was born on April 9, 1815, in [[New York City]], the eldest son of Thomas S. Brady and his wife.{{sfn|Clarke|1869|p=716}} Later, Brady would have seven siblings, two boys and five girls.{{sfn|Houghton|1885|p=459}} The senior Brady had emigrated from [[Ireland]] to the United States while the [[War of 1812]] was ongoing, and started a [[boys school]] in New York.<ref>{{harvnb|Clarke|1869|p=716}}; {{harvnb|Scott|1891|p=65}}</ref> When James was seven, he attended his father's school.{{sfn|Clarke|1869|p=716}} In 1831 James's father had left teaching to become a lawyer, and he helped his father in his practice and in trials. In his father's office, James studied legal material tirelessly, and soon operated most of the firm's managerial affairs.{{sfn|Clarke|1869|p=716–717}}

== Career ==
[[File:Portrait of James T. Brady.jpg|thumb|left|A portrait of James Topham Brady]]
Brady was admitted to the New York Bar in either 1835 or 1836, when he was about twenty years old. His first case was an insurance proceeding, where he opposed the prominent lawyer, Charles O'Connor. The plaintiff staked a claim for insurance money from a property allegedly burned down by a fire. Though Brady lost that case, his proficiency for law and oration was immediately noted.{{sfn|Scott|1891|p=67}}

Brady received his first taste of legal notoriety during the ''Goodyear v. Day'' patent case, where he worked under [[Daniel Webster]] and delivered the [[opening argument]]s for the plaintiff.{{sfn|Spassky|1965|p=53}}{{sfn|Unknown|1869|p=780}} Nevertheless, Brady is best known for his work as a criminal lawyer. In the quarter century preceding his death, he was involved in nearly every notable criminal proceeding in the Eastern United States.<ref name="obituary" /> Among his most famous legal undertakings was the defense of [[Daniel Sickles]] during his trial for the murder of [[Philip Barton Key]], the then [[Attorney General of the District of Columbia]]. During this trial, Brady worked with [[Edwin Stanton]], who would go on to become the [[United States Secretary of War]]. Brady also defended [[Lew Baker]] at the murder trial of the infamous [[William Poole|William "Bill the Butcher" Poole]], whom Baker shot to death in 1855 at Stanwix Hall, a bar on [[Broadway (Manhattan)|Broadway]] in [[Manhattan]].<ref name="obituary" /><ref>{{cite news|url=https://en.wikisource.org/wiki/The_New_York_Times/Shooting|title=Terrible Shooting Affray in Broadway; Bill Poole Fatally Wounded |work=[[The New York Times|The New York Daily Times]]|date=February 26, 1855|page=1}} (Wikisource)</ref> Over his career, Brady tried 52 criminal cases and lost only one, the case of [[Confederate States Navy|Confederate privateer]] [[John Yates Beall]].{{sfn|Clarke|1869|p=722}}

Though he professed membership in the [[Democratic Party (United States)|Democratic Party]], Brady was notably disengaged from politics. In 1843 he was made the interim [[New York County District Attorney|district attorney for New York County]]. Two years later, Brady was appointed the city's [[corporation counsel]], a position in which he served two terms, each term lasting for a year. In 1850 Brady ran for, but was not elected to, the position of [[Attorney General of New York]]. Brady was on [[John C. Breckinridge]] and [[Joseph Lane]]'s 1860 Democratic ticket for [[Governor of New York]]. When the [[American Civil War]] began, Brady switched sides and became an ardent supporter of [[Abraham Lincoln]] and his [[Republican Party (United States)|Republican Party]]. He deeply disdained Southern politics and policy.<ref name="obituary">{{cite news|title=Death of James T. Brady-Sketch of His Life and Character|url=https://query.nytimes.com/mem/archive-free/pdf?res=9A00E5DA1E3AEF34BC4852DFB4668382679FDE|work=[[The New York Times]]|date=1869}}</ref>

== Personal life and death ==
Brady remained a lifelong [[bachelor]]. When a friend asked why, he said, "When my father died, he left five daughters who looked to me for support. All the affection I could have had for a wife went out to those sisters; and I have never desired to recall it."{{sfn|Clarke|1869|p=716}} On Sunday, February 7, 1869 the left side of Brady's body was paralyzed. He died at the age of 53 at his home in New York on February 9 at 4:45 pm.<ref name="obituary" /> Brady's funeral occurred at [[St. Patrick's Cathedral (Manhattan)|St. Patrick's Cathedral]], and he was interred at [[Calvary Cemetery (Queens, New York)|Calvary Cemetery]].<ref name="obituary" />

== References ==
{{Reflist|30em}}

'''Bibliography'''
{{Refbegin|30em}}
* {{cite journal|last1=Clarke|first1=I. Edwards|title=James T. Brady|journal=The Galaxy|date=May 1869|volume=7|issue=5|url=http://digital.library.cornell.edu/cgi/t/text/text-idx?c=gala;idno=gala0007-5|ref=harv}}
* {{cite book|last1=Houghton|first1=Walter Raleigh|title=Kings of fortune; or, The triumphs and achievements of noble, self-made men, whose brilliant careers have honored their calling, blessed humanity, and whose lives furnish instruction for the young, entertainment for the old, and valuable lessons for the aspirants of fortune|date=1885|publisher=A.E. Davis & Co.|location=London|ref=harv}}
* {{cite book|last1=Scott|first1=Henry Wilson|title=Distinguished American Lawyers: With Their Struggles and Triumphs in the Forum|date=1891|publisher=[[Charles L. Webster and Company]]|ref=harv}}
* {{cite book|last1=Spassky|first1=Natalie|editor1-last=Luhrs|editor1-first=Kathleen|title=American Paintings: A Catalogue of the Collection of the Metropolitan Museum of Art, Volumes 1-2|date=1965|publisher=[[Metropolitan Museum of Art]]|location=New York|isbn=0-87099-439-5|ref=harv}}
* {{cite book|author1=Unknown|title=The American Law Review, Volume 3|date=1869|publisher=[[Little, Brown and Company]]|ref=harv}}
{{Refend}}

{{Commons category}}

{{authority control}}

{{DEFAULTSORT:Brady, James T.}}
[[Category:1815 births]]
[[Category:1869 deaths]]
[[Category:People from New York City]]
[[Category:New York lawyers]]
[[Category:New York Democrats]]
[[Category:New York Republicans]]
[[Category:New York gubernatorial candidates]]
[[Category:19th-century American politicians]]