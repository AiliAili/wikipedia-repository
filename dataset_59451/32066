{{use British English|date=November 2012}}
{{infobox constellation
| name = Leo Minor
| abbreviation = LMi
| genitive = Leonis Minoris
| pronounce = {{IPAc-en|ˌ|l|iː|oʊ|_|ˈ|m|aɪ|n|ər}},<br>genitive {{IPAc-en|l|iː|ˈ|oʊ|n|ᵻ|s|_|m|ᵻ|ˈ|n|ɒr|ᵻ|s}}
| symbolism = the lesser [[Lion]]
| RA = {{RA|9|22.4}} to {{RA|11|06.5}}
| dec= 22.84° to 41.43°{{sfn|IAU, ''The Constellations'', Leo Minor}}
| family = [[Ursa Major Family|Ursa Major]]
| quadrant = NQ2
| areatotal = 232
| arearank = 64th
| numbermainstars = 3
| numberbfstars = 34
| numberstarsplanets = 3
| numberbrightstars = 0
| numbernearbystars = 0
| brighteststarname = [[46 Leonis Minoris|46 LMi]] (Praecipua)
| starmagnitude = 3.83
| neareststarname = [[11 Leonis Minoris|11 LMi]]
| stardistancely = 36.46
| stardistancepc = 11.18
| numbermessierobjects = 0
| meteorshowers = [[Leonis Minorids]]
| bordering = [[Ursa Major]]<br />[[Lynx (constellation)|Lynx]]<br />[[Cancer (constellation)|Cancer]] (corner)<br />[[Leo (constellation)|Leo]]
| latmax = [[North Pole|90]]
| latmin = [[45th parallel south|45]]
| month = April
| notes= }}
'''Leo Minor''' is a small and faint [[constellation]] in the [[Northern Hemisphere|northern celestial hemisphere]]. Its name is Latin for "the smaller lion", in contrast to [[Leo (constellation)|Leo]], the larger lion. It lies between the larger and more recognizable [[Ursa Major]] to the north and Leo to the south. Leo Minor was not regarded as a separate constellation by classical [[Astronomy|astronomers]]; it was designated by [[Johannes Hevelius]] in 1687.{{sfn|Ridpath|Tirion|2001|pp=168–69}}

There are 37 stars brighter than [[apparent magnitude]]&nbsp;6.5 in the constellation; three are brighter than magnitude&nbsp;4.5. [[46&nbsp;Leonis Minoris]], an orange [[giant star|giant]] of magnitude&nbsp;3.8, is located some 95&nbsp;[[light-year]]s from Earth. At magnitude&nbsp;4.4, [[Beta Leonis Minoris]] is the second-brightest star and the only one in the constellation with a [[Bayer designation]]. It is a [[binary star]], the brighter component of which is an orange giant and the fainter a yellow-white [[main sequence]] star. The third-brightest star is [[21&nbsp;Leonis Minoris]], a rapidly rotating [[A-type main-sequence star|white main-sequence star]] of average magnitude&nbsp;4.5. The constellation also includes two stars with [[planetary system]]s, two pairs of [[interacting galaxies]], and the unique [[deep-sky object]] [[Hanny's Voorwerp]].

==History==
The classical astronomers [[Aratus]] and [[Ptolemy]] had noted the region of what is now Leo Minor to be undefined and not containing any distinctive pattern; Ptolemy classified the stars in this area as ''amorphōtoi'' (not belonging to a constellation outline) within the constellation Leo.{{sfn|Allen|1963|p=263}}

[[Johannes Hevelius]] first depicted Leo Minor in 1687 when he outlined ten new constellations in his star atlas ''Firmamentum Sobiescianum'',{{sfn|Ridpath, ''Star Tales'' Filling the remaining gaps}} and included 18 of its objects in the accompanying ''Catalogus Stellarum Fixarum''.{{sfn|Hevelius|1687|pp=214–15}} Hevelius decided upon Leo Minor or Leo Junior as a depiction that would align with its beastly neighbours the Lion and the Great Bear.{{sfn|Wagman|2003|pp=189–90}} In 1845, English astronomer [[Francis Baily]] revised the catalogue of Hevelius's new constellations, and assigned a Greek letter known as [[Bayer designation]] to stars brighter than [[apparent magnitude]] 4.5.{{sfn|Wagman|2003|p=8}} [[Richard A. Proctor]] gave the constellation the name ''Leaena'' "the Lioness" in 1870,{{sfn|Allen|1963|p=263}} explaining that he sought to shorten the constellation names to make them more manageable on celestial charts.{{sfn|Proctor|1870|pp=16–17}}

German astronomer [[Christian Ludwig Ideler]] posited that the stars of Leo Minor had been termed ''Al Thibā' wa-Aulāduhā'' "Gazelle with her Young" on a 13th-century Arabic celestial globe, recovered by Cardinal [[Stefano Borgia]] and housed in the prelate's museum at [[Velletri]].{{sfn|Allen|1963|p=42}}<ref>See also Mark R. Chartrand III (1983) Skyguide: A Field Guide for Amateur Astronomers, p. 158 (ISBN 0-307-13667-1).</ref>  Arabist [[Friedrich Wilhelm Lach]] describes a different view, noting that they had been seen as ''Al Haud'' "the Pond", which the Gazelle jumps into.{{sfn|Allen|1963|p=263}} In [[Chinese astronomy]], the stars Beta, 30, 37 and 46 Leonis Minoris made up ''Neiping'', a "Court of Judge or Mediator", or ''Shi'' "Court Eunuch"{{sfn|Ridpath, ''Star Tales'' Leo Minor}} or were combined with stars of the neighbouring Leo to make up a large celestial dragon or State Chariot.{{sfn|Allen|1963|p=263}}<ref>See also Chartrand, at p. 158.</ref>  A line of four stars was known as ''Shaowei''; it represented four Imperial advisors and may have been located in Leo Minor, Leo or adjacent regions.{{sfn|Ridpath, ''Star Tales'' Leo Minor}}

==Characteristics==
A dark area of the sky with a triangle of brighter stars just visible to the naked eye in good conditions,{{sfn|Thompson|Thompson|2007|p=290}} Leo Minor has been described by [[Patrick Moore]] as having "dubious claims to a separate identity".{{sfn|Moore|2000|p=103}} It is a small constellation bordered by [[Ursa Major]] to the north, [[Lynx (constellation)|Lynx]] to the west, [[Leo (constellation)|Leo]] to the south, and touching the corner of [[Cancer (constellation)|Cancer]] to the southwest. The three-letter abbreviation for the constellation, as adopted by the [[International Astronomical Union]] in 1922, is 'LMi'.{{sfn|Russell|1922|p=469}} The official constellation boundaries, as set by [[Eugène Joseph Delporte|Eugène Delporte]] in 1930, are defined by a polygon of 16&nbsp;[[Edge (geometry)|sides]]. In the [[equatorial coordinate system]], the [[right ascension]] coordinates of these borders lie between {{RA|9|22.4}} and {{RA|11|06.5}}, while the [[declination]] coordinates are between 22.84° and 41.43°.{{sfn|IAU, ''The Constellations'', Leo Minor}} Ranked 64th out of 88 constellations in size, Leo Minor covers an area of 232.0&nbsp;square degrees, or 0.562&nbsp;percent of the sky.{{sfn|Ridpath, ''Star Tales'' Constellations – part two}} It [[Culmination|culminates]] each year at midnight on February 24,{{sfn|Thompson|Thompson|2007|p=290}} and at 9 p.m. on May 24.{{sfn|The Constellations : Part 2 Culmination Times}}

==Notable features==

===Stars===
{{see also|List of stars in Leo Minor}}
There are only three [[star]]s in the constellation brighter than magnitude 4.5,{{sfn|Wagman|2003|pp=189–90}} and 37&nbsp;stars with a magnitude brighter than 6.5.{{sfn|Ridpath, ''Star Tales'' Constellations – part two}} Leo Minor does not have a star designated Alpha because Baily erred and allocated a Greek letter to only one star, Beta.{{sfn|Ridpath|Tirion|2001|pp=168–69}} It is unclear whether he intended to give [[46&nbsp;Leonis Minoris]] a Bayer designation, as he recognized Beta and 46&nbsp;Leonis Minoris as of the appropriate brightness in his catalogue. He died before revising his proofs, which might explain this star's omission.{{sfn|Wagman|2003|pp=189–90}}

At magnitude 3.8, the brightest star in Leo Minor is an orange [[giant star|giant]] of [[Stellar classification|spectral class]] K0III named 46&nbsp;Leonis Minoris or Praecipua;{{sfn|SIMBAD 46 Leonis Minoris}} its colour is evident when seen through binoculars.{{sfn|Moore|2000|p=103}} Situated {{Convert|95|ly|pc|abbr=off|lk=on|sigfig=2}} from Earth,{{sfn|Ridpath|Tirion|2001|pp=168–69}} it has around 32&nbsp;times the [[luminosity]] and is 8.5&nbsp;times the size of the Sun.{{sfn|Kaler, ''Praecipua''}} It was also catalogued and named as o Leonis Minoris by [[Johann Elert Bode]], which has been misinterpreted as Omicron Leonis Minoris.{{sfn|Wagman|2003|pp=189–90}} More confusion occurred with its proper name ''Praecipua'', which appears to have been originally applied to [[37 Leonis Minoris]] in the 1814 ''Palermo Catalogue'' of [[Giuseppe Piazzi]], who mistakenly assessed the latter star as the brighter. This name was later connected by Allen with 46 Leonis Minoris—an error perpetuated by subsequent astronomers.{{sfn|Ridpath, ''Star Tales'' Leo Minor}} The original "Praecipua", 37 Leonis Minoris, has an apparent magnitude of 4.69,{{sfn|SIMBAD 37 Leonis Minoris}} but is a distant yellow [[supergiant]] of spectral type G2.5IIa and absolute magnitude of −1.84,{{sfn| Kovtyukh et al.|2010}} around {{Convert|578|ly|pc|abbr=off|lk=off|sigfig=3}} distant.{{sfn|SIMBAD 37 Leonis Minoris}}

[[File:LeoMinorCC.jpg|thumb|left|upright|Leo Minor as seen by the naked eye]]
[[Beta Leonis Minoris]] is a binary star system. The primary is a giant star of spectral class G8 and apparent magnitude of 4.4. It has around double the mass, 7.8&nbsp;times the radius and is 36&nbsp;times the luminosity of the Earth's Sun. Separated by 11&nbsp;seconds of arc from the primary, the secondary is a [[F-type main-sequence star|yellow-white main sequence star]] of spectral type F8. The two orbit around a common [[Barycentric coordinates (astronomy)|centre of gravity]] every 38.62&nbsp;years,{{sfn|Kaler, ''Beta Leonis Minoris''}} and lie {{Convert|154|ly|pc|abbr=off|lk=off|sigfig=2}} away from the [[Solar System]].{{sfn|SIMBAD Beta Leonis Minoris}}

Around {{Convert|98|ly|pc|abbr=off|lk=off|sigfig=2}} away and around 10&nbsp;times as luminous as the Sun, [[21&nbsp;Leonis Minoris]] is a rapidly rotating [[A-type main-sequence star|white main-sequence star]], spinning on its axis in less than 12&nbsp;hours and very likely flattened in shape.{{sfn|Kaler, ''21 Leonis Minoris''}} Of average apparent magnitude 4.5 and spectral type A7V, it is a [[Delta Scuti variable]].{{sfn|SIMBAD 21 Leonis Minoris}} These are short period (six hours at most) [[Variable star#Pulsating variable stars|pulsating]] stars which have been used as [[Cosmic distance ladder#Standard candles|standard candles]] and as subjects to study [[asteroseismology]].{{sfn|Delta Scuti and the Delta Scuti variables}}
[[File:Sidney Hall - Urania's Mirror - Leo Major and Leo Minor.jpg|thumb|right|upright|Leo Minor above the head of Leo, as depicted in ''[[Urania's Mirror]]'', a set of constellation cards published in London c.&nbsp;1825{{sfn|Ridpath, Urania’s Mirror}}]]
Also known as SU and SV Leonis Minoris, [[10&nbsp;Leonis Minoris|10]] and [[11&nbsp;Leonis Minoris]] are yellow giants of spectral type G8III, with average magnitudes 4.54 and 5.34 respectively. Both are [[RS Canum Venaticorum variable|RS Canum Venaticorum]] variables,{{sfn|SIMBAD 10 Leonis Minoris}}{{sfn|SIMBAD 11 Leonis Minoris}} with 10&nbsp;Leonis Minoris varying by 0.012&nbsp;magnitude over 40.4&nbsp;days, and 11&nbsp;Leonis Minoris by 0.033&nbsp;magnitude over 18&nbsp;days.{{sfn| Skiff|Lockwood|1986}} 11&nbsp;Leonis Minoris has a [[red dwarf]] companion of spectral type M5V and apparent magnitude 13.0.{{sfn|SIMBAD GJ 356 B}} [[20 Leonis Minoris]] is a multiple star system {{Convert|49|ly|pc|abbr=off|lk=off|sigfig=2}} away from the Sun. The main star is another yellow star, this time a [[G-type main-sequence star|dwarf]] of spectral type G3Va and apparent magnitude 5.4.{{sfn|SIMBAD 20 Leonis Minoris}} The companion is an old, active red dwarf that has a relatively high [[metallicity]] and is of spectral type M6.5. The fact that the secondary star is brighter than expected indicates it is likely two stars very close together that are unable to be made out separately with current viewing technology.{{sfn|Gizis et al.|2000}}

[[R Leonis Minoris|R]] and [[S Leonis Minoris]] are long-period [[Mira variable]]s, while [[U Leonis Minoris]] is a [[Semiregular variable star|semiregular variable]];{{sfn|Levy|2005|pp=186–87}} all three are [[red giant]]s of spectral types M6.5e-M9.0e,{{sfn|SIMBAD R Leonis Minoris}} M5e {{sfn|SIMBAD S Leonis Minoris}} and M6 respectively.{{sfn|SIMBAD U Leonis Minoris}} R varies between magnitudes 6.3 and 13.2 during a period of 372&nbsp;days, S varies between magnitudes 8.6 and 13.9 during a period of 234&nbsp;days, and U varies between magnitudes 10.0 and 13.3 during a period of 272&nbsp;days. The lack of bright stars makes finding these objects challenging for amateur astronomers.{{sfn|Levy|2005|pp=186–87}} [[G 117-B15A]], also known as RY Leonis Minoris, is a [[pulsating white dwarf]] of apparent magnitude 15.5.{{sfn|SIMBAD RY Leonis Minoris}} With a period of approximately 215&nbsp;seconds, and losing a second every 8.9&nbsp;million years, the 400-million-year-old star has been proposed as the most stable celestial clock.{{sfn|McDonald Observatory}}

[[SX Leonis Minoris]] is a [[dwarf nova]] of the SU Ursae Majoris type that was identified in 1994.{{sfn|Nogami et al.|1997}} It consists of a white dwarf and a donor star, which orbit each other every 97&nbsp;minutes. The white dwarf sucks matter from the other star onto an [[accretion disc]] and heats up to between 6000 and 10000 K. The dwarf star erupts every 34 to 64&nbsp;days, reaching magnitude 13.4 in these outbursts and remaining at magnitude 16.8 when quiet.{{sfn|Wagner et al.|1998}}<!-- cites two previous sentences --> Leo Minor contains another dwarf nova, [[RZ Leonis Minoris]], which brightens to magnitude 14.2 from a baseline magnitude of around 17 but does so at shorter intervals than other dwarf novae.{{sfn|Robertson et al.|1995}}

Two stars with planetary systems have been found. [[HD&nbsp;87883]] is an orange dwarf of magnitude&nbsp;7.57 and spectral type K0V 18&nbsp;parsecs distant from Earth. With a diameter three quarters that of Earth's sun, it is only 31 percent as luminous. It is orbited by a planet around 1.78&nbsp;times the mass of Jupiter every 7.9&nbsp;years, and there are possibly other smaller planets.{{sfn|Fischer et al.|2009}} [[HD 82886]] is a yellow dwarf of spectral type G0 and visual magnitude&nbsp;7.63.{{sfn|SIMBAD HD 82886}} A planet 1.3&nbsp;times the mass of Jupiter and orbiting every 705 days was discovered in 2011.{{sfn|Planet HD 82886 b}}

===Deep-sky objects===
[[File:A cosmological measuring tape.jpg|thumb|Spiral galaxy [[NGC 3021]] which lies about 100 million light-years away.<ref>{{cite web|title=A cosmological measuring tape|url=http://www.spacetelescope.org/images/potw1513a/|accessdate=16 June 2015}}</ref>]]
In terms of [[deep-sky object]]s, Leo Minor contains many galaxies viewable in amateur telescopes.{{sfn|Knoxville Observers – Leo Minor}} Located 3&nbsp;degrees southeast of [[38 Leonis Minoris]], [[NGC 3432]] is seen nearly edge on. Known as the knitting needle galaxy, it is of apparent magnitude&nbsp;11.7 and measures 6.8 by 1.4 arcminutes.{{sfn|Thompson|Thompson|2007|p=290}} Located 42&nbsp;million light-years away, it is moving away from the Solar System at a rate of 616&nbsp;km per second. In 2000, a star within the galaxy brightened to magnitude 17.4, and has since been determined to be a [[luminous blue variable]] and [[supernova impostor]].{{sfn|O'Meara|2011|p=196}} [[NGC&nbsp;3003]], a SBbc [[barred spiral galaxy]] with an apparent magnitude of 12.3 and an angular size of 5.8&nbsp;[[Minute of arc|arcminutes]], is seen almost edge-on.{{sfn|NED NGC 3003}} [[NGC 3344]], 25&nbsp;million light-years distant, is face-on towards Earth.{{sfn|galaxy in a spin}} Measuring 7.1 by 6.5&nbsp;arcminutes in size, it has an apparent magnitude of 10.45.{{sfn|NED NGC 3344}}  [[NGC&nbsp;3504]] is a [[starburst galaxy|starburst]] barred spiral galaxy of apparent magnitude&nbsp;11.67 and measuring 2.1&nbsp;by 2.7&nbsp;arcminutes.{{sfn|NED NGC 3504}} It has hosted supernovae in 1998{{sfn|Garnavich|1998}} and 2001.{{sfn|Matheson et al.|2001}} It and the [[spiral galaxy]] [[NGC&nbsp;3486]] are also almost face-on towards Earth; the latter is of magnitude&nbsp;11.05 and measures 7.1&nbsp;by 5.2&nbsp;arcminutes.{{sfn|NED NGC 3486}} [[NGC&nbsp;2859]] is an SB0-type [[lenticular galaxy]].{{sfn|NED NGC 2859}}

At least two pairs of interacting galaxies have been observed. [[Arp&nbsp;107]] is a pair of galaxies in the process of merging, located 450&nbsp;million light-years away.{{sfn|Spitzer Arp 107}} [[NGC&nbsp;3395]] and [[NGC&nbsp;3396]] are a spiral and irregular barred spiral galaxy, respectively, that are interacting, located 1.33&nbsp;degrees southwest of 46 Leonis Minoris.{{sfn|Motz|Nathanson|1991|p=180}}

The unique deep-sky object known as [[Hanny's Voorwerp]] was discovered in Leo Minor in 2007 by Dutch school teacher Hanny van Arkel while participating as a volunteer in the [[Galaxy Zoo]] project. Lying near the 650-million-light-year-distant spiral galaxy [[IC&nbsp;2497]], it is around the same size as the [[Milky Way]].{{sfn|Hubble Zooms in on a Space Oddity}} It contains a 16,000-light-year-wide hole.{{sfn|Radio observations shed new light on Hanny's Voorwerp}} The voorwerp is thought to be the visual light echo of a [[quasar]] now gone inactive,{{sfn|Rincon|2008}} possibly as recently as 200,000 years ago.{{sfn|Hubble Zooms in on a Space Oddity}}

===Meteor shower===
Discovered by Dick McCloskey and Annette Posen of the Harvard Meteor Program in 1959,{{sfn|Jenniskens|2006|pp=83–84}} the [[Leonis Minorids|Leonis Minorid]] meteor shower peaks between October&nbsp;18 and October&nbsp;29. The shower's parent body is the long period comet [[C/1739 K1 (Zanotti)]].{{sfn|Jenniskens|2012}} It is a minor shower, and can only be seen from the Northern Hemisphere.{{sfn|IMO Meteor Shower Calendar 2012}}

==See also==

*[[3C 234]]

==References==
'''Citations'''
{{Reflist|30em}}
'''Sources'''
{{refbegin|30em}}
* {{cite book
  | last = Allen
  | first = Richard Hinckley
  | authorlink = Richard Hinckley Allen
  | date = 1963
  | origyear = 1899
  | title = Star Names: Their Lore and Meaning
  | edition = reprint
  | publisher = Dover Publications
  | location = New York City
  | isbn = 978-0-486-21079-7
  | ref = harv
  }}
*{{cite journal | url=http://iopscience.iop.org/0004-637X/703/2/1545/fulltext/ | title=Five Planets and an Independent Confirmation of HD 196885 Ab from Lick Observatory | last1=Fischer | first1=Debra | last2=Driscoll | first2=Peter | last3=Isaacson | first3=Howard | last4=Giguere | first4=Matt | last5=Marcy | first5=Geoffrey W. | last6=Valenti | first6=Jeff | last7=Wright | first7=Jason T. | last8=Henry | first8=Gregory W. | last9=Johnson | first9=John Asher | last10=Howard | first10=Andrew | last11=Peek | first11=Katherine | last12=McCarthy | first12=Chris | journal=The Astrophysical Journal | volume=703 | issue=2 | pages=1545–56 | date=2009 | arxiv=0908.1596 | bibcode=2009ApJ...703.1545F | doi=10.1088/0004-637X/703/2/1545
 | ref = {{harvid|Fischer et al.|2009}}
 }}
*{{cite journal
 | first1=P.
 | last1=Garnavich
 | title = Supernova 1998cf in NGC 3504
 |journal = IAU Circular
  | issue = 6914
  |page = 1
  | date= 1998
  |bibcode = 1998IAUC.6914....1G
  | ref = harv
 | volume=6914
  }}
* {{cite journal
 | first1=J. E.
 | last1=Gizis | first2=D. G. | last2=Monet | first3=I. N. | last3=Reid | first4=J. D. | last4=Kirkpatrick | first5=A. J. | last5=Burgasser | title=Two Nearby M Dwarf Binaries from 2MASS | journal=[[Monthly Notices of the Royal Astronomical Society]] | date=2000 | volume=311 | issue=2 | pages=385–88 | url=http://www.blackwell-synergy.com/doi/abs/10.1046/j.1365-8711.2000.03060.x | doi=10.1046/j.1365-8711.2000.03060.x | format=abstract page |bibcode = 2000MNRAS.311..385G
  | ref = {{harvid|Gizis et al.|2000}}
  }}
* {{cite book
  | last = Hevelius
  | first = Johannes
  | date = 1687
  | title = Catalogus Stellarum Fixarum
  | publisher = Gedani, typis J.-Z. Stollii
  |url =  http://lhldigital.lindahall.org/cdm4/document.php?CISOROOT=/astro_atlas&CISOPTR=1671&CISOSHOW=1491
  | language=Latin
  | location = Gdaņsk, Poland
  | ref = harv
  }}
*{{cite book
  |date = 2006
 |last = Jenniskens
  |first = Peter
  |title = Meteor Showers and their Parent Comets
  |isbn=978-0-521-85349-1
  |publisher= Cambridge University Press
  |location  = Cambridge, United Kingdom
 | ref = harv
  }}
*{{cite journal |journal = Sky & Telescope |date = September 2012 |last = Jenniskens |first = Peter |page = 22 |title = Mapping Meteoroid Orbits: New Meteor Showers Discovered
 | ref = harv
  }}
* {{cite journal
 |author1=Kovtyukh, V. V. |author2=Chekhonadskikh, F. A. |author3=Luck, R. E. |author4=Soubiran, C. |author5=Yasinskaya, M. P. |author6=Belik, S. I. | title=Accurate Luminosities for F-G Supergiants from Fe II/Fe I Line Depth Ratios
 | journal=Monthly Notices of the Royal Astronomical Society
 | date=2010
 | volume=408
 | issue=3
 | pages=1568–75
 | doi=10.1111/j.1365-2966.2010.17217.x
  | ref = {{harvid| Kovtyukh et al.|2010}}
  |bibcode = 2010MNRAS.408.1568K }}
* {{cite book
  | last = Levy
  | first = David H.
  | date = 2005
  | title = David Levy's Guide to Variable Stars
  | publisher = Cambridge University Press
  |location  = Cambridge, United Kingdom
  | isbn = 978-0-521-60860-2
  | ref = harv
  }}
*{{cite journal
  | first1=P.
  | last1=Matheson
  | first2 =S.
  | last2 = Jha
  | first3 =P.
  | last3 = Challis
  | first4 =R.
  | last4 = Kirshner
  | first5 =M.
  | last5 = Calkins
  | title = Supernova 2001ac in NGC 3504
   |journal = IAU Circular
  | issue = 7597
  |page = 3
  | date= 2001
  |bibcode = 2001IAUC.7597....3M
  | ref = {{harvid|Matheson et al.|2001}}
  | volume=7597
  }}
* {{cite book
  | last1 = Moore
  | first1 = Patrick
  | date = 2000
  | title = Exploring the Night Sky with Binoculars
  | publisher = Cambridge University Press
  | location = Cambridge, United Kingdom
  | isbn = 978-0-521-79390-2
  | ref = harv
  }}
* {{cite book
  | last1 = Motz
  | first1 = Lloyd
  | last2 = Nathanson
  | first2 = Carol
  | date = 1991
  | title = The Constellations: An Enthusiast's Guide to the Night Sky
  | publisher = Aurum Press
  | location = London, United Kingdom
  | isbn = 978-1-85410-088-7
  | ref = harv
  }}
* {{cite journal
  | last1 = Nogami
  | first1 = Daisaku
  | last2 = Masuda
  | first2 = Seiji
  | last3 = Kato
  | first3 = Taichi
  |date=1997
  |title= The 1994 Superoutburst of the New SU UMa-type Dwarf Nova, SX Leonis Minoris
  |journal=  Publications of the Astronomical Society of the Pacific
  |volume=109
  |pages=1114–21
  |bibcode=1997PASP..109.1114N
  |url= http://articles.adsabs.harvard.edu//full/1997PASP..109.1114N/0001114.000.html
  | ref = {{sfnRef|Nogami et al.|1997}}
  | doi = 10.1086/133983
}}
* {{cite book
  | last = O'Meara
  | first = Stephen James
  | date = 2011
  | title = Deep-Sky Companions: The Secret Deep
  | publisher = Cambridge University Press
  | location = Cambridge, United Kingdom
  | isbn = 978-0-521-19876-9
  | ref = harv
  }}
* {{cite book
  | last = Proctor
  | first = Richard Anthony
  | authorlink = Richard A. Proctor
  | date = 1870
  | title = A Star Atlas for the Library, the School and the Observatory
  | publisher = Longmans, Green
  | location = London, United Kingdom
  | url = https://books.google.com/?id=yzRRAAAAYAAJ&pg=PA17&dq=Leaena+Proctor
  | isbn = 
  | ref = harv
  }}
* {{Cite book |title = Stars and Planets Guide |last1 = Ridpath |first1 = Ian |last2 = Tirion |first2 = Wil |date = 2001 |publisher = Princeton University Press |isbn = 0-691-08913-2 |ref = harv }}
* {{cite journal
  | author = Robertson, J. W.; Honeycutt, R. K.; Turner, G. W.
  | date = 1995
  | title = RZ Leonis Minoris, PG 0943+521, and V1159 Orionis: Three Cataclysmic Variables with Similar and Unusual Outburst Behavior
  | journal = Publications of the Astronomical Society of the Pacific
  | volume = 107
  | pages = 443–49
  | bibcode = 1995PASP..107..443R
  | ref = {{sfnRef|Robertson et al.|1995}}
  | last2 = Honeycutt
  | last3 = Turner
  | doi = 10.1086/133572
  }}
* {{cite journal
  | last = Russell
  | first = Henry Norris
  |date=October 1922
  | title = The New International Symbols for the Constellations
  | journal = [[Popular Astronomy (US magazine)|Popular Astronomy]]
  | volume = 30
  | pages = 469–71
  | bibcode = 1922PA.....30..469R
  | ref = harv
  }}
* {{cite journal
  | last = Skiff
  | first = Brian A.
  | last2 = Lockwood
  | first2 = G.W.
  | date = 1986
  | title = The Photometric Variability of Solar-type Stars. V. The Standard Stars 10 and 11 Leonis Minoris
  | journal = Publications of the Astronomical Society of the Pacific
  | volume = 98
  | issue = 601
  | pages = 338–41
  | jstor = 40678678
  | ref = harv
  | bibcode = 1986PASP...98..338S
  | doi = 10.1086/131763
  }}
* {{cite book
  | last = Thompson
  | first = Robert Bruce
  | last2 = Thompson
  | first2 = Barbara Fritchman
  | date = 2007
  | title = Illustrated Guide to Astronomical Wonders: From Novice to Master Observer
  | publisher = O'Reilly Media
  | location = North Sebastopol, California
  | isbn = 978-0-596-52685-6
  | ref = harv
  }}
* {{cite book
  | last = Wagman
  | first = Morton
  | date = 2003
  | title = Lost Stars: Lost, Missing and Troublesome Stars from the Catalogues of Johannes Bayer, Nicholas Louis de Lacaille, John Flamsteed, and Sundry Others
  | publisher = The McDonald & Woodward Publishing Company
  | location = Blacksburg, Virginia
  | isbn = 978-0-939923-78-6
  | ref = harv
  }}
* {{cite journal
  | author = Wagner, R. Mark; Thorstensen,  John R.; Honeycutt, R. K.; Howell, S. B.; Kaitchuck, R. H.; Kreidl, T. J.; Robertson, J. W.; Sion, E. M.; Starrfield, S. G.
  |date=1998
  |title= A Photometric and Spectroscopic Study of the Cataclysmic Variable SX Leonis Minoris in Quiescence and Superoutburst
  |journal= The Astronomical Journal
  |volume=109
  |pages=787–800
  |doi=10.1086/300201
  |url= http://iopscience.iop.org/1538-3881/115/2/787/fulltext/
  |ref = {{sfnRef|Wagner et al.|1998}}
  | issue = 2
|bibcode = 1998AJ....115..787W | last2 = Thorstensen
  | last3 = Honeycutt
  | last4 = Howell
  | last5 = Kaitchuck
  | last6 = Kreidl
  | last7 = Robertson
  | last8 = Sion
  | last9 = Starrfield
  
  
  }}
{{refend}}
'''Online sources'''
{{refbegin|30em}}
* {{cite web
  |author =Baldwin, Emily
  | title = Radio observations shed new light on Hanny's Voorwerp
  | work = Astronomy Now
  | publisher = Pole Star Publications Ltd.
  | url = http://astronomynow.com/news/n1006/25voorwerp/
  | accessdate = 8 November 2012
  | date = 25 June 2010
  | ref = {{sfnRef|Radio observations shed new light on Hanny's Voorwerp}}
  }}
* {{cite web
  |author =Beck, Sara J.
  | title = Delta Scuti and the Delta Scuti variables
  | work = Variable Star of the Season
  | publisher = AAVSO (American Association of Variable Star Observers)
  | url = http://www.aavso.org/vsots_delsct
  | accessdate = 8 November 2012
  | date = 16 July 2010
  | ref = {{sfnRef|Delta Scuti and the Delta Scuti variables}}
  }}
* {{cite web
  | title = Leo Minor, Constellation Boundary
  | work = The Constellations
  | publisher = International Astronomical Union
  | url = http://www.iau.org/public/constellations/#lmi
  | accessdate = 15 October 2012
  | ref = {{sfnRef|IAU, ''The Constellations'', Leo Minor}}
  }}
*{{cite web
  | work= The Extrasolar Planets Encyclopaedia
  | title=Planet HD 82886 b
  |author = Schneider, Jean
  |publisher = Paris Observatory
  | url=http://exoplanet.eu/catalog/hd_82886_b/
  | accessdate=25 October 2012
  | ref = {{sfnRef|Planet HD 82886 b}}
}}
*{{cite web
  |title=ESA/Hubble Picture of the Week: Galaxy in a spin
  |url=http://www.spacetelescope.org/images/potw1242a/
  |accessdate=23 October 2012
  |work=Hubble website
  | ref = {{sfnRef|galaxy in a spin}}
}}
*{{cite web
  | title = IMO Meteor Shower Calendar 2012   &#124; International Meteor Organization
  | publisher=The International Meteor Organization
  | date = 1997–2012
  | url = http://www.imo.net/calendar/2012#lmi
  | accessdate =31 October 2012
  | ref = {{sfnRef|IMO Meteor Shower Calendar 2012}}
}}
*{{cite web
 | url= http://www.southastrodel.com/Page20502.htm
 | title='The '"Constellations : Part 2 Culmination Times"'
 | work=Southern Astronomical Delights
 | date= 7 February 2011
 | accessdate=19 October 2012
 |author=James, Andrew
 |location=Sydney, New South Wales
 | ref = {{sfnRef|The Constellations : Part 2 Culmination Times}}
}}
* {{cite web
  |title=Hubble Zooms in on a Space Oddity
  |url=http://www.spacetelescope.org/news/heic1102/
  |publisher=[[European Southern Observatory]]
  |date=10 January 2011
  |accessdate=8 November 2012
  | ref = {{sfnRef|Hubble Zooms in on a Space Oddity}}
}}

* {{cite web
  | last = Kaler
  | first = Jim
  | title = Beta Leonis Minoris
  | work = Stars
  | publisher = University of Illinois
  | url = http://stars.astro.illinois.edu/sow/betalmi.html
  | accessdate = 30 March 2007
  | ref = {{sfnRef|Kaler, ''Beta Leonis Minoris''}}
  }}
* {{cite web
  | last = Kaler
  | first = Jim
  | title = 21 Leonis Minoris
  | work = Stars
  | publisher = University of Illinois
  | url = http://stars.astro.illinois.edu/sow/21lmi.html
  | accessdate = 15 October 2012
  | ref = {{sfnRef|Kaler, ''21 Leonis Minoris''}}
  }}
* {{cite web
  | last = Kaler
  | first = Jim
  | title = Praecipua
  | work = Stars
  | publisher = University of Illinois
  | url = http://stars.astro.illinois.edu/sow/praecipua.html
  | accessdate = 15 October 2012
  | ref = {{sfnRef|Kaler, ''Praecipua''}}
  }}
* {{cite web
  | last = Grant
  | first = Shawn
  | title = Leo Minor
  | work = Deepsky Online
  | publisher = Knoxville Observers
  | url = http://www.knoxvilleobservers.org/dsonline/spring/leominor.html
  | accessdate = 14 November 2012
  | ref = 
{{sfnRef|Knoxville Observers – Leo Minor}}
}}
* {{cite web
   | last =McDonald Observatory
   | title =Astronomers Find Most Stable Optical Clock in Heavens; Aids Understanding of Stars' Lives
   | work = 
   | publisher =  Phys.Org
   | date = 5 December 2005
   | url = http://www.spaceref.com/news/viewpr.html?pid=18424
   | accessdate = 22 October 2012
   | ref = {{sfnRef|McDonald Observatory}}
}}
*{{cite web
  | work=NASA/IPAC Extragalactic Database
  | title=Results for NGC 2859
  |author = Jet Propulsion Laboratory, California Institute of Technology
  |publisher = National Aeronautics and Space Administration
  | url=http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=NGC+2859&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES
  | accessdate=23 October 2012
  | ref = {{sfnRef|NED NGC 2859}}
}}
*{{cite web
  | work=NASA/IPAC Extragalactic Database
  | title=Results for NGC 3003
  |author = Jet Propulsion Laboratory, California Institute of Technology
  |publisher = National Aeronautics and Space Administration
  | url=http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=NGC+3003&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES
  | accessdate=23 October 2012
  | ref = {{sfnRef|NED NGC 3003}}
}}
*{{cite web
  | work=NASA/IPAC Extragalactic Database
  | title=Results for NGC 3344
  |author = Jet Propulsion Laboratory, California Institute of Technology
  |publisher = National Aeronautics and Space Administration
  | url=http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=NGC+3344&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES
  | accessdate=23 October 2012
  | ref = {{sfnRef|NED NGC 3344}}
}}
*{{cite web
  | work=NASA/IPAC Extragalactic Database
  | title=Results for NGC 3486
  |author = Jet Propulsion Laboratory, California Institute of Technology
  |publisher = National Aeronautics and Space Administration
  | url=http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=NGC+3486&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES
  | accessdate=23 October 2012
  | ref = {{sfnRef|NED NGC 3486}}
}}
*{{cite web
  | work=NASA/IPAC Extragalactic Database
  | title=Results for NGC 3504
  |author = Jet Propulsion Laboratory, California Institute of Technology
  |publisher = National Aeronautics and Space Administration
  | url=http://ned.ipac.caltech.edu/cgi-bin/objsearch?objname=NGC+3504&extend=no&hconst=73&omegam=0.27&omegav=0.73&corr_z=1&out_csys=Equatorial&out_equinox=J2000.0&obj_sort=RA+or+Longitude&of=pre_text&zv_breaker=30000.0&list_limit=5&img_stamp=YES
  | accessdate=23 October 2012
  | ref = {{sfnRef|NED NGC 3504}}
}}
* {{cite web
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | date = 1988
  | title = Urania’s Mirror
  | work = Old Star Atlases
  | url = http://www.ianridpath.com/atlases/urania.htm
  | accessdate = 5 November 2012
  | ref = {{sfnRef|Ridpath, Urania’s Mirror}}
  }}
* {{cite web
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | date = 1988
  | title = Constellations&nbsp;– Part Two
  | work = Star Tales
  | url = http://www.ianridpath.com/constellations2.htm
  | accessdate = 20 October 2012
  | ref = {{sfnRef|Ridpath, ''Star Tales'' Constellations – part two}}
  }}
* {{cite web
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | date = 1988
  | title = Leo Minor
  | work = Star Tales
  | url = http://www.ianridpath.com/startales/leominor.htm
  | accessdate = 5 November 2012
  | ref = {{sfnRef|Ridpath, ''Star Tales'' Leo Minor}}
  }}
* {{cite web
  | last = Ridpath
  | first = Ian
  | authorlink = Ian Ridpath
  | date = 1988
  | title = Chapter One Concluded...Filling the Remaining Gaps
  | work = Star Tales
  | url = http://www.ianridpath.com/startales/startales1d.htm
  | accessdate = 20 October 2012
  | ref = {{sfnRef|Ridpath, ''Star Tales'' Filling the remaining gaps}}
  }}
*{{cite web|url=http://news.bbc.co.uk/1/hi/sci/tech/7543776.stm |title=Teacher finds new cosmic object |last= Rincon |first= Paul |work= BBC News |date= 5 August 2008|accessdate=19 October 2012
  | ref = {{sfnRef|Rincon|2008}}
}}
* {{cite web
  | title = Beta Leonis Minoris
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=bet+lmi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 8 November 2012
  | ref = {{sfnRef|SIMBAD Beta Leonis Minoris}}
  }}
* {{cite web
  | title = SU Leonis Minoris – Variable Star of RS CVn type
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=10+LMi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 20 October 2012
  | ref = {{sfnRef|SIMBAD 10 Leonis Minoris}}
  }}
* {{cite web
  | title = SV Leonis Minoris – Variable Star of RS CVn type
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=CCDM%20J09357%2B3549%20A
  | accessdate = 20 October 2012
  | ref = {{sfnRef|SIMBAD 11 Leonis Minoris}}
  }}
* {{cite web
  | title = 21 Leonis Minoris – Variable Star of delta Sct type
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=21+Leonis+Minoris&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 15 October 2012
  | ref = {{sfnRef|SIMBAD 21 Leonis Minoris}}
  }}
* {{cite web
  | title = 37 Leonis Minoris
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=37+LMi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 5 November 2012
  | ref = {{sfnRef|SIMBAD 37 Leonis Minoris}}
  }}
* {{cite web
  | title = 46 Leonis Minoris – Variable Star
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=46+LMi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 2 November 2012
  | ref = {{sfnRef|SIMBAD 46 Leonis Minoris}}
  }}
* {{cite web
  | title = GJ 356 B – Star in double system
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=CCDM%20J09357%2B3549%20B
  | accessdate = 20 October 2012
  | ref = {{sfnRef|SIMBAD GJ 356 B}}
  }}
* {{cite web
  | title = HD 82886
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=HD%2082886
  | accessdate = 19 October 2012
  | ref = {{sfnRef|SIMBAD HD 82886}}
  }}
* {{cite web
  | title = LHS 2216 – High Proper Motion Star
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=20+Leonis+Minoris&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 21 October 2012
  | ref = {{sfnRef|SIMBAD 20 Leonis Minoris}}
  }}
* {{cite web
  | title = R Leonis Minoris – Variable Star of Mira Ceti type
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=R+Leonis+Minoris&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 22 October 2012
  | ref = {{sfnRef|SIMBAD R Leonis Minoris}}
  }}
* {{cite web
  | title = RY Leonis Minoris – Pulsating White Dwarf
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=G117-B15A&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 22 October 2012
  | ref = {{sfnRef|SIMBAD RY Leonis Minoris}}
  }}
* {{cite web
  | title = S Leonis Minoris – Variable Star of Mira Ceti type
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=S+Lmi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 22 October 2012
  | ref = {{sfnRef|SIMBAD S Leonis Minoris}}
  }}
* {{cite web
  | title = U Leonis Minoris – Semi-regular pulsating Star
  | work = SIMBAD Astronomical Database
  | publisher = Centre de Données astronomiques de Strasbourg
  | url = http://simbad.u-strasbg.fr/simbad/sim-id?Ident=U+Lmi&NbIdent=1&Radius=2&Radius.unit=arcmin&submit=submit+id
  | accessdate = 22 October 2012
  | ref = {{sfnRef|SIMBAD U Leonis Minoris}}
  }}
*{{cite web|url=http://www.spitzer.caltech.edu/images/2344-sig05-006a-Interacting-Galaxy-Pair-Arp-107|title=Interacting Galaxy Pair Arp 107|date=9 June 2005|work=Spitzer Space Telescope website|publisher=Spitzer Science Center, California Institute of Technology |accessdate=19 October 2012
  | ref = {{sfnRef|Spitzer Arp 107}}
}}
{{refend}}

==External links==
{{Commons|Leo Minor}}
*[http://www.allthesky.com/constellations/leominor/ The Deep Photographic Guide to the Constellations: Leo Minor]
*[http://www.constellation-guide.com/constellation-list/leo-minor-constellation/ Leo Minor at Constellation Guide]

{{Stars of Leo Minor}}
{{navconstel}}
{{Sky|10|00|00|+|35|00|00|10}}

{{featured article}}

{{Authority control}}

[[Category:Leo Minor| ]]
[[Category:Constellations]]
[[Category:Northern constellations]]
[[Category:Constellations listed by Johannes Hevelius]]