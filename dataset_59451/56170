{{Infobox journal
| title         = Central Asian Survey
| cover         = [[File:Central Asian Survey.png]]
| editor        = Deniz Kandiyoti
| discipline    = [[Central Asian studies]]
| peer-reviewed = 
| language      = English
| former_names  = 
| abbreviation  = 
| publisher     = [[Routledge]]
| country       = 
| frequency     = Quarterly
| history       = 1982-present
| openaccess    = 
| license       = 
| impact        = 
| impact-year   = 
| website       = http://www.tandf.co.uk/journals/carfax/02634937.html
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| JSTOR         = 
| OCLC          = 645290357
| LCCN          = 86641151
| CODEN         = 
| ISSN          = 0263-4937
| eISSN         = 
| boxwidth      = 
}}
'''Central Asian Survey''' is an academic journal first published in 1982 concerning [[Caucasus]] and [[Central Asian studies]]. It is published by [[Taylor & Francis]], and has four issues a year.

According to the editorial staff, "The central aim of the journal is to reflect and promote advances in area-based scholarship in the social sciences and humanities and enhance understanding of processes of local and regional change that make Central Asia and the Caucasus an area of significant contemporary interest."<ref>[http://www.informaworld.com/smpp/title~db=all~content=t713409859~tab=summary Central Asian Survey - Aims & Scope] Informaworld</ref>

The editor is [[Deniz Kandiyoti]]. Other scholars serving on the editorial board include [[Touraj Atabaki]], Sally Cummings, Bhavna Dave, and [[Olivier Roy (professor)|Olivier Roy]], among others. The journal's international advisory board also includes Roy Allison, Adeeb Khalid, Yacov Ro'i, [[Nazif Shahrani]], [[Ronald Grigor Suny|Ronald Suny]], and author [[Ahmed Rashid]].<ref>[http://www.informaworld.com/smpp/title~db=all~content=t713409859~tab=editorialboard Central Asian Survey - Editorial Board] Informaworld</ref>

Several members of the editorial and international advisory boards are also members of the [[Central Eurasian Studies Society]].<ref>[http://www.cess.muohio.edu/CESSpg_casurvey.html CESS and the ''Central Asian Survey''] Central Eurasian Studies Society</ref> 

==References==
{{reflist}}

[[Category:Central Asian studies]]
[[Category:Asian studies journals]]
[[Category:Asian history journals]]
[[Category:Publications established in 1982]]
[[Category:Routledge academic journals]]


{{area-journal-stub}}