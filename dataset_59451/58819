{{Infobox journal
| title         =  Mammalian Genome
| cover         = 
| editor        = {{Plainlist|
* Joseph H. Nadeau
* [[Stephen D. M. Brown]]}}
| discipline    = [[Genetics]]
| peer-reviewed = 
| language      = [[English language|English]]
| abbreviation  = Mamm. Genome
| publisher     = [[Springer-Verlag]]
| country       = [[United States]]
| frequency     = 12/year
| history       = 1991–present (''Mouse Genom]'' merged with ''Mamm. Genome'' in 1998)
| openaccess    = 
| license       = 
| impact        = 3.068
| impact-year   = 2014
| website       = http://www.springerlink.com/content/0938-8990
| link1         = 
| link1-name    = 
| link2         = 
| link2-name    = 
| RSS           = 
| atom          = 
| JSTOR         = 
| OCLC          = 39981254
| LCCN          = 
| CODEN         = 
| ISSN          = 0938-8990 
| eISSN         = 1432-1777
| boxwidth      = 
}}

'''''Mammalian Genome''''' is a [[peer-review]]ed [[scientific journal|journal]] that publishes research and review articles in the fields of [[genetics]] and [[genomics]] in mouse, human and related organisms.<ref name=description>{{cite web |url=http://www.springer.com/life+sci/cell+biology/journal/335 |title=Mammalian Genome: Description |accessdate=30 July 2009|publisher=Springer }}</ref><ref>{{cite web |url=http://www.springer.com/life+sci/cell+biology/journal/335?detailsPage=contentItemPage&CIPageCounter=96817 |title=Mammalian Genome: Instructions for Authors |accessdate=30 July 2009|publisher=Springer }}</ref> As of July 2009 its editors-in-chief are Joseph H. Nadeau and [[Stephen D. M. Brown]].<ref>{{cite web |url=http://www.springer.com/life+sci/cell+biology/journal/335?detailsPage=editorialBoard|title=Mammalian Genome: Editorial Board |accessdate=30 July 2009|publisher=Springer }}</ref> ''Mammalian Genome'' has been published by [[Springer (publisher)|Springer]] since the journal was launched in 1991, and is the official journal of the [[International Mammalian Genome Society]].<ref>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/1091977 |title=Mammalian Genome  |accessdate=30 July 2009 |publisher=Entrez NLM Catalog }}</ref> In 1998 the journal ''Mouse Genome'' was merged into ''Mammalian Genome''.<ref>{{cite journal |vauthors=Silver LM, Nadeau JH, Brown SD, Eppig JT, Peters J |title=Mammalian Genome, Incorporating Mouse Genome |journal=Mamm Genome |volume=9 |issue=1 |pages=1 |date=January 1998 |pmid=9435276 |url=http://www.springerlink.com/content/5pfqnn3mf0qplcf7/fulltext.pdf?page=1 |doi=10.1007/s003359900669}}</ref> Authors are allowed to [[Self-archiving|self-archive]],<ref>{{cite web |url=http://www.springer.com/life+sci/cell+biology/journal/335?detailsPage=copyrightInformation|title=Mammalian Genome: Copyright Information|accessdate=30 July 2009|publisher=Springer }}</ref> and can pay extra for [[Open access (publishing)|open access]] for an article.<ref>{{cite web |url=http://www.springer.com/open+access/open+choice?SGWID=0-40359-12-161193-0|title=Springer Open Choice License|accessdate=30 July 2009|publisher=Springer }}</ref>

==See also==
*[[Mammal]]
*[[Genome]]

==References==
{{Reflist|2}}

==External links==
*[http://www.springerlink.com/content/0938-8990 ''Mammalian Genome'' website]

[[Category:Genetics journals]]