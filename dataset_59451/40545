{{Orphan|date=July 2016}}

[[Image:Hypoxia protocol.png|right|thumb|upright=2|Example of a typical intermittent hypoxia protocol.]]

'''Intermittent hypoxia''' (also known as '''episodic hypoxia''') is an intervention in which a person or animal undergoes alternating periods of normoxia and [[hypoxia (medical)|hypoxia]]. Normoxia is defined as exposure to [[oxygen]] levels normally found in [[Atmosphere of Earth|earth's atmosphere]] (~21% O<sub>2</sub>) and hypoxia as any oxygen levels lower than those of normoxia. Normally, exposure to hypoxia is negatively associated to physiological changes to the body, such as [[altitude sickness]].<ref name="Altitude"/> However, when used in moderation, intermittent hypoxia may be used clinically as a means to alleviate various pathological conditions.<ref name="Navarette"/>

==General Mechanisms==
[[File:Gray's Anatomy with markup showing carotid artery bifurcation.png|right|thumb|240px|upright=1.2|The bifurcation of the [[carotid artery]] where the [[carotid body]] would be located.]]

When used as a rehabilitative intervention, particularly for [[Breathing|respiration]] and [[walking]], intermittent hypoxia typically works by using long-term facilitation (LTF). LTF, which is synonymous to [[long-term potentiation]], occurs when there are long-term increases in [[synapse|synaptic]] strength due to [[synaptic plasticity]].<ref name="Kandel"/> In the case of intermittent hypoxia, these increases in synaptic strength result in increased motor output.<ref name="Hayashi"/><ref name="Fuller2000"/>

Reduced [[blood gas tension|partial pressures of oxygen]] in the arteries due to intermittent hypoxia are sensed by and stimulate the [[carotid body]], a [[chemoreceptor|chemoafferent receptor]].<ref name="Millhorn"/><ref name="Fuller2001"/> The activated carotid body triggers the release of [[serotonin]] that attach to [[5-HT receptor|serotonin receptors]] on the surface of [[motor neuron|motoneurons]], such as the [[phrenic nerve|phrenic motoneuron]] in the case of respiratory recovery.<ref name="Fuller2000"/> This [[signal transduction]] pathway then uses downstream molecules such as [[TrkB]],<ref name="Baker-Herman"/> [[BDNF]],<ref name="Baker-Herman"/> and [[protein kinase A|PKA]]<ref name="Hoffman"/> to increase the synaptic output of the involved motor neuron which in turn increases the motor output of the involved muscles and, thus, decreases functional impairment. As the amount of intermittent hypoxia changes the amount of serotonin release and, as a result, the amount of LTF, this process exhibits [[metaplasticity]].<ref name="Wilkerson"/> Metaplasticity occurs when the LTF is itself plastic or variable.

Intermittent hypoxia-induced LTF has also been demonstrated in carotid denervated rats, suggesting that synaptic plasticity due to intermittent hypoxia also works through other mechanisms outside of carotid chemoafferents.<ref name="Sibigtroth"/>

Aside from this, intermittent hypoxia also alters overall [[nitric oxide]] production, concentration, and gene expression, which occurs due to cardiovascular adaptations to hypoxia.<ref name="Manukhina"/> This mechanism is relevant when used as a means to decrease [[hypertension]]<ref name="Lyamina"/> or increase [[bone mineral density]]<ref name="Guner"/>

==Dosage==
{| class="wikitable"  style="float:right; text-align: center;"
|+ Types of IH dosage<ref name="Navarette"/>
! style="text-align:center;background-color:lightblue;" | Type
! style="text-align:center;background-color:lightblue;" | Example
 |-
|Hypoxia severity
|''Fi''O<sub>2</sub> of 0.10
 |-
|Episodic duration
|1 minute per episode
 |-
|Episodes per day
|10 episodes/day
 |-
|Pattern of presentation
|Every other day
 |-
|Cumulative exposure duration
|24 cumulative hours
 |}

An understanding of proper [[Quantity|dosage]] is needed in order to design an effective intermittent hypoxia protocol, particularly due to the [[comorbidities]] associated with hypoxia. For example, intermittent hypoxia has been shown to induce LTF in rats while continuous hypoxia does not.<ref name="Baker"/> And acute IH shows no evidence of the [[hippocampus|hippocampal]] cell death found in rats while chronic intermittent hypoxia exposure does<ref name="Lovett"/>

Though intermittent hypoxia has been used for various therapeutic applications across a number of physiological system, there is a general consensus in what can be considered a safe and beneficial amount of intermittent hypoxia. Such a protocol would involve a [[fraction of inspired oxygen]] (''Fi''O<sub>2</sub>) ranging between 0.09 – 0.16 with 3 – 15 episodes per day with comorbidities found in the range of a ''Fi''O<sub>2</sub> of 0.03 – 0.08  and 48 – 2400 episodes per day.<ref name="Navarette"/>

==Pathological and beneficial effects==
{| style="border: 1px solid black; border-spacing: 0; margin: 1em auto; text-align:center;"
!style="border: 1px solid black; padding: 5px;;background-color:#ffc0c0;"|Pathological effects<ref name="Navarette"/>
!style="border: 1px solid black; padding: 5px;;background-color:lightblue;"|Beneficial effects<ref name="Navarette"/>
|-
| style="border: 1px solid black; padding: 5px;" | Systemic [[hypertension]]
| style="border: 1px solid black; padding: 5px;" | Decrease arterial hypertension
|-
| style="border: 1px solid black; padding: 5px;" | [[Obesity]]
| style="border: 1px solid black; padding: 5px;" | Weight loss
|-
| style="border: 1px solid black; padding: 5px;" | [[Insulin resistance]]
| style="border: 1px solid black; padding: 5px;" | Increase [[glucose tolerance]]
|-
| style="border: 1px solid black; padding: 5px;" | Increase [[sympathetic nervous system|sympathetic]] activation
| style="border: 1px solid black; padding: 5px;" | Strengthen [[immune system|immune]] response
|-
| style="border: 1px solid black; padding: 5px;" | [[cognition|Cognitive]] deficits
| style="border: 1px solid black; padding: 5px;" | Enhance [[spatial memory|spatial learning and memory]]
|-
| style="border: 1px solid black; padding: 5px;" | [[Inflammation]]
| style="border: 1px solid black; padding: 5px;" | Decrease inflammation
|}

==Therapeutic applications==
[[File:Copd_versus_healthy_lung.jpg|right|thumb|COPD vs. healthy lung.]]

Though intermittent hypoxia is initially involved with only the [[respiratory system]], its downstream effects allow it to also be used as an effective rehabilitative intervention in a number of different [[biological system]]s in both animals and humans.

===LTF===
For the respiratory system, the LTF facilitated by intermittent hypoxia aids in increasing [[phrenic nerve|phrenic motor nerve]] output. This has been shown to help people with [[obstructive sleep apnea]]<ref name="Gerst"/> and [[chronic obstructive pulmonary disease|COPD]].<ref name="Haider"/> The ability to increase muscle activity, specifically for [[walking]], has also been demonstrated in both rats<ref name="Lovett"/> and humans<ref name="Hayes"/> after [[spinal cord injury]].

===Hippocampal neurogenesis===
[[hippocampus|Hippocampal]] [[adult neurogenesis|neurogenesis]] has also been demonstrated in rats subjected to intermittent hypoxia. This neurogenesis has shown related [[cognition|cognitive]] improvements such as enhanced [[learning]] and [[memory]]<ref name="Lu"/><ref name="Zhang"/> as well as overall increases in spatial cognitive ability.<ref name="Shao"/> Additionally, [[antidepressant]]-like effects are exhibited in rats undergoing such treatment.<ref name="Zhu"/>

===Nitric oxide production===
Nitric oxide level changes due to intermittent hypoxia also provide potential benefits. People with [[hypertension]] have shown decreases in blood pressure.<ref name="Lyamina"/><ref name="Shatillo"/> Increases in [[bone mineral density]] in rats has also been attributed to this process.<ref name="Guner"/> Such changes to nitric oxide levels also aid in protection from [[coronary artery disease|myocardial ischemia]] and [[perfusion]].<ref name="Bolli"/>

==See also==

* [[Hypoxia (medical)]]
* [[Hypoxia (disambiguation)]]
* [[Intermittent hypoxic training]]

==References==
{{reflist|2|refs=

<ref name="Altitude">{{Cite journal|author=FSF Editorial Staff|url=http://flightsafety.org/hf/hf_may-jun97.pdf|title=Wheel-well Stowaways Risk Lethal Levels of Hypoxia and Hypothermia|journal=Human Factors and Aviation Medicine|page=2|volume=44|issue=3|date=May–June 1997|publisher=Flight Safety Foundation}}</ref>

<ref name="Navarette">{{Cite journal|last=Navarette-Opazo|first=A.|last2=Mitchell|first2=G.S.|date=2014|title=Therapeutic potential of intermittent hypoxia: a matter of dose|url=http://ajpregu.physiology.org/content/ajpregu/307/10/R1181.full.pdf|journal=Am J Physiol Regul Integr Comp Physiol|volume=307|issue=10|pages=R1181-1197|doi=10.1152/ajpregu.00208.2014}}</ref>

<ref name="Kandel">{{Cite journal|last=Kandel|first=E.R.|date=2001|title=The molecular biology of memory storage: A dialogue between genes and synapses|url=http://science.sciencemag.org/content/294/5544/1030.full|journal=Science|volume=294|issue=5544|pages=1030–1038|doi=10.1126/science.1067020|pmid=11691980}}</ref>

<ref name="Hayashi">{{Cite journal|last=Hayashi|first=F.|last2=Coles|first2=S.K.|last3=Bach|first3=K.B.| last4=Mitchell|first4=G.S.|last5=McCrimmon|first5=D.R.|date=1993|title=Time-dependent phrenic nerve responses to carotid afferent activation: intact vs. decerebellate rats|url=http://ajpregu.physiology.org/content/265/4/R811|journal=Am J Physiol Regul Integr Comp Physiol|volume=265|issue=4|pages=R811-819|pmid=8238451}}</ref>

<ref name="Fuller2000">{{Cite journal|last=Fuller|first=D.D.| last2=Bach|first2=K.B.|last3=Baker|first3=T.L.| last4=Kinkead|first4=R.|last5=Mitchell|first5=G.S.|date=2000|title=Long term facilitation of phrenic motor output|url=http://www.sciencedirect.com/science/article/pii/S0034568700001249|journal=Respir Physiol|volume=121|issue=2–3|pages=135–146|doi=10.1016/S0034-5687(00)00124-9}}</ref>

<ref name="Millhorn">{{Cite journal|last=Millhorn|first=D.E.| last2=Eldridge|first2=F.L.|last3=Waldrop|first3=T.G.|date=1980|title=Prolonged stimulation of respiration by a new central neural mechanism|url=http://www.sciencedirect.com/science/article/pii/0034568780900250|journal=Respir Physiol|volume=41|issue=1|pages=87–103|doi=10.1016/0034-5687(80)90025-0}}</ref>

<ref name="Fuller2001">{{Cite journal|last=Fuller|first=D.D.|last2=Zabka|first2=A.G.|last3=Baker|first3=T.L.|last4=Mitchell|first4=G.S.|date=2001|title=Phrenic long-term facilitation requires 5-HT receptor activation during but not following episodic hypoxia|url=http://jap.physiology.org/content/90/5/2001.long|journal=J App Physiol (1985)|volume=90|issue=5|pages=2001–2006|pmid=11299296}}</ref>

<ref name="Baker-Herman">{{Cite journal|last=Baker-Herman|first=T.L.|last2=Fuller|first2=D.D.|last3=Bavis|first3=R.W.|last4=Zabka|first4=A.G.|last5=Golder|first5=F.J.|last6=Doperalski|first6=N.J.|last7=Johnson|first7=R.A.|last8=Watters|first8=J.J.|last9=Mitchell|first9=G.S.|date=2004|title=BDNF is necessary and sufficient for spinal respiratory plasticity following intermittent hypoxia|url=http://www.nature.com/neuro/journal/v7/n1/abs/nn1166.html|journal=Nat Neurosci|volume=7|issue=1|pages=48–55|doi=10.1038/nn1166}}</ref>

<ref name="Hoffman">{{Cite journal|last=Hoffman|first=M.S.|last2=Mitchell|first2=G.S.|date=2011|title=Spinal 5-HT7 receptor activation induces long-lastingphrenic motor facilitation|url=http://onlinelibrary.wiley.com/doi/10.1113/jphysiol.2010.201657/full|journal=J Physiol|volume=589|issue=6|pages=1397–1407|doi=10.1113/jphysiol.2010.201657}}</ref>

<ref name="Wilkerson">{{Cite journal|last=Wilkerson|first=J.E.|last2=Mitchell|first2=G.S.|date=2009|title=Daily intermittent hypoxia augments spinal BDNF levels, ERK phosphorylation and respiratory long-term facilitation|journal=Exp Neurol|volume=217|issue=1|pages=116–123|doi=10.1016/j.expneurol.2009.01.017|pmid=19416672|pmc=2691872}}</ref>

<ref name="Baker">{{Cite journal|last=Baker|first=T.L.|last2=Mitchell|first2=G.S.|date=2000|title=Episodic but not continuous hypoxia elicits long-term facilitation of phrenic motor output in rats|journal=J Physiol|volume=529|issue=1|pages=215–219|doi=10.1111/j.1469-7793.2000.00215.x|pmid=11080263|pmc=2270180}}</ref>

<ref name="Lovett">{{Cite journal|last=Lovett-Barr|first=M.R.|last2=Satriotomo|first2=I.|last3=Muir|first3=G.D.|last4=Wilkerson|first4=J.E.|last5=Hoffman|first5=M.S.|last6=Vinit|first6=S.|last7=Mitchell|first7=G.S.|date=2012|title=Repetitive intermittent hypoxia induces respiratory and somatic motor recovery after chronic cervical spinal injury|url=http://jap.physiology.org/cgi/pmidlookup?view=long&pmid=20724571|journal=J Neurosci|volume=32|issue=11|pages=3591–3600|doi=10.1523/JNEUROSCI.2908-11.2012|pmid=22423083|pmc=3349282}}</ref>

<ref name="Gerst">{{Cite journal|last=Gerst|first=D.G.|last2=3rd Yokohana|first2=S.S.|last3=Carney|first3=L.M.|last4=Lee|first4=D.S.|last5=Badr|first5=M.S.|last6=Qureshi|first6=T.|last7=Anthouard|first7=M.N.|last8=Mateika|first8=J.H.|date=2011|title=The hypoxic ventilatory response and ventilatory long-term facilitation are altered by time of day and repeated daily exposure to intermittent hypoxia|journal=J App Physiol (1985)|volume=110|issue=1|pages=15–28|doi=10.1152/japplphysiol.00524.2010|pmid=20724571|pmc=3785116}}</ref>

<ref name="Haider">{{Cite journal|last=Haider|first=T.|last2=Casucci|first2=G.|last3=Linser|first3=T.|last4=Faulhaber|first4=M.|last5=Gatterer|first5=H.|last6=Ott|first6=G.|last7=Linser|first7=A.|last8=Ehrenbourg|first8=I.|last9=Tkatchouk|first9=E.|last10=Burtscher|first10=M.|last11=Bernardi|first11=L.|date=2009|title=Interval hypoxic training improves autonomic cardiovascular and respiratory control in patients with mild chronic obstructive pulmonary disease|url=http://journals.lww.com/jhypertension/Abstract/2009/08000/Interval_hypoxic_training_improves_autonomic.21.aspx|journal=J Hypertens|volume=27|issue=8|pages=1648–1654|doi=10.1097/HJH.0b013e32832c0018}}</ref>

<ref name="Hayes">{{Cite journal|last=Hayes|first=H.B.|last2=Jayataman|first2=A.|last3=Herrmann|first3=M.|last4=Mitchell|first4=G.S.|last5=Rymer|first5=W.Z.|last6=Trumbower|first6=R.D.|date=2014|title=Daily intermittent hypoxia enhances walking after chronic spinal cord injury: A randomized trial|url=http://www.neurology.org/cgi/pmidlookup?view=long&pmid=24285617|journal=Neurology|volume=82|issue=2|pages=104–113|doi=10.1212/01.WNL.0000437416.34298.43|pmid=24285617|pmc=3897437}}</ref>

<ref name="Lyamina">{{Cite journal|last=Lyamina|first=N.P.|last2=Lyamina|first2=S.V.|last3=Senchiknin|first3=V.N.|last4=Mallet|first4=R.T.|last5=Downey|first5=H.F.|last6=Manukhina|first6=E.B.|date=2011|title=Normobaric hypoxia conditioning reduces blood pressure and normalizes nitric oxide synthesis in patients with arterial hypertension|url=http://journals.lww.com/jhypertension/Abstract/2011/11000/Normobaric_hypoxia_conditioning_reduces_blood.27.aspx|journal=J Hypertens|volume=29|issue=11|pages=2265–2272|doi=10.1097/HJH.0b013e32834b5846}}</ref>

<ref name="Guner">{{Cite journal|last=Guner|first=I.|last2=Uzun|first2=D.D.|last3=Yaman|first3=M.O.|last4=Genc|first4=H.|last5=Gelisgen|first5=R.|last6=Korkmaz|first6=G.G.|last7=Hallac|first7=M.|last8=Yelman|first8=N.|last9=Sahin|first9=G.|last10=Karter|first10=Y.|last11=Simsek|first11=G.|date=2013|title=The effect of chronic long-term intermittent hypobaric hypoxia on bone mineral density in rats: role of nitric  oxide|url=http://link.springer.com/article/10.1007/s12011-013-9722-8|journal=Biol Trace Elem Res|volume=154|issue=2|pages=262–267|doi=10.1007/s12011-013-9722-8}}</ref>

<ref name="Sibigtroth">{{Cite journal|last=Sibigtroth|first=C.M.|last2=Mitchell|first2=G.S.|date=2011|title=Carotid chemoafferent activity is not necessary for all phrenic long-term facilitation following acute intermittent hypoxia|journal=Respir Physiol Neurobiol|volume=176|issue=3|pages=73–79|doi=10.1016/j.resp.2010.11.006|pmid=21093615|pmc=4374991}}</ref>

<ref name="Manukhina">{{Cite journal|last=Manukhina|first=D.B.|last2=Downey|first2=H.F.|last3=Mallet|first3=R.T.|date=2006|title=Role of nitric oxide in cardiovascular adaptation to intermittent hypoxia|url=http://ebm.sagepub.com/content/231/4/343.full/|journal=Exp Biol Med|volume=231|issue=4|pages=343–365|pmid=16565431|doi=10.1007/0-387-29540-2_6}}</ref>

<ref name="Lu">{{Cite journal|last=Lu|first=X.J.|last2=Chen|first2=X.Q.|last3=Weng|first3=J.|last4=Zhang|first4=H.Y.|last5=Pak|first5=D.T.|last6=Luo|first6=J.H.|last7=Du|first7=J.Z.|date=2009|title=Hippocampal spine-associated Rap-specific GTPase-activating protein induces enhancement of learning and memory in postnatally hypoxia-exposed mice|url=http://www.sciencedirect.com/science/article/pii/S0306452209007854/|journal=Neuroscience|volume=162|issue=2|pages=404–414|doi=10.1016/j.neuroscience.2009.05.011|pmid=19442707|pmc=3243647}}</ref>

<ref name="Shao">{{Cite journal|last=Shao|first=G.|last2=Zhang|first2=R.|last3=Wang|first3=Z.L.|last4=Gao|first4=C.Y.|last5=Huo|first5=X.|last6=Lu|first6=G.W.|date=2006|title=Hippocampal spine-associated Rap-specific GTPase-activating protein induces enhancement of learning and memory in postnatally hypoxia-exposed mice|url=http://www.karger.com/Article/FullText/121368|journal=Neuro-Signals|volume=15|issue=6|pages=314–321|doi=10.1159/000121368}}</ref>

<ref name="Zhang">{{Cite journal|last=Zhang|first=J.X.|last2=Chen|first2=X.Q.|last3=Du|first3=J.Z.|last4=Chen|first4=Q.M.|last5=Zhu|first5=C.Y.|date=2005|title=Neonatal exposure to intermittent hypoxia enhances mice performance in water maze and 8-arm radial maze tasks|url=http://onlinelibrary.wiley.com/doi/10.1002/neu.20174/abstract|journal=J Neurobiol|volume=65|issue=1|pages=72–84|doi=10.1002/neu.20174}}</ref>

<ref name="Zhu">{{Cite journal|last=Zhu|first=X.H.|last2=Yan|first2=H.C.|last3=Zhang|first3=J.|last4=Qu|first4=H.D.|last5=Qiu|first5=X.S.|last6=Chen|first6=L.|last7=Li|first7=S.J.|last8=Cao|first8=X.|last9=Bean|first9=J.C.|last10=Chen|first10=L.H.|last11=Qin|first11=X.H.|last12=Liu|first12=J.H.|last13=Bai|first13=X.C.|last14=Mei|first14=L.|last15=Gao|first15=T.M.|date=2010|title=Intermittent hypoxia promotes hippocampal neurogenesis and produces antidepressant-like effects in adult  rats|url=http://www.jneurosci.org/cgi/pmidlookup?view=long&pmid=20861371|journal=J Neurosci|volume=30|issue=38|pages=12653–12663|doi=10.1523/JNEUROSCI.6414-09.2010|pmid=20861371}}</ref>

<ref name="Shatillo">{{Cite journal|last=Shatillo|first=V.B.|last2=Korkushko|first2=O.V.|last3=Ischuk|first3=V.A.|last4=Downey|first4=H.F.|last5=Serebrovskaya|first5=T.V.|date=2008|title=Effects of intermittent hypoxia training on exercise performance, hemodynamics, and ventilation in healthy senior men|url=http://online.liebertpub.com/doi/abs/10.1089/ham.2007.1053|journal=High Alt Med Biol|volume=9|issue=1|pages=43–52|doi=10.1089/ham.2008.1053}}</ref>

<ref name="Bolli">{{Cite journal|last=Bolli|first=R.|date=2001|title=Cardioprotective function of inducible nitric oxide synthase and role of nitric oxide in myocardial ischemia and preconditioning: an overview of a decade of research|url=http://www.sciencedirect.com/science/article/pii/S0022282801914622|journal=J Mol Cell Cardiol|volume=33|issue=11|pages=1897–1918|doi=10.1006/jmcc.2001.1462}}</ref>

}}

[[Category:Medical treatments]]