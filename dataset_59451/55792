{{Infobox Journal
| cover        = [[Image:Australian Journal of Chemistry cover.jpg]]
| discipline   = [[Chemistry]]
| abbreviation = Aust. J. Chem.
| website      = http://www.publish.csiro.au/nid/51.htm
| publisher    = [[CSIRO Publishing]]
| editor = Alison Green
| country      = [[Australia]]
| history      = 1948–present  
| impact       = 1.558
| impact-year  = 2014
| ISSN         = 0004-9425
| CODEN        = AJCHAS
}}

The '''''Australian Journal of Chemistry''''', an international, multidisciplinary journal for the [[chemical sciences]], is a [[peer-reviewed]] [[scientific journal]] publishing original, primary research and review articles on all aspects of chemistry. The Journal includes inter alia synthesis, structure, new materials, [[macromolecules]] and [[polymers]], [[supramolecular chemistry]], [[analytical chemistry|analytical]], natural products, biological and medicinal chemistry, [[nanotechnology]], and [[surface chemistry]]. 

The ''Australian Journal of Chemistry'' is published monthly by [[CSIRO Publishing]], an independent business unit within the [[Commonwealth Scientific and Industrial Research Organisation]] (CSIRO). The journal is supported by the Australian Academy of Science and is affiliated with the [[Royal Australian Chemical Institute]].

The [[editor-in-chief]] is Professor Curt Wentrup of The University of Queensland. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.558.<ref>[[Journal Citation Reports|Journal Citation Reports, 2015]]</ref>

==See also==
* [[Environmental Chemistry (journal)|Environmental Chemistry]](journal)
* [[List of scientific journals in chemistry]]
* [[List of scientific journals]]

== References ==
<references/>

==External links==
* [http://www.publish.csiro.au/nid/51.htm Journal Home]
* [http://www.publish.csiro.au/nid/51/aid/3494.htm Editorial Team]

[[Category:Chemistry journals]]
[[Category:CSIRO Publishing academic journals]]