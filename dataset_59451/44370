{{Infobox airport
| name         = Syracuse Suburban Airport
| image        = Syracuse Suburban Airport 1995.jpg
| caption      = [[USGS]] aerial image, 1995
| FAA          = {{strikethrough|6NK}}
| type         = Public
| owner        = Syracuse Suburban Airport LLC
| operator     = 
| city-served  = [[Syracuse, New York]]
| location     = [[Central Square, New York]]
| elevation-f  = 400
| elevation-m  = 122
| coordinates  = {{coord|43|16|02|N|076|10|46|W|region:US-NY_type:airport_scale:10000}}
| website      = 
| r1-number    = 16/34
| r1-length-f  = 2,500
| r1-length-m  = 762
| r1-surface   = Asphalt
| footnotes    = Source: [[Federal Aviation Administration]]<ref name="FAA">{{FAA-airport|ID=6NK|use=PU|own=PR|site=16267.3*A}}. Federal Aviation Administration. Effective 29 July 2010.</ref>
}}

'''Syracuse Suburban Airport''' {{airport codes|||{{strikethrough|6NK}}}} is a former privately owned public-use [[airport]] in [[Oswego County, New York|Oswego County]], [[New York (state)|New York]], [[United States]]. It is located 13 [[nautical mile]]s (24&nbsp;km) north of the [[central business district]] of the city of [[Syracuse, New York|Syracuse]].<ref name=FAA /> The airport is off Gildner Road in the village of [[Central Square, New York|Central Square]], which is located in the [[Hastings, New York|Town of Hastings]].<ref name="Ellis-2008-01-30">{{cite news |url=http://www.syracuse.com/poststandard/stories/index.ssf?/base/news-10/1201702699173101.xml&coll=1 |title=Turbulence in Hastings - Town's decision: Ground airport or let it fly |first=Suzanne M. |last=Ellis |publisher=''The Post-Standard'' |date=2008-01-03}}</ref> The airport redevelopment was not completed and one of the owners plead guilty to bank fraud in July 2014.<ref>{{cite news|last= Tobin |first= Dave |url= http://www.syracuse.com/news/index.ssf/2014/07/hastings_airport_owner_pleads_guilty_to_conspiracy_to_commit_bank_fraud.html |title= Owner of failed airstrip that received $2.9 million in public money admits bank fraud |publisher= syracuse.com |date= 24 July 2014 |access-date= 26 July 2016}}</ref>

According to the FAA's [[National Plan of Integrated Airport Systems]] for 2009–2013, the facility is [[FAA airport categories|categorized]] as a ''[[reliever airport]]''<ref>[http://www.faa.gov/airports/planning_capacity/npias/ National Plan of Integrated Airport Systems] for 2009–2013: [http://www.faa.gov/airports/planning_capacity/npias/reports/media/2009/npias_2009_appA_part4.pdf Appendix A: Part 4 (PDF, 1.61 MB)]. Federal Aviation Administration. Updated 15 October 2008.</ref> for the [[Syracuse Hancock International Airport]].<ref name="FAA" />

== History ==
The airfield dates back to around 1960, and it was in operation until the mid-1980s. Its original name was '''Central Square Airport'''.<ref>[http://www.airfields-freeman.com/NY/Airfields_NY_Centr.htm#syracusesuburban Central Square Airport / Syracuse Suburban Airport] at [[Abandoned & Little-Known Airfields]]</ref> In the 1970s the town board changed the zoning to residential but the airport was exempted as long as it remained open.<ref name="Ellis-2008-01-30" />

Syracuse Suburban Airport LLC purchased the 93 [[acre]] airport site for $350,000 on July 2, 2004.<ref name="Ellis-2008-01-30" /> That same year it was awarded a $300,000 grant from [[New York State]] to rehabilitate its runway and conduct a master plan study that would be required to complete additional projects.<ref>[http://www.senate.gov/~schumer/SchumerWebsite/pressroom/press_releases/2004/PR02846.SYRsuburbDOT083104.html Syracuse Suburban Airport To Receive $300,000], Press Release from New York State Senator [[Charles Schumer]], 2004-08-24</ref>  In 2005 the company spent $414,000 to purchase three additional properties totaling {{convert|91|acre|m2}}.<ref name="Ellis-2008-01-30" />

In January 2008, the Hastings Town Board denied a zoning change that would have allowed Syracuse Suburban Airport to reopen. The town contends the [[grandfather clause|grandfathered]] use as an airport expired because the previous owner sold the property in 2004 and it hasn't been used as an airport since that time. However, the current owners have a pending [[lawsuit]] against the Hastings Zoning Board of Appeals, which will be heard in the [[New York State Supreme Court]].<ref>{{cite news | url = http://blog.syracuse.com/news/2008/01/hastings_denies_zoning_change.html | title = Hastings denies zoning change to reopen airport | publisher=''The Post-Standard'' |author = Ellis, Suzanne | date = 31 January 2008}}</ref>

One of the airport's owners also owned [[Michael Airfield]] in nearby [[Cicero, New York]], which has been closed. That airport's federally funded status was transferred to Syracuse Suburban Airport, where a new runway was under construction in 2009.<ref>{{cite news | url = http://www.syracuse.com/news/index.ssf/2009/09/two_central_new_york_businessm.html | title = Airstrip to Nowhere: FAA plans to drop $11 million into an Oswego County field | publisher=''The Post-Standard'' |author = Tobin, Dave | date = 29 December 2009}}</ref> The new runway was completed in 2010.<ref>{{Cite document| url = http://www.9wsyr.com/news/local/story/Millions-of-federal-tax-dollars-spent-on-reliever/-PbXiP0biEmf4nBlTOc2Dw.cspx | title = Millions of federal tax dollars spent on 'reliever' airfields | publisher = NewsChannel 9 WSYR | date = 15 June 2010}}</ref>

== Facilities ==
Syracuse Suburban Airport covers an area of {{convert|94|acre|ha|lk=on}} at an [[elevation]] of 400 feet (122 m) above [[mean sea level]]. It has one runway designated 16/34 which measures 2,500 by 60 feet (762 x 18 m).<ref name="FAA" />

Previous FAA records listed a runway 15/33 with a 2,660 by 43 foot (811 x 13 m) [[asphalt]] surface. Although still listed as operational by the FAA, aerial photographs showed the runway marked with X's<ref>[http://maps.live.com/default.aspx?v=2&FORM=LMLTCP&cp=r69g908njw5j&style=b&lvl=1&tilt=-90&dir=0&alt=-1000&scene=9447285&phx=0&phy=0&phscl=1&encType=1 Aerial photo of runway] from [[Live Search Maps]]</ref> which indicated that it was closed.

== References ==
{{Reflist}}

== External links ==
* {{US-airport-minor|6NK}}

[[Category:Airports in New York]]
[[Category:Transportation in Syracuse, New York]]