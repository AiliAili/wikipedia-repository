{{multiple issues|
{{Unreferenced|date=April 2012}}
{{refimprove|date=December 2011}}
}}

{{infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = The Fur Country
| title_orig    = Le Pays des fourrures
| translator    = N. d’Anvers
| image         = Fur Country (title).jpg
| image_size = 200px
| caption = Title page of 1st illustrated French edition
| author        = [[Jules Verne]]
| illustrator   = [[Jules Férat]] and <br>[[Alfred Quesnay de Beaurépaire]]
| cover_artist  =
| country       = France
| language      = French
| series        = [[Voyages Extraordinaires|The Extraordinary Voyages]] #10
| genre         = [[Adventure novel]]
| publisher     = [[Pierre-Jules Hetzel]]
| release_date  = 1873
| english_pub_date = 1873
| media_type    = Print ([[Hardcover|Hardback]])
| pages         =
| preceded_by   = [[The Adventures of Three Englishmen and Three Russians in South Africa]]
| followed_by   = [[Around the World in Eighty Days (book)|Around the World in Eighty Days]]
}}
'''''The Fur Country''''' ({{lang-fr|Le Pays des fourrures}}) is an [[adventure novel]] by [[Jules Verne]] in [[Voyages Extraordinaires|The Extraordinary Voyages]] series, first published in 1873. The novel was serialized in ''Magasin d’Éducation et de Récréation'' from September 1872 to December 1873. The two-volume first original French edition and the first illustrated large-format edition were published in 1873. The first English translation by N. D’Anvers (pseudonym of Mrs. Arthur (Nancy) Bell){{Citation needed|date=April 2012|reason=Please see the talk page}} was also published in 1873{{Citation needed|date=April 2012|reason=Please see the talk page}}.

==Plot summary==
In 1859 Lt. Jasper Hobson and other members of the [[Hudson's Bay Company]] travel through the [[Northwest Territories]] of Canada to [[Cape Bathurst]] on the [[Arctic Ocean]] on the mission to create a fort at [[70th parallel north|70 degrees]], north of the [[Arctic Circle]]. The area they come to is very rich with wildlife and natural resources. Jasper Hobson and his party establish a fort here. At some point, an earthquake occurs, and from then on, laws of physics seem altered (a total eclipse happens to be only partial; tides are not perceived anymore). They eventually realise that they are on an iceberg separated from the sea ice that is drifting south. Hobson does a daily measurement to know the iceberg's location. The iceberg passes the [[Bering Strait]] and the iceberg (which is now much smaller, since the warmer waters has melted some parts) finally reaches a small island. A Danish whaling ship finds them. Every member in Hobson's party is rescued and they all survive.

==Publication history==
*1873, UK, London: Sampson Low, Pub date November 1873; first UK edition, translated by N. D’Anvers (Mrs. Arthur (Nancy) Bell), as ''The Fur Country or Seventy Degrees North Latitude''
*1874, USA, Boston: James Osgood, Pub date 1874; first USA edition
*1879, UK, Routledge, Pub date 1879; translation by Henry Frith
*1966, UK, London: Arco, Pub date 1966; abridged and edited by I.O. Evans in 2 volumes as ''The Sun in Eclipse'' and ''Through the Behring Strait''
*1987, Canada, Toronto: NC Press ISBN 0-920053-82-3, Pub date October 1987; new translation by Edward Baxter
*2008, UK, Classic Comic Store Ltd, Classics Illustrated (JES) #41 facsimile edition (JES13027), retitled "The Floating Island"

==References==
{{Reflist}}

==External links==
{{commons category}}
{{wikisourcelang|fr|Le Pays des fourrures}}
{{Gutenberg|no=8991|name=The Fur Country}}
{{Gutenberg|no=17796|name=Le Pays des Fourrures}} {{fr}}
* {{librivox book | title=The Fur Country | author=Jules Verne}}
* {{librivox book | title=Le Pays des Fourrures|author=jules verne}} {{fr}}
*[http://jv.gilead.org.il/rpaul/Le%20pays%20des%20fourrures/ 105 illustrations for ''The Fur Country''] by Jules Férat and Alfred Quesnay de Beaurépaire

{{Verne}}

{{Authority control}}

{{DEFAULTSORT:Fur Country, The}}
[[Category:1873 novels]]
[[Category:Novels by Jules Verne]]
[[Category:Novels set in Canada]]
[[Category:Novels set in the Arctic]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in French magazines]]


{{1870s-novel-stub}}