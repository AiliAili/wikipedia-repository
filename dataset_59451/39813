{{Infobox astronaut
|name          = L. Gordon Cooper Jr.
|othername     = Leroy Gordon Cooper Jr.
|image         = Gordon_Cooper_2.jpg
|type          = [[NASA]] [[Astronaut]]
|nationality   = United States
|birth_date    = {{birth date|1927|03|06}}
|birth_place   = [[Shawnee, Oklahoma]], U.S.
|death_date    = {{Death date and age|2004|10|04|1927|03|06}}
|death_place   = [[Ventura, California]], U.S.
|occupation    = [[Test pilot]]
|alma_mater    = [[University of Hawaii]]<br>[[University of Maryland]]<br>[[Air Force Institute of Technology|AFIT]], B.S. 1956
|rank          = [[File:US-O6 insignia.svg|25px]] [[Colonel (United States)|Colonel]], [[United States Air Force|USAF]]
|selection     = [[Mercury Seven|1959 NASA Group 1]]
|time          = 9d 09h 14m
|mission       = [[Mercury-Atlas 9]] (Faith 7), [[Gemini 5]]
|insignia      = [[File:Faith 7 insignia.gif|40px]] [[File:Gemini5insignia.png|40px]]
|Date of ret   = July 31, 1970
|awards        = [[File:Dfc-usa.jpg|20px|link=Distinguished Flying Cross (United States)]] [[File:NASA Distinguished Service Medal.jpg|20px|link=NASA Distinguished Service Medal]]
}}

'''Leroy Gordon "Gordo" Cooper Jr.''' (March 6, 1927 – October 4, 2004), ([[Colonel (United States)|Col]], [[United States Air Force|USAF]]), better known as '''Gordon Cooper''', was an American [[aerospace engineer]], [[test pilot]], [[United States Air Force]] [[Aviator|pilot]], and one of the [[Mercury Seven|seven original astronauts]] in [[Project Mercury]], the first manned space program of the United States.

Cooper piloted the longest and [[Mercury-Atlas 9|final Mercury spaceflight]] in 1963. He was the first American to sleep in space during that 34-hour mission and was the last American to be launched alone to conduct an entirely solo [[Orbital spaceflight|orbital]] mission. In 1965, Cooper flew as Command Pilot of [[Gemini 5|Gemini&nbsp;5]].

==Biography==

===Early life and education===
Cooper was born on March 6, 1927, in [[Shawnee, Oklahoma]], to parents Leroy Gordon Cooper Sr. ([[Colonel (United States)|Colonel]], [[United States Air Force|USAF]], Ret.) and Hattie Lee ({{nee|Herd}}) Cooper. He was active in the [[Boy Scouts of America]] where he achieved its second highest rank, [[Life Scout]].<ref>{{cite web|url=http://www.scouting.org/About/FactSheets/scouting_space.aspx |title=Scouting and Space Exploration |work=scouting.org |deadurl=yes |archiveurl=https://web.archive.org/web/20160304032406/http://www.scouting.org/about/factsheets/scouting_space.aspx |archivedate=2016-03-04 |df= }}</ref> Cooper attended Jefferson Elementary School and Shawnee High School in Shawnee, Oklahoma, and was involved in football and track. He moved to Murray, Kentucky, about two months before graduating with his class in 1945 when his father, Leroy Cooper Sr., a World War I veteran, was called back into service. He graduated from [[Murray High School (Kentucky)|Murray High School]] in 1945.

After he learned that the [[United States Army|Army]] and [[United States Navy|Navy]] flying schools were not taking any candidates the year he graduated from high school, he decided to enlist in the [[United States Marine Corps]]. Cooper left for [[Marine Corps Recruit Depot Parris Island|MCRD Parris Island]] as soon as he graduated. However, [[World War II]] had ended before he could get into combat. He was assigned then to the [[Naval Academy Preparatory School]] and was an alternate for an appointment to [[Annapolis]], [[Maryland]]. The man who was the primary appointee made the grade so Cooper was reassigned in the Marines on guard duty in [[Washington, D.C.]] He was serving with the [[Honor guard#United States|Presidential Honor Guard]] in Washington when he was released from duty along with other [[United States Marine Corps Reserve|Marine reservists]].

Following his discharge from the Marine Corps, he went to [[Hawaii]] to live with his parents. His father was assigned to [[Hickam Field]] at the time. He started attending the [[University of Hawaii]], and there he met his first wife, the former Trudy B. Olson of [[Seattle]], [[Washington (state)|Washington]]. She was quite active in flying, the only Mercury wife to have a pilot's license. They were married on August 29, 1947 in [[Honolulu]] when Gordon was 20.  They continued to live there for two more years while he continued his university studies.<ref name=40thmerc7>{{cite web |url=http://history.nasa.gov/40thmerc7/cooper.htm |title=L. Gordon Cooper Jr. |last1=Gray |first1=Tara |work=40th Anniversary of Mercury 7 |publisher=NASA |access-date=July 10, 2015}}</ref>

===Military service===
Cooper transferred his commission to the [[United States Air Force]] in 1949, was placed on active duty and received flight training at [[Perrin Air Force Base]], [[Texas]] and [[Williams Air Force Base|Williams AFB]], [[Arizona]].

Cooper's first flight assignment came in 1950 at [[Ramstein Air Base|Landstuhl Air Base]], [[West Germany]], where he flew [[F-84 Thunderjet]]s and [[F-86 Sabre]]s for four years. He later became [[flight commander]] of the [[525th Fighter Squadron|525th Fighter Bomber Squadron]]. While in Germany, he also attended the European Extension of the [[University of Maryland University College|University of Maryland]]. Returning to the United States in 1954, he studied for two years at the U.S. [[Air Force Institute of Technology]] in [[Ohio]], and in 1956 completed his [[Bachelor of Science]] degree in [[Aerospace Engineering]]. Cooper was then assigned to the [[U.S. Air Force Test Pilot School|USAF Experimental Flight Test School]] (Class 56D) at [[Edwards Air Force Base]] in [[California]], and after graduation was posted to the [[Air Force Test Center|Flight Test Engineering Division]] at Edwards, where he served as a [[test pilot]] and [[project manager]] testing the [[F-102A]] and [[F-106 Delta Dart|F-106B]].<ref name=40thmerc7/> He corrected several deficiencies in the F-106, saving the U.S. Air Force a great deal of money.<ref name=40thmerc7/>

Cooper logged more than 7,000 hours of flight time, with 4,000 hours in [[jet aircraft]]. He flew all types of [[Commercial aviation|commercial]] and [[general aviation]] airplanes and [[helicopter]]s.

===NASA career===

====Project Mercury====
{{Main article|Mercury-Atlas 9}}
{{quote box|align=right|width=33%|'''How about right now?'''|source=Replying to Charles Donlan, associate director of [[Project Mercury]], who welcomed Cooper to the team, on his question when he could leave for [[Langley Research Center|Langley]].<ref name=40thmerc7/>}}

[[File:Mercury Suit Gordon Cooper.jpg|left|thumb|150px|Cooper in his Mercury spacesuit, the [[Navy Mark IV]] ]]

While at Edwards, Cooper was intrigued to read an announcement saying that a contract had been awarded to [[McDonnell Aircraft]] in [[St. Louis, Missouri]], to build a space capsule. Shortly after this he was called to [[Washington, D.C.]], for a [[NASA]] briefing on [[Project Mercury]] and the part astronauts would play in it. Cooper went through the selection process with the other 109 pilots and was not surprised when he was accepted as the youngest of the first seven American astronauts.<ref name="IndyObit">{{Cite news | last = Bond | first = Peter | title = Col Gordon Cooper | newspaper = Independent | location = London | date = 18 November 2004 | url = http://www.independent.co.uk/news/obituaries/col-gordon-cooper-533604.html | accessdate = 3 October 2010}}</ref>

Each of the Mercury astronauts was assigned to a different portion of the project along with other special assignments. Cooper specialized in the [[Mercury-Redstone Launch Vehicle|Redstone rocket]] (and developed a personal survival knife, the Model 17 "Astro" from [[Randall Made Knives]], for astronauts to carry). He also chaired the Emergency Egress Committee, responsible for working out emergency [[launch pad]] procedures for escape. Cooper served as [[capsule communicator]] (CAPCOM) for [[Alan Shepard]]'s first [[sub-orbital spaceflight]] in [[Mercury-Redstone 3]] (''Freedom 7'') and [[Scott Carpenter]]'s flight on [[Mercury-Atlas 7]] (''Aurora 7''). He was backup pilot for [[Wally Schirra]] in [[Mercury-Atlas 8]] (''Sigma 7'').

Cooper was launched into space on May 15, 1963, aboard the [[Mercury-Atlas 9]] (''Faith 7'') spacecraft, the last Mercury mission. He orbited the [[Earth]] 22 times and logged more time in space than all five previous Mercury astronauts combined&mdash;34 hours, 19 minutes and 49 seconds&mdash;traveling 546,167 miles (878,971&nbsp;km) at 17,547&nbsp;mph (28,239&nbsp;km/h), pulling a maximum of 7.6 [[g-force|''g'']] (74.48&nbsp;m/s²). Cooper achieved an altitude of 165.9 statute miles (267&nbsp;km) at [[Apsis|apogee]]. He was the first American astronaut to sleep not only in orbit but on the [[launch pad]] during a countdown.<ref name="IndyObit"/>

{{clear}}

====="Spam in a can"=====
[[File:S63-07856.jpg|thumb|Cooper in an [[SSTV]] broadcast from Faith 7]]
Like all Mercury flights, ''Faith 7'' was designed for fully automatic control, a controversial engineering decision which in many ways reduced the role of an astronaut to that of a passenger, and prompted [[Chuck Yeager]] to describe Mercury astronauts as "Spam in a can".<ref>Wolfe, Tom, ''[[The Right Stuff (book)|The Right Stuff]]'' 1979 ISBN 978-0-312-42756-6</ref>

Toward the end of the ''Faith 7'' flight there were mission-threatening [[Mercury-Atlas 9#Technical problems on the flight|technical problems]]. During the 19th orbit, the capsule had a power failure. Carbon dioxide levels began rising, and the cabin temperature jumped to over 100 degrees [[Fahrenheit]] (38°[[Celsius|C]]). Cooper turned to his understanding of star patterns, took manual control of the tiny capsule and successfully estimated the correct [[Flight dynamics|pitch]] for [[Atmospheric entry|re-entry]] into the atmosphere. Some precision was needed in the calculation, since if the capsule came in too steep, [[g-force]]s would be too large, and if its trajectory were too shallow, it would shoot out of the atmosphere again, back into space. Cooper drew lines on the capsule window to help him check his orientation before firing the re-entry rockets. "So I used my wrist watch for time," he later recalled, "my eyeballs out the window for [[Attitude dynamics and control|attitude]].<!-- "attitude" is correct, meaning orientation of the spacecraft, the word carried in the source is NOT "altitude" --> Then I fired my retrorockets at the right time and landed right by the carrier."<ref name="UFO">space.com, ''[http://www.space.com/news/spaceagencies/gordon_ufos_000728.html Gordon Cooper Touts New Book Leap of Faith]'', 30 July 2000, retrieved 20 January 2008</ref><ref>Wagener, Leon, ''One Giant Leap'', Forge Books, 2004 ISBN 978-0-312-87343-1</ref> Cooper's cool-headed performance and piloting skills led to a basic rethinking of design philosophy for later space missions.{{citation needed|date=April 2016}}

[[File:Cooper and Conrad on Deck - GPN-2000-001494.jpg|thumb|right|[[Pete Conrad]] and Gordon Cooper on deck of recovery carrier [[USS Lake Champlain (CV-39)|USS ''Lake Champlain'']] after Gemini 5 mission]]

====Project Gemini====
{{Main article|Gemini 5}}

Two years later (August 21, 1965), Cooper flew as Command Pilot of [[Gemini 5]] on an eight-day, 120-orbit mission with [[Pete Conrad]]. The two astronauts established a new space endurance record by traveling a distance of 3,312,993 miles (5,331,745&nbsp;km) in 190 hours and 56 minutes, showing that astronauts could survive in space for the length of time necessary to go from the Earth to the [[Moon]] and back.  Cooper was the first astronaut to make a second orbital flight and later served as backup Command Pilot for [[Gemini 12]].

====Apollo program====
Cooper was selected as backup Commander for the May 1969 [[Apollo 10]] mission. He hoped this placed him in position as Commander of [[Apollo 13]], according to the [[Apollo 13#Prime and backup crew|usual crew rotation]] procedure established by the Flight Crew Operations Director - grounded fellow Mercury astronaut [[Deke Slayton]]. However, by May 1969, when another grounded Mercury astronaut, Slayton's assistant [[Alan Shepard]] was returned to flight status, Slayton replaced Cooper with Shepard as Commander of this crew, which was then reassigned to [[Apollo 14]] in order to give Shepard more time to train.<ref name=40thmerc7/><ref>{{Cite book |last=Shayler |first=David |title=Apollo: The Lost and Forgotten Missions |year=2002 |page=281 |isbn=1-85233-575-0}}</ref>  Loss of this command placed Cooper farther down the flight rotation, meaning he would not fly until one of the later flights, if ever.

====Retirement from astronaut corps====
Disappointed by the reduced chances of commanding a Moon landing flight, Cooper retired from NASA and the Air Force on July 31, 1970, as a [[Colonel (United States)|Colonel]], having flown 222 hours in space.  In his book "Leap of Faith" (pp.&nbsp;176–183), Cooper charged that Shepard and Slayton had taken unfair advantage of their control of Apollo flight crew assignments by giving him the "third-in-a-row" backup crew assignment, in order to promote their own chances of flying.

However, Cooper had developed a lax attitude towards training during the Gemini program; for the Gemini 5 mission, other astronauts had to coax him into the simulator. He also entered the [[24 Hours of Daytona]] road race while training. Slayton felt this placed him in too much danger and cancelled his entry.<ref>{{Cite book |last=Chaikin |first=Andrew |authorlink=Andrew Chaikin |others=Foreword by [[Tom Hanks]] |title=[[A Man on the Moon|A Man on the Moon: The Voyages of the Apollo Astronauts]] |year=2007 |page=247 |publisher=[[Penguin Books]] |location=New York |isbn=978-0-14-311235-8 |ref=Chaikin}}</ref>

Slayton wrote in his memoirs that he never intended to rotate Cooper to another mission, and assigned him to the Apollo 10 backup crew simply because of a lack of qualified Astronaut Office manpower at the time the assignment needed to be made. Cooper, Slayton noted, had a very small chance of receiving the Apollo 13 command if he did an outstanding job with the assignment, which he did not.<ref>{{Cite book |title=Deke! U.S. Manned Space: From Mercury to the Shuttle |last1=Slayton |first1=Donald K. "Deke" |authorlink1=Deke Slayton |last2=Cassutt |first2=Michael |authorlink2=Michael Cassutt |year=1994 |page = 236 |edition=1st |publisher=[[Tor Books|Forge]] |location=New York |isbn=0-312-85503-6 |ref=Slayton & Cassutt}}</ref>

===Later years===
Cooper received an [[Honorary degree|Honorary]] [[Doctor of Science|D.Sc.]] from [[Oklahoma State University]] in 1967. His autobiography, ''Leap of Faith'' (ISBN 0-06-019416-2), co-authored by [[Bruce Henderson (author)|Bruce Henderson]], recounted his experiences with the Air Force and [[NASA]], along with his efforts to expose an alleged [[UFO conspiracy theory]]. Cooper was also a major contributor to the book ''[[In the Shadow of the Moon (book)|In the Shadow of the Moon]]'' (published after his death), which offered Cooper's final published thoughts on his life and career.

After leaving [[NASA]], Cooper served on several corporate boards and as technical consultant for more than a dozen companies in fields ranging from high performance boat design to energy, construction, and aircraft design. During the 1970s, he worked for [[The Walt Disney Company]] as a [[Vice President]] of research and development for [[Epcot]].

Cooper's hobbies included treasure hunting, archeology, racing, flying, skiing, boating, hunting, and fishing.

====Personal life====
Cooper married his first wife Trudy B. Olson (1927–1994) in 1947<ref>{{cite web|url=https://www.newspapers.com/newspage/74942456/|title=Nashua Telegraph from Nashua, New Hampshire · Page 2|work=Newspapers.com}}</ref><ref>{{cite web|url=http://sortedbyname.com/pages/c114108.html|title=Genealogy references sorted by name|work=sortedbyname.com}}</ref>),.  She was a Seattle native and flight instructor where he was training. Together, they had two daughters: Camala Keoki, born 1948;<ref name=40thmerc7/> and Janita Lee (1950–2007),.<ref name=40thmerc7/><ref>{{cite news|title=Obituaries: Janita Lee 'Jan' Cooper |url=http://www.easttexasnews.com/Obituaries/Obituaries_Trinity/obits_trinity_3_23_2007.html |accessdate=June 24, 2015 |work=Trinity Standard |date=2007}}</ref>  The couple divorced in 1971.

Cooper married Suzan Taylor in 1972. Together, they had two daughters: Elizabeth Jo, born in 1979; and Colleen Taylor, born in 1980.  The couple remained married until his death in 2004.<ref>{{cite news |last1=Wald |first1=Matthew L. |title=Gordon Cooper, Astronaut, Is Dead at 77 |url=https://www.nytimes.com/2004/10/05/obituaries/05cooper.html |accessdate=July 10, 2015 |work=New York Times |date=October 5, 2004}}</ref>

====Death====
Cooper developed [[Parkinson's disease]] and died at age 77 from [[heart failure]] at his home in [[Ventura, California]], on October 4, 2004. His death occurred on the 47th anniversary of the [[Sputnik 1]] launch and the same day that [[SpaceShipOne]] made its second official qualifying flight.

==UFO sightings==
{{Refimprove section|date=July 2015}}
{{quote box|quote=I believe that these extra-terrestrial vehicles and their crews are visiting this planet from other planets. Most astronauts were reluctant to discuss UFOs.|source=Gordon Cooper, ''Leap of Faith''|width=30%|align=right|salign=right}}
Cooper claimed to have seen his first [[UFO]] while flying over West Germany in 1951, although he denied reports he had seen a UFO during his Mercury flight.<ref>Martin, Robert Scott, ''[http://www.space.com/sciencefiction/phenomena/cooper.html Gordon Cooper: No Mercury UFO]'', space.com, 10 September 1999, retrieved 20 January 2008</ref>

In 1957, when Cooper was 30 and a [[Captain (land)|Captain]], he was assigned to Fighter Section of the [[Air Force Test Center|Experimental Flight Test Engineering Division]] at Edwards AFB in California. He acted as a test pilot and project manager. On May 3 of that year, he had a crew setting up an [[Askania]] [[Cinetheodolite]] precision landing system on a [[dry lake]] bed. This cinetheodolite system would take pictures at one frame per second as an aircraft landed. The crew consisted of James Bittick and Jack Gettys who began work at the site just before 0800, using both still and motion picture cameras. According to his accounts, later that morning they returned to report to Cooper that they saw a "strange-looking, saucer-like" [[aircraft]] that did not make a sound either on landing or take-off.

According to his accounts, Cooper realized that these men, who on a regular basis have seen [[experimental aircraft]] flying and landing around them as part of their job of filming those aircraft, were clearly worked up and unnerved. They explained how the saucer hovered over them, landed 50 [[yards]] away from them using three extended landing gears and then took off as they approached for a closer look. Being [[photographers]] with cameras in hand, they of course shot images with [[35mm]] and [[Large format (photography)|4×5]] still cameras as well as motion picture film. There was a special [[The Pentagon|Pentagon]] number to call to report incidents like this. He called and it immediately went up the [[chain of command]] until he was instructed by a [[General officer|general]] to have the film developed (but to make no [[photographic print|prints]] of it) and send it right away in a locked courier pouch. As he had not been instructed to ''not'' look at the [[Negative (photography)|negative]]s before sending them, he ''did''. He said the quality of the [[photography]] was excellent as would be expected from the experienced photographers who took them. What he saw was exactly what they had described to him. He did not see the movie film before everything was sent away. He expected that there would be a follow up investigation since an aircraft of unknown origin had landed in a highly [[classified information|classified]] [[military]] installation, but nothing was ever said of the incident again. He was never able to track down what happened to those photos. He assumed that they ended up going to the [[United States Air Force|Air Force]]'s official UFO investigation, [[Project Blue Book]], which was based at [[Wright-Patterson Air Force Base]].

He held claim until his death that the U.S. government was indeed covering up information about UFOs. He gave the example of [[President of the United States|President]] [[Harry Truman]] who he claimed said on April 4, 1950, "I can assure you that [[flying saucers]], given that they exist, are not constructed by any power on Earth." He also pointed out that there were hundreds of reports made by his fellow [[aircraft pilot|pilots]], many coming from [[military]] [[jet aircraft|jet]] pilots sent to respond to [[radar]] or visual sightings from the ground.<ref>{{cite web|url=http://www.space.com/news/spaceagencies/gordon_ufos_000728.html |title=SPACE.com - Gordon Cooper Touts New Book Leap of Faith |accessdate=2004-09-21 |deadurl=bot: unknown |archiveurl=https://web.archive.org/web/20100727041953/http://www.space.com/news/spaceagencies/gordon_ufos_000728.html |archivedate=July 27, 2010 |df= }}</ref> In his memoirs, Cooper wrote he had seen other unexplained aircraft several times during his career, and also said hundreds of similar reports had been made. He further claimed these sightings had been "swept under the rug" by the U.S. government.<ref name="UFO"/> Throughout his later life Cooper expressed repeatedly in interviews he had seen UFOs and described his recollections for the documentary [[Out of the Blue (2002 film)|''Out of the Blue'']].<ref name="UFO"/>

==Memorial spaceflights==
On April 29, 2007, Cooper's ashes (along with those of ''[[Star Trek]]'' actor [[James Doohan]] and 206 others) were launched from [[New Mexico]] on a sub-orbital memorial flight by a privately owned [[UP Aerospace]] [[SpaceLoft XL]] [[sounding rocket]]. Although the capsule carrying the ashes fell back toward Earth as planned, it was lost in mountainous landscape. The search was thwarted by bad weather but after a few weeks the capsule was found and the ashes it carried were returned to the families.<ref>uk.reuters.com, ''[http://uk.reuters.com/article/wtMostRead/idUKN1823270020070518 Ashes of "Star Trek's" Scotty found after space ride]'', 18 May 2007, retrieved 20 January 2008</ref><ref>Sherriff, Lucy, ''[http://www.theregister.co.uk/2007/05/22/scotty_ashes_found/ Scotty: ashes located and heading home]'', 22 May 2007, retrieved 20 January 2008</ref> The ashes were then launched on the ''Explorers'' orbital mission (August 3, 2008) but were lost when the [[Falcon 1]] rocket failed two minutes into the flight.<ref>{{cite web|url=http://www.nasaspaceflight.com/2008/08/spacex-falcon-i-fails-during-first-stage-flight/|title=SpaceX Falcon I fails during first stage flight|work=nasaspaceflight.com}}</ref>

On May 22, 2012, Cooper's ashes were among those of 308 people included on the [[SpaceX]] flight that was bound for the [[International Space Station]]. This flight, using the "Falcon" launch vehicle and the "[[SpaceX Dragon|Dragon]]" capsule, was unmanned.

==Organizations==
Cooper was a member of several groups and societies including the [[Society of Experimental Test Pilots]], the [[American Institute of Aeronautics and Astronautics]], the [[American Astronautical Society]], [[Scottish Rite]] and [[York Rite]] [[Freemasonry|Masons]], [[Shriners]], the [[Rotary Club]], [[Order of Daedalians]], [[Confederate Air Force]], [[Adventurers' Club of Los Angeles]], and [[Boy Scouts of America]].

==Awards and honors==
<center>
{|
|colspan=3 align=center|[[File:USAF Master Astronaut badge.jpg|200px]]
|-
|{{ribbon devices|number=0|type=oak|ribbon=Legion of Merit ribbon.svg|width=103}} 
|{{ribbon devices|number=1|type=oak|ribbon=Distinguished Flying Cross ribbon.svg|width=103}} 
|{{ribbon devices|number=0|type=oak|ribbon=NasaDisRib.gif|width=103}}
|-
|{{ribbon devices|number=0|type=service-star|ribbon=USA - NASA Excep Rib.png|width=103}}
|{{ribbon devices|number=0|type=service-star|ribbon=American Campaign Medal ribbon.svg|width=103}}
|{{ribbon devices|number=0|type=service-star|ribbon=World War II Victory Medal ribbon.svg|width=103}}
|-
|{{ribbon devices|number=0|type=service-star|ribbon=Army of Occupation ribbon.svg|width=103}}
|{{ribbon devices|number=1|type=service-star|ribbon=National Defense Service Medal ribbon.svg|width=103}}
|{{ribbon devices|number=4|type=oak|ribbon=Air Force Longevity Service ribbon.svg|width=103}}
|}

{| class="wikitable"
|-
|colspan=3 align=center|[[Astronaut Badge#U.S. Air Force astronauts|Air Force Master Astronaut badge]]
|-
|align=center|[[Legion of Merit]]
|align=center|[[Distinguished Flying Cross (United States)|Distinguished Flying Cross]]<br>with [[oak leaf cluster|cluster]] 
|align=center|[[NASA Distinguished Service Medal]]
|-
|align=center|[[NASA Exceptional Service Medal]]
|align=center|[[American Campaign Medal]]
|align=center|[[World War II Victory Medal (United States)|World War II Victory Medal]]
|-
|align=center|[[Army of Occupation Medal]]<br>with GERMANY Clasp
|align=center|[[National Defense Service Medal]]<br>with one [[Service star|star]]
|align=center|[[Air Force Longevity Service Award]]<br>with four clusters
|}
</center>

Cooper received many other awards including the [[Collier Trophy]], the [[Harmon Trophy]], the [[DeMolay]] Legion of Honor, the [[John F. Kennedy]] Trophy, the [[Iven C. Kincheloe Award]], the [[Air Force Association]] Trophy, the [[John J. Montgomery Award]], the General [[Thomas D. White]] Trophy, the [[University of Hawaii]] Regents Medal, the [[Christopher Columbus|Columbus]] Medal, and the [[Silver Antelope Award]]. He was a [[Master Mason]] (member of Carbondale Lodge # 82 in [[Carbondale, Colorado]]), and was given the honorary 33rd Degree by the [[Scottish Rite]] Masonic body, ''see [[List of Freemasons|List of Notable Freemasons]]''.

The [[Gordon Cooper Technology Center]] in Shawnee, [[Oklahoma]] is named after Cooper.

Cooper was inducted into the [[International Space Hall of Fame]] in 1981,<ref>{{cite web|url=http://www.nmspacemuseum.org/halloffame/detail.php?id=53|title=International Space Hall of Fame :: New Mexico Museum of Space History  :: Inductee Profile|work=nmspacemuseum.org}}</ref> and into the [[U.S. Astronaut Hall of Fame]] on May 11, 1990.<ref>{{cite web|url=http://astronautscholarship.org/Astronauts/l-gordon-cooper-jr/|title=L. Gordon Cooper Jr. - Astronaut Scholarship Foundation|work=astronautscholarship.org}}</ref>

==Cultural influence==
{{unreferenced section|date=July 2015}}
Cooper's accomplishments (along with his widely noted and appealing personality) were depicted in the 1983 film ''[[The Right Stuff (film)|The Right Stuff]]'' in which he was portrayed by actor [[Dennis Quaid]]. Cooper worked closely with the production company on this project and reportedly, every line uttered by Quaid is attributable to Cooper's recollection. Quaid met with Cooper before the casting call and rapidly learned his mannerisms. Quaid also had his hair cut and dyed to match how the former astronaut's hair looked during the 1950s and 1960s. Cooper was later depicted in the 1998 [[HBO]] [[miniseries]] ''[[From the Earth to the Moon (miniseries)|From the Earth to the Moon]]'', in which his character was played by [[Robert C. Treveiler]]. Cooper appeared as himself in an episode of the television series ''[[CHiPs]]'' and during the early 1980s made regular call in appearances on ''[[Late Night with David Letterman]]''. The ''[[Thunderbirds (TV series)|Thunderbirds]]'' character [[Gordon Tracy]] was named after him.

In the 2015 [[American Broadcasting Company|ABC]] TV series ''[[The Astronaut Wives Club]]'', he is portrayed by [[Bret Harrison]]. That same year, he was portrayed by [[Colin Hanks]] in the Season 3 "Oklahoma" of [[Drunk History]]. Laura Steinel retells the story of his [[Mercury-Atlas 9]] flight.

==Physical description==
*Weight: 155&nbsp;lb (70&nbsp;kg)
*Height: 5&nbsp;ft 8 in (1.73 m)
*Hair: Brown
*Eyes: Blue<ref>[http://history.nasa.gov/40thmerc7/cooper.htm Gordon Cooper's physical description]</ref>

==See also==
{{Portal|Biography|United States Air Force|Spaceflight}}
* [[List of spaceflight records]]

==References and notes==
{{reflist}}

==External links==
{{Commons category}}
*[http://www.jamesoberg.com/gordon_cooper2008comments.pdf Cooper Comments]
*[http://www.jsc.nasa.gov/Bios/htmlbios/cooper-lg.html Cooper's official NASA short biography]
*[http://www.muldrake.com/cooper24.html About Gordon Cooper]
*[http://www.astronautix.com/astros/cooper.htm Astronautix biography of L. Gordon Cooper Jr.]
*[http://www.spacefacts.de/bios/astronauts/english/cooper_gordon.htm Spacefacts biography of L. Gordon Cooper Jr.]
*[http://www.daviddarling.info/encyclopedia/C/Cooper.html Cooper at Encyclopedia of Science]
*[http://www.spaceacts.com/STARSHIP/seh/cooper.htm Cooper at Spaceacts]
*''[http://www.nasa.gov/vision/space/features/remembering_gordo.html Remembering 'Gordo' – NASA memories of Gordon Cooper]''
*[http://www.ufoevidence.org/news/article162.htm Interview with Cooper about UFOs and aliens] (with much added speculation)
*Space.com article, ''[http://www.space.com/news/spaceagencies/gordon_ufos_000728.html Pioneering Astronaut Sees UFO Cover-up]''
*{{IMDb name|0178070}}
*[http://www.gctech.org/ Gordon Cooper Technology Center]
*[http://digital.library.okstate.edu/encyclopedia/entries/C/CO052.html Encyclopedia of Oklahoma History and Culture - Cooper, Leroy Gordon]
*[http://www.nmspacemuseum.org/halloffame/detail.php?id=53 Cooper at International Space Hall of Fame]
*{{Find a Grave|9554259|Gordon "Gordo" Cooper Jr}}

{{NASA Astronaut Group 1}}
{{US Air Force navbox}}

{{Authority control}}

{{DEFAULTSORT:Cooper, Gordon}}
[[Category:1927 births]]
[[Category:2004 deaths]]
[[Category:American astronauts]]
[[Category:American aviators]]
[[Category:Aviators from Oklahoma]]
[[Category:United States Air Force astronauts]]
[[Category:United States Astronaut Hall of Fame inductees]]
[[Category:1963 in spaceflight]]
[[Category:1965 in spaceflight]]
[[Category:People from Shawnee, Oklahoma]]
[[Category:People from Murray, Kentucky]]
[[Category:Murray High School (Kentucky) alumni]]
[[Category:Air Force Institute of Technology alumni]]
[[Category:U.S. Air Force Test Pilot School alumni]]
[[Category:University of Hawaii alumni]]
[[Category:University of Maryland, College Park alumni]]
[[Category:United States Air Force officers]]
[[Category:American test pilots]]
[[Category:American aerospace engineers]]
[[Category:American engineers]]
[[Category:American Freemasons]]
[[Category:American businesspeople]]
[[Category:Deaths from Parkinson's disease]]
[[Category:Space burials]]
[[Category:Recipients of the Legion of Merit]]
[[Category:Recipients of the Distinguished Flying Cross (United States)]]
[[Category:Recipients of the NASA Exceptional Achievement Medal]]
[[Category:Collier Trophy recipients]]
[[Category:Harmon Trophy winners]]
[[Category:United States Marines]]
[[Category:20th-century American businesspeople]]
[[Category:Mercury Seven]]