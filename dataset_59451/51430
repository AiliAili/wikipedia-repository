{{Infobox journal
| title = Harvard Environmental Law Review
| cover =
| formernames = 
| abbreviation = Harv. Envtl. L. Rev.
| discipline = [[Environmental law]]
| editor = Samantha Caravello and Genevieve Parshalle
| publisher = [[Harvard Law School]]
| history = 1976–present
| frequency = Biannually
| website = http://harvardelr.com/
| impact = 
| link1-name =
| link2 =
| ISSN = 0147-8257 
| eISSN = 
| OCLC = 03231039
| LCCN = 77641876
| JSTOR = 
}}
The '''''Harvard Environmental Law Review''''' (''[[Bluebook]]'' abbreviation: ''Harv. Envtl. L. Rev.'') is a student-run [[law review]] published at [[Harvard Law School]]. The journal publishes articles, notes, and comments on subjects relating to [[environmental law]], land-use law, and the regulation of natural resources.<ref>{{smallcaps|HeinOnline Law Journal Library}}, [https://heinonline.org/HOL/TitleSummary?index=journals/helr&collection=journals&handle= Additional Information for ''Harvard Environmental Law Journal''] (Accessed November 7, 2016); see also {{smallcaps|Harvard Law School}}, ''[http://hls.harvard.edu/dept/dos/student-journals/journals-and-publications/ Journals and Publications]'' (Accessed November 7, 2016).</ref>

== History and overview ==
The ''Harvard Environmental Law Review'' was founded in 1976 by Harvard Law School students Deborah Williams, James McDaniel, and Alan Gabbay;<ref>Bonnie Docherty, ''25 Years or HELR: The Invention and Reinvention of Environmental Law'', 25 {{smallcaps|Harv. Envt'l L. Rev.}} 323, 328 (2001).</ref> at the time, it was the school's fifth law journal.<ref>Albert M. Sacks, ''Preface'', 1 {{smallcaps|Harv. Envtl. L. Rev.}} xv, xv (1976).</ref> In a preface to the inaugural issue, former Harvard Law School dean [[Albert Sacks]] wrote that one of the journal's goals was to "raise questions" about whether environmental progress "conforms to an intelligent and coherent set of goals."<ref>Albert M. Sacks, ''Preface'', 1 {{smallcaps|Harv. Envtl. L. Rev.}} xv, xvi (1976).</ref> The founding editors explained that the goal of the inaugural issue was to cover recent developments in environmental law and policy.<ref>''Introduction'', 1 {{smallcaps|Harv. Envtl. L. Rev.}} xvii, xx (1976).</ref> However, in its early years, the journal shifted its focus toward a wider range of topics because "it was too ambitious to ask students to research and write a comprehensive overview of recent developments every year."<ref>Bonnie Docherty, ''25 Years or HELR: The Invention and Reinvention of Environmental Law'', 25 {{smallcaps|Harv. Envtl. L. Rev.}} 323, 328 (2001) (internal citation omitted).</ref>

==Impact==
In 2016, [[Washington and Lee University]]'s Law Journal Rankings placed the journal as the top-ranked environmental, natural resources, and land use law journal according to combined score, impact factor, and journal citations.<ref name=wl>[http://lawlib.wlu.edu/LJ/index.aspx "Law Journals: Submission and Ranking, 2008–2015,"] Washington & Lee University (Accessed November 7, 2016).</ref> In his 1998 assessment of environmental law journals, Gregory Scott Crespi described the ''Harvard Environmental Law Review'' as a "leading journal" in its field.<ref>Gregory Scott Crespi, ''Ranking the Environmental Law, Natural Resources Law, and Land Use Planning Journals: A Survey of Expert Opinion'', 23 {{smallcaps|Wm. & Mary Envtl. L. & Pol'y Rev.}} 273, 298 (1998)</ref> The journal has been cited legal treatises, including ''[[American Jurisprudence]]'',<ref>See, e.g., Eric C. Surette, 61 {{smallcaps|Am. Jur. 2d}}, ''Plant and Job Safety'' § 40.</ref> [[American Law Reports]],<ref>See, e.g., Lora A. Lucero, ''Construction and Application of the Cooperation with States Requirement under Sec. 6 of the Endangered Species Act of 1973, 16 U.S.C.A. § 1535'', 8 A.L.R. Fed. 3d Art. 3.</ref> and the ''[[Restatements of the Law|Restatement (Third) of Property]]''.<ref>{{smallcaps|Restatement (Third) of Property (Servitudes)}}, ''Modification and Termination of a Conservation Servitude Because of Changed Conditions'', § 7.11 (2000).</ref>

== Abstracting and indexing ==
The journal is abstracted or indexed in [[EBSCO Information Services|EBSCO databases]], [[HeinOnline]], [[LexisNexis]], [[Westlaw]],<ref name = Finder>{{smallcaps|Washington and Lee University Law Library}}, [http://lawlib.wlu.edu/resolver.aspx?genre=journal&issn=0147-8257&title=harvard+environmental+law+review&searchtype=exact Journal Finder: Harvard Environmental Law Review] (Accessed November 7, 2016).</ref> and the [[University of Washington]]'s Current Index to Legal Periodicals.<ref>{{smallcaps|University of Washington Gallagher Law Library}}, [https://lib.law.washington.edu/cilp/period.html Periodicals Indexed in CLIP] (Accessed November 7, 2016).</ref> Tables of contents are also available through Infotrieve and [[Ingenta]],<ref name=Finder/> and the journal posts past issues on its website.<ref>{{smallcaps|Harvard Environmental Law Review}}, [http://harvardelr.com/print-archives/ Print Archives] (Accessed November 7, 2016).</ref>

== See also ==
* [[List of law journals]]
* [[List of environmental law journals]]

== References ==
{{cbignore}}
{{reflist|30em}}

== External links ==
* {{Official website|http://harvardelr.com/}}

{{DEFAULTSORT:Harvard Environmental Law Review}}
[[Category:American law journals]]
[[Category:Publications established in 1976]]
[[Category:English-language journals]]
[[Category:Environmental law journals]]
[[Category:Law journals edited by students]]
[[Category:Harvard Law School]]