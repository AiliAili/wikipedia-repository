{{Use mdy dates | date=April 2017}}
{{Infobox Officeholder
|name        = Amanda Curtis
|state_house = Montana
|district    = 74th
|term_start  = January 2, 2017
|term_end    = 
|predecessor = Pat Noonan
|successor   = 
|state_house1 = Montana
|district1    = 76th
|term_start1  = January 7, 2013
|term_end1    = January 7, 2015
|predecessor1 = [[Jon Sesso]]
|successor1   = Ryan Lynch
|birth_name  = Amanda Gayle Morse
|birth_date  = {{birth date and age|1979|9|10}}
|birth_place = [[Billings, Montana|Billings]], [[Montana]], [[United States|U.S.]]
|death_date  = 
|death_place = 
|party       = [[Democratic Party (United States)|Democratic]]
|spouse      = {{marriage|Kevin Curtis|()=smaller|2001}}<ref>{{cite web |last1=Christensen |first1=Matt | title=Military helicopter rescues skiers in Highlands | url=http://mtstandard.com/news/local/military-helicopter-rescues-skiers-in-highlands/article_7319d1d4-e502-11e3-b178-001a4bcf887a.html | website=Montana Standard | accessdate=September 21, 2014 | date=May 26, 2014}}</ref><ref name="About">{{cite web | url=http://www.amandaformontana.com/about/ | title=About Amanda Curtis | publisher=AmandaForMontana.com | accessdate=September 24, 2014}}</ref>
|education   = [[Montana Tech of the University of Montana|University of Montana, Tech]] {{small|([[Bachelor of Science|BS]])}}<br>[[University of Montana Western|University of Montana, Western]]<br>[[University of Montana|University of Montana, Missoula]] {{small|([[Master of Education|MEd]])}}
}}
'''Amanda Gayle Curtis''' ([[née]] '''Morse''', born September 10, 1979) is a [[Democratic Party (United States)|Democratic Party]] politician and member of the [[Montana House of Representatives]], representing [[Butte, Montana]] in House District 74. A teacher by profession, Curtis has been teaching math and physics since 2006 and while the [[Montana Legislature]] was out of session, continued to teach.<ref>{{cite web | url=https://www.facebook.com/CurtisForLegislature/info  | title=Representative Amanda Curtis Facebook | publisher=Facebook.com | date= | accessdate=April 15, 2013}}</ref><!--link to leg.mt.gov site is preferable to facebook, may want to update this eventually-->

Curtis was chosen to replace [[John Walsh (U.S. politician)|John Walsh]] as Montana's Democratic Senate nominee in the [[United States Senate election in Montana, 2014|2014 election]] after Walsh dropped out.<ref>{{cite web | url=http://missoulian.com/news/state-and-regional/amanda-curtis-wins-montana-democratic-nomination-to-u-s-senate/article_d7da4810-2558-11e4-9265-0019bb2963f4.html  | title=Amanda Curtis wins Montana Democratic nomination to U.S. Senate | publisher=The Missoulian | date= | accessdate=August 16, 2014}}</ref>

== Early life, education, and career ==
Curtis' parents divorced when she was four years old.  During her childhood, she credits the union benefits and health insurance earned through her father's work for allowing her to obtain health care, including basic vision and dental services. Her family was poor, and her mother struggled with mental illness; the family sometimes relied upon [[Supplemental Nutrition Assistance Program|food stamps]] and endured periods of having their utilities disconnected.<ref name=Gazette16Sept>{{cite web | url=http://billingsgazette.com/news/state-and-regional/montana/who-is-amanda-curtis-a-profile-of-democrats-candidate-for/article_0b80c4a2-133d-5e06-9b2e-01644612bf25.html | title=Who is Amanda Curtis?: A profile of Democrats' candidate for Senate | publisher=Billings Gazette | date=August 16, 2014 | accessdate=September 16, 2014}}</ref><ref>{{cite web | url=http://www.csmonitor.com/USA/DC-Decoder/Decoder-Buzz/2014/0817/Montana-Senate-race-a-lost-cause-for-Democrats-Enter-Amanda-Curtis-video | title=Montana Senate race a lost cause for Democrats? Enter Amanda Curtis (+video) | publisher=The Christian Science Monitor | date=August 17, 2014 | accessdate=September 16, 2014}}</ref> Her support of gun measures such as expanded background checks stems directly from the death of her brother, who killed himself playing [[Russian roulette]] when he was 16.<ref>{{cite web | url=http://www.washingtonpost.com/blogs/the-fix/wp/2014/08/19/in-new-montana-nominee-democrats-ditch-red-state-pragmatism-for-a-gun-control-supporting-vlogger/ | title=Meet Amanda Curtis. She’s a gun control-supporting vlogger. She’s also the Democratic Senate nominee in Montana. | publisher=The Washington Post | date=August 19, 2014 | accessdate=September 16, 2014}}</ref>

Curtis valued education as the way out of poverty.<ref name=Gazette16Sept/> After graduating from [[Montana Tech of the University of Montana]] in 2002 with a [[Bachelor of Science]] in [[biology]], she went on to the [[University of Montana Western]] where she received her secondary teaching certificate in biology and mathematics in 2004.<ref>[http://convergysvotes.com/voter-resources/official/389771/Amanda/Curtis Convergys – State Representative Amanda Curtis]</ref><ref>[http://missoulian.com/news/state-and-regional/butte-legislator-curtis-being-urged-to-run-for-u-s/article_ac48577e-ee7b-11e2-8362-0019bb2963f4.html Butte legislator Curtis being urged to run for U.S. House]</ref> From 2004 to 2006, she worked at [[Butte Central Catholic High School]], teaching math and physics. Between 2006 and 2009, she taught math at Helena Middle School, and since 2009, has worked at [[Butte High School (Butte, Montana)|Butte High School]], also as a math teacher.<ref>{{cite web | url=http://votesmart.org/candidate/biography/136932/amanda-curtis#.UTQe-Vf9ykw  | title=Representative Amanda G. Curtis's Biography | publisher=votesmart.org | date= | accessdate=April 15, 2013}}</ref>

Curtis is currently working toward a [[Master of Education]] in [[educational leadership]] at the [[University of Montana|University of Montana, Missoula]].<ref>[http://mtstandard.com/news/local/butte-s-curtis-not-seeking-re-election-plethora-of-others/article_e91cd566-79b9-11e3-b4cc-0019bb2963f4.html Butte’s Curtis not seeking re-election; plethora of others file]</ref><ref>{{cite web | url= http://curtisforbutte.com/About_Amanda.html | title=About Amanda | publisher=curtisforbutte.com/About_Amanda.html | date= | accessdate=April 15, 2013}}</ref>{{Self-published source | date=February 2017}} She resides in Butte with her husband, Kevin.<ref>{{cite web | url=http://billingsgazette.com/news/state-and-regional/montana/new-lawmakers-learning-ropes-as-legislature-begins/article_ad78f57d-d4b4-5c30-8ed3-415fd3fdc2d8.html  | title=New lawmakers learning ropes as 2013 Legislature begins | publisher=billingsgazette.com | date= | accessdate=April 15, 2013}}</ref>

== Montana House of Representatives ==

=== Election ===
Curtis was elected unopposed on November 6, 2012 to the Montana House of Representatives to succeed fellow Democrat [[Jon Sesso]] who was elected to the [[Montana Senate]]. In the 2013 legislative session, she is one of the 39 Democrats in the House.<ref>{{cite web | url= http://sos.mt.gov/Elections/2012/2012_General_Leg_Canvass.pdf  | title=2012 Legislative General Election Canvass | publisher=sos.mt.gov | date= | accessdate=April 15, 2013}}</ref>

She is not running for re-election in the 2014 cycle, citing both political and professional reasons for not seeking a second term. Curtis mentioned the State Senate as a possible re-entry into Montana politics in 2016.<ref>{{cite web | url=http://missoulian.com/news/local/democrat-republicans-say-they-are-running-for-montana-s-u/article_105904e4-2e31-11e3-8caf-0019bb2963f4.html  | title=1 Democrat, 5 Republicans say they are running for Montana's U.S. House seat so far | publisher=missoulian.com/ | date= | accessdate=October 8, 2013}}</ref><ref>{{cite web | url=http://mtstandard.com/news/local/butte-s-curtis-not-seeking-re-election-plethora-of-others/article_e91cd566-79b9-11e3-b4cc-0019bb2963f4.html  | title=Butte’s Curtis not seeking re-election; plethora of others file | publisher=mtstandard.com/ | date= | accessdate=January 10, 2014}}</ref>

=== Tenure ===
Curtis was sworn in on January 7, 2013.

For the first 87 days of the session, Curtis uploaded a daily [[video blog|vlog]] to the video-sharing website [[YouTube]], discussing what had gone on that day in the [[Montana Legislature]].<ref>{{cite web | url=https://www.youtube.com/user/curtisforbutte  | title=Amanda Curtis YouTube | publisher=youtube.com | date= | accessdate=April 15, 2013}}</ref> In April 2013, Curtis gained media attention after calling out [[Republican Party (United States)|Republican]] members for opposing the repeal of Montana's [[Sodomy law|anti-sodomy law]].<ref>{{cite news | url=http://www.huffingtonpost.com/2013/04/09/amanda-curtis-montana_n_3046636.html | title=Amanda Curtis, Montana Democrat, Calls Out GOP Colleagues For Opposing Repeal Of Anti-Gay Law | publisher=Huffingtonpost.com | date= April 9, 2013 | accessdate=April 15, 2013 | first=Preston | last=Maddock}}</ref>

During a rally sponsored by [[Mayors Against Illegal Guns]] outside of the [[Montana State Capitol]] in August 2013, Curtis urged Montana's senior [[United States Senate|U.S. Senator]] [[Max Baucus]] to rethink his opposition to expanded background checks. She spoke about how gun violence has affected her own family and called for more action to be taken.<ref>{{cite web | url=http://billingsgazette.com/news/state-and-regional/montana/background-check-supporters-rally-at-montana-capitol/article_e0a15cfc-55b9-55ae-8104-dbabb10f345b.html  | title=Background check supporters rally at Montana Capitol | publisher=billingsgazette.com/ | date= | accessdate=August 11, 2013}}</ref>

During the 2013 session, Curtis was the primary sponsor for six bills that were not enacted.<!-- list:
* HB 166 - Primary Sponsor, - Redirect lottery proceeds to Montana university system student aid. 
* HB 227 - Primary Sponsor, - Provide for public disclosure of energy performance
* HB 542 - Primary Sponsor, - Revise speed limit laws. 
* HB 165 - Primary Sponsor, - Revise laws related to speed upon approaching emergency or police vehicles
* HB 476 - Primary Sponsor, - Add cost of living adjustment for volunteer firefighters.
* HB 490 - Primary Sponsor, - Provide for the Hire Montana First Act.--> She was the primary sponsor of two bills that were passed and signed into law, HB 92 to remove public defender from certain court definitions, and HB 164, to revise when county commissioner district boundaries can be modified.<ref>{{cite web | url=http://laws.leg.mt.gov/legprd/law0203w$.startup?P_SESS=20131 | title=Montana Legislative Services Online Search Tool, 2013 | publisher=laws.leg.mt.gov/ | date= | accessdate=August 7, 2014}}</ref>

=== Committee assignments ===
* Business and labor
* Human services
* Local government

== 2014 U.S. Senate election and beyond==
{{Main|United States Senate election in Montana, 2014}}

Incumbent Senator [[John Walsh (U.S. politician)|John Walsh]] won the Democratic primary in June 2014. In July, the ''[[The New York Times]]'' ran an article claiming that Walsh had plagiarized a term paper that was a requirement for his [[Master's thesis]] at the [[United States Army War College|Army War College]].<ref>{{Citation| url = https://www.nytimes.com/2014/07/24/us/politics/montana-senator-john-walsh-plagiarized-thesis.html?hp&action=click&pgtype=Homepage&version=HpSumSmallMediaHigh&module=photo-spot-region&region=photo-spot&WT.nav=photo-spot&_r=0| title = Senator’s Thesis Turns Out to Be Remix of Others’ Works, Uncited| publisher = New York Times| date = August 23, 2014| accessdate = August 16, 2014}}</ref>  On August 7, 2014, Walsh announced that he was leaving the 2014 race. The Montana state Democratic Central Committee had until August 20 to select a replacement candidate to appear on the November ballot.<ref name=Missoulian7Aug>{{cite web|author1=Press release | title=Walsh drops out of race for U.S. Senate | url=http://missoulian.com/news/local/walsh-drops-out-of-race-for-u-s-senate/article_4963ce38-1e6b-11e4-9ba0-001a4bcf887a.html | publisher=Missoulian | accessdate=August 7, 2014 | date=August 7, 2014}}</ref> The Montana Democratic Party set a nominating convention for Saturday August 16.<ref>{{cite web | url=http://www.montanademocrats.org/node/522 | title=Montana Democratic Party Releases Process and Procedures for Special Nominating Convention | publisher=Montana Democratic Party | date= | accessdate=August 12, 2014}}</ref>

The day prior to the nomination convention, Amanda Curtis was considered the front-runner to replace Walsh.<ref>{{cite web|author1=Press release | title=Butte's Curtis may be the Front-runner | url=http://mtstandard.com/butte-s-curtis-may-be-the-front-runner-to-replace/image_11436f8e-243a-11e4-92fe-0019bb2963f4.html | publisher=Montana Standard | accessdate=August 16, 2014 | date=August 15, 2014}}</ref> She had gained several key endorsements, including the [[MEA-MFT]].<ref>{{cite web | url=http://billingsgazette.com/news/state-and-regional/montana/adams-curtis-wanzenried-vying-to-replace-walsh-in-senate-race/article_9a523d7a-f28b-5b01-9d04-cd0c7b24861e.html | title=Adams, Curtis, Wanzenried vying to replace Walsh in Senate race – and maybe Bohlinger | work=Billings Gazette | date=August 11, 2014 | accessdate=August 12, 2014}}</ref> The week leading up to the nominating convention, several people who had expressed interest in the nomination dropped out, leaving Curtis and [[Dirk Adams]] as the remaining candidates.<ref>{{cite news | title=Curtis gets the nod from Montana Democrats | url=http://www.krtv.com/news/curtis-gets-the-nod-from-montana-democrats/ | first=Sanjay | last=Alwani | work=KRTV | date=August 17, 2014}}</ref><ref>{{cite news | title=Franke Wilmer drops out of race for U.S. Senate | url=http://www.kbzk.com/news/franke-wilmer-drops-out-of-race-for-u-s-senate/ | first=Beth | last=Saboe | work=7 KBZK | date=August 11, 2014}}</ref><ref>{{cite web | url=http://missoulian.com/news/local/wanzenried-ends-effort-for-democratic-nomination-in-u-s-senate/article_f30d1f68-23e6-11e4-ba09-0019bb2963f4.html | title=Wanzenried ends effort for Democratic nomination in U.S. Senate race | work=Billings Gazette | date=August 14, 2014 | accessdate=August 14, 2014}}</ref><ref>{{cite web | url=http://www.nbcmontana.com/news/two-democrats-vie-for-senate-candidate-nomination/27515790 | title=2 Democrats vie for Senate candidate nomination | work=NBC Montana | date=August 15, 2014 | accessdate=August 16, 2014}}</ref> During the convention, Curtis received 82 votes to Adams's 46 (with one delegate abstaining), winning the nomination with 64% of the vote.<ref>{{cite web | url=http://missoulian.com/news/state-and-regional/amanda-curtis-wins-montana-democratic-nomination-to-u-s-senate/article_d7da4810-2558-11e4-9265-0019bb2963f4.html | title=Amanda Curtis wins Montana Democratic nomination to U.S. Senate | publisher=The Missoulian | accessdate=August 16, 2014}}</ref> Curtis was able to accept only the maximum legally allowable contribution of $2,000 from funds that Walsh had left over in his campaign account; Walsh also distributed funds to other candidates and to party organizations.<ref>{{cite web | url=http://mtstandard.com/news/local/amanda-curtis-getting-only-from-leftover-campaign-money/article_70559292-2717-11e4-b64d-0019bb2963f4.html | title=Amanda Curtis getting only $2,000 from leftover campaign money | publisher=Montana Standard | accessdate=August 19, 2014}}</ref> On August 21, Curtis announced that she had raised $110,000 in four days and had named Clayton Elliott, director of the League of Rural Voters and lead lobbyist/community organizer of the [[Northern Plains Resource Council]], as her campaign manager.<ref>{{cite web | url=http://www.ktvq.com/news/amanda-curtis-taps-nprc-staffer-to-run-senate-campaign/ | title=Amanda Curtis taps NPRC staffer to run Senate Campaign | publisher=KTVQ | accessdate=August 21, 2014}}</ref><ref>{{cite web | url=http://mtstandard.com/news/local/curtis-names-campaign-manager-for-u-s-senate-race/article_d079a05a-28d9-11e4-9ee0-001a4bcf887a.html | title=Curtis names campaign manager for U.S. Senate race | publisher=Montana Standard | accessdate=August 21, 2014}}</ref> By mid-October, Curtis had raised $723,000 and had begun airing television ads.<ref>{{cite web | url=http://helenair.com/news/state-and-regional/tester-bullock-lend-star-power-in-final-push/article_6a42f9f9-6475-51cc-b5f4-d9f0704e8e91.html | title=Tester, Bullock lend star power in final push | accessdate=November 2, 2014}}</ref>

===After election and return to the Montana House of Representatives===
After the 2014 Senate election, Curtis served the rest of her term in the Montana House of Representatives and left office in 2015. She returned to teaching math and physics at Butte High School. She remained active in [[MEA-MFT]], serving as the NEA Director as of 2015. Curtis endorsed [[Bernie Sanders]] for [[United States presidential election, 2016|2016 United States presidential election]] and helped organize a rally for Senator Sanders in [[Missoula]] in June 2015.<ref>{{cite web | url=http://news.sciencemag.org/policy/2014/11/loser-montana-senate-race-returns-math-classroom-advice-fellow-democrats | title=Loser in Montana Senate race returns to math classroom, with advice to fellow Democrats | accessdate=November 12, 2014}}</ref><ref>{{cite web | url=http://www.mea-mft.org/about_mea_-_mft/governance/officers_board.aspx | title=MEA-MFT Board of Directors }}</ref><ref>{{cite web | url=http://newstalkkgvo.com/missoula-for-bernie-rally-held-sunday-in-missoula-youtube/ | title=Missoula For Bernie Rally Held Sunday in Missoula | accessdate=June 8, 2015}}</ref>

In October 2015, Curtis announced that she would be running for [[Montana House of Representatives]] District 74.<ref>{{cite web | url=http://billingsgazette.com/news/government-and-politics/butte-s-amanda-curtis-running-for-state-legislature/article_b7840b04-5ccf-50df-b58b-1219bdc98314.html | title=Butte's Amanda Curtis running for state Legislature | accessdate=October 29, 2015}}</ref> Curtis was reelected and returned to the Montana House of Representatives in January 2017.<ref>{{cite web | url=https://www.nytimes.com/elections/results/montana-state-house-district-74 | title=Montana 74th District State House Results: Amanda Curtis Wins | accessdate=December 16, 2016}}</ref> Following her reelection, Curtis expressed interest in running for departing Congressman [[Ryan Zinke]]'s seat in [[Montana's at-large congressional district special election, 2017]].<ref>{{cite web | url=http://mtstandard.com/news/local/experience-of-senate-run-could-help-curtis-in-a-race/article_f28950c5-37b2-587b-9636-c4be42f47b25.html | title=Experience of Senate run could help Curtis in a race for Zinke seat | accessdate=January 10, 2017}}</ref><ref>{{cite web | url=http://missoulian.com/news/government-and-politics/democrat-amanda-curtis-interested-in-u-s-house-fagg-graf/article_4abdba53-240e-5949-801b-a51ac3ae6813.html | title=Democrat Amanda Curtis interested in U.S. House; Fagg, Graf join Republican list | accessdate=January 10, 2017}}</ref><ref>{{cite web | url=http://www.kpax.com/story/34168060/political-wild-card-at-mt-legislature-six-lawmakers-considering-run-for-us-house-seat | title=Political wild card at MT Legislature: Six lawmakers considering run for U.S. House seat | accessdate=January 10, 2017}}</ref> She was not selected as the Democratic nominee, losing to [[Rob Quist]].<ref>{{cite web | url=http://www.greatfallstribune.com/story/news/local/2017/03/05/montana-democrats-picking-candidate-congressional-seat/98776790 | title=Rob Quist wins Democratic nomination for congressional seat | date=March 5, 2017}}</ref>

== References ==
{{Reflist|30em}}
<!--
{{reflist | refs=

<ref name="Representative Amanda Curtis Facebook">{{cite web
 | url= https://www.facebook.com/CurtisForLegislature/info
 | title= Representative Amanda Curtis Facebook
 | publisher= Facebook.com
 | accessdate= April 15, 2013}}
</ref>

<ref name="Representative Amanda G. Curtis's Biography">{{cite web
 | url= http://votesmart.org/candidate/biography/136932/amanda-curtis#.UTQe-Vf9ykw
 | title= Representative Amanda G. Curtis's Biography
 | publisher= Votesmart.org
 | accessdate= April 15, 2013}}
</ref>

<ref name="About Amanda">{{cite web
 | url=  http://curtisforbutte.com/About_Amanda.html
 | title= About Amanda
 | publisher= Curtisforbutte.com
 | accessdate= April 15, 2013}}
</ref>

<ref name="2012 Legislative General Election Canvass">{{cite web
 | url=  http://sos.mt.gov/Elections/2012/2012_General_Leg_Canvass.pdf
 | title= 2012 Legislative General Election Canvass
 | publisher= Sos.mt.gov
 | accessdate= April 15, 2013}}
</ref>

<ref name="Amanda Curtis, Montana Democrat, Calls Out GOP Colleagues For Opposing Repeal Of Anti-Gay Law">{{cite news
 | url= http://www.huffingtonpost.com/2013/04/09/amanda-curtis-montana_n_3046636.html
 | title= Amanda Curtis, Montana Democrat, Calls Out GOP Colleagues For Opposing Repeal Of Anti-Gay Law
 | publisher= Huffingtonpost.com
 | accessdate= April 15, 2013
 | first=Preston
 | last=Maddock
 | date=April 9, 2013}}
</ref>

<ref name="New lawmakers learning ropes as 2013 Legislature begins">{{cite web
 | url= http://billingsgazette.com/news/state-and-regional/montana/new-lawmakers-learning-ropes-as-legislature-begins/article_ad78f57d-d4b4-5c30-8ed3-415fd3fdc2d8.html
 | title= New lawmakers learning ropes as 2013 Legislature begins
 | publisher= Billingsgazette.com
 | accessdate= April 15, 2013}}
</ref>

<ref name="Amanda Curtis YouTube">{{cite web
 | url= https://www.youtube.com/user/curtisforbutte
 | title= Amanda Curtis YouTube
 | publisher= Youtube.com 
 | accessdate= April 15, 2013}}
</ref>

<ref name="Freshman Rep. Curtis of Butte mulls run for the U.S. House">{{cite web
 | url= http://mtstandard.com/news/local/freshman-rep-curtis-of-butte-mulls-run-for-the-u/article_29ed7cae-ee90-11e2-a55f-001a4bcf887a.html
 | title= Curtis of Butte mulls run for the U.S. House
 | publisher= Mtstandard.com 
 | accessdate= August 11, 2013}}
</ref>

<ref name="Background check supporters rally at Montana Capitol">{{cite web
 | url= http://billingsgazette.com/news/state-and-regional/montana/background-check-supporters-rally-at-montana-capitol/article_e0a15cfc-55b9-55ae-8104-dbabb10f345b.html
 | title= Background check supporters rally at Montana Capitol
 | publisher= Billingsgazette.com
 | accessdate= August 11, 2013}}
</ref>

<ref name="1 Democrat, 5 Republicans say they are running for Montana's U.S. House seat so far">{{cite web
 | url=http://missoulian.com/news/local/democrat-republicans-say-they-are-running-for-montana-s-u/article_105904e4-2e31-11e3-8caf-0019bb2963f4.html
 | title= Candidates running for Montatan's U.S. House seat
 | publisher= Missoulian.com
 | accessdate= October 8, 2013}}
</ref>

}}  -->

== External links ==
* [http://www.amandaformontana.com/ Official Site]
* [https://www.facebook.com/SupportAmandaCurtis Official Facebook]

{{s-start}}
{{s-par|us-mt-hs}}
{{s-bef | before=[[Jon Sesso]]}}
{{s-ttl | title=Member of the [[Montana House of Representatives]]<br>from the 76th district | years=2013–2015}}
{{s-aft | after=[[Ryan Lynch (politician)|Ryan Lynch]]}}
|-
{{s-ppo}}
{{s-bef | before=[[John Walsh (U.S. politician)|John Walsh]]<br>{{small|Withdrew}}}}
{{s-ttl | title={{nowrap|[[Democratic Party (United States)|Democratic]] nominee for [[List of United States Senators from Montana|U.S. Senator]] from [[Montana]]}}<br>([[Classes of United States Senators|Class 2]]) | years=[[United States Senate election in Montana, 2014|2014]]}}
{{s-inc|recent}}
{{s-end}}
{{Montana House of Representatives}}

{{DEFAULTSORT:Curtis, Amanda}}
[[Category:1979 births]]
[[Category:American schoolteachers]]
[[Category:Living people]]
[[Category:Members of the Montana House of Representatives]]
[[Category:Montana Democrats]]
[[Category:Montana Tech of the University of Montana alumni]]
[[Category:Politicians from Butte, Montana]]
[[Category:University of Montana Western alumni]]
[[Category:Video bloggers]]
[[Category:Women state legislators in Montana]]
[[Category:Educators from Montana]]
[[Category:21st-century educators]]
[[Category:21st-century American politicians]]
[[Category:21st-century women politicians]]