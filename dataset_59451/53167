{{Infobox journal
| title = Zeitschrift für Naturforschung C
| cover = [[File:Cover of Zeitschrift für Naturforschung C.jpg]]
| editor = Jürgen Seibel
| discipline = [[Biosciences]]
| formernames = 
| abbreviation = Z. Naturforsch. C Bio. Sci.
| publisher = [[Walter de Gruyter]]
| country =
| frequency = Bi-Monthly
| history = 1973&ndash;present
| openaccess =
| license =
| website = http://www.degruyter.com/view/j/znc
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 647943207
| LCCN = 88640783
| CODEN = ZNCBDA
| ISSN = 0939-5075
| eISSN = 
}}
'''''Zeitschrift für Naturforschung C''': A Journal of Biosciences'' is a monthly [[peer-reviewed]] [[scientific journal]]. It covers "all areas of [[animal physiology|animal]] and [[plant physiology]], all aspects of [[biochemistry]], [[neurobiology]], [[virology]] and [[molecular genetics]]".<ref>[http://www.degruyter.com/view/j/znc Zeitschrift für Naturforschung C - Information for Authors]</ref>Historically, contributions were accepted in either German or English, but the current requirement is for manuscripts to be written in English.

== History ==
The ''[[Zeitschrift für Naturforschung]]'' (English: ''Journal for Nature Research'') was established in 1946 and split into two parts ([[Zeitschrift für Naturforschung A|A]] and [[Zeitschrift für Naturforschung B|B]]) in 1947. Part C was established in 1973, removing coverage of the biosciences from part B. Since its establishment, the titles used for Part C have been:
* ''Zeitschrift für Naturforschung. Teil C, Biochemie, Biophysik, Biologie, Virologie'' ({{ISSN|0341-0471}}, 1973)
* ''Zeitschrift für Naturforschung. Section C, Biosciences'' ({{ISSN|0341-0382}}, 1974&ndash;1983)
* ''Zeitschrift für Naturforschung. Section C, A European Journal of Biosciences'' ({{ISSN|0341-0382}}, 1984&ndash;1985)
* ''Zeitschrift für Naturforschung. C, A Journal of Biosciences'' ({{ISSN|0939-5075}}, 1986&ndash;present)

== References ==
<references />

== External links ==
* {{Official website|http://www.degruyter.com/view/j/znc}}

{{DEFAULTSORT:Zeitschrift fur Naturforschung C}}
[[Category:Monthly journals]]
[[Category:Publications established in 1973]]
[[Category:English-language journals]]
[[Category:Biology journals]]
[[Category:Walter de Gruyter academic journals]]