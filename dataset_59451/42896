<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name= F8F Bearcat
  |image= File:F8F Bearcat (flying).jpg
  |caption=
}}{{Infobox Aircraft Type
  |type= [[Fighter aircraft]]
 |national origin= United States
  |manufacturer= [[Grumman]]
  |first flight= 21 August 1944
  |introduction= [[1945 in aviation|1945]]
  |retired= 1963 [[VNAF]] <ref>Grandolini, A. "Indo-Chinese fighting: 'Cats: Grumman's superb Bearcat in Vietnam." ''Air Enthusiast'', No. 70, July–August 1997, p. 21.</ref>
  |status= Retired
  |primary user= [[United States Navy]]
  |more users= [[United States Marine Corps]] <br>[[French Air Force]] <br>[[Royal Thai Air Force]]
  |produced=
  |number built= 1,265
  |unit cost=
  |developed from=
  |variants with their own articles=
}}
|}

The '''Grumman F8F Bearcat''' is an American single-engine [[Carrier-based aircraft|carrier-based]] [[fighter aircraft]] introduced in late [[World War II]]. It went on to serve into the mid-20th century in the [[United States Navy]], the [[United States Marine Corps]], and the air forces of other nations. It would be Grumman Aircraft's final [[piston engine]]d fighter aircraft. Modified versions have broken speed records for piston-engined aircraft, and are popular among [[warbird]] owners.

==Design and development==

===Concept===
The Bearcat concept began during a meeting between [[Battle of Midway]] veteran [[Grumman F4F Wildcat|F4F Wildcat]] pilots and [[Grumman]] Vice President [[Jake Swirbul]] at [[Pearl Harbor]] on 23 June 1942. At the meeting, Lieutenant Commander [[John Thach|Jimmie Thach]] emphasized one of the most important requirements in a good fighter plane was "climb rate".<ref>Ewing 2004, pp. 182, 308.</ref>

Climb performance is strongly related to the [[power-to-weight ratio]], and is maximized by wrapping the smallest and lightest possible airframe around the most powerful available engine. Another goal was that the '''G-58''' (Grumman's design designation for the aircraft) should be able to operate from [[escort carriers]], which were then limited to the obsolescent F4F Wildcat as the [[Grumman F6F Hellcat]] was too large and heavy. A small, lightweight aircraft would make this possible. After intensively analyzing [[Aircraft carrier|carrier]] warfare in the [[Asiatic-Pacific Theater|Pacific Theater of Operations]] for a year and a half, Grumman began development of the G-58 Bearcat in late 1943.

There is considerable debate among sources as to whether or not the [[Focke-Wulf Fw 190]] influenced the design of the G-58. It is known that test pilots from Grumman examined and flew a captured Fw 190 in England in early 1943, and the G-58 has a number of design notes in common with the Fw 190 that the Hellcat did not, especially in the cowling and landing gear arrangements. However, no definitive evidence has been presented that these tests had a direct input to the G-58 design.

===Design===
[[File:F6F-3 over California 1943.jpg|thumb|The Bearcat was heavily influenced by its larger cousin, the [[Grumman F6F Hellcat|F6F Hellcat]] (picture).]]

In 1943, Grumman was in the process of introducing the F6F Hellcat, powered by the [[Pratt & Whitney R-2800]] engine which provided {{convert|2000|hp}}. The R-2800 was the most powerful American engine available at that time, so it would be retained for the G-58. This meant that improved performance would have to come from a lighter airframe.

To meet this goal, the Bearcat's fuselage was about {{convert|5|ft}} shorter than the Hellcat, and was cut down vertically behind the cockpit area. This allowed the use of a [[bubble canopy]], the first to be fit to a US Navy fighter. The vertical stabilizer was the same height as the Hellcat's, but increased aspect ratio, giving it a thinner look. Similarly, the main wing had the same span, but having lower thickness, especially at the root. Structurally the fuselage used flush riveting as well as spot welding, with a heavy gauge 302W aluminum alloy skin suitable for carrier landings.<ref name="Scrivner 1990, p.4."/> Armor protection was provided for the pilot, engine and oil cooler.

The Hellcat used a 13&nbsp;ft 1&nbsp;in three-bladed [[Hamilton Standard]] propeller. A slight reduction in size was made by moving to a 12&nbsp;ft 7&nbsp;in Aeroproducts four-bladed propeller. Keeping the prop clear of the deck required long landing gear, which, combined with the shortened fuselage, gave the Bearcat a significant "nose-up" profile on land. The hydraulically operated undercarriage used an articulated [[trunnion]] which extended the length of the [[Oleo strut|oleo]] legs when lowered; as the undercarriage retracted the legs were shortened, enabling them to fit into a wheel well which was entirely in the wing. An additional benefit of the inward retracting units was a wide track, which helped counter propeller torque on takeoff and gave the F8F good ground and carrier deck handling.<ref name="Scrivner 1990, p.4.">Scrivner 1990, p. 4.</ref>

The design team had set the goal that the G-58 should weigh 8,750&nbsp;lb/3,969&nbsp;kg fully loaded. As development continued it became clear this was impossible to achieve as the structure of the new fighter had to be made strong enough for aircraft carrier landings. Ultimately much of the weight-saving measures included restricting the internal fuel capacity to 160&nbsp;gal (606&nbsp;l) <ref name="Scrivner 1990, p.4."/>(later 183)<ref>Scrivner 1990, p. 7.</ref> and limiting the fixed armament to four [[.50 cal]] [[M2 Browning machine gun|Browning M2/AN]] machine guns, two in each wing. The limited range due to the reduced fuel load would mean it would be useful in the [[interceptor aircraft|interception role]], but meant that the Hellcat would still be needed for longer range patrols. A later role was defending the fleet against airborne ''[[kamikaze]]'' attacks.<ref>[http://broadcast.illuminatedtech.com/display/story.cfm?bp=92&sid=7974 "F8F Bearcat."] ''U.S. Naval Air Museum''. Retrieved: 18 August 2010.</ref> Compared to the Hellcat, the Bearcat was 20% lighter, had a 30% better rate of climb and was 50&nbsp;mph (80&nbsp;km/h) faster.<ref>Swanborough and Bowers 1991, p. 241.</ref>

Another weight-saving concept the designers came up with was detachable wingtips. The wings were designed to fold at a point about {{frac|2|3}} out along the span, reducing the space taken up on the carrier. Normally the hinge system would have to be built very strong in order to transmit loads from the outer portions of the wing to the main spar in the inner section, which adds considerable weight. Instead of building the entire wing to be able to withstand high-g loads, only the inner portion of the wing was able to do this. The outer portions were more lightly constructed, and designed to snap off at the hinge line if the g-force exceeded 7.5&nbsp;g. In this case the aircraft would still be flyable and could be repaired after returning to the carrier. This saved {{convert|230|lb}} of weight.<ref>Meyer, Corwin W. [http://findarticles.com/p/articles/mi_qa3897/is_199808/ai_n8826530/pg_1 "Clipping the Bearcat's wing."] ''Flight Journal'', August 1998, p. 1. Retrieved: 18 August 2010.</ref>

[[File:Grumman XF8F-1 Bearcat 1945.jpg|thumb|right|An XF8F-1 prototype at the [[National Advisory Committee for Aeronautics|NACA]] Langley Research Facility in 1945.]]

===Prototypes===
The design was completed in November 1943 and an order for two prototypes was placed on 27 November 1943 under the BuAir designation XF8F-1. The first prototype flew on 21 August 1944, only nine months after the design effort started.<ref>Gunston 1988, p. 48.</ref><ref>Francillon 1989, p. 243.</ref>{{efn|One account states the first flight on 13 August.<ref>{{cite book |first=Richard |last=Thruelsen |title=The Grumman Story |publisher=Praeger |year=1976 |page=213}}</ref>}} The initial flight test demonstrated a {{convert|4,800|ft}} per minute climb rate and a top speed of {{convert|424|mph}}. Compared to the [[Vought F4U Corsair]], the Bearcat was marginally slower but more maneuverable and climbed more quickly.

Testing demonstrated a number of problems, notably a lack of horizontal stability, an underpowered trim system, landing gear that could only be extended at slow speeds, an unreliable airspeed indicator, and a cramped cockpit. The test pilots also requested that six guns be installed. The stability problem was addressed on the second prototype by adding a triangular fillet to the front of the vertical stabilizer. The extra guns could not be incorporated due to weight and balance considerations.

===Production===
The Navy placed a production contract for 2,023 aircraft based on the second prototype on 6 October 1944. On 5 February 1945 they awarded another contract for 1,876 slightly modified aircraft from [[General Motors]], given the designation F3M-1. These differed primarily in having the R-2800-34W engine and a small increase in fuel capacity.

Deliveries from Grumman began on 21 May 1945. The end of the war led to the Grumman order being reduced to 770 examples, and the GM contract being cancelled outright. An additional order was placed for 126 F8F-1B's replacing the .50 cal machine guns with the 20&nbsp;mm M2 cannon, the US version of the widely used [[Hispano-Suiza HS.404]]. Fifteen of these were later modified as F8F-1N night fighters with an APS-19 radar mounted under the starboard wing.

An unmodified production F8F-1 set a 1946 time-to-climb record (after a run of 115&nbsp;ft/35&nbsp;m) of 10,000&nbsp;ft (3,048&nbsp;m) in 94 seconds (6,383 fpm). The Bearcat held this record for 10 years until it was broken by a jet fighter (which still could not match the Bearcat's short takeoff distance).

In 1948 Grumman introduced a number of improvements to produce the F8F-2. Among the changes were a modified cowling design, taller vertical fin, and the slightly more powerful R-2800-30W engine producing {{convert|2,240|hp}}. A total of 293 F8F-2's were produced, along with 12 F8F-2N night fighters and 60 F8F-2P reconnaissance versions.

Production ended in 1949, and the first units began to convert off the type that year. The last Bearcats were withdrawn in 1952.

==Operational history==
[[File:Grumman F8F-1 Blue Angels 1946-49.jpg|thumb|right|On 25 August 1946, the [[Blue Angels]] transitioned to the Grumman F8F-1 Bearcat and introduced the famous "diamond" formation.]]
[[File:Blueangels BearcatF8F.jpg|thumb|A fully restored Bearcat in Blue Angels colors; seen at EAA AirVenture 2011]]

The F8F prototypes were ordered in November 1943 and first flew on 21 August 1944, a mere nine months later.{{efn|Grumman's project pilot for the Bearcat series was noted test pilot Corwin F. "Corky" Meyer.<ref>Dittmeier, Chris. [http://www.grummanpark.org/grumman_test_pilots.htm "Grumman test pilots."] ''www.GrummanPark.org.'' Retrieved: 18 August 2010.</ref><ref>Meyer, Corwin. ''Corky Meyer's Flight Journal: A Test Pilot's Tales Of Dodging Disasters-Just In Time''. North Branch, Minnesota: Specialty Press, 2006. ISBN 1-58007-093-0.</ref>|group=Note}} The first production aircraft was delivered in February 1945 and the first squadron, Fighter Squadron 19 ([[VF-191|VF-19]]), was operational by 21 May 1945, but [[World War II]] was over before the aircraft saw combat service.

One problem that became evident in service was the snap-off wing tips not working as expected. While they worked well under carefully controlled conditions in flight and on the ground, in the field, where aircraft were repetitively stressed by landing on carriers and since the wings were slightly less carefully made in the factories, there was a possibility that only one wingtip would break away with the possibility of the aircraft crashing.<ref>Scrivner 1990, p. 14.</ref> This was replaced with an explosives system to blow the wing tips off together, which also worked well, but this ended when a ground technician died due to an accidental triggering. In the end the wings were reinforced and the aircraft limited to 7.5 g.

Postwar, the F8F became a major U.S. Navy and U.S. Marine Corps fighter, equipping 24 fighter squadrons in the Navy and a smaller number in the Marines. Often mentioned as one of the best-handling piston-engine fighters ever built, its performance was sufficient to outperform many early jets.{{efn|[[Neil Armstrong]] had flown the type in 1950 during his navy advanced training, field qualifying in it at age 19. After his retirement as test pilot and astronaut, an interviewer asked him what was his favorite aircraft to fly. His immediate and unequivocal answer was "The Bearcat".<ref>Hanson 2005, p. 78.</ref>|group=Note}} Its capability for [[aerobatics|aerobatic]] performance is illustrated by its selection as the second demonstration aircraft for the navy's elite [[Blue Angels]] flight demonstration squadron in 1946, replacing the Grumman F6F Hellcat.<ref>https://www.blueangels.navy.mil/aircraft/historical.aspx</ref> The Blue Angels flew the Bearcat until the team was temporarily disbanded in 1950 during the [[Korean War]] and pressed into operational combat service. The [[Grumman F9F Panther|F9F Panther]] and [[McDonnell F2H Banshee]] largely replaced the Bearcat as their performance and other advantages eclipsed piston-engine fighters.

===First combat===
The first combat for the F8F Bearcat was during the [[French Indochina War]] (1946–1954) when nearly 200 Bearcats were delivered to the French forces in 1951. When the war ended in 1954, 28 surviving Bearcats were supplied to the [[Republic of Vietnam]] and entered service in 1956.<ref>Francillon 1989, pp. 252–253.</ref> The [[VNAF]] retired their remaining F8Fs in 1963, replacing them with [[Douglas A-1 Skyraider]]s and [[North American T-28 Trojan]]s as the [[Vietnam War]] (1957–1975) continued.<ref>Grandolini, A. "Indo-Chinese fighting: 'Cats: Grumman's superb Bearcat in Vietnam." Air Enthusiast, No. 70, July–August 1997, pp. 12–21.</ref> F8Fs were also supplied to Thailand during the same time period.<ref>[http://www.dailymotion.com/video/x1zi26_the-war-in-indochina-goes-on-121953_news "The war in Indo-China goes on."] ''The News Magazine of the Screen: Warner Pathé News,'' 12/1953. Retrieved: 18 August 2010.</ref><ref name="AI Jun93 p278-0">Manevy 1993, pp. 278–280.</ref><ref>[http://wp.scn.ru/en/ww3/f/802/17/0 "AVIA Camouflage Profiles: Grumman F8F Bearcat."] ''Wings Palette.'' Retrieved: 18 August 2010.</ref>

===Air racing===
[[File:Bearcat Grumman F8F-2 Rare Bear.jpg|thumb|right|Record-breaking [[Rare Bear]] racer]]

Bearcats have long been popular in [[air racing]]. A stock Bearcat flown by [[Mira Slovak]] and sponsored by [[Bill Stead (pilot)|Bill Stead]] won the first [[Reno Air Races|Reno Air Race]] in 1964. ''[[Rare Bear]]'', a highly modified F8F owned by [[Lyle Shelton]], went on to dominate the event for decades, often competing with [[Daryl Greenamyer]], another famous racer with victories in his own Bearcat (''Conquest I'', now at the Smithsonian's [[National Air and Space Museum|NASM]]) and holder of a propeller-driven aircraft world speed record in it. ''Rare Bear'' also set many performance records, including the 3&nbsp;km World Speed Record for piston-driven aircraft (528.33&nbsp;mph/850.26&nbsp;km/h), set in 1989, and a new time-to-climb record (3,000&nbsp;m in 91.9 seconds (6,425.9 fpm), set in 1972, breaking the 1946 record cited above).<ref>[http://www.RareBear.com "Lyle Shelton's "Rare Bear."] ''www.RareBear.com.'' Retrieved: 18 August 2010.</ref>{{efn|Note that Shelton's claim to be the "fastest propeller-driven aircraft in the world" does not acknowledge faster [[turboprop]] aircraft such as the Russian [[Tupolev Tu-95]] "Bear" bomber. Other sources credit "Rare Bear" as the fastest "piston-driven" aircraft.|group=Note}}<ref>[http://www.aerospaceweb.org/question/performance/q0023.shtml "Aircraft speed records."] ''www.AeroSpaceWeb.org.'' Retrieved: 18 August 2010.</ref><ref>[http://www.airrace.com/New%20speed%20records.htm "Speed records from archives of the Society of Air Racing Historians."] ''www.AirRace.com.'' Retrieved: 18 August 2010.</ref>

==Variants==
[[File:F8F-2 Bearcats of VF-111 on USS Valley Forge (CV-45) in September 1949 (80-G-427668).jpg|thumb|right|[[VF-111]] F8Fs aboard the {{USS|Valley Forge|CV-45|6}}]]
[[File:F8F-2P VC-62 CVB-41.jpg|thumb|An F8F-2P reconnaissance aircraft from [[VFP-62|VC-62]] over {{USS|Midway|CVB-41|6}}, 1949.]]
[[File:French F8F Bearcats at Tourane c1954.jpg|thumb|French Bearcats at [[Da Nang Air Base|Tourane Air Base]], circa 1954.]]
;XF8F-1
:Prototype aircraft, two built.

;F8F-1 Bearcat
:Single-seat fighter aircraft, equipped with folding wings, a retractable tailwheel, self-sealing fuel tanks, a very small dorsal fin, powered by a 2,100&nbsp;hp (1,566&nbsp;kW) Pratt & Whitney R-2800-34W Double Wasp radial piston engine, armed with four 0.50&nbsp;in (12.7&nbsp;mm) machine guns, 658 built.

;F8F-1B Bearcat
:Single-seat fighter version, armed with four AN/M3 20&nbsp;mm cannons, 100 built.

;F8F-1C Bearcat
:Originally designated F8F-1C, redesignated as F8F-1B, 126 built.

;F8F-1D
:F8F-1s converted into drone control aircraft.

;F8F-1(D)B Bearcat
:Unofficial designation for export version for France and Thailand.

;F8F-1E Bearcat
:F8F-1 night-fighter prototype carrying APS-4 radar.

;XF8F-1N
:F8F-1 conversion into night fighter prototypes.

;F8F-1N Bearcat
:Night fighter version, equipped with an APS-19 radar, 12 built.

;F8F-1P Bearcat
:F8F-1 conversion photo reconnaissance conversion.

;F3M-1 Bearcat
:Planned designation for F8F aircraft constructed by [[General Motors]].

;F4W-1 Bearcat
:Planned designation for F8F aircraft constructed by [[Canadian Car and Foundry]].<ref>Hardy 1987 {{page needed|date=May 2015}}</ref>

;XF8F-2
:F8F-1 conversion with engine upgrade, revised engine cowling, taller tail.

;F8F-2 Bearcat
:Improved version, equipped with a redesigned engine cowling, taller fin and rudder, armed with four 20&nbsp;mm cannons, powered by a Pratt & Whitney R-2800-30W radial piston engine, 293 built.

;F8F-2D
:F8F-2s converted into drone control aircraft.

;F8F-2N Bearcat
:Night-fighter version, equipped with an APS-19 radar, 12 built.

;F8F-2P Bearcat
:Photo-reconnaissance version, fitted with camera equipment, armed with two 20&nbsp;mm (.79&nbsp;in) cannons, 60 built.

;G-58A/B
:Two civil aircraft. The first was owned by the [[Gulf Oil Company]] for the use of Major Alford Williams, the second one was used by Grumman as a demonstrator aircraft and was flown by [[Roger Wolfe Kahn]].

==Operators==
;{{FRA}}
* [[French Air Force]]
;{{THA}}
* [[Royal Thai Air Force]]
;{{USA}}
* [[United States Navy]]
* [[United States Marine Corps]]
;{{flag|South Vietnam}}
* [[Vietnam Air Force]]

==Surviving aircraft==
[[File:Grumman Bearcat.jpg|thumb|right|Grumman F8F-2P Bearcat G-RUMM N700HL at Flying Legends, Duxford, UK]]

===Thailand===
;Airworthy
;;F8F-1
*122120 – Tango Squadron, Foundation for the Preservation and Development of Thai Aircraft.<ref>[http://www.thai-aviation.net/files/Tango_Squadron.pdf "Grumman F8F Bearcat/Bu. 122120."] ''Tango Squadron.'' Retrieved: 16 Dec 2013.</ref>
;On display
;;F8F-1
*94956 – [[Royal Thai Air Force Museum]] in [[Bangkok]].<ref>[http://www.rtaf.mi.th/museum/BLDG5-2.HTM#bearcat "Grumman F8F Bearcat/Bu. 94956."] ''Royal Thai Air Force Museum.'' Retrieved: 11 April 2012.</ref>

===United Kingdom===
;Airworthy
;;F8F-2
*121714 – [[The Fighter Collection]], Duxford.<ref>[http://fighter-collection.com/pages/aircraft/bearcat/index.php "Grumman F8F Bearcat/Bu. 121714."] ''The Fighter Collection Duxford.'' Retrieved: 11 April 2012.</ref>

===United States===
;Airworthy
;;F8F-1
*90454 – Jens Meyerhoff in [[Fountain Hills, Arizona]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=9G "FAA Registry: N9G."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*95255 – Lewis Air Legends in [[San Antonio, Texas]].<ref>[http://www.lewisairlegends.com/aircraft/f8f-1-bearcat "Grumman F8F Bearcat/Bu. 95255."] ''Lewis Air Legends.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=58204 "FAA Registry: N58204."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
;;F8F-1B
*122095 – Quality Leasing Company in [[Indianapolis, Indiana]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=2209 "FAA Registry: N2209."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
;;F8F-2
*121748 – Comanche Warbirds Inc. in [[Houston, Texas]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=1DF "FAA Registry: N1DF."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*121752 – Historic Flight Foundation in [[Mukilteo, Washington]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=800H "FAA Registry: N800H."] ''FAA.gov'' Retrieved: 17 May 2011.</ref><ref>[http://historicflight.org/hf/collection/bearcat/ "Grumman F8F Bearcat/Bu. 121752."] ''Historic Flight Foundation.'' Retrieved: 14 July 2014.</ref>
*121776 – BA1945 LLC in [[Wilmington, Delaware]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=N68RW "FAA Registry: N68RW."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*122614 – Lewis Air Legends in [[San Antonio, Texas]].<ref>[http://www.lewisairlegends.com/aircraft/tai-wun-on "Grumman F8F Bearcat/Bu. 122614."] ''Lewis Air Legends.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=747NF "FAA Registry: N747NF."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*122619 – Lewis Air Legends in [[San Antonio, Texas]].<ref>[http://www.lewisairlegends.com/aircraft/de-chrome-cat "Grumman F8F Bearcat/Bu. 122619."] ''Lewis Air Legends.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=14WB "FAA Registry: N14WB."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*[[Rare Bear|122629]] – Lewis Air Legends in [[San Antonio, Texas]].<ref>[http://www.lewisairlegends.com/aircraft/rare-bear "Grumman F8F Bearcat/Bu. 122629."] ''Lewis Air Legends.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=777L "FAA Registry: N777L."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*122637 – Comanche Warbirds Inc. in [[Houston, Texas]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=8TF "FAA Registry: N8TF."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*122674 – [[Commemorative Air Force]] (Southern California Wing) in [[Camarillo, California]].<ref>[http://cafsocal.com/bearcat.htm "Grumman F8F Bearcat/Bu. 122674."] ''CAF Southern California Wing.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=7825C "FAA Registry: N7825C."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
;;G-58 Gulfhawk (two civilian built Bearcats)
*G-58A – Steven Hinton in [[Chino, California]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=3025 "FAA Registry: N3025."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
*G-58B – [[Palm Springs Air Museum]] in [[Palm Springs, California]].<ref>[http://www.palmspringsairmuseum.org/aircrafts.htm "Grumman G-58B Gulfhawk."] ''Palm Springs Air Museum.'' Retrieved: 23 February 2014.</ref><ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=N700A "FAA Registry: N700A."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
;On display
;;F8F-2
*121646 – [[Steven F. Udvar-Hazy Center]] of the [[National Air and Space Museum]] in [[Chantilly, Virginia]].<ref>[http://www.nasm.si.edu/collections/artifact.cfm?id=A19770989000 "Grumman F8F Bearcat/Bu. 121646."] ''National Air & Space Museum.'' Retrieved: 11 April 2012.</ref>
;;F8F-2P
*121710 – [[National Naval Aviation Museum]] at [[NAS Pensacola]], [[Florida]].<ref>[http://www.navalaviationmuseum.org/attractions/aircraft-exhibits/item/?item=f8f_bearcat "Grumman F8F Bearcat/Bu. 121710."] ''National Naval Aviation Museum.'' Retrieved: 11 April 2012.</ref>
;Under Restoration
;;F8F-1
*90446 – to airworthiness by Texas Bearcats LLC in [[Dover, Delaware]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=14HP "FAA Registry: N14HP."] ''FAA.gov'' Retrieved: 26 August 2014.</ref>
*95356 – to airworthiness by Texas Bearcats LLC in [[Dover, Delaware]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=4752Y "FAA Registry: N4752Y."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>
;;F8F-2
*121679 – to airworthiness by Bearcat F8F-2 LLC in [[Hayward, California]].<ref>[http://registry.faa.gov/aircraftinquiry/NNum_Results.aspx?NNumbertxt=818F "FAA Registry: N818F."] ''FAA.gov'' Retrieved: 17 May 2011.</ref>

==Specifications==
[[File:Grumman F8F-2 Bearcat BuAer drawings 1949.PNG|right|thumb|300px|F8F-2 Bearcat]]

===F8F-1===
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=prop
|ref=Jane's Fighting Aircraft of World War II,<ref name="Maloney">Maloney 1969</ref>{{page needed|date=July 2012}}<ref name=jane>Bridgman 1946, p. 233.</ref>
|crew=1 pilot
|length main=28 ft 3 in
|length alt=8.61 m
|span main=35 ft 10 in
|span alt=10.92 m
|height main=13 ft 9 in
|height alt=4.21 m
|area main= 244 ft²<ref name="Swan Navy p225">Swanborough and Bowers 1991, p. 243.</ref>
|area alt= 22.67 m²
|empty weight main=7,070 lb
|empty weight alt=3,207 kg
|loaded weight main=9,600 lb
|loaded weight alt=4,354 kg
|max takeoff weight main=12,947 lb
|max takeoff weight alt=5,873 kg
|engine (prop)=[[Pratt & Whitney R-2800]]-34W "Double Wasp"
|type of prop=two-row [[radial engine]]
|number of props=1
|power main=2,300 hp<ref>[http://www.ww2aircraft.net/forum/engines/r-2800-34-vs-34w-41019.html "Thread: R-2800-34 vs. 34w."] ''ww2aircraft.net/forum''. Retrieved: 4 December 2014.</ref><ref>Dean, Francis H.[http://www.ww2aircraft.net/forum/engines/terminology-engine-data-36560-3.html#post1006503 "Engine charts."] ''America's Hundred Thousand''. New York: Schiffer, 1997. ISBN 978-0-76430-072-1.</ref>
|power alt=1,715 kW
|max speed main=421 mph
|max speed alt=366 kn, 678 km/h
|range main=1,105 mi
|range alt=1,778 km
|ceiling main=38,700 ft
|ceiling alt=11,796 m
|climb rate main=4,570 ft/min
|climb rate alt=23.2 m/s
|loading main=39.3 lb/ft²
|loading alt=192.1 kg/m²
|power/mass main=0.22 hp/lb
|power/mass alt=360 W/kg
|guns=4 × 0.50 in (12.7 mm) Browning M2 machine guns (F8F-1 and F8F-1N); Four 20mm [[Hispano-Suiza HS.404|AN/M3 cannon]] (F8F-1B)
|bombs=1,000 lb (454 kg) bombs
|rockets=4 × 5 in (127 mm) unguided rockets
}}

===F8F-2===
{{aircraft specifications
|ref=F8F Bearcat in Action<ref name="Squadron/Signal">Scrivner 1990, p. 31.</ref>
|plane or copter?=plane
|jet or prop?=prop
|length main=28 ft 3 in
|length alt=8.61 m
|span main=35 ft 10 in
|span alt=10.92 m
|height main=13 ft 10 in
|height alt=4.21 m
|empty weight main= 7,650 lb
|empty weight alt=3,207 kg
|loaded weight main=10,200 lb
|loaded weight alt=4,627 kg
|max takeoff weight main=13,460 lb
|max takeoff weight alt=6,105 kg
|engine (prop)=[[Pratt & Whitney R-2800]]-30W
|type of prop=two-row [[radial engine]]
|number of props=1
|power main=2,250 hp
|power alt=1,678 kW
|max speed main=455 mph
|max speed alt=405 kn, 730 km/h
|range main=1,105 mi
|range alt=1,778 km
|ceiling main=40,800 ft
|ceiling alt=12,436 m
|climb rate main=4,465 ft/min
|climb rate alt=23.2 m/s
|loading main=<!-- lb/ft²-->
|loading alt=<!-- kg/m²-->
|power/mass main=0.22 hp/lb
|power/mass alt=360 W/kg
|guns=4 × 20 mm (.79 in) [[Hispano-Suiza HS.404|AN/M3 cannon]]
|bombs=1,000 lb (454 kg) bombs
|rockets=4 × 5 in (127 mm) unguided rockets
}}

==See also==
{{aircontent
|related=
* [[Grumman F6F Hellcat]]
|similar aircraft=
* [[Focke-Wulf Ta 152]]
* [[Hawker Sea Fury]]
* [[Supermarine Seafang]]
* [[Mitsubishi A7M]]
* [[Nakajima Ki-84]]
* [[Vought F4U Corsair]]
|lists=
* [[List of fighter aircraft]]
* [[List of aircraft of World War II]]
* [[List of military aircraft of the United States]]
* [[List of United States naval aircraft]]
|see also=
}}

==References==

===Notes===
{{notelist}}

===Citations===
{{Reflist|40eh}}

===Bibliography===
{{Refbegin}}
* Andrews, Hal. ''The Grumman F8F Bearcat'' (Aircraft in profile 107). Windsor, Berkshire, UK: Profile Publications Ltd., 1972 (reprinted from 1966).
* Bridgman, Leonard. "The Grumman Bearcat". ''Jane’s Fighting Aircraft of World War II''. London: Studio, 1946. ISBN 1-85170-493-0.
* [[Eric Brown (pilot)|Brown, Eric]]. "Last of the Wartime 'Cats{{'-}}". ''[[Air International]]'', Vol. 18, No. 5, May 1980. Stamford, UK: Key Publishing. {{issn|0306-5634}}.
* Chant, Christopher. ''Grumman F8F Bearcat: Super Profile''. Sparkford, Yeovil, UK: Haynes Publishing, 1985. ISBN 0-85429-447-3.
* Drendel, Lou. ''U.S. Navy Carrier Fighters of World War II''. Carrollton, TX: Squadron/Signal Publications Inc., 1987. ISBN 0-89747-194-6.
* Ewing, Steve. ''Thach Weave: The Life Jimmie Thach''. Annapolis, Maryland: Naval Institute Press. 2004. ISBN 1-59114-248-2.
* Francillon, Rene J. ''Grumman Aircraft Since 1929''. Annapolis, Maryland: Naval Institute Press, 1989. ISBN 0-87021-246-X.
* [[William Green (author)|Green, William]]. "Grumman F8F-1 Bearcat". ''War Planes of the Second World War, Volume Four: Fighters''. London: Macdonald & Co. (Publishers) Ltd., 1961, pp.&nbsp;109–111. ISBN 0-356-01448-7.
* [[William Green (author)|Green, William]] and Gordon Swanborough. "Grumman F8F Bearcat". ''WW2 Fact Files: US Navy and Marine Corps Fighters''. London: Macdonald and Jane's Publishers Ltd., 1976, pp.&nbsp;62–63. ISBN 0-356-08222-9.
* Gunston, Bill. ''Grumman: Sixty Years of Excellence''. London: Orion Books, 1988. ISBN 1-55750-991-3.
* Hanson, James R. ''First Man: The Life of Neil A. Armstrong''. New York: Simon & Schuster, 2005. ISBN 0-7432-5751-0.
* Hardy, M. J. ''Sea, Sky and Stars: An Illustrated History of Grumman Aircraft''. London: Arms and Armour Press, 1987. ISBN 978-0-85368-832-7.
* Maloney, Edward T. ''Grumman F8F Bearcat'' (Aero Series Vol. 20). Fallbrook, California: Aero Publishers, 1969. ISBN 0-8168-0576-8.
* Manevy, Jean Christophe. "French Bearcats in Indo-China 1951–1954". ''[[Air International]]'', Vol. 44, No. 6, June 1993, pp.&nbsp;278–280. Stamford, UK: Key Publishing. {{issn|0306-5634}}.
* Meyer, "Corky". "Clipping the Bearcat's Wing". ''Flight Journal'', Vol. 3, No. 4, August 1998.
* Morgan, Eric B. "Grumman's Hot Rod". ''Twenty-first Profile, Volume 1, no. 12''. New Milton, Hantfordshire, UK: Profile Publications, 1972. {{issn|0961-8120}}.
* Morgan, Eric B. "Grumman Bearcat part II". ''Twenty-first Profile, Volume 2, no. 17''. New Milton, Hantfordshire, UK: Profile Publications, 1972. {{issn|0961-8120}}.
* O'Leary, Michael. ''United States Naval Fighters of World War II in Action''. Poole, Dorset, UK: Blandford Press, 1980. ISBN 0-7137-0956-1.
* Scrivner, Charles L. ''F8F Bearcat in Action'' (Aircraft Number 99). Carrollton, Texas: Squadron/Signal Publications Inc., 1990. ISBN 0-89747-243-8.
* Swanborough, Gordon and Peter M. Bowers. ''United States Navy Aircraft since 1911''. Annapolis, Maryland: Naval Institute Press 1991, pp.&nbsp;241–243. ISBN 0-87021-792-5.
* Taylor, John W.R. "Grumman F8F Bearcat". ''Combat Aircraft of the World from 1909 to the Present''. New York: G.P. Putnam's Sons, 1969. ISBN 0-425-03633-2.
{{Refend}}

==External links==
{{commons category}}
* [http://www.theaviationindex.com/aircraft-types/grumman-f8f-bearcat Grumman F8F Bearcat articles and publications]
* [http://www.warbirdalley.com/bearcat.htm Warbird Alley: Bearcat page – Information about Bearcats still flying today]
* [http://www.scribd.com/doc/81685318 AN 01-85FD-1 Pilot's Handbook for Navy Models F8F-1, F8F-1B, F8F-1N, F8F-2, F8F-2N, F8F-2P Aircraft (1949)]
* [http://toniosky7.blogspot.fr/2012/08/grumman-f8f-bearcat.html Pictures from the Grumman archive]

{{Grumman aircraft}}
{{General Motors aircraft}}
{{USN fighters}}

{{DEFAULTSORT:Grumman F08F Bearcat}}
[[Category:Grumman aircraft|F08F Bearcat]]
[[Category:United States fighter aircraft 1940–1949|Grumman F8F]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Carrier-based aircraft]]
[[Category:Racing aircraft]]