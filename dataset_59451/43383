<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Warrior
 | image=File:Pawnee Warrior Prototype in flight.jpg
 | caption=The Pawnee Warrior prototype in flight
}}{{Infobox Aircraft Type
 | type=[[Helicopter]]
 | national origin=[[United States]]
 | manufacturer=[[Pawnee Aviation]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=Probably just one prototype
 | program cost= <!--Total program cost-->
 | unit cost= [[US$]]15,500 (kit, less engine, 1998)
 | developed from= 
 | variants with their own articles=[[Pawnee Chief]]
}}
|}
The '''Pawnee Warrior''' was an [[United States|American]] [[helicopter]] that was designed and produced by [[Pawnee Aviation]] of [[Longmont, Colorado]]. Now out of production, when it was available the aircraft was supplied as a kit for [[Homebuilt aircraft|amateur construction]].<ref name="Aerocrafter">Purdy, Don: ''AeroCrafter - Homebuilt Aircraft Sourcebook, Fifth Edition'', page 327. BAI Communications, 15 July 1998. ISBN 0-9636409-4-1</ref>

==Design and development==
The Warrior was designed to comply with the US ''Experimental - Amateur-built'' aircraft rules. It featured a single {{convert|21|ft|m|1|abbr=on}} diameter two-bladed main rotor, a two-bladed tail rotor, both made from single metal [[extrusion]]s. The kit's drive components were intended to be supplied complete and ready to install. The aircraft had a single-seat open cockpit without a windshield and skid-type [[landing gear]]. A cockpit enclosure was optional. The standard engine used was a twin cylinder, air-cooled, [[two-stroke]], [[dual-ignition]] {{convert|65|hp|kW|0|abbr=on}} [[Hirth 2706]] powerplant.<ref name="Aerocrafter" />

The aircraft [[fuselage]] was made from [[steel]] and [[aluminum]] tubing and supplied in three major bolt-together sub-assemblies. It had an empty weight of {{convert|437|lb|kg|0|abbr=on}} and a gross weight of {{convert|850|lb|kg|0|abbr=on}}, giving a useful load of {{convert|413|lb|kg|0|abbr=on}}. With full fuel of {{convert|14|u.s.gal}} the payload for the pilot and baggage was {{convert|329|lb|kg|0|abbr=on}}.<ref name="Aerocrafter" />

The manufacturer estimated the construction time from the planned assembly kit as 80&nbsp;hours.<ref name="Aerocrafter" />

The company said "the Warrior was used [as] a proof of concept platform to develop new models" and was followed by the two place [[Pawnee Chief]].<ref name="FAQ">{{cite web|url = http://www.pawneeaviation.net/frequently_asked_questions.htm|title = What happened to the "Warrior" helicopter?|accessdate = 1 April 2015|author = Pawnee Aviation|date =  |archiveurl = https://web.archive.org/web/20021013081601/http://www.pawneeaviation.net/frequently_asked_questions.htm |archivedate = 13 October 2002}}</ref>

==Operational history==
By 1998 the company reported that one aircraft had been completed and was flying.<ref name="Aerocrafter" />

By April 2015 no examples were [[Aircraft registration|registered]] in the United States with the [[Federal Aviation Administration]] and it is likely that no examples exist any more.<ref name="FAAReg">{{cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=WARRIOR&PageNo=1|title = Make / Model Inquiry Results|accessdate = 1 April 2015|last = [[Federal Aviation Administration]]|date = 1 April 2015}}</ref>
<!-- ==Variants== -->
<!-- ==Aircraft on display== -->

==Specifications (Warrior) ==
{{Aircraft specs
|ref=Purdy<ref name="Aerocrafter" />
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=15
|length in=0
|length note=
|width m=
|width ft=
|width in=
|width note=
|height m=
|height ft=
|height in=
|height note=
|airfoil=
|empty weight kg=
|empty weight lb=437
|empty weight note=
|gross weight kg=
|gross weight lb=850
|gross weight note=
|fuel capacity={{convert|14|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Hirth 2706]]
|eng1 type=twin cylinder, air-cooled, [[two stroke]] [[aircraft engine]]
|eng1 kw=
|eng1 hp=65

|rot number=1
|rot dia m=
|rot dia ft=21
|rot dia in=0
|rot area sqm=
|rot area sqft=346
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=100
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=80
|cruise speed kts=
|cruise speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=180
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=10000
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|disk loading kg/m2=
|disk loading lb/sqft=2.5
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

==See also==
*[[List of rotorcraft]]

==References==
{{reflist}}

<!-- ==External links== -->
{{Pawnee Aviation aircraft}}

[[Category:Pawnee Aviation aircraft|Warrior]]
[[Category:United States sport aircraft 1990–1999]]
[[Category:United States helicopters 1990–1999]]
[[Category:Homebuilt aircraft]]
[[Category:Single-engine aircraft]]