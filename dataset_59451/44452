{{Use dmy dates|date=March 2012}}

[[File:DotNet.svg|right|300px|alt=.NET Framework stack]]
Microsoft started development on the [[.NET Framework]] in the late 1990s originally under the name of Next Generation Windows Services (NGWS). By late 2001 the first beta versions of .NET 1.0 were released.<ref>{{cite web|url=http://ben.skyiv.com/clrversion.html |title=Framework Versions |archiveurl=https://web.archive.org/web/20080504160116/http://ben.skyiv.com/clrversion.html |archivedate=4 May 2008 |deadurl=no |df=dmy }}</ref> The first version of .NET Framework was released on 13 February 2002, bringing [[managed code]] to [[Windows NT 4.0]], [[Windows 98|98]], [[Windows 2000|2000]], [[Windows ME|ME]] and [[Windows XP|XP]].

Since the first version, Microsoft has released nine more upgrades for .NET Framework, seven of which have been released along with a new version of [[Visual Studio]]. Two of these upgrades, .NET Framework 2.0 and 4.0, have upgraded [[Common Language Runtime]] (CLR). New versions of .NET Framework replace older versions when the CLR version is the same.

The .NET Framework family also includes two versions for [[Mobile computing|mobile]] or [[Embedded device]] use. A reduced version of the framework, the [[.NET Compact Framework]], is available on [[Windows CE]] platforms, including [[Windows Mobile]] devices such as [[smartphones]]. Additionally, the [[.NET Micro Framework]] is targeted at severely resource-constrained devices.

==Overview==
{{.NET Framework version history}}

==.NET Framework 1.0==

The first version of the .NET Framework was released on 13 February 2002 for [[Windows 98]], [[Windows ME|ME]], [[Windows NT 4.0|NT 4.0]], [[Windows 2000|2000]], and [[Windows XP|XP]]. Mainstream support for this version ended on 10 July 2007, and extended support ended on 14 July 2009, with the exception of [[Windows XP Media Center Edition|Windows XP Media Center]] and [[Windows XP Tablet PC Edition|Tablet PC]] editions.<ref name="MS_DotNET_EoL">{{cite web |url=http://support.microsoft.com/lifecycle/search/?sort=PNα=.NET+Framework |title=Microsoft Product Lifecycle Search |work=Microsoft |accessdate=25 January 2008|archiveurl=http://www.webcitation.org/5jZfjU0F1?url=http://support.microsoft.com/lifecycle/search/?sort%3DPN%26alpha%3D.NET%2BFramework|archivedate=6 September 2009|deadurl=no}}</ref>

On 19 July 2001, the tenth anniversary of the release of Visual Basic, .NET Framework 1.0 Beta 2 was released.<ref>{{Cite web|url=http://betanews.com/2001/12/05/gates-revises-visual-studio-net-release-date/|title=Gates Revises Visual Studio .NET Release Date|date=2001-12-05|website=BetaNews|access-date=2016-07-01}}</ref>

.NET Framework 1.0 is supported on [[Windows 98]], [[Windows ME|ME]], [[Windows NT 4.0|NT 4.0]], [[Windows 2000|2000]], [[Windows XP|XP]], and [[Windows Server 2003|Server 2003]].  Applications utilizing .NET Framework 1.0 will also run on computers with .NET Framework 1.1 installed, which supports additional operating systems.<ref name="netdepends1">{{Cite web|title=.NET Framework System Requirements|url=https://msdn.microsoft.com/en-us/library/8z6watww%28v=vs.90%29.aspx|work=[[MSDN]]|publisher=[[Microsoft]]|accessdate=28 November 2016}}</ref>

==.NET Framework 1.1==
Version 1.1 is the first minor .NET Framework upgrade. It is available on its own as a [[Freely redistributable software|redistributable package]] or in a [[software development kit]], and was published on 3 April 2003. It is also part of the second release of  [[Visual Studio .NET 2003]]. This is the first version of the .NET Framework to be included as part of the Windows operating system, shipping with [[Windows Server 2003]]. Mainstream support for .NET Framework 1.1 ended on 14 October 2008, and extended support ended on 8 October 2013.  .NET Framework 1.1 is the last version to support [[Windows NT 4.0]].

Installing .NET Framework 1.1 also provides the system support for version 1.0, except in rare instances where an application will not run because it checks the version number of a library.<ref>{{cite web|title=.NET Framework Developer Center – Frequently Asked Questions |url=http://msdn.microsoft.com/en-us/netframework/Aa497323.aspx |archiveurl=https://web.archive.org/web/20120724012426/http://msdn.microsoft.com/en-us/netframework/aa497323.aspx |archivedate=24 July 2012 |deadurl=no |df=dmy }}</ref>

Changes in 1.1 include:<ref>{{cite web|title=New and Enhanced Features|url=https://msdn.microsoft.com/en-us/library/h88tthh0(v=vs.71).aspx|website=[[MSDN]]|publisher=[[Microsoft]]|archiveurl=https://web.archive.org/web/20110127162056/http://msdn.microsoft.com/en-us/library/h88tthh0(v=vs.71).aspx|archivedate=27 January 2011 |deadurl=no}}</ref>
* Built-in support for mobile [[ASP.NET]] controls, which was previously available as an add-on
* Enables Windows Forms assemblies to execute in a semi-trusted manner from the Internet
* Enables [[Code Access Security]] in ASP.NET applications
* Built-in support for [[ODBC]] and [[Oracle Database]], which was previously available as an add-on
* [[.NET Compact Framework]], a version of the .NET Framework for small devices
* Internet Protocol version 6 ([[IPv6]]) support

.NET Framework 1.1 is supported on [[Windows 98]], [[Windows ME|ME]], [[Windows NT 4.0|NT 4.0]], [[Windows 2000|2000]], [[Windows XP|XP]], [[Windows Server 2003|Server 2003]], [[Windows Vista|Vista]], and [[Windows Server 2008|Server 2008]].<ref name="netdepends1"/><ref>{{cite web|title=.NET Framework 1.1 Redistributable|url=https://www.microsoft.com/en-us/download/details.aspx?id=26|website=[[MSDN]]|publisher=[[Microsoft]]}}</ref>

==.NET Framework 2.0==
Version 2.0 was released on 22 January 2006. It was also released along with [[Visual Studio 2005]], [[Microsoft SQL Server 2005]], and [[BizTalk]] 2006. A software development kit for this version was released on 29 November 2006.<ref>{{Cite web|url = http://www.microsoft.com/en-us/download/details.aspx?id=19988|title = .NET Framework 2.0 Software Development Kit (SDK) (x86)|date = 29 November 2006|accessdate = |website = Downloads|publisher = [[Microsoft]]|last = |first = }}</ref> It was the last version to support [[Windows 98]] and [[Windows Me]].<ref>{{cite web |url = http://msdn.microsoft.com/library/cc160717.aspx|title = Microsoft .NET Framework 3.5 Administrator Deployment Guide|work = [[MSDN]]|publisher = [[Microsoft]]|accessdate = 26 June 2008}}</ref>

.NET Framework 2.0 with Service Pack 2 requires Windows 2000 with SP4 plus KB835732 or KB891861 update, [[Windows XP]] with SP2 plus [[Windows Installer]] 3.1. It is the last version to support [[Windows 2000]] although there have been some unofficial workarounds to use a subset of the functionality from Version 3.5 in Windows 2000.<ref>{{cite web|title=Microsoft .NET Framework 3.5 in Windows 2000 |url=http://rainstorms.me.uk/blog/2008/03/12/microsoft-net-framework-35-in-windows-2000/ |archiveurl=http://www.webcitation.org/62ENcPdNi?url=http%3A%2F%2Frainstorms.me.uk%2Fblog%2F2008%2F03%2F12%2Fmicrosoft-net-framework-35-in-windows-2000%2F |archivedate=6 October 2011 |accessdate=6 October 2011 |deadurl=no |df=dmy }}</ref>

Changes in 2.0 include:
* Full [[64-bit|64-bit computing]] support for both the [[x64]] and the [[IA-64]] hardware platforms
* Numerous API changes{{which|date=September 2014}}
* [[Microsoft SQL Server]] integration: Instead of using [[Transact-SQL|T-SQL]], one can build [[stored procedures]] and triggers in any of the .NET-compatible languages
* A new hosting API for native applications wishing to host an instance of the .NET runtime: The new API gives a fine grain control on the behavior of the runtime with regards to [[Multithreading (computer architecture)|multithreading]], memory allocation and assembly loading. It was initially developed to efficiently host the runtime in [[Microsoft SQL Server]], which implements its own scheduler and memory manager.
* Many additional and improved ASP.NET web controls{{which|date=September 2014}}
* New data controls{{which|date=September 2014}} with declarative data binding
* New personalization features for [[ASP.NET]], such as support for themes, skins, master pages and webparts
* [[.NET Micro Framework]], a version of the .NET Framework related to the [[Smart Personal Objects Technology]] initiative
* Membership provider
* [[Partial class]]es
* [[Nullable type]]s
* [[Anonymous method]]s
* [[Iterator]]s
* Data tables
* [[Common Language Runtime]] (CLR) 2.0
* Language support for [[Generic programming|generics]] built directly into the .NET [[Common Language Runtime|CLR]]

.NET Framework 2.0 is supported on [[Windows 98]], [[Windows ME|ME]], [[Windows 2000|2000]], [[Windows XP|XP]], [[Windows Server 2003|Server 2003]], [[Windows Vista|Vista]], [[Windows Server 2008|Server 2008]], and [[Windows Server 2008 R2|Server 2008 R2]].<ref name="depend"/>  Applications utilizing .NET Framework 2.0 will also run on computers with .NET Framework 3.0 or 3.5 installed, which supports additional operating systems.

==.NET Framework 3.0==
.NET Framework 3.0, formerly called WinFX,<ref>[http://blogs.msdn.com/somasegar/archive/2006/06/09/624300.aspx WinFX name change announcement] {{webarchive |url=http://www.webcitation.org/5PlRtTPmO?url=http://blogs.msdn.com/somasegar/archive/2006/06/09/624300.aspx |date=21 June 2007 }}</ref> was released on 21 November 2006. It includes a new set of [[managed code]] APIs that are an integral part of [[Windows Vista]] and [[Windows Server 2008]]. It is also available for [[Windows XP]] SP2 and [[Windows Server 2003]] as a download. There are no major architectural changes included with this release; .NET Framework 3.0 uses the same [[Common Language Runtime|CLR]] as .NET Framework 2.0.<ref>{{cite web |url=http://msdn.microsoft.com/netframework/aa663314.aspx |title=.NET Framework 3.0 Versioning and Deployment Q&A |accessdate=1 June 2008}}</ref> Unlike the previous major .NET releases there was no .NET Compact Framework release made as a counterpart of this version.  Version 3.0 of the .NET Framework shipped with Windows Vista.  It also shipped with Windows Server 2008 as an optional component (disabled by default).

.NET Framework 3.0 consists of four major new components:
* [[Windows Presentation Foundation]] (WPF), formerly code-named Avalon: A new [[user interface]] subsystem and [[Application programming interface|API]] based on [[XAML]] markup language, which uses [[3D computer graphics]] hardware and [[Direct3D]] technologies<ref>{{Cite web|url = http://msdn.microsoft.com/en-us/library/ms754130.aspx|title = Windows Presentation Foundation|date = |accessdate = 1 September 2014|website = [[MSDN]]|publisher = [[Microsoft]]|last = |first = }}</ref>
* [[Windows Communication Foundation]] (WCF), formerly code-named Indigo: A service-oriented messaging system which allows programs to interoperate locally or remotely similar to [[web service]]s
* [[Windows Workflow Foundation]] (WF): Allows building task automation and integrated transactions using [[workflow]]s
* [[Windows CardSpace]], formerly code-named InfoCard: A software component which securely stores a person's digital identities and provides a unified [[interface (computing)|interface]] for choosing the identity for a particular transaction, such as logging into a website

.NET Framework 3.0 is supported on [[Windows XP]], [[Windows Server 2003|Server 2003]], [[Windows Vista|Vista]], [[Windows Server 2008|Server 2008]], and [[Windows Server 2008 R2|Server 2008 R2]].<ref name="depend"/>  Applications utilizing .NET Framework 3.0 will also run on computers with .NET Framework 3.5 installed, which supports additional operating systems.

==.NET Framework 3.5==
Version 3.5 of the .NET Framework was released on 19 November 2007. As with .NET Framework 3.0, version 3.5 uses Common Language Runtime (CLR) 2.0, that is, the same version as .NET Framework version 2.0. In addition, .NET Framework 3.5 also installs .NET Framework 2.0 SP1 and 3.0 SP1 (with the later 3.5 SP1 instead installing 2.0 SP2 and 3.0 SP2), which adds some methods and properties to the BCL classes in version 2.0 which are required for version 3.5 features such as [[Language Integrated Query|Language Integrated Query (LINQ)]]. These changes do not affect applications written for version 2.0, however.<ref>{{cite web|url=http://www.hanselman.com/blog/CommentView.aspx?guid=7cd75505-192f-4fef-b617-e47e1e2cb94b |title=Catching RedBits differences in .NET 2.0 and .NET 2.0SP1 |accessdate=1 June 2008 |archiveurl=https://web.archive.org/web/20080430003858/http://www.hanselman.com/blog/CommentView.aspx?guid=7cd75505-192f-4fef-b617-e47e1e2cb94b |archivedate=30 April 2008 |deadurl=no |df=dmy }}</ref>

As with previous versions, a new .NET Compact Framework 3.5 was released in tandem with this update in order to provide support for additional features on Windows Mobile and [[Windows Embedded CE]] devices.

The source code of the [[Framework Class Library]] in this version has been partially released (for debugging reference only) under the [[Microsoft Reference License#Microsoft Reference Source License (Ms-RSL)|Microsoft Reference Source License]].<ref name="sourcerelease">{{cite web|url=http://weblogs.asp.net/scottgu/archive/2007/10/03/releasing-the-source-code-for-the-net-framework-libraries.aspx |title=Releasing the Source Code for the NET Framework |author=[[Scott Guthrie]] |date=3 October 2007 |accessdate=15 September 2010 |archiveurl=https://web.archive.org/web/20100907233621/http://weblogs.asp.net/scottgu/archive/2007/10/03/releasing-the-source-code-for-the-net-framework-libraries.aspx |archivedate=7 September 2010 |deadurl=no |df=dmy }}</ref>

.NET Framework 3.5 is supported on [[Windows XP]], [[Windows Server 2003|Server 2003]], [[Windows Vista|Vista]], [[Windows Server 2008|Server 2008]], [[Windows 7|7]], [[Windows Server 2008 R2|Server 2008 R2]], [[Windows 8|8]], [[Windows Server 2012|Server 2012]], [[Windows 8.1|8.1]], [[Windows Server 2012 R2|Server 2012 R2]], [[Windows 10|10]], and [[Windows Server 2016|Server 2016]].<ref name="depend"/>

===Service Pack 1 {{Anchor|.NET Framework 3.5 SP1}}===
The .NET Framework 3.5 Service Pack 1 was released on 11 August 2008. This release adds new functionality and provides performance improvements under certain conditions,<ref>{{cite web|url=http://msdn.microsoft.com/vstudio/products/cc533447.aspx |title=Visual Studio 2008 Service Pack 1 and .NET Framework 3.5 Service Pack 1 |accessdate=7 September 2008 |archiveurl=https://web.archive.org/web/20080708235537/http://msdn.microsoft.com/vstudio/products/cc533447.aspx |archivedate=8 July 2008 |deadurl=no |df=dmy }}</ref> especially with WPF where 20–45% improvements are expected. Two new data service components have been added, the [[ADO.NET Entity Framework]] and [[ADO.NET Data Services]]. Two new assemblies for web development, System.Web{{Not a typo|.}}Abstraction and System.Web{{Not a typo|.}}Routing, have been added; these are used in the [[ASP.NET MVC]] framework and, reportedly, will be used in the future release of ASP.NET Forms applications. Service Pack 1 is included with [[SQL Server 2008]] and [[Visual Studio 2008|Visual Studio 2008 Service Pack 1]].  It also featured a new set of controls called "Visual Basic Power Packs" which brought back Visual Basic controls such as "Line" and "Shape".  Version 3.5 SP1 of the .NET Framework shipped with Windows 7.  It also shipped with Windows Server 2008 R2 as an optional component (disabled by default).

====.NET Framework 3.5 SP1 Client Profile====
For the .NET Framework 3.5 SP1 there is also a new variant of the .NET Framework, called the ".NET Framework Client Profile", which at 28&nbsp;MB is significantly smaller than the full framework and only installs components that are the most relevant to [[desktop computer|desktop]] applications.<ref>{{cite web
 |url=http://blogs.msdn.com/bclteam/archive/2008/05/21/net-framework-client-profile-justin-van-patten.aspx 
 |title=.NET Framework Client Profile 
 |date=21 May 2008 
 |accessdate=30 September 2008 
 |author=Justin Van Patten 
 |work=BCL Team Blog 
 |publisher=MSDN Blogs 
 |archiveurl=https://web.archive.org/web/20081207211314/http://blogs.msdn.com/bclteam/archive/2008/05/21/net-framework-client-profile-justin-van-patten.aspx 
 |archivedate=7 December 2008 
 |deadurl=no 
 |df=dmy 
}}</ref> However, the Client Profile amounts to this size only if using the online installer on Windows XP SP2 when no other .NET Frameworks are installed or using [[Windows Update]]. When using the off-line installer or any other OS, the download size is still 250&nbsp;MB.<ref>{{cite web
 |url=http://blogs.msdn.com/jaimer/archive/2008/08/20/client-profile-explained.aspx 
 |title=Client profile explained.. 
 |date=20 August 2008 
 |accessdate=15 February 2009 
 |first=Jaime 
 |last=Rodriguez 
 |archiveurl=https://web.archive.org/web/20090205153531/http://blogs.msdn.com/jaimer/archive/2008/08/20/client-profile-explained.aspx 
 |archivedate=5 February 2009 
 |deadurl=no 
 |df=dmy 
}}</ref>

==.NET Framework 4==
Key focuses for this release are:
* [[Parallel Extensions]] to improve support for [[parallel computing]], which target [[multi-core]] or [[Distributed computing|distributed]] systems.<ref>{{cite web
 |url=http://blogs.msdn.com/somasegar/archive/2007/05/09/the-world-of-multi-and-many-cores.aspx 
 |title=The world of multi and many cores 
 |author=[[S. Somasegar]] 
 |accessdate=1 June 2008 
 |archiveurl=http://www.webcitation.org/5Pmrj6emZ?url=http%3A%2F%2Fblogs.msdn.com%2Fsomasegar%2Farchive%2F2007%2F05%2F09%2Fthe-world-of-multi-and-many-cores.aspx 
 |archivedate=22 June 2007 
 |deadurl=no 
 |df=dmy 
}}</ref> To this end, technologies like PLINQ (Parallel [[Language Integrated Query|LINQ]]),<ref>{{cite web
| url = http://msdn.microsoft.com/magazine/cc163329.aspx
| title = Parallel LINQ: Running Queries On Multi-Core Processors
| accessdate =2 June 2008
}}</ref> a parallel implementation of the LINQ engine, and [[Task Parallel Library]], which exposes parallel constructs via method calls,<ref>{{cite web
| url = http://msdn.microsoft.com/magazine/cc163340.aspx
| title = Parallel Performance: Optimize Managed Code For Multi-Core Machines
| accessdate =2 June 2008
}}</ref> are included.
* New [[Visual Basic .NET]] and [[C Sharp (programming language)|C#]] language features, such as implicit line continuations, [[dynamic dispatch]], [[named parameter]]s, and [[Parameter (computer science)#Default arguments|optional parameters]]
* Support for Code Contracts<ref>{{cite web|title=Code Contracts |url=http://msdn.microsoft.com/en-us/devlabs/dd491992.aspx |website=Dev Labs |archiveurl=https://web.archive.org/web/20110216005315/http://msdn.microsoft.com/en-us/devlabs/dd491992.aspx |archivedate=16 February 2011 |deadurl=no |df=dmy }}</ref>
* Inclusion of new types to work with [[arbitrary-precision arithmetic]] (System.Numerics.BigInteger)<ref>{{cite web|title=BigInteger Structure|url=https://msdn.microsoft.com/en-us/library/system.numerics.biginteger(v=vs.100).aspx|website=[[MSDN]]|publisher=[[Microsoft]]|accessdate=11 March 2016}}</ref> and [[complex number]]s (System.Numerics.Complex)<ref>{{cite web|title=Complex Structure|url=https://msdn.microsoft.com/library/system.numerics.complex(VS.100).aspx|website=[[MSDN]]|publisher=[[Microsoft]]|accessdate=11 March 2016}}</ref>
* Introduced Common Language Runtime (CLR) 4.0

.NET Framework 4.0 is supported on [[Windows Server 2003]], [[Windows Vista|Vista]], [[Windows Server 2008|Server 2008]], [[Windows 7|7]] and [[Windows Server 2008 R2|Server 2008 R2]].<ref name="depend"/>  Applications utilizing .NET Framework 4.0 will also run on computers with .NET Framework 4.5 or 4.6 installed, which supports additional operating systems.

===History===
Microsoft announced the intention to ship .NET Framework 4 on 29 September 2008. The Public Beta was released on 20 May 2009.<ref name=v4>{{cite web|url=http://blogs.msdn.com/somasegar/archive/2009/05/18/visual-studio-2010-and-net-fx-4-beta-1-ships.aspx |author=S. Somasegar |title=Visual Studio 2010 and .NET FX 4 Beta 1 ships! |accessdate=25 May 2009 |archiveurl=http://www.webcitation.org/5h5lV7362?url=http%3A%2F%2Fblogs.msdn.com%2Fsomasegar%2Farchive%2F2009%2F05%2F18%2Fvisual-studio-2010-and-net-fx-4-beta-1-ships.aspx |archivedate=27 May 2009 |deadurl=no |df=dmy }}</ref>

On 28 July 2009, a second release of the .NET Framework 4 beta was made available with experimental [[software transactional memory]] support.<ref>{{cite web
 |url=http://blogs.msdn.com/somasegar/archive/2009/07/27/stm-net-in-devlabs.aspx 
 |title=STM.NET on DevLabs 
 |date=27 July 2008 
 |accessdate=6 August 2008 
 |archiveurl=http://www.webcitation.org/5iwEjbaov?url=http%3A%2F%2Fblogs.msdn.com%2Fsomasegar%2Farchive%2F2009%2F07%2F27%2Fstm-net-in-devlabs.aspx 
 |archivedate=11 August 2009
 |deadurl=no 
 |df=dmy 
}}</ref> This functionality is not available in the final version of the framework.

On 19 October 2009, Microsoft released Beta 2 of the .NET Framework 4.<ref name="4beta2">{{cite web|url=http://blogs.msdn.com/somasegar/archive/2009/10/19/announcing-visual-studio-2010-and-net-fx-4-beta-2.aspx |title=Announcing Visual Studio 2010 and .NET FX 4 Beta 2 |author=S. Somasegar |work=[[MSDN]] Blogs |accessdate=20 October 2009 |archiveurl=https://web.archive.org/web/20091022053419/http://blogs.msdn.com/somasegar/archive/2009/10/19/announcing-visual-studio-2010-and-net-fx-4-beta-2.aspx |archivedate=22 October 2009 |deadurl=no |df=dmy }}</ref> At the same time, Microsoft announced the expected launch date for .NET Framework 4 as 22 March 2010.<ref name="4beta2"/> This launch date was subsequently delayed to 12 April 2010.<ref name="4RTM">{{cite web|url=http://blogs.msdn.com/robcaron/archive/2010/01/13/9948172.aspx |title=Visual Studio 2010 and .NET Framework 4 Launch Date |first=Rob |last=Caron |work=[[MSDN]] Blogs |accessdate=13 January 2010 |archiveurl=https://web.archive.org/web/20100117135051/http://blogs.msdn.com/robcaron/archive/2010/01/13/9948172.aspx |archivedate=17 January 2010 |deadurl=no |df=dmy }}</ref>

On 10 February 2010, a [[release candidate]] was published: Version:RC.<ref>http://www.infoworld.com/d/developer-world/microsoft-offers-visual-studio-2010-release-candidate-643  {{webarchive |url=https://web.archive.org/web/20100521085427/http://www.infoworld.com/d/developer-world/microsoft-offers-visual-studio-2010-release-candidate-643 |date=21 May 2010 }}</ref>

On 12 April 2010, the final version of .NET Framework 4.0 was launched alongside the final release of [[Microsoft Visual Studio 2010]].<ref>{{cite web|first=Emil|last=Protalinski|title=Visual Studio 2010 and .NET Framework 4 arrive|url=http://arstechnica.com/information-technology/2010/04/visual-studio-2010-and-net-framework-40-arrive/|website=[[Ars Technica]]|publisher=[[Condé Nast]]|date=12 April 2010}}</ref>

On 18 April 2011, version 4.0.1 was released supporting some customer-demanded fixes for [[Windows Workflow Foundation]].<ref>{{cite web|url=http://support.microsoft.com/kb/2495593 |title=Update 4.0.1 for Microsoft .NET Framework 4 - Design-Time Update for Visual Studio 2010 SP1 |publisher=Support.microsoft.com |date=2012-06-25 |accessdate=2013-01-16}}</ref> Its design-time component, which requires Visual Studio 2010 SP1, adds a workflow state machine designer.<ref>{{cite web|url=http://blogs.msdn.com/b/endpoint/archive/2011/04/18/microsoft-net-framework-4-platform-update-1.aspx |title=Microsoft .NET Framework 4 Platform Update 1 - The .NET Endpoint - Site Home - MSDN Blogs |publisher=Blogs.msdn.com |date=2011-04-19 |accessdate=2013-01-16}}</ref>

On 19 October 2011, version 4.0.2 was released supporting some new features of [[Microsoft SQL Server]].<ref>{{cite web|url=http://support.microsoft.com/kb/2544514 |title=Update 4.0.2 for Microsoft .NET Framework 4 – Runtime Update |publisher=Support.microsoft.com |date=2012-06-14 |accessdate=2013-01-16}}</ref>

Version 4.0.3 was released on 4 March 2012.<ref>{{cite web|url=http://support.microsoft.com/kb/2600211 |title=Update 4.0.3 for Microsoft .NET Framework 4 – Runtime Update |publisher=Support.microsoft.com |date=2012-08-03}}</ref>

===Windows Server AppFabric===
After the release of the .NET Framework 4, Microsoft released a set of enhancements, named [[AppFabric|Windows Server AppFabric]],<ref>[http://blogs.iis.net/appfabric/archive/2010/06/07/windows-server-appfabric-now-generally-available.aspx Windows Server AppFabric now Generally Available : AppFabric Blog : The Official Microsoft IIS Site]</ref> for [[application server]] capabilities in the form of AppFabric Hosting<ref name="dsource">{{cite web | url = http://www.devsource.com/c/a/Architecture/Dublin-App-Server-coming-toNET-40/ | title = 'Dublin' App Server coming to .NET 4 | work = DevSource | accessdate =27 April 2009}}</ref><ref name="arule">{{cite web|url=http://blogs.msdn.com/architectsrule/archive/2008/10/01/net-framework-4-0-and-dublin-application-server.aspx |title=.NET Framework 4 and Dublin Application Server |work=[[MSDN]] Blogs |accessdate=27 April 2009 |archiveurl=https://web.archive.org/web/20090510131816/http://blogs.msdn.com/architectsrule/archive/2008/10/01/net-framework-4-0-and-dublin-application-server.aspx |archivedate=10 May 2009 |deadurl=no |df=dmy }}</ref> and in-memory distributed caching support.

==.NET Framework 4.5==
.NET Framework 4.5 was released on 15 August 2012;<ref name="net45">{{cite web |url=http://blogs.msdn.com/b/dotnet/archive/2012/08/15/announcing-the-release-of-net-framework-4-5-rtm-product-and-source-code.aspx|title=Announcing the release of .NET Framework 4.5 RTM - Product and Source Code|author=Brandon Bray([[MSDN Blogs]]) |accessdate=15 August 2012}}</ref> a set of new or improved features were added into this version.<ref name="net45doc">{{cite web |url=http://msdn.microsoft.com/en-us/library/ms171868%28v=VS.110%29.aspx |title=What's New in the .NET Framework 4.5|author=[[MSDN Library]] |accessdate=15 August 2012}}</ref> The .NET Framework 4.5 is only supported on [[Windows Vista]] or later.<ref>[http://www.microsoft.com/download/en/details.aspx?id=30653 Microsoft .NET Framework 4.5]</ref><ref>[http://www.microsoft.com/visualstudio/11/en-us/downloads#net-45 Standalone Installers .NET 4.5]</ref> The .NET Framework 4.5 uses Common Language Runtime 4.0, with some additional runtime features.<ref>[http://msdn.microsoft.com/en-us/library/bb822049.aspx .NET Framework Versions and Dependencies]</ref>

.NET Framework 4.5 is supported on [[Windows Vista]], [[Windows Server 2008|Server 2008]], [[Windows 7|7]], [[Windows Server 2008 R2|Server 2008 R2]], [[Windows 8|8]], [[Windows Server 2012|Server 2012]], [[Windows 8.1|8.1]] and [[Windows Server 2012 R2|Server 2012 R2]].<ref name="depend"/>  Applications utilizing .NET Framework 4.5 will also run on computers with .NET Framework 4.6 installed, which supports additional operating systems.

=== .NET for Metro-style apps ===
[[Metro-style app]]s were originally designed for specific form factors and leverage the power of the Windows operating system. Two subset of the .NET Framework is available for building Metro-style apps using [[C Sharp (programming language)|C#]] or [[Visual Basic]]: One for [[Windows 8]] and [[Windows 8.1]], called ''.NET APIs for Windows 8.x Store apps''. Another for [[Universal Windows Platform]] (UWP), called ''.NET APIs for UWP''. This version of .NET Framework, as well as the runtime and libraries used for Metro-style apps, is a part of [[Windows Runtime]], the new platform and development model for Metro-style apps. It is an ecosystem that houses many platforms and languages, including [[.NET Framework]], [[C++]] and [[HTML5]] with [[JavaScript]].<ref>{{cite web|title=.NET for Windows apps|url=https://msdn.microsoft.com/en-us/library/windows/apps/br230232.aspx|website=[[MSDN]]|publisher=[[Microsoft]]|accessdate=26 January 2016}}</ref>

=== Core Features ===
*Ability to limit how long the [[regular expression]] engine will attempt to resolve a regular expression before it times out.
*Ability to define the culture for an [[application domain]].
*Console support for [[Unicode]] ([[UTF-16]]) encoding.
*Support for versioning of cultural string ordering and comparison data.
*Better performance when retrieving resources.
*Native support for [[Zip (file format)|Zip]] compression (previous versions supported [[DEFLATE|the compression algorithm]], but not the archive format).
*Ability to customize a [[reflection context]] to override default [[reflection (computer programming)|reflection]] behavior through the '''CustomReflectionContext''' class.
*New asynchronous features were added to the [[C Sharp (programming language)|C#]] and [[Visual Basic .NET|Visual Basic]] languages. These features add a task-based model for performing asynchronous operations,<ref>{{cite web|url=http://blogs.msdn.com/b/dotnet/archive/2012/04/03/async-in-4-5-worth-the-await.aspx |title=Async in 4.5: Worth the Await - .NET Blog - Site Home - MSDN Blogs |publisher=Blogs.msdn.com |date= |accessdate=2014-05-13}}</ref><ref>{{cite web|url=http://msdn.microsoft.com/en-us/library/hh191443.aspx |title=Asynchronous Programming with Async and Await (C# and Visual Basic) |publisher=Msdn.microsoft.com |date= |accessdate=2014-05-13}}</ref> implementing [[futures and promises]].

=== Managed Extensibility Framework (MEF) ===
The Managed Extensibility Framework or MEF is a library for creating lightweight, extensible applications. It allows application developers to discover and use extensions with no configuration required. It also lets extension developers easily encapsulate code and avoid fragile hard dependencies. MEF not only allows extensions to be reused within applications, but across applications as well.<ref name="mef_desc">{{cite web|title=Managed Extensibility Framework (MEF)|url=https://msdn.microsoft.com/en-us/library/dd460648(v=vs.110).aspx|website=MSDN Blogs|publisher=Microsoft|accessdate=4 October 2016}}</ref>

=== ASP.NET ===
* Support for new [[HTML5]] form types.
* Support for model binders in [[Web Forms]]. These let you bind data controls directly to [[data-access]] methods, and automatically convert user input to and from [[.NET Framework]] [[data type]]s.
* Support for unobtrusive [[JavaScript]] in client-side validation scripts.
* Improved handling of client script through [[bundle (software distribution)|bundling]] and [[minification (programming)|minification]] for improved page performance.
* Integrated encoding routines from the Anti-XSS library (previously an external library) to protect from [[cross-site scripting]] attacks.
* Support for [[WebSocket]] protocol.
* Support for reading and writing [[HTTP]] requests and responses [[Ajax (programming)|asynchronously]].
* Support for asynchronous modules and handlers.
* Support for [[content distribution network]] (CDN) fallback in the ScriptManager control.

=== Networking ===
*Provides a new [[programming interface]] for HTTP applications: System.Net{{Not a typo|.}}Http namespace and System.Net.Http{{Not a typo|.}}Headers namespaces are added
*Improved [[internationalization]] and [[IPv6]] support
*RFC-compliant [[URI]] support
*Support for [[internationalized domain name]] (IDN) parsing
*Support for [[Email Address Internationalization]] (EAI)

=== .NET Framework 4.5.1 ===
The release of .NET Framework 4.5.1 was announced on 17 October 2013 along Visual Studio 2013.<ref>{{cite web|title=.NET Framework 4.5.1 RTM => start coding|url=http://blogs.msdn.com/b/dotnet/archive/2013/10/17/net-framework-4-5-1-rtm-gt-start-coding.aspx|work=.NET Framework Blog|publisher=[[Microsoft]]|accessdate=18 November 2013|date=17 October 2013}}</ref> This version requires [[Windows Vista SP2]] and later<ref>{{cite web|title=Microsoft .NET Framework 4.5.1 (Offline Installer)|url=http://www.microsoft.com/en-us/download/details.aspx?id=40779|work=Download Center|publisher=[[Microsoft]]|accessdate=18 November 2013|date=12 October 2013}}</ref> and is included with [[Windows 8.1]] and [[Windows Server 2012 R2]]. New features of .NET Framework 4.5.1:<ref name="net451_features">{{cite web|title=.NET Framework 4.5.1 RTM => start coding|url=https://blogs.msdn.microsoft.com/dotnet/2013/10/17/net-framework-4-5-1-rtm-start-coding/|website=MSDN Blogs|publisher=Microsoft|accessdate=4 October 2016}}</ref>
*Debugger support for X64 edit and continue (EnC)
*Debugger support for seeing managed return values
*Async-aware debugging in the Call Stack and Tasks windows
*Debugger support for analyzing .NET memory dumps (in the Visual Studio Ultimate SKU)
*Tools for .NET developers in the Performance and Diagnostics hub
*Code Analysis UI improvements
*ADO.NET idle connection resiliency

=== .NET Framework 4.5.2 ===
The release of .NET Framework 4.5.2 was announced on 5 May 2014.<ref name="net452_release">{{cite web|title=Announcing the .NET Framework 4.5.2|url=https://blogs.msdn.microsoft.com/dotnet/2014/05/05/announcing-the-net-framework-4-5-2/|website=MSDN Blogs|publisher=Microsoft|accessdate=11 June 2014}}</ref> For [[Windows Forms]] applications, improvements were made for high [[Dots per inch|DPI]] scenarios. For ASP.NET, higher reliability HTTP header inspection and modification methods are available as is a new way to schedule background asynchronous worker tasks.<ref name="net452_release" />

== .NET Framework 4.6 ==
.NET Framework 4.6 was announced on 12 November 2014.<ref name="net46">{{cite web |url=http://blogs.msdn.com/b/dotnet/archive/2014/11/12/announcing-net-2015-preview-a-new-era-for-net.aspx|title=Announcing .NET 2015 Preview: A New Era for .NET|author=.NET Team|accessdate=27 February 2015}}</ref> It was released on 20 July 2015.<ref name=":0">{{cite web|last1=Lander|first1=Rich|title=Announcing .NET Framework 4.6|url=http://blogs.msdn.com/b/dotnet/archive/2015/07/20/announcing-net-framework-4-6.aspx|website=.NET Blog|publisher=[[Microsoft]]|date=20 July 2015|quote=The team is updating the System.Security.Cryptography APIs to support the Windows CNG cryptography APIs [...] since it supports modern cryptography algorithms [Suite B Support], which are important for certain categories of apps.}}</ref> It supports a new [[just-in-time compiler]] (JIT) for 64-bit systems called RyuJIT, which features higher performance and support for [[SSE2]] and [[AVX2]] instruction sets. WPF and Windows Forms both have received updates for high DPI scenarios. Support for [[Transport Layer Security|TLS]] 1.1 and TLS 1.2 has been added to WCF.<ref name=":0" /> This version requires [[Windows Vista]] SP2 or later.<ref name="SysReq">{{cite web|title=.NET Framework System Requirements|url=https://msdn.microsoft.com/en-us/library/8z6watww(v=vs.110).aspx|website=MSDN|publisher=[[Microsoft]]|accessdate=18 August 2016}}</ref>

The cryptographic API in .NET Framework 4.6 uses the latest version of [[Cryptography Next Generation|Windows CNG]] cryptography API. As a result, [[NSA Suite B Cryptography]] is available to .NET Framework. Suite B consists of [[Advanced Encryption Standard|AES]], the [[SHA-2]] family of hashing algorithms, [[elliptic curve Diffie–Hellman]], and [[elliptic curve DSA]].<ref name=":0" /><ref name="Suite B source">{{cite web|title=CNG Features § Suite B Support|url=https://msdn.microsoft.com/library/windows/desktop/bb204775.aspx#suite_b_support|website=Cryptography API: Next Generation|publisher=[[Microsoft]]|accessdate=1 January 2016}}</ref>

.NET Framework 4.6 is supported on [[Windows Vista]], [[Windows Server 2008|Server 2008]], [[Windows 7|7]], [[Windows Server 2008 R2|Server 2008 R2]], [[Windows 8|8]], [[Windows Server 2012|Server 2012]], [[Windows 8.1|8.1]], [[Windows Server 2012 R2|Server 2012 R2]], [[Windows 10|10]] and [[Windows Server 2016|Server 2016]].<ref name="depend"/>  However, .NET Framework 4.6.1 and 4.6.2 drops support for [[Windows Vista]] and [[Windows Server 2008|Server 2008]].

=== .NET Framework 4.6.1 ===
The release of .NET Framework 4.6.1 was announced on 30 November 2015.<ref name="net461_release">{{cite web|title=.NET Framework 4.6.1 is now available!|url=https://blogs.msdn.microsoft.com/dotnet/2015/11/30/net-framework-4-6-1-is-now-available/|website=MSDN Blogs|publisher=Microsoft|accessdate=19 February 2016}}</ref>  This version requires [[Windows 7]] SP1 or later.<ref name="SysReq" /> New features and APIs include:

* WPF improvements for spell check, support for per-user custom dictionaries and improved touch performance.
* Enhanced support for Elliptic Curve Digital Signature Algorithm (ECDSA) X509 certificates.
* Added support in SQL Connectivity for AlwaysOn, Always Encrypted and improved connection open resiliency when connecting to Azure SQL Database.
* Azure SQL Database now supports distributed transactions using the updated System.Transactions APIs .
* Many other performance, stability, and reliability related fixes in RyuJIT, GC, WPF and WCF.

=== .NET Framework 4.6.2 ===
The preview of .NET Framework 4.6.2 was announced on March 30, 2016.<ref>{{cite web|title=Announcing the .NET Framework 4.6.2 Preview|url=https://blogs.msdn.microsoft.com/dotnet/2016/03/30/announcing-the-net-framework-4-6-2-preview/|website=.NET Blog|publisher=Microsoft}}</ref> It was released on August 2, 2016.<ref>{{cite web|title=Announcing .NET Framework 4.6.2|url=https://blogs.msdn.microsoft.com/dotnet/2016/08/02/announcing-net-framework-4-6-2/|website=.NET Blog|publisher=Microsoft}}</ref>  This version requires [[Windows 7]] SP1 or later.<ref name="SysReq" /> New features include:

* Support for paths longer than 260 characters
* Support for [[Digital Signature Algorithm|FIPS 186-3 DSA]] in [[X.509]] certificates
* TLS 1.1/1.2 support for ClickOnce
* Support for localization of data annotations in ASP.NET
* Enabling .NET desktop apps with Project Centennial
* Soft keyboard and per-monitor DPI support for WPF

==References==
{{Reflist|30em}}

{{.NET Framework}}

[[Category:.NET Framework| ]]
[[Category:2002 software]]
[[Category:Computing platforms]]
[[Category:Microsoft development tools]]
[[Category:Microsoft application programming interfaces]]
[[Category:Software version histories]]