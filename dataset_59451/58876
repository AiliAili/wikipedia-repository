{{hatnote|This page refers to a medical journal. For the medical specialty, see [[Pathogenic fungi|medical mycology]].}}
{{Infobox journal
| title = Medical Mycology
| cover = 
| editor = Ira F. Salkin
| discipline = [[Medical mycology]]
| former_names = Sabouraudia, Journal of Medical and Veterinary Mycology
| abbreviation = Med. Mycol.
| publisher = [[Oxford University Press]] on behalf of the [[International Society of Human and Animal Mycology]]
| country =
| frequency = 8/year
| history = 1962-present
| openaccess = 
| license = 
| impact = 2.261
| impact-year = 2013
| website = http://mmy.oxfordjournals.org/
| link1 = http://mmy.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://mmy.oxfordjournals.org/content/by/year
| link2-name = Online archive
| JSTOR = 
| OCLC = 38873464
| LCCN = 99113023
| CODEN = MEMYFR
| ISSN = 1369-3786
| eISSN = 1460-2709
}}
'''''Medical Mycology''''' is a [[peer-reviewed]] [[medical journal]] published by [[Oxford University Press]] on behalf of the [[International Society of Human and Animal Mycology]]. It was established in 1962 as ''Sabouraudia'', honoring the French [[dermatologist]]/medical mycologist, [[Raimond Sabouraud]] and publishing 3 to 4 issues per year. In 1986 the name was changed to ''Journal of Medical and Veterinary Mycology'' and the number of issues was increased to six per year. The journal obtained its current name in 1998, increasing the number of yearly issues to eight in 2005. In addition, the journal occasionally publishes supplemental issues on specific topics of current interest to the medical mycology community or the [[proceedings]] of international conferences. 

The journal covers all aspects of medical, veterinary, and environmental [[mycology]]. The [[editor-in-chief]] is Ira F. Salkin ([[State University of New York]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 2.261.<ref name=WoS>{{cite book |year=2014 |chapter=Medical Mycology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

==External links==
* {{Official website|http://mmy.oxfordjournals.org/}}
* [http://www.isham.org International Society for Human and Animal Mycology]


[[Category:Mycology journals]]
[[Category:Publications established in 1962]]
[[Category:English-language journals]]
[[Category:Oxford University Press academic journals]]