{{Infobox journal
| title = Journal of Planning History
| cover = [[File:Journal of Planning History.jpg]]
| editors = Nicholas Dagen Bloom, Sonia Hirt 
| discipline = [[City planning]], [[history]]
| former_names = 
| abbreviation = J. Plan. Hist.
| publisher = [[SAGE Publications]]  on behalf of the [[Society for American City and Regional Planning History]]
| country = 
| frequency = Quarterly
| history = 2002-present
| openaccess = 
| license = 
| impact = 
| impact-year = 2010
| website = http://www.uk.sagepub.com/journals/Journal201631?siteId=sage-uk&prodTypes=any&q=Journal+of+Planning+History&fs=1
| link1 = http://jph.sagepub.com/content/current
| link1-name = Online access
| link2 = http://jph.sagepub.com/content/by/year
| link2-name = Online archive
| ISSN = 1538-5132
| eISSN = 1552-6585
| OCLC = 48819102
| LCCN = 2002212546
}}
The '''''Journal of Planning History''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] that covers the field of [[history]] of [[city planning]]. The journal's editors are Nicholas Dagen Bloom (New York Institute of Technology) and Sonia Hirt (Virginia Tech). It was established in 2002 and is currently published by [[SAGE Publications]] on behalf of the [[Society for American City and Regional Planning History]].

== Scope ==
''The Journal of Planning History'' covers the history of city and regional planning, particularly as related to the Americas. It covers topics such as transnational planning experiences, planning history [[pedagogy]], and planning history in planning practice.

== Abstracting and indexing ==
''The Journal of Planning History'' is abstracted and indexed in:
* [[America: History and Life]]
* [[Academic Search| Academic Complete]]
* [[Academic Search| Academic Premier]]
* [[CSA Worldwide Political Science Abstracts]]
* [[GEOBASE]]
* [[Historical Abstracts]]
* [[Scopus]]
* [[Sociological Abstracts]]
* [[ZETOC]]

== External links ==
* {{Official website|http://jph.sagepub.com/}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Urban studies and planning journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2002]]