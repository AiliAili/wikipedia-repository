{{Advert|article|date=January 2017}}
{{Infobox book| <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name           = Freddie Steinmark: Faith, Family, Football
| image          = FJS-BookCover.jpg
| caption        = Book cover, depicts Freddie Steinmark on the sideline of the [[Cotton Bowl Classic|Cotton Bowl]] January 1, 1970
| authors        = Bower Yousse and Thomas J. Cryan
| country        = United States
| language       = English
| genre          = [[Biography]]
| publisher      = [[University of Texas Press]]
| release_date   = 1 September 2015
| media_type     = Print
| pages          = 287
| ISBN           = 1477308210
}}
{{Copypaste|section|url=http://www.freddiejoesteinmark.com/pdf/Steinmark.LucindaLiterary.PressRelease.07.21.15(1).pdf|date=January 2017}}
'''''Freddie Steinmark: Faith, Family, Football''''' is a 2015 non-fiction biography written by Bower Yousse and Thomas J. Cryan, and is the exploration of [[University of Texas]] football player [[Freddie Joe Steinmark]]’s brief life. This is the first book written with complete access to the Steinmark family and their archives, by Freddie’s close friend Bower Yousse and colleague Thomas J. Cryan.  The book is an insider’s account of Freddie’s life and the challenges he faced.  This book takes the reader through Freddie’s world of family, faith, and athletics, his tenure as a patient, and eventually his role as a spokesperson for the [[American Cancer Society]], during which time he inspired [[Presidency of Richard Nixon|President Nixon]] to promote and sign into law the [[National Cancer Act of 1971]], and begin the US’s “War on Cancer.”<ref>{{cite web |url=http://www.breitbart.com/texas/2015/11/03/american-film-features-heroic-story-ut-football-legend-freddie-steinmark/ |title='My All American' Film Features Heroic Story of Football Legend Freddie Steinmark | date=3 November 2015 |access-date=6 November 2015}}</ref><ref>{{cite web |url=http://www.5280.com/cultureandevents/film/digital/2015/11/new-film-honors-local-hero |title=New Film Honors Local Hero | date=6 November 2015 |access-date=9 November 2015}}</ref>

== Background and summary ==
Freddie Steinmark was starting safety for the undefeated University of [[Texas Longhorns]] in 1969.<ref>Eldon S. Branda, "[http://www.tshaonline.org/handbook/online/articles/fst32 STEINMARK, FREDDIE JOE]", ''Handbook of Texas Online'', accessed November 9, 2015. Uploaded on June 15, 2010. Modified on September 24, 2015. Published by the Texas State Historical Association.</ref> In the "[[1969 Texas vs. Arkansas football game|Game of the Century]]" a come-from-behind victory against Arkansas that ensured Texas the national championship, Steinmark played with pain in his left leg. Two days after the game, X-rays showed a possible tumor, and four days after that, a biopsy and surgery to amputate his leg revealed a cancerous growth so large that is seemed a miracle Steinmark could walk, let alone play football.<ref>Steinmark, Freddie. ''I Play to Win''. Boston: Little, Brown, 1971. Print.</ref>

An undersized player,{{Clarify|reason=vague|date=January 2017}} Steinmark had quickly become a fan favorite at Texas. What he endured during the Longhorns' 1969 season, and what he encountered afterward, captivated not only Texans but the country at large. Americans watched closely as Steinmark confronted life’s ultimate challenge,<ref>{{cite web |url=http://res.dallasnews.com/ipad/interactives/archives/Blackie/requiem.html |title=Requiem for an All-American | date=7 June 1971 |access-date=9 November 2015}}</ref> and his openness during his battle against savage odds helped reframe the national conversation surrounding cancer and the ongoing race for a cure.<ref>Eldon S. Branda, "[http://www.tshaonline.org/handbook/online/articles/fst32 STEINMARK, FREDDIE JOE]," Handbook of Texas Online, accessed November 09, 2015. Uploaded on June 15, 2010. Modified on September 24, 2015. Published by the Texas State Historical Association.</ref>

== Major themes ==
''Freddie Steinmark, Faith, Family, Football'' addresses several aspects of living, including the application of one's practice of [[faith]] to the everyday tasks of life, in Steinmark's case, his [[Catholic]] faith,<ref>"Texas' Steinmark: 'Gentle Boy With a Deep Faith in God'" ''The Palm Beach Post'' 11 June 1971: D4. Print. | [https://news.google.com/newspapers?id=MDojAAAAIBAJ&sjid=OrcFAAAAIBAJ&pg=1034%2C4355072]</ref> as exemplified by the discipline, commitment, and perseverance of praying the [[rosary]] daily, and in turn applying these elements to the classroom and the practice field; and then relying on that faith when facing one's mortality, in Steinmark's story, as a young man.<ref>{{cite web |url=https://www.youtube.com/watch?v=_idEyiIABBs |title=EWTN, The World Over, with Raymond Arroyo date 29 October 2015}}</ref>

== Films ==
''Freddie Steinmark: Faith, Family, Football'' coincides with the major motion picture ''[[My All American]]'', starring Aaron Eckhart as Texas Coach Darrell Royal and Finn Wittrock as Freddie Steinmark.<ref>{{cite web |url=http://www.prweb.com/releases/2015/07/prweb12861058.htm |title=The First Authorized Biography of the Young Football Legend whose Spirit Captivated Our Country: Freddie Steinmark; Faith, Family, Football | date=23 July 2015 |access-date=6 November 2015}}</ref><ref>{{cite web |url=http://www.mysanantonio.com/entertainment/arts-culture/books/article/Freddie-Steinmark-played-key-roles-in-big-6458127.php |title=Freddie Steinmark played key roles in big Longhorn football games | date=29 August 2015 |access-date=9 November 2015}}</ref>

== Articles ==
The ''Austin American Statesman'', in an article by Jane Sumner, said that “The Authors . . . capture Freddie’s cheerful essence, vividly recreates key games and posits his life against the canvas of history.”<ref>{{cite web |url=http://www.mystatesman.com/news/entertainment/books-literature/freddie-steinmark-a-celebration-of-heart-and-coura/nnWRt/ |title='Freddie Steinmark' A celebration of heart and courage | date=2 September 2015 |access-date=6 November 2015}}</ref> The ''Fort Worth Star-Telegram'' listed the book as "New and Notable".<ref>{{cite web |url=http://www.star-telegram.com/entertainment/books/article31509242.html |title=New & Notable: College Football Books | date=19 August 2015 |access-date=6 November 2015}}</ref>  “Will All the Pages Have a Burnt Orange Hue,” is an article by Jan Buchholz, in the ''Austin Business Journal'', and describes the book as a chronicle of the story of the safety.<ref>{{cite journal |last=Buchholz |first=Jan |date=September 2015 |title=Will All the Pages Have a Burnt Orange Hue? |journal=Austin Business Journal |volume= |page=3}}</ref>

== References ==
{{Reflist}}

== External links ==
* [http://www.freddiejoesteinmark.com Official Freddie Joe Steinmark website]

<!--- Categories --->
[[Category:University of Texas Press books]]
[[Category:American biographies]]
[[Category:2015 non-fiction books]]