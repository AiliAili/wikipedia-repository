{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox orchestra
| name                = Adelaide Symphony Orchestra (ASO)
| image                 = File:ASO LOGO.jpg
| caption = Adelaide Symphony Orchestra logo
| founded        = 1936
| former_name = South Australian Symphony Orchestra
| principal_conductor = 
| website                 = {{URL|www.aso.com.au}}
| current_members     = '''Chief Conductor and Music Director'''
}}

The '''Adelaide Symphony Orchestra''' (ASO) is an Australian orchestra based in [[Adelaide]], South Australia.  Its primary venue is the [[Adelaide Town Hall]], but the ASO also performs in other venues such as [[Elder Conservatorium|Elder Hall]] at the [[University of Adelaide]] and the [[Adelaide Festival Centre]].  The ASO provides the orchestral support for all productions of the [[State Opera of South Australia]], as well as the Adelaide performances of the [[The Australian Ballet|Australian Ballet]] and [[Opera Australia]].  The orchestra is also a regular featured ensemble at the [[Adelaide Festival]].

In 1936 the [[South Australian Orchestra]] was supplanted by the 50-member Adelaide Symphony Orchestra led by [[William Cade]], and sponsored by the [[Australian Broadcasting Corporation|Australian Broadcasting Commission]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article132205559 |title=New Symphony Orchestra of 50 Performers Formed |newspaper=[[The News (Adelaide)]] |volume=XXVI, |issue=4,001 |location=South Australia |date=19 May 1936 |accessdate=7 February 2017 |page=1 |via=National Library of Australia}} This article lists all members except the harpist and tympanists.</ref>
The orchestra reformed in 1949 as the 55-member South Australian Symphony Orchestra, with [[Henry Krips (conductor)|Henry Krips]] as its resident conductor.  The orchestra reverted to its original title, the Adelaide Symphony Orchestra, in late 1975, and currently comprises 74 permanent members.  Chief conductors of the orchestra have included [[Elyakum Shapirra]], [[Piero Gamba]], [[Albert Rosen]], [[Nicholas Braithwaite]], and [[David Porcelijn]].  The ASO's most recent chief conductor was [[Arvo Volmer]], who held the post from 2004 to 2013.  He is currently the ASO's principal guest conductor and artistic advisor.<ref>{{cite news | url=http://www.theaustralian.com.au/arts/volmers-vivid-delivery-a-firecracker-farewell/story-e6frg8n6-1226760120682
 | title=Volmer's vivid delivery a firecracker farewell | work=The Australian | author=Graham Strahle | date=2013-11-15 | accessdate=2013-11-17}}</ref> In 2007, the orchestra partnered with [[Hilltop Hoods]] to prepare a re-orchestrated release of their album ''The Hard Road'', titled ''[[The Hard Road: Restrung]]''.<ref>{{cite news|url=http://www.dailytelegraph.com.au/entertainment/hilltop-hoods-on-classics/story-e6frexl9-1111113555454?nk=9228bd8e67cd6a144026da12163279c5|title=Hilltop Hoods on Classics|last=McCabe|first=Kathy|date=17 May 2007|work=Daily Telegraph|accessdate=10 August 2014}}</ref> In 2015 the Hilltop Hoods collaborated for a second time with the 32-piece Adelaide Symphony Orchestra and the 20-piece Adelaide Chamber Singers Choir for their next re-orchestrated album titled ''[[Drinking from the Sun, Walking Under Stars Restrung]]''.<ref>{{Cite web|url=http://www.abc.net.au/triplej/musicnews/s4364942.htm|title=Hilltop Hoods announce new Restrung album, national tour, drop 'Higher' {{!}} Music News {{!}} triple j|website=www.abc.net.au|access-date=2016-03-02}}</ref>

The ASO's achievements have included its 1998 performances of [[Richard Wagner]]'s ''[[Der Ring des Nibelungen|Ring Cycle]]'', the first Australian production since 1913<ref>The Ring was first performed in Australia at Her Majesty's Theatre in Melbourne in August 1913 (''Das Rheingold'', 19 August; ''Die Walkure'', 22 August; ''Siegfried'', 25 August; ''Gotterdammerung'', 29 August), Erik Irvin, ''Dictionary of the Australian Theatre 1788-1914'', pp. 245, 246</ref> (although it was widely and erroneously claimed to be the first ever in Australia). The orchestra participated in the first fully Australian production of ''The Ring'' in 2004.

In 2009 Premier and Arts Minister [[Mike Rann]] proposed and provided government funding to the ASO to commission a major orchestral work about climate change. The ASO's world premiere of Gerard Brophy's 'The Blue Thread', inspired by the River Murray, was performed at the Concert for the Earth at the Adelaide Town Hall on 27 November 2010.<ref>Samela Harris, Adelaide Advertiser, 25 November 2015, "Don't call me a greenie"; and ABC 7 December 2010, "Australian Broadcast Highlights, The Blue Thread"</ref> The Rann government proposed and arranged funding for two further ASO commissions, the first an orchestral tribute to the cricketer [[Sir Donald Bradman]], and the second commemorating the centenary of the ANZAC landings at Gallipoli. The world premiere of '[[Our Don]]' by [[Natalie Williams]] was performed by the ASO in August 2014.<ref>{{cite news|url=https://www.theguardian.com/music/australia-culture-blog/2014/aug/15/our-don-review-adelaide-natalie-williams|title=Our Don Review-Donald Bradman's Symphonic Tribute Hits for Six|last=Forester|first=Gordon|date=15 August 2014|work=The Guardian|accessdate=10 September 2014}}</ref> The world premiere of an '[[Anzac Day|ANZAC Requiem]]' by composer [[Iain Grandage]] and librettist [[Kate Mulvany]] was performed on 22 April 2015.<ref>ASO Annual Report, 2011</ref>

==Principal conductors==
* [[William Cade]] (resident conductor, 1939)
* [[Bernard Heinze]] (guest conductor, 1939)
* [[Percy Code]] (interim resident conductor, 1949)
* [[Henry Krips (conductor)|Henry Krips]] (resident conductor, 1949–72)
* [[Elyakum Shapirra]] (chief conductor, 1975–79)
* [[José Serebrier]] (principal guest conductor, 1982–83)
* [[Piero Gamba]] (chief conductor, 1983–85)
* [[Albert Rosen]] (chief conductor, 1986)
* [[Nicholas Braithwaite]] (chief conductor, 1987–91)
* [[David Porcelijn]] (chief conductor, 1993–98)
* [[Arvo Volmer]] (chief conductor, 2004–13)

==See also==
*[[Symphony Australia]]

==References==
{{reflist}}

==External links==
*[http://www.aso.com.au/ Adelaide Symphony Orchestra]
*[https://web.archive.org/web/20070201211012/http://www.asoheritage.com:80/ ASO Heritage]

{{Symphony Australia}}
{{Adelaide Symphony Orchestra conductors}}
{{Authority control}}

[[Category:APRA Award winners]]
[[Category:ARIA Award winners]]
[[Category:Australian orchestras]]
[[Category:Musical groups established in 1936]]
[[Category:Musical groups from Adelaide]]
[[Category:Symphony orchestras]]