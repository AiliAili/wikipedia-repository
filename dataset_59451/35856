{{Infobox television episode
| title = The Aenar
| image =
| caption =
| series = [[Star Trek: Enterprise]]
| season = 4
| episode = 14
| production = 414
| airdate = {{Start date|2005|02|11}}
| story = [[Manny Coto]]
| teleplay = [[André Bormanis]]
| director = [[Mike Vejar]]
| guests = 
*[[Jeffrey Combs]] – Commander [[Shran]]
*[[Alexandra Lydon]] – Jhamel
*[[Brian Thompson]] – Admiral Valdore
*[[Geno Silva]] – Senator Vrax
*Alicia Adams – Lissan
*Scott Allen Rinker – Gareb
| prev = [[United (Star Trek: Enterprise)|United]]|
| next = [[Affliction (Star Trek: Enterprise)|Affliction]]
| episode_list = [[List of Star Trek: Enterprise episodes|List of ''Star Trek: Enterprise'' episodes]]
}}
"'''The Aenar'''" is the fourteenth episode of the [[Star Trek: Enterprise (season 4)|fourth season]] of the American [[science fiction on television|science fiction television]] series ''[[Star Trek: Enterprise]]'', and originally aired on February 11, 2005. It was written by [[André Bormanis]] from a story by [[Manny Coto]], and was directed by [[Mike Vejar]]. "The Aenar" was the third installment of a three-part story which concluded the events of episodes "[[Babel One]]" and "[[United (Star Trek: Enterprise)|United]]".

Set in the 22nd century, the series follows the adventures of the first [[Starfleet]] starship ''[[Enterprise (NX-01)|Enterprise]]''. In this episode, Captain [[Jonathan Archer]] ([[Scott Bakula]]) and Commander [[Shran]] ([[Jeffrey Combs]]) travel to [[Andoria]], a moon, seeking the help of the [[Aenar]]—an offshoot race of the [[Andorian]]s—one of whom has been taken by the Romulans to pilot a [[Unmanned combat aerial vehicle|drone]] vessel (first seen in the previous episode).

The episode showed the home world of the Andorians for the first time, with the sets for the planet's ice tunnels being created on a [[sound stage]]. Alexandra Lydon and Alicia Adams made their first ''Star Trek'' appearances in "The Aenar". Reviews of the episode were mostly negative, with critics citing issues with [[plot holes]] and unanswered questions from the story arc. On its first showing, 3.17 million watched the episode.

==Plot==
Senator Vrax ([[Geno Silva]]), fresh from the [[Romulan]] Senate, is disappointed that Admiral Valadore ([[Brian Thompson]]) and scientist Nijil's (J. Michael Flynn) [[Unmanned combat aerial vehicle|drone]] program has failed to provoke a rift between Human, [[Andorian]] and [[Tellarite]] races as they had hoped (seen in "[[Babel One]]" and "[[United (Star Trek: Enterprise)|United]]"). In fact, the opposite has happened – political discord throughout the [[Galactic quadrant (Star Trek)#Alpha and Beta Quadrants|Alpha and Beta Quadrants]] has declined. Now that a second drone vessel is ready to be launched, Valadore suggests a mission against the ''[[Enterprise (NX-01)|Enterprise]]'' in order to impress the Senate. Nijil argues that the pilot requires time to recover from his previous exertions, but Valadore insists and prioritizes the mission.

On ''Enterprise'', analysis of data gathered in the previous encounter with the Romulan ship reveals that the ship is being piloted [[telepathy|telepathically]] by an Andorian. Commander [[Shran]] ([[Jeffrey Combs]]) explains that the data indicates that the pilot is probably a member of the Aenar, a white-skinned and blind Andorian sub-race. This, however, seems unlikely, since the Aenar are few in number, reclusive [[pacifism|pacifists]], and inhabitants of the isolated extreme northern polar region of their moon. Shran and Captain [[Jonathan Archer]] ([[Scott Bakula]]) then [[Transporter (Star Trek)|beam]] down to contact the Aenar. The Aenar's spokesperson, Lissan (Alicia Adams), initially declines to assist as the Aenar do not want to get involved in a war. However, a young Aenar named Jhamel ([[Alexandra Lydon]]) decides to help, since doing so may help locate Gareb (Scott Allen Rinker), her missing brother.

Meanwhile, Doctor [[Phlox (Star Trek)|Phlox]] ([[John Billingsley]]), Commander [[T'Pol]] ([[Jolene Blalock]]), and Commander [[Trip Tucker|Charles "Trip" Tucker III]] ([[Connor Trinneer]]) work in Sickbay on their own "telepresence" unit to help counter the drone ship. T'Pol volunteers to test it, and a concerned Tucker finds it increasingly difficult to balance his duties and emotions. Jhamel then tests the unit, with better results. Later, when the drone ships reappear and attack, she is able to contact the drone pilot, and it is indeed her long-lost brother, who was tricked into working with the Romulans. Learning the deception of his "helpers", he turns the drones on each other and both are soon destroyed, and Valadore angrily kills him as retribution for his failure. With the threat resolved, the Andorians depart ''Enterprise'' and Tucker requests to leave the ship to join the USS ''Columbia''.

==Production==
"The Aenar" was the third and final part in the Romulan [[story arc]], comprising "[[Babel One]]", "[[United (Star Trek: Enterprise)|United]]" and "The Aenar". It was written by [[André Bormanis]] from a story by [[show runner]] [[Manny Coto]]. Bormanis had also written "Babel One", and earlier in the season the episode "[[Awakening (Star Trek: Enterprise)|Awakening]]", which formed part of the Vulcan story arc. "The Aenar" was directed by [[Mike Vejar]], his third episode of the season.<ref name=productionaenar/>

This episode was the first time the homeworld of the Andorian race was represented on screen.<ref name=productionaenar>{{cite web|title=Production Report: First Visit to Icy Andoria in "The Aenar" (Update)|url=http://www.startrek.com/startrek/view/news/article/8433.html|publisher=StarTrek.com|accessdate=November 22, 2015|archiveurl=https://web.archive.org/web/20041211174926/http://www.startrek.com/startrek/view/news/article/8433.html|archivedate=December 11, 2004|date=December 6, 2004}}</ref> The race had been introduced in the ''[[Star Trek: The Original Series]]'' second season episode "[[Journey to Babel]]".<ref name=torbabel>{{cite web|last1=Myers|first1=Eugene|last2=Atkinson|first2=Torrie|title=Star Trek Re-Watch: "Journey to Babel"|url=http://www.tor.com/2010/03/04/lemgstar-treklemg-re-watch-journey-to-babel/|publisher=Tor.com|accessdate=November 22, 2015|date=March 4, 2010}}</ref> The interior of a [[sound stage]] was fitted out to appear like caverns on the ice world as it was theorized that this was the environment in which a race such as the Andorians, including the Aenar sub-species, could have evolved. These sets were enhanced in post-production using [[computer-generated imagery]].<ref name=productionaenar/>

Filming began on November 22, 2004, and was completed on December 2, with the production being halted for two days due to [[Thanksgiving]]. The majority of the guest stars from earlier installments of the trilogy returned for "The Aenar", and they were joined by Alexandra Lydon and Alicia Adams, who were both making their first appearances in a ''Star Trek'' series. Kim Koski was the [[stunt double]] for Jeffrey Combs during a scene in which Shran was impaled through the leg by a [[stalagmite]].<ref name=productionaenar/>

==Reception and home media release==
"The Aenar" was first aired in the United States on [[UPN]] on February 11, 2005.<ref>{{cite web|title=Aenar, The|url=http://www.startrek.com/database_article/aenar-the|publisher=StarTrek.com|accessdate=November 22, 2015}}</ref> It was watched by 3.17 million viewers,<ref>{{cite web|title=Weekly Program Ratings|url=http://abcmedianet.com/web/dnr/dispDNR.aspx?id=021505_06|publisher=ABC Medianet|accessdate=November 22, 2015|archiveurl=https://web.archive.org/web/20150311231316/http://abcmedianet.com/web/dnr/dispDNR.aspx?id=021505_06|archivedate=March 11, 2015}}</ref> which was an increase on the 2.81 million viewers who watched the previous episode.<ref>{{cite web|title=Weekly Program Ratings|url=http://abcmedianet.com/web/dnr/dispDNR.aspx?id=020805_08|publisher=ABC Medianet|accessdate=November 22, 2015|archiveurl=https://web.archive.org/web/20120402131754/http://abcmedianet.com/web/dnr/dispDNR.aspx?id=020805_08|archivedate=April 2, 2012}}</ref> The following episode, "[[Affliction (Star Trek: Enterprise)|Affliction]]", received around the same number of viewers as "The Aenar".<ref>{{cite web|title=Weekly Program Ratings|url=http://abcmedianet.com/web/dnr/dispDNR.aspx?id=022305_08|publisher=ABC Medianet|accessdate=November 22, 2015|archiveurl=https://web.archive.org/web/20150311231343/http://abcmedianet.com/web/dnr/dispDNR.aspx?id=022305_08|archivedate=March 11, 2015}}</ref>

Michelle Erica Green, while writing for the website [[TrekNation]], thought that the episode had strong character development despite the other flaws present. She felt that it opened up a number of questions about the abilities of the Andorians due to their similarity to the Aenar, and enjoyed the issues present in the Tucker/T'Pol relationship. Although she said that it was the "weakest" of the story arc,<ref name=green>{{cite web|last1=Green|first1=Michelle Erica|title=The Aenar|url=http://www.trektoday.com/reviews/enterprise/the_aenar.shtml|publisher=TrekNation|accessdate=November 22, 2015|date=February 12, 2005}}</ref> it was also the most "gripping".<ref name=green/> Jamahl Epsicokhan, at his website Jammer's Reviews, gave the episode two out of four, adding that "The Aenar" was "aimless" since the majority of the storyline ended in the previous episode. He found fault with the plot, as the Romulan threat was unspecific, and because of several plot holes which the episode failed to explain.<ref>{{cite web|last1=Epsicokhan|first1=Jamahl|title=Star Trek: Enterprise "The Aenar"|url=http://www.jammersreviews.com/st-ent/s4/aenar.php|publisher=Jammer's Reviews|accessdate=November 22, 2015}}</ref>

David Greven, in his book ''Gender and Sexuality in Star Trek'', said that the Aenar were an example of the "uncanny whiteness" style characters which appear in the franchise which are used to provide a "denatured form of whiteness" to the viewer.<ref name=greven110/> Other examples of these type of characters include the [[Borg (Star Trek)|Borg]] and the human-like [[Klingon]]s explained later in season four of ''Enterprise''.<ref name=greven110>[[#greven2009|Greven (2009)]]: p. 110</ref><ref name=greven112>[[#greven2009|Greven (2009)]]: p. 112</ref> He added that the blindness of the Aenar acted as an [[allegory]] for the way in which [[white people]] are unable to perceive their own whiteness.<ref name=greven112/>

"The Aenar" was released on home media in the United States on November 1, 2005, as part of the season four DVD box set of ''Enterprise''.<ref name=dvdrelease>{{cite news|last=Douglass Jr.|first=Todd|title=Star Trek Enterprise – The Complete Fourth Season|url=http://www.dvdtalk.com/reviews/18391/star-trek-enterprise-the-complete-fourth-season/|accessdate=October 11, 2014|newspaper=DVD Talk|date=October 24, 2005}}</ref> The [[Blu-ray]] edition was released on April 1, 2014.<ref>{{cite web|title=Final Season Enterprise Blu-ray Set Available April 1|url=http://www.startrek.com/article/final-season-enterprise-blu-ray-set-available-april-1|publisher=StarTrek.com|date=December 18, 2013|accessdate=October 11, 2014}}</ref>

==Notes==
{{reflist|30em}}

==References==
{{refbegin}}
*{{cite book|last=Greven|first=David|title=Gender and Sexuality in Star Trek: Allegories of Desire in the Television Series and Films|year=2009|publisher=McFarland & Co.|location=Jefferson, N.C.|isbn=978-0-786-44413-7|ref=greven2009}}
{{refend}}

==External links==
* {{IMDb title|id=0572246}}
{{Memoryalpha}}
{{StarTrek.com link|ENT|8432}}

{{Star Trek Romulan stories}}
{{Star Trek ENT S4}}
{{Star Trek ENT}}
{{Star Trek}}
{{good article}}

{{DEFAULTSORT:Aenar, The}}
[[Category:Star Trek: Enterprise episodes]]
[[Category:2005 American television episodes]]