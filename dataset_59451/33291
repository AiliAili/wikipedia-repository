{{featured article}}

{{Taxobox
| image = Necropsar rodericanus.jpg
| image_width = 250px
| image_caption = The [[syntype]] bones as depicted in the 1879 description
| status = EX
| status_system = IUCN3.1
| status_ref = <ref name=BLI>{{IUCN|id=22710836 |title=''Necropsar rodericanus'' |assessor=BirdLife International |assessor-link=BirdLife International |version=2012.1 |year=2012 |accessdate=16 July 2012}}</ref>
| extinct = mid-18th century
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Bird|Aves]]
| ordo = [[Passeriformes]]
| subordo = [[Passeri]]
| infraordo = [[Passerida]]
| superfamilia = [[Muscicapoidea]]
| familia = [[Sturnidae]]
| genus = '''''Necropsar'''''
| genus_authority =  [[Albert Günther|Günther]] & [[Alfred Newton|Newton]], 1879
| species = '''''N. rodericanus'''''
| binomial = ''Necropsar rodericanus''
| binomial_authority =  Günther & Newton, 1879
| range_map = LocationRodrigues.PNG
| range_map_width = 250px
| range_map_caption = Location of [[Rodrigues]]
| synonyms =
* ''Necrospa rodericanus'' <small>[[George Ernest Shelley|Shelley]], 1900</small>
* ''Necrospar rodericanus'' <small>(''[[lapsus]]'')</small>
* ''Testudophaga bicolor'' <small>Hachisuka, 1937</small>
}}

The '''Rodrigues starling''' (''Necropsar rodericanus'') is an [[extinct]] [[species]] of [[starling]] that was [[endemic]] to the [[Mascarene]] island of [[Rodrigues]]. Its closest relatives were the [[Mauritius starling]] and the [[hoopoe starling]] from nearby islands; all three appear to be of Southeast Asian origin. The bird was only reported by French sailor [[Julien Tafforet]], who was marooned on the island from 1725 to 1726. Tafforet observed it on the offshore [[islet]] of [[Île Gombrani]]. [[Subfossil]] remains found on the mainland were described in 1879, and were suggested to belong to the bird mentioned by Tafforet. There was much confusion about the bird and its taxonomic relations throughout the 20th century.

The Rodrigues starling was {{convert|25–30|cm|abbr=off|0}} long, and had a stout beak. It was described as having a white body, partially black wings and tail, and a yellow bill and legs. Little is known about its behaviour. Its diet included eggs and dead tortoises, which it processed with its strong bill. Predation by rats introduced to the area was probably responsible for the bird's extinction some time in the 18th century. It first became extinct on mainland Rodrigues, then on Île Gombrani, its last refuge.

==Taxonomy==
[[File:Naturalis Biodiversity Center - RMNH.AVES.110050 - Fregilupus varius (Boddaert, 1783) - Réunion Starling - specimen - video.webm|thumb|left|alt=Perching hoopoe starling museum specimen|thumbtime=0:02|Turnaround video showing a specimen of the related [[hoopoe starling]], [[Naturalis Biodiversity Center]]]]
In 1725, the French sailor [[Julien Tafforet]] was marooned on the [[Mascarene]] island of [[Rodrigues]] for nine months, and his report of his time there was later published as ''Relation d'île Rodrigue''. In the report, he described encounters with various indigenous species, including a white and black bird which fed on eggs and dead tortoises. He stated that it was confined to the offshore islet of [[Île Gombrani]], which was then called au Mât. [[François Leguat]], a Frenchman who was also marooned on Rodrigues from 1691 to 1693 and had written about several species there (his account was published in 1708), did not have a boat, and therefore could not explore the various islets as Tafforet did.<ref name="Extinct Birds">{{cite book
| last1 = Hume
| first1 = J. P.
| first2 = M.
| last2 = Walters
|pages= 273–274
|year= 2012
|location= London
|title= Extinct Birds
|publisher= A & C Black
|isbn=1-4081-5725-X}}</ref><ref>{{Cite book | doi = 10.5962/bhl.title.46034| title = The Voyage of François Leguat of Bresse, to Rodriguez, Mauritius, Java, and the Cape of Good Hope| year = 1891| last1 = Leguat | first1 = F. O. | editor1-last = Oliver| editor1-first = S. P.| volume = 1| location = London| publisher = Hakluyt Society}}</ref> No people who later traveled to the island mentioned the bird.<ref name="Fuller Extinct">{{cite book
  | last = Fuller
  | first = E.
  | authorlink = Errol Fuller
  | year = 2001
  | title = Extinct Birds
  | edition = revised
  | publisher = Comstock
  | location = New York
  |pages = 366–367
  | isbn = 978-0-8014-3954-4
  | ref = harv
  }}</ref> In an article written in 1875, the British ornithologist [[Alfred Newton]] attempted to identify the bird from Tafforet's description, and hypothesised that it was related to the extinct [[hoopoe starling]] (''Fregilupus varius''), which formerly inhabited nearby [[Réunion]].<ref name="Rodriguez">{{Cite journal  | last =  Newton | first =  A. | last2 =   | first2 =   | title = Additional Evidence as to the Original Fauna of Rodriguez  | journal =   Proceedings of the Zoological Society of London | series =   | volume =   | pages = 39–43  | url =  http://www.biodiversitylibrary.org/page/28501180#page/77/mode/1up | year = 1875}}</ref>

[[Subfossil]] bones of a starling-like bird were first discovered on Rodrigues by the [[police magistrate]] [[George Jenner]] In 1866 and 1871, and by the reverend [[Henry Horrocks Slater]] in 1874. They were found in caves on the Plaine Coral, a [[limestone]] plain in south-west Rodrigues.<ref name="Hume 2014 44–51">Hume, J. P. (2014). pp. 44–51.</ref> These bones included the [[cranium]], [[mandible]], [[sternum]], [[coracoid]], [[humerus]], [[metacarpus]], [[ulna]], [[femur]], [[tibia]], and [[metatarsus]] of several birds; the bones were deposited in the [[British Museum]] and the [[Cambridge Museum]]. In 1879, the bones became the basis of a scientific description of the bird by ornithologists [[Albert Günther]] and [[Edward Newton]] (the brother of Alfred).<ref name="Günther"/> They named the bird ''Necropsar rodericanus''; ''Nekros'' and ''psar'' are Greek for "dead" and "starling", while ''rodericanus'' refers to the island of Rodrigues. This [[binomial nomenclature|binomial]] was originally proposed by Slater in an 1874 manuscript he sent to Günther and Newton.<ref>{{Cite journal | doi = 10.1080/08912963.2014.886203| title = In the footsteps of the bone collectors: Nineteenth-century cave exploration on Rodrigues Island, Indian Ocean| journal = Historical Biology| volume = 27| issue = 2| pages = 1| year = 2014| last1 = Hume | first1 = J. P.| last2 = Steel | first2 = L.| last3 = André | first3 = A. A.| last4 = Meunier | first4 = A.}}</ref> Slater had prepared the manuscript for an 1879 publication, which was never released, but Günther and Newton quoted Slater's unpublished notes in their own 1879 article, and credited him for the name.<ref name="Hume 2014 44–51"/> [[BirdLife International]] credits Slater rather than Günther and Newton for the name.<ref name=BLI/> Günther and Newton determined that the Rodrigues starling was closely related to the hoopoe starling, and they only kept it in a separate genus due to what they termed "present ornithological practice". Due to the strongly built bill, they considered the new species likely the same as the bird mentioned in Tafforet's account.<ref name="Günther">{{cite journal| doi = 10.1098/rstl.1879.0043| last1 = Günther | first1 = A.| last2 = Newton | first2 = E.| year = 1879| title = The extinct birds of Rodriguez| journal = [[Philosophical Transactions of the Royal Society of London]]| volume = 168| pages = 423–437| url = https://archive.org/stream/philtrans07832595/07832595#page/n0/mode/2up| pmc = | ref = harv
}}</ref>

In 1900, the English scientist [[George Ernest Shelley]] used the spelling ''Necrospa'' in a book, thereby creating a [[junior synonym]]; however, he attributed the name to zoologist [[Philip Sclater]].<ref>{{cite book | last1 = Shelley | first1 = G. E.  | url = https://archive.org/stream/birdsofafricacom02shel#page/342/mode/2up |year= 1900 | page = 342 |title= The birds of Africa comprising all the species which occur in the Ethiopian region  |publisher= R.H. Porter |isbn= |location=London}}</ref> In 1967, the American ornithologist [[James Greenway]] suggested that the Rodrigues starling should belong in the same genus as the hoopoe starling, ''Fregilupus'', due to the similarity of the species.<ref name ="Greenway">{{cite book
 | last = Greenway
 | first = J. C.
 | title = Extinct and Vanishing Birds of the World
 | publisher = American Committee for International Wild Life Protection
 | series =
 | volume =
 | edition =
 | location = New York
 | year = 1967
 | pages = 129–132
 | doi =
 | isbn = 0-486-21869-4
 | mr =
 | zbl =  }}</ref> More subfossils found in 1974 added support to the claim that the Rodrigues bird was a distinct genus of starling.<ref name="Cowles87">{{Cite book | doi = 10.1017/CBO9780511735769.004| editor1-last = Diamond| editor1-first = A. W.| title = Studies of Mascarene Island Birds| chapter = The fossil record| pages = 90–100| year = 1987| location = Cambridge | last1 = Cowles | first1 = G. S.| isbn = 9780511735769}}</ref> The stouter bill is mainly what warrants [[generic separation]] from ''Fregilupus''.<ref name="Extinct Birds"/> In 2014, the British palaeontologist [[Julian P. Hume]] described a new extinct species, the [[Mauritius starling]] (''Cryptopsar ischyrhynchus''), based on subfossils from Mauritius. It was shown to be closer to the Rodrigues starling than to the hoopoe starling, due to the features of its skull, sternum and humerus.<ref name="Hume 2014 55–58"/> Until then, the Rodrigues starling was the only Mascarene [[passerine]] bird named from fossil material.<ref name="Hume 2008">{{cite journal | last = Hume | first = J. P. | authorlink = Julian Pender Hume | year = 2005 | title = Contrasting taphofacies in ocean island settings: the fossil record of Mascarene vertebrates | journal = Proceedings of the International Symposium "Insular Vertebrate Evolution: the Palaeontological Approach". Monografies de la Societat d'Història Natural de les Balears | volume = 12 | pages = 129–144 }}</ref>
[[File:Necropsar.png|thumb|left|1907 restoration by [[John Gerrard Keulemans]] (left), partially based on a specimen that turned out to be an albinistic [[grey trembler]] (right)]]
In 1898, the British naturalist [[Henry Ogg Forbes]] described a second species of ''Necropsar'', ''N. leguati'', based on a skin in the [[World Museum Liverpool]], specimen D.1792, which was labelled as coming from [[Madagascar]]. He suggested that this was actually the bird mentioned by Tafforet, instead of ''N. rodericanus'' from mainland Rodrigues.<ref>{{Cite journal  | last = Forbes  | first = H. O. | title = On an apparently new, and supposed to be extinct, species of bird from the Mascarene Islands, provisionally referred to the genus ''Necropsar''  | journal = Bulletin of the Liverpool Museums  | series =   | volume = 1  | pages = 28–35  | url = https://archive.org/stream/bulletinofliverp01forb#page/n50/mode/1up  | year = 1898 }}</ref> [[Walter Rothschild]], however, believed the Liverpool specimen to be an [[albinistic]] specimen of a ''Necropsar'' species supposedly from Mauritius.<ref name="Rothschild">{{Cite book  | last = Rothschild  | first = W.  | authorlink = Walter Rothschild, 2nd Baron Rothschild  | title = Extinct Birds  | publisher = Hutchinson & Co  | year = 1907  | location = London  | pages = 5–6  | url = https://archive.org/stream/extinctbirdsatte00roth#page/4/mode/2up}}</ref> In 1953, Japanese writer [[Masauji Hachisuka]] suggested that ''N. leguati'' was distinct enough to warrant its own genus, ''Orphanopsar''.<ref name="Hachisuka">{{cite book  | last = Hachisuka | first = M. |year=1953 |title=The Dodo and Kindred Birds, ''or'', The Extinct Birds of the Mascarene Islands |publisher=H. F. & G. Witherby |location=London}}</ref> In a 2005 DNA analysis, the specimen was eventually identified as an albinistic specimen of the [[grey trembler]] (''Cinclocerthia gutturalis'') from [[Martinique]].<ref name="Olson">{{Cite journal  | last = Olson  | first = S. L.  | last2 = Fleischer  | first2 = R. C. |  last3 = Fisher  | first3 = C. T. |last4 = Bermingham  | first4 = E. | title = Expunging the 'Mascarene starling' ''Necropsar leguati'': archives, morphology and molecules topple a myth  | journal = Bulletin of the British Ornithologists' Club  | series =   | volume = 125  | pages = 31–42  | url = http://www.biodiversitylibrary.org/page/40056328#page/363/mode/1up  | year = 2005 }}</ref>

Hachisuka believed the [[carnivorous]] habits described by Tafforet to be unlikely for a starling, and thought the lack of a crest suggested that it was not closely related to ''Fregilupus''. He was reminded of [[corvid]]s because of the black-and-white plumage, and assumed the bird seen by Tafforet was a sort of [[chough]]. In 1937, he named it ''Testudophaga bicolor'', and coined the common name "bi-coloured chough".<ref>{{Cite journal  | last =  Hachisuka | first =  M. | title =  Extinct chough from Rodriguez | journal =  Proceedings of the Biological Society of Washington | url =  http://www.biodiversitylibrary.org/page/34510472#page/241/mode/1up | volume =  50 | pages =  211–213  | year = 1937}}</ref> Hachisuka's assumptions are disregarded today, and modern ornithologists find Tafforet's bird to be identical to the one described from subfossil remains.<ref name="Hume 2014 44–51"/><ref name="Olson"/><ref name="Cheke87p49">{{Cite book| last1 = Cheke | first1 = A. S. | editor1-last = Diamond| editor1-first = A. W.| doi = 10.1017/CBO9780511735769.003 | chapter = An ecological history of the Mascarene Islands, with particular reference to extinctions and introductions of land vertebrates | title = Studies of Mascarene Island Birds | pages = 5–89 | year = 1987 | isbn = 978-0521113311| location = Cambridge | publisher = Cambridge University Press }}</ref>

In 1987, the British ornithologist Graham S. Cowles prepared a manuscript that described a new species of [[Old World babbler]], ''Rodriguites microcarina'', based on an incomplete sternum found in a cave on Rodrigues.  In 1989, the name was mistakenly published before the description, making it a [[nomen nudum]]. Later examination of the sternum by Hume showed that ''Rodriguites microcarina'' was identical to the Rodrigues starling.<ref name="Hume 2014 44–51"/>

===Evolution===
In 1943, the American ornithologist [[Dean Amadon]] suggested that ''[[Sturnus]]''-like species could have arrived in Africa, and given rise to the [[wattled starling]] (''Creatophora cinerea'') and the Mascarene starlings. According to Amadon, the Rodrigues and hoopoe starlings were related to Asiatic starlings, such as some species of ''Sturnus'', rather than the [[glossy starlings]] (''Lamprotornis'') of Africa and the [[Madagascan starling]] (''Saroglossa aurata''); he concluded this based on the colouration of the birds.<ref name="Amadon 1947">{{cite journal |author=Amadon, D. |author-link=Dean Amadon |year=1943 |title=Genera of starlings and their relationships |journal=[[American Museum Novitates]] |volume=1247 |pages=1–16 |url=http://digitallibrary.amnh.org/dspace/bitstream/2246/4757/1/N1247.pdf |format=PDF |ref=refAmadon47}}</ref><ref>{{cite journal |author=Amadon, D. |author-link=Dean Amadon |year=1956 |title=Remarks on the starlings, family Sturnidae |journal=[[American Museum Novitates]] |volume=1803 |pages=1–41 |url=http://digitallibrary.amnh.org/dspace/bitstream/2246/5410/1/N1803.pdf |format=PDF |ref=refAmadon56}}</ref> A 2008 study, which analysed the DNA of various starlings, confirmed that the hoopoe starling was a starling, but with no close relatives among the sampled species.<ref>{{Cite journal | doi = 10.1111/j.1463-6409.2008.00339.x| title = Phylogenetic relationships among Palearctic-Oriental starlings and mynas (genera ''Sturnus'' and ''Acridotheres'': Sturnidae)| journal = [[Zoologica Scripta]]| volume = 37| issue = 5| pages = 469–481| year = 2008| last1 = Zuccon | first1 = D. | last2 = Pasquet | first2 = E. | last3 = Ericson | first3 = P. G. P. }}</ref>

Extant East Asian starlings, such as the [[Bali myna]] (''Leucopsar rothschildi'') and the [[white-headed starling]] (''Sturnia erythropygia''), have similarities with these extinct species in colouration and other features. As the Rodrigues and Mauritius starlings seem to be more closely related to each other than to the hoopoe starling, which appears to be closer to Southeast Asian starlings, there may have been two separate colonisations of starlings in the Mascarenes from Asia, with the hoopoe starling being the latest arrival. Apart from Madagascar, the Mascarenes were the only islands in the south-west Indian Ocean that contained native starlings. This is probably due to the isolation, varied topography and vegetation of these islands.<ref name="Hume 2014 55–58"/>

==Description==
[[File:Rodrigues Starling.jpg|thumb|Hypothetical restoration, based on Tafforet's account, subfossils, and related species]]
The Rodrigues starling was large for a starling, being {{convert|25–30|cm|abbr=on|0}} in length. Its body was white or greyish white, with blackish-brown wings, and a yellow bill and legs.<ref name="Extinct Birds"/> Tafforet's complete description of the bird reads as follows:
{{Quotation|A little bird is found which is not common, for it is not found on the mainland. One sees it on the islet au Mât [Ile Gombrani], which is to the south of the main island, and I believe it keeps to that islet on account of the birds of prey which are on the mainland, as also to feed with more facility on the eggs of the fishing birds which feed there, for they feed on nothing else but eggs or turtles dead of hunger, which they well know how to tear out of their shells. These birds are a little larger than a blackbird [Réunion bulbul (''[[Hypsipetes borbonicus]]'')], and have white plumage, part of the wings and tail black, the beak yellow as well as the feet, and make a wonderful warbling. I say a warbling, since they have many and altogether different notes. We brought up some with cooked meat, cut up very small, which they eat in preference to seed.{{efn|name=Tafforet}}<ref name="Extinct Birds"/><ref>{{Cite book  | url = http://www.biodiversityheritagelibrary.org/item/99626#page/222/mode/1up | title = The Voyage of François Leguat of Bresse, to Rodriguez, Mauritius, Java, and the Cape of Good Hope | year = 1891 | last1 = Tafforet | first1 = J.  | editor1-last = Oliver | editor1-first = S. P. | volume = 2 | chapter = Relation de l'ile Rodrigue | location = London | publisher = Hakluyt Society }}</ref>}}
Tafforet was familiar with the fauna of Réunion, where the related hoopoe starling lived. He made several comparisons between the faunas of different locations, so the fact that he did not mention a crest on the Rodrigues starling indicates that it was absent. His description of their colouration is similar.<ref name="Hume 2014 44–51"/>
[[File:Necropsar rodericanus skull.png|thumb|upright|left|Skull and [[temporomandibular joint]]]]
Hume notes that the skull of the Rodrigues starling was about the same size as that of the hoopoe starling, but the skeleton was smaller. Though the Rodrigues starling was clearly able to fly, its sternum was smaller compared to that of other starlings; however, it may not have required powerful flight, due to the small area and topography of Rodrigues. The two starlings differed mainly in details of the skull, jaws, and sternum. The [[maxilla]] of the Rodrigues starling was shorter, less curved, had a less slender tip, and had a stouter mandible.<ref name="Hume 2014 44–51"/> Not enough remains of the Rodrigues starling have been found to assess whether it was [[sexually dimorphic]].<ref name="Greenway"/> Subfossils show a disparity in size between specimens, but this may be due to individual variation, as the differences are gradual, with no distinct size classes. There is a difference in bill length and shape between two Rodrigues starling specimens, which could indicate dimorphism.<ref name="Hume 2014 44–51"/>

Günther and Newton noted that the [[skull]] of the Rodrigues starling was shaped somewhat differently and longer than that of the hoopoe starling, being about {{convert|29|mm|in|abbr=on}} long from the [[occipital condyle]]; it was also narrower, being {{convert|21–22|mm|in|abbr=on}} wide. The eyes were set slightly lower, and the upper rims of the [[eye socket]]s were about {{convert|8|mm|in|abbr=on}} apart. The [[interorbital septum]] was more delicate, with a larger hole in its centre. The bill was about {{convert|36–39|mm|in|abbr=on}} long, less curved and proportionally a little deeper than in the hoopoe starling. It also seems to have had larger [[nostril]]s, with the nostril openings in the bone being {{convert|12–13|mm|in|abbr=on}} in length. The mandible was about {{convert|52–60|mm|in|abbr=on}} long and {{convert|4–5|mm|in|abbr=on}} deep [[Anatomical terms of location#Proximal and distal|proximally]]. The skull had an attachment scar above the [[temporal fossa]]. The [[supraoccipital ridge]] on the skull was quite strongly developed, and a [[biventer]] muscle attachment in the [[Parietal bone|parietal]] region below it was conspicuous. This indicates that the starling had strong neck and jaw muscles.<ref name="Günther"/>

According to Günther and Newton, the ulna of the Rodrigues starling was somewhat shorter than that of the hoopoe starling, measuring {{convert|37–40|mm|in|abbr=on}}; the humerus measured {{convert|32–35|mm|in|abbr=on}}, and the keel on its sternum was a bit lower. It had strong [[quill knob]]s on the ulna, indicating that the secondary [[remiges]] were well developed. One coracoid measured {{convert|27.5|mm|in|abbr=on}} in length, and one [[carpometacarpus]] was {{convert|22.5|mm|in|abbr=on}} long. The leg and feet had the same proportions. The femur measured around {{convert|33|mm|in|abbr=on}}, the [[tibiotarsus]] {{convert|52–59|mm|in|abbr=on}}, and the [[tarsometatarsus]] {{convert|36–41|mm|in|abbr=on}}.<ref name="Günther"/>

==Behaviour and ecology==
[[File:Leguat1891frontispieceFr1708.jpg|upright|thumb|Frontispiece to [[François Leguat]]'s 1708 memoir, showing his settlement on Rodrigues, with tortoises and rats at the bottom|alt=Drawing of houses on Rodrigues]]
Little is known about the behaviour of the Rodrigues starling, apart from Tafforet's description, from which various inferences can be made. The robustness of its limbs and the strong jaws with the ability to gape indicates that it foraged on the ground. Its diet may have consisted of the various snails and invertebrates of Rodrigues, as well as scavenged items.<ref name="Extinct Birds"/> Rodrigues had large colonies of seabirds and now-extinct ''[[Cylindraspis]]'' land tortoises, as well as marine turtles, which would have provided a large amount of food for the starling, particularly during the breeding seasons. Tafforet reported that the pigeons and parrots on the offshore southern islets only came to the mainland to drink water, and Leguat noted that the pigeons only bred on the islets due to persecution from rats on the mainland; the starling may have also done this. Originally, the Rodrigues starling may have been widely distributed on Rodrigues, with seasonal visits to the islets. Tafforet's description also indicates that it had a complex song.<ref name="Hume 2014 44–51"/>
[[File:Rodrigues.jpg|thumb|left|Leguat's map of pristine Rodrigues|alt=Map of Rodrigues, decorated with Solitaires]]
The stouter build and more bent shape of the mandible shows that the Rodrigues starling used greater force than the hoopoe starling when searching and perhaps digging for food. It probably also had the ability to remove objects and forcefully open entrances when searching for food; it did this by inserting its wedge-shaped bill and opening its mandibles, as other starlings and crows do. This ability supports Tafforet's claim that the bird fed on eggs and dead tortoises.<ref name="Günther"/> It could have torn dead, presumably juvenile, turtles and tortoises out of their shells. Tafforet did not see any Rodrigues starlings on the mainland, but he stated that they could easily be reared by feeding them meat, which indicates that he brought young birds from a breeding population on Île Gombrani.<ref name="Extinct Birds"/> Tafforet was marooned on Rodrigues during the summer and was apparently able to procure juvenile individuals; some other Rodrigues birds are known to breed at this time, so it is likely that the starling did the same.<ref name="Hume 2014 44–51"/>

Many other species endemic to Rodrigues became extinct after humans arrived, and the island's ecosystem is heavily damaged. Before humans arrived, forests completely covered the island, but very little remains today. The Rodrigues starling lived alongside other recently extinct birds, such as the [[Rodrigues solitaire]], the [[Rodrigues parrot]], [[Newton's parakeet]], the [[Rodrigues rail]], the [[Rodrigues owl]], the [[Rodrigues night heron]], and the [[Rodrigues pigeon]]. Extinct reptiles include the [[domed Rodrigues giant tortoise]], the [[saddle-backed Rodrigues giant tortoise]], and the [[Rodrigues day gecko]].<ref name="Lost Land">{{cite book
| last1 = Cheke | first1 = A. S.
 | first2 = J. P. | last2 = Hume
|year=2008
  |pages = 49–52
|title=Lost Land of the Dodo: an Ecological History of Mauritius, Réunion & Rodrigues
|publisher=T. & A. D. Poyser
|location=New Haven and London
|isbn=978-0-7136-6544-4}}</ref>

==Extinction==
Leguat mentioned that pigeons only bred on islets off Rodrigues, due to predation from rats on the mainland. This may be the reason why Tafforet only observed the Rodrigues starling on an islet. By Tafforet's visit in 1726, the bird must have either been absent or very rare on mainland Rodrigues. Rats could have arrived in 1601, when a Dutch fleet surveyed Rodrigues. The islets would have been the last refuge for the bird, until the rats colonised them, too. The Rodrigues starling was extinct by the time French scientist [[Alexandre Guy Pingré]] visited Rodrigues during the French [[Transit of Venus#1761 and 1769|1761 Transit of Venus expedition]].<ref name="Extinct Birds"/>

The large populations of tortoises and marine turtles on Rodrigues resulted in the export of thousands of animals, and cats were introduced to control the rats, but the cats attacked the native birds and tortoises as well. The Rodrigues starling was already extinct on the mainland by this time. Rats are adept at crossing water, and inhabit almost all islets off Rodrigues today. At least five species of ''[[Aplonis]]'' starlings have become extinct in islands of the Pacific Ocean, and rats also contributed to their demise.<ref name="Hume 2014 55–58">Hume, J. P. (2014). pp. 55–58.</ref>

==Notes==

{{notes
| notes =
{{efn
| name = Tafforet
| Tafforet's original French description is as follows:"On trouve un petit oiseau qui n'est pas fort commun, car il ne se trouve pas sur la grande terre; on en vout sur l'île au Mât, qui est au sud de la grande terre, et je crois qu'il se tient sur cette île à cause des oiseaux de proie qui sont à la grande terre, comme aussi pur vivre avec plus de facilité de oefs ou quelques tortues mortes de faim qu'ils savent assez bien déchirer. Ces ouiseaux sont un peu plus gros qu'un merle et ont le plumage blanc, une partie des aîles et de la queue noire, le bec jaune aussi bein que les pattes, et ont un ramage merveillex; je dis un ramage quoiqu'ils en aient plusieurs, et tous différents, et chacun de plus jolis. Nous en avons nourri quelques uns avec de la viande cuite hachée bien menu, qu'ils mangeaient préférablement aux graines de bois."<ref name="Rodriguez"/>
}}

}}

==References==
{{reflist|30em}}

==Works cited==
* {{Cite journal | doi = 10.11646/zootaxa.3849.1.1  | last = Hume | first =  J. P. | year = 2014 | title = Systematics, morphology, and ecological history of the Mascarene starlings (Aves: Sturnidae) with the description of a new genus and species from Mauritius | url=http://www.mapress.com/zootaxa/2014/f/z03849p075f.pdf | journal =[[Zootaxa]] | volume = 3849 | issue = 1 | pages = 1–75 |format=PDF }}

==External links==
*{{Commons category-inline|Necropsar rodericanus}}
*{{Wikispecies-inline|Necropsar rodericanus}}

{{Birds|state=collapsed}}
{{portalbar|Birds|Animals|Biology|Mauritius|Madagascar|Extinction|Paleontology}}
{{taxonbar}}

[[Category:Birds described in 1879]]
[[Category:Bird extinctions since 1500]]
[[Category:Birds of Mauritius]]
[[Category:Endemic fauna of Mauritius]]
[[Category:Extinct birds of Indian Ocean islands]]
[[Category:Fauna of Rodrigues]]
[[Category:Sturnidae]]
[[Category:Articles containing video clips]]