
{{Infobox company
|name          = RSM US LLP
|logo          = 
|type          = [[Limited Liability Partnership]] <br> Member firm of [[RSM International]]
|location              = [[One South Wacker]]<br>[[Chicago, Illinois|Chicago]], [[Illinois]], [[United States|USA]]
|foundation            = 1926 as IB McGladrey Co by Ira McGladrey
|area_served           = United States
|num_employees         = 9,000
|industry              = [[Professional services]]
|services              = [[Audit]]<br />[[Tax]]<br />[[Business Consulting|Consulting]]
|revenue               = {{increase}} $1.85 billion [[U.S. dollar|USD]] <small>(2016)</small>
|homepage              = http://www.rsmus.com
}}

'''RSM US''' LLP is an audit, tax and consulting firm, focused on the middle market in the [[United States]] and a member of the global accounting network [[RSM International]]. It is the 5th largest accounting firm in the United States.<ref>Accounting Today http://digital.accountin gtoday.com/accountingtoday/top_100_firms_supplement_2014#pg15</ref>

The firm has 9,000 employees across 86 cities nationwide providing [[audit]], [[tax]], and [[Consultant|consulting]] services. Through the RSM Alliance, the firm brings an affiliation of 75 independent accounting firms with 215 locations in 38 states and Puerto Rico. RSM International brings together more than 41,000 professionals from 760 offices located in more than 120 countries.

Attracting, retaining and continually developing a diverse team of talented professionals lies at the heart of RSM's commitment to diversity. The firm strives to create an inclusive workplace that celebrates the differences among its talented employees, while enabling them to embrace full ownership of their work and personal lives.

==History==

===Legal structure===
Through a 1999 partnership with [[H&R Block]], McGladrey operated under two separate legal entities:

* McGladrey & Pullen, LLP
* RSM McGladrey, Inc.

McGladrey & Pullen operated under the [[Limited Liability Partnership|traditional partnership structure]] as most accounting firms, specializing in audit and attest services. RSM McGladrey was established under the partnership agreement with H&R Block to provide tax and advisory services under an alternative practice structure.

In August 2011, H&R Block announced it was ending its partnership and [[divesting]] RSM McGladrey to McGladrey & Pullen, reuniting the firms in a traditional partnership structure. McGladrey & Pullen acquired all employees, assets, and infrastructure from RSM McGladrey. The deal closed on December 1, 2011.

In May 2012, McGladrey & Pullen officially changed its name to McGladrey LLP.  The change took effect at the start of the new fiscal year, May 1.

In July 2012, McGladrey changed its headquarters from [[Bloomington, Minnesota]] to [[Chicago]].<ref>{{cite web|url=http://www.chicagobusiness.com/article/20120718/NEWS01/120719764/accounting-firm-mcgladrey-shifts-hq-to-chicago |title=Accounting firm McGladrey shifts HQ to Chicago |publisher=Chicagobusiness.com |date= |accessdate=2014-01-23}}</ref>

On October 26, 2015 McGladrey LLP changed its name to RSM US LLP. The name change creates a more cohesive global brand with their RSM International(RSMi) network.<ref>{{cite web|url=http://www.accountingtoday.com/news/firm-profession/mcgladrey-to-rebrand-as-rsm-74874-1.html |title=McGladrey to Rebrand as RSM |publisher=Accountingtoday.com |date= |accessdate=2015-07-17}}</ref> McGladrey was an original founding member of RSMi in 1964 (being the "M" in RSM).

==Firm accolades==

In 2016, ''Consulting Magazine'' named RSM #5 Best Firms to Work For.<ref>[http://www.consultingmag-digital.com/consultingmag/september_2016?pg=20#pg20 ]</ref>
In 2016, Forbes named RSM one of America’s Best Management Consulting Firms; Fatherly.com named RSM a 2016 ‘Best Places to Work for New Dads;’ and RSM was recognized by the Forum for Expatriate Management (FEM) as Tax Provider of the Year, and presented with the organization’s esteemed Expatriate Management and Mobility Award (EMMA).

In 2015, prior to the firm's rebrand, RSM (formerly McGladrey) was named to Working Mother's 2015 'Best Companies" list; the AICPA named the firm's Jan Phipps its Woman CPA of the Year; McGladrey takes top spot on Accounting Today's 2015 VAR 100 list; Consulting® Magazine Recognized the firm for its Community Outreach; 
Computerworld Named McGladrey to 2015 List of Best Places to Work in IT; and Accounting Today ranked the firm No. 5 on Top 100 Firms List.

In 2014, the firm was recognized by Working Mother magazine as a Working Mother Company of the Year for 2014, putting the firm in the Top 100 list for the eighth consecutive year. The firm also received the publication’s Candidate Experience award; the firm also joined the 2014 Microsoft Dynamics Inner Circle, President’s Club; was named as a finalist in the American Business Awards’ 12th Annual Competition; was named as one of the Best Companies in Massachusetts for Working Moms; and was ranked by Accounting Today as fifth on its annual list of Top 100 firms for the eighth consecutive year.

==Team RSM==
RSM US [[sponsor (commercial)|sponsors]] three [[professional golfer]]s: [[PGA Tour]] professionals [[Zach Johnson]], [[Chris DiMarco]] and [[Davis Love III]].<ref>{{cite press release |author=<!--Staff writer(s); no by-line.--> |title= McGladrey Inks Deal with PGA TOUR Veteran Davis Love III Makes Team RSM a Foursome |url= http://mcgladrey.com/content/mcgladrey/en_US/newsroom/news-releases/2010/mcgladrey-inks-deal-with-pga-tour-veteran-davis-love-iii-makes-team-mcgladrey-a-foursome.html|publisher= McGladrey|date= July 6, 2010|accessdate=2014-03-07}}</ref> It also sponsors a PGA Tour event, the [[RSM Classic]], and European Tour players [http://rsmus.com/newsroom/golf-partnership/team-mcgladrey/paul-lawrie.html Paul Lawrie] and [http://rsmus.com/newsroom/golf-partnership/team-mcgladrey/andy-sullivan.html Andy Sullivan].

==References==
{{reflist}}

==External links==
* [http://rsmus.com/newsroom/golf-partnership/team-mcgladrey/paul-lawrie.html]
*[http://www.rsmus.com]
*[https://plus.google.com/115895009069861191747/about]

[[Category:Accounting firms of the United States]]
[[Category:Companies based in Chicago]]
[[Category:RSM]]