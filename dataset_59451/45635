{{other people|Geoffrey Taylor}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox scientist
| name              = Sir Geoffrey Ingram Taylor
| image             = G I Taylor.jpg
| image_size       = 250px
| caption           = Sir Geoffrey Ingram Taylor
| birth_date        = {{birth date|df=yes|1886|3|7}}
| birth_place       = [[St. John's Wood]], [[Middlesex]], England
| death_date        = {{death date and age|df=yes|1975|6|27|1886|3|7}}
| death_place       = [[Cambridge]], [[Cambridgeshire]], England
| residence         =
| citizenship       =
| nationality       = British
| ethnicity         =
| field             = [[Physics]]<br> Mathematics<br> [[Fluid mechanics]] <br> [[Fluid dynamics]] <br> [[Solid mechanics]] <br> [[Wave theory]]
| work_institutions =
| alma_mater        = [[Trinity College, Cambridge]]
| academic_advisors = [[J. J. Thomson]]<ref name="mactutor">{{MacTutor Biography|id=Taylor_Geoffrey}}</ref><ref name="mathgene">{{MathGenealogy|id=18588}}</ref>
| doctoral_students = [[George Batchelor]]<br>[[Philip Drazin]]<br>[[Albert E. Green]]<br>[[Francis Bretherton]]<br>Walter Freiberger<br>Rosa Morris
| known_for         = [[Taylor cone]]<br>[[Taylor dispersion]]<br>[[Taylor number]]<br>[[Taylor vortex]]<br>[[Taylor–Couette flow]]<br>[[Taylor–Goldstein equation]]<br>[[Rayleigh–Taylor instability]]<br>[[Taylor–Proudman theorem]]<br>[[Taylor–Green vortex]]<br>[[Taylor microscale]]<br>[[Taylor column]]<br>[[Double-slit experiment]]
| prizes            = [[Knight Bachelor]]<br>[[Royal Medal]] {{small|(1933)}}<br>[[Copley Medal]] {{small|(1944)}}<br>[[Order of Merit]]<br>[[Wilhelm Exner Medal]] <small>(1954)</small><br>[[De Morgan Medal]] {{small|(1956)}}<br>[[Timoshenko Medal]] {{small|(1958)}}<br>[[Franklin Medal]] {{small|(1962)}}<br>[[Royal Society|FRS]]<ref name="frs">{{Cite journal | last1 = Batchelor | first1 = G. K. | doi = 10.1098/rsbm.1976.0021 | title = Geoffrey Ingram Taylor 7 March 1886 -- 27 June 1975 | journal = [[Biographical Memoirs of Fellows of the Royal Society]] | volume = 22 | pages = 565 | year = 1976 | pmid =  | pmc = }}</ref><br>[[Theodore von Karman Medal]]  {{small|(1969)}}
}}
'''Sir Geoffrey Ingram Taylor''' [[Order of Merit|OM]] (7 March 1886 – 27 June 1975) was a British [[physicist]] and mathematician, and a major figure in [[fluid dynamics]] and [[wave theory]]. His biographer and one-time student, [[George Batchelor]], described him as "one of the most notable scientists of this (the 20th) century".<ref>''The Life and Legacy of G. I. Taylor'', by [[George Batchelor]], [[Cambridge University Press]], 1994 ISBN 0-521-46121-9</ref><ref>Taylor, Geoffrey Ingram, Sir, ''Scientific papers''. Edited by G.K. Batchelor, Cambridge University Press 1958–71. (Vol. 1. Mechanics of solids – Vol. 2. Meteorology, oceanography, and turbulent flow – Vol. 3. Aerodynamics and the mechanics of projectiles and explosions – Vol. 4. Mechanics of fluids: miscellaneous papers).</ref><ref>{{Cite book | doi = 10.1016/S0065-2156(08)70086-3 | chapter = G.I. Taylor as I Knew Him | title = Advances in Applied Mechanics Volume 16 | series = Advances in Applied Mechanics | volume = 16 | pages = xii- | year = 1976 | isbn = 9780120020164 | pmid =  | pmc = }}</ref><ref name="physicstoday">{{Cite journal | last1 = Pippard | first1 = S. B. A. | title = Sir Geoffrey Taylor | doi = 10.1063/1.3069178 | journal = Physics Today | volume = 28 | issue = 9 | pages = 67–61 | year = 1975 | pmid =  | pmc = |bibcode = 1975PhT....28i..67P }}</ref>

==Biography==
Taylor was born in [[St. John's Wood]], London. His father, Edward Ingram Taylor, was an artist, and his mother, Margaret Boole, came from a family of mathematicians (his aunt was [[Alicia Boole Stott]] and his grandfather was [[George Boole]]).  As a child he was fascinated by science after attending the [[Royal Institution Christmas Lectures]], and performed experiments using paint rollers and sticky-tape. Taylor read mathematics at [[Trinity College, Cambridge]].

His first paper{{citation needed|date=January 2017}} was on [[quantum|quanta]] showing that [[Double-slit experiment|Young's slit diffraction experiment]] produced fringes even with feeble light sources such that less than one [[photon]] on average was present at a time. He followed this up with work on [[shock wave]]s,{{citation needed|date=January 2017}} winning a [[Smith's Prize]]. In 1910 he was elected to a Fellowship at Trinity College, and the following year he was appointed to a [[meteorology]] post, becoming [[Reader (academic rank)|Reader]] in Dynamical Meteorology. His work on [[turbulence]] in the atmosphere led to the publication of "Turbulent motion in fluids",{{citation needed|date=January 2017}} which won him the [[Adams Prize]] in 1915.

In 1913 Taylor served as a meteorologist aboard the [[International Ice Patrol|Ice Patrol]] vessel ''Scotia'', where his observations formed the basis of his later work on a theoretical model of mixing of the air. At the outbreak of [[World War I]], he was sent to the [[Royal Aircraft Factory]] at [[Farnborough, Hampshire|Farnborough]] to apply his knowledge to aircraft design, working, amongst other things, on the stress on propeller shafts. Not content just to sit back and do the science, he also learned to fly aeroplanes and make parachute jumps.

After the war Taylor returned to Trinity and worked on an application of turbulent flow to [[oceanography]]. He also worked on the problem of bodies passing through a rotating fluid. In 1923 he was appointed to a [[Royal Society]] research professorship as a Yarrow Research Professor. This enabled him to stop teaching, which he had been doing for the previous four years, and which he both disliked and had no great aptitude for. It was in this period that he did his most wide-ranging work on [[fluid mechanics]] and [[solid mechanics]], including research on the deformation of crystalline materials which followed from his war work at Farnborough. He also produced another major contribution to [[turbulent flow]], where he introduced a new approach through a statistical study of velocity fluctuations.

In 1934, Taylor, roughly contemporarily with [[Michael Polanyi]] and [[Egon Orowan]], realised that the [[plasticity (physics)|plastic]] deformation of [[ductile]] materials could be explained in terms of the theory of [[dislocation]]s developed by [[Vito Volterra]] in 1905. The insight was critical in developing the modern science of solid mechanics.

===Manhattan Project===
During [[World War II]], Taylor again applied his expertise to military problems such as the propagation of [[blast wave]]s, studying both waves in air and [[underwater explosion]]s. Taylor was sent to the United States in 1944–1945 as part of the [[British contribution to the Manhattan Project|British delegation]] to the [[Manhattan Project]]. At [[Los Alamos National Laboratory|Los Alamos]], Taylor helped solve implosion instability problems in the development of atomic weapons particularly the plutonium bomb used at Nagasaki on August 9th, 1945. 

In 1944 he also received his knighthood and the [[Copley Medal]] from the [[Royal Society]].

Taylor was present at the [[Trinity (nuclear test)]], July 16, 1945, as part of General [[Leslie Groves]]' "VIP List" of just 10 people who observed the test from Compania Hill, about 20 miles (32&nbsp;km) northwest of the shot tower. By a strange twist, [[Joan Hinton]], another direct descendant of  the mathematician, George Boole, had been working on the same project and witnessed the event in an unofficial capacity. They met at the time but later followed different paths: Joan, strongly opposed to nuclear weapons, to defect to Mao's China, Taylor to hold throughout his career the view that governmental policy was not within the remit of the scientist. <ref> Gerry Kennedy, The Booles and the Hintons, Atrium Press, July 2016 </ref> 

In 1950, he published two papers estimating the yield of the explosion using the [[Buckingham Pi theorem]], and high speed photography stills from that test, bearing timestamps and physical scale of the blast radius, which had been published in [[Life (magazine)|Life]] magazine.  His estimate of 22 kt was remarkably close to the accepted value of 20 kt, which was still [[Classified information|highly classified]] at that time.

===Later life===
Taylor continued his research after the war, serving on the [[Aeronautical Research Committee]] and working on the development of [[supersonic]] aircraft. Though he officially retired in 1952, he continued research for the next twenty years, concentrating on problems that could be attacked using simple equipment. This led to such advances as a method for measuring the second coefficient of [[viscosity]].{{fact|date=January 2017}} Taylor devised an incompressible liquid with separated gas bubbles suspended in it.{{fact|date=January 2017}} The dissipation of the gas in the liquid during expansion was a consequence of the shear viscosity of the liquid. Thus the bulk viscosity could easily be calculated. His other late work{{citation needed|date=January 2017}} included the longitudinal dispersion in flow in tubes, movement through porous surfaces, and the dynamics of sheets of liquids.

Aspects of Taylor's life often found expression in his work. His over-riding interest in the movement of air and water, and by extension his studies of the movement of unicellular marine creatures and of weather, were related to his lifelong love of sailing. In the 1930s he invented the [[Anchor|'CQR' anchor]], which was both stronger and more manageable than any in use, and which was used for all sorts of small craft including [[seaplane]]s.<ref>Taylor, G. I., [http://www.petersmith.net.nz/boat-anchors/docs/taylor-the-holding-power-of-anchors-1934.pdf The Holding Power of Anchors] April 1934</ref>

His final research paper {{citation needed|date=January 2017}}was published in 1969, when he was 83. In it he resumed his interest in electrical activity in [[thunderstorm]]s, as jets of conducting liquid motivated by electrical fields. The cone from which such jets are observed is called the [[Taylor cone]] for him. In the same year Taylor received the [[A. A. Griffith Medal and Prize]] and was appointed to the [[Order of Merit]]. He suffered a stroke in 1972 which effectively put an end to his work; he died in Cambridge in 1975.

==References==
{{reflist}}

==External links==
* A [http://modular.mit.edu:8080/ramgen/ifluids/Low_Reynolds_Number_Flow.rm Real Media stream] of Taylor's Hydrodynamic demo, courtesy of [[MIT]]
* [http://www.deas.harvard.edu/brenner/taylor/ Classical Physics Through the Work of GI Taylor]. Course given on Taylor's work
* [http://www.deas.harvard.edu/brenner/taylor/physic_today/taylor.htm Article on the course above]
* [http://www.sesinc.org/?q=node/1190 G.I. Taylor Medal] of the Society of Engineering Science
* [https://web.archive.org/web/20110321043741/http://etc13.fuw.edu.pl:80/historical-turbulence Video recording of K.R. Sreenivasan's lecture on the life and work of G.I. Taylor]

{{Copley Medallists 1901-1950}}

{{Authority control}}

{{DEFAULTSORT:Taylor, Geoffrey Ingram}}
[[Category:English mathematicians]]
[[Category:English physicists]]
[[Category:English meteorologists]]
[[Category:English inventors]]
[[Category:Manhattan Project people]]
[[Category:Alumni of Trinity College, Cambridge]]
[[Category:Fellows of Trinity College, Cambridge]]
[[Category:Fellows of the Royal Society]]
[[Category:Members of the Order of Merit]]
[[Category:Knights Bachelor]]
[[Category:Recipients of the Copley Medal]]
[[Category:Royal Medal winners]]
[[Category:1886 births]]
[[Category:1975 deaths]]
[[Category:20th-century mathematicians]]
[[Category:People educated at University College School]]
[[Category:Aerodynamicists]]
[[Category:Fluid dynamicists]]