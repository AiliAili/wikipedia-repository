{{Use mdy dates|date=March 2017}}
{{Infobox scientist
| name = Gabriel Aeppli
| image = 
| caption =
| birth_date = {{Birth date and age|df=yes|1956|11|25}}
| birth_place = [[Zürich]]
| death_date =
| death_place = 
| residence = [[Zürich]], [[Switzerland]]
| nationality = [[United States|American]] [[Swiss]]
| fields = {{Plainlist|
* [[Magnetism]] 
* [[Quantum Information]]}}
| work_institution = {{Plainlist|
* [[Massachusetts Institute of Technology]]
* [[University College London]]
* [[ETH Zurich]]
* [[École Polytechnique Fédérale de Lausanne]]
}}
| doctoral_advisor = 
| thesis_title = 
| thesis_url = 
| thesis_year =
| alma_mater = 
| known_for  = 
| archiveurl=
| prizes = 
| website =
}}

'''Gabriel Aeppli''', [[PhD]] [[Fellow of the Royal Society|FRS]] (born 25 November 1956 in [[Zurich]])  is a Swiss-American physicist, co-founder of the [[London Centre for Nanotechnology]] and professor of physics at ETH Zürich and EPF Lausanne, and head of the Synchrotron and Nanotechnology department of the Paul Scherrer Institute, also in Switzerland.<ref>http://www.ethlife.ethz.ch/archive_articles/130927_professorenernennung_ETH_Rat/index_EN</ref><ref>http://search.epfl.ch/ubrowse.action?acro=UPAEPPLI</ref>

He has contributed to spectroscopy on the magnetism of disordered systems and on high-temperature superconductors and [[antiferromagnetism]] identifying magnets with tuneable quantum fluctuations that can be used to study the transition between classical and quantum behavior. His work has helped to demonstrate that quantum spin fluctuations underlie exotic superconductivity.

He has been recipient of multiple honors and he has more than 290 [[peer review]] publications, 14054 total citations by 9542 documents and [[h-index]] of 65.

== Life ==
Son of mathematician Alfred Aeppli and Dorothee Aeppli, Gabriel was born in [[Zürich]] 25 November 1956. He has two brothers, Jacob and Andreas.<ref>http://www.legacy.com/Obituaries.asp?Page=LifeStory&PersonId=117629234</ref>

Shortly after birth, Gabriel Aeppli moved from [[Zürich]], with his father, to the United States.<ref>[http://www.time.com/time/magazine/article/0,9171,1156575,00.html Time Magazine 2006]</ref>

He lived in [[London|London UK]] from 2002 to 2015 when he moved back to [[Zürich]].

== Career ==
He studied at the  [[Massachusetts Institute of Technology]], where he in 1978 obtained a B.Sc. in Mathematics. and PhD, M.Sc. & B.Sc in Electrical Engineering (1983).

He was a research assistant at MIT and industrial co-op -Student of IBM. From 1982 he was at the Bell Laboratories hired in 1993 as a Distinguished Member of the Technical Staff.
From 1996 to 2002, he was a Senior Research Scientist at the NEC Laboratories in Princeton.

From 2002, as [[Quain Professor]] of Physics at University College London (UCL,) he helped to found the [[London Centre for Nanotechnology]], where he acted as director until March 2015.<ref>https://www.london-nano.com/news-and-events/news/professor-gabriel-aeppli-steps-down-as-director-of-the-lcn</ref>

His current technical focus is on the implications of photon science and nanotechnology for information processing and health care. He is a board member of Bio Nano Consulting.<ref>http://www.bio-nano-consulting.com/AboutUs/TheBoard/Gabriel_Aeppli.aspx</ref><ref>http://companycheck.co.uk/director/912493383</ref>

Currently, he is the director of Synchrotron Radiation and Nanotechnology  at the Paul Scherrer Institute.<ref>http://www.psi.ch/organisational-structure</ref>

== Research ==

* [[Kondo insulator]]
* [[Scale-free network]]

== Honors ==

* 2015 Fellow National Academy of Sciences<ref>https://royalsociety.org/people/gabriel-aeppli-10967/</ref>
* 2012 Member of the American Academy of Arts and Sciences
* 2010 Fellow of the Royal Society 
* 2008 Institute of Physics: Mott Prize
* 2005 American Physical Society: Oliver E. Buckley Condensed Matter Prize<ref>http://www.aps.org/programs/honors/prizes/prizerecipient.cfm?last_nm=Aeppli&first_nm=Gabriel&year=2005</ref>
* 2005 Majumdar Memorial Award of the Indian Association for the Cultivation of Science
* 2003 Neel Medal (magnetism price of IUPAP)
* 2002 Wolfson Research Award of the Royal Society
* 2002 Riso National Laboratory Fellow
* 2002 Royal Society Wolfson Research Merit Award
* 1997 Fellow of the American Physical Society
* 1996 Fellow of the Japan Society for the Promotion of Science

In addition, he has been a member and chairman of many panels, sponsored by the USDOE, American Physical Society, EPSRC, and National Research Council (US), among others.<ref>http://www.kni.caltech.edu/calendar/workshops/frontiers-2014/Aeppli_G_2012bio.pdf</ref>

==Books==

* "Neutron Scattering from Random Ferromagnets", Gabriel Aeppli, 1982 - 310 pages
* "Quantum Phase Transitions in Transverse Field Models", Amit Dutta, Gabriel Aeppli, Bikas K. Chakrabarti, Uma Divakaran, Thomas F. Rosenbaum, Diptiman Sen, Cambridge University Press, 28 Jan 2015 - Science - 360 pages

==References==
{{reflist}}

{{FRS 2010}}
{{Authority control}}

{{DEFAULTSORT:Aeppli, Gabriel}}
[[Category:1956 births]]
[[Category:Living people]]
[[Category:Massachusetts Institute of Technology alumni]]
[[Category:American physicists]]
[[Category:Fellows of the Royal Society]]