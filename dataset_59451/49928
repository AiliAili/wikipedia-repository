{{for|the 1978 French film|One Page of Love}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Une page d'amour
| title_orig    = 
| translator    = 
| image         = <!--prefer 1st edition-->
| caption = 
| author        = [[Émile Zola]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = [[Les Rougon-Macquart]]
| genre         = [[Novel]]
| publisher     = Charpentier (book form)
| release_date  = 1877-1878 (serial) & 1878 (book form)
| english_release_date =
| media_type    = Print ([[Serial (literature)|Serial]], [[Hardcover|Hardback]] & [[Paperback]])
| pages         = 
| isbn          =
| preceded_by   = [[L'Assommoir]]
| followed_by   = [[Nana (novel)|Nana]]
}}
'''''Une page d'amour''''' is the eighth novel in the [[Les Rougon-Macquart|Rougon-Macquart]] series by [[Émile Zola]], set among the [[petite bourgeoisie]] in [[Second French Empire|Second Empire]] suburban [[Paris]]. It was first serialized between December 11, 1877, and April 4, 1878, in ''Le Bien public,'' before being published in novel form by Charpentier in April 1878.

The central character of the novel is Hélène Grandjean ''née'' Mouret (b. 1824), first introduced briefly in ''[[La Fortune des Rougon|La fortune des Rougon]]''. Hélène is the daughter of Ursule Mouret ''née'' Macquart, the illegitimate daughter of Adelaïde Fouque (Tante Dide), the ancestress of the Rougon-Macquart family. Hélène's brothers are François Mouret, the central character of ''[[La Conquête de Plassans|La conquête de Plassans]]'', and Silvère Mouret, whose story is told in ''La fortune des Rougon''.

==Plot summary==
The story takes place in 1854-1855. When the novel begins, Hélène has been widowed 18 months, living in what was then the Paris suburb of [[Passy]] with her 11-year-old daughter Jeanne. Her husband Charles Grandjean fell ill the day after they arrived from [[Marseilles]] and died eight days later. Hélène and Jeanne have only been into Paris proper three times. From the window of their home, they can see the entire city, which takes on a dreamlike, foreign, and romantic, yet inaccessible, character for them throughout the novel.

On the night the novel opens, Jeanne has fallen ill with a violent seizure. In panic, Hélène runs into the street to find a doctor. Eventually, she begs her neighbor Dr. Henri Deberle to come attend Jeanne, and his ministrations save the girl's life. Later that week, Hélène goes to thank Dr. Deberle, and befriends his wife Juliette and her circle of friends, including Monsieur Malignon, a handsome, wealthy man-about-town who is exceptionally comfortable in female society.

Hélène's only friends are a pair of stepbrothers who were friends of her husband's: Abbé Jouve, the officiating priest at the parish church of Passy, and Monsieur Rambaud, an oil and produce merchant. The Abbé asks Hélène to visit one of his invalid parishioners, Mother Fétu. While Hélène is at her squalid apartment, Dr. Deberle pays a medical call. Mother Fétu immediately realizes that the Hélène and Deberle know each other and, seeing them so shy with one another, she immediately begins to attempt to bring them together. At a later visit, Mother Fétu arranges to leave the two of them alone together, but Dr. Deberle leaves before either can express their attraction.

Juliette throws a party for the wealthy children of the neighborhood. At the party, Dr. Deberle passionately confesses to Hélène in private that he loves her. She leaves the party in confusion. On contemplating her life, Hélène realizes that she has never really been in love; though she respected her late husband, she felt no love or passion for him. She finds, however, that she is falling in love with Dr. Deberle.

During May, Hélène and Jeanne begin attending church, where they regularly meet Juliette. Dr. Deberle frequently meets them after church ostensibly in order to escort his wife home, and continues to act as escort even on those evenings when Juliette doesn't attend services. At the end of the month, after Hélène's passion for Dr. Deberle is replaced by a passion for the church, Jeanne has another seizure. Her illness lasts three weeks, during which she is assiduously attended by Hélène and Dr. Deberle to the exclusion of all others. At last, the Doctor uses [[leeches]] and Jeanne recovers. Having saved her daughter's life, Hélène admits that she loves the Doctor.

However, as Jeanne recuperates during the ensuing months, she witnesses Hélène and the Doctor talking quietly together and realizes that he is taking her place in Hélène's affections. She is then consumed by intense jealousy and refuses to see him. The symptoms of her illness return whenever he is present, until at last Hélène drives him from her home.

Hélène realizes that Malignon has been pursuing Juliette and the two are planning an assignation. She learns from Mother Fétu that Malignon has taken rooms in her building, and guesses that this will be the place where he and Juliette will meet. When Hélène goes out ostensibly to bring Mother Fétu some shoes, but in reality to look at the rooms (Mother Fétu thinks she is arranging a place for Hélène and the Doctor to meet), Jeanne is extraordinarily distressed to be left alone, especially because Hélène gives no explanation for not taking her along.

The next day, Hélène attempts to warn Juliette not to keep her rendezvous with Malignon, scheduled for that afternoon, but she is unable to do so. Hélène slips a note into the Doctor's pocket with the address and time of the assignation. That afternoon, she decides to go to the apartment and stop the rendezvous, but before she can go, Jeanne insists on going with her. Hélène tells her she cannot go, and Jeanne becomes hysterical at being left and at being lied to. She says that she will die if she is left behind. Hélène goes anyway. At the apartment, she is met by Mother Fétu, who, feeling she has played the part of Hélène's procuress and confidante, lets her into the apartment with a knowing glance. Hélène successfully stops the rendezvous, but just as the prospective lovers part, Henri enters. He thinks that Hélène has arranged for them to be alone together. Hélène gives in to her feelings, and the two of them make passionate love at last.

Meanwhile, Jeanne, left alone, furious and confused and jealous, makes herself sick by hanging her arms out of her bedroom window in the rain. Growing increasingly lethargic and listless, she believes her mother does not care for her anymore, especially after witnessing her mother and Dr. Deberle exchange silent, knowing glances while planning a family excursion to Italy. Eventually, she falls seriously ill, and Deberle diagnosis her with galloping [[tuberculosis|consumption]] (the same disease her grandmother Ursule died of) and gives her three weeks to live. In due course, she dies. Hélène is completely grief-stricken, feeling responsible for her daughter's death. Two years later, she marries M. Rambaud and the two return to Marseilles.

The novel is unusual among Zola's Rougon-Macquart series, with an uncharacteristic absence of social critique, and an intense focus on Helene; even Dr Deberle remains a sketchy figure. Jeanne may be a victim, but perhaps also Helene, repressed by her circumstances at every stage of her dutiful life.

== Relation to the other Rougon-Macquart Novels ==

Zola's plan for the Rougon-Macquart novels was to show how [[heredity]] and environment worked on the members of one family over the course of the Second Empire. In ''Une page d'amour'', he specifically links Jeanne with her great-grandmother, the family ancestress Adelaïde Fouque (Tante Dide), who was possessed by the same seizures, and her grandmother Ursule, who died of the same disease.

In ''[[Le Docteur Pascal|Le docteur Pascal]]'', Zola described the influence of heredity on Hélène as "innateness," a "chemical blending in which the physical and moral natures of the parents are so amalgamated that nothing of them seems to subsist in the offspring." She is one of the "normal" members of the family. Jeanne is described as an instance of "reverting heredity," where the family neuroses have skipped one or more generations. Jeanne thus inherits her grandmother Ursule's "brain affection," which Ursule inherited from her mother Tante Dide. The descriptions of Tante Dide's seizures in ''La fortune des Rougon'' are similar to the descriptions of Jeanne's seizures in ''Une page d’amour''. Jeanne probably also inherited her grandfather's (Ursule's husband, the [[hatter]] Mouret) tendency to [[Obsessive-compulsive disorder|obsession]] (Mouret hanged himself, a year after his wife's death, in a cupboard where her dresses were still hanging), a characteristic she also shares with her uncle François in ''La conquête de Plassans''.

In ''Le docteur Pascal'', Zola tells us that Hélène and Rambaud continue to live in Marseilles (this novel is set in 1872). They have no children.

==Adaptations==
''Une page d'amour'' has had several filmed adaptations:
* ''Una pagina d'amore'', a 1912 Italian film<ref>{{IMDb title|id=1227889|title=Una pagina d'amore}}</ref>
* ''Una pagina d'amore'', a 1923 Italian film<ref>{{IMDb title|id=0963323|title=Una pagina d'amore}}</ref>
* Une page d'amour, a 1980 French TV film  featuring [[Anouk Aimée]]<ref>{{IMDb title|id=0081683|title=Une page d'amour}}</ref>
* Une page d'amour, a 1995 French TV film featuring [[Miou-Miou]]<ref>{{IMDb title|id=0240151|title=Une page d'amour}}</ref>

==Sources==
*Brown, F. (1995). ''Zola: A life''. New York: Farrar, Straus & Giroux.
*Zola, E. ''Une page d'amour'', translated as ''A Love Episode'' by C.C. Starkweather (1910).
*Zola, E. ''Le doctor Pascal'', translated as ''Doctor Pascal'' by [[E. A. Vizetelly]] (1893).

==External links==
{{Gutenberg|no=8561|name=Une Page d'Amour}} (French)
{{Gutenberg|no=13695|name=A Love Episode}} (English)

==References==
{{reflist}}

{{Les Rougon-Macquart}}

{{DEFAULTSORT:Page d'amour}}
[[Category:1878 novels]]
[[Category:Novels by Émile Zola]]
[[Category:Books of Les Rougon-Macquart]]
[[Category:Novels first published in serial form]]
[[Category:Works originally published in French magazines]]
[[Category:1854 in fiction]]
[[Category:Novels set in Paris]]
[[Category:1855 in fiction]]
[[Category:French novels adapted into films]]