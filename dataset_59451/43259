<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Kittiwake
 | image=Mitchell Kittiwake 1 G-ATXN ABIN 11.05.03R edited-2.jpg
 | caption=The prototype Kittiwake 1 attending a rally at [[RAF Abingdon]] in 2003
}}{{Infobox Aircraft Type
 | type=Sports aircraft
 | national origin=[[United Kingdom]]
 | manufacturer=Mitchell-Procter Aircraft (prototype Kittiwake I)<br />Robinson Aircraft (prototype Kittiwake II) 
 | designer=C. G. B. Mitchell
 | first flight=23 May 1967
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=4
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=[[Nash Petrel]]
}}
|}

The '''Mitchell Kittiwake''' is a [[United Kingdom|British]] single engine sporting aircraft designed for [[Homebuilt aircraft|amateur building]]. Plans were available for both single-seat and two-seat versions, but only four were constructed.

==Development==

Mitchell-Procter Aircraft was set up to produce the Kittwake prototype. This single-seat sports aircraft was a development of the Mitchell-Prizeman Scamp design study that was placed third in the Rollason Midget Racer Competition of 1964.  C. G. B. Mitchell was the Kittiwake's designer, with R. G. Procter in charge of building it.<ref name="JAWA66"/> The '''Mitchell-Procter Kittiwake&nbsp;I''' first flew in May 1967, but about 17 months later the partnership was dissolved and plans for home builders were produced by Procter Aircraft Associates.<ref>{{cite journal|magazine=Air Trails|date=Winter 1971|page=78}}</ref>  Mitchell concentrated on the design of a two-seat development, the '''Mitchell Kittiwake&nbsp;II''', with Robinson Aircraft building the prototype.<ref name="JAWA70"/> At about the same time Procter Aircraft were designing their own rather larger two-seat Kittiwake I development, the [[Nash Petrel|Procter Petrel]].<ref name="JAWA70P"/>  Both the two seaters, like the Kittiwake&nbsp;I, were intended for home building.<ref name="JAWA70"/>

The single-seat Kittiwake I [[monoplane]] was designed for sports flying and as a glider tug. It is an all-metal aircraft, with low  cantilever wings of parallel [[chord (aircraft)|chord]] built around a single spar carrying 5° of [[dihedral (aircraft)|dihedral]]. NACA single [[flap (aircraft)#Types|slotted flaps]] occupy the whole of the trailing edge inboard of the ailerons. The wings attach to a centre section which is integral with the fuselage, a feature intended to help construction in a small space like a garage.<ref name="JAWA70"/>  The straight tapered fin carries a [[balanced rudder|horn balanced rudder]] and the constant chord tailplane has a starboard side [[trim tab]].<ref name="JAWA66"/>

The Kittiwake's [[fuselage]] is built around four longerons, with flat sides and bottom and single curvature decking.  Its overwing cockpit has a rearward sliding canopy and its fixed [[tricycle undercarriage]] has cantilever angled steel spring main legs attached to the lower longerons, giving a track of 5&nbsp;ft 9 in (1.75 m). The Kittiwake I was powered by a 100&nbsp;hp (75&nbsp;kW) [[Continental O-200]] flat four engine.<ref name="JAWA70"/>

The Kittiwake II differs chiefly in having two [[side-by-side seating]] and a more powerful 130&nbsp;hp (97&nbsp;kW) [[Continental O-240]]. It is longer, heavier and has an increased span which increases the wing area by about 8%.  The fuselage is wider and the small dorsal fillet of the Kittiwake I gone; the rudder gained a trim tab and the elevators full width tabs.<ref name="JAWA70"/> It first flew on 19 March 1972.<ref name="Simpson"/>

==Operational history==
In addition to the prototype, two Kittiwake Is were built, one by [[Royal Navy]] apprentices in 1971 for glider towing.<ref name="Simpson"/>  For this role a larger diameter (6&nbsp;ft 4 in, 1.93 m) propeller is fitted, increasing the rate of climb by 24%.<ref name="JAWA70"/> A tow release hook is fitted under the tail.  Only one Kittiwake II, the prototype, was built.

==Survivors==
[[File:Mitchell-Proctor Kittiwake 1 AN0932285.jpg|thumb|Mitchell-Proctor Kittiwake 1]]

One Kittiwake was active until at least at 2005 and the other is still active. They remain on the UK Civil Register. These are the prototype, ''G-ATXN''<ref name="ATXN_Im"/><ref name="ATXN"/> and the ex-Naval ''G-BBRN''.<ref name="BBRN_Im"/><ref name="BBRN"/>  The latter is painted, as in its Naval days, as ''XW784''.  
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (Kittiwake I)==
{{Aircraft specs
|ref=Jane's all the World's Aircraft 1970<ref name="JAWA70"/>
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=
|length m=
|length ft=19
|length in=7
|length note=
|span m=
|span ft=24
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=7
|height in=6
|height note=
|wing area sqm=
|wing area sqft=105
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=910
|empty weight note=
|gross weight kg=
|gross weight lb=1250
|gross weight note= for aerobatic flight
|max takeoff weight kg=
|max takeoff weight lb=1350
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental O-200|Rolls Royce/Continental O-200-A]]
|eng1 type=4-cylinder horizontally opposed air cooled piston
|eng1 kw=<!-- prop engines -->
|eng1 hp=100<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=2<!-- propeller aircraft -->
|prop name=McCauley 69CM52
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=5<!-- propeller aircraft -->
|prop dia in=9<!-- propeller aircraft -->
|prop note=metal, fixed pitch; larger propeller fitted for glider towing.

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=131
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=122
|cruise speed kts=
|cruise speed note=at 75% power
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=540
|range nmi=
|range note=at 92 mph (148 km/h)
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=850
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=

|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles=

|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|other armament=

|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}
<!--==Notes==-->

==References==
{{Commons category|Mitchell Kittiwake}}
{{reflist|refs=
<ref name="JAWA66">{{cite book |title= Jane's All the World's Aircraft 1966-67|last= Taylor |first= John W R |coauthors= |edition=|page=165 |year=1966|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|ref=harv }}</ref>
<ref name="JAWA70">{{cite book |title= Jane's All the World's Aircraft 1970-71|last= Taylor |first= John W R |coauthors= |edition=|pages=223–4 |year=1970|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|ref=harv }}</ref>
<ref name="JAWA70P">{{cite book |title= Jane's All the World's Aircraft 1970-71|last= Taylor |first= John W R |coauthors= |edition=|page=228 |year=1970|publisher= Sampson Low, Marston & Co. Ltd|location= London|isbn=|ref=harv }}</ref>
<ref name="Simpson">{{cite book |title= Airlife's World Aircraft|last= Simpson |first= Rod |coauthors= |edition= |page=378|year=2001|publisher= Airlife Publishing Ltd|location= Shrewsbury|isbn=1-84037-115-3|ref=harv }}</ref>

<ref name="ATXN_Im">{{cite web |url=http://www.airliners.net/search/photo.search?regsearch=G-BBRN|title=Dated image of ''G-ATXN'' |author= |date= |work= |publisher= |accessdate=2010-08-21}}</ref>

<ref name="BBRN_Im">{{cite web |url=http://www.suffolkcoastalstrut.org.uk/boxted/2006/visitors/index.html|title=Dated image of ''G-BBRN'' |author= |date= |work= |publisher= |accessdate=2010-08-21}}</ref>

<ref name="ATXN">{{cite web |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=ATXN |title=CAA ''G-ATXN'' |author= |date= |work= |publisher= |accessdate=2010-08-21}}</ref>

<ref name="BBRN">{{cite web |url=http://www.caa.co.uk/application.aspx?catid=60&pagetype=65&appid=1&mode=detailnosummary&fullregmark=BBRN
 |title=CAA ''G-BBRN'' |author= |date= |work= |publisher= |accessdate=2010-08-21}}</ref>

}}
{{refbegin}}
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:British sport aircraft 1960–1969]]
[[Category:Glider tugs]]