{{Infobox military person
| name = Yevgeny Ivanovich Kabanov
| native_name = Евгений Иванович Кабанов
| image = Yevgeny Kabanov.jpg
| birth_date = 20 November 1918
| death_date = 19 June 1989
| birth_place = [[Begichevo]], [[Borisoglebsky Uyezd]], [[Tambov Governorate]], [[Russian Empire]]
| death_place = [[Moscow]]
| placeofburial = [[Mitinskoe Cemetery]]
| allegiance = {{flag|Soviet Union}}
| branch = [[Soviet Naval Aviation]]
| serviceyears = 1939–74
| rank = Major General
| unit = [[73rd Dive Bomber Aviation Regiment]]/[[12th Guards Dive Bomber Aviation Regiment]]
| battles = [[World War II]]
*[[Siege of Leningrad]]
*[[Baltic Offensive]]
*[[Battle of Koenigsberg]]
*[[East Pomeranian Offensive]]
| awards = {{Hero of the Soviet Union}}<br>
{{Order of Lenin}}<br>
[[Order of the Red Banner]] (5)<br>
[[Order of the Patriotic War]] 1st class<br>
[[Order of the Patriotic War]] 2nd class<br>
[[Order of the Red Star]]<br>
[[Honoured Military Navigator of the USSR]]
}}'''Yevgeny Ivanovich Kabanov''' (Russian: Евгений Иванович Кабанов; 20 November 1918 – 19 June 1989) was a [[Soviet Naval Aviation]] [[major general]] and [[Hero of the Soviet Union]]. During [[World War II]], Kabanov served as a bombardier, gunner and navigator in a dive bomber aviation regiment of the [[Baltic Fleet]]. He fought in the [[Siege of Leningrad]], the [[Baltic Offensive]], and the [[Battle of Königsberg]]. Kabanov was awarded the title for making 103 [[Petlyakov Pe-2]] sorties by June 1944. Postwar, Kabanov became chief navigator of the [[Black Sea Fleet]] Air Force, the [[Eighth Fleet|8th Fleet]] Air Force, and the [[9th Fighter Aviation Corps]]. Between 1956 and 1974, Kabanov was chief navigator of Soviet Naval Aviation. He retired in 1974 with the rank of Major general and worked at [[GosNIIAS]]. He died in 1989.<ref name=":0">{{Ruheroes|name=Yevgeny Kabanov|id=12590}}</ref>

== Early life ==
Kabanov was boron on 20 November 1918 in the village of [[Begichevo]] in [[Tambov Governorate]]. He graduated from seventh grade in 1936 and in 1939 the Voronezh Municipal Civil Engineering Technical School. Kabanov became a technical and foreman in the [[Ertilsky District]] Road Department. In December 1939, he was drafted into the [[Red Army]].<ref name=":0" />

== World War II ==
In June 1941, Kabanov graduated from the Naval Aviation School (VMAU) in [[Mykolaiv|Nikolayev]]. Between 1941 and 1942 he was a bombardier and gunner in the VMAU Training Regiment. He fought in combat from November 1942, serving with the [[73rd Dive Bomber Aviation Regiment]] of the Air Force of the Baltic Fleet as a bombardier and gunner with the rank of [[sergeant]]. Kabanov fought in the [[Siege of Leningrad]]. On 22 May 1943 he was awarded his first [[Order of the Red Banner]].<ref>Order No. 45 Baltic Fleet, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie50964144/ pamyat-naroda.ru]</ref> He was promoted to [[Junior Lieutenant|Junior lieutenant]] on 21 June.  Kabanov received a second Order of the Red Banner on 14 October.<ref>Order No. 99 Baltic Fleet, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie51145983/ pamyat-naroda.ru]</ref> On 3 November he was promoted to lieutenant. In 1944, Kabanov joined the [[Communist Party of the Soviet Union]].  In February 1944 the regiment became the [[12th Guards Dive Bomber Aviation Regiment]]. On 30 April Kabanov was awarded a third Order of the Red Banner.<ref>Order No. 32 Baltic Fleet, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie50971722/ pamyat-naroda.ru]</ref> On 17 May he received the rank of [[senior lieutenant]]. By June 1944 he had become a flight navigator. During the summer of 1944 he fought in the [[Baltic Offensive]]. By June 1944 Kabanov had made 103 Pe-2 dive bombing sorties. During these sorties he sank a transport and shared credit for two other sinkings, as well as damaging 3 transports and patrol ships. In aerial combat Kabanov shot down an enemy fighter. For his actions, Kabanov was awarded the title Hero of the Soviet Union and the [[Order of Lenin]].<ref name=":0" /><ref>Hero of the Soviet Union citation, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie50054957/ pamyat-naroda.ru]</ref><ref name=":1">{{Cite web|url=http://pobeda.poklonnayagora.ru/heroes/6428.htm|title=Кабанов Евгений Иванович|website=pobeda.poklonnayagora.ru|language=Russian|trans-title=Kabanov Yevgeny Ivanovich|access-date=2016-04-22}}</ref>

On 1 November 1944, Kabanov was promoted to [[Captain (armed forces)|captain]]. He became navigator for a squadron in the regiment. On 22 February 1945 he was awarded the [[Order of the Patriotic War]] 2nd class.  From late February he fought in the Battle of Königsberg. On 17 May he was awarded a fourth Order of the Red Banner.<ref name=":0" /><ref>Order No. 79 Baltic Fleet, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie51299715/ pamyat-naroda.ru]</ref>

== Postwar ==
From the end of the war to 1946 Kabanov was the regimental navigator. On 20 January 1949 he was promoted to [[major]]. He was awarded the [[Medal for Battle Merit]] on 15 November 1950.  In 1951 he graduated from the  [[Gagarin Air Force Academy|Air Force Academy]]. Between 1951 and 1954 Kabanov was navigator of the [[Black Sea Fleet]] Air Force [[2nd Guards Torpedo Aviation Division]]. He was promoted to [[Lieutenant colonel]] on 3 November 1952. In 1954–1955 he was chief naviagator of the Black Sea Fleet Air Force. Between November and December 1955 Kabanov was chief navigator of the 8th Fleet (the former Baltic Fleet). Between February and December 1956 he became chief navigator of the Baltic Fleet Air Force 9th Fighter Aviation Corps.  From December 1956, Kabanov was the chief navigator of Soviet Naval Aviation. He was awarded the [[Order of the Red Star]] on 30 December. He was promoted to [[colonel]] on 30 March 1957. On 22 February 1963 he was promoted to major general. Kabanov was awarded a fifth Order of the Red Banner on 22 February 1968. On 17 August 1971 he received the title [[Honoured Military Navigator of the USSR]]. He retired in April 1974.<ref name=":0" />

After his retirement, Kabanov lived in Moscow. Between 1976 and 1986 he worked as a specialist in aircraft information display systems at [[GosNIIAS]].<ref name=":1" /> On 6 April 1985 Kabanov was awarded the Order of the Patriotic War 1st class on the 40th anniversary of the end of World War II. <ref>TsAMO Anniversary card file, available online at [https://pamyat-naroda.ru/heroes/podvig-chelovek_yubileinaya_kartoteka1521435762/ pamyat-naroda.ru]</ref> He died on 19 June 1989 and was buried in the [[Mitinskoe Cemetery]].<ref name=":0" />

== Legacy ==
Streets in [[Ertil, Voronezh Oblast|Ertil]] and Begichevo are named for Kabanov. There is a memorial plaque to Kabanov at Ertil School No.1.<ref name=":0" /><ref name=":1" />

== References ==
<references />

{{DEFAULTSORT:Kabanov, Yevgeny}}
[[Category:1918 births]]
[[Category:1989 deaths]]
[[Category:People from Tambov Governorate]]
[[Category:Heroes of the Soviet Union]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Soviet major generals]]
[[Category:Recipients of the Order of the Red Banner]]
[[Category:Recipients of the Order of the Patriotic War, 1st class]]
[[Category:Recipients of the Order of the Patriotic War, 2nd class]]
[[Category:Recipients of the Order of the Red Star]]
[[Category:Soviet Navy personnel]]
[[Category:Communist Party of the Soviet Union members]]