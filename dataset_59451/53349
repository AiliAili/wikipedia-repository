The '''Alliance for Audited Media''' ('''AAM''') is a North American [[non-profit]] industry organization founded in 1914 by the [[Association of National Advertisers]] to help ensure media transparency and trust among advertisers and [[media companies]]. Originally known as the '''Audit Bureau of Circulations''' ('''ABC'''), today the AAM is a leading source of verified media information and technology platform certifications, providing standards, audit services and data for the advertising and publishing industries. It is one of more than three dozen such organizations operating worldwide, affiliated with the [[International Federation of Audit Bureaux of Circulations]] (IFABC).

AAM independently verifies print and digital circulation, mobile apps, website analytics, social media, technology platforms and audience information for [[newspaper]]s, [[magazine]]s and digital media companies in the U.S. and Canada. In the digital advertising space, AAM is regarded as one of the world’s most experienced providers of technology certification audits to industry standards established by the Interactive Advertising Bureau, Media Rating Council, Trustworthy Accountability Group and Mobile Marketing Association.

== History ==
At the turn of the 20th century, the [[Association of National Advertisers]] (ANA) observed a market need for verifiable, authenticated circulation figures from print publishers. As a result, in 1914, advertisers, ad agencies and publishers in the U.S. and Canada united to form the first Audit Bureau of Circulations to bring trust and accountability to the print media market.

Nearly a century later, the need for trust and accountability remains unchanged, although the format in which media is delivered has evolved significantly. With the expansion of digital media platforms, media buyers seek a solution to a chaotic marketplace and increasingly desire credible cross-channel metrics. On November 15, 2012, ABC in North America rebranded its organization as the Alliance for Audited Media to reflect the new media environment and its members' evolving business models.<ref>{{cite web|title=About Us|url=http://www.auditedmedia.com/about.aspx|publisher=Alliance for Audited Media|accessdate=20 January 2014}}</ref>

At about the same time, AAM joined forces with the Certified Audit of Circulations, a nonprofit newspaper auditor based in [[Wayne, New Jersey]].<ref>{{cite web|title=Audit Bureau of Circulations, Certified Audit of Circulations Agree to New Relationship |url=http://auditedmedia.com/news/news-releases/2012/abc,-cac-agree-to-new-relationship.aspx |publisher=Alliance for Audited Media |accessdate=20 January 2014 |date=5 September 2012 |deadurl=yes |archiveurl=https://web.archive.org/web/20140108062034/http://www.auditedmedia.com:80/news/news-releases/2012/abc,-cac-agree-to-new-relationship.aspx |archivedate=8 January 2014 |df= }}</ref> In 2014 AAM acquired a leading digital technology auditor, ImServices Group, to expand its services and customer base. AAM's digital clients include companies like Facebook, Yahoo, AOL and Alibaba.

== Governance ==
AAM is governed by a tripartite board of directors composed of leaders in publishing, marketing and advertising from the U.S. and Canada. Together with an extensive network of committees, the AAM board sets the standards by which print and digital media are measured and reported. AAM audited data is relied upon by marketers and ad agencies to plan for and buy media. This information is housed in an AAM database, disseminated via several complementary industry data providers (such as Gfk MRI and Kantar Media SRDS) and fed directly to proprietary databases of many large ad agencies and client-side marketers.

The organization is headquartered in [[Arlington Heights, Illinois]], with additional offices in [[New York City]] and [[Toronto]].

== Membership ==
Membership is open to all publishers, digital media companies, advertisers and advertising agencies. Additionally, any individual, firm or corporation that requires access to media data may apply for an associate membership. AAM serves as an industry forum, connecting advertisers, ad agencies and publishers to discuss issues and trends and set standards.

== See also ==
* [[Audit Bureau of Circulations (India)]]
* [[Audit Bureau of Circulations (UK)]]
* [[List of magazines by circulation]]
* [[Newspaper circulation]]
* [[OJD Morocco]]

== References ==
{{reflist}}

== External links ==
* [http://www.auditedmedia.com Alliance for Audited Media]
* [http://www.auditedmedia.ca Alliance for Audited Media (CA)]
* [https://web.archive.org/web/20101110002853/http://www.abc.org.uk/ Audit Bureau of Circulations (UK)]
* [http://www.ifabc.org/ International Federation of Audit Bureaux of Circulations]

[[Category:Arlington Heights, Illinois]]
[[Category:Non-profit organizations based in Illinois]]
[[Category:Publishing organizations]]
[[Category:Newspapers circulation audit]]