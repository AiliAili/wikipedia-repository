{{Infobox person
 | name = Sir Isaac Lowthian Bell, Bt
 | image = Isaac Lowthian Bell - britischer Industrieller.jpg
 | alt =
 | caption = Sir Isaac Lowthian Bell, by [[Frank Bramley]]. [[National Railway Museum]], York
 | birth_name =
 | birth_date = {{Birth date|1816|02|18|df=y}}
 | birth_place = [[Newcastle upon Tyne]]
 | death_date = {{Death date and age|1904|12|20|1816|02|18|df=y}}
 | death_place = London
 | other_names =
 | known_for = Ironmaking, [[metallurgy]], [[industrial chemistry]]<br>[[Member of Parliament]]
 | occupation = [[Ironmaster]], Company Director
 | awards = [[Albert Medal (Royal Society of Arts)|Albert Medal]] {{small|(1895)}}
}}

'''Sir Isaac Lowthian Bell, 1st Baronet''', [[Fellow of the Royal Society|FRS]] (18 February 1816 – 20 December 1904) was a [[Victorian era|Victorian]] [[ironmaster]] and [[Liberal Party (UK)|Liberal Party]] politician  from [[Washington, Tyne and Wear|Washington]], County Durham, in the [[north of England]]. He was described as being "as famous in his day as [[Isambard Kingdom Brunel]]".<ref name="Howell, 2008. p7">Howell, 2008. p7</ref>

Bell was an energetic and skilful entrepreneur as well as an innovative [[metallurgist]]. He was involved in multiple partnerships with his brothers to make iron and [[alkali]] chemicals, and with other pioneers including [[Robert Stirling Newall]] to make steel cables. He pioneered the large-scale manufacture of aluminium at his Washington works, conducting experiments in its production, and in the production of other chemicals such as the newly discovered element [[thallium]]. He was a director of major companies including the [[North Eastern Railway (UK)|North Eastern Railway]] and the [[Forth Bridge]] company, then the largest bridge project in the world.

He was a wealthy patron of the arts, commissioning the architect [[Philip Webb]], the designer [[William Morris]] and the painter [[Edward Burne-Jones]] on his Yorkshire mansions [[Rounton Grange]] and [[Mount Grace Priory]].

==Early life==
Bell was the son of Thomas Bell, one of the founders of the iron and alkali company [[Losh, Wilson and Bell]], and his wife Katherine Lowthian.<ref>Charles Mosley, editor, ''Burke's Peerage, Baronetage & Knightage, 107th edition'', 3 volumes (Wilmington, Delaware, U.S.A.: Burke's Peerage (Genealogical Books) Ltd, 2003), volume 1, page 331.</ref> He was born in [[Newcastle-upon-Tyne]] and educated at Dr Bruce's academy in Percy Street, Newcastle, followed by studying physical science at [[Edinburgh University]] and the [[Sorbonne University]], Paris.<ref name=Cooper/><ref name=NEIMME/> He gained experience in manufacturing alkalis at [[Marseilles]] before returning to Newcastle in 1836 to work in his father's [[Walker, Newcastle upon Tyne|Walker]] iron and chemical works.<ref name=Graces>[http://www.gracesguide.co.uk/Isaac_Lowthian_Bell Grace's Guides: Isaac Lowthian Bell]. Retrieved 28 November 2012.</ref><ref name=NorthYorks>[http://archives.northyorks.gov.uk/DServe/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqCmd=Show.tcl&dsqSearch=%28RefNo==%27ZFK%27%29 North Yorkshire County Council Archives: Bell of Rounton Grange Records]</ref>

==Industry==
{{further|Losh, Wilson and Bell}}
[[File:North Eastern Railway map circa 1900.JPG|thumb|Bell was a director of [[North Eastern Railway (UK)|North Eastern Railway]].<ref name=NEIMME/>]]
[[File:Blast furnaces Middlesbrough.jpg|thumb|Blast furnaces at [[Losh, Wilson and Bell|Bell Brothers]]' Port Clarence Ironworks, Middlesbrough, 1917]]

Bell's industrial life was complex: involvement in up to three different partnerships at the same time, created a conflict of loyalties causing allegiances to each to vary over the years.  Inspired to become an entrepreneur he took over the Walker ironworks on his father's death in 1845.  In 1850 at [[Washington, Tyne and Wear|Washington]], County Durham, Bell made a breakthrough discovering a new process for manufacturing [[lead oxychloride]].<ref>Howell (2008), p.420-1</ref>  His partners and co-founders of the Washington chemical company, named after his home, were his brother-in-law Robert Benson Bowman and his father-in-law [[Hugh Lee Pattinson]].  Pattinson was the inventor responsible for the process separating silver from lead that bears his name.<ref name=NEIMME/> Under an 1850 Indenture,<ref>Indenture 1759/13</ref> [[Charles Vane, 3rd Marquess of Londonderry]], Pattinson and Bell declared themselves "chemical manufacturers and co-partners in trade".<ref name=AEworks/> Bell decided the partnership might be incompatible with his continued involvement in the running of [[Losh, Wilson and Bell]].  In September 1849 he wrote to Losh and Wilson informing them that the partnership with Pattinson "would necessitate my relinquishing the office I at present hold in your firm".<ref>{{cite web | url=http://archives.northyorks.gov.uk/DServe/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqCmd=Show.tcl&dsqSearch=%28RefNo==%27ZFK%27%29 | title=Bell Of Rounton Grange Records | publisher=North Yorkshire County Council | year=2013 | accessdate=10 January 2013}}</ref>

Establishing a partnership with [[Robert Stirling Newall]] in 1850, Bell set up the first factory in the world with machines able to manufacture steel rope and submarine cable.<ref name=Howell421>Howell, 2008. p421.</ref> Two years later his brothers Thomas Bell and John Bell joined him to build a major iron works at [[Port Clarence]], [[Middlesbrough]] on the north bank of the [[River Tees]].<ref>Macfarlane, 1917. pp&nbsp;188–189.</ref> By 1853 there were three [[blast furnace]]s, each with a capacity of just over 6000 cubic feet, making them the largest in Britain at the time.<ref name=ICE/> The works produced iron for bridges and steel rails for railways across the [[British Empire]]<ref>Howell, 2008. pp&nbsp;4–5.</ref> including the [[North Eastern Railway (UK)|North Eastern Railway]] company, of which Bell was a [[Board of directors|director]] from 1864 onwards,<ref name=NEIMME/> and deputy chairman from 1895 until his death.<ref>Tomlinson, 1915. p771.</ref> The Bell brothers' company operated its own [[ironstone]] mines at [[Normanby, Redcar and Cleveland|Normanby]] and [[Cleveland, England|Cleveland]], and its own limestone quarries in [[Weardale]],<ref name=ICE/> employing about 6000 men in mining and manufacturing.<ref name=NEIMME/> By 1878 the firm was producing 200,000 tons of iron per annum.<ref name=Graces/> Bell was a professional [[metallurgist]] and [[industrial chemist]], constantly pioneering processes such as the recycling of heat from escaping flue gases, and trialling many process improvements.<ref name=NEIMME/>

1n 1859 Bell opened Britain's first factory able to manufacture [[aluminium]], a metal which had been as costly as gold because of the difficulty of [[reduction (chemistry)|chemically reducing]] the metal from an oxide. On the day it opened at Washington, Bell toured Newcastle in his carriage, saluting the crowds with an aluminium [[top hat]].<ref name=AEworks/><ref name=Howell4/> The plant used the new [[Deville sodium process]].<ref name=DMM>[http://www.dmm.org.uk/whoswho/b917.htm Durham Mining Museum: The Times Obituary: Sir Lowthian Bell]. 21 December 1904. Retrieved 28 November 2012.</ref> he described how critical it was to make aluminium pure in ''The Technologist'':<ref name=BellAluminium>{{cite journal | url=https://archive.org/stream/technologist418631864lond/technologist418631864lond_djvu.txt | title=On the Manufacture of Aluminium | author=Bell, Isaac Lowthian | journal=The Technologist |date=July 1864 | volume=4 | pages=166–168}}</ref>

{{quote|Now, it happens, that the presence of foreign matters, in a degree so small as almost to be infinitesimal, interferes so largely with the colour, as well as with the malleability of the aluminium, that the use of any substance containing them is of a fatal character. Nor is this all, for the nature of that compound which hitherto has constituted the most important application of this metal — aluminium-bronze — is so completely changed by using aluminium containing the impurities referred to [silica, iron, or phosphorus] that it ceases to possess any of those properties which render it valuable.|Isaac Lowthian Bell<ref name=BellAluminium/>}}

In 1863, Bell exhibited "several pounds" of the recently discovered element [[thallium]] when the [[British Association]] met in Newcastle that autumn. The metal was obtained from the flue deposits (mainly lead sulphate) from the manufacture of [[sulphuric acid]] from [[pyrites]], one of the products of the Washington works.  All the credit was given to researcher [[Henri Brivet]], who was "chief" of the laboratories at Washington, having experienced "languor and headache" then known to be caused by breathing thallium fumes.<ref>{{cite journal | url=http://en.wikisource.org/wiki/Page:Popular_Science_Monthly_Volume_1.djvu/493 | title=The Discovery of the Elements | author=Odling, William | journal=Popular Science Monthly |date=August 1872 | volume=1 | pages=474–481}}</ref><ref>{{cite book | title=Report of the Thirty-Third Meeting of the British Association for the Advancement of Science; Held at Newcastle-upon-Tyne in August and September 1863 | publisher=John Murray | author=Bell, Isaac Lowthian | work=On Thallium | year=1864 | location=London | pages=34–36}}</ref>

The [[Cleveland Ironstone Formation|Cleveland ironstone]] had been considered inferior for steelmaking, as it contained a relatively high percentage of [[phosphorus]] at 1.8 to 2.0%, weakening the resulting iron.  Bell directed large-scale experiments at a cost of up to £50,000,<ref>Equivalent to £3 million in 2004, adjusted for RPI.</ref> resulting in a [[base (chemistry)|basic]] steel process which produced steel rails containing no more than 0.07% phosphorus. His obituary in ''[[The Times]]'' of 1904 noted, as a sign of the progress that Bell himself had brought about, that Bell could recall "seeing wooden rails in use on the tramroads by which coal was brought down to the [[River Tees]]".<ref name=DMM/>

In 1867 and 1868, Bell gave papers on the state of the manufacture of iron and that of foreign competitors, in particular France, Belgium and Germany. Britain's great strength lay in its "incomparable fields of coal which constitute so important a feature in our mineral wealth". He stated that although the price and quality of coal were better in Britain, the cost of labour could be as much as 20–25% lower, particularly in Germany.<ref>Bell, Isaac Lowthian:''The present state of the manufacture of iron in Great Britain, and its position as compared with that of some other countries'' (from the report of the [[British Association for the Advancement of Science]] 1867), M & M.W. Lambert, Newcastle upon Tyne, 1867 (retrieved from Tracts vol.2 in the collection of [[North of England Institute of Mining and Mechanical Engineers]]) 5 March 2014</ref> In 1882, Bell successfully drilled for salt at Port Clarence, finding an exploitable salt bed at a depth of 1200 feet.<ref name=Cooper>Cooper, 1984.</ref> He used the salt for making [[sodium carbonate|soda]], but the borehole was sold to the Salt Union in 1888.<ref name=NEIMME/>

In 1875, Bell began to move out of working in industry by retiring from his partnership with [[Robert Benson Bowman]] and [[Robert Stirling Newall]], leaving the two of them to continue to run the chemical works until 1878, when Newall bought out Bowman.<ref name=AEworks>[http://www.aenvironment.co.uk/PDFs/Washington%20Chemical%20works/DTA%20Washington%20Chemical%20Works.pdf Archaeo-Environment: Washington Chemical Works]. Retrieved 28 November 2012.</ref> He continued to own shares in [[Losh, Wilson and Bell|Bell companies]] and the North Eastern Railway. In 1901, at age 85, after a long period of difficulty for heavy industry and with fears of worse to come as manufacturing grew in Germany, America and Japan, he made a decisive move to guard the family's wealth. He sold his railway interests to the North Eastern Railway company, and the sale of a majority holding in his manufacturing companies to rival [[Dorman Long]] was completed in 1903.  From the fortune thus released, he gave £5,000 to each of his many nephews, nieces and grandchildren.<ref>Howell, 2008, pp.61, 423. These gifts (£5,000 x 60 grandchildren = £300,000) would be equivalent to £18 million in 2004 terms, adjusted for RPI, using Howell's methodology, p.433</ref>

==Politics==
For 30 years from 1850, Bell was active on the town council of [[Newcastle upon Tyne]]. He became sheriff of the town in 1851, mayor in 1854 and alderman in 1859. Once again he was elected mayor for Newcastle in 1862.<ref name=NEIMME/><ref>[http://www.newcastle.gov.uk/sites/drupalncc.newcastle.gov.uk/files/wwwfileroot/your-council/lord_mayor/mayors_and_sheriffs_1800-1904.pdf Newcastle.gov: Mayors and Sheriffs 1800–1904]. Retrieved 28 November 2012.</ref>

Bell was elected as the [[Member of Parliament]] for [[North Durham (UK Parliament constituency)|North Durham]] from February to June 1874, and for [[The Hartlepools (UK Parliament constituency)|The Hartlepools]] from 1875 to 1880. He lost his seat in North Durham in 1874 on the grounds that his agents were guilty of intimidation.<ref name=NEIMME/>

==Honours, awards and achievements==

Bell founded the [[Iron and Steel Institute]] and was its president from 1873 to 1875. In 1874 he became the first recipient of [[Bessemer Gold Medal|the gold medal]] instituted by [[Sir Henry Bessemer]].<ref name=NEIMME/><ref>{{cite web | url=http://www.gracesguide.co.uk/images/e/ed/Er18740508.pdf | title=The Iron and Steel Institute | publisher=The Engineer | date=8 May 1874 | accessdate=30 November 2012 | pages=306}}</ref> Bell became a [[Fellow of the Royal Society]] in 1874<ref>{{cite web | url = http://www2.royalsociety.org/DServe/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqCmd=Show.tcl&dsqDb=Persons&dsqPos=10&dsqSearch=%28Surname%3D%27bell%27%29| title = Library and Archive Catalogue|publisher = Royal Society|accessdate = 15 March 2012}}</ref> as someone "''distinguished for his practical and scientific knowledge of Chemistry and Metallurgy''" (and author of) "''various reports and papers on chemical and metallurgical subjects, published in the transactions of various learned Societies''".<ref>{{cite web | url = http://www2.royalsociety.org/DServe/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqSearch=RefNo==%27EC%2F1874%2F02%27&dsqCmd=Show.tcl| title = Library and Archive Catalogue|publisher = Royal Society|accessdate = 15 March 2012}}</ref>

An international figure with a scientific profile, he was invited to serve as a juror at International Exhibitions in [[Philadelphia]] in 1876, and again in Paris in 1878.  He was accordingly made an honorary member of the  American Philosophical Institution and an officer of the [[Legion d'Honneur]].<ref name=Cooper/> In 1877 he founded the Institute of Chemistry of Great Britain (later the [[Royal Institute of Chemistry]]).<ref name=Howell422>Howell, 2008. p422.</ref> being nominated a fellow of the Chemical Society of London, and president of the [[Society of Chemical Industry]].<ref name=NEIMME>[http://www.mininginstitute.org.uk/aboutus/1886-1888%20Sir%20Isaac%20Lowthian%20Bell,%20Bart.html NEIMME: Sir Isaac Lowthian Bell, Bart]. Retrieved 28 November 2012.</ref>

During the 1881 [[Census]], taken on 3 April, Bell was recorded as a visitor at his daughter Maisie's house.  Her husband the Hon. Edward L. Stanley, M.P. in Harley Street, London was the Head of the Household, recording Bell's occupation as "Magistrate, Deputy Lieutenant, Iron Master".<ref>{{cite web | url=http://www.dmm.org.uk/whoswho/b917.htm | title=Isaac Lowthian Bell, Sir | publisher=Durham Mining Museum | work=Who's Who | date=1 November 2012 | accessdate=30 November 2012}}</ref>  In 1882 he became a director of the [[Forth Bridge]] company. At that time this was the world's largest bridge project.<ref name=Howell422>Howell, 2008. p422.</ref> Bell became president of the [[Institution of Mechanical Engineers]] in 1884 and was made a baronet in 1885.<ref name=IMechE/> With an inherited title he adopted the motto ''Perseverantia'' (perseverance) and the arms "Argent on a Fess between three Hawk's Lures Azure as many Hawk's Bells of the first".<ref>[http://www.cracroftspeerage.co.uk/online/content/BellB1885.htm Cracroft's Peerage: Bell of Rounton Grange, Co. York, and of Washington Hall, co. Durham]. Retrieved 28 November 2012.</ref>

In 1886 Bell became the 10th president of the [[North of England Institute of Mining and Mechanical Engineers]].<ref>{{cite web | url=https://www.mininginstitute.org.uk/about-us/history-of-the-institute/past-presidents-of-the-institute | title=Past Presidents of the Institute | publisher=North of England Institute of Mining and Mechanical Engineers | accessdate=4 October 2015}}</ref> He was awarded the [[George Stephenson Medal]] from the [[Institution of Civil Engineers]] in 1890,<ref name=NEIMME/> and the [[Telford Medal]] (then called the Telford Premium) from the same institution, both for papers that he presented there.<ref name=ICE>Institution of Civil Engineers Obituary, 1905.</ref> In 1895 Bell was awarded the [[Albert Medal (RSA)|Albert Medal]] of the [[Royal Society of Arts]], ''in recognition of the services he has rendered to Arts, Manufactures and Commerce, by his [[metallurgy|metallurgical]] researches and the resulting development of the iron and steel industries''.<ref name=IMechE>[http://heritage.imeche.org/Biographies/LowthianBell Institution of Mechanical Engineers: President, Sir Lowthian Bell]</ref>

==Works==
Bell wrote many papers on chemistry and metallurgy. In his ''Iron Trade'', he correctly predicted the outstripping of Britain by Germany in industrial production, unsuccessfully urging government action to avoid this.<ref name=Howell4/> His major works include:<ref name=DMM/>
* ''The Manufacture of Iron in connection with the Northumberland and Durham Coalfields'', The Industrial resources of the District the three northern rivers, the Tyne, Wear and Tees: including the reports on the local manufactures, read before the British Association. ,<ref>Baron William George Armstrong, (1863), pp. 73–119</ref>
* ''On the Manufacture of Aluminium'', The Industrial resources of the District the three northern rivers, the Tyne, Wear and Tees: including the reports on the local manufactures, read before the British Association. (pp 73–119) Baron William George Armstrong, 1863.
* ''The Manufacture of Aluminium'', The Technologist, July 1864.
* ''The Manufacture of Thallium'', British Association, 1864.
* ''The present state of the manufacture of iron in Great Britain, and its position as compared with that of some other countries'' (from the report of the [[British Association for the Advancement of Science]] 1867), M & M.W. Lambert, Newcastle upon Tyne, 1867.
* ''The Chemical Phenomena of Iron Smelting: An Experimental and Practical Examination of the Circumstances Which Determine the Capacity of the Blast Furnace, the Temperature of the Air, and the Proper Condition of the Materials to Be Operated Upon'' (collection of papers published as a book, 435pp), Routledge, London, 1872.
* ''The Iron Trade of the United Kingdom Compared with that of the Other Chief Ironmaking Nations'', Literary and Philosophical Society, Newcastle-upon-Tyne, 1875.
* ''Mr I Lowthian Bell and the Blair Direct Process''. James M'Millin, 1875.
* ''Sir Lowthian Bell and his presidential address''. [[North of England Institute of Mining and Mechanical Engineers]] (vol. 36) 1875.
* ''On the Hot Blast, with an explanation of its Mode of Action in Iron Furnaces of Different Capacities''. Transactions of the American Institute of Mining Engineers (vol. V pp 56–81) May 1876 to February 1877.
* ''The Principles of the Manufacture of Iron and Steel with some notes on the economic conditions of their production '', George Routledge & Sons, London, 1884.
* ''On the manufacture of salt near Middlesbrough'' (with James Forrest). [[Institution of Civil Engineers]], London, 1887.
* ''On the Probable Future of the Manufacture of Iron''.<ref>Transactions of the American Institute of Mining Engineers (vol. XIX pp 834–855) May 1890 to February 1891</ref>
* ''Memorandum as to the wear of rails'', Ben Johnson, 1896.
* ''Memorandum(No.2) as to the wear of rails & broken rails'', Leeds Chorley & Pickersgill 1900.

==Family life==
On 20 July 1842 Bell, known as Lowthian, married Margaret Pattinson, daughter of his business partner [[Hugh Lee Pattinson]]<!--given the distance, overlink seems fine here--> and Phebe Walton. Margaret's younger sisters married Bell's business partners Robert Benson Bowman and [[Robert Stirling Newall]]: all three brothers-in-law were members of Tyneside Naturalists' Field Club and the Natural History Society of Northumberland.<ref name=NatHistSocNorth>{{cite journal | url=http://www.nhsn.ncl.ac.uk/news/wp-content/uploads/Trans64p3-161-168-Bowman.pdf | title=Robert Benson Bowman – an early Newcastle botanist | publisher=Natural History Society of Northumberland | year=2005| accessdate=28 November 2012 | last=Hendra | first=Leslie Anne | volume=64 | pages=161–168}}</ref>

Their children were  Sir [[Thomas Hugh Bell]], 2nd Baronet, known as Hugh, who fathered the explorer and diplomat [[Gertrude Bell]], Florence, Mary Katherine, known as Maisie, who in 1873 married [[Edward Stanley, 4th Baron Stanley of Alderley]],<ref>{{cite journal|title='''STANLEY of Alderley''', 4th Baron|journal=Who's Who|year=1907|volume= 59|pages=1662|url=https://books.google.com/books?id=yEcuAAAAYAAJ&pg=PA1662}}</ref> Ada, Charles, and Ellen (who died in infancy). He had about 60 grandchildren.<ref name=Howell4/>

In 1854, he built Washington New Hall, a few miles south of [[Newcastle-upon-Tyne]]. In 1872 an illegally young [[chimney sweep]], aged 7, died in a chimney in the Hall. Bell promptly moved into the newly built Rounton Grange near [[Northallerton]], leaving Washington Hall empty for nineteen years until he donated it as a home for poor children, named at his request "Dame Margaret's Hall".<ref name=Howell4/> According to his granddaughter's biographer Georgina Howell, he was "a formidable giant of a man" and somewhat abrasive.<ref name=Howell5>Howell, 2008. pp&nbsp;5–7.</ref> The family prepared a Christmas alphabet in 1877 at Rounton Grange, including "C is the Crushing Contemptuous Pater", which Bell's daughter Elsa later annotated "Sir Isaac Lowthian Bell".<ref name=Howell5/> In Howell's view, another event also hinted at Bell's character: one cold winter's night, his coachman was found, according to some papers found in Mount Grace Priory, "frozen stiff on the box-seat of his carriage". Howell notes that the coachman could simply have had a heart attack, and not have died of exposure at all, but still, in her view "consideration for others was not, perhaps, Lowthian's principal quality".<ref name=Howell5/>

[[File:Guest House, Mount Grace Priory - geograph.org.uk - 1544876.jpg|thumb|The converted manor house at [[Mount Grace Priory]], in 2008]]

Rounton Grange was finished in 1876, under the architect [[Philip Webb]]; it was at that time his largest project. The house was five storeys high, of yellow brick with a pantiled roof, enormous mock mediaeval chimneys and "gothic" features. It was set in an estate of 3000 acres with lawns, a wood full of daffodils, a rose garden, and two lakes. Inside was an immense arched gallery that stretched the full width of the house, a wide curved staircase and baronial fireplace. In the main drawing room there was an [[Adamesque]] fireplace, and two grand pianos on a vast carpet. There was a large tapestry frieze of [[Chaucer]]'s  ''Romaunt of the Rose'' designed by [[William Morris]] and [[Edward Burne-Jones]], and made by Lady Bell and her daughters over several years.<ref>Howell, 2008. pp&nbsp;64–66.</ref>

Given his huge wealth, Margaret and he lived relatively simply,<ref name=NEIMME/><ref name=Howell5/> preferring the relatively humble<ref name=Howell4/> [[Arts and Crafts movement|Arts and Crafts]] style for his three-year refurbishment of the mediaeval [[Mount Grace Priory]] near [[Osmotherley]], which was in serious disrepair when he purchased it in 1898<ref>Howell, 2008. p423.</ref> as a weekend retreat.<ref>{{cite web | url=http://www.english-heritage.org.uk/about/news/arts-and-crafts-mount-grace/ | title=Arts and crafts revival planned at Mount Grace | publisher=English Heritage | date=14 January 2010 | accessdate=28 November 2012}}</ref> The house was decorated by the best designers of the day in the "Aesthetic" style, with [[William Morris]]'s "Double Bough" wallpaper hand-printed using 22 apple wood printing blocks. For the restoration of 2010 the original blocks were used to produce a close replica of the original wallpaper, each roll taking a week to print by hand.<ref>{{cite web | url=http://news.bbc.co.uk/local/york/hi/people_and_places/history/newsid_9271000/9271060.stm | title=Arts and Crafts vision restored at Gertrude Bell's family home | publisher=BBC | date=9 December 2010 | accessdate=28 November 2012}}</ref> While decorating the great house, Morris spoke of "ministering to the swinish luxury of the rich".<ref>{{cite news | url=https://www.theguardian.com/artanddesign/2011/mar/26/aestheticism-exhibition-victoria-albert-museum | title=The Guardian | work=The Aesthetic Movement | date=26 March 2011 | accessdate=28 November 2012 | author=MacCarthy, Fiona}}</ref>

Bell died on 20 December 1904 at his house in London, 10 Belgrave Terrace.<ref>Howell, 2008. pp54, 424.</ref> He left £750,000 to his son Hugh Bell on his death in 1904.<ref>Howell, 2008. p433, which states that this would be worth £45 million in 2004 terms, adjusted for the [[retail price index]] of inflation (RPI).</ref>

==Legacy==
After his death, the Institution of Mining Engineers resolved that<ref name=NEIMME/>

{{quote|It is impossible to estimate the value of the services that Sir Lowthian Bell rendered to the Institution of Mining Engineers in promoting its objects, and in devoting his time and energies to the advancement of the Institution.|Council of the Institution of Mining Engineers<ref name=NEIMME/>}}

Gertrude Bell's biographer, Georgina Howell, writes of Lowthian Bell that through his writings such as ''The Chemical Phenomena of Iron Smelting'' he was seen as the "high priest of British Metallurgy". She observes that he was noted also for his wealth and for the innovations he made, such as the use of steel-making slag as phosphate fertilizer,{{efn|Cleveland Ironstone was high in Phosphorus.}} and that he was arguably Britain's "foremost industrialist". Among his friends were famous [[Victorian era|Victorians]] such as [[Charles Darwin]], [[Thomas Huxley]], [[William Morris]] and [[John Ruskin]].<ref name=Howell4>Howell, 2008. p4</ref> However, Howell writes, "Lowthian was admired rather than loved, and appears to have been dictatorial and harsh towards his family."<ref>Howell, 2008. p5</ref> She observes that "There is to this day no biography of the man who was as famous in his day as [[Isambard Kingdom Brunel]]."<ref name="Howell, 2008. p7"/>

==Bibliography==
* {{cite journal | author=Anon |year=1907 |title=Obituary Notices of Fellows Deceased |journal=Proceedings of the Royal Society of London |volume=LXXVIII |issue= |pages=xiv |url=https://books.google.com/books?id=KaUOAAAAIAAJ&pg=PR14 |doi= }}
* {{cite journal |author=Anon |year=1904–1907 |title=Memoir of Sir Lowthian Bell, Bart. |journal=Transactions of the Institution of Mining Engineers |volume=XXXIII |issue= |pages=665–672 |url=https://books.google.com/books?id=880RAAAAIAAJ&pg=RA2-PA665 |doi= }}
* <!--Cooper, Thompson (1884)-->{{cite Men of the Time|name=Bell, Isaac Lowthian|page=97}}
* {{cite book | last=Howell | first=Georgina | title=Gertrude Bell: Queen of the Desert, Shaper of Nations | publisher=Farrar, Straus and Giroux | year=2008|edition=paperback}}
* <!--Macfarlane, Walter (1917)-->{{cite book | url=http://openlibrary.org/books/OL7131814M/The_principles_and_practice_of_iron_and_steel_manufacture | title=The Principles and Practice of Iron and Steel Manufacture | publisher=Longmans, Green | author=Macfarlane, Walter | year=1917 | location=London}}
* {{cite book |last=Tomlinson |first=W.W. |authorlink=William Weaver Tomlinson |title=The North Eastern Railway: its Rise and Development |url=https://archive.org/details/northeasternrail00tomlrich |publisher=Andrew Reid and Company |location=Newcastle-upon-Tyne |year=1915 |ref=harv }}
* <!--Tweedale, Geoffrey (2011)-->{{cite book | url=http://www.oxforddnb.com/index/30/101030690 | title=Oxford Dictionary of National Biography | publisher=Oxford University Press | work=Lowthian Bell |last=Tweedale |first=Geoffrey | year=2011 | accessdate=28 November 2012}}
* {{cite book | url=http://runeberg.org/nfbb/0676.html | title=Nordisk familjebok 2. Armatoler – Bergsund | publisher=Uggleupplagan | work=Bell, Sir Isaac Lowthian | year=1904 | pages=1267–1268}}
* {{cite journal | url=http://www.icevirtuallibrary.com/content/article/10.1680/imotp.1905.16903 | title=Obituary, Sir Isaac Lowthian Bell, Bart, LLD, 1816–1904 | journal=Minutes of the Proceedings of the Institution of Civil Engineers | date=1 January 1905 | volume=160 | issue=1905 | pages=386–388|issn=1753-7843 | doi=10.1680/imotp.1905.16903}}
* [http://archives.northyorks.gov.uk/DServe/dserve.exe?dsqIni=Dserve.ini&dsqApp=Archive&dsqDb=Catalog&dsqCmd=Show.tcl&dsqSearch=%28RefNo==%27ZFK%27%29 North Yorkshire County Council Archives: Bell of Rounton Grange Records]
* [http://www.aenvironment.co.uk/PDFs/Washington%20Chemical%20works/DTA%20Washington%20Chemical%20Works.pdf Archaeo-Environment: Washington Chemical Works]. March 2004. Report 29/1-03 for Sunderland City Council.

== Notes ==
{{notelist}}

== References ==
{{reflist|33em}}

== External links ==
* {{Hansard-contribs | mr-isaac-bell | Sir Lowthian Bell }}
* {{Rayment-hc|date=March 2012|external links=1}}
* {{Rayment-bt|date=March 2012|external links=1}}
* [https://www.mininginstitute.org.uk/113-sir-isaac-lowthian-bell NEIMME: Sir Isaac Lowthian Bell, Bart]
* [http://www.dmm.org.uk/whoswho/b917.htm Durham Mining Museum: Isaac Lowthian Bell, Sir]
* [http://www.gracesguide.co.uk/Isaac_Lowthian_Bell Grace's Guides: Isaac Lowthian Bell]
* [http://www.english-heritage.org.uk/about/news/arts-and-crafts-mount-grace English Heritage: Mount Grace Priory ([[Arts and Crafts movement|Arts and Crafts] under Sir Isaac Lowthian Bell)]
* [http://heritage.imeche.org/Biographies/LowthianBell IMechE: Lowthian Bell] (with portrait photo)
* [http://www.therountons.com/festival/gallery/bell/bell0.htm The Rountons] (portrait painting by unknown artist)

{{S-start}}
{{s-par|uk}}
{{succession box
  | title  = [[Member of Parliament]] for [[North Durham (UK Parliament constituency)|North Durham]]
  | with   = [[Sir Charles Palmer, 1st Baronet|Charles Palmer]]
  | years  = [[United Kingdom general election, 1874|Feb. 1874]]–[[North Durham by-election, 1874|Jun. 1874]]
  | before = [[Sir Charles Palmer, 1st Baronet|Charles Palmer]] and<br />Sir [[Sir George Elliot, 1st Baronet|George Elliot]]
  | after  = [[Sir Charles Palmer, 1st Baronet|Charles Palmer]] and<br />Sir [[Sir George Elliot, 1st Baronet|George Elliot]]
}}
{{succession box
  | title  = [[Member of Parliament]] for [[The Hartlepools]]
  | years  = 1875–[[United Kingdom general election, 1880|1880]]
  | before = [[Thomas Richardson (Hartlepool MP died 1890)|Thomas Richardson]]
  | after  = [[Thomas Richardson (Hartlepool MP died 1890)|Thomas Richardson]]
}}
{{s-reg|uk-bt}}
{{s-new | creation }}
{{s-ttl
  | title  = [[Bell baronets|Baronet]]<br />'''(of Rounton Range and Washington Hall)'''
  | years  = 1885–1904
}}
{{s-aft | after  = [[Sir Thomas Bell, 2nd Baronet|(Thomas) Hugh Bell]] }}
{{s-npo|pro}}
{{s-bef| before=[[Percy G. B. Westmacott]] }}
{{s-ttl |title=[[President]] of the [[Institution of Mechanical Engineers]] |years=1884 }}
{{s-aft|after=[[Jeremiah Head]] }}
{{s-bef| before=[[John Daglish]] }}
{{s-ttl |title=[[President]] of the [[North of England Institute of Mining and Mechanical Engineers]] |years=1886–1888 }}
{{s-aft|after=[[John Marley]] }}
{{S-end}}

{{Use dmy dates|date=March 2016}}

{{Good article}}

{{Authority control}}
{{DEFAULTSORT:Bell, Isaac Lowthian}}
[[Category:1816 births]]
[[Category:1904 deaths]]
[[Category:People from Washington, Tyne and Wear]]
[[Category:English engineers]]
[[Category:English businesspeople]]
[[Category:Liberal Party (UK) MPs]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:UK MPs 1874–80]]
[[Category:Ironmasters]]
[[Category:Baronets in the Baronetage of the United Kingdom|Bell, Sir Lowthian, 1st Baronet]]
[[Category:Fellows of the Royal Society]]
[[Category:North Eastern Railway (UK) people]]
[[Category:English metallurgists]]
[[Category:Mayors of Newcastle upon Tyne]]
[[Category:Bessemer Gold Medal]]