{{good article}}
{{Infobox monarch | name =Michael III Shishman Asen<br>Михаил ІІІ Шишман Асен
| succession     =[[Tsar of Bulgaria]]
| image          =Michael 3 john harris valda.jpg
|image_size      = 200px
| caption        =Tsar Michael Shishman
| reign          =1323–1330
| coronation     =
| predecessor    =[[George II of Bulgaria|George II]]
| successor      =[[Ivan Stephen of Bulgaria|Ivan Stephan]]
| spouse         =[[Anna Neda of Serbia|Anna Neda]]<br />[[Theodora Palaiologina, Empress of Bulgaria|Theodora Palaiologina]]
| issue          =[[Ivan Stephen of Bulgaria|Ivan Stephan]] <br /> Michael <br /> Shishman
| father         =[[Shishman of Vidin]]
| mother         =
| house          = [[Shishman dynasty]]
| birth_date     =after 1280
| birth_place    =
| death_date     =31 July 1330
| death_place    =[[Kyustendil|Velbazhd]]
| buried         =[[Church of St. George, Staro Nagoričane]]
|}}
'''Michael Asen III''' ({{lang-bg|Михаил Асен III}}, ''Mihail Asen III'', commonly called '''Michael Shishman''' (Михаил Шишман, ''Mihail Šišman'')),{{cref|a}} ruled as emperor ([[tsar]]) of [[Bulgaria]] from 1323 to 1330. The exact year of his birth is unknown but it was between 1280 and 1292. He was the founder of the last ruling dynasty of the [[Second Bulgarian Empire]], the [[Shishman dynasty]]. After he was crowned, however, Michael used the name Asen to emphasize his connection with the [[Asen dynasty]], the first one to rule over the Second Empire.

An energetic and ambitious ruler, Michael Shishman led an aggressive but opportunistic and inconsistent foreign policy against the [[Byzantine Empire]] and the [[Serbia in the Middle Ages|Kingdom of Serbia]], which ended in the disastrous [[battle of Velbazhd]] which claimed his own life. He was the last medieval Bulgarian ruler who aimed at military and political hegemony of the [[Second Bulgarian Empire|Bulgarian Empire]] over the [[Balkans]] and the last one who attempted to seize [[Constantinople]]. He was succeeded by his son [[Ivan Stephen of Bulgaria|Ivan Stephen]] and later by his nephew [[Ivan Alexander of Bulgaria|Ivan Alexander]], who reversed his policy by forming an alliance with Serbia.<ref name="kazhdan">Kazhdan, "Michael III Šišman", p. 1365</ref>

==Rise to the throne==
Born between 1280<ref>Андреев, p. 255</ref> and 1292<ref name="kazhdan"/> Michael Shishman was the son of the [[Despot (court title)|despot]] [[Shishman of Vidin]] by an unnamed daughter of the ''[[sebastokrator]]'' Peter and [[Anna-Theodora Asenina of Bulgaria|Anna (Theodora)]], herself daughter of [[Ivan Asen II of Bulgaria|Ivan Asen II]] (r. 1218-1241) and [[Irene Komnene of Epirus]]. He was also a distant cousin of his predecessors on the Bulgarian throne, [[Theodore Svetoslav of Bulgaria|Theodore Svetoslav]] (r. 1300-1321) and [[George II of Bulgaria|George Terter II]] (r. 1321-1322). After the peace between his father and [[Stefan Uroš II Milutin of Serbia|Stefan Milutin]] in 1292, Michael Shishman was engaged to Milutin's daughter [[Anna Neda of Serbia|Anna Neda]] and they married in 1298 or 1299.<ref name="fine268">Fine, p. 268</ref>

Since the middle of the 13th century, the area of [[Vidin]] had been autonomous under ineffective Bulgarian overlordship, and was ruled successively by [[Yakov Svetoslav]] (died 1276), Shishman (died between 1308 and 1313), and then Michael Shishman. Shishman and his son received the high courtly title of ''despotēs'' from their cousin Theodore Svetoslav and the latter was referred to in a contemporary Venetian source as a ''Despot of Bulgaria and Lord of Vidin''.<ref name="fine268"/> With the death of the Serbian king Stefan Milutin, Michael Shishman was able to follow a more active policy in the Bulgarian capital [[Veliko Tarnovo|Tarnovo]]. He soon became a leading noble in the internal affairs of the country and, on the childless death of young George Terter II in 1323, Michael Shishman was elected emperor of Bulgaria by the nobility.<ref>Fine, pp. 268–269</ref> According to some historians he was chosen because he was a descendant of the Asen dynasty and interpret his ascenсion to the throne not as the beginning of a new dynasty but rather as a continuation of the House of Asen.<ref>Божилов, Гюзелев, p. 562</ref> His half-brother, [[Belaur]], succeeded him as despot of Vidin.<ref name="fine269">Fine, p. 269</ref>

==Relations with the Byzantine Empire==

===War against Byzantium===
[[File:Battle of Velbazhd.png|thumb|right|300px|Bulgaria during the rule of Michael Shishman.]]
The sudden death of George Terter II had been followed by a brief period of confusion and uncertainty, which was exploited by the Byzantine emperor [[Andronicus III Palaeologus|Andronikos III Palaiologos]]. The Byzantines overran northeastern [[Thrace]] and captured a number of important cities including [[Yambol]], [[Lardea]], [[Ktenia (fortress)|Ktenia]], [[Rusokastro]], [[Pomorie|Anchialus]], [[Sozopol]] and [[Tsarevo|Agatopol]]. At the same time, a Byzantine-sponsored pretender, [[Voysil (despot)|Voysil]], brother of the former Bulgarian emperor [[Smilets of Bulgaria|Smilets]] (r. 1292-1298), ensconced himself in [[Kran, Stara Zagora Province|Krăn]], controlling the valleys between the [[Balkan mountains]] and [[Sredna Gora]] from [[Sliven]] to [[Anevo Fortress|Kopsis]].<ref name="a256">Андреев, p. 256</ref> At this point the newly elected Michael Shishman marched south against Andronikos III, while another Byzantine army led by Andronikos III himself was besieging Philippopolis ([[Plovdiv]]). Defended by a Bulgarian garrison led by [[Ivan the Russian]], the siege was a failure despite the Byzantines use of a 100-soldier, five-story siege tower.<ref name="a256"/><ref>"Historia by John Kantakouzenos" in GIBI, vol. X, Bulgarian Academy of Sciences, Sofia, p. 224</ref> While the Byzantine army was engaged at Philipopolis, Michael Shishman led his troops to north-eastern Thrace and quickly retook the lost cities thus forcing the Byzantines to pull back.<ref name="a256"/>

Although Michael Shishman forced Andronikos III to retreat, the Byzantines managed to take the Philippopolis while the Bulgarians were changing garrisons.<ref>"Historia by John Kantakouzenos" in GIBI, vol. X, Bulgarian Academy of Sciences, Sofia, p. 227</ref> Despite the loss, Michael Shishman was able to expel Voysil and fully recover Bulgarian control over northern and northeastern Thrace in 1324 which had been taken by the Byzantines in the previous year during the interregnum.<ref>Божилов, Гюзелев, p. 563</ref> Again in 1324, the Bulgarian emperor invaded Byzantium advancing as far as Trajanopolis{{cref|b}} and Vira in the lower course of the [[Maritsa]] river.<ref>Андреев, pp. 256–257</ref> Andronikos III was unable to engage the Bulgarian army because his troops were outnumbered. He offered Michael Shishman a [[duel]] to solve the conflict. The Bulgarian emperor answered with the words cited by [[John VI Kantakouzenos|John Kantakouzenos]]:<ref name="a257">Андреев, p. 257</ref><ref>"Historia by John Kantakouzenos" in GIBI, vol. X, Bulgarian Academy of Sciences, Sofia, p. 228</ref> 
{{cquote|Stupid would be the blacksmith who instead of taking the hot iron with pincers takes it with his hands. He himself would be ridiculed by the Bulgarians if he risks not his large and strong army but his own body.}}
The Byzantine emperor was said to have been infuriated with the answer and the fact that he was outsmarted. However, Michael III who was informed of the conflict between Andronikos III and Andronikos II hinted him that he could help Andronikos III against his grandfather in case of war and returned to Bulgaria promising that soon he would begin negotiations.<ref name="a257"/>

===Peace agreement and involvement in Byzantine civil war===
On a council held in Constantinople on the relations with Bulgaria it was decided that the two countries should begin negotiations despite the calls for punishing the Bulgarians for the invasion. Michael Shishman divorced his wife Anna Neda and married [[Theodora Palaiologina, Empress of Bulgaria|Theodora Palaiologina]], the 35-year-old widow of emperor [[Theodore Svetoslav of Bulgaria|Theodore Svetoslav]].<ref name="f270">Fine, p. 270</ref> The exact reasons for that act are unclear. Many historians suggest that the deterioration of the Bulgarian-Serbian relations was rooted in the Serbian penetration in [[Macedonia (region)|Macedonia]].<ref name="a258">Андреев, p. 258</ref><ref name="bg566">Божилов, Гюзелев, p. 566</ref> The marriage cemented the peace treaty with the Byzantine Empire but the need for an ally against the Serbs made Michael Shishman prone to make concessions. It was decided that the border should follow the Philippopolis-[[Ormenio|Chernomen]]-Sozopol line.<ref name="a258"/> The agreement was finally signed in the autumn of 1324 and Michael Shishman spent the next several years at peace with his neighbors.<ref name="a258"/><ref>Божилов, Гюзелев, p. 564</ref>

In 1327 Michael Shishman became involved in the renewed civil war in the [[Byzantine Empire]], taking the side of his brother-in-law Andronikos III, while his grandfather and rival [[Andronicus II Palaeologus|Andronikos II]] obtained the support of the Serbian king. Andronikos III and Michael Shishman [[Treaty of Chernomen|met at Chernomen]] (according to [[Nicephorus Gregoras]] at [[Didymoteicho|Dimotika]])<ref name="bg565">Божилов, Гюзелев, p. 565</ref> and concluded an aggressive alliance against Serbia. The Byzantine emperor promised to Bulgaria territory with several towns and large amount of money if he would become a sole emperor.<ref name="a259">Андреев, p. 259</ref> Based on that alliance, Andronikos III gained control of Macedonia but his success made Michael Shishman, who aimed at a prolonged conflict within the Byzantine Empire, enter into negotiations with Andronikos II, offering military support in exchange for money and the cession of some border lands.<ref>Андреев, pp. 259–260</ref> The Bulgarian ruler sent a detachment of 3,000 cavalry, commanded by [[Ivan the Russian]], from Yambol to guard the Imperial Palace in Constantinople and Andronikos II but his intentions were to capture the old emperor and the city.<ref>Jireček, p. 419</ref><ref>Павлов</ref> Forewarned by his grandson, Andronikos II prudently kept the Bulgarians away from the capital and his person. When Michael Shishman understood that his plans were revealed he sent Ivan a letter to retreat with a singe feather which meant that the orders had to be promptly executed.<ref name="a260">Андреев, p. 260</ref>

Following the victory of Andronikos III over his grandfather, Michael Shishman attempted to gain some lands by force. He invaded Thrace in June 1328 and pillaged the vicinities of [[Vize|Viza]] but retreated before the advance of Andronikos III.<ref name="a261">Андреев, p. 261</ref> Another showdown in front of [[Adrianople]] 60 days later ended without battle and with the renewal of the peace treaty in October 1328, after which Michael Shishman returned to his country, but not before securing a large payoff.<ref>Fine, p. 271</ref> In return, the Bulgarians gave back the fortress of [[Matochina|Bukelon]] which they had taken during the initial stages of the campaign.<ref>Божилов, Гюзелев, pp. 567–568</ref> In the beginning of the next year the Bulgarian emperor requested a personal meeting with his Byzantine counterpart to negotiate a definitive treaty and joint military operations against the growing power of Serbia.<ref name="kazhdan"/> In the locality known as ''Krimni'' between Sozopol and Anchialus the two signed "lasting peace and eternal alliance".<ref name="a261"/>

==Relations with Serbia==
{{See also|Battle of Velbazhd}}
[[File:Bitkakodvelbuzda1330.jpg|thumb|300px|right|The [[battle of Velbazhd]].]]
The divorce with Anna Neda in 1324 worsened the relations between Bulgaria and Serbia which had been cordial since the beginning of the 14th century.<ref>Андреев, p. 250</ref> Anna Neda had to leave the capital [[Veliko Tarnovo|Tarnovo]] with her sons and sought refuge from her brother [[Stephen Uroš III Dečanski of Serbia|Stephen Dečanski]], the king of Serbia.<ref name="a258"/> Dečanski, who was engaged in war against his cousin [[Stephen Vladislav II of Syrmia|Stephen Vladislav II]], was in no position to oppose Michael Shishman.<ref name="f270"/> The Bulgarian emperor even acknowledged his rival as King of Serbia but his help to Vladislav was insufficient. In the spring of 1324 Dečanski sent the future Serbian archbishop [[Saint Danilo II|Danilo II]] to negotiate with the Bulgarian emperor in Tarnovo but his mission was inconclusive.<ref name="bg565"/> The two countries were again on the opposite sides in the Byzantine civil war when the Bulgarians allied with Andronikos III while the Serbs supported his grandfather.<ref name="f270"/><ref>Андреев, pp. 258–259</ref><ref>Gregory, p. 305</ref>

After the agreement with Andronikos III in 1329, Michael Shishman started preparations to attack while the Serbs were pillaging the areas around [[Ohrid]]. According to the Serbian chroniclers, he arrogantly demanded the submission of the Serbian king and threatened to "set up his throne in the middle of the Serbian land".<ref name="a262">Андреев, p. 262</ref> In 1330, expecting to join the army of Andronikos III advancing from the south, Michael Shishman marched on Serbia with a large force of 15,000 troops, including reinforcements from his vassals and allies from [[Wallachia]] and [[Moldavia]].<ref name="a262"/> At first he headed to Vidin, where historians believe he wanted to join forces with the soldiers of his brother Belaur, and then marched to the south.<ref>Божилов, Гюзелев, p. 571</ref> Due to poor coordination with the Byzantines, the Bulgarian army met the Serbs, whose army numbered 15,000 men as well, alone near Velbazhd ([[Kyustendil]]).<ref>Fine, pp. 271–272</ref> On a personal meeting, the two rulers agreed to a one-day truce as both were expecting reinforcements. Backed on the agreement, Michael Shishman allowed his army to disperse in search for provisions. However, in the morning of 28 July, the main Serbian reinforcements, 1,000 heavily armed Catalan horsemen under the command of the King's son [[Stephen Uroš IV Dušan of Serbia|Stephen Dušan]] arrived, and the Serbs broke their word and attacked the Bulgarians.<ref name="g265">"Historia by John Kantakouzenos" in GIBI, vol. X, Bulgarian Academy of Sciences, Sofia, p. 265</ref> Despite the unexpected assault, Michael Shishman tried to bring his army to order but it was too late and the Serbs were victorious.<ref name="a262"/> The outcome of the battle shaped the balance of power in the Balkans for the next decades to come and although Bulgaria did not lose territory, the Serbs could occupy much of Macedonia.<ref name="f272">Fine, p. 272</ref>

==Death and legacy==
[[File:Манастир Св Ђорђа.JPG|200px|thumb|right|The [[Church of St. George, Staro Nagoričane|Church of St George]] where Michael Shishman was buried.]]
The circumstances around the death of Michael Shishman are unclear. According to the Byzantine emperor and historian [[John VI Cantacuzenus|John Kantakouzenos]] the emperor was mortally wounded in the battle and soon died<ref name="g265"/> while another Byzantine historian suggests that Michael Shishman lived for three more days not able to gain consciousness and died on the fourth day.<ref>Андреев, pp. 263–264</ref> The Serbian chronicles state that his horse fell during the battle and crashed his body. When his body was taken to Dečanski, he mourned him but pointed out that he preferred war to peace.<ref name="a264">Андреев, p. 264</ref> The early 15th-century Bulgarian scholar and cleric [[Gregory Tsamblak]] says that Michael Shishman was captured and killed by the son of the Serbian king, Stephen Dušan.<ref name="a264"/> He was buried in the [[Church of St. George, Staro Nagoričane|Church of St George]] in [[Staro Nagoričane]].<ref>Божилов, Гюзелев, p. 573</ref>

Michael Shishman is considered a vain, aggressive, and opportunistic ruler, whose [[Proteus|Protean]] foreign policy perhaps contributed to the battle that put an end to his life. At the same time he was clearly forceful and energetic, overcoming and reversing Bulgaria's losses during the uncertainty that preceded his accession, and managing to maintain internal peace and security within Bulgaria during his short reign. Andreev calls him the most remarkable 14th-century Bulgarian monarch.<ref name="a264"/> According to Kantacouzenos he desired to expand the country from Byzantium to the Istros, i. e. from [[Constantinople]] to the [[Danube]]<ref name="bg566"/><ref name="a259"/> which makes him the last medieval Bulgarian ruler who effectively attempted to capture the Byzantine capital. He was also the first Bulgarian ruler for decades who tried to lead a more active policy in Macedonia.<ref>Божилов, Гюзелев, p. 569</ref> Michael Shishman's seal is depicted on the [[Obverse and reverse|reverse]] of the Bulgarian 2 [[Bulgarian lev|levs]] banknote, issued in 1999 and 2005.<ref>[http://www.bnb.bg Bulgarian National Bank]. Notes and Coins in Circulation: [http://www.bnb.bg/bnb/notes_coins.nsf/vNotesCoins/CA9CED4AF282A1D5C2256B51003606F1?OpenDocument&EN 2 levs] (1999 issue) & [http://www.bnb.bg/bnb/notes_coins.nsf/vNotesCoins/50629E4972248ABBC22570CF0032A992?OpenDocument&EN 2 levs] (2005 issue). – Retrieved on 26 March 2009.</ref>

==Family==
Michael Shishman was married first to [[Anna Neda of Serbia]], a daughter of [[Stefan Milutin|Stefan Uroš II Milutin]] of Serbia. By this marriage he had several children, including [[Ivan Stephen of Bulgaria|Ivan Stefan]], who succeeded as emperor of Bulgaria (r. 1330-1331), Michael who ruled shortly as despot of Vidin<ref>{{cite web|url=http://vidin-info.hit.bg/vidinski%20vladeteli%202.htm|title=Rulers of Vidin|last=|first=|language=Bulgarian|accessdate=8 April 2011}}</ref> and [[Shishman (son of Michael Shishman)|Shishman]]. By his second marriage to [[Theodora Palaiologina, Empress of Bulgaria]], a daughter of [[Michael IX Palaeologus|Michael IX Palaiologos]] of Byzantium, Michael Shishman had several children whose names are unknown.

<div style="float: centre; width: 100%" class="NavFrame collapsed"><div style="background: #ccddcc; text-align: center; border: 1px solid #667766" class="NavHead">'''Family tree of the Shishman Dynasty'''{{cref|c}}</div>
<div class="NavContent" style="font-size:normal; text-align:center">
{{chart top|width=100%<!--|Add alternativ name here-->}}
{{chart/start|align=center}}
{{Chart| | | | | | | | | | | | | | | | | | | | |Shishman|Shishman=[[Shishman of Vidin|Shishman]] <br> <small>married to Unknown</small>}}
{{Chart| | | | | | | | | |,|-|-|-|-|-|-|-|-|-|-|-|+|-|-|-|.| }}
{{Chart| | | | | | | | |Mhai3| | | | | | | | | |Blaur| |Kerat|Mhai3='''Michael Shishman''' <br> (r.&nbsp;1323–1330) <small>married to 1.&nbsp;[[Anna Neda of Serbia|Anna Neda]]</small> <small>2.&nbsp;[[Theodora Palaiologina, Empress of Bulgaria|Theodora Palaiologina]]</small>|Blaur=[[Belaur]]|Kerat=[[Keratsa Petritsa]], <small>married to despot Sratsimir</small>}}
{{Chart| |,|-|-|-|v|-|-|-|+|-|-|-|v|-|-|-|.| | | | | | | |!| }}
{{Chart|IoanS| |Shish| |Mhai4| |Ludov| |Unkn2| | | | | | |!|IoanS=1.&nbsp;'''[[Ivan Stephen of Bulgaria|Ivan Stephen]]''' <br> (r.&nbsp;1330–1331)|Shish=1.&nbsp;[[Shishman (son of Michael Shishman)|Shishman]]|Mhai4=1.&nbsp;Michael, <br> <small>married to unknown</small>|Ludov=1.&nbsp;Ludovik|Unkn2=2.&nbsp;Unknown}}
{{Chart| |,|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|-|-|-|-|'| }}
{{Chart|IoanA| |IoanK| |Mhail| |Elena| |Teodo|IoanA='''[[Ivan Alexander of Bulgaria|Ivan Alexander]]''' <br> (r.&nbsp;1331–1371) <small>married to 1.&nbsp;[[Theodora of Wallachia|Theodora Besarab]]</small> <small>2.&nbsp;[[Sarah-Theodora]]</small>|IoanK=[[John Komnenos Asen]] <small>married to 1.&nbsp;Unknown</small> <small>2.&nbsp;Anna Palaiologina</small>|Mhail=Michael <br> <small>married to unknown</small>|Elena=[[Helena of Bulgaria|Helena]] <br> <small>married to [[Stephen Uroš IV Dušan of Serbia|Stefan Dushan]] (r.&nbsp;1331–1355)</small>|Teodo=Theodora}}
{{Chart| |!| | | | | | | | | | | |!| | | |!| }}
{{Chart| |!| | | | | | | | | | |AlexK| |Shish|AlexK=Alexander Komnenus Asen, Xenia Ivanina Komnena|Shish=Shishman}}
{{Chart| |)|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|v|-|-|-|.| }}
{{Chart|Mhai4| |IoanS| |IoanA| |KeraT| |KeraM| |Ioan2| |IoanA1| |Desis| |Vasil|Mhai4=1.&nbsp;[[Michael Asen IV of Bulgaria|Michael IV Asen]] <small>married to Irina Palaiologina</small>|IoanS=1.&nbsp;'''[[Ivan Sratsimir of Bulgaria|Ivan Sratsimir]]''' <br> (r.&nbsp;1356–1396) <small>married to Anna</small>|IoanA=1.&nbsp;[[Ivan Asen IV of Bulgaria|Ivan Asen IV]]|KeraT=1.&nbsp;[[Kera Tamara]] <small>married to Constantine</small> <small>[[Murad I]] (r.&nbsp;1362–1389)</small>|KeraM=2.&nbsp;[[Keratsa of Bulgaria|Keratsa-Maria]] <small>married to [[Andronikos IV Palaiologos]] (r.&nbsp;1376–1379)</small>|Ioan2=2.&nbsp;'''[[Ivan Shishman of Bulgaria|Ivan Shishman]]''' <br> (r.&nbsp;1371–1395) <small>married to 1.&nbsp;[[Kira Maria]]</small> <small>2.&nbsp;Dragana</small>|IoanA1=2.&nbsp;[[Ivan Asen V of Bulgaria|Ivan Asen V]]|Desis=2.&nbsp;[[Desislava of Bulgaria|Desislava]]|Vasil=2.&nbsp;Vasilisa}}
{{Chart| |,|-|-|-|+|-|-|-|.| | | |,|-|-|-|(| | | |)|-|-|-|v|-|-|-|v|-|-|-|.| }}
{{Chart|Konst| |Dorot| |Unkn1| |Ioann| |Unkn2| |Alxnd| |Fruzh| |Kerat| |Unkn4|Konst='''[[Constantine II of Bulgaria|Constantine II]]''' <br> (r.&nbsp;1397–1422)|Dorot=[[Dorothea of Bulgaria|Dorothea]] <small>married to [[Tvrtko I of Bosnia|Tvrtko I]] (r.&nbsp;1353-1391)</small>|Unkn1=Unknown daughter|Ioann=[[John VII Palaiologos]] (r.&nbsp;1390)|Unkn2=2 unknown daughters|Alxnd=Alexander|Fruzh=[[Fruzhin]] <small>married to unknown</small>|Kerat=Keratsa|Unkn4=4 unknown; [[Patriarch Joseph II of Constantinople|Patriarch Joseph II]] <small>possible illegitimate son</small>}}
{{Chart| | | | | | | | | | | | | | | | | | | | | | | |,|-|^|-|.| }}
{{Chart| | | | | | | | | | | | | | | | | | | | | | |Shish| |Unkn2|Shish=Shishman|Unkn2=2 unknown}}
{{chart/end}}
{{chart bottom}}
</div></div>

== Ancestry ==

<div style="clear: both; width: 100%; padding: 0; text-align: left; border: none;" class="NavFrame">
<div style="background: #ccddcc; text-align: center; border: 1px solid #667766" class="NavHead">'''Ancestors of Michael Shishman of Bulgaria'''
</div>
<div class="NavContent" style="display:none;">
<center>{{ahnentafel-compact5
|style=font-size: 90%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|boxstyle_1=background-color: #fcc;
|boxstyle_2=background-color: #fb9;
|boxstyle_3=background-color: #ffc;
|boxstyle_4=background-color: #bfc;
|boxstyle_5=background-color: #9fe;
|1= 1. '''Michael Shishman'''
|2= 2. [[Shishman of Vidin]]
|3= 3. daughter of [[Anna-Theodora Asenina of Bulgaria|Anna Asenina]]
|4= 
|5= 
|6= 6. [[Peter (sebastokrator)|Peter]]
|7= 7. [[Anna-Theodora Asenina of Bulgaria|Anna Asenina]]
|8= 
|10= 
|11= 
|12= 
|13= 
|14= 14. [[Ivan Asen II of Bulgaria]] 
|15= 15. [[Irene Komnene of Epirus]] 
|22= 
|23= 
|24= 
|25= 
|26= 
|27= 
|28= 28. [[Ivan Asen I of Bulgaria]]
|29= 29. [[Elena, Queen consort of Bulgaria|Elena of Bulgaria]] 
|30= 30. [[Theodore Komnenos Doukas]]
|31= 31. [[Maria Petraliphaina]]
}}</center></div></div>

==Timeline==
*1291 — Michael Shishman is engaged to Anna Neda of Serbia
*1298 or 1299 — Marries Anna Neda
*By 1308 — Becomes Despot of Vidin
*1323 — Elected Emperor of Bulgaria by the nobility, uses the name Michael Asen
*1324 — Successful war with the Byzantine Empire; divorces his first wife to marry Theodora Palaiologina
*1327 — Involvement into the Byzantine civil war; Treaty of Chernomen
*1329 — Definitive peace treaty with the Byzantines; anti-Serbian agreement
*28 July 1330 — Battle of Velbazhd; Michael Shishman is mortally wounded and dies

==Sources==

===Notes===

{{Cnote|a|The common usage Michael Shishman is technically inaccurate, as it consists of the name ''Michael'' followed by the patronymic ''Shishman'', i.e., ''Michael [the son of] Shishman''. Contemporary Bulgarian and Byzantine sources show that the emperor reigned under the name ''Michael Asen''. He is also known among historians as ''Michael III Shishman'' or ''Michael III Shishman Asen''.}}
{{Cnote|b|Trajanopolis was a town near modern [[Feres, Evros|Feres]], situated at {{convert|2|km}} of the [[Maritsa]] river in [[Western Thrace]].<ref>Editorial footnote to "Historia by John Kantakouzenos" in GIBI, vol. X, Bulgarian Academy of Sciences, Sofia, p. 230</ref>}}
{{Cnote|c|The numbers designate which wife each child was born to.}}

===Citations===
{{reflist|3}}

===Bibliography===
* {{cite book
 | title = Българските ханове и царе 
 |trans-title= The Bulgarian Khans and Tsars
 | last = Андреев (Andreev)
 | first = Йордан (Jordan)
 | first2 = Милчо (Milcho)
 |last2 = Лалков (Lalkov)
 | chapter = 
 | year = 1996
 | language = Bulgarian
 | publisher = Абагар (Abagar)
 | location = Велико Търново ([[Veliko Tarnovo]])
 | isbn = 954-427-216-X
 }}
* {{cite book
 | title = История на средновековна България VII-XIV век 
 |trans-title= History of Medieval Bulgaria 7th–14th Centuries
 | last = Вожилов (Bozhilov)
 | first = Иван (Ivan)
 | first2 = Васил (Vasil)
 | last2 = Гюзелев (Gyuzelev)
 | author-link2=Vasil Gyuzelev
 | chapter = 
 | year = 1999
 | language = Bulgarian
 | publisher = Анубис (Anubis)
 | location = София ([[Sofia]])
 | isbn = 954-426-204-0
 }}
* {{cite book
 | title = The Late Medieval Balkans, A Critical Survey from the Late Twelfth Century to the Ottoman Conquest
 | last = Fine
 | first = J.
 | chapter = 
 | year = 1987
 | publisher = The University of Michigan Press
 | location = 
 | isbn = 0-472-10079-3
 }}
* {{cite book
 | title = Гръцки извори за българската история (ГИБИ), том X (Greek Sources for Bulgarian History (GIBI), volume X)
 | url=http://promacedonia.org/gibi/10/gal/10_224.html
 | last = Колектив (Collective)
 | first = 
 | chapter = XIX. Йоан Кантакузин (XIX. John Kantakouzenos)
 | year = 1980
 | language = Bulgarian, Greek
 | publisher = Издателство на БАН ([[Bulgarian Academy of Sciences]] Press) 
 | location = София (Sofia)
 | isbn = 
 }}
* {{cite book
 | title = A History of Byzantium
 | last = Gregory
 | first = T.
 | chapter = 
 | year = 2005
 | publisher = Blackwell Publishing
 | location = 
 | isbn = 0-631-23513-2
 }}
* {{cite book
 |title=Geschichte der Bulgaren
 |language=German |authorlink=Konstantin Josef Jireček
 |last=Jireček
 |first=Konstantin
 |publisher=Textor Verlag
 |location=[[Frankfurt am Main]] 
 |year=1977
 |isbn=978-3-938402-11-5
 }}
* {{cite book
 | title = The Oxford Dictionary of Byzantium
 | last = Kazhdan
 | first = A.
 |author2=collective
 | chapter = Volume II
 | year = 1991
 | publisher = [[Oxford University Press]]
 | location = [[New York City|New York]], [[Oxford]]
 | isbn = 0-19-504652-8
 }}
* {{cite book
 |last=Павлов (Pavlov)
 |first=Пламен (Plamen)
 |title=Бунтари и авантюристи в средновековна България (Rebels and Adventurers in Medieval Bulgaria)
 |publisher=LiterNet
 |location=[[Varna]]
 |year=2005
 |chapter=Руски "бродници", политически политически бегълци и военачалници през XII-XIV век (Russian "Vagrands", Political Refugees and Commanders during the 12th–14th Centuries)
 |chapterurl=http://liternet.bg/publish13/p_pavlov/buntari/ruski.htm
 |language=Bulgarian
 |isbn=954-304-152-0}}

==External links==
* {{cite web |url= http://sitemaker.umich.edu/mladjov/files/bulgarian_rulers.pdf |title= Detailed List of Bulgarian Rulers |last=Mladjov |first=Ian |work= |publisher= |accessdate=13 April 2011}}
* {{cite web |url= http://ald-bg.narod.ru/biblioteka/Andronik-Gramota-1325.htm |title= Хрисовул на Андроник ІІ за Зографския манастир, потвърждаващ дарение от Михаил Шишман (Chrysobull of Andronikos II to the Zograf Monastery, confirming a donation of Michael Shishman) |last= Горянов|first= Б. Т.|language=Bulgarian, translation from Russian |year= 1951|work= Сборник документов по социально-экономической истории Византии (Collection of Documents of Social-Economical History of Byzantium)|publisher= Академия Наук СССР|accessdate=13 April 2011}}
* {{cite web |url= http://borislav.digicom.bg/statii/shishman.htm |title= Монограми на Михаил Шишман (Monograms of Michael Shishman) |last=Иванов |first=Борислав |language=Bulgarian |date= 10 March 2003 |work= |publisher= |accessdate=13 April 2011}}
* {{cite web |url= http://vidin-info.hit.bg/vidinski%20vladeteli%202.htm |title= Владетели на Видин (Rulers of Vidin) |last= |first= |language=Bulgarian |work= |publisher= |accessdate=13 April 2011}}

{{s-start}}
{{s-bef|before=[[George II of Bulgaria|George Terter II]]}}
{{s-ttl|title=[[List of Bulgarian monarchs|Emperor of Bulgaria]]|years=1323–1330}}
{{s-aft|after=[[Ivan Stephen of Bulgaria|Ivan Stefan]]}}
{{end}}

{{Bulgarian monarchs}}

{{DEFAULTSORT:Bulgaria, Michael Shishman Of}}
[[Category:13th-century births]]
[[Category:1330 deaths]]
[[Category:14th-century Bulgarian emperors]]
[[Category:Military personnel killed in action]]
[[Category:Orthodox monarchs]]
[[Category:Bulgarian people of the Byzantine–Bulgarian Wars]]
[[Category:Shishman dynasty]]
[[Category:Despots (court title)]]
[[Category:Tsardom of Vidin]]
[[Category:People from Vidin]]