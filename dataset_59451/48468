[[File:Hans Herren.jpg|400px|right|Hans Rudolf Herren (2009)]]
'''Hans Rudolf Herren''' (born November 30, 1947 in [[Mühleberg]], [[Switzerland]])<ref name=Tyler/> is a Swiss [[list of entomologists|entomologist]], [[farmer]] and development specialist. He was the first Swiss to receive the 1995 [[World Food Prize]] and the 2013 [[Right Livelihood Award]] for leading a major biological pest management campaign in Africa, successfully fighting the [[Phenacoccus manihoti|Cassava mealybug]] and averting a major food crisis that could have claimed an estimated 20 million lives.<ref name=WFP/><ref>{{fr}} Pascaline Minet, "Hans Herren, agronome durable", ''[[Le Temps]]'', Friday 18 October 2013.</ref>

Herren is the president and CEO of the Washington-based Millennium Institute<ref>http://www.millenniuminstitute.net/about/hans.html</ref> and co-founder and president of the Swiss foundation Biovision.<ref>http://www.biovision.ch/en/biovision/wer-wir-sind/stiftungsrat/dr-hans-rudolf-herren/</ref> He co-chaired the [[International Assessment of Agricultural Knowledge, Science and Technology for Development|International Assessment of Agricultural Knowledge, Science and Technology for Development (IAASTD)]] published 2008, and was Director General of [[International Centre of Insect Physiology and Ecology|International Centre of Insect Physiology and Ecology (''icipe'')]] in Nairobi, Kenya from 1994 to 2005. He was involved in the preparations of the United Nations' Rio+20 Conference on Sustainable Development with Biovision Foundation and Millennium Institute.

==Career==
After receiving his M.Sc. in Agronomy from the [[Swiss Federal Institute of Technology in Zurich|ETH Zurich]] and his doctorate in Biological Control from the same University in 1977, Herren did his post-doctoral studies in Biological Control of insect pests at the [[University of California in Berkeley]].

Only 32 years old, Herren then went to work for the [[International Institute of Tropical Agriculture]] (IITA) in [[Ibadan]], [[Nigeria]]. There he built the Biological Control Programme and designed and implemented the largest biological pest-management known to date, fighting the Cassava mealybug (''[[Phenacoccus manihoti]]'') with its natural enemy, a parasitic wasp (Anagyrus lopezi), which he found in South America. He saved an estimated 20 million lives by averting a major food crisis. For this achievement he received numerous awards including the 1995 [[World Food Prize]]<ref name=WFP/> and the 2003 [[Tyler Prize for Environmental Achievement]].<ref name=Tyler/>

Herren then developed a number of other biological control programmes against field and tree crops as well as aquatic weeds across sub-Saharan Africa.<ref>{{cite web|url=http://www.worldbank.org/html/cgiar/newsletter/Mar96/4wfp.htm |title=IITA's Research Wins the 1995 World Food Prize |publisher=CGIAR Newsletter |date=March 1996 |accessdate=2012-10-24}}</ref> Subsequently Herren became Director of the [[International Centre of Insect Physiology and Ecology]] (icipe). From 1994 to 2005, he was also the Editor-in Chief of the Journal ''Insect Science and Its Application''. With some of his prize money he went on to found Biovision Foundation in 1998 in Zurich. The organisation works with pilot projects, communication projects and political projects to foster ecological development in the global North and South and has grown to an annual budget of over $8 Mio. Since 2005 he head the Washington-based [[Millennium Institute (Washington)|Millennium Institute]], dedicated to system dynamics modeling for scenario-based [[sustainable development]] policy support.<ref>http://www.millennium-institute.org/about/hans.html</ref>
Furthermore, Herren was co-author and co-chairman of the World Agriculture Report of the International Assessment of Agricultural Knowledge, Science and Technology for Development (IAASTD) in 2008.

Further Memberships:
* Until 2007: President of the International Association for the Plant Protection Sciences (IAPPS). 
* Until 2006: Two-term member of the [[CGIAR|Consortium of International Agricultural Research Centers]] (CGIAR) Science Council.<ref>[http://www.cgiar.org/pdf/agm07/agm07_media_biographies.pdf Media biographies]. CGIAR.</ref> 
* Until 2011: Member of the Scientific Advisory Council of the [[École Polytechnique Fédérale de Lausanne|Swiss Institute of Technology in Lausanne]].
* Associate [[National Academy of Sciences|US National Academy of Sciences (NAS)]]<ref>{{cite web|url=http://www.nasonline.org |title=National Academy of Sciences |publisher=Nasonline.org |date= |accessdate=2012-10-24}}</ref>
* Associate [[TWAS|TWAS, the academy of sciences for the developing world]].
* Member Entomological Society of America
* Member African Association of Insect Scientists
* Member [[International Organization for Biological Control]]
* Member [[American Institute of Biological Science]]
* Member [[American Association for the Advancement of Science]].
* Member Worldwatch Institute's Nourishing the Planet Advisory Group.<ref>{{cite web|url=http://blogs.worldwatch.org/nourishingtheplanet/meet-the-nourishing-the-planet-advisory-group-hans-herren-advisory-group-nourishing-the-planet-millenium-institute-agriculture-poverty-hunger-food-security-africa-funding-state-of-the-world-2011 |title=Meet the Nourishing the Planet Advisory Group: Hans Herren |publisher=Nourishing the Planet |date=April 23, 2010 |accessdate=2012-10-24}}</ref>
* Member of the [[European Network of Scientists for Social and Environmental Responsibility]].<ref>{{cite web|url=http://www.huffingtonpost.com/ken-roseboro/biotechs-assault-on-balan_b_5432699.html|title=Biotech's Assault On Balanced Journalism|year=2014|publisher=Huffington Post}}</ref>

==Opinion==
Based on his deep and long experience in biological pest control, sustainable agriculture and rural development issues Herren is an outspoken proponent of agro-ecology, organic and other forms of sustainable agriculture. He criticises that [[GMO]]s currently, and most probably also in the future, offer no significant economic or social advantages to poor small-scale farmers, that they reduce the resilience of agricultural systems through reducing the diversity of crops and the genetical diversity within varieties at a time when more diversity is needed from crop/animal to system levels. According to Herren:
"Today’s GMOs don’t produce more food, they help cut production costs - in the first few years until insects and weeds catch up again - as we have seen earlier with the use of insecticides. That’s why we introduced Integrated Pest Management (IPM), which was meant to treat the causes of pest outbreaks. The GMO crop cultivars that are used today are basically a step back, to the pre-IPM period. Many pest problems can actually be solved with classical breeding and marker assisted breeding methods, that do not force farmers into costly licensing agreements with seed companies or lock them into the use of specific herbicides."<ref>{{cite web|url=http://www.globalchange-discussion.org/interview/hans_herren |title=Hans R. Herren |publisher=Global Change Discussion |date= |accessdate=2012-10-24}}</ref>

Dr Herren believes the way forward was well described in the IAASTD Report. It called for a shift in paradigm, the transition of the industrial and external energy dependent agriculture into a multifunctional agriculture that promotes a system-approach to production and problem solving. The report suggests that business as usual is not an option and recommends a number of action in research and implementation that address the food and nutrition security now and for the decades ahead. Dr. Herren has been at the forefront of the conversation and action for the implementation of the IAASTD report - which was sponsored by 6  UN agencies and the World Bank, involving over 400 scientists, NGOs and the private sector and had been endorsed at the final plenary by 59 countries from around the world.<ref>{{cite web|url=https://www.youtube.com/watch?v=bo7mxGzGkOU&feature=related |title=What Will the World Eat? Dr Hans R. Herren (2 of 5) |publisher=YouTube |date=2010-01-27 |accessdate=2012-10-24}}</ref>

==Prizes==
* 1991 Sir and Lady Rank Prize for Nutrition
* 1991 Merit Award for Outstanding Service to Crop Protection from the XII International Plant Protection Congress at Rio de Janeiro
* 1995 [[World Food Prize]]<ref name=WFP>{{cite web|url=http://www.worldfoodprize.org/en/laureates/19871999_laureates/1995_herren |title=1995: Dr Hans Rudolf Herren |publisher=World Food Prize |date= |accessdate=2012-10-24}}</ref>
* 1995 [[Kilby International Awards|Kilby International Award]]
* 2002 Dr J. E. Brandenberger Prize<ref>{{cite web|url=http://www.stiftungbrandenberger.ch/preis_e.htm |title=Award Winners |publisher=Foundation Dr J. E. Brandenberger |date= |accessdate=2012-10-24}}</ref>
* 2003 [[Tyler Prize for Environmental Achievement]]<ref name=Tyler>{{cite web|url=http://www.usc.edu/dept/LAS/tylerprize/laureates/tyler2003.html#herren |title=Tyler Prize Dr Hans R. Herren |publisher=University of Southern California |date= |accessdate=2012-10-24}}</ref>
* 2004 Honorary professor title (Prof. h.c.), Hubei University, Wuhan, PRC
* 2004 Honorary doctorate (Doctor es Science Honoris Causa), Kenyatta University, Nairobi, Kenya
* 2010 One World Award<ref>{{cite web|url=http://www.one-world-award.de/2010-one-world-award.html |title=One World Award 2010 |publisher=Rapunzel Naturkost |date= |accessdate=2012-10-24}}</ref>
* 2013 [[Right Livelihood Award]]<ref>{{cite news |url=http://www.dw.de/alternative-nobel-prize-awarded-for-fight-against-chemical-weapons/a-17116300 |title='Alternative Nobel Prize' awarded for fight against chemical weapons |newspaper=[[Deutsche Welle]] |date=September 26, 2013 }}</ref>
* 2014 Dr. Herren received the 2013 SwissAward in the category "Society".<ref>{{cite web|url=http://www.srf.ch/unterhaltung/events-shows/swissaward/swissaward-die-gewinner-im-ueberblick |title="SwissAward": Die Gewinner im Überblick }}</ref>

==Publications==
Herren has published extensively (over 70 articles).<ref name=Tyler/> His own publications include the following:

*Herren, H.R. (2011). "No sustainable development without healthy, nutritious and culturally adapted food for all". UNEP Perspectives on Rio+20 on http://www.unep.org/environmentalgovernance/PerspectivesonRIO20/HansHerren/tabid/78431/Default.aspx
*Pretty, Jules; Sutherland, William J.; Ashby, Jacqueline; Auburn, Jill; Baulcombe, David; Bell, Michael; Bentley, Herren, Hans; …. “The top 100 questions of importance to the future of global agriculture”, International Journal of Agricultural Sustainability, Volume 8, Number 4, 2010-11-30
*Herren, H.R.; Mbogo, C. “The Role of DDT in Malaria Control”, Environmental Health Perspectives, 2010
*Beverly D. McIntyre, Hans R. Herren, Judi Wakhungu, Robert T. Watson. “IAASTD International Assessment of Agricultural Knowledge, Science and Technology for Development: Global Report”, International Assessment of Agricultural Knowledge, Science, and Technology for Development (Project), Island Press, 2009
*Andrea M. Bassi, A. Drake, E.L. Tennyson and H.R. Herren. 2009. “Evaluating the Creation of a Parallel Non-Oil Transportation System in an Oil Constrained Future”, TRB Conference: Annual Conference of the Transportation Research Board of the National Academies of Science, Engineering, and Medicine, January 11–15, 2009, Washington DC, USA.
*Pasquet RS, Peltier A, Hufford MB, Oudin E, Saulnier J, Paul L, Knudsen JT, Herren HR, Gepts P. 2008. Long-distance pollen flow assessment through evaluation of pollinator foraging range suggests transgene escape distances. PNAS: 2008;105(36):13456-6
*Herren, H. & Baumgärtner, J. (2007). From Integrated Pest Management to adaptive Ecosystem Management. In S.J. Scherr & J.A. McNeely (Eds.), Farming with Nature: the science and practice of ecoagriculre. Washington D.C.: Island Press.
*Fritz J. Häni, Laszlo Pinter and Hans R Herren. 2007. Sustainable Agriculture: from common principles to common practices. Proceeding and outputs of the first symposium of the international forum on assessing sustainability in agriculture. March 16, 2006, Bern, Switzerland. Edited by Fritz J. Häni, Laszlo Pinter and Hans R Herren. Published by IISD
*Leif Christian Stige, Jørn Stave, Kung-Sik Chan, Lorenzo Ciannelli, Nathalie Pettorelli, Michael Glantz, Hans R. Herren, and Nils Chr. Stenseth.  From the Cover: The effect of climate variation on agro-pastoral production in Africa. PNAS 2006 103: 3049-3053.
*Herren, H.R. 2005. Sustainable Pest Management for Global Food Security. In: Entomology at the Land Grant University Perspectives from the Texas A&M University Centenary. Kevin M. Heinz,Raymond E. Frisbie,Carlos Enrique Bográn (Eds.), 2003 Texas A&M University Press, College Station, TX / USA
*Herren, H.R. 2003. The War against Poverty: the Way Forward. In Resource Management for Poverty Reduction: Approaches and Technologies, Selected Contributions to Ethio-Forum 2002.  Aseffa Abreha, Getachew Tikubet and Johann Baumgaertner (eds). Published by the Ethiopian Social Rehabilitation Fund
*Herren, H.R. 2003. Genetically engineered crops and sustainable agriculture, in: Methods for Risk Assessment of Transgenic Plants, 35, IV. Biodiversity and Biotechnology. K. Ammann, Y. Jacot and R. Braun (eds), 2003 Birkhäuser Verlag Basel/Switzerland
*Zeddies J., Schaab R.P., Neuenschwander P., Herren H.R. Economics of biological control of cassava mealybug in Africa (2001) Agricultural Economics, 24 (2), pp.&nbsp;209–219.
*Herren, H.R., Neuenschwander, P. 1991. Biological control of cassava pests in Africa. Annual Review of Entomology 36:257-283.
*Gutierrez, A.P, B. Wermelinger, F. Schulthess, J.U. Baumgärtner, H.R. Herren, C.K. Ellis & J.S. Yaninek, 1988. Analysis of biological control of cassava pests in Africa: I. Simulation of carbon, nitrogen and water dynamics in cassava. Journal of Applied Ecology, 25:901-920.
*Herren, H.R., P. Neuenschwander, R.D. Hennessey & W.N.O. Hammond, 1987. Introduction and dispersal of Epidinocarsis lopezi (Hymenoptera: Encyrtidae), an exotic parasitoid of the cassava  mealybug, Phenacoccus manihoti (Homoptera: Pseudococcidae), in Africa. Agricultural Ecosystems and Environment, 19:131-144.
*Herren, H.R., 1987. Africa-wide biological control project of cassava and cassava green mites: A review of objectives and achievements. Insect Science and Application, 8:837-840.

Herren was coordinating author of the Agriculture Chapter of UNEP's "Green Economy Report" (2011).<ref name="UNEP">{{cite web|url=http://www.unep.org/greeneconomy/Portals/88/documents/ger/ger_final_dec_2011/2.0-AGR-Agriculture.pdf |title=UNEP Green Economy Report - Agriculture Chapter |format=PDF |publisher=United Nations Environment Programme |date=December 2011 |accessdate=2012-10-24}}</ref>

== Publications about Herren ==
* Herbert Cerutti: Wie Hans Rudolf Herren 20 Millionen Menschen rettete. Die ökologische Erfolgsstory eines Schweizers (biography, available in German only). Orell Füssli, Zürich 2011, ISBN 978-3-280-05409-3
* Beat Pfändler: Swiss Guest Book. Porträts inspirierender Persönlichkeiten (available in German only). Offizin, Zürich 2007, ISBN 978-3-907496-51-0

==References==
{{Reflist|30em}}

==External links==
* [http://www.biovision.ch Biovision Foundation]
* [http://www.millennium-institute.org Millennium Institute]
* [http://www.agassessment.org IAASTD Report]
* [http://www.worldfoodprize.org World Food Prize]

{{Authority control}}

{{DEFAULTSORT:Herren, Hans Rudolf}}
[[Category:1947 births]]
[[Category:Living people]]
[[Category:Swiss entomologists]]
[[Category:Swiss farmers]]
[[Category:Sustainability advocates]]
[[Category:Members of the United States National Academy of Sciences]]
[[Category:Right Livelihood Award laureates]]
[[Category:ETH Zurich alumni]]
[[Category:Swiss expatriates in Nigeria]]
[[Category:Swiss expatriates in Kenya]]