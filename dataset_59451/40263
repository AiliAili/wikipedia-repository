{{Infobox religious building
|building_name           = Notre-Dame de Louviers
|infobox_width           = 
|image                   = Louviers eglise.jpg
|image_size              = 
|caption                 = Notre-Dame de Louviers, south façade and porch
|map_type                = 
|map_size                = 
|map_caption             = 
|location                = 
|geo                     = {{Coord|49|12|47|N|1|10|13|E|type:landmark_region:FR|display=title,inline}}
|latitude                = 
|longitude               = 
|religious_affiliation   = [[Catholic Church]]
|rite                    = [[Roman Rite]]
|region                  = [[Upper Normandy]]
|state                   = [[France]]
|province                = [[Roman Catholic Archdiocese of Rouen|Archdiocese of Rouen]]
|territory               = 
|prefecture              = 
|sector                  = 
|district                = 
|cercle                  = 
|municipality            = 
|consecration_year       = 
|status                  = 
|functional_status       = Active
|leadership              = 
|website                 = {{url|http://www.paroisselouviers.com//}}
|architecture            = yes
|architect               = 
|architecture_type       = [[Parish church]]
|architecture_style      = [[French Gothic architecture|French Gothic]]
|general_contractor      = 
|facade_direction        = 
|groundbreaking          = {{start date|c.1190}}
|year_completed          = {{end date|1510}}
|construction_cost       = 
|specifications          = 
|capacity                = 
|length                  = 
|width                   = 
|width_nave              = 
|height_max              = 
|dome_quantity           = 
|dome_height_outer       = 
|dome_height_inner       = 
|dome_dia_outer          = 
|dome_dia_inner          = 
|minaret_quantity        = 
|minaret_height          = 
|spire_quantity          = 
|spire_height            = 
|materials               = 
|nrhp                    = 
|added                   = 
|refnum                  = 
|designation2            = Monument Historique
|designation2_offname    =   	Notre-Dame de Louviers
|designation2_date       = 1846
|designation2_number     = [http://www.culture.gouv.fr/public/mistral/merimee_fr?ACTION=CHERCHER&FIELD_1=REF&VALUE_1=PA00099471 PA00099471]<ref>{{cite web|title=Mérimée database|url=http://www.culture.gouv.fr/public/mistral/merimee_fr?ACTION=CHERCHER&FIELD_1=REF&VALUE_1=PA00099471|accessdate=29 April 2015}}</ref>
|designation2_free1name  = Denomination
|designation2_free1value = {{lang|fr|Église}}
}}
The '''Church of Notre-Dame de Louviers''' is a [[parish church]] located in [[Louviers]], a town in the [[Eure]] department. It is a notable example of [[French Gothic architecture|Gothic church architecture]] in northern France. The north façade, and, especially the south façade and porch, are some of the best examples of late [[Flamboyant]] Gothic architecture in France.<ref name="Le Mercier" />

== Chronological overview of Notre-Dame de Louviers ==
The plan of the church today is the result of several major construction campaigns that began at the end of the twelfth century and concluded in the early sixteenth. The building features elements from both the [[High Gothic]] and Late Gothic (or [[Flamboyant]]) periods.<ref name=Verdier>{{cite book|last1=Verdier|first1=François|title="L'église Paroissiale Notre-Dame de Louviers," '' in ''Congrès Archéologique de France'', 1980, Évrecin, Lieuvin, Pays d'ouche."|date=1984|publisher=SFA|location=Paris|pages=9–28}}</ref>
#c. 1190-1240s: chevet, nave, and west façade are constructed
#1346: English invaders burn the lantern tower/spire
#1385: Spire over the crossing built
#1414: Challenge Tower construction begins
#1493-96: North facade under construction (architect: Jehan Gillot)
#1496-1506: South facade under construction
#1506-1510: South porch under construction
#1705: Spire destroyed in a severe thunderstorm
#Restoration work in the 19th and 20th centuries, including the cementing of the triforium windows and restoration of both interior and exterior sculptures (L. Delahaye and V. Pyanet)
#2003-2013: massive structural repairs and repainting of the nave

==Interior features==

=== Chevet ===
The [[chevet]], or eastern end of the church containing the altar, has been reworked several times since the thirteenth century. It is difficult to know its original form.<ref name=Mapping>{{cite web|url=http://mappinggothic.org/building/1145|website=Mapping Gothic|accessdate=9 April 2015}}</ref> Documentary sources do not provide a reliable foundation date so scholars have relied on style.

=== 13th-century nave ===
[[File:Nef Notre-Dame Louviers.JPG|thumb|Nave, Notre-Dame de Louviers]]Over the course of the early thirteenth century construction probably progressed westward after the completion of the eastern chevet. The [[nave]], or central vessel of the church, is seven [[Bay (architecture)|bays]] in length and was originally flanked by one aisle on each side. The elevation of the nave consists of three stories: nave arcade, [[triforium]], and [[clerestory]]. The nave columns appear distantly related to [[Notre-Dame de Paris]] but closer prototypes can be found in the hemicycle of [[Rouen Cathedral]].<ref name=Mapping /> A triple bundle of shafts rest on corbels designed to resemble human heads. The shafts pass a very unusual trilobed triforium (the middle level) which was originally open to the space between the aisle vaults and aisle roof.<ref name=Mapping /> The nave [[Ribbed vault|vaults]] spring about two-thirds up the [[clerestory]] (the row of windows that continue along the nave) but were probably lower before the upper parts of the church were modified during the expansion in the 15th and 16th centuries. The nave was restored between 2006-2013 based on traces of medieval paint.

=== 15th- and 16th-century expansions ===
The church of Notre-Dame de Louviers underwent a dramatic expansion project during the [[Late Middle Ages]]. An entirely new [[aisle]] was added to the north between 1493 and 1496.<ref name="Le Mercier">{{cite book|last1=Le Mercier|first1=Edmond|title=Monographie de l'église Notre-Dame de Louviers|date=1906|publisher=Ch. Hérissey et fils|location=Évreux|pages=212}}</ref> According to historical documents, the architect was Jehan Gillot.<ref name=Verdier /> Mr. Gillot worked in the Late Gothic period, and his design for the expansion project shows this. The prismatic bases of the piers in this part of the church are much higher, the linear vault moldings disappear into piers and walls, vault responds intersect one another, and the window tracery is [[Flamboyant]] in nature—it resembles the flickering patterns of a flame. Documentary sources imply that Gillot continued work on the new south nave aisle between 1496-1506 but this has yet to be proven. However, many of the same architectural forms were employed on the south side with the notable omission of the intersecting vault responds. The expansion of the nave necessitated the reworking of the upper parts of the church, which received a glazed [[triforium]] (later filled with cement) and a modified system of [[flying buttresses]]. The amount of usable interior space increased dramatically as a result of these extensive expansion projects, which were funded in large part by Louviers’ cloth guilds and elite patrons, including Guillaume II le Roux, Duke of Bourgtheroulde.

===Stained glass===
[[File:Procession of the drapers, Church of Notre-Dame de Louviers.JPG|thumb|Stained glass in the south nave aisle depicting the procession of the drapers in the early sixteenth century]]A great deal of the late medieval [[stained glass]] has been lost over time; however, several excellent examples remain intact.<ref name="Hérold">{{cite book|last1=Hérold|first1=Michel|title=Louviers: Église Notre-Dame, "Le vitrage de Notre-Dame de Louviers: une histoire mouvementée"|date=2011|publisher=HB Impressions|location=Louviers|isbn=9782912947314|pages=29–40}}</ref> The window of the ''Apparition of Christ to Mary Magdalene'' is located in the south nave aisle. From left to right, it depicts St. George, Mary Magdalene, Christ, and St. Adrian. Guillaume Le Forestier, governor of Louviers in 1515, donated the window; Muraire restored part of it in 1903. Just to the right is a rectangular stained glass panel depicting the ''Procession of the Guilds''. It is equally significant because it depicts the guilds that made donations to fund the church expansion. Hoisting their blazons, weavers, stretchers, wool workers, and tanners accompany a bishop during a religious procession through the town of [[Louviers]]. The glass was visual proof of their patronage and piety.<ref>{{cite book|last1=Chaplain|first1=Jean-Michel|title=La chambre des tisseurs: Louviers, cité drapière, 1680-1840|date=1984|publisher=Champ Vallon|location=Seyssel|isbn=2903528403}}</ref>

=== Nave column sculptures ===
Relatively large sculptures of the [[12 Apostles]] adorn the nave of Notre-Dame de Louviers. These sculptures rest on foliate [[corbels]] just above the [[Capital (architecture)|capitals]] of the nave columns. The entire arrangement recalls the design of the upper chapel of the [[Sainte-Chapelle|Sainte-Chapelle in Paris]], which would suggest the nave of Notre-Dame de Louviers was still under construction during the late 1240s—the period when [[Louis IX]]’s celebrated chapel was completed in the capital. The sculptures themselves, however, are probably from the early 16th century, and can be linked to the artistic patronage of [[Georges d’Amboise]] at [[château de Gaillon]]. The figures—almost [[Mannerism|Manneristic]] in style—have undergone varying degrees of restoration, complicating technical studies.

=== Lantern tower ===
This [[Lantern tower|tower]] above the transept crossing has undergone several changes since its original construction in the thirteenth century. Today it features many [[Flamboyant]] motifs and structures dating to the early sixteenth century. A tall, slender [[spire]] that blew away in a storm in 1705 originally capped the tower.<ref name=Verdier />

==Exterior features==

===West façade===
[[File:Portail occidental.jpg|thumb|West portal and tympanum]]This thirteenth-century [[façade]] corresponds to the three original [[Bay (architecture)|bays]] of the nave.<ref name=Verdier/> The [[Tympanum (architecture)|tympanum]] with four rose motifs was part of a regional vocabulary. The [[Trumeau (architecture)|trumeau]] was replaced in 1607.<ref name=Verdier /> The principal entrance of the church shifted to the south following the completion of the south portal in the early 1500s, effectively reorienting the entire edifice from east-west to north-south in order to embrace the expanding town and to ensure the church remained the major focal point during processions.

===Challenge Tower===
[[File:Tour beffroi.jpg|thumb|"Challenge" Tower]]The years from 1410-1466 were a particularly troubled and violent time for Louviers. The only construction that occurred in the early fifteenth century was the addition of the “Challenge Tower” to the church of Notre-Dame de Louviers. Named after a military captain, construction began in 1414.<ref name=Verdier /> The tower design was influenced by defensive architecture. It features massive walls and enormous sloping buttresses to thwart attackers. The Lovériens told the English it was to be a bell tower, which it later became and remains so today. Its massive construction dominates the west façade of the church.

===North façade===
Jehan Gillot began work on the expansion of the north aisle and [[façade]] in 1493.<ref name=Verdier /> Because it faces a narrow street and is less ostentatious than its southern counterpart, the north facade has not received as much attention from restorers. It features four and a half bays, one of which contains a [[Portal (architecture)|portal]] leading to the interior. The bays are separated by [[Flying buttress|buttresses]] capped with [[pinnacle]]s that shoot upward at least an additional story. The width of each bay is devoted to stained glass windows. The design of the Flamboyant [[tracery]] is generally uniform throughout, with only a few small deviations between bays. Only the entry [[Portal (architecture)|portal]] features a [[gable]], which pierces the [[balustrade]] above. The openwork gable is steeply pitched and complements the tracery in the windows below. The original tier of buttresses and their Late Gothic pinnacles can be seen behind the 1493 expansion. Overall, this expansion is harmonious, refined, and simple—adjectives that do not necessarily describe the modifications made to the exterior of the southern half of the church.

===South façade and porch===
[[File:Notre-Dame de Louviers, south porch.jpg|left|thumb|Notre-Dame de Louviers, south porch]]These two parts of the church represent the zenith of the Late Gothic craft of masonry and are perhaps the best examples of the late [[Flamboyant|Flamboyant style]] in northern France (c. 1480-1525).<ref name=Zerner>{{cite book|last1=Zerner|first1=Henri|title=Renaissance Art in France: The Invention of Classicism|date=2003|publisher=Flammarion|location=Paris|isbn=9782080111449}}</ref> Its magisterial and arresting visual presence offered visual proof of Louviers’ stability, power, and wealth. Stylistically, the projecting porch and [[façade]] created a visual link between Louviers and [[Rouen]], home to very similar Late Gothic monuments and the seat of the powerful [[Roman Catholic Archdiocese of Rouen|archbishop]]. A veritable lacework of intricately carved stone, the completion of the façade and south porch in 1510 effectively reoriented the entire church. Subsequently, the south porch and [[Portal (architecture)|portal]] became the main point of entry and, ultimately, the focal point of the entire town. Indeed, upon entering Louviers from the south, one witnesses the impressive effect of the alignment of the porch and portal with the “Grand-Rue.”<ref name=Verdier /> The architecture draws viewers closer, overwhelming them with its visual complexity. Sinuous [[Flamboyant]] window [[tracery]], tall [[ogee]] gables, an openwork [[balustrade]], spiky [[pinnacle]]s, and countless [[Gothic art#sculpture|sculptures]] nestled in richly-carved nodding niches give the façade an unmatched quality of richness, visual complexity, and dynamism that was never replicated. Within decades of its completion, the great era of medieval [[French Gothic architecture|Gothic construction in France]] drew to a close.

==References==
{{reflist}}

==Sources==
*Bottineau-Fuchs, Yves. ''Haute-Normandie Gothique: Architecture Religieuse''.  Paris: Picard, 2001.
*Chaplain, Jean-Michel.  ''La chambre des tisseurs: Louviers, cité drapière, 1680-1840''.  Seyssel: Champ Vallon, 1984.
*Dibon, Paul. ''Essai historique sur Louviers''.  Rouen: Périaux, 1836.
*Hérold, Michel.  ''Louviers: église paroissale Notre-Dame: les verrières''.  Rouen: Connaissance du patrimoine de Haute-Normandie, 1995.
*Hérold, Michel; Florian Meunier; Hugo Miserey, eds. ''Louviers: Église Notre-Dame''. Louviers: HB Impressions, 2011.
*Le Mercier, Edmond. ''Monographie de l'église Notre-Dame de Louviers''.  Évreux: Impr. de Ch. Hérissey et fils, 1906.
*Murray, Stephen; Andrew Tallon; and Rory O'Neill. "Mapping Gothic France." http://mappinggothic.org/building/1145 (accessed March 11, 2015).	
*Régnier, L. "L'église Notre-Dame de Louviers," ''La Semaine religieuse'', March 1903, 3-13
*Sanfaçon, Roland.  ''L'architecture Flamboyante en France''.  Quebec: Les Presses de l'Université Laval, 1971.
*Verdier, François. "L'église Paroissiale Notre-Dame de Louviers," in ''Congrès Archéologique de France, 1980, Évrecin, Lieuvin, Pays d'ouche''. Paris: SFA, 1984.  pp.&nbsp;9–28.
*Zerner, Henri. ''Renaissance Art in France: The Invention of Classicism''. Paris: Flammarion, 2003.

{{DEFAULTSORT:Church of Notre-Dame de Louviers}}
[[Category:French architecture]]
[[Category:Gothic architecture in France]]
[[Category:Buildings and structures in Eure]]
[[Category:Churches in Eure]]
[[Category:Flamboyant Gothic]]