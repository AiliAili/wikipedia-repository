{{good article}}
{{Infobox monarch
| name               = Æthelwold 
| title              = King of the East Angles
| reign              =c. 654–664
| full name          =
| birth_date         = <!-- {{birth date|YYYY|MM|DD}} -->
| birth_place        =
| death_date         = 664
| death_place        =
| burial_date        =
| burial_place       = 
| predecessor        = [[Æthelhere of East Anglia]]
| successor          = [[Ealdwulf of East Anglia]]
| queen              = unknown
| spouse             =
| issue              = 
| royal house        = [[Wuffingas]]
| dynasty            =
| father             =  [[Eni of East Anglia]]
| mother             =
| religion           = 
}}

'''Æthelwold''', also known as '''Æthelwald''' or '''Æþelwald''' ([[Old English]]: ''Æþelwald'' "noble ruler"; reigned c. 654–664), was a 7th-century king of [[Kingdom of East Anglia|East Anglia]], the long-lived [[Anglo-Saxons|Anglo-Saxon]] kingdom which today includes the English counties of [[Norfolk]] and [[Suffolk]]. He was a member of the [[Wuffingas]] dynasty, which [[List of monarchs of East Anglia|ruled East Anglia]] from their ''regio'' (centre of royal authority) at [[Rendlesham]]. The two Anglo-Saxon cemeteries at [[Sutton Hoo]], the monastery at [[Iken]], the East Anglian [[Episcopal see|see]] at [[Dommoc]] and the emerging port of [[Ipswich]] were all in the vicinity of Rendlesham.

Æthelwold lived during a time of political and religious upheaval in East Anglia, whose Christian kings in the decades prior to his succession all died violent deaths, having proved unworthy of the task of defending the newly converted kingdom against attacks from its neighbouring kingdom, [[Mercia]], led by its pagan king, [[Penda of Mercia|Penda]].  Æthelwold was the last of the nephews of [[Rædwald of East Anglia|Rædwald]] to rule East Anglia. He died in 664 and was succeeded by [[Ealdwulf of East Anglia|Ealdwulf]], the son of his brother Æthelric.

Few records relating to East Anglia have survived and almost nothing is known of Æthelwold's life or reign.  He succeeded his elder brother [[Æthelhere of East Anglia|Æthelhere]], after Æthelhere was killed with Penda of Mercia at the [[Battle of the Winwaed|Battle of the Winwæd]] in about 655. During his rule he witnessed a setback in the aspirations of Mercia to dominate its neighbours, following the Battle of the Winwæd and the murder of Penda's son, [[Peada of Mercia|Peada]].

He was king during the last decade of the co-existence in England of the Christian [[Roman rite]], centred at [[Canterbury]], and the [[Celtic Christianity|Celtic rite]] based in [[Northumbria]].  At the [[Synod of Whitby]], in 664, the Roman cause prevailed and the division of ecclesiastical authorities ceased. In 662, [[Swithelm of Essex]] was persuaded to adopt Christianity and was [[baptised]] at Rendlesham, with Æthelwold present as his sponsor. East Anglia became more closely allied to Northumbria, [[Kingdom of Kent|Kent]] and lands in the [[Fens]] by means of royal marriages such as that between the Northumbrian [[Hereswitha]] and the East Anglian Æthilric.

==Historical context==
[[File:England green top.svg|250px|thumb|right|A map of the [[Anglo-Saxons|Anglo-Saxon kingdoms]], including places relevant to Æthelwold's reign]]

===The emergence of the Kingdom of the East Angles===
The history of [[Kingdom of East Anglia|East Anglia]] and its kings is known from the ''[[The Ecclesiastical History of the English People]]'', compiled by the [[Northumbrian]] monk [[Bede]] in 731, and a genealogical list from the ''[[Anglian collection]]'', dating from the 790s, in which the ancestry of [[Ælfwald of East Anglia]] was traced back through fourteen generations to [[Wōden]].<ref>Warner, ''The Origins of Suffolk'', p. 70.</ref>

East Anglia was a long-lived [[Anglo-Saxon]] kingdom in which a duality of a northern and a southern part existed, corresponding with the modern English counties of [[Norfolk]] and [[Suffolk]].<ref>Lapidge, ''Blackwell Encyclopaedia of Anglo-Saxon England'', p. 154.</ref> It was formed during the 5th century, following the ending of [[End of Roman rule in Britain|Roman power in Britain]] in 410.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', pp. 25–26.</ref> The east of Britain became settled at an early date by [[Saxons]] and [[Angles]] from the continent. During the 5th century, groups of settlers of mixed stock migrated into [[the Fens]] and up the major rivers inland.<ref>Warner, ''The Origins of Suffolk'', pp. 60–61.</ref> From Bede it is known that the people who settled in what became East Anglia were Angles, originally from what is now part of [[Denmark]]. By the 6th century, new settlements had also appeared along the river systems of the east coast of East Anglia, including the [[River Deben|Deben]], the [[River Alde|Alde]] and the [[River Orwell|Orwell]].<ref>Warner, ''The Origins of Suffolk'', p. 64.</ref> The settlers were unaffected by Roman urban civilisation and had their own [[Anglo-Saxon paganism|religion]] and language. As more of the region fell under their control, new kingdoms were formed, replacing the function of the Roman ''territoria''.<ref>Warner, ''The Origins of Suffolk'', pp. 66–67.</ref> Surrounded by sea, fenland, large defensive earthworks such as the [[Devil's Dyke, Cambridgeshire|Devil's Dyke]] and wide rivers, all of which acted to disconnect it from the rest of Britain, the land of the East Angles eventually became united by a single ruling dynasty, the Wuffingas.

===Rædwald and his successors===
The first king of the East Angles of whom more than a name is known was [[Rædwald of East Anglia|Rædwald]], described by Bede as 'the son of Tytil, whose father was Wuffa',<ref>Kirby, ''The Earliest English Kings'', p. 52.</ref> who reigned from about 599 until approximately 624. According to Bede, he was converted to [[Christianity]] at the court of his overlord [[Æthelberht of Kent]] in about 604. Later in his reign he was powerful enough to hold ''imperium'' over several [[Anglo-Saxon England|Anglo-Saxon kingdoms]]. In 616, he defeated [[Æthelfrith of Northumbria]] and installed the exiled [[Edwin of Northumbria|Edwin]] as the new king.<ref>Lapidge, ''The Blackwall Encyclopaedia of Anglo-Saxon England'', p. 385.</ref>  He is thought to have been given a [[ship burial]] and interred amongst a magnificent array of personal treasures and symbols of regal power that were discovered under Mound 1 at [[Sutton Hoo]], in Suffolk.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 82.</ref> His son [[Eorpwald of East Anglia|Eorpwald]] succeeded him and reigned briefly before he was killed soon after his baptism, by a heathen named [[Ricberht of East Anglia|Ricberht]], after which the East Angles reverted to paganism.<ref>Yorke, ''Kings and Kingdoms'', p. 62.</ref> Ricberht was replaced by [[Sigeberht of East Anglia|Sigeberht]], whose Christian education ensured that Christianity was reestablished. During Sigeberht's joint reign with [[Ecgric of East Anglia|Ecgric]], the East Anglian [[Episcopal see|see]] at [[Dommoc]] was established.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', pp. 100–101.</ref>

During 632 or 633, Edwin of Northumbria was overthrown and slain and his kingdom was ravaged by [[Cadwallon ap Cadfan]], supported by [[Penda of Mercia]].<ref>Stenton, ''Anglo-Saxon England'', pp. 80–81</ref> The Mercians then turned on the East Angles and their king, Ecgric. In 640 or 641, they routed the East Anglian army in a battle in which Ecgric and his predecessor [[Sigeberht of East Anglia|Sigeberht]] both perished.<ref name="W110-13">Warner, ''The Origins of Suffolk'', pp. 110–13. See Kirby, ''The Early English Kings'', p. 207 for a discussion of the problematic dating of the battle in which Ecgric was killed.</ref>

Ecgric's successor, Æthelwold's brother [[Anna of East Anglia|Anna]], who was renowned for his devout Christianity and the saintliness of his children, proved ineffective in preventing East Anglia from being invaded by the Mercians. Following a Mercian attack in 651 on the monastery at [[Cnobheresburg]], Anna was exiled by Penda, possibly to the kingdom of the [[Magonsæte]]. After his return, East Anglia was attacked again by Penda, Anna's forces were defeated and he was killed. During the reign of his successor, [[Æthelhere of East Anglia|Æthelhere]] (another brother of Æthelwold), East Anglia was eclipsed by Mercia. In 655, after the Battle of the Winwæd, near [[Leeds]], in which Æthelhere was slain fighting beside Penda,<ref name="Y63">Yorke, ''Kings and Kingdoms'', p. 63.</ref> a new political situation arose. Penda's son [[Peada of Mercia|Peada]], who had ruled the [[Mercia]]n province of the [[Middle Angles]] as a Christian king from 653,<ref name="Y63"/> now succeeded Penda as king of Mercia, but he was murdered a year later.<ref>Stenton, ''Anglo-Saxon England'', p. 84.</ref> Peada's death dealt a severe blow to Mercian aspirations of dominion over the other kingdoms of England.

=== The sphere of Rendlesham ===
The royal seat of Rendlesham, specified by Bede,<ref>Yorke, ''Kings and Kingdoms'', pp. 65–66.</ref> seals the evident importance of the Deben estuary headwaters as a centre of royal power, demonstrated for an earlier period by the royal cemetery of [[Sutton Hoo]].<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', pp. 75–76.</ref> Rendlesham, a short distance from [[Iken]], the site of Botolph's monastery, stands at a strategic point between the rivers [[River Deben|Deben]] and [[River Alde|Alde]] at the headwaters of the [[River Butley|Butley]] estuary, which intersects the peninsula between the two major rivers. The dedication of Rendlesham's church to [[Pope Gregory I|St Gregory]] suggests its early, perhaps primary connection with the royal dwelling mentioned by Bede. If the [[Dommoc]] bishopric was at [[Walton, Suffolk|Walton]], as [[Rochester, Kent|Rochester]] claimed in the thirteenth century, then this was also immediately within the sphere of Rendlesham. Archaeologists have revealed that the quay of Gipeswic (now modern [[Ipswich]]), at a ford of the [[River Orwell]] estuary, was then growing in importance as a centre of seaborne trade to the continent, under direct royal patronage.<ref>Yorke, ''Kings and Kingdoms'', p. 69.</ref>

==Descent, family and accession==
Æthelwold, ([[Old English]] 'noble ruler')<ref>Bosworth, ''A dictionary of the Anglo-Saxon language'', [https://books.google.com/books?id=WRkAAAAAYAAJ&lpg=PA286&ots=F27Sijc9yr&dq=wald%20ruler%20raed%20counsel&pg=PA12#v=onepage&q=ethel&f=false pp. 12, 438.]</ref> was a member of the Wuffingas dynasty, the youngest son of [[Eni of East Anglia|Eni]] and a nephew of Rædwald of East Anglia. Two of his brothers, Anna and Æthelhere, ruled in succession before him.<ref>Yorke, ''Kings and Kingdoms'', p. 68.</ref>

His accession is mentioned by the 12th-century historian [[William of Malmesbury]], in ''[[Gesta Regum Anglorum]]'': 
:"''To Anna succeeded his brother Ethelhere, who was justly slain by Oswy king of the Northumbrians, together with Penda, because he was an auxiliary to him, and was actually supporting his brother and his kinsman. His brother Ethelwald, in due succession, left the kingdom to Adulf and Elwold, the sons of Ethelhere''."<ref>William of Malmesbury, ''Gesta regum Anglorum'' Book 1, chp. 5, p. 89.</ref><ref group=quote>''"Successit Annæ frater ejus Ethelhere, occisusque est a rege Northanhimbrorum Oswio cum Penda merito, quod ei concurreret in auxilium, et fulciret exercitum qui pessum dedisset fratrem et cognatum. Hujus successor frater Ethelwaldus continuatis successionibus regnum reliquit ejusdem Ethelherii filiis Aldnlfo."'' ([https://archive.org/stream/willelmimalmesb00unkngoog#page/n169/mode/2up William of Malmesbury, Book 1, §97])</ref></blockquote>

Dynastic alliances bound Æthelwold's kingdom strongly to the Christian [[kingdom of Kent]], where [[Seaxburh of Ely|Seaxburh]], the eldest daughter of Æthelwold's elder brother Anna, was [[Eorcenberht of Kent]]'s queen.<ref name="Yorke, p. 37">Yorke, ''Kings and Kingdoms'', p. 37.</ref> East Anglia's western stronghold in the [[The Fens|Fens]] was held by Seaxburh's sister [[Æthelthryth]] and, like Kent, it was devoutly attached to the Roman Church. There was also an important Northumbrian connection: in 657, [[Hilda of Whitby|Hilda]] established the monastery of Streoneshalh (identified with [[Whitby]]), which later became the burial-place of Edwin and other Northumbrian kings. Hilda's sister [[Hereswitha]] married Æthelwold's youngest brother Æthelric in around 627–629.<ref>Yorke, ''Kings and Kingdoms'', p. 66.</ref>

== Reign ==

=== Christianity in East Anglia under Æthelwold ===
[[File:Williamson p16 3.svg|thumb|250px|right|The kingdom of East Anglia during the early Saxon period]]
The influence of the [[Celtic rite]] in East Anglia had been strong whilst the monastery of [[Saint Fursey]] and [[Foillan|Saint Foillan]] at [[Burgh Castle|Cnobheresburg]] had existed. The authority of East Anglian Christianity still resided in the East Anglian [[Episcopal see|see]] at Dommoc, obedient to [[Province of Canterbury|Canterbury]]. [[Saint Botolph]] began to build his monastery at [[Iken]], on a tidal island site in the [[River Alde]], [[Suffolk]], in about 653, the year that [[Anna of East Anglia]] was killed at the Battle of Bulcamp.<ref>Plunkett, ''Suffolk in Anglo-Saxon Times'', p. 116.</ref> 
<!--  If the legend that Botolph came to East Anglia through contact with the daughters of Anna at [[Faremoutiers Abbey]] is true, he may have received Celtic teaching there, since the abbess [[Burgundofara]] was reputedly instructed by [[Columbanus|Saint Columbanus]], the Irish missionary to [[Burgundy (region)|Burgundy]]. Anna's daughter [[Æthelburh of Faremoutiers]] succeeded Burgundofara as abbess at Faremoutiers at about this time. -->

Oswiu successfully persuaded [[Sigeberht the Good|Sigeberht II of the East Saxons]] to receive baptism<ref>Stenton, ''Anglo-Saxon England'', p. 121.</ref> and [[Cedd]], a Northumbrian disciple of Aidan's, was diverted from the Northumbrian mission to the Middle Angles under Peada to become Bishop of the East Saxons and re-convert the people. Cedd built monasteries at [[East Tilbury|Tilbury]] in the south and at [[Othona|Ythancæster]], where there was an old Roman fort, at what is now [[Bradwell-on-Sea]], in north-east [[Essex]].<ref>Yorke, ''Kings and Kingdoms'', p. 48.</ref> Sigebert was assassinated by his own thegns and was succeeded by the pagan [[Swithelm of Essex]]. Cedd persuaded him to accept the faith and, according to Bede, his baptism by Cedd took place at [[Rendlesham]], in the presence of King Æthelwold:<ref name="Y63"/>

<blockquote>''"Sigebert was succeeded in the kingdom by Suidhelm, the son of Sexbald, who was baptized by the same Cedd, in the province of the East Angles, at the king's countryseat, called Rendelsham, that is, Rendil's Mansion; and Ethelwald, king of the East Angles, brother to Anna, king of the same people, was his godfather."''<ref>Bede, ''[[Ecclesiastical History of the English People]]'', [[:s:Ecclesiastical History of the English People/Book 3#22|iii, p. 22]].</ref><ref group=quote>''"Successit autem Sigbercto inregnum Suidhelm, filius Sexbaldi, qui baptizatus est ab ipso Cedde in prouincia Orientalium Anglorum, in uico regio, qui dicitur Rendlasham, id est mansio Rendili; suscepitque eum ascendentem de fonto sancto Aediluald rex ipsius gentis Orientalium Anglorum, frater Anna regis eorundem."'' ([http://www.thelatinlibrary.com/bede/bede3.shtml#22 Bede, iii, 22])</ref></blockquote>

=== East Anglian marriage alliances ===
In the early 660s, two important marriages took place. [[Ecgfrith of Northumbria]], the fifteen-year-old son of Oswiu, married Æthelthryth of [[Ely, Cambridgeshire|Ely]], the daughter of Anna of East Anglia, (who was about fourteen years older than him), and moved to live with him at his Northumbrian court. She had remained a virgin for Christ during her first marriage: she continued in this resolve as Ecgfrityh's bride, with the result that he could not expect to father an heir. Æthelthryth retained Ely as her own possession during this marriage.<ref>Yorke, ''Kings and Kingdoms'', pp. 66, 81, 111.</ref>

Meanwhile, [[Wulfhere of Mercia]], a brother of Peada, emerged from safe retreat and was proclaimed king. He was not Christian, but was soon converted and subsequently married Eormenhilda, daughter of [[Eorcenberht of Kent]] and Seaxburh.<ref name="Yorke, p. 37"/> Soon afterwards he founded the monastery of [[Medeshamstede]], which later became known as [[Peterborough]], under abbot Seaxwulf.

== Synod of Whitby ==
{{main|Synod of Whitby}}
Following the death of [[Finan of Lindisfarne|Finan]], bishop of Lindisfarne, [[Alhfrith of Deira]], in collusion with [[Wilfrid|Wilfred of York]], [[Agilbert]] of Wessex and others, were determined to persuade Oswiu to rule in favour of the Roman rite of Christianity within the kingdoms over which he had ''imperium''. The case was debated in Oswiu's presence at the [[Synod of Whitby]] in 664, with [[Colmán of Lindisfarne|Colmán]], [[Hilda of Whitby|Hild]] and [[Cedd]] defending the Celtic rite and the tradition inherited from [[Aidan of Lindisfarne|Aidan]], and Wilifred speaking for the Roman position.<ref>Stenton, ''Anglo-Saxon England'', p. 123.</ref> The Roman cause prevailed and the former division of ecclesiastical authorities was set aside. Those who could not accept it, including Colmán, departed elsewhere.<ref>Yorke, ''Kings and Kingdoms'', p. 80.</ref>

At that time [[Plague (disease)|plague]] swept through Europe and [[Anglo-Saxon England]]. Amongst its victims was Bishop Cedd, Archbishop [[Deusdedit of Canterbury]], and Eorconbehrt of Kent. Æthelwold also died in 664.

==Quotations==
{{Reflist|group=quote}}

==Notes==
{{Reflist|3}}

==Bibliography==
{{refbegin|2}}
*{{Cite Bede HE}}
*{{Cite book
  | last = Plunkett
  | first = Steven
  | title = Suffolk in Anglo-Saxon Times
  | publisher = Tempus
  | location = Stroud
  | year = 2005
  | isbn = 0-7524-3139-0 }}
*{{Cite book
  | last = Bosworth
  | first = Joseph
  | authorlink = Joseph Bosworth
  | title = A Dictionary of the Anglo-Saxon Language
  | publisher =[[Longman]]
  | location = London
  | year = 1838
  | oclc = 465899961}}
*{{Citation
  | last1 = West
  | first1 = S.E.
  | last2 = Scarfe
  | first2 = N.
  | last3 = Camp
  | first3 = R.J.
  | year = 1984
  | title = Iken, St Botolph, and the Coming of East Anglian Christianity
  | publisher = Proc. Suffolk Institute of Archaeology
  | volume = 35
  | issue = Part 4
  | pages = 279–301}}
*{{Cite book
  | last = Kirby
  | first = D. P.
  | title = The Earliest English Kings
  | publisher = Routledge
  | location = London and New York
  | year = 2000
  | isbn = 0-4152-4211-8
}}
*{{Cite book
  | last = Lapidge
  | first = Michael
  | title = The Blackwall Encyclopaedia of Anglo-Saxon England
  | publisher = Blackwell
  | location = Oxford
  | year = 2001
  | isbn = 978-0-631-22492-1}}
*{{Cite book
  | last = Plunkett
  | first = Steven
  | title = Suffolk in Anglo-Saxon Times
  | publisher = Tempus
  | location = Stroud
  | year = 2005
  | isbn = 0-7524-3139-0 }}
*{{Cite book
  | last = Stenton
  | first = Sir Frank
  | authorlink = Frank Stenton
  | title = Anglo-Saxon England
  | publisher = Oxford University Press
  | location = New York
  | year = 1988
  | isbn = 0-19-821716-1}}
*{{Cite book
  | last = Warner
  | first = Peter
  | title = The origins of Suffolk
  | publisher = Manchester University Press
  | location = Manchester and New York
  | year = 1996
  | isbn = 0-7190-3817-0}}
*{{Cite journal
  | author      = Whitelock, D.
  | year        = 1972
  | title       = The Pre-Viking Age Church in East Anglia
  | journal      = Anglo-Saxon England
  | volume       = 1
  | pages        = 1–22
  | location     = Cambridge
  | publisher    = Cambridge University Press
  | doi=10.1017/s0263675100000053}}
*{{Cite book
  | author =  William of Malmesbury 
  | authorlink = William of Malmesbury 
  | editor-last = Giles
  | editor-first = J. A.
  | editor-link = John Allen Giles
  | title = William of Malmesbury's ‘Chronicle of the Kings of England from the Earliest Period to the Reign of King Stephen’
  | url = https://archive.org/stream/williammalmesbu00gilegoog#page/n10/mode/2up
  | origyear  = 12th century
  | publisher = Bohn
  | location = London
  | year = 1867}}
*{{Cite book
  | author =  William of Malmesbury 
  | authorlink = William of Malmesbury 
  | title = Willelmi Malmesbiriensis Monachi Gesta Regum Anglorum
  | volume = 1
  | url = https://archive.org/details/willelmimalmesb00unkngoog
  | origyear  = 12th century
  | publisher = Sumptibus Societatis
  | location = London
  | language  = Latin
  | year = 1840
  | pages = 133–137}}
*{{Cite book
  | last = Yorke
  | first = Barbara
  | authorlink = Barbara Yorke
  | title = Kings and Kingdoms of Early Anglo-Saxon England
  | publisher = Routledge
  | location = London and New York
  | year = 2002
  | isbn = 0-415-16639-X}}
{{refend}}

==External links==
* {{PASE|1337|Æthelwald 5}}

{{S-start}}
{{S-roy|en}}
{{Succession box|
 before= [[Æthelhere of East Anglia|Æthelhere]]
 |
 title= [[Kings of East Anglia|King of East Anglia]]
 |
 years= 654–664
 |
 after= [[Ealdwulf of East Anglia|Ealdwulf]]
 |
}}
{{S-end}}

{{Kings of East Anglia}}

{{DEFAULTSORT:Aethelwold Of East Anglia}}
[[Category:664 deaths]]
[[Category:East Anglian monarchs]]
[[Category:7th-century English monarchs]]
[[Category:Year of birth uncertain]]
[[Category:House of Wuffingas]]