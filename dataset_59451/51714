{{Infobox journal
| title = The Journal of African History
| cover = [[File:The Journal of African History.jpg]]
| discipline = [[History of Africa]]
| abbreviation = J. Afr. Hist.
| editor = Barbara M. Cooper, Richard Reid, Cheikh A. Babou, Lynn M. Thomas
| publisher = [[Cambridge University Press]]
| country =
| history = 1960-present
| frequency = Triannually
| impact = 0.722 (ISI, History)
| impact-year = 2014
| website = http://journals.cambridge.org/jid_AFH
| link1 = http://journals.cambridge.org/action/displayIssue?jid=AFH&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=AFH
| link2-name = Online archive
| ISSN = 0021-8537
| eISSN = 1469-5138
| JSTOR =
| OCLC = 473118490
| LCCN = 63005723
}}
'''''The Journal of African History''''' is a triannual [[Peer review|peer-reviewed]] [[academic journal]] covering [[African history]] from the late Stone Age to the present. It was established in 1960 and is published by [[Cambridge University Press]]. The current [[editors-in-chief]] are Barbara M. Cooper ([[Rutgers University]]), Richard Reid ([[School of Oriental and African Studies]]), Cheikh A. Babou ([[University of Pennsylvania]]), and Lynn M. Thomas ([[University of Washington]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[British Humanities Index]]
* [[GEOBASE (database)|Geographical Abstracts]]
* [[Current Contents]]
* [[Social Sciences Citation Index]]
* [[Arts and Humanities Citation Index]]
* [[Scopus]]
* [[International Bibliography of Periodical Literature]]
* [[International Bibliography of Book Reviews of Scholarly Literature]]
* [[International Bibliography of the Social Sciences]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 0.588.<ref name=WoS>{{cite book |year=2013 |chapter=The Journal of African History |title=2012 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.cambridge.org/jid_AFH}}

{{DEFAULTSORT:Journal Of African History, The}}

[[Category:African history journals]]
[[Category:Cambridge University Press academic journals]]
[[Category:English-language journals]]
[[Category:Triannual journals]]
[[Category:Publications established in 1960]]