{{Multiple issues|
{{fanpov|date=October 2014}}
{{Orphan|date=March 2014}}
}}

[[File:Yuan Gong artist.jpg|thumb|Yuan Gong with his artwork "Perhaps I understand nothing", 2013.]]

'''Yuan Gong''' ({{zh|c=原弓|p=Yuán Gōng}}, born 1961 in Shanghai), is a Chinese contemporary artist. He has obtained a PhD degree in theory of art at the Chinese National Academy of Arts in 2012, but has immersed himself in different aspects of [[Chinese contemporary art]] since the 1990s. Being both creator and researcher, designer and planner, Yuan Gong is a multiple facets artist playing at the interface of [[conceptual art]], [[performance]] and [[fine arts]]. Using a large variety of media, his artworks address many philosophical questions, reflecting his concerns about the Chinese society and impugning the art system, the exhibition format and the status of the artwork.

== Life and works ==

Yuan Gong graduated from the Shanghai Printing Technical School with a specialization in artistic platemaking. He started his career as a designer before pursuing it as an artist. He held his first solo exhibition at the [[Hong Kong Arts Centre]] in 1997 and has participated in more than 20 group exhibitions around the world.

He has hold solo exhibitions in main art spaces in China such as the Guangdong Museum of Art ([[Guangzhou]]), the Tianrenheyi Art Center ([[Hangzhou]]),<ref>Pi Daojian, How History Moves Towards Current Time, in Catalogue, Reproduction-Renascence, Tian Ren He Yi Art Ltd, Chinese/ English, Hangzhou, China, 2013.</ref> the Zendai Contemporary Art Space ([[Shanghai]]), Bund18 Ailing Foundation ([[Shanghai]]).

Major exhibitions in which he has participated in recent years include the Collateral event "Voice of the Unseen" of the 55th [[Venice Biennale]], the thematic exhibition at the [[China Pavilion]] of the 54th [[Venice Biennale]],<ref>Catalogue, Chinese Pavilion 54th Venice Biennial Yuan Gong's work SCENE, Ed. Arts Tracking Publishing Center, Editor Maoming Rong, (Chinese/ English), Beijing, China, 2011.</ref><ref>Catalogue, The Scented air 6000 m3 - Much more than 6000 m3, Research Center for Aesthetics and Aesthetic Education at Peking University (Chinese/ English), Beijing, China, 2011.</ref> the Copenhagen Art Festival, the 6th Prague Contemporary Art Festival, the inaugural [[Beijing 798]] Biennale, the 4th China Songzhuang Culture and Art Festival and the 4th Chengdu Biennale.

Within the "3+X" exhibition pattern, promoted by Shaheen Merali,<ref>Shaheen Merali, Shifts in the imaginary: tracing initiatives by the 3+1 Group, in Catalogue, People and things around contemporary art, Research Center for Aesthetics and Aesthetic Education at Peking University, Chinese/ English, Beijing, China, 2011.</ref> Jin Weihong, [[Ai Weiwei]] and [[Shen Qibin]], Yuan Gong has exhibited with artists such as [[Jin Feng]], [[Qiu Zhijie]], [[Xu Zhen (artist)|Xu Zhen]], [[Zuoxiao Zuzhou]], Xi Jianjun, the [[Gao Brothers]] and Zhao Zhao. Within these exhibitions the artists present their work in different spaces, emphasizing the independence of the creation process, of the artworks and of the artists themselves.
[[File:YG2.jpg|thumb|Sounding off 5.12 with Dong Feng (left) Red Curtains 5.12 (right), 2008-2009. Installation.]]
In 2006 he launched the ''New Power China Biennale'' as a curator in order to promote young emerging Chinese artists. In 2007, he initiated the Tibet series Art Project "Transparent Scene" and organized artist residencies in [[Tibet]] for several months. After the [[2008 Tibetan unrest]] (also known as"3.14 riots") he created ''China Size'', an installation-performance using a 3.14 scale, shown at the 4th Songzhuang Art Festival.

The same year, following the [[Sichuan earthquake]], he was volunteered researcher for one year in the disaster area, from which he produced some of his most iconic and controversial artworks, ''Sounding off 5.12 with Dong Feng'' and ''Red Curtains 5.12''. During this year, he conducted a meticulous survey gathering a rich amount of testimonies from which produced photographs, videos and installations made using materials collected in the disaster area.<ref>Catalogue, Process - Memoir of Yuan Gong, Ed. Chinese Arts and Humanities Press, Ma Cheng & Hu Yan	(Chinese/ English), Hong Kong, China, 2010.</ref><ref>Catalogue, People and things around contemporary art, Research Center for Aesthetics and Aesthetic Education at Peking University, Chinese/ English, Beijing, China, 2011.</ref>
[[File:YG5.jpg|thumb|The Scented Air 6000m3, 2011. Installation, fog & Tibetan incense.China Pavilion, Venice Biennale.]]

In May 2009, after his stay at the Department of Archaeology of [[Peking University]], he was involved as an artist in the archaeological excavation of [[Duke of Zhou]] temple in [[Shaanxi province]]
. Once again breaking rules of conventional thinking, he collected the soil dug from the excavations and launched the ''Duke of Zhou Soil Collection Plan''. 
[[File:YG4.jpg|thumb|left|Duke of Zhou Soil Collection Plan, 2010. Photography.Soil excavated from Duke of Zhou temple archaeological site.]]
Since 2011 and his artwork ''the Scented Air 6000 m<sup>3</sup>'' presented at the [[China Pavilion]] of the 54th [[Venice Biennale]],<ref>Peng Feng, In-Between Existence, published in La Biennale di Venezia: Biennale Arte 2011: ILLUMInations, Ed. Bice Curiger & Giovanni Carmine, published by Marsilio Editori (Venice - Italy), 2011, 604 p.</ref> Yuan Gong uses fog media to pervade the exhibition space or to interfere with people.<ref>Wang Lin, The Importance of the Site-On Yuan Gong’s More than 6000m³, artintern.net, July 2011.</ref><ref>Harvey Dzodin, ‘Pervasion’ Invasion, Global Times, June 2011.</ref> In his new productions, he has developed a variety of devices and different mechanisms to produce fog, invading public spaces and disrupting the established order (''The Stroll, 2012; Air Strikes around the world, 2013'').

In 2007, Yuan Gong was awarded the ''Culture China Person of the Year Award''. He received the ICS City Beat EDGE ''Artist of the Year Award'' in 2009. The same year, his work ''Sounding off 5.12 with Dong Feng'' was included in the book "60 Years of New China Art Interview 1949-2009", edited by the Chinese art theorist Shao Dazhen.<ref>60 Years of New China Art Interview 1949-2009, Ed. Shao Da Zhen, China, 2009.</ref> His artworks have been collected by the Guangdong Museum of Art, the Hubei Museum of Art, the Arthur Sackler Museum of Peking University, and are part of the Sigg’s collection of [[Chinese contemporary art]].

== Exhibitions ==

;2014
*''China ARTE Brasil''. Oca Pavilhão Lucas Nogueira Garcez, São Paulo, Brazil
*''WE-The Contemporary Painting Exhibition of 8 artists''. Shanghai, China
*''The [[Armory Show]] focus : China''. Tianrenheyi Art Center, Pier 94, New York, United States

;2013
[[File:YG8.JPG|thumb|right|Auspicious Clouds, 2013. Installation.Mechanism exhibition, Guangdong Museum of Art.]]
[[File:YG9 Mechanism.JPG|thumb|right|Auspicious Clouds, 2013. Installation. Mechanism exhibition, Guangdong Museum of Art.]]
*''A New Kind of Individual - Release''. Zendai Contemporary Art Space, Shanghai, China
*''Mechanism''. Guangdong Museum of Art, Guangzhou, China
*''Butterflies fly once more''. Bund18 Ailing Foundation, Shanghai, China
*''It's also good without a title''. Embassy of the Grand Duchy of Luxembourg, Beijing, China<
*''Reappearance''. Tianrenheyi Art Center, Hangzhou, China
*''Individual Growth - The Momentum of Contemporary Art''. Tianjin Museum of Fine Arts, Tianjin, China
*''Chinese Contemporary Art Documenta – Contemporary Art and Process of the Society''. Circle Art Center, Shenzhen, China
*''Voice of the Unseen – Collateral event of the 55th Venice Biennale''. Venice, Italy
*Embark! Beyond The Horizon''. Hong Kong Visual Arts Centre – Oil Street Art Space, Hong Kong, China<
*Truth, Beauty, Freedom and Money''. Chi K11 Art Space, Shanghai, China
[[File:YG7Air Strikes Venice.JPG|thumb|Air Strikes around the World, 2013. Venice Biennale.]]
[[File:YG6.JPG|thumb|Air Strikes around the World, 2013.Oriental Pearl Tower Shanghai.]]

;2012
*''RE-INK Invitational Exhibition of Contemporary Ink and Wash Painting 2010-2012''. Today Art Museum, Beijing, China & Hubei Museum of Art, Wuhan, China<
*''Copenhagen Art Festival''. Kunstrad Museum, Copenhagen, Denmark
*''JETLAG - Chinese Culture Year in Germany''. Kunsthalle Faust, Hannover, Germany
*''The 4th Guangzhou Triennial''. Guangdong Museum of Art, Guangzhou, China

;2011
*''The 4th Sino-European Cultural Dialogue''. Neumünster Abbey, Luxembourg City, Luxembourg
*''The 6th Prague Contemporary Art Festival– TINA B''. Prague, Czech Republic
*''Pervasion - 54th Venice Biennale''. The China Pavilion, Venice, Italy
*''Unexpected issues''. Art Channel, Beijing, China.

;2010
*''3+X People And Things Around''. China Art Archives and Warehouse CAAW, Beijing, China
*''The 6th Culture and Art Festival of Songzhuang - CROSSOVER 2010''. China Art Archives and Warehouse CAAW, Beijing, China<
*''3+X Can Be More Poetic''. Shandong Center of Contemporary Art, Shandong, China
*''3+1 Monolog''. Arthur M. Sackler Museum Of Art and Archaeology, Peking University, Beijing, China

;2009
*''Drifting Communities, 798 Biennale''. 798 Art District, Beijing, China
*''Narrating China, 4th Chengdu Biennale''. Chengdu, China
*''REFLECTION– New Art Invitational''. West Lake Museum of Art, Zhejiang, China & Wall Art Museum, Beijing, China

;2008
*''Through the Wild, 4th Songzhuang Art Festival''. China
*''What is Art ? 798 Art Festival''. [[798 Art District]], Beijing, China
*''Virtual City, New Power – China Contemporary Art Biennale'', Yuan Gong Art Museum, Shanghai, China

;2007
*''Transparent Scene''. [[Tibet Museum (Lhasa)|Tibet Museum]], Lhasa, China

;2006
*''New Power''. Shanghai, China

;2000
*''14th Asia International Watercolor Exhibition''. Zhengzhou Museum, Henan, China
*''1990s The Popular Image''. Shanghai Art Fair, Shanghai, China

;1997
*''Yuan Gong Solo Exhibition''. [[Hong Kong Arts Centre]], Hong Kong, China

== References ==
{{reflist}}

== External links ==
* http://www.yuangong-art.com/
* http://www.artfacts.net/en/artist/yuan-gong-300789/profile.html
* http://www.aaa.org.hk/Home/Search?keyword=YUAN%20Gong(%E5%8E%9F%E5%BC%93)
* http://www.artlinkart.com/en/artist/overview/685hwut

{{DEFAULTSORT:Gong, Yuan}}
[[Category:Living people]]
[[Category:1961 births]]
[[Category:Chinese contemporary artists]]
[[Category:Chinese performance artists]]
[[Category:Artists from Shanghai]]