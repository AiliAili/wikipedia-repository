{{Use dmy dates|date=October 2014}}
{{Use American English|date=December 2014}}
{{Infobox military person
|name=Walter Krupinski
|birth_date={{birth date|1920|11|11|df=y}}
|death_date={{death date and age|2000|10|7|1920|11|11|df=y}}
|image=Generalleutnant Walter Krupinski.jpg
|birth_place=Domnau, East Prussia
|death_place=[[Neunkirchen-Seelscheid]]
|caption=Walter Krupinski
|nickname=Graf Punski, The Count
|allegiance={{flag|Nazi Germany}} (to 1945)<br />{{flag|West Germany}}
|branch={{Luftwaffe}}<br/>{{GAF}}
|serviceyears=1940–45<br />1957–76
|rank=''[[Hauptmann]]'' (Wehrmacht)<br />''[[Generalleutnant]]'' (Bundeswehr)
|commands=[[JG 52]] and [[JG 5]]<br />[[Jagdbombergeschwader 33|JaBoG 33]]
|unit=[[JG 52]], [[JG 5]] and [[JV 44]]
|battles=[[World War II]]
|awards=[[Knight's Cross of the Iron Cross with Oak Leaves]]<br />''[[Bundesverdienstkreuz]]''
}}

'''Walter Krupinski''' (11 November 1920 – 7 October 2000) was a German ''[[Luftwaffe]]'' [[fighter ace]] in [[World War II]] and a senior West [[German Air Force]] officer during the [[Cold War]]. He was one of the highest-scoring pilots, credited with 197 victories in 1,100 [[Sortie|sorties]]. He was called by his fellow pilots ''' Graf Punski ''' ([[Count]] Punski) due to his [[Prussian]] origins. Krupinski was one of the first to fly the [[Messerschmitt Me 262]] jet fighter in combat as a member of [[Jagdverband 44|''Jagdverband'' 44]] led by [[Adolf Galland]].

==Childhood, education and early career==
Krupinski was born on 11 November 1920, in the town of [[Domnovo|Domnau]] in the [[Province of East Prussia]], and grew up in Braunsberg, present-day [[Braniewo]], Poland. He was the first son of Friedrich Wilhelm Krupinski, a ''Obergerichtsvollzieher'' ([[bailiff]]), and his wife Auguste, née Helmke. His two younger brothers were Paul and Günther.{{sfn|Braatz|2010|pp=13–14}} Paul joined the ''[[Kriegsmarine]]'' and entered the [[U-boat]] service, and was killed in action on 11 November 1944 while serving on {{GS|U-771||2}} as an ''[[Oberleutnant zur See]]'' (first lieutenant), which was sunk off the Norwegian coast by the British submarine {{HMS|Venturer|P68|6}}.{{sfn|Braatz|2010|p=152}}

Krupinski entered the ''Luftwaffe'' in September 1939 as an ensign. From November 1939 to October 1940, Krupinski entered basic air training and, after being assigned as a fighter pilot, the fighter school.<ref group="Note">Flight training in the ''Luftwaffe'' progressed through the levels A1, A2 and B1, B2, referred to as A/B flight training. A training included theoretical and practical training in aerobatics, navigation, long-distance flights and dead-stick landings. The B courses included high-altitude flights, instrument flights, night landings and training to handle the aircraft in difficult situations.</ref> Following two weeks of vacation, Krupinski completed his training at [[Jagdfliegerschule|''Jagdfliegerschule 5'']] (5th fighter pilot school) in [[Vienna|Wien-Schwechat]] to which he was posted on 1 July 1940. ''Jagdfliegerschule 5'' at the time was under the command of the [[World War I]] flying ace and recipient of the [[Pour le Mérite]] [[Eduard Ritter von Schleich]]. One of his course mates was [[Hans-Joachim Marseille]], who had been posted to the ''Jagdfliegerschule 5'' in late 1939 but had not yet graduated out of disciplinary reasons.{{sfn|Braatz|2010|p=28}} His three-roommates at the school were [[Walter Nowotny]], [[Paul Galland]], the brother of [[Adolf Galland]], and Peter Göring, a nephew of the ''[[Reichsmarschall]]'' (Empire Marshal) [[Hermann Göring]].{{sfn|Braatz|2010|p=29}}

==World War II==
After completing his flight training at ''[[Jagdfliegerschule]]'' 5 Krupinski was sent to [[Ergänzungs-Jagdgruppe Merseburg|''Ergänzungsjagdgruppe'' Merseburg]] on 1 October 1940. He then joined his new unit [[Jagdgeschwader 52|''Jagdgeschwader'' 52]] (JG 52—52nd Fighter Wing), where he was placed in 6. ''[[Luftwaffe Organization#Staffel|Staffel]]'' in February 1941.<ref group="Note">For an explanation of ''Luftwaffe'' unit designations see [[Organization of the Luftwaffe (1933–1945)|Organization of the ''Luftwaffe'' during World War II]].</ref> 6. ''Staffel'' at the time was under the command of ''[[Staffelkapitän]]'' (squadron leader) [[Rudolf Resch]]. Resch later gave Krupinski the [[nickname]] "''Graf Punski''" ("Count Punski") or sometimes just "''Der Graf''" ("The Count"). The nickname had its origins in a late-night conversation between Krupinski and Resch. His father was a professor of Slavic studies in [[Dresden]]. When Krupinski tried to explain his East Prussian origin, Resch informed him that the ending in "-ski" or "-zky" denoted a landowner, or that it indicated a ''[[Freiherr]]'' ("free lord"), and thus the lowest level in the medieval nobal hierarchy in the East. The witty banter which then followed, led at first in his squadron, then in his group and eventually in the entire German fighter force to his nickname which stuck with for the rest of his life.{{sfn|Braatz|2010|pp=14–15}} 

[[File:Bundesarchiv Bild 146-2004-0010, Ukraine, Günther Rall mit Kameraden.jpg|thumb|left|[[Günther Rall]] after his 200th aerial victory. Walter Krupinski (second from right) standing to his left.]]

===Eastern Front===
Krupinski won his first aerial victory during [[Operation Barbarossa]]. On 16 August 1941 at 05:48, he claimed a [[Polikarpov I-16]].{{sfn|Prien|2000|p=43}} Krupinski followed this up with a [[Ilyushin DB-3]] on 17 September 1941.{{sfn|Prien|2000|p=45}} His fourth victory came on the 4 October over a [[Tupolev SB]] near [[Kholm, Kholmsky District, Novgorod Oblast|Kholm]].{{sfn|Prien|2000|p=46}} 

In the summer, 1942, 6./''JG'' 52 was supporting [[Army Group South]] on the front over the [[Caucasus]]. Opposing it was the 4th and 5th Air Armies of the [[Red Air Force]].{{sfn|Bergström|Pegg|2003|p=364}} On 25 October 1942 Krupinski claimed his 53rd victory but was then rammed by an I-16. The Soviet pilot was killed. Four days later Krupinski received the [[Knight's Cross of the Iron Cross]].{{sfn|Bergström|Pegg|2003|p=364}}

By August 1942 at 50, for which he was awarded the [[German Cross]] in Gold ({{lang|de|''Deutsches Kreuz in Gold''}}). 

After another six victories Krupinski was awarded the [[Knight's Cross of the Iron Cross]] ({{lang|de|''Ritterkreuz des Eisernes Kreuzes''}}).{{sfn|Obermaier|1989|p=243}} On 2 March 1943, Krupinski was promoted to ''[[Staffelkapitän]]'' (squadron leader) and was given command of 7. ''Staffel''.{{sfn|Weal|2001|p=67}} At this time [[Erich Hartmann]], who went on to become the highest scoring Ace of the war, served as his wingman. Hartmann adopted Krupinki's close-quarters method of attack. On 18 August 1943, Krupinski was credited with his 100th aerial victory. He was the 51st ''Luftwaffe'' pilot to achieve the century mark.{{sfn|Obermaier|1989|p=243}}

===Oak Leaves to the Knight's Cross===
Krupinski was awarded the [[Knight's Cross of the Iron Cross with Oak Leaves]] ({{lang|de|''Ritterkreuz des Eisernes Kreuzes mit Eichenlaub''}}) for his 174th victory.

Both Krupinski and Hartmann were ordered to the ''[[Ministry of Aviation (Nazi Germany)|Reichsluftfahrtministerium]]'' (Ministry of Aviation) in Berlin for the Oak Leaves presentation. Both arrived in Berlin on 23 March 1944 only to learn that the presentation would be made at the ''[[Führer Headquarters|Führerhauptquartier]]'' (Führer Headquarter). They were instructed to go the [[Anhalter Bahnhof]] where they would take an overnight train to the ''Führerhauptquartier''. Here they met fellow JG&nbsp;52 pilots [[Gerhard Barkhorn]], who was to receive the Swords to his Knight's Cross, and [[Johannes Wiese]]. Also present were [[Kurt Bühligen]], [[Horst Ademeit]], [[Reinhard Seiler]], [[Hans-Joachim Jabs]], Dr. [[Maximilian Otte]], [[Bernhard Jope]] and [[Hansgeorg Bätcher]] from the bomber force, and the Flak officer [[Fritz Petersen]], all destined to receive the Oak Leaves. Krupinski assumed that they were heading for the [[Wolf's Lair]] in East Prussia but the train was heading for the [[Berghof (Hitler)|''Berghof'']] in [[Berchtesgaden]].{{sfn|Braatz|2010|p=118}} On the train, all of them got drunk on cognac and champagne. Supporting each other and unable to stand, they arrived at Berchtesgaden. Major [[Nicolaus von Below]], Hitler's Luftwaffe adjutant, was shocked. After some sobering up, they were still intoxicated. Hartmann took a German officer's hat from a stand and put it on, but it was too large. Von Below became upset, told Hartmann it was Hitler's and ordered him to put it back.{{sfn|Braatz|2010|p=119}}

===Defense of the Reich===
After achieving 177 victories, Krupinski was transferred from the [[Eastern Front (World War II)|Eastern Front]] to Germany, where he was assigned to 1. ''Staffel'' of [[Jagdgeschwader 5|''Jagdgeschwader'' 5]]  (JG 5—5th Fighter Wing). Promoted to the rank of ''[[Hauptmann]]'' (captain) in May 1944, Krupinski was made commander of II. ''[[Luftwaffe Organization#Gruppe|Gruppe]]'' of [[Jagdgeschwader 11|''Jagdgeschwader'' 11]]  (JG 11—11th Fighter Wing). After the Allied invasion of France in June 1944, the ''Gruppe'' was rushed to Normandy to operate on low-level Army support missions. Krupinski claimed 10 Allied aircraft shot down before he was wounded and burned on 12 August.
By September he was transferred as Commanding Officer of III. ''Gruppe'', [[Jagdgeschwader 26|''Jagdgeschwader'' 26]]  (JG 26—26th Fighter Wing). In March 1945, Krupinski was transferred to the aces unit [[Jagdverband 44|''Jagdverband'' 44]], which flew the [[Messerschmitt 262]] jet, claiming his last two aerial victories of the war on 16 and 26 April 1945.

At 3:00 pm on 24 April 1945, Krupinski was one of four pilots to take off from [[Munich-Riem airport|Munich-Riem]] to intercept a [[United States Army Air Forces]] (USAAF) [[B-26 Marauder]] aircraft formation. [[Günther Lützow]], who failed to return from this mission, led the flight of four. Lützow's fate remains unknown to this date. One of the other two pilots was Leutnant [[Klaus Neumann]].{{sfn|Braatz|2005|p=365}}

After having claimed 197 enemy planes (177 Eastern Front, 20 against the Western Allies, in about 1100 missions), Krupinski went into American captivity on 5 May 1945. He was held in US custody at Salzburg, Aibling, Heilbronn, Heidelberg, in England, France, Munich-Oberföhring and Tegernsee before being released on 26 September 1945.
Krupinski had bailed out four times and had been wounded five times.

==Gehlen Organization==
The former General [[Reinhard Gehlen]] had offered his services to the Americans in the end of 1945. Gehlen had served as chief of [[Foreign Armies East|''Fremde Heere Ost'' (FHO)]], the German Army's [[military intelligence]] unit on the [[Eastern Front (World War II)|Eastern Front]]. The [[Gehlen Organization]] was in need for people who were familiar with the air war. Krupinski was hired and helped gather information about the armed forces in the [[Soviet occupation zone]] until 1953. There are many conflicting or missing bits of information about this stage of Krupinski's life. He had done little to lift this veil of uncertainty.{{sfn|Braatz|2010|pp=177–181}}

==Bundeswehr==
Krupinski entered the ''Amt Blank'' (Blank Agency), named after [[Theodor Blank]], the forerunner of the German [[Federal Ministry of Defence (Germany)|Federal Ministry of Defense]] on 15 December 1952. Given the rank of major in 1957, Krupinski went to lead [[Jagdbombergeschwader 33|''Jagdbombergeschwader'' 33]] (JaBoG 33—Fighter-Bomber Wing 33) the first postwar German jet fighter wing. In 1966 Krupinski took command of the German forces of the ''Luftwaffen-Ausbildungs-Kommando'' in [[Fort Bliss]], [[Texas]] with the rank of brigadier general. In July 1969 Krupinski became commander of the 3rd Luftwaffe division. In 1971 he became chief of staff of [[Second Allied Tactical Air Force]]. In October 1974 Krupinski was promoted commanding officer of the airfleet. Due to the ''[[Rudel Scandal]]'' he was forced into early retirement on 8 November 1976 holding the rank of ''[[Generalleutnant]]'' (lieutenant-general). Krupinski died in [[Neunkirchen-Seelscheid]] in 2000.

==Awards==
* [[Wound Badge]] in Gold
* [[Ehrenpokal der Luftwaffe]] on 13 September 1942 as ''[[Leutnant]]'' and pilot{{sfn|Patzwall|2008|p=127}}{{refn|According to Obermaier in May 1942.{{sfn|Obermaier|1989|p=61}}|group="Note"}}
* [[German Cross]] in Gold on 27 August 1942 as ''Leutnant'' in the 6./''Jagdgeschwader'' 52{{sfn|Patzwall|Scherzer|2001|p=258}}
* [[Iron Cross]] (1939)
** 2nd Class (13 May 1940){{sfn|Thomas|1997|p=418}}
** 1st Class (18 September 1941){{sfn|Thomas|1997|p=418}}
* [[Knight's Cross of the Iron Cross with Oak Leaves]]
** Knight's Cross on 29 October 1942 as ''Leutnant'' and pilot in the 6./''Jagdgeschwader'' 52{{sfn|Scherzer|2007|p=479}}{{sfn|Fellgiebel|2000|p=276}}
** 415th Oak Leaves on 2 March 1944 as ''[[Oberleutnant]]'' and ''[[Staffelkapitän]]'' of the 7./''Jagdgeschwader'' 52{{sfn|Scherzer|2007|p=479}}{{sfn|Fellgiebel|2000|p=79}}
* [[Bundesverdienstkreuz]]<ref>''Helden der Wehrmacht II'', p. 121</ref>

==Notes==
{{reflist|group="Note"}}

==References==

===Citations===
{{reflist|25em}}

===Bibliography===
{{refbegin}}
* {{Cite book
  |last=Braatz
  |first=Kurt
  |year=2005
  |title=Gott oder ein Flugzeug - Leben und Sterben des Jagdfliegers Günther Lützow
  |trans_title=God or an Airplane - Life and Death of Fighter Pilot Günther Lützow
  |language=German
  |location=Moosburg, Germany
  |publisher=NeunundzwanzigSechs Verlag
  |isbn=978-3-9807935-6-8
  |ref=harv
}}
* {{Cite book
  |last=Braatz
  |first=Kurt
  |year=2010
  |title=Walter Krupinski - Jagdflieger, Geheimagent, General
  |trans_title=Walter Krupinski - Fighter Pilot, Spy, General
  |language=German
  |location=Moosburg, Germany
  |publisher=NeunundzwanzigSechs Verlag
  |isbn=978-3-9811615-5-7
  |ref=harv
}}
* {{Cite book
  |last1=Bergström
  |first1=Christer
  |last2=Mikhailov
  |first2=Andrey
  |authorlink=
  |year=2001
  |title=Black Cross, Red Star Vol 2
  |location=
  |publisher=Pacifica Military History
  |isbn=978-0-935553-51-2
  |ref=harv
}}
* {{Cite book
  |last1=Bergström
  |first1=Christer
  |last2=Antipov
  |first2=Vlad
  |last3=Sundin
  |first3=Claes
  |year=2003
  |title=Graf & Grislawski – A Pair of Aces
  |location=Hamilton MT
  |publisher=Eagle Editions
  |isbn=978-0-9721060-4-7
  |ref=harv
}}
* {{Cite book
  |last1=Bergström
  |first1=Christer
  |last2=Pegg
  |first2=Martin
  |year=2003
  |title=Jagdwaffe: The War in Russia: January–October 1942
  |location=London
  |publisher=Classic Colours
  |isbn=978-1-903223-23-9
  |ref=harv
}}
* {{Cite book
  |last=Bergström
  |first=Christer
  |authorlink=
  |year=2007
  |title=Barbarossa - The Air Battle: July–December 1941
  |location=London
  |publisher=Chervron/Ian Allen
  |isbn=978-1-85780-270-2
  |ref=harv
}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
  |ref=harv
}}
* {{Cite book
  |last=Forsyth
  |first=Robert
  |year=2008
  |title=Jagdverband 44 Squadron of Experten
  |location=Oxford, UK
  |publisher=[[Osprey Publishing]]
  |isbn=978-1-84603-294-3
}}
* {{Cite book
  |last=Obermaier
  |first=Ernst
  |year=1989
  |title=Die Ritterkreuzträger der Luftwaffe Jagdflieger 1939 – 1945
  |trans_title=The Knight's Cross Bearers of the Luftwaffe Fighter Force 1939 – 1945
  |language=German
  |location=Mainz, Germany
  |publisher=Verlag Dieter Hoffmann
  |isbn=978-3-87341-065-7
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |last2=Scherzer
  |first2=Veit
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
  |ref=harv
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D.
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
  |ref=harv
}}
* {{Cite book
  |last=Prien
  |first=Jochen
  |year=2000
  |title=Die Jagdfliegerverbände der deutschen Luftwaffe 1934 bis 1945: Teil 6 Teilband I: Unternehmen BARBAROSSA Einsatz im Osten - 22.06. bis 05.12.1941
  |trans_title=Fighter Pilot Association of the German Luftwaffe 1934 to 1945 Part 6 Volume 2: Operation Barbarossa phase in the East - 22 June to 5 December 1941
  |language=German
  |location=
  |publisher=Struve-Druck
  |isbn=978-3-923457-68-7
  |ref=harv
}}
* {{Cite book
  |last=Scherzer
  |first=Veit
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
  |ref=harv
}}
* {{Cite book
  |last=Von Seemen
  |first=Gerhard
  |year=1976
  |title=Die Ritterkreuzträger 1939–1945 : die Ritterkreuzträger sämtlicher Wehrmachtteile, Brillanten-, Schwerter- und Eichenlaubträger in der Reihenfolge der Verleihung : Anhang mit Verleihungsbestimmungen und weiteren Angaben
  |trans_title=The Knight's Cross Bearers 1939–1945 : The Knight's Cross Bearers of All the Armed Services, Diamonds, Swords and Oak Leaves Bearers in the Order of Presentation: Appendix with Further Information and Presentation Requirements
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Verlag
  |isbn=978-3-7909-0051-4
}}
* {{Cite book
  |last=Thomas
  |first=Franz
  |year=1997
  |title=Die Eichenlaubträger 1939–1945 Band 1: A–K
  |trans_title=The Oak Leaves Bearers 1939–1945 Volume 1: A–K
  |language=German
  |location=Osnabrück, Germany
  |publisher=Biblio-Verlag
  |isbn=978-3-7648-2299-6
  |ref=harv
}}
* Weal, John (1999). ''Bf 109F/G/K Aces of the Western Front''. Oxford, UK: Osprey Publishing. ISBN 1-85532-905-0.
{{refend}}

==External links==
*{{DNB portal|143083570|TYP=}}
*{{Cite web|work=Aces of the Luftwaffe |title=Walter Krupinski |author=Petr Kacha |url=http://www.luftwaffe.cz/krupinski.html |accessdate=14 June 2013}}
*{{Cite web|work=Historynet.com |title=World War II: Interview with Luftwaffe Ace Walter Krupinski |author=Colin D. Heaton |url=http://www.historynet.com/air_sea/aces/3026136.html |accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 29|year=1962 |title=Walter Krupinski |url=http://www.spiegel.de/spiegel/print/d-45140972.html| language=German|accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 36|year=1966 |title=Datum: 29. August 1966 Betr: Einwände |url=http://www.spiegel.de/spiegel/print/d-46413990.html| language=German|accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 37|year=1966 |title=Walter Krupinski |url=http://www.spiegel.de/spiegel/print/d-46414164.html| language=German|accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 37|year=1966 |title=Bei uns ist alles in die Brüche gegangen Spiegel-Gespräch mit Brigadegeneral Walter Krupinski |url=http://www.spiegel.de/spiegel/print/d-46414163.html| language=German|accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 47|year=1976 |title=Datum: 15. November 1976 Betr.: Krupinski |url=http://www.spiegel.de/spiegel/print/d-41069437.html| language=German|accessdate=14 June 2013}}
*{{Cite news|journal=Der Spiegel |volume= 52|year=1976 |title=Liv Ullmann, Walter Krupinski, Walter F. Mondale, Eleanor Jane, William Hall, Günter Guillaume, Charles M. Schulz |url=http://www.spiegel.de/spiegel/print/d-41119121.html| language=German|accessdate=14 June 2013}}
*{{YouTube|id=jFsS6MrgWOM|title=Me262 Ace Walter "Graf" Krupinski Interview}}
* [http://www.iwm.org.uk/collections/item/object/80011143 Imperial War Museum Interview]

{{s-start}}
{{s-mil}}
{{succession box
| before=none
| after=Oberst [[Georg Wroblewski]]
| title=Commander of [[Jagdbombergeschwader 33]]
| years=1 October 1958 – 31 December 1962
}}
{{succession box
| before = Generalmajor [[Günter Proll]]
| after  = Generalmajor [[Gerhard Limberg]]
| title  = Commander of [[3rd Luftwaffe Division (Bundeswehr)|3. Luftwaffendivision (Bundeswehr)]]
| years  = July 1969 – 30 September 1972
}}
{{s-end}}

{{Top German World War II Aces}}
{{Authority control}}
{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Krupinski, Walter}}
[[Category:1920 births]]
[[Category:2000 deaths]]
[[Category:Luftwaffe pilots]]
[[Category:People from East Prussia]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross with Oak Leaves]]
[[Category:World War II prisoners of war held by the United States]]
[[Category:Bundeswehr generals]]
[[Category:Recipients of the Order of Merit of the Federal Republic of Germany]]
[[Category:German Air Force pilots]]
[[Category:Lieutenant generals of the German Air Force]]