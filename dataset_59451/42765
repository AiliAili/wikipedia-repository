{{Use dmy dates|date=August 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = T-46 "Eaglet"
  |image = File:Fairchild T-46-4.jpg
  |caption = A T-46 out of Edwards AFB
}}{{Infobox Aircraft Type
  |type = Jet [[trainer aircraft]]
  |manufacturer = [[Fairchild Aircraft]]
  |designer =
  |first flight = 15 October 1985
  |introduction =
  |retired =
  |number built = 3
  |status =
  |unit cost =
  |variants with their own articles =
  |primary user =[[United States Air Force]]
  |more users = 
}}
|}

The '''Fairchild T-46''' (nicknamed the '''"Eaglet"''') was an American light jet [[trainer aircraft]] of the 1980s. It was cancelled in 1986 with only three aircraft being produced.

==Design and development==
The [[United States Air Force]] (USAF) launched its [[Next Generation Trainer]] (NGT) program to replace the [[Cessna T-37 Tweet]] primary trainer in 1981.<ref name="Bray p274">Braybrook 1985, p. 274.</ref> Fairchild-Republic submitted a shoulder-winged [[monoplane]] with a twin tail, powered by two [[Garrett F109]] [[turbofan]]s and with pilot and instructor sitting side by side.<ref name="Bray p275">Braybrook 1985, p. 275.</ref>

In order to validate the proposed aircraft's design, and to explore its flight handling characteristics, Fairchild Republic contracted with Ames Industries of [[Bohemia, New York]] to build a flyable 62% scale version. [[Burt Rutan]]'s [[Rutan Aircraft Factory]] (RAF) in [[Mojave, California]] was contracted to perform the flight test evaluations, with test pilot [[Dick Rutan]] doing the flying. The scale version was known at RAF as the '''Model 73 NGT''', this flying on 10 September 1981.<ref name="Bray p275-6">Braybrook 1985, pp. 275–276.</ref>

Fairchild's design, to be designated T-46, was announced winner of the NGT competition on 2 July 1982,<ref name="Bray p275"/> with the USAF placing an order for two prototypes and options for 54 production aircraft.<ref>''Flight International'' 17 July 1982, p. 122.</ref> It was planned to build 650 T-46s for the USAF by 1991.<ref name="morm p650">Mormillo 1986, p. 650.</ref>

The aircraft first flew on 15 October 1985,<ref>''Flight International'' 26 October 1985, p. 8.</ref> six months later than originally programmed date of 15 April. Costs had increased significantly during the development process, with the predicted unit cost rising from $1.5&nbsp;million in 1982 to $3&nbsp;million in February 1985.<ref name="morm p650"/> The 1985 [[Gramm–Rudman–Hollings Balanced Budget Act]] mandated spending cuts for the US Government in an attempt to limit the national debt,<ref name="morm p650"/> and while testing did not reveal any major problems,<ref name="morm p651-2">Mormillo 1986, pp. 651–652.</ref> [[United States Secretary of the Air Force|Secretary of the Air Force]] [[Russell A. Rourke]] cancelled procurement of the T-46, while allowing limited development to continue.<ref name="morm p650"/> While attempts were made in Congress to reinstate the program, which resulted in the FY 1987 budget being delayed, an amendment was passed to the 1987 Appropriations Bill to forbid any spending on the T-46 until further evaluation of the T-46 against the T-37 and other trainers took place.<ref>''Flight International'' 8 November 1986, p. 9.</ref>

The project was cancelled a little more than a year later, for reasons that largely remain controversial.{{citation needed|date=January 2013}} The T-46 was the last project of the Fairchild Republic Corporation, and after the program termination Fairchild had no more income. Without any new contracts and the NGT program cancelled, the company closed the Republic factory in [[Farmingdale, New York]], bringing 60 years of Fairchild aircraft manufacturing to an end.

[[File:Fairchild T-46-2.jpg|thumb|right|Two T-46 aircraft circling [[Edwards AFB]]]]

The aircraft itself featured a side-by-side configuration, a twin (or "H") tail (similar to the company's [[A-10 Thunderbolt II|A-10]]), [[ejection seat]]s, pressurization, and two [[turbofan]] engines.  Had it gone into full production the NGT program called for 650 aircraft being built up to 1992. There was potential for some overseas sales as well, such as in the light ground attack role in addition to its role as a trainer.<ref name="Warwick p29">Warwick 1985, p. 29.</ref>

==Operators==
;{{USA}}
*[[United States Air Force]]

==Aircraft on display==
All three prototypes have been preserved:
* ''84-0492'' can be seen at the [[Air Force Flight Test Center Museum]] at [[Edwards Air Force Base]], California.
* ''84-0493'' is under restoration at the [[National Museum of the United States Air Force]].<ref>[http://www.nationalmuseum.af.mil/factsheets/factsheet_print.asp?fsID=10910&page=2 Fact Sheet: FAIRCHILD REPUBLIC T-46A"]. ''National Museum of the USAF''. Retrieved 15 May 2011.</ref>
* ''85-1596'' can be seen at the [[AMARG]] "Celebrity Row" during the AMARG bus tour from the [[Pima Air Museum]], [[Arizona]]
* The Model 73 NGT Flight Demonstrator can be seen at the [[Cradle of Aviation Museum]], New York

==Specifications (T-46) (performance estimated)==
[[File:T-46, X-32 and YF-23 in the restoration area of the National Museum of the USAF.jpg|thumb|right|T-46, [[X-32]] and [[YF-23]] in the restoration area of the [[National Museum of the United States Air Force]]]]
{{Aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] --> 
|plane or copter?=<!-- options: plane/copter --> plane
|jet or prop?=<!-- options: jet/prop/both/neither --> jet
|ref=Tweety-Bird Replacement<ref name="Bray p276">Braybrook 1985, p. 276.</ref>
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]]. --> 
|crew= two; trainer, student
|capacity=
|length main= 29 ft 6 in
|length alt= 8.99 m
|span main= 38 ft 7¾ in 
|span alt= 11.78 m
|height main= 9 ft 11¾ in
|height alt= 3.04 m
|area main= 160.9 ft²
|area alt= 14.95 m²
|airfoil=
|empty weight main= 5,275 lb
|empty weight alt= 2,393 kg
|loaded weight main= <!--lb--> 
|loaded weight alt= <!--kg--> 
|useful load main= <!--lb--> 
|useful load alt= <!--kg--> 
|max takeoff weight main= 6,962 lb
|max takeoff weight alt= 3,158 kg
|aspect ratio=9.28
|more general=  '''Internal fuel capacity:''' 200 US Gal (757 L)
|engine (jet)= [[Garrett F109-GA-100]]
|type of jet= [[turbofan]] engines
|number of jets=2
|thrust main= 1,330 lbf
|thrust alt= 5.93 kN 
|thrust original=
|max speed main= 397 knots 
|max speed alt= 457 mph, 735&nbsp;km/h
|max speed more=at 25,000 ft (7,600 m)
|cruise speed main= 333 knots 
|cruise speed alt= 383 mph, 616&nbsp;km/h
|cruise speed more=at 45,000 ft (13,700 m)
|never exceed speed main= <!--knots--> 
|never exceed speed alt= <!--mph,km/h--> 
|stall speed main= <!--knots--> 
|stall speed alt= <!--mph,km/h--> 
|range main= 1,190 nm
|range alt= 1,369 mi, 2,205&nbsp;km
|ceiling main= 46,500 ft
|ceiling alt= 14,175 m
|climb rate main= 4,470 ft/min
|climb rate alt= 22.7 m/s
|loading main= 
|loading alt= 
|thrust/weight= 
|more performance=
* '''Take-off distance:''' 1,520 ft (463 m) (to 50 ft (15 m))
|armament=
|avionics=
}}

==See also==

{{Aircontent
|related=

|similar aircraft=
* [[T-37 Tweet]]
* [[T-6 Texan II]]
|sequence=
|lists=
|see also=
*[[Joint Primary Aircraft Training System|JPATS]]
}}

==References==
{{reflist}}

* Braybrook, Roy. "Tweety-Bird Replacement". ''[[Air International]]'', June 1985, Vol 28, No. 6. pp.&nbsp;273–280.
* [http://www.flightglobal.com/pdfarchive/view/1982/1982%20-%201866.html "Fairchild wins NGT"]. ''[[Flight International]]'', 17 July 1982, p.&nbsp;122.
* Mormillo, Frank B. "T-46A: The Trainer of the Future?" ''Aircraft Illustrated'', December 1986, Vol. 19, No. 12. pp.&nbsp;648–653.
* [http://www.flightglobal.com/pdfarchive/view/1985/1985%20-%202906.html "T-46A is flown"]. ''Flight International'' 26 October 1985, p.&nbsp;8.
* [http://www.flightglobal.com/pdfarchive/view/1986/1986%20-%202951.html "USAF trainer contest opened to all"] ''Flight International'', 8 November 1986, p.&nbsp;9.
* Warwick, Graham [http://www.flightglobal.com/pdfarchive/view/1985/1985%20-%201118.html "T-46: A Class Apart"]. ''Flight International'', 13 April 1985, pp.&nbsp;24–29.

==External links==
{{commons}}
* [http://www.globalsecurity.org/military/systems/aircraft/t-46.htm Global Security Article on the T-46]
* [http://www.cradleofaviation.org/exhibits/jet/republic_t-46/t-46.html Includes photo of RAF Model 73 flown by [[Dick Rutan]]]

{{Fairchild aircraft}}
{{USAF trainer aircraft}}

[[Category:Fairchild aircraft|T-46]]
[[Category:United States military trainer aircraft 1980–1989]]