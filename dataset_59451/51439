{{Infobox journal
| title = Hastings Center Report
| cover = [[File:Hcrcoversmall.gif]]
| editor = [[Gregory Kaebnick]]
| discipline = [[Bioethics]], [[philosophy]], [[ethics]], [[humanities]], [[health policy]], [[health law]], [[religious studies]]
| abbreviation = Hastings Cent. Rep.
| publisher = [[Wiley-Blackwell]]
| country = United States
| frequency = Bimonthly
| history = 1971-present
| openaccess = 
| license = 
| impact = 1.731
| impact-year = 2015
| website = http://www.thehastingscenter.org/Publications/HCR/Default.aspx
| link1 = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1552-146X
| link1-name = Online access
| link2 = 
| link2-name =
| JSTOR = 00930334
| OCLC = 15622366
| LCCN = 2004-212569
| CODEN = 
| ISSN = 0093-0334
| eISSN = 1552-146X
}}
The '''''Hastings Center Report''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] of [[bioethics]]. It is published by [[Wiley-Blackwell]] on behalf of the [[Hastings Center]] ([[Garrison, New York]]). The [[editor-in-chief]] is Gregory Kaebnick. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.731, ranking it 8th out of 51 journals in the category "Ethics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Ethics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

The journal focuses on legal, moral, and social issues in [[medicine]] and the [[life sciences]].<ref>[http://www.thehastingscenter.org/Publications/HCR/About.aspx About the Hastings Center Report – www.thehastingscenter.org]</ref> It publishes a variety of article types that may take many forms:<ref>[http://www.thehastingscenter.org/Publications/HCR/Guidelines.aspx Submission Guidelines – www.thehastingscenter.org]</ref>
* articles that explore philosophical and ethical issues in medicine, health care, technology, medical research, the use of human subjects in research, and the environment
* reports or reviews of empirical studies that implicate relevant philosophical and ethical questions
* short, provocative essays; case studies (which may be accompanied by commentary on the case)
* personal narratives about receiving or providing health care
* and brief commentary on relevant events in the news

== See also ==
* [[List of ethics journals]]

==References==
{{reflist}}

==External links==
* {{Official website|http://www.thehastingscenter.org/Publications/HCR/Default.aspx}}

[[Category:Bioethics journals]]
[[Category:Publications established in 1971]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]
[[Category:English-language journals]]