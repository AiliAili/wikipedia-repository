<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=DP.VII
 | image=Dietrich-Gobiet DP.VII.png
 | caption=
}}{{Infobox Aircraft Type
 | type=Light sports aircraft
 | national origin=[[Germany]]
 | manufacturer=Dietrich-Gobiet Flugzeugwerk
 | designer=
 | first flight=Spring 1924
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Dietrich-Gobiet DP.VII''' was a simple, low power, [[Germany|German]] sports aircraft flown in early 1924.

==Design and development==

The low-powered DP.VII was a simple, easily transportable, [[low wing|low-wing]] [[monoplane]] intended to make sports aviation more widely accessible.  It had a simple, thick section wing, essentially rectangular in plan apart from blunted, angled tips.  This had two main wooden box [[spar (aviation)|spar]]s and was braced to the upper [[fuselage]] on each side with an inverted V-form pair of [[strut]]s from the upper fuselage [[longeron]]s to the spars at about one-third span. Unusually, the one-piece wing structure passed through the deep fuselage above the lower longerons and could be extracted in a few minutes then transported away  on a pair of [[trestle]]s normally stowed inside the DP.VII. The aircraft had long-span [[ailerons]] filling about two-thirds of the wing.<ref name=Flight1/>

The simple, flat-sided fuselage was a steel-tube structure with four longerons, linked by welded struts, defining the shape. Internal [[piano wire]] bracing stiffened the fuselage, which was [[aircraft fabric covering|fabric-covered]].  The open [[cockpit]] was over the rear wing and was large enough for a passenger to sit behind the pilot, straddling a box seat. Though the power was low, the structure was light and the load/empty weight ratio (0.89) was noted as high. At the time, the installation of the {{convert|30|hp|kW|abbr=on|disp=flip|0}} air-cooled [[Haacke HFM-2]] [[flat-twin|engine]] was seen as particularly clean, with only the upper cylinders projecting out of the [[aircraft fairing#Engine cowling|cowling]].<ref name=Flight1/>

The DP.VII's mainwheels were mounted on a single axle, conventionally rubber sprung to a cross member attached to the lower fuselage longerons by a V-form pair of struts on each side. Less conventionally, the undercarriage structure was braced by another, transverse, V-strut from the cross piece centre to the longerons. There was a sprung tail skid at the rear.<ref name=Flight1/>

The exact date of the DP.VII's first flight is not certain, but the prototype had been well tested by late May 1924 and there were plans to put it into quantity production.<ref name=Flight1/> At almost the same time, Dietrich-Gobiet were developing the DP.VIIA which, despite the similar designation, was a larger-span, [[parasol]]-wing monoplane.  It had a similar fuselage and empennage to the DP.VII but a much more powerful engine, a [[Siemens-Halske Sh 4]] five-cylinder [[radial engine|radial]] producing about {{convert|55|hp|kW|abbr=on|disp=flip|0}}.  This aircraft was on display at the Third International Aero Show at [[Prague]] in early June 1924.<ref name=Flight2/> Few of either type seem to have been produced.<ref name=GY/>

==Variants==
;DP.VII: Low-wing monoplane, powered by a {{convert|22|kW|hp|abbr=on}} [[Haacke HFM 2]] flat-twin engine.<ref name=Flight1/> 
;DP.VIIA: Parasol-wing monoplane, powered by a {{convert|43|kW|hp|abbr=on}}  [[Siemens-Halske Sh 4]] radial engine, with 20% greater span and 50% heavier loaded weight.<ref name=Flight1/><ref name=Flight2/> Based on the [[Stahlwerk Mark R.III]].{{citation needed|date=September 2014}}

==Specifications (DP.VII)==
{{Aircraft specs
|ref=''Flight, 22 May 1924 p.287.''<ref name=Flight1/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|capacity=one passenger
|length m=5.4
|length note=
|span m=8.0
|span note=
|height m=1.95
|height note=
|wing area sqm=10.63
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=180
|empty weight note=
|gross weight kg=340
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Haacke HFM-2]]
|eng1 type=[[flat-twin engine]], air-cooled
|eng1 kw=<!-- prop engines -->
|eng1 hp=30
|eng1 note=
|power original=30-35 hp

|more power=

|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=115
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=350
|endurance=<!-- if range unknown -->
|ceiling m=2500
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

==References==
{{reflist|refs=

<ref name=Flight1>{{cite magazine |last= |first= |authorlink= |coauthors= |date=22 May 1924 |title= The Dietrich-Gobiet Sport Monoplane|magazine= [[Flight International|Flight]]|volume=XVI |issue=21 |pages=286–7  |url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200286.html  }}</ref>

<ref name=Flight2>{{cite magazine |last= |first= |authorlink= |coauthors= |date=12 June 1924 |title= The Aeroshow at Prague - Dietrich-Gobiet Flugzeugwerk, Cassel|magazine= [[Flight International|Flight]]|volume=XVI |issue=24 |page=385  |url= http://www.flightglobal.com/pdfarchive/view/1924/1924%20-%200385.html  }}</ref>

<ref name=GY>{{cite web |url=http://www.airhistory.org.uk/gy/reg_D-.htmll|title=Reconstructed 1919–1935 German Civil Register |author= |date= |work= |publisher= |accessdate=6 September 2014}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

<!-- Navboxes go here -->

[[Category:German sport aircraft 1920–1929]]