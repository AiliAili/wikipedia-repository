{{Italic title}}
The '''''Accounting, Auditing & Accountability Journal''''' is a [[peer-reviewed]] [[academic journal]] on [[accountancy|accounting]] theory and practice, published by [[Emerald Group Publishing]]. 

==History==
The journal was established in 1988 as part of the Interdisciplinary and Critical Perspectives on Accounting Project at the [[University of Sheffield]], established by [[Ernest Anthony Lowe|Tony Lowe]].<ref>The Sheffield School, [https://www.shef.ac.uk/management/thesheffieldschool Tony Lowe obituary]; www.shef.ac.uk. Retrieved 2015-04-22. {{fails verification|date=November 2015}}</ref> It is described as "an accounting and management research journal that publishes papers on the interaction between accounting and auditing on the one hand and their institutional, socio-economic, political and historical environment on the other."<ref name=interview/> The [[editors-in-chief]] are [[Lee Parker]] ([[RMIT University]], Australia) and [[James Guthrie (academic)|James Guthrie]] ([[Macquarie University]], Australia),<ref name=edteam>Emerald Group Publishing, [http://www.emeraldgrouppublishing.com/products/journals/editorial_team.htm?id=aaaj Editorial Team - ''Accounting, Auditing & Accountability Journal'']. Retrieved 2015-04-22.</ref> the co-founding editors of the journal.<ref name=interview>Emerald Group Publishing, [http://www.emeraldgrouppublishing.com/authors/interviews/aaaj.htm Meet the editors of...''Accounting, Auditing & Accountability Journal'']. Interview with co-editors, 2004; revised 2009. Retrieved 2015-04-23.</ref>

The journal also publishes poetry and short prose from accounting and management academics,<ref name=interview/> reviewed by a literary editor, for example, a work by [[Ruth D. Hines|Ruth Hines]].

The journal sponsors The Asia-Pacific Interdisciplinary Research Conference in Accounting, which is held every three years.<ref name=interview/>

==References==
<references/>

==External links==
* {{Official website|http://www.emeraldinsight.com/info/journals/aaaj/aaaj.jsp}}
* {{ISSN|0951-3574}}

{{DEFAULTSORT:Accounting, Auditing and Accountability Journal}}
[[Category:Accounting journals]]
[[Category:Publications established in 1988]]
[[Category:English-language journals]]
[[Category:Emerald Group Publishing academic journals]]