{{Infobox scientist
| name              = Arthur John Rowledge
| native_name       = 
| native_name_lang  = 
| image             = ArthurRowledge.JPG
| image_size        = 
| alt               = 
| caption           = 
| birth_date        =  {{Birth date|df=y|1876|7|30}}
|birth_place          = [[Peterborough]], England
| death_date        = {{Death date and age|df=y|1957|12|11|1876|7|30}} 
|death_place          = [[Derby]], England
| resting_place     = 
| resting_place_coordinates = <!-- 
                      {{Coord|LAT|LONG|type:landmark|display=inline,title}} -->
| other_names       = 
| residence         = 
| citizenship       = 
|nationality          = [[United Kingdom|British]]
| fields            = 
| workplaces        = {{Plainlist|
* [[Napier & Son]]
*[[Rolls-Royce Limited]]
}}
| alma_mater        = 
| thesis_title      = 
| thesis_url        = 
| thesis_year       = 
| doctoral_advisor  = 
| academic_advisors = 
| doctoral_students = 
| notable_students  = 
| known_for         ={{Plainlist|
* [[Rolls-Royce Merlin]]
*[[Aircraft engine|Aero engines]]
}}
| author_abbrev_bot = 
| author_abbrev_zoo = 
| influences        = 
| influenced        = 
| awards            = {{Plainlist|
*[[Order of the British Empire|MBE]]
*[[Fellow of the Royal Society]]<ref name="frs"/>
}}
| signature         = <!--(filename only)-->
| signature_alt     = 
| website           = <!-- {{URL|www.example.com}} -->
| footnotes         = 
| spouse            = 
| children          = 
}}

'''Arthur John Rowledge''' [[Order of the British Empire|MBE]], [[Royal Society|FRS]]<ref name="frs">{{Cite journal | last1 = Rubbra | first1 = A. A. | authorlink1 = Arthur Rubbra| doi = 10.1098/rsbm.1958.0019 | title = Arthur John Rowledge 1876-1957 | journal = [[Biographical Memoirs of Fellows of the Royal Society]] | volume = 4 | pages = 215 | year = 1958 | pmid =  | pmc = }}</ref> (30 July 1876 – 11 December 1957) was an [[English people|English]] [[engineer]] who designed the [[Napier Lion]] [[Aircraft engine|aero engine]] and was a key figure in the development of the [[Rolls-Royce Merlin]].<ref>Lumsden 2003, p. 14.</ref>

==Career==
Rowledge was born in [[Peterborough]], [[Northamptonshire]] in 1876, the son of John and Ann Rowledge. His father was described as a [[bricklayer]] in 1881.<ref name="Census1881" /> At the time of the 1891 [[Census in the United Kingdom|census]], Rowledge was described as an ''Apprentice to Engineers Draughtsman''<ref name="Census1891" /> and in 1901 he was in [[Erith]], [[Kent]] boarding at Rose Hill Villas, Bexley Road and described as a ''Mechanical Draughtsman''.<ref name="Census1901" />

Arthur Rowledge joined [[Napier & Son]] in 1913 as Chief Designer. After designing car engines and the [[Napier Lion]] aero engine, Rowledge took up a similar position  at [[Rolls-Royce Limited]] in 1921, where he became known as 'Rg' in [[List of Rolls-Royce personnel codes|company shorthand]]. He is credited with designing the [[Rolls-Royce Condor|Condor III]], the [[Rolls-Royce Kestrel|Kestrel]] and the [[Rolls-Royce R]] racing engine, which was used with great success at the 1929 and 1931 [[Schneider Trophy]] races. Development work on the [[Rolls-Royce Merlin|Merlin engine]] was one of his last contributions to aero engine design, along with responsibility for the [[Rolls-Royce Exe|Exe]] and [[Rolls-Royce Pennine|Pennine]] projects, before retiring from Rolls-Royce in 1945 at the age of 70.<ref>Pugh 2000, p. 318.</ref><ref>Lumsden 2003, p. 183.</ref><ref name="Flight58">[http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200003.html ''Flight'', 3 January 1958] www.flightglobal.com. Retrieved: 21 October 2009</ref>

==Death==
Rowledge died on 11 December 1957, aged 81.<ref name="Flight58"/>

==Honours and awards==
* 26 March 1920 -  [[Order of the British Empire|Member of the Order of the British Empire (MBE)]]. Arthur John Rowledge, Esq. Chief Designer, Messrs. Napier and Son, Limited.<ref name="mbe" />
* 1931 - Gold Medal of the [[Motor Industry Research Association#History|Institution of Automobile Engineers]] for contributions to [[Schneider Trophy]] aero engine development, specifically development of the [[Rolls-Royce R]] engine.<ref name="Flight58"/>
* 1941 - Fellow of the [[Royal Society]].<ref name="frs"/><ref name="Flight58"/>

==References==

===Notes===
{{reflist|refs=
<ref name="Census1881">[[Census in the United Kingdom|1881 Census of Peterborough]], RG11/1594, Folio 20, Page 34, Arthur J Rowledge, 47 Gladstone Street, Peterborough.</ref>
<ref name="Census1891">[[Census in the United Kingdom|1891 Census of Peterborough]], RG12/1228, Folio 78, Page 33, Arthur John Rowledge, 118 Gladstone Street, Peterborough.</ref>
<ref name="Census1901">[[Census in the United Kingdom|1901 Census of Dartford]], RG13/699, Folio 108, Page 46, Arthur Rowledge, Rose Heath Villas, Bexley Road, Erith, Kent.</ref>
<ref name="mbe">{{LondonGazette |issue=31840 |date=30 March 1920 |startpage=3858 |supp=x |accessdate=2009-10-21}}</ref>
}}

===Bibliography===
{{refbegin}}
*Lumsden, Alec. ''British Piston Engines and their Aircraft''. Marlborough, Wiltshire: Airlife Publishing, 2003. ISBN 1-85310-294-6.
* Pugh, Peter. ''The Magic of a Name - The Rolls-Royce Story - The First 40 Years''. Cambridge, England. Icon Books Ltd, 2000. ISBN 1-84046-151-9
{{refend}}

==Further reading==
* Rubbra, A.A. ''Rolls-Royce Piston Aero Engines - a designer remembers: Historical Series no 16'' :Rolls Royce Heritage Trust, 1990. ISBN 1-872922-00-7

==External links==
*[http://www.flightglobal.com/pdfarchive/view/1930/untitled0%20-%200321.html Flightglobal.com - Six years' progress of water-cooled engines by A. J. Rowledge, ''Flight'', 14 March 1930]

{{Rolls-Royce aeroengines}}

{{DEFAULTSORT:Rowledge, Arthur}}
[[Category:1876 births]]
[[Category:1957 deaths]]
[[Category:English aerospace engineers]]
[[Category:Fellows of the Royal Society]]
[[Category:Members of the Order of the British Empire]]
[[Category:People from Peterborough]]