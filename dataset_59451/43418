__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=Polikarpov I-1
 |image=Polikarpov I-1.jpg
 |caption=The IL-400 prototype
}}
{{Infobox Aircraft Type
 |type=Monoplane fighter
 |national origin=USSR
 |manufacturer=[[Polikarpov]]
 |designer=[[Nikolai Nikolaevich Polikarpov]]
 |first flight=15 August 1923
 |introduced=
 |retired=
 |status=
 |primary user=
 |more users=
 |produced=
 |number built=2 prototypes + 33 
 |variants with their own articles=
}}
|}

The '''Polikarpov I-1''' was the first indigenous [[USSR|Soviet]] [[monoplane]] [[fighter aircraft|fighter]]. It had to be redesigned after the crash of the first prototype, and was eventually ordered into production in small numbers, but it was never accepted for service with the [[Soviet Air Forces]].

==Development==
The I-1 (I = ''Istrebitel'' or fighter) was a single-seat, low-wing, wooden aircraft. The first prototype, known as the IL-400, was designed around the {{convert|400|hp|adj=on|lk=in}} [[Liberty L-12]] piston engine. It used the [[radiator (engine cooling)|radiator]] and [[propeller (aircraft)|propeller]] of the [[Airco DH.9A]]. The fuselage and wings were covered with a mix of plywood and fabric. The landing gear was braced by wires and the wingtips were protected by tubular curved skids. On the IL-400's [[maiden flight]] on 15 August 1923, the aircraft [[stall (flight)|stalled]] and crashed because its [[Center of gravity of an aircraft|center of gravity]] was too far aft.<ref>Gunston, p. 286</ref>

A second prototype, designated as the IL-400bis or IL-400B, was built, but it had to be redesigned to cure the center of gravity problem. A new, thinner, aluminum wing was designed and the [[tailplane]] and [[vertical stabilizer]] were enlarged. The cockpit and engine were moved forward and the radiator was replaced by a Lamblin type. It made its first flight on 18 July 1924 and was cleared for production on 15 October as the I-1.<ref name=g7>Gunston, pp. 286–87</ref>

33 aircraft were ordered, but the first production aircraft was still a prototype. The aluminum skin was replaced by plywood, the radiator was switched for a honeycomb type, and two synchronised 7.62&nbsp; mm (0.3&nbsp; in) [[PV-1 machine gun]]s were fitted. The production aircraft differed from one another and all were used for testing. This showed that the I-1 did not easily recover from a [[spin (flight)|spin]] and [[Mikhail Mikhaylovich Gromov]] was forced to make the first Soviet [[parachute]] jump during one such incident on 23 June 1927. The aircraft never entered operational service.<ref name=g7/>

==Operators==
;{{USSR}}
*[[Soviet Air Force]]

==Specifications==
{{aerospecs
|ref=Gunston, ''The Osprey Encyclopedia of Russian Aircraft 1875-1995''
|met or eng?=<!-- eng for US/UK aircraft, met for all others -->met

|crew=1
|capacity=
|length m=8.3
|length ft=27
|length in=3.75
|span m=10.80
|span ft=35
|span in=5¼
|height m=
|height ft=
|height in=
|wing area sqm=20
|wing area sqft=215
|empty weight kg=
|empty weight lb=
|gross weight kg=1,510
|gross weight lb=3,329

|eng1 number=1
|eng1 type=[[Liberty L-12]] piston engine
|eng1 kw=<!-- prop engines -->298
|eng1 hp=<!-- prop engines -->400

|max speed kmh=264
|max speed mph=164
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|range km=650
|range miles=404
|ceiling m=6,750
|ceiling ft=22,150
|climb rate ms=
|climb rate ftmin=

|armament1=2 × synchronised 7.62 mm (0.3&nbsp;in) [[PV-1 machine gun]]s.
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of military aircraft of the Soviet Union and the CIS]]
}}

==Notes==
{{Reflist|2}}

==References==
{{commons category|Polikarpov I-1}}
* {{cite book|last=Gunston|first=Bill|authorlink=Bill Gunston |title=The Osprey Encyclopedia of Russian Aircraft 1875-1995|publisher=Osprey|location=London|date=1995|isbn=1-85532-405-9}}
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |pages= }}

{{Soviet fighter aircraft}}
{{Polikarpov aircraft}}

[[Category:Soviet and Russian fighter aircraft 1920–1929]]
[[Category:Polikarpov aircraft|I-001]]