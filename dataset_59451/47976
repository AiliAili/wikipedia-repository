{{Orphan|date=February 2017}}

'''François-Michel Le Tourneau''' is a French geographer born in [[Léhon]] (Côtes d'Armor, France) in 1972. He is a senior research fellow at the [[French National Center for Scientific Research (CNRS)]]. His work focus on settlement and use of sparsely populated areas, especially the Brazilian [[Amazon rainforest|Amazon]]. He is particularly interested in indigenous people or traditional populations and their relationships with their territory. He has authored a number of papers in national and international scientific journals (list [https://cv.archives-ouvertes.fr/francois-michel-le-tourneau here on HAL-SHS], [https://www.researchgate.net/profile/Francois-Michel_Le_Tourneau here on Researchgate] or [https://cnrs.academia.edu/Fran%C3%A7oisMichelLeTourneau here on Academia.edu]).

== Publications ==
He has published three books:
# [http://www.pur-editions.fr/detail.php?idOuv=3125 Le Tourneau F.-M. (2013) ''Le Jari, géohistoire d’un grand fleuve amazonien'', Rennes : PUR, 248 p.]<ref>{{Cite journal|last=Forget|first=Marie|date=2014-10-30|title=François-Michel Le Tourneau, Le Jari, Géohistoire d’un grand fleuve amazonien|url=https://norois.revues.org/5195|journal=Norois. Environnement, aménagement, société|language=fr|issue=232|pages=89–90|doi=10.4000/norois.5195|issn=0029-182X}}</ref>
# [http://www.editions-belin.com/ewb_pages/f/fiche-article-les-yanomami-du-bresil-14245.php Le Tourneau F.-M. (2010) ''Les Yanomami du Brésil, géographie d’un territoire amérindien''. Belin : Paris, collection Mappemonde, 480 p.]<ref>{{Cite journal|last=Arnauld de Sartre|first=Xavier|date=2011-10-05|title=Le Tourneau François-Michel, Les Yanomami du Brésil. Géographie d’un territoire amérindien|url=https://jsa.revues.org/11788|journal=Journal de la société des américanistes|language=fr|issue=97-1|pages=358–363|issn=0037-9174}}</ref><ref>{{Cite journal|last=Théry|first=Hervé|date=2011-08-31|title=François-Michel Le Tourneau, Les Yanomami du Brésil. Géographie d’un territoire amérindien|url=https://cal.revues.org/451|journal=Cahiers des Amériques latines|language=fr|volume=2011/2|issue=67|pages=183–205|issn=1141-7161}}</ref><ref>{{Cite news|url=https://www.monde-diplomatique.fr/2010/12/BAILBY/19983|title=Les Yanomami du Brésil. Géographie d’un territoire amérindien|date=2010-12-01|newspaper=Le Monde diplomatique|access-date=2017-01-13}}</ref><ref>{{Cite journal|last=Ghorra-Gobin|first=Cynthia|date=2010-07-01|title=François-Michel Le Tourneau, Les Yanomami du Brésil|url=https://gc.revues.org/1804|journal=Géographie et cultures|language=fr|issue=74|pages=129–130|issn=1165-0354}}</ref>
# [http://www.editions-belin.com/ewb_pages/f/fiche-article-l-amazonie-bresilienne-et-le-developpement-durable-17136.php Le Tourneau F.-M., Droulers M. (Dirs.) (2010) ''L’Amazonie brésilienne et le développement durable'', Paris : Belin, 480 p.]<ref>{{Cite journal|last=Labarthe|first=Sunniva|date=2012-03-31|title=Martine Droulers, François-Michel Le Tourneau (dir.), L’Amazonie brésilienne et le développement durable|url=https://cal.revues.org/1033|journal=Cahiers des Amériques latines|language=fr|issue=69|pages=178–183|issn=1141-7161}}</ref>

== Expeditions ==
He organized a number of expeditions in remote and isolated regions of the Amazon and French Guiana, some of them were the subject of TV documentaries (see sources):
* '''2015 : « le raid des 7 bornes »,''' a 320&nbsp;km hike along the border between French Guiana and Brazil, in collaboration with the French Foreign Legion ([http://videotheque.cnrs.fr/doc=4842 TV show], [https://www.franceinter.fr/emissions/le-temps-d-un-bivouac/le-temps-d-un-bivouac-05-aout-2015 radio program], [https://lejournal.cnrs.fr/nos-blogs/le-blog-des-sept-bornes/bientot-le-depart blog])
* '''2013 : expédition Culari-Tampak,''' crossing between Brazil and French Guiana up the Jari and Culari rivers and then down the Tampak, (700&nbsp;km of which 400&nbsp;km canoeing) ([https://www.franceinter.fr/emissions/le-temps-d-un-bivouac/le-temps-d-un-bivouac-03-juillet-2013 radio program], [http://www.fmlt.net/culari interactive diary])
* '''2011 : expédition Mapaoni,''' up the Jari and Mapaoni rivers in Brazil until the Trijonction point between Brazil,French Guiana and Suriname (1500&nbsp;km) ([http://boutique.arte.tv/f8004-expedition_mapaoni TV show)].
He also publishes on a blog in the French edition of the [http://www.huffingtonpost.fr/bloggers/francoismichel-le-tourneau/ Huffington Post]

== References ==
{{reflist}}

==External links==
*{{ResearchGate|Francois-Michel_Le_Tourneau}}
{{authority control}}

{{DEFAULTSORT:Tourneau, Francois-Michel}}
[[Category:1972 births]]
[[Category:Living people]]
[[Category:People from Côtes-d'Armor]]
[[Category:French geographers]]