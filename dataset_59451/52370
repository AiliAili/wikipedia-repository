{{Use dmy dates|date=August 2014}}
{{Infobox journal
| title = Nature Communications
| cover =
| editor = Joerg Heber
| discipline = [[Natural sciences]]
| formernames =
| abbreviation = Nat. Commun.
| publisher = [[Nature Publishing Group]]
| country =
| frequency = Upon acceptance
| history = 2010-present
| openaccess = Yes
| license = [[Creative Commons license]]s
| impact = 11.329
| impact-year = 2015
| website = http://www.nature.com/ncomms/index.html
| link2 = http://www.nature.com/ncomms/archive/date.html
| link2-name = Online archive
| JSTOR =
| OCLC = 614340895
| LCCN = 
| CODEN = NCAOBW
| ISSN = 
| eISSN = 2041-1723
}}
'''''Nature Communications''''' is a [[peer-reviewed]] [[open access]] [[scientific journal]] published by the [[Nature Publishing Group]] since 2010. It covers the [[natural sciences]], including [[physics]], [[chemistry]], [[Earth sciences]], and [[biology]].

The [[editor-in-chief]] was Joerg Heber.<ref>{{Cite news|url=http://blogs.plos.org/plos/2016/09/plos-appoints-dr-joerg-heber-editor-in-chief-of-plos-one/|title=PLOS appoints Dr. Joerg Heber Editor-in-Chief of PLOS ONE {{!}} The Official PLOS Blog|date=2016-09-16|newspaper=The Official PLOS Blog|access-date=2016-11-20}}</ref> The founding editor-in-chief was Lesley Anson. The journal has editorial offices in London, New York City, and Shanghai.

Starting October 2014, the journal only accepted submissions from authors willing to pay an [[article processing charge]], and until the end of 2015, part of the published submissions were only available to subscribers. In January 2016, all content became freely accessible to the public.<ref>{{cite web |url=http://www.nature.com/ncomms/open_access/index.html |archive-url=https://web.archive.org/web/20091019184541/http://www.nature.com:80/ncomms/open_access/index.html |dead-url=yes |archive-date=2009-10-19 |title=Open Access |work=Nature Communications  |publisher=Nature Publishing Group |accessdate=2015-11-21}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Science Citation Index]]<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2014-09-27}}</ref>
* [[Current Contents]]/Agriculture, Biology & Environmental Sciences<ref name=ISI/>
* Current Contents/Life Sciences<ref name=ISI/>
* Current Contents/Physical, Chemical & Earth Sciences<ref name=ISI/>
* [[The Zoological Record]]<ref name=ISI/>
* [[BIOSIS Previews]]<ref name=ISI/>
* [[Chemical Abstracts Service]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2015-03-26}}</ref>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url=http://www.ncbi.nlm.nih.gov/nlmcatalog/101528555 |title=Nature Communications |work=NLM Catalog |publisher=[[National Center for Biotechnology Information]] |accessdate=2015-03-26}}</ref>
* [[Scopus]]<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2015-03-26}}</ref>
* [[Directory of Open Access Journals]] (DOAJ)<ref name=DOAJ>{{cite web |url=https://doaj.org/toc/2041-1723 |title=Nature Communications |publisher=[[Directory of Open Access Journals]] |accessdate=2016-06-11}}</ref>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 11.329.<ref name=WoS>{{cite book |year=2016 |chapter=Nature Communications |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==See also==
*''[[Nature (journal)]]''
*''[[Scientific Reports]]''

==References==
{{Reflist|30em}}

==External links==
{{commons category |Media from Nature Communications |Nature Communications}}
*{{Official website|http://www.nature.com/ncomms/index.html}}

{{Georg von Holtzbrinck Publishing Group|state=collapsed}}

[[Category:Nature Publishing Group academic journals]]
[[Category:Multidisciplinary scientific journals]]
[[Category:Open access journals]]
[[Category:Publications established in 2010]]
[[Category:English-language journals]]
[[Category:Creative Commons-licensed journals]]
[[Category:Continuous journals]]