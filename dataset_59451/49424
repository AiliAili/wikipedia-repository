{{About|the building occupied by school of the same name|the school itself|Mac.Robertson Girls' High School}}
{{Orphan|date=May 2012}}

{{Infobox school
| name = MacRobertson Girls’ High School
| image = [[File:MacRobertson Girls' High School Main Entry.jpg|frameless]]
| caption = MacRobertson Girls' High School Main Entry
| location = 350–370 Kings Way<br />Melbourne, Victoria
| country = [[Australia]]
| coordinates = {{Coord|37.8360|S|144.9722|E|type:edu_region:AU-VIC|display=inline,title}}
| principal =
| faculty =
| ratio =
| enrolment =
| type = [[State school|State]]
| established = 1905
| homepage = [http://www.macrob.vic.edu.au/ www.macrob.vic.edu.au]
}}
'''MacRobertson Girls’ High School''' is an all-girls state secondary school located on Kings Way, Albert Park, [[South Melbourne, Victoria]], Australia. It was named after [[Macpherson Robertson|Sir Macpherson Robertson]]’s chocolate factory after he donated £100,000 to the state, £40,000 of which was spent to construct the school.  Norman Seabrook of [[Seabrook and Fildes]] architecture practise, designed the building after winning the state wide design competition with his functional and modern design entry.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=35 |accessdate=4 April 2012}}</ref> Constructed in 1934 during [[1934 Centenary of Melbourne|centenary celebrations of Victoria]], MacRobertson was vital to the progress of [[modernist architecture]] in Australia and essential in the strong re-emergence of the state after the economic downturn of the depression.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=34 |accessdate=4 April 2012}}</ref>

== Description ==
[[File:MacRobertson Girls' High School's courtyard.JPG|thumb|left|MacRobertson Girls' High School's courtyard]]
The school was zoned in a functional manner with four wings for different disciplines including classrooms, science rooms, art rooms, and cookery rooms. This allowed for smooth movement between disciplines and also created distinct external courtyard areas around the building.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=45 |accessdate=4 April 2012}}</ref>

[[File:The School's characteristic clock tower with two white rendered vertical strips.JPG|thumb|The School's characteristic clock tower with two white rendered vertical strips]]

The facade of the building comprises “interlocking cubic forms of differing heights” which is offset by the vertical [[clock tower]] with white rendered vertical strips. The material Seabrook used were functional while at the same time embracing the typical palette of [[De Stijl]] movement by using striking colours of cream brick, red steel framed hopper windows and dark blue glazed brick piers between windows.<ref>{{cite web|title=Mac.Robertson Girls’ High School|url=http://docomomoaustralia.com.au/pdf/Fiche_2007/Macrobertson_Gilrls_High.pdf|publisher=Docomomo Minimum Documentation Fiche|accessdate=6 April 2012}}</ref> Internally softer shades of red, blue, yellow, green and black were used.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=49 |accessdate=4 April 2012}}</ref> With the use of practical floor finish material such as [[linoleum]] for classrooms, terracotta tiles for corridors and granolithic materials for the stairs and services rooms.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=44 |accessdate=4 April 2012}}</ref>

== Key Influences ==

It is believed that the main influence of Seabrook’s design for MacRobertson Girls’ High School was [[Willem Marinus Dudok|William Dudok]] [[Hilversum]]’s town hall (1923-31).<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=39 |accessdate=4 April 2012}}</ref> Both these buildings have similar brickwork, rectilinear interlocking facades, functional planning, open air classrooms, flat roof, industrial aesthetic and a modern interior fitout
.<ref>{{cite book |title=The Encyclopedia of Australian architecture |last=GOAD |first=Philip |author2=WILLS, Julie|year=2012 |publisher=Port Melbourne, Vic. : Cambridge University Press |isbn=9780521888578 |page=620 }}</ref>

The brickwork used in both consists of two strectchers followed by a header in a [[Flemish bond]] with an extra wide and deeply raked horizontal joint which emphasises horizontality,<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=47 |accessdate=4 April 2012}}</ref> while the cream brickwork emphasised shadows. Seabrook selected local Glen Iris Cream bricks at a time when they were only being used sparingly in buildings such as in polychromatic brickwork.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=48 |accessdate=4 April 2012}}</ref> Using the cream brick for the entirety of the building was seen as a modernist approach and set a trend for many future buildings in Victoria.

== Design Approach ==

[[File:Red hopper windows with blue tiled piers in between.JPG|thumb|Red hopper windows with blue tiled piers in between]]

Seabrook had a strong functional design approach to the design of MacRobertson Girls’ High School. He believed that a '' “building must look like what it is, be it a town hall or a destructor plan…” ''.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=40 |accessdate=4 April 2012}}</ref> This frame of mind helped in creating the unique and functional design of the building which has had a great impact on [[Architecture of Australia|Australian architecture]]. It was the first modernist school constructed in Victoria, at a time when other contemporary schools tended to adopt a variety of [[Gothic Revival architecture|Gothic collegiate]] to [[Georgian architecture|Georgian revival style]] in the design. [[Robin Boyd]] described the building as an “evolution of modern architecture” in Australia.<ref>{{cite web|title=Mac Robertson Girls High School|url=http://www.architecture.com.au/i-cms_file?page=4048/MacRobV2.pdf|publisher=RAIA Victoria|accessdate=5 April 2012}}</ref>

The differing masses of MacRobertson impart proportion and scale to the building,<ref>{{cite book |title=Architectural biography:  John Felix Mattews, Seabrook and Fildes, Louis R. Williams, Philip B. Hudson, thesis (Undergrad)—University of Melbourne |last=JUDITH |first=Antcliff |author2=PETER, Dedge |author3=TIM, McCowan |author4=JANET, MacPherson|year=1981 |location=Faculty of Architecture, Building and Planning|page=10 |accessdate= 5 April 2012}}</ref> while the [[De Stijl]] colour of the articulated red steel hopper windows contrast to the blue glazed brick piers and cream brickwork, helping to break up the facade.<ref>{{cite book |title=Architectural biography:  John Felix Mattews, Seabrook and Fildes, Louis R. Williams, Philip B. Hudson, thesis (Undergrad)—University of Melbourne |last=JUDITH |first=Antcliff |author2=PETER, Dedge |author3=TIM, McCowan |author4=JANET, MacPherson|year=1981 |location=Faculty of Architecture, Building and Planning|page=11 |accessdate= 5 April 2012}}</ref> Steel windows were not common in schools at this time and are seen as a modernist and functional approach.

Seabrook also considered the site in his design, using [[native plants]] to embrace the dry, flat [[scrubland]] of South Melbourne.<ref>{{cite book |title=Planting the seeds of Modernism: The work of Seabrook and Fildes 1933-1950|last=PHILLIPS|first=Christine |year=2007 |location=Faculty of architecture, Building and Planning. The University of Melbourne |page=39 |accessdate=4 April 2012}}</ref> The [[flagpole]] and [[clock tower]] are also significant in his design and can be seen in many of Seabrook’s later work.

==Distinguished MacRobertsons==

* [[Jean Burns|Jean Ethel Burns]], first Australian woman to parachute over Australian soil (21 November 1937) and youngest female pilot record holder (13 March 1937-1952)

== References ==
{{reflist}}

== External links ==
* [http://www.macrob.vic.edu.au The Mac.Robertson Girls High School Official Page ]
* [http://vhd.heritage.vic.gov.au/places/result_detail/2961?print=true MacRobertson Girls’ High School in Victorian Heritage Database]
* [http://adb.anu.edu.au/biography/seabrook-norman-hugh-11645 Seabrook, Norman Hugh (1906–1978) by Philip Goad ]
* [http://calitreview.com/48 Architecture and Modernism]

{{DEFAULTSORT:MacRobertson Girls' High School}}
[[Category:Art Deco architecture in Melbourne]]
[[Category:Educational institutions established in 1905]]
[[Category:High schools in Victoria (Australia)]]
[[Category:Girls' schools in Australia]]
[[Category:Selective schools]]
[[Category:Public schools in Victoria (Australia)]]
[[Category:Schools in Melbourne]]
[[Category:School buildings completed in 1934]]
[[Category:1905 establishments in Australia]]