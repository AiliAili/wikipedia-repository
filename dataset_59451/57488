{{Infobox Organization
|name                = International Association for Near-Death Studies
|image               =
|size                =
|caption             =
|abbreviation        = IANDS
|formation           = 1981
|status              = [[Non-profit organisation]]
|purpose             = [[Near-death studies]]
|location            = Durham, North Carolina (USA)
|region_served       = Worldwide
|membership          = Differentiated: Life, Benefactor, Patron, Sponsor, Professional, Supporting, Basic. 
|leader_title        = President
|leader_name         = [[Diane Corcoran]] (2008-)
|main_organ          = IANDS Board of Directors 
|parent_organization =
|affiliations        = 
|budget              =
|website             = [http://iands.org/home.html  IANDS]
}}

The '''International Association for Near-Death Studies''' ('''IANDS''') is a [[non-profit organization]] based in [[Durham, North Carolina]] in the United States, associated with the academic field of [[near-death studies]].<ref name="IANDS Brochure">IANDS. "Near-Death Experiences: Is this what happens when we die?" Durham: International Association for Near-Death Studies. Informational brochure REV 4/11. Available at www.iands.org.</ref><ref name="Graves 2007">Graves, Lee. "Altered States. Scientists analyze the near-death experience". ''The University of Virginia Magazine'', Summer 2007 Feature</ref><ref name="Griffith 2009">Griffith, Linda J. Near-Death Experiences and Psychotherapy. Psychiatry (Edgmont). 2009 October; 6(10): 35–42.</ref><ref name="Beck 2010">Beck, Melinda. "Seeking Proof in Near-Death Claims". ''The Wall Street Journal'' (Health Journal), October 25, 2010</ref><ref name="MacDonald 2011">MacDonald, G. Jeffrey. "Scientists probe brief brushes with the afterlife". ''The Christian Century'', Jan 12, 2011</ref><ref name="Lam 2011">Lam, Stephanie. "Near-Death Experiences: 30 Years of Research — Part 1". ''The Epoch Times'', published online September 13, 2011</ref><ref name="K Upchurch 2012">Upchurch, Keith. "Near-death researcher believes the mind survives death". ''The Herald Sun'', published online 01.24.12</ref>
The Association was founded in the USA in 1981, in order to study and provide information on the phenomena of the [[near death experience]] (NDE). Today it has grown into an international organization, which includes a network of more than 50 local interest groups,<ref name="IANDS Brochure"/> and approximately 850 members worldwide.<ref name="K Upchurch 2012">Upchurch, Keith. "Near-death researcher believes the mind survives death". ''The Herald Sun'', published online 01.24.12</ref> Local chapters, and support groups, are established in major U.S cities.<ref name="Graves 2007"/><ref name="MacDonald 2011"/><ref name="Anderson 1999">Anderson, Jon. "Almost Blinded By The Light Near-death Experiences Share Common Thread". Chicago Tribune, September 29, 1999</ref><ref name="Anderson 2004">Anderson, Jon. "Shedding light on life at death's door". ''Chicago Tribune'', published online May 13, 2004</ref><ref name="Forgrave 2001">Forgrave, Reid. "A glimpse of the 'other side': Seattle conference unites near-death individuals". ''The Seattle Times'', published online Friday, July 27, 2001</ref><ref name="Morgan 2008">Morgan, Kim. "Members of Near Death group hear, share experiences at Center Point." ''The Houston Chronicle'', published online March 13, 2008</ref><ref name="Brody 1988">Brody, Jane E. "Health; Personal Health". ''New York Times'', November 17, 1988.</ref> IANDS also supports and assists near-death experiencers (NDErs) and people close to them. In one of its publications the organization has formulated its vision as one of building "global understanding of near-death and near-death-like experiences through research, education, and support".<ref name="IANDS Brochure"/>

==History==

The organization was originally known as the ''Association for the Scientific Study of Near-Death Phenomena''. This group was founded by researchers [[John Audette]], [[Bruce Greyson]], [[Kenneth Ring]] and [[Michael Sabom]] in 1978.<ref name="IANDS Fact Sheet">[http://iands.org/about/about-iands27/fact-sheet.html "IANDS Fact Sheet, as of December 2010'']. Accessed 2012-02-09.</ref><ref name="Ring 2000">Ring, Kenneth. Religious Wars in the NDE Movement: Some Personal Reflections on Michael Sabom's Light & Death. ''Journal of Near-Death Studies'', 18(4) Summer 2000</ref><ref name="Greyson 2013">Greyson, Bruce. "An Overview of Near-Death Experiences". ''Missouri Medicine'', November/December 2013</ref> The first president of this association was John Audette, who later served as executive director.<ref name="IANDS Fact Sheet"/><ref name="Ring 2000"/><ref name="NYT 1982">New York Times Staff. "Connecticut Guide. Near-death Symposium". New York Times, April 25, 1982</ref> In 1981 the organization changed its name to the ''International Association for Near-Death Studies'' (also known as IANDS).<ref name="IANDS Fact Sheet"/> A headquarter was established in Connecticut, and was affiliated with the [[University of Connecticut]], Storrs.<ref name="NYT 1982"/><ref name="Ziegler 1985">Ziegler, Jan. "Near-death Experiences Deemed Worthy Of Serious Research". ''The Chicago Tribune'', October 06, 1985</ref><ref name="NYT 1986">New York Times Staff. "Near-Death Experiences Illuminate Dying Itself". ''New York Times'', October 28, 1986</ref> Offices were administered by Nancy Evans Bush,<ref name="Bush 1991">Bush, Nancy Evans. Is Ten Years a Life Review? ''Journal of Near-Death Studies'', 10(1) Fall 1991</ref> who later served as executive director.<ref name="Ziegler 1985"/>

Past presidents of IANDS also include researchers Kenneth Ring and Bruce Greyson, who served as Presidents in the early 1980s. The presidencies of Ring and Greyson (1981–83) marked the beginning of professional research on the topic of NDE's, leading up to the establishment of the ''[[Journal of Near-Death Studies]]'' in 1982.<ref name="Griffith 2009"/><ref name="IANDS Fact Sheet"/><ref name="NYT 1982"/> Greyson later served as director of research at IANDS.<ref name="Genova 1988">Genova, Amy Sunshine. "Experiencing Near-death Promotes Better Life". ''The Sun Sentinel'', August 29, 1988</ref> During the presidency of John Alexander, in 1984, the organization held its first research conference in Farmington (CT),.<ref name="IANDS Fact Sheet"/>

Elizabeth Fenske took over the presidency from John Alexander in 1986, and was involved in the relocation of the main office to Philadelphia in the late 1980s.<ref name="IANDS Fact Sheet"/><ref name="Detjen 1989">Detjen, Jim. "Near-death Experiences Deemed Worthy Of Study". ''The Chicago Tribune'', January 08, 1989</ref> The end of the decade also marked a period of outreach for IANDS. Local branches were established in major U.S cities, and the first national IANDS conference was held at Rosemont College (PA) in 1989.<ref name="Brody 1988">Brody, Jane E. "Health; Personal Health". ''New York Times'', November 17, 1988.</ref><ref name="IANDS Fact Sheet"/><ref name="Detjen 1989"/> In the period from 1992-2008 IANDS-offices were administered by external service providers.<ref name="IANDS Fact Sheet"/>

In 2008, during the presidency of Diane Corcoran, the organization established its current headquarter in [[Durham, North Carolina]]. Later activity includes development of the IANDS website, and continued  maintenance of support groups and members.<ref name="Beck 2010"/><ref name="MacDonald 2011">MacDonald, G. Jeffrey. "Scientists probe brief brushes with the afterlife". ''The Christian Century'', Jan 12, 2011</ref><ref name="K Upchurch 2012">Upchurch, Keith. "Near-death researcher believes the mind survives death". ''The Herald Sun'', published online 01.24.12</ref><ref name="IANDS Fact Sheet"/><ref name="Upchurch 2012">Upchurch, Keith. "Retired colonel has dealt with near-death experiences since Vietnam War". ''The Herald Sun'', published online 01.24.12</ref>

==Publications and archives==

IANDS is responsible for the publishing of the ''[[Journal of Near-Death Studies]]'',<ref name="Anderson 1999"/><ref>[http://www.iands.org/research/publications/journal-of-near-death-studies.html IANDS ''Journal of Near-Death Studies'']. Accessed 2011-02-06.</ref><ref name="Anderson 2002">Anderson, Jon. "Doctor delves into mysteries". ''The Chicago Tribune'', April 26, 2002</ref><ref name="Williams 2007">Williams, Daniel. "At the Hour Of Our Death". ''TIME Magazine''. Friday, Aug. 31, 2007</ref><ref name="Graves 2007"/><ref name="Beck 2010"/><ref name="Greyson 2013"/><ref name="The Atlantic 2015">Lichfield, Gideon. "The Science of Near-Death Experiences. Empirically investigating brushes with the afterlife". ''The Atlantic'', April 2015</ref> originally known as "Anabiosis".<ref name="Griffith 2009"/> The only scholarly journal in the field of [[Near-death studies|Near-Death Studies]]. It is peer-reviewed, and is published quarterly.<ref name="Anderson 2004"/><ref name="IANDS Fact Sheet"/>

Another publication is the quarterly newsletter ''Vital Signs'', first published in 1981.<ref name="IANDS Brochure"/><ref name="Anderson 1999"/><ref name="Genova 1988"/><ref name="Anderson 2002"/> The organization also maintains an archive of near-death case histories for research and study.<ref>[http://www.iands.org/research/nde-research/nde-archives31.html IANDS: NDE Archives]. Accessed 2011-02-06.</ref>

==Conferences==

IANDS arranges conferences on the topic of Near-death Experiences.<ref name="IANDS Fact Sheet"/><ref name="Greyson 2013"/><ref name="Sofka 2010">Sofka, Carla J. "News and Notes". ''Death Studies'', 34: 671–672, 2010</ref><ref name="The Atlantic 2015"/> The conferences are held in major U.S cities, almost annually. The first meeting was a medical seminar at Yale University, New Haven (CT) in 1982. This was followed by the first clinical conference in Pembroke Pines (FL), and the first research conference in Farmington (CT) in 1984.<ref name="IANDS Fact Sheet"/> Each conference is usually defined by the formulation of a conference theme. In 2004 the conference theme was "Creativity from the light".<ref name="Daily Northwestern 2004">Gordon, Scott. "Evanston’s brush with death." ''The Daily Northwestern'', published online June 30, 2004</ref>

The organization also collaborates with academic locations in regard to hosting conferences. In 2001 the IANDS conference was held at [[Seattle Pacific University]].<ref name="Forgrave 2001"/> In 2008 IANDS collaborated with [[University of Texas M.D. Anderson Cancer Center]], which became the first medical institution to host the annual IANDS conference.<ref name="Morgan 2008"/><ref name="Hopper 2006">Hopper, Leigh. "Conference to shed light on 'near-death' experiences." ''The Houston Chronicle'', published online October 25, 2006</ref> The 2014 conference was held in Newport Beach (Calif.) and gathered the attention from the newspaper [[The Epoch Times]], which produced several reports from the meeting.<ref name="The Epoch Times 2014">MacIsaac, Tara. "11 Powerful, Thought-Provoking Quotes From a Near-Death Experiences Conference". ''The Epoch Times'', September 12, 2014</ref>

==See also==

* [[Near-death studies]]
* [[Parapsychology]]
* [[Bruce Greyson]]
* [[Raymond Moody]]
* [[Kenneth Ring]]
* [[American Society for Psychical Research]]
* [[Parapsychological Association]]
* [[Society for Psychical Research]]
* [[List of parapsychology topics]]

==References==
{{reflist|2}}

== Further reading ==
* Holden JM, Greyson B, James D, editors. (2009) ''The Handbook of Near-Death Experiences: Thirty Years of Investigation''. Santa Barbara, CA: Praeger/ABC-CLIO

== External links ==
* {{Official website|http://www.iands.org}}
* [http://www.iands.es IANDS España]. IANDS Spain
* [http://netzwerk-nahtoderfahrung.org/ Netzwerk Nahtoderfahrung] IANDS Germany

{{Parapsychology}}

{{DEFAULTSORT:International Association For Near-Death Studies}}
[[Category:Near-death experiences]]
[[Category:Transpersonal psychology]]
[[Category:Parapsychology]]
[[Category:Non-profit organizations based in North Carolina]]
[[Category:International scientific organizations]]
[[Category:Organizations based in Durham, North Carolina]]
[[Category:International organizations based in the United States]]