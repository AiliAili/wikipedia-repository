{{Infobox journal
|cover = [[File:International Relations of the Asia-Pacific.gif|195px|alt=Cover]]
|editor = Yoshihide Soeya<br />G. John Ikenberry
|discipline = International Relations Theory
|peer-reviewed =
|language =
|formernames =
|abbreviation =
|publisher = [[Oxford University Press]]
|country =
|frequency = Triannual
|history = 2001–present
|openaccess =
|license =
|impact = 0.758
|impact-year = 2014
|website = http://irap.oxfordjournals.org/
|link1 = http://irap.oxfordjournals.org/content/current
|link1-name = Online access
|link2 =http://irap.oxfordjournals.org/archive
|link2-name = Online archive
|JSTOR = 1470482X
|OCLC = 47130410
|LCCN = 2001208520
|CODEN =
|ISSN = 1470-482X
|eISSN = 1470-4838
}}
'''''International Relations of the Asia-Pacific''''' is a triannual, [[Peer review|peer-reviewed]], [[academic journal]], established in 2001, and published by the [[Oxford University Press]] on behalf of the Japan Association of International Relations ([[Japanese language|Japanese]]:''Nihon Kokusai Seiji Gakkai'', ''日本国際政治学会''), and assisted by [[HighWire Press]].

It is also the official journal of the [[Japan Association of International Relations]]. The [[editor in chief|co-editors in chief]] are [[Yoshihide Soeya]] ([[Keio University]]) and [[G. John Ikenberry]] ([[Princeton University]]).<ref name=about>
{{cite web|title=About the journal|work=International Relations of the Asia-Pacific|publisher=Oxford University Press|url=http://www.oxfordjournals.org/our_journals/irasia/about.html|accessdate=2010-09-11}}</ref>

==Scope==
The focus of this journal is significant developments in the Asia-Pacific region. Topics covered include China's politics, America's anti-terrorist war, America's place in the Asia-Pacific region, regional institutions, regional governance, Japan, Asian NGOs, China's relationship to the world economy, China's path of globalization, and China's national identification. Broad topical coverage encompasses international relations, Asia foreign relations, Pacific area relations.<ref name=about/>

The journal also functions as a forum for regional issues, points of view, presentation of scholarship, methodological approaches, schools of thought, and new ideas. Contributors work in all areas of the [[international relations]] field.<ref name=about/>

Publishing formats encompass original research (6,000 to 10,000 words), research notes (less than 10,000), book chapters, review essay articles (3,000–4,000), book reviews (600–1,000).<ref name=author>{{cite web|title=Author guidelines|work=International Relations of the Asia-Pacific|publisher=Oxford University Press|url=http://www.oxfordjournals.org/our_journals/irasia/for_authors/index.html|accessdate=2010-09-11}}</ref>

==Abstracting and indexing==
The journal is abstracted and indexed in<ref>{{cite web|title=International Relations of the Asia-Pacific|work=Master Journal List|publisher=[[Thomson Reuters]]|url=http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN=1470-482X|accessdate=2010-09-11}}</ref> the [[International Bibliography of the Social Sciences]], [[ProQuest]], [[CSA (database company)|CSA Worldwide Political Science Abstracts]], [[Scopus]], and the [[Social Sciences Citation Index]]

According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 0.758, ranking it 40th out of 115 journals in the category "Business".<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Political Science |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
* {{Official|http://irap.oxfordjournals.org/}}
* [http://wwwsoc.nii.ac.jp/jair/index_en.html The Japan Association of International Relations]

[[Category:International relations journals]]
[[Category:Asian studies journals]]
[[Category:Publications established in 2001]]
[[Category:Triannual journals]]
[[Category:Oxford University Press academic journals]]
[[Category:English-language journals]]