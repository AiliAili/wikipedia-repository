{{Infobox journal
| title = International Journal of Advanced Computer Technology
| cover = [[File:IJACST cover volume 1 number 1.jpg|200px]]
| former_name =
| abbreviation = Int. J. Adv. Comp. Technol.
| discipline = [[Computer science]]
| editor = Rishi Asthana 
| publisher = Research India Publications
| country = 
| history = 2012–present
| frequency = Continuous
| openaccess = Yes
| license = 
| impact = 
| impact-year = 
| ISSN = 2319-7900
| eISSN =
| CODEN =
| JSTOR = 
| LCCN = 
| OCLC =
| website = http://www.ijact.org/
| link2 = http://www.ijact.org/content.htm
| link2-name = Online archive
}}
The '''''International Journal of Advanced Computer Technology''''' is a publication which has been described as a [[Predatory open access publishing|predatory open access journal]]<ref name=io9/><ref name=Vox/><ref name=Mic/> – a publication which has some of the surface attributes of a benign [[open access journal]] but is actually an exploitative and deceptive corruption of that model, operating as a disreputable [[vanity press]] with little scholarly value.<ref name=CHE2012/><ref name=Slate/>

== Publication controversy ==
[[File:Get me off your fucking mailing list.png|thumb|left|One of the figures from Mazières' and Kohler's paper]]

In 2005, two scientists, David Mazières and [[Eddie Kohler]], wrote a paper titled ''Get me off Your Fucking Mailing List'' and submitted it to WMSCI 2005 (the 9th [[World Multiconference on Systemics, Cybernetics and Informatics]]), in protest of the conference's notoriety for its spamming and lax standards for paper acceptance.<ref name=Shieber/> The paper consisted essentially only of the sentence "Get me off your fucking mailing list" repeated many times.<ref name=GMOYFML/>

In 2014, after receiving a spam email from the ''International Journal of Advanced Computer Technology'', Peter Vamplew forwarded Mazières' and Kohler's old paper as an acerbic response.<ref name=SOA/> To Vamplew's surprise, the paper was reviewed, rated as "excellent" by the journal's peer-review process and accepted for publication.<ref name=SOA/><ref name=Slate/> The paper was not actually published as Vamplew declined to pay the required US$150 ({{Inflation|index=US|value=150|start_year=2005|fmt=eq|cursign=$}}) [[article processing charge]].<ref name=SOA/> This case has led commenters to question the legitimacy of the journal as an authentic scholarly undertaking.<ref name=io9/><ref name=Vox/><ref name=Slate/><ref name=SOA/>

{{clear}}

== References ==
{{reflist|colwidth=30em|refs=
<ref name=CHE2012>{{Cite web |last=Elliott |first=Carl |title=On Predatory Publishers: a Q&A With Jeffrey Beall |website=Brainstorm |publisher=The Chronicle of Higher Education |date=June 5, 2012 |url=http://chronicle.com/blogs/brainstorm/on-predatory-publishers-a-qa-with-jeffrey-beall/47667 |accessdate=November 22, 2014}}</ref>

<ref name=Shieber>{{cite web |url=http://blogs.law.harvard.edu/pamphlet/2010/05/01/worlds-most-excruciatingly-ironic-conference/ |title=World’s most excruciatingly ironic conference? |author=Stuart Shieber |date=May 1, 2010 |work=The Occasional Pamphlet on Scholarly Communication |accessdate=November 22, 2014}}</ref>

<ref name=GMOYFML>{{cite web |url=http://www.scs.stanford.edu/~dm/home/papers/remove.pdf |title=Get me off Your Fucking Mailing List |last1=Mazières |first1=David |last2=Kohler |first2=Eddie |publisher=Stanford Secure Computer Systems Group, Stanford University |accessdate=November 22, 2014}}</ref>

<ref name=io9>{{cite web |url=http://io9.com/sham-journal-accepts-totally-absurd-but-completely-appr-1661329028 |title=Sham Journal Accepts Totally Absurd But Completely Appropriate Paper |author=Robbie Gonzalez |date= |work=io9 |accessdate=November 22, 2014}}</ref>

<ref name=SOA>{{cite web|url=http://scholarlyoa.com/2014/11/20/bogus-journal-accepts-profanity-laced-anti-spam-paper/ |title=Bogus Journal Accepts Profanity-Laced Anti-Spam Paper |author=Jeffrey Beall |date=November 20, 2014 |work=Scholarly Open Access |accessdate=November 22, 2014 |deadurl=yes |archiveurl=https://web.archive.org/web/20141122164005/http://scholarlyoa.com/2014/11/20/bogus-journal-accepts-profanity-laced-anti-spam-paper/ |archivedate=November 22, 2014 |df= }}</ref>

<ref name=Vox>{{cite web |url=http://www.vox.com/2014/11/21/7259207/scientific-paper-scam |title="Get Me Off Your Fucking Mailing List" is an actual science paper accepted by a journal |author=Joseph Stromberg |date=November 21, 2014 |work=[[Vox (website)|Vox]] |publisher=[[Vox Media]] |accessdate=November 22, 2014}}</ref>

<ref name=Mic>{{cite web |url=http://mic.com/articles/104870/this-insane-science-article-exposes-a-major-problem-in-the-science-world |title="Get Me Off Your Fucking Mailing List" Is the Easiest Science Paper You'll Ever Read |author=Matt Connolly |date=November 22, 2014 |work=Mic |accessdate=November 22, 2014}}</ref>

<ref name=Slate>{{cite web |url=http://www.slate.com/blogs/browbeat/2014/11/24/bogus_academic_journal_accepts_paper_that_reads_get_me_off_your_fucking.html |title=The Bogus Academic Journal Racket Is Officially Out of Control |author=Rebecca Schuman |date=November 24, 2014 |work=Browbeat (Slate's culture blog) |publisher=Slate |accessdate=November 25, 2014}}</ref>
}}

==External links==
* {{Official website|http://www.ijact.org/}}

[[Category:English-language journals]]
[[Category:Open access journals]]
[[Category:Computer science journals]]
[[Category:Publications established in 2012]]