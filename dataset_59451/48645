'''Huyton Hill Preparatory School''' was a school for boys ages 8 to 13 focused on preparation for entering a [[Independent school (United Kingdom)|Public School]], open from 1926 to 1969.

The school is one of several that were evacuated from cities in England at the outbreak of [[World War II]] to escape the risk of German bombs. It was known for a progressive, liberal approach to discipline.

==The school at Huyton==

Huyton Hill was established in September 1926 with four pupils at Victoria Road in [[Huyton]] near Liverpool, England. It is listed in the Liverpool Schools directory<ref>[http://liverpool-schools.co.uk/html/d_-_k.html Liverpool Schools D-K]</ref> with its first headmaster, Hubert D. Butler.<ref>[http://liverpool-schools.co.uk/html/1938_kelly_s.html Liverpool Schools Teachers]</ref> 

The school motto was "I will with a good will", which set the tone for the ethos of the school and its administration.

Huyton Hill was the first school in the country to host its own aircraft landing strip as reported in <em>Flight Magazine</em>.<ref>[http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200683.html Flight Magazine July 8th 1932]</ref> Before becoming headmaster, Hubert Butler served in the Flying Corps in World War I as 2nd Lieutenant Flying Officer.

Butler became a member of the Incorporated Association of Preparatory Schools (IAPS) in November 1929, as listed in "Preparatory Schools Review", No. 104, Vol. X.<ref>"Preparatory Schools Review", No. 104, Vol. X.</ref>  Butler's name also appears in the "List of Members and Articles of Association", IAPS, January 1930, p.&nbsp;5.<ref>"List of Members and Articles of Association", IAPS, January 1930, p.5.</ref>  In the IAPS List of Members and Articles of Association, May 1951, p.&nbsp;7.,<ref>IAPS "List of Members and Articles of Association", May 1951, p.7.</ref> the names of both Butler brothers are listed.  Gerald V Butler became a member in 1950.

==The school in the Lake District==

<!-- Deleted image removed: [[File:Huyton Hill Preparatory School - aerial.JPG|200px|thumb|right|Aerial photograph of Huyton Hill Preparatory School, circa 1950{{Pufc|1=Huyton Hill Preparatory School - aerial.JPG|date=21 September 2011}}]] -->
In September 1939 at the outbreak of World War II, the school moved to the [[Lake District]] where Pull Wood House, at the northwest corner of [[Windermere]] just south of Pull Wyke, was rented to accommodate the school. After the war headmaster Butler purchased the house and grounds. Butler's brother Gerard joined him as joint headmaster in 1950, after leaving the army, and the two brothers ran the school until 1969.

The number of pupils averaged 60 in the post-war period.  The largest class, in 1965, was made up of 74 pupils, but the Pull Wood House couldn't accommodate a larger class so fewer attended in the next few years, with 61 attending in 1967. They were split into two houses, Alfred and Arthur, creating a healthy sense of peer competition.

Brigadier Gordon H. Osmaston joined the school as the mathematics teacher in 1948 after serving in the army in Iraq during World War II. Osmaston was most known for his three years (1936–1938) surveying and mapping the [[Himalayas]] with sherpa [[Tenzing Norgay]].<ref>[http://www.himalayanclub.org/journal/gordon-osmaston-and-tenzing/ Himalayan Journal 48, 1990-91, "Gordon Osmaston and Tenzing"]</ref>

==Progressive disciplinary policy==

The [[school discipline]] policy was very progressive for its time, using a system of points that triggered rewards or loss of privileges instead of traditional methods.  By the mid 1960s, corporal punishment had been abolished in the school.

The points system evolved over time. During the 1950s daily points were given or taken away depending upon behaviour. The boys could earn extra points during holidays by asking their parents to vouch that they had taken a cold bath every morning of the holidays (one point per day) or by pulling willow weeds along the school drive (one point for a hundred willows).  A published Conduct List displayed the ranking of boys according to their point score (affectionately known as the 'Spite and Favour' list). 

In the 1960s every pupil was awarded 10 points a day, making a perfect score for the week 70. Minor infringements could lead to the loss of one or two points, and up to 10 points could be lost for major misbehaviour. Scores were announced at the end of the week, with good conduct badges for those who lost no points and loss of privileges for those who lost too many points. Finishing the week with a negative score led to extra detention.

==Dormitories==

The [[dormitories]] in the new school facility were named after local [[List of hills in the Lake District|mountains]] in the Lake District as follows:

First floor (junior): [[Dollywaggon]], [[Catbells]], [[Langdale]], [[Helvellyn]], [[Latterbarrow]]*. 

Second floor (senior): [[Skiddaw]], [[Loughrigg]], [[Bowfell]], [[Kirkstone]], [[Wetherlam]], [[Scafell]].

<nowiki>* </nowiki>not always used as a dormitory, depending on school population.

==School song==
[[File:Huyton Hill School Song.jpg|200px|thumb|right|Huyton Hill school song printed for the Floodlight Review, June 1966]]
The school song was written by Hubert Butler with the help of pupils and set to the music [[Monk's Gate]], which is best known as the hymn [[To be a Pilgrim]] by [[John Bunyan]], 1684.

:''Often when tireless waves''
:''Hurl them together''
:''Into Tintagel's caves,''
:'''Spite of the weather''
:''Knights of the Table Round''
:''On quests of Mercy bound''
:''Sing to the thunder's sound''
:''Laugh at the lightning.''

:''Danes over wold and fen''
:''Britain encumber;''
:''Scarcely a thousand men''
:''Alfred can number,''
:''Yet shall the Wessex ground''
:''To their proud tramp resound,''
:''Drummed by the thunder's sound,''
:''Lit by the lightning.''

:''So here at Huyton Hill''
:''We pledge the future;''
:''We will with brave good will''
:''Meet all adventure.''
:''Where tasks do most confound,''
:''There may we straight be found,''
:''Though thunder echoes round''
:''After the lightning.''

==School closure==
[[File:Hubert Butler's Message 1969.jpg|200px|thumb|right|Hubert Butler's message to the school, pupils and parents, May 1969]]
Gerald Butler died in 1967 and the school was closed by his brother in 1969. Hubert Butler then converted the house into holiday flats. He died in 1971 whilst working for UNICEF in Switzerland and the ownership of the building passed over to his son.

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->
{{Reflist}}

== External links ==
* [http://liverpool-schools.co.uk/html/d_-_k.html Liverpool Schools D-K]
* [http://liverpool-schools.co.uk/html/1938_kelly_s.html Liverpool Schools Teachers]
* [http://www.flightglobal.com/pdfarchive/view/1932/1932%20-%200683.html Flight Magazine July 8th 1932]
* [http://www.iaps.org.uk Incorporated Association of Preparatory Schools (IAPS)]
* [http://www.huytonhillschool.co.uk Huyton Hill School]

{{Schools in Liverpool}}
{{coord|53|24|43.98|N|2|49|59.89|W|region:GB|display=title}}

[[Category:Boys' schools in Merseyside]]
[[Category:Educational institutions established in 1926]]
[[Category:1926 establishments in England]]
[[Category:Defunct schools in Liverpool]]
[[Category:Educational institutions disestablished in 1969]]
[[Category:1969 disestablishments in England]]