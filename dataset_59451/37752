{{Good article}}
{{Infobox officeholder
|name          = John Berry
|image         = Berry official portrait.jpg
|office        = [[United States Ambassador to Australia]]
|president     = [[Barack Obama]]
|term_start    = September 25, 2013
|term_end      = September 2016
|predecessor   = [[Jeffrey L. Bleich|Jeff Bleich]]
|successor     = TBD
|office1       = Director of the [[United States Office of Personnel Management|Office of Personnel Management]]
|president1    = [[Barack Obama]]
|term_start1   = April 13, 2009
|term_end1     = April 13, 2013
|predecessor1  = [[Kathie Whipple]] {{small|(Acting)}}
|successor1    = [[Elaine D. Kaplan|Elaine Kaplan]] {{small|(Acting)}}
|office2       = Director of the [[National Zoological Park (United States)|National Zoological Park]]
|president2    = [[George W. Bush]]
|term_start2   = October 1, 2005
|term_end2     = April 13, 2009
|predecessor2  = [[Lucy Spelman]]
|successor2    = [[Steven Monfort]] {{small|(Acting)}}<ref>{{cite press release |title=Steven L. Monfort Named Acting Director of the National Zoo |url=http://nationalzoo.si.edu/AboutUs/Staff/Monfortactingdirector.cfm |publisher=[[National Zoological Park (United States)|National Zoological Park]] |year=2009 |accessdate=November 24, 2010}}</ref>
|office3       = Director of the [[National Fish and Wildlife Foundation]]
|president3    = [[Bill Clinton]]<br>[[George W. Bush]]
|term_start3   = 2000
|term_end3     = 2005
|predecessor3  = 
|successor3    = [[Jeff Trandahl]]
|birth_name    = Morrell John Berry
|birth_date    = {{birth date and age|1959|2|10}}
|birth_place   = [[Rockville, Maryland|Rockville]], [[Maryland]], [[United States|U.S.]]
|death_date    = 
|death_place   = 
|party         = [[Democratic Party (United States)|Democratic]]
|spouse        = Curtis Yee
|alma_mater    = [[University of Maryland, College Park]]<br>[[Maxwell School of Citizenship and Public Affairs|Syracuse University]]
}}
'''Morrell John Berry'''<ref name="form">{{cite news|title=Nomination disclosure form|publisher=''National Journal''|date=March 26, 2009|url=http://www.govexec.com/pdfs/032609ar1.pdf|format=PDF|accessdate=March 29, 2009}}</ref> (born February 10, 1959) is an American former government official who was named President of the [[American Australian Association]] in 2016.<ref>{{cite press release |last= |first= |date=June 9, 2016 |title=American Australian Association Announces Amb. John Berry as New President |url=http://www.americanaustralian.org/news/293191/Press-Release-American-Australian-Association-Announces-Amb.-John-Berry-as-New-President.htm |location=New York, NY and Sydney, Australia |publisher=American Australian Association |access-date=December 5, 2016}}</ref> Berry was director of the [[United States Office of Personnel Management]] from 2009 to 2013 and [[United States Ambassador to Australia]] from 2013 to 2016.

Berry was born in [[Montgomery County, Maryland|Montgomery County]], [[Maryland]], to parents who worked for the [[Federal government of the United States|federal government]]. He completed degrees at the [[University of Maryland, College Park]] and [[Syracuse University]] and worked in local government and as a legislative aide in state government from 1982 to 1985. From 1985 to 1994, he worked as [[Legislative assistant|legislative director]] for [[United States House of Representatives|U.S. Representative]] [[Steny Hoyer]]. He held posts in the [[United States Department of the Treasury|U.S. Treasury Department]], the [[Smithsonian Institution]], and the [[United States Department of the Interior|U.S. Department of the Interior]] until 2000, and worked as director of the [[National Fish and Wildlife Foundation]] and the [[National Zoological Park (United States)|National Zoological Park]] until 2009, when he was nominated by President [[Barack Obama]] as director of the [[United States Office of Personnel Management]]. Berry took office after being confirmed by the [[United States Senate]] in April 2009. In June 2013, President Obama nominated Berry to replace [[Jeff Bleich]] as U.S. Ambassador to Australia. He was confirmed by  unanimous consent of the U.S. Senate in August 2013.<ref name=australian/>

==Early life and education==
Berry was born February 10, 1959, in [[Rockville, Maryland|Rockville]], [[Montgomery County, Maryland|Montgomery County]], [[Maryland]], United States.<ref name="form" /><ref name="BB090320">{{cite news|last=Wechsler|first=Pat|author2=Marcus, Aliza|title=Gay Obama Nominee Succeeds as Values Turn on Rescuing Families|publisher=[[Bloomberg Television]]|date=March 20, 2009|url=https://www.bloomberg.com/apps/news?pid=20601109&sid=ayPMVbGij1KY|accessdate=March 20, 2009}}</ref> His father served in the [[United States Marine Corps|U.S. Marine Corps]], his mother worked for the [[United States Census Bureau|U.S. Census Bureau]], and he has a brother and a sister.<ref name="WP090326">{{cite news|last=O'Keefe|first=Ed|title=Berry Sails Through Confirmation Hearing|publisher=''[[The Washington Post]]''|date=March 26, 2009|url=http://voices.washingtonpost.com/federal-eye/2009/03/john_berry.html|accessdate=March 29, 2009}}</ref> Berry graduated from high school in 1977 and finished a [[Bachelor of Arts]] in government and politics from the [[University of Maryland, College Park]] in 1980.<ref name="form" /> In 1981, Berry graduated from [[Syracuse University]] with a [[Master of Public Administration]].<ref name="form" />

==Career==
Berry served in management for the Montgomery County government from 1982 to 1984 and as staff director of the [[Maryland Senate]] Finance Committee from 1984 to 1985.<ref name="form" /> From 1985 to 1994, he was [[Legislative assistant|legislative director]] for [[United States House of Representatives|U.S. Representative]] [[Steny Hoyer]], and associate staffer on the [[United States House Committee on Appropriations|House Appropriations Committee]].<ref name="form" /><ref name="Keefe-Jan">{{cite news|last=O'Keefe|first=Ed|title=Likely OPM Director Earns Rave Reviews|publisher=''The Washington Post''|date=January 13, 2009|url=http://voices.washingtonpost.com/federal-eye/2009/01/new_opm_director_soon.html|accessdate=March 4, 2009}}</ref> Berry assisted Hoyer on employment issues of the [[Federal government of the United States|federal government]], and played a leading role in negotiations that led to the [[Federal Employees Pay Comparability Act of 1990]], which established the locality pay system.<ref name="Keefe-Mar">{{cite news|last=O'Keefe|first=Ed|title=Zoo Director to Head Office of Personnel Management|publisher=''The Washington Post''|date=March 3, 2009|url=http://voices.washingtonpost.com/44/2009/03/03/zoo_director_to_head_office_of.html|accessdate=March 4, 2009}}</ref><ref name="GE090103">{{cite news|last=Rosenberg|first=Alyssa|title=Zoo chief could be named OPM director soon|publisher=''National Journal''|date=January 13, 2009|url=http://www.govexec.com/dailyfed/0109/011309ar1.htm|accessdate=March 29, 2009}}</ref> From 1994 to 1995, Berry served as Deputy Assistant Secretary and acting Assistant Secretary for Law Enforcement in the [[United States Department of the Treasury|U.S. Treasury Department]].<ref name="form" /><ref name="LIT090303">{{cite news |title=Obama Names New FCC, OPM Directors |work=The White House |publisher=[[National Journal]]|date=March 3, 2009 |url=http://lostintransition.nationaljournal.com/2009/03/nominees-for-fcc-chairman-opm.php |accessdate=April 15, 2009}}</ref> From 1995 to 1997, Berry worked as director of government relations and as senior policy advisor at the [[Smithsonian Institution]].

==Department of the Interior==
Berry was appointed Assistant Secretary for Policy, Management and Budget at the [[United States Department of the Interior|U.S. Department of the Interior]] during the [[Presidency of Bill Clinton|Clinton administration]], serving from 1997 to 2001.<ref name="form" /><ref name="Keefe-Jan" /> At the Interior Department, Berry improved credit union and continuing education options, oversaw the expansion of department programs to improve employees' [[work-life balance]], and held [[town hall meeting]]s with Interior employees and used their suggestions to upgrade a cafeteria and health center.<ref name="GE090103" /> These changes were partly funded through partnerships with federal employees, unions and other agencies to reduce costs for the department.<ref name="GE090103" /> Berry worked to create a complaint procedure for employees who experience discrimination because of their [[sexual orientation]], to expand relocation benefits and counseling services to domestic partners of employees, to establish a liaison to gay and lesbian workers, and to eliminate discriminatory provisions of the [[National Park Service]]'s law enforcement standards.<ref name="GE090303">{{cite news|last=Rosenberg|first=Alyssa |title=John Berry named OPM director|publisher=''National Journal''|date=March 3, 2009|url=http://www.govexec.com/dailyfed/0309/030309ar1.htm|accessdate=March 29, 2009}}</ref> He helped establish an office supply store for Interior employees, which he staffed with disabled workers.<ref name="GE090103" /> Berry oversaw one of the largest budgetary increases in the department's history.<ref name="LIT090303" />

In 2000, Berry became director of the [[National Fish and Wildlife Foundation]], where he worked with Interior [[Inspector General]] [[Earl Devaney]] to reconcile twenty years of financial records, improve management, and conserve wildlife habitat through public-private partnerships.<ref name="form" /><ref name="LIT090303" /> Berry was appointed from October 1, 2005, to serve as director of the National Zoo, which had been found to have shortcomings in record keeping and maintenance.<ref name="NYT050729">{{cite news |first=Elizabeth |last=Olson |title=National Briefing - Washington: New Director For National Zoo |date=July 29, 2005 |url=https://query.nytimes.com/gst/fullpage.html?res=9F04E2DD113FF93AA15754C0A9639C8B63 |publisher=''[[The New York Times]]'' |accessdate=December 11, 2008}}</ref><ref>{{cite press release |title=Smithsonian Names John Berry as New Director for the National Zoological Park |url=http://nationalzoo.si.edu/Publications/PressMaterials/PressReleases/NZP/2005/NewZooDirector.cfm |publisher=Smithsonian National Zoological Park |date=July 29, 2005 |accessdate=November 24, 2010}}</ref> Berry created a strategic planning and modernization process for the zoo.<ref name="GE090103" /> This included a twenty-year capital plan, securing $35 million in funding to provide for fire protection, and beginning renovations to animal houses.<ref name="LIT090303" />

The [[Berry Bastion]], an Antarctic mountain, was named in his honor.

==Office of Personnel Management==
[[File:Berry 1st day.jpg|thumb|Berry greeting employees at OPM headquarters on his first day as agency director]]
In 2008, Berry was mentioned as a possible nominee for [[United States Secretary of the Interior|U.S. Secretary of the Interior]], a position obtained by [[Ken Salazar]].<ref>{{cite news|last=Sidoti|first=Liz|author2=Cappiello, Dina|title=Obama chooses team for energy, the environment|date=December 11, 2008|url=http://seattletimes.nwsource.com/html/politics/2008493988_obamaenv11.html|work=[[Associated Press]]|publisher=[[The Seattle Times]]|accessdate=April 15, 2009}}</ref><ref>{{cite press release|title=Ken Salazar Confirmed as 50th Secretary of the Interior|url=http://www.doi.gov/news/09_News_Releases/012009.html|publisher= [[United States Department of the Interior]]|date=January 20, 2009|accessdate=March 29, 2009}}</ref> [[President of the United States|President]] [[Barack Obama]] announced his intention to nominate Berry as director of the [[United States Office of Personnel Management|Office of Personnel Management]] on March 3, 2009,<ref name="Keefe-Mar" /> and did so on March 4.<ref name="OPM090403">{{cite press release |title=John Berry Confirmed As OPM Director |url=http://opm.gov/news/john-berry-confirmed-as-opm-director,1458.aspx |publisher= [[United States Office of Personnel Management]]|date=April 3, 2009 |accessdate=April 4, 2009}}</ref> The nomination hearing before the [[United States Senate Committee on Homeland Security and Governmental Affairs|Senate Homeland Security Committee]] on March 26, 2009, led to expectation of easy confirmation for Berry, despite opposition from conservative activists based on Berry's homosexuality.<ref name="BB090320" /><ref name="WP090326" /> In the hearing Berry stated he supported any effective employee compensation system, but that the federal government had the obligation to give employees with comparable job performances similar pay and treatment.<ref name="GE090326">{{cite news|last=Rosenberg|first=Alyssa|title=Personnel pick remains flexible on pay-for-performance systems|publisher=''National Journal''|date=March 26, 2009|url=http://www.govexec.com/story_page.cfm?articleid=42358|accessdate=March 29, 2009}}</ref> He pledged to preserve [[Veterans' Preference Act|veterans preference]] and supplement it with training programs to prepare veterans for federal jobs, and promised reviews of proposals to improve the [[security clearance]] and hiring processes.<ref name="GE090326" /> Berry emphasized the importance for agencies to use all recruitment tools, citing relocation benefits that could keep agencies competitive with the private sector,<ref name="GE090326" /> and stated he would create a strategic plan and set performance goals for the Office of Personnel Management.<ref name="GE090326" /> Berry had stated support for benefits for same-sex partners of federal employees and a repeal of the [[Defense of Marriage Act]].<ref name="BB090320" />

The Senate confirmed Berry on April 3, 2009,<ref name="OPM090403" /> and he was sworn in April 13 as the first agency director in the Obama administration with all senior staff in place.<ref name="WP090414">{{cite news|last=O'Keefe|first=Ed|title=John Berry Gets to Work|publisher=''The Washington Post''|date=April 14, 2009|url=http://voices.washingtonpost.com/federal-eye/2009/04/john_berry_gets_to_work.html|accessdate=April 15, 2009}}</ref> The ceremonial swearing in on April 23 was attended by [[First Lady of the United States|First Lady]] [[Michelle Obama]].<ref name="WP090423">{{cite news|last=O'Keefe|first=Ed|title=Michelle Obama Visits, John Berry Sworn In at OPM|publisher=''The Washington Post''|date=April 23, 2009|url=http://voices.washingtonpost.com/federal-eye/2009/04/michelle_obama_visits_opm.html|accessdate=April 24, 2009}}</ref> At the time, Berry was, according to the [[Human Rights Campaign]], the highest-ranking openly gay official to serve in the executive branch in any U.S. administration.<ref name="GE090303" />

==U.S. Ambassador to Australia==
In June 2013, President Obama nominated Berry to be the [[United States Ambassador to Australia|US ambassador to Australia]], the first openly gay U.S. ambassador to a [[G-20 major economies|G-20 nation]].<ref name=australian>{{cite news|title=Openly gay man John Berry to be next US ambassador to Australia |url=http://www.theaustralian.com.au/national-affairs/foreign-affairs/openly-gay-man-john-berry-to-be-next-us-ambassador-to-australia/story-fn59nm2j-1226669459721|accessdate=August 13, 2013|newspaper=The Australian|date=June 25, 2013}}</ref> On August 1, 2013, the United States Senate confirmed Berry by unanimous consent.<ref>{{cite news|last=Heil|first=Emily|title=HRC welcomes confirmations of gay candidates|url=http://www.washingtonpost.com/blogs/in-the-loop/post/hrc-welcomes-confirmations-of-gay-candidates/2013/08/02/7befc95e-fb69-11e2-9bde-7ddaa186b751_blog.html|accessdate=August 13, 2013|newspaper=Washington Post|date=August 2, 2013}}</ref> Australian media coverage of Berry's appointment has been overwhelmingly positive with a video he posted to the [[Embassy of the United States, Canberra|US Embassy]] website being described as the "friendliest introduction video in diplomatic history" while Berry himself was described as "modest", with an "impressive record".<ref name=smh>{{cite news|last=Swan|first=Jonathan|title=Barack Obama's chief people person, new US ambassador John Berry, lives up to his reputation|url=http://www.smh.com.au/federal-politics/political-news/barack-obamas-chief-people-person-new-us-ambassador-john-berry-lives-up-to-his-reputation-20130918-2ty9r.html|accessdate=22 September 2013|newspaper=[[Sydney Morning Herald]]|date=18 September 2013}}</ref> [[Federal News Radio]], in the US, reported that, "more than 200 people had posted responses" to the video, "most of which were warm and cordial".<ref name=WFED>{{cite news|last=O'Connell|first=Michael|title=Ambassador John Berry checks in Down Under|url=http://www.federalnewsradio.com/526/3457790/Ambassador-John-Berry-checks-in-Down-Under|accessdate=22 September 2013|newspaper=[[WFED]]|date=19 September 2013}}</ref>

==Personal life==
Before being appointed as ambassador to Australia Berry lived in [[Washington, D.C.]]<ref name="OPM090403" /> Berry is openly [[gay]]. On August 10, 2013, he married Curtis Yee, his partner for 17 years, at St Margaret's Episcopal Church in Washington.<ref>{{cite news|last=Young|first=Matt|title=US ambassador to Australia marries in private same-sex ceremony |url=http://www.news.com.au/lifestyle/relationships/us-ambassador-to-australia-marries-in-private-samesex-ceremony/story-fnet09p2-1226695651201|accessdate=August 13, 2013|newspaper=news.com.au|date=August 12, 2013}}</ref>

==See also==
[[List of LGBT Ambassadors of the United States]]

==References==
{{Reflist|2}}
{{Portal|Biography|International relations|Politics|LGBT|Australia|United States|Maryland|Washington, D.C.}}

==External links==
{{Commons category|John Berry}}
*{{C-SPAN|John Berry 03}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[Kathie Whipple]]<br>{{small|Acting}}}}
{{s-ttl|title=Director of the [[United States Office of Personnel Management|Office of Personnel Management]]|years=2009–2013}}
{{s-aft|after=[[Elaine D. Kaplan|Elaine Kaplan]]<br>{{small|Acting}}}}
|-
{{s-dip}}
{{s-bef|before=[[Jeffrey L. Bleich|Jeff Bleich]]}}
{{s-ttl|title=[[United States Ambassador to Australia]]|years=2013–2016}}
{{s-aft | after = TBD}}
{{s-end}}

{{DEFAULTSORT:Berry, John}}
[[Category:1959 births]]
[[Category:Ambassadors of the United States to Australia]]
[[Category:Directors of the United States Office of Personnel Management]]
[[Category:LGBT ambassadors of the United States]]
[[Category:LGBT people from the United States]]
[[Category:Living people]]
[[Category:Gay men]]
[[Category:Maryland Democrats]]
[[Category:Obama administration personnel]]
[[Category:People from Rockville, Maryland]]
[[Category:Smithsonian Institution people]]
[[Category:Syracuse University alumni]]
[[Category:United States Assistant Secretaries of the Interior]]
[[Category:United States Department of the Treasury officials]]
[[Category:University of Maryland, College Park alumni]]