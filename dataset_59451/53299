{{Infobox college athletics
| name = NC State Wolfpack
| logo = North Carolina State University Athletic logo.svg
| logo_width = 120
| university = [[North Carolina State University]]
| association = NCAA
| conference = [[Atlantic Coast Conference]], [[East Atlantic Gymnastics League]], [[Great America Rifle Conference]]
| division = [[NCAA Division I|Division I]]/[[NCAA Division I Football Bowl Subdivision|FBS]]
| director = [[Debbie Yow]]
| city = [[Raleigh, North Carolina|Raleigh]]
| state = [[North Carolina]]
| teams = 23
| stadium = [[Carter-Finley Stadium]]
| basketballarena = [[PNC Arena]]
| baseballfield = [[Doak Field]]
| arena2 = [[Reynolds Coliseum]]
| mascot = Mr. Wuf & Mrs. Wuf
| nickname = Wolfpack
| pageurl = http://gopack.com/
}}

The '''NC State Wolfpack''' are the athletic teams representing [[North Carolina State University]]. They compete as a member of the [[NCAA Division I]] level (Football Bowl Subdivision (FBS) sub-level for football), primarily competing in the [[Atlantic Coast Conference]] (ACC) for all sports since the 1953–54 season. Men's sports include baseball, basketball, cheerleading, cross country, football, golf, soccer, swimming and diving, tennis, track and field, and wrestling. Women's sports include basketball, cheerleading, cross country, golf, gymnastics, soccer, softball, swimming and diving, tennis, track and field, and volleyball. Rifle is a co-ed team.

The athletic teams of the Wolfpack compete in 23 intercollegiate varsity sports. NC State is a founding member of the [[Atlantic Coast Conference]] and has won eight national championships: two NCAA championships, two [[AIAW]] championships, and four titles under other sanctioning bodies. Most NC State fans and athletes recognize the [[Carolina-NC State rivalry|rivalry with the North Carolina Tar Heels]] as their biggest.

The primary logo for NC State athletics is a red block 'S' with an inscribed 'N' and 'C'. The block S has been in use since 1890 but has seen many alterations through the years. The color red was adopted from the state bird, the cardinal. It became the sole logo for all NC State athletic teams in 2000 and was modernized to its current design in 2006.

NC State athletic teams are nicknamed the 'Wolfpack'. The name was adopted in 1921 when a disgruntled fan described the behavior of some of the school's football players as being "as unruly as a pack of wolves".<ref name=nickname>{{cite web|title=Welcome to Wolfpack Awareness Week|url=http://www.gopack.com/genrel/101810aab.html|work=GoPack.com|accessdate=3 July 2012|archiveurl=http://www.webcitation.org/68tX3GdRc|archivedate=2012-07-03|date=18 October 2010}}</ref>  Prior to the adoption of the current nickname, North Carolina State athletic teams went by such names as the Aggies, the Techs, the Red Terrors, and Farmers.<ref>http://grfx.cstv.com/photos/schools/ncst/sports/m-footbl/auto_pdf/history1-10.pdf</ref> Since the 1960s the Wolfpack has been represented at athletic events by its mascots, Mr. and Mrs. Wuf who were married on February 28, 1981 by the Demon Deacon at Reynolds Coliseum at half time of an NC State game versus Wake Forest. The Demon Deacon presided over the wedding.<ref>http://news.lib.ncsu.edu/scrc/2011/02/14/mr-and-mrs-wuf-got-married/</ref> In print, the "Strutting Wolf" is used and is known by the name "Tuffy." In September 2010, a purebred [[Tamaskan dog]] became the new  live mascot, "Tuffy".<ref name="Tuffy's mascot tryout at game pleases N.C. State officials">[http://www.newsobserver.com/2010/09/18/687991/tuffys-tryout-at-game-pleases.html Tuffy's mascot tryout at game pleases N.C. State officials] September 2010</ref><ref name="NC State to use Tamaskan Dog for Mascot">[http://www.dailytarheel.com/index.php/blog/on_the_wire/2010/09/nc_state_to_use_tamaskan_dog_for_mascot NC State to use Tamaskan Dog for Mascot] September 2010</ref><ref name="The Cutest Live College Mascots">[http://www.peoplepets.com/photos/cute/the-cutest-live-college-mascots/5 The Cutest Live College Mascots] September 2010</ref><ref name="Tuffy Tamaskan">[http://www.facebook.com/group.php?gid=115054155219579 Tuffy Tamaskan] September 2010</ref><ref name="Tuffy's Page">[http://www.gopack.com/graphics/html_files/tuffy-page/index.html Tuffy's Page] October 2010</ref>

==Baseball==
{{main article|NC State Wolfpack baseball}}
{{see also|2015 NC State Wolfpack baseball team}}
*Head Coach: [[Elliott Avent]]
*Stadium: [[Doak Field]]
*ACC Championships: 5 (1968, 1973, 1974, 1975, 1992)
*CWS appearances: 2 (1968, 2013)

==Men's basketball==
{{main article|NC State Wolfpack men's basketball}}
*Head Coach: [[Kevin Keatts]]
*Arena: [[PNC Arena]]
*National Championships: 2 ([[1974 NCAA Men's Division I Basketball Tournament|1974]], [[1983 NCAA Men's Division I Basketball Tournament|1983]])
*Southern Conference Championships: 7 (1929,1947,1948,1949,1950,1951,1952)
*ACC Championships: 10 (1954, 1955, 1956, 1959, 1965, 1970, 1973, 1974, 1983, 1987)
''The above record of conference titles does not include regular season 1st place finishes as championships – the ACC recognizes only the winner of the ACC Tournament as its champion.''

==Women's basketball==
{{main article|NC State Wolfpack women's basketball}}
{{see also|2014–15 NC State Wolfpack women's basketball team}}
*Head Coach: [[Wes Moore (basketball)|Wes Moore]]
*Arena: [[Reynolds Coliseum]]
*ACC Championships: 1978, 1980, 1983, 1985, 1990 (regular season); 1980, 1985, 1987, 1991 (ACC Tournament)

==Football==
{{main article|NC State Wolfpack football}}
{{see also|2015 NC State Wolfpack football team}}
*Head Coach: [[Dave Doeren]]
*Stadium: [[Carter-Finley Stadium]]
*ACC Championships: 7 (1957, 1963, 1964, 1965, 1968, 1973, 1979)
*Southern Conference Championships: 1 (1927)
*South Atlantic Intercollegiate Championships: 3 (1907, 1910, 1913)
*Bowl games: 26 (14-11-1)

==Wrestling==
North Carolina State University's wrestling team was established in 1925 and goes by the team nickname of the "Wolfpack". Pat Popolizio was named head wrestling coach for the Wolfpack on April 10, 2012. Currently in his second  season with the team, Popolizio is a three-time NCAA qualifier. The wrestling team competes at home on campus in the [[Reynolds Coliseum]].<ref>{{cite web|title=NCSU Wrestling Facts|url=http://grfx.cstv.com/photos/schools/ncst/sports/m-wrestl/auto_pdf/2011-12/quick_facts/quick_facts.pdf|publisher=North Carolina State University Athletics|accessdate=2014-01-01}}</ref> NC State Wrestling also has a blog online mainly to keep up to date with the team and focus on the NCAA Championships.<ref>{{cite web|title=NCSU Wrestling Blog|url=http://wolfpackwrestling.blogspot.com/|publisher=NCSU Wrestling Blog|accessdate=2014-01-01}}</ref>

In 2012, Popolizio left his previous program, Binghamton University, and took all-American heavyweight Nick Gwiazdowski with him. After redshirting for a year (to avoid sitting out a season per NCAA transfer rules), Gwiazdowski won national titles in 2014 and 2015, becoming the first Wolfpack wrestler to win consecutive titles.

During the 2015–16 season, North Carolina State went as high as number two in the national rankings and had the school record for most wins in a single season.

'''NC State Wolfpack Wrestling achievements''':
*11 Academic All-Americans
*15 [[Atlantic Coast Conference|ACC]] Championships
*28 NCAA All-Americans
*6 individual NCAA Champions: (1980) Matt Reiss 167, (1984) Tab Thacker Hwt., (1988) Scott Turner 150, (1993) [[Sylvester Terkay]] Hwt., (2009) Darrion Caldwell 149, (2014, 2015) Nick Gwiazdowski Hwt

==Other sports==
In addition to baseball, basketball, football, and wrestling, NC State competes in 11 other varsity sports. Unless noted, both a men's and a women's team are fielded.

*[[Cross Country Running|Cross country]]: 2 [[Association for Intercollegiate Athletics for Women championships#Cross Country|AIAW Women's National Championships]] (1979, 1980); 20 Women's ACC Championships
*[[Golf]]: 1990 Men's ACC co-champions, [[Matt Hill (golfer)|Matt Hill]] – [[2009 NCAA Division I Men's Golf Championship|2009]] [[NCAA Division I Men's Golf Championships|NCAA Champion]]
*[[Gymnastics]] (women's only)
*[[Shooting sports|Rifle]]
*[[Soccer]] (Men's): 13 NCAA appearances (1 Semifinal appearance: 1990); 1990 ACC Tournament Champions
*[[Women's soccer]]: 1988 NCAA Finalist; 1988 ACC Regular Season and Tournament Champions; 11 NCAA appearances (9-10-3 NCAA Tournament Record); 7 All-Americans; three former U.S. National Team players
*[[Softball]] (women's only): 2 Women's ACC Championship (2006, 2013)
*[[Swimming (sport)|Swimming]] & [[diving]]: 26 Men's ACC Champions; 72 Men's All-Americans; 5 Men's Individual National Champions; Kristen Davies – 2009 NCAA Women's Platform Diving Champion<ref>http://fs.ncaa.org/Docs/NCAANewsArchive/2009/Division+I/california_staves_off_georgia_charge_in_women_s_swimming_03_23_09_ncaa_news.html</ref>
*[[Tennis]]: notable former players: Christian Welte
*[[Track and field|Track & field]]: 6 Men's ACC Championships
*[[Volleyball]] (women's only)

==Non-varsity sports==
The North Carolina State University Men's Rugby Football Club was founded in 1965.<ref>Men's Rugby Club at NCSU, Home, http://www.ncstaterugby.com/</ref> NC State plays [[college rugby]] in the [[Atlantic Coast Rugby League]] against its traditional ACC rivals.  The NC State rugby team is led by head coach Jim Latham.<ref>Men's Rugby Club at NCSU, Coaches, http://www.ncstaterugby.com/officers/</ref> The Wolfpack play their home games at the Upper Method Road Field.
NC State won the Atlantic Coast Invitational 7s tournament in 2010 and 2011.<ref>Atlantic Coast Rugby League, Home, http://www.atlanticcoastrugby.com/index.html</ref> The Wolfpack finished 13th at the 2011 [[USA Rugby Sevens Collegiate National Championships]].  NC State finished 12th at the 2012 [[Collegiate Rugby Championship]], a tournament broadcast live on NBC from [[PPL Park]] in Philadelphia. NC State scored a notable upset against #7 ranked Davenport to reach the finals of the 2012 ACI 7s tournament in Blacksburg, only to lose in the final to host Virginia Tech.<ref>Rugby Mag, Virginia Tech Wins ACI Opener, Sep. 15, 2012, http://rugbymag.com/news/colleges/collegiate-sevens/5788-virginia-tech-wins-aci-opener.html</ref>

As the university's oldest active club, the NC State Sailing Club was founded in 1954 and also fields a competitive intercollegiate co-ed and a women's sailing team. With their home facility at Lake Wheeler<ref>[http://www.raleighnc.gov/arts/content/PRecRecreation/Articles/LakeWheeler.html Lake Wheeler]</ref> in Raleigh, the "SailPack,"<ref>[http://www.sailpack.org | SailPack]</ref> as the club is known, competes in the South Atlantic Intercollegiate Sailing Association<ref>[http://classic.collegesailing.org/saisa/ SAISA]</ref> division of the Intercollegiate Sailing Association ([[Intercollegiate Sailing Association|ICSA]]). The ICSA is the governing body of college sailing. The SailPack has reached the SAISA regional championship consecutively for the past four seasons, and as of 2013 is the highest ranking active program in North Carolina ahead of Duke, UNC-Chapel Hill, UNC-Wilmington, and Davidson.<ref>{{cite web|url=http://www.sailpack.org|title=SailPack Foundation|work=SailPack Foundation}}</ref>

NC State's ski team is a member of the [[United States Collegiate Ski and Snowboard Association]] (USCSA) and competes in races regularly during the winter season.

NC State fields a full varsity team in [[cheerleading]] and competes in dance, neither of which are sanctioned by the NCAA.

NC State has a proud intercollegiate tradition in [[Ultimate (sport)|ultimate frisbee]]. The men's team won the national championships in 1999.

==Championships==
===NCAA team championships===

North Carolina State has won 2 NCAA team national championships.<ref>http://fs.ncaa.org/Docs/stats/champs_records_book/Overall.pdf</ref>

*'''Men's (2)'''
**[[NCAA Men's Division I Basketball Championship#Team titles|Basketball]] (2): 1974, 1983
*see also:
**[[Atlantic Coast Conference#NCAA team championships|ACC NCAA team championships]]
**[[List of NCAA schools with the most NCAA Division I championships#NCAA Division I Team Championships|List of NCAA schools with the most NCAA Division I championships]]

==Notable former athletes==
{{div col|2|colwidth=30em}}
* [[Nazmi Albadawi]], men's soccer (2010–13)
* [[Andy Barkett]], baseball (1992–95)
* [[Brian Bark]], baseball (1987–90)
* [[Aaron Bates]], baseball (2005–06)
* [[Andrew Brackman]], baseball and men's basketball (2005–08)
* [[Greg Briley]], baseball (1986)
* [[Andre Brown (running back)|Andre Brown]], football
* [[Chucky Brown]], basketball (1985–89)
* [[Jimmy Brown (baseball)|Jimmy Brown]], baseball (1932)
* [[Ted Brown (American football)|Ted Brown]], football (1975–78)
* [[Tommy Burleson]], men's basketball (1972–74)
* [[Dick Burrus]], baseball (1919)
* [[Mike Caldwell (baseball)|Mike Caldwell]], baseball (1968–71)
* [[Kenny Carr]], men's basketball (1975–77)
* [[Lorenzo Charles]], men's basketball (1982–85)
* [[Tim Clark (golfer)|Tim Clark]], men's golf (1996–97)
* [[Chris Colmer]], football (2002–05)
* [[Chris Corchiani]], men's basketball (1988–91)
* [[Jerricho Cotchery]], football (2000–04)
* [[Bill Cowher]], football (1977–79)
* [[Doug Davis (infielder)|Doug Davis]], baseball (1982–84)
* [[Joe DeBerry]], baseball (1917–20)
* [[Vinny Del Negro]], men's basketball (1983–1987)
* [[Joey Devine]], baseball (2003–05)
* [[Bill Evans (1910s pitcher)|Bill Evans]], baseball (1915)
* [[Adam Everett]], baseball (1996)
* [[Stu Flythe]], baseball (1934–36)
* [[David Fox (swimmer)|David Fox]], men's swimming & diving (1990–94)
* [[Roman Gabriel]], football (1960–62)
* [[Mike Glennon]], football (2008–12)
* [[Tom Gugliotta]], men's basketball (1989–92)
* [[Jeff Hartsock]], baseball (1986–88)
* [[J.J. Hickson]], men's basketball (2007–08)
* [[Julius Hodge]], men's basketball (2001–05)
* [[Dutch Holland]], baseball (1923–25)
* [[Torry Holt]], football (1995–98)
* [[Charmaine Hooper]], women's soccer (1987–90)
* [[Cullen Jones]], men's swimming & diving (2002–06)
* [[Erik Kramer]], football (1985–87)
* [[Johnny Lanning]], baseball (1931–32)
* [[Manny Lawson]], football (2002–05)
* [[Corey Lee]], baseball (1994–96)
* [[Sidney Lowe]], men's basketball (1980–83)
* [[Matt Mangini]], baseball (2005–06)
* [[Pablo Mastroeni]], men's soccer (1995–98)
* [[Joe McIntosh (American football)|Joe McIntosh]], football (1981–84)
* [[Nate McMillan]], men's basketball (1985–86)
* [[Jim McNamara]], baseball (1984–86)
* [[Louie Meadows]], baseball (1980–82)
* [[Rodney Monroe]], men's basketball (1988–91)
* [[George Murray (baseball)|George Murray]], baseball (1918–21)
* [[Jessica O'Rourke]], women's soccer (2004–07)
* [[Chad Orvella]], baseball (2002–03)
* [[Chink Outen]], baseball (1927–28)
* [[Jeff Pierce (baseball)|Jeff Pierce]], baseball (1990–91)
* [[Dan Plesac]], baseball (1981–83)
* [[Mike Quick]], football (1978–1981)
* [[Tab Ramos]], men's soccer (1984–87)
* [[Buck Redfern]], baseball (1921–24)
* [[Jim Ritcher]], football (1976–1979)
* [[Philip Rivers]], football (2000–04)
* [[Dave Robertson]], baseball (1910–12)
* [[Koren Robinson]], football (1999–2001)
* [[Carlos Rodon]], baseball (2012–14)
* [[Ronnie Shavlik]], men's basketball (1954–56)
* [[Tommy Smith (baseball)|Tommy Smith]], baseball (1972–74)
* [[Tim Stoddard]], baseball (1972–75)
* [[Doug Strange]], baseball (1983–85)
* [[Eric Surkamp]], baseball (2006)
* [[Craig Sutherland]], men's soccer (2010–11)
* [[Sylvester Terkay]], wrestling (1991–93)
* [[David Thompson (basketball)|David Thompson]], men's basketball (1973–75)
* [[Monte Towe]], men's basketball (1972–75)
* [[Trea Turner]], baseball (2012–14)
* [[TJ Warren]], men's basketball (2012–14)
* [[Spud Webb]], men's basketball (1984–85)
* [[Mario Williams]], football (2003–05)
* [[Adrian Wilson (American football)|Adrian Wilson]], football (1997–01)
* [[Russell Wilson]], football (2007–10), baseball (2008–10)
* [[Tracy Woodson]], baseball (1982–84)
{{div col end}}

==NC State Fight Song==
The words to the Fight Song were written by Hardy Ray, Class of 1926, and the music was written by [[Edmund L. Gruber]] in 1908.<ref name="FightSong">[http://www.ncsu.edu/nso/traditions/athletics/fightsong.htm NC State Fight Song]</ref> It is essentially a sped-up version of "[[The Caisson Song]]", or more recently, "[[The Army Goes Rolling Along]]."<ref name="Army">[http://www.1perscom.army.mil/band/mp3/army_song_long.mp3 United States Army Europe Band – The Army Goes Rolling Along (mp3)]</ref>

==Red and White Song==
The Red and White Song is a popular song sung by fans and played by the band at many NC State athletic events, especially at football and basketball games. It was written by J. Perry Watson, a former Director of Music at NC State, and was introduced in 1961; students first sang the "Red and White" song at the NC State – Maryland game on February 13, 1961.<ref>{{cite news |url=http://www.lib.ncsu.edu/specialcollections/|newspaper=Technician|date=9 February 1961|title=Special Collections Research Center|location=NCSU Libraries Special Collections Research Center, Raleigh}}</ref>  The song, although very popular, is in fact not the official Fight Song of NC State.<ref name="Redwhite">[http://www.ncsu.edu/nso/traditions/athletics/redandwhite.htm NC State Traditions – Red and White Song]</ref> The colors mentioned in the song refer to NC State's main athletic colors, while "Caroline", "Devils", and "Deacs" refer to other [[Tobacco Road]] team names: [[North Carolina Tar Heels]], [[Duke Blue Devils]], and [[Wake Forest Demon Deacons]].

==The NC State Alma Mater==
NC State's Alma Mater was written by two students in the early 1920s. Dr. Alvin M. Fountain, a class of '22 alumnus and editor of ''The Technician'', wrote the words, while Bonnie Norris, from the class of 1923, composed the music.<ref name="almamater">http://www.ncsu.edu/nso/traditions/athletics/almamater.htm</ref>

==See also==
{{Portal|ACC}}
* [[List of college athletic programs in North Carolina]]
* [[Carolina–State Game|NC State–Carolina rivalry]]
* [[Textile Bowl]]
* [[East Carolina–NC State rivalry|NC State–East Carolina rivalry]]

==References==
{{Reflist|30em}}

==External links==
* {{Official website}}

{{North Carolina State University}}
{{Navboxes
|titlestyle = {{CollegePrimaryStyle|NC State Wolfpack|color=white}}
|list =
{{Atlantic Coast Conference navbox}}
{{East Atlantic Gymnastics League navbox}}
{{Great America Rifle Conference navbox}}
{{North Carolina Sports}}
}}

[[Category:NC State Wolfpack|*]]