{{Infobox journal
| cover = [[File:CMMI new cover.gif]]
| discipline = [[Chemistry]]
| abbreviation = Contrast Media Mol. Imaging
| publisher = [[John Wiley & Sons]]
| country = 
| history = 2006-present
| frequency = Bimonthly
| website = http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1555-4317
| ISSN = 1555-4309
| eISSN = 1555-4317
| LCCN = 2005212234
| OCLC = 58749692
}}
'''''Contrast Media & Molecular Imaging''''' is a bimonthly [[peer review|peer-reviewed]] [[scientific journal]] published by [[John Wiley & Sons]] since 2006. It covers the areas of [[magnetic resonance imaging]] and [[magnetic resonance spectroscopy]], but also all other in vivo imaging technologies such as x-Ray, PET/CT, etc. The current [[editor-in-chief|editors-in-chief]] are [[Silvio Aime]] ([[University of Turin]]) and [[Robert N. Muller]] ([[University of Mons-Hainaut]]).<ref name="onlinelibrary">[http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1555-4317/homepage/EditorialBoard.html Wiley Online Library]</ref>

== Most-cited papers ==
The three most-cited papers are:
# 'Comparative study of the physicochemical properties of six clinical low molecular weight gadolinium contrast agents', Volume 1, Issue 3, May-Jun 2006, Pages: 128–137, Laurent S, Elst LV, Muller RN
# 'Long-term retention of gadolinium in tissues from nephrogenic systemic fibrosis patient after multiple gadolinium-enhanced MRI scans: case report and implications', Volume 2, Issue 4, Jul-Aug 2007, Pages: 199-205, Thakral C, Alhariri J, Abraham JL
# 'How to determine free Gd and free ligand in solution of Gd chelates. A technical note', Volume 1, Issue 5, Sep-Oct 2006, Pages: 184-188, Barge A, Cravotto G, Gianolio E, et al.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[SciFinder]], [[Scopus]], and [[Web of Science]].<ref name="onlinelibrary"/> The 2014 [[impact factor]] is 2.923. It is ranked 27th out of 125 journals in the category "Radiology, Nuclear Medicine and Medical Imaging".<ref>Institute for Scientific Information, ''Journal Citation Reports'', 2015</ref>

== See also ==
* [[Molecular imaging]]

== References ==
<references />

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1002/(ISSN)1555-4317}}

{{DEFAULTSORT:Contrast Media and Molecular Imaging}}
[[Category:Chemistry journals]]
[[Category:John Wiley & Sons academic journals]]
[[Category:Publications established in 2006]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]