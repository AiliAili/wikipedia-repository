<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Dornier 328
 |image= File:Sun-Air Do-328.jpg
 |caption= [[British Airways]] Dornier 328
}}{{Infobox Aircraft Type
 |type= [[Airliner]]
 |national origin = [[Germany]]
 |manufacturer= [[Dornier Flugzeugwerke|Dornier]],<br>[[Fairchild Aircraft|Fairchild-Dornier]]
 |designer=
 |first flight= 6 December 1991
 |introduced= October 1993
 |retired=
 |status= Out of production, in service
 |primary user= [[Loganair]]
 |more users= [[Air Alps]] <br/>[[Sun Air of Scandinavia]]<!--up to three more. please separate with <br/>.-->
 |produced= 1991-2000
 |number built= 217
 |unit cost= $8,000,000<ref name = "describe 33">Moxon, Barrie and Goold 1991, p. 33.</ref>
 |developed from =
 |variants with their own articles=
 |developed into= [[Fairchild Dornier 328JET]]
}}
|}

The '''Dornier 328''' is a [[turboprop]]-powered commuter airliner. Initially produced by [[Dornier Flugzeugwerke|Dornier Luftfahrt GmbH]], the firm was acquired in 1996 by [[Fairchild Aircraft]]. The resulting firm, named [[Fairchild (aircraft manufacturer)|Fairchild-Dornier]], manufactured the 328 family in [[Oberpfaffenhofen]], [[Germany]], conducted sales from [[San Antonio, Texas]], [[United States]], and supported the product line from both locations. There is also a jet-powered version of the aircraft, the [[Fairchild Dornier 328JET]].

==Development==

===Origins===
The Dornier 328 (or Do 328) program was initially started while Dornier was still owned by [[DaimlerChrysler Aerospace|Deutsche Aerospace]]. According to Deutsche Aerospace program manager Reinhold Birrenbach, the 328 had its origins in market research conducted in and around 1984; feedback from airlines indicated a desire for a fast, quiet and easy to maintain commuter airliner with a 30-seat capacity.<ref name = "high 69">Moxon 1991, p. 69.</ref> This market research reportedly led Deutsche Aerospace to formulate a sales prediction of 400 or greater units being purchased overall; this forecast was in part derived from the reasoning that the 328 would be more advanced than its nearest competitors.<ref>Moxon, Barrie and Goold 1991, pp. 31-32.</ref> Favourable features included a high cruising speed of 345 kt (640&nbsp;km/h) as well as a higher cruising altitude and range, making the aircraft almost as fast as jet airliners while being more fuel-efficient; a trend away from [[spoke–hub distribution paradigm|spoke–and-hub distribution]] in favour of [[point-to-point transit]] was also viewed as being favourable to the 328.<ref name = "describe 32-33">Moxon, Barrie and Goold 1991, pp. 32-33.</ref>

In December 1988, the 328 project was re-launched following the granting of shareholder approval after negotiations between the Dornier family and Daimler Benz.<ref name = "high 69"/><ref name = "describe 32"/> As the result of a six-month evaluation, a selection of powerplants deemed to be appropriate for the 328 was formed, these being the [[General Electric T700|General Electric CT7-9D]], the [[Garrett TPE331|Garrett TPE-341-21]] and the [[Pratt & Whitney Canada PW100|Pratt & Whitney Canada PW119A]]. While the Garrett engine was viewed by Deutsche Aerospace as being technically superior, Pratt & Whitney's powerplant was more advanced in development and thus was chosen.<ref name = "describe 34">Moxon, Barrie and Goold 1991, p. 34.</ref> The engine selection was soon followed by the selection of a six-bladed [[composite material|composite]] propeller from [[Hartzell]], Hartzell's submission being reportedly substantially lighter than competing bids from [[Dowty Rotol|Dowty]] and [[Hamilton Standard]].<ref name = "describe 34"/> Following various considerations between [[electromechanics|electromechanical]] and [[digital electronics|digital]] instrumentation, Dornier opted for a digital [[glass cockpit]] and selected [[Honeywell]] to provide this after considering options from [[Saab Group]], [[Rockwell Collins]], and [[GE Aviation Systems|Smiths Aerospace]].<ref name = "describe 38">Moxon, Barrie and Goold 1991, p. 38.</ref>

In May 1991, [[Horizon Air]], a US-based airline, placed an order for 35 aircraft, this was the largest order for the 328 at that point and was larger than any other order for it or its competitors to be placed that year.<ref name = "describe 32">Moxon, Barrie and Goold 1991, p. 32.</ref> In October 1991, the first prototype of the 328 was formally rolled out.<ref name = "describe 31">Moxon, Barrie and Goold 1991, p. 31.</ref> On 6 December 1991, the first prototype conducted the type's [[maiden flight]].<ref name="Daimler p123">Swanborough 1992, p. 123.</ref> On 4 June 1992, a second 328 prototype performed its first flight.<ref>[https://www.flightglobal.com/pdfarchive/view/1992/1992%20-%201541.html "Second Dornier 328 Joins Test Programme."] ''Flight International'', 23 June 1992. p. 9.</ref> On 14 December 1992, one of the 328 prototypes suffered a near-catastrophic in-flight propeller failure when all six propeller blades on one engine detached before puncturing the fuselage, the subsequent temporary loss of control caused the aircraft to roll 280 degrees and descent 5,000 feet before control was recovered.<ref>Barrie, Douglas. [https://www.flightglobal.com/pdfarchive/view/1993/1993%20-%200006.html "Dornier 328 prototype hit by propeller failure."] ''Flight International'', 12 January 1993. p. 4.</ref>

On 13 October 1993, the 328 formally entered commercial service.<ref name="Brasseys 99 p195">Taylor 1999, p. 195.</ref><ref>[https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%202570.html "Premiere: Dornier 328."] ''Flight International'', 8 October 1991. p. 28.</ref> The 328 was launched onto the market during a period in which it was crowded with large numbers of competing turboprop aircraft, as well as facing increasing competition from newly launched regional jets, which were becoming increasingly popular during the early 1990s.<ref name = "ref bus">[http://www.referenceforbusiness.com/history2/76/Fairchild-Dornier-Gmbh.html "Fairchild Dornier Gmbh Company Profile, Information, Business Description, History, Background Information on Fairchild Dornier Gmbh."] ''Reference for Business'', Retrieved: 5 May 2008.</ref><ref name = "berlin 1992"/><ref>[https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%203148.html "DASA warns of clash in regional launch plans."] ''Flight International'', 10 December 1991. p. 6.</ref> The 328 had the advantages of being both quieter and faster than many of its rivals, however this did not ensure its commercial success.<ref name = "ref bus"/> The latter half of the 328 program was performed during a recession, curtailing demand for new aircraft from operators.<ref name = "describe 32"/> Both the 328 and the wider Dornier division of Deutsche Aerospace both proved to be losing money; accordingly Deutsche Aerospace wavered in committing more resources to the regional aircraft market, repeatedly delaying a decision to proceed with a 48-seat stretched model of the 328, which had originally been unveiled in 1991.<ref>Barrie, Douglas. [https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%202730.html "Domier details a stretched Do.328."] ''Flight International'', 29 October 1991. pp. 4-5.</ref><ref>[https://www.flightglobal.com/pdfarchive/view/1993/1993%20-%201017.html "DASA dithers on Dornier 328 stretch."] ''Flight International'', 11 May 1993. p. 10.</ref><ref>[https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%201429.html "Buyers demand stretched Dornier 328."] ''Flight International'', 11 June 1991. p. 5.</ref>

===Further development===
During the early 1990s, Deutsche Aerospace and [[Fokker]] explored the prospects of a commercial relationship to mutually engage the regional aircraft market;<ref name = "berlin 1992">[https://www.flightglobal.com/pdfarchive/view/1992/1992%20-%201335.html "Decisions, Decisions."] ''Flight International'', 2 June 1992. pp. 35, 38.</ref> this cumulated in Deutsche Aerospace purchasing a 40 per cent stake in Fokker in 1993.<ref name = "Fokker troub"/> In June 1995, Deutsche Aerospace and [[Daewoo Heavy Industries]] were reportedly conducting talks on the establishment of a second 328 assembly line in [[South Korea]] for the Asian market.<ref>Jeziorski, Andrzej. [https://www.flightglobal.com/news/articles/dasa-in-talks-over-korean-328-deal-production-25682/ "DASA in talks over Korean 328 deal production."] ''Flight International'', 14 June 1995.</ref> In 1995, both Fokker and Deutsche Aerospace suffered substantial financial difficulties, which ultimately led to the abortion of the latter's ambitions to dominate the European regional aircraft market.<ref>Jeziorski, Andrzej. [https://www.flightglobal.com/news/articles/dasa-dealt-double-blow-24457/ "DASA dealt double blow."] ''Flight International'', 23 August 1995.</ref><ref name = "Fokker troub">Jeziorski, Andrzej. [https://www.flightglobal.com/news/articles/dasa-folds-its-wings-19453/ "DASA folds its wings."] ''Flight International'', 31 January 1996.</ref> In June 1996, Deutsche Aerospace sold the majority of Dornier Luftfahrt GmbH to American manufacturer [[Fairchild Aircraft]], leading to the creation of Fairchild Dornier GmbH.<ref name = "ref bus"/> The newly combined Fairchild Dornier company emerged as the third largest regional aircraft manufacturer in the world, and viewed the 328 as being both a key product in its lineup and the basis for a future family of aircraft.<ref name = "ref bus"/>

In addition to continuing development of a jet-powered variant of the 328, initially designated as the ''Dornier 328-300'' and later simply known as the [[Dornier 328JET]], which had been started under Deutsche Aerospace; Fairchild Dornier sought to develop a stretched version, designated as the [[Dornier 428JET]], and a dedicated freighter model.<ref name = "ref bus"/><ref>[https://www.flightglobal.com/pdfarchive/view/1996/1996%20-%202413.html "Dornier sets jet date."] ''Flight International'', 17 September 1996. p. 9.</ref><ref>Doyle, Andrew. [https://www.flightglobal.com/pdfarchive/view/1999/1999%20-%202789.html "Regional Aviation: Joining the Jet Age."] ''Flight International'', 28 September 1999.</ref> Further derivatives of the 328 included the shortened ''Dornier 528'', the further stretched [[Fairchild Dornier 728 family|Dornier 728JET]] and the significantly enlargened [[Fairchild Dornier 728 family|Dornier 928JET]]. In addition to typical passenger models, [[business jet]] configurations of both the 728JET and 928JET were projected, tentatively referred to as ''Envoy 3'' and ''Envoy 7'' respectively.<ref name = "ref bus"/> The ambitious family project drew the support of the German government, who guaranteed $350 million-worth of loans for the scheme.<ref name = "ref bus"/> During the late 1990s, Fairchild Dornier struggled to find both capital and strategic partners to support the project, and the company ultimately entered [[bankruptcy]] in April 2002.<ref name = "ref bus"/>

[[File:Dornier 328 (304650241).jpg|thumb|A Dornier 328, 2006]]
Following the bankruptcy of Fairchild Dornier, the rights to the Dornier 328 and its 328JET cousin were acquired by [[AvCraft Aviation]], however this company entered bankruptcy itself less than three years later.<ref>Sarsfield, Kate. [https://www.flightglobal.com/news/articles/back-to-life-nine-civil-types-revived-410656/ "Back to life: nine civil types revived."] ''Flight International'', 27 Match 2015.</ref><ref name = "dan 2015"/> In June 2006, 328 Support Services GmbH acquired the [[type certificate]] for the Dornier 328 but did not pursue production of the type, instead electing to focus on providing maintenance, repair, and overhaul services to the existing in-service fleet.<ref>{{cite web |url=http://www.328.eu/index.php |title=328 Support Services GmbH |work=328.eu |accessdate=7 March 2016}}</ref><ref>[http://rgl.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/40010B67B85AAA138625769B005571AD?OpenDocument&Highlight=no. a45nm "FAA Type Certificate Data Sheet No. A45NM, Revision 7."] ''Federal Aviation Administration'', Retrieved: 3 July 2016.</ref> In February 2015, 328 Support Services GmbH was itself acquired by the US engineering company [[Sierra Nevada Corporation]].<ref>[http://www.sncorp.com/AboutUs/NewsDetails/781 "Sierra Nevada Corporation Expands into Global Aircraft Markets through Investments in UK and Germany - SNC Acquires 328 Support Services GmbH."] ''Sierra Nevada Corporation'', 5 February 2015.</ref><ref name = "dan 2015">Thisdell, Dan. [https://www.flightglobal.com/news/articles/new-era-for-dornier-328-with-sierra-nevada-acquisiti-409179/ "New era for Dornier 328 with Sierra Nevada acquisition."] ''Flight International'', 18 February 2015.</ref> Shortly thereafter, Seirra Nevada's owner, Turkish-American Engineer [[Fatih Ozmen]], established a private corporation named Özjet Havacılık Teknolojileri A.Ş. at Technopark of [[Bilkent University]], [[Ankara]] and signed a [[Memorandum of understanding]] with the [[Ministry of Transport, Maritime and Communication (Turkey)|Turkish Ministry of Transport, Maritime Affairs & Communications]] in order to manufacture the 328 at Ankara.<ref>[http://www.sncorp.com/AboutUs/NewsDetails/1261 "Sierra Nevada Corporation Collaborates on Regional Jet Aircraft in Turkey: Powerful global partnership brings unique technology to the aviation world."] ''Sierra Nevada Corporation'', 27 May 2015.</ref>

In June 2015, the Turkish government announced the formal launch of the Turkish regional aircraft project, which intends to manufacture a modernized version of Dornier 328/Fairchild Dornier 328JET, designated as the ''TRJ-328''. At the time, multiple powerplants were being considered for the aircraft with the use of either a turboprop or jet engine being possible.<ref>Perry, Dominic. [https://www.flightglobal.com/news/articles/paris-turkish-regional-aircraft-may-feature-pw-eng-413598/ "PARIS: Turkish regional aircraft may feature P&W engines."] ''Flight International'', 15 June 2015.</ref> The TRJ-328 shall also serve as the technology base for a larger TRJ-628 which is intended to be designed, developed and produced entirely by Turkish engineers. [[Turkish Aerospace Industries]] (TAI) has been placed in charge of manufacturing activities, alongside several other Turkish companies, including [[ASELSAN]], [[TUSAŞ Engine Industries|TEI]], [[HAVELSAN]], [[Turkish Technic]], Alp Aviation and Kale Aviation.<ref name="al1">{{cite news |url=http://www.airliners.net/aviation-forums/general_aviation/read.main/6409451/ |newspaper=Airliners |title=Turkish Regional Jets Launched - TRJ-328 & 628 |date=27 May 2015 }}</ref><ref name="s1">{{cite news |url=http://www.sabah.com.tr/ekonomi/2015/05/27/iste-yerli-yolcu-ucaginin-teknik-ozellikleri |newspaper=[[Sabah (newspaper)|Sabah]] |title=İşte yerli yolcu uçağının teknik özellikleri |date=27 May 2015|language=Turkish }}</ref> Initially, parts of the original supply chain shall be used, but these are to be replaced by Turkish-sourced counterparts once available.<ref>O'Keeffe, Niall. [https://www.flightglobal.com/news/articles/programme-partner-outlines-production-plan-for-mode-417785/ "Programme partner outlines production plan for modernised 328s."] ''Flight International'', 15 October 2015.</ref>

==Design==
[[File:THE 328 (419180200).jpg|thumb|Forward section of a 328]]
The Dornier 328 is a twin-[[turboprop]] engine [[regional aircraft]], principally designed for shorthaul passenger operators; Deutsche Aerospace often promoted the type as being a "third-generation airliner".<ref name = "high 69"/> The fuselage of the 328 employs an unusual steamlined shape, having been optimised for high cruising speeds; the aircraft is capable of higher cruise and approach speeds than most turboprop-powered aircraft, this allows it to be more readily slotted around jetliners during landing approaches.<ref name = "high 69 70"/> According to Deutsche Aerospace, the 328 offered the "lowest noise level, widest cabin, highest standing room, widest cabin floor, and widest seats in the three-abreast class".<ref name = "describe 40">Moxon, Barrie and Goold 1991, p. 40.</ref> The 328 is capable of operations from semi-prepared airstrips and rough runways, incorporating features such as its retractable [[landing gear]] being equipped with high-floatation tyres and steerable nose gear and a gravel guard.<ref name = "broch"/>

It is equipped with a pair of [[Pratt & Whitney Canada PW100|Pratt and Whitney PW119C]] turboprop engines, which drive fully reversible [[Hartzell]] HD-E6C-3B propellers.<ref name = "broch"/> The propeller blades generate notably less noise in comparison to their contemporary counterparts due to features such as their lower [[RPM]],<ref name = "high 70"/><ref name = "describe 32"/> propeller synchrophasing, and the use of a six-blade configuration.<ref name = "describe 34"/> The propeller system is dual-acting, being capable of adjusting pitch to maintain a constant engine RPM.<ref name = "describe 34"/> During the early 1990s, the manufacturer claimed that the use of various noise reduction measures across the aircraft kept the internal cabin noise "below that of even some modern jet aircraft".<ref name = "describe 31"/>

The fuselage of 328 allows for a comfortable three-abreast airline-style seating arrangement to be used as well as a dense four-abreast configuration to accommodate greater passenger numbers, of which it is able to carry a maximum of 27.<ref name = "broch">[http://mediakit.sncorp.com/mediastore/document/SNC_Dornier-328-100-Product-Sheet_20141112.pdf "Dornier-328-100."] ''Sierra Nevada Corporation'', 2014.</ref> There are a total of six cabin configurations available for passenger and cargo operations, these include a flexible [[combi aircraft]] layout with a movable wall separating passengers and cargo, and a medical evacuation arrangement equipped with bio-floors and positions for four litters and medical attendees.<ref name = "broch"/> The 328 is pressurized, a first for Dornier-built aircraft, which was implemented to achieve a higher level of passenger comfort; the passenger cabin is designed to be more akin to those of much larger passenger aircraft.<ref name = "high 69 70">Moxon 1991, pp. 69-70.</ref> A full-sized galley, toilet and washbasin can also be installed.<ref name = "high 69"/>

[[File:Aerocardal (9318864545).jpg|thumb|left|Forward-facing internal view from the cockpit of an in-flight 328]]
The 328 is furnished with the same [[supercritical wing]] design that had been originally developed from Dornier's earlier [[Dornier Do 228|Do 228]], this wing provides the aircraft with both excellent cruise and climb capabilities.<ref name = "high 70"/><ref name = "describe 32"/> The straightforward construction techniques of the Do 228 were also reproduced for the 328, despite making increased use of [[composite material]]s in areas such as the rear fuselage and [[empennage]]. The 328 reportedly made greater use of composites than any of its direct competitors at launch, the use of the [[Kevlar]]-[[carbon fiber]] composites is claimed to have reduced its weight by 20 per cent.<ref name = "high 70"/><ref name = "describe 39"/> Various materials are used across the airframe; amongst these, an [[aluminium]] alloy is used for the pressure fuselage and much of the [[wingbox]], a [[titanium]] alloy for the tail cone, and [[fiberglass|glass fibre reinforced plastic]] for the [[radome]] and leading edge of the [[vertical stabilizer]].<ref name="auto">Moxon, Barrie and Goold 1991, pp. 38-39.</ref> Noise-absorbent material is located across the fuselage, while the cabin wall is hung from isolator [[bracket]]s to reduce vibration and noise transference.<ref name = "high 70"/><ref name = "describe 39">Moxon, Barrie and Goold 1991, p. 39.</ref>

The two-man [[glass cockpit]] of the 328 is equipped with a [[Honeywell Primus#Primus 2000.2F2000XP|Honeywell Primus 2000]] avionics suite, the cockpit is outfitted with an [[electronic flight instrument system]] comprising five 20 x 17.5&nbsp;cm [[cathode ray tube]] (CRT) monitors.<ref name = "broch"/> The central CRT serves as the [[engine-indicating and crew-alerting system]], while the two inner CRTs are used as multi-function displays and the outermost two CRTs perform as the primary flight displays respectively.<ref name="auto"/> Addition avionics include a dual integrated avionics computer, a digital [[Bus (computing)|databus]] (a commercial derivative
of the [[MIL-STD-1553]] databus), dual Primus II integrated radio system, automatic [[flight control system]], dual digital air data reference units, Primus 650 [[weather radar]], dual mode-S [[transponder]], [[ground proximity warning system|enhanced ground proximity warning system]], and [[traffic collision avoidance system]] (TCAS).<ref name = "broch"/><ref name = "high 70">Moxon 1991, p. 70.</ref><ref name = "describe 36 39">Moxon, Barrie and Goold 1991, pp. 36, 39.</ref>

==Operational history==
{{expand section|date=July 2016}}
In 2005, the [[Australian Maritime Safety Authority]] (AMSA) awarded a contract to AeroRescue to provide long-range Search and Rescue (SAR) capability around Australia. Accordingly, five 328-100s were progressively commissioned from April 2006 <ref>{{cite web|url=http://www.amsa.gov.au/about_amsa/media_releases/2006/2006_aug_17.asp |title=AMSA : Australian Maritime Safety Authority|author=Australian Maritime Safety Authority|work=amsa.gov.au|accessdate=3 July 2015}}</ref> to February 2007 and stationed around the Australian coastline to provide a 24-hour, 30 minute response capability. These aircraft were equipped with a comprehensive electronic sensor suite by Aerodata AG in Germany including; [[Israel Aerospace Industries]] ELTA EL/M 2022A Radar, FSI Star SAFire III Forward Looking Infra Red (FLIR),  Direction Finder and an ARGON ST Infra Red/Ultra Violet scanner. The aircraft are also fitted with an [[Aero Engineers Australia|Aeronautical Engineers Australia]] dispatch system, allowing rescue stores to be dropped from the aircraft through a chute through the underwing emergency exit. These are progressively being upgraded with an inflight opening cargo door to allow dispatch of larger items, up to 20-man life rafts and boat dewatering pumps for open water rescues.

==Variants==
[[File:D328 D-CPRP.jpg|thumb|right|[[Excellent Air]] Do 328-100]]
[[File:01 United States Air Force, Dornier Do-328-110, Larnaca, Cyprus.jpg|thumb|right|[[United States Air Force]] version C-146A operating for [[524th Special Operations Squadron]]]]
[[File:VHPPJ.JPG|thumb|right|An [[AeroRescue]] Dornier 328-100 operated for the [[Australian Maritime Safety Authority]]]]
[[File:CentralMountainAir-Dornier-328-YVR.jpg|thumb|right|A [[Central Mountain Air]] Dornier 328-100 on approach to [[Vancouver International Airport]]]]

* '''328-100''' - Initial 328.
* '''328-110''' - Standard 328 with greater range and weights
* '''328-120''' - 328 with improved [[STOL]] performance.
* '''328-130''' - 328 with progressive rudder authority reduction at higher airspeeds.
* '''[[Fairchild Dornier 328JET|328JET]]''' - Turbofan-powered variant, formerly the '''328-300'''.
* '''C-146A Wolfhound''' - Designation assigned to seventeen Dornier 328s operated by the [[United States Air Force]]'s [[Air Force Special Operations Command]].<ref name="AFSOC">{{cite web|url=http://www.af.mil/AboutUs/FactSheets/Display/tabid/224/Article/467729/c-146a-wolfhound.aspx|title=C-146A Wolfhound|date=December 13, 2013|publisher=United States Air Force|accessdate=2014-08-02}}</ref>

==Operators==
In August 2013 a total of 166 remain in operation with 48 Dornier 328-100 aircraft in airline service. Major operators include: [[Loganair]] (5), and [[Sky Work Airlines]] (5). Thirteen other airlines operate smaller numbers of the type.<ref name="FG2013">[http://www.flightglobal.com/Assets/GetAsset.aspx?ItemID=52125 World Airliner Census 2013]. FlightGlobal. July 2013. Accessed 2014-09-09.</ref>

===Military/Government===
;{{AUS}}
* [[Australian Maritime Safety Authority]] (5)<ref>{{cite web|url=https://www.amsa.gov.au/forms-and-publications/Fact-Sheets/Dornier_2012.pdf|title=Dornier 328-120 turboprop aircraft|date=July 2012|publisher=Australian Maritime Safety Authority|accessdate=2014-09-09|location=Canberra, ACT}}</ref>
;{{BOT}}
* [[Botswana Defence Force Air Wing]] (1)<ref>{{cite web|url=http://www.planespotters.net/Production_List/Dornier/Do-328/3083,OB2-Botswana-Defence-Force.php|title=OB2 Botswana Defence Force Dornier 328-110 - cn 3083|year=2014|work=Planespotters.net|accessdate=2014-09-09}}</ref>
;{{USA}}
* [[Air Force Special Operations Command]] (17)<ref name="AFSOC"/>

===Civilian===
;{{KAZ}}
* [[Caspiy]] (1)
;{{ASM}}
* [[Inter Island Airways]] (1)<ref name="FG2013"/>
;{{AUT}}
* [[Welcome Air]] (2)<ref name="FG2013"/>
* [[Air Alps]] (3)
;{{CAN}}
* [[Flair Airlines]] (1)<ref name="FG2013"/>
* [[Central Mountain Air]] (3)<ref name="FG2013"/>
* [[Calm Air]] (2) <ref name="FG2013"/>
;{{COL}}
* [[Aerolínea de Antioquia]] (5)<ref name="FG2013"/>
;{{CHL}}
* [[Aerocardal]] (2)<ref name="FG2013"/>
;{{COD}}
* [[Katana Wings]] (3)<ref name="FG2013"/>
;{{DEN}}
* [[Sun Air of Scandinavia]] (3)<ref name="FG2013"/>
;{{GER}}
* [[Rhein-Neckar Air]] (2)
* [[Private Wings]] (6)
;{{IDN}}
* [[XpressAir]] (5)<ref name="FG2013"/>
;{{MLT}}
* [[Medavia]] (1)<ref name="FG2013"/>
;{{NGA}}
* [[Dornier Aviation Nigeria AIEP]] (3)
* [[Air Peace]] (3)
;{{PHL}}
* [[South East Asian Airlines]] (2)
* [[SkyJet]] (1)
;{{SUI}}
* [[Sky Work Airlines]] (5)<ref name="FG2013"/>
;{{GBR}}
* [[Loganair]] (3)<ref name="FG2013"/>
;{{USA}}
* [[Berry Aviation]] (3)<ref name="FG2013"/>
* [[Vision Airlines]] (1)<ref name="FG2013"/>
* [[Corning, Inc.]] (2) 
The Dornier 328 turboprop was also operated in the past in scheduled passenger service by several U.S. regional airlines including [[Air Wisconsin]], [[Horizon Air]], [[Lone Star Airlines]], and [[Mountain Air Express]].  In addition, the aircraft was previously used to provide passenger feeder services in the U.S. operating as [[United Express]] and [[US Airways Express]] flights.

==Accidents==
* On 25 February 1999, [[Minerva Airlines]] Flight 1553 on a flight from [[Cagliari-Elmas Airport]] to [[Genoa Cristoforo Colombo Airport]] in [[Italy]].  Upon landing on runway 29 the aircraft ran off the end of the runway and crashed into the sea. Four of the 31 passengers and crew died in the accident.<ref>{{cite web|url=http://aviation-safety.net/database/record.php?id=19990225-0|title=ASN Aircraft accident Dornier 328-110 D-CPRR Genoa-Cristoforo Colombo Airport (GOA)|author=Harro Ranter|date=25 February 1999|work=aviation-safety.net|accessdate=3 July 2015}}</ref>

==Specifications (Dornier 328-110)==
[[File:Do 328 sideview (turboprop and jet).svg|300px|right]]
[[File:EM Vision Air DO328 (3108438070).jpg|thumb|Interior of a Dornier 328]]
{{aircraft specifications
| jet or prop? =prop
|crew=Three (two Pilots, one Flight Attendant)
|capacity=30 to 33 (14 in [[First class (aviation)|First Class]] Config) 
|length main=21.23 <!-- one of the merged articles said 21.22 m  -->m
|length alt= 69 ft 8 in
|span main=20.98 m
|span alt=68 ft 10 in
|height main=7.05 m
|height alt=23 ft 2 in
|area main=40 m²
|area alt=431 sq ft
|airfoil=
|empty weight main=9,100 kg 
|empty weight alt=20,062 lb (incl. crew)
|loaded weight main= 
|loaded weight alt=
|max useful load main=4,890 kg
|max useful load alt=10,781 lb
|max takeoff weight main=13,990 kg
|max takeoff weight alt=30,843 lb
|more general=
<!--
        Powerplant
-->
|engine (prop) = [[Pratt & Whitney Canada PW100|Pratt & Whitney Canada PW119B]]
|type of prop = [[turboprop]]s
|number of props = 2
|power main = 1,625 kW
|power alt = 2,180 [[horsepower|hp]]
|power original=
<!--
        Performance
-->
|cruise speed main= 620 km/h
|cruise speed alt= 335 [[knot (unit)|kn]]
|stall speed main= |stall speed alt= 
|never exceed speed main= 
|never exceed speed alt= 
|range main=1,852 km
|range alt=1,000 nmi, 1,150 mi
|ceiling main=9,492 m
|ceiling alt= 31,140 ft
|climb rate main= 
|climb rate alt= 
|loading main= 
|loading alt= 
|thrust/weight=
|power/mass main= 
|power/mass alt= 
|more performance=
|armament=
|avionics=[[Honeywell Primus]] 2000
}}

==See also==
{{External media|topic= |width=23% |align=right |video1=[https://www.youtube.com/watch?v=U6SpAJmJfv8 In-cockpit footage of a 328 during a visual approach landing] |video2=[https://www.youtube.com/watch?v=nmY_epAUcsc Promotional video of a Dornier 328 private jet conversion] |video3=[https://www.youtube.com/watch?v=86TYRQRaCT0 Video of a Dornier 328 taxiing and taking off]}}
{{aircontent
|related=
* [[Dornier Do 228]]
* [[Fairchild Dornier 328JET]]
|similar aircraft=
* [[DHC-8]] 100/200
* [[Embraer EMB 120 Brasilia]]
* [[Jetstream 41]]
* [[Saab 340]]
|lists=
|see also=
}}

==References==

===Citations===
{{reflist}}

===Bibliography===
{{refbegin}}
* Moxon, Julian. [https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%201397.html "High Tech Commuter."] ''Flight International'', 4 June 1991. pp.&nbsp;69–70.
* Moxon, Julian., Douglas Barrie and Ian Goold. [https://www.flightglobal.com/pdfarchive/view/1991/1991%20-%202637.html "Do.328 Described: Friendly, Fast, and Frugal."] ''Flight International'', 15 October 1991. pp.&nbsp;31–40.
* Swanborough, Gordon. "Dornier 328: A Daimler for Commuters". ''[[Air International]]'', March 1992, Vol. 42 No. 3. pp.&nbsp;123–128. ISSN 0306-5634.
* Taylor, Michael J.H. ''Brassey's World Aircraft & Systems Directory 1999/2000''. London:Brassey's, 1999. ISBN 1-85753-245-7.
{{refend}}

==External links==
{{Commons category-inline|Dornier 328JET}}<br>
{{Commons category-inline|Dornier 328}}
* [http://www.trjet.com/ TRJet-Turkish regional aircraft project official page] 
* [http://www.328.eu 328 Support Services GmbH official page]
* {{cite news |url= http://aviationweek.com/business-aviation/used-aircraft-report-dornier-328-turboprop |title= Used Aircraft Report: Dornier 328 Turboprop |date= Jul 26, 2016 |author= Fred George |work= Business & Commercial Aviation |publisher= aviation week}}

{{Fairchild aircraft}}
{{Dornier aircraft}}
{{USAF transports}}

[[Category:Dornier aircraft|328]]
[[Category:Fairchild aircraft]]
[[Category:International airliners 1990–1999]]
[[Category:Twin-engined tractor aircraft]]
[[Category:Turboprop aircraft]]
[[Category:High-wing aircraft]]
[[Category:T-tail aircraft]]