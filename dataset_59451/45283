{{Use dmy dates|date=February 2015}}
{{Use British English|date=February 2015}}
{{Infobox engineer
|image                = George Carter, Glosters engineer.jpg
|image_size           = 
|alt                  =
|caption              = 
|name                 = George Carter
|nationality          = British
|citizenship          = 
|birth_date           = 9 March 1889
|birth_place          = Bedford
|death_date           = {{d-da|27 February 1969|9 March 1889}}
|death_place          = Gloucestershire
|education            = 
|spouse               = Hilda Back
|parents              = George Alfred Carter, Bertha Ellen Odell 
|children             = Wilfred Maxwell (Peter)
|discipline           = Aeronautics
|institutions         = [[Royal Aeronautical Society|RAeS]]
|practice_name        = 
|employer             = [[Gloster Aircraft Company]]
|significant_projects = 
|significant_design   = [[Gloster Meteor]], [[Gloster E.1/44]]
|significant_advance  = [[Gloster E.28/39]]
|significant_awards   = RAeS Silver Medal (1947)
}}

'''Wilfred George Carter''' CBE FRAeS (9 March 1889 – 27 February 1969) was a British engineer, who was the chief designer at [[Gloster Aircraft Company|Gloster]]s from 1937. He was awarded the [[Order of the British Empire|C.B.E.]] in 1947 and was appointed Technical Director of Gloster Aircraft in 1948 remaining on the board of directors until 1954. He continued to serve Glosters for a number of years after his retirement in a consultancy role until 1958. He designed the first recognised jet aircraft.

==Career==
Carter had his apprenticeship with W. H. Allen Sons and Co. Ltd of Bedford from 1906–1912. From 1916-20 he was Chief Draughtsman of [[Sopwith Aviation Company]], then Chief Designer from 1920–1924 of [[Hawker Aircraft|Hawker Engineering Co. Ltd]], working on the [[Hawker Heron|Heron]] and [[Hawker Hornbill|Hornbill]] fighter aircraft, and the [[Hawker Horsley|Horsley]] bomber. From 1924–1928 he worked with [[Short Brothers|Short Bros]] of [[Rochester, Kent|Rochester]], designing a seaplane for the 1927 Schneider Trophy. From 1928–1931, Carter worked for [[de Havilland]]. From 1935–1936, he also worked for [[Avro]].

===Gloster===
Carter joined the [[Gloster Aircraft Company|Gloucestershire (later Gloster) Aircraft Company]], at [[Brockworth, Gloucestershire]], in 1931. He initially worked on the [[De Havilland DH.72]] bomber (only one was ever built), which was given to Gloster from de Havilland.

At Gloster Aircraft, Carter was instrumental in the design of two of the most significant biplane fighters for the RAF, the [[Gloster Gauntlet|Gauntlet]] and [[Gloster Gladiator|Gladiator]]. Carter also designed the [[Gloster F.9/37]] a promising twin-engine ([[Bristol Taurus]]) fighter design that never entered production, before he turned to work on jet aircraft. He was Chief Designer from 1936–1948. In 1934 Gloster had been taken over by Hawker, causing the chief designer, [[Henry Folland]], to leave, making way for his successor.

==Jet aircraft==
It was during a visit by [[Frank Whittle]] to Gloster that Carter became involved in the development of [[jet aircraft]]. At the time Gloster were working on a twin-boom fighter, for specification F.18/37 - also used for the [[Hawker Typhoon]], to be powered by a [[Napier Sabre]] piston engine which attracted the attention of Whittle who thought that the layout would be suitable for his new engine. Although the design Whittle saw would not progress beyond the project stage, within a few weeks, Carter was asked by the [[Air Ministry]] to submit plans for a brand new aircraft to use Whittle's engine. He agreed to the project before seeing the engine for himself. While not impressed with the engine itself, when he saw it running he was convinced that it could develop into a suitable powerplant given what they had managed to achieve in the somewhat primitive conditions at [[Lutterworth]]. 

[[File:Gloster E28-39 first prototyp lr.jpg|thumb|160px|left|Gloster E.28/39]]
The [[Gloster E.28/39]] was designed primarily to prove the concept of [[turbojet]] powered flight, the Air Ministry however insisted that the design include provision for four [[0.303 British|gun]]s and 2,000 rounds of ammunition even if these were not fitted in the [[prototype]]. The contract to build the E.28/39 also known as the Pioneer was placed with Gloster on 3 February 1940. The aircraft was built in secret at the Regents garage, Cheltenham and first flew on 15 April 1941 at [[RAF Cranwell]], becoming the first British and Allied jet aircraft. 

Even before the Pioneer flew, the Air Ministry encouraged Carter to design a practical jet fighter since the Pioneer was not suitable because it was unlikely that an engine of at least {{convert|2000|lbf|kN|abbr=on}} thrust would be available in the near future. Carter therefore decided that the design would require two engines. The result was designated the F.9/40 which first flew on 5 March 1943 and would find worldwide fame as the [[Gloster Meteor]]. His later designs included the [[Gloster E.1/44|E.1/44]]. He supervised the design of the Gloster GA-5 delta-wing fighter (later the [[Gloster Javelin]] which first flew in 1951 from [[RAF Moreton Valence]] south of Gloucester), which was designed by Richard Walker (Gloster's chief designer) and powered by [[Armstrong Siddeley Sapphire]] engines.

==Honours==
[[File:Gloster Meteor III ExCC.jpg|thumb|160px|right|Gloster Meteor]]
Along with other pioneering aircraft designers, Carter was honoured in 1997 with the issuance of a special [[postage stamp]] in a series called "The Architects of the Air." Other partnerships featured on the stamps were [[R. J. Mitchell]] and Supermarine's celebrated [[Supermarine Spitfire|Spitfire]] on the 20p stamp, R.E. Bishop and the [[de Havilland]] company's [[de Havilland Mosquito|Mosquito]] on a 37p stamp and [[Sydney Camm]], designer of the [[Hawker Hunter]] fighter, featured on the 63p value. 

Chairman of [[Royal Mail]]'s Stamp Advisory Committee, Adam Novak said: "Each one of the aircraft featured on the stamps was unique and revolutionary in its own way. The Architects of the Air were the trail blazers for today's modern aircraft designs.<ref>[http://www.prnewswire.co.uk/cgi/news/release?id=40227 "Stamp Designs Pay Tribute To Revolutionary British Aircraft."] ''prnewswire.co.'' Retrieved: 11 May 2011.</ref>" George Carter's face dramatically forms the clouds overlooking a flight of a Meteor Mk T7 on the 1997 43p stamp.

===Personal life===
(Wilfred) George Carter was born in Bedford. His father George Alfred Carter was a journeyman carpenter and later became a builder.
Carter married Hilda Back at St Martin's, Bedford on 20 April 1916.  They had a son, Wilfred Maxwell (Peter), born in 1917.
Carter became CBE in 1947, and lived on ''Dog Lane'' in [[Crickley Hill]], Gloucester. He was awarded the [[Royal Aeronautical Society|RAeS]] Silver Medal in 1947.

==References==
;Notes
{{reflist}}
;Bibliography
{{Refbegin}}
* James, Derek N. ''Gloster Aircraft since 1917.'' London: Putnam, 1987, ISBN 0-85177-807-0. First edition 1971. ISBN 0-370-00084-6.
{{refend}}

==External links==
* [http://www.flightglobal.com/pdfarchive/view/1946/1946%20-%200554.html "Jet Propulsion and Military Aircraft", a 1946 article in ''Flight'' by W. G. Carter]
* [http://www.meteorflight.com/A55D74/meteor.nsf/pages/jet_people-wcarter Meteor Flight]

{{DEFAULTSORT:Carter, George}}
[[Category:English aerospace engineers]]
[[Category:1889 births]]
[[Category:1969 deaths]]
[[Category:People from Gloucestershire]]
[[Category:Fellows of the Royal Aeronautical Society]]