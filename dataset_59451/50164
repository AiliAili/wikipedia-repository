{{refimprove|date=November 2013}}
[[Image:Abkhazia detail map2.png|thumb|300px|Map of Abkhazia]] 
'''[[Abkhazia]]''' is a region in [[South Caucasus]]. It is ''[[de facto]]'' independent republic, but is mostly internationally recognized as an [[autonomous republic]] within [[Georgia (country)|Georgia]]. It is recognized as an independent nation by [[Russia]], [[Nicaragua]], [[Venezuela]], [[Nauru]], and [[South Ossetia]]. The article deals with the geography and climate of Abkhazia.

==Geography==
Abkhazia covers an area of about {{convert|8660|km2|sqmi}}<ref name=EB>{{cite encyclopedia |editor-first=Dale H. |editor-last=Hoiberg|encyclopedia=Encyclopedia Britannica |title=Abkhazia|edition = 15th |year=2010|publisher=Encyclopedia Britannica Inc. |volume=I: A-ak Bayes |location=Chicago, IL |isbn=978-1-59339-837-8|pages=33}}</ref> at the western end of Georgia, on the north shore of the [[Black Sea]]. The [[Caucasus Mountains]] to the north and the northeast divide Abkhazia from [[Circassia]]. To the east, the region is bordered by [[Svaneti]]. To the southeast, Abkhazia is bounded by [[Samegrelo]]; and on the south and southwest by the Black Sea. The topography is varied as it ranges from the lowlands around the [[black Sea]] to the high peaks in the north.

The region is extremely mountainous (nearly 75% is classified as mountains or foothills) and settlement is largely confined to the coast and a number of deep, well-watered valleys.<ref name=EB/>  The [[Caucasus Mountains|Greater Caucasus Mountain Range]] runs along the region's northern border.  The [[Gagra Range|Gagra]], [[Bzyb Range|Bzyb]], and [[Kodori Range|Kodori]] Ranges branch off from the Main Caucasus Range.  The highest peaks of Abkhazia are in the northeast and east (along the border with [[Svaneti]]) and several exceed {{convert|4000|m|ft|0|sp=us}} above sea level. The highest mountain is [[Dombai-Ulgen]] ({{convert|4046|m|ft|0|abbr=on|disp=or}}). 

==Landscape==
[[Image:Ritsa1.jpg|thumb|left| 250px|Ritsa lake]]
The landscapes of Abkhazia range from coastal forests ([[endemic (ecology)|endemic]] [[Pitsunda pine]] forests near Bichvinta/Pitsunda) and citrus plantations, to eternal snows and [[glaciers]] to the north of the republic. Because of Abkhazia's complex topographic setting, most of the territory has been spared from significant human cultivation and development.  Therefore, a large portion of Abkhazia (nearly 70% of the territory) is still covered by forests today. Abkhazia is also well known for the high number of endemic species of plants that are found only in the Caucasus, only in Georgia, or only in Abkhazia. The forests of Abkhazia used to be more prevalent and have since been cleared drastically. They consisted of [[oak]], [[beech]], and [[hornbeam]].<ref name=EB/> Southeastern Abkhazia, a part of the [[Colchis|Colchis Lowland]], is still covered by Colchian forests ([[alder]], [[hornbeam]], [[oak]], [[Oriental Beech|beech]]), or by [[citrus]] and [[tea]] plantations.

The world's deepest known cave, [[Krubera Cave|Krubera (Voronja) Cave]], is located in Abkhazia's western Caucasus mountains. The latest survey (as of September 2007) has measured the vertical span of this cave system as {{convert|2191|m|ft|0|abbr=on}} between its highest and lowest explored points.

The foothills, up to an elevation of {{convert|600|m|ft|0|sp=us}} above sea level, are covered by [[deciduous]] forests (with [[evergreen]] elements), and include tree species such as oak, hornbeam, beech, and [[buxus]]. The forest covers from {{convert|600|to|1800|m|ft|0|sp=us}} above sea level and is made up of both deciduous and [[coniferous]] tree species. The most common species are beech, [[Caucasian Spruce|spruce]], and [[Nordmann Fir|fir]]. The mixed forest zone is home to some of the tallest trees in [[Europe]] and the world, where some specimens of the Nordmann Fir (especially around [[Lake Ritsa]]) reach heights of over {{convert|70|m|ft|0|sp=us}}. The zone extending {{convert|1800|to|2900|m|ft|0|sp=us}} above sea level is made up of either subalpine forests or [[Alpine climate|alpine]] meadows. Territory lying above {{convert|2900|m|ft|0|sp=us}} is mainly covered by eternal snows and glaciers.

==Climate==
{{See also|Abkhazia#Geography and climate|l1=Geography and climate}}

[[Image:Ridge view from pitsunda cape.jpg|thumb| 250px| View of the Caucasus mountains from Pitsunda cape]]
Because of Abkhazia's proximity to the [[Black Sea]], its climate is very mild, considering the northern [[latitude]].  The Caucasus Mountains are greatly responsible for moderating the region's climate, as they shield Abkhazia from cold northerly winds. 

The coastal areas of the Republic have a subtropical climate, where the average annual temperature in most regions is around {{convert|15|°C|lk=on}}.  Average winter (January) temperatures vary between {{convert|4|and|6|°C|°F|1}}, while average summer (July) temperatures are anywhere between {{convert|22|and|23|°C|°F|1}}. The coastal territory rarely experiences strong frosts during the winter.  

Higher elevations of Abkhazia, above {{convert|1000|m|ft|0|sp=us}} above sea level have a maritime, mountain climate, experiencing relatively cold winters and long, warm summers.  Elevations above {{convert|2000|m|ft|0|sp=us}} above sea level have colder winters and shorter summers.  Abkhazia's highest regions have a cold, summerless climate throughout the year.

Abkhazia receives high amounts of precipitation, but is known for its unique micro-climate (transitional from subtropical to mountain) along most of its coast, causing lower levels of humidity.  The annual precipitation along the coast ranges from  {{convert|1200|to|1200|mm|in|1|abbr=on}}.<ref name=EB/>  The foothills, the lower ranges, and the interior gorges of the Republic receive anywhere between {{convert|1000|to|1800|mm|in|1|abbr=on}} of precipitation annually.  Some of the interior gorges that are sheltered from the moist influences of the Black Sea receive the lowest amounts of precipitation.  The higher mountainous regions receive {{convert|1700|to|3500|mm|in|1|abbr=on}} of precipitation per year.  Although there is usually no significant snowfall in the coastal regions, the mountains of Abkhazia do receive significant amounts of snow.  [[Avalanches]] in the northeast sometimes pose a threat to populated areas.  Snow depths often exceed {{convert|5|m|ft|1|sp=us}} or {{convert|500|cm|in|1|abbr=on}} in some of the high, mountainous areas facing the Black Sea. The climate is mild, which in the Soviet times caused it to become a popular holiday destination known as the "Georgian Riviera".

==References==
{{reflist}}

==See also== 
{{wikiatlas|Abkhazia}}
{{Geography of Asia}}
{{Asia topic|Climate of}}
{{Geography of Europe}}
{{Europe topic|Climate of}}

[[Category:Geography of Abkhazia| ]]
[[Category:Geography of Georgia (country)]]