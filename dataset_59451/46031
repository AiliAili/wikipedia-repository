{{Use dmy dates|date=April 2015}}
{{Use British English|date=April 2015}}
{{Infobox MP
| honorific-prefix =
| name = Beki Adam
| honorific-suffix = 
| image = Beki_Adam_standing_outside_Parliament_in_January_2015.png
| imagesize = 
| caption = Beki Adam at the Houses of Parliament.
|office2 = Currently standing as an Independent [[Candidate]] for [[Mid Sussex (UK Parliament constituency)]]
| birthname =
| birth_date = {{Birth date and age|1966|06|25|df=y}}
| birth_place = [[Cuckfield]], [[West Sussex]], [[England]]
| death_date =
| death_place =
| nationality = British
| spouse =
| party = [[Independent (politician)|Independent]]
| relations =
| children =
| residence =
| alma_mater = 
| occupation = 
| profession =
| religion =
| signature =
| website = http://www.voteadam.info/
| footnotes =
}}
'''Beki Adam''' (born 25 June 1966), is a researcher, Sussex farm and campsite owner. She was a parliamentary candidate for [[Mid Sussex (UK Parliament constituency)]] in the [[United Kingdom general election, 2015|2015 British General Election]].

==Journalism==

===Television===

Beki worked as a journalist for print, before later moving into television and presented the [[BBC]] Television show [[Top Gear (1977 TV series)|Top Gear]] during the 1980s.<ref>{{cite news|last1=Fletcher|first1=Harry|title=Former Top Gear presenter Beki Adam to stand as MP|work=Digital Spy}}</ref> Speaking of her time presenting the programme, “Back then there were only four television channels and at our height we were getting seven million viewers. I just remember looking down the camera lens and thinking I’ve got this huge audience and I’m talking about a car. It felt like such a waste.”.<ref>{{cite news|title=Top Gear presenter’s bid to become East Grinstead’s next MP|url=http://www.eastgrinsteadcourier.co.uk/Gear-presenter-8217-s-bid-East-Grinstead-8217-s/story-26085157-detail/story.html|accessdate=28 April 2015|work=East Grinstead Courier}}</ref>

===Print===

Beki has published several books related to motoring, including ‘Star Cars’, ‘Cobra’ and ‘Ferrari V8’.<ref>{{cite web|title=Beki Adam (author page)|url=https://openlibrary.org/authors/OL2910633A/Beki_Adam|website=OpenLibrary|publisher=OpenLibrary.org|accessdate=21 April 2015}}</ref>

==Research==

===Cannabis===

Beki spent time investigating the prohibition of cannabis during the 1990s, resulting in her opening a cannabis cafe in [[Brighton]] in 1993. The cafe was shut down by police after 57 minutes.<ref>{{cite news|last1=Renton|first1=Alex|title=Police snuff out first cannabis joint|url=http://www.independent.co.uk/news/police-snuff-out-first-cannabis-joint-1508187.html|accessdate=21 April 2015|work=The Independent|date=2 October 1993}}</ref>

===Fracking===

Beki is a known opponent of hydraulic fracturing, claiming that the UK has “other alternatives for UK energy supply and security, and, quite clearly, it is time to turn our undivided attention to them.”.<ref>{{cite news|last1=Gross|first1=Oli|title=Fracking legislation divides opinion|url=http://www.midsussextimes.co.uk/news/local/fracking-legislation-divides-opinion-1-6545480|accessdate=28 April 2015|work=Mid Sussex Times}}</ref> Beki’s independent research and opposition of fracking led her to become a contributor of evidence during the Infrastructure Bill committee debates in 2015.<ref>{{cite web|title=Infrastructure Bill: written evidence submitted by Miss E R Adam|url=http://www.publications.parliament.uk/pa/cm201415/cmpublic/infrastructure/memo/ib21.htm|website=Parliament.uk|accessdate=21 April 2015}}</ref> Beki campaigned against the Infrastructure Bill in 2015 and suggested that it was the “arguably the most irresponsible piece of legislation ever to be laid before a British Parliament”.<ref>{{cite news|title=Why We Should Bin The Fracking Infrastructure Bill|url=http://www.talkfracking.org/news/why-we-should-bin-the-fracking-infrastructure-bill/|accessdate=28 April 2015|work=Talk Fracking|date=19 December 2014}}</ref>

===Electric Cars===

Beki advocates electric car use, "The more people buy electric cars, the more the price will come down. I think today's politics are outdated and so is our view on technology. We need a 21st century vision."<ref>{{cite news|first1=East Grinstead|last1=Courier|title=Ex-Top Gear presenter brings dream car to East Grinstead|url=http://www.eastgrinsteadcourier.co.uk/Ex-Gear-presenter-brings-electric-dream-East/story-26223758-detail/story.html|accessdate=21 April 2015|work=East Grinstead Courier|date=28 March 2015}}</ref>

==Spiritual Life==
After working as a journalist  Beki travelled to Nepal and upon her return, she visited Samye Ling in Scotland and was ordained as a Buddhist Nun .<ref>{{cite book|last1=Brown|first1=Mick|title=The Dance of 17 Lives: The Incredible True Story of Tibet's 17th Karmapa|date=2010|publisher=Bloomsbury Publishing PLC|isbn=0747568715|url=https://books.google.co.uk/books?id=I55R-hGfcosC&pg=PT15&lpg=PT15&dq=beki+adam+ani+chudrun&source=bl&ots=dbxKdpC60i&sig=FpXt3cM2S-4nlAQarqgsuWqf-NI&hl=en&sa=X&ei=fS82VZivK4KP7AaotoGYBA&ved=0CE0Q6AEwCg#v=onepage&q=beki%20adam%20ani%20chudrun&f=false|accessdate=21 April 2015}}</ref> During her seventeen years of ordination, she spent three years in the United States in silent retreat and taught meditation in Mid Sussex.<ref>{{cite news|title=Beki to stand against Sir Nicholas for Mid Sussex seat|url=http://bluebelldigital.co.uk/eastgrinsteadonline/2015/01/22/beki-fight-sir-nicholas-mid-sussex-seat/|accessdate=28 April 2015|work=East Grinstead Online|date=22 January 2015}}</ref> Beki featured in a short documentary about her life as a nun titled ‘Unusual Choices’ filmed in 2009.<ref>{{cite web|title=Unusual Choices: Ani Chudrun (video)|url=https://vimeo.com/37114392|website=Vimeo|publisher=Planetary Collective|accessdate=28 April 2015}}</ref>

==Politics==
Beki was a candidate in the 2015 British General Election for the Mid Sussex constituency. As an Independent candidate, she stood ‘beyond party politics’ without ‘vested interests’ for the benefit of a ‘fair + safe society’.<ref>{{cite web|last1=Adam|first1=Beki|title=Homepage|url=http://www.voteadam.info/|website=Vote Adam|accessdate=21 April 2015}}</ref> Beki wished to offer “a viable option for people who have lost faith in the main political parties.” were she elected.<ref>{{cite news|title=Mid Sussex independent candidate to run in election|url=http://www.midsussextimes.co.uk/news/local/mid-sussex-independent-candidate-to-run-in-election-1-6562978|accessdate=28 April 2015|work=Mid Sussex Times|date=7 February 2015}}</ref> She has never voted in an election.<ref>{{cite news|last1=Alex|first1=Williams|title=Ex-Top Gear presenter’s bid to be Mid Sussex MP|url=http://brightfm.net/ex-top-gear-presenters-bid-mid-sussex-mp/|accessdate=28 April 2015|work=Bright.fm|date=21 January 2015}}</ref> She cites her previous experience in journalism and as a Buddhist nun as instrumental in leading her into a political career path, campaigning for a “safe and secure” society.<ref>{{cite news|title=Top Gear presenter’s bid to become East Grinstead’s next MP|url=http://www.eastgrinsteadcourier.co.uk/Gear-presenter-8217-s-bid-East-Grinstead-8217-s/story-26085157-detail/story.html|accessdate=28 April 2015|work=East Grinstead Courier|date=25 February 2015}}</ref> During campaigning, Beki spoke of her desire to launch a local bank providing small loans to local traders and small businesses,<ref>{{cite news|title=Mid Sussex independent candidate to run in election|url=http://www.midsussextimes.co.uk/news/local/mid-sussex-independent-candidate-to-run-in-election-1-6562978|accessdate=28 April 2015|work=Mid Sussex Times|date=7 February 2015}}</ref> with her aim in office to bring economic controls back to “support and protect people and place” <ref>{{cite news|title=Packed house for Election Hustings at the Jubilee Centre|url=http://bluebelldigital.co.uk/eastgrinsteadonline/2015/04/16/packed-house-for-election-hustings-at-the-jubilee-centre/|accessdate=28 April 2015|work=East Grinstead Online|date=16 April 2015}}</ref>

==Publications==
*''Star Cars'' (Osprey Publishing, Oxford, 1987) ISBN 085045803X
*''Cobra'' (Osprey Publishing, Oxford, 1989) ISBN 0850458099
*''Ferrari V8'' (Osprey Publishing, Oxford, 1990) ISBN 0850458811

== References ==
{{reflist}}

{{DEFAULTSORT:Adam, Beki}}
[[Category:Independent politicians in the United Kingdom]]
[[Category:Living people]]
[[Category:1966 births]]
[[Category:British political candidates]]