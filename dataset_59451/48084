'''Bashir Nur Gedi''' (died 19 October 2007) was the radio manager and acting director of [[Radio Shabelle]], an independent radio station that regularly ran watchdog reports on government corruption in [[Mogadishu]], [[Somalia]]. Gedi was murdered in 2007, though the precise circumstances of his death remain unclear. He was one of several prominent members of the station's leadership killed, including fellow acting media directors, [[Muktar Mohamed Hirabe]] and [[Hassan Osman Abdi]].<ref name=aljazeera>[http://www.aljazeera.com/news/africa/2012/01/20121295474898100.html Radio journalist gunned down in Mogadishu - Africa - Al Jazeera English<!-- Bot generated title -->]</ref> In 2007, he was the eighth journalist killed that year in Somalia.<ref name=reuters1>[http://www.reuters.com/article/2007/10/19/idUSHUL973327 Somali journalist gunned down in Mogadishu | Reuters<!-- Bot generated title -->]</ref><ref name=reuters2>[http://www.reuters.com/article/2007/10/20/idUSL20381530 Media watchdogs denounce death of Somali reporter | Reuters<!-- Bot generated title -->]</ref>

==Career==
Bashir Gedi served as the acting director for Radio Shabelle, an independent Somalian radio station that reports news while routinely highlighting government and military corruption. During Gedi's tenure, the government shut down Radio Shabelle, [[HornAfrik Media Inc|HornAfrik]] and [[IQK Koranic]].,<ref name=reuters1 /> and intimidated the station with violence, shooting at the building and rounding up 18 employees for questioning.<ref name=canada>[http://www.canada.com/windsorstar/story.html?id=1630695a-1dc8-4560-b28f-4e7f75b9495e Somali journalist gunned down in Mogadishu<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20140328084818/http://www.canada.com/windsorstar/story.html?id=1630695a-1dc8-4560-b28f-4e7f75b9495e |date=March 28, 2014 }}</ref>

==Death==
After one week of hiding inside the building of Radio Shabelle because of threats, Bashir Gedi ventured home on October 19, 2007. It was then that he was killed by a group of young gunmen, reported to be between the ages of 14 and 20, who were armed with pistols.<ref name=CPJ>[http://cpj.org/2007/10/radio-shabelle-manager-assassinated.php Radio Shabelle manager assassinated - Committee to Protect Journalists<!-- Bot generated title -->]</ref><ref name=Garowe>[http://www.garoweonline.com/artman2/publish/Press_Releases_32/Somalia_Urgent_appeal_to_international_community_after_media_owner_murdered.shtml Garowe Online<!-- Bot generated title -->] {{webarchive |url=https://web.archive.org/web/20071021081913/http://www.garoweonline.com/artman2/publish/Press_Releases_32/Somalia_Urgent_appeal_to_international_community_after_media_owner_murdered.shtml |date=October 21, 2007 }}</ref>
<ref>http://www.thejournalistsmemorial.org/?tri=date&iframe=true&width=80%&height=80%&pays_naissance=13&pays_deces=58&id_article=1460</ref>  Jafar Kukay, Radio Shabelle's successor to Gedi as acting director was quoted in Reuter's reports saying, "The acting chairman of Radio Shabelle, Bashir Nur Gedi, was shot dead by unknown men armed with pistols while sitting in front of his house". To date, his murder remains unsolved.<ref name=canada />  Following his death, two more acting directors of Radio Shaballe, Muktar Mohamed Hirabe and Hassan "Fantastic" Osman Abdi,<ref name="10news">[http://archive.is/20130116005416/http://www.10news.com/news/30324396/detail.html Gunmen Kill Journalist In Somalia - San Diego News Story - KGTV San Diego<!-- Bot generated title -->]</ref> have been murdered in very similar fashion.<ref name=aljazeera />

==Context==
Journalists in Somalia, which has been in a state of anarchy since dictator [[Mohamed Siad Barre]] was overthrown in 1991, routinely face violence, arbitrary imprisonment and harassment.{{citation needed|date=December 2013}}  The danger has intensified since the Somali government, with Ethiopian military help, expelled Islamist militants from Mogadishu in 2007.  Journalists not only face attacks from the government but also the insurgent forces angry with the critical reporting by Somalia's media.<ref name=reuters2 /> According to statistics kept by the [[Committee to Protect Journalists]], 40 journalists have been killed or murdered in Somalia since 1992 and it is considered one of the most dangerous places in the world for journalists.<ref>[http://cpj.org/killed/ Journalists Killed since 1992 - Committee to Protect Journalists<!-- Bot generated title -->]</ref>

==Impact==
Although Bashir Gedi knew he was under constant threat of being harmed or killed by those factions angry with his Radio Shabelle's reports, he continued to lead his station in their mission to disseminate news.  His and his colleagues' subsequent murders have drawn worldwide attention to the problems faced by Somalian journalists.

==Reactions==
[[Human Rights Watch]] called the development of independent and critical radio networks like Shabelle one of the most positive developments since Somalia slipped into anarchy.<ref name="M&G">[http://mg.co.za/article/2007-10-23-rights-group-chides-somali-govt-for-media-abuses Rights group chides Somali govt for media abuses - News - Mail & Guardian Online<!-- Bot generated title -->]</ref>

According to the Committee to Protect Journalists, "Shabelle, considered one of the leading stations in Somalia, has been harassed, threatened, and attacked by both government security forces and insurgents because of its critical reporting of the ongoing violence in Mogadishu."

[[Reporters Without Borders]] said, "The security situation in Mogadishu seems to be completely out of control. Abandoned by the Somali authorities and their international partners, journalists have become key targets who are easily accessible. One after another, the country's leading media owners are being eliminated. By taking no action in this climate of total impunity, those who could be in a position to stop these murders will end up becoming accomplices."<ref>[http://allafrica.com/stories/201201311207.html allAfrica.com: Somalia: Murder of the Head of Radio Shabelle in Mogadishu<!-- Bot generated title -->]</ref> It criticized the state of total impunity whereby no suspects are ever brought to trial or convicted.<ref name=reuters2 /><ref name=Garowe />

The Somali press freedom group, the National Union of Somali Journalists (NUSOJ), has proclaimed the murder to be politically motivated. Omar Faruk Osman, who is the secretary-general NUSOJ, said, "It is totally intolerable and sends a clear message to each media person that his or her life is at risk because of his or her media activity. We have been appealing to political groups to end killing of media people, but no group listens."<ref name=reuters2 />

== See also ==
[[Somali civil war]]

==References==
{{reflist}}

{{DEFAULTSORT:Gedi, Bashir Nur}}
[[Category:Year of birth missing]]
[[Category:2007 deaths]]
[[Category:Deaths by firearm in Somalia]]
[[Category:Journalists killed in Somalia]]
[[Category:Journalists killed while covering military conflicts]]
[[Category:Murdered Somalian journalists]]
[[Category:Somalian victims of crime]]