{{Infobox journal
| title = Conservation Biology
| cover = [[File:CoverIssueConservationBiology.jpg]]
| discipline = [[Conservation biology]]
| abbreviation = Conserv. Biol.
| impact = 4.320
| impact-year = 2013
| editor = Mark Burgman
| publisher = [[Wiley-Blackwell]] on behalf of the [[Society for Conservation Biology]]
| country = 
| history = 1987-present
| frequency = Bimonthly
| website = http://www.conbio.org/publications/conservation-biology
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1523-1739
| link1-name = Journal page at publisher's website
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1523-1739/currentissue
| link2-name = Online access
| link3 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1523-1739/issues
| link3-name = Online archive
| ISSN = 0888-8892
| eISSN = 1523-1739
| CODEN = CBIOEF
| LCCN = 88659972
| OCLC = 715539913
}}
{{Portal|Biology}}
'''''Conservation Biology''''' is a bimonthly [[peer-reviewed]] [[scientific journal]] of the [[Society for Conservation Biology]], published by [[Wiley-Blackwell]]. It covers the science and practice of conserving Earth's [[biological diversity]], including issues concerning any of the Earth's [[ecosystems]] or regions. According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 4.320.<ref name=WoS>{{cite book |year=2014 |chapter=Conservation Biology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.conbio.org/publications/conservation-biology}}

[[Category:Ecology journals]]
[[Category:Conservation biology]]
[[Category:English-language journals]]
[[Category:Publications established in 1987]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]
[[Category:Academic journals associated with learned and professional societies]]