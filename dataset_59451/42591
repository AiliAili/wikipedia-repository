__NOTOC__
{|{{Infobox aircraft begin
|name           =COW Biplane
|image          =COWBiplane-No10.jpg
|caption        =Biplane No. 10
}}{{Infobox aircraft type
|type           =Military Biplane
|national origin=United Kingdom
|manufacturer   =[[Coventry Ordnance Works]]
|designer       =[[W.O. Manning]]
|first flight   =1912
|number built   =2
}}
|}
The '''COW Biplane''' was a [[United Kingdom|British]] tractor biplane built to compete in the [[1912 British Military Aeroplane Competition]]. It was not successful.

==Design and development==
When the [[War Office]] held a competition to find a military aeroplane for the newly formed [[Royal Flying Corps]], the directors of the [[Coventry Ordnance Works]] decided to enter two aircraft.<ref name="ee"/> The company had just taken over the business of [[Howard T. Wright]] in Battersea, and they directed Wright and [[W.O. Manning]] to design and build the aircraft.<ref name="ee"/>

Manning designed two slightly different aircraft. Both were unequal-span tractor biplanes, the first had two crew seated side-by-side, and was powered by a {{convert|100|hp|kW|0|abbr=on}} [[Gnome et Rhône#Gnome|Gnome rotary engine]], the other had the two crew in tandem and was powered by a {{convert|110|hp|kW|0|abbr=on}} Chenu inline engine.<ref name="ee"/>

Construction of the Gnome powered aircraft started at Battersea in early 1912, by the end of April 1912 the components of the aircraft were moved to Hangar No. 32 at [[Brooklands]] for completion.<ref name="ee"/> The aircraft flew soon afterwards piloted by [[Thomas Sopwith]] who had been hired as a test pilot.<ref name="ee"/> On the day after the first flight the aircraft entered an impromptu competition and race at Brooklands, taking three passengers with two of the passengers sitting outside of the cockpit on the lower wing.<ref name="ee"/>

Construction of the second Chenu-powered aircraft followed and it was delivered to Brooklands in July 1912.<ref name="ee"/> The second aircraft differed in engine, seating arrangement, smaller wingspan, and shorter fuselage. Unusual for its time, it was fitted with a four-bladed propeller made from two two-bladed propellers joined together.<ref name="ee"/>

The War Office allocated Trial No. 10 to the Gnome-powered aircraft and No. 11 to the Chenu-powered one, thereafter they were always identified as Biplane No. 10 and Biplane No. 11.<ref name="ee"/> No. 10 arrived at [[Larkhill#History|Larkhill Aerodrome]] in good time for the competition but No. 11 was moved by road and due to delays it missed the entry deadline.<ref name="ee"/> Although No. 11 was not disqualified it failed to compete due to engine problems, suffering repeated failures of the [[Ignition magneto|magneto]] drive followed by failure of the reduction gear housing (similar problems with a Chenu engine also grounded the [[Martinsyde|Martin and Handasyde]] entry to the competition).<ref name="ee"/><ref name="Bruce RFC p16-8, 30-1">Bruce 1982, pp. 16–18, 30–31.</ref> No. 10 started the competition but had to be withdrawn with propeller problems.<ref name="ee"/>

The aircraft could not be fixed at Larkhill as Manning was abroad and Wright had left the company, so the aircraft were returned to Brooklands after the competition for further work.<ref name="ee"/> Manning decided to re-build No. 10 using the original fuselage and tail and retaining the engine, but it was fitted with new wings and landing gear.<ref name="ee"/> The modified No. 10 flew again on 13 January 1913 and was flown throughout 1913. The fate on No. 11 is unknown.<ref name="ee"/>

==Variants==
;No. 10
:Gnome-powered biplane for 1912 Military Aeroplane Competition with side-by-side seating, later modified with new wings and other changes.<ref name="ee"/>
;No. 11
:Chenu-powered biplane for 1912 Military Aeroplane Competition with tandem seating.<ref name="ee"/>

==Specifications (No. 10)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|ref=English Electric Aircraft and their predecessors,<ref name="ee"/> Flight <ref>{{citation|url=http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200626.html|title=The Coventry Ordnance Biplane|journal=Flight|date=13 July 1912|page=626}}</ref>
|crew=2
|capacity=
|payload main=
|payload alt=
|payload more=
|length main= 33 ft 3 in
|length alt=10.13 m
|span main=40 ft<ref>lower plane: {{convert|24|ft|8|in|m|abbr=on}}</ref>
|span alt=12.2 m
|height main=
|height alt=
|area main=336.7 ft²
|area alt= 31.3 m²
|airfoil=
|empty weight main= 1,200 lb
|empty weight alt= 544 kg
|loaded weight main= 1,950 lb
|loaded weight alt= 885 kg
|useful load main=
|useful load alt=
|max takeoff weight main=
|max takeoff weight alt=
|max takeoff weight more=
|more general='''Fuel capacity:''' 40 gallons plus 10 gallon gravity feed auxiliary
|engine (prop)=[[Gnome Omega|Gnome Omega-Omega]]
|type of prop=14 cylinder air-cooled rotary engine
|number of props=1
|power main=100 hp
|power alt=75 kW
|power original=, with 2:1 chain reduction
|propeller or rotor?=propeller
|propellers=two-bladed
|number of propellers per engine=1
|propeller diameter main=11 ft 6 in
|propeller diameter alt=3.51
|max speed main= 60 mph
|max speed alt= 97 km/h
|max speed more=
|cruise speed main=
|cruise speed alt=
|cruise speed more=
|stall speed main=
|stall speed alt=
|stall speed more=
|never exceed speed main=
|never exceed speed alt=
|range main=
|range alt=
|range more=
|ferry range main=
|ferry range alt=
|ferry range more=
|endurance=
|ceiling main=
|ceiling alt=
|ceiling more=
|climb rate main=
|climb rate alt=
|climb rate more=
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
}}

==References==

===Notes===
{{reflist|refs=
<ref name="ee">Ransom and Fairclough 1987, pp. 102-109</ref>
}}

===Bibliography===
{{refbegin}}
*{{cite book|last=Bruce|first=J.M.|title=The Aeroplanes of the Royal Flying Corps (Military Wing)|year=1982|publisher=Putnam|location=London|isbn=0-370-30084-X}}
*{{cite book|last=Ransom|first=Stephen|first2=Robert|last2=Fairclough|title=English Electric Aircraft and their predecessors|year=1987|publisher=Putnam|location=London|isbn=0-85177-806-2}}
*{{citation|url=http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200437.html|journal=Flight|title=The Coventry Ordnance Biplane|date=18 May 1912|pages=437–441}}
{{refend}}

==External links==
{{Commons category|COW Biplane}}

[[Category:British military aircraft 1910–1919]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Coventry Ordnance Works aircraft|Biplane]]