{{stack begin}}
{{Infobox architectural practice
| name                  = 5468796 ARCHITECTURE
| image                 = 
| caption               = 
| architects            =
| partners              =  Sasa Radulovic (b. 1972), Johanna Hurme (b. 1975), Colin Neufeld (b. 1975)
| city                  = [[Winnipeg]]
| coordinates           = <!-- Use {{coord}} -->
| founded               = 2007
| dissolved             = 
| awards                = 
| significant_projects  = 
| significant_design    = 
}}

[[File:Bloc10 exterior 5468796 Architecture.jpg|thumb|Bloc10 exterior 5468796 Architecture]]

[[File:OMS Stage 5468796 Architecture.jpg|thumb|OMS Stage 5468796 Architecture]]

[[File:Centre Village by 5468796 Architecture.jpg|thumb|Centre Village by 5468796 Architecture, Winnipeg, Manitoba]]

[[File:Guertin Boatport by 5468796 Architecture.jpg|thumb|Guertin Boatport by 5468796 Architecture, Winnipeg, Manitoba]]

'''5468796 Architecture''' is a Winnipeg-based architecture practice founded in 2007. The practice name incorporates its company registration number.

==Background==
Johanna Hurme (MAA AAA SAA OAA MRAIC SAFA LEED a.p.) and Sasa Radulovic (MAA AAA SAA OAA MRAIC LEED.a.p.) founded 5468796 Architecture in 2007, and were later joined by Colin Neufeld (MAA MRAIC). The practice promotes a collaborative approach amongst its 12 members, leading a new wave of contemporary architecture in Winnipeg. The firm has been described as "Canada’s most exciting new architectural firm in a decade, one dedicated to applying design innovation to the humblest of tasks, a plains-born, good-humoured, resolutely resourceful verve for building modestly, but with elegance." <ref>Trevor Boddy, ''Article about 5468796 Architecture'' in [[Architectural Review]]</ref> The Houston-based Rice Design Alliance recently stated that they “truly believe 5468796 to be one of the most talented young design firms worldwide.”<ref>Lonnie Hoogeboom, ''2014 Spotlight Prize Goes to 5468796'', Rice Design Alliance</ref>

One of the first projects to gain the firm international recognition was ''OMS Stage,''<ref name="AchDaily">[http://www.archdaily.com/114274/oms-stage-5468796-architecture/ Architecture Daily Article]</ref> which is located in [[Winnipeg]]'s historic [[Exchange District]].

In 2012, '''5468796 Architecture''' and Jae-Sung Chon (B.Sc.Arch., G.Dip., M.Arch.) <ref>[http://umanitoba.ca/faculties/architecture/facstaff/faclist/chon.html U of M Staff]</ref> were selected as Canada’s official entry to the 13th International Architecture Exhibition - [[La Biennale di Venezia]] for their exhibition [http://migratinglandscapes.ca/ Migrating Landscapes]. In 2013, '''5468796 Architecture''' was selected by the [[Canada Council for the Arts]] as recipients of the Professional Prix de Rome in Architecture for their travel and research project, [http://www.tablefor12.ca/ Table for 12].

==Recognition==
Since its founding, the firm's work has been recognized through national and international awards and distinctions<br/> including:
*Rice Design Alliance Spotlight Prize
*Canada Council for the Arts Professional Prix de Rome in Architecture
*[[Royal Architectural Institute of Canada]] Emerging Architectural Practice Award
*WAN 21 for 21 Award
*[[Governor General’s Award]] for Architecture (2)
*[[Architectural Review]] Emerging Architecture Awards (2)
*Arthur Erickson Memorial Award
*[[Royal Architectural Institute of Canada]] Award of Excellence (2)
*[[Architectural Record]] Design Vanguard,
*Progressive Architecture Awards (2)
*Canadian Architect Awards of Excellence (4)
*Prairie Design Awards (5)

== Selected awards ==
*2014: Canadian Architect - Award of Excellence, Arthur Residence <ref>{{cite web|title=2014 Canadian Architect Awards of Excellence winners announced |publisher=Canadian Architect |url=http://www.canadianarchitect.com/news/2014-canadian-architect-awards-of-excellence-winners-announced/1003396390/}}</ref>
*2014: World Architecture Festival - Future Project of the Year, AGGV <ref>{{cite web|title=Spiny gallery extension by 5468796 named Future Project of the Year 2014 |publisher=Dezeen |url=http://www.dezeen.com/2014/10/03/art-gallery-of-greater-victoria-5468796-future-project-of-the-year-world-architecture-festival-2014/}}</ref>
*2014: Rice Design Alliance Spotlight Prize <ref>{{cite web|title=2014 Spotlight Prize Goes to 5468796 |publisher=Rice Design Alliance |url=http://www.ricedesignalliance.org/2014/2014-spotlight-prize-goes-to-5468796/}}</ref>
*2014: Mies Crown Hall Americas Prize for Emerging Architecture, OMS Stage [shortlisted] <ref>{{cite web|title=Inaugural MCHAP.emerge Winner Announced |publisher=IIT College of Architecture |url=https://arch.iit.edu/life/inaugural-mchapemerge-winner-announced}}</ref>
*2014: Governor General's Medal in Architecture, OMS Stage <ref>{{cite web|title=Governor General's Medals in Architecture 2014 Recipients |publisher=[[Royal Architectural Institute of Canada]] |url=http://www.raic.org/honours_and_awards/awards_gg_medals/2014recipients/index_e.htm}}</ref>
*2013: World Architecture News - 21 for 21, <ref>{{cite web|title=2013 WAN 21 for 21 |publisher=[[World Architecture News]] |url=http://www.worldarchitecturenews.com/index.php?fuseaction=wanappln.projectview&upload_id=22497}}</ref>
*2013: Royal Architectural Institute of Canada - Award of Excellence, Bloc_10 <ref>{{cite web|title=2013 Awards of Excellence |publisher=[[Royal Architectural Institute of Canada]] |url=http://raic.org/honours_and_awards/awards_raic_awards/2013recipients/innov_bloc10_e.htm}}</ref>
*2013: RAIC Emerging Architectural Practice Award <ref>{{cite web|title=5468796 Emerging Architectural Practice Award |publisher=RAIC |url=http://raic.org/honours_and_awards/awards_emerging/2013/recipient_e.htm}}</ref>
*2013: Architizer A+ Award, Guertin Boatport <ref>{{cite web|title=Guertin Boatport |publisher=Architizer |url=http://www.architizer.com/en_us/projects/view/guertin-boatport/36926/#.UVmzc6t35bU}}</ref>
*2013: Heritage Winnipeg Special President's Award, The Avenue on Portage <ref>{{cite web|title=HW's 28th Annual Preservation Awards |publisher=Heritage Winnipeg |url=http://www.heritagewinnipeg.com/blog.html?filter_category=24}}</ref>
*2012: Architectural Review - Emerging Architecture Award, Bloc_10 <ref name="ArchitecturalReview">{{cite web|title=Bloc 10 Housing, Winnipeg, Canada by 5468796 |publisher=[[The Architectural Review]] |first=Trevor |last=Boddy |date=November 23, 2012 |url=http://www.architectural-review.com/buildings/bloc-10-housing-winnipeg-canada-by-5468796/8638712.article?blocktitle=Winners&contentID=6111}}</ref>
*2012: Prairie Wood Design Awards, Bloc_10 <ref>{{cite web|title=Prairie Wood Design Awards 2013 |publisher=Wood WORKS!|url=http://www.wood-works.org/index.php?option=com_content&view=article&id=25&Itemid=135}}</ref>
*2012: Governor General's Medal in Architecture, Bloc_10 <ref>{{cite web|title=Governor General's Medals in Architecture 2012 Recipients |publisher=[[Royal Architectural Institute of Canada]] |url=http://raic.org/honours_and_awards/awards_gg_medals/2012recipients/index_e.htm}}</ref>
*2012: Architect Magazine P/A Award, Bond Tower <ref>{{cite web|title=First Award: Bond Tower |date=February 7, 2012 |publisher=[[Architect (magazine)|Architect]] |url=http://www.architectmagazine.com/office-projects/bond-tower.aspx}}</ref>
*2011: Canadian Architect - Award of Merit, Bond Tower <ref>{{cite web|title=2011 Canadian Architect Awards of Excellence winners announced |publisher=Canadian Architect |url=http://www.canadianarchitect.com/news/2011-canadian-architect-awards-of-excellence-winners-announced/1000762198/}}</ref>
*2011: Canadian Interiors - Best of Canada Award, OMS Stage <ref>{{cite web|title=Best of Canada |publisher=Canadian Interiors |url=http://www.canadianinteriors.com/bestofcanada/default.aspx}}</ref>
*2011: AZ Awards, OMS Stage <ref>{{cite web|title=1st Annual AZ Award winners announced |publisher=[[Azure (design magazine)|Azure]] |date=June 16, 2011 |url=http://www.azuremagazine.com/article/1st-annual-az-award-winners-announced/}}</ref>
*2011: Winnipeg Art Council Awards - Making a Mark Award, 5468796 Architecture <ref>{{cite web|title='Fireworks' for arts notables |publisher=[[Winnipeg Free Press]] |date=June 10, 2011 |first=Alison |last=Mayes |url=http://www.winnipegfreepress.com/local/fireworks-for-arts-notables-123611609.html}}</ref>
*2011: Royal Architectural Institute of Canada - Award of Excellence, OMS Stage <ref>{{cite web|title=2011 Awards of Excellence |publisher=[[Royal Architectural Institute of Canada]] |url=http://www.raic.org/honours_and_awards/awards_raic_awards/2011recipients/oms_e.htm}}</ref>
*2010: Canadian Architect - Award of Excellence, Bloc_10 <ref>{{cite web|title=2010 Canadian Architect Awards of Excellence winners announced |date=December 21, 2010 |publisher=Canadian Architect |url=http://www.canadianarchitect.com/news/2010-canadian-architect-awards-of-excellence-winners-announced/1000396670/}}</ref>
*2010: Architectural Review - Award for Emerging Architecture, OMS Stage <ref>{{cite web|title=OMS Stage by 5468796 Architecture, Old Market Square, Winnipeg, Canada |publisher=[[The Architectural Review]] |date=November 24, 2010 |url=http://www.architectural-review.com/buildings/oms-stage-by-5468796-architecture-old-market-square-winnipeg-canada/8608330.article}}</ref>
*2010: Architect Magazine P/A Award, BGBX <ref>{{cite web|title=BGBX |publisher=[[Architect (magazine)|Architect]] |first=Katie |last=Gerfen |date=January 20, 2010 |url=http://www.architectmagazine.com/multifamily/bgbx.aspx}}</ref>
*2009: Canadian Architect - Award of Merit, YouCube <ref>{{cite web|title=2009 Canadian Architect Awards of Excellence winners announced |publisher=Canadian Architect |date=December 20, 2009 |url=http://www.canadianarchitect.com/news/2009-canadian-architect-awards-of-excellence-winners-announced/1000351330/}}</ref>

== Selected projects ==
* The Avenue on Portage; Winnipeg, MB<ref>[http://www.theavenueonportage.com/ The Avenue (Credits end page)]</ref><ref>[http://archrecord.construction.com/projects/building_types_study/adaptive_reuse/2013/1302-avenue-building-renovation-5468796-architecture.asp Architectural Record Article]</ref>
* Manitoba Start; Winnipeg, MB <ref>[http://www.interiordesign.net/slideshow/2782-The_New_Wave_5468796_Architecture.php Interior Design Article]</ref>
* Migrating Landscapes; 2012 [[Venice Biennale]] in Architecture 
* WRHA on Hargrave; Winnipeg, MB <ref>[http://spacing.ca/toronto/2012/10/04/no-mean-city-tonight-5468796s-colin-neufeld-at-ryerson-in-toronto/ Spacing Article]</ref>
* bloc_10; Winnipeg, MB <ref>[http://www.treehugger.com/green-architecture/bloc-10-gorgeous-green-modern-multifamily-built-winnipeg.html Treehugger Article]</ref>
* OMS Stage; Winnipeg, MB<ref name="AchDaily" />

== Sources ==
* William Hanley, "The Box Outside", ''Architectural Record'', 2013 <ref>{{cite web|title=The Box Outside: A revamped commercial building brings drama to a frequently frozen city's main street. |date=February 2013 |first=William |last=Hanley |publisher=[[Architectural Record]] |url=http://archrecord.construction.com/projects/building_types_study/adaptive_reuse/2013/1302-avenue-building-renovation-5468796-architecture.asp}}</ref>
*"The Sky's The Limit", ''Gestalten'', 2012 <ref>{{cite book|title=The Sky's the Limit |first1=Robert |last1=Klanten |first2=Sven |last2=Ehmann |first3=Sofia |last3=Borges |publisher=[[Gestalten]] |year=2012 |isbn=9783899554229 |url=http://shop.gestalten.com/skys-the-limit.html}}</ref> 
* Trevor Boddy, "BLOC 10 HOUSING, WINNIPEG, CANADA BY 5468796", ''Architectural Review'', 2012 <ref name="ArchitecturalReview" />
* Alex Bozikovic, "A condo that pushes theory into the built world", ''Globe & Mail'', 2012 <ref>{{cite news|title=A condo that pushes theory into the built world |first=Alex |last=Bozikovic |newspaper=[[The Globe and Mail]] |date=June 28, 2012 |url=http://www.theglobeandmail.com/life/home-and-garden/architecture/a-condo-that-pushes-theory-into-the-built-world/article4376427/%5d/}}</ref>
* John Bentley Mays, "Migrating to Venice", ''Canadian Architect'', 2012 <ref>{{cite web|title=Migrating to Venice |first=John Bentley |last=Mays |publisher=Canadian Architect |date=July 1, 2012 |url=http://www.canadianarchitect.com/news/migrating-to-venice/1001545713/}}</ref>
* William Hanley, "What’s in a name? A Canadian firm connects its collective identity to its practice and projects", ''Architectural Record'', 2011 <ref>{{cite web|title=What’s in a name? A Canadian firm connects its collective identity to its practice and projects. |first=William |last=Hanley |publisher=[[Architectural Record]] |url=http://archrecord.construction.com/features/designvanguard/2011/5468796-Architecture.asp}}</ref>
* Terri Fuglem, "Home away from home", ''Canadian Architect'', 2011 <ref>{{cite web|title=Home Away From Home |date=April 1, 2011 |first=Terri |last=Fuglem |publisher=Canadian Architect |url=http://www.canadianarchitect.com/news/home-away-from-home/1000408259/}}</ref>

== References ==
{{reflist}}

== External links ==
* [http://www.5468796.ca/ 5468796 Architecture (official website)]
* [http://migratinglandscapes.ca/ Migrating Landscapes (official website)]

{{Commonscat|5468796 Architecture}}
<!-- Categories -->
[[Category:Architecture firms of Canada]]