{{Infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = A Tale of the Ragged Mountains
| title_orig    = 
| image         =[[File:Poe ragged mountains.JPG|200px]] 
| caption       =Illustration for "Tales and poems - vol.2" published in 1800's
| translator    = 
| author        = [[Edgar Allan Poe]]
| country       = United States
| language      = English
| series        = 
| genre         = [[Adventure]]<br />[[Science fiction]]<br />[[Short story]]
| published_in   = ''[[Godey's Lady's Book]]''
| publisher     = [[Louis Antoine Godey]]
| media_type    = Magazine
| pub_date  = 1844
| english_pub_date = 
| preceded_by   = 
| followed_by   = 
}}
"'''A Tale of the Ragged Mountains'''" is a [[short story]] written by [[Edgar Allan Poe]] partially based on his experiences while a student at the [[University of Virginia]]. Set near [[Charlottesville, Virginia|Charlottesville]], it is the only one of Poe's stories to take place in Virginia. It was first published in ''[[Godey's Lady's Book]]'' in April 1844<ref>Sova, Dawn B. ''Edgar Allan Poe: A to Z''. New York: Checkmark Books, 2001: 231. ISBN 0-8160-4161-X</ref> and was included in Poe's short story collection ''Tales'', published in New York by Wiley and Putnam in 1845. There is a Spanish translation by [[Julio Cortázar]].

== Plot summary ==
Set in late November 1827, the tale is begun by an unidentified narrator whose story is the loose outer frame for the central tale of Augustus Bedloe, a wealthy young invalid whom the narrator has known "casually" for eighteen years yet who still remains an enigma. Because of ongoing problems with [[neuralgia]], Bedloe has retained the exclusive services of 70-year-old physician Dr. Templeton, a devotee of [[Franz Mesmer]] and the doctrine of [[animal magnetism]], also called "mesmerism".

Augustus Bedloe had met the doctor previously at Saratoga where Bedloe seemed to benefit from Templeton's ministrations. Though they most likely met in a medical context at the [[Saratoga Springs, New York#The Springs|Saratoga mineral springs]].

The unidentified narrator recites the tale as told by Bedloe, delivered after his late return from one of his customary long rambles in the [[Ragged Mountains]], "the chain of wild and dreary hills that lie westward and southward of Charlottesville". At 9:00 that morning, after a breakfast of strong [[coffee]] and [[morphine]] for the pain of his neuralgia, Bedloe leaves Charlottesville heads towards the Ragged Mountains. About an hour later, he enters a gorge of "absolutely virgin" solitude, filled with a "thick and peculiar mist" in which the visual beauty of his surroundings stands out to him in delightful brilliance as the morphine takes effect.

Soon, Bedloe hears unexpected drumming and a metallic rattling sound after which he is startled when "a dusky-visaged and half-naked man rushed past... with a shriek" followed by a hyena. Overcome by this bizarre encounter, Bedloe sits beneath a tree and suddenly notices that its shadow is that of a [[Arecaceae|palm tree]] not native to Virginia. In "perfect command" of his senses, Bedloe notices a strange odor on the breeze and hears a low murmur after which a wind clears the fog and he sees "an Eastern-looking city, such as we read of in [[One Thousand and One Nights|the Arabian Tales]]" later identified by Dr. Templeton as [[Benares]]. Bedloe descends into the city and eventually finds himself barricaded in a kiosk with British officers as a battle rages. In the fighting Bedloe is killed by an arrow shaped like "the writhing [[kris|creese]] of the Malay" that strikes him in the temple.

As Bedloe recounts his inexplicable journey and return, Dr. Templeton strangely seems to know the story already. When the unnamed narrator challenges Bedloe's claim of death, the odd rapport between the doctor and the patient becomes evident as Bedloe trembles in pale silence while Templeton stares with bulging eyes and chattering teeth.

Bedloe then describes the physical and mental experiences of disembodiment and re-embodiment punctuated by distinct [[galvanism|galvanic shocks]], on his return from Calcutta to the Ragged Mountains. Though Bedloe cannot ultimately dismiss his adventure as a dream, the story concludes ambiguously leaving us with suggestions that either the power of Dr. Templeton's writing caused Bedloe's disembodied time-travel experience or that Bedloe is the reincarnation of Templeton's friend Oldeb who died in 1780 fighting alongside [[Warren Hastings]] and a group of British soldiers and [[sepoys]] during the insurrection of Cheyt Singh.

The story ends with Bedloe's death by the accidental application of a poisonous "sangsue" (sanguisuge) or [[leech]] to relieve a "great determination of blood to the head". The narrator completes the tale with a note of astonished perplexity upon reading the obituary and finding Bedloe's name accidentally spelled without the "e", making "Bedlo" the perfect reverse of "Oldeb".

== Criticism ==
Some critics, like E.F. Bleiler and Doris V. Falk, criticize the story for its lack of clarity and literary sophistication. They argue that its ambiguity hurts rather than helps its literary quality, especially in the canon of Poe's characteristically well-elaborated works. Other critics look to the focus on mesmerism as a source of its weakness. They argue that it gives the story too scientific a slant that distracts from its strengths in plot and style.<ref>Falk, Doris V. "Poe and the Power of Animal Magnetism" as collected in ''PMLA'', May 1969. p. 536-546</ref> One scholar claims that Poe is describing a case of [[Marfan syndrome]] in Augustus Bedloe more than five decades before [[Antoine Marfan]] presented his first and famous patient, five-year old Gabrielle, to a French medical association.<ref>Battle, Robert. "Edgar Allan Poe: A Case Description of Marfan Syndrome in an Obscure Short Story". ''American Journal of Cardiology''. 2011: 108(1):148-9.</ref>

== References ==
{{reflist}}

== External links ==
* {{wikisource-inline}}
* [http://www.eapoe.org/works/info/pt046.htm Publishing history of "A Tale of the Ragged Mountains"] from the [http://www.eapoe.org Edgar Allan Poe Society online]
* "[http://www.eapoe.org/papers/psblctrs/pl19741.htm Poe, Creator of Words]"
* {{librivox book | title=A Tale of the Ragged Mountains | author=Edgar Allan Poe}}
{{Edgar Allan Poe}}

{{DEFAULTSORT:Tale of the Ragged Mountains, A}}
[[Category:Short stories by Edgar Allan Poe]]
[[Category:1844 short stories]]
[[Category:Works originally published in Godey's Lady's Book]]
[[Category:Virginia in fiction]]