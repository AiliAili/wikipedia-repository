{{about|the tavernkeeper|the museum in Alexandria, Virginia|Gadsby's Tavern Museum}}
{{Orphan|date=December 2015}}

{{Infobox person
| name        = John Gadsby
| death_date  = {{Death date and age|1844|5|15|1766|1|1}}
| birth_place = Possibly [[Brighton]], [[Essex|Essex County]], [[England]], [[UK]]
| death_place = [[Washington, DC]], [[United States]]
| spouse      = Margaret Smelt (?-1805?, her death)<br><br>Margaret “Peggy” McLaughlin (1805-1812, her death)<br><br>Providence “Provey” (Norris) Langworthy (1813-1844, his death)
| children    = by Margaret Smelt:<br>Ann Sophia (Gadsby) Newton<br>Margaret Sarah (Gadsby) Chapman, mother of [[John Gadsby Chapman]]<br><br>by Peggy McLaughlin:<br>William Gadsby<br><br>by Provey (Norris) Langworthy:<br>Virginia Gadsby<br>Augusta (Gadsby) McBlair<br>Julia (Gadsby) Ten Eyck
| occupation  = tavernkeeper, hosteller and businessman 
}}

'''John Gadsby''' (1766&nbsp;– 15 May 1844) was an [[England|English]] [[tavern]]keeper in [[Alexandria, Virginia|Alexandria]], [[Baltimore]], and [[Washington, DC]].

==Early life and migration to Alexandria==
Born in 1766 in [[England]],<ref name=FindAGrave>{{cite web|title=John Gadsby|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=81022536|work=Find-A-Grave|accessdate=24 June 2013}}</ref> John Gadsby's earliest known record in the [[United States]] is in 1795.<ref name=HCC>{{cite book|last=Roberts|first=Rebecca Boggs and Sandra K. Schmidt|title=Historic Congressional Cemetery|year=2012|publisher=Arcadia Publishing|location=Charleston, SC|isbn=9780738592244|page=72|url=https://books.google.com/books?id=VRRI147PnMkC&pg=PA72}}</ref> He traveled with his wife, Margaret Smelt, and his two daughters, Ann Sophia and Margaret Sarah, to [[Alexandria, Virginia]], then a major shipping port.

His first establishment was at the Union Tavern and Coffee House in Alexandria, a subscription coffee house that catered to business men in the middle and upper classes.<ref name=Union>{{cite news|title=Union Tavern, Union St., Alexandria|newspaper=Washington Gazette|date=31 Dec 1796}}</ref>

==City Hotel==
{{main article|Gadsby's Tavern Museum}}
On October 6, 1796 Gadsby leased the City Hotel from John Wise, now the site of [[Gadsby's Tavern Museum]] in Alexandria.<ref name=Philly>{{cite news|last=Reed|first=Bill|title=Potomac partners Alexandria, Va.|url=http://articles.philly.com/2007-06-03/news/25234780_1_scottish-stabler-leadbeater-apothecary-museum-ramsay-house|accessdate=24 June 2013|newspaper=Philly.com|date=3 June 2007}}</ref>  The tavern quickly became the center for community events, from the George Washington birthnight ball to dancing assemblies to meetings of local clubs, particularly the Alexandria Jockey Club of which Gadsby was a benefactor.<ref name=Moore>{{cite book|last=Moore|first=Gay Montague|title=Seaport in Virginia: George Washington's Alexandria|year=2009|publisher=Project Gutenberg|url=http://www.gutenberg.org/files/30747/30747-8.txt}}</ref>  He made money renting his carriages and investing in a stagecoach line.

When he renegotiated his lease, Gadsby's property and possessions totaled $22,441.<ref name=NPS>{{cite web|title=Member Details: Gadsby's Tavern Museum|url=http://www.nps.gov/subjects/ugrr/ntf_member/ntf_member_details.htm?SPFID=7282|work=Network to Freedom|publisher=National Park Service|accessdate=24 June 2013}}</ref>  His most valuable "possessions" were the [[Slavery in the United States|enslaved]] African-Americans who comprised the majority of his staff.

By 1805, Gadsby's first wife Margaret had died, and he married Peggy McLaughlin of Georgetown.<ref name=FindAGrave /> He then quickly married off his daughters. First, Margaret Sarah Gadsby was married to Charles Thomas Chapman in 1807, and the couple welcomed [[John Gadsby Chapman]] by 1808. In July of that year Ann Sophia Gadsby married Augustine Newton.

Though at that point Gadsby still had eight years remaining on his lease with John Wise, he abruptly sold it and moved to [[Baltimore]], possibly for economic opportunity.

==Indian Queen Hotel==
The Indian Queen Hotel, at Hanover and Baltimore Streets,<ref name=Scharf>{{cite book|last=Scharf|first=John Thomas|title=History of Baltimore City and County, from the earliest period to the present day: including biographical sketches of their representative men|year=1881|publisher=L.H. Everts|location=Baltimore|page=514|url=https://books.google.com/books?id=6tF4AAAAMAAJ&pg=PA514}}</ref> was a sizeable tavern that showed off new mechanical innovations, from a coffee roaster to mechanical spits. In addition to his tavern's position as a stagecoach station on four lines, this hostelry par excellence brought Gadsby both a fortune and an excellent reputation in Baltimore. Guest [[Samuel Breck (politician)|Samuel Breck]] wrote:

{{quote | We alighted at the Indian Queen in Market Street, John Gadsby in a style exceeding anything that I recollect to have seen in Europe or America. This inn is so capacious that it accommodates two hundred lodgers, and has two splendid billiard-rooms, large stables and many other appendages. The numerous bed-chambers have all bells, and the servants are more attentive than in any public or private house I ever knew.<ref>{{cite book|last=Scudder|first=H.E.|title=Recollections of Samuel Breck with Passages from his Note-Books|year=1877|publisher=Porter and Coates|location=Philadelphia|page=266|url=https://books.google.com/books?id=9uOgq_EMnkIC&pg=PA266#v=onepage&q&f=false}}</ref>}}

On May 10, 1808, Gadsby and Peggy welcomed a son, William.<ref name=FindAGrave /> But on February 12, 1812, Peggy passed away, and a short 11 months later, Gadsby married Providence "Provey" (Norris) Langworthy, twenty years his junior.<ref name=FindAGrave /> The Norris family was prominent in Baltimore and attended numerous events at the Indian Queen Hotel. The patriarch of the family, Provey's father Benjamin Bradford Norris, was significant in town for being a signer of the [[Bush Declaration]]. The couple had three children in Baltimore: Virginia (1813), Augusta (1816), and Julia (1818). They also had a son, Charles, who died as a young child.<ref name=FindAGrave />

Of the many notable guests Gadsby welcomed before, during, and after the [[Battle of Baltimore]], [[Francis Scott Key]] would become the most significant. Key finished his song the "[[Defence of Fort McHenry]]," now better known as "[[The Star-Spangled Banner]]," at the Indian Queen on September 16, 1814.<ref name=FoxKey>{{cite news|last=Rogers|first=Douglas|title=Original national anthem manuscript reunited with Francis Scott Key|url=http://www.foxnews.com/travel/2013/06/17/original-national-anthem-manuscript-reunited-with-francis-scott-key/|accessdate=24 June 2013|newspaper=Fox News|date=17 June 2013}}</ref>

During the [[Panic of 1819]], Gadsby sold his lease and left Baltimore nearly penniless. The reasons behind his financial downfall are unsure.

Gadsby looked toward Washington Hall Hotel in [[Philadelphia]], the next stop on his stagecoach line to the north, for his next venture. He was in negotiations to take over the hotel, but its loss by fire rendered his plans moot.<ref name="WHH fire">{{cite news|title=Fire at Philadelphia|newspaper=Baltimore Patriot|date=19 March 1823}}</ref> Instead he looked to the next stagecoach stop to the south: Franklin Hotel in [[Washington, DC]].

==Franklin Hotel==
In 1824, Gadsby bought up the Franklin Hotel, which had a reputation for hosting the most distinguished men in the nation as they visited Washington. Quickly, the Franklin Hotel became known as Gadsby's Hotel.

By 1826, Gadsby's Hotel boasted a billiard room and an extensive coach house as well as parlors, private suites, and rooms for dozens of men. Not all visitors were impressed, however; [[Lukas Vischer (collector)|Lukas Vischer]], a Swiss traveler, chronicled the following:

{{quote | This inn passes for the foremost in the metropolis. Gadsby... in Baltimore...went bankrupt, and now truly starveling conditions reign in his house. All is aimed at pretense. Five chafing-dishes permanently decorate the table; butter and occasionally radish is put upon them. The lunch consisted of a poor soup and two main dishes, roast beef and ham, roast veal and fish, and so on; vegetables scarcely sufficient for 2 or 3 persons, almost every day fried chicken which in fact are parched cocks with really not the least to gnaw off...Gadsby is a scoundrel who want to do it right by making empty compliments...<ref name=Vischer>{{cite journal|last=Feest|first=Christian F.|title=Lukas Vischer in Washington: A Swiss View of the District of Columbia in 1825|journal=Records of the Columbia Historical Society |year=1974 |volume=49|page=90|url=http://www.jstor.org/stable/40067736|accessdate=25 June 2013}}</ref>
}}

Since the carriage ride from the Franklin to the Capitol took forty minutes,<ref name=HCC /> the location was not ideal and Gadsby looked elsewhere. The Franklin Hotel became a rental property, and Gadsby began to deal in the real estate business with other private dwellings.

==National Hotel==
The grand dining rooms, premier ballrooms, and private suites of the National Hotel, located on Pennsylvania Avenue at Sixth Street, hosted presidents, foreign ministers, congressmen, and prominent travelers.<ref name=HCC /> The hotel was originally a series of rowhouses which Gadsby converted into one <ref name=Weightman>{{cite journal|last=Clark|first=Allen C.|title=General Roger Chew Weightman, a Mayor of the City of Washington|journal=Records of the Columbia Historical Society, Washington, D.C.|year=1919|volume=22|page=63|jstor=40067121}}</ref><ref name=Carrier>{{cite book|last=Carrier|first=Thomas J.|title=Washington D.C.: A Historical Walking Tour|year=1999|publisher=Arcadia Publishing|location=Charleston, SC|isbn=9780738500492|page=44|url=https://books.google.com/books?id=SQSFkNcYvZgC&pg=PA44}}</ref> and contained several stores under the same roof: a bank, a stage office, a wine store, and a lottery office.<ref name=Elliot>{{cite book|last=Elliot|first=Jonathan|title=Historical sketches of the ten miles square forming the District of Columbia|year=1830|publisher=J. Elliot, Jr.|location=Washington|page=314|url=https://books.google.com/books?id=cfsLHcNlHCMC&pg=PA314}}</ref> Two years after the grand opening – the February 22, 1827 George Washington Birthnight Ball at which President [[John Quincy Adams]] and most of his administration was present<ref name="Adams memoirs">{{cite book|last=Adams|first=Charles Francis|title=Memoirs of John Quincy Adams, comprising portions of his diary from 1795 to 1848|year=1876|publisher=J.B. Lippincott & Co.|location=Philadelphia|url=https://archive.org/stream/memoirsofjohnquis08adam/memoirsofjohnquis08adam_djvu.txt}}</ref>  – the hotel was still under construction.

By all accounts, the hotel could accommodate several hundred people.<ref name=Elliot /> A large number of Congressmen resided at Gadsby's Hotel for the season, mostly Congressmen from [[Massachusetts]] and [[South Carolina]]. Some lived at Gadsby's for as many as seven years. [[Andrew Jackson]] stayed at the National Hotel before his inauguration.<ref name="Adams memoirs" />

In 1830, Gadsby owned 39 slaves and employed 4 free black women.<ref>{{cite book|title=U.S. Federal Census for Washington City|year=1830|page=123}}</ref>

[[Jonathan Elliot (historian)|Jonathan Elliot]] praised Gadsby's individualism and perseverance as a businessman as his personal charm:
{{quote | Mr. Gadsby, who superintends indefatigably and courteously, possesses ample experience and peculiar skill in his profession. His spirit of enterprise, liberal system, and moderate charges, and the various conveniences and attractions which are combined in his Hotel, form very strong claims upon the patronage of travelers, and deserve to be widely made known through the press.<ref name=Elliot /> 
}}

In 1835, Gadsby's daughter Augusta married John Hollins McBlair, a grocer and eventual army officer.<ref name=FindAGrave /> Another of his daughters, Virginia, died in July 1836 at the age of 20. She was much lamented in the local papers, where she was remembered as her kindred's "centre of light and life."<ref name=virginiaobit>{{cite news|title=Obituary: Virginia Gadsby|newspaper=Daily National Intelligencer (Washington, DC)|date=27 July 1836}}</ref>

==Retirement to Decatur House==
Gadsby retired in September 1836 at the age of 70, leaving the business to his son William.<ref>{{cite news|last=Gadsby|first=William|title=Classifieds: Gadsby's Hotel|newspaper=Daily Union (Washington, DC)|date=18 September 1845}}</ref> He bought [[Decatur House]], one of the largest residences in DC that had a history of grand entertaining, for $12,000 at auction on October 24, 1836.<ref name=Navy>{{cite web|title=The Decatur House and its Distinguished Occupants|url=http://www.history.navy.mil/library/online/decatur_house.htm|work=Naval Historical Foundation Publication|publisher=Navy Department Library|accessdate=24 June 2013}}</ref>

Ten slaves moved with the Gadsbys from the National Hotel to Decatur House. Two families of slaves, the Kings and the Williams, lived in the two-story structure added to the property by the Gadsbys, one of the few extant examples of urban slave quarters.<ref name="Decatur slaves">{{cite web|title=Decatur House Slave Quarters|url=http://www.whitehousehistory.org/decatur-house/african-american-tour/content/Decatur-House-Slave-Quarters|work=White House History|publisher=White House Historical Association and National Trust for Historic Preservation|accessdate=28 June 2013}}</ref> In addition, an enslaved African American named Rosa Marks lived there even through emancipation, and is now buried in the family vault.<ref name=GadsbyVault>{{cite web|title=Gadsby Vault|url=http://www.congressionalcemetery.org/site-gadsby-vault|work=Historic Congressional Cemetery|accessdate=24 June 2013}}</ref> Occupants of the house included John and Provey, unmarried daughter Julia, married daughter Augusta (with her husband and children), and a young cousin, Mary Augusta Bruff, who would go on to marry William Gadsby.<ref>{{cite web|title=Mary Augusta Bruff Gadsby|url=http://www.findagrave.com/cgi-bin/fg.cgi?page=gr&GRid=81044812|work=Find-A-Grave|accessdate=28 June 2013}}</ref> In 1845, Julia married well to [[John C. Ten Eyck|John Conover Ten Eyck]] who went on to serve as a US Senator from [[New Jersey]].<ref name=FindAGrave />

Gadsby died on May 15, 1844 at the age of 78 and was buried in [[Congressional Cemetery]].<ref name=HCC /> He was remembered as a "well-respected and useful citizen."<ref name=gadsbyobit>{{cite news|title=Deaths: John Gadsby|newspaper=Daily National Intelligencer (Washington, DC)|date=17 May 1844}}</ref>

His wife, Provey, died in 1858,<ref name=FindAGrave /> operating Decatur House as a boarding house before her death.<ref name="decatur history">{{cite web|title=A Brief History of Decatur House|url=http://www.whitehousehistory.org/decatur-house/history_decatur-house.html|work=White House History|publisher=White House Historical Association and National Trust for Historic Preservation|accessdate=28 June 2013}}</ref>

==References==
{{Reflist}}

{{DEFAULTSORT:Gadsby, John}}
[[Category:Washington, D.C.]]
[[Category:Baltimore]]
[[Category:1844 deaths]]
[[Category:Alexandria, Virginia]]
[[Category:1766 births]]
[[Category:English people]]