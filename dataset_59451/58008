{{Infobox journal
| title = Journal of Common Market Studies
| cover = [[File:2014 JCMS cover.gif]]
| editor = Michelle Cini, Amy Verdun
| discipline = [[European studies]]
| abbreviation = J. Common Mark. Stud.
| publisher = [[John Wiley & Sons]] on behalf of the [[University Association for Contemporary European Studies]]
| frequency = Bimonthly
| history = 1962-present
| impact = 1.830
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5965
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5965/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5965/issues
| link2-name = Online archive
| ISSN = 0021-9886
| eISSN = 1468-5965
| OCLC = 39263251
| LCCN = 65071201
}}
The '''''Journal of Common Market Studies''''' is a bimonthly [[peer-reviewed]] [[academic journal]] covering the politics and economics of [[European integration]], focusing principally on developments within the [[European Union]]. It was established in 1962 and is published by [[John Wiley & Sons]] on behalf of the [[University Association for Contemporary European Studies]]. The [[editors-in-chief]] are Michelle Cini ([[University of Bristol]]) and Amy Verdun ([[University of Victoria]]).

== Abstracting and indexing ==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[ABI/INFORM]]
* [[CAB Abstracts]]
* [[CSA Biological Sciences Database]]
* [[CSA Environmental Sciences & Pollution Management Database]]
* [[Current Contents]]/Social & Behavioral Sciences
* [[EconLit]]
* [[International Bibliography of the Social Sciences]]
* [[InfoTrac]]
* [[International Political Science Abstracts]]
* [[Research Papers in Economics]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[Worldwide Political Sciences Abstracts]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.830, ranking it 26th out of 163 journals in the category "Political Science", 13th out of 86 journals in the category "International Relations" and 61st out of 345 journals in the category "Economics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science, International Relations and Economics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]
* [[List of international relations journals]]
* [[List of economics journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1468-5965}}
* [http://www.uaces.org University Association for Contemporary European Studies]

[[Category:English-language journals]]
[[Category:European studies journals]]
[[Category:Publications established in 1962]]
[[Category:Bimonthly journals]]
[[Category:John Wiley & Sons academic journals]]


{{Area-studies-journal-stub}}