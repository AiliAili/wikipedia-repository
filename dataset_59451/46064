{{Ahmadiyya by country}}
The '''Ahmmodiyya Muslem Community, Bangladesh''' or '''Ahmmodiyya Muslem Jama'at Bangladesh''' (Bengali- আহ্মমদীয়া মুসলেম জামা'ত, বাংলাদেশ) is Bangladesh's branch of the worldwide [[Ahmadiyya Muslim Community]] founded in 1889 in India by [[Mirza Ghulam Ahmad]] of [[Qadian]] (1835–1908). Within Bangladesh, the Ahmadiyya Muslim Community have over 100 local branches and can attract up to 10,000 attendees at their national events.<ref name="newreligion.eu">{{cite web|url=http://www.newreligion.eu/2013/02/sectarianism-in-bangladesh-must-be.html |title=Bangladesh: Ahmadiyya persecution overview; New Religion |publisher=newreligion.eu |date= |accessdate=2013-08-08}}</ref>

==History==
[[File:Ahmadiyya annual convention.JPG|thumb|87th annual convention of Ahmadiyya Muslim Jama'at Bangladesh]]
The Ahmadiyya movement reached in [[Bangladesh]] (then Bengal province) in 1905 when a man named Ahmad Kabir Noor Muhammad from [[Chittagong]] pledged allegiance to [[Mirza Ghulam Ahmad]]. The second man to join the community from Bengal was Rais Uddin Khan, of [[Kishorganj]]. His wife Syeda Azizatunnisa also pledged allegiance and she was the first Ahmadi woman from Bengal. In 1909 a student named Mubarak Ali from [[Bogra]] went to [[Qadian]] and also became a member of the community. The Ahmadiyya movement gained speed in 1912 when a well known sage from [[Brahmanbaria]], Moulana Syed Muhammad Abdul Wahed, joined the movement. The Ahmadiyya Muslim Community became officially established in Bengal in 1913 with the name of "Anjuman e Ahmadiyya".<ref>{{cite book|last=Babul|first=Jahangir|title=Ahmadiyyater Itihashe Banglar Shoronio Bektitto|year=2010|publisher=Ahmadiyya Muslim Jamaat Bangladesh|isbn=978-984-99102-0-6|pages=5, 7, 36, 65, 66}}</ref>

==Persecution==
{{Main|Persecution of Ahmadis#Bangladesh|Freedom of religion in Bangladesh#Persecution of Ahmadis}}
Since its establishment in Bangladesh, members of the Ahmadiyya Muslim Community have faced persecution from other Muslim groups. In 1963 two Ahmadis were killed in [[Brahmanbaria]]. In 1992, the Ahmadiyya headquarters in Dhaka were attacked by a mob and a number of [[Quran]]s & other books were burnt. In [[1999 bombing of Khulna|1999, a bomb blast at an Ahmadiyya mosque]] killed seven people. On 29 October 2003, an Ahmadi Imam named Shah Alam in Roghunathpurbak village in Jhikargachha upazila of Jessore was killed.<ref>{{cite web|url=http://www.wluml.org/action/bangladesh-continued-attacks-ahmadiyya-community |title=Bangladesh: Continued attacks on the Ahmadiyya community &#124; Women Reclaiming and Redefining Cultures |publisher=Wluml.org |date= |accessdate=2012-12-05}}</ref> In 2004, the International Khatme Nabuyat Movement (IKNM) besieged several Ahmadiyya mosques countrywide.<ref>{{cite web|url=http://www.thepersecution.org/y2004apr.html |title=Religious Persecution of Ahmadiyya Muslim Community - Updates April-June, 2004 |publisher=Thepersecution.org |date= |accessdate=2012-12-05}}</ref> On 17 June 2010 an angry mob vandalised an Ahmadiyya mosque and the house of an Ahmadiyya believer at Ghatail upazila in Tangail Thursday.<ref>{{cite web|url=http://www.thepersecution.org/world/bangladesh/10/06/ds19.html |title=Ahmadiyyas in Tangail attacked - The Daily Star, Bangladesh |publisher=Thepersecution.org |date= |accessdate=2012-12-05}}</ref> In February 2013, a mob set fire to Ahmadiyya property at a site which had been prepared to hold the community's centenary celebrations, causing tens of millions worth of damage in local currency.<ref name="newreligion.eu"/>

==Countrywide centers==
[[File:Masjid baitul baset chittagong.jpg|thumb|Masjid Baitul Baset chittagong]]
* The Bangali Ahmadiyya Community currently has 120 local chapters across the country, in 425 cities and villages.<ref name="around_the_world_118">Ahmadiyya Muslim Mosques Around the World, pg. 118</ref>
* There are 65 missionaries, an MTA (Muslim Television Ahmadiyya) studio in [[Dhaka]] and a Jamia Ahmadiyya (Missionary Training College).<ref name="around_the_world_118" />
* Maharajpur Mosque in the [[Natore]] District <ref name="around_the_world_119">Ahmadiyya Muslim Mosques Around the World, pg. 119</ref>
* Ahmadiyya Muslim Mosque in [[Khulna]] <ref name="around_the_world_119" />
* Galim Gazi Mosque in Betal, [[Kishoregonj]] <ref name="around_the_world_119" />
* Masjid Baitul Baset, in [[Chittagong]].

== References ==
{{reflist}}

==External links==
*[http://www.ahmadiyyabangla.org/index.htm '''Official website of Ahmadiyya Muslim Community of Bangladesh''']

{{Asia topic|Ahmadiyya in}}

[[Category:Ahmadiyya by country|Bangladesh]]
[[Category:Islam in Bangladesh]]