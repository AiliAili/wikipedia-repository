<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name=V-770 
 |image=Ranger V-770 Inverted.jpg
 |caption=Preserved Ranger V-770
}}
{{Infobox Aircraft Engine
 |type=[[Piston]] [[aircraft engine|aero-engine]]
 |manufacturer= [[Fairchild Ranger|Ranger Aircraft Engine Division]] 
 |first run={{avyear|1931}}
 |major applications= [[Curtiss SO3C Seamew]]
 |number built = 
 |program cost = 
 |unit cost = $11,000 U.S. Dollars ''circa 1944''<ref name="Museum" />
 |developed from = 
 |developed into = 
 |variants with their own articles =
}}
|}

The '''Ranger V-770''' was an American air-cooled inverted [[V12 engine|V-12]] aero-engine developed by the [[Ranger/Fairchild Engines|Ranger Aircraft Engine Division]] of the [[Fairchild Aircraft|Fairchild Engine & Aircraft Corporation]] in the early 1930s.<ref name="Janes">{{citation | title=Jane's Fighting Aircraft of World War II | url= https://books.google.com/books?q=editions:ISBN 0-517-67964-7 | first1= Frederick Thomas | last1= Jane | first2=Leonard | last2=Bridgman | first3=Bill | last3= Gunston | publisher= Random House | location = London | year= 1989 | isbn = 1-85170-493-0}}</ref>

==Design and development==

In 1931, the '''V-770''' design was put to paper, based on the [[Ranger L-440|Ranger 6-440]] series of inverted inline air-cooled engines, and test flown in the [[Vought]] XSO2U-1 Scout. In 1938 it was tested in the Curtiss [[SO3C Seamew]] and found to be unreliable with a tendency to overheat in low-speed flight.<ref>{{citation | title=Aircraft Piston Engines: From the Manly Balzer to the Continental Tiara | first=Herschel H. | last=Smith | publisher=Sunflower University Press | year=1986 | isbn=978-0-89745-079-9 | url=https://books.google.com/books?id=w1XIAAAACAAJ | page= | format=}} {{dead link|date=May 2010}}</ref><ref name="EdenMoeng">{{citation | title=The Complete Encyclopedia of World Aircraft | url=https://books.google.com/books?id=6xMYAAAACAAJ | first1=Paul | last1=Eden | first2=Soph |last2=Moeng | publisher=Amber Books | location = London | year=2002 | isbn=978-0-7607-3432-2}}</ref> By 1941 a more developed '''V-770''' was installed in the [[Fairchild AT-21|Fairchild XAT-14 Gunner]] prototype and was used in the production [[Fairchild AT-21|Fairchild AT-21 Gunner]] gunnery school aircraft.<ref name="Swanborough">{{citation | title=United States Military Aircraft Since 1909 | url=https://books.google.com/books?id=3QZUAAAAMAAJ&q=V-770 | first1=F. G. | last1= Swanborough | first2=Peter M. |last2=Bowers | publisher=Putnam | location =New York | isbn=0-85177-816-X | year=1964}}</ref>

Produced from 1941 to 1945, the '''V-770''' featured a two-piece aluminum alloy crankcase, steel barreled cylinders with integral aluminum alloy fins and aluminum alloy heads. The '''V-770''' was the only American inverted V12-type inline air-cooled engine to reach production. The engine was used in very few Army Air Forces aircraft, among them the short lived [[Fairchild AT-21]] twin-engine bomber trainer,<ref name="Museum">{{citation|url=http://www.nationalmuseum.af.mil/factsheets/factsheet.asp?id=853 |title=Ranger V-770 Inverted |publisher=National Museum of the USAF |accessdate=2008-11-07 |deadurl=yes |archiveurl=https://web.archive.org/web/20081230004530/http://www.nationalmuseum.af.mil:80/factsheets/factsheet.asp?id=853 |archivedate=2008-12-30 |df= }} Includes photo</ref> and in the two [[Bell XP-77]] light-weight fighter prototypes.

==Variants==
;V-770-4: Installed in the [[Vought XSO2U-1]] scout aircraft
;V-770-6: Installed in the [[Fairchild XAT-14 Gunner]] prototype, intended for the [[Ryan SOR-1]] Scout
;V-770-7: Installed in the [[Bell XP-77]] light-weight fighter prototype
;V-770-8: Installed in the [[Curtiss SO3C Seamew]] Scout.<ref name="EdenMoeng" />
;V-770-9: Installed in the [[North American XAT-6E Texan]] prototype.<ref name="Swanborough" />
;V-770-11: Installed in the [[Fairchild AT-21]] Gunner.<ref name="Swanborough" />
;V-770-15: Installed in the [[Fairchild AT-21]] Gunner.<ref name="Swanborough" />
;SGV-770C-1: Tested in the [[Curtiss XF6C-7 Hawk]] Fighter-Bomber at 350&nbsp;hp.<ref name="EdenMoeng" />
;SGV-770C-B1:Installed in the [[Ikarus 214]] prototype
;SGV-770D-5: Developed for post-war commercial use,<ref name="Janes" /> 700&nbsp;hp (kW) at 3,600 RPM, weight 870&nbsp;lb (395&nbsp;kg), height 31.11 in (790&nbsp;mm), length 74.92 in (1,900&nbsp;mm), width 33.28 in (846&nbsp;mm)

==Applications==
* [[Bell XP-77]]
* [[Curtiss SO3C]]
* [[Edo OSE]]
* [[Fairchild F-46]] (Duramold)
* [[Fairchild AT-21]]
* [[Fairchild BQ-3]]
* [[Ikarus 212]]
* [[Ikarus 213]] / Utva 213 Vihor / Government Factories Type 213 Vihor
* [[Ikarus 214]] (prototype)
* [[Vought XSO2U]]

==Specifications (SGV-770C-1)==
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with )</li> and start a new, fully formatted line with <li> -->
|ref=Janes Fighting Aircraft of World War II (1989) <ref name="Janes" />
|type= 12-cylinder inverted Vee piston engine
|bore= 4 in (101.6 mm)
|stroke= 5 <small>{{frac|1|8}}</small> in (130.2 mm)
|displacement= 773 in<sup>3</sup> (12.6 L)
|length= 62 in (1,574.8 mm)
|diameter=
|width= 28 in (711.2 mm)
|height= 32.2 in (817.88 mm)
|weight= 730 lb (331 kg)
|valvetrain= [[Single overhead camshaft]] (SOHC) (1 shaft per bank), gear driven
|supercharger= Single-Speed, Single-Stage, produced 45 inches of mercury (1.5 bar, 7.5 psi) at take-off
|turbocharger=
|fuelsystem= Holley non-icing carburetor
|fueltype= 87 octane petrol
|oilsystem= Full pressure type
|coolingsystem= Air-cooled
|power= 520 hp at 3,150 rpm  (387.7 kW) 
|specpower=  0.673 hp/in<sup>3</sup>
|compression= 6.5:1
|fuelcon=
|specfuelcon=
|oilcon=
|power/weight= 0.71 hp/lb
}}

==Survivors==
*[[Carolinas Aviation Museum]] 1 restored engine in storage
*2 engines in the Davis Aircraft private collection*
*One survives at [[Cincinnati State Aviation school]]
*One V770-7 is at the Museum of Flight restoration center.

==See also==
{{aircontent
<!-- other related articles that have not already linked: -->
|see also=

<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft that are of similar role, era, and capability as this design: -->
|similar aircraft=

<!-- relevant lists that this aircraft appears in: -->
|lists=
*[[List of aircraft engines]]
<!-- For aircraft engine articles.  Engines that are of similar to this design: -->
|similar engines=
*[[Argus As 410]]
*[[Argus As 411]]
*[[de Havilland Gipsy Twelve]]
*[[Isotta Fraschini Delta]]
*[[Walter Sagitta]]

<!-- See [[WP:Air/PC]] for more explanation of these fields. -->
}}

==References==
{{Commons category}}
{{reflist}}

{{Ranger aeroengines}}
{{US military piston aeroengines}}

[[Category:Aircraft air-cooled V piston engines]]
[[Category:Aircraft piston engines 1930–1939]]
[[Category:Inverted V12 aircraft engines]]