{{italic title}}
{{Infobox Journal
| cover = [[File:BTXC small.jpg|200px]]
| editor       = Roger O. McClellan
| discipline   = [[Toxicology]]
| abbreviation = Crit. Rev. Toxicol.
| publisher    = [[Taylor and Francis Group]]
| country      = UK
| frequency    = 10 issues per year
| history      = 1971-present
| openaccess   =
| license      =
| impact       = 5.097
| impact-year  = 2014
| website      = http://www.tandfonline.com/itxc
| link1        =
| link1-name   =
| link2        =
| link2-name   =
| atom         =
| JSTOR        =
| OCLC         =
| LCCN         =
| CODEN        =
| ISSN         = 1040-8444
| eISSN        = 1547-6898
  }}

'''''Critical Reviews in Toxicology''''' is an [[academic journal]] that publishes [[Review journal|review articles]] on the mechanisms, responses and assessments of [[toxin]]s and [[toxicant]]s. It is published by [[Taylor and Francis]].

The editor is [[Roger O. McClellan]], [[Albuquerque]].<ref>{{citation|url=http://www.tandfonline.com/itxc|title=Editor in Chief|location=London|publisher=Taylor and Francis Group|year=2014}}.</ref>

Indexed by [[Web of Science|ISI]] Critical Reviews in Toxicology received an [[impact factor]] of 5.097 as reported in the 2014 [[Journal Citation Reports]] by Thomson Reuters, ranking it seventh out of 87 journals in the category ''Toxicology''.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Toxicology |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate= |work=Web of Science |postscript=.}}</ref>

The journal has been accused of being "broker of junk science" by the [[Center for Public Integrity]].<ref>[http://www.publicintegrity.org/2016/02/18/19307/brokers-junk-science Brokers of junk science?]</ref>

==References==
{{Reflist}}

[[Category:Publications established in 1971]]
[[Category:Toxicology journals]]
[[Category:Taylor & Francis academic journals]]
[[Category:English-language journals]]