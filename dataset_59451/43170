<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Maeda 703
 | image=A-1605前田式703同形機ガル翼.jpg
 | caption=First prototype
}}{{Infobox Aircraft Type
 | type=Single-seat [[glider (sailplane)|glider]]
 | national origin=[[Japan]]
 | manufacturer=
 | designer=Kenichi Maeda
 | first flight=1940
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=3
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Maeda 703''' was one of the first indigenous Japanese [[glider (sailplane)|gliders]], a high performance single seat aircraft which first flew in 1940. Three were built, two with [[gull wing]] wings; one of these set a national endurance record in 1941.

==Design and Development==
[[File:前田式703型直翼A-1607.jpg|right|thumb|Third 703: straight, not gull, wing]]

Gliding had attracted little interest In Japan until the mid-1930s, when a group of enthusiasts managed to fund a visit from [[Wolf Hirth]]. His tour of Japan with two gliders and a tow plane generated imports of [[Germany|German]] gliders, copies of them and eventually true Japanese designs. The Maede 703, built by Kenichi Maede, his engineering colleagues Kimura and Kurahara and with academic input from Hiroshi Sato from Kyushu Imperial College was one of the first of the latter.<ref name=SimonsII/>

The 703 was a wood framed aircraft covered with a mixture of [[plywood]] and [[aircraft fabric covering|fabric]]. Its [[cantilever]] [[monoplane#Types of monoplane|mid-mounted wing]] had a single spar and an associated D-box, skinned in plywood, formed the [[leading edge]]. The wing was fabric covered behind the spar. The leading edge was straight from [[wing root|root]] to [[wing tip|tip]], with slight sweep-back. From root out almost to half-span the wing tapered in plan only gently but further out, where the trailing edge was entirely formed by the [[aileron]] the wing tapered more strongly to a rounded tip. The [[NACA]]-derived [[airfoil]] provided a high maximum [[lift coefficient]] and small [[pitching moment]]s and the wing had [[washout (aviation)|washout]] to avoid [[stall (flight)#Stalling a fixed-wing aircraft|tip stall]]. There were [[air brake (aircraft)|Schempp-Hirth style airbrakes]] mounted on the rear of the spar in the central section, extending above and below the wing. On the first two 703s the central sections were set with strong (6.5°) [[dihedral (aircraft)|dihedral]] and the outer section with none, forming a [[gull wing]]. The third 703 had the same wing but with constant dihedral from root to tip.<ref name=SimonsII/>

The [[fuselage]] of the 703 was entirely ply skinned, tapering gently from a blunt nose to the tail. There, a small, ply skinned [[fin]] carried a largely fabric covered, rounded, wide [[chord (aircraft)|chord]], [[balanced rudder]] which extended down to the keel. The straight tapered [[tailplane]], also largely fabric covered, was set forward of the fin and at the top of the fuselage, so the [[elevator (aircraft)|elevators]] required only a small cut-out for the rudder to move in. The pilot sat upright just ahead of the wing leading edge under a multi-piece [[canopy (aircraft)|canopy]] which merged into the aft fuselage. The 703 had no landing wheel but just a sprung skid from the nose to behind the cockpit, on the deepest part of the fuselage. It was assisted by a tail bumper.<ref name=SimonsII/>

Early tests, beginning in 1940 revealed good handling and performance.<ref name=SimonsII/>

==Operational history==
[[File:A Tadao Kawabe glider pilot.jpg|right|thumb|Tadeo Kawabe in the 703 after landing in the dark at the end of his record flight.]]

Development of the Maeda 703 was stopped by the spread of [[World War II]] to the [[Pacific]], but not before Tadeo Kawabe had set a new national glider endurance record of 13&nbsp;h&nbsp;41&nbsp;m in February 1941 in the second 703, ''A1606''. In 1945 all gliders in Japan, along with most in [[Germany]], were destroyed by Allied forces.<ref name=SimonsII/>

==Specifications==
{{Aircraft specs
|ref=Sailplanes 1920-1945<ref name=SimonsII/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=6.70
|length note=
|span m=14.98
|height m=1.10
|height note=over cockpit<ref name=vsha/>
|wing area sqm=14.3
|wing area note=
|aspect ratio=15.7
|airfoil= MKK3 (developed from [[NACA]] 64016 by Maede, Kinushi and Kuahara )
|empty weight kg=153
|empty weight note=structure
|gross weight kg=230
|gross weight note=maximum
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=25.4:1 at 64.5 km/h (40 mph; 34.8 kn)<ref name=vsha/>
|sink rate ms=0.63
|sink rate note=minimum, at 55 km/h (34.2 mph; 29.7 kn)<ref name=vsha/>
|lift to drag=
|wing loading kg/m2=16.1
|wing loading note=
|more performance=
*'''Landing speed:''' 43 km/h (26.7 mph; 23.2 kn)<ref name=vsha/>
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

<!--==Notes==-->

==References==
{{reflist|refs=

<ref name=SimonsII>{{cite book |title=Sailplanes 1945-1965 |last=Simons |first=Martin |edition=2nd revised |year=2006|publisher= EQIP Werbung & Verlag GmbH|location=Königswinter |isbn=3 9807977 4 0|pages=203–5}}</ref>

<ref name=vsha>{{cite web |url=http://www.vsha.jp/photo/maedasiki-703gata.pdf|title=Vintage Sailplane Historical Association - Maede 703 (in Japanese, machine translatable) |author= |date= |work= |publisher= |accessdate=21 September 2012}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->

[[Category:Japanese sailplanes 1940–1949]]