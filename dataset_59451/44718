{{italictitle}}
{{Infobox journal
|title          = Hispania
| cover         =
| editor        = Sheri Spaine Long
| discipline    = [[Education]]
| language      = [[English language|English]], [[Spanish language|Spanish]], [[Portuguese language|Portuguese]]
| frequency     = Quarterly
| abbreviation  =
| impact        =
| impact-year   =
| publisher     = [[Johns Hopkins University Press]] on behalf of the [[American Association of Teachers of Spanish and Portuguese]]
| country       = United States
| history       = 1917-present
| website       = http://www.press.jhu.edu/journals/hispania/index.html
| link1         = http://www.aatsp.org/?page=Hispania
| link1-name    = Webpage at association's site
| ISSN          = 0018-2133
| eISSN         =
| LCCN          = 2002-227125
| OCLC          = 50709558
| JSTOR         = 00182133
}}
'''''Hispania''''' is a [[Peer review|peer-reviewed]] [[academic journal]] and the official journal of the [[American Association of Teachers of Spanish and Portuguese]].<ref name=history>{{cite web |url=http://www.aatsp.org/?page=History |title=The History of the AATSP - AATSP |format= |work= |accessdate=2010-04-12}}</ref> It is published quarterly by the AATSP and covers [[Spanish language|Spanish]] and [[Portuguese language|Portuguese]] [[literature]], [[linguistics]], and [[pedagogy]]. ''Hispania'' contains three major sections: literature, linguistics, and pedagogy, with a fourth section devoted solely to book and media reviews, which are subdivided into Pan-Hispanic/[[Portuguese Brazilian|Luso-Brazilian]] Literary and Cultural Studies, linguistics, language, media, and fiction and film.

==History==
The first publication of ''Hispania'' dates to the earliest days of the American Association of Teachers of Spanish and Portuguese and the first issue featured a summation of the possibilities of the new organization, written by Lawrence Wilkins, as well as an outline of future plans for the journal written by its founding [[editor in chief]], Aurelio M. Espinosa ([[Stanford University]]).<ref name=Walsh>Walsh, Donald Devenish. Hispania, Vol. 50, No. 4, Fiftieth Anniversary Number (Dec. 1967) p. 823-833</ref> In the outline, Espinosa defined the journal's chief aim as "the betterment of the teaching of Spanish in our schools and colleges".<ref>Espinosa, Aurelio. Hispania, Vol. 1, Organization Number (Nov., 1917) p. 19-23</ref> Accordingly, ''Hispania'''s original subtitle was "A Journal Devoted to the Interests of Teachers of Spanish," which reflected the earnest pedagogic tone and content of its first volumes.<ref name=Walsh/> In keeping with his vision of the journal's purpose, Espinosa published more pedagogical material than any of his successors: fully 64 percent of the articles in the nine volumes he edited were pedagogical in nature.<ref name=Walsh/>

Alfred Coester succeeded Espinosa as editor, a position he held for the next 14 years. During his tenure the journal grew significantly in scope and readership, adding new sections, such as the "Professional Literature" section devoted to reviewing pedagogical articles and books.<ref name=Walsh/> In 1941, a report of the Committee on General Policies led to a revision of ''Hispania'''s scope by recommending among others that its "contents should strike a balance between material of interest to secondary teachers and articles primarily of interest to university members".<ref name=Walsh/> Further revisions called for more articles on Spanish in elementary schools, on relations with Hispanic America, and on attitudes of personal educators toward the teaching of Spanish and Portuguese.<ref name=Walsh/>

Henry Grattan Doyle succeeded Coester in 1942. Doyle had been editor of the ''[[Modern Language Journal]]'' for four years before he became editor of ''Hispania''.<ref name=Walsh/> Doyle instituted the anonymous peer review process that is still in place today. He also increased the number of articles dealing with Luso-Brazilian languages and literatures. Other additions Doyle made to the editorial process included assigning books for review and ceasing to accept unsolicited reviews.

The post [[World War II]] era was ushered in with a new editor, Donald Walsh, in 1949. Walsh is considered by many, including his successor, Robert Mead Jr., to have developed ''Hispania'' into the modern language journal it is today.<ref name=Mead>Mead, Robert G., Jr. Hispania, Vol. 75, No. 4, “The Quincentennial of the Columbian Era.” (Oct., 1992) p. 1083-1086.</ref><ref name=Mead/> Walsh broke with the previous editors' focus on pedagogy by broadening ''Hispania'''s scope, diversifying its contents, adding new sections, soliciting contributions from colleagues known for their expertise, and making sure that every issue of the journal had something of practical use and pedagogical value to both the [[K-12 (education)|K-12 teacher]] and the [[postgraduate education|graduate]]-level professor.<ref name=Mead/> Walsh also instituted the change to a two-column format, which saved the journal space and money. He retired from the editorship in 1957. 

Robert Meade, Jr. ([[University of Connecticut]]) became interim editor in 1958, having already served since 1953 as an associate editor. Meade reiterated the message that articles must be broad enough in scope to interest the more than 5,000 members, nearly half of whom taught in [[secondary school]]s.<ref name=Walsh/> Meade served during a time of rapid growth within the profession due to increased interest in foreign language study as well as increased funding through the [[National Defense Education Act]] (instituted to keep pace with the former USSR in mathematics, science, and foreign languages).<ref name=Walsh/> Meade's editorial leadership increased the number of literary articles published in order to rectify an imbalance between the various sections. 

Seymour Menton ([[University of Kansas]]) succeeded Meade in 1963. During his three-year tenure as editor, Menton also strove to rectify an imbalance in the journal's article ratio by calling for more literary contributions from members.<ref name=Walsh/> He created a second book review section that focused on reviewing books of scholarly and literary interest, as opposed to the original one, which focused on textbook reviews. 

Irving P. Rothberg, who had edited the book review section for nearly nine years prior, succeeded Menton in 1966 and served until 1974. He was in turn succeeded by Donald W. Bleznick, who instituted the inclusion of an editorial policy in each number of ''Hispania'' outlining what the journal would and would not accept for publication, a move intended to reduce the number of unwanted submissions.<ref name=Don>Bleznick, Donald W. Hispania, Vol. 75, No. 4, “The Quincentennial of the Columbian Era.” (Oct., 1992) p. 1083-1086.</ref> Bleznick managed the journal during one of its most trying times, after a fire destroyed its printing facility in [[Appleton, Wisconsin]] in May 1979. He oversaw the reconstruction of the journal and found it a new publisher in time to have it in print again by 1980.<ref name=Don/>

Theodore A. Sackett became editor in 1984 and directed the journal through most the 1980s. Sackett added "Pedagogy" as a special section in ''Hispania'' in order to address the changing face of the classroom that resulted from the advent of computers and the new concern for communicative competence.<ref>Oxford, Raquel. Hispania, Vol. 82, No. 2 (May, 1999), p.293-297.</ref> He was succeeded by Estelle Irizarry in 1993. During her editorship, ''Hispania'' became the first journal in the humanities to digitalize and produce a [[Compact disc|CD]] of articles dating from its inception in 1917 through 1990, a project for which members volunteered to prepare issues or volumes. Irizarry also improved efficiency and reduced decision time. Under her tenure, the journal was awarded second place for the "Phoenix Award for Significant Editorial Achievement" from the [[Council of Editors of Learned Journals]] in 1995.<ref name=award>{{cite web |url=http://www.celj.org/winners |title=Winners &#124; The Council of Editors of Learned Journals |format= |work= |accessdate=2010-04-12}}</ref>

Janet Pérez assumed the editorship in 2000. Her final term ended in 2009. The end of her editorship coincided with her induction as a permanent member of the [[North American Academy of the Spanish Language]] at a ceremony at the [[Instituto Cervantes]] in [[New York City]].

Sheri Spaine Long, the former editor of ''[[Foreign Language Annals]]'', succeeded Pérez in January 2010. Concurrently, [[Johns Hopkins University Press]] became the publisher of ''Hispania''.

== Abstracting and indexing ==
The journal is abstracted and indexed in [[Current Contents]], [[EBSCO Publishing|EBSCOhost]], [[FRANCIS (database)|FRANCIS]], [[Hispanic American Periodicals Index]], [[International Bibliography of Periodical Literature in the Humanities and Social Sciences]], [[Linguistics and Language Behavior Abstracts]], and [[Russian Academy of Sciences Bibliographies]].<ref name=indexing>{{cite web |url=http://www.press.jhu.edu/journals/hispania/indexing.html |title=The Johns Hopkins University Press &#124; Hispania |work= |accessdate=2010-04-12}}</ref>

== External links ==
* {{Official|1=http://www.aatsp.org/?page=Hispania}}
* [http://www.aatsp.org/ American Association of Teachers of Spanish and Portuguese]
* [http://muse.jhu.edu/journals/hispania/ ''Hispania''] at [[Project MUSE]]

== References ==
{{Reflist}}

[[Category:Education journals]]
[[Category:Language education journals]]
[[Category:Multilingual journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1917]]
[[Category:Johns Hopkins University Press academic journals]]