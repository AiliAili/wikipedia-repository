{{Infobox journal
| title = Public Administration Review
| editor =  [[James L. Perry]]
| discipline = [[Public administration]]
| abbreviation = Publ. Admin. Rev.
| publisher = [[Wiley-Blackwell]] on behalf of the [[American Society for Public Administration]]
| frequency = Bimonthly
| history = 1940–present
| impact = 1.973
| impact-year = 2014
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-6210
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-6210/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-6210/issues
| link2-name = Online archive
| ISSN  = 0033-3352
| eISSN = 1540-6210
| JSTOR = 00333352
}}
'''''Public Administration Review''''' is a bimonthly [[Peer review|peer-reviewed]] [[academic journal]] covering research, theory, and practice in the field of [[public administration]]. It was established in 1940 and has been one of the top-rated journals in the field.<ref>{{cite journal |last1=Vocino |first1=Thomas |last2=Elliott |first2=Robert H. |title=Research Note: Public Administration Journal Prestige: A Time Series Analysis |journal=[[Administrative Science Quarterly]] |volume=29 |issue=1 |date=March 1984 |pages=43–51 |doi=10.2307/2393079}}</ref><ref>{{cite journal |last1=Colson |first1=Harold |title=Citation Rankings of Public Administration Journals |journal=[[Administration & Society]] |volume=21 |issue=4 |date=February 1990 |pages=452–471 |doi=10.1177/009539979002100404}}</ref><ref>{{cite journal |last1=Forrester |first1=John P. |last2=Watson |first2=Sheilah S. |title=An Assessment of Public Administration Journals: The Perspective of Editors and Editorial Board Members |journal=Public Administration Review |volume=54 |issue=5 |date=September 1994 |pages=474–482 |doi=10.2307/976433}}</ref> It is the official journal of the [[American Society for Public Administration]], published by [[Wiley-Blackwell]]. The current [[editor-in-chief]] is [[James L. Perry]]. According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 1.973, ranking it 6th out of 46 journals in the category "Public Administration".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Public Administration |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social sciences |accessdate=2016-04-16 |series=[[Web of Science]] |postscript=.}}</ref>

== Former editors-in-chief ==
The following persons have been editors-in-chief of ''Public Administration Review'':
{{columns-list|colwidth=30em|
* 2006-2011 [[Richard J. Stillman II]]
* 2000-2005 Larry D. Terry
* 1997-1999 Irene S. Rubin
* 1991-1996 [[David H. Rosenbloom]]
* 1985-1990 Chester A. Newland
* 1977-1984 [[Louis C. Gawthrop]]
* 1966-1977 [[Dwight Waldo]]
* 1963-1966 [[Vincent Ostrom]]
* 1961-1963 John A. Perkins
* 1958-1960 [[James W. Fesler]]
* 1956-1958 York Wilbern
* 1953-1956 [[Frederick C. Mosher]]
* 1951-1953 Wallace S. Sayre
* 1949-1951 [[Fritz Morstein Marx]]
* 1947-1949 Rowland Egger
* 1945-1947 [[E. Pendleton Herring]]
* 1943-1945 Gordon R. Clapp
* 1940-1943 [[Leonard D. White]]
}}

== References ==
{{Reflist}}

== External links ==
* {{Official|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1540-6210}}

[[Category:Public administration]]
[[Category:Wiley-Blackwell academic journals]]
[[Category:Bimonthly journals]]
[[Category:Political science journals]]
[[Category:Publications established in 1940]]
[[Category:English-language journals]]