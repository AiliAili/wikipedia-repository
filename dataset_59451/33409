{{Infobox single
| Name           = Say Say Say
| Cover          = Say Say Say (album cover art).jpg
| Border         = yes
| Alt            = Against a blue background, "Say Say Say" is printed in pink and takes up the left and bottom of the image. To the right, there is an artwork depiction of two men holding each others' hands in the air.
| Artist         = [[Paul McCartney]] and [[Michael Jackson]]
| Album          = [[Pipes of Peace]]
| B-side         = "Ode to a Koala Bear"
| Released       = {{Start date|1983|10|03|df=y}}
| Format         = {{flatlist|
*[[Single (music)|7"]]
*[[12-inch single|12"]]
}}
| Recorded       = [[Associated Independent Recording|AIR Studios]], May 1981<br />[[Cherokee Studios]], April 1982
| Genre          = [[Post-disco]]
| Length         = 3:55<br />5:40 (remix by [[John "Jellybean" Benitez]])
| Label = [[Parlophone Records|Parlophone]] (UK)<br/>[[Columbia Records|Columbia]] (US)
| Writer = {{flatlist |
* [[Paul McCartney]]
* [[Michael Jackson]]
  }}
| Producer = [[George Martin]]

| Chart position = 
| Chronology = [[Paul McCartney]] singles
| Last single = "[[The Girl Is Mine]]" <br /> (1982)
| This single = "'''Say Say Say'''" <br /> (1983)
| Next single = "[[Pipes of Peace (song)|Pipes of Peace]]" <br /> (1983)
| Misc =
 {{extra chronology
  | Artist         = [[Michael Jackson]] singles
  | Type           = singles
  | Last single    = "[[P.Y.T. (Pretty Young Thing)]]" <br /> (1983)
  | This single    = "'''Say Say Say'''" <br /> (1983)
  | Next single    = "[[Thriller (song)|Thriller]]" <br /> (1984)
  }}
 {{extra track listing
  | Album          = [[Pipes of Peace]]
  | Type           = Studio album
  | prev_track     = "[[Pipes of Peace (song)|Pipes of Peace]]"
  | prev_no        = 1
  | this_track     = "'''Say Say Say'''"
  | track_no       = 2
  | next_track     = "The Other Me"
  | next_no        = 3
  }}
}}
"'''Say Say Say'''" is a song written and performed by [[Paul McCartney]] and [[Michael Jackson]], released in 1983. The track was produced by [[George Martin]] for McCartney's album, ''[[Pipes of Peace]]''.  The song was recorded during production of McCartney's 1982 ''[[Tug of War (Paul McCartney album)|Tug of War]]'' album, about a year before the release of "[[The Girl Is Mine]]", the pair's first duet from Jackson's album ''[[Thriller (Michael Jackson album)|Thriller]]'' (1982).

After its release in October 1983, "Say Say Say" became Jackson's seventh top-ten hit inside a year. It was a number one hit in the United States, Canada, Norway, Sweden, and several other countries, reached number two in the United Kingdom, and peaked within the top ten in Australia, Austria, New Zealand, The Netherlands, Switzerland, and over 20 other nations.  In 2013, ''[[Billboard (magazine)|Billboard]]'' magazine listed the song as the 41st biggest hit of all-time on the [[Billboard Hot 100|''Billboard'' Hot 100]] charts.<ref name= Billboard>{{cite web|url= http://www.billboard.com/charts/greatest-hot-100-singles |title= Greatest of All Time Hot 100 Singles |work= Billboard |accessdate= 7 September 2016}}</ref>

The single was [[Music recording sales certification|certified]] gold by the [[Recording Industry Association of America]] in December 1983, representing sales of one million copies. The single was promoted with a [[music video]] directed by [[Bob Giraldi]]. The video, filmed in [[Santa Ynez Valley]], California, features cameo appearances by [[Linda McCartney]], [[La Toya Jackson]] and Giraldi. The short film centers around two con artists called "Mac and Jack" (played by McCartney and Jackson), and is credited for the introduction of dialogue and storyline to music videos.

==Background, recording, and composition==
McCartney biographer [[Ray Coleman]] asserted that the majority of the song's lyrics were written by Jackson, and given to McCartney the next day.<ref name="coleman 129">Coleman, p. 129.</ref> Recording began at [[AIR Studios]] in London in May 1981. At the time, McCartney was recording ''[[Tug of War (Paul McCartney album)|Tug of War]]'', the former [[The Beatles|Beatle]]'s first solo album after the breakup of his group [[Wings (band)|Wings]].<ref name="halstead 268">Halstead, p. 268.</ref><ref>Andersen, p. 99.</ref>

Jackson stayed at the home of McCartney and his wife [[Linda McCartney|Linda]] during the recording sessions, and became friends with both.<ref name="tara 333">Taraborrelli, p. 333.</ref> While at the dining table one evening, Paul McCartney brought out a booklet that displayed all the songs to which he owned the publishing rights. "This is the way to make big money", the musician informed Jackson. "Every time someone records one of these songs, I get paid. Every time someone plays these songs on the radio, or in live performances, I get paid." McCartney's words influenced Jackson's later purchase of [[Sony/ATV Music Publishing|ATV Music Publishing]] in 1985.<ref name="tara 333" />

{{listen|pos=left|filename=Paul McCartney and Michael Jackson Say Say Say.ogg|title="Say Say Say"|description="Say Say Say" is a pop song played in the key of B{{music|flat}} minor. The track has been cited as a "pleading kind of love song".|format=Ogg}}

McCartney played several instruments on "Say Say Say", including percussion, synthesiser, guitar, and bass guitar. The [[harmonica]] was played by Chris Smith and the [[rhythm guitar]] was played by David Williams. The song was engineered by former Beatles sound engineer, [[Geoff Emerick]].<ref>Linear notes of ''Pipes of Peace'' by Paul McCartney.</ref> The production of "Say Say Say" was completed in February 1983, after it had been refined and [[overdubbing|overdubbed]] at [[Cherokee Studios]] in California.

[[George Martin]], who had worked with The Beatles, produced the song. He said of his experience with Jackson, "He actually does radiate an aura when he comes into the studio, there's no question about it. He's not a musician in the sense that Paul is&nbsp;... but he does know what he wants in music and he has very firm ideas."<ref name="coleman 129" /><ref name="halstead 268" />

Jackson also spoke of the experience in his autobiography, ''[[Moonwalk (book)|Moonwalk]]''. The younger singer revealed that the collaboration boosted his confidence, as [[Quincy Jones]]—producer of ''Thriller''—was not present to correct his mistakes. Jackson added that he and McCartney worked as equals, stating, "Paul never had to carry me in that studio."<ref>Jackson, p. 188.</ref>

According to Musicnotes.com by [[Alfred Music Publishing]], "Say Say Say" was performed in [[time signature|common time]], with a dance beat of 116&nbsp;[[tempo|beats per minute]].<ref name="sheet">{{cite news |title=Say Say Say&nbsp;– Paul McCartney Digital Sheet Music (Digital Download)|work=MusicNotes.com|publisher=[[Alfred Publishing|Alfred Publishing Co. Inc]]}}</ref> It is in the key of [[B-flat minor|B{{music|flat}} minor]] and sung in a vocal range from F<sub>4</sub> to B{{music|flat}}<sub>5</sub>.<ref name="sheet" /> The lyrics to "Say Say Say" reflect an attempt to "win back" a girl's affection; ''[[Deseret News]]'' considered the song to be a "pleading kind of love song".<ref name="sheet" /><ref name="McCartney, Jackson together again" />

==Release and reception==
Following the release of ''Thriller'' and most of its singles, "Say Say Say" was released on 3 October 1983 by [[Parlophone Records]] in the UK and [[Columbia Records]] in the US.<ref>Grant, p. 68.</ref><ref>Harry, p. 171.</ref><ref>Hill, p. 381.</ref> It remained atop [[Billboard Hot 100|''Billboard''{{'}}s Hot 100]] for six weeks and became Jackson's seventh top ten hit of 1983, breaking a record that until then was held jointly by The Beatles and [[Elvis Presley]].<ref name="everett 282">Everett, p. 282</ref><ref name="Campbell 68">Campbell, p. 68</ref><ref name="lewis 6">Lewis, p. 6</ref> Also in the US, "Say Say Say" reached number two on the [[R&B chart]] (behind "[[Time Will Reveal (song)|Time Will Reveal]]" by [[DeBarge]]) and number three on the [[Hot Adult Contemporary Tracks]] chart.<ref name="George 39">George, p. 39.</ref>  Although the song had peaked at number ten in the UK, it began to fall steadily; McCartney subsequently held an early weekday live television interview, where he discussed the song's music video. This, along with screenings of the video on ''[[Top of the Pops]]'' (which normally played only singles that were rising in the charts), ''[[The Tube (TV series)|The Tube]]'' and [[Noel Edmonds]]' ''[[The Late, Late Breakfast Show]]'', helped propel the song to number two on the [[UK Singles Chart]].<ref name="Barrow 92">Barrow, p. 92.</ref><ref>Heryanto, p. 92.</ref> "Say Say Say" reached number one in Norway and Sweden, and the single also reached the top ten in Austria, Australia, New Zealand, the Netherlands, and Switzerland.<ref name="Barrow 92" /><ref name="Austria" /> With wholesale shipments of at least one million units, the single was later [[music recording sales certification|certified]] platinum by the [[Recording Industry Association of America]].<ref>{{cite web|url=http://www.riaa.com/goldandplatinumdata.php?table=SEARCH|title=RIAA database|publisher=[[Recording Industry Association of America]]|accessdate=30 April 2009}} (To search the database for "Say Say Say", type "Paul McCartney" or "Michael Jackson" into the Artist field and "Say Say Say" into the Title field.)</ref>

"Say Say Say" received mixed reviews from [[music critic]]s. The lyrics were named the worst of 1983 by ''[[The Buffalo News]]''<nowiki>'s</nowiki> Anthony {{Not a typo|Violanti}},<ref>Violanti, Anthony (18 August 1996). "Schlock: An Unusually Confused and Nasal Dylan". ''[[The Buffalo News]]''. Retrieved on 19 March 2009.</ref> while the ''[[Lexington Herald-Leader]]'' stated in a review of ''Pipes of Peace'' that, aside from "Say Say Say" and "The Man", "McCartney waste[d] the rest of the album on bathos and whimsy".<ref>"Paul McCartney's New Album Is Just 'Embarrassing Fluff'". ''[[Lexington Herald-Leader]]''. (15 January 1983). Retrieved on 19 March 2009.</ref> The ''[[Los Angeles Times]]''{{'}} Paul Grein also reviewed the McCartney album and opined that the singer had redeemed himself with the success of the "spunky" song "but plunged back into wimpdom with '[[No More Lonely Nights]]'".<ref>Grein, Paul (3 January 1988). "Hits That Hurt In Some Cases, That Top 10 Smash Can Smash an Artist's Image". ''[[Los Angeles Times]]''. Retrieved on 19 March 2009.</ref> Journalist Whitney Pastorek compared the song to McCartney's 1982 duet with [[Stevie Wonder]], "[[Ebony and Ivory]]". She asserted that "Say Say Say" was a better song, and had a better "though slightly more nonsensical" music video, adding that the song had no "heavy-handed social content".<ref>{{cite web|first=Whitney|last=Pastorek|url=http://www.ew.com/ew/article/0,,20037860_2,00.html|title=This Week in '82|work=Entertainment Weekly|date=3 May 2007|accessdate=19 March 2009}}</ref> [[Penn State]]'s ''[[The Daily Collegian (Penn State)|The Daily Collegian]]'' described the track as a good song, despite its [[ad nauseam]] broadcasts.<ref>{{cite web|first=Ron|last=Yeany|url=http://digitalnewspapers.libraries.psu.edu/Default/Skins/BasicArch/Client.asp?Skin=BasicArch&&AppName=2&enter=true&BaseHref=DCG/1983/11/09&EntityId=Ar01400|title=McCartney and Simon|work=[[The Daily Collegian (Penn State)|The Daily Collegian]]|date=22 November 1990|accessdate=2 March 2009}}</ref>

''Deseret News'' stated that the "pleading love song" had a "masterful, catchy hook".<ref name="McCartney, Jackson together again">{{cite web |url=https://news.google.com/newspapers?id=DwcPAAAAIBAJ&sjid=AIMDAAAAIBAJ&pg=7210,1553388&dq=say-say-say+jackson|title=McCartney, Jackson together again|work=[[Deseret News]]|date=18 November 1993|accessdate=7 March 2009}}</ref> In a ''[[Rolling Stone]]'' review, the track was described as an "amiable though vapid dance groove". The reviewer, Parke Puterbaugh, added that it was an "instantly hit-bound froth-funk that tends, after all, toward banality".<ref>{{cite web|first=Parke|last=Puterbaugh|url=http://www.rollingstone.com/reviews/album/212833/review/5945684?utm_source=Rhapsody&utm_medium=CDreview|title=Pipes of Peace review|work=Rolling Stone|date=19 January 1984|accessdate=7 March 2009}}</ref> Music critic [[Nelson George]] stated that "Say Say Say" would not have "deserved the airplay it received without McCartney and Jackson".<ref>{{cite journal|url=https://books.google.com/?id=ZSQEAAAAMBAJ&pg=PA50&dq=%22say+say+say%22+jackson&cd=21#v=onepage&q=%22say%20say%20say%22&f=false|title=Black <nowiki>'</nowiki>84|last=George|first=Nelson|date=22 December 1984|work=[[Billboard (magazine)|Billboard]]|volume= 96| issue = 51|issn=0006-2510|accessdate=11 February 2010}}</ref> [[Salon.com]] later described the song as a "sappy duet". The online magazine concluded that McCartney had become a "wimpy old fart".<ref>{{cite web|first=Gilbert|last=Garcia|url=http://dir.salon.com/story/ent/music/feature/2003/01/27/paul_yoko/index1.html|title=The ballad of Paul and Yoko|publisher=[[Salon.com]]|date=27 January 2003 |accessdate=7 March 2009}}</ref> [[Billboard (magazine)|''Billboard'']] listed "Say Say Say" as Michael Jackson's all-time biggest [[Billboard Hot 100|Hot 100]] single.<ref>{{cite web|title='Say Say Say' ranks as Michael Jackson's biggest Billboard hit|url=http://www.billboard.com/articles/news/957728/say-say-say-ranks-as-michael-jacksons-biggest-billboard-hit|accessdate=12 April 2016}}</ref> In a 2007 article, a writer for the magazine ''[[Vibe (magazine)|Vibe]]'' listed "Say Say Say" as the 22nd greatest duet of all time. The writer commented that the song was "a true falsetto fantasy" and that it was "still thrilling to hear the sweet-voiced duo trade harmonies on the chorus".<ref>{{cite news|url=https://books.google.com/?id=rSYEAAAAMBAJ&pg=PA88&dq=%22say+say+say%22+jackson&cd=25#v=onepage&q=%22say%20say%20say%22%20jackson&f=false|title=The 50 greatest duets of all time|last=Caramanica|first=Jon|date=February 2007|work=[[Vibe (magazine)|Vibe]]|accessdate=11 February 2010}}</ref> In 2005, Dutch musicians [[Hi Tack]] sampled "Say Say Say" on their debut single, "[[Say Say Say (Waiting 4 U)]]". The song featured Jackson's vocals from the original recording, plus McCartney's "Baby".<ref>{{cite journal|url=https://books.google.com/books?id=2BQEAAAAMBAJ&pg=PA15&dq=%22say+say+say%22+%22waiting+4+u%22&cd=1#v=onepage&q=%22say%20say%20say%22%20%22waiting%204%20u%22&f=false|title=Upfront|date=7 January 2006|work=Billboard|volume= 118| issue = 1|issn=0006-2510|accessdate=24 February 2010|author1=Nielsen Business Media|first1=Inc}}</ref>

=== 2015 version ===
On October 6, 2015, McCartney released a new version of the song in which the vocal roles of him and Jackson are reversed. It was remixed by Steve Orchard and [[Spike Stent|Mark "Spike" Stent]]. On the new version, which is over three minutes longer than the original, the opening of the first is sung by Jackson instead of McCartney. Orchard said of the remix: "Paul remembered that there were two unused lead vocal performances by Michael and himself. We rearranged the vocal sequence and inverted the original performance so that Michael opened the first verse instead of Paul, to give the song a different take to the original version."<ref>https://www.yahoo.com/music/sir-paul-mccartney-switches-vocals-1272344086667318.html</ref> The song appears on the 2015 re-issue of ''[[Pipes of Peace]]''.<ref>http://www.usatoday.com/story/life/music/2015/10/08/song-week-paul-mccartney-and-michael-jackson-together-again/73581018/</ref> A radio edit of the 2015 remix was released for streaming on October 30, 2015,<ref name="play.spotify.com">https://play.spotify.com/album/2VANZtyaZ8Y5B2Z33xRXBZ?play=true&utm_source=open.spotify.com&utm_medium=open</ref> while an instrumental version of it is available for download at paulmccartney.com.<ref name="paulmccartney.com">http://www.paulmccartney.com/downloads</ref>

== Personnel ==
*[[Paul McCartney]]: [[guitar]]s, [[Keyboard instrument|keyboards]], [[Singing|vocals]]
*[[Michael Jackson]]: vocals
*Chris Hammer Smith: [[harmonica]]
*David Williams: guitar
*Nathan Watts: bass
*Bill Wolfer: keyboards
*Linda McCartney, Eric Stewart: backing vocals
*Ricky Lawson: drums
*Jerry Hey, Ernie Watts, Gary E. Grant, Gary Herbig: horns

==Music video==

===Production, plot, and reception===
The music video (or "short film") for "Say Say Say" was directed by [[Bob Giraldi]], who had previously directed Michael Jackson's music video for "[[Beat It]]". Cameo appearances in the video are made by McCartney's then wife [[Linda McCartney|Linda]], as well as Jackson's older sister [[La Toya Jackson|La Toya]].<ref>Grant, p. 270.</ref><ref>{{cite web|url=http://www.mtv.com/news/articles/1425543/19980420/beatles.jhtml|title=Linda McCartney Dies Of Cancer|publisher=MTV|date=20 April 1998|accessdate=8 March 2009}}</ref> According to LaToya Jackson, during filming of the video, the McCartneys were staying at a piece of property called Sycamore Ranch.  Michael visited them and expressed interest in someday buying the property.  In 1988, he would, renaming it Neverland Ranch.<ref name="LaToya">{{cite book|author1=La Toya Jackson|author2=Jeffré Phillips|title=Starting Over|url=https://books.google.com/books?id=SeQfvRHvZfwC&pg=PA69|accessdate=12 May 2016|date=29 May 2012|publisher=Simon and Schuster|isbn=978-1-4516-2059-7|page=69}}</ref> To accommodate Jackson's busy schedule the video was filmed at [[Los Alamos, California|Los Alamos]] near [[Santa Barbara, California]]. McCartney flew in specially for filming.<ref>{{cite web|url=http://pqasb.pqarchiver.com/latimes/access/677046232.html?dids=677046232:677046232&FMT=ABS&FMTS=ABS:AI&type=historic&date=Mar+01%2C+1984&author=&pub=Los+Angeles+Times&desc=You&pqatl=google|title=You|last=Reba|first=Bonnie Churchill|date=1 March 1984|work=Los Angeles Times|format=Payment required to access full article.|accessdate=19 March 2010}}</ref><ref name="Campbell 69">Campbell, p. 69.</ref> The video cost $500,000 to produce.<ref name="Barrow 92" />

The British television premiere of the video was extremely controversial. The video had not been ready when the track debuted in the UK singles chart. By the time it was finalised, the track had dropped in the chart. McCartney flew to London to premiere the video on the [[BBC1]] TV show ''[[Top Of The Pops]]'', but the show had a strict policy that no single that had dropped in position could feature on the programme and refused to show it. A furious argument ensued, with claims McCartney threatened to withdraw all his music from the corporation. As a compromise, the BBC offered to air the video two days later on ''[[The Late, Late Breakfast Show]]'' hosted by [[Noel Edmonds]], a variety show that featured live music performances, but rarely aired videos. They agreed to show it on the programme only if McCartney appeared live and gave an interview. He reluctantly agreed and appeared with his wife [[Linda McCartney|Linda]] live on the show on Saturday, 29 October 1983; his first live UK TV appearance since 1973. The interview was stilted and the McCartneys made little or no effort to answer any of Edmonds' questions. After some reportedly hostile negotiations, the programme's entire show was built around the 'medicine men' theme of the video and the guest who had been booked to appear that week [[Olivia Newton John]] had to agree to appear to promote the video in a skit, reportedly against her will and she expressed anger at having her 'starring' role in the show downgraded into a lesser guest spot to make way for the video and McCartney.<ref>Bowen, Mark. 2009. 'McCartney Solo: See You Next Time'. ISBN 9781409298793. Page 119</ref><ref>Badman, Keith. 'The Beatles Diary Volume 2: After The Break-Up 1970-2001'. Omnibus Press 2009. ISBN 9780857120014</ref><ref>Wincentsen, Edward. 'The Olivia Newton John Companion'. Wynn Publishing 2002. ISBN 978-0971059108</ref> The airing of the video on this highly rated show was successful as the track climbed back up the chart the following week and aired on ''Top Of The Pops'' on Thursday, 4 November 1983.<ref>http://www.chartstats.com</ref>

In the short film, the duo play "Mac and Jack", a pair of conmen who sell a "miracle potion". The salesman (McCartney) offers Jackson the potion, and claims that it is "guaranteed to give you the strength of a raging bull". Jackson drinks the potion and challenges a large man to arm-wrestle. Unbeknownst to a watching crowd, the man—along with Linda—is also in on the scam. After Jackson wins the rigged contest, the crowd of people surge forward and buy the potion. Mac and Jack then donate all of the money earned from the scam to an orphanage.<ref name="Campbell 69" /> After this scene, McCartney and Jackson star as [[vaudeville]] performers who sing and dance at a bar.<ref name="Curtis 323">Curtis, p. 323.</ref> On stage, the duo appear in clown makeup at one point and quickly go through a number of costume changes.<ref name="Lhamon 219">Lhamon, p. 219.</ref> Jackson flirts with a young woman portrayed by his real-life sister La Toya.<ref>{{cite web|first=Davina|last=Morris|url=http://www.voice-online.co.uk/content.php?show=14158|title=Happy birthday MJ|work=[[The Voice (newspaper)|The Voice]]|date=24 August 2008|accessdate=8 March 2009}}</ref> When law-enforcement officers appear at the back of the venue, Mac quickly starts a small fire onstage and Linda hollers "FIRE!", emptying the venue and allowing the group to escape via backstage (yet somehow finding time to change into tuxedoes first). The video ends with Paul, Linda, and Michael as they drive off into the sunset. La Toya, who was handed a bunch of flowers by McCartney, is left at the roadside.<ref name="Curtis 323" />

The video also features appearances by director Giraldi as a [[Glossary of cue sports terms#sharks|pool shark]] who is conned by McCartney, and [[Art Carney]] as an audience member for the vaudeville show.<ref>{{cite AV media
 | people = Paul McCartney
 | title = [[The McCartney Years]]
 | medium = DVD
 | publisher = [[Rhino Entertainment]]
 | date = 2007 }}</ref>

Giraldi said of Jackson and McCartney, "Michael didn't outdance Paul, and Paul didn't outsing Michael". He added that production of the video was hard work because "the egos could fill a room".<ref>{{cite web|first=Guy|last=Garcia|url=http://www.time.com/time/magazine/article/0,9171,952263,00.html|title=People: Nov. 14, 1983|work=[[Time (magazine)|Time]]|date=18 November 1983|accessdate=8 March 2009}}</ref> The video introduced both dialogue and storyline, an element extended upon in ''[[Michael Jackson's Thriller (music video)|Michael Jackson's Thriller]]''.<ref>{{cite web|first=Ron|last=Sklar|url=http://digitalnewspapers.libraries.psu.edu/Default/Skins/BasicArch/Client.asp?Skin=BasicArch&&AppName=2&enter=true&BaseHref=DCG/1983/12/07&EntityId=Ar02201|title=Thriller video|work=The Daily Collegian|date=23 November 1990|accessdate=8 March 2009}}</ref> In a 1984 study of music videos conducted by the National Coalition on Television Violence, [[the Jackson 5|the Jacksons]] were rated "very violent," citing Michael Jackson's "Billie Jean," "Thriller", and "Say Say Say" as well as Jermaine Jackson's "Dynamite" and the Jacksons' "Torture."<ref>Bishop, Pete. "[http://articles.chicagotribune.com/1985-01-25/entertainment/8501050715_1_music-video-television-violence-atlanta-superstation Group on TV Violence Looks at Music Videos and Is Not Amused]" ''Chicago Tribune'', 25 January 1985.</ref> In a list compiled by ''Billboard'' at the end of 1984, the music video was named the fourth best of the year, and the rest of the top four were also short films by Jackson.<ref>Campbell, p. 105.</ref>

''[[The Manchester Evening News]]'' later described the "Say Say Say" video as an "anarchic caper" that "plays out like an [[Emir Kusturica]] feature".<ref>{{cite web|first=Stephen|last=Gilliver|url=http://www.manchestereveningnews.co.uk/entertainment/music/s/1025047_dvd_review_paul_mccartney__the_mccartney_years_warner_|title=DVD review: Paul McCartney&nbsp;– The McCartney Years (Warner)|work=[[The Manchester Evening News]]|date=20 November 2007|accessdate=8 March 2009}}</ref> [[PopMatters]] stated that the music videos of "Say Say Say" and "[[Goodnight Tonight]]" turned "a pair of otherwise forgettable songs into something worth watching".<ref>{{cite web|first=Terry|last=Lawson|url=http://www.popmatters.com/pm/article/old-rockers-go-on-a-dvd-roll|title=Old rockers go on a DVD roll|publisher=[[PopMatters]]|date=20 November 2007|accessdate=8 March 2009}}</ref> Steven Greenlee of ''[[The Boston Globe]]'' reflected that the video was both "horrifying and compelling", and stated the ridiculousness of a potion which could aid Jackson in beating somebody at arm wrestling. He added, "It's even harder to believe that the two of them didn't get the pulp beaten out of them in that bar for dressing like a pair of [[Chess King]] employees".<ref>{{cite web|first=Steven|last=Greenlee|url=http://www.boston.com/ae/music/gallery/2008mtv_music_videos?pg=6|title=Back when MTV had videos|work=The Boston Globe|accessdate=16 March 2009}}</ref> The "Say Say Say" video was later included on the DVDs ''[[The McCartney Years]]'' and ''[[Michael Jackson's Vision]]''.<ref>Paul McCartney, ''The McCartney Years'', DVD.</ref>

===Themes===
Two authors later reviewed the short film and documented two central themes. The first is a "Child/Man" theme; the role of both a boy and an adult, which writer James M. Curtis states Jackson plays throughout the music video for "Say Say Say".<ref name="Curtis 323" /> Curtis writes that the bathroom scene involving the shaving foam is reminiscent of boys copying their fathers. He adds that the scene marks "the distinction between Michael's roles as a Child and as a Man". The writer also highlights the part where the singer supposedly becomes strengthened with a miracle potion, a further play on the "Child/Man" theme.<ref name="Curtis 323" /> Furthermore, Curtis observes that Paul and Linda McCartney seem to act as if they are Jackson's parents in the short film.<ref name="Curtis 323" /> The author also notes that in a scene where Jackson is handed a bouquet of flowers from a girl, it is a reversal of one from ''[[City Lights]]'', a 1931 film starring [[Charlie Chaplin]], whom the singer greatly adored.<ref name="Curtis 323" />

The second of the two main themes in the music video is of [[African American history]] and [[African American culture|culture]], as some of the vaudeville scenes in the short film acknowledge [[minstrel shows]] and [[blackface]].<ref name="Lhamon 219" /> Author W. T. Lhamon writes that the video is set in the Californian Depression, and that McCartney and Jackson "convey a compactly corrupt history of blackface" as they con their way to riches with the Mac and Jack show.<ref name="Lhamon 219" /> Lhamon was critical of the pair and of the video because he felt that the African American theme had not been made explicitly known. The author expressed his view that aspects of the short film were historically out-of-synch with interracial relations.<ref name="Lhamon 219" /> He stated, "Nearly everything in the video is backward. Mack's white hand continually helping black Jack on board, for instance, reverses the general process I have shown of blacks providing whites with their sustaining gestures."<ref name="Lhamon 219" /> Lhamon added, "In a just world, Jackson should be pulling McCartney onto the wagon, not the other way around."<ref name="Lhamon 219" />

==Track listing ==
*;UK 7" single
#"Say Say Say" (with Michael Jackson) – 3:55
#"Ode to a Koala Bear" – 3:45

*;US 12" single
#"Say Say Say" (special version) (with Michael Jackson) – 5:40
#"Say Say Say (Instrumental)" (with Michael Jackson) – 7:00
#"Ode to a Koala Bear" – 3:45

*;Digital streaming <ref name="play.spotify.com"/>
#"Say Say Say" (radio edit / 2015 remix) (with Michael Jackson) – 3:41

*;Digital download - via paulmccartney.com <ref name="paulmccartney.com"/>
#"Say Say Say" (radio edit instrumental / 2015 remix) (with Michael Jackson) - 3:41

==Chart performance==
{{col-begin}}
{{col-2}}
{| class="wikitable sortable" style="text-align: center;"
! scope="col" | Chart
! col="col" | Peak <br /> position
|-
! scope="row" | [[Kent Music Report|Australian Singles Chart]]<ref name="Austria" />
| 4
|-
! scope="row" | [[Austrian Singles Chart]]<ref name="Austria">{{cite web|url=http://austriancharts.at/showitem.asp?interpret=Paul+McCartney+%26+Michael+Jackson&titel=Say+Say+Say&cat=s|title=Austrian Singles Chart Archives|accessdate=3 March 2009|work=austriancharts.at |publisher=Hung Medien}}</ref>
| 10
|-
! scope="row" | Canadian [[RPM (magazine)|RPM Magazine]] Chart
| 1
|-
! scope="row" | [[Dutch Singles Chart]]<ref>{{cite web|url=http://www.dutchcharts.nl/showitem.asp?interpret=Paul+McCartney+%26+Michael+Jackson&titel=Say+Say+Say&cat=s|title=Dutch Charts| publisher = www.dutchcharts.nl |accessdate=27 April 2016}}</ref>
| 8
|-
! scope="row" | [[German Singles Chart]]<ref>{{cite web|url=https://www.offiziellecharts.de/titel-details-1007|title=Offizielle Deutsche Charts| publisher = www.offiziellecharts.de|accessdate=27 April 2016}}</ref>
| 12
|-
! scope="row" | [[Irish Singles Chart]]<ref>{{cite web|url=http://www.irishcharts.ie/search/placement?page=14|title=The Irish Charts|publisher = www.irishcharts.ie|accessdate=27 April 2016}}</ref>
| 3
|-
! scope="row" | [[New Zealand Singles Chart]]<ref>{{cite web|url=http://www.charts.org.nz/showitem.asp?interpret=Paul+McCartney+%26+Michael+Jackson&titel=Say+Say+Say&cat=s |title=New Zealand Charts|publisher = www.charts.org.nz|accessdate=27 April 2016}}</ref>
| 10
|-
! scope="row" | [[Norwegian Singles Chart]] <ref>{{cite web|url=http://www.norwegiancharts.com/showitem.asp?interpret=Paul+McCartney+%26+Michael+Jackson&titel=Say+Say+Say&cat=s |title=VG Lista|publisher = www.norwegiancharts.com |accessdate=27 April 2016}}</ref>
| 1
|-
! scope="row" | [[Swedish Singles Chart]]  <ref>{{cite web|url=http://www.swedishcharts.com/showitem.asp?interpret=Paul+McCartney+%26+Michael+Jackson&titel=Say+Say+Say&cat=s |title=Swedish Charts |publisher = www.swedishcharts.com |accessdate=27 April 2016}}</ref>
| 1
|-
! scope="row" | [[Swiss Singles Chart]] <ref>{{cite web|url=http://www.hitparade.ch/song/Paul-McCartney-&-Michael-Jackson/Say-Say-Say-1007 |title=Swiss Charts |publisher = www.hitparade.ch |accessdate=27 April 2016}}</ref>
| 2
|-
! scope="row" | [[UK Singles Chart]]<ref name="George 39" />
| 2
|-
! scope="row" | US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref name="George 39" />
| 1
|-
! scope="row" | US [[R&B singles chart|R&B Singles Chart]]<ref name="George 39" />
| 2
|}

===Year-end charts===
{| class="wikitable" style="text-align: left;"
! scope="col" | Chart (1984)
! col="col" | Position
|-
! scope="row" | US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref>http://www.bobborst.com/popculture/top-100-songs-of-the-year/?year=1984</ref>
| <center>3</center>
|}

===End-of-decade charts===
{| class="wikitable sortable"
|-
!End of decade (1980–89)
!Position
|-
| U.S. ''Billboard'' Hot 100
| style="text-align:center;"|9
|}

=== All-time charts ===
{|class="wikitable plainrowheaders"
!Chart
!Position
|-
!scope="row"|US [[Billboard Hot 100|''Billboard'' Hot 100]]<ref name= Billboard/>
|align="center"|41
|}
{{col-end}}

==Certifications==
{{Certification Table Top}}
{{Certification Table Entry|type=single|autocat=yes|relyear=1983|title=Say Say Say|artist=Michael Jackson|region=Australia|award=Gold|certyear=1994|certref=<ref name="ARIA">Michael Jacksons Charts & Sales history at UKMIX. You can find this information at [ukmix.org/forums/viewtopic.php?f=24&t=63110&hilit=Jackson]</ref>|salesamount=67,000|salesref=<ref name="ARIA"/>}}
{{Certification Table Entry|type=single|autocat=yes|relyear=1983|title=Say, Say, Say|artist=Paul Mccartney|region=Canada|award=Platinum|certyear=1984|salesamount=100,000}}
{{Certification Table Entry|type=single|autocat=yes|relyear=1983|title=Say Say Say|artist=Paul McCARTNEY & Michael JACKSON|region=France|award=Gold|certyear=1983|certref=<ref name="SNEP">{{cite web|url=http://infodisc.fr/Single_Certif.php|title=Les Certifications (Albums) du SNEP (Bilan par Artiste) > McCARTNEY Paul & JACKSON Michael > "Ok|language=French|publisher=InfoDisc.fr|accessdate=2015-07-22}}</ref>|salesamount=825,000|salesref=<ref name="SNEP"/>}}
{{Certification Table Entry|type=single|autocat=yes|relyear=1983|title=Say, Say, Say|artist=PAUL McCARTNEY & MICHAEL JACKSON|region=United Kingdom|award=Silver|certyear=1983}}
{{Certification Table Entry|type=single|autocat=yes|relyear=1983|title=Say Say Say|artist=MC CARTNEY, PAUL AND MICHAEL JACKSON|region=United States|award=Platinum|certyear=1992}}
{{Certification Table Bottom|nounspecified=yes}}

==Cover versions==
* Chinese singer-songwriter [[Cui Jian]] recorded a Chinese version of the song called "誰、誰、誰" ("Who, Who, Who"/shéi, shéi, shéi) on his 1985 album "'85回顧" ("'85 Review"/huígù).

==See also==
{{portal|The Beatles|Michael Jackson}}
*[[List of Billboard Hot 100 number-one singles of 1983|List of ''Billboard'' Hot 100 number-one singles of 1983]]
*[[List of Billboard Hot 100 number-one singles of 1984|List of ''Billboard'' Hot 100 number-one singles of 1984]]

==References==
{{reflist|colwidth=30em}}

'''Bibliography'''
{{refbegin}}
* {{cite book|last=Andersen|first=Christopher P.|title=Michael Jackson: unauthorized|publisher=Simon & Schuster|year=1994|isbn=0-671-89239-8}}
* {{cite book|last=Barrow|first=Tony|title=Inside the Music Business|year=1994|publisher=Routledge|isbn=0-415-13660-1}}
* {{cite book|last=Campbell|first=Lisa|title=Michael Jackson: The King of Pop|publisher=Branden|year=1993|isbn=0-8283-1957-X}}
* {{cite book|last=Coleman|first=Ray|authorlink=Ray Coleman|title=McCartney: Yesterday and Today|publisher=Dove Books|year=1996|isbn=0-7871-1038-8}}
* {{cite book|last=Curtis|first=James M.|title=Rock Eras|year=1987|publisher=Popular Press|isbn=0-87972-369-6}}
* {{cite book|last=Everett|first=Walter|title=The Beatles as musicians: Revolver through the Anthology|publisher=Oxford University Press|year=1999|isbn=0-19-512941-5}}
*[[Nelson George|George, Nelson]] (2004). ''[[Michael Jackson: The Ultimate Collection]]'' booklet. [[Sony BMG]].
* {{cite book|last=Grant|first=Adrian|title=Michael Jackson: The Visual Documentary|publisher=Omnibus Press|year=2009|isbn=978-1-84938-261-8}}
* {{cite book|last=Halstead|first=Craig|title=Michael Jackson: For the Record|year=2007|publisher=Authors OnLine|isbn=978-0-7552-0267-6}}
* {{cite book|last=Harry|first=Bill|year=2002|title=The Paul McCartney Encyclopedia|publisher=Virgin|isbn=0-7535-0716-1}}
* {{cite book|last=Hill|first=Tim|title=The Beatles: unseen archives|publisher=Parragon|year=2000|isbn=0-7525-4080-7}}
* {{cite book|last=Jackson|first=Michael|title=Moonwalk|publisher=Random House Inc|year=2009|isbn=0-307-71698-8}}
* {{cite book|last=Jones|first=Jel|title=Michael Jackson, the King of Pop: The Big Picture: the Music! the Man! the Legend! the Interviews!|year=2005|publisher=Amber Books Publishing|isbn=0-9749779-0-X}}
* {{cite book|last=Lhamon|first=W. H.|title=Raising Cain|year=1998|publisher=Harvard University Press|isbn=0-674-74711-9}}
* {{cite book|last=Romanowski|first=Patricia|title=The New Rolling Stone encyclopedia of rock & roll|publisher=Fireside|year=1995|isbn=0-684-81044-1}}
* {{cite book|last=Taraborrelli|first=J. Randy|authorlink=J. Randy Taraborrelli|title=The Magic and the Madness|year=2004|publisher=Headline|isbn=0-330-42005-4}}
{{refend}}

==External links==
*[http://www.paulmccartney.com/ Paul McCartney's official website]
*[http://www.michaeljackson.com/ Michael Jackson's official website]* {{YouTube|aLEhh_XpJ-0|"Say Say Say by Paul McCartney and Michael Jackson" music video}}

{{Paul McCartney}}
{{Michael Jackson singles}}

{{Featured article}}

[[Category:1983 singles]]
[[Category:1983 songs]]
[[Category:Vocal duets]]
[[Category:Michael Jackson songs]]
[[Category:Paul McCartney songs]]
[[Category:Billboard Hot 100 number-one singles]]
[[Category:European Hot 100 Singles number-one singles]]
[[Category:RPM Top Singles number-one singles]]
[[Category:Number-one singles in Norway]]
[[Category:Number-one singles in Italy]]
[[Category:Number-one singles in Sweden]]
[[Category:Parlophone singles]]
[[Category:Song recordings produced by George Martin]]
[[Category:Songs written by Michael Jackson]]
[[Category:Songs written by Paul McCartney]]
[[Category:Music videos directed by Bob Giraldi]]
[[Category:Music published by MPL Music Publishing]]
[[Category:Columbia Records singles]]