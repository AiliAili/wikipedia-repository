{{BLP primary sources|date=February 2015}}
{{Infobox golfer
| name              = Chirapat Jao-Javanil
| image             =
| imagesize         = 
| caption           = 
| fullname          = Chirapat Jao-Javanil
| nickname          = Ja
| birth_date        = {{Birth date and age|1993|2|14|df=y}}
| birth_place       = [[Bangkok, Thailand]]
| death_date        = <!-- {{Death date and age|YYYY|MM|DD|1993|2|14|df=y}} -->
| death_place       = 
| height            = {{height|ft=5|in=4}}
| weight            = 
| nationality       = {{THA}}
| residence         = Cha am, Thailand
| spouse            = Nat Bhanthumachinda
| partner           = 
| children          = 
| college           = [[University of Oklahoma]]
| yearpro           = 2014
| retired           = <!-- Year retired -->
| tour              = [[Symetra Tour]] (joined [[2015 Symetra Tour|2015]])
| prowins           = 4
| lpgawins          = 
| letwins           = 
| klpgawins         = <!-- LPGA of Thailand Tour wins -->
| lagtwins          = <!-- Number of Ladies Asian Golf Tour wins -->
| alpgwins          = <!-- Number of ALPG Tour wins -->
| futwins           = <!-- Number of Futures Tour wins -->
| otherwins         = 
| majorwins         =
| nabisco           = 
| lpga              = 
| wusopen           = 
| wbritopen         = 
| evian             = 
| wghofid           = <!-- World Golf Hall of Fame member ID -->
| wghofyear         = <!-- World Golf Hall of Fame year inducted -->
| award1            = 
| year1             = 
| award2            = 
| year2             = 
| awardssection     = <!-- location of awards page or section -->
}}
'''Chirapat "Ja" Jao-Javanil''' (born 14 February 1993) is a [[Thai people|Thai]] professional golfer who plays on the [[Symetra Tour]], Thailand LPGA Tour, Cactus Tour, and [[LPGA Tour]]. On 25 May 2012, she became the first women's golfer in [[University of Oklahoma]] history to win the [[NCAA Division I Women's Golf Championships|NCAA Golf Championship]] as an individual.<ref name=Sportsbio>{{cite web|url=http://www.soonersports.com/ViewArticle.dbml?DB_OEM_ID=31000&ATCLID=208409682|title=Chirapat Jao-Javanil Sooner Sports Bio|publisher=Sooner Sports Properties, LLC|accessdate=22 January 2015}}</ref><ref>{{cite web|url=http://www.golfdigest.com/blogs/the-loop/2012/05/alabama-women-captures-first-n.html|title=Alabama women captures first NCAA title|publisher=Golf Digest|accessdate=22 January 2015}}</ref> She turned professional in May 2014 at age 21.

==Family and education==
Chirapat is the only daughter of Chiravid and Chanasthorn Jao-Javanil.<ref name=Sportsbio/> She graduated high school from Somtawin Witead Suksa Huaymongkhon School.<ref name=Sportsbio/> In May 2014, she received a bachelor's degree in Psychology from the University of Oklahoma.

==Amateur career==
Chirapat became the first women's golfer in school history to win the NCAA title, shooting a 282 (−6) at the NCAA Championships.<ref name=Sportsbio/> She also became the first Oklahoma golfer to win three tournament titles in a single season (NCAA Championships, Central District Invitational, Golfweek Conference Challenge).<ref name=Sportsbio/> She has earned two first-team All-American honors, one honorable mention All-American honor, and four All-Big 12 honors. She ranks first in school history with 73.07 career scoring average (+1.42 versus par) and recorded four of the six best single-season averages in OU history.<ref name=Sportsbio/> She has won an OU-record six tournaments, including the 2012 NCAA individual title and the 2014 Big 12 Championship. She competed in all 44 team events during her career and finished in the top five in 32% of her career collegiate events (in top 10 in 50%).<ref name=Sportsbio/>

At the end of her junior season, she was ranked No. 6 nationally by Golfstat and No. 7 by Golfweek. She also won the Susie Maxwell Berning Classic by two strokes in October with school-record-tying −7 performance (209), and won the SunTrust Gator Invitational by five shots in March with school-record 206 (−4).<ref name=Sportsbio/> She finished in second place at Clover Cup in March (4-under 212) and the SMU/Dallas Athletic Club Invitational (1-over 217). Other notable finishes included a tie for third at the Florida State Match-Up Invitational (even-par 216), a tie for fifth at the season-opening Golfweek Conference Challenge (1-under 215), a tie for sixth out of 126 competitors at NCAA Central Regional in Norman with three-round 211 (−5), and a team leading tie for 17th place at NCAA Championships with four-round total of 293.<ref name=Sportsbio/>

Junior Accomplishments: She had the overall low gross of the TrueVisions International Junior Golf Championships in 2009 and 2010 in Thailand. She also won the team portion of the 2010 Enjoy Jakarta World Junior Golf Championship in Indonesia. In 2009, she finished seventh in the Callaway Junior World Golf Championship in 2009 and was named to the Senior National Team of Thailand. In 2008, she won the DRB-Hicom ASEAN Junior Golf Championship in Malaysia.<ref name=Sportsbio/>

===Amateur highlights===
*2008 DRB-Hicom ASEAN Junior Golf Champion
*2009 Member of the Senior National Team of Thailand
*2009 TrueVisions International Junior Golf Champion
*2010 TrueVisions International Junior Golf Champion
*2010 Enjoy Jakarta World Junior Golf Team Champion
*2012 TrueVisions Player of the Year
*2012 [[U.S. Women's Amateur Public Links]] Quarterfinals
*2012 [[U.S. Women's Amateur]] Round of 64
*2013 [[U.S. Women's Amateur Public Links]] Round of 16
*Two-time first-team Golfweek/NGCA All-American Honoree
*WGCA Honorable Mention All-American Honoree
*Four-time All-Big 12 Conference Honoree

===Professional highlights===
*2015 Final Stage LPGA Q School
*2015 Texas Women's Open Runner-up
*2016 Cactus Tour at Sundance Runner-up
*2016 LPGA Yokohama Classic [[Monday qualifier]]

==Professional career==

===2014===
Jao-Javanil announced that she had turned professional in May 2014. She signed a sponsorship deal with the Singha Corporation.

In only her second professional event, Chirapat claimed the 2014 Golf Capital of Tennessee Women's Open title following her exceptional performance at Stonehenge Golf Club.<ref name=Golfweek>{{cite web|url=http://golfweek.com/news/2014/aug/04/golf-capital-tenn-womens-open-chirapat-jao-javanil/|title=Jao-Javanil wins Golf Capital of Tenn. Women's Open|publisher=Golfweek|accessdate=22 January 2015}}</ref><ref name=TNgolf>{{cite web|url=http://www.tngolf.org/Jao-Javanil-Wins-2014-Golf-Capital-of-Tennessee-Women-s-Open-1240C3027.html?LayoutID=59|title=Jao-Javanil Wins 2014 Golf Capital of Tennessee Women's Open|publisher=Tennessee Golf Association|accessdate=22 January 2015}}</ref> Only her first time to play in the Tennessee Women's Open, Jao-Javanil navigated Stonehenge like a seasoned veteran, posting 73-70-71, overall 2-under-par 214—the only player to post a sub-par 54-hole total.<ref name=Golfweek/><ref name=TNgolf/>

Chirapat made it past first and second stage of LPGA Qualifying school. During the final stage of LPGA qualifying school, she did not earn her LPGA Tour card. With her finish, she earned a full exempt status of the Symetra Tour for 2015.

===2015===
In the summer of 2015, Jao-Javanil finished runner-up at the [[Texas Women's Open]]. She captured the low professional award with a two round score of 74-70=144, 2-over-par.<ref name="Northern Texas PGA">{{cite web |url=http://www.ntpga.com/Amateur-Maddie-McCrary-Captures.news?LayoutID=59 |title=Texas Women's Open at Rolling Hills |publisher=Northern Texas PGA |accessdate=8 July 2015}}</ref> Jao-Javanil sustained an injury during the middle of the year that resulted in some time off for rehab and recovery.

===2016===
In the beginning of 2016, Jao-Javanil finished tied for 15th at the first [[Thai LPGA]] event of the year.  After flying back to the US, she has competed in one Cactus Tour event and finished tied for 2nd with a three round score of 77-72-70, 3-over par. In early May, Jao-Javanil qualified for the LPGA Yokohama Classic in Prattville, Alabama with a score of 70 (−2).<ref name=Yokohama>{{cite web |url=http://www.kiaclassic.com/news/2016-yokohama-pre-tour-notes-tuesday?LayoutID=59 |title=Monday Qualifier Wraps Up On Tuesday |publisher=LPGA Tour |accessdate=8 May 2016}}</ref> This marks the LPGA event that she has played in for her career. Shortly after this event, on May 11, 2016, she took home the title at the Cactus Tour Las Colinas Event in Arizona. Her three-day total for the event was −17 under par (69-66-64) and the final round 64 marks her career low. On May 25, 2016, she won her second consecutive Cactus Tour event at Lookout Mountain Golf Club with a three-day total of -6 under par (72-68-70). Her most recent victory came at the Cactus Tour event at Walnut Creek CC where she shot 71-72-67 = 210 (-6 under par).

==College wins (6)==
{| class="wikitable" style="font-size:95%;"
!No.
!Date
!Tournament
!Winning score
!To par
!Margin of<br>victory
!Runner-up
|- style="background:#e5d1cb;"
|-
|align=center|1
|align=right|21 Sep 2011
|Golfweek Conference Challenge
|align=right|70-75-72=217
|align=center|+1
|3 strokes
|{{flagicon|USA}} Grace Na
|-
|align=center|2
|align=right|21 Feb 2012
|Central District Invitational
|align=right|68-70-74=212
|align=center|−4
|2 strokes
|{{flagicon|USA}} Tessa Teachman
|-
|align=center|3
|align=right|25 May 2012
|[[NCAA Division I Women's Golf Championships|NCAA Division I Championship]]
|align=right|69-73-70-70=282
|align=center|−6
|4 strokes
|{{flagicon|USA}} [[Brooke Pancake]]
|-
|align=center|4
|align=right|17 Oct 2012
|Susie Maxwell Berning Classic
|align=right|70-71-68=209
|align=center|−7
|2 strokes
|{{flagicon|USA}} Lindsey Weaver
|-
|align=center|5
|align=right|17 Mar 2013
|Suntrust Gator Invitational
|align=right|69-68-69=206
|align=center|−4
|5 strokes
|{{flagicon|USA}} Portland Rosen
|-
|align=center|6
|align=right|27 Apr 2014
|Big XII Championship
|align=right|69-72-68=209
|align=center|−7
|2 strokes
|{{flagicon|THA}} Yupaporn Kawinpakorn
|}

==Professional wins (4)==
{| class="wikitable" style="font-size:95%;"
!No.
!Date
!Tournament
!Winning score
!To par
!Margin of<br>victory
!Runner-up
!Winner's<br>share ([[United States dollar|US$]])
|-
|align=center|1
|align=right|2 Aug 2014
|Tennessee Women's Open
|align=right|73-70-71=214
|align=center|−2
|5 strokes
|{{flagicon|USA}} Ashli Bunch
|align=center| 5,000
|-
|align=center|2
|align=right|11 May 2016
|Cactus Tour Las Colinas 
|align=right|69-66-64=199
|align=center|−17
|6 strokes
|{{flagicon|USA}} Mikado Nagatori
|align=center| 3,000
|-
|align=center|3
|align=right|25 May 2016
|Cactus Tour Lookout Mountain 
|align=right|72-68-70=210
|align=center|−6
|1 stroke
|{{flagicon|USA}} Jennifer Hahn
|align=center| 2,800
|-
|align=center|4
|align=right|2 July 2016
|Cactus Tour Walnut Creek CC 
|align=right|71-72-67=210
|align=center|−6
|1 stroke
|{{flagicon|USA}} Jennifer Park
|align=center| 2,700
|}

==References==
{{reflist}}

{{DEFAULTSORT:Jao-Javanil, Chirapat}}
[[Category:Thai female golfers]]
[[Category:Sportspeople from Bangkok]]
[[Category:1993 births]]
[[Category:Living people]]