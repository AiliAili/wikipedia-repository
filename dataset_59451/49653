{{Other uses|First Love (disambiguation)}}
{{infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = First Love
| title_orig    = Первая любовь (Pervaya ljubov)
| translator    = 
| image         = 
| author        = [[Ivan Turgenev]]
| illustrator   = 
| cover_artist  = 
| country       = Russia
| language      = Russian
| series        = 
| genre         = 
| publisher     = 
| release_date  = March, 1860
| english_release_date =
| media_type    =
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
'''''First Love''''' ({{lang-ru|Первая любовь}}, ''Pervaya ljubov'') is a [[novella]] by [[Ivan Turgenev]], first published in 1860. It is one of his most popular pieces of short fiction. It tells the love story between a 21-year-old girl and a 16-year-old boy.

==Background to the work==
''First Love'' was published in March 1860 in the ''[[Biblioteka Dlya Chteniya|Reader's Library]]''. Like many of Turgenev's works, this one is highly autobiographical. Indeed, the author claimed it was the most autobiographical of all his works.<ref>Magarshack, David. 1954. ''Turgenev: a Life.'' London: Faber and Faber, 20.</ref> Here Turgenev is retelling an incident from his own life, his infatuation with a young neighbor in the country, Catherine Shakovskoy (the Zinaida of the novella), an infatuation that lasted until his discovery that Catherine was in fact his own father's mistress.

Critics were divided. Some criticized its light subject matter that did not touch upon any of the pressing social and political issues of the day. Others condemned the impropriety of that subject matter, namely a father and son in love with the same woman and a young woman who was the mistress of a married man. But it had its many admirers, including the French novelist [[Gustave Flaubert]], who gushed in a letter to Turgenev, "What an exciting girl that Zinochka [Zinaida] is!"<ref>Magarshack, 238.</ref> The Countess Lambert, a close acquaintance of Turgenev, told the author that the [[Alexander III of Russia|Russian emperor]] himself had read the novella to the empress and been delighted by it.<ref>Troyat, Henri. 1988. ''Turgenev'' (New York: Dutton), 70.</ref>

==Plot summary==
''First Love'' is an example of a [[frame story]]. The story starts with the protagonist, Vladimir Petrovich, at a party. The three guests, all men "not old but no longer young", are taking turns recounting the stories of their first loves. When Vladimir's turn comes to tell his story, he suggests he write down the story in a notebook because it is a rather long, unusual tale and he is not adept at extemporaneous narration. The other men agree and a few weeks later the [[story within a story|story within the story]] continues with Vladimir reading from his notebook as he recounts the memory of his first love.

Vladimir Petrovich, a sixteen-year-old, is staying in the country with his family and there meets his new neighbor, Zinaida Alexandrovna Zasyekina, a beautiful twenty-one-year-old woman who is staying with her mother, the Princess Zasyekina. This family, as with many of the Russian minor nobility with royal ties of that time, were only afforded a degree of respectability because of their titles; the Zasyekins, in the case of this story, are a very poor family. The young Vladimir falls in love with Zinaida, who has a set of several other (socially more eligible) suitors whom he joins in their difficult and often fruitless efforts for the young lady's favour. Zinaida, as is revealed throughout the story, is a thoroughly capricious and somewhat playful mistress to these rather love-struck suitors. She fails to reciprocate Vladimir's love, often misleading him, mocking his comparative youth in contrast to her early adulthood. But eventually the true object of her affections and a rather tragic conclusion to the story is revealed.

Vladimir discovers that the true object of Zinaida's affection is his own father, Pyotr Vasilyevich. In the tragic and devastatingly succinct closing two chapters, Vladimir secretly observes a final meeting between Pyotr and Zinaida at the window of her house in which his father strikes her arm with a riding crop. Zinaida kisses the welt on her arm and Pyotr bounds into the house. Eight months later, Vladimir's father receives a distressing letter from Moscow and tearfully begs his wife for a favor. Pyotr dies of a stroke several days later, after which his wife sends a considerable sum of money to Moscow. Three or four years later, Vladimir learns of Zinaida's marriage to a Monsieur Dolsky and subsequent death during childbirth.

==Central characters==

'''Vladimir Petrovich''' – The storyteller, at the time of narration a 16-year-old boy; the protagonist of the story.

'''Zinaida Alexandrovna Zasyekina''' – The object of Vladimir's affections. Capricious, mocking and difficult, she is inconsistent in her affections towards her suitors, of which Vladimir is the one to whom she shows (outwardly) the most affection. However, it is the affection of sister to brother rather than between lovers.

'''Pyotr Vasilyevich''' – Vladimir's father, a stoic symbol of 19th century masculinity; very 'British' in outlook and apparently unreceptive to emotion but the object of quiet admiration by the son

==English translations==
*Turgenev, Ivan. ''Turgenev's Novels'', v. 11 ("The Torrents of Spring." "First Love." "Mumu."). Trans. Constance Garnett. London: Heinemann, 1897. Out of print.
*Turgenev, Ivan. ''First Love''. Trans. Isaiah Berlin. London: Hamish Hamilton, 1950. Out of print. Now available in Penguin Classics, 1978. ISBN 0-14-044335-5.
:Penguin edition includes an introduction by V.S. Pritchett.
*Turgenev, Ivan. ''First Love and Other Stories'', Oxford World's Classics. Trans. Richard Freeborn. New York: Oxford University Press, 1999. ISBN 0-19-283689-7.
:The translation is based on the text from I.S. Turgenev, ''Polnoye sobraniye sochineniy i pisem''. Moskva-Leningrad, Vol. IX, 1965, pp. 7–76. This edition also contains ''The Diary of a Superfluous Man'', ''Mumu'', ''Asya'', ''King Lear of the Steppes'', and ''The Song of Triumpant Love''.
*Turgenev, Ivan. ''First Love.'' New York: Penguin Books, 2007.

==Film adaptation==

"Pervaya lyubov'" (Первая любовь) USSR, Mosfilm 1968, 76 min

A [[First Love (1970 film)|German film]] adapting the Turgenev's novella was released on 1970.

[[Anne Flournoy]]'s 1983 short ''Nadja Yet'' is an adaptation of "First Love." Zinaida Alexandrovna Zasyekina is played by  [[Jenny Wright]].<ref>[http://anneflournoy.com/#block-ebc78490796dfcabe36c ''Nadja Yet''] Anne Flournoy website</ref> Pyotr Vasilyevich is played by Stephen Payne. Vladimir Petrovich is played by a live-action housefly. ''Nadja Yet'' can be seen on [[YouTube]].<ref>{{YouTube|e5JzVrhjuuM|''Nadja Yet''}}</ref>

The film ''Lover's Prayer'' combining Turgenev's novella and Chekhov's ''The Peasant Woman'' was released in 1999.

The Tamil [[Tamil language]] movie ''[[Sindhu Samaveli]]'' is an adaptation of ''First Love''. In literal context, ''Sindhu Samaveli'' is the Tamil name for [[Indus Valley Civilization]]

==References==

{{Reflist}}

==External links==
{{Gutenberg|no=9911|name=Torrents of Spring}} Trans. Constance Garnett. Includes ''First Love'' along with two other Turgenev stories, ''[[Torrents of Spring]]'' and ''[[Mumu (novella)|Mumu]]''.
* {{librivox book | title=First Love | author=Ivan TURGENEV}}
* [http://ilibrary.ru/text/1335/ Full text of ''First Love'' in the original Russian] at Alexei Komarov's Internet Library

{{Ivan Turgenev}}

{{Authority control}}

{{DEFAULTSORT:First Love (Novella)}}
[[Category:1860 novels]]
[[Category:Frame stories]]
[[Category:Novellas by Ivan Turgenev]]
[[Category:Russian novels adapted into films]]