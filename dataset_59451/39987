{{Use dmy dates|date=June 2013}}
[[File:Alcockandbrown mail.jpg|thumb|right|upright|Alcock and Brown taking on mail]]
[[File:alcock.brown.statue.arp.750pix.jpg|thumb|right|upright|Statue of Alcock and Brown at [[London Heathrow Airport]]]]
British aviators '''John Alcock and Arthur Brown''' made the first non-stop [[transatlantic flight]] in June 1919.<ref>http://www.aviation-history.com/airmen/alcock.htm</ref> They flew a modified First World War [[Vickers Vimy]]<ref>{{cite web|title=Alcock and Brown's Vickers Vimy biplane, 1919|url=http://collectionsonline.nmsi.ac.uk/detail.php?t=objects&type=all&f=&s=vimy&record=0|publisher=Science Museum|accessdate=4 February 2017|author=|date=}}</ref> bomber from [[St. John's, Newfoundland and Labrador|St. John's]], [[Dominion of Newfoundland|Newfoundland]], to [[Clifden]], [[Connemara]], [[County Galway]], Ireland.<ref>{{cite news |author= |coauthors= |title=What are the wild waves saying |url=http://www.economist.com/node/17358828?story_id=17358828&CFID=147268996&CFTOKEN=45472874 |quote=Jack Alcock and Arthur Whitten Brown became the first men to cross the Atlantic by air in June 1919, flying in a Vickers Vimy biplane, its bomb bays filled with extra fuel. The dashing aviators, who took their pet kittens, Twinkletoes and Lucky Jim, with them, made the crossing from Newfoundland to County Galway in 16 hours and 27 minutes. |work=[[The Economist]] |date=28 October 2010 |accessdate=3 November 2010 }}</ref> The [[Secretary of State for Air]], [[Winston Churchill]], presented them with the [[Daily Mail aviation prizes|''Daily Mail'' prize]] for the first crossing of the Atlantic Ocean by aeroplane in "less than 72 consecutive hours".<ref name="DailyMail1913">{{cite news
| title = ₤10,000 for first transatlantic flight (in 72 continuous hours)
| url = http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200387.html
| work = Flight magazine
| page = 393
| date = 5 April 1913
| accessdate =5 January 2009
}}</ref> A small amount of mail was carried on the flight, making it the first transatlantic airmail flight. The two aviators were awarded the honour of [[Order of the British Empire|Knight Commander of the Most Excellent Order of the British Empire (KBE)]] a week later by [[George V of the United Kingdom|King George V]] at [[Windsor Castle]].

==Background==
[[John Alcock (aviator)|John Alcock]] was born in 1892 in Basford House on Seymour Grove, [[Firswood]], [[Manchester]], [[England]]. Known to his family and friends as "Jack", he first became interested in flying at the age of seventeen and gained his pilot's licence in November 1912. Alcock was a regular competitor in aircraft competitions at [[Hendon Aerodrome|Hendon]] in 1913–14. He became a military pilot during the [[First World War]] and was taken [[Prisoner of war|prisoner]] in [[Gallipoli Campaign|Turkey]] after the engines on his [[Handley Page]] bomber failed over the [[Gulf of Xeros]].<ref>{{cite web|title=Captain Jack Alcock (1892–1919)|url=http://www.mosi.org.uk/media/33871083/captainjackalcock.pdf|publisher=Museum of Science & Industry|accessdate=13 September 2013|author=|date=2006}}</ref> After the war, Alcock wanted to continue his flying career and took up the challenge of attempting to be the first to fly directly across the [[Atlantic Ocean|Atlantic]].

[[Arthur Whitten Brown]] was born in [[Glasgow]], [[Scotland]] in 1886 to [[United States|American]] parents, and shortly afterwards the family moved to Manchester. Known to his family and friends as "Teddie", he began his career in engineering before the outbreak of the First World War. Brown also became a prisoner of war, after being shot down over [[Germany]]. Once released and back in Britain, Brown continued to develop his [[aerial navigation]] skills.

In April 1913 the London newspaper the [[Daily Mail aviation prizes|''Daily Mail'' offered a prize of £10,000]]<ref>Nevin, David. "Two Daring Flyers Beat the Atlantic before Lindbergh." ''Journal of Contemporary History 28: (1) 1993, 105.</ref> to {{Cquote|the aviator who shall first cross the Atlantic in an aeroplane in flight from any point in the United States of America, Canada or [[Dominion of Newfoundland|Newfoundland]] and any point in Great Britain or Ireland in 72 continuous hours.<ref name="DailyMail1918">{{cite news
| title = ₤10,000 for first transatlantic flight (in 72 consecutive hours)
| url = http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%201315.html
| work = ''Flight'' magazine
| page = 1316
| date = 21 November 1918
| accessdate =5 January 2009
}}</ref>}}

The competition was suspended with the outbreak of war in 1914 but reopened after [[Armistice with Germany|Armistice]] was declared in 1918.<ref name="DailyMail1918"/>

During his imprisonment Alcock had resolved to fly the Atlantic one day, and after the war he approached the [[Vickers]] engineering and aviation firm at [[Weybridge]], who had considered entering their [[Vickers Vimy]] IV twin-engined bomber in the competition but had not yet found a pilot. Alcock's enthusiasm impressed the Vickers' team and he was appointed as their pilot. Work began on converting the Vimy for the long flight, replacing the [[bomb rack]]s with extra petrol tanks.<ref>Cooksley, Peter G. ‘Alcock, Sir John William (1892–1919)’, in [http://www.oxforddnb.com/view/article/30363 ''Oxford Dictionary of National Biography''] (Oxford University Press, 2004), online ed., Jan 2011; accessed 16 June 2012.</ref> Shortly afterwards Brown, who was unemployed, approached Vickers seeking a post and his knowledge of long distance navigation convinced them to take him on as Alcock's navigator.<ref>Shepherd, E. C. ‘Brown, Sir Arthur Whitten (1886–1948)’, rev. Peter G. Cooksley, in [http://www.oxforddnb.com/view/article/30363 ''Oxford Dictionary of National Biography''] (Oxford University Press, 2004), online ed., Jan 2011; accessed 16 June 2012.</ref>

==Flight {{Anchor|Flight}}==
{{Location map+|North Atlantic|relief=1|float=right|width=250|alt=A map of the North Atlantic|caption=Location map|places=
{{Location map~|North Atlantic|lat=47.56|long=-52.7|position=top|label=<div style="position: relative; top: 2em; left: -2.4em;">St. John's</div>}}
{{Location map~|North Atlantic|lat=53.48|long=-10.1|position=top|label=<div style="position: relative; top: -0.1em; left: 1.8em;">Clifden</div>}}
}}
[[File:Alcockandbrown takeoff1919.jpg|thumb|right|Alcock and Brown takeoff from St. John's, Newfoundland in 1919]]
[[File:Alcock-Brown-Clifden.jpg|thumb|right|Alcock and Brown landing in Ireland 1919]]

Several teams had entered the competition and when Alcock and Brown arrived in [[St. John's, Newfoundland and Labrador|St. John's, Newfoundland]], the [[Handley Page]] team were in the final stages of testing their aircraft for the flight, but their leader, Admiral Mark Kerr, was determined not to take off until the plane was in perfect condition. The Vickers team quickly assembled their plane and at around 1:45&nbsp;p.m. on 14 June, whilst the Handley Page team were conducting yet another test, the Vickers plane took off from Lester's Field.<ref name="Century">{{cite web|url=http://www.century-of-flight.net/Aviation%20history/daredevils/Atlantic%202.htm|title=The Atlantic Challenge:Alcock and Brown Take the Atlantic|last=Anon|work=Century of Flight|publisher=Centuryofflight.net|accessdate=16 June 2012}}</ref> Alcock and Brown flew the modified Vickers Vimy, powered by two [[Rolls-Royce Eagle]] 360&nbsp;hp engines which were supported by an on-site Rolls Royce team lead by engineer [[Eric Platford]].<ref>{{cite web
|url=http://www.aviation-history.com/airmen/alcock.htm|title=Alcock and Brown|publisher=Aviation History Online Museum}}</ref>

It was not an easy flight. The overloaded aircraft had difficulty taking off the rough field and only barely missed the tops of the trees.<ref name=avhist>{{cite web | url=http://www.aviation-history.com/airmen/alcock.htm | title=Capt. John Alcock and Lt. Arthur Whitten Brown}}</ref><ref name=fglobal>{{cite web | url=https://www.flightglobal.com/pdfarchive/view/1969/1969%20-%202238.html | title=Flight Global article}}</ref>
At 17:20 the wind-driven electrical generator failed, depriving them of radio contact, their intercom and heating.<ref name=fglobal/>
An exhaust pipe burst shortly afterwards, causing a frightening noise which made conversation impossible without the failed intercom.<ref name=avhist/><ref name=fglobal/>

At 5.00pm they had to fly through thick fog.<ref name=avhist/>  This was serious because it prevented Brown from being able to navigate using his sextant.<ref name=avhist/><ref name=fglobal/>
Blind flying in fog or cloud should only be undertaken with gyroscopic instruments, which they did not have, and Alcock twice lost control of the aircraft and nearly hit the sea after a spiral dive.<ref name=avhist/><ref name=fglobal/>  Alcock also had to deal with a broken trim control that made the plane become very nose-heavy as fuel was consumed.<ref name=fglobal/>

At 12:15am Brown got a glimpse of the stars and could use his sextant, and found that they were on course.<ref name=avhist/><ref name=fglobal/>
Their electric heating suits had failed, making them very cold in the open cockpit, but their coffee was spiked with whiskey.<ref name=avhist/>

Then at 3:00am they flew into a large snowstorm.<ref name=avhist/>  They were drenched by rain, their instruments iced up, and the plane was in danger of icing and becoming unflyable.<ref name=avhist/>  The carburettors also iced up; it has been said that Brown had to climb out onto the wings to clear the engines, although he made no mention of that.<ref name=avhist/><ref name=fglobal/>

They made landfall in [[County Galway]] at 8:40&nbsp;a.m. on 15 June 1919, not far from their intended landing place, after less than sixteen hours' flying time.
The aircraft was damaged upon arrival because of an attempt to land on what appeared from the air to be a suitable green field, but which turned out to be a [[bog]], near [[Clifden]] in County Galway in Ireland, but neither of the airmen was hurt.<ref name="Century"/><ref>Listen to interviews with people that met the plane on Bowman Sunday Morning, RTÉ radio archives, http://www.rte.ie/radio1/bowmansundaymorning/1249939.html</ref>
Brown said that if the weather had been good they could have pressed on to London.<ref name=fglobal/>

Their altitude varied between sea level and 12,000&nbsp;ft (3,700 m). They took off with 865 [[imperial gallon]]s (3,900 L) of fuel.  They had spent around fourteen-and-a-half hours{{citation needed|date=April 2016}} over the North Atlantic crossing the coast at 4:28&nbsp;p.m.,<ref>Straightforward calculation: ['Arrived' (locally)- 'Departed' + Tzones Compensation ] = [16:28 hrs – 13:50 hrs (est fm 1:45pm takeoff) + 12 (comp for day changed+Tzones factor)] = apx (2.5 + 12) hrs = 14.5 hrs (approximate minimum time) over the water</ref> having flown 1,890 miles (3,040&nbsp;km) in 15 hours 57 minutes at an average speed of 115&nbsp;mph (185&nbsp;km/h).<ref>Inscription, Alcock and Brown memorial, near Clifden, Ireland</ref> Their first interview was given to [[Tom 'Cork' Kenny]] of ''[[Connacht Tribune|The Connacht Tribune]]''.
[[File:Alcock & Brown Civic Reception 1919.jpg|thumb|left|upright|Cover of civic reception programme for Alcock & Brown, given by the Corporation of Manchester on 17 July 1919]]

Alcock and Brown were treated as heroes on the completion of their flight.<ref>{{cite news |author= |coauthors= |title=Alcock And Brown Get London Ovation. Carried to Automobiles on Shoulders of Soldiers on Arrival from Dublin. Aerial Escort For Train. First Nonstop Transatlantic Fliers Parade and Are Entertained by Aero Club |url=https://select.nytimes.com/gst/abstract.html?res=FA0713FB3C5E157A93CAA8178DD85F4D8185F9 |quote=London gave Captain Alcock and Lieutenant Brown a wonderful welcome tonight. ...|work=[[New York Times]] |date=18 July 1919 |accessdate=3 November 2010 }}</ref> In addition to the Daily Mail award of £10,000, the crew received 2,000 [[Guinea (British coin)|guineas]] (£2,100) from the Ardath Tobacco Company and £1,000 from Lawrence R. Phillips for being the first [[British subject]]s to fly the Atlantic Ocean. Both men were knighted a few days later by [[George V of the United Kingdom|King George V]].<ref name="Knight2">{{cite news
| title = Alcock and Brown Knighted by King George V
| url = http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200830.html
| work = Flight magazine
| page = 830
| date = 26 June 1919
| accessdate =9 January 2009
}}</ref>

Alcock and Brown flew to [[Manchester]] on 17 July 1919, where they were given a civic reception by the [[Lord Mayor of Manchester|Lord Mayor]] and [[Corporation of Manchester|Corporation]], and awards to mark their achievement.

===Memorials===
Alcock was killed on 18 December 1919 when he crashed near [[Rouen]] whilst flying the new [[Vickers Viking]] amphibian to the [[Paris Airshow]]. Brown died on 4 October 1948.

[[File:Alcock brown landing site.jpg|thumb|right|Landing site, County Galway]]
Two memorials commemorating the flight are sited near the landing spot in County Galway, Ireland. The first is an isolated cairn four kilometres south of Clifden on the site of [[Guglielmo Marconi|Marconi's]] first transatlantic wireless station from which the aviators transmitted their success to London, and around 500 metres from the spot where they landed. In addition there is a sculpture of an aircraft's tail-fin on Errislannan Hill two kilometres north of their landing spot, dedicated on the fortieth anniversary of their landing, 15 June 1959.
[[File:Alcock brown memorial.jpg|thumb|right|Memorial, County Galway]]

Three monuments mark the flight's starting point in Newfoundland. One was erected by the government of Canada in 1954 at the junction of Lemarchant Road and Patrick Street in St. John's,<ref>[http://www.pc.gc.ca/rech-srch/clic-click.aspx?/cgi-bin/MsmGo.exe?grab_id=0&page_id=27414&query=alcock%20brown&hiword=BROW%20BROWNE%20BROWNED%20BROWNES%20BROWNING%20BROWNS%20BROWS%20alcock%20brown%20 Alcock – Brown Transatlantic Flight National Historic Event], Parks Canada</ref> a second monument is located on Lemarchant Road,<ref>http://www.stjohns.ca/sites/default/files/files/publication/Streets,%20Areas,%20Monuments,%20Plaques.pdf</ref> while the third was unveiled by then Premier of Newfoundland Joey Smallwood on Blackmarsh Road.<ref>{{cite news|last1=MacEachern|first1=Daniel|title=St. John's finance committee rejects request for funding to repair vandalized monument|url=http://www.thetelegram.com/News/Local/2014-10-18/article-3907370/St.-Johns-finance-committee-rejects-request-for-funding-to-repair-vandalized-monument/1|accessdate=21 October 2015|work=The Telegrah|date=October 18, 2014}}</ref>

[[File:Vickers Vimy (6436284927).jpg|thumb|Alcock and Brown's Vickers Vimy in the [[London Science Museum|Science Museum]] ]]

A memorial statue was erected at [[London Heathrow Airport]] in 1954 to celebrate their flight. There is also a monument at [[Manchester Airport]], less than 8 miles from John Alcock's birthplace. Their aircraft (rebuilt by the Vickers Company) can be seen in the [[London Science Museum|Science Museum]] in [[South Kensington]], [[London]].

The [[Royal Mail]] issued a 5d (approximately 2.1p in modern UK currency) [[List of United Kingdom commemorative stamps|stamp]] commemorating the 50th anniversary of the flight on 2 April 1969.

===Memorabilia===

On 19 March 2017 an edition of the [[Antiques Roadshow]] was broadcast in the UK in which the granddaughter of Alcock's cousin presented a handwritten note which was carried by Alcock on the flight. The note, which was valued at £1000 - £1200 read as follows:

The Cochrane<br/>
St John's June 12th 1919<br/>

My dear Elsie<br/>
Just a hurried line before<br/>
I start. This letter will<br/>
travel with me in the<br/>
official mail bag, the<br/> 
first mail to be carried<br/>
over the Atlantic.<br/>
:Love to all,<br/>
Your loving Brother<br/>
:Jack <ref>{{Cite episode |title=Burton Constable 2 |episode-link= |url= |access-date= |series=The Antiques Roadshow |series-link= |first= |last= |network=BBC |station=BBC 1 |date=19 March 2017 |season= |series-no=39 |number= |minutes= |time= |transcript= |transcript-url= |quote= |language=}}</ref>

==Other crossings==
{{Main article|Transatlantic flight}}
Two weeks before Alcock and Brown's flight, the first transatlantic flight had been made by the [[NC-4]], a [[United States Navy]] [[flying boat]], commanded by Lt. Commander [[Albert Cushing Read]], who flew from [[Naval Air Station Rockaway]], [[New York City|New York]] to [[Plymouth]] with a crew of five, over 23 days, with six stops along the way. This flight was not eligible for the Daily Mail prize since it took more than 72 consecutive hours and also because more than one aircraft was used in the attempt.<ref name=DailyMailprize1918>{{cite web|url= http://www.flightglobal.com/pdfarchive/view/1918/1918%20-%201315.html|title= Daily Mail £10,000 prize conditions 1918}}</ref>

A month after Alcock and Brown's achievement, British airship [[R34 (airship)|R34]] made the first double crossing of the Atlantic, carrying 31 people (one a stowaway) and a cat;<ref>{{cite book |last=Abbott |first=Patrick |title=Airship: The Story of R34 |year=1994 |publisher=Brewin Books |location=[[Studley, Warwickshire|Studley]], Warwickshire |isbn=1-85858-020-X |pages=13–14}}</ref> 29 of this crew, plus two flight engineers and a different American observer, then flew back to Europe.<ref>{{cite book |title=Airship |last=Abbott |page=64}}</ref>

On 2–3 July 2005, American adventurer [[Steve Fossett]] and co-pilot Mark Rebholz recreated the flight in a replica of the Vickers Vimy aeroplane. They did not land in the bog near Clifden, but a few miles away on the [[Connemara]] golf course. They had to call on the services of a local motor mechanic to fabricate a replacement part from materials at hand.<ref>http://www.ctv.ca/servlet/ArticleNews/story/CTVNews/20050601/fossett_biplane_20050601?s_name=Autos&no_ads=</ref>

A replica Vimy, NX71MY, was built in Australia and the USA in 1994 for an American, Peter McMillan, who flew it from England to Australia with Australian Lang Kidby in 1994 to re-enact the first England-Australia flight by Ross & Keith Smith with Vimy G-EAOU in 1919. In 1999, Mark Rebholz and John LaNoue re-enacted the first flight from London to [[Cape Town]] with this same replica, and in late 2006 the aeroplane was donated to [[Brooklands Museum]] at [[Weybridge]], [[Surrey]]. After making a special Alcock & Brown 90th anniversary return visit to Clifden in June 2009 (flown by John Dodd and Clive Edwards), and some final public flying displays at the [[Goodwood Revival]] that September, the Vimy made its final flight on 15 November 2009 from [[Dunsfold Park]] to [[Brooklands]] crewed by John Dodd (pilot), Clive Edwards and Peter McMillan. Retired from flying for the foreseeable future, it is now on public display in the Museum's [[Bellman hangar]] but will be maintained to full airworthy standards.

One of the propellers from the Vickers Vimy was given to Arthur Whitten Brown and hung for many years on the wall of his office in [[Swansea]] before he presented it to the [[RAF College Cranwell]]. It is believed to have been displayed in the RAF Careers Office in [[Holborn]] until 1990.<ref>{{cite web|url=http://www.walesonline.co.uk/showbiz-and-lifestyle/books/news/2009/12/01/writer-seeks-out-propeller-from-historic-plane-that-first-crossed-the-atlantic-ocean-91466-25292306/|title=Writer seeks propeller from plane that first crossed Atlantic |last=Turner|first=Robin|date=1 December 2009|work=walesonline|publisher=Media Wales Ltd|accessdate=15 June 2012}}</ref> It is believed to be in use today as a ceiling fan in Luigi Malone's Restaurant in [[Cork (city)|Cork]], Ireland.<ref>{{cite book |author=Fallon, Linda |title=Cork, 2nd: The Bradt City Guide (Bradt Mini Guide) |publisher=Bradt Travel Guides |location= |year=2007 |pages=105 |isbn=1-84162-196-X |oclc= |doi= |accessdate=}}</ref>

The other propeller, serial number G1184.N6, was originally given to the Vickers Works Manager at Brooklands, Percy Maxwell Muller and displayed for many years suspended inside the transatlantic terminal ([[Heathrow Terminal 3|Terminal 3]]) at London's [[Heathrow Airport]]. In October 1990 it was donated by the BAA (via its former Chairman, Sir Peter Masefield) to Brooklands Museum, where it is now motorised and displayed as part of a full-size Vimy wall mural.

A small amount of mail, 196 letters and a parcel, was carried on Alcock and Brown's flight, the first time mail was carried by air across the ocean. The government of the [[Dominion of Newfoundland]] [[overprinted]] stamps for this carriage with the inscription "Transatlantic air post 1919".<ref>{{cite web |url=http://www.icao.int/secretariat/PostalHistory/international_exhibition_of_air_mail_postage_1969.htm |title=The Postal History of ICAO - International Exhibition of Air Mail Postage|website=International Civil Aviation Organization|date=1969|author= |accessdate= 16 June 2016}}</ref><ref name="NYT">{{cite news|url=http://query.nytimes.com/mem/archive-free/pdf?res=F00B12F83C5E157A93CBA8178DD85F4D8185F9|title=Deliver Atlantic Air Mail: Alcock and Bown leave letters they carried in London|last=Anon|date=19 June 1919|work=The New York Times|publisher=The New York Times|pages=8|accessdate=15 June 2012}}</ref>

Upon landing in Paris after his own record breaking flight in 1927, [[Charles Lindbergh]] told the crowd welcoming him that "Alcock and Brown showed me the way!"<ref>http://www.articlearchives.com/international-relations/national-security-foreign-defense/535928-1.html</ref>

==See also==
* [[Curtiss NC-4]] – First transatlantic flight via Azores to Portugal
* [[Daily Mail Trans-Atlantic Air Race]] – An event to celebrate the 50th anniversary of the flight
* [[Felixstowe Fury]] – Contender for the transatlantic crossing
* [[List of firsts in aviation]]
* [[R34 (airship)]] – First airship transatlantic crossing, also first east-west crossing
* [[Timeline of aviation]]

==References==
{{Reflist|30em}}

==Further reading==
*Lynch, Brendan (2009). ''Yesterday We Were in America&nbsp;— Alcock and Brown&nbsp;— First to fly the Atlantic non-stop'' (Haynes, ISBN 978-1-84425-681-5)

==External links==
* [http://www.fi.edu/flights/long/ Alcock and Brown at "Flights of Inspiration"]
* [http://www.sciencemuseum.org.uk/objects/aeronautics/1919-476.aspx Alcock and Brown's plane at the London Science Museum]
* [http://www.brooklandsmuseum.com Brooklands Museum website]
* [http://www.newyorker.com/fiction/features/2012/04/16/120416fi_fiction_mccann/ Colum McCann fiction short story based on Alcock and Brown's flight]
* [http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200801.html "The Transatlantic Flight"] a 1919 ''Flight'' article on the flight


[[Category:British aviators|Alcock and Brown]]
[[Category:Aviation pioneers|Alcock and Brown]]
[[Category:History of St. John's, Newfoundland and Labrador]]
[[Category:Aviation history of the United Kingdom]]
[[Category:Aviation history of Canada]]
[[Category:Transatlantic flight]]
[[Category:Aviation accidents and incidents in Ireland]]