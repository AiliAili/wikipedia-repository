<!-- text begins below this table - scroll down to edit-->
{|{{Infobox Aircraft Begin
  |name = Albatros D.V
  |image = Albatros D.Va Duxford Airshow 2012.jpg
  |caption = <small>'''Albatros D.Va''' reproduction  at Duxford Air Show, 2012</small>
}}
{{Infobox Aircraft Type
  |type = [[Fighter plane|Fighter]]
  |manufacturer = [[Albatros-Flugzeugwerke]]   
  |designer = Robert Thelen
  |first flight = April 1917
  |introduced = 
  |retired =
  |status =
  |primary user = ''[[Luftstreitkräfte]]''
  |more users = 
  |produced = 
  |number built = approximately 2500
  |unit cost =
  |variants with their own articles =
}}|}
The '''Albatros D.V''' was a [[fighter aircraft]] used by the  ''[[Luftstreitkräfte]]'' (Imperial German Air Service) during [[World War I]]. The D.V was the final development of the Albatros D.I family, and the last Albatros fighter to see operational service. Despite its well-known shortcomings and general obsolescence, approximately 900 D.V and 1,612 D.Va aircraft were built before production halted in early 1918. The D.Va continued in operational service until the end of the war.

==Design and development==
[[File:DVa.jpg|thumb|right|Captured '''Albatros D.V''' (serial D.1162/17) with British roundels]]
[[File:DVa2.jpg|thumb|right|[[Manfred von Richthofen]]'s '''Albatros D.V''' (serial unknown)]]

In April 1917, Albatros received an order from the ''[[Idflieg]]'' (''Inspektion der Fliegertruppen'') for an improved version of the [[Albatros D.III|D.III]]. The resulting D.V prototype flew later that month.

The D.V closely resembled the D.III and used the same 127&nbsp;kW (170&nbsp;hp) [[Mercedes D.III]]a engine. The most notable difference was a new, fully elliptical cross-section [[fuselage]] which was 32&nbsp;kg (70&nbsp;lb) lighter than the partially flat-sided fuselage of the earlier D.I through D.III designs.<ref name="Mikesh p15">Mikesh 1980, p. 15.</ref> The new elliptical cross-section required an additional longeron on each side of the fuselage. The vertical fin and tailplane initially remained unchanged from the D.III.<ref name="Mikesh p15" /> The prototype D.V retained the standard rudder of the Johannisthal-built D.III, but production examples used the enlarged [[rudder]] featured on D.IIIs built by Ostdeutsche Albatros Werke (OAW).<ref name="Grosz pp21-22">Grosz 2003, pp. 21-22.</ref> The D.V also featured a larger spinner and ventral fin.

Compared to the D.III, the upper wing of the D.V was repositioned 4.75&nbsp;inches closer to the fuselage, while the lower wings attached to the fuselage without a [[Aircraft fairing|fairing]]. The D.V's wings themselves were almost identical to those of the standard D.III, which had adopted a [[sesquiplane]] wing arrangement broadly similar to the French [[Nieuport 11]]. The only significant difference between wings of the D.III and D.V was a revised linkage of the [[aileron]] cables, which in the new aircraft was contained entirely in the upper wing.<ref name="Connors p22">Connors 1981, p. 22.</ref> ''Idflieg'' therefore conducted structural tests on the fuselage, but not the wings, of the D.V.<ref name="Van Wyngarden p40">Van Wyngarden 2007, p. 40.</ref>

Early examples of the D.V featured a large headrest, which was usually removed in service because it interfered with the pilot's field of view.<ref name="Connors p22"/> The headrest was eventually deleted from production. Aircraft deployed in [[Palestine (region)|Palestine]] used two wing [[radiator (engine cooling)|radiators]], to cope with the warmer climate.

''[[Idflieg]]'' issued production contracts for 200 D.V aircraft in April 1917, followed by additional orders of 400 in May and 300 in July.<ref name="Van Wyngarden p40"/> Initial production of the D.V was exclusively undertaken by the Johannisthal factory, while the Schneidemühl factory produced the D.III through the remainder of 1917.

==Operational history==
[[File:AlbatrosD.jpg|thumb|right|'''Albatros D.Va''' (serial D.7098/17)]]
[[File:Deskau.jpg|thumb|right|'''Albatros D.Va''' (serial D.5629/17)]]

The D.V entered service in May 1917 and, like the D.III before it, immediately began experiencing structural failures of the lower wing.<ref name="Van Wyngarden p40"/> Indeed, anecdotal evidence suggests that the D.V was even more prone to wing failures than the D.III. The outboard sections of the upper wing also suffered failures, requiring additional wire bracing.<ref name="Van Wyngarden p40"/> Furthermore, the D.V offered very little improvement in performance.<ref name="Connors p22"/> This caused considerable dismay among frontline pilots, many of whom preferred the older D.III. [[Manfred von Richthofen]] was particularly critical of the new aircraft. In a July 1917 letter, he described the D.V as "so obsolete and so ridiculously inferior to the English that one can't do anything with this aircraft." British tests of a captured D.V revealed that the aircraft was slow to maneuver, heavy on the controls, and tiring to fly.<ref name="Bennett p124">Bennett 2006, p. 124.</ref>

Albatros responded with the '''D.Va''', which featured stronger wing spars, heavier wing ribs, and a reinforced fuselage.<ref name="Van Wyngarden p65">Van Wyngarden 2007, p. 65.</ref> These modifications made the D.Va 23&nbsp;kg (50&nbsp;lb) heavier than the D.III, while failing to cure entirely the structural problems of the type. Use of the high-compression 130&nbsp;kW (180&nbsp;hp) Mercedes D.IIIaü engine offset the increased weight of the D.Va.<ref name="Mikesh p17">Mikesh 1980, p. 17.</ref> The D.Va also reverted to the D.III's aileron cable linkage, running outwards through the lower wing, then upwards to the ailerons, to provide a more positive control response.<ref name="Connors p22" /> The wings of the D.III and D.Va were in fact interchangeable.<ref name="Connors p22" /> The D.Va was also fitted with a small diagonal brace connecting the lower section of the forward [[interplane strut]] to the leading edge of the lower wing. This brace was retrofitted to some D.V aircraft.<ref name="Mikesh p17"/>

''Idflieg'' placed orders for 262 D.Va aircraft in August 1917, followed by additional orders for 250 in September and 550 in October.<ref name="Van Wyngarden p65"/> Ostdeutsche Albatros Werke, which had been engaged in production of the D.III, received orders for 600 D.Va aircraft in October.<ref name="Van Wyngarden p65"/>

Deliveries of the D.Va commenced in October 1917.<ref name="Van Wyngarden p65"/> The structural problems of the [[Fokker Dr.I]] and the mediocre performance of the [[Pfalz D.III]] left the ''Luftstreitkräfte'' with no viable alternative to the D.Va until the [[Fokker D.VII]] entered service in mid-1918. Production ceased in April 1918.<ref name="Mikesh p7">Mikesh 1980, p. 7.</ref> As of May 1918, 131 D.V and 928 D.Va aircraft were in service on the Western Front. This number declined as the Albatros was replaced by Fokker D.VIIs and other types during the final months of the war, but the D.Va remained in use until the [[Armistice with Germany (Compiègne)|Armistice]] (11 November 1918).

==Survivors==
[[File:Albatross DVa at the Australian War Memorial July 2015.jpg|thumb|'''D.Va''' (serial D.5390/17) on display at the Australian War Memorial in Canberra]]

Today, two D.Va aircraft survive in museums.
* It is believed serial D.7161/17 served with ''Jasta'' 46 before being captured sometime in April or May 1918. In 1919, the aircraft was presented to the De Young Memorial Museum in San Francisco, California. The [[National Air and Space Museum]] acquired the aircraft in 1949. It was placed in storage until restoration began in 1977. Since 1979, D.7161/17 has been on display at the Air and Space Museum, in Washington D.C.  This aircraft carries the distinctive personal marking of "Stropp" on the fuselage sides.
* Serial D.5390/17 was shot down during a fight with an [[Australian Flying Corps]] [[Royal Aircraft Factory R.E.8|R.E.8]] on 17 December 1917. It landed intact behind the lines of the 21st Infantry Battalion of the Second Australian Division, [[First Australian Imperial Force|AIF]]. The unit recovered the aircraft and took the pilot, ''Leutnant'' Rudolf Clausz of ''Jasta'' 29, prisoner.<ref name="Australian War Memorial">[http://cas.awm.gov.au/item/RELAWM04806 "RELAWM04806 - Albatros D.Va Scout Aircraft."] ''Australian War Memorial''. Retrieved: 29 May 2012.</ref> In February 1918, the [[War Office]] ceded D.5390/17 to the AFC as a war trophy. It was put on display at the Australian War Memorial.<ref name="Australian War Memorial"/> The aircraft was removed from display in 2001 and underwent extensive restoration at the Treloar Technology Centre.<ref>[http://www.awm.gov.au/blog/2008/04/03/conservation-of-the-albatros-dva/ "Albatros fabric research blog post."] ''Australian War Memorial''. Retrieved: 28 May 2012.</ref> In 2008, D.5390/17 returned to public display at the [[Australian War Memorial|AWM]]'s ANZAC Hall in Canberra.

==Operators==
;{{flag|German Empire}}
*''[[Luftstreitkräfte]]''
*''[[Kaiserliche Marine]]''
;{{POL}}
*[[Polish Air Force]] (postwar)
;{{flagcountry|Ottoman Empire}}
*[[Ottoman Air Force]]

==Specifications (D.V)==
[[File:Albatros D.V dwg.jpg|thumb|Official Albatros D.V ''Baubeschreibung'' drawing, submitted to ''[[IdFlieg]]'']]
{{Aircraft specs
|ref=German Aircraft of the First World War<ref name="Gray p52">Gray and Thetford 1970, p. 52.</ref>
<!-- reference -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, 
met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|length m=7.33
|span m=9.05
|height m=2.7
|wing area sqm=21.2
|empty weight kg=687
|gross weight kg=937
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Mercedes D.IIIaü]]
|eng1 type=piston engine
|eng1 hp=200<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=wooden propeller
<!--
        Performance
-->
|max speed kmh=186
|endurance=350 km
|ceiling m=5,700
|climb rate ms=4.17
|time to altitude={{convert|1,000|m|ft|abbr=on|0}} in 4 minutes
<!--
        Armament
-->
|guns= 2 ×  {{convert|7.92|mm|in|abbr=on|3}} [[LMG 08/15]] [[machine gun]]s
}}

== Gallery ==

<gallery>
Image:Albatros1_Wiki.jpg|Albatros D.V belonging to [[Paul Bäumer]], from [[Jagdstaffel 5]]
Image:Albatros2_Wiki.jpg|Albatros D.V from [[Jagdstaffel 15]] in early 1918, belonging to Lieutenant Dingel
Image:Albatros DVa Hippel.jpg|Albatros D.Va, Hans von Hippel, [[Jagdstaffel 5]]
Image:Albatros DVa Udet.jpg|Albatros D.Va, [[Ernst Udet]], Jasta 37
Image:Albatros_Manfred_von_Richthofen_neu.jpg|The red Albatros D.V of the leader of [[Jagdgeschwader 1 (World War I)|JG 1]], [[Manfred von Richthofen]].
Image:Albatros D.V Jasta 79b Seite li. klein72.jpg|Albatros D.Va from Jasta 79b, flown by Lieutenant Hans Böhning in early 1918
</gallery>

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
* [[List of military aircraft of Germany]]
* [[List of fighter aircraft]]
|see also=
}}

==References==
;Notes
{{Reflist|2}}

;Bibliography
{{Refbegin}}
* Bennett, Leon. ''Gunning for the Red Baron''. College Station, Texas: Texas A&M University Press, 2006. ISBN 1-58544-507-X.
* Connors, John F.  ''Albatros Fighters in Action (Aircraft No. 46)''. Carrollton, Texas: Squadron/Signal Publications, Inc., 1981. ISBN 0-89747-115-6.
* Gray, Peter and Owen Thetford. ''German Aircraft of the First World War''. London: Putnam & Company Ltd., 1970. 2nd ed. ISBN 0-370-00103-6.
* Green, William and Gordon Swanborough. ''The Complete Book of Fighters''. London: Salamander Books, 1994. ISBN 0-8317-3939-8.
* Grosz, Peter M. ''Albatros D.III (Windsock Datafile Special)''. Berkhamsted, Herts, UK: Albatros Publications, 2003. ISBN 1-902207-62-9.
* Mikesh, Robert C. ''Albatros D.Va: German Fighter of World War I''. Washington, D.C.: Smithsonian Institution Press, 1980. ISBN 0-87474-633-7
* VanWyngarden, Greg. ''Albatros Aces of World War I Part 2 (Aircraft of the Aces No. 77)''. Oxford, UK: Osprey Publishing, 2007. ISBN 1-84603-179-6.
{{Refend}}

==External links==
{{commons category|Albatros D.V}}
* [http://airandspace.si.edu/collections/artifact.cfm?object=nasm_A19500092000 Smithsonian NASM's Albatros D.Va page]
* [http://oldrhinebeck.org/ORA/albatros-d-va/ Old Rhinebeck Aerodrome's reproduction Albatros D.Va page]

{{Albatros aircraft}}
{{World War I Aircraft of the Central Powers}}
{{Idflieg fighter designations}}
{{wwi-air}}

{{Authority control}}

[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:German fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Albatros aircraft|D.05]]