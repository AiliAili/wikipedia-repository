'''Enhanced publications''' or '''enhanced ebooks''' are a form of [[electronic publishing]] for the dissemination and sharing of research outcomes, whose first formal definition can be tracked back to 2009.<ref name="book"/> As many forms of digital publications, they typically feature a unique identifier (possibly a [[persistent identifier]]) and descriptive [[metadata]] information. Unlike traditional digital publications (e.g. PDF article), enhanced publications are often tailored to serve specific scientific domains and are generally constituted by a set of interconnected ''parts'' corresponding to research assets of several kinds (e.g. datasets, videos, images, stylesheets, services, workflows, databases, presentations) and to textual descriptions of the research (e.g. papers, chapters, sections, tables). The nature and format of such parts and of the relationships between them, depends on the application domain and may largely vary from case to case.

The main motivations behind enhanced publications are to be found in the limitations of traditional scientific literature to describe the whole context and outcome of a research activity. Their goal is to move "beyond the simple PDF" ([[FORCE11]] initiative<ref name="force11"/>) and support scientists with advanced [[Information and communications technology|ICT]] tools for sharing their research more comprehensively, without losing the narrative spirit of "the publication" as dissemination means. This trend is confirmed by the several enhanced publication systems devised in the literature, offering to research communities one or more of the following functionalities: Packaging of related research assets; Web 2.0 reading capabilities; Interlinking research outputs; Re-production and assessment of scientific experiments.

== Packaging of related research assets ==

The first enhancement introduced to move beyond the mare digitization of the publication and investigate new avenues in the digital scholarly communication was likely accompanying a digital publication (e.g. PDF file) with supplementary material. In such scenarios, scientists can share packages consisting of publication and supplementary material in an attempt to better deliver hypothesis and results of the research presented in the publication.
{| class="wikitable"
|-
! Enhanced Publication system !! References
|-
| Journals with supplementary material policies || Elsevier Supplementary Data policies,<ref name="elsevierPolicies"/> SAGE Journals: Author Guide to Supplementary Files<ref name="sagePolicies"/>
|-
| Modular Article || Kircz, Joost G. (2002)<ref name="modularArticle"/>
|-
| LORE || Gerber, A., & Hunter, J. (2010)<ref name="LORE"/>
|}

== Web 2.0 reading capabilities  ==

This category of approaches focuses on enhanced publication data models whose parts, metadata and relationships are defined with the purpose of improving the end-user experience when visualizing and discovering research materials. These approaches are the natural extension of the traditional publication, oriented to reading, and typically integrate all tools made available by the [[World Wide Web|web]] infrastructure and its data sources.<ref name="Jankowski2013"/> 
Specifically, they explore the possibilities of: 
# structuring narrative text into interconnected sub-parts;
# re-using the universe of web resources to enrich the text;
# including dynamic forms of content within the text.

{| class="wikitable"
|-
! Enhanced Publication system !! References
|-
| D4Science Live documents || Candela, L., Castelli, D., Pagano P., & Simi, M. (2005)<ref name="Candela2005"/>
|-
| SciVee || Fink & Bourne (2007)<ref name="Fink2007"/>
|-
| PLOS Neglected Tropical Diseases || Shotton, Portwin, Klyne, & Miles (2009)<ref name="Shotton2009"/>
|-
| The Veteran Tapes Project || van den Heuvel, van Horik, Sanders, Scagliola, & Witkamp  (2010)<ref name="veteran2010"/>
|-
| Utopia Documents || Attwood, T. K., Kell, D. B., McDermott, P., Marsh, J., Pettifer, S. R., & Thorne, D.  (2010)<ref name="utopia2010"/>
|-
| Rich Internet Publications || Breure, Voorbij, & Hoogerwerf (2011)<ref name="RIPs"/>
|-
| SOLE || Pham, Malik, Foster, Di Lauro, & Montella (2012)<ref name="SOLE"/>
|-
| Elsevier's Article of the Future || Aalbersberg, I. J., Heeman, F., Koers, H., & Zudilova-Seinstra, E. (2012)<ref name="Aalbersberg2012"/>
|-
| Bookshelf || Hoeppner (2013)<ref name="Bookshelf"/>
|}

== Interlinking research outputs ==

Scientific communities, organizations, and funding agencies supports initiatives, standards and best practices for publishing and citing datasets and publications on the [[World Wide Web|web]].<ref name="Reilly2011"/><ref name="Mooney2012"/> Examples are [[Datacite|DataCite]],<ref name="datacite"/> EPIC<ref name="epic"/> and [[Crossref|CrossRef]],<ref name="crossref"/> which establish common best practices to assign metadata information and [[persistent identifier]]s to datasets and publications. Data publishing and citation practices are advocated by research communities that believe that datasets should be discoverable and reusable in order to improve the scientific activity.<ref name="Callaghan2012"/><ref name="Parsons2013"/> On this respect, several enhanced publication information systems were built to offer the possibility to enrich a publication with links to relevant research data, possibly deposited in data repositories or discipline specific databases. The existence of links between literature and data support the discovery of used and produced scientific data, strengthen data citation, facilitate data re-use, and reward the precious work underlying data management procedures. 
{| class="wikitable"
|-
! Enhanced Publication system !! References
|-
| SCOPE || Cheung, Hunter, Lashtabeg, & Drennan (2008)<ref name="SCOPE"/>
|-
| CENS ORE Aggregations || Pepe, Mayernik, Borgman and Van de Sompel (2009) <ref name="Pepe2009"/>
|-
| europePMC || McEntyre et al. (2011)<ref name="ukPMC"/>
|-
| WormBase || Yook et al. (2012)<ref name="wormBase"/>
|-
| DANS Enhanced Publication Project || Farace, Frantzen, Stock, Sesink, & Rabina (2012)<ref name="greyNet"/>
|-
| BioTea || Garcia et al. (2013)<ref name="bioTea"/>
|-
| OpenAIRE multi-disciplinary Enhanced Publications || Hoogerwerf et al. (2013)<ref name="openAIRE_EP"/>
|}

== Re-production and assessment of scientific experiments ==

Scientists should be equipped with the facilities necessary to repeat or reproduce somebody else's experiment.  In enhanced publications with executable parts, narrative parts are accompanied by executable workflows with the purpose of reproducing and repeating experiments.

{| class="wikitable"
|-
! Enhanced Publication system !! References
|-
| Scientific Model Packages || Hunter (2006a),<ref name="Hunter2006a"/> Hunter (2006b),<ref name="Hunter2006b"/> Hunter & Cheung (2007)<ref name="Hunter2007"/>
|-
| [[MyExperiment]] ([[Research Objects]]) || De Roure et al. (2009),;<ref name="DeRoure2009"/>  Bechhofer, Soure, Gamble, Goble, & Buchan (2010)<ref name="Bechhofer2010"/>
|-
| IODA || Siciarek and Wiszniewski (2011)<ref name="IODA"/>
|-
| Paper Mâché  || Brammer, Crosby, Matthews, & Williams (2011)<ref name="Brammer2011"/>
|-
| Collage Authoring Environment  || Nowakowski et al. (2011)<ref name="COLLAGE"/>
|-
| [[SciCrunch]]'s [[RRID]]s || Jeffrey et al. (2014)<ref>{{cite journal|last1=Jeffrey|first1=Grethe|last2=Anita|first2=Bandrowski|last3=Davis|first3=Banks|last4=Christopher|first4=Condit|last5=Amarnath|first5=Gupta|last6=Stephen|first6=Larson|last7=Yueling|first7=Li|last8=Ibrahim|first8=Ozyurt|last9=Andrea|first9=Stagg|last10=Patricia|first10=Whetzel|last11=Luis|first11=Marenco|last12=Perry|first12=Miller|last13=Rixin|first13=Wang|last14=Gordon|first14=Shepherd|last15=Maryann|first15=Martone|title=SciCrunch: A cooperative and collaborative data and resource discovery platform for scientific communities|journal=Frontiers in Neuroinformatics|date=2014|volume=8|doi=10.3389/conf.fninf.2014.18.00069}}</ref>
|-
| SHARE || Van Gorp & Mazanek (2011)<ref name="SHARE"/>
|}

== References ==

{{Reflist|refs=
<ref name="book">{{cite book | last1 = Vernooy-Gerritsen | first1 = Marjan | title = Emerging standards for enhanced publications and repository technology: survey on technology | publisher = Amsterdam University Press | year = 2009}}</ref>
<ref name="force11">{{cite web | url=http://www.force11.org | title=FORCE11 initiative}}</ref>
<ref name="elsevierPolicies">{{cite web | url=http://www.elsevier.com/journals/vaccine/0264-410X/guide-for-authors#87000 | title=Elsevier Supplementary Data policies}}</ref>
<ref name="sagePolicies">{{cite web | url=http://www.uk.sagepub.com/repository/binaries/doc/Supplemental_data_on_sjo_guidelines_for_authors.doc | title=SAGE Journals: Author Guide to Supplementary Files}}</ref>
<ref name="modularArticle">
{{cite journal
 | last1 = Kircz | first1 = Joost G.
 | title = New practices for electronic publishing 2: New forms of the scientific paper
 | journal = Learned Publishing
 | volume = 15
 | issue = 1
 | pages = 27–32
 | year = 2002
 | publisher = Association of Learned and Professional Society Publishers
 | doi = 10.1087/095315102753303652
 }}
</ref>
<ref name="LORE">
{{cite journal
 | last1 = Gerber | first1 = A.
 | last2 = Hunter | first2 = J.
 | title = Authoring, editing and visualizing compound objects for literary scholarship
 | journal = Journal of digital information
 | volume = 11
 | issue = 1
 | year = 2010
 | issn = 1368-7506
 | url = http://journals.tdl.org/jodi/index.php/jodi/article/view/755
 }}
</ref>
<ref name="Jankowski2013">
{{cite journal
 | last1 = Jankowski | first1 = N. W.
 | last2 = Scharnhorst | first2 = A.
 | last3 = Tatum | first3 = C.
 | last4 = Tatum | first4 = Z.
 | title = Enhancing scholarly publications: Developing hybrid monographs in the humanities and social sciences 
 | journal = Scholarly and Research Communication
 | volume = 4
 | issue = 1
 | year = 2013
 | url = http://src-online.ca/index.php/src/article/viewFile/40/123
}}
</ref>
<ref name="Reilly2011">
{{cite journal
 | last1 = Reilly | first1 = A.
 | last2 = Schallier | first2 = W.
 | last3 = Schrimpf | first3 = S.
 | last4 = Smit | first4 = E.
 | last5 = Wilkinson | first5 = M.
 | title = Report on integration of data and publications 
 | journal = Scholarly and Research Communication
 | volume = 4
 | issue = 1
 | year = 2013
 | url = http://src-online.ca/index.php/src/article/viewFile/40/123
}}
</ref>
<ref name="Mooney2012">
{{Cite journal | doi = 10.7710/2162-3309.1035| title = The Anatomy of a Data Citation: Discovery, Reuse, and Credit| year = 2012| last1 = Mooney | first1 = H. | last2 = Newton | first2 = M. | journal = Journal of Librarianship and Scholarly Communication| volume = 1}}
</ref>
<ref name="datacite">{{cite web | url=http://www.datacite.org | title=DataCite | accessdate=27 January 2014}}</ref>
<ref name="epic">{{cite web | url=http://www.pidconsortium.eu | title=EPIC - European Persistent Identifier Consortium | accessdate=28 January 2014}}</ref>
<ref name="crossref">{{cite web | url=http://www.crossref.org | title=CrossRef | accessdate=27 January 2014}}</ref>
<ref name="Callaghan2012">{{Cite journal | doi = 10.2218/ijdc.v7i1.218| title = Making Data a First Class Scientific Output: Data Citation and Publication by NERC's Environmental Data Centres| year = 2012| last1 = Callaghan | first1 = S. | last2 = Donegan | first2 = S. | last3 = Pepler | first3 = S. | last4 = Thorley | first4 = M. | last5 = Cunningham | first5 = N. | last6 = Kirsch | first6 = P. | last7 = Ault | first7 = L. | last8 = Bell | first8 = P. | last9 = Bowie | first9 = R. | last10 = Leadbetter | first10 = A. | last11 = Lowry | first11 = R. | last12 = Moncoiffé | first12 = G. | last13 = Harrison | first13 = K. | last14 = Smith-Haddon | first14 = B. | last15 = Weatherby | first15 = A. | last16 = Wright | first16 = D. | journal = International Journal of Digital Curation| volume = 7| pages = 107–113}}</ref>
<ref name="Parsons2013">{{Cite journal | doi = 10.2481/dsj.wds-042| title = Is Data Publication the Right Metaphor?| year = 2013| last1 = Parsons | first1 = M. A. | last2 = Fox | first2 = P. A. | journal = Data Science Journal| volume = 12| pages = WDS32}}</ref>
<ref name="Candela2005">{{Cite book | doi = 10.1007/11599517_2| chapter = From Heterogeneous Information Spaces to Virtual Documents| title = Digital Libraries: Implementing Strategies and Sharing Experiences| series = Lecture Notes in Computer Science| year = 2005| last1 = Candela | first1 = L. | last2 = Castelli | first2 = D. | last3 = Pagano | first3 = P. | last4 = Simi | first4 = M. | isbn = 978-3-540-30850-8| volume = 3815| pages = 11}}</ref>
<ref name="Fink2007">{{cite journal 
 | last1 = Fink | first1 =  J. Lynn
 | last2 = Bourne | first2 = Philip E.
 | title = Reinventing scholarly communication for the electronic age
 | journal = CTWatch Quarterly
 | volume = 3
 | issue = 3
 | year = 2007
 | url = http://www.ctwatch.org/quarterly/articles/2007/08/reinventing-scholarly-communication-for-the-electronic-age/
}}</ref> 
<ref name="Shotton2009">{{Cite journal | last1 = Shotton | first1 = D. | last2 = Portwin | first2 = K. | last3 = Klyne | first3 = G. | last4 = Miles | first4 = A. | editor1-last = Bourne | editor1-first = Philip E | title = Adventures in Semantic Publishing: Exemplar Semantic Enhancements of a Research Article | doi = 10.1371/journal.pcbi.1000361 | journal = PLoS Computational Biology | volume = 5 | issue = 4 | pages = e1000361 | year = 2009 | pmid =  19381256| pmc =2663789 }}</ref>
<ref name="veteran2010">{{cite conference  
 | last1 = van den Heuvel | first1 =  H.
 | last2 = van Horik | first2 = R.
 | last3 = Scagliola | first3 =  S. I.
 | last4 = Sanders | first4 = E. P.
 | last5 = Witkamp | first5 = P.
 | url=http://hdl.handle.net/2066/85921 
 | title=The VeteranTapes: Research Corpus, Fragment Processing Tool, and Enhanced Publications for the e-Humanities 
 | booktitle=Proceedings of LREC
 | conference=7th International Conference on Language Resources and Evaluation (LREC)
 | pages=2687–2692
 | year=2010
 | accessdate=27 January 2014
}}</ref>
<ref name="utopia2010">{{Cite journal | last1 = Attwood | first1 = T. K.| authorlink1 = Terri Attwood | last2 = Kell | first2 = D. B. | authorlink2 = Douglas Kell| last3 = McDermott | first3 = P. | last4 = Marsh | first4 = J. | last5 = Pettifer | first5 = S. R. | authorlink5 = Steve Pettifer| last6 = Thorne | first6 = D. | doi = 10.1093/bioinformatics/btq383 | title = Utopia documents: Linking scholarly literature with research data | journal = Bioinformatics | volume = 26 | issue = 18 | pages = i568–i574 | year = 2010 | pmid = 20823323 | pmc =2935404 }}</ref>
<ref name="RIPs">{{cite journal 
 | last1 = Breure | first1 =  L.
 | last2 = Voorbij | first2 = H.
 | last3 = Hoogerwerf | first3 = M.
 | title = Rich Internet Publications: "Show What You Tell"
 | journal = Journal of Digital Information
 | volume = 12
 | issue = 1
 | year = 2011
 | url = http://journals.tdl.org/jodi/index.php/jodi/article/view/1606/1738
}}</ref> 
<ref name="SOLE">{{Cite book | doi = 10.1007/978-3-642-34222-6_16| chapter = SOLE: Linking Research Papers with Science Objects| title = Provenance and Annotation of Data and Processes| series = Lecture Notes in Computer Science| year = 2012| last1 = Pham | first1 = Q. | last2 = Malik | first2 = T. | last3 = Foster | first3 = I. | last4 = Lauro | first4 = R. | last5 = Montella | first5 = R. | isbn = 978-3-642-34221-9| volume = 7525| pages = 203}}</ref>
<ref name="Aalbersberg2012">{{Cite journal | doi = 10.1629/2048-7754.25.1.33| title = Elsevier's Article of the Future enhancing the user experience and integrating data through applications| year = 2012| last1 = Zudilova-Seinstra | first1 = E. | last2 = Koers | first2 = H. | last3 = Heeman | first3 = F. | last4 = Aalbersberg | first4 = I. J. | journal = Insights: the UKSG journal| volume = 25| pages = 33–43}}</ref>
<ref name="Bookshelf">{{Cite journal | doi = 10.1093/nar/gks1279| title = NCBI Bookshelf: Books and documents in life sciences and health care| year = 2012| last1 = Hoeppner | first1 = M. A.| journal = Nucleic Acids Research| volume = 41| pages = D1251| pmid=23203889| pmc=3531209}}</ref>
<ref name="SCOPE">{{Cite journal | doi = 10.2218/ijdc.v3i2.55| title = SCOPE: A Scientific Compound Object Publishing and Editing System| year = 2008| last1 = Cheung | first1 = K. | last2 = Hunter | first2 = J. | last3 = Lashtabeg | first3 = A. | last4 = Drennan | first4 = J. | journal = International Journal of Digital Curation| volume = 3| issue = 2| pages = 4–18}}</ref>
<ref name="Pepe2009">{{cite web 
| last1 = Pepe | first1 =  A.
| last2 = Mayernik | first2 = M.
| last3 = Borgman | first3 = C. L.
| last4 = van de Sompel | first4 = H.
| url=http://works.bepress.com/albertopepe/11 
| title=Technology to Represent Scientific Practice: Data, Life Cycles, and Value Chains 
| year=2009
}}</ref>
<ref name="ukPMC">{{Cite journal | last1 = McEntyre | first1 = J. R. | last2 = Ananiadou | first2 = S. | last3 = Andrews | first3 = S. | last4 = Black | first4 = W. J. | last5 = Boulderstone | first5 = R. | last6 = Buttery | first6 = P. | last7 = Chaplin | first7 = D. | last8 = Chevuru | first8 = S. | last9 = Cobley | first9 = N. | last10 = Coleman | doi = 10.1093/nar/gkq1063 | first10 = L. -A. | last11 = Davey | first11 = P. | last12 = Gupta | first12 = B. | last13 = Haji-Gholam | first13 = L. | last14 = Hawkins | first14 = C. | last15 = Horne | first15 = A. | last16 = Hubbard | first16 = S. J. | last17 = Kim | first17 = J. -H. | last18 = Lewin | first18 = I. | last19 = Lyte | first19 = V. | last20 = MacIntyre | first20 = R. | last21 = Mansoor | first21 = S. | last22 = Mason | first22 = L. | last23 = McNaught | first23 = J. | last24 = Newbold | first24 = E. | last25 = Nobata | first25 = C. | last26 = Ong | first26 = E. | last27 = Pillai | first27 = S. | last28 = Rebholz-Schuhmann | first28 = D. | last29 = Rosie | first29 = H. | last30 = Rowbotham | first30 = R. | title = UKPMC: A full text article resource for the life sciences | journal = Nucleic Acids Research | volume = 39 | issue = Database issue | pages = D58–D65 | year = 2010 | pmid = 21062818 | pmc =3013671  
}}</ref>
<ref name="wormBase">{{Cite journal | doi = 10.1093/nar/gkr954| title = Worm ''Base'' 2012: More genomes, more data, new website| year = 2011| last1 = Yook | first1 = K.| last2 = Harris | first2 = T. W.| last3 = Bieri | first3 = T.| last4 = Cabunoc | first4 = A.| last5 = Chan | first5 = J.| last6 = Chen | first6 = W. J.| last7 = Davis | first7 = P.| last8 = de la Cruz | first8 = N.| last9 = Duong | first9 = A.| last10 = Fang | first10 = R.| last11 = Ganesan | first11 = U.| last12 = Grove | first12 = C.| last13 = Howe | first13 = K.| last14 = Kadam | first14 = S.| last15 = Kishore | first15 = R.| last16 = Lee | first16 = R.| last17 = Li | first17 = Y.| last18 = Muller | first18 = H. -M. | last19 = Nakamura | first19 = C.| last20 = Nash | first20 = B.| last21 = Ozersky | first21 = P.| last22 = Paulini | first22 = M.| last23 = Raciti | first23 = D.| last24 = Rangarajan | first24 = A.| last25 = Schindelman | first25 = G.| last26 = Shi | first26 = X.| last27 = Schwarz | first27 = E. M.| last28 = Ann Tuli | first28 = M.| last29 = Van Auken | first29 = K.| last30 = Wang | first30 = D.| journal = Nucleic Acids Research| volume = 40| pages = D735 | pmid=22067452 | pmc=3245152}}</ref>
<ref name="greyNet">{{cite journal 
| last1 = Farace | first1 =  D. J.
| last2 = Frantzen | first2 = J.
| last3 = Stock | first3 = C.
| last4 = Sesink | first4 = L.
| last5 = Rabina | first5 = D. L.
| url=http://www.opengrey.eu/data/70/01/53/GL13_Farace_et_al_2012_Conference_Preprint.pdf 
| title=Linking full-text grey literature to underlying research and post-publication data: An Enhanced Publications Project 2011-2012 
| journal=The Grey journal 
| year=2012 
| volume=8 
| issue=3 
| pages=181–189
}}</ref>
<ref name="bioTea">{{Cite journal | doi = 10.1186/2041-1480-4-S1-S5| title = Biotea: RDFizing Pub ''Med'' Central in support for the paper as an interface to the Web of Data| year = 2013| last1 = Garcia Castro | first1 = L. | last2 = McLaughlin | first2 = C. | last3 = Garcia | first3 = A. | journal = Journal of Biomedical Semantics| volume = 4| pages = S5}}</ref>
<ref name="openAIRE_EP">{{cite conference 
| last1 = Hoogerwerf | first1 =  M.
| url=http://www.ais.up.ac.za/digi/docs/hoogerwerf_paper.pdf 
| title=Durable enhanced publications 
| booktitle=Proceedings of African Digital Scholarship & Curation 
| year=2009
| accessdate=27 January 2014 
}}</ref>
<ref name="Hunter2006a">{{cite conference 
| last1 = Hunter | first1 =  Jane
| url=http://espace.library.uq.edu.au/view/UQ:7942
| title=Scientific Models: A User-oriented Approach to the Integration of Scientific Data and Digital Libraries 
| conference=VALA2006 
| year=2006
| pages=1–16
| accessdate=27 January 2014 
}}</ref>
<ref name="Hunter2006b">{{Cite journal | doi = 10.2218/ijdc.v1i1.4| title = Scientific Publication Packages – A Selective Approach to the Communication and Archival of Scientific Output| year = 2006| last1 = Hunter | first1 = J. | journal = International Journal of Digital Curation| volume = 1| pages = 33–52}}</ref>
<ref name="Hunter2007">{{Cite journal | doi = 10.1007/s00799-007-0018-5| title = Provenance Explorer-a graphical interface for constructing scientific publication packages from provenance trails| year = 2007| last1 = Hunter | first1 = J. | last2 = Cheung | first2 = K. | journal = International Journal on Digital Libraries| volume = 7| pages = 99–107}}</ref>
<ref name="DeRoure2009">{{Cite journal | last1 = De Roure | first1 = D. | authorlink1 = David De Roure| last2 = Goble | first2 = C. | authorlink2 = Carole Goble| last3 = Stevens | first3 = R.| authorlink3 = Robert David Stevens | doi = 10.1016/j.future.2008.06.010 | title = The design and realisation of the [[myExperiment]] Virtual Research Environment for social sharing of workflows | journal = Future Generation Computer Systems | volume = 25 | issue = 5 | pages = 561–567 | year = 2009 | pmid =  | pmc = }}</ref>
<ref name="Bechhofer2010">{{Cite journal | doi = 10.1038/npre.2010.4626.1| title = Research Objects: Towards Exchange and Reuse of Digital Knowledge| year = 2010| last1 = Bechhofer | first1 = S. | last2 = Bechhofer | first2 = S. | last3 = De Roure | first3 = D. | last4 = Gamble | first4 = M. | last5 = Goble | first5 = C. | last6 = Buchan | first6 = I. | journal = Nature Precedings}}</ref>
<ref name="IODA">{{Cite journal | doi = 10.1016/j.procs.2011.04.070| title = IODA - an Interactive Open Document Architecture| year = 2011| last1 = Siciarek | first1 = J.| last2 = Wiszniewski | first2 = B.| journal = Procedia Computer Science| volume = 4| pages = 668–677}}</ref>
<ref name="Brammer2011">{{Cite journal | doi = 10.1016/j.procs.2011.04.069| title = Paper Mâché: Creating Dynamic Reproducible Science| year = 2011| last1 = Brammer | first1 = G. R. | last2 = Crosby | first2 = R. W. | last3 = Matthews | first3 = S. J. | last4 = Williams | first4 = T. L. | journal = Procedia Computer Science| volume = 4| pages = 658–667}}</ref>
<ref name="COLLAGE">{{Cite journal | doi = 10.1016/j.procs.2011.04.064| title = The Collage Authoring Environment| year = 2011| last1 = Nowakowski | first1 = P. | last2 = Ciepiela | first2 = E. | last3 = Harężlak | first3 = D. | last4 = Kocot | first4 = J. | last5 = Kasztelnik | first5 = M. | last6 = Bartyński | first6 = T. | last7 = Meizner | first7 = J. | last8 = Dyk | first8 = G. | last9 = Malawski | first9 = M. | journal = Procedia Computer Science| volume = 4| pages = 608–617
}}</ref>
<ref name="SHARE">{{Cite journal | doi = 10.1016/j.procs.2011.04.062| title = SHARE: A web portal for creating and sharing executable research papers| year = 2011| last1 = Van Gorp | first1 = P. | last2 = Mazanek | first2 = S. | journal = Procedia Computer Science| volume = 4| pages = 589–597}}</ref>
}}

[[Category:Scholarly communication]]
[[Category:Information systems]]
[[Category:Academic publishing]]
[[Category:Electronic publishing]]