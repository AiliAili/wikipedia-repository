<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Fk9
 | image=EDKR Bergfliegen 2012 1907.jpg
 | caption=B&F FK9
}}{{Infobox Aircraft Type
 | type=Two seat [[ultralight aircraft|ultralight]]
 | national origin=[[Germany]]
 | manufacturer=B&F Technik  Vertriebs gmbh, [[Speyer]]
 | designer=Otto and Peter Funk
 | first flight=early 1989
 | introduced=
 | retired=
 | status=In production
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=370+ by end 2008
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''B&F Fk9''', also marketed as the '''FK-Lightplanes FK9''', is a [[Germany|German]]-designed single-engine, two-seat [[ultralight aircraft|ultralight]], first flown in 1989.  It has been developed from a mixed structure, fabric covered aircraft to a wholly composite machine. It remains in production at factories in Germany and [[Poland]] and has sold in large numbers, flying in four continents.<ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 50. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 52. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
Otto Funk had designed and built gliders and motor glider since 1970 but the Fk9 was both his first commercial product and his first aircraft with design input from his son Peter.  Peter Funk and Dirk Breitkreuz set up B&F Technik in 1990 to produce it.<ref name="FunkHist"/> The early Marks 1 and 2 were wholly constructed in [[Krosno]], [[Poland]]; current models are 85% Polish built, with final assembly in [[Speyer]], [[Germany]].<ref name="JAWA10"/>  The type is quite often referred to as the '''Funk Fk9''', though not by its makers.

The Fk9 is a conventionally laid out high wing, single-engine [[ultralight aircraft|ultralight]], with side-by-side seating and a fixed undercarriage.  The wings have parallel chord and are fitted with ailerons and three-position [[flap (aircraft)|flaps]].  They are braced with a single faired [[strut]] to the lower fuselage on each side.  In the Mark 1 and 2 models, the wings were composite structures with fabric covering; later models have had all-carbon [[Composite material|composite]] wings apart from aluminium control surfaces.  There is now the option of an all-carbon fibre wing.<ref name="JAWA10"/>

The early Fk9 marks also had a steel tube, fabric covered fuselage, but this has been replaced with an all-[[glass fibre]] structure, apart from a steel cabin frame.  Access to the dual control cabin, which has overhead transparencies,<ref name="Rev"/> is via top-hinged doors on each side.  The fin and rudder are swept, mostly on the leading edge; the elevators are [[balanced rudder|horn balanced]].  A [[tricycle undercarriage]] is standard, with (usually) [[aircraft fairing|spatted]] mainwheels on spring cantilever legs mounted on the fuselage at the base of the wing struts plus a spatted, steerable nosewheel.  The mainwheels have brakes operated by a central hand control.<ref name="Rev"/> A [[conventional undercarriage]] is an option, with the mainwheel legs fuselage-mounted further forward, below the cabin doors, plus a carbon-and-steel tailwheel.<ref name="JAWA10"/>

A variety of engines have been fitted.  Early models used a 26&nbsp;kW (35&nbsp;hp) [[Rotax 447]] or [[Jabiru]], or a (37&nbsp;kW) 50&nbsp;hp [[Rotax 503]].<ref name="Simp"/>  The Mark 3 had a 60&nbsp;kW (80&nbsp;hp) [[Rotax 912|Rotax 912 UL]] and the Mark IV offers a choice between this engine, the uprated 73&nbsp;kW (99&nbsp;hp) [[Rotax 912|Rotax&nbsp;912&nbsp;ULS]] or a 60&nbsp;kW (80&nbsp;hp) [[Mercedes M160]], first used in production Fk9s in the '''Smart''' variant.<ref name="JAWA10"/>

The design is an accepted [[Federal Aviation Administration]] special [[light-sport aircraft]].<ref name="FAASLSA">{{cite web|url = https://www.faa.gov/aircraft/gen_av/light_sport/media/SLSA_Directory.xlsx|title = SLSA Make/Model Directory|accessdate = 19 February 2017|last = [[Federal Aviation Administration]]|date = 26 September 2016}}</ref>

==Operational history==
The Fk9 prototype first flew early in 1989, making its first public appearance that April at the Aero trade fair in [[Friedrichshafen]].  By late 2008, before the introduction of the ELA variant, at least 370 Fk9s of all types had been sold.<ref name="JAWA10"/> The mid-2010 European registers (excluding Russia) note 387 aircraft, including four ELAs.<ref name="EuReg"/> Other Fk9s fly in North and South America, where there are agencies, as in South Africa. US LSA for kits was given in 2005.<ref name="JAWA10"/> [[united Kingdom|UK]] microlight weight limits exclude the Fk9 as an assembled aircraft, but the recent ELA regulations are expected to allow that version to be registered.<ref name="Rev"/>

Reviewer Marino Boric described the design in a 2015 review as "an all-composite Cessna 150 lookalike, but with mush improved performance".<ref name="WDLA15"/>

==Variants==
''Information from''<ref name="FunkHist"/><ref name="JAWA10"/><ref name="Simp"/>
*; Fk9 Mark 1:Composite wings, metal tube fuselage.  First production version. Introduced 1991.
*; Fk9 Mark 2: Revised to accept more powerful (45–80&nbsp;hp) engines. [[Conventional gear]].  Introduced 1995.
:::'''Fk9TG''': Mark 2 with tricycle gear.
[[File:FK9 Mk 3 Utility im Queranflug.jpg|thumb|Fk9 Mark 3 Utility fitted for glider towing]]
*; Fk9 Mark 3: First all-composites version; fuselage shape strongly revised, more slender aft. Introduced 1997.
:::'''Mark 38''': Marketed in Brazil as '''Alpha Bravo Fk9'''.  Span 10.33 m.
:::'''Mark 3 Utility/Club''': For trainer or glider towing roles. New wings. Introduced 2000.
*; Fk9 Smart: powered by [[Mercedes M160]] three-cylinder turbocharged engine from [[Smart (automobile)|Smart]] car.  Introduced 2004.<ref name="Smart"/>
[[File:B&F FK 9 MarkIV SW 2.jpg|right|thumb|Fk9 Mark IV SW]] 
*; Fk9 Mark IV: Replaced Mark 3 with larger cabin, flaps (30°) and tailplane. Introduced 2003.
:::'''Mark IV SW''': Short span wing, better downward visibility.
:::'''Mark IV Utility''': For trainer or glider towing roles.  Tricycle or conventional gear options.
*; Fk9 ELA: Mark IV improved to fit European Light Aircraft category.  Introduced 2009.

==Specifications (Mark IV, Rotax 912 UL, tricycle undercarriage)==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2010/11<ref name="JAWA10"/><!-- for giving the reference for the data -->
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=
|capacity=two
|length m=5.85
|length ft=
|length in=
|length note=
|span m=9.85
|span ft=
|span in=
|span note=
|height m=2.15
|height ft=
|height in=
|height note=
|wing area sqm=11.60
|wing area sqft=
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=278
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=544
|max takeoff weight lb=
|max takeoff weight note=or national ultralight limit
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Rotax 912]]UL
|eng1 type=4-cylinder horizontally opposed
|eng1 kw=60<!-- prop engines -->
|eng1 hp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=3<!-- propeller aircraft -->
|prop name=Junkers or Duc carbon fibre, with ground-adjustable pitch
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

<!--
        Performance
-->
|perfhide=
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=122
|cruise speed kts=
|cruise speed note=at 75% power
|stall speed kmh=64<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=230
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=1000
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=+6.01/-3.0<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=5.0
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{reflist|refs=

<ref name="JAWA10">{{cite book |title= Jane's All the World's Aircraft 2010-11|last= Jackson |first= Paul |coauthors= |edition= |year=2010|publisher= IHS Jane's|location=Coulsdon, Surrey|isbn= 978-0-7106-2916-6 |pages=227–8}}</ref>

<ref name="EuReg">{{cite book |title=European registers handbook 2010 |last= Partington |first=Dave |coauthors= |edition= |year=2010|publisher= Air Britain (Historians) Ltd|location= |isbn=978-0-85130-425-0|page=}}</ref>

<ref name="Simp">{{cite book |title= Airlife's World Aircraft|last= Simpson |first= Rod |coauthors= |edition= |year=2001|publisher= Airlife Publishing Ltd|location= Shrewsbury|isbn=1-84037-115-3|pages=248–9}}</ref>

<ref name="FunkHist">{{cite web|url=http://www.fk-lightplanes.com/html/fk_history.html |title=Fk lightplanes - history |author= |date= |work= |publisher= |accessdate=2011-01-08 |deadurl=yes |archiveurl=https://web.archive.org/web/20101125075741/http://www.fk-lightplanes.com/html/fk_history.html |archivedate=2010-11-25 |df= }}</ref>

<ref name="Rev">{{cite web|url=http://www.fk-leichtflugzeuge.de/Downloads/SA_Flyer_Apr07_Fk9.pdf |title=SA Fk9 review |author= |date= |work= |publisher= |accessdate=2011-01-08 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>

<ref name="Smart">{{cite web |url=http://www.fk-lightplanes.com/news.php?ln=2&pg=14&id_g=4 |title=FK9 Smart engine |author= |date= |work= |publisher= |accessdate=}}</ref>

}}
<!-- ==Further reading== -->

==External links==
{{commons category|B&F FK9}}
*{{Official website|http://www.fk-lightplanes.com/}}
<!-- Navboxes go here -->
{{B&F aircraft}}

{{DEFAULTSORT:BandF Fk9}}
[[Category:German sport aircraft 1980–1989]]
[[Category:FK-Lightplanes aircraft]]