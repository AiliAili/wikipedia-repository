{{Infobox military person
| name          =Antonio Amantea
| image         =
| caption       =
| birth_date          = <!-- {{Birth date and age|YYYY|MM|DD}} -->28 September 1894
| death_date          = <!-- {{Death date and age|YYYY|MM|DD|YYYY|MM|DD}} -->13 July 1983
| placeofburial_label = 
| placeofburial = 
| birth_place  =[[Lecce]], [[Italy]]
| death_place  =Lecce, Italy
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| allegiance    =Italy
| branch        =Artillery; aviation
| serviceyears  =1914 - 1918
| rank          =Sottotenente
| unit          =43a Squadriglia, [[71a Squadriglia]]
| commands      =
| battles       =
| awards        =[[Medal for Military Valor]] (1 bronze and 3 silver awards)
| relations     =
| laterwork     =
}}
Sottotenente '''Antonio Amantea''' was a World War I [[flying ace]] credited with five aerial victories. He lived to become the last surviving Italian ace of the war.<ref name="The Aerodrome">[http://www.theaerodrome.com/aces/italy/amantea.php ''The Aerodrome''] Retrieved on 31 May 2010.</ref>

==World War I service==
Amantea was working as an electrician when he was conscripted into Italian military in September 1914. He volunteered for aviation. A year later, on 1 September 1915, ''Sergente'' Amantea pinned on his wings. His first assignment was to fly artillery spotting missions in a [[Caudron G-3]] on the [[Isonzo front]]. He flew 173 combat sorties over the next few months before being picked for fighter training in February 1917. In late March, he returned to action as a member of ''71a Squadriglia'', flying over the [[Asiago plateau]].<ref name=fronts14>Varriale 2009, pp. 14-15.</ref>

On 2 August 1917, he scored his first confirmed victory, although he had to break off his attack because of a bum magneto and sputtering engine and crashland his own plane. He submitted a claim for the 23rd that went unverified. However, he had better luck the next day, sharing a confirmed win over an [[Albatros D.III]] with [[Antonio Riva (pilot)|Antonio Riva]] but not having a solo second victory confirmed.<ref name="The Aerodrome"/> In December 1917, he upgraded to piloting a [[Spad VII]]. By the time he ended his triumphant string on 3 May 1918,<ref name=fronts14/> he had posted nine victory claims<ref name="The Aerodrome"/> to have five confirmed under the stringent rules used by the Italians.<ref name=fronts14/>

==Postwar==
Amantea left Italian aviation with the rank of ''Tenente''. In 1922, he returned. He fought in the [[Second Italo-Abyssinian War|Ethiopian War]], and worked his way up to ''Colonello'' by June 1940, when Italy entered World War II. He was commanding [[Galatina Air Force Base|Galatina Airfield]] when Italy declared its armistice on 8 September 1943. He served three more years, retiring in 1946.<ref name=fronts14/> By the time of his death on 13 July 1983 in [[Lecce]], he was the last surviving Italian ace from World War I.<ref name="The Aerodrome"/>

==Notes==
{{reflist}}

==References==
* [[Norman Franks|Franks, Norman]]; Guest, Russell; Alegi, Gregory.  ''Above the War Fronts: The British Two-seater Bomber Pilot and Observer Aces, the British Two-seater Fighter Observer Aces, and the Belgian, Italian, Austro-Hungarian and Russian Fighter Aces, 1914–1918: Volume 4 of Fighting Airmen of WWI Series: Volume 4 of Air Aces of WWI''. Grub Street, 1997. ISBN 1-898697-56-6, ISBN 978-1-898697-56-5.
* Varriale, Paolo. ''Italian Aces of World War 1''. Osprey Pub Co, 2009. ISBN 978-1-84603-426-8.

{{DEFAULTSORT:Amantea, Antonio}}
[[Category:Recipients of the Bronze Medal of Military Valor]]
[[Category:Recipients of the Silver Medal of Military Valor]]
[[Category:1894 births]]
[[Category:1983 deaths]]
[[Category:Italian World War I flying aces]]