{{good article}}
{{EngvarB|date=July 2014}}
{{Use dmy dates|date=July 2014}}
{{Infobox University Boat Race
| name= 126th Boat Race
| winner = Oxford
| margin = Canvas
| winning_time= 19 minutes 20 seconds
| date= 5 April 1980
| umpire = Alan Burrough
| prevseason= [[The Boat Race 1979|1979]]
| nextseason= [[The Boat Race 1981|1981]]
| overall =68&ndash;57
| reserve_winner =Isis
| women_winner =Oxford
}}
The 126th [[The Boat Race|Boat Race]] took place on 5 April 1980. Held annually, the Boat Race is a [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  It was won by Oxford in the closest finish for a century, despite one of their oarsman collapsing before the end of the race.

In the reserve race, Isis beat [[Goldie (Cambridge University Boat Club)|Goldie]] by five lengths, and in the [[Women's Boat Race]], Oxford were victorious.

==Background==
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 8 April 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  First held in 1829, the race takes place on the {{convert|4.2|mi|km}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 3 June 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref>  The rivalry is a major point of honour between the two universities and followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford-Cambridge Boat Race crew|date=6 April 2014|publisher=CBC News|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=7 April 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Ltd | accessdate = 5 July 2014}}</ref>  Oxford went into the race as reigning champions, having beaten Cambridge by three-and-a-half lengths in the [[The Boat Race 1979|previous year's race]].  However Cambridge held the overall lead, with 68 victories to Oxford's 56.<ref name=dodd348/> The race was sponsored for fourth time by [[Ladbrokes]].<ref name=dodd348/><ref name=prof/> It was the last race to be commentated on BBC Radio by [[John Snagge]].<ref>{{Cite book | url = https://books.google.com/books?id=wAEk9veAhpAC&pg=PA256&lpg=PA256&dq=%22John+Snagge%22+1980&source=bl&ots=iRe1tN9zxP&sig=Dqg8Pxc4I6eSBY7v9GIi5ZwuWPc&hl=en&sa=X&ei=I5yJU-6iH8PTPNDfgYgD&ved=0CDsQ6AEwAzgU#v=onepage&q=%22John%20Snagge%22%201980&f=false | title = The A to Z of British Radio | accessdate = 31 May 2014 | page = 256 | first = Seán |last = Street | publisher = Scarecrow Press| isbn=978-0-8108-6847-2}}</ref>

The first [[Women's Boat Race]] took place in 1927, but did not become an annual fixture until the 1960s. Until 2014, the contest was conducted as part of the [[Henley Boat Races]], but as of the [[The Boat Races 2015|2015 race]], it is held on the River Thames, on the same day as the men's main and reserve races.<ref>{{Cite web | url = http://theboatraces.org/women/history | archiveurl = https://web.archive.org/web/20141006112628/http://theboatrace.org/women/history| archivedate= 6 October 2014| title = A brief history of the Women's Boat Race | publisher = The Boat Race Company Limited| accessdate = 5 July 2014}}</ref>  The reserve race, contested between Oxford's Isis boat and Cambridge's Goldie boat has been held since 1965.  It usually takes place on the Tideway, prior to the main Boat Race.<ref name=results/>

Cambridge coach Graeme Hall was stroke for the Light Blues in the [[The Boat Race 1968|1968 race]], racing against Oxford coach [[Daniel Topolski]] who was bowman for the Dark Blues that year.<ref name=match>{{Cite news | title = Tideway matchmakers | work = [[The Guardian]] | date = 5 April 1980|first = Christopher | last = Dodd | page = 22}}</ref>  Cambridge Boat Club president John Woodhouse noted: "We have cut down the number of formal dinners with old Blues and old coaches, and we have tried to keep the evenings quiet."<ref name=match/>

==Crews==
[[File:Hugh Laurie Actors Guild.jpg|right|upright|thumb|[[Hugh Laurie]] rowed for Cambridge in 1980.]]
The Oxford crew featured three [[Blue (university sport)|Old Blues]], [[Chris Mahoney (rower)|Chris Mahoney]], Mike Diserens, and [[Boris Rankov]], who was rowing in his third University Boat Race.<ref>{{Cite web | url = http://www.telegraph.co.uk/sport/olympics/rowing/5062749/60-Second-Interview-Boris-Rankov-Boat-Race-Umpire.html | work = [[The Daily Telegraph]] | title = 60 Second Interview: Boris Rankov, Boat Race Umpire | first = Gareth A. | last = Davies | accessdate = 31 May 2014 | date = 27 March 2009}}</ref> In J.S. Palmer and A.G. Phillips, Cambridge's crew contained two Blues returning from the previous year's race.<ref name=dodd348/> [[Hugh Laurie]] was following the tradition of his father, former Light Blue president and Olympic gold medallist [[Ran Laurie|Ran]], who had won the Boat Race in [[The Boat Race 1934|1934]], [[The Boat Race 1935|1935]] and [[The Boat Race 1936|1936]].<ref>Dodd pp. 328&ndash;29</ref>

{| class="wikitable"<!-- floatright-->
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]]|| S.R.W. Francis || [[Corpus Christi College, Oxford|Corpus Christi]] || 13 st 12 lb || L.W.J. Baart || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 13 st 4 lb
|-
| 2 || N.A. Conington || [[Oriel College, Oxford|Oriel]] || 13 st 0 lb || M.F. Panter || [[Lady Margaret Boat Club]] || 14 st 1 lb
|-
| 3 || [[Mark Andrews (rower)|M.D. Andrews]] || [[Magdalen College, Oxford|Magdalen]] || 14 st 0.5 lb || T.W. Whitney || [[Jesus College, Cambridge|Jesus]] || 13 st 7 lb
|-
| 4 || J.L. Bland || [[Merton College, Oxford|Merton]] || 13 st 11 lb || [[Hugh Laurie|J.H.C. Laurie]] || [[Selwyn College, Cambridge|Selwyn]] || 13 st 12 lb
|-
| 5 || [[Boris Rankov|N.B. Rankov]] (P) || [[Corpus Christi College, Oxford|Corpus Christi]] || 14 st 3 lb || A.G. Phillips || [[Jesus College, Cambridge|Jesus]] || 13 st 5.5 lb
|-
| 6 || [[Chris Mahoney (rower)|C.J. Mahoney]] || [[Oriel College, Oxford|Oriel]] || 13 st 6 lb || J.W. Woodhouse (P) || [[Selwyn College, Cambridge|Selwyn]] || 13 st 9 lb
|-
| 7 || T.C.M Barry || [[Oriel College, Oxford|Oriel]] || 13 st 4.5 lb || J.S. Palmer || [[Pembroke College, Cambridge|Pembroke]] || 14 st 8 lb
|-
| [[Stroke (rowing)|Stroke]] || M.J. Diserens|| [[Keble College, Oxford|Keble]] || 12 st 13 lb || A.D. Dalrymple || [[Downing College, Cambridge|Downing]] || 12 st 8 lb
|-
| [[Coxswain (rowing)|Cox]] || 	J.S Mead || [[St Edmund Hall, Oxford|St Edmund Hall]] || 8 st 3.5 lb || C.J. Wigglesworth || [[Jesus College, Cambridge|Jesus]] || 7 st 13.5 lb
|-
!colspan="7"| Source:<ref name=dodd348/><br>(P) &ndash; Boat club president
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]] along which the Boat Race is contested]]
Oxford won the toss and elected to start on the Surrey station.<ref name=dodd348>Dodd, p. 348</ref> The race started at 4.45&nbsp;pm.  Following a clash soon after the start, and another at [[Harrods]], the crews were level.  Despite Oxford's number seven, Thomas Barry, losing part of his blade, his crew established a lead and defended it against a higher rating Cambridge.  Umpire Alan Burrough allowed the race to continue, despite the clash and close steering.<ref>{{Cite news | title = Magnificent seven beat brave eight| first = Jim | last = Railton | work = [[The Times]] | page = 11 | issue = 60594 | date = 7 April 1980}}</ref> By [[Barnes Railway Bridge|Barnes Bridge]], Oxford's bowman, Steve Francis, was exhausted and "had more or less stopped rowing".<ref name=dodd242>Dodd, p. 242</ref> Cambridge's push continued but Oxford passed the finishing post with a [[Canvas (rowing)|canvas']] advantage, the closest finish of the century.<ref name=feeble>{{Cite web | url = http://www.henleystandard.co.uk/news/news.php?id=1129777 | work = [[Henley Standard]] | title = Actor's 'feeble' bid to follow in father's shoes | date = 2 July 2012 | accessdate = 7 June 2014}}</ref> Francis was treated for exhaustion,<ref name=dodd242/> but was later diagnosed with hepatitis.<ref name=prof>{{Cite web | url = http://theboatraces.org/report/professionalism-arrives | publisher = The Boat Race Company Limited | title = Professionalism arrives | accessdate = 31 May 2014}}</ref>

In the reserve race, Isis beat Goldie by five lengths, their first victory in four years.<ref name=results>{{Cite web | url = http://theboatraces.org/results | title =Men &ndash; Results | publisher = The Boat Race Company Limited | accessdate = 31 May 2014}}</ref> In the [[Women's Boat Race 1980|35th running]] of the [[Women's Boat Race]], Oxford triumphed, only their second victory in twenty years.<ref name=results/>

==Reaction==
It was the first time Oxford had won five consecutive races since 1913 and was the "closest finish since at least the turn of the century."<ref name=battle>{{Cite news | title = Oxford battle to fifth win | first = Roger | last = Jennings| work = [[The Observer]]| date = 6 April 1980 | page = 1}}</ref>  Laurie recalls his attempts to emulate his father's rowing career as "feeble".<ref name=feeble/> Rankov, rowing in the third of his six Boat Races, recalls the race as one of his most memorable.<ref>{{Cite web | url = http://www.telegraph.co.uk/sport/olympics/rowing/5062749/60-Second-Interview-Boris-Rankov-Boat-Race-Umpire.html | work = [[The Daily Telegraph]] | title = 60 Second Interview: Boris Rankov, Boat Race Umpire | first = Gareth A. | last = Davies | accessdate = 7 June 2014 | date = 27 March 2009}}</ref>  He continued: "It was unbelievable.  They kept coming at us and every time we tried to get away nothing happened."<ref name=canv/>  Woodhouse lamented: "All I can say is that we shouldn't have lost by so much."<ref name=canv>{{Cite news | title = On the canvas&nbsp;... | work = [[The Guardian]] | first = Christopher |last = Dodd | date = 7 April 1980 | page = 14}}</ref>

BBC reporter Snagge, commentating on his last Boat Race, remarked: "An absolutely cracking race&nbsp;... a grand battle."<ref>{{Cite news | title = Oxford bags it again | work = [[The Observer]] | page = 31| date = 6 April 1980}}</ref>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1980}}
[[Category:The Boat Race]]
[[Category:1980 in English sport]]
[[Category:1980 in rowing]]
[[Category:April 1980 sports events]]