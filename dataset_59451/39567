{{Infobox journal
| cover = [[File:IJSEM Sept 2007.gif]]
| discipline = [[Microbiology]], [[systematics]], [[evolutionary biology]]
| formernames = International Bulletin of Bacteriological Nomenclature and Taxonomy, International Journal of Systematic Bacteriology
| abbreviation = Int. J. Syst. Evol. Microbiol.
| editor = [[Aharon Oren]]
| publisher = [[Society for General Microbiology]]
| country =
| frequency = Monthly
| history =1951–present
| impact = 2.798
| impact-year = 2013
| openaccess = [[Delayed open access|Delayed]], after 24 months
| website = http://ijs.sgmjournals.org/
| link1 = http://ijs.sgmjournals.org/content/current
| link1-name = Online access
| link2 = http://ijs.sgmjournals.org/content/by/year
| link2-name = Online archive
| ISSN = 1466-5026
| eISSN = 1466-5034
| OCLC = 807119723
| LCCN = 00252051
| CODEN = ISEMF5
}}
The '''''International Journal of Systematic and Evolutionary Microbiology''''' is a [[Peer review|peer-reviewed]] [[scientific journal]] covering research in the field of [[microbiology|microbial]] [[systematics]] that was established in 1951. Its scope covers the [[Taxonomy (biology)|taxonomy]], [[nomenclature]], identification, characterisation, culture preservation, [[phylogeny]], [[evolution]], and [[biodiversity]] of all [[microorganism]]s, including [[prokaryote]]s, [[yeast]]s and yeast-like organisms, [[protozoa]] and [[algae]]. The journal is currently published monthly by the [[Society for General Microbiology]].

An official publication of the [[International Committee on Systematics of Prokaryotes]] (ICSP)<ref>[http://www.the-icsp.org/ International Committee on Systematics of Prokaryotes] (accessed 26 September 2007)</ref> and of the [[International Union of Microbiological Societies]] (Bacteriology and Applied Microbiology Division),<ref>[http://iums.org/Publications/index.html International Union of Microbiological Societies: Publications] (accessed 8 August 2011)</ref> the journal is the single official international forum for the publication of new species names for prokaryotes.<ref name="Microbe_2007">[http://d.umn.edu/~rhicks/rhicks1/diversity/Stackebrant%202007%20Microbe%202.pdf Stackebrandt E. (2007) Forces shaping bacterial systematics ''Microbe'' 2: 283–288] (accessed 27 September 2007)</ref><ref name="About_IJSEM">[http://ijs.sgmjournals.org/misc/about.shtml SGM: About ''International Journal of Systematic and Evolutionary Microbiology''] (accessed 26 September 2007)</ref><ref name="ICSP_role">[http://www.the-icsp.org/misc/ICSP_intro.htm Trüper HG, Tindall BJ. The Role of the ICSP (International Committee on Systematics of Prokaryotes) in the Nomenclature and Taxonomy of Prokaryotes (28 November 2005)] (accessed 27 September 2007)</ref> In addition to research papers, the journal also publishes the minutes of meetings of the ICSP and its various subcommittees.<ref name="About_IJSEM" />

== Background and history ==
From the first identification of a bacterial species in 1872, microbial species were named according to the [[binomial nomenclature]], based on largely subjective descriptive characteristics.<ref name="Microbe_2007" /> By the end of the 19th century, however, it was clear that this nomenclature and classification system required reform. Although several different comprehensive nomenclature systems were invented (most notably, that described in ''[[Bergey's Manual of Determinative Bacteriology]]'', first published in 1923), none gained international recognition. In 1930, a single international body, now named the International Committee on Systematics of Prokaryotes (ICSP), was established to oversee all aspects of prokaryotic nomenclature. Work began in 1936 on drafting a ''Code of Bacteriological Nomenclature'', the first version of which was approved in 1947.<ref name="Microbe_2007" />

In 1950, at the 5th International Congress for Microbiology, a journal was established to disseminate the committee's conclusions to the microbiological community. It first appeared the following year under the title of ''International Bulletin of Bacteriological Nomenclature and Taxonomy''.<ref name="Microbe_2007" /> In 1980, the ICSP published an exhaustive list of all existing bacterial species considered valid in the ''Approved Lists of Bacterial Names''.<ref name="Microbe_2007" /> Thereafter, the committee's ''Code'' required all new names to be either published or indexed in its journal to be deemed valid.<ref name="ASM_News_2004">[http://forms.asm.org/microbe/index.asp?bid=27946 Euzéby JP, Tindall BJ. Valid publication of new names or new combinations: making use of the validation lists ''ASM News'' (June 2004)] (accessed 8 August 2011)</ref>

The journal was at first published quarterly by [[Iowa State College Press]],<ref name="Microbe_2007" /> which later increased to bimonthly. In 1966, the journal was renamed the ''International Journal of Systematic Bacteriology''.<ref name="Microbe_2007" /><ref name="SGM_history">[http://www.socgenmicrobiol.org.uk/about/history.cfm SGM: About SGM: History: A Short History of the SGM] (accessed 26 September 2007)</ref> The cover at one point bore a quotation from Mueller{{clarify|date=April 2013}}: "the sure and definite determination (of species of bacteria) requires so much time, so much acumen of eye and judgement, so much of perseverance and patience that there is hardly anything else so difficult."<ref>[http://www.microbemagazine.org/images/stories/arch2007/sept07/znw00907000415.pdf Lau PCK. (2007) A Pandora’s can of worms ''Microbe'' 2: 415] (accessed 15 July 2013)</ref> Between 1971 and the end of 1997, the journal was published by the [[American Society for Microbiology]].<ref name="Microbe_2007" /><ref name="SGM_history" /><ref>[http://forms.asm.org/Membership/index.asp?bid=24762 American Society for Microbiology: Timeline of the Society] (accessed 8 August 2011)</ref>

Publication moved to the United Kingdom in 1998, the journal being taken over by the [[Society for General Microbiology]], in conjunction with [[Cambridge University Press]].<ref name="SGM_history" /> The title was changed to ''International Journal of Systematic and Evolutionary Microbiology'' in 2000, to reflect the broadened focus of the journal. A major redesign brought the journal into line with the three other society journals in 2003, and at the same date the printer/typesetter changed to the Charlesworth Group. The frequency increased to monthly in 2006.<ref name="SGM_history" />

== Role in nomenclature validation ==
The journal publishes research papers establishing novel prokaryotic names, which are summarized in a notification list. Each monthly issue also contains a compilation of validated new names (the ''validation list'') that have been previously published in other scientific journals or books.<ref name="Microbe_2007" /><ref name="About_IJSEM" /><ref name="ASM_News_2004" /> Since August 2002, publications relating to new bacterial taxa and validation of publication elsewhere have both required [[Biological type|type strains]] to have been deposited at two recognised public collections in different countries.<ref name="ASM_News_2004" /><ref>[http://ijs.sgmjournals.org/misc/ifora.shtml ''International Journal of Systematic and Evolutionary Microbiology'': Instructions for authors] (accessed 27 September 2007)</ref>

As of 2007, the journal has officially validated around 6500 species and 1500 genera.<ref name="Microbe_2007" /> It was estimated in 2004 that over 300 new names had been published but not validated.<ref name="ASM_News_2004" />

== Modern journal ==
As of 2012, the [[editor-in-chief]] is [[Aharon Oren]] ([[Hebrew University of Jerusalem]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 2.112.<ref name=WoS>{{cite book |year=2011 |chapter=International Journal of Systematic and Evolutionary Microbiology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}} {{subscription required}}</ref>

== Notable papers ==
{{says who|date=June 2013}}
* {{cite journal |doi=10.1099/ijs.0.038075-0 |title=Introducing EzTaxon-e: A prokaryotic 16S rRNA gene sequence database with phylotypes that represent uncultured species |year=2011 |last1=Kim |first1=O.-S. |last2=Cho |first2=Y.-J. |last3=Lee |first3=K. |last4=Yoon |first4=S.-H. |last5=Kim |first5=M. |last6=Na |first6=H. |last7=Park |first7=S.-C. |last8=Jeon |first8=Y. S. |last9=Lee |first9=J.-H. |last10=Yi |first10=H. |last11=Won |first11=S. |last12=Chun |first12=J. |journal=International Journal of Systematic and Evolutionary Microbiology |volume=62 |pages=716–21 |pmid=22140171 |issue=Pt 3|display-authors=8 }}
* {{cite journal |doi=10.1099/ijs.0.034926-0 |title=''Dehalococcoides mccartyi'' gen. nov., sp. nov., obligately organohalide-respiring anaerobic bacteria relevant to halogen cycling and bioremediation, belong to a novel bacterial class, Dehalococcoidia classis nov., order Dehalococcoidales ord. Nov. And family Dehalococcoidaceae fam. Nov., within the phylum Chloroflexi |year=2012 |last1=Loffler |first1=F. E. |last2=Yan |first2=J. |last3=Ritalahti |first3=K. M. |last4=Adrian |first4=L. |last5=Edwards |first5=E. A. |last6=Konstantinidis |first6=K. T. |last7=Muller |first7=J. A. |last8=Fullerton |first8=H. |last9=Zinder |first9=S. H. |last10=Spormann |first10=A. M. |journal=International Journal of Systematic and Evolutionary Microbiology |volume=63 |pages=625–35 |pmid=22544797 |issue=Pt 2|display-authors=8 }}
* {{cite journal |doi=10.1099/00207713-27-1-44 |title=Comparative Cataloging of 16S Ribosomal Ribonucleic Acid: Molecular Approach to Procaryotic Systematics |year=1977 |last1=Fox |first1=G. E. |last2=Pechman |first2=K. R. |last3=Woese |first3=C. R. |journal=International Journal of Systematic Bacteriology |volume=27 |pages=44}}

== References ==
{{reflist|30em}}

==External links==
* {{Official website|http://ijs.sgmjournals.org/}}
{{Use dmy dates|date=August 2011}}


[[Category:Microbiology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1951]]
[[Category:Monthly journals]]
[[Category:English-language journals]]
[[Category:Academic journals published by learned and professional societies]]
[[Category:Prokaryotes taxonomy]]