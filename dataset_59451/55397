{{otheruses|Alcheringa (disambiguation)}}

{{Infobox journal
| title = Alcheringa: An Australasian Journal of Palaeontology
| cover =
| editor = Dr Stephen McLoughlin
| discipline = [[Palaeontology]]
| abbreviation = Alcheringa
| publisher = [[Taylor & Francis]]
| country = UK
| frequency = Quarterly
| history = 1975-present
| openaccess =
| license =
| impact = 1.578!
| impact-year = 2010
| website = http://www.tandf.co.uk/journals/titles/03115518.asp
| link1 = 
| link1-name =
| link2 =
| link2-name =
| JSTOR =
| OCLC = 
| LCCN =
| CODEN =
| ISSN = 0311-5518
| eISSN = 1752-0754
}}

'''''Alcheringa: An Australasian Journal of Palaeontology''''' is the official journal of the [[Association of Australasian Palaeontologists]]. The journal is international in scope and publishes articles on all aspects of [[palaeontology]] and its ramifications into the earth and biological sciences, including: [[Taxonomy (biology)|taxonomy]], [[biostratigraphy]], [[micropalaeontology]], [[vertebrate palaeontology]], [[palaeobotany]], [[palynology]], [[palaeobiology]], [[palaeoanatomy]], [[palaeoecology]], [[biostratinomy]], [[biogeography]], [[chronobiology]], [[biogeochemistry]] and [[palichnology]]. The journal was established in 1975 and is currently published as four issues per year. Alcheringa is derived from the [[Arrernte language]] of the [[Arrernte people|Arrernte]] aboriginal people of the [[Alice Springs, Northern Territory|Alice Springs]] area of [[central Australia]]. Alcheringa (also spelt ''altjeringa'') is the popularized English version of an Arunta expression that means 'in the beginning' or 'from all eternity'.<ref> Strehlow 1971, p. 614</ref> ''Alcheringa'' is also the name given to a 2.7-2.8 billion year old [[stromatolite]] from the [[Pilbara]] region of [[Western Australia]],<ref>Walter 1972, p. 123)</ref> and symbolizes the antiquity of life and its record in sedimentary rocks. An image of the stromatolite is illustrated on the cover of the journal.
==Notes==
{{Reflist}}
==References==

*Strehlow, T.G.H., 1971. Songs of central Australia. Angus & Robertson, Sydney, liv + 755 pp.
*Walter, M.R., 1972. Stromatolites and the biostratigraphy of the Australian Precambrian and Cambrian. Special Papers in Palaeontology 11, i-x + 190 pp., 33 pl.

[[Category:Science and technology in Australia]]
[[Category:Paleontology journals]]