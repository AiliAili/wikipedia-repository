{{Coord|49|21|50.9|N|8|41|3.8|E|display=title}}

{{Infobox journal
| title = Economics of Governance
| discipline = [[Economics]]
| abbreviation = Econ. Gov.
| editor = A. Glazer, M. Polborn
| publisher = [[Springer Science+Business Media]]
| frequency = Quarterly
| history = 2000-present
| impact = 0.364
| impact-year = 2013
| website = http://www.springer.com/economics/journal/10101
| link2 = http://link.springer.com/journal/volumesAndIssues/10101
| link2-name = Online archive
| ISSN = 1435-6104
| eISSN = 1435-8131
| OCLC = 43769645
}}
'''''Economics of Governance''''' is a [[peer-reviewed]] [[academic journal]] of [[economics]] published by [[Springer Science+Business Media]] covering [[governance]] in a large variety
of organizations such as [[government]]s, [[corporation]]s, and [[non-profit association]]s. The [[editors-in-chief]] are Amihai Glazer ([[University of California, Irvine]]) and Mattias Polborn ([[University of Illinois at Urbana–Champaign]]). The journal has been ranked fourth out of 31 journals in the field of [[public economics]],<ref>{{cite journal |doi=10.1111/j.1467-9779.2008.00351.x |title=Ranking Journals Following a Matching Model Approach: An Application to Public Economics Journals |journal=[[Journal of Public Economic Theory]] |volume=10|pages=55–76 |year=2008 |last1=Pujol |first1=Francesc}}</ref> and 125th out of 1017 journals in economics by impact by [[RePEc]].<ref>{{cite web |url=http://ideas.repec.org/top/top.journals.simple.html |title=RePEc simple impact factor ranking of Economics journals |publisher=RePEc |accessdate=2012-08-05}}</ref> According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 0.364.<ref name=WoS>{{cite book |year=2014 |chapter=Economics of Governance |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.springer.com/economics/journal/10101}}

[[Category:Economics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2000]]
[[Category:English-language journals]]


{{econ-journal-stub}}