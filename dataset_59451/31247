{{Other uses|Go Man Go (disambiguation)}}
{{Infobox named horse
| horsename = Go Man Go
| image = Gomangofairuse.jpg
| alt = Horse at full gallop racing along a racetrack with a rider bent over the horse's neck.
| caption = Go Man Go being exercised by jockey Robert Strauss, Los Alamitos Racetrack, about 1956<ref name=Chamberlain/>
| breed = [[American Quarter Horse|Quarter Horse]]
| discipline = [[Horse racing|Racing]]
| sire = [[Top Deck (horse)|Top Deck (TB)]]
| grandsire = Equestrian (TB)
| dam = Lightfoot Sis
| damsire = Very Wise (TB)
| sex = [[Stallion]]
| foaled = 1953
| country = United States
| color = [[Roan (horse)|Roan]]
| breeder = J. B. Ferguson
| racerecord = 47-27-9-3, AAAT [[Speed index|speed rating]]<ref name=Digest/>
| raceearnings = $86,151.00 (approximately ${{formatnum:{{inflation|US|86151|1957|r=-2}} }} as of {{CURRENTYEAR}})<ref name=Digest/>
| racewins = PCQHRA Futurity, Autumn Championship (twice), Wonder Lad Stakes (twice), Clabbertown G stakes (three times);<ref name=Moments130>Nye ''Great Moments'' pp. 130–133</ref><ref name=Pitzer/> Winner Take All Stakes; Barbara B Handicap; Champion Stakes; Ruidoso Derby; State Fair Stallion Stakes; Gold Bar Stakes; New Mexico State Fair<ref name=SireRecord>''AQHA Official Get of Sire Summary Record for Go Man Go''</ref>
| raceawards= 1955 World Champion Quarter Running Horse; 1956 World Champion Quarter Running Horse; 1957 World Champion Quarter Running Horse;<ref name=Digest/> Superior Race Horse; 1957 High Money Earning Race Horse; 1956 High Money Earning Horse<ref name=SireRecord/>
| honors = [[American Quarter Horse Hall of Fame|AQHA Hall of Fame]]<ref name=AQHAHoF/>
|updated= April 29, 2009
}}

'''Go Man Go''' (1953–1983) was an [[American Quarter Horse]] [[stallion]] and [[Horse racing|race horse]]. He was named World Champion Quarter Running Horse three times in a row, one of only two horses to achieve that distinction. Go Man Go was considered to be of difficult temperament. While waiting in the [[starting gate]] for his very first race, he threw his jockey, broke down the gate, and ran alone around the track; he was eventually caught and went on to win the race. During his five years of competition until his retirement from racing in 1960 he had 27&nbsp;wins, earning more than $86,000 (approximately ${{formatnum:{{inflation|US|86151|1957|r=-3}}}} as of {{CURRENTYEAR}}).

Neither of Go Man Go's parents raced. His sire (father), the [[Thoroughbred]] stallion [[Top Deck (horse)|Top Deck]], was bred by the [[King Ranch]]. His [[mare|dam]] (mother) hailed from Louisiana; Go Man Go is thought to have gained his swiftness on the track from her. For the first years of Go Man Go's racing career, his owner faced difficulty in registering him with the [[American Quarter Horse Association]] (AQHA), a matter that remained unresolved until 1958.

Go Man Go went on to sire two [[All American Futurity]] winners and seven Champion Quarter Running Horses. He was inducted into the [[American Quarter Horse Hall of Fame]], as were two of his offspring. His daughters also produced, or were the mothers of, a number of race winners, including the Hall of Fame members [[Kaweah Bar]] and [[Rocket Wrangler]]. The director of racing for the AQHA once compared his impact on Quarter Horse racing and breeding to that of [[Man o' War]] in [[Thoroughbred horse race|Thoroughbred racing]], or that of human athletes such as [[Ben Hogan]] and [[Babe Ruth]].

==Background and early life==
Go Man Go was [[foal]]ed in [[Wharton, Texas]] in 1953, as a result of the second breeding between the Thoroughbred stallion Top Deck and the [[American Quarter Horse Association#Registration|Appendix Quarter Horse]] [[Mare (horse)|mare]] Lightfoot Sis.<ref name=Legends/> Top Deck was bred by the King Ranch, and was unraced.<ref name=Wiggins79>Wiggins ''Great American Speedhorse'' p. 79</ref> J. B. Ferguson had purchased Lightfoot Sis when her then-owner, Octave Fontenot of Prairie Ronde, Louisiana, decided to get out of the [[horse breeding]] business.<ref name=Cajun55/> Ferguson paid $350 for her (approximately ${{formatnum:{{inflation|US|350|1952|r=-2}} }} as of {{CURRENTYEAR}}) and bred her in 1952 to Top Deck (TB), resulting in Go Man Go's birth the next year.<ref name=Cajun55/>{{efn|Other sources give a sale price of $300 for Lightfoot Sis.<ref>Close, et al. ''Legends'' p. 121</ref>}}{{efn|A horse's pregnancy lasts 11 months and two weeks.}} Ferguson also purchased Top Deck, after the stallion injured himself as a yearling.<ref name=Legends117>Close, et al. ''Legends'' pp. 117–119</ref>

Lightfoot Sis showed classic short speed in her pedigree,<ref name=Legends>Close, et al. ''Legends: Outstanding Quarter Horse Stallions and Mares'' p. 121</ref> although she was unraced due to an injury as a [[filly]] that left her blind in one eye.<ref name=Cajun55>LeBlanc ''Cajun-Bred Running Horses'' pp. 55–57</ref> Her sire was the Thoroughbred stallion Very Wise, and her dam was a Quarter Horse mare named Clear Track.<ref name=Legends/>{{efn|Quarter Horses are closely related to Thoroughbreds, and many Quarter Horse race horses are descended mostly from Thoroughbred bloodlines. The [[American Quarter Horse Association|AQHA]] allows the registration of the offspring of a Thoroughbred and a Quarter Horse, but they are registered into a special section of the AQHA's [[stud book]], the Appendix Section. In order to qualify for full registration in the main section of the stud book, the horse must prove itself in competition, either racing or in the [[horse show|show ring]]. Once it does so, the horse is considered a full-blooded Quarter Horse. It is possible, and often happens, that an Appendix-registered horse will qualify for a regular registration number, then be bred back to a Thoroughbred, starting the cycle once more. This explains how horses such as [[Easy Jet (horse)|Easy Jet]] or Go Man Go can have many more Thoroughbred ancestors than Quarter Horses but still be considered Quarter Horses.<ref>Goodhue "A History of Early AQHA Registration" ''Legends'' pp. 4–10, Price ''American Quarter Horse'' pp. 67–68</ref>}}

Scott Wells, a racing correspondent, wrote in ''The Speedhorse Magazine'' that Go Man Go "grew up lean and hard-boned, long-bodied and long-hipped, but not the best looking horse in the world. Not the best looking, just the best."<ref name=QWiggins79>quoted in Wiggins ''Great American Speedhorse'' p. 79</ref> Go Man Go had a reputation for being difficult to handle.<ref name=Chamberlain/> His trainer once told Walt Wiggins, Sr. that Go Man Go was "jes plain mean as a bear most of the time".<ref name=QChamberlain>Quoted in Chamberlain "April 15" ''Quarter Horse Journal''</ref> Throughout his racing career, Go Man Go stayed mean. One of his jockeys, Robert Strauss, recalled later that Go Man Go "was ornery from the day I met him, but he was the greatest horse I ever rode".<ref name=QLegends122>quoted in Close, et al. ''Legends'' p. 122</ref>

==Racing career==
In his five-year racing career, Go Man Go competed in 47&nbsp;races.<ref name=Digest/> He appeared to take naturally to racing; during his training he ran off with his rider—his eventual jockey Robert Strauss—before he was supposed to run.<ref name=Chamberlain/> As Strauss said, "When we were breaking him, he ran off with me before we ever wanted him to run. I mean, just flat ran off with me."<ref name=QChamberlain10>Quoted in Chamberlain "April 15" ''Quarter Racing Journal'' p. 10</ref> Robert's brother Eldridge, who was the trainer, once worked the colt minus half a shoe and Go Man Go still managed a time of 18.9&nbsp;seconds for a {{convert|350|yd|m|adj=on}} distance.<ref name=Chamberlain/>

In the moments before his very first race began, Go Man Go flipped over in the starting gate, unseated his rider, crashed through the front, and ran around the whole track.  He finally allowed himself to be caught and reloaded into the starting gate and went on to win that race. He won his next five races with a total lead of nine&nbsp;[[Length (horse racing)|horse-lengths]].<ref name=Chamberlain>Chamberlain "April 15" ''Quarter Racing Journal''</ref> He faced Vandy's Flash, himself a World Champion Quarter Racing Horse, twelve times.<ref name=Wiggins83/><ref name=NyeGreat155>Nye ''Great Moments in Quarter Racing'' p. 155</ref> Their last meeting, on September 6, 1959 at [[Ruidoso Downs (track)|Ruidoso Downs]], was also Go Man Go's final race, and was the only one of their races won by Vandy's Flash.<ref name=Wiggins83>Wiggins ''Great American Speedhorse'' p. 83</ref>

Go Man Go won 27&nbsp;times, placed second 9&nbsp;times and was third 3&nbsp;times.<ref name=Digest/> Because he placed so regularly, by the end of his racing career tracks had difficulty filling races if other racing stables knew he was entered.<ref name=Legends/> His race earnings were $86,151 (approximately ${{formatnum:{{inflation|US|86151|1957|r=-2}} }} as of {{CURRENTYEAR}}) with 88&nbsp;AQHA racing points, which earned him a Superior Race Horse award as well as a Race Register of Merit from the AQHA. The best [[Speed index|speed rating]], or racing grade, he achieved was AAAT, the highest grade awarded at the time.<ref name=Digest>Wagoner ''Quarter Racing Digest'' pp. 426–432</ref> Go Man Go was named World Champion Quarter Running Horse for three years running, from 1955 to 1957.<ref name=Pitzer>Pitzer ''The Most Influential Quarter Horse Sires'' pp. 43–44</ref> He was the first two-year-old to win the title.<ref name=Chamberlain/> He was a multiple [[Graded stakes race|stakes]] winner,<ref name=Wiggins80/> and his wins included the Pacific Coast Quarter Racing Association Futurity, LA Autumn Championship,<ref name=Moments130/> and the Clabbertown G Stakes, which he won three times in a row.  At his retirement, he held the world records at {{convert|440|yd|m}} and {{convert|350|yd|m}}, as well as age and sex records at {{convert|400|yd|m}}.<ref name=Wiggins80>Wiggins ''Great American Speedhorse'' p. 80</ref> Go Man Go is still the only stallion who has been World Champion Quarter Running Horse three times, and, along with the mare Woven Web (TB), is one of only two horses to be three-time winners of the award.<ref name=Champions>[http://racing.aqha.com/racing/dyn_content.aspx?FQD=http://www.aqha.com/aqharacing.com/champions/worldchampionhistroy.html AQHA Racing Champions History]</ref>

==Ownership and registration problems==
In 1955, when Go Man Go was a two-year-old, A.B. Green bragged that he intended to buy the horse from Ferguson.  Although Ferguson did not want to sell, he felt he had to at least set a price.  After hearing rumors that Green was prepared with a [[cashier's check]] for $40,000 (approximately ${{formatnum:{{inflation|US|40000|1955|r=-2}} }} as of {{CURRENTYEAR}}), Ferguson set the price at $42,000 cash (approximately ${{formatnum:{{inflation|US|42000|1955|r=-2}} }} as of {{CURRENTYEAR}}) and twenty-one&nbsp;breedings to the stallion. To Ferguson's surprise, Green had that much cash available; Ferguson felt compelled to sell Go Man Go. Two years later, at a [[Los Alamitos Race Course|Los Alamitos]] race meet, Green claimed that his newest horse, Double Bid, could outrace Go Man Go. This incensed Ferguson, who had just entered Go Man Go's full brother Mr Mackay in a race with Double Bid. Ferguson bet Green $42,000 (approximately ${{formatnum:{{inflation|US|42000|1957|r=-2}} }} as of {{CURRENTYEAR}}) against Go Man Go that Mr Mackay would beat Double Bid in the upcoming race. Mr Mackay won the race, and Ferguson regained ownership of Go Man Go. Later in 1960, because he also owned Go Man Go's full brother, father, and mother, he sold Go Man Go to Frank Vessels Sr. and Bill and Harriet Peckham for $125,000 (approximately ${{formatnum:{{inflation|US|125000|1960|r=-2}} }} as of {{CURRENTYEAR}}). Later, however, all three horses retained by Ferguson died prematurely.<ref name=Groves>Groves "Letting Go of Go Man Go" ''Quarter Horse Journal'' July 1994 p. 18</ref>{{efn|Top Deck died of an illness in 1965, after heroic efforts by vets to save his life. The cause of death of Go Man Go's mother and siblings is not given in the sources.<ref>Simmons ''Legends'' pp. 117–119</ref>}}

Green ran into problems with Go Man Go's registration. At that time, the AQHA had two types of registration, the Appendix and the Tentative. Appendix-registered horses were the offspring of Thoroughbreds and either Tentative-registered Quarter Horses or Appendix-registered Quarter Horses. Go Man Go was originally registered in the Appendix, as his dam was an Appendix-registered mare. The way to advance out of the Appendix into the Tentative registry was to qualify on performance grounds and pass a [[equine conformation|conformation]] examination conducted by the AQHA. Go Man Go certainly qualified under the performance criteria, but his conformation was such that he resembled a Thoroughbred more than he resembled a Quarter Horse. Green knew that in order to increase his [[stud fee]]s—the price paid for the right to breed a mare to a stallion—Go Man Go needed to acquire a regular registration number instead of his Appendix number. So Green appealed to the Executive Committee of the AQHA, which had the authority to award Tentative numbers to horses regardless of conformation exam results. In both 1956 and 1957, the committee declined to take action, waiting to evaluate the quality of Go Man Go's first foals before making a decision. Finally, in 1958, they awarded Go Man Go number 82,000 in the Tentative registry.<ref name=Legends/>

==Breeding career and legacy==

Retired to the breeding shed, Go Man Go early on proved his worth as a [[Stallion (horse)|stallion]].<ref name=Legends/> Of his first foal crop, born in 1958, three reached the finals of the All American Futurity: Mr Meyers, Dynago Miss and Angie Miss.<ref name=Wiggins110>Wiggins ''Great American Speedhorse'' pp. 110–112</ref>{{efn|They finished fifth, sixth and eighth, respectively.<ref>Wiggins ''Great American Speedhorse'' p. 110</ref>}} His stud fee in 1960 was $500 (approximately ${{formatnum:{{inflation|US|500|1960}} }} as of {{CURRENTYEAR}}), but by 1963 it had risen to $2,500 (approximately ${{formatnum:{{inflation|US|2500|1963}} }} as of {{CURRENTYEAR}}).<ref name=Complete443>Nye ''Complete Book of the Quarter Horse'' p. 443</ref><ref>per the CPI valuation at [http://www.measuringworth.com/uscompare/ Measuring Worth] using $500 and $2500 as the starting figures and 1960 and 1963 as the starting years. Accessed on July 26, 2008</ref> He sired 942&nbsp;foals, of which 552 earned their Race Register of Merit. Seventy-two of his offspring were awarded a Superior Race Horse award.<ref name=Legends/> Among his [[get (animal)|get]], or offspring, were Go Josie Go, Dynago Miss, Duplicate Copy, Story Man, and Hustling Man.<ref name=Pitzer/> His daughter [[Goetta]] won the All American Futurity and was inducted into the American Quarter Horse Hall of Fame.<ref name=Hall07>"Hall of Fame 2007" ''Quarter Horse Journal'' March 2007 p. 51</ref> Another daughter, [[Ought To Go]] was also inducted into the AQHA Hall of Fame. Two grandget were also inducted into the AQHA Hall of Fame: Kaweah Bar and Rocket Wrangler.<ref name=AQHAHoF>[http://www.aqha.com/Foundation/Museum/Hall-of-Fame/Hall-of-Fame-Inductees.aspx AQHA Hall of Fame Inductees] {{webarchive |url=https://web.archive.org/web/20111012223820/http://www.aqha.com/Foundation/Museum/Hall-of-Fame/Hall-of-Fame-Inductees.aspx |date=October 12, 2011 }}</ref> Eight of his offspring won Champion Quarter Running Horse awards.<ref name=Pitzer/> His entry listing his offspring who won Race Register of Merits in the ''Quarter Racing Digest'' covers five full pages plus part of another.<ref name=Digest/> As a broodmare sire, or maternal grandsire, his daughters have produced Rocket Wrangler, Mr Kid Charge, Kaweah Bar, and Go Together.<ref name=Wiggins110/><ref name=Denhardt268>Denhardt ''Quarter Running Horse'' p. 268</ref><ref name=Wiggins91>Wiggins ''Great American Speedhorse'' p. 91</ref> As of April 2008, his offspring had earned over $7,000,000 on the racetrack.<ref name=SireRecord/>

As a breeding stallion, Go Man Go continued to have a reputation as a scoundrel, although Kathlyn Green, wife of A. B. Green, disputed that image. She said that he liked to have his lip tugged, and would lean over the stall door waiting for people to come along and tug on it for him.<ref name=Legends/> However, she said of him "he absolutely hated getting his feet dirty".<ref name=QLegends124>quoted in Close et al. ''Legends'' p. 124</ref> Go Man Go passed through a number of hands after Green owned him, including Les Gosselin, Frank Vessels, and Harriett Peckham, who was his owner by 1972.<ref name=Legends/> In 1967, when Vessels sold his half-interest in Go Man Go to Briarwood Farms, the deal was said to be a record price for a Quarter Horse.<ref name=VesselsSells>Staff "Vessels Sells Go Man Go" ''The Independent''</ref> Go Man Go died in 1983 and was buried near the headquarters of the Buena Suerte Ranch in [[Roswell, New Mexico]]. His crown-shaped granite headstone is engraved: "Go Man Go, The King."<ref name=LastRites>Wohlfarth "Last Rites" ''Quarter Horse Journal'' July 1996 p. 14</ref>

Go Man Go was inducted into the American Quarter Horse Hall of Fame in 1990.<ref name=AQHAHoF/> A further honor was the naming of a stakes race after him,<ref name=Complete311>Nye ''Complete Book of the Quarter Horse'' pp. 311, 374</ref> the [[Graded stakes race|Grade I]] Go Man Go Handicap run in September at Los Alamitos.<ref name=StakesSchedule>"[http://www.aqha.com/Racing/Content-Pages/Horseman-Info/~/media/Files/Racing/Stakes%20and%20Race%20Dates/2012%20Graded%20Stakes.ashx 2012 AQHA Stakes Schedule]}"</ref> Walt Wiggins, a racing commentator and author, said of Go Man Go: "He was a brilliant speedhorse, some say the fastest ever. He was wild and reckless, a rogue at first, and often a clown who seldom saw the uniqueness of his talents or the seriousness of his commission. He had intrinsic greatness and couldn't care less."<ref name=Wiggins78>Wiggins ''Great American Speedhorse'' p. 78–79</ref> Dan Essary, who was Director of Racing for the AQHA for many years, described Go Man Go's impact on the Quarter Horse breed as "He was to Quarter Horse racing what Babe Ruth was to baseball, what Ben Hogan was to Golf and what Man o'War was to Thoroughbred racing. Horses may have run faster and horses have earned more money, but the fame of Go Man Go lingers."<ref name=QWiggins112>quoted in Wiggins ''Great American Speedhorse'' p. 112</ref>

==Pedigree==
{{Ahnentafel-compact5
|style=font-size: 100%; line-height: 110%;
|border=1
|boxstyle=padding-top: 0; padding-bottom: 0;
|1 = Go Man Go
|2 = [[Top Deck (horse)|Top Deck (TB)]]{{efn|(TB) is an abbreviation for Thoroughbred.}}
|3 = Lightfoot Sis
|4 = Equestrian (TB)
|5 = River Boat (TB)
|6 = Very Wise (TB)
|7 = Clear Track
|8 = [[Equipoise (horse)|Equipoise (TB)]]
|9 = Frilette (TB)
|10 = Chicaro (TB)
|11 = Last Boat (TB)
|12 = Wise Counsellor (TB)
|13 = Omona (TB)
|14 = The Dun Horse
|15 = Ella
|16 = Pennant (TB)
|17 = Swinging (TB)
|18 = [[Man o' War|Man o' War (TB)]]
|19 = *Frillery (TB){{efn|An asterisk means the horse was imported into North America.}}
|20 = *Chicle (TB)
|21 = Wendy (TB)
|22 = [[Sir Gallahad III|*Sir Gallahad 3rd (TB)]]
|23 = Taps (TB)
|24 = Mentor (TB)
|25 = Rustle (TB)
|26 = *Ormond (TB)
|27 = Simona (TB)
|28 = Dewey (TB)
|29 = Mais
|30 = Old DJ
|31 = mare by Beauregard}}

==Notes==
{{notelist|60em}}

==Citations==
{{Reflist|40em}}

==References==
{{refbegin|60em}}
* {{cite web |url=http://www.aqha.com/Racing/Content-Pages/Horseman-Info/~/media/Files/Racing/Stakes%20and%20Race%20Dates/2012%20Graded%20Stakes.ashx |title=2012 AQHA Racing Stakes Schedule |publisher=American Quarter Horse Association |date=July 8, 2011 |format=pdf |accessdate= April 1, 2012}}
* {{cite web|url=http://www.aqha.com/Foundation/Museum/Hall-of-Fame/Hall-of-Fame-Inductees.aspx |title=AQHA Hall of Fame Inductees |publisher=[[American Quarter Horse Association]] |accessdate=August 1, 2011 |deadurl=yes |archiveurl=https://web.archive.org/web/20111012223820/http://www.aqha.com/Foundation/Museum/Hall-of-Fame/Hall-of-Fame-Inductees.aspx |archivedate=October 12, 2011 |df= }}
* ''AQHA Official Get of Sire Summary Record for Go Man Go'' American Quarter Horse Association Records Department. Retrieved on April 5, 2008
* {{cite web |url=http://racing.aqha.com/racing/dyn_content.aspx?FQD=http://www.aqha.com/aqharacing.com/champions/worldchampionhistroy.html |title=AQHA Racing Champions History |publisher=[[American Quarter Horse Association]] |accessdate = April 4, 2008}}
* {{cite journal |author= Chamberlain, Richard |date=April 1995 |title= April 15 |journal= Quarter Racing Journal |page=10 |issn= 0899-3130}}
* {{cite book |author=Close, Pat; Simmons, Diane (editors) |title= Legends: Outstanding Quarter Horse Stallions and Mares |publisher=Western Horseman |location=Colorado Springs, CO |year=1993  |isbn=0-911647-26-0 }}
* {{cite book |author=Denhardt, Robert M.|title=The Quarter Running Horse: America's Oldest Breed  |year=1979 |publisher=University of Oklahoma Press |location=Norman, OK |isbn=0-8061-1500-9}}
* {{cite encyclopedia |author=Goodhue, Jim |title=A History of Early AQHA Registration |editor=Simmons, Diane C. |encyclopedia=Legends: Outstanding Quarter Horse Stallions and Mares |pages=4–10 |publisher=Western Horseman |location=Colorado Springs, CO |year=1993 |isbn=0-911647-26-0 }}
* {{cite journal |author= Groves, Lesli Krause |date=July 1994 |title= Letting Go of Go Man Go |journal= Quarter Horse Journal |page=18|issn= 1538-3490 }}
* {{cite book |author= LeBlanc, Francis S.|title= Cajun-Bred Running Horses: Notes on Horse Racing in Southwest Louisiana  |year= 1978 |publisher= The Acadiana Press |location= Lafayette, LA|oclc= 4859031 }}
* {{cite book |author=Nye, Nelson C. |authorlink= Nelson C. Nye |title= The Complete Book of the Quarter Horse: A Breeder's Guide and Turfman's Reference|year= 1964|publisher= A. S. Barnes and Co.|location=New York|oclc= 1373730 }}
* {{cite book |author=Nye, Nelson C. |authorlink= Nelson C. Nye |title= Great Moments in Quarter Racing History |year= 1983 |publisher= Arco Publishing |location=New York|isbn= 0-668-05304-6 }}
* {{cite book |author= Pitzer, Andrea Laycock|title= The Most Influential Quarter Horse Sires |year= 1987 |publisher= Premier Pedigrees |location= Tacoma, WA|oclc= 18561545 }}
* {{cite book |author=Price, Steven D. |title=The American Quarter Horse: An Introduction to Selection, Care, and Enjoyment |publisher=The Lyons Press |location=New York |year=1998 |isbn=1-55821-643-X }}
* {{cite journal |author= Staff |date=March 2007 |title= Hall of Fame 2007 |journal= Quarter Horse Journal |pages=42–55 |issn= 1538-3490}}
* {{cite news |author=Staff |title=Vessels Sells Go Man Go in Richest 1/4 Horse Deal |newspaper=The Independent |date=December 28, 1967 |via=Newspapers.com |url=https://www.newspapers.com/clip/822632// |page=34 }} {{Open access}}
* {{cite book |author= Wagoner, Dan |title= Quarter Racing Digest: 1940 to 1976 |year= 1976 |publisher= Equine Research |location= Grapevine, TX|oclc= 13811854 }}
* {{cite book |author= Wiggins, Walt|title= The Great American Speedhorse: A Guide to Quarter Racing  |year= 1978 |publisher= Sovereign Books |location= New York |isbn= 0-671-18340-0 }}
* {{cite journal |author= Wohlfarth, Jenny |date=July 1996 |title= Last Rites |journal= Quarter Horse Journal|page=14|issn= 1538-3490 }}
{{refend}}

==External links==
* [http://www.qhd.com/horse/stallion.asp?id=5084 Go Man Go at Quarter Horse Directory]
* [http://www.allbreedpedigree.com/go+man+go3 All Breed Pedigree Database Pedigree of Go Man Go]
* [http://www.aqha.com/en/Foundation/Content-Pages/Hall-of-Fame/~/media/Files/Foundation/Hall%20of%20Fame/Inductees/Horses/Go%20Man%20Go.ashx Go Man Go biography at the AQHA Hall of Fame]

{{featured article}}

{{Authority control}}

[[Category:1953 racehorse births]]
[[Category:1983 racehorse deaths]]
[[Category:Racehorses bred in Texas]]
[[Category:American Quarter Horse sires]]
[[Category:American Quarter Horse racehorses]]
[[Category:Racehorses trained in the United States]]