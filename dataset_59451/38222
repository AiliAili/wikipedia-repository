{{Good article}}
{{Infobox University Boat Race
| name= 48th Boat Race
| winner =Oxford
| margin = 1/2 length
| winning_time= 21 minutes 48 seconds
| date= {{Start date|1891|3|21|df=y}}
| umpire =[[Frank Willan (rower)|Frank Willan]] <br>(Oxford)
| prevseason= [[The Boat Race 1890|1890]]
| nextseason= [[The Boat Race 1892|1892]]
| overall =22&ndash;25
}}
The '''48th Boat Race''' took place on 21 March 1891.  [[The Boat Race]] is an annual [[Rowing (sport)#Side by side|side-by-side rowing]] race between crews from the Universities of [[University of Oxford|Oxford]] and [[University of Cambridge|Cambridge]] along the [[River Thames]].  Oxford went into the race leading 24&ndash;22 in the event overall.  In total, eight rowers who were participating had previous Boat Race experience.  Umpired by former Oxford rower [[Frank Willan (rower)|Frank Willan]], pre-race favourites Oxford won by half-a-length in a time of 21 minutes 48 seconds.  It was Oxford's narrowest winning margin since the [[The Boat Race 1867|1867 race]].

==Background==
[[File:R.C.Lehmann.jpg|right|thumb|upright|[[R. C. Lehmann]] coached Oxford, despite being a [[Cantabrigian]] and former captain of [[Trinity College Boat Club|1st Trinity Boat Club]].]]
[[The Boat Race]] is a [[Rowing (sport)#Side by side|side-by-side rowing]] competition between the boat clubs of [[University of Oxford]] (sometimes referred to as the "Dark Blues")<ref name=blues>{{Cite web | url = https://www.theguardian.com/sport/2003/apr/06/theobserver | work = [[The Observer]] | title = Dark Blues aim to punch above their weight | date = 6 April 2003 | accessdate = 11 September 2014 }}</ref> and the [[University of Cambridge]] (sometimes referred to as the "Light Blues").<ref name=blues/>  The race was first held in 1829, and since 1845 has taken place on the {{convert|4.2|mi|km|adj=on}} [[The Championship Course|Championship Course]] on the [[River Thames]] in southwest London.<ref>{{Cite web | url = http://www.telegraph.co.uk/travel/destinations/europe/uk/london/10719622/University-Boat-Race-2014-spectators-guide.html | work = [[The Daily Telegraph]] | accessdate = 20 August 2014 | date = 25 March 2014 |title = University Boat Race 2014: spectators' guide | first = Oliver |last =Smith}}</ref><ref>{{Cite web | url = http://theboatraces.org/the-course | title = The Course| accessdate = 20 August 2014 | publisher = The Boat Race Company Limited}}</ref>  The rivalry is a major point of honour between the two universities, as of 2014 it is followed throughout the United Kingdom and broadcast worldwide.<ref name=CBC>{{cite news|title=Former Winnipegger in winning Oxford&ndash;Cambridge Boat Race crew|date=6 April 2014|publisher=[[CBC News]]|url=http://www.cbc.ca/news/canada/manitoba/former-winnipegger-in-winning-oxford-cambridge-boat-race-crew-1.2600176|accessdate=20 August 2014}}</ref><ref>{{Cite web | url = http://theboatraces.org/tv-and-radio | title = TV and radio | publisher = The Boat Race Company Limited | accessdate = 5 July 2014}}</ref><ref>{{Cite book | url = https://books.google.com/books?id=o2QpA0fGyiIC&pg=PA287&lpg=PA287&dq=%22boat+race%22+%22united+kingdom%22+audience&source=bl&ots=WJsXwqiRfL&sig=5C_pRDSK839-C46kyEaZJXgjjSk&hl=en&sa=X&ei=gbElVLH2E8jW7Qat-oC4Aw&ved=0CCcQ6AEwAQ#v=onepage&q=%22boat%20race%22%20%22united%20kingdom%22%20audience&f=false | title=Gaming the World: How Sports Are Reshaping Global Politics and Culture| first = Andrei |last=Markovits|first2=Lars |last2=Rensmann| publisher = Princeton University Press| date= 6 June 2010 | isbn=978-0691137513|pages=287&ndash;288}}</ref> Oxford went into the race as reigning champions, having beaten Cambridge by one length in the [[The Boat Race 1890|previous year's race]], and held the overall lead, with 24 victories to Cambridge's 22 (excluding the [[The Boat Race 1877|"dead heat" of 1877]]).<ref>{{Cite web | url= http://theboatraces.org/classic-moments-the-1877-dead-heat | publisher = The Boat Race Company Limited | title = Classic moments – the 1877 dead heat | accessdate = 11 November 2014}}</ref><ref name=results>{{Cite web | url = http://theboatraces.org/results | title =Men &ndash; Results | publisher = The Boat Race Company Limited | accessdate = 27 September 2014}}</ref>

Cambridge were coached by Arthur Middleton Hutchinson (who had rowed for the Light Blues in the [[The Boat Race 1881|1881]] and [[The Boat Race 1882|1882 races]]) while Oxford's coach was [[R. C. Lehmann]], former president of the [[Cambridge Union Society]] and captain of the [[Trinity College Boat Club|1st Trinity Boat Club]].  Although Lehmann had rowed in the trials eights for Cambridge, he was never selected for the Blue boat.<ref>{{Cite book | title = A History of the University of Cambridge: Volume 3, 1750&ndash;1870 | first = Peter | last = Searby| publisher = [[Cambridge University Press]] | date = 6 November 1997 | isbn = 978-0521350600 | page = 664 | url = https://books.google.com/books?id=VoMPRz8nYQEC&pg=PA664}}</ref>

The umpire for the race for the third year in a row was [[Frank Willan (rower)|Frank Willan]] who won the event four consecutive times, rowing for Oxford in the [[The Boat Race 1866|1866]], [[The Boat Race 1867|1867]], [[The Boat Race 1868|1868]] and [[The Boat Race 1869|1869 races]].<ref>Burnell, pp. 49, 59</ref>

==Crews==
{{multiple image
| align = right
| direction = horizontal
| width1 = 220
| image1 = Vivian Nickalls on June 11, 1914.jpg
| width2 = 160
| image2 = Nickalls 5537493922 6b328c147b o.jpg
| footer =Brothers [[Vivian Nickalls|Vivian]] ''(left)'' and [[Guy Nickalls]] ''(right)'' rowed for Oxford.
}}
The Oxford crew weighed an average of 12&nbsp;[[Stone (unit)|st]] 3.75&nbsp;[[Pound (mass)|lb]] (77.7&nbsp;kg), {{convert|7.75|lb|kg|1}} per rower more than their opponents.  Cambridge saw four former [[Blue (university sport)|Blues]] return to the boat, in Gilbert Francklyn, Edmund Towers Fison, John Friend Rowlatt and [[Cambridge University Boat Club]] president Gerard Elin.  Oxford's crew contained four rowers with Boat Race experience, including [[Guy Nickalls]] who was rowing in his fourth consecutive race, this year alongside his brother [[Vivian Nickalls|Vivian]].  The Dark Blues also featured [[Coxswain (rowing)|cox]] John Pemberton Heywood-Lonsdale for the third time in a row.<ref name=burn65>Burnell, p. 65</ref>  

Four of the Oxford crew were studying at [[Magdalen College, Oxford|Magdalen College]] while five of the Cambridge crew had matriculated to [[Trinity Hall, Cambridge|Trinity Hall]].<ref name=burn65/> Two rowers were registered as non-British, F. Wilkinson for Oxford and Edward Wason Lord for Cambridge both hailed from Australia.<ref>Burnell, p. 38</ref>  Wilkinson was forced to leave the Oxford crew during practice as he suffered from influenza;  he was considered sufficiently fit to return to the crew but, according to Drinkwater, "never found his real form again".<ref name=drink95/>

{| class="wikitable"
|-
! rowspan="2" scope="col| Seat
! colspan="3" scope="col| Oxford <br> [[File:Oxford-University-Circlet.svg|30px]]
! colspan="3" scope="col| Cambridge <br> [[File:University of Cambridge coat of arms official.svg|30px]]
|-
! Name
! College
! Weight
! Name
! College
! Weight
|-
| [[Bow (rowing)|Bow]] || [[William Mansfield Poole|W. M. Poole]] || [[Magdalen College, Oxford|Magdalen]] || 10 st 7.5 lb || J. W. Noble || [[Gonville and Caius College, Cambridge|Gonville and Caius]] || 11 st 5.75 lb
|-
| 2 || R. P. P. Rowe || [[Magdalen College, Oxford|Magdalen]] || 11 st 11 lb || E. W. Lord|| [[Trinity Hall, Cambridge|Trinity Hall]] || 10 st 10.25 lb
|-
| 3 ||  [[Vivian Nickalls|V. Nickalls]] || [[Magdalen College, Oxford|Magdalen]] || 12 st 9 lb ||G. Francklyn||  [[Trinity College, Cambridge|3rd Trinity]] || 12 st 3 lb
|-
| 4 || [[Guy Nickalls|G. Nickalls]] || [[Magdalen College, Oxford|Magdalen]] || 12 st 5 lb ||  E. T. Fison || [[Corpus Christi College, Cambridge|Corpus Christi]] || 12 st 7.5 lb
|-
| 5 || F. Wilkinson || [[Brasenose College, Oxford|Brasenose]] || 13 st 8 lb || W. Landale || [[Trinity Hall, Cambridge|Trinity Hall]] || 12 st 11 lb
|-
| 6 || [[Oliver Russell, 2nd Baron Ampthill|Lord Ampthill]] (P) || [[New College, Oxford|New College]] || 13 st 5lb || J. F. Rowlatt ||  [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 12 lb
|-
| 7 || [[William Fletcher (rower)|W. A. L. Fletcher]] || [[Christ Church, Oxford|Christ Church]] || 13 st 2 lb ||  C. T. Fogg-Elliot || [[Trinity Hall, Cambridge|Trinity Hall]] || 11 st 4 lb
|-
| [[Stroke (rowing)|Stroke]] || C. W. Kent || [[Brasenose College, Oxford|Brasenose]] || 13 st 0 lb || G. Elin (P) ||[[Trinity College, Cambridge|3rd Trinity]]  || 10 st 13 lb
|-
| [[Coxswain (rowing)|Cox]] || J. P. Heywood-Lonsdale || [[New College, Oxford|New College]] || 8 st 6 lb || J. V. Braddon || [[Trinity Hall, Cambridge|Trinity Hall]] || 7 st 12 lb
|-
!colspan="7"|Source:<ref name=dodd309>Dodd, p. 309</ref><br>(P) &ndash; boat club president<ref>Burnell, pp. 50&ndash;51</ref>
|}

==Race==
[[File:University Boat Race Thames map.svg|right|thumb|[[The Championship Course]], along which the race is conducted]]
Oxford were pre-race favourites,<ref name=drink95/> and won the [[coin flipping|toss]] and elected to start from the Middlesex station, handing the Surrey side of the river to Cambridge.<ref name=burn65/> The race commenced at 11:09&nbsp;a.m.,<ref name=drink96>Drinkwater, p. 96</ref> in a northerly wind, and despite being [[stroke rate|outrated]] by Cambridge, the Dark Blues took an early lead and were a quarter of a length ahead as the crews passed Craven Steps.<ref name=drink95>Drinkwater, p. 95</ref>  Just before the Mile Post, Oxford's lead was around half a length, but Cambridge pushed on to take the lead by [[Hammersmith Bridge]].  Oxford responded and held a marginal lead as the crews passed The Doves pub, and the boats exchanged the lead several times along Chiswick Reach.<ref name=drink96/>

The Dark Blues took the lead and were three-quarters of a length up at [[Barnes Railway Bridge|Barnes Bridge]].<ref name=drink96/>  Elin increased Cambridge's stroke rate; Charles Kent, the Oxford stroke responded and although the bend in the river favoured the Dark Blues, Cambridge closed the gap.  However, it was too late to stop Oxford taking the victory.<ref name=drink96/>  The Dark Blues passed the finishing post with a half-length lead in a time of 21 minutes 48 seconds.  It was their second consecutive victory and the narrowest winning margin since the [[The Boat Race 1867|1867 race]], taking the overall record to 25&ndash;22 in Oxford's favour.<ref name=results/>

==References==
'''Notes'''
{{reflist|30em}}

'''Bibliography'''
*{{Cite book | title = One Hundred and Fifty Years of the Oxford and Cambridge Boat Race | first = Richard | last = Burnell | authorlink = Dickie Burnell | year=1979| isbn= 0950063878 | publisher = Precision Press}}
*{{Cite book | title = The Oxford & Cambridge Boat Race | first = Christopher| last = Dodd | isbn= 0-09-151340-5 | publisher =Stanley Paul |year= 1983}}
*{{Cite book | title = The University Boat Race &ndash; Official Centenary History | first = G. C. |last = Drinkwater | first2= T. R. B. |last2=Sanders |publisher = Cassell & Company, Ltd.| year = 1929}}

==External links==
* [http://theboatraces.org/ Official website]

{{The Boat Race}}

{{DEFAULTSORT:Boat Race 1891}}
[[Category:1891 in English sport]]
[[Category:The Boat Race]]
[[Category:March 1891 sports events]]