{{good article}}
{{Infobox video game
|title = The Battle For Midway
|image = File:The Battle For Midway cover.jpg
|caption = ZX Spectrum cover art
|developer = [[Personal Software Services]]
|publisher = Personal Software Services
|series = ''Strategic Wargames''
|designer = Alan Steel
|released = {{vgrelease|UK|1984}}
|genre = [[Turn-based strategy]]
|modes = [[Single-player video game|Single-player]]
|platforms = [[Commodore 64]], [[ZX Spectrum]], [[MSX]], [[Amstrad CPC]]
}}

'''''The Battle For Midway''''' is a [[turn-based strategy]] video game developed and published by [[Personal Software Services]]. It was first released in the United Kingdom and France for the [[MSX]] in 1984, and was re-released for the [[Amstrad CPC]], [[Commodore 64]] and [[ZX Spectrum]] in 1985. It is the second instalment of the ''Strategic Wargames'' series. The game is set during the [[Battle of Midway]] in the [[Pacific Ocean theatre of World War II|Pacific Ocean theatre]] of [[World War II]] and revolves around the [[United States Navy]] attacking a large [[Empire of Japan|Imperial Japanese]] fleet stationed at [[Midway Atoll]], in retaliation for the [[attack on Pearl Harbour]].

In the game, the player assumes control of American forces and must eliminate all Japanese forces around the atoll by air or naval combat. ''The Battle For Midway'' received largely negative reviews upon release. It was criticised for its incompatibility with [[black and white television]] sets, as the game was only accessible in a limited range of colours. The easy difficulty of the gameplay was also criticised.

==Gameplay==
[[File:Battle for Midway gameplay.jpg|thumb|left|A map showing combat around [[Midway Atoll]].]]
The game is a [[turn-based strategy]] and focuses on naval battles during the [[Battle of Midway]], which is initiated in response to the Japanese [[attack on Pearl Harbour]].<ref name=your/> The player commands three American task forces; two [[United States Navy]] forces and one [[United States Air Force]] unit, which are stationed on [[Midway Atoll]]. The objective of the game is to defeat three attacking [[Empire of Japan|Imperial Japanese]] naval forces.<ref name=crash/> Each American task force has an [[aircraft carrier]], whereas the Japanese have four. The player begins the game with two American search aircraft used to locate and track the attacking Japanese forces.<ref name=crash/><ref name=aus/>

When the main attacking Japanese force has been located, the player must send all available air units to engage them in combat.<ref name=SU/> Air combat takes place over real time, and may take up to a minute of travel time once launched from an aircraft carrier. Aircraft will run out of fuel over time and will crash if not refuelled at a carrier.<ref name=crash/> The game contains elements of [[arcade game|arcade]] gameplay, which will automatically enable once the player comes into contact with the enemy.<ref name=aus/> The arcade sequences involves the player utilising an anti-air machine gun in order to shoot down Japanese aircraft.<ref name=crash/> The game ends once all four Japanese aircraft carriers have been destroyed.<ref name=crash/><ref name=aus/>

==Background and release==
[[Personal Software Services]] was founded in [[Coventry]], England, by Gary Mays and Richard Cockayne in 1981.<ref name=PSS>{{cite journal|title=History of PSS|journal=Your Computer|date=13 June 1986|volume=6|issue=6|pages=84–85|url=https://archive.org/stream/your-computer-magazine-1986-06/YourComputer_1986_06#page/n83/mode/2up|accessdate=3 October 2015}}</ref> The company was known for creating games that featured historic war battles and conflicts, such as ''[[Theatre Europe]]'', ''Bismark'' and ''[[Falklands '82]]''. The company had a partnership with French video game developer [[ERE Informatique]], and published localised versions of their products to the United Kingdom. In 1986, Cockayne took a decision to alter their products for release on 16-bit machines, as he found that smaller 8-bit computers, such as the [[ZX Spectrum]], lacked the processing power for larger strategy games. The decision was falsely interpreted as "pulling out" from the Spectrum market by [[video game journalism|video game journalist]] Phillipa Irving.<ref name=pull>{{cite journal|last1=Jarratt|first1=Steve|title=Seasonal Drought|journal=Crash|date=May 1988|url=https://archive.org/stream/crash-magazine-52/Crash_52_May_1988#page/n5/mode/2up/search/pss|page=7|issue=52|accessdate=18 October 2015}}</ref> Following years of successful sales throughout the mid 1980s, Personal Software Services experienced financial difficulties, in what Cockayne admitted in a retrospective interview that "he took his eye off the ball". The company was acquired by [[Mirrorsoft]] in February 1987,<ref name=feb>{{cite journal|title=Mirrorsoft has new strategy with PSS|journal=Personal Computing Weekly|date=12 February 1987|volume=6|issue=7|page=6|url=https://archive.org/stream/popular-computing-weekly-1987-02-12/PopularComputing_Weekly_Issue_1987-02-12#page/n0/mode/2up|accessdate=18 October 2015}}</ref> and was later dispossessed by the company due to strains of debt.<ref name=pain>{{cite journal|last1=Arnot|first1=Chris|title=Taking pain out of gain|journal=The Independent|date=26 March 1995|accessdate=4 October 2015|url=http://www.independent.co.uk/news/business/taking-pain-out-of-gain-1612866.html}}</ref> 

Upon release, ''The Battle For Midway'' was packaged with an exclusive ring-binder and a manual detailing the nature of the [[Battle of Midway]].<ref name=crash/> It was later re-released as part of a ''Strategic Wargames'' compilation cassette known as ''Conflicts 2'', published by Personal Software Services.<ref name=YS/>

==Reception==
{{Video game reviews
| CRASH = 4/10<ref name=crash/>
| rev1 = ''[[Your Sinclair]]''
| rev1Score = 5/10<ref name=YS/>
| rev2 = ''[[Sinclair User]]'' 
| rev2Score = {{Rating|2|5}}<ref name=SU/>
| rev3 = ''[[Your Spectrum]]''
| rev3Score = 2/5<ref name=your/>
| rev4 = ''[[Amstrad Action]]''
| rev4Score = 59%<ref name=action/>
| rev5 = ''[[Amtix]]''
| rev5Score = 57%<ref name=amtix/>
| rev6 = ''[[Your Computer (British magazine)|Your Computer]]''
| rev6Score = {{rating|2|5}}<ref name=YC/>
}}
The game received negative reviews upon release. Angus Ryall of ''[[Crash (magazine)|Crash]]'' criticised the game's incompatibility with [[black and white television]] sets, stating that, despite a growing British economy, Ryall expected the developers to have designed games for "the lowest common denominator".<ref name=crash>{{cite journal|last1=Ryall|first1=Angus|title=The Battle for Midway review (Crash)|journal=Crash|date=July 1985|issue=18|page=126|url=http://www.zxspectrumreviews.co.uk/review.aspx?gid=548&rid=5649|accessdate=16 January 2016}}</ref> Gwyn Hughes of ''[[Your Sinclair]]'' criticised the tactical elements of the game as too light, stating that the success of the player depends on dexterity, and not "brainpower".<ref name=YS>{{cite journal|last1=Hughes|first1=Gwyn|title=Conflicts 2: Battle for Midway|journal=Your Sinclair|date=August 1987|issue=20|page=74|url=http://wos.meulie.net/pub/sinclair/magazines/YourSinclair/Issue20/Pages/YourSinclair2000074.jpg|accessdate=16 January 2016}}</ref> Clare Edgely of ''[[Sinclair User]]'' praised the game's historical accuracy, however she felt that its late release in comparison to other wargames made ''The Battle For Midway'' feel "ordinary".<ref name=SU>{{cite journal|last1=Edgely |first1=Clare |title=Battle of Midway review |url=http://www.sincuser.f9.co.uk/040/sftwreb.htm |journal=Sinclair User |date=September 1985 |issue=40 |accessdate=16 January 2016 |ref=http://web.archive.org/web/20120207142110/http://www.sincuser.f9.co.uk/040/sftwreb.htm |deadurl=yes |archiveurl=https://web.archive.org/web/20120207142110/http://www.sincuser.f9.co.uk/040/sftwreb.htm |archivedate=February 7, 2012 }}</ref> A reviewer of ''[[Your Computer (British magazine)|Your Computer]]'' stated that the game was a "flawed" attempt to recreate the famous Battle of Midway, despite admitting that it contained "some nice touches".<ref name=YC>{{cite journal|title=Battle for Midway review|journal=Your Computer|date=November 1984|volume=4|issue=11|page=51|url=https://archive.org/stream/your-computer-magazine-1984-11/YourComputer_1984_11#page/n50/mode/1up|accessdate=24 January 2016}}</ref> A reviewer of ''Australian Commodore Review'' praised the game's wide range of features such as the save and load functions. However, they criticised menu designs and "insufficiently integrated" arcade sequences, calling them both "poor".<ref name=aus>{{cite journal|title=Games Review: Battle For Midway|journal=Australian Commodore Review|date=April 1986|volume=3|issue=4|pages=25, 26|url=https://archive.org/stream/Australian_Commodore_Review_The_Volume_3_Issue_4_1986-04_Saturday_Magazine_AU#page/n27/mode/2up/search/midway|accessdate=24 January 2016}}</ref>

Two reviewers of ''[[Your Spectrum]]'' criticised the combat sequences' reliance on the speed of pressing keys instead of the use of strategy. One reviewer considered the game to be sophisticated, however the other reviewer viewed the game's slow pace and graphics negatively.<ref name=your>{{cite journal|title=Joystick Fury: Battle of Midway|journal=Your Spectrum|date=August 1985|issue=17|url=http://www.users.globalnet.co.uk/~jg27paw4/yr17/yr17_39.htm|accessdate=16 January 2016}}</ref> Despite the criticism, Ryall praised the real time element of the game and accessibility, saying that ''The Battle For Midway'' is up to "current standards".<ref name=crash/> A reviewer of ''[[Amstrad Action]]'' praised the game's "accurate" reproduction of events and different levels of speed, however criticised the easy predictability of Japanese forces.<ref name=action>{{cite journal|title=Battle for Midway review|journal=Amstrad Action|date=October 1985|issue=1|page=87|url=https://archive.org/stream/amstrad-action-001/Amstrad_Action_001#page/n87/mode/2up/search/midway|accessdate=24 January 2016}}</ref> A reviewer of ''[[Amtix]]'' stated that the game suffered from "average" graphics and "poor" sound, and also questioned the inclusion of the arcade sequences.<ref name=amtix>{{cite journal|title=Battle for Midway review (Amtix)|journal=Amtix|date=December 1985|issue=2|page=74|url=https://archive.org/stream/amtix-magazine-02/Amtix_02_Dec_1985#page/n73/mode/2up/search/midway|accessdate=24 January 2016}}</ref> A reviewer of ''[[Your 64]]'' recommended ''The Battle for Midway'' for beginners to the genre, despite stating that it was "not a simple game".<ref name=y64>{{cite journal|title=Battle of Midway review (Y64)|journal=Your 64|date=February 1985|issue=6|pages=51|url=https://archive.org/stream/your64-magazine-06/Your64_Issue_06_1985_Feb#page/n50/mode/1up|accessdate=24 January 2016}}</ref> A reviewer of ''Commodore Horizons'' called it an "enthralling" game.<ref>{{cite journal|title=Soft Hits: Battle for Midway|journal=Commodore Horizons|date=October 1984|issue=10|page=24|url=https://archive.org/stream/commodore-horizons-10/Commodore_Horizons_Issue_10_1984_Oct#page/n17/mode/1up|accessdate=24 January 2016}}</ref>

==References==
{{reflist|2}}

{{DEFAULTSORT:Battle For Midway}}
[[Category:1984 video games]]
[[Category:MSX games]]
[[Category:ZX Spectrum games]]
[[Category:Amstrad CPC games]]
[[Category:Commodore 64 games]]
[[Category:Single-player-only video games]]
[[Category:Turn-based strategy video games]]
[[Category:Video games developed in the United Kingdom]]
[[Category:World War II video games]]
[[Category:Personal Software Services games]]