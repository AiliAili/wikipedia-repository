{{for|the Caleb Carr novel|The Alienist}}
{{infobox short story | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = The Psychiatrist
| title_orig    = O alienista
| translator    = William L. Grossman
| author        = [[Machado de Assis]]
| illustrator   = 
| cover_artist  = 
| country       = [[Brazil]]
| language      = [[Brazilian Portuguese]]
| genre         = [[Novella]], satire
| publication_type = [[Anthology]]
| published_in  = [[Papéis avulsos]]
| publisher     = 
| pub_date      = (1881–)[[1882 in literature|1882]]
| english_pub_date = [[1963 in literature|1963]]
| media_type    = Print 
| pages         = 86
}}
'''"O alienista"''' (translated as '''"The Psychiatrist"''' then '''"The Alienist"''') is a satiric [[novella]] written by the [[Brazil]]ian author surnamed [[Machado de Assis]] (1839–1908). The story ran in [[Rio de Janeiro]]'s newspaper ''A Estação'' (from 15 October 1881 to 15 March 1882), then was published in [[1882 in literature|1882]] as part of the author's short-story collection ''[[Papéis avulsos]]'' ("Single Papers"). An English translation was published in [[1963 in literature|1963]].

In [[1970 in film|1970]], the story was adapted into the comedy film ''[[The Alienist (film)|The Alienist]]''. In [[2007 in comics|2007]], it was adapted into a same-titled graphic novella<!--Brazilian Portuguese (2007) and French (2014), no English as of December 2014--> by [[Fábio Moon]] and [[Gabriel Bá]] (and won the 2008 [[Prêmio Jabuti]]<ref>{{pt}} [http://premiojabuti.com.br/edicoes-anteriores/premio-2008/ 2008 winners] at [[Prêmio Jabuti]]</ref> for educational material<!--"Didático ou paradidático do ensino fundamental e médio"-->).

==Plot summary==
Published a year after Machado's first major novel, ''[[Memórias Póstumas de Brás Cubas]]'', "The Psychiatrist" follows the scientific efforts of Dr. Simon Bacamarte (''Simão Bacamarte'' in the original – "bacarmarte" being Portuguese for "[[blunderbuss]]", an old scattershot gun). Bacamarte, a Brazil-born Portuguese (when Brazil was a colony), is a prominent physician whose sincere obsession for discovering a universal method to cure pathological disorders drives inhabitants of the small Brazilian town of Itaguaí to fear, conspiracy, and revolutionary attempts.

;Chapters 1–4
In a short space of time, Bacamarte's newly opened asylum, popularly named "the Green House" (''Casa Verde''), passes to take inside of its walls not only mentally ill patients but also healthy citizens who, according to the doctor's diagnoses, are about to develop some sort of mental illness.

;Chapters 5–10
Porfírio, the town's barber, indicts Bacamarte for his corruptive influence over the Municipal Council, which since the beginning approved the experiments taken place at the Green House, "the Bastille of human Knowledge". A revolt and council change ensues, but the new regime proposes an alliance to the alienist, until being toppled back to the original council.

;Chapters 11–12
This gives pause to Bacamarte, who changes his tack and decides that balanced people are actually a small minority, and thus the anomaly that should be cured: the modest, the loyal, the wise, the patient, etc., are now admitted to be scientifically disequilibrated according to his new theory.

;Chapter 13
After they all have been "cured" and discharged, Bacamarte eventually considers that he's the most well-balanced person of the village and thus the one most in need of treatment. Uncompromising to the last, he locks himself alone into his asylum, where he dies seventeen months later. The village concludes that he was the only madman from day one. "Be that as it may, his funeral was conducted with great pomp and rare solemnity."

==English editions==
* 1963, ''The Psychiatrist, and Other Stories'' (trans. [[William L. Grossman]] for the novella<ref>[https://books.google.com/books?id=mEyzC0EzmksC&pg=PR5 Table of contents] at Google Books</ref> and three stories, and [[Helen Caldwell]] for the other 8 stories), [[University of California Press|University of California]]
* 2012, ''The Alienist'' (trans. [[William Grossman]]), [[Melville House Publishing|Melville House]]
* 2013, ''The Alienist and Other Stories of Nineteenth-century Brazil'' (trans. [[John Charles Chasteen]]), [[Hackett Publishing Company|Hackett]]

==See also==

* "[[The System of Doctor Tarr and Professor Fether]]", a short story by Edgar Allan Poe about an asylum run by the inmates.

==References==
<references/>

==External links==
{{Wikisourcelang|pt|O Alienista}}

;Reviews
* [https://www.theguardian.com/books/2012/dec/23/alienist-machado-de-assis-review "The Alienist by Machado de Assis – review"], ''[[The Observer]]'' (at ''[[The Guardian]]''), 23 December 2012

{{Use dmy dates|date=August 2014}}

{{Works by Machado de Assis}}

{{DEFAULTSORT:Alienista, O}}
[[Category:1881 short stories]]
[[Category:1882 short stories]]
[[Category:Brazilian short stories]]
[[Category:Brazilian novellas]]
[[Category:Machado de Assis]]