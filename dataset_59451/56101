{{Infobox Journal
| title = Canadian Journal of Linguistics/Revue canadienne de linguistique
| cover = [[File:Canadian Journal of Linguistics.gif]]
| editors = [[Éric Mathieu]]
| language = English, French
| discipline = [[Linguistics]]
| abbreviation = Can. J. Linguist.
| publisher = [[Cambridge University Press]] on behalf of the [[Canadian Linguistic Association]]
| frequency = Triannually
| history = 1954-present
| openaccess =
| website = http://journals.cambridge.org/action/displayJournal?jid=CNJ
| link1 = http://journals.cambridge.org/action/displayIssue?jid=CNJ&tab=currentissue
| link1-name = Online access
| link2 = http://journals.cambridge.org/action/displayBackIssues?jid=CNJ
| link2-name = Online archive
| link3 = http://muse.jhu.edu/journal/270
| link3-name = Online archive at [[Project MUSE]]
| JSTOR =
| OCLC = 654708127
| LCCN = 91645410
| ISSN = 0008-4131
| eISSN = 1710-1115
}}
The '''''Canadian Journal of Linguistics/Revue canadienne de linguistique''''') is a triannual [[peer-reviewed]] [[academic journal]] covering [[theoretical linguistics|theoretical]] and [[applied linguistics]]. It is published by [[Cambridge University Press]] on behalf of the [[Canadian Linguistic Association]]. The journal was established in 1954 and the [[editor-in-chief]] is [[Éric Mathieu]] ([[University of Ottawa]]).

==Abstracting and indexing==
The journal is abstracted and indexed in the [[Arts and Humanities Citation Index]],<ref name=ISI>{{cite web |url=http://ip-science.thomsonreuters.com/mjl/ |title=Master Journal List |publisher=[[Thomson Reuters]] |work=Intellectual Property & Science |accessdate=2016-06-08}}</ref> [[Current Contents]]/Arts & Humanities,<ref name=ISI/> and [[Scopus]].<ref name=Scopus>{{cite web |url=http://www.elsevier.com/online-tools/scopus/content-overview |title=Content overview |publisher=[[Elsevier]] |work=Scopus |accessdate=2016-06-08}}</ref>
==References==
{{Reflist}}

==External links==
*{{Official website|1=http://journals.cambridge.org/action/displayJournal?jid=CNJ}}

{{DEFAULTSORT:Canadian Journal Of Linguistics}}
[[Category:Linguistics journals]]
[[Category:Publications established in 1954]]
[[Category:Multilingual journals]]
[[Category:Triannual journals]]
[[Category:Cambridge University Press academic journals]]


{{ling-journal-stub}}