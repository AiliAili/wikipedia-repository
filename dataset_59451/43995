{|{{Infobox Aircraft Begin
 |name = L5
 |image = File:Junkers L 5 im Technikmuseum Hugo Junkers Dessau 2010-08-06 01.jpg
 |caption = Junkers L5
}}{{Infobox Aircraft Engine
 |type=[[Inline engine (aviation)|Inline]] [[aircraft engine]]
 |manufacturer=[[Junkers|Junkers Motorenbau GmbH (Jumo)]]
 |national origin=
 |first run={{avyear|1922}}<ref name="Gunston"/>
 |major applications=[[Junkers F.13]]
 |number built =>1,000<ref name="Kay"/> 
 |program cost = 
 |unit cost = 
 |developed from = 
 |variants with their own articles = [[Junkers L55]]
 |developed into = 
}}
|}

The '''Junkers L 5''' was a six-cylinder, [[water cooling|water-cooled]], [[Inline engine (aviation)|inline engine]] for aircraft built in Germany during the 1920s. First run in [[1925 in aviation|1925]], it was a much enlarged development of the [[Junkers L2]], in turn a licensed development of the [[BMW IV]].

==Design and development==
The Junkers L5 was a development of Junkers' first water-cooled engine, the [[Junkers L2|L2]], but at four times the swept volume was a much more powerful engine.  It was a water-cooled upright inline 6-cylinder unit, four-stroke and petrol-fuelled, with a capacity of nearly 23 litres. It adopted some of the L2 features, having twin exhaust and inlet valves in each cylinder<ref name="Kay"/>driven by an overhead camshaft, twin [[spark plugs]] and twin [[ignition magneto|magneto]]s.  The [[splash lubrication|splash]] component of the L2's lubrication was abandoned in favour of a completely forced recirculating system.  The twin carburettors of the L2 were replaced with a single float chamber, dual-venturi model.  Like the L2, the L5 was a direct drive engine.<ref name="Kay"/>

The compression ratio of the standard version was 5.5:1, but variants had other ratios to cope with fuels with octane ratings between 76 and 95.  The G series introduced carburettor heating together with an hydraulically damped mounting system.  There were also choices of starting system, from inertial or compressed air systems to the traditional hand swinging.<ref name="Kay"/>

==Operational history==
The L5 proved to be reliable and became the engine of choice for most Junkers aircraft in the mid-1920s as well as powering aircraft from other German manufacturers.<ref name="Kay"/>  Many of these powered the [[Junkers F.13]] and its derivatives like the [[Junkers W33|W&nbsp;33]], which dominated world air transport in the mid-1920s.<ref name="Kay2"/>

The best demonstration of the reliability of the L5 was given by the unit which powered the single-engined W 33 ''Bremen '' in the first fixed wing east to west crossing of the Atlantic in April 1928.  For this flight the compression ratio was raised to 7:1 to provide sufficient power for the heavily fuelled aircraft at take off.  In July  1925 a W&nbsp;33 powered by a L5 stayed aloft for 65&nbsp;h 25&nbsp;min, with a fuel consumption of 35.6&nbsp;kg/h.<ref name="Kay"/>

==Variants==
*'''L5''' many variants including a variety of on compression ratios, power output levels and starting systems.
*'''[[Junkers L55|L55]]''' an upright V-12 built from two L5s on a common crankshaft.
*'''L8''' a significant 1929 development with the same swept volume but cruising at 2,100&nbsp;rpm and delivering 354&nbsp;hp; take off power was 413&nbsp;hp. The output was geared down at ratios between 2.47:1 and 1.44:1 to enhance propeller efficiency.  Only a few were produced, powering early configurations of the [[Junkers G 38]] as outer engines, with two L55s inboard.<ref name="Kay"/>
*'''[[Junkers L88|L88]]''' an upright V-12 built from two L8s on a common crankshaft.

==Applications (L5)==
* [[Albatros L 73]]
* [[Albatros L 75]]
* [[Albatros L 83 Adler]]
* [[Focke-Wulf A 32]]
* [[Heinkel HD 42]]
* [[Heinkel He 50]] 
* [[Junkers F 13]]
* [[Junkers A 35|Junkers A 20]]
* [[Junkers A 35]]
* [[Junkers G 23]]
* [[Junkers G 24]]
* [[Junkers K 30]]
* [[Junkers G 31]]
* [[Junkers W 33]]
* [[Messerschmitt M 24]]
* [[Rohrbach Roland|Rohrbach Ro.VIII]]

==Specifications (Jumo L 5)==
[[File:Junkers L 5 im Technikmuseum Hugo Junkers Dessau 2010-08-06 02.jpg|thumb|Preserved Junkers L 5 engine on display at the [[Technikmuseum Hugo Junkers|Junkers Museum]]]]
{{pistonspecs
|<!-- If you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- Please include units where appropriate (main comes first, alt in parentheses). If data are missing, leave the parameter blank (do not delete it). For additional lines, end your alt units with </li> and start a new, fully formatted line with <li> -->
|ref=<ref name="Kay"/>
|type=6-cylinder liquid-cooled inline
|bore={{convert|160|mm|in|abbr=on|2}}
|stroke={{convert|190|mm|in|abbr=on|2}}
|displacement= {{convert|22.92|l|cuin|abbr=on|2}}
|length={{convert|1750|mm|ft|in|abbr=on|2}}
|diameter=
|width={{convert|650|mm|ft|in|abbr=on|2}}
|height={{convert|1265|mm|in|abbr=on|2}}
|weight={{convert|334|kg|lb|abbr=on|2}} dry
|valvetrain=large single exhaust and single inlet valves driven by a single overhead camshaft shaft and gear driven from the crankshaft
|supercharger=none
|turbocharger=
|fuelsystem=single float, dual venturi carburettor; twin plugs per cylinder, twin magnetos
|fueltype=95 octane (dependent on compression ratio)
|oilsystem=forced<ref name="Kay"/>
|coolingsystem=liquid
|power=Take-off - {{convert|260|kW|hp|abbr=on|1}} at 1,450 rpm<br/>
::::Cruise - {{convert|280|hp|kW|abbr=on|1|disp=flip}}
|specpower=
|compression=7:1
|fuelcon= 61.6&nbsp;kg/h (136 lb/hr)
|specfuelcon=
|oilcon=
|power/weight=1.22&nbsp;kg/kW (2.01 lb/hp) @ cruise rpm
}}

==See also==
{{Aircontent
|related=
|similar engines=

|lists=
* [[List of aircraft engines]]
|see also=
}}

==References==
{{Reflist|refs=
<ref name="Gunston">{{cite book |title= World Encyclopaedia of Aero Engines|last=Gunston|first=Bill|edition=2| year=1989|page=85 |publisher=Patrick Stephens Ltd |location=Wellingborough |isbn=1-85260-163-9|ref=harv}}</ref>

<ref name="Kay">{{cite book |title= Junkers Aircraft & engines 1913-1945|last=Kay|first=Antony| year=2004|volume= |publisher=Putnam Aeronautical Books |location=London |pages =264–5|isbn=0-85177-985-9|ref=harv}}</ref>

<ref name="Kay2">Kay (2004) ''Ibid'', p. 62</ref>
}}

==External links==
{{Commons category}}
* [https://web.archive.org/web/20091027072434/http://www.geocities.com/hjunkers/ju_l5_a1.htm  The Hugo Junkers Homepage]
* http://www.oldengine.org/members/diesel/Duxford/germaer1.htm

{{Junkers Jumo aeroengines}}

[[Category:Aircraft piston engines 1920–1929]]
[[Category:Junkers aircraft engines|L5]]