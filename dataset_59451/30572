{{Infobox baseball biography
|name=Stan Coveleski
|image=Stan Coveleski npcc 13989.jpg
|caption=Coveleski with the Senators
|position=[[Starting pitcher]]
|birth_date={{Birth date|1889|7|13}}
|birth_place=[[Shamokin, Pennsylvania]]
|death_date={{death date and age|1984|3|20|1889|7|13}}
|death_place=[[South Bend, Indiana]]
|bats=Right
|throws=Right
|debutleague = MLB
|debutdate=September 10
|debutyear=1912
|debutteam=Philadelphia Athletics
|finalleague = MLB
|finaldate=August 3
|finalyear=1928
|finalteam=New York Yankees
|statleague = MLB
|stat1label=[[Win–loss record (pitching)|Win–loss record]]
|stat1value=215–142
|stat2label=[[Earned run average]]
|stat2value=2.89
|stat3label=[[Strikeout]]s
|stat3value=981
|teams=
* [[Philadelphia Athletics]] ({{Baseball year|1912}})
* [[Cleveland Indians]] ({{Baseball year|1916}}–{{Baseball year|1924}})
* [[Washington Senators (1901–60)|Washington Senators]] ({{Baseball year|1925}}–{{Baseball year|1927}})
* [[New York Yankees]] ({{Baseball year|1928}})
|highlights=
*[[World Series]] champion ([[1920 World Series|1920]])
* 2× [[List of Major League Baseball annual ERA leaders|AL ERA leader]] (1923, 1925)
* [[List of Major League Baseball annual strikeout leaders|AL strikeout leader]] (1920)
|hoflink = National Baseball Hall of Fame and Museum
|hoftype = National
|hofdate={{Baseball year|1969}}
|hofmethod=Veteran's Committee
}}
'''Stanley Anthony Coveleski''' (born Stanislaus Kowalewski, July 13, 1889&nbsp;– March 20, 1984) was an American [[Major League Baseball]] [[pitcher]] during the 1910s and 1920s who primarily threw the [[spitball]]. In 14 seasons in the [[American League]] (AL), Coveleski pitched for the [[Philadelphia Athletics]], [[Cleveland Indians]], [[Washington Senators (1901–60)|Washington Senators]] and [[New York Yankees]]. In 450 career games, Coveleski pitched 3,082 [[innings pitched|innings]] and posted a [[win–loss record (pitching)|win–loss record]] of 215–142, with 224 [[complete game]]s, 38 [[shutouts in baseball|shutout]]s, and a 2.89 [[earned run average]] (ERA). He was inducted into the [[Baseball Hall of Fame]] in 1969.

Born in [[Shamokin, Pennsylvania]], Coveleski began playing professional baseball in 1908, following in the footsteps of his brother, [[Harry Coveleski]]. He played mostly for the [[Lancaster Red Roses]] until he made his major league debut with the Athletics in 1912. Following three more seasons in the minor leagues, he stayed in the major leagues after signing with the Indians in 1916. During his nine seasons with the Indians, his accomplishments included winning three games during the [[1920 World Series]].

After his time with the Indians ended, Coveleski spent three seasons with the Senators and one with the Yankees before retiring after the 1928 season. He retired to [[South Bend, Indiana]], where he died in 1984. A [[starting pitcher]], Coveleski specialized in throwing the spitball, a pitch where the ball is altered with a foreign substance such as chewing tobacco. It was legal when his career began and outlawed in 1920, but he was one of 17 pitchers permitted to continue throwing the pitch.

==Early years==
Stanislaus Anthony Kowalewski was the youngest of five baseball-playing brothers in the coal-mining community of [[Shamokin, Pennsylvania]].<ref name="kashatus9-10">Kashatus, pp. 9–10.</ref> His oldest brother Jacob died serving in the [[Spanish–American War]]; his other brothers Frank and John also played baseball, but never reached the major leagues.<ref name=sabr>{{cite web|first=Dan|last=Levitt|url=http://sabr.org/bioproj/person/7b589446|title=Stan Coveleski|work=The Baseball Biography Project|publisher=Society for American Baseball Research|accessdate=April 1, 2012}}</ref> His older brother, [[Harry Coveleski]], later won 20 games in a season on three occasions during his major league career.<ref>{{cite news|title=Three Coveleski Boys Sign| work=[[Standard-Examiner|The Ogden Standard]]|date=February 8, 1909|page=5}}</ref><ref>{{cite web|url=http://www.baseball-reference.com/players/c/covelha01.shtml|title=Harry Coveleski Statistics and History|work=Baseball-Reference|accessdate=April 9, 2012}}</ref>

Like many his age in the Shamokin area, Coveleski began work as a "[[breaker boy]]" at a local [[Coal mining|colliery]] at the age of 12.<ref name="kashatus9-10"/> In return for 72 hours of labor per week, Coveleski received $3.75, or about five cents an hour.<ref name="kashatus9">Kashatus, p. 9.</ref> "There was nothing strange in those days about a twelve-year-old Polish kid working in the mines for 72 hours a week at a nickel an hour", he later recalled. "What was strange is that I ever got out of there".<ref name="kashatus9"/> Coveleski was rarely able to play baseball as a child due to his work schedule.<ref name="kashatus9-10"/>

Nevertheless, he worked on his pitching skills during the evenings, when he threw stones at a tin can placed 50 feet away.<ref name="kashatus10">Kashatus, p. 10.</ref> When he was 18 years old, Coveleski's abilities caught the attention of the local semi-professional ball club, which invited him to pitch for them. "When it came to throwing a baseball, why it was easy to pitch", Coveleski recalled. "After all, the plate's a lot bigger than a tin can to throw at".<ref name="kashatus10"/> His baseball career in Shamokin was short-lived; after five games, Coveleski relocated to [[Lancaster, Pennsylvania]].<ref name="kashatus10"/>

==Philadelphia Athletics and minor leagues==
Coveleski signed his first professional contract in 1909 with the minor league [[Lancaster Red Roses]], a club affiliated with the [[Tri-State League]]. Originally reluctant to sign for the club, he only agreed to do so if his older brother John also joined; at that time he [[anglicized]] his name, changing it to Coveleskie, which it would remain throughout his professional career.<ref name=sabr/> During his first trip to Lancaster, he recalled that it was "the first time I ever rode on a train", and he added that he "was too shy to eat in the hotel with the rest of the team".<ref name="kashatus44">Kashatus, p. 44.</ref> In 272 innings of work his first season, Coveleski had a 23–11 win-loss record with an [[earned run average]] of 1.95.<ref name=brm>{{cite web|url=http://www.baseball-reference.com/minors/player.cgi?id=covele001sta|title=Stan Coveleski Minor League Statistics & History|work=Baseball-Reference|accessdate=April 9, 2012}}</ref> He pitched two more seasons for Lancaster, earning a record of 53–38 in 109 appearances through three seasons.<ref name="kashatus44"/>

[[File:Lancaster Red Roses baseball team (team photo, 1909).jpg|thumb|left|200px|1909 Lancaster Red Roses; Stan Coveleski is standing fourth from left]]
In 1912, he pitched for the relocated Lancaster team, the [[Atlantic City Lanks]], where he had a 20–14 record with a 2.53 ERA in 40 appearances, 30 of them starts.<ref name=brm/> In September 1912, manager [[Connie Mack]] signed him to a contract with the [[Philadelphia Athletics]] and brought him to the major leagues, where he made his debut for the Athletics on September 10. Coveleski pitched in five games for the Athletics that season, starting two of them and finishing the season with a 2–1 record and a 3.43 ERA.<ref name=br>{{cite web|url=http://www.baseball-reference.com/players/c/covelst01.shtml|title=Stan Coveleski Statistics and History|work=Baseball-Reference|accessdate=April 9, 2012}}</ref> After the season ended, Mack felt that Coveleski needed more seasoning, and sent him to the [[Spokane Indians]] of the [[Northwestern League]].<ref name=sabr/> Coveleski finished the 1913 season with a 17–20 record and a 2.82 ERA.<ref name=brm/> Around that time, he married Mary Stivetts, and the following season he went 20–15, pitched over 300 innings, and led the league in strikeouts.<ref name=sabr/>

At the time of his debut, the Philadelphia club retained several talented pitchers, including [[Eddie Plank]], [[Chief Bender]], and [[Jack Coombs]]. Coveleski admitted that he "[didn't] know if I could have beat them out for a spot in the rotation."<ref name="kashatus86">Kashatus, p. 86.</ref> After the 1914 season, the [[Portland Beavers]] of the [[Pacific Coast League]] wanted Coveleski, and traded five players to Spokane to acquire him.<ref name=sabr/> While Connie Mack had an agreement with Spokane that Coveleski would be promoted after playing there for a time, the Athletics fell under new ownership in 1913 and lost control of him due to the Athletics' rights expiring.<ref>Macht, pp. 559–560</ref> Coveleski spent his time in Portland learning to throw the [[spitball]]; originally using chewing tobacco, he later used [[alum]].<ref name=sabr/> In his lone season with Portland, he won and lost 17 games, and had a 2.67 ERA.<ref name=brm/> After the season ended, the [[Cleveland Indians]] purchased Coveleski from Portland, and he joined the major league squad in 1916.<ref name=sabr/>

==Cleveland Indians==
[[File:Stan Coveleski.jpg|thumb|with Cleveland in 1918]]
When Coveleski was brought up to the majors, the original intention was to use him as a [[relief pitcher]]. Due to an injury to [[Ed Klepfer]], the Indians used him as a starter early on in the season, and kept him in the role when he performed well.<ref>{{cite news|first=Ed|last=Bang|title=Cleveland Still On Top|url=http://www.la84foundation.org/SportsLibrary/SportingLife/1916/VOL_67_NO_12/SL6712008.pdf|work=[[Sporting Life (magazine)|Sporting Life]]|date=May 20, 1916|page=8}}</ref> He was scheduled to pitch in the first week of the season against his brother Harry, but the matchup never took place at Harry's behest.<ref name=sabr/> Coveleski fought health problems during the season, suffering from tonsillitis in the middle of the year and pitching one game with a fever of {{convert|102|F}}.<ref>{{cite news|first=Herman|last=Nickerson|url=http://www.la84foundation.org/SportsLibrary/SportingLife/1917/VOL_68_NO_20/SL6820006.pdf|title=Affairs in American League|work=Sporting Life|date=January 13, 1917|page=6}}</ref> He finished the season with a 15–13&nbsp;record and a 3.41 ERA in 45 games, 27 of them starts.<ref name=br/> Coveleski had lost 10&nbsp;pounds due to illness during 1916, but recovered during the offseason, gained 20&nbsp;pounds, and appeared healthier by the time the season began.<ref>{{cite news|first=Ed|last=Bang|url=http://www.la84foundation.org/SportsLibrary/SportingLife/1917/VOL_68_NO_22/SL6822007.pdf|title=Cleveland Better Prepared in Case of a Player Strike Than Any Other Club|work=Sporting Life|date=January 27, 1917|page=7}}</ref>

[[File:Stan Coveleskie (1922 baseball card).jpg|thumb|200px|left|Baseball card of Coveleski; he originally spelled his last name as Coveleskie.]]
Coveleski's status as the [[ace (baseball)|ace]] of the staff was demonstrated when he was named the starter for [[Opening Day]] of the [[1917 Cleveland Indians season|1917 season]]; he beat the [[Detroit Tigers]] 6–4 on April 11.<ref>{{cite web|url=http://www.retrosheet.org/boxesetc/1917/VCLE01917.htm|title=The 1917 Cleveland Indians Game Log|publisher=[[Retrosheet]]|accessdate=May 5, 2012}}</ref> He improved statistically during the 1917 season, winning 19 games and losing 14 with an ERA of 1.81 and a career high 133 strikeouts.<ref name=br/> On September 19, Coveleski pitched a [[one-hitter (baseball)|one-hitter]] against the New York Yankees; the only hit came from [[Fritz Maisel]] in the seventh inning.<ref>Schneider, p. 156.</ref> Coveleski continued to improve during the war-shortened [[1918 Cleveland Indians season|1918 season]]. His outings that year included pitching a complete game against the [[New York Yankees]], where he pitched 19 innings, allowing two runs as the Indians won, 3–2.<ref>{{cite news|url=https://news.google.com/newspapers?id=UaZVAAAAIBAJ&sjid=jOADAAAAIBAJ&pg=4490,5720146|title=Cleveland Wins 19-Inning Game|work=[[The Spokesman-Review]]|date=May 25, 1918|page=14}}</ref> He finished the season with a 22–13 record, a 1.82 ERA, and 311&nbsp;innings pitched in 38 games, 33 of them starts; his wins and ERA were both second in the American League to [[Walter Johnson]].<ref name=sabr/><ref name=br/> In 1919, Coveleski pitched in 43 games, starting 34, and had a 24–12 record and an ERA of 2.61.<ref name=br/>

At the beginning of the [[1920 Cleveland Indians season|1920 season]], the spitball was banned by Major League Baseball. As a current spitball pitcher, Coveleski was [[grandfather clause|grandfathered]] in, and was allowed to continue using the pitch until his retirement.<ref name="kashatus86"/> He won his first seven starting appearances of the season, but on May 28, his wife died suddenly, and he was given some time off to mourn, returning to pitching two weeks later.<ref name=sabr/> Nearly three months later, he was the starting pitcher against the New York Yankees on August 16. In the game, Coveleski hit a [[sacrifice fly]] to help the Indians win, 4–3, but it was best remembered as the game where Yankees pitcher [[Carl Mays]] hit [[Ray Chapman]], leading to the only death in MLB history from a pitch.<ref>Sowell, pp. 172–174.</ref> He finished the 1920 regular season with 24 wins, 14 losses, a 2.49 ERA, and 133 strikeouts; he led the AL in strikeouts and finished second in ERA to [[Bob Shawkey]].<ref name=br/>

Coveleski's helped the Indians to win the [[List of American League pennant winners|AL pennant]] and play in the [[1920 World Series]] against the [[Brooklyn Robins]]. Coveleski was the star of the 1920 World Series, in which he pitched three complete game victories. He pitched the first game against [[Rube Marquard]], and allowed one run and five hits in a 3–1 Cleveland victory.<ref name=ws1920>{{cite web|url=http://www.baseball-reference.com/postseason/1920_WS.shtml|title=1920 World Series|work=Baseball-Reference.com|accessdate=May 27, 2012}}</ref> Four days later, he pitched game four, again allowing one run and five hits in a 5–1 win.<ref name=ws1920/> In game seven, the final one of the series, Coveleski threw a complete game [[shutouts in baseball|shutout]] with five hits against fellow spitballer [[Burleigh Grimes]]; the 3–0 victory gave the Indians the first World Series championship in franchise history.<ref name=ws1920/> Coveleski had an ERA of 0.67, which remains a World Series record.<ref name="kashatus86"/>

After spending the offseason hunting with [[Smoky Joe Wood]],<ref>Sowell, p. 279.</ref> Coveleski returned to the Indians in 1921, and throughout the season, the Indians battled the Yankees for first in the American League. On September 26, the two teams faced off, but Coveleski failed to make it past the third inning; the Yankees won 8–7 to ensure they won the pennant.<ref>{{cite news|url=https://news.google.com/newspapers?id=uGRVAAAAIBAJ&sjid=tj4NAAAAIBAJ&pg=6268,2064994|title=25,000 Crowd Polo Grounds For The Game|work=[[The Miami News]]|date=September 27, 1921|page=9}}</ref> Coveleski pitched 315 innings in 1921, matching his career high from the year before, and had a 23–13 record and a 3.37 ERA.<ref name=br/> The following season, Coveleski married Frances Stivetts, the sister of his late wife.<ref name=sabr/> While he did cause the Yankees to move out of first place after winning an August 23 game against them, 4–1,<ref>{{cite news|title=Coveleskie Proves Too Much|work=[[Christian Science Monitor]]|date=August 24, 1922|page=12}}</ref> it was his last game of the season. He finished the year with a 17–14 record, the first time since 1917 he did not have 20 wins, and a 3.32 ERA.<ref name=br/>

Early on in the [[1923 Cleveland Indians season|1923 season]], Coveleski pitched 27 straight scoreless innings, winning three consecutive games during that time.<ref>{{cite news|title=Coveleskie Gains Another Triumph|work=[[The New York Times]]|date=May 1, 1923}}</ref><ref name=log23>{{cite web|url=http://www.baseball-reference.com/players/gl.cgi?id=covelst01&t=p&year=1923|title=Stan Coveleski 1923 Pitching Gamelogs|work=Baseball-Reference.com|accessdate=June 7, 2012}}</ref> However, he won less frequently as the season wore on, losing three straight games in mid-August. His last game came on August 15, with over a month left in the season.<ref name=log23/> Coveleski finished the season with a 13–14 record, his first season with a losing record. Despite that, he had an ERA of 2.76 and five shutouts, both of which led the AL.<ref name=br/> In 1924, Coveleski struggled, and at the end of May, he had four losses and an ERA of 6.49.<ref>{{cite web|url=http://www.baseball-reference.com/players/gl.cgi?id=covelst01&t=p&year=1924|title=Stan Coveleski 1924 Pitching Gamelogs|work=Baseball-Reference.com|accessdate=June 7, 2012}}</ref> By the end of the season, he had a 15–16 record and a 4.04 ERA.<ref name=br/> In December 1924, after nine years pitching for Cleveland, Coveleski was traded to the [[Washington Senators (1901–60)|Washington Senators]] for pitcher [[By Speece]] and outfielder [[Carr Smith]].<ref name=br/>

Despite Coveleski's success in Cleveland, he was not a fan of playing there; he stated that he "didn't like the town. Now the people are all right, but I just didn't like the town."<ref name=sabr/> He also stated that it began to affect his performance on the mound, and that he began to get "lazy" from being with the club so long.<ref name=sabr/> He did, however, have praise for his catcher: "The best thing that happened to me there was pitching to [[Steve O'Neill]]. He caught me for nine years in Cleveland and knew me so well he didn't even need to give me a sign".<ref name="kashatus86"/>

==Washington Senators and New York Yankees==
Due to the acquisition of Coveleski, combined with winning the [[1924 World Series]], the Washington Senators were considered favorites to win the AL in 1925.<ref>{{cite news|url=https://news.google.com/newspapers?id=CqYhAAAAIBAJ&sjid=R5oFAAAAIBAJ&pg=4078,1149734|title=Senators and Yankees Favored to Cop Pennant|work=[[Reading Eagle]]|date=February 6, 1925|page=24}}</ref> During his first season in Washington, Coveleski bounced back from his [[1924 Cleveland Indians season|1924 season]], and by mid-July, critics regarded his success as the biggest surprise in baseball; Cleveland considered him to be past his best.<ref>{{cite news|url=https://news.google.com/newspapers?id=gxhkAAAAIBAJ&sjid=EHsNAAAAIBAJ&pg=2452,2170063|title=Tight race in the Majors|work=[[Calgary Herald]]|date=July 20, 1925|page=12}}</ref> He won 20 games and lost five that year, and his ERA of 2.84 led the AL.<ref name=sabr/> Coveleski also finished 12th in MVP voting that year.<ref name=br/> The Senators won the AL and were to face the [[Pittsburgh Pirates]] in the [[1925 World Series]], but Coveleski suffered from sore back muscles late in the season.<ref>{{cite news|url=https://news.google.com/newspapers?id=pZ1cAAAAIBAJ&sjid=GFgNAAAAIBAJ&pg=1745,5697574|title=Senators and Pirates Are Evenly Matched for Series|work=[[Youngstown Vindicator]]|date=October 3, 1925|page=11}}</ref> Coveleski pitched two games in the World Series. In game two, he faced [[Vic Aldridge]] in a pitcher's duel; the teams were tied at one apiece in the eighth inning, but a Pittsburgh home run led to a 3–2 loss.<ref>Thomas, pp. 275–276.</ref> Aldridge and Coveleski faced off again in game five, and Coveleski allowed four runs in under seven innings, leading to a 6–3 Pirates win.<ref>Thomas, p. 279.</ref> The Senators lost the series in seven games, and he finished with a 3.77 ERA, five walks, three strikeouts, and two of the Senators' four losses.<ref name=br/>

Coveleski continued to pitch for Washington during the [[1926 Washington Senators season|1926 season]]. His performances that season included a 2–0 victory against the Boston Red Sox on August 31, in which the game was finished in only 78 minutes.<ref>{{cite news|url=https://news.google.com/newspapers?id=W2EyAAAAIBAJ&sjid=b68FAAAAIBAJ&pg=1551,4903748|title=Cards Lead League Beat Pirates Two|work=[[The Owosso Argus-Press]]|date=September 1, 1926|page=14}}</ref> Coveleski finished the season with 14 wins, 11 losses, 3 shutouts, and a 3.12 ERA in 36 games.<ref name=br/> To start the [[1927 Washington Senators season|1927 season]], due to an injury to Walter Johnson, Coveleski became the [[List of Washington Senators Opening Day starting pitchers|Senators' Opening Day starter]] against the Red Sox; he won the game 6–2.<ref>Thomas, p. 293.</ref> However, his performance declined due to "a chronically sore arm", which limited his playing time that season.<ref name="kashatus88">Kashatus, p. 88.</ref> Due to his sore arm, the Senators released him unconditionally on June 17, 1927.<ref name=sabr/> He finished the season with a 2–1 record and a 3.14 ERA in five games.<ref name=br/>

On December 21, 1927, Coveleski signed with the New York Yankees in an attempt at a comeback. In his final season, he posted a 5–1 record with a 5.74 ERA in 12 appearances.<ref name="kashatus88"/> Coveleski failed to regain his form, however, pitching his last game on August 3, and after the signing of [[Tom Zachary]], manager [[Miller Huggins]] released Coveleski.<ref>{{cite news|url=https://news.google.com/newspapers?id=U2hgAAAAIBAJ&sjid=EXINAAAAIBAJ&pg=4843,1692795|title=Yankees Sign Zachary and Release Covey|work=[[Rochester Evening Journal]]|date=August 24, 1928|page=10}}</ref> He retired from the game later that year.<ref name=br/>

==Later life and legacy==
In 1929, after leaving major league baseball, Coveleski relocated to [[South Bend, Indiana]]. There, he ran Coveleski Service Station for a time, but closed the business during the [[Great Depression]]. He became a popular member of the community in South Bend, providing free pitching lessons to local youths in a field behind his garage.<ref name="kashatus126">Kashatus, p. 126.</ref> After his playing career ended, he dropped the "e" at the end of his name, as he never corrected anyone if his last name was incorrectly spelled.<ref>Nemec, p. 98.</ref> In 1969, Coveleski was named to the [[National Baseball Hall of Fame and Museum|Baseball Hall of Fame]] by the Veterans' Committee alongside 1920s pitcher [[Waite Hoyt]]. Of his introduction into the Hall he said, "I figured I'd make it sooner or later, and I just kept hoping each year would be the one."<ref name=sabr/> His health declined in later years, and he was eventually admitted to a local nursing home, where he died on March 20, 1984 at the age of 94.<ref name=sabr/>

In addition to Coveleski's induction into the Baseball Hall of Fame,<ref name="kashatus88"/> he was inducted into the [[National Polish-American Sports Hall of Fame]] in 1976.<ref>{{cite web|url=http://polishsportshof.com/inductees/baseball/stan-covaleski/|title=Stan Coveleski|publisher=National Polish-American Hall of Fame|accessdate=June 11, 2012}}</ref> In 1984, the [[Stanley Coveleski Regional Stadium|minor league baseball stadium]] in South Bend, Indiana, was named in his honor.<ref name="kashatus126"/> Coveleski was interviewed by [[Lawrence Ritter]] for his 1966 book ''[[The Glory of Their Times]]'', a series of interviews with players of the early 20th century.<ref name=sabr/> To fellow ballplayers, Coveleski was considered "taciturn and ornery" on days when he was scheduled to pitch, but was otherwise friendly with a lively sense of humor.<ref name=sabr/>

Coveleski had 216 wins and 142 losses with a 2.89 ERA in 450 games, 385 of them starts in a 14-year career. He had 224 [[complete game]]s, 38 shutouts, 981 strikeouts, and pitched 3,082 total innings.<ref name=br/> His control was highly regarded. He never considered himself a strikeout pitcher, and it was not unusual for him to pitch a complete game having thrown 95 pitches or less.<ref name=sabr/> He once pitched seven innings of a game where every pitch was either a hit or a strike.<ref>Sowell, p. 99.</ref> In 2001, baseball statistician [[Bill James]] ranked Coveleski 58th among the all-time greatest major league pitchers.<ref>James, p. 882.</ref>

==See also==
{{Portal|Baseball|Biography}}
* [[List of Major League Baseball annual ERA leaders]]
* [[List of Major League Baseball career wins leaders]]
* [[List of Major League Baseball annual strikeout leaders]]
{{Clear}}

==Notes==
{{Reflist|30em}}

==References==
*{{cite book|first=Bill|last=James|authorlink=Bill James|title=The New Bill James Historical Baseball Abstract|year=2001|publisher=[[Simon & Schuster]]|isbn=978-0-7432-2722-3}}
*{{cite book|last=Kashatus|first=William C.|title=Diamonds in the Coalfields: 21 Remarkable Baseball Players, Managers, and Umpires from Northeast Pennsylvania|publisher=[[McFarland & Company|McFarland]]|year=2002|isbn=978-0-7864-1176-4}}
*{{cite book|first=Norman Lee|last=Macht|title=Connie Mack and the Early Years of Baseball|year=2007|publisher=[[University of Nebraska Press]]|isbn=978-0-8032-3263-1}}
*{{cite book|last=Nemec|first=David|authorlink=David Nemec|title=Players of Cooperstown: Baseball's Hall of Fame|year=1995|publisher=Publications International|isbn=978-0-7853-1438-7}}
*{{cite book|last=Schneider|first=Russell|title=The Cleveland Indians Encyclopedia|year=2004|publisher=[[Skyhorse Publishing|Sports Publishing LLC]] |isbn=978-1-58261-840-1}}
*{{cite book|last=Sowell|first=Mike|title=The Pitch That Killed|year=1989|publisher=[[Ivan R. Dee]]|isbn=978-1-56663-551-6}}
*{{cite book|last=Thomas|first=Henry W.|title=Walter Johnson: Baseball's Big Train|year=1995|publisher=[[Bison Books]]|isbn=978-0-8032-9433-2}}

==External links==
{{Baseballstats|br=c/covelst01|brm=covele001sta}}
{{bbhof|coveleski-stan}}
*[http://polishsportshof.com/inductees/baseball/oscar-bielaski/ National Polish-American Sports HOf profile]

{{1920 Cleveland Indians}}
{{Cleveland Indians Opening Day starting pitchers}}
{{AL ERA champions}}
{{AL strikeout champions}}
{{1969 Baseball HOF}}
{{Baseball Hall of Fame members}}
{{New York Yankees HOF}}

{{featured article}}

{{DEFAULTSORT:Coveleski, Stan}}
[[Category:1889 births]]
[[Category:1984 deaths]]
[[Category:American people of Polish descent]]
[[Category:American League ERA champions]]
[[Category:American League strikeout champions]]
[[Category:Atlantic City Lanks players]]
[[Category:Baseball players from Pennsylvania]]
[[Category:Cleveland Indians players]]
[[Category:Lancaster Red Roses players]]
[[Category:Major League Baseball pitchers]]
[[Category:National Baseball Hall of Fame inductees]]
[[Category:New York Yankees players]]
[[Category:Philadelphia Athletics players]]
[[Category:Portland Beavers players]]
[[Category:Spokane Indians players]]
[[Category:Washington Senators (1901–60) players]]
[[Category:People from Shamokin, Pennsylvania]]