[[File:Loening Solberg.jpg|thumb|300 px| Loening C-2-C Air Yacht used by Thor Solberg and Paul Oscanyon. (Norwegian Museum of Science and Technology)]]

'''Thor Solberg''' (March 28, 1893 – February 26, 1967) was a Norwegian-born aviation pioneer who made the first successful flight from the United States of America to Norway in 1935.<ref name = "nj">{{Citation|author = Curtis Leeds|url = http://www.nj.com/hunterdon-county-democrat/index.ssf/2010/09/75_years_after_his_dad_did_it.html|newspaper=[[The Hunterdon County Democrat]]|title = 75 years after his Dad did it, Thor Solberg of Readington Twp. will retrace his path flying to Norway|date = September 19, 2010|accessdate = April 16, 2015}}</ref><ref name = "airport">{{Citation|author = Meg Goldewski|work = [[General Aviation News]]|url = http://generalaviationnews.com/2014/06/29/kn51-in-the-middle-of-a-tug-of-war/|title = KN51 in the middle of a tug of war|date = June 29, 2014|accessdate = April 16, 2015}}</ref> He made the journey, which started in New York City, in an open-cockpit single-engine aircraft with no landing instruments. For this reason, he was restricted to an altitude of {{convert|1000|to|10000|ft|m}}.<ref name = "nj"/> Solberg also founded the [[Solberg-Hunterdon Airport]] in [[New Jersey]].

==Early life==
Thor Simonsen Solberg was born on his family's farm (''Solberg på Årebrot'') at [[Florø]] in [[Sogn og Fjordane]], Norway.<ref name = "review">{{Citation|author = Henry Goddard Leach|url = https://books.google.com/?id=ApR9AAAAMAAJ&dq=%22Emerson%2C+of+Auburn%2C+New+York.+He+purchased+%22&q=%22Thor+Solberg%22|title = The American-Scandinavian Review, Volume 24|page=58|year = 1936|accessdate= }}</ref><ref name = "death"/> He had ten brothers and sisters, including Halfdan and Lars Solberg.<ref name = "review"/><ref>{{Citation|author = Rob Mulder|url = http://www.europeanairlines.no/thor-solberg-and-his-expeditions/|title = Thor Solberg and his expeditions|date = June 16, 2010|accessdate = April 16, 2015}}</ref> Solberg was interested in motors and aviation from an early age and was a "daring" motorcyclist in his youth. He was also interested in artistic picture framing in his youth.<ref name = "norseman"/><ref name = "norwegians"/> Solberg participated in a number of motorcycling speed events during his early years. He also rode from [[Oslo]] to [[Paris]] in 48 hours in 1923.<ref name = "review"/>

==Early flying career==
Solberg received his pilot's training in Germany in the years following [[World War I]]. He took a pilot's license in 1919 and became one Norway's aviation pioneers.<ref name = "norseman">{{Citation|url = https://books.google.com/?id=PS9jAAAAIAAJ&q=%22He+was+the+first+to+fly+from+the+United+%22&dq=%22He+was+the+first+to+fly+from+the+United+%22|title = The Norseman|page = 51|year = 1966|accessdate = April 19, 2015}}</ref>

Solberg  who was inspired by aviators such as [[Charles Lindbergh]]. Solberg was to be the first person to fly from Norway to the United States solo, receiving some aid and advice from [[Roald Amundsen]].<ref name = "review"/><ref name = "death"/><ref name = "norseman"/>

Solberg lived to the United States from 1925 or 1928. Upon arriving in the United States, Solberg worked at various jobs, such as proprietor of an art studio and picture framing store in [[Brooklyn]], while planning his flight.<ref name = "death"/><ref name = "norseman"/> Solberg flew over much of the United States in his [[Bellanca CH-200 Pacemaker]] while preparing for the flight.<ref name = "review"/>

==Flight to Norway==
In preparation for his flight to Norway, Solberg practiced [[blind flying]], studied his route, and created his own maps of it. He received financial and technical support from two of his brothers, [[Bernt Balchen]], and several other people.<ref name = "review"/>

After managing to convince the [[Enna Jettick Shoe Company]] to fund his flight, Solberg made his first attempt to fly from the United States to Norway with Carl Petersen in a [[Bellanca K]], named ''Enna Jettick'', (formerly ''Roma'' from a failed US to Italy flight), on August 23, 1932, but that attempt failed. They encountered fog and snow near [[Newfoundland]] and were forced to make a crash landing. Shortly afterwards, they were rescued by nearby fishing boats.<ref name = "ref1"/>

On July 18, 1935, Solberg made a second attempt with a different plane, an [[amphibious plane|amphibious]] single-engine [[Loening C-2-C Air Yacht]] that he named ''[[Liev Eiriksson]]''. On this flight,  Paul C. Oscanyon, who had worked for Eastern Airlines,  joined the flight as radio operator.<ref>{{cite web|url= http://www.europeanairlines.no/thor-solberg-and-his-expeditions/
|title=Thor Solberg and his expeditions|publisher= europeanairlines.no|author= Rob Mulder|date= June 16, 2010|accessdate= March 25, 2016}}</ref>
They departed from [[Floyd-Bennett Field]] and made four stops at locations between the United States and Norway before landing in Norway on 16 August 1935 after passing through Labrador, [[Greenland]], [[Iceland]], the [[Faroe Islands]], and [[Bergen, Norway|Bergen]].<ref name = "norseman"/><ref name = "norwegians"/><ref name = "ref1"/><ref name = "ref1">{{Citation|author = Joshua Stoff|url = https://books.google.com/?id=MxkdHbUPklgC&pg=PA83&dq=%22Thor+Solberg%22#v=onepage&q=%22Thor%20Solberg%22&f=false|title = Transatlantic Flight: A Picture History, 1873–1939|year = 2013|accessdate = April 19, 2015|isbn = 9780486148007}}</ref> Solberg's course roughly followed the path that [[Leif Erikson]]'s voyage took.<ref>{{Citation|url = http://airandspace.si.edu/collections/artifact.cfm?object=nasm_A19640009000|title = Silver commemorative spoon; Thor Solberg|accessdate = April 19, 2015}}</ref> Upon his arrival in Norway, the [[Haakon VII of Norway|King of Norway]] gave Solberg a gold medal.<ref name = "nj"/>

==Later flying career==
In 1939, Solberg founded the [[Solberg-Hunterdon Airport]] in central [[New Jersey]].<ref>{{Citation|magazine = [[Flying (magazine)|Flying]]|url = https://books.google.com/?id=ylN4Bu-A-xcC&pg=PA45&dq=%22Thor+Solberg%22#v=onepage&q=%22Thor%20Solberg%22&f=falseM|title = Not In My Backyard|publisher=Flying Magazine|page=45|accessdate = April 19, 2015|date = July 2000}}</ref> 
In 1941, he received permission from the [[Readington Township, New Jersey|Readington Township]] Committee to operate a commercial airport. The airport was also used as a training facility during [[World War II]] where Solberg trained approximately 5,000 aviators.<ref name = "airport"/>

==Personal life, death, and legacy==
Solberg was a Knight of the [[Order of St. Olav]].<ref name = "norseman"/> He was also a notable member of the Norsemen Lodge, an organization which was composed of people of Norwegian birth or descent. Additionally, he was a life member of the Explorers' Club in New York.<ref name = "norwegians">{{Citation|author = A.N. Rygg|url = https://archive.org/stream/norwegiansinnewy00rygg/norwegiansinnewy00rygg_djvu.txt|title = Full text of "Norwegians in New York, 1825–1925"|accessdate = April 19, 2015}}</ref>

Solberg died in [[Branchburg, New Jersey]] at the age of 73 and was buried at the churchyard in Florø.<ref name = "death">{{Citation|author = Henry Goddard Leach|url = https://books.google.com/?id=nMsgAAAAMAAJ&dq=%22America+in+1928+after+having+been+%22&q=%22Thor+Solberg|title = The American-Scandinavian Review, Volumes 54–55|date = 1960s|accessdate = April 19, 2015}}</ref>

==Legacy==
*The Loening C-2-C Air Yacht piloted by Thor Solberg in 1935 is presently on display at the [[Norwegian Museum of Science and Technology]] in [[Oslo, Norway]].
*A statue of Solberg was erected at the [[Florø Airport]] in 1985 to commemorate the 50th anniversary of his flight.<ref>{{cite web
|url= http://www.coast-alive.eu/content/thor-solberg-statue|title=Thor Solberg statue|publisher=Florø Lufthamn (Airport|accessdate= March 25, 2016}}</ref>

==See also==
*[[Solberg–Hunterdon Airport]]
*[[List of aviation pioneers]]

==References==
{{Reflist|2}}

==External links==
* [https://www.flickr.com/photos/sdasmarchives/4589639271/  Bellanca K ''Roma'' re-named ''Enna Jettick'']
* [https://www.flickr.com/photos/70364571@N05/8537120549  ''Leif Eiriksson'']  [[Norwegian Museum of Science and Technology]] 
*[http://www.solbergairport.com/ Solberg Airport]

{{DEFAULTSORT:Solberg, Thor}}
[[Category:1893 births]]
[[Category:1967 deaths]]
[[Category:People from Flora, Norway]]
[[Category:Norwegian expatriates in Germany]]
[[Category:Aviation pioneers]]
[[Category:Norwegian aviators]]
[[Category:Norwegian emigrants to the United States]]
[[Category:Recipients of the St. Olav's Medal]]