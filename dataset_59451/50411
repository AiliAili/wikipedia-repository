{{Infobox journal
| title = Albany Law Journal of Science and Technology
| discipline =  [[Law]]
| abbreviation = Albany Law J. Sci. Technol.
| editor = Erin Ginty
| publisher = [[Albany Law School]]
| country = United States
| frequency = Triannual
| history = 1990-present
| openaccess =
| impact =
| impact-year =
| website = http://www.albanylawjournal.org
| ISSN = 1059-4280
| LCCN = 91658615
| OCLC = 23860428
}}
The '''''Albany Law Journal of Science and Technology''''' (''[[Bluebook]]'' abbreviation: ''Alb. L.J. Sci. & Tech.'') is a triannual [[law journal]] edited by students at [[Albany Law School]].<ref name="About">{{cite web |title=About |work=Albany Law Journal of Science and Technology |url=http://www.albanylawjournal.org/about/Pages/default.aspx |accessdate=28 March 2015}}</ref> It was established in 1990 and covers legal issues involving science and technology.<ref name="Journals & Publications">{{cite web |url=http://www.albanylaw.edu/academics/Pages/Journals.aspx |title=Journals & Publications |publisher=Albany Law School |accessdate=29 March 2015}}</ref> The Volume 27 [[editor-in-chief]] is Erin Ginty.<ref name="Current Membership">{{cite web |title= Current Membership |work=Albany Law Journal of Science and Technology |url=http://www.albanylawjournal.org/about/Pages/current-membership.aspx |accessdate=28 March 2015}}</ref> The journal also organizes an annual symposia.

== Membership ==
Members are students at Albany Law School. Students become eligible for journal membership upon completion of their first year of law school. Offers of membership are extended based on student class standing or on the results of a writing competition jointly administered by the school's three student-edited journals.

== Notable symposia ==
* ''Facebook Firing: The Intersection of Social Media, Employment, & Ethics'' (2013)<ref>{{cite web |title=Facebook Firing: The Intersection of Social Media, Employment, & Ethic |url=http://www.albanylaw.edu/facebookfiring/Pages/default.aspx |publisher=Albany Law School}}</ref>
* ''Building a High-Tech, 21st Century Economy'' (2015)<ref>{{cite web |title=Albany Law School conference asks how to build a high tech economy |url=http://www.bizjournals.com/albany/news/2015/03/18/albany-law-school-conference-asks-how-to-build-a.html?page=all |work=[[Albany Business Review]]}}</ref>

== Past Editors-in-Chief ==
* Vol. 26 - James Faucher II (2016)
* Vol. 25 - Gary J. Repke, Jr. (2015)
* Vol. 24 - Elizabeth A. Cappillino (2014)
* Vol. 23 - Nadia Isobel Arginteanu (2013)
* Vol. 22 - Christina M. French (2012)
* Vol. 21 - Caitlin Donovan (2011)
* Vol. 20 - Andrew Wilson (2010)
* Vol. 19 - William Q. Lowe (2009)

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.albanylawjournal.org}}

[[Category:American law journals]]
[[Category:Law and public policy journals]]
[[Category:Triannual journals]]
[[Category:English-language journals]]
[[Category:Publications established in 1990]]
[[Category:Law journals edited by students]]
[[Category:Science and law]]


{{law-journal-stub}}