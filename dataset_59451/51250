{{primary sources|date=July 2010}}
{{Infobox organization
| name = European Association of Geoscientists and Engineers
| logo = [[File:European Association of Geoscientists and Engineers (logo).png]]
| type = Professional Organization
| founded_date = 1951
| location = <!-- this parameter modifies "Headquarters" -->
| origins =
| key_people =
| area_served = Worldwide
| focus =
| method = Events, Publications, Education
| num_volunteers =
| num_employees = 70
| num_members = 19,000
| Non-profit_slogan =
| homepage = [http://www.eage.org/ www.eage.org]
| tax_exempt =
| footnotes =
}}
The '''European Association of Geoscientists and Engineers''' ('''EAGE''') is a [[Transdisciplinarity|multi-disciplinary]] [[professional association]] for [[Earth science|geoscientists]] and [[engineer]]s. It was founded in 1951 and has a worldwide membership.<ref>{{cite web|url=http://www.eage.org/?evp=1932 |title=In Short - EAGE (European Association of Geoscientists & Engineers) |publisher=EAGE |date= |accessdate=2012-08-30}}</ref> The association changed name from European Association of Exploration Geophysicists in 1995.<ref>{{cite web|url=http://www.eage.org/index.php?evp=8414 |title=History - EAGE (European Association of Geoscientists & Engineers) |publisher=EAGE |date= |accessdate=2015-07-01}}</ref> The association is intended for persons that are studying or otherwise professionally involved in [[geophysics]], [[petroleum exploration]], [[geology]], [[reservoir engineering]], [[mining]], and [[civil engineering]]. EAGE operates two divisions: the Oil & Gas Geoscience Division and the Near Surface Geoscience Division.<ref>{{cite web|url=http://www.eage.org/index.php?evp=1933 |title=Divisions - EAGE (European Association of Geoscientists & Engineers) |publisher=EAGE |date= |accessdate=2012-08-30}}</ref> The head office of EAGE is located in the Netherlands, with regional offices in [[Moscow]], [[Dubai]], and [[Kuala Lumpur]].

== Activities ==
The main activities of EAGE are:
* The organisation of conferences, exhibitions, and workshops
* Publications (journals, books)
* Educational Programmes (short courses, lectures)
* Student Programmes
* Recruitment

== Events ==
Every year, EAGE organizes a large number of conferences, exhibitions and lecture tours for geoscience engineers and professionals.  The largest of these events is the EAGE Annual Conference and Exhibition, attracting almost 6,000 visitors from all over the world. Throughout the year, many different and more informal workshops are scheduled on various topics such as: Passive Seismic, Tar Mats, Tight Gas, Borehole Geophyics, Land Seismic, CO2 storage, and more.<ref>{{cite web|url=http://www.eage.org/index.php?evp=4432 |title=Workshops - EAGE (European Association of Geoscientists & Engineers) |publisher=EAGE |date= |accessdate=2012-08-30}}</ref>

== Publications ==
EAGE's flagship [[magazine]] is ''[[First Break]]''. In addition, EAGE publishes four [[scientific journal]]s: ''[[Geophysical Prospecting]]'', ''[[Near Surface Geophysics]]'', ''[[Petroleum Geoscience]]'', and ''[[Basin Research]]''. EAGE also publishes several books per year.

== See also ==
* [[List of geoscience organizations]]
* [[Society of Exploration Geophysicists]]
* [[Society of Petroleum Engineers]]
* [[American Association of Petroleum Geologists]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.eage.org}}

[[Category:Geology societies]]
[[Category:Geophysics societies]]
[[Category:Houten]]
[[Category:International professional associations of Europe]]
[[Category:Organisations based in Utrecht (province)]]
[[Category:Petroleum engineering]]
[[Category:Professional associations based in the Netherlands]]
[[Category:Scientific organisations based in the Netherlands]]
[[Category:Scientific organizations established in 1951]]