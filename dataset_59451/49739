{{about|the novel|the film adaptation|Moonfleet (1955 film)}}
{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{multiple issues|
{{refimprove|date=April 2012}}
{{more footnotes|date=April 2012}}
}}

{{Infobox book | <!-- See Wikipedia:WikiProject_Novels or Wikipedia:WikiProject_Books -->
| name          = Moonfleet
| title_orig    =
| translator    =
| image         = <!--prefer 1st edition-->
| caption       =
| author        = [[J. Meade Falkner]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = [[Adventure novel]]
| publisher     = [[Edward Arnold (publisher)|Edward Arnold]]
| release_date  = 1898
| english_release_date =
| media_type    = Print (hardback & paperback)
| pages         = 305 (+ 32 ads)<ref>[http://www.harringtonbooks.co.uk/winter0607.html/ Adrian Harrington Rare Books] Winter Catalogue 2006–2007</ref>
| isbn          =
| preceded_by   =
| followed_by   =
}}
'''''Moonfleet''''' is a tale of [[smuggling]], royal treasure and shipwreck by the English novelist [[J. Meade Falkner]], first published in 1898. The book was extremely popular among children worldwide up until the 1970s, mostly for its themes of adventure and gripping storyline. It remains a popular story widely read and is still sometimes studied in schools.

==Plot summary==
In 1757, Moonfleet is a small village near the sea in the south of England. It gets its name from a formerly prominent local family, the Mohunes, whose [[coat of arms]] includes a symbol shaped like a capital 'Y'. John Trenchard is an orphan who lives with his aunt, Miss Arnold. Other notable residents are the [[sexton (office)|sexton]] Mr Ratsey, who is friendly to John; Parson Glennie, the local clergyman who also teaches in the village school; Elzevir Block, the landlord of the local inn, called the ''Mohune Arms'' but nicknamed the ''Why Not?'' because of its sign with the Mohune 'Y'; and Mr Maskew, the unpopular local [[magistrate]] and his beautiful daughter, Grace.

Village legend tells of the notorious Colonel John "Blackbeard" Mohune who is buried in the family crypt under the church. He is reputed to have stolen a diamond from King [[Charles I of England|Charles I]] and hidden it. His ghost is said to wander at night looking for it and the mysterious lights in the churchyard are attributed to his activities.

As the main part of the story opens, Block's youthful son, David, has just been killed by Maskew during a raid by the Maskew and other authorities on a [[smuggler|smuggling]] boat. One night a bad storm hits the village and there is a flood. While attending the Sunday service at church, John hears strange sounds from the crypt below. He thinks it is the sound of the coffins of the Mohune family. The next day, he finds Elzevir and Ratsey against the south wall of the church. They claim to be checking for damage from the storm, but John suspects they are searching for Blackbeard's ghost.

Later John finds a large [[sinkhole]] has opened in the ground by a grave. He follows the passage and finds himself in the crypt with coffins on shelves and casks on the floor. He realises his friends are smugglers and this is their hiding place. He has to hide behind a coffin when he hears Ratsey and Elzevir coming. When they leave, they fill in the hole, inadvertently trapping him. John finds a locket in a coffin which holds a piece of paper with verses from the Bible. John eventually passes out after drinking too much of the wine while trying to quench his thirst, having not eaten or drunk for days. Later he wakes up in the ''Why Not?'' inn - he has been rescued by Elzevir and Ratsey. When he is better, he returns to his aunt's house, but she, suspecting him of drunken behaviour, throws him out. Fortunately, Elzevir takes him in.

But when Block's lease on the ''Why Not?'' comes up for renewal, Maskew bids against him in the auction and wins. Block must leave the inn and Moonfleet but plans one last smuggling venture. John feels honour-bound to go with him, and sadly, says goodbye to Grace Maskew, whom he loves and has been seeing in secret, and gets his mother's prayer book as a good luck charm. The excisemen and Maskew are aware of the planned smuggling run but do not know exactly where it will occur. During the landing Maskew appears and is caught by the smugglers. Elzevir is bent on vengeance for his son by killing Maskew, and while the rest land the cargo and leave, he and John keep watch over Maskew. Just as Block prepares to shoot Maskew the excisemen attack. They wound John and unintentionally kill Maskew. Block carries John away to safety and they hide in some old quarries. While there, John inadvertently finds out that the verses from Blackbeard's locket contain a code that will reveal the location of his famous diamond.

Once John's wound heals, he and Block decide to recover the diamond from [[Carisbrooke Castle]]. After a suspenseful scene in the well where the jewel is hidden, they succeed in escaping to Holland where they try to sell it to a Jewish diamond merchant named Crispin Aldobrand. The merchant cheats them, claiming the diamond is fake. Elzevir falls for the deceit and angrily throws the diamond out of the window. John, however, knows they have been duped, and suggests they try to recover the diamond through burglary. The attempt fails and, they are arrested and sentenced to prison. John curses the merchant for his lies.

John and Elzevir go to prison for life. Eventually they are separated. Then, unexpectedly, ten years later, their paths cross again. They are being [[penal transportation|transported]], and board a ship. A storm blows up, and by a strong coincidence, the ship is wrecked upon Moonfleet beach. While trying to reach the beach Elzevir helps John to safety, but is himself dragged under by the surf and drowned.

John arrives where he originally started, in the ''Why Not?'', and is reunited with Ratsey. He is also reunited with Grace. She is now a rich young lady, having inherited her father's money. However, she is still in love with John. John tells her about the diamond and his life in prison. He regrets having lost everything, but she says, rich or not, she loves him.

Then Parson Glennie visits and reveals he had received a letter from Aldobrand. The merchant, suffering a guilty conscience and in an attempt to make amends, had bequeathed the worth of the diamond to John.

John gives the money to the village, and new [[almshouses]] are built, and the school and the church renovated. John marries Grace and becomes Lord of the Manor and Justice of the Peace. They have three children, including their first-born son, Elzevir. They grow up and the sons go away to "serve King George on sea and land" and their daughter, too it seems, has married away. But John and Grace themselves do not leave their beloved Moonfleet ever again.

===Backgammon===
A feature of the narrative is a continuing reference to the [[boardgame]] of [[backgammon]] which is played by the patrons of the ''Why Not?'' on an antique board which bears a [[Latin]] inscription ''Ita in vita ut in lusu alae pessima jactura arte corrigenda est'' (translated in the book as ''As in life, so in a game of hazard, skill will make something of the worst of throws'').

==Geography of the book==
Falkner uses the local geography of Dorset and the [[Isle of Wight]] in the book, only changing some of the place names. The village of ''Moonfleet'' is based on [[East Fleet]] in Dorset by [[Chesil Beach]]. The headland in the book called ''The Snout'' is [[Portland Bill]]. The castle is [[Carisbrooke Castle]] on the [[Isle of Wight]].

==Adaptations in other media==

The book was filmed by [[Fritz Lang]] in 1955 and released [[Moonfleet (1955 film)|under the same name]], with a screenplay adapted by {{Interlanguage link multi|Jan Lustig|de}} from the novel. A handful of scenes from the book survived, including John's ordeal in the church crypt with the remains of Blackbeard (here renamed Redbeard), and his descent into the well to retrieve the diamond, but the movie altered the novel's plot substantially. Among other major changes, its young hero was given the newly invented rogue gentleman Jeremy Fox for a mentor (played by [[Stewart Granger]]), while the role of the working class Elzevir Block was reduced to leading a group of the smugglers seeking to kill John. Lang's film has enjoyed some cachet among French film critics.

In 1963 the BBC aired a 5-episode radio series of Moonfleet adapted by [[Morna Stuart]] and produced by [[Brandon Acton-Bond]].<ref>{{cite web |title=Genome BETA Radio Times 1923 - 2009 |url=http://genome.ch.bbc.co.uk/b1a22d8644c444f5b493d32b3056643f |accessdate=10 December 2016 |publisher=BBC}} </ref> 

In 1964 the BBC filmed a 6-episode TV adaptation under the title ''Smuggler's Bay'', starring future [[Doctor Who]] stars [[Frazer Hines]] and [[Patrick Troughton]] as John Trenchard and Ratsey, respectively.

In 1984 a TV mini-series was filmed, starring [[Adam Godley]] and [[David Daker]]. There is also a 90-minute BBC radio version, starring [[Richard Pearce (British actor)|Richard Pearce]] (from BBC Radio's ''[[The Adventures of Tintin]]'', as well) as John Trenchard.

[[Colonial Radio Theatre|The Colonial Radio Theatre on the Air]] released a 300 min. production of the book in May 2009, Starring [[Jerry Robbins]], [[David Ault]], and [[Rob Cattell]]. It was dramatised by [[Deniz Cordell]], and produced by M. J. Cogburn.

[[Angel Exit Theatre Company]] devised a production which toured the UK in 2009.

In 2010 [[Chris de Burgh]] released an album of songs called ''[[Moonfleet & Other Stories]]'' featuring a story based on the book.

[[Sky1]] filmed a two-part TV adaptation in Ireland in 2013 starring [[Ray Winstone]], [[Aneurin Barnard]] and [[Karl McCrone]]. This was aired 28 December 2013 and 29 December 2013 <ref>{{cite web|author=RT&Eacute; |url=http://www.rte.ie/ten/news/2013/0610/455765-sky-adventure-to-shoot-in-ireland/ |title=Sky adventure to shoot in Ireland – RTÉ Ten |publisher=Rte.ie |date=10 June 2013 |accessdate=16 July 2013}}</ref>  Though more of the plot of the book remains than in the 1955 film, it still bears little relation;  John is in his mid-twenties, Maskew has become an aristocrat and tyrannical descendant of Blackbeard, and Elzevir Block the leader of a brothel-frequenting, knife-fighting band of gangsters.

==Notes==
{{reflist}}

== External links ==
{{wikisource}}
{{Gutenberg|no=10743|name=Moonfleet}}
*[http://www.time.com/time/magazine/article/0,9171,889236,00.html ''Time Magazine'' review of ''Moonfleet'' dated August 13, 1951]
*[http://www.penguinreaders.com/pdf/downloads/pr/teachers-notes/9781405878586.pdf Penguin Reader's Factsheets for ''Moonfleet'', Teacher's Notes, 2008]
* {{librivox book | title=Moonfleet | author=John Meade FALKNER}}

[[Category:1898 novels]]
[[Category:English adventure novels]]
[[Category:Novels about orphans]]
[[Category:Novels set in the 18th century]]
[[Category:Novels set in Dorset]]
[[Category:19th-century British novels]]
[[Category:British novels adapted into films]]