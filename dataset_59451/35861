{{good article}}
{{Infobox single <!-- See Wikipedia:WikiProject_Songs -->
| Name           = Aesthetics of Hate
| Cover          = 
| Cover size     = 
| Border         = 
| Caption        = 
| Artist         = [[Machine Head (band)|Machine Head]]
| Album          = [[The Blackening]]
| A-side         = 
| B-side         = 
| Released       = March 27, 2007
| Format         = [[CD single|CD]]
| Recorded       = 2006
| Genre          = [[Thrash metal]]
| Length         = 6:35
| Label          = [[Roadrunner Records|Roadrunner]]
| Writer         = [[Robert Flynn]], [[Adam Duce]]
| Producer       = Robert Flynn
| Certification  = 
| Last single    = "[[Days Turn Blue to Gray]]"<br/>(2004)
| This single    = "'''Aesthetics of Hate'''"<br/>(2007)
| Next single    = "Now I Lay Thee Down"<br/>(2007)
| Misc           = 
{{External music video|{{YouTube|6RBP8_QSg-c|"Aesthetics of Hate"}}
}}
}}

"'''Aesthetics of Hate'''" is a song by [[Heavy metal music|heavy metal]] band [[Machine Head (band)|Machine Head]] from their sixth studio album ''[[The Blackening]]''. Written by Machine Head vocalist and guitarist [[Robert Flynn]], the song is a retaliation to an article written by William Grim. Grim wrote that late guitarist [[Dimebag Darrell]] was "an ignorant, barbaric, untalented possessor of a guitar", among other comments which angered Flynn deeply enough to write the song. It was nominated for [[Grammy Award for Best Metal Performance|Best Metal Performance]] at the [[50th Grammy Awards]].

==Background==
"Aesthetics of Hate" was written by Machine Head vocalist and guitarist [[Robb Flynn]]. It was written as a retaliation to an article by William Grim for the web site Iconoclast. Titled "Aesthetics of Hate: R.I.P. Dimebag Abbott, & Good Riddance", Grim wrote the article stating Darrell was "part of a generation that has confused sputum with art and involuntary reflex actions with emotion", "an ignorant, barbaric, untalented possessor of a guitar" who looks "more simian than human".<ref name="Event summation">{{cite web
|title=Aesthetics of Hate by William Grim / Robert Flynn’s Reply / MachineHead Lyrics
|author=Rob
|publisher=quickrob.com
|date=2007-07-06
|url=http://www.quickrob.com/weblog/?p=1074
|accessdate=2008-08-10
|archiveurl=https://web.archive.org/web/20090101212546/http://www.quickrob.com/weblog/?p=1074
|archivedate=2009-01-01}}</ref>

After reading the article, Flynn was furious and wrote "Aesthetics of Hate" as a condemnation of Grim's article, and Dimebag detractors. He wrote a message on the band's forum expressing his friendship with Darrell and spoke about Grim:

{{Quote|What would YOU know about love or values? What would YOU know about giving to the world? All that you know is teaching prejudice, and your heart is as black as the 'ignorant, filthy, and hideously ugly, heavy metal fans' you try and paint in your twisted, fictitious ramblings. It's because of people like YOU, that there are [[Nathan Gale]]s in this world, NOT the Dimebags and metal musicians who work to unite people through music.<ref name="Long Live Dimebag Darrell">{{cite web
 |title=Machine Head's Robert Flynn: "Long Live Dimebag Darrell In The Hearts Of Us All" 
 |author=Flynn, Robert 
 |publisher=Blabbermouth.net 
 |date=2004-12-20 
 |url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=30543 
 |accessdate=2008-08-10 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20080402132040/http://www.roadrunnerrecords.com:80/blabbermouth.net/news.aspx?mode=Article&newsitemID=30543 
 |archivedate=2008-04-02 
 |df= 
}}</ref> |Robert Flynn
}}

==Recording==
After "Slanderous" and "Beautiful Mourning" "Aesthetics of Hate" was the third song that was written for Machine Heads album ''[[The Blackening]]''.<ref name="JamPlay - Machine Head Aesthetics of Hate History">{{cite web
|title=Machine Head Aesthetics of Hate History
|publisher=Youtube
|url=http://www.youtube.com/watch?v=zUUpPm6vYaQ&t=0m55s
|accessdate=2017-02-16}}</ref> Flynn originally presented it to the band as "The Thrashterpiece" which became the songs working title, because according to Flynn "it owes a huge debt to the band [[Exodus (band)|Exodus]], (there's) a lot of [[Bay area thrash|Bay Area thrash]] worship going on in this song." In February 2005, Machine Head had penned a rough version of "Aesthetics of Hate". A 13-track November 2005 demo featured the song, although it contained what Flynn described as a "totally fucking lame '[[Angel of Death (Slayer song)|Angel of Death]]' rip off. I hated it every time we played it so I was glad to see that part go!"<ref name="Midwest Metal">{{cite web
|title=	Machine Head|publisher=Midwest Metal|url=http://www.midwestmetalmagazine.com/interviewmachinehead_2.html
|accessdate=2007-08-21| archiveurl= https://web.archive.org/web/20070809132751/http://www.midwestmetalmagazine.com/interviewmachinehead_2.html| archivedate= 9 August 2007 <!--DASHBot-->| deadurl= no}}</ref> The band entered Sharkbite Studios, in [[Oakland, California]] on August 21, 2006 to begin recording. Production duties were handled by Flynn with assistance from Mark Keaton, and [[Audio mixing (recorded music)|mixing]] by [[Colin Richardson]].<ref name="ExcitedRecording">{{cite web
|title=	Machine Head Frontman: "I Can't Remember Ever Being This Excited About Recording"|publisher= [[Blabbermouth.net]]|date=2006-08-22|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=56862
|accessdate=2007-08-18| archiveurl= https://web.archive.org/web/20071001003840/http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=56862| archivedate= 1 October 2007 <!--DASHBot-->| deadurl= no}}</ref>
==Music Video==
The video was shot mostly in Richmond, VA, then a little bit in Norfolk for the live stuff. Fans and members of [[Gojira (band)|Gojira]] &  [[Trivium (band)|Trivium]] made appearances in the video.

==Critical reception==
"Aesthetics of Hate" received positive reviews from music critics. [[Blabbermouth.net]]'s Don Kaye described the track as "literally breathtaking" and said that the song "channels its title emotion into a blazing volcano of pure speed and furious guitarwork from Flynn and Phil Demmel."<ref name="BlabbermouthReview">{{cite web|title=Blabbermouth Review - ''The Blackening'' |author=Kaye, Don |publisher=Blabbermouth.net |url=http://www.roadrunnerrecords.com/blabbermouth.net/showreview.aspx?reviewID=1066 |accessdate=2007-12-02 |deadurl=yes |archiveurl=https://web.archive.org/web/20071015224258/http://roadrunnerrecords.com:80/blabbermouth.net/showreview.aspx?reviewID=1066 |archivedate=2007-10-15 |df= }}</ref> Reviewing for France's ''Hard 'N Heavy'' magazine, [[Anthrax (American band)|Anthrax]]'s [[Scott Ian]] felt that the song is "a riff-o-rama showing off Robb Flynn and Phil Demmel's killer guitar work."<ref name="ScottIanReview">{{cite web|title = Hard N' Heavy Review - ''The Blackening''|author=Ian, Scott|publisher = Blabbermouth.net|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=70735|accessdate=2007-12-02}}</ref> 
Thom Jurek of [[Allmusic]] felt "the intense dual [[arpeggio]]s between both guitarists -- Flynn and [[Phil Demmel]] on 'Aesthetics of Hate' (as just one example) are among the tightest ever".<ref name="Allmusic - The Blackening">{{cite web
|title=Allmusic - ''The Blackening''
|author=Jurek, Thom
|publisher=Allmusic
|url={{Allmusic|class=album|id=r1023773|pure_url=yes}}
|accessdate=2007-08-03}}</ref> However, J.D. Considine of ''[[Blender (magazine)|Blender]]'' commented that the song "cuts from screaming guitars to an ominously whispered, 'May the hands of God strike them down'. Without oversize hooks, calls for biblical vengeance just sound silly."<ref name="Blender - The Blackening">{{cite web
 |title=Blender - The Blackening 
 |author=J.D. Considine 
 |publisher=''Blender'' 
 |date=2007-04-17 
 |url=http://www.blender.com/guide/reviews.aspx?id=4497 
 |accessdate=2007-08-03 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20070927195128/http://www.blender.com/guide/reviews.aspx?id=4497 
 |archivedate=2007-09-27 
 |df= 
}}</ref>

"Aesthetics of Hate" received a nomination for [[Grammy Award for Best Metal Performance|Best Metal Performance]] at the [[50th Grammy Awards]]. The ceremony took place on February 12, 2008, with Machine Head being beaten out by [[Slayer]]'s "Final Six".<ref name="Best Metal Performance ">{{cite web
|title=Best Metal Performance 
|publisher=Grammy.com
|url=http://www.grammy.com/GRAMMY_Awards/50th_Show/list.aspx#04
|archiveurl=https://web.archive.org/web/20071208065050/http://www.grammy.com/GRAMMY_Awards/50th_Show/list.aspx
|archivedate=2007-12-08
|accessdate=2007-12-07}}</ref> Flynn commented on the nomination, "We are completely blown away, and honored by this. It's incredible that the anger of this song has connected with so many people. It proves to Dimebag's detractors the positive impact he had on his fans and fellow bands alike."<ref name="MACHINE HEAD Frontman Comments On GRAMMY Nod ">{{cite web
|title=Machine Head frontman comments on Grammy nod  
|publisher=Blabbermouth.net
|date=2008-01-08
|url=http://www.roadrunnerrecords.com/blabbermouth.net/news.aspx?mode=Article&newsitemID=88025
|accessdate=2008-01-24}}</ref> 

On August 8 2008, the song was confirmed to be as part of the "Roadrunner Records" pack in ''[[Rock Band]]''.<ref name="DLC Week of August 12th: Roadrunner 6 Pack">{{cite web
|title=DLC Week of August 12th: Roadrunner 6 Pack
|publisher=RockBand.com
|url=http://www.rockband.com/forums/showthread.php?t=69720
|accessdate=2008-08-08| archiveurl= https://web.archive.org/web/20080810225430/http://www.rockband.com/forums/showthread.php?t=69720| archivedate= 10 August 2008 <!--DASHBot-->| deadurl= no}}</ref>

==Personnel==
*[[Robert Flynn|Robb Flynn]] – [[lead vocalist|lead vocals]],  [[rhythm guitar]]
*[[Dave McClain (drummer)|Dave McClain]] – [[Drum kit|drums]]
*[[Adam Duce]] – [[Bass guitar|bass]], [[backing vocalist|backing vocals]]
*[[Phil Demmel]] – [[lead guitar]]

==References==
{{reflist|2}}

==External links==
* {{MetroLyrics song|machine-head|aesthetics-of-hate}}<!-- Licensed lyrics provider -->

{{Machine Head}}

[[Category:Machine Head (band) songs]]
[[Category:2007 singles]]
[[Category:Songs written by Robb Flynn]]
[[Category:2006 songs]]
[[Category:Roadrunner Records singles]]
[[Category:Songs written by Adam Duce]]