{{+r|date=April 2015}}{{Use mdy dates|date=October 2011}}
{{Infobox journal
| cover	=
| discipline	=	[[Consumer Law]]
| abbreviation	=	CLR and<br /> Loy. Consumer L. Rev.
| publisher	=	[[Loyola University Chicago School of Law]]
| country	=	USA
| frequency	=	Quarterly
| history	=	Fall 1987 – present
| openaccess	=
| website	=	http://www.luc.edu/clr
| ISSN	=	1530-5449
}}
The '''''Loyola Consumer Law Review''''' ('''CLR'''), is a student-run legal journal affiliated with the [[Loyola University Chicago School of Law]]. Established in 1988 as the '''''Loyola Consumer Law Reporter''''', the publication focuses on legal issues and their relationship to and effect on consumers. On the 10th anniversary of the publication, in 1998, the Loyola Consumer Law Reporter officially became the Loyola Consumer Law Review. CLR is the only law review in the country that is dedicated to examining legal issues as they relate to consumers. The publication provides a forum for dialogue among those in the field including practitioners and law professors. It aims to be approachable to a wide audience and relies on fewer footnotes then the average legal journal.

Each volume of the publication is composed of four issues. The Loyola Consumer Law Review is currently producing Volume 23.

== Editors-in-chief ==

Because the Loyola Consumer Law Review is student-run, each year the publication has had a different editor-in-chief. Here is the complete list of CLR Editors-in-Chief.
{| class="wikitable"
|-
! Volume
! Dates
! Name
|- 
| 25
| 2012–2013
| [[Leslie Cornell]]
|- 
| 24
| 2011–2012
| [[Brad Lorden]]
|-
| 23
| 2010–2011
| [[Peter Matejcak]]
|-
| 22
| 2009–2010
| [[Sarah Tennant]]
|-
| 21
| 2008–2009
| [[Peter Bergan]]
|-
| 20
| 2007–2008
| [[Jeffrey M. Sussman]]
|-
| 19
| 2006–2007
| [[Dara S. Chevlin]]
|-
| 18
| 2005–2006
| [[Eric T. Blum]]
|-
| 17
| 2004–2005
| Amanda Adams
|-
| 16
| 2003–2004
| James Michel
|-
| 15
| 2002–2003
| [[Heather E. Nolan]]
|-
| 14
| 2001–2002
| [[Randy M. Awdish]]
|-
| 13
| 2000–2001
| [[Jennifer K. Gust]]
|-
| 12
| 1999–2000
| [[Lynn Hanley]]
|-
| 11
| 1998–1999
| [[Jeremy Stephenson]]
|-
| 10
| 1997–1998
| [[Jeremy Stephenson]] (Issue 4)<br /> [[Tisha Pates Underwood]] (Issues 1–3)
|-
| 9
| 1996–1997
| [[Tisha Pates Underwood]] (Issue 4)<br />[[Glen S. Thomas]] (Issues 1–3)
|-
| 8
| 1995–1996
| [[Glen S. Thomas]] (Issue 4)<br />[[Janet Garetto]] (Issue 3)<br />[[Heather Shore]] (Issues 1–2)
|-
| 7
| 1994–1995
| [[W. Matthew Bryant]]
|-
| 6
| 1993–1994
| [[Julia C. McLaughlin]]
|-
| 5
| 1992–1993
| [[Allison Despard]]
|-
| 4
| 1991–1992
| [[Pamela B. Hall]]
|-
| 3
| 1990–1991
| [[David V. Goodsir]]
|-
| 2
| 1989–1990
| [[David Colaric]]
|-
| 1
| 1988–1989
| [[Vivian R. Hessel]]
|}

==External links==
*[http://www.luc.edu/clr Official website]
{{Loyola University Chicago}}
[[Category:American law journals]]
[[Category:Law journals edited by students]]
[[Category:Loyola University Chicago School of Law]]
{{law-journal-stub}}