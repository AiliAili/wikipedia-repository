{{Use dmy dates|date=May 2015}}
{{Infobox journal
| title = Intervirology
| formernames = Journal of the Virology Division of the International Union of Microbiological Societies
| cover = [[File:Intervirology_Cover.jpg]]
| editor = [[Jean-Claude Manuguerra]]
| discipline = [[Virology]]
| abbreviation = Intervirology
| publisher = [[Karger Publishers]]
| country =
| frequency = Bimonthly
| history = 1973–present
| openaccess = [[Hybrid open access journal|Hybrid]]
| license =
| impact = 1.773
| impact-year = 2013
| website = http://www.karger.com/INT
| JSTOR =
| OCLC = 1788734
| LCCN =
| CODEN =
| ISSN = 0300-5526
| eISSN = 1423-0100
}}
'''''Intervirology''''' is a bimonthly [[peer-reviewed]] [[medical journal]] covering all aspects of [[virology]], especially concerning animal viruses. It was established in 1973 and is published by [[Karger Publishers]]. The [[editor-in-chief]] is Jean-Claude Manuguerra.

== History ==
The journal was established in 1973 by J.L. Melnick as the ''Journal of the Virology Division of the International Union of Microbiological Societies''. In 1982, it published a paper providing the first [[taxonomic]] description of the [[Ebola virus]] into the [[Filoviridae]].<ref>{{cite journal |author1=Kiley MP|author2=Bowen ETW|author3=Eddy GA|author4=Isaäcson M|author5= Johnson KM|author6= McCormick JB|author7= Murphy FA|author8= Pattyn SR|author9= Peters D|author10=Prozesky OW|author11=Regnery RL|author12= Simpson DIH|author13= Slenczka W|author14=Sureau P|author15= van der Groen G|author16= Webb PA|author17= Wulff H |year=1982|title=Filoviridae: a Taxonomic Home for Marburg and Ebola Viruses?  |url=http://www.karger.com/Article/Abstract/149300  |journal=Intervirology  |volume=18 |pages=24–32  |doi=10.1159/000149300 |issue=1-2 |pmid=7118520}}</ref>

== Abstracting and indexing ==
''Intervirology'' is abstracted and indexed in
{{columns-list|colwidth=30em|
*[[MEDLINE]]/[[PubMed]]
*[[Science Citation Index Expanded]]
*[[Excerpta Medica]]
*[[Chemical Abstracts]]
*[[Biological Abstracts]]
*[[Scopus]]
*[[Current Contents]]
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.773.<ref name=WoS>{{cite book |year=2014 |chapter=Intervirology |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://www.karger.com/INT}}

[[Category:Karger academic journals]]
[[Category:Publications established in 1973]]
[[Category:English-language journals]]
[[Category:Bimonthly journals]]
[[Category:Virology journals]]