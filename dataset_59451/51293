{{Infobox Academic Conference
 | history = 1986-present
 | discipline = [[Programming Languages]]
 | abbreviation = ESOP
 | publisher = [[Lecture Notes in Computer Science]]
 | country = European
 | frequency = Annually
}}

The '''European Symposium on Programming''' (ESOP) is an annual conference devoted to fundamental issues in the specification, design, analysis, and implementation of [[programming language]]s and systems. It is one of the 10 top conferences in Programming Languages.<ref name="arnet">[http://arnetminer.org/page/conference-rank/html/PL,SE.html Ranking of Programming languages and Software Engineer Conferences]</ref>

Initially a biannual conference, ESOP moved in 1998 into an annual schedule and became one of the founding conferences of the European Joint Conferences on Theory and Practice of Software ([[ETAPS]]).<ref>[http://www.etaps.org/ ETAPS official web site]</ref>

==See also==
* [[List of computer science conferences]]
* [[List of computer science conference acronyms]]
* [[List of publications in computer science]]
* [[Outline of computer science]]

== References ==
{{reflist}}

== Further reading ==
* [http://www.sciencedirect.com/science/journal/03043975/411/51-52 Special issue] of [[Theoretical Computer Science (journal)|''Theoretical Computer Science'']] on the European Symposium on Programming
* [http://dl.acm.org/citation.cfm?id=1275498&dl=ACM&coll=DL&CFID=206551778&CFTOKEN=28672129 Special issue] of ''[[ACM Transactions on Programming Languages and Systems]]'' on the European Symposium on Programming
* [http://www.sciencedirect.com/science/journal/01676423/32/1-3 Special issue] of ''[[Science of Computer Programming]]'' on the European Symposium on Programming

== External links ==
* {{Official website|http://www.etaps.org/index.php/about/conferences}}
* [http://www.informatik.uni-trier.de/~ley/db/conf/esop/ DBLP Page of ESOP Conferences]


[[Category:Computer science conferences]]
[[Category:Programming languages conferences]]