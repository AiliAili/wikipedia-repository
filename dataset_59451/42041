<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= Omega
 | image=File:AdvanceOmega5.jpg
 | caption=Omega 5
}}{{Infobox Aircraft Type
 | type=[[Paraglider]]
 | national origin=[[Switzerland]]
 | manufacturer=[[Advance Thun SA]]
 | designer=
 | first flight=
 | introduced=
 | retired=
 | status=In production (Omega XAlps, 2016)
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=
 | program cost= <!--Total program cost-->
 | unit cost= [[Pound sterling|£]]2,399 (Omega 5 30, 2004)
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Advance Omega''' is a series of [[Switzerland|Swiss]] single-place, [[paraglider]]s, designed and produced by [[Advance Thun]] of [[Thun]].<ref name="WDLA04">Bertrand, Noel; Rene Coulon; et al: ''World Directory of Leisure Aviation 2003-04'', page 10. Pagefast Ltd, Lancaster OK, 2003. ISSN 1368-485X</ref>

==Design and development==
The Omega was designed as the competition glider in the company's line.<ref name="WDLA04" />

The design has progressed through many generations of models, from the original Omega, through the Omega 2, 3, 4, 5, 6, 7, 8 to the Omega XAlps, each improving on the last. The models are each named for their rough wing area in square metres.<ref name="WDLA04"/>

The Omega XAlps variant was developed as a specially lightened model, for cross country [[bivouac flying]], involving flying, camping and hiking.<ref name="advance.ch">{{cite web|url=http://www.advance.ch/en/products/paragliders/omega-xalps/|title=Omega XAlps|work=advance.ch|author=[[Advance Thun SA]]|accessdate=14 March 2016}}</ref>

==Operational history==
The Omega XAlps was flown by Chrigel Maurer and Sebastian Huber in the 2015 [[Red Bull X-Alps]] competition. They flew {{convert|1000|km|mi|abbr=on}} over seven days and placed first and second respectively.<ref name="advance.ch"/>

==Variants==
;Omega 5 24
:Small-sized model for lighter pilots. Its {{convert|11.4|m|ft|1|abbr=on}} span wing has wing area of {{convert|23.0|m2|sqft|abbr=on}}, 64 cells and the [[Aspect ratio (aeronautics)|aspect ratio]] is 5.6:1. The pilot weight range is {{convert|65|to|85|kg|lb|0|abbr=on}}. The glider model is DHV 2-3 and AFNOR certified.<ref name="WDLA04"/>
;Omega 5 27
:Mid-sized model for medium-weight pilots. Its {{convert|12.1|m|ft|1|abbr=on}} span wing has wing area of {{convert|26.0|m2|sqft|abbr=on}}, 64 cells and the aspect ratio is 5.6:1. The pilot weight range is {{convert|80|to|105|kg|lb|0|abbr=on}}. The glider model is DHV 2-3 and AFNOR certified.<ref name="WDLA04"/>
;Omega 5 30
:Large-sized model for heavier pilots. Its wing has an area of {{convert|29.2|m2|sqft|abbr=on}}, 64 cells and the aspect ratio is 5.6:1. The pilot weight range is {{convert|100|to|120|kg|lb|0|abbr=on}}. The glider model is DHV 2-3 and AFNOR certified.<ref name="WDLA04"/>
;Omega 7 24
:Small-sized model for lighter pilots. Its {{convert|12.22|m|ft|1|abbr=on}} span wing has wing area of {{convert|23.7|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.3:1. The glider weight is {{convert|5.95|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|70|to|90|kg|lb|0|abbr=on}}. [[Glide ratio]] is 10:1.<ref name="advance.ch1">{{cite web|url=http://www.advance.ch/Technical_details.320.0.html?&L=1|archiveurl=https://web.archive.org/web/20060925023707/http://www.advance.ch/Technical_details.320.0.html?&L=1|title=Omega 7 Technical details|author=[[Advance Thun SA]]|archivedate=25 September 2006|work=advance.ch|accessdate=14 March 2016}}</ref>
;Omega 7 26
:Mid-sized model for medium-weight pilots. Its {{convert|12.77|m|ft|1|abbr=on}} span wing has wing area of {{convert|25.89|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.3:1. The glider weight is {{convert|6.35|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|80|to|103|kg|lb|0|abbr=on}}. Glide ratio is 10:1.<ref name="advance.ch1"/>
;Omega 7 28
:Mid-sized model for medium-weight pilots. Its {{convert|13.29|m|ft|1|abbr=on}} span wing has wing area of {{convert|28.05|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.3:1. The glider weight is {{convert|6.75|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|90|to|116|kg|lb|0|abbr=on}}. Glide ratio is 10:1.<ref name="advance.ch1"/>
;Omega 7 30
:Large-sized model for heavier pilots. Its {{convert|13.73|m|ft|1|abbr=on}} span wing has wing area of {{convert|29.91|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.3:1. The glider weight is {{convert|7.15|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|100|to|130|kg|lb|0|abbr=on}}. Glide ratio is 10:1.<ref name="advance.ch1"/>
;Omega 8 23
:Small-sized model for lighter pilots. Its {{convert|12.32|m|ft|1|abbr=on}} span wing has wing area of {{convert|22.55|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.8:1. The glider weight is {{convert|5.4|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|65|to|85|kg|lb|0|abbr=on}}.<ref name="advance.ch2">{{cite web|url=http://www.advance.ch/en/products/paragliders/omega-8/|archiveurl=https://web.archive.org/web/20140530163314/http://www.advance.ch/en/products/paragliders/omega-8/|title=Omega 8|author=[[Advance Thun SA]]|archivedate=30 May 2014|work=advance.ch|accessdate=14 March 2016}}</ref>
;Omega 8 25
:Mid-sized model for medium-weight pilots. Its {{convert|12.86|m|ft|1|abbr=on}} span wing has wing area of {{convert|24.5|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.8:1. The glider weight is {{convert|5.7|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|75|to|95|kg|lb|0|abbr=on}}.<ref name="advance.ch2"/>
;Omega 8 27
:Mid-sized model for medium-weight pilots. Its {{convert|13.38|m|ft|1|abbr=on}} span wing has wing area of {{convert|26.5|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.8:1. The glider weight is {{convert|6.0|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|85|to|110|kg|lb|0|abbr=on}}.<ref name="advance.ch2"/>
;Omega 8 29
:Large-sized model for heavier pilots. Its {{convert|13.87|m|ft|1|abbr=on}} span wing has wing area of {{convert|28.5|m2|sqft|abbr=on}}, 73 cells and the aspect ratio is 6.8:1. The glider weight is {{convert|6.4|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|100|to|130|kg|lb|0|abbr=on}}.<ref name="advance.ch2"/>
;Omega XAlps 22
:Small-sized model for lighter pilots. Its {{convert|12.6|m|ft|1|abbr=on}} span wing has wing area of {{convert|23.0|m2|sqft|abbr=on}}, 63 cells and the aspect ratio is 6.9:1. The glider weight is {{convert|3.40|kg|lb|0|abbr=on}} and the take-off weight range is {{convert|75|to|95|kg|lb|0|abbr=on}}. The glider model is EN/LTF D certified.<ref name="advance.ch"/>
;Omega XAlps 24
:Large-sized model for heavier pilots. Its {{convert|13.1|m|ft|1|abbr=on}} span wing has wing area of {{convert|24.8|m2|sqft|abbr=on}}, 63 cells and the aspect ratio is 6.9:1. The glider weight is {{convert|3.55|kg|lb|0|abbr=on}} and the  take-off weight range is {{convert|90|to|110|kg|lb|0|abbr=on}}. The glider model is EN/LTF D certified.<ref name="advance.ch"/>
<!-- ==Aircraft on display== -->

==Specifications (Omega 5 24) ==
{{Aircraft specs
|ref=Bertrand<ref name="WDLA04" />
|prime units?=met<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=
|length m=
|length ft=
|length in=
|length note=
|span m=11.4
|span ft=
|span in=
|span note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=23.0
|wing area sqft=
|wing area note=
|aspect ratio=5.6:1
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|more general=
<!--
        Performance
-->
|perfhide=

|max speed kmh=52
|max speed mph=
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|g limits=
|roll rate=
|glide ratio=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|sink rate ms=
|sink rate ftmin=
|sink rate note=<!--  at {{convert|XX|km/h|mph|0|abbr=on}} -->
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|more performance=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=
}}

==References==
{{reflist}}

==External links==
{{Commons category|Advance Omega}}
*{{Official website|http://www.advance.ch/en/products/paragliders/omega-xalps/}}
{{Advance aircraft}}
[[Category:Advance aircraft|Omega]]
[[Category:Paragliders]]