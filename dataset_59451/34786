{{good article}}
{{Infobox hurricane season
|Track=1862 Atlantic hurricane season map.png
|Basin=Atl
|Year=1762
|First storm formed=June 15, 1862
|Last storm dissipated=November 25, 1862
|Strongest storm name=[[#Hurricane Two|Two]] and [[#Hurricane Three|Three]]
|Strongest storm winds=90
|Total storms=6
|Total hurricanes=3
|Fatalities=3
|Damages=
| five seasons=[[1860 Atlantic hurricane season|1860]], [[1861 Atlantic hurricane season|1861]], '''1862''', [[1863 Atlantic hurricane season|1863]], [[1864 Atlantic hurricane season|1864]]
}}
The '''1862 Atlantic hurricane season''' featured six [[tropical cyclone]]s, with only one making [[Landfall (meteorology)|landfall]]. The season had three tropical storms and three hurricanes, none of which became [[Saffir-Simpson hurricane wind scale#Category 3|major hurricanes]].{{#tag:ref|A major hurricane is a storm that ranks as Category&nbsp;3 or higher on the [[Saffir-Simpson hurricane wind scale]].<ref>{{cite book|url=http://www.aoml.noaa.gov/hrd/tcfaq/tcfaqHED.html|title=Hurricane Research Division: Frequently Asked Questions|chapter=A: Basic Definitions|chapterurl=http://www.aoml.noaa.gov/hrd/tcfaq/tcfaqA.html|at=[http://www.aoml.noaa.gov/hrd/tcfaq/A3.html A3) What is a super-typhoon? What is a major hurricane ? What is an intense hurricane ?]|author=Christopher W. Landsea and Neal Dorst|publisher=Atlantic Oceanographic and Meteorological Laboratory|date=June 2, 2011|accessdate=December 27, 2011}}</ref>|group="nb"}} However, in the absence of modern satellite and other remote-sensing technologies, only storms that affected populated land areas or encountered ships at sea were recorded, so the actual total could be higher. An undercount bias of zero to six tropical cyclones per year between 1851 and 1885 has been estimated.<ref>{{cite book |title=Hurricanes and Typhoons: Past, Present and Future |chapter=The Atlantic hurricane database re-analysis project: Documentation for the 1851–1910 alterations and additions to the HURDAT database|author=Christopher W. Landsea|editor=R. J. Murname and K.-B. Liu|year=2004 |publisher=Columbia University Press |location=New York |isbn=0-231-12388-4 |pages=177–221}}</ref> Jose Fernandez-Partagas and Henry Diaz initially documented five tropical cyclones in a 1995 report on this season. A sixth system was added by Michael Chenoweth in 2003 from records taken in [[Colón, Panama]].

The first tropical cyclone was observed as a tropical storm offshore the [[East Coast of the United States]] from June&nbsp;15 to June&nbsp;17. The second and third systems were active in mid-August and mid-September, respectively, and both attained Category 2 intensity at their peaks on the modern-day [[Saffir-Simpson hurricane wind scale]] and neither made landfall. A fourth tropical cyclone caused flooding in [[Saint Lucia]] and brought heavy rain to parts of [[Barbados]] on October&nbsp;5, but its track prior to that date is unknown. The fifth hurricane was known to be active for a few days in October off the East Coast of the United States. Finally, a sixth system was centered near Panama &ndash; between November&nbsp;22 and November&nbsp;25.
__TOC__
{{clear}}

==Systems==
<center><timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/06/1862 till:01/01/1863
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/06/1862

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:15/06/1862 till:17/06/1862 color:TS text:"One (TS)"
  from:18/08/1862 till:21/08/1862 color:C2 text:"Two (C2)"
  from:12/09/1862 till:20/09/1862 color:C2 text:"Three (C2)"
  from:06/10/1862 till:06/10/1862 color:TS text:"Four (TS)"
  from:14/10/1862 till:16/10/1862 color:C1 text:"Five (C1)"
  from:22/11/1862 till:25/11/1862 color:TS text:"Six (TS)"

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/06/1862 till:01/07/1862 text:June
  from:01/07/1862 till:01/08/1862 text:July
  from:01/08/1862 till:01/09/1862 text:August
  from:01/09/1862 till:01/10/1862 text:September
  from:01/10/1862 till:01/11/1862 text:October
  from:01/11/1862 till:01/12/1862 text:November
  from:01/12/1862 till:01/01/1863 text:December

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir-Simpson hurricane wind scale]])"

</timeline>
</center>

===Tropical Storm One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic tropical storm 1 track.png
|Formed=June 15
|Dissipated=June 17
|1-min winds=50
}}
Based on reports from four ships, a tropical storm is known to have existed for two days in mid-June off the East Coast of the United States.<ref name="partagas-1">{{cite book|author=Jose Fernández-Partagás and Henry F. Diaz|title=A Reconstruction of Historical Tropical Cyclone Frequency in the Atlantic from Documentary and other Historical Sources 1851-1880 Part 1: 1851-1870|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/part1.htm
|publisher=Climate Diagnostics Center, National Oceanic and Atmospheric Administration|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|location=Boulder, Colorado|year=1995a|accessdate=March 17, 2014}}</ref> It formed approximately 340&nbsp;miles (550&nbsp;km) east of [[Savannah, Georgia]] on June&nbsp;15 and moved slowly north before dissipating two days later, while located about 250&nbsp;miles (400&nbsp;km) east of [[Virginia Beach, Virginia]].{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic hurricane 2 track.png
|Formed=August 18
|Dissipated=August 21
|1-min winds=90
}}
A Category&nbsp;2 hurricane on the modern-day [[Saffir-Simpson hurricane wind scale]] was first seen on August&nbsp;18, while located approximately 620&nbsp;miles (1,000&nbsp;km) east of [[Florida]]. Over the next three days, it tracked north and moved parallel to the [[East Coast of the United States]]. The system dissipated roughly 310&nbsp;miles (500&nbsp;km) south of [[Newfoundland (island)|Newfoundland]] on August&nbsp;21.{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic hurricane 3 track.png
|Formed=September 12
|Dissipated=September 20
|1-min winds=90
}}
On September&nbsp;12, a Spanish ship, the ''Julian de Unsueta'', was de-masted by a strong gale and thrown onto its beam ends. A few days later, she docked at [[Saint Thomas, U.S. Virgin Islands|Saint Thomas]] in the [[U.S. Virgin Islands]]. On September&nbsp;13, the [[barque]]s ''Montezuma'' and ''Gazelle'' were also both de-masted by a hurricane near [[Barbados]]. No information is available on the hurricane between September&nbsp;14 to September&nbsp;16, but on September&nbsp;17, the barques ''Abbyla'' and ''Elias Pike'' encountered the hurricane, roughly 500&nbsp;miles (800&nbsp;km) off the coast of [[North Carolina]]. Several ships reported encountering hurricane conditions on September 19 off the East Coast of the United States, some as far north as [[Sable Island]].<ref name="partagas-1"/> Based on these reports, the track began about 500&nbsp;miles (800&nbsp;km) northeast of the [[Virgin Islands]] on September&nbsp;12 and ended on September&nbsp;20 off the coast of [[Nova Scotia]].{{Atlantic hurricane best track}}
{{Clear}}

===Tropical Storm Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic tropical storm 4 track.png
|Formed=October 6
|Dissipated=October 6
|1-min winds=50
}}
On October&nbsp;5, a tropical storm caused flooding in [[Saint Lucia]]. That day and throughout the next, high winds and heavy rain were observed in [[Speightstown]], [[Barbados]]. The storm may also have affected [[Saint Vincent (island)|Saint Vincent]]. No track has been identified for the storm and it has been assigned a single location in the [[HURDAT]] database.<ref name="partagas-1"/>
{{Clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic hurricane 5 track.png
|Formed=October 14
|Dissipated=October 16
|1-min winds=70
}}
A modern-day Category&nbsp;1 hurricane was first seen on October&nbsp;14 approximately 310&nbsp;miles (500&nbsp;km) west of [[Bermuda]].{{Atlantic hurricane best track}} A schooner, ''Albert Treat'', encountered the storm and was thrown onto its beam ends. The schooner suffered considerable damage and three men drowned. The next day, further north, the barque ''Acacia'' fell onto its beam ends, but managed to reach safety. Throughout October&nbsp;16 the hurricane traveled northward, parallel to the East Coast of the United States. The ship ''Oder'' reported losing its sails in a hurricane off [[Sable Island]] that day. The storm became [[Extratropical cyclone|extratropical]] around midday on October&nbsp;16 and dissipated completely by October&nbsp;17.<ref name="partagas-1"/>
{{Clear}}

===Tropical Storm Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1862 Atlantic tropical storm 6 track.png
|Formed=November 22
|Dissipated=November 25
|1-min winds=60
}}
Based on meteorological records kept by an officer of the U.S. steamer ''James Adger'', a strong tropical storm was centered to the northwest of [[Colón, Panama|Aspinwall, Panama]] from November&nbsp;22 through to November&nbsp;25. The storm weakened late on November&nbsp;24 and began drifting slowly westward on November&nbsp;25 before dissipating later that day.<ref name="meta">{{cite report|work=Hurricane Research Division; Atlantic Oceanographic and Meteorological Laboratory|year=2008|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|publisher=National Oceanic and Atmospheric Administration|accessdate=March 17, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_master.html#1862_1}}</ref>
{{Clear}}

== See also ==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]
*[[Tropical cyclone observation]]
*[[Atlantic hurricane reanalysis project]]

==Notes==
{{Reflist|group=nb}}

==References==
{{Reflist}}

{{TC Decades|Year=1860|basin=Atlantic|type=hurricane}}

[[Category:Atlantic hurricane seasons]]
[[Category:1860–1869 Atlantic hurricane seasons]]
[[Category:Articles which contain graphical timelines]]