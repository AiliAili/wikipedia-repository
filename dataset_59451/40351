{{Use mdy dates|date=December 2016}}
{{Infobox person
 | name             = Jørgen Christian Dreyer
 | image            = Jorgen Dreyer with Lionesses.jpg
 | image_size       = 180px
 | caption          = Photo from Kansas City Life Insurance
 | signature        = JCD Draft Card signature.jpg
 | birth_date       = {{birth date|1877|12|26}}
 | birth_place      = [[Tromsø]], Norway
 | parents          = Hans and Regina Mikkelsdatter Dreyer
 | death_date       = {{death date and age|1948|11|17|1877|12|26}}
 | death_place      = 3721 Michigan Avenue<br>[[Kansas City]], [[Missouri]]
 | death_cause      = Heart ailment
 | resting_place    = [[Elmwood Cemetery (Kansas City, Missouri)|Elmwood Cemetery]]
 | nationality      = American
 | spouse           = Lorena McWilliams Dreyer<br>(m. 1920–1948, his death)
}}
'''Jørgen Christian Dreyer''' (December 26, 1877 – November 17, 1948) was a Norwegian-born American sculptor. He emigrated to the United States in 1903 and worked as a professor of sculpture at the [[Kansas City Art Institute]] from 1907 to 1909. In his career Dreyer created a number of monumental sculptures, some of which are located in Kansas City, Missouri. His major works include: ''Life Drift''; ''The Goddess of Dawn''; ''Sphinxes'' (pair); ''Biology and Chemistry'' (pair of [[Atlas (mythology)|Atlas figures]]); ''Lionesses'' (pair); ''The Message'' (a bust of [[Archibald Butt]]); ''Bust of Sir [[Carl Busch]]''; [[Mercury (mythology)|''Mercury, god of commerce'']]; and ''Bust of Major General [[Sterling Price]]''.

==Early life (1877–1909)==
Dreyer was born in [[Tromsø]], Norway, on December 26, 1877. He was the son of Hans Dreyer and his wife Regina Dreyer (née Mikkelsdatter). He grew to become a tall man, of slender build, and had blue eyes and auburn brown hair. After schooling at the Latin School in Tromsø, he attended the [[Oslo National Academy of the Arts|Royal School of Art and Industries]] in [[Oslo, Norway|Oslo]].  His first sculpture was in snow, of an old professor, which everyone recognized.<ref name="KCS1912a">Kansas City Star, Vol. 32, Issue 119, p. 10, January 14, 1912</ref> So even though he had started in drawing and painting at the Royal Academy, he saw (and others recognized) that his true gifts were modeling and sculpting.  He continued studies with [[Stephan Sinding]], one of the greatest of European sculptors.<ref name="KCS1948">Kansas City Star, p. 3, November 17, 1948.</ref> Dreyer modeled a bust of an [[Henrik Ibsen|Ibsen]] character, which Ibsen saw at a school exhibition.  Ibsen sought out Dreyer and complimented him in person.<ref name="KCS1912a"/>

After art school, Dreyer was considering a move to Paris to begin his career.  But after a letter from his sister in Topeka, Kansas told of an awakening interest in art and a shortage of sculptors, the young sculptor decided to try America rather than the already overcrowded city of Paris.  Dreyer went to America in 1903 and made Kansas City, Missouri, his home for more than 40 years.<ref name="Ford1999a">Ford, Susan Jezak. [http://www.kchistory.org/cdm4/item_viewer.php?CISOROOT=/Biographies&CISOPTR=36&CISOBOX=1&REC=3 "Dreyer biography"], 1999.</ref>

He moved into a studio and an apartment at 407 Studio Building (previously at the northwest corner of 9th Street and Locust) in Kansas City.  He was a sculpture professor at the Fine Arts Institute (which later became the [[Kansas City Art Institute]]) from 1907 to 1909.<ref name="Lawson1981a">Lawson, Patricia. "The Stones of Jorgen Dreyer", Historic Kansas City Foundation Gazette, pp. 1 and 5, August–September 1981. Kansas City Public Library, Missouri Valley Special Collection, location q977.8411 H673.</ref> Dreyer was dismissed from the Art Institute because he used nude models, a charge for which [[Thomas Hart Benton (painter)|Thomas H. Benton]] would also be dismissed from the Art Institute in the 1930s.<ref name="Lawson1981a"/> He married Lorena E. McWilliams, one of his students, in 1918.<ref name="Lawson1981a"/>

==Early works (1909–1925)==
[[File:LifeDrift with photo.jpg|thumb|left|165px|''Life Drift'' by Dreyer, 1909]]
[[File:Jorgen C. Dreyer sculptures -1917-1918 - Chemistry and Biology.JPG|thumb|right|280px|''Chemistry'' and ''Biology'' statues by Dreyer, 1918]]
One of his first works was a sculpture of a woman reclining on the ground.  One cast of this sculpture was acquired in the first year of the City Art Museum of Saint Louis (which later became the Saint Louis Art Museum) in 1909.  The museum sold this sculpture, titled "Life Drift", through Christie's East, New York, October 15, 1986, sale number 6215, lot 47.<ref name="KCS1908">Kansas City Star, Vol. 29, Issue 50, p. 1, November 6, 1908.</ref>

A rush commission was given to Dreyer in 1912 following the sinking of the [[RMS Titanic|RMS ''Titanic'']].  He sculpted a bust of Major [[Archibald Butt]] who was killed in the ''Titanic'' disaster. Butt was a close personal aide to presidents [[Franklin Deleno Roosevelt|Roosevelt]] and [[William Howard Taft|Taft]]. The commissioned piece, which Dreyer completed on June 15, 1912, was on a base representing a ship on the ocean.  He titled the work "Message (The)".<ref name="The Message">{{cite web|title=Catalogue of Copyright Entries – ''The Message'' by Jorgen C. Dreyer [sculptured bust of Major Butt on base representing ship on ocean],  June 12, 1912|url=https://commons.wikimedia.org/wiki/File:Copyright_of_The_Message,_1912.png|accessdate=July 9, 2015}}</ref>

Dreyer sculpted a bust of Sir [[Carl Busch]] in 1912.  Busch was the founder of what became the [[Kansas City Symphony|Kansas City Symphony Orchestra]], and was its conductor from 1911 until 1918. This [[Plaster cast|plaster bust]] was previously in the foyer of the Music Hall of the Kansas City Auditorium, and was moved to the Carl Busch Papers and Musical Instrument Collection housed within the [[Sousa Archives and Center for American Music]] in Champaign, Illinois, in 1936.<ref name="MusMon1915">Musical Monitor, Official Magazine of the National Federation of Musical Clubs, Vol. IV, No. 6, p. 207, February 1915.</ref><ref name="Sousa2015">Carl Busch Statue, non-accession no. 2004.120991.160, Carl Busch Papers and Music Instrument Collection, circa 1833–1924, Sousa Archives and Center for American Music, University of Illinois at Urbana-Champaign</ref> Dreyer sculpted a bust of Frank Marvin, founder and dean of the engineering school at the [[University of Kansas]] in 1914.  This bronze bust was on a marble pedestal in a corridor of Marvin Hall at KU.<ref name="KCS1914">Kansas City Star, Vol. 77, Issue 163, p. 3, July 9, 1914.</ref>

Dreyer was on the faculty of the Dillenbeck School of Expression from 1915 to 1917.<ref name="Dillen1915">Dillenbeck School. [http://www.vintagekansascity.com/education/dillenbeckschool/ "Faculty of Dillenbeck School"], 1915.</ref> In 1915 Dreyer created a bust of lumber magnate John Barber White, which is displayed in the main branch of the Kansas City Library, Missouri Valley Special Collections.<ref name="Lawson1981b">Lawson, Patricia. [http://www.kchistory.org/cdm4/item_viewer.php?CISOROOT=/Local&CISOPTR=15482&CISOBOX=1&REC=2 "Bust of John Barber White"], 1981.</ref> Also in 1915 Dreyer made a bust of John Priest Greene, former president of [[William Jewell College]].  It is on the 2nd floor of Curry Hall of the college at Liberty, Missouri.<ref name="KCS1915">Kansas City Star, Vol. 35, Issue 260, p. 8, June 4, 1915.</ref> In 1916 Dreyer made a bust of Delbert J. Haff which is located near E. Meyer Blvd. and Monroe Ave. in Kansas City.  Haff was city Park Commissioner from 1908 to 1912.<ref name="kcf1916">kcfountains.com. [http://www.kcfountains.com/fountains/details.php?photo=73 "Haff Fountain"], 1916.</ref> Although the Haff bust was completed in 1916, it went into storage until former Parks Director Frank Vaydik discovered the sculpture and had it installed in its present location in 1967.<ref name="Piland1985">"Fountains of Kansas City", Sherry Piland and Ellen J. Uguccioni, City of Fountains Foundation (kcfountains.com), 1985.</ref>

For the [[Jensen-Salsbery Laboratories]], Dreyer sculpted male Atlas figures representing [[Biology]] and [[Chemistry]] in 1918.  These figures are on the 3rd floor of the building above the entry doors.<ref name="kcvma2013">Kansas City Veterinary Medical Association. [http://www.kcvma.com/downloads/Jen-Sal.pdf "Jen-Sal Labs"], July/August 2013.</ref><ref name="KCT1964">Kansas City Times, p. 38, December 8, 1964.</ref> In 1918 Dreyer registered for the draft with the Selective Service (see the 1918 draft card<ref name="Draft Card">{{cite web|last1=Dreyer|first1=Jorgen|title=Draft Card|url=https://commons.wikimedia.org/wiki/File:JCD_Draft_Card.jpg|publisher=United States of America|accessdate=July 9, 2015|via=WikiMedia Commons}}</ref>), but did not serve in the war.

In 1919 Dreyer was commissioned by the 1919 class of William Jewell College to create a bronze memorial tablet in honor of 16 students who died in [[World War I]].  It was in the library and is now in the archives of the college at Liberty, Missouri.<ref name="KCS1919">Kansas City Star, Vol. 82, Issue 81, p. 6, April 4, 1919.</ref> In 1920 Dreyer sculpted two diminutive bakers for the Rushton Baking Company on Southwest Boulevard in Rosedale, a suburb of Kansas City.<ref name="KCT1964"/> These figures were given to the [[American Institute of Baking]] in Manhattan, Kansas, in 1975.  The two sculptures are in the lobby of AIB International.

Another form of Dreyer's work was in ornamental statuary for private residences in Kansas City.  One example of that form was for the Emory J. Sweeney residence at 5921 Ward Parkway.  The Italian-style residence was built in 1918, and the landscape architects [[Hare and Hare]] were contracted to add lawn and garden treatments.  Hare and Hare asked Dreyer to design two fountains to flank the pool, which were installed in 1922. One was a boy riding a swan, shown in the Gallery below.  The other was a girl also riding a swan.  The pool was later filled in and the disposition of the statues is unknown.<ref name="Piland1985"/>

In 1922 Dreyer was commissioned by the Kansas City Park Board to add a lion to a fountain for the Fitzsimons Memorial at 12th and The Paseo.  Dreyer was commissioned by the [[DeMolay International|Order of DeMolay]] in 1925 to design and execute a Medal of Heroism, "for heroic endeavor".  Dreyer's version of this medal was in use by DeMolay from 1926 to 1960.<ref name="RTD1927">Richmond (VA) Times Dispatch, p. 18, August 18, 1927.</ref><ref name="BH1927">Boston (MA) Herald, p. 101, April 24, 1927.</ref>

==Major sculptures (1925–1939)==
[[File:South Lioness at Kansas City Life Insurance.JPG|thumb|left|165px|Lioness sculpture by Dreyer, completed in 1925]]
{{multiple image
 | align=right
 | direction=horizontal
 | width=
 | footer=
 | width1=150
 | image1= East Sphinx at the Scottish Rite Temple.JPG
 | alt1=
 | caption1=The east sphinx of a pair at the Scottish Rite Temple, 1928. It has the face of a woman.
 | width2=150
 | image2= The goddess of Dawn at the Hotel Phillips.JPG
 | alt2= 
 | caption2=''The Goddess of Dawn'', 1932, in the lobby of the [[Hotel Phillips]]. 
 }}

Two of Dreyer's most visible sculptures are the pair of lionesses in front of the [[Kansas City Life Insurance]] building, located at 3520 Broadway.<ref name="KCLI1998">Kansas City Life Insurance Company. [https://www.kclife.com/company/Files.aspx?type=2&fid=61 "History of the building"], 1998.</ref> Dreyer viewed lions at morning and evening hours at the Swope Park Zoo, now named the [[Kansas City Zoo]], and chose the lioness because it did not have the mane to hide the fluid body lines.  He preferred to see the lions moving, as that gave him more visibility into their muscle structures.  Flaws were found in the cast of one of the lionesses and had to be re-cast three times.  This caused delays in the sculpture schedule, and the building was dedicated in 1924 without the sculptures.  The lionesses were carved in pink, black and white [[granite]] and were installed on April 6, 1925.

In 1925, Dreyer was commissioned by the widow of U.S. Representative [[William P. Borland]] to place a bas relief sculpture on the gravestone of Rep. Borland in Elmwood Cemetery, Block C, Lot 137 in Kansas City.  The finished marker was unveiled on November 11, 1925.<ref name="WS1925">Washington DC Evening Star, p. 58, December 6, 1925.</ref> The bas relief is gone, but Dreyer's engraving of Borland's signature remains.

Perhaps his largest works are the pair of sphinxes at the Scottish Rite Temple in Kansas City.<ref name="srkc2012">Kansas City Scottish Rite Temple. [http://www.srkc.org/history/ "History of the Temple"], 2012.</ref> Each sphinx weighed 20,000 pounds, and Dreyer put the face of a woman on each.  On the back of each sphinx he placed [[Masonic symbols]].  These were completed in 1928.<ref>Kansas City Lens [https://kansascitylens.wordpress.com/2011/08/19/scottishrite/ "Photos from Kansas City"], August 19, 2011.</ref>

In 1931 Dreyer sculpted some marble figures for the Rose Hill Cemetery Mausoleum,<ref name="Ford1999a"/> and in 1932 he installed a sculpture of the ''Goddess of Dawn'' for the new [[Hotel Phillips]].<ref name="VL2004">Van Luchene, Katie. [http://books.google.combooks?isbn=0762734531 "Kansas City, p. 45"], 2004.</ref> This sculpture is 11 feet tall, and is located on the second floor landing of the main staircase in the lobby.

In 1933 Dreyer was again commissioned by Kansas City Life Insurance to complete a sculpture of Mercury.  Dreyer chose to copy the [[Giambologna]] "Flying Mercury" at the Bargello National Museum in Florence, Italy (1580), but instead of using the breath of a zephyr to suspend Mercury (per Giambologna), Dreyer used a wave as he did with Goddess of Dawn.  Inscribed in the circumference of the pedestal is a quote from Horace, Satires 2.6: "NO MORE I ASK, O SON OF MAIA, BUT THAT YOU MAKE THESE GIFTS MY OWN FOR LIFE".  Mercury ("SON OF MAIA") in this context is a bringer of good fortune.  Mercury is in the lobby of the main Kansas City Life Insurance building, surrounded by the murals of fellow [[Art Deco]] muralist Daniel MacMorris.

In 1935 Dreyer was commissioned to create a medal for the Golden Jubilee of Bishop Thomas F. Lillis of Kansas City.<ref name="KCS1948"/> In 1936 Dreyer was awarded a commission for five [[bronze]] medallions that are in the lobby of the Jackson County Courthouse.  These bronze and [[Spelter|white metal]] medallions represent Faith, Authority, Justice, Aspiration and Progress.<ref name="Ford1999b">Ford, Susan Jezak. [http://www.kchistory.org/cdm4/item_viewer.php?CISOROOT=/Building_Pr&CISOPTR=7&CISOBOX=1&REC=3 "Jackson County Courthouse, p.2"], 1999.</ref>

The [[United Daughters of the Confederacy]] commissioned Dreyer in 1939 to create a bust of Major General [[Sterling Price]].<ref name="SD1939">Sedalia (MO) Democrat, p. 10, September 17, 1939.</ref> It is in the Visitor Center of the [[Battle of Lexington State Historic Site]] in Lexington, Missouri.

==Honors==
Dreyer was awarded a gold medal in sculpture at the 1912 Missouri State Fair in Sedalia.<ref name="KCS1912b">Kansas City Star, Vol. 33, Issue 18, p. 2, October 5, 1912.</ref>
Dreyer was made an Honorary Associate of the Kansas City chapter of the [[American Institute of Architects]].<ref name="KCT1962">Kansas City (MO) Times, p. 4, January 31, 1962.</ref>

==Death and legacy==
Dreyer died on November 17, 1948 in Kansas City, Missouri, at the age of 70. He was cremated at [[Elmwood Cemetery (Kansas City, Missouri)|Elmwood Cemetery]] in Kansas City. Dreyer is best known for his [[monumental sculpture]]s.

==Gallery==
<gallery>
1908 Dreyer teaching at Fine Arts Institute.png|teaching at Fine Arts Institute, 1908
1912 Sculptor Jorgen Dreyer at work in his studio.png|at work in his studio, 1912
1914 Carl Busch, as sculpted by Jorgen C. Dreyer.jpg|Carl Busch, 1912
1914 Jorgen Dreyer bust of Carl Busch at Sousa Archives.jpg|2015 image of Carl Busch bust in Sousa Archives, 1912
1914 Dean Frank Marvin, as sculpted by Jorgen C. Dreyer.jpg|Dean Frank Marvin, 1914
1915 John B. White, as sculpted by Jorgen Dreyer.jpg|John B. White, 1915
Bust of John Barber White, Lumber Magnate.JPG|2015 image of John White bust
Bust of John Priest Green, William Jewell College.JPG|2015 image of John Priest Greene bust, 1915
Bust of Delbert J. Haff, Park Commissioner.JPG|2015 image of Delbert Haff bust, 1916
1919 Jorgen Dreyer memorial tablet for William Jewell College.jpg|2015 image of William Jewell College memorial tablet, 1919
1920 diminutive bakers at Rushton Baking Company, as sculpted by Jorgen C. Dreyer.jpg|diminutive bakers, 1920
1920 Jorgen Dreyer sculpture of baker with pie.JPG|2015 image of baker with pie, 1920
1922 Jorgen Dreyer sculpture of boy on swan.jpg|Boy on a swan, 1922
1922 Jorgen Dreyer sculpture for Fitzsimons Memorial.JPG|Fitzsimons Memorial fountain, 1922
1922 Jorgen Dreyer lion for Fitzsimons Memorial.JPG|Fitzsimons Memorial lion, 1922
Gravestone of Rep. William P. Borland.JPG|2015 image of William Borland gravestone, 1925
Lionesses at Kansas City Life Insurance.JPG|2015 image of Lionesses at KCLI, 1925
1927 DeMolay Medal of Heroism, as sculpted by Jorgen C. Dreyer.jpg|DeMolay Medal of Heroism, 1927
Two sphinxes at the Scottish Rite Temple.JPG|2015 image of Scottish Rite Temple sphinxes, 1930
Rose Hill Mausoleum.JPG|2015 image of north face of Rose Hill Mausoleum, 1931
1932 Goddess of Dawn, as sculpted by Jorgen C. Dreyer.jpg|2015 image of Goddess of Dawn, 1932
1933 Jorgen Dreyer sculpture of Mercury.JPG|2015 image of Mercury, copied from Giambologna, 1933
1933 Jorgen Dreyer sculpture of Mercury torso.JPG|2015 image of Mercury torso, 1933
1935 Jorgen Dreyer Jubilee medal of Bishop Lillis.jpg|Golden Jubileee medal of Bishop Lillis, 1935
Five Medallions in Jackson County County Courthouse.JPG|2015 image of Medallions in Jackson County Courthouse, 1936
1939 Jorgen Dreyer right aspect of bust of Gen. Sterling Price.JPG|2015 image of General Sterling Price, 1939
1948 Jorgen Dreyer obit photo.jpg|obit photo KC Star (age about 30 in photo)
</gallery>

==References==
{{Reflist|30em}}

==External links==
* [http://www.kclife.com/ The Kansas City Life Insurance Company]
* [http://www.srkc.org/ The Scottish Rite Temple of Kansas City]
* [http://www.hotelphillips.com/boutique-hotel/hotel-phillips-history The Hotel Phillips]
* [http://dcjetc.com/UncleJorgen Uncle Jorgen: a Family Tribute] has clippings of above newspaper sources, as well as many more photos.

{{DEFAULTSORT:Dreyer, Jorgen}}
[[Category:1877 births]]
[[Category:1948 deaths]]
[[Category:People from Tromsø]]
[[Category:Oslo National Academy of the Arts alumni]]
[[Category:Norwegian emigrants to the United States]]
[[Category:20th-century American sculptors]]
[[Category:American male sculptors]]
[[Category:Monumental masons]]
[[Category:Kansas City Art Institute faculty]]
[[Category:Burials at Elmwood Cemetery (Kansas City, Missouri)]]