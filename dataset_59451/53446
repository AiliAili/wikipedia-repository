{{Financial markets}}

'''Common stock''' is a form of corporate [[equity (finance)|equity]] ownership, a type of [[security (finance)|security]]. The terms '''voting share''' and '''ordinary share''' are also used frequently in other parts of the world; "common stock" being primarily used in the [[United States]].  They are known as '''Equity shares''' or Ordinary shares in the UK and other [[Commonwealth of Nations|Commonwealth]] realms.  This type of share gives the [[stockholder]] the right to share in the profits of the company, and to vote on matters of [[corporation|corporate]] [[policy]] and the composition of the members of the [[board of directors]].

It is called "common" to distinguish it from [[preferred stock]]. If both types of stock exist, common/equity stockholders usually cannot be paid [[dividend]]s until all preferred/preference stock dividends are paid in full; it is possible to have common stock that has dividends that are paid alongside the preferred stock.

In the event of [[bankruptcy]], common stock investors receive any remaining funds after bondholders, creditors (including employees), and preferred stockholders are paid. As such, common stock investors often receive nothing after a liquidation bankruptcy [[Chapter 7, Title 11, United States Code|Chapter 7]].

Common stockholders can also earn money through [[capital appreciation]].  Common shares may perform better than preferred shares or bonds over time, in part to accommodate the increased risk.<ref>{{Cite web|title=Common Stock|url=http://www.investopedia.com/terms/c/commonstock.asp|publisher=ValueClick|work=Investopedia.com|accessdate=2013-05-12}}</ref>

==Shareholder rights==
Shareholder rights are more conceptual than technical or factual. Their most common source is in the statutory and case law of the jurisdiction in which the company was formed. Information about what people think of as shareholder rights can also be found in the corporate charter and governance documents, but companies do not actually have documentation outlining specific "Shareholder Rights." Some shareholders elect to enter into shareholder agreements that create new rights among the shareholders, and it is common for the company to be a party to that agreement. {{fact|date=November 2016}}

Some common stock shares have voting rights on certain matters, such as electing the board of directors. However in the [[United States]], a company can have both a "voting" and "non-voting" series of common stock, as with preferred stock, but not in countries which have laws against multiple voting and [[Non-voting stock|non-voting]] shares

Hypothetically speaking, holders of voting common stock can influence the corporation through votes on establishing corporate objectives and policy, stock splits, and electing the company's board of directors. In practice, it's questionable whether or not such actions can be organized or ruled in their favor. Some shareholders, including holders of common stock, also receive preemptive rights, which enable them to retain their proportional ownership in a company if it issues additional stock or other securities.
There is no fixed dividend paid out to common/equity stockholders and so their returns are uncertain, contingent on earnings, company reinvestment, and efficiency of the market to value and sell stock.<ref>{{cite web|url=http://www.wfu.edu/~palmitar/Law&Valuation/chapter%204/4-3-1.htm|title=Characteristics of common stock}}</ref>

==Classification==
Common/Equity stock is classified to differentiate it from preferred stock. Each is considered a "class" of stock, with different series of each issued from time to time such as Series B Preferred Stock. Nevertheless, using "Class B Common Stock" is a common label for a super-voting series of common stock.

==See also==
* [[Capital surplus]]
* [[Equity (finance)]]
* [[Share capital]]
* [[Shares authorized]]
* [[Shares issued]]
* [[Shares outstanding]]
* [[Treasury stock]]

==References==
{{Reflist}}

==External links==
* [http://www.allbusiness.com/business-planning/business-structures-corporations-stock/3779142-1.html Common Stock vs. Preferred Stock]

{{Stock market|state=collapsed}}

[[Category:Stock market terminology]]
[[Category:Equity securities]]
[[Category:Corporate finance]]