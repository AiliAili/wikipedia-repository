<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=LA-8
 | image=AeroVolga LA-8 (RA-0344G).jpg
 | caption=First prototype in 2008, with upturned tips
}}{{Infobox Aircraft Type
 | type=Twin engine [[amphibious aircraft]]
 | national origin=[[Russia]]
 | manufacturer=[[AeroVolga]]
 | designer=
 | first flight=20 November 2004
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=9 by mid-2012, 3 more on order
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from original
 | variants with their own articles=
}}
|}

The '''AeroVolga LA-8''' is an 8-seat [[amphibious aircraft]] designed and built in [[Russia]].  First flown in 2004, about six had been sold by mid-2012.

==Design and development==
The LA-8 has its origin with the [[Chaika L-6]], first flown in 2000, and its immediate successor the L-6M, promoted by AeroVolga and first flown in 2001. Both of these were twin engine, V-tailed [[amphibious aircraft]], the latter differing in its hull design, undercarriage and maximum take-off weight. Chaika developed the L-6M into the [[Chaika L-4|L-4]] and AeroVolga developed it into the LA-8. These are very different designs: the LA-8 has a [[T-tail]] and is some 25% longer than the L-4, with [[tricycle gear]].<ref name=JAWA12/><ref name=JAWA13/>

The LA-8 is largely built from [[foam rubber|plastic foam]] and [[PVC]] filled [[glass fibre]] sandwich. Its wing is divided into watertight compartments.  The amphibian is a [[cantilever]] [[monoplane#Types|high-wing monoplane]] with straight tapered wings; most of the sweep is on the [[trailing edge]]. The prototype originally had downturned [[wing tip]]s but these were replaced with extended upturned tips by 2006 and by early 2011 a later aircraft had [[winglets]] which extended both above and below the tips.  There is no [[dihedral (aircraft)|dihedral]].  Its twin engines are placed above and ahead of the wing [[leading edge]], as close to the centre-line as the clearance between [[propeller (aircraft)|propeller]] and fuselage allows. The prototype was powered by two [[Avia M 337#Variants|LOM M337]]AK air-cooled six-cylinder [[supercharged]] inverted [[inline engines]].  Two engine types have been fitted to production aircraft: the LA-8C models have more powerful {{convert|185|kW|hp|abbr=on|0}} [[Avia M 337#Variants|LOM M337]]Cs and the LA-8L variants {{convert|175|kW|hp|abbr=on|0}} [[Lycoming IO-540]] air-cooled [[flat-six engine|flat-sixes]].<ref name=JAWA13/>

The two-[[step (hull)|step]] hull is also subdivided into watertight compartments. Two fixed underwing stepped floats at about 75% span stabilize the aircraft on water.  The cabin is under the wing, with two long continuous transparencies, one on each side plus two shorter panels in the main entry hatch in the rear roof behind the trailing edge.  There is seating for eight, including one or two pilots. The LA-8's [[T-tail]] has a slightly tapered, straight-edged, swept [[fin]] with a sub-fin extension; both the [[rudder]] and the single piece, externally [[aileron#Mass balance weights|mass balanced]] [[elevator (aircraft)|elevator]] have electrically actuated [[trim tab]]s.<ref name=JAWA13/>

The LA-8 has a tricycle undercarriage with mainwheels retracting into the fuselage sides and the nosewheel retracting rearwards.  A [[ballistic parachute]] is an option.<ref name=JAWA13/>

The LA-8 first prototype flew for the first time on 20 November 2004. The first production aircraft flew in August 2006 and the first Lycoming powered machine was completed in mid-2010.<ref name=JAWA13/>

==Operational history==
By mid-2012 six LA-8s had been produced for customers as well as the three prototypes. Two were with Russian operators, one flying around [[Lake Baikal]] and the other on [[Lake Seliger]].  Two went to an operator in [[Montenegro]].  Another five have been sold to Rimos Ltd for work in the [[Persian Gulf]]; by July 2012 two of these had been delivered.<ref name=JAWA13/>
[[File:AeroVolga LA-8 (RA-0778G).jpg|thumb|right|LA-8C]]

==Variants==
;LA-8C: Baseline version powered by 2x {{convert|235|hp|kW|abbr=on|disp=flip}} [[LOM Praha M-337C-AV01]].
;LA-8L:2x {{convert|235|hp|kW|abbr=on|disp=flip}} [[Lycoming IO-540B4B5]] engines.
;LA-8C-RS: Long range version of LA-8C, powered by 2x {{convert|235|hp|kW|abbr=on|disp=flip}} [[LOM Praha M-337C-AV01]].
;LA-8L-RS: Long range version of LA-8L, 2x {{convert|235|hp|kW|abbr=on|disp=flip}} [[Lycoming IO-540B4B5]] engines..
;LA-8HC: Flying boat / Hydroplane / skis based on the LA-8C
;LA-8HL: Flying boat / Hydroplane / skis based on the LA-8L
;LA-8LDM: High specification variant.
;LA-8FF: Fire-fighting variant.
<!-- ==Units using this aircraft/Operators (choose)== -->

== Specifications (LA-8L-RS) ==
{{Aircraft specs
|ref=Jane's All the World's Aircraft 2013-14<ref name=JAWA13/><ref name=Note group=Notes/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One or two
|capacity=max 7 passengers.
|length m=10.80
|length note=
|span m=15.00
|span note=
|height m=3.14
|height note=
|wing area sqm=22.10
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=P-lll-15
|empty weight kg=1655 
|empty weight note=
|gross weight kg=2720
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=2
|eng1 name=[[Lycoming IO-540B4B5]]
|eng1 type=air-cooled [[flat-six engine|flat six]]
|eng1 kw=175
|eng1 note=
|power original=
|more power=

|prop blade number=3
|prop name=[[Hoffman propellers|Hoffman]] HO-V123K / MT -V propellers 
|prop dia m=1.95
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=259
|max speed note=
|cruise speed kmh=220
|cruise speed note=normal
|stall speed kmh=86
|stall speed note=pwer off, flaps down
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=5000
|ceiling note=
|g limits=+3.8/-1.9
|roll rate=<!-- aerobatic -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|power/mass=
|more performance=
*'''Take-off run, land:''' 350 m (1,149 ft)
*'''Take-off run, water:''' 450 m (1,477 ft)
|avionics=
*'''Communications:''' Bendix/King KY96 radio, GTX 527 responder. Artex ME406 emergency location transmitter.
*'''[[GPS]] options:''' Bendix/King KY94 or Garmin GNC 420 or GNS 430.
*'''Other options:''' KR 87 radio compass, KN 62A DME (distance measuring equipment), KN 53 [[VHF omnidirectional range|VOR]] and Garmin GL 108 GS/LOC ([[Instrument landing system]])
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|group=Notes|refs=
<ref name=Note group=Notes>Jane's (2013) warns that the manufacturer has, in 2013, given contradicting dimensions and weights, so the following data "should be regarded with suspicion."  In addition, all performance figures are estimates.</ref>
}}

==References==
{{commons category|AeroVolga LA-8}}
{{reflist|refs=
<ref name=JAWA12>{{cite book |title= Jane's All the World's Aircraft  : development & production : 2012-13|last= Jackson |first= Paul A. |coauthors= |edition= |year=2012|publisher=IHS Global|location=|isbn=978-0-7106-3000-1 |page=514}}</ref>

<ref name=JAWA13>{{cite book |title= Jane's All the World's Aircraft  : development & production : 2013-14|last= Jackson |first= Paul A. |coauthors= |edition= |year=2013|publisher=IHS Global|location=|isbn=978-0-7106-3040-7 |pages=509–510}}</ref>

}}

<!-- ==Further reading== -->
<!-- ==External links== -->

[[Category:Amphibious aircraft]]
[[Category:Russian civil aircraft 2000–2009]]