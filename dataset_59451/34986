{{Good article}}
{{Infobox hurricane season
| Basin=EPac
| Year=1957
| Track=1957 Pacific hurricane season summary map.png
| First storm formed=July 15, 1957
| Last storm dissipated=December 6, 1957
| Strongest storm name=Hurricane Twelve
| Strongest storm pressure=
| Strongest storm winds=120
| Average wind speed=1
| Total storms=13
| Total hurricanes=9
| Total intense=1
| Fatalities=19 direct, 2 indirect
| Damages=.1
| Inflated=1
|five seasons=[[1955 Pacific hurricane season|1955]], [[1956 Pacific hurricane season|1956]], '''1957''', [[1958 Pacific hurricane season|1958]], [[1959 Pacific hurricane season|1959]]
| Atlantic season=1959 Atlantic hurricane season
| West Pacific season=1959 Pacific typhoon season}}
The '''1957 Pacific hurricane season''' was a moderately active year in which 13 [[tropical cyclone]]s formed. The hurricane season ran through the summer and fall months which conventionally delimit the period during which most tropical cyclones form in the northeastern [[Pacific Ocean]]. The first tropical cyclone developed on July 15. The final storm dissipated on December 6, becoming one of the few Pacific storms to exist outside of the seasonal dates. Of the season's 13 storms, five of these formed or crossed into the central Pacific.

During the season, five storms impacted land. Hurricane Twelve was the deadliest, leaving eight casualties in Mazatlán and the costliest was Hurricane Nina, causing an estimated $100,000 in losses. In addition to the damage, four people were killed by Nina in Hawaii. Hurricane Six killed seven people and Hurricane Ten killed two in Mexico.

==Systems==
<timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270

AlignBars  = early
DateFormat = dd/mm/yyyy
Period     = from:01/07/1957 till:31/12/1957
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/07/1957

Colors =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_&lt;39_mph_(0-62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39-73_mph_(63-117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74-95_mph_(119-153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96-110_mph_(154-177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111-130_mph_(178-209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131-155_mph_(210-249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=
  barset:Hurricane width:11 align:left fontsize:S shift:(4,-4) anchor:till
  from:15/07/1957 till:26/07/1957 color:C1 text:"Kanoa"
  from:06/08/1957 till:16/08/1957 color:C2 text:"Two"
  from:09/08/1957 till:15/08/1957 color:C1 text:"Three"
  from:01/09/1957 till:09/09/1957 color:C1 text:"Della"
  barset:break
  from:09/09/1957 till:11/09/1957 color:TS text:"Five"
  from:17/09/1957 till:18/09/1957 color:C1 text:"Six"
  from:20/09/1957 till:23/09/1957 color:TS text:"Seven"
  from:25/09/1957 till:28/09/1957 color:TS text:"Eight"
  from:26/09/1957 till:27/09/1957 color:TS text:"Nine"
  from:27/09/1957 till:30/09/1957 color:TS text:"Faye"
   barset:break
  from:01/10/1957 till:06/10/1957 color:C1 text:"Ten"
  from:17/10/1957 till:20/10/1957 color:C1 text:"Eleven"
  from:20/10/1957 till:22/10/1957 color:C4 text:"Twelve"
  from:29/11/1957 till:06/12/1957 color:C1 text:"[[Hurricane Nina (1957)|Nina]]"
   barset:break

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/07/1957 till:31/07/1957 text:July
  from:01/08/1957 till:31/08/1957 text:August
  from:01/09/1957 till:30/09/1957 text:September
  from:01/10/1957 till:31/10/1957 text:October
  from:01/11/1957 till:30/11/1957 text:November
  from:01/12/1957 till:31/12/1957 text:December

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir-Simpson Hurricane Scale]])"

</timeline>
{{Clear}}
===Hurricane Kanoa===
{{Infobox Hurricane Small
|Basin=EPac
|Image=Hurricane Kanoa 1957 weather map.png
|Track=Kanoa 1957 track.png
|Formed=July 15
|Dissipated=July 26
|1-min winds=75
|Pressure=985
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
}}
The first hurricane of the season was identified by the National Weather Bureau in San Francisco, California on July&nbsp;15. The previous day, the ''S.S. Garvel Park'' recorded sustained winds of 50&nbsp;mph (85&nbsp;km/h) when it was situated roughly 75&nbsp;mi (120&nbsp;km) south of [[Manzanillo, Colima|Manzanillo, Mexico]].<ref name="CPHC">{{cite web|author=Central Pacific Hurricane Center|publisher=National Oceanic and Atmospheric Administration|date=May 4, 2007|accessdate=January 10, 2010|title=The 1957 Central Pacific Tropical Cyclone Season|url=http://www.prh.noaa.gov/cphc/summaries/1957.php}}</ref> Although listed as a Category 1 hurricane for its entire known existence by the [[HURDAT|hurricane database]], the storm was not confirmed to have attained hurricane intensity until July&nbsp;21.{{EPAC hurricane best track}} The storm took a steady westward track during the early portion of its existence in response to a strong [[Ridge (meteorology)|ridge]] located north of Hawaii.<ref name="CPHC"/><ref name="Kanoa2E"/> On July&nbsp;18, the National Weather Bureau discontinued advisories on the storm as no information on it was being received.<ref name="Kanoa2E">{{cite web|author=Rue E. Rush|publisher=National Weather Bureau in Honolulu, Hawaii|date=March 21, 1960|accessdate=January 10, 2010|title=Two Unique Eastern Pacific Hurricanes of 1957|url=http://docs.lib.noaa.gov/rescue/mwr/088/mwr-088-03-0107.pdf|format=PDF}}</ref>

On July&nbsp;21, a vessel named ''Cape Horn'' relayed information regarding the storm to the National Weather Bureau, leading to them re-issuing advisories on the storm, upgrading it to a hurricane.<ref name="CPHC"/> The following day, a [[reconnaissance mission]] from [[Oahu, Hawaii]] located the storm's 40&nbsp;mi (65&nbsp;km) wide [[Eye (cyclone)|eye]] and recorded sustained winds of 80&nbsp;mph (130&nbsp;km/h) and gusts up to 115&nbsp;mph (185&nbsp;km/h).<ref name="Kanoa2E"/> Shortly after, the storm likely attained its peak intensity as a high-end Category 1 hurricane, with winds of 85&nbsp;mph (140&nbsp;km/h).{{EPAC hurricane best track}} Early on July&nbsp;23, warning responsibility of the storm was given to the National Weather Bureau in Honolulu, Hawaii.<ref name="CPHC"/>

Upon transferring responsibility, the hurricane was given the name Kanoa, the Hawaiian name meaning "the free one".<ref name="CPHC"/>{{EPAC hurricane best track}} As the storm approached Hawaii, it began to weaken as [[Atmospheric convection|convection]] filled the eye. The storm's track mirrored that of the shipping lane between the [[Panama Canal]] and Hawaii, leading to several ships being affected.<ref name="CPHC"/> Colder air also began to enter the circulation, leading to further weakening. By the time the system reached Hawaii, it was no more than an area of disturbed weather. The remnants of Kanoa persisted until July&nbsp;26, at which time they dissipated over the Hawaiian Islands.<ref name="Kanoa2E"/> The remnants of the storm brought beneficial rainfall to most of Hawaii, with heavy rains being reported in parts of the [[Hawaii (island)|Big Island]].<ref name="CPHC"/>
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=August 6
|Dissipated=August 16
|1-min winds=90
|Pressure=987
|Image=2-E 1957 weather map.png
|Track=2-E 1957 track.png
}}
Roughly two weeks after Kanoa dissipated, the season's second hurricane was identified over the open waters of the eastern Pacific on August&nbsp;6.{{EPAC hurricane best track}} This storm traveled in a similar fashion to Kanoa in response to a high pressure system over the northern Pacific.<ref name="Kanoa2E"/> By August&nbsp;9, the system turned northwest and weakened.{{EPAC hurricane best track}} Cold, dry air began to enter the storm as it accelerated over cooler waters. Several reconnaissance missions were flown into the storm due to rapid changes in the storm's wind field and structure.<ref name="Kanoa2E"/> Late on August&nbsp;10, the system intensified into a Category 1 hurricane and its forward motion slowed as it turned more westward.{{EPAC hurricane best track}}

Although at an unusual latitude, the storm maintained this intensity for over a day before weakening to a tropical storm.{{EPAC hurricane best track}} During the afternoon of August&nbsp;13, the small system re-intensified into a hurricane. Reconnaissance reported that gale-force winds extended no more than 100&nbsp;mi (155&nbsp;km) from the storm's eye.<ref name="Kanoa2E"/> Shortly thereafter, the system further intensified into a Category 2 hurricane, attaining maximum winds of 105&nbsp;mph (160&nbsp;km/h).{{EPAC hurricane best track}} Weather maps at the time depicted the storm as having a minimum pressure of 987&nbsp;mbar (hPa).<ref name="Kanoa2E"/> By August&nbsp;14, the storm began to weaken once more, gradually becoming a tropical storm on August&nbsp;15. After tunrning northward, the cyclone became extratropical at a high latitude of 39.8°N.{{EPAC hurricane best track}}<ref name="Kanoa2E"/>
{{Clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=August 9
|Dissipated=August 15
|1-min winds=75
|Image=3-E track 1957.png
|Pressure=972
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
}}
As Hurricane Two intensified over open waters, the season's third storm was identified on August&nbsp;9. Initially tracking westward, the hurricane turned sharply north, maintaining an intensity of 85&nbsp;mph (140&nbsp;km/h) during its known existence. Briefly slowing on August&nbsp;11, the system quickly picked up forward speed as it turned northwestward, paralleling the coastline of the [[Baja California Peninsula]]. On August&nbsp;14, the hurricane turned north once more; however, it lost its identity the following day off the coast of [[Baja California]].{{EPAC hurricane best track}}
{{Clear}}

===Hurricane Della===
{{Infobox Hurricane Small
|Basin=EPac
|Image=Della 1957 track.png
|Formed=September 1
|Dissipated=September 9 <small>crossed 180°</small>
|1-min winds=75
|Pressure=980
}}
Hurricane Della was first identified on September&nbsp;1 southeast of Hawaii as a tropical depression. The depression quickly intensified into a tropical storm as it traveled northwest;{{EPAC hurricane best track}} however, operational advisories were not issued until September&nbsp;3. By that time, a ship reported they had encountered a storm and recorded winds of 100&nbsp;mph (155&nbsp;km/h). [[Weather radar|Radar]] images also depicted an eye had developed within the storm.<ref name="CPHC"/> After becoming a hurricane, Della began a gradual turn towards the southwest.{{EPAC hurricane best track}} During the day on September&nbsp;4, the storm passed roughly 10&nbsp;mi (15&nbsp;km) south of the [[French Frigate Shoals]], bringing strong winds gusting up to 110&nbsp;mph (175&nbsp;km/h). During the storm's passage, a pressure of 980&nbsp;mbar (hPa) was recorded, the lowest in relation to the storm.<ref name="CPHC"/> By September&nbsp;9, Della began to turn westward as it approached the [[International Date Line]] with winds of 85&nbsp;mph (140&nbsp;km/h).{{EPAC hurricane best track}}

During the day, the storm crossed 180°, entering the western Pacific basin and being re-designated as a typhoon.<ref name="CPHC"/> The storm tracked steadily northwestward, attaining a peak intensity of 125&nbsp;mph (205&nbsp;km/h), a high-end Category 3 storm, before turning back towards the east. The storm gradually weakened, transitioning into an [[extratropical cyclone]] on September&nbsp;17 and again crossing the International Date Line. The system dissipated shortly thereafter over open waters.<ref name="CPHC"/>{{EPAC hurricane best track}} Throughout its existence, Hurricane/Typhoon Della traveled roughly {{convert|5000|mi|km|abbr=on}}, the longest known track of any Pacific hurricane at the time.<ref name="CPHC"/>
{{Clear}}

===Tropical Storm Five===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=September 9
|Dissipated=September 11
|1-min winds=45
|Image=5-E 1957 track.png
}}
The fifth tropical cyclone of the season was first identified as a tropical storm several hundred miles southeast of Hawaii on September&nbsp;9. The storm quickly attained winds of 50&nbsp;mph (85&nbsp;km/h) before weakening. By September&nbsp;11, the system weakened to a tropical depression.{{EPAC hurricane best track}} Later that day, the system entered an area with no ships available for reporting. However, no ships reported a storm in the region for several days, signifying the depression's dissipation.<ref name="CPHC"/>
{{Clear}}

===Hurricane Six===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=September 17
|Dissipated=September 18
|1-min winds=75
|Pressure=984
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=6-E 1957 track.png
}}
On September&nbsp;17, a new hurricane was identified just off the southern coast of Mexico with winds of 85&nbsp;mph (140&nbsp;km/h). Moving northwestward, the storm eventually made landfall near [[Acapulco]], Mexico at this intensity. Shortly after moving over land, the storm lost its identity.{{EPAC hurricane best track}}

High winds and heavy rains from the storm caused moderate damage to structures and vessels throughout Acapulco, Mexico.<ref>{{cite news|author=Staff Writer|work=Chicago Daily Tribune|page=2|date=September 18, 1957|title=Storm Pounds Acapulco; Many Boats Damaged}}</ref> Several roads were washed out or flooded by the storm.<ref>{{cite news|author=United Press International|publisher=The Sunday News Journal|date=September 18, 1957|accessdate=January 10, 2010|title=Acapulco Lashed|url=https://news.google.com/newspapers?nid=1879&dat=19570918&id=MG4eAAAAIBAJ&sjid=jMkEAAAAIBAJ&pg=5027,2760105}}</ref> Seven people were killed throughout the region by the storm, including one U.S. citizen who was electrocuted by a downed power line.<ref>{{cite news|author=Staff Writer|publisher=The New York Times|page=15|date=September 19, 1957|title=Mexican Storm Kills 7; U.S. Citizen Electrocuted by Fallen Wire at Acapulco}}</ref>
{{Clear}}

===Tropical Storm Seven===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=September 20
|Dissipated=September 23
|1-min winds=45
|Pressure=991
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=7-E 1957 track.png
}}
Several days after Hurricane Six lost its identity over Mexico, a new tropical storm formed near the southern edge of the [[Gulf of California]] on September&nbsp;20. Some forecasters stated that the system may have been the remnants of the preceding storm which regenerated. However, the Hurricane Database did not confirm this. The system tracked nearly due north, attaining maximum winds of 50&nbsp;mph (85&nbsp;km/h), nearing the coastline of Mexico several times. However, the storm did not make landfall during its existence. On September&nbsp;22, it turned westward before dissipating just off the coast of Baja California Sur.{{EPAC hurricane best track}}
{{Clear}}

===Tropical Storm Eight===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=September 25
|Dissipated=September 28
|1-min winds=45
|Image=8-E 1957 track.png
}}
The fifth tropical cyclone to form or enter the central Pacific was first identified on September&nbsp;25 several hundred miles south of the Hawaiian Islands. Traveling nearly due west, the storm attained an intensity of 50&nbsp;mph (85&nbsp;km/h) before weakening. By September&nbsp;27, the storm weakened to a tropical depression and later dissipated on September&nbsp;28 over open waters.{{EPAC hurricane best track}}
{{Clear}}

===Tropical Storm Nine===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=September 26
|Dissipated=September 27
|1-min winds=45
|Pressure=999
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=9-E 1957 track.png
}}
The shortest lived storm of the season, Tropical Storm Nine, formed south of Mexico on September&nbsp;26. The system attained peak winds of 50&nbsp;mph (85&nbsp;km/h) during its existence. Later that day, the center of the storm relocated several dozen miles to the west. Shortly thereafter, the storm dissipated on September&nbsp;27 over open waters.{{EPAC hurricane best track}}
{{Clear}}
===Hurricane Ten===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=October 1
|Dissipated=October 6
|1-min winds=75
|Pressure=996
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=10-E 1957 track.png
}}
The tenth known storm of the season was identified on October&nbsp;1 several hundred miles southwest of the tip of the Baja California Peninsula. Maximum winds observed during the storm's existence reached 85&nbsp;mph (140&nbsp;km/h); however, no air pressure was recorded. Initially tracking westward, the system turned northwest and later northeast by October&nbsp;3. After a brief acceleration on October&nbsp;4, the hurricane slowed as it neared the coastline of Baja California. Early on October&nbsp;5, the storm crossed the Peninsula as a Category 1 hurricane before entering the Gulf of California. The hurricane made another landfall near [[Navojoa, Mexico]] later that day. Rapid  weakening took place as the storm moved over the high terrain over northern Mexico. Early on October&nbsp;6, the system transitioned into an [[extratropical cyclone]] over [[New Mexico]] before dissipating several hours later.{{EPAC hurricane best track}}

At least two people were killed after their home collapsed on them a result of the storm in Mexico.<ref>{{cite news|author=Staff Writer|work=The Hartford Courant|date=October 7, 1957|title=Tropical Storm Causes Two Deaths in Mexico}}</ref> Severe cotton crop damage was reported in the Mexican states of [[Sonora]] and [[Baja California Sur]] and bridges were washed away by flood waters.<ref>{{cite news|author=Associated Press|work=The Lewiston Daily Sun|date=October 7, 1957|accessdate=January 9, 2010|title=Storm Causes Two Deaths in Mexico|url=https://news.google.com/newspapers?nid=1928&dat=19571007&id=_EcpAAAAIBAJ&sjid=LWgFAAAAIBAJ&pg=4810,3458878}}</ref>
{{Clear}}

===Hurricane Eleven===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=October 17
|Dissipated=October 20
|1-min winds=75
|Pressure=986
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=11-E 1957 track.png
}}
The season's eleven known storm identified several hundred miles south of Mexico on October&nbsp;17. Quickly tracking northwestward, the storm attained an intensity of 85&nbsp;mph (140&nbsp;km/h) during its existence. Early on October&nbsp;19, the hurricane turned due north, brushing the coastline of Mexico before dissipating just offshore the following day.{{EPAC hurricane best track}}
{{Clear}}

===Hurricane Twelve===
{{Infobox Hurricane Small
|Basin=EPac
|Formed=October 20
|Dissipated=October 22
|1-min winds=120
|Pressure=959
|Pressurepost=http://www.ncdc.noaa.gov/IPS/cdns/cdns.html
|Image=12-E 1957 track.png
}}
The strongest storm of the season, Hurricane Twelve, was first identified as a tropical depression several hundred miles southwest of [[Sonora]], Mexico on October&nbsp;20. Tracking northeastward, the system gradually intensified into a tropical storm. Late on October&nbsp;21, the storm underwent a brief period of [[Rapid deepening|explosive intensification]], strengthening from a 50&nbsp;mph (85&nbsp;km/h) tropical storm to a 140&nbsp;mph (220&nbsp;km/h) [[SSHS#Category 4|Category 4 hurricane]] in roughly six hours. The hurricane maintained this intensity for a further six hours before making landfall near [[Mazatlán]]. Rapid weakening took place as the hurricane moved inland, with the system dissipating during the afternoon of October&nbsp;22.{{EPAC hurricane best track}}

Throughout the affected region, eight people were killed by the storm. Extensive property damage was reported in the region, including the complete collapse of the local baseball stadium. Power lines and telegraph wires were downed throughout the region and high winds reportedly tossed vehicles into buildings.<ref>{{cite news|author=United Press International|work=The Deseret News|date=October 22, 1957|accessdate=January 10, 2010|title=Storm Wrecks Mexico Port|url=https://news.google.com/newspapers?id=wQQpAAAAIBAJ&sjid=PkgDAAAAIBAJ&dq=pacific%20mexico%20storm%20-audrey&pg=4878%2C4587070}}</ref> Many shrimp trawlers sank in the storm, leaving substantial losses in Mexico's shrimping industry.<ref>{{cite news|author=Staff Writer|work=New York Times|date=October 24, 1957|accessdate=January 9, 2010|title=Gale Sinks Trawlers; Mexico Shrimp Fleet Suffers in Pacific Coast Storm|url=https://select.nytimes.com/gst/abstract.html?res=F50710F63A5A177B93C6AB178BD95F438585F9}}</ref>
{{Clear}}

===Hurricane Nina===
{{Infobox Hurricane Small
|Basin=EPac
|Image=Nina 1957 track.png
|Formed=November 29
|Dissipated=December 6
|1-min winds=75
|Pressurepre = , under | Pressure=1002
}}
{{Main|Hurricane Nina (1957)}}
The last storm of the 1957 season, Hurricane Nina, was an unusually late-forming system. It was first identified on November&nbsp;29 as a tropical storm to the southwest of Hawaii.{{EPAC hurricane best track}} Steadily tracking north-northeast, the storm intensified into a Category 1 hurricane, by which time it was given the name Nina. As it approached Hawaii, the National Weather Bureau issued warnings for the storm, advising residents to take precaution and possibly evacuate.<ref name="CPHC"/> However, the storm turned westward before moving over the Hawaiian Islands with winds of 85&nbsp;mph (140&nbsp;km/h). Gradual weakening took place as the storm continued to move over open waters. Nina eventually dissipated after turning south on December&nbsp;6.{{EPAC hurricane best track}}

Waves up to {{convert|35|ft|m|abbr=on}}<ref name="CPHC"/> damaged up to 50 homes across Hawaii<ref name="Modesto Bee and News Herald">{{cite news|author=Associated Press|year=1957|title=Hurricane Nina Spends Her Fury|work=Modesto Bee and News Herald}}</ref> and roughly 12 of them being destroyed on [[Kauai]] alone.<ref name="Oakland Tribune">{{cite news|author=Associated Press|year=1957|title=Gale Forces 1,500 From Kauai Homes|work=Oakland Tribune}}</ref> One person was killed on land after being electrocuted by a downed power line.<ref name="Hayward Daily Review">{{cite news|author=Associated Press|year=1957|title=The Daily Review of December 2|work=Hayward Daily Review}}</ref> Offshore, a [[sampan]] called the ''Setsu Maru'' sent a distress call reporting that the boat was sinking 10&nbsp;miles east of [[Niihau]].<ref name="The Gleaner">{{cite news|author=Associated Press|year=1957|title=Hurricane 'Nina' Rips Hawaii; Sweeps North|work=The Gleaner}}</ref> All three people aboard the boat were reported dead as a result.<ref name="Lowell Sun">{{cite news|author=Associated Press|year=1957|title=Hurricane Hits the Hawaiian Islands; 1,100 Flee Homes|work=The Lowell Sun}}</ref> In all, damage from the hurricane was estimated at $100,000.<ref name="CPHC"/>
{{Clear}}

==Storm names==
During 1957, hurricanes that formed east of 140°W were not given names by the local warning center.<ref name="Naming">{{cite web|author=National Hurricane Center |publisher=National Oceanic and Atmospheric Administration |date=January 8, 2010 |accessdate=January 12, 2010 |title=Worldwide Tropical Cyclone Names |url=http://www.nhc.noaa.gov/aboutnames.shtml |archiveurl=http://www.webcitation.org/5sb5rygVR?url=http%3A%2F%2Fwww.nhc.noaa.gov%2Faboutnames.shtml |archivedate=2010-09-08 |deadurl=no |df= }}</ref> Those that either crossed or formed west of that point were named by the National Weather Bureau in [[Honolulu, Hawaii]]. These names were taken from the names used for [[Pacific typhoon]]s. Three names were used in 1957, they were Kanoa, Della and Nina. Due to the lack of major damage from these storms, their names were not [[Tropical cyclone naming#Retirement|retired]].<ref name="CPHC"/>{{EPAC hurricane best track}} However, once the Central Pacific adopted its own naming scheme, these names were no longer used.<ref name="Naming"/>

==Season effects==
This is a table of the storms in 1957 and their [[landfall (meteorology)|landfall(s)]], if any; the table does not include storms that did not make landfall, which is defined as the center of the storm moving over a landmass. Deaths in parentheses are additional and indirect (an example of an indirect death would be a traffic accident), but are still storm-related. Damage and deaths include totals while the storm was extratropical or a wave or low.

{{EPAC areas affected (Top)}}
|-
| '''Kanoa''' || {{Sort|0715|July 15&nbsp;– 26}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{sort|085|85&nbsp;mph (140&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Two''' || {{sort|0806|August 6&nbsp;– 16}} || bgcolor=#{{storm colour|2}}|Category 2 hurricane || bgcolor=#{{storm colour|2}}|{{sort|105|105&nbsp;mph (165&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Three''' || {{sort|0809|August 9&nbsp;– 16}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Della''' || {{Sort|0901|September 1&nbsp;– 9}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || Hawaii || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Five''' || {{Sort|0909|September 9&nbsp;– 11}} || bgcolor=#{{storm colour|TS}}|{{Sort|0|Tropical storm}} || bgcolor=#{{storm colour|TS}}|{{Sort|050|50&nbsp;mph (85&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Six''' || {{Sort|0917|September 17&nbsp;– 18}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || Mexico || {{ntsh|1}} Unknown || {{nts|7}} ||
|-
| '''Seven''' || {{Sort|0920|September 20&nbsp;– 23}} || bgcolor=#{{storm colour|TS}}|{{Sort|0|Tropical storm}} || bgcolor=#{{storm colour|TS}}|{{Sort|050|50&nbsp;mph (85&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Eight''' || {{Sort|0925|September 25&nbsp;– 28}} || bgcolor=#{{storm colour|TS}}|{{Sort|0|Tropical storm}} || bgcolor=#{{storm colour|TS}}|{{Sort|050|50&nbsp;mph (85&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Nine''' || {{Sort|0926|September 26&nbsp;– 27}} || bgcolor=#{{storm colour|TS}}|{{Sort|0|Tropical storm}} || bgcolor=#{{storm colour|TS}}|{{Sort|050|50&nbsp;mph (85&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Ten''' || {{sort|1001|October 1&nbsp;– 6}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || Mexico || {{ntsh|1|Unknown}} || {{nts|2}} ||
|-
| '''Eleven''' || {{Sort|1017|October 17&nbsp;– 20}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || None || {{ntsh|0}} None || {{ntsh|0}} None ||
|-
| '''Twelve''' || {{Sort|1020|October 20&nbsp;– 22}} ||  bgcolor=#{{storm colour|4}}|Category 4 hurricane || bgcolor=#{{storm colour|4}}|{{sort|140|140&nbsp;mph (220&nbsp;km/h)}} || Mexico || Unknown || 8 ||
|-
| '''Nina''' || {{Sort|1129|November 29&nbsp;– December 6}} || bgcolor=#{{storm colour|1}}|Category 1 hurricane || bgcolor=#{{storm colour|1}}|{{Sort|085|85&nbsp;mph (140&nbsp;km/h)}} || Mexico || {{ntsp|100000||$}} || 4
|-
{{EPAC TC Areas affected (Bottom)|TC's=13 storms|dates=July 15&nbsp;– December 6|winds=140&nbsp;mph (220&nbsp;km/h)|damage={{ntsp|100000||$}}|deaths=21|Refs=}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of Pacific hurricane seasons]]
* [[1957 Atlantic hurricane season]]
* [[1957 Pacific typhoon season]]

==References==
{{Reflist}}

==External links==
* [http://www.nhc.noaa.gov/ National Hurricane Center]

{{DEFAULTSORT:1957 Pacific Hurricane Season}}
[[Category:Pacific hurricane seasons]]
[[Category:1957 Pacific hurricane season| ]]
[[Category:Articles which contain graphical timelines]]