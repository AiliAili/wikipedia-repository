<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Milholland Legal Eagle
 | image=Legal Eagle Flight.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Ultralight aircraft]]
 | national origin=[[United States]]
 | manufacturer=[[Better Half VW]]
 | designer=[[Leonard Milholland]]
 | first flight=
 | introduced=1998
 | retired=
 | status=Plans available
 | primary user=
 | more users= 
 | produced= 
 | number built=Legal Eagle - over 100 (as of 2011)<br />Double Eagle - 6 (as of 2011)
 | program cost= 
 | unit cost=
 | developed from= 
 | variants with their own articles=
}}
|}

[[File:Legal Eagle Ultralight.jpg|right|thumb|Legal Eagle]]
[[File:LegalEagleXL.jpg|thumb|Legal Eagle XL]]

The '''Milholland Legal Eagle''' is an [[United States|American]] high wing, [[Strut|strut-braced]], single engine, [[tractor configuration]], [[conventional landing gear]]-equipped [[ultralight aircraft]] that is available as plans from [[Better Half VW]] of [[Brookshire, Texas]] and also produced in kit form by [[J&N Bolding Enterprises]] of [[Baytown, Texas]] and intended for [[Homebuilt aircraft|amateur construction]].<ref name="Cliche">Cliche, Andre: ''Ultralight Aircraft Shopper's Guide'' 8th Edition, page B-37. Cybair Limited Publishing, 2001. ISBN 0-9680628-1-4</ref><ref name="BHVWhome">{{cite web|url = http://www.betterhalfvw.com/|title = Leonard Milholland's Better Half VW Engine & the Legal Eagle Ultralight|accessdate = 26 May 2010|last = Gibson|first = Graeme|authorlink = |year = n.d.}}</ref><ref name="BHVWQA">{{cite web|url = http://www.betterhalfvw.com/qanda.htm|title = Frequently Asked Questions on Leonard Milholland's Legal Eagle Ultralight|accessdate = 26 May 2010|last = Milholland|first = Leonard|authorlink = |year = 2002}}</ref><ref name="KitplanesDec2011">Vandermeullen, Richard: ''2012 Kit Aircraft Buyer's Guide'', Kitplanes, Volume 28, Number 12, December 2011, page 46. Belvoir Publications. ISSN 0891-1851</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', page 33. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

The Legal Eagle is so named because it is capable of being built in legal compliance with the United States [[FAR 103 Ultralight Vehicles]] regulations, even when equipped with a four stroke engine.<ref name="Cliche" /><ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', page 35. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>

==Design and development==
The Legal Eagle features an open cockpit and is powered by a {{convert|30|hp|kW|0|abbr=on}} [[Half VW]] engine.<ref name="Cliche" /><ref name="WDLA15"/>

The design features a [[fuselage]] of welded [[4130 steel]] tubing, mated to an all-wood wing made from [[spruce]]. The struts and tail surfaces are made from 6061 T6 aluminium tubing. The flying surfaces are covered with doped [[aircraft fabric]]. The rear fuselage is normally left as an open truss structure to save weight and to ensure that the aircraft does not exceed the FAR 103 maximum speed of 55 knots (102&nbsp;km/h; 63&nbsp;mph). Like the fuselage, the taildragger landing gear is sprung steel.<ref name="Cliche" /><ref name="BHVWQA" />

The aircraft has been developed into several variants of the basic design including the XL version for larger pilots and a two-seat ultralight trainer and [[Light-sport aircraft]].<ref name="WDLA15"/><ref name="XL">{{cite web|url = http://www.betterhalfvw.com/LEXL/|title = Introducing the Legal Eagle XL|accessdate = 26 May 2010|last = Gibson|first = Graeme|authorlink = |year = 2009}}</ref><ref name="Double">{{cite web|url = http://www.doubleeagleairplane.com/|title = The Double Eagle - A Light, Strong, Simple & Inexpensive Airplane|accessdate = 26 May 2010|last = Gibson|first = Graeme|authorlink = |year = 2007}}</ref>
<!-- ==Operational history== -->

==Variants==
[[File:Legal Eagle Ultralight1.JPG|thumb|right|Legal Eagle]]
;Legal Eagle
:Basic model with a {{convert|244|lb|kg|0|abbr=on}} empty weight, powered by a {{convert|30|hp|kW|0|abbr=on}} [[Half VW]] engine<ref name="Cliche" /><ref name="WDLA11" /><ref name="WDLA15"/><ref name="KitplanesDec2007">Downey, Julia: ''2008 Kit Aircraft Directory'', Kitplanes, Volume 24, Number 12, December 2007, page 44. Primedia Publications. ISSN 0891-1851</ref>
;Legal Eagle XL
:Model for larger pilots with a wider and taller seat, greater wing area and longer tail. {{convert|246|lb|kg|0|abbr=on}} empty weight<ref name="WDLA11" /><ref name="WDLA15"/><ref name="XL" />
;Double Eagle
:Model with two seats in side-by-side configuration with a {{convert|385|lb|kg|0|abbr=on}} empty weight and a {{convert|900|lb|kg|0|abbr=on}} gross weight. Designed as an ultralight trainer and Light Sport Aircraft. Powered by a {{convert|60|hp|kW|0|abbr=on}} [[Volkswagen air-cooled engine]] giving a {{convert|70|mph|km/h|0|abbr=on}} cruise speed.<ref name="WDLA11" /><ref name="WDLA15"/><ref name="Double" /><ref name="KitplanesDec2007" />

==Specifications (Legal Eagle) ==
{{Aircraft specs
|ref= Cliche, Better Half VW and Kitplanes<ref name="Cliche" /><ref name="KitplanesDec2011"/><ref name="KitplanesDec2007" /><ref name="Specs">{{cite web|url = http://www.betterhalfvw.com/DETAILS.HTM|title = The Legal Eagle Ultralight Specifications|accessdate = 26 May 2010|last = Gibson|first = Graeme|authorlink = |year = 2002}}</ref>
|prime units?=imp

|genhide=

|crew=one
|capacity=
|length m=
|length ft=13
|length in=0
|length note=
|span m=
|span ft=23
|span in=6
|span note=
|height m=
|height ft=5
|height in=6
|height note=
|wing area sqm=
|wing area sqft=107
|wing area note=
|aspect ratio=
|airfoil=
|empty weight kg=
|empty weight lb=244
|empty weight note=
|gross weight kg=
|gross weight lb=500
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=5 US gallons (38 litres)
|more general=

|eng1 number=1
|eng1 name=[[Half VW]]
|eng1 type=horizontally opposed twin-cylinder, [[four-stroke]], single ignition aircraft engine
|eng1 kw=
|eng1 hp=30
|eng1 note=
|power original=

|prop blade number=2
|prop name=Culver wooden
|prop dia m=
|prop dia ft=
|prop dia in=
|prop note=

|perfhide=

|max speed kmh=
|max speed mph=60
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=50
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=25
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=80
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=100
|range nmi=
|range note=
|endurance=
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=
|roll rate=
|glide ratio=
|climb rate ms=
|climb rate ftmin=300
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=4.7
|wing loading note=

|power/mass=

|more performance=

|avionics=*none
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=
|similar aircraft=
*[[Eipper Quicksilver]]
*[[Ultraflight Lazair]]
*[[Zenair Zipper]]
|lists=<!-- related lists -->
}}

==References==
{{reflist|30em}}

==External links==
{{Commons category|Milholland Legal Eagle}}
*[http://www.betterhalfvw.com/ Legal Eagle official website]
*[http://www.doubleeagleairplane.com/ Double Eagle official website]

[[Category:United States ultralight aircraft 1990–1999]]
[[Category:Light-sport aircraft]]