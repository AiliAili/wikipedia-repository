{{distinguish|Learning society}}
{{refimprove|date=May 2013}}
A '''learned society''' (also known as a '''learned academy''', '''scholarly society''' or '''academic association''') is an [[organization]] that exists to promote an [[discipline (academia)|academic discipline]] or [[profession]], or a group of related disciplines or professions.<ref>{{cite web|url=http://www.esac.ca/about/what-is-a-learned-society/|title=The Environmental Studies Association of Canada - What is a Learned Society?|accessdate=10 May 2013}}</ref> Membership may be open to all, may require possession of some qualification, or may be an honor conferred by election.<ref name="auto">{{cite web|url=http://www.britishcouncil.org/science-uk-organisations-learned-societies.htm|title=Learned societies & academies|accessdate=10 May 2013}}</ref>

Most learned societies are [[non-profit organization]]s, and many are [[professional association]]s. Their activities typically include holding regular [[academic conference|conference]]s for the presentation and discussion of new research results and publishing or sponsoring [[academic journal]]s in their discipline. Some also act as [[professional bodies]], regulating the activities of their members in the public interest or the collective interest of the membership.

==History==
Some of the oldest learned societies are the [http://jeuxfloraux.fr/index.html Académie des Jeux floraux] (founded 1323), the [[Sodalitas Litterarum Vistulana]] (founded 1488), the [[Accademia della Crusca]] (founded 1585), the [[Accademia dei Lincei]] (founded 1603), the [[Académie Française]] (founded 1635), the [[Academy of Sciences Leopoldina]] (founded 1652), the [[Royal Society|Royal Society of London]] (founded 1660) and the [[Académie des sciences|French Academy of Sciences]] (founded 1666).

==Significance==

Scholars in the [[sociology of science]]{{who|date=September 2014}} argue that learned societies are of key importance and their formation assists in the emergence and development of new disciplines or professions.

==Structure==
Societies can be very general in nature, such as the [[American Association for the Advancement of Science]], specific to a given discipline, such as the [[Modern Language Association]], or specific to a given area of study, such as the [[Royal Entomological Society of London|Royal Entomological Society]].

Most are either specific to a particular country (e.g. the [[Entomological Society of Israel]]), though they generally include some members from other countries as well, often with local branches, or are international, such as the [[International Federation of Library Associations]] (IFLA) or the [[Regional Studies Association]], in which case they often have national branches. But many are local, such as the [[Massachusetts Medical Society]], the publishers of the internationally known ''[[New England Journal of Medicine]]''.

Some learned societies (such as the [[Royal Society of New Zealand]]) have been rechartered by legislation to form [[quango|quasi-autonomous non-governmental organizations]].

==Membership and fellowship==
Membership may be open to all, may require possession of some qualification, or may be an honor conferred by election.<ref name="auto"/> This is the case{{clarify|date=September 2014}} with some learned societies, such as the [[Poland|Polish]] [[Sodalitas Litterarum Vistulana]] (founded 1488), the Italian [[Accademia dei Lincei]], the [[Académie Française]], the [[German Academy of Sciences Leopoldina]], the UK's [[Royal Society]] and [[Royal Academy of Engineering]] or the [[French Academy of Sciences]].

Some societies offer membership to those who have an interest in a particular subject or discipline, provided  they pay their membership fees. Older and more  academic/professional societies may offer associateships and/or fellowships to [[fellow]]s who are appropriately qualified  by ''[[Honorary degree|honoris causa]]'', or by submission of a portfolio of work or an original thesis.  A benefit of membership may be discounted subscription rates for the publications of the society. Many of these societies award [[post-nominal letters]] to their memberships.

==Online academic communities==
Following the [[globalization]] and the development of information technology, certain scholarly societies—such as the [[Modern Language Association]]—have created virtual communities for their members.  In addition to established academic associations, academic [[virtual communities]] have been also organized that in some cases became even more important platform for interaction and scientific collaborations among researchers and faculty than traditional scholarly societies.{{citation needed|date=September 2014}}
Members of these online academic communities, grouped by areas of interests, use for their communication shared and dedicated listservs (for example [[JISCMail]]), social networking services (like [[Facebook]], [[Linkedin]]) and academic oriented social networks (like [[Mendeley]], [[Academia.edu]]).<ref>{{cite web|url=http://www.elsevier.com/connect/how-virtual-science-communities-are-transforming-academic-research|title=How virtual science communities are transforming academic research|accessdate=10 May 2013}}</ref><ref>{{cite journal|url=http://www.sciencedirect.com/science/article/pii/S0747563213004019|title=Participation in virtual academic communities of practice under the influence of technology acceptance and community factors.|accessdate=10 May 2013 | doi=10.1016/j.chb.2013.10.051}}</ref>

==See also==

{{main cat|Learned societies}}
* [[Academic conference]]s
* [[List of learned societies]]
* [[National academy]]
* [[Professional association]]
* [[Text publication society]]

==Notes and references==
{{Reflist|30em}}

==External links==
{{EB1911 Poster|Societies, Learned|Learned society}}
* [http://www.lib.uwaterloo.ca/society/overview.html Scholarly Societies Project from the University of Waterloo Libraries]—database of hundreds of scholarly societies in various fields, including some of the oldest societies
* [http://www.morrin.org/eclectica Eclectica], virtual exhibit on the history of Canadian learned societies.
*https://commons.mla.org The online scholarly network of the Modern Language Association.

{{Authority control}}
{{Portal bar|Science}}

{{DEFAULTSORT:Learned Society}}
[[Category:Learned societies| ]]
[[Category:Types of organization]]
[[Category:Clubs and societies]]