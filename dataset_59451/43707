<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=SA-5 Flut-R-Bug
 | image=Stits SA6B Flutrbug LX-PUR Lux Findel 27.07.65 edited-3.jpg
 | caption=SA-6B Flut-R-Bug at [[Luxembourg Airport]] in 1965
}}{{Infobox Aircraft Type
 | type=[[Homebuilt aircraft]]
 | national origin=[[United States]]
 | manufacturer=Stits Aircraft Company
 | designer=Ray Stits
 | first flight=1956
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=27 full kits sold, 1200 set of plans sold.<ref>{{cite book|title=Kitplane Construction|author=Ronald J. Wanttaja|page=5}}</ref>
 | program cost= <!--Total program cost-->
 | unit cost=[[US$]]1250 in 1956 <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Stits SA-5 Flut-R-Bug''' is a [[homebuilt aircraft]] designed by Ray Stits.

==Design and development==
The Flut-R-Bug can be built as a single place or [[tandem]] seat aircraft. The Flut-R-Bug was an early complete-kit aircraft, sold with a pre-welded [[fuselage]]. Stits planned to deliver 100 kits to the German market for homebuilding.<ref>{{cite journal|magazine=Flying Magazine|date=July 1956|title=Check Pilot Report ion the Flut-R-Bug}}</ref> Examples have been completed in the United States and in Europe.

The SA-5 is a [[mid-wing]], [[tricycle landing gear]] design with folding wings. The aircraft was intended to be towed by a vehicle by the (lowered) tail on its main gear with wings folded along its sides. The cockpit can be open, or covered with a [[bubble canopy]]. The fuselage is constructed from welded steel tubing with [[aircraft fabric covering]]. The wings use [[spruce]] wooden [[spar (aviation)|spar]]s with fabric covering.<ref>{{cite journal|magazine=Sport Aviation|date= January 1959|page=25}}</ref><!-- ==Operational history== -->

==Variants==
;SA-5A
:Single place variant
;SA-6A
:Two seat tandem variant with a {{convert|1015|lb|kg|0|abbr=on}} gross weight.<ref>{{cite journal|magazine=Flying Magazine|date=November 1960|title=EAA Fly-In|page=36}}</ref>
;SA-6B
:Two seat variant with wider span wings and larger tail surface.<ref>{{cite journal|magazine=Flying Magazine|date=March 1957|title=Report on the Stits Homebuilt|author=Dean McCarty}}</ref>

==Specifications (Stits SA-5 Flut-R-Bug) ==
{{Aircraft specs
|ref=Sport Aviation<!-- for giving the reference for the data -->
|prime units?=kts<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=

|crew=1
|capacity=1 passenger
|length m=
|length ft=17
|length in=
|length note=
|span m=
|span ft=23
|span in=
|span note=
|height m=
|height ft=
|height in=70
|height note=
|wing area sqm=
|wing area sqft=103.5
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=NACA 4412
|empty weight kg=
|empty weight lb=528
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|fuel capacity={{convert|9|u.s.gal}}
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Continental A65]]
|eng1 type=Horizontally opposed piston
|eng1 kw=<!-- prop engines -->
|eng1 hp=65<!-- prop engines -->

|prop blade number=2<!-- propeller aircraft -->
|prop name=[[Sensenich]]
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=6<!-- propeller aircraft -->
|prop dia in=0<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=100
|max speed kts=
|max speed note=
|cruise speed kmh=
|cruise speed mph=80
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=40<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|range km=
|range miles=200
|range nmi=
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=12,000
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=1000
|climb rate note=
|time to altitude=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=6.4
|wing loading note=
|power/mass=
|thrust/weight=
|more performance=
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==References==
{{commons category|Stits SA-5 Flut-R-Bug}}
{{reflist}}
<!-- ==External links== -->

{{Stits aircraft}}

[[Category:Homebuilt aircraft]]