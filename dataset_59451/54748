{{italic title}}

[[File:Mother and Child by Katharine R. Wireman.jpg|thumb|Illustration by Katharine R. Wireman from "The Birds' Christmas" by Kate Douglas Wiggin]]
'''''The Birds' Christmas Carol''''' is a novel by [[Kate Douglas Wiggin]] printed privately in 1886 and published in 1888<ref name=":0">{{Cite book|url=https://books.google.com/books?id=OSuXAAAAQBAJ&lpg=PA41&dq=birds%20christmas%20carol&pg=PA41#v=onepage&q=birds%20christmas%20carol&f=false|title=The Christmas Encyclopedia, 3d ed.|last=Crump|first=William D.|date=2001-09-15|publisher=McFarland|isbn=9781476605739|location=|pages=41|language=en|quote=|via=Google Books}}</ref> with illustrations by [[Katharine R. Wireman]].  Wiggin published the book to help fund the Silver Street Free Kindergarten, which she founded in 1878.<ref name=":0" />

The story is about Carol Bird, a Christmas-born child, a young girl who is unusually loving and generous, having a positive effect on everyone with whom she comes into contact.  She is the youngest member of her family and has several devoted older brothers.  At about the age of 5, Carol contracts an unspecified illness (possibly tuberculosis), and, by the time she is 10, she is bedridden; physicians say that she does not have long to live.  Most of the brief novel's plot involves Carol making plans for a Christmas celebration for the nine Ruggles children, a poor, working-class family living near the Birds.  

Although Wiggin's story is basically a moral tale about a saintly child, Carol is unaffected and cheerful rather than pious.  The story is also enlivened by many humorous touches, particularly in the scenes of the Ruggles family's home life.

In 1917, the story was adapted as the silent movie "A Bit o' Heaven" starring [[Mary Louise (actor)|Mary Louise]] as Carol, [[Donald Watson]] and [[Ella Gilbert]] as Mr. and Mrs. Bird and [[Mary Talbot]] as Mrs. Ruggles.<ref name=":0" />

==References== 
{{Reflist}}

==External links==
{{gutenberg|no=24286|name=The Birds' Christmas Carol}}

*

{{Librivox book}}

{{DEFAULTSORT:Birds Christmas Carol, The}}
[[Category:1887 novels]]
[[Category:Christmas novels]]
[[Category:19th-century American novels]]
[[Category:American novels adapted into films]]

{{1880s-novel-stub}}