{{Multiple issues|
{{COI|date=November 2014}}
{{Self-published|date=November 2014}}
{{Third-party|date=November 2014}}}}
{{Use American English|date=January 2014}}
{{Infobox spaceflight
| name                  = Geostationary Operational Environmental Satellites - R Series
| names_list            = GOES-16 <!--list of previous names if the spacecraft has been renamed. 
Include the dates applicable if possible, and separate each name with a linebreak. 
Omit if the spacecraft has only ever been known by one name. 
Do not include Harvard, COSPAR/NSSDC or SATCAT/NORAD/NASA designations as alternative names-->

<!--image of the spacecraft/mission-->
| image                 = GOES-R SPACECRAFT.jpg
| image_caption         = 
| image_alt             = <!--image alt text-->
| image_size            = 260px

<!--Basic details-->
| mission_type          = Earth weather forecasting, [[space weather]]<!--eg. Technology, Reconnaissance, ISS assembly, etc-->
| operator              = [[National Oceanic and Atmospheric Administration|NOAA]]{{\}}[[NASA]] <!--organisation(s) that operate(d) the spacecraft-->
| Harvard_designation   = <!--spacecraft launched 1962 and earlier only (eg. 1957 Alpha 2)-->
| COSPAR_ID             = 2016-071A
| SATCAT                = 41866
| website               = {{url|http://www.goes-r.gov}}
| mission_duration      = 15 years (planned)<br/>Elapsed: {{time interval|19 November 2016|show=ymd|sep=,}}
| distance_travelled    = <!--How far the spacecraft travelled (if known)-->
| orbits_completed      = <!--number of times the spacecraft orbited the Earth - see below for spacecraft beyond Earth orbit-->
| suborbital_range      = <!--downrange distance reached if spacecraft did not enter orbit-->
| suborbital_apogee     = <!--altitude reached if spacecraft did not enter orbit-->

<!--Spacecraft properties-->
| spacecraft            = <!--Spacecraft name/serial number (eg. Space Shuttle ''Discovery'', Apollo CM-118), etc-->
| spacecraft_type       = GOES-R series
| spacecraft_bus        = [[A2100]]
| manufacturer          = [[Lockheed Martin]]<ref>{{cite web|url=http://www.lockheedmartin.com/us/products/geostationary-operational-environmental-satellite-r-series--goes.html|title=GOES-R|publisher=|accessdate=7 March 2017}}</ref>
| launch_mass           = {{convert|5192|kg|lb}}
| BOL_mass              = <!--spacecraft mass in orbit at beginning of operational life, after LEOP phase-->
| dry_mass              = {{convert|2857|kg|lb}}
| payload_mass          = <!--Mass of cargo carried by spacecraft (eg. for Space Shuttle), or total mass of instrumentation/equipment/experiments for mission-->
| dimensions            = 6.1 x 5.6 x 3.9 m
| power                 = 4 kW

<!--Launch details-->
| launch_date           = {{start date|2016|11|19|23|42|00|TZ=Z}}
| launch_rocket         = [[Atlas V|Atlas V541 AV-069]] <ref>{{cite web|url=https://spaceflightnow.com/tag/av-069/|title=AV-069 – Spaceflight Now|publisher=|accessdate=7 March 2017}}</ref>
| launch_site           = [[Cape Canaveral Air Force Station|Cape Canaveral]] [[Cape Canaveral Air Force Station Space Launch Complex 41|SLC-41]]
| launch_contractor     = [[United Launch Alliance]]
| deployment_from       = <!--place where deployed from-->
| deployment_date       = <!--date deployed-->
| entered_service       = <!--date on which the spacecraft entered service, if it did not do so immediately after launch-->

<!--end of mission-->
| disposal_type         = <!--Whether the spacecraft was deorbited, decommissioned, placed in a graveyard orbit, etc-->
| deactivated           = <!--when craft was decommissioned-->
| destroyed             = <!--when craft was destroyed (if other than by re-entry)-->
| last_contact          = <!--when last signal received if not decommissioned-->
| recovery_by           = <!--recovered by-->
| recovery_date         = <!--recovery date-->
| decay_date            = <!--when craft re-entered the atmosphere, not needed if it landed-->

<!--
    The following template should be used for ONE of the three above fields "end_of_mission", "decay" or "landing" if the spacecraft is no longer operational.
    If it landed intact, use it for the landing time, otherwise for the date it ceased operations, or the decay date if it was still operational when it re-entered.
    {{end date|YYYY|MM|DD|hh|mm|ss|TZ=Z}} (for Zulu/UTC) or {{end date|YYYY|MM|DD}} (if time unknown) 
-->

<!--orbit parameters-->
<!--as science-related articles, SI units should be the principal units of measurement, however we usually use {{convert}} to display imperial units in parentheses after the initial values-->
| orbit_reference       = [[Geocentric orbit|Geocentric]]
| orbit_regime          = [[Geostationary orbit|Geostationary]] 
| orbit_longitude       = 105° West 
| orbit_slot            = <!--Designation of orbital position or slot, if not longitude (e.g plane and position of a GPS satellite)-->
| orbit_semimajor       = {{convert|42,164.0|km|mi}}
| orbit_eccentricity    = 0.0001251
| orbit_periapsis       = {{convert|35,788.1|km|mi}}
| orbit_apoapsis        = {{convert|35,798.7|km|mi}}
| orbit_inclination     = 0.01°
| orbit_period          = 1,436.1 minutes
| orbit_RAAN            = 127.4027°
| orbit_arg_periapsis   = 240.1274°
| orbit_mean_anomaly    = 352.4533°
| orbit_mean_motion     = 1.00273071
| orbit_repeat          = <!--repeat interval/revisit time-->
| orbit_velocity        = <!--speed at which the spacecraft was travelling at epoch - only use for spacecraft with low orbital eccentricity-->
| orbit_epoch           = {{start date|23 January 2017}}
| orbit_rev_number      = <!--revolution number-->
| apsis                 = gee

<!--transponder parameters-->
| trans_band            = <!--Transponder frequency bands-->
| trans_frequency       = <!--specific frequencies-->
| trans_bandwidth       = <!--bandwidth-->
| trans_capacity        = <!--capacity of the transponders-->
| trans_coverage        = <!--area covered-->
| trans_TWTA            = <!--TWTA output power-->
| trans_EIRP            = <!--equivalent isotropic power-->
| trans_HPBW            = <!--half-power beam width-->

<!--Only use where a spacecraft/mission is part of a clear programme of sequential missions. 
If in doubt, leave it out-->
| programme             = [[Geostationary Operational Environmental Satellite|GOES]]
| previous_mission      = [[GOES 15]]
| next_mission          = [[GOES-S]]

<!--mission insignia or patch-->
| insignia              = GOES-R_LOGO_SMALL.jpg 
| insignia_caption      = GOES-R insignia mission
| insignia_alt          = <!--image alt text-->
| insignia_size         = 179px
}}

'''GOES-16''', previously known as '''GOES-R''',<ref>{{cite web|url=https://www.nesdis.noaa.gov/GOES-R-Launch|title=GOES-16 - NOAA NESDIS|publisher=|accessdate=7 March 2017}}</ref> is an American weather satellite, which, upon completion of testing, will form part of the Geostationary Operational Environmental Satellite (GOES) system operated by the U.S. National Oceanic and Atmospheric Administration. It is the first of the next generation of [[geosynchronous satellite|geosynchronous]] [[environmental satellite]]. It is expected to provide [[atmosphere|atmospheric]] and surface measurements of the Earth’s [[Western Hemisphere]] for [[weather forecasting]], severe storm tracking, [[space weather]] monitoring and [[meteorology|meteorological]] research. GOES-16 launched at approximately 23:42 UTC on November 19, 2016 from the [[Cape Canaveral Air Force Station]], [[Florida]], [[United States]].

==Overview==
GOES-16 is a follow-on to the current [[Geostationary Operational Environmental Satellite|GOES]] system that is used by [[National Oceanic and Atmospheric Administration|NOAA's]] [[National Weather Service]] for weather monitoring and forecasting operations as well as by researchers for understanding interactions between land, ocean, atmosphere and climate.  The GOES-R series program is a collaborative effort between NOAA and [[NASA]] to develop, deploy and operate the satellites.  They are managed from [[Goddard Space Flight Center]] in Greenbelt, Maryland. The GOES-R series (GOES-R, S, T, & U) will extend the availability of the operational [[Geostationary Operational Environmental Satellite|GOES]] satellite system through 2036.<ref>{{cite web|url=http://www.nesdis.noaa.gov/flyout_schedules.html |title=Flyout Charts/Schedules - NOAA's Satellite and Information Service (NESDIS) |website=Nesdis.noaa.gov |date=2015-09-21 |accessdate=2016-02-10}}</ref>

GOES-16 has several improvements over the old GOES system.<ref name=ProvG>{{cite web |archiveurl=https://web.archive.org/web/20120205071513/http://www.goes-r.gov/users/proving-ground.html |url=http://www.goes-r.gov/users/proving-ground.html |archivedate=February 5, 2012 |title=Proving Ground |work=GOES-R |publisher= [[NASA]], [[NOAA]]}}</ref>  Its advanced instruments and data processing provides:

*Three times more [[Spectral resolution|spectral information]]
*Four times greater [[Angular resolution|spatial resolution]]
*Five times faster coverage
*Real-time mapping of total [[lightning]] activity
*Increased [[thunderstorm]] and [[tornado]] warning lead time
*Improved [[Tropical cyclone|hurricane]] track and intensity forecasts
*Improved monitoring of solar x-ray flux
*Improved monitoring of [[solar flare]]s and [[coronal mass ejection]]s
*Improved [[geomagnetic storm]] forecasting<ref>{{cite web|url=http://www.goes-r.gov/downloads/tri-brochure.pdf |format=PDF |title=GOES-R Brochure |website=Goes-r.gov |accessdate=2016-02-10}}</ref>

==Spacecraft==
The GOES-16 spacecraft [[A2100]] bus is 3-axis stabilized and designed for 10 years of on-orbit operation preceded by up to 5 years of on-orbit storage. It will provide near-continuous observations as well as vibration isolation for the Earth-pointed optical bench and high-speed spacecraft-to-instrument interfaces designed to maximize data collection. The cumulative time that GOES-16 science data collection (including imaging) is interrupted by momentum management, station-keeping and yaw flip maneuvers will be under 120 minutes/year. This represents a nearly two order of magnitude improvement compared to older [[Geostationary Operational Environmental Satellite|GOES]] satellites.

==Instruments==
The GOES-16 instrument suite includes three types of instruments: Earth sensing, solar imaging, and space environment measuring.<ref>{{cite web|url=http://www.goes-r.gov/spacesegment/spacecraft.html |title=GOES-R Spacecraft Overview |work=GOES-R |publisher=[[NASA]], [[NOAA]] |accessdate=November 2, 2014}}</ref>

=== Earth facing ===
Two instruments point toward Earth:
[[File:ABI Prototype Model.png|thumb|left|ABI Prototype Model]]

==== Advanced Baseline Imager ====
The Advanced Baseline Imager (ABI), designed and built by Exelis Geospatial Systems (now Harris Space & Intelligence Systems), is the primary instrument on GOES-16 for imaging Earth’s weather, climate and environment. ABI will be able to view the Earth across 16 [[spectral bands]], including two visible channels, four near-infrared channels and ten [[infrared]] channels. It will provide three times more spectral information, up to four times the spatial resolution (depending on the band), and more than five times faster coverage. Forecasters will be able to use the higher resolution images to track the development of storms in their early stages.<ref>{{cite web|url=http://www.goes-r.gov/downloads/2012-AMS/02/Schmit.pdf |format=PDF |title=The ABI on GOES-R |website=Goes-r.gov |accessdate=2016-02-10}}</ref> Instruments nearly identical to ABI have been delivered to [[Japan]] for use on [[Himawari 8]] and [[Himawari 9]].

==== Geostationary Lightning Mapper ====
[[File:GLM Engineering Development Unit.jpg|thumb|right|GLM Engineering Development Unit]]
The GOES-16 Geostationary Lightning Mapper (GLM) will take continuous day and night measurements of the frequent intra-cloud lightning that accompanies many severe storms, and will do so even when the high-level [[cirrus cloud]]s atop mature thunderstorms may obscure the underlying convection from the imager. Research and testing has demonstrated the GLM potential for improvement in tornado warning lead time and false alarm rate reduction.<ref name="GLM-slides">{{cite web|url=http://www.goes-r.gov/downloads/2012-Science-Week/pres/tues/Goodman.pdf |format=PDF |title=The Geostationary Lightning Mapper (GLM) for the GOES-R Series of Geostationary Satellites |website=Goes-r.gov |accessdate=2016-02-10}}</ref> It is anticipated that GLM data will have applications to aviation weather services, climatological studies, and severe thunderstorm forecasts and warnings. The GLM will provide information to identify growing, active, and potentially destructive thunderstorms over land as well as ocean areas.<ref>{{cite web|url=http://www.goes-r.gov/downloads/2010-AMS/GLM-Goodman.pdf |format=PDF |title=The Geostationary Lightning Mapper (GLM) on the GOES-R Series: | website=Goes-r.gov |accessdate=2016-02-10}}</ref>

Studies show that a sudden increase in Total Lightning Activity—or flash rate—correlates with an increase in storm intensity. Thus, a significant increase in a storm's flash rate may signify a storm becoming severe and causing damaging winds, large hail, and/or tornadoes.<ref>{{cite web|url=https://www.youtube.com/watch?v=UDb6xb07OSI |title=Lightning, Tornadoes, and the Future of NOAA Satellites [1080p |publisher=YouTube |date= |accessdate=2016-02-10}}</ref>

The GLM consists of a telescopic CCD camera sensitive to 777.4 nm light. It has a spatial resolution of {{convert|8|km}} (at nadir) to {{convert|14|km}} (at edge of field of view) and captures 500 frames per second. The CCD's pixel pitch varies across its area.<ref name="GLM-slides"/>

=== Sun facing ===
Two sun-facing instruments are mounted to the arm holding the solar panel: the Solar Ultraviolet Imager and the Extreme Ultra Violet (EUVS) / X-Ray Irradiance Sensors.

==== Solar Ultraviolet Imager ====
Solar Ultraviolet Imager (SUVI) is a telescope that observes the Sun in the [[extreme ultraviolet]] (EUV) wavelength range. SUVI will observe and characterize complex, active regions of the Sun, [[solar flare]]s and the eruptions of solar filaments that may give rise to [[coronal mass ejection]]s. Depending on the size and the trajectory of solar eruptions, the possible effects to the Earth’s environment, referred to as space weather, include the disruption of power utilities, communication and navigation systems and possible damage to orbiting satellites and the [[International Space Station]]. SUVI observations of flares and solar eruptions will provide an early warning of possible impacts to the Earth environment and enable better forecasting of potentially disruptive events.<ref>{{cite web|url=http://www.satnews.com/cgi-bin/story.cgi?number=356899010 |title=Satnews Publishers: Daily Satellite News |website=Satnews.com |date=2009-12-30 |accessdate=2016-02-10}}</ref> [[File:Simulated SUVI image.jpg|thumb|Simulated GOES-R Solar Ultraviolet Imager image]]
[[File:GOESR Poster.png|thumb|GOES-R will offer advanced imaging for more accurate forecasts, real-time mapping of lightning activity, and improved monitoring of solar activity.]]

==== Extreme Ultra Violet (EUVS) / X-Ray Irradiance Sensors ====
The Extreme Ultraviolet and X-Ray Irradiance Sensors (EXIS) detect solar soft X-ray [[irradiance]] and solar extreme ultraviolet spectral irradiance in the 5-127&nbsp;nm range. The [[Solar X-ray Imager|X-Ray Sensor (XRS)]] monitors solar flares that can disrupt communications and degrade navigational accuracy, affecting satellites, astronauts, high latitude airline passengers and [[Power Grid|power grid]] performance. The Extreme Ultraviolet Sensor monitors solar variations that directly affect satellite drag/tracking and [[Ionosphere|ionospheric]] changes, which impact communications and navigation operations. This information is critical to understanding the outer layers of the Earth’s atmosphere.<ref>{{cite web|url=http://www.goes-r.gov/spacesegment/exis.html |title=GOES-R EXIS Instrument Page |website=Goes-r.gov |date=2014-07-07 |accessdate=2016-02-10}}</ref>

=== Space environment ===
Two in-situ instruments will monitor the space environment: the Space Environment In-Situ Suite and a magnetometer.

==== Space Environment In-Situ Suite ====
The Space Environment In-Situ Suite (SEISS) consists of an array of sensors that will monitor the [[proton]], [[electron]] and heavy ion fluxes at [[geosynchronous orbit]]. The data will be used for assessing radiation hazards to astronauts and satellites. In addition to hazard assessment, the data can be used to warn of high flux events, mitigating damage to radio communication. The instrument suite consists of the Energetic Heavy Ion Sensor (EHIS), the Magnetospheric Particle Sensor - High and Low (MPS-HI and MPS-LO), and the Solar and Galactic Proton Sensor (SGPS).  Data will drive the solar radiation storm portion of [[National Oceanic and Atmospheric Administration|NOAA]] [[space weather]] scales and other alerts and warnings and will improve [[Solar energetic particles|solar energetic particle]] forecasts.<ref>{{cite web|url=http://www.goes-r.gov/spacesegment/seiss.html |title=GOES-R SEISS Instrument Page |website=Goes-r.gov |date=2014-07-07 |accessdate=2016-02-10}}</ref>

==== Magnetometer ====
The GOES-16 Magnetometer (MAG) will measure the space environment magnetic field that controls charged particle dynamics in the outer region of the [[magnetosphere]]. These particles can be dangerous to spacecraft and human spaceflight. The [[Earth's magnetic field|geomagnetic field]] measurements will provide alerts and warnings to satellite operators and power utilities. This data will also be used in research. Data products will be part of NOAA [[space weather]] operations, providing information on the general level of geomagnetic activity and permitting detection of sudden magnetic storms. In addition, measurements will be used to validate large-scale space environment models that are used in operations.<ref>{{cite web|url=http://www.goes-r.gov/spacesegment/mag.html |title=GOES-R Magnetometer |website=Goes-r.gov |date=2014-07-07 |accessdate=2016-02-10}}</ref>

==Unique Payload Services ==
The GOES-16 Unique Payload Services (UPS) consist of [[transponder]] payloads that provide communications relay services in addition to primary mission data. The UPS suite consists of the Data Collection System (DCS), the High Rate Information Transmission/Emergency Managers Weather Information Network (HRIT/EMWIN), GOES-16 Rebroadcast (GRB), and the Search and Rescue Satellite Aided Tracking (SARSAT) System.

GOES-16 Rebroadcast (GRB) is the primary space relay of Level 1b products and will replace the GOES VARiable (GVAR) service. GRB will provide full resolution, calibrated, navigated, near-real-time direct broadcast data. The data distributed via GRB service will be the full set of Level 1b products from all instruments aboard the GOES-R series spacecraft. This concept for GRB is based on analysis that a dual-pole circularly-polarized [[L band|L-band]] link of 12&nbsp;MHz bandwidth can support up to a 31-megabits per second (Mbps) data rate – enough to include all ABI channels in a lossless compressed format as well as data from GLM, SUVI, EXIS, SEISS and MAG.<ref>{{cite web|url=http://www.goes-r.gov/users/grb.html |title=GOES Rebroadcast |website=Goes-r.gov |date= |accessdate=2016-02-10}}</ref>
[[File:SARSAT Overview.jpg|right|SARSAT System Overview]]
The Data Collection System (DCS) is a relay system used to collect information from a large number of Earth-based platforms that transmit in-situ environmental sensor data on predefined frequencies and schedules, in response to thresholds in sensed conditions, or in response to requests. Enhancements to the DCS program during the GOES-16 era include expansion in the number of user-platform channels from 266 to 433.<ref>{{cite web|url=http://www.noaasis.noaa.gov/DCS/htmfiles/intro.html |format=PDF |title=GOES Data Collection System |website=Goes-r.gov |accessdate=2016-02-10}}</ref>

The Emergency Managers Weather Information Network (EMWIN) is a direct service that provides users with weather forecasts, warnings, graphics and other information directly from the [[National Weather Service]] in near-real time. The GOES EMWIN relay service is one of a suite of methods to transmit these products to end users. The HRIT service provides broadcast of low-resolution GOES satellite imagery data and selected products to remotely located user HRIT terminals.<ref>{{cite web|url=http://www.goes-r.gov/users/hrit.html |title=GOES-R HRIT/EMWIN Overview page |website=Goes-r.gov |date=2014-07-07 |accessdate=2016-02-10}}</ref>

As an integral part of the international search and rescue satellite program called [[COSPAS-SARSAT]], NOAA operates the Search and Rescue Satellite Aided Tracking (SARSAT) System to detect and locate mariners, aviators and other recreational users in distress almost anywhere in the world at any time and under almost any conditions. This system uses a network of satellites to detect and locate distress signals from [[Distress radiobeacon|emergency beacons]] onboard aircraft, vessels and from handheld personal locator beacons (PLB). The SARSAT transponder on the GOES-16 satellite will provide the capability to immediately detect distress signals from emergency beacons and relay them to ground stations - called Local User Terminals. In turn, this signal is routed to a SARSAT Mission Control Center and then sent to a Rescue Coordination Center which dispatches a search and rescue team to the location of the distress.<ref>{{cite web|url=http://www.sarsat.noaa.gov/ |title=NOAA - Search and Rescue Satellite Aided Tracking - Welcome |website=Sarsat.noaa.gov |date=2016-01-22 |accessdate=2016-02-10}}</ref>

==Ground system==
[[File:NOAA NSOF2.png|thumb|right|NOAA Satellite Operations Facility, Suitland, Maryland]]
[[File:NOAA WCDAS2.png|thumb|right|Wallops Command and Data Acquisition Station, Wallops Island, Virginia]]
NOAA is developing a ground system (GS) to receive data and generate and distribute it in real-time.<ref>{{cite web|url=http://www.goes-r.gov/ground/overview.html |title=GOES-R Ground Segment Overview |website=Goes-r.gov |date= |accessdate=2016-02-10}}</ref> The key functions of the GS are Mission Management, Product Generation, Product Distribution and Enterprise Management/Infrastructure. The ground system will operate from multiple locations:
* NOAA Satellite Operations Facility (NSOF): Located in [[Suitland, Maryland]], the NSOF will house the majority of GOES-16 mission operations. The EM, PG, and PD functions will be performed there. The majority of operations and product staff will also be housed at NSOF. Four existing 9.1-meter antennas will be upgraded for compatibility with GOES-16. These antennas will maintain compatibility with existing [[Geostationary Operational Environmental Satellite|GOES]] satellites and will operate continuously for the life of the GOES-R series.
* [[Wallops Flight Facility|Wallops Command and Data Acquisition Station (WCDAS)]]: Located in Wallops, Virginia, WCDAS will be the primary site for space-to-ground radio frequency (RF) communications. Level 1b data will be processed at WCDAS to produce GOES Rebroadcast (GRB) for satellite uplink. WCDAS will also provide uplink to the satellites to support UPS. Three new 16.4-meter antennas will be constructed at WCDAS. They are designed to withstand sustained winds of 110&nbsp;mph (Category 2 hurricane) and to survive (in a stowed position) gusts of up to 150&nbsp;mph ([[Saffir–Simpson hurricane wind scale|Category 4 hurricane]]). These antennas will be compatible with existing GOES satellites and will operate continuously for the life of the GOES-R series.
* Remote Backup (RBU) Facility: Located in [[Fairmont, West Virginia]], RBU's primary function will be to support contingency operations and perform the critical functions of NSOF and WCDAS through the production and distribution of life and property products. RBU will provide product generation for all Key Performance Parameters (KPPs). It will also serve as a backup during system/equipment testing or maintenance. Three new 16.4-meter antennas will be constructed there to the same specifications as at WCDAS.

==User community==
GOES-16 is preparing the satellite data user community for the coming types of imagery and data. Users include individuals, companies and institutions.

GOES-16 data is expected to appeal to a wide variety of users. Larger, institutional users can exploit the increases in spectral, temporal and spatial resolution for greater accuracy. GOES-16 data will be used in real time for life and property forecasting and warning applications primarily by the [[National Weather Service]], where these users will be able to monitor severe storms. Other smaller public and private sector users will be able to obtain GOES-16 data through their own Earth stations, or through network distribution.<ref>{{cite web|url=http://www.goes-r.gov/users/user-readiness-overview.html |title=GOES-R User Readiness Overview |website=Goes-r.gov |date=2015-09-10 |accessdate=2016-02-10}}</ref>

===Cooperative institutes===
[[File:GOES-R CI Map Updated 6.12.13.png|thumb|right|GOES-R Cooperative Institutes]]
Cooperative Institutes (CIs) are non-federal academic and non-profit research institutions supported by NOAA that provide resources to the agency's mission, goals, and strategic plans.<ref>{{cite web|url=http://ci.noaa.gov/Locations.aspx |title=CI Locations |website=Ci.noaa.gov |date= |accessdate=2016-02-10}}</ref> Eight [[National Oceanic and Atmospheric Administration|NOAA]] CIs support GOES-16:

:*[http://www.climateandsatellites.org/ Cooperative Institute for Climate and Satellites (CICS)]
:*[http://www.cifar.uaf.edu/ Cooperative Institute for Alaska Research (CIFAR)]
:*[http://www.cimms.ou.edu/  Cooperative Institute for Mesoscale Meteorological Studies (CIMMS)]
:*[[Cooperative Institute for Meteorological Satellite Studies]] (CIMSS)
:*[http://cioss.coas.oregonstate.edu/CIOSS/index.html/  Cooperative Institute for Oceanographic Satellite Studies (CIOSS)]
:*[[Cooperative Institute for Research in the Atmosphere]] (CIRA)
:*[[Cooperative Institute for Research in Environmental Sciences]] (CIRES)
:*[http://www.soest.hawaii.edu/jimar/  Joint Institute for Marine and Atmospheric Research (JIMAR)]

===Proving ground===
Many GOES-16 products will be aimed at monitoring severe weather and helping forecasters issue earlier, more accurate, severe weather warnings. The Proving Ground was established to prepare operational forecasters for the amount of data that will be available with the GOES-R series and ensure maximum utilization when the satellites are launched and operational. Its goals are: training forecasters to use new products, identifying different utilities of each product, identifying weaknesses or errors with each product, user-feedback development, and day one readiness for GOES-R operations.<ref name=ProvG/><ref>{{cite web|url=http://www.goes-r.gov/users/proving-ground.html |title=GOES-R Proving Ground Page |website=Goes-r.gov |date=2015-06-05 |accessdate=2016-02-10}}</ref>

==Launch vehicle==
[[File:GOESR-LAUNCH-1-480x270.jpg|thumb|Launch of GOES R satellite, 11/19/2016.]]

The GOES-16 satellite launched aboard an [[Atlas V|Atlas V 541]] [[Expendable launch system|expendable launch vehicle]] from [[Space Launch Complex 41]] at [[Cape Canaveral Air Force Station]], Florida.<ref>{{cite news|title=NASA Awards Launch Contract For Goes-R And Goes-S Missions |url=http://www.nasa.gov/home/hqnews/2012/apr/HQ_C12-016_GOES-R_GOES-S_Launch.html |first2=George H. |last2=Diller |publisher=[[NASA]] |date=April 5, 2012 |accessdate=November 2, 2014 |first=Michael |last=Braukus}}</ref><ref>[http://spaceref.biz/company/lockheed-martin-completes-assembly-of-next-generation-weather-satellite.html Lockheed Martin Completes Assembly of Next-Generation Weather Satellite]. 4 June 2015</ref>

==See also==
{{Div col||30em}}
*[[Cospas-Sarsat]]
*[[Earth observation satellite]]
*[[Geostationary orbit]]
*[[Geostationary Operational Environmental Satellite]]
*[[Remote sensing]]
*[[Space Weather Prediction Center]]
*[[Storm Prediction Center]]
*[[Weather satellite]]
{{Div col end}}

==References==
{{Reflist|30em}}

==Attributions==
*{{NASA|article=Mission Overview|url=https://web.archive.org/web/20120410001407/http://www.goes-r.gov/mission/mission.html}}
*{{NASA|article=Proving Ground|url=https://web.archive.org/web/20120205071513/http://www.goes-r.gov/users/proving-ground.html}}
*{{NASA|article=GOES-R Spacecraft Overview|url=https://web.archive.org/web/20120510150321/http://www.goes-r.gov/spacesegment/spacecraft.html}}
*{{NASA|article=EUV and X-Ray Irradiance Sensors (EXIS)|url=http://www.goes-r.gov/spacesegment/exis.html}}
*{{NASA|article=Space Environment In-Situ Suite (SEISS)|url=https://web.archive.org/web/20120328175047/http://www.goes-r.gov/spacesegment/seiss.html}}
*{{NASA|article=Magnetometer (MAG)|url=https://web.archive.org/web/20120328175035/http://www.goes-r.gov/spacesegment/mag.html}}
*{{NASA|article=GOES-R Unique Payload Services (UPS)|url=https://web.archive.org/web/20110811235744/http://www.goes-r.gov/spacesegment/ups.html}}
*{{NASA|article=Program Systems Engineering Overview|url=https://web.archive.org/web/20120229085809/http://www.goes-r.gov/syseng/functions.html}}
*{{NASA|article=User Readiness Overview|url=https://web.archive.org/web/20120528172950/http://www.goes-r.gov/users/user-readiness-overview.html}}

==External links==
{{Div col||30em}}
* [http://www.goes-r.gov/ Official Website]
* [http://economics.noaa.gov/?goal=climate&file=obs/satellite/goes/ Social & Economic Benefits of GOES]
* [http://cimss.ssec.wisc.edu/goes_r/proving-ground.html GOES-R Satellite Proving Ground]
* [http://searchandrescue.gsfc.nasa.gov/ NASA Search and Rescue Satellite-aided Tracking]
* [http://www.sarsat.noaa.gov/ NOAA Search and Rescue Satellite-aided Tracking]
{{Div col end}}

{{GOES}}
{{Orbital launches in 2016}}
{{Space-based meteorological observation}}

[[Category:Weather satellites of the United States]]
[[Category:Artificial satellites in geosynchronous orbit]]
[[Category:National Oceanic and Atmospheric Administration]]
[[Category:Goddard Space Flight Center]]
[[Category:Satellites using the A2100 bus]]
[[Category:Spacecraft launched in 2016]]
[[Category:Spacecraft launched by Atlas rockets]]