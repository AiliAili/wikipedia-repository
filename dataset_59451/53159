{{Infobox journal
| title = Zeitschrift für celtische Philologie
| cover = [[File:Zeitschrift für celtische Philologie cover.gif]]
| editor = Stefan Zimmer, Jürgen Uhlich
| discipline = [[Celtic studies]]
| language = English, French, German, Italian, Spanish, 
| abbreviation =
| publisher = [[University of Bonn]]
| country = [[Germany]]
| frequency = Irregularly
| history = 1897-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://www.zcp.uni-bonn.de
| link1 = 
| link1-name = 
| link2 = 
| link2-name =
| JSTOR = 
| OCLC = 
| LCCN = 
| CODEN = 
| ISSN = 1865-889X
| eISSN =
}}
The '''''Zeitschrift für celtische Philologie''''' is an [[academic journal]] of [[Celtic studies]], which was established in 1897 by the German scholars [[Kuno Meyer]] and [[Ludwig Christian Stern]].<ref name=Busse>Busse, Peter E. "''Zeitschrift für celtische Philologie''." In ''Celtic Culture. A Historical Encyclopedia'', ed. J.T. Koch. 5 vols: vol. 5. Santa Barbara et al., 2006. p. 1823.</ref> It was the first journal devoted exclusively to [[Celtic languages]] and literature and the oldest significant journal of [[Celtic studies]] still in existence today.<ref name=Busse /> The emphasis is on (early) [[Irish language]] and [[Celtic literature|literature]] and [[Continental Celtic languages]], but other aspects of Celtic [[philology]] and literature (including modern literature) also receive attention.<ref name=Busse />

Apart from Stern and Meyer, previous editors include [[Julius Pokorny]], Ludwig Mühlhausen, [[Rudolf Thurneysen]], Rudolf Hertz, Heinrich Wagner, Hans Hartmann, and Karl Horst Schmidt.<ref name=Wirges>{{cite web|author=Christian Wirges |url=http://www.zcp.uni-bonn.de/editors.html |title=Zeitschrift für celtische Philologie: Editors |publisher=[[University of Bonn]] |date=2010-10-26 |accessdate=2012-02-09}}</ref> The current [[editors-in-chief]] are Jürgen Uhlich, Torsten Meißner and Bernhard Maier.

In addition to the regular volumes, the journal also has a subsidiary series, ''Buchreihe der Zeitschrift für celtische Philologie''.<ref name=Busse />

The journal features in a poem by [[Flann O'Brien]] which satirises scholars who "rose in their nightshift / To write for the Zeitschrift".<ref name=Wirges />

== References ==
{{reflist}}

==External links==
* {{Official website|http://www.zcp.uni-bonn.de}}
*[http://celtologica.eu/zcp/inhalt.html Tables of contents (1897-2008)]
*[http://www.reference-global.com/loi/zcph Recent issues of ZCP online]
*[https://archive.org/details/zeitschriftfrc01meyeuoft Volumes 1 to 14 available] from the [[Internet Archive]]

{{DEFAULTSORT:Zeitschrift Fur Celtische Philologie}}
[[Category:Celtic studies]]
[[Category:Philology journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1897]]
[[Category:University of Bonn]]
[[Category:Celtic languages]]