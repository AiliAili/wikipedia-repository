<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Ka-4 Rhönlerche II
 | image=Aussenlandung KA 4 25062010089.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Glider (sailplane)|Glider]]
 | national origin=[[West Germany]]
 | manufacturer=[[Alexander Schleicher GmbH & Co]]
 | designer=[[Rudolf Kaiser]]
 | first flight=7 December 1953
 | introduced=1955
 | retired=
 | status=Production completed
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=338
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Schleicher Ka-4 Rhönlerche II''' ({{lang-en|[[Rhön Mountains|Rhön]] [[Lark]]}}), sometimes called the '''KA-4''' or even '''K 4''', is a [[West Germany|West German]] [[high-wing]], [[strut-braced]], two-seat [[Glider (sailplane)|glider]] that was designed by [[Rudolf Kaiser]] and produced by [[Alexander Schleicher GmbH & Co]].<ref name="SD">{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=179|title = Ka-4 Rhonlerche Schleicher |accessdate = 22 July 2011|last = Activate Media|authorlink = |year = 2006}}</ref><ref name="SoaringNov83">{{cite journal|last=Said|first=Bob|title=1983 Sailplane Directory|journal=Soaring Magazine|date=November 1983|id=USPS 499-920|publisher=Soaring Society of America}}</ref><ref name="7G6">{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/7e8c683f8ff2b5ef8625748e0055f26d/$FILE/7g6.PDF|title = Type Certificate Data Sheet No. 7G6 |accessdate = 22 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=October 1960}}</ref>

==Design and development==
The Rhönlerche II was first flown 7 December 1953.<ref>http://www.alexander-schleicher.de/flugzeuge/ka-4-rhoenlerche-ii/</ref> Its design goals were to produce a simple, inexpensive and robust two-seat [[Trainer (aircraft)|trainer]] for school and club use. The design was a success and several hundred were constructed.<ref name="SD" /><ref name="SoaringNov83" />

[[File:Ka4 D-5627 fuselage.jpg|thumb|right|Rhönlerche fuselage showing underlying welded steel tube structure]]
The Rhönlerche II is constructed with a welded steel tube [[fuselage]] and a wooden wing and tail surfaces, all finished in doped [[aircraft fabric covering]]. The {{convert|13.0|m|ft|1|abbr=on}} span wing is supported by single [[lift strut]]s and employs a Goettingen 533 [[airfoil]]. The aircraft's [[landing gear]] is a fixed monowheel, with a nose skid.<ref name="SD" /><ref name="SoaringNov83" /><ref name="Incomplete">{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 22 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}</ref>

The aircraft was issued a US [[type certificate]] on 28 September 1960.<ref name="7G6" />

==Operational history==
The Ka-4 was widely used by clubs and schools in West Germany and also by Canadian military gliding clubs stationed in West Germany, including the Lahr Gliding Club at [[CFB Lahr]]. One of these aircraft was later shipped to [[Canada]] and operated by the Cold Lake Gliding Club at [[CFB Cold Lake]] and in July 2011 was owned by the Gravelbourg Gliding And Soaring Club, [[Gravelbourg, Saskatchewan]].<ref name="SoaringNov83" /><ref name="TCCAR">{{Cite web|url = http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp|title = Canadian Civil Aircraft Register|accessdate = 22 July 2011|last = [[Transport Canada]]|authorlink = |date=July 2011}}</ref>

In July 2011 there were still five Ka-4s on the [[United States]] [[Federal Aviation Administration]] aircraft registry.<ref name="FAAReg">{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=RHONLERCHE&PageNo=1|title = Make / Model Inquiry Results|accessdate = 22 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}</ref>
<!-- ==Variants== -->

==Aircraft on display==
[[File:Schleicher KA 4 Rhönlerche vl.jpg|thumb|right|Rhönlerche on display in the [[Technikmuseum Speyer]]]]
[[File:Schleicher Ka-4 Rhönlerche.jpg|thumb|right|Museum Rhönlerche display]]
*[[Aviodrome]] - 3<ref name="Aviodrome">{{Cite web|url = http://www.aviodrome.nl/uploads/files/projectenpdf/Collectiestuk%20M-Z.pdf|title = Collectiestuk 
|accessdate = 22 July 2011|last = [[Aviodrome|Nationaal Luchtvaart-Themapark Aviodrome]]|authorlink = |year = n.d.}}</ref>
*[[Technikmuseum Speyer]]
*[[US Southwest Soaring Museum]] - 2<ref name="SWSMCollection">{{cite web|url = http://swsoaringmuseum.org/collection.htm|title = Sailplanes, Hang Gliders & Motor Gliders|accessdate = 22 July 2011|last = US Southwest Soaring Museum |authorlink = |year = 2010}}</ref>
*[[Austrian Aviation Museum]]{{Citation needed|date=August 2016}}

==Specifications (Ka-4) ==
[[File:KA4 Windenstart.jpg|thumb|right|Ka-4 [[winch-launching]]]]
{{Aircraft specs
|ref=Sailplane Directory, Soaring and Type Certificate 7G6,<ref name="SD" /><ref name="SoaringNov83" /><ref name="7G6" /> The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs du Monde<ref name=Shenstone>{{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}</ref>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=one
|capacity=one passenger
|length m=7.3
|span m=13
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=16.34
|wing area sqft=
|wing area note=
|aspect ratio=10.3:1
|airfoil=root: Göttingen 533 15.7%, mid: Göttingen 533 15.7%, tip:  Göttingen 533 12.5%
|empty weight kg=107.5
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=400
|max takeoff weight lb=
|max takeoff weight note=
|more general=
<!--
        Performance
-->
|stall speed kmh=56
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=170
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=<br/>
*'''Rough air speed max:''' {{convert|120|km/h|mph kn|abbr=on|1}}
*'''Aerotow speed:''' {{convert|120|km/h|mph kn|abbr=on|1}}
*'''Winch launch speed:''' {{convert|90|km/h|mph kn|abbr=on|1}}
|g limits=+4.7 -2.3 at {{convert|170|km/h|mph|0|abbr=on}}
|roll rate=
|glide ratio=17.5:1 at {{convert|78|km/h|mph|0|abbr=on}}
|sink rate ms=1.1
|sink rate ftmin=
|sink rate note= at {{convert|62|km/h|mph|0|abbr=on}}
|lift to drag=
|wing loading kg/m2=24.5
|wing loading lb/sqft=
|wing loading note=
|more performance=
|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=
*[[Scheibe Specht]]
*[[Schweizer SGU 2-22]]
*[[SZD-10 Czapla]]
|lists=
* [[List of gliders]]
}}

==Notes==
{{reflist}}

==References==
{{refbegin}}
* {{cite book|last=Shenstone|first=B.S.|title=The World's Sailplanes:Die Segelflugzeuge der Welt:Les Planeurs dans Le Monde|year=1958|publisher=Organisation Scientifique et Technique Internationale du Vol a Voile (OSTIV) and Schweizer Aero-Revue|location=Zurich|pages=9–13|edition=1st |author2=K.G. Wilkinson |author3=Peter Brooks|language=English, French, German}}
*{{cite journal|last=Said|first=Bob|title=1983 Sailplane Directory|journal=Soaring Magazine|date=November 1983|id=USPS 499-920|publisher=Soaring Society of America}}
*{{Cite web|url = http://www.airweb.faa.gov/Regulatory_and_Guidance_Library/rgMakeModel.nsf/0/7e8c683f8ff2b5ef8625748e0055f26d/$FILE/7g6.PDF|title = Type Certificate Data Sheet No. 7G6 |accessdate = 22 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=October 1960}}
*{{Cite web|url = http://www.sailplanedirectory.com/PlaneDetails.cfm?PlaneID=179|title = Ka-4 Rhonlerche Schleicher |accessdate = 22 July 2011|last = Activate Media|authorlink = |year = 2006}}
*{{Cite web|url = http://www.ae.illinois.edu/m-selig/ads/aircraft.html|title = The Incomplete Guide to Airfoil Usage |accessdate = 22 July 2011|last = Lednicer |first = David |authorlink = |year = 2010}}
*{{Cite web|url = http://wwwapps2.tc.gc.ca/Saf-Sec-Sur/2/ccarcs/aspscripts/en/quicksearch.asp|title = Canadian Civil Aircraft Register|accessdate = 22 July 2011|last = [[Transport Canada]]|authorlink = |date=July 2011}}
*{{Cite web|url = http://registry.faa.gov/aircraftinquiry/AcftRef_Results.aspx?Mfrtxt=&Modeltxt=RHONLERCHE&PageNo=1|title = Make / Model Inquiry Results|accessdate = 22 July 2011|last = [[Federal Aviation Administration]]|authorlink = |date=July 2011}}
{{refend}}

==External links==
{{Commons category-inline|Schleicher Ka-4}}
{{Schleicher}}

{{DEFAULTSORT:Schleicher Ka-4 Rhonlerche II}}
[[Category:German sailplanes 1950–1959]]
[[Category:Schleicher aircraft]]