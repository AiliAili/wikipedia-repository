{{Use dmy dates|date=September 2013}}
{{Infobox military person
| honorific_prefix  = 
| name              = Juan Bielovucic
| honorific_suffix  = 
| native_name       = 
| native_name_lang  = 
| image         = File:Juan Bielovucic Cavalié dc978e9c34 -crop.jpg
| image_size    = 300px
| alt           = 
| caption       = Juan Bielovucic, {{circa}} 1913
| birth_date    = {{Birth date|1889|07|30|df=yes}}
| death_date    = {{Death date and age|1949|01|14|1889|07|30|df=yes}}
| birth_place   = [[Lima]], [[Peru]]
| death_place   = [[Paris]], [[France]]
| nickname      = Bielo
| birth_name    = 
| allegiance    =  {{plainlist |
* {{flag|Peru}}
* {{flag|France}}
* {{flag|Croatia}}
}}
| branch        =  {{plainlist |
* [[Peruvian Air Force|Peruvian Aviation Corps]]
* ''[[French Air Force|Service Aéronautique]]''<br>of the [[French Army]] 
  }}
| serviceyears  = 
| rank          = [[Colonel]] <small>(Peruvian Aviation Corps)</small>
| servicenumber = <!--Do not use data from primary sources such as service records.-->
| unit          = ''[[Escadrille les Cigognes]]''<br><small>(''Service Aéronautique'')</small>
| commands      = lieutenant commander of the Peruvian Aviation Corps Reserve
| battles       = [[World War I]]
| battles_label = 
| awards        = {{plainlist |
* [[Legion of Honour]]
* [[Croix de guerre 1914–1918 (France)|''Croix de guerre'' 1914–1918]] with a bronze palm
  }}
| laterwork     = Held several [[Aircraft records|aviation records]]<br>Peruvian [[air attaché]] in France
}}
'''Juan Bielovucic''' (30 July 1889 – 14 January 1949) was a Peruvian aviator of Croatian and French descent who set several speed and altitude [[Aircraft records|aviation records]] in 1910–13. He was also the first person to complete a successful powered aircraft crossing of the [[Alps]] in 1913, following a 1910 attempt by [[Jorge Chávez]] that ended in a fatal [[crash landing]]. He established the first aviation school in [[South America]] in [[Lima]], Peru. Bielovucic became a [[colonel]] of the [[Peruvian Air Force|Peruvian Aviation Corps]] (PAC) in 1911, joined the ''[[French Air Force|Service Aéronautique]]'' of the [[French Army]] as a volunteer in 1914 and earned the [[Legion of Honour]] for his service in [[World War I]]. He retired from active aviation in 1920 and returned to Peru where he became the lieutenant commander of the PAC Reserve. He was also active with the [[French Resistance]] during [[World War II]]. In [[Croatia]], he is regarded as the first Croatian aviator.

==Early life==
Juan Bielovucic was born on 30 July 1889 in [[Lima]], [[Peru]] to [[Croat]] father Miho Bjelovučić (Juan Miguel Bielovucic) and French mother Adriana Cavalié.{{sfn|Zlatar Stambuk|1990|p=41}} The couple had moved to Peru after Bielovucic's father, originally from [[Mokošica]] in [[Rijeka Dubrovačka]] area, retired from his position as a [[merchant navy]] captain. When Bielovucic was eight, his father fell ill and moved to [[Dubrovnik]] where he died shortly afterwards. After his death, Bielovucic and his mother moved to live with her relatives in France. Bielovucic attended school and university in [[Paris]] where he graduated with degrees in philosophy and literature.{{sfn|Hrvatski vojnik|2008}}{{sfn|de la Jara|1975|p=483}} He was nicknamed "Bielo".{{sfn|Serrano Villard|2002|p=101}}

==Aviation career==
In 1908, Bielovucic enrolled in the [[Voisin brothers]]' flying school and was awarded flying licence number 87 by the [[Aéro-Club de France]] on 10 June 1910. He took part in the first [[Budapest]] [[airshow]] on 1–15 June 1910, registering under his Croatian name as Ivan Bjelovučić. On 1–3 September,{{sfn|Hrvatski vojnik|2008}} he flew a new aircraft from Paris to [[Bordeaux]] with four stopovers: he reached [[Orléans]], {{convert|75|mi|abbr=off}} away, on 1 September, continued the next morning to {{convert|90|mi|abbr=off}} distant [[Poitiers]]; and completed the {{convert|64|mi|adj=on}} third leg of the journey to [[Angoulême]]. The final part of the trip was completed at noon on 3 September, when he landed in Bordeaux. The {{convert|540|km|adj=on}} distance was covered in six hours and fifteen minutes of flying, representing a world record.{{sfn|Serrano Villard|Allen|2000|p=66}} The new type became known as the [[Voisin Type Bordeaux]]. In 1910 he entered ''Circuito Internationale Aereo Di Milano'' flying a Voisin aircraft. He took part in [[Le Bourget Air Show]] and completed the {{convert|295|km|adj=on}} Paris–[[Nancy, France|Nancy]] flight in 1911, setting a new record of two hours and fifty minutes for the route.{{sfn|Hrvatski vojnik|2008}}

On 15 January 1911, Bielovucic returned to Peru, bringing with him an airplane and the technical staff requested by the Peruvian Aviation League. In Santa Beatriz, he took off piloting the first aircraft flight in Peru, flying over Lima, [[Callao]] and above the Pacific Ocean coast. The flight was witnessed by the [[President of Peru]] [[Augusto B. Leguía]] and other dignitaries. On 29 January he completed a 20-minute flight from Lima to [[Ancón District|Ancón]], a day before he became a flight instructor in the first flying school in South America. In early 1911 he became [[Peruvian Air Force|Peruvian Aviation Corps]] (PAC) colonel and returned to France as an air force envoy.{{sfn|Hrvatski vojnik|2008}}

On 12 May 1911 his aircraft caught fire as he took off from [[Issy]] airfield, but Bielovucic survived by jumping from the aircraft at a low altitude. On 28 May, he took part in the [[Paris–Rome–Turin air race]], and in August joined the 2nd Military Competition in France flying a [[Hanriot (aircraft company)|Hanriot]] monoplane. In late 1912 he set a [[climb rate]] record in a [[Blériot Aéronautique|Blériot]] monoplane, climbing to {{convert|2200|m|abbr=off}} in 12 minutes. That year, [[Le Matin (France)|Le Matin]] readers voted Bielovucic the best French aviator.{{sfn|Hrvatski vojnik|2008}}

In 1913 Bielovucic was the first to successfully cross the [[Alps]] by airplane, flying a Hanriot [[monoplane]] powered by a {{convert|80|hp|adj=on}} engine,{{sfn|Hrvatski vojnik|2008}} completing the crossing first attempted by [[Jorge Chávez]] on 23 September 1910. Chávez had almost completed his flight, but lost control of his final descent, crashed from an altitude of {{convert|10|m|abbr=off}}, and sustained fatal injuries. Bielovucic chose to cross the Alps in winter because he noted that the Alpine winds were less frequent then.{{sfn|Serrano Villard|2002|p=101}} His first attempt of 14 January failed, and he returned to the starting airfield. He made a second attempt to cross on 25 January,{{sfn|Hrvatski vojnik|2008}} taking off from [[Brig-Glis]], Switzerland at noon, and successfully landed in [[Domodossola]] in Italy at 12:25.{{sfn|Popular Mechanics|1913|p=516}} During the flight, he reached an altitude of {{convert|3200|m|abbr=off}}, setting another record. Later that year, he set another climb rate record, climbing {{convert|1000|m|abbr=off}} in 150 seconds in a {{convert|60|hp|adj=on}} [[Le Rhône]] engine-powered Ponnier monoplane.{{sfn|Hrvatski vojnik|2008}}

In 1914, as [[World War I]] began, he joined the ''[[French Air Force|Service Aéronautique]]'' of the [[French Army]] as a volunteer, being assigned to the ''[[Escadrille les Cigognes]]'' as a French and Peruvian officer. He flew numerous reconnaissance flights over Belgium until he was wounded.{{sfn|Hrvatski vojnik|2008}} Bielovucic was awarded the [[Legion of Honour]],{{sfn|Serrano Villard|2002|p=102}} as well as the [[Croix de guerre 1914–1918 (France)|''Croix de guerre'']] with a bronze palm and a number of Belgian and Peruvian medals. Later on he worked as the head of an engine testing team in Bellanger and then as the principal of an aviation school in [[Reims]]. Bielovucic retired from active piloting in 1920,{{sfn|Hrvatski vojnik|2008}} and returned to Peru where he became lieutenant commander of the PAC Reserve, regarded as a national hero.{{sfn|Zlatar Stambuk|1990|p=300}}

==Later life and legacy==
During [[World War II]], while posted in Paris as Peruvian [[air attaché]],{{sfn|Zlatar Stambuk|1990|p=301}} Bielovucic actively cooperated with the [[French Resistance]]. At the age of 57, he parachuted from the [[Eiffel Tower]]. Bielovucic died in Paris on 14 January 1949{{sfn|Hrvatski vojnik|2008}} and was buried at the ''Cimetiere nouveau de Neuilly'' in the [[Nanterre]] [[Communes of France|commune]] of Paris.{{sfn|Catillon|1997|p=212}} His biography was written by Lieutenant General [[José Zlatar Stambuk]] of the Peruvian Air Force and published in 1990. In Croatia, he is regarded as the first Croatian aviator because of his paternal ethnic origin. He is known as Ivan Bjelovučić there,{{sfn|Hrvatski vojnik|2008}} while most sources refer to him as Jean Bielovucic{{sfn|Pascoe|2003|pp=11–12}} or Juan Bielovucic Cavalié.{{sfn|Zlatar Stambuk|1990|p=300}}

In late September 1910 Bielovucic took [[Filippo Tommaso Marinetti]], founder of the [[Futurism|Futurist movement]], over [[Milan]] as a passenger in a Voisin biplane during the International Air Week. Marinetti stated that the experience moved him to his new concept of art.{{sfn|Pascoe|2003|pp=11–12}}

==Footnotes==
{{reflist|20em}}

==References==
;Books
{{refbegin|60em}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=dpCCjvJ_fyQC|title=Mémorial aéronautique: qui était qui?|first=Marcel|last=Catillon|language=French|trans_title=Aviation Memorial:Who was who?|publisher=Nouvelles Editions Latines|location=Paris, France|year=1997|isbn=9782723305297}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=5N1iAAAAMAAJ|title=Historia aeronáutica del Perú, Volumen 1|trans_title=Aeronautical History of Peru, Volume 1|publisher=Comisión Encargada del Estudio, Revisión y Edición de la Historia Aeronáutica del Perú|location=Lima, Peru|first=Carlos A.|last=de la Jara|oclc=4549274|language=Spanish|year=1975}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=zuFTCnfhxH0C|title=Aircraft|first=David|last=Pascoe|publisher=[[Reaktion Books]]|location=London, England|isbn=9781861891631|year=2003}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=tDmR7DhM_uEC|title=Contact!: The Story of the Early Aviators|first=Henry|last=Serrano Villard|year=2002|publisher=[[Courier Dover Publications]]|location=Mineola, New York|isbn=9780486423272}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=WiKzlpOyHtkC|title=Looping the Loop: Posters of Flight|first1=Henry|last1=Serrano Villard|first2=Willis M.|last2=Allen|year=2000|publisher=Kales Press|location=San Diego, California|isbn=9780967007625}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=hRdDAAAAIAAJ|title=Aërial Navigation: A Popular Treatise on the Growth of Air Craft and on Aëronautical Meteorology|first=Albert Francis|last=Zahm|year=1911|publisher=[[D. Appleton & Company]]|location=New York City|oclc=2639018}}
*{{cite book|ref=harv|url=https://books.google.com/books?id=M8tiAAAAMAAJ|title=Bielovucic, pionero de la aeronáutica castrense|first=José|last=Zlatar Stambuk|year=1990|publisher=Editorial Cientifica S.R.L.|location=Lima, Peru|oclc=25413456|language=Spanish|trans_title=Bielovucic, Military Aviation Pioneer}}
{{refend}}

;Other sources
{{refbegin|60em}}
*{{cite news|ref={{harvid|The New York Times|4 September 1910}}|newspaper=[[The New York Times]]|url=https://query.nytimes.com/gst/abstract.html?res=9C03E6D61E39E333A25757C0A96F9C946196D6CF|title=Completes 366-Mile Flight; Bielovucci Arrives at Bordeaux by Aeroplane from Paris|date=4 September 1910}}
*{{cite journal|ref={{harvid|Hrvatski vojnik|2008}}|journal=[[Hrvatski vojnik]]|publisher=[[Ministry of Defence (Croatia)]]|url=http://www.hrvatski-vojnik.hr/hrvatski-vojnik/2102008/podlistak.asp|language=Croatian|title=Ivan Bjelovučić, prvi hrvatski zrakoplovac|trans_title=Ivan Bjelovučić, the First Croatian Aviator|issue=210| date=October 2008 |first=Leonard|last=Eleršek|issn=1333-9036}}
*{{cite journal|ref={{harvid|Popular Mechanics|1913}}|journal=[[Popular Mechanics]]|publisher=[[Hearst Magazines]]|issn=0032-4558|url=https://books.google.com/books?id=890DAAAAMBAJ| date=April 1913 |title=Peruvian Airman Flies over the Alps}}
{{refend}}

==External links==
*[http://earlyaviators.com/ebielovu.htm Juan Bielovucic entry at Early Aviators]
*[http://aviation.maisons-champagne.com/dir.php?centre=04-bio-bielovucic&menu=11 Juan Bielovucic entry at Champagne : Berceau de l'aviation du Monde]

{{Authority control}}
{{good article}}

{{DEFAULTSORT:Bielovucic Cavalie, Juan}}
[[Category:1889 births]]
[[Category:1949 deaths]]
[[Category:Aviation pioneers]]
[[Category:Chevaliers of the Légion d'honneur]]
[[Category:Peruvian Air Force personnel]]
[[Category:Peruvian aviators]]
[[Category:Peruvian people of Croatian descent]]
[[Category:Peruvian people of French descent]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]