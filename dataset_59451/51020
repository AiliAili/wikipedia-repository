{{primary sources|date=July 2011}}
{{Rellink|Not to be confused with [[Groupement de recherche et d'études pour la civilisation européenne|GRECE]]'s publishing house, called (Éditions) Copernic.}}
{{Infobox publisher
| status = Active
| founded = {{Start date|1994}}
| country = Germany
| headquarters = Göttingen
| distribution = Worldwide
| publications = [[Scientific journal]]s
| topics = [[Earth science]]
| url = {{URL|publications.copernicus.org}}
}}
'''Copernicus Publications''' (also: Copernicus GmbH) is a [[publishing|publisher]] of scientific literature based in Göttingen, Germany.<ref name="pmid20976226">{{cite journal| author=Bornmann L, Daniel HD| title=Do author-suggested reviewers rate submissions more favorably than editor-suggested reviewers? A study on Atmospheric Chemistry and Physics | journal=[[PLoS ONE]] | year= 2010 | volume= 5 | issue= 10 | pages= e13345 | pmid=20976226 | doi=10.1371/journal.pone.0013345 | pmc=2954795 | url= }}</ref> Founded in 1994, Copernicus Publications currently publishes 28 [[peer review|peer-reviewed]] [[open access]] [[scientific journal]]s and other publications on behalf of the [[European Geosciences Union]].

Copernicus Publications is part of the open-access publishing movement. Initially, the [[CC-BY-NC]] was used. In 2007, they switched to the [[CC-BY]] attribution license.<ref name="copernicus 2007">{{cite web | url=http://publications.copernicus.org/for_authors/license_and_copyright.html | title=License and Copyright Agreement | publisher=Copernicus Publications | accessdate=March 22, 2012}}</ref> Copernicus Publications has been described as the largest open access publisher in the Geo- and Earth system sciences,<ref>[http://www.mpg.de/568610/pressRelease20080128 Press release] of the [[Max Planck Society]] ([http://www.webcitation.org/60TMK1Y3u?url=http://www.mpg.de/568610/pressRelease20080128 WebCite])</ref> and it is known as one of the first publishers to embrace [[public peer review]].<ref>[http://go.nature.com/qamrfc The editors of Atmospheric Chemistry and Physics explain their journal's approach] ([http://www.webcitation.org/60TLn19rA?url=http://www.nature.com/nature/peerreview/debate/nature04988.html WebCite])</ref> 

In 2014, one of their journals was terminated under allegations of [[nepotism|nepotistic]] reviewing and malpractice; see ''[[Pattern Recognition in Physics#History]]'' for details.

== See also ==
* [[:Category:Copernicus Publications academic journals]]
* [[Open Access Scholarly Publishers Association]], of which Copernicus Publications is a founding member<ref>[http://oaspa.org/about/founding-members/]</ref>

==References==
{{Reflist}}

==External links==
{{Commonscat|Media from Copernicus}}
* {{Official website|http://publications.copernicus.org}}

[[Category:Academic publishing companies]]
[[Category:Publishing companies established in 1994]]
[[Category:Open access publishers]]
[[Category:1994 establishments in Germany]]

{{Publish-company-stub}}