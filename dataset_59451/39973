{{For|the American college president (1863–1917)|Frederick Henry Sykes}}
{{Use dmy dates|date=April 2012}}
{{Infobox military person
|name= Sir Frederick Hugh Sykes
|image= Maj Gen Frederick Sykes.jpg
|caption= Major General Frederick Sykes c. 1918
|nickname= 
|birth_date= {{birth date|1877|07|23|df=y}}
|birth_place= [[Addiscombe]], [[Surrey]], England
|death_date= {{death date and age|1954|09|30|1877|07|23|df=y}}
|death_place= [[London]], England
|allegiance= United Kingdom
|branch= [[British Army]] (1899–18)<br/>[[Royal Air Force]] (1918–19)
|serviceyears= 1899–1919
|rank= [[Air Vice Marshal]]
|unit= 
|commands= [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] (1918–19)<br/>[[RNAS Eastern Mediterranean]] (1915–16)<br/>Military Wing of the [[Royal Flying Corps]] (1912–14)
|battles= [[Second Boer War]]<br/>[[First World War]]
|awards= [[Knight Grand Commander of the Order of the Star of India]]<br/>[[Knight Grand Commander of the Order of the Indian Empire]]<br/>[[Knight Grand Cross of the Order of the British Empire]]<br/>[[Knight Commander of the Order of the Bath]]<br/>[[Companion of the Order of St Michael and St George]]<br/>[[Mentioned in Despatches]] (2)<br/>[[Order of Saint Vladimir|Order of Saint Vladimir, 4th Class]] (Russia)<br/>[[Order of Leopold (Belgium)|Commander of the Order of Leopold]] (Belgium)<br/>[[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] (United States)<br/>[[Commander of the Legion of Honour]] (France)<br/>[[Order of the Rising Sun|Order of the Rising Sun, 2nd Class]] (Japan)
|relations= 
|laterwork= Statesman, politician
}}
[[Air Vice Marshal]] '''Sir Frederick Hugh Sykes''', {{postnominals|country=GBR|size=100%|sep=,|GCSI|GCIE|GBE|KCB|CMG}} (23 July 1877 – 30 September 1954) was a British military officer, statesman and politician.

Sykes was a junior officer in the [[15th The King's Hussars|15th Hussars]] before becoming interested in military aviation. He was the first Officer Commanding the Military Wing of the [[Royal Flying Corps]] before the First World War and later served as the Flying Corps' Chief of Staff in France during the 1914 and 1915. Later in the war, he served in the [[Royal Naval Air Service]] in the Eastern Mediterranean before returning to Great Britain where he worked to organise the [[Machine Gun Corps]] and manpower planning.  In late 1917 and early 1918, Sykes was the deputy to [[Sir Henry Wilson, 1st Baronet|General Wilson]] on the [[Supreme War Council]] and from April 1918 to early 1919 he served as the second [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]].

After the War, Sykes was appointed the Controller of Civil Aviation and he continued in this role until 1922 when he entered politics, becoming the Conservative MP for [[Sheffield Hallam (UK Parliament constituency)|Sheffield Hallam]], which he held until 1928 when he resigned.  From 1928 to 1931 Sykes was [[Governor of Bombay]], after which time he returned to Great Britain where he involved himself in business and public life. During the [[Second World War]], Sykes was an MP once more, this time for [[Nottingham Central (UK Parliament constituency)|Central Nottingham]]. He lost his seat in 1945 and he died nine years later.

==Military career==
Sykes was the son of Henry Sykes and Margaret Sykes (née Sykes), and nephew of the artist [[Godfrey Sykes]].<ref name=odnb>{{cite web|url=http://www.oxforddnb.com/view/article/36393 |title=Sir Frederick Sykes |publisher= Oxford Dictionary of National Biography|accessdate=5 August 2012}}</ref> Following civilian employment as a clerk and after working on a tea plantation in [[Ceylon]],<ref name=odnb/> Sykes enlisted as a trooper in the [[Imperial Yeomanry]] Scouts regiment of the [[British Army]] at the start of the [[Second Boer War]].<ref name=air>{{cite web|url=http://www.rafweb.org/Biographies/Sykes.htm |publisher=Air of Authority – A History of RAF Organisation |title= Air Vice Marshal Sir Frederick Sykes|accessdate=5 August 2012}}</ref> Following capture, Sykes was forcibly marched across South Africa but was later abandoned and returned to the British forces.<ref name=odnb/> In 1900 he was commissioned into [[Frederick Roberts, 1st Earl Roberts|Lord Roberts']] Bodyguard but suffered a serious wound to the chest which resulted in his being invalided back to Great Britain.<ref name=odnb/>  On 2 October 1901 he was granted a regular commission as a [[second lieutenant]] in the [[15th The King's Hussars|15th Hussars]].<ref>{{London Gazette|issue=27360|startpage=6397|date=1 October 1901|supp=|accessdate=5 August 2012}}</ref> He was posted to the West African Regiment and granted the local rank of [[Lieutenant (British Army and Royal Marines)|lieutenant]] on 7 March 1903.<ref>{{London Gazette|issue=27537|startpage=1985|date=24 March 1903|supp=|accessdate=5 August 2012}}</ref> He was promoted to the substantive rank of lieutenant on 29 July 1903.<ref>{{London Gazette|issue=27595|startpage=5598|date=8 September 1903|supp=|accessdate=5 August 2012}}</ref>

In 1904, Sykes's interest in aviation was first demonstrated when he obtained a ballooning certificate whilst being attached to the Balloon Section of the [[Royal Engineers]].<ref name=odnb/> He was restored to the establishment of the 15th Hussars on 22 September 1904.<ref>{{London Gazette|issue=27745|startpage=8720|date=20 December 1904|supp=|accessdate=5 August 2012}}</ref> He joined the Intelligence Staff at [[Shimla|Simla]] in [[India]] in 1905 before attending [[Command and Staff College|Staff College, Quetta]] in Autumn 1908.<ref name=air/> He was promoted to [[Captain (British Army and Royal Marines)|captain]] on 1 October 1908.<ref>{{London Gazette|issue=28193|startpage=8028|date=6 November 1908|supp=|accessdate=5 August 2012}}</ref> In 1910 Sykes commenced flying lessons at [[Brooklands]] which led to an award from the [[Royal Aero Club]] [[List of pilots awarded an Aviator's Certificate by the Royal Aero Club in 1911|certificate No. 96]] in June 1911.<ref>{{cite web|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200567.html|title=Aviators' Certificates|year=1911|publisher=Flight International|accessdate=5 August 2012}}</ref> On 25 February 1911 Sykes was posted as a staff officer to the Directorate of Military Operations at the [[War Office]].<ref>{{London Gazette|issue=28471|startpage=1636|date=3 March 1911|supp=|accessdate=5 August 2012}}</ref> As a firm believer in the importance of wartime aerial reconnaissance, he was chosen to join the sub-committee of the Committee of Imperial Defence which was given the task of investigating the use of aircraft.<ref name=prob5>Probert, p.5</ref>

[[File:First flying and administrative officers of the Royal Flying Corps RAE-O790.jpg|thumb|200px|left|Sykes in 1913 with some of his officers]]
On 13 May 1912 Sykes was appointed Officer Commanding the Military Wing of the Royal Flying Corps with the temporary rank of [[Major (United Kingdom)|major]].<ref>{{London Gazette|issue=28609|startpage=3583|date=17 May 1912|supp=|accessdate=5 August 2012}}</ref> His duties included the recruitment and training of pilots.<ref name=prob5/> While in command, Sykes solicited suggestions for a new motto for the Corps: Sykes approved J S Yule's suggestion, ''[[Per ardua ad astra|Per Ardua ad Astra]]'', and it was this phrase which was subsequently adopted by the [[Royal Air Force]] as its motto.<ref>{{cite web|url=http://www.newenglishreview.org/print.cfm?pg=custpage&frm=50757&sec_id=104546|title=Per Ardua ad Astra|date=January 2012|publisher=New English Review|accessdate=5 August 2012}}</ref> On 9 July 1913 his role was restyled as Commandant of the Military Wing of the Royal Flying Corps and he was granted the temporary rank of [[Lieutenant colonel (United Kingdom)|lieutenant-colonel]].<ref>{{London Gazette|issue=28735|startpage=4870|date=8 July 1913|supp=|accessdate=5 August 2012}}</ref>

With the outbreak of the [[First World War]], Royal Flying Corps squadrons were deployed to France in August 1914.<ref name=prob5/> Although the configuration and effectiveness of the deployed forces owed much to Sykes, as a middle-ranking officer he lacked the seniority thought necessary for command in the field.<ref name=prob5/> [[David Henderson (British Army officer)|General Henderson]] became the [[General Officer Commanding]] the Royal Flying Corps in the Field and Sykes acted as his [[Chief of staff (military)|Chief of Staff]] from 5 August 1914.<ref>{{London Gazette|issue=28879|startpage=6685|date=25 August 1914|supp=|accessdate=5 August 2012}}</ref> On 22 November 1914, Henderson was appointed General Officer Commanding the [[1st Infantry Division (United Kingdom)|1st Infantry Division]]<ref name=odnb/> and Sykes took up command of the Royal Flying Corps in the Field.<ref>{{London Gazette|issue=30359|startpage=11252|date=30 October 1917|supp=yes|accessdate=5 August 2012}}</ref> However, Sykes did not spend long in command.  The decision to post Henderson and replace him with Sykes was not to [[Herbert Kitchener, 1st Earl Kitchener|Lord Kitchener's]] liking and he ordered a reversal of the posting.  On 21 December 1914, Henderson resumed command of the Royal Flying Corps in the Field and Sykes was granted the temporary rank of [[Colonel (United Kingdom)|colonel]] and once again made his Chief of Staff.<ref>{{London Gazette|issue=29054|startpage=990|date=29 January 1915|supp=yes|accessdate=5 August 2012}}</ref> He was promoted to the substantive rank of brevet lieutenant colonel on 18 February 1915.<ref>{{London Gazette|issue=29074|startpage=1687|date=16 February 1915|supp=yes|accessdate=5 August 2012}}</ref> With the rapid expansion of the Corps, there was a growing debate between those who believed that the Corps should remain under central control and those who hoped that its units could be placed under the control of the corps or divisional commanders.<ref name=prob5/>  Unsurprisingly as Chief of Staff, Sykes took the former view and following increasing arguments, Sykes was posted on 26 May 1915 being placed at the disposal of the [[Admiralty]].<ref name=air/>

Sykes visited the [[Dardanelles]] to investigate the confused air situation and after writing a report he was appointed as the Officer Commanding the [[Royal Naval Air Service]] Eastern Mediterranean Station on 24 July 1915<ref name=air/> with the rank of colonel commandant in the [[Royal Marines]]<ref>{{London Gazette|issue=29304|startpage=9323|date=21 September 1915|supp=|accessdate=5 August 2012}}</ref> as well as the rank of Wing Captain in the [[Royal Naval Air Service]].<ref>{{London Gazette|issue=29304|startpage=9324|date=21 September 1915|supp=|accessdate=5 August 2012}}</ref> This made Sykes the air commander for the [[Dardanelles Campaign]].<ref>{{cite web|url=http://rafmuseum-1.titaninternet.co.uk/milestones-of-flight/british_military/1915_2.cfm |title=Milestones of Flight: British Military Aviation in 1915|publisher=RAF Museum|accessdate=5 August 2012}}</ref>  During this time he acted on the recommendations of his report, building up air forces that sunk several Turkish ships.  He was honoured as [[Companion of the Order of St Michael and St George]] on 14 March 1916<ref>{{London Gazette|issue=29507|startpage=2869|date=14 March 1916|supp=yes|accessdate=5 August 2012}}</ref> and [[mentioned in despatches]] on 16 March 1916.<ref>{{London Gazette|issue=29507|startpage=2867|date=14 March 1916|supp=yes|accessdate=5 August 2012}}</ref>

Sykes was made Assistant Adjutant and Quartermaster-General of the 4th Mounted Division in March 1916 <ref name=air/> and, having been awarded the Russian [[Order of St Vladimir|Order of St Vladimir, 4th Class]] on 12 April 1916,<ref>{{London Gazette|issue=29547|startpage=3915|date=14 April 1916|supp=|accessdate=5 August 2012}}</ref> he was appointed Assistant Adjutant-General at the [[War Office]] with responsibility for organising the [[Machine Gun Corps]] and manpower planning on 9 June 1916.<ref>{{London Gazette|issue=29699|startpage=7860|date=8 August 1916|supp=yes|accessdate=5 August 2012}}</ref> He was made Deputy Director of Organisation at the War Office and granted the temporary rank of [[brigadier general]] on 8 February 1917.<ref>{{London Gazette|issue=30087|startpage=5037|date=22 May 1917|supp=yes|accessdate=5 August 2012}}</ref> On 27 November 1917 he became Deputy Adjutant and Quartermaster-General at the War Office<ref>{{London Gazette|issue=30528|startpage=2130|date=15 February 1918|supp=yes|accessdate=5 August 2012}}</ref> in which role he served on the British section of the [[Supreme War Council|Allied War Council]] in the [[Palace of Versailles]] under General [[Sir Henry Wilson, 1st Baronet|Sir Henry Wilson]].<ref name=air/>

Sykes's military career culminated in his appointment as [[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]] on 13 April 1918<ref name=air/> and, in that role, he did much to establish the new service.<ref name=odnb/> However, in January 1919, [[Winston Churchill]] was appointed [[Secretary of State for War]] and [[Secretary of State for Air]]. While Churchill was preoccupied with implementing post-War defence cuts and the demobilization of the Army, Sykes submitted a paper with what were at the time unrealistic proposals for a large air force of the future.<ref>Ash, pp. 175–177</ref> Being dissatisfied with Sykes performance, Churchill decided to reinstate [[Hugh Trenchard, 1st Viscount Trenchard|Sir Hugh Trenchard]], the previous Chief of the Air Staff.<ref>Boyle, pp. 325–328</ref> Accordingly, on 1 January 1919 Sykes was appointed a [[Knight Commander of the Order of the Bath]]<ref>{{London Gazette|issue=31098|startpage=91|date=31 December 1918|supp=yes|accessdate=5 August 2012}}</ref> and allowed to take early retirement with the rank of [[Major-general (United Kingdom)|major general]] with effect from 31 March 1919.<ref>{{London Gazette|issue=31348|startpage=6249|date=20 May 1919|supp=|accessdate=5 August 2012}}</ref>

Sykes was appointed a Commander of the Belgian [[Order of Leopold (Belgium)|Order of Leopold]] on 15 July 1919<ref>{{London Gazette|issue=31457|startpage=8987|date=11 July 1919|supp=yes|accessdate=5 August 2012}}</ref> and awarded the American [[Distinguished Service Medal (U.S. Army)|Distinguished Service Medal]] on the same date.<ref>{{London Gazette|issue=31457|startpage=8986|date=11 July 1919|supp=yes|accessdate=5 August 2012}}</ref> He was also granted the rank of [[air vice marshal]] when the RAF introduced its own rank structure on 1 August 1919, appointed a [[Knight Grand Cross of the Order of the British Empire]] on 26 August 1919<ref>{{London Gazette|issue=31522|startpage=10753|date=26 August 1919|supp=|accessdate=5 August 2012}}</ref> and appointed an officer of the French [[Legion of Honour]] on 18 November 1919.<ref>{{London Gazette|issue=31519|startpage=10724|date=22 August 1919|supp=|accessdate=5 August 2012}}</ref> 
[[File:British Air Section at the 1919 Paris Peace Conference.jpg|thumb|200px|right|The British Air Section at the Paris Peace Conference in 1919]]

From 1919 to 1922, Sykes was the Controller of Civil Aviation.<ref name=air/> He was awarded the Japanese [[Order of the Rising Sun|Order of the Rising Sun, 2nd Class]] on 4 January 1921<ref>{{London Gazette|issue=32180|startpage=62|date=4 January 1921|supp=|accessdate=5 August 2012}}</ref> and in 1922 he published ''Aviation in War and Peace'', a history of aviation in three chapters which covered pre-War flight, aviation during World War I and both military and civil aviation in peace time.<ref>{{cite web|url=http://www.amazon.co.uk/Aviation-Peace-War-ebook/dp/B0082X9VYI/ref=sr_1_1?ie=UTF8&qid=1344380349&sr=8-1|title=Aviation in War and Peace|publisher=Amazon Books|accessdate=5 August 2012}}</ref>

==Political career==
[[File:Frederick Sykes.jpg|thumb|200px|left|Sir Frederick Sykes circa 1940]]
Sykes entered political life at the [[United Kingdom general election, 1922|general election]] in November 1922 when he was elected the [[Conservative Party (UK)|Conservative]] [[Member of Parliament]] for [[Sheffield Hallam (UK Parliament constituency)|Sheffield Hallam]].<ref>{{London Gazette|issue=32775|startpage=8709|date=8 December 1922|supp=|accessdate=5 August 2012}}</ref> Sykes retained the seat at the [[United Kingdom general election, 1923|1923 election]]<ref>{{London Gazette|issue=32897|startpage=365|date=11 January 1924|supp=|accessdate=5 August 2012}}</ref> and the [[United Kingdom general election, 1924|1924 election]].<ref>{{London Gazette|issue=32996|startpage=8531|date=25 November 1924|supp=|accessdate=5 August 2012}}</ref> He [[Resignation from the British House of Commons|resigned]] the seat on 26 June 1928<ref>{{London Gazette|issue=33405|startpage=4897|date=20 June 1928|supp=|accessdate=5 August 2012}}</ref> to become [[Governor of Bombay]] on 17 October 1928.<ref>{{London Gazette|issue=33433|startpage=6856|date=26 October 1928|supp=|accessdate=5 August 2012}}</ref> He was appointed a [[Knight Grand Commander of the Order of the Indian Empire]] on 3 November 1928<ref>{{London Gazette|issue=33436|startpage=7205|date=6 November 1928|supp=|accessdate=5 August 2012}}</ref> and a member of the [[Privy Council of the United Kingdom|Privy Council]] on 20 November 1928<ref>{{London Gazette|issue=33440|startpage=7549|date=20 November 1928|supp=|accessdate=5 August 2012}}</ref> and served in Bombay until 8 November 1933.<ref>{{London Gazette|issue=33995|startpage=7375|date=14 November 1933|supp=|accessdate=5 August 2012}}</ref>

Sykes returned to Great Britain in 1933 and for the next six years he held various directorships and official committees posts. He was appointed a [[Order of the Star of India|Knight Grand Commander of the Order of the Star of India]] on 2 February 1934<ref>{{London Gazette|issue=34020|startpage=749|date=2 February 1934|supp=|accessdate=5 August 2012}}</ref> and a [[Venerable Order of Saint John|Knight of Justice of the Order of St John]] on 19 June 1936.<ref>{{London Gazette|issue=34297|startpage=4013|date=23 June 1936|supp=|accessdate=5 August 2012}}</ref> With the outbreak of War in 1939 Sykes offered his services to the British Government but he was not required and so he stood for Parliament once more. After the death in May 1940 of [[Terence O'Connor]], the [[Solicitor General for England and Wales|Solicitor General]] and MP for [[Nottingham Central (UK Parliament constituency)|Nottingham Central]], Sykes was returned unopposed in [[Nottingham Central by-election, 1940|the resulting by-election]].<ref>{{London Gazette|issue=34903|startpage=4529|date=23 July 1940|supp=|accessdate=5 August 2012}}</ref> He served as Nottingham Central MP until the [[United Kingdom general election, 1945|1945 general election]]<ref name=air/> and died at Beaumont Street in [[London]] on 30 September 1954.<ref name=odnb/> He was cremated at [[Golders Green Crematorium|Golders Green]].<ref>{{Cite newspaper The Times|date=5 October 1954|supp=|accessdate=10 October 2014}}</ref>

==Family==
In 1920 Sykes married Isabel Harrington Law, the elder daughter of [[Bonar Law|Andrew Bonar Law]], Conservative Prime Minister. Frederick and Isabel Sykes had one son, Bonar Sykes.<ref name=odnb/>

==References==
{{Reflist|30em}}

==Sources==
*{{cite book|last=Ash|first= Eric |year=1999|title=Sir Frederick Sykes and the air revolution, 1912–1918|publisher=Frank Cass| isbn= 0-7146-4828-0}}
*{{cite book |last=Boyle |first=Andrew |authorlink=Andrew Boyle |title=Trenchard Man of Vision |year=1962 |publisher=[[HarperCollins|Collins]] |location=St James's Place, London|asin=B0000CLC2N |ref=Boyle62}}
*{{cite book|last=Probert|first= Henry|title=High Commanders of the Royal Air Force|publisher=HMSO|year= 1991|isbn= 0-11-772635-4}}
*{{cite book|last=Sykes|first=Frederick|title=Many Angles: an autobiography|publisher=Harrap, London|year=1942|asin=B000ZFPGE0}}

== External links ==
{{commons category|Frederick Sykes}}
* {{Hansard-contribs | sir-frederick-sykes | Sir Frederick Sykes }}
* {{Gutenberg author | id=Sykes,+Frederick+Hugh,+Sir }}
* {{Internet Archive author |sname=Frederick Hugh Sykes |sopt=t}}

{{s-start}}
{{s-mil}}
|-
{{s-bef|before=[[Sir Alexander Bannerman, 11th Baronet|Sir Alexander Bannerman]]<br /><small>As Commandant of the [[Air Battalion Royal Engineers|Air Battalion]]</small>}}
{{s-ttl|title=Officer Commanding the Military Wing of the [[Royal Flying Corps]]|years=13 May 1912 – 5 August 1914}}
{{s-aft|after=[[Hugh Trenchard, 1st Viscount Trenchard|Hugh Trenchard]]}}
|-
{{s-new|reason=Outbreak of war}}
{{s-ttl|title=Chief of Staff, [[Royal Flying Corps]] in the Field|years=5 August – 22 November 1914}}
{{s-vac|next=Himself}} 
|-
{{s-bef|before=[[David Henderson (general)|Sir David Henderson]]}} 
{{s-ttl|title=Officer Commanding the [[Royal Flying Corps]] in the Field|years=22 November 1914 – 20 December 1914}}
{{s-aft|after=Sir David Henderson}}
|-
{{s-vac|last=Himself}}
{{s-ttl|title=Chief of Staff, [[Royal Flying Corps]] in the Field|years=20 December 1914 – 26 May 1915}}
{{s-aft|after=[[Robert Brooke-Popham]]}}
|-
{{s-bef|before=[[Charles Rumney Samson|Charles Samson]]<br><small>As Officer Commanding No. 3 Wing RNAS</small>}}
{{s-ttl|title=Officer Commanding [[RNAS Eastern Mediterranean]]|years=July 1915 – February 1916}}
{{s-aft|after=[[Francis Rowland Scarlett|Francis Scarlett]]}}
|-
{{s-bef|before=[[Hugh Trenchard, 1st Viscount Trenchard|Sir Hugh Trenchard]]}} 
{{s-ttl|title=[[Chief of the Air Staff (United Kingdom)|Chief of the Air Staff]]|years=1918–1919}}
{{s-aft|after=Sir Hugh Trenchard}}
|-
{{s-gov}}
{{s-new}}
{{s-ttl|title=Controller of Civil Aviation|years=1919–1922}}
{{s-aft|after=[[Sefton Brancker|Sir Sefton Brancker]]<br /><small>As Director</small>}}
|-
{{s-par|uk}}
{{s-bef|before = [[Douglas Vickers]]}} 
{{s-ttl|title = [[Member of Parliament]] for [[Sheffield Hallam (UK Parliament constituency)|Sheffield Hallam]]|years=[[United Kingdom general election, 1922|1922]]–[[Sheffield Hallam by-election, 1928|1928]]}}
{{s-aft|after = [[Louis Smith (British politician)|Sir Louis Smith]]}}
|-
{{s-bef|before = [[Terence O'Connor]]}} 
{{s-ttl|title = [[Member of Parliament]] for [[Nottingham Central (UK Parliament constituency)|Nottingham Central]]
|years=[[Nottingham Central by-election, 1940|1940]]–[[United Kingdom general election, 1945|1945]]}}
{{s-aft|after = [[Geoffrey de Freitas]]}}
|-
{{s-off}}
{{s-bef|before = [[Leslie Orme Wilson|Sir Leslie Orme Wilson]]}} 
{{s-ttl|title = [[Governor of Bombay]] | years = 1928–1931 }}
{{s-aft|after = [[Michael Knatchbull, 5th Baron Brabourne|The Lord Brabourne]]}}
{{s-end}}

{{Chief of the Air Staff}}

{{Authority control}}

{{DEFAULTSORT:Sykes, Frederick}}
[[Category:1877 births]]
[[Category:1954 deaths]]
[[Category:People from Addiscombe]]
[[Category:Members of the Parliament of the United Kingdom for English constituencies]]
[[Category:Companions of the Order of St Michael and St George]]
[[Category:Knights Commander of the Order of the Bath]]
[[Category:Knights Grand Commander of the Order of the Star of India]]
[[Category:Knights Grand Commander of the Order of the Indian Empire]]
[[Category:Knights Grand Cross of the Order of the British Empire]]
[[Category:Knights of Justice of the Order of St John]]
[[Category:Recipients of the Order of St. Vladimir, 4th class]]
[[Category:Recipients of the Order of the Rising Sun, 2nd class]]
[[Category:Commandeurs of the Légion d'honneur]]
[[Category:Recipients of the Croix de guerre 1914–1918 (France)]]
[[Category:Members of the Privy Council of the United Kingdom]]
[[Category:British Army personnel of the Second Boer War]]
[[Category:British Yeomanry soldiers]]
[[Category:Royal Flying Corps officers]]
[[Category:15th The King's Hussars officers]]
[[Category:South African military personnel]]
[[Category:British Army cavalry generals of World War I]]
[[Category:Royal Air Force generals of World War I]]
[[Category:Chiefs of the Air Staff (United Kingdom)]]
[[Category:Royal Air Force air marshals]]
[[Category:Military theorists]]
[[Category:Conservative Party (UK) MPs]]
[[Category:Politics of Sheffield]]
[[Category:Politics of Nottingham]]
[[Category:Governors of Bombay]]
[[Category:UK MPs 1922–23]]
[[Category:UK MPs 1923–24]]
[[Category:UK MPs 1924–29]]
[[Category:UK MPs 1935–45]]