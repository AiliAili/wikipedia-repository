{{Orphan|date=September 2015}}

{{Infobox company
|name             = AuCom Electronics Ltd
|logo             = AuCom_Electronics_Logo.png
|type             = Limited company
|founded          = 1978
|founder          = Ray Archer & Mark Empson
|location_city    = Christchurch
|location_country = New Zealand
|industry         = Industrial switchgear
|products         = Soft starters, switchgear
|homepage         = [http://www.aucom.com www.aucom.com]
}}

'''AuCom Electronics Ltd''' is a global company headquartered in Christchurch, [[New Zealand]].<ref>{{cite web|last=Young|first=David|title=Widening the silk road|url=http://www.listener.co.nz/uncategorized/widening-the-silk-road/|publisher=APN Holdings NZ|accessdate=14 May 2012}}</ref> It is one of the top 50 technology exporters in New Zealand.<ref>{{cite web|publisher=Technology Investment Network |title=TIN100 2009 Rankings |url=http://www.tinetwork.co.nz/TIN100+Report/TIN100+2009+Rankings.html |accessdate=14 May 2012 |archiveurl=https://web.archive.org/web/20120104155551/http://www.tinetwork.co.nz/TIN100+Report/TIN100+2009+Rankings.html |archivedate=4 January 2012 |deadurl=no |df= }}</ref>

==History==
AuCom was established in 1978 by engineer Mark Empson and entrepreneur Ray Archer. The company was originally set up to manufacture a range of top end stereo amplifiers, but was also involved in the design energy saving devices for single and three phase induction motors.

AuCom Electronics began manufacturing these energy saving devices under licence from American space agency NASA. From this technology AuCom developed a range of commercially viable soft starters.<ref>{{cite news|last=Hedquist|first=Ulrika|title=Variety on display among Hi-Tech finalists|url=http://computerworld.co.nz/news.nsf/news/variety-on-display-among-hi-tech-finalists|publisher=Fairfax Media Business Group|accessdate=14 May 2012}}</ref>

AuCom has since expanded its range to include a wider range of industrial electronics, motor control centres and [[switchgear]].

==Sales and distribution==
AuCom has sales offices in New Zealand, Germany, USA, China, and Dubai. It has over 40 distributors in more than 80 countries throughout the world.<ref>{{cite web|title=Soft Start Motor and Power Control {{!}} AuCom Electronics|url=http://www.aucom.com|website=www.aucom.com|accessdate=9 December 2015}}</ref>

==Products==
Some examples of AuCom's industrial motor control offerings are:
*Low Voltage Soft Starters: EMX4, EMX3, CSXi, CSX, IMS2
*Medium Voltage Soft Starters: MVS, MVX, MVE

==References==
{{Reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see http://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->

[[Category:Manufacturing companies of New Zealand]]
[[Category:Audio amplifier manufacturers]]
[[Category:Companies based in Christchurch]]
[[Category:Companies established in 1978]]
[[Category:New Zealand brands]]