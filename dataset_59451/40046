[[File:Dadaglobe Form Letter to Vagts (Nov 1920).jpg|thumb|Dadaglobe solicitation form letter signed by Francis Picabia, Tristan Tzara, Georges Ribemont-Dessaignes,and Walter Serner, c. week of November 8, 1920. This example was sent from Paris to Alfred Vagts in Munich.]]

'''''Dadaglobe''''' was an ambitious anthology of the [[Dada]] movement slated for publication in 1921, but abandoned for financial and other reasons and never published. At 160 pages with over a hundred reproductions of artworks and over a hundred texts by some fifty artists in ten countries, ''Dadaglobe'' was to have documented Dada's apotheosis as an artistic and literary movement of international breadth. Edited by Dada co-founder [[Tristan Tzara]] (1896-1963) in Paris, ''Dadaglobe'' was not conceived of as a summary of the movement since its founding in 1916, but rather meant to be a snapshot of its expanded incarnation at war's end. Not merely a vehicle for existing works, the project functioned as one of Dada's most generative catalysts for the production of new works.<ref>See Adrian Sudhalter, ed. ''Dadaglobe Reconstructed'' (Zurich: Scheidegger and Spiess and Kunsthaus Zürich, 2016) ISBN 978-3-85881-775-4</ref>

==History==
[[File:Max Ernst, 1920, Punching Ball ou l'Immortalité de Buonarroti, photomontage, gouache, et encre sur photographie.jpg|thumb|[[Max Ernst]], 1920, ''Punching Ball'' (''l'Immortalité de Buonarroti''), photomontage, gouache and ink on photograph]]
The ''Dadaglobe'' solicitation letter, sent from Paris in early November 1920, requested four types of visual submissions—photographic portraits (which could be manipulated, but should "retain clarity"); original drawings; photographs of artworks; and designs for book pages—along with prose, poetry, or other verbal "inventions."  Contributors included [[Jean Arp|Jean (Hans) Arp]], [[Marcel Duchamp]], [[Max Ernst]], [[George Grosz]], [[John Heartfield]], [[Hannah Höch]], [[Francis Picabia]], [[Man Ray]], [[Kurt Schwitters]], and [[Sophie Taeuber]] among others (see full list of contributors below). 

[[File:Johannes Theodor Baargeld – Typische Vertikalklitterung als Darstellung des Dada Baargeld - 1920.jpg|thumb|[[Johannes Theodor Baargeld|Johannes Baargeld]], 1920, ''Typical Vertical Mess as Depiction of the Dada Baargeld'' (''Typische Vertikalklitterung als Darstellung des Dada Baargeld'')]] Some of Dada's most iconic artworks were created in direct response to the ''Dadaglobe'' solicitation letter: Ernst's self-portrait montage commonly known as ''Dadamax'' (The Punching Ball or the Immortality of Buonarroti) and his ''Chinese Nightingale'',<ref>[http://www.wikiart.org/en/max-ernst/the-chinese-nightingale-1920 Max Ernst, ''The Chinese Nightingale (Le Rossignol chinois)'', 1920, Musée des Beaux-Arts, Grenoble, France]</ref> Taeuber's ''Dada Head'',<ref>[http://www.moma.org/explore/inside_out/2016/01/19/celebrating-sophie-taeuber-arps-127th-birthday/ ''Celebrating Sophie Taeuber-Arp’s 127th Birthday'', MoMA]</ref> and Baargeld's ''Typical Vertical Mess as Depiction of the Dada Baargeld'' are just a few examples. These works were conceived by the artists with their presentation in reproduction foremost in mind. ''Dadaglobe'' was to have been a manifesto on the revised status of the artwork in reproduction.  

The volume was advertised in Duchamp's and Man Ray's journal ''New York Dada'' in 1921: "Order from the publishing house 'La Sirène' 7 rue Pasquier, Paris, DADAGLOBE, the work of dadas from all over the world […] The incalculable number of pages of reproductions and of text is a guarantee of the success of the book."<ref>Marcel Duchamp and Man Ray, eds. ''New York Dada'' (single issue; April 1921), n. p. [2].</ref>  When [[André Breton]], later founder of [[Surrealism]], saw the intended contents of the book, he remarked: "The grand album 'Dadaglobe' […] will soon appear. […] After the publication of this volume it will be impossible to contest Dada's artistic value."<ref>Quoted in Adrian Sudhalter, "Max Ernst and Tristan Tzara's Dadaglobe" in ''Max Ernst: Retrospective'', Werner Spies and Julia Drost, eds. Exh. cat.. (Ostfildern: Hatje Cantz Verlag, 2013).p. 86. ISBN 9783775734479</ref> 

In scope, ambition, and even title, Tzara's Paris-based ''Dadaglobe'', was modeled on [[Richard Huelsenbeck]]'s Berlin-based, ''Dadaco'', planned the previous year, but also abandoned and never published.<ref>For the relationship between the two unrealized volumes see: Richard Sheppard, "Introduction", in ''Zürich — Dadaco — Dadaglobe: The Correspondence between Richard Huelsenbeck, Tristan Tzara, and Kurt Wolff (1916–1924)'' (Tayport: Hutton Press 1982), pp. 5–8. ISBN 9780907033073</ref> ''Dadaglobe'' reached an advanced stage of planning before financial and interpersonal obstacles put a halt to the project in spring 1921, leaving a lacuna where there ought to have been a magnum opus.  

==Dadaglobe Reconstructed==
Numerous archival traces provide an indication of the intended contents of ''Dadaglobe''. The French scholar [[Michel Sanouillet]] (1924-2015) rediscovered the abandoned project and, in 1966, published a selection of the texts intended for the original anthology.<ref>Michel Sanouillet, ''Le Dossier de Dadaglobe'', in ''Cahiers de l'association internationale pour l'étude de Dada et du Surréalisme'', no. 1 (1966), pp. 111–143.</ref> On the occasion of Dada's centennial in 2016, American scholar Adrian Sudhalter published ''Dadaglobe Reconstructed'', a book that includes a 160-page reconstruction of ''Dadaglobe'' within a scholarly armature, accompanied by a preface by Sanouillet.<ref>Adrian Sudhalter, ed. ''Dadaglobe Reconstructed'' (Zurich: Scheidegger and Spiess and Kunsthaus Zürich, 2016) ISBN 978-3-85881-775-4</ref> The publication accompanies Sudhalter's exhibition of the same name, on view at the Kunsthaus Zürich (February 5-May 1, 2016)<ref>[http://www.kunsthaus.ch/en/exhibitions/coming-soon/dadaglobereconstructed/?redirect_url=title%3Dcash Dadaglobe Reconstructed. Kunsthaus Zurich]</ref> and The Museum of Modern Art, New York (June 12-September 18, 2016).<ref>[http://press.moma.org/2015/12/dadaglobe-reconstructed/ Dadaglobe Reconstructed. Museum of Modern Art]</ref> 

==Participants==
Participants in ''Dadaglobe'' included:

{{Div col|2}}
* [[Louis Aragon]] (1897-1982), France
* [[Jean Arp]] (1886-1966), Germany, France
* [[Johannes Baader]] (1875 –1955), Germany
* [[Johannes Theodor Baargeld]] (1892–1927), Germany
* Egidio Bacchi (1897-1963), Italy
* [[Erwin Blumenfeld]] (1897–1969), Germany, Netherlands, France, USA 
*[[Constantin Brancusi]] (1876-1957), Romania, France
* [[André Breton]] (1896-1966), France
* [[:fr:Gabrièle Buffet-Picabia|Gabrielle Buffet-Picabia]] (1881-1985), France
* Margueritte Buffet
* [[Gino Cantarelli]] (1899 – 1950). Italy
* [[:fr:Serge Charchoune|Serge Charchoune]] (1889-1975), Russia, France
* [[Paul Citroen]] (1896-1983), Germany, Netherlands
* [[Jean Cocteau]] (1889-1963), France
* [[Jean Crotti]] (1886-1951), France, USA
* [[Paul Dermée]] (1886-1951), Belgium, France
* [[Theo van Doesburg]] (1883-1931) Netherlands
* [[Marcel Duchamp]] (1887-1968), France
* [[Suzanne Duchamp]] (1889-1963), France
* Jacques Edwards
* [[Paul Éluard]] (1895-1952), France
* [[Max Ernst]] (1891-1976), Germany, USA
* [[Julius Evola]] (1898-1974), Italy
* Aldo Fiozzi
* Baroness [[Elsa von Freytag-Loringhoven]] (1874-1927), Germany, USA
* [[Otto Griebel]] (1895-1972), Germany
* [[George Grosz]] (1893-1959), Germany, France, USA
* Job Haubric
* [[Raoul Hausmann]] (1886-1971), Germany
* [[John Heartfield]] (1891-1968), Germany, USSR, Czechoslovakia, Great Britain
* [[Hannah Höch]] (1889-1978), Germany
* [[Richard Huelsenbeck]] (1892-1974), Germany
* [[Marcel Janco]] (1895-1984), Romania, Israel
* Adon Lacroix (1887–1975), Belgium, USA
* [[Clément Pansaers]] (1885-1922), Belgium
* [[Benjamin Péret]] (1899-1959), France
* [[Francis Picabia]] (1879-1953), France
* [[Man Ray]] (1890-1976), France, USA
* [[Georges Ribemont-Dessaignes]] (1884-1974), France
* [[Jacques Rigaut]] (1898-1929), France
* [[Kurt Schwitters]] (1887-1948), Germany
* [[Philippe Soupault]] (1897-1990), France
* [[Joseph Stella]] (1877-1946), Italy, USA
* [[:de:Luise Straus|Luise Straus]] (Armada von Duldgedalzen) (1893-1944), Germany
* [[Sophie Taeuber-Arp]] (1889-1943), Switzerland, France
* [[Guillermo de Torre]] (1900-1971), Spain, France, Argentina
* [[Tristan Tzara]] (1896-1963), Romania, France
* Alfred Vagts (1892-1986), Germany, USA 
* [[Edgar Varèse]] (1883–1965), France, USA
* [[:de:Melchoir Vischer|Melchoir Vischer]] (1895-1975), Germany
* Fried-Hardy Worm
{{Div col end}}

==See also==
*[[Dada]]
*[[New York Dada]]

==References==
{{reflist}}
<!--- After listing your sources please cite them using inline citations and place them after the information they cite. Please see https://en.wikipedia.org/wiki/Wikipedia:REFB for instructions on how to add citations. --->
*
*
*
*
==External links==
{{Library resources box|by=no|onlinebooks=no|about=yes|wikititle=Dada}}
{{Wikiquote|Dada}}
*{{commons category inline|Dada}}
*The [http://www.lib.uiowa.edu/dada/ International Dada Archive] - at the University of Iowa has early Dada periodicals and includes online scans of publications
*[http://www.dadart.com/dadaism/dada/index.html Dadart] - includes history, bibliography, documents, and news
* [http://bibliothequekandinsky.centrepompidou.fr/clientBookline/service/reference.asp?INSTANCE=incipio&OUTPUT=PORTAL&DOCID=0473982&DOCBASE=CGPP New York dada (magazine), Marcel Duchamp and Man Ray, April, 1921], Bibliothèque Kandinsky, Centre Pompidou (access online)

[[Category:Dada]]
[[Category:20th-century French literature]]
[[Category:20th-century German literature]]