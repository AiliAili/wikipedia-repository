{{Infobox journal
| title = {{lang|en|Canadian Journal of Chemistry}}<br/>{{lang|fr|Revue canadienne de chimie}}
| cover = [[File:Canadian Journal of Chemistry cover.jpg|150 px]]
| discipline = [[Chemistry]]
| abbreviation = Can. J. Chem.
| language = English, French
| website = http://nrcresearchpress.com/cjc
| editor = Yining Huang
| publisher = [[NRC Research Press]]
| country = Canada
| history = 1951-present
| impact = 1.003
| impact-year = 2015
| frequency = Monthly
| link1 = http://nrcresearchpress.com/loi/cjc
| link1-name = Online archive
| link2 = http://nrcresearchpress.com/page/cjc/editors
| link2-name = About the Journal
| ISSN = 0008-4042
| eISSN = 1480-3291
| CODEN = CJCHAG
| LCCN =    54024664
| OCLC = 02248672
}}
The '''''Canadian Journal of Chemistry''''' (fr. '''''Revue canadienne de chimie''''') is a [[peer review|peer-reviewed]] [[scientific journal]] published by [[NRC Research Press]]. It was established in 1951 as the continuation of ''[[Canadian Journal of Research, Section B: Chemical Sciences]]''. Papers are loaded to the web in advance of the printed issue and are available in both pdf and HTML formats.

== Abstracting and indexing ==
The journal is abstracted and indexed by the following services: [[Chemical Abstracts Service|Chemical Abstracts]], [[ChemInform]], [[Chemistry Citation Index]], [[Compendex]], [[Current Contents]], [[Derwent Biotechnology Abstracts]], [[GeoRef]], [[International Nuclear Information System|INIS Atomindex]], [[Methods in Organic Synthesis]], [[Referativny Zhurnal]], and the [[Science Citation Index]].<ref>{{cite web |url=http://pubs.nrc-cnrc.gc.ca/rp-ps/journalDetail.jsp?jcode=cjc&lang=eng |title=About the journal |publisher=NRC Research Press |format= |work=Canadian Journal of Chemistry |accessdate=2010-12-17}}</ref> According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 1.061.<ref>Journal Citation Reports, 2015</ref>

== References ==
{{Reflist}}

== External links ==
* {{Official website|1=http://pubs.nrc-cnrc.gc.ca/cgi-bin/rp/rp2_desc_e?cjc}}

[[Category:Chemistry journals]]
[[Category:Monthly journals]]
[[Category:Multilingual journals]]
[[Category:Publications established in 1951]]
[[Category:NRC Research Press academic journals]]