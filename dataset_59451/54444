{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
'''North Adelaide Grammar School''', later '''Whinham College''' was a private school operated in [[North Adelaide, South Australia]] by John Whinham (3 August 1803 – 13 March 1886) and his family.

==History==

===John Whinham===
The founder of the school was born at [[Sharperton]], [[Northumberland]], and when very young displayed a thirst for knowledge and an aptitude for mathematics. He was tutored by a Roman Catholic clergyman, and at age 19 while acting as an assistant teacher qualified for entry to the University of Dublin, but family illnesses kept him in England, and in 1823 he took to teaching, and opened a school in [[Ovingham]], near [[Newcastle upon Tyne]]. He was very successful there, and he received offers from Newcastle to move there, but chose to remain in Ovingham, where he married and became the father of six daughters and two sons. He became quite well off financially, but lost most of his savings in the economic downturn of 1848–1849. The family emigrated to Australia on the ''Athenian'', and arrived in Adelaide, by way of Melbourne, in 1852. He had intended to start afresh as a farmer, but must have had second thoughts as he accepted an appointment as mathematics master at [[St Peter's College, Adelaide|St. Peter's College]], which he left after a few months to open his own school in North Adelaide, first at the Salem chapel, then at [[Matthew Goode and Co|Goode's store]] in Kermode Street,<ref>{{cite news |url=http://nla.gov.au/nla.news-article28691144 |title=Mr. Whinham's Old Scholars' Association Reunion |newspaper=[[The South Australian Advertiser]] |location=Adelaide |date=20 December 1872 |accessdate=19 January 2015 |page=3 |publisher=National Library of Australia}}</ref> finally at new premises at the corner of Ward and Jeffcott Streets, which later became [[Australian Lutheran College]]. It started small but rapidly grew, and for many years was one of the largest private boarding and day schools in Australia.<ref>{{cite news |url=http://nla.gov.au/nla.news-article50188031 |title=The Late Mr. John Whinham |newspaper=[[South Australian Register]] |location=Adelaide |date=15 March 1886 |accessdate=19 January 2015 |page=7 |publisher=National Library of Australia}}</ref>

There is a reference to him losing his fortune a second time, which has yet to be explained. His youngest daughter died in 1882 and he retired shortly after, handing over management of the school to David, his eldest son. The school was then renamed "Whinham College" in his honour. In October 1881, his old scholars presented him with an oil portrait of himself painted by [[Andrew MacCormac]].

Less than two years later, David died as the result of an accident, and John was forced to resume management duties; he himself died less than two years later.

===Robert Whinham===
Son Robert and a sister were teaching at the School in 1863<ref>{{cite news |url=http://nla.gov.au/nla.news-article31824881 |title=Topics of the Day |newspaper=[[The South Australian Advertiser]] |location=Adelaide |date=19 June 1863 |accessdate=19 January 2015 |page=2 |publisher=National Library of Australia}}</ref> and he was described as "second master" in 1864.

He made headlines when he pulled a prank on an old friend, which a third party objected to and informed the police. The incident became a court case and ''cause célèbre''.<ref>{{cite news |url=http://nla.gov.au/nla.news-article41018673 |title=Mr. Whinham's Case |newspaper=[[South Australian Register]] |location=Adelaide |date=13 June 1866 |accessdate=19 January 2015 |page=3 |publisher=National Library of Australia}}</ref>

In 1882 John Whinham, nearly 80 years old, formally retired from teaching and handed over management of the school to his son Robert. His rule was cut short however, when he was thrown from his horse at North Adelaide when returning from the city on 24 October 1884, and died that evening. It is likely that the reins, which had been broken early that day, had been inexpertly repaired and broke again. The horse, David's favourite, was a skittish animal and without both reins was uncontrollable, and taking alarm at some small thing threw him sideways, breaking his spine when he hit the ground.<ref>{{cite news |url=http://nla.gov.au/nla.news-article35969200 |title=Fatal Accident to Mr. Robert Whinham |newspaper=[[The South Australian Advertiser]] |location=Adelaide |date=13 October 1884 |accessdate=19 January 2015 |page=7 |publisher=National Library of Australia}}</ref>

David was well known as an [[elocutionist]], probably encouraged by his mother, and was frequently called upon to perform public recitations.

===George Newman===
George Gough Newman, B.A. (ca.1862 – 30 May 1929) George was a student at Grote Street Model School<ref>{{cite news |url=http://nla.gov.au/nla.news-article129136574 |title=Noted Educationist |newspaper=[[The News (Adelaide)|The News]] |location=Adelaide |date=30 May 1929 |accessdate=20 January 2015 |page=18 Edition: Home |publisher=National Library of Australia}}</ref> and North Adelaide Grammar School, matriculated in Adelaide, passed through the Teachers' College and was appointed assistant master at Hindmarsh school. He gained his B.A. from [[London University]], then served as master for two years at [[St Peter's College, Adelaide|St. Peter's College]]. He was appointed headmaster of Whinham College in 1894<ref>{{cite news |url=http://nla.gov.au/nla.news-article161802789 |title=The Head Master of Whinham College |newspaper=[[Adelaide Observer]] |location=SA |date=28 April 1894 |accessdate=20 January 2015 |page=16 |publisher=National Library of Australia}}</ref> under a board of management, and took over the lease of the school in 1895. The University School of North Adelaide was merged with Whinham College in 1896<ref name=bankrupt/>

In mid-1898 Newman was charged with indecent assault against a 14-year-old student in late 1897. The case came before the Supreme Court; Newman was defended by [[J. H. Symon]] Q.C., who demonstrated that the boy was unable to recall dates of the alleged incidents, or even of the date the school broke up. The judge, [[William Henry Bundey|Mr. Justice Bundey]], directed the jury to acquit.<ref>{{cite news |url=http://nla.gov.au/nla.news-article54389071 |title=Law and Criminal Courts |newspaper=[[South Australian Register]] |location=Adelaide |date=13 August 1898 |accessdate=20 January 2015 |page=3 |publisher=National Library of Australia}}</ref> Around the same time he was declared bankrupt; he blamed impatience of creditors.<ref name=bankrupt>{{cite news |url=http://nla.gov.au/nla.news-article162328726 |title=Insolvency Court |newspaper=[[Adelaide Observer]] |location=SA |date=3 September 1898 |accessdate=20 January 2015 |page=21 |publisher=National Library of Australia}}</ref> That was the last year the school operated. Newman was the author of a large number of educational booklets.<ref>{{cite news |url=http://nla.gov.au/nla.news-article4881822 |title=Mr. Newman's Educational Series |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=23 September 1902 |accessdate=20 January 2015 |page=6 |publisher=National Library of Australia}}</ref>

==Whinham family==
John Whinham (3 August 1803 – 13 March 1886) was married to Mary, née Bedlington, (ca.1811 – 4 September 1891)
*[[William Whinham]] (ca.1842 – 5 September 1925), teacher and pastoralist, founded the suburb of [[Ovingham, South Australia]], and was member for [[Electoral district of Victoria]] 1883–1884. He is reported to have married late in life, and she died around 1913,<ref name=william>{{cite news |url=http://nla.gov.au/nla.news-article89632130 |title=Obituary |newspaper=[[The Chronicle (South Australia)|The Chronicle]] |location=Adelaide |date=12 September 1925 |accessdate=20 January 2015 |page=32 |publisher=National Library of Australia}}</ref> though details are elusive.
*Robert Whinham (c. 1847 – 24 October 1884)
*eldest daughter Jane Whinham ( – 21 January 1908) married John William Parsons (died 14 December 1900) of Nairne on 5 April 1877, later lived at Whinham Street, [[Fitzroy, South Australia|Fitzroy]]
*Isabella Whinham (c. 1838 – 24 August 1931) married Walter Boyd Tate Andrews (c. 1824 – 5 April 1899) on 8 July 1858. Andrews was killed in a railway crossing accident at [[Upper Sturt, South Australia|Upper Sturt]].
*Mary Whinham ( – 19 October 1901) married John Harrison Packard (1847 – 11 August 1929) on 8 April 1874
*Annie Whinham ( – ) married Thomas William Harris ( – ) on 30 September 1875
*youngest daughter Margaret Emily Whinham ( – 22 May 1882)
They lived at Ovingham House, Buxton Street, North Adelaide

==Notable students==
*[[Harold Boas]] town planner and architect in Western Australia
*[[John Ragless Brookman]]<ref name=TBSA>Cumming, D. A. & Moxham, G. ''They Built South Australia'' published by the authors 1986, Adelaide, South Australia ISBN 0 9589111 0 X</ref>
*Edmund Sidney Clark<ref name=TBSA/>
*[[Alfred Rutter Clarke]] stockbroker and investor
*[[John Cowan (South Australian politician)|Sir John Cowan]] politician
*[[James Cowan (South Australian politician)|James Cowan]] flour miller and politician
*[[John Millard Dunn]] organist
*John Henry Osborn Eaton<ref name=TBSA/>
*[[Harry Congreve Evans]] journalist and editor
*James Fitzgerald, hero of the [[SS Gothenburg|SS ''Gothenburg'']] disaster<ref name=Hutchison>{{cite news |url=http://nla.gov.au/nla.news-article6429663 |title=Personal |newspaper=[[The_Advertiser_(Adelaide)|The Advertiser (Adelaide, SA : 1889 - 1931)]] |location=Adelaide, SA |date=18 August 1914 |accessdate=29 September 2015 |page=6 |publisher=National Library of Australia}}</ref>
*Arthur John Green<ref name=TBSA/>
*[[O. P. Heggie]] film and theatre actor.
*[[William Hutchison (pastoralist)|William Hutchison]] prominent pastoralist in the South-East<ref name=Hutchison/>
*[[Arthur Charles Jeston Richardson]] cyclist and mining engineer
*[[William Benjamin Rounsevell]] politician
*[[Alfred Sandover]] hardware merchant
*[[Henry Sparks]] businessman
*[[James Stewart (South Australian politician)|James Stewart]] pastoralist and politician
*[[Edward Vardon]] politician
*[[Ebenezer Ward]] politician and journalist
*[[Arthur Wellington Ware]] brewer, publican and Mayor of Adelaide
*Sir [[Reginald Victor Wilson]] businessman and politician.
*[[Walter James Young]] businessman

== References ==
{{Reflist}}


{{coord|34.909141|S| 138.592774|E|format=dms|display=inline,title}}
{{DEFAULTSORT:North Adelaide Grammar School}}
[[Category:Educational institutions established in 1852]]
[[Category:Private schools in South Australia]]
[[Category:High schools in South Australia]]
[[Category:Boys' schools in Australia]]
[[Category:Defunct schools in Australia]]
[[Category:Schools in Adelaide]]
[[Category:1852 establishments in Australia]]
[[Category:History of Adelaide]]