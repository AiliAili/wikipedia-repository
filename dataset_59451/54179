{{for|the sign painter|John Downer (signpainter)}}

{{Use dmy dates|date=August 2015}}
{{Use Australian English|date=August 2015}}
{{Infobox premier
|name = John Downer
|birthname = John William Downer
|birth_date = {{birth date|1843|6|6|df=yes}}
|birth_place = [[Adelaide, South Australia]]
|death_date = {{death date and age|1915|8|2|1843|6|6|df=yes}}
|death_place = [[North Adelaide]], South Australia
|honorific-prefix = [[The Honourable]] Sir
|honorific-suffix =  [[Order of St Michael and St George|KCMG]] [[Queen's Counsel|KC]]
|image=John Downer (Australian politician).jpg
|order=16th [[Premier of South Australia]]<br /><small>Elections: [[South Australian colonial election, 1893|1893]]<small>
|term_start1=16 June 1885
|term_end1=11 June 1887
|term_start2=15 October 1892
|term_end2=16 June 1893
|predecessor1=[[John Colton (politician)|John Colton]]
|successor1=[[Thomas Playford II]]
|predecessor2=[[Frederick Holder]]
|successor2=[[Charles Kingston]]
|monarch1  = [[Queen Victoria|Victoria]]
|governor1 = [[William C. F. Robinson|Sir William Robinson]]
|monarch2  = [[Queen Victoria|Victoria]]
|governor2 = [[Algernon Keith-Falconer, 9th Earl of Kintore|Earl of Kintore]]
|order3    = 3rd [[Leader of the Opposition (South Australia)|Leader of the Opposition]] (SA)
|term_start3  = 1885
|term_end3    = 1885
|predecessor3 = [[John Cox Bray]]
|successor3   = [[Jenkin Coles]]
|term_start4  = 1887
|term_end4    = 1889
|predecessor4 = [[Thomas Playford II]]
|successor4   = [[John Cockburn (Australian politician)|John Cockburn]]
|term_start5  = 1893
|term_end5    = 1895
|predecessor5 = [[Frederick Holder]]
|successor5   = [[William Copley (South Australian politician)|William Copley]]
|term_start6  = 1897
|term_end6    = 1899
|predecessor6 = [[William Copley (South Australian politician)|William Copley]]
|successor6   = [[Vaiben Louis Solomon]]
|office7 = [[Australian Senate|Senator]] for [[South Australia]]
|term_start7 = 30 March 1901
|term_end7 = 31 December 1903
|party=[[Conservatism|Conservative]] (second term) <br> [[Protectionist Party]]
}}

'''Sir John William Downer''', [[Order of St Michael and St George|KCMG]], [[Queen's Counsel|KC]] (6 July 1843 – 2 August 1915) was the [[Premiers of South Australia|Premier of South Australia]] twice, from 16 June 1885 until 11 June 1887 and again from 1892 to 1893. He was the first of four Australian politicians from the [[Downer family]] dynasty.

==Early life==
Born in [[Adelaide]], John Downer (the son of [[Downer family|Henry Downer]] who came to [[South Australia]] in 1838 and his wife Jane, ''née'' Field) was educated on a scholarship at [[St Peter's College, Adelaide]], where he was a brilliant student.<ref>Downer, Alick (2012). ''The Downers of South Australia'', p. 35. Wakefield Press, Adelaide. ISBN 9781743051993</ref> Later (23 March 1867), he was admitted to the bar, and soon won a reputation as being among Adelaide's most talented and eloquent lawyers.

==South Australian politician==
Downer became a [[Queen's Counsel]] in 1878, the same year in which he was elected to the [[South Australian House of Assembly|House of Assembly]] for [[Barossa Valley|Barossa]]. He represented this constituency until 1901, leaving it only to enter federal politics.

In the House of Assembly he soon made his mark and became [[Attorney-General of South Australia|Attorney-General]] in [[John Cox Bray]]'s cabinet on 24 June 1881. He endeavoured to bring in several law reforms, and though his married women's property bill was not passed, he succeeded in carrying bills allowing accused persons to give evidence on oath, and amending the insolvency and marriage acts. The government was defeated in June 1884, but a year later, on 16 June 1885, Downer himself became Premier for the first time, as well as being Attorney-General once again.

While Premier, Downer oversaw the construction of the first train line from Adelaide to Melbourne. He also made significant contributions to establishing irrigation settlements along the Murray River. Although this ministry lasted two years and passed a fair amount of legislation, it was often in difficulties, and in June 1886 had to be reconstructed.

At the [[1887 Colonial Conference|Colonial Conference]] held in London during 1887, Downer represented South Australia, but during his return journey to Australia his government was defeated. This ministry was responsible for a tariff imposing increased protective duties. Downer was not in office again for several years, but in October 1892 again became Premier, taking also the portfolio of Chief Secretary. In May 1893 he exchanged this for the position of [[Treasurer of South Australia]], but was ousted at the [[South Australian colonial election, 1893|1893 election]] by liberal [[Protectionist Party|Protectionist]] [[Charles Kingston|Kingston]] with the support of the new [[Australian Labor Party|Labor Party]] led by [[John McPherson (Australian politician)|John McPherson]]. Downer remarked of this party: 'They are very clever fellows. I have great respect for the way they use either side for their purposes with absolute impartiality'. For most of the period until 1899 Downer led the Opposition.

Downer was a strong federalist and had represented South Australia at the 1883 and 1891 [[Federation of Australia|conventions]]. At the latter meeting, he took an important part in protecting the interests of the smaller states, and was a member of the constitutional committee. He was elected one of the 10 representatives of South Australia at the 1897 convention, and was again on the constitutional committee.

==Federal politician and return to state politics==
At the time of Federation in 1901, Downer was elected as one of the inaugural [[Senate of Australia|senators]] for the [[Protectionist Party]] in [[South Australia]] at the first [[Parliament of Australia]], but he did not seek re-election in 1903. He entered the [[South Australian Legislative Council]] as a [[National Defence League]] ([[Liberal Union (South Australia)|Liberal Union]] from 1910) representative of the southern district in 1905, and continued to be re-elected until his death on 2 August 1915.<ref>{{cite news |url=http://nla.gov.au/nla.news-article59414161 |title=Family Notices |newspaper=[[The Register (Adelaide)]] |volume=LXXX, |issue=21,446 |location=South Australia |date=5 August 1915 |accessdate=7 June 2016 |page=11 |via=National Library of Australia}}</ref>

==Character==
[[Alfred Deakin]] assessed Downer in the following terms: 'bull-headed, and rather thick-necked, … with the dogged set of the mouth of a prize fighter' and 'smallish eyes'. Downer was regarded a first-rate barrister, and some of his speeches to juries were singled out by contemporaries as laudable examples of forensic art. He was equally successful in parliamentary debate; one of his colleagues called him the best debater in a house that contained Charles Kingston, [[Frederick Holder]], [[John Cockburn (Australian politician)|John Cockburn]], and [[John Jenkins (Australian politician)|John Jenkins]].

In politics Downer tended to be conservative without being obstinate. He described himself as a [[Tory]], and partly on account of this he often found himself in a minority during his later years in parliament. Nevertheless, he consistently advocated the rights of married women to their own property, [[women's suffrage|female suffrage]], protection of local industries, and [[Federation of Australia|federation]].

==Family and Legacy==
[[File:John Downer.jpg|thumb|right]]
Downer married twice: firstly in 1871 to Elizabeth Henderson, daughter of the Rev. [[James Henderson (minister)|James Henderson]];<ref>{{cite news |url=http://nla.gov.au/nla.news-article162425473 |title=OBITUARY. |newspaper=[[The Observer (Adelaide)|The Observer]] |volume=LXII, |issue=3,316 |location=South Australia |date=22 April 1905 |accessdate=8 June 2016 |page=31 |via=National Library of Australia}}</ref> and secondly, in Sydney 29 November 1899 to Una Stella Haslingden Russell, daughter of Henry Edward Russell.<ref>Una Stella Haslingden Russell (b. 1871 at Goulbourn), is not related to [[Henry Chamberlain Russell]], notwithstanding the report in the article 'Interesting Weddings' ({{cite news |url=http://nla.gov.au/nla.news-article123226399 |title=Interesting Weddings. |newspaper=[[The Sunday Times (Sydney, Australia)|Sunday Times (Sydney, NSW : 1895 - 1930)]] |location=Sydney, NSW |date=2 March 1919 |accessdate=3 February 2016 |page=2 |publisher=National Library of Australia}}. Her parents being Henry Edward Russell and Frances Emily Russell nee Robey ({{cite news |url=http://nla.gov.au/nla.news-article101472669 |title=Family Notices |newspaper=[[The Goulburn Herald And Chronicle]] |location=New South Wales, Australia |date=8 June 1870 |accessdate=7 June 2016 |page=2 |via=National Library of Australia}}). Frances Emily Russell being the daughter of [[Ralph Mayer Robey]], MLC (1809 - 1864)</ref> With Elizabeth he had three children, John Henry (born 1872), James Frederick (born 1874) and Harold Sydney (born in 1875 and died in infancy).<ref>Downer, Alick (2012). ''The Downers of South Australia'', p. 36. Wakefield Press, Adelaide. ISBN 9781743051993</ref>
The son of his second marriage was [[Alexander Russell Downer]] (born 1910), who served in the [[Robert Gordon Menzies|Menzies]] government, was knighted, and served as Australian High Commissioner in London, and whose son, [[Alexander Downer]] served as leader of the (Opposition) Liberal party in 1994 and Foreign Minister in the [[John Howard|Howard]] government.

The home he purchased in 1880 at 42 Pennington Terrace, [[North Adelaide]], is now [[St Mark's College (University of Adelaide)|St Mark's College]] and the original part of the building is known as Downer House. A draft of the Australian Constitution was prepared in the ballroom in 1897.<ref>Downer, Alick (2012). ''The Downers of South Australia'', p. 37. Wakefield Press, Adelaide. ISBN 9781743051993</ref>

A brother and partner in his business, [[Henry Edward Downer]] (1836&ndash;1905), entered the South Australian parliament in 1881 and was attorney-general in the [[John Cockburn (Australian politician)|John Cockburn]] ministry from May to August 1890. Another brother, [[A. G. Downer|A(lexander) George Downer]] (1839&ndash;1916) was his partner in the legal firm G & J Downer and a prominent businessman.<ref>{{cite news |url=http://nla.gov.au/nla.news-article87312225 |title=A Splendid Citizen |newspaper=[[The Chronicle (Adelaide)|The Chronicle]] |location=Adelaide |date=19 August 1916 |accessdate=13 November 2012 |page=37 |publisher=National Library of Australia}}</ref>

In 1887, at the [[Imperial Conference]] in London (now the [[Commonwealth Heads of Government Meeting]]), Downer was created [[Order of St Michael and St George|KCMG]], recommended to the Queen by the Marquis of Salisbury.<ref>Downer, Alick (2012). ''The Downers of South Australia'', p. 44. Wakefield Press, Adelaide. ISBN 9781743051993</ref>
During retirement, he joined the [[Adelaide University]] Council and became president of the Commomwealth Club.<ref>Downer, Alick (2012). ''The Downers of South Australia'', p. 72. Wakefield Press, Adelaide. ISBN 9781743051993</ref>

The [[Canberra]] suburb of [[Downer, Australian Capital Territory]] was named after him in 1960. On [[Garema Place]], [[Canberra]] stands a commemorative sculpted fountain titled Father and Son and was presented by his son [[Alec Downer|Alick]] in 1964.<ref>{{cite web |url= http://www.environment.gov.au/cgi-bin/ahdb/search.pl?mode=place_detail;place_id=18027 |title= Father and Son Sculpture, Garema Pl, Canberra, ACT, Australia|date = 2014 |website= Australian Heritage Database |accessdate= 2 May 2014 }}</ref>

==Notes==
{{reflist}}

==References==
*Bartlett, P. '[http://www.adb.online.anu.edu.au/biogs/A080355b.htm Downer, Sir John William (1843–1915)]', ''[[Australian Dictionary of Biography]]'', Volume 8, Melbourne University Press, 1981, pp.&nbsp;330–332. Retrieved on 4 October 2008.
*{{Dictionary of Australian Biography|first=John William|last=Downer|shortlink=0-dict-biogD.html#downer1|accessdate=2008-10-04}}
*''Parliamentary Debates'' (South Australia), 1883–84, 2031
*''Intercolonial Convention, 1883: Report of the Proceedings of the Intercolonial Convention, held in Sydney, in November and December, 1883'' (Syd, 1883)
*''Proceedings of the Colonial Conference, 1887: Papers Laid before the Conference'' (Lond, 1887)
*National Australasian Convention, 1891 to 1898, ''Official Record of the Proceedings …'' (Sydney 1891, Adelaide 1897, Sydney 1898 and Melbourne 1898)
*''British Australasian'', 17 June 1887
*Edmund Barton papers (National Library of Australia)
*Alfred Deakin papers (National Library of Australia)
*P. M. Glynn diaries, 1880–1918 (National Library of Australia)
*''The Register'', Adelaide, 3 August 1915
*''The Advertiser'', Adelaide, 3 August 1915
*E. Hodder, ''The History of South Australia''
*Quick and Garran, ''The Annotated Constitution of the Australian Commonwealth''
*P. Mennell, ''The Dictionary of Australasian Biography''

==External links==
*[http://www2.parliament.sa.gov.au/formermembers/Detail.aspx?pid=4191 Parliament Profile]
{{Commons category|John William Downer}}

{{s-start}}
{{s-off}}
{{s-bef|before=[[John Cox Bray]]}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition of South Australia]]|years=1884&ndash;1885}}
{{s-aft|after=[[Jenkin Coles]]}}
|-
{{s-bef | before = [[John Colton (politician)|John Colton]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1885&ndash;1887}}
{{s-aft|after=[[Thomas Playford II|Thomas Playford]]}}
|-
{{s-bef|before=[[Thomas Playford II|Thomas Playford]]}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition of South Australia]]|years=1887&ndash;1889}}
{{s-aft|after=[[John Cockburn (Australian politician)|John Cockburn]]}}
|-
{{s-bef | before = [[Frederick Holder]]}}
{{s-ttl | title = [[Premier of South Australia]] | years =1892&ndash;1893}}
{{s-aft|after=[[Charles Kingston]]}}
|-
{{s-bef|before=[[Frederick Holder]]}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition of South Australia]]|years=1893&ndash;1895}}
{{s-aft|after=[[William Copley (South Australian politician)|William Copley]]}}
|-
{{s-bef|before=[[William Copley (South Australian politician)|William Copley]]}}
{{s-ttl|title=[[Leader of the Opposition (South Australia)|Leader of the Opposition of South Australia]]|years=1897&ndash;1899}}
{{s-aft|after=[[Vaiben Solomon]]}}
|-
{{s-par|au-sa-la}}
{{s-bef|before=[[John Dunn (miller)#Family|John Dunn Jr.]]}}
{{s-ttl|title=Member for [[Electoral district of Barossa|Barossa]]|years=1878&ndash;1901|alongside=[[Martin Peter Friedrich Basedow|Martin Basedow]], [[James Hague]]}}
{{s-aft|after=[[E. H. Coombe|Ephraim Coombe]]}}
{{end}}

{{Premiers of South Australia}}

{{Authority control}}

{{DEFAULTSORT:Downer, John}}
[[Category:1843 births]]
[[Category:1915 deaths]]
[[Category:Premiers of South Australia]]
[[Category:Attorneys-General of South Australia]]
[[Category:Australian barristers]]
[[Category:Australian suffragists]]
[[Category:Australian federationists]]
[[Category:Politicians from Adelaide]]
[[Category:Australian people of English descent]]
[[Category:Members of the Australian Senate for South Australia]]
[[Category:Members of the Australian Senate]]
[[Category:Members of the South Australian Legislative Council]]
[[Category:Australian Knights Commander of the Order of St Michael and St George]]
[[Category:People educated at St Peter's College, Adelaide]]
[[Category:Downer family]]
[[Category:Leaders of the Opposition in South Australia]]
[[Category:Treasurers of South Australia]]
[[Category:Australian Queen's Counsel]]
[[Category:Male feminists]]
[[Category:Protectionist Party members of the Parliament of Australia]]
[[Category:19th-century Australian politicians]]
[[Category:20th-century Australian politicians]]