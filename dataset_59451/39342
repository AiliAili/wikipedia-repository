{{good article}}
{{Infobox football match
| title              = Cardiff Blues vs Leicester Tigers
| image              = 
| event              = [[2008–09 Heineken Cup]]
| team1              = [[Cardiff Blues]]
| team1association   = {{flagicon|WAL|size=30px}}
| team1score         = 26
| team2              = [[Leicester Tigers]]
| team2association   = {{flagicon|ENG|size=30px}}
| team2score         = 26
| details            = Leicester won 7–6 on [[Penalty shootout|penalties]]
| date               = 3 May 2009
| stadium            = [[Millennium Stadium]]
| city               = [[Cardiff]]
| man_of_the_match1a = [[Tom Croft]] (Leicester)
| referee            = [[Alain Rolland]] ([[Irish Rugby Union|Ireland]])
| attendance         = 44,212
| weather            = 
}}
The second semi-final of the [[2008–09 Heineken Cup]], the premier European club [[rugby union]] competition, saw [[Cardiff Blues]] take on [[Leicester Tigers]] at the [[Millennium Stadium]] in Cardiff on 3 May 2009. The scores were level after regular time and neither team was able to score during [[extra time]], resulting in the first ever [[Penalty shootout#Rugby union|penalty shoot-out]]{{efn|The tournament organisers used the term "Place-kick competition" in the tournament rules, however most media outlets referred to this as a "Penalty shoot-out".}} in a professional rugby match. Both teams missed one of their first five kicks, taking the shoot-out to sudden death. Both teams were successful with their kicks in the first two rounds of sudden death, before [[Martyn Williams]] missed Cardiff's eighth kick allowing [[Jordan Crane (rugby union)|Jordan Crane]] to hit the winner.

In regular time, Cardiff took an early lead with a [[Penalty (rugby union)|penalty]] before Leicester's [[Scott Hamilton (rugby union)|Scott Hamilton]] scored a converted try and they kicked another penalty. Cardiff scored three further penalties to put them back into the lead, before another successful penalty by Leicester gave them a one-point lead going into half-time. Early in the second half, [[Geordan Murphy]] scored another try for Leicester, which again was converted and kicked two more penalties to extend their lead to 26–12 with 20 minutes remaining. Leicester then had two players sent to the [[sin bin]], and Cardiff scored two tries in the last 10 minutes of regular time, first by [[Jamie Roberts]] and then [[Tom James (rugby player)|Tom James]], with both conversions successful to bring the scores level at 26–26 and take the game into extra time.

Following the match, there was criticism of the format used for the penalty shoot-out, specifically the way that the game was decided by kicks at goal attempted by players who wouldn't ordinarily kick the ball during a rugby match. One journalist commented that was a "ludicrous" way of deciding a game,<ref name="TelegraphOpinion" /> while another believed it had turned the semi-final into a "pantomime" and was an "unnecessarily demeaning way" of deciding a winner.<ref name="GuardianOpinion" /> A review was promised by tournament organisers, and changes were made to the format ahead of the following season's tournament, though no other Heineken Cup match ever needed to be decided by a penalty shoot-out.

==Background==
[[File:Millenium Stadium Reflected.jpg|thumb|right|Cardiff elected to stage the game at the [[Millennium Stadium]]]]
Cardiff Blues had been the only team to finish the [[2008–09 Heineken Cup pool stage|pool stage of the Heineken Cup]] unbeaten during the 2008–09 season,<ref>{{cite news |url=https://www.theguardian.com/sport/2009/jan/23/heineken-cup-cardiff-blues-calvisano |title=Blair opens floodgates for Blues to cruise through in top spot |first=Paul |last=Rees |date=23 January 2009 |work=The Guardian |accessdate=14 June 2015}}</ref> which saw them given top [[Seed (sports)|seeding]] for the quarter-finals. This meant that they would have a home quarter-final against the second-best runners-up from the pool stage, [[Stade Toulousain|Toulouse]].<ref name="ERCquarterfinals">{{cite news |title=Heineken Cup quarter-finals confirmed |url=http://www.ercrugby.com/eng/5018_12188.php |work=ercrugby.com |publisher=European Rugby Cup |date=31 January 2009 |archiveurl=https://web.archive.org/web/20090531170254/http://www.ercrugby.com/eng/5018_12188.php |archivedate=31 May 2009 |deadurl=yes }}</ref> Cardiff won the quarter-final 9–6.<ref>{{cite news |url=https://www.theguardian.com/sport/2009/apr/11/heineken-cup-cardiff-blues-toulouse |title=Blair's boot guides Cardiff Blues to thrilling win |work=The Guardian |first=Andrew |last=Baldock |date=11 April 2009 |accessdate=14 June 2015}}</ref>

Leicester lost two games during the pool stage, but amassed sufficient points to finish as the top side in the pool and to be seeded fourth, which meant they were given the last of the home draws against the fifth-seeded side, [[Bath Rugby|Bath]].<ref name="ERCquarterfinals"/> Leicester beat Bath 20–15 to reach the semi-final.<ref>{{cite news |url=https://www.theguardian.com/sport/2009/apr/13/heineken-cup-leicester-bath |title=Cockerill insists scrum-half Dupuy is staying at Leicester |first=Mike |last=Averis |date=13 April 2009 |work=The Guardian |accessdate=14 June 2015}}</ref>

In a draw made prior to the quarter-finals, it was determined that the winner of the match between Cardiff and Toulouse would have home country advantage, meaning that they could choose any venue in their own country for the semi-finals, provided it was not their designated home ground. The winner of the match between Leicester and Bath was drawn against them as the away team.<ref>{{cite news |url=http://www.ercrugby.com/eng/12_12201.php |title=Heineken Cup Semi-Final Draw Completed |date=27 January 2009 |work=ercrugby.com |publisher=European Rugby Cup |archiveurl=https://web.archive.org/web/20090131102230/http://www.ercrugby.com/eng/12_12201.php |archivedate=31 January 2009 |deadurl=yes }}</ref> Cardiff elected to stage the game at the Millennium Stadium in Cardiff, which is located adjacent to their usual [[Cardiff Arms Park]] ground and has a much larger capacity of 74,500, compared to the 12,500 capacity of the Arms Park. Cardiff had already moved their quarter-final game against Toulouse to the Millennium Stadium, despite being able to use their own ground at that stage of the competition, to take advantage of the increased capacity.<ref>{{cite news |url=http://news.bbc.co.uk/sport1/hi/rugby_union/8463879.stm |title=Dai Young disappointed at Cardiff Blues crowd |date=17 January 2010 |work=BBC Sport |accessdate=14 June 2015}}</ref>

==Regular time==
Cardiff suffered an early setback when their [[Captain (sports)|captain]], [[Paul Tito]], was injured after nine minutes and had to be replaced.<ref name="BBCMatchReport">{{cite news |url=http://news.bbc.co.uk/sport1/hi/rugby_union/8021824.stm |title=Cardiff Blues 26-26 Leicester (aet) |work=BBC Sport |date=3 May 2009 |accessdate=14 June 2015}}</ref> [[Ben Blair]] gave Cardiff the lead with a [[Penalty (rugby union)|penalty kick]] before the 15-minute mark, but Leicester were then awarded three penalties in quick succession due to infringements by Cardiff. For each, [[Julien Dupuy]] elected to attempt a kick at goal, but was unsuccessful with all three attempts. Leicester's first score came in the form of a [[try]] from [[Scott Hamilton (rugby union)|Scott Hamilton]] after 21 minutes, following a line break by [[Toby Flood]], who then drew two defenders towards him before passing the ball to Hamilton. The try was [[Conversion (rugby)|converted]] by Dupuy, and a successful Dupuy penalty kick on 24 minutes made the score 10–3 to Leicester. Cardiff hit back with three successful penalty kicks of their own, one from Blair and two from [[Leigh Halfpenny]], before Dupuy successfully kicked another penalty just before [[half-time]] to give Leicester a 13–12 lead at the break.<ref>{{cite news |url=http://www.rte.ie/sport/rugby/heineken-cup/2009/0503/248643-cardiff_leicester/ |title=Cardiff 26-26 Leicester (6-7 pens) |work=RTÉ Sport |date=3 May 2009 |accessdate=14 June 2015}}</ref>

After half-time, Leicester increased their lead with a converted try from [[Geordan Murphy]] after 45 minutes and two more penalties from Dupuy extended Leicester's lead further to 26–12 going into the last 20 minutes. Flood left the field due to an injury and was replaced by [[Aaron Mauger]].<ref name="BBCLiveText">{{cite news |url=http://news.bbc.co.uk/sport1/hi/rugby_union/8031625.stm |title=Blues v Leicester as it happened |work=BBC Sport |date=3 May 2009 |accessdate=14 June 2015 |archiveurl=https://web.archive.org/web/20090508002157/http://news.bbc.co.uk/sport1/hi/rugby_union/8031625.stm |archivedate=8 May 2009 |deadurl=no}}</ref> Leicester then had two players sent to the [[sin bin]] in quick succession. First, [[Craig Newby]] was shown a [[Penalty card#Yellow card|yellow card]] on 62 minutes for killing the ball during a Cardiff attack, and then Geordan Murphy was shown a yellow card on 68 minutes for what was adjudged to be a deliberate [[Knock-on (rugby union)|knock-on]] during another Cardiff attack.<ref name="BBCMatchReport"/> Leicester did not concede during Newby's absence, but shortly after his return and with Murphy still off the pitch in the sin bin, Cardiff's [[Jamie Roberts]] scored a try with Ben Blair successfully kicking the conversion from within five metres of the touchline. At the restart, Cardiff gathered the ball and Roberts made a break from within 22 metres of Cardiff's own goal line before passing to [[Tom James (rugby player)|Tom James]], who ran 60 metres to score a try in almost exactly the same place as Roberts. Blair again kicked the conversion from the touchline to tie the scores at 26–26 with three minutes remaining.<ref>{{cite news |url=https://www.theguardian.com/sport/2009/may/03/leicester-cardiff-blues-heineken-crane |title=Jordan Crane lifts Leicester past Cardiff Blues in Heineken Cup shoot-out |first=Paul |last=Rees |date=3 May 2009 |work=The Guardian |accessdate=14 June 2015}}</ref> With no further scores, the match went into [[extra time]].

==Extra time==
This was the second occasion a Heineken Cup semi-final match had gone to extra time – it had only previously been required in the match between Toulouse and [[CA Brive|Brive]] in [[1997–98 Heineken Cup|1998]].<ref name="BBCLiveText"/> Extra time consisted of two halves of 10 minutes each. A winner would be decided only if one team's score was higher than the other at the end of extra time, not by the "first to score" system used in some other sports. The rules also provided a tiebreaker if the scores were level after extra time; the team that had scored the most tries in the match would be the winner. In the event, neither team made any score during extra time, and both had scored the same number of tries during the match, which meant that the match would be decided by a penalty shoot-out, the first time such an occurrence had happened in a professional rugby match.<ref>{{cite news |url=https://www.theguardian.com/sport/2009/may/03/leicester-cardiff-blues-heineken-cup |title=Jordan Crane lifts Leicester past Cardiff Blues and into Heineken Cup final |date=3 May 2009 |work=The Guardian |first=Robert |last=Kitson |accessdate=14 June 2015}}</ref>{{efn|Some sources suggest this was the first senior level match to feature a penalty shoot-out, but the [[1983–84 French Rugby Union Championship|1984 French Rugby Championship]] final, which took place when rugby union was restricted to amateur players, had been decided by a penalty shoot-out.}}

In the final minute of extra time, Leicester medical staff indicated they needed to make a blood injury replacement for [[Dan Hipkiss]], and elected to bring Julien Dupuy, who had been substituted in the last 10 minutes of regular time, back onto the field as the replacement.<ref>{{cite news |first=Robert |last=Kitson |title=We can't turn a blind eye to gouging and other dirty tricks |url=https://www.theguardian.com/sport/blog/2009/may/05/heineken-cup-penalty-shootout-robert-kitson |work=The Guardian |date=5 May 2009 |accessdate=11 August 2015}}</ref>{{efn|Under the laws of the game in force at the time, a non-front row player substituted during a match cannot later be brought back into the game, except in the event they are temporarily replacing a player that is bleeding and requires treatment to control the bleeding and cover the wound.}} The match officials requested the Leicester physiotherapist demonstrate an open wound by wiping the blood from the cut, and satisfied the injury was legitimate, allowed the replacement.<ref name="walesonlineblood">{{cite news |title=Blues dismiss claims of Leicester blood bin row |url=http://www.walesonline.co.uk/sport/rugby/rugby-news/blues-dismiss-claims-leicester-blood-2089828 |work=Wales Online |publisher=Media Wales |date=24 August 2009 |first=Delme |last=Parfitt |accessdate=15 November 2015}}</ref> With the penalty shoot-out approaching, Cardiff decided to use a substitution to bring [[Ceri Sweeney]], an experienced goal-kicker, onto the field in place of Jamie Roberts.<ref name="BBCLiveText"/>

==Penalty shoot-out==
{{Quote box
|width=250px
|align=right
|quote=I've had a penalty shoot-out card for all these years, and it's come out of the jacket.
|source=— [[Miles Harrison]], [[Sky Sports]] commentator
}}
The rarity of a penalty shoot-out in rugby meant that no standard format existed and tournament organisers were left to devise their own rules on the format a shoot-out would take in the event it was required, such as the positions the kicks would be taken from, the type of kick used and the number and order of players who would participate.<ref>{{cite news |url=http://www.telegraph.co.uk/sport/rugbyunion/club/5273324/European-Rugby-Cup-to-review-unpopular-penalty-shoot-out-system.html |title=European Rugby Cup to review unpopular penalty shoot-out system |date=4 May 2009 |first=Brendan |last=Gallagher |work=The Telegraph |accessdate=14 June 2015}}</ref> In the case of the Heineken Cup, the rules set by organisers [[European Rugby Cup]] (ERC) prescribed a format closely resembling that used in [[association football|soccer]] matches.<ref>{{cite news |url=http://www1.skysports.com/rugby-union/live/match/34308/report |title=Tigers claim shoot-out glory |date=4 May 2009 |work=Sky Sports |accessdate=14 June 2015}}</ref> Each team would alternately take five place kicks, each from the same position on the 22-metre line, directly in front of the posts. Each kick had to be taken by a different player from the 15 players that were present on the pitch at the end of extra time. In the event the scores were level after the five kicks, "sudden death" would take place, with each team alternately taking a kick until a round occurred where one team was successful with their kick and the other team missed. Again, each kick was required to be taken by a different player, starting with players that had yet to take a kick in the first part of the shoot-out. If no winner had been decided after all 15 players had attempted a kick, the process would continue with each player eligible to take a kick again until all 15 players had made another attempt.<ref name="TelegraphOpinion">{{cite news |url=http://www.telegraph.co.uk/sport/rugbyunion/club/5269008/Leicesters-penalty-shoot-out-victory-a-poor-way-to-decide-a-superb-contest.html |title=Leicester's penalty shoot-out victory a poor way to decide a superb contest |date=4 May 2009 |work=The Telegraph |first=Brendan |last=Gallagher |accessdate=14 June 2015}}</ref>

Cardiff won the coin toss and elected to kick first.<ref>{{cite news |url=http://www.dailymail.co.uk/sport/rugbyunion/article-1177014/Cardiff-Blues-26pts-Leicester-26pts-Football-recruit-wins-rugby-8217-s-penalty-shootout.html |title=Cardiff Blues 26pts Leicester 26pts: Football recruit wins rugby’s first penalty shootout |date=4 May 2009 |work=Daily Mail |publisher=Associated Newspapers |accessdate=14 June 2015}}</ref> Ben Blair and Julien Dupuy, the goal-kickers used during the match were the first to attempt kicks for their respective sides, were both successful. The teams then nominated other players on their team who had experience at goal-kicking, both teams being successful with their second and third kicks. Cardiff's fourth player Ceri Sweeney, was also an experienced goal-kicker and was successful; however, Leicester selected [[Johne Murphy]], who was not a regular kicker, and he missed to make the score 4–3 after four of the five rounds. This meant if Cardiff were successful with their next kick, they would have an unassailable lead and would win the match. With no more experienced goal kickers to choose from, winger Tom James stepped up to take the kick, but missed, allowing Leicester the chance to draw level. Scott Hamilton scored with his kick to bring the scores to 4–4 after five kicks and take the shoot-out to sudden death. The teams were both successful with their next two kicks, including Craig Newby for Leicester who was the first [[Forward (rugby union)|forward]] to take a kick in the shoot-out. With all seven [[Back (rugby union)|backs]] having already attempted a kick, Cardiff were required to select a forward player for their eighth kick, and chose flanker [[Martyn Williams]]. He missed, giving Leicester the chance to win the match if they were successful with their eighth kick. Number 8 [[Jordan Crane (rugby union)|Jordan Crane]] was selected for the attempt and was successful, resulting in Leicester winning the shoot-out 7–6 and advancing to the [[2009 Heineken Cup Final]].<ref name="TelegraphOpinion"/><ref name="BBCLiveText"/>

==Match details==
{{rugbybox
|date=3 May 2009
|time=15:00 [[British Summer Time|BST]]
|home=[[Cardiff Blues]] {{flagicon|WAL}}
|score=26–26 ([[Extra time|a.e.t.]])
|report=[https://web.archive.org/web/20090505115942/http://www.ercrugby.com/eng/12_12558.php Report]
|away={{flagicon|ENG}} [[Leicester Tigers]] 
|try1=[[Jamie Roberts|Roberts]] 73' {{Abbr|c|Converted}}<br />[[Tom James (rugby player)|James]] 74' {{Abbr|c|Converted}}
|con1=[[Ben Blair|Blair]] (2/2)
|pen1=[[Ben Blair|Blair]] (2/2) 14', 33'<br />[[Leigh Halfpenny|Halfpenny]] (2/2) 27', 35'
|try2=[[Scott Hamilton (rugby union)|Hamilton]] 21' {{Abbr|c|Converted}}<br />[[Geordan Murphy|G. Murphy]] 45' {{Abbr|c|Converted}}
|con2=[[Julien Dupuy|Dupuy]] (2/2)
|pen2=[[Julien Dupuy|Dupuy]] (4/7) 24', 38', 54', 56'
|stadium=[[Millennium Stadium]], [[Cardiff]]
|attendance=44,212
|referee=[[Alain Rolland]] ([[Irish Rugby Football Union|Ireland]])
|penalties1=[[Ben Blair|Blair]] {{tick|13}}<br />[[Nick Robinson (rugby union)|N. Robinson]] {{tick|13}}<br />[[Leigh Halfpenny|Halfpenny]] {{tick|13}}<br />[[Ceri Sweeney|Sweeney]] {{tick|13}}<br />[[Tom James (rugby player)|James]] {{cross|13}}<br />[[Tom Shanklin|Shanklin]] {{tick|13}}<br />[[Richie Rees|Rees]] {{tick|13}}<br />[[Martyn Williams|M. Williams]] {{cross|13}}
|penaltyscore=6–7
|penalties2={{tick|13}} [[Julien Dupuy|Dupuy]]<br />{{tick|13}} [[Sam Vesty|Vesty]]<br />{{tick|13}} [[Geordan Murphy|G. Murphy]]<br />{{cross|13}} [[Johne Murphy|J. Murphy]]<br />{{tick|13}} [[Scott Hamilton (rugby union)|Hamilton]]<br />{{tick|13}} [[Aaron Mauger|Mauger]]<br />{{tick|13}} [[Craig Newby|Newby]]<br />{{tick|13}} [[Jordan Crane (rugby union)|Crane]]
}}

{| width="100%"
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0"
|-
!width="25"| !!width="25"|
|-
|[[Full back (rugby union)|{{Abbr|FB|Full back}}]] ||'''15'''||{{flagicon|NZL}} [[Ben Blair]]
|-
|[[Wing (rugby union)|{{Abbr|RW|Right wing}}]] ||'''14'''||{{flagicon|WAL}} [[Leigh Halfpenny]]
|-
|[[Outside centre (rugby union)|{{Abbr|OC|Outside centre}}]] ||'''13'''||{{flagicon|WAL}} [[Tom Shanklin]]
|-
|[[Inside centre (rugby union)|{{Abbr|IC|Inside centre}}]] ||'''12'''||{{flagicon|WAL}} [[Jamie Roberts]] || || {{suboff|98}}
|-
|[[Wing (rugby union)|{{Abbr|LW|Left wing}}]] ||'''11'''||{{flagicon|WAL}} [[Tom James (rugby player)|Tom James]]
|-
|[[Fly half (rugby union)|{{Abbr|FH|Fly half}}]] ||'''10'''||{{flagicon|WAL}} [[Nicky Robinson (rugby union)|Nicky Robinson]]
|-
|[[Scrum half (rugby union)|{{Abbr|SH|Scrum half}}]] ||'''9''' ||{{flagicon|WAL}} [[Richie Rees]]
|-
|[[Number eight (rugby union)|{{Abbr|N8|Number 8}}]] ||'''8''' ||{{flagicon|NZL}} [[Xavier Rush]]
|-
|[[Flanker (rugby union)|{{Abbr|OF|Openside flanker}}]] ||'''7''' ||{{flagicon|WAL}} [[Martyn Williams]]
|-
|[[Flanker (rugby union)|{{Abbr|BF|Blindside flanker}}]] ||'''6''' ||{{flagicon|TON}} [[Maama Molitika]] || || {{suboff|59}}
|-
|[[Lock (Rugby Union)|{{Abbr|RL|Right lock}}]] ||'''5''' ||{{flagicon|NZL}} [[Paul Tito]] ([[Captain (sports)|c]]) || || {{suboff|9}}
|-
|[[Lock (Rugby Union)|{{Abbr|LL|Left lock}}]]  ||'''4''' ||{{flagicon|WAL}} [[Bradley Davies]]
|-
|[[Prop (rugby union)|{{Abbr|TP|Tighthead prop}}]] ||'''3''' ||{{flagicon|TON}} [[Taufaʻao Filise]] || || {{suboff|73}}
|-
|[[Hooker (rugby union)|{{Abbr|HK|Hooker}}]] ||'''2''' ||{{flagicon|WAL}} [[Gareth Williams (rugby union, born 1978)|Gareth Williams]]
|-
|[[Prop (rugby union)|{{Abbr|LP|Loosehead prop}}]] ||'''1''' ||{{flagicon|WAL}} [[Gethin Jenkins]]
|-
|colspan=3|'''Substitutions:'''
|-
|[[Prop (rugby union)|{{Abbr|LP|Loosehead prop}}]] ||'''16'''||{{flagicon|WAL}} [[John Yapp]] || || {{subon|73}}
|-
|[[Hooker (rugby union)|{{Abbr|HK|Hooker}}]] ||'''17'''||{{flagicon|WAL}} [[T Rhys Thomas]]
|-
|[[Lock (Rugby Union)|{{Abbr|LK|Lock}}]] ||'''18'''||{{flagicon|WAL}} [[Deiniol Jones]] || || {{subon|9}}
|-
|[[Flanker (rugby union)|{{Abbr|FL|Flanker}}]] ||'''19'''||{{flagicon|WAL}} [[Andy Powell (rugby)|Andy Powell]] || || {{subon|59}}
|-
|[[Scrum half (rugby union)|{{Abbr|SH|Scrum half}}]] ||'''20'''||{{flagicon|WAL}} [[Darren Allinson]]
|-
|[[Fly half (rugby union)|{{Abbr|FH|Fly half}}]] ||'''21'''||{{flagicon|WAL}} [[Ceri Sweeney]] || || {{subon|98}}
|-
|[[Centre (rugby union)|{{Abbr|CE|Centre}}]] ||'''22'''||{{flagicon|WAL}} [[Gareth Thomas (rugby)|Gareth Thomas]]
|-
|-
|colspan="3"|'''Coach:'''
|-
|colspan="4"|{{flagicon|WAL}} [[Dai Young]]
|}
|valign="top"|[[File:Cardiff Blues vs Leicester Tigers 2009-05-03.svg|350px|alt=Diagram showing the team line-ups]]
|valign="top" width="50%"|
{| style="font-size: 90%" cellspacing="0" cellpadding="0" align="center"
|-
!width="25"| !!width="25"|
|-
|[[Full back (rugby union)|{{Abbr|FB|Full back}}]] ||'''15'''||{{flagicon|IRE|rugby union}} [[Geordan Murphy]] ([[Captain (sports)|c]]) || {{sin bin|66|76}}
|-
|[[Wing (rugby union)|{{Abbr|RW|Right wing}}]] ||'''14'''||{{flagicon|NZL}} [[Scott Hamilton (rugby union)|Scott Hamilton]]
|-
|[[Outside centre (rugby union)|{{Abbr|OC|Outside centre}}]]  ||'''13'''||{{flagicon|ENG}} [[Dan Hipkiss]] || || || {{suboff|98}}
|-
|[[Inside centre (rugby union)|{{Abbr|IC|Inside centre}}]]  ||'''12'''||{{flagicon|ENG}} [[Sam Vesty]]
|-
|[[Wing (rugby union)|{{Abbr|LW|Left wing}}]]  ||'''11'''||{{flagicon|IRE|rugby union}} [[Johne Murphy]] || ||colspan="2"| {{blood bin|63|73}}
|-
|[[Fly half (rugby union)|{{Abbr|FH|Fly half}}]] ||'''10'''||{{flagicon|ENG}} [[Toby Flood]] || || {{suboff|60}}
|-
|[[Scrum half (rugby union)|{{Abbr|SH|Scrum half}}]] ||'''9''' ||{{flagicon|FRA}} [[Julien Dupuy]] || || {{suboff|74}} || {{subon|98}}
|-
|[[Number eight (rugby union)|{{Abbr|N8|Number 8}}]] ||'''8''' ||{{flagicon|ENG}} [[Jordan Crane (rugby union)|Jordan Crane]]
|-
|[[Flanker (rugby union)|{{Abbr|OF|Openside flanker}}]] ||'''7''' ||{{flagicon|ENG}} [[Ben Woods]] || || {{suboff|73}}
|-
|[[Flanker (rugby union)|{{Abbr|BF|Blindside flanker}}]] ||'''6''' ||{{flagicon|NZL}} [[Craig Newby]] || {{sin bin|61|71}}
|-
|[[Lock (Rugby Union)|{{Abbr|RL|Right lock}}]] ||'''5''' ||{{flagicon|ENG}} [[Ben Kay]] || || {{suboff|92}}
|-
|[[Lock (Rugby Union)|{{Abbr|LL|Left lock}}]] ||'''4''' ||{{flagicon|ENG}} [[Tom Croft]]
|-
|[[Prop (rugby union)|{{Abbr|TP|Tighthead prop}}]] ||'''3''' ||{{flagicon|ITA}} [[Martin Castrogiovanni]] || || {{suboff|50}} || {{subon|91}}
|-
|[[Hooker (rugby union)|{{Abbr|HK|Hooker}}]] ||'''2''' ||{{flagicon|ENG}} [[George Chuter]] || || {{suboff|59}}
|-
|[[Prop (rugby union)|{{Abbr|LP|Loosehead prop}}]] ||'''1''' ||{{flagicon|ARG}} [[Marcos Ayerza]] || || || {{suboff|91}}
|-
|colspan=3|'''Substitutions:'''
|-
|[[Hooker (rugby union)|{{Abbr|HK|Hooker}}]] ||'''16'''||{{flagicon|FRA}} [[Benjamin Kayser]] || || {{subon|59}}
|-
|[[Prop (rugby union)|{{Abbr|PR|Prop}}]] ||'''17'''||{{flagicon|ENG}} [[Julian White]] || || {{subon|50}}
|-
|[[Lock (Rugby Union)|{{Abbr|LK|Lock}}]] ||'''18'''||{{flagicon|RSA}} [[Marco Wentzel]] || || {{subon|92}}
|-
|[[Flanker (rugby union)|{{Abbr|FL|Flanker}}]] ||'''19'''||{{flagicon|ENG}} [[Lewis Moody]] || || {{subon|73}}
|-
|[[Scrum half (rugby union)|{{Abbr|SH|Scrum half}}]] ||'''20'''||{{flagicon|ENG}} [[Harry Ellis]] || || {{subon|74}}
|-
|[[Centre (rugby union)|{{Abbr|CE|Centre}}]] ||'''21'''||{{flagicon|NZL}} [[Aaron Mauger]] || || {{subon|60}}
|-
|[[Centre (rugby union)|{{Abbr|CE|Centre}}]] ||'''22'''||{{flagicon|ENG}} [[Matt Smith (rugby union)|Matt Smith]] || || {{subon|63}} || {{suboff|73}}
|-
|colspan="3"|'''Coach:'''
|-
|colspan="4"|{{flagicon|ENG}} [[Richard Cockerill]]
|}
|}

==Post-match reaction==
[[File:Martyn Williams.jpg|thumb|[[Martyn Williams]] missed the decisive penalty. Several writers criticised the use of a penalty shootout, as few players are skilled in kicking.]]
Martyn Williams remained in the dressing room for 90 minutes after the game, and when he emerged opted not to speak to the media.<ref name="GuardianOpinion">{{cite news |url=https://www.theguardian.com/sport/blog/2009/may/04/cardiff-leicester-heineken-cup |title=Cardiff sing the blues after Martyn Williams ends up loser in lottery |first=Paul |last=Rees |date=4 May 2009 |work=The Guardian |accessdate=14 June 2015}}</ref> He later did give his reaction, expressing his opinion that the format was a bit of a lottery and observed "somebody was going to miss – I've just got to live with the fact that it was me."<ref>{{cite news |url=https://www.theguardian.com/sport/2009/may/05/martyn-williams-heineken-cup-penalty |title=Martyn Williams says Heineken Cup penalty miss was 'a surreal feeling' |date=5 May 2009 |work=The Guardian |accessdate=14 June 2015}}</ref> He also credited several of the victorious Leicester players who had commiserated with him after the game and others who had sent messages of support to him. Jordan Crane said after the match that he had felt calm prior to taking his kick, as Williams' miss meant that in the event Crane also missed, the scores would remain level and sudden death would continue. Tom James who had missed a kick earlier in the shoot-out which would have won the match for Cardiff said that "gutted is the only word that properly describes how I am feeling".<ref name="GuardianOpinion"/>

A number of outlets expressed disappointment at the penalty shoot-out, in particular the way that Martin Williams, who as a flanker who would not ordinarily take a kick at goal, had been to "blame" for Cardiff's loss. ''[[The Guardian]]'''s Paul Rees expressed the opinion that asking forwards to take kicks at goal "is demanding something not only unnatural but untried and it turned a rousing semi-final into a pantomime, an unnecessarily demeaning way of settling the issue."<ref name="GuardianOpinion"/> Brendan Gallagher writing in ''[[The Daily Telegraph]]'' noted that only three or four players on a rugby team would kick the ball during a match in a meaningful way, and said it was "ludicrous that a game should be decided by emphasising that particular skill". He also questioned what would have happened in the event the shoot-out continued and the game been decided between props [[Martin Castrogiovanni]] and [[Gethin Jenkins]], both considered amongst the finest players in their position but never ordinarily being required to kick the ball at goal.<ref name="TelegraphOpinion"/> Chris Hewett expressed similar views in ''[[The Independent]]'', observing that Martyn Williams "lost the game by failing to perform a task outside the skill-set of the majority of players" and commenting that "short of asking a cat to bark, it is hard to imagine anything more preposterous."<ref>{{cite news |url=http://www.independent.co.uk/sport/rugby/rugby-union/club-rugby/shootouts-have-no-place-in-rugby-1679033.html |title=Shoot-outs have no place in rugby |first=Chris |last=Hewett |date=5 May 2009 |work=The Independent |accessdate=14 June 2015}}</ref>

In light of the criticism, tournament organisers ERC agreed to review the penalty shoot-out system, although they confirmed that they had already considered and dismissed some other possible ways of deciding matches, including replaying the game, having a 'next try wins' rule in extra time, reducing the number of players on the pitch periodically in extra time to create more space and deciding the game on a coin toss.<ref>{{cite news |url=https://www.theguardian.com/sport/2009/may/05/cardiff-leicester-penalty-shootout-heineken |title=ERC to review penalty shootout system |date=5 May 2009 |work=The Guardian |accessdate=14 June 2015}}</ref> The format for the penalty shoot-out was changed prior to the following season's tournament, removing the requirement for all players on the field (including forwards) to participate,<ref>{{cite web |url=https://www.theguardian.com/sport/blog/2009/dec/06/martyn-williams-heineken-cup |title=Toulouse test means it is time to move on from shoot-out misery |work=The Guardian |first=Martyn |last=Williams |date=6 December 2009 |accessdate=14 June 2015}}</ref> however for the remainder of its existence, the Heineken Cup retained the penalty shoot-out as the final system for deciding a winner if required.<ref>{{cite web |title=Summary of Key Heineken Cup Rules |url=http://archive.ercrugby.com/heinekencup/rules.php |work=ercrugby.com |publisher=European Rugby Cup |accessdate=14 June 2015}}</ref> No other Heineken Cup match needed to use it.

In August 2009, [[Martin Offiah]], who was working as a pundit for broadcaster [[Sky Sports]] during the match, spoke to the [[News of the World]], stating that he believed the blood replacement in the final minute of extra time where Julien Dupuy returned to the field "looked very suspicious and, in light of [[Bloodgate|what happened in the previous round with Harlequins]], I think Hipkiss and Leicester should be called to question."<ref>{{cite news |url=http://www.newsoftheworld.co.uk/sport/466906/Bloodgate-II-scandal-Martin-Offiah-demands-an-investigation-into-Heineken-Cup-claims.html |title=Dan Hipkiss In Bloodgate Row |date=22 August 2009 |first=Cliff |last=Hayes |work=News of the World |publisher=News Group|archiveurl=https://web.archive.org/web/20090827001647/http://www.newsoftheworld.co.uk/sport/466906/Bloodgate-II-scandal-Martin-Offiah-demands-an-investigation-into-Heineken-Cup-claims.html |archivedate=27 August 2009 |deadurl=yes}}</ref> Leicester coach [[Richard Cockerill]] responded that the replacement was within the laws of the game and that the club had done nothing illegal. ERC confirmed that the match commissioner had spoken to the referee regarding the incident and on the basis that the referee was satisfied, no further action was required. Cardiff also confirmed that they were satisfied that no wrongdoing had taken place and would not seek any further action.<ref name="walesonlineblood" />

==Notes==
{{notelist}}

==References==
{{reflist|30em}}

{{DEFAULTSORT:Cardiff Blues vs Leicester Tigers (2008-09 Heineken Cup)}}
[[Category:2008–09 Heineken Cup]]
[[Category:Cardiff Blues]]
[[Category:Leicester Tigers matches]]
[[Category:21st century in Cardiff]]