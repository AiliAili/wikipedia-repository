{{Infobox Journal
| title = Photochemical & Photobiological Sciences
| cover = [[File:CoverIssuePPS.jpg]]
| discipline = [[Photochemistry]], [[Photobiology]]
| abbreviation = Photochem. Photobiol. Sci.
| impact = 2.235
| impact-year = 2015
| editor = Dario Bassani, Santi Nonell
| website = http://www.rsc.org/Publishing/Journals/pp/Index.asp
| link1 = http://www.rsc.org/Publishing/Journals/pp/Article.asp?Type=CurrentIssue
| link1-name = Online access
| publisher = [[Royal Society of Chemistry]]
| country = United Kingdom
| history = 2002-present
| ISSN = 1474-905X
| eISSN = 1474-9092
| CODEN = PPSHCB
| LCCN = 2002257028
| OCLC = 49233320
}}
'''''Photochemical & Photobiological Sciences''''' is a monthly [[peer-reviewed]] [[scientific journal]] covering all areas of [[photochemistry]] and [[photobiology]]. It is published monthly by the [[Royal Society of Chemistry]] and is the official journal of the [[European Photochemistry Association]], [[European Society for Photobiology]], [[Asia and Oceania Society for Photobiology]], and the [[Korean Society of Photoscience]]. The [[editors-in-chief]] are Dario Bassani and Santi Nonell. According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 2.235.<ref>[[Journal Citation Reports|Journal Citation Reports, 2014]]</ref>

== Owner societies ==
The journal is co-owned by the European Photochemistry Association and the European Society for Photobiology, and is affiliated with the Asia and Oceania Society for Photobiology and the Korean Society of Photoscience.

== Abstracting and indexing ==
The journal is abstracted and indexed in:<ref name=masterList>{{Cite web
 | title =Master Journal search
 | work =Coverage
 | publisher =[[Thomson Reuters]]
 | date =
 | url =http://science.thomsonreuters.com/cgi-bin/jrnlst/jlresults.cgi?PC=MASTER&ISSN= 1474-905X
 | format =
 | accessdate =2011-01-29}}</ref><ref name=cassi>{{Cite web
 | title =''Photochemical & Photobiological Sciences''
 | work =Chemical Abstracts Service Source Index (CASSI) (Displaying Record for Publication)
 | publisher =[[American Chemical Society]]
 | date =
 | url = http://cassi.cas.org/publication.jsp?P=LglBQf5Q2NQyz133K_ll3zLPXfcr-WXfeIkmQnC5L8FTugx6LjttiTLPXfcr-WXfimSBIkq8XcUjhmk0WtYxmzLPXfcr-WXfi338yjs0CC8KAbXoEpU6xg
 | format =
 | accessdate =2011-01-29}}</ref>

* [[Chemical Abstracts Service]]
* [[PubMed]]/[[MEDLINE]]
* [[Science Citation Index]]
* [[Scopus]]

== See also ==
* [[Chemical biology]]

== References ==
{{reflist}}

== External links ==
* {{Official website|http://www.rsc.org/Publishing/Journals/pp/index.asp}}
* [http://www.photochemistry.eu/ European Photochemistry Association]
* [http://www.esp-photobiology.it/ European Society for Photobiology]
* [http://aosp.umin.jp/ Asia and Oceania Society for Photobiology]
* [http://photos.or.kr/2010/ Korean Society of Photoscience]


{{Royal Society of Chemistry|state=collapsed}}

[[Category:Chemistry journals]]
[[Category:Biology journals]]
[[Category:Royal Society of Chemistry academic journals]]
[[Category:Publications established in 2002]]
[[Category:Monthly journals]]
[[Category:English-language journals]]