{{about||the 1955 Italian comedy film|A Hero of Our Times|the album by Satanic Surfers|Hero of Our Time}}
{{infobox book
| name          = A Hero of Our Time
| title_orig    = Герой нашего времени <!-- translit: 'Geroy nashevo vremeni' -- please type this _correctly_, I don't know how -->
| translator    = 
| image         = Geroy nashego vremeni.png
| author        = [[Mikhail Lermontov]]
| illustrator   = 
| cover_artist  = 
| country       = Russia
| language      = [[Russian language|Russian]]
| series        = 
| genre         = 
| publisher     = Iliya Glazunov & Co ({{lang-ru|Типография Ильи Глазунова и Ко}})
| release_date  = 1840
| media_type    = 
| pages         = 
| isbn          = 
| preceded_by   = 
| followed_by   = 
}}
'''''A Hero of Our Time''''' ({{lang-ru|Герой нашего времени}}, ''Geroy nashego vremeni'') is a [[novel]] by [[Mikhail Lermontov]], written in 1839, published in 1840, and revised in 1841.

It is an example of the [[superfluous man]] novel, noted for its compelling [[Byronic hero]] (or [[antihero]]) Pechorin and for the beautiful descriptions of the [[Caucasus]].  There are several English translations, including one by [[Vladimir Nabokov]] and [[Dmitri Nabokov]] in 1958.

==Plot structure==
The book is divided into five short stories or [[novella]]s, with an authorial preface added in the second edition. There are three major narrators.  The first is a young, unnamed officer in the [[Imperial Russian army|Russian army]] travelling through the [[Caucasus mountains]]. He is documenting his travels for publication later. Almost as soon as the story begins, he meets Captain Maxim Maximych, who is significantly older and has been stationed in the Caucasus for a long time. He is therefore wise to the lifestyle of Russian soldiers in this region, and  immediately demonstrates this to the narrator through his interactions with the local [[Ossetian people|Ossetian]] tribesman.

Maxim Maximych serves as the second narrator, relaying to his traveling companion stories of his interactions with Grigory Alexandrovich Pechorin, the main character of the story and the ultimate Byronic hero. Maxim Maximych was stationed in the Caucasus with Pechorin for some time, though when and for how long is not specified.  Ultimately, Maxim Maximych gives Pechorin's diaries to the unnamed narrator. Pechorin seemingly abandoned them when he was discharged from his post, and the old Captain has been carrying them around since.

The third narrator is Pechorin himself. However, unlike the other two, he is not actually a character immediately in the story. Instead, he narrates through his diaries, which were published along with the unnamed narrator's travel notes after Pechorin's death. The diaries, however, seem to switch at least once from the past tense (as a diary would be written) to the present tense. Pechorin, the "hero of our time" is shown to be alternately impulsive and calculating through Maxim Maximych's stories.  He is shown to be calculating, manipulative, emotionally unavailable and arrogant through his own recollections. However he is both sensitive and cynical as well as intelligent, a fact he is all too aware of.

In the longest novella, ''Princess Mary'', Pechorin flirts with the [[Princess]] of the title, while conducting an affair with his ex-lover Vera, and kills his friend Grushnitsky (of whom he is secretly contemptuous) in a [[duel]] in which the participants stand in turn on the edge of a cliff so that the loser's death can be explained as an accidental fall. Eventually he rejects one woman only to be abandoned by the other.

The preface explains the author's idea of his character: "A Hero of Our Time, my dear readers, is indeed a portrait, but not of one man. It is a portrait built up of all our generation's vices in full bloom. You will again tell me that a human being cannot be so wicked, and I will reply that if you can believe in the existence of all the villains of tragedy and romance, why wouldn't you believe that there was a Pechorin? If you could admire far more terrifying and repulsive types, why aren't you more merciful to this character, even if it is fictitious? Isn't it because there's more truth in it than you might wish?"

==Grigory Alexandrovich Pechorin==
Pechorin is the embodiment of the [[Byronic hero]].  Byron’s works were of international repute and Lermontov mentions his name several times throughout the novel.  According to the Byronic tradition, Pechorin is a character of contradiction.  He is both sensitive and [[cynical]].  He is possessed of extreme arrogance, yet has a deep insight into his own character and epitomizes the [[Melancholia (temperament)|melancholy]] of the [[romantic hero]] who broods on the futility of existence and the certainty of death.  Pechorin’s whole philosophy concerning existence is oriented towards the nihilistic, creating in him somewhat of a distanced, alienated personality.* The name Pechorin is drawn from that of the [[Pechora River]], in the far north, as a homage to [[Aleksandr Pushkin]]'s [[Eugene Onegin]], named after the [[Onega River]].<ref>{{cite book | last = Murray | first = Christopher | coauthors = | title =Encyclopedia of the Romantic Era, 1760–1850 | page = 498 | edition =   | year = 2004  | publisher = Taylor & Francis | location = New York | url = https://books.google.com/books?id=bXnMs-YDEF4C&pg=PA498&lpg=PA498&dq=pechora+river+lermontov| isbn = 1-57958-423-3}}</ref>

Pechorin treats women as an incentive for endless conquests and does not consider them worthy of any particular respect.  He considers women such as Princess Mary to be little more than pawns in his games of romantic conquest, which in effect hold no meaning in his listless pursuit of pleasure.  This is shown in his comment on Princess Mary: “I often wonder why I’m trying so hard to win the love of a girl I have no desire to seduce and whom I’d never marry.”

The only contradiction in Pechorin’s attitude to women are his genuine feelings for Vera, who loves him despite, and perhaps due to, all his faults. At the end of “Princess Mary” one is presented with a moment of hope as Pechorin gallops after Vera.  The reader almost assumes that a meaning to his existence may be attained and that Pechorin can finally realize that true feelings are possible.  Yet a lifetime of superficiality and cynicism cannot be so easily eradicated and when fate intervenes and Pechorin’s horse collapses, he undertakes no further effort to reach his one hope of redemption: “I saw how futile and senseless it was to pursue lost happiness.  What more did I want?  To see her again?  For what?”

Pechorin's chronologically last adventure, was first described in the book, showing the events that explain his upcoming fall into depression and retreat from society, resulting in his self-predicted death. The narrator is Maxim Maximytch telling the story of a beautiful Circassian princess 'Bela', whom Azamat abducts for Pechorin in exchange for Kazbich's horse. Maxim describes Pechorin's exemplary persistence to convince Bela to give herself sexually to him, in which she with time reciprocates. After living with Bela for some time, Pechorin starts explicating his need for freedom, which Bela starts noticing, fearing he might leave her. Though Bela is completely devoted to Pechorin, she says she's not his slave, rather a daughter of a Circassian tribal Chieftain, also showing the intention of leaving if he 'doesn't love her'. Maxim's sympathy for Bela makes him question Pechorin's intentions. Pechorin admits he loves her and is ready to die for her, but 'he has a restless fancy and insatiable heart, and that his life is emptier day by day'. He thinks his only remedy is to travel, to keep his spirit alive.

[[File:Mikhail Vrubel Duel Pechorin vs Grushnizky.jpg|thumb|''The duel of Pechorin and Grushnitsky'' by [[Mikhail Vrubel]]]]
However Pechorin's behavior soon changes after Bela gets kidnapped by his enemy Kazbich, and becomes mortally wounded. After 2 days of suffering in delirium Bela spoke of her inner fears and her feelings for Pechorin, who listened without once leaving her side. After her death, Pechorin becomes physically ill, loses weight and becomes unsociable. After meeting with Maxim again, he acts coldly and antisocial, explicating deep depression and disinterest in interaction. He soon dies on his way back from Persia, admitting before that he is sure to never return.

Pechorin described his own personality as self-destructive, admitting he himself doesn't understand his purpose in the world of men. His boredom with life, feeling of emptiness, forces him to indulge in all possible pleasures and experiences, which soon, cause the downfall of those closest to him. He starts to realize this with Vera and Grushnitsky, while the tragedy with Bela soon leads to his complete emotional collapse.

His crushed spirit after this and after the duel with Grushnitsky can be interpreted that he is not the detached character that he makes himself out to be. Rather, it shows that he suffers from his actions. Yet many of his actions are described both by himself and appear to the reader to be arbitrary. Yet this is strange as Pechorin's intelligence is very high (typical of a Byronic hero). Pechorin's explanation as to why his actions are arbitrary can be found in the last chapter where he speculates about fate. He sees his arbitrary behaviour not as being a subconscious reflex to past moments in his life but rather as fate. Pechorin grows dissatisfied with his life as each of his arbitrary actions lead him through more emotional suffering which he represses from the view of others.

==Cultural references==
[[Albert Camus]]' novel ''[[The Fall (Albert Camus novel)|The Fall]]'' begins with an excerpt from Lermontov's foreword to ''A Hero of Our Time'': "Some were dreadfully insulted, and quite seriously, to have held up as a model such an immoral character as ''A Hero of Our Time''; others shrewdly noticed that the author had portrayed himself and his acquaintances. ''A Hero of Our Time'', gentlemen, is in fact a portrait, but not of an individual; it is the aggregate of the vices of our whole generation in their fullest expression."

In [[Ian Fleming]]'s ''[[From Russia, with Love (novel)|From Russia with Love]]'' the plot revolves upon Soviet agent Tatiana Romanova feigning an infatuation with MI6's James Bond and offering to defect to the West provided he'll be sent to pick her up in Istanbul, Turkey. The Soviets elaborate a complex backstory about how she spotted the file about the English spy during her clerical work at [[SMERSH]] headquarters and became smitten with him, making her state that his picture made her think of Lermontov's Pechorin. The fact that Pechorin was all but a 'hero' or even a positive character at all in Lermontov's narration stands to indicate Fleming's wry self-deprecating wit about his most famous creation; the irony is lost, however, on western readers not familiar with Lermontov's work.

In [[Ingmar Bergman]]'s film ''[[The Silence (1963 film)|The Silence]]'', the young son is seen reading the book in bed. In the opening sequence of Bergman's next film ''[[Persona (film)|Persona]]'' the same child actor is seen waking in what appears to be a mortuary and reaching for the same book.

[[Claude Sautet]]'s film ''[[A Heart in Winter]]'' (''[[Un Coeur en Hiver]]'') was said to be based on "his memories of" the Princess Mary section. The relationship with Lermontov's work is quite loose – the film takes place in contemporary Paris, where a young violin repairer (played by Daniel Auteuil) seeks to seduce his business partner’s girlfriend, a gifted violinist named Camille, into falling for his carefully contrived charms. He does this purely for the satisfaction of gaining control of her emotionally, while never loving her sincerely. He is a modern-day Pechorin.

==Quotations==
{{move section to wikiquote}}
*"My whole life has been merely a succession of miserable and unsuccessful denials of feelings or reason."
*"...I am not capable of close friendship: of two close friends, one is always the slave of the other, although frequently neither of them will admit it.  I cannot be a slave, and to command in such circumstances is a tiresome business, because one must deceive at the same time."
*"Afraid of decision, I buried my finer feelings in the depths of my heart and they died there."
*"It is difficult to convince women of something; one must lead them to believe that they have convinced themselves."
*"What of it?  If I die, I die.  It will be no great loss to the world, and I am thoroughly bored with life.  I am like a man yawning at a ball; the only reason he does not go home to bed is that his carriage has not arrived yet."
*"When I think of imminent and possible death, I think only of myself; some do not even do that. Friends, who will forget me tomorrow, or, worse still, who will weave God knows what fantastic yarns about me; and women, who in the embrace of another man will laugh at me in order that he might not be jealous of the departed—what do I care for them?"
*"Women! Women! Who will understand them? Their smiles contradict their glances, their words promise and lure, while the sound of their voices drives us away. One minute they comprehend and divine our most secret thoughts, and the next, they do not understand the clearest hints."
*"There are two men within me – one lives in the full sense of the word, the other reflects and judges him. In an hour's time the first may be leaving you and the world for ever, and the second? ... the second? ..."
*"To cause another person suffering or joy, having no right to so—isn't that the sweetest food of our pride? What is happiness but gratified pride?"
*"I'll hazard my life, even my honor, twenty times, but I will not sell my freedom.  Why do I value it so much? What am I preparing myself for? What do I expect from the future? in fact, nothing at all."
* '''Grushnitski''' (to Pechorin): "Mon cher, je haïs les hommes pour ne pas les mépriser car autrement la vie serait une farce trop dégoûtante." ("My friend, I hate people to avoid despising them because otherwise, life would become too disgusting a [[farce]].")
* '''Pechorin''' (replying to Grushnitski): "Mon cher, je méprise les femmes pour ne pas les aimer car autrement la vie serait un mélodrame trop ridicule" ("My friend, I despise women to avoid loving them because otherwise, life would become too ridiculous a [[melodrama]].")
*"Passions are merely ideas in their initial stage."
*"I was prepared to love the whole world . . . I learned to hate."
*"Whether I am a fool or a villain I know not; but this is certain, I am also most deserving of pity – perhaps more so than she. My soul has been spoiled by the world, my imagination is unquiet, my heart insatiate. To me everything is of little moment. I have become as easily accustomed to grief as to joy, and my life grows emptier day by day."
*"That is just like human beings! They are all alike; though fully aware in advance of all the evil aspects of a deed, they aid and abet and even give their approbation to it when they see there is no other way out—and then they wash their hands of it and turn away with disapproval from him who dared assume the full burden of responsibility. They are all alike, even the kindest and wisest of them!"
*"Women love only the men they don't know."

==Stage adaptation==
In 2011 Alex Mcsweeney adapted the novel into an English-language playscript. Previewed at the International Youth Arts Festival in Kingston upon Thames, Surrey, UK in July, it subsequently premiered in August of the same year at Zoo Venues in the Edinburgh Fringe Festival. Critics received it positively, generally giving 4- and 5-star reviews.

In 2014, German stage director Kateryna Sokolova adapted the novel focusing on its longest novella, ''Princess Mary''. The play, directed by Kateryna Sokolova, premiered at the Schauspielhaus Zürich on 28 May.<ref>{{Cite news|url=http://www.katerynasokolova.com/en/portfolio/a-hero-of-our-time/|title=A Hero of Our Time|work=Kateryna Sokolova|access-date=2017-04-07|language=en-GB}}</ref><ref>{{Cite web|url=http://www.schauspielhaus.ch/de/play/421-Ein-Held-unserer-Zeit|title=Ein Held unserer Zeit {{!}} Schauspielhaus Zürich|website=www.schauspielhaus.ch|access-date=2017-04-07}}</ref> The production received universal acclaim<ref>{{Cite news|url=https://www.nzz.ch/zuerich/zuercher_kultur/nihilismus-und-ehre-1.18312035|title=Lermontow-Roman in der Schauspielhaus-Kammer: Nihilismus und Ehre|last=Steiger|first=Claudio|date=2014-05-30|work=Neue Zürcher Zeitung|access-date=2017-04-07|language=de-CH|issn=0376-6829}}</ref><ref name=":0">{{Cite news|url=http://bazonline.ch/kultur/theater/Kurz--kritisch-im-Mai/story/12843804|title=Kurz & kritisch im Mai|date=2014-05-30|work=Basler Zeitung, Basler Zeitung|access-date=2017-04-07|language=de|issn=1420-3006}}</ref><ref>{{Cite news|url=http://www.tagesanzeiger.ch/kultur/theater/Kurz--kritisch-im-Mai/story/12843804|title=Kurz & kritisch im Mai|date=2014-05-30|work=Tages-Anzeiger, Tages-Anzeiger|access-date=2017-04-07|language=de|issn=1422-9994}}</ref>, especially praising it for not having lost "neither the linguistic finesse nor the social paralysis of Lermontov’s Zeitgeist"<ref name=":0" />, both of which constitute the novel’s Byronic character.

On July 22, 2015, The Bolshoi Theatre in Moscow premiered a ballet adaptation of "Hero of Our Time". The ballet was choreographed by San Francisco Ballet's Choreographer in Residence, Yuri Possokhov, and directed by [[Kirill Serebrennikov]] - who is also the author of the libretto. The score was commissioned purposefully for this production and composed by Ilya Demutsky. This production focuses on three novellas from Lermontov's novel - Bela, Taman, and Princess Mary.<ref>http://www.bolshoi.ru/en/performances/813/</ref>

==Bibliography of English translations<ref>Based on B.L. Kandel, [http://feb-web.ru/feb/lermont/texts/selected/gnv/gnv-203-.htm ''Bibliography of translations of "A Hero of Our Time" into foreign languages''] {{webarchive |url=https://web.archive.org/web/20110827082110/http://feb-web.ru/feb/lermont/texts/selected/gnv/gnv-203-.htm |date=August 27, 2011 }} (Russian); published in: ''A Hero of Our Time.'' Moscow, 1962, pp. 209–210.</ref>==

#''Sketches of Russian life in the Caucasus. By a Russe, many years resident amongst the various mountain tribes.'' London, Ingram, Cook and Co., 1853. 315 pp. "The illustrated family novellist" series, #2. (a liberal translation with changed names of the heroes; "Taman" not translated).
#''The hero of our days.'' Transl. by Theresa Pulszky. London, Hudgson, 1854. 232 pp. "The Parlour Library". Vol.112. ("Fatalist" not translated).
#''A hero of our own times. Now first transl. into English.'' London, Bogue, 1854. 231 pp., ill. (the first full translation of the novel by an anonymous translator).
#''A hero of our time.'' Transl. by R.I. Lipmann. London, Ward and Downey, 1886. XXVIII, 272 pp. ("Fatalist" not translated).
#''Taman''. In: ''Tales from the Russian. Dubrovsky by Pushkin. New year's eve by Gregorowitch. Taman by Lermontoff.'' London, The Railway and general automatic library, 1891, pp.&nbsp;229–251.
#''Russian reader: Lermontof's modern hero, with English translation and biographical sketch by Ivan Nestor-Schnurmann.'' Cambridge, Univ. press, 1899. XX, 403 pp. (a dual language edition; "Fatalist" not translated)
#''Maxim Maximich.'' — In: Wiener L. ''Anthology of Russian literature''. T. 2, part 2. London—N.Y., 1903, pp.&nbsp;157–164. (a reduced version of the "Maxim Maximich" chapter).
#''The heart of a Russian.'' Transl. by J.H. Wisdom and Marr Murray. London, Herbert and Daniel, 1912. VII, 335 pp. (also published in 1916 by Hodder and Stoughton, London—N.Y.—Toronto).
#''The duel. Excerpt from The hero of our own time.'' Transl. by T. Pulszky. — In: ''A Russian anthology in English''. Ed. by C.E.B. Roberts. N. Y., 1917, pp.&nbsp;124–137.
#''A traveling episode''. — In: ''Little Russian masterpieces''. Transl. by Z.A. Ragozin. Vol. 1. N.Y., Putnam, 1920, pp.&nbsp;165–198. (an excerpt from the novel).
#''A hero of nowadays''. Transl. by John Swinnerton Phillimore. London, Nelson, 1924.
#''Taman'''. — In: Chamot A. ''Selected Russian short stories''. Transl. by A.E. Chamot. London, 1925—1928, pp. 84—97.
#''A hero of our time''. Transl. by Reginald Merton. Mirsky. London, Allan, 1928. 247 pp.
#''Fatalist. Story.'' Transl. by G.A. Miloradowitch. — In: ''Golden Book Magazine''. Vol. 8. N.Y., 1928, pp. 491—493.
#''A hero of our own times''. Transl. by Eden and Cedar Paul for the Lermontov centenary. London, Allen and Unwin, 1940. 283 pp. (also published by Oxford Univ. Press, London—N.Y., 1958).
#''Bela''. Transl. by Z. Shoenberg and J. Domb. London, Harrap, 1945. 124 pp. (a dual language edition).
#''A hero of our time''. Transl. by Martin Parker. Moscow, Foreign languages publ. house, 1947. 224 pp., ill. (republished in 1951 and 1956; also published by Collet's Holdings, London, 1957).
#''A hero of our time. A novel.'' Transl. by [[Vladimir Nabokov]] in collab. with [[Dmitri Nabokov]]. Garden City, N.Y., Doubleday, 1958. XI, 216 pp. "Doubleday Anchor Books".
#''A Lermontov reader''. Ed., transl., and with an introd. by Guy Daniels. New York: Grosset & Dunlap, 1965.
#''A hero of our time''. Transl. with an introduction by Paul Foote. Harmondsworth, Middlesex: Penguin Books, 1966.
#''Major poetical works''. Transl., with an introduction and commentary by Anatoly Liberman. Minneapolis : University of Minnesota Press, 1983.
#''Vadim''. Transl. by Helena Goscilo. Ann Arbor: Ardis Publishers, 1984.
#''A hero of our time''. Transl. with an introduction and notes by Natasha Randall; foreword by Neil Labute. New York: Penguin, 2009.
#''A hero of our time''. Translated by Philip Longworth. With an afterword by William E. Harkins, London, 1964, & New York : New American Library 1964
#''A hero of our time''. Transl. by Martin Parker, revised and edited by Neil Cornwell, London : Dent 1995
#''A hero of our time''. Transl. by Alexander Vassiliev, London: Alexander Vassiliev 2010.  (a dual language edition).
#''A hero of our time''. Transl. by Nicholas Pasternak Slater, Oxford World’s Classics 2013.
<ref>Translations from Longworth on as cited in COPAC catalogue.</ref>

==See also==
{{portal|Novels}}
* [[Romanticism]]
* [[Tragic hero]]

==References==
{{reflist|30em}}

==Further reading==
*{{cite book |title=Lermontov's "A Hero of Our Time": A Critical Companion |editor1-first=Lewis |editor1-last=Bagby |year=2002 |publisher=Northwestern University Press |location=Chicago |isbn=978-0-8101-1680-1 |url= |accessdate=}} {{link language|en}}

==External links==
*[http://www.eldritchpress.org/myl/hero.htm Full text (English translation) at Eldritch Press]. {{link language|en}}
{{gutenberg|no=913|name=A Hero of Our Time}} {{link language|en}}
* {{librivox book | title=A Hero of Our Time | author=Mikhail Yurevich LERMONTOV}}  
*[http://ilibrary.ru/text/12/ Full text of ''A Hero of Our Time'' in the original Russian] {{link language|ru}}
*[http://geroj-nashego-vremeni.ru/ Website dedicated to the novel ''A Hero of Our Time''] {{link language|ru}}
*[http://www.bibliomania.com/1/7/291/2019/frameset.html Study guides for ''A Hero of Our Time'' at Bibliomania] {{link language|en}}
*[http://www.iyafestival.org.uk/ Website for the Premiere of the English Language Adaptation]. {{link language|en}}
*[https://web.archive.org/web/20110928140919/http://www.broadwaybaby.com/edinburgh-fringe/10442-a-hero-of-our-time Review of "A Hero of Our Time" stage production] {{link language|en}}

{{Mikhail Lermontov}}

{{Authority control}}

{{DEFAULTSORT:Hero of Our Time, A}}
[[Category:1840 novels]]
[[Category:Novels by Mikhail Lermontov]]
[[Category:Novels set in 19th-century Russia]]
[[Category:Gothic novels]]
[[Category:Russian novels adapted into plays]]