{{for|the political scientist|Michael P. Howlett}}
{{Infobox State Senator
|image = howlett.jpg
|name=Michael J. Howlett, Sr.
|order= 33rd
|office= Secretary of State of Illinois
|term_start= January 8, 1973
|term_end= January 10, 1977
|governor= [[Dan Walker (politician)|Daniel Walker]]
|predecessor= [[John W. Lewis, Jr.]]
|successor= [[Alan J. Dixon]]
|order2= 24th
|office2= Illinois Auditor of Public Accounts
|term_start2 = January 9, 1961
|term_end2 = January 8, 1973
|governor2= [[Otto Kerner, Jr.]]<br>[[Samuel H. Shapiro]]<br>[[Richard B. Ogilvie]]
|preceded2 = [[Elbert S. Smith]]
|succeeded2 = [[George W. Lindberg]]<br> as [[Illinois Comptroller]]
|birth_date= {{Birth date |1914|8|30|}}
|birth_place= [[Chicago, Illinois]]
|death_date= {{death date and age|1992|5|4|1914|8|30}}
|death_place= Chicago, Illinois
|spouse=Helen Geary
|children= 6
|party= [[Democratic Party (United States)|Democratic]]
|religion= [[Roman Catholic]]
| branch= [[United States Navy]]
| serviceyears=
|battles=[[World War II]]
}}

'''Michael J. Howlett, Sr.''' (August 30, 1914 - May 4, 1992) was a [[Democratic Party (United States)|Democratic]] politician from the [[U.S. state]] of [[Illinois]], who was elected several times to statewide office.

==Early life==
Howlett was born in [[Chicago]], a son of Irish immigrants. As a youth, Howlett was [[All-American]] [[water polo]] player, participating on ten championship teams of the Illinois Athletic Club.<ref name = 73BB>[http://www.idaillinois.org/cdm4/document.php?CISOROOT=/bb&CISOPTR=37373&REC=19 1973-1974 Illinois Blue Book p22]</ref> He graduated from [[Providence-St. Mel|St. Mel High School]] and briefly attended [[DePaul University]], leaving the latter in 1934 to become a state bank examiner. Subsequently, he founded his own [[insurance]] business, served as Chicago-area director of the [[National Youth Administration]], was an executive for the [[Chicago Park District]], was appointed regional director of the U.S. [[Office of Price Stabilization]], and was a steel company executive. He was also a [[United States Navy|U.S. Navy]] veteran of [[World War II]].

==Statewide Officeholder==
In 1956, Howlett ran for Illinois Auditor and is credited with exposing embezzlement by incumbent Auditor [[Orville Hodge]] of more than $1.5 million in state funds. Hodge resigned and eventually went to prison, but Howlett lost the general election to [[Elbert Sidney Smith]] as part of a national [[Republican Party (United States)|Republican]] [[List of landslide victories|landslide]]. However, in the next general election, in 1960, Howlett ''was'' elected Auditor of Public Accounts (the Auditor's Office was the predecessor to the current office of [[Comptroller]]), and was re-elected twice. During Howlett's first term as Auditor, he cut the budget of the office by one-fifth, and returned over $600,000 to the state treasury.<ref name = 73BB/> In 1972, Howlett was elected [[Illinois Secretary of State]], becoming the first Democrat state officer to win four consecutive statewide elections.

==1976 Illinois Gubernatorial Campaign==
Howlett was prepared to run for re-election in 1976, but was encouraged by [[Cook County Democratic Party|Cook County Democrats]] to challenge [[incumbent]] [[Governor of Illinois|Governor]] [[Dan Walker (politician)|Dan Walker]] for the Democratic nomination in 1976. Howlett defeated Walker in the March primary, and stood as the Democratic nominee for governor of Illinois in the general election, whereupon he was defeated by [[Republican Party (United States)|Republican]] nominee [[James R. Thompson]].

Throughout the campaign, Howlett was dogged by conflict of interest charges, first raised by Walker, over payments Howlett received as an executive at Sun Steel Company. A report issued by former Illinois Supreme Court Justice [[Marvin Burt]] at the behest of Republican state Attorney General [[William J. Scott (Illinois)|William J. Scott]] was highly critical of Howlett. However, a Cook County judge ruled no conflict of interest had arisen, and cleared Howlett. Thompson, who successfully prosecuted former Illinois governor [[Otto Kerner, Jr.]],  continued to hammer the issue during the general election campaign, and attacked Howlett as corrupt, and Attorney General Scott vowed to appeal the judge's ruling. Ironically, it was Scott who later was forced to resign after a felony conviction.<ref>[http://www.lib.niu.edu/1980/ii801204.html "Scott's Justice," ''Illinois Issues'', December 1980]</ref>

Early polls of the contest had Howlett in the lead, although Thompson had nearly closed the gap by the time of the primary.<ref name='illiss'>[http://www.lib.niu.edu/1976/ii761106.html "Howlett v. Thompson," ''Illinois Issues'', November 1976]</ref> However, Walker's attacks during the bitter primary weakened Howlett, and by August, Thompson held a slim lead in the polls.<ref name=illiss/> His lead expanded during the campaign, and Howlett ended up losing by 30 percentage points (nearly 1.4 million votes), the widest margin of defeat for any [[Democratic Nominee for Governor of Illinois]] in history. Thompson was the first candidate for Governor to receive over 3 million votes, and his tally of 3,000,395 remains the highest number of votes ever cast for a candidate in an election for Governor of Illinois.

==Retirement==
After his loss in the 1976 governor's race, Howlett opened a private consulting business.

Howlett would later see his son run for statewide office through bizarre circumstances. In the 1986 Democratic primary for [[Lieutenant Governor of Illinois|Lieutenant Governor]], former U.S. Senator [[Adlai Stevenson III]] and the Democratic Party selected State senator [[George E. Sangmeister]] as the party-preferred candidate, however he narrowly lost the [[primary election|primary]] to [[Mark J. Fairchild|Mark Fairchild]] (a [[Lyndon LaRouche]] activist). After LaRouche followers had won the Democratic nominations for both Lieutenant Governor and Secretary of State, Stevenson refused to run as the Democratic standard-bearer, and formed the [[Solidarity Party]]. When Sangmeister was unwilling to run with Stevenson in the fall, Howlett's son Michael J. Howlett, Jr., then a Cook County judge,<ref name=cst>[https://query.nytimes.com/gst/fullpage.html?res=9A0DEED8153EF935A35756C0A960948260 AROUND THE NATION; Stevenson Announces Illinois Running Mate] May 6, 1986, The New York Times</ref> was nominated by the Solidarity Party. Stevenson-Howlett went down to defeat in the fall, with only 40% of the vote. Another son, Edward G. Howlett, was the unsuccessful Republican nominee for Chicago City Clerk in 1995.

==Death and legacy==
Howlett died in Chicago's [[Mercy Hospital and Medical Center|Mercy Hospital]] of chronic [[kidney failure]]. He had suffered a stroke three months earlier and remained hospitalized from then until his death.

The building housing the offices of the Illinois Secretary of State in [[Springfield, Illinois]], formerly known as the [[Centennial Building (Illinois)|Centennial Building]], is named after Michael J.Howlett.

==Election history==

{{Election box begin |title=[[Illinois gubernatorial election, 1976|1976 gubernatorial election, Illinois]]}}

{{Election box candidate with party link
|party      = Republican Party (US)
  |candidate  = [[James R. Thompson]]
  |votes      = 3,000,395
  |percentage = 64.68
  |change     = +15.66
}}
{{Election box candidate with party link
|party      = Democratic Party (US)
  |candidate  = Michael Howlett
  |votes      = 1,610,258
  |percentage = 34.71
  |change     = -15.97
}}
{{Election box candidate with party link
|party      = Libertarian Party (US)
  |candidate  = F. Joseph McCaffrey
  |votes      = 7,552
  |percentage = 0.16
  |change     =
}}
{{Election box candidate
|party      = [[Communist Party USA|Communist Party (US)]]
  |candidate  = Ishmael Flory
  |votes      = 10,091
  |percentage = 0.22
  |change     = +0.12
}}
{{Election box candidate
|party      =
  |candidate  = Others
  |votes      = 10,375
  |percentage = 0.23
  |change     =
}}{{Election box majority|
  |votes      = 1,390,137
  |percentage = 29.93
  |change     =
}}
{{Election box turnout
|votes      = 4,638,671
  |percentage =
  |change     =
}}
{{Election box gain with party link|
|winner     = Republican Party (US)
  |loser    = Democratic Party (US)
  |swing    =
}}
{{Election box end}}

{{s-start}}
!bgcolor=#cccccc |Year
!bgcolor=#cccccc |Office
!bgcolor=#cccccc |Election
!
!bgcolor=#cccccc |Subject
!bgcolor=#cccccc |Party
!bgcolor=#cccccc |Votes
!bgcolor=#cccccc |%
!
!bgcolor=#cccccc |Opponent
!bgcolor=#cccccc |Party
!bgcolor=#cccccc |Votes
!bgcolor=#cccccc |%
|-
|1976
|[[Governor of Illinois]]
|Primary
||
|bgcolor=#DDEEFF |Michael Howlett
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF align = "right"|811,721
|bgcolor=#DDEEFF |53.82
|
|bgcolor=#DDEEFF |[[Dan Walker (politician)|Dan Walker]] (Inc.)
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF align = "right"|696,380
|bgcolor=#DDEEFF |46.18
|-
|1972
|[[Illinois Secretary of State]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |2,360,327
|bgcolor=#DDEEFF |51.69
|
|bgcolor=#FFE8E8 |Edmund J. Kucharski
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |2,187,544
|bgcolor=#FFE8E8 |47.91
|-
|1968
|[[Illinois Comptroller|Illinois Auditor of Public Accounts]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett (Inc.)
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |2,215,401
|bgcolor=#DDEEFF |50.99
|
|bgcolor=#FFE8E8 |William C. Harris
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |2,106,676
|bgcolor=#FFE8E8 |48.49
|-
|1964
|[[Illinois Comptroller|Illinois Auditor of Public Accounts]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett (Inc.)
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |2,513,831
|bgcolor=#DDEEFF |55.47
|
|bgcolor=#FFE8E8 |John Kirby
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |2,017,951
|bgcolor=#FFE8E8 |44.53
|-
|1960
|[[Illinois Comptroller|Illinois Auditor of Public Accounts]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |2,296,220
|bgcolor=#DDEEFF |50.44
|
|bgcolor=#FFE8E8 |[[Elbert S. Smith]] (Inc.)
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |2,246,833
|bgcolor=#FFE8E8 |49.35
|-
|1956
|[[Illinois Comptroller|Illinois Auditor of Public Accounts]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |1,992,707
|bgcolor=#DDEEFF |47.23
|
|bgcolor=#FFE8E8 |[[Elbert S. Smith]]
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |2,217,229
|bgcolor=#FFE8E8 |52.55
|-
|1950
|[[Illinois Treasurer]]
|General
|
|bgcolor=#DDEEFF |Michael Howlett
|bgcolor=#DDEEFF |[[Democratic Party (United States)|Democratic]]
|bgcolor=#DDEEFF |1,568,763
|bgcolor=#DDEEFF |44.32
|
|bgcolor=#FFE8E8 |[[William G. Stratton]]
|bgcolor=#FFE8E8 |[[Republican Party (United States)|Republican]]
|bgcolor=#FFE8E8 |1,959,734
|bgcolor=#FFE8E8 |55.36
|-
{{s-end}}

==References==
{{reflist}}

==Sources==
*''Chicago Tribune'' Historical Archive online (May 5, 1992), retrieved April 28, 2007.
*[http://www.ioc.state.il.us/Office/history.cfm#HOWLETT Illinois Comptroller web site - History of the Office - Howlett]
*[http://www.lib.niu.edu/1976/ii761106.html "Howlett v. Thompson," ''Illinois Issues'', November 1976]
*[http://www.idaillinois.org/cdm4/document.php?CISOROOT=/bb&CISOPTR=36716&REC=20 1975-1976 Illinois Blue Book p40]

{{s-start}}
{{s-off}}
{{s-bef| before=[[Elbert S. Smith]]}}
{{s-ttl| title=[[Illinois Auditor of Public Accounts]]| years=1961 &ndash; 1973}}
{{s-aft| after=[[George W. Lindberg]]<br><small>''as [[Illinois Comptroller]]''</small>}}
{{s-bef| before=[[John W. Lewis, Jr.|John W. Lewis]]}}
{{s-ttl| title=[[Illinois Secretary of State]]| years=1973 &ndash; 1977}}
{{s-aft| after=[[Alan J. Dixon]]}}

{{s-ppo}}
{{succession box|title= [[Democratic Nominee for Governor of Illinois]]|before=[[Dan Walker (politician)|Daniel Walker]]|after=[[Michael Bakalis]]|years=1976}}
{{s-end}}

{{Illinois Secretaries of State}}
{{Illinois Comptrollers}}

{{DEFAULTSORT:Howlett, Michael J.}}
[[Category:1914 births]]
[[Category:1992 deaths]]
[[Category:DePaul University alumni]]
[[Category:Deaths from renal failure]]
[[Category:Politicians from Chicago]]
[[Category:American people of Irish descent]]
[[Category:Secretaries of State of Illinois]]
[[Category:Comptrollers of Illinois]]
[[Category:Illinois Democrats]]
[[Category:American Roman Catholics]]
[[Category:Illinois gubernatorial candidates]]
[[Category:20th-century American politicians]]