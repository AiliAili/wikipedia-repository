[[Image:World population growth rates 1800-2005.png|thumb|300px|A chart of estimated annual growth rates in world population, 1800–2005. Rates before 1950 are annualized historical estimates from the [http://www.census.gov/ipc/www/worldhis.html US Census Bureau]. Red = USCB projections to 2025.]]

A '''Malthusian catastrophe''' (also known as '''Malthusian check''' or '''Malthusian spectre''') is a prediction of a forced return to [[subsistence]]-level conditions once [[population growth]] has outpaced [[agriculture|agricultural]] [[Production (economics)|production]].

==Thomas Malthus==
In 1779, [[Thomas Robert Malthus|Thomas Malthus]] wrote:

{{quote|Famine seems to be the last, the most dreadful resource of nature. The power of population is so superior to the power of the earth to produce subsistence for man, that premature death must in some shape or other visit the human race. The vices of mankind are active and able ministers of depopulation. They are the precursors in the great army of destruction, and often finish the dreadful work themselves. But should they fail in this war of extermination, sickly seasons, epidemics, pestilence, and plague advance in terrific array, and sweep off their thousands and tens of thousands. Should success be still incomplete, gigantic inevitable famine stalks in the rear, and with one mighty blow levels the population with the food of the world.|Thomas Malthus, 1798. ''[[An Essay on the Principle of Population]]''. Chapter VII, p61<ref name="Oxford World's Classics reprint">Oxford World's Classics reprint</ref>}}

Notwithstanding the apocalyptic image conveyed by this particular paragraph, Malthus himself did not subscribe to the notion that mankind was fated for a "catastrophe" due to population overshooting resources. Rather, he believed that population growth was generally restricted by available resources:
[[File:Malthus PL en.svg|thumb]]
{{quote|The passion between the sexes has appeared in every age to be so nearly the same that it may always be considered, in algebraic language, as a given quantity. The great law of necessity which prevents population from increasing in any country beyond the food which it can either produce or acquire, is a law so open to our view...that we cannot for a moment doubt it. The different modes which nature takes to prevent or repress a redundant population do not appear, indeed, to us so certain and regular, but though we cannot always predict the mode we may with certainty predict the fact.|Thomas Malthus, 1798. ''[[An Essay on the Principle of Population]]''. Chapter IV.}}

==Neo-Malthusian theory==<!-- This section is linked from [[Demographic transition]] -->

[[File:Wheat yields in Least Developed Countries.svg|thumb|250px|Wheat yields in developing countries since 1961, in kg/[[hectare|ha]].  The steep rise in crop yields in the U.S. began in the 1940s.  The percentage of growth was fastest in the early rapid growth stage.  In developing countries maize yields are still rapidly rising.<ref>{{Cite journal
| last1 = Fischer
| first1 =R. A.
| last2 =Byerlee
| first2 =Eric
| last3 =Edmeades
| first3 =E.  O.
| author1-link =
| title = Can Technology Deliver on the Yield Challenge to 2050
| year = 
|journal=Expert Meeting on How to Feed the World
|issue=
|pages=12
|publisher= Food and Agriculture Organization of the United Nations
| url = ftp://ftp.fao.org/docrep/fao/012/ak977e/ak977e00.pdf
}}</ref>]]

After [[World War II]], [[mechanized agriculture]] produced a dramatic increase in productivity of agriculture and the [[Green Revolution]] greatly increased crop yields, expanding the world's food supply while lowering food prices.  In response, the growth rate of the world's population accelerated rapidly, resulting in predictions by [[Paul R. Ehrlich]], Simon Hopkins,<ref>{{cite book|last = Hopkins|first = Simon|title = A Systematic Foray into the Future|publisher = Barker Books|year = 1966|pages = 513–569}}
</ref> and many others of an imminent Malthusian catastrophe.  However, populations of most developed countries grew slowly enough to be outpaced by gains in productivity.

By the early 21st century, many technologically developed countries had passed through the [[demographic transition]], a complex social development encompassing a drop in [[total fertility rate]]s in response to various [[fertility factor (demography)|fertility factors]], including lower [[infant mortality]], increased [[urbanization]], and a wider availability of effective [[birth control]].

On the assumption that the [[demographic transition]] is now spreading from the developed countries to [[less developed countries]], the [[United Nations Population Fund]] estimates that human population may peak in the late 21st century rather than continue to grow until it has exhausted available resources.<ref name="UN">{{cite web|url=http://www.un.org/esa/population/publications/longrange2/WorldPop2300final.pdf|title=2004 UN Population Projections, 2004.|format=PDF}}</ref>

[[Image:World-Population-1800-2100.svg|thumb|250px|World population from 1800 to 2100, based on [http://www.un.org/esa/population/publications/longrange2/WorldPop2300final.pdf UN 2004 projections] (red, orange, green) and [http://www.census.gov/ipc/www/worldhis.html US Census Bureau historical estimates] (black)]]
[[Image:Food production per capita.svg|thumb|250px|Growth in food production has been greater than population growth. Food per person increased since 1961]]

Historians have estimated the total human population back to 10,000 BC.<ref>{{cite web|url=http://www.census.gov/ipc/www/worldhis.html|title=Historical Estimates of World Population, U.S. Bureau of the Census, 2006.}}</ref> The figure on the right shows the trend of total population from 1800 to 2005, and from there in three projections out to 2100 (low, medium, and high).<ref name="UN"/> The United Nations population projections out to 2100 (the red, orange, and green lines) show a possible peak in the world's population occurring by 2040 in the first scenario, and by 2100 in the second scenario, and never ending growth in the third.

The graph of annual growth rates (at the top of the page) does not appear exactly as one would expect for long-term exponential growth.  For exponential growth it should be a straight line at constant height, whereas in fact the graph from 1800 to 2005 is dominated by an enormous hump that began about 1920, peaked in the mid-1960s, and has been steadily eroding away for the last 40 years. The sharp fluctuation between 1959 and 1960 was due to the combined effects of the [[Great Leap Forward]] and a natural disaster in China.<ref name="USCB">{{cite web|url=http://www.census.gov/ipc/www/idb/|title=International Data Base}}</ref> Also visible on this graph are the effects of the [[Great Depression]], the two world wars, and possibly also the [[1918 flu pandemic]].

Though short-term trends, even on the scale of decades or centuries, cannot prove or disprove the existence of mechanisms promoting a Malthusian catastrophe over longer periods, the prosperity of a major fraction of the human population at the beginning of the 21st century, and the debatability of [[ecological collapse]] made by [[Paul R. Ehrlich]] in the 1960s and 1970s, has led some people, such as economist [[Julian Lincoln Simon|Julian L. Simon]], to question its inevitability.<ref>Simon, Julian L, "[http://www.juliansimon.org/writings/Articles/REVOLUTI.txt More People, Greater Wealth, More Resources, Healthier Environment]", ''Economic Affairs: J. Inst. Econ. Affairs'', April 1994.</ref>

A 2004 study by a group of prominent economists and ecologists, including [[Kenneth Arrow]] and Paul Ehrlich<ref>Arrow, K., P. Dasgupta, L. Goulder, G. Daily, P. Ehrlich, G. Heal, S. Levin, K. Mäler, S. Schneider, D. Starrett and B. Walker, "Are We Consuming Too Much" ''Journal of Economic Perspectives'', 18(3), 147-172, 2004.</ref> suggests that the central concerns regarding sustainability have shifted from population growth to the consumption/savings ratio, due to shifts in population growth rates since the 1970s.  Empirical estimates show that public policy (taxes or the establishment of more complete property rights) can promote more efficient consumption and investment that are sustainable in an ecological sense; that is, given the current (relatively low) population growth rate, the Malthusian catastrophe can be avoided by either a shift in consumer preferences or public policy that induces a similar shift.

A 2002 study<ref>[http://www.fao.org/english/newsroom/news/2002/7828-en.html World agriculture 2030: Global food production will exceed population growth] August 20, 2002.</ref> by the [[United Nations Food and Agriculture Organization|UN Food and Agriculture Organization]] predicts that world food production will be in excess of the needs of the human population by the year 2030; however, that source also states that hundreds of millions will remain hungry (presumably due to economic realities and political issues).

==Criticism==
Since the mid-19th Century, many economists have suggested that humanity will not face a Malthusian catastrophe because "necessity is the mother of invention" and "the market will fix itself".

[[Karl Marx]] and [[Friedrich Engels]] argued that Malthus failed to recognize a crucial difference between humans and other species. In capitalist societies, as Engels put it, scientific and technological "progress is as unlimited and at least as rapid as that of population".<ref>Engels, Friedrich."Outlines of a Critique of Political Economy", ''Deutsch-Französische Jahrbücher'', 1844, pg.1.</ref> Marx argued, even more broadly, that the growth of both a human population ''in toto'' and the "[[relative surplus population]]" within it, occurred in direct proportion to [[Capital accumulation|accumulation]].<ref>Karl Marx (transl. Ben Fowkes), ''Capital Volume 1'', Harmondsworth, Penguin, 1976 (originally 1867),  pp. 782–802.</ref>

[[Henry George]] criticized Malthus's view that population growth was a cause of poverty, arguing that poverty was caused by human greed. George also argued that if population growth were a cause of hardship, it would not be promoted by all religions.<ref>Progress and Poverty, Chapter 7, Malthus vs. Facts in http://www.henrygeorge.org/pchp7.htm</ref>{{Incomplete short citation|date=October 2016}}

[[Ester Boserup]] suggested that population levels determined agricultural methods, rather than agricultural methods determining population.<ref>Ester Boserup, ''The Conditions of Agricultural Growth: The Economics of Agrarian Change under Population Pressure''</ref>{{Incomplete short citation|date=October 2016}}

[[Julian Lincoln Simon|Julian Simon]] was another economist who argued that there could be no global Malthusian catastrophe, because of two factors: (1) the existence of new knowledge, and educated people to take advantage of it, and (2) "economic freedom", that is, the ability of the world to increase production when there is a profitable opportunity to do so.<ref>The Ultimate Resource II: People, Materials, and Environment in http://www.juliansimon.com/writings/Ultimate_Resource/</ref>{{Incomplete short citation|date=October 2016}}

In contrast to these criticisms, some individuals, such as [[Joseph Tainter]], argue that science has diminishing marginal returns<ref>Tainter, Joseph: ''The Collapse of Complex Societies'', Cambridge University Press, Cambridge, UK, 2003.</ref>{{Incomplete short citation|date=October 2016}} and that scientific progress is becoming more difficult, harder to achieve, and more costly.

== See also ==
* [[Demographic trap]]
* [[The dismal science]]
* [[Food security]]
* [[Human overpopulation]]
* [[Malthusian trap]]
* [[Overshoot (population)]]
* [[Olduvai theory]]
* [[Population Matters#Pledge two or fewer|Pledge two or fewer]] (campaign for smaller families)
* [[r/K selection theory]]

==Notes==
{{Reflist|colwidth=30em}}

==References==
{{Refbegin}}
* Korotayev A., Malkov A., Khaltourina D. ''[http://cliodynamics.ru/index.php?option=com_content&task=view&id=124&Itemid=70 Introduction to Social Macrodynamics: Compact Macromodels of the World System Growth.]'' Moscow: URSS, 2006. ISBN 5-484-00414-4
* Korotayev A., Malkov A., Khaltourina D. ''[http://cliodynamics.ru/index.php?option=com_content&task=view&id=172&Itemid=70 Introduction to Social Macrodynamics: Secular Cycles and Millennial Trends.]'' Moscow: URSS, 2006. ISBN 5-484-00559-0 [http://cliodynamics.ru/index.php?option=com_content&task=view&id=302&Itemid=1 See especially Chapter 2 of this book]
* Korotayev A. & Khaltourina D. ''[http://cliodynamics.ru/index.php?option=com_content&task=view&id=165&Itemid=70 Introduction to Social Macrodynamics: Secular Cycles and Millennial Trends in Africa.]'' Moscow: URSS, 2006. ISBN 5-484-00560-4
*{{Cite document
 | last = Malthus
 | first = Thomas Robert
 | author-link =Thomas Malthus
 | year = 1826
 | title =An Essay on the Principle of Population: A View of its Past and Present Effects on Human Happiness; with an Inquiry into Our Prospects Respecting the Future Removal or Mitigation of the Evils which It Occasions
 | edition = Sixth
 | publication-place = London
 | publisher =John Murray
 | url =http://www.econlib.org/library/Malthus/malPlong.html
 | accessdate =2008-11-22 }}
* [[Peter Turchin|Turchin, P.]], et al., eds. (2007). [http://edurss.ru/cgi-bin/db.pl?cp=&page=Book&id=53185&lang=en&blang=en&list=Found History & Mathematics: Historical Dynamics and Development of Complex Societies.] Moscow: KomKniga. ISBN 5-484-01002-0
*[http://cliodynamics.ru/index.php?option=com_content&task=view&id=309&Itemid=1 A Trap At The Escape From The Trap? Demographic-Structural Factors of Political Instability in Modern Africa and West Asia. ''Cliodynamics'' 2/2 (2011): 1-28].
{{Refend}}

==External links==
*[http://www.blupete.com/Literature/Biographies/Philosophy/Malthus.htm Essay on life of Thomas Malthus]
*[http://www.ac.wwu.edu/~stephan/malthus/malthus.0.html Malthus' Essay on the Principle of Population]
*[http://www.daviddfriedman.com/Academic/Laissez-Faire_In_Popn/L_F_in_Population.html David Friedman's essay arguing against Malthus' conclusions]
*[http://www.un.org/popin/wdtrends.htm United Nations Population Division World Population Trends homepage]
{{Navboxes|list=
{{Human impact on the environment}}
{{Population}}
{{Population country lists}}
{{biological organisation}}
{{Globalization|state=autocollapse}}
{{Doomsday}}
}}

{{DEFAULTSORT:Malthusian Catastrophe}}
[[Category:Futurology]]
[[Category:Population]]
[[Category:Demography]]
[[Category:Human geography]]
[[Category:Demographic economics]]
[[Category:Economic problems]]
[[Category:Doomsday scenarios]]
[[Category:Disaster preparedness]]
[[Category:Human overpopulation]]