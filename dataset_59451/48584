{{Orphan|date=May 2014}}

{{Infobox company
| name     = HotelQuickly
| logo     =HotelQuickly_Logo_Square.png 
| type     = 
| area_served      = [[Hong Kong]], [[Singapore]], [[Thailand]], [[Malaysia]], [[Indonesia]], [[Philippines]], [[Taiwan]], [[Vietnam]], [[Macau]], [[Cambodia]], [[Australia]], [[New Zealand]], [[South Korea]], [[Myanmar]],  [[Laos]],  [[Japan]]
| key_people       = 
| industry         = [[Hotel]], [[Application software|Apps]]
| genre            =
| products         =
| services         = [[Tourism|Travel Industry]], [[Hotel|Hotel Booking]]
| revenue          = 
| operating_income =
| net_income       = 
| owner            =
| parent           =
| divisions        =
| subsid           =
| caption= 
| foundation       = December 2012
| founders          = Tomas Laboutka (CEO), Christian Mischler (Chairman, CMO), Michal Juhas (CTO), Mario Peng (CFO), Raphael Cohen (CSO)
| location         = [[Hong Kong]], Hong Kong
| locations        =
| incorporated     = December 2012
| company_slogan   = 
| website              = [http://www.hotelquickly.com/ hotelquickly.com]
}}
 
'''HotelQuickly''' is a mobile application available for [[iPhone]] and [[Android (operating system)|Android]] devices that allows users to find discounted hotels throughout [[Asia Pacific]].<ref>{{cite web|last=Yap|first=Jacky|title=HotelQuickly launches in 6 countries, books curated hotels|url=http://e27.co/hotelquickly-launches-in-6-countries-helps-you-book-hotels-from-a-curated-list/|work=e27|accessdate=26 March 2014}}</ref><ref>{{cite web|last=Wee|first=Willis|title=HotelQuickly Books Your Hotels Really Quickly, Launches in 6 Asian Countries|url=http://www.techinasia.com/hotelquickly-books-your-hotels-really-quickly/|work=Tech In Asia|accessdate=26 March 2014}}</ref><ref>{{cite web|last=Yeoh|first=Siew Hoon|title=Same-day hotel booking space heats up as HotelQuickly joins fray|url=http://webintravel.com/news/sameday-hotel-booking-space-heats-up-as-hotelquickly-joins-fray_3682|work=Web In Travel|accessdate=26 March 2014}}</ref> The application features more than 20,000 hotels and is localized to 6 different languages including [[Thai language|Thai]], [[Vietnamese language|Vietnamese]], [[Japanese language|Japanese]], traditional and simplified [[Chinese language|Chinese]], and [[English language|English]].<ref>{{cite web|last=Huang|first=Elaine|title=HotelQuickly travels to Philippines taking its presence to 12 countries|url=http://e27.co/hotelquickly-travels-to-philippines-adds-500-new-hotels-in-60-days/|work=e27|accessdate=26 March 2014}}</ref><ref>{{cite web|last=Grant|first=Rebecca|title=Same-day booking app HotelQuickly claims dominance in Asia before rival HotelTonight|url=http://venturebeat.com/2013/06/25/same-day-booking-app-hotelquickly-claims-dominance-in-asia-before-rival-hoteltonight/|work=VentureBeat}}</ref>

== History ==

HotelQuickly was founded in December 2012 by Tomas Laboutka ([[CEO]]), Christian Mischler ([[Chief operating officer|COO]] & [[Chief marketing officer|CMO]]), Michal Juhas ([[Chief Technical Officer|CTO]]), Mario Peng ([[CFO]]), and Raphael Cohen ([[Chief sales officer|CSO]]).<ref>{{cite news|title=Investment Bank Defectors Fuel Startup Scene in Hong Kong: Tech|url=http://www.businessweek.com/news/2014-01-29/investment-bank-defectors-fuel-startup-scene-in-hong-kong-tech|newspaper=Businessweek|date=29 January 2014}}</ref><ref>{{cite web|last=Walter|first=Christian|title=Hotel Quickly receives a US$1.16 million investment|url=http://bangkokstartup.com/2013/10/04/hotel-quickly-receives-us1-16-million-investment/|work=Bangkok Startup|accessdate=4 October 2013}}</ref>  Laboutka and Juhas previously founded ventures in Central Europe, Mischler and Cohen were co-founders and Managing Directors of [[Rocket Internet]] ventures in Asia, and Peng joined from global private equity fund [[Partners Group]].<ref>{{cite web|last=O'Hear|first=Steve|title=Ex-Rocket Internet Execs Launch HotelQuickly In Bid To Become The HotelTonight Of Asia|url=http://techcrunch.com/2013/03/20/hotelquickly/|work=TechCrunch|accessdate=26 March 2014}}</ref><ref>{{cite web|last=Xiang|first=Tracey|title=Hong Kong-based HotelQuickly Launches Last-minute Hotel Booking Service in Six Regions across Asia|url=http://technode.com/2013/03/21/hong-kong-based-hotelquickly-launches-last-minute-hotel-bookings-in-six-regions-across-asia/|work=TechNode|accessdate=26 March 2014}}</ref> Mischler is also currently serving as a partner at [[Swiss Founders Fund]].

HotelQuickly’s company headquarter and regional holding is based in [[Hong Kong]], while the operational head office is located in [[Bangkok]], [[Thailand]]. 
HotelQuickly launched publicly on March 20, 2013 in Hong Kong, [[Taiwan]], [[Thailand]], [[Malaysia]], [[Singapore]], and [[Indonesia]]. The app was initially available only for Apple devices and Android powered smartphones, in Summer 2013 the app was made available also for BlackBerry 10 devices.<ref>{{cite web|last=Hong|first=Kaylene|title=Booking app HotelQuickly announces launch of Blackberry app as it targets Indonesia and corporate world|url=http://thenextweb.com/asia/2013/06/03/booking-app-hotelquickly-announces-launch-of-blackberry-app-as-it-targets-indonesia-and-corporate-world/#!BqVwb|work=The Next Web}}</ref><ref>{{cite web|last=Yeoh|first=Siew Hoon|title=HotelQuickly to launch Blackberry app, sees spike in weekend bookings|url=http://webintravel.com/news/hotelquickly-to-launch-blackberry-app-sees-spike-in-weekend-bookings_3813|work=Web In Travel}}</ref>
Over the course of 2013, HotelQuickly expanded to 5 more markets including three Asian countries ([[Vietnam]], [[Macau]], [[Cambodia]]) and the main pacific markets ([[Australia]], [[New Zealand]]).<ref>{{cite news|title=HotelQuickly arrives in Vietnam|url=http://webintravel.com/news/hotelquickly-arrives-in-vietnam_3729|newspaper=Web In Travel|date=1 May 2013}}</ref><ref>{{cite news|last=Summers|first=Nick|title=HotelQuickly launches its same-day hotel booking app in Australia with Sydney and Melbourne listings|url=http://thenextweb.com/apps/2013/05/08/hotel-quickly-launches-its-last-minute-hotel-booking-app-in-australia-with-sydney-and-melbourne-listings/#!Bq1h4|newspaper=The Next Web|date=8 May 2013}}</ref> HotelQuickly is the first company to make last-minute mobile-only hotel booking available on the two continents.<ref>{{cite news|last=Racoma|first=J. Angelo|title=HotelQuickly expands to new countries and gives us an Echelon special|url=http://e27.co/hotelquickly-expands-to-new-countries-and-gives-us-echelon-special/|date=28 May 2013}}</ref>
In August 2013, HotelQuickly secured 9m HKD in angel funding.<ref>{{cite web|last=Prabu|first=Karthick|title=The last-minute hotel market is alive and kicking, HotelQuickly raises $1.2 million funding|url=http://www.tnooz.com/article/last-minute-hotel-booking-market-alive-kicking-hotelquickly-raises-1-2-million-funding/|work=Tnooz}}</ref> Prominent investors included [[Koh Boon Hwee]], former Chairman of [[Singtel]] and Chairman of [[Singapore Airlines]] and current board member of several private companies such as Singapore’s [[Temasek Holdings]].<ref>{{cite web|last=Shu|first=Catherine|title=HotelQuickly Raises $1.16M Series A To Expand In Asia|url=http://techcrunch.com/2013/09/24/hotelquickly-raises-1-16m-series-a-to-expand-in-asia/|work=TechCrunch}}</ref> A USD 4.5m Series A-1 round was raised in Spring 2014, bringing the total funds raised to date to USD 5.65m.<ref>{{cite news|last1=O'Hear|first1=Steve|title=HotelQuickly Raises $4.5 Million To Double-Down On Last Minute Hotel Booking In Asia-Pacific|url=http://techcrunch.com/2014/07/09/hotelquickly-raises-4-5-million-to-double-down-on-last-minute-hotel-booking-in-asia-pacific/|accessdate=11 September 2014|publisher=Tech Crunch|date=9 July 2014}}</ref> The second round was led by [[GREE]] and co-invested by [[William E. Heinecke]], Chairman and CEO of [[Minor International]].<ref>{{cite news|last1=Hong|first1=Kaylene|title=HotelQuickly wants to make last-minute hotel booking big in Asia with funding from games giant GREE|url=http://thenextweb.com/asia/2014/07/10/hotelquickly-wants-to-make-last-minute-hotel-booking-big-in-asia-with-funding-from-games-giant-gree/|accessdate=11 September 2014|publisher=The Next Web|date=10 July 2014}}</ref><ref>{{cite news|last1=Dorbian|first1=Iris|title=Hotel Quickly books $4.5 mln|url=http://www.pehub.com/2014/07/hotel-quickly-books-4-5-mln/|accessdate=11 September 2014|publisher=Reuters|date=11 July 2014}}</ref>  
HotelQuickly has continued expanding throughout Asia Pacific to the [[Philippines]], [[Myanmar]], [[Laos]], [[South Korea]] and [[Japan]] bringing the number of available countries on the application to 16.<ref>{{cite news|title=Rocket Internet alumni club startup Hotel Quickly rolls into the Philippines due to high demand|url=http://ph.news.yahoo.com/rocket-internet-alumni-club-startup-040551391.html|newspaper=Yahoo! News|date=10 January 2014}}</ref><ref>{{cite news|title=HotelQuickly goes live in the Philippines offering late deals|url=http://www.ttgasia.com/article.php?article_id=22344|newspaper=TTG Asia|date=10 January 2014}}</ref><ref name=":0">{{Cite web|url=http://www.dealstreetasia.com/stories/last-minute-booking-startup-hotelquickly-acquires-japans-tonight-app-adds-16th-market-31703/|title=HotelQuickly acquires Japan's Tonight|website=DealStreetAsia|language=en-US|access-date=2016-08-17}}</ref> End of February 2016, HotelQuickly announced the acquisition of Japanese last-minute hotel booking app Tonight, which was acquired for an undisclosed amount and fully integrated into HotelQuickly. <ref name=":0" />

==Description==
HotelQuickly is a mobile app available for free on iOS and Android devices. The app features discounted last-minute rooms at hotels in 16 Asia-Pacific countries. HotelQuickly is a private sales channel. Members are required to sign up before they can view discounted hotel rates. In October 2015 the app expanded it booking window to a seven-day calendar. 
 
HotelQuickly offer members exclusive discounts from hotels looking to sell distressed inventory. Each day the app curates the best deals in each of its destinations. HotelQuickly's best price guarantee states that its room rates will always be lower than those found online.

== References ==

{{Reflist|2}}

== External links ==
*[http://www.hotelquickly.com/ Website]
*[http://www.hotelquickly.com/blog Travel related company blog]

{{DEFAULTSORT:HotelQuickly}}
[[Category:Companies established in 2012]]