{{Infobox journal
| title = Genes & Development
| cover = [[Image:Genes Dev cover (Dec 2008).gif]]
| editor = [[Terri Grodzicker]]
| discipline = [[Molecular biology]], [[developmental biology]], [[genetics]], [[cell biology]]
| abbreviation = Genes Dev.
| publisher = [[Cold Spring Harbor Laboratory Press]]
| country = United States
| frequency = Biweekly
| history = 1987–present
| openaccess = [[Delayed open access|Delayed]], after 6 months
| impact = 10.042
| impact-year = 2015
| website = http://genesdev.cshlp.org/
| link1 = http://genesdev.cshlp.org/content/current
| link1-name = Online access
| link2 = http://genesdev.cshlp.org/content
| link2-name = Online archive
| JSTOR = 
| OCLC = 301163874
| LCCN = 91642058
| CODEN = GEDEEP
| ISSN = 0890-9369
| eISSN = 1549-5477
}}
'''''Genes & Development''''' is a [[peer-reviewed]] [[scientific journal]] covering [[molecular biology]], [[molecular genetics]], [[cell biology]], and [[developmental biology|development]]. It was established in 1987 and is published twice monthly by [[Cold Spring Harbor Laboratory Press]] in association with [[The Genetics Society]].<ref>{{cite web |url=http://genesdev.cshlp.org/site/misc/about.xhtml |title=About the Journal |publisher=Cold Spring Harbor Laboratory Press |work=Genes & Development |accessdate=2012-12-22}}</ref>

According to the ''[[Journal Citation Reports]]'', the journal has a 2012 [[impact factor]] of 12.44, ranking it 14th out of 181 journals in the category "Cell Biology",<ref name=WoS1>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Cell Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-12-22 |series=[[Web of Science]] |postscript=.}}</ref> third out of 40 journals in the category "Developmental Biology",<ref name=WoS2>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Developmental Biology |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-12-22 |series=[[Web of Science]] |postscript=.}}</ref> and 7th out of 158 journals in the category "Genetics & Heredity".<ref name=WoS3>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Genetics & Heredity |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |accessdate=2012-12-22 |series=[[Web of Science]] |postscript=.}}</ref> Over 1999–2004, the journal was ranked fifth in the "Molecular Biology and Genetics" category according to ''ScienceWatch'', with an average of 47 citations per paper.<ref>http://archive.sciencewatch.com/jan-feb2005/sw_jan-feb2005_page1.htm  {{webarchive |url=https://web.archive.org/web/20081208204107/http://archive.sciencewatch.com/jan-feb2005/sw_jan-feb2005_page1.htm |date=December 8, 2008 }}</ref> All issues are available online via the journal website as [[Portable Document Format|PDF]]s, with a text version additionally available from August 1997. Content over 6 months old is freely available.

Since 1989, the [[editor-in-chief]] has been [[Terri Grodzicker]] ([[Cold Spring Harbor Laboratory]]).<ref>{{cite web |url=http://genesdev.cshlp.org/site/misc/edboard.xhtml |title=Genes & Development Editorial Board |publisher=Cold Spring Harbor Laboratory Press |work=Genes & Development |accessdate=2012-12-22}}</ref>

==References==
{{reflist}}

==External links==
* {{Official website|http://genesdev.cshlp.org/}}

{{DEFAULTSORT:Genes and Development}}
[[Category:Developmental biology journals]]
[[Category:Delayed open access journals]]
[[Category:Publications established in 1987]]
[[Category:Cold Spring Harbor Laboratory Press academic journals]]
[[Category:Biweekly journals]]
[[Category:English-language journals]]
[[Category:Academic journals associated with learned and professional societies]]