{{Use dmy dates|date=April 2012}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox person
|name        = Gordon Percy Olley
|image       = 
|caption     = 
|alt         = 
|birth_date  = {{Birth date|1893|04|29|df=yes}}
|birth_place = [[Harleston, Norfolk|Harleston]], [[Norfolk]], [[England]] 
|death_date  = {{Death date and age|1958|03|18|1893|04|29|df=yes}}
|death_place = [[Wimbledon, London|Wimbledon]], [[London]], England
|other_names = 
|known_for   = First World War flying ace
|occupation  = [[Aviation]] and Company Director
|nationality = [[United Kingdom]]
|awards      = [[Military Medal]]
}}

Flying Officer '''Gordon Percy Olley''' [[Military Medal|MM]] (29 April 1893 – 18 March 1958) was a [[First World War]] flying ace who later formed his own airline, [[Morton Air Services#Olley Air Service|Olley Air Services]].<ref name="Aerodrome" /> He was the first pilot to fly a million miles in total.<ref name="News on civil aviation">{{cite magazine |date=28 March 1958 |title=News on civil aviation |magazine=Flight |volume=73 |issue=2566 |page=433 |url=http://www.flightglobal.com/pdfarchive/view/1958/1958%20-%200417.html |format=[[PDF]] archived at [[Flightglobal.com]] |accessdate=16 November 2009}}</ref><ref name="Scholarship" />

==Early years==
Olley was born in Harleston, Norfolk on 29 April 1893.<ref name="Aerodrome" /> In the 1901 Census Olley, aged 7, is described as living at 161, Gloucester Road, Bristol with his parents George and Eliza Olley and a brother and sister.<ref name="Census1901" /> His father is described as a Tobacconist and Hairdresser.<ref name="Census1901" /> In the 1911 Census Olley is still living at the same address, now aged 17, and described as an Apprentice in a wholesale clothing warehouse. In 1912 he became a "motor salesman" at London's [[Selfridges]] department store.<ref name="ServiceRecord" />

==Military aviator==
Olley joined the [[Queen Victoria's Rifles]] in August 1914.<ref name="Aerodrome" /><ref name="ServiceRecord" /> He later transferred to the [[Royal Fusiliers]], rising to the rank of [[sergeant|serjeant]], before being posted to the [[Royal Flying Corps]] as an [[Air Mechanic 2nd Class|Air Mechanic Second Class]], going to France with them on 17 June 1915.<ref name="Aerodrome" /><ref name="MIC" /> At first he was a [[despatch rider]], and then he became an [[air observer]] with [[No. 1 Squadron RAF|No. 1 Squadron]].<ref name="Aerodrome" />

After training as a pilot he rejoined the squadron in 1917 to fly [[Nieuport 11|Nieuport scouts]].<ref name="Aerodrome" /> In September 1917 he was awarded the [[Military Medal]] for bravery in the field.<ref name="mmedal" /> He was then commissioned as a temporary [[second lieutenant]] on the [[General List]],<ref name="comm" /> and appointed a flying officer in the [[Royal Flying Corps]] on 28 January 1918.<ref name="app" /> He transferred to the [[Royal Air Force]] on its formation on 1 April 1918 and was promoted to [[first lieutenant|lieutenant]].<ref name="ServiceRecord" />

After the war he was transferred to Unemployed List on 22 June 1919.<ref name="unemployed" />

His service record states that in addition to his MM he was [[Mentioned in Despatches]] "for valuable services" during the war.<ref name="ServiceRecord" /> During operations with 1 Squadron he is credited with ten aerial victories, comprising 3 destroyed, 5 ( and 2 shared) 'out of control'.<ref>'Above the Trenches'; Shores, Franks & Guest, page 294</ref>

He rejoined the RAF in 1923 when he was commissioned as a [[flying officer]] on probation in Class A of the [[Reserve of Air Force Officers]] on 4 December,<ref name="reserve" /> he was confirmed in that rank on 4 June 1924.<ref name="conf" /> He transferred to Class C of the reserve on 4 December 1932,<ref name="ClassC" /> and relinquished the commission on 4 December 1936, and was permitted to retain his rank.<ref name="relinquish" />

==Civil aviator==
[[Image:DH.104 Dove 1B G-AJBI Olley RWY 09.07.54 edited-2.jpg|thumb|right|Olley Air Services [[De Havilland Dove]] in 1954]]
After leaving the Royal Air Force he worked as a pilot for Handley Page Air Transport, [[Imperial Airways]] and [[KLM]]. In 1931, he became the world's first pilot to log one million miles.<ref name="News on civil aviation"/> Leaving Imperial, he started his own airline, [[Olley Air Services]], in 1934. The firm originally operated from its base at [[Croydon Airport]] as a charter airline. Olley Air Services eventually was part of a group of airlines that included [[Blackpool & West Coast Air Services]], [[Channel Air Ferries]] and [[Isle of Man Air Services]]. After the [[Second World War]] the airline resumed services from Croydon as both a charter airline and a scheduled service before being sold to [[Morton Air Services]] in 1953.

Olley died in Wimbledon on 18 March 1958.<ref name="Aerodrome" /> His obituary reported that not one of his 40,000 passengers suffered a casualty.<ref name="News on civil aviation"/>

==References==

===Notes===
{{reflist|refs=
<ref name="Aerodrome">{{cite web | title=Gordon Percy Olley  | url=http://www.theaerodrome.com/aces/england/olley.php | work= | publisher=theaerodrome.com | date= | accessdate=14 November 2009}}
</ref>

<ref name="mmedal">{{LondonGazette |issue=30287 |date=17 September 1917 |startpage=9610 |supp=x |accessdate=14 November 2009}}</ref>

<ref name="Scholarship">{{cite web | title=Captain Gordon P Olley Scholarship in Aviation | url=http://acserv.admin.utas.edu.au/rules/scholarships/rule104.html | work= | publisher=University of Tasmania | date= 5 September 2007 | accessdate=14 November 2009}}
</ref>

<ref name="Census1901">[[Census in the United Kingdom|1901 Census of Bristol]], [http://www.nationalarchives.gov.uk/catalogue/externalrequest.asp?requestreference=RG13/2399 RG 13/2399], Folio 75, Page 23, Gordon P. Olley, 161, Gloucester Road, Bristol.  [[The National Archives (United Kingdom)|The National Archives]].</ref>

<ref name="comm">{{LondonGazette |issue=30535|date=19 February 1918|startpage=2284|supp=x |accessdate=16 November 2009}}</ref>

<ref name="app">{{LondonGazette |issue=30541|date=22 February 1918|startpage=2442|supp=x |accessdate=16 November 2009}}</ref>

<ref name="unemployed">{{LondonGazette |issue=31449|date=11 July 1919|startpage=8855|accessdate=16 November 2009}}</ref>

<ref name="reserve">{{LondonGazette |issue=32885|date=22 December 1923|startpage=8452|accessdate=16 November 2009}}</ref>

<ref name="conf">{{LondonGazette |issue=32947|date=17 June 1924|startpage=4747|accessdate=16 November 2009}}</ref>

<ref name="ClassC">{{LondonGazette |issue=33889|date=6 December 1932|startpage=7749|accessdate=16 November 2009}}</ref>

<ref name="relinquish">{{LondonGazette |issue=34363|date=26 January 1937|startpage=561|accessdate=16 November 2009}}</ref>

<ref name="ServiceRecord">{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=8252459|title=RAF officers' service records 1918 - 1919—Image details—Olley, Gordon Percy|work=DocumentsOnline|publisher=[[The National Archives (United Kingdom)|The National Archives]]|format=fee usually required to view full pdf of original service record|accessdate=16 November 2009}}</ref>

<ref name="MIC">{{cite web|url=http://www.nationalarchives.gov.uk/documentsonline/details-result.asp?Edoc_Id=4647699|title=WW1 Campaign Medals—Image details—Medal card of Olley, Gordon P|work=DocumentsOnline|publisher=[[The National Archives (United Kingdom)|The National Archives]]|format=fee usually required to view full pdf of original medal index card|accessdate=16 November 2009}}</ref>
}}

{{DEFAULTSORT:Olley, Gordon}}
[[Category:1893 births]]
[[Category:1958 deaths]]
[[Category:British World War I flying aces]]
[[Category:English aviators]]
[[Category:Royal Flying Corps soldiers]]
[[Category:Royal Flying Corps officers]]
[[Category:Royal Air Force officers]]
[[Category:People from Redenhall with Harleston]]
[[Category:Recipients of the Military Medal]]
[[Category:Royal Fusiliers soldiers]]