{{Infobox book 
| name          = Abbé Jules
| title_orig    = ''L'Abbé Jules''
| translator    = 
| image         = MirbeauAbbeJules.jpg
| image_size    = 180px
| caption = 
| author        = [[Octave Mirbeau]]
| illustrator   = 
| cover_artist  = 
| country       = France
| language      = French
| series        = 
| subject       = revolt and madness of a Catholic priest
| genre         = Novel
| publisher     = Ollendorff
| release_date  = April 1888
| oclc = 
}}

'''''L'Abbé Jules''''' ('''''Abbé Jules''''') is a novel written by the French journalist, novelist and playwright [[Octave Mirbeau]], and published by Ollendorff in 1888.

== Plot summary ==

After reading [[Fyodor Dostoevsky|Dostoïevsky]], Mirbeau plumbs the depths of psychology to describe a Catholic priest, [[Jules Dervelle]], whose body and mind are rebelling against social oppression and the corruption of the [[Catholic Church]].

An indictment of the dreary materialism of provincial French society, where life is governed by cupidity and closed-mindedness, Octave Mirbeau’s 1888 novel, ''L'Abbé Jules'' also offers an indictment of the repressive institutions of family and religion. Object of his neighbors’ fearful curiosity, the novel's eponymous hero, [[Jules Dervelle]], constitutes, for the author, a vehicle for exploring the mysteries of the human psyche, the abuses of religion, and the human longing for the transcendental and the sacred.

Returning to his native village of Viantais after a six-year absence in [[Paris]], Jules revolutionizes his countrymen with his scandalous behavior and unorthodox religious views. Consenting to tutor his young nephew, Albert Dervelle – whom [[Octave Mirbeau|Mirbeau]] uses as an uncorrupted and innocent narrator – [[Jules Dervelle|Jules]] exposes his ideas on sexuality, education, and man’s « quest for an ideal ».<ref>[[Pierre Michel]], Introduction, ''L'Abbé Jules'', in [[Octave Mirbeau]], ''Œuvre romanesque'', Buchet/Chastel, 2000, vol. 2.</ref> 
[[File:L'abbé Jules, vu par Hermann-Paul (1904).jpg|thumb|right|220px|[[Hermann-Paul]], ''L'Abbé Jules'', 1904]]
Retrospective narrative allows Mirbeau to recount [[Jules Dervelle|Jules]]'s past, his introduction into the priesthood, and the scandalous behavior resulting in his subsequent exile to a remote parish. After his repatriation in Viantais, Jules installs himself in an overgrown country estate, where he delights in the unspoiled simplicity of nature. Wishing to instill in Albert the artlessness of animals, Jules instructs his young charge to throw away his books. He advises Albert that it is easier to « fabricate a [[Jesus]] or [[Mohammed]] » than it is to dismantle the adulterated social being that each individual has become so that can return to the original purity of his status as a “Nothing.”

[[Jules Dervelle|Jules]] is a self-contradictory and self-loathing character. He is a bibliomaniac who despises the artificiality of the knowledge found in books ; when he comes to finagle from [[Père Pamphile]] – an old Trinitarian monk, who is both a double and the opposite of Jules – money he needs for his library, Pamphile indignantly refuses. He is an enemy of Catholicism, but he yearns for an experience of the divine. 
[[File:L'abbé Jules et le père Pamphile, par Hermann-Paul.jpg|thumb|right|220px|[[Hermann-Paul]], Jules and Pamphile, 1904]]
Through [[Jules Dervelle|Jules]] and [[Père Pamphile|Pamphile]], two rich and complex characters, the author elaborates his evolving views on the social evils that pervert man's instincts, artistic sensibilities, and spiritual yearnings for the absolute.

Robert Ziegler : « For the most part, the message of Mirbeau's novel is a negative one, aimed at exposing the imposture perpetuated by doctors, judges, educators and priests, demantling the symbolic systems that culture creates. Mirbeau's text designates  [[literature]] as repository of meaning. »

==References==
{{Reflist}}

== English translation ==

* ''Abbé Jules'', Sawtry, Dedalus, « Empire of the Senses », 1996, 232 pages.Translated by Nicolette Simborowski.

== Bibliography ==

* Yannick Lemarié, « Lazare en Octavie : le roman du mort vivant », ''[[Cahiers Octave Mirbeau]]'', n° 17, 2010, 51-67.
* Robert Ziegler, « Birth and the book : The Incunabulum in [[Octave Mirbeau]]'s ''L'Abbé Jules'' », ''Dalhousie french studies'', n° 36, fall 1996, 100-112.

== External links ==

* {{fr icon}} {{cite web|url= http://www.leboucher.com/pdf/mirbeau/jules.pdf |title=Octave Mirbeau, ''L’Abbé Jules'' }}&nbsp;{{small|(1.06&nbsp;MB)}}.
* {{fr icon}} [[Pierre Michel]], [http://mirbeau.asso.fr/darticlesfrancais/PM-preface%20%20Abbe%20Jules.pdf Foreword, Éditions du Boucher, 2003].
* {{fr icon}} [[Pierre Michel]], [http://www.scribd.com/doc/73443728/Pierre-Michel-preface-de-L-Abbe-Jules Foreword, L'Âge d'Homme, 2010].
* {{fr icon}} [http://www.litteratureaudio.com/livre-audio-gratuit-mp3/mirbeau-octave-labbe-jules.html/ ''L'Abbé Jules'', audio version] [[Image:Speaker Icon.svg|20px]]
* [http://membres.lycos.fr/magnadea/darticles%20etrangers/Ziegler-cinerarium.pdf Robert Ziegler, « Octave Mirbeau's cinerarium-novel : ''L'Abbé Jules'' »].

{{DEFAULTSORT:Abbe Jules}}
[[Category:1888 novels]]
[[Category:Novels by Octave Mirbeau]]
[[Category:Decadent literature]]