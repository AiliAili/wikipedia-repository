{{good article}}
{{Infobox television episode
| title = Badlaa
| image = [[Image:BadlaaXFiles.jpg|250px|Badlaa|alt=A man who is missing his legs rests on a wheeled cart.]]
| caption = The Indian beggar, the episode's main antagonist. The character was played by noted stuntman [[Deep Roy]].
| series = [[The X-Files]]
| season = [[The X-Files (season 8)|8]]
| episode = 10
| airdate =January 21, 2001
| production = 8ABX12
| writer = [[John Shiban]]
| director = [[Tony Wharmby]]
| length = 44 minutes
| guests = 
* Tony Adelman as Trevor's Father
* Jane Daly as Mrs. Holt
* Ruth de Sosa as Quinton's Mother
* [[Bill Dow]] as Charles Burks
* Jacob Franchek as Red-Headed Kid
* Andy Hubbell as Quinton's Father
* Christopher Huston as Mr. Burrard
* Kiran Rao as Customs Agent
* Calvin Remsberg as Hugh Potocki
* [[Deep Roy]] as Beggar Man
* Mimi Savage as Teacher
* [[Maura Soden]] as Trevor's Mother
* Winston Story as Bellboy
* Jordan Blake Warkol as Quinton
* [[Michael Welch (actor)|Michael Welch]] as Trevor<ref name=cast>{{cite web|title=The X-Files - "Badlaa"|url=http://www.thexfiles.com/episodes/season8/8x10.html|archiveurl=https://web.archive.org/web/20020207004630/http://www.thexfiles.com/episodes/season8/8x10.html|archivedate=7 February 2002|publisher=[[Fox Broadcasting Company]]|work=''TheXFiles.com''|accessdate=27 April 2012|date=February 2002}}</ref>
| prev = [[Salvage (The X-Files)|Salvage]]
| next = [[The Gift (The X-Files)|The Gift]]
| episode_list = [[List of The X-Files episodes|List of ''The X-Files'' episodes]]
}}

"'''Badlaa'''" is the tenth episode of the [[The X-Files (season 8)|eighth season]] of the American [[science fiction on television|science fiction]] television series ''[[The X-Files]]''. It premiered on the [[Fox Broadcasting Company|Fox network]] on {{nowrap|January 21, 2001}}. The episode was written by [[John Shiban]] and directed by [[Tony Wharmby]]. "Badlaa" is a [[List of Monster-of-the-Week characters in The X-Files|"Monster-of-the-Week"]] story, unconnected to the series' wider [[Mythology of The X-Files|mythology]]. The episode received a [[Nielsen rating]] of 7.3 and was viewed by 11.8 million viewers. Overall, the episode received mostly negative reviews from critics.

The series centers on [[Federal Bureau of Investigation|FBI]] special agents [[Dana Scully]] ([[Gillian Anderson]]) and her new partner [[John Doggett]] ([[Robert Patrick]])—following the alien abduction of her former partner, [[Fox Mulder]] ([[David Duchovny]])—who work on cases linked to the paranormal, called [[X-File]]s. When a mystic smuggles himself out of [[India]], Scully and Doggett give chase as his murderous spree starts terrorising two families in suburban Washington, D.C. But Scully soon comes upon a crisis of faith when she realises how dissimilar her techniques are from those of Mulder, even as she tries to be the believer.

"Badlaa" was inspired by stories of Indian [[fakir]]s as well as the idea of someone asking for money actually being "a bad guy." Gurdeep Roy, a noted stuntman better known as [[Deep Roy]], was chosen to play the part of the antagonistic beggar. The episode's title means "retort" or "revenge" in [[Hindi]].

==Plot==
At the [[Chhatrapati Shivaji International Airport|Sahar International Airport]] in [[Mumbai|Mumbai, India]], an American businessman dismissively makes his way past a [[paraplegic]] beggar. Later, while using the airport's toilet, the businessman is pulled out of the stall violently by the beggar that he passed earlier. Later, the man checks into a [[Washington, D.C.]] hotel and sits down on his bed. Soon, blood streams out of his bodily orifices.

[[Dana Scully]] ([[Gillian Anderson]]) arrives late to the crime scene and John Doggett ([[Robert Patrick]]) tells her that the man's blood all drained abruptly in the hotel. A child’s bloody print is found, but Scully doesn’t believe that a child did this. Meanwhile, the beggar, somehow disguised as an ordinary looking Caucasian man, applies for a janitorial job at a [[Cheverly, Maryland]] elementary school. In the morgue, Scully describes the massive stomach damage done to the body which leads Doggett to the idea of drugs being forcibly cut out of him. However, the man showed no sign of drugs in the blood tests and Scully tells Doggett that his time of death was 24 to 36 hours prior, long before he left India. Due to a discrepancy in weight, she begins to believe that there was a passenger in the corpse.

Quinton, a student at the school mentioned before, calls his father after he sees the legless beggar man in his room at night. His father tells him that it was his imagination, goes back downstairs, but then screams. Quinton rushes down to find his father dead and his eyes turned red with blood. Doggett and Scully investigate this latest death after the police tell them about the strange man the boy saw. While discussing the lack of any damage to the body except the broken blood vessels in the eyes, Scully comes to the conclusion that the man is still inside the latest victim. She rushes to the morgue and finds the boy’s father with a distended belly. She cuts into him and then sees a hand emerge from the scalpel incision. After being temporarily knocked over, she follows a bloody trail, opens a door at the end of the trail, and finds no one there. Unbeknownst to her, however, the beggar is watching her, unnoticed.

At the school, the principal tells the legless man’s janitor guise that she was very worried when he did not show up that morning. Trevor, a bully who had earlier tormented Quinton, sees partially through the beggar's forms for a moment. Trevor later shows up at Quinton’s home to say he is sorry and says he thinks he knows who killed his father.

Scully and Doggett consult Chuck Burks, an old friend of [[Fox Mulder]]'s ([[David Duchovny]]), who tells the two that [[Siddhi]] mystics could do the things Scully described; the mystics have powers of the mind and can alter people’s perceptions of reality. Scully theorizes that a mystic is acting out of revenge since an American plant inadvertently released a gas cloud that killed 118 people in Vishi, outside of Mumbai. One of the victims was the 11-year-old son of a holy man of the beggar caste.

Trevor runs home after hearing the squeaking wheels and goes outside after encountering his mother. She follows him outside to find him face-down in the middle of a pool. She dives down to get him but his form turns into the legless man. At the scene of the crime, the real Trevor tells Scully that it was the "little man" who killed his mother. Later, Quinton and Trevor, after realizing that the janitor is actually the beggar, hunt the legless man in the school. Eventually, the beggar takes the form of Trevor. At that moment, Scully enters the school and opens fire, wounding the beggar and reverting him back to his true form. Two weeks later in Sahar International Airport, the beggar, unharmed, watches another obese American man pass by.<ref>{{cite web|title=Badlaa|url=http://www.bbc.co.uk/cult/xfiles/episodeguide/eight/page10.shtml|work=BBC Cult|publisher=BBC|accessdate=16 January 2012}}</ref>

==Production==
[[File:Fakir.jpg|The episode was inspired by stories of Indian [[fakir]]s.|200px|thumb|left]]
"Badlaa" was written by [[John Shiban]] and inspired by stories of Indian [[fakir]]s. In fact, the episode's title means "to retort" or "to revenge" in [[Urdu]].<ref>{{cite web|title=English to Urdu Dictionary - Revenge|url=http://hamariweb.com/dictionaries/revenge_urdu-meanings.aspx|publisher=Hamari Web|accessdate=29 May 2012}}</ref> Further inspiration came "from a little bit of desperation" according to Shiban. He later recalled that he was walking through the Vancouver airport and suddenly had the thought, "What if someone who came up to me and asked me for money was actually a bad guy."<ref name="thetruth"/> Shiban later noted that his early drafts of the episode featured the antagonist with a different power. He explained, "My original idea was a beggar with no legs who can actually shrink himself and climb inside your ear, and Chris Carter–and this is why he's Chris Carter–said 'No, no, no! I know what's even better.'"<ref name="Bigbook">Hurwitz and Knowles, p. 189</ref> Shiban later said that "... one thing about this episode that I'm sort of proud is that people often have told me that it is the most disgusting thought that they ever had, that this little man would actually enter your body and travel around inside you."<ref name="thetruth"/>

The scenes featuring the Indian airport were filmed at a cruise line terminal in [[Long Beach, California]]. Ilt Jones, the location manager for the series, felt that the "dated feel" of the terminal added to the scene. He noted, "if you look at newsreel footage of India, they always have old English cars from the sixties, the cruise line terminal in Long Beach was perfect."<ref name="thetruth"/>

Casting director Rick Millikan was tasked with finding a suitable actor to play the part of the beggar. Millikan's only instructions were to look for "a small all-Indian man with no legs."<ref name="thetruth"/> Eventually, Gurdeep Roy, better known as [[Deep Roy]] was chosen to play the part. Deep Roy was a noted stunt man who had notably played Droopy McCool of the [[Max Rebo Band]] in ''[[Return of the Jedi]]''. Deep Roy, however, is not an amputee and so a cart with a false bottom was created. Anytime there was a scene where the beggar had to move, [[chroma key|blue screen technology]] was used to add the background in during post-production. The cart featured a distinct "squeak" that Paul Rabwin described as "creepy".<ref name="thetruth"/> He noted, "There was a squeak that had to let us know that it was him. It had to scare us [...] Finally we came up with what we thought was just the right squeak and John [Shiban] said 'Okay, that's the one.'"<ref name="thetruth">{{cite video |people=[[Chris Carter (screenwriter)|Carter, Chris]] |date=2000 |title=The Truth Behind Season 8 |medium= DVD |location=''[[The X-Files (season 8)|The X-Files: The Complete Eighth Season]]''|publisher=[[Fox Broadcasting Company|Fox Home Entertainment]]|display-authors=etal}}</ref>

Producer [[Paul Rabwin]] was displeased with the final episode, noting, "'Badlaa' was the one episode I did not like the most [...] I think if I had done it different, I would have had John Shiban change the method of transportation. I don't think it ever worked on any level for me. It was just weird and creepy, but I think the whole idea was distasteful to me." He later bluntly concluded that "it's the only episode that I kind of wish we hadn't done."<ref name="locations">Fraga, p. 173</ref>

==Reception==
"Badlaa" first aired on Fox on 21 January 2001.<ref name="BBCdate"/> The episode earned a [[Nielsen ratings|Nielsen household rating]] of 7.3, meaning that it was seen by 7.3% of the nation's estimated households.<ref name=ratinggood/> The episode was viewed by 7.46 million households<ref name=ratinggood>{{cite journal|last=Associated Press|title=Prime-time Nielsen ratings|journal=Associated Press Archives|date=23 January 2001|accessdate=7 March 2012}}</ref>{{#tag:ref|At the time of airing, the estimated number of households was 102.2 million.<ref name=ratinggood/> Thus, 7.3 percent of 102.2 million is 7.46 million households.|group="nb"}} and 11.8 million viewers.<ref>{{cite news|last=Kissell|first=Rick|title=Peacock mines gold in Globes' Nielsens|url=http://www.lexisnexis.com.www2.lib.ku.edu:2048/lnacui2api/api/version1/getDocCui?lni=4266-WSY0-0006-00WK&csi=140595&hl=t&hv=t&hnsd=f&hns=t&hgn=t&oc=00240&perma=true|accessdate=29 November 2012|newspaper=[[Variety (magazine)|Variety]]|publisher=[[Penske Business Media]]|date=23 January 2001}}</ref> The episode ranked as the 50th most-watched episode for the week ending January 21.<ref name=ratinggood/> The episode subsequently debuted in the United Kingdom on the [[BBC Two]] on May 12, 2002.<ref name="BBCdate">{{cite AV media notes |title=The X-Files: The Complete Eighth Season |titlelink=The X-Files (season 8) |year=2000–2001 |others=[[Kim Manners]], et al  |publisher=[[Fox Broadcasting Corporation|Fox]] }}</ref> Fox promoted the episode with the tagline "Imagine a man who can squeeze into a shoebox... a suitcase... or a victim."<ref>{{Cite sign |title=Badlaa|year=2001 |type=Promotional Flyer |publisher=[[Fox Broadcasting Company]] |location=[[Los Angeles, California]]|url=http://i550.photobucket.com/albums/ii421/maurisap/xfiles%20forum/143squeezeinshoebox.jpg}}</ref>

Critical reception to the episode was mostly negative. [[Television Without Pity]] writer Jessica Morgan rated the episode a "C" and criticized the episode's plot holes, such as how the beggar escaped back to India after being shot by Scully.<ref name="TWP">{{cite web|url=http://www.televisionwithoutpity.com/show/the-xfiles/badlaa.php?page=12 |title=Badlaa |work=[[Television Without Pity]] |publisher=[[NBCUniversal]] |first=Jessica |last=Morgan |date=21 January 2001 |accessdate=5 January 2012 }}{{dead link|date=October 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> [[Robert Shearman]] and [[Lars Pearson]], in their book ''Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen'', rated the episode two stars out of five. The two noted that the episode was "best" when "it's at its most tasteless", citing the beggar "crawling up the bottom of an obese man" as "pretty tasteless". Shearman and Pearson, however, noted that it suffered from the fact that "it doesn't have the courage of its convictions".<ref name="shear">Shearman and Pearson, p. 237</ref>

Todd VanDerWerff of ''[[The A.V. Club]]'' awarded the episode a "C+", calling it "a messy episode".<ref name=avclub/> He argued that it is an example of "magnificently bad television".<ref name=avclub/> This in turn, makes it somewhat entertaining; VanDerWerff argued that he would "rather watch this episode several times than I would some of those [[The X-Files (season 7)|season seven]] outings where everybody seemed like they would rather be just about anywhere else."<ref name=avclub/> However, he did compliment the episode's gross-out scares, noting that the beginning was "a pretty great [[cold open]]".<ref name=avclub>{{cite web|last=VanDerWerff|first=Todd|title='Salvage'/'Badlaa' {{!}} The X-Files/Millennium|url=http://www.avclub.com/articles/salvagebadlaa,105076/|work=[[The A.V. Club]]|publisher=[[The Onion]]|accessdate=November 2, 2013|date=November 2, 2013}}</ref> Tom Kessenich, in his book ''Examinations'', was extremely critical of the episode. Referring to it as the series' "[[nadir]]", he ridiculed the plot and sarcastically labeled the main villain "Butt Munch".<ref name="kess">Kessenich, p. 146</ref> Paula Vitaris from ''[[Cinefantastique]]'' gave the episode a negative review and awarded it one star out of four.<ref name=cinepaula>{{cite journal|last=Vitaris|first=Paula|title=The X-Files Season Eight Episode Guide|journal=[[Cinefantastique]]|date=April 2002|volume=34|issue=2|accessdate=29 March 2012|pages=42–49}}</ref> Vitaris, sardonically referring to the episode as "[[Xenophobia|The X(enophobic)-Files]]", noted that while "the butt-crawler is new, the plot is pure "X-Files generic Monster-of-the-Week."<ref name=cinepaula/> Matt Hurwitz and Chris Knowles noted in their book ''The Complete X-Files'' that the episode soon became known as the "'Butt Genie' episode" among fans.<ref name="Bigbook"/>

Despite the negativity, several reviews wrote positively of the episode's antagonistic beggar. Both ''[[TV Guide]]'' and [[UGO Networks]] listed him amongst the greatest monster-of-the-week characters in ''The X-Files''.<ref name="UGO">{{Cite web|url=http://www.ugo.com/movies/top-xfiles-monsters-beggar |title=The Beggar - Top X-Files Monsters |date=21 July 2008 |accessdate=1 March 2012 |work=[[UGO Networks]] |publisher=[[IGN Entertainment]] |deadurl=yes |archiveurl=https://web.archive.org/web/20110830123021/http://www.ugo.com:80/movies/top-xfiles-monsters-beggar |archivedate=30 August 2011 |df= }}</ref><ref name="TVGuide">{{cite web |url=http://www.tvguide.com/PhotoGallery/Scariest-X-Files-Monsters-1885#5462 |title=X Files Scariest Monsters Pictures, Milagro Photos - Photo Gallery: The Scariest X-Files Monsters |accessdate=1 March 2012 |work=[[TV Guide]]|publisher=United Video Satellite Group}}</ref> The UGO review, in particular, noted that the character was "One of the series' more blatant allegories [...], as a legless Indian Mystic [...] literally climbs into his victims to travel where he will. [...] Scully and Doggett investigate the bloody goings-on [...] and a gut-wrenching climax, though not entirely successful, still opens up some thorny issues over how we view weakness, deformity, race, and 'otherness.'<ref name="UGO"/>

== Notes ==
<references group="nb" />

==References==
;Footnotes
{{Reflist|2}}

;Bibliography
*{{cite book | year=2010 | first1=Erica |last1=Fraga | title=LAX-Files: Behind the Scenes with the Los Angeles Cast and Crew|publisher=CreateSpace|isbn=9781451503418}}
*{{Cite book |title=The Complete X-Files |first1=Matt |last1=Hurwitz |first2=Chris |last2=Knowles |publisher=Insight Editions |year=2008 |isbn=1933784806 }}
* {{cite book | last= Kessenich |first= Tom | title = Examination: An Unauthorized Look at Seasons 6–9 of the X-Files  | publisher = [[Trafford Publishing]] | year = 2002 | isbn = 1553698126 }}
*{{cite book | year=2009 | first1=Robert |last1=Shearman |first2=Lars |last2=Pearson | title=Wanting to Believe: A Critical Guide to The X-Files, Millennium & The Lone Gunmen|publisher=Mad Norwegian Press|isbn=097594469X}}

== External links ==
{{wikiquote|The_X-Files|TXF Season 8}}
*{{imdb episode|0751082|Badlaa}}
*[http://www.tv.com/shows/the-xfiles/badlaa-12442/ "Badlaa"] at [[TV.com]]

{{TXF episodes|8}}

[[Category:2001 television episodes]]
[[Category:Maryland in fiction]]
[[Category:Mumbai in fiction]]
[[Category:Revenge in fiction]]
[[Category:The X-Files (season 8) episodes]]