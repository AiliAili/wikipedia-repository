{{DISPLAYTITLE:Awakening (''Star Trek: Enterprise'')}}
{{Infobox television episode
| title = Awakening
| image = 
| caption = 
| series = [[Star Trek: Enterprise]]
| season = 4
| episode = 8
| production = 408
| airdate = {{Start date|2004|11|26}}
| writer = [[André Bormanis]]
| director = [[Roxann Dawson]]
| music = [[Jay Chattaway]]
| guests = 
* [[Robert Foxworth]] - Administrator V'Las
* [[Gary Graham]] - Ambassador [[Soval]]
* [[John Rubinstein]] - Minister Kuvak
* [[Bruce Gray]] - [[Surak]]
* [[Kara Zediker]] - [[T'Pau (Star Trek)|T'Pau]]
* [[Joanna Cassidy]] - T'Les<ref>{{cite news|title=Star Trek: Enterprise Series 4 - 8. Awakening|url=http://www.radiotimes.com/episode/ck2dj/star-trek-enterprise--series-4---8-awakening|accessdate=April 25, 2013|newspaper=Radio Times}}</ref> 
| prev = [[The Forge (Star Trek: Enterprise)|The Forge]]
| next = [[Kir'Shara]]
| episode_list = [[List of Star Trek: Enterprise episodes|List of ''Star Trek: Enterprise'' episodes]]
}}

"'''Awakening'''" is the eighth episode of the fourth season of the American [[science fiction]] television series ''[[Star Trek: Enterprise]]'', and originally aired on November 26, 2004 on [[UPN]]. The script was written by [[André Bormanis]] and the episode was directed by ''[[Star Trek: Voyager]]'' alumni [[Roxann Dawson]]. The episode was the first of the season for both Bormanis and Dawson. The episode is the second of a three-part episode arc which started in "[[The Forge (Star Trek: Enterprise)|The Forge]]" and concludes in "[[Kir'Shara]]".

Set in the 22nd century, the series follows the adventures of the first [[Starfleet]] starship ''[[Enterprise (NX-01)|Enterprise]]'', registration NX-01. In this episode, the Vulcan government seek to make the ''Enterprise'' leave orbit so they can attack a renegade faction of Vulcans, and afterwards the long-standing enemy of the [[Vulcan (Star Trek)|Vulcans]], the [[Andorian]]s. Meanwhile, Captain [[Jonathan Archer]] and Commander [[T'Pol]] have been captured by the Syrrannites, and it is discovered that Archer has the [[Katra (Star Trek)|katra]] of [[Surak]]. He has visions which lead him to find an ancient Vulcan artefact called the "Kir'Shara" as the group come under attack from the Vulcans.

Elements of the plot of the episode were compared by executive producer to the [[Protestant Reformation]] with the [[Vulcan High Command]] representing the [[Catholic Church]]. The producers took care to cast actors in the roles of [[T'Pau (Star Trek)|T'Pau]] and Surak who looked similar to the actors who portrayed those parts in ''[[Star Trek: The Original Series|The Original Series]]''. Nielsen ratings for the first run of the episode saw a decrease from the first part of the trilogy, down to 1.8/3. The critical response was mixed, saying that whilst they were entertained by the episode, there were several elements in the plot which were problematic.

==Plot==
Ambassador [[Soval]] is summoned before Administrator V'Las and the High Council to face punishment over his use of a [[mind meld]]. Since the act is widely considered to be criminal by the Vulcan authorities, Soval is summarily dismissed from the Ambassadorial service. Meanwhile, Captain [[Jonathan Archer|Archer]] and Commander [[T'Pol]] are questioned by the Syrrannites. After a short while, T'Pol is taken to see her mother, T'Les, and the two disagree about the tenets of the group — the Vulcan authorities call them extremists, a term T'Les disagrees with. Soon, Archer begins to see visions of an old Vulcan, and the dissidents determine that he had the [[Vulcan (Star Trek)#Katra|katra]] of [[Surak]] transferred into him via mind meld.

V'Las, now largely unopposed on the Council, becomes increasingly obsessed with decisively ending the Syrrannite threat once and for all. He postpones his plans to bombard the encampment, after delays in convincing ''[[Enterprise (NX-01)|Enterprise]]'' to leave orbit. He contacts [[Starfleet]], and the Admiralty give Commander [[Trip Tucker|Tucker]] direct orders, which he refuses to carry out directly. He attempts, with assistance from Soval, to send a rescue shuttlepod to "The Forge", but they are intercepted by Vulcan patrol vessels. V'Las then finally orders Vulcan warships to directly engage ''Enterprise'', and Soval suggests that they should retreat before they are severely damaged.

A ritual is performed to transfer the katra into the mind of [[T'Pau (Star Trek)|T'Pau]], but the attempt fails. Archer continues to see Surak who informs him that he must find the relic known as the "Kir'Shara". The Vulcan military begin to bombard the complex. Archer, T'Pol, and T'Pau remain behind to search for the relic, and Archer is able to use his knowledge to unlock a door to reveal it. As they exit, T'Pol finds her mother, but she soon dies after being seriously injured in the attack. On ''Enterprise'', Soval reveals that the Vulcans, despite the recent peace accord, are preparing a pre-emptive strike against the [[Andorians]], and Tucker orders an immediate course at [[warp drive|maximum warp]].

==Production==
[[image:Roxann Dawson.JPG|thumb|right|upright|"Awakening" was the tenth and final episode of ''Enterprise'' to be directed by former ''Voyager'' actress Roxann Dawson]]
"Awakening" was the second part of a three-part trilogy of episodes during the fourth season of ''Enterprise'' that were created to deal with the differences between the ''Enterprise''-era [[Vulcan (Star Trek)|Vulcan]]s and those seen in series set later in the timeframe of the franchise. Show runner [[Manny Coto]] summed up these differences saying, "Our Vulcans lie, our Vulcans are monolithic, our Vulcans are not pacifistic."<ref name=tidbits>{{cite web|title=Producers Reveal Tidbits about Season 4|url=http://www.startrek.com/startrek/view/news/article/6193.html|archiveurl=https://web.archive.org/web/20041014015107/http://www.startrek.com/startrek/view/news/article/6193.html|publisher=Star Trek.com|date=July 21, 2004|archivedate=October 14, 2004|accessdate=April 13, 2013}}</ref> He sought to introduce a situation which he compared to the 16th-century [[Protestant Reformation]] and wanted to include a Vulcan character who would effectively be in the role of [[Martin Luther]], while the Vulcan High Command represented the [[Catholic Church]].<ref name=tidbits/>

The episode saw the re-casting of two roles which had previously appeared in episodes of ''[[Star Trek: The Original Series|The Original Series]]''. These were the parts of T'Pau and Surak. T'Pau made her first appearance in "[[Amok Time]]", where she was played by [[Celia Lovsky]]. For "Awakening", Kara Zediker was cast in the role. Zediker had previously appeared in [[24 (season 1)|the first season of ''24'']] as Elizabeth Nash.<ref name=mythos>{{cite web|title=Production Report: "Awakening" Deepens Vulcan Mythos|url=http://www.startrek.com/startrek/view/news/article/7558.html| archiveurl=https://web.archive.org/web/20041009212918/http://www.startrek.com/startrek/view/news/article/7558.html|publisher=Star Trek.com|date=October 8, 2004|archivedate=October 9, 2004|accessdate=April 13, 2013}}</ref> Surak had been played by [[Barry Atwater]] in "[[The Savage Curtain]]", but this role was taken by Bruce Gray for "Awakening". Gray had previously portrayed Admiral Chekote in ''[[Star Trek: The Next Generation|The Next Generation]]'' episode "[[Gambit (Star Trek: The Next Generation)|Gambit]]" and the ''[[Star Trek: Deep Space Nine|Deep Space Nine]]'' episode "[[The Circle (Star Trek: Deep Space Nine)|The Circle]]". For both of these parts, the producers attempted to cast actors who looked similar to the originals.<ref name=mythos/>

Robert Foxworth reprises his role from the first part of the trilogy as Administrator V'Las, and Joanna Casidy had previously portrayed T'Les earlier in the season in the episode "[[Home (Star Trek: Enterprise)|Home]]". John Rubinstein, who appears in "Awakening" as Koval, has previously appeared as a Mazarite earlier in the series in the episode "[[Fallen Hero (Star Trek: Enterprise)|Fallen Hero]]" and had appeared in "[[The 37's]]", an episode of ''Voyager''. Gary Graham returned as Soval, who he has portrayed throughout the series from the pilot episode onwards.<ref name=mythos/> Director [[Roxann Dawson]] has previously portrayed [[B'Elanna Torres]] in ''Voyager'', and "Awakening" marked the tenth episode of the series that she had directed. It was her only episode of season four, and her last on ''Enterprise''. It was also writer [[André Bormanis]]' first episode of the season, who had previously written several episodes of the series as well as ''Voyager''.<ref name=mythos/>

Filming started on September 23, 2004, and concentrated on ''Enterprise'' ship scenes on the standing sets for the first two days. After that production moved to the cave sets, which were dressed with Vulcan artifacts. On the fourth day of production, those artifacts were removed so that the same sets could use used to film the visions that Archer has of Surak. All exterior scenes in the Vulcan desert-like Forge were shot on a soundstage. The final day of filming took place on October 1, when all the scenes set in the Vulcan High Command were filmed. These involved only Foxworth, Graham and Rubinstein as well as a handful of extras.<ref name=mythos/>

==Reception and home media==
"Awakening" was first aired in the United States on [[UPN]] on November 26, 2004. The broadcast saw the episode come in fifth place during the timeslot, with a [[Nielsen rating]] of 1.8/3. This means that it was seen by 1.8 percent of all households, and 3 percent of all of those watching television at the time of the broadcast.<ref name=ratings>{{cite news|title=CBS Tops NBC for Friday Win|url=http://tv.zap2it.com/tveditorial/tve_main/1,1002,272_617%7C92032%7C1%7C,00.html|archiveurl=https://web.archive.org/web/20050318012804/http://tv.zap2it.com/tveditorial/tve_main/1,1002,272_617%7C92032%7C1%7C,00.html|accessdate=April 13, 2013|newspaper=Zap2it|archivedate=March 18, 2005|date=November 27, 2004}}</ref> It gained higher ratings than [[The WB Television Network|The WB]], which aired re-runs of [[What I Like About You (TV series)|''What I Like About You'']] and ''[[Grounded for Life]]'', but was behind the other four major networks with [[NBC]]'s ''[[Dateline NBC|Dateline]]'' winning the hour with ratings of 5.9/11.<ref name=ratings/> The ratings received by ''Enterprise'' continued a downward trend in recent episodes,<ref>{{cite news|title='The Forge' Fails to Ignite Ratings|url=http://www.trektoday.com/news/201104_01.shtml|accessdate=April 13, 2013|newspaper=''TrekNation''|date=November 20, 2004}}</ref> with ratings of 1.9/3 received by the previous episode.<ref>{{cite news|title='CSI: NY' Gives CBS the Lead on Friday|url=http://tv.zap2it.com/tveditorial/tve_main/1,1002,272_617%7C91919%7C1%7C,00.html|archiveurl=https://web.archive.org/web/20051116164713/http://tv.zap2it.com/tveditorial/tve_main/1,1002,272_617%7C91919%7C1%7C,00.html|archivedate=November 16, 2005|date=November 20, 2004|accessdate=April 13, 2013|newspaper=Zap2it}}</ref>

Michelle Erica Green, reviewing the episode for [[TrekNation]] was undecided about whether the main point of the episode where Captain Archer is expected to lead the Vulcans back to their main path of logic was a "wonderfully progressive concept or just regressive Trek in which humans have all the answers".<ref name=treknationreview>{{cite news|last=Green|first=Michelle Erica|title=Awakening|url=http://www.trektoday.com/reviews/enterprise/awakening.shtml|accessdate=April 13, 2013|newspaper=''TrekNation''|date=November 27, 2004}}</ref> She thought that the change in Soval's opinions in this episode was difficult to accept and that the other members of the main cast didn't get a great deal to do in the episode. However, whilst she thought that "Awakening" was a visual improvement over "The Forge", she was reserving judgement until she had seen the third part of the trilogy.<ref name=treknationreview/> Jamahl Epsicokhan at his website "Jammer's Reviews" thought that certain elements of the plot didn't follow logical paths, such as how Archer found the Kir'shara in a few minutes when apparently the Syrrannites had been looking for it for two years even though "it sits in a chamber behind a door that practically announces, 'IMPORTANT RELIC INSIDE'?"<ref name=jammer>{{cite web|last=Epsicokhan|first=Jamahl|title=Star Trek: Enterprise "The Awakening|url=http://www.jammersreviews.com/st-ent/s4/awakening.php|publisher=Jammer's Reviews|accessdate=April 13, 2013}}</ref> However, he called the episode entertaining and gave it a score of three out of four.<ref name=jammer/>

The first home media release of "The Forge" was in the season four DVD box set of ''Enterprise'', originally released in the United States on November 1, 2005.<ref name=dvdrelease>{{cite news|last=Douglass Jr.|first=Todd|title=Star Trek Enterprise - The Complete Fourth Season|url=http://www.dvdtalk.com/reviews/18391/star-trek-enterprise-the-complete-fourth-season/|accessdate=February 3, 2013|newspaper=DVD Talk|date=October 24, 2005}}</ref> The [[Blu-ray]] edition was released on April 1, 2014.<ref>{{cite web|title=Final Season Enterprise Blu-ray Set Available April 1|url=http://www.startrek.com/article/final-season-enterprise-blu-ray-set-available-april-1|publisher=StarTrek.com|date=December 18, 2013|accessdate=October 11, 2014}}</ref>

==References==
{{reflist|30em}}

==External links==
* {{IMDb episode|0572176}}
* {{TV.com episode|star-trek-enterprise/awakening-2-370490}}
{{Memory Alpha|Awakening}}
{{StarTrek.com link|ENT|7548|Awakening}}

{{Star Trek ENT S4}}
{{Star Trek ENT}}
{{Star Trek}}

{{Good article}}

[[Category:Star Trek: Enterprise episodes]]
[[Category:2004 American television episodes]]