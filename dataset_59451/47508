{{Orphan|date=October 2013}}

'''DiAna DiAna''' is an American hairdresser and [[HIV/AIDS]] activist from [[Columbia, South Carolina]].<ref name="Delgado1999">{{cite book|last=Delgado|first=Melvin|title=Social work practice in nontraditional urban settings|year=1999|publisher=Oxford University Press.|location=New York|isbn=9780195112481|page=98}}</ref> Her work in the field of HIV/AIDS and basic sex education was featured in the 1989 documentary film ''[[Diana's Hair Ego]]''.

== Career ==
Troubled about the lack of sex education and HIV prevention within African-American communities, DiAna began distributing condoms free of charge from her salon. When she noticed that the women were reluctant to carry these condoms home with them, she "decided to wrap the condoms up in gift wrap as a means of destigmatizing the process."<ref name="Delgado1999" /> DiAna's efforts soon began to be recognized outside of her network of clients. She gave presentations to church groups,<ref name="Longfellow">{{cite book|last=Longfellow|first=edited by Brenda|title=The perils of pedagogy : the works of John Greyson|publisher=McGill-Queen's University Press|location=Montreal|isbn=9780773541443|author2=MacKenzie, Scott |author3=Waugh, Thomas}}</ref> in local elementary schools,<ref>{{cite book|last=Gever|first=edited by Martha|title=Queer looks : perspectives on lesbian and gay film and video|year=1993|publisher=Routledge|location=New York [etc.]|isbn=9780415907422|author2=Parmar, Pratibha |author3=Greyson, John }}</ref> and engaged high school students to become community leaders.<ref name="Holmlund1997">{{cite book|last=Holmlund|first=Chris|title=Between the sheets, in the streets : queer, lesbian, gay documentary|year=1997|publisher=University of Minnesota Press|location=Minneapolis|isbn=9780816627752|page=42|edition=[Online-Ausg.].}}</ref>
DiAna's activism was instrumental in the development of beauty locations as sites of HIV outreach and education across the United States.<ref name="Delgado1999" />

=== South Carolina AIDS Education Network ===
DiAna founded the South Carolina AIDS Education Network (SCAEN) with Dr. Bambi Gaddist (née Sumpter) in 1987.<ref name="Holmlund1997" />
Working out of DiAna's hair salon, the two women organized film screenings on the topic of HIV/AIDS, safer-sex presentations, "Tupperware"-style sex toy parties, and free condom distribution.<ref>{{cite book|last=Delgado|first=Melvin|title=Social work practice in nontraditional urban settings|year=1999|publisher=Oxford University Press.|location=New York|isbn=9780195112481|page=99}}</ref> SCAEN has been praised for its "creative strategies and nonjudgmental concern" in its grassroots campaign against the spread of HIV.<ref name="Longfellow" /> The group estimates that it has provided one-to-one information on HIV to over 9,000 people in South Carolina.<ref name="Holmlund1997" /> In addition to face-to-face activism, DiAna, Bambi, and other volunteers for SCAEN created informational videos, music, pamphlets, and colouring books to distribute state-wide, accessible to children and adults with low or little reading ability.<ref name="Holmlund1997" />

== References ==
{{Reflist}}

{{DEFAULTSORT:DiAna, DiAna}}
[[Category:HIV/AIDS activists]]
[[Category:Hairdressers]]
[[Category:People from Columbia, South Carolina]]
[[Category:Year of birth missing (living people)]]
[[Category:Living people]]