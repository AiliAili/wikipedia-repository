<!-- Please leave this line alone! -->
{{Infobox journal
| title = Australian and New Zealand Journal of Psychiatry
| cover = 
| editor = Gin S. Malhi
| discipline = 
| former_names = 
| abbreviation = Aust. N. Z. J. Psychiatry
| publisher = [[SAGE Publications]]
| country = 
| frequency = Monthly
| history = 
| openaccess = 
| license = 
| impact = 3.407
| impact-year = 2014
| website = http://anp.sagepub.com/
| link1 = http://anp.sagepub.com/content/current
| link1-name = Current Issue
| link2 = http://anp.sagepub.com/content/by/year
| link2-name = Online Archive of All Issues
| ISSN = 0004-8674
| eISSN = 1440-1614
| OCLC = 605130312
| LCCN = 70012053
}}

'''''Australian and New Zealand Journal of Psychiatry''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes papers in the field of [[Psychiatry]]. The journal's [[Editor-in-Chief|editor]] is Professor Gin S. Malhi. It is currently published by [[SAGE Publications]] on behalf of [[The Royal Australian and New Zealand College of Psychiatrists]].

== Scope ==
''Australian and New Zealand Journal of Psychiatry'' aims to publish original articles which describe research or report opinions of interest to psychiatrists. The journal contains original research, reviews, descriptions of patients and letters to the editor. ''Australian and New Zealand Journal of Psychiatry'' also publishes editorial comments and book reviews. 

== Abstracting and indexing ==
''Australasian and New Zealand Journal of Psychiatry'' is abstracted and indexed in, among other databases:  [[SCOPUS]], and the [[Social Sciences Citation Index]]. According to the ''[[Journal Citation Reports]]'', its 2014 [[impact factor]] is 3.407, ranking it 43 out of 140 journals in the category ‘Psychiatry’.<ref name=WoS>{{cite book |year=2015 |chapter=Journals Ranked by Impact: Psychiatry |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition= Sciences |accessdate=2011-09-30 |work=Web of Science |postscript=.}}</ref> 

== References ==
{{reflist}}

== External links ==
* {{Official website|1=http://anp.sagepub.com/}}
* {{Official website|http://www.ranzcp.org/|RANZCP official website}}

[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Psychiatry journals]]
[[Category:Monthly journals]]