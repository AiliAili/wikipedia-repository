{{Infobox album <!-- See Wikipedia:WikiProject Albums -->
| Name       = Carnival Of Carnage
| Type       = studio
| Artist     = [[Insane Clown Posse]]
| Cover      = ICP_Carnival_of_Carnage.jpg
| Released   = October 18, 1992
| Recorded   = 1991–1992
| Studio       = Miller Midi Productions<br/><small>([[Detroit|Detroit, Michigan]])</small><br/>The Tempermill Studio<br/><small>([[Ferndale, Michigan]])</small>
| Length     = 66:17
| Genre      = [[Horrorcore]], [[midwest hip hop]]
| Label      = [[Psychopathic Records]]
| Producer   = [[Violent J|Joseph Bruce]], [[Mike E. Clark|Mike Clark]], [[Esham]], Chuck Miller
| Last album = 
| This album = '''Carnival of Carnage''' <br />(1992)
| Next album = [[Ringmaster (album)|Ringmaster]]<br />(1994)
}}
'''''Carnival of Carnage''''' is the debut album by American [[hip hop]] group [[Insane Clown Posse]], released on October 18, 1992, by [[Psychopathic Records]]. It is the first Joker Card in the first deck. Recording sessions for the album took place from 1991 to 1992 at Miller Midi Productions and The Tempermill Studio. The album is the first [[Dark Carnival (Insane Clown Posse)#Joker.27s Cards|Joker's Card]] in the group's [[Dark Carnival (Insane Clown Posse)|Dark Carnival]] mythology. The album's lyrics describe the Carnival of Carnage as a representation of the violence that occurs within the [[ghetto]]s, which takes the form of a traveling carnival to enact the same brutality on the [[upper class]].

''Carnival Of Carnage'' was the first album on which Insane Clown Posse collaborated with producer [[Mike E. Clark]], who would work with the group throughout much of their career. It features guest appearances by popular [[Detroit]] rappers [[Esham]] and [[Kid Rock]]. The album features the only recorded appearances of member John Kickjazz, who left the group prior to the album's release. Although the album did not initially sell well, it became eligible for [[Music recording sales certification|gold certification]] by the [[Recording Industry Association of America|RIAA]] in 2010.

==Conception==

===Background===
{{main|Insane Clown Posse}}
[[Violent J|Joseph Bruce]] and [[Shaggy 2 Dope|Joseph Utsler]] formed a hip hop group in 1990.<ref name="BehindthePaint539">{{cite book |last=Bruce |first=Joseph |last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=second |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=539–542 |chapter=Complete Discography }}</ref> Under the stage names Violent J, 2 Dope, and John Kickjazz, the group began performing at local night clubs under the name of their gang, Inner City Posse.<ref name="BehindthePaint539"/> By late 1991, the group had invested more money into production than was covered by returns. They decided that their [[gangsta rap]] style was the cause of the problem: Most emcees at the time used similar styles, making it difficult for Inner City Posse to distinguish itself stylistically.<ref name="BehindthePaint151">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J|last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=second |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=151–185 |chapter=The Dark Carnival }}</ref> Bruce suggested the band instead adapt a style similar to the hallucinatory, surrealistic "[[acid rap]]" of fellow Detroit rapper [[Esham]], in a bid to have Detroit represent acid rap, much as [[Los Angeles, California|Los Angeles]] represented gangsta rap. The group agreed, but not to copying the style of Esham closely. Instead, they suggested using horror-themed lyrics as an emotional outlet for all their negative life experiences. They were also unanimous in deciding not to rap openly about [[Satan]], which Esham often did.<ref name="BehindthePaint151"/>

After the change in musical style, the group decided that it needed a new name. Utsler suggested keeping the "I.C.P." initials to inform the community that Inner City Posse was not defunct, an idea to which the group agreed.<ref name="BehindthePaint151"/> Several names were considered before Bruce recalled his dream of a clown running around in [[Delray, Detroit|Delray]], which became the inspiration for the group's new name [[Insane Clown Posse]]. The other members agreed, deciding that they would take on this new genre and name, and would all don face paint due to the success of their former clown-painted [[promotion (marketing)|hype]] man.<ref name="BehindthePaint151"/>

===Recording===
''Carnival of Carnage'' began recording at Miller Midi Productions in [[Detroit|Detroit, Michigan]] with Chuck Miller producing and mastering the album.<ref name="BehindthePaint151"/> Miller charged the group US$6,000 to produce the songs "Red Neck Hoe," "Psychopathic," "Your Rebel Flag," and part of "Night of the Axe."<ref name="BehindthePaint188">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J |last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=second |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=188–208 |chapter=The Broken Path of a Dream }}</ref> Seeing that they were being overcharged, Alex Abbiss made his first major managerial move by finding another producer, [[Mike E. Clark]].<ref name="BehindthePaint188"/> The group finished recording the album with Clark at The Tempermill Studio in [[Ferndale, Michigan]]. Clark mastered his part of the album at Rythmatic Studio, and continued to work with the group throughout their career.

Original group member John Kickjazz appeared on the songs "Your Rebel Flag," "Psychopathic," "Blacken' Your Eyes," "Wizard of the Hood," "Red Neck Hoe," and "Taste."<ref name="BehindthePaint189">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J |last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=2nd |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=189–208 |chapter=The Broken Path of a Dream }}</ref> "Carnival of Carnage" was originally recorded by [[Esham]] at Hells Doors Studio, but he pronounced "carnage" as "carnicks" and refused to redo it.<ref name="BehindthePaint189"/> The final version of the song was recorded by Joseph Bruce over a reversed recording of the original.<ref name="BehindthePaint189"/>

Awesome Dre was originally going to do a verse on "Taste." While Insane Clown Posse waited in the studio for him to arrive, Esham suggested that he appear on the track instead for the same amount of money, and the group allowed him to record a verse. Esham was paid $500 for his appearance.<ref name="BehindthePaint189"/> [[Kid Rock]] demanded a hundred more than Esham, and was paid $600 to appear on "Is That You?"<ref name="BehindthePaint189"/> He showed up to record the song intoxicated, but re-recorded his vocals and record scratching the following day.<ref name="BehindthePaint189"/>

===Joker's Cards===
''Carnival of Carnage'' is the first [[Dark Carnival (Insane Clown Posse)#Joker's Cards|Joker's Card]] in Insane Clown Posse's [[Dark Carnival (Insane Clown Posse)|Dark Carnival]] [[concept album]] series.<ref name="McIver">{{cite book |last=McIver |first=Joel |title=Nu-metal: The Next Generation of Rock & Punk |year=2002 |publisher=Omnibus Press |isbn=0-7119-9209-6 |page=64 }}</ref> The Dark Carnival is a concept of the [[afterlife]] in which souls are sent to a form of [[limbo]] while waiting to be sent to [[heaven]] or [[hell]] based on their [[Divine judgment|individual actions]]. These concepts are related by Insane Clown Posse in a series of albums called the six [[Joker (playing card)|Joker's Card]]s. Each of the six Joker's Cards relate to a specific character — an entity of the Dark Carnival — that tries to "save the human soul" by showing the wicked inside of one's self.<ref name="BehindthePaint174">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J|last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=second |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=174–185 |chapter=The Dark Carnival }}</ref><ref name="Friedman">{{cite news |title=''Juggalos'' |first=David |last=Friedman |url=http://my.texterity.com/murderdogmagazine/volume16#pg192 |publisher=''Murder Dog'' |date=November 2009 |pages=192–198 }}</ref>

{| style="font-size:80%" align="right" border="0" color="black" width=130px
| 
{{Listen|filename=Ghetto Freak Show.ogg|title="Ghetto Freak Show" (sample)|description="Ghetto Freak Show", from the group's 1992 album ''Carnival of Carnage''.|format=[[Ogg]]}}
|}

This Joker's Card is a representation of the [[ghetto]]s and the violence that occurs within them.<ref name="Apollo">{{cite web |url=http://www.paoracle.com/?archive=77 |title=An Intelligent Look at the Insane Clown Posse |author=Phoebus Apollo |date=2004-01-22 |publisher=phoebus apollo |accessdate=2009-11-25}}</ref><ref name="COC">Insane Clown Posse (1992). ''Carnival of Carnage''. Liner notes. [[Psychopathic Records]]. {{UPC|0731452456229}}</ref> It takes the form of a traveling carnival which releases the same brutality on those who have ignored the inner cities' cries for help.<ref name="COC"/> The Card issues a warning against the upper-class and government's negligence toward the lower classes.<ref name="Apollo"/><ref name="COC"/> The cover of ''Carnival of Carnage'' was drawn by Joseph Utsler, who would later create artwork for the rest of the albums in the Joker's Cards series.<ref name="BehindthePaint539"/>

==Music==

===Samples===
Mike Clark [[music sampling|samples]] [[Johnny "Hammond" Smith]]'s "Big Sur Suite", from Smith's 1974 album ''Higher Ground'', [[Black Sabbath]]'s "The Wizard", from their [[Black Sabbath (album)|1970 debut album]], and the [[Beastie Boys]] "[[Pass the Mic]]" from their 1992 album [[Check Your Head]] in his production of "Never Had It Made."<ref>{{cite web |url=http://www.the-breaks.com/search.php?term=Insane+Clown+Posse&type=6 |title=Insane Clown Posse |publisher=The Breaks |accessdate=7 April 2010}}</ref><ref>http://www.whosampled.com/sample/92624/Insane-Clown-Posse-Never-Had-It-Made-Beastie-Boys-Pass-the-Mic/</ref> Joseph Bruce samples several clips from the film ''[[The Wizard of Oz (1939 film)|The Wizard of Oz]]'' in "[[Wizard of the Hood (song)|Wizard of the Hood]]." The song "Psychopathic" features a sample of "[[More Bounce to the Ounce]]" by [[Zapp (band)|Zapp]] and "''Halloween'' theme" from the [[Halloween (franchise)|Halloween film franchise]]. The song "Redneck Hoe" features a sample from "City, Country, City" by WAR. The song "Taste" samples [[Esham]]'s "Word After Word".

===Lyricism===
Joseph Bruce uses elements of [[political hip hop]] throughout the album. Many of his lyrics were derived from his experiences of growing up in a poor family that was neglected by the government. He and his brother [[Robert Bruce (rapper)|Robert]] used to escape from their impoverished reality by gathering themselves in a forest called "Picker Forest". Joe cites "Picker Forest" as a strong influence on the [[Dark Carnival (Insane Clown Posse)|Dark Carnival]] mythology which began with this album.<ref name="BehindthePaint43">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J|last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=second |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=43–47 |chapter=The Floobs }}</ref> The themes of the Dark Carnival also derived from a dream Bruce had shortly after the group adopted its new name, in which  spirits in a [[traveling carnival]] appeared to him.<ref name="BehindthePaint151"/>

"Red Neck Hoe" and "Your Rebel Flag" stem from the group's anti-[[bigotry]] philosophy, based on various experiences witnessed by Bruce.<ref name="BehindthePaint106">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J|last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=2nd |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=106–119 |chapter=Rude Boy and the Magical Land of Toxic Waste }}</ref> As a teenager, he had briefly lived in Bonnie Doone, [[North Carolina]], a trailer park town just outside [[Fort Bragg (North Carolina)|Fort Bragg]], where his brother Robert had been staying with the [[U.S. Army]]. There, Joseph witnessed firsthand the hatred and open racism directed toward [[African American]] citizens, as well as the minorities serving in the Army, and became disgusted and infuriated with the actions that took place.<ref name="BehindthePaint106"/> "[[Wizard of the Hood (song)|Wizard of the Hood]]" was originally written by Bruce sometime in the late 1980s.<ref name="Bruce-151">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J |last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=2nd |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=151–155 |chapter=Paying Dues }}</ref> The first recorded version of the song appeared on the ''[[Intelligence and Violence]]'' EP under the name "Wizard of Delray."<ref name="Bruce-151"/> The ''Carnival of Carnage'' version is derived from a 1991 recording which appeared on the EP ''[[Dog Beats]]''.<ref name="Bruce-151"/>

==Release==

Just weeks prior to the release of their album, John left the group because he felt that it was "taking up too much of [his] life."<ref name="BehindthePaint189"/> When Bruce and Utsler attempted to call a meeting to talk about the issues, John did not attend.<ref name="BehindthePaint189"/> ''Carnival of Carnage'' was released on October 18, 1992, with distribution within a {{convert|120|mi|km|adj=on}} radius of Detroit.<ref name="BehindthePaint188"/> ''Carnival of Carnage'' sold 17 copies on its release date.<ref name="GOTJ2002">{{cite video |people=Bruce, Joseph |date=2002 |title=ICP seminar from the Gathering of the Juggalos |medium=DVD |publisher=[[Psychopathic Records]] |accessdate=2009-02-04 |time= |id={{UPC|822489991224}} }}</ref> The number would become a reoccurring theme in Insane Clown Posse's work throughout much of the following decade. A condensed [[extended play]] featuring tracks from ''Carnival of Carnage'' was pressed on [[Gramophone record|vinyl]] in hopes that [[Disc jockey|DJ]]s would play the songs in Detroit-area nightclubs.<ref name="BehindthePaint539"/>

==Reception==
{{Album ratings
| rev1 = [[Allmusic]] 
| rev1Score = {{Rating|3|5}}<ref name="AMGHipHop"/>
| rev3 = ''[[Rolling Stone]]'' 
| rev3Score = {{Rating|1|5}}<ref name="RSAlbumGuide"/>
| rev4 = [[SputnikMusic]]
| rev4Score = {{rating|3.2|5}}
}}

Although ''Carnival of Carnage'' was not reviewed at the time of its release, later reviews of the album have been unfavorable. [[Allmusic]] reviewer [[Stephen Thomas Erlewine]] gave the album three out of five stars, comparing the group's performance on the album to "a third-rate [[Beastie Boys]] supported by a cut-rate [[Faith No More]], all tempered with the sensibility that made [[Gwar|GWAR]] cult heroes—only with [...] more sexism and jokes that are supposed to be street, but wind up sounding racist", but stating that the album would appeal to fans of the group.<ref name="AMGHipHop">{{cite book |editor= |others= |title=All Music Guide to Hip-Hop: The Definitive Guide to Rap & Hip-hop |year=2003 |isbn=0-87930-759-5 |publisher=Backbeat Books |pages=229–231 }}</ref> In ''The Great Rock Discography'', Martin Charles Strong gave the album four out of ten stars.<ref name="Strong">{{cite book |last1=Strong |first1=Martin Charles  |title=The Great Rock Discography |edition=7th |year=2004 |publisher=Canongate |isbn=1-84195-615-5 |page=733 |chapter=Insane Clown Posse }}</ref> The album received one star out of five in ''The New Rolling Stone Album Guide'', in which [[Ben Sisario]] panned it, along with the rest of the group's discography as "gangsta-inspired [[wigger|wigga]] posturing".<ref name="RSAlbumGuide"/>

==Legacy==
During a live performance of the song "The Juggla" in 1993, Bruce addressed the audience as ''[[Juggalo]]s'', and the positive response resulted in the group using the word thereafter.<ref name="BehindthePaint231">{{cite book |last=Bruce |first=Joseph |authorlink=Violent J|last2=Echlin |first2=Hobey |editor=Nathan Fostey |title=ICP: Behind the Paint |date=August 2003 |edition=2nd |publisher=Psychopathic Records |location=Royal Oak, Michigan |isbn=0-9741846-0-8 |pages=237–238 |chapter=Ringmaster's Word }}</ref> The word has been the subject of criticism from both Sisario and Erlewine, who suggested the term is similar to the racial slur ''[[List of ethnic slurs#J|jigaboo]].''<ref name="AMGHipHop"/><ref name="RSAlbumGuide">{{cite book |title=The New Rolling Stone Album Guide |editor=Brackett, Nathan |year=2004 |publisher=[[Simon & Schuster]] |isbn=0-7432-0169-8 |pages=405–6 }}</ref> In 1997, [[Twiztid]] released a cover of the song "First Day Out" on the duo's debut album, ''[[Mostasteless]]''.<ref>{{cite web |url=http://www.insaneclownposse.com/music/ |title=Insane Clown Posse Discography |publisher=Psychopathic Records |accessdate=5 June 2010}}</ref> In 1998, the album was reissued by [[Island Records]] without the tracks "Blackin' Your Eyes" and "Night of the Axe."<ref>{{cite web |url={{Allmusic|class=album|id=r268815|pure_url=yes}} |title=Overview for ''Carnival of Carnage'' |first=Stephen Thomas |last=Erlewine |publisher=[[Allmusic]] |accessdate=19 February 2010}}</ref> The original version continues to be sold by [[Psychopathic Records]].<ref>{{cite web|url=http://secure.hatchetgear.com/v3/shop.php?ar=1&it=5&ds=46 |title=CD - ICP - Carnival of Carnage OG |accessdate=2009-02-04 |publisher=[[Psychopathic Records]] |deadurl=yes |archiveurl=https://web.archive.org/web/20090119102408/http://secure.hatchetgear.com:80/v3/shop.php?ar=1&it=5&ds=46 |archivedate=2009-01-19 |df= }}</ref> By 2010, the album had sold well enough to become eligible for [[Music recording sales certification|gold certification]] by the [[Recording Industry Association of America|RIAA]].<ref name="PRNewswire">{{cite web |url=http://www.prnewswire.com/news-releases/fontana-partners-with-psychopathic-records-84586717.html |title=Fontana Partners With Psychopathic Records |publisher=PR Newswire Association LLC |date= February 17, 2010 |accessdate=20 February 2010}}</ref> In the [[Aqua Teen Hunger Force]] episode "Juggalo", Bruce and Utsler appear as themselves during a trial after Master Shake commits suicide. [[George Lowe]] asks "Mr. 2 Dope" to read lyrics from "Blackin' Your Eyes".

==Track listing==
{{tracklist
| headline        = PSY 1004
| extra_column    = Producer(s)
| total_length    = 66:17
| writing_credits = yes
| title1          = Intro
| writer1         = 
| length1         = 1:20
| title2          = Carnival of Carnage
| writer2         = [[Violent J|Joseph Bruce]] and [[Esham|Esham A. Smith]]
| length2         = 2:33
| extra2          = Joseph Bruce and Esham A. Smith
| title3          = The Juggla
| length3         = 4:55
| writer3         = J. Bruce
| extra3          = [[Mike E. Clark|Mike Clark]] and Joseph Bruce
| title4          = First Day Out
| length4         = 4:21
| writer4         = J. Bruce and [[Shaggy 2 Dope|Joseph Utsler]]
| extra4          = Mike Clark and Joseph Bruce
| title5          = Red Neck Hoe
| length5         = 4:50
| writer5         = J. Bruce and Utsler
| extra5          = Chuck Miller and Joseph Bruce
| title6          = Wizard of the Hood
| length6         = 5:24
| writer6         = J. Bruce and Utsler
| extra6          = Chuck Miller and Joseph Bruce
| title7          = Guts on the Ceiling
| length7         = 4:25
| writer7         = J. Bruce
| extra7          = Mike Clark and Joseph Bruce
| title8          = Is That You?
| length8         = 4:34
| writer8         = J. Bruce and R. Ritchie
| extra8          = Mike Clark and R. Ritchie
| note8           = featuring [[Kid Rock]]
| title9          = Night of the Axe
| length9         = 5:00
| writer9         = J. Bruce
| extra9          = Chuck Miller and Joseph Bruce
| title10         = Psychopathic
| length10        = 4:43
| writer10        = J. Bruce
| extra10         = Chuck Miller and Joseph Bruce
| title11         = Blackin' Your Eyes
| length11        = 4:40
| writer11        = J. Bruce and Utsler
| extra11         = Esham A. Smith and Joseph Bruce
| title12         = Never Had it Made
| length12        = 5:45
| writer12        = J. Bruce
| extra12         = Mike Clark, Joseph Bruce, and Esham A. Smith
| title13         = Your Rebel Flag
| length13        = 4:24
| writer13        = J. Bruce and Utsler
| extra13         = Chuck Miller and Joseph Bruce
| title14         = Ghetto Freak Show
| length14        = 4:14
| writer14        = J. Bruce
| extra14         = Esham A. Smith and Joseph Bruce
| title15         = Taste
| length15        = 5:09
| writer15        = J. Bruce, Robert Bruce, Utsler, Nathan Williams, Capitol E., and Smith
| extra15         = Mike Clark and Joseph Bruce
| note15          = featuring [[Robert Bruce (rapper)|Jumpsteady]], Capitol E., Nate The Mack and [[Esham]]
}}

{{tracklist
| collapsed       = yes
| headline        = Vinyl Version
| extra_column    = Producer(s)
| total_length    = 
| writing_credits = yes
| title1          = Carnival of Carnage
| writer1         = Joseph Bruce and Esham A. Smith
| length1         = 2:32
| extra1          = Joseph Bruce and Esham A. Smith
| title2          = Never Had it Made
| length2         = 5:45
| writer2         = J. Bruce
| extra2          = Mike Clark, Joseph Bruce and Esham A. Smith
| title3          = Guts on the Ceiling
| length3         = 4:24
| writer3         = J. Bruce
| extra3          = Mike Clark and Joseph Bruce
| title4          = Taste
| length4         = 5:10
| writer4         = J. Bruce, Robert Bruce, Utsler, Nathan Williams, Capitol E., and Smith
| extra4          = Mike Clark and Joseph Bruce
| note4           = featuring Jumpsteady, Capitol E., Nate The Mack and Esham
| title5          = Psychopathic
| length5         = 4:43
| writer5         = J. Bruce
| extra5          = Chuck Miller and Joseph Bruce
| title6          = Ghetto Freak Show
| length6         = 4:14
| writer6         = J. Bruce
| extra6          = Esham A. Smith and Joseph Bruce
| title7          = Wizard of the Hood
| length7         = 5:24
| writer7         = J. Bruce and Joseph Utsler
| extra7          = Chuck Miller and Joseph Bruce
| title8          = Is That You?
| length8         = 4:34
| writer8         = J. Bruce and R. Ritchie
| extra8          = Mike Clark and R. Ritchie
| note8           = featuring Kid Rock
}}

{{tracklist
| collapsed       = yes
| headline        = Island 524514-2
| extra_column    = Producer(s)
| total_length    = 
| writing_credits = yes
| title1          = Intro
| length1         = 1:20
| title2          = Carnival of Carnage
| writer2         = Joseph Bruce and Esham A. Smith
| length2         = 2:33
| extra2          = Joseph Bruce and Esham A. Smith
| title3          = The Juggla
| length3         = 4:55
| writer3         = J. Bruce
| extra3          = Mike Clark and Joseph Bruce
| title4          = First Day Out
| length4         = 4:21
| writer4         = J. Bruce and Joseph Utsler
| extra4          = Mike Clark and Joseph Bruce
| title5          = Red Neck Hoe
| length5         = 4:50
| writer5         = J. Bruce and Utsler
| extra5          = Chuck Miller and Joseph Bruce
| title6          = Wizard of the Hood
| length6         = 5:24
| writer6         = J. Bruce and Utsler
| extra6          = Chuck Miller and Joseph Bruce
| title7          = Guts on the Ceiling
| length7         = 4:25
| writer7         = J. Bruce
| extra7          = Mike Clark and Joseph Bruce
| title8          = Is That You?
| length8         = 4:34
| writer8         = J. Bruce and R. Ritchie
| extra8          = Mike Clark and R. Ritchie
| note8           = featuring Kid Rock
| title9          = Psychopathic
| length9         = 4:43
| writer9         = J. Bruce
| extra9          = Chuck Miller and Joseph Bruce
| title10         = Never Had it Made
| length10        = 5:45
| writer10        = J. Bruce
| extra10         = Mike Clark, Joseph Bruce and Esham A. Smith
| title11         = Your Rebel Flag
| length11        = 4:24
| writer11        = J. Bruce and Utsler
| extra11         = Chuck Miller and Joseph Bruce
| title12         = Ghetto Freak Show
| length12        = 4:14
| writer12        = J. Bruce
| extra12         = Esham A. Smith and Joseph Bruce
| title13         = Taste
| length13        = 5:09
| writer13        = J. Bruce, Robert Bruce, Utsler, Nathan Williams, Capitol E., and Smith
| extra13         = Mike Clark and Joseph Bruce
| note13          = featuring Jumpsteady, Capitol E., Nate The Mack and Esham
}}

==Personnel==
* [[Violent J]] – vocals, production
* [[Shaggy 2 Dope|2 Dope]] – vocals, scratching
* John Kickjazz – vocals
* Nate The Mack – guest vocals
* [[Kid Rock]] – guest vocals, scratching
* Capitol E – guest vocals
* [[Robert Bruce (rapper)|Jumpsteady]] – guest vocals
* [[Esham]] – guest vocals, production
* [[Mike E. Clark]] – production
* Chuck Miller - production

==References==
{{Reflist}}

{{Insane Clown Posse}}

{{good article}}

{{DEFAULTSORT:Carnival Of Carnage}}
[[Category:1992 debut albums]]
[[Category:Albums produced by Esham]]
[[Category:Albums produced by Mike E. Clark]]
[[Category:English-language albums]]
[[Category:Insane Clown Posse albums]]
[[Category:Island Records albums]]
[[Category:Self-released albums]]
[[Category:Psychopathic Records albums]]