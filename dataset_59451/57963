{{Infobox journal
| title         = Journal of Cancer Research and Clinical Oncology
| cover         = 
| caption       = 
| former_names   = Zeitschrift für Krebsforschung, Zeitschrift für Krebsforschung und klinische Onkologie
| abbreviation  = J. Cancer Res. Clin. Oncol.
| discipline    = [[Oncology]]
| peer-reviewed = 
| language      = English, German
| editor        = Klaus Hoffken
| publisher     = [[Springer Science+Business Media]]
| country       = 
| history       = 1903-present
| frequency     = Monthly
| openaccess    = 
| license       = 
| impact        = 3.081
| impact-year   = 2014
| ISSNlabel     =
| ISSN          = 0171-5216
| eISSN         = 1432-1335
| CODEN         =JCROD7
| JSTOR         = 
| LCCN          = 
| OCLC          =525582786
| website       =http://www.springer.com/medicine/oncology/journal/432
| link1         = http://link.springer.com/journal/volumesAndIssues/432
| link1-name    =Online archive
| link2         = <!-- up to |link5= -->
| link2-name    = <!-- up to |link5-name= -->
| boxwidth      = 
}}
The '''''Journal of Cancer Research and Clinical Oncology''''' is a monthly [[peer-reviewed]] [[medical journal]] covering [[oncology]]. It was established in 1903 under the name '''''Zeitschrift für Krebsforschung''''', and changed its name to '''''Zeitschrift für Krebsforschung und klinische Onkologie''''' in 1971.<ref>{{cite web | url=http://www.ncbi.nlm.nih.gov/nlmcatalog?term=%22Zeitschrift%20f%C3%BCr%20Krebsforschung%20und%20klinische%20Onkologie.%20%20Cancer%20research%20and%20clinical%20oncology%22%5BTITLE%5D%20NOT%207902060%5BNLM%20Unique%20ID%5D | title=Zeitschrift für Krebsforschung und klinische Onkologie | work=NLM Catalog | accessdate=18 August 2015}}</ref> The journal obtained its current name in 1979.<ref>{{cite web | url=http://www.ncbi.nlm.nih.gov/nlmcatalog/7902060 | title=Journal of Cancer Research and Clinical Oncology | work=NLM Catalog | accessdate=18 August 2015}}</ref> It is published by [[Springer Science+Business Media]] and the [[editor-in-chief]] is Klaus Hoffken ([[University of Düsseldorf]]). According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 3.081.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Cancer Research and Clinical Oncology |title=2014 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==References==
{{Reflist}}

==External links==
*{{Official website|http://www.springer.com/medicine/oncology/journal/432}}

[[Category:Oncology journals]]
[[Category:Publications established in 1903]]
[[Category:Springer Science+Business Media academic journals]]