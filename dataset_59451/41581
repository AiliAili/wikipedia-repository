{{infobox military conflict
| conflict    = Battle of Banjo
| partof      = the [[Kamerun Campaign]] in [[World War I]]
| image       = 
| caption     = 
| date        = 4–6 November 1915
| place       = [[Banyo, Cameroon|Banjo]], [[Kamerun]]
| coordinates = 
| map_type    = 
| latitude    = 
| longitude   = 
| map_size    = 
| map_marksize =
| map_caption = 
| map_label   = 
| territory   =
| result      = British victory
| status      =
| combatant1  = {{flagicon|United Kingdom}} [[British Empire]]
*[[File:Flag of British Colonial Nigeria.svg|25px]] [[Colony and Protectorate of Nigeria|British Nigeria]]
| combatant2  = {{flagicon|German Empire}} [[German Empire]]
*[[File:Flag of Deutsch-Kamerun.svg|25px]] [[Kamerun|German Kamerun]]
| combatant3  = 
| commander1  =  {{flagicon|United Kingdom}} Brigadier General Frederick Hugh Cunliffe
{{flagicon|United Kingdom}} Captain Bowyer-Smijth {{KIA}}
| commander2  =  {{flagicon|German Empire}} Captain Adolf Schipper {{KIA}}
| commander3  = 
| units1      =
| units2      = 
| units3      = 
| strength1   = 
| strength2   = 23 Europeans<br>200 Africans<ref name="W2343">Wood et al. Vol. 8, p. 2343.</ref>
| strength3   = 
|casualties1= 50<ref name="TST"/><ref name="TBC">The Brisbane Courier, 23 Nov. 1915.</ref>
|casualties2= 3 Europeans<br>25 Africans<ref name="TST">The Straits Times, 6 Nov. 1915.</ref>

|campaignbox = {{Campaignbox West African Campaign}}
}}

During the '''Battle of Banjo''' or '''Battle of Banyo''', [[United Kingdom|British]] forces besieged [[German Empire|German]] forces entrenched on the [[Banyo, Cameroon|Banjo]] mountain from 4 to 6 November 1915 during the [[Kamerun campaign]] of the [[First World War]]. By 6 November much of the German force had deserted, while the rest surrendered. The battle resulted in victory for the [[Allies of World War I|Allies]] and breakdown of German resistance in northern [[Kamerun]].

==Background==
Following the outbreak of war between Britain and Germany in the summer of 1914, British forces in neighboring [[Colony and Protectorate of Nigeria|Nigeria]] mounted an attack on the German forts at Garua in northern [[Kamerun]]. After failing to take the forts and suffering heavy casualties at the [[First Battle of Garua]], British forces in the border regions adopted a defensive strategy. The German commander at Garua, Von Crailsheim gained confidence and mounted a raid across the border which was repulsed at the [[Battle of Gurin]] in April 1915.<ref>Wood et al. Vol. 6, p. 1702.</ref> Following the German defeat, Captain Adolf Schipper led the wounded German soldiers to the fort at Banjo to the south. The German raid stimulated retaliation from the British commander in the area, Hugh Cunliffe who went on to finally capture the forts at Garua at the [[Second Battle of Garua]]. In July, Cunliffe went on to win the [[Battle of Ngaundere]] further south. Due to heavy rains however, Cunliffe chose to take part in the [[Siege of Mora]] instead of make an advance southward to the German base at Jaunde.<ref name="GEWA">''Germans in East and West Africa'', 1915 pp. 1-10.</ref>

Because of an improvement in weather conditions, Cunliffe resumed his push south in October. On 22 October, the town of Bamenda was occupied by Cunliffe's forces. British forces occupied the village Banjo on 24 October.<ref name="O60"/> On 3 November the column occupied the town of [[Tibati]], approximately 90 kilometers southwest of Ngaundere.<ref name="AG">Ashburton Guardian, 23 Nov. 1915.</ref> The German fort on a hill near the town of [[Banyo, Cameroon|Banjo]] was near to the border with Nigeria and was the last German stronghold in northern Kamerun that stood between Cunliffe and Jaunde. The fort was situated atop a mountain with steep slopes but a relatively flat summit. A number of boulders were located on the slopes of the mountain.<ref name="TBC"/> In preparation for the battle, German forces built approximately 300 [[Sangar (fortification)|sangars]] between these boulders and constructed numerous entrenchments on the slopes of the hill.<ref name="D185">Dane 1919, p. 185.</ref><ref>Strachan 2004.</ref> Under the command of Captain Adolf Schipper, the fort had prepared itself for a long siege like the ones seen at Mora and Garua. Before the battle took place, the garrison had even  prepared the top of the mountain for agricultural cultivation.<ref name="D186">Dane 1919, p. 186.</ref> By October 1915 the fort was protected by 23 European officers and approximately 200 native [[Askaris]].<ref name="W2343"/>

==Battle==
While British forces had occupied the town of Banjo since late October, fighting did not start until an attempt was made to capture the fort. On the morning of 4 November, a British company under Captain Bowyer-Smijth launched an assault on the German defenses with the support of three artillery pieces that had been brought.<ref name="O60">O'Neill 1918, p. 60.</ref> Due to dense fog, the British force was able to surprise the German defenders. During the conflict that ensued, Bowyer-Smijth was killed in action near the summit along with others from his company, which retreated back down the mountain.<ref name="D186"/> That night five British companies attempted another assault on the hill. The Germans were able to hold off the attack through the bombardment of the oncoming infantry with dynamite. On 5 November, British forces remained positioned on the slopes of the hill approximately 100 meters from the summit.<ref name="D186"/> At this time the three British artillery pieces began to run out of ammunition. By that evening however, more had arrived which enabled another assault on the fort.<ref name="O61">O'Neill 1918, p. 61.</ref> On the night of 5 November, during a thunderstorm, the final attack was made.<ref name="AG"/> When Nigerian troops finally reached the summit, fighting at close quarters ensued that cost both sides heavy casualties. During the battle, much of the German garrison had deserted while those that remained surrendered by the morning of 6 November 1914.<ref>Burg 1998, p. 89.</ref> The German commander, Captain Adolf Schipper, was killed during the battle<ref name="Walter">Walter 2001, pp. 39-42</ref> along with approximately 27 other German soldiers.<ref name="TST"/> British losses included around 50 killed.<ref name="TBC"/><ref name="AG"/>

==Aftermath==
Following the battle, British forces were able to capture most of the German deserters. When the British occupied the fort at Banjo they found it to be a considerably strong position with sufficient ammunition and supplies for further resistance. On the summit the Germans had pigs, sheep and 226 cattle.<ref name="D186"/> The British victory at Banjo meant that German resistance in northern Kamerun was virtually over. The forces of Cunliffe in the north were now able to come into contact with those of Dobell, in the southwest. The capture of the German fort here allowed for a second assault on Jaunde and the German escape to the neutral Spanish colony of [[Rio Muni]].<ref name="O61"/><ref name="D187">Dane 1919, p. 187.</ref>

==Notes==
{{Reflist|30em}}

==References==
*Burg, David F., and L. Edward. Purcell. ''Almanac of World War I''. Lexington, KY: University of Kentucky, 1998.
*Dane, Edmund. ''British Campaigns in Africa and the Pacific, 1914-1918'',. London: Hodder and Stoughton, 1919.
*''Germans in East and West Africa.'' Journal of the African Society 15 (1915): 1-10.
*[http://paperspast.natlib.govt.nz/cgi-bin/paperspast?a=d&d=AG19151123.2.32.44 ''In the Cameroons - Allies' Success.''] Ashburton Guardian 23 Nov. 1915: 6. Paperspast. New Zealand National Library. Web.
*[http://trove.nla.gov.au/ndp/del/article/20067372 ''In the Cameroons - Anglo-French Success.''] The Brisbane Courier 23 Nov. 1915: 7.Trove. National Library of Australia. Web.
*O'Neill, Herbert C. ''The War in Africa and the Far East.'' London: London Longmans Green, 1918.
*Strachan, Hew. The First World War in Africa. Oxford University Press. 2004. ISBN 0-199-25728-0
*[http://newspapers.nl.sg/Digitised/Article/straitstimes19151106-1.2.60.aspx ''Success in Cameroons.''] The Straits Times [Singapore] 6 Nov. 1915: 9. National Library Singapore. Web.
*Walter Nuhn: ''Schutztruppenoffiziere auf dem Soldatenfriedhof in Banyo/Kamerun''. In: ''Mitteilungsblatt des Traditionsverbandes ehemaliger Schutz- und Überseetruppen'', 2001, ISSN 1430-0613.
*Wood, Leonard, Austin M. Knight, Frederick Palmer, Frank H. Simonds, and Arthur B. Ruhl. The Story of the Great War: With Complete Historical Record of Events to Date. Ed. Francis J. Reynolds, Allen L. Churchill, and Francis T. Miller. Vol. 6: P.F. Collier & Sons, 1916.
*Wood, Leonard, Austin M. Knight, Frederick Palmer, Frank H. Simonds, and Arthur B. Ruhl. ''The Story of the Great War: With Complete Historical Record of Events to Date.'' Ed. Francis J. Reynolds, Allen L. Churchill, and Francis T. Miller. Vol. 8. P.F. Collier & Sons, 1916.

{{coord|06|46.5|N|11|49.1|E|display=title}}

{{DEFAULTSORT:Banjo, Battle of 1915}}
[[Category:Battles of World War I involving Germany]]
[[Category:Battles of World War I involving the United Kingdom]]
[[Category:1915 in Africa]]
[[Category:African theatre of World War I]]
[[Category:Battles of the African Theatre (World War I)]]
[[Category:Military history of Cameroon]]
[[Category:Battles of the Kamerun campaign]]
[[Category:Conflicts in 1915]]