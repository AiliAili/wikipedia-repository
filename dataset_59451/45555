<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]] and [[Wikipedia:WikiProject Biography/Military]]. -->
{{Infobox military person
| name =Oren John Rose
| image = Oren John Rose.jpg
| image_size        = 250
| caption = Oren John Rose, 1918
| birth_date = <!-- {{Birth date and age|YYYY|MM|DD}} -->23 March 1892
| death_date = {{d-da|21 June 1971|23 March 1892}}
| placeofburial_label =
| placeofburial =
| birth_place =[[Platte County, Missouri]], USA
| death_place =[[Sunnyvale, California]], USA
| placeofburial_coordinates = <!-- {{coord|LAT|LONG|display=inline,title}} -->
| nickname =
| allegiance    = {{flag|United States|23px}}
| branch        = [[Royal Air Force]] (United Kingdom)
| serviceyears =
| rank =Captain
| unit          = Royal Air Force
* [[No. 92 Squadron RAF]]
| commands =
| battles       = [[File:World War I Victory Medal ribbon.svg|50px]]&nbsp;[[World War I]]
| awards =[[Distinguished Flying Cross (United Kingdom)|Distinguished Flying Cross]] with Bar
| relations =
| laterwork =
}}

Captain '''Oren John Rose''', usually referred to as '''O. J. Rose''' (23 March 1892 – 21 June 1971) was a World War I [[flying ace]] credited with 16 aerial victories.<ref name=AA>{{cite book |title=''American Aces of World War 1'' |page= 39 }}</ref><ref name=aerodrome>{{cite web |url=http://www.theaerodrome.com/aces/usa/rose1.php |title=Oren Rose |publisher=theaerodrome.com |accessdate=11 January 2010}}</ref>

==World War I==
Rose joined the [[Royal Flying Corps]] in 1917. He went to France, and was assigned to 92 Squadron.<ref name=AA/> Flying one of four different [[RAF SE.5a]]s, he tallied up his victories between 30 July and 4 November 1918, becoming the top scoring ace of [[No. 92 Squadron RAF]].

He tallied 14 enemy airplanes destroyed, including one shared with [[James Victor Gascoyne]], and two "driven down out of control".<ref name=aerodrome/>

==After World War I==

In 1919, Rose fought in the [[Russian Civil War]].

He returned to the US and went into business, becoming a car dealer in [[Marysville, Ohio]].

During World War II, Rose joined the [[U.S. Army Air Corps]] as an Operations and Training Officer in Kentucky.<ref name=aerodrome/> He moved on to a squadron command in Louisiana during 1943 and 1944. By the time he was discharged in 1946, Rose had become commander of the Aircraft Assignment Base at [[Wright Field]].

He returned to his auto business, from which he retired in the 1960s.<ref name=AA/>

==Honors and awards==
'''Distinguished Flying Cross (DFC)'''

Lieut. Oren John Rose. <small>(FRANCE)</small>

A very gallant officer who has accounted for nine enemy aeroplanes. On 29 September he observed an enemy scout attacking some of our machines; engaging it, he drove it down from 15,000 feet to 6,000 feet, when it fell in flames. On his return journey he attacked and destroyed an enemy two-seater.<ref>{{cite web |url=http://www.london-gazette.co.uk/issues/31046/supplements/14325 |title=Supplement to the London Gazette |date=3 December 1918 |accessdate=11 April 2010}}</ref>

'''Distinguished Flying Cross (DFC) Bar'''

Lt (A./Capt.) Oren John Rose, D.F.C. <small>(FRANCE)</small>

A brilliant and fearless leader who, since 9 October, has destroyed seven enemy aeroplanes. His personal example of skill and determination in aerial combats and in attacking troops and transport on the ground is of the greatest value in maintaining the high standard of efficiency in his squadron.<ref>http://www.flightglobal.com/pdfarchive/view/1919/1919%20-%200213.html Retrieved 11 January 2010.</ref>

==See also==
{{Portal|World War I|Biography}}
* [[List of World War I flying aces from the United States]]

==References==
{{Reflist}}
{{Refbegin}}
{{Refend}}

==Bibliography==
* ''SE 5/5a Aces of World War I.'' Norman Franks. Osprey Publishing, 2007. ISBN 1-84603-180-X, 9781846031809.
* ''American Aces of World War 1'' Harry Dempsey. Osprey Publishing, 2001. ISBN 1-84176-375-6, ISBN 978-1-84176-375-0.

==External links==

{{DEFAULTSORT:Rose, Oren}}
[[Category:1892 births]]
[[Category:1971 deaths]]
[[Category:American World War I flying aces]]
[[Category:Aviators from Missouri]]


{{US-mil-bio-stub}}