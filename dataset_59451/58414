{{Infobox Journal
| title = The Journal of Philosophy
| cover = [[File:jphil-wikicover.gif]]
| editor = The [[Columbia University]] Philosophy Department
| discipline = [[Philosophy]]
| abbreviation = J. Phil.
| publisher = The Journal of Philosophy, Inc.
| country = United States
| frequency = Monthly
| history = 1921–present; 1904-1920 as ''The Journal of Philosophy, Psychology and Scientific Methods''
| openaccess = 
| impact = 
| impact-year = 
| website = http://www.journalofphilosophy.org/
| link1 = 
| link1-name = 
| link2 = http://www.pdcnet.org/jphil/toc
| link2-name = Online access
| RSS = http://www.pdcnet.org/pdc/bvdb.nsf/getrssxml?openagent&synonym=jphil
| atom = 
| JSTOR = 0022362X
| OCLC = 37699220
| LCCN = 06000973
| CODEN = 
| ISSN = 0022-362X
| eISSN = 1939-8549
}}
'''''The Journal of Philosophy''''' is a monthly [[Peer review|peer-reviewed]] [[academic journal]] on [[philosophy]], founded in 1904 at [[Columbia University]]. Its stated purpose is "To publish philosophical articles of current interest and encourage the interchange of ideas, especially the exploration of the borderline between philosophy and other disciplines."<ref name="history">{{cite web |url=http://www.journalofphilosophy.org/generalinfo.html |title=Purpose and History |publisher=The Journal of Philosophy |accessdate=2015-07-13}}</ref> Subscriptions and online access are managed by the [[Philosophy Documentation Center]].<ref>"Special Announcement", ''The Journal of Philosophy'', volume 111, no.11, November 2014, back cover.</ref>

The Journal was ranked the second highest-quality philosophy journal in a poll conducted on the popular philosophy blog Leiter Reports,<ref>{{Cite web|title = Leiter Reports:  A Philosophy Blog: Top philosophy journals, without regard to area|url = http://leiterreports.typepad.com/blog/2013/07/top-philosophy-journals-without-regard-to-area.html|website = leiterreports.typepad.com|accessdate = 2016-01-05}}</ref> and is widely regarded as one of the most prestigious journals in the field.<ref>{{Cite web|title = Against Rank {{!}} Inside Higher Ed|url = https://www.insidehighered.com/views/2010/06/21/against-rank|website = www.insidehighered.com|accessdate = 2016-01-05}}</ref> The journal also publishes the Dewey, Woodbridge, and Nagel Lectures series held at Columbia University.

== History ==
The journal was founded at Columbia University in 1904 as ''The Journal of Philosophy, Psychology and Scientific Methods'', under the editorship of Professor [[Frederick James Eugene Woodbridge|J. E. Woodbridge]] and Professor [[James McKeen Cattell]].<ref name=":0">Iris Bean, "Brief History of the Journal of Philosophy", 1942.</ref> Wendell T. Bush became co-editor of the journal in 1906 and provided it with its endowment.<ref name=":0" /> The inaugural issue announced that the journal was founded with the intent of "covering the whole field of scientific philosophy, psychology, ethics and logic" so that "the relations between philosophy and psychology should remain intimate."<ref>{{Cite journal|jstor = i308767|title = Notes and News|last = |first = |date = 7 January 1904|journal = The Journal of Philosophy, Psychology and Scientific Methods|doi = |pmid = |issue = Vol. 1, No. 1}}</ref> In 1921 the name of the journal was shortened to ''The Journal of Philosophy.''

From 1954 to 1985, the President of the journal was Albert G. Redpath. After Redpath's death, [[Corliss Lamont]] was President for a short period. [[Arthur Danto|Arthur C. Danto]] was President from 1985 to 2010, followed by [[Akeel Bilgrami]], the current President.

The journal is published from Columbia University. From its founding until 1998, the journal was printed by the Lancaster Press in Lancaster, Pennsylvania.<ref>{{Cite journal|url = |title = Vol. 95|last = |first = |date = 1998|journal = The Journal of Philosophy|doi = |pmid = }}</ref> Today the journal is printed by the Sheridan Press in Hanover, Pennsylvania.

Past contributors to the journal include: [[Theodor W. Adorno]], [[Elizabeth Anscombe|G.E.M. Anscombe]], [[David Malet Armstrong|D. M. Armstrong]], [[A. J. Ayer]][[Elizabeth Anscombe|,]] [[Jonathan Bennett (philosopher)|Jonathan Bennett]], [[Henri Bergson]], [[Ned Block]], [[Tyler Burge]], [[Rudolf Carnap]], [[Stanley Cavell]], [[David Chalmers]], [[Roderick Chisholm]], [[Noam Chomsky]], [[Paul Churchland]], [[Arthur Danto]], [[Donald Davidson (philosopher)|Donald Davidson]], [[Daniel Dennett]], [[John Dewey]], [[Fred Dretske]], [[W. E. B. Du Bois|W.E.B. Du Bois]], [[Michael Dummett]], [[Ronald Dworkin]], [[Kit Fine]], [[Jerry Fodor]], [[Harry Frankfurt]], [[Peter Geach]], [[Alvin Goldman]], [[Nelson Goodman]], [[Jürgen Habermas]], [[Ian Hacking]], [[Gilbert Harman]], [[Carl Gustav Hempel|Carl Hempel]], [[Jaakko Hintikka]], [[Frank Cameron Jackson|Frank Jackson]], [[William James]], [[Jaegwon Kim]], [[David Lewis (philosopher)|David Lewis]], [[Walter Kaufmann (philosopher)|Walter Kaufmann]], [[Christine Korsgaard]], [[Saul Kripke]], [[Alasdair MacIntyre]], [[J. L. Mackie]], [[John McDowell]], [[George Herbert Mead]], [[Sidney Morgenbesser]], [[Ernest Nagel]], [[Thomas Nagel]], [[Robert Nozick]], [[Martha Nussbaum]], [[Derek Parfit]], [[Charles Sanders Peirce]], [[Alvin Plantinga]], [[Hilary Putnam]], [[Willard Van Orman Quine|W.V.O. Quine]], [[John Rawls]], [[Hans Reichenbach]], [[Richard Rorty]], [[Bertrand Russell]], [[George Santayana]], [[T. M. Scanlon]], [[Wilfrid Sellars]], [[Amartya Sen]], [[Elliott Sober|Elliot Sober]], [[Robert Stalnaker]], [[P. F. Strawson]], [[Charles Taylor (philosopher)|Charles Taylor]], [[Tim van Gelder]], and [[Peter van Inwagen]].

==Notable articles==
According to the Thomson Reuters [[Journal Citation Reports]], the most cited articles published by the journal include:

=== Early 20th Century ===
* "Does 'Consciousness' Exist" (1904) - [[William James]]
* "A World of Pure Experience" (1904) - William James
* "The Postulate of Immediate Empiricism" (1905) - [[John Dewey]]
* "The Social Self" (1913) - [[George Herbert Mead]]
* "Dewey's Naturalistic Metaphysics" (1925) - [[George Santayana]]
* "Behaviorism and Purpose" (1925) - [[Edward C. Tolman]]
* "Logical Positivism" (1931) - Albert E. Blumberg and [[Herbert Feigl]]

=== Mid 20th Century ===
* "Present Standpoints and Past History" (1939) - [[Arthur Oncken Lovejoy|Arthur O. Lovejoy]]
* "The Function of General Laws in History" (1942) - [[Carl Gustav Hempel|Carl G. Hempel]]
* "The Problem of Counterfactual Conditionals" (1947) - [[Nelson Goodman]]
* "Quantifiers and Propositional Attitudes" (1956) - [[W.V.O. Quine]]
* "Actions, Reasons, and Causes" (1963) - [[Donald Davidson (philosopher)|Donald Davidson]]
* "The Artworld" (1964) - [[Arthur Danto|Arthur C. Danto]]
* "An Argument for the Identity Theory" (1966) - [[David Kellogg Lewis]]
* "Singular Terms, Truth-Value Gaps, and Free Logic" (1966) - [[Bas van Fraassen|BC Van Fraassen]]
* "Counterpart Theory and Quantified Modal Logic" (1968) - David Kellogg Lewis
* "Ontological Relativity" (1968) - W.V.O. Quine
* "Alternate Possibilities and Moral Responsibility" (1969) - [[Harry Frankfurt]]

=== Late 20th Century ===
* "Epistemic Operators" (1970) - [[Fred Dretske]]
* "Intentional Systems" (1971) - [[Daniel C. Dennett]]
* "Freedom of the Will and the Concept of a Person" (1971) - Harry Frankfurt
* "Causation, Nomic Subsumption, and the Concept of Event" (1973) - [[Jaegwon Kim]]
* "Meaning and Reference" (1973) - [[Hilary Putnam]]
* "Functional Analysis" (1975) - Robert Cummins
* "Free Agency" (1975) - Gary Watson
* "Outline of a Theory of Truth" (1975) - [[Saul Kripke]]
* "Preference and Urgency" (1975) - [[T. M. Scanlon]]
* "Discrimination and Perceptual Knowledge" (1976) - [[Alvin Goldman|Alvin I. Goldman]]
* "Wide Reflective Equilibrium and Theory Acceptance in Ethics" (1979) - [[Norman Daniels]]
* "Kantian Constructivism in Moral Theory" (1980) - [[John Rawls]]
* "Rational and Full Autonomy" (1980) - John Rawls
* "Eliminative Materialism and the Propositional Attitudes" (1981) - [[Paul M. Churchland]]
* "Moral Information" (1985) - [[Amartya Sen]]
* "Skepticism about Practical Reason" (1986) - [[Christine Korsgaard]]
* "What Mary Didn't Know" (1986) - [[Frank Cameron Jackson|Frank C. Jackson]]
* "Individualism and Self-Knowledge" (1988) - [[Tyler Burge]]
* "Why Abortion is Immoral" (1989) - [[Don Marquis (philosopher)|Don Marquis]]
* "The Structure and Content of Truth" (1990) - Donald Davidson
* "National Self-Determination" (1990) - [[Avishai Margalit]] and [[Joseph Raz]]
* "Real Patterns" (1991) - Daniel C. Dennett
* "Reconciliation Through the Public Use of Reason: Remarks on John Rawls's Political Liberalism" (1995) - [[Jürgen Habermas]]
* "What Might Cognition Be, If Not Computation?" (1995) - [[Tim van Gelder]]
* "The Woodbridge Lectures: Having the World in View: Sellars, Kant, and Intentionality" (1997) - [[John McDowell]]

=== 21st Century ===
* "Causation as Influence" (2000) - David Lewis
* "Knowing How" (2001) - [[Jason Stanley]] and [[Timothy Williamson]]
* "The Harder Problem of Consciousness" (2002) - [[Ned Block]]
* "Challenges to the Hypothesis of Extended Cognition" (2004) - Robert D. Rupert
* "What Do We Want from a Theory of Justice" (2006) - Amartya Sen
* "The Dewey Lectures: What Kind of Creatures Are We?" (2013) - [[Noam Chomsky]]

== See also ==
* [[List of philosophy journals]]

==References==
{{Reflist}}

== External links ==
* {{Official website|http://www.journalofphilosophy.org/}}
* [https://www.pdcnet.org/jphil/The-Journal-of-Philosophy The Journal of Philosophy at the Philosophy Documentation Center]

{{DEFAULTSORT:Journal Of Philosophy, The}}
[[Category:Philosophy journals]]
[[Category:Philosophical literature]]
[[Category:Columbia University publications]]
[[Category:Monthly journals]]
[[Category:Publications established in 1904]]
[[Category:English-language journals]]