{{Use dmy dates|date=March 2011}}
{{Infobox artist
| name          = Peter Liddle 
| image         = Peter_Liddle_Portrait_2011.jpg
| imagesize     = 
| caption       = Peter Liddle, 2011 
| birth_name    = 
| birth_date    = 1940 
| birth_place   = [[Blackwell, County Durham]], England 
| death_date    = 
| death_place   = 
| nationality   = British 
| field         = painter, sculptor 
| training      = [[Nottingham College of Art]] 
| movement      = 
| works         =  
| patrons       = 
| influenced by = [[Georges Seurat]], [[Mark Rothko]]
| influenced    = 
| awards        = 
}}

'''Peter Liddle''' (born 1940) is a British landscape artist and sculptor, known for his allegorical depictions of the British Isles.

== Biography ==

Liddle attended the [[Nottingham Trent University, School of Art and Design|Nottingham College of Art]] from around 1957-59 and studied drawing and art history under John Powell<ref name="since1843">{{cite book|title=Since 1843: In The Making (Catalogue of exhibition for former students of Nottingham University)|year=2014|url=http://www.boningtongallery.co.uk/shop/since-1843-in-the-making-catalogue}}</ref> and smithing under the silversmith G.K. Kitson.  Liddle befriended fellow students [[Keith Albarn]] and his partner, Hazel.<ref>{{Citation |author=Jacqui Smith|date=May 2013|publisher=Network Nottingham Trent University Alumni Magazine|title=Artist Peter reflects on student life|url=http://www.ntualumni.org.uk/news_and_events/network_magazine/artist_peter_reflects_on_student_life|accessdate=6 January 2013|archiveurl=http://www.webcitation.org/6N74XU7bG |archivedate=3 February 2014}}</ref>  After studying, Liddle lived for a time in London where he married the Marshal Scholar Patricia Moyer.<ref name="cumbriaLifeCW">{{Citation |author=Chris Wadswoth|date=July–August 1992|publisher=Cumbria Life|title=Art: Peter Liddle|archiveurl=http://www.webcitation.org/6N73uKOsB|archivedate=3 February 2014|url=http://peterliddle.com/content/01-about/03-press/pl3.pdf}}</ref>  
  
He also struck up a close friendship with the adult educationalist, [[Frank Turk (biologist)|Frank Turk]], learning much from his experience of eastern spirituality.  The pair met regularly and maintained a correspondence long after Peter left Cornwall and up until Frank’s death in 1996.  However, with the end of his first marriage in 1969, Peter left Cornwall and after a period of instability settled in the Lake District in 1971.  He remarried in 1972 to Sandra Stone (b. 1950) and together they had two children Sorrel (Stratford) 1973 and Jude 1975.<ref name="cumbriaLifeSA">{{Citation |author=Sue Allan|date=August–September 2004|publisher=Cumbria Life|title=Artist on the edge|archiveurl=http://www.webcitation.org/6N743RbEb |archivedate=3 February 2014 |url=http://peterliddle.com/content/01-about/03-press/pl1.pdf}}</ref>  Peter never fell in with a particular school after leaving Cornwall but exhibited at times with other artists represented by [[Godfrey Pilkington]]’s renowned [[Piccadilly Gallery]] in the 70s and 80s.<ref name="waddingtonGalleries"/>

==Art==
While still living in London Liddle began painting the [[Cornwall|Cornish]] coast, three paintings from this period were selected for the [[Royal Academy of Arts|Royal Academy]] Summer Exhibition in 1965.<ref name="RA">{{cite book|last=|first=|title=Royal Academy Exhibitors, 1905-1970 a dictionary of artists and their work in the Summer Exhibitions of the Royal Academy of Arts, in 4 volumes|year=1985|publisher=Hilmarton Manor Press|location=Calne|isbn=|page=|pages=|url=}}</ref>  Later that year Peter and Patricia relocated to a studio in Cornwall where he began to experiment with acrylic paint, exhibiting regularly with both The Newlyn Society of Artists and the [[Penwith Society of Arts]].<ref name="cornishIndex"/>  Two of his closest peers while in Cornwall were the painters [[Margo Maeckelberghe]] and [[Jack Pender]].  During this five year period Liddle’s style changed completely.  He credits the challenge of the incredible Portholland light in his transition from heavy, textured, oils to whiter, more transparent, acrylics.<ref name="cornishIndex"/><ref>{{Citation |author=Julia McIntosh|date=January–February 2012|publisher=[[Cornwall Life]]|title=Peter Liddle Portrait}}</ref>

Liddle is a British painter known for his allegorical depictions of the wild reaches of the British Isles.  His paintings can be austere and unsettling, often hinting at something beyond the landscape they superficially represent.  The award winning author [[Sarah Hall (writer)|Sarah Hall]] has described the atavistic quality of his work as such, ''"He seems able to lay this place bare, restore it to its original form, its ancientness, its soul. Mountain ranges often seem prehistoric, or like dinosaur relics."''<ref name="cumbriaLifeSH">{{Citation |author=[[Sarah Hall (writer)|Sarah Hall]]|date=January–February 2004|publisher=Cumbria Life|title=A life in...painting: "The Wilderness years"|archiveurl=http://www.webcitation.org/6N748opne |archivedate=3 February 2014 |url=http://peterliddle.com/content/01-about/03-press/pl2.pdf}}</ref>  When Liddle talks about his work, he frequently asserts that experience of nature and particularly stone, is core to his practice. He has been quoted as saying, ''"My purpose in life seems to have been to record adventures."''<ref>{{cite web|title=Peter Liddle|url=http://www.cumbria-artefacts.org.uk/artists-crafts/peterliddle.asp|accessdate=31 January 2014 |publisher=Artefacts : Artists and Craftspeople}}</ref> He has exhibited nationally,<ref name="since1843"/><ref name="cornishIndex">{{cite web|title=Liddle, Peter|url=http://cornwallartists.org/cornwall-artists/peter-liddle-0|publisher=Cornish Artist Index|accessdate=30 January 2014}}</ref><ref>{{cite news|last=Mullen|first=Adrian|title=High level buzz|url=http://www.thewestmorlandgazette.co.uk/leisure/whats_on/429633.High_level_buzz/|accessdate=31 January 2014|newspaper=The West Morland Gazette|date=7 November 2003}}</ref> including at the Royal Academy,<ref name="RA"/> and internationally.<ref name="waddingtonGalleries">{{cite web|title="English Realism" @ Waddington Galleries: Montreal & Toronto 1978|url=http://peterliddle.com/content/01-about/02-cv/waddington.pdf|archiveurl=http://www.webcitation.org/6N766CGUD |archivedate=3 February 2014}}</ref>  His work is held in public collections including both the [[Leicestershire County Council]] Art Collection<ref>{{cite web|title=Galloway No.5 (Peter Liddle, 1972) on BBC Your Paintings|url=http://www.bbc.co.uk/arts/yourpaintings/paintings/galloway-82528|accessdate=1 February 2014|archiveurl=http://www.webcitation.org/6N74sJrG0 |archivedate=3 February 2014}}</ref>  and the Otter Gallery, [[University of Chichester]].<ref>{{cite web|title=Beck Falls No.1 (Peter Liddle, 1974) on BBC Your Paintings|url=http://www.bbc.co.uk/arts/yourpaintings/paintings/beck-falls-1-70413|accessdate=1 February 2014|archiveurl=http://www.webcitation.org/6N74nN81R |archivedate=3 February 2014}}</ref> He has regularly appeared in the ''Cumbria Life'' publication<ref name="cumbriaLifeCW"/><ref name="cumbriaLifeSA"/><ref name="cumbriaLifeSH"/> and has also featured on several regional<ref>{{Cite episode
 | series        = Lie of the Land:
 | title         = Peter Liddle "Fool on the Hill"
 | first         = Richard 
 | last          = Else
 | network       = BBC
 | station       = North East
 | date          = 1983
 | minutes       = 30
}}</ref><ref>
{{Cite episode
 | series        = 7th Heaven arts programme 
 | title         = Liddle 92 Exhibition
 | first         = Christian 
 | last          = Diamond
 | network       = Border TV
 | date          = 1992
}}</ref><ref>
{{Cite episode
 | title         = Liddle with Union Dance at the Cochrane Theatre Exhibition
 | first         = Clem
 | last          = Shaw
 | network       = Border TV
 | date          = 1997
}}</ref>
and national
<ref>{{Cite episode
 | series        = The Late Show
 | network       = BBC
 | station       = 2
 | date          = 22 February 1995
}}
</ref> television programmes.

{{Gallery
|title=The Work of Peter Liddle
|width=160 | height=170 | lines=4
|align=center
|state=expanded
|footer=
|File:Mussel Shell Mausoleum (Peter Liddle, 1978).jpg|
 alt1=Mussel Shell Mausoleum (Peter Liddle, 1978)
 |Mussel Shell Mausoleum (Peter Liddle, 1978)
|File:Cornish Garden No.1 (Margo Maeckelberghe's Garden) Peter Liddle, 1988.jpg |
 alt2=Cornish Garden No.1 (Margo Maeckelberghe's Garden) Peter Liddle, 1988
 |Cornish Garden No.1 ([[Margo Maeckelberghe|Margo Maeckelberghe's]] Garden) Peter Liddle, 1988
|File:Red Cliff (Peter Liddle, 2003).jpg |
 alt3=Red Cliff (Peter Liddle, 2003) 
 |Red Cliff (Peter Liddle, 2003)

}}

== References ==

{{Reflist}}

== External links ==
* [http://cornwallartists.org/cornwall-artists/peter-liddle-0 Cornish Artist Index]
* [http://peterliddle.com Artist's Website]

{{DEFAULTSORT:Liddle, Peter}}
[[Category:1940 births]]
[[Category:Living people]]
[[Category:Alumni of Nottingham Trent University]]
[[Category:British mixed media artists]]
[[Category:British landscape painters]]
[[Category:20th-century English painters]]
[[Category:English male painters]]
[[Category:21st-century English painters]]
[[Category:People from County Durham]]