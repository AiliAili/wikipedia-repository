<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=Pottier P.40
 | image=RMM Brussel Pottier P40.JPG
 | caption=in the [[Royal Museum of the Armed Forces and Military History]]
}}{{Infobox Aircraft Type
 | type=Single seat [[tailless aircraft|tailless]] [[homebuilt]] sports aircraft
 | national origin=
 | manufacturer=
 | designer=[[Jean Pottier]]
 | first flight=1975
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built= 2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}

The [[France|French]] [[tailless aircraft|tailless]] '''Pottier P.40''' was the first aircraft designed by Jean Pottier. It flew in 1975.

==Design==

The Pottier P.40 was the first of Jean Pottier's many designs,<ref name=GaillardII/> begun around 1967, though not the first to fly as the [[Pottier P.70|P.70]] flew in  August 1974.<ref name=GaillardIIA/> Construction of the P.40 by Bela Nogrady was started in 1968 but the first flight was not made until 1975.<ref name=P40/>

The P.40 is a tailless aircraft with a swept, [[cantilever]], [[low wing]]. In plan, the wing has a rectangular centre section and straight tapered outer panels with [[elevon]]s. There are [[wing tip]] [[fin]]s and outward opening [[rudder]]s which extend a little below the wing.<ref name=GaillardII/>

The short [[fuselage]] is flat sided, mostly occupied by a long [[aircraft canopy|canopy]] over the single seat cockpit.  The engine, a {{convert|25|hp|kW|abbr=on|0|order=flip}} [[Volkswagen air-cooled engine#Aircraft|Volkswagen 1.2 litre]] air-cooled [[flat-four]], is in the rear in [[pusher configuration]].<ref name=GaillardII/>  The P.40 has a low, fixed, [[aircraft fairing|faired]] [[landing gear|bicycle undercarriage]].

==Operational history==

The first P.40 made only one short flight and was then destroyed.<ref name=GaillardII/><ref name=Chillon/> The history of the one surviving example, ''OO-68'', on display in the [[Belgium|Belgian]] [[Royal Museum of the Armed Forces and Military History]] in [[Brussels]] is obscure. The two machines differed a little, with varying engine cooling and exhaust arrangements, and ''OO-68'' has a three, rather than two, blade [[propeller (aeronautics)|propeller]].

==Specifications==
[[File:Pottier P40, OO-68.JPG|thumb|In the Royal Museum of the Armed Forces and Military History]]
{{Aircraft specs
|ref=Gaillard (1991) p.150<ref name=GaillardII/>
|prime units?=met
<!--
        General characteristics
-->
|genhide=

|crew=One
|length m=2.60
|length note=
|span m=6
|span note=
|height m=1.20
|height note=
|wing area sqm=8
|wing area note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=150
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=270
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Volkswagen air-cooled engine#Aircraft|Volkswagen 1.2 litre]]
|eng1 type=air-cooled [[flat-four]]
|eng1 hp=25
|eng1 note=
|power original=
|more power=

|prop blade number=2 (3 on second aircraft)
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

<!--
        Performance
-->
|perfhide=

|max speed kmh=180
|max speed note=
|cruise speed kmh=160
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=600
|range note=
|endurance=<!-- if range unknown -->
|ceiling m=2900
|ceiling note=<ref name=P40/>
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
}}

==References==
{{reflist|refs=

<ref name=Chillon>{{cite book |title=Fox Papa - Registre des avions Français amateur|last=Chillon |first=Jacques|edition=2009|year=|publisher=Ver Luisant|location= Brive|isbn=978-2-3555-1-066-3|page=162}}</ref>

<ref name=GaillardII>{{cite book |title=Les Avions Francais de 1965 à 1990|last=Gaillard|first=Pierre|year=1991|publisher=Éditions EPA|location=Paris|isbn=2 85120 392 4|pages=150}}</ref>

<ref name=GaillardIIA>{{cite book |title=Les Avions Francais de 1965 à 1990|last=Gaillard|year=1991|publisher=Éditions EPA|pages=130}}</ref>

<ref name=P40>{{cite web|url=http://1000aircraftphotos.com/Contributions/Hodgson/8569.htm|title=Poittier P.40 (OO-68) |author=Walter van Tilborg|accessdate=1 February 2015}}</ref>

}}
<!-- ==Further reading== -->
<!-- ==External links== -->

{{Pottier aircraft}}

[[Category:Homebuilt aircraft]]
[[Category:Pusher aircraft]]
[[Category:Tailless aircraft]]
[[Category:French sport aircraft 1950–1959]]
[[Category:Pottier aircraft|P.040]]