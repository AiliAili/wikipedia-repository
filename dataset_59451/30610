{{Taxobox| status = EX| status_system = IUCN3.1
| status_ref = <ref name=iucn>Hoffman, 2008</ref>
| fossil range = [[Holocene]]
| regnum = [[Animal]]ia
| phylum = [[Chordate|Chordata]]
| classis = [[Mammal]]ia
| ordo = [[Carnivora]]
| familia = [[Eupleridae]]
| genus = ''[[Cryptoprocta]]''
| species = [[extinction|†]]'''''C. spelea'''''
| binomial = ''Cryptoprocta spelea''
| binomial_authority=[[Guillaume Grandidier|G. Grandidier]], 1902
|synonyms_ref=<ref name=Gea130/>
| synonyms=
*''Cryptoprocta ferox'' var. ''spelea'' <small>G. Grandidier, 1902</small>
*''Cryptoprocta spelea'': <small>Petit, 1935</small>
*''Cryptoprocta antamba'' <small>Lamberton, 1939</small>
| range_map = Cryptoprocta subfossil range map.svg
| range_map_caption = Subfossil sites for ''Cryptoprocta'' species: blue—''C.&nbsp;spelea''; green—''C.&nbsp;ferox'' and ''C.&nbsp;spelea''; red—''C.&nbsp;ferox''<ref name=Gea141/>
}}

'''''Cryptoprocta spelea''''', also known as the '''giant fossa''',<ref>Alcover and McMinn, 1994, table&nbsp;1</ref> is an [[extinction|extinct]] species of carnivore from [[Madagascar]] in the family [[Eupleridae]], which is most closely related to the [[mongoose]]s and includes all Malagasy carnivorans. It was first described in 1902, and in 1935 was recognized as a separate species from its closest relative, the living [[fossa (animal)|fossa]] (''Cryptoprocta ferox''). ''C.&nbsp;spelea'' is larger than the fossa, but otherwise similar. The two have not always been accepted as distinct species. When and how the larger form became extinct is unknown; there is some anecdotal evidence, including reports of very large fossas, that there is more than one surviving species.

The species is known from [[subfossil]] bones found in a variety of caves in northern, western, southern, and central Madagascar. In some sites, it occurs with remains of ''C.&nbsp;ferox'', but there is no evidence that the two lived at the same time. Living species of comparably sized, related carnivores in other regions manage to coexist, suggesting that the same may have happened with both ''C.&nbsp;spelea'' and ''C.&nbsp;ferox''.  ''C.&nbsp;spelea'' would have been able to prey on larger animals than its smaller relative could have, including the recently extinct [[Subfossil lemur|giant lemurs]].

==Taxonomy==
In 1902, [[Guillaume Grandidier]] described [[subfossil]] carnivoran remains from two caves on Madagascar as a larger "variety" of the living [[fossa (animal)|fossa]] (''Cryptoprocta ferox''), ''C. ferox'' var. ''spelea''. G. Petit, writing in 1935, considered ''spelea'' to represent a distinct species.<ref name=Gea130>Goodman et al., 2004, p.&nbsp;130</ref> [[Charles Lamberton]] reviewed subfossil and living ''Cryptoprocta'' in 1939 and agreed with Petit in recognizing two species,<ref>Goodman et al., 2004, pp.&nbsp;130–131</ref> naming this species from a specimen found  at [[Ankazoabo Cave]] near [[Itampolo]]. The [[specific name (zoology)|specific name]] ''spelea'' means "cave" and was given because of the location of its discovery.<ref name=Gea1167>Goodman et al., 2003, p.&nbsp;1167</ref> However, Lamberton apparently had at most three skeletons of the living fossa, not nearly enough to capture the range of variation in that species, and some later authors did not separate ''C.&nbsp;spelea'' and ''C.&nbsp;ferox'' as species.<ref name=Gea131>Goodman et al., 2004, p.&nbsp;131</ref> [[Steven Goodman]] and colleagues, using larger samples, compiled another set of ''Cryptoprocta'' measurements that was published in a 2004 article. They found that some subfossil ''Cryptoprocta'' fell outside the range of variation of living ''C.&nbsp;ferox'', and identified those as representing ''C.&nbsp;spelea''.<ref name=Gea136>Goodman et al., 2004, p.&nbsp;136</ref> Grandidier had not designated a [[type specimen]] for the species, and to maintain ''C.&nbsp;spelea'' as the name for the larger form of the fossa, Goodman and colleagues designated a specimen to serve as the type specimen (specifically, a [[neotype]]).<ref>Goodman et al., 2004, pp.&nbsp;136–137</ref>

Lamberton recognized a third species, ''Cryptoprocta antamba'', on the basis of a [[mandible]] (lower jaw) with abnormally broad spacing between the [[condyloid process]]es at the back.<ref>Lamberton, 1939, p.&nbsp;191</ref> He also referred two [[femur|femora]] (upper leg bones) and a [[tibia]] (lower leg bone) intermediate in size between ''C.&nbsp;spelea'' and ''C.&nbsp;ferox'' to this species.<ref>Lamberton, 1939, p.&nbsp;193</ref> The specific name refers to the "antamba", an animal allegedly from southern Madagascar described by [[Étienne de Flacourt]] in 1658 as a large, rare, leopard-like carnivore that eats men and calves and lives in remote mountainous areas;<ref>Goodman et al., 2003, p.&nbsp;1169; 2004, p.&nbsp;131</ref> it may have been the giant fossa.<ref>Turvey, 2009, p.&nbsp;34</ref> Goodman and colleagues could not locate Lamberton's material of ''Cryptoprocta antamba'', but suggested that it was based on an abnormal ''C.&nbsp;spelea''.<ref name=Gea137>Goodman et al., 2004, p.&nbsp;137</ref> Together, the fossa and ''C.&nbsp;spelea'' form the genus ''Cryptoprocta'' within the family [[Eupleridae]], which also includes the other Malagasy [[carnivora]]ns—the [[falanouc]], the [[fanaloka]]s, and the [[Galidiinae]]. [[DNA sequence]] studies suggest that the Eupleridae form a single natural ([[monophyletic]]) group and are most closely related to the [[mongoose]]s of Eurasia and mainland Africa.<ref>Garbutt, 2007, p.&nbsp;208</ref>

==Description==
[[File:Cryptoprocta Ferox.JPG|thumb|left|alt=A cat-like mammal on a rock|The [[fossa (animal)|fossa]] (''Cryptoprocta ferox'') is a smaller relative of ''C.&nbsp;spelea'' that still survives.]]
Although some [[morphology (anatomy)|morphological]] differences between the two fossa species have been described,<ref name=L182>Lamberton, 1939, p.&nbsp;182</ref> these may be [[allometric]] (growth-related), and in their 1986 ''[[Mammalian Species]]'' account of the fossa, Michael Köhncke and Klaus Leonhardt wrote that the two were morphologically identical.<ref name=KL2>Köhncke and Leonhardt, 1986, p.&nbsp;2</ref> However, remains of ''C.&nbsp;spelea'' are larger than any living ''C.&nbsp;ferox''. Goodman and colleagues found that skull measurements in specimens they identified as ''C.&nbsp;spelea'' were 1.07 to 1.32 times as large as in adult ''C.&nbsp;ferox'', and [[postcrania]]l measurements were 1.19 to 1.37 times as large.<ref name=Gea136/> The only specimen of ''C.&nbsp;spelea'' in which [[condylobasal length]] (a measure of total skull length) could be ascertained measured 153.4&nbsp;mm (6.04&nbsp;in), compared to a range of 114.5 to 133.3&nbsp;mm (4.51 to 5.25&nbsp;in) in adult ''C.&nbsp;ferox''. [[Humerus]] (upper arm bone) length in twelve ''C.&nbsp;spelea'' is 122.7 to 146.8&nbsp;mm (4.83 to 5.78&nbsp;in), averaging 137.9&nbsp;mm (5.43&nbsp;in), compared to 108.5 to 127.5&nbsp;mm (4.27 to 5.02&nbsp;in), averaging 116.1&nbsp;mm (4.57&nbsp;in), in the extant fossa.<ref name=Geat1>Goodman et al., 2004, table&nbsp;1</ref> Body mass estimates for ''C.&nbsp;spelea'' range from 17&nbsp;kg (37&nbsp;lb)<ref>Personal communication from R. Dewer in Burness et al., 2001, table&nbsp;1</ref> to 20&nbsp;kg (44&nbsp;lb),<ref>Wroe et al., 2004, p.&nbsp;297</ref> and it was among the largest carnivores of the island.<ref>Burness et al., 2001, table&nbsp;1</ref>  By comparison, adult ''C.&nbsp;ferox'' range from 5&nbsp;kg (11&nbsp;lb) to 10&nbsp;kg (22&nbsp;lb).<ref>Garbutt, 2007, p.&nbsp;211</ref>
{{-}}

==Distribution, ecology, and extinction==
{| class="wikitable" align="right" style="margin-left: 0.5em; margin-top: 0em"
|+ Collection sites<ref name=Gea141/>
|-
! Site !! ''spe.'' !! ''fer.''
|-
| [[Ampasambazimba]] ||  || +
|-
| [[Ankarana]] || + || +
|-
| [[Ankazoabo]] || + ||
|-
| [[Antsirabe]] || + || +
|-
| [[Behova]] || + || +
|-
| [[Beloha]] || + || +
|-
| [[Belo sur Mer]] || + || +
|-
| [[Bemafandry]] || + ||
|-
| [[Betioky]] || + ||
|-
| [[Lakaton'ny akanga]] || + ||
|-
| [[Lelia (Madagascar)|Lelia]] || || +
|-
| [[Manombo]] || + || +
|-
| [[Tsiandroina]] || + ||
|-
| [[Tsiravé]] || || +
|-
| colspan="3" | Abbreviations:
*''spe.'': ''C.&nbsp;spelea''
*''fer.'': ''C.&nbsp;ferox''
|}
''Cryptoprocta spelea'' is the only extinct [[carnivora]]n mammal known from Madagascar;<ref name=Gea1167/> recently extinct Madagascan animals also include at least 17&nbsp;species of [[lemur]]s, most of which are larger than the living forms,<ref name=Gea137/> as well as [[elephant bird]]s and [[Malagasy hippopotamus]]es, among others.<ref>Burney et al., 2004, p.&nbsp;25</ref> Subfossil remains of the giant fossa have been found in [[Holocene]] cave sites<ref name=Gea130/> from the northern end of Madagascar along the west coast to the far south, and in the central highlands. Some sites have yielded both ''C.&nbsp;spelea'' and smaller remains referable to the living species, ''C.&nbsp;ferox''; however, lack of robust [[stratigraphy|stratigraphic]] knowledge and no available [[radiocarbon dating]] on subfossil ''Cryptoprocta'' bones makes it uncertain whether the two species lived in the same region at the same time.<ref>Goodman et al., 2003, pp.&nbsp;1167–1168; 2004, pp.&nbsp;140–141</ref> The size ratio between the two species is within the range of ratios seen between similar-sized living [[Felidae|cats]] and [[Herpestidae|mongooses]] found in the same areas, suggesting that the two species may have been able to occur together.<ref name=Gea141>Goodman et al., 2004, p.&nbsp;141</ref>

With its large size and massive jaws and teeth,<ref name=Gea138>Goodman et al., 2004, p.&nbsp;138</ref> ''C.&nbsp;spelea'' was a formidable, "puma-like"<ref>Goodman, 2003, quoted in Colquhoun, 2006, p.&nbsp;148</ref> predator, and in addition to smaller [[lemurid]]s, it may have eaten some of the big, now extinct [[subfossil lemur]]s that would have been too large for ''C.&nbsp;ferox''.<ref>Goodman et al., 2004, pp.&nbsp;138–140; Colquhoun, 2006, pp.&nbsp;148, 156</ref> No subfossil evidence has been found to definitively show that lemurs were its prey; this assumption is based on the diet of the smaller, extant species of fossa.<ref>Goodman, 2003, p.&nbsp;1227</ref> Other possible prey include [[tenrec]]s, smaller [[euplerid]]s, and even young [[Malagasy hippopotamus]]es.<ref>Alcover and McMinn, 1994, p.&nbsp;14</ref> Its extinction may have changed predation dynamics on Madagascar.<ref>Goodman et al., 2004, p.&nbsp;140</ref>

The [[IUCN Red List]] currently lists ''C.&nbsp;spelea'' as an extinct species; why and when it became extinct remains unknown.<ref name=iucn/> However, local people on Madagascar often recognize two forms of fossa, a larger ''fosa mainty'' (or "black ''Cryptoprocta''") and a smaller ''fosa mena'' (or "reddish ''Cryptoprocta''").<ref>Goodman et al., 2003, p.&nbsp;1168; 2004, p.&nbsp;141</ref>  There are also some anecdotal records of very large living fossas, such as a 2-m (7&nbsp;ft), 30-kg (70&nbsp;lb) fossa at [[Morondava]].  Goodman and colleagues suggested that further research may demonstrate that there is more than one species of fossa yet alive.<ref name=Gea141/>

==References==
{{reflist|colwidth=30em}}

==Literature cited==
*Alcover, J.A. and McMinn, M. 1994. [http://www.jstor.org/stable/1312401 Predators of vertebrates on islands] (subscription required). BioScience 44(1):12–18.
*Burness, G.P., Diamond J. and Flannery, T. 2001. [http://www.jstor.org/stable/3057309 Dinosaurs, dragons, and dwarfs: The evolution of maximal body size] (subscription required). Proceedings of the National Academy of Sciences 98(25):14518–14523.
*Burney, D.A., Burney, L.P., Godfrey, L.R., Jungers, W.L., Goodman, S.M., Wright, H.T. and Jull, A.J.T. 2004. [http://linkinghub.elsevier.com/retrieve/pii/S0047248404000843 A chronology for late prehistoric Madagascar] (subscription required). Journal of Human Evolution 47(1–2):25–63.
*Colquhoun, I.C. 2006. [http://dx.doi.org/10.1159/000089701 Predation and cathemerality: Comparing the impact of predators on the activity patterns of lemurids and ceboids] (subscription required). Folia Primatologica 77(1–2):143–165.
*Garbutt, N. 2007. Mammals of Madagascar: A Complete Guide. A & C Black, 304&nbsp;pp. ISBN 978-0-7136-7043-1
*Goodman, S.M. 2003. Predation on lemurs. Pp.&nbsp;1221–1228 in Goodman, S.M. and Benstead, J.P. (eds.). The Natural History of Madagascar. University of Chicago Press, 1728&nbsp;pp. ISBN 0-226-30306-3
*Goodman, S.M., Ganzhorn, J.U. and Rakotondravony, D. 2003. Introduction to the mammals. Pp.&nbsp;1159–1186 in Goodman, S.M. and Benstead, J.P. (ed.). The Natural History of Madagascar. University of Chicago Press, 1728&nbsp;pp. ISBN 0-226-30306-3
*Goodman, S.M., Rasoloarison, R.M. and Ganzhorn, J.U. 2004. [http://www.mnhn.fr/museum/front/medias/publication/1334_z04n1a9.pdf On the specific identification of subfossil ''Cryptoprocta'' (Mammalia, Carnivora) from Madagascar]. Zoosystema 26(1):129–143.
*Hoffman, M. 2008. {{IUCNlink|136456|Cryptoprocta spelea}}. In IUCN. IUCN Red List of Threatened Species. Version 2009.2. <[http://www.iucnredlist.org/ www.iucnredlist.org]>. Downloaded on May 20, 2010.
*Köhncke, M. and Leonhardt, K. 1986. [http://www.science.smith.edu/departments/Biology/VHAYSSEN/msi/pdf/i0076-3519-254-01-0001.pdf ''Cryptoprocta ferox'']. Mammalian Species 254:1–5.
*Lamberton, C. 1939. Les Cryptoprocta fossiles. Mémoires de l'Académie malgache 27:155–193.
*Turvey, S.T. 2009. Holocene Extinctions. Oxford University Press US, 359&nbsp;pp. ISBN 978-0-19-953509-5
*Wroe, S., Field, J., Fullagar, R. and Jermiin, L.S. 2004. [http://www.informaworld.com/index/795075894.pdf Megafaunal extinction in the late Quaternary and the global overkill hypothesis] (subscription required). Alcheringa 28(1):291–331.
{{featured article}}

[[Category:Euplerids]]
[[Category:Extinct carnivorans]]
[[Category:Holocene extinctions]]
[[Category:Prehistoric animals of Madagascar]]