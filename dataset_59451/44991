{{Use Australian English|date=March 2013}}
{{Use dmy dates|date=March 2013}}
{{Infobox Train
|name            = 2000/2100 class railcars
|image           = AdelaideRail 4.jpg
|imagesize       = 250px
|caption         = 2011 & 2106 at [[Gawler railway station|Gawler station]] in June 2005
|interiorimage   =
|interiorcaption =
|capacity        =  64 passengers (2000)<br>104 passengers (2100)
|maxspeed        =  {{convert|140|km/h|mph|abbr=on}}<br>{{convert|90|km/h|mph|abbr=on}} ''Network Speed''
|formation       =
|replaced        =
|manufacturer    = [[Commonwealth Engineering|Comeng]]
|factory         = [[Granville, New South Wales|Granville]]
|yearconstruction= 1979-1980
|service         = 1980-2015
|yearscrapped    = 2016
|numberbuilt     = 30
|numberservice   = 0
|numberpreserved  = 6
|numberscrapped  = 24
|fleetnumbers    = 2001–2012<br>2101–2118
|depots          = Dry Creek<br>Gawler<br>Lonsdale<br>Belair
|operator        =  [[State Transport Authority (South Australia)|State Transport Authority]]<br>[[TransAdelaide]]
|weight          = {{convert|68|t}} (2000)<br>{{convert|42|t}} (2100)
|carlength       =
|width           =
|height          =
|gauge           = {{RailGauge|1600mm|lk=on}}
|art-sections    =
|engine          =  Originally two  [[MAN Truck & Bus|MAN]] D3650  underfloor [[Turbocharger|turbocharged]] [[diesel engine]]s until the early 1990s<br>after that, two turbocharged [[Cummins]] underfloor diesel engines<br>N/A Trailer
|poweroutput = Originally two x {{convert|377|kW|hp|abbr=on}} until the early 1990s<br>after that, two x {{convert|390|kW|hp|abbr=on}}
|aux             = 175 kVA [[alternator]], N/A Trailer
|powersupply    = [[Volt]]age?
|transmission    = [[Voith]] T420r [[Torque converter|Diesel Hydraulic]] (2000)<br>N/A Trailer (2100)}}

The '''2000/2100 class''' were a class of diesel [[railcar]]s operated by [[Adelaide Metro]]. They were built by [[Commonwealth Engineering|Comeng]], [[Granville, New South Wales|Granville]] in 1979-1980.

==History==
[[File:2000 railcar.jpg|thumbnail|250px|Three car set at [[Lonsdale railway station|Lonsdale station]] in May 2006]]
[[File:2000 class 4car gawler.jpg|thumbnail|250px|Four car set at [[Gawler railway station|Gawler station]] in May 2008]]
The 2000/2100 class are self-propelled diesel railcars operated by [[Adelaide Metro]] on the [[Adelaide]] [[Railways in Adelaide|metropolitan rail network]]. The body shell design was based on the [[Budd SPV-2000]], [[Budd Metroliner|Metroliner]] and [[Amfleet]] cars but the 2000 class railcars have a slightly different curve to the Amfleet. The bodyshells were built by [[Commonwealth Engineering|Comeng]], [[Granville, New South Wales|Granville]]  before they were railed to Adelaide for fitting out. They entered service in 1980. They acquired the nickname "Jumbos" due to the raised [[Control car (rail)|driving cab]], similar to that of the [[Boeing 747]]. This raised cab was designed so that two passengers could sit at the front or rear window.<ref>{{cite book|last=Dunn|first=John|title=Comeng: A History of Commonwealth Engineering Volume 4: 1977-1985|year=2013|publisher=Rosenberg Publishing|location=Kenthurst|pages=16–30|isbn=9781922013514}}</ref>

Twelve 2000 powercars and eighteen 2100 class  trailer cars were built.<ref>[http://www.railsa.org/motive-power/diesel-railcars/2000-class/ 2000 Class] Rail SA</ref><ref>[http://www.railsa.org/motive-power/diesel-railcars/2100-class/ 2100 Class] Rail SA</ref> The powercars were originally powered by V12 turbocharged [[MAN Truck & Bus|MAN]] engines that have since been replaced by two turbocharged 6 cylinder [[Cummins]] engines under the floor driving a [[Voith]] hydraulic transmission. They usually operate in 2-car (power-trailer) or 3-car (trailer-power-trailer) configurations.

From 23 February 2014, these cars were no longer permitted to operate on the [[Belair railway line, Adelaide|Belair]] and [[Seaford railway line|Seaford]] lines due to low clearances as a result of the electrification of these lines. After February 2015, they only operated on the [[Gawler railway line, Adelaide|Gawler]], [[Outer Harbor railway line, Adelaide|Outer Harbor]] and [[Grange railway line, Adelaide|Grange]] lines, with 11 of the original 30 railcars still in service.<ref>"Adelaide Metropolitan News" ''Catch Point'' ([[National Railway Museum, Port Adelaide|National Railway Museum]]) March 2015 page 14</ref> The remaining fleet was retired in August 2015.<ref>"Limited life for 2000 class Jumbo railcars" ''[[Railway Digest]]'' January 2015 page 20</ref>

==Preservation==

2006 and 2112 have been preserved and were delivered to the [[National Railway Museum, Port Adelaide]] in July 2016, while 2010 and 2109 have been allocated to [[SteamRanger]], they were delivered by road in June 2016.<ref>{{Cite web|url=https://twitter.com/MatthewPantelis/status/737899944426799104|title=Matthew Pantelis on Twitter|website=Twitter|access-date=2016-06-07}}</ref>

In addition, both 2009 (cut in half) and 2104 have been donated to the [[South Australian Metropolitan Fire Service]] to be used as training areas. The rest of the railcars were scrapped in June 2016.

==Statistics==
{| class="wikitable"
|-
!colspan=3 align=center bgcolor="#9999ff"|2000/2100 class
|-
| width="30%"|&nbsp;|| '''2000 class''' || '''2100 class'''
|-
|Type:||Diesel hydraulic ||Trailer
|-
|Track gauge:||width="33%"|{{RailGauge|1600mm|allk=on}} ||width="33%"| {{RailGauge|1600mm|allk=on}}
|-
|Transmission:||[[Voith]] T420r || N/A – Trailer
|-
|Power plant:||Two turbocharged [[Cummins]] 6-cylinder {{convert|390|kW|hp|abbr=on}} underfloor diesel engines [These replaced the original {{convert|377|kW|abbr=on}} [[MAN SE|MAN]] D3650 turbocharged engines in the early 1990s], plus two [[torque converter]]s and 175 kVA [[alternator]] || N/A – Trailer
|-
|Maximum speed:|| {{convert|140|km/h|mph|abbr=on}} (conservative) but limited to {{convert|90|km/h|mph|abbr=on}} in service || Same
|-
|Number in class:|| 3 (originally 12) || 3 (originally 18)
|-
|Unit numbers:|| 2001–2012 || 2101–2118
|-
|Introduced:|| 1980 || 1980
|-
|Built by: || [[Commonwealth Engineering|Comeng]] || [[Commonwealth Engineering|Comeng]]
|-
|Passenger [[seating capacity]]: || 64 || 98 (90 in 2103, 2112 & 2116)
|-
|Weight: || {{convert|68|t}} || {{convert|40|to|42|t}}
|-
|}

==References==
{{Reflist}}
*State Transport Authority Working Timetable – Book No. 15 (General Instruction and Addenda to Working Timetable – 12 February 1984)
*State Transport Authority Working Timetable – Book No. 26 (General Instruction and Addenda to Working Timetable – 9 October 1988)

==Further reading==
"2000 Class Railcars", ''[[Australian Railway History|Australian Railway Historical Society Bulletin]]'' March 1988 pp&nbsp;49–69

{{South Australian Railways locos|state=collapsed}}

[[Category:Diesel multiple units of South Australia|2000 class]]
[[Category:Transport in Adelaide]]