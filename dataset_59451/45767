{{Infobox aviator
| full_name        = Jeana Lee Yeager
| image            = JeanaYeager.jpg
| image_size       = 250
| birth_date       = {{birth date and age|1952|05|18}}
| birth_place      = [[Fort Worth]], [[Texas]]<ref name="India Today">{{cite web | url=http://indiatoday.intoday.in/education/story/jeana-yeager-63rd-birth-anniversary-all-you-should-know/1/438683.html | title=Jeana Yeager: All you should know about the  first woman to fly around the World Non-stop : Worldly Science | publisher=INDIATODAY.IN | date=Oct 23, 2015}}</ref>
| famous_flights   = The first non-stop, non-refueled flight around the world with [[Dick Rutan]]
| license_date     = 1978
| awards           = [[Presidential Citizens Medal]]<br>[[Harmon Trophy]]<br>[[Fédération Aéronautique Internationale|FAI]] De la Vaulx Medal<br> [[Collier Trophy]]<br>[[Edward Longstreth Medal]]
| spouse           = ? (1971–1976)<br> William Z. Williams (1992–1994)<br> Dale A. Rinehart (1994–1996)<ref>{{cite web
 | url =http://www.ustaxcourt.gov/InOpHistoric/rinehart8.TCM.WPD.pdf#page=4
 | title =T.C. Memo. 2003-109
 | date =April 18, 2003
 | publisher =United States Tax Court
 | access-date = Oct 23, 2015
}}</ref>
}}

'''Jeana Yeager''' is an American [[aviator]].  She co-piloted, along with [[Dick Rutan]], the first non-stop, non-refueled flight around the world in the [[Rutan Voyager]] aircraft from  December 14 to 23, 1986.<ref>{{cite web | url = http://www.centennialofflight.gov/essay/Explorers_Record_Setters_and_Daredevils/rutan/EX32.htm | first = David H | last = Onkst | title = Dick Rutan, Jeana Yeager, and the Flight of the Voyager | publisher = U.S. Centennial of Flight Commission}}</ref> The flight took 9 days, 3 minutes, and 44 seconds and covered 24,986 miles (40,211&nbsp;km), more than doubling the old [[Flight distance record|distance record]] set by a [[Boeing B-52]] [[bomber]] in 1962. 

==Early life==
Jeana Lee Yeager was born on May 18, 1952, in Fort Worth, Texas, moving with her family to Garland, Texas; Oxnard, California; and Commerce, Texas.  She is not related to aviator [[Chuck Yeager]].<ref>{{cite news |title=Jeana Yeager Was Not Just Along for the Ride |url=http://articles.latimes.com/1986-12-24/news/mn-236_1_jeana-yeager |accessdate=February 26, 2016 |work=Los Angeles Times |date=December 24, 1986}}</ref> Following graduation from high school, Yeager at age 19 married a police officer, but the couple divorced after five years of marriage.<ref name="India Today" />  She worked as a draftsman and surveyor for a geothermal energy company in Santa Rosa, California, where she obtained her private pilot's license in 1978.<ref name="Gathering of Eagles">{{cite web | url=http://www.goefoundation.org/index.php/eagles/biographies/y/yeager-jeana-l./ | title=Gathering of Eagles Foundation :: Yeager, Jeana L. | publisher=Gathering of Eagles Foundation | accessdate=October 23, 2015}}</ref>  

Jeana went to work for [[Robert Truax]] who was developing a reusable spacecraft. She met [[Dick Rutan]] in 1980 and they soon both set distance records in the [[Rutan VariEze]] and [[Rutan Long-EZ|Long-EZ]] planes, designed by Dick's brother [[Burt Rutan]]. In early 1982, Jeana set a new women's speed record for the 2,000-kilometer closed course and in the fall of 1984 using the VariEze, she set the open-distance record of 2,427.1 statute miles.<ref name="Gathering of Eagles" /><ref name="FAI">{{cite web | url=http://www.fai.org/record-powered-aeroplanes | title=Powered Aeroplanes World Records | publisher=Fédération Aéronautique Internationale | accessdate=October 23, 2015 | pages=Yeager - C-1b - Landplanes: take off weight 500 to 1000kg}}</ref>

==Round-the-world flight==
[[File:VoyagerAircraftAtNASM-common.jpg|right| 250 px|thumb|The [[Rutan Voyager]]]]
Jeana and Dick Rutan decided to attempt to fly around the world without refueling. They formed Voyager Aircraft, Inc., and Burt Rutan began designing the aircraft. Initially unable to find a commercial sponsor, Jeana started the Voyager Impressive People (VIP) program which became the major source of money to build, test, and fly the aircraft. By mid-1986, Voyager was ready for the flight. She flew as copilot on the 216-hour flight and set a world's absolute distance record--the first time a woman had been listed in an absolute category.

==Awards==
In recognition of the Voyager flight, she received the [[Harmon Trophy]], the [[Fédération Aéronautique Internationale|FAI]] [[De la Vaulx Medal]], the [[Presidential Citizens Medal]] from President [[Ronald Reagan]] (1986) and is the first woman to have received the [[Collier Trophy]]--receiving the latter two honors along with Dick and Burt Rutan.  She was also awarded the [[Edward Longstreth Medal]] from the [[Franklin Institute]] in 1988.<ref name="LongstrethMedal_Laureates">{{cite web|url=http://www.fi.edu/winners/show_results.faw?gs=&ln=&fn=&keyword=&subject=&award=LONG+&sy=1987&ey=1989&name=Submit |title=Franklin Laureate Database - Edward Longstreth Medal 1988 Laureates |publisher=[[Franklin Institute]] |accessdate=November 14, 2011}}</ref>

==References==
{{Reflist}}


{{DEFAULTSORT:Yeager, Jeana}}
[[Category:1952 births]]
[[Category:Living people]]
[[Category:American female aviators]]
[[Category:Aviators from Texas]]
[[Category:Harmon Trophy winners]]
[[Category:Collier Trophy recipients]]
[[Category:Flight distance record holders]]
[[Category:People from Fort Worth, Texas]]
[[Category:Presidential Citizens Medal recipients]]
[[Category:TOYP Awardees]]