{{pp-move-indef}}
{{Infobox earthquake
|title = 1997 Qayen earthquake
|date = {{start-date| May 10, 1997}}
|origintime = 12:57 local time
|map2 = {{Location map+ | Iran
|places =
{{Location map~|Iran|lat=36.3|long=59.6|label=Mashhad|position=left|mark=Green pog.svg}}
{{Location map~|Iran|lat=30.28|long=57.08|label=Kerman|mark=Green pog.svg}}
{{Location map~|Iran|lat=31.9|long=54.37|label=Yazd|position=left|mark=Green pog.svg}}
{{Location map~|Iran|lat=33.82|long=59.8|mark=Bullseye1.png|marksize=40}}
| relief = yes
| width = 250
| float = right
| caption =}}
|caption = Map showing the earthquake's epicenter
|map alt = The earthquake's epicenter is shown in northeastern Iran. Just next to the earthquake is a mountain ridge. To the west is the Afghan border.
|magnitude = 7.3 [[Moment magnitude scale|M<sub>w</sub>]]
|depth = {{convert|10|km|mi|0|abbr=on}}
|location = {{Coord|33.82|N|59.80|E |region:IR_type:event |display=inline,title}}
|type = [[Fault (geology)#Strike-slip faults|Strike-slip]]
|countries affected = [[Iran]] <br> [[Afghanistan]]
|damages = $100 million USD
|intensity = [[Mercalli intensity scale|X (''Extreme'')]]
|casualties = at least 1,567 dead, 2,300 injured, and around 50,000 homeless<ref name=USGS1>{{cite web|url=http://neic.usgs.gov/neis/eq_depot/1997/eq_970510/|title=Magnitude 7.3 Northern Iran 1997 May 10 07:57:29 UTC Preliminary Earthquake Report|work=[[United States Geological Survey]]|date=April 22, 2004|accessdate=October 18, 2008| archiveurl= https://web.archive.org/web/20081110060053/http://neic.usgs.gov/neis/eq_depot/1997/eq_970510/| archivedate= November 10, 2008 <!--DASHBot-->| deadurl= no}}</ref><ref name=EHP/>
}}

The '''Qayen earthquake''', also known as the '''Ardekul''' or '''Qaen earthquake''', struck Northern [[Iran]]'s [[Khorasan Province]] on May 10, 1997, at 07:57 [[Coordinated universal time|UTC]] (12:57 local time).<ref name=USGS1/><ref name=ldeo/> The largest in the area since 1990, the earthquake measured 7.3 on the [[moment magnitude scale]] and was centered approximately {{convert|270|km|mi|-1|sp=us}} south of [[Mashhad]] on the village of Ardekul. The third earthquake that year to cause severe damage, it devastated the Birjand–[[Qayen, Iran|Qayen]] region, killing 1,567 and injuring more than 2,300. The earthquake—which left 50,000&nbsp;homeless and damaged or destroyed over 15,000&nbsp;homes—was described as the deadliest of 1997 by the [[United States Geological Survey]].<ref name=EHP>{{cite web|url=http://neic.usgs.gov/neis/eq_depot/1997/|title=Earthquake Information for 1997|work=[[United States Geological Survey]]|date=May 2, 2004|accessdate=January 19, 2009| archiveurl= https://web.archive.org/web/20090125162621/http://neic.usgs.gov/neis/eq_depot/1997/| archivedate= January 25, 2009 <!--DASHBot-->| deadurl= no}}</ref> Some 155&nbsp;[[aftershock]]s caused further destruction and drove away survivors. The earthquake was later discovered to have been caused by a rupture along a [[Fault (geology)|fault]] that runs underneath the Iran–Afghanistan border.

Damage was eventually estimated at $100&nbsp;million, and many countries responded to the emergency with donations of blankets, tents, clothing, and food. Rescue teams were also dispatched to assist local volunteers in finding survivors trapped under the debris. The destruction around the earthquake's [[epicenter]] was, in places, almost total; this has been attributed to poor construction practices in rural areas, and imparted momentum to a growing movement for changes in [[building codes]]. With 1 in 3,000 deaths in Iran attributable to earthquakes, a US [[geophysicist]] has suggested that a country-wide rebuilding program would be needed to address the ongoing public safety concerns.

== Background and geology ==
Iran experiences regular earthquakes, with 200 reported in 1996 alone.<ref name=wfn/> Like dozens that had [[List of earthquakes in Iran|preceded it]], the 1997 Qayen event was of significant magnitude.<ref name=Adsabs>{{Cite web|url=http://www.iiees.ac.ir/iiees/English/Publication/jsee/jsee10_1.html|title=Aftershock Analysis for the 1997 Ghaen–Birjand (Ardekul earthquake)|work=[[Harvard University]]|author=Amir Mansour Farahbod|author2=Conrad Lindholm|author3=Mohammad Mokhtari|author4=Hilmar Bungum|last-author-amp=yes|accessdate=January 21, 2009}}</ref> It occurred on Saturday, May 10, 1997, at 12:57 [[IRST]] in the [[Sistan]] region, one of the most seismically active areas of the country.<ref name=Berberian/>  The first major earthquake in that region since 1979, it registered 7.3 on the [[moment magnitude scale]] (M<sub>w</sub>), 7.2 on the [[surface wave magnitude]] scale (M<sub>s</sub>), 7.7 on the energy magnitude scale (M<sub>e</sub>), and had a maximum perceived intensity of ''X'', or ''Extreme'' on the [[Mercalli intensity scale]].<ref name=Adsabs/>
[[File:Crocus sativus2.jpg|thumb|left|[[Saffron]] was a spice farmed by some of the earthquake victims.|alt=Two purple flowers rise above a variety of weeds. The stigmas of the plant are visible.]]

The earthquake was caused by a rupture along the [[Abiz Fault]], part of the Sistan [[suture zone]] of eastern Iran.<ref name=Berberian>{{cite journal|last=Berberian|first=M.|author2=Jackson, J.A.|author3=Qoarashi, M|title=The 1997 May 10 Zirkuh (Qa'enat) earthquake (Mw 7.2): faulting along the Sistan suture zone of eastern Iran|year=1999 |journal=Geophysical Journal International |volume=136|pages=671–694|doi=10.1046/j.1365-246x.1999.00762.x|url=http://www.discountcardonline.com/user/1999_zirkuh_gji.pdf| accessdate=November 28, 2009 <!--DASHBot-->|bibcode = 1999GeoJI.136..671B |display-authors=etal}}</ref> Located northeast of the main collision zone between the [[Arabian Plate|Arabian]] and [[Eurasian plate|Eurasian]] [[Plate tectonics|tectonic plate]]s, the Sistan zone marks the eastern boundary of the [[Iranian Plate|Iranian microplate]] where it intersects with the Afghan [[crustal block]].<ref name=ldeo/> Most of Iran is contained on one microplate, causing seismic activity mainly along its borders. Both the [[1968 Dasht-e Bayaz and Ferdows earthquake|1968 Dasht-e-Bayez earthquake]] (magnitude 7.3, resulting in 12,000–20,000&nbsp;deaths) and the Qayen earthquake were the results of [[Fault (geology)#Strike-slip faults|strike-slip faults]], meaning that the crustal blocks on either side of the faults shifted against each other horizontally.<ref name=USGS2/> The Qayen earthquake was caused by [[dextral|right lateral]] movement along the Abiz Fault.<ref name=CiNii>{{cite journal|title=Preliminary Report of The Damage Due To The Qayen Earthquake of 1997, Northeast Iran|author=Hakuno, Motohiko|author2=Ikeda, Yasutaka|author3=Meguro, Kimiro|url=http://ci.nii.ac.jp/naid/110002674231/|journal=Journal of natural disaster science|publisher=Japan Society for Natural Disaster Science|volume=19|issue=1|pages=67–81|accessdate=October 10, 2009|year=1997|display-authors=etal}}</ref> In addition to the dominant strike-slip displacement, there was also local evidence of [[Fault (geology)#Dip-slip faults|reverse faulting]]. The average displacement of about 2 m indicates a low static stress drop, more consistent with an [[interplate earthquake]] than an [[Intraplate earthquake|intraplate event]].<ref name=Berberian/> The maximum horizontal acceleration during the quake was approximately 6.9 meters per second—nearly three-quarters of the acceleration an object would have in [[gravitational acceleration|free fall]]—and occurred near the earthquake's epicenter. The [[crust (geology)|crustal layer]] involved in the rupture was {{convert|20|–|25|km|sp=us}} thick.<ref name=ldeo>{{cite web |url=http://www.ldeo.columbia.edu/~mwest/museum/description.html |title=The Ardekul, Iran earthquake—May 10, 1997 Description of earthquake and area of impact |publisher=[[Lamont Doherty Earth Observatory]] ([[Columbia University]]) |date=May 5, 1999 |accessdate=October 18, 2008}}</ref> The [[Fault (geology)|surface fault]] that caused the earthquake extended for {{convert|110|km|mi|0|sp=us}}, which was longer than would be expected given the earthquake's magnitude.<ref name=CiNii/> There were at least 155&nbsp;[[aftershock]]s, reaching a magnitude of up to 5.5 on the Richter magnitude scale.<ref name=Toll/> Many of the aftershocks occurred along the rupture up to {{convert|24|km|mi|sp=us}} below the surface.<ref name=ldeo/>

The earthquake's epicenter was within the village of Ardekul<ref name=Adsabs/> in South Khorasan Province, which borders Afghanistan.<ref name=Toll>{{cite news|url=https://query.nytimes.com/gst/fullpage.html?res=9A01E4D91039F931A25756C0A961958260&n=Top/News/Science/Topics/Earthquakes&scp=4&sq=1997%20iran%20earthquake&st=cse|title=Earthquake Toll in Iran Estimated to Climb to 2,400|work=The New York Times|date=May 12, 1997|accessdate=January 19, 2009}}</ref> The village is isolated between mountains and hills.  Although the Iranian government had distributed more than 800 [[seismograph]]s throughout the country, few had been placed in the Qayen region due to its  [[desert climate]] and the remoteness of the area.<ref name=CiNii/>

As a result of the dry climate, timber—a main component in building earthquake-resistant homes—is scarce in Qayen; homes are instead constructed of [[adobe]].<ref name=CiNii/> The inhabitants of the poverty-stricken region rely on [[subsistence agriculture|subsistence farming]], raising livestock and crops such as wheat and [[saffron]].<ref name=TollPT/>  When the earthquake struck, much of the population was already working in the fields;<!--this makes implications about the time of day the quake hit, but the time isn't mentioned above--> for the most part, these people survived.<ref name=CiNii/> Many of those treated for injuries were found to be undernourished.<ref name=TollPT/>{{clear}}

== Damage and casualties ==
The earthquake was felt over an area of {{convert|500000|km2|mi2|0|sp=us}}, including in the cities of [[Mashhad]], [[Kerman]] and [[Yazd]]. Destruction was most severe within a {{convert|60|mi|km|sigfig=1|sp=us|adj=on|order=flip}} strip between the epicenter and [[Birjand, Iran|Birjand]]. The tremors triggered landslides across the region and proved highly destructive to the region's mud-hut buildings. Entire streets were reduced to rubble,<ref name=Toll/> and in one village, 110&nbsp;young girls were killed when their elementary school collapsed.<ref name=DailyNews />

An initial report in ''[[The New York Times]]'' claimed that more than 2,000 people had died in the worst-affected area, with a further 394 in Birjand and two in the small town of Khavaf. The earthquake was also said to have caused five fatalities in Afghanistan.<ref name=Toll/> As rescue efforts proceeded these figures were revised; the United States Geological Survey states that 1,572{{refn|1567 deaths in Iran and a further five in the Herat area of Afghanistan.<ref name=USGS2 />|group=nb}} people were killed and as many as 2,300 injured.<ref name=USGS2/>  As bodies were retrieved, they were buried in mass graves.<ref name=Toll/>  Officials worried that a temperature fluctuation—from {{convert|41|to|84|F|C|order=flip}} on the day of the earthquake—would cause the corpses to rot more quickly, spreading infection.<ref name=TollPT>{{Cite news |url=https://query.nytimes.com/gst/fullpage.html?res=9A01E4D91039F931A25756C0A961958260&sec=&spon=&pagewanted=2 |title=Earthquake Toll in Iran Estimated to Rise to 2,400 |page=2 |work=The New York Times |date=May 12, 1997 |accessdate=January 19, 2009}}</ref>

[[File:Qayen.JPG|thumb||400px|Southwest view of Qayen in 2006|alt=Sparse brown vegetation in dusty soil fills the foreground, fading to distant mountains along the horizon. A barely discernible scattering of buildings is in the middle-distance.]]

Many villages lost both power and water, leaving survivors unable to fend for themselves.<ref name=DailyNews/>  The injured were often up to {{convert|90|mi|km|order=flip|sp=us}} away from the nearest hospital.<ref name=Toll/><ref name=DailyNews/> One doctor, highlighting the desperate need for physicians to treat the injured, said "I don't know how many casts I have done today, but it seems like hundreds."<ref name=DailyNews>{{Cite news |url=http://www.thefreelibrary.com/7.1+QUAKE+LEVELS+IRANIAN+VILLAGES%3B+THOUSANDS+KILLED%3B+PLEA+FOR+AID...-a083867070 |title=7.1 Quake Levels Iranian Villages; Thousands Killed; Plea For Aid Issued |work=Daily News |accessdate=March 21, 2009}}</ref>
The extensive aftershocks prompted survivors to leave the vicinity of their homes and take to tents.<ref name=Toll/> Several days later, another earthquake of magnitude 4.8 struck.<ref name=ReliefWeb/> In the wake of the earthquake and its aftershocks, every one of the 700&nbsp;houses in the tiny village of Abiz, {{convert|90|km|mi|...|sp=us}} east of Qayen, was destroyed, and 400 of its 1200 residents killed.<ref name=ldeo/>

According to an Iranian radio station report, 200 villages sustained severe damage or were totally destroyed.<ref name=DailyNews/>  The United States Geological Survey estimated that 10,533&nbsp;houses were destroyed; an additional 5,474 homes sustained varying degrees of damage.  Fifty thousand people were left homeless.<ref name=USGS2>{{cite web |url=http://earthquake.usgs.gov/regional/world/world_deaths.php |title=Earthquakes with 1,000 or More Deaths since 1900 |publisher=[[United States Geological Survey]] |date=July 16, 2008 |accessdate=October 18, 2008| archiveurl= https://web.archive.org/web/20081011012207/http://www.earthquake.usgs.gov/regional/world/world_deaths.php| archivedate= October 11, 2008 <!--DASHBot-->| deadurl= no}}</ref>  Local officials initially estimated the cost of the damage at $67&nbsp;million 1997 US$ (roughly 89.5&nbsp;million 2008 USD).<ref name=Toll/> The estimate was later raised to 100&nbsp;million 1997 USD (roughly 133.6&nbsp;million 2008 USD). One hundred schools and many health centers in the stricken areas were discovered to be in need of repair work.<ref name=ReliefWeb>{{Cite web |url=http://www.reliefweb.int/rw/rwb.nsf/db900SID/SKAR-654K95?OpenDocument |title=Iran Earthquake Appeal |publisher=ReliefWeb |date=May 16, 1997 |accessdate=March 21, 2009}}</ref>

Many of the more seriously damaged homes were of simple construction, with walls made of mud, adobe, or brick packed 40–50&nbsp;cm (16–20&nbsp;in) thick.  These materials are generally more vulnerable to the force of the earthquake. However, some of the traditionally constructed homes sustained little or no damage.  This was due to a range of factors, possibly including the height-to-width ratio, the lack of windows, and the quality of the materials used.<ref name=WorldConf>{{Cite report |title=Performance of Traditional Arches and Domes in Recent Iranian Earthquakes |url= http://www.archidev.org/IMG/pdf/3.pdf|author=Mahdi,T.|year=2004 |publisher=13th World Conference on Earthquake Engineering |accessdate=November 25, 2009}}</ref> In general, reinforced concrete-framed homes, built after the 1979 earthquake, were better able to withstand the earthquake. Those near the epicenter still sustained severe damage due to the weight of the roofs and the weak joint connections between major structural elements of the buildings.<ref name=CiNii/>

== Relief efforts ==
<div style="float:right; width:25%">
{| class="wikitable" border="1"
|-
!Organization/Country
!Amount donated
|-
|United Nations-related agencies||$525,000
|-
|Japan||$300,000
|-
|[[Organization of Petroleum Exporting Countries|OPEC countries]]||$300,000
|-
|Germany||$235,000
|-
|United Kingdom||$200,000
|-
|Norway||$90,000
|-
|Denmark||$35,000
|-
|International Red Cross and Red Crescent Organizations||$33,800
|-
|United States||$25,000
|}
</div>
Several thousand volunteers were brought in to join the search for survivors buried under mounds of brick and cement debris.  Many volunteers worked with nothing but their hands. Local organizations, including the Iranian [[Red Crescent]], sent 9,000&nbsp;tents, more than 18,000&nbsp;blankets, canned food, rice, and dates.<ref name=Toll/> An additional 80&nbsp;tons of supplies were sent by the Iranian government to Mashhad, from where the relief efforts were being coordinated.<ref>{{Cite news |title=Iran reaches for international aid after a devastating quake |date=May 12, 1997 |work=[[Christian Science Monitor]]}}</ref>

The United Nations Secretary General, [[Kofi Annan]], pleaded that the international community "respond promptly and with generosity".<ref name=Toll/> France dispatched a cargo plane filled with blankets, tents, clothing, and food, while Swiss authorities sent a rescue team with dogs trained in search-and-rescue.<ref name=Toll/> Several aircraft carrying tents, blankets, and kerosene stoves from European and Arab countries arrived in Mashad on May 14.<ref name="searchclose"/> Representatives from the United States, calling the disaster a "humanitarian issue", said that despite their strong differences with Iran they would donate supplies and other aid if requested.<ref name=Toll/> The [[Mennonite Central Committee]], an American relief agency stationed in [[Akron, Pennsylvania]], redirected to the relief effort 400 metric tons of lentils and cooking oils intended for immigrant refugees.<ref name=wfn>{{cite news |url=http://www.wfn.org/1997/05/msg00099.html |last=Sensenzig|first=Pearl |title=MCC offers assistance to Iranian earthquake victims |date=May 19, 1997 |work=Worldwide Faith News |accessdate=January 21, 2009}}</ref> A specialist British disaster rescue organisation, the [[International Rescue Corps]], offered to send a team but were refused visas (with the reasoning that "enough rescue crews had already arrived at the disaster site")<ref name=Toll/> and a Swiss offer of additional assistance was also turned down. Several countries within the [[Persian Gulf]] political region sent condolences to the families of victims and the government in the area.<ref name=Toll/>

Because the affected area is extremely remote, distributing the relief supplies was difficult.  Reaching some villages would require a five-hour drive over unpaved roads, some of which had collapsed or had been covered by landslides during the earthquake.  Helicopters were used to provide supplies to some otherwise inaccessible areas.<ref name=DailyNews/>

Although aid operations continued for some time, the Iranian government ceased rescue work on May 14.  No more survivors were expected to be found in the rubble.<ref name="searchclose">{{cite news |url=http://docs.newsbank.com/openurl?ctx_ver=z39.88-2004&rft_id=info:sid/iw.newsbank.com:NewsBank:DHSB&rft_val_format=info:ofi/fmt:kev:mtx:ctx&rft_dat=1002F9248780472A&svc_dat=InfoWeb:aggregated5&req_dat=0D0CB579A3BDA420 |title=Search for quake survivors closes |date=May 15, 1997 |work=[[The Herald-Sun (Durham, North Carolina)|The Herald-Sun]]  |page=A3 |accessdate=January 22, 2009}}</ref>

== Future threats ==
Iran was listed as "the worst offender" in a 2004 report on countries with poor [[earthquake engineering]].<ref name=SciD>{{cite news|url=http://www.sciencedaily.com/releases/2004/12/041219182609.htm|title=Contractor Ignorance Kills Earthquake Victims In Sesmic Zones, Says U. Of Colorado Professor|publisher=LiveScience|date=December 28, 2004|accessdate=October 11, 2009}}</ref>  Professor Roger Bilham of the [[University of Colorado at Boulder]], a [[geophysicist]] who specializes in earthquake-related deformation and hazards, blames construction practices for the fact that since the start of the 20th century, 1 in 3,000 Iranians has died in an earthquake-related incident.  Bilham argues that "Most of Iran needs rebuilding."<ref name=SciD/> The United Nations have prepared a Common Country Assessment for Iran, which likewise states that, "While adequate building regulations exist for large cities, it is generally believed that they are not rigorously adhered to... most of those who have suffered in recent major earthquakes have lived in small towns and villages. Earthquake-proof construction is very rare in those areas and adequate building regulations are not yet in place".<ref name=UN>{{cite web|title= United Nations Common Country Assessment for the Islamic Republic of Iran|url=http://www.undp.org.ir/DocCenter/reports/npd/CCA.pdf|date=August 2003|publisher=United Nations|accessdate=November 23, 2009}}</ref> An analysis of the performance of traditional buildings during the earthquake concluded that several factors, including high construction costs, poor materials, a shortage of skills in rural areas, and a lack of building regulations governing traditional construction techniques, have led to a deterioration in the quality of such buildings. The study recommended regulations to govern the construction of traditional arches and domes.<ref name=WorldConf/>

The earthquakes of Iran are a large concern to the populace and are an impediment to economic development. Twelve earthquakes with a Richter magnitude of over seven have occurred within the last century. Three-quarters of the major cities of Iran are in areas prone to major earthquakes. The [[1990 Manjil–Rudbar earthquake]], with at least 42,000 fatalities, cost Iran roughly 7.2 percent of its [[Gross National Product]] (GNP) for that year and wiped out two years of economic growth.<ref name=UN/>

In 2007, the Asian Centre on Seismic Risk Reduction was formed in response to the regular earthquakes experienced by the southern, southwestern, and central Asian areas. This organization exists to "encourage regional and inter-regional networking and partnerships to reduce seismic damage". Earthquakes account for 73 percent of natural disaster deaths in the area.<ref>{{cite news|url=http://www.un.org/apps/news/story.asp?NewsID=22486&Cr=disaster&Cr1=natural|title=Iran, UN agencies launch first regional seismic risk reduction centre|publisher=United Nations|date=May 9, 2007|accessdate=November 14, 2009}}</ref>

== Footnotes ==
{{Reflist|group=nb}}

== References ==
{{Reflist|colwidth=30em}}
{{Use mdy dates|date=May 2012}}

{{Featured article}}

{{Earthquakes in 1997}}
{{Earthquakes in Iran}}

{{DEFAULTSORT:1997 Qayen Earthquake}}
[[Category:1997 earthquakes|Qayen]]
[[Category:1997 in Iran|Qayen earthquake]]
[[Category:Earthquakes in Iran]]
[[Category:History of South Khorasan Province]]
[[Category:May 1997 events]]