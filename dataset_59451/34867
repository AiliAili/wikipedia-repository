{{good article}}
{{Infobox hurricane season
| Basin=Atl
| Year=1913
| Track=1913 Atlantic hurricane season summary map.png
| First storm formed=May 5, 1913
| Last storm dissipated=October 30, 1913
| Strongest storm name=Four
| Strongest storm pressure=976
| Strongest storm winds=75
| Total depressions=10
| Total storms=6
| Total hurricanes=4
| Damagespre=At least
| Damages=4
| Fatalities=6
| five seasons=[[1911 Atlantic hurricane season|1911]], [[1912 Atlantic hurricane season|1912]], '''1913''', [[1914 Atlantic hurricane season|1914]], [[1915 Atlantic hurricane season|1915]]
}}
The '''1913 Atlantic hurricane season''' was the third consecutive year with a [[tropical cyclone]] developing before June. Although no "hurricane season" was defined at the time, the present-day delineation of such is June&nbsp;1 to November&nbsp;30.<ref name="FAQG1">{{cite web|author=Neal Dorst|publisher=National Oceanic and Atmospheric Administration|year=1993|accessdate=March 9, 2009|title=When is hurricane season ?|url=http://www.aoml.noaa.gov/hrd/tcfaq/G1.html| archiveurl= https://web.archive.org/web/20090308083302/http://www.aoml.noaa.gov/hrd/tcfaq/G1.html| archivedate= 8 March 2009 <!--DASHBot-->| deadurl= no}}</ref> The first system, a tropical depression, developed on May&nbsp;5 while the last transitioned into an [[extratropical cyclone]] on October&nbsp;30. Of note, the seventh and eighth cyclones existed simultaneously from August&nbsp;30 to September&nbsp;4.

Of the season's ten tropical cyclones, six became tropical storms and four strengthened into hurricanes. Furthermore, none of these strengthened into a major hurricane—Category&nbsp;3 or higher on the modern-day [[Saffir–Simpson hurricane wind scale]]—marking the sixth such occurrence since [[1900 Atlantic hurricane season|1900]].<ref name="ACE"/> The strongest hurricane of the season peaked as only a Category&nbsp;1 with winds of 85&nbsp;mph (140&nbsp;km/h). That system left five deaths and at least $4&nbsp;million in damage in [[North Carolina]]. The first hurricane of the season also caused one fatality in [[Texas]], while damage in [[South Carolina]] from the fifth hurricane reached at least $75,000.

The season's activity was reflected with an [[accumulated cyclone energy]] (ACE) rating of 36.<ref name="ACE">{{cite report|work=[[Hurricane Research Division]]; [[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|date=February 2014|title=Atlantic basin Comparison of Original and Revised HURDAT|accessdate=July 26, 2014|url=http://www.aoml.noaa.gov/hrd/hurdat/comparison_table.html|location=Miami, Florida}}</ref> ACE is, broadly speaking, a measure of the power of the hurricane multiplied by the length of time it existed, so storms that last a long time, as well as particularly strong hurricanes, have high ACEs. It is only calculated for full advisories on tropical systems at or exceeding 39&nbsp;mph (63&nbsp;km/h), which is tropical storm strength.<ref>{{cite report|author=David Levinson|date=August 20, 2008|title=2005 Atlantic Ocean Tropical Cyclones|publisher=National Oceanic and Atmospheric Administration|work=[[National Climatic Data Center]]|accessdate=July 26, 2014|url=http://www.ncdc.noaa.gov/oa/climate/research/2005/2005-atlantic-trop-cyclones.html|location=Asheville, North Carolina}}</ref>

==Systems==
<center><timeline>
ImageSize = width:800 height:225
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270
AlignBars = early

DateFormat = dd/mm/yyyy
Period     = from:01/05/1913 till:01/12/1913
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/05/1913

Colors     =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_(TD)_=_&lt;39_mph_(0–62_km/h)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–209-km/h)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–155_mph_(210–249_km/h)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_&gt;=156_mph_(&gt;=250_km/h)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=

  barset:Hurricane width:10 align:left fontsize:S shift:(4,-4) anchor:till
  from:05/05/1913 till:08/05/1913 color:TD text:"TD"
  from:13/06/1913 till:17/06/1913 color:TD text:"TD"
  from:21/06/1913 till:29/06/1913 color:C1 text:"Hurricane #1" (C1)
  from:20/07/1913 till:23/07/1913 color:TD text:"TD"
  from:04/08/1913 till:07/08/1913 color:TD text:"TD"
  from:14/08/1913 till:16/08/1913 color:TS text:"Tropical Storm #2" (TS)
  from:26/08/1913 till:12/09/1913 color:TS text:"Tropical Storm #3" (TS)
  from:30/08/1913 till:04/09/1913 color:C1 text:"Hurricane #4" (C1)
  from:06/10/1913 till:10/10/1913 color:C1 text:"Hurricane #5" (C1)
  from:28/10/1913 till:30/10/1913 color:C1 text:"Hurricane #6" (C1)

  bar:Month width:15 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/05/1913 till:01/06/1913 text:May
  from:01/06/1913 till:01/07/1913 text:June
  from:01/07/1913 till:01/08/1913 text:July
  from:01/08/1913 till:01/09/1913 text:August
  from:01/09/1913 till:01/10/1913 text:September
  from:01/10/1913 till:01/11/1913 text:October
  from:01/11/1913 till:01/12/1913 text:November
TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir–Simpson Hurricane Scale]])"

</timeline>
</center>

===Hurricane One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic hurricane 1 track.png
|Formed=June 21
|Dissipated=June 29
|1-min winds=65
|Pressure=988
}}
Weather maps and ship data indicate the development of a tropical depression in the southwestern [[Caribbean Sea]] around 06:00&nbsp;[[Coordinated Universal Time|UTC]] on June&nbsp;21.<ref name="documentation">{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/metadata_dec12.html|title=Documentation of Atlantic Tropical Cyclones Changes in HURDAT|author=Christopher W. Landsea|date=December 2012|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|accessdate=July 23, 2014|location=Miami, Florida|display-authors=etal}}</ref> Moving north-northwestward, the depression accelerated and intensified into a tropical storm on the following day. Early on June&nbsp;23, the storm made [[Landfall (meteorology)|landfall]] near the [[Honduras]]–[[Nicaragua]] border with winds of 40&nbsp;mph (65&nbsp;km/h). Thereafter, it continued north-northwestward and oscillated slightly in strength. The system made another landfall near [[Cancún]], [[Quintana Roo]], with winds of 45&nbsp;mph (75&nbsp;km/h) late on June&nbsp;25. After briefly crossing the [[Yucatan Peninsula]], the cyclone emerged into the [[Gulf of Mexico]] and eventually began moving more to the west-northwest. Early on June&nbsp;27, it deepened into a Category&nbsp;1 hurricane and peaked with [[maximum sustained wind]]s of 75&nbsp;mph (120&nbsp;km/h). The hurricane then curved northwestward and made [[landfall]] in [[Padre Island]], [[Texas]], at the same intensity around 01:00&nbsp;UTC on June&nbsp;28. After moving inland, the storm quickly weakened and dissipated over [[Val Verde County, Texas|Val Verde County]] just under 24&nbsp;hours later.{{Atlantic hurricane best track}}

Impact in Central America and Mexico is unknown. The storm brought heavy rainfall to portions of [[South Texas]]. At Montell, {{convert|20.6|in|mm|abbr=on}} of precipitation fell in about 19&nbsp;hours, while [[Uvalde, Texas|Uvalde]] observed {{convert|8.5|in|mm|abbr=on}} of rain in approximately 17&nbsp;hours. The resultant flooding caused considerable damage to lowlands, houses, and stock. Additionally, communication by telegraph and telephone were cut-off for several days and traffic was interrupted due to inundated streets. One person drowned in Montell. Strong winds were also reported, with winds of {{convert|100|mph|km/h|abbr=on}} over Central Padre Island. Along the coast, storm surge peaked at {{convert|12.7|ft|m|abbr=on}} in [[Galveston, Texas|Galveston]].<ref name="documentation"/>
{{Clear}}

===Tropical Storm Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic tropical storm 2 track.png
|Formed=August 14
|Dissipated=August 16
|1-min winds=40
|Pressure=<1008
}}
A [[low pressure area]] detached from a [[stationary front]] and developed into a tropical depression on August&nbsp;14 while located about 185&nbsp;mi (300&nbsp;km) west-southwest of [[Bermuda]].<ref name="documentation"/>{{Atlantic hurricane best track}} The depression moved rapidly northeastward and intensified into a tropical storm on August&nbsp;15. Thereafter, it peaked with maximum sustained winds of 45&nbsp;mph (75&nbsp;km/h).{{Atlantic hurricane best track}} By August&nbsp;16, the storm transitioned into an [[extratropical cyclone]] while situated about {{convert|290|mi|km|abbr=on}} southeast of [[Sable Island]], [[Nova Scotia]].<ref name="documentation"/>{{Atlantic hurricane best track}} The remnants were promptly absorbed by a [[Frontal system|frontal boundary]].<ref name="documentation"/>
{{Clear}}

===Tropical Storm Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic tropical storm 3 track.png
|Formed=August 26
|Dissipated=September 12
|1-min winds=60
|Pressure=<995
}}
Weather maps and ship data indicated a tropical depression formed near the west coast of [[Africa]] on August&nbsp;26.<ref name="documentation"/> Early the next day, the system strengthened into a tropical storm. It then tracked westward for several days, threatening the [[Lesser Antilles]] before turning north-northwestward on September&nbsp;3. Eventually, the storm recurved to the northeast before beginning an eastward direction on September&nbsp;7. The following morning, it peaked with maximum sustained winds of 70&nbsp;mph (110&nbsp;km/h) &ndash; just shy of hurricane status. Thereafter, the cyclone moved northward to northwestward for the next few days. Around 12:00&nbsp;UTC on September&nbsp;12, the storm became extratropical while located about {{convert|290|mi|km|abbr=on}} southeast of [[Cape Race]], [[Newfoundland and Labrador]].{{Atlantic hurricane best track}} 
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic hurricane 4 track.png
|Formed=August 30
|Dissipated=September 4
|1-min winds=75
|Pressure=976
}}
A tropical storm developed about halfway between Bermuda and the [[Bahamas]] at 12:00&nbsp;UTC on August&nbsp;30. The storm moved slowly north-northwestward and approached the [[East Coast of the United States]]. By September&nbsp;1, it intensified into a Category&nbsp;1 hurricane. Eventually, the hurricane curved to the northwest. While located offshore [[North Carolina]] early on September&nbsp;3, the cyclone peaked with maximum sustained winds of 85&nbsp;mph (140&nbsp;km/h) and a minimum barometric pressure of {{convert|976|mbar|inHg|abbr=on}}, making it the strongest tropical cyclone of the season. Hours later, it made landfall near [[Cape Lookout, North Carolina|Cape Lookout]] at the same intensity. Shortly after moving inland, the system weakened to a tropical storm. By September&nbsp;4, it deteriorated to a tropical depression before dissipating over northeastern [[Georgia (U.S. state)|Georgia]].{{Atlantic hurricane best track}}

In North Carolina, winds as high as {{convert|74|mph|km/h|abbr=on}} at [[Hatteras, North Carolina|Hatteras]] caused severe crop losses, especially in areas adjacent to [[Pamlico Sound]]. The worst of the property damage occurred in the vicinity of [[Newbern, North Carolina|New Bern]] and [[Washington, North Carolina|Washington]]. At the latter, northeast to southeast gales caused waterways to rise {{convert|10|ft|m|abbr=on}} above previous high-water marks. Large railroad bridges in both New Bern and Washington were washed away, as were many other small bridges. Many low-lying streets were inundated.<ref name="documentation"/> The storm was considered "the worst in history" at [[Goldsboro, North Carolina|Goldsboro]].<ref name="hudgins">{{cite report|url=http://www.erh.noaa.gov/rnk/Research/NC_Tropical_Cyclone_History.pdf|title=Tropical cyclones affecting North Carolina since 1586: An historical perspective|author=James E. Hudgins|date=April 2000|work=[[National Weather Service]]|publisher=[[National Oceanic and Atmospheric Administration]]|page=25|accessdate=September 3, 2015|location=Blacksburg, Virginia|format=PDF}}</ref> In [[Farmville, North Carolina|Farmville]], a warehouse collapsed, killing two people inside while three deaths occurred elsewhere in the state.<ref name="MWR"/> Throughout North Carolina, numerous telegraph and telephone lines were damaged. Similarly, telegraph and telephone lined were downed in rural areas of [[Virginia]]. Small houses in [[Newport News, Virginia|Newport News]], [[Ocean View, Virginia|Ocean View]], and [[Old Point, Virginia|Old Point]] were unroofed. Overall, this storm caused five fatalities and $4–5&nbsp;million in damage.<ref name="documentation"/>
{{clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic hurricane 5 track.png
|Formed=October 6
|Dissipated=October 10
|1-min winds=65
|Pressure=989
}}
An extratropical cyclone developed offshore [[Rhode Island]] on October&nbsp;2 and moved southeastward. Eventually, the system curved southwestward and transitioned into a tropical storm on October&nbsp;6 while situated about 325&nbsp;mi (525&nbsp;km) northwest of Bermuda.<ref name="documentation"/>{{Atlantic hurricane best track}} After becoming tropical, the storm continued to move southwestward and approached the Southeastern United States. By October&nbsp;7, it curved westward and began to intensify. The storm became a Category&nbsp;1 hurricane around 06:00&nbsp;UTC on October&nbsp;8. Peaking with maximum sustained winds of 75&nbsp;mph (120&nbsp;km/h) and a minimum barometric pressure of {{convert|989|mbar|inHg|abbr=on}}, the hurricane made landfall near [[McClellanville, South Carolina]], about eight hours later. By the evening of October&nbsp;8, the cyclone weakened to a tropical storm and fell to tropical depression intensity by late on October&nbsp;9. Early the following day, it became extratropical and was soon absorbed by a strong [[cold front]] over North Carolina.{{Atlantic hurricane best track}}

Although the storm had been a hurricane at landfall, the highest recorded winds in South Carolina were 37&nbsp;mph (60&nbsp;km/h). The Georgetown Railway and Light Company and the Home Telephone Company suffered the worst damage. Throughout [[Georgetown, South Carolina|Georgetown]], wires and poles were toppled, which briefly cut-off communications. Fences and trees limbs were also blown down.<ref name="documentation"/> Heavy rain, peaking at 4.88&nbsp;in (123.95&nbsp;mm), was recorded along the coast of South Carolina.<ref name="MWR">{{cite report|author=Bernard Bunnemeyer|year=1914|accessdate=September 4, 2015|title=Monthly Weather Review for 1913|publisher=[[National Weather Service|United States Weather Bureau]]|url=http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1913.pdf|format=[[PDF]]}}</ref> Precipitation led to minor crop damage, totaling approximately $75,000.<ref name="documentation"/>
{{Clear}}

===Hurricane Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=1913 Atlantic hurricane 6 track.png
|Formed=October 28
|Dissipated=October 30
|1-min winds=65
|Pressure=<992
}}
The final tropical storm developed in the northwestern Caribbean Sea about {{convert|60|mi|km|abbr=on}} southeast of [[Banco Chinchorro]], Mexico, at 00:00&nbsp;UTC on October&nbsp;28. Moving north-northeast, the storm reached Category&nbsp;1 hurricane intensity about 24&nbsp;hours after its classification. Around that time, it peaked with maximum sustained winds of 75&nbsp;mph (120&nbsp;km/h). Around 06:00&nbsp;UTC on October&nbsp;29, the system made landfall near [[Cape San Antonio, Cuba]], at the same intensity. After about six hours over [[Cuba]], the hurricane weakened to a tropical storm.{{Atlantic hurricane best track}} It transitioned into an extratropical cyclone in present-day [[Mayabeque Province]] before being absorbed by a frontal boundary shortly thereafter.<ref name="documentation"/>{{Atlantic hurricane best track}}
{{Clear}}

===Other systems===
In addition to the six systems that reached tropical storm status, four other [[tropical depression]]s developed. The first formed northeast of Bermuda on May&nbsp;5, marking the third consecutive year in which a [[List of off-season Atlantic hurricanes|tropical cyclone originated before June]]. The depression was absorbed by a frontal boundary on May&nbsp;7. Another tropical depression developed in the [[Bay of Campeche]] on June&nbsp;13. Drifting slowly northward and westward, the system made landfall in southern [[Texas]] on June&nbsp;16 and dissipated the following day. The next tropical depression formed in the vicinity of the [[Azores]] on July&nbsp;20. After four days, it was absorbed into a frontal system. The final cyclone that failed to tropical storm status developed in the northeastern Gulf of Mexico to the south of [[Tallahassee, Florida]], on August&nbsp;4. Drifting southwestward over the next few days, the depression dissipated by August&nbsp;7.<ref name="documentation"/>

==See also==
{{Portal|Tropical cyclones}}
*[[List of tropical cyclones]]
*[[List of Atlantic hurricane seasons]]

==References==
{{Reflist}}

==External links==
{{Commons category|1913 Atlantic hurricane season}}
*[http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1913.pdf Monthly Weather Review]

{{TC Decades|Year=1910|basin=Atlantic|type=hurricane}}

{{DEFAULTSORT:1913 Atlantic Hurricane Season}}
[[Category:1910–1919 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]