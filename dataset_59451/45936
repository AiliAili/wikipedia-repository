{{Infobox cycling race report
| name               = 2009 Giro d'Italia Femminile
| date               = 3–12 July 2009
| stages             = 9
| distance           = 918
| unit               = km
| first              = [[Claudia Häusler]]
| first_nat          = GER
| first_color        = pink
| first_team         = {{ct|CWT|2009}}
| second             = [[Mara Abbott]]
| second_nat         = USA
| second_team        = {{ct|SLU|2009a}}
| third              = [[Nicole Brändli]]
| third_nat          = SWI
| third_team         = [[Bigla Cycling Team]]
| points             = [[Claudia Häusler]]
| points_nat         = GER
| points_color       = violet
| points_team        = {{ct|CWT|2009}}
| mountains          = [[Mara Abbott]]
| mountains_nat      = USA
| mountains_color    = green
| mountains_team     = {{ct|SLU|2009a}}
| youth              = [[Lizzie Armitstead]]
| youth_nat          = GBR
| youth_color        = white
| youth_team         = {{ct|LBL|2009}}
| team               = {{ct|CWT|2009}}
| previous           = [[2008 Giro d'Italia Femminile|2008]]
| next               = [[2010 Giro d'Italia Femminile|2010]]
}}
The '''2009 Giro d'Italia Femminile''', or '''Giro Donne''', was the 20th running of the [[Giro d'Italia Femminile]], one of the premier events of the women's road cycling calendar. It was held over nine stages from 3–12 July 2009, starting in [[Scarperia]] and finishing in [[Grumo Nevano]].<ref name="Route">{{cite web | url=http://www.cyclingnews.com/races/20th-giro-donne-2-1-we | title=Giro Donne 2009 | work=[[Cyclingnews.com]] | publisher=[[Future plc]] | accessdate=21 July 2013}}</ref> It was won by [[Claudia Häusler]] of {{ct|CWT}}.

==Teams==
Eighteen teams were invited to the Giro d'Italia Femminile.<ref>http://www.girodonne.it/portale/index.php?option=com_content&task=view&id=666&Itemid=106</ref>
These teams were:
{{colbegin||20em}}
*Australia National Team
*[[Bigla Cycling Team]]
*[[Bizkaia-Durango-Champion System]]
*{{ct|CWT|2009}}
*[[Noris Cycling|Equipe Nürnberger Versicherung]]
*[[Fenixs-Edilsavino]]
*[[Gauss RDZ Ormu-Colnago]]
*{{ct|LBL|2009}}
*[[S.C. Michela Fanini Record Rox]]
*[[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
*[[Selle Italia Ghezzi]]
*[[Team Cmax Dila']]
*{{ct|SLU|2009a}}
*[[Team Flexpoint]]
*[[Team System Data]]
*[[Top Girls Fassa Bortolo Raxy Line]]
*USA National Team
*[[Forno d'Asolo Colavita|USC Chirio Forno d'Asolo]]
{{colend}}

==Route and stages==
{| class="wikitable"
|+ Stage results<ref name="Route"/><ref>{{cite web | url=http://www.girodonne.it/portale/index.php?option=com_content&task=view&id=161&Itemid=90 | title=Reglement | publisher=Epinike Associazione Sportiva Dilettantistica | accessdate=21 July 2013 | archiveurl=https://web.archive.org/web/20090627024417/http://www.girodonne.it/portale/index.php?option=com_content&task=view&id=161&Itemid=90 | archivedate=27 June 2009}}</ref>
|-
!Stage
!Date
!Course
!Distance
!colspan="2"|Type
!Winner
|-
! style="text-align:center"| P 
| style="text-align:center;"| 3 July
| [[Scarperia]]
| style="text-align:center;"| {{convert|2.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Time Trial.svg|22px|link=|alt=]]
| [[Individual time trial]]
| {{flagathlete|[[Kirsten Wild]]|NED}}
|-
! style="text-align:center"| 1
| style="text-align:center;"| 4 July
| [[San Piero a Sieve]] to [[Vaglia|Pratolino di Vaglia]]
| style="text-align:center;"| {{convert|99.9|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mediummountainstage.svg|22px|alt=|link=]]
| Medium-mountain stage
| {{flagathlete|[[Edita Pučinskaitė]]|LTU}}
|-
! style="text-align:center"| 2
| style="text-align:center;"| 5 July
| [[Pontedera]] to [[Santa Maria a Monte]]
| style="text-align:center;"| {{convert|13.5|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Time Trial.svg|22px|link=|alt=]]
| [[Individual time trial]]
| {{flagathlete|[[Amber Neben]]|USA}}
|-
! style="text-align:center"| 3
| style="text-align:center;"| 6 July
| [[Calcinaia]] to Prato a Calci/Monte Serra
| style="text-align:center;"| {{convert|106.4|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mountainstage.svg|22px|link=|alt=]]
| Stage with mountain(s)
| {{flagathlete|[[Mara Abbott]]|USA}}
|-
! style="text-align:center"| 4
| style="text-align:center;"| 7 July
| [[Porto Sant'Elpidio]] to Porto Sant'Elpidio
| style="text-align:center;"| {{convert|109.2|km|0|abbr=on}}
| style="text-align:center;"| [[Image:Mediummountainstage.svg|22px|alt=|link=]]
| Medium-mountain stage
| {{flagathlete|[[Ina-Yoko Teutenberg]]|GER}}
|-
! style="text-align:center"| 5
| style="text-align:center"| 8 July
| [[Fossacesia]] to [[Cerro al Volturno]]
| style="text-align:center"| {{convert|109.2|km|0|abbr=on}}
| style="text-align:center"| [[Image:Mountainstage.svg|22px|link=|alt=]]
| Stage with mountain(s)
| {{flagathlete|[[Noemi Cantele]]|ITA}}
|-
! style="text-align:center"| 6
| style="text-align:center"| 9 July
| [[Cerro al Volturno]] to [[Sant'Elena Sannita]]
| style="text-align:center"| {{convert|119.3|km|0|abbr=on}}
| style="text-align:center"| [[Image:Mountainstage.svg|22px|link=|alt=]]
| Stage with mountain(s)
| {{flagathlete|[[Judith Arndt]]|GER}}
|-
! style="text-align:center"| 7
| style="text-align:center"| 10 July
| [[Andria]] to [[Castel del Monte, Apulia|Castel del Monte]]
| style="text-align:center"| {{convert|131.2|km|0|abbr=on}}
| style="text-align:center"| [[Image:Mediummountainstage.svg|22px|alt=|link=]]
| Medium-mountain stage
| {{flagathlete|[[Claudia Häusler]]|GER}}
|-
! style="text-align:center"| 8
| style="text-align:center"| 11 July
| [[San Marco dei Cavoti]] to [[Pesco Sannita]]
| style="text-align:center"| {{convert|115.6|km|0|abbr=on}}
| style="text-align:center"| [[Image:Mediummountainstage.svg|22px|alt=|link=]]
| Medium-mountain stage
| {{flagathlete|[[Trixi Worrack]]|GER}}
|-
! style="text-align:center"| 9
| style="text-align:center"| 12 July
| [[Grumo Nevano]]
| style="text-align:center"| {{convert|111.2|km|0|abbr=on}}
| style="text-align:center"| [[Image:Plainstage.svg|22px|link=|alt=]]
| Plain stage
| {{flagathlete|[[Kirsten Wild]]|NED}}
|-
|}

==Classification leadership==
There were four different jerseys awarded in the 2009 Giro Donne. These followed the same format as those in the men's [[Giro d'Italia]]. The leader of the [[General classification]] received a pink jersey. This classification was calculated by adding the combined finishing times of the riders from each stage, and the overall winner of this classification is considered the winner of the Giro.<ref name="demystified">{{cite web
|author=Laura Weislo
|url=http://autobus.cyclingnews.com/road/2008/giro08/?id=/features/2008/giro_classifications08
|title=Giro d'Italia classifications demystified
|date=2008-05-13
|publisher=Cycling News
|accessdate=2009-08-27
}}</ref>

Secondly, the [[points classification]] awarded the maglia ciclamino, or mauve jersey. Points were awarded for placements at stage finishes as well as at selected intermediate sprint points on the route, and the jersey would be received by the rider with the most overall points to their name.<ref name="demystified" />

In addition to this, there was a [[mountains classification]], which awarded a green jersey. Points were allocated for the first few riders over selected mountain passes on the route, with more difficult passes paying more points, and the jersey would be received by the rider with the most overall points to their name.<ref name="demystified" />

Finally, there was the jersey for the Best Young Rider, which was granted to the highest-placed rider on the General classification aged 23 or under. This rider would receive a white jersey.

{| class="wikitable" style="text-align: center; font-size:smaller;"
|+Classification leadership by stage
|- style="background-color: #efefef;"
! width="1%"  | Stage
! width="14%" | Winner
! width="15%"| [[General classification]]<br>[[Image:Jersey pink.svg|25px|link=|alt=]]
! width="15%"| [[Points classification]]<br>[[Image:Jersey violet.svg|25px|link=|alt=]]
! width="15%"| [[Mountains classification]]<br>[[Image:Jersey green.svg|25px|link=|alt=]]
! width="15%"| [[Young rider classification]]<br>[[Image:Jersey white.svg|25px|link=|alt=]]
|-
! P
| [[Kirsten Wild]]
| style="background:pink;"| [[Kirsten Wild]]
| style="background:#EFEFEF;"| ''not awarded''
| style="background:#EFEFEF;"| ''not awarded''
| style="background:white;"| [[Trine Schmidt]]
|-
! 1
| [[Edita Pučinskaitė]]
| style="background:pink;"| [[Edita Pučinskaitė]]
| style="background:violet;"|  [[Edita Pučinskaitė]]
| style="background:lightgreen;"| [[Edita Pučinskaitė]]
| style="background:white;"| [[Alyona Andruk]]
|-
! 2
| [[Amber Neben]]
| style="background:pink;"| [[Amber Neben]]
| style="background:violet;"| [[Amber Neben]]
| style="background:lightgreen;"| [[Amber Neben]]
| style="background:white;" rowspan="8"| [[Lizzie Armitstead]]
|-
! 3
| [[Mara Abbott]]
| style="background:pink;" rowspan="3"| [[Emma Pooley]]
| style="background:violet;" rowspan="2"| [[Emma Pooley]]
| style="background:lightgreen;" rowspan="7"| [[Mara Abbott]]
|-
! 4
| [[Ina-Yoko Teutenberg]]
|-
! 5
| [[Noemi Cantele]]
| style="background:violet;" rowspan="3"| [[Judith Arndt]]
|-
! 6
| [[Judith Arndt]]
| style="background:pink;" rowspan="4"| [[Claudia Häusler]]
|-
! 7
| [[Claudia Häusler]]
|-
! 8
| [[Trixi Worrack]]
| style="background:violet;" rowspan="2"| [[Claudia Häusler]]
|-
! 9
| [[Kirsten Wild]]
|-
! colspan="2"| '''Final'''
! style="background:#F660AB;"| '''[[Claudia Häusler]]'''
! style="background:#B93B8F;"| '''[[Claudia Häusler]]'''
! style="background:#50C878;"| '''[[Mara Abbott]]'''
! style="background:offwhite;"| '''[[Lizzie Armitstead]]'''
|}

==Classification standings==
{| class="wikitable"
|-
!colspan=4| Legend
|-
| &nbsp;&nbsp;[[File:Jersey pink.svg|20px|link=|alt=Pink jersey]]&nbsp;&nbsp;
| Denotes the leader of the General classification
| &nbsp;&nbsp;[[File:Jersey green.svg|20px|link=|alt=Green jersey]]&nbsp;&nbsp;
| Denotes the leader of the Mountains classification
|-
| &nbsp;&nbsp;[[File:Jersey violet.svg|20px|link=|alt=Mauve jersey]]&nbsp;&nbsp;
| Denotes the leader of the Points classification
| &nbsp;&nbsp;[[File:Jersey white.svg|20px|link=|alt=White jersey]]&nbsp;&nbsp;
| Denotes the leader of the Young rider classification
|}
{{columns-start}}

===General classification===
{| class="wikitable"
|-
!Rank
!Rider
!Team
!Time
|-
| 1
| {{flagathlete|[[Claudia Häusler]]|GER}} [[Image:Jersey pink.svg|20px|Pink jersey]][[Image:Jersey violet.svg|20px|Mauve jersey]]
| {{ct|CWT|2009}}
| style="text-align:right;"| {{nowrap|25h 21' 32"}}
|-
| 2
| {{flagathlete|[[Mara Abbott]]|USA}} [[Image:Jersey green.svg|20px|Green jersey]]
| {{ct|SLU|2009a}}
| style="text-align:right;"| + 30"
|-
| 3
| {{flagathlete|[[Nicole Brändli]]|SUI}}
| [[Bigla Cycling Team]]
| style="text-align:right;"| + 2' 33"
|-
| 4
| {{flagathlete|[[Emma Pooley]]|GBR}}
| {{ct|CWT|2009}}
| style="text-align:right;"| + 6' 35"
|-
| 5
| {{flagathlete|[[Svetlana Bubnenkova]]|RUS}}
| [[Fenixs-Edilsavino]]
| style="text-align:right;"| + 6' 51"
|-
| 6
| {{flagathlete|[[Fabiana Luperini]]|ITA}}
| [[Selle Italia Ghezzi]]
| style="text-align:right;"| + 8' 53"
|-
| 7
| {{flagathlete|[[Tatiana Guderzo]]|ITA}}
| [[S.C. Michela Fanini Record Rox]]
| style="text-align:right;"| + 10' 54"
|-
| 8
| {{flagathlete|[[Carla Ryan]]|AUS}}
| {{ct|CWT|2009}}
| style="text-align:right;"| + 12' 29"
|-
| 9
| {{flagathlete|[[Susanne Ljungskog]]|SWE}}
| [[Team Flexpoint]]
| style="text-align:right;"| + 14' 13"
|-
| 10
| {{flagathlete|[[Edita Pučinskaitė]]|LTU}}
| [[Gauss RDZ Ormu-Colnago]]
| style="text-align:right;"| + 14' 23"
|}
{{column}}

===Points classification===
{| class="wikitable"
|-
!Rank
!Rider
!Team
!Points
|-
| 1
| {{flagathlete|[[Claudia Häusler]]|GER}} [[Image:Jersey pink.svg|20px|Pink jersey]][[Image:Jersey violet.svg|20px|Mauve jersey]]
| {{ct|CWT|2009}}
| style="text-align:right;"| 49
|-
| 2
| {{flagathlete|[[Mara Abbott]]|USA}} [[Image:Jersey green.svg|20px|Green jersey]]
| {{ct|SLU|2009a}}
| style="text-align:right;"| 41
|-
| 3
| {{flagathlete|[[Svetlana Bubnenkova]]|RUS}}
| [[Fenixs-Edilsavino]]
| style="text-align:right;"| 39
|-
| 4
| {{flagathlete|[[Giorgia Bronzini]]|ITA}}
| [[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
| style="text-align:right;"| 32
|-
| 5
| {{flagathlete|[[Nicole Brändli]]|SUI}}
| [[Bigla Cycling Team]]
| style="text-align:right;"| 29
|-
| 6
| {{flagathlete|[[Emma Pooley]]|GBR}}
| {{ct|CWT|2009}}
| style="text-align:right;"| 29
|-
| 7
| {{flagathlete|[[Kirsten Wild]]|NED}}
| {{ct|CWT|2009}}
| style="text-align:right;"| 27
|-
| 8
| {{flagathlete|[[Ina-Yoko Teutenberg]]|GER}}
| {{ct|SLU|2009a}}
| style="text-align:right;"| 25
|-
| 9
| {{flagathlete|[[Edita Pučinskaitė]]|LTU}}
| [[Gauss RDZ Ormu-Colnago]]
| style="text-align:right;"| 22
|-
| 10
| {{flagathlete|[[Trixi Worrack]]|GER}}
| [[Noris Cycling|Equipe Nürnberger Versicherung]]
| style="text-align:right;"| 21
|}
{{columns-end}}
{{columns-start}}

===Mountains classification===
{| class="wikitable"
|-
!Rank
!Rider
!Team
!Points
|-
| 1
| {{flagathlete|[[Mara Abbott]]|USA}} [[Image:Jersey green.svg|20px|Green jersey]]
| {{ct|SLU|2009a}}
| style="text-align:right;"| 38
|-
| 2
| {{flagathlete|[[Nicole Brändli]]|SUI}}
| [[Bigla Cycling Team]]
| style="text-align:right;"| 36
|-
| 3
| {{flagathlete|[[Claudia Häusler]]|GER}} [[Image:Jersey pink.svg|20px|Pink jersey]][[Image:Jersey violet.svg|20px|Mauve jersey]]
| {{ct|CWT|2009}}
| style="text-align:right;"| 27
|-
| 4
| {{flagathlete|[[Trixi Worrack]]|GER}}
| [[Noris Cycling|Equipe Nürnberger Versicherung]]
| style="text-align:right;"| 25
|-
| 5
| {{flagathlete|[[Emma Pooley]]|GBR}}
| {{ct|CWT|2009}}
| style="text-align:right;"| 19
|-
| 6
| {{flagathlete|[[Tatiana Antoshina]]|RUS}}
| [[Gauss RDZ Ormu-Colnago]]
| style="text-align:right;"| 13
|-
| 7
| {{flagathlete|[[Giorgia Bronzini]]|ITA}}
| [[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
| style="text-align:right;"| 10
|-
| 8
| {{flagathlete|[[Susanne Ljungskog]]|SWE}}
| [[Team Flexpoint]]
| style="text-align:right;"| 10
|-
| 9
| {{flagathlete|[[Fabiana Luperini]]|ITA}}
| [[Selle Italia Ghezzi]]
| style="text-align:right;"| 10
|-
| 10
| {{flagathlete|[[Kristin Armstrong]]|USA}}
| {{ct|CWT|2009}}
| style="text-align:right;"| 9
|}
{{column}}

===Young rider classification===
{| class="wikitable"
|-
!Rank
!Rider
!Team
!Time
|-
| 1
| {{flagathlete|[[Lizzie Armitstead]]|GBR}} [[Image:Jersey white.svg|20px|White jersey]]
| {{ct|LBL|2009}}
| style="text-align:right;"| {{nowrap|25h 48' 32"}}
|-
| 2
| {{flagathlete|[[Elena Berlato]]|ITA}}
| [[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
| style="text-align:right;"| + 30"
|-
| 3
| {{flagathlete|[[Tiffany Cromwell]]|AUS}}
| Australia National Team
| style="text-align:right;"| + 5' 18"
|-
| 4
| {{flagathlete|[[Valentina Carretta]]|ITA}}
| [[Top Girls Fassa Bortolo Raxy Line]]
| style="text-align:right;"| + 7' 06"
|-
| 5
| {{flagathlete|[[Olena Oliynyk]]|UKR}}
| [[Forno d'Asolo Colavita|USC Chirio Forno d'Asolo]]
| style="text-align:right;"| + 8' 53"
|-
| 6
| {{flagathlete|[[Shara Gillow]]|AUS}}
| Australia National Team
| style="text-align:right;"| + 14' 25"
|-
| 7
| {{flagathlete|[[Lieselot Decroix]]|BEL}}
| {{ct|CWT|2009}}
| style="text-align:right;"| + 29' 47"
|-
| 8
| {{flagathlete|[[Alice Donadoni]]|ITA}}
| [[Gauss RDZ Ormu-Colnago]]
| style="text-align:right;"| + 32' 19"
|-
| 9
| {{flagathlete|[[Carlee Taylor]]|AUS}}
| Australia National Team
| style="text-align:right;"| + 34' 55"
|-
| 10
| {{flagathlete|[[Alyona Andruk]]|UKR}}
| [[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
| style="text-align:right;"| + 44' 28"
|}
{{columns-end}}
{{columns-start}}

===Team classification===
{| class="wikitable"
|-
!Rank
!Team
!Points
|-
| 1
| {{ct|CWT|2009}}
| style="text-align:right;"| {{nowrap|76h 16' 07"}}
|-
| 2
| {{ct|SLU|2009a}}
| style="text-align:right;"| + 9' 12"
|-
| 3
| [[S.C. Michela Fanini Record Rox]]
| style="text-align:right;"| + 50' 55"
|-
| 4
| [[Selle Italia Ghezzi]]
| style="text-align:right;"| + 1h 05' 47"
|-
| 5
| [[Team Flexpoint]]
| style="text-align:right;"| + 1h 07' 25"
|-
| 6
| [[Gauss RDZ Ormu-Colnago]]
| style="text-align:right;"| + 1h 07' 31"
|-
| 7
| [[Bigla Cycling Team]]
| style="text-align:right;"| + 1h 08' 22"
|-
| 8
| [[Pasta Zara-Cogeas|Safi-Pasta Zara Titanedi]]
| style="text-align:right;"| + 1h 30' 57"
|-
| 9
| Australia National Team
| style="text-align:right;"| + 1h 33' 01"
|-
| 10
| [[Fenixs-Edilsavino]]
| style="text-align:right;"| + 1h 59' 54"
|}
{{columns-end}}

==Sources==
{{reflist}}

{{Giro d'Italia Femminile}}

{{DEFAULTSORT:2009 Giro D'italia Femminile}}
[[Category:Giro d'Italia Femminile]]
[[Category:2009 in women's road cycling|Giro d'Italia Femminile]]
[[Category:2009 in Italian sport|Giro d'talia Fem]]