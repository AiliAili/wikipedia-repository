{{EngvarB|date=January 2017}}
{{Use dmy dates|date=January 2017}}
{{Infobox government agency
 | seal = 
|logo = File:NationalAuditOffice.svg
 |formed = 1983
 |preceding1 = Exchequer and Audit Department<ref name=EADgovUK/>
 |jurisdiction = [[Government of the United Kingdom]]
 |motto = Helping the nation spend wisely
|employees = 796
 |budget = £64.5m net budget 16–17
 |chief1_name = [[Amyas Morse|Sir Amyas Morse]]
 | chief1_position = 
| chief2_name = 
| chief2_position = 
| chief3_name = 
| chief3_position = 
| chief4_name = 
| chief4_position = 
| chief5_name = 
| chief5_position = 
| chief6_name = 
| chief6_position = 
| chief7_name = 
| chief7_position = 
| chief8_name = 
| chief8_position = 
| chief9_name = 
| chief9_position = 
|parent_department = [[Public Accounts Committee (United Kingdom)|Committee of Public Accounts]]
 |website = {{URL|nao.org.uk/}}
 |agency_name = National Audit Office
 |agency type = independent Parliamentary body
 }}
The '''National Audit Office''' ('''NAO''') is an independent Parliamentary body in the United Kingdom which is responsible for [[Financial audit|auditing]] [[Departments of the United Kingdom Government|central government departments]], [[Executive Agency|government agencies]] and [[non-departmental public body|non-departmental public bodies]]. The NAO also carries out Value for Money (VFM) audit into the administration of public policy.

==Function==
[[File:157-167 Buckingham Palace Road (geograph 4753186).jpg|thumb|The National Audit Office's Head Office in [[Buckingham Palace Road]], London, built originally as the Imperial Airways Empire Terminal]]
The NAO is the auditor of bodies funded directly by the Parliament of the United Kingdom.

The NAO reports to the [[Comptroller and Auditor General]] who is an officer of the [[British House of Commons|House of Commons]] of the [[Parliament of the United Kingdom]] and in turn reports to the [[Public Accounts Select Committee|Public Accounts Committee]], a [[Select Committee (Westminster System)|select committee]] of the House of Commons. The reports produced by the NAO are reviewed by PAC and in some cases investigated further.

The NAO has two main streams of work:
*Financial Audits
*Value For Money (VFM) audits

''Financial audit'' The NAO’s financial audits give assurance over three aspects of government expenditure: the truth and fairness of financial statements; the regularity (or statutory validity) of the expenditure, and; the propriety of the audited body’s conduct in accordance with parliamentary, statutory and public expectations. Financial audits are carried out in much the same way as private auditing bodies and the NAO voluntarily applies the International Standards of Auditing (ISAs). The NAO is subject to inspection by the Audit Quality Review team of the Financial Reporting Council.

''Value for Money'' (VFM) audits are [[Performance audit|non-financial audits]] to measure the effectiveness, economy and efficiency of government spending. Roughly sixty of these reports are produced each year, the most notable from recent years being the reports on [[Methicillin-resistant Staphylococcus aureus|MRSA]], which led to an increase in public interest in the topic, the report on the rescue of [[British Energy]] and the report in the [[Public-private partnership|Public Private Partnership]] to maintain the [[London Underground]]. The remits of the NAO and the Public Accounts Committee do not allow them to question the policy itself and so VFM reports only examine the implementation of policy. The responsibility for questioning policy is left for other select committees and debating chambers of Parliament, but this has not prevented the PAC being named committee of the year in 2006.

'Good Governance', an output somewhere between financial and VFM audits, was previously a strand of NAO work, but is no longer a focus of activity. The NAO does, however, publish best practice guidance for public sector organisations. An example includes the fact sheet on governance statements. In addition, the NAO undertakes fast-paced and more narrowly focused work called investigations.

The NAO received new powers under the [[Local Audit and Accountability Act 2014]] to provide an end-to-end view of policy implementation, and produce reports aimed at the local government sector.

===The Comptroller Function===
The Comptroller Function is administered by the small, but important, Exchequer Section within the NAO.

Its work centres on recording all transactions to and from the Consolidated and National Loans funds. Money cannot be paid from either of these without the C&AG's prior approval. This approval is granted every banking day through a mechanism known as 'the credit'. The Exchequer Section is also responsible for agreeing payments from the Consolidated Fund directly to certain bodies, including the Queen (through the civil list), judicial salaries, MEPs salaries and the European Commission.

===Parliamentary links===

The NAO produces a number of briefings for select committees, but its key audience is the Public Accounts Committee. It also has a strong relationship with the Public Accounts Commission that oversees the work of the NAO and approves its budgets.

====Public Accounts Committee====
The NAO and Public Accounts Committee (PAC) form the key links of the Public Audit Circle which has the following sequence:
*The NAO performs financial and VFM audits and makes its reports public
*The PAC has hearings based on NAO reports wherein failures in meeting regularity or propriety requirements are apparent.
*The PAC provides a report with recommendations based on PAC hearings.
*The Government responds to the PAC report in a Treasury Minute.
*The NAO publishes a reply to the minute and there may be an NAO/PAC follow-up study.

====Public Accounts Commission====
The [[Public Accounts Commission]] (TPAC) annually approves the NAO's corporate plans and budgets. It also receives value for money reports on the operation of the NAO. These are written by private sector audit firms in much the same manner as the NAO reports on Central Government.

====International Work====
The National Audit Office is a member of the International Organisation of Supreme Audit Institutions ([[International Organization of Supreme Audit Institutions|INTOSAI]]). The NAO shares knowledge and experience with other Supreme Audit Institutions (SAIs) around the world and undertakes the audit of some international bodies. For example, between 2010–2016 the C&AG was one of three members of the United Nations Board of Auditors, responsible for auditing the United Nations itself, including peacekeeping operations and related organisations such as UNICEF and the UN High Commissioner for Refugees (UNHCR).

==History and establishment==
The NAO developed from the former '''Exchequer and Audit Department'''<ref name=EADgovUK>{{cite web |url=https://www.gov.uk/government/organisations/exchequer-and-audit-department |title=Exchequer and Audit Department |website=gov.uk|publisher=UK Government |accessdate=14 November 2016}}</ref> (founded in 1866) in 1983 as the [[auditor]] for central government (including most of the externalised agencies and public bodies) as part of an "appropriate mechanism" to check and reinforce departmental balance and matching of quantitative allocation with qualitative purpose (as set out by public policy). The existence and work of the NAO are underpinned by three fundamental principles of public audit:<ref>“The Audit Commission” by Couchman V. in Sherer & Turley: ''Current Issues in Auditing'', Paul Chapman Publishing (1997)</ref>

*Independence of auditors from the audited (executives and Parliament)
*Auditing for: regularity, propriety and VFM (value for money)
*Public reporting that enables democratic and managerial [[accountability]]

The basic need for the NAO arises from these three fundamental principles, in that, as Parliament votes on public expenditure of various activities by public bodies, they need auditors that are independent of the body in question, the government and/or opposing political parties; while auditing for compliance and legal spending by departments on the activities voted for by Parliament, in a transparent and public forum.

==Structure==
The NAO is based in London and Newcastle and has a staff of 800.

The NAO is structured into Directorates, each with a responsibility for a government department (for instance, the [[Home Office]] or [[Department for Culture, Media and Sport]]). Each Directorate contains 20–30 staff, many of whom are qualified accountants or in training for qualification with the [[Institute of Chartered Accountants in England and Wales]] (ICAEW). Within Directorates, staff will be split between Financial Audit and Value for Money work and include staff of the following grades:

*Analyst: Junior researcher on a value for money engagement;
*Assistant Auditors: auditors employed on a three-year training contract, leading to a qualification with the ICAEW;
*Auditors: auditors that have completed their ICAEW exams but have not yet obtained the full qualification;
*Senior analyst: Oversees value for money studies;
*Audit Principals: senior auditors, with responsibility for leading a financial audit engagement;
*Audit Managers (Civil Service equivalent [[Her Majesty's Civil Service#Grading schemes|Grade 7]]): oversee a portfolio of audit engagements or a value for money studies;
*Director: (Civil Service equivalent Grade 5/[[Senior Civil Service|SCS 1]]): responsibility for a directorate.

Directorates are arranged into clusters of government departments of similar characteristics. This enables the NAO to undertake comparative work between departments.

Above director grade, Directors General have responsibility for specific cross NAO functions (such as Audit Practice and Quality, and Finance and Commerce) and Executive Leaders (previously ''Assistant Auditors General'') support the Board.

The NAO has finance, human resource and ICT functions to support its operations.

===The NAO Offices===

Part of the NAO's London Office is a listed building, originally built for [[Imperial Airways]] as their "''Empire Terminal''". The building underwent a £60m restoration and refurbishment, completed in 2009. The NAO rents part of its offices to tenants, generating income of c£1.3mn in 2012–13<ref>http://www.nao.org.uk/wp-content/uploads/2013/05/ANNUAL-REPORT-2013_WEB-1.pdf NAO annual report</ref>

The building is a modern, open plan office and the refurbishment enabled the NAO to introduce many environmentally friendly features, such as rain-water harvesting.

The NAO has a separate office in Newcastle employing c90 staff.

===Governance===
Following the controversy over a previous C&AG’s expenses —see the criticisms— the governance arrangements of the NAO were overhauled and a Board was put in place to oversee the running of the organisation. The Board is made up of

*Non-Executive chairman – [[Michael Bichard, Baron Bichard]]
*[[Comptroller and Auditor General]] – [[Amyas Morse|Sir Amyas Morse]] 
*chief operating officer responsible for the day-to-day running of the NAO – [[Michael Whitehouse]]
*Three other Executive Directors
*Four non-Executive Directors

==Criticisms==
Some of the criticisms that have been levelled at the NAO include the following:
*'''It is not sufficiently accountable.''' Although the NAO publicly scrutinises other public bodies, the scrutiny that it is subjected to is not fully transparent. Its reports are subject to external review both before and after publication by teams of academics from [[Oxford University]] and the [[London School of Economics]]. These reviews consider whether the methods, findings and conclusions of the reports are sound, and have on occasion found the intellectual basis of the reports to be thin. The results of the reviews are not, however, made public.
*'''Its reports are neutral and cautious.''' This criticism stems from the normal way in which the reports are written. Initial drafts of reports are shared with the department(s) about which they are written. This begins a process of 'clearance', during which all facts are agreed between the NAO and departments. The reason for this is to give the PAC a mutually agreed report on which to base its later hearing; the hearing would be pointless if the departmental witnesses were able to disagree with the findings of the report. In practice, the clearance process is said to lead to a watering down of the initial draft, with the most contentious early findings removed at the behest of the department (and never, therefore, made public).
*'''Failure to publish.''' In extreme cases where information is too politically sensitive, a report is not published. The often quoted case is their 1992 investigation into the [[Al Yamamah]] arms deal where due to ongoing legal investigations the report has not been released. It refused to release a copy to investigators during the [[Al-Yamamah arms deal|Serious Fraud Office investigation into the Al Yamamah corruption allegations]] in 2006 as to do so would have required a special vote by the House of Commons.
*'''Its savings are not robustly calculated.''' The NAO claims to save the taxpayer £9 for every £1 it costs to run. These savings include reductions in public expenditure and quantifications of non-financial impacts of the NAO's work. The latter includes expenditure being better targeted and, in some cases, ''increased'' expenditure. (For example, the NAO published a report on how the [[Department for Work and Pensions]] was making the general public aware of state benefits to which they might be entitled. Any increase in the take-up of benefits that could be shown to be directly attributable to the report would be counted as a 'saving' by the NAO.) If the definition of 'savings' were restricted to reductions in public expenditure, the amount of savings that the NAO could claim to have made on behalf of the taxpayer would be significantly reduced. The argument used to support this practice is that the NAO's remit is to examine the effectiveness and efficiency of public spending as well as the economy.
*'''Some of its reports are insufficiently strategic.''' The NAO produces a wide range of reports on all aspects of central government expenditure, but many of these deal with marginal topics like government leaflets, countryside rights of way and railway stations. As David Walker notes,<ref>[http://politics.guardian.co.uk/tax/comment/0,,524584,00.html Summed up] – David Walker – The Guardian, Friday 20 July 2001</ref> the NAO does not and cannot examine major strategic issues such as the underlying principles of the [[Private Finance Initiative]] and the effect of class sizes on educational attainment.
*'''Its reports do not deal adequately with the issue of value for money.''' The NAO uses a broad brush definition of 'value for money' to plan and carry out its reports. The reports do not, as might be expected, focus purely on detailed financial analysis of whether or not a particular scheme or initiative is value for money. Instead, they include qualitative analysis of costs and benefits to give a more comprehensive assessment. In 2005, an NAO report on NHS Local Investment Finance Trust (LIFT) was criticised by one of the PAC members at the time, [[Jon Trickett]], for its focus on qualitative analysis of the benefits of LIFT schemes and the paucity of its financial analysis.<ref>[http://www.publications.parliament.uk/pa/cm200506/cmselect/cmpubacc/562/56202.htm House of Commons Public Accounts Committee – Forty-Seventh Report]</ref> The NAO has recently published a report about the use of consultants in the public sector.<ref>[http://www.nao.org.uk/publications/nao_reports/06-07/0607128.pdf Central Government's use of consultants] – NAO</ref> Critics identify that this report did not directly answer the question of whether consultants employed by the public sector give good value for money. However, as the report highlights as one of its findings, this was not possible because insufficient information is gathered by departments. Furthermore, the report did not consider the quality of the advice given to departments by consultants.
*'''Sir John Bourn's Expenses''' In May 2007, ''[[Private Eye]]'' released information obtained under the [[Freedom of information in the United Kingdom|Freedom of Information Act]] detailing the travel expenses of the then head of the NAO, Sir [[John Bourn]]. These included tickets on [[Concorde]] and stops at luxury hotels.<ref>[http://www.telegraph.co.uk/news/main.jhtml?xml=/news/2007/05/24/nbourn24.xml Auditor General's luxury hotel bills under fire] – The Telegraph.</ref> In one instance, Sir John and his wife attended a three-day audit conference in [[The Bahamas]]. The conference was Wednesday to Friday. Sir John arrived on the Friday and he and his wife stayed on the island Saturday and Sunday.
*'''Al Yamamah''' The NAO was also accused of hampering a police investigation into the [[Al Yamamah]] deal by ''[[The Guardian]]'' newspaper in July 2006. It would, however, have been a breach of parliamentary privilege and hence illegal for the NAO to have handed over the requested information.<ref>[https://www.theguardian.com/armstrade/story/0,,1828253,00.html Parliamentary auditor hampers police inquiry into arms deal] – The Guardian.</ref>

==Other public sector auditing bodies==
There are several other public sector auditing bodies in the United Kingdom:
* The [[Wales Audit Office]], [[Audit Scotland]] and [[Northern Ireland Audit Office]] are responsible for auditing their devolved assemblies and associated public bodies.

==See also==
*[[Whole of Government Accounts]]
* A few other "[[Supreme audit institution]]s" (for a full list, see [[International Organization of Supreme Audit Institutions]]):
**European [[Court of Auditors]] (EU)
**[[Government Accountability Office]] (US)
**[[Cour des Comptes]] (France)
**[[Chamber of Accounts (Greece)]]
**[[Australian National Audit Office]]
**[[National Audit Office of the People's Republic of China]]

==Notes==
{{Reflist|30em}}

==References==
* ''Audit, Accountability and Government'' White & Hollingsworth, Oxford University Press (1999)

== External links ==
* {{Commons category inline|National Audit Office, London}}
* {{Official website}}

[[Category:Public bodies and task forces of the United Kingdom government]]
[[Category:Government agencies established in 1983]]
[[Category:Government audit|United Kingdom]]
[[Category:Parliament of the United Kingdom]]
[[Category:1983 establishments in the United Kingdom]]
[[Category:Grade II listed buildings in the City of Westminster]]