{{Infobox journal
| title = Journal of Church and State
| cover = [[File:Journal of Church & State.gif]]
| editor = Jerold Waltman
| discipline = 
| former_names = 
| abbreviation = J. Church State
| publisher = [[Oxford University Press]] on behalf of the J. M. Dawson Institute of Church-State Studies ([[Baylor University]])
| country = 
| frequency = Quarterly
| history = 1959-present
| openaccess = 
| license = 
| impact = 
| impact-year = 
| website = http://jcs.oxfordjournals.org/
| link1 = http://jcs.oxfordjournals.org/content/current
| link1-name = Online access
| link2 = http://jcs.oxfordjournals.org/archive/
| link2-name = Online archive
| JSTOR = 
| OCLC = 639087108
| LCCN = 
| CODEN = 
| eISSN = 2040-4867
| ISSN = 0021-969X
}}
The '''''Journal of Church and State''''' is a quarterly [[Peer review|peer-reviewed]] [[academic journal]] of [[religious studies]] and [[political science]],<ref name="oxford">{{cite web |url=http://www.oxfordjournals.org/our_journals/jcs/about.html |title=Journal of Church and State: About the Journal |publisher=Oxford University Press |work=Oxford Journals: Humanities & Law & Social Sciences |accessdate=2012-02-15}}</ref><ref name="baylor">{{cite web |url=http://www.baylor.edu/church_state/index.php?id=35398 |title=Church-State Studies: Journal of Church & State |publisher=Baylor University |work= |accessdate=2012-02-15}}</ref><ref name="dictionary">Walter A. Elwell, [https://books.google.com/books?id=yu846j61u0wC&pg=PA254&dq=%22journal+of+church+and+state%22&hl=en&sa=X&ei=ymk2T9G5JKWm0QWKgsHFAg&redir_esc=y#v=onepage&q=%22journal%20of%20church%20and%20state%22&f=false ''Evangelical Dictionary of Theology''], Baker Academic, 2001, p. 254</ref> covering [[First Amendment to the United States Constitution|First Amendment]] issues.<ref>Ted G. Jelen, Clyde Wilcox, [https://books.google.com/books?id=1bp1YMFG4VwC&pg=PA12&dq=%22journal+of+church+and+state%22&hl=en&sa=X&ei=ymk2T9G5JKWm0QWKgsHFAg&redir_esc=y#v=onepage&q=%22journal%20of%20church%20and%20state%22&f=false ''Public Attitudes Toward Church and State''], M.E. Sharpe, 1995, p. 12</ref> It is published by [[Oxford University Press]] on behalf of the J. M. Dawson Institute of Church-State Studies ([[Baylor University]]).<ref name="baylor"/><ref name="carper">James C. Carper, Thomas C. Hunt, [https://books.google.com/books?id=2JIaVFph_AIC&pg=PA268&dq=%22journal+of+church+and+state%22&hl=en&sa=X&ei=Tmc2T_ymAqmj0QWfw9SaAg&redir_esc=y#v=onepage&q=%22journal%20of%20church%20and%20state%22&f=false ''The Praeger Handbook of Religion and Education in the United States: A-L''], ABC-CLIO, 2009, p. 268</ref> It was established in 1959.<ref name="baylor"/><ref name="carper"/><ref name="civil">[https://books.google.com/books?id=DRzA4j9SvMEC&pg=PA237&dq=%22journal+of+church+and+state%22&hl=en&sa=X&ei=Tmc2T_ymAqmj0QWfw9SaAg&redir_esc=y#v=onepage&q=%22journal%20of%20church%20and%20state%22&f=false ''Church and State in America: A Bibliographical Guide: The Civil War to the Present Day''], Volume 2, ABC-CLIO, 1987, p. 237</ref> The [[editor-in-chief]] is Jerold Waltman (Baylor University).

== References ==
{{Reflist|colwidth=30em}}

== External links ==
* {{Official website|http://jcs.oxfordjournals.org/}}

[[Category:Religious studies journals]]
[[Category:Political science journals]]
[[Category:Oxford University Press academic journals]]
[[Category:Publications established in 1959]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Church and state law in the United States]]


{{poli-journal-stub}}
{{reli-journal-stub}}