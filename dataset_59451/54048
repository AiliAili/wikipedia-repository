'''Michael Plant Atchison O.A.M.''' (4 August 1933 &ndash; 16 February 2009) was an Australian cartoonist who worked for the [[South Australia]]n ''[[The Advertiser (Adelaide)|Advertiser]]'' for over 40 years.

He was born in [[Sandringham, Victoria]] and moved to South Australia with his family in 1939. He was educated at Glenelg Primary, then King's College, a boys' school which later became the co-ed [[Pembroke School, Adelaide|Pembroke]]. He attended [[Adelaide Teachers' College]] and embarked on a teaching career. He married fellow Teachers' College student Olga and together left for England in 1960, where he worked as a freelance artist, contributing to magazines including [[Punch (magazine)|Punch]] as well as working as art director for a London advertising agency. They moved to [[Sydney]] in 1967.

He began work at ''The Advertiser'' in 1967 as the eventual replacement ([[Robert Hannaford]] intervened) for [[Pat Oliphant]] who had left for a career with the ''[[The Denver Post|Denver Post]]'' in 1964. And like Oliphant, he was to illustrate the frequent prose and poetic contributions of his great friend [[Max Fatchen]].

==Style and influences==
Fellow cartoonist [[John Stoneham (cartoonist)|John Stoneham]] observed that Atchison "always worked completely freehand, never following pencil lines, but drawing ink straight on to paper, an art form which is long forgotten". He acknowledged the influence of cartoonists [[Ronald Searle]], [[Bruce Petty]] and [[André François]].<ref name=tribute>[http://www.adelaidenow.com.au/tributes-pour-in-for-loved-cartoonist/story-e6freol3-1111118878831 Tributes pour in for loved cartoonist] Harris, Samela ''The Advertiser'' 17 February 2009 accessed 16 September 2011</ref>

==Word for Word==
From 1989 ''The Advertiser'' carried on its daily comics page Atchison's "Word for Word" panel which explored origins and meanings of English words and phrases. Its last appearance was on 7 May 2009.<ref name=tribute/> A series of "Word for Word" collections has been published.

==Signature==
[[Image:Atchison mural, Aldgate SA.jpg|thumb|300px|right|Atchison's SALA mural, [[Aldgate, South Australia]]]]
Many cartoonists have a trademark which may be found in their works; Atchison's, born in 1974, was a scruffy little dog which occasionally behaved disgracefully. His personal trademark was a pair of red braces.<ref name=tribute/>

==Last years==
Atchison lived with cancer from 1994, but despite pain and irksome operations never lost his affable good humour. He was forced by the pain to retire in June 2008, replaced by Jos Valdman. Michael died the following year, survived by his wife of over 50 years, Olga Atchison, two daughters Michelle and Nicola and three grandchildren Anthony, Matthew and Caillin.<ref name=tribute/>

==Recognition==
He was awarded the traditional artist's smock by his colleagues in 1998.<!-- who decorated it? -->

He was inducted into the [[South Australian Media Hall of Fame]] in 2004.

Michael was awarded the [[Medal of the Order of Australia]] (OAM) in 2007.

He received the Jim Russell Award from the [[Australian Cartoonists' Association]] in 2007.

An original cartoon (featuring the "dog of no name") may be seen in an alleyway off the main street in the [[Adelaide Hills]] town of [[Aldgate, South Australia|Aldgate]]. It forms part of a mural contributed by half-a-dozen cartoonists during the 2007 S.A.L.A. ([[South Australian Living Artists Festival|South Australian Living Artists]]) festival.<!-- will it be permissible to upload photo after it's been completely defaced by taggers or the wall demolished? -->

==Artwork links==

A cartoon of Prime Minister [[Gough Whitlam]] may be seen at [http://primeministers.naa.gov.au/image.aspx?id=tcm:13-21530 National Archives of Australia]

A comment on 21st Century aboriginal life may be seen here [http://www.nma.gov.au/collections-search/display?app=tlf&irn=136511 National Museum of Australia]

==Publications==
*''Songs for My Dog and Other Wry Rhymes'' Max Fatchen and Michael Atchison 2004 ISBN 978-1-86254-478-9
*''Wry Rhymes for Troublesome Times'' Max Fatchen and Michael Atchison Kestrel 1985 ISBN 978-0-7226-5807-9
*''Songs for My Dog and Other People'' Max Fatchen and Michael Atchison 1980 ISBN 978-0-7226-5645-7
*''Word Perfect'' Dudley Burton and Michael Atchison 1987 ISBN 978-0-86778-034-5
*''Words, Words, Words'' Tom Burton, Michael Atchison Radio Adelaide 5UV ISBN 0-86396-263-7
*''Words in Your Ear'' Tom Burton and Michael Atchison Wakefield Press 1999 ISBN 1-86254-475-1
*''Wallaby; An Aussie, a Broad'' Tom Burton, Michael Atchison, Rod MacLean Wakefield Press Pty, Ltd. ISBN 1-86254-653-3
*''Mr. Dohnt's Notice Garden'' Michael Page and Michael Atchison HarperCollins Australia, 1988 ISBN 978-0-7322-4804-8
*''Lakeliners 2007: Short Stories and Poems from Milang'' Stuart Jones, Christine Stratton, Doreen Simpson, Greta Mansveld, Michael Atchison Milang Progress Association, Inc. ISBN 0-646-47882-6
*''Long Words Bother Me'' Stuart Jones, Christine Stratton, Doreen Simpson, Greta Mansveld, Michael Atchison Sutton Pub. Ltd. ISBN 0-7509-3973-7
*''What's Your Problem, Vol II'' Barbara Ross, Michael Atchison Advertiser Newspapers, Ltd. ISBN 0-9592572-7-6
*''Word for Word: A Cartoon History of Word Origins Book 1'' Dog-eared Pub. ISBN 0-9585579-0-X
*''Word for Word: A Cartoon History of Word Origins Book 2'' Dog-eared Pub. ISBN 0-9585579-1-8

== References ==
{{Reflist}}

== External links ==
*[http://www.openaustralia.org/senate/?id=2009-03-18.128.1 Tribute to Michael Atchison by Senator Dana Wortley]
*[http://www.daao.org.au/bio/michael-atchison/ Atchison, Michael in Design & Art Australia Online]

{{DEFAULTSORT:Atchison, Michael}}
[[Category:Australian editorial cartoonists]]
[[Category:Australian cartoonists]]
[[Category:2009 deaths]]
[[Category:1933 births]]