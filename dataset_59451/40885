{{EngvarB|date=August 2014}}
{{Use dmy dates|date=August 2014}}
{{Infobox person
| honorific_prefix          =
| name                      = Javed Naseer Rind
| honorific_suffix          =
| native_name               =
| native_name_lang          =
| image                     = Javed_Naseer_Rind.jpg
| image_size                = 200px
| alt                       =
| caption                   =
| birth_name                = Javed Naseer Rind
| birth_date                = <!-- {{Birth date and age|df=yes|YYYY|MM|DD}} -->
| baptism_date              =
| birth_place               =
| disappeared_date          = 10 September 2011<!-- {{Disappeared date and age|YYYY|MM|DD|YYYY|MM|DD}} (disappeared date then birth date) -->
| disappeared_place         = Mehmoodabad area, Hub city, Laseba district, Balochistan, Pakistan
| disappeared_status        = found dead
| death_date                = 5 November 2011 <!-- {{Death date and age|df=yes|YYYY|MM|DD|YYYY|MM|DD}} (death date then birth date) -->
| death_place               = Khuzdar, Central Balochistan, Pakistan
| death_cause               =
| body_discovered           = Ghazgi Chowk
| resting_place             =
| resting_place_coordinates = <!-- {{Coord|LAT|LONG|type:landmark|display=inline}} -->
| monuments                 =
| residence                 =
| nationality               = Pakistani
| other_names               = Javid Naseer Rind, Javeed Nasir Rind
| ethnicity                 = Baloch <!-- Ethnicity should be supported with a citation from a reliable source -->
| citizenship               =
| education                 =
| alma_mater                =
| occupation                = Senior-sub editor & columnist
| years_active              =
| employer                  = ''Daily Tawar'' (Urdu language)
| organization              =
| agent                     =
| known_for                 =
| notable_works             =
| style                     =
| home_town                 =
| salary                    =
| net_worth                 = <!-- Net worth should be supported with a citation from a reliable source -->
| height                    = <!-- {{height|m=}} -->
| weight                    = <!-- {{convert|weight in kg|kg|lb}} -->
| television                =
| title                     =
| term                      =
| predecessor               =
| successor                 =
| party                     =
| movement                  = Baloch National Movement
| opponents                 =
| boards                    =
| religion                  = <!-- Religion should be supported with a citation from a reliable source -->
| denomination              = <!-- Denomination should be supported with a citation from a reliable source -->
| criminal_charge           = <!-- Criminality parameters should be supported with citations from reliable sources -->
| criminal_penalty          =
| criminal_status           =
| spouse                    =
| partner                   = <!-- unmarried life partner; use ''Name (1950–present)'' -->
| children                  =
| parents                   =
| relatives                 =
| callsign                  =
| awards                    =
| signature                 =
| signature_alt             =
| signature_size            =
| module                    =
| module2                   =
| module3                   =
| module4                   =
| module5                   =
| module6                   =
| website                   = <!-- {{URL|Example.com}} -->
| footnotes                 =
| box_width                 =
}}

'''Javed Naseer Rind''', also transliterated as '''Javed Nasir Rind''', '''Javid Naseer Rind''' or '''Jawaid Naseer Rind''', (died 5 November 2011) was abducted 10 September 2011 and his corpse was discovered 5 November 2011 in [[Balochistan]], Pakistan.<ref name=dawn>{{cite web|url=http://beta.dawn.com/news/671594/journalist-found-shot-dead-in-hub |title=Journalist found shot dead in Hub |publisher=Dawn.com |date=5 November 2011 |accessdate=2 October 2013}}</ref><ref name=dailytimes1>{{cite web|url=http://www.dailytimes.com.pk/default.asp?page=2011%5C11%5C06%5Cstory_6-11-2011_pg7_3 |first=mohammad |last=zafar |title=Bullet-riddled body of journalist found in Khuzdar |publisher=Daily Times |date=6 November 2011 |accessdate=2 October 2013}}</ref> Rind was an Urdu-language editor and columnist for the ''Daily Tawar'' and an active member of the [[Baloch National Movement]] and one of three journalists with the newspaper that were killed. His murder is an example of the "kill-and-dump method" of eliminating enemies, although the side committing the offence remains in dispute, and this method was used in similar cases in Balochistan, such as Lala Hameed Hayatan, Siddiq Eido and Ilyas Nazar.<ref name=reuters>{{cite news|first=Matthew |last=Green |title=Special Report: The struggle Pakistan does not want reported |publisher=Reuters |date=24 September 2013 |accessdate=1 December 2013 |url=http://www.reuters.com/article/2013/09/24/us-pakistan-disappearances-specialreport-idUSBRE98N0OA20130924}}</ref><ref name=presspakistan/>

== Early history ==
Javed Naseer Rind was an editor and columnist for the Urdu-language ''Daily Tawar'', which is known as a pro-independence and anti-government newspaper that is based in Quetta.<ref name=reuters /><ref name=presspakistan /><ref name=CPJ2>{{cite web|author=Javed Naseer Rind |url=http://cpj.org/killed/2011/javed-naseer-rind.php |title=Javed Naseer Rind – Journalists Killed – Committee to Protect Journalists |publisher=Cpj.org |accessdate=2 October 2013}}</ref> Rind was an active member of the Baloch National Movement, as were other employees of the ''Daily Tawar'', and he was a coordinator for his local.<ref name=reuters /><ref name=examiner>{{cite news|first=Ahmar |last=Mustikhan |title=Baloch publisher says executed journalist lost his job after contacting him |publisher=examiner.com |date=10 November 2011}}</ref><ref name=CPJ3>{{cite web|author=In Pakistan, missing journalist found dead |url=http://cpj.org/2011/11/in-pakistan-missing-journalist-found-dead.php |title=In Pakistan, missing journalist found dead – Committee to Protect Journalists |publisher=Cpj.org |date=7 November 2011 |accessdate=2 October 2013}}</ref> A London-based source said that Rind had been fired and lost his position within the movement when he had been in communication with him while reporting on the UK trial of Hyrbyair Marri and Faiz Baluch on charges of terrorism.<ref name=examiner />

== Death ==
{{Location map many
| Pakistan
| AlternativeMap = Balochistan in Pakistan.svg
| width       = 300
| float       = left
| border      =
| caption     = Places in Balochistan, Pakistan, where journalists have been killed. The red shaded area is Balochistan, one of the four provinces of Pakistan. Mentioned locations within Pakistan relative to the capital Islamabad and the largest city Karachi.
| alt         = Places in Balochistan where journalists have been killed.
| places =
<!--first label/marker-->
    | label1 = Islamabad
    |lat1_deg = 33 | lat1_min = 44 | lat1_sec= 0 | lat1_dir = N
    | lon1_deg = 73 | lon1_min = 6 |lon1_sec= 0 | lon1_dir = E
<!--second label/marker-->
    | label2 = Karachi
    | mark2 = Blue_pog.svg
    | lat2_deg = 24 | lat2_min = 53 | lat2_sec= 36 | lat2_dir = N
    | lon2_deg = 67 | lon2_min = 1 | lon2_sec= 39 | lon2_dir = E
<!--fourth label/marker-->
   | label4 = Khuzdar
    | mark4 = Blue_pog.svg
    | lat4_deg = 27| lat4_min =  48| lat4_sec =0  | lat4_dir = N
    | lon4_deg = 66| lon4_min = 37| lon4_sec =0  | lon4_dir = E
<!--fifth label/marker-->
   | label5 = Quetta
    | mark5 = Blue_pog.svg
    | lat5_deg = 30| lat5_min =  12| lat5_sec =34| lat5_dir = N
    | lon5_deg = 67| lon5_min = 1 | lon5_sec =5  | lon5_dir = E
<!--repeat as needed up to 9-->
}}
According to his nephew who was an eyewitness, Javed Naseer Rind was abducted by gunpoint and forcibly disappeared along with Rind's relative Abdul Samad Baloch near his home in [[Hub Tehsil|Hub City]], [[Lasbela District]], Balochistan, Pakistan on 10 September 2011.<ref name=presspakistan>{{cite news|title=Baloch Journalist Javid Naseer Rind Kidnapped|url=http://thebalochhal.com/2011/09/13/editorial-the-kidnapping-of-another-baloch-journalist/ |accessdate=3/12/12 |newspaper=The Baloch Hal |date= September 2011|first=Malik Siraj |last=Akbar|location=Pakistan|page=2}}</ref> After his disappearance, Balochistan Union of Journalists requested government intervention.<ref name=newsblaze>{{cite web|url=http://newsblaze.com/story/20111108194519khan.nb/topstory.html |title=Abducted Journalist Javed Naseer Rind's Body Found |publisher=Newsblaze.com |date=8 November 2011 |accessdate=2 October 2013}}</ref> His corpse was found 5 November 2011 near Ghazgi Chowk, [[Khuzdar]], and his corpse had multiple bullet wounds, showed evidence of torture, and was found with a slip of paper identifying him as the journalist who had been previously kidnapped.<ref name=dawn /><ref name=dailytimes1 /> His corpse was then transported to the civil hospital in Khuzdar for an autopsy.<ref name=balochhalnews>{{cite news|title=Disappeared Baloch Journalist's Bullet-riddled body found in Khuzdar|url=http://www.thebalochhal.com/2011/11/disappeared-baloch-journalists-bullet-riddled-body-found-in-khuzdar|accessdate=3/12/12|newspaper=Baloch Hal News|author=Malik Siraj Akbar|agency=Baloch Hal News|location=Balochistan|page=1|date=5 November 2011}}</ref>

== Context ==
Balochistan is a resource rich region of Pakistan with an active separatist movement that has caused conflict between Pakistan and the people living within the region. The Balochistan conflict has created an unstable environment for journalists as they receive pressure from both the Pakistani government and nationalist groups about the coverage given to the other side.<ref name=dailytimes2>{{cite news|title=COMMENT : Truth invites IRE |url=http://www.dailytimes.com.pk/default.asp?page=2013%5C09%5C08%5Cstory_8-9-2013_pg3_2 |accessdate=3/12/12|newspaper=Daily Times|date= 8 September 2013 |first=Mir Mohammad Ali |last=Talpur |page=2}}</ref> Journalists are often the target of death threats.<ref name=DawnBaloch /><ref name=CPJ1 /><ref name=critical>{{cite news|title=Javid Naseer Rind|url=http://criticalppp.com/archives/61697|accessdate=3/12/12|newspaper=Critical PPP|author=Laibaah|location=Balochistan|page=11|date=5 November 2011}}</ref>
By 2012, between 28 and 33 journalists were killed in Balochistan.<ref>{{cite news|url=http://thepeninsulaqatar.com/pakistan-afghanistan/223682-journalists-demand-more-protection-from-authorities-.html |title=Journalists demand more protection from authorities |publisher=The Peninsula (Qatar) |date=29 January 2013 |accessdate=29 January 2013}}</ref><ref name=dailytimes3>{{cite news|first=Muhammad |last=Akbar Notezai |title=To be a journalist in Balochistan |publisher=Daily Times |date=23 July 2013 |accessdate=1 December 2013 |url=http://www.dailytimes.com.pk/default.asp?page=2013%5C07%5C23%5Cstory_23-7-2013_pg3_6}}</ref> Rind is among the 22 journalists between 2008 and 2012 who have become casualties of the [[Balochistan conflict]].<ref name=DawnBaloch>{{cite news|url=http://dawn.com/2012/07/09/22-balochistan-journalists-killed-in-four-years/ |title=22 Balochistan journalists killed in four years |newspaper=Dawn (Pakistan) |date=9 July 2012 |accessdate=27 November 2012}}</ref> In 2010, Balochistan was considered the most dangerous place for journalists with six out of ninety seven deaths worldwide. In 2011, Rind's death pushed the count to over ten deaths with no investigations<ref name=newsblaze /> Nationwide, he was one of 12 Pakistani journalists killed in 2011 and one of around 23 Pakistani journalists killed from the beginning of 2010 to the end of 2011.<ref name=DawnPakistan>{{cite news|first=Ikram |last=Junaidi |title=2011 saw 12 journalists killed in Pakistan |newspaper=Dawn (Pakistan) |date=31 December 2011 |accessdate=9 November 2013 |url=https://dawn.com/news/684514/2011-saw-12-journalists-killed-in-pakistan-2}}</ref><ref name=officialnews1>{{cite news|first=Amin |last=Yousuf |agency=General Pakistan Federal Union of Journalists  |title=Journalists of Pakistan face constant danger: 2011 over view |accessdate=3 December 2012 |newspaper=Official News |date=31 December 2011 |location=Pakistan |pages= 4, 6 |url=http://www.officialnews.pk/13981/journalists-of-pakistan-face-constant-danger-2011-overview-by-amin-yousuf-secretary-general-pakistan-federal-union-of-journalists/ }}</ref> Regionwide, he was one of 19 killed in 2011 in Afghanistan, Pakistan, and India, according to the South Asian Free Media Association.<ref name=DawnPakistan />

== Impact ==
Following Rind's death the Balochistan Union of Journalists has requested a high level committee be put together by the government to investigate his death as well as the death of numerous colleagues.<ref name=CPJ2 />

The perpetrator of the kill-and-dump method is disputed with some sides suggesting that it is Pakistan security and other sides claiming it is the Baloch Armed Defence Army.<ref name=CPJ1>{{cite news|title=Threats and Menace: Pakistan's war on words|url=http://cpj.org/blog/2012/02/threats-and-menace-pakistans-war-on-words.php|accessdate=22 March 2012|newspaper=Committee to Protect Journalists|date=21 February 2012|author=Bob Dietz|agency=CPJ|page=4}}</ref><ref name=rsf>{{cite web|url=http://en.rsf.org/pakistan-bullet-riddled-body-of-missing-08-11-2011,41357.html |title=Bullet-riddled body of missing journalist found in Balochistan – Reporters Without Borders |publisher=En.rsf.org |accessdate=2 October 2013}}</ref>

== Reactions ==
In Pakistan, the Baluchistan Union of Journalists issued a condemnation after his abduction.<ref name=CPJ2 /><ref name=newsblaze /> A spokesperson for [[Committee to Protect Journalists]] joined with the Balochistan Union of Journalists and called for Rind's journalism to be investigated as motive for the abduction.<ref name=CPJ3 /><ref name=dailytimes3 />

[[Reporters Without Borders]] and [[International Federation of Journalists]] have also spoken out against Rind's death.<ref name=examiner /><ref name=ifj>{{cite web|url=http://www.ifj.org/en/articles/abducted-journalist-found-dead-tortured-in-balochistan |title=IFJ Global – Abducted Journalist Found Dead, Tortured in Balochistan |publisher=IFJ.org |accessdate=2 October 2013}}</ref> A spokesperson for IFJ said, "Authorities must act swiftly to end this cycle of violence and impunity in what is statistically one of the most dangerous places in the world to be a journalist."<ref name=ifj />

The [[Pakistan Federal Union of Journalists]] is educating journalists about how to react when they receive threats and the support they can get as a result.<ref name=officialnews1 />

== See also ==
* [[List of journalists killed during the Balochistan conflict (1947–present)]]

== References ==
{{Reflist|30em}}

== External links ==
{{Baloch nationalism}}

{{DEFAULTSORT:Rind, Javed Naseer}}
[[Category:Articles created via the Article Wizard]]
[[Category:Year of birth missing]]
[[Category:2011 deaths]]
[[Category:Baloch journalists]]
[[Category:People from Balochistan, Pakistan]]
[[Category:Assassinated Baloch journalists]]
[[Category:People murdered in Balochistan, Pakistan]]