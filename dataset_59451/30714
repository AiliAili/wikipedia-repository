{{Use dmy dates|date=March 2012}}
{{Use Australian English|date=March 2012}}
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name        = Diorama
| Type        = studio
| Artist      = [[Silverchair]]
| Cover       = Silverchair - Diorama.jpg
| Released    = 31 March 2002
| Recorded    = [[Daniel Johns]]' home, 301 Studios and Mangrove Studios, [[Australia]], April–October 2001
| Genre       = {{flatlist|
* [[Post-grunge]]
* [[alternative rock]]
* [[baroque pop]]
* [[art rock]]
}}
| Length      = 57:13
| Label       = {{flatlist|
* [[Atlantic Records|Atlantic]]
* [[Eleven: A Music Company|Eleven]]
}}
| Producer    =  {{flatlist|
* [[David Bottrill]]
* [[Daniel Johns]]
}}
| Last album  = ''[[The Best Of: Volume 1 (Silverchair album)|The Best Of: Volume 1]]''<br />(2000)
| This album  = '''''Diorama'''''<br />(2002)
| Next album  = ''[[Rarities 1994–1999]]''<br />(2002)
| Misc        =
{{Singles
  | Name        = Diorama
  | Type        = studio
  | single 1    = [[The Greatest View]]
  | single 1 date = 28 January 2002
  | single 2    = [[Without You (Silverchair song)|Without You]]
  | single 2 date = 13 May 2002
  | single 3    = [[Luv Your Life]]
  | single 3 date = 2 September 2002
  | single 4    = [[After All These Years (Silverchair song)|After All These Years]]
  | single 4 date = 1 December 2002
  | single 5    = [[Across the Night]]
  | single 5 date = 11 March 2003
}}
}}

'''''Diorama''''' is the fourth [[studio album]] by Australian [[alternative rock]] band by [[Silverchair]]. Released on 31 March 2002 by [[Atlantic Records|Atlantic]]/[[Eleven: A Music Company|Eleven]]. It won the 2002 [[ARIA Music Awards of 2002|ARIA Music Award]] for Best Group and Best Rock Album. The album was [[Record producer|co-produced]] by [[Daniel Johns]] and [[David Bottrill]]. While Bottrill had worked on albums for a variety of other bands, ''Diorama'' marked the first production credit for [[lead singer]] Johns.

Johns wrote most of the album at the [[piano]] instead of his usual guitar, while the band took a 12-month break following their previous studio album, ''[[Neon Ballroom]]''. Silverchair worked with composer [[Van Dyke Parks]] on ''Diorama''; the album contains numerous [[orchestra]]l arrangements and [[power ballad]]s, a change from the [[post-grunge]] music typical of their earlier work, but consistent with the band's previous orchestrations on ''[[Neon Ballroom]]''. The album's title refers to "a world within a world".<ref>{{cite web|url=http://www.chairpage.com/biography/ |title=Silverchair - Biography|publisher=Chairpage.com|accessdate=2008-04-08}}</ref> Five singles were released: "[[The Greatest View]]", "[[Without You (Silverchair song)|Without You]]", "[[Luv Your Life]]", "[[Across the Night]]" and "After All These Years". All except "After All These Years", a promotional single, appeared on the [[ARIA Charts|Australian singles chart]].

''Diorama'' was successful in the charts but was not as well received by critics as the band's earlier albums. It reached number one on the [[Australian Recording Industry Association]] (ARIA) [[ARIA Charts|Albums Chart]] and received a rating of 71 (out of 100) on review aggregator [[Metacritic]]. It was certified triple-platinum by ARIA, selling in excess of 210,000 copies, and won five [[ARIA Awards]] in [[ARIA Music Awards of 2002|2002]]. ''Diorama'' was nominated for Highest Selling Album in [[ARIA Music Awards of 2003|2003]], and three songs from the album were nominated for awards over the two years.

==Recording and production==
On ''Diorama'', Silverchair worked with a new producer, [[David Bottrill]], who replaced [[Nick Launay]]. Though Launay had produced the band's three previous albums, lead singer Daniel Johns decided that he needed someone "who understood where he wanted to go".<ref name=Mix /> Johns believed ''Diorama'' would be "the kind of record that people were either going to be into or were really going to hate",<ref name=Mix /> and needed a producer who would understand the band's new direction. He interviewed several candidates, eventually choosing Bottrill and taking the role of co-producer himself.<ref name=Mix>{{cite web|url=http://mixonline.com/recording/interviews/audio_silverchair/ |title=Silverchair's Diorama|publisher=''[[Mix (magazine)|Mix]]''|date=1 January 2003|author=David John Farinella|accessdate=2008-03-21}}</ref>

Johns initially recorded eight songs, only to delete the files thinking they were too similar to tracks on the previous album, ''[[Neon Ballroom]]''. Leaving the security and darkness of his earlier work, he restarted from scratch to create something more uplifting.<ref name=DrumMedia>{{cite news|title=Another Point of View|author=Mark Neilsen|publisher=''[[Drum Media]]''|date=23 April 2002}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=2 --></ref> ''Diorama'' represented a radical change in Silverchair's musical structure; the heavy [[grunge]] influence on their prior work was replaced by [[string (music)|string]] and [[horn (music)|horn]] ensembles and highly complex song structures. Johns felt more comfortable in making this radical change rather than a minor one, finding it helped him to regain his passion for music, which had diminished during the grunge days.<ref name=Mix /> Johns wrote much of the album at a [[grand piano|baby grand piano]]; he had previously taught himself the instrument and composed songs on it for the first time with ''Diorama''. This change in songwriting technique had a significant effect on the sound of the album; Johns commented on the difference in how his vocals resonated with piano as compared to guitar.<ref name=DrumMedia /> He worked with others in developing the album; [[Van Dyke Parks]] ([[Beach Boys]], [[U2]]) collaborated on [[orchestra]]l arrangements, and the pair spent much of their recording time attempting to describe the music in metaphorical terms, with Johns describing Parks' orchestral swells as "tidal waves" and violins as "a flock of birds".<ref name=ComingOfAge /> The pair described the collaborative experience as "mind-blowing".<ref name=ComingOfAge /> A [[DVD]] entitled ''Across The Night: The Creation Of Diorama'' was released in 2002, featuring interviews with Johns and Parks.<ref name=Silverlining />

Several songs on ''Diorama'' were inspired by Johns' then-girlfriend [[Natalie Imbruglia]], but he cautioned against possible misinterpretations of the songs, stating "Everyone will think that any lyric that's about someone in a positive light will be about her" and noting that there were other people he cared for about whom he wrote the songs.<ref name=Natalie /> Johns denied rumours that he had written songs intending Imbruglia to sing them.<ref name=Silverlining />

Silverchair intended to tour supporting ''Diorama'' following its release, but plans were postponed when Johns developed [[reactive arthritis]]; causing his joints to [[Swelling (medical)|swell]] and making guitar playing and singing too painful.<ref>{{cite web|url=http://www.ama.com.au/web.nsf/doc/SHED-5EXHVQ |title=Dr Kerryn Phelps, Health Editor, with Steve Leibmann, Channel Nine, 'Today'|publisher=Australian Medical Association|date=6 May 2002|accessdate=2008-04-07}}</ref><ref name=MW>{{cite news|title=Guitarist takes life as it comes|publisher=''Music Writer''|author=Patrick Donovan|date=17 October 2002}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=14 --></ref> After performing "[[The Greatest View]]" at the [[ARIA Music Awards of 2002|2002]] [[ARIA Awards]], Johns said that he wanted "to perform [''Diorama''{{'}}s] 11 songs at least once in front of an audience" before laying the album to rest.<ref name=Silverlining>{{cite news|url=http://www.smh.com.au/articles/2002/10/20/1034561389193.html |title=Silver lining |publisher=''[[The Sun-Herald]]''|author=David Anderson |date=21 October 2002|accessdate=2008-05-10}}</ref> He travelled to [[California]] to receive treatments for his arthritis, including daily [[physiotherapy]].<ref name=MW />

==Album and single releases==
{{Listen|filename=Silverchair-TheGreatestView-15s.ogg|title="The Greatest View"|description="The Greatest View" contained heavier guitars than many other Silverchair songs,<ref name=ComingOfAge>{{cite news|title=Coming of Age |publisher=''Guitar Player''|date=August 2002}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=32 --></ref> and reflected Johns' experience of the media "watching over" him.<ref name=SLLAL />|format=[[Ogg]]}}
{{Listen|filename=Silverchair-LuvYourLive-25s.ogg|title="Luv Your Life"|description="Luv Your Life", dedicated by Johns to "all my ladies",<ref name=Kerrang /> was written on piano and featured an orchestral arrangement by Van Dyke Parks.<ref name=SLLAL />|format=[[Ogg]]}}

Following a 31 March 2002 release on record label [[Eleven: A Music Company|Eleven]],<ref name=PopMatters /> ''Diorama'' reached number one on the [[ARIA Charts|ARIA Albums Chart]] on 14 April, making it Silverchair's fourth chart-topping album.<ref name=auscharts>{{cite web|url=http://australian-charts.com/showitem.asp?key=4155&cat=a |title=Silverchair - Diorama|publisher=australian-charts.com|accessdate=2008-03-31}}</ref> It went on to be certified triple-platinum by [[Australian Recording Industry Association|ARIA]],<ref name=certif>{{cite web|url=http://www.aria.com.au/pages/aria-charts-accreditations-albums-2002.htm |title=ARIA Charts - Accreditations - 2002 Albums |publisher=[[Australian Recording Industry Association|ARIA]] |accessdate=2008-03-31 |deadurl=yes |archiveurl=https://web.archive.org/web/20070613020750/http://www.aria.com.au/pages/aria-charts-accreditations-albums-2002.htm |archivedate=13 June 2007 |df=dmy }}</ref> indicating sales in excess of 210,000 copies.<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-accreditations.htm |title=Accreditations |publisher=[[Australian Recording Industry Association|ARIA]] |accessdate=2008-05-11 |deadurl=yes |archiveurl=https://web.archive.org/web/20110721000000/http://www.aria.com.au/pages/aria-charts-accreditations.htm |archivedate=21 July 2011 |df=dmy }}</ref> The album peaked at number seven in New Zealand, thirteen in Austria, forty in Switzerland, and 116 in France.<ref name=auscharts /> ''Diorama'' reached number ninety-one on the U.S. [[Billboard 200|''Billboard'' 200]].<ref name=AMGAlbums>{{cite web|url={{Allmusic|class=album|id=r596205/charts-awards|pure_url=yes}} |title=Diorama > Charts & Awards > Billboard Albums|publisher=[[Allmusic]]|accessdate=2008-03-31}}</ref>

The first [[single (music)|single]], "[[The Greatest View]]", was released in advance of the album on 28 January 2002. It reached number three in Australia, where it was also certified gold,<ref>{{cite web|url=http://www.aria.com.au/pages/aria-charts-accreditations-singles-2002.htm |title=ARIA Charts - Accreditations - 2002 Singles |publisher=[[Australian Recording Industry Association|ARIA]] |accessdate=2008-04-03 |deadurl=yes |archiveurl=https://web.archive.org/web/20110611190357/http://www.aria.com.au/pages/aria-charts-accreditations-singles-2002.htm |archivedate=11 June 2011 |df=dmy }}</ref> and number four in New Zealand and Canada.<ref>{{cite web|url=http://australian-charts.com/showitem.asp?interpret=Silverchair&titel=The+Greatest+View&cat=s |title=Silverchair - The Greatest View|publisher=australian-charts.com|accessdate=2008-03-31}}</ref> It charted at number thirty-six on ''Billboard's'' [[Alternative Songs|Hot Modern Rock Tracks]] in 2007 when re-released alongside the band's next album, ''[[Young Modern]]''.<ref name=AMGsingles>{{cite web|url={{Allmusic|class=artist|id=p144747/charts-awards/billboard-singles|pure_url=yes}} |title=Silverchair > Charts & Awards > Billboard Singles|publisher=[[Allmusic]]|accessdate=2008-03-31}}</ref> Johns wrote "The Greatest View" as a response to the media "always watching [him] in different way".<ref name=SLLAL /> It was not intended to be aggressive, rather a straightforward commentary on the media frenzy that had surrounded the band for many years.<ref name=SLLAL>{{cite news|title=
Silverchair - Luving life at last |publisher=''Rock Sound''|author=Jennifer Weir}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=33 --></ref>

On 13 May 2002, "[[Without You (Silverchair song)|Without You]]" was released as the second single. It peaked at number eight in Australia, but dropped to number twenty-nine the following week, only spending five weeks on the chart.<ref>{{cite web|url=http://australian-charts.com/showitem.asp?key=234328&cat=s |title=Silverchair - Without You|publisher=australian-charts.com|accessdate=2008-03-31}}</ref> The song was first announced by Silverchair [[bassist|bass guitarist]] [[Chris Joannou]] in November 1999, when he told fans the band had "a very small cache of recorded material stored away", including "Without You".<ref>{{cite web|url=http://www.mtv.com/news/articles/1434061/19991214/silverchair.jhtml |title=Silverchair Plays Last Gig Before Its 2000 Time-Out|publisher=[[MTV News]]|author=Sorelle Saidman|date=14 December 1999|accessdate=2008-04-12}}</ref> "Without You" was followed by "[[Luv Your Life]]", which peaked at number twenty in Australia after its 20 September release.<ref>{{cite web|url=http://australian-charts.com/showitem.asp?key=287548&cat=s |title=Silverchair - Luv Your Life|publisher=australian-charts.com|accessdate=2008-03-31}}</ref> The inspiration for the song came to Johns during a [[therapy]] session, based on the idea that "there were people in the world who needed treatment but couldn't afford therapy."<ref name=Natalie /> Johns composed most of the song's lyrics while listening to a therapist.<ref name=Natalie>{{cite news|title=Daniel Johns' debt to Natalie |publisher=''[[The Daily Telegraph (Australia)|The Daily Telegraph]]''|author=Kathy McCabe}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=29 --></ref> In a performance at [[London]]'s [[Shepherds Bush Empire]], Johns jokingly said "Luv Your Life" was dedicated "to all my ladies".<ref name=Kerrang>{{cite news|title=Silver Side Up|publisher=''[[Kerrang!]]''|date=21 June 2003}}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=11 --></ref>

"After All These Years", a promotional single, followed "Luv Your Life", but failed to reach the charts.<ref name=auscharts /> The final single "[[Across the Night]]" was released on 11 March 2003. The song, which Johns wrote over nine hours on a sleepless night,<ref name=SLLAL /> peaked at number twenty-four on its three weeks on the Australian chart.<ref>{{cite web|url=http://australian-charts.com/showitem.asp?key=287561&cat=s |title=Silverchair - Across The Night|publisher=australian-charts.com|accessdate=2008-03-31}}</ref> The arrangement by Parks features twin [[Keyboard instrument|keyboards]] and a string ensemble.<ref name=SLLAL /><ref>{{cite web|url=http://www.rollingstone.com/artists/silverchair/articles/story/5935373/silverchair_see_past_tomorrow |title=Silverchair See Past Tomorrow|publisher=''[[Rolling Stone]]''|author=David Fricke|date=20 May 2003|accessdate=2008-05-11}}</ref> The band's much-delayed tour in support of ''Diorama'' took its name from "Across the Night".<ref>{{cite news|title=Silverchair Live! - Rock Sound |publisher=''Rock Sound''|author=Robyn Doreian }}<!-- http://www.silverchair.nu/sc_nu/interviews/showinterviews.php?id=10 --></ref>

=== ''The Diorama Box'' ===
{{Infobox album <!-- See Wikipedia:WikiProject_Albums -->
| Name       = The Diorama Box
| Type       = box
| Artist     = [[Silverchair]]
| Cover      = DioramaBox.jpg
| Alt        = 
| Released   = 1 December 2002
| Recorded   = 
| Genre      = [[Post-grunge]], [[alternative rock]], [[baroque pop]], [[art rock]]
| Length     = 1:21:34
| Label      = [[Eleven: A Music Company|Eleven]]
| Producer   = [[David Bottrill]], [[Daniel Johns]]
}}

On 1 December 2002, a limited-edition CD [[box set]] was released as ''The Diorama Box'', consisting of the first three singles from the album as well as an exclusive single, "[[After All These Years (Silverchair song)|After All These Years]]".

==Response==
{{Album ratings
| rev1 = [[Allmusic]]
| rev1score = {{rating|4|5}}<ref name="allmusic">{{cite web|last=Torreano |first=Bradley |url=http://www.allmusic.com/album/r596205 |title=Diorama - Silverchair |publisher=AllMusic |date=30 July 2002 |accessdate=2011-08-15}}</ref>
| rev2 = ''[[Blender (magazine)|Blender]]''
| rev2score = {{rating|3|5}}<ref name="metacritic.com">{{cite web|url=http://www.metacritic.com/music/diorama/critic-reviews |title=Critic Reviews for Diorama at Metacritic |publisher=Metacritic.com |date= |accessdate=2011-08-15}}</ref>
| rev3 = ''[[Mojo (magazine)|Mojo]]''
| rev3score = {{rating|3.5|5}}<ref name="metacritic.com"/>
| rev4 = ''[[NME]]''
| rev4score = {{rating|3|10}}<ref>{{cite web|url=http://www.nme.com/reviews/artistKeyname/6626 |title=NME Album Reviews - Silverchair : Diorama |publisher=Nme.Com |date=9 August 2002 |accessdate=2011-08-15}}</ref>
| rev5 = [[PopMatters]]
| rev5score = (very favorable)<ref>{{cite web|last=Chabe |first=Matthew |url=http://www.popmatters.com/pm/review/silverchair-diorama2 |title=Silverchair: Diorama < PopMatters |publisher=Popmatters.com |date= |accessdate=2011-08-15}}</ref>
| rev6 = ''[[Q (magazine)|Q]]''
| rev6score = {{Rating|3|5}}<ref name="metacritic.com"/>
| rev7 = ''[[Rolling Stone]]''
| rev7score = {{rating|3|5}}<ref>{{cite web|author=Mark Kemp |url=http://www.rollingstone.com/music/albumreviews/diorama-20020716 |title=Diorama by Silverchair &#124; Music Reviews |publisher=Rollingstone.com |date=16 July 2002 |accessdate=2011-08-15}}</ref>
}}

''Diorama'' received a score of 71 out of 100 on review aggregator [[Metacritic]], based on nine reviews.<ref>{{cite web|url=http://www.metacritic.com/music/artists/silverchair/diorama?q=Silverchair |title=Silverchair: Diorama (2002)|publisher=[[Metacritic]]|accessdate=2008-03-16}}</ref> Australian radio station [[Triple J]] listeners voted the album number one on their Top 10 Albums of 2002, while Triple J staff Rosie Beaton and Gaby Brown placed it third and fifth respectively.<ref>{{cite web|url=http://www.abc.net.au/triplej/2004/best_albums/2002.htm |title=Best Albums of 2002 |publisher=[[abc.net.au]]|work=[[Triple J]]|accessdate=2008-03-22}}</ref>

Music magazine ''[[Rolling Stone]]'' gave ''Diorama'' four and a half stars in Australia and three out of five stars in the US. Reviewer [[Mark Kemp]] praised Silverchair's development, saying that the band had developed a strong, independent musical ability, in contrast to their heavily influenced debut album, ''[[Frogstomp]]''. Kemp spoke highly of the "heavy orchestration, unpredictable melodic shifts and a whimsical pop sensibility", also noting Parks' arrangements gave the music "more breadth and depth". He argued that the album's strength was a product of Johns' confidence, resulting in high quality on "World Upon Your Shoulders", "Tuna in the Brine", and "After All These Years". However, "[[Without You (Silverchair song)|Without You]]" saw Silverchair slip into "old habits", according to Kemp, and contained an "[[MTV]]-approved hook".<ref name=RSReview>{{cite web|url=http://www.rollingstone.com/artists/silverchair/albums/album/201588/review/5944466/diorama |title=Silverchair: Diorama|publisher=''[[Rolling Stone]]''|author=Mark Kemp|date=8 August 2002|accessdate=2008-03-16|archiveurl=https://web.archive.org/web/20071217034356/http://www.rollingstone.com/artists/silverchair/albums/album/201588/review/5944466/diorama <!--Added by H3llBot-->|archivedate=17 December 2007}}</ref>

Bradley Torreano, of review website [[Allmusic]], gave ''Diorama'' four stars, labeling it an AMG Album Pick. He began by noting that Silverchair's improvement from the ''Frogstomp'' era was impressive, and that ''Diorama'' saw the band "finally growing into their own skin."<ref name="allmusic"/> Bottrill's production was praised, and the result likened to [[Big Country]] and [[U2]], while Johns showcased "his rich voice and shockingly catchy tunes with a gusto missing from their earlier albums". Torreano's criticism was reserved for two songs on the album; he described the apparent [[Goo-Goo Dolls]] influences on "Without You" as "an unwelcome twist", and felt that on "One Way Mule", the band reverted "back to their grunge sound".<ref name=AMG>{{cite web|url={{Allmusic|class=album|id=r596205|pure_url=yes}} |title=Diorama > Overview|publisher=[[Allmusic]]|author=Bradley Torreano|accessdate=2008-03-16}}</ref>

James Jam, of music magazine ''[[NME]]'', was critical of ''Diorama'', calling it "over-produced Aussie rock". Jam compared Silverchair to [[Bryan Adams]] in their attempt to "venture boldly into exciting new musical landscapes". "Tuna in the Brine" was "grossly pretentious and overblown", while he saw the album as a whole as inoffensive, especially in comparison to the band's past post-grunge. According to Jam, the band were not trying to make a mature musical statement with the album, but rather to "impress their parents".<ref name="NMEreview">{{cite web|url=http://www.nme.com/reviews/silverchair/6626 |title=Over-produced Aussie rock|publisher=''[[NME]]''|author=James Jam|date=9 August 2002|accessdate=2008-04-08}}</ref>

Nikki Tranter of pop-music website [[PopMatters]] called the album mature, praising everything from the cover art to the "finely crafted pop melodies". Tranter praised ''Diorama'' for standing out in the "very similar" Australian music scene. The majority of songs on the album were rated highly; she thought "[[The Greatest View]]" was a stand-out with "orchestral twangs", and "After All These Years" had "sweeping horns, introspective lyrics and soft, haunting vocals".<ref name=PopMatters>{{cite web|url=http://www.popmatters.com/music/reviews/s/silverchair-diorama.shtml |title=Silverchair: Diorama|publisher=[[PopMatters]]|author=Nikki Tranter|date=6 September 2002|accessdate=2008-03-16}}</ref>

Rob O'Connor of [[Yahoo!]]'s [[Yahoo! Music|music website]] gave ''Diorama'' a positive review, agreeing that the band had matured greatly since their early high school releases. The pop songs on the album, "Luv Your Life" and "Too Much Of Not Enough", were said to "glide", and O'Connor praised Johns for "whisper[ing] his lyrics with grace and subtlety" where in the past he would "shout in angst", drawing comparisons to [[Elliott Smith]]. His main critique of the album was that it still contained some "obligatory 'grunge' efforts"; he felt eliminating those would allow the band to reach its full potential.<ref name=yahreview>{{cite web|url=http://music.yahoo.com/read/story/12036986 |title=Diorama |author=Rob O'Connor |publisher=[[Yahoo! Music]] |date=8 June 2002 |accessdate=2008-04-07 |deadurl=yes |archiveurl=https://web.archive.org/web/20080605012055/http://music.yahoo.com/read/story/12036986 |archivedate=5 June 2008 }}</ref>

==Commercial performance==
''Diorama'' charted highest in Australia; spending 50&nbsp;weeks on the [[ARIA Charts|ARIA Albums Chart]] including a week at number one. This popularity was not matched in other countries; it spent 10 weeks or less on all other charts, though reaching number seven in New Zealand.

==Track listing==
{{Track listing
| total_length    = 57:13
| all_writing     = [[Daniel Johns]]
| title1          = [[Across the Night]]
| length1         = 5:37
| title2          = [[The Greatest View]]
| length2         = 4:06
| title3          = [[Without You (Silverchair song)|Without You]]
| length3         = 5:17
| title4          = World Upon Your Shoulders
| length4         = 4:37
| title5          = One Way Mule
| length5         = 4:15
| title6          = Tuna in the Brine
| length6         = 5:40
| title7          = Too Much of Not Enough
| length7         = 4:43
| title8          = [[Luv Your Life]]
| length8         = 4:29
| title9          = The Lever
| length9         = 4:22
| title10         = My Favourite Thing
| length10        = 4:14
| title11         = After All These Years / Outro ([[hidden track]])
| length11        = 9:53
}}

===Notes===
* A limited edition of the album comes with a short film entitled "The Making of ''Diorama''".
* The Australian limited edition has a cardboard cover, and a gift promo card included inside.
* The vinyl version of the album was issued only as a promo, and is limited to 500 copies worldwide.
* There is a limited edition cassette of the album.

==Personnel==
*[[Daniel Johns]]&nbsp;– vocals, guitars, piano (tracks 2, 9, 11), harpsichord (track 1), orchestral arrangements (tracks 2, 4, 10, 11)
*[[Ben Gillies]]&nbsp;– drums, percussion
*[[Chris Joannou]]&nbsp;– bass guitar

'''Additional personnel'''
*[[David Bottrill]]&nbsp;– production
*[[Van Dyke Parks]]&nbsp;– orchestral arrangements (tracks 1, 6, 8)
*[[Larry Muhoberac]]&nbsp;– orchestral arrangements (tracks 2, 4, 10)
*Rob Woolf&nbsp;– hammond organ (tracks 3, 9, 10)
*Michele Rose&nbsp;– pedal steel (track 7)
*[[Paul Mac]]&nbsp;– piano (tracks 1, 4, 6, 7, 8, 10)
*[[Jim Moginie]]&nbsp;– keyboards (tracks 2, 5), piano (track 5)

==Charts==
{| class="wikitable sortable plainrowheaders" style="text-align:center;"
|-
!Chart (2002)
!Peak
|-
{{album chart|Australia|1|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
{{album chart|Austria|13|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
{{album chart|France|116|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
{{album chart|Germany3|12|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
{{album chart|New Zealand|7|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
{{album chart|Switzerland|40|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
! scope="row"| UK Albums ([[Official Charts Company|OCC]])<ref>{{cite web|url=http://zobbel.de/cluk/CLUK_S.HTM |title=Chart Log UK (1994–2010): DJ S – The System Of Life |publisher=zobbel.de |accessdate=5 April 2015 |deadurl=yes |archiveurl=https://web.archive.org/web/20150322040621/http://zobbel.de/cluk/CLUK_S.HTM |archivedate=22 March 2015 |df=dmy }}</ref>
| 91
|-
{{album chart|Billboard200|91|artist=Silverchair|album=Diorama|accessdate=5 April 2015|rowheader=true}}
|-
|}

==Certifications==
{{Certification Table Top}}
{{Certification Table Entry|region=Australia|award=Platinum|number=3|artist=Silverchair|title=Diorama|type=album|relyear=2002|certyear=2002|autocat=yes|accessdate=5 April 2015}}
{{Certification Table Bottom}}

==See also==
{{portal|Music of Australia}}
*[[Silverchair discography]]

==References==
{{reflist|30em}}
{{s-start}}
{{succession box
 | before = ''[[A New Day Has Come]]'' by [[Celine Dion]]
 | title = Australian [[ARIA Charts|ARIA Albums Chart]] [[Number-one albums of 2002 (Australia)|number-one album]]
 | years = 8–14 April 2002
 | after = ''[[Laundry Service]]'' by [[Shakira]]
}}
{{end}}
{{Silverchair}}
{{featured article}}

[[Category:2002 albums]]
[[Category:Albums produced by David Bottrill]]
[[Category:ARIA Award-winning albums]]
[[Category:Atlantic Records albums]]
[[Category:Eleven: A Music Company albums]]
[[Category:Silverchair albums]]