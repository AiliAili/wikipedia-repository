{{Infobox journal
| title = Lias
| cover = [[File:Cover Lias.jpg]]
| editor = D. van Miert
| discipline = [[History]] of learning and [[education]]
| language = Multilingual
| formernames =
| abbreviation = Lias
| publisher = [[Peeters Academic Publishing]]
| country =
| frequency = Biannual
| history = 1974-present
| openaccess =
| license =
| impact =
| impact-year =
| website = http://poj.peeters-leuven.be/content.php?url=journal&journal_code=LIAS
| link1 = http://webdoc.ubn.ru.nl/tijd/l/lias/
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR =
| OCLC = 1798652
| LCCN =
| CODEN =
| ISSN = 0304-0003
| eISSN =
}}
'''''Lias, Journal of Early Modern Intellectual Culture and its Sources''''' (Dutch for sheaf or file; French: ''[[:fr:wikt:liasse|liasse]]'') is a biannual [[double blind|double-blind]] [[peer-reviewed]] [[academic journal]] covering the [[history]] of learning and education in a very broad sense. The [[editor-in-chief]] is Dirk van Miert.

==History==
The journal was established in 1974 by a group of [[Dutch people|Dutch]] and [[Belgium|Belgian]] scholars, and subtitled ''Sources and Documents relating to the Early Modern History of Ideas''. The aim was to provide a platform for the edition and study of primary sources of relatively small size pertaining to the cultural and intellectual history of Early Modern Europe.<ref>See J. Roegiers and H. Bots, 'To the Reader', Lias 2009, vol. 36, no. 2</ref> Until 2010, the journal was published by [[Academic Publishers Associated]]. Back-issues up till 2004 are freely available.<ref>[http://webdoc.ubn.kun.nl/tijd/l/lias/ ''Lias'': Online access]</ref> The majority of the articles in the first 36 issues concentrated on texts from, or relating to, the [[Low Countries]].{{cn|date=October 2012}} In 2010, the journal was taken over by a new publisher, [[Peeters Academic Publishing]]. It appears on the "initial list" of history journals in the European Reference Index of the Humanities of the [[European Science Foundation]].<ref>[http://www.esf.org/research-areas/humanities/erih-european-reference-index-for-the-humanities/erih-initial-lists.html European Reference Index of the Humanities]</ref>

==Scope==
The journal covers the periods from Early [[Humanism]] (the fourteenth century) to the late [[Age of Enlightenment|Enlightenment]] (the early nineteenth century). It prints source texts in their original languages without restrictions. However, introduction and footnotes must be written in English, although they may be in French for French sources. The journal also accepts studies based on neglected printed sources.

==References==
{{Reflist}}

==External links==
*{{Official website|1=http://poj.peeters-leuven.be/content.php?url=journal&journal_code=LIAS}}

{{DEFAULTSORT:Lias}}
[[Category:Multilingual journals]]
[[Category:Publications established in 1974]]
[[Category:Biannual journals]]
[[Category:History journals]]