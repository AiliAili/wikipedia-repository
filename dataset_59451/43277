__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Type G
  |image = Morane-Saulnier G racer.jpg
  |caption = 
}}{{Infobox Aircraft Type
  |type = Sport aircraft
  |manufacturer = [[Morane-Saulnier]]
  |designer = 
  |first flight = 1912
  |introduced = 
  |retired = 
  |status =
  |primary user = 
  |more users =  
  |produced = 
  |number built =
  |unit cost = 
  |variants with their own articles = 
}}
|}
The '''Morane-Saulnier G''' was a two-seat sport and racing [[monoplane]] produced in France before the First World War.<ref name="JEA">Taylor 1989, 648</ref><ref name="IEA">"The Illustrated Encyclopedia of Aircraft", 2539</ref> It was a development of the racing monoplanes designed by [[Léon Morane]] and Raymond Saulnier after leaving [[Etablissements Borel|Borel]] and, like its predecessors, was a wire-braced, shoulder-wing monoplane.<ref name="IEA" /> Construction was of fabric-covered wood throughout, except for the undercarriage struts which were of steel tube.<ref name="Flight 564">"The Latest Morane-Saulnier Monoplane", 564</ref>

The type was a sporting success. In April 1913, [[Roland Garros (aviator)|Roland Garros]] took second place in the inaugural [[Schneider Cup]] in a floatplane version,<ref>Hartmann 2001, 10. This machine is often misreported as a [[Morane-Saulnier H]]</ref> finishing with a time of 40 minutes 40 seconds.<ref name="Monaco Meeting">"The Monaco Meeting", 450</ref> On 26 June, [[Claude Grahame-White]] flew another float-equipped example from [[Paris]] to [[London]] via [[Le Havre]], [[Boulogne-sur-Mer]], and [[Dover]],<ref name="GW">"Mr Grahame-Wnite's Seine—Thames Trip"</ref> covering some {{convert|500|km|mi|abbr=on}} that day.<ref name="Hartmann 10">Hartmann 2001, 10</ref> Between 21 and 28 September the same year, two float-equipped Type Gs competed at the seaplane meeting at [[San Sebastián]], with [[John Evans, 10th Lord Carbery|Lord Carbery]] winning the short takeoff prize on one, and [[Edmond Audemars]] winning the maneuverability prize on the other.<ref name="Hartmann 11">Hartmann 2001, 11</ref> The following week, Carbery flew his Type G in the Italian Waterplane Contest from [[Lake Como]] to [[Pavia]] and back, along with two other Type Gs in the field of fifteen competitors, these flown by Garros and Morane.<ref name="Hartmann 12">Hartmann 2001, 12</ref><ref name="Como">"Italian Waterplane Contest", 1129</ref> Garros not only won the Grand Prize in the "general class", but also the prizes for best speed (127.7&nbsp;km/h, 79.8&nbsp;mph) and greatest altitude (2,100 m, 6,000&nbsp;ft).<ref name="Hartmann 12" />

On 28 September 1913 Roland Garros became the first person to cross the [[Mediterranean Sea]] by air, flying from [[Fréjus]] in the south of France to [[Bizerte]] in [[Tunisia]]<ref>{{cite journal | date = 27 September 1913 | title = Flying the Mediterranean | journal = [[Flight International|''Flight'']] | volume = V | issue = 39 | pages = 1078 | url = http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201052.html | accessdate = 24 September 2014}}</ref> in a Morane-Saulnier G.

In 1914, Russian manufacturer [[Duks]] arranged to build the type under licence at their Moscow factory for the Russian Army,<ref name="Hartmann 12" /> and the same year, the Turkish military ordered 40 examples.<ref name="Hartmann 12" /> Before these could be delivered, however, war broke out, and the aircraft were impressed into the French Army.<ref name="Hartmann 12" /> To these, the Army soon added an order of 94 aircraft, and the British Royal Flying Corps also acquired a number, these latter machines purchased from [[Grahame-White]], who was manufacturing the type in the UK under licence.<ref name="IEA" /> At the outbreak of war, the type's military value was found to be wanting, and the French machines were quickly relegated to training duties.<ref name="IEA" />

Despite this, a dedicated single-seat fighter version was built in 1915, armed with an 8&nbsp;mm [[Hotchkiss machine gun]] that fired through the propeller arc, the propeller blades being protected by deflector plates.<ref name="EADS">"Morane-Saulnier type G"</ref> Only one or two prototypes were built, and the type never entered service.<ref name="Green and Swanborough">Green and Swanborough 1994</ref>

Some Type Gs were modified by Morane-Saulnier to have their wings mounted above the fuselage, parasol-fashion, rather than at the fuselage sides. This arrangement was found to offer far better visibility for the pilot, and formed the basis for the [[Morane-Saulnier L]].<ref name="IEA" />

A Type G is preserved at the [[Museo del Aire (Madrid)]] ([[Museo del Aire (Madrid)|Museo del Aire de Cuatrovientos]]).

==Variants==
;Type GA:version with 40&nbsp;kW (60&nbsp;hp) Le Rhône engine
;Type GB:version with 60&nbsp;kW (80&nbsp;hp) Gnome engine
;Type WB:version for export to Russia with glazed forward fuselage
;Thulin B:Licence-built by [[AB Thulinverken]] in [[Sweden]]
;Grahame-White Type XIV:License built by [[Claude Grahame-White]] in the [[United Kingdom]]

== Operators ==
; {{ARG}}
* [[Argentine Air Force]]
; {{CUB}}
* [[Cuban Air Force]] - One aircraft.
; {{flag|France}}
* ''[[Aéronautique Militaire]]'' (94 ordered, plus 40 impounded from Turkish order)
* [[French Navy]]
; {{flag|Russia}}
* [[Imperial Russian Air Force]]
; {{flag|USSR}}
* [[Soviet Air Force]] - Taken over from the Imperial Russian Air Force.
; {{flag|Spain}}
* [[Spanish Air Force]]
; {{flag|Turkey}}
* [[Ottoman Air Force]] - 40 ordered, but never delivered.
; {{flag|United Kingdom}}
* [[Royal Flying Corps]]

==Specifications (GB)==
{{aerospecs
|ref=<!-- reference -->''Jane's Fighting Aircraft of World War I'', 116
|met or eng?=<!-- eng for US/UK aircraft, met for all others. You MUST include one or the other here, or no specifications will show -->met

|crew=one pilot
|capacity=one passenger
|length m=6.30
|length ft=20
|length in=8
|span m=9.20
|span ft=30
|span in=2
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|height m=
|height ft=
|height in=
|wing area sqm=16
|wing area sqft=172
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|aspect ratio=<!-- sailplanes -->
|wing profile=<!-- sailplanes -->
|empty weight kg=95
|empty weight lb=208
|gross weight kg=370
|gross weight lb=815
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->

|eng1 number=1
|eng1 type=Gnome
|eng1 kw=<!-- prop engines -->60
|eng1 hp=<!-- prop engines -->80
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|max speed kmh=123
|max speed mph=76
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=<!-- if max speed unknown -->
|cruise speed mph=<!-- if max speed unknown -->
|stall speed kmh=<!-- aerobatic and STOL aircraft -->
|stall speed mph=<!-- aerobatic and STOL aircraft -->
|range km=
|range miles=
|endurance h=<!-- if range unknown -->
|endurance min=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|g limits=<!-- aerobatic aircraft -->
|roll rate=<!-- aerobatic aircraft -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=1.8
|climb rate ftmin=345
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->

|armament1=
|armament2=
|armament3=
|armament4=
|armament5=
|armament6=
}}
<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Morane-Saulnier G}}
* {{cite book |last=Green |first=William |author2=Gordon Swanborough |title=The Complete Book of Fighters |year=1994 |publisher=Colour Library Direct |location=Godalming, UK |isbn=0-86288-220-6}}
* {{cite web |last=Hartmann |first=Gérard |title=L'incroyable Morane-Saulnier hydro |work=La Coupe Schneider et hydravions anciens/Dossiers historiques hydravions et moteurs |year=2001 |url=http://www.hydroretro.net/etudegh/moranesaulnierhydro.pdf |accessdate=2008-11-07}}
* {{cite book |title=The Illustrated Encyclopedia of Aircraft |publisher=Aerospace Publishing|location=London }}
* {{cite journal |title=Italian Waterplane Contest |journal=[[Flight International|Flight]] |date=11 October 1913 |page=1129 |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%201103.html |accessdate=2008-11-10}}
* {{cite book |title=Jane's Fighting Aircraft of World War I |publisher=Studio Editions |location=London |year=2001 |isbn=0-517-03376-3}}
* {{cite journal |title=The Latest Morane-Saulnier Monoplane |journal=[[Flight International|Flight]] |date=24 May 1913 |pages=561–64 |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200539.html |accessdate=2008-11-07}}
* {{cite journal |title=The Monaco Meeting |journal=[[Flight International|Flight]] |date=19 April 1913 |page=450 |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200430.html |accessdate=2008-11-07}}
* {{cite journal |title=Mr Grahame-Wnite's Seine—Thames Trip |journal=[[Flight International|Flight]] |date=5 July 1913 |page=749 |url=http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200723.html |accessdate=2008-11-07}}
* {{cite web |title=Morane-Saulnier type G |work=EADS website |url=http://www.eads.net/1024/en/eads/history/airhist/1910_1919/morane_saulnier_g_1912.html |accessdate=2008-11-07}} 
* {{cite book |last= Taylor |first= Michael J. H. |title=Jane's Encyclopedia of Aviation |year=1989 |publisher=Studio Editions |location=London |isbn= 0-7106-0710-5 }}
<!-- == External links == -->

{{Morane-Saulnier aircraft}}

[[Category:French sport aircraft 1910–1919]]
[[Category:Morane-Saulnier aircraft|G]]
[[Category:French fighter aircraft 1910–1919]]