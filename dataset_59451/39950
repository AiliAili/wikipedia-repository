{{Infobox military person
|name=Jorma Sarvanto
|birth_date=22 February 1912
|death_date= {{d-da|16 October 1963|22 February 1912}}
|birth_place=[[Turku]]
|death_place=[[Inkeroinen]]
|image=Jorma Kalevi Sarvanto.jpg
|caption=
|nickname=Zamba
|allegiance=
|serviceyears=1937–1960
|rank=[[Lieutenant Colonel|Lt.Col.]]
|branch=Air Force
|commands=
|unit=
|battles=
|awards=
|relations=
|laterwork=
}}
'''Jorma Kalevi Sarvanto''' (22 August 1912 &ndash; 16 October 1963) was a [[Finland|Finnish]] [[Finnish Air Force|Air Force]] pilot and the foremost Finnish fighter ace of the [[Winter War]].

==Early life==
Sarvanto was born and raised in Turku, Finland.  He attended high school in Turku and graduated in 1933.<ref>Sarvanto, Jorma: ''Havittajalentajana Karjalan Taivaalla'', 2nd Edition, 1989, front cover flap</ref>  He was first admitted to the Pori Infantry Regiment, but decided to apply when the Finnish Air Force sent out a notice that they would select officer trainees. He was admitted and sent to the Reserve Officer Pilot Course number 4 at the Kauhava Air Base. He knew he had come to the right place. He completed his reserve officer training in 1934 and was unemployed for a while due to the [[Great Depression]]. He decided to apply to the Cadet School, Air Warfare Section where he wanted to become a cadre (professional) officer, and he was admitted in autumn 1934. In May 1937 his training was completed.

Sarvanto was at first sent to ''Lentoasema'' 1 (Air Base One) at Utti and later to [[Lentorykmentti 4|''Lentorykmentti'' 4]] (Flight Regiment 4), which was a bomber squadron. He had wished to fly fighters and requested a transfer to Fighter Squadron 24, which was granted. He started to fly Fokker D.XXIs there in 1937. He excelled in the firing tests, with a hit average of 92%.<ref>Sarvanto, Jorma: ''Stridsflygare under Karelens himmel'', pp. 157-159</ref>

==War service==
[[File:Fokker D.XXI.jpg|thumb|280px|Fokker D XXI planes in the Finnish Air Force during World War II.]]
[[File:Ilyushin DB-3M.jpg|thumb|280px|An Ilyushin DB-3M in Finnish markings]]
[[File:Sarvanto fin.jpg|thumb|280px|Sarvanto holding a piece of a rudder from one of the downed aircraft.]]
On 30 November 1939 the [[Winter War]] erupted and Finland was at war with the [[Soviet Union]]. He saw his first battle on 19 December and his first two victories came on 23 December 1939.

On 6 January 1940 Sarvanto took part in an airfight with a group of 8 Soviet [[Ilyushin DB-3]]s. One of the Soviet bombers was shot down by another Finnish pilot, lieutenant Per-Erik Sovelius, who had first encountered them while on patrol and who then radioed their position to the Utti Air Force Base, from where Sarvanto and others started to join the fight. In the ensuing encounter, Sarvanto managed to shoot down six of the enemy aircraft in quick sequence.<ref>Keskinen, Kalevi & Stenman, Kari: ''Ilmavoitot Osa 2/Aerial Victories Part 2'', p. 48 - 49, 54.</ref>

This incident drew a lot of attention worldwide, and the press considered it a world record. Most of the major Western newspapers published a photo of lieutenant Sarvanto holding a large sheet of aluminum with a big "5" on it, a trophy from one of the victims.<ref>Appel, Erik: ''Finland i krig'', p. 201</ref>

Sarvanto was to become the top scoring Finnish ace of the Winter War with 13 victories. During the [[Continuation War]] he downed four more aircraft with [[Brewster Buffalo]]es, bringing his total score to 17. He flew a total of 255 combat missions during World War II.<ref>Stenman, Kari: ''Hävittäjä-Ässät''</ref>

In 1941 he was appointed captain and he was given different staff positions, e.g. as a liaison officer with the German ''Luftflotte'' 1, as the commander of [[LeLv 24]]'s 2nd division and later as the commander of [[TLeLv 35]].<ref>Sarvanto, Jorma: ''Stridsflygare under Karelens himmel'', pp. 160-162</ref>

==After the war==
Sarvanto would continue his military career, which led him to become the commander of the Flight School in Kauhava. In 1954 he became Finnish [[military attaché]] in London, a position he held for 3½ years before returning to his position at the Flight School. Sarvanto resigned from the air force in 1960, with the rank of lieutenant colonel.

Sarvanto had married before the war and had four children. He would work as the CEO of a bank until his death on 16 October 1963.

Sarvanto's grandsons include [[United States|American]] champion [[bagpipes|bagpipe player]] [[Jori Chisholm]] and American political consultant [[Kari Chisholm]].  One of his granddaughters is married to General [[:fi:Kim Jäämeri|Kim Jäämeri]], commander of the Finnish Air Force.

==Aerial victories==
[[File:Tupolev SB 2.jpg|thumb|280px|A Tupolev SB]]
{|style="font-size: 0.90em;"
|-
!Number!!Date!!Place!!Own aircraft!!Enemy aircraft!!Enemy regiment
|-
|1||Dec 23, 1939||Noskuanselkä||FR-97||[[Tupolev SB|SB]]||44.SBAP
|-
|2||Dec 23, 1939||Noisniemi||FR-97||[[Tupolev SB|SB]]||44.SBAP
|-
|3||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|4||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|5||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|6||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|7||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|8||Jan 6, 1940||Utti-Tavastila||FR-97||[[Ilyushin DB-3|DB-3]]||6.DBAP
|-
|9||Jan 17, 1940||Heinjoki||FR-99||[[Tupolev SB|SB]]||54.SBAP
|-
|10||Feb 3, 1940||Nuijamaa||FR-80||[[Ilyushin DB-3|DB-3]]||42.DBAP
|-
|11||Feb 15, 1940||Vyborg||FR-80||[[Ilyushin DB-3|DB-3]]||42.DBAP
|-
|12||Feb 18, 1940||Simola||FR-100||[[Ilyushin DB-3|DB-3]]||1.AP KPF
|-
|13||Feb 19, 1940||Vyborg||FR-100||[[Ilyushin DB-3|DB-3]]||21.DBAP
|-
|14||Jun 25, 1941||Utti||BW-357||[[Tupolev SB|SB]]||201.SBAP
|-
|15||Jun 29, 1941||Utti||BW-357||[[Petlyakov Pe-2|Pe-2]]||58.SBAP
|-
|16||Apr 21, 1943||Gulf of Finland||BW-373||[[Lavochkin La-5|La-5]]||4.GIAP KBF
|-
|17||May 9, 1943||Gulf of Finland||BW-357||[[Yakovlev Yak-7|Yak-7]]||?
|-
|}
<ref>Sarvanto, Jorma: ''Stridsflygare under Karelens himmel'', p. 202</ref>

==Honors==
*[[Order of the Cross of Liberty|Cross of Liberty, 2nd Class, with swords, of the Order of the Cross of Liberty]]
*[[Order of the Cross of Liberty|Cross of Liberty, 3rd Class, with swords, of the Order of the Cross of Liberty]]
*[[Order of the White Rose|Commander of the Order of the White Rose of Finland]]
*[[Order of the German Eagle|Order of the German Eagle 3rd Class, with swords]]
*[[Luftwaffe's pilot badge]] ''honoris causa''

==External links==
*[http://www.sci.fi/~fta/finace26.htm Jorma Sarvanto - the Top Finnish Ace of  the Winter War]

==References==
{{Reflist|2}}
----
* {{cite book |last= Appel |first= Erik|title= Finland i krig 1939-1940 - första delen |year= 2001 |publisher= Schildts förlag Ab |location= Espoo, Finland |language= Swedish |pages= 261 |isbn = 951-50-1182-5|display-authors=etal}}
* {{cite book |last= Sarvanto |first= Jorma |title= Stridsflygare under Karelens himmel |year= 2005 |publisher= Luleå Grafiska |location= Luleå, Sweden |language= Swedish |pages= 213 |isbn = 91-975315-4-5}}
* {{cite book |last= Stenman |first= Kari and Keskinen, Kalevi |title= Aircraft of the Aces 23 - Finnish Aces of World War 2 |year= 1998 |publisher= Osprey Publishing |location=  |pages= |isbn = 1-85532-783-X}}
* {{cite book |last= Stenman |first= Kari, Keskinen, Kalevi and Niska, Klaus|title= Hävittäjä-Ässät - Suomen Ilmavoimien Historia 11 |year= 1994 |publisher= Apali |location=  |language= Finnish, English |pages= |isbn = 952-5026-00-0}}
* {{cite book |last= Keskinen |first= Kalevi and Stenman, Kari|title= Ilmavoitot Osa 2/Aerial Victories Part 2 - Suomen Ilmavoimien Historia 27 |year= 2006 |publisher= Kari Stenman |location=  |language= Finnish, English |pages= |isbn = 952-99432-9-6}}

{{DEFAULTSORT:Sarvanto, Jorma}}
[[Category:1912 births]]
[[Category:1963 deaths]]
[[Category:Finnish Air Force personnel]]
[[Category:Finnish World War II flying aces]]
[[Category:Recipients of the Order of the Cross of Liberty, 2nd Class]]
[[Category:Recipients of the Order of the Cross of Liberty, 3rd Class]]