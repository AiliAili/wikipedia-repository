{{Infobox journal
| title = Business Ethics Quarterly
| cover =[[File:Business_Ethics_Quarterly_(BEQ)_journal_cover.jpg]]
| editor = Denis G. Arnold
| discipline = [[Business ethics]]
| abbreviation = Bus. Ethics Q.
| publisher = [[Cambridge University Press]]
| country = United States
| frequency = Quarterly
| history = 1991–present
| impact = 1.735
| impact-year = 2015
| website = http://journals.cambridge.org/BEQ
| link2 = http://journals.cambridge.org/BEQ-alerts
| link2-name = Online access
| link3 =
| link3-name =
| OCLC = 38554432
| JSTOR = 1052150X
| LCCN = 93-648250
| CODEN =
| ISSN = 1052-150X
| eISSN = 2153-3326
}}
'''''Business Ethics Quarterly''''' is a [[Peer review|peer-reviewed]] [[academic journal]] that publishes theoretical and [[empirical research]] relevant to all aspects of [[business ethics]]. It publishes articles and reviews on a broad range of topics, including the internal ethics of business organizations, the role of business organizations in larger social, [[politics|political]], and cultural frameworks, and the ethical quality of [[free market|market]]-based societies and market-based relationships. ''Business Ethics Quarterly'' is an official journal of the [[Society for Business Ethics]] and is published on a [[non-profit]] basis by the [[Cambridge University Press]]. The [[editor-in-chief]] is [[Denis G. Arnold]] ([[University of North Carolina at Charlotte]]).

== Abstracting and indexing ==
The journal is abstracted and indexed by:
{{Div col|2}}
* [[ABI/INFORM]]
* [[ATLA Religion Database]]
* [[Business ASAP]]
* [[Business Source]]
* [[Business & Corporate Resource Center]]
* [[Business Periodicals Index]]
* [[Corporate ResourceNet]]
* [[Dow Jones Insight]]
* [[Emerald Reviews]]
* [[Index Philosophicus]]
* [[International Bibliography of the Social Sciences]]
* [[MEDLINE]]
* [[PAIS International]]
* [[Philosopher's Index]]
* [[Philosophy Research Index]]
* [[PhilPapers]]
* [[ProQuest]] Social Science Journals
* [[Public Affairs Index]]
* [[Scopus]]
* [[Social Sciences Citation Index]]
* [[Wilson Business Abstracts]]
{{Div col end}}

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.735, ranking it 7th out of 51 journals in the category "Ethics".<ref name=WoS>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Ethics |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of ethics journals]]

== References ==
{{Reflist}}

== External links ==
* {{Official website|http://journals.cambridge.org/BEQ}}
* [http://www.societyforbusinessethics.org/ Society for Business Ethics]


[[Category:Cambridge University Press academic journals]]
[[Category:Business and management journals]]
[[Category:Business ethics]]
[[Category:English-language journals]]
[[Category:Publications established in 1991]]
[[Category:Quarterly journals]]
[[Category:Ethics journals]]