{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
  |name = Fairey Delta 1
  |image =Fairey Delta FD1.jpg
  |caption = Fairey Delta 1
}}{{Infobox Aircraft Type
  |type = research aircraft
  |national origin=United Kingdom
  |manufacturer = [[Fairey Aviation Company]]
  |designer = 
  |first flight = 12 March 1951
  |introduced = 
  |retired = 1956
  |status = 
  |primary user = <!--please list only one-->
  |more users = <!--up to three more. please separate with <br/>.-->
  |produced = <!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
  |number built = 1
  |unit cost = 
  |variants with their own articles = 
}}
|}

The '''Fairey Delta 1''' or '''FD1''' was a [[United Kingdom|British]] research aircraft produced by the [[Fairey Aviation Company]]  for investigation of [[delta wing]] flight characteristics and control at [[transonic]] speeds. In testing, the FD1 exhibited unfavourable handling and stability leading to cancellation of two further airframes.  It was the first British-designed delta wing.

==Design and development==
Originally conceived as a [[vertical takeoff]] (VTO) fighter, the proposed fighter was intended to be launched from an inclined ramp. Already in the early design stage at Fairey, the [[Ministry of Supply]] (MoS) decided to have the aircraft built as a more conventional jet-powered research vehicle to [[List of Air Ministry Specifications|Ministry specification]] E.10/47. The Fairey Type R design was a mid-wing tail-less [[Delta wing|delta]] [[monoplane]], with a circular cross-section [[fuselage]] and engine air-inlet at the extreme front. The engine was a [[Rolls-Royce Limited|Rolls-Royce]] [[Rolls-Royce Derwent|Derwent 8]] centrifugal turbojet. Although designed as a [[transonic]] aircraft, the Delta 1 had a short-coupled, "portly" appearance, completely at odds with Fairey's next design, the sleek and elegant [[Fairey Delta 2|Delta 2]]. Three aircraft were ordered with the name "Fairey Delta" applied to the project; subsequently, the name was changed to Fairey Delta 1.<ref name= "Winchester p 254">Winchester 2005, p. 254.</ref>

==Testing==
[[File:Fairey FD1 VX350 RWY 05.50 edited-2.jpg|thumb|right|The Fairey Delta One at [[Manchester Airport|Manchester (Ringway) Airport]] in May 1950 assembled for ground taxying trials and already fitted with the additional delta-shaped control surface at the tip of its tail fin.]]
The only FD1 to be completed was built at Fairey's [[Heaton Chapel]] [[Stockport]] factory and taken by road to their test facility at Manchester's [[Ringway Airport]] for final assembly. Starting on 12 May 1950 it made several high-speed taxi runs down the 4,200-foot main runway there before being partially dismantled and transported by road to the [[Aeroplane and Armament Experimental Establishment]] (AAEE) at [[RAF Boscombe Down]]. After further taxi tests, the aircraft ([[United Kingdom military aircraft serials|serial number]] ''VX350'') made its maiden flight on 12 March 1951, flown by Fairey test pilot [[Group Captain]] Gordon Slade.<ref>Flight p348</ref> The FD1 was fitted with a small horizontal delta-shaped control surface on the top of the tail-fin which was intended to stop the "serious pitching as it gathered speed." <ref name= "Winchester p 254"/> Continued testing pointed to serious stability problems that were characterized as "dangerous." <ref name= "Winchester p 254"/>  The additional tail surface limited the top speed to a relatively pedestrian 345&nbsp;mph (555&nbsp;km/h).
 
After a landing accident in September 1951, the FD1 was modified with the removal of the temporary [[leading edge slats|slots]] as well as the removal of the streamlined housings for the anti-spin parachutes that were mounted at the wingtips. The large [[Flight control surfaces|control surfaces]] made the FD 1 difficult to control or even fly with precision although a rapid roll rate was achieved. Only one FD1 was built, with the second (''VX357'') and third (''VX364'') airframes being cancelled before they entered production.<ref name= "Winchester p 254"/>

==Final years==
Following the cancellation of the programme by the Air Ministry the aircraft continued to be flown on trials work by Fairey.<ref name="Chorlton">Chorlton 2012, p. 104</ref> On 6 February 1956 it was damaged beyond repair in a landing accident at Boscombe Down.<ref name="Chorlton" /> The aircraft was moved in October 1956 for use as a target on the [[Shoeburyness]] weapons range and it was later scrapped.<ref name="Chorlton" /> Fairey had spent £382,000 of their own money on the FD1.<ref>{{citation |journal=Flight |url=http://www.flightglobal.com/FlightPDFArchive/1955/1955%20-%201707.PDF |title=A Fairey 'Forty Years' AGM |date=2 December 1955 |page=826}}</ref>

==Specifications (Fairey Delta 1)==
{{aircraft specifications|
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane
|jet or prop?=jet
<!-- Now, fill out the specs. Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For additional lines, end your alt units with )</li> and start a new, fully-formatted line with <li> -->
|ref=''Jane's''<ref>Taylor 1976, p. 91.</ref>
|crew=1
|length main=26 ft 3 in
|length alt= 8 m
|span main=19 ft 6.5 in 
|span alt=5.9 m
|height main= 19 ft 6.5 in
|height alt=5.9 m
|area main=
|area alt=
|empty weight main=
|empty weight alt=
|loaded weight main=6,800 lb
|loaded weight alt=3,084 kg
|max takeoff weight main=
|max takeoff weight alt=
|engine (jet)=[[Rolls-Royce Derwent|Rolls-Royce Derwent 8]]
|type of jet=[[turbojet]]
|number of jets=1
|thrust main=3,600 lb
|thrust alt=1,633 kg 
|max speed main=545 knots
|max speed alt=1,011 km/h, 628 mph
|range main= 
|range alt= 
|ceiling main= 
|ceiling alt= 
|climb rate main= 
|climb rate alt= 
|loading main=
|loading alt=
|thrust/weight=
}}

==See also==
{{aircontent|
|related=
*[[Fairey Delta 2]]
|similar aircraft=
*[[Avro 707|Avro 707C]]
*[[Boulton Paul P.111|Boulton Paul P.111A]]
*[[Convair XF-92|Convair XF-92A]]
|lists=
*[[List of experimental aircraft]]
}}

==References==
{{commons category|Fairey Delta 1}}
;Notes
{{reflist}}

;Bibliography
{{refbegin}}
* Chorlton, Martyn (ed). ''Fairey - Company Profile 1915-1960''. Cudham, Kent, England:Kelysey Publishing, 2012. ISBN 978-1-907426-60-5
* Sturtivant, Ray. ''British Research and Development Aircraft''. Somerset, UK: Haynes Publishing Group, 1990. ISBN 0-85429-697-2
* Taylor, H. A. ''Fairey Aircraft since 1915''. London: Putnam, 1974. ISBN 0-370-00065-X.
* Taylor, John W.R. ''Jane's Pocket Book of Research and Experimental Aircraft''. London: Macdonald and Jane's Publishers Ltd, 1976. ISBN 0-356-08409-4. 
* Twiss, Peter. ''Faster than the Sun''. London: Grub Street Publishing, 2000. ISBN 1-902304-43-8.
* Winchester, Jim. ''X-Planes and Prototypes''. London: Amber Books Ltd., 2005. ISBN 1-904687-40-7.
*{{citation |url=http://www.flightglobal.com/pdfarchive/view/1951/1951%20-%200564.html |title=From Dunne to Delta |journal=Flight |date=23 March 1951  |page=348}}
{{refend}}

{{Fairey aircraft}}

[[Category:British experimental aircraft 1950–1959]]
[[Category:Delta-wing aircraft]]
[[Category:Fairey aircraft|Delta 1]]