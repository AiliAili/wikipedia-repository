{{multiple issues|
{{cleanup reorganize|date=July 2014}}
{{external links|date=July 2014}}
}}
{{EngvarB|date=December 2014}}
{{Use dmy dates|date=June 2013}}
{{Infobox school
|name              = Pope John Senior High School and Minor Seminary
|image             = Pope John Senior High School and Minor Seminary logo.png
|imagesize         = 
|caption           = Pojoss Insignia
|location          = [[Koforidua]], [[Ghana]]
|streetaddress     = P. O. Box 370
|region            = Eastern Region
|city              = [[Koforidua]]
|state             = 
|district          = New Juabeng Municipal Assembly
|province          = Eastern Region
|county            = Ghana
|postcode          = 23321
|postalcode        = 
|zipcode           = 00233
|country           = [[Ghana]]
|coordinates       = 
|schoolnumber      = 
|schoolboard       = Board of Governors
|authority         = 
|religion          = Christianity
|denomination      = [[Roman Catholic]]
|oversight         = Ghana Education Service
|affiliation       = [[Roman Catholic Church]], [[Ghana]]
|authorizer        = 
|superintendent    = 
|trustee           = 
|founder           = Bishop [[Joseph Oliver Bowers]] [[Societas Verbi Divini|SVD]]
|specialist        = 
|session           = 
|president         = 
|head of school    = 
|headteacher       = 
|head_label        = Headmistress 
|head              = Mrs. Benedicta Foli
|chairperson       = 
|principal         = 
|viceprincipal     = 
|dean              = 
|faculty           = 
|administrator     = 
|rector            = Monsignor Francis Twum-Barimah
|chaplain          = Rev.Fr. Joel Kwame
|director          = 
|custodian         = 
|staff             = 93 teachers
|ranking           = Grade A
|bar pass rate     = 
|roll              = 
|MOE               = 
|ceeb              = 
|school code       = 
|LEA               = 
|ofsted            = 
|testaverage       = 
|national_ranking  = 1st ([[WAEC]] 2015)
|classrooms        = 
|class             = 
|classes offered   = 
|avg_class_size    = 50
|ratio             = 
|SAT               = 
|ACT               = 
|graduates         = 
|gender            = Boys
|lower_age         = 12
|upper_age         = 18
|houses            = 7
|schooltype        = Boys Boarding/ Residential
|fundingtype       = Government funded
|system            = 
|fees              = 
|tuition           = 
|revenue           = 
|endowment         = 
|budget            = 
|enrollment        = c. 1500
|products          = 
|age range         = 14-18
|grades            = Forms' (1–3)
|medium            = 
|language          = English
|campus            = Effiduase
|campus size       = 
|campus type       = 
|Hours_in_Day      = 
|athletics         = Track and Field
|conference        = 
|slogan            = Pojoba Daasebre!!
|song              = O Great Pojomma Arise and Shine!
|fightsong         = Sacred Heart of Jesus
|motto             = '''Vela Damus'''
|accreditation     = 
|rival             = 
|mascot image      = 
|sports            = 
|patron            = Sacred Heart of [[Jesus]] and [[John the Evangelist|St. John]]
|mascot            = Boat
|nickname          = [[POJOSS]]
|school_colours    = Yellow {{Color box|yellow|border=darkgray}} and blue-black {{Color box|blue|border=darkgray}}
|yearbook          = 
|publication       = POJOMAG
|newspaper         = The Gong
|opened            = 
|established       = 21 January 1958
|founded           = 
|status            = 
|closed            = 
|students          = 
|pupils            = 
|alumni            =Pope John Old Boys Association(POJOBA)
|nobel_laureates   = 
|gradeK            = 
|grade1            = 
|grade2            = 
|grade3            = 
|grade4            = 
|grade5            = 
|grade6            = 
|grade7            = 
|grade8            = 
|grade9            = 
|grade10           = 
|grade11           = 
|grade12           = 
|grade13           = 
|other_grade_label = 
|other             = 
|communities       = 
|feeders           = 
|free_label        = Address
|free_text         = P. O. Box 370, Effiduase-K'Dua, [[Accra]], [[Ghana]] {{flagicon|Ghana}}
|free_label1       = 
|free_text1        = 
|free_label2       = 
|free_text2        = 
|free_label3       = 
|free_text3        = 
|free_label4       =
|free_text4        =  
|free_label5       = 
|free_text5        = 
|footnotes         = 
|picture           = 
|homepage          =[http://pojoss.edu.gh/ pojoss.edu.gh]
}}
'''Pope John Senior High School and Minor Seminary''' (formerly '''St John’s Seminary and College''', nicknamed '''POJOSS'''), is an all-boys [[boarding school]], located at Effiduase, Koforidua, in the Eastern Region of [[Ghana]]. It was established in 1958 by Bishop [[Joseph Oliver Bowers]] [[Societas Verbi Divini|SVD]] as a [[Catholic]] [[seminary]] for boys who wished to become priests.
Students are offered courses such as general arts, general science, business and visual arts to pursue and after their stay in the school, sit an external examination called the [[West African Senior School Certificate Examination]] to be placed in any of the tertiary institutions in the country.

The students of the school are usually referred to as ''Pojomma'' while the alumni are addressed as  Pojoba,<ref>{{cite news|title=CDS Donates 10000 cedis to alma mater|url=http://www.ghanaweb.com/GhanaHomePage/NewsArchive/artikel.php?ID=153334}}</ref><ref>{{cite news|title=KNUST branch of POJOBA donate to alma mater|url=http://www.modernghana.com/news/175245/1/knust-branch-of-pojoba-donate-computers-to-their-a.html|newspaper=Modern Ghana}}</ref> to which they respond ''Daasebre''. Since its establishment in 1958, the school has achieved many successes and has produced great men for [[Ghana]] and the world over. There are 2100 boys in the school.

== History ==

[[File:BISHOP JOSEPH OLIVER BOWERS SVD.JPG|thumbnail|right|Bishop [[Joseph Oliver Bowers]]]]
On 8 November 1953, Bishop [[Joseph Oliver Bowers]] [[Societas Verbi Divini|SVD]],<ref>{{cite news|title=Bishop Bowers dies at 103|url=http://edition.myjoyonline.com/pages/news/201211/96822.php|newspaper=Joy News}}</ref> Catholic Bishop of the then Diocese of Accra, now [[Roman Catholic Archdiocese of Accra]] came to the New Juabeng Traditional Area in Eastern Region, Ghana on his first pastoral visit. He held discussions with Nana Frempong Mposo II, chief of the area, which led to the [[Roman Catholic Church]] mission acquiring land at Effiduase in Koforidua. Father Anthony Bauer and Fr. Henk Janseen were then asked to survey the land and their report was favourable. Early in 1955, Bishop J. O. Bowers decided to build a junior seminary for the Diocese of Accra on that land. In January 1955, Fr. Jude SVD, Dr. Balduricus and Dr. Lucian Orians came to construct the first buildings, one classroom block and a combined Fathers' residence and administrative block. In early 1957, Dr. Damian Brockmann SVD constructed the first Science Block. Today in its place stands a students' dormitory called Elsbend House, named after the first headmaster. In October 1957, Bishop Bowers appointed Rev. Fr. Alphonse Elsbend<ref>{{cite web|last=Section|first=Fr. Alphonse Elsbend|title=SVD IN GHANA|url=http://www.svdghana.org/svd-in-ghana.html|work=Svenska Dagbladet}}</ref>  as the first Headmaster and Seminary Rector, assisted by Rev. John O'Sullivan and Rev. Joseph Skorupka and Bismark Sosu. The school's chapel was constructed the following year in 1958.

=== Opening ===

On 21 January 1958, St. John's Seminary and College officially opened with 45 students; 14 seminarians and 31 day students in two forms. One Ghanaian lay teacher, Mr. Paul Ohene-Boakye was employed to help the 3 SVD priests who had been appointed by the Bishop to teach and instruct the young boys in their academic work, moral and religious lives . In June 1958, electricity was extended to the school at the cost of 45 pounds. On 20 July 1961, the first Speech and Prize-Giving Day was held. The Guest Speaker was Rev. Maurice Lesage SVD. M.SC., then headmaster of [[St. Thomas Aquinas Senior High School]] in Accra, and the distribution of prizes was done by Nana Frempong Mposo II, chief of Effiduase. The school's enrollment at this time was 23 Seminarians and 90 day students, totaling 113 students. Pius Kpeglo (now a catholic retired Monsignor), Senior Prefect of the seminary and school, was successful in his G.C.E. examinations, and left on Scholarship on 7 August 1961 to do Philosophy and Theology at the Diocesan Seminary in Regensburg, Germany.

=== Change of name and absorption into the public education system ===

From 1958 to 1968, St. John's College operated as a private Catholic institution financed by the Catholic Diocese of Accra and by the school fees of the few students. Due to the high cost of running the school, Father Fredrischs, the second Headmaster, with the approval of Bishop Bowers, applied for incorporation into the Public Education System of the Ministry of Education of Ghana. On 1 September 1968, St. John Seminary and College was absorbed into the Ghana Education Service of the [[Ministry of Education (Ghana)]] as a government assisted secondary school. The name of the school was changed to [[Pope John Secondary School and Junior Seminary]] in order to avoid confusion with other Catholic Schools in Ghana already designated "St. John". The name Pope John was chosen in memory of [[Pope John XXIII]] who had just convened the [[Second Vatican Council]], and to keep the identity and purpose of the former St. John's College. The 2007 Ghana education reform under the [[John Kufuor]] administration saw the school re-designated Pope John Senior High School and Minor Seminary.
[[File:Pope John XXIII - 1959.jpg|thumbnail|right|Pope Saint John XXIII]]

=== Growth and development ===

The mustard seed which the [[Society of the Divine Word]] (SVD) Fathers of the Catholic Church sowed under the supervision of Bishop Bowers<ref>{{cite news|title=Former Roman Catholic Bishop of Dominican descent dies in Africa.|url=http://www.caribbean360.com/index.php/news/dominica_news/631997.html#axzz2W4rzhblo|newspaper=Caribbean 360}}</ref>  has hence seen remarkable growth and development.<ref>{{cite web|title=SVD IN GHANA|url=http://www.svdghana.org/svd-in-ghana.html}}</ref>
In July 1992 the [[Roman Catholic Diocese of Koforidua]] was erected, giving it autonomy from the now Metropolitan [[Roman Catholic Archdiocese of Accra]].<ref>{{cite web|last=Archdiocese of Accra|first=Catholic|title=Accra Archdiocese History|url=http://accracatholic.org/index.php?option=com_content&view=category&layout=blog&id=16&Itemid=22}}</ref> Most. Rev. Dr. [[Charles G. Palmer-Buckle]] became the first bishop of the new diocese.<ref>{{cite web|last=Bishop Buckle|first=Former Bishop of Koforidua|title=Archbishop Buckle of Accra|url=http://cbcgha.org/cbc/index.php?option=com_content&view=article&id=290:most-rev-charles-g-palmer-buckle&catid=41:bishops&Itemid=444}}</ref> The management of Pope John Senior High School and Minor Seminary thus became the responsibility of the [[Roman Catholic Diocese of Koforidua]] within the Ecclesiastical Province of Accra. Today, there is a professional teaching staff of 92 and a non-teaching staff of 85. There are over 2000 boarding students with a little under 100 seminarians. Pope John Secondary School now ranks as one of the best in Ghana, making its mark on all fronts; academics, sport, music, discipline, among others.

More than 8,000 students have passed through the classrooms and examination halls of Pope John Senior High School and Minor Seminary. Over 100 of its former students who have been ordained as priests for the Catholic Church,<ref>{{cite news|title=Emmanuel Obeng Cudjoe ordained|url=http://www.minibasilica.com/index.php/component/content/article/14-parish-news/37-ordination-of-rev-fr-emmanuel-obeng-cudjoe}}</ref><ref>{{cite web|title=Rev. Maxwell Wullar|url=http://www.svdghana.org/rev-maxwell-wullar.html|publisher=SVD Ghana}}</ref><ref>{{cite news|title=New Vicar General for Accra Archdiocese|url=http://edition.myjoyonline.com/pages/news/200702/1929.php|accessdate=25 August 2007|newspaper=Joy News}}</ref> including Archbishop [[Charles G. Palmer-Buckle]]. Others have become pastors for other churches and many more can be found in all spheres of life both on the local and international job market, contributing in diverse ways to the development of humanity.

=== School anthem ===
{{Wikisource|Index:Pope John Anthem.pdf|O Great Pojomma Arise and Shine}}
[[File:SACRED HEART POJOSS.jpg|thumbnail|right|The Sacred Heart of Jesus monument]] After years of existence without any official school anthem, Rev. Fr. Burke [[Societas Verbi Divini|SVD]] (headmaster from 1985-1995) began the process for the school acquiring one. This was also partly influenced by the fact that though a young institution, Pojoss had already begun to claim many laurels and perform excellently on the educational scene of [[Ghana]]. In 1997, he asked a student, who was also a member of the school choir to compose an anthem for the school. The student was Master Anthony Barnieh. Anthony's composition ("O Great Pojomma Arise and Shine") was submitted and was immediately adopted as the official school anthem. It was first performed by the school's choir in 1998. Today Mr. Barnieh is a teacher of social sciences in Pope John and the music director for the school choir and all musical groups in the school. Additionally, he is the founder and director of one of the best choirs in Ghana.

Early in the first decade of the twenty-first century, the monument of the Sacred Heart was dedicated to the Sacred Heart of Jesus under the chaplaincy of Rev. Fr. Justine Mensah and Master Emmanuel Obeng Codjoe (now a [[Catholic]] priest). The need for a song to the Sacred Heart arose and this was the ode chosen to pay respect to [[Jesus Christ]]. History however indicates that it was composed and used as an anthem by the school long before the introduction of "O Great Pojomma Arise and Shine".
The two songs of Pope John have become associated with all meetings and gatherings concerning the school or its alumni (notable among which are the opening and closing masses), and have been incorporated into a hymnal for students. Other songs related to students’ life on campus include the famous Father Burke hits: "Life in this world is a great struggle", "I danced in the morning" and "We are going".

== School attire ==

[[File:POJOMMA.jpg|thumbnail|right|[[POJOSS]] students in their ''reflector''.]]
Christened the "Reflector", the school's uniform is a yellow shirt and blue pair of trousers or shorts which may be worn with the school's tie and blazer. The yellow shirt are mostly worn over a pair of shorts. Only form 3 or final year students are allowed to wear a pair of trousers. Other uniforms include the school cloth and the Friday African print wear. House jerseys are worn for sports and every house has its own color.

== Culture ==

Pope John Senior High School maintains a very strong Roman Catholic culture which underpins almost all activities.It is by convention that appointment of the head of the school and key student leaders such as the senior prefect, student's chaplain and their assistants belong to the Catholic faith.
Founders' Day [[Mass]] is celebrated annually all over the country and in the school. There are administrative student units independent of each other. These assist the school's administration in governing the student body and maintaining discipline. They are; The Students' Representative Council (SRC), The Prefectorial Board, The Editorial Board, The Entertainment Board, The Chaplaincy Board and The School Cadet.

== Academic departments ==

Academic activities of the school are structured into departments are headed by heads-of-department who are directly responsible to the assistant headmaster academics. They are the departments of General Science, General Arts, Business, and Visual Arts.

== Curriculum ==

POJOSS follows the Ghanaian educational Senior High School curriculum,<ref>{{cite web|title=Education in Ghana|url=http://www.bibl.u-szeged.hu/oseas_adsec/ghana.htm}}</ref> operating in a three-year academic cycle, from form one to form three. Students are taught throughout the year, over a period of three academic terms. The first term of the academic year marks the enrollment of form one students to the school while the third term marks the graduation of form three students. The programmes run by the school include [[General Science]], [[General Arts]], [[Business]] and [[Visual Arts]]. Form one applicants select four Elective courses. Unlike Elective courses, core courses are offered to all students, irrespective of their programme of study. The core courses in the school are: English language, core mathematics, social studies, integrated science, ICT and physical education, however, students are only examined externally, in the first five aforementioned courses. There are subjects taught which are unique to the school because of its attachment to a seminary. They are [[Religion]], [[Doctrine]] studies, [[Latin]] and [[Music]].

== Facilities ==
[[File:POJOSS NYT.jpg|thumbnail|right|Evening view of the Science Center ([[POJOSS]]).]]
Pope John Senior High School and Minor Seminary has an ultramodern Science laboratory, a state of the arts I.C.T center, a library, a multipurpose athletic field, a basketball and volleyball court, among a host of others.

The school's Parent Teacher Association (PTA),<ref>{{cite web|title=PTA Hands over dormitory to school|url=http://www.modernghana.com/news/67520/1/pojoss-pta-hands-over-dormitory-to-school.html|publisher=Modern Ghana}}</ref> the SRC, [[Roman Catholic Church]] and the [[POJOBA]] have contributed to the development of these facilities in the school. The school's administration makes sure that these facilities are effectively utilized to enhance teaching and learning in the school.{{clear}}

== Chaplaincy ==
[[File:CHAPEL POJOSS.jpg|thumbnail|POJOSS Chapel, constructed in 1958.]]
Pope John Senior High School and Minor Seminary has a vibrant chaplaincy<ref>{{cite news|last=Final year students of Pope John|first=Pope John Chaplaincy Donations|title=Pope John Donations|url=http://gains.org.gh/partner-news/pope-john-students-donate-aids-centre|newspaper=Ghana News Agency (GNA)|date=5 April 2012}}</ref>  headed by a chaplain who is appointed by the [[Archbishop of Accra]]. Among other things, the POJOSS chaplaincy is the headquarters for religious groups like the Catholic Students Union (CASU), [[Legion of Mary]], [[Eastern Region, Ghana|Eastern Region]] and the [[Sacred Heart]] Confraternity in the Eastern Region.

It therefore sees to the organization of the [[St. Thomas Aquinas]] Day celebrations and Kwahu-Tafo pilgrimage held annually in the Eastern Region. The chaplaincy is currently headed by Rev. Fr. Joel Yao Kwame of the [[Roman Catholic Archdiocese of Accra]].{{clear}}

== Annual events ==

Throughout the school's academic season, events are held to enhance the formation of students in addition to academic work. They are as follows:

=== Founders' Day celebrations ===

Each year on 21 August,a Holy Mass is held in memory of the founding fathers of the school. It is celebrated by the Alumni of the school at the Holy Spirit Cathedral in the [[Roman Catholic Archdiocese of Accra]], and by the students at school. [[Holy Mass]] precedes the celebrations and in Accra, is said by all alumni-priests of the school.

=== Homecoming Reunion ===

[[File:ALUMNI POJOSS.jpg|thumbnail|right|Alumni of Pope John sing in the school choir at an outdoor mass during a Home-coming event.]]
This event brings together all alumni of Pojoss at the school for fun games. During this event, old boys deliver motivational talks to the students.<ref>{{cite web|title=POJOBA Homecoming Announcement|url=http://first-thoughts.org/on/Sacred+Heart+Church/|accessdate=3 January 2013}}</ref>

=== Speech and Prize-Giving Day ===
[[File:POJOSS ANNIVERSARY.jpg|thumb|Dr. Omane Boamah presenting an award to a teacher at the 58th Anniversary celebration of the school.]]
A speech and prize-giving day is held to award long-serving teachers and students who have excelled in all areas of their formation; academic work, sports and social life.<ref>{{cite news|title=Pope John SHS Celebrates Anniversary|url=http://www.ghananewsagency.org/education/pope-john-shs-celebrates-55th-anniversary-5213|agency=Ghana News Agency}}</ref>

=== SRC Symposium and Funfair ===

This event is organized by the student administrative unit to bring together all sister schools and schools within the locality for educational symposiums, entertainment jams and socialization. Occasionally, schools from the [[Greater Accra Region]], [[Ashanti Region]] and [[Volta Region]] are invited. The event is given a unique name by each group of administrators every year.

=== St. Thomas Aquinas Day celebrations ===

Also known in student circles as ''Tom Aqua'', it is organized by the chaplaincy of the school. It brings together all Catholic students within the [[Eastern Region, Ghana|Eastern Region]] to honor their [[patron saint]], [[St. Thomas Aquinas]] and discuss matters affecting them.<ref>{{cite web|url=http://ghananewsagency.org/education/catholic-students-celebrate-st-aquinas-day-39071|title=Catholic students celebrate St Aquinas day|date=9 February 2012|work=ghananewsagency.org}}</ref>
Under the 2001/2002 executives of the Koforidua Diocese Catholic Students Union (KODCASU) the St. Thomas Aquinas Day was institutionalized. The first was organized on 2 February 2002 at the St. George's Cathedral, Koforidua. The main celebrant of the Holy mass was Most.Rev Charles Gabriel Palmer-Buckle, then Bishop of Koforidua. Notable among the students were Mr. Emmanuel Obeng Codjoe (now Rev. Fr. Emmanuel Obeng Codjoe) who was the Students' Chaplain of Pope John at the time.

=== Inter-house athletics ===

This event is organized by the Sports Department to award students who distinguish themselves in field and track events. It also serves as an avenue for sport boys to be picked in representing the school at National Athletic Competitions.

=== Elections ===

General elections are organized every year to select new prefects and student administrators for the SRC.

== Campus ==

[[File:NEW HOUSE.jpg|thumb|right|alt=Newest House in POJOSS|The latest house in [[POJOSS]]]]
There are staff bungalows and teachers' flats where instructors of the students reside. There is also a Fathers' residence which hosts the school's chaplain and other priests of the [[Roman Catholic Diocese of Koforidua]].

=== Houses ===
There are currently seven houses available for boarding students who require a stay throughout a term:
*Pope John Seminary (Pope john House)
*Bishop Bowers House
*Nana Frempong Mposo House
*Alphonse Elsbernd House
*Paul Boakye House
*Fr. Amos Fredric's House
*Most Rev. Leon Badikebele Kalenga House.

{{clear}}
{{wide image|Pj pana.jpg|600px|align-cap=center|Panoramic view of the [[POJOSS]] campus}}
{{clear|left}}

== Academic performance ==

[[File:Pojoss debate 00.jpg|thumbnail|right|Pope John wins National Independence Day Debate 2000. Team members include renowned Ghanaian journalist Kojo Oppong Nkrumah, second from left.]]
[[File:Pope John Senior High School and Minor Seminary students receive their WASSCE upon Graduation.jpg|thumb|right|alt=Debate Victory|2013 National Independence Day Debate Victory<ref>{{cite news|title=Pope John wins debate|url=http://news.ghananation.com/vivvo_general/299709-pope-johns-shs-wins-16th-interschool%E2%80%99s-debate.html|newspaper=GH Headlines}}</ref>]] Pope John Senior High School and Minor Seminary maintains a very high academic standard and has over the years distinguished itself in the areas<ref>{{cite web|title=Pope John SHS|url=http://ghanaschools.info/pope-john-secondary-school/}}</ref>  of Science, Business,and Arts. In 2012, the school placed 9th on the [[WASSCE]] order of merit ([[Koforidua|Education in Koforidua]]), and has emerged victorious in many inter-school academic competitions.<ref>http://www.ghana.gov.gh/index.php/about-ghana/ghana-at-a-glance/56th-independence/20292-pope-johns-senior-high-wins-16th-interschools-debate</ref> A survey indicates that Pojoss contributes over 60% of its students to tertiary institutions around the globe every year.Also, in 2015, Pope John Senior High School came 1st in the WASSCE rankings.

=== Awards and recognition ===

*VALCO Soccer Tournament (1999) (Winner)
*National Independence Debate Championship (2000) (Winner)
*Milo Schools Soccer Championship (2001) (Winner)
*National Science and Maths Quiz (2001) (Winner)
*VALCO Soccer Tournament (2002) (Winner)<ref>{{cite web|url=http://ghanaweb.com/mobile/wap.small/sports.article.php?ID=28291 |accessdate=15 June 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20150402164943/http://ghanaweb.com/mobile/wap.small/sports.article.php?ID=28291 |archivedate=2 April 2015 }}</ref>
*Microsoft Internet Safety and Security Focus On Ghana National Quiz Contest (1st Runners Up)
*Junior Achievement Barclays Bank Chairman Awards (2009) (Placed Second)
*Project Citizen National Showcase (2011) (Placed 3rd)<ref>{{cite web|url=http://www.modernghana.com/newsthread1/369143/1/|title=St Francis Xavier Wins 2011 Project Citizen Ghana|work=Modern Ghana}}</ref>
*Ghana Youth Forum Debate (2012) (Winner)<ref>{{cite web|url=http://www.modernghana.com/news/414369/1/schools-must-instil-useful-values-in-youth.html|title=Schools Must Instil Useful Values In Youth|work=Modern Ghana}}</ref>
*National Independence Debate Championship (2013) (Winner)<ref>{{cite web|url=http://graphic.com.gh/News/pope-john-wins-national-debate.html |accessdate=15 June 2013 |deadurl=yes |archiveurl=https://web.archive.org/web/20130407145819/http://graphic.com.gh/News/pope-john-wins-national-debate.html |archivedate=7 April 2013 }}</ref>
*National Independence Debate Championship (2015) (Winner)

=== Notable teachers ===

*Mr Emmanuel Agyepong (Received 3rd Prize for National Best Teacher Award 2001)<ref>{{cite web|url=http://www.mclglobal.com/History/Mar2001/30c2001/30c1e.html|title=GRi Education News Ghana 30 – 03 - 2001|work=mclglobal.com}}</ref>
*Most Reverend [[Charles G. Palmer-Buckle]] (former chaplain and teacher)
*Mr. Yaw Gyebi (Received 2nd Prize National Best Teacher Award)
*Mr. Isaac Amanor(Geography department)
{{clear}}
*Mr Eric Bempoe (senior House Master)

== Clubs and societies ==

[[File:School Choir Pojoss.jpg|thumbnail|Members of the choir pose for a picture after Sunday Mass]]
Students of Pope John Senior High School and Minor Seminary are involved in [[Extracurricular activities]] through their membership in clubs and societies. This has brought out the talents in many of the students and has been utilized in securing victories for the school through debates, quizzes and boot camps. They include:[[File:CADET POJOSS.jpg|thumbnail|right|School Cadet on parade at the 54th Anniversary]]
*Science and Maths Club (PJSMC)
*Literary and drama club
*[[School]] [[Choir]]
*Civic Education Club
*Child's Right International
*Mystical Vibes
*Olympic Club
*GUNSA
*Future Leaders Investment Club (FLIC)
*SYTO
*FRENCH Club
*[[Presbyterian]] and [[Methodist]] Students' Union (PREMESU)
*[[Catholic]] Students' Union (CASU)
*[[American Field Service]] (AFS)
*[[Legion of Mary]]
*[[Knights of the Altar]]
*[[Air force]] Cadet
*[[Red Cross Society]]
*Art Renaissance Club
*Pope John InfoTech Club
*Junior Achievement Company
*Students' Representative Council (SRC)
*School Brass Band
*Scholarship and Talent Club
*Business club (GNABS,POJOSS)
{{clear}}

== Student Representative Council ==

[[File:POJOSS SRC.jpg|thumbnail|right|2013 year group of Student Administrators]]
Due to the large size of the school and student body, administration has been decentralized to the students level to ensure the maintenance of high standard of discipline in the school. At the helm of student administration is the Students' Representative Council, headed by a President with two assistants who perform the role of Senior Prefects as well.<ref>{{cite book|last=Official Pope John School Mag|first=Magazine|title=Daasebre|year=2013|publisher=Paa-Wills Press|location=[[Koforidua]], Ghana|pages=100}}</ref>

All other student groups, clubs and societies fall under the SRC and are accountable to its president. Among other things, the SRC ensures the maintenance of discipline among students and promotes a good student-administration forum.

It has over the years, also contributed to the development of amenities and infrastructure on the school's campus. Notable among these are the Visitors' Lounge and students' bath house. It is currently headed by Master Patrick Nana Biney.
{{clear}}

== POJOSS and Society ==

The Pope John community has through the decades maintained a very cordial relationship with society, especially with the [[Roman Catholic Church]], the town of Effiduase in Koforidua and sister schools across [[Ghana]].

=== Alliances ===

==== POJOROSA ====

[[File:pojorosa.jpg|thumbnail|right|A cartoon published in the 2012 edition of the POJOMAG illustrating the relationship between Pojoss and Roses]]
POJOROSA is the acronym representing the alliance between Pope John Senior High School and Minor Seminary and St. Roses Senior High School. This alliance has been made manifest in the marriages of alumni of the two schools. Teachers from St. Roses have taught in Pojoss and vice versa. Alumni from Pojoss have also returned the gesture and taught in St. Roses. Annual programmes organized by the two schools are often well attended by students of the schools as a gesture of the friendliness they bear each other. Apart from all these, Pope John and Saint Roses share the same heritage in their founding fathers, the [[Societas Verbi Divini|SVD]] missionaries.

==== POJOKRO ====

POJOSS' relationship with Krobo Girls' Senior High School has also resulted in the birth of POJOKRO: Though many see this as an impossible alliance owing to the fact that Krobo Girls' is a Presbyterian school, the students have often tried to breach that divide by inviting students of Krobo Girls' to some of their annual programmes.

==== The Roman Catholic Church ====

The church maintains a cordial relationship with Pojoss. Many of the school's alumni have become Roman Catholic priests for the dioceses all over Ghana. The school's boys' choir has performed at many Catholic events including the burial mass of their founder, Joseph Bowers. Catholic programmes are often held in Pope John, notable among which is the Sacred Heart Congress.

==== Electoral Commission of Ghana ====

Pojoss remains a polling station where elections in Ghana are concerned. Members of the school staff are often employed as polling officials in every general election of the country.

==== The [[University of Education, Winneba]] ====

The academic campus of Pope John Senior High School and Minor Seminary is used weekly as a weekend campus for Koforidua-resident distant learners offering courses at the university. Some of the school's teaching staff also give tuition at these weekend lectures.

=== Affiliations ===

==== The [[Roman Catholic Church]] ====

The establishment of Pope John was a vision initiated by the Roman Catholic church. It is therefore known as a Catholic school. The seminary continues to receive funding from the church to support the formation of seminarians.

==== [[Kwame Nkrumah University of Science and Technology]] ====

There is an informal relationship with KNUST. Most of the university's senior lecturers including its immediate past vice chancellor are alumni of Pope John. Additionally, a considerable number of alumni are enrolled in KNUST annually. In 2012 under the Community Impact Program, KNUST donated books to the school's library and supervised the renovation of its science lab.

=== Rivalry ===
There exists a friendly rivalry between Pope John Senior High and [[St. Peter's Boys Senior Secondary School]], Nkwatia. Inter-school programmes are therefore held termly to promote unity and cordiality among students of the two institutions.

== Chronology of headmasters ==

[[File:Benedicta Foli.JPG|thumbnail|right|Mrs. Benedicta Foli, Headmistress of Pope John]]
{| class="wikitable"
|-
! HEADMASTER !! BEGINNING OF OFFICE !! END OF OFFICE
|-
| Rev. Fr. Alphonse J. Elsbernd [[Societas Verbi Divini|SVD]] || 1958 || 1961
|-
| Rev. Fr. Amos Fredrichs SVD || 1961 || 1974
|-
| Rev. Fr. Thomas Potts SVD || 1974 || 1981
|-
| Rev. Fr. Vincent Michael Burke SVD || 1981 || 1984
|-
| Rev. Edmund Nomo SVD || 1984 || 1989
|-
| Rev. Vincent Michael Burke SVD || 1989 || 1997
|-
| Mr. Paul Ofori-Atta || 1997 || 2007
|-
| Mr. Isaac Laweh Odenteh  || 2008 || 2013
|-
| Mrs. Benedicta Foli || 2014 || Present
|}

==Notable alumni ==
{{refimprove section|date=October 2015}}
[[File:Charles G. Palmer-Buckle.JPG|right|thumbnail|Archbishop [[Charles G. Palmer-Buckle]], [[Archbishop of Accra]].]]

* [[Charles G. Palmer-Buckle|Archbishop Charles G. Palmer- Buckle]], [[Archbishop of Accra]]
* Most Rev. Dennis Kofi Agbenyadzi, S.M.A Bishop of The [[Roman Catholic Diocese of Berbérati]], [[Central African Republic]]
* [[Lieutenant General]] [[Joseph Boateng Danquah]], former [[Chief of the Defence Staff (Ghana)|Chief of Defense Staff]] (CDS) of the [[Ghana Armed Forces]]
* [[Brigadier General]] William Omane Agyekum, Director General of Personnel Administration  [[Ghana Armed Forces]] Headquarters
* [[Brigadier General]] Kweku Asamoah Yeboah, Director General of International Peace Support Operations, [[Ghana Armed Forces]]
* Ernest Ansah, Founder and Chancellor of [[DataLink University College]] 
* [[Professor William Otoo Ellis]], former Vice Chancellor of [[Kwame Nkrumah University of Science and Technology]]<ref>{{cite web|author=University Relations Office|title=Half Term Report on the Stewardship of Professor William Otoo Ellis, Vice-Chancellor of KNUST|url=http://www.knust.edu.gh/news/news-items/half-term-report-on-the-stewardship-of-professor-william-otoo-ellis-vice-chancellor-of-knust|work=Kwame Nirumah University website| date=27 May 2013|accessdate=6 December 2013}}</ref>
* Professor Samuel I.K Ampadu, former Provost, [[College of Engineering]], [[KNUST]] and former President of the Ghana Institute of Engineers (GhIE)
* Professor Kodjo Amedjorteh Senah, Department of Sociology, [[University of Ghana]]
* Professor Kwadwo Ofori, Dean of Graduate Studies, [[University of Ghana]]
* Professor Rexford Assasie Oppong, Department of Architecture, [[Kwame Nkrumah University of Science and Technology]] 
* Dr. Kwame Adu Kankam, Professor of Statistics, [[Pennsylvania State University]]<ref>{{cite web|author=Penn State Statistics Site Administrator|title=Kwame Kankam|url=http://stat.psu.edu/people/kak440|work=Penn State University website| date=17 May 2016|accessdate=6 August 2016}}</ref>
* Dr. Vincent Agyapong, Associate Clinical Professor, Department of Psychiatry, [[University of Alberta]], [[Canada]]
* Dr. Emmanuel Karlo Nyarko, Assistant Professor, [[Faculty of Electrical Engineering, University of Osijek]] 
* Justice Dr. Richmond Osei-Hwere, High Court Judge and Lecturer at [[KNUST]] 
* [[Edward Omane Boamah]], Minister of Communications
* Hon. Samuel Nuamah Donkor, former Ashanti Regional Minister
* Hon. Daniel Christian Dugan, former Deputy Minister of Fisheries and Aquaculture
* Hon. Antwi-Boasiako Sekyere, former Eastern Regional Minister
* [[Ebenezer Okletey Terlabi]], MP for Lower Manya Krobo and former Deputy Minister of Defence
* [[Kojo Oppong Nkrumah]], Politician, Lawyer, Entrepreneur and former Broadcaster
* Franklin Cudjoe, Founder and President, [[IMANI Centre for Policy and Education]]
* Dr. Michael Kpessa Whyte, Executive Director of the National Service Secretariat
* Dr. Kwame Akuffo Anoff- Ntow, Director General of the [[Ghana Broadcasting Corporation]]

==See also==
* [[Ministry of Education (Ghana)]]
* [[List of senior secondary schools in Ghana]]
* [[Joseph Oliver Bowers]]
* [[List of schools in Ghana]]

== References ==

{{Reflist|30em}}

== External links ==
* {{cite news
|last=joy |first=fm
|url=http://business.myjoyonline.com/pages/banking/201109/73902.php
|title=Bank of Africa arrives in Ghana
|work=news agent |date=30 August 2011 |accessdate=30 August 2011 }}
* {{cite news
|last=Online |first=Graphic
|url=http://graphic.com.gh/I-ll-Tell-My-Story/kojo-oppong-nkrumah-a-young-achiever.html
|title=Kojo Oppong Nkrumah: A young achiever.
|work=National Newspaper |date=18 September 2012 |accessdate=18 September 2012}}
* [http://cbcgha.org/cbc/ Ghana Catholic Bishops Conference]
* [https://web.archive.org/web/20131008081006/http://ghanaschoolsonline.com/intermediate/school_profile.cfm?levelID=2&schID=1713]
* [https://web.archive.org/web/20140512222325/http://www.globalnewsreel.com/index.php?option=com_content&view=article&id=428:pope-johns-senior-high-school-kick-starts-eastern-region-social-media-training-project&catid=16:education&Itemid=107]
* http://www.ghanaschoolsnet.com/group/popejohnseniorhighschool/forum/topics/prominent-old-boys
* [http://www.ghana.gov.gh/index.php/about-ghana/ghana-at-a-glance/56th-independence/20292-pope-johns-senior-high-wins-16th-interschools-debate|publisher=Ghana%20Government|accessdate=5th%20March%202013 http://www.ghana.gov.gh/index.php/about-ghana/ghana-at-a-glance/56th-independence/20292-pope-johns-senior-high-wins-16th-interschools-debate|publisher=Ghana Government|accessdate=5 March 2013]
* http://cbcgha.org/cbc/index.php?option=com_content&view=article&id=179&Itemid=322
* [https://archive.is/20130713045925/http://www.studentworldassembly.org/page.php?subsub=268&catid=228&url=/616047228/george_sarfo_dwomoh_.html]

[[Category:Christian educational institutions]]
[[Category:Roman Catholic minor seminaries]]
[[Category:High schools in Ghana]]
[[Category:Roman Catholic secondary schools in Ghana]]
[[Category:Roman Catholic schools in Ghana]]
[[Category:1958 establishments in Ghana]]
[[Category:Educational institutions established in 1958]]
[[Category:Education in the Eastern Region (Ghana)]]