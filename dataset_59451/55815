{{other uses|Baileya (disambiguation)}}
{{unreferenced|date=May 2011}}
{{Italic title}}
'''''Baileya''''' ({{ISSN|0005-4003}}) is a [[scientific journal]] of [[cultivated plant taxonomy|horticultural taxonomy]], published quarterly by the [[Liberty Hyde Bailey Hortorium]] ([[Cornell University]]). The journal was established in 1953, but is currently inactive. Its name honors the late [[Liberty Hyde Bailey]].

==Volumes==
''Baileya'''s first volume was published in March, 1953. Thereafter, volumes 2-15 were published regularly, over consecutive years, from 1954-1967. Volumes 16-26 (26 was the last volume), however, were published irregularly from 1968-1996.{{Citation needed|date=May 2011}}

== Notable articles ==
The following articles are representative of ''Baileya'''s focus on cultivated plant taxonomy.{{According to whom|date=May 2011}}

*Moore HE, Jr. 1954. The seersucker plant - ''Geogenanthus undatus. Baileya'' 2: 41-45.
*Moore HE, Jr. 1957. ''Musa'' and ''Ensete''. The cultivated bananas. ''Baileya'' 5: 167-194.
*Egolf DR. 1970. ''Hibiscus syriacus'' 'Diana', a new cultivar (Malvaceae). ''Baileya'' 17 (2): 75-78.
*Hoshizaki BJ. 1971. The genus ''Adiantum'' in cultivation (Polypodiaceae). ''Baileya'' 17 (3): 97-144.
*Heiser CB. 1972. Notes on some species of ''Solanum'' (Sect. ''Leptostemonum'') in Latin America. ''Baileya'' 18 (2): 59-65.
*Hyypio PA. 1973. A note on ''Syngonanthus elegans''(Eriocaulaceae). ''Baileya'' 19 (3): 116-117.
*D'Arcy WG, Eshbaugh WH. 1974. New World peppers (''Capsicum'', Solanaceae) north of Colombia: a resume. ''Baileya'' 19: 93-105.
*Heiser CB, Pickersgill B. 1975. Names for the bird peppers (Capsicum - Solanaceae). Baileya 19 (4): 151-156.
*Christie SR, Hall DW. 1979. A new hybrid species of ''Nicotiana'' (Solanaceae). ''Baileya'' 20 (4): 133 - 136.
*Tucker AO, Hensen KJW. 1985. The cultivars of lavender and lavandin (Labiatae). ''Baileya'' 22 (4): 168-177.
*Soreng RJ, Cope EA. 1991. On the taxonomy of cultivated species of the ''Chrysanthemum'' genus-complex (Anthemideae : Compositae). ''Baileya'' 23 (3): 145-165.
*Whalen MD. 1991. Taxonomy of ''Saccharum'' (Poaceae). ''Baileya'' 23 (3): 109-125.

==References==
{{reflist}}

[[Category:Botany journals]]
[[Category:Publications established in 1953]]
[[Category:English-language journals]]
[[Category:Cornell University]]
[[Category:Quarterly journals]]
[[Category:Publications with year of disestablishment missing]]
[[Category:Defunct journals]]


{{botany-journal-stub}}