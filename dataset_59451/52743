{{Infobox journal
| title = Review of Economics of the Household
| cover =
| editor = [[Shoshana Grossbard]]
| discipline = [[Economics]]
| former_names =
| abbreviation = Rev. Econ. Househ.
| publisher = [[Springer Science+Business Media]]
| country =
| frequency = Quarterly
| history = 2003-present
| openaccess =
| license =
| impact = 0.732
| impact-year = 2010
| website = http://www.springer.com/economics/microeconomics/journal/11150
| link1 = http://www.springerlink.com/openurl.asp?genre=issue&issn=1569-5239&issue=current
| link1-name = Online access
| link2 =
| link2-name =
| JSTOR = 
| OCLC = 300197795
| LCCN = 2007209324
| CODEN = 
| ISSN = 1569-5239
| eISSN = 1573-7152
}}
The '''''Review of Economics of the Household''''' is a [[Peer review|peer-reviewed]] [[academic journal]] established in 2001 by [[Shoshana Grossbard]] and first published in 2003. It covers research on [[household economics]], including empirical and theoretical research on the economic behavior and decision-making processes of single and multi-person households. Articles published deal with [[Consumption_(economics)|consumption]], savings, [[Labour_supply|labor supply]] and other [[Time_use_research|time uses]], [[Economics_of_marriage|marriage]] and [[divorce#Causes_of_divorce|divorce]], [[Health_economics#Health_care_demand|demand for health]] and other forms of human capital, [[fertility]] and investment in children's human capital, households and environmental economics, economics of  [[Human_migration#Theories_for_migration_for_work_in_the_21st_century|migration]], economic development, and [[economics of religion]]. It also publishes articles on household economics from the perspective of the history of economic thought and emphasizes effects of both micro- and macro-economic policy instruments.<ref>{{cite web |url=http://www.springer.com/economics/microeconomics/journal/11150 |title=About this journal |publisher=[[Springer Science+Business Media]] |work=Review of Economics of the Household |accessdate=2012-01-25}}</ref><ref>Grossbard, Shoshana (2011) Reflections of a Founding Editor, ''Newsletter of the Committee on the Status of Women in the Economics Profession'', Spring, p. 7</ref>
The original advisory board included [[Gary Becker]] and [[Jacob Mincer]], the founders of the Columbia School of Household Economics often called the [[New Home Economics]], as well as another Nobel prize winner, [[Clive Granger]]. Co-editor [[Michael Grossman (economist)|Michael Grossman]], associate editor Barry Chiswick, and advisory board member [[Victor Fuchs]] are prominent contributors to the Columbia School of Household Economics.

== References ==
{{Reflist}}

== External links ==
* {{Official|http://www.springer.com/economics/microeconomics/journal/11150}}

[[Category:Economics journals]]
[[Category:Springer Science+Business Media academic journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 2003]]
[[Category:English-language journals]]