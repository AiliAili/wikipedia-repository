{{Infobox coin
| Denomination        = Indian Head quarter eagle ($2.50)
| Country             = United States
| Value               = 2.5
| Unit                = [[United States dollar]]s
| Mass                = 4.18
| Gold_troy_oz        = .12094
| Diameter            = 18
| Edge                = reeded
| Composition         = .900 gold, .100 copper
| Mint marks          = [[Denver Mint|D]]. Located to left of arrowhead on reverse. [[Philadelphia Mint]] pieces lack mint mark.
| Years of Minting    = 1908–1915, 1925–1929
| Obverse             = {{Css Image Crop|Image = NNC-US-1908-G$2½-Indian Head.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 3|Location = center|Description=}}
| Obverse Design      = Native American male in headdress
| Obverse Designer    = [[Bela Lyon Pratt]]
| Obverse Design Date = 1908
| Reverse             = {{Css Image Crop|Image = NNC-US-1908-G$2½-Indian Head.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|Description=}}
| Reverse Design      = Standing eagle
| Reverse Designer    = Bela Lyon Pratt
| Reverse Design Date = 1908
}}
{{Infobox coin
| Denomination        = Indian Head half eagle ($5)
| Country             = United States
| Value               = 5
| Unit                = [[United States dollar]]s
| Mass                = 8.359
| Gold_troy_oz        = .24187
| Diameter            = 21.6
| Edge                = reeded
| Composition         = .900 gold, .100 copper
| Mint marks          = [[Denver Mint|D]], [[New Orleans Mint|O]], [[San Francisco Mint|S]]. Located to left of arrowhead on reverse. [[Philadelphia Mint]] pieces lack mint mark.
| Years of Minting    = 1908–1916, 1929
| Obverse             = {{Css Image Crop|Image = NNC-US-1908-G$5-Indian Head.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 1|Location = center|Description=}}
| Obverse Design      = Native American male in headdress.
| Obverse Designer    = [[Bela Lyon Pratt]]
| Obverse Design Date = 1908
| Reverse             = {{Css Image Crop|Image = NNC-US-1908-G$5-Indian Head.jpg |bSize = 475|cWidth = 235|cHeight = 235|oTop = 3|oLeft = 238|Location = center|Description=}}
| Reverse Design      = Eagle
| Reverse Designer    = Bela Lyon Pratt
| Reverse Design Date = 1908
}}

The '''Indian Head gold pieces''' or '''Pratt-Bigelow gold coins''' were two separate coin series, identical in design, struck by the [[United States Mint]]: a two-and-a-half dollar piece, or [[quarter eagle]], and a five-dollar coin, or [[half eagle]]. The quarter eagle was struck from 1908 to 1915 and from 1925–1929. The half eagle was struck from 1908 to 1916, and in 1929. The pieces remain the only US circulating coins with recessed designs. These coins were the last of their denominations to be struck for circulation, ending a series which had begun in the 1790s.

[[President of the United States|President]] [[Theodore Roosevelt]], from 1904, vigorously advocated new designs for United States coins, and had the Mint engage his friend, the sculptor [[Augustus Saint-Gaudens]], to design five coins (the four gold pieces and the cent) that could be changed without congressional authorization.  Before his death in August 1907, Saint-Gaudens completed designs for the [[eagle (United States coin)|eagle]] ($10 piece) and [[double eagle]], although both required subsequent work to make them fully suitable for coining.

With the eagle and double eagle released into circulation by the end of 1907, the Mint turned its attention to the half eagle and quarter eagle, originally planning to duplicate the double eagle's design.  The Mint had difficulty fitting the required inscriptions on the small gold coins. President Roosevelt, in April 1908, convinced Mint Director [[Frank Leach]] that it would be a better idea to strike a design similar to that of the eagle, but below the background, to secure a high-relief effect.  Such coins were designed by Boston sculptor [[Bela Lyon Pratt]] at the request of the President's friend, [[William Sturgis Bigelow]].  After some difficulty, the Mint was successful in this work, though Pratt was unhappy at modifications made by the Mint's engravers, headed by longtime Chief Engraver [[Charles E. Barber]].

The two pieces were struck until [[World War I]] caused gold to vanish from circulation, and then again in the late 1920s.  Neither coin circulated much; the quarter eagle saw popularity as a Christmas present.  In 1933, President [[Franklin Roosevelt]] stopped the issuance of gold in coin form, and recalled many pieces which were in private or bank hands.

== Inception ==

{{for|additional detail on the origin of the idea for the 1907–1908 United States gold coin redesign|Saint-Gaudens double eagle#Inception|Indian Head eagle#Inception}}

In 1904, [[President of the United States|US President]] [[Theodore Roosevelt]] complained about the artistic quality of American coinage to his [[United States Secretary of the Treasury|Secretary of the Treasury]], [[Leslie Mortier Shaw]], and asked if it were possible to hire a private sculptor such as the President's friend [[Augustus Saint-Gaudens]] to give modern, artistic designs to US coins.{{sfn|Moran|2008|p=216}}  At Roosevelt's instigation, Shaw had the Mint (part of the [[United States Department of the Treasury|Department of the Treasury]]) hire Saint-Gaudens to redesign five denominations of US coinage that could be changed without an [[Act of Congress]]: the cent and the four gold pieces (the [[quarter eagle]], [[half eagle]], [[eagle (United States coin)|eagle]] and [[double eagle]]).{{sfn|Moran|2008|p=235}}  By the [[Mint Act of 1792]], an "eagle" was made equivalent to ten dollars.{{sfn|Hobson|1971|p=113}}

Mint officials originally assumed that whatever design was selected for the double eagle would simply be scaled down for the three lower denominations.  In May 1907, however, President Roosevelt decided that the eagle and double eagle would bear different designs, a departure from past practice.{{sfn|Burdette|2006|pp=317–318}}  In August (the month of Saint-Gaudens' death from cancer),{{sfn|Burdette|2006|p=105}} outgoing Mint Director [[George E. Roberts]] wrote, "no instructions have been received from the President as to the half and quarter eagle, but I expected that the eagle design would be used upon them".{{sfn|Burdette|2006|pp=106–107}}  After considerable difficulties, the Mint issued the eagle and double eagle based on Saint-Gaudens' designs later that year.  The eagle featured Liberty wearing an Indian headdress on the obverse and a perched [[bald eagle]] on the reverse; the double eagle featured Liberty striding forward on the obverse and a flying eagle on the reverse.{{sfn|Hobson|1971|pp=159–160}}

Due to the difficulties with the two larger coins, little attention was given to the half eagle and quarter eagle until late 1907.  On November 28, 1907, Treasury Secretary [[George Cortelyou]] wrote in a letter that the double eagle design was to be used for the two small gold pieces.  On December 2, Mint Director [[Frank Leach]] instructed the [[Philadelphia Mint]] to prepare coinage dies for the small pieces, using the double eagle design.  Chief Engraver [[Charles E. Barber]] replied  a week later that it would be difficult to put all the legends that were required by law on the new pieces, such as the name of the country.  On the double eagle, "[[E Pluribus Unum]]" is placed on the edge, an impractical setting on pieces about the size of the [[nickel (United States coin)|nickel]] and [[dime (United States coin)|dime]].{{sfn|Burdette|2006|p=318}}  Philadelphia Mint Superintendent John Landis forwarded Barber's letter to Leach with his own note, stating, "I know it will be difficult to put the inscription 'E Pluribus Unum' on the periphery of a quarter eagle, but I do not see where else it can [go] and we must try to do it".{{sfn|Burdette|2006|pp=318–319}}

Barber was assigned the task of solving these difficulties.  He planned to use his low-relief version of Saint-Gaudens' double eagle design, but he made slow progress on the assignment. Leach wrote to Saint-Gaudens' attorney to ask if the sculptor's assistant [[Henry Hering]] could do the work.  Hering was willing, and asked for enlarged models of the double eagle designs.  Barber opposed bringing in outsiders, citing delays in the preparation of the earlier gold coin designs which he attributed to the Saint-Gaudens studio: "it is entirely unnecessary to trouble Mr. Hering any further, unless another year is to be wasted in vain endeavor".{{sfn|Burdette|2006|p=321}}  On January 3, 1908, Leach wrote to Hering to inform him that all work would be done by the Mint.{{sfn|Burdette|2006|p=321}}

== Innovation ==

The President's friend, Dr. [[William Sturgis Bigelow]], had been in Japan for most of 1907; on his return to his Boston home he heard about the Saint-Gaudens coinage from Senator [[Henry Cabot Lodge]].  Bigelow was one of a number of Roosevelt's friends given early specimens of the double eagle.  He wrote to the President on January 8, 1908, praising the Saint-Gaudens coins and stating that he was working with a Boston sculptor, [[Bela Pratt]], on an idea that would allow coins to be struck in high relief.  Pieces struck in this manner would have the designs protected from wear and be able to stack easily (both problems with high relief coins).  The President responded to express his interest on January 10, and Pratt was soon busy on a model for him to examine.  Roosevelt did not then tell the Mint of the new proposal.{{sfn|Burdette|2006|pp=324–326}}

[[File:Pratt plaster models.png|thumb|240px|left|Bela Pratt's plaster models for the half eagle]]

The newly released eagle and double eagle had provoked considerable controversy over their omission of the motto "[[In God We Trust]]", and with Congress already preparing to require the motto's use, Leach ordered work suspended on the half and quarter eagle on January 18.  The Mint had not expected to have to put "In God We Trust" on small coins of the double eagle design, on which it was already having trouble finding space for the other required legends.  On the assumption the bill would succeed, Leach had Barber continue with his work, and at least one pattern half eagle was struck on February 26 for Leach to show the President.{{sfn|Burdette|2006|p=196}}

Pratt completed plaster models for the coin, using the obverse design for the ten-dollar piece as the basis, but using a photograph of an unknown, male Indian from his photo collection rather than Saint-Gaudens' female Liberty.  He displayed one model in his [[Connecticut]] studio, and sent another to Bigelow for presentation to the President the next time the two friends met.  Roosevelt and Bigelow had lunch with Mint Director Leach on April 3.  The President was enthusiastic about the proposed coin.{{sfn|Burdette|2006|pp=326–328}}  Leach recalled in his memoirs:

[[File:NNC-US-1907-G$10-Indian Head (no motto).jpg|thumb|240px|The [[Indian Head eagle]], designed by Saint-Gaudens, was the basis for the designs for the smaller gold pieces.]]

{{quote |
Originally it was the intention to give the $5 and $2.50 pieces the same design as that used on the double eagle or $20 piece, but before final action to that end was taken President Roosevelt invited me to lunch with him at the White House. His purpose was to have me meet Doctor William Sturgis Bigelow of Boston, a lover of art and friend of the President, who was showing great interest in the undertaking for improving the appearance of American coins, and who had a new design for the smaller gold coins. It was his idea that the commercial needs of the country required coins that would "stack" evenly, and that the preservation of as much as possible of the flat plane of the piece was desirable. A coin, therefore, with the lines of the design, figures, and letters depressed or incused, instead of being raised or in relief, would meet the wishes of the bankers and business men, and at the same time introduce a novelty in coinage that was artistic as well as adaptable to the needs of business.{{sfn|Leach|1917|p=381}}
}}

[[File:Indian coin group.jpg|thumb|240px|left|Examples of the Indian Head gold pieces grouped with modern coins for size comparison purposes]]

As a result of the White House lunch meeting, Leach agreed to abandon the idea that the double eagle design be used for the small gold pieces, and to cooperate with Bigelow and Pratt in the making of the new coins.  Leach even undertook to reimburse Pratt's fee of $300 to Bigelow from government funds.{{sfn|Burdette|2006|p=329}}  The President wanted to see Saint-Gaudens' standing eagle from the ten-dollar piece adapted in a recessed surface for the smaller pieces, and, if it did not constitute a change of design, used on the ten-dollar piece as well (a project that did not go beyond the talking stage).  Bigelow wrote to Pratt on May 1 after conferring with Leach, stating that the Mint Director would likely not object if Pratt were to improve Saint-Gaudens' standing eagle, but "I would not, if I were you, get too far from the original, as the President likes it.  Perhaps you can make him like it better."{{sfn|Burdette|2006|p=330}}

Dies had been cut for the Saint-Gaudens half eagle, causing Leach to ask for a legal opinion on whether that constituted a change of design—if it did, no further change could be made for 25&nbsp;years without an Act of Congress.  The opinion must have been satisfactory, as Roosevelt approved Pratt's obverse design in mid-May, subject to minor changes requested by the Mint.  Leach decided that both the Mint and Pratt would make versions of the standing eagle reverse; Pratt's was adopted.  Pratt sent the models and casts to the Mint on June 29.{{sfn|Burdette|2006|pp=331–332}}  Barber did not make master dies based on Pratt's work until he returned from his August vacation at [[Ocean Grove, New Jersey]].  Experimental pieces to a total face value of $75 (likely ten half eagles and ten quarter eagles) were sent to Leach in Washington from the Philadelphia Mint on September 21.{{sfn|Burdette|2006|p=333}}  Leach showed the pieces to the President, who kept a half eagle and gave it to Bigelow.  As Leach had worked against practices that allowed pattern coins to leave the Mint, the coin sent to Bigelow may have been the only pattern not to be melted.{{sfn|Burdette|2006|pp=334–335}}  The present location of the coin is not known; no pattern coins of the Indian Head gold pieces are presently known to exist.{{sfn|Burdette|2006|p=368}}  Leach approved the designs subject to some "improvements" which Barber wanted to make.{{sfn|Burdette|2006|p=333}}  The Mint Director wrote to Superintendent Landis on September 26,

{{quote |
I desire that this shall be accomplished as soon as possible as I am under obligation to the President to have several thousand pieces coined by the first of November next and I want enough half eagle dies prepared so that a couple of pairs at least can be supplied [to] Denver and San Francisco.  The quarter eagle will be coined only at your institution.{{sfn|Burdette|2006|p=333}}
}}

After production of the new coins began, Bigelow received one of each; he showed them to Pratt who wrote to his mother, "They have 'knocked spots' out of my design at the mint.  They let their die cutter spoil it, which he did most thoroughly&nbsp;... but they tried to retouch it and gee!  They made a mess of it.  With a few deft strokes the butcher or blacksmith [Barber] who is at the head of things there, changed it from a thing that I was proud of to one [of which] I am ashamed."{{sfn|Burdette|2006|p=339}}

== Design ==

[[File:1924 double eagle.jpg|thumb|240px|Saint-Gaudens' double eagle design, which the Mint hoped to modify for the smaller gold coins]]

The half eagle and quarter eagle are identical in design, and are unique in American coinage in having incuse (engraved, as opposed to bas-relief) designs.{{sfn|Yeoman|2013|p=264}}  The obverse features the head of a Native American man, wearing a headdress and facing left.  The designer's initials, BLP, are found just above the date.  The reverse features a standing eagle on a bunch of arrows, its left talon holding an olive branch in place.  The [[mint mark]] is found to the left of the arrowheads.{{sfn|Akers|2008|pp=10–11}}

Although Saint-Gaudens' design for the eagle had featured Liberty in an Indian-style headdress, no attempt was made to make her features appear to be Native American.  According to numismatist Mike Fuljenz in his book on early 20th century American gold coinage, the obverse of the eagle had featured "Lady Liberty topped with a fanciful head covering designed to look like an Indian headdress".{{sfn|Fuljenz|2010|p=16}}  Until Saint-Gaudens' and Pratt's pieces were struck, only Mint Chief Engraver [[James Longacre]] had attempted to depict Indians on US circulating coinage (in the 1850s), with his [[Indian Head cent]] and Indian Princess designs for the gold dollar and three-dollar pieces.  After Pratt, only [[James Earle Fraser (sculptor)|James Earle Fraser]]'s depiction of an Indian in 1913 on the [[Buffalo nickel]] would appear until the 2000 arrival of the [[Sacagawea dollar]].{{sfn|Fuljenz|2010|p=16}}

Art historian [[Cornelius Vermeule]] in 1970 dismissed complaints made at the time of issuance that the Indian was too thin: "the Indian is far from emaciated, and the coins show more imagination and daring of design than almost any other issue in American history.  Pratt deserves to be admired for his medals and coins."{{sfn|Vermeule|1971|p=121}}  Vermeule suggests that Pratt's design "marked a transition, in the 'emaciated' Indian at least, to [[naturalism (arts)|naturalism]]".{{sfn|Vermeule|1971|p=121}}

Breen suggests the sunken surfaces were similar to those on coins from Egypt's [[Fourth dynasty of Egypt|Fourth Dynasty]].  Under the [[Mint Act of 1792]], the obverse was to bear an "impression emblematic of Liberty"; he notes that a Native American on the obverse was particularly appropriate "for after all the Indians were free peoples before the white man's laws made them third-class citizens" and suggests that Pratt's eagle, before it was modified by Barber, was "worthy of [[John James Audubon|J.J. Audubon]]".{{sfn|Breen|1988|p=502}}

== Production, circulation, and collecting ==

[[File:Frank A Leach.jpg|thumb|200px|Mint Director [[Frank A. Leach]] thought well of the new coins despite the controversy.]]

Dies for the half eagle were sent to the mints in [[Denver]] and [[San Francisco]]; both western mints reported difficulties in striking the new pieces.  Landis wrote to his counterparts at the other mints, advising them that the planchets, or blanks, needed to be shaved very slightly to strike properly.  The new coins proved to be thinner than earlier coins of their denomination, due to the field being raised above the design.  This meant that automated sorting machines could not reliably sort them when mixed with earlier coins.{{sfn|Burdette|2006|p=344}}

The new gold pieces entered circulation in early November 1908, attracting some negative comment.{{sfn|Taxay|1983|p=326}}  Philadelphia numismatist Samuel Chapman wrote to Roosevelt in early December to criticize the new coins.  The indentations in the new coins would harbor dirt and germs, Chapman argued; the coins could be easily counterfeited by carving a disc of metal.  They could not adequately stack, and they were in any event not handsome, with the Indian "emaciated".{{sfn|Taxay|1983|pp=326–328}}

According to numismatic historian Roger Burdette, "Chapman's letter caused some consternation at the White House".{{sfn|Burdette|2006|p=358}}  The President prepared a reply in which he expressed himself strongly to Chapman, but Bigelow persuaded him to substitute a milder letter over Bigelow's signature, defending the new coins.{{sfn|Burdette|2006|pp=358–359}}  Bigelow's letter replied to Chapman's complaint about the Indian, "The answer to this is that the head was taken from a recent photograph of an Indian whose health was excellent.  Perhaps Mr. Chapman has in mind the fatter but less characteristic type of Indian sometimes seen on the reservations."{{sfn|Fuljenz|2010|p=9}} Chapman wrote again, and had the correspondence published in the numismatic press, but no one at the [[lame duck (politics)|lame duck]] Roosevelt White House bothered to reply, according to Burdette, "the new coins were issued and would remain as they were for twenty-five years, or until Congress ordered them changed".{{sfn|Burdette|2006|pp=358–359}}  Leach wrote to Bigelow on January 2, 1909, "I was somewhat amused by their savage attack, and should have liked to have been in a position to reply to this unjust criticism.  However, I am pleased to say that adverse criticism of the coins is an exception.  I feel very well pleased with the result."{{sfn|Burdette|2006|p=360}}

Both the half and quarter eagle were struck each year through 1915.  While "hard money" circulated in quantity in the West, in the East banknotes were much more common.  A common use of the small gold pieces was as Christmas presents—the pieces would be produced at the various mints late in the year, be purchased from banks in December and return to vaults by late January.  The establishment of the [[Federal Reserve System]] in 1913 increased the circulation of banknotes, and the Mint ceased to strike quarter eagles after 1915 and half eagles after 1916.{{sfn|Fuljenz|2010|p=11}}  An additional factor was the economic unrest caused by [[World War I]], causing gold prices to rise and coins made of that metal to vanish from circulation.  After the war, gold did not return to circulation, and most gold coins struck were double eagles, used for international transactions and backing for gold certificates.{{sfn|Bowers|2004|p=240}}

The quarter eagle remained popular as a Christmas gift but did not initially come back into production as the Treasury held stocks of the pieces from the prewar years.  This surplus was slow to dissipate, as gift givers preferred the older Liberty Headquarter eagle that had been struck until 1908.  With the Liberty Head pieces becoming rarer and acquiring a premium above face value, the quarter eagle was finally struck again in 1925, principally to be given as presents.{{sfn|Lange|2006|p=164}}  The 1925 quarter eagle was struck only at Denver, and it was then struck from 1926 to 1929 only at Philadelphia.{{sfn|Yeoman|2013|p=249}}  With the economic collapse which started [[the Depression]], the quarter eagle was not called for in commerce, and the Mint halted production.{{sfn|Fuljenz|2010|p=11}}

[[File:1911-D Indian Head quarter eagle reverse detail.jpg|thumb|left|The mint mark is located to the left of the arrowhead]]
The half eagle was not struck again until 1929, at Philadelphia.  Coins of that date have a rarity not reflected in the mintage of 668,000 as few entered commerce.  Gold coins not released were melted in the mid-1930s, along with those recalled from banks and private holders, after President [[Franklin Roosevelt]] in 1933 ended the issuance of gold coins.  Roosevelt's actions put an end to the quarter and half eagle series, which had begun in 1796 and 1795, respectively.{{sfn|Akers|2008|p=122}}{{sfn|Yeoman|2013|pp=243, 253}}{{sfn|Lange|2006|p=165}}

There are only 15 different Indian Headquarter eagles by date and mintmark; the key is the 1911 struck at Denver (1911-D), which the 2014 ''[[A Guide Book of United States Coins]]'' values at $2,850 even in well-circulated VF (Very Fine, or 20 on the [[coin grading|Mint State]] scale) condition.{{sfn|Yeoman|2013|p=249}}  The half eagle series is longer; 24 pieces by date and mint mark.  The final entry, 1929 is the key date for the half eagle series, followed by the 1909-O, struck at the [[New Orleans Mint]] in its final year of operation.{{sfn|Yeoman|2013|p=264}}

== References ==

{{reflist|20em}}

'''Bibliography'''

* {{cite book
  | last = Akers
  | first = David W.
  | year = 2008
  | title = A Handbook of 20th Century Gold Coins, 1907–1933
  | publisher = Zyrus Press
  | location = Irvine, Cal.
  | edition = 2nd
  | isbn = 978-1-933990-14-9
  | ref = harv
  }}
* {{cite book
  | last = Bowers
  | first = Q. David
  | authorlink = Q. David Bowers
  | year = 2004
  | title = A Guide Book of Double Eagle Gold Coins
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1784-8
  | ref = harv
  }}
* {{cite book
  | last = Breen
  | first = Walter
  | year = 1988
  | title = Walter Breen's Complete Encyclopedia of U.S. and Colonial Coins
  | publisher = Doubleday
  | location = New York, N.Y.
  | isbn = 978-0-385-14207-6
  | ref = harv
  }}
* {{cite book
  | last = Burdette
  | first = Roger W.
  | year = 2006
  | title = Renaissance of American Coinage, 1905–1908
  | publisher = Seneca Mill Press L.L.C
  | location = Great Falls, Va.
  | isbn = 978-0-9768986-1-0
  | ref = harv
  }}
* {{cite book
  | last = Fuljenz
  | first = Mike
  | year = 2010
  | title = Indian Gold Coins of the 20th Century
  | publisher = Subterfuge Publishing
  | location = Lumberton, Tex.
  | isbn = 978-0-9819488-9-8
  | ref = harv
  }}
* {{cite book
  | last = Hobson
  | first = Walter
  | year = 1971
  | title = Historic Gold Coins of the World
  | publisher = Doubleday and Co <!-- That's the way it is on the title page, I know it is different from the other Doubledays, location too. -->
  | location = Garden City, N.Y.
  | isbn = 978-0-385-08137-5
  | ref = harv
  }}
* {{cite book
  | last = Lange
  | first = David W.
  | year = 2006
  | title = History of the United States Mint and its Coinage
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-1972-9
  | ref = harv
  }}
* {{cite book
  | last = Leach
  | first = Frank
  | authorlink = Frank A. Leach
  | year = 1917
  | title = Recollections of a Newspaperman
  | publisher = S. Levinson
  | location = San Francisco, Cal.
  | url = https://books.google.com/books?id=PCEuAAAAYAAJ
  | ref = harv
  }}
* {{cite book
  | last = Moran
  | first = Michael F.
  | year = 2008
  | title = Striking Change: The Great Artistic Collaboration of Theodore Roosevelt and Augustus Saint-Gaudens
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | isbn = 978-0-7948-2356-6
  | ref = harv
  }}
* {{cite book
  | last = Taxay
  | first = Don
  | year = 1983
  | title = The U.S. Mint and Coinage
  | publisher = Sanford J. Durst Numismatic Publications
  | location = New York, N.Y.
  | edition = reprint of 1966
  | isbn = 978-0-915262-68-7
  | ref = harv
  }}
* {{cite book
  | last = Vermeule
  | first = Cornelius
  | authorlink = Cornelius Clarkson Vermeule III
  | year = 1971
  | title = Numismatic Art in America
  | publisher = The Belknap Press of Harvard University Press
  | location = Cambridge, Mass.
  | isbn = 978-0-674-62840-3
  | ref = harv
  }}
* {{cite book
  | last = Yeoman
  | first = R.S.
  | authorlink = Richard S. Yeoman
  | year = 2013
  | title = [[A Guide Book of United States Coins]] (The Official Red Book)
  | publisher = Whitman Publishing
  | location = Atlanta, Ga.
  | edition = 67th
  | isbn = 978-0-7948-4180-5
  | ref = harv
  }}

== External links ==
* [https://books.google.com/books?id=PpAUAAAAYAAJ&dq=The%20answer%20to%20this%20is%20that%20the%20head%20was%20taken%20from%20a%20recent%20photograph%20of%20an%20Indian%20whose%20health%20was%20excellent.&pg=PA33#v=onepage&q=The%20answer%20to%20this%20is%20that%20the%20head%20was%20taken%20from%20a%20recent%20photograph%20of%20an%20Indian%20whose%20health%20was%20excellent.&f=false Article in February 1909 ''The Numismatist'' discussing the new gold pieces and containing the Chapman correspondence.]

{{Featured article}}
{{Coinage (United States)}}

[[Category:1908 introductions]]
[[Category:Coins of the United States]]