{{good article}}
{{Infobox person
| name            = Satoshi Kon
| image           = [[File:Satoshi Kon.jpg|230px|A photograph of Satoshi Kon]]
| image_size      =
| alt             = 
| caption         = 
| birth_date      = {{Birth date|1963|10|12}}
| birth_place     = [[Sapporo, Hokkaido]], Japan
| death_date      = {{Death date and age|2010|08|24|1963|10|12}}
| death_place     = [[Tokyo, Japan]]<ref>{{cite web |author=Scott, A. O. |url=https://www.nytimes.com/2010/08/26/arts/design/26kon.html |title=Satoshi Kon, Anime Filmmaker, Dies at 46 |work=The New York Times |publisher=The New York Times Company |date=August 26, 2010 |accessdate=March 5, 2013 }}</ref>
|death_cause      = [[Pancreatic cancer]]
| other_names     = Yoshihiro Wanibuchi
| occupation      = [[Animator]], [[film director]], [[screenwriter]], [[Mangaka|manga artist]]
| years_active    = 1984–2010
| alma_mater      = [[Musashino Art University]]
| spouse          = Kyoko Kon (?–2010; his death)<ref>{{cite web|last1=Osmond|first1=Andrew|title=Satoshi Kon obituary|url=https://www.theguardian.com/film/2010/aug/26/satoshi-kon-obituary|website=[[theguardian.com]]|publisher=[[Guardian Media Group]]|accessdate=25 August 2015|date=26 August 2010}}</ref>
| website         = [http://konstone.s-kon.net konstone.s-kon.net]
}}
{{Anime}}
{{Nihongo |'''Satoshi Kon'''|今 敏|Kon Satoshi|October 12, 1963 – August 24, 2010<ref>{{cite web | url = http://konstone.s-kon.net/modules/info/index.php?page=article&storyid=37 | script-title=ja:《今 敏 永眠のお知らせ》 |language=Japanese |accessdate=September 7, 2010}}</ref>}} was a Japanese film director, [[animator]], [[screenwriter]] and [[Mangaka|manga artist]] from [[Sapporo, Hokkaidō]] and a member of the [[Japanese Animation Creators Association]] (JAniCA).<ref name="JAniCAIncorporation">{{cite web |url=http://www.animenewsnetwork.com/news/2008-06-06/japan-animator-labor-group-legally-incorporates |title=Japan's Animator Labor Group Legally Incorporates - Anime News Network |accessdate=29 May 2009 |author= |last= |first= |authorlink= |coauthors= | date=2008-06-06 |work= |publisher=[[Anime News Network]] |location= |pages= |doi= |archiveurl= |archivedate= |quote= }}</ref> He was a graduate of the [[Graphic Design]] department of the [[Musashino Art University]]. He is sometimes credited as {{Nihongo|"'''Yoshihiro Wanibuchi'''"|鰐淵良宏|Wanibuchi Yoshihiro}} in the credits of ''[[Paranoia Agent]]''. He was the younger brother of guitarist and studio musician Tsuyoshi Kon.

==Biography==

===Early life===
Satoshi Kon was born on October 12, 1963.<ref name="Madhouse">{{cite web |url=http://www.madhouse.co.jp/column/mhpf/mhpf_no035.html |script-title=ja:マッドハウス・公式プロフィール | language=Japanese |accessdate=August 25, 2010 |publisher=[[Madhouse (company)|Madhouse]] |archiveurl=https://web.archive.org/web/20110616212422/http://www.madhouse.co.jp/column/mhpf/mhpf_no035.html|archivedate= June 16, 2011}}</ref> Due to his father's job transfer, Kon's education from the fourth elementary grade up to the second middle school grade was based in [[Sapporo]]. Kon was a classmate and close friend of manga artist Seihō Takizawa. While attending [[Hokkaido Prefectural Board of Education|Hokkaido Kushiro Koryo High School]], Kon aspired to become an animator.<ref name="KT profile">{{cite web |url=http://konstone.s-kon.net/modules/profile/ |title=Konstone 公式プロフィール |language=Japanese |accessdate=August 25, 2010}}</ref> His favorite works were ''[[Space Battleship Yamato]]'' (1974), ''[[Heidi, Girl of the Alps]]'' (1974), ''[[Future Boy Conan]]'' (1978) and ''[[Mobile Suit Gundam]]'' (1979),<ref name="interHomeTheatre">{{cite web|last=Giron |first=Aimee |date=December 7, 2005 |url=http://www.soundandvision.com/httalksto/1205talks/ |title=HT Talks To . . . FilmMaker Satoshi Kon |publisher=[[Sound & Vision (magazine)|Sound & Vision]] |accessdate=August 25, 2010}}</ref> as well as [[Katsuhiro Otomo]]'s ''[[Domu: A Child's Dream]]''.<ref name="interPerfectBlue">{{cite web |url=http://www.perfectblue.com/interview.html |title=Interview with Satoshi Kon, Director of ''Perfect Blue'' |accessdate=August 25, 2010|archiveurl=https://web.archive.org/web/20120402165610/http://www.perfectblue.com/interview.html|archivedate=April 2, 2012}}</ref> [[Yasutaka Tsutsui]] served as an influence on Kon's drawings. Kon graduated from the Graphic Design course of the [[Musashino Art University]] in 1982.<ref name="KT profile"/> During that time, Kon viewed numerous foreign films and enthusiastically read Yasutaka Tsutsui's books.<ref name="interPerfectBlue"/><ref name="interCatsuka">{{cite web |url=http://www.catsuka.com/focuson/itw_satoshikon |title=Interview de Satoshi Kon sur le site Catsuka |date=December 10, 2006 |accessdate=August 8, 2016 |language=French | archiveurl=https://web.archive.org/web/20111018052223/http://www.catsuka.com/focuson/itw_satoshikon | archivedate=2011-10-18 | deadurl=no }}</ref>

===Early career===
While in college, Kon made his debut as a manga artist with the short manga ''Toriko'' (1984) and earned a runner-up spot in the 10th Annual Tetsuya Chiba Awards held by ''[[Young Magazine]]'' ([[Kodansha]]).<ref name="KT profile"/><ref name="Tetsuya Chiba Prize">{{cite web |url=http://www.chibapro.co.jp/index.php?startIndex=30&tbl=award_yng&key= |script-title=ja:ちばてつや公式サイト |language=Japanese |accessdate=August 26, 2010}}</ref><ref name="interANN">{{cite web |url=http://www.animenewsnetwork.com/interview/2008-08-21/satoshi-kon |accessdate=24 October 2008 |last=Sevakis |first=Justin |date=August 21, 2008 |work=[[Anime News Network]] |  title=Interview: Satoshi Kon |archiveurl=https://web.archive.org/web/20160408215742/http://www.animenewsnetwork.com/interview/2008-08-21/satoshi-kon |archivedate=2016-04-08  | deadurl=no }}</ref> Afterward, he found work as Katsuhiro Otomo's assistant.<ref name="interANN"/><ref name="inter06">{{cite web |url=http://konstone.s-kon.net/modules/interview/index.php/content0007.html |date=March 1998 |script-title=ja:フランスから「パーフェクトブルー」に関するインタビュー |language=Japanese |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20120311213135/http://konstone.s-kon.net/modules/interview/index.php/content0007.html|archivedate=March 11, 2012}}</ref> After graduating from college in 1987,<ref name="KT profile"/> Kon authored the one-volume manga ''Kaikisen'' (1990) and wrote the script for Katsuhiro Otomo's live-action film ''World Apartment Horror''.<ref name="interANN"/> In 1991, Kon worked as an animator and layout artist for the animated film ''[[Roujin Z]]''.<ref name="KT profile"/><ref name="interANN"/> Kon worked as a supervisor for [[Mamoru Oshii]]'s ''[[Patlabor 2: The Movie]]'' along with other animated films.<ref name="KT profile"/> He then worked on the manga ''Seraphim: 266,613,336 Wings'' with Oshii, it was published in 1994 in [[Animage]].<ref name=osmond />{{rp|17}} In 1995, Kon served as the scriptwriter, layout artist and art director of the short film ''[[Magnetic Rose]]'', the first of three short films in Katsuhiro Otomo's omnibus ''[[Memories (1995 film)|Memories]]''.<ref name="KT profile"/><ref name="interANN"/> Kon's work afterward would be distinguished by the recurring theme of the blending of fantasy and reality.<ref>DVD ''Memories'' Interview</ref>

===Directing===
In 1993, Kon scripted and co-produced the fifth episode of the original video animation ''[[JoJo's Bizarre Adventure]]''.<ref name="interPerfectBlue"/> In 1997, Kon began work on his directorial debut ''[[Perfect Blue]]'' (based on [[Yoshikazu Takeuchi]]'s novel of the same name).<ref name="interTokyopop">{{cite web|last=Aguiar |first=Bill |date=April 25, 2007 |url=http://www.tokyopop.com/Robofish/tp_article/688419.html |title=Interview with Satoshi Kon |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20110414164311/http://www.tokyopop.com/Robofish/tp_article/688419.html|archivedate=April 14, 2011}}</ref> A suspense story centered on a pop idol, it was the first film by Kon to be produced by [[Madhouse (company)|Madhouse]].<ref name="Madhouse"/> Kon was initially unsatisfied with the original screenplay written by the author and requested to make changes to it.<ref name="interMidnighteye">{{cite web|last=Mes |first=Tom |url=http://www.midnighteye.com/interviews/satoshi-kon/ |title=INTERVIEW Satoshi Kon |date=February 11, 2002 |publisher=Midnight Eye |accessdate=August 26, 2010}}</ref><ref>{{cite web |url=http://konstone.s-kon.net/modules/interview/index.php/content0008.html |title=Interview 07 2004年6月 アメリカから、監督作品全般に関するインタビュー |language=Japanese |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20120311213158/http://konstone.s-kon.net/modules/interview/index.php/content0008.html|archivedate=March 11, 2012}}</ref> Aside from maintaining three elements of the novel ("idol", "horror", and "stalker"), Kon was allowed to make any changes he desired.<ref name="interMidnighteye"/> The screenplay was written by Sadayuki Murai,<ref name="interPerfectBlue"/> who worked in the idea of a blurred border between the real world and imagination.<ref name="interMidnighteye"/>

Following ''Perfect Blue'', Kon considered adapting the 1993 Yasutaka Tsutsui novel ''[[Paprika (novel)|Paprika]]'' into his next film. However, these plans were stalled when the distribution company for ''Perfect Blue'' (Rex Entertainment) went bankrupt.<ref name="interMidnighteye2">{{cite web|last=Gray |first=Jason |url=http://www.midnighteye.com/interviews/satoshi-kon-2/ |title=INTERVIEW Satoshi Kon Part2 |date=November 20, 2006 |publisher=Midnight Eye |accessdate=August 26, 2010}}</ref> Coincidentally, Kon's next work would also feature the theme of the blending of imagination and reality.<ref name="interMidnighteye"/> In 2002, Kon's second film, ''[[Millennium Actress]]'', was released to the public. The film centers on a retired actress who mysteriously withdraws from the public eye at the peak of her career. Having the same estimated budget as ''Perfect Blue'' (approximately 120,000,000 yen),<ref name="interCatsuka"/> ''Millennium Actress'' garnered higher critical and financial success than its predecessor and earned numerous awards. The screenplay was written by Sadayuki Murai,<ref name="interMidnighteye"/> who utilized a seamless connection between illusion and reality to create a "[[Trompe-l'œil]] kind of film".<ref name="interdvdvisionjapan">{{cite web |url=http://www.dvdvisionjapan.com/actress2.html |title=Director Satoshi Kon Interview DVJ2.0 |accessdate=August 26, 2010}}</ref> ''Millennium Actress'' was the first Satoshi Kon film to feature [[Susumu Hirasawa]], of whom Kon was a long-time fan, as composer.<ref>{{cite web |url=http://konstone.s-kon.net/modules/interview/index.php/content0025.html |title=Interview 23 2007年6月 アメリカから｢パプリカ｣について|archiveurl=https://web.archive.org/web/20120311213205/http://konstone.s-kon.net/modules/interview/index.php/content0025.html|archivedate=March 11, 2012}}</ref>

In 2003, Kon's third work, ''[[Tokyo Godfathers]]'', was announced. The film centers on a trio of homeless persons in Tokyo who discover a baby on Christmas Eve and set out to search for her parents. ''Tokyo Godfathers'' cost more to make than Kon's previous two films (with a budget of approximately 300,000,000 yen),<ref name="interCatsuka"/> and centered on the themes of homelessness and abandonment, with a comedic touch worked in.<ref name="interANN"/><ref name="interMidnighteye"/> The screenplay was written by [[Keiko Nobumoto]].<ref name="FicheTG">{{cite web |url=http://www.sonypictures.jp/archive/movie/worldcinema/tgf/ |title=東京ゴッドファーザーズ：オフィシャルサイト |accessdate=August 26, 2010}}</ref>

In 2004, Kon released the 13-episode television series ''[[Paranoia Agent]]'', in which Kon revisits the theme of the blending of imagination and reality, as well as working in additional social themes.<ref name="intergamestar">{{cite web |url=http://www.gamestar.com/11_04/pause/pause_disc_satoshikon.shtml |title=Interview with Satoshi Kon |publisher=Gamestar |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20100813052314/http://www.gamestar.com/11_04/pause/pause_disc_satoshikon.shtml|archivedate=August 13, 2010}}</ref> The series was created from an abundance of unused ideas for stories and arrangements that Kon felt were good but did not fit into any of his projects.<ref name="mousou">{{cite web |url=http://konstone.s-kon.net/modules/moso/index.php/content0005.html |title=妄想の産物 妄想の二｢総監督の謎｣ |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20120311213225/http://konstone.s-kon.net/modules/moso/index.php/content0005.html|archivedate=March 11, 2012}}</ref>

In 2006, ''[[Paprika (2006 film)|Paprika]]'' was announced, after having been planned out and materializing for several years. The story centers on a new form of psychotherapy that utilizes dream analysis to treat mental patients. The film was highly successful and earned a number of film awards. Kon summed up the film with {{Nihongo|"Kihonteki na ''story'' igai wa subete kaeta"|基本的なストーリー以外は全て変えた}}<ref name="interPremière">{{cite web |url=http://www.premiere.fr/Bandes-annonces/Video/Satoshi-Kon-ITW |title=Satoshi Kon-ITW-Interview |accessdate=August 26, 2010}}</ref>—roughly, "Everything but the fundamental story was changed." Much like Kon's previous works, the film focuses on the synergy of dreams and reality.<ref name="interCatsuka"/>

After ''Paprika'', Kon teamed up with Mamoru Oshii and [[Makoto Shinkai]] to create the 2007 [[NHK]] television production ''[[Ani*Kuri15]]'', for which Kon created the short ''Ohayō''.<ref>{{cite web | url=http://www.animenewsnetwork.com/news/2007-05-01/nhk-ani-kuri-15 | title=NHK Launches Ani-Kuri 15 | publisher=Anime News Network | date=1 May 2007 | accessdate=29 August 2013}}</ref> That same year, Kon helped establish and served as a member of the Japan Animation Creators Association (JAniCA).<ref>{{cite web | url=http://www.animenewsnetwork.com/news/2008-06-06/japan-animator-labor-group-legally-incorporates | title=Japan's Animator Labor Group Legally Incorporates | publisher=Anime News Network | date=6 June 2008 | accessdate=29 August 2013}}</ref>

===Health deterioration and death===
Following ''Ohayō'', Kon began work on his next film, ''[[Dreaming Machine]]''. In May 2010, Kon was diagnosed with terminal [[pancreatic cancer]]. Given half a year to live, Kon chose to spend the remainder of his life in his home. Shortly before his death Kon composed a final message, which was uploaded to his blog by his family upon his death. As Kon explained in the message, he chose not to make news of his rapidly advancing illness public, in part out of embarrassment at how drastically emaciated and ravaged his body had become. The result was that the announcement of his death was met with widespread shock and surprise, particularly given that Kon had shown no signs of illness at relatively recent public events, as the cancer progressed to a terminal state in a matter of months after being diagnosed.<ref>{{cite web |title=Anime News Network |url=http://www.animenewsnetwork.com/news/2010-08-24/award-winning-director-satoshi-kon-passes-away|accessdate=January 5, 2011}}</ref> Kon died on August 24, 2010 at the age of 46.<ref name="KONSTONE">{{cite web |url=http://konstone.s-kon.net/modules/notebook/archives/565 |title=公式ブログ｢KON'S TONE｣ |accessdate=August 25, 2010}}</ref><ref name='Itoh2010-08-26'>{{cite web | url = http://www.makikoitoh.com/journal/satoshi-kons-last-words | title = Satoshi Kon's last words | accessdate = 2010-11-11 | last = Itoh | first = Makiko | date = 2010-08-26 | work = Makiko Itoh: Not a Nameless Cat | quote = As far as I know there's no translation of the whole document into English out there, so here it is.}}</ref> After his death, Kon was mentioned among the ''Fond Farewells'' in [[Time (magazine)|TIME]]'s people of the year 2010. [[Darren Aronofsky]] wrote a [[eulogy]] to him, which was printed in {{Nihongo|''Satoshi Kon's Animation Works''|今敏アニメ全仕事}}, a Japanese retrospective book of his animation career.<ref>{{cite news|last=Corliss |first=Richard |publication-date=September 13, 2010 |url=http://content.time.com/time/specials/packages/article/0,28804,2036683_2036477_2036995,00.html |publisher=[[Time (magazine)|Time]] |title=Person Of The Year 2010 |date=December 15, 2010}}</ref><ref>{{cite web|url=http://www.animenewsnetwork.com/news/2011-05-26/satoshi-kon-book-adds-message-by-black-swan-aronofsky|title=Satoshi Kon Book Adds Message by Black Swan's Aronofsky (Updated)|publisher=Anime News Network}}</ref>

As of 2013, the completion of ''Dreaming Machine'' remains uncertain due to funding difficulties, with only 600 of the 1500 shots being animated. At Otakon 2012, Madhouse founder Masao Maruyama stated: "Unfortunately, we still don't have enough money. My personal goal is to get it within five years after his passing. I'm still working hard towards that goal."<ref>{{cite web|last=Sevakis|first=Justin|date=2012-07-28|url=http://www.animenewsnetwork.com/convention/2012/otakon/11|title=Masao Maruyama/MAPPA Q&A|publisher=[[Anime News Network]]|accessdate=2013-01-29}}</ref> In July 2015, Madhouse reported that ''Dreaming Machine'' remains in production but they are looking for a director to match Kon's abilities and similar vision.

==Themes==
When asked about his interest in female characters, Kon stated that female characters were easier to write because he is not able to know the character in the same way as a male character, and "can project my obsession onto the characters and expand the aspects I want to describe."<ref name="ca">{{cite book | title=Cinema Anime  - "Excuse Me, Who Are You?": Performance, the Gaze, and the Female in the Works of Kon Satoshi by Susan Napier | publisher=Palgrave Macmillan | author=Brown, Steven | year=September 2008 | pages=23–43 | isbn=978-0-230-60621-0}}</ref> With a frame of reference up to ''Tokyo Godfathers'', [[Susan J. Napier]] notes that while the theme of performance is the one obvious commonality in his works, she finds that the concept of the [[male gaze]] is the more important topic for discussion. Napier shows the evolution of Kon's use of the gaze from its restrictive and negative aspects in ''Magnetic Rose'' and ''Perfect Blue'', to a collaborative gaze in ''Millennium Actress'' before arriving at a new type of gaze in ''Tokyo Godfathers'' which revels in uncertainty and illusion.<ref name="ca" />

[[Dean DeBlois]] said, "Satoshi Kon used the hand-drawn medium to explore social stigmas and the human psyche, casting a light on our complexities in ways that might have failed in live action. Much of it was gritty, intense, and at times, even nightmarish. Kon didn't shy away from mature subject matter or live-action sensibilities in his work, and his films will always occupy a fascinating middle ground between 'cartoons' and the world as we know it."<ref>{{cite news|last=Solomon|first=Charles|title=Satoshi Kon dies at 46; Japanese anime director|url=http://articles.latimes.com/2010/aug/26/local/la-me-satoshi-kon-20100826|newspaper=Los Angeles Times|date=August 26, 2010}}</ref>

==Influences==
Satoshi Kon's most prominent influences were the works of [[Philip K. Dick]] and [[Yasutaka Tsutsui]].<ref name="interCatsuka"/><ref name="interTokyopop"/> Kon viewed various manga and anime up until high school.<ref name="interPerfectBlue"/> He was particularly fond of ''Space Battleship Yamato'', ''Future Boy Conan'', ''[[Galaxy Express 999]]'' (1978), ''Mobile Suit Gundam'' and ''[[Domu: A Child's Dream]]''.<ref name="interHomeTheatre"/><ref name="inter05">{{cite web |url=http://konstone.s-kon.net/modules/interview/index.php/content0006.html |title=Interview 05 1998年2月 アメリカから｢パーフェクトブルー｣に関するインタビュー |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20120311213049/http://konstone.s-kon.net/modules/interview/index.php/content0006.html|archivedate=March 11, 2012}}</ref> Western films also served as an influence, most notably [[George Roy Hill]]'s ''[[Slaughterhouse-Five (film)|Slaughterhouse-Five]]'' (1972), ''[[The City of Lost Children]]'' (1995) and the works of [[Terry Gilliam]] (particularly ''[[Time Bandits]]'' (1981), ''[[Brazil (1985 film)|Brazil]]'' (1985) and ''[[The Adventures of Baron Munchausen]]'' (1989)).<ref name="Madhouse"/><ref name="interPerfectBlue"/><ref name="interTokyopop"/> In addition to Gilliam, he also blogged about watching [[Monty Python]].<ref>{{cite web|url=http://konstone.s-kon.net/modules/notebook/archives/396 |title=NOTEBOOK »NOTEBOOK» ブログアーカイブ » 先月の傾向 - KON'S TONE |publisher=Konstone.s-kon.net |date= |accessdate=2013-05-25}}</ref>
He was fond of the works of [[Akira Kurosawa]], and worked in a reference to him in ''Paprika''.<ref name="interMidnighteye2"/> However he claimed to be largely unfamiliar with Japanese film in general.<ref name="interdvdvisionjapan"/><ref>{{cite web |url=http://konstone.s-kon.net/modules/interview/index.php/content0004.html   |title=Interview 03 2002年12月 カナダから、主に｢千年女優｣に関するインタビュー |accessdate=August 26, 2010|archiveurl=https://web.archive.org/web/20120311213055/http://konstone.s-kon.net/modules/interview/index.php/content0004.html|archivedate=March 11, 2012}}</ref>

== Filmography ==
{| class="wikitable sortable"
|-
! Year !! Title !! [[Film director|Director]] !! [[Screenwriter|Writer]] !! [[Animator]] !! Notes
|-
| 1991 
| ''[[World Apartment Horror]]'' 
| 
| {{yes}}
| 
| Feature film. Based on Kon's own manga of the same name.<ref name=cat>{{cite web | url=http://www.catsuka.com/satoshikon/en/about | title=A Tribute to Satoshi Kon | publisher=Catsuka | accessdate=6 December 2013}}</ref>
|-
| 1991 
|''[[Roujin Z]]''  
|  
| 
| {{yes}}
| Feature film. Background animator.<ref name=osmond />{{rp|16}}
|-
| 1992 
| ''[[Hashire Melos!]]''
| 
| 
| {{yes}} 
| Feature film. Layout animator.<ref name=cat />
|-
| 1993 
| ''[[Patlabor 2]]'' 
|  
|
| {{yes}}
| Feature film. Layout animator.<ref name=cat />
|-
| 1994 
| ''[[JoJo's Bizarre Adventure (OVA)|JoJo's Bizarre Adventure]]''
|
| {{yes}}
| {{yes}}
| OVA. Animator on episode 2. Director on episode 5.<ref name=cat />
|-
| 1995 
| ''[[Memories (1995 film)|Memories]]'' 
| 
| {{yes}} 
| {{yes}}
| Anthology film. Writer, background designer, and layout animator of ''Magnetic Rose''.<ref name=cat />
|-
| 1997 
| ''[[Perfect Blue]]'' 
| {{yes}}
| {{yes}} 
|
| Feature film. Director, script adapter.<ref name=osmond />{{rp|122}}
|-
| 1998
| ''[[Detatoko Princess]]'' 
| 
|  
| {{yes}}
| OVA.
|-
| 2001 
| ''[[Millennium Actress]]'' 
| {{yes}}
| {{yes}} 
|
| Feature film. Director, original story writer, and character designer.<ref name=osmond />{{rp|122}}
|-
| 2003 
| ''[[Tokyo Godfathers]]'' 
| {{yes}}
| {{yes}} 
|
| Feature film. Director and original story writer.<ref name=osmond />{{rp|123}}
|-
| 2004 
| ''[[Paranoia Agent]]'' 
| {{yes}}
| {{yes}} 
|
| TV series. Director, writer, and storyboards for the opening and episodes 1, 9 and 13.<ref name=osmond />{{rp|123}}
|-
| 2006 
| ''[[Paprika (2006 film)|Paprika]]'' 
| {{yes}}
| {{yes}}
|
| Feature film. Director and screenwriter.<ref name=osmond />{{rp|124}}
|-
| 2008 
| ''[[Good Morning (short)|Good Morning]]'' 
| {{yes}}
| {{yes}}
|
| Short film. Part of ''[[Ani*Kuri15]]''<ref name=cat />
|-
| TBA 
| ''[[Dreaming Machine]]'' 
| {{yes}}
| {{yes}}
|
| Feature film. Director and writer, production stopped after Kon's passing.
|}

== Bibliography ==
=== Manga ===
{| class="wikitable sortable"
|-
! Year !! Title !! Notes
|-
| 1984 || {{nihongo|''Toriko''|虜|}} || Manga debut and a [[doujinshi]] work. It won the 2nd place Tetsuya Chiba Award for "Superior Newcomer".<ref name=osmond />{{rp|14}} Published in English in the collection ''Dream Fossil''. It is not related to the Mitsutoshi Shimabukuro manga [[Toriko]].<ref>{{cite web | url=http://www.animenewsnetwork.com/news/2010-12-22/satoshi-kon-short-manga-stories-to-be-published | title=Satoshi Kon's Short Manga Stories to be Published (Updated) | publisher=Anime News Network | date=22 December 2010 | accessdate=20 August 2013}}</ref> 
|-
| 1990 || {{nihongo|''Tropic of the Sea''|海帰線|}} || Published in Young Magazine by [[Kodansha]].<ref name=osmond>{{cite book | title=Satoshi Kon: The Illusionist | publisher=Stonebridge Press | author=Osmond, Andrew | year=2009 | isbn=978-1933330747}}</ref>{{rp|15}} Published in English in 2013 by [[Vertical (company)|Vertical Comics]].
|-
| 1990 || ''[[Akira (manga)|Akira]]'' || Uncredited assistant artist.<ref name=osmond />{{rp|15}}
|-
| 1991 || {{nihongo|''[[World Apartment Horror]]''|ワールド・アパートメントホラー|}} || Adapted from the [[World Apartment Horror|film of the same name]] directed by [[Katsuhiro Otomo]] from a [[Keiko Nobumoto]] screenplay. After the main feature the volume collects three much shorter manga original to Kon: ''Visitors'', ''Waira'' and ''Joyful Bell'', which last prefigures ''[[Tokyo Godfathers]]''.
|-
| 1994 || {{nihongo|''Seraphim: 266613336 Wings''|セラフィム 2億6661万3336の翼|}} || An unfinished collaboration with [[Mamoru Oshii]] that first ran in the May 1994 issue to the November 1995 issue of [[Animage]].<!--Prologue in May 1994 Issue of Animage, Chapters 1 to 10 in June 1994 through March 1995 issues of Animage,  two month hiatus, Chapters 11 to 16 in June 1995 through November 1995 issues of Animage.--><ref name=osmond />{{rp|17}}<ref>{{cite web | url = http://konstone.s-kon.net/modules/works/index.php?content_id=5 | title = Kon's Tone Unreleased Comic |language=Japanese |accessdate=February 14, 2014}}</ref> Partially reprinted posthumously in a memorial supplement of [[Monthly Comic Ryū]] in 2010 and published in comic book form by [[Tokuma Shoten]] in December that same year. Published in English in 2015 by [[Dark Horse Comics]].<ref>{{cite web | url = http://konstone.s-kon.net/modules/info/index.php?page=article&storyid=42 | date= October 19, 2010| title =Kon's Tone 月刊COMICリュウ　12月号| trans_title= Kon's Tone December issue Monthly Comic Ryū |language=Japanese |accessdate=February 14, 2014}}</ref><ref>{{cite web | url=https://www.animenewsnetwork.com/news/2010-12-08/satoshi-kon-seraphim-opus-manga-reprinted | title=Satoshi Kon's Seraphim, Opus Manga Reprinted | publisher=Anime News Network | date=December 8, 2010 | accessdate=February 14, 2014}}</ref><ref>{{cite book |last1=Oshii |first1=Mamoru|last2=Kon|first2=Satoshi|date=December 4, 2014|pages=6, 231|script-title=ja:セラフィム 2億6661万3336の翼 |trans_title=Seraphim: 266,613,336 Wings |url=http://www.tokuma.jp/bookinfo/9784199502200 |language=Japanese |location=Tokyo |publisher=Tokuma Shoten |isbn=978-4-19-950220-0 |accessdate=February 14, 2014}}</ref>
|-
| 1995–1996 || ''[[Opus (manga)|Opus]]'' || An incomplete manga that was released bi-monthly in ''Comic Guys'' from 1995-1996.<ref>{{cite web | url=http://halcyonrealms.com/japan/opus-satoshi-kon-manga-book-review/ | title=Opus – Satoshi Kon Manga Book Review | website=Halcyon Realms | accessdate=December 6, 2013}}</ref> It was collected and re-released in December 2010.<ref>{{cite web | url=http://www.animate-onlineshop.jp/pn/%E3%80%90%E3%82%B3%E3%83%9F%E3%83%83%E3%82%AF%E3%80%91OPUS%28%E3%82%AA%E3%83%BC%E3%83%91%E3%82%B9%29%20%28%E4%B8%8A%29/pd/1049009/ | title=【コミック】OPUS(オーパス) (上) (Opus) | publisher=Anime Onlineshop | accessdate=6 December 2013}}</ref> Published in English in 2014 by [[Dark Horse Comics]].<ref>{{cite web | url=https://www.darkhorse.com/Books/25-876/Satoshi-Kon-s-OPUS-TPB | title=Satoshi Kon's OPUS TPB :: Profile | publisher=Dark Horse Comics| accessdate=February 17, 2014}}</ref>
|}

=== Other literary works ===
{| class="wikitable sortable"
|-
! Year !! Title !! Publisher !! ISBN !! Notes
|-
| 2002 || {{nihongo|Kon's Tone: The Road to "Millennium Actress"|コンズ・トーン: 「千年女優」への道|}} || Shobunsha Publications || {{ISBNT | 978-4794965462}} || Biography-type work documenting his journey to his second film.<ref name="Road Millennium Actress">{{cite web | url=http://www.monkeysfightingrobots.com/in-remembrance-of-satoshi-kon/ |title=In remembrance of Satoshi Kon |date=2015-08-24 |accessdate=2016-07-08}}</ref> Reissued in 2013.
|-
| 2002 || {{nihongo|Chiyoko: Millennial Actress|千年女優画報―『千年女優』ビジュアルブック|}} || Kawade Shobo Shinsha  || {{ISBNT | 978-4309905112}} || Artbook containing interviews with the cast and director as well as character design art by [[Madhouse (company)|Madhouse]] animation.<ref name="Art Millennium Actress">{{cite web | url=http://cdn.halcyonrealms.com/anime/the-art-of-satoshi-kons-millenium-actress/ |title=The Art of Satoshi Kon's Millennium Actress |website=Halcyon Realms |date=2009-05-29 |accessdate=2016-07-08}}</ref>
|-
| 2003 || {{nihongo|Tokyo Godfathers: Angel Book|東京ゴッドファーザーズ・エンジェルブック|}} || Takarajimasha || {{ISBNT | 978-4796636803}} || Artbook containing interviews with the cast and director as well as character and background art.<ref name="Art Tokyo Godfathers">{{cite web | url=http://cdn.halcyonrealms.com/japan/the-art-of-tokyo-godfathers-satoshi-kon/ |title=The Art of Tokyo Godfathers - Satoshi Kon |website=Halcyon Realms  |date=2009-07-08 |accessdate=2016-07-08}}</ref>
|-
| 2011 || {{nihongo|The Anime Works of Satoshi Kon|今 敏アニメ全仕事|}} || G.B. Co. Ltd. || {{ISBNT | 978-4901841948}} || Retrospective book containing interviews, character design sheets, and more.<ref name="Anime Works">{{cite web | url=http://cdn.halcyonrealms.com/animation/the-anime-works-of-satoshi-kon-book-review/ |title=The Anime Works of Satoshi Kon Book Review |website=Halcyon Realms  |date=2012-01-09 |accessdate=2016-07-29}}</ref>
|-
| 2012 || Kon's Tone II ||  ||  || Sold exclusively through ''Kon's Tone'' online store. Collection of essays and sketches including his farewell message.<ref name="Kon's Tone Tumblr">{{cite web | url=http://ca-tsuka.tumblr.com/post/29607466813/kons-tone-ii-satoshi-kon-notebook-collection-of |title=CATSUKA - KON’S TONE II Satoshi Kon notebook. Collection of... |website=[[Tumblr]]  |date=2012-08-17 |accessdate=2016-07-29}}</ref><ref name="Kon's Tone Crunchyroll">{{cite web | url=http://www.crunchyroll.com/anime-news/2012/08/17/kons-tone-ii-segundo-volumen-de-materiales-de-satoshi-kon |title='Kon’s Tone II', segundo volumen de materiales de Satoshi Kon |website=[[Crunchyroll]] |language=Spanish |date=2012-08-17 |accessdate=2016-07-29}}</ref>
|-
| 2015 || {{nihongo|Perfect Blue Storyboard Collection|今敏 絵コンテ集 PERFECT BLUE|}} || Fukkan || {{ISBNT | 978-4835451411}} || Complete storyboards for ''[[Perfect Blue]]'', smaller print originally bundled with Limited Edition of the 2008 release of ''[[Perfect Blue]]'' on home video.<ref name="Perfect Blue Storyboards">{{cite web | url=http://cdn.halcyonrealms.com/anime/perfect-blue-storyboard-collection-art-book-review/ |title=Perfect Blue Storyboard Collection Art Book Review |website=Halcyon Realms  |date=2015-04-01 |accessdate=2016-07-08}}</ref>
|-
| 2015 || Dream Fossil: The Complete Stories of Satoshi Kon || [[Vertical (company)|Vertical Comics]] || {{ISBNT | 978-1941220245}} || Originally published in Japan in 2011. A collection of his short manga stories published between 1984 and 1989. Includes ''Toriko'', ''Waira'', and ''Joyful Bell''.
|-
| 2015 || The Art of Satoshi Kon || [[Dark Horse Comics]]  || {{ISBNT | 978-1616557416}} || Released in Japan as ''Kon's Work 1982-2010'' in 2014. Artbook spanning entire career, contains character design for unreleased film ''Dreaming Machine'' .<ref name="Art of Satoshi Kon">{{cite web | url=http://cdn.halcyonrealms.com/animation/kons-work-1982-2010-kon-satoshi-art-book-review/ |title=Kon's Work 1982-2010 - Kon Satoshi Art Book Review |website=Halcyon Realms  |date=2014-01-07 |accessdate=2016-07-08}}</ref>
|}

== Further reading ==
* [[Andrew Osmond|Andrew, Osmond]]. ''Satoshi Kon: The Illusionist'', Stone Bridge Press, 2009, ISBN 1-93333-074-0

==References==
{{Reflist|2}}

==External links==
* [http://s-kon.net/ Personal website] {{ja icon}}
* {{IMDb name|0464804|Satoshi Kon}}
* {{jmdb name|id=0122550|name=今敏 (Satoshi Kon)}}
* {{ann|people|1161}}
* [http://www.sf-encyclopedia.com/entry/kon_satoshi Entry] in the [[Encyclopedia of Science Fiction]]
* [http://makikoitoh.com/journal/satoshi-kons-last-words English translation of Satoshi Kon's last words]

{{Satoshi Kon}}

{{Authority control}}

{{DEFAULTSORT:Kon, Satoshi}}
[[Category:1963 births]]
[[Category:2010 deaths]]
[[Category:Japanese anime directors]]
[[Category:Animated film directors]]
[[Category:Deaths from cancer in Japan]]
[[Category:Deaths from pancreatic cancer]]
[[Category:Japanese film directors]]
[[Category:Madhouse (company) people]]
[[Category:People from Kushiro, Hokkaido]]
[[Category:Storyboard artists]]
