The '''Brothers of the Baladi''' is a [[World music]] band based in [[Portland, Oregon]], USA, that plays both traditional Middle Eastern music, and also combines traditional Middle Eastern and western sounds and instruments for a unique [[Worldbeat]] sound.  Band leader/percussionist/vocalist Michael Beach provides lyrics in Arabic, Turkish, [[Persian language|Persian]], French, Spanish, Kurdish, Armenian and English, and the band features many traditional Middle Eastern instruments including [[oud]], [[Saz (musical instrument)|saz]], [[Mizmar (instrument)|mizmar]], [[midjwiz]], [[arghool]], [[doumbek]], [[riq]], [[Def (instrument)|def]], [[Tar (disambiguation)|tar]], [[bendir]] and [[davul]].<ref name=Richmond>{{cite news|last=Peters|first=Rob|title=Brothers of the Baladi:  Middle East meets West|newspaper=The Richmond News|date=2005-07-27|page=23}}</ref>

== History ==

=== Formation of the Band ===

The Brothers of the Baladi was formed by band leader/percussionist/vocalist Michael Beach in [[Yuma, Arizona]] in 1975 to back up local Belly Dancer Zamara.  Beach, who had fallen in love with the sounds of the Arabic, Turkish, Greek, and Persian music in Zamara's collection of cassette tapes and LPs,<ref name=Thrust>{{cite journal|title=Brothers of the Baladi|journal=Thrust|date=April 1994|volume=1|issue=4|pages=3–4}}</ref> initially began accompanying the tapes on a hand drum,<ref name=scene>{{cite news|last=Naftaly|first=Stanley|title=Mid East Indie|newspaper=Music Scene|date=2007-07-20|page=12}}</ref> but soon recruited Colby Girard, Joshua Mertz and Pete Walter to try to emulate the music with a mix of doumbek, bongos, dulcimer, 12-string guitar, recorder, and clarinet.<ref name=Chronicles>{{cite journal|last=Serqet|title=Brothers of the Baladi|journal=The Chronicles|date=July 2005|volume=3|issue=3|pages=2 }}</ref><ref name=Oregonian>{{cite news|last=Levy|first=Shawn|title=Band holds Night of Love|newspaper=The Sunday Oregonian|date=11/4/2011|page=E2}}</ref>  The band got its name when, one night as Zamara was being introduced by El Hakim (Josh Mertz), he spontaneously introduced the band as "The Brothers of the Baladi."  <ref name="Chronicles" /><ref name=Boise>{{cite news|last=Daigle|first=Rachel|title=Baladi Brothers, Aug. 24, The Bouquet|newspaper=Boise Weekly|date=2007-08-22|page=26}}</ref>

A chance turn of events in 1978 found Beach and his hand drums on The Main Stage of the [[Vancouver Folk Music Festival]], performing with the world-renowned composer/conductor [[David Amram]]. This performance was a life-changing experience for Beach. Later that same year, Beach was in Santa Cruz, CA where he heard and was inspired by the widely popular Middle Eastern band, Sirocco.<ref name=BoBsite>{{cite web|title=Brothers of the Baladi - About|url=http://baladi.com/history.html|work=Brothers of the Baladi|accessdate=2012-1-22}}</ref>

In 1978, Beach moved to Oregon and connected with multi-instrumentalist Joseph Pusey.  Beach and Pusey resurrected the Brothers of the Baladi name to perform at a local benefit, and the band began playing regularly at The Mouse Trap in Grants Pass, Oregon.<ref name="Chronicles" />

=== 1982-1989 ===

They released their first album, ''Dance with Gladness'', in 1982, featuring 15 tracks comprising two full traditional belly dance routines, played on traditional instruments including saz, oud, nai, mizmar, and hand drums.<ref name=BLD01>{{cite AV media notes |title=Dance With Gladness|others=Brothers of the Baladi|year=1982|publisher= |id=Baladi Productions|ref=BLD01}}</ref>

This album, originally available only on tape and LP, was re-released in January 2012 on CD.<ref name=Allegro>{{cite web|last=Allegro Music|title=Dance with Gladness|url=http://www.allegro-music.com/online_catalog.asp?sku_tag=BLD31|work=Brothers of the Baladi Discography|accessdate=2012-1-22}}</ref>

The group followed up in 1983 with ''Food of Love'', which again offered music for two full belly dance routines, and featured traditional Egyptian, Armenian, and Persian songs played on traditional instruments.<ref name=BLD02>{{cite AV media notes |title=Food of Love |others=Brothers of the Baladi |year=1983 |type=CD Liner |publisher=Brothers of the Baladi |id=BLD02}}</ref>

The band's next album, ''Beyond the Tenth'', was released in 1989, and included popular Arabic, Turkish, Greek, Armenian and Israeli songs sung in their native tongues, backed by traditional saz, oud, clarinet, zurna, and hand drums.<ref name=BLD03>{{cite AV media notes |title=Beyond the Tenth |others=Brothers of the Baladi |year=1989 |type=CD Liner |publisher=Brothers of the Baladi |id=BLD03}}</ref>  Sales of this album were charted as far away as New Zealand.<ref name=Nightlife>{{cite news|first=Attilo|title=Brothers of the Baladi Cross Cultures|newspaper=Downtown Portland Nightlife|date=1989-05-15|page=22}}</ref>

=== 1989-1999 ===

Bass guitarist J. Michael Kearsey joined the band in 1989.<ref name=DA>{{cite web|title=Brothers of the Baladi|url=http://dynamicartists.com/artists/brothers-of-the-baladi2|work=Dynamic Artists|accessdate=2012-1-22}}</ref>  Pusey left in 1991, and Middle Eastern musicians Ishmael (kanoon) and Boujemma Razgui (oud, nay, violin and vocals) joined shortly after.<ref name="BoBsite" />  By this time, the band was starting to fuse Middle Eastern and western instruments and styles.  In 1994, the group teamed up with Stephen Skaggs to release "Further Journeys," featuring traditional acoustic Arabic and Turkish music.<ref name=BLD04>{{cite AV media notes |title=Further Journeys |others=Brothers of the Baladi |year=1994 |type=CD Liner |publisher=Brothers of the Baladi |id=BLD04}}</ref>

In 1995, Ishmael and Razgui left the group and Beach and Kearsey were joined by Attillo (keyboards) and Multi instrumentalist Tariq Banzi (oud, guitar, nay, bouzouki, dumbek, drums and riq). Banzi would later team up with his wife, Julia Banzi, to form the [[Al-Andalus Ensemble|Al Andalus Ensemble]]. 

During this time, the Brothers had started to feel confined by traditional Middle Eastern music, and sought to broaden their spectrum by adding other strains of world music, including Celtic, reggae, and Afro-Cuban pop, to klezmer, Cajun, and rock.<ref name="LATimes">{{cite news|last=Roos|first=John|title=It Started With a Belly Dance|newspaper=Los Angeles Times|date=1998-06-27}}</ref> The band teamed up with former Santana percussionist [[Michael Shrieve]], who produced the Brothers' album ''Eye on the World'', which includes Arabic, Persian, Turkish, and Celtic songs, some original compositions, and a Middle Eastern stylized cover of the [[Rolling Stones]]' [[Paint it Black]] <ref name="BLD05">{{cite AV media notes |title=Eye on the World |others=Brothers of the Baladi |year=1995 |type=CD Liner |publisher=Brothers of the Baladi |id=5 |location=Portland, Oregon }}</ref> that was picked up as interlude music by NPR.<ref name="Recordnet">{{cite web|last=Sauro|first=Tony|title=American immerses himself in exotic sounds of brothers of the baladi|url=http://www.recordnet.com/apps/pbcs.dll/article?AID=/20090730/A_ENTERTAIN/907300312/-1/A_ENTERTAIN07|work=Recordnet.com|accessdate=2012-1-22|date=2009-07-30}}</ref>

''Heart of the Beast'' was released in 1998, introducing keyboard player Geoff George.  This album features a cover of [[The Yardbirds]]' [[Over Under Sideways Down]], as well as Middle Eastern, Celtic, Afro Pop, and Reggae mixes.<ref name=BLD06>{{cite AV media notes|title=Heart of the Beast|others=Brothers of the Baladi|year=1998|type=CD Liner|publisher=Brothers of the Baladi|id=BLD06}}</ref>

=== Holiday Album ===

''A Time of Peace'', the band's popular holiday album, followed in 1999, featuring an instrumental collection of 15 popular Christmas songs performed on kanoon, ney, oud, bouzouk, zurna, darbuka/tabla, zarb, riq, def & davul, arranged by Ishmael and Beach.<ref name=BLD07>{{cite AV media notes|title=A Time of Peace|others=Brothers of the Baladi|year=1999|type=CD Liner|publisher=Brothers of the Baladi|id=BLD07}}</ref>

=== 2000-2008 ===

The band hit a minor stumbling block in the wake of September 11, 2001, when several venues decided they did not want to risk hosting a Middle Eastern influenced band and cancelled their bookings.  In response, the Brothers of the Baladi sponsored its own benefit concert in its home town of Portland, Oregon, with all proceeds to benefit the American Red Cross and the disaster relief effort.  The event, which featured music of Armenia, Turkey, Persia, Lebanon, and Egypt, was a success, and the positive response helped the band to weather the initial negative effects.<ref name="Oregonian" /> Despite this, the band says it has never experienced any serious negative political attention, and has generally been considered ambassadors for peace in the Middle East by presenting the music in a way that North American people can relate to, while also being well received by Middle Eastern people who enjoy hearing the songs they grew up with.<ref name="Richmond" /><ref name=Willamette>{{cite news|title=Brothers of the Baladi|newspaper=Willamette Week|date=2008-04-23|page=35}}</ref>

In 2002, the band was joined by [[John Bilezikjian]] and Sulieman El Coyote Feldthouse of the band [[Kaleidoscope (US band)]] and Stephen Skaggs for the album ''Hope'', which features acoustic and electric songs in English, Arabic, Turkish, and Armenian.  Feldthouse performed on the Armenian folk song ''Laz,'' which is essentially the same piece as Kaleidoscope's hit ''Seven Ate Suite''.<ref name=BLD08>{{cite AV media notes|title=Hope|others=Brothers of the Baladi|year=2002|type=CD Liner|publisher=Brothers of the Baladi|id=BLD08}}</ref>

The Brothers of the Baladi recorded ''Presence of the Past'' in 2005, in which they returned to traditional Arabic, Turkish, and Armenian styles, featuring renowned kanoon player Ishmael, Boujemma Razgui, and Skaggs, as well as Brenda Erickson, Geoff George, and Daniel Eshoo.<ref name=BLD09>{{cite AV media notes|title=Presence of the Past|others=Brothers of the Baladi|year=2005|type=CD Liner|publisher=Brothers of the Baladi|id=BLD10}}</ref>

In 2008, the band further evolved its Middle Eastern/World Music sound with the album ''Just Do What's Right'', including covers of classic tunes by [[Buffalo Springfield]], [[Chris Rea]], and [[Neil Young]].  Beach provides vocals in Arabic, [[Persian language|Persian]], Spanish, French, English and the old Anglo Saxon dialect of Northern England.<ref name=BLD11>{{cite AV media notes|title=Just Do What's Right|others=Brothers of the Baladi|year=2008|type=CD Liner|publisher=Brothers of the Baladi |id=BLD11}}</ref> This album was nominated for a Grammy Award in 2008, and served as the basis for a tour to the United Kingdom that same year.<ref name="DA" /><ref name=Tribune>{{cite news|last=Mitchell|first=Barbara|title=Brothers of the Baladi|newspaper=Portland Tribune|date=2008-04-22|page=B2}}</ref>

=== 2009-2016 ===

The band continued touring in the United States and England, but in 2014 announced that they were stepping back from touring for a time to focus more on a new album. ''Gravity of Love'' was released in early 2016, and represents the band's first foray into electronic music and programming.

== Recent and Current Projects ==

Brothers of the Baladi continues to tour in the United States and United Kingdom. In March 2016 they released their 12th album, ''Gravity of Love'', which was nominated for the Grammy Rock Album of the Year in 2016. Their music has also been featured on NPR, TV's Lost (ABC), Sexual Healing (Showtime), and Core Culture (USA Network).  TBN's documentary, "Holy Family of Egypt - Jesus in Egypt" premiered in Cairo, also features the Brothers' music.  The band's music also is included in over 50 belly dance compilations.<ref name="BoBsite" />

== Discography ==

*''Dance with Gladness'' (1982)
*''Food of Love'' (1983)
*''Beyond the Tenth'' (1989)
*''Further Journeys'' (1993)
*''Eye on the World'' (1995)
*''Heart of the Beast'' (1998)
*''A Time of Peace'' (1999)
*''Hope'' (2002)
*''Presence of the Past'' (2005)
*''Just Do What's Right'' (2008)
*''Gravity of Love'' (2016)

== Current members ==
Band members are:<ref name="BoBsite" />
* '''Michael Beach''': Lead vocals, doumbek / Arabic tabla, mizmar, midjwiz, def, riq and Davul
* '''J. Michael Kearsey''': vocals, electric bass and percussion
* '''Clark Salisbury''': vocals, acoustic and electric oud, saz and guitars
* '''Charles Pike''': drum kit, vocals, doumbek, and percussion

==References==
{{reflist|2}}

[[Category:American world music groups]]
[[Category:Culture of Portland, Oregon]]
[[Category:Musical groups established in 1975]]