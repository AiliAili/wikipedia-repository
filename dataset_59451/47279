{{multiple issues|
{{Underlinked|date=May 2015}}
{{Orphan|date=May 2015}}
}}

A '''creep-testing machine'''  measures the [[Creep (deformation)|creep]] (the tendency of a material after being subjected to high levels of stress, e.g. high temperatures, to change its form in relation to time) of an object. It is a device that measures the alteration of a material after it has been put through different forms of stress. Creep machines are important to see how much strain (load) an object can handle under pressure, so engineers and researchers are able to determine what materials to use. The device generates a creep time-dependent curve by calculating the steady rate of creep in reference to the time it takes for the material to change. Creep machines are primarily used by engineers to determine the stability of a material and its behaviour when it is put through ordinary stresses.

==Background==
The first creep testing machines were created in 1948 in Britain to test materials for aircraft to see how they would stand in high altitudes, temperature and pressure.<ref name ="archive">A.  I. Smith, D. Murray, M. F. Day, "Creep Testing Equipment-Design Features and Control"; Volume 180 Pt. 3A, 1965-66, pg.303-307</ref> The machines were first developed to further calculate and understand the steady rate of creep in materials. Creep is the tendency of a material to change form over time after facing high temperature and stress. Creep increases with temperature and it is more common when a material is exposed to high temperatures for a long time or at the melting point of the material. Creep machines are used to understand the creep of materials and determine which type can do the job better, which is important when making and designing materials for everyday uses. They most commonly test the creep of alloys and plastics for the understanding of their properties and advantages of one material's use over another.<ref>W. Blum, Materials Science and Engineering: A; Volumes 319-321, December 2001, pg.735-740</ref>

==Design==
Researchers look to test objects with a creep machine to understand the process of metallurgy and the physical mechanical properties of a metal, test the development of alloys, receive data from the loads that are derived and to find out whether a sample or material is within the boundary of what they are testing.<ref name="archive"/> The basic design of a creep machine is the furnace, loading device and support structure.

The main type of creep testing machine that is most commonly used is a constant load creep testing machine.The constant load creep machine consists of a loading platform, foundation, fixture devices and furnace. The fixture devices are the grips and pull rods.<ref>J.B. Grishaber,"A Novel Computer Controlled Constant Stress Lever Arm Creep Testing Machine", Review of Scientific Instruments; Jul97, Vol. 68 Issue 7, pg.2812</ref>

*'''Load platform''' or sometimes called load hanger is where the object will endure pressure at a constant rate.<ref name="MoMoh"/>
*'''Grips''' are used to hold the material you are testing in a certain position. Position is important because if the alignment is off, the machine will deliver inaccurate readings of the creep  of the material.<ref name="MoMoh"/>
*'''Dial Gauge''' is used to measure the strain. It is the object that captures the movement of the object in the machine. The load beam transfers the movement from the grip to the dial gauge.<ref name="MoMoh"/>
*'''Heating Chamber''' is what surrounds the object and maintain the temperature that the object is subjected to.<ref name="MoMoh"/>

==Applications==
Creep machines are most commonly used in experiments to determine how efficient and stable a material is. The machine is used by students and companies  to create a  creep curve on how much pressure and stress a material can handle. The machine is able to calculate the stress rate, time and pressure.<ref name="MoMoh">John J. MOMOH, Department of Mechanical Engineering, Federal Polytechnic, Ado-Ekiti, "Modification and Performance Evaluation of a Low Cost Electro-Mechanically Operated Creep Testing Machine", http://ljs.academicdirect.org/A16/083_094.htm</ref>

Creep testing has three different applications in the industry:
#'''Displacement-Limited applications ''': the size must be precise and there must be little errors or tendency to change.This is most commonly found in turbine rotors in jet engines.
# '''Rupture Limited applications''': in this application the break cannot occur to the material but there can be various dimensions as the material goes through creep. High pressure tubes are examples of them.
#'''Stress relaxation limited application ''': the tension at the beginning becomes more relaxed and the tension will continue to relax as the time goes by, such as cable wires and bolts.<ref name="ppt">Dr. Sabbah Ataya, "Creep Testing Machines", 2008, http://www.slideshare.net/ea2m/creep-testing-machines-presentation</ref>

===Graphing of Creep===
Creep is dependent on time so the curve that the machine generates is a time vs. strain graph. The slope of a creep curve is the creep rate dε/dt<ref name="MoMoh"/> The trend of the curve is an upward slope. The graphs are important to learn the trends of the alloys or materials used and by the production of the creep-time graph, it is easier to determine the better material for a specific application.

====[[Stages of Creep]]====
There are three stages of creep: 
*'''Primary Creep''': the initial creep stage where the slope is rising rapidly at first in a short amount of time. After a certain amount of time has elapsed, the slope will begin to slowly decrease from its initial rise.
*'''Steady State Creep''': the creep rate is constant so the line on the curve shows a straight line that is a steady rate.
*'''Tertiary Creep''': the last stage of creep when the object that is being subjected to pressure is going to reach its breaking point. In this stage, the object's creep continuously increases until the object breaks. The slope of this stage is very steep for most materials.
By examining the three stages above, scientists are able to determine the temperature and interval in which an object will be disturbed once exposed to the load. Some materials have a very small secondary creep state and may go straight from the primary creep to the tertiary creep state. This is dependent on the properties of the material that is being tested. This is important to note because going straight to the tertiary state causes the material to break faster from its form.<ref name="lab">Experiment 4 Lab, Center for Reliability of Ceramics, Department of Mechanical Engineering, University of Houston, http://www.me.uh.edu/ceramics/3445LabManual/4.CREEP.pdf</ref>

A linear graph  denotes that the material under stress is gradually deforming and this would be harder to track at what level of stress an object can handle. This would also mean that the material would not have distinct stages,which would make object's breaking point would be less predictable. This is a disadvantage to scientists and engineers when trying to determine the level of creep the object can handle.<ref>Narayana, V. J. S., K. Balasubramaniam, and R. V. Prakash. "Detection And Prediction Of Creep-Damage Of Copper Using Nonlinear Acoustic Techniques." AIP Conference Proceedings 1211.1,2010, pg.1410-1417</ref>

== References ==
{{Reflist}}

<!--- Categories --->
[[Category:Articles created via the Article Wizard]]
[[Category:Measuring instruments]]