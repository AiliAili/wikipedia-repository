{{for|other actions at Savannah|Battle of Savannah (disambiguation)}}
{{Infobox military conflict
| conflict    = Battle of Savannah
| partof      = the [[American Revolutionary War]]
| image       = [[File:ArchibaldCampbell.jpg|200px]]
| caption     = Lieutenant Colonel [[Archibald Campbell (British Army officer)|Archibald Campbell]] (portrait by [[George Romney (painter)|George Romney]], c. 1792)
| date        = December 29, 1778
| place       = [[Savannah, Georgia]]
| coordinates = {{coord|32|03|03|N|81|06|14|W |type:event_region:US-GA |display=inline,title}}
| result      = British victory
| combatant1  = {{flag|United States|1777}}
| combatant2  = {{flagcountry|Kingdom of Great Britain}}
| commander1  = [[Robert Howe (Continental Army officer)|Robert Howe]]
| commander2  = [[Archibald Campbell (British Army officer)|Archibald Campbell]]
| strength1   = 850<br/>[[infantry]] and [[militia]]<br/>4 [[field gun|artillery pieces]]<ref name="Wilson79">Wilson, p. 79</ref>
| strength2   = 3,100<br/>infantry and militia<br/>unknown artillery<ref name="Wilson79"/>
| casualties1 = 83 killed<br>11 wounded<br>453 captured<ref name="Wilson80">Wilson, p. 80</ref>
| casualties2 = 7 killed<br>17 wounded<ref name="Wilson80"/>
| campaignbox = {{Campaignbox American Revolutionary War: Southern 1775-1779}}
}}

The '''Capture of Savannah''', or sometimes the '''First Battle of Savannah''' (because of a [[Siege of Savannah|siege]] in 1779), was an [[American Revolutionary War]] battle fought on December 29, 1778 pitting local American [[Patriot (American Revolution)|Patriot]] [[militia]] and [[Continental Army]] units, holding the city, against a [[Kingdom of Great Britain|British]] invasion force under the command of [[Lieutenant Colonel]] [[Archibald Campbell (British Army officer)|Archibald Campbell]]. The British seizure of the city led to an extended occupation and was the opening move in the British [[Southern theater of the American Revolutionary War|southern strategy]] to regain control of the rebellious Southern provinces by appealing to the relatively strong [[Loyalist (American Revolution)|Loyalist]] sentiment there.

General [[Henry Clinton (American War of Independence)|Sir Henry Clinton]], the commander-in-chief of the British forces based in New York City, dispatched Campbell and a 3,100-strong force from New York to capture Savannah, and begin the process of returning Georgia to British control.  He was to be assisted by troops under the command of Brigadier General [[Augustine Prevost]] that were marching up from [[Saint Augustine, Florida|Saint Augustine]] in [[East Florida]].  After landing near Savannah on December 23, Campbell assessed the American defenses, which were comparatively weak, and decided to attack without waiting for Prevost.  Taking advantage of local assistance  he successfully flanked the American position outside the town, captured a large portion of [[Major General]] [[Robert Howe (Continental Army officer)|Robert Howe]]'s army, and drove the remnants to retreat into [[South Carolina]].

Campbell and Prevost followed up the victory with the capture of [[Sunbury, Georgia|Sunbury]] and an expedition to [[Augusta, Georgia|Augusta]].  The latter was only occupied by Campbell for a few weeks before he retreated back to Savannah, citing insufficient Loyalist and [[Native Americans in the United States|Native American]] support and the threat of Patriot forces across the [[Savannah River]] in [[South Carolina]].  The British held off a Franco-American [[Siege of Savannah|siege]] in 1779, and held the city until late in the war.

==Background==
{{main|Southern theater of the American Revolutionary War}}
In March 1778, following the [[Saratoga campaign|defeat]] of a [[British Army during the American War of Independence|British army]] at [[Battle of Saratoga|Saratoga]] and the consequent entry of France into the [[American Revolutionary War]] as an [[Franco-American Alliance|American ally]], Lord [[George Germain]], the British secretary responsible for the war, wrote to Lieutenant General [[Henry Clinton (American War of Independence)|Sir Henry Clinton]] that capturing the southern colonies was "considered by the [[George III of the United Kingdom|King]] as an object of great importance in the scale of the war".<ref name="Morrill40">Morrill, p. 40</ref>  Germain's instructions to Clinton, framed as recommendations, were that he should abandon [[Philadelphia]] and then embark on operations to recover Georgia and the Carolinas, while making diversionary attacks against [[Virginia]] and [[Maryland]].<ref>Wilson, p. 61</ref>

===British preparations===
[[Image:GAMap-doton-Savannah 200px.PNG|thumb|right|Location of Savannah in modern Georgia]]
In June and July 1778 Clinton successfully removed his troops from Philadelphia back to New York.<ref>Wilson, p. 60</ref>  In November, after dealing with the threat of a French fleet off New York and [[Newport, Rhode Island]], Clinton turned his attention to the south.  He organized a force of about 3,000 men in New York and sent orders to [[Saint Augustine, Florida|Saint Augustine]], the capital of [[East Florida]], where Brigadier General [[Augustine Prevost]] was to organize all available men and Indian agent [[John Stuart (loyalist)|John Stuart]] was to rally the local [[Creek people|Creek]] and [[Cherokee people|Cherokee]] warriors to assist in operations against Georgia.<ref name=Piecuch132>Piecuch, p. 132</ref>  Clinton's basic plan, first proposed by [[Thomas Brown (loyalist)|Thomas Brown]] in 1776, began with the capture of the capital of Georgia, [[Savannah, Georgia|Savannah]].<ref>Cashin, p. 73</ref>

Clinton gave command of the detachment from New York to Lieutenant Colonel [[Archibald Campbell (British Army officer)|Archibald Campbell]].  The force consisted of two battalions (the 1st and 2nd) of the [[71st Regiment of Foot, Fraser's Highlanders|71st Regiment of Foot]], the [[Hessian (soldiers)|Hessian]] regiments von Wöllwarth and von Wissenbach, and four [[Loyalist (American Revolution)|Loyalist]] provincial units: one battalion from the [[New York Volunteers]], two from [[DeLancey's Brigade]], and one from [[New Jersey Volunteers (Skinner's Greens)|Skinner's Brigade]].  Campbell sailed from New York on November 26 and arrived off [[Tybee Island]], near the mouth of the [[Savannah River]], on December 23.<ref>Wilson, p. 71</ref>

===American defenses===
[[File:MajGenRobertHowe.jpg|left|thumb|170px|Major General Robert Howe]]
The state  of Georgia was defended by two separate forces.  Units of the [[Continental Army]] were under the command of General [[Robert Howe (Continental Army officer)|Robert Howe]], who was responsible for the defense of the entire South, while the state's militia companies were under the overall command of Georgia Governor [[John Houstoun]].  Howe and Georgia authorities had previously squabbled over control of military expeditions against Prevost in East Florida, and those expeditions had failed.<ref>Wilson, p. 67</ref>  These failures led the [[Continental Congress]] to decide in September 1778 to replace Howe with Major General [[Benjamin Lincoln]], who had successfully negotiated militia participation in events surrounding the British [[Battles of Saratoga|defeat at Saratoga]].<ref>Wilson, p. 69</ref>  Lincoln had not yet arrived when word reached Howe that Clinton was sending troops to Georgia.

During November 1778 British raids into Georgia became more and more threatening to the state's population centers.<ref>Wilson, pp. 70–72</ref>  Despite the urgency of the situation, Governor Houstoun refused to allow Howe to direct the movements of the Georgia militia.  On November 18, Howe began marching south from [[Charleston, South Carolina]] with 550 Continental Army troops, arriving in Savannah late that month.  He learned that Campbell had sailed from New York on December 6.  On December 23 sails were spotted off [[Tybee Island]].  The next day, Governor Houstoun assigned 100 Georgia militia to Howe.<ref>Russell, p. 101</ref>

A war council decided to attempt a vigorous defense of Savannah, in spite of the fact that they were likely to be significantly outnumbered, hoping to last until Lincoln's troops arrived.  Due the large number of potential landing points, Howe was forced to hold most of his army in reserve until the British had actually landed.<ref name=Wilson72/>

==Battle==
The place Campbell selected for landing was Girardeau's Plantation, located about {{convert|2|mi|km}} below the city.<ref name=Piecuch132/><ref name=Wilson72>Wilson, p. 72</ref>  When word reached Howe that the landing had started on December 29, he sent a company of Continentals to occupy the bluffs above the landing site.  Campbell realized that the bluffs would need to be controlled before the majority of his forces could land, and dispatched two companies of the 71st Regiment to take control of them.  The Continentals opened fire at about {{convert|100|yd|m}}; the British, rather than returning fire, advanced rapidly with bayonets fixed, denying the Continentals a second shot.  The Continentals retreated, having killed four and wounded five at no cost to themselves.  By noon, Campbell had landed his army and began to proceed cautiously toward the city.<ref>Wilson, pp. 73–74</ref>

[[Image:SavannahCapture1778.jpg|thumb|right|An 1891 copy of a map depicting the action (note that south is to the top)]]
Howe held a council that morning, and ground was chosen at which to make a stand.  About one-half mile (0.7&nbsp;km) south of the city he established a line of defense in the shape of an open V, with the ends anchored by swampy woods.  On the left Howe placed Georgia Continentals and militia under [[Samuel Elbert]], while on the right he put South Carolina Continentals and militia under [[Isaac Huger]] and [[William Thomson (military officer)|William Thomson]].  The line was supported by four pieces of [[field artillery]], and light infantry companies guarded the flanks.  Most of Howe's troops, including the Continentals, had seen little or no action in the war.<ref>Russell, pp. 101–102</ref>

When Campbell's advance companies spotted Howe's line around 2:00 pm, the main body stopped short of the field and Campbell went to see what he was up against.  He viewed Howe's defenses as essentially sound, but a local slave told him that there was a path through the swamp on Howe's right.<ref>Wison, p. 74</ref><ref>Piecuch, p. 133</ref>  Campbell ordered Sir James Baird to take 350 light infantry and 250 New York Loyalists and follow the slave through the swamp, while he arrayed his troops just out of view in a way that would give the impression he would attempt a [[flanking maneuver]] on Howe's left.  One of his officers climbed a tree to observe Baird's progress.  True to the slave's word, the trail came out near the Continental barracks, which had been left unguarded; the Continentals were unaware they had been flanked.  When they reached position, the man in the tree signaled by waving his hat, and Campbell ordered the regulars to charge.<ref>Wilson, p. 75</ref>

The first sounds of battle Howe heard were musket fire from the barracks, but these were rapidly followed by cannon fire and the appearance of charging British and German troops on his front.  He ordered an immediate retreat, but it rapidly turned into a rout.  His untried troops hardly bothered to return fire, some throwing down their weapons before attempting to run away through the swampy terrain.  Campbell reported that "It was scarcely possible to come up with them, their retreat was rapid beyond Conception."<ref name="Wilson76">Wilson, p. 76</ref>  The light infantry in the Continental rear cut off the road to Augusta, the only significant escape route, forcing a mad scramble of retreating troops into the city itself.  The Georgia soldiers on the right attempted to find a safe crossing of Musgrove Creek, but one did not exist, and many of the troops were taken prisoner.<ref>Russell, p. 103</ref>  Soldiers who did not immediately surrender were sometimes bayoneted.  Colonel Huger managed to form a rear-guard to cover the escape of a number of the Continentals.  Some of Howe's men managed to escape to the north before the British closed off the city, but others were forced to attempt swimming across Yamacraw Creek; an unknown number drowned in the attempt.<ref name="Wilson77"/>

==Aftermath==
Campbell gained control of the city at the cost to his forces of seven killed and seventeen wounded, not including the four men killed and five wounded during preliminary skirmishing.  Campbell took 453 prisoners, and there were at least 83 dead and 11 wounded from Howe's forces.  When Howe's retreat ended at [[Purrysburg, South Carolina]] he had 342 men left, less than half his original army.  Howe would receive much of the blame for the disaster, with [[William Moultrie]] arguing that he should have either disputed the landing site in force or retreated without battle to keep his army intact.<ref name="Wilson77"/>  He was exonerated in a [[court martial]] that inquired into the event, although the tribunal pointed out that Howe should have made a stand at the bluffs or more directly opposed the landing.<ref>Wilson, p. 78</ref>

[[Image:JamesWrightBySoldi.jpg|thumb|left|upright|Royal Governor [[James Wright (governor)|James Wright]], portrait by [[Andrea Soldi]]]]
General Prevost arrived from East Florida in mid-January, and shortly after sent Campbell with 1,000 men to take Augusta.  Campbell occupied the frontier town against minimal opposition, but by then General Lincoln had begun to rally support in South Carolina to oppose the British.<ref>Russell, p. 104</ref>  Campbell abandoned Augusta on February 14, the same day a Loyalist force en route to meet him was defeated in the [[Battle of Kettle Creek]].  Although Patriot forces following the British were ambushed in the March 3 [[Battle of Brier Creek]], the Georgia backcountry remained in Patriot hands.<ref>Russell, pp. 105–106</ref>

Campbell wrote that he would be "the first British officer to [rend] a star and stripe from the flag of Congress".<ref name="Wilson77">Wilson, p. 77</ref>  Savannah was used as a base to conduct coastal raids which targeted areas from [[Charleston, South Carolina]] to the Florida coast.  In the fall of 1779, a combined French and American [[Siege of Savannah|siege]] to recapture Savannah failed with significant casualties.<ref name="Evacuation">{{cite web|url=http://www.georgiaencyclopedia.org/nge/Article.jsp?id=h-2709|title=Revolutionary War in Georgia|publisher=Georgia Encyclopedia|accessdate=2 August 2009}}</ref>  Control of Georgia was formally returned to its royal governor, [[James Wright (governor)|James Wright]], in July 1779,<ref>{{cite journal|last1=Brooking|first1=Greg|title='Of Material Importance': Governor James Wright and the Siege of Savannah|journal=Georgia Historical Quarterly|date=Winter 2014|volume=98|issue=4|url=http://search.ebscohost.com/login.aspx?direct=true&db=a9h&AN=110131794&site=eds-live&scope=site|accessdate=10 June 2016}}</ref> but the backcountry would not come under British control until after the 1780 [[Siege of Charleston]].<ref>Coleman, pp. 82–84</ref>  Patriot forces recovered Augusta [[Siege of Augusta|by siege]] in 1781, but Savannah remained in British hands until 11 July 1782.<ref>Coleman, pp. 85–86</ref>
{{clear}}

==Notes==
{{reflist|30em}}

==References==
{{refbegin}}
*{{cite book|last=Cashin|first=Edward|title=The King's Ranger: Thomas Brown and the American Revolution on the Southern Frontier|publisher=Fordham University Press|year=1999|location=New York|isbn=978-0-8232-1907-0|oclc=246304277}}
*{{cite book|last=Coleman|first=Kenneth|title=A History of Georgia|publisher=University of Georgia Press|year=1991|location=Athens, GA|isbn=978-0-8203-1269-9|oclc=21975722}}
*{{cite book|last=Morrill|first=Dan|title=Southern Campaigns of the American Revolution|publisher=Nautical & Aviation Publishing|year=1993|location=Baltimore, MD|isbn=978-1-877853-21-0|oclc=231619453}}
*{{cite book|last=Piecuch|first=Jim|title=Three peoples, one king : Loyalists, Indians, and Slaves in the Revolutionary South, 1775–1782|publisher=University of South Carolina Press|year=2008|location=Columbia, SC|isbn=978-1-57003-737-5|oclc=185031351}}
*{{cite book|title=The Southern Strategy: Britain's Conquest of South Carolina and Georgia, 1775–1780|first=David K|last=Wilson|publisher=University of South Carolina Press|year=2005|isbn=1-57003-573-3|location=Columbia, SC|oclc=232001108}}
{{refend}}

{{good article}}

[[Category:Battles of the American Revolutionary War|Savannah (1778)]]
[[Category:Battles involving the United States|Savannah (1778)]]
[[Category:Battles involving Great Britain|Savannah (1778)]]
[[Category:History of Savannah, Georgia]]
[[Category:Georgia (U.S. state) in the American Revolution|Savannah (Capture of)]]
[[Category:Conflicts in 1778]]
[[Category:1778 in the United States]]
[[Category:1778 in Georgia (U.S. state)]]
[[Category:December 1778 events]]