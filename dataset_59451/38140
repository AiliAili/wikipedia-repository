{{for|the World War I-era ship|SMS Blücher}}
{| {{Infobox ship begin}}
{{Infobox ship image
| Ship image   = [[File:Bundesarchiv DVM 10 Bild-23-63-09, Kreuzer "Blücher".jpg|300px]]
| Ship caption = ''Blücher'' while on trials
}}
{{Infobox ship career
| Hide header = 
| Ship country =[[Nazi Germany]]
| Ship flag ={{shipboxflag|Nazi Germany|naval}}
| Ship name = ''Blücher''
| Ship namesake = [[Gebhard Leberecht von Blücher]]
| Ship laid down = 15 August 1936
| Ship launched = 8 June 1937
| Ship commissioned = 20 September 1939
| Ship fate = Sunk in the [[Battle of Drøbak Sound]] on 9 April 1940
}}
{{Infobox ship characteristics
| Hide header = 
| Header caption = 
| Ship class ={{sclass-|Admiral Hipper|cruiser}}
| Ship displacement| Ship displacement =
*Design:
**{{convert|16170|MT|lk=on|abbr=on}}
*Full load:
**{{convert|18200|LT|abbr=on}}
| Ship length ={{convert|203.20|m|ftin|0|abbr=on}} [[Length overall|overall]]
| Ship beam   ={{convert|22|m|ftin|0|abbr=on}}
| Ship draft = Full load: {{convert|7.20|m|abbr=on}}
| Ship propulsion =
* 3 × [[Blohm + Voss|Blohm & Voss]] steam turbines
* 3 × three-blade propellers
* {{convert|132000|shp|MW|abbr=on}}
| Ship speed ={{convert|32|kn|lk=in}}
| Ship range = 
| Ship complement =
*    42 officers
* 1,340 enlisted men
| Ship armament =
*  8 × [[20.3 cm SK C/34 naval gun|{{convert|20.3|cm|in|abbr=on}}]] guns
* 12 × {{convert|10.5|cm|in|abbr=on}} guns
* 12 × {{convert|3.7|cm|in|abbr=on}} guns
*  8 × {{convert|2|cm|in|abbr=on}} guns (20 × 1)
*  6 × {{convert|53.3|cm|in|0|abbr=on}} [[torpedo tube]]s
| Ship armor =
* Belt:         {{convert| 70|to|80|mm|abbr=on}}
* Armor deck:   {{convert| 20|to|50|mm|abbr=on}}
* Turret faces: {{convert|105|mm|abbr=on}}
| Ship aircraft = 3 aircraft
| Ship aircraft facilities = 1 catapult
| Ship notes = 
}}
|}

'''''Blücher''''' was the second of five {{sclass-|Admiral Hipper|cruiser|0}} [[heavy cruiser]]s of [[Nazi Germany]]'s ''[[Kriegsmarine]]'', built after the rise of the [[Nazi Party]] and the repudiation of the [[Treaty of Versailles]]. Named for [[Gebhard Leberecht von Blücher]], the Prussian victor of the [[Battle of Waterloo]], the ship was laid down in August 1936 and launched in June 1937. She was completed in September 1939, shortly after the outbreak of [[World War II]]. After completing a series of [[sea trials]] and training exercises, the ship was pronounced ready for service with the fleet on 5 April 1940.

Assigned to Group 5 during the [[Operation Weserübung|invasion of Norway]] in April 1940, ''Blücher'' served as ''Konteradmiral'' [[Oskar Kummetz]]'s [[flagship]]. The ship led the flotilla of warships into the [[Oslofjord]] on the night of 8 April, to seize [[Oslo]], the capital of Norway. Two old {{convert|28|cm|abbr=on}} coastal guns in the [[Oscarsborg Fortress]] engaged the ship at very close range, scoring two hits.{{sfn|Binder & Schlünz|p=90}}
Two torpedoes fired by land-based torpedo batteries struck the ship, causing serious damage. A major fire broke out aboard ''Blücher'', which could not be contained. After a magazine explosion, the ship sank, with major loss of life. The wreck remains on the bottom of the Oslofjord.

== Construction ==
{{main article|Admiral Hipper-class cruiser}}
[[File:Admiral Hipper ONI.jpg|thumb|left|Recognition drawing of an ''Admiral Hipper''-class cruiser]]

''Blücher'' was ordered by the ''Kriegsmarine'' from the ''[[Deutsche Werke]]'' shipyard in [[Kiel]].{{sfn|Gröner|p=65}} Her keel was laid on 15 August 1936,{{sfn|Williamson|p=22}} under construction number 246.{{sfn|Gröner|p=65}} The ship was launched on 8 June 1937, and was completed slightly over two years later, on 20 September 1939, the day she was commissioned into the German fleet.{{sfn|Gröner|p=67}} The commanding admiral of the ''Marinestation der Ostsee'' (Baltic Naval Station), Admiral [[Conrad Albrecht]], gave the christening speech. Frau Erdmann, widow of ''Fregattenkapitän'' Alexander Erdmann, former commander of {{SMS|Blücher}}, performed the christening.{{sfn|Koop & Schmolke|p=113}} As built, the ship had a straight [[Stem (ship)|stem]], though after her launch this was replaced with a [[clipper]] bow increasing the overall length to {{convert|205.90|m|ft|sp=us}}.{{sfn|Koop & Schmolke|p=13}} A raked funnel cap was also installed.{{sfn|Williamson|p=35}}

As launched, ''Blücher'' was {{convert|202.80|m|ft|sp=us}} [[length overall|long overall]], had a beam of {{convert|21.30|m|ft|abbr=on}} and a maximum draft of {{convert|7.74|m|ft|abbr=on}}.{{sfn|Koop & Schmolke|p=13}} The ship had a design displacement of {{convert|16170|MT|abbr=on}} and a full load displacement of {{convert|18200|LT|abbr=on}}. ''Blücher'' was powered by three sets of geared [[steam turbine]]s, which were supplied with steam by twelve ultra-high pressure oil-fired [[boiler]]s. The ship's top speed was {{convert|32|kn|lk=in}}, at {{convert|132000|shp|lk=in}}.{{sfn|Gröner|p=65}} As designed, her standard complement consisted of 42 officers and 1,340 enlisted men.{{sfn|Gröner|p=66}}

''Blücher''{{'}}s primary armament was eight [[20.3 cm SK C/34 naval gun|{{convert|20.3|cm|in|abbr=on|1}} SK L/60]] guns mounted in four twin [[gun turret]]s, placed in [[superfire|superfiring pairs]] forward and aft.{{efn|name=gun nomenclature}} Her anti-aircraft battery consisted of twelve {{convert|10.5|cm|abbr=on}} L/65 guns, twelve {{convert|3.7|cm|abbr=on}} guns, and eight {{convert|2|cm|abbr=on}} guns. The ship would also have carried a pair of triple {{convert|53.3|cm|abbr=on}} torpedo launchers abreast of the rear superstructure. She had four triple {{convert|53.3|cm|abbr=on}} torpedo launchers, all on the main deck next to the four FLAK range finders.{{sfn|Koop & Schmolke|p=22}}{{sfn|Binder & Schlünz|p=97}} The ship was equipped with three [[Arado Ar 196]] seaplanes and one catapult.{{sfn|Gröner|p=66}} ''Blucher'' never had more than two seaplanes on board, and en route to Oslo one had to rest on the catapult as one of the hangars was used for storing bombs and torpedoes.{{sfn|Koop & Schmolke|p=115}} ''Blücher''{{'}}s [[belt armor|armored belt]] was {{convert|70|to|80|mm|abbr=on}} thick; her upper deck was {{convert|12|to|30|mm|abbr=on}} thick while the main armored deck was {{convert|20|to|50|mm|abbr=on}} thick. The main battery turrets had {{convert|105|mm|abbr=on}} thick faces and 70&nbsp;mm thick sides.{{sfn|Gröner|p=65}}

== Service history ==
[[File:Sjøsetting av Blücher.jpg|thumb|''Blücher'' launching at Kiel, 8 June 1937]]
''Blücher'' spent the majority of November 1939 [[fitting out]] and finishing additional improvements. By the end of the month, the ship was ready for [[sea trials]]; she steamed to [[Gotenhafen]] in the [[Baltic Sea]].{{sfn|Williamson|pp=23–24}} The trials lasted until mid-December, after which the ship returned to Kiel for final modifications. In January 1940, she resumed her exercises in the Baltic, but by the middle of the month, severe ice forced the ship to remain in port. On 5 April, she was deemed to be ready for action, and was therefore assigned to the forces participating in the invasion of Norway.{{sfn|Williamson|p=24}}

=== Operation ''Weserübung'' ===
{{main article|Operation Weserübung}}

On 5 April 1940, ''Konteradmiral'' [[Oskar Kummetz]] came aboard the ship while she was in [[Swinemünde]]. An 800-strong detachment of ground troops from the [[163rd Infantry Division (Germany)|163rd Infantry Division]] also boarded. Three days later, on 8 April, ''Blücher'' left port, bound for Norway; she was the flagship for the force that was to seize Oslo, the Norwegian capital. Organized as Group 5 of the invasion force{{sfn|Rohwer|p=18}} she was accompanied by the heavy cruiser [[German cruiser Deutschland|''Lützow'']], the [[light cruiser]] {{ship|German cruiser|Emden||2}}, and several smaller escorts. While steaming through the [[Kattegat]] and [[Skagerrak]], the British [[submarine]] {{HMS|Triton|N15|2}} spotted the convoy and fired a spread of torpedoes; the Germans successfully evaded the torpedoes, however, and proceeded with the mission.{{sfn|Williamson|p=24}}

[[File:Bundesarchiv Bild 101II-MO-0683-07, Leichter Kreuzer 'Emden'.jpg|thumb|left|''Blücher'' ''en route'' to Norway, as seen from the light cruiser [[German cruiser Emden|''Emden'']]]]

Night had fallen by the time the German flotilla reached the approaches to the [[Oslofjord]]. Shortly after 23:00 (Norwegian time) the flotilla was spotted by the Norwegian patrol boat {{HNoMS|Pol III||2}}. The German torpedo boat [[Type 23 torpedo boat|''Albatros'']] attacked ''Pol III'' and set her on fire, but not before the Norwegian patrol boat raised the alarm with a radio report of being attacked by unknown warships.{{sfn|Tamelander & Zetterling|p=72}} At 23:30 (Norwegian time) the south battery on Rauøy spotted the flotilla in the searchlight and fired two warning shots.{{sfn|Fjeld et al.|p=179}} Five minutes later, the guns at the Rauøy battery fired four rounds at the approaching Germans, but visibility was poor and no hits were scored.{{sfn|Fjeld et al.|p=179}} The guns at Bolærne fired only one warning shot at 23:32. Before ''Blücher'' could be targeted again, she was out of the firing sector of these shore guns and was seen no more by them after 23:35.{{sfn|Fjeld et al.|p=180}}

The German flotilla steamed on at a speed of {{convert|12|kn}}.{{sfn|Lunde|p=220}} Shortly after midnight (Norwegian time), an order from the Commanding Admiral to extinguish all lighthouses and navigation lights was broadcast over the [[NRK]] (''Norsk rikskringkasting'') [Norwegian Broadcasting Corporation].{{sfn|Berg|p=9}} The German ships had been ordered to fire only in the event they were directly fired on first.{{sfn|Williamson|p=24}} Between 00:30 and 02:00, the flotilla stopped and 150 infantrymen of the landing force were transferred to the escorts ''R17'' and ''R21'' (from ''Emden'') and ''R18'' and ''R19'' (from ''Blücher'').{{sfn|Binder & Schlünz|p=74}}

The R-boats were ordered to engage Rauøy, Bolærne and the naval port and city of [[Horten]].{{sfn|Binder & Schlünz|p=74}} Despite the apparent loss of surprise, the ''Blücher'' proceeded further into the fjord to continue with the timetable to reach Oslo by dawn. At 04:40, Norwegian searchlights again illuminated the ship and at 04:21 the {{convert|28|cm|abbr=on}} guns of [[Oscarsborg Fortress]] opened fire on ''Blücher'' at very close range, beginning the [[Battle of Drøbak Sound]] with two hits on her port side.{{sfn|Williamson|p=24}} The first was high above the bridge, hitting the battle station for the commander of the anti-aircraft guns, killing AO II ''Kapitänleutnant'' Hans-Erich Pochhammer.{{sfn|Binder & Schlünz|p=92}} The main range finder in the top of the battle mast was knocked out of alignment, but ''Blücher'' had four more major rangefinders (B-turret, on the bridge roof, the aft battle station (Nachtstand) and C-turret) and many smaller {{clarify|date=August 2015}} on the bridge and the four rangefinder stations for the AA. The commander in D-turret, ''Oberstückmeister'' Waldeck, stated that the first 28&nbsp;cm hit had no influence on the battle capability of the 20.3&nbsp;cm guns.{{sfn|Binder & Schlünz|p=119}} ''Blücher'' immediately returned fire.

===Fire starts===
{{main article|Battle of Drøbak Sound}}

The second 28&nbsp;cm shell struck near the aircraft hangar and started a major fire. As the fire spread, it detonated explosives carried for the infantry, hindering firefighting efforts.{{sfn|Binder & Schlünz|p=92}} The explosion set fire to the two Arado seaplanes on board: one on the catapult and the other in one of the hangars.{{sfn|Binder & Schlünz|p=93}} The explosion also probably punched a hole in the armored deck over turbine room 1. Turbine 1 and generator room 3 stopped for lack of steam and only the outboard shafts from turbine room 2/3 were operational.{{sfn|Binder & Schlünz|p=126}}

The Germans were unable to locate the source of the gunfire. ''Blücher'' increased speed to {{convert|32|kn}} in an attempt to get past the Norwegian guns.{{sfn|Williamson|p=24}} The {{convert|15|cm|abbr=on}} guns on [[Drøbak]], some {{convert|400|yd|abbr=on}} on ''Blücher''{{'}}s starboard side, opened fire as well.{{sfn|Williamson|p=33}} At a distance of {{convert|500|m}} ''Blücher'' entered the narrows between Kopås and Hovedbatteriet (the main battery) at Kaholmen. The Kopås battery ceased firing at ''Blücher'' and engaged the next target, ''Lützow'', scoring multiple hits.{{sfn|Fjeld et al.|p=189}} First engineer ''Leitende Ingenieur Fregattenkapitän Dip. Ing.'' Karl Thannemann wrote in his report that the hits from the guns on Drøbak, which were fired on the starboard side, were all between section IV and X in a length of {{convert|75|m}} amidships, between B-turret and C-turret. However, all damage were on the port side.{{sfn|Binder & Schlünz|p=83}} The shells must have traversed most of the superstructure before exploding. 
[[File:28 cm gun at Oscarsborg Fortress.jpg|thumb|One of the 28&nbsp;cm guns at Oscarsborg Fortress]]

After the first salvo from the 15&nbsp;cm batteries in Drøbak, the steering from the bridge was disabled. ''Blücher'' had just passed Drøbakgrunnen (Drøbak shallows) and was in a turn to port. The commander got her back on track by using the side shafts, but she lost speed.{{sfn|Koop & Schmolke|p=126}} Normally the rudder is controlled electrically from the bridge to the motors forward of the ''Handsteuerraum'' (hand steering room) deep under the armored deck, forward of the rudder. In an emergency it can be switched within seconds to manual steering, but orders from the bridge to the rudder may be delayed.{{sfn|Koop & Schmolke|p=42}} At 04:34 Norwegian land-based torpedo batteries scored two hits on the ship.{{sfn|Williamson|p=33}} The targeting device in the torpedo battery was very primitive. The speed of the torpedo was known and set, but the speed of the target had to be adjusted by [[Dead reckoning]] (guessing).{{r|Hovland}}

According to Admiral Kummetz' report, the first torpedo hit Kesselraum 2 (boileroom 2, just under the funnel) and the second hit Turbinenraum 2/3 (the turbine room for the side shafts). Boiler 1 had already been destroyed by gunfire. Only one boiler remained, but the steam pipes through boiler 1 and 2 and turbine room 2/3 had been damaged and turbine 1 for the main shaft lost its power.{{sfn|Koop & Schmolke|p=126}} By 04:34, the ship had been severely damaged, but had successfully passed through the firing zone; the Norwegian guns could no longer bear on her. The 15&nbsp;cm guns in the Kopås battery were all standing in open positions with a wide sector of firing. After the torpedo hits, ''Blücher'' was still within range. The battery asked for orders, but Eriksen concluded: ''The fortress has served its purpose''.<ref>Lislegaard, Othar; Børte, Torbjørn:''Skuddene som reddet Norge?'', H.Aschehaoug &co (W. Nygaard) Oslo 1975, p 42</ref> With all propellers stopped, about 60% of the electric power gone, Blucher quickly lost speed and at about 04:50 dropped anchors at Askoholmene.

===Sinking===
{{multiple image
 | align = right
 | direction = vertical
 | footer  = ''Blücher'' sinking in [[Drøbak Sound]]
 | width = 200
 | image1 = Senkingen av Blücher (5654566265).jpg
 | image2 = BildeB3.jpg
 | image3 = German soldiers and Blücher sinking.jpg
}}
After passing the gun batteries, the crew of ''Blücher'', including the personnel manning the guns, were tasked with fighting the fire. By that time she had taken on a list of 18&nbsp;degrees, although this was not initially problematic. The fire eventually reached one of the ship's 10.5&nbsp;cm ammunition magazines between turbine room 1 and turbine room 2/3, which exploded violently. The blast ruptured several bulkheads in the engine rooms and ignited the ship's fuel stores.{{sfn|Williamson|p=33}} The battered ship slowly began to capsize and the order to abandon ship was given.{{sfn|Williamson|pp=33–34}} ''Blücher'' rolled over and sank at 07:30, with significant casualties.{{sfn|Williamson|p=34}} Naval historian Erich Gröner states that the number of casualties is unknown,{{sfn|Gröner|p=67}} but Henrik Lunde gives a loss of life figure ranging between 600 and 1,000 soldiers and sailors.{{sfn|Lunde|p=220}} Jürgen Rohwer meanwhile states that 125 seamen and 195 soldiers died in the sinking.{{sfn|Rohwer|p=19}}
[[File:BlucherAnker.JPG|thumb|left|upright|One of ''Blücher''{{'}}s anchors is now at [[Aker Brygge]] in Oslo]]

The loss of ''Blücher'' and the damage done to ''Lützow'' caused the German force to withdraw. The ground troops were landed on the eastern side of the fjord; they proceeded inland and captured the Oscarborg Fortress by 09:00 on 10 April. They then moved on to attack the capital. Airborne troops captured the [[Oslo Airport, Fornebu|Fornebu Airport]] and completed the encirclement of the city, and by 14:00 on 10 April it was in German hands. The delay caused by the temporary withdrawal of ''Blücher''{{'}}s task force, however, allowed the Norwegian government and royal family to escape the city.{{sfn|Lunde|p=220}}

''Blücher'' remains at the bottom of the Drøbak Narrows, at a depth of {{convert|35|fathom|lk=on}}.{{sfn|Gardiner & Chesneau|p=229}} The ship's screws were removed in 1953, and there have been several proposals to raise the wreck since 1963, but none have been carried out.{{sfn|Gröner|p=67}} When ''Blücher'' left Germany, she had about {{convert|2670|m3}} of oil on board. She expended some of the fuel en route to Norway, and some was lost in the sinking, but she was constantly leaking oil. In 1991 the leakage rate increased to {{convert|50|l|sp=us}} per day, threatening the environment. The Norwegian government therefore decided to remove as much oil as possible from the wreck. In October 1994 the company Rockwater AS, together with deep sea [[Underwater diving|divers]] drilled holes in 133 fuel tanks and removed {{convert|1000|MT|abbr=on}} of oil; 47 fuel bunkers were unreachable and may still contain oil. After being run through a cleaning process, the oil was sold. The oil extraction operation provided an opportunity to recover one of ''Blücher''{{'}}s two [[Arado 196]] aircraft. The plane was raised on 9 November 1994 and is currently at the ''[[Flyhistorisk Museum, Sola]]'' aviation museum near [[Stavanger]].{{sfn|Binder & Schlünz|p=180}}

The shipwreck was protected as a war memorial on 16 June 2016, but also protected by law by [[Norwegian Directorate for Cultural Heritage]] for those who actually have their burial at the bottom of the fjord.  The intention was to protect the ship from wreck looters.<ref>[https://www.nrk.no/kultur/freder-_blucher_-for-a-hindre-vrakplyndring-1.13000719 "Blucher" protected by law to prevent looting] [[Norwegian broadcasting corporation|NRK]] 6/16/2016 {{no icon}}</ref>

==Footnotes==
{{commons category|Blücher (ship, 1937)}}

'''Notes'''

{{notes
| notes =

{{efn
| name = gun nomenclature
| "L/60" denotes the length of the gun in terms of [[Caliber (artillery)|calibers]]. The length of 60 caliber gun is 60 times greater than it is wide in diameter.
}}

}}

'''Citations'''

{{reflist
| colwidth = 20em
| refs =

{{refn
| name=Hovland
| A.n. Hovland;''Centralsikteapparat for torpedobatteriet'', Verdens Gang 13. April and 6. May 1953. (Comments by the inventor of the targeting device)
}}

}}

==References==
* {{cite book
  | last = Berg
  | first = Ole F
  | year = 1997
  | language = Norwegian
  | title = I skjærgården og på havet, Marinens krig 8. april 1940&nbsp;– 8 mai 1945
  | publisher = Marinens krigsveteranforening
  | isbn = 978-82-993545-2-3
  | ref ={{sfnRef|Berg}}
  }}
* {{cite book
  | last1 = Binder
  | first1 = Frank
  | last2 = Schlünz
  | first2 = Hans Hermann
  | year = 1990
  | language = German
  | title = Schwerer Kreuzer Blücher
  | edition = first
  | publisher = Koehlers Verlagsgesellschaft
  | isbn = 978-3-7822-0487-3
  | ref ={{sfnRef|Binder & Schlünz}}
  }}
* {{cite book
  | last1 = Fjeld
  | first1 = Odd T
  |author2=Tor Jørgen Melien |author3=Jan Egil Fjørtoft |author4=Tor Georg Monsen |author5=Reidar Lauritz Godø |author6=Robert Eichinger
   | year = 1999
  | title = Klar til strid, Kystartilleriet gjennom århundrene&#124;Utgitt av Kystartilleriets Offisersforening
  | language = Norwegian
  | isbn = 978-82-995208-0-5
  | ref ={{sfnRef|Fjeld et al.}}
  }}
* {{cite book
  | editor1-last = Gardiner
  | editor1-first = Robert
  | editor2-last = Chesneau
  | editor2-first = Roger
  | year = 1980
  | title = Conway's All the World's Fighting Ships, 1922–1946
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-913-9
  | url = https://books.google.com/books?id=bJBMBvyQ83EC&printsec=frontcover
  | ref ={{sfnRef|Gardiner & Chesneau}}
  }}
* {{cite book
  | last = Gröner
  | first = Erich
  | year = 1990
  | title = German Warships: 1815–1945
  | publisher = Naval Institute Press
  | location = Annapolis
  | isbn = 978-0-87021-790-6
  | ref ={{sfnRef|Gröner}}
  }}
* {{Cite book
  | last1 = Koop
  | first1 = Gerhard
  | last2 = Schmolke
  | first2 = Klaus-Peter
  | title = Die Schweren Kreuzer der Admiral Hipper-Klasse
  | trans_title = The Heavy Cruisers of the Admiral Hipper Class
  | location = Bonn, Germany
  | publisher = Bernard & Graefe Verlag
  | language = German
  | year = 1992
  | isbn = 978-3-7637-5896-8
  | ref ={{sfnRef|Koop & Schmolke}}
}}
* {{cite book
  | last = Lunde
  | first = Henrik O.
  | year = 2010
  | title = Hitler's Pre-Emptive War: The Battle for Norway, 1940
  | publisher = Casemate Publishers
  | location = Havertown, PA
  | isbn = 978-1-935149-33-0
  | ref ={{sfnRef|Lunde}}
  }}
* {{cite book
  | last = Rohwer
  | first = Jürgen
  | authorlink = Jürgen Rohwer
  | year = 2005
  | title = Chronology of the War at Sea, 1939–1945: The Naval History of World War Two
  | publisher = US Naval Institute Press
  | location = Annapolis
  | isbn = 978-1-59114-119-8
  | ref ={{sfnRef|Rohwer}}
  }}
* {{cite book
  | last1 = Tamelander
  | first1 = Michael
  | last2 = Zetterling
  | first2 = Niklas
  | year = 2001
  | language = Norwegian
  | title = 9. april, Nazitysklands invasjon av Norge
  | publisher = Spartacus forlag AS
  | location = 
  | isbn = 978-82-430-0191-6
  | ref ={{sfnRef|Tamelander & Zetterling}}
  }}
* {{cite book
  | last = Williamson
  | first = Gordon
  | year = 2003
  | title = German Heavy Cruisers 1939–1945
  | publisher = Osprey Publishing
  | location = Oxford
  | isbn = 978-1-84176-502-0
  | ref ={{sfnRef|Williamson}}
  }}

==External links==
*[http://tv.nrk.no/program/NNFA41008615/invasjonen-av-norge-bluchers-siste-reise Blücher's siste reise] ([[Norwegian broadcasting corporation|NRK, Norwegian broadcasting corporation]]) Video (Norwegian)

{{Admiral Hipper class cruiser}}
{{April 1940 shipwrecks}}
{{Good article}}
{{coord|59|42|N|10|35.5|E|display=title}}
{{use dmy dates|date=September 2011}}

{{Authority control}}

{{DEFAULTSORT:Blucher}}
[[Category:Admiral Hipper-class cruisers]]
[[Category:Ships built in Kiel]]
[[Category:1937 ships]]
[[Category:World War II cruisers of Germany]]
[[Category:World War II shipwrecks in the North Sea]]
[[Category:Maritime incidents in April 1940]]