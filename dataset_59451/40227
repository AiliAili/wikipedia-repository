'''Bullying in higher education''' refers to the [[bullying]] of [[students]] as well as faculty and staff taking place at institutions of higher education such as [[colleges]] and [[universities]]. It is believed to be common although it has not received as much attention from researchers as bullying in some other contexts.<ref>Keashly L Faculty Experiences with Bullying in Higher Education Causes, Consequences, and Management - Administrative Theory & Praxis Volume 32, Number 1 March 2010</ref> This article focuses on bullying of students;  see [[Bullying in academia]] regarding faculty and staff.

In a [[higher education]] environment bullying and similar behaviors may include [[hazing]], [[harassment]] or [[stalking]].

18.5% of college [[undergraduates]] have reported being bullied once or twice, while 22% report being the victim of cyberbullying.  All students, regardless of [[race (human classification)|race]], [[weight]], [[gender]], [[ethnicity]], etc., can be targeted as victims of bullying.<ref name="three">"Bullying on College Campuses." Alberti Center for Bullying Abuse Prevention, n.d. Web. 26 Jan. 2014. <http://gse.buffalo.edu/gsefiles/documents/alberti/Bullying%20In%20College%20FAQ%20Final.pdf></ref>
Two research articles have examined bullying at the post-secondary level in great detail. These articles both appeared in the [[journal]] Adolescence in 2004 and 2006.<ref name="four">4.	MacDonald, Christine D. "Cyberbullying among College Students: Prevalence and Demographic Differences." Cyberbullying among College Students: Prevalence and Demographic Differences. N.p., 2010. Web. 26 Mar. 2014.</ref>
It is estimated that 100,000 students drop out of college each year due to bullying.<ref>5.	Cardin, Kathryn. "Bullying in College: Silent Yet Prevalent." Usatoday.com. N.p., n.d. Web. 26 Jan. 2014. <http://www.usatoday.com/story/news/nation/2013/10/18/college-bullying-silent-yet-prevalent/3008677/></ref>

== Bullying in academia ==
{{main|Bullying in academia}}

Bullying of scholars and staff in [[academia]], especially institutions of [[higher education]] such as [[colleges]] and [[universities]] has been known to exist, although has not received as much attention from [[researchers]] as bullying in some other contexts.<ref>Keashly, Loraleigh; Neuman, Joel H. (2010). "Faculty Experiences with Bullying in Higher Education - Causes, Consequences, and Management". Administrative Theory & Praxis (Public Administration Theory Network) 32(1): 48–70. doi:10.2753/ATP1084-1806320103. Retrieved 2013-10-29.</ref>

== Hazing ==
{{main|Hazing}}

Hazing is the practice of [[rituals]] and other activities involving [[harassment]], [[abuse]] or [[humiliation]] as a way of [[initiating]] a person into a group. Hazing is seen in many different types of social groups, including [[gangs]], [[sports team]]s, [[schools]], [[military units]], [[fraternities and sororities]]. Hazing is often prohibited by law and may comprise either [[physical abuse|physical]] or psychological abuse. It may also include [[nudity]] or sexually-oriented offenses.

More than half of hazing incidents on college campuses result in [[pictures]] publicly posted on the [[Internet]]. Students have reported that they are not adequately exposed to hazing prevention programs on campuses.<ref>7.	"FIPG Risk Management Policy" (PDF). Fraternal Information & Programming Group. July 2008. Retrieved 27 May 2009.</ref>
Two out of every five college students acknowledge incidents of hazing on their campus according to RA Magazine. 55% of college students who are involved in campus clubs, teams and other organizations have reported being hazed in some form.<ref>8.	"RA Magazine.com | The Responsibility Issue | Bullying in College." RA Magazine.com | The Responsibility Issue | Bullying in College. N.p., n.d. Web. 01 Feb. 2014. <http://www.ramagazine.com/issue/11/bully.html></ref>

== Cyberbullying ==
{{main|Cyberbullying}}

Cyberbullying is the use of electronic communication to bully a person, typically by sending messages of an intimidating or threatening nature.<ref>{{Cite web|url=https://en.oxforddictionaries.com/definition/us/cyberbullying|title=cyberbullying - definition of cyberbullying in English {{!}} Oxford Dictionaries|website=Oxford Dictionaries {{!}} English|access-date=2016-11-15}}</ref> This form of bullying can easily go undetected because of lack of parental/authoritative supervision. Because bullies can pose as someone else, it is the most anonymous form of bullying. Cyberbullying includes, but is not limited to, abuse using [[email]], [[instant messaging]], [[text messaging]], [[websites]], [[social networking sites]], etc.<ref>Jose Bolton; Stan Graeve (2005). No Room for Bullies: From the Classroom to Cyberspace. Boys Town Press.ISBN 978-1-889322-67-4. Retrieved 2013-10-29.</ref>
In a study performed at Indiana State University, it was determined that electronic media such as social networking and text messaging are more common outlets for cyberbullying, while chat rooms and other websites are less likely to be used in cyberbullying.<ref name="four" />

Once a young adult enters college, there is little to no computer monitoring, leading to the misuse of technology and the added probability of cyberbullying.<ref name="ten">{{cite journal | last1 = Schnenk | first1 = Allison | year = | title = Characteristics of College Cyberbullies | url = http://www.sciencedirect.com/science/article/pii/S0747563213001647 | journal = Computers in Human Behavior| volume =  29| issue = | pages = 2320–2327| doi=10.1016/j.chb.2013.05.013}}</ref>

There have been occasions were the bullying was not intentional, but still occurred. It should be noted that even if the bullying was not consciously intended it can still have awful impacts. One study concluded that people had been ostracised online as a way of protecting another group of people. One woman had been listed as a political opponent on a pro-trans website due to misunderstandings and alternating views, which resulted in targeted messages and harassment. The listed person in question did not actually have any reservations against the trans community, and so they ended up being the bullied person. While the motivations of the authors of the website are unknown, it can be assumed that they did not specifically aim to target the person effected and thus the bullying was an inconsiderate result.<ref>{{cite journal|title=James Roffee & Andrea Waling Rethinking microaggressions and anti-social behaviour against LGBTIQ+ Youth | doi=10.1108/SC-02-2016-0004 | volume=15 | journal=Safer Communities | pages=190–201}}</ref>

== Techniques ==

According to an article in the [[Chronicle of High Education]], academic bullies have initiated a variety of covert behaviors used to target their victims. These subtle actions include interruptions during group meetings, eye-rolling, undermining credibility, and exclusion from [[social interactions]]. Because of these techniques, bullying in academia is considered to be of a lower intensity.<ref>11.	"The Clute InstituteContemporary Issues In Education Research." Bullying in Academe: Prevalent, Significant, And Incessant. N.p., n.d. Web. 27 Jan. 2014. <http://journals.cluteonline.com/index.php/CIER/article/view/4236/4307></ref>

== Reasons ==

NoBullying.com lists a variety of reasons that bullying in college occurs. The first reason is that there are new targets available to the bully’s disclosure. The bully has said goodbye to the people he or she previously socialized with and/or bullied, so there is a need to satisfy such behaviors.

Another reason is there is less direct authority. Leaving for college introduces many students to their first time on their own without the interference of parents and guardians. Faculty and staff are less interested in interpersonal relationships between their students and thus pay less attention to classroom dynamics as opposed to the attention a high school teacher might provide.<ref name="twelve" >12.	"NOBullying.com." No BullyingExpert Advice On Cyber Bullying School Bullying. N.p., n.d. Web. 09 Feb. 2014. <http://nobullying.com/bullying-in-college/></ref>  College faculty and staff follow research that encourages them to take a backseat to bullying and allow the students to overcome adversity on their own.

Students at most universities and colleges are not afforded the luxury of leaving after the school day as they would in highschool. Most have to spend time outside of school with their classmates whether they choose to or not. In college, a majority of the campuses are residential and thus students may see much more of their potential bullies and/or victims.<ref name="twelve" />

Roommate conflicts inside the residential dorms can lead to active bullying. In fall 2012, a [[Rutgers University]] student committed suicide after his roommate had been filming him and his boyfriend engaging in sexual activities and posted the video online for all to see. The roommate said he did not want him dead, but wanted his friends to know he was disgusted by his behavior.<ref name="twelve" />

== Locations ==

Lynne McDougall uncovered, in her study of bullying in higher education, that the majority of the locations where bullying occurs in colleges were quite conventional.

A majority of the bullying is reported as occurring in the same corridor or department, thus suggesting that students within the same groups, divisions or under the same faculty are responsible for the bullying of their peers.<ref name="smith">13.	McDougall, Lynne,. "A Study of Bulllying in Further Education." Pastoral Care (1999): 31-37. Web. 10 Feb. 2014.</ref>
Entrance ways of buildings are another prime location for bullying to occur. Entrances and exit ways are common areas where students have the opportunity to smoke and socialize in between their classes.<ref name="smith" />

The library was deemed an area of bullying in McDougall’s study as well, hinting that bullying occurs in places where little to no supervision or control is present.<ref name="smith" />

The advancement of technology in the classroom has allowed for cyberbullying to occur while students are gathered for the intent of education. Social media websites such as [[Twitter]], allow students to actively post content bashing their classmates. College-specific accounts have been created where members of the student body can send posts and messages to an administrator who then retweets or posts the content for all the account’s followers to read. [needs source]

== Legality ==

Colleges are not mandated to produce strategies or policies regarding [[Bullying|anti-bullying]], however some have codes of conduct that encourage students to exhibit appropriate behavior at all times. In most codes of conduct the word bullying is never cited in the physical text.<ref name="smith" />

“Both the perpetrators and the victims are adults, so the legal framework is very, very different,” said Charlie Rose, the U.S. Department of Education’s general counsel.<ref>14.	"Prevention Update: Bullying and Cyberbullying at Colleges and Universities." The Higher Education Center for Alcohol, Drug Abuse, and Violence Prevention, n.d. Web. 03 Feb. 2014. <http://safesupportivelearning.ed.gov/sites/default/files/sssta/20130315_january2012.pdf></ref>
The difference between bullying and [[sexual harassment]] is the added context of sexuality. Sexual harassment is defined as unwelcome conduct of a sexual nature that interferes with a student’s ability to learn, work, achieve or participate in activities. This can include unwanted sexual advances, sexual touching, requests for sexual favors, or other verbal, nonverbal,and physical actions of a sexual nature. This includes spreading sexual rumors, making sexual comments, jokes, [[gestures]], vandalism, pictures, written materials, rating students sexually and circulating Web content of a sexual nature.<ref>15.	"Michigan State University MSU Extension." Bullying Issues Continue into the College Years for Some Students. N.p., n.d. Web. 27 Jan. 2014. <http://msue.anr.msu.edu/news/bullying_issues_continue_into_the_college_years_for_some_students></ref>
Human resource departments may be used to address bullying among faculty and staff, while judicial review committees apply sanctions and regulations to students charged with harassment of their peers.<ref>16.	Prevention of Bullying in Schools, Colleges, and Universities. American Education Research Association, n.d. Web. 03 Feb. 2014. <http://www.bc.edu/content/dam/files/schools/lsoe/pdf/Newsitenms/Prevention%20of%20Bullying%20in%20Schools,%20Colleges%20and%20Universities.pdf></ref>

== See also ==

*[[Bullying]]
*[[Cyberbullying]]
*[[Bullying in academia]]
*[[Hazing]]
*[[School violence]]
*[[Social exclusion]]
*[[Social isolation]]

== References ==

{{reflist}}

[[Category:Bullying]]