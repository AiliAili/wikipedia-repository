{{For|other individuals with the same name|Alexander Yakovlev (disambiguation)}}
{{refimprove|date=January 2012}}
{{Infobox engineer
|name                 = Alexander Sergeyevich Yakovlev<br>Александр Сергеевич Яковлев
|image                = Poststamp Yakovlev.jpg
|nationality          = [[Soviet Union]], [[Russia]]
|birth_date           = {{OldStyleDate|1 April|1906|19 March}}
|birth_place          = [[Moscow]], [[Russian Empire]]
|death_date           = {{death date and age|1989|08|22|1906|04|01|df=yes}}
|death_place          = [[Moscow]], [[Soviet Union]]
|education            = 
|spouse               = 
|parents              = Nina Vladimirovna
|children             = 
|discipline           = [[Aeronautical Engineering]]
|institutions         = 
|practice_name        = 
|employer             = [[Yakovlev]] design bureau
|significant_projects = 
|significant_design   = 
|significant_advance  = 
|significant_awards   = 
}}
'''Alexander Sergeyevich Yakovlev''' ({{lang-ru|Алекса́ндр Серге́евич Я́ковлев}}; {{OldStyleDate|1 April|1906|19 March}}{{spaced ndash}}22 August 1989) was a [[USSR|Soviet]] [[aerospace engineering|aeronautical engineer]]. He designed the [[Yakovlev]] military aircraft and founded the [[Yakovlev Design Bureau]].<ref>[http://www.monino.ru/ Central Museum of the Military Air Forces of the Russian Federation]</ref> Yakovlev was a member of the Communist Party of the Soviet Union from 1938.<ref>http://www.britannica.com/EBchecked/topic/651304/Aleksandr-Sergeyevich-Yakovlev</ref>

==Biography==
Yakovlev was born in [[Moscow]], where his father was an employee of the [[Nobel Brothers]] oil company. From 1919-1921 he worked as a part-time courier while still in school, and in 1922 he built his first model airplane as part of a school project. In 1924, he built a glider, the AVF-10, which made its first flight on 24 September 1924. The design won an award, and secured him a position as a worker at the [[Nikolay Yegorovich Zhukovsky|Zhukovsky Air Force Military Engineering Academy]]. However, his repeated attempts to gain admission to the Academy were denied due to his “lack of [[proletariat]] origins”. In 1927, Yakovlev designed the AIR-1 ultralight aircraft. This was the first of a series of ten aircraft he designed between 1927-1933.

In 1927, Yakovlev finally gained admittance to the Academy, and graduated in 1931. He was then assigned to the Moscow Aviation Plant № 39, where his first design bureau of lightweight aviation was established in 1932. He became the main designer in 1935, then the chief designer (1956–1984) of aircraft for the [[Yakovlev]] Design Bureau.

The Yakovlev Design Bureau developed large numbers of fighter aircraft used by the [[Soviet Air Force]] during [[World War II]].  Particularly well known are the [[Yakovlev Yak-1|Yak-1]], [[Yakovlev Yak-3|Yak-3]] and [[Yakovlev Yak-9|Yak-9]] as well as the [[Yakovlev Yak-6|Yak-6]] transport. In 1945 Yakovlev designed one of the first Soviet aircraft with a jet engine, the [[Yakovlev Yak-15|Yak-15]]. He also designed the first{{citation needed|date=October 2011}} Soviet  all-weather interceptor, the [[Yak-25P]], and the first{{citation needed|date=October 2011}} Soviet supersonic bomber, the [[Yakovlev Yak-28|Yak-28]]. In the post-war period, Yakovlev was best known for the civilian airliner, the [[Yakovlev Yak-42|Yak-42]], a three-engine medium-range aircraft, and numerous models for aerobatics.

Yakovlev served under [[Joseph Stalin]] as a Vice-Minister of Aviation Industry between 1940–1946. Before the start of World War II, he made a number of trips aboard, including Italy, England and Germany, to study aircraft development in those countries. After the start of the war, he helped supervise the evacuation of aircraft factories to the east, and the organization of production, while continuing as head designer of his Bureau. He was also a correspondent-member of the [[Russian Academy of Sciences#The USSR Academy of Sciences|USSR Academy of Science]] in 1943. In 1946 he was awarded the title "[[Colonel General|General-Colonel]] of Aviation".

In 1976 Yakovlev became academician of the USSR Academy of Science. He was a deputy of the [[Supreme Soviet of the USSR]] (1946–1989). Yakovlev retired 21 August 1984. He was buried in the [[Novodevichy Cemetery]] in Moscow.

==Awards and honors==
* [[Hero of Socialist Labour]] (1940, 1957)
* [[Lenin Prize]] (1972)
* [[Stalin Prize]] (1941, 1942, 1943, 1946, 1947, 1948)
* [[USSR State Prize]] 1977)
* [[Order of Lenin]] (10 times)
* [[Order of the October Revolution]]
* [[Order of the Red Banner]] (twice)
* [[Order of Suvorov]], 1st and 2nd class,
* [[Order of the Patriotic War]] of the 1st class (twice)
* [[Order of the Red Banner of Labour]]
* [[Order of the Red Star]]
* [[Legion of Honor]], Officer (France)
* Gold medal of the [[Fédération Aéronautique Internationale]]

== See also ==
{{Portal|Russia|Soviet Union|Biography|Aviation}}
* [[List of Russian aerospace engineers]]
{{Clear}}

== References ==
{{reflist}}

== Further reading ==
*Iakovlev, I.A. “Aim of a Lifetime, Story of Alexander Yakovlev, Designer of the YAK Fighter Plane”, Central Publishers, 1972, ISBN 0-7147-0474-1

{{Authority control}}

{{DEFAULTSORT:Yakovlev, Alexander Sergeyevich}}
[[Category:1906 births]]
[[Category:1989 deaths]]
[[Category:Writers from Moscow]]
[[Category:Yakovlev]]
[[Category:Soviet politicians]]
[[Category:Soviet aerospace engineers]]
[[Category:20th-century engineers]]
[[Category:Russian aerospace engineers]]
[[Category:Communist Party of the Soviet Union members]]
[[Category:Heroes of Socialist Labour]]
[[Category:Recipients of the USSR State Prize]]
[[Category:Recipients of the Order of the October Revolution]]
[[Category:Stalin Prize winners]]
[[Category:Lenin Prize winners]]
[[Category:Recipients of the Order of Lenin]]
[[Category:Recipients of the Order of the Red Banner of Labour]]
[[Category:Recipients of the Order of the Red Star]]
[[Category:Recipients of the Order of the Patriotic War, 1st class]]
[[Category:Recipients of the Order of Suvorov, 1st class]]
[[Category:Officiers of the Légion d'honneur]]
[[Category:Recipients of the Order of the Red Banner]]
[[Category:Full Members of the USSR Academy of Sciences]]
[[Category:Burials at Novodevichy Cemetery]]