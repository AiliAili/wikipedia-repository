The chameleon is among the most highly visually-oriented [[lizard]]s, using this sense in [[prey]] capture, mating behavior, and [[predator avoidance]].<ref name =Lustig2012>{{cite journal|last=Lustig|first=Avichai|author2=Hadas Keter-Katz|author3=Gadi Katzir|title=Threat perception in the chameleon (Chamaeleo chameleon): evidence for lateralized eye use|journal=Animal Cognition|year=2012|volume=15|pages=609–621|pmid=22460630|doi=10.1007/s10071-012-0489-7}}</ref> Unique features of chameleon vision include a [[negative lens]], a positive [[cornea]], and [[Monocular vision|monocular]] focusing. The development of the chameleon visual system could have evolved to aid in prey capture and/or in predator avoidance.
[[File:Chamäleon2.jpg|thumb|alt=image_chameleon_eye|Chameleon eye]]

The angle, or [[amplitude]], of eye movement in chameleons is very large for a [[vertebrate]]<ref name = Lustig2012/> and the eyes move independently of each other.<ref name =Sandor2001>{{cite journal|last=Sandor|first=Peter S.|author2=Maarten A. Frens|author3=Volker Henn|title=Chameleon eye position obeys Listing’s law|journal=Vision Research|year=2001|volume=41|pages=2245–2251|doi=10.1016/s0042-6989(01)00111-0}}</ref> This allows a chameleon to watch an approaching object while simultaneously scanning the rest of its environment.<ref name ="  Lustig2012"/> Chameleon eyes protrude laterally from the head, giving the lizard [[panoramic]] sight.<ref name = Sandor2001/> An eyelid fused to the pupil protects the eyes, leaving only a small part exposed.<ref name = Sandor2001/> With a negative (nearsighted or concave) lens and a positive (farsighted or convex) cornea, chameleons use a method of monocular focusing to judge distance called corneal accommodation. Each eye focuses independently, which is achieved by the chameleon eye’s unique anatomy of separated nodal and center points of the eye.<ref name = Ott1998>{{cite journal|last=Ott|first=M. |author2=F. Schaeffel |author3=W. Kirmse |title=Binocular vision and accommodation in prey-catching chameleons|journal=Journal of Comparative Physiology A|year=1998|volume=182|pages=319–330|doi=10.1007/s003590050182}}</ref> Finally, “striated rather than smooth ciliary muscle in sauropsids” allows for rapid focusing.<ref name = Ott1998/>

==Negative Lens==


Chameleon eyes feature a negative lens, meaning that the lens is concave. This increases retinal image size, allowing "more precise focusing."<ref name = Ott1998/><ref name = Ott1995>{{cite journal|last=Ott|first=Matthias|author2=Frank Schaeffel|title=A negatively powered lens in the chameleon|journal=Nature|year=1995|volume=373|pages=692–694|doi=10.1038/373692a0}}</ref> In fact, image magnification in chameleons is higher in a scaled comparison to all other vertebrates eyes.<ref name = Ott1995/>

==Positive Cornea==


While the lens is negative, the [[cornea]] of chameleon eyes is positive, meaning that it is convex. The increased power of the cornea also contributes to a precision in focusing "better than other vertebrates."<ref name = Ott1998/> The cornea improves sight resolution in a narrower field of vision.<ref name = Ott1998/>

==Monocular Focusing and Corneal Accommodation==


The combination of a negative lens and a positive cornea in the chameleon eye allow for accurate focusing by corneal accommodation.<ref name = Ott1995/> Using corneal accommodation for depth perception<ref name = Srinivasan1999>{{cite journal|last=Srinivasan|first=Mandyam V.|title=Ecology: When one eye is better than two|journal=Nature|year=1999|volume=399|pages=305–307|doi=10.1038/20550}}</ref> makes the chameleon the only vertebrate to focus monocularly.<ref name = Lustig2012/> While sight is primarily independent in the two chameleon eyes, the eye that first detects prey will guide accommodation in the other eye.<ref name = Ott1998/> Contrary to previous thought that chameleons used stereopsis (both eyes) for depth perception, research has shown monocular focusing to be more likely.<ref name = Harkness1977>{{cite journal|last=Harkness|first=Lindesay|title=Chameleons use accommodation clues to judge distance|journal=Nature|year=1977|volume=267|pages=346–349|doi=10.1038/267346a0}}</ref> Corneal accommodation however can be coupled, depending on the chameleon's step in the predation sequence, coupled vision meaning the eyes independently focus on the same object.<ref name = Ott1998/> When scanning the environment and in judging distance to prey, vision and accommodation are uncoupled:  the eyes are focusing on different objects, such as the environment and the newly-sighted prey. Immediately before the chameleon's characteristic tongue is extended, accommodation in both eyes is coupled:  both eyes focus independently on the prey.<ref name = Ott1998/> Imprecise alignment of the images from each eye, as demonstrated by measuring various angles from eye to target, shows that stereopsis is unlikely for depth perception in the chameleon.<ref name = Ott1998/>

==Nodal Point Separation==


The nodal point in the eye is the point at which "lines connecting points in the scene and corresponding points in the image intersect."<ref name = Srinivasan1999/> In chameleons, the nodal point is located a significant distance before the center of rotation, the point around which the eye rotates in the eye socket. As a result of this nodal point separation, images of objects move more or less on the retina based on whether they are farther or closer away. The position of an image on the retina is the "primary means by which chameleons judge distance."<ref name = Ott1998/> Therefore, the rotation of one eye informs the chameleon of the "relative distances of different objects."<ref name = Srinivasan1999/> An important effect of the ability to judge distance with one eye is that the head does not have to be turned to allow stereoptic viewing of the object.<ref name = Pettigrew1999>{{cite journal|last=Pettigrew|first=John D.|author2=Shaun P. Collin|author3=Matthias Ott|title=Convergence of specialised behaviour, eye movements and visual optics in the sandlance (Teleostei) and the chameleon (Reptilia)|journal=Current Biology|year=1999|volume=9|pages=421–424|doi=10.1016/s0960-9822(99)80189-4}}</ref>

==Evolution==

===Chameleons as an Evolutionary Transition to Stereopsis===

A suggested theory for the evolution of squamate vision is that corneal accommodation and monocular depth perception are "primitive" mechanisms in comparison to binocular vision and stereopsis.<ref name = Ott1998/> Chameleons use an alternative strategy to stereopsis in functional coupling of the eyes immediately before the tongue shot. This differs from stereopsis in that the images from both eyes are not reconciled into one. However, it is possible that this was first used for neural static reduction.<ref name = Ott1998/> This suggests that chameleons could be seen as a transition between independent and coupled eye use.<ref>{{cite journal|last=Ott|first=Matthias|title=Chameleons have independent eye movements but synchronise both eyes during saccadic prey tracking|journal=Experimental Brain Research|year=2001|volume=139|pages=173–179|doi=10.1007/s002210100774}}</ref> However, it is also possible that the chameleon vision system is an alternative, equally successful mode of prey capture and predator avoidance, and perhaps more appropriate for the chameleon's niche as a camouflaged, arboreal hunter than other vision systems.

===Prey/Predator Causes of Chameleon Eye Development===

The chameleon, a camouflaged, slow-moving lizard, is an arboreal hunter that hides and ambushes prey.<ref name = Sandor2001/> Prey and predators alike can be sighted and monitored using monocular depth perception. Also, nodal point separation allows distance to be judged with one eye, so minimal head movement is needed by the chameleon in watching its surroundings, reinforcing the chameleon strategy of inconspicuousness.<ref name = Srinivasan1999/>

====Prey Capture====

The specialized strategy by which chameleons capture prey is reflected in their sensory anatomy and use, in particular that of vision.<ref name="Sandor2001"/> First, prey is sighted and distance assessed using one eye.<ref name = Harkness1977/> To avoid detection by prey, a chameleon uses minimal head movement, made possible by nodal point separation.<ref name = Srinivasan1999/> The chameleon then slowly turns its head toward the prey. Both eyes focus independently on the prey before the tongue shot.<ref name = Ott1998/>

====Predator Avoidance====

The chameleon predator avoidance response is vision-mediated.<ref name = Lustig2012/> In predator avoidance, chameleons use minimal head movement and a unique method to monitor potential threats. Due to nodal point separation, a chameleon can judge distance to a potential threat with minimal head movement needed. When confronted with a potential threat, chameleons rotate their slender bodies to the opposite side of their perch to avoid detection.<ref name = Lustig2012/> They will keep moving around the branch to keep the branch between themselves and the threat and to keep the threat in their line of sight.<ref name = Lustig2012/> If the branch is narrow, a chameleon can observe a threat binocularly around the branch. While a wide branch might present a difficulty in depth perception to another lizard as it is forced to view the threat monocularly, a chameleon due to corneal accommodation and nodal point separation can judge distance between itself and a potential threat with only one eye viewing the threat.<ref name = Lustig2012/>

====Comparison to the Sandlance Fish====

While the chameleon eye is unique in lizards, parallels exist in other members of the animal kingdom. In particular, the sandlance fish (Limnichthyes fasciatus) shares key vision features with the chameleon. This is because the environmental circumstances such as the need for camouflaged quick prey capture that led to the development of the chameleon eye seem to have acted on the sandlance fish as well.<ref name = Pettigrew1999/> Rapid predatory attacks are made possible through the chameleon and the sandlances' striated corneal muscles allowing for corneal accommodation, a reduced power lens, and increased corneal power.<ref name = Ott1998/> A nearly complete eyelid covering and lack of head movement due to nodal separation reduce conspicuousness to prey and predators.<ref name = Pettigrew1999/>

== References ==
{{reflist|30em}}

{{vision in animals}}

[[Category:Vision]]
[[Category:Vertebrate anatomy]]