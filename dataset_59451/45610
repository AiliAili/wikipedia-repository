{{notability|Biographies|date=November 2016}}
{{Infobox military person
|name=Bruno Stolle
|birth_date=13 April 1915
|death_date={{death date and age|2004|1|22|1915|4|13|df=y}}
|image=Bruno_Stolle.jpg
|birth_place=[[Münster]] 
|death_place=[[Rheinbach]]
|allegiance={{flag|Nazi Germany}}<br />{{flag|West Germany}}
|branch={{Luftwaffe}} 
|serviceyears=?–1945, 1956–72
|rank=[[Hauptmann]] (''Wehrmacht'')<br/>[[Oberstleutnant]] (''[[Bundeswehr]]'')
|unit=[[Lehrgeschwader 1|LG 1]]; [[Jagdgeschwader 51|JG 51]]; [[Jagdgeschwader 2|JG 2]]; [[Jagdgeschwader 11|JG 11]]
|battles=[[World War II]]
*[[Battle of France]]
*[[Battle of Britain]]
*[[Defence of the Reich]]
|awards=[[Knight's Cross of the Iron Cross]]
|laterwork=}}

'''Bruno Stolle''' (13 April 1915 – 22 January 2004) was a German fighter pilot in the [[Luftwaffe]] during [[World War II]]. He was a recipient of the [[Knight's Cross of the Iron Cross]], awarded by [[Nazi Germany]] to recognise extreme battlefield bravery or successful military leadership.  Bruno Stolle was credited with 35 aerial victories in 271 combat missions.{{citation needed|date=October 2016}}

==Awards==
* [[Ehrenpokal der Luftwaffe]] on 15 September 1941 as ''[[Oberleutnant]]'' in a ''Jagdgeschwader''<ref>Patzwall 2008, p. 200.</ref>
* [[German Cross]] in Gold on 29 October 1942 as ''[[Oberleutnant]]'' in the 8./Jagdgeschwader 2<ref>Patzwall & Scherzer 2001, p. 462.</ref>
* [[Knight's Cross of the Iron Cross]] on 17 March 1943 as ''[[Hauptmann]]'' and ''[[Staffelkapitän]]'' of the 8./Jagdgeschwader 2 "Richthofen"<ref>Fellgiebel 2000, p. 335.</ref>

==References==

===Citations===
{{Reflist|25em}}

===Bibliography===
{{refbegin}}
* {{Cite book
  |last=Fellgiebel
  |first=Walther-Peer
  |authorlink=Walther-Peer Fellgiebel
  |year=2000
  |origyear=1986
  |title=Die Träger des Ritterkreuzes des Eisernen Kreuzes 1939–1945 — Die Inhaber der höchsten Auszeichnung des Zweiten Weltkrieges aller Wehrmachtteile
  |trans_title=The Bearers of the Knight's Cross of the Iron Cross 1939–1945 — The Owners of the Highest Award of the Second World War of all Wehrmacht Branches
  |language=German
  |location=Friedberg, Germany
  |publisher=Podzun-Pallas
  |isbn=978-3-7909-0284-6
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |last2=Scherzer
  |first2=Veit 
  |year=2001
  |title=Das Deutsche Kreuz 1941 – 1945 Geschichte und Inhaber Band II
  |trans_title=The German Cross 1941 – 1945 History and Recipients Volume 2
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-45-8
}}
* {{Cite book
  |last1=Patzwall
  |first1=Klaus D. 
  |year=2008
  |title=Der Ehrenpokal für besondere Leistung im Luftkrieg
  |trans_title=The Honor Goblet for Outstanding Achievement in the Air War
  |language=German
  |location=Norderstedt, Germany
  |publisher=Verlag Klaus D. Patzwall
  |isbn=978-3-931533-08-3
}}
* {{Cite book
  |last=Scherzer
  |first=Veit 
  |year=2007
  |title=Die Ritterkreuzträger 1939–1945 Die Inhaber des Ritterkreuzes des Eisernen Kreuzes 1939 von Heer, Luftwaffe, Kriegsmarine, Waffen-SS, Volkssturm sowie mit Deutschland verbündeter Streitkräfte nach den Unterlagen des Bundesarchives
  |trans_title=The Knight's Cross Bearers 1939–1945 The Holders of the Knight's Cross of the Iron Cross 1939 by Army, Air Force, Navy, Waffen-SS, Volkssturm and Allied Forces with Germany According to the Documents of the Federal Archives
  |language=German
  |location=Jena, Germany
  |publisher=Scherzers Militaer-Verlag
  |isbn=978-3-938845-17-2
}}
{{refend}}

{{s-start}}
{{s-mil}}
{{succession box|
before=Hauptmann [[Egon Mayer]]|
after=Hauptmann [[Herbert Huppertz]]|
title= Commander of III./[[Jagdgeschwader 2]]|
years=1 July 1943 – February 1944
}}
{{succession box|
before=Hauptmann [[Walter Matoni]]|
after=Hauptmann Rüdiger Kirchmayr|
title= Commander of I./[[Jagdgeschwader 11]]|
years=November 1944 – 25 November 1944
}}
{{s-end}}

{{Knight's Cross recipients of JG 2}}

{{Subject bar
| portal1=Aviation
| portal2=Biography
| portal3=Military of Germany
| portal4=World War II
}}

{{DEFAULTSORT:Stolle, Bruno}}
[[Category:1915 births]]
[[Category:2004 deaths]]
[[Category:People from Münster]]
[[Category:Luftwaffe pilots]]
[[Category:German World War II flying aces]]
[[Category:Recipients of the Gold German Cross]]
[[Category:Recipients of the Knight's Cross of the Iron Cross]]
[[Category:People from the Province of Westphalia]]