{{Infobox newspaper
|name = Evangelical Times
|logo = Evangelical Times Newspaper Logo.jpg
|logo_size = 220
|image = Front Page of the Evangelical Times for June 2011.jpg
|image_size = 220
|caption = June 2011 edition of ''Evangelical Times''
|type = Monthly
|format = [[Tabloid (newspaper format)|Tabloid]]
|foundation = 1967 
|owners = 
|political = 
|headquarters = [[Darlington, County Durham|Darlington]]
|editor =  Roger Fay
|website = {{URL|http://www.evangelical-times.org/}}
| oclc = 32472057
| ISSN = 1358-7285
}}

'''Evangelical Times''' is a monthly,  [[British Conservative Evangelicalism|conservative]] [[Evangelicalism|evangelical]] [[newspaper]].<ref>[http://www.biblicalstudies.org.uk/pdf/ref-rev/09-4/9-4_book-notices.pdf Reference to Evangelical Times as 'a British Monthly']</ref><ref>[http://www.eauk.org/resources/publications/upload/torontointro.pdf Repeated references made to Evangelical Times as a source of conservative response to the 'Toronto Blessing']</ref> It is published in a [[Tabloid (newspaper format)|tabloid]] format and is based in [[Darlington]], [[County Durham]]. It has a circulation of approximately 7000 copies each month. There is a Christmas edition which is purchased in larger quantities by churches and individuals to be given to friends and contacts.<ref>[http://www.evangelical-times.org/about-et/ Christmas Edition]</ref>The June edition contains a list of evangelical 'Holiday Churches.'<ref>[http://maps.google.co.uk/maps?f=q&source=embed&hl=en&geocode=&q=http:%2F%2Fwww.evangelical-times.org%2Fdownloads%2FChurches-v3.kml&aq=&sll=53.800651,-4.064941&sspn=19.805845,39.506836&vpsrc=6&ie=UTF8&t=m&ll=54.775346,-1.340332&spn=6.086131,14.0625&z=6 'Holiday Churches']</ref> The current editor is Roger Fay, pastor of [[Zion Evangelical Baptist Church]] in [[Ripon]].

==History==
It was founded in 1967 by Dr Peter Masters, currently minister at the [[Metropolitan Tabernacle]], London.

==Content==
The newspaper is composed of the following items:
* News items of particular interest to evangelical Christians
* Articles which are focused on: Church History, Theology or Mission
* Book Reviews

==Website==
The [http://www.evangelical-times.org website] contains most of the content of past issues with a search facility.

==Senior editors==
*David C Potter MBE <ref>[http://www.e-n.org.uk/p-3321-Tribute-to-an-encourager-%28Bob-Horn%29.htm David C Potter as an associate Editor]</ref>
*Bob Horn <ref>[http://www.e-n.org.uk/p-3314-Robert-Millen-Horn-1933-2005.htm Bob Horn leaves the Evangelical Times to start Evangelicals Now]</ref><ref>[http://www.biblicalcreation.org/bcs_publications/Origins46_7.pdf Reference to Bob Horn as Editor]</ref>
*Prof [[Edgar Andrews]]<ref>[http://www.evangelical-times.org/archive/item/3783/News/News---Evangelical-Times-editors/ Reference to Edgar Andrews as editor]</ref>
*Roger Fay

==References==
{{Reflist}}

==External links==
* [http://www.evangelical-times.org Evangelical Times Website]
* [http://www.e-n.org.uk/ Evangelicals Now Website]

[[Category:Evangelical organizations established in the 20th century]]