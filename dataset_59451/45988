{{Infobox gymnast
|name= Ruth Abeles
|image= File:Women in Israel Olympic Team1960.jpg
|imagesize=
|caption= Israel's women gymnasts on departure for Rome Olympics; L-R, [[Miriam Kara]], Ruth Abeles, [[Ralli Ben-Yehuda]].
|fullname= 
|altname= רות אבלס
|nickname=
|country= {{ISR}}
|formercountry= 
|birth_date= {{Birth date and age|mf=yes|1942|8|7}}
|birth_place=[[Haifa]], Israel
|hometown=
|residence=
|height= 5-0 (153 cm)
|discipline= WAG
|level=
|natlteam=
|club=
|gym=
|collegeteam=
|headcoach=
|assistcoach=
|formercoach=
|choreographer=
|eponymousskills=
|retired=
|medaltemplates=
}}
'''Ruth Abeles''' (רות אבלס; born August 7, 1942) is an Israeli former Olympic gymnast.<ref name="sports-reference.com">{{cite web|url=http://www.sports-reference.com/olympics/athletes/ab/ruth-abeles-1.html|title=Ruth Abeles Bio, Stats, and Results|publisher=sports-reference.com}}</ref><ref>{{cite web|url=https://www.olympic.org/ruth-abeles|title=Ruth ABELES - Olympic Gymnastics Artistic - Israel|date=13 June 2016|publisher=sports-reference.com}}</ref>

She was born in [[Haifa]], Israel, and is Jewish.<ref name="sports-reference.com"/><ref name="jewsinsports.org">[http://www.jewsinsports.org/olympics.asp?ID=610 "Abeles, Ruth"]</ref>

==Gymnastics career==
She competed for [[Israel at the 1960 Summer Olympics]] in Rome, Italy, in gymnastics at the age of 18.<ref name="sports-reference.com"/>  In the [[Gymnastics at the 1960 Summer Olympics|Women's Individual All-Around]], she came in 93rd out of 124 gymnasts, in the [[Gymnastics at the 1960 Summer Olympics – Women's floor|Women's Floor Exercise]] she came in 79th, in the [[Gymnastics at the 1960 Summer Olympics – Women's vault|Women's Horse Vault]] she came in 100th, in the [[Gymnastics at the 1960 Summer Olympics – Women's uneven bars|Women's Uneven Bars]] she came in tied for 55th, and in the [[Gymnastics at the 1960 Summer Olympics – Women's balance beam|Women's Balance Beam]] she came in tied for 108th.<ref name="sports-reference.com"/><ref>{{cite web|url=http://www.sports-reference.com/olympics/summer/1960/GYM/womens-individual-all-around.html|title=Gymnastics at the 1960 Roma Summer Games: Women's Individual All-Around|publisher=sports-reference.com}}</ref> When she competed in the Olympics she was 5-0 (153&nbsp;cm) tall and weighed 106&nbsp;lbs (48&nbsp;kg).<ref name="sports-reference.com"/>

At the [[1962 World Artistic Gymnastics Championships]], she helped the Israeli team gain 15th place as she finished in 67th place in the individual all-around.<ref name="jewsinsports.org"/>

== References ==
<!-- Inline citations added to your article will automatically display here. See https://en.wikipedia.org/wiki/WP:REFB for instructions on how to add citations. -->
{{reflist}}

{{DEFAULTSORT:Abeles, Ruth}}
[[Category:Gymnasts at the 1960 Summer Olympics]]
[[Category:Israeli female artistic gymnasts]]
[[Category:Living people]]
[[Category:1942 births]]
[[Category:Olympic gymnasts of Israel]]
[[Category:Jewish gymnasts]]
[[Category:People from Haifa]]