{{Use dmy dates|date=December 2016}}
{{Use British English|date=December 2016}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
|name =5F.1 Dolphin
|image =Sopwith 5F.1 Dolphin C3988 (8575063201).jpg
|caption=<small>Dolphin Mk I composite airframe displayed at the [[Royal Air Force Museum London]], 2013</small>
}}{{Infobox Aircraft Type
|type = [[Fighter plane|Fighter]]
|manufacturer = [[Sopwith Aviation Company]]
|designer = Herbert Smith
|first flight=23 May 1917
|introduced = February 1918
  |retired = <!--date the aircraft left military or revenue service. if vague or multiples, it probably should be skipped-->
  |status = <!--in most cases, this field is redundant; use it sparingly-->
  |primary user = [[Royal Flying Corps]]
  |more users = [[Royal Air Force]] <!--up to two more. please separate with <br/>.-->
  |produced =<!--years in production, e.g. 1970-1999, if still in active use but no longer built-->
|number built = 2,072<ref name="Mason p105">Mason 1992, p. 105.</ref>
}}
|}

The '''Sopwith 5F.1 Dolphin''' was a [[United Kingdom|British]] [[fighter aircraft]] manufactured by the [[Sopwith Aviation Company]]. It was used by the [[Royal Flying Corps]] and its successor, the [[Royal Air Force]], during the [[World War I|First World War]]. The Dolphin entered service on the [[Western Front (World War I)|Western Front]] in early 1918 and proved to be a formidable fighter. The aircraft was not retained in the postwar inventory, however, and was retired shortly after the war.
{{TOC limit|limit=2}}

==Design and development==
[[File:Sopdol3.jpg|thumb|right|Third prototype]]
[[File:Dolp4.jpg|thumb|right|Third prototype at [[Brooklands#Brooklands Airfield|Brooklands Airfield]]]]
In early 1917, Sopwith's chief engineer Herbert Smith began designing a new fighter (internal Sopwith designation 5F.1) powered by the geared 200&nbsp;hp [[Hispano-Suiza 8]]B.<ref name="Franks p7">Franks 2002, p. 7.</ref>  The resulting Dolphin was a two-bay, single-seat biplane. The upper wings were attached to an open steel [[cabane strut|cabane]] frame above the cockpit. To maintain the correct centre of gravity, the lower wings were positioned 13&nbsp;inches forward of the upper wings, creating the Dolphin’s distinctive negative wing [[Stagger (aviation)|stagger]].<ref name="Franks p7"/><ref name="Cooksley p34">Cooksley 1991, p. 34.</ref> The pilot sat with his head raised through the frame, where he had an excellent field of view. This configuration sometimes caused difficulty for novice pilots, who found it difficult to keep the aircraft pointed at the horizon because the nose was not visible from the cockpit.<ref name="Franks p8"/> The cockpit was nevertheless warm and comfortable,<ref name="Cooksley p34"/> in part because pipes ran alongside the cockpit walls to the two side-mounted radiator blocks. Shutters in front of each radiator core allowed control of engine temperature.

The first Dolphin prototype was powered by a geared 150&nbsp;hp [[Hispano-Suiza]] 8 and featured a deep "car-type" frontal radiator.<ref name="Mason fighter p104">Mason 1992, p.  104.</ref><ref name="Robertson p102"/><ref name="Davis p126">Davis 1999, p. 126.</ref><ref name="Bruce v3p15">Bruce 1969, p. 15.</ref> Test pilot [[Harry Hawker]] carried out the maiden flight on 23 May 1917.<ref name="Franks p8"/><ref name="Robertson p102">Robertson 1970, p. 102.</ref> In early June, the prototype was sent to [[RAF Martlesham Heath|Martlesham Heath]] for official trials. On 13 June, the prototype flew to [[Saint-Omer]], France, where the aircraft's unfamiliar shape prompted Allied anti-aircraft gunners to fire on it.<ref name="Robertson p102"/> Several pilots, including [[Billy Bishop]] of No. 60 Squadron, evaluated the prototype and reported favorably on it.<ref name="Davis pp126-127">Davis 1999, pp. 126–127.</ref> On 28 June 1917, the [[Minister of Munitions|Ministry of Munitions]] ordered 200 Dolphins from [[Hooper (coachbuilder)|Hooper & Co.]]<ref name="Davis p127">Davis 1999, p. 127.</ref> Shortly thereafter, the Ministry ordered a further 500 aircraft from Sopwith and 200 aircraft from [[Darracq Motor Engineering Company]]<ref name="Davis p127"/>

The second prototype introduced upper wing radiators in lieu of the frontal radiator and large cut-outs in the lower wing roots to improve the pilot's downward vision.<ref name="Davis p127"/> These features proved unsuccessful and were omitted from subsequent aircraft.<ref name="Bruce v3p15"/><ref name="Davis p128">Davis 1999, p. 128.</ref> The third and fourth prototypes incorporated numerous modifications to the radiator, upper fuselage decking, fin and rudder.<ref name="Franks p8">Franks 2002, p. 8.</ref><ref name="Davis p128"/> The fourth prototype was selected as the production standard.<ref name="Davis p128"/><ref>Bruce 1961, p. 134.</ref> Series production commenced in October 1917, with 121 Dolphins delivered by the end of the year.<ref name="Mason p105"/><ref name="Bruce v3p15"/>

==Operational history==
[[File:C-DolphinInterior.jpg|thumb|right|Dolphin cockpit]]
[[File:Sopdol2.jpg|thumb|right|Dolphin fitted with two upward firing Lewis guns and Norman vane sights]]
The '''Dolphin Mk I''' became operational with Nos. [[No. 19 Squadron RAF|19]] and [[No. 79 Squadron RAF|79]] Squadrons in February 1918. Nos. [[No. 87 Squadron RAF|87]] and [[No. 23 Squadron RAF|23]] Squadrons followed in March. The Dolphin’s debut was marred by several incidents in which British and Belgian pilots attacked the new aircraft, mistaking it for a German type.<ref name="Franks p11">Franks 2002, p. 11.</ref> For the next few weeks, Dolphin pilots accordingly exercised caution near other Allied aircraft.

New pilots also voiced concern over the Dolphin’s wing arrangement, fearing serious injury to the head and neck in the event of a crash.<ref name="Franks p21">Franks 2002, p. 21.</ref> Early aircraft were often fitted with improvised crash pylons consisting of steel tubes over the cockpit to protect the pilot's head. Operational usage eventually showed that fears of pilot injury from overturning were largely unfounded. Crash pylons thereafter disappeared from front line aircraft, though they were often retained on training aircraft. Night-flying Dolphins of [[No. 141 Squadron RAF|No. 141 Squadron]], a Home Defence unit, had metal loops fitted above the inner set of [[interplane strut]]s.<ref name="Lamberton p62">Lamberton 1960, p. 62.</ref>

Despite early problems, the Dolphin eventually proved successful and generally popular with pilots. The aircraft was fast, maneuverable, and easy to fly, though a sharp stall was noted. When functioning properly, the Hispano-Suiza afforded the Dolphin excellent performance at high altitude. Accordingly, the Dolphin was often deployed against German reconnaissance aircraft such as the [[Rumpler C.VII]], which routinely operated at altitudes above 20,000&nbsp;ft.<ref name="Franks p21"/>  No. 87 Squadron explored the use of equipment to supply pilots with oxygen at high altitude, but the experiment was abandoned after trials showed that the oxygen tanks exploded when struck by gunfire.<ref name="Franks p21"/>

The highest-scoring Dolphin unit was No. 87 Squadron, which shot down 89 enemy aircraft in the type.<ref name="Robertson p106">Robertson 1970, p. 106.</ref> Pilots of No. 79 Squadron shot down 64 enemy aircraft in the eight and one half months that the aircraft was at the front.<ref>Bruce 1961, p. 135.</ref> The top two Dolphin aces served in No. 79 Squadron. Captain [[Francis W. Gillet]], an American, scored 20 victories in the type.<ref name="Franks p31">Franks 2002, p. 31.</ref> Lieutenant [[Ronald Bannerman]], a New Zealander, scored 17 victories.<ref name="Franks p32">Franks 2002, p. 32.</ref> The third-ranking Dolphin ace was Captain [[Arthur Vigers]] of No. 87 Squadron, who attained all 14 of his victories while flying the same aircraft.<ref name="Franks p53">Franks 2002, p. 53.</ref> Another notable ace, Major [[Albert Desbrisay Carter]] of No. 19 Squadron, obtained approximately 13 of his 29 confirmed victories in the Dolphin.<ref name="Franks p24">Franks 2002, p. 24.</ref> Captain [[Henry Biziou]] scored eight victories in the type.<ref>Shores, et al, pp. 78–79.</ref>

Four Royal Air Force squadrons operated the Dolphin as their primary equipment, while other squadrons used it in small numbers. No. 1 (Fighter) Squadron, a [[Canadian Air Force (1918-1920)|Canadian Air Force]] unit, formed with Dolphins at [[RAF Upper Heyford]].<ref name= "Milberry p. 16">Milberry 1984, p. 16.</ref> The unit became operational shortly after the [[Armistice with Germany (Compiègne)|Armistice]].<ref name="Robertson p106"/>

In October 1918, the [[American Expeditionary Force]] purchased five standard Mk Is for evaluation, sending four back to the United States.<ref name="Bruce p. 150"/>

===Engine problems===
[[File:Hispano-Suiza 8BE.jpg|thumb|A geared Hispano-Suiza 8BE engine on display at the [[NMUSAF]] ]]
The scarcity and unreliability of the French-built [[Hispano-Suiza 8#Hispano-Suiza 8B (HS-8B)|Hispano-Suiza 8B]] engine proved to be the most serious problem in the deployment and use of the Dolphin. Use of insufficiently hardened metal in the [[pinion]] gears led to numerous failures of the reduction gearing,<ref name="Mason p105"/>  particularly in engines built by the French firm [[Brasier]].<ref name="Davis p125">Davis 1999, p. 125.</ref> The engine also suffered persistent lubrication problems.<ref name="Davis p129">Davis 1999, p. 129.</ref> Limited production capacity for the Hispano-Suiza engine, and the priority afforded to French aircraft like the [[SPAD S.XIII]] slowed Dolphin deliveries. Availability of the Hispano-Suiza improved in early 1918 as the French firm Emile Mayen began deliveries on an order placed by the [[British Admiralty]].

===Use of the Lewis guns===
[[File:Sopdol.jpg|thumb|right|No. 87 Squadron Dolphin flown by [[Cecil Montgomery-Moore]]. A Lewis gun is mounted atop the lower right wing]]
The official armament of the Dolphin was two fixed, [[Synchronization gear|synchronized]] [[Vickers machine gun]]s and two [[Lewis gun]]s mounted on the forward cabane crossbar, firing at an upward angle to avoid the propeller disc. The mounting provided three positions in elevation and limited sideways movement.<ref name="Davis p128"/><ref name="Robertson p105">Robertson 1970, p. 105.</ref> In service, however, the Lewis guns proved unpopular, as they were difficult to aim and tended to swing into the pilot's face.<ref name="Mason p105"/> Pilots also feared that the gun butts would inflict serious head injuries in the event of a crash. Most pilots therefore discarded the Lewis guns,<ref name="Mason p105"/> though a minority retained one or both guns specifically for [[Schräge Musik|attacking high altitude reconnaissance aircraft from below]].

Pilots of No. 87 Squadron, including Arthur Vigers, experimentally fitted some aircraft with two forward firing, unsynchronized Lewis guns mounted on top of the lower wing, just inboard of the inner wing struts.<ref name="Mason p105"/> These guns could fire incendiary ammunition, which could not be used with the synchronized Vickers guns.<ref name="Robertson p105"/> However, the 97-round ammunition drums could not be changed once empty, nor could the pilot clear gun jams. This field modification did not become standard.

===Postwar service===
[[File:Dolphin (Canadian Air Force).jpg|thumb|left|[[Canadian Air Force (1918-1920)|Canadian Air Force]] Dolphins of No. 1 (Fighter) Squadron at RAF Upper Heyford, December 1918]]
Dolphins were quickly retired after the war. Nos. 19 and 87 Squadrons demobilized in February 1919, followed by No. 23 Squadron in March.<ref name="Davis p135">Davis 1999, p. 135.</ref> The last RAF unit to operate Dolphins was No. 79 Squadron, based at [[Bickendorf]], Germany, as part of the [[British Army of the Rhine|army of occupation]]. The squadron demobilized in July 1919.<ref name="Davis p135"/><ref name="Franks p18">Franks 2002, p. 18.</ref> The Dolphin was declared obsolete on 1 September 1921.

No. 1 (Fighter) Squadron of the Canadian Air Force, which operated Dolphins and [[Royal Aircraft Factory S.E.5|S.E.5a]]s, [[Sopwith Snipe]]s and captured [[Fokker D.VII]]s,<ref name= "Milberry p. 160">Milberry 2008, p. 160.</ref> was disbanded on 28 January 1920.<ref>Payne 2006, p. 47.</ref> Although retired from Canadian Air Force service, a small number of Dolphins were sent back to Canada.<ref name="Connors p. 12">Connors 1976, p. 12.</ref>

One Dolphin was converted for civilian use. In 1920, Handley Page Ltd. obtained D5369 and operated it as a demonstrator under the civil registration G-EATC.<ref name="Bruce p. 150"/><ref name="Connors p. 12"/> This aircraft was sold in 1923.

The Polish Air Force operated 10 Dolphins during the [[Polish-Soviet War]]. From August 1920, these aircraft were primarily used for ground attack duties in the [[Battle of Warsaw (1920)|Battle of Warsaw]] and other actions. They were soon grounded due to lack of spare parts.<ref name="Davis p135"/>

In October 1920, two Polish Dolphins were loaned to the [[Ukrainian Air Force]] (1. ''Zaporoska Eskadra Ukraińska'') for use against the Soviets. Both aircraft were returned to the Poles in February 1921.<ref>Kopañski 2001, pp. 11–40.</ref>

==Production and planned developments==
A total of 2,072 Dolphin Mk I aircraft were produced by Sopwith, [[Darracq Motor Engineering Company]] and [[Hooper (coachbuilder)|Hooper & Co]].<ref name="Mason p105"/> Approximately 1,500 Dolphins were stored awaiting engines at the time of the [[Armistice with Germany (Compiègne)|Armistice]]. These incomplete airframes were eventually scrapped.<ref name="Mason p106">Mason 1992, p. 106.</ref>

Two developments of the Dolphin were planned. The French firm SACA (''Société Anonyme des Constructions Aéronautiques'') commenced licensed production of the '''Dolphin Mk II''' in 1918.  The RAF expressed no interest in this variant, which was intended for the French ''[[History of the Armée de l'Air (1909-1942)|Aéronautique Militaire]]'' and the [[US Army Air Service]].<ref name="Davis p134">Davis 1999, p. 134.</ref><ref name="Bruce v3p20">Bruce 1969, p. 20.</ref> The Mk II's 300&nbsp;hp direct-drive Hispano-Suiza 8F gave a maximum speed of 225&nbsp;km/h (140&nbsp;mph) and a ceiling of 8,047 m (24,600&nbsp;ft). The new engine had a displacement of 18.5 litres (1,129 in<sup>3</sup>) and required an enlarged, bulbous cowling that fully enclosed the guns.<ref name="Bruce p. 150">Bruce 1961, p. 150.</ref> The Mk II also featured an additional fuel tank, a variable incidence tailplane, strengthened airframe and longer exhaust pipes.<ref name="Davis p134"/> The Air Service anticipated delivery of over 2,000 Mk II aircraft by the summer of 1919, but only a few were delivered before the Armistice.

Meanwhile, persistent difficulties with the geared 200&nbsp;hp Hispano-Suiza 8B prompted development of the '''Dolphin Mk III''', which used a direct-drive version of the 200&nbsp;hp engine.<ref name="Cooksley p34"/> The Mk III first flew in October 1918 and went into production just as hostilities ended.<ref name="Cooksley p34"/> Many existing Dolphins were also converted to Mk III standard at aircraft repair depots by removing the reduction gearing and fitting a modified cowling to accommodate the resultant lowered thrustline.<ref name="Davis p135"/>

==Survivors==
[[File:MichaelBeethamConservation-02.jpg|thumb|Dolphin Mk I under restoration at the Michael Beetham Conservation Centre, part of the [[Royal Air Force Museum Cosford]]]]
No complete Dolphin is known to have survived to the present. However, a composite Dolphin Mk I was rebuilt at the [[Royal Air Force Museum Cosford#Michael Beetham Conservation Centre|Michael Beetham Conservation Centre]], part of the [[Royal Air Force Museum Cosford]]. The airframe is based on an original 6&nbsp;ft length of rear fuselage from serial no. C3988. It includes many other original parts, including a fuel tank, wheels, radiators, tailplane, and elevators from serial nos. D5329 and C4033. In March 2012, the Dolphin went on display in the Grahame White Hangar, at the [[Royal Air Force Museum London]].

==Reproduction==
[[File:ORA's Sopwith Dolphin in flight.jpg|thumb|right|Old Rhinebeck Aerodrome's Sopwith Dolphin reproduction in flight, early 1980s]]
In 1977, [[Cole Palen]] built an accurate Dolphin reproduction for his [[Old Rhinebeck Aerodrome]] living aviation museum, in upstate New York. It flew regularly at Old Rhinebeck's weekend airshows until September 1990, when it crash-landed after a fuel pump failure.<ref>King and Wilkinson 1997, pp. 215–224.</ref> The aircraft is presently under restoration to flying condition.<ref>{{cite web |url=http://oldrhinebeck.org/ORA/work-proceeds-on-rebuild-of-oras-sopwith-dolphin/#more-3109 |title=Work Proceeds on Rebuild of ORA’s Sopwith Dolphin |author=<!--Staff writer(s); no by-line.--> |date=31 March 2015 |website=oldrhinebeck.org |publisher=Cole Palen's Old Rhinebeck Aerodrome |access-date=17 June 2015 |quote=}}</ref>

==Variants==
;Dolphin Mk I
:Main production version. Powered by a geared 200 hp (149 kW) Hispano-Suiza 8B.
;Dolphin Mk II
:Manufactured under license in France. Powered by a direct-drive 300 hp (224 kW) Hispano-Suiza 8F.
;Dolphin Mk III
:Powered by a direct-drive 200 hp (149 kW) Hispano-Suiza 8B.

==Operators==
;{{flag|Canada|1868}}:
* [[Canadian Air Force (1918-1920)|Canadian Air Force]]
** No. 1 (Fighter) Squadron
;{{flag|Second Polish Republic}}:
* [[Polish Air Force]] (postwar, donated by United Kingdom, operated 1920–1923)
** 19. ''Eskadra Myśliwska''

;[[File:Ukraine1918.png|22px]] [[Ukrainian People's Republic]]
* [[Ukrainian Air Force]] (postwar, two aircraft loaned by Poland in October 1920, returned to Poland in February 1921)
** 1. ''Zaporoska Eskadra Ukraińska''

;{{UK}}:
* [[Royal Flying Corps]]/[[Royal Air Force]]
** [[No. 19 Squadron RAF|No. 19 Squadron]]
** [[No. 23 Squadron RAF|No. 23 Squadron]]
** [[No. 56 Squadron RAF|No. 56 Squadron]] (operational trials only)
** [[No. 79 Squadron RAF|No. 79 Squadron]]
** [[No. 85 Squadron RAF|No. 85 Squadron]]
** [[No. 87 Squadron RAF|No. 87 Squadron]]
** [[No. 90 Squadron RAF|No. 90 Squadron]]
** [[No. 91 Squadron RAF|No. 91 Squadron]]
** [[No. 141 Squadron RAF|No. 141 Squadron]]

;{{USA}}
*[[American Expeditionary Force]]
*[[United States Army Air Service]]

==Specifications (Dolphin Mk I)==
[[File:Sopwith 5.F.1 Dolphin dwg.jpg|thumb|Sopwith 5.F.1 Dolphin drawing (prototype shown)]]
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|ref=
|crew=1
|capacity=
|length main=6.78 m
|length alt=22 ft 3 in
|span main= 9.91 m
|span alt= 32 ft 6 in
|height main= 2.59 m
|height alt= 8 ft 6 in
|area main= 24.4 m²
|area alt= 263 ft²
|airfoil=
|empty weight main= 641 kg
|empty weight alt= 1,410 lb
|loaded weight main=  
|loaded weight alt=   
|useful load main= 
|useful load alt= 
|max takeoff weight main= 890 kg
|max takeoff weight alt= 1,959 lb
|more general=
|engine (prop)= [[Hispano-Suiza 8]]B
|number of props=1
|power main= 149 kW
|power alt= 200 hp
|power original=

|max speed main= 211 km/h
|max speed alt= 131 mph
|max speed more= at sea level
|cruise speed main=  
|cruise speed alt=  
|never exceed speed main=  
|never exceed speed alt=  
|stall speed main=  
|stall speed alt=  
|range main= 315 km
|range alt= 195 mi
|ceiling main= 6,100 m
|ceiling alt= 20,000 ft
|climb rate main= 12 min 5 sec to 3,048 m
|climb rate alt= 10,000 ft
|loading main= 36.5 kg/m²
|loading alt= 7.45 lb/ft²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main= 0.232 kW/kg
|power/mass alt= 0.102 hp/lb
|more performance= 
|armament=
* 2× 0.303 in (7.7 mm) [[Vickers machine gun]]s; up to 2× 0.303 in (7.7 mm) [[Lewis gun]]s
* Up to four 25 lb bombs.
|avionics=
}}

==See also==
{{aircontent
<!-- designs which were developed into or from this aircraft: -->
|related=

<!-- aircraft similar in appearance or function to this design: -->
|similar aircraft=
* [[Airco DH.5]]

<!-- the manufacturer or operator (military etc) sequence this aircraft belongs in: -->
|sequence=

<!-- any lists that are appropriate: -->
|lists=
*[[List of aircraft of the Royal Air Force]]

<!-- other articles that could be useful to connect with: -->
|see also=

}}

==References==

===Notes===
{{Reflist|3}}

===Bibliography===
{{Refbegin}}
* Bruce, J.M. "The Sopwith Dolphin." ''Aircraft in Profile, Volume 8''. New York: Doubleday & Company, Inc., 1970. ISBN 0-85383-016-9.
* Bruce, J.M. "The Sopwith 5F.1 Dolphin." ''Air Pictorial''. Vol. 23, No. 5, May 1961.
* Bruce, J,M. ''War Planes of the First World War: Volume Three: Fighters''. London: Macdonald, 1969, ISBN 0-356-01490-8.
* Connors, John F. "The 11th Hour Sopwiths." ''Wings'', Volume 6, No. 1, February 1976.
* Cooksley, Peter. ''Sopwith Fighters in Action (Aircraft No. 110).'' Carrollton, Texas: Squadron/Signal Publications, 1991. ISBN 0-89747-256-X.
* Davis, Mick. ''Sopwith Aircraft''. Ramsbury, Marlborough, Wiltshire: Crowood Press, 1999. ISBN 1-86126-217-5.
* Franks, Norman. ''Dolphin and Snipe Aces of World War I (Aircraft of the Aces No. 48).'' Oxford: Osprey Publishing, 2002. ISBN 1-84176-317-9.
* King, Richard and Stephan Wilkinson. ''The Skies Over Rhinebeck: A Pilot's Story.'' Visalia, California: Jostens, 1997. ISBN 0-96613-350-1.
* Kopañski, Tomasz Jan. ''Samoloty brytyjskie w lotnictwie polskim 1918–1930 (British Aircraft in the Polish Air Force 1918–1930)'' (in Polish). Warsaw: Bellona, 2001. ISBN 83-11-09315-6.
* Lamberton, W.M., and E.F. Cheesman. ''Fighter Aircraft of the 1914–1918 War''. Letchworth, UK: Harleyford, 1960. ISBN 0-900435-01-1.
* Mason, Francis K. ''The British Fighter Since 1912.'' Annapolis, Maryland: Naval Institute Press, 1992. ISBN 1-55750-082-7.
* Milberry, Larry. ''Aviation in Canada: The Pioneer Decades''. Toronto: CANAV Books, 2008. ISBN 0-921022-19-0.
* Milberry, Larry. ''Sixty Years: The RCAF and Air Command 1924–1984''. Toronto: CANAV Books, 1984. ISBN 0-9690703-4-9.
* Payne, Stephen, ed. ''Canadian Wings: A Remarkable Century of Flight''. Vancouver: Douglas & McIntyre, 2006. ISBN 1-55365-167-7.
* Robertson, Bruce. ''Sopwith – The Man and His Aircraft''. London: Harleyford, 1970. ISBN 0-900435-15-1.
* Shores, Christopher F. et al. ''Above the Trenches: A Complete Record of the Fighter Aces and Units of the British Empire Air Forces 1915–1920''. London: Grub Street, 1990. ISBN 978-0-948817-19-9.
{{Refend}}

==External links==
{{commons category|Sopwith Dolphin}}
* [http://oldrhinebeck.org/ORA/sopwith-dolphin/ Old Rhinebeck Aerodrome Sopwith Dolphin]
* [http://oldrhinebeck.org/ORA/work-proceeds-on-rebuild-of-oras-sopwith-dolphin/ Old Rhinebeck's Sopwith Dolphin restoration page]
* [http://www.militaryfactory.com/aircraft/detail.asp?aircraft_id=538 Military Factory article]
* [https://warbirdtails.net/2015/07/31/sopwiths-first-world-war-part-7-the-dolphin/ "Warbird Tales - Sopwith’s First World War – Part 7: The Dolphin"]

{{Sopwith Aviation Company aircraft}}
{{wwi-air}}

[[Category:British fighter aircraft 1910–1919]]
[[Category:Military aircraft of World War I]]
[[Category:Sopwith aircraft|Dolphin]]