{{Infobox airport
| name         = Hattfjelldal Airport
| nativename   = Hattfjelldal flyplass
| image        = Hattfjelldal flyplass 50414.jpeg
| image-width  = 300px
| caption      = The airport in 1948
| image2       = 
| image2-width = 
| IATA         = 
| ICAO         = ENHT
| FAA          =
| TC           =
| LID          =
| GPS          =
| WMO          =
| type         =
| owner-oper   =
| owner        =
| operator     = Hattfjelldal Flyklubb
| city-served  = [[Hattfjelldal]], [[Norway]]
| location     =
| hub          =
| built        = <!-- military airports -->
| used         = <!-- military airports -->
| commander    = <!-- military airports -->
| occupants    = <!-- military airports -->
| metric-elev  = yes
| elevation-f  = 690
| elevation-m  = 210.3
| coordinates  = {{coord|65|35|42|N|13|59|24|E|display=inline,title}}
| website                =
| image_map              =
| image_mapsize          =
| image_map_alt          =
| image_map_caption      =
| pushpin_map            = Norway
| pushpin_label_position =
| pushpin_label          = ENHT
| pushpin_map_alt        =
| pushpin_mapsize        =
| pushpin_image          =
| pushpin_map_caption    =
| pushpin_relief         = yes
| r1-number    = 09–27
| r1-length-f  =
| r1-length-m  = 715
| r1-surface   = Concrete <!-- up to r8 -->
| metric-rwy   = yes
| h1-number    =
| h1-length-f  =
| h1-length-m  =
| h1-surface   = <!-- up to h12 -->
| stat1-header =
| stat1-data   = <!-- up to stat4 -->
| stat-year    =
| footnotes    =
}}

'''Hattfjelldal Airport''' ({{lang-no|Hattfjelldal flyplass}}) is a [[general aviation]] airport located in [[Hattfjelldal]], [[Norway]]. The first simple landing field was constructed in 1933. During [[World War II]] it was upgraded by the [[Luftwaffe]] to a concrete {{convert|930|m|sp=us|adj=on}} concrete runway and it served as a strategic airfield during the [[Operation Weserübung|occupation of Norway]], mostly for a stopovers. The concrete surface built by the Germans is still in use today and is operated by Hattfjelldal Flyklubb.

==History==
[[File:Hattfjelldal Airport 1933.png|thumb|left|The opening of the airport in 1933]]
The airport was created as a simple landing field in 1933.<ref name=lex>{{cite encyclopedia |first=Harald |last=Krogtoft |author2=Billy Jacobsen |authorlink= | encyclopedia=NRK Nordland Fylkesleksikon |title=Flyplassen i Hattfjelldal |editor=[[Hans-Tore Bjerkaas]] |url=http://www.nrk.no/nordland/fylkesleksikon/index.php/Flyplassen_i_Hattfjelldal |accessdate=13 March 2013 | year=2009 |publisher=[[Norwegian Broadcasting Corporation]] |location= |isbn= |pages= |language=Norwegian }}</ref> With the break-out of World War II the airport was used by the [[Norwegian Army Air Service]] as a stop-over for flights heading to Northern Norway.<ref name=gynnild>{{cite web |url=http://avinor.moses.no/index.php?seks_id=135&element=kapittel&k=2 |title=Flyplassenes og flytrafikkens historie |work=Kulturminner på norske lufthavner – Landsverneplan for Avinor |publisher=[[Avinor]] |last=Gynnild |first=Olav |year=2009  |accessdate=25 January 2012 |archivedate=25 January 2012 |archiveurl=http://www.webcitation.org/6DwQ1MXLE |deadurl=no}}</ref> After the German forces took control of the area in 1940 they immediately started construction of a wooden runway.<ref name=herfra>{{cite news |url=http://www.nrk.no/nordland/tysk-betongflyplass-fortsatt-i-bruk-1.7413049 |title=Herfra bombet tyskerne Narvik |last=Krogtoft |first=Harald |date=6 December 2010 |publisher=[[Norwegian Broadcasting Corporation]] |language=Norwegian |accessdate=28 September 2013}}</ref> More than a thousand people participated in the construction.<ref name=gynnild /> This allowed them to use the airfield as a stopover for [[Junkers Ju 87]]s, especially for bombing raids during the [[Battles of Narvik]].<ref name=herfra /> Throughout the war the airport saw daily landings of bombers in transit between Southern and Northern Norway.<ref name=gynnild />

To improve the airport, it was upgraded with a concrete runway, which was completed in 1943.<ref name="lex"/> Prior to the war the only structures at Hattfjelldal were a church and some farms. By the end of the war there were significant structures which had been built at and around the airport, which were taken over by the community and became the municipal hall, school and community center. A major employer, Arbor, established itself in the hangar during the 1950s.<ref name=herfra />

An aviation club, Hattfjeldal flyklubb, was established in 1948. Activity was limited until 1960, when the club bought its first aircraft, an [[Taylorcraft Auster|Auster Mark 5]]. With the assistance of the aviation club in Bodø, Hattfjelldal flyklubb was able to establish pilot's training for the first time in 1962. Training was conducted on Bodø's ski-equipped [[Piper Cub]]. Three of the four pupils passed their exams. The club crashed their Auster in [[Rana, Norway|Rana]] on 11 March 1963 and after that bought a [[Piper Super Cub]], which could be equipped with wheels, skis and floats. Additional training commenced in 1972 with a leased [[Cessna 150]], and later the same year the club bought a used [[Cessna 172]]. The latter crashed in [[Glomfjorden]] in 1975 and replaced by a new aircraft of the same type, which remains in use by the club. A third and final round of training was organized at Hattfjelldal in 1981.<ref>{{cite web |url=http://www.home.no/jesper-s/klubben.html |title=Om klubben |publisher=Hattfjelldal flyklubb |language=Norwegian |accessdate=28 September 2013}}</ref>

==Facilities==
[[File:Arbor Hattfjelldal.JPG|thumb|The Arbor [[Particle board|chipboard]] plant at Hattfjelldal]].
The airport has a concrete runway which measures {{convert|930|by|50|m|sp=us}} and is {{convert|25|cm|sp=us}} thick.<ref name=lex /> It remains the only unmodified airfield from the war, as all other airfields have either been demolished or modernized.<ref name=herfra /> The hangar at the airport is the site of Arbor, which uses part of the runway as a storage facilities for its output.<ref>{{cite web |url=http://img4.custompublish.com/getfile.php/2139597.2044.budrdqcwvf/Fremtiden+for+flyplass+i+Hattfjelldal.pdf?return=www.hattfjelldal-kommune.no |title=Fremtiden for flyplassen i Hattfjelldal som sted for utvikling of formidling |last=Rasmusen |first=Bjørn |date=29 September 2011 |language=Norwegian |format=PDF |accessdate=28 September 2013}}</ref>

==References==
{{commons category|Hattfjelldal Airport}}
{{reflist}}

{{Airports in Norway}}
{{Portal bar|Aviation|Norway}}

[[Category:Airports in Nordland]]
[[Category:Norwegian Army Air Service stations]]
[[Category:Luftwaffe airports in Norway]]
[[Category:Hattfjelldal]]
[[Category:Airports established in 1933]]
[[Category:1933 establishments in Norway]]
[[Category:Military installations in Nordland]]