{{Infobox journal
| title = British Journal of Politics and International Relations
| cover = 
| editor = John Peterson<br />Alan Convery
| discipline = [[Political science]]
| former_names = 
| abbreviation = Br. J. Polit. Int. Relat.
| publisher = [[SAGE Publications]] on behalf of the [[Political Studies Association]]
| country = United Kingdom
| frequency = Quarterly
| history = 1999-present
| openaccess = 
| license = 
| impact = 1.423
| impact-year = 2015
| website = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-856X
| link1 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-856X/currentissue
| link1-name = Online access
| link2 = http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-856X/issues
| link2-name = Online archive
| link3= http://www.psa.ac.uk/members/membership-benefits/publications/bjpir
| link3-name = Journal page at society website
| JSTOR = 
| OCLC = 49056411
| LCCN = sn99033216
| CODEN = 
| ISSN = 1369-1481
| eISSN = 1467-856X
}}
The '''''British Journal of Politics and International Relations''''' is a  quarterly [[peer-reviewed]] [[academic journal]] published by [[SAGE Publications]] on behalf of the [[Political Studies Association]]. It was established in 1999.<ref name="edpsa">{{cite web |url=http://www.psa.ac.uk/members/membership-benefits/publications/bjpir/editorial-statement |title=Editorial Statement |publisher=Political Studies Association |accessdate=25 June 2013}}</ref>

Until 2016, the journal was published by [[Wiley-Blackwell]]. As of February 2015, its [[editors-in-chief]] are John Peterson and Alan Convery ([[University of Edinburgh]]).

According to the ''[[Journal Citation Reports]]'', the journal has a 2015 [[impact factor]] of 1.423, ranking it 41st out of 163 journals in the category "Political Science"<ref name=WoS1>{{cite book |year=2016 |chapter=Journals Ranked by Impact: Political Science |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref> and 21st out of 86 journals in the category "International Relations".<ref name=WoS2>{{cite book |year=2016 |chapter=Journals Ranked by Impact: International Relations |title=2015 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Social Sciences |series=[[Web of Science]]}}</ref>

== See also ==
* [[List of political science journals]]
* [[List of international relations journals]]

== References ==
{{reflist|30em}}

== External links ==
* {{Official website|http://onlinelibrary.wiley.com/journal/10.1111/(ISSN)1467-856X}}

{{DEFAULTSORT:British Journal of Politics and International Relations, The}}
[[Category:International relations journals]]
[[Category:SAGE Publications academic journals]]
[[Category:English-language journals]]
[[Category:Quarterly journals]]
[[Category:Publications established in 1999]]
[[Category:Academic journals associated with learned and professional societies of the United Kingdom]]


{{poli-journal-stub}}