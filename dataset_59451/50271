{{Infobox company 
|name = First Gulf Bank PJSC
|logo = FGB's new logo.jpeg
|type = [[Public company|Public]]
|traded_as = {{ADSM|FGB}}
|foundation = 1979
|location = [[Abu Dhabi]], [[UAE]]
|num_employees = 2,000 
|industry = [[Financial Services]] 
|products = [[Wholesale banking]]<br/>[[Consumer Banking]] <br/>Treasury and investments
|slogan = "Be first"
|homepage = http://www.fgb.ae/
}}

'''FGB''' (formerly known as '''First Gulf Bank''') is currently the third largest bank by assets in the [[United Arab Emirates]] (UAE).<ref>[http://www.reuters.com/article/2014/03/06/firstgulfbank-sukuk-idUSL3N0M32B420140306 First Gulf Bank to set up $1.07 billion sukuk in Malaysia]: ''Reuters, March 6, 2014''</ref> Established in 1979, FGB is headquartered in the [[emirate]] of [[Abu Dhabi]].

FGB offers a wide range of financial services in the wholesale, consumer and treasury banking sectors, including [[Islamic banking]] and [[bancassurance]] solutions for businesses and consumers via a network of branches across the UAE. Internationally, FGB has a branch in Singapore - that includes global wealth management services and a branch in Qatar, representative offices in London, India, Hong Kong and Seoul, South Korea, and a subsidiary in Libya.

In 2013, the bank restructured its business into three divisions: the Wholesale Banking Group, the Consumer Banking Group and the Treasury & Global Markets Group. In the same year, FGB also announced that it had acquired full ownership of [[Shariah]]-compliant finance company Aseel Islamic Finance<ref>[http://www.thenational.ae/business/industry-insights/finance/first-gulf-bank-taps-rising-demand-for-sharia-finance-with-purchase-of-aseel First Gulf Bank taps rising demand for Sharia finance with purchase of Aseel]: ''The National, October 26, 2013''</ref> and consumer finance firm Dubai First.<ref>[http://gulfnews.com/business/banking/first-gulf-bank-completes-acquisition-of-dubai-first-1.1252107 First Gulf Bank completes acquisition of Dubai First]: ''Gulf News, November 6, 2013''</ref>

In December 2016, shareholders approved the bank's plans to merge with the [[National Bank of Abu Dhabi|National Bank of Abu Dhabi (NBAD)]]<ref name="Gulf News FGB Merger 2016">[http://gulfnews.com/business/sectors/banking/shareholders-of-nbad-fgb-approve-merger-1.1941936 Shareholders of NBAD, FGB approve merger]:''Gulf News, December 7, 2016''</ref> and both banks finalised the second-tier management by February 2017.<ref>[http://www.emirates247.com/business/economy-finance/ahead-of-merger-uae-banks-nbad-fgb-finalise-second-tier-management-2017-02-07-1.647762 Ahead of merger, UAE banks NBAD, FGB finalise second-tier management]:''Emirates 24|7, February 7, 2017''</ref> The plan to merge the banks, which was first announced in June this year, will be executed through a share swap, with FGB shareholders receiving 1.254 NBAD shares for each FGB share they hold. The merged entity will continue to be known as NBAD and will be the largest bank in Middle East and North Africa.<ref name="Gulf News FGB Merger 2016" />

==Growth and successes==

FGB has [[Shareholder Equity]] of AED 34.1 billion as of December 31, 2014,<ref>[http://www.zawya.com/story/FGB_2014_Net_Profit_at_AED_566_Billion_up_18_from_2013-ZAWYA20150128133233/ FGB 2014 Net Profit at AED 5.66 Billion, up 18% from 2013]: ''Zawya, January 28, 2015''</ref> making it one of the largest equity based banks in the UAE.

FGB reported a total net profit of AED 5.66 billion in 2014, which represents an 18% increase when compared with AED 4.77 billion achieved in 2013.<ref>[http://www.thenational.ae/business/banking/record-profit-for-big-abu-dhabi-banks Record profit for big Abu Dhabi banks]: ''The National, January 28, 2015''</ref> 2014 also marked the 15th consecutive year of uninterrupted net profit growth for the bank. {{Citation needed|date=October 2015}}

The bank’s consistent performance led to a Financial Strength Rating of 'A+' from Capital Intelligence in 2014.<ref>[http://www.ciratings.com/news/2014/06/25/1416 First Gulf Bank - Ratings Affirmed with Stable Outlooks]: ''Capital Intelligence, June 25, 2014''</ref>
In 2013, RAM Ratings provided the bank with an AAA grade,<ref>[http://www.ram.com.my/pressReleaseView.aspx?ID=fc9688bc-a391-4635-8c1b-e2365a1f9a93 RAM Ratings assigns AAA/Stable/P1 ratings to First Gulf Bank, the UAE’s third-largest bank]: ''RAM Ratings, March 6, 2014''</ref> and FGB’s Long Term Rating was affirmed at A+ by [[Fitch ratings|Fitch]].<ref>[http://www.reuters.com/article/2013/04/08/fitch-affirms-first-gulf-bank-at-a-outlo-idUSFit65403520130408 RPT-Fitch Affirms First Gulf Bank at 'A+'; Outlook Stable; Upgrades VR to 'bbb']: ''Reuters, April 8, 2013''</ref>

[[Forbes]] Middle East ranked FGB as the 8th most powerful company and 4th leading bank in its 2014 ‘Top 500 companies in the Arab World’ list.<ref>[http://ameinfo.com/blog/banking/fgb-ranked-top-10-companies-named-4th-leading-bank-arab-world-forbes-middle-east/ FGB ranked in top 10 companies and named 4th leading bank in the Arab world by Forbes Middle East]: ''AME Info, June 19, 2014''</ref> FGB was also named the ‘Best Bank in the United Arab Emirates’ and ‘Best Wealth Management Firm’ at the [[The Banker|Banker]] Middle East Industry Awards 2014.<ref>[https://www.zawya.com/story/FGB_named_Best_Bank_in_United_Arab_Emirates_and_Best_Wealth_Management_firm-ZAWYA20140604101745/ FGB named 'Best Bank in United Arab Emirates' and 'Best Wealth Management firm']: ''Zawya, June 4, 2014''</ref>

==International activities==

Throughout 2014, FGB enhanced its presence in the Asia Pacific market with the launch of a new representative office in South Korea <ref>[http://gulfnews.com/business/banking/fgb-begins-operations-in-seoul-1.1359867 FGB begins operations in Seoul]: ''Gulf News, July 14, 2014''</ref> and a Global Wealth Management service at its Singapore branch.<ref>[http://gulfnews.com/business/banking/fgb-launches-global-wealth-management-service-in-singapore-1.1328404 FGB launches Global Wealth Management service in Singapore]: ''Gulf News, May 5, 2014''</ref> The bank also opened a new representative office in London and has announced plans to open a new office in China in the future.<ref>[http://gulfnews.com/business/banking/fgb-s-wholesale-group-to-take-part-in-deals-worth-15b-1.1286564 FGB’s wholesale group to take part in deals worth $15b]: ''Gulf News, February 4, 2014''</ref>

FGB’s other international activities in 2014 include the conclusion of a Negotiable Certificate of Deposit (NCD) programme via its Singapore branch <ref>[http://gulftoday.ae/portal/229df30b-56df-4200-9596-a7e601774eb9.aspx FGB sets up $1b Singapore programme to fund Asian expansion]: ''Gulf Today, April 16, 2014''</ref> and the issuance of the bank’s debut 250 million Australian dollar (USD 228.35 million) ‘Kangaroo’ 5-year bond.<ref>[https://www.bloomberg.com/news/2014-03-24/first-gulf-bank-follows-nbad-with-middle-eastern-kangaroo-bond.html First Gulf Bank Follows NBAD With Middle Eastern Kangaroo Bond]: ''Bloomberg, March 24, 2014''</ref> FGB has also received approval for a [[Sukuk]] issuance in Malaysia <ref>[http://gulftoday.ae/portal/fdc44d7e-b3dc-4c7c-9773-13c72878fd10.aspx FGB to set up $1.07b sukuk in Malaysia]: ''Gulf Today, March 7, 2014''</ref> and became the first [[MENA]] [[issuer]] in the Tokyo Pro-Bond Market, selling a debut 10 billion yen (USD 98.2 million) bond.<ref>[http://www.wam.ae/en/news/economics/1395267118917.html FGB concludes 5-year bond on new Tokyo Pro-Bond Market]: ''WAM, June 26, 2014''</ref>

==New brand==

In April 2014, FGB, formerly known as First Gulf Bank, unveiled its current logo after announcing a shift in the bank’s corporate identity.<ref>[http://www.thenational.ae/business/industry-insights/finance/new-name-and-growth-focus-for-first-gulf-bank New name and growth focus for First Gulf Bank]: ''The National, April 20, 201''4</ref> As part of the rebrand, the bank adopted the acronym, ‘FGB’, which was already widely used by its stakeholders. FGB remains First Gulf Bank in Arabic however, and no changes have been made to the bank as a legal entity.

FGB announced to media that the rebrand was part of a long-term strategy to grow the bank locally and internationally, and reflects its UAE and Abu Dhabi heritage, commitment to [[Emirati]] development and aspirations and ambitions for further success and growth in the future.{{Citation needed|date=October 2015}}

==Sponsorships==

FGB is a sponsor of [[Ferrari World Abu Dhabi]] (FWAD), the largest indoor and the only [[Ferrari]]-branded theme park in the world. FGB sponsors the Junior GT ride at FWAD, which is part of its driving school where children receive their first training about road safety and discipline and learn about the essential rules they will need to abide by once they start driving.

The bank also sponsors the [[Al Ain]] Sports & Cultural Club and in 2014, launched the ‘FGB Arena’,<ref>[http://www.albawaba.com/business/pr/fgb-arena-abu-dhabi--552248 Inaugurating the ‘FGB Arena’ in Abu Dhabi]: ''AlBawaba, February 4, 2014''</ref> which is located in [[Zayed Sports City]], Abu Dhabi. The ‘FGB Arena’ is home to a range of regular programmes, from sports and music events through to conferences and exhibitions. Managed by an operator in co-operation with FGB, the venue can accommodate up to 6,000 spectators.

==Executives==

'''Board of Directors'''

H.H. Sheikh Tahnoon Bin Zayed Al Nahyan – Chairman
 
Ahmed Ali Al Sayegh - Vice Chairman

Khaldoon Khalifa Al Mubarak - Board Member

Sultan Khalfan Al Ktebi - Board Member

Mohammed Saif Al Suwaidi - Board Member

Jassim Al Siddiqi - Board Member

Abdulhamid Mohammed Saeed - Member & Managing Director

'''Management Team'''

André Sayegh	- Chief Executive Officer

Zulfiqar Sulaiman	- Chief Operating Officer

Arif Shaikh	- Chief Risk Officer

Karim Karoui	- Chief Financial Officer

Sara Al Binali	- Head of Strategic Planning

Shirish Bhide	- Acting Head of Wholesale Banking Group

Hana Al Rostamani	- Head of Consumer Banking Group

TG Ramani	- Acting Head of Treasury & Global Markets Group

Melvin Fraser	- Acting Head of Human Resources

Rajesh Deshpande	- Acting Head of Wholesale Credit Group

Jasim Al Ali	- Chief Executive Officer, First Gulf Properties & Mismak Properties

Nurendra Perera	- Head of Internal Audit

==External links==
*[http://www.fgb.ae/en Official website]
*[http://www.aseelfinance.ae/ Aseel Islamic Finance]
*[http://www.dubaifirst.com/ Dubai First]
*[http://www.myfgbcard.com/ FGB Credit cards]
*[http://www.fgbwealth.com/ FGB Wealth management products]

==References==
{{Reflist}}

[[Category:Banks of the United Arab Emirates]]
[[Category:Companies based in Abu Dhabi]]
[[Category:Companies listed on the Abu Dhabi Securities Market]]