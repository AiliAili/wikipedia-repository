{{Infobox journal
| title = Journal of Solid State Chemistry
| cover = 
| editor = M.G. Kanatzidis
| discipline = [[Solid-state chemistry]]
| former_names = 
| abbreviation = J. Solid State Chem.
| publisher = [[Elsevier]]
| country = 
| frequency = Monthly
| history = 1969-present
| openaccess = 
| license = 
| impact = 2.133
| impact-year = 2014
| website = http://www.journals.elsevier.com/journal-of-solid-state-chemistry/
| link1 = http://www.sciencedirect.com/science/journal/00224596
| link1-name = Online access
| link2 = 
| link2-name = 
| JSTOR = 
| OCLC = 36946153
| LCCN = 71010527
| CODEN = JSSCBI
| ISSN = 0022-4596
| eISSN = 
}}
The '''''Journal of Solid State Chemistry''''' is a monthly [[Peer review|peer-reviewed]] [[scientific journal]] published by [[Elsevier]]. The journal covers the [[chemical property|chemical]], [[structural engineering|structural]], [[thermodynamic]], [[solid state electronics|electronic]], and [[electromagnetism|electromagnetic]] characteristics and properties of [[solid]]s, including [[Ceramic materials|ceramics]] and [[amorphous solid|amorphous materials]]. The [[editor-in-chief]] is [[M.G. Kanatzidis]] ([[Northwestern University]]).

== Abstracting and indexing ==
This journal is abstracted and indexed by:
* [[BioEngineering Abstracts]]
* [[Chemical Abstracts Service]]
* Coal Abstracts - [[International Energy Agency]]
* [[Current Contents]]/Physics, Chemical, & Earth Sciences
* [[Engineering Index]]
* [[Science Abstracts]]
* [[Science Citation Index]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2014 [[impact factor]] of 2.133.<ref name=WoS>{{cite book |year=2015 |chapter=Journal of Solid State Chemistry |title=2014 Journal Citation Reports |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References ==
{{reflist}}

== See also ==
* [[Solid-state chemistry]]

== External links ==
{{Official website|http://www.journals.elsevier.com/journal-of-solid-state-chemistry/}}

[[Category:Elsevier academic journals]]
[[Category:Materials science journals]]
[[Category:Chemistry journals]]
[[Category:Engineering journals]]
[[Category:Publications established in 1969]]
[[Category:Monthly journals]]