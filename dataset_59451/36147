{{good article}}
{{Infobox President
| name          = Alphonse Amadou Alley
| image         = Alphonse Alley.gif
| caption       = President Alley
| order         = [[President of Dahomey]]
| term_start    = December 19, 1967
| term_end      = July 17, 1968
| predecessor   = [[Maurice Kouandété]]
| successor     = [[Émile Derlin Zinsou]]
| birth_date    = {{birth date|1930|04|09|mf=y}}
| birth_place   = [[Bassila]], [[French Dahomey|Dahomey]]
| death_date    = {{death date and age|1987|03|28|1930|4|9}}
| death_place   = 
| occupation    = military officer
| spouse        = 
| party         = 
| religion      =
}}
'''Alphonse Amadou Alley''' (April 9, 1930 – March 28, 1987) was a [[Benin]]ese army officer and [[political figure]]. He was most active when his country was known as [[Republic of Dahomey|Dahomey]]. He was born in [[Bassila]], central Dahomey, and enrolled in schools in [[Togo]], [[Cote d'Ivoire]], and [[Senegal]] before enlisting in the [[French army]] in 1950. He saw combat in Indochina from 1950 to 1953, in Morocco from 1955 to 1956, and in Algeria from 1959 to 1961. After the [[1965 Dahomeyan coup d'état|coup in 1965]], President [[Christophe Soglo]] promoted Alley [[Military of Benin|Chief of Staff of the Army]]. Young army officer [[Maurice Kouandété]] was appointed Alley's ''chef de cabinet'' in 1967.

Kouandété launched another coup against Soglo on December 17, but he was forced to hand power to Alley two days later. His administration oversaw the creation of a new constitution and a presidential election, Dahomey's first since 1964. The results were annulled because of a boycott that prevented almost three-quarters of the country from voting. Alley lost popularity with the suggestion that the military should retreat back to the barracks, and was eventually reduced to a [[Spokesperson|mouthpiece]] for Kouandété. On July 17, 1968, Alley was forced to hand power to [[Emile Zinsou]], a veteran politician.

Alley's retirement was marked by a series of discharges from the military, trials, and prison sentences. At one trial, Zinsou's conduct sparked another coup led by Kouandété. On October 26, 1972, [[Mathieu Kérékou]] seized power in a coup. He ended Alley's military career, as well as that of every other senior officer, and named Alley commissioner of the National Oil Wells (SNADAH), a role with [[sinecure|very little responsibility]]. Kérékou accused Alley of plotting against him on February 28, 1973, and sentenced the latter to 20 years in prison. He died on March 28, 1987.

==Military background==
[[File:BJ-Bassila.png|thumb|left|90px|Location of Bassila in Dahomey]]
[[File:LocationBenin.svg|thumb|left|150px|Location of Dahomey in Africa]]
Alley was born on April 9, 1930, in [[Bassila]], central Dahomey.<ref name=Decalo13>{{Harvnb|Decalo|1976|p=13}}.</ref> He was a member of the small [[Widji]] ethnic group, based in the north.<ref>{{Harvnb|Daggs|1970|p=249}}.</ref> His father was also a military commander, who served the French in Syria during 1942 and helped train police in Togo. Alphonse enrolled in schools in [[Togo]], [[Cote d'Ivoire]], and [[Senegal]] until he enlisted in the French army in 1950. His first combat operation later that year was at the [[Indochinese Peninsula]] for the [[First Indochina War]]. Alley withdrew in late 1953, shortly before [[Operation Castor]] was launched at [[Dien Bien Phu]]. After this wartime experience, he went the Saint Maxient Non-Commissioned Officer School in France (now the [[ National Active Non-Commissioned Officers School (France)]] or École Nationale des Sous-Officiers d’Actives (ENSOA)). He saw combat in Morocco from 1955 to 1956 and in Algeria from 1959 to 1961, where he became a paratrooper.<ref name=Decalo13/>

After Dahomey gained independence in 1960, Alley travelled back to his homeland and led a paratrooper unit. At first, he was a lieutenant, but he was promoted to captain in 1962 and major in 1964. Later that year he led several soldiers to the Dahomey-Niger border during a border dispute.<ref name=Decalo13/> Historian Samuel Decalo described Alley as "a jovial, dashing, easygoing and well-liked figure" and was known by diplomats as "the wine, women and song officer".<ref name=Decalo14>{{Harvnb|Decalo|1976|p=14}}.</ref>

In Dahomeyan coups in [[1963 Dahomeyan coup d'état|1963]] and [[1965 Dahomeyan coup d'état|1965]], Alley urged General [[Christophe Soglo]] to seize power. After the 1965 coup, Soglo promoted Alley Chief of Staff of the Army. Alley made known his disagreements with Soglo on several occasions, though he remained loyal nonetheless.<ref name=Decalo13/> Young army officer [[Maurice Kouandété]] was appointed Alley's ''chef de cabinet'' in 1967<ref name=Decalo79>{{Harvnb|Decalo|1976|p=79}}.</ref> and his frequent opposition to Alley during staff meetings helped to create factions in the Dahomeyan Army.<ref name=SD462>{{Harvnb|Decalo|1973|p=462}}.</ref>

==1967 coup d'état==
Kouandété had aspirations of his own. On December 17, 1967, he and 60 other soldiers led a military coup and toppled Soglo. Kouandété seized the presidency, though he was unsure what to do with it. Members of his faction urged the new president to remain at his post, though the general public's opinion was against him. Meanwhile, France refused to aid Dahomey and would not recognise Kouandété.<ref name=SD464>{{Harvnb|Decalo|1973|p=464}}.</ref> He was forced to appointed Alley provisional president two days later, although Kouandété had placed Alley under house arrest and accused him of "shirking [his] duties" and maintained a "policy of appeasement."<ref name=Decalo13/><ref name=SD464/> Kouandété served as prime minister thereafter.<ref name="SD464"/>

==President of Dahomey==
Alley was one of the few figures who were trusted by northern and southern Dahomeyans alike.<ref name="Decalo14"/> His role was only temporary, until power was to be ceded back to civilians in six months time.<ref name=voidedresults/> Among the events on the official timetable, which the military published on January 17, 1968, was the creation of a nonmilitary Constitution Commission on January 31, which would write a new Dahomeyan constitution.<ref name=DR205>{{Harvnb|Ronen|1975|p=205}}.</ref> The document granted Alley strong executive power,<ref name=SD465>{{Harvnb|Decalo|1973|p=465}}.</ref> and was adopted by the Comite Militaire Revolutionaire, Alley's interim government comprisising only military officers, in early March. A national referendum on the constitution was held on March 31, which passed with 92 percent in favor.<ref name=DR205/>

The Comite decided to ban all former presidents, vice presidents, government ministers, and National Assembly presidents from [[1968 Dahomeyan presidential election|the upcoming presidential election]]. This was to prevent Dahomeyan politics from repeating its practices of old. The Supreme Court ruled the proscription was 
unconstitutional, although Alley overruled the decision.<ref name=DR205/> He instead only recognised five candidates as legitimate.<ref name=DR206>{{Harvnb|Ronen|1975|p=206}}.</ref>

In response to their disqualification, former presidents [[Hubert Maga]] and [[Sourou-Migan Apithy]] staged protests while [[Justin Ahomadégbé-Tomêtin]], another ex-president, supported an obscure candidate named [[Basile Adjou Moumouni]]. The election was held on May 15, and was Dahomey's first since 1964. Moumouni won the election with 80 percent of the vote, but Alley declared the result void because the protest prevented nearly three-quarters of the electorate from voting.<ref name=voidedresults>{{citation|url=https://query.nytimes.com/mem/archive-free/pdf?res=F50D16FB3D5E147493C1A8178ED85F4C8685F9|title=Results of Election In Dahomey Voided By Military Regime|newspaper=[[The New York Times]]|publisher=[[The New York Times Company]]|date=May 13, 1968|page=17|accessdate=2008-12-13 | format=PDF}}.</ref> This result sparked further demonstrations, and Maga, Apithy, Ahomadégbé-Tomêtin, and former president [[Christophe Soglo]] were forbidden to enter the country, in an attempt to crack down on dissent.<ref>{{citation|url=https://query.nytimes.com/mem/archive-free/pdf?&res=F70A10FC3E5F127A93C5A9178CD85F4C8685F9|author=[[United Press International]]|title=Junta in Dahomey Tightens Control to Quiet Discontent|newspaper=[[The New York Times]]|publisher=[[The New York Times Company]]|date=7 July 1968|page=2|accessdate=2008-12-14 | format=PDF}}.</ref> Alley felt he had made a mistake in disqualifying Maga, Apithy, and Ahomadégbé-Tomêtin, as he believed that only they could bring unity to Dahomey.<ref name=voidedresults/>

In a radio address on May 11, Alley announced that due to the nullification, the military would heve to stay in power beyond June 17. He noted that his administration would require extra time to find a successor who was backed by everyone.<ref name=voidedresults/> Alley suggested that the military should retreat back to the barracks at Camp Ghezo and leave Dahomeyan politics to the career politicians. The view was unpopular, and he was outvoted by his military comrades. Alley eventually became little more than Kouandété's mouthpiece.<ref name=SD465/>

Alley attempted to remove Kouandété from the army, though to no avail.<ref name=Decalo14/> In any case, by June his fellow officers had made up their mind as to the next president.<ref name=DR206/> After talks with unionists, civil servants, and academics,<ref name=SD465/> they "entrust[ed] the reins of power to [[Émile Derlin Zinsou]] for at least five years", who was "charged to form a government of national union", as per a June 28 newspaper article by the state press.<ref name=DR206/> On July 17, Alley handed power to Zinsou, a veteran politician.<ref name=DR206/>

==Later life==
[[File:Mathieu Kérékou 2006Feb10.JPG|thumb|[[Mathieu Kérékou]] in 2006]]
After Alley was retired from the presidency, he was purged from combat in the army and was assigned the new post of military attaché in Washington, D.C, an appointment he refused to accept.<ref name=Decalo14/> General [[Etienne Eyadema]], the president of neighboring Togo, thought that this "serve[d Alley] right, for being stupid enough to give power back to the politicians. Don't think I'm ever going to be that dumb."<ref>{{citation|url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=F00913FC345D13728DDDAC0A94DD405B898AF1D3|title=Togo General, Citing 'Popular Will,' Retains Power|last=Apple|first=R.W.|date=May 25, 1969|newspaper=[[The New York Times]]|publisher=[[The New York Times Company]]|page=16|accessdate=2009-01-11 | format=PDF}}.</ref> Alley was discharged from the armed forces altogether in September,<ref name=DR213>{{Harvnb|Ronen|1975|p=213}}.</ref> with Kouandété taking his place as Chief of Staff.<ref name=Decalo14/>

On July 11, 1969, Kouandété accused Alley of plotting to kidnap and murder him.<ref name=exchiefjailed>{{citation|url=https://query.nytimes.com/mem/archive-free/pdf?_r=1&res=F30910FD3454127B93C7A9178BD95F4D8685F9|title=Dahomey Ex-Chief Jailed|author=[[Associated Press]]|date=October 5, 1969|newspaper=[[The New York Times]]|publisher=[[The New York Times Company]]|page=70|accessdate=2009-01-11 | format=PDF}}.</ref> Facing the death penalty, Alley was sentenced to ten years of hard labor at an open trial held on October 4.<ref name=Decalo14/><ref name=DR213/><ref name=exchiefjailed/> Zinsou had intervened for Alley, and it strained relations between the president and Kouandété.<ref name=Decalo14/> The latter decided to lead another coup on December 10.<ref name=DR212>{{Harvnb|Ronen|1975|p=212}}.</ref> In the aftermath, Alley was released from incarceration and reinstated in the army.<ref name=Decalo14/> In July 1968, he was named Secretary General of National Defence. Kouandété ended up becoming Alley's [[adjutant]].<ref>{{Harvnb|Ronen|1975|p=221}}.</ref>

In 1971, Alley allowed Togolese refugee [[Noe Kutuklui]] protection in Dahomey, despite official government policy to the contrary. On October 26, 1972, [[Mathieu Kérékou]] seized power in a coup. He ended Alley's military career, as well as that of every other senior officer, and named him commissioner of the National Oil Wells (SNADAH), a role with [[sinecure|very little responsibility]]. Kérékou accused Alley of plotting against him on February 28, 1973, and sentenced him to 20 years in prison.<ref name=Decalo14/> He was released on [[amnesty]] on August 1, 1984, as well as all other political detainees besides those involved in the "ignoble and barbarous imperialist armed aggression of Sunday, January 16, 1967," as the official press release states.<ref>{{citation|url=http://mail.rochester.edu/~hgoemans/Case%20DescriptionJuly2006.pdf|title=Archigos: A Data Set of Leaders 1875–2004|last1=Goemans|first1=Hein|last2=Gleditsch|first2=Kristian Skrede|last3=Chiozza|first3=Giacomo|date=July 2006|publisher=[[Rochester University]]|format=[[PDF]]|page=214|accessdate=2009-01-24}}.</ref>

Alley died on March 28, 1987. He was survived by his son, Zacharie. Plans for a mausoleum are in the works, decades after his death.<ref>{{citation|url=https://sites.google.com/site/abt2011/dans-la-press/101225_grande_mobilisation_autour-de-ABT-a-Bassila|title=Grande mobilisation samedi dernier autour de Abt à Bassila: Une sortie qui déchaîne le soutien des populations|first=Justin|last=Edikou|date=January 2, 2011|newspaper=Abdoulaye Bio Tchane|accessdate=2012-07-27|language=French}}.</ref> {{clear}}

==References==
{{reflist|colwidth=30em}}

==Bibliography==
{{refbegin}}
* {{citation|last=Daggs|first=Elisa|title=All Africa: All Its Political Entities of Independent or Other Status|publisher=Hastings House|location=[[Metuchen, New Jersey]]|year=1970|isbn=0-8038-0336-2|oclc=1959674}}
* {{citation|last=Decalo|first=Samuel|date=April 1973|title=Regionalism, Politics, and the Military in Dahomey|journal=The Journal of Developing Areas|publisher=College of Business, Tennessee State University |volume=7|issue=3|jstor=4190033}}
* {{citation|last=Decalo|first=Samuel|title=Historical Dictionary of Dahomey (People's Republic of Benin)|publisher=Scarecrow Press|location=[[Metuchen, New Jersey]]|year=1976|isbn=0-8108-0833-1|oclc=82503}}
* {{citation|last=Ronen|first=Dov|title=Dahomey: Between Tradition and Modernity|publisher=Cornell University Press|location=[[Ithaca, New York]]|year=1975|isbn=0-8014-0927-6|oclc=1527290}}
{{refend}}

{{BeninPresidents}}

{{DEFAULTSORT:Alley, Alphonse Amadou}}
[[Category:1930 births]]
[[Category:People of French West Africa]]
[[Category:French military personnel of the First Indochina War]]
[[Category:French military personnel of the Algerian War]]
[[Category:Beninese military personnel]]
[[Category:People from Bassila]]
[[Category:1987 deaths]]