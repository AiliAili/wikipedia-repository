{{Use dmy dates|date=July 2011}}
{{Use Australian English|date=May 2011}}
{{Motorsport venue
| Name              = Gillman Speedway
| Nicknames         = 
| Image             = [[File:Gillman Speedway.jpg|300px|Gillman Speedway looking from the outside of the back straight towards the start-finish line. Taken before the Darcy Ward Benefit Meeting on 7 November 2015.]]
| Location          = 65 Wilkins Road, [[Gillman, South Australia|Gillman]], [[South Australia]] 5013 
| Broke_ground      = 1997
| Opened            = 16 May 1998
| Coordinates       = {{coord|34|50|13|S|138|32|14|E|display=it}}
| Owner             = 
| Operator          = Speedway Riders' Association of South Australia Inc.
| Manager           = David Parker
| Construction_cost =
| Architect         =
| Former_names      =
| Events            = [[Australian Individual Speedway Championship|Australian Solo Championship]]<br>[[Australian Speedway Sidecar Championship]]<br>[[Australian Under-21 Speedway Championship|Australian Under-21 Championship]]<br>[[South Australian Individual Speedway Championship|South Australian Solo Championship]]<br>South Australian [[Sidecar speedway|Sidecar]] Championship<br>[[Jack Young (speedway rider)|Jack Young]] Solo Cup<br>Harry Denton Memorial Shield<br>Gillman Solo Championship
| Capacity          = 8,000
| Layout1           = Speedway
| Surface           = [[Dolomite]], crushed granite and clay mix
| Miles_first       = True
| Length_mi         = 0.186
| Length_km         = 0.300
| Banking           = 
| Record_time       = 0:54.13 (4 laps clutch start)
| Record_driver     = [[Justin Sedgmen]]
| Record_team       = 
| Record_year       = 2016
| Record_class      = [[Motorcycle speedway]]
| Layout2           = Junior Speedway Track
| Surface2          = Dolomite, crushed granite and clay mix
| Miles_first2      = True
| Length_mi2        = 0.068
| Length_km2        = 0.111
| Banking2          = 
| Record_time2      = 0:41.55 (4 laps clutch start)
| Record_driver2    = [[Max Fricke]]
| Record_team2      = 
| Record_year2      = 2011
| Record_class2     = 125cc [[Motorcycle speedway|Junior Solo]]
}}

'''Gillman Speedway''' (sometimes called '''Gillman Speedway Stadium''') is a purpose built, {{convert|300|m|yd|abbr=off}} long [[motorcycle speedway]] located in the [[Adelaide]] suburb of [[Gillman, South Australia|Gillman]] in [[South Australia]]. The track opened in 1998 and runs approximately 13 meetings per season from October to March/April.

Gillman Speedway is currently managed and promoted by former [[Rowley Park Speedway]] [[Sidecar speedway|Sidecar]] racer David Parker.<ref>[http://www.gillmanspeedway.com/contact/ Gillman Speedway contact information]</ref>

==History==
The sudden and forced closure of the [[North Arm Speedway]] at the end of the 1996-97 season left Adelaide without a weekly operating venue for motorcycle speedway for the first time in almost 80 years. North Arm was located on land owned by the [[Government of South Australia]] and the land was reclaimed in mid-1997. David Parker, the President of the Speedway Riders' Association of South Australia and Motorcycling SA President Ivan Golding inspected many sites for a new speedway before settling on the Heini Becker Park Motorsport Complex at Gillman, adjacent to the Heini Becker [[Motocross]] track and only 2 km from where North Arm was located. The only equipment they had to start building the new venue was an old water truck, an old Gallion Grader and a [[Massey Ferguson]] tractor (which is still going, whereas the other machinery has since died and been replaced).

Gillman Speedway, located on what was the car park of the motocross track, held its first official practice on Sunday, 19 October 1997 on a small {{convert|213|m|yd|abbr=off}} track formed using [[Dolomite]] from the old North Arm track. The track ran north-south with the start line located approximately where turns 1 and 2 for solos now stand, and held regular monthly practice meetings while a larger {{convert|400|m|yd|abbr=off}} dirt track was constructed (running east-west). The original safety fence of the 213 metre track consisted of used car tyres (painted white) stacked three high. After monthly practice meetings were held on the larger track, the first meeting was held on 16 May 1998.

Meetings were held at the circuit for the next two years while continued improvements were carried out. Temporary lights were installed in November 1999, and on 7 January 2000 the first meeting under lights was held which was the [[South Australian Individual Speedway Championship|South Australian Solo Championship]], won by Adelaide rider [[Nigel Sadler]].

Over the winter during 2000, the Speedway Riders' Association of SA Inc. was given financial help and also had the volunteer manpower help of the local riders and their family and friends. This allowed the Association to level both existing tracks and to build the international standard track that is in use to this day.

The current 300 metre track is 13 metres wide on the straights and 15 metres on corners with slight camber. The surface is a dolomite, crushed granite and clay mix, while the Safety fence is constructed of 1.2 metre high rubber belting, suspended on high tensile wire strands. The speedway has eight 15 metre high light poles with each having three x 2000 watt metal halide lights. The referee's and commentary box, located at the start/finish line, is fitted with an [[Fédération Internationale de Motocyclisme|FIM]] type control panel. Terraced and banked spectator areas surround the track with the exception of the back straight where the pits are located. The land in front of the pits has been deliberately left flat to allow the future building of a grandstand when funds permit. Currently this area is used as a viewing area for those in the pits. Gillman Speedway also boasts a VIP area located on the outside of turn 3 (turn 2 for the sidecars) where up to 50 people can watch the racing from an elevated position overlooking the track.

The track opened on [[Australia Day]] (26 January) 2001 with the staging of the [[Jack Young (speedway rider)|Jack Young]] Solo Cup and the Harry Denton Memorial Shield for [[Sidecar speedway|Sidecars]]. [[Leigh Adams]], who had previously won the cup at North Arm in 1994 and 1997, won his third Jack Young Cup from Adelaide's own [[Shane Parker (speedway rider)|Shane Parker]] and [[Mildura]]'s [[Travis McGowan]] (Adams would win the cup again in 2002 and 2003). [[Townsville]]'s [[Garry Moon]] and Chris Hughes won the Harry Denton Shield from then [[Australian Speedway Sidecar Championship|Australian Champions]] Glenn and Nathan O'Brien from [[Perth]], with the local team of Justin and Mark Plaisted finishing third.

During its first season of operation, Gillman also played host to the [[Australian Under-21 Individual Speedway Championship|2001 Australian Under-21 Solo Championship]] won by Adelaide rider [[Rusty Harrison]]. After the success of the meeting, Gillman again hosted the Under-21 Championship in 2002, with Travis McGowan taking out his third and last U/21 title.

Since its opening and with constant improvements to the venue including spectator terracing and a covered and paved pit area, Gillman has been regarded as one of the premier motorcycle speedways in [[Australia]]. Since 2005 the speedway has held at least one round of the [[Australian Individual Speedway Championship|Australian Solo Championship]],<ref>[http://www.vintagespeedway.com/Titles.html Australian Solo Championship honor board]</ref> including the final round of the championship in 2009, 2012 and 2014, and the opening round in 2006, 2010 and 2013. Gillman also held the Australian Sidecar Championship in 2008, 2011, and 2013.<ref>[http://www.vintagespeedway.com/sidecartitles.html Australian Speedway Sidecar Championship honor board]</ref> Gillman also hosted the [[Australian Under-21 Speedway Championship|Australian Under-21 Solo Championship]] on 2001, 2002, 2006, 2007 and 2014.

On 11 May 2011 it was announced that Gillman Speedway would hold the third and final round of the 2011/12 Australian Solo Championship on 21 January 2012.<ref>[http://www.gillmanspeedway.com/release.asp?NewsId=40620 Gillman Speedway News 11/05/2011]</ref> The championship saw [[Chris Holder]] from [[New South Wales]] unbeaten to claim his 4th Australian Championship in 5 years.<ref>[http://www.gillmanspeedway.com/release.asp?NewsId=42765 Chris Holder wins 4th Aussie Title @ Gillman Speedway]</ref> Later in the year Holder joined [[Lionel Van Praag]] ([[1936 Individual Speedway World Championship|1936]]), [[Bluey Wilkinson]] ([[1938 Individual Speedway World Championship|1938]]), Jack Young ([[1951 Individual Speedway World Championship|1951]] and [[1952 Individual Speedway World Championship|1952]]), and [[Jason Crump]] ([[2004 Speedway Grand Prix|2004]], [[2006 Speedway Grand Prix|2006]] and [[2009 Speedway Grand Prix|2009]]) to become Australia's 5th [[List of Individual Speedway World Championship medalists|Speedway World Champion]] when he won the [[2012 Speedway Grand Prix]] series.<ref>[http://www.ma.org.au/index.php?id=12&tx_ttnews%5Btt_news%5D=7744&cHash=82f19ce655 Chris Holder 2012 Speedway World Champion]</ref>

Nine days later on 20 May 2011 it was announced that Gillman Speedway would hold the final round of the new 13 round 2011/12 Speedway Sidecar Grand Slam, The series, the first of its kind in Australia for Sidecars, starts on 14 October at the [[Pioneer Park Speedway]] in [[Ayr, Queensland]] will be headlined by five times Australian champions Glenn O’Brien from [[Western Australia]] and Darrin Treloar from [[New South Wales]] as well as current World Champions Mick Headland and Paul Waters. The provisional date for the final at Gillman Speedway was 7 April 2012.<ref>[http://www.gillmanspeedway.com/release.asp?NewsId=40695 Gillman Speedway News 20/05/2011]</ref>

In August 2015, the long planned open-air (no roof) grandstand for the back straight was finally completed. The stand, which is located directly in front of the pits, has a seating capacity of 216.<ref>[http://www.speedwayfirst.com/#!GILLMAN-SPEEDWAY-ON-THE-RISE/c1t1c/55eb63190cf24e84f7624bd4 Gillman Speedway on the rise]</ref> Speedway management also announced that Gillman would host the FIM Speedway Sidecar World Cup on 26 March 2016.<ref>[http://www.gillmanspeedway.com/news/blackchrome-2016-fim-speedway-sidecar-world-cup-announced/ 2016 FIM Speedway Sidecar World Cup]</ref>

On 7 November 2015, Gillman Speedway hosted a benefit meeting for injured Australian rider [[Darcy Ward]] with all the money made at the gate (plus more from the auction of items such as helmets signed by [[MotoGP]] riders [[Valentino Rossi]] and [[Jorge Lorenzo]]) going to Ward to help with his medical costs. A crowd of 1,933 was in attendance to see Chris Holder take out the solo final from [[Troy Batchelor]] and the surprise of the meeting, young German rider [[Kai Huckenback]]. During the night Holder also swapped two wheels for three and did 4 laps on the back of Mark Plaisted's 1000cc sidecar.<ref>[https://www.youtube.com/watch?v=Y_zi2q70jrM Sidecar Speedway Chris Holder]</ref> Reigning Australian sidecar champions Justin Plaisted and Sam Harrison took out the sidecar final from Mick and Jesse Headland with Trent Headland / Daz Whetstone finishing third. The meeting raised over $40,000 for the Darcy Ward Rehabilitation Fund.<ref>[http://www.gillmanspeedway.com/news/darcy-ward-benefit-meeting-results/ Darcy Ward Benefit Meeting]</ref>

The current 4 lap record at Gillman is 54.13 seconds set on 29 October 2016 by Victorian rider [[Justin Sedgmen]]. The 4 lap Sidecar record is held by Mark Mitchell (SA) and Dale Knights (SA) with a time of 57.52 seconds set on 28 December 2015.

==Air Fence==
To increase the safety of the riders, the Speedway Riders Association installed an Air Fence through turns one and two and three and four at Gillman Speedway by the start of the 2011-12 season.<ref>[http://www.gillmanspeedway.com/airfence.asp Gillman Speedway Air Fence]</ref> The '''Speedway Safety System Airfence''', the most up-to-date and safest Speedway fence that is manufactured, consists of sixty six 3 x 1.2 panels that are 75 centimetres thick. Each panel consists of a foam frame and reinforced PVC cover. The Air Fence was installed at a cost in excess of [[Australian dollar|A$]]100,000 with the money raised by the Speedway Riders' Association through sponsorship packages.

The Air Fence fund also received a donation from [[Sweden]]'s six time [[Speedway World Championship|Speedway World Champion]] [[Tony Rickardsson]] after he visited the venue and said ''"this is how Speedway should be"''.<ref>[http://www.gillmanspeedway.com/information/ Gillman Speedway Information]</ref>

==Junior Track==
In 2006 a Junior track was built on the infield of the main track to encourage junior development, and was opened on 21 October by legendary [[Rowley Park Speedway]] promoter, the late [[Kym Bonython]]. It is the second junior track in operation in Adelaide after the [[Sidewinders Speedway]] opened in 1978 and is still in operation as of 2016.

Other distinguished guests on the night included six time [[Speedway World Championship|World Champion]] [[Ivan Mauger]] (who gave a demonstration ride on the main track at age 67, and was only 7 seconds slower over 4 laps than then track record holder Leigh Adams), former captain and later manager of the [[England national speedway team|English]] and [[Great Britain national speedway team|Great Britain]], the late [[Nigel Boocock]], twice Australian and nine time South Australian champion from the 1960s and 70's [[John Boulger]], South Australian speedway legend [[Bill Wigzell]], who although more known for driving [[Midget car racing|Speedcars]] and the purple #88 [[Sprint car racing|Super Modified]] ''"Suddenly"'', actually got his on track start in speedway as a Solo rider at Adelaide's old [[Kilburn Speedway]] in the late 1940s after initially being a Speedcar mechanic. The former flagman/Clerk of Course at Rowley Park, as well as the [[Australian Grand Prix]] in [[Adelaide Street Circuit|Adelaide]], Glen Dix, was also a guest.<ref>[http://www.saspeedway.info/gillman/gillman_classic_2006.htm Gillman Classic 2006]</ref>

Like its big brother, the {{convert|111|m|yd|abbr=off}} long junior track is a dolomite, crushed granite and clay mix. The junior track has no safety fence, although the shortest run-off area is 17 metres giving riders ample time to stop or turn before hitting the fence on the main track.

Victorian rider [[Max Fricke]], the [[2016 Individual Speedway Junior World Championship|2016 Under-21 World Champion]], currently holds the 4 lap record on the junior track with a time of 41.55 seconds set on 22 January 2011. The 4 lap junior sidecar record is held by NSW pair Shane Hudson and Sam Gilbert with a time of 42.46 seconds set on 22 March 2011.

==Track Information==
===Main Track===
'''Length'''  -  {{convert|300|m|yd|abbr=off}}, 1 metre out from the pole line<br>
'''Width'''  -  13 metres (straights), 15 metres (turns)<br>
'''Banking'''  -  slight camber<br>
'''Surface'''  -  Dolomite, crushed granite, clay mix<br>
'''Safety Fence'''  -  1.2 metre high rubber belting, suspended on high tensile wire strands. The latest and safest Speedway fence, the Airfence Speedway Safety System, installed in front of the existing fence on the corners in November 2011<br>
'''Lighting''': -  24 x 2000 watt metal halide on eight 15 metre poles<br>
'''Referees Box'''  -  Fitted with an F.I.M. type Control panel.

===Junior Track===
'''Length'''  -  {{convert|111|m|yd|abbr=off}}, 1 metre out from the pole line<br>
'''Surface'''  -  Dolomite, crushed granite, clay mix<br>
'''Safety Fence'''  -  No safety fence.<ref>[http://www.gillmanspeedway.com/trackinfo.asp Gillman Speedway track info]</ref>

==Gillman Solo Championship==
Since 2003, Gillman Speedway has staged the Gillman Solo Championship run for both Division 1 and Division 2 riders. The meeting is staged in the traditional single meeting format with semi-finals and a final. The championship was not run in 2006, 2007 and 2008.<ref>[http://www.gillmanspeedway.com/ai1ec_event/gillman-classic-championships-neil-munro-tribute-meeting-3/?instance_id Gillman Solo Championship winners]</ref>

<small>''Results shown for the Division 1 championship.''</small>
{| class="wikitable"
|- align=center
! Year
! {{Speedway color|P1}} | Winner
! {{Speedway color|P2}} | Runner-up
! {{Speedway color|P3}} | 3rd place
|-
|align=center| 2003
| [[Kevin Doolan]] (Vic)
| [[Matthew Weathers]] (SA)
| [[Ford Keane]] (SA)
|-
|align=center| 2004
| [[Kevin Doolan]] (Vic)
| [[Cory Gathercole]] (Vic)
| [[Mark Jones (speedway rider)|Mark Jones]] (Vic)
|-
|align=center| 2005
| [[Brett Woodifield]] (SA)
| [[Trevor Harding (speedway rider)|Trevor Harding]] (WA)
| [[Tom Headley]] (Vic)
|-
|align=center| 2006
| colspan=3 | ''Not Held''
|-
|align=center| 2007
| colspan=3 | ''Not Held''
|-
|align=center| 2008
| colspan=3 | ''Not Held''
|-
|align=center| 2009
| [[Tyron Proctor]] (Vic)
| [[Robert Ksiezak]] (SA)
| [[Frank Smart]] (WA)
|-
|align=center| 2010
| [[Sam Martin (speedway rider)|Sam Martin]] (SA)
| [[Robert Branford]] (SA)
| [[Ben Turner (speedway rider)|Ben Turner]] (SA)
|-
|- align=center
! Year
! {{Speedway color|P1}} | Winner
! {{Speedway color|P2}} | Runner-up
! {{Speedway color|P3}} | 3rd place
|-
|align=center| 2011
| [[Nigel Sadler]] (SA)
| [[Brenton Barnfield]] (SA)
| [[Robert Medson]] (SA)
|-
|align=center| 2012
| [[Sam Martin (speedway rider)|Sam Martin]] (SA)
| [[Robert Ksiezak]] (SA)
| [[Arlo Bugeja]] (SA)
|-
|align=center| 2013
| [[Shane Parker (speedway rider)|Shane Parker]] (SA)
| [[Robert Branford]] (SA)
| [[Max Fricke]] (Vic)
|-
|align=center| 2014
| [[Shane Parker (speedway rider)|Shane Parker]] (SA)
| [[Justin Sedgmen]] (Vic)
| [[Max Fricke]] (Vic)
|-
|align=center| 2015
| [[Justin Sedgmen]] (Vic)
| [[Brodie Waters]] (Vic)
| [[Robert Branford]] (SA)
|-
|align=center| 2016<ref>[http://www.gillmanspeedway.com/news/robert-branford-wins-his-first-gillman-solo-championship/ 2016 Gillman Solo Championship]</ref>
| [[Robert Branford]] (SA)
| [[Tyron Proctor]] (Vic)
| [[Arlo Bugeja]] (SA)
|-
|align=center| 2017
| [[Dakota Ballantyne]] (Vic)
| [[Robert Medson]] (SA)
| [[Arlo Bugeja]] (SA)
|-
|}

==Australian Championships==
===Australian Solo Championship===
* 2005 - Round 4 {{flagicon|Victoria}} [[Leigh Adams]] (''series winner - Leigh Adams'')
* 2006 - Round 1 {{flagicon|Victoria}} [[Leigh Adams]] (''series winner - Leigh Adams'')
* 2007 - Round 4 {{flagicon|Victoria}} [[Leigh Adams]] (''series winner - [[Jason Crump]]'')
* 2008 - Round 2 {{flagicon|New South Wales}} [[Chris Holder]] (''series winner - Chris Holder'')
* [[2009 Australian Individual Speedway Championship|2009]] - Round 3 {{flagicon|Victoria}} [[Leigh Adams]] (''series winner - Leigh Adams'')
* 2010 - Round 1 {{flagicon|Queensland}} [[Troy Batchelor]] (''series winner - Chris Holder'')
* 2011 - Round 2 {{flagicon|Queensland}} [[Davey Watt]] (''series winner - Chris Holder'')
* 2012 - Round 4 {{flagicon|New South Wales}} [[Chris Holder]] (''series winner - Chris Holder'')
* 2013 - Round 1 {{flagicon|Queensland}} [[Troy Batchelor]] (''series winner - Troy Batchelor'')
* [[2014 Australian Individual Speedway Championship|2014]] - Round 3 {{flagicon|New South Wales}} [[Rohan Tungate]] (''series winner - Chris Holder'')
* [[2015 Australian Individual Speedway Championship|2015]] - Round 1 {{flagicon|New South Wales}} [[Jason Doyle]] (''series winner - Jason Doyle'')
* [[2016 Australian Individual Speedway Championship|2016]] - Round 3 {{flagicon|New South Wales}} [[Brady Kurtz]] (''series winner - Brady Kurtz'')
* [[2017 Australian Individual Speedway Championship|2017]] - Round 1 {{flagicon|Queensland}} [[Troy Batchelor]] (''series winner - [[Sam Masters]]'')
* [[2018 Australian Individual Speedway Championship|2018]] - Round 4 '''''TBD''''' (''series winner - TBD'')

===Australian Under-21 Championship===
* 2001 - {{flagicon|South Australia}} [[Rusty Harrison]]
* 2002 - {{flagicon|Victoria}} [[Travis McGowan]]
* 2006 - {{flagicon|New South Wales}} [[Chris Holder]]
* 2007 - {{flagicon|New South Wales}} [[Chris Holder]]
* 2014 - {{flagicon|Victoria}} [[Max Fricke]]
* 2016 - {{flagicon|New South Wales}} [[Jack Holder]]

===Australian Sidecar Championship===
* 2008 - {{flagicon|New South Wales}} Darrin Treloar / {{flagicon|South Australia}} Justin Plaisted
* 2011 - {{flagicon|Western Australia}} Glenn O'Brien / {{flagicon|Western Australia}} Aaron Maynard
* 2013 - {{flagicon|New South Wales}} Darrin Treloar / {{flagicon|Victoria}} Simon Cohrs

==Lap Records==
<small>''as of April 2016''</small><br> {{convert|300|m|yd|abbr=off}} track
*[[Motorcycle speedway|Solo]] (4 laps clutch start): 0:54.13 - [[Justin Sedgmen]] ({{flagicon|Victoria}}), 29 October 2016
*[[Sidecar speedway|Sidecar]] (4 laps clutch start): 0:57.52 - Mark Mitchell ({{flagicon|South Australia}}) / Dale Knights ({{flagicon|South Australia}}), 28 December 2015
{{convert|111|m|yd|abbr=off}} Junior Track
*125cc Junior Solo (4 Laps clutch start): 0:41.55 - [[Max Fricke]] ({{flagicon|Victoria}}), 22 January 2011
*250cc Junior Sidecar (4 Laps clutch start): 0:42.46 - Shane Hudson ({{flagicon|New South Wales}}) / Sam Gilbert ({{flagicon|New South Wales}}), 26 March 2011

==References==
{{reflist}}

==External links==
*[http://www.gillmanspeedway.com/ Gillman Speedway official site]

{{Adelaide landmarks}}

[[Category:Speedway venues in Australia]]
[[Category:Sports venues in Adelaide]]
[[Category:Motorsport in Adelaide]]