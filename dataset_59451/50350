{{Italic title}}
'''''Acta Médica Portuguesa''''' is a [[Peer review|peer-reviewed]] [[medical journal]] covering all aspects of [[medicine]] that is published by the [[Portuguese Medical Association]] on behalf of the [[Portuguese Order of Physicians]]. Types of articles published include: original research, reviews, publishing, and medical imaging perspective. It is an [[open access]] journal published under a [[Creative Commons licence]] (BY-NC-ND).

== History ==
[[File:Capa (1).jpg|thumb|Cover of the first issue - 1979]] 
[[File:Amp 2014.JPG|thumb|Edição 2014]]

''Acta Médica Portuguesa'' was established in February 1979. In 1987, the journal was acquired by the Southern Regional Section of the Portuguese Order of Physicians. In March 1989, it became the official journal of the Order of Physicians. Since 2004, the journal has been available only in digital format.

== Editors ==
The following persons have been [[editors-in-chief]] of the journal:
* A. Galvão-Teles (1979-1987)<ref>[http://www.nedo.pt/UserFiles/File/NEDO/Curriculos/CV_Prof_Alberto_Galvao_Telles.pdf CV Alberto Galvão-Teles]</ref>
* F. Veiga Fernandes (1987-1993)
* A. Sales Luís (1993-1996)
* Carlos Ribeiro (1996-1998)<ref>[http://www.hpv.min-saude.pt/contents/pdfs/eventos/Homenagem_Prof_Dr_Carlos_Ribeiro.pdf Homepage Carlos Ribeiro] {{webarchive |url=https://web.archive.org/web/20100623024450/http://www.hpv.min-saude.pt/contents/pdfs/eventos/Homenagem_Prof_Dr_Carlos_Ribeiro.pdf |date=June 23, 2010 }}.</ref>
* J. Germano de Sousa (1999-2004)
* Pedro Nunes (2005-2010)
* Rui Tato Marinho (2011–present)

== Abstracting and indexing ==
''Acta Médica Portuguesa'' is abstracted and indexed in:
* [[PubMed]]/[[Medline]]
* [[Science Citation Index Expanded]]
* [[Chemical Abstracts]] 
* [[EMBASE]]
* [[SafetyLit]]
According to the ''[[Journal Citation Reports]]'', the journal has a 2011 [[impact factor]] of 0.256, ranking it 145th out of 153 journals in the category "Medicine, General and Internal".<ref name=WoS>{{cite book |year=2012 |chapter=Journals Ranked by Impact: Medicine, General and Internal |title=2011 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]] |postscript=.}}</ref>

== References==
{{Reflist}}

== External links ==
* {{Official website|http://www.actamedicaportuguesa.com/}}
* [http://www.ordemdosmedicos.pt/ Ordem dos Médicos]

{{DEFAULTSORT:Acta Medica Portuguesa}}
[[Category:General medical journals]]
[[Category:Publications established in 1979]]
[[Category:Multilingual journals]]
[[Category:Bimonthly journals]]