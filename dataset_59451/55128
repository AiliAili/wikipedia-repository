{{Infobox venue
| name                = Al-Jazira Mohammed bin Zayed Stadium
| nickname            =
| image               = Mohammed_Bin_Zayed_Stadium.JPG
| caption             = Al-Jazira Mohammed bin Zayed Stadium
| fullname            = Al-Jazira Mohammed bin Zayed Stadium
| location            = [[Abu Dhabi]]<br />[[United Arab Emirates]]
| coordinates         = {{Coord|24|27|09.95|N|54|23|31.27|E|type:landmark|display=inline|name=Mohammed Bin Zayed Stadium}}
| broke_ground        = 1979
| built               = 
| opened              = 1980
| renovated           = 2006–2009
| expanded            = 
| closed              = 
| demolished          = 
| owner               = [[Al Jazira Club|Al-Jazira]]
| operator            = [[Al Jazira Club|Al-Jazira]]
| surface             = Grass
| construction_cost   =
| architect           = 
| structural engineer = 
| services engineer   = 
| general_contractor  = 
| project_manager     = 
| main_contractors    = 
| former_names        = 
| tenants             = [[UAE national football team]]<br>[[Al Jazira Club|Al-Jazira]]
| capacity            = {{small|15,000 (1980–2006)<br>24,000 (2006–2010)}}<br>42,056 (2010–)<ref>http://www.worldofstadiums.com/asia/united-arab-emirates/al-jazira-mohammed-bin-zayed-stadium/</ref>
| record_attendance   = {{formatnum: 40,893}} [[United Arab Emirates national football team|United Arab Emirates]]-[[Australia national soccer team|Australia]]<br />(6 September 2016)
| dimensions          = 
| scoreboard          = 
}}

The '''Al-Jazira Mohammed bin Zayed Stadium''' ({{lang-ar|ستاد محمد بن زايد}}) is a [[multi-purpose stadium]] in [[Abu Dhabi]], [[United Arab Emirates]]. It is currently used mostly for [[football (soccer)|football]] and [[cricket]] matches and is the home ground of [[Al Jazira Club]]. It is named after General Sheikh [[Mohammed bin Zayed Al Nahyan]].

==Capacity change==
The stadium's original capacity was 15,000 but it is currently being expanded which will increase its capacity and make it into an ultra-modern air-conditioned sporting arena.  The expansion project also includes two residential towers to be built on the stadium.  Half of the project was completed by December 2006 and the stadium hosted the 18th Gulf Cup of Nations.  The UAE, host of the 18th Gulf Cup, won the tournament in the stadium which held 24,000 spectators.  The final stages of the stadium expansion are currently underway.

==Trivia==

* Hosted 3 {{Cric LA}} matches in 1999 between the 'A' teams of [[India A cricket team|India]], [[Pakistan A cricket team|Pakistan]] and [[Sri Lanka A cricket team|Sri Lanka]].
* Some of the matches of the [[2003 FIFA World Youth Championship]] were played here.
* Hosted 3 matches in the [[2009 FIFA Club World Cup]].
* Hosted the [[2010 FIFA Club World Cup]] along with  [[Zayed Sports City]].

==References==
{{Reflist}}

==External links==
{{Commons category|Mohammed Bin Zayed Stadium}}
*[http://www.jc.ae/web2/index.php Official Site – Al Jazira Club]
*[http://www.worldstadiums.com/stadium_pictures/middle_east/uae/abu_dhabi_bin.shtml Image of the Stadium]
*[http://www.afl-uk.com/ProjectDetail.aspx?projectID=28 AFL Architects]

{{s-start}}
{{Succession box |
title=[[FIFA U-17 World Cup]]<br>Final Venue|
before=[[Estadio Azteca]] <br>[[Mexico City]]|
after=[[Estadio Sausalito]]<br>[[Viña del Mar]]|
years=[[2013 FIFA U-17 World Cup|2013]]
}}
{{s-end}}
{{Al Jazira Club}}

[[Category:Al Jazira Club]]
[[Category:Football venues in the United Arab Emirates]]
[[Category:Football venues in Abu Dhabi]]
[[Category:Sports venues in Abu Dhabi]]
[[Category:Multi-purpose stadiums in the United Arab Emirates]]
[[Category:Sports venues completed in 1980]]