{{Infobox person
| name   = George Tilghman Richards
| image     = Lee-Richards Annular Monoplane No I.jpg
| image_size     = 250px
| caption  = The first annular monoplane
| birth_date  = 1883
| birth_place = Altrincham, Cheshire
| death_date  = 1960
| death_place = 
| occupation     = [[Aeronautical engineer]]<br>[[Museum curator]]
| spouse         = 
| parents        = George Richards, Amy Florence Richards
| children       = 
}}
'''George Tilghman Richards''' (1884–1960), usually known as '''G. Tilghman Richards''', was a British aeronautical engineer and museum curator, best known for his work on the [[Lee-Richards annular monoplane|Lee-Richards annular aeroplane]]s.

He was a Fellow of the [[Royal Aeronautical Society]] (FRAeS) and Member of the Institute of Aeronautical Engineers (MIAeE).<ref>''Flight'' 20 December 1928 [https://www.flightglobal.com/FlightPDFArchive/1928/1928%20-%201161.PDF Page 1067].</ref>

__TOC__

==Early life==
George Tilghman Richards was born in  Altrincham, Cheshire on 18 August 1883 to George Richards and Amy Florence Richards.<ref>"England and Wales Birth Registration Index, 1837-2008," database, FamilySearch (https://familysearch.org/ark:/61903/1:1:2X25-B2Z : 1 October 2014), George Tilghman Richards, 1883; from "England & Wales Births, 1837-2006," database, findmypast (http://www.findmypast.com : 2012); citing Birth Registration, Altrincham, Cheshire, England, citing General Register Office, Southport, England.</ref><ref>"England Births and Christenings, 1538-1975," database, FamilySearch (https://familysearch.org/ark:/61903/1:1:NGK1-QP8 : 6 December 2014), George Tilghman Richards, 04 Oct 1883; citing , reference ; FHL microfilm 438,183.</ref>

By 1908 Richards had gained experience as an automotive engineer working for [[Rolls-Royce Limited|Rolls-Royce]] and [[Belsize Motors|Belsize]], and set up as an independent consultant in [[Manchester]].<ref>[http://archive.commercialmotor.com/article/20th-august-1908/10/news-and-comment ''The Commercial Motor'', 20 August 1908, Page 10]</ref>

==Lee-Richards annular aeroplanes==
[[File:BAPC-20 (15596688315).jpg|thumb|Non-flying replica of the biplane]]
{{main|Lee-Richards annular monoplane}}
In 1910 Cedric Lee approached Richards to work on an annular biplane which he had acquired half-finished from J. G. A. Kitchen. Together they finished the aeroplane, fitting a {{convert|50|hp|abbr=on}} [[Gnome Omega]] engine in the front. The machine is known variously as the Kitchen annular biplane and the Lee-Richards annular biplane. Flight tests in 1911 were disappointing and that Autumn the biplane was destroyed on the ground by high winds, when its hangar collapsed.<ref name="jarrett">Jarrett (1976)</ref><ref name="lewis">Lewis, P.; ''British Aircraft 1809-1914'', Putnam, 1962, pages 340-343.</ref> A non-flying replica later appeared in the 1965 film ''[[Those magnificent men in their flying machines]]'' and is now on display at the [[Newark Air Museum]].

Lee and Richards continued experimenting with models and Richards especially further developed the aerodynamic theory of the annular wing. They developed a form having a circular lower wing with an auxiliary plane above the front half of the main wing. A full-size manned glider proved successful.<ref name="jarrett" /> Model tests of a new design at the [[National Physical Laboratory (United Kingdom)|National Physics Laboratory]] gave promising results, confirming that an annular monoplane would be aerodynamically stable and have benign [[Stall (fluid mechanics)|stalling]] characteristics.<ref name="lewis"/>

Through 1913-14 three monoplanes were built and flown. All were stable in flight and successively showed improved handling.<ref name="jarrett" />

Richards would continue to promote the benefits of the annular wing throughout his life, but without success. While working for [[William Beardmore and Company|Beardmore]] in 1916 he patented an improved method of construction.

==WWI and aftermath==
[[File:Beardmore W.B.III.jpg|thumb|The Beardmore W.B.III]]
When war broke out, Richards was commissioned into the [[Royal Naval Air Service]] (RNAS) as an inspector of Naval aircraft. He was sent to [[William Beardmore and Company|Beardmore]], who were manufacturing other companies' designs under licence. In due course he resigned his commission to become assistant manager and designer. From 1916 to 1917 he produced their first in-house aircraft designs, achieving his greatest success with the [[Beardmore W.B.III|W.B.III]], a navalised version of the [[Sopwith Pup]] which Beardmore were manufacturing under license. Other Beardmore types which he designed included the [[Beardmore W.B.1|W.B.1]], [[Beardmore W.B.2|W.B.2]], [[Beardmore W.B.IV|W.B.IV]], [[Beardmore W.B.V|W.B.V]] and [[Beardmore W.B.VI|W.B.VI]].

After the war there was little demand for aircraft and he moved to [[Martinsyde]] as general manager, where they were changing over from aircraft manufacture to motor cycles.<ref name="flight1954">''Flight'', 22 January 1954,.[https://www.flightglobal.com/FlightPDFArchive/1954/1954%20-%200205.PDF Page 95]</ref>

==Science Museum==
In 1924 he went to work for the [[Science Museum, London]] and remained there until his retirement in 1954.<ref name="flight1954" />

During his time there a model of a Lee-Richards annular monoplane was commissioned and put on display.<ref name="jarrett" />

He also wrote ''The History and Development of Typewriters'', Science Museum, London, 1938.

==Death==
Richards died on 22 June 1960 at the age of 76.<ref>''Flight'' 1 July 1960 [https://www.flightglobal.com/pdfarchive/view/1960/1960%20-%200891.html?search=Tilghman%20Richards Page 3]</ref>

==References==

===Notes===
{{reflist}}

===Bibliography===
*Jarrett, P.; "Circles in the Sky", ''Aeroplane Monthly'', (Part I) September 1976 Pages 493-499, (Part II) October 1976 Pages 526-531, 553.

{{DEFAULTSORT:Richards, G. Tilghman}}
[[Category:1883 births]]
[[Category:People from Manchester]]
[[Category:Aviation pioneers]]
[[Category:Fellows of the Royal Aeronautical Society]]
[[Category:1960 deaths]]
[[Category:People associated with the Science Museum, London]]