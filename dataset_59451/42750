{|{{Infobox Aircraft Begin
|name           = SportStar<!-- avoid stating manufacturer (it's stated 3 lines below) unless name used by other aircraft manufacturers -->
|image          = 24-7324 Evektor-Aerotechnik EV-97 SportStar Max (6931052501).jpg<!--in the ''Image:filename'' format with no image tags-->
|caption        = SportStar<!--Image caption; if it isn't descriptive, please skip-->
}}{{Infobox Aircraft Type
|type           = [[Light Sport Aircraft]]
|national origin = [[Czech Republic]]<!-- Use the main nation (ie. UK), not constituent country (England); don't use "EU". List collaborative programs of only 2 or 3 nations; for more than 3, use "Multi-national:. -->
|manufacturer   = [[Evektor-Aerotechnik]]
|designer =
|first flight   = <!--If this hasn't happened, skip this field!-->
|introduction   = <!--Date the aircraft entered or will enter military or revenue service-->
|retired        = <!--Date the aircraft left service. If vague or more than a few dates, skip this -->
|status         = <!--In most cases, redundant; use sparingly-->
|primary user   = <!-- List only one user; for military aircraft, this is a nation or a service arm. Please DON'T add those tiny flags, as they limit horizontal space. -->
|more users     = <!-- Limited to THREE (3) 'more users' here (4 total users).  Separate users with <br/>. -->
|produced       = <!--Years in production (eg. 1970-1999) if still in active use but no longer built -->
|number built   = 
|program cost   = <!--Total program cost-->
|unit cost      = <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
|variants with their own articles = [[Evektor EPOS]]
}}
|}

[[File:Mainair EV-97.jpg|thumb|Eurostar EV-97]]
[[File:eurostar ev97 g-cdtu in flight arp.jpg|thumb|Eurostar EV-97 in flight]]
[[File:Evektor EV-97 G-CDAP Open Canopy BTN 02.06.11R edited-3.jpg|thumb|right|EV-97 Eurostar showing the large hinged clear view cockpit canopy]]
[[File:Evektor-Aerotechnik Sportstar C-ILUV 07.JPG|thumb|right|SportStar instrument panel]]
[[File:Evektor-Aerotechnik Sportstar C-ILUV 08 showing split flap.JPG|thumb|right|SportStar wing showing the aircraft's  [[Flap (aircraft)|split flap]] arrangement]]

The '''SportStar''' and '''EuroStar''' are a family of a two-seat, [[light sport aircraft]] (LSA), manufactured by [[Evektor-Aerotechnik]] of the [[Czech Republic]] and powered by a [[Rotax 912|Rotax 912ULS]], {{convert|100|hp}} engine.<ref name="MaxOverview">{{cite web|url = http://www.evektoraircraft.com/en/aircraft/sportstar-max/overview|title = SportStar Max Overview|accessdate = 4 November 2011|last =Evektor, spol. s r.o.|authorlink = |year = n.d.}}</ref><ref name="WDLA11">Bayerl, Robby; Martin Berkemeier; et al: ''World Directory of Leisure Aviation 2011-12'', pages 47-48. WDLA UK, Lancaster UK, 2011. ISSN 1368-485X</ref>

The SportStar was the first approved special [[light-sport aircraft]] (S-LSA) and was named "S-LSA Aircraft of the Year" by AeroNews Network.<ref name="MaxOverview" />

==Design and development==
The SportStar is an all-metal design made from anodized, corrosion-proofed [[aluminum]]. The airframe uses a [[Rivet#Blind rivets|pop-riveted]] and [[Adhesive|bonded]] construction, which the company claims will improve fatigue characteristics and result in a longer service life. The company also claims this construction technique results in better crashworthiness, the elimination of rivet zippering in an accident and quieter in-flight noise levels due to the elimination of oil-canning and flexing.<ref name="Airframe">{{cite web|url = http://www.evektoraircraft.com/en/aircraft/sportstar-max/advanced-airframe |title = Advanced Airframe with Long Service Life|accessdate = 5 November 2011|last = Evektor-Aerotechnik }}</ref>

The SportStar was designed for towing [[Glider (sailplane)|sailplanes]] up to 1544&nbsp;lbs (700&nbsp;kg) gross weight and for towing banners up to 1479 sq ft (140 m<sup>2</sup>).<ref name="Towing">{{Cite web|url = http://www.evektoraircraft.com/en/aircraft/sportstar-max/glider-towing|title = Glider Towing|accessdate = 4 November 2011|last = Evektor-Aerotechnik|date = n.d.}}</ref>

==Variants==
;EuroStar SL
:Model for the European ultralight category, with a gross weight of {{convert|1041|lb|kg|0|abbr=on}}<ref name="WDLA11" /><ref name="EuroStarSL">{{Cite web|url = http://www.evektoraircraft.com/en/aircraft/eurostar-sl/look|title = EuroStar SL|accessdate = 4 November 2011|last = Evektor|date = n.d.}}</ref>
;EuroStar SL+
:Version of the EuroStar SL with a new wing and integral fuel tanks, bigger baggage compartment and lower empty weight by {{convert|8|kg|lb|0|abbr=on}}.<ref name="WDLA15">Tacke, Willi; Marino Boric; et al: ''World Directory of Light Aviation 2015-16'', pages 48-50. Flying Pages Europe SARL, 2015. {{ISSN|1368-485X}}</ref>
;EuroStar SLW
:Model for the European ultralight category, with a gross weight of {{convert|1041|lb|kg|0|abbr=on}}. It combines the EuroStar fuselage with the wing and stabilizer from the Harmony.<ref name="WDLA15"/><ref name="EuroStarSLW">{{Cite web|url = http://www.evektoraircraft.com/en/aircraft/eurostar-slw/overview|title = EuroStar SLW|accessdate = 4 November 2011|last = Evektor|date = n.d.}}</ref>
;Harmony
:Model for the US LSA market, based on the Harmony [[airframe]], with enlarged [[aileron]]s and rudder to improve crosswind capabilities, an improved wing, [[winglet]]s and tail, wider and longer cockpit, as well as refined [[wheel pants]] and other fairings.<ref name="WDLA11" /><ref name="WDLA15"/>
;SportStar
:Initial model
;SportStar SL
:Improved model
;SportStar Max
:Version for the US LSA market with a gross weight of {{convert|1320|lb|kg|0|abbr=on}}<ref name="MaxOverview" /><ref name="WDLA11" />
;SportStar RTC
:SportStar RTC was developed to meet EASA VLA certification and intended for use in flight training.<ref>{{cite web|url=http://www.evektor.com/en/sportstar-rtc|title=SportStar RTC|work=evektor.com|accessdate=5 October 2015}}</ref>
;[[Evektor EPOS]]
:[[Electric aircraft]] version, using the SportStar RTC fuselage and a new wing design.<ref name="WDLA15"/>

==Specifications (SportStar MAX) ==
{{aircraft specifications
<!-- if you do not understand how to use this template, please ask at [[Wikipedia talk:WikiProject Aircraft]] -->
<!-- please answer the following questions -->
|plane or copter?=plane<!-- options: plane/copter -->
|jet or prop?=prop<!-- options: jet/prop/both/neither -->

|ref=Evektor website<ref>{{
cite web
 |url= http://www.evektoraircraft.com/en/aircraft/sportstar-max/technical-data-99-506
 |title= Technical Data
 |last=Evektor, spol. s r.o. 
 |first=
 |date=2011
 |accessdate=2011-11-05
 |work=
 |publisher=
}}</ref>
<!-- Now, fill out the specs.  Please include units where appropriate (main comes first, alt in parentheses). If an item doesn't apply, like capacity, leave it blank. For instructions on using |more general=, |more performance=, |power original=, and |thrust original= see [[Template talk:Aircraft specifications]].  To add a new line, end the old one with a right parenthesis ")", and start a new fully formatted line beginning with * -->
|crew=one
|capacity=one passenger
|payload main=
|payload alt=
|payload more=

|length main= 17 ft 7.5 in
|length alt= 5.98 m
|span main= 28 ft 5 in
|span alt= 8.65 m
|height main=8 ft 2 in
|height alt=2.48 m
|area main= ft²
|area alt= m²
|airfoil=
|empty weight main=668 lb
|empty weight alt=308 kg
|loaded weight main=1320 lb
|loaded weight alt=600 kg
|useful load main=640 lb
|useful load alt=292 kg
|max takeoff weight main=1320 lb
|max takeoff weight alt=600 kg
|more general=

|engine (jet)=
|type of jet=
|number of jets=
|thrust main= lbf
|thrust alt= kN
|thrust original=
|afterburning thrust main= lbf
|afterburning thrust alt= kN

|engine (prop)=[[Rotax 912|Rotax 912ULS]],
|type of prop=ground adjustable propeller
|number of props=1
|power main=100 hp
|power alt=75 kW
|power original=
   
|propeller or rotor?=propeller<!-- options: propeller/rotor -->
|propellers=Klassic 1700/3/R, composite,<!-- eg the manufacturer or number of blades -->
|number of propellers per engine=1 
|propeller diameter main=5 ft 6 in
|propeller diameter alt= 1.7 m

|max speed main=115 knots
|max speed alt=213 km/h
|max speed more=
|cruise speed main=110 knots
|cruise speed alt=204 km/h
|cruise speed more=
|never exceed speed main=146 knots
|never exceed speed alt=270 km/h
|stall speed main=40 knots
|stall speed alt=74 km/h
|range main=700 nm
|range alt=1300 km
|range more=
|ferry range main=
|ferry range alt=
|ferry range more=
|ceiling main=15,500 ft
|ceiling alt=4720 m
|ceiling more=
|climb rate main=1020 ft/min
|climb rate alt=5.2 m/s
|climb rate more=
|loading main= lb/ft²
|loading alt= kg/m²
|thrust/weight=<!-- a unitless ratio -->
|power/mass main=12.68 lb/hp
|power/mass alt=0.13 W/kg
|more performance=

|armament=<!-- if you want to use the following specific parameters, do not use this line at all-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints= 
|hardpoint capacity=

|avionics=
}}

== References ==
{{commons category|Aerotechnik EV-97 Eurostar}}
{{reflist}}
{{Evektor-Aerotechnik aircraft}}

[[Category:Czech sport aircraft 2000–2009]]
[[Category:Light-sport aircraft]]
[[Category:Single-engined tractor aircraft]]
[[Category:Low-wing aircraft]]
[[Category:Evektor aircraft|SportStar]]