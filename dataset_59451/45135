{{About|Rock 'n' Roller Coaster at [[Disney's Hollywood Studios]] and [[Walt Disney Studios Park]]|the [[Opryland USA|Opryland]] ride|Rock n' Roller Coaster (Opryland)}}
{{Refimprove|date=June 2008}}
{{Infobox roller coaster
|name=Rock 'n' Roller Coaster
|logo=
|logo_width=
|image=rockin outside.JPG
|imagedimensions=249px
|caption=A giant red [[Fender Stratocaster]] greets Aerosmith enthusiasts outside the ride at Disney's Hollywood Studios at the Walt Disney World Resort.
|previousnames=
|location=Disney's Hollywood Studios
|section=          <!--Must not be linked.-->
|subsection=       <!--Should be linked.-->
|opened={{Start date|1999|7|29}}
|year=1999
|closed=
|cost=
|extend={{Infobox roller coaster extend
|location=Walt Disney Studios Park
|section=          <!--Must not be linked.-->
|subsection=       <!--Should be linked.-->
|status= Operating 
|opened={{Start date|2002|3|16}}
|year=2002
|closed=
|cost=
|coordinates={{coord|48.864850|2.779931|display=it}}
}}
|type=Steel
|type2=Enclosed
|type3=Launched
|status=Operating
|manufacturer=Vekoma
|designer=[[Walt Disney Imagineering]]
|model=[[Linear synchronous motor|LSM]] Coaster
|track=
|lift=LSM launch with catch car
|height_ft=86.2
|drop_ft=          <!--Must be expressed in feet and may contain only numeric characters.-->
|length_ft=3403
|speed_mph=57
|inversions=3
|duration=1:22 (Pre-show is 3:12)
|angle=            <!--Do not include "degrees", it is added automatically.-->
|capacity=1800
|acceleration_from=0
|acceleration_mph=57
|acceleration_in=2.8
|gforce=5 g's
|restriction_in=48
|trains=5 (max. 4 in operation)
|carspertrain=6
|rowspercar=2
|ridersperrow=2
| virtual_queue_name  = [[Disney's Fastpass|Fastpass]]
| virtual_queue_image = Fastpass availability icon.svg
| virtual_queue_name2 = [[Disney's Fastpass#FastPass+|FastPass+]]
| virtual_queue_image2= Fastpass+ Logo.png
| virtual_queue_status= available
|single_rider=available at Disney's Hollywood Studios            <!--Must be "available" if available.-->
|custom_label_1= Sponsor
|custom_value_1= [[Hanes]]
|custom_label_2= 
|custom_value_2= 
|custom_label_3= 
|custom_value_3= 
|custom_label_4= 
|custom_value_4= 
|accessible=              <!--Must be "available" if available.-->
|transfer_accessible=     <!--Must be "available" if available.-->
|rcdb_number=560
|rcdbnumber2=1444
|coordinates={{coord|28|21|33.9|N|81|33|38.5|W|display=inline}}
}}

'''Rock 'n' Roller Coaster Starring Aerosmith''' (Avec Aerosmith in France) is an [[enclosed roller coaster|enclosed]] [[Launched roller coaster|launched]] [[steel roller coaster|steel]] [[roller coaster]] at [[Disney's Hollywood Studios]] at the [[Walt Disney World|Walt Disney World Resort]] and at the [[Walt Disney Studios Park]] in [[Disneyland Paris]]. As the attraction's name suggests, the coaster features [[Aerosmith]] members, [[Steven Tyler]], [[Joe Perry (musician)|Joe Perry]], [[Tom Hamilton (musician)|Tom Hamilton]], [[Joey Kramer]], and [[Brad Whitford]].

The Florida attraction opened on July 29, 1999 and is located at the end of [[Sunset Boulevard]], an area of the park which also features [[The Twilight Zone Tower of Terror]]. The [[Paris]] attraction opened on March 16, 2002 in the Backlot section of the park; it is currently the fastest roller coaster in [[France]].<ref>{{cite web|url=http://rcdb.com/rhr.htm?l=26056|title=Record Holders|work=rcdb.com|publisher=Duane Marden|accessdate=1 August 2014}}</ref> As of July 2008, [[Hanes]] is the attraction's presenting sponsor.<ref>{{cite news|url=http://blogs.orlandosentinel.com/features_orlando/2008/07/dude-looks-like.html|title=Dude Looks Like a Lady (depending on his underwear)|date=2008-07-16|publisher=''[[Orlando Sentinel]]''|author=Matt Palm|accessdate=2008-07-16}}</ref>

The coaster accelerates from 0 to 57 miles per hour in 2.8 seconds (making this the second-fastest attraction at the Walt Disney World Resort, behind only [[Test Track]]). The riders experience 4.5 G as they enter the first inversion, more than an [[astronaut]] does on a [[space shuttle]] launch. Both versions of the attraction feature five trains, although only four can run at one time. The remaining train is kept in backup while being serviced (each train is rotated out periodically for safety reasons).

== History ==
Cast member previews for the ride were initially held the last week of June, 1999. On 29 July 1999, the ride officially opened with a special, invitation-only party, with Aerosmith as the guests of honor. Winners rode to Disney's Hollywood Studios in stretch limousines and were treated to an all-you-can-eat buffet and bar. After a special performance by painter [[Denny Dent]], winners got the chance to ride the roller coaster with one of the Aerosmith band members. At the exit of the ride, outside of the gift shop, there hangs a picture from the special event. The paintings Denny Dent made of the five band members hang in various employee office locations on Walt Disney World property.

The pre-show has changed from when the attraction first opened. Currently, band member [[Joe Perry (musician)|Joe Perry]] would ask "Chris" to "grab my black [[Gibson Les Paul|Les Paul]]." A Disney cast member in the pre-show area would then pick up and remove a black guitar signed by Joe Perry from the set. The script uses the unisex name "Chris" so either a male or female could play the part. Although not used as often as when the ride first opened, the position is still used from time to time (generally, if the attraction is overstaffed). The film also includes a [[Road crew|roadie]] saying "Hey Joe, I'll get it for ya" as a backup, in the event that a cast member is not available for the part.

In 2007, the queue was modified to accommodate [[single rider]]s in addition to [[FASTPASS]].

== Ride experience ==

=== Queue ===
Guests begin the queue by entering through a small and narrow tent, with boxes designed like instrument cases decorating the scene. The guests then wait in an unshaded, gridded-queue area (if the queue is long), and then proceed to a ramp that takes them back and forth, to curve around the front of the building, behind the giant electric guitar, and into the "G-Force Records" recording studio. Guests then enter a circular room with a high ceiling decorated with a giant record. Guests can see posters featuring artists Disney has signed, other miscellaneous and famous bands/singers and even the guests themselves. These posters are displayed via LCD screens and are able to display a guest's name to customize the visual as they walk past.<ref name=itm-custom-posters>{{cite news|title=Rock ‘n’ Roller Coaster introduces personalized rock posters at Walt Disney World|url=http://www.insidethemagic.net/2016/04/video-rock-n-roller-coaster-introduces-personalized-rock-posters-at-walt-disney-world/|author=McCabe, Adam|date=April 19, 2016|accessdate=21 October 2016}}</ref> Guests then continue immediately to enter a small room with doors covered in marbles and a small exhibit of recording instruments. Guests can interact with the marble doors and often do, and if they take the time to look at the exhibit, they will see various funny parts (i.e. the person who organized and presented the exhibit is named Mike Rofone). Guests are then called by a Cast Member to join other guests (this is where the Fastpass+ line and Stand-by lines merge) to enter the studio that [[Aerosmith]] is recording in. Guests wait for the recording to stop, and the automatic doors eventually open. As guests enter, the song "[[Walk This Way]]" plays, except, after the introduction is played, the drums, vocals, and guitar cuts out, leaving only the bassline (the guitar occasionally comes back in). Guests see [[Aerosmith]] recording and wait momentarily, until the band is interrupted by their manager (played by [[Illeana Douglas]]). The manager tells the band that they have a show to get to, and they can't stay with the guests any longer. [[Steven Tyler]] expresses discontent with this outcome and suggests to his manager to get the guests to their concert, along with backstage passes. The manager reluctantly accepts, after a few seconds of persuasion. After she accepts to give the ride and tickets to the show, the guests see her call her assistant "Sal," who she tells to get a "stretch" limo. After a few seconds of thinking, she takes it back and instead asks for a "super stretch." She then tells the guests that she has gotten a "really fast car" for them to ride to the concert because the show is all the way across town. Guests then see the limo with the band in it already peel out of the lot, leaving the manager behind.

The automatic doors open to the outdoor parking garage, where guests will be boarding their limos. As the doors open, the audio for "Walk This Way" plays as guests exit. Guests walk along a fence that separates them from the limos. Immediately after entering the garage, guests see the limos that are ready to be launched off from 0 to 57 MPH in 2.8 seconds. As cars launch off, guests hear echoed and intense screaming, and extra added sound effects. Guests then proceed from here through a short line to board their limo.

=== Ride ===
While the guests wait for the ride to begin, a radio DJ presents the safety spiel, followed by a traffic report. The highway sign flashes humorous messages like: "Traffic bug you? Then STEP on it!" Guests wait for the car ahead of them to finish the ride, and eventually they hear an introduction (which varies, and includes messages such as "We're only just getting started!" or "Hold on, here we go!"). After this introduction, Steven Tyler alternates between each ear the countdown of "Five, four, three, two", as they hear the introduction of the song they're about to hear. As Tyler gets to 2 in the countdown, he skips 1 and guests suddenly begin to accelerate from 0 to 58 MPH in less than 2.8 seconds. During this initial second, the on-ride camera takes the photo of the guests. After a long straightway, the car proceeds to do a [[Roller coaster elements#Roll over|Roll Over]] (sea serpent) roll, 
which is a two inversion element,<ref>{{Cite web|title = Rock 'n' Roller Coaster - Walt Disney World - Disney's Hollywood Studios (Lake Buena Vista, Florida, USA)|url = http://rcdb.com/560.htm|website = rcdb.com|access-date = 2016-02-03}}</ref> and then some less intense maneuvers. During the ride, there are neon signs on the side of the track, designed to mimic road signs. The car continues along the track, until it reaches the third and final inversion, a corkscrew. Finally, guests perform a humpback as they enter the "VIP parking" section for the fictitious concert. Guests wait in a tunnel at a stop for a moment, then proceed to the VIP backstage area, where they're greeted by a red carpet and monitors displaying their ride photos. Guests then exit through the gift shop.

===Soundtrack===
[[Image:rock n rollercoaster night.jpg|thumb|The giant red [[Fender Stratocaster]] outside of the Disney Rock ‘n’ Roller Coaster at night]]
[[Image:Lock'n'roll.JPG|thumb|The garage sign in the ride's load area]]
 
[[Walt Disney Imagineering]] worked with Aerosmith to produce a special soundtrack for the roller coaster. Each coaster train features different Aerosmith songs.
* The songs heard to each ride contain some new lyrics written specifically for the attraction. (''Love in an Elevator'' is sung as "Love in a roller coaster," for example.)
* Car license plates and songs heard on each car (Note: Updated Jan 2008 after 2007 refurbishment):
** 1QKLIMO: "[[Nine Lives (Aerosmith song)|Nine Lives]]"
** UGOBABE: "[[Love in an Elevator]]" (Rollercoaster) & "[[Walk This Way]]" (Formerly UGOGIRL Pre-2004 Refurbishment)
** BUHBYE: "[[Pump (album)|Young Lust]]", "[[F.I.N.E.*]]" & "[[Love in an Elevator]]" (Rollercoaster)
** H8TRFFC: "[[Back in the Saddle]]" & "[[Dude (Looks Like a Lady)]]"
** 2FAST4U: "[[Sweet Emotion]]" (live, as featured in ''[[A Little South of Sanity]]'')

There is a 6th Limo in the fleet of ride vehicles of Rock 'n' Roller Coaster. This vehicle is without a license plate, and is always "in refurbishment." The vehicles are rotated in and out of use after a period of many thousands of laps around the track. However, the maintenance teams will switch out the plate and add the proper song to the new vehicle every time a rotation is made.

The ride formerly featured Uncle Joe Benson, a well-known Los Angeles rock radio DJ, as the station's DJ. Currently, Bill Hart (known as Bill St. James), the host of [[Citadel Media|ABC Radio]]'s ''[[Flashback (radio program)|Flashback]]'', provides the voice for the DJ of "LA's Classic Rock Station."

The height requirement on this ride is 48 inches.

==Walt Disney Studios Park attraction==
Rock 'n' Roller Coaster also exists in Disneyland Paris' Walt Disney Studios Park, named "Rock 'n' Roller Coaster avec Aerosmith".

Although the track layout is identical to its Orlando counterpart, the theme of the ride differs&mdash;instead of guests being taken on the Los Angeles freeways, the Paris version is based around an Aerosmith music video. Lighting rigs, projectors, strobes, and smoke effects are used in place of the road signs that exist in the U.S. version. The name of the record company is Tour de Force (instead of "G-Force") Records, and the vehicles in Paris are called "Soundtrackers" instead of limousines.

The story of the Paris version is that Aerosmith, working with engineers, have created a revolutionary new music experience at the Tour De Force Records studios. After watching the pre-show which features Aerosmith's Steven Tyler hyping up the ride, guests are lured into the testing area where they board one of five Soundtrackers, the prototype vehicles for the new experience.

A unique aspect of the Walt Disney Studios version is that each Soundtracker has its own theme. There are five different lightshows and five different soundtracks, one for each Soundtracker. The themes are as follows:

* '''Soundtracker 1:''' green lightshow theme; plays "Back In The Saddle" and "Dude Looks Like A Lady".
* '''Soundtracker 2:''' purple lightshow theme; plays "Young Lust," "F.I.N.E." and "Love In An Elevator."
* '''Soundtracker 3:''' multicolour lightshow theme; plays "Love In An Elevator" and "Walk This Way."
* '''Soundtracker 4:''' red/yellow lightshow theme; plays "Nine Lives."
* '''Soundtracker 5:''' blue lightshow theme; plays "Sweet Emotion (live)."

The minimum height at Parc Walt Disney Studios is 1.2 metres (47 inches).

==Hidden Mickeys==
{{main article|Hidden Mickey}}
* One in the pre-show (found on a small chalkboard in the bottom right corner of the sound room (this one was made by a cast member and changes at times)
* One in the pre-show (found on the ground, formed by three coils of wire).
* One in place of each "Expiration Date" for the license plates on the limos.
* Many on the floor in Studio C, hidden in the carpet pattern.
* Three on the ride track itself (one found on a yellow sign behind a red limo, two on the ground in a cityscape area).
* Two in the tile mosaic in the rotunda before the recording studio near the marble doors.
* One in the post show area to the right as one enters this area.
* Many on [[Steven Tyler]]'s shirt on the poster outside.
* One on [[Joe Perry (musician)|Joe Perry]]'s medallion on the outside poster.

==Speakers==
In each train, there are a total of 120 speakers. There are 5 speakers per seat including 1 subwoofer (under the seat) and 4 located in the headrest. There are 820 speakers located in the ride's show building and launch area (not including the train). This makes a total of about 900 speakers in the attraction.

==See also==
* [[Incidents at Disney parks]] - information on incidents and accidents involving the attraction.

==References==
{{reflist}}

==External links==
* [https://disneyworld.disney.go.com/attractions/hollywood-studios/rock-and-roller-coaster-starring-aerosmith/ Official Disney's Hollywood Studios Rock 'n' Roller Coaster website]
* [http://www.disneylandparis.co.uk/attractions/walt-disney-studios-park/rock-n-roller-coaster-starring-aerosmith/ Official Walt Disney Studios Park Rock 'n' Roller Coaster website]
* [http://www.photosmagiques.com/gallery/walt-disney-studios-park/backlot/rock-n-roller-coaster-avec-aerosmith/ Photos Magiques - Rock 'n' Roller Coaster avec Aerosmith]

{{WDW Coasters}}
{{Disneyland Paris Coasters}}
{{Disney's Hollywood Studios}}
{{Walt Disney Studios Park}}
{{HanesBrands}}
{{Aerosmith}}

[[Category:Aerosmith]]
[[Category:Backlot (Walt Disney Studios Park)]]
[[Category:Disney's Hollywood Studios]]
[[Category:Enclosed roller coasters]]
[[Category:Licensed properties at Walt Disney Parks & Resorts]]
[[Category:Roller coasters at Walt Disney Studios Park]]
[[Category:Roller coasters in Greater Orlando]]
[[Category:Roller coasters introduced in 1999]]
[[Category:Sunset Boulevard (Disney's Hollywood Studios)]]
[[Category:Walt Disney Studios Park]]
[[Category:Walt Disney Parks and Resorts attractions]]