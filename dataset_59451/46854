{{Infobox architect
|name        = Alice Ross Carey
|image       = Alice carey.jpg
|nationality = American
|birth_date  = {{BirthDeathAge|B|1948|11|10|2013}}
|birth_place = [[Brooklyn, New York|Brooklyn]], [[New York (state)|New York]]
|death_date  = {{BirthDeathAge| |1948| | |2013|7|27}}
|practice    = Carey & Co.
|alma_mater  = [[College of Environmental Design, UC Berkeley | College of Environmental Design]]
|significant_buildings = 
|significant_projects  = Jordan Hall at Stanford University<br>San Francisco’s Fairmont Hotel<br>San Francisco City Hall<br>San Francisco War Memorial Opera House<br>Sunol Water Temple<br>San Francisco Palace of Fine Arts<br>Marin County Civic Center
|awards      = 
}}

'''Alice Ross Carey''' (1948&ndash;2013) was an award-winning preservation architect, advocate, and early practitioner of [[historic preservation]], restoration, and reuse. 

== Early life ==
Alice Ross Carey was born in [[Brooklyn, New York]], and raised in [[Toledo, Ohio]].<ref>{{cite web|title=Nomination for the San Francisco Beautiful Award|url=http://www.alicecareymemorial.com/posthumous-awards.html|website=http://www.alicecareymemorial.com/|accessdate=25 March 2015}}</ref> She received a Bachelor of Fine Arts from the [[University of Colorado]]. Carey worked as a carpenter and had her own small construction firm before completing a master's degree in architecture at the [[University of California, Berkeley]] in 1976.<ref name=GateObit>[http://www.sfgate.com/bayarea/article/Alice-Carey-dies-architect-avid-preservationist-4701831.php Alice Carey dies: architect, avid preservationist], by John King, in the San Francisco Chronicle; published 1 Aug. 2013; accessed 3 Mar. 2015.</ref>

== Career ==
Following graduation, Carey worked for the firms Esherick, Homsey Dodge & Davis (EHDD) and Whisler/Patri. While at EHDD, she became interested in the Bay Region Style of architecture and its architects [[Joseph Esherick (architect)|Joseph Esherick]], [[William Wurster]], [[Charles Moore (architect)|Charles Moore]] and [[William Turnbull, Jr.]]. She established her own practice Carey & Co., in 1983 in San Francisco, one of the first woman-owned architectural practices specializing in historic preservation in the United States. She was named to San Francisco’s Landmarks Preservation Advisory Board by 1988. After the [[1989 Loma Prieta earthquake]], her firm managed the preservation of several San Francisco historically significant civic buildings, including [[San Francisco City Hall|City Hall]] and the [[War Memorial Opera House]].<ref>{{cite web|title=Oakland edifices inspired by 1906, 1989 earthquakes|url=http://www.insidebayarea.com/oaklandtribune/localnews/ci_5156502|website=http://www.insidebayarea.com|accessdate=25 March 2015}}</ref><ref name=GateObit>[http://www.sfgate.com/bayarea/article/Alice-Carey-dies-architect-avid-preservationist-4701831.php Alice Carey dies: architect, avid preservationist], by John King, in the San Francisco Chronicle; published 1 Aug. 2013; accessed 3 Mar. 2015.</ref> Her work on these projects earned Carey & Co. nearly three dozen state and national awards.<ref>{{cite web|title=Oakland edifices inspired by 1906, 1989 earthquakes|url=http://www.insidebayarea.com/oaklandtribune/localnews/ci_5156502|website=http://www.insidebayarea.com|accessdate=25 March 2015}}</ref> During her career, Carey worked on restoring countless buildings, including Jordan Hall at [[Stanford University]], [[Fairmont San Francisco|San Francisco’s Fairmont Hotel]], [[Oakland City Hall]], Berkeley City Hall, [[Sunol Water Temple]], [[Palace of Fine Arts|San Francisco Palace of Fine Arts]],<ref>{{cite web|title=Nomination for the San Francisco Beautiful Award|url=http://www.alicecareymemorial.com/posthumous-awards.html|website=http://www.alicecareymemorial.com|accessdate=25 March 2015}}</ref> and the [[Marin County Civic Center]].  She was a founding member of the Friends of Terra Cotta and on the Boards of several organizations including the Association of Advocates for Preservation, San Francisco Heritage, and the Environmental Design Archives at U.C. Berkeley.<ref>{{cite web|title=Carey & Co. Inc. {{ndash}} Architecture Preservation & Planning|url=http://careyco.com/|website=Carey & Co. website|accessdate=25 March 2015}}</ref>

== Legacy ==
Carey was a champion in the preservation community and advocated for the use of historic resources.<ref>Alice Carey, The Importance of Construction Documents to Restoration Architects. American Archivist. Spring 1996. Accessed 11 Mar. 2015.[http://archivists.metapress.com/content/k25306812x04l27q/]</ref> Throughout her career, Carey fought to preserve countless buildings, including the New Mission Theater,<ref>The Friends of 800. San Francisco's New Mission Theater Chronology. Accessed 11 Mar. 2015.[http://www.friendsof1800.org/NEWMISSION/chronology.html]</ref> the Fairmont Hotel Tonga Room, and the Metropolitan Club at 640 Sutter Street, all in San Francisco.<ref>{{cite web|last1=Ionin|first1=Jonas|title=Historic Preservation Commission Resolution|url=http://commissions.sfplanning.org/hpcpackets/Carey.pdf|website=http://commissions.sfplanning.org|accessdate=25 March 2015}}</ref><ref>{{cite web|title=Alice Ross Carey|url=https://640hpf.org/about-us/governance/alice-ross-carey/|website=https://640hpf.org|accessdate=25 March 2015}}</ref> She received a California Governor's Historic Preservation Awards posthumously in 2013 for her dedication to and work in the field of historic preservation.<ref>{{cite web|title=2013 Governor''s Historic Preservation Awards|url=http://ohp.parks.ca.gov/?page_id=27570|website=http://ohp.parks.ca.gov|accessdate=25 March 2015}}</ref>

== Archives ==
The Alice Carey/Carey & Company Records is held by the Environment Design Archives at the University of California, Berkeley.

== References ==
{{reflist}}

== External links ==
* [http://archives.ced.berkeley.edu/] Environmental Design Archives

<!--- Categories --->

[[Category:Preservationist architects]]
{{DEFAULTSORT:Carey, Alice Ross}}
[[Category:1948 births]]
[[Category:2013 deaths]]
[[Category:People from Brooklyn]]