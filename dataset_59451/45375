{{EngvarB|date=September 2014}}
{{Use dmy dates|date=September 2014}}
{{Infobox aviator
|name            =Graham Gilmour 
|image           = 
|image_size      = 
|alt             = 
|caption         = 
|full_name       =Douglas Graham Gilmour 
|birth_date      ={{Birth date|df=yes|1885|3|5}}
|birth_place     = [[Dartford, Kent]], England
|death_date      ={{death date and age|df=yes|1912|2|17|1885|3|5}}
|death_place     =[[Richmond, Surrey]], England
|death_cause     =Aircraft accident 
|resting_place   =[[Mickleham, Surrey]] 
|resting_place_coordinates = <!-- {{Coord|LATITUDE|LONGITUDE|type:landmark}} -->
|monuments       = 
|nationality     =British 
|education       =[[Clifton College]] 
|spouse          = 
|relatives       = 
|known_for       = 
|first_flight_aircraft =[[Antoinette (manufacturer)|Antoinette]] 
|first_flight_date= 
|famous_flights  = 
|license_date    =19 May 1910 
|license_place   =[[Pau, Pyrénées-Atlantiques|Pau]]

}}
'''Graham Gilmour''' (1885–1912) was a British pioneer aviator, known for his impromptu public displays of flying. He was killed on 17 February 1912 when his [[Martinsyde|Martin-Handasyde]] monoplane suffered a structural failure and crashed in the [[Old Deer Park]] in [[Richmond, London]].

==Early life==
Gilmore was born at [[Blackheath, London|Blackheath]] in Kent on 5 March 1885, the son of David Gilmour of Shanghai and Margaret Jane (née Muirhead), and educated at [[Clifton College]] as an Engineer.<ref name=timesobit >{{Cite newspaper The Times|articlename=Mr D. Graham Gilmour|section=News |date=19 February 1912|day_of_week= |page_number=8|issue=39825|column=}}</ref><ref name="eng" />

He started his practical engineering training at Allens of Bedford from 1905 to 1907 and then to the Adams Motor Company where he specialised in internal combustion engines.<ref name="eng">{{cite paper|newspaper=Manchester Courier and Lancashire General Advertiser|title=Motor Notes – Some reflection|author=H. Thornton Rutter|date=13 March 1912}}</ref><ref name="allen">{{cite paper|newspaper=Luton Times and Advertiser|title=The Late Graham Gilmour|date=23 February 1912}}</ref>

==Aviation career==
Gilmour went to the [[Grande Semaine d'Aviation|Rheims aviation meeting]] in August 1909 and bought himself a Blériot aircraft; he next had to learn to fly it.<ref name="eng" /> Gilmour learnt to fly in France, first at the [[Antoinette (manufacturer)|Antoinette]] school at [[Pau, Pyrénées-Atlantiques|Pau]]<ref>{{cite journal|journal=l'Aérophile|url=http://gallica.bnf.fr/ark:/12148/bpt6k65639070/f172.image|title=au Jour le Jour: Un Peu Partout|page=170|date=1 April 1910|language=French }}</ref> school and later at the [[Blériot Aeronautique|Blériot]] school, and was awarded French flying license No.75 on 19 May 1910.<ref>{{cite journal|journal=l'Aérophile|title=Comité de Direction du 19 Mai 1910  |date=1 July 1910  |pages=310–1  |language=French|url=http://gallica.bnf.fr/ark:/12148/bpt6k65639070/f312.image}}</ref>
<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Eighty-Six Certified Pilots  |date=28 May 1910  |page=408  |language=|url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200410.html }}</ref>

His Blériot did not fare so well: it was delivered from Paris to Pau but was damaged on the railway journey. It was repaired by Blériot and test flown by [[Alfred Leblanc]] but the next day its hangar blew down, and it was not until 9 March 1910 that Gilmour sat in his own aircraft.<ref name="eng" /> It had only taken Gilmour seven flights to gain his French certificate, with only one accident.<ref name="eng" /> Gilmour was the owner of 29 motor cycles and with his engineering training it was not hard for him to handle the Anzani engine fitted to the Blériot.<ref name="eng" />

On returning to England he based himself at Brooklands and quickly established a reputation as an able pilot, flying an [[Anzani 3-cylinder fan engines|Anzani]]-engined [[Blériot XI]] monoplane which he named ''Big Bat'' at the aviation meetings at [[Lanark]] and [[Wolverhampton]].  On 30 Sep he made a flight setting a record for flights at [[Brooklands]], staying in the air for over an hour.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Brooklands Aerodrome  |date=8 October 1910  |page=817  |url=http://www.flightglobal.com/pdfarchive/view/1910/1910%20-%200819.html }}</ref>  At the end of 1910 he was supplied with a [[Bristol Boxkite]] by the [[Bristol Aeroplane Company]] which he fitted with a loaned [[E.N.V.]] engine to make an attempt at winning the [[Michelin Cup]], but the attempt came to nothing owing to a cylinder head blowing off.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Salisbury Plain|date=7 January 1911|pages=14–15 |url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200014.html}}</ref>

This was the beginning of an association with Bristol, with Gilmour at first working at their flying schools at Larkhill and Brooklands, and also gaining publicity by his exploits.  On 1 April, flying a Boxkite, he was one of six pilots to overfly the [[Boat Race]], circling above [[Hammersmith Bridge]] and thrilling the assembled crowd by several times turning his engine off and gliding (at the time considered a daring manouevre), until the competing crews passed under him: he then accompanied them to the finish at [[Mortlake]].  He afterwards ran out of petrol and had to land on the Chiswick Polytechnic cricket field. A passing motorist supplied him with some petrol, and after instruction from Gilmour a man from the crowd which had gathered started the engine, while others hung on to the aircraft until the engine was run up to speed.
<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Seeing the Boat Race by Aeroplane |url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200311.html|date=8 April 1911|page= 313}}</ref> 
On 6 May he took part in a well-publicised race from Brooklands to [[Shoreham-by-Sea|Shoreham]], afterwards bombing the submarine depot in [[Portsmouth]] with oranges.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=After the Race |url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200413.html| date= 13 May 1911|page=415}}</ref>

Gilmour was to have flown a Bristol in the [[Gordon Bennett Trophy (aeroplanes)|Gordon Bennett Trophy]] competition at [[RNAS Eastchurch|Eastchurch]], but his racing aircraft was not ready for the start: instead Gilmour made some exhibition flights in a Bristol biplane.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=The Gordon Bennett Race|url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200586.html|date=8 July 1911|page= 588}}</ref> He was also prevented from competing in the [[Daily Mail aviation prizes|1912 Circuit of Britain]] race, in which he was to have flown a [[Bristol Type T]]. On 5 July Gilmour had made a flight over central London, and was reported to have circled the dome of [[St Paul's cathedral]] before following the course of the [[Thames]] as far as [[Westminster Bridge]], causing members of parliament to crowd the terrace of the [[Houses of Parliament]] to witness his flight.<ref>{{Cite newspaper The Times|articlename=Aeroplane Flight Round St. Paul's|section=News |date=6 July 1911|day_of_week=Thursday|page_number=8|issue=39630|column=E }}</ref> Two days later had made an appearance at the [[Henley Regatta]], flying over the river at an altitude of between 400 and
500&nbsp;ft (120–150&nbsp;m), before flying down and briefly
skimming the water with the aircraft's wheels. On 18 July the [[Royal Aero Club]] suspended his pilot's license for a month for reckless flying.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Aviator's Certificates|url= http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200634.html |page=636|date=22 July 1911}}</ref> Although complaints had been made about his alleged circling of St Paul's the Aero Club committee believed Gilmour's assertion that he had held to the course of the river, but nevertheless banned him for the flight at Henley, despite Gilmour's defence: 
{{quote|The only thing that could have happened would have been for the machine to have stopped through fouling the water ; in that case I should have got a ducking, to the amusement of the public and the great discomfort of myself, but without any danger to anybody. And taking into consideration the other parts of the performance, when I was flying at a height of 400&nbsp; ft to 500&nbsp; ft there was absolutely no danger to the public on the river as I did not pass over their heads too low down but only over the course.}}

Gilmour marked the ban by placing a black crêpe bound mourning wreath above his hangar. The suspension prevented Gilmour from taking part in competitions which were under the control of the Aero Club, but did not prevent him from flying: he is reported as having flown a Martin Handasyde on 23 July.<ref>http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200661.html</ref>

Another crowd-pleasing flight occurred when, after testing a re-built Bristol biplane at [[Filton]], he gave the Bristol workers a display of "some very clever trick flying":<ref>{{cite journal|journal=[[Flight International|Flight]]|title=Flying at Bristol |url=http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200743.html?search=gilmour|date= 26 August 1911|page= 745}}</ref> 
{{quote|First diving sharply to the ground he then rose again at a steep angle, after which he banked the machine very considerably in some sharp turns. A long switchback flight followed, and Mr. Gilmour concluded his fine performance by coming down at a very sharp angle, bringing his machine to rest exactly opposite the door of the shop where it was to spend the night. During the flight Mr. Gilmour took his hands from the controls, and waved a salute to the crowd beneath, afterwards travelling for some distance with his arms folded.}}  He continued to fly for Bristol instructing and making trial flights and late in 1911 began making trial flights for Martin-Handasyde.<ref>{{cite journal|journal=[[Flight International|Flight]]|url= http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%201081.html|page= 1089|date=16 December 1911|title=Brooklands Aerodrome}}</ref>

Although sometimes described as a reckless dare-devil by the popular press, this was not the opinion held of him by the aviation community, who thought him one of England's finest flyers: daring, but not one to indulge in dangerous tricks without appreciating the risk involved, and having a reputation for always carefully inspecting any aircraft before flying it.<ref>{{cite journal|journal=[[Flight International|Flight]]|url= http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200175.html|date= 25 February 1912|page= 175|title=Brooklands Aerodrome}}</ref>

In 1911 he was charged with manslaughter after he ran down a 10-year-old boy at [[Wylye, Wiltshire]] while he was driving a motor-car, Gilmour was overtaking a cart when the boy ran out into the road. At the trial in May 1911 at [[Salisbury]] [[Crown Court]] he was found not guilty of the charge.<ref>"Airman Acquitted on Charge of Manslaughter." Times [London, England] 30 May 1911: 4. The Times Digital Archive. Web. 28 December 2013.</ref>

==Death==
Gilmour had set off from Brooklands at about 11 a.m. to make a trial cross-country flight in a Martin Handasyde monoplane. While flying over the Old Deer Park in Richmond at about 400&nbsp;ft (120&nbsp;m) the aircraft suffered a structural failure and crashed, killing Gilmour instantly. Eyewitnesses reported that the left wing of the aircraft had folded in mid-air, although an examination of the wreckage revealed that all the bracing wires were intact. The accident was possibly due to Gilmour encountering an air pocket: other aviators had encountered such conditions that day.<ref>{{cite journal|journal=[[Flight International|Flight]]|title=The Late Mr Grahame Gilmour|url=http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200172.html|date=24 February 1912|page= 172}}</ref>

An inquest into the death was held at Richmond on 20 February 1912, the coroner and jury first inspected the wreck in the Old Deer Park and had the assistance of the manufacturer Martin & Handyside, an aeronautical engineer and Tom Sopwith who had flown the aircraft previously.<ref name="inq">{{cite paper|newspaper=Evening Telegraph|title=Graham Gilmour Left Letter|date=20 February 1912|page=1}}</ref> Witnesses talked about the state of health of Gilmour and the condition of the machine, a letter from Gilmour with his wishes for funeral was presented to the inquiry.<ref name="inq" /> The coroner said the inquiry had to decide if it was a pure accident or a "weak spot" in the aircraft, the jury after consideration returned a verdict of accidental death, they thought that something had happened to the aircraft but they did not have enough evidence to show what.<ref name="inq" />

His funeral at Mickleham near Dorking, Surrey featured a motor lorry driven by the aviator [[James Radley]] instead of a hearse, the flat bed draped in purple cloth: the grave was lined with pink [[azalea]]s, coloured flowers only were requested and no bells were tolled. The letter Gilmour had left outlining his wishes for his funeral ended "I want every one to be merry and bright, for I don't believe in moaning"<ref name=timesobit /><ref>{{cite paper|newspaper=Dundee Courier|title=Graham Gilmour's Unconventional Funeral|date=22 February 1912|page=3}}</ref> He was buried at [[St. Michael's Churchyard, Mickleham]] with his parents David (1842–1907) and Margaret (1849–1910).

==References==
{{reflist}}

[[Category:1885 births]]
[[Category:1912 deaths]]
[[Category:People educated at Clifton College]]
[[Category:British aviators]]
[[Category:Aviators killed in aviation accidents or incidents in the United Kingdom]]

{{Aviators killed in early aviation accidents}}