__NOTOC__
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name= O5U
 | image=Vought XO5U-1.jpg
 | caption=
}}{{Infobox Aircraft Type
 | type=Observation [[floatplane]]
 | national origin=United States
 | manufacturer=[[Vought]]
 | designer=
 | first flight=8 May 1934
 | introduced=
 | retired=
 | status=
 | primary user=
 | number built=1
 | developed from= 
 | variants with their own articles=
}}
|}

The '''Vought O5U''' was a 1930s prototype [[United States|American]] observation [[floatplane]] to meet a [[United States Navy]] requirement for a [[Aircraft catapult|catapult]] launched scouting aircraft. The contract was won by Curtiss who went on to produce the [[SOC Seagull]]; only one O5U was built.<ref name="heroic">{{cite journal | title=Heroic Seagull | journal=Air Classics | page= | author=Michael O'Leary| url=http://findarticles.com/p/articles/mi_qa3901/is_200211/ai_n9150793/ |date=November 2002}}</ref>

==Development==
The United States Navy contracted three companies to produce prototypes to meet a requirement for a catapult launched biplane, with a central float and folding wings. Douglas produced the XO2D-1, Curtiss the XO3C-1 and Vought the XO5U-1.

The XO5U-1 ([[United States military aircraft serials|serial number]] ''9399'') was powered by a single [[Pratt & Whitney R-1340|Pratt & Whitney R-1340-12]] piston engine<ref name="serial">Andrade 1979, p. 205</ref> and first flew on the 8 May 1934.<ref name="heroic" />

The contract was awarded to Curtiss and only one XO5U-1 was built. The Status of Naval Aircraft, dated June 1937, listed the XO5U-1 as assigned at [[Mustin Field]] at the [[Naval Aircraft Factory]], [[Philadelphia]], Pennsylvania.<ref>Larkins, William T., "U.S. Navy Aircraft 1921-1941 / U.S. Marine Corps Aircraft 1914-1959", Orion Books, a division of Crown Books, New York, 1988, Library of Congress card number 88-17753, ISBN 0-517-56920-5, page 202.</ref> It crashed in May 1938.

==Specifications (XO5U-1) ==
{{Aircraft specs
|ref=<ref name="EcklandKinn">{{cite web|url=http://aerofiles.com/_vot.html|title=Vought|last=Eckland|first=K.O.|publisher=Aerofiles.com|accessdate=21 June 2012|location=USA}}</ref>
|prime units?=imp
<!--
        General characteristics
-->
|crew=2
|length ft=32
|length in=6
|span ft=36
|span in=
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=
|height in=
|height note=
|wing area sqm=
|wing area sqft=
|wing area note=
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- sailplanes -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Pratt & Whitney R-1340]]
|eng1 type=9-cyl. air-cooled radial piston engine
|eng1 kw=<!-- prop engines -->
|eng1 hp=500
|eng1 shp=<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->

|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 shp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->

|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 shp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop dia note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=155
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=
|stall speed mph=
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=15,100
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2=
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|fuel consumption kg/km=
|fuel consumption lb/mi=
|power/mass=
|thrust/weight=

|more performance=
<!--
        Armament
-->
|armament=<!-- add bulletted list here or if you want to use the following 
specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=

|avionics=
}}

==See also==
{{Portal|Aviation|United States Navy}}
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[SOC Seagull|Curtiss O3C]]
*[[Douglas O2D]]
|sequence=<!-- designation sequence, if appropriate -->
|lists=<!-- related lists -->
*[[List of seaplanes and amphibious aircraft]]
}}

==References==
{{commons category|Vought O5U}}
;Notes
{{reflist}}

;Bibliography
{{refbegin}}
* {{cite book |last= Andrade |first= John |title= U.S.Military Aircraft Designations and Serials since 1909|year=1979 |publisher=Midland Counties Publications|isbn= 0-904597-22-9}}
{{refend}}

{{Vought aircraft}}
{{USN observation aircraft}}

[[Category:Vought aircraft|O05U]]
[[Category:United States military reconnaissance aircraft 1930–1939]]
[[Category:Single-engined tractor aircraft]]
[[Category:Biplanes]]
[[Category:Floatplanes]]
[[Category:Amphibious aircraft]]