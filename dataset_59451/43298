{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 |name= Saras
 |image= File:HAL Saras.jpg
 |caption = NAL Saras taking off
}}
{{Infobox Aircraft Type
 |type= Light transport aircraft
 |national origin = [[India]]
 |manufacturer= [[National Aerospace Laboratories]]
 |first flight= 29 May 2004
 |introduced=
 |retired=
 |status= revived<ref name="newindianexpress.com">{{cite web|url=http://www.newindianexpress.com/cities/bengaluru/After-16-Years-and-Rs-300-Crore-Civil-Aircraft-Dream-Crashlands/2016/01/20/article3235255.ece|title=After 16 Years and Rs 300 Crore, Civil Aircraft Dream Crashlands|work=The New Indian Express|accessdate=7 March 2016}}</ref>
 |primary user= [[Indian Air Force]]<small>intended</small>
 |more users= [[Indian Army]]<small>intended</small> <!--up to three more. please separate with <br/>.-->
 |produced=
 |number built = 2 prototypes
 |unit cost= {{INRConvert|139|c|0|nolink=no}}
 |developed from =
 |variants with their own articles=
}}
|}

The '''NAL Saras''' ([[Sanskrit language|Sanskrit]] सारस: "[[Sarus crane|Crane]]") is the first Indian multi-purpose [[Commercial aviation|civilian aircraft]] in the [[List of light transport aircraft|light transport aircraft]] category as designed by the [[National Aerospace Laboratories]] (NAL).

In January 2016, it was reported that the project has been cancelled.<ref name="newindianexpress.com"/> But in February 2017, the project has been revived.<ref>{{cite web|title= NAL Saras|url=http://www.livefistdefence.com/2017/02/6-stand-out-india-stories-on-aeroindia2017-day-1.html}}</ref>

==Design and development==

In the mid-1980s, the Research Council recommended that the NAL should study the civil aviation requirements of India and recommended ways of establishing a viable civil aviation industry. It further recommended that the NAL should carry out a formal techno-economical feasibility study of a multi role light transport aircraft (LTA – renamed SARAS in October 1993). The feasibility study (November 1989) showed that there was a significant demand for a 9–14 seat multi-role LTA in the country and estimated a market potential of about 250–350 aircraft in the next 10 years. NAL submitted the feasibility study report to the Research Council in November 1990 and started its search for an industrial partner.

The project began in 1991 as a collaboration with Russia ([[Myasishchev]] had a similar project called the Duet), but financial trouble led the Russians to drop out early in the project. The project almost came to a halt when it was hit by US-imposed [[International sanctions|sanctions]] in 1998, after India's [[Nuclear testing|nuclear tests]] in [[Pokhran]]. The Saras project was sanctioned on 24 September 1999 with initial schedule of its maiden flight by March 2001.

The original design target parameters included a maximum take-off weight of 6,100&nbsp;kg and a maximum payload of 1,232&nbsp;kg, a high cruise speed of over 600&nbsp;km/h, an endurance of six hours, a maximum flight altitude of 12&nbsp;km (cruise altitude 10.5&nbsp;km), short take-off and landing distances of about 600&nbsp;m, a maximum rate of climb of 12&nbsp;m/s, a low cabin noise of 78&nbsp;dB, a range of 600&nbsp;km with 19 passengers, 1,200&nbsp;km with 14 passengers and 2,000&nbsp;km with eight passengers, a high specific range of 2.5&nbsp;km/kg and a low cost of operation of ₹ 5/km.

The first Saras (PT1) completed its maiden flight at the [[HAL Bangalore Airport|HAL airport]] in [[Bangalore]] on 29 May 2004.<ref>[http://www.nal.res.in/oldhome/pages/sarasfirstexptflight.htm NAL news report, with pictures, of the First Saras Test Flight] {{webarchive |url=https://web.archive.org/web/20070821175014/http://www.nal.res.in/oldhome/pages/sarasfirstexptflight.htm |date=21 August 2007 }}</ref>

While the designed empty weight of the aircraft is around 4,125&nbsp;kg, the first prototype weighed in around 5,118&nbsp;kg. This issue is sought to be addressed by including composite wings and tail by the third prototype. The airframe of Saras-PT2 was built with lighter composites to reduce its overall weight by about 400&nbsp;kg from its first prototype, which was overweight by about 900&nbsp;kg. The aircraft is powered by two Canadian [[Pratt & Whitney]] [[turbo-prop]] engines.<ref>{{cite web|url=http://articles.timesofindia.indiatimes.com/2009-03-06/bangalore/28027693_1_aircraft-crash-saras-project-national-aerospace-laboratories |title=Breaking News TOI Article on Plane Crash |work=The Times of India |date=6 March 2009 |accessdate=21 October 2012}}</ref>

==Current status==
The first prototype will be upgraded to meet the latest design criteria including higher-power 1,200&nbsp;hp (895&nbsp;kW) Pratt & Whitney [[Pratt & Whitney Canada PT6|Canada PT6A-67A]] engines and improvements to the flight control and flight operations systems. The upgraded PT1 is due to make its first flight by the end of 2011 leading to certification and first deliveries in 2013 and 2014 respectively.<ref>{{cite web|url=http://www.flightglobal.com/articles/2011/02/25/353501/saras-back-from-the-brink-again-as-nal-targets-2013.html|title=Saras back from the brink again as NAL targets 2013 certification|author=Reed Business Information Limited|publisher=Flight Global|accessdate=26 December 2014}}</ref>

The IAF has signed up with [[National Aerospace Laboratories]], Bangalore for the purchase of 15 Saras aircraft. “NAL signed a memorandum of understanding with IAF to sell 15 Saras aircraft. The Kanpur unit of Hindustan Aeronautics Ltd will manufacture these planes,” The 14-seater twin-engine aircraft would be used for coastal surveillance as well as training young cadets on transport flying.<ref>[http://www.deccanherald.com/content/190990/home-spun-saras-train-air.html Home-spun Saras to train Air Force’s trainee pilots], New Delhi, 14 Sep 2012, DHNS</ref> The second prototype of the aircraft is overweight by 500&nbsp;kg against the specified design weight of 4125&nbsp;kg. The third prototype has yet to take flight.<ref>{{cite web|url=http://idrw.org/nals-saras-remains-off-radar|title=NAL’s Saras remains off radar|work=idrw.org|accessdate=7 March 2016}}</ref>

As of 20 January 2016 National Aeronautics Limited (NAL) has stopped all work on Saras as the funding for the project stopped by end of 2013. Engineers who were working on NAL Saras got redeployed to other ongoing similar projects with higher strategic importance.<ref>{{Cite web|title = After 16 Years and Rs 300 Crore, Civil Aircraft Dream Crashlands|url = http://www.defencenews.in/article/After-16-Years-and-Rs-300-Crore,-Civil-Aircraft-Dream-Crashlands-2197|website = www.defencenews.in|access-date = 2016-01-20}}</ref>

NAL is hoping to revive funding for the project.<ref>{{cite web|url=https://www.flightglobal.com/news/articles/nal-hopeful-of-funding-to-revive-saras-423225/|title=NAL hopeful of funding to revive Saras|date=17 March 2016|publisher=Flight Global}}</ref> In October 2016, it was reported that government is mulling a revival plan. The Council for Scientific and Industrial Research (CSIR), that had almost shelved the plan, is on a rethink mode with additional funding in the pipeline.[http://english.mathrubhumi.com/mobile/news/india/iaf-plans-for-dedicated-satellite-on-track-sukhoi-to-fire-brahmos-in-3-months-aste--1.1402970]

As of Feb 14 2017, reconfigured first prototype has just been handed over to the IAF’s Aircraft & Systems Testing Establishment (ASTE), which has conducted a few low-speed ground runs. The National Aerospace Lab’s (NAL) director Jitendra J. Jadhav is said to be looking at putting the Saras back into the air by June–July, though officers on the programme seem to think August–September was a more likely timeframe.<ref>{{Cite web|url=http://www.livefistdefence.com/2017/02/6-stand-out-india-stories-on-aeroindia2017-day-1.html|title=6 Stand-out ‘India Stories’ On #AeroIndia2017 Day 1|last=Aroor|first=Shiv|website=Livefist|access-date=2017-02-16}}</ref>

==Incidents and accidents==

On 6 March 2009, 2 [[Indian Air Force]] test pilots, Wing Commander Praveen Kotekoppa and Wing Commander Dipesh Shah along with a Flight Test Engineer Squadron Leader Ilayaraja, were killed when the second prototype Saras aircraft crashed and caught fire in an open field near [[Bidadi]], about 30&nbsp;km from [[Bangalore]].<ref>{{cite web|url=http://www.bharat-rakshak.com/NEWS/newsrf.php?newsid=10677 |title=Indian Military News Headlines :: |publisher=Bharat-Rakshak.com |accessdate=23 November 2010}}</ref> A court of inquiry found that wrong engine relight drills given to the pilots contributed to the crash,<ref>{{cite web|url=http://www.hindu.com/2009/07/21/stories/2009072156231300.htm |title=National : "Wrong relight drills caused Saras crash" |work=The Hindu |date=21 July 2009 |accessdate=23 November 2010}}</ref> concluding that an "Incorrect relight procedure devised by the designer and adopted by the crew at insufficient height leading to rapid loss of altitude and abnormal behaviour of aircraft resulted into accident."<ref name="auto">{{cite web|url=http://dgca.nic.in/accident/reports/VT-XRM.pdf |title=Final Investigation Report on accident to NAL SARAS PT2  |publisher=DGCA.NIC.IN |accessdate= 8 July 2014}}</ref>

==Specifications (Saras)==
{{aircraft specifications
|plane or copter?=plane
|jet or prop?=prop
|crew=3 (Pilot,Co-Pilot,Flight Engineer)
|capacity=14 passengers
|payload main=
|payload alt=
|length main= 15.02 m
|length alt= 49.28 ft
|span main= 14.70 m
|span alt= 48.23 ft
|height main= 5.20 m
|height alt= 17.06 ft
|area main=
|area alt=
|airfoil=
|empty weight main=
|empty weight alt=
|loaded weight main=
|loaded weight alt=
|useful load main= 1,232 kg
|useful load alt= 2,710 lb
|max takeoff weight main= 7,100 kg
|max takeoff weight alt= 15,653 lb
|more general=
|engine (prop)= [[Pratt & Whitney Canada PT6A]]-67A
|type of prop= [[turboprop]]
|number of props=2
|power main= 1200 shp
|power alt= 895 kW
|power original=
|propeller or rotor?= propeller
|propellers= 5-blade constant speed MT-Propellers
|number of propellers per engine= 1
|propeller diameter main= 2.65 m
|propeller diameter alt=
|max speed main= 550 km/h
|max speed alt= 342 mph
|cruise speed main= 520 km/h
|cruise speed alt= 280 knots
|stall speed main=
|stall speed alt=
|never exceed speed main=
|never exceed speed alt=
|range <ref>{{cite web|url=http://nal.res.in/pdf/saras-2011.pdf |title=Saras Brochure}}</ref>
**'''Range with 14 passengers:''' 590 km (45 min reserves)
**'''Range with 8 passengers:''' 1275 km (45 min reserves)
|ferry range main = 1627 km (with 45 min reserves) 
|ceiling main= 9,144 m
|ceiling alt= 30,000 ft<ref name="auto"/>
|climb rate main= 610m/min
|climb rate alt= 2000ft/min
|loading main=
|loading alt=
|thrust/weight=
|power/mass main=
|power/mass alt=
|more performance=*'''Takeoff distance:''' 670 m (2,200 ft) 
*'''Landing distance:''' 900 m (2,950 ft) 
* '''Endurance:''' 4 hours 45 minutes 
|armament= 
|avionics= integrated digital avionics system using ARINC 429 data bus interfaces
}}

==See also==
{{aircontent
|similar aircraft=
*[[Dornier Do 228]]
*[[Embraer/FMA CBA 123 Vector]] 
*[[Beechcraft 1900]]
*[[Beechcraft Model 99]]
*[[Beechcraft Super King Air]]
*[[Piaggio P.180 Avanti]]
*[[Evektor EV-55 Outback]]
|lists=
*[[List of airliners]]
*[[List of civil aircraft]]
* [[List of STOL aircraft]]
|see also=
}}

==References==
{{reflist|colwidth=30em}}

==External links==
* [http://nal.res.in/pages/saras.htm NAL Saras page]
* [http://www.frontlineonnet.com/fl2113/stories/20040702002408900.htm A Soaring Success]
* [http://www.hindu.com/2007/04/18/stories/2007041801131500.htm Saras second Proto-Type Status]
* [http://www.livemint.com/2008/08/15002523/HAL-to-make-India8217s-firs.html HAL to make India's first passenger aeroplane]

{{HAL aircraft}}

[[Category:NAL aircraft|Saras]]
[[Category:Indian civil utility aircraft 2000–2009]]
[[Category:Twin-engined pusher aircraft]]
[[Category:HAL aircraft|Saras]]
[[Category:Turboprop aircraft]]
[[Category:Low-wing aircraft]]
[[Category:T-tail aircraft]]