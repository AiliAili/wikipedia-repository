{|{{Infobox ship begin}}
{{Infobox ship image
|Ship image=Buque Barroso (1865).jpg
|Ship caption=
}}
{{Infobox ship class overview
|Name=
|Builders=
|Operators={{navy|Empire of Brazil}}
|Class before= {{ship|Brazilian ironclad|Brasil||2}}
|Class after={{ship|Brazilian ironclad|Tamandaré||2}}
|Built range=1865–1866
|In service range=1866–1882
|In commission range=1866–1882
|Total ships completed=1
|Total ships scrapped=1
}}
{{Infobox ship career
|Hide header=
|Ship country=Empire of Brazil
|Ship flag={{shipboxflag|Empire of Brazil}}
|Ship name=''Barroso''
|Ship namesake=Admiral [[Francisco Manoel Barroso da Silva]]
|Ship ordered=
|Ship builder=Arsenal de Marinha da Côrte, [[Rio de Janeiro]]
|Ship laid down=21 February 1865
|Ship launched=4 November 1865
|Ship completed=11 January 1866
|Ship decommissioned=1882
|Ship original cost=£55,046
|Ship fate=[[ship breaking|Scrapped]] 1937
|Ship status=
|Ship notes=
}}
{{Infobox ship characteristics
|Hide header=
|Header caption=
|Ship type=Armored [[gunboat]]
|Ship displacement=*{{convert|980|t|LT|sp=us}} (normal)
*{{convert|1354|t|LT|sp=us}} ([[deep load]])
|Ship length={{convert|61.44|m|ftin|abbr=on}}
|Ship beam={{convert|10.97|m|ftin|abbr=on}}
|Ship draft={{convert|2.74|m|ft|abbr=on}} (mean)
|Ship power={{convert|420|ihp|lk=in|abbr=on}}
|Ship propulsion=1 shaft, 1 [[Marine steam engine#Trunk|steam engine]], 2 [[boiler (steam generator)|boiler]]s
|Ship speed={{convert|9|kn|lk=in}}
|Ship range= 
|Ship sail plan=[[Schooner]]-rigged
|Ship complement=149 officers and men
|Ship armament=*1 × [[rifled]] [[120-pounder Whitworth naval gun|120-pounder Whitworth gun]]
*2 × [[rifled]] [[70-pounder Whitworth naval gun|70-pounder Whitworth guns]]
*2 × [[smoothbore]] [[68-pounder gun]]s
*2 × smoothbore 12-pounder guns
|Ship armor=*[[Belt armor|Belt]]: {{convert|51|-|102|mm|in|abbr=on}}
*[[Casemate]]: {{convert|102|mm|in|abbr=on}}
*[[Deck (ship)|Deck]]: {{convert|12.7|mm|in|abbr=on}}
|Ship notes=
}}
|}

The '''Brazilian ironclad ''Barroso''''' was an armoured [[gunboat]] built for the [[Brazilian Navy]] during the [[Paraguayan War]] in the mid-1860s. ''Barroso'' bombarded Paraguayan fortifications in 1866 and 1867 a number of times before she participated in the [[Passagem de Humaitá]] in February 1868. Afterwards the ship provided fire support for the army for the rest of the war. She was assigned to the [[Mato Grosso]] [[Flotilla]] after the war. ''Barroso'' was decommissioned in 1882, but was not [[ship breaking|scrapped]] until 1937.

==Design and description==
''Barroso'' was designed to meet the need of the Brazilian Navy for a small, simple, shallow-draft armored ship capable of withstanding heavy fire. The ship is best characterized as a [[Central battery ship|central battery]] design because the [[casemate]] did not extend the length of the ship. A bronze [[Naval ram|ram]], {{convert|1.8|m|ftin|sp=us}} long, was fitted. The hull was sheathed with [[Muntz metal]] to reduce [[biofouling]]. For sea passages the ship's  free board could be increased to {{convert|1.7|m|ftin|sp=us}} by use of removable [[wikt:bulwark|bulwarks]] {{convert|1.1|m|ftin|sp=us}} high. On riverine operations, the bulkwarks and the ship's masts were usually removed.<ref name=g53>Gratz, p. 144</ref>

The ship measured {{convert|61.44|m|ftin|sp=us}} [[length overall|long overall]], with a [[beam (nautical)|beam]] of {{convert|10.97|m|ftin|sp=us}} and had a mean [[draft (ship)|draft]] of {{convert|2.74|m|ftin|sp=us}}. ''Barosso'' normally displaced {{convert|980|t|LT|sp=us}} and {{convert|1354|t|LT|sp=us}} at [[deep load]]. Her crew numbered 149 officers and men.<ref name=g53/>

===Propulsion===
''Barroso'' had a single [[John Penn (engineer)|John Penn & Sons]] 2-cylinder [[Marine steam engine#Trunk|steam engine]] driving a single 2-bladed propeller. Her engine was powered by two tubular [[boiler (steam generator)|boiler]]s. The engine produced a total of {{convert|420|ihp|lk=in}} which gave the ship a maximum speed of {{convert|9|kn|lk=in}}. The ship's funnel was mounted directly in front of her casemate. ''Barroso'' carried enough coal for six days' steaming.<ref name=g53/>

===Armament===
''Barroso'' mounted one [[120-pounder Whitworth naval gun|120-pounder Whitworth]] and two [[70-pounder Whitworth naval gun|70-pounder Whitworth]] [[rifled muzzle loader]]s, two [[68-pounder gun|68-pounder]] and two 12-pounder [[smoothbore]] guns in her [[casemate]].<ref name=g53/> To minimize the possibility of shells or splinters entering the casemate through the [[Glossary of nautical terms#G|gun ports]] they were as small as possible, allowing only a 24°-arc of fire for each gun. The rectangular, {{convert|9.8|m|ftin|adj=on|sp=us}} casemate had two gun ports on each side as well as the front and rear.<ref name=g7>Gratz, p. 147</ref>

The {{convert|7|in|mm|sing=on|sigfig=3}} shell of the 120-pounder gun weighed {{convert|151|lb|kg|1}} while the gun itself weighed {{convert|16660|lb|kg|1}}. The 70-pounder gun weighed {{convert|8582|lb|kg|1}} and fired a {{convert|5.5|in|mm|sing=on|sigfig=3}} shell that weighed {{convert|81|lb|kg|1}}. The {{convert|7.9|in|mm|sing=on|sigfig=3}} [[roundshot|solid shot]] of the 68-pounder gun weighed approximately {{convert|68|lb|kg|1}} while the gun itself weighed {{convert|10640|lb|kg|1}}. The gun had a range of {{convert|3200|yd}} at an elevation of 12°. The exact type of 12-pounder gun is not known. All of the guns could fire both [[Round shot|solid shot]] and [[Shell (projectile)|explosive shells]].<ref>Holley, p. 34</ref><ref>Lambert, pp. 85–7</ref>

===Armor===

The hull of ''Barroso'' was made from three layers of wood, each {{convert|203|mm|in|sp=us|1}} thick.<ref name=g53/> The ship had a complete [[wrought iron]] waterline [[Belt armor|belt]], {{convert|1.52|m|ft|sp=us|1}} high. It had a maximum thickness of {{convert|102|mm|in|sp=us|0}} covering the machinery and [[magazine (artillery)|magazine]]s, {{convert|51|mm|in|sp=us|0}} elsewhere.<ref name=g53/> The curved [[deck (ship)|deck]], as well as the roof of the casemate, was armoured with {{convert|12.7|mm|in|sp=us|1}} of wrought iron. The casemate was protected by 102&nbsp;millimetres of armour on all four sides, backed by {{convert|609|mm|in|sp=us|1}} of wood capped with a 102&nbsp;mm layer of [[Aspidosperma polyneuron|peroba hardwood]].<ref name=g7/>

==Service==

[[File:Abordagem do encouraçado Barroso e do monitor Rio Grande pelos paraguaios.jpg|thumb|300px|Paraguayans trying to [[Naval boarding|board]] the battleship Barroso and the [[Brazilian monitor Rio Grande|monitor Rio Grande]] during the [[War of the Triple Alliance]].]]

''Barroso'' was laid down at the Arsenal de Marinha da Côrte in [[Rio de Janeiro]] on 21 February 1865. She was launched on 4 November 1865 and completed on 11 January 1866. On 26–28 March 1866 she bombarded the Paraguayan fortifications at [[Curupaity]] where she was hit 20 times, but not significantly damaged. The ship bombarded Curuzu Fort, downstream of Curupaity, on 1 September in company with the ironclads {{ship|Brazilian ironclad|Rio de Janeiro||2}}, {{ship|Brazilian ironclad|Brasil||2}}, {{ship|Brazilian ironclad|Lima Barros||2}}, {{ship|Brazilian ironclad|Tamandaré||2}}, and the monitor {{ship|Brazilian monitor|Bahia||2}}. The ships bombarded Curupaity again on 4 September and ''Barroso'' was hit four more times.<ref name=g49>Gratz, p. 149</ref> On 22 September the Allied army attempted to [[Battle of Curupaity|storm the fortifications]] at Curupaity, supported by fire from the Brazilian ironclads, but was rebuffed with heavy losses.<ref>Meister, p. 12</ref> Between 24 and 29 December ''Barroso'', ''Tamandaré'', ''Brasil'', and 11 gunboats bombarded Curuzu Fort again.<ref name=g49/>

The Brazilians broke through the river defences at Curupaity during daylight on 15 August 1867 with ''Barroso'', ''Tamanadaré'', and eight other ironclads. The ships were hit 256 times, but not seriously damaged, and only suffered 10 killed and 22 wounded. They repeated the operation again on 9 September.<ref>Meister, p. 13</ref> On 19 February 1868 six Brazilian ironclads, including ''Barroso'', [[Passagem de Humaitá|sailed past Humaitá]] at night. Three {{sclass|Pará|monitor|0}} river [[monitor (warship)|monitor]]s, {{ship|Brazilian monitor|Rio Grande||2}}, {{ship|Brazilian monitor|Alagoas||2}} and {{ship|Brazilian monitor|Pará||2}} were lashed to the larger ironclads in case any engines were disabled by the Paraguayan guns. ''Barroso'' led with ''Rio Grande'', followed by ''Bahia'' with ''Alagoas'' and ''Tamandaré'' with ''Pará''. ''Barroso'' continued upstream with the other undamaged ships and they bombarded [[Asunción]] on 24 February. On 23 March ''Rio Grande'' and ''Barroso'' sank the steamer ''Igurey'' and both ships were [[Boarding (attack)|boarded]] by Paraguayan soldiers on the evening of 9 July, although they managed to repel the boarders.<ref name=g50>Gratz, pp. 149–50</ref>

After the war the ship served with the Mato Grosso Flotilla and was decommissioned in 1882. However, ''Barroso'' was not scrapped until 1937.<ref>Gratz, p. 150</ref>

==Footnotes==
{{reflist|30em}}

==References==
* {{cite book|title=Warship 1999–2000|editor=Preston, Antony|chapter=The Brazilian Imperial Navy Ironclads, 1865–1874|author=Gratz, George A.|publisher=Conway Maritime Press|location=London|year=1999|isbn=0-85177-724-4}}
* {{cite book|last=Holley|first=Alexander Lyman|title=A Treatise on Ordnance and Armor|url=https://archive.org/details/treatiseonordnan00hollrich|year=1865|publisher=D. Van Nostrand|location=New York}}
* {{cite book|last=Lambert|first=Andrew|title=Warrior: Restoring the World's First Ironclad|year=1987|publisher=Conway|location=London|isbn=0-85177-411-3}}
* {{cite journal|last=Meister|first=Jurg|year=1977|title=The River Operations of the Triple Alliance Against Paraguay, Part III|journal=F.P.D.S. Newsletter|publisher=F.P.D.S.|location=Akron, Ohio|volume=V|issue=2|pages=10–14}}

==External links==
* [https://web.archive.org/web/20071123102430/http://www.naval.com.br/ngb/B/B017/B017.htm Brief history of ''Barroso''] {{pt icon}}

{{Brazilian ironclads}}

{{Good Article}}

{{DEFAULTSORT:Barroso}}
[[Category:Ships built in Brazil]]
[[Category:Gunboats of the Brazilian Navy]]
[[Category:Ironclad warships of the Brazilian Navy]]
[[Category:1865 ships]]
[[Category:Riverine warfare]]