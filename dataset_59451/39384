{{use dmy dates|date=December 2014}}
{{good article}}
{{Infobox military person
|name         = Arthur Schuyler Carpender
|birth_date   = {{Birth date |1884|10|24|df=y}}
|death_date   = {{Death date and age |1960|1|10|1884|10|24|df=y}}
|birth_place = [[New Brunswick, New Jersey]]
|death_place = [[Washington, D.C.]]
|placeofburial=[[Arlington National Cemetery]]
|placeofburial_label= Place of burial
|image        = Arthur Carpender (colored).jpg
|caption      = Vice Admiral Arthur S. Carpender
|nickname     = Chips
|allegiance={{flag|United States of America|1908}}
|branch={{flag|United States Navy}}
|serviceyears = 1908–1946
|servicenumber = 0-6600
|rank         = [[File:US-O10 insignia.svg|45px|alt=Four stars]] [[Admiral (United States)|Admiral]]
|commands     = [[Ninth Naval District]]<br/>[[Seventh Fleet]]<br/>Destroyers, [[United States Fleet Forces Command|Atlantic Fleet]]<br/>Destroyer Squadron 31<br/>Destroyer Squadron 32<br/>{{USS|Northampton|CA-26|6}}<br/>{{USS|Macdonough|DD-331|6}}<br/>Submarine Division 14<br/>{{USS|Maddox|DD-168|6}}<br/>{{USS|Radford|DD-120|6}}<br/>{{USS|Fanning|DD-37|6}}
|unit         =
|battles      = [[Mexican Revolution]]
* [[United States occupation of Veracruz]]
[[World War I]]
*[[Action of 17 November 1917]]
[[World War II]]
*[[Battle of the Atlantic]]
*[[New Guinea Campaign]]
|awards       = [[Army Distinguished Service Medal]]<br/>[[Navy Distinguished Service Medal]]<br/>[[Legion of Merit]] (2)<br/>[[Distinguished Service Order]] (United Kingdom)<br/>[[Commander of the Order of the British Empire]] (Australia)<br/>Grand Officer of the [[Order of Orange-Nassau]] (Netherlands)
|laterwork    =
}}

'''Arthur Schuyler Carpender''' (24 October 1884 – 10 January 1960) was an American [[Admiral (United States)|admiral]] who commanded the Allied Naval Forces in the [[South West Pacific Area (command)|Southwest Pacific Area]] during [[World War II]].

A 1908 graduate of the [[United States Naval Academy]], Carpender sailed around the world with the [[Great White Fleet]]. He commanded a landing force that went ashore at [[Puerto Cortes, Honduras]] in 1911, and participated in the [[United States occupation of Veracruz]] as [[adjutant]] of the First Regiment of Bluejackets in 1914. As commander of the destroyer {{USS|Fanning|DD-37|6}} in the [[action of 17 November 1917]] during [[World War I]], he engaged the [[U-boat]] ''[[SM U-58|U-58]]'', and forced it to surrender.

At the start of World War II Carpender was Commander Destroyers, [[United States Fleet Forces Command|Atlantic Fleet]]. In July 1942, he arrived in the Southwest Pacific Area, where he became commander of Task Force 51, the naval forces based in [[Western Australia]]. In September 1942, he was appointed commander of the Southwest Pacific Force, later renamed the [[Seventh Fleet]], and Allied Naval Forces, Southwest Pacific Area, which he led through the [[Battle of Buna–Gona]] and the [[Battle of the Bismarck Sea]]. The following year he oversaw the fleet's operations during [[Operation Cartwheel]]. He commanded the [[Ninth Naval District]] from January 1944 until August 1945, retiring in November 1946 with a [[tombstone promotion]] to the rank of admiral.

==Early life==
A direct descendant of [[Wolphert Gerretse|Wolphert Gerretse Van Kouwenhoven]],<ref name="roots">{{cite web |url=http://wc.rootsweb.ancestry.com/cgi-bin/igm.cgi?op=GET&db=wolphert&id=I309865 |accessdate=10 January 2012 |title=Descendants of Wolphert Gerretse Van Kouwenhoven – Person Page 418 |first=David Kipp |last=Conover |publisher=Ancestry.com }}</ref> one of the early settlers the [[New Netherland]] colony,<ref>{{cite news |last=Cohen |first=Joyce |url=https://query.nytimes.com/gst/fullpage.html?res=9C01E1D81231F930A35750C0A9649C8B63 |title=If You're Thinking of Living In/Gerritsen Beach; Secluded Peninsula in South Brooklyn |date=3 March 2002 |accessdate=14 April 2012 |newspaper=[[New York Times]] }}</ref> Arthur Schuyler Carpender was born in [[New Brunswick, New Jersey]], the sixth of seven children of John Neilson Carpender and his wife Anna Neilson née Kemp on 24 October 1884. He was educated at [[St. Paul's School (Concord, New Hampshire)|St. Paul's School]] in [[Concord, New Hampshire]], and [[Rutgers Preparatory School]] in New Brunswick.<ref name="roots"/><ref name="Navy">{{harvnb|Navy Biographies Section|1951|p=1}}</ref>

Carpender was appointed to the [[United States Naval Academy]] by [[United States Senate|Senator]] [[John Kean (New Jersey)|John Kean]] in 1904. He graduated in 1908. At the time midshipmen had to serve two years service at sea before being commissioned, so he reported for duty with the crew of the new [[battleship]] {{USS|Minnesota|BB-22|6}}. This was one of the battleships of the [[Great White Fleet]] sent by [[President of the United States|President]] [[Theodore Roosevelt]] on an epic voyage around the world in 1907. In 1909, Carpender was transferred to the {{USS|Marietta|PG-15|6}}. He was commissioned as an [[ensign]] in the [[United States Navy]] on 6 June 1910.<ref name="Navy"/> Amidst the backdrop of the [[Banana Wars]], he commanded a 16-man landing force from the ''Marietta'' that was put ashore at [[Puerto Cortes, Honduras]], on 14 January 1911 to help protect American citizens during a period of unrest; after four days ashore Carpender's force returned to the ship.<ref>{{cite web |url=http://www.history.navy.mil/library/online/haiti_list_exp.htm |accessdate=12 January 2012 |title=Haiti – List of Expeditions 1901–1929  |publisher=[[Naval History & Heritage Command]] }}</ref>

Leaving the ''Marietta'' in March 1911, Carpender was involved with the fitting out of the new battleship {{USS|Utah|BB-31|6}}.<ref name="Navy"/> Like other naval officers of the day, he acquired a nickname, "Chips" (a traditional nickname for a ship's carpenter in the days of wooden ships).<ref name="Morison, p. 32"/> He married Helena Bleecker Neilson, who was also from New Brunswick, on 30 April 1912. Their marriage produced no children.<ref name="roots"/>

==World War I==
Carpender participated in the [[United States occupation of Veracruz]] in April 1914 during the [[Mexican Revolution]] as [[adjutant]] of the First Regiment of Bluejackets,<ref name="Navy"/> which was formed from sailors from ''Florida'', ''Utah'' and ''Arkansas''.<ref name=NYTimes>{{cite news|url=https://query.nytimes.com/mem/archive-free/pdf?res=F50C13FF3C5E13738DDDA80894DD405B848DF1D3 |title=Vera Cruz Wonders As Our Troops Land |work=New York Times |date=1 May 1914 |accessdate=15 April 2012 |p=1&ndash;2}}</ref> Landing mid-morning on 21 April, the sailors remained under fire on the beachhead until early the next morning when they began their advance through Veracruz. After a series of street fights, they captured the town shortly before noon on 22 April.<ref name=NYTimes/> The town was cleared and defense lines established before it was handed over to United States Army troops on 30 April.<ref name=NYTimes/> On returning to the United States, Carpender was assigned to the Office of Naval Militia Affairs in Washington, D.C.<ref name="Navy"/>

In June 1916, Carpender helped fit out and commission the new [[destroyer]] {{USS|Davis|DD-65|6}} at the [[Bath Iron Works]] in [[Bath, Maine]]. He served as a member of its crew until March 1917, when he assumed command of the destroyer {{USS|Fanning|DD-37|6}}.<ref name="Navy"/> During the [[action of 17 November 1917]], he engaged the [[U-boat]] ''[[SM U-58|U-58]]'', which was forced to the surface and compelled to surrender.<ref>{{cite news | url= https://query.nytimes.com/gst/abstract.html?res=9B07E7D71E3AE433A25753C3A9649D946696D6CF
 | title= Tells Whole Story of Sinking U-Boat |date= 30 December 1917 | newspaper= [[The New York Times]] | accessdate= 29 March 2009 }}</ref> For his part in the engagement, Carpender was awarded the [[Navy Distinguished Service Medal]].<ref name="Navy"/>

In December 1917, Carpender became an aide to the Commander, Destroyer Flotillas Operating in European Waters. In August 1918 he reported to the [[Newport News Shipbuilding and Drydock Company]] to help fit out the new destroyer {{USS|Radford|DD-120|6}}, and assumed command of the ship when it was commissioned on 30 September 1918.<ref name="Navy"/> The ship sailed for Europe in October 1918, escorting a convoy.<ref>{{cite web |url=http://www.history.navy.mil/danfs/r1/radford-i.htm |accessdate=8 April 2012 |title=Radford |publisher=U.S. Navy }}</ref>

==Between the wars==
[[File:Arthur S. Carpender (1928).jpg|thumb|right|upright|Lieutenant Commander Arthur S. Carpender, USN in 1928.]]
Carpender returned to the United States in April 1919, and became a Member of the Naval Examining Board, and Judge Advocate General of the General Court Martial at the [[Naval Training Station Great Lakes]]. In August 1921 he assumed command of the {{USS|Maddox|DD-168|6}}. He reported to the [[Naval Submarine Base New London]] for training in June 1922, after which he was posted to the [[United States Asiatic Fleet]] as commander of Submarine Division 14.<ref name="Navy2">{{harvnb|Navy Biographies Section|1951|p=2}}</ref>

In August 1923 Carpender returned to Washington, D.C., where he served ashore for the next two years in the [[Bureau of Navigation (United States Navy)|Bureau of Navigation]], before becoming executive officer of the {{USS|Pittsburgh|CA-4|6}} in December 1925. He was assigned to the Receiving Ship, New York, from October 1926 until March 1927, when he assumed command of the destroyer {{USS|Macdonough|DD-331|6}}.<ref name="Navy2"/>

Following the familiar pattern of shore duty alternating with sea duty, Carpender served in the Office of the [[Chief of Naval Operations]] in Washington, D.C. from 1928 until 1931. This was followed by two years as executive officer of the [[light cruiser]] {{USS|Omaha|CL-4|6}}. He then attended the [[Naval War College]] at [[Newport, Rhode Island]], after which he returned to the Office of the Chief of Naval Operations. In June 1936, he became Chief of Staff of Destroyers, [[Scouting Force]]. He assumed command of the cruiser {{USS|Northampton|CA-26|6}} in August 1937. In February 1938 he became Professor of Naval Science and Tactics of the [[Naval Reserve Officers Training Corps]] at [[Northwestern University]] at [[Evanston, Illinois]].<ref name="Navy2"/>

==World War II==
Carpender returned to sea duty in September 1939, when he helped fit out a new destroyer squadron, Destroyer Squadron 32. He commanded it until September 1940, when he became Director of Officer Personnel at the Bureau of Navigation. In this capacity, he helped foster the careers of many other officers.<ref name="Navy2"/> In December 1941 he was promoted to [[Rear Admiral (United States)|rear admiral]], as Commander Destroyers, [[United States Fleet Forces Command|Atlantic Fleet]].<ref name="Ancell&Miller"/>

In July 1942, Carpender arrived in the [[South West Pacific Area (command)|Southwest Pacific Area]], where he reported to  [[Vice Admiral (United States)|Vice Admiral]] [[Herbert F. Leary]], the commander of the Southwest Pacific Force and Allied Naval Forces, Southwest Pacific Area. Leary assigned Carpender to replace Captain [[Charles A. Lockwood]] in command of the naval forces based in [[Western Australia]], known as Task Force 51. The main U.S. naval forces based in the west were the submarines, which remained under Lockwood. As a submariner himself, Carpender took a great interest in submarine operations, and did not like what he saw. Carpender and Lockwood did not get along well, and soon came to detest one another. "I've heard about how they run things in the Atlantic Fleet", Lockwood wrote, "so often that I'm ready to shoot any Atlantic Fleet sailor on sight&mdash;and they, after all, haven't done so much to write home about."<ref>{{harvnb|Blair|1975|pp=283–284}}</ref>

On 11 September 1942, Carpender succeeded Leary as  commander of both the Southwest Pacific Force and the Allied Naval Forces, Southwest Pacific Area.<ref name="Morison, p. 32">{{harvnb|Morison|1950|p=32}}</ref> In the former role, he reported to the Commander in Chief, [[United States Fleet]], [[Admiral (United States)|Admiral]]  [[Ernest J. King]]; in the latter he was answerable directly to the Commander in Chief, Southwest Pacific Area, [[General (United States)|General]] [[Douglas MacArthur]]. The new post came with a promotion to the rank of vice admiral, but Carpender was not the most senior naval officer in the theater, as the [[Royal Australian Navy]]′s [[Admiral (Royal Navy)|Admiral]] Sir [[Guy Royle]] and the [[Royal Netherlands Navy]]′s Vice Admiral [[Conrad Emil Lambert Helfrich|Conrad Helfrich]] were both senior to him.<ref name="Wheeler">{{harvnb|Wheeler|1994|pp=346–349}}</ref> The  Southwest Pacific Force was small; when Carpender assumed command, it consisted of just five cruisers, eight destroyers and 20 submarines.<ref name="Morison, p. 32"/>

[[File:USS Bowmin submarine - full view side.jpg|thumb|450px|left|{{USS|Bowfin|SS-287|6}}, one of the submarines based in Western Australia|alt=A submarine, long, sleek and painted grey at anchor. It flies a red and white striped ensign.]]
Leary's reluctance to risk his ships, and his habit of communicating directly with King without going through MacArthur's General Headquarters (GHQ) in [[Brisbane]],  had aroused the ire of MacArthur. Carpender would soon find himself involved in similar conflicts.<ref name="James, p. 226">{{harvnb|James|1975|p=226}}</ref> In October, Carpender rebuffed a request  for the Allied Naval Forces to transport troops to [[Cape Nelson (Papua New Guinea)|Cape Nelson]]. Carpender refused as there was no adequate [[Hydrography|hydrographic]] survey of that part of the Papuan coast, making it dangerous to sail at night, and movements in the area by day were subject to attack from Japanese aircraft. A survey was conducted in October and [[Lighter (barge)|lighter]]s and [[lugger]]s began making their way up the coast to Cape Nelson, escorted on occasion by Royal Australian Navy [[corvette]]s.<ref name="Morison, p. 32"/><ref>{{harvnb|James|1975|pp=231–232}}</ref>

In November 1942, Carpender turned down a similar request from the Commander of Allied Land Forces, [[General (Australia)|General]] Sir [[Thomas Blamey]], for the Allied Naval Forces to escort some small transports to [[Oro Bay]], as the [[Imperial Japanese Navy]] was doing during the [[Battle of Buna–Gona]].<ref>{{harvnb|James|1975|pp=239–242}}</ref> However, Carpender subsequently relented somewhat and, starting in December, small ships escorted by corvettes carried out [[Operation Lilliput]] to deliver vital supplies to Oro Bay.<ref>{{harvnb|Morison|1950|pp=46–47}}</ref> During the [[Pacific Military Conference]] in March 1943, MacArthur's chief of staff, [[Major General (United States)|Major General]] [[Richard K. Sutherland]], spoke to Admiral King and expressed his dissatisfaction with Carpender.<ref>{{harvnb|Buell|1980|pp=319–320}}</ref>

On 15 March 1943, the Southwest Pacific Force, known colloquially as "MacArthur’s Navy", became the [[Seventh Fleet]].<ref>{{harvnb|James|1975|pp=226, 311}}</ref> It remained very small. The Seventh Fleet acquired an amphibious force under the command of Rear Admiral [[Daniel E. Barbey]]. This eventually became the [[VII Amphibious Force]], but for some time most of its strength was only on paper, or ''en route'' to Australia from the United States.<ref>{{harvnb|Morison|1950|pp=130–131}}</ref> Observing the capabilities of [[PT boat]]s during his evacuation from the Philippines, MacArthur encouraged their use, although initial results were disappointing.<ref>{{harvnb|Morison|1950|pp=47–49}}</ref> Carpender made effective use of them  during the [[Battle of the Bismarck Sea]] on 25 March 1943.<ref>{{harvnb|Morison|1950|pp=60–62}}</ref>

[[File:PT boat New Guinea 1943.jpg|thumb|right|A PT boat patrols off New Guinea, 1943|alt=Silhouette of a boat moving at speed across the water.]]
Carpender oversaw the Seventh Fleet's operations during the early stages of [[Operation Cartwheel]], MacArthur's advance towards the main Japanese base at [[Rabaul]]. A crisis arose during the [[Battle of Finschhafen]], when Carpender became reluctant to reinforce the Australian position. As the situation at Finschhafen became increasingly precarious, [[Lieutenant General (Australia)|Lieutenant General]] Sir [[Edmund Herring]] grew frustrated with Carpender's attitude, and appealed to Blamey, who in turn took up the matter with MacArthur. On 29 September 1943, Carpender agreed to use [[high-speed transport]]s to send an additional battalion to Finschhafen, and the crisis passed. Carpender told Lieutenant General [[Frank Berryman]] that he "resented the implication that [[Uncle Sam]]'s Navy was letting [the Australians] down at Finschhafen."<ref>{{harvnb|Horner|1982|pp=299–301}}</ref>

Carpender was replaced by Admiral [[Thomas C. Kinkaid]] on 26 November 1943. For his services in the Southwest Pacific, he was awarded the [[Army Distinguished Service Medal]] by MacArthur, and the [[Legion of Merit]] by the Navy. He was also appointed an honorary [[Commander of the Order of the British Empire]] on the recommendation of the Australian government, and a Grand Officer of the [[Order of Orange-Nassau]] by the Netherlands. He returned to the United States, where he commanded the [[Ninth Naval District]] from 3 January 1944 until 31 August 1945, for which he was awarded a second Legion of Merit.<ref name="Navy3"/>

==Later life==
Carpender's last naval assignment was as Coordinator of Public Relations in the Office of the [[Secretary of the Navy]] from 28 May 1946. He retired from the Navy on 1 November 1946,<ref name="Navy3"/> with a [[tombstone promotion]] to the rank of admiral.<ref name="Ancell&Miller"/> He lived in retirement in [[Washington, D.C.]] until his death on 10 January 1960,<ref name="Ancell&Miller">{{harvnb|Ancell|Miller|1996|pp=510–511}}</ref><ref name="Navy3">{{harvnb|Navy Biographies Section|1951|p=3}}</ref> and was buried in [[Arlington National Cemetery]].<ref>{{cite web |title=Arthur Schuyler Carpender, Admiral, United States Navy |url=http://www.arlingtoncemetery.net/ascarpender.htm |accessdate=11 January 2012 |publisher=[[Arlington National Cemetery]] }}</ref> His papers are held by the [[New Jersey Historical Society]].<ref>{{cite web |url=http://www.history.navy.mil/sources/nj/nni.htm |accessdate=12 January 2012 |title=New Jersey Historical Society |publisher=[[Naval History & Heritage Command]] }}</ref>

==Notes==
{{Reflist|30em}}

==References==
*{{cite book
 | last = Ancell
 | first = R. Manning
 | last2 = Miller
 | first2 = Christine
 | title = The Biographical Dictionary of World War II Generals and Flag Officers: The US Armed Forces
 | location = Westport, Connecticut
 | publisher=Greenwood Press
 | year = 1996
 | isbn = 0-313-29546-8
 | oclc = 33862161
 | ref = harv
}}
*{{cite book
 | last = Blair
 | first = Clay
 | authorlink = Clay Blair
 | title = Silent Victory: The U.S. Submarine War Against Japan 
 | publisher = Lippincott
 | location = Philadelphia
 | year = 1975
 | isbn = 0-397-00753-1 
 | oclc = 821363
 | ref = harv
}}
*{{Cite book
 | first = Thomas B.
 | last = Buell
 | title = Master of Sea Power: A Biography of Fleet Admiral Ernest J. King
 | publisher = Naval Institute Press
 | year=1980
 | location = Annapolis, Maryland
 | isbn=1-55750-092-4
 | oclc = 5799946
 | ref = harv
}}
* {{cite book
 | last = Horner
 | first = David
 | authorlink = David Horner
 | title = High Command, Australia and Allied Strategy, 1939–1945
 | publisher = Allen & Unwin
 | location = Sydney
 | year = 1982
 | isbn = 0-86861-076-3
 | oclc = 9464416
 | ref = harv
}}
*{{cite book
 | last = James
 | first = D. Clayton
 | title = The Years of MacArthur: Volume 2, 1941–1945
 | publisher = Houghton Mifflin
 | location = Boston
 | year = 1975
 | isbn = 0-395-20446-1
 | oclc = 12591897
 | ref = harv
}}
*{{cite book
 | last = Morison
 | first = Samuel Eliot
 | authorlink = Samuel Eliot Morison
 | year = 1950
 | title = Breaking the Bismarcks Barrier: 22 July 1942–1 May 1944
 | series = [[History of United States Naval Operations in World War II]]
 | publisher = Little, Brown and Company
 | location = Boston, Massachusetts
 | oclc = 10310299
 | ref = harv
}}
*{{citation
 | last = Navy Biographies Section
 | title = Admiral Arthur S. Carpender
 | publisher = Naval Historical Center
 | location = Washington, D.C.
 | date = 17 November 1951
 | ref = harv
}}.
*{{cite book
 | last = Wheeler
 | first = Gerald E.
 | authorlink=
 | year = 1994
 | title = Kinkaid of the Seventh Fleet: A Biography of Admiral Thomas C. Kinkaid, U.S. Navy
 | publisher = Naval Historical Center
 | location = Washington, D.C.
 | isbn = 0-945274-26-2
 | oclc = 31078997
 | ref = harv
}}

{{Authority control}}
{{Portal bar|Biography|United States Navy|World War I|World War II}}
{{DEFAULTSORT:Carpender, Arthur S.}}
[[Category:1884 births]]
[[Category:1960 deaths]]
[[Category:American naval personnel of World War I]]
[[Category:American naval personnel of World War II]]
[[Category:United States Navy World War II admirals]]
[[Category:Burials at Arlington National Cemetery]]
[[Category:Companions of the Distinguished Service Order]]
[[Category:Grand Officers of the Order of Orange-Nassau]]
[[Category:Honorary Commanders of the Order of the British Empire]]
[[Category:People from New Brunswick, New Jersey]]
[[Category:Recipients of the Distinguished Service Medal (United States)]]
[[Category:Recipients of the Legion of Merit]]
[[Category:United States Naval Academy alumni]]
[[Category:United States Navy admirals]]