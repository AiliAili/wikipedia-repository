{{Infobox book
| name             = Incidents in the Life of a Slave Girl
| image            = IncidentsInTheLifeOfASlaveGirl.jpg
| caption          = Frontispiece of the first edition
| author           = [[Harriet Ann Jacobs]]
| title_orig       = 
| translator       = 
| illustrator      = 
| cover_artist     = 
| country          = 
| language         = 
| series           = 
| subject          = 
| genre            = [[Slave narrative]]
| publisher        = [[Thayer & Eldridge]]
| pub_date         = 1861
| english_pub_date = 
| media_type       = 
| pages            = 
| isbn             = 
| oclc             = 
| dewey            = 
| congress         = 
| preceded_by      = 
| followed_by      = 
}}
'''''Incidents in the Life of a Slave Girl''''' is an autobiography by a young mother and fugitive slave published in 1861 by [[Lydia Maria Child|L. Maria Child]], who edited the book for its author, [[Harriet Ann Jacobs]]. Jacobs used the [[pseudonym]] '''Linda Brent.''' The book documents Jacobs' life as a slave and how she gained freedom for herself and for her children. Jacobs contributed to the genre of slave narrative by using the techniques of sentimental novels "to address race and gender issues."<ref name="patton">[https://books.google.com/books?id=BGSf-8E_QfoC&pg=PA53&source=gbs_toc_r&cad=4#v=onepage&q&f=false Venetria K. Patton, ''Women in Chains: The Legacy of Slavery in Black Women's Fiction''], Albany, New York: SUNY Press, 2000, pp. 53-55</ref> She explores the struggles and [[sexual abuse]] that female slaves faced on [[slave plantation|plantations]] as well as their efforts to practice motherhood and protect their children when their children might be sold away.

Jacobs' book is addressed to white women in the North who do not fully comprehend the evils of slavery. She makes direct appeals to their humanity  to expand their knowledge and influence their thoughts about slavery as an institution.

Jacobs began composing ''Incidents in the Life of a Slave Girl'' after her escape to New York, while living and working at Idlewild, the [[Hudson River]] home of writer and publisher [[Nathaniel Parker Willis]].<ref>Baker, Thomas N. ''Nathaniel Parker Willis and the Trials of Literary Fame''. New York, Oxford University Press, 2001, p. 4. ISBN 0-19-512073-6</ref> Portions of her journals were published in serial form in the ''[[New-York Tribune]]'', owned and edited by [[Horace Greeley]]. Jacobs' reports of sexual abuse were deemed too shocking for the average newspaper reader of the day, and publication ceased before the completion of the narrative.

Boston publishing house Phillips and Samson agreed to print the work in book form if Jacobs could convince Willis or abolitionist author [[Harriet Beecher Stowe]] to provide a preface. She refused to ask Willis for help and Stowe never responded to her request. The Phillips and Samson company closed.<ref>Yellin, Jean Fagan. ''Harriet Jacobs: A Life''. Cambridge, Massachusetts: Basic Civitas Books, 2004, pp. 146–147. ISBN 0-465-09288-8</ref> Jacobs eventually signed an agreement with the [[Thayer & Eldridge]] publishing house, and they requested a preface by abolitionist [[Lydia Maria Child]], who agreed. Child also edited the book, and the company introduced her to Jacobs. The two women remained in contact for much of their remaining lives. Thayer & Eldridge, however, declared bankruptcy before the narrative could be published.

==Historical context==
After being printed in serial form in the ''New-York Tribune'', ''Incidents in the Life of a Slave Girl'' was published as a complete work in 1861.<ref>Yellin, ''Harriet Jacobs'' (2004), pp. 120–121.</ref> Its publication was soon overshadowed by the start of the [[American Civil War|Civil War]], although it attracted some attention as it addressed themes highlighted by the [[Abolitionism in the United States|abolitionist]] movement.

===Abolitionist works===
{{Main article|Uncle Tom's Cabin}}
''Incidents in the Life of a Slave Girl'' addressed some issues earlier raised in works by white abolitionists, most notably [[Harriet Beecher Stowe]] in her novel, ''[[Uncle Tom's Cabin]]'' (1852), who had artfully combined the genres of slave narratives and sentimental novels.<ref name="patton"/> Stowe based her novel on several slave narratives.

While Stowe wrote about characters who struggle through lives of slavery,<ref>"Uncle Tom's Cabin, An Introduction to," in ''Children's Literature Review''. Ed. Tom Burns. Vol. 131. Detroit: Gale, 2008. p. 93. Web. 27 October 2014.</ref> Jacobs' work presents an authentic account of one black woman's girlhood and adolescence navigating the complex terrain of male-chauvinist slave-based, white-supremacist civilization. Stowe explores the far-reaching influence of [[chattel slavery]], saying that everyone in the United States was implicated in its cycle, including Southern women, people living in the North, and people who did not own slaves. The book aroused anti-slavery support in the North and resistance in the South; meanwhile, several [[anti-Tom novels]] were published by Southern authors who presented a more benevolent view of plantation slavery. President [[Abraham Lincoln]] credited Stowe's work with contributing to the start of the Civil War.<ref>Charles Edward Stowe, ''Harriet Beecher Stowe: The Story of Her Life'' (1911), p. 203</ref>

Jacobs had initially sought Stowe's help, wanting to dictate her story to her. Instead, Stowe proposed to include her account in ''[[A Key to Uncle Tom's Cabin]]'' (1853) and sent it to Mrs. Willis for verification.<ref name="patton"/> Jacobs resented this patronizing detour as a betrayal, as Stowe had thereby revealed the origin of her children whose circumstances of birth she had never discussed with Mrs. Willis. The incident contributed to Jacobs' deciding to write her story on her own. On her behalf, Mrs. Willis told Stowe of Jacobs' plan, but Stowe never responded.<ref name="patton"/>

===Cult of True Womanhood===
{{Main article|Cult of Domesticity}}
In the antebellum period, the [[Cult of Domesticity|Cult of True Womanhood]] was prevalent among upper and middle-class white women. This set of ideals, as described by Barbara Welter, asserted that all women possessed (or should possess) the virtues of piety, purity, domesticity, and submissiveness.<ref>Welter, Barbara. “The Cult of True Womanhood: 1820-1860,” ''American Quarterly'' 18. (1966): 151-74.</ref>  Venetria K. Patton explains that Jacobs and [[Harriet E. Wilson]], who wrote ''[[Our Nig]]'', reconfigured the genres of slave narrative and sentimental novel, claiming the titles of "woman and mother" for black females, and suggesting that society's definition of womanhood was too narrow.<ref name="patton"/> They argued and "remodeled" Stowe's descriptions of black maternity.<ref>Patton (2000), ''Women in Chains'', p. 39</ref>

They also showed that the institution of slavery made it impossible for African-American women to control their virtue, as they were subject to the social and economic power of men.<ref>Larson, Jennifer. "Converting Passive Womanhood to Active Sisterhood: Agency, Power, and Subversion in Harriet Jacobs' ‘Incidents in the Life of a Slave Girl’," ''Women's Studies'' 35.8 (2006): 739-756. Web. 29 October 2014</ref> Jacobs showed that slave women had a different experience of motherhood but had strong feelings as mothers despite the constraints of their position.<ref>Patton (2000), ''Women in Chains'', p. 37</ref>

Jacobs was clearly aware of the womanly virtues, as she referred to them as a means to appeal to female abolitionists to spur them into action to help protect enslaved black women and their children. In the narrative, she explains life events that prevent Linda Brent from practicing these values, although she wants to. For example, as she cannot have a home of her own for her family, she cannot practice domestic virtues.

===Slavery and the Civil War===
When Jacobs' novel was finally published, the Civil War had started, and the novel was buried beneath news of the war.<ref>"Harriet Jacobs," ''The Norton Anthology of American Literature'' (Shorter Eighth Edition). Ed. Nina Baym and Robert S. Levine. New York: W. W. Norton & Company, 2013. 818-19.</ref> Jacobs had wanted to appeal to abolitionists and particularly to gain the support of white affluent middle-class women on behalf of female slaves.

Her book also addresses the influence of the [[Fugitive Slave Act of 1850]] on people in the North as well as the South. This act required law enforcement and private citizens in free states to cooperate in the capture and return of fugitive slaves to their masters, increasing penalties for interference. It made it a felony for anybody who found a refugee slave not to return the slave to his or her owner, but abolitionists and activists on the Underground Railroad continued to aid slaves through the 1850s.

==Plot summary==
{{Slavery}}
Born into [[slavery]] in Edenton, NC in 1813,<ref>{{cite web|last1=Andrews|first1=William L.|url=http://docsouth.unc.edu/fpn/jacobs/bio.html|website=Documenting the American South}}</ref> Linda has happy years as a young child with her brother, parents, and maternal grandmother, who are relatively well-off slaves in good positions. It is not until her mother dies that Linda even begins to understand that she is a slave. At the age of six, she is sent to live in the big house under the extended care of her mother's mistress, who treats her well and teaches her to read. After a few years, this mistress dies and bequeaths Linda to a relative. Her new masters are cruel and neglectful, and Dr. Flint, the father, takes an interest in Linda. He tries to force her into a sexual relationship with him when she comes of age. The girl resists his entreaties and maintains her distance.

Knowing that Flint will do anything to get his way, as a young woman Linda consents to a relationship with a white neighbor, Mr. Sands, hoping he can protect her from Flint. As a result of their relations, Sands and Linda have two mixed-race children: Benjamin, often called Benny, and Ellen. Because they were born to a slave mother, they are considered slaves, under the principle of [[partus sequitur ventrem]], which had been part of southern slave law since the 17th century. Linda is ashamed, but hopes this illegitimate relationship will protect her from assault at the hands of Dr. Flint. Linda also hopes that Flint would become angry enough to sell her to Sands, but he refuses to do so. Instead, he sends Linda to his son's plantation to be broken in as a field hand.

When Linda discovers that Benny and Ellen are also to be sent to the fields, she makes a desperate plan. Escaping to the North with two small children would be nearly impossible. Unwilling either to submit to Dr. Flint's abuse or abandon her family, she hides in the attic of her grandmother Aunt Martha's cabin. She hopes that Dr. Flint, believing that she has fled to the North, will sell her children rather than risk having them escape as well. Linda is overjoyed when Dr. Flint sells Benny and Ellen to a slave trader secretly representing Sands. Promising to free the children one day, Sands assigns them to live with Aunt Martha. Linda becomes physically debilitated by being confined to the tiny attic, where she can neither sit nor stand. Her only pleasure is to watch her children through a tiny peephole.

Mr. Sands marries and is elected as a congressman. When he takes the slave girl Ellen to [[Washington, D.C.]], to be an eventual companion for his newborn daughter, Linda realizes that he may never free their children. Worried that he will eventually sell them, she determines to escape with them to the North. But Dr. Flint continues to hunt for her, and leaving the attic is still too risky.

After seven years in the attic, Linda finally escapes to the North by boat. Benny remains with Aunt Martha.  Linda tracks down Ellen, by then nine years old and living in [[Brooklyn]], New York, in the home of Sands’ cousin, Mrs. Hobbs. Linda is dismayed to see Ellen is being treated as a slave, after the institution was abolished in New York. She fears that Mrs. Hobbs will take Ellen back to the South and put her beyond her mother's reach. Linda finds work as a nursemaid for the Bruces, a family in New York City who treat her very kindly.

Learning that Dr. Flint is still in pursuit, Linda flees to [[Boston]], where she is reunited with her son Benny, who had also escaped. Dr. Flint claims that the sale of Benny and Ellen was illegitimate, and Linda is terrified that he will re-enslave her and her children. After a few years, Mrs. Bruce dies. Linda spends some time living with her children in Boston. She spends a year in England caring for Mr. Bruce's daughter, and for the first time in her life enjoys freedom from racial prejudice. When Linda returns to Boston, she sends Ellen to boarding school. Benny moves to [[California]] with Linda's brother William, who had also escaped to the North. Mr. Bruce remarries, and Linda takes a position caring for their new baby. Dr. Flint dies, but his daughter, Emily, writes to Linda to claim ownership of the fugitive slave.

The Fugitive Slave Act of 1850 is passed by Congress, making Linda and her children extremely vulnerable to capture and re-enslavement, as it requires cooperation by law enforcement and citizens of free states. Emily Flint and her husband, Mr. Dodge, arrive in New York to capture Linda. When the refugee goes into hiding, the new Mrs. Bruce offers to purchase her freedom. At first Linda refuses, unwilling to be bought and sold again, and makes plans to follow Benny to California. Mrs. Bruce buys Linda's freedom from Flint. Linda is grateful to Mrs. Bruce, but expresses disgust at the institution that required such a transaction. Linda notes that she has not yet realized her dream of making a home with her children.

The book closes with two testimonials to its accuracy, one from [[Amy Post]], a white abolitionist, and the other from George W. Lowther, a black anti-slavery writer.

==Character analysis==
'''Linda Brent''' – The protagonist, and a pseudonym for [[Harriet Jacobs]]. At the start of the story, Linda is unaware of her status as a slave due to her first kind masters, who taught her how to read and write. She faces harassment by her subsequent masters, the Flints. Linda learns along the way how to defend herself against her masters. She uses psychological warfare to avoid the advances of Dr. Flint. Jacobs reveals in the beginning of the book that there were aspects of her story that she could not bear to write. She is torn between her desire for personal freedom and responsibility to her family, especially her children Benny and Ellen.

'''Dr. Flint''' – Linda's master, enemy and would-be lover. He has the legal right to do anything he wants to Linda, but wants to seduce her rather than take her by force in [[rape]]. Throughout the book, Linda constantly rebels against him and refuses any sexual dealings. He becomes enraged, obsessing about breaking her spirit. Dr. Flint never recognizes that Linda is a full human being. Dr. Flint objectifies Linda as a woman slave and consistently fights with his wife.

'''Aunt Martha''' – Linda's maternal grandmother and close friend. Religious and patient, she is saddened about the treatment of her children and grandchildren by their white masters. She grieves when her loved ones escape to freedom, as she knows they will never meet again. Family to her must be preserved even at the cost of freedom and happiness. Aunt Martha stands up for herself, speaking to the Flints out of her dignity. She is the only slave whom Dr. Flint respects.

'''Mrs. Flint''' – Linda's mistress and Dr. Flint's wife. Suspecting a sexual relationship between Linda and her husband, she treats the girl viciously. Though a church woman, she is brutal and insensitive to her slaves, representing the corruption caused by slavery. She and her husband fight about Linda, as he protects her from  corporal punishment by Mrs. Flint.

'''Mr. Sands''' – Linda's white sexual partner and the father of her children, Benny and Ellen. Though somewhat kindly, Sands has no real love for his two slave children. But he serves as Linda's portal to partial freedom, and the two can use each other. Sands breaks promises to Linda and he eventually doesn't talk to her anymore. He eventually has another child by his wife and treats that child with more affection than he gave Benny and Ellen.<ref>[http://www.sparknotes.com/lit/incidents/canalysis.html "Analysis of Major Characters for 'Incidents in the Life of a Slave Girl' "], Spark Notes</ref>

==Fictional characters==
[[Harriet Jacobs]] uses fictional names to protect the identities of persons and alters events and characters for dramatic effect. The characters are believed by scholars to generally correspond to Jacobs and people in her life.

'''Linda Brent''' (stand for [[Harriet Jacobs]]), the book's protagonist and a pseudonym for the author.

'''William Brent''' is John Jacobs: Linda's brother, to whom she is close. William's escape from Mr. Sands shows that even a privileged slave desires freedom above all else.

'''Ruth Nash''' is Margaret Horniblow.

'''Emily Flint''' is Mary Matilda Norcom, Dr. Flint's daughter and Linda's legal owner.

'''Dr. Flint''' is Dr. James Norcom. Based on Harriet Jacobs' master, the character of Dr. Flint is drawn with emphasis on his villainy.

'''Aunt Martha''' is Molly Horniblow. She is one of the narrative's most complex characters, representing an ideal of domestic life and maternal love. She works tirelessly to buy her children's and grandchildren's freedom.

'''Mr. Sands''' is [[Samuel Tredwell Sawyer]]; he is Linda's white husband and the father of her children. After arranging to buy the children, he repeatedly breaks promises to Linda to have sex with her.

'''Benny Sands''' is Joseph Sawyer.

'''Ellen Sands''' is Louisa Sawyer.

'''Mr. Bruce''' is [[Nathaniel Parker Willis]].

'''Gertrude Bruce''' is Cornelia Grinnel Willis.<ref>Characters [http://www.sparknotes.com/lit/incidents/characters.html "Characters", ''Incidents in the Life of a Slave Girl''], Spark Notes</ref>

==Critical response==
''Incidents in the Life of a Slave Girl'' initially received favorable reviews, but it quickly lost attention due to the start of the Civil War.<ref>"Harriet Jacobs." ''The Norton Anthology of American Literature'' (Shorter Eighth Edition). Ed. Nina Baym and Robert S. Levine. New York: W. W. Norton & Company, 2013. 818-19. Print.</ref> After the war ended, readers who discovered the work were confused as to the identity of the author; because of the use of the pseudonym, some thought that the author was [[Lydia Maria Child]], or abolitionist author [[Harriet Beecher Stowe]]. The book was accepted as a novel.

Interest in the book revived during the 1970s and 1980s, under the influence of renewed emphasis on minority and women's rights and culture. Historian [[John Blassingame]] argued at the time, however, that Jacobs' novel was not an authentic work by a former slave because it "did not conform to the guidelines of representativeness", noting that it differed from other slave narratives.<ref name="patton"/> Since then other historians and critics have recognized that Jacobs was approaching the issues from a different viewpoint than did the mostly male authors of other narratives. They have noted how Jacobs differed her approach to present her view as a black woman in slavery.

[[Jean Fagan Yellin]] began researching the book in this period. She documented that Harriet Jacobs, an escaped slave who later was freed, was the real author. She also annotated and documented events and people in the novel as related to those in Harriet's life, showing the autobiographical basis of the work.<ref>Yellin, ''Harriet Jacobs'' (2004), p. xi–xii.</ref>

==African-American literary and historiographic canons==

As an African-American novel and historical artifact, ''Incidents'' constituted a critical intervention into the early [[African-American literature|African-American literary]] canon and African-American historiographies of slavery by foregrounding the experiences of black women under slavery.<ref>Hine, Darlene Clark. "Rape and the inner lives of Black women in the Middle West." Signs (1989): 912-920.</ref> Before [[black feminist]] challenges to the African-American literary and historiographic canons in the 1970s and '80s, the former and the latter foregrounded themes of slavery, racism, social inequality, and black cultural resistance, but failed to highlight the specific exigencies faced by black women under slavery; namely, institutionalized rape and reproductive violence. Thus, ''Incidents,'' along with other black women's novels and historiographies, directed critical attention to the ways in which race and gender interacted in the lives of black bondswomen to produce gender-specific conditions of unfreedom.<ref>Davis, Angela Y. Women, race, & class. Vintage, 2011.</ref>

==The Garret==

The space of the garret, in which Jacobs’ confined herself for seven years, has been taken up as a metaphor in black critical thought, most notably by theorist [[Katherine McKittrick]]. In her text ''Demonic Grounds: Black Women and the Cartographies of Struggle'', McKittrick argues that the garret “highlights how geography is transformed by Jacobs into a usable and paradoxical space.”<ref>{{cite book|last1=McKittrick|first1=Katherine|title=Demonic Grounds: Black Women and the Cartographies of Struggle|date=2006|publisher=University of Minnesota Press|page=xxviii}}</ref> When she initially enters her “loophole of retreat,” Jacobs states that “[its] continued darkness was oppressive…without one gleam of light…[and] with no object for my eye to rest upon." However, once she bores holes through the space with a gimlet, Jacobs creates for herself an oppositional perspective on the workings of the plantation—she comes to inhabit what McKittrick terms a "disembodied master-eye, seeing from nowhere.”<ref>{{cite book|last1=McKittrick|first1=Katherine|title=Demonic Grounds: Black Women and the Cartographies of Struggle|date=2006|publisher=University of Minnesota Press|page=43}}</ref> The garret offers Jacobs an alternate way of seeing, allowing her to reimagine freedom while shielding her from the hypervisibility to which black people—especially black women—are always already subject.

Katherine McKittrick reveals how theories of geography and spatial freedom produce alternative understandings and possibilities within Black feminist thought. By centering geography in her analysis, McKittrick portrays the ways in which gendered-racial-sexual domination is spatially organized. McKittrick writes, “Recognizing black women’s knowledgeable positions as integral to physical, cartographic, and experiential geographies within and through dominant spatial models also creates an analytical space for black feminist geographies: black women’s political, feminist, imaginary, and creative concerns that respatialize the geographic legacy of racism-sexism.”<ref>McKittrick, Katherine (2006). ''Demonic Grounds: Black Women and the Cartographies of Struggle''. University of Minnesota Press. p. 53.</ref>

In analyzing the hiding place of Harriet Jacobs (Linda Brent) – the space of her grandmother’s garret - McKittrick illuminates the tensions that exist within this space and how it occupies contradictory positions. Not only is the space of the garret one of resistance and freedom for Brent, but it is also a space of confinement and concealment. That is, the garret operates as a prison and, simultaneously, as a space of liberation. For Brent, freedom in the garret takes the form of loss of speech, movement, and consciousness. McKittrick writes, “Brent’s spatial options are painful; the garret serves as a disturbing, but meaningful, response to slavery.” As McKittrick reveals, the geographies of slavery are about gendered-racial-sexual captivities – in these sense, the space of the garret is both one of captivity and protection for Brent.

==References==
{{reflist}}

==External links==
{{Gutenberg|no=11030|name=Incidents in the Life of a Slave Girl}}	
* {{librivox book | title=Incidents in the Life of a Slave Girl | author=Harriet JACOBS}}
*''[https://books.google.com/books?id=1RwEAAAAYAAJ&dq=Incidents%20in%20the%20Life%20of%20a%20Slave%20Girl&pg=PR4 Incidents in the Life of a Slave Girl],'' at Free Google eBook
* [http://www.freeaudiobooksonline.org/book/incidents-in-the-life-of-a-slave-girl/ ''Incidents in the Life of a Slave Girl audio edition''] - MP3 Streams.
{{Slave narrative|state=expanded}}

{{DEFAULTSORT:Incidents In The Life Of A Slave Girl}}
[[Category:1861 books]]
[[Category:Feminist books]]
[[Category:Slave narratives]]
[[Category:African-American novels]]
[[Category:Works by Lydia Maria Child]]