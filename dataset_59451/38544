{{Infobox Seamount
| Name=Bowie Seamount
| Map= [[File:Bowie Seamount map.jpg|300px]]
| Map caption=Map of Bowie Seamount
| Depth= {{convert|24|m|ft|0|abbr=on}}<ref name="AV"/><ref name="SI"/>
| Height= ~{{convert|3000|m|ft|0|abbr=on}}
| Summit =
| Location= North [[Pacific Ocean]], {{convert|180|km|mi|0|abbr=on}} west of the [[Queen Charlotte Islands]]
| Group=
| Coordinates ={{coord|53|18|24.93|N|135|39|33.23|W|type:mountain |display=inline,title}}
| Country =[[Canada]]
| Type=[[Guyot]]
| Volcanic group=[[Kodiak-Bowie Seamount chain]]
| Age=[[Pleistocene]]
| Last eruption=18,000&nbsp;years ago<ref name="SI"/>
| Discovered=
| Discovered_by=
| First_visit=
| Translation=
| Pronunciation=
| Photo=Bowie Seamount1.jpg
| Photo caption=3-D depiction of Bowie Seamount
}}

'''Bowie Seamount''' is a large [[submarine volcano]] in the northeastern [[Pacific Ocean]], located {{convert|180|km|mi|-1|abbr=on}} west of [[Haida Gwaii]], [[British Columbia]], [[Canada]].

The seamount is named after [[William Bowie (engineer)|William Bowie]] of the [[Coast and Geodetic Survey|Coast &amp; Geodetic Survey]].<ref>{{cite web|publisher=GEOnet Names Server|title=Undersea Features History|url=http://earth-info.nga.mil/gns/html/history.htm|accessdate=2012-03-07}}</ref>

The volcano has a flat-topped summit (thus making it a [[guyot]]) rising about {{convert|3000|m|ft|-3|abbr=on}} above the [[seabed]], to {{convert|24|m|ft|0|abbr=on}} below [[sea level]].<ref name="AV"/> The seamount lies at the southern end of a long underwater volcanic [[mountain range]] called the Pratt-Welker or [[Kodiak-Bowie Seamount chain]], stretching from the [[Aleutian Trench]] in the north almost to the Queen Charlotte Islands in the south.<ref name="AV">{{cite web | title = Bowie Seamount Marine Protected Area Management Plan | publisher = [[Fisheries and Oceans Canada]] | date = August 2001 | url =http://www.pac.dfo-mpo.gc.ca/oceans/mpa/BowieMgmntPln.pdf |accessdate=October 27, 2008|archiveurl=https://web.archive.org/web/20090324232706/http://www.pac.dfo-mpo.gc.ca/oceans/mpa/BowieMgmntPln.pdf|archivedate=2009-03-24}}</ref>

Bowie Seamount lies on the [[Pacific Plate]], a large segment of the Earth's surface which moves in a northwestern direction under the Pacific Ocean. Its northern and eastern flanks are surrounded{{clarify|date=October 2016}}<!--How is it "surrounded" by only two volcanoes?--> by neighboring submarine volcanoes; [[Hodgkins Seamount]] on its northern flank and [[Graham Seamount]] on its eastern flank.

==Geology==

===Structure===
[[Seamount]]s are volcanic mountains which rise from the seafloor. The unlimited supply of water surrounding these volcanoes can cause them to behave differently from volcanoes on land. The lava emitted in eruptions at Bowie Seamount is made of [[basalt]], a common gray to black or dark brown [[volcanic rock]] low in [[silica]] content (the lava is [[mafic]]). When basaltic lava makes contact with the cold sea water, it may cool very rapidly to form [[pillow lava]], through which the hot lava breaks to form another pillow. Pillow lava is typically fine-grained, due to rapid cooling, with a glassy crust, and has radial jointing.<ref>{{cite web | title = Submarine Volcanoes, Vents, Ridges, and Eruptions | publisher = USGS |date=March 13, 2005 | url = http://vulcan.wr.usgs.gov/Glossary/SubmarineVolcano/description_submarine_volcano.html  |accessdate=October 27, 2008}}</ref>

With a height of at least {{convert|3000|m|ft|-3|abbr=on}} and rising to within only {{convert|24|m|ft|0|abbr=on}} of the sea surface, Bowie Seamount is the shallowest submarine volcano on the [[British Columbia Coast]], as well as in Canadian waters, and one of the shallowest submarine volcanoes in the northeast Pacific Ocean.<ref name="AV"/><ref name="SI"/> Most seamounts are found hundreds to thousands of metres below sea level, and are therefore considered to be within the [[deep sea]]. In contrast,{{clarify|date=October 2016}}<!--in contrast with what?--> if Bowie Seamount were on land it would be about {{convert|600|m|ft|-2|abbr=on}} higher than [[Whistler Mountain]] in southwestern British Columbia and {{convert|800|m|ft|-2|abbr=on}} lower than [[Mount Robson]], the highest mountain in the [[Canadian Rockies|Canadian portion of the Rocky Mountains]].<ref name="SI">{{cite web | title = The Bowie Seamount Area | publisher=John F. Dower and Frances J. Fee | date = February 1999 | url = http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/reports/bowback.pdf |accessdate=October 27, 2008|archiveurl=https://web.archive.org/web/20090324232701/http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/reports/bowback.pdf|archivedate=2009-03-24}}</ref>

Bowie Seamount is about {{convert|55|km|mi|0|abbr=on}} long and {{convert|24|km|mi|0|abbr=on}} wide.<ref name="SI"/> Its flat-topped summit is made of weakly consolidated [[tephra]] and consists of two terraces.<ref name="SI"/><ref name="ZUI"/> The lowest terrace is about {{convert|230|m|ft|-1|abbr=on}} below sea level while the highest is about {{convert|80|m|ft|-1|abbr=on}} below sea level, but contains steep-sided secondary summits that rise to within {{convert|25|m|ft|0|abbr=on}} below sea level. From a physical perspective, the effective size of the submarine volcano is possibly a lot greater than its mass alone would suggest.{{clarify|date=October 2016}}<!--what is this supposed to mean--> The effects of other submarine volcanoes along the [[Pacific Northwest]], including [[Cobb Seamount]] off the coast of [[Washington (U.S. state)|Washington]], can be noticed in the composition and abundance of the tiny floating organisms called [[plankton]] up to {{convert|30|km|mi|-1|abbr=on}} away from the seamount summit. Because of its similar size, Bowie Seamount most likely has a similar effect on its adjacent waters.<ref name="SI"/>

===Eruptive history===
[[File:Hodgkins and Bowie.jpg|thumb|right|3-D depiction of [[Hodgkins Seamount]] with Bowie Seamount in the background]]
Bowie Seamount was formed by [[submarine eruption]]s along [[fissure vent|fissures]] in the seabed throughout the [[last glacial period|last glacial, or "Wisconsinian", period]], which began about 110,000&nbsp;years ago and ended between 10,000 and 15,000 years ago. While most submarine volcanoes in the Pacific Ocean are more than one million years old, Bowie Seamount is relatively quite young. Its base was formed less than one million years ago but its summit shows evidence of volcanic activity as recently as 18,000 years ago.<ref name="SI"/> This is very recent in geological terms, suggesting the volcano may yet have some ongoing volcanic activity.<ref name="AV"/>

Close to Bowie's submerged summit, former coastlines cut by wave actions and beach deposits show that the submarine volcano would once have stood above sea level, as either a single volcanic island or as a small cluster of shoals that would have been volcanically active. Sea levels during the last glacial period, when Bowie Seamount was formed, were at least {{convert|100|m|ft|-2|abbr=on}} lower than they are today.<ref name="AV"/><ref name="SI"/>

[[File:Kodiak-Bowie Seamounts.jpg|thumb|left|Map of the Kodiak-Bowie Seamount chain]]

===Origins===
The origin of the volcanism that produced Bowie Seamount is not without controversy.{{clarify|date=October 2016}}<!--it is very unclear what is controversial--> Geological studies indicate that the [[Kodiak-Bowie Seamount chain]] may have formed above a center of upwelling magma called a [[mantle plume]]. The seamounts comprising the Kodiak-Bowie Seamount chain would be formed above the mantle plume and carried away from the mantle plume's magmatic source as the [[Pacific Plate]] moves in a northwesterly direction towards the [[Aleutian Trench]], along the southern coastline of [[Alaska]].<ref name="AA">{{cite book | last = Faure | first = Gunter | coauthors = | title = Origin of Igneous Rocks: The Isotopic Evidence | publisher = Springer | year = 2001 | location = | pages =66, 67 | url = https://books.google.com/books?id=9aVUTgKDNYEC&pg=PA67&lpg=PA67&dq=Bowie+Seamount+origin&source=web&ots=klYcebIALf&sig=aAO3NvXIScWajGc4EKHZBYJjVYM&hl=en&sa=X&oi=book_result&resnum=9&ct=result
| isbn = 978-3-540-67772-7}}</ref>

The volcanic rocks which make up some of the seamounts in the Kodiak-Bowie Seamount chain are unusual in that they have an acid-neutralizing chemical substance like typical ocean-island basalts but a low percentage of [[strontium]] as found at [[mid-ocean ridge]] basalts. However, the strontium-bearing volcanic rocks comprising Bowie Seamount also contain [[lead]]. Therefore, the magma mixtures that formed Bowie Seamount seem to have originated from varying degrees of [[partial melting]] of a depleted source in the Earth's [[mantle (geology)|mantle]] and basalts which had distinctly high lead isotopic ratios. Estimates during geological studies indicate that the abundance of the depleted-source component ranges from 60 to 80 percent.<!--of the total magmatic content?--><ref name="AA"/>

Some aspects of the origin of the Kodiak-Bowie Seamount chain remain uncertain. The volcanic rocks found at the [[Tuzo Wilson Seamounts]] south of Bowie are fresh glassy [[pillow basalt]]s of recent age, as would be expected if these seamounts are located above or close to a mantle plume south of the Queen Charlotte Islands. However, the origin of Bowie Seamount is less certain because even though the seafloor which Bowie lies on formed 16&nbsp;million years ago during the late [[Miocene]] period, Bowie's summit shows evidence of recent volcanic activity. If Bowie Seamount formed above a mantle plume at the site presently occupied by the Tuzo Wilson Seamounts, it has been displaced from its magma source by about {{convert|625|km|mi|-1|abbr=on}} at a rate of about {{convert|4|cm|in|0|abbr=on}} per year. The geologic history of Bowie Seamount is consistent with its flat-topped eroded summit, but the source for Bowie's recent volcanic activity remains uncertain.<ref name="AA"/> Still others, such as Dickens Seamount and Pratt Seamount further north of Bowie Seamount, fall a little to the side of the chain's expected trend.<ref name="FG"/> Another hypothesized origin of some or all seamounts in the Kodiak-Bowie Seamount chain is that they formed on top of the [[Explorer Ridge]], a divergent [[plate tectonics|tectonic plate]] boundary west of [[Vancouver Island]], and have been displaced from it by [[seafloor spreading]].<ref name="AA"/>

Although some of the seamounts in the Kodiak-Bowie Seamount chain appear to follow the expected age progression for a mantle plume trail, others, such as [[Denson Seamount]], are older than that hypothesis would suggest.<ref name="FG">{{cite web | title = Seamounts in the Eastern Gulf of Alaska: A Volcanic Hotspot with a Twist? | publisher = [[National Oceanic and Atmospheric Administration]] |date=July 12, 2005 | url = http://oceanexplorer.noaa.gov/explorations/04alaska/background/volcanic/volcanic.html |accessdate=October 27, 2008}}</ref> As a result, the Kodiak-Bowie Seamount chain has also been proposed by geoscientists to be a mix of ridge and mantle plume volcanism.

==Biology==
[[File:Haida Heritage Centre.jpg|thumb|right|Haida Heritage Centre at Kaay Llnagaay where the Bowie Seamount Marine Protected Area was announced]]
Bowie Seamount supports a biologically rich area with a vigorous ecosystem. Studies have recorded high densities of [[crab]], [[sea star]]s, [[sea anemone]]s, [[sponge]]s, [[squid]], [[octopus]], [[Sebastes|rockfish]], [[halibut]] and [[sablefish]]. Eight species of [[marine mammal]] have been found in the Bowie Seamount area, including [[Steller sea lion]]s, [[orca]], [[humpback whale|humpback]] and [[sperm whale]]s, along with 16&nbsp;varieties of [[seabird]]s.<ref name="ZO">{{cite web | title = Bowie Seamount Marine Protected Area | publisher = [[Fisheries and Oceans Canada]] |date=May 25, 2006 | url = http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/default_e.htm |accessdate=October 27, 2008|archiveurl=https://web.archive.org/web/20090301053819/http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/default_e.htm|archivedate=2009-03-01 }}</ref> This has made Bowie Seamount a rare habitat in the northeast Pacific Ocean and one of the most biologically rich submarine volcanoes on Earth.<ref name="AV"/><ref name="IO"/> The rich marine life is due to the intense food supply of microscopic animals and plants, including [[phytoplankton]] and [[zooplankton]].<ref name="UQ"/>

===Bowie Seamount Marine Protected Area===
Because of its biological richness, Bowie Seamount was designated as Canada's seventh Marine Protected Area on April&nbsp;19, 2008 under the [[Oceans Act]] and has been described as an "Oceanic Oasis".<ref name="ZO"/> The announcement was made by federal Fisheries Minister [[Loyola Hearn]] and [[Guujaaw]], President of the [[Council of the Haida Nation]], in [[Skidegate, British Columbia|Skidegate]] on the [[Queen Charlotte Islands]], also called Haida Gwaii.{{clarify|date=October 2016}}<!--what is also called Haida Gwaii?--> During the announcement, Natural Resources Minister [[Gary Lunn]] said: "Bowie Seamount is an oceanic oasis in the deep sea, a rare and ecologically rich marine area, and our government is proud to take action to ensure it is protected. By working in partnership with the Council of the Haida Nation and groups like the World Wildlife Fund-Canada, we are ensuring this unique treasure is preserved for future generations."<ref name="IO"/> It measures about {{convert|118|km|mi|0|abbr=on}} long and {{convert|80|km|mi|0|abbr=on}} wide, totaling an area of {{convert|6131|km2|sqmi|0|abbr=on}}.<ref name="UQ">{{cite web | title = Designation of Bowie Seamount as a Marine Protected Area | publisher = [[Fisheries and Oceans Canada]] |date=October 11, 2008 | url = http://www.dfo-mpo.gc.ca/media/back-fiche/2008/pr12-eng.htm |accessdate=October 30, 2008}}</ref> This is the northernmost of the two Marine Protected Areas on the British Columbia Coast; the southernmost is the [[Endeavour Hydrothermal Vents]], an active [[hydrothermal vent]] zone of the [[Juan de Fuca Ridge]] {{convert|250|km|mi|-1|abbr=on}} southwest of Vancouver Island.<ref>{{cite web | title = Marine Protected Areas | publisher = [[Fisheries and Oceans Canada]] |date=March 7, 2008 | url= http://www.dfo-mpo.gc.ca/oceans/marineareas-zonesmarines/mpa-zpm/index-eng.htm |accessdate=November 7, 2008}}</ref> The Bowie Seamount Marine Protected Area also includes [[Peirce Seamount]] (also called Davidson Seamount) and [[Hodgkins Seamount]].<ref name="IO"/>

==Diving explorations and studies==
The shallow depth of Bowie Seamount makes it the only underwater mountain off the British Columbia Coast easily reached using [[scuba diving]] equipment. In March 1969, dives were made at the submarine volcano by [[Canadian Forces Maritime Command]] divers from the CSS Parizeau during a new study for device package placement.{{clarify|date=October 2016}} Two dives were made to the summit where [[monochrome photography|monochrome photographs]] were taken to record the environment and some biological samples were gathered to detect possible harmful plants, animals, or bacteria. These specimens were identified at the Pacific Biological Station in [[Nanaimo]], creating a list of eleven varieties of sea bottom invertebrates.<ref name="ZUI">{{cite web | title = Biological Observations at Bowie Seamount: August 3-5, 2003 | publisher=N. McDaniel, D. Swanston, R. Haight, D. Reid and G. Grant | date = October 22, 2003 | url = http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/reports/biologicalobs2003.pdf |accessdate=November 6, 2008|archiveurl=https://web.archive.org/web/20090324232646/http://www.pac.dfo-mpo.gc.ca/oceans/mpa/bowie/reports/biologicalobs2003.pdf|archivedate=2009-03-24}}</ref>

[[File:Bowie Seamount2.jpg|thumb|left|Eastern flank of Bowie Seamount]]
In August 1969, Canadian Forces Maritime Command divers made more dives during scientific studies by the Fisheries Research Board of Canada. They found very dense shoals of [[Sebastes|rockfish]] floating over Bowie's flat-topped summit and a variety of bottom life. A number of monochrome photographs were taken and a few [[seaweed]]s were gathered for documents,{{clarify|date=October 2016}}<!--seaweeds gathered for documents?--> but no species record was created for other types of oceanic life around Bowie Seamount.<ref name="ZUI"/>

In November 1996, an issue of the [[National Geographic Magazine]] included an article titled "Realm of the Seamount", describing dives made at Bowie Seamount by two explorers named Bill Curtsinger and Eric Hiner. They explored the slopes of Bowie Seamount using scuba diving equipment down to {{convert|150|m|ft|-1|abbr=on}}. Their photographs featured one of Bowie's rugged peaks thickly covered with seaweeds and colourful sea bottom invertebrates. Shoals of young rockfish were seen on Bowie's steep flanks.<ref name="ZUI"/>

Scientist Bill Austin of Khoyatan Marine Lab in the Northeast Pacific examined a video made during the National Geographic dives to identify the [[benthic]] [[flora]] and [[fauna]] of Bowie Seamount. From the video, Austin recognized some of the most noticeable invertebrates and noted that a few species generally occurring in the [[intertidal zone]] and in shallow subtidal environments were found deeper than might normally be expected, and were bigger than normal.<ref name="ZUI"/>

A team of five divers, led by photographer/videographer Neil McDaniel, visited the seamount August 3–5, 2003 and conducted a biological and photographic survey of the summit down to depths of about {{Convert|40|m|ft|abbr=on}}. A total of 18 taxa of algae, 83 taxa of conspicuous invertebrates and 12 taxa of fishes were documented, approximately 180 underwater still photographs were taken and approximately 90 minutes of digital video were recorded. Of particular note were the dense schools of rockfish hovering over the summit and numerous curious prowfish.<ref name="ZUI"/>

==Indigenous people==
To the Haida Nation, the [[indigenous peoples of the Pacific Northwest Coast|indigenous people]] who played a key role to establish the Bowie Seamount Marine Protected Area, the submarine volcano is called ''S<U>g</U>aan <U>K</U>inghlas''. In their language it means "Supernatural Being Looking Outward".<ref name="IO">{{cite web | title = Bowie Seamount Designated as Canada's Seventh Marine Protected Area | publisher = [[Fisheries and Oceans Canada]] |date=April 19, 2008 | url = http://www.dfo-mpo.gc.ca/media/npress-communique/2008/pr12-eng.htm |accessdate=October 27, 2008}}</ref>

This seamount has long been recognized by the Haida Nation as a special place. [[Guujaaw]], President of the Council of the Haida Nation, has said: "S<U>g</U>aan <U>K</U>inghlas represents a shift in recognizing the need for respect and care for the Earth. This is a very significant turning point in reversing the trends that have been leading to the depletion of life in the sea."<ref name="IO"/>

==Marine hazard==
Given its shallow depth, Bowie Seamount is a potential marine hazard because of the strong storms that strike the [[British Columbia Coast]] during winter. Waves have been recorded with heights of more than {{convert|20|m|ft|-1|abbr=on}},<ref name="SI"/> enough to expose the summit{{clarify|date=October 2016}}<!--How does a wave height of 20m expose bottom at a depth of 24m?--> and cause devastation to any vessel transiting the area. For this reason, Bowie Seamount is recognized as a hazard to navigation and is avoided by shipping vessels.<ref name="DQ">{{cite web |title = Record of Meeting | publisher = [[Fisheries and Oceans Canada]] |date=February 19, 2002 | url = http://www-comm.pac.dfo-mpo.gc.ca/pages/consultations/oceans/MPA's/BowieSeamount/Skidegate021902MR_e.htm |accessdate=October 27, 2008 |archiveurl = https://web.archive.org/web/20040620080913/http://www-comm.pac.dfo-mpo.gc.ca/pages/consultations/oceans/MPA's/BowieSeamount/Skidegate021902MR_e.htm |archivedate = June 20, 2004}}</ref>

==See also==
{{Portal|Volcanism of Canada}}
*[[Volcanism of Canada]]
*[[Volcanism of Western Canada]]
*[[List of volcanoes in Canada]]

==References==
{{Reflist}}

==External links==
{{commons category|Bowie Seamount}}
*[https://web.archive.org/web/20080513145812/http://www.cpawsbc.org/node/191 Bowie Seamount protected on B.C. Coast]
*[http://assets.panda.org/downloads/bowie_seamount_mpa_for_website_final.pdf Bowie Seamount Marine Protected Area]

{{Good article}}

[[Category:Volcanoes of British Columbia]]
[[Category:Submarine volcanoes]]
[[Category:Guyots]]
[[Category:Seamounts of the Pacific Ocean]]
[[Category:Hotspot volcanoes]]
[[Category:Ridge volcanoes]]
[[Category:Pleistocene volcanoes]]
[[Category:Haida]]
[[Category:Former islands of Canada]]
[[Category:Underwater diving sites in Canada]]
[[Category:Protected areas of British Columbia]]
[[Category:Natural hazards in British Columbia]]
[[Category:Polygenetic volcanoes]]