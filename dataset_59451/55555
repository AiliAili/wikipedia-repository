{{Infobox journal
| title= Annales de la Société Entomologique de France
| ISSN = 0037-9271
| language = French / English
| publisher = [[Société Entomologique de France]]
| country = France
| discipline = Entomology
| history = 1832–present
| frequency = Quarterly
| editor = Pierre Rasmont
| impact = 0.600
| website = http://ann.sef.free.fr/
| cover = [[File:Annales de la Société Entomologique de France cover 46 (3-4).jpg|200px]]
}}

'''''Annales de la Société Entomologique de France''''' ({{ISSN|0037-9271}}) is one of the oldest [[entomology]] [[scientific journal|journals]] in the world. It was founded in 1832, and began a new series (''{{lang|fr|nouvelle serie}}'') in 1965, when it merged with ''{{lang|fr|Revue Française d'Entomologie}}'' and ''{{lang|fr|la Revue de Pathologie végétale et d'Entomologie agricole de France}}''.<ref>{{Cite web |url=http://ann.sef.free.fr/ |title=Annales de la Société Entomologique de France (nouvelle série). Revue internationale d'entomologie - International Journal in Entomology |publisher=[[Société Entomologique de France]] |accessdate=February 27, 2011}}</ref>

==See also==
*[[List of entomology journals]]

==References==
{{reflist}}

==External links==
* {{Official website|http://ann.sef.free.fr/}}
* [http://www.biodiversitylibrary.org/title/8188 ''Annales de la Société Entomologique de France''], early volumes (1832–1922), at the [[Biodiversity Heritage Library]]
* [http://gallica.bnf.fr/ark:/12148/cb34349289k/date ''Annales de la Société Entomologique de France''], early volumes (1832–2002), at [[Bibliothèque nationale de France|Gallica]]

{{DEFAULTSORT:Annales De La Societe Entomologique De France}}
[[Category:Entomology journals and magazines]]
[[Category:Publications established in 1832]]
[[Category:Quarterly journals]]
[[Category:Multilingual journals]]
[[Category:Academic journals published by learned and professional societies]]


{{zoo-journal-stub}}
{{insect-stub}}