{{infobox war faction
|name=Democratic Forces for the Liberation of Rwanda
|native_name=Forces démocratiques de libération du Rwanda
|native_name_lang=
|war=[[Second Congo War]] & the [[Kivu conflict]]
|image=[[File:Logo of the FDLR.jpg|150px]]<br />[[File:Flagge FDLR.svg|150px]]
|caption=Logo & flag of the FDLR.
|active=30 September 2000 – present
|ideology=[[Hutu Power|Hutu nationalism]]
|leaders=[[Ignace Murwanashyaka]]<br />[[Callixte Mbarushimana]]<br />[[Sylvestre Mudacumura]]
|groups=
|headquarters=[[Kibua]] (civilian) and [[Kalonge]] (military)
|area=Eastern [[Democratic Republic of the Congo]]
|strength=6,000 – 7,000 (October 2007)
|partof=
|previous=[[Army for the Liberation of Rwanda]]
|next=
|allies=
|opponents=
|battles=[[Kivu conflict]]
*[[2008 Nord-Kivu campaign]]
*[[2009 Eastern Congo offensive]]
*[[M23 rebellion]]
|url=
}}
{{Hutu militants}}
The '''Democratic Forces for the Liberation of Rwanda''' is the primary remnant Rwandan [[Hutu]] rebel group in the east of the [[Democratic Republic of the Congo]]. It is often referred to as simply the '''FDLR''' after its original [[French language|French]] name: the '''''Forces démocratiques de libération du Rwanda'''''. It has been involved in fighting from its formation on 30 September 2000 throughout the last phase of the [[Second Congo War]] and the  fighting which has continued since then. It is composed almost entirely of ethnic [[Hutu]]s opposed to [[Tutsi]] rule and influence in the region. The FDLR was formed after negotiations between the [[Army for the Liberation of Rwanda]] and the remnant Hutu military command agreed that the ALiR be dissolved. Paul Rwarakabije was appointed commander in chief of the entire force, but ALiR had to accept the political leadership of the FDLR.<ref name=ICG03 />

As of December 2009, Major General [[Sylvestre Mudacumura]] was the FDLR’s overall military commander. He was the former deputy commander of the FAR Presidential Guard in Rwanda in 1994.<ref name=RomkenaVennhoop>Hans Romkena, De Vennhoop [http://www.wilsoncenter.org/index.cfm?topic_id=1417&fuseaction=topics.event_summary&event_id=276321 Opportunities and Constraints for the Disarmament and Repatriation of Foreign Armed Groups in the DRC], Multi Country Demobilization and Recovery Program, April 2007, report on FDLR described as 'excellent' by Prunier 2009.</ref>

The FDLR made a partial separation between its military and civilian wings in September 2003
when a formal armed branch, the Forces Combattantes Abacunguzi (FOCA), was created.<ref name=RomkenaVennhoop />

According to the U.S. National Counterterrorism Center, the FDLR is believed to be responsible for about a dozen terrorist attacks committed in 2009.<ref>{{cite web|title=2009 Report on Terrorism|publisher=U.S. National Counterterrorism Center. Office of the Director of National Intelligence|accessdate=16 April 2011|date=April 30, 2010}}</ref> These acts of terrorism have killed hundreds of civilians in Eastern Congo.

==Dispositions at merger==
Before ALiR merged with the FDLR in September 2000, the military configuration was as follows:
*ALiR was split into two divisions, each containing three brigades of about 2000 men (a total of 12,000 men). The first division was stationed in [[North Kivu]] and the second around the [[Kahuzi-Biéga National Park|Kahuzi Biega forest]] (in the [[Shabunda Territory|Shabunda]], [[Mwenga Territory|Mwenga]], [[Kalehe Territory|Kalehe]] territories) and in [[South Kivu]].
*The FDLR troops consisted of one division of three brigades, plus one more incomplete brigade. After fighting for Kinshasa, troop numbers were down to little more than 7000-8000 men, according to the FDLR. But this figure does not take into account the probable recruitment and training of three supplementary brigades, as reported and denounced by the Rwandan government. After the ALiR/FDLR merger, for logistical reasons, an operations centre for troops present in southern Kivu remained in Kamina."<ref name=ICG03>ICG, Rwandan Hutu Rebels in the Congo; A New Approach to Disarmament and Reintegration, ICG Africa Report No.63, 23 May 2003, p.6</ref>

[[Gerard Prunier]] presents a different picture to the ICG's assessment. As of approximately August 2001, he describes two separate ALiR groups, the 'old' ALiR I in [[North Kivu]], made up of ex-FAR and ''[[Interahamwe]],'' about 4,000 strong, and the 'new' ALiR II operating in [[South Kivu]] out of DR Congo government supported bases in [[Kasai region|Kasai]] and northern [[Katanga Province|Katanga]]. Prunier says of ALiR II that '..it had over 10,000 men, and although many of the officers were old ''genocidaires'' most of the combatants were recruited after 1997. They were the ones that fought around [[Pepa, Democratic Republic of the Congo|Pepa]], [[Moba, Democratic Republic of the Congo|Moba]], and [[Pweto]] in late 2000.'<ref name=Prunier>Gérard Prunier, From Genocide to Continental War: The "Congolese" Conflict and the Crisis of Contemporary Africa, C. Hurst & Co, 2009, ISBN 978-1-85065-523-7, p.268</ref> 'The even newer FDLR had around 3,000 men, based in [[Kamina]] in [[Katanga Province|Katanga]]. Still untried in combat, they had been trained by the Zimbabweans and were a small, fully equipped conventional army.'<ref>Prunier, 2009, p.268, drawing upon interview with EU Great Lakes Political Officer Christian Manahl, Nairobi, September 2001</ref>

It is not clear which if either of these two accounts is correct.
The ALiR is currently listed on the U.S. Department of State's Terrorist Exclusion List as a terrorist organization.<ref>{{cite web|title=Terrorist Exclusion List|url=http://www.state.gov/s/ct/rls/other/des/123086.htm|publisher=Office of the Coordinator for Counterterrorism|accessdate=16 April 2011}}</ref>

==Timeline==
The FDLR counts among its number the original members of the [[Interahamwe]] that carried out the 1994 [[Rwandan Genocide]]. It received extensive backing from, and cooperation from, the government of Congolese President [[Joseph Kabila]], who used the FDLR as a [[war by proxy|proxy]] force against the foreign armies operating in the country, in particular the [[Rwandan Patriotic Army]] and Rwanda-backed [[Rally for Congolese Democracy]]. In July 2002, FDLR units still in Kinshasa-held territory moved into [[North Kivu|North]] and [[South Kivu]]. At this time it was thought to have between 15,000 and 20,000 members. Even after the official end of the Second Congo War in 2002, FDLR units continued to attack Tutsi forces both in eastern DRC and across the border into Rwanda, vastly increasing tensions in the region and raising the possibility of another Rwandan offensive into the DRC – what would be their third since 1996. In mid-2004, a number of attacks forced 25,000 Congolese to [[forced migration|flee their homes]].

Following several days of talks with Congolese government representatives, the FDLR announced on 31 March 2005 that they were abandoning their armed struggle and returning to Rwanda as a [[political party]]. The talks held in [[Rome]], [[Italy]] were mediated by [[Community of Sant'Egidio|Sant'Egidio]]. The Rwandan government stated that any returning genocidaires would face justice, most probably through the [[gacaca court]] system. It was stated that if all of the FDLR commanders, who are believed to control about 10,000 militants, disarmed and returned, a key source of cross-border tensions would be removed.<ref>[http://news.bbc.co.uk/2/hi/africa/4396785.stm Rwandan Hutus end armed struggle], [[BBC]], 31 March 2005</ref>

On October 4, 2005, the [[United Nations Security Council]] issued a statement demanding the FDLR disarm and leave the [[Democratic Republic of the Congo]] immediately. Under an agreement reached in August, the rebels had pledged to leave Congo by September 30.<ref>[http://www.un.org/apps/news/story.asp?NewsID=16090&Cr=democratic&Cr1=congo Security Council calls on Rwandan rebel group to disarm, leave DR of Congo], [[UN News Centre]], 4 October 2005</ref><ref>[http://www.un.org/News/Press/docs/2005/sc8518.doc.htm Security Council deplores failure of foreign armed group to disarm, repatriate combatants from Democratic Republic of Congo] 4 October 2005 [[UN News]]</ref>

In August 2007, the [[military of the Democratic Republic of the Congo|Congolese military]] announced that it was ending a seven-month offensive against the FDLR, prompting a sharp rebuke by the government of Rwanda. Prior to this, Gen. [[Laurent Nkunda]] had split from the government, taking [[Banyamulenge]] (ethnic Tutsis in the DRC) soldiers from the former [[Rally for Congolese Democracy]] and assaulting FDLR positions, displacing a further 160,000 people.<ref>[http://news.bbc.co.uk/2/hi/africa/6947399.stm "Rwanda anger at Congo rebel move"], ''[[BBC News]]'', 15 August 2007</ref>

In October 2007 the [[International Crisis Group]] said that the group's military forces had dropped from an estimated 15,000 in 2001 to 6–7,000 then, organised into four battalions and a reserve brigade in [[North Kivu]] and four battalions in [[South Kivu]].<ref>International Crisis Group [http://www.crisisgroup.org/home/index.cfm?id=5134 Congo: Bringing Peace to North Kivu], Africa Report No.133, 31 October 2007, Annex D, p.27</ref> It named the political and military headquarters as Kibua and Kalonge respectively, both in the jungle covered [[Walikale]] region of [[North Kivu]]. It also said that 'about the same number' of Rwandan citizens, family members of combatants, and unrelated refugees remained behind FDLR lines in separate communities.

In December 2008 DR Congo and Rwanda agreed to attempt to disband the FDLR,<ref>{{cite news|url=http://africa.reuters.com/top/news/usnJOE4B900I.html|title=Mediator says Congo rebel talks make progress|publisher=Reuters|date=2008-12-09|accessdate=2008-12-10}}</ref> though they will have to destroy the organisation by force or otherwise shut it down. On January 20, 2009, the Rwandan Army, in concert with the Congolese government, [[2009 Eastern Congo offensive|entered the DR Congo]] to hunt down lingering FDLR fighters.<ref>{{cite news|url=http://news.bbc.co.uk/2/hi/africa/7839510.stm |title=Rwandan soldiers enter DR Congo|publisher=BBC|date=2009-01-20|accessdate=2009-01-20}}</ref>

===Later developments===
On 9 and 10 May 2009, FDLR rebels were blamed for attacks on the villages of [[Ekingi]] ([[South Kivu]]) and [[Busurungi]] ([[Walikale]] territory, southern boundary of [[North Kivu]]).<ref name="BBC">{{cite news   | last =   | first =   | coauthors =   | title = 'Dozens killed' in DR Congo raids| work =   | place =  | pages =   | language = | publisher = BBC  | date = 13 May 2009  | url = http://news.bbc.co.uk/1/hi/world/africa/8049105.stm | accessdate = 14 May 2009}}</ref>  More than 90 people were killed at Ekingi, including 60 civilians and 30 government troops, and "dozens more" were said to be killed at Busurungi.<ref name="BBC"/>  The FDLR were blamed by the United Nations' [[Office for the Coordination of Humanitarian Affairs]].<ref name="BBC"/> The UN Group of Experts' report, S/2009/603, issued 9 November 2009, said "Consistent statements collected by the Group from FDLR elements who participated in this attack confirmed
that it was conducted in retaliation against the FARDC for the killings in late April 2009 at Shalio."<ref>United Nations Group of Experts on the Democratic Republic of the Congo, Final Report, dated 9 November 2009, S/2009/603, accessible at [http://www.reliefweb.int/rw/rwb.nsf/db900SID/EGUA-7YHV4X?OpenDocument], English language bootleg copy page 50 of 58</ref>

The Group further commented that "The attack at Busurungi on 10 May 2009 was conducted in clear violation of international human rights law and international humanitarian law. The systematic nature of attacks by the FDLR against the civilian population at Busurungi suggests that they could qualify as crimes against humanity. The attack on Busurungi was perpetrated by the elements of the FDLR battalion “Zodiac” under the command of Lt Col Nzegiyumva of the FDLR Reserve Brigade, in turn under the command of Col Kalume. Reportedly, the attacks were also perpetrated by the Special Company under the command of Capt Mugisha Vainquer. Some information received by the Group indicated that the operation was supported by an FDLR commando unit."<ref>Group of Experts, S/2009/603, bootleg English copy page 50-51 of 58</ref>

The FDLR had attacked several other villages in the preceding weeks and clashes occurred between FDLR forces and the Congolese Army, during which government forces are reported to have lost men killed and wounded.<ref name="AP">{{cite news   | last =   | first =   | coauthors =   | title = Dozens of civilians killed in DRCongo rebel attacks: UN| work =   | place =  | pages =   | language = | agency = Associated Press  | date = 13 May 2009  | url = https://www.google.com/hostednews/afp/article/ALeqM5iEw4rts174XhpU0NaFtFR-LB2zSw| accessdate = 14 May 2009}}</ref>  The most recent attacks have forced a significant number of people from their homes in Busurungi to [[Hombo]], {{convert|20|km|mi}} north.<ref name="AP"/> The Congolese Army and MONUC have conducted Operation Kimia II in North and South Kivu to try to eliminate the FDLR, which has not been very successful.<ref name="AP"/>

The FDLR website was hosted in Germany, but after the request of the German newspaper [[Die tageszeitung|Die Tageszeitung]], it was taken offline. The website is now hosted by the Italian provider Register.it.<ref>[http://www.taz.de/1/politik/afrika/artikel/1/webseite-der-hutu-miliz-abgeschaltet/ Webseite der Hutu-Miliz abgeschaltet] 30 August 2009 [[Die tageszeitung|taz]]</ref>

The UN peacekeeping mission [[MONUC]] has been accused of sharing intelligence with the FDLR.<ref>http://allafrica.com/stories/200911120079.html</ref> However, these accusations are unreliable at best as they were made by the New Times, a media outlet under Rwanda state control.  The government of Rwanda has been hostile towards MONUC and during its proxy war with the Congo, its military forces even attacked peacekeepers while part of the CNDP.

On August 24, 2010, the United Nations confirmed that rebels from the FDLR and from the [[Mai Mai]] militia raped and assaulted at least 154 civilians from July 30 to August 3, in the town of [[Luvungi]] in North Kivu province. [[United Nations Secretary-General]] [[Ban Ki-moon]], who had made protecting civilians and combating sexual violence central themes of his presidency, was reported to be outraged by the attack. Atul Khare, deputy head of the U.N.'s peacekeeping department, was dispatched to the region, and [[Margot Wallström]], the organisation's special representative for sexual violence in conflict, was instructed to take charge of the U.N. response and follow up. The United Nations had withdrawn 1,700 peace keepers in recent months, responding to the Congolese government's demand to end the UN peacekeeping mission (recently renamed MONUSCO). Earlier Wallstrom was quoted as saying that this withdrawal would make the struggle against sexual violence in the region significantly more difficult.<ref>{{cite news | title = U.N.'s Ban sends top aide to Congo after mass rape | publisher = Reuters Afrika  | date = 24 August 2010 | url = http://af.reuters.com/article/worldNews/idAFTRE67N5VQ20100824?sp=true | accessdate = 24 August 2010}}</ref>

=== Arrests ===
FDLR chairman [[Ignace Murwanashyaka]] was arrested in Mannheim, Germany, in April 2006, but released shortly thereafter.<ref>[http://allafrica.com/stories/200604260057.html allafrica.com] (subscription required)</ref> However, in November 2009, after pressure applied by the United Nations, the German [[Federal Criminal Police Office (Germany)|Bundeskriminalamt]] captured Murwanashyaka, the 46-year-old chairman of the FDLR, again, along with his 48-year-old deputy, [[Straton Musoni]], in [[Karlsruhe]].<ref name="FDLR leaders arrest">{{cite news|last= |first= |coauthors= |title=FDLR leaders arrest |work= |place= |pages= |language= |publisher=nbc-2 |date=17 November 2009 |url=http://video.nbc-2.com/Global/story.asp?S=11519242 |accessdate=19 November 2009 }}{{dead link|date=December 2016 |bot=InternetArchiveBot |fix-attempted=yes }}</ref> This was considered a severe blow to the FDLR organization.

The trial for Murwanashyaka and Musoni began on May 4, 2011 before the [[Oberlandesgericht]] in [[Stuttgart]]. They are accused of several counts of war crimes and crimes against humanity according to the German [[Völkerstrafgesetzbuch]]. Their trial is the first to be held in Germany for crimes against this law.<ref>{{Cite news| url=http://www.bbc.co.uk/news/world-africa-13275795 | title=Rwanda: Ignace Murwanashyaka and Straton Musoni tried | work=BBC | date=4 May 2011 | accessdate=2011-05-05 }}</ref><ref>{{cite web| url=http://olg-stuttgart.de/servlet/PB/menu/1266093/index.html?ROOT=1182029 | title=Oberlandesgericht Stuttgart (5. Strafsenat) eröffnet Hauptverfahren gegen zwei mutmaßliche Führungsfunktionäre der "Forces Démocratiques de Libération du Rwanda" (FDLR ) | language=German | work=Oberlandesgericht Stuttgart | date=4 March 2011 | accessdate=2011-05-05 }}</ref>

In October 2010, the Executive Secretary, [[Callixte Mbarushimana]], was arrested in France under a sealed warrant from the [[International Criminal Court]] (ICC) for crimes against humanity and war crimes allegedly committed in the Kivus ([[Democratic Republic of the Congo]]). The warrant concerns widespread attacks allegedly committed by FDLR troops against civilians in North and South [[Kivu]] in 2009. The Court's judges state that there are reasonable grounds to believe that Mbarushimana bears criminal responsibility for these attacks, including murder, torture, rape, persecution and inhumane acts. The warrant alleges that Mbarushimana was part of a plan to create a humanitarian catastrophe to extract concessions of political power for the FDLR.<ref>http://www.icc-cpi.int/NR/exeres/7119036D-EE20-449B-862C-19D5B90AC45C.htm</ref>

On 13 July 2012, the ICC announced an arrest mandate against the FDLR chief [[Sylvestre Mudacumura]] for war crimes committed in the Kivus<ref>[http://www.lefigaro.fr/flash-actu/2012/07/13/97001-20120713FILWWW00567-cpi-mandat-d-arret-contre-mudacumura.php CPI : mandat d'arrêt contre Mudacumura], ''[[Le Figaro]]'', 13 juillet 2012.</ref> as well as against [[Bosco Ntaganda]].<ref>[http://www.lefigaro.fr/flash-actu/2012/07/13/97001-20120713FILWWW00608-2e-mandat-d-arret-contre-ntaganda.php 2e mandat d'arrêt contre Ntaganda], ''[[Le Figaro]]'', 13 juillet 2012.</ref>

==See also==
{{Portal|Rwanda}}
* [[Rasta (Congo)]] – a rebel splinter group of the Democratic Forces for the Liberation of Rwanda

== References ==
{{reflist|30em}}

==Further reading==
*Brig. Gen. Jacques Musemakweli, SSR in the Great Lakes Region: A Tool for Security and Sustainable Development, Department for Defence Management and Security Analysis, [[Defence College of Management and Technology]] thesis, August 2006, R/06/963, accessible via [[Defence College of Management and Technology]] Library, [[Defence Academy of the United Kingdom]] Shrivenham, UK
*Hans Romkena, [http://www.mdrp.org/PDFs/MDRP_Diss_Note6.pdf The End in Sight?], Updated MDRP study, August 2009
*United Nations Group of Experts on the Democratic Republic of the Congo, Final Report, dated 9 November 2009, S/2009/603, French language in official UN form accessible at [http://www.reliefweb.int/rw/rwb.nsf/db900SID/EGUA-7YHV4X?OpenDocument], bootleg English version accessible at [http://rabbitsliketrumpets.typepad.com/Group_of_Experts_Report_DRC_november_2009.pdf]
*Miller, Eric: "The Inability of Peacekeeping to Address the Security Dilemma," 2010. ISBN 978-3-8383-4027-2

== External links ==
* [http://cartogracy.com/conflict/drc Cartogracy: Democratic Forces for the Liberation of Rwanda (FDLR)]
* [http://www.oenz.de/ FDLR-Studie von OENZ 2009]
* [http://www.crisisgroup.org/home/index.cfm?id=3426&l=1 "The Congo: Solving the FDLR Problem Once and for All"], [[International Crisis Group]], 12 May 2005

{{Armed groups in the Congo wars}}

{{DEFAULTSORT:Democratic Forces For The Liberation Of Rwanda}}
[[Category:Factions in the Second Congo War]]
[[Category:Rebel groups in Rwanda]]
[[Category:History of the Democratic Republic of the Congo]]
[[Category:Rwandan genocide]]
[[Category:Military history of Rwanda]]
[[Category:Rebel groups in the Democratic Republic of the Congo]]