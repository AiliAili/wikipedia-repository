{{Good article}}
{{Use mdy dates|date=May 2012}}
{{Infobox person
| name = Jacob L. Beilhart
| image = Beilhart.png
| caption = Jacob Beilhart in 1904
| birth name = Jacob L. Beilhart
| birth_date = {{birth date|1867|03|4}}
| birth_place = [[Columbiana County]], [[Ohio]], United States
| residence =
| death_date ={{Death date and age|1908|11|24|1867|3|4}}
| death_place =[[Ingleside, Illinois]]
| occupation =
| religion =
| denomination = [[Spirit Fruit Society]]
| spouse = Olive Louema Blow
| children =
| networth =
| website =
| signature =
}}
'''Jacob L. Beilhart''' (March 4, 1867 – November 24, 1908)<ref name=OhioHistory>{{cite journal|last=Fogarty|first=Robert S.|author2=Grant, H. Roger|title=Free Love in Ohio: Jacob Beilhart and the Spirit Fruit Colony|journal=Ohio History|date=Spring 1980|volume=89|issue=2|pages=206–221|url=http://publications.ohiohistory.org/ohstemplate.cfm?action=detail&Page=0089206.html&StartPage=206&EndPage=221&volume=89&newtitle=Volume%2089%20Page%20206|accessdate=March 30, 2011}}</ref> was the founder and leader of a [[communitarianism|communitarian]] group known as the [[Spirit Fruit Society]]. Beilhart believed that jealousy, [[economic materialism|materialism]], and the fear of losing another's love were at the root of much of the illness in the world. Under his direction, the Spirit Fruit Society sought to model and practice those beliefs.

Beilhart was born in [[Ohio]] and raised in the [[Lutheranism|Lutheran]] faith. He moved to [[Kansas]] when he was 18 and embraced [[Seventh-day Adventist Church|Seventh-day Adventism]]. He later attended college to become a licensed preacher. Eventually, Beilhart came to believe that it was more important to put faith into practice helping others so he left preaching to study nursing and work in a sanitarium. He returned to Ohio in 1899 to start the Spirit Fruit Society, an [[intentional community]] based on his experiences, learning, and views. Beilhart led the group for nine years before he died of [[appendicitis]] and [[peritonitis]] in 1908. Beilhart's commune survived for another 22 years making it one of the longest surviving communes in America.<ref name=Murphy />

==Biography==
Beilhart was born March 4, 1867 in [[Columbiana County]], [[Ohio]] to a Lutheran father and a [[Mennonite]] mother.<ref name=Hinds>{{cite book|last=Hinds|first=William Alfred|title=American communities and co-operative colonies|year=1908|publisher=Charles H. Kerr & Co.|location=Chicago|isbn=978-1-150-99447-0|pages=556–562|url=https://books.google.com/books?id=wl89AAAAYAAJ&dq=%22Spirit+Fruit+Society%22&output=text&source=gbs_navlinks_s}}</ref><ref name=Echoes>{{cite journal|last=Murphy|first=James L.|title=Jacob Beilhart and the Spirit Fruit Society|journal=Echoes|date=July 1979|volume=18|issue=7|pages=1, 3|url=https://kb.osu.edu/dspace/bitstream/handle/1811/38882/Beilhart.pdf;jsessionid=45888EF8F2249C9A8FE5BE153FF4938A?sequence=1|accessdate=March 28, 2011}}</ref> The ninth of 11 children,<ref name=Murphy>{{cite book|last=Murphy|first=James L.|title=The Reluctant Radicals|year=1989|publisher=University Press of America|location=Lanham, MD|isbn=978-0-8191-7423-9}}</ref> Beilhart was raised in the Lutheran church and his early home environment was strictly religious. Jacob did not receive much in the way of formal education. His father died when he was six or seven years old so Jacob attended only elementary school. At the age of 17, Beilhart went to work at his brother-in-law's harness shop in southern Ohio and, when they moved to [[Ottawa, Kansas]], Jacob went with them.<ref name=OhioHistory /><ref name=Murphy /><ref name=Tutwiler>{{cite web|last=Tutwiler|first=Paul|title=Santa Cruz Spirituality: Spirit Fruit Society|url=http://www.santacruzpl.org/history/articles/494/|work=Santa Cruz County History -Religion & Spirituality|accessdate=March 10, 2011}}</ref>

Beilhart left his brother-in-law's home a year later and went to live and work on a sheep farm. The family he lived with were devout Seventh Day Adventists. During this time, he became convinced that their view of the Bible and its teachings were quite different than what he had been raised to understand, but he accepted their religion entirely.

Although it is not known for certain, it is possible that the sheep farmer was the father of Olive Louema Blow, whom Beilhart married in 1887.<ref name=Murphy /> Jacob and Louema attended [[Pacific Union College|Healdsburg College]] in [[California]] where Jacob received a license to preach. Beilhart returned to Kansas to begin a career in preaching. After two years, however, faced with the prospect of being sent to teach in other areas, Beilhart left preaching. He maintained that he wanted to do something "besides talk."<ref name=OhioHistory /> Beilhart felt a strong need to help the sick so he enrolled in a nursing program at the [[Battle Creek Sanitarium]] that was run by Dr. [[John Harvey Kellogg]].{{Cref2|a}}<ref name=OhioHistory />

Once, while working at the Sanitarium, Beilhart was called to the bedside of a young girl for whom doctors had given up hope. When Beilhart prayed for her, she experienced a quick and complete recovery. Beilhart's conviction that faith-healing was the remedy for illness put him in disfavor with the sanitarium officials and he was asked to resign.<ref name=OhioHistory />

Beilhart became friends with [[C. W. Post]],{{Cref2|b}} who was a patient at the sanitarium, but Post was healed by a [[Christian Science]] [[faith healer]], Mrs Elizabeth K. Gregory.<ref name= Murphy /> In 1892, Post started ''La Vita Inn'', a sanitarium of his own and brought Beilhart on as an associate.<ref name=Tutwiler /> The two men took instruction in Christian Science. Both Post and Beilhart rejected the [[doctrine]] of the religion but they embraced the view that illness was an illusion and could be overcome by mental suggestion, prayer, and [[self-sacrifice]].<ref name=Murphy /><ref name=Tutwiler /><ref name=Dretske>{{cite web|last=Dretske|first=Diana|title=Beilhart's Spirit Fruit Society|url=http://lakecountyhistory.blogspot.com/2009/09/beilharts-spirit-fruit-society.html|work=Illuminating Lake County, Illinois History|accessdate=March 28, 2011}}</ref>

After the birth of their daughter, Edith in 1896, Louema revealed that the father of both Edith and her older brother, Harvey (1893), was C. W. Post. Though Post offered him financial interest in ''La Vita Inn'' and his prospective famous breakfast drink, Postum, provided Beilhart he would stay and work at the Inn. Outraged at his friend's betrayal, Beilhart ordered Post out of his house and left [[Battle Creek]] for Ohio. Louema left Jacob and took the children back to her family in Kansas in 1897. This experience undoubtedly had a profound effect upon Beilhart's view of marriage.<ref name=Murphy/>

==The Spirit Fruit Society==
{{Main|Spirit Fruit Society}}
In 1896, Beilhart returned to Ohio and settled in [[Lisbon, Ohio|Lisbon]], close to where he had grown up. In 1899 he decided to create an intentional community in order to practice his newly developed beliefs and to model this practice for others.<ref name=OhioHistory /> Beilhart also started two newspapers called ''Spirit Fruit'' and ''Spirit Voice'' that were distributed widely by subscription.<ref name=Tutwiler /> The Spirit Fruit Society was incorporated as a religious organization in 1901. The stated goal in the incorporation documents was to, "teach mankind how to apply the truths taught by Jesus Christ".<ref name=OhioHistory /><ref name = Murphy/>
Jacob made no effort to solicit members for his commune other than through his newspapers and sometimes rejected applicants when he felt they were not fit candidates. The commune attracted only about a dozen residents – mostly from outside the Ohio area.<ref name=OhioHistory />

Beilhart preached in Chicago and elsewhere, but was not known to proselytize strongly; however, although the group typically kept to themselves, their mysterious nature led to misconceptions and suspicion in the press.<ref name="Echoes"/> In 1904, numerous newspaper articles and editorials were written, mostly in Chicago, about the Society. Those articles were often sensationalist and served to put the Society in a bad light.<ref name=OhioHistory /> The views and practices of the Society, particularly those against marriage and promoting free love, were not well accepted in the small Ohio community.<ref name=OhioHistory /> Thus, in 1904, the group left Lisbon for Chicago where they expected their progressive ideas might be better tolerated.<ref name=Dretske />

==Chicago and Jacob's death==
Beilhart had preached in Chicago on a number of occasions and felt that the reception of the Society by that community would be better. Unfavorable publicity followed the group to Chicago however, and this period lasted only a relatively short time. In 1906 Beilhart purchased the 90 acre "Dalziel Farm" on the shores of Wooster Lake. Owing to his beliefs against personal property, almost immediately after the purchase he had his name removed from the deed because he did not want anything in his name.<ref name=Murphy />

In November 1908, returning from a meeting in Chicago, Beilhart fell ill with stomach pains. He was diagnosed with appendicitis, and a surgeon was brought in. There is some discrepancy concerning the diagnosis once surgery was performed. One doctor indicated that his appendix was healthy while another maintained that it had ruptured. Beilhart developed peritonitis and died on November 24, 1908.<ref name=Murphy /><ref name=OttawaObit>{{cite news|title=Father Jacob Dies|url=https://news.google.com/newspapers?nid=2289&dat=19081204&id=xBQoAAAAIBAJ&sjid=6wQGAAAAIBAJ&pg=897,1572072|accessdate=21 May 2012|newspaper=The Ottawa Free Trader -|date=27 November 1908}}</ref>

In accordance with his beliefs, the Society buried Beilhart in an unmarked grave on the farm overlooking Wooster Lake. Although most of the area is now covered by a residential development, Beilhart's grave site has been preserved, surrounded by brush.<ref name=Murphy /><ref name=Dretske />

==Notes==
{{Cnote2 Begin|liststyle=lower-alpha|colwidth=40em}}
{{Cnote2|a|Kellogg would later go on to invent [[corn flakes]] with his brother, [[Will Keith Kellogg]].<ref name=OhioHistory />}}
{{Cnote2|b|During his friendship with Post, the two men invented a breakfast drink called [[Postum]]. This drink eventually became the basis for Post's breakfast cereal company.<ref name=Echoes />}}
{{Cnote2 End}}

==References==
{{reflist}}

{{DEFAULTSORT:Beilhart, Jacob L.}}
[[Category:1867 births]]
[[Category:People from Lisbon, Ohio]]
[[Category:People from Ottawa, Kansas]]
[[Category:Founders of utopian communities]]
[[Category:1908 deaths]]