{{Good article}}
{{Infobox person
| honorific_prefix          = 
| name                      = Jean Bartik
| honorific_suffix          = 
| image                     = Jean_Bartik.jpg
| image_size                = 
| alt                       = 
| caption                   = 
| native_name               = 
| native_name_lang          = 
| birth_name                = Betty Jean Jennings
| birth_date                = {{birth date |1924|12|27}}
| birth_place               = [[Gentry County, Missouri]]
| death_date                = {{death date and age |2011|03|23 |1924|12|27}} 
| nationality               = American
| alma_mater                = [[Northwest Missouri State University|Northwest Missouri State Teachers College]] (honorary D. Sc., 2002), [[University of Pennsylvania]] (B.S., 1945; Master's 1967)
| awards                    = [[Computer Pioneer Award]] <small>(2008)</small>
| module                    = {{infobox engineering career
| discipline           = 
| institutions         = 
| practice_name        = 
| employer             = [[University of Pennsylvania]],<br>[[Eckert–Mauchly Computer Corporation]],<br>[[Auerbach Publishers]],<br>Data Decisions
| significant_projects = [[ENIAC]]
| significant_design   = 
| significant_advance  = 
| significant_awards   = [[WITI Hall of Fame]] <br> [[Computer History Museum]] Fellow (2008)
}}
| spouse                    = William Bartik
}}

'''Jean Jennings Bartik''' (December 27, 1924 – March 23, 2011) was one of the original programmers for the [[ENIAC]] computer. She studied mathematics in school then began work at the [[University of Pennsylvania]], first manually calculating [[Ballistics|ballistics trajectories]], then using ENIAC to do so. She and her colleagues developed and codified many of the fundamentals of [[Programming language|programming]] while working on the ENIAC, since it was the first computer of its kind. After her work on ENIAC, Bartik went on to work on [[BINAC]] and [[UNIVAC]], and spent time at a variety of technical companies as a writer, manager, engineer and programmer. She spent her later years as a real estate agent and died in 2011 from congestive heart failure complications.

==Early life and education==
Born '''Betty Jean Jennings''' in [[Gentry County, Missouri]], in 1924, she was the sixth of seven children. Her father, William Smith Jennings (1893-1971) was from [[Alanthus Grove, Missouri|Alanthus Grove]], where he was a schoolteacher as well as a farmer. Her mother, Lula May Spainhower (1887-1988) was from [[Alanthus, Missouri|Alanthus]]. Jennings had three older brothers, William (January 10, 1915) Robert (March 15, 1918); and Raymond (January 23, 1922); two older sisters, Emma (August 11, 1916) and Lulu (August 22, 1919), and one younger sister, Mable (December 15, 1928).<ref name=":0">{{cite web|url=http://www.computerhistory.org/fellowawards/hall/bios/Jean,Bartik/|title=Jean Bartik|website=Computer History Museum|accessdate=October 31, 2016}}</ref>

In her childhood, she would ride on horseback to visit her grandmother, who bought the young girl a newspaper to read every day and became a role model for the rest of her life. She began her education at a local [[one-room school]], and gained local attention for her [[softball]] skill. In order to attend high school, she lived with her older sister in the neighboring town, where the school was located, and then began to drive every day despite being only 14. She graduated from Stanberry High School in 1941, aged 16.<ref name=":1">{{Cite web|url=http://www-groups.dcs.st-and.ac.uk/~history/Biographies/Bartik.html|title=Jean Bartik biography|last=O'Connor|first=J. J.|last2=Robertson|first2=E.F.|website=www-groups.dcs.st-and.ac.uk|accessdate=2016-06-26}}</ref>

She attended [[Northwest Missouri State University|Northwest Missouri State Teachers College]], majoring in mathematics with a minor in English and graduating in 1945. Jennings was awarded the only mathematics degree in her class.<ref name=":0"/> Although she had originally intended to study journalism, she decided to change to mathematics because she had a bad relationship with her adviser.<ref name="ieee"/> Later in her life, she earned a master's degree in English at the [[University of Pennsylvania]] in 1967 and was awarded an honorary doctorate degree from Northwest Missouri State University in 2002.<ref name=":0"/>

==Career==
[[File:Two women operating ENIAC.gif|right|thumbnail|upright=1.5|thumb|Programmers Betty Jean Jennings (left) and [[Fran Bilas]] (right) operate the ENIAC's main control panel.]]
{{Listen|type=speech|pos=right|filename=The ENIAC Programmers (As Told By U.S. Chief Technology Officer Megan Smith).ogg|title=The ENIAC Programmers (As Told By U.S. Chief Technology Officer Megan Smith)|description= }}
In 1945, the Army was recruiting mathematicians from universities to aid in the war effort; despite a warning by her adviser that she would be just "a cog in a wheel" with the Army, and despite encouragement to become a mathematics teacher instead, Bartik decided to become a [[human computer]]. Her calculus professor, encouraged Bartik to take the job at University of Pennsylvania because they had a differential analyzer.<ref name="ieee"/><ref name=autobio>{{cite book|last=Bartik|first=Jean Jennings|editor1-last=Rickman|editor1-first=Jon T.|editor2-last=Todd|editor2-first=Kim D.|authorlink=Jean Jennings Bartik|year=2013|title=Pioneer programmer: Jean Jennings Bartik and the computer that changed the world|publisher=Truman State University Press|location=Kirksville, Missouri|isbn=9781612480862|oclc=854828754|ref=harv}}</ref>

She applied to both [[IBM]] and the University of Pennsylvania at the age of 20. Although rejected by IBM, Jennings was hired by the [[University of Pennsylvania]] to work for Army Ordnance at [[Aberdeen Proving Ground]], calculating [[ballistics]] [[trajectory|trajectories]] by hand.<ref name=":1"/><ref name="NYTobit">{{Cite web|url=https://www.nytimes.com/2011/04/08/business/08bartik.html|title=Jean Bartik, Software Pioneer, Dies at 86|last=Lohr|first=Steve|date=2011-04-07|work=[[The New York Times]]|accessdate=2011-04-08}}</ref> While working there, Bartik met her husband, William Bartik, who was an engineer working on a Pentagon project at the University of Pennsylvania. They married in December 1946.<ref name="ieee"/><ref name=NYTobit/>

When the Electronic Numeric Integrator and Computer (ENIAC) was developed for the purpose of calculating the ballistic trajectories human computers like Bartik had been doing by hand, she applied to become a part of the project and was eventually selected to be one of its first programmers. Bartik was asked to set up problems for the ENIAC without being taught any techniques.<ref name=":2" />  Bartik, who became the co-lead programmer (with [[Betty Holberton]]), and the other four original programmers became extremely adept at running the ENIAC; with no manual to rely on, the group reviewed diagrams of the device, interviewed the engineers who had built it, and used this information to teach themselves the skills they needed.<ref name=ieee>{{cite journal|last=Barkley Fritz|first=W.|title=The Women of ENIAC|journal=IEEE Annals of the History of Computing|date=1996|volume=18|issue=3|doi=10.1109/85.511940|pages=13–28}}<!--|accessdate=21 April 2014--></ref> Initially, they were not allowed to see the ENIAC's hardware at all since it was still classified and they had not received security clearance; they had to learn how to program the machine solely through studying schematic diagrams.<ref name="witi"/> The five-woman team were also not initially given space to work together, so they found places to work where they could, in abandoned classrooms and fraternity houses.<ref name="ieee"/>

While the five women worked on ENIAC, they developed [[subroutine]]s, [[Nesting (computing)|nesting]], and other fundamental programming techniques, and arguably invented the discipline of programming digital computers.<ref name=":1"/><ref name="NYTobit"/> Bartik's programming partner on the ENIAC was [[Betty Holberton]]. Together, they created the master program for the ENIAC and led the ballistics programming group.<ref name="witi" /> The team also learned to physically modify the machine, moving switches and rerouting cables, in order to program it.<ref name="witi"/> In addition to performing the original ballistic trajectories they were hired to compute, they soon became operators on the [[Los Alamos National Laboratory|Los Alamos]] nuclear calculations, and generally expanded the programming repertoire of the machine.<ref name="ieee"/> Bartik was chosen along with Betty Holberton to write a program to display trajectory simulations for the first public demonstration of the ENIAC.<ref name="autobio" />

Bartik described the first public demonstration of the ENIAC in 1946:{{blockquote|The day ENIAC was introduced to the world was one of the most exciting days of my life. The demonstration was fabulous. ENIAC calculated the trajectory faster than it took the bullet to travel. We handed out copies of the calculations as they were run. ENIAC was 1,000 times faster than any machine that existed prior to that time. With its flashing lights, it also was an impressive machine illustrating graphically how fast it was actually computing.<ref name=ieee/>}}

Even though Bartik played an integral part in developing ENIAC,her work at [[University of Pennsylvania]] and on the [[ENIAC]] was completely hidden until it was documented in the film [[Top Secret Rosies: The Female "Computers" of WWII]]. Bartik and the four other original programmers later became part of a group including [[John von Neumann|John Von Neumann]] and [[Dick Clippinger]], charged with converting the ENIAC into a [[stored program computer]]. After the end of the war, Bartik went on to work with the ENIAC designers [[John Eckert]] and [[John Mauchly]],<ref name=":2">{{Cite book|title=Grace Hopper and the Invention of the Information Age|last=Beyer|first=Kurt|publisher=MIT Press|year=2009|isbn=978-0-262-01310-9|location=|pages=178–181|quote=|via=}}</ref> and helped them develop the [[BINAC]] and [[UNIVAC I]] computers.<ref name=":1"/><ref name="NYTobit"/> BINAC was the first computer to use [[Magnetic tape data storage|magnetic tape]] instead of [[Punched card|punch cards]] to store data and the first computer to utilize the twin unit concept, and Bartik programmed a guidance system to run on it for [[Northrop Corporation|Northrop Aircraft]].<ref name="ieee"/> She was responsible for designing the UNIVAC's [[Logic gate|logic circuits]].<ref name=":1"/> Recalling her time working with Eckert and Mauchly on these projects, she described their close group of computer engineers as a "technical Camelot."<ref name="NYTobit"/>

In the early 1950s, she moved to Washington with her then-husband to train [[Remington Rand]] programmers and write UNIVAC programs for the Navy. Later, the couple moved to Philadelphia when William Bartik took a job with Remington Rand based there; Jean Bartik, frustrated at being barred from working in the same Remington Rand location as her husband, left the industry for 16 years, during which time the couple had three children.<ref name=":1"/>

==Later life==
In 1967, Bartik joined the [[Auerbach Corporation]], writing and editing technical reports on [[minicomputer]]s.<ref name=":1"/><ref name=ieee/> She remained with Auerbach for eight years, then moved among positions with a variety of other companies for the rest of her career as a manager, writer, and engineer.<ref name=":1"/> She and William Bartik divorced in 1968.<ref name="NYTobit"/> Bartik ultimately retired from the computing industry in 1986 when her final employer, Data Decisions, closed down; she spent the following 25 years as a real estate agent. She died from [[Heart failure|congestive heart failure]] in a [[Poughkeepsie, New York]] nursing home on March 23, 2011.<ref name=":1"/><ref name="NYTobit"/>

==Awards and honors==
* Inductee, [[Women in Technology International]] [[WITI Hall of Fame|Hall of Fame]] (1997).<ref name="witi">{{cite web|title=ENIAC Programmers|url=http://www.witi.com/center/witimuseum/halloffame/298369/ENIAC-Programmers-Kathleen-McNulty,-Mauchly-Antonelli,-Jean-Jennings-Bartik,-Frances-Synder-Holber-Marlyn-Wescoff-Meltzer,-Frances-Bilas-Spence-and-Ruth-Lichterman-Teitelbaum/|work=WITI Hall of Fame|publisher=Women in Technology International (WITI)|accessdate=22 April 2014}}</ref>
* Fellow, [[Computer History Museum]] (2008)<ref>{{cite web
 | url = http://www.computerhistory.org/fellowawards/hall/bios/Jean,Bartik/
 | title = Jean Bartik
 | publisher = Computer History Museum
 | accessdate = 2013-05-23
}}</ref><ref>{{Cite web|url=https://googleblog.blogspot.com/2008/12/jean-bartik-untold-story-of-remarkable.html|title=Jean Bartik: the untold story of a remarkable ENIAC programmer|last=Kleiman|first=Kathy|website=Official Google Blog|access-date=2016-06-27}}</ref>
* [[IEEE Computer Pioneer Award]], [[IEEE Computer Society]] (2008)<ref name="award">{{cite web|url=https://www.computer.org/web/awards/pioneer-betty-jean-bartik|title=Betty Jean Jennings Bartik|publisher=IEEE Computer Society|work=IEEE Computer Society Awards|accessdate=22 April 2014}}</ref> 
* Korenman Award from the Multinational Center for Development of Women in Technology (2009)<ref name=":1" /><ref name="award" />
The Jean Jennings Bartik Computing Museum at [[Northwest Missouri State University]] in [[Maryville, Missouri]] is dedicated to the history of computing and Bartik's career.<ref>{{Cite web|url=http://www.nwmissouri.edu/archives/computing/index.htm|title=Jean Jennings Bartik Computing Museum {{!}} Special Collections & Archives {{!}} Northwest|last=University|first=Northwest Missouri State|website=www.nwmissouri.edu|access-date=2016-06-27}}</ref>

Content-management framework [[Drupal]]'s default theme, ''Bartik,'' is named in honor of her.<ref>{{Cite web|title = Bartik {{!}} Drupal.org|url = https://www.drupal.org/project/bartik|website = www.drupal.org|accessdate = 2015-11-16}}</ref>

==See also==
* [[Adele Goldstine]]
* [[Betty Holberton]]
* [[Frances Spence]]
* [[Ruth Teitelbaum]]
* [[Marlyn Wescoff]]
* [[Kathleen McNulty]]
* [[List of pioneers in computer science]]

==References==
{{reflist}}

==External links==
{{external media
|video1=[https://www.youtube.com/watch?v=aPweFhhXFvY Jean Bartik and the ENIAC Women], "Computer History Museum", November 10, 2010
|video2= [https://www.youtube.com/watch?v=buAYHonF968 Jean Jennings Bartik - ENIAC Pioneer], ''Computer History Museum'', October 22, 2008}}
* [http://www.eniacprogrammers.org/ ENIAC Programmers documentary] 
* [http://purl.umn.edu/104288 Oral history from Bartik at the UNIVAC conference], [[Charles Babbage Institute]]
* [http://www.nwmissouri.edu/archives/computing/index.htm Jean Jennings Bartik Computing Museum at NWMSU]
* [https://www.youtube.com/watch?v=82EifuJ7O8U Bartik receives the Computer Pioneer Award]
* [http://archive.computerhistory.org/resources/text/Oral_History/Bartik_Jean/102658322.05.01.acc.pdf Oral history given by Bartik to the Computer History Museum in 2008]

{{DEFAULTSORT:Bartik, Jean}}
[[Category:1924 births]]
[[Category:2011 deaths]]
[[Category:People from Gentry County, Missouri]]
[[Category:Disease-related deaths in New York]]
[[Category:American computer programmers]]
[[Category:People in information technology]]
[[Category:Northwest Missouri State University alumni]]
[[Category:Women computer scientists]]
[[Category:Women mathematicians]]
[[Category:20th-century women scientists]]