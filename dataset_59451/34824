{{Infobox hurricane season
| Basin=Atl
| Year=1899
| Track=1899 Atlantic hurricane season summary map.png
| First storm formed=June 26, 1899
| Last storm dissipated=November 10, 1899
| Strongest storm name="[[1899 San Ciriaco hurricane|San Ciriaco]]"
| Strongest storm winds=130
| Strongest storm pressure=930
| Average wind speed=1
| Total depressions=10
| Total storms=10
| Total hurricanes=5
| Total intense=2
| Damagespre=At least
| Damages=21.3
| Fatalities=3,656
| five seasons=[[1897 Atlantic hurricane season|1897]], [[1898 Atlantic hurricane season|1898]], '''1899''', [[1900 Atlantic hurricane season|1900]], [[1901 Atlantic hurricane season|1901]]
}}
<!-- Created with subst: of [[Template:Hurricane season single]]. -->The '''1899 Atlantic hurricane season''' featured the longest-lasting [[tropical cyclone]] in the [[Atlantic hurricane season|Atlantic basin]] on record. There were nine tropical storms, of which five became hurricanes. Two of those strengthened into major hurricanes, which are Category&nbsp;3 or higher on the modern day [[Saffir–Simpson hurricane wind scale]]. The first system was initially observed in the northeastern [[Gulf of Mexico]] on June&nbsp;26. The tenth and final system dissipated near [[Bermuda]] on November&nbsp;10. These dates fall within the period with the most tropical cyclone activity in the Atlantic. In post-season analysis, two tropical cyclones that existed in October were added to [[HURDAT]] &ndash; the official Atlantic hurricane database. At one point during the season, September&nbsp;3 through the following day, a set of three tropical cyclones existed simultaneously.

The most significant storm of the season was Hurricane Three, nicknamed the [[1899 San Ciriaco hurricane|San Ciriaco hurricane]]. A post-season analysis of this storm indicated that it was the longest-lasting Atlantic tropical cyclone on record. The path impacted the [[Lesser Antilles]], [[Puerto Rico]], [[Dominican Republic]], [[the Bahamas]], [[Florida]], [[South Carolina]], [[North Carolina]], [[Virginia]], and the [[Azores]]. The San Ciriaco hurricane alone caused about $20&nbsp;million (1899&nbsp;[[United States dollar|USD]]) in damage and at least 3,656&nbsp;deaths. Another notable tropical cyclone, the [[1899 Carrabelle hurricane|Carrabelle hurricane]], brought extensive damage to Dominican Republic and [[Florida Panhandle]]. Losses in Florida reached about $1&nbsp;million. At least 9&nbsp;deaths were associated with the storm. Hurricane Nine in October brought flooding to [[Cuba]] and [[Jamaica]], as well as minor damage to South Carolina, North Carolina, and Virginia.
__TOC__

==Systems==
<center>
<timeline>
ImageSize = width:800 height:200
PlotArea  = top:10 bottom:80 right:20 left:20
Legend    = columns:3 left:30 top:58 columnwidth:270

AlignBars  = early
DateFormat = dd/mm/yyyy
Period     = from:01/06/1899 till:01/12/1899
TimeAxis   = orientation:horizontal
ScaleMinor = grid:black unit:month increment:1 start:01/06/1899

Colors =
  id:canvas value:gray(0.88)
  id:GP     value:red
  id:TD     value:rgb(0.38,0.73,1)  legend:Tropical_Depression_=_&lt;39_mph_(0–62_km/h)_(TD)
  id:TS     value:rgb(0,0.98,0.96)  legend:Tropical_Storm_=_39–73_mph_(63–117 km/h)_(TS)
  id:C1     value:rgb(1,1,0.80)     legend:Category_1_=_74–95_mph_(119–153_km/h)_(C1)
  id:C2     value:rgb(1,0.91,0.46)  legend:Category_2_=_96–110_mph_(154–177_km/h)_(C2)
  id:C3     value:rgb(1,0.76,0.25)  legend:Category_3_=_111–130_mph_(178–208_km/h)_(C3)
  id:C4     value:rgb(1,0.56,0.13)  legend:Category_4_=_131–156_mph_(209–251_km/h)_(C4)
  id:C5     value:rgb(1,0.38,0.38)  legend:Category_5_=_≥157_mph_(≥252_km/h)_(C5)

Backgroundcolors = canvas:canvas

BarData =
  barset:Hurricane
  bar:Month

PlotData=
  barset:Hurricane width:11 align:left fontsize:S shift:(4,-4) anchor:till
  from:26/06/1899 till:27/06/1899 color:TS text:"One (TS)"
  from:28/07/1899 till:02/08/1899 color:C2 text:"[[1899 Carrabelle hurricane|Two]] (C2)"
  from:03/08/1899 till:26/08/1899 color:C4 text:
  barset:break
  barset:skip
  barset:skip
  from:29/08/1899 till:04/09/1899 color:C1 text:"[[1899 San Ciriaco hurricane|Three]] (C4)"
  from:29/08/1899 till:08/09/1899 color:C2 text:"Four (C2)"
  from:03/09/1899 till:15/09/1899 color:C3 text:"Five (C3)"
  barset:break
  from:02/10/1899 till:08/10/1899 color:TS text:"Six (TS)"
  from:10/10/1899 till:14/10/1899 color:TS text:"Seven (TS)"
  from:15/10/1899 till:18/10/1899 color:TS text:"Eight (TS)"
  from:26/10/1899 till:31/10/1899 color:C2 text:"Nine (C2)"
  from:07/11/1899 till:10/11/1899 color:TS text:"Ten (TS)"

  bar:Month width:5 align:center fontsize:S shift:(0,-20) anchor:middle color:canvas
  from:01/06/1899 till:01/07/1899 text:June
  from:01/07/1899 till:01/08/1899 text:July
  from:01/08/1899 till:01/09/1899 text:August
  from:01/09/1899 till:01/10/1899 text:September
  from:01/10/1899 till:01/11/1899 text:October
  from:01/11/1899 till:01/12/1899 text:November

TextData =
   pos:(570,30)
   text:"(From the"
   pos:(617,30)
   text:"[[Saffir-Simpson Hurricane Scale]])"

</timeline>
</center>
{{Clear}}

===Tropical Storm One===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm One analysis 27 Jun 1899.png
|Track=1899 Atlantic tropical storm 1 track.png
|Formed=June 26
|Dissipated=June 27
|1-min winds=35
}}
Weather maps first indicated a tropical storm in the extreme northwestern Gulf of Mexico on June&nbsp;26.<ref name="Partagas"/> With initial sustained winds of 40&nbsp;mph (65&nbsp;km/h), the storm did not differentiate in intensity as it headed northwestward. At 0900&nbsp;UTC on June&nbsp;27, the system made landfall near the southwestern end of [[Galveston Island]], Texas at the same intensity. Three hours later, it weakened to a tropical depression and later dissipated over [[Southeast Texas]] at 1800&nbsp;UTC.{{Atlantic hurricane best track}} Heavy rainfall produced by the storm from [[Granbury, Texas|Granbury]] to [[Waco, Texas|Waco]] and toward the coast contributed to an ongoing flood event in the state.<ref>{{cite report|url=http://www.wpc.ncep.noaa.gov/research/txhur.pdf|title=Texas Hurricane History|author=David M. Roth|date=January 17, 2010|work=[[Weather Prediction Center]]|publisher=National Oceanic and Atmospheric Administration |pages=28, 29|accessdate=August 4, 2013|location=Camp Springs, Maryland}}</ref> According to Texas State Senator [[Asbury Bascom Davidson]], the [[Brazos River|Brazos]], [[Colorado River (Texas)|Colorado]], [[Guadalupe River (Texas)|Guadalupe]],<ref name="Partagas"/> [[Navasota River|Navasota]], and [[San Saba River]]s overflowed. An estimated {{convert|12000|sqmi|sqkm}} of land were inundated. In [[Hearne, Texas|Hearne]], water rose above every rain gauge. Thousands of people were left homeless. The flood caused $9&nbsp;million in damage and 284&nbsp;deaths.<ref>{{cite report|url=http://lifeonthebrazosriver.com/Floods.htm|title=Floods of the Brazos River in Texas|publisher=LifeOnTheBrazosRiver.com|accessdate=August 4, 2013}}</ref>
{{Clear}}

===Hurricane Two===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Two analysis 31 Jul 1899.png
|Track=1899 Atlantic hurricane 2 track.png
|Formed=July 28
|Dissipated=August 2
|1-min winds=85
|Pressure=979
}}
{{main article|1899 Carrabelle hurricane}}
A hurricane was first observed south of Dominican Republic on July&nbsp;28. Shortly thereafter, it made landfall in [[Azua Province]] with an intensity equivalent to a Category&nbsp;1 hurricane. Early on July&nbsp;29, the system weakened to a tropical storm, shortly before emerging into the southwestern Atlantic Ocean. It then moved west-northwestward and remained at relatively the same intensity over the next 24&nbsp;hours. The storm made landfall near [[Islamorada, Florida]] on July&nbsp;30. Crossing the [[Florida Keys]], it soon emerged into the [[Gulf of Mexico]]. The storm began to re-intensify on July&nbsp;31 and became a hurricane later that day. Early on August&nbsp;1, it peaked with winds of 100&nbsp;mph (155&nbsp;km/h), several hours before making landfall near [[Apalachicola, Florida]] at the same intensity. The storm quickly weakened inland and dissipated over southern [[Alabama]] on August&nbsp;2.{{Atlantic hurricane best track}}

In Dominican Republic, three large schooners were wrecked at [[Santo Domingo]]; only one crew member on the three vessels survived. "Great" damage was reported along coastal sections of the country, while a loss of telegraph service impacted most of interior areas. In Florida, damage in the city of Carrabelle was extensive, with no more than a score of "unimportant" houses remained. Losses in the city reached approximately $100,000. At least 57&nbsp;shipping vessels were destroyed; damage from these ships collectively totaled about $375,000. Additionally, 13&nbsp;lumber vessels were beached. Many boats at the harbor and the wharfs in [[Lanark Village|Lanark]] were wrecked. Large portions of stores and pavilions in the city were damaged. The towns of Curtis Mill and McIntyre were completely destroyed, while the resort city of [[St. Teresa, Florida|St. Teresa]] suffered significant damage. Seven deaths were confirmed in Florida.<ref name="Partagas">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1898-1900/1899_1.pdf|title=Year 1899|author=Jose F. Partagas|date=1996|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=[[National Oceanic and Atmospheric Administration]]|pages=39–53|accessdate=May 22, 2013|location=Miami, Florida|format=PDF}}</ref> Overall, losses reached at least $1&nbsp;million.<ref name="nyt1">{{cite news|url=https://query.nytimes.com/mem/archive-free/pdf?res=F70713FC3C5911738DDDAC0894D0405B8985F0D3|title=Destruction In Florida|date=August 5, 1899|newspaper=[[The New York Times]]|accessdate=June 3, 2013|location=River Junction, Florida}}</ref>
{{Clear}}

===Hurricane Three===
{{Infobox Hurricane Small
|Basin=Atl
|Image=San Ciriaco Hurricane SWA (1899).JPG
|Track=1899 San Ciriaco hurricane track.png
|Formed=August 3
|Dissipated=September 4
|1-min winds=130
|Pressure=930
}}
{{Main article|1899 San Ciriaco hurricane}}
The next storm, known as the San Ciriaco Hurricane,<ref>{{Cite conference  | first = Frank  | last = Mújica-Baker   | title = Huracanes y Tormentas que han afectadi a Puerto Rico | publisher = Estado Libre Asociado de Puerto Rico, Agencia Estatal para el manejo de Emergencias y Administracion de Desastres | url = http://www.gobierno.pr/NR/rdonlyres/49EA64D0-305B-4881-8B85-04B518004BD5/0/Ciclones_en_PR.pdf | pages = 10 | accessdate = August 30, 2010}}</ref> was first observed as a tropical storm to the southwest of Cape Verde on August&nbsp;3. It slowly strengthened while heading steadily west-northwestward across the Atlantic Ocean. By late on August&nbsp;5, the storm strengthened into a hurricane. During the following 48&nbsp;hours, it deepened further, reaching Category&nbsp;4 hurricane status before crossing the Leeward Islands on August&nbsp;7. Later that day, the storm attained its peak intensity with maximum sustained winds of 150&nbsp;mph (240&nbsp;km/h) and a minimum barometric pressure of {{convert|930|mbar|inHg|abbr=on}}. The storm weakened slightly before making landfall in [[Guayama, Puerto Rico]] with winds of 140&nbsp;mph (220&nbsp;km/h) on August&nbsp;8. Several hours later, it emerged into the southwestern Atlantic as a Category&nbsp;3 hurricane; it would remain at that intensity for over 9&nbsp;days. The system paralleled the north coast of [[Dominican Republic]] and then crossed the Bahamas, striking several islands, including [[Andros, Bahamas|Andros]] and [[Grand Bahama]]. After clearing the Bahamas, it began heading northward on August&nbsp;14, while centered east of Florida. Early on the following day, the storm re-curved northeastward and appeared to be heading out to sea. However, by August&nbsp;17, it turned back to the northwest. At 0100&nbsp;UTC on August&nbsp;18, the storm made landfall near [[Hatteras, North Carolina]] with 120&nbsp;mph winds.{{Atlantic hurricane best track}}

The storm weakened after moving inland and fell to Category&nbsp;1 intensity by 1200&nbsp;UTC on August&nbsp;18. Later that day, the storm re-emerged into the Atlantic Ocean. Now heading northeastward, it continued weakening, but maintained Category&nbsp;1 intensity. By late on August&nbsp;20, the storm curved eastward over the northwestern Atlantic. It also began losing tropical characteristics and transitioned into an extratropical cyclone at 0000&nbsp;UTC on August&nbsp;22, while located about 325 miles (525&nbsp;km) south of [[Sable Island]]. However, after four days, the system regenerated into a tropical storm while located about 695 miles (1,120&nbsp;km) west-southwest of [[Flores Island (Azores)|Flores Island]] in the [[Azores]] on August&nbsp;26. It moved slowly north-northwestward, until curving to the east on August&nbsp;29. Between August&nbsp;26 and September&nbsp;1, the storm did not differentiate in intensity, but began re-strengthening while turning southeastward on September&nbsp;2. Early on the following day, the storm again reached hurricane intensity. It curved northeastward and passed through the Azores on September&nbsp;3, shortly before transitioning into an extratropical cyclone.{{Atlantic hurricane best track}} The storm had the longest duration of an Atlantic hurricane on record, lasting for 31&nbsp;days, 28 of which it was tropical.<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/|title=NOAA Revisits Historic Hurricanes|work=Hurricane Research Division; Atlantic |publisher=Atlantic Oceanographic and Meteorological Laboratory|accessdate=August 4, 2013|location=Miami, Florida}}</ref>

In [[Guadeloupe]], the storm unroofed and flooded many houses. Communications were significantly disrupted in the interior portions of the island. Impact was severe in [[Montserrat]], with nearly every building destroyed and 100&nbsp;deaths reported. About 200&nbsp;small houses were destroyed on [[Saint Kitts]], with estates suffering considerable damage, while nearly all estates were destroyed on [[Saint Croix, U.S. Virgin Islands|Saint Croix]]. Eleven deaths were reported on the island.<ref name="Partagas"/> In Puerto Rico, the system brought strong winds and heavy rainfall, which caused extensive flooding. Approximately 250,000&nbsp;people were left without food and shelter. Additionally, telephone, telegraph, and electrical services were completely lost. Overall, damage totaled approximately $20&nbsp;million, with over half were losses inflicted on crops, particularly coffee. At the time, it was the costliest and worst tropical cyclone in Puerto Rico. It was officially estimated that the storm caused 3,369&nbsp;fatalities.<ref name="las">{{cite report|url=http://latinamericanstudies.org/puertorico/hurricane.pdf|title=The Hurricane of San Ciriaco: Disaster, Politics, Society in Puerto Rico, 1899&ndash;1901|author=Stuart B. Schwartz|date=1992|work=Latin American Studies|publisher=[[Duke University Press]]|accessdate=May 3, 2013|format=PDF|location=Durham, North Carolina}}</ref> In the Bahamas, strong winds and waves sank 50&nbsp;small crafts, most of them at [[Andros]]. Severe damage was reported in the capital city of [[Nassau, Bahamas|Nassau]], with over 100&nbsp;buildings destroyed and many damaged, including the Government House. A few houses were also destroyed in Bimini. The death toll in the Bahamas was at least 125.<ref name="Partagas"/> In North Carolina, storm surge and rough sea destroyed fishing piers and bridges, as well as sink about 10&nbsp;vessels. Because [[Hatteras Island]] was almost entirely inundated with {{convert|4|to|10|ft|m}} of water, a great proportion of homes on the island were damaged, with much destruction at Diamond City. There were at least 20&nbsp;deaths in the state of North Carolina.<ref name="nc">{{cite report|url=http://repository.wrclib.noaa.gov/cgi/viewcontent.cgi?article=1002&context=nws_tech_memos|title=Tropical cyclones aﬀecting North Carolina since 1586: An historical perspective|author=James E. Hudgins|date=April 2000|format=PDF|work=[[National Weather Service]]|publisher=National Oceanic and Atmospheric Administration|pages=21–22|accessdate=August 4, 2013|location=Springfield, Virginia|archiveurl=https://web.archive.org/web/20070311045226/http://repository.wrclib.noaa.gov/cgi/viewcontent.cgi?article=1002&context=nws_tech_memos|archivedate=March 11, 2007}}</ref> In the Azores, the storm also caused one fatality and significant damage on some islands.<ref name="Partagas2">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1898-1900/1899_2.pdf|title=Year 1899|author=Jose F. Partagas|date=1996|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|pages=59; 65–68|accessdate=May 22, 2013|location=Miami, Florida|format=PDF}}</ref>
{{Clear}}

===Hurricane Four===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Four analysis 4 Sept 1899.png
|Track=1899 Atlantic hurricane 4 track.png
|Formed=August 29
|Dissipated=September 5
|1-min winds=90
}}
Weather maps indicated a tropical storm just east of the Lesser Antilles beginning on August&nbsp;29.<ref name="Partagas2"/> The storm moved westward and strengthened into a hurricane early on August&nbsp;30. Several hours later, it entered the Caribbean Sea after passing near [[Antigua]] and Montserrat.{{Atlantic hurricane best track}} Impact was generally light in the Lesser Antilles. At [[San Juan, Puerto Rico]], sustained winds reached {{convert|48|mph|km/h|abbr=on}}.<ref name="Partagas2"/> The storm maintained winds of 80&nbsp;mph (130&nbsp;km/h) as it continued westward across the Caribbean Sea.{{Atlantic hurricane best track}} Vessels sailing from ports in Cuba and Hispaniola were advised to "take every precaution". After the storm curved northward late on September&nbsp;1, vessels from Hispaniola only were advised to take caution.<ref name="Partagas2"/>

Late on September&nbsp;1, the hurricane made landfall east of [[Jacmel]], [[Haiti]] with winds of 80&nbsp;mph (130&nbsp;km/h). By 1800&nbsp;UTC, it weakened to a tropical storm. The storm emerged into the Atlantic Ocean early on September&nbsp;2, after weakening further. While passing just east of the [[Turks and Caicos Islands]] early on September&nbsp;3, the storm re-strengthened and attained hurricane status again. Several hours later, it strengthened into a Category&nbsp;2 hurricane and peaked with winds of 105&nbsp;mph (165&nbsp;km/h). After weakening to a Category&nbsp;1 hurricane late on September&nbsp;4, the storm passed northwest of Bermuda.{{Atlantic hurricane best track}} Hurricane-force winds caused considerable damage on the island.<ref name="Partagas2"/> At 1200&nbsp;UTC on September&nbsp;5, the hurricane became extratropical.{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Five===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Five analysis 12 Sept 1899.png
|Track=1899 Atlantic hurricane 5 track.png
|Formed=September 3
|Dissipated=September 15
|1-min winds=105
|Pressure=939
}}
HURDAT initially indicates a tropical storm about {{convert|920|mi|km}} west-southwest of [[Brava, Cape Verde]] on September&nbsp;3. The storm moved west-northwestward and slowly intensified, reachig hurricane status late on September&nbsp;5. It continued to slowly strengthen, becoming a Category&nbsp;2 hurricane on September&nbsp;6. About 24&nbsp;hours later, the cyclone deepened into a Category&nbsp;3 hurricane while located near the [[Lesser Antilles]].{{Atlantic hurricane best track}}  On Saint Kitts, sustained winds reached {{convert|62|mph|km/h|abbr=on}}, while up to {{convert|3.13|in|mm}} of rainfall was reported. Many houses were destroyed on [[Anguilla]] and [[Barbuda]]. In the former, an estimated 200&nbsp;homes were demolished, leaving 800&nbsp;people homeless.<ref name="Partagas3">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1898-1900/1899_3.pdf|title=Year 1899|author=Jose F. Partagas|date=1996|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|pages=71–83|accessdate=May 22, 2013|location=Miami, Florida|format=PDF}}</ref> Early on September&nbsp;9, the storm reached maximum sustained winds of 120&nbsp;mph (195&nbsp;km/h). The storm maintained intensity as a Category&nbsp;3 hurricane and re-curved northward by September&nbsp;11.{{Atlantic hurricane best track}}

The hurricane turned northeastward on September&nbsp;12 and began to accelerate. Early on September&nbsp;13, it passed very close to Bermuda, with a minimum barometric pressure of {{convert|939|mbar|inHg|abbr=on}} observed on the island.{{Atlantic hurricane best track}} Cedar trees were uprooted, while fruit and ornamental trees were swept out to sea. Some houses were destroyed, while others were deroofed. Severe damage was also reported at the naval yard and colonial government buildings. At Her Majesty's Dockyard alone, damage was "at least five figures".<ref name="Partagas3"/> Early on September&nbsp;14, the storm weakened to a Category&nbsp;2 hurricane, then to a Category&nbsp;1 several hours later. Shortly after 0000&nbsp;UTC on September&nbsp;15, the hurricane struck the [[Avalon Peninsula]] of [[Newfoundland (island)|Newfoundland]] with winds of 85&nbsp;mph (140&nbsp;km/h). It soon became extratropical.{{Atlantic hurricane best track}} In Newfoundland, severe damage was reported at fishing premises.<ref name="Partagas3"/> The schooners ''Angler'', ''Daisy'', and ''Lily May'' either capsized or were driven ashore, resulting in 16&nbsp;deaths.<ref>{{cite news|url=https://query.nytimes.com/mem/archive-free/pdf?res=F40913FA3E5811738DDDA00994D1405B8985F0D3|title=Newfoundland Hurricane|date=September 19, 1899|newspaper=[[The New York Times]]|accessdate=August 4, 2013|location=St. John's, Newfoundland}}</ref>
{{Clear}}

===Tropical Storm Six===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Six analysis 05 Oct 1899.png
|Track=1899 Atlantic tropical storm 6 track.png
|Formed=October 2
|Dissipated=October 6
|1-min winds=50
|Pressure=
}}
A ship in the western Caribbean Sea reported a tropical storm on October&nbsp;2.<ref name="Partagas3"/> The storm moved north-northwestward and entered the Gulf of Mexico early on the following day. Late on October&nbsp;3, it peaked with maximum sustained winds of 60&nbsp;mph (95&nbsp;km/h). The storm re-curved eastward while situated over the northeastern Gulf of Mexico. At 0000&nbsp;UTC on October&nbsp;5, this system made landfall in modern-day [[Largo, Florida]] at the same intensity. Thereafter, the storm headed northeastward, until becoming extratropical early on October&nbsp;6, while located offshore [[Georgia (U.S. state)|Georgia]].{{Atlantic hurricane best track}}

Impact from this system was generally minor. Prior to landfall in Florida, the storm produced winds up to 40&nbsp;mph (65&nbsp;km/h) in [[Port Eads, Louisiana]]. The highest wind speed in Florida was {{convert|37|mph|km/h|abbr=on}} in [[Jupiter, Florida|Jupiter]]. There, the storm also dropped {{convert|4.94|in|mm}} of rain. The Jupiter area also reported rough seas, with the highest tides in 7&nbsp;years.<ref name="Partagas3"/> The storm wrecked two schooners &ndash; the ''John R. Anidia'' at [[Fernandina Beach, Florida|Fernandina Beach]] and the ''John H. Tingue'' at [[Cumberland Island]], Georgia.<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/history/|title=Chronological Listing of Tropical Cyclones affecting North Florida and Coastal Georgia 1565-1899|author=Al Sandrik|author2=Christopher W. Landsea|last-author-amp=yes|date=May 2003|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration |accessdate=August 4, 2013|location=Miami, Florida}}</ref> After becoming extratropical, the remnants of the storm brought wind gusts up to {{convert|56|mph|km/h|abbr=on}} to [[Cape Henry]], Virginia and [[Block Island]], [[Rhode Island]].<ref name="Partagas3"/>
{{Clear}}

===Tropical Storm Seven===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Seven analysis 11 Oct 1899.png
|Track=1899 Atlantic tropical storm 7 track.png
|Formed=October 10
|Dissipated=October 14
|1-min winds=40
|Pressure=
}}
Reports from a ship on October&nbsp;10 indicated a tropical storm with sustained winds of 45&nbsp;mph (75&nbsp;km/h) and a minimum barometric pressure of {{convert|1008|mbar|inHg|abbr=on}}. Located well southwest of Cape Verde, the storm moved northwestward without differentiating in intensity. It was lasted noted on October&nbsp;14, while situated at 21.5°N, 43.5°W.{{Atlantic hurricane best track}}<ref>{{cite report|url=http://www.aoml.noaa.gov/hrd/hurdat/excelfiles_centerfix/1899/1899_7.XLS|title=1899 Storm 7|work=[[Atlantic Oceanographic and Meteorological Laboratory]]|publisher=National Oceanic and Atmospheric Administration|accessdate=May 23, 2013|format=XLS|location=Virginia Key, Florida}}</ref>
{{Clear}}

===Tropical Storm Eight===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Eight analysis 17 Oct 1899.png
|Track=1899 Atlantic hurricane 8 track.png
|Formed=October 15
|Dissipated=October 18
|1-min winds=40
}}
A tropical depression developed in the central Bahamas on October&nbsp;15. The depression moved east-northeastward strengthened into a tropical storm by the following day. Later on October&nbsp;16, the storm peaked with winds of 45&nbsp;mph (75&nbsp;km/h). It re-curved northwestward and slowly began to weaken. Early on October&nbsp;18, the system fell to tropical depression intensity. Several hours later, the cyclone dissipated while located about 195&nbsp;miles (315&nbsp;km) east-southeast of [[Virginia Beach, Virginia]].{{Atlantic hurricane best track}}
{{Clear}}

===Hurricane Nine===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Hurricane Nine analysis 31 Oct 1899.png
|Track=1899 Atlantic hurricane 9 track.png
|Formed=October 26
|Dissipated=October 31
|1-min winds=95
|Pressure=955
}}
An area of disturbed weather developed into a tropical storm while located south-southwest of Jamaica on October&nbsp;26.<ref name="Partagas3"/> The system moved slowly north-northwestward and gradually strengthened, reaching hurricane status on October&nbsp;28. By early on the following day, it made landfall on the southern coast of [[Sancti Spíritus Province]], Cuba. Briefly weakening to a tropical storm, the system re-intensified into a hurricane after reaching the Atlantic Ocean late on October&nbsp;29. Moving toward the Bahamas, the storm became a Category&nbsp;2 hurricane on October&nbsp;30. Around that time, it struck [[Grand Bahama]] island. After peaking with winds of 110&nbsp;mph (175&nbsp;km/h), the system accelerated north-northwestward and made landfall near [[Myrtle Beach, South Carolina]] on October&nbsp;31. It quickly weakened and became extratropical over Virginia later that day.{{Atlantic hurricane best track}}

In the city of [[Black River, Jamaica]], rough seas caused significant damage to the marine industry and washed out crops. There were "many dead" in Jamaica, though the actual number of fatalities is unknown. Damage from the storm in Cuba was reported in the [[Sancti Spíritus Province|Sancti Spíritus]] and [[Santa Clara Province]]s. Due to the threat of the [[Zaza Reservoir|Zaza River]] overflowing, residents were forced to evacuate. Strong winds and flooding destroyed several houses and severely damaged a number of others.<ref name="Partagas3"/> At [[Wrightsville Beach, North Carolina]], tides were reported as {{convert|8|ft|m}} above normal. Water came over the wharves in [[Wilmington, North Carolina|Wilmington]] and inundated some streets; there was also flooding in [[New Bern, North Carolina|New Bern]], [[Morehead City, North Carolina|Morehead City]], and [[Beaufort, North Carolina|Beaufort]]. One steamer was wrecked on the coast and 10&nbsp;smaller vessels were driven ashore. One fatality was reported and damage was estimated at roughly $200,000.<ref name="nc"/>
{{Clear}}

===Tropical Storm Ten===
{{Infobox Hurricane Small
|Basin=Atl
|Image=Tropical Storm Ten analysis 08 Nov 1899.png
|Track=1899 Atlantic tropical storm 9 track.png
|Formed=November 7
|Dissipated=November 10
|1-min winds=55
}}
A ship observed a tropical storm north of [[Panama]] on November&nbsp;7. The storm strengthened and headed northeastward across the central Caribbean Sea. It curved northward on November&nbsp;8, around the time of peaking with winds of 65&nbsp;mph (100&nbsp;km/h). Later that day, the storm made landfall in [[Saint Thomas Parish, Jamaica]] at the same intensity. Thereafter, the system weakened and struck extreme western [[Santiago de Cuba Province]], Cuba with winds of 50&nbsp;mph (85&nbsp;km/h) on November&nbsp;9. It continued to weaken while crossing the island and emerged into the southwestern Atlantic Ocean later that day. The storm curved northeastward and passed through the Bahamas on November&nbsp;10. It then weakened to a tropical depression, several hours before dissipating about {{convert|385|mi|km}} southeast of Bermuda.{{Atlantic hurricane best track}}

The storm brought strong winds and heavy rainfall to Jamaica and Cuba. Significant damage was reported at [[Port Antonio]], Jamaica, especially the property and agriculture of the [[United Fruit Company]]. Several districts of Saint Thomas Parish became isolated and the town of [[Morant Bay]] was "shattered". In Cuba, rainfall peaked at {{convert|5.7|in|mm}} in the city of [[Santiago de Cuba]]. Damage to buildings and crops were reported in the region. Four fatalities occurred when a tree fell on their farmhouse in [[Manzanillo, Cuba|Manzanillo]], [[Granma Province]].<ref name="Partagas4">{{cite report|url=http://www.aoml.noaa.gov/hrd/Landsea/Partagas/1898-1900/1899_4.pdf|title=Year 1899|author=Jose F. Partagas|date=1996|work=Atlantic Oceanographic and Meteorological Laboratory|publisher=National Oceanic and Atmospheric Administration|pages=90–93|accessdate=August 4, 2013|location=Miami, Florida|format=PDF}}</ref>
{{Clear}}

==See also==
{{Portal|Tropical cyclones}}
* [[List of Atlantic hurricane seasons]]
* [[List of tropical cyclones]]

==References==
{{Reflist|2}}

==External links==
* [http://www.aoml.noaa.gov/general/lib/lib1/nhclib/mwreviews/1899.pdf Monthly Weather Review]

{{TC Decades|Year=1890|basin=Atlantic|type=hurricane}}

{{Good article}}

{{DEFAULTSORT:1899 Atlantic Hurricane Season}}
[[Category:1890–1899 Atlantic hurricane seasons| ]]
[[Category:Articles which contain graphical timelines]]