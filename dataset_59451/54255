{{Userspace draft|source=ArticleWizard|date=March 2017}} 
{{for|his contemporary, the merchant of Goolwa, South Australia|Thomas Goode (merchant)}}
'''Thomas Good''' was a merchant of Adelaide, South Australia, a founder of the wholesale drapery business of '''Good, Toms & Co.'''

==History==
Thomas Good (c. 1822 – 21 January 1889) of [[Birmingham]] left England for South Australia in the ''John Mitchell'' with (later Sir) Charles Goode ( – 5 February 1922), arriving in Adelaide in April 1849.<ref name=death>{{cite news |url=http://nla.gov.au/nla.news-article35279546 |title=Death of Sir Charles Goode |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=6 February 1922 |accessdate=8 February 2013 |page=7 |publisher=National Library of Australia}}</ref> Together they travelled the State by horse and cart hawking softgoods, and were successful enough to start a small drapery business in Kermode Street, [[North Adelaide, South Australia|North Adelaide]]. They each married a sister of the other.

In 1850 John Good & Co. began trading as drapers in Rundle Street, Adelaide, opposite Berry's China Warehouse.<ref>{{cite news |url=http://nla.gov.au/nla.news-article207117486 |title=Advertising |newspaper=[[Adelaide Times]] |volume=II, |issue=204 |location=South Australia |date=20 July 1850 |accessdate=1 April 2017 |page=5 |via=National Library of Australia}}</ref>

In January 1853 he opened a general store<ref>{{cite news |url=http://nla.gov.au/nla.news-article158092634 |title=Advertising |newspaper=[[Adelaide Observer]] |volume=XI, |issue=498 |location=South Australia |date=8 January 1853 |accessdate=1 April 2017 |page=1 |via=National Library of Australia}}</ref> opposite Low's Inn, [[Mount Barker, South Australia|Mount Barker]], followed by a grain store<ref>{{cite news |url=http://nla.gov.au/nla.news-article147846160 |title=Early Mount Barker |newspaper=[[The Mount Barker Courier And Onkaparinga And Gumeracha Advertiser]] |volume=49, |issue=2575 |location=South Australia |date=11 April 1930 |accessdate=1 April 2017 |page=1 |via=National Library of Australia}}</ref> which in 1864 he sold to William Barker, previously a partner of [[Sidney George Wilcox]]'s brothers Joseph and Emery in [[Gawler, South Australia|Gawler]].

[[File:Good, Toms & Co.jpg|thumb|Good, Toms & Co. warehouse, Stephens Place]]
In 1872 Good and Samuel Toms founded the wholesale firm of Good, Toms & Co.<ref>{{cite news |url=http://nla.gov.au/nla.news-article47056187 |title=Death of Mr. Thomas Good |newspaper=[[South Australian Register]] |location=Adelaide |date=22 January 1889 |accessdate=10 February 2013 |page=5 |publisher=National Library of Australia}}</ref> on King William Street, later office on Wyatt Street and a warehouse at 22 Stephens Place.
The business ceased trading in the early 1930s.<ref>{{cite news |url=http://nla.gov.au/nla.news-article58843902 |title=Land and Buildings. |newspaper=[[The Mail (Adelaide)]] |volume=20, |issue=989 |location=South Australia |date=9 May 1931 |accessdate=1 April 2017 |page=23 |via=National Library of Australia}}</ref>
In 1932 the warehouse was purchased by [[Charles Birks & Co]] and around 1934 incorporated into their adjacent retail establishment.<ref>{{cite web|url=http://collections.slsa.sa.gov.au/resource/B+2489|title=Good, Toms & Co. warehouse|publisher=State Library of South Australia|accessdate=1 April 2017}}</ref>

Good's business partner '''Samuel Toms''' (c. 1842 – 27 January 1907) may have been educated at [[J. L. Young]]'s [[Adelaide Educational Institution]].<ref>Though appearing in reference cited, his name is not mentioned in reference to any prizegiving or reunion, nor in Diana Chessell's excellent ''Adelaide's Dissenting Headmaster'', Wakefield Press 2014 ISBN 978 1 74305 240 2</ref> He worked at [[Goode Brothers]]' warehouse before joining with Thomas Good as Good, Toms & Co. Three of his sons were involved in the business. Toms was closely associated with the (Anglican) [[Holy Trinity Church, Adelaide|Trinity Church]] and was a keen cricketer, serving as umpire at many important games held at the [[Adelaide Oval]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article56965522 |title=Death of Mr. S. Toms |newspaper=[[The Register (Adelaide)]] |volume=LXXII, |issue=18,785 |location=South Australia |date=28 January 1907 |accessdate=1 April 2017 |page=5 |via=National Library of Australia}}</ref>
A third partner was William Kent, who managed the London office.

==Family==
Thomas Good (c. 1822 – 21 January 1889) married Mary Ann Goode (c. 1822 – 21 July 1895) in 1850. She was a sister of emigrants [[Charles H. Goode]], Matthew Goode (of [[Matthew Goode and Co.]]), Samuel Goode, jun., and Elizabeth Ann Goode.
*Emily Good (1851–1933) married Cornelius Proud ( –1905) in 1882
*Samuel Good (1856 – 31 January 1912) of Good, Toms, & Co.
*Mary Good (1858–1860)
*Annie Good (1859–1942) married David Williams ( – ) in 1884
*Elizabeth "Bessie" Good (1861–1921) married John Francis Hummel ( –1925) in 1884
*Charles Thomas Good (1863–1926) married Helena Russell Goode ( –1953) in 1890. He was an architect, designed the Grenfell Street premises of [[Goode, Durrant and Co.]])<ref>{{cite news |url=http://nla.gov.au/nla.news-article43913362 |title=Obituary |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=Adelaide |date=3 March 1926 |accessdate=8 February 2013 |page=19 |publisher=National Library of Australia}}</ref>
*Dr. J(oseph) Ernest Good (6 December 1867 – 6 December 1935) married Agnes Minnie Williams ( –1954), served in several notable British hospitals, returned to practise in [[Prospect, South Australia|Prospect]].<ref>{{cite news |url=http://nla.gov.au/nla.news-article74152930 |title=Sudden Death Of Dr. J. E. Good On Birthday |newspaper=[[The Advertiser (Adelaide)]] |location=South Australia |date=7 December 1935 |accessdate=1 April 2017 |page=21 |via=National Library of Australia}}</ref>
:*Gwynnyth Fay Good (1899– ) married [[Keith Angas|(John) Keith Angas]] (1900–1977) in 1924
:*Phyllis Gypsy Good (1899–1947) married Cavendish Lister "Pat" Colley (1898–1982), a granddaughter of [[R. B. Colley]], in 1923
*Miss L. Good ( – )<ref>{{cite news |url=http://nla.gov.au/nla.news-article34516888 |title=The Late Mrs. Thomas Good |newspaper=[[The Advertiser (Adelaide)|The Advertiser]] |location=South Australia |date=22 July 1895 |accessdate=1 April 2017 |page=7 |via=National Library of Australia}}</ref>
Good's sister Mary Harriet Good (c. 1830 – 18 August 1889) married his partner (later Sir) Charles Goode on 6 August 1856.<ref>{{cite news |url=http://nla.gov.au/nla.news-article49757977 |title=Family Notices. |newspaper=[[South Australian Register]] |location=Adelaide |date=7 August 1856 |accessdate=9 February 2013 |page=2 |publisher=National Library of Australia}}</ref> She was an invalid for much of her adult life; they had no children.

==Other Adelaide softgoods wholesalers==
*[[D. & W. Murray Limited]]
*[[G. & R. Wills & Co.]]
*[[Matthew Goode & Co]]
*[[Charles Henry Goode]] 
:(these two operated for 30 years as Goode Brothers)

==See also==
*[[Thomas Goode (merchant)|Thomas Goode]] (1816–1882), merchant of Goolwa
*[[Thomas Goode (pastoralist)|Thomas Goode]] (c. 1834–1926), pastoralist of [[Canowie Station]]

== References ==
{{Reflist}}
{{DEFAULTSORT:Good, Thomas}}
[[Category:Australian businesspeople]]
[[Category:Australian company founders]]
[[Category:1822 births]]
[[Category:1889 deaths]]


[[Category:Articles created via the Article Wizard]]