{{Infobox journal
| title = Evolution and Human Behavior
| cover = [[File:Evolution and Human Behavior cover.gif|image:Evolution_and_Human_Behavior_cover.gif]]
| discipline = [[Evolutionary psychology]]
| editor = S. Gaulin, M. G. Haselton, R. Kurzban, [[Ruth Mace|R. Mace]]
| publisher = [[Elsevier]]
| country =
| history = 1980-present
| frequency = Bimonthly
| impact = 3.130
| impact-year = 2014
| website = http://www.ehbonline.org/
| link1 = http://www.ehbonline.org/current
| link1-name = Online access
| link2 = http://www.ehbonline.org/issues
| link2-name = Online archive
| ISSN = 1090-5138
| CODEN = EHIBEF
| LCCN = 97648255
| OCLC = 35307676
}}
'''''Evolution and Human Behavior''''' is a [[Peer review|peer-reviewed]] [[academic journal]] covering research in which [[evolution]]ary perspectives are brought to bear on the study of [[human behavior]]. It is primarily a [[scientific journal]], but articles from [[scholarly method|scholar]]s in the [[humanities]] are also published. Papers reporting on [[theory|theoretical]] and [[empirical]] work on other [[species]] may be included if their relevance to the human animal is apparent. The journal is published by [[Elsevier]] on behalf of the [[Human Behavior and Evolution Society]].

==See also==
*[[Dual inheritance theory]]
*[[Evolutionary developmental psychology]]
*[[Evolutionary psychology]]
*[[Human behavioral ecology]]

==External links==
*{{Official website|http://journals.elsevierhealth.com/periodicals/ens}}

[[Category:Evolutionary psychology journals]]
[[Category:Elsevier academic journals]]
[[Category:Human Behavior and Evolution Society]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1980]]
[[Category:English-language journals]]
[[Category:Ethology journals]]
[[Category:Evolutionary biology journals]]
[[Category:Academic journals associated with learned and professional societies of the United States]]