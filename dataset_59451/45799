{{About|the artistic movement|the album by The White Stripes|De Stijl (album)}}
{{italic title}}
[[File:Rietveld chair 1.JPG|thumb|right|200px|''[[Red and Blue Chair]]'' designed by [[Gerrit Rietveld]] in 1917.]]
'''''De Stijl''''' ({{IPAc-en|d|ə|_|ˈ|s|t|aɪ|l}}; {{IPA-nl|də ˈstɛil}}), [[Dutch language|Dutch]] for "The Style", also known as '''neoplasticism''', was a Dutch [[art]]istic movement founded in 1917 in Amsterdam.  The De Stijl consisted of artists and architects <ref>{{cite book|last=Linduff|first=David G. Wilkins, Bernard Schultz, Katheryn M.|title=Art past, art present|date=1994|publisher=Prentice Hall|location=Englewood Cliffs, N.J.|isbn=0-13-062084-X|pages=523|edition=2nd}}</ref>  In a narrower sense, the term ''De Stijl'' is used to refer to a body of work from 1917 to 1931 founded in the [[Netherlands]].<ref name="tate">{{cite web |url=http://www.tate.org.uk/learn/online-resources/glossary/d/de-stijl |title=De Stijl |accessdate=2006-07-31 |work=Tate Glossary |publisher=The Tate}}</ref><ref name="archdic">{{cite book |last=Curl |first=James Stevens |title=A Dictionary of Architecture and Landscape Architecture |year=2006 |type=Paperback |edition=Second |publisher=Oxford University Press |isbn=0-19-860678-8}}</ref> Proponents of De Stijl advocated pure [[abstract art|abstraction]] and universality by a reduction to the essentials of [[Art#Forms, genres, mediums, and styles|form]] and [[colour]]; they simplified visual compositions to vertical and horizontal, using only [[black]], [[white]] and [[primary color]]s.

''De Stijl'' is also the name of a journal that was published by the Dutch painter, designer, writer, and critic [[Theo van Doesburg]] (1883–1931) that served to propagate the group's theories. Next to van Doesburg, the group's principal members were the painters [[Piet Mondrian]] (1872–1944), [[Vilmos Huszár]] (1884–1960), and [[Bart van der Leck]] (1876–1958), and the architects [[Gerrit Rietveld]] (1888–1964), [[Robert van 't Hoff]] (1887–1979), and [[J.&nbsp;J.&nbsp;P. Oud]] (1890–1963). The artistic philosophy that formed a basis for the group's work is known as ''neoplasticism''—the new plastic art (or ''Nieuwe Beelding'' in Dutch).

== Principles and influences ==

Mondrian sets forth the delimitations of neoplasticism in his essay "Neo-Plasticism in Pictorial Art". He writes, "this new plastic idea will ignore the particulars of appearance, that is to say, natural form and colour. On the contrary, it should find its expression in the abstraction of form and colour, that is to say, in the straight line and the clearly defined primary colour". With these constraints, his art allows only primary colours and non-colours, only squares and rectangles, only straight and horizontal or vertical lines.<ref>[http://www.tate.org.uk/learn/online-resources/glossary/n/neo-plasticism Tate Glossary: Neo-Plasticism ]</ref> The De Stijl movement posited the fundamental principle of the geometry of the straight line, the square, and the rectangle, combined with a strong asymmetricality; the predominant use of pure primary colors with black and white; and the relationship between positive and negative elements in an arrangement of non-objective forms and lines".<ref>[http://www.guggenheim.org/new-york/collections/collection-online/movements/195212/description/ The [[Solomon R. Guggenheim Museum|Guggenheim]] Collection Online: De Stijl]</ref>

The name De Stijl is supposedly derived from [[Gottfried Semper]]'s ''Der Stil in den technischen und tektonischen Künsten oder Praktische Ästhetik'' (1861–3), which Curl<ref name="archdic"/> suggests was mistakenly believed to advocate [[materialism]] and [[Functionalism (architecture)|functionalism]]. The "plastic vision" of De Stijl artists, also called Neo-Plasticism, saw itself as reaching beyond the changing appearance of natural things to bring an audience into intimate contact with an immutable core of reality, a reality that was not so much a visible fact as an underlying spiritual vision.<ref>{{cite journal|last1=Denker|first1=Susan A.|title=De Stijl: 1917–1931, Visions of Utopia|journal=Art Journal|date=September 1982|volume=42|issue=3|pages=242–246|doi=10.1080/00043249.1982.10792803|url=http://www.tandfonline.com/doi/abs/10.1080/00043249.1982.10792803}}</ref> In general, De Stijl proposed ultimate simplicity and abstraction, both in architecture and painting, by using only straight horizontal and vertical lines and rectangular forms. Furthermore, their formal vocabulary was limited to the primary colours, [[red]], [[yellow]], and [[blue]], and the three primary values, [[black]], [[white]], and [[grey]]. The works avoided symmetry and attained aesthetic balance by the use of opposition. This element of the movement embodies the second meaning of ''stijl'': "a post, jamb or support"; this is best exemplified by the construction of crossing joints, most commonly seen in [[carpentry]].

In many of the group's three-dimensional works, vertical and horizontal lines are positioned in layers or planes that do not intersect, thereby allowing each element to exist independently and unobstructed by other elements. This feature can be found in the [[Rietveld Schröder House]] and the [[Red and Blue Chair]].

De Stijl was influenced by [[Cubist]] painting as well as by the mysticism and the ideas about "ideal" geometric forms (such as the "perfect straight line") in the [[neoplatonic]] philosophy of mathematician [[M.&nbsp;H.&nbsp;J. Schoenmaekers]]. The De Stijl movement was also influenced by [[Neopositivism]].<ref>{{cite book|last=Linduff|first=David G. Wilkins, Bernard Schultz, Katheryn M.|title=Art past, art present|date=1994|publisher=Prentice Hall|location=Englewood Cliffs, N.J.|isbn=0-13-062084-X|page=523|edition=2nd}}</ref>  The works of De Stijl would influence the [[Bauhaus]] style and the [[International Style (architecture)|international style]] of architecture as well as clothing and interior design. However, it did not follow the general guidelines of an "-ism" (e.g., [[Cubism]], [[Futurism]], [[Surrealism]]), nor did it adhere to the principles of art schools like the Bauhaus; it was a collective project, a joint enterprise.

In [[music]], De Stijl was an influence only on the work of composer [[Jakob van Domselaer]], a close friend of Mondrian. Between 1913 and 1916, he composed his ''Proeven van Stijlkunst'' ("Experiments in Artistic Style"), inspired mainly by Mondrian's paintings. This [[minimalist]]ic—and, at the time, revolutionary—music defined "horizontal" and "vertical" musical elements and aimed at balancing those two principles. Van Domselaer was relatively unknown in his lifetime, and did not play a significant role within the De Stijl group.

== History ==
{{Unreferenced section|date=July 2008}}
[[Image:Theo van Doesburg Composition VII (the three graces).jpg|thumb|right|[[Theo van Doesburg]], neoplasticism: ''Composition VII (the three graces)'' 1917.]]

=== Early history ===
[[File:Destijl anthologiebonset.jpg|thumb|right|Page from ''De Stijl'' magazine.]]
From the flurry of new art movements that followed the [[Impressionist]] revolutionary new perception of painting, [[Cubism]] arose in the early 20th century as an important and influential new direction. In the [[Netherlands]], too, there was interest in this "new art".

However, because the Netherlands remained neutral in [[World War&nbsp;I]], Dutch artists were not able to leave the country after 1914 and were thus effectively isolated from the international art world—and in particular, from [[Paris]], which was its centre then.

During that period, painter [[Theo van Doesburg]] started looking for other artists to set up a [[Literary magazine|journal]] and start an art movement. Van Doesburg was also a writer, poet, and critic, who had been more successful writing about art than working as an independent artist.<ref>{{cite book|last1=Theo van Doesburg|editor1-last=Translated by Janet Seligman|editor2-last=Introd. by Hans M. Wingler|editor3-last=Postscript by H.L.C. Jaffé|title=Grundbegriffe der Neuen Gestaltenden Kunst (Grondbeginselen der Nieuwe beeldende Kunst [Principles of Neo-Plastic Art])|date=1918|publisher=Lund Humphries (1968)|location=London, UK|isbn=0853311048|language=German, Dutch, English}}</ref> Quite adept at making new contacts due to his flamboyant personality and outgoing nature, he had many useful connections in the art world.

=== Founding of ''De Stijl'' ===
<!-- Deleted image removed: [[File:Mondrian CompRYB.jpg|thumb|right|''Composition with Yellow, Blue, and Red'', 1937–42, [[Piet Mondrian]]. Oil on canvas; 72.5 x 69 cm. London, [[Tate Gallery]].]] -->
[[File:Piet Mondrian, 1911, Gray Tree (De grijze boom), oil on canvas, 79.7 x 109.1 cm, Gemeentemuseum Den Haag, Netherlands.jpg|thumb|left|[[Piet Mondrian]], ''[[Gray Tree]], 1912]]
Around 1915, [[Theo van Doesburg|Van Doesburg]] started meeting the artists who would eventually become the founders of the journal. He first met [[Piet Mondrian]] at an exhibition in [[Stedelijk Museum Amsterdam]]. Mondrian, who had moved to Paris in 1912 (and there, changed his name from "Mondriaan"), had been visiting the Netherlands when war broke out. He could not return to Paris, and was staying in the artists' community of [[Laren, North Holland|Laren]], where he met [[Bart van der Leck]] and regularly saw [[M.&nbsp;H.&nbsp;J. Schoenmaekers]]. In 1915, Schoenmaekers published ''Het nieuwe wereldbeeld'' ("The New Image of the World"), followed in 1916 by ''Beginselen der beeldende wiskunde'' ("Principles of Plastic Mathematics"). These two publications would greatly influence Mondrian and other members of De Stijl.

Van Doesburg also knew [[J.&nbsp;J.&nbsp;P. Oud]] and the Hungarian artist [[Vilmos Huszár]]. In 1917 the cooperation of these artists, together with the poet [[Anthony Kok]], resulted in the founding of De Stijl. The young architect [[Gerrit Rietveld]] joined the group in 1918.  At its height De Stijl had 100 members and the journal had a circulation of 300.<ref>{{cite web|last1=Dujardin|first1=Alain|last2=Quirindongo|first2=Jop|title=This 100-Year-Old Dutch Movement Shaped Web Design Today|url=https://backchannel.com/100-years-of-clean-lines-how-de-stijl-is-influencing-digital-design-4397f1a72e37?mbid=synd_digg&curator=MediaREDEF#.kphtjs1er|website=[[Backchannel (blog)]]|accessdate=29 January 2017}}</ref>

During those first few years, the group was still relatively homogeneous, although Van der Leck left in 1918 due to artistic differences of opinion. [[Manifesto]]s were being published, signed by all members. The social and economic circumstances of the time formed an important source of inspiration for their theories, and their ideas about architecture were heavily influenced by [[Hendrik Petrus Berlage|Berlage]] and [[Frank Lloyd Wright]].

The name ''Nieuwe Beelding'' was a term first coined in 1917 by Mondrian, who wrote a series of twelve articles called ''De Nieuwe Beelding in de schilderkunst'' ("Neo-Plasticism in Painting") that were published in the journal ''De Stijl''. In 1920 he published a book titled ''Le Neo-Plasticisme''.

=== After 1920 ===
Around 1921, the group's character started to change. From the time of van Doesburg's association with [[Bauhaus]], other influences started playing a role. These influences were mainly [[Malevich]] and Russian [[Constructivism (art)|Constructivism]], to which not all members agreed. In 1924 Mondrian broke with the group after van Doesburg proposed the theory of elementarism, suggesting that a diagonal line is more vital than horizontal and vertical ones. In addition, the De Stijl group acquired many new "members". [[Dada]]ist influences, such as [[I.&nbsp;K. Bonset]]'s poetry and [[Aldo Camini]]'s "antiphilosophy" generated controversy as well. Only after Van Doesburg's death was it revealed that Bonset and Camini were two of his pseudonyms.

=== After van Doesburg's death ===
Theo van Doesburg died in [[Davos]], Switzerland, in 1931. His wife, Nelly, administered his estate.

Because of van Doesburg's pivotal role within De Stijl, the group did not survive. Individual members remained in contact, but De Stijl could not exist without a strong central character. Thus, it may be wrong to think of De Stijl as a close-knit group of artists. The members knew each other, but most communication took place by letter. For example, Mondrian and Rietveld never met in person.

[[File:RietveldSchroederhuis.jpg|thumb|right|The [[Rietveld Schröder House]]—the only building realised completely according to the principles of De Stijl]]
Many, though not all, artists did stay true to the movement's basic ideas, even after 1931. Rietveld, for instance, continued designing furniture according to De Stijl principles, while Mondrian continued working in the style he had initiated around 1920. Van der Leck, on the other hand, went back to figurative compositions after his departure from the group.

=== Influence on architecture ===
[[File:Aubette Ciné-dancing 01.jpg|thumb|[[Aubette (building)|Aubette dance hall]], 1929]]
The De Stijl influence on architecture remained considerable long after its inception; [[Mies van der Rohe]] was among the most important proponents of its ideas. Between 1923 and 1924, [[Gerrit Rietveld|Rietveld]] designed the [[Rietveld Schröder House]], the only building to have been created completely according to De Stijl principles. Examples of Stijl-influenced works by [[J.J.P. Oud]] can be found in [[Rotterdam]] (''[[Café De Unie]]'') and [[Hoek van Holland]]. Other examples include the [[Eames House]] by Charles and Ray Eames, and the interior decoration for the [[Aubette (building)|Aubette dance hall]] in Strasbourg, designed by Sophie Taeuber-Arp, Jean Arp and van Doesburg.

=== Present day ===
Works by De Stijl members are scattered all over the world, but De Stijl-themed exhibitions are organised regularly. Museums with large De Stijl collections include the [[Gemeentemuseum Den Haag|Gemeentemuseum]] in [[The Hague]] (which owns the world's most extensive, although not exclusively De Stijl-related, Mondrian collection) and [[Amsterdam]]'s [[Stedelijk Museum]], where many works by Rietveld and Van Doesburg are on display. The [[{{Not a typo|Centraal}} Museum]] of [[Utrecht]] has the largest Rietveld collection worldwide; it also owns the [[Rietveld Schröder House]], Rietveld's adjacent "show house", and the Rietveld Schröder Archives.

The movement inspired the design aesthetics of [[Rumyantsevo (Moscow Metro)|Rumyantsevo]] and [[Salaryevo]] stations of [[Moscow Metro]] opened in 2016.<ref>{{cite web|url=http://www.dw.com/en/marble-and-mondrian-a-tour-of-moscows-metro/g-19098971|title=Marble and Mondrian: a tour of Moscow metro|publisher=DW|accessdate=2017-04-06}}</ref><ref>{{cite web|url=http://www.artlebedev.com/metro/rumyantsevo/|title=Type design for Rumyantsevo Moscow Metro station|accessdate=2017-04-06}}</ref>

== Neoplasticists ==
<!--need a ref for each artist included here-->
* [[Ilya Bolotowsky]] (1907–1981), painter and sculptor<ref>{{cite web|title=Ilya Bolotowsky|url=http://www.sullivangoss.com/ilya_Bolotowsky/|publisher=Sullivan Goss|accessdate=24 September 2015}}</ref>
* [[Burgoyne Diller]] (1906–1965), painter<ref>{{cite web|title=Burgoyne Diller|url=http://www.sullivangoss.com/burgoyne_Diller/|publisher=Sullivan Goss|accessdate=24 September 2015}}</ref>
* [[Theo van Doesburg]] (1883–1931), painter, designer, and writer; co-founder of De Stijl movement; published ''De Stijl'', 1917–1931<ref name="tate"/>
* [[Cornelis van Eesteren]] (1897–1981), architect<ref name="the-artists"/>
* [[Jean Gorin]] (1899–1981), painter, sculptor<ref name="the-artists">{{cite web |title=de Stijl |url=http://the-artists.org/artistsbymovement/de-stijl |publisher=the-artists.org |accessdate=24 September 2015}}</ref>
* [[Robert van 't Hoff]] (1887–1979), architect<ref>{{cite web |title=Robert Van 'T Hoff in The Kröller-Müller Museum |url=http://en.nai.nl/collection/view_the_collection/item/_rp_kolom2-1_elementId/1_669536 |publisher=Het Nieuwe Instituut |accessdate=24 September 2015}}</ref>
* [[Vilmos Huszár]] (1884–1960), painter<ref>{{cite web |title=Vilmos huszar De Stijl |url=http://www.moma.org/collection/works/7893?locale=en |publisher=MoMA |accessdate=24 September 2015}}</ref>
* [[Frederick John Kiesler]] (1890-1965), architect, theater designer, artist, sculptor<ref>{{cite web |title=AD Classics: Endless House / Friedrick Kiesler |url=http://www.archdaily.com/126651/ad-classics-endless-house-friedrick-kiesler |publisher=ArchDaily |accessdate=24 September 2015}}</ref>
* [[:nl:Antony Kok|Antony Kok]] (1882–1969), poet<ref>{{cite book |last=White |first=Michael |title=De Stijl and Dutch Modernism |url=https://books.google.co.uk/books?id=aC8WsMCrdX4C&pg=PA134&lpg=PA134 |date=20 September 2003 |publisher=Manchester University Press |isbn=978-0-7190-6162-2 |page=134}}</ref>
* [[Bart van der Leck]] (1876–1958), painter<ref name="tate"/>
* [[Piet Mondrian]] (1872–1944), painter, co-founder of De Stijl<ref name="tate"/>
* [[Marlow Moss]] (1889–1958), painter<ref name="the-artists"/>
* [[Jacobus Oud|J. J. P. Oud]] (1890–1963), architect<ref name="tate"/>
* [[Gerrit Rietveld]] (1888–1964), architect and designer<ref name="tate"/>
* [[Kurt Schwitters]] (1887–1948), painter,<ref>{{cite book |last=Hauffe |first=Thomas |title=Design |date=1998 |publisher=Laurence King |location=London |isbn=9781856691345 |oclc=40406039 |page=71 |edition=Reprinted}}</ref> sculptor<ref>[http://www.sprengel-museum.com/painting_and_sculpture/spaces/test-spaces.htm?PHPSESSID=9c95a8e742c4b6fae9a5b9f753ea5184&text=schwitters&anmelden_x=0&anmelden_y=0&snr=4 ''Spaces for the Permanent Collection'', Sprengel Museum Hannover]</ref>
* [[Georges Vantongerloo]] (1886–1965), sculptor<ref name="tate"/>
* [[Friedrich Vordemberge-Gildewart]], painter<ref name="tate"/>
* [[Jan Wils]] (1891–1972), architect<ref name="White2003">{{cite book |last=White |first=Michael |title=De Stijl and Dutch Modernism |url=https://books.google.com/books?id=aC8WsMCrdX4C&pg=PA36 |date=20 September 2003 |publisher=Manchester University Press|isbn=978-0-7190-6162-2 |page=36}}</ref>

== See also==
{{portal|Arts|Netherlands}}
* [[Abstract art]]
* [[Concrete art]]
* [[Art Concret]]
* [[Abstraction-Création]]
* [[Constructivism (art)]]
* [[Fourth dimension in art]]
* [[Mathematics and art]]
* [[Rietveld Schröder House]]

== References and sources ==
; References
{{reflist}}
; Sources
{{refbegin}}
* {{cite web
 |url=http://www.artandculture.com/cgi-bin/WebObjects/ACLive.woa/wa/movement?id=112 
 |title=De Stijl Architecture 
 |accessdate=2006-07-31 
 |work=Design Arts 
 |publisher=Art and Culture 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20060327132656/http://www.artandculture.com/cgi-bin/WebObjects/ACLive.woa/wa/movement?id=112 
 |archivedate=2006-03-27 
 |df= 
}}
* {{cite web
 |url=http://caad.arch.ethz.ch/teaching/nds/ws97/script/text/doesburg.html 
 |title=Towards a plastic architecture 
 |accessdate=2006-07-31 
 |last=van Doesburg 
 |first=Theo 
 |year=1924 
 |work=Translation of original published in De Stijl, XII, 6/7 
 |publisher=Architecture & CAAD 
 |deadurl=yes 
 |archiveurl=https://web.archive.org/web/20051128042504/http://caad.arch.ethz.ch:80/teaching/nds/ws97/script/text/doesburg.html 
 |archivedate=2005-11-28 
 |df= 
}}
{{refend}}

== Further reading ==
{{refbegin}}

* {{cite book |last=Blotkamp |first=Carel (ed.) |author-link= Carel Blotkamp |title=De beginjaren van De Stijl 1917–1922 |location=Utrecht |publisher=Reflex |year=1982}}
* {{cite book |last=Blotkamp |first=Carel (ed.) |title=De vervolgjaren van De Stijl 1922–1932 |location=Amsterdam |publisher=Veen |year=1996}}
* {{cite book |last=Jaffé |first=H. L. C. |title=De Stijl, 1917–1931, The Dutch Contribution to Modern Art |edition=1st |location=Amsterdam |publisher=J.M. Meulenhoff |year=1956}}
* {{cite book |author1=Janssen, Hans  |author2=White, Michael |title=The Story of De Stijl |publisher=Lund Humphries |year=2011 |isbn=978-1-84822-094-2}}
* {{cite book |last=Overy |first=Paul |title=De Stijl |edition=1st |location=London |publisher=Studio Vista |year=1969}}
* {{cite book |last=White |first=Michael |title=De Stijl and Dutch Modernism |location=Manchester [etc] |publisher=Manchester University Press |year=2003}}

{{refend}}

== External links ==
{{commons category|De Stijl}}
{{Wikiquote}}
* [http://sdrc.lib.uiowa.edu/dada/De_Stijl/index.htm ''De Stijl'']
* [[Jakob van Domselaer]]'s [http://www.dofoundation.com/dr001.html ''Proeven van Stijlkunst''], rare recording.
* Essay about [http://www.newcriterion.com/archive/14/sept95/hilton.htm Mondrian and mysticism] Scans of the complete first volume of the journal.

{{Geometric abstraction}}
{{Modern architecture}}
{{Avant-garde}}
{{Westernart}}

{{Authority control}}

[[Category:20th-century architectural styles]]
[[Category:Architecture magazines]]
[[Category:Architecture groups]]
[[Category:Art movements]]
[[Category:Artist groups and collectives of the Northern Netherlands]]
[[Category:De Stijl| ]]
[[Category:Dutch architectural styles]]
[[Category:Dutch magazines]]
[[Category:Dutch-language magazines]]
[[Category:Magazines established in 1917]]
[[Category:Media in Amsterdam]]
[[Category:Modern art]]
[[Category:Avant-garde art]]
[[Category:Piet Mondrian]]