[[File:Roger Malvin's Burial.png|thumb|"Roger Malvin's Burial", as first published in 1832]]

"'''Roger Malvin's Burial'''" is a short story by American author [[Nathaniel Hawthorne]]. It was included in the collection ''[[Mosses from an Old Manse]]''. The tale concerns two fictional colonial survivors returning home after the historical battle known as [[Battle of Pequawket]].

==Plot summary==
Following [[Battle of Pequawket]] (Hawthorne uses the name ''Lovell's Fight'') in 1725, two survivors of the battle struggle to return home.  Roger Malvin and Reuben Bourne are both wounded and weak, and they have little hope that they will survive. They rest near a rock that resembles an enormous tombstone.

Malvin, a much older man, asks Reuben to leave him to die alone, since his wounds are mortal. Reuben insists that he will stay with Malvin as long as he remains alive, but the old man knows that this would mean death for both of them. Malvin convinces Reuben to leave.

Reuben survives. Because he has not honored his promise to bury the old man, he is not at peace. His unease is exacerbated by his failure to tell his fiancée, Dorcas (Malvin's daughter) that he left her father to die. Reuben is considered a brave man by his compatriots, but inside he feels that he has failed them.

Dorcas and Reuben marry, but Reuben's guilt-induced moodiness renders him unfit for normal society. Many years later, when Reuben and Dorcas's son is already grown, Reuben decides that they will move away from the town and settle on a piece of land by themselves. They travel through wilderness. While encamped, Reuben and his son wander into the forest while Dorcas prepares a meal. They become separated. Reuben thinks he hears a deer in the brush and fires his gun, but discovers that he has killed his own son. As he observes the terrain, he realizes it is the same place where he had left Roger Malvin many years before.

==Publication history==
"Roger Malvin's Burial" was likely conceived as early as 1825, the year Hawthorne graduated from [[Bowdoin College]] in [[Brunswick, Maine]], and the 100th anniversary of the historical event that inspired its plot.<ref>Wineapple, Brenda. Hawthorne: A Life. Random House: New York, 2003: 67. ISBN 0-8129-7291-0</ref> The story was first published anonymously in the annual [[gift book]] ''[[The Token and Atlantic Souvenir|The Token]]'' in 1832. Hawthorne published ''[[Twice-Told Tales]]'' in 1837, which collected several of his stories previously included in gift books, though it excluded many of his darker stories like "Roger Malvin's Burial", "[[My Kinsman, Major Molineux]]", and "[[Young Goodman Brown]]", each of which has become recognized as one of Hawthorne's early masterpiece by modern critics.<ref>Mellow, James R. ''Nathaniel Hawthorne in His Times''. Boston: Houghton Mifflin Company, 1980: 77. ISBN 0-395-27602-0</ref> "Roger Malvin's Burial" was not collected in book form until 1846 with the publication of ''Mosses from an Old Manse'' (1846).

==Adaptations to other media==
In 1949 the story was adapted to the syndicated radio program ''[[The Weird Circle]]'' as "The Burial of Roger Malvin".

==References==
{{reflist}}

==External links==
{{Wikisource}}
* Lovejoy, David S. 1954. ''Lovewell's Fight and Hawthorne's "Roger Malvin's Burial"''. In: ''The New England Quarterly'', Vol. 27, No. 4 (Dec., 1954), pp. 527-531. The New England Quarterly, Inc.
* Mackenzie, Manfred. [http://muse.jhu.edu/login?uri=/journals/new_literary_history/v027/27.3mackenzie.html ''Hawthorne's Roger Malvin's Burial: A Postcolonial Reading New'']. In: ''Literary History'', Volume 27, Number 3, Summer 1996, pp. 459-472.

{{Nathaniel Hawthorne}}

[[Category:Short stories by Nathaniel Hawthorne]]
[[Category:1832 short stories]]
[[Category:Filicide in fiction]]