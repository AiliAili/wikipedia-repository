[[File:French Consulate NYC 001.JPG|thumb|right|250px|Charles E. Mitchell House, today French Consulate of New York]]

The '''Consulate General of France''' is the consular representation of the [[French Republic]] in the state of [[New York (state)|New York]], in the [[United States of America]]. The Consulate General is housed in the Charles E. Mitchell House, at 934 [[Fifth Avenue]], between 74th and 75th streets.

The Consulate’s mission is to provide protection and administrative services to French citizens living or traveling in the district. Under the authority of the French Embassy in the United States, its consular district extends across three states ([[New York (state)|New York]], [[Connecticut]] and [[New Jersey]]), as well as the British Overseas Territory of [[Bermuda]].

Currently housing the Consulate General of France, 934 Fifth Avenue was the residence of [[Charles E. Mitchell]], President of the National City Bank (now [[Citibank]]). Ms. Anne-Claire Legendre has been the Consul General since August 2016.

== History of the Consulate ==

===The Charles E. Mitchell House===

The Italian Renaissance-style townhouse, designed by architects A. Stewart Walker and Leon N. Gillette, was built between 1925 and 1926 on Fifth Avenue for Charles E. Mitchell.

While residing at 934 Fifth Avenue, from 1925 to 1933, Mitchell  served as informal advisor to American presidents [[Warren G. Harding]] and [[Herbert Hoover]]. But the prestige of this address owed in great part to his wife, Elizabeth Mitchell, who hosted numerous musical evenings at the house. Indeed, musicians such as George Gerswhin, Fritz Kreisler, Rudolph Ganz, Ignay Padrewski or José Iturbi regularly gave recitals in the “Pink Room” at 934 Fifth Avenue.

In the early 1930s, following the stock exchange crash and investigations on his financial activities, Charles E. Mitchell lost most of his fortune and had to give up his residence. Number 934 is the only survivor of the seven townhouses that formerly lined this block. Within fifty years, the first houses built in the 1880s were replaced by equally luxurious large apartment buildings. The Charles E. Mitchell House was preserved thanks to the decision of the French government, which acquired it in 1942 and made it the official Consulate General building.

=== Before the consulate ===

As historic partners, France and the United States have maintained ties of friendship and cooperation since the first days of the American nation. The first French consular representation was established in Philadelphia in 1778. As soon as 1783, a French consulate was founded in New York, the first consulate to be established in this city. Saint John de Crèvecoeur became the first Consul. However, very little information is available on the buildings that housed the consulate over the 18th centuries.


During the First World War, the French Consulate General in New York was located at 8 Bridge Street, Manhattan.<ref>{{cite book|title=New York City Directory|date=1915|publisher=R. K. Polk & Co.'s|location=New York, NY|page=149|edition=1915|url=https://archive.org/details/newyorkcitydirec1915unse|accessdate=April 3, 2017}}</ref>


From 1933 to 1942, the Consulate General of France was located at [[Rockefeller Center]], at [[Fifth Avenue|640 Fifth Avenue]]. As prestigious as this address was, it was decided, in 1941, to acquire another building that could house the offices and residence of the consul. In 1942, 934 Fifth Avenue became French property. But it wasn’t until 1943, after Franco-American relationships were reestablished (following an interruption under the Vichy regime), that consular affairs resumed with the French resistance representatives.

In keeping with the spirit of its founders, Mr. Charles E. Mitchell and his wife, who conceived the 934 as a place for culture, with an emphasis on literature and music, the consulate has perpetuated this tradition and welcomes, every year, numerous receptions involving the French community. The consulate hosts up to 150 events every year, including the monthly Conferences@934, which bring together French and American speakers.

=== List of the Consul Generals of France in New York ===

The successive consuls have been:

{| class="wikitable"
|-
! Consul General !! Decree of nomination
|-
| J. Hector St John de Crèvecoeur || 1783
|-
| Antoine-René-Charles-Mathurin de La Forest (interim) || 1785
|-
| J. Hector St John de Crèvecoeur || 1787
|-
| ... || 
|-
| Jean-Marie Sotin de La Coindière || 1798
|-
| ... || 
|-
| Alcide Ebray || 
|-
| Étienne Lanel || February, 1907
|-
| [[Marie Gabriel Georges Bosseront d'Anglade]] || November 4, 1913
|-
| Gaston Liébert || January 4, 1916
|-
| Barret || March 19, 1923
|-
| ... || 
|-
| Roger Seydoux || September 23, 1950
|-
| Jean Vyau de Lagarde || 1952
|-
| Jacques Baeyens || May 20, 1957
|-
| Raymond Laporte || November 24, 1958
|-
| Michel Legendre || April 2, 1963
|-
| Jean Béliard || December 10, 1968
|-
| Henri Claudel || October 14, 1969
|-
| Gérard Gaussen || 1972
|-
| Gérard Le Saige de la Villesbrunne || 1978
|-
| Bertrand de la Taillade || 1981
|-
| André Gadaud || 1984
|-
| Benoît d'Aboville || 1989
|-
| André Baeyens || July 15, 1993
|-
| Patrick Gautrat || January 3, 1996
|-
| Richard Duqué || January 16, 1998
|-
| François Delattre || June 21, 2004
|-
| Guy Yelda || May 19, 2008
|-
| Philippe Lalliot || May 15, 2009
|-
| Bertrand Lortholary || August 22, 2012
|-
|Anne-Claire Legendre || August 29, 2016
|}

=== The current Consul General: Anne-Claire Legendre ===

Anne-Claire Legendre took up her position of Consul General of France in New York on August 29, 2016. She is the first woman to hold this position. As Consul General, Ms. Legendre is in charge of promoting the influence and appeal of France across the tri-state area of New York, Connecticut, and New Jersey, as well as Bermuda. She serves a community of 80,000 French citizens, whose vitality actively contributes to the visibility of France in the United States.

A native of Brittany, Anne-Claire Legendre is 37 years old. She graduated from the Institut d’Etudes Politiques in Paris, and holds degrees from Institut National des Langues et Civilisations Orientales (in Arabic) and the University of Sorbonne-Nouvelle in Paris (in modern languages and literature).

Anne-Claire Legendre previously served at the French embassy in Yemen in 2005-2006, before joining the Direction of the French Abroad at the Ministry of Foreign Affairs, where she contributed to develop consular cooperation between member states from the European Union.

From 2008 to 2010, she was in charge of bilateral relations with Algeria as part of the Direction of North Africa and the Middle East. She was then appointed to the Permanent Mission of France to the United Nations, where she served under the current French Ambassador to the United States, Mr. Gérard Araud, until 2013. As the Arab Spring upheavals placed the Middle East at the center of attention, she supervised negotiations on Syria, Lebanon, Israel-Palestine, and Irak, at the Security Council of the United Nations.

In 2013, she was called to the cabinet of the Minister of Foreign Affairs and International Development, Mr. Laurent Fabius, where she served as Advisor on North Africa and the Middle East.

=== Honorary Consulates ===

The Consulate General of France supervises four honorary consuls located respectively in the following towns:

• Hamilton (Bermuda)

• Hartford (Connecticut)

• Princeton (New Jersey)

• Buffalo (New York)

== See also ==
* [[France–United States relations]]
* [[List of diplomatic missions of France]]
* [[Embassy of France, Washington, D.C.]]
{{France diplomatic missions}}

==References==
{{reflist}}

In the Journal officiel de la République française (JORF), on Gallica or Légifrance :

# [http://gallica.bnf.fr/ark:/12148/bpt6k6315034j/f2 ↑ Décret du ?? février 1907 [archive<nowiki>]</nowiki>, JORF no 57 du 27 février 1907, p. 1610.]
# [http://gallica.bnf.fr/ark:/12148/bpt6k6315034j/f2 ↑ Décret du 4 novembre 1913 [archive<nowiki>]</nowiki>, JORF no 306 du 11 novembre 1913, p. 9870.]
# [http://gallica.bnf.fr/ark:/12148/bpt6k6354512g/f3 ↑ Décret du 4 janvier 1916 [archive<nowiki>]</nowiki>, JORF no 78 du 19 mars 1916, p. 2143.]
# [http://gallica.bnf.fr/ark:/12148/bpt6k64757195/f9 ↑ Décret du 19 mars 1923 [archive<nowiki>]</nowiki>, JORF no 90 du 1er avril 1923, p. 3301.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19500924&pageDebut=10008 ↑ Décret du 23 septembre 1950 [archive<nowiki>]</nowiki>, JORF no 227 du 24 septembre 1950, p. 10008.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19570524&pageDebut=05205 ↑ Décret du 20 mai 1957 [archive<nowiki>]</nowiki>, JORF no 120 du 24 mai 1957, p. 5205.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19581126&pageDebut=10592 ↑ Décret du 24 novembre 1958 [archive<nowiki>]</nowiki>, JORF no 276 du 26 novembre 1958, p. 10592.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19630404&pageDebut=03204 ↑ Décret du 2 avril 1963 [archive<nowiki>]</nowiki>, JORF no 81 du 4 avril 1963, p. 3204.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19681213&pageDebut=11676 ↑ Décret du 10 décembre 1968 [archive<nowiki>]</nowiki>, JORF no 293 du 13 décembre 1968, p. 11676.]
# [http://legifrance.gouv.fr/jopdf/common/jo_pdf.jsp?numJO=0&dateJO=19691015&pageDebut=10208 ↑ Décret du 14 octobre 1969 [archive<nowiki>]</nowiki>, JORF no 242 du 15 octobre 1969, p. 10208.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000361897 ↑ Décret du 15 juillet 1993 [archive<nowiki>]</nowiki>, JORF no 166 du 21 juillet 1993, p. 10274, NOR MAEA9320299D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000171247 ↑ Décret du 3 janvier 1996 [archive<nowiki>]</nowiki>, JORF no 3 du 4 janvier 1996, p. 139, NOR MAEA9520568D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000569261 ↑ Décret du 16 janvier 1998 [archive<nowiki>]</nowiki>, JORF no 17 du 21 janvier 1998, p. 983, NOR MAEA9820003D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000438148 ↑ Décret du 21 juin 2004 [archive<nowiki>]</nowiki>, JORF no 144 du 23 juin 2004, p. 11403, texte no 51, NOR MAEA0420217D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000018821194 ↑ Décret du 19 mai 2008 [archive<nowiki>]</nowiki>, JORF no 117 du 21 mai 2008, texte no 41, NOR MAEA0810039D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000020617572 ↑ Décret du 15 mai 2009 [archive<nowiki>]</nowiki>, JORF no 114 du 17 mai 2009, texte no 29, NOR MAEA0909505D.]
# [http://legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000026310886 ↑ Décret du 22 août 2012 [archive<nowiki>]</nowiki>, JORF no 196 du 24 août 2012, texte no 30, NOR MAEA1231543D.]
# [http://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000032005517 ↑ Décret du 10 février 2016 [archive<nowiki>]</nowiki>, JORF no0035 du 11 février 2016, texte no 58, NOR: MAEA1603336D.]

=== Other references  ===

# ↑ « [http://www.consulfrance-newyork.org/Coordonnees-et-horaires Team, contact information and opening hours] » [archive], sur consulfrance-newyork.org (consulté le 26 avril 2011).
# ↑ (en) Abraham Phineas Nasatir et Gary Elwyn Monell, French Consuls in the United States: A Calendar of Their Correspondence in the Archives Nationales, Washington, [[:fr:Bibliothèque du Congrès|Bibliothèque du Congrès]], 1967, in-8°, XII-606 p.

=== Bibliography ===

• Anthony W. Robins et Cécile Girardeau (trad. Lynda Stringer, photogr. Gary Landsman), Consulat général de France à New York, Paris, Éditions internationales du patrimoine, coll. « Résidences de France », 2013, 175 p. (ISBN 979-10-90756-07-6)
• (en) Michael C. Kathrens, Great Houses of New York: 1880-1930, Acanthus Press, coll. « Urban Domestic Architecture Series », 2005 (ISBN 0-926494-34-1), p.&nbsp;371–372

== External links ==
{{commons category|French Consulate General, New York}}
* {{Official website|http://www.consulfrance-newyork.org/-English-}}

{{coord|40.7739|N|73.9656|W|source:wikidata|display=title}}

[[Category:Diplomatic missions of France]]
[[Category:Buildings and structures in Manhattan]]