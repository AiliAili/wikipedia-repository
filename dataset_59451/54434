{{Use dmy dates|date=October 2012}}
{{Infobox Australian place
| type                = suburb
| name                = Munno Para
| city                = Adelaide
| state               = sa
| image               = 
| caption             = 
| pop                 = 1,754
| pop_year = {{CensusAU|2006}}
| pop_footnotes = <ref name="ABS">{{Census 2006 AUS|id =SSC42111|name=Munno Para (State Suburb)|accessdate=10 April 2011| quick=on}}</ref>
| pop2                = 
| pop2_year           = 
| pop2_footnotes      = 
| est                 = 
| postcode            = 5115
| area                = 
| dist1               = 
| dir1                = NW
| location1           = [[Adelaide city centre]]
| lga                 = City of Playford
| stategov            = [[Electoral district of Napier|Napier]]<ref>{{cite web |url=http://www.ecsa.sa.gov.au/component/edocman/?task=document.download&id=570&Itemid=0 |title=District of Napier Background Profile |date=29 April 2014 |publisher=[[Electoral Commission of South Australia]], Government of South Australia |accessdate=8 April 2016}}</ref>
| fedgov              = [[Division of Wakefield|Wakefield]]
| near-n              = [[Kudla, South Australia|Kudla]]
| near-ne             = [[Evanston South, South Australia|Evanston South]]
| near-e              = [[Blakeview, South Australia|Blakeview]]
| near-se             = [[Blakeview, South Australia|Blakeview]]
| near-s              = [[Smithfield, South Australia|Smithfield]]
| near-sw             = [[Smithfield Plains, South Australia|Smithfield Plains]]
| near-w              = [[Munno Para West, South Australia|Munno Para West]]
| near-nw             = [[Munno Para Downs, South Australia|Munno Para Downs]]
}}

'''Munno Para''' is a northern suburb of [[Adelaide]], [[South Australia]]. It is located in the [[City of Playford]].

In the local [[Kaurna]] dialect, Munno Para means ''golden wattle creek''.<ref name="Manning">{{cite web |url=http://www.slsa.sa.gov.au/manning/pn/m/m12.htm#munnoP |title=Place Names of South Australia |author= |date= |work=The Manning Index of South Australian History |publisher=State Library of South Australia |accessdate=10 April 2011}}</ref> The name refers to either the [[Gawler River (South Australia)|Gawler River]] (which starts at the confluence of the [[South Para River]] and [[North Para River]]) or the [[Little Para River]] from which the much larger cadastral [[Hundred of Munno Para]] derives its name.{{Citation needed|date=February 2016}}

==History==
Until 1997, Munno Para was part of the [[City of Munno Para]], formerly the district councils of [[District Council of Munno Para East|Munno Para East]] and [[District Council of Munno Para West|Munno Para West]] which were established in 1853 on land bounded by the cadastral [[Hundred of Munno Para]].<ref name="PlayfordHistory">{{Cite web |url=http://www.playford.sa.gov.au/page.aspx?u=1339 |title=History of Playford |publisher=City of Playford |accessdate=23 February 2016}}</ref><ref name="MarsdenMunnoPara">{{Cite web |url=https://www.lga.sa.gov.au/webdata/resources/files/LGA-89938_-_2011_18_-_FINAL_History_of_SA_Councils.pdf |title=LOCAL GOVERNMENT ASSOCIATION OF SOUTH AUSTRALIA: A HISTORY OF SOUTH AUSTRALIAN COUNCILS to 1936 |first=Susan |last=Marsden |date=2012 |accessdate=23 February 2016}}</ref><ref name="Manning"/> The older part of the suburb itself began from a subdivision in 1955 and the suburb was named in 1978.<ref name="PLB">{{cite web |url=http://maps.sa.gov.au/plb/# |id=SA0001699 |title=Placename Details: Munno Para |accessdate=4 December 2014 |date=15 September 2011 |publisher=Land Services Group, [[Department of Planning, Transport and Infrastructure]]}}</ref> Until the locality was urbanised from 1955, Munno Para was part of the farming township of [[Smithfield, South Australia|Smithfield]].

The Playford Alive development began in the 2000s, establishing urban development in Munno Para to the west of the railway line, centred initially on Coventry Road, then establishing a new "town centre" with shops and commercial development along Curtis Road and Peachey Road extended north of Curtis Road into the suburb.

==Geography==
The suburb lies northeast of [[Elizabeth, South Australia|Elizabeth]] and [[Smithfield, South Australia|Smithfield]]. The original part of Munno Para is basically rectangular in shape, lying lengthwise between the [[Gawler Central railway line]] and [[Main North Road]]. More recent development on the western side of the railway line is also in the suburb of Munno Para.<ref name="PLB"/> The entire suburb is fairly flat, sloping slightly down to the west and south.

The only road across the railway line in Munno Para is Curtis Road along the southern boundary of the suburb. Pedestrians can also cross at the [[Munno Para railway station]]. Main North Road runs along the eastern boundary, Stebonheath Road along the western boundary. Coventry Road historically was parallel and halfway between these two, however the suburban development has introduced some bends for traffic calming on this route.

==Demographics==
{{update|section|date=December 2014}}
The 2006 Census by the [[Australian Bureau of Statistics]] counted 1,754 persons in Munno Para on census night. Of these, 49.2% were male and 50.8% were female.<ref name="ABS"/>

The majority of residents (74.7%) are of Australian birth, with 12.9% declaring [[England]] as their country of birth.<ref name="ABS"/>

The average age of Munno Para residents is slightly lower than the greater Australian population. 60.4% of residents were over 25 years in 2006, compared to the Australian average of 66.5%; and 39.6% were younger than 25 years, compared to the Australian average of 33.5%.<ref name="ABS"/>

==Community==
The local newspaper is the [[News Review Messenger]]. Other regional and national newspapers such as [[The Advertiser (Adelaide)|The Advertiser]] and [[The Australian]] are also available.

===Schools===
Munno Para Primary School is located near the centre of the original part of the suburb. [[Mark Oliphant College]] and [[Adelaide North Special School]] are in the northern part of the newer area.

==Attractions==

===Parks===
There is greenspace throughout the residential areas of the suburb. The older part is designed with a linear park through the middle of the long axis. The newer part has landscaped wetlands along Curtis Road near Douglas Drive (formerly Coventry Road) and near Stebonheath Road. These wetlands calm and treat stormwater before releasing it into Smith Creek. The North Lakes golf course is in the northwestern corner.

==Transportation==

===Roads===
Munno Para is serviced by [[Main North Road]], linking the suburb to [[Gawler, South Australia|Gawler]], [[Elizabeth, South Australia|Elizabeth]] and [[Salisbury, South Australia|Salisbury]].

===Public transport===
Munno Para is serviced by buses run by the [[Adelaide Metro]]. The suburb also lies astride the [[Gawler Central railway line]], being serviced at [[Munno Para railway station]].

===Bicycle routes===
A bicycle path extends through parklands through the middle of the original part of the suburb.

==See also==
*[[List of Adelaide suburbs]]
*[[Hundred of Munno Para]]

==References==
{{reflist}}

==External links==
*[http://www.playford.sa.gov.au/ City of Playford]
*[http://www.lga.sa.gov.au/site/page.cfm?c=4143 Local Government Association of SA - City of Playford]
*[http://www.censusdata.abs.gov.au/ABSNavigation/prenav/ProductSelect?textversion=false&areacode=4&subaction=-1&action=201&period=2006&collection=census&navmapdisplayed=true&breadcrumb=L& 2006 ABS Census Data by Location]

{{Coord|-34.6668|138.7|format=dms|type:city_region:AU-SA|display=title}}
{{City of Playford suburbs}}

[[Category:Suburbs of Adelaide]]