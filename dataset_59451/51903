{{Infobox journal
| title = Journal of Information Ethics
| editor = R. Hauptmann
| discipline = [[Information science]]
| publisher = [[McFarland & Company]]
| frequency = Biannually
| history = 1992–Present
| website = http://icie.zkm.de/publications/journals/ie
| link1 =
| link1-name =
| ISSN = 1061-9321
| eISSN = 1941-2894
}}
The '''''Journal of Information Ethics''''' is an [[academic journal]] of [[philosophy]]. The [[editor-in-chief]] is Robert Hauptmann.<ref name=joie>[http://www.mcfarlandbooks.com/customers/journals/journal-of-information-ethics/ McFarland & Company, Inc., Publishers (2012). Journal of Information Ethics - macfarlandpub.com. Retrieved from http://www.mcfarlandbooks.com/customers/journals/journal-of-information-ethics/]</ref> It has been published biannually since 1992 by [[McFarland & Company]] and the [[Center for Art and Media Karlsruhe]]. The publisher description of editorial content reads:"From the ethics of Caller ID to transmission of sexually explicit materials via Internet, the information age presents a barrage of ethical challenges. In this acclaimed twice-yearly journal, some of the brightest and most influential figures in the information sciences confront a broad range of these transdisciplinary issues."<ref name=joie />

According to ''[[Ulrich's Periodicals Directory]]'', it "deals with ethical issues in all of the information sciences, from library acquisitions to database management, with a multidisciplinary approach."<ref>Ulrichs Online</ref>{{Who|date=June 2010}}

==Review and indexes==
According to the publisher's web site, the journal has been reviewed by ''[[Library Journal]]'', ''[[Choice: Current Reviews for Academic Libraries]]'', ''Special Libraries'', and ''Library and Information Science Annual''.<ref name=joie />

It is abstracted and indexed in ''Library Literature'', ''Library, Information Science & Technology Abstracts'', [[Scopus]], [[PubMed]], [[American Theological Library Association|ATLA Religion]], and ''[[Philosophy Documentation Center#Research guides|The Philosophers' Index]]''.

== See also ==
*[[List of philosophy journals]]

==References==
{{Reflist}}

==External links==
* {{Official|http://www.mcfarlandbooks.com/customers/journals/journal-of-information-ethics/}}

{{DEFAULTSORT:Journal Of Information Ethics}}
[[Category:Ethics journals]]
[[Category:Philosophy journals]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 1992]]