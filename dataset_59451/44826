{{distinguish|Durham Athletic Park}} 
{{Infobox venue
| stadium_name        = Durham Bulls Athletic Park
| nickname            = DBAP
| image               = [[File:Durham Bulls Athletic Park Durham.JPG|250px|center]] The main entrance in 2014
| location            = 409 Blackwell Street<br>Durham, North Carolina 27701
| coordinates         = {{coord|35|59|30.08|N|78|54|15.07|W|type:landmark_scale:2000|display=inline,title}}
| broke_ground        = April 24, 1993<ref>{{cite news |title=The Beginning of a Renaissance in Downtown Durham|first=Grant|last=Parsons|url=http://nl.newsbank.com/nl-search/we/Archives?p_product=RLOB&p_theme=rlob&p_action|newspaper=[[The News & Observer]]|location=Raleigh|date=May 26, 1993|accessdate=February 21, 2012}}
</ref>
| opened               = April 6, 1995
| renovated           = 2002–2004, 2009, 2014, 2016 
| expanded           = 1997–1998, 2009–2010, 2014
| closed              = 
| demolished          = 
| owner               = [[Durham, North Carolina|City of Durham]]
| operator            = [[Durham Bulls|Durham Bulls Baseball Club]]
| surface             = Grass
| construction_cost   = US$18.5 million<br>(${{formatprice|{{Inflation|US|18500000|1995}}}} in {{CURRENTYEAR}} dollars{{inflation-fn|US}})
| architect           = HOK Sport now [[Populous (company)|Populous]] <br>[[Philip Freelon|The Freelon Group]]<ref>{{cite web |title=Durham Bulls Athletic Park|url=http://www.freelon.com/portfolio/231/Other|publisher=The Freelon Group|accessdate=October 1, 2011}}</ref>
| project_manager     = CHA Enterprises<ref>{{cite news |title=Ballpark $2.4 Million Over Budget|first=Tim|last=Vercellotti|url=http://infoweb.newsbank.com/iw-search/we/InfoWeb?p_product=AWNB&p_theme=aggregated5&p_action=doc&p_docid=0EB033EFCB75EBA5&p_docnum=31&p_queryname=102|newspaper=[[The News & Observer]]|location=Raleigh|date=November 18, 1993|accessdate=July 3, 2012}}</ref>
| structural engineer = 
| services engineer   = Knott Benson Engineering Associates P.A.<ref>{{cite web |title=Sports Facilities|url=http://www.kbengs.com/Sports.htm|publisher=Knott Benson Engineering Associates|accessdate=July 3, 2012}}</ref>
| general_contractor  = George W. Kane Construction Co.<ref>{{cite web |title=Durham Bulls Athletic Park|url=http://www.goduke.com/ViewArticle.dbml?DB_OEM_ID=4200&ATCLID=205086984|publisher=Duke Athletics|accessdate=October 1, 2011}}</ref>
| main_contractors    = 
| former_names        = 
| tenants             = [[Durham Bulls]] ([[Carolina League|CL]] and [[International League|IL]]) (1995–present)<br>[[Duke Blue Devils]] ([[National Collegiate Athletic Association|NCAA]]) (2010–present)<br>[[North Carolina Central University|North Carolina Central]] Eagles ([[National Collegiate Athletic Association|NCAA]])<br>[[Atlantic Coast Conference Baseball Tournament|ACC Tournament]] ([[1996 Atlantic Coast Conference Baseball Tournament|1996]], [[1998 Atlantic Coast Conference Baseball Tournament|1998]], [[1999 Atlantic Coast Conference Baseball Tournament|1999]], [[2009 Atlantic Coast Conference Baseball Tournament|2009]], [[2011 Atlantic Coast Conference Baseball Tournament|2011]], [[2013 Atlantic Coast Conference Baseball Tournament|2013]])<br>[[Triple-A All-Star Game|2014 Triple-A All-Star Game]]
| seating_capacity    = 10,000 (1998–present)<br>9,033 (1995–1997)<ref>{{cite web |title=Durham Bulls Athletic Park|first=Graham|last=Knight|url=http://www.baseballpilgrimages.com/minors/durham.html|publisher=Baseball Pilgrimages|accessdate=October 1, 2011}}</ref>
| dimensions          = Left Field – {{convert|305|ft|m|abbr=on}}<br>Left Center – {{convert|375|ft|0|abbr=on}} <br>Center Field – {{convert|400|ft|0|abbr=on}}<br>Right Center – {{convert|375|ft|0|abbr=on}}<br>Right Field – {{convert|325|ft|0|abbr=on}}
}}

'''Durham Bulls Athletic Park''' ('''DBAP''', pronounced "d-bap") is a 10,000-seat ballpark in [[Durham, North Carolina]] that is home to the [[Durham Bulls]], the [[Triple-A (baseball)|AAA]] affiliate of the [[Tampa Bay Rays]] of [[Major League Baseball]].  It is also home to the [[Duke Blue Devils]]<ref name="duke">{{cite web |title=Duke, Durham Bulls Announce Partnership|url=http://www.goduke.com/ViewArticle.dbml?SPSID=22847&SPID=1850&DB_OEM_ID=4200&ATCLID=204835163|publisher=Duke Athletics|accessdate=November 17, 2009|archiveurl=http://www.webcitation.org/5lMZfj51o?url=http://www.goduke.com/ViewArticle.dbml?SPSID%3D22847%26SPID%3D1850%26DB_OEM_ID%3D4200%26ATCLID%3D204835163|archivedate=November 17, 2009}}</ref> and [[North Carolina Central University|North Carolina Central]] Eagles [[college baseball]] teams.<ref name="box">{{cite web|title=Savannah State Univ. vs N.C. Central Univ. (Mar 07, 2009) |url=http://nccueaglepride.cstv.com/sports/m-basebl/stats/2008-2009/ssunccu1.html |publisher=North Carolina Central Athletics |date=March 7, 2009 |accessdate=November 15, 2009 |archiveurl=http://www.webcitation.org/5lJht8N11?url=http://nccueaglepride.cstv.com/sports/m-basebl/stats/2008-2009/ssunccu1.html |archivedate=November 15, 2009 |deadurl=yes |df= }}</ref> The $18.5-million park opened in 1995.

==History==
The ballpark was designed by HOK Sport (now [[Populous (company)|Populous]]), who also designed [[Oriole Park at Camden Yards|Camden Yards]] in Baltimore, [[Jacobs Field]] in Cleveland, and [[Coors Field]] in Colorado as part of the "new" old-stadium-like movement of the 1990s. The Bulls began playing at the DBAP in 1995 when the team played in the Class A Advanced [[Carolina League]]. In 1998, Durham moved up to the Triple-A level, causing the DBAP to be expanded. The first Triple-A game was played on April 16, 1998.<ref name="Durham Bulls Athletic Park Fun Facts"  />

[[Image:Durhambull.jpg|thumb|left|250px|The second iteration of the snorting bull sign in May 2007]]

A roof covers approximately 2,500 seats behind home plate and down both the first and third base lines to the end of each dugout. All seats at the DBAP are extra wide with seat backs, extra leg room and over 95% of the seats have cup holders. The stadium was designed and built so that every seat gives fans a great view of the field with an intimate ballpark feel. Durham Bulls Athletic Park is located in downtown Durham and can be accessed from the Durham Freeway. The ballpark reflects many characteristics of old-time parks and the historic downtown Durham architecture.

Following a playoff game on September 6, 2007, the playing surface was named '''Goodmon Field''', in honor of the owner of the Durham Bulls and CEO of [[Capitol Broadcasting]].<ref>{{cite news |title=DBAP Field Named for Goodmon|url=http://www.wralsportsfan.com/rs/video/1786800/#/vid1786800|work=[[WRAL-TV|WRAL]]|location=Raleigh|date=September 6, 2007|accessdate=February 15, 2013}}</ref>

On August 30, 2011, Triple-A Baseball announced that Durham Bulls Athletic Park would be the host site of the 2012 Triple-A National Championship Game on Tuesday, September 18, 2012. The Triple-A National Championship Game pits the winner of the International League's Governors' Cup against the Pacific Coast League Champions in a one-game, winner take all championship. The Bulls were the first International League team to host this annual game. The game was projected to bring in $2.5 million just for the city of Durham. With another $2 million for the adjacent cities (including Raleigh). The game itself saw Reno of the PCL win an easy 10-3 victory over Pawtuckett of the IL.<ref>{{cite web |title=Aces Win Triple-A Championship!|url=http://www.milb.com/news/article.jsp?ymd=20120918&content_id=38632228&vkey=news_t2310&fext=.jsp&sid=t2310|publisher=[[Minor League Baseball]]|date=September 18, 2012|accessdate=February 15, 2013}}</ref>

===The Blue Monster===
The ballpark's left-field fence is a {{convert|32|ft|m|adj=mid|-high|1}} wall, {{convert|305|ft|m|0}} from home plate, known as the Blue Monster. It resembles [[Fenway Park]]'s [[The Green Monster|Green Monster]], including a manual scoreboard. Original plans called for a wall 8 feet high, however due to the park's dimensions conflicting with a nearby road designers shortened left field by several feet. As a result, the wall ended up being 24 feet tall. Eventually, the wall reached its current height in 1998.<ref name="Durham Bulls Athletic Park Fun Facts">http://www.milb.com/documents/6/5/0/115853650/Durham_Bulls_Athletic_Park_o9p90evv.pdf</ref> The club introduced a furry "Blue Monster" mascot in during the 2007 season who now shares mascot duties with "Wool E. Bull" and "Lucky the Wonder Dog".

The bull sign mounted atop of the Blue Monster was modeled after the bull used in the 1988 film, ''[[Bull Durham]]''. The actual sign from the movie (which featured at the Bulls previous home, [[Durham Athletic Park]]) formerly hung in the concourse level of DBAP but is now in storage. Although much sturdier than the original, the new sign's limitations were revealed in violent winds that rocked the Piedmont on April 16, 2007 – the bull's head and forelegs were torn off by the storm.<ref>{{cite press release |title=High Winds Destroy Famous Snorting Bull at DBAP|url=http://www.durhambulls.com/team/press_release.html?id=70|publisher=[[Durham Bulls|Durham Bulls Baseball Club]]|date=April 16, 2007|accessdate=April 16, 2007|archiveurl=https://web.archive.org/web/20070927230311/http://www.durhambulls.com/team/press_release.html?id=70|archivedate=September 27, 2007|quote=The press release also includes a [http://www.durhambulls.com/images/features/screen_4623cc13cb612.jpg picture] of the damage}}</ref> The damage was fixed by that weekend, but plans were made to replace the sign.<ref name="Bulls History1990-2007">{{cite web |title=Bulls History (1990-2012)|url=http://www.milb.com/team3/page.jsp?ymd=20110803&content_id=22698538&vkey=team3_t234&fext=.jsp&sid=t234|publisher=[[Durham Bulls|Durham Bulls Baseball Club]]|date=August 3, 2011|accessdate=February 15, 2013}}</ref>

==Renovations==
The ballpark has undergone a number of renovations and enhancements since opening, with its first renovations starting only a couple of years after the park opened. Construction of a "warehouse type" building, Diamond View, began in 1997 and was completed during the 1998 season. Diamond View is located behind the right field seating sections and uses the same architecture as the DBAP, including the green roof, brickwork and windows. In 2002, the DBAP unveiled a new playground area in the right field section of the concourse. In the Fall of 2003, the field of the DBAP received a major face lift. After nine years of service, the top layers of grass and soil were removed and replaced with brand new top soil and Tissport™ Bermuda grass. The renovation took place over several weeks and cost over $100,000.

Before the 2004 season, a 13' by 17' [[Daktronics]] ProStar [[LED]] video board was installed in place of the original scoreboard, giving the Bulls a major technology upgrade for the time. They were able to show instant replay and other graphics.<ref>http://www.capitolbroadcasting.com/2004/03/19/bulls-purchase-new-video-board-for-dbap/</ref>
Following the 2006 season, the DBAP underwent major renovations in the outfield, including a new left field wall complete with a new video board located above the manual scoreboard. The old video board was reshaped into a video billboard and placed in right field.

In 2008, with the addition of the Diamond View II building in left field, the Blue Monster pavilion opened allowing fans to watch the game atop the Blue Monster for the first time. The famous snorting bull was replaced by a new two-sided bull so that it may be viewed from Diamond View 2 and 3. The Bulls' TV Crew were also equipped with high-definition cameras and production equipment along with HD production capabilities that same year. This also complemented a new state-of-the-art BOSE sound system. 

One year later a stairwell was added to the pavilion. This connected it to the third base concourse, making the DBAP a 360° ballpark. For the 2010 season, the Diamond View II Building opened a new restaurant called the "Tobacco Road Sports Cafe". It has outdoor seating to watch the game during game days or just to enjoy a North Carolina summer evening. Outside the ballpark are four more restaurants: the Cuban Revolution, Saladelia, Tyler's Restaurant & Taproom, and Mellow Mushroom. Construction of Diamond View III began in 2012 and was completed in 2013.
Before the start of the 2016 season the Bulls announced additions to the park, including an updated kids play area called Playground 42. Two new message boards also were installed at the main entrance. The boards were made up from pieces of the park's former video board, which was removed due to the 2014 renovations.<ref>http://www.milb.com/news/article.jsp?ymd=20160406&content_id=170786886&fext=.jsp&vkey=news_t234</ref>

===2014 Renovation===
After the end of the 2013 season for the Durham Bulls, major renovations to the entire stadium began and were finished by opening day of the 2014 season.  After a 20-year lease extension was signed between the Bulls and city of Durham, keeping the minor league team at the stadium through 2033, both parties agreed that improvements were much needed.<ref>{{cite news |title=Bulls, DBAP Lease Agreement Approved|first=Aaron|last=Schoonmaker|url=http://www.wralsportsfan.com/bulls-dbap-lease-agreement-approved/12417813/|work=[[WRAL-TV|WRAL]]|location=Raleigh|date=May 6, 2013|accessdate=December 31, 2013}}</ref>  The renovations cost approximately $9 million, $6 million of which was covered by the city.<ref name=upgrade>{{cite news |title=DBAP Upgrade|url=http://www.heraldsun.com/sports/durhambulls/x2082476198/DBAP-upgrade|newspaper=[[The Herald-Sun]]|location=Durham, North Carolina|date=October 27, 2013|accessdate=December 31, 2013}}</ref>  In their agreement to renovate the stadium, it was set that the city of Durham would have a maximum cap of $12 million spent on all renovations.  Anything beyond the $12 million would be covered by the Bulls, with the team having to pay a minimum of $2 million towards the total costs.<ref name=renovations>{{cite news |title=DBAP Renovation Unfolding Well, Bulls Say|first=Ray|last=Gronberg|url=http://www.heraldsun.com/news/localnews/x1388441310/DBAP-renovation-unfolding-well-Bulls-say|newspaper=[[The Herald-Sun]]|location=Durham, North Carolina|date=December 26, 2013|accessdate=December 31, 2013}}</ref>

[[File:DBAP game action 1.JPG|thumb|right|200px|Game action at DBAP in 2010]]

The biggest issue for fans attending a game at the DBAP had been long concession lines and an overcrowded concourse with no view of the field.<ref name=WRAL>{{cite news |title=DBAP to Get An Upgrade in Off-Season|first=Aaron|last=Schoonmaker|url=http://wap.wral.com/w/localnews/story/99152594/?sp=1|work=[[WRAL-TV|WRAL]]|location=Raleigh|date=September 6, 2013|accessdate=December 31, 2013}}</ref>  To alleviate this problem, concessions were added to the upper-level concourse, which previously had none.  New picnic areas were added down the third-base line and in the outfield stands.  Additionally, a new club area was built that can also be used for non-Durham Bulls related events during the off-season.<ref name=renovations/>  The club area has its own kitchen, separate from the regular concessions' kitchen to help improve the speed of the food service.

The field was completely restructured within the renovation as well. The entire field and underground plumbing was removed and updated with new plumbing, better drainage, new sod, and a new crown to ensure that the field drains properly.<ref name=upgrade/>  This had not been done in the 18-year life of the stadium and was a much-needed part of the renovation.

Additionally, the stadium has new field lighting, a new ticket entrance down the third base line, and new scoreboards and video boards. TS Sports of [[Dallas]] was contracted to install three new state-of-the-art, HD displays.<ref>{{cite web |title=New HD Videoboards Among Exciting DBAP Upgrades|url=http://www.milb.com/news/article.jsp?ymd=20131119&content_id=64029964&vkey=news_t234&fext=.jsp&sid=t234|publisher=Minor League Baseball|date=November 19, 2013|accessdate=December 31, 2013}}</ref> They include a Blue Monster primary display (25.4' (h) x 63' (w)), right field LED wall display (6.5' (h) x 327.6' (w)), and a club level [[Fascia (architecture)|fascia]] display (3.2' (h) x 119.8' (w)).

The renovations were finished before the opening pitch of the 2014 Durham Bulls season on April 3, 2014.<ref>{{cite news |title=Major-League Quality: Renovated DBAP Will Have More Concessions, Improved Field, ‘Awesome’ Lights|first=Steve|last=Wiseman|url=http://www.heraldsun.com/news/localnews/x2025293478/Major-league-quality|newspaper=[[The Herald-Sun]]|location=Durham, North Carolina|date=April 3, 2014|accessdate=April 6, 2014}}</ref>  The quick turnaround was primarily due to Durham being the host city for the 2014 [[Triple-A All-Star Game]].  This nationally televised game brought in visitors from across the country for a 5-day festival. Had it not been for this reason, Mike Birling, Bulls general manager, stated that the renovation would have most likely been done in phases.<ref name=WRAL/>

==Dimensions==
The original dimensions were: Left Field – {{convert|305|ft|1|abbr=on}}, Left Center Field – {{convert|371|ft|1|abbr=on}}, Center Field – {{convert|400|ft|1|abbr=on}}, Right Center Field – {{convert|373|ft|1|abbr=on}}, Right Field – {{convert|327|ft|1|abbr=on}}. The power alleys are now posted as {{convert|375|ft|1|abbr=on}} and right field as {{convert|325|ft|1|abbr=on}}.
[[File:DBAP from center field.JPG|thumb|left|upright=2.5|DBAP game action, bathed in twilight glow.]]
{{clear}}
{{clear}}

==Other events==
In addition to hosting the Bulls, DBAP has also been host to other baseball events since its opening. These include hosting six [[Atlantic Coast Conference Baseball Tournament|ACC Baseball Tournaments]], hosting the Triple-A All-Star Game in 2014, and hosting the Triple-A National Championship Game in 2012. Perhaps the most significant game during any of the events was an 18-inning contest between the [[North Carolina Tar Heels baseball|UNC Tar Heels]] and the [[NC State Wolfpack baseball|NC State Wolfpack]] during the 2013 ACC tournament semifinals. It set records for both the largest attendance for a college baseball game in NC with 11,392 attending, and the longest baseball game in ACC tournament history.<ref name="Durham Bulls Athletic Park Fun Facts"  />

==See also==
* [[List of NCAA Division I baseball venues]]
* [[American Tobacco Historic District]]

==References==
{{reflist|2}}

==External links==
{{Commons category|Durham Bulls Athletic Park}}
* {{osmrelation|5366060}}
* [http://www.milb.com/content/page.jsp?ymd=20110817&content_id=23336674&sid=t234&vkey=team2 Official Bulls web site description of the stadium]
* [http://www.littleballparks.com/Stadium/2002/Durham/Durham.htm Durham Bulls Athletic Park Views - ''Ball Parks of the Minor Leagues'']
{{s-start-collapsible|header={{s-sta|et}}}}
{{succession box
 | title = Home of the<br>[[Durham Bulls]]
 | years = 1995 &ndash; present
 | before = [[Durham Athletic Park]]
 | after = current
}}
{{end}}
{{IL Ballparks}}
{{Atlantic Coast Conference baseball venue navbox}}
{{North Carolina NCAA Division I college baseball venue navbox}}
{{Triangle sports venues}}

[[Category:Minor league baseball venues]]
[[Category:Sports venues in Durham, North Carolina]]
[[Category:Baseball venues in North Carolina]]
[[Category:Duke Blue Devils baseball]]
[[Category:North Carolina Central Eagles baseball]]
[[Category:College baseball venues in the United States]]
[[Category:1995 establishments in North Carolina]]
[[Category:Sports venues completed in 1995]]