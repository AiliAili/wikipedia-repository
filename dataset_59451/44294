{{Infobox airport
| name         = Reus Airport
| nativename   = <small>''Aeroport de Reus''</small>
| nativename-a = 
| nativename-r = 
| image        = <!--airport logo-->
| image-width  = 200
| image2       = REU exterior.jpg
| image2-width = 250
| IATA         = REU
| ICAO         = LERS
| pushpin_map            = Spain Catalonia 
| pushpin_map_caption    = Location of airport in Catalonia
| pushpin_label          = REU
| pushpin_label_position = right
| type         = Public
| owner        = 
| operator     = [[Aena]]
| city-served  = [[Reus]], [[Spain]]
| location     = <!--If different than above-->
| elevation-f  = 233
| elevation-m  = 71
| coordinates  = {{coord|41|08|51|N|001|10|02|E|type:airport_region:ES}}
| website      = [http://www.aena-aeropuertos.es/csee/Satellite/Aeropuerto-Reus/en/ www.aena.es]
| metric-elev  = Y
| metric-rwy   = Y
| r1-number    = 07/25
| r1-length-f  = 8,068
| r1-length-m  = 2,459
| r1-surface   = Asphalt
| stat-year    = 2016
| stat1-header = Passengers
| stat1-data   = 817,611
| stat2-header = Passenger change 15-16
| stat2-data   = {{increase}}16.0%
| stat3-header = Aircraft movements
| stat3-data   = 14,472
| stat4-header = Movements change 15-16
| stat4-data   = {{increase}}6.9%
| footnotes    = Source: AENA<ref name="stats15">[http://www.aena.es/csee/ccurl/483/1011/Copia%20de%2012.Estadisticas_Diciembre_2015.pdf Informes Anuales (provisional) - 2015][http://www.aena.es/csee/ccurl/339/464/Reus%20Airport%202015.pdf][http://www.aena.es/en/reus-airport/introduction.html] - [[Aena]]</ref>
}}

'''Reus Airport''' {{Airport codes|REU|LERS}} is located by the beaches of [[Costa Daurada]], equidistant in relation to the town of [[Constantí]] and the city of [[Reus]] and approximately {{convert|7.5|km|abbr=on}} from the city of [[Tarragona]], in [[Catalonia]], [[Spain]].  The airport receives a large amount of tourist traffic from passengers destined for the beach resorts of [[Salou, Spain|Salou]] and [[Cambrils]] as well as for [[Barcelona]], which is approximately {{convert|100|km|abbr=on}} to the northeast. It is also close to one of Europe's largest theme parks, [[PortAventura World]].  In addition, passengers travel to the [[Mountains of Prades]], a [[Mediterranean]] forest in the [[Comarques of Catalonia|comarca]] of [[Baix Camp]].

==History==
===Early years===
The airport was founded in 1935 as a venture by the Aeroclub de Reus. It served as a Republican base during the Spanish Civil War and after the fascist victory served as a Spanish Air Force base.  The base was demilitarised in the early 1990s and became a fully civilian airport administered by AENA, the Spanish airports authority.

===Development since the 2000s===
The airport is a [[Ryanair]] base since October 2008 although for the winter 2009–2010 season Ryanair reduced the number of flights and destinations from Reus by a substantial amount. This reduction was only temporary for the winter months and a full flight programme recommenced late March 2010.

On 29 June 2011 Ryanair announced that their base would close on 30 October with the loss of 28 routes after failing to reach an agreement with the local Government. Ryanair resumed some flights in March 2012, but they are operated by aircraft not based at Reus.<ref>http://www.elpais.com/articulo/economia/Ryanair/abandonara/Reus/elpepuespcat/20110629elpepueco_10/Tes</ref>

==Terminal==
[[File:Airport Reus seen from air.jpg|thumb|Aerial view]]
In order to adapt Reus Airport to future air traffic demand, Aena Aeropuertos has carried out a series of improvements and extended its facilities.  These include a new check-in building between the arrivals and departures buildings, integrating the three buildings into one.  The departures building has also been remodelled for use as a boarding area. The new departures terminal has 23 check-in desks and 12 boarding gates spread over two rooms: gates 1 to 6 are intended for Non-[[Schengen Area|Schengen]] flights, and 7 to 12 are dedicated to Schengen destinations. The public area and the passenger only zone have cafeteria and restaurant services and duty-free shops.

==Airlines and destinations==
{{Airport-dest-list
<!-- -->
| [[AlbaStar]] | '''Seasonal charter:''' [[Dublin Airport|Dublin]] (begins 27 June 2017),<ref>http://www.falconholidays.ie/flight/timetable</ref> [[Ireland West Airport Knock|Knock]]
<!-- -->
| [[ASL Airlines Ireland]] | '''Seasonal charter:''' [[Cork Airport|Cork]], [[Dublin Airport|Dublin]], [[Shannon Airport|Shannon]]
<!-- -->
| [[CityJet]] | '''Seasonal charter:''' [[Dublin Airport|Dublin]] (begins 3 June 2017)<ref>https://www.sunway.ie/sunholidays/search.asp?view=detail&aff=&form=main&type=J&dest=REU&grade=2&duration=7&origin=DUB&sDate=03/06/2017&pax=2&childs=0&infants=0#2 STAR</ref>
| [[Flybe]] <br> operated by [[Stobart Air]] | '''Seasonal:''' [[London Southend Airport|London-Southend]] (begins 10 May 2017)<ref>http://www.flybe.com</ref>
<!-- -->
| [[Jet2.com]] | '''Seasonal:''' [[Belfast International Airport|Belfast–International]], [[Birmingham Airport|Birmingham]] (begins 29 April 2017)<ref>{{cite web|url=http://www.routesonline.com/news/38/airlineroute/267804/jet2com-adds-birmingham-routes-in-s17/|title=Jet2.com Adds Birmingham Routes in S17|publisher=routesonline|accessdate=7 July 2016}}</ref> [[East Midlands Airport|East Midlands]], [[Edinburgh Airport|Edinburgh]], [[Glasgow Airport|Glasgow]], [[Leeds Bradford International Airport|Leeds/Bradford]], [[London Stansted Airport|London-Stansted]] (begins 28 April 2017),<ref>http://www.jet2.com/timetable</ref> [[Manchester Airport|Manchester]], [[Newcastle Airport|Newcastle upon Tyne]]
<!-- -->
| [[Orbest]] | '''Seasonal charter:''' [[Francisco de Sá Carneiro Airport|Porto]]<ref>[http://www.presstur.com/site/news.asp?news=50165 "Soltour announces charters to Costa Dourada"] ''(in portuguese)''.</ref>
<!-- -->
| [[Pobeda (airline)|Pobeda]] | '''Seasonal:''' [[Vnukovo International Airport|Moscow-Vnukovo]] (begins {{date|2017-6-3}})<ref>{{cite news|last1=Liu|first1=Jim|title=Pobeda plans Reus June 2017 launch|url=http://www.routesonline.com/news/38/airlineroute/272243/pobeda-plans-reus-june-2017-launch/|accessdate=7 April 2017|work=Routesonline|date=7 April 2017}}</ref>
<!-- -->
| [[Ryanair]] | [[Eindhoven Airport|Eindhoven]], [[London Stansted Airport|London–Stansted]] <br>'''Seasonal:''' [[Birmingham Airport|Birmingham]], [[Bristol Airport|Bristol]], [[Brussels South Charleroi Airport|Charleroi]], [[Cork Airport|Cork]], [[Dublin Airport|Dublin]], [[East Midlands Airport|East Midlands]], [[Frankfurt–Hahn Airport|Hahn]], [[Liverpool John Lennon Airport|Liverpool]], [[Glasgow Prestwick Airport|Prestwick]]
<!-- -->
| [[Thomas Cook Airlines]] | '''Seasonal charter:''' [[Aberdeen Airport|Aberdeen]], [[Belfast International Airport|Belfast–International]], [[Birmingham Airport|Birmingham]], [[Bristol Airport|Bristol]], [[Cardiff Airport|Cardiff]], [[Gatwick Airport|London-Gatwick]], [[Glasgow Airport|Glasgow]], [[London-Stansted]], [[Manchester Airport|Manchester]], [[Newcastle Airport|Newcastle upon Tyne]]
<!-- -->
| {{nowrap|[[Thomas Cook Airlines Belgium]]}} | '''Seasonal charter:''' [[Brussels Airport|Brussels]]
<!-- -->
| [[Thomson Airways]] | '''Seasonal:''' [[Belfast International Airport|Belfast–International]], [[Birmingham Airport|Birmingham]], [[Bristol Airport|Bristol]], [[Cardiff Airport|Cardiff]], [[Robin Hood Airport Doncaster Sheffield|Doncaster/Sheffield]], [[Glasgow Airport|Glasgow]], [[Gatwick Airport|London–Gatwick]], [[London Luton Airport|London–Luton]], [[Manchester Airport|Manchester]], [[Newcastle Airport|Newcastle upon Tyne]]
<!-- -->
| [[Transavia]] |'''Seasonal:''' [[Amsterdam Airport Schiphol|Amsterdam]] (begins 15 April 2017)<ref>[http://www.luchtvaartnieuws.nl/nieuws/categorie/2/airlines/transavia-veel-nieuwe-europese-lijndiensten-in-2017 "Transavia: veel nieuwe Europese lijndiensten in 2017"] ''(in Dutch)''.</ref> <br> '''Seasonal charter:''' [[Amsterdam Airport Schiphol|Amsterdam]]
<!-- -->
| [[TUI fly Belgium]] | '''Seasonal:''' [[Brussels Airport|Brussels]]
<!-- -->
| [[Ural Airlines]] | '''Seasonal charter:''' [[Moscow Domodedovo Airport|Moscow–Domodedovo]]
<!-- -->
}}

==Statistics==
In 1995, approximately 500,000 passengers passed through the airport. In 2004 this number more than doubled to 1.1 million and in 2009 the airport reached a peak of 1.7 million. By 2014 this had dropped (-12.4%) to 850,492 passengers.<ref>[http://www.aena.es/csee/ccurl/728/306/Definitivo%202014.pdf Informes Anuales - 2014] - AENA</ref> In 2015 the drop continued to 705,067 passengers (-17,1%).<ref name="stats15"/>

==Incidents and accidents==
* On 20 July 1970, a Condor Boeing 737-100 (registered D-ABEL) which was approaching Reus Airport, collided with a privately owned Piper Cherokee light aircraft (registration EC-BRU) near Tarragona, Spain. The Piper subsequently crashed, resulting in the death of the three persons on board. The Condor Boeing suffered only minor damage, and there were no injuries amongst the 95 passengers and 5 crew members.
* In 1996, two powerful bombs placed by the [[Basque people|Basque]] group [[Euskadi ta Askatasuna]] (ETA) in the airport left more than 30 people injured. That same day two other bombs exploded in two hotels located near the airport.<ref name="terrorist attack">http://www.elmundo.es/papel/hemeroteca/1996/07/21/nacional/132433.html</ref>
* In 2004 a [[Swearingen Metro]] aircraft was to be repositioned to [[Barcelona Airport]] for maintenance work, but during takeoff, while accelerating 80 knots, the nose gear collapsed. The aircraft sustained serious damage to the fuselage, engines and the propellers.<ref name="Aviation Safety Network">http://aviation-safety.net/database/airport/airport.php?id=REU</ref>

==References==
{{reflist}}

==External links==
{{Commonscat-inline}}
* [http://www.aena.es/csee/Satellite/Aeropuerto-Reus/en/Home.html Official website]

{{Portalbar|Spain|Aviation}}
{{Airports in Spain}}

[[Category:Airports in Catalonia]]
[[Category:Province of Tarragona]]
[[Category:Airports established in 1935]]