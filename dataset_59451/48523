{{Use British English|date=August 2011}}
{{Use dmy dates|date=August 2011}}
{{Infobox professional wrestler
|name=Al Hobman
|image= Al Hobman.png
|caption=
|names='''Al Hobman'''
<!-- Please don't change the height or weight. These are the measures as officially stated and they should not be changed. -->
|height= {{height|m=1.85}}<ref name="KPW">{{cite web |url=http://www.kiwiprowrestling.co.nz/Al-Hobman.582.0.html |title=Al Hobman |date=September 2007 |work=Legends |publisher=KiwiProWrestling.co.nz |accessdate=26 July 2010}}</ref>
|weight= {{convert|110|kg|lb|abbr=on}}<ref name="KPW"/>
|birth_date = {{birth date|df=yes|1925|4|23}}
|death_date = {{death date and age|df=yes|2008|9|21|1925|4|23}}
|birth_place = New Zealand
|death_place =[[Wellington]], New Zealand
|resides=
|billed=
|trainer=
|debut=
|retired=
|website=
|}}
'''Allan "Al" Hobman''' (23 April 1925 – 21 September 2008) was a New Zealand [[professional wrestler]], [[Personal trainer|trainer]] and [[Promoter (entertainment)|promoter]]. Hobman was one of the first homegrown stars to emerge from the [[Dominion Wrestling Union]], and later [[Steve Rickard]]'s All Star-Pro Wrestling, during the 1960s and 70s such as [[Tony Garea]], [[Peter Maivia]] and [[The Bushwhackers|The Sheepherders]]. Hobman twice won the [[NWA New Zealand Heavyweight Championship]] from [[John Da Silva]] in 1960 and Steve Rickard in 1964 with a combined reign of nearly 6 years as champion. He and Rickard were also the first New Zealand Tag Team Champions.

Prior to wrestling, Hobman had a successful athletic career playing [[rugby union]] at [[Rongotai College]] as a [[lock (rugby union)|lock]] for Oriental Rongotai as well as a [[bodybuilder|professional bodybuilder]] winning both the Mr. Australasia and Mr. New Zealand bodybuilding competitions in 1951.

Hobman wrestled throughout the world during his career, most often in Asia and the South Pacific, but also had successful stints in the United States, most notably, against then [[NWA World Heavyweight Champion]] [[Gene Kiniski]] in 1969. He was a successful promoter both prior to and during his later career, mainly working with Da Silva's Central Wrestling Association, and trained a number of wrestlers with [[Bruno Bekkar]] at the famed [[Anton Koolmann|Koolmans Gym]] including Jock Ruddock and [[Butch Miller (wrestler)|Butch Miller]]; two of his four children, Kurt and Linda Hobman, both became wrestlers as well with the elder Hobman staging the country's first officially sanctioned [[Women's professional wrestling|women's wrestling match]] between his daughter and Dutch female wrestler Monica Schumaker.

He was among several New Zealand wrestling legends involved with [[Kiwi Pro Wrestling]], one of the first wrestling promotions to open since the close of All Star-Pro Wrestling in 1992, and inducted into KPW's New Zealand Wrestling Hall of Fame in 2006. He also attended live events made other public appearances until his sudden death two years later. In 2009, less than a year after his death, Hobman was ranked No.&nbsp;5 in a top ten list of New Zealand's greatest wrestlers by ''Fight Times Magazine''.

==Career==

===Early athletic and wrestling career===
A lifelong resident of [[Wellington]], Allan Hobman was active in sports and athletics at a young age and began playing [[rugby union]] at [[Rongotai College]] as a [[lock (rugby union)|lock]] for the Oriental Rongotai. Hobman weighed around 75&nbsp;kg at this time and suffered constant injuries going up against 100 – 130&nbsp;kg sized opponents. He was soon introduced to weightlifting by a friend of his and, with nutrition and training regimen, gradually built up his body-weight to around 110&nbsp;kg. In addition to improving his performance in rugby, Hobman also took up competitive bodybuilding and won both Mr. Australasia and Mr. New Zealand titles in 1951.<ref name="KPW"/><ref name="Rhodes">{{cite web |url=http://www.kiwiprowrestling.co.nz/News-Item.487.0.html?&tx_ttnews%5BpS%5D=1250196406&tx_ttnews%5Bpointer%5D=38&tx_ttnews%5Btt_news%5D=236&tx_ttnews%5BbackPid%5D=488&cHash=ceb0f229d176ec57790d49f15374c1e6 |title=Tribute to Al Hobman |author=Rhodes, Blair |date=28 October 2008 |work=News Item |publisher=KiwiProWrestling.co.nz |accessdate=26 July 2010}}</ref><ref name="Rongotai">{{cite web |url=http://www.rongotai.school.nz/resourcesmodule/download_resource/id/82/ |title=Alan (Al) Hobman |author=Rongotai College |authorlink=Rongotai College  |date=March 2009 |work=Obituaries |publisher=The Chariot |accessdate=26 July 2010| archiveurl= https://web.archive.org/web/20100619162032/http://www.rongotai.school.nz/resourcesmodule/download_resource/id/82/| archivedate= 19 June 2010 <!--DASHBot-->| deadurl= no}}</ref><ref name="Cameron">{{cite web |url=http://www.fighttimes.com/magazine/magazine.asp?article=1131 |title=My Top Ten New Zealand Born Wrestlers |author=Cameron, Dave |date=March 2009 |work=Fight Times|publisher=Fight Times Magazine|accessdate=8 July 2010}}</ref>

Hobman spent much of his training at [[Anton Koolmann|Koolman's Gym]] in Wellington where he also acted as a sparring partner to boxers and wrestlers from both New Zealand and overseas. It was during this time that Hobman became interested in a professional career as a wrestler, which he described as being claimed by its "call", and was wrestling for promoters throughout the country by the late-1950s. His debut match against Australian wrestler Rick Wallace ended with his losing [[Best 2 out of 3 Falls|two falls to one]] in the [[Wellington Town Hall]]. Wrestling for the [[Dominion Wrestling Union|New Zealand Wrestling Union]], he soon became a familiar face to New Zealand wrestling fans wrestling in nearly every town and city "from [[Kaitaia]] to [[Invercargill]]". Hobman was soon invited to his first overseas tour for Greek promoter George Gardiner in Australia followed by visits to the United States, Canada and the South Pacific; specifically the Cook Islands, Tahiti, Fiji, Malaysia and Singapore.<ref name="Rhodes"/> He was also brought over to Japan by Canadian and Commonwealth Champion, Gordon Nelson.<ref name="KPW"/>

===Headliner during the 1960s and 70s===
Hobman was not only as a formidable journeyman wrestler but was also a top draw in his native country with bouts at the Wellington Town Hall against The Great Zorro, [[Tony Kontellis|Tony "The Greek" Kontellis]], [[John Da Silva]], [[Pat O'Connor (wrestler)|Pat O'Connor]] and [[Bruno Bekkar]] among others.<ref name="Rongotai"/> One of his bouts against Becker in [[New Plymouth]] featured [[Lofty Blomfield]] as [[special guest referee]]. The proceeds from this match were donated to the [[IHC New Zealand|Intellectually Handicapped Children’s Society]] of which Blomfield was president.<ref name="KPW"/> On 26 October 1960, Hobman defeated Da Silva for the vacant [[NWA New Zealand Heavyweight Championship]].<ref>{{cite web |url=http://www.pwinsider.com/ViewArticle.php?id=34218&p=1 |title=This Day In History, Including A Detailed Look At Guerrero vs. Mysterio At Halloween Havoc |author=Woodward, Bob |date=26 October 2008 |publisher=PWInsider.com |accessdate=26 July 2010}}</ref> He won the title a second time from [[Steve Rickard]] in 1964 and held the belt for three years before losing it to Da Silva in Wellington on 7 September 1967; his combined title reigns totalled nearly six years.<ref name="Rhodes"/> He and Rickard also became the first New Zealand Tag Team Champions.<ref name="Cameron"/> On 22 April 1969, Hobman wrestled [[Gene Kiniski]] for the [[NWA World Heavyweight Championship]] in [[Portland, Oregon]].<ref>{{cite web |url=http://slam.canoe.ca/Slam/Wrestling/ResultsArchive/Wrestlers/kiniski.html |title=Gene Kiniski|last=Nevada|first=Vance|first2=J Michael|last2=Kenyon|first3=Matt|last3=Farmer|first4=Tim|last4=Hornbaker|first5=Don|last5=Luce|first6=Greg|last6=Oliver|first7=Chris|last7=Parsons|first8=Gary|last8=Will|first9=Jim|last9=Melby|first10=Mike|last10=Rodgers|first11=Scott|last11=Teal|first12=Robert|last12=Van Kavelaar|first13=Jim|last13=Zordani|date=30 April 2008 |work=Slam! Sports|publisher=[[Canadian Online Explorer]]|accessdate=26 July 2010}}</ref>

Much of his early career was spent in Rickard's NWA-affiliated All Star-Pro Wrestling, however, he also competed for smaller promotions such as South Pacific Wrestling Association and John Da Silva's Central Wrestling Association. Towards the end of the decade, he became a promoter for the CWA as well and trained a number of wrestlers out of [[Anton Koolmann|Koolmans Gym]] with Bruno Bekkar and [[George Kidd (wrestler)|George Kidd]]. Among his students were Jock Ruddock and [[Butch Miller (wrestler)|Butch Miller]]. Two of his four children, Kurt and Linda Hobman, also became professional wrestlers with Al Hobman promoting the first-ever [[Women's professional wrestling|women's wrestling match]] between his daughter, wrestling under the name Linda Tyson, and daughter Dutch professional wrestler Willen Schumaker, Monica Schumaker.<ref>{{cite web |url=http://www.amyaction.com/new_zealand_overview.html |title=New Zealand Women's Wrestling History |author=Action, Amy |work=History of Women's Wrestling |publisher=AmyAction.com |accessdate=26 July 2010}}</ref> Prior to 1973, female wrestling was illegal in New Zealand.<ref name="KPW"/><ref name="Rhodes"/> He also made appearances on the popular television programme ''[[On the Mat]]''.<ref name="Afi">[[Siva Afi|Afi, Siva]]. ''Reign of Fire: My Testimony from the Depths of Hell I Rose Into the Majesty of God Almighty''. Lulu.com (pg. 20) ISBN 1-4357-2653-7</ref>

===Retirement and later years===
Retiring by the 1980s, he remained absent from the industry for a quarter of a century until the opening of [[Kiwi Pro Wrestling]] in May 2006. It was one of the first promotions to open since the close of All Star-Pro Wrestling in 1992, one of three major companies to emerge during the 2000s, with Hobman as one of its strongest supporters. He appeared at many of its live events and was one of several New Zealand wrestling legends to be interviewed by KiwiProWrestling.co.nz. Hobman was also inducted into the New Zealand Wrestling Hall of Fame by former student and KPW Commissioner Butch Miller<ref name="Rhodes"/> on 8 December 2006, along with Bob Crozier, Bruno Bekkar and [[Rip Morgan]].<ref>{{cite web|url=http://www.oklafan.com/columns/473/article.html |title=Oklahoma Scoop |date=18 December 2006 |work=The Oklahoma Wrestling Fan's Resource Center |publisher=OklaFan.com |accessdate=26 July 2010 |deadurl=yes |archiveurl=https://web.archive.org/web/20110930225647/http://www.oklafan.com/columns/473/article.html |archivedate=30 September 2011 |df=dmy }}</ref> He and other veterans also worked with KPW's younger wrestlers<ref>{{cite web |url=http://www.caulifloweralleyclub.org/2010/09/21/state-of-play-professional-wrestling-in-new-zealand/ |title=State of Play: Professional Wrestling in New Zealand |author=Ogilvie, Steve |date=21 September 2010 |publisher=[[Cauliflower Alley Club]] |accessdate=26 July 2010}}</ref> and remained closely associated with the promotion until dying unexpectledly at his home on 21 September 2008.<ref name="Rongotai"/>

A month after his death, Kurt Hobman, Steve Rickard, Bruno Bekkar, Juno Huia, Cowboy Billy Wright, Ricky Wallace and Bob Crozier appeared at the KPW supercard "Halloween Howl 3" held in [[Wellington High School (New Zealand)|Wellington High School]] to pay tribute to Hobman's death; the main event between [[KPW Heavyweight Championship|KPW Champion]] H-Fame and Inferno was dedicated to Hobman's memory.<ref>{{cite web |url=http://www.nzpwi.co.nz/index.php?option=com_content&task=view&id=4566&Itemid=1 |title=KPW Halloween Howl 3: H-Flame vs. Inferno |author=Dunn, David |date=30 October 2008 |work=Stories |publisher=NZPWI.co.nz |accessdate=26 July 2010}}</ref> In a statement from KPW's website, ''[[Off the Ropes]]'' [[play by play|play by play announcer]] Blair "The Flair" Rhodes wrote ''"Mr. Hobman was renowned not only as an exemplary sportsman in the ring, but was also remembered amongst all his peers as a gentleman outside the ring. Being such a man of real character and bearing, Mr Hobman was a true ambassador for the business of professional wrestling"''.<ref name="Rhodes"/> Five months later, he was ranked No.&nbsp;5 in a top ten list of New Zealand's greatest wrestlers by ''Fight Times Magazine''.<ref name="Cameron"/>

==Championships and accomplishments==

===Bodybuilding===
*Mr. Australasia (1951)
*Mr. New Zealand (1951)

===Professional wrestling===
*'''[[Kiwi Pro Wrestling]]'''
**New Zealand Wrestling Hall of Fame (Class of 2006)
*'''[[List of NWA territories|NWA New Zealand]]'''
**[[NWA New Zealand Heavyweight Championship]] (2 times)<ref name="Wrestling-Titles">{{cite web |url=http://www.wrestling-titles.com/nz/nz-h.html |title=New Zealand Heavyweight Title |year=2003 |work=Wrestling-Titles.com |publisher=Puroresu Dojo |accessdate=30 May 2010}}</ref>
**NWA New Zealand Tag Team Championship (1 time, first) – with [[Steve Rickard]]<ref name="Cameron"/>

==References==
{{reflist|2}}

==External links==
*[http://www.genickbruch.com/index.php?befehl=bios&wrestler=13803 Al Hobman at Genickbruch.com]

{{DEFAULTSORT:Hobman, Al}}
[[Category:1925 births]]
[[Category:2008 deaths]]
[[Category:New Zealand male professional wrestlers]]
[[Category:New Zealand rugby union players]]
[[Category:Professional bodybuilders]]
[[Category:People from Wellington City]]
[[Category:People educated at Rongotai College]]