'''Chris Huggett''' is a British  engineer and designer who co-founded [[Electronic Dream Plant]] (EDP), founded [[Oxford Synthesizer Company]] and who is currently design consultant for [[Novation Digital Music Systems]], all manufacturers of audio synthesizers.

== Electronic Dream Plant ==
[[Image:Electronic Dream Plant Wasp Synthesizer.jpg|thumb|160px|[[Electronic Dream Plant|EDP]] WASP]]
{{main|Electronic Dream Plant}}

In 1977, Huggett had been working for Ferrograph, for 3M in their digital multi-track division, and as a freelance studio maintenance engineer. He met up with synthesist Adrian Wagner (a descendant of the German composer [[Richard Wagner]]), who had ideas for an inexpensive synthesizer.<ref name="Mark Vail 1993, p. 54">{{harvnb|Vail|1993|p=54}} &nbsp; (see also {{harvnb|Vail|2000|p=[https://books.google.com/books?id=tNci9y0jlRgC&dq=Chris%20Huggett&lpg=PP1&pg=PA63#v=onepage&f=false 63]}})</ref> Electronic Dream Plant (commonly abbreviated to EDP), a [[United Kingdom|British]] [[sound synthesizer]] manufacturing firm in [[Oxfordshire]] was formed.<ref name="colbeck1996p19">{{harvnb|Colbeck|1996|p=19}}</ref>

Huggett designed EDP's most successful product, the Wasp, a synthesizer with a hybrid digital VCO / analog VCF design. Employing a unique contact keyboard (with no moving parts), the Wasp was priced at  £199, which was less than half the price of any comparable synth at the time<ref name="Mark Vail 1993, p. 54"/> Huggett later designed the Spider sequencer and the Gnat synthesizer<ref name=SOS_Feb1995>
{{cite journal
 | author    = Chris Carter
 | title     = EDP Wasp
 | url       = http://www.soundonsound.com/sos/1995_articles/feb95/edpwasp.html
 | journal   = Sound On Sound
 | issue     = February 1995
}}
</ref> before EDP's demise in the end of 1981<ref name=vail2000p64>{{harvnb|Vail|2000|p=[https://books.google.com/books?id=tNci9y0jlRgC&dq=Chris%20Huggett&lpg=PP1&pg=PA64#v=onepage&f=false 64]}}: "''... However, neither company lasted very long. By the end of 1981, production of Wasps, Spiders, Gnats, and Caterpillars had stopped. ...''"</ref> or 1982.

== Oxford Synthesizer Company ==
[[Image:OSC OSCar.jpg|thumb|160px|[[OSC OSCar]]]]
{{main|OSC OSCar}}

After EDP, Huggett went on to form Oxford Synthesizer Company (OSC) with financing and management from his parents.<ref name=vail1993p56>{{harvnb|Vail|1993|p=56}} &nbsp; (see also {{harvnb|Vail|2000|p=[https://books.google.com/books?id=tNci9y0jlRgC&dq=Chris%20Huggett&lpg=PP1&pg=PA64#v=onepage&f=false 64]}}: "''In 1883, Huggett reemerged with the OSCar, produced by the Oxford Synthesizer Company, which was financed and run by Huggett's parents.''")</ref> He designed the [[OSC OSCar]] with Paul Wiffen and Anthony Harris-Griffin.<ref name=colbeck1996p92>{{harvnb|Colbeck|1996|p=92}}</ref> The OSCar was intended to be an affordable yet sophisticated performance synthesizer with state-of-the-art sounds. The OSCar was a more substantial synthesizer than the Wasp, with dual oscillators and a full-size three-octave keyboard. The OSCar was also one of the first programmable synthesizers, and included both an [[arpegiator|arpeggiator]] and a [[step sequencer]].<ref>
{{cite web
 | author    = Mark Cottle
 | title     = Introduction
 | url       = http://www.airburst.co.uk/oscar/intro.htm
 | work      = [http://www.airburst.co.uk/oscar/ The OSCar Homepage] (airburst.co.uk)
}}</ref>

== Akai ==
[[Image:Akai S1000.jpg|thumb|160px|[[Akai S1000]]]]
{{main|Akai|Akai S1000}}

Huggett later moved on to Akai, where he wrote the operating system for the [[Akai S1000]] [[Sampler (musical instrument)|sampler]] alongside David Cockerell, who designed the hardware. Huggett remained at Akai for successive models of Akai's rackmount sampler line, including the S3200, whose operating system he completed in 1993.<ref name=vail1993p57>{{harvnb|Vail|1993|p=57}} &nbsp; (see also {{harvnb|Vail|2000|p=[https://books.google.com/books?id=tNci9y0jlRgC&dq=Chris%20Huggett&lpg=PP1&pg=PA64#v=onepage&f=false 64]}}: "''After working as a freelance programmer for Akai for over a decade, during which he created operating systems for their samplers, Chris Huggett is now on the design staff for Novation.''")
</ref>

== Novation Digital Music Systems ==
{{multiple image |align=right |direction=vertical |width=160
 |image1=Novation SuperNova II (rear).jpg|caption1=Supernova II (rear)
 |image2=Novation Nova.jpg               |caption2=Nova
 |image3=Novation X-Station 49.jpg       |caption3=X-Station 49
 |image4=Remote 25.jpg                   |caption4=Remote 25
 |image5=Novation Remote 25SL.jpg        |caption5=Remote 25 SL }}
{{main|Novation Digital Music Systems}}

While working for Akai, Huggett provided advice and support to Novation's founders, working on the development of the BassStation, which had the Wasp filter in it. He later joined Novation full time to design the [[Novation Supernova]].<ref name=SOS_Sep1999>
{{cite journal
 | author    = Paul Wiffen
 | title     = Oxford Synthesizer Company Oscar
 | url       = http://www.soundonsound.com/sos/sep99/articles/oscar.htm
 | journal   = Sound On Sound
 | issue     = September 1999
}}</ref> Huggett's involvement with Novation has continued through many of their hardware synths and MIDI controllers ever since, including the Nova family of synths and the ReMOTE & ReMOTE SL series of controllers.<ref name=Chris_Huggett_and_the_History_of_Novation>
{{cite web|date=2007-08-07 |title=Chris Huggett and the history of Novation |url=http://img3.musiciansfriend.com/dbase/pdf/spec/701053.pdf |format=PDF |work=Press Release |publisher=Novation |location=High Wycombe, UK |quote=''If you look back at the Novation range of synthes, controllers and modules, there is one man who has been involved almost from the start. That man is Chris Huggett and he has been a major design influence in the Supernova 1&2, Nova, A- and K-Station, KS range, ReMOTE 25/25 Audio, X-Station, and the latest ReMOTE SL controller range with its revolutionary Automap mode ... But Chris' career didn't start with Novation - far from it! ...'' |deadurl=yes |archiveurl=https://web.archive.org/web/20110718064120/http://img3.musiciansfriend.com/dbase/pdf/spec/701053.pdf |archivedate=July 18, 2011 }}
</ref>

== References ==
<!--- See http://en.wikipedia.org/wiki/Wikipedia:Footnotes on how to create references using <ref></ref> tags which will then appear here automatically -->

;Bibliography
*{{cite book |ref={{sfnref|Vail|1993}}
 | author1   = Paul Wiffen
 | author2   = Mark Vail
 | title     = Vintage Synthesizers
 | publisher = <!-- GPI Books Division -->, [[Miller Freeman, Inc.|Miller Freeman]], 1993
 | <!-- publication-date = 1993 -->
 | isbn      = 978-0-87930-275-7
 }}
*{{cite book |ref={{sfnref|Vail|2000}}
 | author    = Mark Vail
 | title     = Vintage Synthesizers
 | url       = https://books.google.com/books?id=tNci9y0jlRgC
 | publisher = Backbeat Books, 2000
 | <!-- publication-date = 2000 -->
 | isbn      = 978-0-87930-603-8
 }}
*{{cite book |ref={{sfnref|Colbeck|1996}}
 | author    = Julian Colbeck
 | title     = Keyfax Omnibus Edition
 | publisher = MixBooks, 1996
 | isbn      = 978-0-918371-08-9
 | <!-- publication-date = 1996 -->
}}

;Citations
{{Reflist}}

==See also==
* [[Electronic Dream Plant]]
* [[Akai|AKAI professional]]
* [[Novation Digital Music Systems]]

==External links==
{{Commons category|Chris Huggett}}
* {{cite web
   | title     = Electronic Musical Instrument 1870 - 1980
   | url       = http://www.keyboardmuseum.com/pre60/older.html
   | publisher = Keyboard Museum
   | archiveurl= https://web.archive.org/web/20101124181349/http://keyboardmuseum.com/pre60/older.html
   | archivedate = 2010-11-24
  }}
* [http://global.novationmusic.com/ Novation Official Website]

{{DEFAULTSORT:Huggett, Chris}}
[[Category:Living people]]
[[Category:Inventors of musical instruments]]