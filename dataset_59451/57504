{{l2l|date=March 2017}}The '''International Conference on Communications''' ('''ICC''') is an annual international [[academic conference]] organised by the [[Institute of Electrical and Electronics Engineers]]' [[IEEE Communications Society|Communications Society]]. The conference grew out of the [[Global Communications Conference]] (GLOBECOM) when, in 1965, the seventh GLOBECOM was sponsored by the Communications Society's predecessor as the "IEEE Communications Convention". The following year it adopted its current name and GLOBECOM was disbanded (it has since been revived).<ref name="history">{{cite web|url=http://www.comsoc.org/socstr/documents/chapmanual/cm_1_2.html |title=IEEE Communications Society — History |publisher=IEEE Communications Society |accessdate=2006-03-22 |deadurl=yes |archiveurl=https://web.archive.org/web/20050312124208/http://www.comsoc.org/socstr/documents/chapmanual/cm_1_2.html |archivedate=March 12, 2005 }}</ref> The conference was always held in the [[United States]] until 1984 when it was held in [[Amsterdam]];<ref name="history" /> it has since been held in several other countries.<ref>{{cite web|url=http://www.comsoc.org/confs/icc/index.html|title=ICC|publisher=IEEE Communications Society|accessdate=2006-03-22}}</ref>

Some major telecommunications discoveries have been announced at ICC, such as the invention of [[turbo code]]s.<ref>{{cite journal|title=Near Shannon limit error-correcting coding: turbo codes|author1=Berrou, C. |author2=Glavieux, A. |author3=Thitimajshima, P. |journal=Proc. IEEE International Conference on Communications|date=May 1993|pages=1064–1070|volume=2|doi=10.1109/ICC.1993.397441|chapter=Near Shannon limit error-correcting coding and decoding: Turbo-codes. 1|isbn=0-7803-0950-2}}</ref> In fact, this ground breaking paper had been submitted to ICC the previous year, but was rejected by the referees who thought the results too good to be true.<ref>{{cite journal|title=Turbo-codes: the ultimate error control codes? (In particular, Section 2, pg. 156)|url=http://ieeexplore.ieee.org/search/wrapper.jsp?arnumber=945440|author=Alister Burr|journal=IEE Electronics & Communication Engineering Journal|date=August 2001|volume=13|issue=4|pages=155–165|issn=0954-0695}}</ref>

Recent ICCs have been attended by 1200–1400 people.<ref>{{cite web|url=http://webapps1.ieee.org/conferenceSearch/details.do?tagNo=8845|title=ICC 2004 details|publisher=IEEE|accessdate=2006-03-22}}</ref><ref>{{cite web|url=http://webapps1.ieee.org/conferenceSearch/details.do?tagNo=9490|title=ICC 2005 details|publisher=IEEE|accessdate=2006-03-22}}</ref>

The program of a typical ICC features major Symposia, Industry Forums, Workshops and Tutorials. Technical sessions are typically held from Tuesday-Thursday, while special sessions, workshops, tutorials etc. are held Sundays, Mondays, and Fridays.

== History of the Conference ==

{|class="wikitable" style="font-size: 100%" align="center" width:"100%"
!colspan=8|History of the ICC conference
|-
! width="40"| Year !! width="150"|City !! width="200"|Country !! width="225"|Date 
|-
| 2020 || [[Dublin]] || Ireland || 7-11 June
|-
| 2019 || [[Shanghai]] || China || 20-24 May
|-
| 2018 || [[Kansas City]] || United States || 20-24 May
|-
| 2017 || [[Paris]] || France || 21–25 May 
|-
| [http://icc2016.ieee-icc.org/ 2016] || [[Kuala Lumpur]] || Malaysia  || 23–27 May
|-
| [http://icc2015.ieee-icc.org/ 2015] || [[London]] || United Kingdom  || 8–12 June
|-
| [http://icc2014.ieee-icc.org/ 2014] || [[Sydney]] || Australia || 10–14 June
|-
| [http://icc2013.ieee-icc.org/ 2013] || [[Budapest]] || Hungary || 9–13 June
|-
| [http://www.ieee-icc.org/2012 2012] || [[Ottawa]] || Canada || 10–15 June
|-
| 2011 || [[Kyoto]] || Japan || 5–9 June
|-
| 2010 || [[Cape Town]] || South Africa || 23–27 May
|-
| 2009 || [[Dresden]] || Germany || 14–18 June
|-
| 2008 || [[Beijing]] || China || 19–23 May 
|-
| 2007 || [[Glasgow]] || United Kingdom || 24–28 June
|-
| 2006 || [[Istanbul]] || Turkey || 11–15 June
|-
| 2005 || [[Seoul]] || Korea || 16–20 May
|-
| 2004 || [[Paris]] || France || 20–24 June
|-
| 2003 || [[Anchorage, Alaska]] || United States || 11–15 May
|-
| 2002 || [[New York City]] || United States || 28 April - 2 May 
|-
| 2001 || [[Helsinki]] || Finland || 11–14 June
|-
| 2000 || [[New Orleans]] || United States || 18–22 June
|-
| 1999 || [[Vancouver]] || Canada || 6–10 June
|-
| 1998 || [[Atlanta]] || United States || 7–11 June
|-
| 1997 || [[Montreal]] || Canada || 8–12 June
|-
| 1996 || [[Dallas]] || United States || 23–27 June
|-
| 1995 || [[Seattle]] || United States || 18–22 June
|-
| 1994 || [[New Orleans]] || United States || 1–5 May
|-
| 1993 || [[Geneva]] || Switzerland || 23–26 May
|-
| 1992 || [[Chicago]] || United States || 14–18 June
|-
| 1991 || [[Denver]] || United States || 23–26 June
|-
|}

== References ==
{{Reflist}}
{{IEEE conferences}}
[[Category:IEEE conferences]]
[[Category:Telecommunication conferences]]
[[Category:Computer networking conferences]]