{{Use dmy dates|date=August 2013}}
{{Infobox military person
| honorific_prefix  = Freiherr
| name              = Gottfried Freiherr von Banfield
| honorific_suffix  =
| native_name       =
| native_name_lang  =
| image         = Gottfried von Banfield.jpg
| caption       =
| birth_date    = 6 February 1890
|death_date= {{Death date and age|df=y|1986|9|23|1890|02|06}}
| birth_place   = [[Herceg Novi|Castelnuovo]], [[Austria-Hungary]]
| death_place   = [[Trieste]], Italy
| placeofburial =
| placeofburial_label =
| placeofburial_coordinates = <!-- {{Coord|LAT|LONG|display=inline,title}} -->
| nickname      =
| birth_name    =
| allegiance    ={{flag|Austria-Hungary}}
| branch        ={{Navy|Austria-Hungary}}
| serviceyears  = 1909–1918
| rank          =
| servicenumber =
| unit          =
| commands      =
| battles       =
*[[Montenegro]]
*[[Gulf of Trieste]] in the month of June 1915
| battles_label =
| awards        =
*[[File:Ord.MariaTeresa-CAV.png|20px]] [[Military Order of Maria Theresa]]
*[[Military Merit Medal (Austria-Hungary)|Large Military Merit Medal with Swords]]
*[[Iron Cross|Iron Cross First Class]]
| relations     = [[Raffaello de Banfield|Richard Banfield]] (Father)
| laterwork     =
| signature     =
| website       = <!-- {{URL|example.com}} -->
}}

'''Gottfried [[Freiherr]]<ref>{{German title Freiherr}}</ref> von Banfield''' (6 February 1890 – 23 September 1986) was the most successful [[Austro-Hungarian]] naval aeroplane [[Aviator|pilot]] in the [[First World War]].<ref>This article is translated from German Wikipedia, with additions from the French and Italian wikipedia.</ref> He was known as the 'Eagle of Trieste' and was the last person in history to wear the [[Military Order of Maria Theresa]]. He may have been the only [[flying ace]] who flew a flying boat to five or more victories.<ref name=aces83>Chant, Rolfe 2000, pp. 83–86.</ref>

==Family==
Of [[Normans|Norman]] origin, the Banfields were an Irish family in the 16th century. The ancestor Thomas Banfield, an officer in the [[British army]], while in [[Bavaria]] married an [[Austrians|Austrian]] noblewoman. He took part in the [[Crimean War]] and died after the taking of [[Sevastopol]]. His son Richard Banfield, born in [[Vienna]] in 1836 and educated in Austria, chose Austrian citizenship, became an officer of the [[Austro-Hungarian Navy|k.u.k. Kriegsmarine]] and took part in the [[Battle of Lissa (1866)|Battle of Lissa]] as one of the commanders on [[Wilhelm von Tegetthoff]]'s flagship, the ''Erzherzog Ferdinand Max''.<ref>This paragraph translated from Italian Wikipedia.</ref>{{citation needed|date=July 2013}}

==Early training==
Banfield was born 6 February 1890 in [[Herceg Novi|Castelnuovo]], which is situated in the [[Bay of Kotor|Bay of Cattaro]] and was the homeport of an Austrian fleet. His father was a British subject, but the boy Gottfried took Austrian nationality.{{citation needed|date=July 2013}}

He attended the Military secondary-school in [[Sankt Pölten]], and the Naval academy in [[Fiume]]: on 17 June 1909 he emerged as cadet. In May 1912 he was promoted to frigate-Lieutenant. One month later he began pilot training in the flying school in [[Wiener Neustadt]], and in August he obtained his flying licence.<ref name=aces83/> Enthused with aviation like his older brother, who had already become a well-known aviator, he was chosen to be among the first pilots of the Austrian navy, and went off to perfect his training at the [[Donnet-Lévèque]] pilot school in France, where his trainer was the company's chief pilot, the naval lieutenant [[Jean-Louis Conneau]], a pilot famous at the time for having won many air contests under the pseudonym of ''Beaumont''. On the [[Pula|Pola]] Naval Air base of Santa Caterina island he trained in [[seaplane]]s.{{citation needed|date=July 2013}} As a result of a forced landing in 1913 he broke a leg so badly that the foot was barely saved.<ref name=aces83/> He was not airborne again until the outbreak of war.{{citation needed|date=July 2013}}

==Wartime experience==

At the start of the [[First World War]], Banfield was posted to fly the Lohner flying boat E.21 allocated to the pre-dreadnought battleship [[SMS Zrínyi|SMS ''Zrínyi'']] for aerial reconnaissance.<ref name=aces83/> He took part in the first aerial actions against [[Montenegro]] from the [[Bay of Kotor|base of Cattaro]]. In the period following he worked as a test pilot and instructor at the airfield on the island of Santa Catarina off [[Pula|Pola]]. Once the Italians entered the war he was commissioned with building up a larger seaplane station near Trieste, and after its completion was named as its commanding officer.{{citation needed|date=July 2013}} He retained his command until the end of the war.<ref name=aces83/> He won his first air-battles in a [[Lohner L|Lohner]] biplane seaplane against the Italians and their French allies in the gulf of Trieste in the month of June 1915, downing a balloon on the 27th. Even coming up against his old teacher Jean-Louis Conneau (better known as [[André Beaumont]]) in September 1915. Experimenting with a monoplane seaplane early in 1916, he won many victories and for a time held first place among the Austrian aces. He was wounded in combat in 1918.{{citation needed|date=July 2013}}

==Decorations and military tally==
Banfield's 9 confirmed and 11 unconfirmed air-kills make him the most successful Austro-Hungarian naval airplane fighter, and he holds a place among the most successful flying aces of Austro-Hungary. It was because he made most of his expeditions over the northern [[Adriatic]], and therefore many of his attributed air-victories could not be confirmed, that accounts for his high tally of unconfirmed air-conquests.<ref name=aces83/> For his military services he was in 1916 decorated with the [[Military Merit Medal (Austria-Hungary)|Large Military Merit Medal with Swords]]. Founded on 1 April 1916, this honour was intended for the "highest especially praiseworthy recognition" and was awarded only 30 times. 28 of its recipients were officers of general's rank; the other two were the [[cryptologist]] [[Hermann Pokorny]] (1918) and Banfield himself. On 17 August 1917 Banfield was further honoured when he received the Military Order of Maria Theresa. Individuals who received the order and were not already members of the [[Austrian nobility]] were ennobled and received the hereditary title of 'Freiherr', meaning 'Baron' to their family name. At the time of his death in 1986, Freiherr von Banfield was the last living Knight of the Military Order of Maria Theresa.<ref>Bassett, Richard, For God and Kaiser: The Imperial Austrian Army, 1619-1918, Yale University Press, 2015.</ref>

==Il Barone at Trieste==
After the First World War, the city of Trieste was annexed by Italy, and Freiherr von Banfield was for a time imprisoned by the occupation police. In 1920 he emigrated to England and became a British subject. He married the Contessa Maria Tripcovich of Trieste (d. 1976). They settled in [[Newcastle-upon-Tyne]], where their son Raphael Douglas, known to the world as the composer [[Raffaello de Banfield|Raffaello de Banfield Tripcovich]], was born in 1922. In 1926, Gottfried took Italian nationality and returned to Trieste to become Director of the Diodato Tripcovich and Co. Trieste Shipping-Company, which he took over from his father-in-law. Trieste Company ships then sailed under the Italian flag. Banfield became a celebrity of the city, usually called "Our Baron", ''Il nostro Barone'',{{citation needed|date=July 2013}} even winning a local tennis championship in 1927.<ref>Triestino Tennis Club website [http://www.tctriestino.com/tennis/clubstoria.php] Retrieved 29 July 2013.</ref> Serving as the Honorary Consul of France at Trieste, he was decorated with the [[Legion d'Honneur]] in 1977. Freiherr von Banfield died in Trieste 23 September 1986, at the age of 96.<ref>Bassett, Richard, ''For God and Kaiser: The Imperial Austrian Army'', 1619-1918, Yale University Press, 2015.</ref>

==A military tribute==
As a memorial the 1990 graduating year's class of the Theresa Military academy in Wiener-Neustadt, the greater number of whom had begun their foundation military service in the year of Freiherr von Banfield's death, called itself the 'Banfield Class.'{{citation needed|date=July 2013}}

==See also==

*[[Austro-Hungarian Imperial and Royal Aviation Troops|k. u. k. Luftfahrtruppen]]
*[[List of World War I flying aces]]

==Notes==
{{German title Freiherr}}

==References==
;Notes
{{Reflist}}
;Bibliography
{{refbegin}}
* Christopher Chant, Mark Rolfe: Austro Hungarian Aces of World War I  (Osprey Aircraft of the Aces), 2002.
* Martin O'Connor: Air Aces of the Austro-Hungarian Empire, 1914–1918, Flying Machines Press,US. 2000.
{{refend}}

==External links==
* [http://www.theaerodrome.com/aces/austrhun/banfield.php Short biography in English]

{{Authority control}}

{{DEFAULTSORT:Banfield, Gottfried Von}}
[[Category:1890 births]]
[[Category:1986 deaths]]
[[Category:Austro-Hungarian World War I flying aces]]
[[Category:Austro-Hungarian Navy officers]]
[[Category:Italian male tennis players]]
[[Category:People from Herceg Novi]]
[[Category:Barons of Austria]]
[[Category:Recipients of the Iron Cross (1914), 1st class]]
[[Category:Italian people of Austrian descent]]
[[Category:Austrian people of British descent]]