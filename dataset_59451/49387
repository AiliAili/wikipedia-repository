{{Use dmy dates|date=February 2017}}
{{Use British English|date=February 2017}}
{{Infobox person
| name        = Edwin G Lucas
| image       = Self Portrait by Edwin G Lucas, 1948.JPG
| image_size  = 150px
| caption     = Self Portrait, 1948
| birth_date  = 30 March 1911
| birth_place = [[Leith]]
| death_date  = 9 December 1990
| death_place = [[Edinburgh]]
}}

'''Edwin G Lucas''' (30 March 1911 – 9 December 1990) was a Scottish [[Modernist]] artist. He was an amateur, self-taught apart from evening classes at [[Edinburgh College of Art]], but during the period 1939-1952 he produced [[Surrealist]] works that are said to be unprecedented in Scottish art of the period.<ref name="Herald">{{cite web | accessdate = 2014-08-23 | work=[[The Herald (Glasgow)]] | last=Miller | first=Phil | title=Surrealist's hidden treasures brought to light at gallery | url = http://www.heraldscotland.com/news/home-news/surrealists-hidden-treasures-brought-to-light-at-gallery.22432612 | date=2013-10-16 }}</ref>

His work attracted little attention from the art world at the time, but it started receiving recognition following its "discovery" in 2013 by curators of the [[Scottish National Gallery of Modern Art]].

==Early career==

[[File:Snuff Mill, Juniper Green by Edwin G Lucas, 1936.JPG|thumb|Snuff Mill, Juniper Green, 1936]]

Edwin George Lucas was born in [[Leith]] in 1911, and grew up in [[Juniper Green]], a village on the outskirts of [[Edinburgh]]. He was educated at Juniper Green Primary School and then [[George Heriot's School]].<ref name="EENews">{{cite web| accessdate = 2014-08-23|work=[[Edinburgh Evening News]] |title=Recognition for artist who gave it up for day job |url = http://www.edinburghnews.scotsman.com/news/recognition-for-artist-who-gave-it-up-for-day-job-1-3143499 |date=2013-10-16 }}</ref>

He was interested in a career in art, but his family discouraged this because his uncle, [[E G  Handel Lucas]], who is now a well regarded Victorian artist, struggled to make a decent living and lived the latter part of his life in poverty.<ref name="Herald" /> Instead, Edwin joined the Civil Service in Edinburgh and studied at [[Edinburgh University]], graduating as a Bachelor of Law in 1934.<ref name="EENews" />

Throughout the 1930s he painted conventional, well executed landscapes.<ref name="Telegraph">{{cite web | accessdate = 2014-08-23 | work=[[The Daily Telegraph]] | last=Gleadell | first=Colin | title=Art Sales: Surrealist discovered by his son | url = http://www.telegraph.co.uk/luxury/art/27776/art-sales-surrealist-discovered-by-his-son.html | date=2014-03-18 }}</ref> These were mainly watercolours, with a strong emphasis on the countryside close to the family home in Juniper Green. They were often accepted for exhibition by the [[Society of Scottish Artists]]<ref name="SSA">[[Society of Scottish Artists]], Annual Exhibition Catalogues, 1934, 1935, 1937, 1940, 1941, 1942, 1943, 1945.</ref> (SSA) and the [[Royal Scottish Academy]]<ref>[[Royal Scottish Academy]], Catalogue 1941.</ref>

One of these paintings attracted attention from the French journal ''La Revue Moderne des Arts et de la Vie''.<ref>La Revue Moderne des Arts et de la Vie, 38e Année No 9, 15 Mai 1938, p.25.</ref> Its review called him ''"artiste extrêmement cultivé...un artiste plein de claire simplicité"'' (a highly educated artist...a painter of great clarity and simplicity). It also commented on his ''"liberté d'esprit et sa souplesse de style"'' (freedom of spirit and stylistic versatility), attributes that were to become defining characteristics of his later work.

[[File:Diagnosis 1 by Edwin G Lucas, 1939.jpg|thumb|upright=0.6|Diagnosis 1, 1939, Lucas's first Surrealist work]]

==Influence of Surrealism==

Surrealist works were shown at a number of exhibitions in Edinburgh during the late 1930s. One example was the SSA exhibition in December 1937,<ref name="SSA" /> which included the first works by [[Salvador Dalí]], [[Max Ernst]] and [[Giorgio de Chirico]] to be shown in Edinburgh.<ref name="Scotsman1937">Society of Scottish Artists, An Exhibition of interest and Merit, [[The Scotsman]], 1937-12-11, p.15.</ref> Lucas would certainly have attended because one of his watercolours was also included. Another example was an exhibition at Gladstone's Land Galleries in June 1939 by a group of graduates of [[Edinburgh College of Art]], including [[William Gear]].<ref name="Scotsman1939">Abstract Art, Edinburgh Group Exhibition, [[The Scotsman]], 1939-06-14, p.14.</ref>

Gear and Lucas were both close friends of [[Wilhelmina Barns-Graham]], from whom Lucas started renting a studio in 1939.<ref name="Telegraph" /> Almost immediately he started painting Surrealist works, the first of which is dated August 1939, probably influenced by shows such as those mentioned above.<ref name="Herald" /> He would later refer to this period as a brief "flirtation with Surrealism"<ref name="Telegraph" /> but it had a great impact on his future career as he developed his own highly individual take on Surrealism and produced works that are said to be unprecedented in Scottish art of the period.<ref name="Herald" />

==Post-War work==
[[File:Walking the Dog by Edwin G Lucas, 1949.JPG|thumb|Walking the Dog, 1949]]

Lucas was a committed pacifist and as a [[conscientious objector]] he went away from Edinburgh to do hospital work during the war.<ref name="CapitalViewBook">{{cite book |last=Popiel |first=Alyssa Jean |date=2014 |title=A Capital View |location=Edinburgh |publisher=[[Birlinn (publisher)|Birlinn Ltd]] |page=175 |isbn=978-1-78027-196-5}}</ref> He returned to Edinburgh in 1944 and the immediate post-war years were a very creative and prolific time for him. He returned to work as a Civil Servant, in the Estate Duty Office, but also attended evening classes at [[Edinburgh College of Art]] and saw himself as a serious painter who had a day job.<ref name="Telegraph" />

Patrick Elliott, senior curator at the [[Scottish National Gallery of Modern Art]], described the purchase in 2013 of some of Lucas's works of this period as follows:
<blockquote>
They are impressive because they are inexplicable, I've not seen anything quite like them before in my 20 years at the Gallery of Modern Art: there's a bit of Picasso, but overall he's got nothing in common with anyone painting in Scotland at the time - or in fact anywhere else.<ref name="Herald" />
</blockquote>
However, at the time they were painted Lucas's innovative works had little appeal to the Edinburgh art world. Unlike his landscapes, they were generally not accepted for exhibition. Lucas proceeded to hold solo shows at the New Gallery in Shandwick Place, Edinburgh, in 1950 and 1951<ref name="Herald" /> but these received little attention from the art establishment.

==Later life==

Lucas married Marjorie Eileen McCulloch in 1952. They bought a house in Ann Street in [[Stockbridge, Edinburgh]] and had two sons, born in 1953 and 1957. He stopped painting after his marriage, except for a period in the 1980s. His painting was ended by deteriorating eyesight and he died of leukaemia in 1990.<ref name="Times">{{cite web | accessdate = 2014-08-23 | work=[[The Times]] | title=Stored 'lost' work goes on display | url = http://www.thetimes.co.uk/tto/news/uk/scotland/article3895792.ece | date=2013-10-16 }}</ref>

==Posthumous recognition==

In 2009 his family started renting gallery space in Edinburgh to hold occasional exhibitions, but his work remained almost completely unknown until a letter to the [[Scottish National Gallery of Modern Art]] led to five paintings being acquired. They were hung beside works by Picasso and Miró in the "New Acquisitions" exhibition from October 2013 to May 2014.<ref name="Telegraph" /><ref name="SNGMAExhibition">{{cite web| accessdate = 2014-08-23|work=[[Scottish National Gallery of Modern Art]] |title=New Acquisitions at the Scottish National Gallery of Modern Art |url = http://www.nationalgalleries.org/visit/modern-one/exhibitions/new-acquisitions-at-the-scottish-national-gallery-of-modern-art |date=2013-10-12 }}</ref>

A number of newspapers published stories about the discovery of this "lost Surrealist", including [[The Daily Telegraph]],<ref name="Telegraph" /> [[The Times]],<ref name="Times" /> [[The Herald (Glasgow)]]<ref name="Herald" /> and the [[Edinburgh Evening News]].<ref name="EENews" />

Since then the Edinburgh City Art Centre has shown some of Lucas's work in two exhibitions: "A Capital View - the Art of Edinburgh"<ref name="CapitalViewExhibition">{{cite web| accessdate = 2015-11-16| work=Edinburgh City Art Centre |title=A Capital View - the Art of Edinburgh |url = http://www.edinburghmuseums.org.uk/Venues/City-Art-Centre/Exhibitions/2014-15/A-Capital-View---the-Art-of-Edinburgh |date=2014-05-10}}</ref> and "Jagged Generation: William Gear’s Contemporaries and Influences".<ref name="JaggedGenerationExhibition">{{cite web| accessdate = 2015-11-16| work=Edinburgh City Art Centre |title=Jagged Generation: William Gear’s Contemporaries and Influences |url = http://www.edinburghmuseums.org.uk/Venues/City-Art-Centre/Exhibitions/2015-16/Gear-s-Contemporaries-and-Influences |date=2015-10-24}}</ref> Also, two exhibitions devoted to his work were held at the [[Fine Art Society]].<ref name="FASExhibition1">{{cite web| accessdate = 2015-11-16|work= [[Fine Art Society]], Edinburgh |title=Edwin Lucas: Surrealist (1911-1990) |url = http://www.fasedinburgh.com/exhibitions/2447/ |date=2014-03-27}}</ref><ref name="FASExhibition2">{{cite web| accessdate = 2015-11-16|work= [[Fine Art Society]], Edinburgh |title=EDWIN G LUCAS (1911-1990) |url = http://www.fasedinburgh.com/exhibitions/3021/ |date=2015-02-06}}</ref>

==References==
{{reflist}}

==External links==
* [http://www.EdwinGLucas.com www.EdwinGLucas.com including digital photos of many of Lucas's works]
* [http://www.nationalgalleries.org/collection/artists-a-z/l/artist/edwin-lucas Edwin Lucas works at the Scottish National Gallery of Modern Art]
* {{Art UK bio}}

{{DEFAULTSORT:Lucas, Edwin G}}
[[Category:1990 deaths]]
[[Category:1911 births]]
[[Category:People educated at George Heriot's School]]
[[Category:Surrealist artists]]
[[Category:Scottish artists]]
[[Category:Artists from Edinburgh]]
[[Category:20th-century Scottish painters]]
[[Category:Scottish male painters]]
[[Category:Alumni of the University of Edinburgh]]
[[Category:Scottish conscientious objectors]]
[[Category:Scottish pacifists]]