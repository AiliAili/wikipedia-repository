<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=D.6
 | image=Dunne D.6.png
 | caption=
}}{{Infobox Aircraft Type
 | type=[[Experimental aircraft|Experimental]] inherently stable [[monoplane]]
 | national origin=[[United Kingdom]]
 | manufacturer=The Blair Atholl Syndicate/ [[Short Brothers]]
 | designer=[[J. W. Dunne]]
 | first flight=''c''. June 1911
 | introduced=
 | retired=
 | status=
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=1
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Dunne D.6''' was one of [[J. W. Dunne]]'s swept wing tailless aircraft designed to have automatic stability, flying about 1911. It was a single seat, single engined [[pusher configuration|pusher]] monoplane and was developed into the '''Dunne D.7'''.

==Design and development==

Although J. W. Dunne is best known for his inherently stable tailless biplanes like the [[Dunne D.5|D.5]] and [[Dunne D.8|D.8]], he also developed a series of inherently stable monoplanes. He had submitted a monoplane design to the [[War Office]] in 1905 before he joined the [[School of Ballooning|Balloon/Aircraft Factory]] at [[Farnborough Airfield|Farnborough]], though they rejected it.<ref>{{Harvnb|Goodall|Tagg|2001|page=100}}</ref>  In 1907, employed by the Balloon Factory and encouraged by its head [[John Capper|Colonel John Capper]], Dunne is said to have built a parasol winged monoplane glider known as the '''Dunne-Capper glider'''.<ref name="glider">{{Harvnb|Goodall|Tagg|2001|page=101}}</ref>

All Dunne's tailless aircraft had swept wings with marked [[washout (aviation)|washout]] (reduction of [[angle of incidence (aerodynamics)|angle of incidence]]) at the tips. Since sweepback placed the tips well behind the centre of gravity, they provided longitudinal (pitch) stability in just the same way as a conventional tailplane, mounted at lower incidence than the wing.<ref>Tailless Trials</ref> Both the D.5 and the monoplanes had wing [[Camber (aerodynamics)|camber]] which increased outwards, though the wing profiles varied differently.<ref name="FlightD6"/> The biplanes gained yaw stability from the inherent design of the wing, which the monoplanes augmented using down-turned wingtips. The glider mounted this wing above a pair of transverse A-frames, linked by longitudinal members that supported the pilot's arms; he controlled the aircraft by moving his weight.<ref name="glider"/>

Several of Dunne's aircraft from his Balloon Factory period were tested at [[Blair Atholl]] in [[Scotland]] to maintain secrecy, and some historians suspect that the Dunne-Capper glider was one of these, although Walker's thoroughly-researched account makes no mention of it.,<ref name="walker">Percy Walker, ''Early Aviation at Farnborough, Volume 2: The First Aeroplanes'', Macdonald, 1974.</ref>  Piloted by Capper, these tests were not successful.<ref name="glider"/> Walker attributes these tests to the D.1 biplane glider.<ref name="walker" />

The proposed glider is said to have been built with the intention of adding an engine, but this was not done until early in 1911, when a 60&nbsp;hp (45&nbsp;kW) [[Green Engine Co|Green]] was added in a pusher configuration.<ref name="powered-glider"/>  Three supporting A-frames were now present and the wing had wingtip elevons for control, though because of the down-turned tips these also provided some rudder-like forces.  This version of the '''Dunne-Capper monoplane''' was tested at [[Larkhill]] on [[Salisbury Plain]] in January 1911 but failed to take off. The earlier undercarriage with two wheels on the rear A-frame and skids at the front was replaced by one with two pairs of wheels at the back and two more wheels at the front.<ref name="powered-glider">{{Harvnb|Goodall|Tagg|2001|page=102}}</ref>

The D.6, major parts of which were built by Short Brothers,<ref>{{Harvnb|Barnes|1989|page=506}}</ref> used a similar wing with a very different structure supporting it, the engine, pilot and undercarriage.  The wing was straight edged, tapering from a central chord of 6&nbsp;ft 3 in (1.91 m) to 5&nbsp;ft 0 in (1.52 m) at the tips.  The leading edge was swept at 35°. The A-frames with [[King post|kingposts]] on the centreline were replaced with a pair of rectangular frames which extended above and below the wings, linked at the bottom by two transverse members. These frames served as double kingposts from which each wing was wire braced above and below. A substantial undercarriage structure was mounted at the bottom of the frames, comprising a long pair of skids which extended from the pusher propeller line well forward beyond the nacelle and curving strongly upwards. Each skid was multiply braced to its frame and inwards to the nacelle; the pair were joined by a cross strut near the forward tip. Both carried a pair of wheels and, at the rear, an articulated and sprung extension to absorb landing shocks.<ref name="FlightD6">The Dunne Monoplane, 1911</ref><ref name="GTD6">{{Harvnb|Goodall|Tagg|2001|page=103}}</ref><ref name="PLD6">{{Harvnb|Lewis|1962|pages=224–6}}</ref>

The nacelle that carried the pilot's seat and the engine behind him was no more than an open wooden framework.  The same Green engine was used as before, driving a two bladed, 7&nbsp;ft 3 in (2.21 m) diameter propeller. A tall, rectangular radiator was placed longitudinally above the wing, positioned to raise the centre of gravity as high as possible.  A pair of levers, one for each hand, controlled the aircraft.<ref name="FlightD6"/><ref name="GTD6"/><ref name="PLD6"/>

This aircraft was tested at [[Eastchurch airfield]] on the [[Isle of Sheppey]] in June 1911, flown by Dunne.  Designated the '''Dunne D.7''' or '''D.7 ''Auto Safety'''''.  This was very similar to the D.6, but had a 1&nbsp;ft (305&nbsp;mm) shorter span and a 50&nbsp;hp (37&nbsp;kW) 7-cylinder [[Gnome rotary engine]]. It first appeared, not quite ready for flight, at the [[Olympia, London|Olympia]] Aero Show in March 1911 and was on test with the D.6 at Eastchurch  that June.<ref name="GTD6"/><ref name="PLD6"/> Dunne was pleased with the improved performance.<ref>From the British Flying Grounds</ref>  In January 1912 Dunne demonstrated the D.7 to members of the [[Royal Aeronautical Society]], writing a note whilst flying hands off at 60&nbsp;mph.<ref name="GTD6"/><ref name="PLD6"/>

During 1912 the D.6 was modified into a two seater, the '''Dunne D.7 bis''', with a 70&nbsp;hp (52&nbsp;kW) Gnome to cope with the increased load. It had the shorter span wing of the D.7, but a centrally mounted surface was added to improve pitch control.<ref>{{Harvnb|Goodall|Tagg|2001|page=103–4}}</ref><ref>The Dunne D.7 bis</ref>  This aircraft was successfully flown at [[Villacoublay]] in [[France]] by either Dunne or N. S. Percival in April 1913.<ref>The Dunne monoplane in France</ref>
<!-- ==Design== may be combined with above as "Design and development" -->
<!-- ==Operational history== -->
<!-- ==Variants== -->
<!-- ==Units using this aircraft/Operators (choose)== -->

==Specifications (D.6)==
{{Aircraft specs
|ref={{Harvnb|Goodall|Tagg|2001|page=103}}
|prime units?=imp<!-- imp or kts first for US aircraft, and UK aircraft pre-metrification, met(ric) first for all others. You MUST choose a format, or no specifications will show -->
<!--
        General characteristics
-->
|genhide=
|crew=1
|capacity=
|length m=
|length ft=21
|length in=0
|length note=
|span m=
|span ft=36
|span in=0
|span note=
|upper span m=
|upper span ft=
|upper span in=
|upper span note=
|mid span m=
|mid span ft=
|mid span in=
|mid span note=
|lower span m=
|lower span ft=
|lower span in=
|lower span note=
|swept m=<!-- swing-wings -->
|swept ft=<!-- swing-wings -->
|swept in=<!-- swing-wings -->
|swept note=
|dia m=<!-- airships etc -->
|dia ft=<!-- airships etc -->
|dia in=<!-- airships etc -->
|dia note=
|width m=<!-- if applicable -->
|width ft=<!-- if applicable -->
|width in=<!-- if applicable -->
|width note=
|height m=
|height ft=11
|height in=0
|height note=
|wing area sqm=
|wing area sqft=248
|wing area note=including elevons
|swept area sqm=<!-- swing-wings -->
|swept area sqft=<!-- swing-wings -->
|swept area note=
|volume m3=<!-- lighter-than-air -->
|volume ft3=<!-- lighter-than-air -->
|volume note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=
|empty weight note=
|gross weight kg=
|gross weight lb=
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|lift kg=<!-- lighter-than-air -->
|lift lb=<!-- lighter-than-air -->
|lift note=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Green]]
|eng1 type=water cooled inline
|eng1 kw=<!-- prop engines -->
|eng1 hp=60<!-- prop engines -->
|eng1 kn=<!-- jet/rocket engines -->
|eng1 lbf=<!-- jet/rocket engines -->
|eng1 note=
|power original=
|thrust original=
|eng1 kn-ab=<!-- afterburners -->
|eng1 lbf-ab=<!-- afterburners -->
|eng2 number=
|eng2 name=
|eng2 type=
|eng2 kw=<!-- prop engines -->
|eng2 hp=<!-- prop engines -->
|eng2 kn=<!-- jet/rocket engines -->
|eng2 lbf=<!-- jet/rocket engines -->
|eng2 note=
|eng2 kn-ab=<!-- afterburners -->
|eng2 lbf-ab=<!-- afterburners -->
|eng3 number=
|eng3 name=
|eng3 type=
|eng3 kw=<!-- prop engines -->
|eng3 hp=<!-- prop engines -->
|eng3 kn=<!-- jet/rocket engines -->
|eng3 lbf=<!-- jet/rocket engines -->
|eng3 note=
|eng3 kn-ab=<!-- afterburners -->
|eng3 lbf-ab=<!-- afterburners -->
|more power=
|prop blade number=2
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=7<!-- propeller aircraft -->
|prop dia in=3<!-- propeller aircraft -->
|prop note=
|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=Y
|max speed kmh=
|max speed mph=
|max speed kts=
|max speed note=
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|stall speed kmh=<!-- aerobatic -->
|stall speed mph=<!-- aerobatic -->
|stall speed kts=
|stall speed note=
|never exceed speed kmh=
|never exceed speed mph=
|never exceed speed kts=
|never exceed speed note=
|minimum control speed kmh=
|minimum control speed mph=
|minimum control speed kts=
|minimum control speed note=
|range km=
|range miles=
|range nmi=
|range note=
|combat range km=
|combat range miles=
|combat range nmi=
|combat range note=
|ferry range km=
|ferry range miles=
|ferry range nmi=
|ferry range note=
|endurance=<!-- if range unknown -->
|ceiling m=
|ceiling ft=
|ceiling note=
|g limits=<!-- aerobatic -->
|roll rate=<!-- aerobatic -->
|glide ratio=<!-- sailplanes -->
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=
|sink rate ms=<!-- sailplanes -->
|sink rate ftmin=<!-- sailplanes -->
|sink rate note=
|lift to drag=
|wing loading kg/m2
|wing loading lb/sqft=
|wing loading note=
|disk loading kg/m2=
|disk loading lb/sqft=
|disk loading note=
|power/mass=
|thrust/weight=
|more performance=
<!--
        Armament
-->
|armament=<!-- add (bulleted list) here or if you want to use the following specific parameters, remove this parameter-->
|guns= 
|bombs= 
|rockets= 
|missiles= 
|hardpoints=
|hardpoint capacity=
|hardpoint rockets=
|hardpoint missiles=
|hardpoint bombs=
|hardpoint other=
|other armament= 
|avionics=
}}

<!-- ==See also== -->
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
|lists=<!-- related lists -->
}}

==Notes==
{{reflist|2}}

==References==
{{commons category|Dunne aircraft}}
{{refbegin}}
*{{cite book |title= Shorts Aircraft since 1900|last1= Barnes|first1=C.H. |last2 =James |first2=D. N.|year=1989 |publisher=Putnam Publishing  |location=London |isbn= 0-87021-662-7|ref=harv}} 
*{{cite book |title= British Aircraft before the Great War|last=Goodall|first=Michael H.|last2=Tagg|first2=Albert E.|year=2001|publisher=Schiffer Publishing Ltd|location=Atglen, PA, USA|isbn=0-7643-1207-3|ref=harv}}
*{{cite book |title= British Aircraft 1809-1914|last=Lewis|first=Peter| year=1962|volume= |publisher=Putnam Publishing |location=London |isbn=|ref=harv}}
*{{cite magazine |last= Poulsen |first= C. M. |title=Tailless Trials |magazine=[[Flight International|Flight]] |pages=556–58 |url=http://www.flightglobal.com/PDFArchive/View/1943/1943%20-%201376.html |date= 27 May 1943  |number=1796 |volume=XLIII}}
*{{cite magazine |last= |first= |authorlink= |title= The Dunne Monoplane, 1911.|magazine= [[Flight International|Flight]]|volume= |issue=24 June 1911|pages=542–5 |id= |url= http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%200540.html |accessdate= |quote= }}
*{{cite magazine |last= |first= |authorlink= |title= From the British Flying Grounds.|magazine= [[Flight International|Flight]]|volume= |issue=30 December 1911|page=1140 |id= |url= http://www.flightglobal.com/pdfarchive/view/1911/1911%20-%201132.html |accessdate= |quote= }}
*{{cite magazine |last= |first= |authorlink= |title= The Dunne D.7 bis.|magazine= [[Flight International|Flight]]|volume= |issue=22 June 1912|page=563 |id= |url= http://www.flightglobal.com/pdfarchive/view/1912/1912%20-%200563.html |accessdate= |quote= }}
*{{cite magazine |last= |first= |authorlink= |title= The Dunne monoplane in France.|magazine= [[Flight International|Flight]]|volume= |issue=12 April 1913|page=419 |id= |url= http://www.flightglobal.com/pdfarchive/view/1913/1913%20-%200413.html |accessdate= |quote= }}
{{refend}}

{{Dunne aircraft}}

[[Category:British experimental aircraft 1910–1919]]
[[Category:Dunne aircraft|D.6]]
[[Category:Single-engine aircraft]]
[[Category:Pusher aircraft]]