{{Infobox journal
| title = Yale Journal on Regulation
| cover =
| editor = John Brinkerhoff, Ryan Yeh
| discipline = [[Law review]]
| abbreviation = Yal J. Regul.
| publisher = Yale Journal on Regulation
| country = United States
| frequency = Biannual
| history = 1983–present
| openaccess =
| impact =
| impact-year =
| website = http://www.yalejreg.com/
| ISSN = 0741-9457
| LCCN = 84646898
| OCLC = 10212254
}}
The '''''Yale Journal on Regulation''''' (''[[Bluebook]]'' abbreviation: '' Yale J. on Reg.'') is a biannual student-edited [[law review]] covering [[regulatory law|regulatory]] and [[administrative law]] published at [[Yale Law School]]. The journal publishes articles, essays, notes, and commentaries that cover a wide range of topics in regulatory, [[corporate law|corporate]], administrative, [[international law|international]], and [[comparative law]]. According to the 2015 Washington and Lee University law journal rankings, the journal is ranked first in Administrative Law, in Corporations and Associations,  in Bankruptcy, in Commercial Law, in Communications Law, Media and Journalism, and in Health, Medicine, Psychology and Psychiatry.<ref>{{cite web |url=http://lawlib.wlu.edu/LJ/ |title=Law Journals: Submissions and Ranking |date=2016-04-08 |publisher=Washington and Lee University School of Law |accessdate=2016-06-13}}</ref> The 2007 ''ExpressO'' Guide to Top Law Reviews ranked the journal first among business law reviews based on the number of manuscripts received.<ref>{{cite web |url=http://law.bepress.com/expresso/2007/subject.html |title=2007 ExpressO Law Review Submissions Guide |publisher=[[The Berkeley Electronic Press]] |date= |accessdate=2012-10-28}}</ref>

==History==
The journal was established in 1983 by Mark Goldberg and [[Bruce Judson]].<ref>{{cite web |last=Judson |first=Bruce |title=Remarks at the Twenty-Fifth Anniversary of the Yale Journal on Regulation |journal=Yale Journal on Regulation |volume=25 |pages=331 |date=2008 |url=http://heinonline.org/HOL/LandingPage?handle=hein.journals/yjor25&div=21&id=&page=}}</ref> It has featured symposia and special issues on [[environmental law]], [[federalism]], and [[telecommunications]]. In 2009, it was a sponsor of the [[Weil, Gotshal & Manges]] Roundtable on the "Future of Financial Regulation," where legal academics and panelists evaluated the causes of the subprime mortgage crisis and proposed solutions.

In 2008, the journal launched the Walton H. Hamilton Prize (in honor of the former Yale Law professor, [[New Deal]] economic advisor, and antitrust division official [[Walton Hale Hamilton]]), awarded to the most outstanding accepted manuscript on the study and understanding of regulatory policy.

== Notable alumni ==
* [[Daniel C. Esty]] – Commissioner of the [[Connecticut Department of Energy and Environmental Protection]] and Professor at [[Yale Law School]]
* [[Elizabeth Esty]] – [[U.S. Representative]] for [[Connecticut's 5th congressional district]]
* [[Jack Goldsmith]] – Henry L. Shattuck Professor of Law at [[Harvard Law School]]
* [[David Huebner]] – [[United States Ambassador to New Zealand|United States Ambassador]] to [[New Zealand]] and [[Samoa]]<ref name=confirmation>{{cite web |url=http://www.senate.gov/pagelayout/legislative/one_item_and_teasers/nom_confc.htm |title=Nominations Confirmed (Civilian) | date=November 20, 2009 | work=[[United States Senate]] |publisher=[[United States Senate]] |accessdate=November 21, 2009}}</ref>
*[[Bruce Judson]] – author and media innovator
* Claire Priest – Professor of Law at [[Yale Law School]]<ref>{{cite web |url=http://www.law.yale.edu/faculty/CPriest.htm |title=Claire Priest &#124; Yale Law School |publisher=Yale University School of Law |date= |accessdate=2013-05-11}}</ref>
* [[Richard J. Sullivan]] – [[United States District Judge]] for the [[Southern District of New York]]
* [[Bryan Townsend (American politician)|Bryan Townsend]] – [[Delaware]] [[State Senator]]
* [[Kevin K. Washburn]] – Assistant Secretary, [[Bureau of Indian Affairs]]

==References==
{{Reflist|30em}}

==External links==
*{{Official website|http://www.yalejreg.com}}
* [http://www.yalejreg.com/blog Notice and Comment Blog]
* [http://www.law.yale.edu/news/8595.htm Yale Law School, "Corporate Law Center Celebrates 10th Anniversary With Financial Regulation Roundtable Feb. 13"]

{{DEFAULTSORT:Yale Journal On Regulation}}
[[Category:American law journals]]
[[Category:Yale Law School]]
[[Category:English-language journals]]
[[Category:Biannual journals]]
[[Category:Publications established in 1983]]
[[Category:Law journals edited by students]]