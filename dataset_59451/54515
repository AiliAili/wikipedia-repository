{{EngvarB|date=September 2013}}
{{Use dmy dates|date=September 2013}}
{{multiple issues|section=|
{{all plot|date=February 2016}}
{{no footnotes|date=February 2016}}
}}
{{Infobox book | <!-- See [[Wikipedia:WikiProject Novels]] or [[Wikipedia:WikiProject Books]] -->
| name          = Auriol: or, The Elixir of Life
| title_orig    =
| translator    =
| image         = <!--prefer 1st edition-->
| caption =
| author        = [[William Harrison Ainsworth]]
| illustrator   =
| cover_artist  =
| country       = United Kingdom
| language      = English
| series        =
| genre         = Historical novel
| publisher     = [[Routledge]]
| release_date  = 1844
| english_release_date =
| media_type    = Print (hardback)
| pages         = 246 pp
| preceded_by   =
| followed_by   =
}}

'''''Auriol: or, The Elixir of Life''''' is a novel by British historical novelist [[William Harrison Ainsworth]]. It was first published in 1844 in serial form, under the title ''Revelations of London''.

Auriol, written 1844, is slightly unusual in the Ainsworth repertoire as the action is entirely couched as a fantasy, so that the supernatural element (which occurs also, for instance, in his ''Guy Fawkes'' and his ''Windsor Castle'') can take comparatively free rein. The story is accordingly a thoroughly gothic romance. It is in effect Ainsworth's contribution to the [[Faust]] genre. There is also a distinct connection with [[The Cabinet of Dr. Caligari]], in the kidnapping of girls theme, and in that the story concludes in the atmosphere of the lunatic's confinement (and possible recovery), and the villain of the story is his keeper. Indeed, the use of the phantasmagorical aspects of the story to create a nightmarish commentary on contemporary society of the 1830s and 1840s anticipates (in the early 19th century) the [[expressionism]] of [[Robert Wiene]]'s Caligari. German interest in English literature of this period is also suggested in the works of [[Edward Bulwer-Lytton]] (''[[Rienzi]]'' and ''[[The Coming Race]]''). Similarly it was [[John Gay]] and Dr Pepusch who provided the source-structure in ''[[The Beggar's Opera]]'' for [[Bertolt Brecht]]'s ''[[Threepenny Opera]]''. There is an excellent and characteristic series of illustrations by [[Hablot Knight Browne|'Phiz']].

== Plot ==
'''Prologue (1599)''': Auriol Darcy is surprised attempting to remove the heads of two traitors from the [[Southwark]] Gateway of [[Old London Bridge]]. He is injured by the warder, Baldred, and carried to the house of Dr Lamb, an alchemist and Auriol Darcy's grandfather, who is assisted by his faithful dwarf Flapdragon. Lamb, on the point of discovering the [[elixir of life]], has a seizure and dies as his ungrateful grandson consumes the draught.

'''Book the first 'Ebba' (1830)''': Two varmints, Tinker and Sandman, waylay a gentleman in a fantastical ruined house in the [[Vauxhall]] Bridge Road in London, but they are surprised and he is carried unconscious to the house of Mr Thorneycroft, a scrap-iron dealer. While he convalesces and falls in love with Ebba, the iron-dealer's daughter, Tinker and Sandman and their associate Ginger (a 'dog-fancier' who steals dogs and resells them) discover in the gentleman's pocket-book the private diary of a man who has lived for over two hundred years, and has committed nameless crimes. Auriol (for it is he) seeks to dissuade Ebba from her love, for he bears an awful doom. A tall sinister stranger has Auriol in his power, and employs a dwarf (who is Flapdragon) to recover the pocket-book. The stranger confronts Auriol and informs his that Ebba must be surrendered to him according to their contract. Auriol refuses, but Ebba is snatched from him, and he is imprisoned, during a nocturnal assignation at a picturesque ruin near Millbank Street. Tinker, Sandman and Ginger offer their services to Mr Thorneycroft to attempt her rescue. Ebba is conveyed to a mysterious darkened chamber where the stranger demands that she sign a scroll surrendering herself body and soul to him. She calls to heaven for protection: in the darkness a tomb is revealed and opened by menacing cowled figures, and Auriol is brought forth. Ebba hurls herself into the tomb to precede him and save him, but then re-emerges silent and cowled to sign the scroll.
[[Image:Phiz Auriol.jpg|thumb|right|250px|[[Phiz]] illustration: Mr Thorneycroft, Sandman and Tinker in the enchanted chairs, observed by Ginger.]]
'''Intermean (1800)''': Cyprian Rougemont visits a deserted mansion at [[Stepney]] Green, where he finds the portrait of his ancestor (of the same name), a [[Rosicrucian]] brother of the 16th century, one of the [[Illuminati]]. [[Satan]] has appeared to him in a dream and promised him an ancestral treasure, the price for which is his own soul, or that of Auriol Darcy. Cyprian strikes the portrait and a plaque falls away, revealing the access to the ancestral tomb. There in a seven-sided vault lit by the ever-burning lamp and painted with kabbalistic symbols he finds the uncorrupt body with a book of mysteries, a vial of infernal potion, and a series of chests filled with gold, silver and jewels. With use of the potion, he lures Auriol into a compact whereby he is given a magnificent mansion in [[St James's Square]] and £120,000, in exchange for a female victim whenever Rougemont requires one from him. Thus Auriol can win the woman he loves, Elizabeth Talbot; but Rougemont, once the contract is signed, demands Elizabeth Talbot as his first victim, in a week's time. Auriol seeks to defy him and to marry her within the week, but he is thwarted and Elizabeth is abducted on the seventh night.

'''Book the Second, 'Cyprian Rougemont' (1830)''': Thorneycroft, Sandman and Tinker (with Ginger) continue their pursuit led by another, who is the brother of Rougemont's second victim, Clara Paston. They enter a mysterious mansion, and becoming trapped in a chamber and locked into enchanted or mechanically-contrived chairs three of them are muffled by bell-masks which descend from the ceiling, and then plunged through traps in the floor. Flapdragon appears and attempts to help them find Ebba, while Paston, Ginger and Thorneycroft find Rougemont and confront him with pistols, but Rougemont is impervious to the bullets. Thorneycroft, Tinker and Sandman are trapped in a pit over which an iron roof closes by a giant mechanical contrivance, and Ebba is never found again. Auriol, meanwhile, awakes to find himself in Elizabethan costume, chained in a vaulted dungeon. The voice of Rougemont addresses him, telling him that he has been mad, but that he has given him a potion to heal him, and is his keeper. [[James I of England|James I]] is now the King of England. Old Dr Lamb is still living, and his dwarf Flapdragon, and Auriol is taken to him, where they begin to hope that Auriol's cure has been effected. He becomes convinced that he has lived centuries in a few nights and has awakened from a delusion... but even in the last sentence, addressing Dr Lamb, the author relates what he says to his ''supposed'' grandsire.

==External links==
*[https://ebooks.adelaide.edu.au/a/ainsworth/william_harrison/auriol/ Online text of Auriol, also known as The Elixir Of Life]
* {{librivox book | title=Auriol | author=William Harrison AINSWORTH}}

==References==
*{{cite book | last=Bleiler | first=Everett | authorlink=Everett F. Bleiler | title=The Checklist of Fantastic Literature | location=Chicago | publisher=Shasta Publishers | year=1948 | page=19}}

{{William Harrison Ainsworth}}
{{Faust navbox}}

{{DEFAULTSORT:Auriol (Novel)}}
[[Category:1599 in fiction]]
[[Category:Novels by William Harrison Ainsworth]]
[[Category:1844 novels]]
[[Category:British historical novels]]
[[Category:Works based on the Faust legend]]
[[Category:Novels first published in serial form]]
[[Category:Novels set in London]]
[[Category:1800 in fiction]]
[[Category:1830 in fiction]]
[[Category:Novels set in the 1800s]]
[[Category:Novels set in the 1830s]]
[[Category:19th-century British novels]]