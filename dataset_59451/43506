<!-- This article is a part of [[Wikipedia:WikiProject Aircraft]]. Please see [[Wikipedia:WikiProject Aircraft/page content]] for recommended layout. -->
{|{{Infobox Aircraft Begin
 | name=C.E.1
 | image=
 | caption=
}}{{Infobox Aircraft Type
 | type=Patrol flying boat
 | national origin=[[United Kingdom]]
 | manufacturer=[[Royal Aircraft Factory]]
 | designer=William Farren
 | first flight=17 January 1918
 | introduced=
 | retired=
 | status=Prototype
 | primary user=
 | more users= <!--Limited to three in total; separate using <br /> -->
 | produced= <!--years in production-->
 | number built=2
 | program cost= <!--Total program cost-->
 | unit cost= <!--Incremental or flyaway cost for military or retail price for commercial aircraft-->
 | developed from= 
 | variants with their own articles=
}}
|}
The '''Royal Aircraft Factory C.E.1''' (Coastal Experimental 1) was a prototype [[United Kingdom|British]] [[flying boat]] of the [[First World War]]. It was a single-engined [[pusher configuration]] [[biplane]] intended to carry out coastal patrols to protect shipping against German [[U-boat]]s, but only two were built, the only flying boats to be designed and built by the [[Royal Aircraft Factory]].

==Design and development==

In February 1917, Germany restarted [[U-boat Campaign (World War I)#1917: Resumption of unrestricted submarine warfare|unrestricted submarine warfare]] against Britain, France and their allies, resulting in heavy losses to unescorted merchant shipping. There was a shortage of maritime patrol aircraft as the large [[Felixstowe F.2]] [[flying boat]]s had not yet entered large scale service, and the [[Royal Aircraft Factory]], despite the fact that most of its aircraft were intended for land service with the [[Royal Flying Corps]], decided to design and build a coastal patrol flying boat, the '''C.E.1''' (Coastal Experimental 1) to help combat the U-boat menace.<ref name="Hare p193">Hare 1990, p. 193.</ref><ref name="London p35-6">London 2003, pp. 35–36.</ref>

Work started on the C.E.1, designed by William Farren, in July 1917, with two prototypes being built.  It was a single-engined [[pusher configuration|pusher]], of similar layout to the pre-war [[Sopwith Bat Boat]], but considerably larger, with a wooden hull featuring a single step, and the tail surfaces carried on tailbooms above and behind the hull. The aircraft's [[interplane strut|two-bay]] [[biplane]] wings folded rearwards for storage.  The crew of two sat in tandem open cockpits, with a planned armament of up to three [[Lewis gun]]s on pillar mountings, while bombs could be carried below the lower wings.<ref name="London p35-6"/><ref name="Hare p193-4">Hare 1990, pp. 193–194.</ref><ref name="Bruce British p387-8">Bruce 1957, pp. 387–388.</ref>

The first prototype, powered by a 230&nbsp;hp (172&nbsp;kW) [[RAF 3]] [[V12 engine]] driving a four-bladed propeller was completed at Farnborough late in 1917, being sent to [[Hamble-le-Rice|Hamble]] near [[Southampton]] for final assembly and initial flight testing on 25 December.<ref name="Bruce British p388-9">Bruce 1957, pp. 388–389.</ref> The C.E.1 made its maiden flight, piloted by its designer, on 17 January 1918. After modifications to its controls it was sent to the [[Port Victoria Marine Experimental Aircraft Depot]] on the [[Isle of Grain]] for service trials in April, being quickly followed by the second prototype, which was powered by a 260&nbsp;hp (190&nbsp;kW) [[Sunbeam Maori]] engine. Trials showed that the C.E.1 was inferior to the larger and moree powerful twin-engine Felixstowe flying boats, and no production followed, the two prototypes being used for hydrodynamic experiments to validate test data obtained from model tests in a test tank at the [[National Physical Laboratory (United Kingdom)|National Physical Laboratory]].<ref name="London p35-6"/><ref name="Hare p194-5">Hare 1990, pp. 194–195.</ref>

==Specifications (Second prototype - Maori engine) ==
{{Aircraft specs
|ref=The Royal Aircraft Factory<ref name="Hare p195-6">Hare 1990, pp. 195–196.</ref> 
|prime units?=imp 
<!--
        General characteristics
-->
|genhide=

|crew=2
|capacity=
|length m=
|length ft=36
|length in=3
|length note=
|span m=
|span ft=46
|span in=0
|span note=
|height m=
|height ft=13
|height in=4
|height note=
|wing area sqm=
|wing area sqft=609
|wing area note=
|aspect ratio=<!-- give where relevant eg sailplanes  -->
|airfoil=
|empty weight kg=
|empty weight lb=3342
|empty weight note=
|gross weight kg=
|gross weight lb=5000
|gross weight note=
|max takeoff weight kg=
|max takeoff weight lb=
|max takeoff weight note=
|fuel capacity=
|more general=
<!--
        Powerplant
-->
|eng1 number=1
|eng1 name=[[Sunbeam Maori]]
|eng1 type=water-cooled [[V12 engine]]
|eng1 kw=<!-- prop engines -->
|eng1 hp=260<!-- prop engines -->
|eng1 note=
|power original=

|prop blade number=<!-- propeller aircraft -->
|prop name=
|prop dia m=<!-- propeller aircraft -->
|prop dia ft=<!-- propeller aircraft -->
|prop dia in=<!-- propeller aircraft -->
|prop note=

|rot number=<!-- helicopters -->
|rot dia m=<!-- helicopters -->
|rot dia ft=<!-- helicopters -->
|rot dia in=<!-- helicopters -->
|rot area sqm=<!-- helicopters -->
|rot area sqft=<!-- helicopters -->
|rot area note=
<!--
        Performance
-->
|perfhide=

|max speed kmh=
|max speed mph=92
|max speed kts=
|max speed note=at sea level
|max speed mach=<!-- supersonic aircraft -->
|cruise speed kmh=
|cruise speed mph=
|cruise speed kts=
|cruise speed note=
|endurance=3 hrs 45 min<!-- if range unknown -->
|ceiling m=
|ceiling ft=7500
|ceiling note=
|climb rate ms=
|climb rate ftmin=
|climb rate note=
|time to altitude=

|more performance=
<!--
        Armament
-->

|guns= 3× mountings for machine guns<ref name="Bruce British p390">Bruce 1957, p. 390.</ref>
|bombs= racks for bombs under inner wings
|rockets=

|avionics=
}}

==See also==
{{aircontent
<!-- include as many lines are appropriate. additional lines/entries with carriage return. -->
|see also=
|related=<!-- related developments -->
|similar aircraft=<!-- similar or comparable aircraft -->
*[[AD Flying Boat]]
*[[Sopwith Bat Boat]]
|lists=<!-- related lists -->
}}

==Notes==
{{reflist}}

==References==
{{commons category|Royal Aircraft Factory}}
{{refbegin}}
*Bruce, J.M. ''British Aeroplanes 1914–1918''. London:Putnam, 1957.
*Hare, Paul R. ''The Royal Aircraft Factory''. London:Putnam, 1990. ISBN 0-85177-843-7.
*London, Peter. ''British Flying Boats''. Stroud, UK:Sutton Publishing, 2003. ISBN 0-7509-2695-3.
<!-- insert the reference sources here -->
{{refend}}
<!-- ==Further reading== -->
<!-- ==External links== -->
<!-- Navboxes go here -->

{{Royal Aircraft Factory aircraft}}
{{wwi-air}}

[[Category:Flying boats]]
[[Category:British patrol aircraft 1910–1919]]
[[Category:Royal Aircraft Factory aircraft|CE01]]
[[Category:Single-engined pusher aircraft]]
[[Category:Biplanes]]