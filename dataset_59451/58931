{{Infobox journal
| title = Minerva Medica
| cover = [[File:Minerva Medica journal cover.jpg|200px]]
| caption = 
| former_name = 
| language = English, Italian
| abbreviation = Minerva Med.
| discipline = [[Internal medicine]]
| editor = M. L. Benzo
| publisher = [[Edizioni Minerva Medica]]
| country =
| history = 1909-present
| frequency = Bimonthly
| openaccess = 
| license = 
| impact = 1.202
| impact-year = 2013
| ISSN = 0026-4806
| eISSN = 1827-1669
| CODEN = MIMEAO
| JSTOR = 
| LCCN = 
| OCLC = 801136561
| website = http://www.minervamedica.it/en/journals/minerva-medica/index.php
| link1 = http://www.minervamedica.it/en/journals/minerva-medica/issue.php?cod=last
| link1-name = Online access
| link2 = http://www.minervamedica.it/en/journals/minerva-medica/archive.php
| link2-name = Online archive
}}
'''''Minerva Medica''''' is a bimonthly [[peer-reviewed]] [[medical journal]] published in English and Italian.<ref name=UWEB/> It was established in 1909 and is published by [[Edizioni Minerva Medica]]. The [[editor-in-chief]] is M. L. Benzo. In 1970 it absorbed ''Minerva Medica Siciliana''.

==Abstracting and indexing==
The journal is abstracted and indexed in:
{{columns-list|colwidth=30em|
* [[Abstracts in Anthropology]]<ref name=UWEB/>
* [[Chemical Abstracts]]<ref name=CASSI>{{cite web |url=http://cassi.cas.org/search.jsp |title=CAS Source Index |publisher=[[American Chemical Society]] |work=[[Chemical Abstracts Service]] |accessdate=2014-12-15}}</ref>
* [[Current Contents]]/Clinical Medicine<ref name=ISI/>
* [[Embase]]<ref name=UWEB/>
* [[Index Medicus]]/[[MEDLINE]]/[[PubMed]]<ref name=MEDLINE>{{cite web |url= http://www.ncbi.nlm.nih.gov/nlmcatalog/0400732 |title= ''Minerva Medica'' |work=NLM Catalog |publisher= [[National Center for Biotechnology Information]] |accessdate= }}</ref>
* [[Reactions Weekly]]<ref name=UWEB/>
* [[Referativnyi Zhurnal]]<ref name=UWEB/>
* [[Science Citation Index Expanded]]<ref name=ISI>{{cite web |url= http://ip-science.thomsonreuters.com/mjl/ |title= Master Journal List |publisher= [[Thomson Reuters]] |work= Intellectual Property & Science |accessdate= }}</ref>
* [[Scopus]]<ref name=UWEB/>
}}
According to the ''[[Journal Citation Reports]]'', the journal has a 2013 [[impact factor]] of 1.202, ranking it 83rd out of 156 journals in the category "Medicine, General & Internal".<ref name=WoS>{{cite book |year=2014 |chapter=Journals Ranked by Impact: Medicine, General & Internal |title=2013 [[Journal Citation Reports]] |publisher=[[Thomson Reuters]] |edition=Science |series=[[Web of Science]]}}</ref>

==History==
The journal was established in 1909, but publication was suspended for 1920.<ref name=MEDLINE/> From 1921 to 1929 volumes were numbered as a new series from 1 to 9 but constitute, in fact, volumes 12 to 20.<ref name=MEDLINE/> In 1970 ''Minerva Medica'' absorbed ''Minerva Medica Siciliana'', previously titled ''Gazzetta Medica Siciliana'', which was established in 1869.<ref name=UWEB>{{cite web |title=''Minerva Medica'' |work=[[Ulrich's Periodicals Directory]] |url= http://www.ulrichsweb.serialssolutions.com/title/1418639819013/604995 |accessdate=2014-12-15 |subscription=yes}}</ref> The absorbed publication continues as a supplement with its own numbering.<ref name=CASSI/>

==References==
{{reflist|30em}}

==External links==
*{{Official website|http://www.minervamedica.it/en/journals/minerva-medica/index.php}}

[[Category:Internal medicine journals]]
[[Category:Multilingual journals]]
[[Category:Bimonthly journals]]
[[Category:Publications established in 1909]]